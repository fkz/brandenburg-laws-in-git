## Verordnung zur Festsetzung des Wasserschutzgebietes für das Wasserwerk Rehbrücke

Auf Grund des § 51 Absatz 1 Satz 1 Nummer 1 und Satz 2 und Absatz 2 und des § 52 Absatz 1 des Wasserhaushaltsgesetzes vom 31.
Juli 2009 (BGBl.
I S.
2585) in Verbindung mit § 15 Absatz 1 Satz 1 und Absatz 2 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr.
20) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1  
Allgemeines
-----------------

(1) Zur Sicherung der öffentlichen Wasserversorgung wird zum Schutz des Grundwassers im Einzugsgebiet des Wasserwerkes Rehbrücke das in § 2 näher umschriebene Wasserschutzgebiet festgesetzt.
Begünstigte ist die Energie und Wasser Potsdam GmbH.

(2) Das Wasserschutzgebiet gliedert sich in den Fassungsbereich (Zone I), in die engere Schutzzone (Zone II) und in die weitere Schutzzone (Zone III).
Die Zone III unterteilt sich in die Zone III A und die Zone III B.

§ 2  
Räumlicher Geltungsbereich
--------------------------------

(1) Lage und Größe des Wasserschutzgebietes und der Schutzzonen ergeben sich aus der Übersichtskarte in der Anlage 2 und den in Absatz 2 genannten Karten.

(2) Die Schutzzonen sind in der topografischen Karte im Maßstab 1 : 10 000 in der Anlage 3 und außerdem in der Liegenschaftskarte im Maßstab 1 : 2 500 in der Anlage 4, die aus 20 Blättern besteht, dargestellt.
Für die Abgrenzung der Schutzzonen ist die Darstellung in der Liegenschaftskarte maßgebend.

(3) Zu Informationszwecken werden zusätzlich auf Papier ausgefertigte Exemplare der in Absatz 2 genannten Karten bei den unteren Wasserbehörden des Landkreises Potsdam-Mittelmark und der Landeshauptstadt Potsdam sowie in den Gemeinden Nuthetal, Stahnsdorf und Michendorf hinterlegt und können dort während der Dienststunden von jedermann kostenlos eingesehen werden.
Diese Karten sind mit dem Dienstsiegel des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (Siegelnummer 20) versehen.
Eine weitere so gesiegelte Ausfertigung der Karten befindet sich im Ministerium für Umwelt, Gesundheit und Verbraucherschutz.

(4) Veränderungen der Topografie sowie von Flurstücksgrenzen oder -bezeichnungen berühren den räumlichen Geltungsbereich der Schutzzonen nicht.

§ 3  
Schutz der Zone III B
---------------------------

In der Zone III B sind verboten:

1.  das Düngen mit Gülle, Jauche, Geflügelkot, Festmist, Silagesickersaft, Gärresten, Wirtschaftsdüngern aus pflanzlichen Stoffen, Bodenhilfsstoffen, Kultursubstraten, Pflanzenhilfsmitteln, gütegesicherten Grünabfall- oder Bioabfallkomposten, Abfällen aus der Herstellung oder Verarbeitung landwirtschaftlicher Erzeugnisse  
    oder sonstigen Düngemitteln mit im Sinne des § 2 Nummer 10 der Düngeverordnung wesentlichen Nährstoffgehalten an Stickstoff oder Phosphat,
    
    1.  wenn die Düngung nicht im Sinne des § 3 Absatz 4 der Düngeverordnung in betriebsspezifisch analysierten zeit- und bedarfsgerechten Gaben oder nicht durch Geräte, die den allgemein anerkannten Regeln der Technik entsprechen, erfolgt,
        
    2.  wenn keine schlagbezogenen Aufzeichnungen über die Zu- und Abfuhr von Stickstoff und Phosphat erstellt und mindestens sieben Jahre lang nach Ablauf des Düngejahres aufbewahrt werden,
        
    3.  auf abgeerntetem Ackerland, wenn nicht unmittelbar Folgekulturen einschließlich Zwischenfrüchte angebaut werden,
        
    4.  auf landwirtschaftlich oder erwerbsgärtnerisch genutzten Flächen vom 1.
        Oktober bis 15.
        Februar,
        
    5.  auf landwirtschaftlich oder erwerbsgärtnerisch genutzten Flächen bei Verwendung von Gülle, Jauche, sonstigen flüssigen organischen oder organisch-mineralischen Düngemitteln, einschließlich Gärresten vom 15.
        September bis 1.
        März,
        
    6.  auf Brachland oder stillgelegten Flächen oder
        
    7.  auf wassergesättigten, oberflächig oder in der Tiefe gefrorenen oder schneebedeckten Böden,
        
2.  das Lagern oder Ausbringen von Fäkalschlamm oder Klärschlämmen aller Art einschließlich in Biogasanlagen behandelten Klärschlämmen, Abfällen aus der Herstellung und Verarbeitung nichtlandwirtschaftlicher Erzeugnisse und nicht gütegesicherter Grünabfall- und Bioabfallkomposte, ausgenommen die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen,
    
3.  das Errichten, Erweitern oder Betreiben von Dunglagerstätten, ausgenommen befestigte Dunglagerstätten mit Sickerwasserfassung und dichtem Jauchebehälter, der über eine Leckageerkennungseinrichtung verfügt,
    
4.  das Errichten oder Betreiben von Erdbecken zur Lagerung von Gülle, Jauche oder Silagesickersäften,
    
5.  das Errichten, Erweitern oder Betreiben von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle, Jauche, Silagesickersaft, Gärresten und flüssigem Kompost, ausgenommen Anlagen mit Leckageerkennungseinrichtung und Sammeleinrichtung, wenn der unteren Wasserbehörde
    
    1.  vor Inbetriebnahme,
        
    2.  bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung sowie
        
    3.  wiederkehrend alle fünf Jahre
        
    
    ein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit der Sammeleinrichtung vorgelegt wird,
    
6.  das Lagern von organischen oder mineralischen Düngern oder von Silage auf unbefestigten Flächen oder auf nicht baugenehmigten Anlagen, ausgenommen das Lagern von Kompost aus dem eigenen Haushalt oder Garten,
    
7.  das Errichten von ortsfesten Anlagen für die Silierung von Pflanzen oder die Lagerung von Silage, ausgenommen
    
    1.  Anlagen mit Silagesickersaft-Sammelbehälter, der über eine Leckageerkennungseinrichtung verfügt, und
        
    2.  Anlagen mit Ableitung in Jauche- oder Güllebehälter,
        
    
    wenn der unteren Wasserbehörde vor Inbetriebnahme, bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung, sowie wiederkehrend alle fünf Jahre ein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit der Behälter und Leitungen vorgelegt wird,
    
8.  die Silierung von Pflanzen oder Lagerung von Silage außerhalb ortsfester Anlagen, ausgenommen Ballensilage im Wickelverfahren,
    
9.  das Errichten oder Erweitern von Stallungen für Tierbestände für mehr als 50 Großvieheinheiten gemäß Anlage 1 Nummer 1,
    
10.  die Anwendung von Pflanzenschutzmitteln oder von Biozidprodukten,
    
    1.  wenn die Pflanzenschutzmittel nicht für Wasserschutzgebiete zugelassen sind,
        
    2.  wenn die Zulassungs- und Anwendungsbestimmungen nicht eingehalten werden,
        
    3.  wenn der Einsatz von Pflanzenschutzmitteln nicht durch Anwendung der Allgemeinen Grundsätze des integrierten Pflanzenschutzes und für Biozidprodukte in entsprechender Weise auf das notwendige Maß beschränkt wird,
        
    4.  wenn keine flächenbezogenen Aufzeichnungen für Pflanzenschutzmittel nach dem Pflanzenschutzgesetz und für Biozidprodukte in entsprechender Weise über den Einsatz auf erwerbsgärtnerisch, land- oder forstwirtschaftlich genutzten Flächen geführt und mindestens sieben Jahre lang nach dem Einsatz aufbewahrt werden,
        
    5.  in einem Abstand von weniger als 10 Metern zu oberirdischen Gewässern,
        
    6.  zur Bodenentseuchung oder
        
    7.  auf Dauergrünland und Grünlandbrachen,
        
11.  die Beregnung landwirtschaftlich oder erwerbsgärtnerisch genutzter Flächen, wenn die Beregnungshöhe 20 Millimeter pro Tag oder 60 Millimeter pro Woche überschreitet,
    
12.  der Umbruch von Dauergrünland oder von Grünlandbrachen,
    
13.  das Anlegen von Schwarzbrache im Sinne der Anlage 1 Nummer 3,
    
14.  Erstaufforstungen mit Nadelbaumarten oder Robinien,
    
15.  die Umwandlung von Wald in eine andere Nutzungsart,
    
16.  Holzerntemaßnahmen, die eine gleichmäßig verteilte Überschirmung von weniger als 60 Prozent des Waldbodens oder Freiflächen größer als 1 000 Quadratmeter erzeugen,
    
17.  Erdaufschlüsse im Sinne des § 49 Absatz 1 des Wasserhaushaltsgesetzes, selbst wenn Grundwasser nicht aufgedeckt wird, ausgenommen das Verlegen von Ver- und Entsorgungsleitungen und die Herstellung von Baugruben und Bohrungen,
    
18.  das Errichten oder Erweitern von vertikalen Anlagen zur Gewinnung von Erdwärme,
    
19.  das Errichten oder Erweitern von Rohrleitungsanlagen für wassergefährdende Stoffe, ausgenommen Rohrleitungsanlagen im Sinne des § 62 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes,
    
20.  das Errichten, Erweitern oder Betreiben von Anlagen zur behälterlosen Lagerung oder Ablagerung von Stoffen im Untergrund,
    
21.  das Behandeln, Lagern oder Ablagern von Abfall, tierischen Nebenprodukten oder bergbaulichen Rückständen, ausgenommen
    
    1.  die vorübergehende Lagerung in dichten Behältern,
        
    2.  die ordnungsgemäße kurzzeitige Bereitstellung von vor Ort angefallenem Abfall zum Abtransport durch den Entsorgungspflichtigen und
        
    3.  die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen,
        
22.  das Ein- oder Aufbringen von Abfällen, bergbaulichen Rückständen oder Ersatzbaustoffen in oder auf Böden oder deren Einbau in bodennahe technische Bauwerke,
    
23.  das Errichten oder Erweitern von Anlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden radioaktiver Stoffe im Sinne des Atomgesetzes, ausgenommen für medizinische Anwendungen sowie für Mess-, Prüf- und Regeltechnik,
    
24.  der Umgang mit wassergefährdenden Stoffen außerhalb von zugelassenen Anlagen, Vorrichtungen und Behältnissen, aus denen ein Eindringen in den Boden nicht möglich ist,
    
25.  das Einleiten oder Einbringen von wassergefährdenden Stoffen im Sinne des § 62 Absatz 3 des Wasserhaushaltsgesetzes in den Untergrund oder in Gewässer,
    
26.  das Errichten von Industrieanlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden wassergefährdender Stoffe in großem Umfang, wie zum Beispiel in Raffinerien, Metallhütten oder chemischen Fabriken,
    
27.  das Errichten von Kraftwerken oder Heizwerken, die der Genehmigungspflicht nach Bundesimmissionsschutzrecht unterliegen, ausgenommen mit Gas, Sonnenenergie oder Windkraft betriebene Anlagen,
    
28.  das Errichten von Biogasanlagen,
    
29.  das Errichten oder Erweitern von Abwasserbehandlungsanlagen, ausgenommen
    
    1.  die Sanierung bestehender Abwasserbehandlungsanlagen zugunsten des Gewässerschutzes und
        
    2.  Abwasservorbehandlungsanlagen wie Fett-, Leichtflüssigkeits- oder Amalgamabscheider,
        
30.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik eingehalten werden,
    
31.  das Errichten oder Erweitern von Abwassersammelgruben, ausgenommen
    
    1.  Anlagen mit allgemeiner bauaufsichtlicher Zulassung des Deutschen Institutes für Bautechnik und
        
    2.  monolithische Sammelgruben aus Beton, die den allgemein anerkannten Regeln der Technik entsprechen,
        
32.  das Betreiben oder Unterhalten von Abwassersammelgruben, wenn der unteren Wasserbehörde nicht
    
    1.  vor Inbetriebnahme,
        
    2.  bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung sowie
        
    3.  wiederkehrend alle fünf Jahre
        
    
    ein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit vorgelegt wird,
    
33.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten, ausgenommen Anlagen mit dichtem Behälter,
    
34.  das Einleiten von Abwasser – mit Ausnahme von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 Nummer 4 – in oberirdische Gewässer, sofern die Einleitung nicht schon zum Zeitpunkt des Inkrafttretens dieser Verordnung wasserrechtlich zugelassen war,
    
35.  das Ausbringen von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes,
    
36.  das Einleiten oder Versickern von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser,
    
37.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen
    
    1.  das breitflächige Versickern von Niederschlagswasserabflüssen von gering belasteten Herkunftsflächen im Sinne der Anlage 1 Nummer 4 über die belebte Bodenzone einer mindestens 20 Zentimeter mächtigen und bewachsenen Oberbodenschicht oder
        
    2.  mit wasserrechtlicher Erlaubnis,
        
    
    sofern die Versickerung außerhalb von Altlasten, Altlastenverdachtsflächen oder Flächen mit schädlichen Bodenveränderungen und nur auf Flächen mit einem zu erwartenden Flurabstand des Grundwassers von 100 Zentimetern oder größer erfolgt,
    
38.  das Anwenden von Auftaumitteln auf Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen auf der Bundesautobahn 115, den Landes- und Kreisstraßen sowie ausgenommen bei Extremwetterlagen wie Eisregen,
    
39.  das Errichten oder Erweitern von Straßen oder Wegen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik für bautechnische Maßnahmen an Straßen in Wasserschutzgebieten eingehalten werden,
    
40.  das Errichten von Rangier- oder Güterbahnhöfen,
    
41.  das Verwenden von Baustoffen, Böden oder anderen Materialien, die auslaug- und auswaschbare wassergefährdende Stoffe enthalten (zum Beispiel Schlacke, Bauschutt, Teer, Imprägniermittel) für Bau- und Unterhaltungsmaßnahmen, zum Beispiel im Straßen-, Wege-, Deich-, Wasser-, Landschafts- oder Tiefbau,
    
42.  das Errichten von öffentlichen Freibädern oder Zeltplätzen sowie Camping aller Art, ausgenommen
    
    1.  Einrichtungen mit ordnungsgemäßer Abfall- und Abwasserentsorgung und
        
    2.  das Zelten von Fuß-, Rad-, Reit- und Wasserwanderern abseits von Zelt- und Campingplätzen für eine Nacht,
        
43.  das Errichten, Erweitern oder Betreiben von Sportanlagen, ausgenommen Anlagen mit ordnungsgemäßer Abfall- und Abwasserentsorgung,
    
44.  das Errichten oder Erweitern von Motorsportanlagen,
    
45.  das Errichten oder Erweitern von Schießständen oder Schießplätzen für Feuerwaffen, ausgenommen Schießstände in geschlossenen Räumen,
    
46.  das Errichten von Golfanlagen,
    
47.  das Errichten von Flugplätzen im Sinne des § 6 Absatz 1 Satz 1 des Luftverkehrsgesetzes,
    
48.  das Starten oder Landen motorgetriebener Luftfahrzeuge, mit Ausnahme in Fällen des § 25 Absatz 2 des Luftverkehrsgesetzes,
    
49.  das Errichten oder Erweitern von militärischen Anlagen, Standort- oder Truppenübungsplätzen,
    
50.  das Durchführen von militärischen Übungen, ausgenommen das Durchfahren auf klassifizierten Straßen,
    
51.  Bergbau einschließlich die Aufsuchung oder Gewinnung von Erdöl oder Erdgas,
    
52.  das Durchführen von Sprengungen, sofern die Gefahr besteht, dass dabei das Grundwasser aufgedeckt wird,
    
53.  die Neuausweisung oder Erweiterung von Industriegebieten.
    

§ 4  
Schutz der Zone III A
---------------------------

Die Verbote der Zone III B gelten auch in der Zone III A.
In der Zone III A sind außerdem verboten:

1.  das Errichten von Anlagen zur Lagerung von Gülle, Jauche, Silagesickersäften, Gärresten oder flüssigem Kompost, ausgenommen Hochbehälter, bei denen Undichtigkeiten am Fußpunkt zwischen Behältersohle und aufgehender Wand sofort erkennbar sind und die über eine Leckageerkennungseinrichtung und Sammeleinrichtung verfügen,
    
2.  das Errichten von Stallungen für Tierbestände, ausgenommen für Kleintierhaltung zur Eigenversorgung,
    
3.  die Freilandtierhaltung im Sinne der Anlage 1 Nummer 2, wenn die Ernährung der Tiere nicht im Wesentlichen aus der jeweils beweideten Grünlandfläche erfolgt oder wenn die Grasnarbe flächig verletzt wird, ausgenommen Kleintierhaltung für die Eigenversorgung,
    
4.  das Errichten oder Erweitern von Gartenbaubetrieben oder Kleingartenanlagen, ausgenommen Gartenbaubetriebe, die in geschlossenen Systemen produzieren,
    
5.  die Erstanlage oder Erweiterung von Baumschulen oder forstlichen Pflanzgärten, Weihnachtsbaumkulturen sowie von gewerblichem Wein-, Hopfen-, Gemüse-, Obst- oder Zierpflanzenanbau, ausgenommen Gemüse- sowie Zierpflanzenanbau unter Glas in geschlossenen Systemen und Containerproduktion von Baumschulprodukten auf versiegelten Flächen,
    
6.  das Einrichten von Holzlagerplätzen über 100 Raummeter, die dauerhaft oder unter Einsatz von Nassholzkonservierung betrieben werden,
    
7.  das Errichten, Erweitern oder Erneuern von
    
    1.  Bohrungen, welche gering leitende Deckschichten über oder unter dem genutzten Grundwasserleiter verletzen können,
        
    2.  von Grundwassermessstellen oder
        
    3.  Brunnen,
        
    
    ausgenommen das Erneuern von Brunnen für Entnahmen mit wasserrechtlicher Erlaubnis oder Bewilligung,
    
8.  das Errichten oder Erweitern von Anlagen zum Umgang mit wassergefährdenden Stoffen, ausgenommen doppelwandige Anlagen mit Leckanzeigegerät und ausgenommen Anlagen, die mit einem Auffangraum ausgerüstet sind, der das maximal in der Anlage vorhandene Volumen wassergefährdender Stoffe aufnehmen kann, und soweit
    
    1.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 1 das für die Anlage maßgebende Volumen von 1 000 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 1 die für die Anlage maßgebende Masse von 1 000 Tonnen,
        
    2.  in oberirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 2 das für die Anlage maßgebende Volumen von 100 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 2 die für die Anlage maßgebende Masse von 100 Tonnen,
        
    3.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 2 das für die Anlage maßgebende Volumen von 10 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 2 die für die Anlage maßgebende Masse von 10 Tonnen,
        
    4.  in oberirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 3 das für die Anlage maßgebende Volumen von 10 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 3 die für die Anlage maßgebende Masse von 10 Tonnen,
        
    5.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 3 das für die Anlage maßgebende Volumen von 1 Kubikmeter beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 3 die für die Anlage maßgebende Masse von 1 Tonne
        
    
    nicht überschritten wird,
    
9.  das Errichten oder Erweitern von Niederschlagswasser- oder Mischwasserentlastungsbauwerken,
    
10.  das Errichten oder Erweitern von Bahnhöfen oder Schienenwegen der Eisenbahn, ausgenommen Baumaßnahmen an vorhandenen Anlagen zur Anpassung an den Stand der Technik und zum Erhalt oder zur Verbesserung der Verkehrssicherheit,
    
11.  das Abhalten oder Durchführen von Märkten, Volksfesten oder Großveranstaltungen außerhalb der dafür vorgesehenen Anlagen,
    
12.  das Durchführen von Motorsportveranstaltungen, ausgenommen das Durchfahren auf klassifizierten Straßen,
    
13.  Bestattungen, ausgenommen innerhalb bereits bei Inkrafttreten dieser Verordnung bestehender Friedhöfe,
    
14.  die Darstellung von neuen Bauflächen oder Baugebieten im Rahmen der vorbereitenden Bauleitplanung, wenn darin eine Neubebauung bisher unbebauter Gebiete vorgesehen wird,
    
15.  die Festsetzung von neuen Baugebieten im Rahmen der verbindlichen Bauleitplanung, ausgenommen
    
    1.  Gebiete, die im zum Zeitpunkt des Inkrafttretens dieser Verordnung gültigen Flächennutzungsplan als Bauflächen oder Baugebiete dargestellt sind, und
        
    2.  die Überplanung von Bestandsgebieten, wenn dies zu keiner wesentlichen Erhöhung der zulässigen Grundfläche im Sinne des § 19 Absatz 2 der Baunutzungsverordnung führt.
        

§ 5  
Schutz der Zone II
------------------------

Die Verbote der Zonen III B und III A gelten auch in der Zone II.
In der Zone II sind außerdem verboten:

1.  das Düngen mit Gülle, Jauche, Festmist, Wirtschaftsdüngern aus pflanzlichen Stoffen, Gärresten, Bodenhilfsstoffen, Kultursubstraten, Pflanzenhilfsmitteln, gütegesicherten Grünabfall- oder Bioabfallkomposten, Abfällen aus der Herstellung oder Verarbeitung landwirtschaftlicher Erzeugnisse oder sonstigen organischen Düngern sowie die Anwendung von Silagesickersaft,
    
2.  das Errichten von Dunglagerstätten,
    
3.  das Errichten von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle, Jauche, Silagesickersaft, Gärresten und flüssigem Kompost,
    
4.  die Silierung von Pflanzen oder Lagerung von Silage,
    
5.  die Freilandtierhaltung im Sinne der Anlage 1 Nummer 2, ausgenommen Kleintierhaltung für die Eigenversorgung, sofern diese bereits bei Inkrafttreten dieser Verordnung ausgeübt wurde,
    
6.  die Beweidung,
    
7.  die Anwendung von Biozidprodukten außerhalb geschlossener Gebäude oder von Pflanzenschutzmitteln,  
    
8.  die Beregnung landwirtschaftlich oder erwerbsgärtnerisch genutzter Flächen,
    
9.  das Errichten, Erweitern oder Erneuern von Dränungen oder Entwässerungsgräben,
    
10.  der Einsatz von forstwirtschaftlichen Kraftfahrzeugen abseits von Straßen, Wegen oder forstwirtschaftlichen Rückegassen,
    
11.  das Vergraben, Lagern oder Ablagern von Tierkörpern oder Teilen davon,
    
12.  das Errichten oder Betreiben von Wildfütterungen, Kirrungen oder Luderplätzen,
    
13.  das Errichten von Anlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden wassergefährdender Stoffe,
    
14.  der Einsatz von mineralischen Schmierstoffen zur Verlustschmierung oder mineralischen Schalölen,
    
15.  das Lagern, Abfüllen oder Umschlagen wassergefährdender Stoffe, ausgenommen haushaltsübliche Kleinstmengen,
    
16.  das Befahren mit Fahrzeugen mit wassergefährdender Ladung,
    
17.  das Errichten oder Erweitern von Transformatoren oder Stromleitungen mit flüssigen wassergefährdenden Kühl- oder Isoliermitteln,
    
18.  das Behandeln, Lagern oder Ablagern von Abfall, tierischen Nebenprodukten oder bergbaulichen Rückständen, ausgenommen
    
    1.  die ordnungsgemäße kurzzeitige Bereitstellung von in der Zone II angefallenem Abfall zum Abtransport durch den Entsorgungspflichtigen und
        
    2.  die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen,
        
19.  der Umgang mit radioaktiven Stoffen im Sinne des Atomgesetzes,
    
20.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, ausgenommen Anlagen, die zur Entsorgung vorhandener Bebauung dienen und wenn hierbei die allgemein anerkannten Regeln der Technik eingehalten werden,
    
21.  das Errichten, Erweitern oder Betreiben von Abwassersammelgruben,
    
22.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten,
    
23.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen das breitflächige Versickern von Niederschlagswasserabflüssen von gering belasteten Herkunftsflächen im Sinne der Anlage 1 Nummer 4 über die belebte Bodenzone einer mindestens 20 Zentimeter mächtigen und bewachsenen Oberbodenschicht,
    
24.  das Errichten oder Erweitern von Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen Wege mit breitflächiger Versickerung der Niederschlagswasserabflüsse über die belebte Bodenzone einer mindestens 20 Zentimeter mächtigen und bewachsenen Oberbodenschicht,
    
25.  das Errichten von öffentlichen Freibädern oder Zeltplätzen sowie Camping aller Art,
    
26.  das Errichten von Sportanlagen,
    
27.  das Abhalten oder Durchführen von Sportveranstaltungen, Märkten, Volksfesten oder Großveranstaltungen,
    
28.  das Errichten oder Erweitern von Baustelleneinrichtungen oder Baustofflagern,
    
29.  das Durchführen von Bohrungen, ausgenommen Maßnahmen zur Abwehr von Gefahren für das Grundwasser unter Beachtung der Sicherheitsvorkehrungen zum Grundwasserschutz,
    
30.  das Durchführen von unterirdischen Sprengungen,
    
31.  das Errichten oder Erweitern von baulichen Anlagen, ausgenommen Veränderungen in Gebäuden und Instandhaltungsmaßnahmen,
    
32.  das Ausweisen von Hundeauslaufplätzen.
    

§ 6  
Schutz der Zone I
-----------------------

Die Verbote der Zonen III B, III A und II gelten auch in der Zone I.
In der Zone I sind außerdem verboten:

1.  das Betreten oder Befahren,
    
2.  landwirtschaft-, forstwirtschaft- oder gartenbauliche Nutzung,
    
3.  Veränderungen oder Aufschlüsse der Erdoberfläche.
    

§ 7  
Maßnahmen zur Wassergewinnung
-----------------------------------

Die Verbote des § 4 Nummer 8, des § 5 Nummer 13, 19, 29 bis 32 sowie des § 6 Nummer 1 und 3 gelten nicht für Maßnahmen zur Wassergewinnung, die durch diese Verordnung geschützt ist.

§ 8  
Widerruf von Befreiungen
------------------------------

(1) Befreiungen nach § 52 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes sind widerruflich und bedürfen der Schriftform.
Abweichend von Satz 1 ist eine Befreiung von dem Verbot gemäß § 3 Nummer 53 und von den Verboten gemäß § 4 Nummer 14 und 15 nicht widerruflich.

(2) Im Fall des Widerrufs einer Befreiung kann die untere Wasserbehörde vom Grundstückseigentümer verlangen, dass der frühere Zustand wiederhergestellt wird, sofern es das Wohl der Allgemeinheit, insbesondere der Schutz der Wasserversorgung, erfordert.

§ 9  
Sicherung und Kennzeichnung des Wasserschutzgebietes
----------------------------------------------------------

(1) Die Zone I ist von der Begünstigten auf Anordnung der unteren Wasserbehörde gegen unbefugtes Betreten, zum Beispiel durch eine Umzäunung, zu sichern.

(2) Die Begünstigte hat auf Anordnung der unteren Wasserbehörde zur Kennzeichnung des Wasserschutzgebietes im Bereich öffentlicher Verkehrsflächen bei der Straßenverkehrsbehörde die Anordnung des Verkehrszeichens 354 zu beantragen und im Bereich nichtöffentlicher Flächen in Abstimmung mit der Gemeinde nichtamtliche Hinweiszeichen aufzustellen.

§ 10  
Duldungspflichten
------------------------

(1) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet haben die Überwachung des Wasserschutzgebietes, insbesondere hinsichtlich der Beachtung dieser Verordnung und der nach ihr getroffenen Anordnungen, sowie das Beobachten der Gewässer und des Bodens durch die zuständigen Wasserbehörden, die Begünstigte oder deren Beauftragte zu dulden.

(2) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet sind auf Anordnung der unteren Wasserbehörde verpflichtet,

1.  das Errichten und Unterhalten von Einrichtungen zur Sicherung der Zone I gegen unbefugtes Betreten,
    
2.  das Aufstellen, Unterhalten und Beseitigen von Hinweis-, Warn-, Gebots- und Verbotszeichen,
    
3.  das Betreten der Grundstücke durch Bedienstete der zuständigen Behörden, die Begünstigte oder deren Beauftragte zum Beobachten, Messen und Untersuchen des Grundwassers und zur Entnahme von Boden- und Vegetationsproben sowie
    
4.  das Anlegen und Betreiben von Grundwassermessstellen
    

zu dulden.
Die Anordnung erfolgt durch schriftlichen Bescheid gegenüber den betroffenen Eigentümern oder Nutzungsberechtigten.
Soweit bergrechtliche Belange berührt sind, ergeht die Entscheidung im Benehmen mit der zuständigen Bergbehörde.

(3) Auf Verlangen der unteren Wasserbehörde ist Einsicht in die Aufzeichnungen nach § 3 Nummer 1 Buchstabe b und Nummer 10 Buchstabe c dieser Verordnung zu gewähren oder diese unverzüglich vorzulegen.

§ 11  
Übergangsregelung
------------------------

(1) Für bei Inkrafttreten dieser Verordnung errichtete und betriebene Anlagen gilt das Verbot des Betreibens gemäß § 3 Nummer 3, 4 und 5 nach einem Jahr nach Inkrafttreten dieser Verordnung.

(2) Für bei Inkrafttreten dieser Verordnung bestehende Einleitungen oder Versickerungen von Niederschlagswasserabflüssen von mittel oder hoch belasteten Herkunftsflächen in den Untergrund ohne wasserrechtliche Erlaubnis gelten die Verbote des § 3 Nummer 34 und 37 nach einem Jahr nach Inkrafttreten dieser Verordnung.

§ 12  
Ordnungswidrigkeiten
---------------------------

(1) Ordnungswidrig im Sinne des § 103 Absatz 1 Nummer 7a des Wasserhaushaltsgesetzes handelt, wer vorsätzlich oder fahrlässig eine nach den §§ 3, 4, 5 oder 6 verbotene Handlung ohne eine Befreiung gemäß § 52 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes vornimmt.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

§ 13  
Inkrafttreten, Außerkrafttreten
--------------------------------------

(1) Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Ausgenommen davon sind die Maßgaben zur landwirtschaftlichen Bodennutzung nach § 5 Nummer 1, 5, 6 und 7, die am 1.
Januar 2014 in Kraft treten.

(2) Gleichzeitig tritt das mit den folgenden Beschlüssen

1.  Beschluss Nummer 0122 vom 20.
    Dezember 1973 des Rates des Kreises Potsdam,
    
2.  Beschluss Nummer 84-13/81 vom 6.
    Mai 1981 des Kreistages Potsdam,
    
3.  Beschluss vom 19.
    Dezember 1985 der Stadtverordnetenversammlung der Stadt Potsdam,
    
4.  Beschluss Nummer 0127 vom 22.
    September 1988 des Rates des Kreises Potsdam und
    
5.  Beschluss Nummer 185-26/88 vom 22.
    Dezember 1988 des Kreistages Potsdam
    

festgesetzte Trinkwasserschutzgebiet Rehbrücke außer Kraft.

Potsdam, den 27.
November 2013

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

### Anlagen

1

[WSGRehbrücke-Anlg-1](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__001Anlage-1.pdf "WSGRehbrücke-Anlg-1") 605.0 KB

2

[WSGRehbrücke-Anlg-2](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__002Anlage-2.pdf "WSGRehbrücke-Anlg-2") 2.7 MB

3

[WSGRehbrücke-Anlg-3](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__003Anlage-3.pdf "WSGRehbrücke-Anlg-3") 5.1 MB

4

[WSGRehbrücke-Anlg-4-Blatt-01](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__004Anlage-4-Blatt-01.pdf "WSGRehbrücke-Anlg-4-Blatt-01") 1.7 MB

5

[WSGRehbrücke-Anlg-4-Blatt-02](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__005Anlage-4-Blatt-02.pdf "WSGRehbrücke-Anlg-4-Blatt-02") 1.5 MB

6

[WSGRehbrücke-Anlg-4-Blatt-03](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__006Anlage-4-Blatt-03.pdf "WSGRehbrücke-Anlg-4-Blatt-03") 2.7 MB

7

[WSGRehbrücke-Anlg-4-Blatt-04](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__007Anlage-4-Blatt-04.pdf "WSGRehbrücke-Anlg-4-Blatt-04") 2.1 MB

8

[WSGRehbrücke-Anlg-4-Blatt-05](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__008Anlage-4-Blatt-05.pdf "WSGRehbrücke-Anlg-4-Blatt-05") 3.1 MB

9

[WSGRehbrücke-Anlg-4-Blatt-06](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__009Anlage-4-Blatt-06.pdf "WSGRehbrücke-Anlg-4-Blatt-06") 4.5 MB

10

[WSGRehbrücke-Anlg-4-Blatt-07](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__010Anlage-4-Blatt-07.pdf "WSGRehbrücke-Anlg-4-Blatt-07") 2.9 MB

11

[WSGRehbrücke-Anlg-4-Blatt-08](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__011Anlage-4-Blatt-08.pdf "WSGRehbrücke-Anlg-4-Blatt-08") 1.7 MB

12

[WSGRehbrücke-Anlg-4-Blatt-09](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__012Anlage-4-Blatt-09.pdf "WSGRehbrücke-Anlg-4-Blatt-09") 1.8 MB

13

[WSGRehbrücke-Anlg-4-Blatt-10](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__013Anlage-4-Blatt-10.pdf "WSGRehbrücke-Anlg-4-Blatt-10") 1.8 MB

14

[WSGRehbrücke-Anlg-4-Blatt-11](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__014Anlage-4-Blatt-11.pdf "WSGRehbrücke-Anlg-4-Blatt-11") 1.9 MB

15

[WSGRehbrücke-Anlg-4-Blatt-12](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__015Anlage-4-Blatt-12.pdf "WSGRehbrücke-Anlg-4-Blatt-12") 1.8 MB

16

[WSGRehbrücke-Anlg-4-Blatt-13](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__016Anlage-4-Blatt-13.pdf "WSGRehbrücke-Anlg-4-Blatt-13") 3.1 MB

17

[WSGRehbrücke-Anlg-4-Blatt-14](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__017Anlage-4-Blatt-14.pdf "WSGRehbrücke-Anlg-4-Blatt-14") 3.0 MB

18

[WSGRehbrücke-Anlg-4-Blatt-15](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__018Anlage-4-Blatt-15.pdf "WSGRehbrücke-Anlg-4-Blatt-15") 2.9 MB

19

[WSGRehbrücke-Anlg-4-Blatt-16](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__019Anlage-4-Blatt-16.pdf "WSGRehbrücke-Anlg-4-Blatt-16") 2.5 MB

20

[WSGRehbrücke-Anlg-4-Blatt-17](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__020Anlage-4-Blatt-17.pdf "WSGRehbrücke-Anlg-4-Blatt-17") 2.3 MB

21

[WSGRehbrücke-Anlg-4-Blatt-18](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__021Anlage-4-Blatt-18.pdf "WSGRehbrücke-Anlg-4-Blatt-18") 2.6 MB

22

[WSGRehbrücke-Anlg-4-Blatt-19](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__022Anlage-4-Blatt-19.pdf "WSGRehbrücke-Anlg-4-Blatt-19") 2.6 MB

23

[WSGRehbrücke-Anlg-4-Blatt-20](/br2/sixcms/media.php/68/WSGRehbruecke__an__anlg__023Anlage-4-Blatt-20.pdf "WSGRehbrücke-Anlg-4-Blatt-20") 3.0 MB