## Verordnung zur Übertragung von Zuständigkeiten des Ministeriums für Infrastruktur und Landwirtschaft für die Berechnung und Zahlung von Reisekosten sowie die Bewilligung, Berechnung und Zahlung von Trennungsgeld auf die Zentrale Bezügestelle des Landes Brandenburg   (Reisekosten-Trennungsgeldzuständigkeitsübertragungsverordnung - MIL – RkTrZÜVMIL)

§ 1  
**Übertragung von Aufgaben**

(1) Die Zuständigkeit für die Berechnung und Zahlung von Reisekosten im Sinne des § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes wird ab den folgenden Zeitpunkten für die nachfolgenden Bereiche auf die Zentrale Bezügestelle des Landes Brandenburg übertragen:

1.  für das Ministerium für Infrastruktur und Landwirtschaft ab dem 1.
    Januar 2014,
2.  für das Landesamt für Bauen und Verkehr ab dem 1.
    Februar 2014,
3.  für das Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung ab dem 1.
    März 2014,
4.  für den Landesbetrieb Straßenwesen ab dem 1.
    Mai 2014,
5.  für den Landesbetrieb Forst Brandenburg ab dem 1.
    Januar 2015.

(2)  Zu den in Absatz 1 genannten Zeitpunkten wird ebenso die Zuständigkeit für die Bewilligung, Berechnung und Zahlung von Trennungsgeld nach § 1 Satz 1 der Brandenburgischen Trennungsgeldverordnung in Verbindung mit  § 9 Absatz 3 der Trennungsgeldverordnung in der Fassung der Bekanntmachung vom 29.
Juni 1999 (BGBl.
I  S.
1533), die zuletzt durch Artikel 3 Absatz 38 der Verordnung vom 12.
Februar 2009 (BGBl.
I S.
320) geändert worden ist, auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

§ 2  
**Vertretung bei Klagen**

Im Rahmen der Übertragung der Zuständigkeit nach § 1 wird die Zentrale Bezügestelle des Landes Brandenburg ermächtigt, die unter § 1 Absatz 1 Nummer 1 bis 5 genannten Stellen in verwaltungs- und arbeitsgerichtlichen Streitigkeiten zu vertreten.
Dies gilt auch für Anträge in einstweiligen Rechtsschutzverfahren.

§ 3  
**Übergangsvorschrift**

Für Anträge auf Berechnung und Zahlung von Reisekosten sowie auf Bewilligung, Berechnung und Zahlung von Trennungsgeld, die vor der Übertragung der Zuständigkeit eingegangen sind und über die noch nicht abschließend entschieden worden ist, verbleibt es bei der Zuständigkeit der bisher zuständigen Stellen.
Dies gilt auch für die Vertretung in zu diesem Zeitpunkt bereits anhängigen Rechtsstreitigkeiten.