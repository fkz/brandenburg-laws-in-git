## Verordnung über das Naturschutzgebiet „Glinziger Teich- und Wiesengebiet“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
 Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1  
**Erklärung zum Schutzgebiet**

Die in § 2 näher bezeichnete Fläche im Landkreis Spree-Neiße wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Glinziger Teich- und Wiesengebiet“.

§ 2  
**Schutzgegenstand**

(1) Das Naturschutzgebiet hat eine Größe von rund 257 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Gemeinde:**

**Gemarkung:**

**Flur:**

Kolkwitz

Kolkwitz

2;

Kolkwitz

Kolkwitz

5;

Kolkwitz

Glinzig

1;

Kolkwitz

Limberg

1.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 01 bis 02 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 01 bis 05 aufgeführten Liegenschaftskarten.
 Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 3 beigefügt.

(3) Innerhalb des Naturschutzgebietes ist gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes eine Zone 1 mit Beschränkungen der landwirtschaftlichen Nutzung festgesetzt.
Die Zone 1 umfasst rund 33 Hektar und liegt in folgenden Fluren:

**Gemeinde:**

**Gemarkung:**

**Flur:**

Kolkwitz

Kolkwitz

5;

Kolkwitz

Limberg

1;

Kolkwitz

Glinzig

1.

Die Grenzen der Zone 1 sind in der in Anlage 2 Nummer 1 genannten topografischen Karte mit der Blattnummer 01 sowie in den in Anlage 2 Nummer 2 genannten Liegenschaftskarten mit den Blattnummern 01 und 02 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Spree-Neiße, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

§ 3  
**Schutzzweck**

(1) Schutzzweck des Naturschutzgebietes, das geprägt ist durch die Vielzahl der Teiche, Kleingewässer und Gräben mit reicher und weitgehend intakter Naturausstattung, ist

1.  die Erhaltung, Entwicklung und Wiederherstellung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der ausgedehnten Röhrichtbestände der Teiche, der Großseggenriede, Rasenschmielen- und Kohlkratzdistelwiesen sowie der Erlenbruchgesellschaften, Laubgehölze und Gebüsche;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Zungen-Hahnenfuß (Ranunculus lingua);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Vögel, Amphibien, Reptilien und Wirbellosen, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Fischadler (Pandion haliaetus), Seeadler (Haliaeetus albicilla), Kranich (Grus grus), Schwarzmilan (Milvus migrans), Drosselrohrsänger (Acrocephalus arundinaceus), Eisvogel (Alcedo atthis), Moorente (Aythya nyroca);
4.  die Erhaltung und Entwicklung der Gewässer als Rast-, Schlaf- und Überwinterungshabitate für Schwäne, Taucher, nordische Gänse und Enten;
5.  die Erhaltung des Gebietes wegen der besonderen Eigenart der charakteristischen Wiesenlandschaft mit Gehölzstreifen und -gruppen, bewirtschafteten Teichen und Kleingewässern mit hoher Struktur- und Artenvielfalt;
6.  die Erhaltung und Entwicklung des Gebietes für den Genaustausch bedrohter Arten als wesentlicher Teil des regionalen Biotopverbundes zwischen den einzelnen Schutzgebieten in der Spreeaue und dem Biosphären-reservat Spreewald.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Glinziger Teich- und Wiesengebiet“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinem Vorkommen von

1.  Oligo- bis mesotrophen stehenden Gewässern mit Vegetation der Isoeto-Nanojuncetea, Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Flüssen der planaren bis montanen Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Feuchten Hochstaudenfluren der planaren und montanen bis alpinen Stufe als Biotope von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG);
2.  Auen-Wäldern mit Alnus glutinosa (Schwarz-Erle) als prioritärer Biotop („prioritärer Lebensraumtyp“ im Sinne des Anhangs I der Richtlinie 92/43/EWG);
3.  Fischotter (Lutra lutra) und Rotbauchunke (Bombina bombina) als Tierarten von gemeinschaftlichem Interesse (im Sinne des Anhangs II der Richtlinie 92/43/EWG), einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

§ 4  
**Verbote**

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 12 jeweils nach dem 30.
    Juni eines jeden Jahres;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der Wege, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, und außerhalb der Waldbrandwundstreifen zu reiten;
11.  mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen; ausgenommen bleibt die Zufahrt zur Verkaufsstelle am Oberteich und das Abstellen auf den dort vorhandenen Stellflächen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  zu baden oder zu tauchen;
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (wie zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien wie zum Beispiel Gärfutter zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

§ 5  
**Zulässige Handlungen**

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  auf Grünland § 4 Absatz 2 Nummer 24 gilt; bei Narbenschäden ist eine umbruchlose Nachsaat zulässig, in der Zone 1 ist die Zustimmung der unteren Naturschutzbehörde erforderlich,
    2.  in der Zone 1 Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertende Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und Sekundärrohstoffdünger (wie zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) einzusetzen; § 4 Absatz 2 Nummer 23 gilt;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  die Walderneuerung auf den Flächen des in § 3 Absatz 2 genannten Waldlebensraumtyps und in Erlenbruchwäldern durch Naturverjüngung erfolgt.
        Im Übrigen dürfen nur Arten der potenziell natürlichen Vegetation in gesellschaftstypischen Anteilen eingebracht werden, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
    2.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    3.  auf den Flächen des in § 3 Absatz 2 genannten Waldlebensraumtyps und in Erlenbruchwäldern die Nutzung der Bestände einzelstamm- bis truppweise erfolgt,
    4.  § 4 Absatz 2 Nummer 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters weitgehend ausgeschlossen ist,
    2.  § 4 Absatz 2 Nummer 19 gilt;
4.  die Teichbewirtschaftung, die den Anforderungen des § 5 Absatz 4 des Bundesnaturschutzgesetzes in Verbindung mit dem Fischereigesetz des Landes Brandenburg entspricht und die im Sinne der guten fachlichen Praxis gemäß den Leitlinien zur naturschutzgerechten Teichwirtschaft in Brandenburg vom 16.
    März 2011 auf den bisher rechtmäßig dafür genutzten Flächen durchgeführt wird.
    Fanggeräte und Fangmittel sind so einzusetzen oder auszustatten, dass eine Gefährdung des Fischotters weitgehend ausgeschlossen ist;
5.  die Genehmigung von Maßnahmen zur Vergrämung und Tötung von Kormoranen im Bereich der fischereilich genutzten Teiche durch die zuständige Naturschutzbehörde, sofern hierfür die erforderliche artenschutzrechtliche Ausnahmegenehmigung oder Befreiung vorliegt.
    Die Genehmigung kann mit Auflagen versehen werden; sie ist zu erteilen, wenn der Schutzzweck von der Maßnahme nicht wesentlich beeinträchtigt wird;
6.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass
    1.  die Angelfischerei innerhalb des Teichgebietes nur am Oberteich zulässig ist,
    2.  § 4 Absatz 2 Nummer 13 gilt, wobei der Betrieb von manuell betriebenen Angelkähnen auf dem Oberteich zulässig ist;
7.  für den Bereich der Jagd:
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Jagd auf Wasservögel an den Teichen nördlich der Landesstraße 49 erst ab dem 15.
        November eines jeden Jahres bis zum Ende der gesetzlich festgelegten Jagdzeit gestattet ist.
        Werden Teiche von Gänsen oder Kranichen als Schlafplatz genutzt, ist die Wasservogeljagd unzulässig,
        
        bb)
        
        die Fallenjagd ausschließlich mit Lebendfallen erfolgt,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde,
    3.  der Einsatz transportabler und mobiler Ansitzeinrichtungen,
    4.  die Anlage von Kirrungen außerhalb der nach § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 32 des Brandenburgischen Naturschutzgesetzes geschützten Biotope.Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern sind unzulässig; im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
8.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter die Nummer 10 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
9.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
10.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen.
    Die ordnungsgemäße Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen her-gestellt werden;
11.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
12.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch jeweils nach dem 30.
    Juni eines jeden Jahres;
13.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
14.  Schutz-, Pflege- und Entwicklungsmaßnahmen sowie Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
15.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen; darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 an Straßen und Wegen freigestellt;
16.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen; die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten; sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

§ 6  
**Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen**

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  westlich des Unterteiches soll durch geeignete Maßnahmen die Stabilisierung des Landschaftswasserhaushaltes im angrenzenden Feuchtwiesenkomplex verbessert werden;
2.  am westlichen Auslauf des Unterteiches (westlicher Rand des Binsenröhrichts) sollen unter Berücksichtigung der Vegetationsausbildung neue Kleingewässer und temporäre Blänken als Laichplätze für bestandsbedrohte Amphibienarten, wie zum Beispiel Rotbauchunke und Moorfrosch, angelegt werden;
3.  der Fischbesatz soll jährlich an mindestens einem Teich so erfolgen, dass die Bestände der Rotbauchunke gefördert werden; mit der Bewirtschaftung der Teiche sollen kontinuierlich Wasserflächen im Gebiet bereitgehalten werden, so dass nicht alle Teiche gleichzeitig trocken liegen; insbesondere zur Reproduktion der Amphibien sollen Teiche ab Anfang März bespannt werden (Unterteich);
4.  für die Teiche soll ein Bewirtschaftungsplan erstellt werden, der folgende Mindestangaben enthält: Besatz nach Arten und Altersklassen, Bespannungszeiträume, Düngung, Teichpflege- und Sanierungsmaßnahmen jeweils nach Art, Umfang und Zeitpunkt;
5.  ein bis zwei Teiche, darunter der Pappelteich, sollen mindestens im dreijährigen Turnus im Spätsommer abgelassen werden;
6.  die Grünlandflächen sollen mosaikartig ab dem 1.
    Juni eines jeden Jahres und vorrangig als Mähwiesen genutzt werden;
7.  die Ackerflächen unmittelbar nördlich der Landesstraße 49 sollen als Grünland genutzt werden;
8.  Feuchtwiesen und Hochstaudenfluren sollen durch Pflegemaßnahmen offen gehalten beziehungsweise die Gehölzsukzession zurückgedrängt werden.

§ 7  
**Befreiungen**

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

§ 8  
**Ordnungswidrigkeiten**

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

§ 9  
**Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen**

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere die §§ 31 bis 33 und § 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

§ 10  
**Geltendmachen von Rechtsmängeln**

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

§ 11  
**Inkrafttreten**

§ 5 Absatz 1 Nummer 1 Buchstabe b dieser Verordnung tritt am 1.
Juli 2013 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 20.
November 2012

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

Anlage 1  
(zu § 2 Absatz 1)

![Das Naturschutzgebiet Glinziger Teich- und Wiesengebiet hat eine Größe von rund 257 Hektar. Es liegt im Landkreis Spree-Neiße im Bereich der Gemeinde Kolkwitz und umfasst Teile der Gemarkung Kolkwitz, Glinzig und Limberg.](/br2/sixcms/media.php/69/Nr.99-1.GIF "Das Naturschutzgebiet Glinziger Teich- und Wiesengebiet hat eine Größe von rund 257 Hektar. Es liegt im Landkreis Spree-Neiße im Bereich der Gemeinde Kolkwitz und umfasst Teile der Gemarkung Kolkwitz, Glinzig und Limberg.")

Anlage 2  
(zu § 2 Absatz 2)

**1.
Topografische Karten im Maßstab 1 : 10 000**

**Titel:**

Topografische Karte zur Verordnung über das Naturschutzgebiet „Glinziger Teich- und Wiesengebiet“

**Blattnummer**

**Kartenblatt**

**Unterzeichnung**

01

4251-NW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV), am  
10.
Oktober 2012

02

4251-SW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 10.
Oktober 2012

****2.
 Liegenschaftskarten im Maßstab 1 : 2 500****

**Titel:**

Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Glinziger Teich- und Wiesengebiet“

**Blattnummer**

**Gemarkung**

**Flur**

**Unterzeichnung**

01

Glinzig  
Kolkwitz  
Limberg

1  
5  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 10.
Oktober 2012

02

Glinzig  
Kolkwitz

1  
5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 10.
Oktober 2012

03

Glinzig  
Limberg

1  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 10.
Oktober 2012

04

Glinzig

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 10.
Oktober 2012

05

Glinzig  
Kolkwitz

1  
2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 10.
Oktober 2012

 Anlage 3   
(zu § 2 Absatz 2)

**Flurstücksliste zur Verordnung über das Naturschutzgebiet „Glinziger Teich- und Wiesengebiet“**

**Landkreis: Spree-Neiße**

**Gemeinde**

**Gemarkung**

**Flur**

**Flurstücke**

Kolkwitz

Kolkwitz

2

114/4;

Kolkwitz

Kolkwitz

5

125 bis 128, 130, 135, 136, 138 bis 140, 143, 144, 147, 148/1, 149, 151, 152, 154, 155, 178 bis 191, 193, 195, 197, 199, 203 bis 205;

Kolkwitz

Glinzig

1

30, 31, 48, 50, 51, 58, 63 bis 65, 74, 75, 77, 82 anteilig, 84, 90/2 anteilig, 91, 92 anteilig, 93, 332, 333, 334 anteilig, 335, 337 bis 352, 354 bis 373, 374 anteilig, 375, 376, 428, 454;

Kolkwitz

Limberg

1

256 bis 268 anteilig, 269 bis 274, 276 bis 281, 282 anteilig, 283 anteilig, 284 bis 290, 291 anteilig, 292 bis 294, 295 anteilig, 296 bis 301, 302 anteilig, 303/2 anteilig, 304 bis 306, 307 anteilig, 308 anteilig, 309, 310, 311 anteilig, 312 bis 320, 321 anteilig, 337 anteilig, 406 anteilig, 407 anteilig, 408/1 anteilig, 778, 779, 790 anteilig, 807 anteilig.

Flächen der Zone 1:

**Gemeinde**

**Gemarkung**

**Flur**

**Flurstücke**

Kolkwitz

Kolkwitz

5

135 anteilig, 136 anteilig, 138, 139 anteilig, 140 anteilig, 143, 144, 147, 148/1 anteilig, 149 anteilig, 152, 154, 155, 178 bis 185, 187, 189, 190, 195 anteilig, 197 anteilig, 203, 204, 205 anteilig;

Kolkwitz

Limberg

1

256 bis 268 anteilig, 269 bis 274, 276 bis 280, 285, 779;

Kolkwitz

Glinzig

1

65, 74, 75 anteilig, 333 anteilig, 350, 352 anteilig, 354 anteilig, 357 anteilig, 361 anteilig, 363 anteilig, 365 anteilig, 367 anteilig, 369 anteilig, 370 bis 373.