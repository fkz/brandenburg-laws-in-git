## Gesetz zu dem Staatsvertrag zwischen dem Land Berlin und dem Land Brandenburg über die Freistellung von ehrenamtlich Engagierten in den Freiwilligen Feuerwehren und im Katastrophenschutz

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem in Potsdam am 26.
März 2019 vom Land Brandenburg unterzeichneten Staatsvertrag zwischen dem Land Berlin und dem Land Brandenburg über die Freistellung von ehrenamtlich Engagierten in den Freiwilligen Feuerwehren und im Katastrophenschutz wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

#### § 2 

Durch Artikel 1 Absatz 2 und 3 des Staatsvertrages wird das Grundrecht auf Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 3 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel 4 Satz 2 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 5.
Juni 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag](/de/vertraege-248211)

* * *

### Anlagen

1

[Staatsvertrag zwischen dem Land Berlin und dem Land Brandenburg über die Freistellung von ehrenamtlich Engagierten in den Freiwilligen Feuerwehren und im Katastrophenschutz](/br2/sixcms/media.php/68/GVBl_I_26_2019-Anlage.pdf "Staatsvertrag zwischen dem Land Berlin und dem Land Brandenburg über die Freistellung von ehrenamtlich Engagierten in den Freiwilligen Feuerwehren und im Katastrophenschutz") 170.9 KB