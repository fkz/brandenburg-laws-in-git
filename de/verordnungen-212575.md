## Verordnung zur Bestimmung der zur amtlichen Beglaubigung befugten Behörden im Land Brandenburg (Brandenburgische Beglaubigungsbestimmungsverordnung - BbgBeglBestV)

Auf Grund des § 6 des Verwaltungsverfahrensgesetzes für das Land Brandenburg vom 7.
Juli 2009 (GVBl.
I S.
262,264) verordnet die Landesregierung:

§ 1  
Befugnis zur amtlichen Beglaubigung
-----------------------------------------

(1) Zur amtlichen Beglaubigung nach den §§ 33 und 34 des Verwaltungsverfahrensgesetzes in Verbindung mit § 1 Absatz 1 des Verwaltungsverfahrensgesetzes für das Land Bandenburg sind die Behörden des Landes, der amtsfreien Gemeinden, der Ämter, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden juristischen Personen des öffentlichen Rechts befugt.

(2) Behörde im Sinne dieser Verordnung ist jede Stelle, die Aufgaben der öffentlichen Verwaltung wahrnimmt.

§ 2  
Inkrafttreten, Außerkrafttreten
-------------------------------------

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung zur Bestimmung der zur amtlichen Beglaubigung befugten Behörden vom 8.
Juli 1993 (GVBl.
II S.
334) außer Kraft.

Potsdam, den 30.
August 2011

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident  
Matthias Platzeck

Der Minister des Innern  
Dr.
Dietmar Woidke