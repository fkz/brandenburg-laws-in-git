## Gesetz zu dem Staatsvertrag zwischen dem Land Brandenburg und dem Land Mecklenburg-Vorpommern über die Bildung eines Vollzugsverbundes in der Sicherungsverwahrung

Der Landtag hat das folgende Gesetz beschlossen:

§ 1 
----

Dem am 13.
März 2014 vom Land Brandenburg unterzeichneten Staatsvertrag zwischen dem Land Brandenburg und dem Land Mecklenburg-Vorpommern über die Bildung eines Vollzugsverbundes in der Sicherungsverwahrung wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

§ 2 
----

Über den bevorstehenden Beitritt eines weiteren Landes zum Staatsvertrag unterrichtet die Landesregierung den Landtag, bevor sie nach Artikel 8 Absatz 3 des Staatsvertrages die oberste Landesjustizbehörde des Landes Mecklenburg-Vorpommern ermächtigt, die Zustimmung zum Beitritt zu erklären.

§ 3 
----

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel 9 Satz 2 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu machen.

Potsdam, den 10.
Juli 2014

Der Präsident des Landtages Brandenburg

Gunter Fritsch

[zum Staatsvertrag](/de/vertraege-237590)