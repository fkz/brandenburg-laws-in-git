## Zweite Verordnung über die Aufhebung von Wasserschutzgebieten

Auf Grund des § 15 Absatz 3 Satz 4 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr.
20) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1  
Aufhebung von Wasserschutzgebieten
----------------------------------------

(1) Folgende, auf der Grundlage des § 28 des Landeskulturgesetzes vom 14.
Mai 1970 (GBl.
I Nr. 12 S. 67), des § 28 des Wassergesetzes vom 17.
April 1963 (GBl.
I Nr. 5 S. 77) und der Verordnung über die Festlegung von Schutzgebieten für die Wasserentnahme aus dem Grund- und Oberflächenwasser zur Trinkwassergewinnung vom 11.
Juli 1974 (GBl.
I Nr. 37 S. 349) festgesetzte und nach § 46 des Wassergesetzes vom 2.
Juli 1982 (GBl.
I Nr. 26 S. 467) aufrechterhaltene Wasserschutzgebiete werden aufgehoben:

1.  die mit Beschluss Nr. 61-10/71 vom 17.
    November 1971 des Kreistages Kyritz festgesetzten Wasserschutzgebiete Dreetz/See und Schönermark,
2.  das mit Beschluss Nr. 51-15/72 vom 6.
    Juli 1972 des Kreistages Strasburg festgesetzte Wasserschutzgebiet Trebenow,
3.  die mit Beschluss Nr. 60-16/73 vom 19.
     April 1973 des Kreistages Oranienburg festgesetzten Wasserschutzgebiete Oranienburg-Heidelberger Straße, Oranienburg-Leninallee, Oranienburg-Germendorfer Allee und Neuholland,
4.  das mit Beschluss Nr. 5-20/74 vom 17.
     Januar 1974 des Kreistages Königs Wusterhausen festgesetzte Wasserschutzgebiet Töpchin,
5.  die mit Beschluss Nr.
    0017 vom 19.
    November 1974 des Kreistages Jüterbog festgesetzten Wasserschutzgebiete Treuenbrietzen OT Frohnsdorf und Treuenbrietzen OT Lüdendorf,
6.  das mit Beschluss Nr. 20/11/74 vom 21.
     November 1974 des Kreistages Gransee festgesetzte und mit Beschluss Nr. 134/9/87 vom 18.
     September 1987 des Kreistages Gransee für den Brunnen 6 erweiterte Wasserschutzgebiet Gransee,
7.  das mit Beschluss Nr. 244/78 vom 15.
    Juni 1978 des Kreistages Neuruppin für das Wasserwerk des VE Schlacht- und Verarbeitungsbetriebes Neuruppin im OT Treskow festgesetzte Wasserschutzgebiet,
8.  das mit Beschluss Nr. 125-26/78 vom 27.
     November 1978 des Kreistages Perleberg festgesetzte Wasserschutzgebiet Kleeste,
9.  die mit Beschluss Nr.
    0061 vom 18.
    Juni 1980 des Kreistages Kyritz festgesetzten Wasserschutzgebiete Döllen, Granzow und Kunow,
10.  das mit Beschluss Nr. 60-11/81 vom 26.
    März 1981 des Kreistages Perleberg festgesetzte Wasserschutzgebiet Karstädt,
11.  das mit Beschluss Nr. 84-31/81 vom 6.
    Mai 1981 des Kreistages Potsdam festgesetzte Wasserschutzgebiet Wildenbruch-Six,
12.  das mit Beschluss Nr. 77-15/81 vom 26.
     November 1981 des Kreistages Perleberg festgesetzte Wasserschutzgebiet Muggerkuhl,
13.  die mit Beschluss Nr. 13/56/81 vom 21.
    Mai 1981 des Kreistages Pasewalk für die Wasserfassungen Grimme und Menkin festgesetzten Wasserschutzgebiete,
14.  das mit Beschluss Nr. 87-14/1981 vom 1.
     Juli 1981 des Kreistages Eberswalde für das Wasserwerk II Eberswalde-Finow festgesetzte Wasserschutzgebiet,
15.  das mit Beschluss Nr.
    0068 vom 23.
     September 1981 des Kreistages Belzig festgesetzte Wasserschutzgebiet Hagelberg/Grützdorf,
16.  die mit Beschluss Nr. 70-17/81 vom 18.
     Dezember 1981 des Kreistages Prenzlau festgesetzten Wasserschutzgebiete Eickstedt, Grünow, Sternhagen und Ziemkendorf,
17.  das mit Beschluss Nr. 101/12/81 vom 22.
     Dezember 1981 des Kreistages Gransee festgesetzte Wasserschutzgebiet Krewelin,
18.  die mit Beschluss Nr. 145 vom September 1983 der Stadtverordnetenversammlung Frankfurt (Oder) für die Wasserfassungsanlagen der Strandbereiche Ost und Mitte der Trinkwasserversorgungsanlage des Naherholungsgebietes Helene-See festgesetzten Wasserschutzgebiete.

(2) Folgende, auf der Grundlage des § 29 des Wassergesetzes vom 2.
Juli 1982 und der Dritten Durchführungsverordnung zum Wassergesetz – Schutzgebiete und Vorbehaltsgebiete – vom 2.
Juli 1982 (GBl.
I Nr. 26 S. 487) festgesetzte Wasserschutzgebiete werden aufgehoben:

1.  die mit Beschluss Nr.
    0017-19.82 vom 14.
     Juli 1982 des Kreistages Pritzwalk festgesetzten Wasserschutzgebiete Blumenthal und Grabow bei Blumenthal,
2.  das mit Beschluss Nr.
    0063/85 vom 27.
     Februar 1985 des Kreistages Neuruppin für das Wasserwerk der 8 kt ALV-Anlage für Speisekartoffeln der LPG Kränzlin festgesetzte Wasserschutzgebiet,
3.  die mit Beschluss Nr.
    09/36/85 vom 21.
     August 1985 des Kreistages Beeskow festgesetzten Wasserschutzgebiete Kehrigk und Limsdorf,
4.  das mit Beschluss Nr.
    0019/89 vom 20.
     Dezember 1989 des Kreistages Rathenow festgesetzte Wasserschutzgebiet Liepe.

§ 2  
Inkrafttreten
-------------------

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 26.
April 2012

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack