## Zustimmung zu dem Fünften Staatsvertrag über die Änderung des Landesplanungsvertrages

Dem in Berlin und Potsdam am 16.
Februar 2011 unterzeichneten Fünften Staatsvertrag über die Änderung des Landesplanungsvertrages wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

[zum Landesplanungsvertrag](/de/vertraege-214214)