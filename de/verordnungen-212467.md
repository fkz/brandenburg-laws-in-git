## Verordnung über die Übertragung des Rechts zur Berufung der Hochschullehrerinnen und Hochschullehrer an der Fachhochschule Potsdam

Auf Grund des § 38 Absatz 5 Satz 2 des Brandenburgischen Hochschulgesetzes vom 18.
Dezember 2008 (GVBl. I S. 318) verordnet die Ministerin für Wissenschaft, Forschung und Kultur:

#### § 1  
Übertragung des Berufungsrechts

Der Fachhochschule Potsdam wird das Recht zur Berufung der Hochschullehrerinnen und Hochschullehrer über­tragen.

#### § 2  
Übergangsvorschrift

(1) Für laufende Berufungsverfahren wird das Berufungsrecht übertragen, soweit vor Inkrafttreten dieser Verordnung bei dem für die Hochschulen zuständigen Mitglied der Landesregierung kein Berufungsvorschlag eingereicht worden ist.

(2) Soweit ein Berufungsvorschlag vor Inkrafttreten dieser Verordnung bei dem für die Hochschulen zuständigen Mitglied der Landesregierung eingereicht worden ist, kann das für die Hochschulen zuständige Mitglied der Landesregierung das Berufungsrecht im Einzelfall übertragen.

#### § 3  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 12.
Mai 2010

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Dr.
Martina Münch