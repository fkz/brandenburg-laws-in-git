## Verordnung zur Bestimmung des Amtes für Statistik Berlin-Brandenburg als Vollstreckungsbehörde in Verfahren nach dem Zensusgesetz 2011 und dem Gesetz zur Ausführung des Zensusgesetzes 2011 im Land Brandenburg  (Brandenburgische Vollstreckungsbehörden-Bestimmungsverordnung - BbgVbBestV)

Auf Grund des § 2 Absatz 4 Satz 2 zweite Alternative des Verwaltungsvollstreckungsgesetzes für das Land Brandenburg vom 18.
Dezember 1991 (GVBl.
S.
661) verordnet der Minister des Innern:

#### § 1  
Vollstreckungsbehörde

Die Aufgabe der Beitreibung von Zwangs- und Bußgeldern wird vom Amt für Statistik Berlin-Brandenburg wahrgenommen, soweit es diese festsetzt.

#### § 2  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft und am 31.
Dezember 2015 außer Kraft.

Potsdam, den 23.
Mai 2011

Der Minister des Innern

Dr.
Dietmar Woidke