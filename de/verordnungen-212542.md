## Verordnung zur Bestimmung der Städte Bernau bei Berlin, Falkensee und Oranienburg zu Großen kreisangehörigen Städten  (BestGkSV)

Auf Grund des § 1 Absatz 3 der Kommunalverfassung des Landes Brandenburg vom 18.
Dezember 2007 (GVBl.
I S. 286) und der §§ 9 Absatz 2 und 16 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186) in Verbindung mit § 2 der Verordnung zur Ausführung des Reichs- und Staatsangehörigkeitsgesetzes vom 28.
November 1991 (GVBl.
 S. 524) verordnet der Minister des Innern:

#### § 1  
Statusregelungen

Die im Folgenden genannten kreisangehörigen Städte erhalten den Status einer Großen kreisangehörigen Stadt:

1.  die Stadt Bernau bei Berlin (Landkreis Barnim),
    
2.  die Stadt Falkensee (Landkreis Havelland),
    
3.  die Stadt Oranienburg (Landkreis Oberhavel).
    

#### § 2  
Übergangsvorschrift

Unbeschadet der Bestimmung in § 1 sind für die in § 1 Satz 1 der Verordnung über die Zuständigkeit in Staatsangehörigkeitssachen vom 12.
März 1992 (GVBl.
II S. 82), die zuletzt durch Verordnung vom 23.
November 2004 (GVBl.
II S. 890) geändert worden ist, genannten Aufgaben als Große kreisangehörige Stadt nur die Städte zuständig, die diesen Status am 31.
Dezember 2004 hatten.
§ 1 Absatz 4 der Kommunalverfassung des Landes Brandenburg bleibt unberührt.

#### § 3  
Inkrafttreten

Diese Verordnung tritt am 1.
 Januar 2011 in Kraft.

Potsdam, den 13.
Dezember 2010

Der Minister des Innern

Dr.
Dietmar Woidke