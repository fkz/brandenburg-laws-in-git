## Gesetz zu dem Ersten Staatsvertrag zur Änderung des Staatsvertrages über die gemeinsame Berufsvertretung der Psychologischen Psychotherapeuten und der Kinder- und Jugendlichenpsychotherapeuten

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem am 17.
März 2021 vom Land Brandenburg unterzeichneten Ersten Staatsvertrag zur Änderung des Staatsvertrages über die gemeinsame Berufsvertretung der Psychologischen Psychotherapeuten und der Kinder- und Jugendlichenpsychotherapeuten wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

#### § 2 

Durch Artikel 1 Absatz 3 des Staatsvertrages über die Gemeinsame Berufsvertretung der Psychotherapeutinnen und Psychotherapeuten wird das Grundrecht auf Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 3 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem Artikel 2 Absatz 1 Satz 1 am 1. Juli 2021 in Kraft.
Sollte der Staatsvertrag nach seinem Artikel 2 Absatz 1 Satz 2 gegenstandslos werden, ist dies im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 8.
Juni 2021

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke

* * *

[zum Staatsvertrag](/de/vertraege-237641)

* * *

### Anlagen

1

[Erster Staatsvertrag zur Änderung des Staatsvertrages über die gemeinsame Berufsvertretung der Psychologischen Psychotherapeuten und der Kinder- und Jugendlichenpsychotherapeuten](/br2/sixcms/media.php/68/GVBl_I_15_2021-Anlage.pdf "Erster Staatsvertrag zur Änderung des Staatsvertrages über die gemeinsame Berufsvertretung der Psychologischen Psychotherapeuten und der Kinder- und Jugendlichenpsychotherapeuten") 225.5 KB