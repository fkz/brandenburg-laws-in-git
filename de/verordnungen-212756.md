## Verordnung über Zuständigkeiten nach dem Holzhandels-Sicherungs-Gesetz (HolzSiGZV)

Auf Grund des § 9 Absatz 2 des Landesorganisationsgesetzes vom 24. Mai 2004 (GVBl. I S. 186) in Verbindung mit § 14 Absatz 1 Satz 2 des Landesorganisationsgesetzes und des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19. Februar 1987 (BGBl. I S. 602) verordnet die Landes-regierung:

§ 1  
**Zuständigkeiten**

(1) Die untere Forstbehörde ist die nach Landesrecht zuständige Behörde im Sinne des § 1 Absatz 2 Satz 2 des Holzhandels-Sicherungs-Gesetzes vom 11. Juli 2011 (BGBl. I S. 1345), das durch das Gesetz vom 3. Mai 2013 (BGBl. I S. 1104) geändert worden ist, in der jeweils geltenden Fassung.

(2) Die untere Forstbehörde ist im Rahmen der Zuständigkeit nach Absatz 1 für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 7 des Holzhandels-Sicherungs-Gesetzes zuständig.

§ 2  
**Inkrafttreten**

Diese Verordnung tritt am Tag nach der Verkündungin Kraft.

Potsdam, den 2.
Juli 2013

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Matthias Platzeck