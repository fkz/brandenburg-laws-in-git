## Brandenburgisches Besoldungs- und Versorgungsanpassungsgesetz 2011/2012 (BbgBVAnpG 2011/2012)

Der Landtag hat das folgende Gesetz beschlossen:

### § 1  
Geltungsbereich

(1) Dieses Gesetz gilt für die

1.  Beamtinnen und Beamten des Landes, der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts,
2.  Richterinnen und Richter des Landes,
3.  Versorgungsempfängerinnen und Versorgungsempfänger, denen laufende Versorgungsbezüge zustehen, die das Land, eine Gemeinde, ein Gemeindeverband oder eine der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten oder Stiftungen des öffentlichen Rechts zu tragen hat.

(2) Dieses Gesetz gilt nicht für Ehrenbeamtinnen und Ehrenbeamte sowie für ehrenamtliche Richterinnen und Richter.

(3) Dieses Gesetz gilt nicht für öffentlich-rechtliche Religionsgesellschaften und ihre Verbände.

### § 2  
Anpassung der Besoldung im Jahr 2011

(1) Die nachfolgenden Dienstbezüge und sonstigen Bezüge werden ab 1.
April 2011 um 1,5 Prozent erhöht:

1.  die Grundgehaltssätze,
2.  der Familienzuschlag mit Ausnahme der Erhöhungsbeträge für die Besoldungsgruppen A 2 bis A 5,
3.  die Amtszulagen, auch soweit sie landesrechtlich geregelt sind, sowie die allgemeine Stellenzulage nach Vorbemerkung Nummer 27 der Bundesbesoldungsordnungen A und B,
4.  der Auslandszuschlag und der Auslandskinderzuschlag,
5.  die Anwärtergrundbeträge.

(2) Absatz 1 gilt entsprechend für

1.  die Leistungsbezüge nach § 33 Absatz 1 des Bundesbesoldungsgesetzes in der am 31.
    August 2006 geltenden Fassung, soweit sie gemäß § 2a Absatz 6 des Brandenburgischen Besoldungsgesetzes, § 2 Absatz 3 der Hochschulleistungsbezügeverordnung vom 23.
    März 2005 (GVBl.
    II S. 152) oder § 3 Absatz 2 der Leistungsbezügeverordnung FHPol vom 3.
    August 2005 (GVBl.
    II S. 454), die durch Artikel 4 des Gesetzes vom 24.
     Oktober 2007 (GVBl.
    I S. 134, 140) geändert worden ist, an den regelmäßigen Besoldungsanpassungen teilnehmen,
2.  den Betrag nach § 4 Absatz 1 Nummer 1 der Erschwerniszulagenverordnung in der am 31.
    August 2006 geltenden Fassung,
3.  die Beträge nach § 4 Absatz 1 und 3 der Verordnung über die Gewährung von Mehrarbeitsvergütung für Beamte in der am 31.
    August 2006 geltenden Fassung,
4.  die in § 2 Absatz 1 Nummer 6 bis 10 des Brandenburgischen Besoldungs- und Versorgungsanpassungsgesetzes 2008 genannten Bezüge.

### § 3  
Anpassung der Besoldung im Jahr 2012

(1) Ab 1.
Januar 2012 werden die in § 2 Absatz 1 und 2 genannten Bezüge um 1,9 Prozent erhöht.

(2) Ab 1.
Januar 2012 werden außerdem die Grundgehaltssätze um jeweils 17 Euro und die Anwärtergrundbeträge um jeweils 6 Euro erhöht.

### § 4  
Rundungsregelung

Bei der Berechnung der nach den §§ 2 und 3 erhöhten Bezüge sind die sich ergebenden Bruchteile eines Cents hinsichtlich der Beträge des Familienzuschlags der Stufe 1 auf den nächsten durch zwei teilbaren Centbetrag aufzurunden.
Im Übrigen sind Bruchteile eines Cents unter 0,5 abzurunden und Bruchteile von 0,5 und mehr aufzurunden.

### § 5  
Anpassung der Versorgungsbezüge

(1) Bei Versorgungsempfängerinnen und Versorgungsempfängern gelten die Erhöhungen nach den §§ 2 und 3 entsprechend für die in Artikel 2 § 2 Absatz 1 bis 5 des Bundesbesoldungs- und -versorgungsanpassungsgeset-zes 1995 vom 18.
Dezember 1995 (BGBl.
I S. 1942) genannten Bezügebestandteile sowie für die in § 14 Absatz 2 Satz 1 Nummer 3 und § 84 Absatz 1 Nummer 4, 5 und 7 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung aufgeführten Stellenzulagen und Bezüge.

(2) Versorgungsbezüge, die in festen Beträgen festgesetzt sind, und der Betrag nach Artikel 13 § 2 Absatz 4 des Fünften Gesetzes zur Änderung besoldungsrechtlicher Vorschriften vom 28.
Mai 1990 (BGBl.
I S. 967, 976) werden ab 1.
April 2011 um 1,4 Prozent und ab 1.
Januar 2012 um 1,8 Prozent erhöht.

(3) Bei Versorgungsempfängerinnen und Versorgungsempfängern, deren Versorgungsbezügen ein Grundgehalt der Besoldungsgruppe A 1 bis A 8 zugrunde liegt, vermindert sich das Grundgehalt ab 1.
April 2011 um 51,24 Euro und ab 1.
Januar 2012 um 52,21 Euro, wenn ihren ruhegehaltfähigen Dienstbezügen die Stellenzulage nach Vorbemerkung Nummer 27 Absatz 1 Buchstabe a oder b der Bundesbesoldungsordnungen A und B bei Eintritt in den Ruhestand nicht zugrunde gelegen hat.

### § 6  
Einmalzahlung im Jahr 2011

(1) Beamtinnen und Beamte, Richterinnen und Richter, die mindestens für einen Tag des Monats April 2011 Anspruch auf Dienstbezüge aus einem Beamten- oder Richterverhältnis hatten, erhalten für diesen Monat eine Einmalzahlung in Höhe von 360 Euro.
§ 6 Absatz 1 und § 72a Absatz 1 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung gelten entsprechend; maßgebend sind die Verhältnisse am 1.
April 2011.
Wenn an diesem Tag kein Anspruch auf Dienstbezüge bestanden hat, ist maßgebend der erste Tag mit Anspruch auf Dienstbezüge im Monat April 2011.

(2) Beamtinnen und Beamte auf Widerruf, die mindestens an einem Tag des Monats April 2011 Anspruch auf Anwärterbezüge hatten, erhalten für diesen Monat eine Einmalzahlung in Höhe von 120 Euro.

(3) Am 1.
April 2011 vorhandene Empfängerinnen und Empfänger von laufenden Versorgungsbezügen erhalten eine Einmalzahlung, die sich nach dem jeweils maßgebenden Ruhegehaltssatz und den Anteilssätzen des Witwen- und Waisengeldes sowie des Unterhaltsbeitrages aus dem Betrag von 360 Euro ergibt.
Zu den laufenden Versorgungsbezügen rechnet nicht der Unfallausgleich nach § 35 des Beamtenversorgungsgesetzes in der am 31.
August 2006 geltenden Fassung.
§ 49 Absatz 8 des Beamtenversorgungsgesetzes in der am 31.
August 2006 geltenden Fassung gilt entsprechend.
Satz 1 gilt nicht für Personen, denen für den Monat April 2011 ein Unterhaltsbeitrag durch Gnadenerweis oder Disziplinarentscheidung zusteht oder die Übergangsgeld nach den §§ 47 und 47a des Beamtenversorgungsgesetzes in der am 31.
August 2006 geltenden Fassung erhalten.

### § 7  
Gewährung der Einmalzahlung

(1) Die Einmalzahlung nach § 6 Absatz 1 und 2 wird den Berechtigten nur einmal gewährt.
Bei mehreren Dienstverhältnissen gilt § 5 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung entsprechend.

(2) Im Jahr 2011 gewährte vergleichbare Einmalzahlungen aus einem anderen Rechts- oder Arbeitsverhältnis im öffentlichen Dienst (§ 40 Absatz 6 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung) werden auf die Einmalzahlung angerechnet.
Dem öffentlichen Dienst im Sinne des Satzes 1 steht der Dienst bei öffentlich-rechtlichen Religionsgesellschaften und ihren Verbänden gleich.

(3) Die Einmalzahlung bleibt bei sonstigen Besoldungs- und Versorgungsleistungen unberücksichtigt.

(4) Bei der Anwendung von Ruhens- und Anrechnungsvorschriften ist die Einmalzahlung oder eine entsprechende Leistung, die eine versorgungsberechtigte Person aus einer Erwerbstätigkeit oder zu weiteren Versorgungsbezügen erhält, in dem jeweiligen Auszahlungsmonat zu berücksichtigen.
Die bei der Anwendung der Ruhensvorschriften nach den in § 2 des Zweiten Beamtenversorgungsergänzungsgesetzes vom 19.
Dezember 2008 (GVBl.
I S. 363, 364), das zuletzt durch Artikel 8 des Gesetzes vom 3.
April 2009 (GVBl.
I S. 26, 58) geändert worden ist, und § 54 des Beamtenversorgungsgesetzes in der am 31.
 August 2006 geltenden Fassung bestimmten Höchstgrenzen erhöhen sich jeweils um den nach der maßgebenden Höchstgrenze berechneten Betrag der Einmalzahlung.

### § 8  
Bekanntmachung

Das Ministerium der Finanzen macht die Beträge der nach den §§ 2 und 3 erhöhten Bezüge im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I durch Neubekanntmachung der Anlagen 1 bis 16 des Brandenburgischen Besoldungs- und Versorgungsanpassungsgesetzes 2008 und der Anlage 2 des Brandenburgischen Besoldungsgesetzes bekannt.

### § 9   
Inkrafttreten

Dieses Gesetz tritt mit Wirkung vom 1.
April 2011 in Kraft.

Potsdam, den 18.
Oktober 2011

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch