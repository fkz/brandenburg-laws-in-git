## Gesetz zu dem Staatsvertrag über die Hochschulzulassung

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem in Berlin am 21.
März 2019 vom Land Brandenburg unterzeichneten Staatsvertrag über die Hochschulzulassung wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

#### § 2 

Die Rechtsverordnungen nach Artikel 12 und Artikel 18 des Staatsvertrages erlässt das für Hochschulen zuständige Mitglied der Landesregierung.

#### § 3 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel 19 Absatz 1 Satz 1 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 5.
Juni 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag](/de/vertraege-248241)

* * *

### Anlagen

1

[Staatsvertrag über die Hochschulzulassung](/br2/sixcms/media.php/68/GVBl_I_25_2019-Anlage.pdf "Staatsvertrag über die Hochschulzulassung") 434.3 KB