## Staatsvertrag zwischen dem Land Brandenburg und dem Land Mecklenburg-Vorpommern über die Bildung eines Vollzugsverbundes in der Sicherungsverwahrung

Das Land Brandenburg,  
vertreten durch den Ministerpräsidenten,  
dieser vertreten durch den Minister der Justiz

und

das Land Mecklenburg-Vorpommern,  
vertreten durch den Ministerpräsidenten,  
dieser vertreten durch die Justizministerin,

schließen vorbehaltlich der Zustimmung ihrer verfassungsmäßig berufenen Organe folgenden Staatsvertrag:

### Artikel 1   
Zweck und Grundlage des Vertrages

(1) Dieser Vertrag regelt die länderübergreifende Unterbringung von Sicherungsverwahrten in einem Vollzugsverbund mit dem Ziel, durch Schwerpunktsetzung differenzierte Behandlungsmöglichkeiten zu schaffen.

(2) Der Vollzug der Sicherungsverwahrung richtet sich nach den bundes- und landesrechtlichen Regelungen zur Ausgestaltung der Unterbringung in der Sicherungsverwahrung im Sinne eines einerseits freiheitsorientierten und therapiegerichteten, andererseits auf die Sicherheit der Bevölkerung ausgerichteten Gesamtkonzepts.

### Artikel 2   
Organisatorischer Rahmen

(1) Jedes Land hält in geeigneten Einrichtungen so viele Unterbringungsplätze vor, wie nach der im eigenen Land zu erwartenden Anzahl Sicherungsverwahrter benötigt werden.
Die Länder sind sich einig, im Rahmen dieser Kapazitäten Sicherungsverwahrte des jeweils anderen Landes in eigene Einrichtungen zu übernehmen, soweit diese nicht für die Unterbringung eigener Sicherungsverwahrter benötigt werden.

(2) Die Einweisung oder Verlegung in Einrichtungen der beteiligten Länder richtet sich nach den gesetzlichen Bestimmungen dieser Länder über den Vollzug der Sicherungsverwahrung in Verbindung mit §§ 26, 53 der Strafvollstreckungsordnung.
Die danach erforderliche Einigung der zu beteiligenden Landesbehörden gilt als erfolgt, wenn der nach Artikel 3 vorgeschriebene Verfahrensweg beschritten ist.

(3) Der Vollzug der Sicherungsverwahrung richtet sich nach den gesetzlichen Bestimmungen des Landes, in dem der Sicherungsverwahrte untergebracht ist.

(4) Die Landesjustizverwaltungen unterrichten einander fortlaufend über die Anzahl der zur Unterbringung anstehenden Sicherungsverwahrten und über die jeweils vorhandenen Behandlungs- und Betreuungsprogramme, die Beratungs- und Beschäftigungsangebote und deren Aufnahmefähigkeit.

### Artikel 3   
Verteilungsverfahren

(1) Die Länder richten eine Fachkommission ein.
Je Land gehören ihr eine Vertreterin oder ein Vertreter der obersten Landesjustizbehörde und eine Vertreterin oder ein Vertreter der Leitung der für den Vollzug der Sicherungsverwahrung zuständigen Einrichtung an.
Mindestens ein Mitglied aus jedem Land muss Psychologin oder Psychologe sein.

(2) Spätestens ein Jahr vor dem Ende der Strafvollstreckung unterrichtet die abgebende Justizvollzugsanstalt die Fachkommission unter Vorlage der Vollzugsunterlagen über die bevorstehende Unterbringung eines Strafgefangenen in der Sicherungsverwahrung.
Wird die Verlegung eines Sicherungsverwahrten angestrebt, hat die Unterrichtung der Fachkommission durch die abgebende Einrichtung so frühzeitig wie möglich zu erfolgen.
Die Fachkommission gibt mit der Mehrheit ihrer Mitglieder eine Empfehlung zur Einweisung oder Verlegung in eine geeignete Einrichtung des Vollzugsverbundes ab.
Auf eine gleichmäßige Auslastung der Einrichtungen im Verbund ist hinzuwirken.

(3) Das abgebende Land veranlasst unter Beachtung der Empfehlung der Fachkommission die Einweisung oder Verlegung.

### Artikel 4   
Kosten

(1) Das in dem Verfahren nach Artikel 3 abgebende Land trägt die Kosten für die Inanspruchnahme von Unterbringungsplätzen.
Die Abrechnung erfolgt nach Tagessätzen.
Außergewöhnliche, einem Sicherungsverwahrten direkt zurechenbare Kosten, etwa für die Unterbringung in einem externen Krankenhaus oder besonders aufwändige Therapien, werden zusätzlich gesondert abgerechnet.

(2) Die Höhe des Tagessatzes der jeweiligen Einrichtung bemisst sich nach einem zwischen den Ländern abgestimmten Berechnungsschema.
Die Länder informieren einander über die Berechnungsgrundlagen und geben jeweils bis zum 1.
April eines Jahres auf der Grundlage der Vorjahreskosten die Höhe des Tagessatzes bekannt, die dann für das gesamte laufende Kalenderjahr gilt.

### Artikel 5   
Entlassung

Die Vorbereitung der Entlassung erfolgt in dem Land, in dem der Sicherungsverwahrte voraussichtlich entlassen wird.
Dies ist grundsätzlich das Land, welches für die Vollstreckung der vorangegangenen Freiheitsstrafe zuständig war.
Auf die Verlegung zum Zwecke der Entlassungsvorbereitung findet Artikel 3 Absatz 2 Satz 2 bis 4 und Absatz 3 entsprechende Anwendung.

### Artikel 6   
Verwaltungsvereinbarung

Die obersten Landesjustizbehörden bestimmen durch Verwaltungsvereinbarung

1.  die nach Artikel 1 Absatz 1 in den Einrichtungen zu bildenden Behandlungsschwerpunkte,
2.  das Nähere zum Verteilungsverfahren nach Artikel 3,
3.  das Nähere zu den Kosten und zum Abrechnungsverfahren nach Artikel 4.

### Artikel 7  
Vertragsdauer

(1) Dieser Vertrag wird auf unbestimmte Zeit geschlossen.

(2) Nach Ablauf von drei Jahren kann jedes Land den Vertrag zum 31.
Dezember eines jeden Jahres mit einer Frist von 24 Monaten kündigen.

### Artikel 8  
Beitritt weiterer Länder

(1) Andere Länder können diesem Vertrag beitreten.
Der Beitritt bedarf der Zustimmung der am Vertrag beteiligten Länder.

(2) Die Beitrittserklärung ist schriftlich gegenüber der obersten Landesjustizbehörde des Landes Mecklenburg-Vorpommern abzugeben, erforderlichenfalls mit Zustimmung der gesetzgebenden Körperschaft des beitretenden Landes.
Über den Eingang der Beitrittserklärung unterrichtet die oberste Landesjustizbehörde des Landes Mecklenburg-Vorpommern die übrigen am Vertrag beteiligten Länder.

(3) Die Zustimmung nach Absatz 1 Satz 2 erklärt die oberste Landesjustizbehörde des Landes Mecklenburg-Vorpommern im eigenen Namen und im Namen der übrigen am Vertrag beteiligten Länder in schriftlicher Form gegenüber der obersten Landesjustizbehörde des beitretenden Landes, sobald die obersten Landesjustizbehörden der übrigen am Vertrag beteiligten Länder, erforderlichenfalls nach Zustimmung ihrer gesetzgebenden Körperschaften, sie hierzu ermächtigt haben.

(4) Die Regelungen dieses Vertrages gelten für das beitretende Land ab dem Ersten des Monats, der auf die Erteilung der Zustimmung nach Absatz 3 folgt.

### Artikel 9  
Inkrafttreten

Der Vertrag bedarf der Ratifikation.
Er tritt am ersten Tag des zweiten Monats nach dem Austausch der Ratifikationsurkunden in Kraft.

Potsdam, den 13.
März 2014

Für das Land Brandenburg

Für den Ministerpräsidenten

Der Minister der Justiz

Dr.
Helmuth Markov

Schwerin, den 13.
März 2014

Für das Land Mecklenburg-Vorpommern

Für den Ministerpräsidenten

Die Justizministerin

Uta-Maria Kuder

[zum Gesetz](/de/gesetze-212960)