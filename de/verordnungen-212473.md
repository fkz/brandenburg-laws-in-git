## Verordnung zur Regelung der Zuständigkeiten nach der Energieeinsparverordnung im Land Brandenburg (Brandenburgische EnEV-Zuständigkeitsverordnung -  BbgEnEVZV)

Auf Grund des § 7 Absatz 2 des Energieeinsparungsgesetzes in der Fassung der Bekanntmachung vom 1. September 2005 (BGBl. I S. 2684), der durch Artikel 1 des Gesetzes vom 28. März 2009 (BGBl.
I S. 643) geändert worden ist, und auf Grund des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19. Februar 1987 (BGBl. I S. 602) in Verbindung mit § 1 der Verordnung über die Übertragung von Ermächtigungen zur Durchführung der Energieeinsparverordnung vom 31. Januar 2008 (GVBl. II S. 10) verordnet der Minister für Infrastruktur und Landwirtschaft:

#### § 1   
Zuständigkeit

Die unteren Bauaufsichtsbehörden sind zuständig für die Durchführung der Energieeinsparverordnung vom 24. Juli 2007 (BGBl. I S. 1519), die durch die Verordnung vom 29. April 2009 (BGBl.
I S. 954) geändert worden ist, in der jeweils geltenden Fassung, soweit in dieser Verordnung nichts anderes bestimmt ist.
Sie sind zuständige Verwaltungsbehörde für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 27 der Energieeinsparverordnung.

#### § 2   
Ausstellungsberechtigung für Energieausweise

(1) Zur Ausstellung von Energieausweisen nach § 16 Absatz 1 der Energieeinsparverordnung sind berechtigt:

1.  Bauvorlageberechtigte nach § 48 Absatz 4 der Brandenburgischen Bauordnung,
2.  Ausstellungsberechtigte nach § 21 Absatz 1 Satz 1 Nummer 1 der Energieeinsparverordnung und
    
3.  Prüfsachverständige für energetische Gebäudeplanung nach § 1 Absatz 3 der Brandenburgischen Prüfsachverständigenverordnung.
    

(2) Absatz 1 gilt auch für Personen, die eine vergleichbare Berechtigung auf der Grundlage gleichwertiger Ausbildungen und Berufserfahrungen in einem anderen Mitgliedstaat der Europäischen Union, einem anderen Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum oder der Schweiz nachweisen können.

#### § 3  
Vordrucke

Für Unternehmererklärungen nach § 26a der Energieeinsparverordnung sind die von der obersten Bauaufsichtsbehörde unter www.mil.brandenburg.de veröffentlichten Vordrucke zu verwenden.

#### § 4   
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 21.
Juni 2010

Der Minister für Infrastruktur und Landwirtschaft

Jörg Vogelsänger