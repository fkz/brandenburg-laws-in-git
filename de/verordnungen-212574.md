## Verordnung zur Übertragung der Zuständigkeiten und der Ermächtigung zum Erlass von Rechtsverordnungen nach dem Öko-Landbaugesetz (Öko-Landbau-ZuständigkeitsV)

Auf Grund des § 2 Absatz 1 des Öko-Landbaugesetzes vom 7.
Dezember 2008 (BGBl.
 I S.
2358) in Verbindung mit § 9 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S.
186) und auf Grund des § 2 Absatz 3 Satz 2 des Öko-Landbaugesetzes verordnet die Landesregierung:

§ 1 
----

Zuständige Behörde im Sinne des § 2 Absatz 1 des Öko-Landbaugesetzes ist das für Landwirtschaft zuständige Ministerium.

§ 2 
----

Die der Landesregierung durch § 2 Absatz 3 Satz 1 des Öko-Landbaugesetzes erteilte Ermächtigung zum Erlass von Rechtsverordnungen wird auf das für Landwirtschaft zuständige Mitglied der Landesregierung übertragen.

§ 3 
----

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über die Zuständigkeiten für die Kennzeichnung und Kontrolle des ökologischen Landbaus vom 27.
August 1992 (GVBl.
II S.
554) außer Kraft.

Potsdam, den 30.
August 2011

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Matthias Platzeck

Der Minister für Infrastruktur und Landwirtschaft

Jörg Vogelsänger