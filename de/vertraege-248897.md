## Vertrag zwischen dem Landtag Nordrhein-Westfalen, dem Landtag Brandenburg und dem Landtag von Baden-Württemberg über das Versorgungswerk der Mitglieder der Landtage von Nordrhein-Westfalen, Brandenburg und Baden-Württemberg

Der Landtag Nordrhein-Westfalen, vertreten durch den Präsidenten des Landtags, der Landtag Brandenburg, vertreten durch die Präsidentin des Landtags, und der Landtag von Baden-Württemberg, vertreten durch die Präsidentin des Landtags, schließen nachstehenden Vertrag:

#### Artikel 1  
Name, Sitz und Mitgliedschaft

(1) Das Versorgungswerk der Mitglieder des Landtags Nordrhein-Westfalen und des Landtags Brandenburg trägt ab dem 1.
Dezember 2019 den Namen „Versorgungswerk der Mitglieder der Landtage von Nordrhein-Westfalen, Brandenburg und Baden-Württemberg (Versorgungswerk der Landtage – VLT)“ (im Folgenden: Versorgungswerk).
Das Versorgungswerk hat seinen Sitz in Düsseldorf.

(2) Die Abgeordneten des Landtags von Baden-Württemberg, die am 1.
Dezember 2019 dem Landtag von Baden-Württemberg angehören oder später eintreten, sind Mitglieder des Versorgungswerks.

#### Artikel 2  
Rechtsgrundlagen

(1) Die Rechte und Pflichten der Mitglieder und der sonstigen Leistungsberechtigten des Versorgungswerks ergeben sich aus den Bestimmungen dieses Vertrages, aus § 10 Abgeordnetengesetz Nordrhein-Westfalen vom 5. April 2005, § 15 Abgeordnetengesetz Brandenburg vom 19.
Juni 2013 und § 11 Abgeordnetengesetz Baden-Württemberg vom 12.
September 1978 sowie aus der Satzung des Versorgungswerks in der jeweils geltenden Fassung.
Für die Zeit zwischen dem 1. Dezember 2019 und dem Inkrafttreten der neuen Satzung des Versorgungswerks kann nach Maßgabe dieses Vertrages von den Regelungen der Satzung abgewichen werden.

(2) Die Präsidentinnen und Präsidenten können in ihrer Funktion als Behördenleiterinnen bzw.
Behördenleiter mit Zustimmung des Vorstands ergänzende Vereinbarungen zu diesem Vertrag zur Umsetzung des Beitritts des Landtags von Baden-Württemberg zum Versorgungswerk, insbesondere hinsichtlich organisatorischer und technischer Fragen, treffen.
Eine solche Vereinbarung bedarf nicht der Zustimmung der jeweiligen Landtage.

(3) Das Versorgungswerk kann von den Vertragspartnern Auskünfte über die Mitglieder und die sonstigen Leistungsberechtigten einholen, soweit die Auskünfte für die Feststellung der Mitgliedschaft, der Beitragspflicht oder der Versorgungsleistung erforderlich sind.
Die Vertragspartner können Auskünfte vom Versorgungswerk über ihre Mitglieder und sonstigen Leistungsberechtigten einholen, soweit diese für die Gewährung von Leistungen nach den jeweiligen Abgeordnetengesetzen erforderlich sind.

#### Artikel 3  
Übergangsregelungen zur Beitragspflicht und zur Mindestbeitragszeit für Mitglieder des Landtags von Baden-Württemberg

(1) Der Landtag von Baden-Württemberg ist berechtigt, in seinem Abgeordnetengesetz Übergangsregelungen zur Befreiung von der Beitragspflicht für bestimmte Gruppen von Abgeordneten bis zum 30.
April 2031 vorzusehen.
Darüber hinaus kann der Vorstand des Versorgungswerks in begründeten Einzelfällen Ausnahmen von der Beitragspflicht zulassen.

(2) Für Mitglieder des Landtags von Baden-Württemberg, die dem Landtag am 1. Dezember 2019 angehören, entsteht abweichend von § 15 der Satzung für Mandatszeiten zwischen dem 1. Dezember 2019 und dem 30. April 2021 ein Anspruch auf Altersrente, sofern zu diesem Zeitpunkt mindestens zwölf Monate Beiträge in Höhe des Pflichtbeitrages eingezahlt worden sind und die übrigen Voraussetzungen nach der Satzung erfüllt sind.

#### Artikel 4  
Vertreterversammlung

(1) Die Vertreterversammlung besteht aus insgesamt 40 Vertreterinnen und Vertretern sowie Stellvertretern in gleicher Anzahl.
Die Festlegung der Anzahl der Vertreterinnen und Vertreter aus den jeweiligen Ländern erfolgt im Verhältnis der gesetzlichen Mitgliederzahlen der Landtage von Nordrhein-Westfalen, Brandenburg und Baden-Württemberg (19 NRW, 9 BB, 12 BW).
Die Vertreterinnen und Vertreter sind unabhängig und an Weisungen nicht gebunden.
Die nordrhein-westfälischen, brandenburgischen und baden-württembergischen Mitglieder des Versorgungswerks (Landesgruppen) wählen zu Beginn der Wahlperiode ihres jeweiligen Landtags die auf sie entfallenden Vertreterinnen und Vertreter für die Dauer der jeweiligen Wahlperiode.
Wählbar und wahlberechtigt sind nur Mitglieder des Versorgungswerks, die nicht nach Artikel 3 Absatz 1 von der Beitragspflicht befreit sind.
Die gewählten Vertreterinnen und Vertreter führen ihre Ämter bis zur Wahl ihrer Nachfolgerinnen und Nachfolger fort.
Das Nähere bestimmt die Wahlordnung als Bestandteil der Satzung.

(2) Die Vertreterversammlung wählt aus ihrer Mitte die Vorsitzende oder den Vorsitzenden auf Vorschlag der Gruppe der Vertreterinnen und Vertreter aus Nordrhein-Westfalen, die erste Stellvertreterin oder den ersten Stellvertreter auf Vorschlag der Gruppe der Vertreterinnen und Vertreter aus Brandenburg sowie die zweite Stellvertreterin oder den zweiten Stellvertreter auf Vorschlag der Gruppe der Vertreterinnen und Vertreter aus Baden-Württemberg.
Die Amtsdauer der Gewählten richtet sich nach der Amtsdauer der jeweils vorschlagsberechtigten Landesgruppe der Vertreterversammlung.

(3) Die Vertreterversammlung ist beschlussfähig, wenn mehr als die Hälfte ihrer Mitglieder anwesend ist.
Die Anwesenheit kann auch durch Zuschaltung mittels Videokonferenz aus den jeweiligen Landtagen hergestellt werden.
Ist die Beschlussfähigkeit nicht gegeben, wird die Vertreterversammlung erneut einberufen.
In dieser Sitzung ist sie auch beschlussfähig, wenn mehr als ein Drittel aller Mitglieder anwesend ist.
Zwischen beiden Sitzungen müssen mindestens zwei Tage liegen.

(4) Die Beschlüsse werden mit einfacher Mehrheit der anwesenden Mitglieder der Vertreterversammlung gefasst.
Beschlüsse über den Erlass oder die Änderung der Satzung einschließlich der Wahlordnung bedürfen einer qualifizierten Mehrheit von zwei Dritteln der Mitglieder jeder Landesgruppe in der Vertreterversammlung.
Die Satzung kann weitere Fälle vorsehen, für deren Entscheidung eine qualifizierte Mehrheit erforderlich ist, sowie ein Vetorecht jeder Landesgruppe in der Vertreterversammlung für bestimmte Entscheidungen festlegen.

(5) Die Sitzungen der Vertreterversammlung werden in der Regel in Form von Videokonferenzen durchgeführt.
In Ausnahmefällen können auch Präsenzsitzungen am Sitz des Landtags Nordrhein-Westfalen stattfinden.
Die oder der Vorsitzende der Vertreterversammlung und die erste und zweite Stellvertreterin bzw.
der erste und zweite Stellvertreter können einvernehmlich beschließen, die Vertreterversammlung auch nach Landesgruppen getrennt einzuberufen.
Die Vertreterversammlung ist einzuberufen, wenn ein Drittel ihrer Mitglieder oder die Vertreterinnen und Vertreter einer Landesgruppe in der Vertreterversammlung dies verlangen.

(6) Die bei Inkrafttreten dieses Vertrags im Amt befindlichen Vertreterinnen und Vertreter bleiben bis zur jeweils nächsten regulären Neuwahl gemäß Absatz 1 Satz 4 im Amt.
Der Landtag von Baden-Württemberg wählt zehn Vertreterinnen und Vertreter, die bei Inkrafttreten dieses Vertrags Mitglied der Vertreterversammlung werden.
Anlässlich der nächsten regulären Neuwahl der Landesgruppe Brandenburg und anlässlich der nächsten regulären Neuwahl der Landesgruppe Nordrhein-Westfalen wählt der Landtag von Baden-Württemberg je eine weitere Vertreterin oder einen weiteren Vertreter, die oder der Mitglied der Vertreterversammlung wird, wenn die jeweils neu gewählte Landesgruppe in der Vertreterversammlung ihr Amt antritt.
Die Vertreterversammlung erlässt nach der Wahl der Neumitglieder aus Brandenburg und Baden-Württemberg eine neue Satzung für das Versorgungswerk.

#### Artikel 5  
Vorstand

(1) Der Vorstand besteht aus insgesamt elf Mitgliedern, von denen fünf dem Landtag Nordrhein-Westfalen, zwei dem Landtag Brandenburg und zwei dem Landtag von Baden-Württemberg angehören müssen.
Weitere Mitglieder sind eine ehemalige Abgeordnete oder ein ehemaliger Abgeordneter sowie eine Geschäftsführerin oder ein Geschäftsführer, die oder der nicht dem Versorgungswerk angehört.
Es sollen diejenigen Fraktionen einen Sitz im Vorstand erhalten, die in mehr als der Hälfte der Mitgliedsländer vertreten sind.
Im Falle eines Beitritts weiterer Landtage zum Versorgungswerk bleiben die Mehrheitsverhältnisse bei der Zusammensetzung des Vorstands unverändert.

(2) Die Mitglieder des Vorstands nach Absatz 1 Satz 1 werden von der Vertreterversammlung einzeln und in geheimer Wahl gewählt.
Jede Landesgruppe in der Vertreterversammlung hat das Vorschlagsrecht für so viele Mitglieder, wie ihr nach Absatz 1 Satz 1 zustehen.
Die Amtsdauer der Vorstandsmitglieder entspricht der Amtsdauer der jeweils vorschlagsberechtigten Landesgruppe in der Vertreterversammlung.

(3) Die Vertreterversammlung bestellt die Geschäftsführerin oder den Geschäftsführer und wählt die ehemalige Abgeordnete oder den ehemaligen Abgeordneten jeweils auf Vorschlag der Landesgruppe Nordrhein-Westfalen.
Die Amtsdauer der oder des ehemaligen Abgeordneten richtet sich nach der Amtsdauer der nordrhein-westfälischen Vorstandsmitglieder; die Geschäftsführerin oder der Geschäftsführer wird auf unbestimmte Zeit bestellt.

(4) Der Vorstand wählt aus seiner Mitte einzeln und in geheimer Wahl die Vorsitzende oder den Vorsitzenden auf Vorschlag der Vorstandsmitglieder aus Nordrhein-Westfalen, die erste Stellvertreterin oder den ersten Stellvertreter auf Vorschlag der Vorstandsmitglieder aus Brandenburg und die zweite Stellvertreterin oder den zweiten Stellvertreter auf Vorschlag der Vorstandsmitglieder aus Baden-Württemberg.

(5) Die Vertreterversammlung wählt in ihrer ersten Sitzung nach dem Beitritt des Landtags von Baden-Württemberg die Vorstandsmitglieder aus Brandenburg (2) und aus Baden-Württemberg (2) in den Vorstand.
Der bisherige Vorstand bleibt bis zur Wahl dieser Mitglieder im Amt.

#### Artikel 6  
Geschäftsführender Vorstand und Geschäftsführung

(1) Aus der Mitte des Vorstands wird ein geschäftsführender Vorstand gebildet.
Dieser besteht aus der oder dem Vorstandsvorsitzenden, den beiden Stellvertreterinnen oder Stellvertretern, einem weiteren Vorstandsmitglied aus Nordrhein-Westfalen sowie der Geschäftsführerin oder dem Geschäftsführer.

(2) Die Geschäftsführung verbleibt auf Dauer am Sitz des Versorgungswerks in Düsseldorf.
Dies gilt auch für den Fall des Beitritts weiterer Landtage zum Versorgungswerk.

(3) Die Aufgabenverteilung zwischen der Geschäftsführung und den Verwaltungen der jeweiligen Landtage wird in einer Verwaltungsvereinbarung zwischen dem Versorgungswerk und der jeweiligen Präsidentin oder dem jeweiligen Präsidenten geregelt.

#### Artikel 7  
Rechtsaufsicht

(1) Die Versicherungsaufsicht sowie die Körperschaftsaufsicht über das Versorgungswerk führt das Ministerium der Finanzen des Landes Nordrhein-Westfalen im Benehmen mit den für die Versicherungsaufsicht zuständigen Ministerien der Länder Brandenburg und Baden-Württemberg.
Diese sind befugt, Vertreterinnen oder Vertreter zu den Sitzungen der Vertreterversammlung und des Vorstands zu entsenden.

(2) Das Versorgungswerk leitet den geprüften Jahresabschluss den nach Absatz 1 zuständigen Ministerien der Länder Brandenburg und Baden-Württemberg zu.

#### Artikel 8  
Vermögen und Kosten

(1) Das von den Mitgliedern des Versorgungswerks eingebrachte Vermögen wird gemeinsam verwaltet.

(2) Die bis zum 1.
Dezember 2019 erworbenen Ansprüche der Mitglieder des Versorgungswerks der Mitglieder des Landtags Nordrhein-Westfalen und des Landtags Brandenburg bleiben unberührt.
Das Versorgungswerk erstellt zum 30.
November 2019 eine Zwischenbilanz.
Die zu diesem Bilanzstichtag ausgewiesenen Aktiva und Passiva sowie alle stillen Reserven und stillen Lasten werden in wirtschaftlicher Hinsicht dauerhaft den zu diesem Zeitpunkt dem Versorgungswerk angehörenden Mitgliedern zugerechnet.
Über weitere Maßnahmen zur Umsetzung der Ansprüche nach Satz 1 entscheidet der Vorstand.

(3) Der Landtag von Baden-Württemberg beteiligt sich an der bis zum 30.
November 2019 aufgebauten Verlustrücklage des Versorgungswerks mit einem Betrag in Höhe von 1.200.000 Euro.
Der Betrag ist in drei oder vier gleich hohen jährlichen Raten zu zahlen, die erste Zahlung ist am 1. Dezember 2019 fällig.
Das Versorgungswerk und der Landtag von Baden-Württemberg können abweichende Zahlungsmodalitäten vereinbaren; der Gesamtbetrag muss jedoch spätestens am 31. Dezember 2023 beglichen sein.

(4) Die Verwaltungskosten werden anteilig im Verhältnis der gesetzlichen Mitgliederzahlen des Landtags Nordrhein-Westfalen, des Landtags Brandenburg und des Landtags von Baden-Württemberg auf die jeweiligen Landesparlamente umgelegt und vom Landtag Nordrhein-Westfalen eingezogen.
Dies gilt nicht für Reisekosten und Aufwandsentschädigungen der Mitglieder.

(5) Die vom Landtag von Baden-Württemberg zu tragenden Verwaltungskosten bestimmen sich abweichend von Absatz 4 Satz 1 für die Zeit bis zum 30.
April 2031 nach dem Verhältnis der gesetzlichen Mitgliederzahlen des Landtags Nordrhein-Westfalen und des Landtags Brandenburg zur Zahl der Mitglieder des Landtags von Baden-Württemberg, die nicht nach Artikel 3 Absatz 1 von der Beitragspflicht befreit sind, solange diese Zahl niedriger ist als die Zahl der gesetzlichen Mitglieder.

#### Artikel 9  
Beiträge

Der Landtag von Baden-Württemberg führt ab dem 1.
Dezember 2019 die Beiträge seiner Mitglieder nach § 11 Abgeordnetengesetz Baden-Württemberg an das Versorgungswerk ab.
Die Zahlung erfolgt monatlich.
Die Beiträge für den Monat Dezember 2019 müssen spätestens bis 10. Januar 2020 beim Versorgungswerk eingegangen sein.

#### Artikel 10  
Kündigung

(1) Dieser Vertrag kann von jedem der vertragsschließenden Landtage zum Ablauf seiner auf den Ausspruch der Kündigung folgende nächsten Wahlperiode gekündigt werden.
Der Vertrag besteht zwischen den anderen beiden Landtagen fort.
Bei Kündigung durch zwei Landtage wird der Vertrag mit Wirksamwerden der zweiten Kündigung beendet.

(2) Im Fall einer Kündigung oder Beendigung des Vertrages findet keine Vermögensauseinandersetzung statt.
Die von den Mitgliedern des Versorgungswerks eingebrachten Beiträge verbleiben im Vermögen des Versorgungswerks; die zum Zeitpunkt des Wirksamwerdens einer Kündigung oder Beendigung des Vertrages erworbenen Anwartschaften sowie Ansprüche wegen der Nichterfüllung der Wartezeit für eine Altersrente bleiben bestehen, soweit sie nicht durch Maßnahmen zur Deckung von Fehlbeträgen oder zum Ausgleich von Bilanzverlusten gemindert werden.
Solange Anwartschaften auf Leistungen bestehen oder Renten aus dem Versorgungswerk gezahlt werden, ist nach der Kündigung bei der Umlegung der Verwaltungskosten abweichend von Artikel 8 Absatz 4 Satz 1 für den kündigenden Landtag die Zahl der Mitglieder des Versorgungswerks aus dem entsprechenden Land maßgeblich, sobald diese Zahl niedriger ist als die Zahl der gesetzlichen Mitglieder des Landtags.

(3) Soweit im Falle einer Kündigung über die Bestimmungen der Absätze 1 und 2 hinausgehende Maßnahmen zu treffen sind, regeln die Vertragspartner diese im gegenseitigen Einvernehmen.

#### Artikel 11  
Inkrafttreten, Außerkrafttreten

(1) Dieser Vertrag bedarf der Zustimmung der Landtage von Nordrhein-Westfalen, Brandenburg und Baden-Württemberg.

(2) Er tritt am Tag nach der Unterzeichnung der Vertragsurkunde durch alle Vertragspartner in Kraft.
Soweit keine gemeinsame Vertragsunterzeichnung erfolgt, hinterlegt jeder Vertragspartner eine unterzeichnete Vertragsurkunde beim Landtag Nordrhein-Westfalen.
In diesem Fall tritt der Vertrag am Tag nach der Hinterlegung durch den dritten Vertragspartner in Kraft.
Sollte die gemeinsame Unterzeichnung oder Hinterlegung nach dem 1.
Dezember 2019 erfolgen, tritt der Vertrag abweichend von Satz 1 und 3 rückwirkend zum 1. Dezember 2019 in Kraft.

(3) Gleichzeitig tritt der Vertrag zwischen dem Landtag Nordrhein-Westfalen und im Landtag Brandenburg über das Versorgungswerk der Mitglieder des Landtags Nordrhein-Westfalen und des Landtags Brandenburg vom 14.
Januar 2014 außer Kraft.

Stuttgart, den 22.
November 2019

André Kuper  
(Präsident des Landtags Nordrhein-Westfalen)

Dr.
Ulrike Liedtke  
(Präsidentin des Landtags Brandenburg)

Muhterem Aras  
(Präsidentin des Landtags von Baden-Württemberg)

[zum Gesetz](/gesetze/bbgvltg)