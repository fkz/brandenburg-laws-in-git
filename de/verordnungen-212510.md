## Verordnung über das vorübergehende Verlassen des Bereichs der Aufenthaltsgestattung

Auf Grund des § 58 Absatz 6 des Asylverfahrensgesetzes in der Fassung der Bekanntmachung vom 2.
 September 2008 (BGBl.
I S. 1798) verordnet die Landesregierung:

#### § 1 

(1) Asylbegehrende, die nicht oder nicht mehr verpflichtet sind, in einer Aufnahmeeinrichtung im Sinne des § 44 Absatz 1 des Asylverfahrensgesetzes zu wohnen, dürfen sich ohne Erlaubnis vorübergehend im gesamten Gebiet des Landes Brandenburg aufhalten.

(2) Die Verpflichtung, in einer bestimmten Gemeinde oder in einer bestimmten Unterkunft zu wohnen, bleibt unberührt.

#### § 2 

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 23.
 Juli 2010

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident  
Matthias Platzeck

Der Minister des Innern  
Rainer Speer