## Verordnung über die beamtenrechtlichen Zuständigkeiten im Geschäftsbereich des Ministeriums der Finanzen  (Beamtenzuständigkeitsverordnung MdF - BZVMdF)

Auf Grund des § 2 Absatz 4 in Verbindung mit § 4 Absatz 1 Satz 2 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S. 26) in Verbindung mit § 1 Absatz 3 Satz 1 der Ernennungsverordnung vom 1.
August 2004 (GVBl.
II S. 742) und auf Grund des § 9 Absatz 1 Satz 1 und des § 13 Absatz 1 Satz 3 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186) in Verbindung mit

*   § 56 Absatz 1 Satz 4 des Landesbeamtengesetzes, § 57 Absatz 1 Satz 2 des Landesbeamtengesetzes in Verbindung mit § 42 Absatz 2 des Beamtenstatusgesetzes vom 17.
    Juni 2008 (BGBl.
     I S. 1010), § 66 Absatz 4 des Landesbeamtengesetzes, §§ 84 Satz 2, 86 Absatz 1 Satz 2, 89 Satz 3 des Landesbeamtengesetzes, § 92 Absatz 2 des Landesbeamtengesetzes in Verbindung mit § 41 Satz 2 des Beamtenstatusgesetzes, § 103 Absatz 2 des Landesbeamtengesetzes in Verbindung mit § 54 Absatz 3 des Beamtenstatusgesetzes,
*   § 12 Absatz 2 Satz 3 und § 66 Absatz 1 des Bundesbesoldungsgesetzes in der Fassung der Bekanntmachung vom 28.
    April 2011 (BGBl.
    I S. 678),
*   § 15 Absatz 2 Satz 2 des Bundesbesoldungsgesetzes in der Fassung der Bekanntmachung vom 28.
    April 2011 (BGBl.
    I S. 678),
*   § 8 Satz 1 des Bundesreisekostengesetzes vom 26.
    Mai 2005 (BGBl.
    I S. 1418) in Verbindung mit § 63 des Landesbeamtengesetzes,
*   § 4 Absatz 1 Satz 1, § 11 Absatz 5, § 15 Absatz 2 Satz 3, § 18 Absatz 4 Satz 6, § 34 Absatz 1 Satz 2, § 35 Absatz 1 Satz 1, Absatz 2 Satz 2, Absatz 3 Satz 4, § 38 Absatz 2 Satz 1, § 42 Absatz 3 und § 47 Absatz 4 Satz 1 der Ausbildungs- und Prüfungsordnung für die Steuerbeamten vom 29.
    Oktober 1996 (BGBl.
    I S. 1581), von denen § 15 Absatz 2 und § 18 Absatz 4 durch Verordnung vom 29.
    Juli 2002 (BGBl.
    I S. 2917, 2918, 2919) geändert worden sind,
*   § 9 Absatz 4 Satz 3 der Laufbahnverordnung vom 16.
    September 2009 (GVBl.
    II S. 622),
*   § 2 Absatz 3 Satz 2 der Erholungsurlaubs- und Dienstbefreiungsverordnung vom 16.
    September 2009 (GVBl.
     II S. 618),
*   § 8 Absatz 1 der Verordnung über die Gewährung von Jubiläumszuwendungen an Beamte und Richter des Bundes in der Fassung der Bekanntmachung vom 13.
    März 1990 (BGBl.
    I S.
    87) in Verbindung mit § 64 Absatz 1 des Landesbeamtengesetzes,
*   § 9 Absatz 3 der Trennungsgeldverordnung in der Fassung der Bekanntmachung vom 29.
    Juni 1999 (BGBl.
    I S. 1533) in Verbindung mit § 63 Absatz 1 des Landesbeamtengesetzes,
*   § 54 Absatz 3 des Beamtenstatusgesetzes vom 17.
    Juni 2008 (BGBl.
    I S. 1010)

verordnet der Minister der Finanzen:

§ 1  
Ernennung, Entlassung und Versetzung in den Ruhestand
-----------------------------------------------------------

(1) Die Ausübung der Befugnis zur Ernennung für die Beamten, denen ein Amt des mittleren oder des gehobenen Dienstes verliehen wird, sowie für die entsprechenden Beamten ohne Amt und die Ausübung der Befugnis zur Entlassung und Versetzung in den Ruhestand für die Beamten des mittleren und des gehobenen Dienstes wird auf

1.  die Finanzämter (einschließlich des Technischen Finanzamtes),
2.  die Zentrale Bezügestelle des Landes Brandenburg,
3.  das Landesamt zur Regelung offener Vermögensfragen Brandenburg,
4.  die Landeshauptkasse

jeweils für ihren Geschäftsbereich übertragen.

(2) Die Ausübung der Befugnis zur Ernennung für die Beamten, denen ein Amt des mittleren, des gehobenen Dienstes oder des höheren Dienstes verliehen wird, sowie für die entsprechenden Beamten ohne Amt und die Ausübung der Befugnis der Entlassung und Versetzung in den Ruhestand wird auf den Brandenburgischen Landesbetrieb für Liegenschaften und Bauen für seinen Geschäftsbereich übertragen.
Dies gilt nicht für die Beamten der Geschäftsführung.

(3) Die Ausübung der Befugnis zur Ernennung für die Beamten, denen ein Amt des mittleren oder des gehobenen Dienstes verliehen wird, sowie für die entsprechenden Beamten ohne Amt und die Ausübung der Befugnis der Entlassung und Versetzung in den Ruhestand für die Beamten des mittleren und des gehobenen Dienstes wird auf die Fachhochschule für Finanzen des Landes Brandenburg für ihren Geschäftsbereich sowie für die Geschäftsbereiche der Landesfinanzschule und des Fortbildungszentrums der Finanzverwaltung übertragen.
Die Ernennung von Beamten, die als hauptamtlich oder nebenamtlich Lehrende an der Fachhochschule für Finanzen des Landes Brandenburg oder der Landesfinanzschule tätig sind, durch die Fachhochschule für Finanzen des Landes Brandenburg bedarf der vorherigen Zustimmung des Ministeriums der Finanzen.

(4) Die nach den Absätzen 1 bis 3 übertragene Befugnis wird im Namen des Landes Brandenburg ausgeübt.

§ 2  
Allgemeine Zuständigkeiten
--------------------------------

(1) Die in § 1 Absatz 1 und 3 genannten Behörden und Einrichtungen sind bei Beamten des mittleren und gehobenen Dienstes, die in § 1 Absatz 2 genannte Einrichtung bei Beamten des mittleren, gehobenen und höheren Dienstes – mit Ausnahme der Geschäftsführung – zuständig für:

1.  die Entscheidungen über die Versagung der Aussagegenehmigung gemäß § 56 Absatz 1 Satz 2 und 3 des Landesbeamtengesetzes; die Versagung der Aussagegenehmigung bedarf der vorherigen Zustimmung des Ministeriums der Finanzen,
2.  die Entscheidungen auf dem Gebiet des Nebentätigkeitsrechts gemäß den §§ 83 bis 89 des Landesbeamtengesetzes,
3.  die Untersagung einer Beschäftigung oder Erwerbstätigkeit nach Beendigung des Beamtenverhältnisses gemäß § 92 Absatz 2 des Landesbeamtengesetzes in Verbindung mit § 41 Satz 2 des Beamtenstatusgesetzes,
4.  die Kürzung der Anwärterbezüge gemäß § 66 Absatz 1 des Bundesbesoldungsgesetzes,
5.  die Entscheidungen gemäß § 9 Absatz 4 Satz 3 der Laufbahnverordnung,
6.  die Anerkennung des Urlaubs gemäß § 2 Absatz 3 Satz 2 der Erholungsurlaubs- und Dienstbefreiungsverordnung,
7.  die Entscheidungen gemäß § 8 Absatz 1 der Verordnung über die Gewährung von Jubiläumszuwendungen an Beamte und Richter des Bundes,
8.  die Entscheidung und den Bescheiderlass hinsichtlich der Anweisung des dienstlichen Wohnsitzes gemäß § 15 Absatz 2 Satz 2 des Bundesbesoldungsgesetzes.

(2) Die Zustimmungsbefugnis gemäß § 57 Absatz 1 Satz 2 des Landesbeamtengesetzes in Verbindung mit § 42 Absatz 2 des Beamtenstatusgesetzes wird auf die nachgeordnete Behörde oder Einrichtung übertragen, in der der Beamte tätig ist oder zuletzt tätig war.

§ 3  
Zuständigkeiten der Zentralen Bezügestelle des Landes Brandenburg
-----------------------------------------------------------------------

Die Zentrale Bezügestelle des Landes Brandenburg ist zuständig für:

1.  die Zustimmung zum vollständigen oder teilweisen Verzicht einer Rückforderung gemäß § 12 Absatz 2 Satz 3 des Bundesbesoldungsgesetzes für die Beamten im Geschäftsbereich des Ministeriums der Finanzen,
2.  die Geltendmachung von Schadensersatzansprüchen des Landes sowie von Entscheidungen über die Gewährung von Schadenersatz für Sachschäden gemäß den §§ 66 und 67 des Landesbeamtengesetzes für die Beamten des Geschäftsbereichs des Ministeriums der Finanzen,
3.  die Gewährung von Trennungsgeld nach § 9 Absatz 3 der Trennungsgeldverordnung für die Beamten im  
    Geschäftsbereich des Ministeriums der Finanzen,
4.  die Entscheidungen gemäß § 8 Satz 1 zweiter Halbsatz des Bundesreisekostengesetzes für die Beamten im Geschäftsbereich des Ministeriums der Finanzen.

§ 4  
Zuständigkeiten der Fachhochschule für Finanzen des Landes Brandenburg und der Landesfinanzschule Brandenburg
-------------------------------------------------------------------------------------------------------------------

Die Fachhochschule für Finanzen des Landes Brandenburg und die Landesfinanzschule Brandenburg sind jeweils für ihren Geschäftsbereich zuständig für:

1.  die Bestellung der Dozenten des gehobenen Dienstes gemäß § 4 Absatz 1 Satz 1 der Ausbildungs- und Prüfungsordnung der Steuerbeamten im Einvernehmen mit dem Ministerium der Finanzen,
2.  die Entscheidungen gemäß § 15 Absatz 2 Satz 3 und § 18 Absatz 4 Satz 6 der Ausbildungs- und Prüfungsordnung der Steuerbeamten,
3.  die Berufung der Mitglieder der Prüfungsausschüsse und Bestellung der Vorsitzenden gemäß § 34 Absatz 1 Satz 2 der Ausbildungs- und Prüfungsordnung der Steuerbeamten,
4.  das Ansetzen und die organisatorische Leitung der Prüfungen gemäß § 35 Absatz 1 der Ausbildungs- und Prüfungsordnung der Steuerbeamten,
5.  die Entscheidung über die Gestattung der Anwesenheit von Personen, die nicht dem Prüfungsausschuss angehören, in den mündlichen Prüfungen gemäß § 35 Absatz 2 der Ausbildungs- und Prüfungsordnung der Steuerbeamten,
6.  die Entscheidungen gemäß § 35 Absatz 3 der Ausbildungs- und Prüfungsordnung der Steuerbeamten,
7.  die Auswahl der Prüfungsaufgaben gemäß § 38 Absatz 2 der Ausbildungs- und Prüfungsordnung der Steuerbeamten,
8.  die Entscheidung über den Antrag des Prüflings auf Einsichtnahme in seine Prüfungsarbeiten einschließlich der Bewertung und der ihr zugrunde liegenden Unterlagen gemäß § 42 Absatz 3 und § 46 Absatz 4 der Ausbildungs- und Prüfungsordnung der Steuerbeamten,
9.  die Entscheidung über die Zuerkennung der Befähigung für die Laufbahn des mittleren Dienstes bei Prüflingen, die die Laufbahnprüfung für den gehobenen Dienst endgültig nicht bestanden oder auf deren Wiederholung verzichtet haben, gemäß § 47 Absatz 4 Satz 1 der Ausbildungs- und Prüfungsordnung der Steuerbeamten.

§ 5  
Befugnis zum Erlass von Widerspruchsbescheiden
----------------------------------------------------

Für den Erlass von Widerspruchsbescheiden in beamtenrechtlichen Angelegenheiten der Beamten, Ruhestandsbeamten und früheren Beamten im Geschäftsbereich des Ministeriums der Finanzen sowie deren Hinterbliebenen sind die in § 1 Absatz 1 bis 3 genannten Behörden und Einrichtungen zuständig, soweit diese die mit dem Widerspruch angegriffene Maßnahme getroffen oder unterlassen haben.
Dies gilt auch für den Erlass von Widerspruchsbescheiden durch die Zentrale Bezügestelle des Landes Brandenburg, soweit diese im Zuständigkeitsbereich der Zentralen Bezügestelle des Landes Brandenburg liegen.

§ 6  
Vertretung bei Klagen aus dem Beamtenverhältnis
-----------------------------------------------------

Die Vertretung des Landes vor den Gerichten der Verwaltungsgerichtsbarkeit wird auf die in § 1 Absatz 1 bis 3 genannten Behörden und Einrichtungen übertragen, soweit diese selbst über den Widerspruch zu entscheiden haben.
Im Übrigen, insbesondere bei Disziplinarsachen, verbleibt die Vertretungsbefugnis beim Ministerium der Finanzen.
Die Sätze 1 und 2 gelten entsprechend für die Verfahren auf einstweiligen Rechtsschutz nach der Verwaltungsgerichtsordnung.

§ 7  
Übergangsvorschrift
-------------------------

Bei bereits anhängigen Klageverfahren in beamtenrechtlichen Angelegenheiten bleibt für die Vertretung des Landes vor den Gerichten der Verwaltungsgerichtsbarkeit die bisher bearbeitende Stelle weiter zuständig.

§ 8  
Inkrafttreten, Außerkrafttreten
-------------------------------------

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Beamtenzuständigkeitsverordnung MdF vom 15.
August 2008 (GVBl.
II S. 327) außer Kraft.

Potsdam, den 8.
Mai 2012

Der Minister der Finanzen  
Dr.
Helmuth Markov