## Gesetz zum Vertrag über die Errichtung des IT-Planungsrats und über die Grundlagen der Zusammenarbeit beim Einsatz der Informationstechnologie in den Verwaltungen von Bund und Ländern (Vertrag zur Ausführung von Artikel 91c GG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem am 4.
November 2009 vom Land Brandenburg unterzeichneten Vertrag über die Errichtung des IT-Planungsrats und über die Grundlagen der Zusammenarbeit beim Einsatz der Informationstechnologie in den Verwaltungen von Bund und Ländern (Vertrag zur Ausführung von Artikel 91c GG) wird zugestimmt.
Der Vertrag wird nachstehend veröffentlicht.

#### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Vertrag nach seinem § 7 Absatz 1 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 16.
Februar 2010

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Vertrag](/de/vertraege-248218)