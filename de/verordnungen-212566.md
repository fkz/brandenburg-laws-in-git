## Verordnung zur Übertragung von Zuständigkeiten des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz für die Bewilligung, Berechnung und Zahlung von Trennungsgeld und für die Berechnung und Zahlung von Reisekosten auf die Zentrale Bezügestelle des Landes Brandenburg  (Reisekostenzuständigkeitsübertragungsverordnung MUGV - RkZÜVMUGV)

Auf Grund des § 63 Absatz 3 Satz 2 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S.
26) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz im Einvernehmen mit dem Minister der Finanzen:

§ 1  
Übertragung von Aufgaben
------------------------------

(1) Die Zuständigkeit des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz für die Berechnung und Zahlung von Reisekosten im Sinne des § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes wird für den gesamten Geschäftsbereich auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

(2) Ebenso wird die Zuständigkeit für die Bewilligung, Berechnung und Zahlung von Trennungsgeld nach § 9 Absatz 3 der Trennungsgeldverordnung auch im Falle von Abordnungen im Rahmen der Aus- oder Fortbildung auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

§ 2  
Vertretung bei Klagen
---------------------------

Im Rahmen der Übertragung der Zuständigkeit nach § 1 wird die Zentrale Bezügestelle des Landes Brandenburg ermächtigt, das Ministerium für Umwelt, Gesundheit und Verbraucherschutz in verwaltungs- und arbeitsrechtlichen Streitigkeiten zu vertreten.
Dies gilt auch für Anträge in einstweiligen Rechtsschutzverfahren.

§ 3  
Übergangsvorschrift
-------------------------

Für Anträge auf Bewilligung, Berechnung und Zahlung von Trennungsgeld und die Berechnung und Zahlung von Reisekosten, die vor dem jeweiligen Inkrafttreten dieser Verordnung eingegangen sind und über die noch nicht abschließend entschieden worden ist, verbleibt es bei der Zuständigkeit der bisher zuständigen Stelle.
Dies gilt auch für die Vertretung in zu diesem Zeitpunkt bereits anhängigen Rechtsstreitigkeiten.

§ 4  
Inkrafttreten
-------------------

Diese Verordnung tritt mit Wirkung vom 1.
Juli 2011 in Kraft.

Potsdam, den 12.
September 2011

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack