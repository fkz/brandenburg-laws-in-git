## Ordnungsbehördliche Verordnung zum Schutz von Kriegsstätten

Auf Grund des § 25 Absatz 1 in Verbindung mit § 30 Absatz 1 des Ordnungsbehördengesetzes in der Fassung der Bekanntmachung vom 21.
August 1996 (GVBl.
I S.
266) verordnet der Minister des Innern im Einvernehmen mit der Ministerin für Wissenschaft, Forschung und Kultur nach Kenntnisnahme durch den Ausschuss für Inneres des Landtages:

§ 1
---

Es ist verboten, auf Kriegsstätten nach

1.  Kriegstoten einschließlich Leichenteilen,
2.  Uniformen oder sonstigen Kleidungsstücken,
3.  Erkennungsmarken oder sonstigen Gegenständen, die der Identifizierung dienen,
4.  Nachlassgegenständen,
5.  Auszeichnungen sowie
6.  militärischem Gerät

zu suchen, Kriegstote sowie vorbezeichnete Gegenstände auszugraben oder in Besitz zu nehmen.
Das Verbot erstreckt sich auch auf die Benutzung von Sonden und anderen technischen Hilfsmitteln, wenn hierdurch Kriegstote sowie vorbezeichnete Gegenstände zutage gefördert oder gefährdet werden können.

§ 2
---

(1) Als Kriegstote gelten

1.  Personen, die in der Zeit vom 26.
    August 1939 bis 8.
    Mai 1945 während ihres militärischen oder militär-ähnlichen Dienstes gefallen oder tödlich verunglückt oder an den Folgen der in diesem Dienste erlittenen Gesundheitsschäden gestorben sind,
2.  Zivilpersonen, die in der Zeit vom 26.
    August 1939 bis 8.
    Mai 1945 durch unmittelbare Kriegseinwirkung zu Tode gekommen oder an den Folgen der durch unmittelbare Kriegseinwirkung erlittenen Gesundheitsschäden gestorben sind,
3.  sonstige in § 1 Absatz 1 des Gräbergesetzes aufgeführte Personengruppen.

(2) Kriegsstätten sind solche Gebiete, auf denen bis 8.
Mai 1945 Kampfhandlungen stattgefunden haben und auf denen Kriegstote außerhalb ausgewiesener Begräbnisplätze bestattet wurden, oder in denen die in § 1 aufgeführten Gegenstände lagern.

(3) Die Kreisordnungsbehörden bestimmen durch ordnungsbehördliche Verordnungen räumlich umgrenzte Gebiete zu Kriegsstätten.

§ 3
---

Von dem Verbot nach § 1 können die Kreisordnungsbehörden Befreiung erteilen, wenn ein berechtigtes Interesse vorliegt.
Die Befreiung kann nur erteilt werden, wenn gewährleistet ist, dass die beabsichtigte Such- oder Grabungsmaßnahme ordnungsgemäß durchgeführt und das öffentliche Interesse nicht beeinträchtigt wird.
Vor Erteilung einer Befreiung ist das Benehmen mit dem Brandenburgischen Landesamt für Denkmalpflege und Archäologisches Landesmuseum herzustellen.

§ 4
---

(1) Ordnungswidrig handelt, wer gegen das Verbot des § 1 verstößt.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße geahndet werden.

(3) Zuständige Verwaltungsbehörde im Sinne des § 36 Absatz 1 Nummer 1 des Gesetzes über Ordnungswidrigkeiten ist die Kreisordnungsbehörde.

(4) Die durch eine Ordnungswidrigkeit nach Absatz 1 in Besitz genommenen Gegenstände können eingezogen werden.

§ 5
---

Die Vorschriften des Gräbergesetzes in der Fassung der Bekanntmachung vom 16.
Januar 2012 (BGBl.
I S.
98), das zuletzt durch Artikel 9 des Gesetzes vom 23.
Juli 2013 (BGBl.
I S.
2586) geändert worden ist, bleiben unberührt.

§ 6
---

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 31.
März 2014

Der Minister des Innern

Ralf Holzschuher