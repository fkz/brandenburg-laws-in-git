## Verordnung über den Schutzwald „Naturwald Kreuzbach“

Auf Grund des § 12 des Waldgesetzes des Landes Brandenburg vom 20. April 2004 (GVBl. I S. 137) verordnet der Minister für Infrastruktur und Landwirtschaft:

### § 1  
Erklärung zum Schutzwald

Die in § 2 näher bezeichneten Waldflächen mit besonderer Schutzfunktion als Naturwald werden zum Schutzwald erklärt.
Der Schutzwald trägt die Bezeichnung „Naturwald Kreuzbach“ und wird in das Register der geschützten Waldgebiete aufgenommen.

### § 2  
Schutzgegenstand

(1) Das geschützte Waldgebiet befindet sich im Landkreis Prignitz und hat eine Größe von rund 23 Hektar.
Es umfasst folgende Flurstücke:

**Gemeinde:**

**Gemarkung:**

**Flur:**

**Flurstück:**

Stadt Putlitz

Lütkendorf

3

2, 3, 5, 24 bis 27 (jeweils anteilig).

(2) Zur Orientierung ist dieser Verordnung eine Kartenskizze über die Lage des Schutzwaldes als Anlage beigefügt.

(3) Die Grenze des Schutzwaldes ist in der „Topografischen Karte zur Verordnung über den Schutzwald ‚Naturwald Kreuzbach‘“, Maßstab 1 : 15 000 und in der „Liegenschaftskarte zur Verordnung über den Schutzwald ‚Naturwald Kreuzbach‘“, Maßstab 1 : 10 000 mit ununterbrochener Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Maßgeblich ist die Einzeichnung in der Liegenschaftskarte.
Die Karten sind mit dem Dienstsiegel des Ministeriums für Infrastruktur und Landwirtschaft, Siegelnummer 26, versehen und vom Siegelverwahrer am 30. Januar 2013 unterschrieben worden.

(4) Die Verordnung mit Karten kann beim Ministerium für Infrastruktur und Landwirtschaft in Potsdam, oberste Forstbehörde, sowie beim Landesbetrieb Forst Brandenburg in Potsdam, untere Forstbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

### § 3  
Schutzzweck

(1) Schutzzweck des Schutzwaldes ist

1.  die Erhaltung und Entwicklung eines grundwasserbeeinflussten Stieleichen-Hainbuchenwaldes subatlantischer Ausprägung mit Stechpalme und mit Vorkommen von Erlen- und Eschenwald zum Zwecke der wissenschaftlichen Beobachtung und Erforschung der naturnahen Entwicklung des Waldes;
2.  die Erhaltung und Wiederherstellung der Leistungsfähigkeit des Naturhaushaltes, insbesondere des Wasserhaushaltes des von der Stepenitz beeinflussten Waldgebietes;
3.  die Erhaltung und Entwicklung des Gebietes als Lebensraum gefährdeter Pflanzenarten.

(2) Der besondere Schutzzweck ist der Erhalt des Naturwaldes insbesondere zur

1.  Repräsentation eines besonders wertvollen Restbestandes eines grundwasserbeeinflussten Laubmischwaldes in der nördlichen waldarmen Prignitz;
2.  langfristigen wissenschaftlichen Erforschung der durch den Menschen nicht direkt beeinflussten Waldentwicklung eines grundwasserbeeinflussten eutrophen Laubmischwaldes mit einer Vielzahl einheimischer Laubbaumarten;
3.  Erforschung der Waldstruktur, des Bodens, der Flora und der Fauna sowie der natürlichen Selbstorganisationsprozesse des grundwasserbeeinflussten Laubmischwaldes;
4.  Nutzung als lokale und regionale Weiserfläche zur Ableitung und exemplarischen Veranschaulichung von Erkenntnissen für die Waldbaupraxis und forstliche Lehre;
5.  Erhaltung und Regeneration forstgenetischer Ressourcen;
6.  Erhaltung der floristischen und faunistischen Artenvielfalt in sich natürlich entwickelnden Lebensgemeinschaften.

### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind im Schutzwald gemäß § 12 Absatz 6 des Waldgesetzes des Landes Brandenburg alle Handlungen verboten, die dem in § 3 genannten Schutzzweck zuwiderlaufen und das Gebiet oder einzelne seiner Bestandteile nachhaltig stören, verändern, beschädigen oder zerstören können.

(2) Es ist insbesondere verboten:

1.  das Gebiet forstwirtschaftlich zu nutzen;
2.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
3.  das Gebiet außerhalb der Waldwege zu betreten;
4.  im Gebiet mit motorisierten Fahrzeugen und Gespannen zu fahren oder diese dort abzustellen;
5.  zu zelten, zu lagern, Wohnwagen aufzustellen, zu rauchen und Feuer anzuzünden;
6.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
7.  die Bodengestalt zu verändern, Böden zu verfestigen oder zu versiegeln;
8.  Düngemittel einschließlich Wirtschaftsdünger und Sekundärrohstoffdünger zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
9.  Pflanzenschutzmittel jeder Art oder Holzschutzmittel anzuwenden;
10.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten.
    

### § 5  
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen;
2.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Forstbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Schutzwaldes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Forstbehörden und sonstige von den Forstbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer und Nutzer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums.

### § 6  
Befreiungen

Wenn überwiegende Gründe des Gemeinwohls es erfordern, kann die untere Forstbehörde im Einvernehmen mit der unteren Naturschutzbehörde auf Antrag Befreiungen von den Verboten dieser Verordnung gewähren.

### § 7  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 37 Absatz 1 Nummer 7 des Waldgesetzes des Landes Brandenburg handelt, wer vorsätzlich oder fahrlässig den Regelungen der §§ 4 und 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 37 Absatz 3 des Waldgesetzes des Landes Brandenburg mit einer Geldbuße bis zu zwanzigtausend Euro geahndet werden.

### § 8  
Verhältnis zu anderen rechtlichen Bestimmungen

(1) Die Regelungen naturschutzrechtlicher Schutzgebietsausweisungen im Bereich des in § 2 genannten Gebietes bleiben unberührt.

(2) Ebenfalls unberührt bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere die §§ 31 bis 33 und 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutz-gesetzes).

### § 9  
Geltendmachung von Rechtsmängeln

Eine Verletzung der in § 1 der Waldschutzgebietsverfahrensverordnung genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber der obersten Forstbehörde geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

### § 10  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 8.
März 2013

Der Minister für Infrastruktur und Landwirtschaft

Jörg Vogelsänger

**Anlage  
**(zu § 2 Absatz 2)

### Kartenskizze zur Verordnung über den Schutzwald „Naturwald Kreuzbach“

![Die Anlage zu § 2 Absatz 2 bildet eine Kartenskizze zur Verordnung über den Schutzwald „Naturwald Kreuzbach“ im Maßstab 1 : 10 000 ab. Es wird die Grenze des Schutzwaldes mit ununterbrochener Linie dargestellt. Der Schutzwald hat eine Größe von rund 23 Hektar.](/br2/sixcms/media.php/69/Nr.%20231.GIF "Die Anlage zu § 2 Absatz 2 bildet eine Kartenskizze zur Verordnung über den Schutzwald „Naturwald Kreuzbach“ im Maßstab 1 : 10 000 ab. Es wird die Grenze des Schutzwaldes mit ununterbrochener Linie dargestellt. Der Schutzwald hat eine Größe von rund 23 Hektar.")