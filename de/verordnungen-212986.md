## Verordnung über das Zentrum für Lehrerbildung und Bildungsforschung (ZeLBV)

Auf Grund des § 71 Absatz 1 Satz 3 des Brandenburgischen Hochschulgesetzes vom 28.
April 2014 (GVBl.
I Nr. 18) verordnet die Ministerin für Wissenschaft, Forschung und Kultur nach Anhörung des Ausschusses für Wissenschaft, Forschung und Kultur:

§ 1  
Übertragung der Zuständigkeit
-----------------------------------

Das Zentrum für Lehrerbildung und Bildungsforschung ist für die in § 2 bestimmten Aufgaben der Lehrerbildung und Bildungsforschung an der Universität Potsdam zuständig.
Im Übrigen bleiben die im Brandenburgischen Hochschulgesetz geregelten Zuständigkeiten der Fakultäten unberührt.

§ 2   
Aufgaben des Zentrums für Lehrerbildung und Bildungsforschung
--------------------------------------------------------------------

(1) Im Bereich der Lehrerbildung hat das Zentrum für Lehrerbildung und Bildungsforschung folgende Aufgaben:

1.  Abweichend von § 72 Absatz 2 Nummer 2 des Brandenburgischen Hochschulgesetzes ist das Zentrum für Lehrerbildung und Bildungsforschung zuständig für die fakultätsübergreifende Struktur- und Entwicklungsplanung der lehramtsbezogenen Lehre und Forschung sowie des lehramtsbezogenen Studiums.
    
2.  Das Zentrum für Lehrerbildung und Bildungsforschung erlässt anstelle der Fakultäten die Studien- und  
    Prüfungsordnungen für die lehramtsbezogenen weiterbildenden Studiengänge im Sinne des § 25 Absatz 2 des Brandenburgischen Hochschulgesetzes und trifft die weiteren Regelungen für die Weiterbildung im Bereich der Lehrerbildung.
    
3.  Das Zentrum für Lehrerbildung und Bildungsforschung erlässt die fächer- oder studienbereichsübergreifenden Ordnungen für schulpraktische Studien gemäß der Lehramtsstudienverordnung.
    
4.  Abweichend von § 40 Absatz 2 Satz 2 des Brandenburgischen Hochschulgesetzes werden in Berufungsverfahren zur Besetzung von lehramtsrelevanten Stellen für Hochschullehrerinnen oder Hochschullehrer zwei Mitglieder der Berufungskommission aus der Gruppe der Hochschullehrerinnen und Hochschullehrer durch die Versammlung des Zentrums für Lehrerbildung und Bildungsforschung gewählt.
    
5.  Abweichend von § 46 Absatz 2 Satz 1 des Brandenburgischen Hochschulgesetzes erfolgt bei Juniorprofessorinnen oder Juniorprofessoren, bei deren Berufung das Zentrum für Lehrerbildung und Bildungsforschung gemäß Nummer 4 mitgewirkt hat, die Entscheidung über die Bewährung auf der Grundlage von Stellungnahmen des Zentrums für Lehrerbildung und Bildungsforschung und der Fakultäten.
    

(2) In der Forschung koordiniert und fördert das Zentrum für Lehrerbildung und Bildungsforschung die Schwerpunktforschung im Bereich der Bildungsforschung sowie der Schul- und Unterrichtsforschung.
Es beteiligt sich wissenschaftlich an der Qualitätssicherung und Qualitätsverbesserung in der Lehrerbildung.
Zu diesen Zwecken kann es über die ihm hierfür zugewiesenen Mittel verfügen.

(3) Das Zentrum für Lehrerbildung und Bildungsforschung unterstützt die Kooperation mit der Fachhochschule Potsdam im Bereich der frühkindlichen Bildung.

§ 3   
Mitglieder und Organe des Zentrums für Lehrerbildung und Bildungsforschung
---------------------------------------------------------------------------------

(1) Mitglieder des Zentrums für Lehrerbildung und Bildungsforschung sind:

1.  Hochschullehrerinnen und Hochschullehrer sowie akademische Mitarbeiterinnen und Mitarbeiter, die Aufgaben in der Bildungsforschung und in der schul- und unterrichtsbezogenen Forschung sowie Aufgaben in der Lehre in lehramtsbezogenen Studiengängen und in der Lehrerfort- und Lehrerweiterbildung wahrnehmen nach Maßgabe der Satzung nach § 8,
    
2.  sonstige Mitarbeiterinnen und Mitarbeiter, die Aufgaben mit Bezug zu den unter Nummer 1 genannten Auf-gaben wahrnehmen, nach Maßgabe der Satzung nach § 8,
    
3.  Studierende, die in einem lehramtsbezogenen Bachelor- oder Masterstudiengang oder in einem Zertifikatsstudium nach der Befähigungserwerbsverordnung immatrikuliert sind.
    Dies gilt nicht für Neben- und Gasthörer.
    

(2) Die sich aus einer Fakultätsmitgliedschaft ergebenden Rechte und Pflichten bleiben durch die Mitgliedschaft im Zentrum für Lehrerbildung und Bildungsforschung unberührt.

(3) Organe des Zentrums für Lehrerbildung und Bildungsforschung sind die Direktorin oder der Direktor, die Versammlung und der Kooperationsrat.

§ 4   
Direktorin oder Direktor
-------------------------------

(1) Die Direktorin oder der Direktor leitet das Zentrum für Lehrerbildung und Bildungsforschung und vertritt es innerhalb der Universität Potsdam.

(2) Die Direktorin oder der Direktor wird auf Vorschlag der Präsidentin oder des Präsidenten der Universität Potsdam von der Versammlung gewählt.
Für die Wahl und die Abwahl gilt die Regelung des § 73 Absatz 1 und 2 des Brandenburgischen Hochschulgesetzes zur Wahl der Dekanin oder des Dekans entsprechend.

(3) Die Direktorin oder der Direktor gibt strategische Impulse für die Lehrerausbildung, die Bildungsforschung sowie die schul- und unterrichtsbezogene Forschung, die Lehrerfort- und Lehrerweiterbildung sowie für die institutionelle Weiterentwicklung an der Universität Potsdam und stellt Konzepte für die Entwicklung des Zentrums für Lehrerbildung und Bildungsforschung auf.

(4) Für die Ermäßigung des Lehrdeputats der Direktorin oder des Direktors gemäß der Lehrverpflichtungsver-ordnung und ähnliche Ausgleichsmaßnahmen gelten die Bestimmungen für Dekaninnen und Dekane entsprechend.

(5) Zur Unterstützung der Direktorin oder des Direktors wird eine Geschäftsführung des Zentrums für Lehrerbildung und Bildungsforschung gebildet.

§ 5   
Versammlung
------------------

(1) Die Fakultäten mit lehramtsbezogener Lehre und Forschung wählen aus dem Kreis der Mitglieder des Zentrums für Lehrerbildung und Bildungsforschung die Mitglieder der Versammlung.
Mitglieder der Versammlung sind die Vertreterinnen und Vertreter der Gruppe der Hochschullehrerinnen und Hochschullehrer, der akademischen Mitarbeiterinnen und Mitarbeiter, der Studierenden und der sonstigen Mitarbeiterinnen und Mitarbeiter im Verhältnis von 7 : 1 : 4 : 1.
 Näheres zur Wahl der Mitglieder der Versammlung, insbesondere zur Anzahl der durch die einzelnen Fakultäten zu wählenden Versammlungsmitglieder, regelt das nach der Grundordnung zuständige Organ der Uni-versität Potsdam durch die Satzung nach § 8.

(2) Die Versammlung hat folgende Aufgaben:

1.  den Erlass von Satzungen des Zentrums für Lehrerbildung und Bildungsforschung,
    
2.  die Entscheidung über die fakultätsübergreifende Struktur- und Entwicklungsplanung der lehramtsbezogenen Lehre und Forschung sowie des lehramtsbezogenen Studiums,
    
3.  den Erlass von fächer- oder studienbereichsübergreifenden Ordnungen für die schulpraktischen Studien (Bachelor und Master) sowie von Ordnungen für die lehramtsbezogenen weiterbildenden Studiengänge,
    
4.  die Mitwirkung an Berufungsverfahren für lehramtsrelevante Stellen für Hochschullehrerinnen und Hochschullehrer,
    
5.  die Stellungnahmen zur Bewährung von Juniorprofessorinnen und Juniorprofessoren, soweit das Zentrum für Lehrerbildung und Bildungsforschung an dem Berufungsverfahren mitgewirkt hat,
    
6.  die Aufsicht über die Direktorin oder den Direktor,
    
7.  die Wahl und Abwahl der Direktorin oder des Direktors und ihrer oder seiner Vertretung.
    

§ 6   
Kooperationsrat
----------------------

(1) Der Kooperationsrat fördert die Zusammenarbeit zwischen der Universität Potsdam und den im Land Brandenburg zuständigen Behörden für die Lehrerbildung durch eine regelmäßige Abstimmung.

(2) Die Mitglieder des Kooperationsrates sind:

1.  die oder der für Lehre und Studium zuständige Vizepräsidentin oder Vizepräsident der Universität Potsdam,
    
2.  die Direktorin oder der Direktor des Zentrums für Lehrerbildung und Bildungsforschung,
    
3.  die Geschäftsführerin oder der Geschäftsführer des Zentrums für Lehrerbildung und Bildungsforschung,
    
4.  eine Vertreterin oder ein Vertreter der für Schulen zuständigen obersten Landesbehörde,
    
5.  eine Vertreterin oder ein Vertreter der für Hochschulen zuständigen obersten Landesbehörde,
    
6.  eine Vertreterin oder ein Vertreter der für Lehrerbildung zuständigen Schulbehörde.
    

§ 7   
Zuweisung
----------------

Die Präsidentin oder der Präsident der Universität Potsdam ist zuständig für die befristete und leistungsbezogene Zuweisung von Mitteln und Stellen an das Zentrum für Lehrerbildung und Bildungsforschung gemäß § 65 Absatz 1 Nummer 5 des Brandenburgischen Hochschulgesetzes.

§ 8   
Satzung
--------------

Nähere Regelungen, insbesondere zur Organisation des Zentrums für Lehrerbildung und Bildungsforschung und zur Zusammenarbeit mit den Fakultäten, erlässt das nach der Grundordnung zuständige Organ der Universität Potsdam durch Satzung, die der Genehmigung der für die Hochschulen zuständigen obersten Landesbehörde bedarf.

§ 9   
Evaluierungsklausel
--------------------------

Die Regelungen dieser Verordnung sind zum 31.
Dezember 2018 durch ein externes Gremium zu evaluieren.
Die Universität Potsdam unterrichtet die für Hochschulen zuständige oberste Landesbehörde über das Ergebnis der Überprüfung, insbesondere über den möglichen Änderungsbedarf, auch unter dem Gesichtspunkt der Kompetenzerweiterung.

§ 10   
Inkrafttreten
=====================

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 6.
November 2014

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Prof.
Dr.-Ing.
Dr.
Sabine Kunst