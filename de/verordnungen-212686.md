## Verordnung über das Naturschutzgebiet „Teufelssee bei Sperenberg“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2, § 21 Absatz 1 Satz 2 und § 78 Absatz 1 Satz 3 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1  
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Teltow-Fläming wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Teufelssee bei Sperenberg“.

### § 2   
Schutzgegenstand

(1)  Das Naturschutzgebiet hat eine Größe von rund 11 Hektar.
Es umfasst Flächen in folgenden Flurstücken:

Gemeinde:

Gemarkung:

Flur:

Flurstücke:

Am Mellensee

Kummersdorf-Gut

6

17, 24/1 teilweise und 41 teilweise.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den topografischen Karten im Maßstab 1 : 10 000 mit dem Titel „Topografische Karte zur Verordnung über das Naturschutzgebiet ‚Teufelssee bei Sperenberg‘“ mit den Blattnummern 1 und 2 und in der Liegenschaftskarte zur Verordnung im Maßstab 1 : 2 500 mit dem Titel „Liegenschaftskarte zur Verordnung über das Naturschutzgebiet ‚Teufelssee bei Sperenberg‘“ mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die topografischen Karten ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Liegenschaftskarte.
Die Karten sind von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz am 2.
Juli 2012 unterzeichnet worden.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Teltow-Fläming, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

### § 3  
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes, das eine vermoorte Senke mit umgebenden Trockenstandorten in der nördlichen Luckenwalder Heide bei Sperenberg umfasst, ist

1.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Moore und angrenzender Bruch- und Moorwälder sowie der trockenen Offenlandfluren;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Sumpfporst (Ledum palustre), Sumpf-Schlangenwurz (Calla palustris), Rundblättriger Sonnentau (Drosera rotundifolia), Sumpf-Schwertlilie (Iris pseudacorus), Fieberklee (Menyanthes trifoliata), Krebsschere (Stratiotes aloides) und Sand-Strohblume (Helichrysum arenarium);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Amphibien und Reptilien sowie der Libellen, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Glattnatter (Coronella austriaca), Ringelnatter (Natrix natrix), Zauneidechse (Lacerta agilis), Knoblauchkröte (Pelobates fuscus), Moorfrosch (Rana arvalis), Grüne Mosaikjungfer (Aeshna viridis), Keilfleck-Mosaikjungfer (Aeshna isosceles) und Schwarze Heidelibelle (Sympetrum danae);
4.  die Erhaltung des Teufelssees als eines der ältesten Naturschutzgebiete der Region aus naturgeschichtlichen und landeskundlichen Gründen;
5.  die Erhaltung der vermoorten Senke mit den umgebenden Moor- und Bruchwäldern wegen ihrer Seltenheit, Vielfalt, besonderen Eigenart und Schönheit;
6.  die Erhaltung und Entwicklung des Gebietes als Teil des Biotopverbundes zwischen der Nuthe-Notte-Niederung und dem Baruther Urstromtal sowie als Trittsteinbiotop innerhalb der Luckenwalder Heide.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Teufelssee“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinem Vorkommen von

1.  dystrophen Seen und Teichen sowie Übergangs- und Schwingrasenmooren als Biotope von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG);
2.  Moorwäldern als prioritärer Biotop („prioritärer Lebensraumtyp“ im Sinne des Anhangs I der Richtlinie 92/43/EWG).

### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet, seinen Naturhaushalt oder einzelne seiner Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der Wege, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, zu reiten;
11.  mit Fahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
12.  zu baden oder zu tauchen;
13.  Wasserfahrzeuge aller Art einschließlich Luftmatratzen zu benutzen;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (zum Beispiel Abwasser, Klärschlamm und Bioabfälle) zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden.

### § 5  
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  ine Nutzung nur einzelstamm- bis truppweise erfolgt,
    2.  auf Mooren und in Moorwäldern keine forstwirtschaftlichen Maßnahmen erfolgen, in den übrigen Waldflächen nur Arten der potenziellen natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
    3.  ein Altholzanteil von mindestens 10 Prozent des aktuellen Bestandesvorrates zu sichern ist, wobei mindestens fünf Stämme Altholz je Hektar mit einem Mindestdurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß aus der Nutzung zu nehmen und dauerhaft zu markieren sind, in Jungbeständen ist ein solcher Altholzanteil zu entwickeln,
    4.  je Hektar mindestens fünf Stück stehendes Totholz mit einem Durchmesser von mehr als 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden sowie liegendes Totholz im Bestand verbleibt,
    5.  Horstbäume und Höhlenbäume nicht gefällt werden,
    6.  § 4 Absatz 2 Nummer 17 und 23 gilt;
2.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd außerhalb der Übergangs- und Schwingrasenmoorflächen,
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.  
          
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Aufstellung anzuzeigen.
        Die untere Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten.
        Die Entscheidung hierüber soll unverzüglich erfolgen.
    
    Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern sind unzulässig, im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Brandenburgischen Jagdgesetzes unberührt;
    
3.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter die Nummer 5 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
4.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
5.  der Betrieb von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
    Die ordnungsgemäße Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
6.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
7.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
8.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
9.  erforderliche Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne einer Fischbestandskontrolle, -regulierung und -förderung mit Zustimmung der unteren Naturschutzbehörde.
    Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
    Die fischereiwirtschaftliche Flächennutzung sowie die Ausübung der Angelfischerei bleiben unzulässig;
10.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen;
11.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2)  Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

### § 6  
Pflege- und Entwicklungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  militärische Altlasten sollen beseitigt werden;
2.  die moortypischen Pflanzen- und Tierarten sollen durch Pflegemaßnahmen wie zum Beispiel Mahd der verschilften Bereiche mit Eutrophierungszeigern gefördert werden;
3.  die trockenen Offenflächen im Ostteil des Gebietes sollen freigehalten werden;
4.  Kiefernreinbestände und nicht standortheimische Forstkulturen sollen langfristig in naturnahe, reich strukturierte Mischwaldbestände mit standortheimischen Baumarten entwickelt werden.

### § 7  
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

### § 8  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten der §§ 4 und 5 oder den Maßgaben des § 6 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

### § 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere §§ 31 bis 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

### § 10  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

### § 11   
Inkrafttreten, Außerkrafttreten

(1) Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig tritt die Verordnung über das „Naturschutzgebiet Teufelssee“ im Preußischen Forstamt Kummersdorf, Kreis Teltow, der Preußischen Regierung in Potsdam vom 2.
September 1937 (Amtsblatt der Preußischen Regierung in Potsdam, Publikations-Nummer 38 vom 11.
September 1937, Seite 211 bis 212) außer Kraft.

Potsdam, den 20.
September 2012

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

**Anlage**  
(zu § 2 Absatz 1)

![Das Naturschutzgebiet Teufelssee bei Sperenberg liegt in der Flur 6 der Gemarkung Kummersdorf-Gut im Bereich der Gemeinde Am Mellsensee umd umfasst die Flurstücke 17 sowie 24/1 und 41 jeweils teilweise. ](/br2/sixcms/media.php/69/Nr.%2024-1.gif "Das Naturschutzgebiet Teufelssee bei Sperenberg liegt in der Flur 6 der Gemarkung Kummersdorf-Gut im Bereich der Gemeinde Am Mellsensee umd umfasst die Flurstücke 17 sowie 24/1 und 41 jeweils teilweise. ")