## Verordnung über den Wahltag und die Wahlzeit der allgemeinen Kommunalwahlen 2019

Auf Grund des § 7 Absatz 1 Satz 2, des § 64 Absatz 2, des § 73 Absatz 1 Satz 2, des § 85 Absatz 1 und des § 97 Absatz 1 des Brandenburgischen Kommunalwahlgesetzes in der Fassung der Bekanntmachung vom 9.
Juli 2009 (GVBl.
I S. 326), von denen § 64 Absatz 2 durch Gesetz vom 5. Dezember 2013 (GVBl.
I Nr. 38) geändert worden ist, sowie der Bekanntmachung der Geschäftsbereiche der obersten Landesbehörden vom 17.
März 2015 (GVBl.
II Nr.
15) verordnet der Minister des Innern und für Kommunales:

#### § 1 Wahlen

(1) Die allgemeinen Wahlen zu den Gemeindevertretungen der kreisangehörigen Gemeinden, zu den Stadtverordnetenversammlungen der kreisangehörigen und kreisfreien Städte und zu den Kreistagen der Landkreise sowie die Wahlen der ehrenamtlichen Bürgermeisterinnen und Bürgermeister der amtsangehörigen Gemeinden und Städte finden am 26.
Mai 2019 in der Zeit von 8 bis 18 Uhr statt.

(2) In Gemeinden oder Städten mit Ortsteilen finden gleichzeitig mit den in Absatz 1 genannten Wahlen die unmittelbaren Wahlen der Ortsbeiräte oder der Ortsvorsteherinnen und Ortsvorsteher statt.

(3) In einer Gemeinde oder Stadt entfällt die nach Absatz 1 vorgesehene Wahl der Gemeindevertretung oder Stadtverordnetenversammlung oder der ehrenamtlichen Bürgermeisterin oder des ehrenamtlichen Bürgermeisters, in einem Ortsteil die nach Absatz 2 vorgesehene unmittelbare Wahl des Ortsbeirats oder der Ortsvorsteherin oder des Ortsvorstehers, wenn in der Gemeinde, in der Stadt oder in dem Ortsteil seit dem 26.
Mai 2018 bereits eine einzelne Neuwahl der Gemeindevertretung, der Stadtverordnetenversammlung, der ehrenamtlichen Bürgermeisterin oder des ehrenamtlichen Bürgermeisters, des Ortsbeirats oder der Ortsvorsteherin oder des Ortsvorstehers durch die wahlberechtigten Bürgerinnen und Bürger erfolgt ist.

#### § 2 Stichwahlen

Etwa notwendig werdende Stichwahlen ehrenamtlicher Bürgermeisterinnen und Bürgermeister sowie Ortsvorsteherinnen und Ortsvorsteher finden am 16.
Juni 2019 in der Zeit von 8 bis 18 Uhr statt.

#### § 3 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft und am 1.
Januar 2025 außer Kraft.

Potsdam, den 15.
August 2018

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter