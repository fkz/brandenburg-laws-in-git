## Gesetz über die Feststellung des Haushaltsplanes des Landes Brandenburg für das Haushaltsjahr 2011  (Haushaltsgesetz 2011 - HG 2011)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1   
Feststellung des Haushaltsplanes

Der diesem Gesetz als Anlage beigefügte Haushaltsplan des Landes Brandenburg für das Haushaltsjahr 2011 wird in Einnahmen und Ausgaben festgestellt auf 10 139 987 100 Euro.
Der Gesamtbetrag der Verpflichtungsermächtigungen wird festgestellt auf 1 693 313 300 Euro.

#### § 2  
**Kreditermächtigungen**

(1) Das Ministerium der Finanzen wird ermächtigt, zur Deckung von Ausgaben Kredite aufzunehmen bis zur Höhe von 440 000 000 Euro.

(2) Der Kreditermächtigung nach Absatz 1 wachsen die Beträge zur Tilgung von im Haushaltsjahr 2011 fällig werdenden Krediten zu, deren Höhe sich aus den Finanzierungsübersichten ergibt.

(3) Über die Kreditermächtigung nach Absatz 1 hinaus darf das Ministerium der Finanzen zur Vorfinanzierung von Ausgaben, die aus den Fonds der Europäischen Union nachträglich erstattet werden, Kredite bis zur Höhe von insgesamt 200 000 000 Euro aufnehmen.
Die nach Satz 1 aufgenommenen Kredite sind mit den Erstattungen aus den Fonds zu tilgen.

(4) Im Rahmen der Kreditfinanzierung kann das Ministerium der Finanzen auch ergänzende Vereinbarungen treffen, die der Begrenzung von Zinsänderungsrisiken, der Erzielung günstigerer Konditionen und ähnlichen Zwecken bei neuen Krediten und bestehenden Schulden dienen.
Das Ministerium der Finanzen wird ermächtigt, Darlehen vorzeitig zu tilgen oder Kredite mit unterjähriger Laufzeit aufzunehmen, soweit dies im Zuge von Zinsanpassungen oder zur Erlangung günstigerer Konditionen notwendig wird.
Die Kreditermächtigung nach Absatz 1 erhöht sich in Höhe der nach Satz 2 getilgten Beträge.
Diese Ermächtigung gilt auch für die im Wirtschaftsplan des Landeswohnungsbauvermögens vorgesehene Kreditaufnahme.

(5) Das Ministerium der Finanzen wird ermächtigt, ab Oktober des Haushaltsjahres im Vorgriff auf die Ermächtigung des nächsten Haushaltsjahres Kredite bis zur Höhe von 8 Prozent des in § 1 Satz 1 festgestellten Betrages aufzunehmen.
Die hiernach aufgenommenen Kredite sind auf die Kreditermächtigung des nächsten Haushaltsjahres anzurechnen.

(6) Der Zeitpunkt der Kreditaufnahme ist nach der Kassenlage, den jeweiligen Kapitalmarktverhältnissen und gesamtwirtschaftlichen Erfordernissen zu bestimmen.

(7) Das Ministerium der Finanzen wird ermächtigt, zur Aufrechterhaltung einer ordnungsgemäßen Kassenwirtschaft im Haushaltsjahr 2011 bis zur Höhe von 12 Prozent des in § 1 Satz 1 festgestellten Betrages zuzüglich der nach Absatz 1 noch nicht in Anspruch genommenen Kreditermächtigungen Kassenverstärkungsmittel aufzunehmen.
Soweit diese Kredite zurückgezahlt sind, kann die Ermächtigung wiederholt in Anspruch genommen werden.

#### § 3  
Bürgschaften und Rückbürgschaften

(1) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2011 Bürgschaften für Kredite an die Wirtschaft, die freien Berufe und Einrichtungen der freien Wohlfahrtspflege sowie die Land- und Forstwirtschaft bis zur Höhe von insgesamt 200 000 000 Euro zu übernehmen.

(2) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2011 Bürgschaften für Kredite zur Förderung des Wohnungsbaus und des Stadtumbaus bis zur Höhe von 10 000 000 Euro zu übernehmen.

(3) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2011 zur Absicherung von Krediten an Dritte für Investitionen des Landes im Rahmen von Sonderfinanzierungen nach § 9 Bürgschaften oder Sicherheitserklärungen bis zu einer Höhe von 30 000 000 Euro zugunsten der Investitionsbank des Landes Brandenburg oder der finanzierenden Einrichtungen zu übernehmen.

(4) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2011 Bürgschaften im Falle eines unvorhergesehenen und unabweisbaren Bedürfnisses, insbesondere für Notmaßnahmen im Land Brandenburg, bis zur Höhe von 15 000 000 Euro zu übernehmen.
Überschreitet die aufgrund dieser Ermächtigung zu übernehmende Bürgschaft im Einzelfall den Betrag von 5 000 000 Euro, bedarf es der Einwilligung des Ausschusses für Haushalt und Finanzen des Landtages.

(5) Bürgschaften gemäß den Absätzen 1 bis 3 dürfen nur für Kredite übernommen werden, deren Rückzahlung durch den Schuldner bei normalem wirtschaftlichem Ablauf innerhalb der für den einzelnen Kredit vereinbarten Zahlungstermine erwartet werden kann.

#### § 4  
Garantien und sonstige Gewährleistungen

(1) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2011 im Interesse der Kapitalversorgung kleiner und mittelständischer Unternehmen Garantien bis zur Höhe von 20 000 000 Euro für die Übernahme von Kapitalbeteiligungen zu übernehmen.
Diese Garantien können auch als Rückgarantien gegenüber Kreditinstituten übernommen werden.

(2) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2011 Garantien zur Finanzierung von Film- und Fernsehproduktionen sowie Projektentwicklungen im Medienbereich bis zur Höhe von 15 000 000 Euro zu übernehmen.

(3) Das Ministerium der Finanzen wird ermächtigt, unter Anrechnung auf die Ermächtigungen gemäß Absatz 1 und 2 Garantien zur Finanzierung von Produktionen, Projektentwicklungen und Existenzgründungen im Bereich der Kultur- und Kreativwirtschaft zu übernehmen.

(4) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2011 zur Absicherung von Risiken, die sich aus dem Betrieb von kerntechnischen Anlagen und dem Umgang mit radioaktiven Stoffen in Forschungseinrichtungen des Landes ergeben, Gewährleistung bis zur Höhe von 5 000 000 Euro zu übernehmen.

(5) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2011 zur Deckung des Haftpflichtrisikos von Zuwendungsempfängern des Landes aus der Haftung für Leihgaben im Bereich Kunst und Kultur sowie für wissenschaftliche Forschungsinstitute, die vom Bund und vom Land gemeinsam getragen werden, Garantien bis zum Höchstbetrag von 5 000 000 Euro zu übernehmen.

(6) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2011 zur Absicherung von Risiken, die sich aus der Tätigkeit der Ethikkommission der Landesärztekammer Brandenburg nach _§ 7 Absatz 1 des Heilberufsgeset_zes ergeben, Gewährleistungen bis zur Höhe von 5 000 000 Euro zu übernehmen.

(7) Haftungsfreistellungen und Garantien gemäß den Absätzen 1 und 2 dürfen nur unter den in § 3 Absatz 5 genannten Voraussetzungen übernommen werden.

#### § 5  
Grundsätze für neue Steuerungsinstrumente

(1) In den Einzelplänen 02 bis 12 werden aus den Personalausgaben je Einzelplan Personalbudgets gebildet.
In den Einzelplänen 02 bis 12 sowie im Einzelplan 20 werden aus den sächlichen Verwaltungsausgaben, den Ausgaben für den Erwerb beweglicher Sachen und den Verwaltungseinnahmen je Einzelplan Verwaltungsbudgets gebildet.

(2) Das Personalbudget umfasst mit Ausnahme der Gruppen 432 und 453 die Ausgaben der Hauptgruppe 4.
Diese sind innerhalb des Einzelplans gegenseitig deckungsfähig, davon ausgenommen ist das Kapitel 05 302 (Personalkostenausgleichsfonds).
 Rücklagen aus dem Vorjahr dürfen zur Verstärkung der Ausgaben verwendet werden; vorgezogene Entnahmen im Vorjahr sind durch Minderausgaben im laufenden Haushaltsjahr auszugleichen.
Wird das Personalbudget beim Jahresabschluss über- oder unterschritten, kann der Betrag bis zur Höhe der Über- oder Unterschreitung auf das Personalbudget für den nächsten Haushalt vorgetragen werden.

(3) Die Ausgaben der Gruppe 453 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.
Das jeweilige Personalbudget ist einseitig deckungsfähig zugunsten der Ausgaben der Gruppe 453.
Die Ausgaben der Gruppe 432 sind über alle Einzelpläne gegenseitig deckungsfähig.

(4) Das Verwaltungsbudget umfasst die Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Titel 518 25 und der Gruppe 529, und der Obergruppe 81 und die Einnahmen der Obergruppen 11 bis 13.
Die Ausgaben sind innerhalb des Einzelplans gegenseitig deckungsfähig.
Das jeweilige Verwaltungsbudget ist einseitig deckungsfähig zugunsten des Titels 518 25.
Rücklagen aus Vorjahren dürfen zur Verstärkung der Ausgaben verwendet werden.
Wird das Verwaltungsbudget beim Jahresabschluss über- oder unterschritten, kann der Betrag bis zur Höhe der Über- oder Unterschreitung auf das Verwaltungsbudget für den nächsten Haushalt vorgetragen werden.
Einzelne Einnahmen und Ausgaben können vom Verwaltungsbudget ausgenommen werden.

(5) Mehreinnahmen bei den Obergruppen 11 bis 13 können zur Verstärkung der Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Gruppe 529, und der Obergruppe 81 im Rahmen des Verwaltungsbudgets verwendet werden, wenn ein verwaltungsmäßiger oder sachlicher Zusammenhang besteht oder eine wirtschaftliche und sparsame Verwendung gefördert wird.
Minderausgaben beim Personalbudget können zur Verstärkung der Ausgaben des Verwaltungsbudgets im jeweiligen Einzelplan verwendet werden, soweit sich daraus keine Überschreitung des Personalbudgets beim Jahresabschluss ergibt.

(6) Minderausgaben beim Verwaltungsbudget können zur Verstärkung der Ausgaben bei Kapitel 12 020 Titel 891 61 – Zuführungen für Investitionen – herangezogen werden.

(7) Die allein aus Landesmitteln finanzierten und nicht zur Komplementärfinanzierung von Drittmitteln bestimmten Ausgaben der Hauptgruppe 6 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.
Ebenso sind die allein aus Landesmitteln finanzierten und nicht zur Komplementärfinanzierung von Drittmitteln bestimmten Ausgaben der Obergruppen 83 bis 89 innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.

(8) Für die Wirtschaftspläne der Landesbetriebe nach § 26 Absatz 1 der Landeshaushaltsordnung gelten die vorstehenden Absätze entsprechend, soweit keine besonderen Regelungen getroffen sind.

(9) Die im Einzelplan 06 veranschlagten Universitäten und Fachhochschulen werden jeweils nur mit ihrem Zuschussbedarf veranschlagt.
Die Einnahmen und Ausgaben dieser Einrichtungen werden in Wirtschaftsplänen veranschlagt, die dem Haushaltsplan als Erläuterungen beigefügt sind.
Für die Bewirtschaftung gelten die Absätze 1 bis 6 entsprechend, soweit keine besonderen Regelungen getroffen sind.

(10) Das Nähere regelt das Ministerium der Finanzen.

#### § 6  
Neue Steuerungsinstrumente im Bereich des Landtages, Verfassungsgerichts und Landesrechnungshofes

(1) Gegenseitig deckungsfähig sind innerhalb der Einzelpläne 01, 13 und 14 die Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Titel 518 25 und der Gruppe 529, und der Obergruppe 81.
Das jeweilige Verwaltungsbudget ist einseitig deckungsfähig zugunsten des Titels 518 25.
Werden die Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Gruppe 529, und der Obergruppe 81 beim Jahresabschluss unterschritten, kann der Betrag in Höhe der Unterschreitung einer Rücklage zugeführt werden.
Sofern es beim Jahresabschluss zu einer Überschreitung kommt, kann der Betrag in Höhe der Überschreitung in den nächsten Haushalt vorgetragen werden.
Rücklagen aus dem Vorjahr dürfen zur Verstärkung der entsprechenden Ausgaben verwendet werden.

(2) Nicht verausgabte Mittel der Titelgruppe 99 - Kosten für Datenverarbeitung - können bei Unterschreitung der veranschlagten Ausgaben in Höhe der Unterschreitung einer Rücklage zugeführt werden.
Auf die Bildung dieser Rücklage ist Absatz 1 nicht anzuwenden.
Innerhalb der Titelgruppe 99 dürfen Einnahmen, die der für Datenverarbeitung gebildeten Rücklage entnommen werden, zur Deckung von Mehrausgaben verwendet werden.

(3) Für die Ausgaben der Hauptgruppe 4, mit Ausnahme der Ausgaben der Gruppe 411 - Aufwendungen für Abgeordnete - im Kapitel 01 010 und der Gruppen 432 und 453, wird innerhalb des jeweiligen Einzelplans ein Personalbudget gebildet.
Die Ausgaben sind innerhalb des Personalbudgets gegenseitig deckungsfähig.
Rücklagen aus dem Vorjahr dürfen zur Verstärkung der Ausgaben verwendet werden; vorgezogene Entnahmen im Vorjahr sind durch Minderausgaben im laufenden Haushaltsjahr auszugleichen.
Wird das Personalbudget beim Jahresabschluss über- oder unterschritten, kann der Betrag bis zur Höhe der Über- oder Unterschreitung auf das Personalbudget für den nächsten Haushalt vorgetragen werden.

(4) Die Ausgaben der Gruppe 453 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.
Das jeweilige Personalbudget ist einseitig deckungsfähig zugunsten der Ausgaben der Gruppe 453.
Die Ausgaben der Gruppe 432 sind über alle Einzelpläne gegenseitig deckungsfähig.

(5) Mehreinnahmen bei den Obergruppen 11 bis 13 können zur Verstärkung der Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Gruppe 529, und der Obergruppe 81 verwendet werden, wenn ein verwaltungsmäßiger oder sachlicher Zusammenhang besteht oder eine wirtschaftliche und sparsame Verwendung gefördert wird.
Minderausgaben beim Personalbudget können zur Verstärkung der in Satz 1 bezeichneten Ausgaben im jeweiligen Einzelplan verwendet werden, soweit sich daraus keine Überschreitung des Personalbudgets beim Jahresabschluss ergibt.

#### § 7  
Besondere Regelungen für den Brandenburgischen Landesbetrieb für Liegenschaften und Bauen (BLB)

(1) Das Ministerium der Finanzen wird ermächtigt, nach Bestätigung des Wirtschaftsplans für den Landesbetrieb Einnahmen, Ausgaben, Verpflichtungsermächtigungen, Planstellen und Stellen in den Landesbetrieb umzusetzen, soweit weitere Liegenschaften in die Teilnahme am Vermieter-Mieter-Modell überführt werden.

(2) Die Ansätze bei den Titeln 518 25 sind bis zum Abschluss der jeweiligen Mietverträge mit dem BLB gesperrt.
Von dieser Sperre sind Ausgaben nicht erfasst, die im Zusammenhang mit der Bewirtschaftung der Liegenschaften stehen.

(3) Nicht veranschlagte Ausgaben für Mieten nach dem Vermieter-Mieter-Modell beim Titel 518 25 stellen keine Mehrausgaben nach § 37 der Landeshaushaltsordnung dar.
Sie können vom Ministerium der Finanzen zugelassen werden, wenn sie durch Minderausgaben oder Mehreinnahmen an anderer Stelle gedeckt sind.

(4) Die Ansätze des Titels 518 25 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.

(5) Vom BLB zurückgezahlte Beträge aus der Abrechnung von Betriebs- und Nebenkosten sind bei Titel 518 25 und bei Kapitel 12 020 bei Titel 518 61 abzusetzen.

#### § 8  
Mehrausgaben, Komplementärmittel

(1) Der gemäß § 37 Absatz 1 Satz 4 der Landeshaushaltsordnung festzulegende Betrag wird auf 7 500 000 Euro Landesmittel festgesetzt, für Verpflichtungsermächtigungen (§ 38 Absatz 1 Satz 3 der Landeshaushaltsordnung) als Jahresbetrag.
 Überschreiten Mehrausgaben im Einzelfall den Betrag von 5 000 000 Euro Landesmittel, bei Verpflichtungsermächtigungen als jährlich fällig werdender Betrag, ist die Einwilligung des Ausschusses für Haushalt und Finanzen des Landtages einzuholen.

(2) Eines Nachtragshaushaltsgesetzes bedarf es zudem nicht, wenn

1.  Komplementärmittel von der Europäischen Union oder vom Bund unvorhergesehen bereitgestellt werden, die eine zusätzliche anteilige Finanzierung durch das Land erforderlich machen, oder
    
2.  Umschichtungen innerhalb eines Fonds der Europäischen Union oder zwischen den Fonds, einschließlich der Kofinanzierung durch das Land, erforderlich sind.
    

In den Fällen des Satzes 1 Nummer 2 bedarf es der Einwilligung des Ausschusses für Haushalt und Finanzen, wenn die Umschichtungen im Einzelfall 5 000 000 Euro EU- und Landesmittel, bei Verpflichtungsermächtigungen als jährlich fällig werdender Betrag, überschreiten.

(3) Veranschlagte Landesmittel und Verpflichtungsermächtigungen, die nicht mehr zur Kofinanzierung von Leistungen Dritter für die gemäß Haushaltsplan vorgesehenen Zwecke erforderlich sind, sind gesperrt.
Die Aufhebung der Sperre bedarf der Zustimmung des Ministeriums der Finanzen.
Das Ministerium der Finanzen wird ermächtigt, die Vorfinanzierung von Maßnahmen, für die die Leistung von Dritten vorgesehen ist, zuzulassen.

(4) Im Bereich der Fonds der Europäischen Union dürfen mit Einwilligung des Ministeriums der Finanzen Mehrausgaben bis zur Höhe der Minderausgaben aus Vorjahren geleistet werden, soweit die zugehörigen Erstattungsanträge an die EU-Kommission bis spätestens zum II.
Quartal des Folgejahres gestellt werden oder die Mehrausgaben zur Kofinanzierung von Mitteln aus den Fonds dienen.

#### § 9  
Sonderfinanzierungen

(1) Durch den Abschluss von Leasing-, Mietkauf- und ähnlichen Verträgen (Sonderfinanzierungen) für Bauinvestitionen dürfen Verpflichtungen zulasten künftiger Haushaltsjahre eingegangen werden.
Diese Befugnis gilt auch bei Umsetzung von Bauinvestitionen im Rahmen von Öffentlich Privaten Partnerschaften, die auch die Betriebsphase umfassen (Lebenszyklusansatz).
Das Ministerium der Finanzen wird ermächtigt, mit Zustimmung des Ausschusses für Haushalt und Finanzen des Landtages Sonderfinanzierungen zuzulassen; § 38 Absatz 1 der Landeshaushaltsordnung bleibt unberührt.

(2) Verpflichtungsermächtigungen für Investitionsfinanzierungen dürfen abweichend von § 8 Absatz 1 bis zu der Höhe überschritten werden, in der sie für Maßnahmen nach Absatz 1 Satz 1 benötigt werden.

(3) Die Wirtschaftlichkeit von Sonderfinanzierungen ist in jedem Einzelfall zu belegen.

#### § 10  
Industrieansiedlungsverträge

Soweit die veranschlagten Ausgaben bei voller Ausschöpfung der Deckungsfähigkeit und die Verpflichtungsermächtigungen nicht ausreichen, Industrieansiedlungsverträge mit finanziellen Verpflichtungen für das Land abzuschließen, ist das Ministerium für Wirtschaft und Europaangelegenheiten ermächtigt, über Industrieansiedlungsverträge zu verhandeln und - bei Zustimmung des Ministeriums der Finanzen und nach Einwilligung des Ausschusses für Haushalt und Finanzen im Benehmen mit dem Ausschuss für Wirtschaft des Landtages - zusätzliche Verpflichtungen zulasten des Landes einzugehen.

#### § 11  
Besondere Regelungen für Zuwendungen

(1) Ausgaben und Verpflichtungsermächtigungen für Zuwendungen im Sinne des § 23 der Landeshaushaltsordnung zur Deckung der gesamten Ausgaben oder eines nicht abgegrenzten Teils der Ausgaben einer Stelle außerhalb der Landesverwaltung (institutionelle Förderung) sind gesperrt, bis der Haushalts- oder Wirtschaftsplan des Zuwendungsempfängers von dem zuständigen Ministerium gebilligt worden ist.

(2) Die in Absatz 1 genannten Zuwendungen zur institutionellen Förderung dürfen nur mit der Auflage bewilligt werden, dass der Zuwendungsempfänger seine Beschäftigten nicht besser stellt als vergleichbare Bedienstete des Landes; vorbehaltlich einer abweichenden tarifvertraglichen Regelung dürfen deshalb keine günstigeren Arbeitsbedingungen vereinbart werden, als sie für Bedienstete des Landes jeweils vorgesehen sind.
Entsprechendes gilt bei Zuwendungen zur Projektförderung, wenn die Gesamtausgaben des Zuwendungsempfängers überwiegend aus Zuwendungen der öffentlichen Hand bestritten werden.
Das Ministerium der Finanzen kann bei Vorliegen zwingender Gründe Ausnahmen zulassen.

(3) Die in den Erläuterungen zu den Titeln, aus denen Zuwendungen im Sinne des § 23 der Landeshaushaltsordnung zur institutionellen Förderung geleistet werden, für andere als Projektaufgaben ausgebrachte Planstellen für Beamte sowie Stellen für Arbeitnehmer sind hinsichtlich der Gesamtzahl und der Zahl der für die einzelnen Besoldungs- und Entgeltgruppen ausgebrachten Planstellen und Stellen verbindlich.
Das Ministerium der Finanzen wird ermächtigt, Ausnahmen von der Verbindlichkeit der Stellenpläne zuzulassen.
Die Wertigkeit außertariflicher Stellen ist durch die Angabe der entsprechenden Besoldungsgruppe zu kennzeichnen.
Das Ministerium der Finanzen kann Abweichungen in den Wertigkeiten der Stellen zulassen.
Sind im Wirtschaftsplan Stellen außerhalb der Anlagen B 2 und B 3 zum Tarifvertrag der Länder (TV-L) ohne Angaben des Entgelts ausgebracht, bedarf die Festsetzung des Entgelts in jedem Einzelfall der vorherigen Zustimmung des Ministeriums der Finanzen.
Sonstige Abweichungen bedürfen der Einwilligung des Ministeriums der Finanzen und setzen eine Tätigkeitsdarstellung voraus.

#### § 12  
Personalwirtschaftliche Regelungen

(1) Zur Einhaltung des Stellenplans gemäß der gültigen Personalbedarfsplanung des Landes Brandenburg und des Personalbudgets sind die Ressorts verpflichtet, alle Möglichkeiten zur Einsparung von Planstellen, Stellen, Beschäftigungspositionen und Personalausgaben zu nutzen.
Dazu können abweichend von § 50 Absatz 1 Satz 1 der Landeshaushaltsordnung auch Mittel oder Planstellen und Stellen umgesetzt werden, ohne dass Aufgaben von einer Verwaltung auf eine andere Verwaltung übergehen.
Das Nähere regelt das Ministerium der Finanzen.

(2) Die Erläuterungen zu den Titeln der Gruppe 422 für Stellen der Beamten auf Probe bis zur Anstellung und zu den Titeln der Gruppe 428 sind hinsichtlich der zulässigen Zahl der für die einzelnen Besoldungs- und Entgeltgruppen ausgebrachten Stellen verbindlich.
Die den Wirtschaftsplänen der Landesbetriebe nach § 26 Absatz 1 der Landeshaushaltsordnung beigefügten Stellenübersichten sind verbindlich.
Das Ministerium der Finanzen kann Ausnahmen von der Verbindlichkeit der Stellenpläne für die Landesbetriebe zulassen.

(3) Abweichend von § 49 der Landeshaushaltsordnung können auf Planstellen auch beamtete Hilfskräfte und Arbeitnehmer geführt werden.

(4) Einnahmen aus Zuschüssen für die berufliche Eingliederung behinderter Menschen und für Arbeitsbeschaffungsmaßnahmen fließen den entsprechenden Ansätzen für Personalausgaben zu.
Innerhalb der einzelnen Kapitel fließen die Einnahmen den Ausgaben bei folgenden Titeln - einschließlich den entsprechenden Titeln - in Titelgruppen zu:

1.  Gruppe 428 aus Erstattungen der Förderleistungen der Bundesagentur für Arbeit in Bezug auf das Altersteilzeitgesetz,
    
2.  Gruppen 422, 428, 441, 443 und 446 aus Schadensersatzleistungen Dritter.
    

(5) Planstellen und Stellen können für Zeiträume, in denen Stelleninhaber vorübergehend nicht oder nicht vollbeschäftigt sind, innerhalb des jeweiligen Einzelplans im Umfang der nicht in Anspruch genommenen Planstellen- oder Stellenanteile für die Beschäftigung von beamteten Hilfskräften und Kräften in zeitlich befristeten Arbeitsverträgen in Anspruch genommen werden.

(6) Das Ministerium der Finanzen wird ermächtigt, Planstellen für Lehrkräfte zur Besetzung mit Beamten, für die die Einstufung nach den Brandenburgischen Besoldungsordnungen nicht gilt, nach Maßgabe des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung zu heben.

#### § 13  
Besondere Regelungen für Planstellen und Stellen

(1) Planstellen und Stellen, die einen kw-Vermerk tragen, können nach ihrem Freiwerden mit schwer behinderten Menschen wiederbesetzt werden, wenn die gesetzliche Pflichtquote gemäß § 71 Absatz 1 des Neunten Buches Sozialgesetzbuch bei den Planstellen und Stellen in der Landesverwaltung nicht erreicht wird.
In diesem Fall ist der schwer behinderte Mensch auf der nächsten freiwerdenden Planstelle oder Stelle der betreffenden oder nächsthöheren Besoldungs- oder Entgeltgruppe innerhalb des Einzelplans zu führen.
Das Ministerium der Finanzen kann Ausnahmen zulassen.

(2) Das Ministerium der Finanzen wird ermächtigt zuzulassen, dass von einem kw-Vermerk mit Datumsangabe abgewichen wird, wenn die Planstelle oder Stelle weiter benötigt wird, weil sie nicht rechtzeitig frei wird; in diesem Fall ist der Stelleninhaber auf der nächsten freiwerdenden Planstelle oder Stelle der betreffenden oder nächsthöheren Besoldungs- oder Entgeltgruppe innerhalb des Einzelplans zu führen.

(3) Das Ministerium der Finanzen wird ermächtigt, mit Einwilligung des Ausschusses für Haushalt und Finanzen des Landtages Planstellen für Beamte, Richter und Stellen für Arbeitnehmer zusätzlich auszubringen, wenn hierfür ein unabweisbares, auf andere Weise nicht zu befriedigendes Bedürfnis besteht.

(4) Mit Einwilligung des Ministeriums der Finanzen können nach Änderungen im Besoldungs- oder Tarifrecht Planstellen- und Stellenveränderungen vorgenommen werden.
 Stellenveränderungen sind mit Einwilligung des Ministeriums der Finanzen auch dann möglich, wenn tarifrechtliche Ansprüche bestehen.

(5) Arbeitnehmer, die vor der Überleitung aus dem BAT/BAT-O und dem MTArb/MTArb-O in den TV-L einen Bewährungs- oder Fallgruppenaufstieg gemäß den §§ 23a, 23b BAT/BAT-O beziehungsweise den vergleichbaren Bestimmungen für Arbeiter vollzogen haben oder bei denen nach den bisherigen tarifrechtlichen Bestimmungen ein Bewährungs- oder Fallgruppenaufstieg in der jeweiligen Fallgruppe vorgesehen war, sowie nach dem 1.
November 2006 neu eingestellte oder neu eingruppierte Arbeitnehmer mit einem höherwertigen Tarifanspruch gemäß Anlage 4 TVÜ-Länder können bis zum Wirksamwerden neuer Eingruppierungsvorschriften für den TV-L oder bis zum Ausscheiden auf einer niedrigwertigeren TV-L-Stelle geführt werden, die der ursprünglichen Stelle in der Struktur des durch den TV-L ersetzten BAT/BAT-O und des MTArb/MTArb-O entspricht.

(6) Das Nähere regelt das Ministerium der Finanzen.

#### § 14  
Ausbringung zusätzlicher Leerstellen

(1) Werden planmäßige Beamte, Richter und Arbeitnehmer im dienstlichen Interesse des Landes mit Zustimmung der obersten Dienstbehörde im Dienst einer öffentlichen zwischenstaatlichen oder überstaatlichen Einrichtung, einer Bundesbehörde oder einer kommunalen Gebietskörperschaft oder für eine Tätigkeit bei einer Fraktion oder einer Gruppe des Landtages, des Deutschen Bundestages oder einer zwischenstaatlichen Einrichtung unter Wegfall der Dienstbezüge länger als ein Jahr verwendet und besteht ein unabweisbares Bedürfnis, die Planstellen und Stellen neu zu besetzen, so kann das Ministerium der Finanzen dafür gleichwertige Leerstellen ausbringen.
Das Gleiche gilt für eine Verwendung bei sonstigen landesunmittelbaren und -mittelbaren juristischen Personen des öffentlichen Rechts sowie bei juristischen Personen des Privatrechts, soweit diese vom Land institutionell gefördert werden oder das Land mehrheitlich beteiligt ist.

(2) Absatz 1 findet entsprechend Anwendung, wenn Beamte nach § 80 Absatz 1 Satz 1 Nummer 2 des Landesbeamtengesetzes länger als ein Jahr beurlaubt werden oder wenn die Rechte und Pflichten aus dem Dienstverhältnis nach § 72 Satz 1 des Landesbeamtengesetzes ruhen.

(3) Für planmäßige Beamte außerhalb der Schulkapitel, die nach § 71 des Landesbeamtengesetzes länger als ein Jahr ohne Unterbrechung Elternzeit nehmen, gilt vom Beginn der Beurlaubung an eine Leerstelle der entsprechenden Besoldungsgruppe als ausgebracht.
Satz 1 gilt auch für die Beurlaubung von Richtern aus familiären Gründen gemäß § 5 des Brandenburgischen Richtergesetzes.

(4) Die Absätze 2 und 3 gelten entsprechend für Richter und Arbeitnehmer.

(5) Für planmäßige Beamte, Richter und Arbeitnehmer, die im Rahmen der Umsetzung der Altersteilzeitregelung am Blockmodell teilnehmen, gilt vom Beginn der Freistellungsphase an eine Leerstelle der entsprechenden Besoldungs- und Entgeltgruppe als ausgebracht.
Zum Zeitpunkt des Übergangs in den Ruhestand fällt diese Leerstelle weg.
Diese Beschäftigten sind bis zum Ausscheiden auf diesen Leerstellen zu führen.

(6) Über den weiteren Verbleib der nach den Absätzen 1 bis 5 ausgebrachten Leerstellen ist im nächsten Haushaltsplan zu entscheiden.

#### § 15  
Vergabe leistungsbezogener Besoldungselemente an Beamte

(1) Für die Vergabe von Leistungsstufen ist die Brandenburgische Leistungsstufenverordnung sowie für die Vergabe von Leistungsprämien und Leistungszulagen die Brandenburgische Leistungsprämien- und -zulagenverordnung anzuwenden.

(2) Innerhalb eines Kapitels dürfen Zulagen für eine befristete Übertragung einer herausgehobenen Funktion nach § 45 des Bundesbesoldungsgesetzes für Beamte bis zur Höhe von 0,1 Prozent der Ausgaben der Titel 422 10 geleistet werden.
In den Einzelplänen 02 bis 12 dürfen die Zulagen nur im Einvernehmen mit dem Ministerium der Finanzen gewährt werden.
Das Ministerium der Finanzen kann hinsichtlich der Ausgabenhöhe in Satz 1 Ausnahmen zulassen.

(3) Die für die Vergabe leistungsbezogener Besoldungselemente anfallenden Ausgaben sind aus Einsparungen bei anderen Titeln der Hauptgruppe 4 im jeweiligen Einzelplan (ausgenommen Gruppen 432 und 453) oder durch Entnahmen aus der Rücklage Personalbudget zu decken.

#### § 16  
Verbilligte Veräußerung und Nutzungsüberlassung von Grundstücken

(1) Grundstücke des Allgemeinen Grundvermögens dürfen gemäß § 63 Absatz 3 Satz 2 und Absatz 4 der Landeshaushaltsordnung

1.  bei der Nutzungsbindung von mindestens 15 Jahren für Einrichtungen des Sozial-, Kinder- und Jugendwesens in gemeinnütziger Trägerschaft um bis zu 25 Prozent unter dem vollen Wert veräußert werden;
    
2.  bebaut (mit besonderem Sanierungsaufwand) und unbebaut bei einer Belegungsbindung von mindestens 15 Jahren um bis zu 40 Prozent unter dem vollen Wert veräußert werden, wenn sichergestellt ist, dass sie für Maßnahmen der sozialen Wohnraumförderung nach § 2 des Wohnraumförderungsgesetzes verwendet werden;
    
3.  bei einer Belegungsbindung von mindestens 15 Jahren um bis zu 50 Prozent unter dem vollen Wert veräußert werden, wenn sichergestellt ist, dass sie im Rahmen des vom Land geförderten Studentenwohnraumbaus zur Schaffung von Studentenwohnungen oder einer vergleichbaren Förderung verwendet werden.
    Unter den gleichen Voraussetzungen können bebaute und unbebaute Grundstücke an Studentenwerke unentgeltlich abgegeben werden;
    
4.  im Wege der Bestellung eines Erbbaurechts vergeben werden, wobei der Erbbauzins je nach dem zu fördernden Zweck für die Dauer der Nutzungs- und Belegungsbindung abgesenkt werden darf, und zwar
    
    1.  für die gemeinnützigen außeruniversitären Forschungseinrichtungen auf 0 Prozent, wobei der Erbbauzins nach Ablauf von jeweils zehn Jahren um jeweils 1 Prozent erhöht werden kann,
        
    2.  in den Fällen der Nummer 1 auf 2 Prozent,
        
    3.  in den Fällen der Nummer 2 auf 3 Prozent und
        
    4.  in den Fällen von Nummer 3 Satz 2 auf 0 Prozent für die ersten zehn Jahre, 1 Prozent für die folgenden zehn Jahre und so fortlaufend bis zu 3 Prozent nach 30 Jahren ausgehend vom Bodenwert.
        In den Fällen von Nummer 3 Satz 1 auf 3 Prozent vom Bodenwert;
        
5.  vom Land institutionell geförderten außeruniversitären Forschungseinrichtungen gegen Übernahme der Betriebs- und zumutbaren Bauunterhaltungskosten unentgeltlich zur Nutzung überlassen werden.
    

(2) Für die nach dem Gesetz über die Verwertung der Liegenschaften der Westgruppe der Truppen in der Titelgruppe 65 „WGT-Liegenschaftsvermögen im AGV“ im Kapitel 20 630 ausgewiesene Vermögensmasse gilt über die Regelung des Absatzes 1 hinaus, dass bebaute und unbebaute Grundstücke um bis zu 25 Prozent unter dem vollen Wert veräußert oder im Erbbaurecht vergeben werden dürfen, die für unmittelbare Verwaltungszwecke vom Land sowie für kommunale Infrastrukturmaßnahmen von den Kreisen und den Gemeinden dauerhaft genutzt werden können.

(3) Über die Verbilligungen gemäß Absatz 1 hinaus wird gemäß § 63 Absatz 3 Satz 2 und Absatz 4 der Landeshaushaltsordnung zugelassen, dass landeseigene bebaute und unbebaute Grundstücke an Gebietskörperschaften für die im Bundeshaushalt aufgeführten Zwecke bis zu dem Prozentsatz unter dem vollen Wert veräußert, im Wege der Erbbaurechtsbestellung zur Verfügung gestellt, vermietet, verpachtet oder zur Nutzung überlassen werden, zu dem der Bund dem Land Verbilligungen bei der Veräußerung, Zurverfügungstellung im Wege des Erbbaurechts, Vermietung, Verpachtung oder Nutzungsüberlassung von bundeseigenen Grundstücken für gleiche Zwecke einräumt.
Vom Gegenseitigkeitserfordernis nach Satz 1 sind die Liegenschaften, die in der Titelgruppe 65 „WGT-Liegenschaftsvermögen im AGV“ im Kapitel 20 630 ausgewiesen sind, ausgenommen.

(4) Gemäß § 61 Absatz 1 Satz 1, § 63 Absatz 3 Satz 2 und Absatz 4 der Landeshaushaltsordnung wird die vorübergehende oder dauernde Abgabe von Grundstücken des Allgemeinen Grundvermögens an das Verwaltungsgrundvermögen ohne Werterstattung zugelassen; dies gilt nicht für Grundstücke, die zur in der Titelgruppe 65 „WGT-Liegenschaftsvermögen im AGV“ im Kapitel 20 630 ausgewiesenen Vermögensmasse gehören.

#### § 17  
Besondere Regelungen für geheim zu haltende Ausgaben

(1) Aus zwingenden Gründen des Geheimschutzes wird die Bewilligung von Ausgaben, die nach einem geheim zu haltenden Wirtschaftsplan bewirtschaftet werden sollen, von der Billigung des Wirtschaftsplans durch die Parlamentarische Kontrollkommission nach § 23 des Brandenburgischen Verfassungsschutzgesetzes abhängig gemacht.
Die Mitglieder dieser Kontrollkommission sind zur Geheimhaltung aller Angelegenheiten verpflichtet, die ihnen bei dieser Tätigkeit bekannt geworden sind.

(2) Der Präsident des Landesrechnungshofes prüft in den Fällen des Absatzes 1 nach § 9 des Landesrechnungshofgesetzes und unterrichtet die Parlamentarische Kontrollkommission sowie die zuständige oberste Landesbehörde und das Ministerium der Finanzen über das Ergebnis ihrer Prüfung der Jahresrechnung sowie der Haushalts- und Wirtschaftsführung.
§ 97 Absatz 4 der Landeshaushaltsordnung bleibt unberührt.

#### § 18  
Berichtspflichten gegenüber dem Ausschuss für Haushalt und Finanzen des Landtages

(1) Das Ministerium der Finanzen berichtet dem Ausschuss für Haushalt und Finanzen des Landtages

1.  mit Stand 30.
    Juni 2011 im Rahmen eines Berichtes über wesentliche Kenngrößen der bereinigten Gesamteinnahmen und -ausgaben des Landes sowie über den aktuellen Mittelabfluss aus dem Landeshaushalt.
    In diesem Bericht sollen auch Angaben zur Entwicklung der Einnahmearten und der Ausgabearten insbesondere zur Umsetzung der EU-Fonds und zum Stand der Verschuldung sowie Prognosedaten der weiteren Entwicklung bis zum Jahresende enthalten sein;
    
2.  über den Jahresabschluss 2011 im Rahmen eines Berichtes wie in Nummer 1 allerdings ohne Prognoseaussage;
    
3.  mit Stand 31.
    Dezember 2011 bis zum 31.
    März 2012 über die Gewährung und Inanspruchnahme von Bürgschaften, Rückbürgschaften, Garantien und sonstigen Gewährleistungen durch das Land gemäß den §§ 3 und 4;
    
4.  mit Stand 31.
    Dezember 2011 im Rahmen eines Berichtes über die Beteiligungen des Landes.
    

(2) Die Ministerien berichten dem Ausschuss für Haushalt und Finanzen des Landtages

1.  zu den in Absatz 1 Nummer 1 und 2 genannten Stichtagen im Rahmen eines Berichtes über den Stand der Bewilligungen bei sämtlichen Titeln der Hauptgruppen 6 und 8 mit einem Ansatz ab 1 000 000 Euro und den aktuellen Mittelabfluss,
    
2.  zu den in Absatz 1 Nummer 1 und 2 genannten Stichtagen im Rahmen eines Berichtes über die Inanspruchnahme von Verpflichtungsermächtigungen,
    
3.  zu den in Absatz 1 Nummer 1 und 2 genannten Stichtagen im Rahmen eines Berichtes über die Inanspruchnahme von Ausgaberesten bei sämtlichen Titeln der Hauptgruppen 6 und 8 mit einem Ansatz ab 1 000 000 Euro,
    
4.  mit Stand 31.
    Mai 2011 im Rahmen eines Berichtes über die Besetzung der Planstellen und Stellen.
    

(3) Die Ministerien berichten dem Ausschuss für Haushalt und Finanzen des Landtages

1.  mit Stand 30.
    Juni 2011 zum 1.
    August 2011 im Rahmen eines Berichtes über den Stand der Entgeltzahlungen an die Investitionsbank des Landes Brandenburg im Zusammenhang mit der Wahrnehmung der Geschäftsbesorgung für die Bewilligung, Gewährung von Zuwendungen und zur Verwendungsnachweisprüfung,
    
2.  mit Stand 31.
    Dezember 2011 zum 1.
    Februar 2012 im Rahmen eines Berichtes wie in Nummer 1.
    

(4) Das Ministerium für Wirtschaft und Europaangelegenheiten berichtet dem Ausschuss für Haushalt und Finanzen des Landtages

1.  zum 30.
    Juni 2011 im Rahmen eines Berichtes über den Stand der Bewilligung von Fördermitteln aus der Gemeinschaftsaufgabe "Verbesserung der regionalen Wirtschaftsstruktur".
    Der Bericht erfolgt in Form einer Übersicht der bewilligten Einzelförderungen mit einem Förderbetrag von mehr als 1 000 000 Euro.
    In der Übersicht sind die der Bewilligung zugrunde gelegten Kriterien und der Fördersatz anzugeben;
    
2.  zum 30.
    September 2011 im Rahmen eines Berichtes wie in Nummer 1;
    
3.  zum 31.
    Dezember 2011 im Rahmen eines Berichtes wie in Nummer 1.
    

#### § 19  
Weitergeltung von Vorschriften und Ermächtigungen

Die Vorschriften und Ermächtigungen in den §§ 3, 4, 5, 6, 8 Absatz 1 und 2, §§ 11 bis 15 und 17 gelten bis zur Verkündung des Haushaltsgesetzes 2012 weiter.

#### § 20   
Inkrafttreten

Dieses Gesetz tritt am 1.
 Januar 2011 in Kraft.

Potsdam, den 20.
Dezember 2010

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

**Anlage**

**Haushaltsplan des Landes Brandenburg  
für das Haushaltsjahr 2011**  
  
**Gesamtplan**

I.             Haushaltsübersicht                                               (§ 13 Absatz 4 Nummer 1 LHO)

A.            Zusammenfassung der Einnahmen und Ausgaben je Einzelplan

B.            Zusammenfassung der Verpflichtungsermächtigungen je Einzelplan

II.            Finanzierungsübersicht                                          (§ 13 Absatz 4 Nummer 2 LHO)

III.           Kreditfinanzierungsplan                                        (§ 13 Absatz 4 Nummer 3 LHO)

  
 

**Teil I Haushaltsübersicht 2011**

A.
Zusammenfassung der Einnahmen und Ausgaben der Einzelpläne

**Einnahmen**

**Ausgaben**

**0**

**1**

**2**

**3**

**4**

**Einzelplan /  
Ressort**

**Einnahmen aus  
Steuern und  
steuerähnlichen  
Abgaben**

**Verwaltungs-  
einnahmen,  
Einnahmen aus  
Schuldendienst  
und dgl.**

**Einnahmen aus  
Zuweisungen  
und Zuschüssen  
mit Ausnahme  
für  
Investitionen**

**Einnahmen aus  
Schuldenauf-  
nahmen, aus  
Zuweisungen  
und Zuschüssen  
für Investitionen,  
besondere  
Finanzierungs-  
einnahmen**

**Summe  
  
Einnahmen**

**Personal-  
ausgaben**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**1**

**2**

**3**

**4**

**5**

**6**

**7**

**01 / LT**

4.500

**4.500**

22.047.200

**02 / Stk**

13.600

83.700

664.000

**761.300**

10.555.800

**03 / MI**

47.523.200

9.573.100

**57.096.300**

433.277.500

**04 / MdJ**

108.574.400

25.482.800

9.400.900

**143.458.100**

243.308.000

**05 / MBJS**

2.570.800

23.401.600

22.064.000

**48.036.400**

983.133.300

**06 / MWFK**

6.853.800

99.926.100

76.365.400

**183.145.300**

31.023.600

**07 / MASF**

14.112.100

106.358.000

24.639.400

**145.109.500**

46.884.000

**08 / MWE**

16.334.600

6.395.200

400.329.700

**423.059.500**

24.077.700

**10 / MUGV**

34.808.400

805.900

60.408.300

**96.022.600**

78.838.900

**11 / MIL**

664.600

8.190.200

529.013.600

316.505.800

**854.374.200**

61.724.000

**12 / MdF**

20.898.200

22.049.300

13.854.800

**56.802.300**

167.003.000

**13 / LRH**

25.500

2.102.600

**2.128.100**

9.949.000

**14 / LVG**

600

**600**

367.900

**20 / AFV**

5.132.500.000

85.468.400

2.269.645.200

642.374.800

**8.129.988.400**

120.393.000

**Summe 2011**

**5.133.164.600**

**345.378.300**

**3.092.734.500**

**1.568.709.700**

**10.139.987.100**

**2.232.582.900**

**Summe 2010**

**4.805.964.600**

**334.210.900**

**3.251.596.400**

**2.119.716.800**

**10.511.488.700**

**2.223.592.900**

Vgl.
zu 2010

+327.200.000

+11.167.400

\-158.861.900

\-551.007.100

\-371.501.600

+8.990.000

  
**Teil I Haushaltsübersicht 2011**

A.
Zusammenfassung der Einnahmen und Ausgaben der Einzelpläne

**Ausgaben**

**5**

**6**

**7**

**8**

**9**

**Einzelplan /  
Ressort**

**Sächliche  
Verwaltungs-  
ausgaben und  
Ausgaben für  
den  
Schuldendienst**

**Ausgaben für  
Zuweisungen  
und  
Zuschüsse  
mit  
Ausnahme  
für  
Investitionen**

**Baumaßnahmen**

**Sonstige  
Ausgaben für  
Investitionen  
und  
Investitions-  
förderungs-  
maßnahmen**

**Besondere  
Finanzierungs-  
ausgaben**

**Summe  
  
Ausgaben**

**\+ Überschuss  
  
\- Zuschuss**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**1**

**8**

**9**

**10**

**11**

**12**

**13**

**14**

**01 / LT**

3.382.900

6.877.700

43.500

202.600

**32.553.900**

\-32.549.400

**02 / Stk**

3.360.400

271.300

68.300

**14.255.800**

\-13.494.500

**03 / MI**

162.318.200

16.276.300

3.053.200

23.537.100

3.865.800

**642.328.100**

\-585.231.800

**04 / MdJ**

149.669.800

31.301.300

19.100

4.488.600

2.182.000

**430.968.800**

\-287.510.700

**05 / MBJS**

12.818.800

421.085.300

11.517.000

10.762.000

**1.439.316.400**

\-1.391.280.000

**06 / MWFK**

9.736.200

542.478.000

74.541.500

3.645.500

**661.424.800**

\-478.279.500

**07 / MASF**

8.772.300

579.520.700

5.343.000

18.511.400

**659.031.400**

\-513.921.900

**08 / MWE**

16.709.700

101.759.600

600.000

363.319.000

113.400

**506.579.400**

\-83.519.900

**10 / MUGV**

39.371.300

83.764.700

18.384.300

141.216.800

\-4.819.900

**356.756.100**

\-260.733.500

**11 / MIL**

43.711.800

783.718.400

28.960.000

452.721.700

\-2.082.900

**1.368.753.000**

\-514.378.800

**12 / MdF**

37.003.700

40.939.800

114.804.200

485.800

**360.236.500**

\-303.434.200

**13 / LRH**

1.359.900

2.000

124.000

20.900

**11.455.800**

\-9.327.700

**14 / LVG**

216.900

**584.800**

\-584.200

**20 / AFV**

779.991.300

2.278.681.900

462.158.100

14.518.000

**3.655.742.300**

+4.474.246.100

**Summe 2011**

**1.268.423.200**

**4.886.677.000**

**51.016.600**

**1.653.882.800**

**47.404.600**

**10.139.987.100**

**0**

**Summe 2010**

**1.242.276.500**

**4.821.039.600**

**60.186.400**

**1.775.949.000**

**388.444.300**

**10.511.488.700**

**0**

Vgl.
zu 2010

+26.146.700

+65.637.400

\-9.169.800

\-122.066.200

\-341.039.700

\-371.501.600

0

 **Teil I Haushaltsübersicht 2011**

B.
Zusammenfassung der Verpflichtungsermächtigungen der Einzelpläne und deren Inanspruchnahme

**Einzel-  
plan**

**Bezeichnung**

**Verpflich-  
tungs-  
ermächti-  
gungen**

**durch die Verpflichtungsermächtigung  
entstehende Rechtsverpflichtungen**

**2011**

**2012**

**2013**

**2014**

**2015 ff.**

**1.000 EUR**

**1**

**2**

**3**

**4**

**5**

**6**

**7**

**01**

Landtag

**02**

Ministerpräsident und Staatskanzlei

2.100,0

1.200,0

900,0

**03**

Ministerium des Innern

37.116,0

30.151,0

5.965,0

1.000,0

**04**

Ministerium der Justiz

803,6

401,8

401,8

**05**

Ministerium für Bildung, Jugend und Sport

13.015,0

5.555,0

6.870,0

500,0

90,0

**06**

Ministerium für Wissenschaft, Forschung und Kultur

26.781,0

18.685,0

5.396,0

1.400,0

1.300,0

**07**

Ministerium für Arbeit, Soziales, Frauen und Familie

98.888,1

67.925,6

23.474,5

7.488,0

**08**

Ministerium für Wirtschaft und Europaangelegenheiten

435.222,0

142.096,9

162.723,6

130.401,5

**10**

Ministerium für Umwelt, Gesundheit und Verbraucherschutz

90.984,7

56.179,5

22.903,3

11.436,7

465,2

**11**

Ministerium für Infrastruktur und Landwirtschaft

756.602,9

309.545,0

169.596,0

98.682,9

178.779,0

**12**

Ministerium der Finanzen

190.800,0

80.820,0

72.000,0

37.980,0

**13**

Landesrechnungshof

**14**

Verfassungsgericht des Landes Brandenburg

**20**

Allgemeine Finanzverwaltung

41.000,0

27.000,0

12.000,0

2.000,0

**Zusammen**

**1.693.313,3**

**739.559,8**

**482.230,2**

**290.889,1**

**180.634,2**

  
**Teil II Finanzierungsübersicht 2011**

**Insgesamt  
2011  
(Mio EUR)**

**I.**

**HAUSHALTSVOLUMEN**

**10.140,0**

**II.**

**ERMITTLUNG DES FINANZIERUNGSSALDOS**

**1.**

**Ausgaben**

**10.085,3**

(ohne Ausgaben zur Schuldentilgung am Kreditmarkt, Zuführungen an Rücklagen, Ausgaben zur Deckung eines kassenmäßigen Fehlbetrags und haushaltstechnische Verrechnungen)

**2.**

**Einnahmen**

**9.526,7**

(ohne Einnahmen aus Krediten vom Kreditmarkt, Entnahmen aus Rücklagen,  
Einnahmen aus kassenmäßigen Überschüssen und haushaltstechnischen Verrechnungen)

**3.**

**Finanzierungssaldo**

**\-558,6**

**III.**

**AUSGLEICH DES FINANZIERUNGSSALDOS**

**4.**

**Nettoneuverschuldung am Kreditmarkt**

**440,0**

4.1

Einnahmen aus Krediten vom Kreditmarkt (brutto)

3.289,1

4.2

Ausgaben zur Schuldentilgung am Kreditmarkt

\-2.849,1

4.21

planmäßige Tilgungen

\-2.849,1

4.22

mögliche vorzeitige Tilgungen

0,0

4.23

Tilgungen kurzfristiger Schulden

0,0

**5.**

**Rücklagenbewegung**

**118,6**

5.1

Entnahmen aus Rücklagen

172,2

5.2

Zuführungen an Rücklagen

\-53,6

**6.**

**Abwicklung der Vorjahre**

**0,0**

6.1

Ausgaben zur Deckung kassenmäßiger Fehlbeträge

0,0

6.2

Einnahmen aus kassenmäßigen Überschüssen

\--

**7.**

**Haushaltstechnische Verrechnungen**

**0,0**

7.1

Ausgaben

\-1,1

7.2

Einnahmen

1,1

**zusammen**

**558,6**

Abweichungen in den Summen ergeben sich durch Runden der Zahlen

**Teil III Kreditfinanzierungsplan 2011**

**Insgesamt  
2011  
(Mio EUR)**

**I.**

**EINNAHMEN AUS KREDITEN**

bei Gebietskörperschaften, Sondervermögen usw.

\--

vom Kreditmarkt

3.289,1

Zusammen

3.289,1

**II.**

**TILGUNGSAUSGABEN FÜR KREDITE**

bei Gebietskörperschaften, Sondervermögen usw.

**\--**

vom Kreditmarkt

2.849,1

Zusammen

2.849,1

**III.**

**NETTONEUVERSCHULDUNG insgesamt**

bei Gebietskörperschaften, Sondervermögen usw.

**\--**

vom Kreditmarkt

440,0

Zusammen

440,0

Abweichungen in den Summen ergeben sich durch Runden der Zahlen