## Verordnung über die elektronische Führung des Handels-, Genossenschafts-, Partnerschafts- und Vereinsregisters (Registerverordnung - RegV)

Abschnitt 1  
Vereinsregister
-----------------------------

### § 1   
Elektronisches Vereinsregister

Das Vereinsregister sowie das zu seiner Führung erforderliche Verzeichnis werden elektronisch geführt.

### § 2   
Elektronische Aktenführung

(1) In Vereinsregistersachen werden die Akten bei den Amtsgerichten ab dem 1. Dezember 2014 elektronisch geführt.

(2) Die am 1.
Dezember 2014 in Papierform angelegten Akten werden ab diesem Tag in elektronischer Form geführt.
Akten, die nach diesem Tag in Papierform von anderen Gerichten übernommen werden, werden ab dem Zeitpunkt des Eingangs bei dem aufnehmenden Gericht in elektronischer Form geführt.
Eine rückwärtige Erfassung findet nur in Ausnahmefällen statt.

### § 3   
Eingehende Papierdokumente

(1) In Papierform eingereichte Schriftstücke und sonstige Unterlagen sollen zur Ersetzung der Urschrift in ein elektronisches Dokument übertragen werden.
Es ist sicherzustellen, dass das elektronische Dokument mit den eingereichten Schriftstücken und sonstigen Unterlagen bildlich und inhaltlich übereinstimmt.
Im Übrigen gilt § 298a Absatz 3 der Zivilprozessordnung entsprechend.

(2) Nicht rückgabepflichtige Schriftstücke und sonstige Unterlagen sind nach der Übertragung zu vernichten.
Das gilt nicht für Zahlungsanzeigen der Landeskasse und Kostenrechnungen; diese werden je Registerakte in einem gesonderten Kostenheft geführt.

(3) Rückgabepflichtige Schriftstücke und sonstige Unterlagen werden bis zur Rückgabe in einem besonderen Heft verwahrt.
In der elektronischen Akte ist auf das besondere Heft hinzuweisen.

### § 4  
Beschwerdeverfahren

(1) Wird der Beschwerde nicht abgeholfen, gewährt das Registergericht dem Beschwerdegericht Zugang zur elektronischen Registerakte.
Anderenfalls fertigt das Registergericht von allen elektronisch vorliegenden Dokumenten und Registerauszügen Ausdrucke und nimmt diese zu einer in Papierform anzulegenden Beschwerdeakte, soweit dies zur Durchführung des Beschwerdeverfahrens notwendig ist.
§ 298 Absatz 2 der Zivilprozessordnung gilt entsprechend.
Nach rechtskräftigem Abschluss des Beschwerdeverfahrens ist die elektronische Registerakte gemäß § 3 zu vervollständigen.

(2) In Papierform eingereichte Schriftstücke und die Ausdrucke gemäß Absatz 1 Satz 2 und 3 sind mindestens bis zum rechtskräftigen Abschluss des Beschwerdeverfahrens aufzubewahren.

### § 5  
Abgabe von Registerakten

(1) Ist aufgrund einer Sitzverlegung die elektronische Akte an das Gericht des neuen Sitzes zu versenden, bei dem der elektronische Rechtsverkehr auch insoweit eröffnet ist, so ist der Sitzverlegungsantrag mitsamt allen dazugehörigen Dokumenten an dieses Gericht elektronisch zu übermitteln.
Der Inhalt der elektronischen Registerakte wird elektronisch übermittelt, wenn das Gericht des neuen Sitzes die Registerakten elektronisch führt.

(2) Anderenfalls sind die Bestandteile der elektronischen Registerakte einschließlich der in Absatz 1 genannten Dokumente auszudrucken, zu beglaubigen und mit einer gegebenenfalls noch vorhandenen Papierakte zu einer vollständigen Registerakte in Papierform zusammenzuführen.
§ 298 Absatz 2 der Zivilprozessordnung gilt entsprechend.
Sind weitere Papierakten dazu vorhanden, so sind diese mit zu übersenden.

(3) Mit Eingang der Nachricht von der Eintragung in das Register des dann zuständigen Registergerichts und der Eintragung der Sitzverlegung beim bisherigen Registergericht ist die Beauskunftung des Registerordners zu sperren.
 Registerordner und Registerakte beim bisherigen Registergericht sind zu schließen.

(4) Die vorstehenden Absätze gelten entsprechend für den Fall einer Umwandlung.

### § 6   
Anforderung von Akten

(1) Ist aufgrund einer Anforderung die Registerakte an ein anderes Gericht, eine Behörde oder eine Kammer zu übersenden, kann die Akte auf Anforderung in elektronischer Form übersandt werden, soweit dies technisch möglich ist.

(2) Anderenfalls sind die Bestandteile der elektronischen Akte auszudrucken, zu beglaubigen und mit einer gegebenenfalls noch vorhandenen Papierakte zu einer vollständigen Akte in Papierform zusammenzuführen.
Sind weitere Papierakten dazu vorhanden, so sind diese mit zu übersenden.
Nach der Rückkehr ist die elektronische Akte gemäß § 3 zu vervollständigen.

Abschnitt 2   
Handels-, Genossenschafts-, Partnerschafts- und Vereinsregister
------------------------------------------------------------------------------

### § 7   
Datenübermittlung an andere Amtsgerichte

Die Daten der elektronisch geführten Register können an andere Amtsgerichte übermittelt und auch dort zur Einsicht und zur Erteilung von Ausdrucken bereitgehalten werden.

### § 8   
Ersatzregister

Zuständige Stelle im Sinne von § 54 Absatz 1 Satz 1 der Handelsregisterverordnung, § 1 der Genossenschaftsregisterverordnung in Verbindung mit § 54 Absatz 1 Satz 1 der Handelsregisterverordnung, § 1 Absatz 1 der Partnerschaftsregisterverordnung in Verbindung mit § 54 Absatz 1 Satz 1 der Handelsregisterverordnung und § 38 Absatz 1 Satz 1 der Vereinsregisterverordnung ist die Behördenleitung des zuständigen Amtsgerichts.