## Verordnung zur Bestimmung der Stadt Königs Wusterhausen zur Großen kreisangehörigen Stadt

Auf Grund des § 1 Absatz 3 der Kommunalverfassung des Landes Brandenburg vom 18. Dezember 2007 (GVBl. I S. 286) verordnet der Minister des Innern und für Kommunales:

#### § 1 Statusregelung

Die kreisangehörige Stadt Königs Wusterhausen wird zur Großen kreisangehörigen Stadt bestimmt.

#### § 2 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 15.
Mai 2019

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter