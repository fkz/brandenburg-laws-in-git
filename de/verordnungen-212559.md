## Verordnung über die zuständigen Behörden für die Aufgaben nach § 6b des Bundeskindergeldgesetzes (BKGG-Zuständigkeitsverordnung)

Auf Grund des § 13 Absatz 4 des Bundeskindergeldgesetzes in der Fassung der Bekanntmachung vom 28.
Januar 2009 (BGBl.
I S. 142, 3177), der durch Artikel 5 des Gesetzes vom 24.
März 2011 (BGBl.
I S. 453, 490) eingefügt worden ist, in Verbindung mit § 9 Absatz 2 und § 16 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186) verordnet die Landesregierung:

#### § 1  
Zuständige Behörden

(1) Zuständige Behörden für die Durchführung der Aufgaben nach § 6b des Bundeskindergeldgesetzes sind die Landkreise und kreisfreien Städte.

(2) Die Landkreise und kreisfreien Städte nehmen die in Absatz 1 genannten Aufgaben als pflichtige Selbstverwaltungsaufgaben wahr.

(3) Die Rechtsaufsicht über die Aufgabenwahrnehmung nach § 6b des Bundeskindergeldgesetzes obliegt dem für Soziales zuständigen Ministerium.

#### § 2  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 9.
Mai 2011

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident  
Matthias Platzeck

Der Minister für Arbeit,  
Soziales, Frauen und Familie

Günter Baaske