## Verordnung über Zuständigkeiten bei der Strafvollstreckung im Verhältnis der Staatsanwaltschaft Frankfurt (Oder) zur Staatsanwaltschaft Neuruppin und der Staatsanwaltschaft Potsdam zur Staatsanwaltschaft Cottbus

Auf Grund des § 143 Absatz 5 Satz 1 des Gerichtsverfassungsgesetzes in der Fassung der Bekanntmachung vom 9.
 Mai 1975 (BGBl.
I S. 1077), der durch Artikel 14 des Gesetzes vom 3.
Mai 2000 (BGBl.
I S. 632, 633) eingefügt worden ist, in Verbindung mit § 1 Nummer 15 der Justiz-Zuständigkeitsübertragungsverordnung vom 28.
November 2006 (GVBl.
II S. 479), der zuletzt durch Verordnung vom 5.
Dezember 2012 (GVBl.
II Nr.
103) geändert worden ist, verordnet der Minister der Justiz:

### § 1  
Zuständigkeitszuweisung

(1)Der Staatsanwaltschaft Frankfurt (Oder) wird für den Bezirk des Amtsgerichts Schwedt/Oder aus dem Zuständigkeitsbereich der Staatsanwaltschaft Neuruppin die Zuständigkeit für die Strafvollstreckung und die Vollstreckung von Maßregeln der Besserung und Sicherung hinsichtlich der Verfahren zugewiesen, in denen die Rechtskraft der zugrunde liegenden Entscheidung bis zum 31.
Dezember 2012 eingetreten ist.

(2)Der Staatsanwaltschaft Potsdam wird für den Bezirk des Amtsgerichts Königs Wusterhausen aus dem Zuständigkeitsbereich der Staatsanwaltschaft Cottbus die Zuständigkeit für die Strafvollstreckung und die Vollstreckung von Maßregeln der Besserung und Sicherung hinsichtlich der Verfahren zugewiesen, in denen die Rechtskraft der zugrunde liegenden Entscheidung bis zum 31.
Dezember 2012 eingetreten ist.

(3)Die vorstehenden Zuweisungen gelten nicht, soweit die Staatsanwaltschaften Neuruppin und Cottbus zu Schwerpunktstaatsanwaltschaften bestimmt sind.

### § 2  
Inkrafttreten

Diese Verordnung tritt am 01.Januar 2013 in Kraft.

Potsdam, den 14.
Dezember 2012

Der Minister der Justiz  
Dr.
Volkmar Schöneburg