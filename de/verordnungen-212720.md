## Verordnung zur Aufhebung der Prüfungsordnung für den Ausbildungsberuf  Vermessungstechniker/Vermessungstechnikerin

Auf Grund des § 3 des Gesetzes zur Ausführung des Berufsbildungsgesetzes im öffentlichen Dienst vom 14.
Februar 1994 (GVBl.
I S. 17) und des § 1 Nummer 4 erster Anstrich der Verordnung über Zuständigkeiten nach dem Berufsbildungsgesetz im öffentlichen Dienst vom 12.
Februar 1993 (GVBl.
II S. 94), der durch Artikel 13 des Gesetzes vom 6.
 Dezember 2001 (GVBl.
I S. 244, 248) neu gefasst worden ist, in Verbindung mit § 47 Absatz 1 des Berufsbildungsgesetzes vom 23.
März 2005 (BGBl.
I S. 931) verordnet der Minister des Innern:

### § 1  
Aufhebungsregelung

Die Prüfungsordnung für den Ausbildungsberuf Vermessungstechniker/Vermessungstechnikerin vom 17.
Juli 1996 (GVBl.
II S. 587), die durch Artikel 27 des Gesetzes vom 13.
März 2012 (GVBl.
I Nr. 16) geändert worden ist, wird aufgehoben.

### § 2  
Übergangsregelung

Für Berufsausbildungsverhältnisse, die in dem Ausbildungsberuf Vermessungstechniker/Vermessungstechnikerin vor dem 1.
August 2010 begonnen wurden und nicht nach § 16 der Verordnung über die Berufsausbildung in der Geoinformationstechnologie vom 30.
Mai 2010 (BGBl.
I S. 694) fortgesetzt werden, sind die §§ 1 bis 36 der Prüfungsordnung für den Ausbildungsberuf Vermessungstechniker/Vermessungstechnikerin vom 17.
Juli 1996 (GVBl.
II S. 587), von denen § 3 durch Artikel 27 des Gesetzes vom 13.
März 2012 (GVBl.
I Nr. 16) geändert worden ist, weiter anzuwenden.

### § 3  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 31.
Januar 2013

Der Minister des Innern

Dr.
Dietmar Woidke