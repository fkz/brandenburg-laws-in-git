## Verordnung über den Schutzwald „Naturwald Ruppiner Schweiz“

Auf Grund des § 12 Absatz 1 des Waldgesetzes des Landes Brandenburg vom 20.
April 2004 (GVBl.
I S. 137) verordnet der Minister für Infrastruktur und Landwirtschaft:

#### § 1  
Erklärung zum Schutzwald

Die in § 2 näher bezeichneten Waldflächen mit besonderer Schutzfunktion als Naturwald in der amtsfreien Stadt Neuruppin werden zum Schutzwald erklärt.
Der Schutzwald trägt die Bezeichnung „Naturwald Ruppiner Schweiz“ und wird in das Register der geschützten Waldgebiete aufgenommen.

#### § 2  
Schutzgegenstand

(1) Der Schutzwald hat eine Größe von rund 9 Hektar und besteht aus zwei Teilflächen.
Er umfasst folgende Flurstücke:

**Stadt:**

**Gemarkung:**

**Flur:**

**Flurstück:**

Neuruppin

Gühlen Glienicke

9

129 (teilweise);

 

 

10

162 und 164 (jeweils teilweise).

(2) Die Grenze des Schutzwaldes ist in der als Anlage 1 beigefügten „Topografischen Karte zur Verordnung über den Schutzwald ‚Naturwald Ruppiner Schweiz‘“ im Maßstab 1 : 10 000 und in den als Anlage 2 und 3 beigefügten „Liegenschaftskarten zur Verordnung über den Schutzwald ‚Naturwald Ruppiner Schweiz‘“ im Maßstab 1 : 2 500 mit ununterbrochener Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich ist die Einzeichnung in den Liegenschaftskarten.

(3) Die Verordnung mit Karten kann beim Ministerium für Ländliche Entwicklung, Umwelt und Verbraucherschutz des Landes Brandenburg in Potsdam, oberste Forstbehörde, sowie beim Landesbetrieb Forst Brandenburg in Potsdam, untere Forstbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3  
Schutzzweck

(1) Schutzzweck des Schutzwaldes ist

1.  die Erhaltung und Entwicklung eines natürlich entstandenen Buchenwaldes verschiedener Vegetationsausprägungen;
    
2.  die Bewahrung seiner besonderen Eigenart und hervorragenden Schönheit;
    
3.  die Erhaltung und Wiederherstellung der Leistungsfähigkeit des Naturhaushaltes.
    

(2) Der besondere Schutzzweck ist der Erhalt des Naturwaldes insbesondere zur

1.  langfristigen wissenschaftlichen Erforschung der durch den Menschen nicht direkt beeinflussten Waldentwicklung;
    
2.  Erforschung der Waldstruktur, des Bodens, der Flora und der Fauna;
    
3.  Nutzung als lokale und regionale Weiserfläche zur Ableitung und exemplarischen Veranschaulichung von Erkenntnissen für die Waldbaupraxis und für die forstliche Lehre;
    
4.  Erhaltung und Regeneration forstgenetischer Ressourcen;
    
5.  Erhaltung der floristischen und faunistischen Artenvielfalt in sich natürlich entwickelnden Lebensgemeinschaften.
    

(3) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teils des Gebietes von gemeinschaftlicher Bedeutung „Ruppiner Schweiz“ mit der Gebietsnummer DE 2942-302 im Sinne des § 2a Absatz 1 Nummer 8 des Brandenburgischen Naturschutzgesetzes mit seinem Vorkommen von „Waldmeister-Buchenwald“ als Biotop von gemeinschaftlichem Interesse („natürlicher Lebensraumtyp“ im Sinne des Anhangs I der Richtlinie 92/43/EWG).

#### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind im Schutzwald gemäß § 12 Absatz 6 des Waldgesetzes des Landes Brandenburg alle Handlungen verboten, die dem in § 3 genannten Schutzzweck zuwiderlaufen und das Gebiet oder einzelne seiner Bestandteile nachhaltig stören, verändern, beschädigen oder zerstören können.

(2) Es ist insbesondere verboten:

1.  das Gebiet forstwirtschaftlich zu nutzen;
    
2.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
    
3.  das Gebiet außerhalb der Waldwege zu betreten;
    
4.  im Gebiet mit motorisierten Fahrzeugen und Gespannen zu fahren oder diese dort abzustellen;
    
5.  zu zelten, zu lagern, Wohnwagen aufzustellen, zu rauchen und Feuer anzuzünden;
    
6.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
    
7.  die Bodengestalt zu verändern, Böden zu verfestigen oder zu versiegeln;
    
8.  Düngemittel einschließlich Wirtschaftsdünger und Sekundärrohstoffdünger zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
    
9.  Pflanzenschutzmittel jeder Art oder Holzschutzmittel anzuwenden;
    
10.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten.
    

#### § 5  
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die im Sinne des § 28 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes  
    ordnungsgemäße Unterhaltung der Gewässer sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen jeweils im Einvernehmen mit der unteren Forstbehörde;
    
2.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen;
    
3.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Forstbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
     Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    

(2) Die in § 4 für das Betreten und Befahren des Schutzwaldes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Forstbehörden und sonstige von den Forstbehörden beauftragte Personen sowie für Dienstkräfte  
und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer und Nutzer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums.

#### § 6  
Befreiungen

Wenn überwiegende Gründe des Gemeinwohls es erfordern, kann die untere Forstbehörde im Einvernehmen mit der unteren Naturschutzbehörde auf Antrag Befreiungen von den Verboten dieser Verordnung gewähren.

#### § 7  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 37 Absatz 1 Nummer 7 des Waldgesetzes des Landes Brandenburg handelt, wer vorsätzlich oder fahrlässig den Regelungen der §§ 4 und 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 37 Absatz 3 des Waldgesetzes des Landes Brandenburg mit einer Geldbuße bis zu 20 000 (in Worten: zwanzigtausend) Euro geahndet werden.

#### § 8  
Verhältnis zu anderen rechtlichen Bestimmungen

(1) Die Regelungen naturschutzrechtlicher Schutzgebietsausweisungen im Bereich des in § 2 genannten Gebietes bleiben unberührt.

(2) Die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 31 bis 35 des Brandenburgischen Naturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 39 bis 55 des Bundesnaturschutzgesetzes, §§ 37 bis 43 des Brandenburgischen Naturschutzgesetzes) bleiben unberührt.

#### § 9  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 1 der Waldschutzgebietsverfahrensverordnung genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber der obersten Forstbehörde geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 10  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 6.
Mai 2010

Der Minister für Infrastruktur und Landwirtschaft

Jörg Vogelsänger

![Die Anlage 1 zu § 2 Absatz 2 bildet eine topografische Karte zur Verordnung über den Schutzwald "Naturwald Ruppiner Schweiz" im Maßstab 1 : 10000 ab. Es wird die Grenze des Schutzwaldes mit ununterbrochener Linie dargestellt. Der Schutzwald besteht aus zwei Teilflächen.  Eine Fläche, mit einer Größe von 5,22 Hektar, grenzt an das östliche Kalkseeufer. An der Bienenbachbrücke beginnend, verläuft die Grenze etwa 450 Meter entlang des Bienenbaches nach Süden bis zur Inabrücke. Dann verläuft die Grenze etwa 100 Meter in Richtung Westen und nimmt ihren Verlauf nach Norden etwa 450 Meter auf dem sogenannten Postbotensteig bis zur Bienenbachbrücke.     Die andere Fläche, mit einer Größe von 3,58 Hektar, grenzt an das westliche Ufer des Kalksees. Vom westlichsten Punkt des Kalksees nach etwa 120 Metern in südlicher Richtung beginnend, verläuft die Grenze dieser Fläche etwa 100 Meter entlang des Ufers. In Richtung Südwesten nimmt die Grenze in einem Kerbtal ihren Verlauf bis an den sogenannten Blockweg. Gut 180 Meter folgt sie dann dem Blockweg nordwestlich in Richtung Gühlen Glienicke. Die nördliche Grenze des Gebietes ist etwa 270 Meter lang und endet am Anfangspunkt am Kalkseeufer.   ](/br2/sixcms/media.php/69/Nr.24-1.JPG "Die Anlage 1 zu § 2 Absatz 2 bildet eine topografische Karte zur Verordnung über den Schutzwald "Naturwald Ruppiner Schweiz" im Maßstab 1 : 10000 ab. Es wird die Grenze des Schutzwaldes mit ununterbrochener Linie dargestellt. Der Schutzwald besteht aus zwei Teilflächen.  Eine Fläche, mit einer Größe von 5,22 Hektar, grenzt an das östliche Kalkseeufer. An der Bienenbachbrücke beginnend, verläuft die Grenze etwa 450 Meter entlang des Bienenbaches nach Süden bis zur Inabrücke. Dann verläuft die Grenze etwa 100 Meter in Richtung Westen und nimmt ihren Verlauf nach Norden etwa 450 Meter auf dem sogenannten Postbotensteig bis zur Bienenbachbrücke.     Die andere Fläche, mit einer Größe von 3,58 Hektar, grenzt an das westliche Ufer des Kalksees. Vom westlichsten Punkt des Kalksees nach etwa 120 Metern in südlicher Richtung beginnend, verläuft die Grenze dieser Fläche etwa 100 Meter entlang des Ufers. In Richtung Südwesten nimmt die Grenze in einem Kerbtal ihren Verlauf bis an den sogenannten Blockweg. Gut 180 Meter folgt sie dann dem Blockweg nordwestlich in Richtung Gühlen Glienicke. Die nördliche Grenze des Gebietes ist etwa 270 Meter lang und endet am Anfangspunkt am Kalkseeufer.   ")

![Die Anlage 2 zu § 2 Absatz 2 bildet eine Liegenschaftskarte zur Verordnung über den Schutzwald "Naturwald Ruppiner Schweiz" im Maßstab 1 : 2500 ab. Es wird die Grenze der Teilfläche des Schutzwaldes in der Flur 9 der Gemarkung Gühlen-Glienicke mit ununterbrochener Linie dargestellt. Die Fläche befindet sich teilflächenweise im Flurstück 129. Sie grenzt östlich an das Flurstück 151 und nördlich an das Flurstück 100 der Gemarkung Gühlen Glienicke. Südlich grenzt die Fläche an das Flurstück 4 aus der Flur 2 der Gemarkung Neuruppin.](/br2/sixcms/media.php/69/Nr.24-2.JPG "Die Anlage 2 zu § 2 Absatz 2 bildet eine Liegenschaftskarte zur Verordnung über den Schutzwald "Naturwald Ruppiner Schweiz" im Maßstab 1 : 2500 ab. Es wird die Grenze der Teilfläche des Schutzwaldes in der Flur 9 der Gemarkung Gühlen-Glienicke mit ununterbrochener Linie dargestellt. Die Fläche befindet sich teilflächenweise im Flurstück 129. Sie grenzt östlich an das Flurstück 151 und nördlich an das Flurstück 100 der Gemarkung Gühlen Glienicke. Südlich grenzt die Fläche an das Flurstück 4 aus der Flur 2 der Gemarkung Neuruppin.")

![Die Anlage 3 zu § 2 Absatz 2 bildet eine Liegenschaftskarte zur Verordnung über den Schutzwald "Naturwald Ruppiner Schweiz" im Maßstab 1 : 2500 ab. Es wird die Grenze der Teilfläche des Schutzwaldes in der Flur 10 der Gemarkung Gühlen-Glienicke mit ununterbrochener Linie dargestellt. Die Fläche befindet sich teilflächenweise im Flurstück 162 und 164. Der Anteil der Fläche des Schutzwaldes in der Teilfläche des Flurstückes 162 liegt bei 60 Prozent. Zu 40 Prozent der Fläche befindet sich der Schutzwald im Teilbereich des Flurstückes 164.](/br2/sixcms/media.php/69/Nr.24-3.JPG "Die Anlage 3 zu § 2 Absatz 2 bildet eine Liegenschaftskarte zur Verordnung über den Schutzwald "Naturwald Ruppiner Schweiz" im Maßstab 1 : 2500 ab. Es wird die Grenze der Teilfläche des Schutzwaldes in der Flur 10 der Gemarkung Gühlen-Glienicke mit ununterbrochener Linie dargestellt. Die Fläche befindet sich teilflächenweise im Flurstück 162 und 164. Der Anteil der Fläche des Schutzwaldes in der Teilfläche des Flurstückes 162 liegt bei 60 Prozent. Zu 40 Prozent der Fläche befindet sich der Schutzwald im Teilbereich des Flurstückes 164.")