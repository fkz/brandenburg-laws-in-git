## Vereinbarung zwischen Landtag und Landesregierung über die Unterrichtung des Landtages nach Artikel 94 der Verfassung des Landes Brandenburg

In Ausführung von Artikel 94 der Verfassung des Landes Brandenburg schließen der Landtag, vertreten durch den Präsidenten des Landtages, und die Landesregierung, vertreten durch den Ministerpräsidenten, folgende Vereinbarung über die Unterrichtung des Landtages durch die Landesregierung:

Präambel
--------

Die Vereinbarung ist getragen vom beiderseitigen Respekt für die dem Landtag und der Landesregierung durch die Verfassungsordnung zugewiesenen Aufgaben und Kompetenzen.

Der Abschluss dieser Vereinbarung erfolgt in der Überzeugung, dass ein konstruktives Miteinander der Verfassungsorgane Landtag und Landesregierung dem Wohle des Landes Brandenburg und seiner Bürgerinnen und Bürger dienlich ist.

In diesem Sinne soll durch eine frühzeitige und vollständige Information des Landtages die Voraussetzung für eine Positionierung der Abgeordneten zu Vorhaben der Landesregierung geschaffen werden.

Landtag und Landesregierung sehen diese Vereinbarung und deren kooperative Anwendung somit als praxisnahe Verwirklichung verfassungsrechtlicher Vorgaben an.

I.
Vorhaben der Landesgesetzgebung
----------------------------------

1.  Die Landesregierung unterrichtet den Landtag über Gesetzentwürfe spätestens dann, wenn sie den kommunalen Spitzenverbänden, sonstigen Verbänden, Organisationen oder Körperschaften oder anderen außerhalb der Landesregierung stehenden Stellen im Rahmen der formellen Beteiligungsverfahren zugeleitet werden unter Angabe der Adressaten der jeweiligen Zuleitung.
    
2.  Die Nummer 1 gilt entsprechend für Rechtsverordnungen und Verwaltungsvorschriften, soweit sie den kommunalen Spitzenverbänden, sonstigen Verbänden, Organisationen oder Körperschaften oder anderen außerhalb der Landesregierung stehenden Stellen im Rahmen der formellen Beteiligungsverfahren zugeleitet werden.
    

II.
Beabsichtigte Staatsverträge und Verwaltungsabkommen
--------------------------------------------------------

1.  Mit Einleitung des Kabinettverfahrens (Mitzeichnungsverfahrens) ist der Landtag über den beabsichtigten Abschluss eines Staatsvertrages bzw.
    eines Verwaltungsabkommens zu unterrichten.
    
2.  Die Unterrichtung enthält den voraussichtlichen Text des Staatsvertrages bzw.
    des Verwaltungsabkommens und die für den Abschluss sprechenden Gründe.
    
3.  Erfolgt eine Stellungnahme des Landtages oder in eilbedürftigen Angelegenheiten eine vorläufige Stellungnahme des federführenden Ausschusses, so wird die Landesregierung diese bei Abschluss des Staatsvertrages bzw.
    Verwaltungsabkommens einbeziehen.
    
4.  Für die beabsichtigte Änderung, Aufhebung oder Kündigung eines Staatsvertrages bzw.
    Verwaltungsabkommens gelten die vorstehenden Regelungen entsprechend.
    

III.
Bundesratsangelegenheiten
------------------------------

1.  Die Landesregierung unterrichtet den Landtag zeitnah, wenn beim Bundesrat Vorlagen eingegangen sind, mit denen im Weg der Verfassungsänderung Kompetenzen der Länder auf den Bund oder Kompetenzen des Bundes auf die Länder verlagert werden sollen oder die für das Land Brandenburg von grundsätzlicher Bedeutung sind.
    
2.  Soweit die Landesregierung entsprechende Vorlagen im Bundesrat einbringt, leitet die Landesregierung dem Landtag den Text der Initiative spätestens gleichzeitig mit der Übermittlung an den Bundesrat zu.
    
3.  Erfolgt eine Stellungnahme des Landtages oder in eilbedürftigen Angelegenheiten eine vorläufige Stellungnahme des federführenden Ausschusses, so wird die Landesregierung diese bei ihrer Entscheidung über ihr Abstimmungsverhalten im Bundesrat einbeziehen.
    

IV.
Zusammenarbeit mit dem Bund, den Ländern, den Regionen,  
anderen Staaten und zwischenstaatlichen Einrichtungen
-------------------------------------------------------------------------------------------------------------------

Die Unterrichtung über öffentlich zugänglichen Ergebnisse der Ministerpräsidentenkonferenzen und der Fachministerkonferenzen erfolgt über die betreffende Internetseite des Bundesrates[\[1\]](#_ftn1).

V.
Angelegenheiten der Europäischen Union
-----------------------------------------

1.  Die Landesregierung unterrichtet den für Europapolitik zuständigen Ausschuss zeitnah über alle Vorhaben im Rahmen der Europäischen Union, die für das Land von grundsätzlicher landespolitischer Bedeutung sind, und gibt ihm damit die Möglichkeit zur Stellungnahme.
    Sie unterrichtet den Landtag zeitnah insbesondere auch über Initiativen, die eine Verlagerung von Kompetenzen der Länder auf die Europäische Union zur Folge hätten.
    
2.  Die Landesregierung übermittelt dem Landtag zeitnah die über den Bundesrat zugeleiteten Grunddrucksachen.
    Soweit diese im Internet verfügbar sind, sind sie auf der betreffenden Internetseite des Bundesrates[\[2\]](#_ftn2) abrufbar.
    
3.  Die Landesregierung übermittelt dem Landtag alle vom Bundesrat im Rahmen der Subsidiaritätsprüfung erhaltenen Frühwarndokumente durch automatische Weiterleitung und weist den Landtag unverzüglich auf im Zusammenhang mit der Behandlung von Vorhaben der Europäischen Union vom Bundesrat festgestellte Verstöße gegen das Subsidiaritätsprinzip hin.
    
4.  Die Landesregierung unterrichtet den Landtag zeitnah über die Ergebnisse der Europaministerkonferenzen und der Plenarsitzungen des Ausschusses der Regionen, soweit diese für das Land Brandenburg von grundsätzlicher landespolitischer Bedeutung sind.
    
5.  Die Landesregierung berichtet dem für Europapolitik zuständigen Ausschuss zeitnah über beabsichtigte Vertragsänderungen im Rahmen von Regierungskonferenzen der Mitgliedstaaten der Europäischen Union, die die Zuständigkeiten des Landes berühren.
    
6.  Die Landesregierung legt dem Landtag jährlich eine Bewertung des jeweiligen Arbeitsprogramms der Kommission für das laufende Jahr unter besonderer Berücksichtigung brandenburgischer Interessen sowie der Beachtung des Subsidiaritätsprinzips vor.
    Darüber hinaus berichtet die Landesregierung jährlich über die Schwerpunkte ihrer europapolitischen Aktivitäten wie zum Beispiel grundsätzliche und neue europapolitische Entwicklungen im Bundesrat und die Arbeit im Ausschuss der Regionen.
    
7.  Die Landesregierung übermittelt dem Landtag die Schwerpunkte der aktuellen Vorhabenplanung der Präsidentschaft des Rates der Europäischen Union.
    
8.  Die Landesregierung wird ihr rechtzeitig zugegangene Stellungnahmen des Landtages zu Vorhaben der Europäischen Gemeinschaften, die Gesetzgebungszuständigkeiten der Länder berühren, bei ihrer Entscheidung einbeziehen.
    Entsprechendes gilt bei der Übertragung von Hoheitsrechten der Länder auf die Europäische Union.
    In Fällen, in denen durch ein Vorhaben im Schwerpunkt ausschließlich Gesetzgebungsbefugnisse der Länder betroffen sind und daher die Verhandlungsführung im Rat der Europäischen Union auf einen Vertreter der Länder übertragen worden ist, sagt die Landesregierung zu, im Bundesrat Stellungnahmen des Landtages bei ihrer Entscheidung einzubeziehen.
    

VI.
Absehen von der Unterrichtung
---------------------------------

Die Landesregierung kann von einer Unterrichtung absehen, wenn überwiegende öffentliche oder private Interessen an der Geheimhaltung dies zwingend erfordern.
Eine Verpflichtung zur Information aus dem Kernbereich exekutiver Eigenverantwortung besteht nicht.

VII.
Anwendung der Vereinbarung
-------------------------------

1.  Bei Anwendung dieser Vereinbarung wird die Landesregierung das Interesse des Landtages einbeziehen,
    
    1.  auch von maßgeblichen Änderungen gegenüber dem übermittelten Sachstand zu erfahren; dies gilt sinngemäß, wenn die abschließende Entscheidung der Landesregierung wesentlich von einer zuvor mitgeteilten eigenen Position oder einem Beschluss des Landtages zu dieser Unterrichtung abweicht;
        
    2.  auch dann eine Information zu erhalten, wenn Gegenstände von grundsätzlicher landespolitischer Bedeutung über die vereinbarten Fallgruppen hinaus Belange des Landtages wesentlich berühren.
        
2.  Der Landtag wird bei Anwendung dieser Vereinbarung einbeziehen,
    
    1.  dass die Landesregierung hinsichtlich Art, Zeitpunkt und Inhalt der Unterrichtung die jeweiligen tatsächlichen und verfahrensökonomischen Möglichkeiten berücksichtigen muss; dies schließt ein, dass grundsätzlich alle Mitglieder der Landesregierung Gelegenheit haben müssen, vor einer Mitteilung an den Landtag über den Unterrichtungsgegenstand informiert zu werden;
        
    2.  dass die Landesregierung auch unabhängig vom Vorliegen einer Stellungnahme beschließen kann, wenn besondere Eilbedürftigkeit besteht.
        Die Gründe für die besondere Eilbedürftigkeit werden auf Bitte des federführenden Landtagsausschusses innerhalb von vier Wochen dargelegt.
        
3.  Soweit in dieser Vereinbarung festgelegt ist, dass die Landesregierung eine Stellungnahme des Landtages einbezieht, bedeutet dies keine rechtliche Bindung der Landesregierung, wohl aber deren Verpflichtung, der Stellungnahme des Landtages in ihrer Meinungsbildung besonderes Gewicht beizumessen.
    
4.  Landtag und Landesregierung sind sich darin einig, beim Austausch von Informationen die Möglichkeiten der modernen Informations- und Kommunikationstechnik zu nutzen.
    Die Landesregierung stellt Dokumente zur Verfügung, soweit diese nicht im Internet verfügbar sind.
    Soweit Dokumente im Internet verfügbar sind, wird die Fundstelle bzw.
    ihre Aktualisierung mitgeteilt.
    
5.  Soweit sich bei der Anwendung der Vereinbarung Unklarheiten ergeben oder sich zeigt, dass bestimmte Regelungen der Intention der Vereinbarung zuwiderlaufen, werden Landtag und Landesregierung im Rahmen der vertrauensvollen Zusammenarbeit die Zweckmäßigkeit der in Rede stehenden Regelung überprüfen.
    Darüber hinaus soll regelmäßig, jeweils in der Mitte einer Wahlperiode, überprüft werden, ob auf Grund der konkreten Erfahrungen eine Veränderung dieser Vereinbarung angezeigt scheint.
    
6.  Die Landesregierung regelt die Zuständigkeiten für die jeweiligen Unterrichtungspflichten in der Gemeinsamen Geschäftsordnung für die Ministerien des Landes Brandenburg.
    

VIII.
Inkrafttreten
-------------------

Diese Vereinbarung wird im Gesetz- und Verordnungsblatt des Landes Brandenburg Teil I bekannt gemacht.
Sie tritt am 7.
Oktober 2010 in Kraft.

Potsdam, den 7.
Oktober 2010

Für den Landtag  
Der Präsident

Gunter Fritsch

Für die Landesregierung  
Der Ministerpräsident

Matthias Platzeck

  
 

* * *

[\[1\]](#_ftnref1) [http://www.bundesrat.de/](http://www.bundesrat.de/DE/service/archiv/bv-archiv/bv-archiv-node.html)

[\[2\]](#_ftnref2) [http://www.bundesrat.de/](http://www.bundesrat.de/DE/service/archiv/bv-archiv/bv-archiv-node.html)