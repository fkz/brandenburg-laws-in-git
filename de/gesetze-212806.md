## Brandenburgisches Besoldungs- und Versorgungsanpassungsgesetz 2013/2014 (BbgBVAnpG 2013/2014)

Der Landtag hat das folgende Gesetz beschlossen:

§ 1  
**Geltungsbereich**

(1) Dieses Gesetz gilt für die

1.  Beamtinnen und Beamten des Landes, der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts,
2.  Richterinnen und Richter des Landes,
3.  Versorgungsempfängerinnen und Versorgungsempfänger, denen laufende Versorgungsbezüge zustehen, die das Land, eine Gemeinde, ein Gemeindeverband oder eine der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten oder Stiftungen des öffentlichen Rechts zu tragen hat.
    

(2) Dieses Gesetz gilt nicht für Ehrenbeamtinnen und Ehrenbeamte sowie für ehrenamtliche Richterinnen und Richter.

(3) Dieses Gesetz gilt nicht für öffentlich-rechtliche Religionsgesellschaften und ihre Verbände.

§ 2  
**Anpassung der Besoldung im Jahr 2013**

(1) Die nachfolgenden Dienstbezüge und sonstigen Bezüge werden ab 1.
Juli 2013 um 2,45 Prozent erhöht:

1.  die Grundgehaltssätze,
2.  der Familienzuschlag mit Ausnahme der Erhöhungsbeträge für die Besoldungsgruppen A 2 bis A 5,
3.  die Amtszulagen, auch soweit sie landesrechtlich geregelt sind, sowie die allgemeine Stellenzulage nach Vorbemerkung Nummer 27 der Bundesbesoldungsordnungen A und B in der am 31.
    August 2006 geltenden Fassung,
4.  der Auslandszuschlag und der Auslandskinderzuschlag.

(2) Absatz 1 gilt entsprechend für

1.  die Leistungsbezüge nach § 33 Absatz 1 des Bundesbesoldungsgesetzes in der am 31.
    August 2006 geltenden Fassung, soweit sie gemäß § 2a Absatz 6 des Brandenburgischen Besoldungsgesetzes, § 2 Absatz 3 der Hochschulleistungsbezügeverordnung vom 23.
    März 2005 (GVBl.
    II S.
    152) oder § 3 Absatz 2 der Leistungsbezügeverordnung FHPol vom 3.
    August 2005 (GVBl.
    II S.
    454), die durch Artikel 4 des Gesetzes vom 24.
    Oktober 2007 (GVBl.
    I S.
    134, 140) geändert worden ist, an den regelmäßigen Besoldungsanpassungen teilnehmen,
2.  den Betrag nach § 4 Absatz 1 Nummer 1 der Erschwerniszulagenverordnung in der am 31.
    August 2006 geltenden Fassung,
3.  die Beträge nach § 4 Absatz 1 und 3 der Verordnung über die Gewährung von Mehrarbeitsvergütung für Beamte in der am 31.
    August 2006 geltenden Fassung,
4.  die in § 2 Absatz 1 Nummer 6 bis 10 des Brandenburgischen Besoldungs- und Versorgungsanpassungsgesetzes 2008 genannten Bezüge.
    

(3) Die Grundgehaltssätze werden anschließend ab 1.
Juli 2013 jeweils um 21 Euro erhöht.

(4) Die Anwärtergrundbeträge werden ab 1.
Juli 2013 um 60 Euro erhöht.

§ 3  
**Anpassung der Besoldung im Jahr 2014**

(1) Die nachfolgenden Dienstbezüge und sonstigen Bezüge werden ab 1.
Juli 2014 um 1,8 Prozent erhöht:

1.  die Grundgehaltssätze,
2.  der Familienzuschlag mit Ausnahme der Erhöhungsbeträge für die Besoldungsgruppen A 4 und A 5,
3.  die Amtszulagen sowie die allgemeine Stellenzulage nach Vorbemerkung Nummer 13 der Besoldungsordnungen A und B,
4.  die Anwärtergrundbeträge.

(2) Absatz 1 gilt entsprechend für

1.  die Leistungsbezüge nach § 30 Absatz 1 des Brandenburgischen Besoldungsgesetzes, soweit sie gemäß § 32 Satz 5 des Brandenburgischen Besoldungsgesetzes, § 2 Absatz 3 der Hochschulleistungsbezügeverordnung vom 23. März 2005 (GVBl.
    II S. 152) oder § 3 Absatz 2 der Leistungsbezügeverordnung FHPol vom 3.
    August 2005 (GVBl.
    II S. 454), die durch Artikel 4 des Gesetzes vom 24.
    Oktober 2007 (GVBl.
     I S. 134, 140) geändert worden ist, an den regelmäßigen Besoldungsanpassungen teilnehmen,
2.  den Betrag nach § 4 Absatz 1 Nummer 1 der Erschwerniszulagenverordnung in der am 31.
     August 2006 geltenden Fassung,
3.  die Beträge nach § 4 Absatz 1 und 3 der Verordnung über die Gewährung von Mehrarbeitsvergütung für Beamte in der am 31.
    August 2006 geltenden Fassung,
4.  die in § 2 Absatz 1 Nummer 6 bis 10 des Brandenburgischen Besoldungs- und Versorgungsanpassungsgesetzes 2008 genannten Bezüge.

§ 4  
**Rundungsregelung**

Bei der Berechnung der nach den §§ 2 und 3 erhöhten Bezüge sind die sich ergebenden Bruchteile eines Cents hinsichtlich der Beträge des Familienzuschlags der Stufe 1 auf den nächsten durch zwei teilbaren Centbetrag aufzurunden.
Im Übrigen sind Bruchteile eines Cents unter 0,5 abzurunden und Bruchteile von 0,5 und mehr aufzurunden.

§ 5  
**Anpassung der Versorgungsbezüge**

(1) Bei Versorgungsempfängerinnen und Versorgungsempfängern gelten die Erhöhungen nach den §§ 2 und 3 entsprechend für die in Artikel 2 § 2 Absatz 1 bis 5 des Bundesbesoldungs- und -versorgungsanpassungsgesetzes 1995 vom 18.
Dezember 1995 (BGBl.
I S.
1942) genannten Bezügebestandteile sowie für die in § 14 Absatz 2 Satz 1 Nummer 3 und § 84 Absatz 1 Nummer 4, 5 und 7 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung aufgeführten Stellenzulagen und Bezüge.

(2) Versorgungsbezüge, die in festen Beträgen festgesetzt sind, und der Betrag nach Artikel 13 § 2 Absatz 4 des Fünften Gesetzes zur Änderung besoldungsrechtlicher Vorschriften vom 28.
Mai 1990 (BGBl.
I S.
967, 976) werden ab 1.
Juli 2013 um 2,35 Prozent und ab 1.
Juli 2014 um 1,7 Prozent erhöht.

(3) Bei Versorgungsempfängerinnen und Versorgungsempfängern, deren Versorgungsbezügen ein Grundgehalt der Besoldungsgruppe A 1 bis A 8 zugrunde liegt, vermindert sich das Grundgehalt ab 1.
Juli 2013 um 53,49 Euro und ab 1.
 Juli 2014 um 54,45 Euro, wenn ihren ruhegehaltfähigen Dienstbezügen die Stellenzulage nach Vorbemerkung Nummer 27 Absatz 1 Buchstabe a oder b der Bundesbesoldungsordnungen A und B in der am 31.
August 2006 geltenden Fassung bei Eintritt in den Ruhestand nicht zugrunde gelegen hat.

§ 6  
**Bekanntmachung**

Das Ministerium der Finanzen macht die Beträge der nach den §§ 2 und 3 erhöhten Bezüge im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I durch Neubekanntmachung der Anlagen 1 bis 16 des Brandenburgischen Besoldungs- und Versorgungsanpassungsgesetzes 2008 und der Anlage 2 des Brandenburgischen Besoldungsgesetzes bekannt.

§ 7  
**Inkrafttreten**

Dieses Gesetz tritt mit Wirkung vom 1.
Juli 2013 in Kraft.

Potsdam, den 15.
Oktober 2013

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch