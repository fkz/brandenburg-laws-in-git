## Verordnung über das Naturschutzgebiet „Krayner Teiche/Lutzketal“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (GVBl.
I S.
2542) in Verbindung mit § 19 Absatz 1 und 2, § 21 Absatz 1 Satz 2 und § 78 Absatz 1 Satz 3 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S.
350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1  
Erklärung zum Schutzgebiet
--------------------------------

Die in § 2 näher bezeichnete Fläche im Landkreis Spree-Neiße wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Krayner Teiche/Lutzketal“.

§ 2  
Schutzgegenstand
----------------------

(1) Das Naturschutzgebiet hat eine Größe von rund 545 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Gemeinde:**

**Gemarkung:**

**Flur:**

 

 

 

Schenkendöbern

Grano

1, 3;

Schenkendöbern

Krayne

1, 2;

Schenkendöbern

Lübbinchen

1, 2;

Schenkendöbern

Groß Drewitz

1, 6, 7;

Schenkendöbern

Schenkendöbern

1, 2.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 01 bis 03 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 01 bis 14 aufgeführten Liegenschaftskarten.
Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 3 beigefügt.

(3) Innerhalb des Naturschutzgebietes wird gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes eine Zone 1 festgesetzt, in der keine forstwirtschaftliche Nutzung erfolgt.
Die Zone 1 umfasst rund 3 Hektar und liegt in der Gemarkung Grano, Flur 1.

(4) Innerhalb des Naturschutzgebietes wird außerdem eine Zone 2 mit einer Größe von rund 13 Hektar mit forstlichen Nutzungsbeschränkungen festgesetzt.
 Diese Zone liegt in der Gemarkung Grano, Flur 1 und 3.

(5) Die Grenzen der Zonen 1 und 2 sind in der in Anlage 2 Nummer 1 genannten topografische Karte mit der Blattnummer 03 sowie in den in Anlage 2 Nummer 2 genannten Liegenschaftskarten mit den Blattnummern 05, 06, 09 und 10 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(6) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Spree-Neiße, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

§ 3  
Schutzzweck
-----------------

(1) Schutzzweck des Naturschutzgebietes als ein struktur- und artenreicher Biotopkomplex innerhalb des Gubener Landes ist

1.  die Erhaltung, Entwicklung und Wiederherstellung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der naturnahen Wälder, der Moore, der Frisch-, Feucht- und Nasswiesen, der Hochstaudenfluren, der Großseggenriede, der Fließ- und Stillgewässer und der Trockenlebensräume;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Mondraute (Botrychium lunaria), Kartäuser-Nelke (Dianthus carthusianorum), Sumpf-Herzblatt (Parnassia palustris), Langblättriger Sonnentau (Drosera anglica), Langblättriges Waldvögelein (Cephalantera longifolia) und Steifblättriges Knabenkraut (Dactylorhiza incarnata);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Vögel, Amphibien, Reptilien und Wirbellosen, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Fischadler (Pandion haliaetus), Seeadler (Haliaeetus albicilla), Kranich (Grus grus), Schwarzmilan (Milvus migrans), Drosselrohrsänger (Acrocephalus arundinaceus), Eisvogel (Alcedo atthis), Zauneidechse (Lacerta agilis), Schwalbenschwanz (Papilio machaon) und Grüne Mosaikjungfer (Aeshna viridis);
4.  die Erhaltung und Entwicklung der Gewässer als Rast- und Überwinterungshabitate für Kraniche, Schwäne, Taucher und Enten, insbesondere Schellenten;
5.  die Erhaltung des Lutzketals mit natürlichen Bachmäandern in einem durch steile Hänge gekennzeichneten Kerbtal aus wissenschaftlichen Gründen;
6.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des regionalen Biotopverbundes zwischen Oder/Neißetal, Lieberoser Hochfläche und Spreewald.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Krayner Teiche/Lutzketal“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinen Vorkommen von

1.  Oligo- bis mesotrophen kalkhaltigen Gewässern mit benthischer Vegetation aus Armleuchteralgen, Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Flüssen der planaren bis montanen Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Pfeifengraswiesen auf kalkreichem Boden, Feuchten Hochstaudenfluren der planaren und montanen bis alpinen Stufe, Mageren Flachland-Mähwiesen (Alopecurus pratensis \[Wiesen-Fuchsschwanz\], Sanguisorba officinalis \[Großer Wiesenknopf\]), Übergangs- und Schwingrasenmooren, Torfmoor-Schlenken, Subatlantischem oder mitteleuropäischem Stieleichenwald oder Hainbuchenwald (Carpinion betuli) (Stellario-Carpinetum), Alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur als Biotope von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG);
2.  trockenen, kalkreichen Sandrasen, naturnahen Kalk-Trockenrasen und deren Verbuschungsstadien (Festuco-Brometalia) und Auen-Wäldern mit Alnus glutinosa (Schwarz-Erle) als prioritäre Biotope („prioritäre Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG);
3.  Biber (Castor fiber), Fischotter (Lutra lutra), Kamm-Molch (Triturus cristatus) und Bauchiger Windelschnecke (Vertigo moulinsiana) als Tierarten von gemeinschaftlichem Interesse (im Sinne des Anhangs II der Richtlinie 92/43/EWG) einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

§ 4  
Verbote
-------------

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten außerhalb der Moore, Feuchtgebiete, Bruchwälder, Röhrichte, Nass- und Feuchtwiesen sowie des Talbereichs der Lutzke zum Zweck der Erholung sowie des nichtgewerblichen Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 14 jeweils nach dem 30.
    Juni eines jeden Jahres;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb von Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, zu reiten;
11.  mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
12.  zu baden oder zu tauchen;
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle), Rückstände aus Biogasanlagen und Sekundärrohstoffdünger (wie zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) zum Zweck der Düngung einzusetzen sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien wie zum Beispiel Gärfutter zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

§ 5  
Zulässige Handlungen
--------------------------

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertende Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und vergleichbare Rückstände aus Biogasanlagen oder Sekundärrohstoffdünger (wie zum Beispiel solche aus Abwasser, Klärschlamm oder Bioabfällen) einzusetzen,
    2.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt, wobei bei Narbenschäden eine umbruchlose Nachsaat auf Grünland mit Zustimmung der unteren Naturschutzbehörde zulässig bleibt,
    3.  bei der Nutzung der Ackerflächen der Einsatz von chemisch-synthetischen Düngemitteln, Gülle, Herbiziden und Insektiziden unzulässig ist;

2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen außerhalb der Zone 1 mit der Maßgabe, dass
    1.  die Walderneuerung auf den Flächen der in § 3 Absatz 2 genannten Waldlebensraumtypen und sonstiger naturnaher Wälder durch Naturverjüngung erfolgt.
        Auf den übrigen Wald- und Forstflächen dürfen nur Arten der potenziell natürlichen Vegetation eingebracht werden, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
    2.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    3.  auf den Flächen der in § 3 Absatz 2 genannten Waldlebensraumtypen und sonstiger naturnaher Wälder eine naturnahe Waldentwicklung mit einem Totholzanteil von mindestens 10 Prozent des aktuellen Bestandesvorrats erfolgt, bis zu fünf Stück je Hektar lebensraumtypische, abgestorbene, stehende Bäume (Totholz) mit einem Brusthöhendurchmesser von mindestens 30 Zentimetern ohne Rinde in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden und liegendes Totholz (mindestens zwei Stück je Hektar mit einem Durchmesser von 65 Zentimetern am stärksten Ende) im Bestand verbleibt,
    4.  auf den Flächen der in § 3 Absatz 2 genannten Waldlebensraumtypen und sonstiger naturnaher Wälder die Nutzung der Bestände einzelstamm- bis truppweise erfolgt.
        In den übrigen Wäldern und Forsten sind Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von 0,5 Hektar zulässig,
    5.  Neuaufforstungen auf den Flächen der in § 3 Absatz 2 genannten Lebensraumtypen und gesetzlich geschützten Biotope unzulässig sind,
    6.  hydromorphe Böden nur bei Frost zu befahren sind,
    7.  keine flächige, in den Mineralboden eingreifende Bodenverwundung erfolgt,
    8.  innerhalb der Zone 2 die Nutzung der Bestände einzelstammweise erfolgt und stehendes und liegendes Totholz im Bestand verbleibt,
    9.  § 4 Absatz 2 Nummer 17 und 23 gilt;
3.  erforderliche Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne einer Fischbestandskontrolle, -regulierung und -förderung mit Zustimmung der unteren Naturschutzbehörde.
    Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird;
4.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes in Verbindung mit dem Fischereigesetz des Landes Brandenburg entsprechende Teichbewirtschaftung im Sinne der guten fachlichen Praxis gemäß den Leitlinien zur naturschutzgerechten Teichwirtschaft in Brandenburg vom 16.
    März 2011 auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters und des Bibers weitgehend ausgeschlossen ist,
    2.  das Speicherbecken und der Unterteich im 1- bis 2-jährigen Turnus abzulassen sind; das Speicherbecken ist mindestens im Zeitraum vom 1.
        März bis zum 15.
        August eines jeden Jahres anzustauen.
        Änderungen der Bespannungszeiträume sind mit Zustimmung der unteren Naturschutzbehörde zulässig.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird;
5.  die Genehmigung von Maßnahmen zur Vergrämung und Tötung von Kormoranen im Bereich der fischereiwirtschaftlich genutzten Teiche durch die zuständige Naturschutzbehörde, sofern hierfür die erforderliche artenschutzrechtliche Ausnahmegenehmigung oder Befreiung vorliegt.
    Die Genehmigung kann mit Auflagen versehen werden, sie ist zu erteilen, wenn der Schutzzweck von der Maßnahme nicht wesentlich beeinträchtigt wird;
6.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass
    1.  § 4 Absatz 2 Nummer 13 gilt, wobei die Benutzung von maximal sechs muskelkraft- oder elektromotorbetriebenen Booten zum Anfüttern, zum Auslegen von Ködern und zum Raubfischangeln vom 1.
        Oktober bis zum 31.
        Dezember eines jeden Jahres zulässig bleibt.
        Es sind die in den in § 2 Absatz 2 genannten topografischen Karten dargestellten Bootsliegeplätze zu nutzen,
    2.  die Angelfischerei nur an den in den in § 2 Absatz 2 genannten topografischen Karten dargestellten Angelstellen am Oberteich ausgeübt wird,
    3.  die Angelberechtigten bei der Ausübung der Angelfischerei Anglerschutzzelte zum Schutz vor Witterungsunbilden verwenden dürfen,
    4.  die Angelberechtigten zum Aufsuchen der zugelassenen Angelstellen gemäß Buchstabe b die in den in § 2 Absatz 2 genannten topografischen Karten gekennzeichneten Zuwegungen befahren dürfen und nach Entladung der Ausrüstung die Kraftfahrzeuge an den in § 2 Absatz 2 genannten topografischen Karten ausgewiesenen zugelassenen Stellplätzen abzustellen haben;
7.  für den Bereich der Jagd:
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Jagd auf Wasservögel am Speicherbecken mit Ausnahme der Durchführung einer eintägigen Gesellschaftsjagd pro Jahr verboten ist,
        
        bb)
        
        am Ober- und Unterteich die Wasservogeljagd ab dem 1.
        Oktober bis zum Ende der gesetzlich festgelegten Jagdzeit eines jeden Jahres gestattet ist,
        
        cc)
        
        die Fallenjagd ausschließlich mit Lebendfallen erfolgt,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    3.  die Aufstellung transportabler und mobiler Ansitzeinrichtungen außerhalb einer 20 Meter breiten Zone ab Gewässerufer um den gesamten Unterteich und entlang des Südufers des Speicherbeckens,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und des Lebensraumtyps „Magere Flachlandmähwiese“,
    5.  die Unterhaltung der in den in § 2 Absatz 2 genannten topografischen Karten gekennzeichneten Wild-wiesen und Wildäcker.Ablenkfütterungen sowie die Anlage von Wildwiesen und Wildäckern sind unzulässig; im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
8.  das Baden in Verbindung mit dem Betreten außerhalb der Wege an der Badestelle bei Krayne, die in der in § 2 Absatz 2 genannten topografischen Karte mit der Blattnummer 03 gekennzeichnet ist;
9.  die Nutzung der in den in § 2 Absatz 2 genannten topografischen Karten ausgewiesenen Grillplätze;
10.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter die Nummer 11 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
11.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
12.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen(Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsmaßnahmen kann durch langfristig gültige Vereinbarungen erteilt werden;
13.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
14.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch nach dem 30.
    Juni eines jeden Jahres;
15.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
16.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
17.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 an Straßen und Wegen freigestellt;
18.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

§ 6  
Pflege- und Entwicklungsmaßnahmen
---------------------------------------

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  zur Stabilisierung des Landschaftswasserhaushalts soll der Wasserrückhalt im Gebiet verbessert werden;
2.  die Grünlandflächen sollen frühestens ab dem 1.
    Juni eines jeden Jahres und vorrangig als Mähwiesen genutzt werden;
3.  kleinflächig unbestockte Flächen mit schutzwürdigen Biotopen (Orchideenwiesen, Trockenrasen) sollen durch Pflegemaßnahmen offen gehalten werden; die Gehölzsukzession soll in diesen Bereichen entnommen werden;
4.  die Ackerflächen bei Grano sollen in Grünland umgewandelt und extensiv genutzt werden;
5.  die Nadelbaumbestockung im nördlichen Teil des Lutzketals soll zu einem naturnahen Wald entwickelt werden;
6.  innerhalb der Wald- und Forstflächen sollen mindestens fünf Stämme (Biotop-, Horst-, Höhlenbäume) je Hektar bis zum Absterben und Zerfall aus der Nutzung genommen werden;
7.  in den Wald- und Forstflächen sollen in der Zeit vom 15.
    März bis zum 31.
    Juli eines jeden Jahres keine Fällarbeiten durchgeführt werden;
8.  für die Bewirtschaftung der Teiche:
    1.  der Besatz mit Teichfischen im Speicherbecken und in den Kupfermühlenteichen soll auf einen Zielertrag von maximal 200 Kilogramm Abfischung pro Hektar Teichfläche oder die Produktion einsömmriger Fische ausgerichtet sein,
    2.  durch technische Umrüstung der Ablasseinrichtungen und allmähliches Ablassen der Fischteiche soll eine Belastung der unterhalb anschließenden Fließgewässerabschnitte mit Schwebstoffen minimiert werden,
    3.  für die Teiche soll ein Bewirtschaftungsplan erstellt werden, der folgende Mindestangaben enthält: Besatz nach Arten und Altersklassen, Bespannungszeiträume, Düngung, Maßnahmen zur Verhinderung der Teichverlandung nach Umfang und Zeitpunkt, Teichpflege- und Sanierungsmaßnahmen jeweils nach Art, Umfang und Zeitpunkt;
9.  Moorstandorte sollen durch geeignete Maßnahmen des Wasserrückhalts vernässt werden; die moortypische Vegetation soll gegebenenfalls durch zusätzliche Pflegemaßnahmen gefördert werden.

§ 7  
Befreiungen
-----------------

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

§ 8  
Ordnungswidrigkeiten
--------------------------

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

§ 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen
--------------------------------------------------------------------------------

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere die §§ 31 bis 33 und § 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

§ 10  
Geltendmachen von Rechtsmängeln
--------------------------------------

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

§ 11  
Inkrafttreten, Außerkrafttreten
--------------------------------------

(1) § 5 Absatz 1 Nummer 1 tritt am 1.
Juli 2013 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig tritt der Beschluss Nummer 75/81 des Bezirkstages Cottbus vom 25.
März 1981 zur Festsetzung des Naturschutzgebietes „Lutzketal“ außer Kraft.

Potsdam, den 6.
Februar 2013

Die Ministerin für Umwelt, Gesundheit und Verbraucherschutz

Anita Tack

Anlage 1  
(zu § 2 Absatz 1)
----------------------------

![Das Naturschutzgebiet Krayner Teiche/Lutzketal hat eine Größe von rund 545 Hektar. Es liegt im Landkreis Spree-Neiße in der Gemeinde Schenkendöbern und umfasst Teile der Gemarkungen Grano, Krayne, Lübbinchen, Groß Drewitz und Schenkendöbern.](/br2/sixcms/media.php/69/image002.jpg "Das Naturschutzgebiet Krayner Teiche/Lutzketal hat eine Größe von rund 545 Hektar. Es liegt im Landkreis Spree-Neiße in der Gemeinde Schenkendöbern und umfasst Teile der Gemarkungen Grano, Krayne, Lübbinchen, Groß Drewitz und Schenkendöbern.")

Anlage 2  
(zu § 2 Absatz 2)
----------------------------

### 1.
Topografische Karten im Maßstab 1 : 10 000

**Titel:**

Topografische Karte zur Verordnung über das Naturschutzgebiet „Krayner Teiche/Lutzketal“

Blattnummer

Kartenblatt

Unterzeichnung

01

3953-SW

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV), am 13.
November 2012

02

4053-NW

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

03

4053-NO

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

### 2.
Liegenschaftskarten im Maßstab 1 : 2 500

**Titel:**

Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Krayner Teiche/Lutzketal“

Blattnummer

Gemarkung

Flur

Unterzeichnung

01

Groß Drewitz

1, 7

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

02

Groß Drewitz

1, 7

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

03

Groß Drewitz  
  
Krayne

6, 7  
  
2

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

04

Groß Drewitz  
  
Krayne

6, 7  
  
2

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

05

Grano

1

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

06

Grano

1

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

07

Krayne  
  
Lübbinchen

2  
  
1

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

08

Krayne  
  
Lübbinchen

1, 2  
  
1, 2

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

09

Grano  
  
Krayne  
  
Lübbinchen  
  
Schenkendöbern

1  
  
1, 2  
  
1, 2  
  
1

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

10

Grano  
  
Krayne  
  
Schenkendöbern

1, 3  
  
1  
  
1

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

11

Lübbinchen

1

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

12

Lübbinchen

1, 2

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

13

Krayne  
  
Lübbinchen  
  
Schenkendöbern

1  
  
2  
  
1

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

14

Grano  
  
Krayne  
  
Schenkendöbern

3  
  
1  
  
1, 2

unterzeichnet und gesiegelt von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 13.
November 2012

Anlage 3  
(zu § 2 Absatz 2)
----------------------------

### Flurstücksliste zur Verordnung über das Naturschutzgebiet „Krayner Teiche/Lutzketal“

**Landkreis:** Spree-Neiße

Gemeinde

Gemarkung

Flur

Flurstücke

Schenkendöbern

Grano

1

37, 40, 41, 45 bis 68, 69 bis 72 jeweils anteilig, 80 anteilig, 85 bis 92 jeweils anteilig, 93, 95 bis 100, 221, 226 bis 228, 229 anteilig, 230 bis 238;

Schenkendöbern

Grano

3

7 anteilig, 116 bis 119 jeweils anteilig, 121 bis 137, 141 anteilig, 142 anteilig, 145 anteilig, 146 anteilig, 152 anteilig, 153 bis 156, 158 bis 167, 171, 175 bis 185, 198 bis 202, 204 anteilig, 205, 206, 298 bis 309, 311 anteilig, 326, 327;

Schenkendöbern

Krayne

1

2/1 anteilig, 8 anteilig, 129 anteilig, 130 bis 133, 135/1, 135/2, 135/3 anteilig, 136/1 bis 136/4, 137/1 bis 137/3, 138 anteilig, 142 anteilig, 148 anteilig, 150, 151, 152/1, 154, 155/1, 155/2, 160 bis 163, 174, 176/1, 177, 179 anteilig, 180, 184 bis 186, 190 bis 193, 202 bis 207, 210 bis 213, 215 bis 217, 219 bis 221, 223 bis 225, 227, 229, 231, 242, 244, 248, 250, 254, 256, 266 bis 277, 278 anteilig, 279, 280 anteilig, 281 bis 283, 286, 287;

Schenkendöbern

Krayne

2

17, 18 anteilig, 19 anteilig, 20 anteilig, 21, 22, 24 anteilig, 25 anteilig, 26, 27, 29, 30, 31 bis 35 jeweils anteilig, 37 anteilig, 39, 40, 42, 43/1, 43/2, 44 bis 46, 50 bis 60, 61 anteilig, 90 bis 96;

Schenkendöbern

Lübbinchen

1

1 anteilig, 5, 8/1, 9 bis 11, 13 bis 19, 23 anteilig, 166 bis 179, 190 bis 196, 198 bis 234, 236 bis 259, 261 bis 264, 265/1, 265/2, 266 bis 280, 281 bis 286 jeweils anteilig, 287, 288 bis 295 jeweils anteilig, 300, 301, 315, 317, 318;

Schenkendöbern

Lübbinchen

2

1/1, 1/2, 2/2 bis 2/5, 160 bis 168, 169 anteilig, 171 bis 173, 174/1, 175/1, 176 bis 179, 180/1, 181 bis 186, 187/1, 188, 189, 190/2, 190/3 anteilig, 190/4, 190/5, 191/1 bis 191/5, 192/1, 192/2, 193/1, 193/2, 194/1, 194/2, 195 bis 204, 205/1, 205/2, 206 bis 219, 221 bis 229, 231 bis 241, 242/1 bis 242/4, 256, 266 bis 269;

Schenkendöbern

Groß Drewitz

1

185 bis 187, 189, 194, 195/1 anteilig, 195/2 anteilig, 259 bis 268;

Schenkendöbern

Groß Drewitz

6

2/4 anteilig, 3/1 anteilig, 18, 19/2, 20 anteilig, 32 anteilig, 33 bis 48;

Schenkendöbern

Groß Drewitz

7

39 anteilig, 40, 41, 42, 44 bis 48, 49/1, 50 anteilig, 51/1, 51/3 anteilig, 51/4 anteilig, 52, 53/2, 65/8, 68/1, 69 anteilig, 73 bis 85, 87 bis 92;

Schenkendöbern

Schenkendöbern

1

37 anteilig, 38, 41 bis 54, 57 bis 61, 63 bis 65, 69 bis 73, 74/1, 74/2, 75 bis 83, 85 bis 92, 104 bis 117, 118 anteilig, 119, 120, 122, 123, 126 bis 130, 132 bis 136, 138 bis 147, 168, 169 anteilig, 170 bis 178, 185, 189 anteilig, 192, 193, 195, 197 bis 211, 213, 215 bis 218;

Schenkendöbern

Schenkendöbern

2

53 anteilig, 254 anteilig, 256.

### Flächen der Zone 1:

Gemeinde

Gemarkung

Flur

Flurstücke

Schenkendöbern

Grano

1

40 anteilig, 46, 233 bis 238.

### Flächen der Zone 2:

Gemeinde

Gemarkung

Flur

Flurstücke

Schenkendöbern

Grano

1

40 anteilig, 41, 45, 47 anteilig, 55 anteilig, 56 anteilig, 58 bis 62, 226 anteilig, 227, 228, 231 anteilig, 232;

Schenkendöbern

Grano

3

198, 199, 205, 206 anteilig.