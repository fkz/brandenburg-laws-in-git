## Verordnung über das Naturschutzgebiet „Naturentwicklungsgebiet Kockot“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S.
2542) in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr.
3) und des § 4 Absatz 1 der Naturschutzzuständigkeits-verordnung vom 27.
Mai 2013 (GVBl.
II Nr.
43) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1  
Erklärung zum Schutzgebiet
--------------------------------

Die in § 2 näher bezeichnete Fläche im Landkreis Dahme-Spreewald wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Naturentwicklungsgebiet Kockot“.

§ 2  
Schutzgegenstand
----------------------

(1) Das Naturschutzgebiet hat eine Größe von rund 26 Hektar.
Es befindet sich innerhalb des Geltungsbereiches der Verordnung über die Festsetzung von Naturschutzgebieten und einem Landschaftsschutzgebiet von zentraler Bedeutung mit der Gesamtbezeichnung „Biosphärenreservat Spreewald“ vom 12.
September 1990 (GBl.
SDr.
Nr.
1473).
Es umfasst folgende Flächen:

Gemeinde:

Gemarkung:

Flur:

Flurstück:

Märkische Heide

Kuschkow

6

26/3.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den topografischen Karten im Maßstab 1 : 10 000 mit dem Titel „Topografische Karte zur Verordnung über das Naturschutzgebiet ‚Naturentwicklungsgebiet Kockot‘“ und in der Liegenschaftskarte im Maßstab 1 : 2 500 mit dem Titel „Liegenschaftskarte zur Verordnung über das Naturschutzgebiet ‚Naturentwicklungsgebiet Kockot‘“ mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die topografischen Karten mit den Blattnummern 01 und 02 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Liegenschaftskarte mit der Blattnummer 01.
Die Karten sind von der Siegelverwahrerin, Siegelnummer 21, des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 8.
September 2014 unterzeichnet worden.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Dahme-Spreeewald, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

§ 3  
Schutzzweck
-----------------

Schutzzweck des Naturschutzgebietes ist es, die Funktion einer Kernzone innerhalb des „Biosphärenreservates Spreewald“ zu erfüllen.
Insbesondere werden naturnahe Stieleichen-Hainbuchen- und Erlen-Eschen-Wälder auf Niedermoor-, Moorgley- und Anmoorstandorten der direkten menschlichen Einflussnahme entzogen und langfristig der natürlichen Entwicklung überlassen.

§ 4  
Verbote, Zulässige Handlungen
-----------------------------------

(1) Im Naturschutzgebiet „Naturentwicklungsgebiet Kockot“ ist jegliche wirtschaftliche Nutzung verboten.

(2) Im Übrigen gelten weiterhin die Regelungen für die Schutzzone II gemäß § 4 Absatz 3 Nummer 9 (Naturschutzgebiet „Kockot“) in Verbindung mit § 5 Absatz 1 Nummer 5 und den §§ 6, 7 und 9 der Verordnung über die Festsetzung von Naturschutzgebieten und einem Landschaftsschutzgebiet von zentraler Bedeutung mit der Gesamtbezeichnung „Biosphärenreservat Spreewald“.

§ 5  
Befreiungen
-----------------

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

§ 6  
Ordnungswidrigkeiten
--------------------------

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

§ 7  
Verhältnis zu anderen naturschutzrechtlichen Bestimmungen
---------------------------------------------------------------

Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 17 und 18 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 29 Absatz 3 und § 30 des Bundesnaturschutzgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten, ihrer Lebensstätten und Biotope (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 54 Absatz 7 des Bundesnaturschutzgesetzes) unberührt.

§ 8  
Geltendmachen von Rechtsmängeln
-------------------------------------

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

§ 9  
Inkrafttreten
-------------------

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 22.
September 2014

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

### Anlagen

1

[Naturentwicklungsgebiet Kockot Kartenskizze](/br2/sixcms/media.php/68/GVBl_II_73_2014_Anlage.pdf "Naturentwicklungsgebiet Kockot Kartenskizze") 779.6 KB