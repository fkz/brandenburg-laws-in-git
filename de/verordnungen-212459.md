## Verordnung über die Organisation der Sonderabfallentsorgung im Land Brandenburg (Sonderabfallentsorgungsverordnung - SAbfEV)

Auf Grund des § 14 Absatz 1 und des § 15 des Brandenburgischen Abfall- und Bodenschutzgesetzes vom 6.
Juni 1997 (GVBl.
I S. 40), von denen durch Artikel 1 des Gesetzes vom 27.
Mai 2009 (GVBl.
I S. 175) § 14 Absatz 1 neu gefasst und § 15 eingefügt worden ist, verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz im Einvernehmen mit dem Minister der Finanzen und dem Minister des Innern:

#### § 1  
Bestimmung der zentralen Einrichtung

Zentrale Einrichtung für die Organisation der Entsorgung von Sonderabfällen im Land Brandenburg ist die SBB Sonderabfallgesellschaft Brandenburg/Berlin mbH mit Sitz in Potsdam.

#### § 2   
Aufgaben der zentralen Einrichtung

(1) Die zentrale Einrichtung nimmt insbesondere folgende Aufgaben wahr:

1.  die Zuweisung der von den Abfallbesitzern ordnungsgemäß angedienten gefährlichen Abfälle (Sonderabfälle) in dafür zugelassene und aufnahmebereite Abfallentsorgungsanlagen,
    
2.  die Sicherung ausreichender Entsorgungsmöglichkeiten,
    
3.  die Information und Beratung von Andienungspflichtigen und Abfallentsorgungsunternehmen über die Möglichkeiten der Vermeidung, Verwertung und Beseitigung von gefährlichen Abfällen,
    
4.  die Mitarbeit an Entsorgungskonzepten.
    

(2) Neben den durch diese Verordnung zugewiesenen Aufgaben nimmt die zentrale Einrichtung die durch die Abfall- und Bodenschutz-Zuständigkeitsverordnung zugewiesenen Aufgaben wahr, die im Zusammenhang stehen mit

1.  der Überprüfung der ordnungsgemäßen Entsorgung von gefährlichen Abfällen,
    
2.  der Nachweisführung für Abfälle,
    
3.  der grenzüberschreitenden Verbringung von Abfällen und
    
4.  der Entscheidung über Transportgenehmigungen und Genehmigungen für Vermittlungsgeschäfte.
    

#### § 3  
Andienungspflicht

(1) Der Andienungspflicht unterliegen

1.  gefährliche Abfälle zur Beseitigung im Sinne des Kreislaufwirtschafts- und Abfallgesetzes sowie
    
2.  von der zuständigen Behörde im Einzelfall (in Abweichung vom europäischen Abfallverzeichnis) als gefährlich eingestufte Abfälle zur Beseitigung,
    

die im Land Brandenburg erzeugt worden sind oder im Land Brandenburg entsorgt werden sollen.
 Die zentrale Einrichtung ist zur Feststellung befugt, ob Abfälle der Andienungspflicht unterliegen.
Damit stellt sie auch die Gefährlichkeit der Abfälle fest.
Besteht eine Andienungspflicht, kann sie die Andienung der betreffenden Abfälle anordnen.

(2) Abweichend von Absatz 1 unterliegen der Andienungspflicht nicht

1.  die durch Rechtsvorschrift geregelte Rücknahme von Abfällen,
    
2.  Abfälle, die in den, aus dem oder durch den Geltungsbereich des Kreislaufwirtschafts- und Abfallgesetzes verbracht werden (grenzüberschreitende Verbringung),
    
3.  Abfälle aus der Sanierung von schädlichen Bodenveränderungen und Altlasten im Sinne des Bundes-Bodenschutzgesetzes, die im Bereich der von der Altlastensanierung betroffenen Fläche nach Behandlung (on-site-Behandlung) oder ohne vorherige Behandlung auf dem Sanierungsgrundstück wieder eingebracht werden sollen, wenn durch einen für verbindlich erklärten Sanierungsplan im Sinne des § 13 oder eine Anordnung zur Durchsetzung der Pflichten des § 4 des Bundes-Bodenschutzgesetzes sichergestellt wird, dass das Wohl der Allgemeinheit nicht beeinträchtigt wird.
    

(3) Andienungspflichtig sind der Erzeuger und der Besitzer von Abfällen, die gemäß Absatz 1 der Andienungspflicht unterliegen.
Die Notwendigkeit zur Andienung entfällt für den Pflichtigen, wenn der jeweils andere Pflichtige im Sinne von Satz 1 die Andienung für denselben Abfall des betreffenden Entsorgungsvorgangs bereits vorgenommen hat.
Die Pflichten der Erzeuger und Besitzer nach dem Kreislaufwirtschafts- und Abfallgesetz zur Abfallvermeidung und zur ordnungsgemäßen Entsorgung von Abfällen bleiben hiervon unberührt.

(4) Abweichend von Absatz 3 ist andienungspflichtig

1.  der Einsammler, wenn ein Sammelentsorgungsnachweis geführt wird,
    
2.  der öffentlich-rechtliche Entsorgungsträger für gefährliche Abfälle, die ihm überlassen wurden oder die er im Rahmen der Sammlung nach § 3 Absatz 3 Satz 2 und 3 des Brandenburgischen Abfall- und Bodenschutzgesetzes angenommen hat,
    
3.  der freiwillig zurücknehmende Hersteller oder Vertreiber von gefährlichen Abfällen; die zentrale Einrichtung kann in diesem Fall Abweichungen von den Anforderungen des § 4 zulassen.
    

#### § 4   
Verfahren der Andienung

(1) Anzudienen ist der betreffende Abfall spätestens nachdem er angefallen ist.
Dies geschieht durch Übersendung der notwendigen Unterlagen im Sinne der Nachweisverordnung und einer schriftlichen Erklärung, den Abfall anzudienen.

(2) Der Abfallerzeuger oder -besitzer darf zur Durchführung des Andienungsverfahrens einen Vertreter bevollmächtigen.
 Bevollmächtigen kann der Andienungspflichtige die zentrale Einrichtung auch mit der Einholung der Annahmeerklärung und der Behördenbestätigung.

(3) Die zentrale Einrichtung hat dem Andienungspflichtigen innerhalb von zehn Arbeitstagen den Eingang der Andienung unter Angabe des Datums zu bestätigen.
Sie hat nach Eingang unverzüglich zu prüfen, ob die Andienungsunterlagen den Anforderungen entsprechen.
Sind die Unterlagen nicht vollständig, so fordert sie den Andienungspflichtigen unverzüglich auf, die Unterlagen innerhalb einer angemessenen Frist zu vervollständigen.

(4) Die Absätze 1 bis 3 gelten entsprechend bei der Verwendung von Sammelentsorgungsnachweisen und bei einer Nachweisführung im privilegierten Verfahren.

(5) Wird der Nachweis elektronisch geführt, so erfolgt auch die Andienung durch Übersendung elektronischer Unterlagen.
Einzelheiten hierzu kann die oberste Abfallwirtschaftsbehörde im Amtsblatt für Brandenburg bekannt geben.

#### § 5  
Zuweisung durch die zentrale Einrichtung

(1) Die zentrale Einrichtung weist die ihr ordnungsgemäß angedienten Abfälle unter Einhaltung der Voraussetzungen nach Absatz 3 durch Bescheid dafür zugelassenen und annahmebereiten Abfallentsorgungsanlagen zu.
Der andienungspflichtige Abfallerzeuger und -besitzer ist verpflichtet, die Abfälle der zugewiesenen Abfallentsorgungs­anlage zuzuführen.
Abfallentsorger dürfen der Andienungspflicht unterliegende Abfälle nur annehmen, wenn sie von der zentralen Einrichtung zugewiesen sind.

(2) Die zentrale Einrichtung soll innerhalb von 30 Kalendertagen nach Eingang der Andienungsunterlagen über die Zuweisung entscheiden.
Fordert die zentrale Einrichtung den Andienungspflichtigen zur Ergänzung der Andienungsunterlagen auf, so wird die Frist nach Satz 1 unterbrochen.
Hat der Andienungspflichtige die zentrale Einrichtung nach § 4 Absatz 2 mit der Einholung einer Annahmeerklärung und einer behördlichen Bestätigung bevollmächtigt, so beginnt die Frist nach Satz 1 mit Eingang der vollständigen Annahmeerklärung und behördlichen Bestätigung bei der zentralen Einrichtung.

(3) Voraussetzung der Zuweisung ist, dass die beabsichtigte Abfallentsorgung ordnungsgemäß ist und den Zielen der Abfallwirtschaftsplanung entspricht.
Maßgebend sind dafür insbesondere die Anforderungen an die Verwertung und die Beseitigung von Abfällen nach dem Kreislaufwirtschafts- und Abfallgesetz und den auf Grund dieses Gesetzes ergangenen Rechtsverordnungen, die Grundsätze in Artikel 39 Absatz 6 der Verfassung des Landes Brandenburg, die Ziele des Brandenburgischen Abfall- und Bodenschutzgesetzes, insbesondere der Vorrang der Abfallbeseitigung in den Ländern Brandenburg und Berlin, sowie die Inhalte der Abfallwirtschaftspläne des Landes.
Bei der Entscheidung soll die zentrale Einrichtung die Angaben des Abfallbesitzers nach § 4 Absatz 1 Satz 2 berücksichtigen, ist aber nicht daran gebunden.
Sie ist nach Anhörung des Andienungspflichtigen zur Zuweisung in eine andere annahme­bereite Entsorgungsanlage befugt, wenn diese nach den in den Sätzen 1 und 2 genannten Kriterien zur Entsorgung besser geeignet ist.

(4) Die Zuweisung kann mit Nebenbestimmungen verbunden werden.
Sie soll in der Regel nur befristet erfolgen und ist mit einem Widerrufsvorbehalt und mit einer Meldepflicht für den Fall zu versehen, dass der andienungspflichtige Abfallbesitzer die vorgehaltenen Entsorgungskapazitäten nur noch teilweise oder gar nicht mehr nutzt oder nutzen kann.
Durch Auflagen kann auch festgelegt werden, wie die Abfälle der zugewiesenen Abfallentsorgungsanlage zuzuführen sind, und insbesondere eine Vorbehandlung verlangt werden.
Es kann die Vorlage eines Beleges über die Kosten der Entsorgung gefordert werden.

#### § 6  
Zurückweisung angedienter Abfälle

(1) Kann eine Zuweisung unter Einhaltung der in § 5 Absatz 3 genannten Voraussetzungen nicht erfolgen, so weist die zentrale Einrichtung die angedienten Abfälle zurück.
Sie kann die Zuweisung auch dann zurückweisen, wenn die Andienung durch den Abfallbesitzer nicht den Anforderungen des § 4 entspricht, der Andienungspflichtige Anordnungen oder Auflagen der zuständigen Behörde oder der zentralen Einrichtung nicht beachtet oder ein sonstiger wichtiger Grund vorliegt.

(2) Bei einer Zurückweisung bleibt die Andienungspflicht gemäß § 3 bestehen.

#### § 7  
Auskunfts- und Untersuchungspflichten

(1) Erzeuger oder Besitzer von gefährlichen Abfällen, Entsorgungsträger oder mit der Entsorgung beauftragte Dritte sind verpflichtet,

1.  der zentralen Einrichtung Auskünfte im Sinne des § 40 Absatz 2 Satz 1 des Kreislaufwirtschafts- und Abfall­gesetzes zu erteilen; die Rechte nach § 40 Absatz 2 Satz 2 bis 4 des Kreislaufwirtschafts- und Abfallgesetzes bleiben der zuständigen Behörde vorbehalten.
    Der zur Erteilung einer Auskunft Verpflichtete kann die Auskunft auf solche Fragen verweigern, deren Beantwortung ihn selbst oder einen in § 383 Absatz 1 Nummer 1  
    bis 3 der Zivilprozessordnung bezeichneten Angehörigen der Gefahr strafgerichtlicher Verfolgung oder eines Verfahrens nach dem Gesetz über Ordnungswidrigkeiten aussetzen würde,
    
2.  auf Verlangen der zentralen Einrichtung Analysen zur Beurteilung der angedienten Abfälle zu erstellen oder auf eigene Kosten durch Dritte, die von der zentralen Stelle benannt werden können, erstellen zu lassen.
    

(2) Die zentrale Einrichtung ist befugt, auf Kosten der in Absatz 1 genannten Personen den angedienten Abfällen Proben zu entnehmen oder entnehmen zu lassen.

(3) Die Absätze 1 und 2 gelten entsprechend im Rahmen der nach § 3 Absatz 1 Satz 2 vorzunehmenden Feststellungen über das Bestehen einer Andienungspflicht.

#### § 8  
Beratung und Organisation

(1) Zur Organisation der Sonderabfallentsorgung führt die zentrale Einrichtung insbesondere folgende Aufgaben aus:

1.  die Beratung der Erzeuger und Besitzer gefährlicher Abfälle über Möglichkeiten der Vermeidung, Verwertung und Beseitigung dieser Abfälle,
    
2.  die Entgegennahme, Erfassung, Prüfung und Auswertung der Begleitscheine nach den §§ 10 bis 13 und 21 der Nachweisverordnung nach den Anforderungen der obersten Abfallwirtschaftsbehörde,
    
3.  die sonstige Auswertung der Abfallströme zur Erstellung von Statistiken zur Sonderabfallentsorgung, insbesondere unter Berücksichtigung der Möglichkeiten zur Vermeidung und Verwertung von Sonderabfällen, in Abstimmung mit der obersten Abfallwirtschaftsbehörde,
    
4.  die Mitwirkung an der Entwicklung neuer Möglichkeiten zur Vermeidung und Verwertung von Sonderabfällen, insbesondere durch die Erarbeitung von Broschüren, Konzepten und durch die Vergabe von Forschungsauf­trägen.
    

(2) Die zentrale Einrichtung hat der obersten Abfallwirtschaftsbehörde regelmäßig über alle die Sonderabfallentsorgung betreffenden wesentlichen Vorgänge in ihrer Geschäftstätigkeit zu berichten, ihr auf Verlangen Auskünfte zu erteilen und Unterlagen zur Verfügung zu stellen sowie die Weisungen der Aufsichtsbehörde zu befolgen.

#### § 9  
Abfallwirtschaftskonzept und Abfallbilanz

(1) Die zentrale Einrichtung erstellt jährlich bis zum 1.
 April jeweils für das vergangene Jahr eine Abfallbilanz über die angedienten und entsorgten gefährlichen Abfälle.
Hierzu sind die vorhandenen Informationen auszuwerten, und zwar insbesondere auf

1.  Art und Menge der verwerteten und beseitigten Abfälle,
    
2.  Herkunft der Abfälle und
    
3.  Entsorgungswege.
    

(2) Die zentrale Einrichtung erstellt bis zum 31.
Dezember 2010 ein Abfallwirtschaftskonzept zu den andienungspflichtigen Abfällen.
Dieses umfasst insbesondere eine Prognose über einen Zeitraum von fünf Jahren

1.  zu Art und Menge der anfallenden Abfälle,
    
2.  zur Vermeidung und Verwertung von Abfällen und
    
3.  zur Auslastung von Entsorgungsanlagen und zum Anlagenbedarf.
    

(3) Ausgehend von den Zuweisungskriterien nach § 5 Absatz 3 soll die zentrale Einrichtung in dem Abfallwirtschaftskonzept ihre abfallwirtschaftlichen Ziele und die beabsichtigten Maßnahmen ihrer Umsetzung darlegen.
Das Abfallwirtschaftskonzept ist im Hinblick auf die Abfallwirtschaftsplanung der obersten Abfallwirtschaftsbehörde vorzulegen.
Es ist nach Bedarf, spätestens aber jeweils nach fünf Jahren zu aktualisieren.

#### § 10  
Kosten der zentralen Einrichtung

Die zentrale Einrichtung erhebt gemäß § 15 Absatz 3 des Brandenburgischen Abfall- und Bodenschutzgesetzes für die Aufgaben nach dieser Verordnung von den andienungspflichtigen Abfallbesitzern Gebühren und Auslagen (Kosten) auf der Grundlage einer gesonderten Verordnung.

#### § 11   
Bekanntmachung von Formularen

Die zentrale Einrichtung macht die zum Vollzug dieser Verordnung erforderlichen Formulare im Amtsblatt für Brandenburg bekannt.

#### § 12   
Ordnungswidrigkeiten

Ordnungswidrig im Sinne des § 48 Absatz 1 Nummer 1 des Brandenburgischen Abfall- und Bodenschutzgesetzes handelt, wer vorsätzlich oder fahrlässig

1.  entgegen § 3 Absatz 1 und 3 andienungspflichtige Abfälle nicht andient oder entgegen § 4 Absatz 1 nicht rechtzeitig andient,
    
2.  entgegen § 5 Absatz 1 Satz 2 angediente Abfälle einer anderen als der zugewiesenen Entsorgungsanlage oder ohne Zuweisung Abfälle einer Entsorgungsanlage zuführt,
    
3.  entgegen § 5 Absatz 1 Satz 3 andienungspflichtige Abfälle ohne Zuweisung oder entgegen einer Zuweisung annimmt,
    
4.  eine vollziehbare Auflage nach § 5 Absatz 4 nicht, nicht richtig, nicht vollständig oder nicht rechtzeitig erfüllt,
    
5.  entgegen § 7 Absatz 1 Nummer 1, auch in Verbindung mit § 7 Absatz 3, eine Auskunft nicht, nicht richtig oder nicht vollständig erteilt,
    
6.  entgegen § 7 Absatz 1 Nummer 2, auch in Verbindung mit § 7 Absatz 3, Analysen nicht oder nicht richtig erstellt oder erstellen lässt.
    

#### § 13  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am ersten Tag des auf die Verkündung folgenden zweiten Monats in Kraft.
 Gleichzeitig tritt die Verordnung vom 3.
Mai 1995 (GVBl. II S. 404), die zuletzt durch Artikel 3 Absatz 7 des Gesetzes vom 27.
Mai 2009 (GVBl.
I S. 175, 184) geändert worden ist, außer Kraft.

Potsdam, den 8.
Januar 2010

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack