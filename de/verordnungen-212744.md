## Verordnung über die Vergütungsstufen der Ausbildungsbeihilfe, des Arbeitsentgelts und der finanziellen Anerkennung nach dem Brandenburgischen Justizvollzugsgesetz und nach dem Brandenburgischen Sicherungsverwahrungsvollzugsgesetz (Brandenburgische Justizvollzugs- und Sicherungsverwahrungsvollzugsvergütungsordnung - BbgJVollzSVVergO )

Auf Grund des § 66 Absatz 3 Satz 3 des Brandenburgischen Justizvollzugsgesetzes vom 24.
April 2013 (GVBl.
I Nr.
14) und des § 60 Absatz 3 Satz 3 des Brandenburgischen Sicherungsverwahrungsvollzugsgesetzes vom 16.
Mai 2013 (GVBl.
I Nr.
17) verordnet der Minister der Justiz:

§ 1  
Arbeitsentgelt
--------------------

(1) Der Grundlohn des Arbeitsentgelts nach § 66 Absatz 1 Nummer 1 des Brandenburgischen Justizvollzugsgesetzes und nach § 60 Absatz 1 Nummer 3 des Brandenburgischen Sicherungsverwahrungsvollzugsgesetzes wird nach folgenden Vergütungsstufen festgesetzt:

Vergütungsstufe A1:

Arbeiten einfacher Art, die keine Vorkenntnisse und nur eine kurze Einweisungs- oder Einarbeitungszeit erfordern und die nur geringe Anforderungen an die körperliche oder geistige Leistungsfähigkeit oder an die Geschicklichkeit stellen;

Vergütungsstufe A2:

Arbeiten, die eine Anlernzeit erfordern und durchschnittliche Anforderungen an die Leistungsfähigkeit und die Geschicklichkeit stellen;

Vergütungsstufe A3:

Arbeiten, die die Kenntnisse und Fähigkeiten eines Facharbeiters erfordern oder gleichwertige Kenntnisse und Fähigkeiten voraussetzen und die ein besonderes Maß an Können, Einsatz und Verantwortung erfordern.

(2) Der Grundlohn beträgt in der

Vergütungsstufe A1

80 Prozent,

Vergütungsstufe A2

100 Prozent,

Vergütungsstufe A3

120 Prozent

der Eckvergütung nach § 66 Absatz 2 Satz 1 des Brandenburgischen Justizvollzugsgesetzes für Gefangene und nach § 60 Absatz 2 Satz 1 des Brandenburgischen Sicherungsverwahrungsvollzugsgesetzes für Untergebrachte.

§ 2  
Zulagen
-------------

Zum Grundlohn können Zulagen gewährt werden

1.  für Arbeiten unter arbeitserschwerenden Umgebungseinflüssen, die das übliche Maß erheblich übersteigen, bis zu 5 Prozent des Grundlohnes,
2.  für Arbeiten zu ungünstigen Zeiten bis zu 5 Prozent des Grundlohnes,
3.  für Zeiten, die über die festgesetzte Arbeitszeit hinausgehen, bis zu 25 Prozent des Grundlohnes.

§ 3  
Ausbildungsbeihilfe
-------------------------

(1) Die Ausbildungsbeihilfe nach § 66 Absatz 1 Nummer 2 des Brandenburgischen Justizvollzugsgesetzes und nach § 60 Absatz 1 Nummer 2 des Brandenburgischen Sicherungsverwahrungsvollzugsgesetzes wird nach folgenden Vergütungsstufen festgesetzt:

Vergütungsstufe B1:

arbeitstherapeutische Maßnahmen; Arbeitstraining; schulische und berufliche Qualifizierungsmaßnahmen mit geringen Leistungsanforderungen;

Vergütungsstufe B2:

Arbeitstraining, wenn der Arbeitsfortschritt dies rechtfertigt; schulische und berufliche Qualifizierungsmaßnahmen der Vergütungsstufe B1 nach der Hälfte der Gesamtdauer der Maßnahme, wenn der Lernfortschritt dies rechtfertigt; schulische und berufliche Qualifizierungsmaßnahmen, die nicht zu formalen Abschlüssen führen oder diese vorbereiten, aber zusätzliche schulische, berufliche und soziale Leistungen erfordern;

Vergütungsstufe B3:

schulische und berufliche Qualifizierungsmaßnahmen der Vergütungsstufe B2 nach der Hälfte der Gesamtdauer der Maßnahme, wenn der Lernfortschritt dies rechtfertigt; schulische und berufliche Qualifizierungsmaßnahmen, die zu formalen Abschlüssen führen oder diese vorbereiten;

Vergütungsstufe B4:

schulische und berufliche Qualifizierungsmaßnahmen, die zu formalen Abschlüssen führen oder diese vorbereiten, nach der Hälfte der Gesamtdauer der Maßnahme, wenn der Lernfortschritt des Gefangenen dies rechtfertigt.

(2) Die Ausbildungsbeihilfe beträgt in der

Vergütungsstufe B1

80 Prozent,

Vergütungsstufe B2

90 Prozent,

Vergütungsstufe B3

100 Prozent,

Vergütungsstufe B4

110 Prozent

der Eckvergütung nach § 66 Absatz 2 Satz 1 des Brandenburgischen Justizvollzugsgesetzes für Gefangene und nach § 60 Absatz 2 Satz 1 des Brandenburgischen Sicherungsverwahrungsvollzugsgesetzes für Untergebrachte.

(3) Für die Gewährung von Zulagen gilt § 2 entsprechend.

§ 4  
Finanzielle Anerkennung
-----------------------------

Die finanzielle Anerkennung nach § 66 Absatz 1 Nummer 3 des Brandenburgischen Justizvollzugsgesetzes und nach § 60 Absatz 1 Nummer 1 des Brandenburgischen Sicherungsverwahrungsvollzugsgesetzes beträgt 100 Prozent der Eckvergütung nach § 66 Absatz 2 Satz 1 des Brandenburgischen Justizvollzugsgesetzes für Gefangene und nach § 60 Absatz 2 Satz 1 des Brandenburgischen Sicherungsverwahrungsvollzugsgesetzes für Untergebrachte.

§ 5  
Inkrafttreten, Außerkrafttreten
-------------------------------------

Diese Verordnung tritt am 1.
Juni 2013 in Kraft.
Gleichzeitig tritt die Brandenburgische Jugendstraf- und Untersuchungshaftvollzugsvergütungsverordnung vom 13.
Dezember 2010 (GVBl.
II Nr.
88) außer Kraft.

Potsdam, den 22.
Mai 2013

Der Minister der Justiz

Dr.
Volkmar Schöneburg