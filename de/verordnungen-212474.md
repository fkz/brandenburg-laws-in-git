## Verordnung über die Anforderungen an die Strukturqualität in Einrichtungen und ihnen gleichgestellten  Wohnformen nach dem Brandenburgischen Pflege- und Betreuungswohngesetz  (Strukturqualitätsverordnung - SQV)

Auf Grund des § 9 Absatz 3 Nummer 1 und 2 des Brandenburgischen Pflege- und Betreuungswohngesetzes vom  
8.
Juli 2009 (GVBl.
I S. 298) verordnet der Minister für Arbeit, Soziales, Frauen und Familie im Einvernehmen mit dem Minister der Finanzen:

**Inhaltsübersicht**

[§ 1   Allgemeine Grundsätze](#_Toc275866672)  
[§ 2   Leitung](#_Toc275866674)  
[§ 3   Beschäftigte und sonstige Mitarbeiterinnen und Mitarbeiter](#_Toc275866676)  
[§ 4   Fachkräfte](#_Toc275866678)  
[§ 5   Anwesenheit von Fachkräften](#_Toc275866680)  
[§ 6   Fort- und Weiterbildung](#_Toc275866682)  
[§ 7   Persönliche Ausschlussgründe](#_Toc275866684)  
[§ 8   Wohnflächen und Ausstattung](#_Toc275866686)  
[§ 9   Räume zur besonderen Nutzung](#_Toc275866688)  
[§ 10   Bewegungsfreiheit](#_Toc275866690)  
[§ 11   Sanitäre Ausstattung](#_Toc275866692)  
[§ 12   Zugang zu Kommunikations- und Informationsmedien](#_Toc275866694)  
[§ 13   Ordnungswidrigkeiten](#_Toc275866696)  
[§ 14   Übergangsvorschriften](#_Toc275866698)  
[§ 15   Inkrafttreten](#_Toc275866700)

#### § 1  
Allgemeine Grundsätze

Einrichtungen und den Einrichtungen gleichgestellte Wohnformen nach § 4 des Brandenburgischen Pflege- und Betreuungswohngesetzes müssen baulich, sächlich und personell so ausgestattet sein, dass den Bewohnerinnen und Bewohnern ein selbstbestimmtes Leben und die Teilhabe am Leben in der Gemeinschaft ermöglicht wird.
Die Ausstattung muss die erforderliche Unterstützung sichern und der Wahrung und Verwirklichung der Persönlichkeitsrechte der Bewohnerinnen und Bewohner und ihrem Bedarf nach autonomer Lebensführung förderlich sein.

#### § 2  
Leitung

(1) Der Leistungsanbieter hat die kompetente und zuverlässige Leitung der Einrichtung durch fachlich und persönlich geeignetes Personal mit entsprechenden Führungskompetenzen sicherzustellen.

(2) Die Verantwortungsbereiche und die Entscheidungsbefugnisse der Leitung umfassen insbesondere

1.  die Koordinierung und die Kontrolle der die Einrichtung betreffenden übergreifenden Betriebsabläufe wie Verwaltung, Wirtschaft, Personalführung und Vertretung der Einrichtung nach außen sowie
    
2.  die Steuerung und Kontrolle der Pflege- und Betreuungsprozesse.
    

Es ist sicherzustellen, dass die nötigen Leitungsentscheidungen in räumlicher und zeitlicher Nähe getroffen werden können.
Hierfür ist eine der Größe, der Betriebsorganisation und der Lebenswirklichkeit der Bewohnerinnen und Bewohner angepasste Anwesenheit und Erreichbarkeit der Leitung für Bewohnerinnen und Bewohner, Beschäftigte, Angehörige und Dritte zu gewährleisten.

(3) Zur Wahrnehmung von Leitungsaufgaben nach Absatz 2 Satz 1 Nummer 1 ist fachlich geeignet, wer

1.  eine mindestens dreijährige berufliche Qualifikation mit staatlich anerkanntem Abschluss in einem Pflegeberuf oder in einem sozialen Beruf mit sozialpflegerischer Ausrichtung und jeweils eine betriebswirtschaftliche Zusatzqualifikation,
    
2.  eine mindestens dreijährige berufliche Qualifikation mit staatlich anerkanntem Abschluss in einem kaufmännischen Beruf oder in einem Beruf der öffentlichen Verwaltung und jeweils eine sozialpflegerische Zusatzqualifikation oder
    
3.  einen zumindest mit dem Bachelor-Grad abgeschlossenen, akkreditierten oder staatlich anerkannten Studiengang mit gesundheits-, pflege- oder sozialwirtschaftlichem Schwerpunkt
    

nachweisen kann und über Führungskompetenzen verfügt.
Führungskompetenzen werden durch eine mindestens zweijährige hauptberufliche Tätigkeit in vergleichbaren Einrichtungen oder durch den Abschluss einer Zusatzqualifikation mit einem Umfang von mindestens 720 Stunden erworben, sofern hierbei die für die Leitung erforderlichen Kenntnisse und Fähigkeiten vermittelt werden.

(4) Zur Wahrnehmung von Leitungsaufgaben nach Absatz 2 Satz 1 Nummer 2 ist fachlich geeignet, wer eine Ausbildung zur Fachkraft im Gesundheits- oder Sozialwesen mit staatlich anerkanntem Abschluss nachweisen kann und über Führungskompetenzen durch eine mindestens zweijährige hauptberufliche Tätigkeit verfügt.

(5) Leben in der Einrichtung mehr als 80 Bewohnerinnen und Bewohner ist die gleichzeitige Wahrnehmung von Aufgaben nach Absatz 2 Satz 1 Nummer 1 und 2 unzulässig.
In Einrichtungen für pflegebedürftige Menschen sind in diesem Fall die Aufgaben nach Absatz 2 Satz 1 Nummer 2 durch eine vollzeitbeschäftigte Person wahrzunehmen, die über einen Hochschulabschluss im Bereich der Pflege mit pflegerischem Grundberuf verfügt.

#### § 3  
Beschäftigte und sonstige Mitarbeiterinnen und Mitarbeiter

(1) Der Leistungsanbieter muss dafür sorgen, dass die Beschäftigten und die sonstigen Mitarbeiterinnen und Mitarbeiter die erforderliche persönliche und fachliche Eignung für die von ihnen ausgeübte Funktion und Tätigkeit besitzen.
Er hat sicherzustellen, dass sie die körperliche Unversehrtheit, die Würde, das Eigentum und die persönliche Integrität der Bewohnerinnen und Bewohner achten und deren geschlechtliche, ethnische, religiöse und sexuelle Identität respektieren.

(2) Die Pflichterfüllung nach Absatz 1 wird vermutet, wenn vor der Einstellung von Beschäftigten die persönlichen Ausschlussgründe nach § 7 durch den Leistungsanbieter überprüft wurden sowie durch Handlungsanweisungen und fortlaufende Fortbildung der Beschäftigten die Voraussetzungen für eine kultur- und geschlechtssensible Betreuung sowie für den sicheren Umgang mit Patientenverfügungen, Vollmachten und den Einsatz freiheitsentziehender Maßnahmen gesichert werden.

(3) Die Beschäftigten und die sonstigen Mitarbeiterinnen und Mitarbeiter müssen die für den sicheren Betrieb der Einrichtung erforderlichen Kenntnisse, insbesondere über das Brandschutzkonzept der Einrichtung haben.
Sollten die erforderlichen Kenntnisse nicht vorliegen, hat der Leistungsanbieter für entsprechende Schulungen Sorge zu tragen.

#### § 4  
Fachkräfte

(1) Pflegende und betreuende Tätigkeiten dürfen nur durch Fachkräfte oder unter angemessener Beteiligung von Fachkräften geleistet werden.
Fachkräfte im Sinne dieser Verordnung müssen über den staatlich anerkannten Abschluss einer einschlägigen Berufsausbildung verfügen, die Kenntnisse und Fähigkeiten zur selbstständigen und eigenverantwortlichen Wahrnehmung der von ihnen ausgeübten Funktion und Tätigkeit vermittelt.
Die einschlägigen Berufsabschlüsse werden durch Erlass des für Soziales zuständigen Mitglieds der Landesregierung benannt.

(2) Ausschließlich von Fachkräften wahrzunehmende Aufgaben sind:

1.  die Festlegung von Zielen und Maßnahmen in Pflege- und Betreuungsprozessen sowie die Evaluation durchgeführter Pflege- und Betreuungsmaßnahmen,
    
2.  die Beratung der Bewohnerinnen und Bewohner über fachlich begründete Maßnahmen zur Sicherung der gesundheitlichen und psychosozialen Versorgung sowie die Mitwirkung bei Entscheidungen über deren Anwendung,
    
3.  die Überwachung der Erforderlichkeit und Angemessenheit zulässiger freiheitsentziehender Maßnahmen,
    
4.  die Weitergabe personenbezogener Informationen über Bewohnerinnen und Bewohner an weiter- oder nachbetreuende Dienste oder Einrichtungen und
    
5.  die Anleitung, Aufsicht und Kontrolle von Hilfskräften und der sonstigen Beschäftigten oder Mitarbeiterinnen und Mitarbeiter.
    

(3) Absatz 1 Satz 1 gilt als eingehalten, wenn mindestens 50 Prozent der mit pflegenden oder betreuenden Tätigkeiten Beschäftigten Fachkräfte im Sinne dieser Verordnung sind und qualifikationsgerecht eingesetzt werden, sofern nicht ein außerordentlicher Pflege- oder Betreuungsbedarf eine darüber hinausgehende Beteiligung von Fachkräften erforderlich macht.
Die Berechnung erfolgt anhand der Vollzeitäquivalente.
Zusätzliches Betreuungspersonal im Sinne des § 87b Absatz 1 Satz 2 Nummer 2 des Elften Buches Sozialgesetzbuch und Beschäftigte, die ausschließlich Leistungen nach § 45b Absatz 1 Satz 6 Nummer 3 des Elften Buches Sozialgesetzbuch erbringen, bleiben bei der Berechnung unberücksichtigt.

(4) Bei Abweichungen von der Annahme des Absatzes 3 muss der Leistungsanbieter jederzeit nachweisen können, dass die Gestaltung und Umsetzung von Pflege- und Betreuungsprozessen nach dem anerkannten Stand der Erkenntnisse unter Beachtung der Anforderungen des Absatzes 2 sichergestellt ist.
Hierfür ist die Planung und Umsetzung eines nach Qualifikation und Funktion differenzierten Personaleinsatzes nachzuweisen.
Der Einsatz von Schülerinnen und Schülern, die sich im dritten Ausbildungsjahr eines zur Fachkraft qualifizierenden Berufes befinden, kann dabei angemessen berücksichtigt werden.

#### § 5  
Anwesenheit von Fachkräften

(1) Durch die Anwesenheit von Fachkräften muss sichergestellt sein, dass Bewohnerinnen und Bewohner zu jeder Tages- und Nachtzeit krankheits- oder behinderungsbedingt erforderlich werdende Hilfe und Unterstützung erhalten.

(2) Hierfür muss in Einrichtungen, in denen sich die Bewohnerinnen und Bewohner rund um die Uhr aufhalten, auch nachts mindestens eine Fachkraft anwesend sein.

(3) Die Erfüllung der Anforderung des Absatzes 1 kann auch durch den Nachweis erfolgen, dass nach dem tatsächlichen Pflege- oder Betreuungsbedarf die unverzügliche Herbeiholung einer Fachkraft in Notfallsituationen ausreicht und gewährleistet ist.

#### § 6  
Fort- und Weiterbildung

(1) Der Leistungsanbieter ist verpflichtet, den Beschäftigten den Besuch von Fort- und Weiterbildungen, die sie zur Ausübung der Pflege und Betreuung nach dem aktuell anerkannten Stand der Erkenntnisse benötigen, zu ermöglichen.

(2) Eine ausreichende Fort- und Weiterbildungsmöglichkeit wird vermutet, wenn ein Fort- und Weiterbildungskonzept mit folgenden Themen und Tätigkeitsfeldern nachgewiesen wird:

1.  laufende berufsspezifische Fortbildungen der Beschäftigten; in Einrichtungen, in denen Pflegeleistungen erbracht werden insbesondere zu den Qualitätsanforderungen an eine aktivierende Pflege nach dem Elften Buch Sozialgesetzbuch und zu den Grundsätzen der gerontopsychiatrischen Pflege,
    
2.  Grundsätze der Umsetzung des Gesetzes zu dem Übereinkommen der Vereinten Nationen vom 13.
    Dezember 2006 über die Rechte von Menschen mit Behinderungen sowie zu dem Fakultativprotokoll vom 13.
     Dezember 2006 zum Übereinkommen der Vereinten Nationen über die Rechte von Menschen mit Behinderungen vom 21.
    Dezember 2008 (BGBl.
    II S. 1419) sowie der Charta der Rechte hilfe- und pflegebedürftiger Menschen,
    
3.  Reflexion von Pflege- und Betreuungsbeziehungen,
    
4.  Würde, Persönlichkeitsrechte und Interessen pflegebedürftiger und behinderter Menschen in Krisensituationen, insbesondere im Umgang mit Sterben und Tod sowie mit Notlagen bei psychischen Erkrankungen und seelischen Behinderungen,
    
5.  Umgang mit Sexualität im Alter oder bei Behinderung,
    
6.  Umgang mit Patientenverfügungen,
    
7.  Umgang mit freiheitsentziehenden Maßnahmen,
    
8.  Hygiene und Infektionsschutz sowie
    
9.  Umgang mit Medikamenten.
    

(3) Beschäftigten in Einrichtungen, in denen alt gewordene oder pflegebedürftig gewordene Menschen mit Behinderungen leben, ist Gelegenheit zu einer altenpflegerischen Qualifizierung einzuräumen.

#### § 7  
Persönliche Ausschlussgründe

Bei den in der Einrichtung tätigen Personen dürfen keine Tatsachen vorliegen, die die Annahme rechtfertigen, dass sie für die Ausübung ihrer Tätigkeit persönlich ungeeignet sind.
Persönlich ungeeignet ist insbesondere,

1.  wer
    
    1.  wegen eines Verbrechens oder wegen einer Straftat gegen das Leben, die sexuelle Selbstbestimmung oder die persönliche Freiheit, wegen vorsätzlicher Körperverletzung, wegen Diebstahls, Unterschlagung, Raubes, Erpressung, Hehlerei, Betrugs, Untreue, wegen einer gemeingefährlichen Straftat oder wegen einer Straftat nach den §§ 29 bis 30b des Betäubungsmittelgesetzes oder
        
    2.  als Leitung der Einrichtung tätig ist oder tätig werden möchte und wegen Urkundenfälschung oder wegen einer Insolvenzstraftat
        
    
    zu einer Freiheitsstrafe von drei oder mehr Monaten rechtskräftig verurteilt worden ist und die Tilgung im Zentralregister noch nicht erledigt ist,
    
2.  das Leitungspersonal, gegen das wegen einer Ordnungswidrigkeit nach § 25 des Brandenburgischen Pflege- und Betreuungswohngesetzes mehr als zweimal eine Geldbuße rechtskräftig festgesetzt worden ist, soweit nicht fünf Jahre seit Rechtskraft des letzten Bußgeldbescheids vergangen sind.
    

#### § 8  
Wohnflächen und Ausstattung

(1) Der Leistungsanbieter hat dafür Sorge zu tragen, dass Größe und Ausstattung des unmittelbaren Wohnumfeldes und der gemeinschaftlichen Wohnflächen eine selbstständige Lebensführung der Bewohnerinnen und Bewohner ermöglichen und sich diese in ihrer Privatsphäre frei entfalten können.

(2) Das unmittelbare Wohnumfeld soll grundsätzlich einer Bewohnerin oder einem Bewohner zur Verfügung stehen.
Die Nutzung durch mehr als zwei Personen ist unzulässig.
 Durchgangszimmer dürfen nicht als unmittelbares Wohnumfeld genutzt werden.
Die Mitnahme persönlicher Gegenstände und Möbel zur Ausstattung des unmittelbaren Wohnumfeldes darf nicht untersagt werden, soweit berechtigte Interessen der Mitbewohnerin oder des Mitbewohners oder Sicherheitsbelange in der Einrichtung nicht entgegenstehen.
Die Bewohnerinnen und Bewohner müssen die Beleuchtung und die Raumtemperatur in ihrem unmittelbaren Wohnumfeld selbst regulieren können.

(3) Das unmittelbare Wohnumfeld muss mindestens eine Größe aufweisen, die ausreichend Platz für ein Bett, einen Kleiderschrank, Möbel zur Mediennutzung und Sitzgelegenheiten mit Tisch sowie genügend Fläche zur Fortbewegung entsprechend dem persönlichen Bedarf bietet.
Das wird vermutet, wenn die Wohnfläche 14 Quadratmeter, bei zwei Personen 24 Quadratmeter nicht unterschreitet und dabei ausreichend Stellfläche für die Unterbringung persönlicher Gegenstände und Möbel zur Verfügung steht.

(4) Gemeinschaftliche Wohnflächen sind Wohnküchen, Speiseräume, Terrassen, Balkone, Funktionsräume, Räume zur Tagesstrukturierung und sonstige zum gemeinsamen Aufenthalt geeignete Flächen.
Ihre Ausstattung muss eine individuelle Nutzung durch die Bewohnerinnen und Bewohner zulassen.
Dazu gehören insbesondere Kochgelegenheiten und ausreichend Stellfläche für die Unterbringung von Gegenständen zur hauswirtschaftlichen Versorgung und zur eigenen Wäscheversorgung.

(5) Die gemeinschaftlichen Wohnflächen müssen zusammen eine Größe aufweisen, dass alle Bewohnerinnen und Bewohner, deren unmittelbares Wohnumfeld im räumlichen und organisatorischen Zusammenhang zu den gemeinschaftlichen Wohnflächen steht, diese gleichzeitig nutzen können.
Das wird vermutet, wenn die Wohnfläche mindestens fünf Quadratmeter pro Person bemisst.

(6) In Räumen, die durch Bewohnerinnen und Bewohner genutzt werden, ist eine angemessene Temperatur durch allgemein übliche Vorkehrungen sicherzustellen.

(7) Abweichungen von der Annahme des Absatzes 3 Satz 2 oder des Absatzes 5 Satz 2 sind insbesondere bei kleinteilig ausgelegten Einrichtungen möglich, wenn sie in Wohnungen oder im sonstigen baulichen Bestand, der durch seine Lage eine Teilnahme am Leben in der örtlichen Gemeinschaft in besonderem Maße gewährleistet, errichtet worden sind.

#### § 9  
Räume zur besonderen Nutzung

(1) Die Einrichtung muss baulich so ausgestattet sein, dass die Würde, die Privatsphäre und das Selbstbestimmungsrecht der Bewohnerinnen und Bewohner insbesondere in krankheitsbedingten Krisensituationen und im Sterben gewahrt bleiben.

(2) Die Anforderungen des Absatzes 1 sind in der Regel erfüllt, wenn

1.  gewährleistet ist, dass Bewohnerinnen und Bewohnern, denen das unmittelbare Wohnumfeld nicht allein zur Verfügung steht, in Krisenfällen einen für Wohn- und Betreuungszwecke geeigneten Raum vorübergehend nutzen können; die Nutzungsmöglichkeit ist den Bewohnerinnen und Bewohnern auf geeignete Weise bekannt zu geben,
    
2.  für Bewohnerinnen und Bewohner, die auf ärztliche Versorgung in der Einrichtung angewiesen sind, die Gelegenheit für eine ungestörte ärztliche Untersuchung und Behandlung gegeben ist und
    
3.  in Einrichtungen der Pflege ein Verabschiedungs- und Totenraum vorhanden ist, sofern nicht im unmittelbaren Wohnumfeld eine Aufbahrung möglich ist.
    

#### § 10  
Bewegungsfreiheit

(1) Die bauliche Ausstattung muss den Bewohnerinnen und Bewohnern eine Bewegungsfreiheit ermöglichen, die ihren Fähigkeiten entspricht.

(2) Bauliche Einschränkungen des Zugangs zum öffentlichen Raum sind nicht zulässig.

(3) Die Anforderung des Absatzes 1 ist in der Regel erfüllt, wenn

1.  der Zugang sowie die individuell und gemeinschaftlich genutzten Wohnflächen barrierefrei sind,
    
2.  Flure und Treppen an beiden Seiten mit festen Handläufen versehen sind und
    
3.  für Bewohnerinnen und Bewohner mit beeinträchtigten kognitiven Fähigkeiten oder Sehbehinderung angemessene Orientierungshilfen vorgehalten werden.
    

(4) Von den Regelanforderungen nach Absatz 3 kann abgewichen werden, wenn Einschränkungen der Mobilität bei dem aufgenommenen Personenkreis typischerweise nicht zu erwarten sind und die Abweichung ausdrücklich mit den Bewohnerinnen und Bewohnern vereinbart worden ist.

#### § 11  
Sanitäre Ausstattung

(1) Der Leistungsanbieter hat sicherzustellen, dass den Bewohnerinnen und Bewohnern in unmittelbarer Nähe zu ihrem Wohnumfeld ausreichend Gelegenheiten zur Körperhygiene zur Verfügung stehen und diese individuell unter Wahrung der Intimsphäre genutzt werden können.

(2) Pflegebedürftigen Bewohnerinnen und Bewohnern ist Gelegenheit zur Nutzung einer ihren körperlichen Beeinträchtigungen angemessen ausgestatteten Badewanne innerhalb der Einrichtung zu geben.
§ 8 Absatz 7 findet mit der Maßgabe, dass die Abweichung mit den Bewohnerinnen und Bewohnern ausdrücklich vereinbart worden ist, entsprechende Anwendung.

#### § 12  
Zugang zu Kommunikations- und Informationsmedien

(1) Die Ausstattung der Einrichtung muss den Kontakt der Bewohnerinnen und Bewohner zu ihren Mitmenschen und den uneingeschränkten Zugang zu Informationen sicherstellen.

(2) Im unmittelbaren Wohnumfeld sind die technischen Voraussetzungen vorzuhalten, so dass die Bewohnerinnen und Bewohner Rundfunk, Fernsehen, Internet und Telefon durch vorhandene oder eigene Geräte individuell nutzen können.
Eine durch Dritte ungestörte Nutzung von Internet und Telefon ist zu ermöglichen.

#### § 13  
Ordnungswidrigkeiten

Ordnungswidrig im Sinne des § 25 Absatz 3 Nummer 1 des Brandenburgischen Pflege- und Betreuungswohngesetzes handelt, wer vorsätzlich oder fahrlässig

1.  entgegen § 2 Absatz 1 in Verbindung mit Absatz 3 Personen mit Leitungsaufgaben betraut,
    
2.  Personen beschäftigt, die nach § 7 persönlich ungeeignet sind,
    
3.  Tätigkeiten entgegen § 4 Absatz 2 nicht durch Fachkräfte oder entgegen § 4 Absatz 1 Satz 1 nicht unter Anleitung oder angemessener Beteiligung von Fachkräften wahrnehmen lässt oder
    
4.  eine Einrichtung betreibt, in der
    
    1.  entgegen § 8 Absatz 2 Satz 2 mehr als zwei Personen ein unmittelbares Wohnumfeld nutzen,
        
    2.  entgegen § 8 Absatz 2 Satz 4 die Mitnahme persönlicher Gegenstände oder eigener Möbel untersagt wird,
        
    3.  entgegen § 10 Absatz 2 der Zugang zum öffentlichen Raum baulich eingeschränkt ist oder
        
    4.  entgegen § 11 Absatz 1 die Mindestanforderungen an die sanitäre Ausstattung nicht vorgehalten werden.
        

#### § 14  
Übergangsvorschriften

(1) Die Anforderungen nach § 2 Absatz 3, 4 und 5 Satz 2 gelten nicht für Personen, die bei Inkrafttreten dieser Verordnung

1.  Leitungsaufgaben nach § 2 Absatz 2 Satz 1 Nummer 1 wahrnehmen und die Anforderungen des § 2 Absatz 2 der Heimpersonalverordnung vom 19.
    Juli 1993 (BGBl.
    I S. 1205), die durch Artikel 1 der Verordnung vom  
    22.
    Juni 1998 (BGBl.
    I S. 1506) geändert worden ist, erfüllen oder
    
2.  Leitungsaufgaben nach § 2 Absatz 2 Satz 1 Nummer 2 wahrnehmen und die Anforderungen des § 4 Absatz 2 der Heimpersonalverordnung erfüllen.
    

(2) Erfüllt eine Einrichtung, die bei Inkrafttreten dieser Verordnung im Betrieb, im Bau oder im baureifen Planungsstadium ist, die Mindestanforderungen des § 8 Absatz 2 Satz 1, 2 oder Satz 5, Absatz 3, 4 Satz 2 und 3, Absatz 5, 6 oder § 9 Absatz 2 Nummer 1 oder Nummer 2 nicht und sind Ausnahmen oder Abweichungen nicht statthaft, so hat die zuständige Behörde zur Angleichung an die einzelnen Anforderungen eine angemessene Frist einzuräumen.
Die Frist darf zehn Jahre nicht übersteigen.
Sie kann bei Vorliegen eines wichtigen Grundes verlängert werden.

#### § 15  
Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Juli 2010 in Kraft.

Potsdam, den 28.
Oktober 2010

Der Minister für Arbeit,  
Soziales, Frauen und Familie

Günter Baaske