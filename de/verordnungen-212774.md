## Kostenordnung zum Verwaltungsvollstreckungsgesetz für das Land Brandenburg (Brandenburgische Kostenordnung - BbgKostO)

Auf Grund des § 39 des Verwaltungsvollstreckungsgesetzes für das Land Brandenburg vom 16. Mai 2013 (GVBl. I Nr. 18) verordnet der Minister des Innern im Einvernehmen mit dem Minister der Finanzen:

§ 1  
**Gebührenarten**

Für die Maßnahmen nach dem Verwaltungsvollstreckungsgesetz für das Land Brandenburg werden folgende Gebühren erhoben:

1.  Mahngebühr,
2.  Grundgebühr,
3.  Pfändungsgebühr,
4.  Versteigerungs- oder Verwertungsgebühr,
5.  Gebühr für die Abnahme einer Vermögensauskunft,
6.  Androhungsgebühr,
7.  Festsetzungsgebühr,
8.  Wegnahmegebühr,
9.  Zwangsräumungsgebühr,
10.  Verwaltungsgebühr bei Ersatzvornahmen.

§ 2  
**Grundsätze der Gebührenberechnung**

Soweit nichts anderes bestimmt ist, werden die Gebühren für jede Vollstreckungsmaßnahme erhoben, auch wenn verschiedene oder gleichartige Vollstreckungsmaßnahmen wiederholt ergriffen werden.
Der Berechnung der Gebühren nach den §§ 4 bis 6 wird die Summe der beizutreibenden Geldforderungen zugrunde gelegt, deretwegen gleichzeitig gemahnt oder vollstreckt wird.

§ 3  
**Mehrheit von Schuldnerinnen und Schuldnern**

(1) Wird gegen mehrere Schuldnerinnen oder Schuldner wegen verschiedener Forderungen gleichzeitig vollstreckt, so werden die Vollstreckungsgebühren von jeder Schuldnerin und jedem Schuldner gesondert erhoben.

(2) Absatz 1 gilt auch, wenn gegen mehrere Schuldnerinnen oder Schuldner aus einer Forderung vollstreckt wird, für die sie als Gesamtschuldner haften.
Sind die Gesamtschuldner Eheleute oder durch eingetragene Lebenspartnerschaft verbundene Personen, werden die Gebühren nur einmal erhoben; für die Gebühren haften die Eheleute oder die durch eingetragene Lebenspartnerschaft verbundenen Personen als Gesamtschuldner.

(3) Die Gebühr wird nur einmal erhoben, wenn gegen mehrere Schuldnerinnen oder Schuldner, die miteinander in einem Gesamthandverhältnis stehen, in das Gesamthandvermögen vollstreckt wird.

§ 4  
**Mahngebühr**

(1) Für die Mahnung nach § 19 Absatz 2 Nummer 4 des Verwaltungsvollstreckungsgesetzes für das Land Brandenburg wird eine Mahngebühr erhoben, es sei denn, die Mahnung erfolgt durch öffentliche Bekanntmachung.
Die Mahngebühr wird auch bei wiederholter Mahnung nur einmal erhoben.

(2) Die Mahngebühr beträgt 1 Prozent des Mahnbetrages, mindestens jedoch 5 Euro und höchstens 100 Euro.
Zur Berechnung der Gebühr wird der Betrag, dessentwegen gemahnt wird, auf den nächsten Betrag, der ohne Rest durch zehn teilbar ist, abgerundet.
In den Fällen, in denen neben der Mahngebühr bei Eintritt der Voraussetzungen auch Säumniszuschläge erhoben werden, beträgt die Mahngebühr abweichend von Satz 1 5 Euro.

(3) Die Mahngebühr entsteht, sobald das Mahnschreiben zur Post gegeben ist oder eine Person mit seiner Überbringung beauftragt worden ist.
Im Fall der Mahnung durch Postnachnahmeauftrag wird die Mahngebühr nur fällig, wenn die Schuldnerin oder der Schuldner die Nachnahme nicht einlöst.

§ 5  
**Grundgebühr**

(1) Für die Maßnahmen der Vollstreckungsbehörde zur Beitreibung von Geldforderungen wird eine einmalige Grundgebühr erhoben, die mit der Beauftragung der Vollstreckungsbehörde entsteht.

(2) Die Grundgebühr richtet sich nach der Höhe der beizutreibenden Geldforderung.
Sie beträgt 31 Euro bei einer Geldforderung bis einschließlich 500 Euro und 42 Euro bei einer Geldforderung von mehr als 500 bis einschließlich 1 000 Euro.
Bei Geldforderungen über 1 000 Euro erhöht sich die Grundgebühr um 10 Euro je angefangene 1 000 Euro; sie beträgt jedoch höchstens 100 Euro.

(3) Nimmt in dem Fall des § 22 Absatz 2 des Verwaltungsvollstreckungsgesetzes für das Land Brandenburg die ersuchende Behörde vor Beginn der Vollstreckung das Ersuchen zurück, wird nur eine halbe Grundgebühr erhoben.

§ 6  
**Pfändungsgebühr**

(1) Die Pfändungsgebühr wird erhoben

1.  für die Pfändung von beweglichen Sachen, von Früchten, die vom Boden noch nicht getrennt sind, von Forderungen aus Wechseln oder anderen Papieren, die durch Indossament übertragen werden können, und
2.  für die Pfändung von Forderungen, die nicht unter Nummer 1 fallen, und von anderen Vermögensrechten.

(2) Die Pfändungsgebühr richtet sich nach der Höhe der beizutreibenden Geldforderungen.
Sie beträgt 10,50 Euro bei einer Geldforderung bis einschließlich 500 Euro und 21 Euro bei einer Geldforderung von mehr als 500 bis einschließlich 1 000 Euro.
Bei Forderungen über 1 000 Euro erhöht sich die Pfändungsgebühr um 10 Euro je angefangene 1 000 Euro.

(3) Die Pfändungsgebühr entsteht

1.  im Fall des Absatzes 1 Nummer 1, sobald die Vollstreckungsdienstkraft sich zur Vornahme der Pfändung an Ort und Stelle des Vollstreckungsschuldners begeben hat,
2.  im Fall des Absatzes 1 Nummer 2, sobald die Vollstreckungsbehörde die Pfändungsverfügung zum Zweck der Zustellung zur Post gegeben hat oder eine Person mit der Überbringung beauftragt worden ist.
    

(4) Bei der Pfändung von Sachen wird die Pfändungsgebühr auch für Anschlusspfändungen sowie für Pfändungsversuche erhoben, die deshalb erfolglos bleiben, weil die Vollstreckungsdienstkraft keine zur Pfändung geeigneten Sachen vorfindet oder weil von der Verwertung der zu pfändenden Gegenstände ein Überschuss über die Kosten der Zwangsvollstreckung nicht zu erwarten ist.
Bei der Pfändung von Forderungen wird die Pfändungsgebühr auch erhoben, wenn die Pfändung nur deshalb erfolglos bleibt, weil keine Forderung besteht, die die beizutreibenden Forderungen deckt.

§ 7  
**Versteigerungs- oder Verwertungsgebühr**

(1) Die Versteigerungs- oder Verwertungsgebühr wird für die Versteigerung und andere Verwertung von Gegenständen erhoben.

(2) Die Versteigerungs- oder Verwertungsgebühr richtet sich nach dem Erlös, höchstens nach der Summe der beizutreibenden Beträge.
Sie beträgt 21 Euro bei einem Erlös bis einschließlich 500 Euro und 32 Euro bei einem Erlös von mehr als 500 bis einschließlich 1 000 Euro.
Bei einem Erlös über 1 000 Euro erhöht sich die Gebühr um 10 Euro je angefangene 1 000 Euro.

(3) Die Gebühr entsteht, sobald die Vollstreckungsbehörde den Auftrag zur Versteigerung oder sonstigen Verwertung der Vollstreckungsdienstkraft oder der sonstigen beauftragten Person zuleitet.

(4) Die Versteigerungs- oder Verwertungsgebühr entfällt, wenn die Vollstreckungsbehörde den Auftrag zurücknimmt, bevor die Vollstreckungsdienstkraft oder die sonstige beauftragte Person Schritte zur Ausführung der Versteigerung oder Verwertung unternommen hat.

§ 8  
**Gebühr für die Abnahme einer Vermögensauskunft**

(1) Die Gebühr wird erhoben für die Abnahme einer Vermögensauskunft durch die Vollstreckungsbehörde nach § 22 Absatz 1 Nummer 2 des Verwaltungsvollstreckungsgesetzes für das Land Brandenburg in Verbindung mit § 284 der Abgabenordnung.
Die Gebühr beträgt 25 Euro.

(2) Die Gebühr entsteht mit dem Zugang der Ladung zur Abgabe der Vermögensauskunft.
Wird zu einem späteren Zeitpunkt von der Abnahme der eidesstattlichen Versicherung abgesehen, kann die Gebühr vermindert oder von ihrer Erhebung ganz abgesehen werden.

§ 9  
**Androhungsgebühr**

Für die schriftliche Androhung eines Zwangsmittels wird eine Gebühr erhoben, wenn die Androhung nicht mit dem Verwaltungsakt verbunden ist, durch den eine sonstige Handlung, Duldung oder Unterlassung aufgegeben wird.
Die Gebühr beträgt mindestens 10 Euro und höchstens 65 Euro.

§ 10  
**Festsetzungsgebühr**

Für die Festsetzung eines Zwangsgeldes wird eine Gebühr erhoben.
Sie beträgt mindestens 10 Euro und höchstens 65 Euro.

§ 11  
**Wegnahmegebühr**

(1) Für die Wegnahme einer herauszugebenden Sache wird eine Gebühr erhoben.
Dies gilt nicht, wenn die Sache durch Inbesitznahme gepfändet wird.
Die Gebühr beträgt 20 Euro.

(2) Die Gebührenschuld entsteht, sobald die Vollstreckungsdienstkraft oder die von der Vollstreckungsbehörde beauftragte Person Schritte zur Ausführung des Auftrages unternommen hat.

§ 12  
**Zwangsräumungsgebühr**

(1) Für die Zwangsräumung nach § 35 des Verwaltungsvollstreckungsgesetzes für das Land Brandenburg wird eine Gebühr erhoben.
Sie beträgt 75 Euro.

(2) Die Gebührenschuld entsteht, sobald die Vollstreckungsdienstkraft oder die von der Vollstreckungsbehörde beauftragte Person Schritte zur Ausführung des Auftrages unternommen hat.

§ 13  
**Verwaltungsgebühr bei Ersatzvornahmen**

(1) Für die Maßnahmen der Vollstreckungsbehörde bei Ersatzvornahmen wird eine Verwaltungsgebühr erhoben.
 Sie beträgt mindestens 10 Euro und höchstens 1 000 Euro.

(2) Die Verwaltungsgebühr entsteht, sobald die Vollstreckungsbehörde Schritte zur Durchführung der Ersatz-vornahme eingeleitet hat.

§ 14  
**Auslagen im Mahnverfahren**

Im Mahnverfahren werden Auslagen, insbesondere Postentgelte, nicht erhoben.
Das gilt nicht im Fall der Mahnung durch Postnachnahmeauftrag, wenn die Nachnahme eingelöst wird.

§ 15  
**Auslagen der Vollstreckungsbehörden**

(1) Nicht erstattet werden:

1.  Reisekosten der Vollstreckungsdienstkräfte sowie
2.  Postentgelte einschließlich Telegramm-, Fernsprech- und Fernschreibentgelte mit Ausnahme von Postzustellungsentgelten.

(2) Die übrigen Auslagen sind der Vollstreckungsbehörde vom Vollstreckungsschuldner zu erstatten.
Zu den Auslagen gehören insbesondere:

1.  Aufwendungen, die durch öffentliche Bekanntmachungen entstehen,
2.  Beträge, die an Treuhänderinnen, Treuhänder, Sachverständige und Hilfspersonen der Vollstreckungsdienstkraft insbesondere bei der Öffnung von Türen oder Behältnissen oder bei der Anwendung unmittelbaren Zwangs zu zahlen sind,
3.  Aufwendungen für Beförderung, Verwahrung und Beaufsichtigung gepfändeter Sachen, für die Aberntung gepfändeter Früchte und die Erhaltung gepfändeter Tiere,
4.  Steuern, die anlässlich der Pfandverwertung zu entrichten sind,
5.  Gerichtskosten, insbesondere soweit sie bei der Zwangsvollstreckung in das unbewegliche Vermögen entstehen.

(3) Werden bei mehreren Vollstreckungsschuldnerinnen oder Vollstreckungsschuldnern gepfändete Sachen gemeinsam versteigert oder aus freier Hand veräußert, sind die Auslagen der gemeinsamen Verwertung auf die Vollstreckungsschuldnerinnen oder Vollstreckungsschuldner angemessen zu verteilen.

§ 16  
**Übergangsregelung**

Für Vollstreckungsverfahren, die vor Inkrafttreten dieser Verordnung eingeleitet wurden, werden die Gebühren und Auslagen nach der Kostenordnung zum Verwaltungsvollstreckungsgesetz für das Land Brandenburg vom 16. Juni 1992 (GVBl. II S. 299) erhoben.

§ 17  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1. September 2013 in Kraft.
Gleichzeitig tritt die Kostenordnung zum Verwaltungsvollstreckungsgesetz für das Land Brandenburg vom 16. Juni 1992 (GVBl. II S. 299) außer Kraft.

Potsdam, den 2.
September 2013

Der Minister des Innern

Ralf Holzschuher