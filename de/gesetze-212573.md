## Gesetz zu dem Staatsvertrag vom 7. Februar 2011 zur Änderung des Staatsvertrages über die Errichtung gemeinsamer Fachobergerichte der Länder Berlin und Brandenburg

(1) Dem am 7.
Februar 2011 unterzeichneten Staatsvertrag zur Änderung des Staatsvertrages über die Errichtung gemeinsamer Fachobergerichte der Länder Berlin und Brandenburg wird zugestimmt.

(2) Der Staatsvertrag wird als Anlage zu diesem Gesetz veröffentlicht.

[zum Staatsvertrag](/de/vertraege-237647)