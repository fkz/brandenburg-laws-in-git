## Verordnung über das Naturschutzgebiet „Putlitzer Stadtheide“

Auf Grund der §§ 22, 23 und § 32 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

#### § 1  
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Prignitz wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Putlitzer Stadtheide“.

#### § 2  
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 45 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Gemeinde:**  

**Gemarkung:**  

**Flur:**

Putlitz  

Lockstädt  

1;

Putlitz  

Putlitz  

9.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 bei-gefügt.
Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 2 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 und 2 mit dem Titel „Topografische Karten zur Verordnung über das Naturschutzgebiet ‚Putlitzer Stadtheide‘“ und in der Liegenschaftskarte mit dem Titel „Liegenschaftskarte zur Verordnung über das Naturschutzgebiet ‚Putlitzer Stadtheide‘“ mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die topografischen Karten ermöglichen die Verortung im Gelände.
 Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Liegenschaftskarte im Maßstab 1 : 2 500.
Die Karten sind unterzeichnet von dem Siegelverwahrer, Siegelnummer 22, des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz am 11.
August 2010.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Prignitz, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3  
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung und Entwicklung der Lebensräume wild lebender Pflanzengesellschaften, insbesondere naturnaher Wälder mit an die potenziell natürliche Vegetation angepassten Beständen und hohem Alt- und Totholzanteil, wie Erlenbruchwälder, Eichen-Hainbuchenwälder, Eichen-Buchenmischwälder, Eichenmischwälder bodensaurer Standorte und reich strukturierter Waldränder sowie saurer Arm- und Zwischenmoore mit einem dystrophen Moorgewässer und kleinflächig feuchter Heiden mit Erica tetralix (Glockenheide);
    
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Torfmoose (Sphagnum spec.);
    
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Fledermäuse, Vögel, Amphibien und Libellen, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten;
    
4.  die Erhaltung und Entwicklung des Gebietes wegen der besonderen Eigenart der Putlitzer Stadtheide mit einem Mosaik aus reich strukturierten Waldflächen sowie wegen der Seltenheit eines naturnahen Waldmoores;
    
5.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Bestandteil des regionalen Biotopverbundes zwischen der Niederung der Sagast, des Freudenbaches und dem Gewässersystem der Stepenitz.
    

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teiles des Europäischen Vogelschutzgebietes „Agrarlandschaft Prignitz-Stepenitz“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) in seiner Funktion als Lebensraum von Arten nach Anhang I der Richtlinie 2009/147/EG, insbesondere Kranich (Grus grus), Mittelspecht (Dendrocopus medius) und Schwarzstorch (Ciconia nigra) einschließlich ihrer Brut- und Nahrungsbiotope.

#### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
    
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
    
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
    
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
    
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
    
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
    
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
    
8.  die Ruhe der Natur durch Lärm zu stören;
    
9.  das Gebiet außerhalb der dem öffentlichen Verkehr gewidmeten Wege zu betreten, ausgenommen ist das Betreten von Wegen zum Zwecke der Erholung in der Zeit jeweils vom 1.
    August eines jeden Jahres bis zum 28.
    Februar des Folgejahres;
    
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 51 des Brandenburgischen Naturschutzgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
    
11.  mit Fahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen; ausgenommen ist das Befahren von Wegen mit nicht motorisierten Fahrzeugen in der Zeit vom 1.
    August eines jeden Jahres bis zum 28.
    Februar des Folgejahres.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzgesetzes und des Waldgesetzes des Landes Brandenburg;
    
12.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
    
13.  Hunde frei laufen zu lassen;
    
14.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
    
15.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (zum Beispiel Abwasser, Klärschlamm und Bioabfälle) zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
    
16.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
    
17.  Tiere zu füttern oder Futter bereitzustellen;
    
18.  Tiere auszusetzen oder Pflanzen anzusiedeln;
    
19.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
    
20.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
    
21.  Pflanzenschutzmittel jeder Art anzuwenden.
    

#### § 5  
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  forstwirtschaftliche Maßnahmen in der Zeit vom 1.
        März bis zum 31.
        August eines jeden Jahres unzulässig sind,
        
    2.  nur Arten der potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Baum-arten unter Ausschluss eingebürgerter Arten zu verwenden sind.
         Nebenbaumarten dürfen dabei nicht als Hauptbaumart eingesetzt werden,
        
    3.  mindestens fünf Stämme Altholz je Hektar mit einem Mindestdurchmesser von 40 Zentimetern in  
        1,30 Meter Höhe über dem Stammfuß aus der Nutzung zu nehmen und dauerhaft zu markieren sind.
        In Jungbeständen ist ein solcher Altholzanteil zu entwickeln,
        
    4.  eine naturnahe Waldentwicklung mit einem Totholzanteil von mindestens zehn Prozent des aktuellen Bestandesvorrates zu gewährleisten ist, wobei mindestens fünf Stück stehendes Totholz je Hektar mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß und einer Mindesthöhe von  
        fünf Metern, sofern vorhanden, zu erhalten sind,
        
    5.  eine Nutzung der in § 3 Absatz 1 Nummer 1 genannten Waldgesellschaften nur einzelstamm- bis truppweise erfolgt; auf den übrigen Waldflächen sind **Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von 0,5 Hektar zulässig,**
        
    6.  Horst- und Höhlenbäume nicht entfernt werden,
        
    7.  § 4 Absatz 2 Nummer 21 gilt;
        
2.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass diese in der Zeit vom 1.
        März bis 31.
         August eines jeden Jahres unzulässig ist,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Errichtung anzuzeigen.
        Die Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        
    
    Im Übrigen bleiben Wildfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern im gesamten Gebiet und die Anlage von Kirrungen in den geschützten Biotopen unzulässig;
    
3.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
    
4.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, die den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
    
5.  der Betrieb von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
     Die ordnungsgemäße Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
    
6.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
    
7.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
    
8.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der unteren Naturschutzbehörde gebilligt oder angeordnet worden sind;
    
9.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen. Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl.
      
    S.
    1734) an Straßen und Wegen freigestellt;
    
10.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
 Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

#### § 6  
Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  Kiefernforste und andere nicht mit einheimischen beziehungsweise mit standortfremden Gehölzen bestockte Flächen sollen in naturnahe Laubwälder umgewandelt werden;
    
2.  durch eine am Schutzzweck orientierte Wasserrückhaltung soll die Wasserversorgung des Moores gesichert werden.
    

#### § 7  
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8   
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu 50 000 (in Worten: fünfzigtausend) Euro geahndet werden.

#### § 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere §§ 31 bis 33 und § 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

#### § 10  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11   
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 25.
Oktober 2010

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

**Anlage 1  
**(zu § 2 Absatz 1)

![Das rund 45 Hektar große Naturschutzgebiet "Putlitzer Stadtheide" liegt in der Gemeinde Putlitz im Landkreis Prignitz. Es umfasst Teile der Gemarkungen Lockstädt und Putlitz und liegt südlich der Ortslage Karlshof.](/br2/sixcms/media.php/69/Putlitzer.jpg "Das rund 45 Hektar große Naturschutzgebiet "Putlitzer Stadtheide" liegt in der Gemeinde Putlitz im Landkreis Prignitz. Es umfasst Teile der Gemarkungen Lockstädt und Putlitz und liegt südlich der Ortslage Karlshof.")

**Anlage 2  
**(zu § 2 Absatz 1)

Flurstücksliste zur Verordnung über das Naturschutzgebiet „Putlitzer Stadtheide“

Landkreis: Prignitz

Gemeinde

Gemarkung

Flur

Flurstück

Geteilt\*

Putlitz

Lockstädt

1

1

Ja

Putlitz

Lockstädt

1

2

Ja

Putlitz

Lockstädt

1

37

Ja

Putlitz

Lockstädt

1

279

Ja

Putlitz

Lockstädt

1

280

Ja

Putlitz

Lockstädt

1

281

Ja

Putlitz

Lockstädt

1

282

Ja

Putlitz

Putlitz

9

13

Ja

Putlitz

Putlitz

9

14

Ja

Putlitz

Putlitz

9

15

Ja

Putlitz

Putlitz

9

16

Ja

Putlitz

Putlitz

9

17

Ja

Putlitz

Putlitz

9

19

Ja

Putlitz

Putlitz

9

20

Ja

Putlitz

Putlitz

9

21

Ja

Putlitz

Putlitz

9

22

Ja

Putlitz

Putlitz

9

23

Ja

Putlitz

Putlitz

9

24

Ja

Putlitz

Putlitz

9

25

Ja

Putlitz

Putlitz

9

26

Ja

Putlitz

Putlitz

9

27

Ja

Putlitz

Putlitz

9

28

Ja

Putlitz

Putlitz

9

29

Nein

Putlitz

Putlitz

9

30

Ja

Putlitz

Putlitz

9

31

Ja

Putlitz

Putlitz

9

32

Ja

Putlitz

Putlitz

9

33

Ja

Putlitz

Putlitz

9

34

Ja

Putlitz

Putlitz

9

35

Ja

Putlitz

Putlitz

9

36

Ja

Putlitz

Putlitz

9

37

Nein

Putlitz

Putlitz

9

38

Nein

Putlitz

Putlitz

9

39

Nein

Putlitz

Putlitz

9

40

Nein

Putlitz

Putlitz

9

41

Nein

Putlitz

Putlitz

9

42

Nein

Putlitz

Putlitz

9

43

Nein

Putlitz

Putlitz

9

44

Nein

Putlitz

Putlitz

9

45

Nein

Putlitz

Putlitz

9

46

Nein

Putlitz

Putlitz

9

47

Nein

Putlitz

Putlitz

9

48

Nein

Putlitz

Putlitz

9

49

Nein

Putlitz

Putlitz

9

50

Nein

Putlitz

Putlitz

9

51

Nein

Putlitz

Putlitz

9

52

Nein

Putlitz

Putlitz

9

53

Nein

Putlitz

Putlitz

9

54

Nein

Putlitz

Putlitz

9

55

Nein

\* Flurstück liegt vollständig im NSG = Nein / Flurstück liegt teilweise im NSG = Ja