## Verordnung zur Übertragung von Zuständigkeiten für die Berechnung und Zahlung von Reisekosten sowie die Bewilligung, Berechnung und Zahlung von Trennungsgeld für den Geschäftsbereich des Ministerpräsidenten auf die Zentrale Bezügestelle des Landes Brandenburg  (Reisekosten-Trennungsgeldzuständigkeitsübertragungsverordnung MP - RkTrZÜVMP)

Auf Grund des § 63 Absatz 3 Satz 2 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S.
26) verordnet der Ministerpräsident im Einvernehmen mit dem Minister der Finanzen:

### § 1  
Übertragung von Aufgaben

(1) Die Zuständigkeit für die Berechnung und Zahlung von Reisekosten im Sinne des § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes wird für den Geschäftsbereich des Ministerpräsidenten auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

(2) Ebenso wird die Zuständigkeit für die Bewilligung, Berechnung und Zahlung von Trennungsgeld nach § 1 Satz 1 der Brandenburgischen Trennungsgeldverordnung in Verbindung mit § 9 Absatz 3 der Trennungsgeldverordnung in der Fassung der Bekanntmachung vom 29.
Juni 1999 (BGBl.
I S.
1533), die zuletzt durch Artikel 3 Absatz 38 der Verordnung vom 12.
Februar 2009 (BGBl.
I S.
320) geändert worden ist, auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

### § 2  
Vertretung bei Klagen

Im Rahmen der Übertragung der Zuständigkeit nach § 1 wird die Zentrale Bezügestelle des Landes Brandenburg ermächtigt, den Geschäftsbereich des Ministerpräsidenten in verwaltungs- und arbeitsgerichtlichen Streitigkeiten zu vertreten.
Dies gilt auch für Anträge in einstweiligen Rechtsschutzverfahren.

### § 3  
Übergangsvorschrift

Für Anträge auf Berechnung und Zahlung von Reisekosten sowie auf Bewilligung, Berechnung und Zahlung von Trennungsgeld, die vor dem Inkrafttreten dieser Verordnung eingegangen sind und über die noch nicht abschließend entschieden worden ist, verbleibt es bei der Zuständigkeit der bisher zuständigen Stellen.
Dies gilt auch für die Vertretung in zu diesem Zeitpunkt bereits anhängigen Rechtsstreitigkeiten.

### § 4  
Inkrafttreten

Diese Verordnung tritt am 01.
Januar 2013 in Kraft.

Potsdam, den 04.
Dezember 2012

Der Ministerpräsident  
Matthias Platzeck