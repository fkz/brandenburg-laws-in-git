## Verordnung über den Inhalt und die Form des elektronischen Wasserbuches für das Land Brandenburg  (Brandenburgische Wasserbuchverordnung - BbgWaBuV)

### § 1  
Eintragungspflichtige wasserrechtliche Entscheidungen und Rechtsverhältnisse,Grundlage der Eintragung

(1) In das Wasserbuch sind die in § 87 Absatz 2 Satz 1 des Wasserhaushaltsgesetzes benannten Rechtsverhältnisse des Wasserrechts einzutragen.

(2) Grundlage für die Eintragung ist der Wortlaut der Entscheidungen, der Inhalt der Verordnungen oder Karten, die Erklärung oder der Nachweis alter Rechte oder Befugnisse.
Soweit die wasserrechtliche Entscheidung konzentriert wird, ist der das Wasserrecht betreffende Teil der Entscheidung Grundlage der Eintragung.

(3) Die Eintragung erfolgt im Regelfall erst, wenn die wasserrechtliche Entscheidung bestandskräftig geworden ist.
Wird die Eintragung auf Grund der erheblichen Auswirkungen und der Vollziehbarkeit vor diesem Zeitpunkt vorgenommen, ist die Eintragung als vorläufig zu kennzeichnen.

(4) Rechtsverhältnisse von untergeordneter wasserwirtschaftlicher Bedeutung werden nicht eingetragen.
 Rechtsverhältnisse sind insbesondere von untergeordneter Bedeutung, wenn

1.  die Zulassung befristet für unter einem Jahr erteilt wird,
2.  das Entnehmen, Zutageleiten, Zutagefördern oder Ableiten von nicht mehr als 8 Kubikmeter Wasser täglich zugelassen wird,
3.  das Einleiten von gereinigtem Abwasser aus Kleinkläranlagen bis zu 8 Kubikmeter täglich zugelassen wird,
4.  das ortsnahe Versickern von Niederschlagswasser zugelassen wird oder
5.  die grundstücksbezogene Nutzung von Erdwärme erfolgt,

und keine erheblichen Auswirkungen auf den Wasserhaushalt zu erwarten sind.

### § 2  
Aufbau und Führung des Wasserbuches, Eintragungen

(1) Das Wasserbuch besteht aus Wasserbuchblättern.

(2) Das Wasserbuch wird vom Wasserwirtschaftsamt (Wasserbuchbehörde) geführt.
Für jedes einzutragende Rechtsverhältnis ist ein Wasserbuchblatt anzulegen.
Jedes Wasserbuchblatt erhält eine eigenständige Wasserbuchblattnummer.
Die Wasserbuchblattnummern werden durch das Wasserwirtschaftsamt vergeben.

(3) Die Eintragungen, Änderungen und Löschungen wasserrechtlicher Entscheidungen sollen durch die für die Entscheidung zuständige Wasserbehörde und bei Betrieben, die der Bergaufsicht unterliegen, durch das Landesamt für Bergbau, Geologie und Rohstoffe erfolgen.
Soweit die zuständige Wasserbehörde oder das Landesamt für Bergbau, Geologie und Rohstoffe die Eintragungen, Änderungen und Löschungen nicht selbstständig vornehmen kann, übermittelt sie die Entscheidung sowie das durch die oberste Wasserbehörde vorgegebene Formular zur Eintragung unverzüglich nach Bestandskraft oder mit deren Vollziehbarkeit dem Wasserwirtschaftsamt zur Eintragung.
Eine Bescheinigung der Bestandskraft ist anzufügen beziehungsweise nachzureichen.

(4) Wird die wasserrechtliche Zulassung in einer nach anderen Rechtsvorschriften ergangenen Zulassung konzentriert und ist die Zulassungsbehörde nicht zugleich eine Wasserbehörde oder das Landesamt für Bergbau, Geologie und Rohstoffe, veranlasst die Zulassungsbehörde die Eintragung und Übermittlung der in Absatz 3 benannten Unterlagen über die für die konzentrierte wasserrechtliche Zulassung ansonsten zuständige Wasserbehörde.

(5) Änderungen oder Löschungen der Eintragungen sind nur durch die zuständigen Wasserbehörden, das Landesamt für Bergbau, Geologie und Rohstoffe oder das Wasserwirtschaftsamt zulässig.

(6) Für jede Änderung eines Wasserrechts wird eine neue Version des Datensatzes unter der nach Absatz 2 vergebenen Wasserbuchnummer angelegt.

(7) Alte Rechte und alte Befugnisse, deren Rechtsbestand noch nicht nachgewiesen ist, sind bei der Eintragung als „behauptete Rechte und Befugnisse“ zu kennzeichnen.

### § 3  
Umfang der Eintragung

Die Datensätze für die Eintragungen in das jeweilige Wasserbuchblatt enthalten mindestens die folgenden Angaben:

1.  Wasserbuchblattnummer,
2.  Datum, Aktenzeichen, Zulassungsbehörde und gegebenenfalls beteiligte Wasserbehörde, wesentlicher Inhalt unter Bezugnahme auf den Wortlaut der Entscheidung und der wesentlichen Nebenbestimmungen der behördlichen Entscheidung oder Vereinbarung; bei alten Rechten oder Befugnissen der wesentliche Inhalt des Rechts oder der Befugnis, bei Schutzgebieten der Schutzgegenstand und Schutzzweck sowie Datum und Fundstelle der zugrunde liegenden Festsetzung oder Bestimmung,
    
3.  Inhaber des Rechts oder der Befugnis, insbesondere Name und Anschrift; bei einer Mehrheit von Inhabern ist jeder Inhaber einzutragen,
4.  Raumbezug der behördlichen Entscheidung oder Vereinbarung, insbesondere Angabe der Koordinaten im einheitlichen Bezugssystem für das Land Brandenburg zu Gemeinde, Flussgebiet sowie zusätzlich – sofern bekannt – zu Flusskilometer, Gemarkung, Flurstück, bei Grenzgewässern auch Grenzabschnitt und Grenzzeichen,
5.  Datum der Eintragungen, Änderungen und Löschungen.

### § 4  
Zugang zur Datenbank

(1) Die Eintragungen sind den Wasserbehörden und bei Betrieben, die der Bergaufsicht unterliegen, dem Landesamt für Bergbau, Geologie und Rohstoffe zur Erfüllung der ihnen obliegenden Aufgaben zugänglich zu machen.

(2) Zur Gewährleistung des Informationsrechts der Öffentlichkeit nach § 142 Absatz 2 des Brandenburgischen Wassergesetzes wird ein dienstebasierter Zugriff auf das Wasserbuch über das Internet eröffnet.
Personenbezogene Daten der Wasserrechtsinhaber sowie etwaige Betriebs- und Geschäftsgeheimnisse, mit Ausnahme der Emissions-daten, sind dabei für eine Ansicht und gegebenenfalls ein Herunterladen zu sperren.