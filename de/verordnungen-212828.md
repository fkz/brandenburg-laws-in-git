## Verordnung über die Zuständigkeit zur Erteilung einer Zulassung nach § 3 der Präimplantationsdiagnostikverordnung

Auf Grund des § 113 Absatz 1 des Heilberufsgesetzes vom 28.
April 2003 (GVBl.
l S. 126) in Verbindung mit § 18 Absatz 3 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186) verordnet die Landesregierung im Einvernehmen mit der Landesärztekammer Brandenburg:

§ 1 
----

(1) Zuständige Behörde nach § 3 der Präimplantationsdiagnostikverordnung vom 21.
Februar 2013 (BGBl.
I S. 323) ist die Landesärztekammer Brandenburg.

(2) Zur Kostendeckung erhebt die Landesärztekammer Brandenburg Gebühren nach Maßgabe ihrer Gebührenordnung.

§ 2 
----

Diese Verordnung tritt mit Wirkung vom 1.
Februar 2014 in Kraft.

Potsdam, den 11.
Februar 2014

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack