## Verordnung über den Erwerb des Latinums/Graecums durch eine Latinum-/Graecumprüfung (Latinum-/Graecumprüfungsverordnung - LaGrPV)

Auf Grund des § 60 Absatz 4 Satz 1 und des § 61 Absatz 3 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
August 2002 (GVBl.
I S. 78) verordnet die Ministerin für Bildung, Jugend und Sport:

#### § 1  
Voraussetzungen für eine Latinum-/Graecumprüfung

(1) Schülerinnen und Schüler können das Latinum oder Graecum erwerben, wenn sie in einer Prüfung nachweisen, dass sie über die Kenntnisse in Latein oder Griechisch gemäß § 2 verfügen (Latinum-/Graecumprüfung).
Die Latinum-/Graecumprüfung besteht aus einer schriftlichen und einer mündlichen Prüfung.

(2) Voraussetzung für eine Latinum-/Graecumprüfung ist ein aufsteigender und ununterbrochener Pflicht-, Wahlpflicht- oder Wahlunterricht in Latein oder Griechisch von mindestens drei Schuljahren in der Sekundarstufe I oder in der gymnasialen Oberstufe oder in den Bildungsgängen des Zweiten Bildungsweges.

(3) Eine Latinum-/Graecumprüfung ist ausgeschlossen, wenn Latein oder Griechisch Abiturprüfungsfach ist.

#### § 2  
Umfang der nachzuweisenden Kenntnisse

(1) In der Latinumprüfung wird festgestellt, ob der Prüfling über die Fähigkeit verfügt, lateinische Originaltexte gegebenenfalls mit Hilfe eines zweisprachigen Wörterbuchs im sprachlichen Schwierigkeitsgrad inhaltlich anspruchsvollerer Stellen bezogen auf Bereiche der politischen Rede, der Philosophie und der Historiographie in Inhalt, Aufbau und Aussage zu erfassen.
Diese Fähigkeit ist durch eine sachlich richtige Übersetzung in angemessenem Deutsch nachzuweisen.
 Die Übersetzung kann durch eine vertiefende Interpretation ergänzt werden.
 Hierzu werden Sicherheit in der für die Texterschließung notwendigen Formenlehre und Syntax, ein ausreichender Wortschatz und die erforderlichen Kenntnisse aus den Bereichen römische Politik, Geschichte, Philosophie und Literatur vorausgesetzt.

(2) In der Graecumprüfung wird festgestellt, ob der Prüfling über die Fähigkeit verfügt, griechische Originaltexte gegebenenfalls mit Hilfe eines zweisprachigen Wörterbuchs im sprachlichen Schwierigkeitsgrad inhaltlich anspruchsvollerer Platon-Stellen in Inhalt, Aufbau und Aussage zu erfassen.
Diese Fähigkeit ist durch eine sachlich richtige Übersetzung in angemessenem Deutsch nachzuweisen.
 Die Übersetzung kann durch eine vertiefende Interpretation ergänzt werden.
 Hierzu werden Sicherheit in der für die Texterschließung notwendigen Formenlehre und Syntax, ein ausreichender Wortschatz und die erforderlichen Kenntnisse aus den Bereichen griechische Politik, Geschichte, Philosophie und Literatur vorausgesetzt.

(3) In der schriftlichen Prüfung ist ein unbekannter lateinischer Text im Umfang von etwa 180 Wörtern oder ein unbekannter griechischer Text im Umfang von etwa 195 Wörtern zu übersetzen.

(4) In der mündlichen Prüfung ist ein unbekannter lateinischer Text im Umfang von etwa 50 Wörtern oder ein unbekannter griechischer Text im Umfang von etwa 60 Wörtern zu übersetzen.

#### § 3  
Anmeldung und Zulassung zur Latinum-/Graecumprüfung

(1) Die Meldung zur Latinum-/Graecumprüfung erfolgt schriftlich an die Schulleiterin oder den Schulleiter zu Beginn des Schulhalbjahres, an dessen Ende die Prüfung abgelegt werden soll.
Prüflinge des E-learning-Angebotes Latein melden sich bei der Schulleiterin oder dem Schulleiter ihrer Stammschule an.
Die Meldung ist an die Schulrätin oder den Schulrat mit der Generalie für das Fach Latein weiterzuleiten.
Der Meldung ist eine Darlegung über die Art der Vorbereitung auf die Latinum-/Graecumprüfung beizufügen, aus der auch hervorgeht, mit welchen Autorinnen oder welchen Autoren sich der Prüfling besonders beschäftigt hat.

(2) Die Zulassung zur Latinum-/Graecumprüfung erfolgt schriftlich durch die Schulleiterin oder den Schulleiter, sofern die Voraussetzungen gemäß § 1 gegeben sind.

(3) Meldet sich an einer Schule ein Prüfling zu einer Latinum-/Graecumprüfung und gibt es an der Schule keine befähigten Lehrkräfte, um einen Prüfungsausschuss bilden zu können, oder kann ein Prüfungsausschuss nicht gebildet werden, weil der Prüfling ein Angehöriger gemäß § 20 Absatz 1 des Verwaltungsverfahrensgesetzes (Bund) in Verbindung mit den §§ 1 und 3 des Verwaltungsverfahrensgesetzes für das Land Brandenburg ist, so entscheidet das staatliche Schulamt, an welcher Schule die Latinum-/Graecumprüfung stattfindet.

#### § 4  
Prüfungsausschuss

Zur Durchführung der Latinum-/Graecumprüfung bildet die Schulleiterin oder der Schulleiter für Latein oder Griechisch einen Prüfungsausschuss, dem als vorsitzendes Mitglied (Prüfungsausschussvorsitz) sie oder er, als prüfendes Mitglied eine Lehrkraft, die Latein oder Griechisch unterrichtet, sowie als protokollführendes Mitglied eine weitere Lehrkraft, die über hierfür ausreichende Latein- oder Griechischkenntnisse verfügen muss, angehören.
Das protokollführende Mitglied des Prüfungsausschusses kann einer anderen Schule angehören, wenn es an der eigenen Schule keine hierfür befähigte Person gibt und das staatliche Schulamt dem zugestimmt hat.
Der Prüfungsausschuss trifft alle Entscheidungen im Zusammenhang mit der Latinum-/Graecumprüfung, sofern nicht eine andere Zuständigkeit festgelegt ist.
Für Prüflinge des E-learning-Angebotes Latein obliegt die Organisation der Prüfung und die Zusammenstellung des Prüfungsausschusses der Schulrätin oder dem Schulrat mit der Generalie für das Fach Latein.

#### § 5  
Durchführung der Latinum-/Greacumprüfung

(1) Die schriftliche Prüfung dauert drei Zeitstunden.
Der Prüfling erhält eine Aufgabenstellung, die vollständig zu bearbeiten ist.
Die Aufgabenstellung wird von dem prüfenden Mitglied des Prüfungsausschusses erarbeitet und von dem vorsitzenden Mitglied des Prüfungsausschusses genehmigt, das sich dabei fachlich durch eine andere Lehrkraft beraten lassen kann.

(2) Die Prüfungsarbeiten werden von dem prüfenden Mitglied des Prüfungsausschusses korrigiert und mit einer Note bewertet.
Ist auch das protokollführende Mitglied fachlich befähigt, kann es von dem vorsitzenden Mitglied des Prüfungsausschusses ebenfalls mit Korrektur und Bewertung beauftragt werden.
Korrekturen und Bewertungen bedürfen der Bestätigung durch das vorsitzende Mitglied des Prüfungsausschusses, das sich dabei fachlich durch eine andere Lehrkraft beraten lassen kann.
Falls Aufgaben zur Interpretation einbezogen werden, ist eine Übersetzungsleistung gegenüber der Interpretationsleistung doppelt zu gewichten.
 Das Ergebnis der schriftlichen Prüfung und die Zulassung zur mündlichen Prüfung wird den Prüflingen schriftlich mitgeteilt.

(3) Die mündliche Prüfung dauert 20 Minuten.
Jeder Prüfling erhält unmittelbar vor der mündlichen Prüfung eine Vorbereitungszeit von 30 Minuten.
Die Aufgabenstellung wird von dem prüfenden Mitglied des Prüfungsausschusses erarbeitet.
Sie besteht aus einer Übersetzungsaufgabe und wird durch ein Prüfungsgespräch ergänzt, das dem Nachweis eines vertieften Textverständnisses und erforderlichenfalls dem Nachweis hinreichender Kenntnisse der Elementargrammatik dient.

(4) Der mündlichen Prüfung folgt die Beratung über die Prüfungsleistung und auf der Grundlage eines Vorschlages des prüfenden Mitgliedes des Prüfungsausschusses die Festlegung der Bewertung mit einer Note.
Die Entscheidung trifft der Prüfungsausschuss mit Mehrheit.
Stimmenthaltung ist nicht zulässig.

(5) Die Latinum-/Graecumprüfung ist bestanden, wenn die Gesamtnote aus schriftlicher und mündlicher Prüfung mindestens „ausreichende“ Leistungen (Note 4, in der gymnasialen Oberstufe 5 Punkte) ergibt.
Dabei darf kein Prüfungsteil mit „ungenügend“ abgeschlossen werden.
Wird der erste Teil der Prüfung mit „ungenügend“ abgeschlossen, wird der zweite Teil der Prüfung nicht mehr durchgeführt und die Prüfung als „nicht bestanden“ beendet.
Das Ergebnis der mündlichen Prüfung und das Gesamtergebnis werden den Prüflingen am Ende des Prüfungstages schriftlich mitgeteilt.
Die Bescheinigung über den Erwerb des Latinums/Graecums wird zusammen mit dem Zeugnis am Ende der Jahrgangsstufe ausgegeben, in der die Prüfung bestanden wurde.
Wird die Latinum-/Graecumprüfung nicht bestanden, ist dies ebenfalls zu bescheinigen.

#### § 6  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Ergänzungsprüfungsverordnung Latinum/Graecum vom 9.
September 1997 (GVBl.
II S. 781) außer Kraft.

Potsdam, den 26.
Mai 2011

Die Ministerin für Bildung, Jugend und Sport

Dr.
Martina Münch