## Vierte Verordnung über die Aufhebung von Wasserschutzgebieten

Auf Grund des § 15 Absatz 3 Satz 4 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1  
**Aufhebung von Wasserschutzgebieten**

(1) Folgende, auf der Grundlage des § 28 des Landeskulturgesetzes vom 14.
Mai 1970 (GBl.
I Nr. 12 S. 67), des § 28 des Wassergesetzes vom 17.
April 1963 (GBl.
I Nr. 5 S. 77) und der Verordnung über die Festlegung von Schutzgebieten für die Wasserentnahme aus dem Grund- und Oberflächenwasser zur Trinkwassergewinnung vom 11.
Juli 1974 (GBl.
I Nr. 37 S. 349) festgesetzte und nach § 46 des Wassergesetzes vom 2.
Juli 1982 (GBl.
I Nr. 26 S. 467) aufrechterhaltene Wasserschutzgebiete werden aufgehoben:

1.  das mit Beschluss Nummer 51-15/72 vom 6.
     Juli 1972 des Kreistages Strasburg für die Schutzzonen I und II und mit Beschluss Nummer 0004 vom 22.
    Januar 1981 des Kreistages Strasburg für die Schutzzone III des Brunnenstandortes Güterberg festgesetzte Trinkwasserschutzgebiet,
2.  die mit Beschluss Nummer 77-21/73 vom 9.
     November 1973 des Kreistages Angermünde für die Wasserfassungen Lindenhof, Schule und Wilhelmsfelde Flur 2-196 in Neukünkendorf festgesetzten Trinkwasserschutzgebiete,
3.  das mit Beschluss Nummer 20/11/74 vom 21.
     November 1974 des Kreistages Gransee für das Wasserfassungsgebiet Zehdenick Werk II festgesetzte Trinkwasserschutzgebiet,
4.  das mit Beschluss Nummer 0027 vom 24.
     September 1975 des Kreistages Luckenwalde für das Wasserwerk Liepe festgesetzte Trinkwasserschutzgebiet,
5.  das mit Beschluss Nummer 55/75 vom 18.
     Dezember 1975 des Kreistages Finsterwalde für das Wasserwerk Gahro festgesetzte Trinkwasserschutzgebiet,
6.  das mit Beschluss über die Festsetzung des Trinkwasserschutzgebietes für das Wasserwerk Döllingen (Schutzzone I, II, III) vom 8.
    Januar 1976 des Kreistages Bad Liebenwerda festgesetzte Trinkwasserschutzgebiet,
7.  das mit Beschluss über die Festlegung des Trinkwasserschutzgebietes für die Wasserwerke Elsterwerda I, II und III (Schutzzone I, II, III) vom 8.
    Januar 1976 des Kreistages Bad Liebenwerda festgesetzte Trinkwasserschutzgebiet,
8.  das mit Beschluss über die Festlegung des Trinkwasserschutzgebietes für das Wasserwerk Gröden (Schutzzonen I, II, III) vom 8.
    Januar 1976 des Kreistages Bad Liebenwerda festgesetzte Trinkwasserschutzgebiet,
9.  das mit Beschluss über die Festlegung des Trinkwasserschutzgebietes für das Wasserwerk Hirschfeld (Schutzzonen I, II, III) vom 8.
    Januar 1976 des Kreistages Bad Liebenwerda festgesetzte Trinkwasserschutzgebiet,
10.  die mit Beschluss Nummer 0057 vom 22.
    Juli 1976 des Kreistages Nauen für das Spitzenwasserwerk Nauen und das Wasserwerk Brädikow festgesetzten Trinkwasserschutzgebiete,
11.  das mit Beschluss Nummer 85/7/76 vom 22.
     Juli 1976 des Kreistages Gransee für das Wasserfassungsgebiet des Wasserwerkes Meseberg festgesetzte Trinkwasserschutzgebiet,
12.  das mit Beschluss Nummer 177/3/77 vom 24.
     März 1977 des Kreistages Gransee für das Wasserfassungsgebiet des Wasserwerkes Falkenthal festgesetzte Trinkwasserschutzgebiet,
13.  das mit Beschluss Nummer 63-20/77 vom 8.
     September 1977 des Kreistages Strasburg für die zwei Brunnenstandorte in Hetzdorf festgesetzte Trinkwasserschutzgebiet,
14.  die mit Beschluss Nummer 135/9/77 vom 15.
     September 1977 des Kreistages Gransee für die Wasserfassungsgebiete der Gemeinde Mildenberg „Wasserversorgungsanlagen der LPG und für die Bevölkerung des Mühlenweges“ und „Menz – DLA für ausländische Vertretungen – Feriendorf Menz“ festgesetzten Trinkwasserschutzgebiete,
15.  das mit Beschluss Nummer 2-12-1/79 vom 11.
     Januar 1979 über die Festlegung des Trinkwasserschutzgebietes für das Wasserwerk Schöna-Kolpien des Kreistages Herzberg festgesetzte Trinkwasserschutzgebiet,
16.  das mit Beschluss Nummer K 160/79 vom 15.
    März 1979 des Kreistages Finsterwalde für das Wasserwerk Göllnitz festgesetzte Trinkwasserschutzgebiet,
17.  die mit Beschluss Nummer 0019 vom 5.
    Dezember 1979 des Kreistages Jüterbog für die Trinkwassergewinnungsanlagen in den Gemeinden Danna, Mellnsdorf und Werkzahna festgesetzten Trinkwasserschutzgebiete,
18.  das mit Beschluss Nummer 29/1980 vom 17.
    Januar 1980 des Kreistages Spremberg festgesetzte Trinkwasserschutzgebiet Klein Loitz,
19.  das mit Beschluss Nummer 22-05/80 vom 17.
    März 1980 des Kreistages Angermünde festgesetzte Trinkwasserschutzgebiet Rosow – OT Neurosow,
20.  das mit Beschluss Nummer 0021/80 vom 8.
     September 1980 des Kreistages Königs Wusterhausen für das Wasserwerk im NVA-Objekt Motzen festgesetzte Trinkwasserschutzgebiet,
21.  die mit Beschluss Nummer 0004 vom 22.
    Januar 1981 des Kreistages Strasburg festgesetzte Schutzzone III des Trinkwasserschutzgebietes Trebenow,
22.  die mit Beschluss Nummer 84-13/81 vom 6.
    Mai 1981 des Kreistages Potsdam für die Versorgungsanlagen „NB Caputh“, „WW Groß Glienicke“, „WW Funkamt Deutsche Post“, „WW Stammbetrieb VEB GRW Teltow“ und „VEB IFA Automobilwerk Ludwigsfelde“ festgesetzten Trinkwasserschutzgebiete,
23.  die mit Beschluss Nummer 005 vom 7.
    Mai 1981 des Kreistages Nauen für die Brunnen in Bredow (2 Brunnen), für die Kleinstwasserwerke Ribbeck und Selbelang und für das Wasserwerk Wachow festgesetzten Trinkwasserschutzgebiete,
24.  die mit Beschluss Nummer 53/81 vom 13.
    Mai 1981 des Kreistages Bad Freienwalde für die Wasserwerke Dannenberg, Dannenberg OT Platzfelde, Neuenhagen, Schiffmühle OT Gabow, Wriezen und Zäckeriner Loose festgesetzten Trinkwasserschutzgebiete,
25.  die mit Beschluss Nummer 13/56/81 vom 21.
     Mai 1981 des Kreistages Pasewalk für die Wasserfassungen Bagemühl II, Bagemühl III und Woddow festgesetzten Trinkwasserschutzgebiete,
26.  die mit Beschluss Nummer 87-14/1981 vom 1.
     Juli 1981 des Kreistages Eberswalde für die Wasserwerke Werbellin und „Pionierlager ‚Makar.‘ Brodowin“ festgesetzten Trinkwasserschutzgebiete,
27.  die mit Beschluss Nummer 70-17/81 vom 18.
     Dezember 1981 des Kreistages Prenzlau für die Versorgungsgebiete Arendsee, Baumgarten, Cremzow, Ferdinandshorst, Kröchlendorf, Ludwigsburg, Tornow, Wittstock und Zernikow festgesetzten Trinkwasserschutzgebiete,
28.  das mit Beschluss Nummer 101/12/81 vom 22.
     Dezember 1981 des Kreistages Gransee für das „Wasserwerk NVA Objekt Wolfsruh“ festgesetzte Trinkwasserschutzgebiet,
29.  die mit Beschluss Nummer 0075/81 vom 21.
     Dezember 1981 des Kreistages Königs Wusterhausen für die Wassergewinnungsanlagen „VEB Likörfabrik Zernsdorf“, „VEB Molkerei Königs Wusterhausen“ und „Nationale Volksarmee, Objekt Kablow“ festgesetzten Trinkwasserschutzgebiete,
30.  die mit Beschluss Nummer 85 vom 21.
    Dezember 1981 des Kreistages Templin für die Wassergewinnungsanlagen Annenwalde, Alt-Tremmen, Fürstenau, Haferkamp, Lychen II, Poratz und Ruthenberg festgesetzten Trinkwasserschutzgebiete.

(2) Folgende, auf der Grundlage des § 29 des Wassergesetzes vom 2.
Juli 1982 und der Dritten Durchführungsverordnung zum Wassergesetz – Schutzgebiete und Vorbehaltsgebiete – vom 2.
Juli 1982 (GBl.
I Nr. 26 S. 487) festgesetzte Trinkwasserschutzgebiete werden aufgehoben:

1.  das mit Beschluss Nummer 10/82 vom 18.
     November 1982 des Kreistages Guben für das Wasserwerk Groß Gastrose / Taubendorf festgesetzte Trinkwasserschutzgebiet,
2.  das mit Beschluss Nummer 87-24/83 vom 13.
     Juli 1983 des Kreistages Angermünde für das „Wasserwerk Mescherin OT Staffelde“ festgesetzte Trinkwasserschutzgebiet,
3.  die mit Beschluss Nummer 157/83 vom 7.
     September 1983 des Kreistages Strausberg für die Wasserwerke „Altlandsberg, Rinderkombinat“, „Altlandsberg, VEB Ratio Neuenhagen“, „Buchholz, Hochschule f.
    Ökonomie Schulungszentrum“, „Buchholz, VEB Signal- und Sicherungstechnik, Ferienobjekt“, „Dahlwitz-Hoppegarten VEB Rennbetrieb“, „Dahlwitz-Hoppegarten VEB IFA-Vertrieb“, „Dahlwitz-Hoppegarten, Kombinat Elektronische Bauelemente, Ingenieur und Projektierungsbüro“, „Dahlwitz-Hoppegarten VEB BTV - Berlin“, „Garzin OT Liebenhof“, „Garzin Stallkomplex / Gärtnerei“, „Grunow / Ernsthof, OT Ernsthof“, „Herzfelde KIM Industrielle Kälbermast“, „Hennickendorf Zusatzeinspeisung VEB WAB“, „Hohenstein, Rinder-anlage“, „Hennickendorf Zusatzeinspeisung TGA“, „Klosterdorf, Stallkomplex LPG (T) Klosterdorf“, „Müncheberg ZE Eggersdorf / Siedlung (WAB)“, „Müncheberg, ZBE ACZ“, „Müncheberg, VEG (T) Schweinemastanlage“, „Müncheberg, VEG (T) Schäferei Marienfeld“, „Neuenhagen, Bezirksfleischkombinat“, „Neuenhagen, VKSK Entrichstraße“, „Prötzel LPG (T) Prötzel-MVA“, „Rehfelde Herrenseestraße“, „Rehfelde ZWVA Schulstraße“, „Strausberg, Treuenhof“, „Strausberg, Ferienlager VEB Makosit“, „Strausberg, Gaststätte Postbruchhütte“, „Vogelsdorf KIM Spreenhagen BT ‚Cairina‘“, „Petershagen, Konsumbackwarenkombinat“, „Waldsieversdorf, Forstinstitut“, „Werder IRA Rinderkombinat“ und „Werder Scheune Ritzow“ festgesetzten Trinkwasserschutzgebiete,
4.  die mit Beschluss Nummer 18-24/83 vom 12.
     September 1983 des Kreistages Seelow für die Wasserwerke Altbarnim, Dolgelin, Döbberin, Genschmar, Kreis-Feierabendheim Falkenhagen, Falkenhagen, Groß Neuendorf, Kienitz/Nord, Marxdorf, Steintoch OT Wollup, Mallnow, Neubarnim und Worin festgesetzten Trinkwasserschutzgebiete,
5.  die mit Beschluss Nummer 145-27/83 vom 2.
     November 1983 des Kreistages Bernau für die Wasserwerke Bernau und Klosterfelde festgesetzten Trinkwasserschutzgebiete,
6.  das mit Beschluss Nummer 0134 vom 22.
     Dezember 1983 des Kreistages Nauen für das Wasserwerk Ketzin (Erich-Weinert-Siedlung) festgesetzte Trinkwasserschutzgebiet,
7.  das mit Beschluss über die Festlegung eines Trinkwasserschutzgebietes für das Wasserwerk Werchau vom 1.
    November 1984 des Kreistages Herzberg festgesetzte Trinkwasserschutzgebiet,
8.  das mit Beschluss Nummer 0051/85 vom 28.
     August 1985 des Kreistages Rathenow für die Trinkwasserfassungsanlage Semlin festgesetzte Trinkwasserschutzgebiet,
9.  die mit Beschluss Nummer 29/85 vom 4.
    September 1985 des Kreistages Bad Freienwalde für die Trinkwassergewinnungsanlagen „LPG (T) Altranft WW MVA“, „LPG (T) Altwrz.
    / Eichw.
    WW/OT Altwriezen“, „LPG (T) Neuküstrinchen WW Altglietzen“, „VEB Feuerfestwerke WETRO, Wasserwerk Bad Freienw.“, „VEB (K) Großwäscherei WW Bad Freienw.“, „LPG (P) Neulewin WW Eichwerder Gärtnerei“, „VdgB Komb.
     Milchwirtsch.
    WW Wriezen“, „VE Landbaukomb.
    Wriezen WW Wriezen / Betonwerk“, „VEB KIM Entenzucht und -produktion.
    WW Wriezen / BT Aufzucht“, „VEB KIM Entenzucht und -produktion.
    WW Wriezen / BT Trockenmast“, „ZGE Milchproduktion Steinbeck WW MPA“, „R.
    d.
    Gem.
    Altranft WW Altranft“, „VEB WAB FfO.
    WW Leuenberg“, „VEB WAB FfO.
    WW Hohensaaten Brunnen im Uhlengrund“, „LPG (T) Schulzendorf WW MVA“ und Wriezen festgesetzten Trinkwasserschutzgebiete,
10.  die mit Beschluss Nummer 0059/86 vom 24.
     März 1986 des Kreistages Königs Wusterhausen für die Wasserversorgungsanlagen „HKW Interflug Flughafen Schönefeld“, „48 Wohneinheiten südlich Straße nach Teurow“ und „Deutsche Reichsbahn Schwellenwerk Zernsdorf“ festgesetzten Trinkwasserschutzgebiete,
11.  das mit Beschluss Nummer 0081/86 vom 26.
     Juni 1986 des Kreistages Rathenow für die Trinkwasserfassungsanlage Steckelsdorf festgesetzte Trinkwasserschutzgebiet,
12.  die mit Beschluss Nummer 51-11/1986 vom 2.
     Juli 1986 des Kreistages Eberswalde in Ergänzung des Kreistagsbeschlusses Nummer 87-14/1981 festgesetzten Trinkwasserschutzgebiete „WW Bölkendorf (18 WE)“,  
    „BA WBU Niederfinow Hebewerk“, „BA Hohenfinow, Am Bahnhof Niederfin.“, „BA Hohenfinow, Struwenberg Nummer 4“, „BA Pflegeheim Liepe“, „BA VEB Komb.
     Milchwirtsch.
    Ebw.“, „WW Altenhof (Kinderhort)“, „WW Erholungsheim ‚K.
     Liebknecht‘ Finowfurt“ und „BA VEB Energiekomb.
    Heizwerk Coppistr.“,
13.  das mit Beschluss Nummer 134/9/87 vom 17.
     September 1987 des Kreistages Gransee für die Wasserfassungsanlagen des „VEB Grüneberger Spirituosenfabrik“ festgesetzte Wasserschutzgebiet,
14.  das mit Beschluss Nummer 116/88 vom 21.
    März 1988 des Kreistages Luckenwalde präzisierte Trinkwasserschutzgebiet „GWV Liepe“,
15.   das mit Beschluss Nummer KT 34/89 vom 21.
    September 1989 des Kreistages Guben für das Wasserwerk Drewitz festgesetzte Trinkwasserschutzgebiet.

§ 2   
**Inkrafttreten**

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 29.
August 2013

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack