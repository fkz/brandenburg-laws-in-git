## Verordnung zur Übertragung der Zuständigkeit und der Aufgaben für die Verwaltung des Nationalparks „Unteres Odertal“ auf die Einrichtung „Nationalpark Unteres Odertal-Verwaltung“

Auf Grund des § 32 Absatz 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr.
3) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1  
Zuständigkeitsübertragung
-------------------------------

(1) Durch diese Verordnung wird die Zuständigkeit für die Verwaltung des Nationalparks Unteres Odertal im Sinne des § 32 Absatz 1 Satz 1 und 2 des Brandenburgischen Naturschutzausführungsgesetzes und für die Wahrnehmung der Befugnisse und Aufgaben der Nationalparkverwaltung Unteres Odertal gemäß Nationalparkgesetz Unteres Odertal auf die Einrichtung des Landes gemäß § 13 des Landesorganisationsgesetzes übertragen.

(2) Die Bezeichnung der in Absatz 1 genannten Einrichtung lautet „Nationalpark Unteres Odertal-Verwaltung“.

§ 2  
Fachaufsicht
------------------

Die Einrichtung untersteht der Fachaufsicht des für Naturschutz und Landschaftspflege zuständigen Fachministeriums des Landes Brandenburg.

§ 3  
Inkrafttreten
-------------------

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 28.
April 2014

Die Ministerin für Umwelt, Gesundheit und Verbraucherschutz

Anita Tack