## Verordnung zur Übertragung der Befugnis zur Regelung der Dienstkleidung für Beamtinnen und Beamte des Polizeivollzugsdienstes im Land Brandenburg  (Polizeidienstkleidungsübertragungsverordnung - BbgPolDkÜV)

Auf Grund des § 59 Satz 2 und des § 109 Absatz 1 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl. I S. 26) in Verbindung mit § 9 Absatz 1 des Landesorganisationsgesetzes vom 24.
 Mai 2004 (GVBl.
I S. 186) verordnet die Landesregierung:

### § 1  
Übertragung

Die Befugnis nach § 59 Satz 2 des Landesbeamtengesetzes zum Erlass von Verwaltungsvorschriften über die Dienstkleidung wird für den Bereich des Polizeivollzugsdienstes des Landes Brandenburg auf das Ministerium des Innern übertragen.

### § 2  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 24.
Februar 2012

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Matthias Platzeck

Der Minister des Innern

Dr.
Dietmar Woidke