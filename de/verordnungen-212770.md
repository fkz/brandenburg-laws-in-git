## Brandenburgische Verordnung über die Festsetzung des Lärmschutzbereichs für den Verkehrsflughafen Berlin Brandenburg (FlugLärmSBBbgV)

Auf Grund des § 4 Absatz 2 Satz 1 des Gesetzes zum Schutz gegen Fluglärm in der Fassung der Bekanntmachung vom 31.
Oktober 2007 (BGBl.
 I S.
2550) verordnet die Landesregierung:

§ 1  
**Zweck und Geltungsbereich**

Zum Schutz der Allgemeinheit und der Nachbarschaft vor Gefahren, erheblichen Nachteilen und erheblichen Belästigungen durch Fluglärm in der Umgebung des Verkehrsflughafens Berlin Brandenburg wird für das Brandenburger Hoheitsgebiet der in § 2 bestimmte Lärmschutzbereich festgesetzt.

§ 2   
**Lärmschutzbereich**

(1) Der Lärmschutzbereich gliedert sich in die Tag-Schutzzonen 1 und 2 sowie in die Nacht-Schutzzone nach § 2 Absatz 2 Nummer 2 des Gesetzes zum Schutz gegen Fluglärm.
Die Schutzzonen umfassen jeweils das Gebiet außerhalb des Flugplatzgeländes.
Sie werden durch die in Anlage 1 aufgeführten, ohne Glättungsverfahren verbundenen Kurvenpunkte bestimmt.

(2) Soweit sich die Tag-Schutzzone 2 und die Nacht-Schutzzone auf das Hoheitsgebiet des Landes Berlin erstrecken, sind die betreffenden Angaben in dieser Verordnung nur nachrichtlich.

§ 3  
**Bauliche Anlagen**

Liegt eine bauliche Anlage zu einem Teil in einer der Schutzzonen des § 2, so gilt sie als ganz in dieser Schutzzone gelegen.

§ 4  
**Topografische Karten**

(1) Der nach § 2 bestimmte Lärmschutzbereich ist als Übersichtskarte im Maßstab 1 : 50 000 und in 42 Detailkarten im Maßstab 1 : 5 000 dargestellt.
Die Übersichtskarte ist in verkleinerter Form als Anlage 2, Karte 1 dieser Verordnung beigefügt.

(2) In den gemäß Absatz 3 niedergelegten Detailkarten sind die für den Zeitpunkt der Entstehung von Ansprüchen nach § 9 des Gesetzes zum Schutz gegen Fluglärm maßgeblichen Isophonen und die für die Schallschutzanforderungen an bauliche Anlagen maßgeblichen Isophonen-Bänder nach § 3 Absatz 1 Satz 1 der Flugplatz-Schallschutzmaßnahmenverordnung vom 8.
September 2009 (BGBl.
I S.
 2992) nachrichtlich dargestellt.
 Die Schutzzonen liegen innerhalb der jeweiligen Begrenzungskurven.

(3) Die Übersichtskarte und die 42 Detailkarten sind im Landesamt für Umwelt, Gesundheit und Verbraucherschutz, Seeburger Chaussee 2, 14476 Potsdam OT Groß Glienicke, zu jedermanns Einsicht archivmäßig gesichert niedergelegt.

§ 5  
**Inkrafttreten, Außerkrafttreten**

(1) Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

(2) Diese Verordnung ersetzt nach Artikel 3 des Gesetzes zur Verbesserung des Schutzes vor Fluglärm in der Umgebung von Flugplätzen vom 1.
Juni 2007 (BGBl.
I S.
986) die Verordnung über die Festsetzung des Lärmschutzbereichs für den Verkehrsflughafen Berlin-Schönefeld vom 16.
Juni 1997 (BGBl.
I S.
1374).

Potsdam, den 7.
August 2013

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Matthias Platzeck

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

### Anlagen

1

[Anlage 1](/br2/sixcms/media.php/68/GVBl_II_61_2013-Anlage%201.pdf "Anlage 1") 7.4 MB

2

[Anlage 2](/br2/sixcms/media.php/68/GVBl_II_61_2013-Anlage%202.pdf "Anlage 2") 8.8 MB