## Verordnung zur Festsetzung des Wasserschutzgebietes für das Wasserwerk Potsdam-Leipziger Straße

Auf Grund des § 51 Absatz 1 Satz 1 Nummer 1 und Satz 2 und Absatz 2 und des § 52 Absatz 1 des Wasserhaushaltsgesetzes vom 31.
Juli 2009 (BGBl.
I S. 2585) in Verbindung mit § 15 Absatz 1 Satz 1 und Absatz 2 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1   
Allgemeines
------------------

(1) Zur Sicherung der öffentlichen Wasserversorgung wird zum Schutz des Grundwassers im Einzugsgebiet der Wasserfassungen des Wasserwerkes Potsdam-Leipziger Straße das in § 2 näher umschriebene Wasserschutzgebiet festgesetzt.
Begünstigte ist die Energie und Wasser Potsdam GmbH.

(2) Das Wasserschutzgebiet gliedert sich in den Fassungsbereich (Zone I), in die engere Schutzzone (Zone II) und in die weitere Schutzzone (Zone III).

§ 2  
Räumlicher Geltungsbereich
--------------------------------

(1) Lage und Größe des Wasserschutzgebietes und der Schutzzonen ergeben sich aus der Übersichtskarte in der Anlage 2 und den in Absatz 2 genannten Karten.

(2) Die Schutzzonen sind in der topografischen Karte im Maßstab 1 : 10 000 in der Anlage 3 und außerdem in der Liegenschaftskarte im Maßstab 1 : 2 500 in der Anlage 4, die aus zwölf Blättern besteht, dargestellt.
Für die Abgrenzung der Schutzzonen ist die Darstellung in der Liegenschaftskarte maßgebend.

(3) Zu Informationszwecken werden zusätzlich auf Papier ausgefertigte Exemplare der in Absatz 2 genannten Karten bei den unteren Wasserbehörden der kreisfreien Stadt Potsdam und des Landkreises Potsdam-Mittelmark sowie in den Gemeinden Michendorf und Nuthetal hinterlegt und können dort während der Dienststunden von jedermann kostenlos eingesehen werden.
Diese Karten sind mit dem Dienstsiegel des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (Siegelnummer 20) versehen.
Eine weitere so gesiegelte Ausfertigung der Karten befindet sich im Ministerium für Umwelt, Gesundheit und Verbraucherschutz.

(4) Veränderungen der Topografie sowie von Flurstücksgrenzen oder -bezeichnungen berühren den räumlichen Geltungsbereich der Schutzzonen nicht.

§ 3  
Schutz der Zone III
-------------------------

In der Zone III sind verboten:

1.  das Düngen mit Düngemitteln aller Art, ausgenommen in Haus- und Kleingärten,
    
2.  die Anwendung von Pflanzenschutzmitteln oder von Biozidprodukten,
    
    1.  wenn die Pflanzenschutzmittel nicht für Wasserschutzgebiete zugelassen sind,
        
    2.  wenn die Zulassungs- und Anwendungsbestimmungen nicht eingehalten werden,
        
    3.  wenn der Einsatz von Pflanzenschutzmitteln nicht durch Anwendung der allgemeinen Grundsätze des integrierten Pflanzenschutzes und der Einsatz von Biozidprodukten in entsprechender Weise auf das notwendige Maß beschränkt wird,
        
    4.  wenn keine flächenbezogenen Aufzeichnungen über den Einsatz von Pflanzenschutzmitteln nach dem Pflanzenschutzgesetz und für Biozidprodukte in entsprechender Weise über den Einsatz geführt und mindestens sieben Jahre lang nach dem Einsatz aufbewahrt werden,
        
    5.  in einem Abstand von weniger als 10 Metern zu oberirdischen Gewässern,
        
    6.  zur Bodenentseuchung oder
        
    7.  auf Dauergrünland und Grünlandbrachen,
        
3.  das Errichten, Erweitern oder Betreiben von befestigten Dunglagerstätten, ausgenommen befestigte Dunglagerstätten mit Sickerwasserfassung und dichtem Jauchebehälter, der über eine Leckageerkennungseinrichtung verfügt,
    
4.  das Errichten, Erweitern oder Betreiben von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle, Jauche, Silagesickersaft, Gärresten und flüssigem Kompost,
    
5.  die Freilandtierhaltung im Sinne der Anlage 1 Nummer 1, ausgenommen Kleintierhaltung für die Eigenversorgung,
    
6.  die Erstanlage oder Erweiterung von Baumschulen, forstlichen Pflanzgärten oder Weihnachtsbaumkulturen, ausgenommen Containerproduktion von Baumschulprodukten auf versiegelten Flächen,
    
7.  Erstaufforstungen mit Nadelbaumarten oder Robinien,
    
8.  die Umwandlung von Wald in eine andere Nutzungsart,
    
9.  Holzerntemaßnahmen, die eine gleichmäßig verteilte Überschirmung von weniger als 60 Prozent des Waldbodens oder Freiflächen größer als 1 000 Quadratmeter erzeugen, ausgenommen Femel- oder Saumschläge,
    
10.  das Einrichten oder Erweitern von Holzlagerplätzen über 100 Raummeter, die dauerhaft oder mit Nassholzkonservierung betrieben werden,
    
11.  Erdaufschlüsse im Sinne des § 49 Absatz 1 des Wasserhaushaltsgesetzes, selbst wenn Grundwasser nicht aufgedeckt wird, ausgenommen das Verlegen von Ver- und Entsorgungsleitungen und die Herstellung von Baugruben und Bohrungen,
    
12.  das Errichten, Erweitern oder Erneuern von
    
    1.  Bohrungen, welche die gering leitende Deckschichten über oder unter dem genutzten Grundwasser verletzen können,
        
    2.  Grundwassermessstellen oder
        
    3.  Brunnen,
        
    
    ausgenommen das Erneuern von Brunnen für Entnahmen mit bei Inkrafttreten dieser Verordnung rechtskräftiger wasserrechtlicher Erlaubnis oder Bewilligung,
    
13.  das Errichten oder Erweitern von vertikalen Anlagen zur Gewinnung von Erdwärme,
    
14.  das Errichten oder Erweitern von Anlagen zum Umgang mit wassergefährdenden Stoffen, ausgenommen doppelwandige Anlagen mit Leckanzeigegerät und ausgenommen Anlagen, die mit einem Auffangraum ausgerüstet sind, der das maximal in der Anlage vorhandene Volumen wassergefährdender Stoffe aufnehmen kann, und soweit
    
    1.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 1 das für die Anlage maßgebende Volumen von 1 000 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 1 die für die Anlage maßgebende Masse von 1 000 Tonnen,
        
    2.  in oberirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 2 das für die Anlage maßgebende Volumen von 100 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 2 die für die Anlage maßgebende Masse von 100 Tonnen,
        
    3.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 2 das für die Anlage maßgebende Volumen von 10 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 2 die für die Anlage maßgebende Masse von 10 Tonnen,
        
    4.  in oberirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 3 das für die Anlage maßgebende Volumen von 10 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 3 die für die Anlage maßgebende Masse von 10 Tonnen,
        
    5.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 3 das für die Anlage maßgebende Volumen von 1 Kubikmeter beziehungsweise bei festen oder gasförmigen Stoffen der Wasser-gefährdungsklasse 3 die für die Anlage maßgebende Masse von 1 Tonne
        
    
    nicht überschritten wird,
    
15.  der Umgang mit wassergefährdenden Stoffen im Sinne des § 62 Absatz 3 des Wasserhaushaltsgesetzes außerhalb von zugelassenen Anlagen, Vorrichtungen und Behältnissen, aus denen ein Eindringen in den Boden nicht möglich ist, ausgenommen der Umgang mit haushaltsüblichen Kleinstmengen,
    
16.  das Einleiten oder Einbringen von wassergefährdenden Stoffen im Sinne des § 62 Absatz 3 des Wasserhaushaltsgesetzes in den Untergrund oder in Gewässer,
    
17.  das Errichten oder Erweitern von Rohrleitungsanlagen für wassergefährdende Stoffe, ausgenommen Rohrleitungsanlagen im Sinne des § 62 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes,
    
18.  das Errichten von Anlagen zur behälterlosen Lagerung oder Ablagerung von Stoffen im Untergrund,
    
19.  das Behandeln, Lagern oder Ablagern von Abfall, tierischen Nebenprodukten oder bergbaulichen Rückständen, ausgenommen
    
    1.  die vorübergehende Lagerung in dichten Behältern,
        
    2.  die ordnungsgemäße kurzzeitige Bereitstellung von vor Ort angefallenem Abfall zum Abtransport durch den Entsorgungspflichtigen,
        
    3.  die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen,
        
20.  das Ein- oder Aufbringen von Abfällen, bergbaulichen Rückständen oder Ersatzbaustoffen in oder auf Böden oder deren Einbau in bodennahe technische Bauwerke, ausgenommen die Umsetzung des Abschlussbetriebsplans Potsdam-Süd P15-1.4-1-4 vom 30.
    Juli 1997, soweit hierdurch keine nachteiligen Veränderungen von Gewässereigenschaften zu besorgen sind,
    
21.  das Errichten oder Erweitern von Anlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden radioaktiver Stoffe im Sinne des Atomgesetzes, ausgenommen für medizinische Anwendungen sowie für Mess-, Prüf- und Regeltechnik,
    
22.  das Errichten von Industrieanlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden wassergefährdender Stoffe in großem Umfang, wie zum Beispiel in Raffinerien, Metallhütten oder chemischen Fabriken,
    
23.  das Errichten von Kraftwerken oder Heizwerken, die der Genehmigungspflicht nach Bundesimmissions-schutzrecht unterliegen, ausgenommen mit Gas, Sonnenenergie oder Windkraft betriebene Anlagen,
    
24.  das Errichten von Biogasanlagen,
    
25.  das Errichten oder Erweitern von Abwasserbehandlungsanlagen, ausgenommen
    
    1.  die Sanierung bestehender Abwasserbehandlungsanlagen zugunsten des Gewässerschutzes und
        
    2.  Abwasservorbehandlungsanlagen wie Fett-, Leichtflüssigkeits- oder Amalgamabscheider,
        
26.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik eingehalten werden,
    
27.  das Errichten oder Erweitern von Niederschlagswasser- oder Mischwasserentlastungsbauwerken,
    
28.  das Errichten oder Erweitern von Abwassersammelgruben, ausgenommen
    
    1.  Anlagen mit allgemeiner bauaufsichtlicher Zulassung des Deutschen Institutes für Bautechnik und
        
    2.  monolithische Sammelgruben aus Beton, die den allgemein anerkannten Regeln der Technik entsprechen,
        
29.  das Betreiben oder Unterhalten von Abwassersammelgruben, wenn der Wasserbehörde nicht
    
    1.  vor Inbetriebnahme,
        
    2.  bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung sowie
        
    3.  wiederkehrend alle fünf Jahre
        
    
    ein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit vorgelegt wird,
    
30.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten, ausgenommen Anlagen mit dichtem Behälter,
    
31.  das Einleiten von Abwasser – mit Ausnahme von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 Nummer 2 – in oberirdische Gewässer, sofern die Einleitung nicht schon zum Zeitpunkt des Inkrafttretens dieser Verordnung wasserrechtlich zugelassen war,
    
32.  das Ausbringen von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes,
    
33.  das Einleiten oder Versickern von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser,
    
34.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des  
    Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen
    
    1.  das breitflächige Versickern von Niederschlagswasser von gering belasteten Herkunftsflächen im Sinne der Anlage 1 Nummer 2 über die belebte Bodenzone einer ausreichend mächtigen und bewachsenen Oberbodenschicht gemäß den allgemein anerkannten Regeln der Technik oder
        
    2.  mit wasserrechtlicher Erlaubnis,
        
    
    sofern die Versickerung außerhalb von Altlasten, Altlastenverdachtsflächen oder Flächen mit schädlichen Bodenveränderungen und nur auf Flächen mit einem zu erwartenden Flurabstand des Grundwassers von 100 Zentimetern oder größer erfolgt,
    
35.  das Anwenden von Auftaumitteln auf Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen auf der Bundesstraße 2, den Straßen „Am Havelblick“, „Am Brauhausberg“, „Alter Tornow“, der Heinrich-Mann-Allee, der Albert-Einstein-Straße, der Templiner Straße, der Tornowstraße, der Drevestraße und der Friedhofsgasse sowie bei Extremwetterlagen wie Eisregen,
    
36.  das Errichten sowie der Um- oder Ausbau von Straßen oder Wegen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik für bautechnische Maßnahmen an Straßen in Wasserschutzgebieten eingehalten werden,
    
37.  das Errichten oder Erweitern von Bahnhöfen oder Schienenwegen der Eisenbahn, ausgenommen Baumaßnahmen an vorhandenen Anlagen zur Anpassung an den Stand der Technik und zum Erhalt oder zur Verbesserung der Verkehrssicherheit,
    
38.  das Verwenden von Baustoffen, Böden oder anderen Materialien, die auslaug- und auswaschbare wassergefährdende Stoffe enthalten (zum Beispiel Schlacke, Bauschutt, Teer, Imprägniermittel) für Bau- und Unterhaltungsmaßnahmen, zum Beispiel im Straßen-, Wege-, Wasser-, Landschafts- oder Tiefbau,
    
39.  das Einrichten, Erweitern oder Betreiben von öffentlichen Freibädern oder Zeltplätzen sowie Camping aller Art, ausgenommen
    
    1.  Einrichtungen mit ordnungsgemäßer Abfall- und Abwasserentsorgung und
        
    2.  das Zelten von Fuß-, Rad-, Reit- und Wasserwanderern abseits von Zelt- und Campingplätzen für eine Nacht,
        
40.  das Einrichten, Erweitern oder Betreiben von Sportanlagen, ausgenommen Anlagen mit ordnungsgemäßer Abfall- und Abwasserentsorgung,
    
41.  das Errichten von Motorsportanlagen,
    
42.  das Errichten oder Erweitern von Schießständen oder Schießplätzen für Feuerwaffen, ausgenommen Schießstände in geschlossenen Räumen,
    
43.  das Errichten von Golfanlagen,
    
44.  das Abhalten oder Durchführen von Märkten, Volksfesten oder Großveranstaltungen außerhalb der dafür vorgesehenen Anlagen,
    
45.  das Durchführen von Motorsportveranstaltungen, ausgenommen das Durchfahren auf klassifizierten Straßen,
    
46.  Bestattungen, ausgenommen innerhalb bereits bei Inkrafttreten dieser Verordnung bestehender Friedhöfe,
    
47.  das Errichten von Flugplätzen im Sinne des § 6 Absatz 1 Satz 1 des Luftverkehrsgesetzes,
    
48.  das Starten oder Landen motorgetriebener Luftfahrzeuge, mit Ausnahme in Fällen des § 25 Absatz 2 des Luftverkehrsgesetzes,
    
49.  das Errichten von militärischen Anlagen, Standort- oder Truppenübungsplätzen,
    
50.  das Durchführen von militärischen Übungen, ausgenommen das Durchfahren auf klassifizierten Straßen,
    
51.  Bergbau einschließlich die Aufsuchung oder Gewinnung von Erdöl oder Erdgas, ausgenommen die Umsetzung der bereits bei Inkrafttreten dieser Verordnung genehmigten bergrechtlichen Betriebspläne, soweit hierdurch keine nachteiligen Veränderungen von Gewässereigenschaften zu besorgen sind,
    
52.  das Durchführen von Sprengungen, sofern die Gefahr besteht, dass dabei das Grundwasser aufgedeckt wird,
    
53.  die Darstellung von neuen Bauflächen oder Baugebieten im Rahmen der vorbereitenden Bauleitplanung, wenn darin eine Neubebauung bisher unbebauter Gebiete vorgesehen wird,
    
54.  die Festsetzung von neuen Baugebieten im Rahmen der verbindlichen Bauleitplanung, ausgenommen
    
    1.  Gebiete, die im zum Zeitpunkt des Inkrafttretens dieser Verordnung gültigen Flächennutzungsplan als Bauflächen oder Baugebiete dargestellt sind, und
        
    2.  die Überplanung von Bestandsgebieten, wenn dies zu keiner wesentlichen Erhöhung der zulässigen Grundfläche im Sinne des § 19 Absatz 2 der Baunutzungsverordnung führt.
        

§ 4  
Schutz der Zone II
------------------------

Die Verbote der Zone III gelten auch in der Zone II.
In der Zone II sind außerdem verboten:

1.  das Düngen mit Düngemitteln aller Art, ausgenommen mit Kompost von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen,
    
2.  das Errichten von Dunglagerstätten,
    
3.  das Errichten von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle, Jauche, Silagesickersaft, Gärresten und flüssigem Kompost,
    
4.  die Silierung von Pflanzen oder Lagerung von Silage,
    
5.  die Freilandtierhaltung im Sinne der Anlage 1 Nummer 1, ausgenommen Kleintierhaltung für die Eigenversorgung, sofern diese bereits bei Inkrafttreten dieser Verordnung ausgeübt wurde,
    
6.  die Beweidung,
    
7.  die Anwendung von Biozidprodukten außerhalb geschlossener Gebäude oder von Pflanzenschutzmitteln,
    
8.  das Errichten, Erweitern oder Erneuern von Dränungen oder Entwässerungsgräben,
    
9.  der Einsatz von forstwirtschaftlichen Kraftfahrzeugen abseits von Straßen, Wegen oder forstwirtschaftlichen Rückegassen,
    
10.  das Vergraben, Lagern oder Ablagern von Tierkörpern oder Teilen davon,
    
11.  das Errichten oder Betreiben von Wildfütterungen, Kirrungen oder Luderplätzen,
    
12.  das Errichten, Erweitern oder Betreiben von Anlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden wassergefährdender Stoffe,
    
13.  der Einsatz von mineralischen Schmierstoffen zur Verlustschmierung oder von mineralischen Schalölen,
    
14.  das Lagern, Abfüllen oder Umschlagen wassergefährdender Stoffe, ausgenommen haushaltsübliche Kleinstmengen,
    
15.  das Befahren mit Fahrzeugen mit wassergefährdender Ladung, nachdem die Anordnung des entsprechenden Vorschriftzeichens 269 durch die Straßenverkehrsbehörde erfolgte,
    
16.  das Befahren mit Kraftfahrzeugen mit einer Geschwindigkeit über 30 Kilometer pro Stunde, nachdem die Anordnung des Vorschriftzeichens 274 durch die Straßenverkehrsbehörde erfolgte,
    
17.  das Errichten von Transformatoren oder Stromleitungen mit flüssigen wassergefährdenden Kühl- oder Isoliermitteln,
    
18.  das Behandeln, Lagern oder Ablagern von Abfall, bergbaulichen Rückständen oder tierischen Nebenprodukten, ausgenommen
    
    1.  die ordnungsgemäße kurzzeitige Bereitstellung von in der Zone II angefallenem Abfall zum Abtransport durch den Entsorgungspflichtigen und
        
    2.  die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen,
        
19.  der Umgang mit radioaktiven Stoffen im Sinne des Atomgesetzes,
    
20.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, ausgenommen die Abwasserdruckleitung Caputh-Potsdam und Anlagen, die zur Entsorgung vorhandener Bebauung dienen, wenn hierbei die allgemein anerkannten Regeln der Technik eingehalten werden,
    
21.  das Errichten, Erweitern oder Betreiben von Abwassersammelgruben,
    
22.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten,
    
23.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen das breitflächige Versickern von Niederschlagswasserabflüssen von gering belasteten Herkunftsflächen im Sinne der Anlage 1 Nummer 2 über die belebte Bodenzone einer mindestens 20 Zentimeter mächtigen und bewachsenen Oberbodenschicht,
    
24.  das Einleiten von Abwasser, mit Ausnahme von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 Nummer 2, in oberirdische Gewässer,
    
25.  das Errichten sowie der Um- oder Ausbau von Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen Baumaßnahmen an vorhandenen Straßen und Wegen zur Anpassung an den Stand der Technik und zur  
    Verbesserung der Verkehrssicherheit unter Einhaltung der allgemein anerkannten Regeln der Technik,
    
26.  das Errichten, Erweitern oder Betreiben von öffentlichen Freibädern oder Zeltplätzen sowie Camping aller Art,
    
27.  das Errichten oder Erweitern von Sportanlagen,
    
28.  das Abhalten oder Durchführen von Sportveranstaltungen, Märkten, Volksfesten oder Großveranstaltungen, ausgenommen Sportveranstaltungen im Bereich des Sportplatzes an der Templiner Straße,
    
29.  das Errichten oder Erweitern von Baustelleneinrichtungen oder Baustofflagern,
    
30.  das Durchführen von Bohrungen, ausgenommen Maßnahmen zur Abwehr von Gefahren für das Grundwasser unter Beachtung der Sicherheitsvorkehrungen zum Grundwasserschutz,
    
31.  das Durchführen von unterirdischen Sprengungen,
    
32.  das Errichten oder Erweitern von baulichen Anlagen, ausgenommen Veränderungen in Gebäuden und Instandhaltungsmaßnahmen.
    

§ 5  
Schutz der Zone I
-----------------------

Die Verbote der Zonen III und II gelten auch in der Zone I.
In der Zone I sind außerdem verboten:

1.  das Betreten oder Befahren,
    
2.  landwirtschaft-, forstwirtschaft- oder gartenbauliche Nutzung,
    
3.  Veränderungen oder Aufschlüsse der Erdoberfläche.
    

§ 6   
Maßnahmen zur Wassergewinnung
------------------------------------

Die Verbote des § 4 Nummer 12, 19, 24, 29 bis 32 sowie des § 5 Nummer 1 und 3 gelten nicht für Maßnahmen zur Wassergewinnung, die durch diese Verordnung geschützt ist.

§ 7   
Widerruf von Befreiungen
-------------------------------

(1) Befreiungen nach § 52 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes sind widerruflich und bedürfen der Schriftform.
Abweichend von Satz 1 ist eine Befreiung von den Verboten gemäß § 3 Nummer 53 und 54 nicht widerruflich.

(2) Im Fall des Widerrufs einer Befreiung kann die untere Wasserbehörde vom Grundstückseigentümer verlangen, dass der frühere Zustand wiederhergestellt wird, sofern es das Wohl der Allgemeinheit, insbesondere der Schutz der Wasserversorgung, erfordert.

§ 8  
Sicherung und Kennzeichnung des Wasserschutzgebietes
----------------------------------------------------------

(1) Die Zone I ist von der Begünstigten auf Anordnung der unteren Wasserbehörde gegen unbefugtes Betreten, zum Beispiel durch eine Umzäunung, zu sichern.

(2) Die Begünstigte hat auf Anordnung der unteren Wasserbehörde zur Kennzeichnung des Wasserschutzgebietes im Bereich öffentlicher Verkehrsflächen bei der Straßenverkehrsbehörde die Anordnung des Richtzeichens 354 und der Vorschriftzeichen 269 und 274 zu beantragen und im Bereich nichtöffentlicher Flächen in Abstimmung mit den Kommunen nichtamtliche Hinweiszeichen aufzustellen.

§ 9   
Duldungspflichten
------------------------

(1) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet haben die Überwachung des Wasserschutzgebietes, insbesondere hinsichtlich der Beachtung dieser Verordnung und der nach ihr getroffenen Anordnungen, sowie das Beobachten der Gewässer und des Bodens durch die zuständigen Wasserbehörden, die Begünstigte oder deren Beauftragte zu dulden.

(2) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet sind auf Anordnung der unteren Wasserbehörde verpflichtet,

1.  das Errichten und Unterhalten von Einrichtungen zur Sicherung der Zone I gegen unbefugtes Betreten,
    
2.  das Aufstellen, Unterhalten und Beseitigen von Hinweis-, Warn-, Gebots- und Verbotszeichen,
    
3.  das Betreten der Grundstücke durch Bedienstete der zuständigen Behörden, der Begünstigten oder deren Beauftragte zum Beobachten, Messen und Untersuchen des Grundwassers und zur Entnahme von Boden- und Vegetationsproben sowie
    
4.  das Anlegen und Betreiben von Grundwassermessstellen
    

zu dulden.
Die Anordnung erfolgt durch schriftlichen Bescheid gegenüber den betroffenen Eigentümern oder Nutzungsberechtigten.
Soweit bergrechtliche Belange berührt sind, ergeht die Entscheidung im Benehmen mit der zuständigen Bergbehörde.

(3) Auf Verlangen der Wasserbehörde ist Einsicht in die Aufzeichnungen nach § 3 Nummer 2 Buchstabe d dieser Verordnung zu gewähren oder diese unverzüglich vorzulegen.

§ 10  
Übergangsregelung
------------------------

Für bei Inkrafttreten dieser Verordnung bestehende Einleitungen oder Versickerungen von Niederschlagswasserabflüssen von mittel oder hoch belasteten Herkunftsflächen in den Untergrund ohne wasserrechtliche Erlaubnis gilt das Verbot des § 3 Nummer 34 nach einem Jahr nach Inkrafttreten dieser Verordnung.

§ 11   
Ordnungswidrigkeiten
----------------------------

(1) Ordnungswidrig im Sinne des § 103 Absatz 1 Nummer 7a des Wasserhaushaltsgesetzes handelt, wer vorsätzlich oder fahrlässig eine nach den §§ 3, 4 oder 5 verbotene Handlung ohne eine Befreiung gemäß § 52 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes vornimmt, ausgenommen die Verbote nach § 4 Nummer 16 und 17.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

§ 12  
Inkrafttreten, Außerkrafttreten
--------------------------------------

Diese Verordnung tritt am 1.
März 2014 in Kraft.
Gleichzeitig tritt das mit Beschluss Nummer 35/4/75 vom 29.
Januar 1975 der Stadtverordnetenversammlung der Stadt Potsdam festgesetzte und durch den Beschluss der Stadtverordnetenversammlung Potsdam vom 4.
Juli 1990 veränderte Trinkwasserschutzgebiet für das Wasserwerk Potsdam-Leipziger Straße außer Kraft.

Potsdam, den 11.
Februar 2014

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

### Anlagen

1

[WSGPotsdam-Leipziger Straße-Anlg-1](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-1.pdf "WSGPotsdam-Leipziger Straße-Anlg-1") 598.5 KB

2

[WSGPotsdam-Leipziger Straße-Anlg-2](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-2.pdf "WSGPotsdam-Leipziger Straße-Anlg-2") 2.7 MB

3

[WSGPotsdam-Leipziger Straße-Anlg-3](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-3.pdf "WSGPotsdam-Leipziger Straße-Anlg-3") 3.0 MB

4

[WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-01](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-4-Blatt-01.pdf "WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-01") 3.3 MB

5

[WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-02](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-4-Blatt-02.pdf "WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-02") 3.3 MB

6

[WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-03](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-4-Blatt-03.pdf "WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-03") 3.6 MB

7

[WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-04](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-4-Blatt-04.pdf "WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-04") 1.7 MB

8

[WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-05](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-4-Blatt-05.pdf "WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-05") 1.7 MB

9

[WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-06](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-4-Blatt-06.pdf "WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-06") 3.1 MB

10

[WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-07](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-4-Blatt-07.pdf "WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-07") 1.8 MB

11

[WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-08](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-4-Blatt-08.pdf "WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-08") 1.3 MB

12

[WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-09](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-4-Blatt-09.pdf "WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-09") 1.6 MB

13

[WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-10](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-4-Blatt-10.pdf "WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-10") 1.6 MB

14

[WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-11](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-4-Blatt-11.pdf "WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-11") 1.6 MB

15

[WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-12](/br2/sixcms/media.php/68/GVBl_II_10_2014-Anlage-4-Blatt-12.pdf "WSGPotsdam-Leipziger Straße-Anlg-4-Blatt-12") 2.2 MB