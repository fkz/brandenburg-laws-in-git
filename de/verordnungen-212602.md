## Verordnung zur Festsetzung des Wasserschutzgebietes für das Wasserwerk Spremberg / Grodk

Auf Grund des § 51 Absatz 1 Satz 1 Nummer 1 und Satz 2 und Absatz 2 und des § 52 Absatz 1 des Wasserhaushaltsgesetzes vom 31.
Juli 2009 (BGBl.
I S. 2585) in Verbindung mit § 15 Absatz 1 Satz 1 und Absatz 2 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1  
**Allgemeines**

(1) Zur Sicherung der öffentlichen Wasserversorgung wird zum Schutz des Grundwassers im Einzugsgebiet des Wasserwerkes Spremberg / Grodk das in § 2 näher umschriebene Wasserschutzgebiet festgesetzt.
Begünstigter ist der Spremberger Wasser- und Abwasserzweckverband.

(2) Das Wasserschutzgebiet gliedert sich in den Fassungsbereich (Zone I), in die engere Schutzzone (Zone II) und in die weitere Schutzzone (Zone III).

§ 2  
**Räumlicher Geltungsbereich**

(1) Lage und Größe des Wasserschutzgebietes und der Schutzzonen ergeben sich aus der Übersichtskarte in der Anlage 2 und den in Absatz 2 genannten Karten.

(2) Die Schutzzonen sind in der topografischen Karte im Maßstab 1 : 10 000 in der Anlage 3 und außerdem in der Liegenschaftskarte im Maßstab 1 : 2 500 in der Anlage 4, die aus acht Blättern besteht, dargestellt.
Für die Abgrenzung der Schutzzonen ist die Darstellung in der Liegenschaftskarte maßgebend.

(3) Zu Informationszwecken werden zusätzlich auf Papier ausgefertigte Exemplare der in Absatz 2 genannten Karten bei der unteren Wasserbehörde des Landkreises Spree-Neiße und der Stadt Spremberg hinterlegt und können dort während der Dienststunden von jedermann kostenlos eingesehen werden.
Diese Karten sind mit Datum vom 24.
Oktober 2011 und mit dem Dienstsiegel des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (Siegelnummer 20) versehen.
Eine weitere so gesiegelte Ausfertigung der Karten befindet sich im Ministerium für Umwelt, Gesundheit und Verbraucherschutz.

(4) Veränderungen der Topografie sowie von Flurstücksgrenzen oder -bezeichnungen berühren den räumlichen Geltungsbereich der Schutzzonen nicht.

§ 3  
**Schutz der Zone III**

In der Zone III sind verboten:

1.  das Düngen mit Gülle, Jauche, Geflügelkot, Festmist, Siliersaft oder sonstigen Düngemitteln mit im Sinne des § 2 Nummer 10 der Düngeverordnung wesentlichen Nährstoffgehalten an Stickstoff oder Phosphat,
2.  das Lagern oder Ausbringen von Fäkalschlamm oder Klärschlämmen aller Art einschließlich in Biogasanlagen behandelte Klärschlämme,
3.  das Errichten von befestigten Dunglagerstätten,
4.  das Errichten von Erdbecken zur Lagerung von Gülle, Jauche oder Silagesickersäften,
5.  das Errichten von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle,
6.  unbefestigte Feldrandzwischenlager für organische oder mineralische Dünger,
7.  das Errichten von ortsfesten Anlagen für die Silierung von Pflanzen oder die Lagerung von Silage,
8.  die Silierung von Pflanzen oder Lagerung von Silage außerhalb ortsfester Anlagen,
9.  das Errichten von Stallungen für Tierbestände,
10.  die Freilandtierhaltung, ausgenommen Kleintierhaltung für die Eigenversorgung,
11.  die Anwendung von Pflanzenschutzmitteln,
    1.  wenn die Pflanzenschutzmittel nicht für Wasserschutzgebiete zugelassen sind,
    2.  wenn keine flächenbezogenen Aufzeichnungen nach dem Pflanzenschutzgesetz über den Einsatz auf forstwirtschaftlich genutzten Flächen geführt werden,
    3.  in einem Abstand von weniger als 10 Metern zu oberirdischen Gewässern oder
    4.  zur Bodenentseuchung,
12.  das Errichten oder Erweitern von Gartenbaubetrieben oder Kleingartenanlagen,
13.  die Neuanlage von Baumschulen, forstlichen Pflanzgärten, Weihnachtsbaumkulturen, ausgenommen Containerproduktion von Baumschulprodukten auf versiegelten Flächen,
14.  der Umbruch von Offenlandflächen,
15.  das Anlegen von Schwarzbrache,
16.  Erstaufforstungen mit Nadelbaumarten oder Robinien,
17.  die Umwandlung von Wald in eine andere Nutzungsart größer als 1 000 Quadratmeter,
18.  Holzerntemaßnahmen, die eine gleichmäßig verteilte Überschirmung von weniger als 40 Prozent des Waldbodens oder Freiflächen größer als 1 000 Quadratmeter erzeugen, ausgenommen Femel- oder Saumschläge,
19.  das Einrichten oder Erweitern von dauerhaften Holzlagerplätzen über 100 Raummeter,
20.  Erdaufschlüsse im Sinne des § 49 Absatz 1 des Wasserhaushaltsgesetzes, selbst wenn Grundwasser nicht aufgedeckt wird, ausgenommen das Verlegen von Ver- und Entsorgungsleitungen und die Herstellung von Baugruben und Bohrungen,
21.  das Errichten, Erweitern oder Erneuern von Tiefenbohrungen über 100 Meter, Grundwassermessstellen oder Brunnen, ausgenommen das Erneuern von Brunnen für Entnahmen mit wasserrechtlicher Erlaubnis oder Bewilligung,
22.  das Errichten von vertikalen Anlagen zur Gewinnung von Erdwärme,
23.  das Errichten von Anlagen zum Umgang mit wassergefährdenden Stoffen, ausgenommen doppelwandige Anlagen mit Leckanzeigegerät und ausgenommen Anlagen, die mit einem Auffangraum ausgerüstet sind, der das maximal in der Anlage vorhandene Volumen wassergefährdender Stoffe aufnehmen kann, und soweit
    1.   in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 1 das für die Anlage maßgebende Volumen von 1 000 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 1 die für die Anlage maßgebende Masse von 1 000 Tonnen,
    2.  in oberirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 2 das für die Anlage maßgebende Volumen von 100 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 2 die für die Anlage maßgebende Masse von 100 Tonnen,
    3.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 2 das für die Anlage maßgebende Volumen von 10 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 2 die für die Anlage maßgebende Masse von 10 Tonnen,
    4.  in oberirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 3 das für die Anlage maßgebende Volumen von 10 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 3 die für die Anlage maßgebende Masse von 10 Tonnen,
    5.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 3 das für die Anlage maßgebende Volumen von 1 Kubikmeter beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 3 die für die Anlage maßgebende Masse von 1 Tonne
  
nicht überschritten wird,25.  das Errichten oder Erweitern von Rohrleitungsanlagen für wassergefährdende Stoffe,
26.  das Errichten, Erweitern oder Betreiben von Anlagen zur behälterlosen Lagerung oder Ablagerung von Stoffen im Untergrund,
27.  das Behandeln, Lagern oder Ablagern von Abfall, tierischen Nebenprodukten oder bergbaulichen Rückständen, ausgenommen
    1.  die vorübergehende Lagerung in dichten Behältern,
    2.  die ordnungsgemäße kurzzeitige Zwischenlagerung von vor Ort angefallenem Abfall zur Abholung durch den Entsorgungspflichtigen sowie
    3.  die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen,
28.  das Ein- oder Aufbringen von Abfällen, bergbaulichen Rückständen oder Ersatzbaustoffen in oder auf Böden sowie deren Einbau in bodennahe technische Bauwerke,
29.  das Errichten von Anlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden radioaktiver Stoffe im Sinne des Atomgesetzes, ausgenommen für medizinische Anwendungen sowie für Mess-, Prüf- und Regeltechnik,
30.  das Errichten von Industrieanlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden wassergefährdender Stoffe in großem Umfang, wie zum Beispiel in Raffinerien, Metallhütten oder chemischen Fabriken,
31.  das Errichten von Kraftwerken oder Heizwerken, die der Genehmigungspflicht nach Bundesimmissionsschutzrecht unterliegen, ausgenommen mit Gas, Sonnenenergie oder Windkraft betriebene Anlagen,
32.  das Errichten von Biogasanlagen,
33.  das Errichten von Abwasserbehandlungsanlagen,
34.  das Errichten von Abwasserkanälen oder -leitungen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik eingehalten werden,
35.  das Errichten von Regen- oder Mischwasserentlastungsbauwerken,
36.  das Errichten von Abwassersammelgruben, ausgenommen
    1.  Anlagen mit allgemeiner bauaufsichtlicher Zulassung des Deutschen Institutes für Bautechnik und
    2.  monolithische Sammelgruben aus Beton, die den allgemein anerkannten Regeln der Technik entsprechen,
37.  das Betreiben oder Unterhalten von Abwassersammelgruben, wenn der Wasserbehörde nicht
    
    1.  vor Inbetriebnahme,
    2.  bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung sowie
    3.  wiederkehrend alle fünf Jahre
    
      
    ein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit vorgelegt wird,
38.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten, ausgenommen Anlagen mit dichtem Behälter,
39.  das Einleiten von Abwasser – mit Ausnahme von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 – in oberirdische Gewässer, sofern die Einleitung nicht schon zum Zeitpunkt des Inkrafttretens dieser Verordnung wasserrechtlich zugelassen war,
40.  das Ausbringen von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes,
41.  das Einleiten oder Versickern von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser,
42.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen das großflächige Versickern von Niederschlagswasser über die belebte Bodenzone,
43.  das Anwenden von Auftaumitteln auf Straßen, Wegen oder sonstigen Verkehrsflächen,
44.  das Errichten oder Erweitern von Straßen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik für bautechnische Maßnahmen an Straßen in Wasserschutzgebieten eingehalten werden,
45.  das Errichten oder Erweitern von Bahnhöfen oder Schienenwegen der Eisenbahn, ausgenommen Baumaßnahmen an vorhandenen Anlagen zur Anpassung an den Stand der Technik und zum Erhalt oder zur Verbesserung der Verkehrssicherheit und der Verkehrsabwicklung,
46.  das Verwenden wassergefährdender, auslaug- oder auswaschbarer Materialien (zum Beispiel Schlacke, Bauschutt, Teer, Imprägniermittel) zum Wege- oder Wasserbau,
47.  das Einrichten von Zeltplätzen sowie Camping aller Art, ausgenommen
    
    1.  Einrichtungen mit ordnungsgemäßer Abfall- und Abwasserentsorgung und
    2.  das Zelten von Fuß-, Rad-, Reit- und Wasserwanderern abseits von Zelt- und Campingplätzen für eine Nacht,
48.  das Einrichten von Sportanlagen, ausgenommen Anlagen mit ordnungsgemäßer Abfall- und Abwasserentsorgung,
49.  das Errichten von Motorsportanlagen,
50.  das Errichten von Schießständen oder Schießplätzen für Feuerwaffen, ausgenommen Schießstände in geschlossenen Räumen,
51.  das Errichten von Golfanlagen,
52.  das Abhalten oder Durchführen von Märkten, Volksfesten oder Großveranstaltungen,
53.  das Durchführen von Motorsportveranstaltungen, ausgenommen das Durchfahren auf klassifizierten Straßen,
54.  Bestattungen,
55.  das Errichten von Flugplätzen im Sinne des § 6 Absatz 1 Satz 1 des Luftverkehrsgesetzes,
56.  das Starten oder Landen motorgetriebener Luftfahrzeuge, mit Ausnahme in Fällen des § 25 Absatz 2 des Luftverkehrsgesetzes,
57.  das Errichten von militärischen Anlagen, Standort- oder Truppenübungsplätzen,
58.  das Durchführen von militärischen Übungen,
59.  Bergbau einschließlich Erdöl- oder Erdgasgewinnung,
60.  das Durchführen von Sprengungen, sofern die Gefahr besteht, dass dabei das Grundwasser aufgedeckt wird,
61.  die Darstellung von neuen Bauflächen oder Baugebieten im Rahmen der vorbereitenden Bauleitplanung, wenn darin eine Neubebauung bisher unbebauter Gebiete vorgesehen wird,
62.  die Festsetzung von neuen Baugebieten im Rahmen der verbindlichen Bauleitplanung, wenn dies zu einer Erhöhung der zulässigen Grundfläche im Sinne des § 19 Absatz 2 der Baunutzungsverordnung führt.
    

§ 4  
**Schutz der Zone II**

Die Verbote der Zone III gelten auch in der Zone II.
In der Zone II sind außerdem verboten:

1.  die Beweidung,
2.  die Anwendung von Pflanzenschutzmitteln,
3.  das Errichten von Dränungen oder Entwässerungsgräben,
4.  der Einsatz von forstwirtschaftlichen Kraftfahrzeugen abseits von Straßen, Wegen oder forstwirtschaftlichen Rückegassen,
5.  das Vergraben, Lagern oder Ablagern von Tierkörpern oder Teilen davon,
6.  das Errichten oder Betreiben von Wildfütterungen, Kirrungen oder Luderplätzen,
7.  das Errichten von Anlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden wassergefährdender Stoffe,
8.  der Einsatz von mineralischen Schmierstoffen zur Verlustschmierung oder von mineralischen Schalölen,
9.  das Lagern, Abfüllen oder Umschlagen wassergefährdender Stoffe, ausgenommen haushaltsübliche Kleinstmengen,
10.  das Befahren mit Fahrzeugen mit wassergefährdender Ladung,
11.  das Errichten von Transformatoren oder Stromleitungen mit flüssigen wassergefährdenden Kühl- oder Isoliermitteln,
12.  die Lagerung von Abfall oder bergbaulichen Rückständen in dichten Behältern,
13.  der Umgang mit radioaktiven Stoffen im Sinne des Atomgesetzes,
14.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, ausgenommen Anlagen, die zur Entsorgung vorhandener Bebauung dienen und wenn hierbei die allgemein anerkannten Regeln der Technik eingehalten werden,
15.  das Errichten oder Erweitern von Abwassersammelgruben,
16.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten,
17.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen das großflächige Versickern von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 über die belebte Bodenzone,
18.  das Errichten oder Erweitern von Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen
    1.  Baumaßnahmen an vorhandenen Straßen zur Anpassung an den Stand der Technik und zur Verbesserung der Verkehrssicherheit unter Einhaltung der allgemein anerkannten Regeln der Technik für bautechnische Maßnahmen an Straßen in Wasserschutzgebieten sowie
    2.  Wege mit großflächiger Versickerung der Niederschlagswasserabflüsse über die belebte Bodenzone,
19.  das Errichten von Zeltplätzen sowie Camping aller Art,
20.  das Errichten von Sportanlagen,
21.  das Abhalten oder Durchführen von Sportveranstaltungen,
22.  das Errichten von Baustelleneinrichtungen oder Baustofflagern,
23.  das Durchführen von Bohrungen, ausgenommen Maßnahmen zur Abwehr von Gefahren für das Grundwasser unter Beachtung der Sicherheitsvorkehrungen zum Grundwasserschutz,
24.  das Durchführen von unterirdischen Sprengungen,
25.  das Errichten oder Erweitern von baulichen Anlagen, ausgenommen Veränderungen in Gebäuden und Instandhaltungsmaßnahmen.

§ 5  
**Schutz der Zone I**

Die Verbote der Zonen III und II gelten auch in der Zone I.
In der Zone I sind außerdem verboten:

1.  das Betreten oder Befahren,
2.  land-, forst- oder gartenbauliche Nutzung,
3.  Veränderungen oder Aufschlüsse der Erdoberfläche.

§ 6  
**Maßnahmen zur Wassergewinnung**

Die Verbote des § 3 Nummer 21 und 40, des § 4 Nummer 9, 13, 22 bis 25 sowie des § 5 Nummer 1 und 3 gelten nicht für Maßnahmen zur Wassergewinnung, die durch diese Verordnung geschützt ist.

§ 7   
**Widerruf von Befreiungen**

(1) Befreiungen nach § 52 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes sind widerruflich und bedürfen der Schriftform.
Abweichend von Satz 1 ist eine Befreiung von den Verboten gemäß § 3 Nummer 61 und 62 nicht widerruflich.

(2) Im Fall des Widerrufs einer Befreiung kann die untere Wasserbehörde vom Grundstückseigentümer verlangen, dass der frühere Zustand wiederhergestellt wird, sofern es das Wohl der Allgemeinheit, insbesondere der Schutz der Wasserversorgung, erfordert.

§ 8  
**Sicherung und Kennzeichnung des Wasserschutzgebietes**

(1) Die Zone I ist vom Begünstigten auf Anordnung der unteren Wasserbehörde gegen unbefugtes Betreten, zum Beispiel durch eine Umzäunung, zu sichern.

(2) Der Begünstigte hat auf Anordnung der Wasserbehörde zur Kennzeichnung des Wasserschutzgebietes im Bereich öffentlicher Verkehrsflächen bei der Straßenverkehrsbehörde die Anordnung des Verkehrszeichens 354 zu beantragen und im Bereich nichtöffentlicher Flächen in Abstimmung mit der Gemeinde nichtamtliche Hinweiszeichen aufzustellen.

§ 9  
**Duldungspflichten**

(1) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet haben die Überwachung des Wasserschutzgebietes, insbesondere hinsichtlich der Beachtung dieser Verordnung und der nach ihr getroffenen Anordnungen, sowie das Beobachten der Gewässer und des Bodens durch die zuständigen Wasserbehörden, den Begünstigten oder deren Beauftragte zu dulden.

(2) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet sind auf Anordnung der unteren Wasserbehörde verpflichtet,

1.  das Errichten und Unterhalten von Einrichtungen zur Sicherung der Zone I gegen unbefugtes Betreten,
2.  das Aufstellen, Unterhalten und Beseitigen von Hinweis-, Warn-, Gebots- und Verbotszeichen,
3.  das Betreten der Grundstücke durch Bedienstete der zuständigen Behörden, den Begünstigten oder deren Beauftragte zum Beobachten, Messen und Untersuchen des Grundwassers und zur Entnahme von Boden- und Vegetationsproben sowie
4.  das Anlegen und Betreiben von Grundwassermessstellen

zu dulden.
Die Anordnung erfolgt durch schriftlichen Bescheid gegenüber den betroffenen Eigentümern oder Nutzungsberechtigten.
Soweit bergrechtliche Belange berührt sind, ergeht die Entscheidung im Benehmen mit der zuständigen Bergbehörde.

§ 10  
**Inkrafttreten, Außerkrafttreten**

Diese Verordnung tritt mit Wirkung vom 20.
Dezember 2011 in Kraft.
Gleichzeitig treten die Verordnung zur Festsetzung des Wasserschutzgebietes für das Wasserwerk Spremberg / Grodk vom 24.
Oktober 2011 (GVBl.
II Nr. 74) und das mit Beschluss Nummer 123/77 vom 27.
Oktober 1977 des Kreistages Spremberg festgesetzte Trinkwasserschutzgebiet für das Wasserwerk Spremberg außer Kraft.

Potsdam, den 23.
Januar 2013

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

### Anlagen

1

[WSGSpremberg-Anlg-1](/br2/sixcms/media.php/68/GVBl_II_11_2013-Anlage%201.pdf "WSGSpremberg-Anlg-1") 87.9 KB

2

[WSGSpremberg-Anlg-2](/br2/sixcms/media.php/68/GVBl_II_11_2013-Anlage%202.pdf "WSGSpremberg-Anlg-2") 665.4 KB

3

[WSGSpremberg-Anlg-3](/br2/sixcms/media.php/68/GVBl_II_11_2013-Anlage%203.pdf "WSGSpremberg-Anlg-3") 1.6 MB

4

[WSGSpremberg-Anlg-4-Blatt-01](/br2/sixcms/media.php/68/GVBl_II_11_2013-Anlage%204-Blatt-01.pdf "WSGSpremberg-Anlg-4-Blatt-01") 1.3 MB

5

[WSGSpremberg-Anlg-4-Blatt-02](/br2/sixcms/media.php/68/GVBl_II_11_2013-Anlage%204-Blatt-02.pdf "WSGSpremberg-Anlg-4-Blatt-02") 710.7 KB

6

[WSGSpremberg-Anlg-4-Blatt-03](/br2/sixcms/media.php/68/GVBl_II_11_2013-Anlage%204-Blatt-03.pdf "WSGSpremberg-Anlg-4-Blatt-03") 644.1 KB

7

[WSGSpremberg-Anlg-4-Blatt-04](/br2/sixcms/media.php/68/GVBl_II_11_2013-Anlage%204-Blatt-04.pdf "WSGSpremberg-Anlg-4-Blatt-04") 778.9 KB

8

[WSGSpremberg-Anlg-4-Blatt-05](/br2/sixcms/media.php/68/GVBl_II_11_2013-Anlage%204-Blatt-05.pdf "WSGSpremberg-Anlg-4-Blatt-05") 1.0 MB

9

[WSGSpremberg-Anlg-4-Blatt-06](/br2/sixcms/media.php/68/GVBl_II_11_2013-Anlage%204-Blatt-06.pdf "WSGSpremberg-Anlg-4-Blatt-06") 611.4 KB

10

[WSGSpremberg-Anlg-4-Blatt-07](/br2/sixcms/media.php/68/GVBl_II_11_2013-Anlage%204-Blatt-07.pdf "WSGSpremberg-Anlg-4-Blatt-07") 536.2 KB

11

[WSGSpremberg-Anlg-4-Blatt-08](/br2/sixcms/media.php/68/GVBl_II_11_2013-Anlage%204-Blatt-08.pdf "WSGSpremberg-Anlg-4-Blatt-08") 388.8 KB