## Staatsvertrag der Länder Berlin und Brandenburg über die Errichtung und den Betrieb einer gemeinsamen Jugendarrestanstalt

Das Land Berlin,  
vertreten durch den Regierenden Bürgermeister,  
dieser vertreten durch den Senator für Justiz und Verbraucherschutz

und

das Land Brandenburg,  
vertreten durch den Ministerpräsidenten,  
dieser vertreten durch den Minister der Justiz und für Europa und Verbraucherschutz

schließen vorbehaltlich der Zustimmung ihrer verfassungsmäßig berufenen Organe folgenden Staatsvertrag:

### Präambel

Es ist der übereinstimmende Wille des Senats von Berlin und der Landesregierung Brandenburg, den Vollzug des Jugendarrestes in einer gemeinsamen Jugendarrestanstalt durchzuführen, ihn konsequent auf die Förderung der Arrestierten auszurichten und sozialpädagogisch auszugestalten.
Durch die Errichtung und den Betrieb dieser gemeinsamen Einrichtung sollen Synergieeffekte erzielt, weitere unterstützende Netzwerke aufgebaut, bereits bestehende Ressourcen besser genutzt und die Qualität der Arbeit insgesamt weiter verbessert werden.

### Artikel 1

(1) Die Länder Berlin und Brandenburg errichten zum Zeitpunkt des Inkrafttretens des Staatsvertrags eine gemeinsame Einrichtung für den Vollzug des Jugendarrestes in beiden Ländern.

(2) Die Einrichtung führt den Namen „Jugendarrestanstalt Berlin-Brandenburg“ (im Folgenden: Anstalt).

### Artikel 2

(1) Der Vollzug des Jugendarrestes in der Anstalt erfolgt unter Berücksichtigung der Vorgaben des Absatzes 2 auf der Grundlage der bundesrechtlichen Bestimmungen und der im Land Berlin geltenden gesetzlichen Regelungen.

(2) Der Jugendarrest wird in einer selbstständigen Anstalt der Justizverwaltung vollzogen.
Die Anstalt ist baulich und räumlich getrennt von sonstigen Einrichtungen der Justizverwaltung.
Sie verfügt über bedarfsgerechte Einrichtungen für pädagogische Gruppen- und Einzelmaßnahmen sowie über wohnliche und zweckentsprechende Arrest-, Funktions-, Gemeinschafts- und Besuchsräume und ermöglicht soziale Gruppenarbeit.
Die Arrestierten werden außerhalb der Aufschlusszeiten in ihren Arresträumen einzeln untergebracht.
Die Anstalt gewährleistet einen Vollzug des Jugendarrestes, der

1.  dem Ziel dient, den Arrestierten das von ihnen begangene Unrecht und ihre Verantwortung hierfür bewusst zu machen und ihnen Hilfen für eine Lebensführung ohne Straftaten aufzuzeigen und zu vermitteln,
2.  auf die Förderung der Arrestierten ausgerichtet ist und auf deren Auseinandersetzung mit ihren Straftaten, deren Ursachen und deren Folgen hinwirkt,
3.  die unverzichtbare Bereitschaft der Arrestierten, an der Erreichung des Arrestziels mitzuwirken, weckt und fördert,
4.  als stationäre Kurzzeitmaßnahme in die in beiden Ländern bestehenden ambulanten Hilfesysteme eingebunden wird, um einen nahtlosen Anschluss nachsorgender Maßnahmen zu ermöglichen,
5.  den Arrestierten Außenkontakte ermöglicht, wenn dies dem Arrestziel nicht entgegensteht und die Sicherheit und Ordnung der Anstalt hierdurch nicht gefährdet wird,
6.  besondere Sicherungsmaßnahmen und die Anwendung unmittelbaren Zwangs auf ein Mindestmaß beschränkt,
7.  zur Konfliktregelung und als Reaktion auf Pflichtverstöße pädagogische Gespräche und Maßnahmen sowie die Angebote zur einvernehmlichen Streitbeilegung vorsieht und weitestgehend auf Disziplinierung und Hausstrafen verzichtet,
8.  eng mit Externen zusammenarbeitet, um das Ziel des Arrestes zu erreichen, und eine Weiterführung der für erforderlich erachteten Maßnahmen nach der Entlassung sicherstellt und
9.  Arrestierten aus dem Land Brandenburg zur Erreichung des unter Buchstabe a genannten Ziels Aufenthalte außerhalb der Anstalt gemäß § 14 des Brandenburgischen Jugendarrestvollzugsgesetzes auch in Einrichtungen freier Träger gewährt.

### Artikel 3

(1) Die in der Anstalt tätigen Bediensteten stehen im Dienst des Landes Berlin.
Die Dienstaufsicht führt die für Justiz zuständige Senatsverwaltung.

(2) Die Bestellung der Leiterin oder des Leiters der Anstalt erfolgt durch das Land Berlin im Benehmen mit dem für Justiz zuständigen Ministerium des Landes Brandenburg.

(3) Die für Justiz zuständige Senatsverwaltung des Landes Berlin bestellt im Einvernehmen mit dem für Justiz zuständigen Ministerium des Landes Brandenburg eine Arrestleiterin oder einen Arrestleiter.
Sie oder er trägt die Verantwortung für die sozialpädagogische Ausgestaltung und Organisation des Arrestes, leitet die Bediensteten fachlich an und vertritt für diesen Bereich die Leiterin oder den Leiter der Anstalt.

(4) Die Anstalt untersteht der Fachaufsicht beider Länder.
Sie wird durch die für Justiz zuständige Senatsverwaltung des Landes Berlin im Einvernehmen mit dem für Justiz zuständigen Ministerium des Landes Brandenburg ausgeübt.
Das Nähere regelt eine Verwaltungsvereinbarung.

(5) Die Anstalt erarbeitet eine Konzeption für die Gestaltung des Jugendarrestes, die der Zustimmung der für Justiz zuständigen Senatsverwaltung des Landes Berlin und des für Justiz zuständigen Ministeriums des Landes Brandenburg bedarf.

### Artikel 4

Der Vollzug, insbesondere seine Aufgabenerfüllung und Gestaltung, die Umsetzung seiner Ziele sowie die Maßnahmen zu deren Umsetzung und die Wirkung der Maßnahmen auf die Erreichung des Vollzugsziels, soll regelmäßig durch den kriminologischen Dienst, durch eine Hochschule oder durch eine andere geeignete Stelle wissenschaftlich begleitet und erforscht werden.

### Artikel 5

(1) Das Land Berlin stellt eine für den Betrieb der gemeinsamen Einrichtung geeignete Baulichkeit einschließlich der notwendigen Außenanlagen, der technischen Ausrüstung und der erforderlichen Einrichtungsgegenstände zur Verfügung.
Hierfür angefallene Kosten werden nicht erstattet.

(2) Die Länder Berlin und Brandenburg tragen die Personalausgaben einschließlich der Versorgungsausgaben für Dienstzeiten nach Inkrafttreten des Staatsvertrags sowie die konsumtiven und investiven Ausgaben für den Geschäftsbetrieb der Jugendarrestanstalt gemeinsam, soweit sie nicht durch die Einnahmen gedeckt sind.

(3) Die Verteilung der Ausgaben nach Absatz 2 erfolgt im Verhältnis der Zahl der Arrestplätze, für die die Länder Belegungsrechte haben.
Bei Inkrafttreten des Staatsvertrags stehen dem Land Berlin Belegungsrechte für 50 und dem Land Brandenburg Belegungsrechte für zehn Arrestplätze zu.
Die Verteilung der Ausgaben bei der Inanspruchnahme einer von einem der beiden Länder darüber hinausgehenden Belegung wird durch Verwaltungsvereinbarung geregelt.

### Artikel 6

(1) Der Entwurf des Haushaltsplans einschließlich des Stellenplans für die gemeinsame Einrichtung wird von der für Justiz zuständigen Senatsverwaltung des Landes Berlin im Einvernehmen mit dem für Justiz zuständigen Ministerium des Landes Brandenburg aufgestellt und im Haushaltsplan des Landes Berlin ausgebracht.

(2) Nach Beendigung des Haushaltsjahres stellt das Land Berlin fest, wie hoch der nicht durch Einnahmen gedeckte Betrag der Ausgaben ist und welcher Anteil gemäß der Verteilung nach Artikel 5 Absatz 3 auf das Land Brandenburg entfällt.

(3) Das Land Berlin kann nach Abschluss eines jeden Vierteljahres vom Land Brandenburg Abschlagszahlungen auf den am Ende des Haushaltsjahres zu erwartenden Finanzierungsanteil anfordern.

### Artikel 7

Dieser Staatsvertrag wird auf unbestimmte Zeit geschlossen.
Er kann nur im Einvernehmen beider Länder geändert werden.
Jedes Land kann den Vertrag schriftlich bis zum 31.
Dezember eines Jahres mit Wirkung zum 31.
Dezember des Folgejahres kündigen.

### Artikel 8

Dieser Staatsvertrag bedarf der Ratifikation.
Er tritt am ersten Tag des zweiten Monats nach dem Austausch der Ratifikationsurkunden in Kraft.

Berlin, den 21.
Oktober 2015

Für das Land Berlin  
Der Regierende Bürgermeister  
vertreten durch den Senator für  
Justiz und Verbraucherschutz

Dr.
Thomas Heilmann

Potsdam, den 10.
September 2015

Für das Land Brandenburg  
Der Ministerpräsident  
vertreten durch den Minister der Justiz  
und für Europa und Verbraucherschutz

Dr.
Helmuth Markov

[zum Gesetz](/gesetze/stv_jva_heidering)