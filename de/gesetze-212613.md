## Gesetz zu dem Staatsvertrag zwischen dem Land Berlin und dem Land Brandenburg über die Errichtung und den Betrieb der Justizvollzugsanstalt Heidering

Der Landtag hat das folgende Gesetz beschlossen:

§ 1
---

Dem am 25.
August 2011 unterzeichneten Staatsvertrag zwischen dem Land Berlin und dem Land Brandenburg über die Errichtung und den Betrieb der Justizvollzugsanstalt Heidering wird zugestimmt.
Der Vertrag wird nachstehend veröffentlicht.

§ 2 
----

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel 5 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 19.
Dezember 2011

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Staatsvertrag](/de/vertraege-237642)