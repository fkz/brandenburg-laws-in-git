## Staatsvertrag zwischen dem Land Berlin und dem Land Brandenburg über die Freistellung von ehrenamtlich Engagierten in den Freiwilligen Feuerwehren und im Katastrophenschutz

Das Land Berlin,  
vertreten durch den Regierenden Bürgermeister,

und

das Land Brandenburg,  
vertreten durch den Ministerpräsidenten,

schließen folgenden Staatsvertrag:

### Präambel

Die Länder Berlin und Brandenburg bilden für viele Menschen einen gemeinsamen Lebens- und Arbeitsraum und sie sind Partner für eine länderübergreifende Zusammenarbeit.
Die Länder Berlin und Brandenburg sind deshalb übereingekommen, die landesgrenzüberschreitende Freistellung von der Arbeit unter Fortzahlung des Arbeitsentgelts für ehrenamtlich Engagierte in den Freiwilligen Feuerwehren und im Katastrophenschutz zur Teilnahme an Einsätzen und behördlich angeordneten Übungen durch den nachfolgenden Staatsvertrag zu regeln:

#### Artikel 1  
Anerkennung von Vorschriften

(1) Für Angehörige der Freiwilligen Feuerwehren des Landes Brandenburg und für Mitglieder der im Land Brandenburg im Katastrophenschutz mitwirkenden Hilfsorganisationen, die jeweils einer Erwerbstätigkeit im Land Berlin nachgehen, gilt § 8 Absatz 1 und 2 sowie Absatz 3 Satz 2 des Feuerwehrgesetzes vom 23. September 2003 (GVBl.
Bln S. 457), das zuletzt durch Artikel 1 des Gesetzes vom 9.
Mai 2016 (GVBl.
Bln S.
240) geändert worden ist, entsprechend.

(2) Für Angehörige der Freiwilligen Feuerwehren des Landes Berlin und für freiwillige Helfer im Katastrophenschutzdienst des Landes Berlin, die jeweils einer Erwerbstätigkeit im Land Brandenburg nachgehen, gilt § 27 Absatz 1 Satz 3 und 4 sowie Absatz 2 des Brandenburgischen Brand- und Katastrophenschutzgesetzes vom 24.
Mai 2004 (GVBl.
Bbg I S.
197), das zuletzt durch das Gesetz vom 18.
Juni 2018 (GVBl.
Bbg I Nr. 12) geändert worden ist, entsprechend.

(3) Sofern für Angehörige von Berufsfeuerwehren, von Feuerwehren mit hauptamtlichen Kräften und von Werkfeuerwehren der ehrenamtliche Dienst in den Freiwilligen Feuerwehren und im Katastrophenschutz zulässig ist, finden die Absätze 1 und 2 entsprechende Anwendung.

#### Artikel 2  
Erstattung

Die Erstattung weitergewährten Arbeitsentgelts richtet sich nach den jeweiligen landesrechtlichen Bestimmungen des Landes, in dem die oder der ehrenamtlich Engagierte das Ehrenamt ausübt.

#### Artikel 3  
Kündigung

Dieser Staatsvertrag kann von jedem Land mit einer Frist von einem Jahr zum Ende eines Kalenderjahres schriftlich gekündigt werden.

#### Artikel 4  
Ratifikation, Inkrafttreten

Dieser Staatsvertrag bedarf der Ratifikation.
Er tritt an dem Tag in Kraft, der auf den Austausch der Ratifikationsurkunden folgt.

Für das Land Berlin:  
Der Regierende Bürgermeister     

Berlin, den 04.04.2019 

  
Michael Müller

Für das Land Brandenburg:  
Der Ministerpräsident

Potsdam, den 26.03.2019

  
Dr. Dietmar Woidke

[zum Gesetz](/de/gesetze-247377)