## Gesetz über die Zuständigkeit in Staatsangehörigkeitsangelegenheiten  (StAngZustG)

Der Landtag hat das folgende Gesetz beschlossen:

§ 1  
**Sachliche Zuständigkeit**

Zur Ausführung des Staatsangehörigkeitsgesetzes sowie der staatsangehörigkeitsrechtlichen Bestimmungen in sonstigen Rechtsvorschriften des Bundes nehmen die Landkreise und kreisfreien Städte die Aufgaben der Staatsangehörigkeitsbehörden als Auftragsangelegenheiten wahr.
Sie sind nach Satz 1 im Einzelnen für folgende Aufgaben zuständig:

1.  Entscheidungen über
    1.  die Einbürgerung ausländischer Personen, einschließlich früherer deutscher Staatsangehöriger, denen zwischen dem 30.
        Januar 1933 und dem 8.
        Mai 1945 die Staatsangehörigkeit rassistisch oder aus politischen oder religiösen Gründen entzogen worden ist, und ihrer Abkömmlinge sowie heimatloser ausländischer Personen und seit der Geburt staatenloser Personen,
    2.  die Entlassung aus der deutschen Staatsangehörigkeit,
    3.  die Genehmigung eines Verzichts auf die deutsche Staatsangehörigkeit,
    4.  die Erteilung einer Genehmigung zur Beibehaltung der deutschen Staatsangehörigkeit,
    5.  das Bestehen oder Nichtbestehen der deutschen Staatsangehörigkeit und
    6.  die Rechtsstellung als Deutsche oder Deutscher ohne deutsche Staatsangehörigkeit;
2.  Entgegennahme von Erklärungen zur
    1.  deutschen Staatsangehörigkeit und
    2.  Beibehaltung einer ausländischen Staatsangehörigkeit;
3.  Zustellung einer Information an erklärungspflichtige Personen über ihre Obliegenheit zur Abgabe einer in Nummer 2 genannten Erklärung und über insoweit gesetzlich bestimmte mögliche Rechtsfolgen;
4.  Feststellung des Fortbestands oder des Verlustes der deutschen Staatsangehörigkeit erklärungspflichtiger Personen;
5.  Ausstellung von
    1.  Staatsangehörigkeitsausweisen,
    2.  Ausweisen über die Rechtsstellung als Deutsche oder Deutscher ohne deutsche Staatsangehörigkeit und
    3.  sonstigen die deutsche Staatsangehörigkeit einer Person betreffenden Bescheinigungen;
6.  Übermittlung oder Weitergabe von Daten zu Entscheidungen oder Rechtsfolgen nach den Nummern 1, 2, 4 und 5 an das Bundesverwaltungsamt (Registerbehörde) sowie an die Melde- und Ausländerbehörden;
7.  rteilung von Auskünften an das Amt für Statistik Berlin-Brandenburg für Erhebungen über Einbürgerungen, die als Bundesstatistik durchgeführt werden.
    

§ 2  
**Aufsichtsbehörde**

Aufsichtsbehörde ist das für Staatsangehörigkeitsangelegenheiten zuständige Ministerium.
Sind Staatsangehörigkeitsangelegenheiten mehreren Geschäftsbereichen zugeordnet, ist das Ministerium zuständig, in dessen Geschäftsbereich die jeweilige Angelegenheit fällt.

§ 3  
**Mehrbelastungsausgleich**

(1) Die Landkreise und kreisfreien Städte erhalten zum finanziellen Ausgleich ihrer Mehrbelastung durch die in § 1 bestimmten Aufgaben

1.  für jede abschließende Entscheidung nach § 1 Satz 2 Nummer 1 Buchstabe a oder Buchstabe d sowie für jede Feststellung nach § 1 Satz 2 Nummer 4, auch wenn eine Entscheidung nach § 1 Satz 2 Nummer 1 Buchstabe d zu treffen war, einen Pauschalbetrag in Höhe von 250 Euro;
2.  für die einmalige Beratung einer Person über die allgemeinen Voraussetzungen einer Einbürgerung, wenn die Einbürgerung nicht innerhalb von zwölf Monaten nach der Beratung beantragt wird, einen Pauschalbetrag in Höhe von 50 Euro;
3.  für jede Entscheidung nach § 1 Satz 2 Nummer 1 Buchstabe b oder Buchstabe c einen Pauschalbetrag in Höhe von 50 Euro;
4.  für jede Entscheidung von Amts wegen nach § 1 Satz 2 Nummer 1 Buchstabe e oder Buchstabe f einen Pauschalbetrag in Höhe von 150 Euro und, wenn eine solche Entscheidung auf Antrag zu treffen war, für jede Ausstellung eines Ausweises nach § 1 Satz 2 Nummer 5 Buchstabe a oder Buchstabe b einen Pauschalbetrag in Höhe von 100 Euro;
5.  für jede Ausstellung einer Bescheinigung nach § 1 Satz 2 Nummer 5 Buchstabe c einen Pauschalbetrag in Höhe von 25 Euro.
    

Die Pauschalbeträge nach Satz 1 Nummer 1, 3, 4 und 5 berücksichtigen den Verwaltungsaufwand für die Ausführung der in § 1 Satz 2 Nummer 2, 3, 6 und 7 bestimmten Aufgaben.
Im Übrigen sind die Kosten der Landkreise und kreisfreien Städte für die Wahrnehmung der Aufgaben nach § 1 durch die Gebühren gedeckt, die in Staatsangehörigkeitsangelegenheiten zu erheben sind.

(2) Zur Berechnung der Beträge des pauschalen Mehrbelastungsausgleichs nach Absatz 1 legen die Landkreise und kreisfreien Städten der Aufsichtsbehörde für jedes Haushaltshaltsjahr eine überprüfbare geschäftsstatistische Aufstellung der Amtshandlungen vor.
Die Aufsichtsbehörde zahlt die Beträge binnen drei Monaten nach Vorlage der Aufstellung aus.
§ 2 Satz 2 gilt entsprechend.

(3) Die Aufsichtsbehörde überprüft und bewertet die Angemessenheit der Höhe der in Absatz 1 bestimmten Pauschalbeträge im Einvernehmen mit dem für Finanzen zuständigen Ministerium regelmäßig nach Ablauf von fünf Haushaltsjahren.

§ 4  
**Übergangsregelung**

Für Anträge, die vor dem 1.
Januar 2014 gestellt sind, bleiben die bis zum Ablauf des 31.
Dezember 2013 zuständigen Behörden zuständig.
Satz 1 gilt nicht für außerhalb des Geltungsbereichs dieses Gesetzes gestellte Anträge, die erst nach dem 31.
Dezember 2013 an Behörden im Geltungsbereich dieses Gesetzes abgegeben werden.
Die Sätze 1 und 2 gelten für Erklärungen, die vor dem 1.
 Januar 2014 entgegengenommen wurden, entsprechend.
Soweit die Zuständigkeit für eine Staatsangehörigkeitsangelegenheit vor dem 1.
Januar 2014 nicht durch Rechtsvorschrift bestimmt war, richtet sie sich von diesem Zeitpunkt an auch dann nach § 1, wenn die Angelegenheit bis dahin von einer anderen als der danach zuständigen Behörde wahrgenommen wurde.

§ 5  
**Inkrafttreten, Außerkrafttreten**

Dieses Gesetz tritt am 1.
Januar 2014 in Kraft.
Gleichzeitig treten die Verordnung zur Ausführung des Reichs- und Staatsangehörigkeitsgesetzes vom 28.
November 1991 (GVBl.
S.
524) und die Verordnung über die Zuständigkeit in Staatsangehörigkeitssachen vom 12.
März 1992 (GVBl.
II S.
82), die zuletzt durch die Verordnung vom 23.
November 2004 (GVBl.
II S.
890) geändert worden ist, außer Kraft.

Potsdam, den 10.
September 2013

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch