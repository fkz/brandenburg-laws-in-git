## Verordnung über die beamtenrechtlichen Zuständigkeiten im Geschäftsbereich des Ministeriums für Wirtschaft und Europaangelegenheiten (Beamtenzuständigkeitsverordnung MWE - BZVMWE)

Auf Grund des § 9 Absatz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186) in Verbindung mit

*   § 4 Absatz 1 Satz 3 des Landesbeamtengesetzes vom 3.
    April 2009 (GVBl.
    I S. 26) in Verbindung mit § 1 Absatz 3 Satz 1 der Ernennungsverordnung vom 1.
    August 2004 (GVBl.
    II S. 742),
*   § 32 Absatz 1 Satz 1, § 38 Satz 2, § 54 Absatz 1 Satz 1, § 56 Absatz 1 Satz 5, § 57 Absatz 1 Satz 2, § 66 Absatz 4, § 69 Absatz 5 Satz 1, § 84 Satz 2, § 85 Absatz 2 Satz 1, § 86 Absatz 1 Satz 3, § 87 Satz 4, § 88 Satz 5, § 89 Satz 3 und  § 92 Absatz 2 des Landesbeamtengesetzes,
*   § 4 Absatz 4, § 22 Absatz 4 Satz 1 und § 39 Absatz 1 der Laufbahnverordnung vom 16.
    September 2009 (GVBl.
    II S. 622),
*   § 12 Absatz 2 Satz 3, § 15 Absatz 2 Satz 2, § 66 Absatz 1 des Bundesbesoldungsgesetzes in der am 31.
    August 2006 geltenden Fassung,
*   § 64 Absatz 1 des Landesbeamtengesetzes, der durch das Gesetz vom 11.
    März 2010 (GVBl.
    I Nr. 13) neu gefasst worden ist, in Verbindung mit § 8 Absatz 1 der Verordnung über die Gewährung von Jubiläumszuwendungen an Beamte und Richter des Bundes in der Fassung der Bekanntmachung vom 13.
    März 1990 (BGBl.
    I S. 487) und
*   § 71 des Landesbeamtengesetzes in Verbindung mit § 1 Absatz 1 Satz 3 der Mutterschutz- und Elternzeitverordnung vom 12.
     Februar 2009 (BGBl.
    I S. 320)

verordnet der Minister für Wirtschaft und Europaangelegenheiten:

### § 1  
Dienstvorgesetzter

(1) Der für die beamtenrechtlichen Entscheidungen über die persönlichen Angelegenheiten der ihm nachgeordneten Beamten aller Laufbahngruppen zuständige Dienstvorgesetzte im Landesamt für Bergbau, Geologie und Rohstoffe Brandenburg ist der Präsident des Landesamtes für Bergbau, Geologie und Rohstoffe Brandenburg.

(2) Der für die beamtenrechtlichen Entscheidungen über die persönlichen Angelegenheiten der ihm nachgeordneten brandenburgischen Beamten der Laufbahngruppen des mittleren und gehobenen Dienstes zuständige Dienstvorgesetzte im Landesamt für Mess- und Eichwesen Berlin-Brandenburg ist der Direktor des Landesamtes für Mess- und Eichwesen Berlin-Brandenburg.

### § 2  
Übertragung der Ernennungsbefugnis

(1) Dem Landesamt für Bergbau, Geologie und Rohstoffe des Landes Brandenburg wird die Befugnis zur Ernennung der Landesbeamten in seinem Geschäftsbereich, die nicht gemäß § 1 Absatz 1 Satz 1 der Ernennungsverordnung durch die Landesregierung ernannt werden, übertragen.

(2) Dem Landesamt für Mess- und Eichwesen Berlin-Brandenburg wird die Befugnis zur Ernennung der Landesbeamten der Laufbahn des mittleren und des gehobenen Dienstes in seinem Geschäftsbereich übertragen.

### § 3  
Übertragung weiterer Befugnisse

Den in § 1 Absatz 1 und 2 genannten Dienststellen werden für die Beamten, für die ihnen die Befugnis zur Ernennung übertragen ist, folgende weitere Befugnisse übertragen:

1.  Entscheidungen zur Entlassung von Beamten kraft Gesetzes gemäß § 32 Absatz 1 Satz 1 des Landesbeamtengesetzes,
2.  Entscheidungen über die Versetzung in den Ruhestand von Beamten, die sich im Beamtenverhältnis auf Probe befinden, gemäß § 38 Satz 1 des Landesbeamtengesetzes,
3.  Entscheidungen über das Verbot der Führung der Dienstgeschäfte gemäß § 54 Absatz 1 des Landesbeamtengesetzes,
4.  Entscheidungen über die Versagung der Aussagegenehmigung gemäß § 56 Absatz 1 Satz 3 des Landesbeamtengesetzes,
5.  Entscheidungen über die Ausnahmen vom Verbot der Annahme von Belohnungen, Geschenken und sonstigen Vorteilen gemäß § 57 Absatz 1 des Landesbeamtengesetzes,
6.  Entscheidungen über die Gewährung von Ersatz von Sachschäden sowie die Geltendmachung von Schadenersatzansprüchen des Landes gemäß § 66 des Landesbeamtengesetzes,
7.  Entscheidungen über die Führung der Amtsbezeichnung gemäß § 69 Absatz 5 des Landesbeamtengesetzes,
8.  Entscheidungen über die Verpflichtung zur Ausübung einer Nebentätigkeit im öffentlichen Dienst gemäß § 84 Satz 1 des Landesbeamtengesetzes,
9.  Entscheidungen über die Auskunftspflichten bei Nebentätigkeiten gemäß § 85 Absatz 2 Satz 1 und § 88 Satz 2 und 3 des Landesbeamtengesetzes,
10.  Entscheidungen über das Verbot der Übernahme einer Nebentätigkeit gemäß § 86 Absatz 1 Satz 1 und 2 des Landesbeamtengesetzes,
11.  Entscheidungen über die Ausnahmen zur Ausübung einer Nebentätigkeit während der Arbeitszeit gemäß § 87 Satz 1 des Landesbeamtengesetzes,
12.  Entscheidungen über die Inanspruchnahme von Einrichtungen, Personal oder Material des Dienstherrn zur Ausübung einer Nebentätigkeit gemäß § 89 Satz 2 des Landesbeamtengesetzes,
13.  Entscheidungen über das Verbot einer Erwerbstätigkeit nach Beendigung des Beamtenverhältnisses gemäß § 92 Absatz 2 des Landesbeamtengesetzes,
14.  Entscheidungen über Art und Umfang der Ausschreibung und ihre Bekanntmachung gemäß § 4 Absatz 4 der Laufbahnverordnung,
15.  Entscheidungen über die Zulassung zur Einführung in die nächsthöhere Laufbahn gemäß § 22 Absatz 4 Satz 1 der Laufbahnverordnung,
16.  die Befugnis zur Entscheidung über den Erwerb der Laufbahnbefähigung bei Laufbahnen des mittleren und des gehobenen Dienstes der besonderen Fachrichtung gemäß § 39 Absatz 1 der Laufbahnverordnung,
17.  Entscheidungen über die Zustimmung zum Absehen von der Rückforderung von Bezügen gemäß § 12 Absatz 2 Satz 3 des Bundesbesoldungsgesetzes,
18.  Entscheidungen über die Anweisung des dienstlichen Wohnsitzes gemäß § 15 Absatz 2 Satz 1 des Bundesbesoldungsgesetzes,
19.  Entscheidungen über die Kürzung der Anwärterbezüge gemäß § 66 Absatz 1 Satz 1 des Bundesbesoldungsgesetzes,
20.  Entscheidungen über die Ausnahmen vom Verbot der Mehrarbeit, Nacht- und Sonntagsarbeit werdender und stillender Mütter gemäß § 1 Absatz 1 Satz 3 der Mutterschutz- und Elternzeitverordnung,
21.  Entscheidungen über die Gewährung und Versagung der Jubiläumszuwendung gemäß § 8 Absatz 1 der Verordnung über die Gewährung von Jubiläumszuwendungen an Beamte und Richter des Bundes.

### § 4  
Übergangsvorschriften

Soweit vor Inkrafttreten dieser Verordnung andere als die in dieser Verordnung festgelegten Zuständigkeiten bestanden haben, verbleibt es für die zum Zeitpunkt des Inkrafttretens dieser Verordnung anhängigen Verwaltungsverfahren bei den bisherigen Zuständigkeiten.

### § 5  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Beamtenzuständigkeitsverordnung MW vom 16.
September 2005 (GVBl.
II S. 494) außer Kraft.

Potsdam, den 21.
Juni 2012

Der Minister für Wirtschaft  
und Europaangelegenheiten

Ralf Christoffers