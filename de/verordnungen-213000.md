## Verordnung über Zuständigkeiten auf dem Gebiet des Mess- und Eichwesens (Mess- und Eichzuständigkeitsverordnung - MessEichZV)

Auf Grund des § 9 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
 I S.
186) und des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl.
I S.
602) verordnet die Landesregierung:

§ 1  
Zuständigkeiten
---------------------

Das Landesamt für Mess- und Eichwesen Berlin-Brandenburg ist zuständige Behörde für

1.  die Durchführung des Einheiten- und Zeitgesetzes in der Fassung der Bekanntmachung vom 22.
    Februar 1985 (BGBl.
    I S.
    408), das zuletzt durch Artikel 4 Absatz 68 des Gesetzes vom 7.
    August 2013 (BGBl.
    I S.
    3154, 3200) geändert worden ist, in der jeweils geltenden Fassung,
    
2.  die Durchführung des Mess- und Eichgesetzes vom 25.
    Juli 2013 (BGBl.
    I S.
    2722, 2723) in der jeweils geltenden Fassung und
    
3.  die Durchführung der auf Grund dieser Gesetze erlassenen Rechtsverordnungen,
    

soweit sich nicht aus diesen Gesetzen oder Rechtsverordnungen etwas anderes ergibt.

§ 2  
Ordnungswidrigkeiten
==========================

Zuständige Behörde für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 10 des Einheiten- und Zeitgesetzes und § 60 des Mess- und Eichgesetzes ist das Landesamt für Mess- und Eichwesen Berlin-Brandenburg.

§ 3  
Inkrafttreten, Außerkrafttreten
-------------------------------------

Diese Verordnung tritt am 1.
Januar 2015 in Kraft.
Gleichzeitig tritt die Eichzuständigkeitsverordnung vom 29.
Juni 1992 (GVBl.
II S.
342) außer Kraft.

Potsdam, den 5.
August 2014

Die Landesregierung des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister für Wirtschaft und Europaangelegenheiten

Ralf Christoffers