## Verordnung über das Naturschutzgebiet „Sergen-Kathlower Teich- und Wiesenlandschaft“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2 und § 78 Absatz 1 Satz 3 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1  
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Spree-Neiße wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Sergen-Kathlower Teich- und Wiesenlandschaft“.

### § 2   
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 679 Hektar.
Es umfasst fünf Teilflächen in folgenden Fluren:

**Gemeinde:**

**Gemarkung:**

**Flur:**

Neuhausen/Spree

Kathlow

1, 3, 4, 5;

Neuhausen/Spree

Sergen

1, 3;

Neuhausen/Spree

Roggosen

1;

Neuhausen/Spree

Komptendorf

1;

Neuhausen/Spree

Gablenz

1;

Wiesengrund

Trebendorf

1;

Wiesengrund

Jethe

6.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummer 01 bis 05 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 01 bis 14 aufgeführten Liegenschaftskarten.
 Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 3 beigefügt.

(3) Innerhalb des Naturschutzgebietes werden gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes eine Zone 1 mit rund 63 Hektar und eine Zone 2 mit rund 129 Hektar mit unterschiedlichen Beschränkungen der Nutzung festgesetzt.

Die Zone 1 umfasst Flächen in folgenden Fluren:

**Gemeinde:**

**Gemarkung:**

**Flur:**

Neuhausen/Spree

Kathlow

1, 3, 4;

Neuhausen/Spree

Sergen

1, 3;

Neuhausen/Spree

Roggosen

1;

Neuhausen/Spree

Komptendorf

1;

Neuhausen/Spree

Gablenz

1;

Wiesengrund

Jethe

6.

Die Zone 2 umfasst Flächen in folgenden Fluren:

**Gemeinde:**

**Gemarkung:**

**Flur:**

Neuhausen/Spree

Kathlow

1, 3, 4;

Neuhausen/Spree

Sergen

1;

Neuhausen/Spree

Roggosen

1.

Die Grenzen der Zonen 1 und 2 sind in den in Anlage 2 Nummer 1 genannten topografischen Karten mit den Blattnummern 01 bis 04 sowie in den in Anlage 2 Nummer 2 genannten Liegenschaftskarten mit den Blattnummern 01 bis 14 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Spree-Neiße, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

### § 3  
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung, Entwicklung und Wiederherstellung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der ausgedehnten Röhrichtbestände, Schwimmblattgesellschaften, reichen Feuchtwiesen und Sandtrockenrasen, der Erlenbruchwälder und naturnaher Laub- und Nadelwälder;
2.  die Entwicklung des Flechten-Kiefernwaldes auf exponierten Dünenstandorten;
3.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Weiße Seerose (Nymphaea alba), Sumpf-Schwertlilie (Iris pseudacorus), Heide-Nelke (Dianthus deltoides) und Niederlausitzer Tieflandfichte (Picea abies);
4.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbeson­dere der Wasservögel, Limikolen und Greifvögel sowie der Fledermäuse, Amphibien und Reptilien, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundes-naturschutzgesetzes besonders und streng geschützte Arten, insbesondere Kranich (Grus grus), Eisvogel(Alcedo atthis), Zwergdommel (Ixobrychus minutus), Rothalstaucher (Podiceps grisegena), Schwarzstorch(Ciconia nigra), Schwarzmilan (Milvus nigrans), Rotmilan (Milvus milvus), Seeadler (Haliaetus albicilla), Fischadler (Pandion haliaetus), Kreuzkröte (Bufo calamita), Wechselkröte (Bufo viridis), Laubfrosch (Hyla arborea), Knoblauchkröte (Pelobates fuscus), Ringelnatter (Natrix natrix), Zauneidechse (Lacerta agilis);
5.  die Erhaltung und Entwicklung der Gewässer als Rast-, Schlaf- und Überwinterungshabitate für Kraniche, Schwäne, Gänse, Taucher und Enten;
6.  die Erhaltung des Gebietes wegen der besonderen Eigenart als charakteristische Wiesenlandschaft mit Gehölzgruppen, Teichen, natürlichen Erlen-Eichenwäldern, Stieleichenbeständen und naturnahen Kiefern-Birkenwäldern;
7.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des landesweiten Biotopverbundes.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Sergen-Kathlower Teich- und Wiesenlandschaft“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) mit seinen Vorkommen von

1.  Dünen mit offenen Grasflächen mit Corynephorus und Agrostis (Dünen im Binnenland), Oligo- bis mesotrophen stehenden Gewässern mit Vegetation der Isoeto-Nanojuncetea, Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Flüssen der planaren bis montanen Stufe mit Vegeta-tion des Ranunculion fluitantis und des Callitricho-Batrachion, Feuchten Hochstaudenfluren der planaren und montanen bis alpinen Stufe, Mageren Flachlandmähwiesen, Subatlantischem oder mitteleuropäischem Stiel-eichenwald oder Hainbuchenwald (Carpinion betuli) (Stellario-Carpinetum), Alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur und Montanen bis alpinen bodensauren Fichtenwäldern (Vaccinio-Piceetea) als Biotope von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhang I der Richtlinie 92/43/EWG),
2.  Auen-Wäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion, Alnion incanae, Salicion albae) als prioritärer Biotop („prioritärer Lebensraumtyp“ im Sinne des Anhang I der Richtlinie 92/43/EWG),
3.  Fischotter (Lutra lutra), Rotbauchunke (Bombina bombina) und Grüner Keiljungfer (Ophiogomphus cecilia) als Tierarten von gemeinschaftlichem Interesse (im Sinne des Anhang II der Richtlinie 92/43/EWG) einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet, seinen Naturhaushalt oder einzelne seiner Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten zum Zweck der Erholung sowie des nicht gewerblichen Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 12 jeweils nach dem 31.
    Juli eines jeden Jahres;
10.  in der Zone 2 zu reiten und im Übrigen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb von Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, zu reiten;
11.  mit Fahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
12.  zu baden oder zu tauchen;
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (wie zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland umzubrechen oder neu anzusäen.

### § 5  
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Anforderungen und Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  Gehölze in geeigneter Weise gegen Verbiss und sonstige Beschädigungen sowie Ränder von Gewässern wirksam gegen Trittschäden von weidenden Nutztieren geschützt werden,
        
    2.  Grünland in der Zone 1 als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertende Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und Sekundärrohstoffdünger, wie zum Beispiel solche aus Abwasser und Bioabfällen einzusetzen; § 4 Absatz 2 Nummer 23 und 24 gilt;  
        
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  nur Arten der potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
    2.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    3.  auf den Flächen der in § 3 Absatz 2 genannten Waldlebensraumtypen und sonstiger naturnaher Wälder eine naturnahe Waldentwicklung mit einer Anhebung des Totholzanteils erfolgt; bis zu fünf Stück je Hektar lebensraumtypische, abgestorbene, stehende Bäume (Totholz) mit einem Brusthöhendurchmesser von mindestens 30 Zentimetern ohne Rinde in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden und liegendes Totholz (mindestens zwei Stück je Hektar mit einem Durchmesser von 65 Zentimetern am stärksten Ende) im Bestand verbleibt,
    4.  eine Nutzung der in § 3 Absatz 2 genannten Waldlebensraumtypen und der Erlenbruchwälder sowie die Nutzung innerhalb der Zone 2 einzelstamm- bis truppweise durchgeführt wird.
         In den übrigen Wäldern und Forsten sind Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von 0,5 Hektar zulässig,
        
    5.  in der Zone 2 in der Zeit vom 1.
        Januar bis zum 31.
        Juli eines jeden Jahres keine forstwirtschaftlichen Maßnahmen erfolgen;
    6.  § 4 Absatz 2 Nummer 17 und 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters weitgehend ausgeschlossen ist,
    2.  § 4 Absatz 2 Nummer 19 gilt;
4.  die Teichbewirtschaftung, die den Anforderungen des § 5 Absatz 4 des Bundesnaturschutzgesetzes in Verbindung mit dem Fischereigesetz des Landes Brandenburg entspricht und die im Sinne der guten fachlichen Praxis gemäß den Leitlinien zur naturschutzgerechten Teichwirtschaft in Brandenburg vom 16.
    März 2011 auf den bisher rechtmäßig dafür genutzten Flächen durchgeführt wird, mit der Maßgabe, dass Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters weitgehend ausgeschlossen ist;
5.  die Genehmigung von Maßnahmen zur Vergrämung und Tötung von Kormoranen im Bereich der fischereilich genutzten Teiche durch die zuständige Naturschutzbehörde, sofern hierfür die erforderliche artenschutzrechtliche Ausnahmegenehmigung oder Befreiung vorliegt.
     Die Genehmigung kann mit Auflagen versehen werden; sie ist zu erteilen, wenn der Schutzzweck von der Maßnahme nicht wesentlich beeinträchtigt wird;
6.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass die Angelfischerei nur an den in der topografischen Karte gemäß § 2 Absatz 2 dargestellten Steganlagen und Angelstellen am Autobahnsee in der Gemarkung Roggosen, Flur 1, Flurstück 391/1 ausgeübt wird.
    Der Weg für die Zufahrt und der Bereich für das Abstellen von Fahrzeugen sind ebenfalls in der topografischen Karte gemäß § 2 Absatz 2 dargestellt;
7.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Jagd auf Wasservögel in der Teichgruppe Sergen und am Johannesteich vom 1.
        Oktober eines jeden Jahres bis zum 15.
        Januar des Folgejahres sowie am Schlossteich und in der Teichgruppe Kathlow vom 15.
        November eines jeden Jahres bis zum 15.
        Januar des Folgejahres zulässig ist.
        Werden Teiche von Gänsen oder Kranichen als Schlafplatz genutzt, bleibt die Wasservogeljagd unzulässig,
        
        bb)
        
        die Fallenjagd ausschließlich mit Lebendfallen erfolgt,
        
        cc)
        
        in der Zone 2 in der Zeit vom 1.
        Januar bis zum 31.
        Juli eines jeden Jahres die Jagdausübung unzulässig ist, bei Wildschäden bleibt eine Bejagung ohne die Anlage von Kirrungen zulässig,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd außerhalb der Zone 2 mit Zustimmung der unteren Naturschutzbehörde,
    3.  die Errichtung transportabler und mobiler Ansitzeinrichtungen,
    4.  die Anlage von Kirrungen außerhalb der nach § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 32 des Brandenburgischen Naturschutzgesetzes geschützten Biotope.Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern sind unzulässig, die Unterhaltung der in der topografischen Karte gemäß § 2 Absatz 2 gekennzeichneten Wildäcker bleibt zulässig; im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
8.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter die Nummer 10 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
9.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
10.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
11.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
12.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch jeweils nach dem 31.
    Juli eines jeden Jahres;
13.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
14.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde gebilligt oder angeordnet worden sind;
15.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 an Straßen und Wegen freigestellt;
16.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
 Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist**.
**Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

### § 6  
Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgaben benannt:

1.  die Nutzung des Grünlandes soll als Mähwiese erfolgen;
2.  durch geeignete Maßnahmen der Wasserrückhaltung sollen hohe Grundwasserstände wiederhergestellt und ein maximaler Wasserrückhalt im Kathlower Bruch und im Sergener Luch gesichert werden, dabei werden oberflächennahe Wasserstände mit Blänkenbildung bis zum 30.
    April eines jeden Jahres angestrebt;
3.  in den ausgebauten Abschnitten der Fließgewässer (Tranitz/Mühlenfließ und Grenzfließ) sollen naturnahe Verhältnisse wiederhergestellt und der Meliorationsgraben (Nummer 184) westlich des Kathlower Großteiches soll für einen besseren Wasserrückhalt umgestaltet werden;
4.  die Wasserversorgung der Teichflächen soll gesichert werden;
5.  das Mähen der Gräben soll nur bei Notwendigkeit und einseitig erfolgen;
6.  für die Bewirtschaftung der Teiche:
    
    1.  der Großteich Kathlow soll aus naturschutzfachlichen Gründen mit einer geringen Besatzdichte (Abfischmenge von maximal 200 Kilogramm/Hektar) bewirtschaftet werden; wenn bei ungünstigen Wasserverhältnissen keine Fischproduktion erfolgt, soll der Großteich (entsprechend dem Wasserdargebot) mindestens von Januar bis Ende August angestaut werden;
    2.  in der Teichgruppe Sergen soll jährlich an mindestens einem Teich und am Schlossteich im mindestens 2- bis 3-jährigen Turnus eine Bewirtschaftung mit Amphibien förderndem Besatz zur Förderung der Rotbauchunke erfolgen,
    3.  für die Teiche soll ein Bewirtschaftungsplan erstellt werden, der folgende Mindestangaben enthält: Besatz nach Arten und Altersklassen, Bespannungszeiträume, Düngung, Teichpflege- und Sanierungsmaßnahmen jeweils nach Art, Umfang und Zeitpunkt;
7.  Heiden, Trockenrasen und Dünen sollen durch Pflegemaßnahmen offen gehalten werden;
8.  die Walderneuerung auf Flächen der in § 3 Absatz 2 genannten Lebensraumtypen soll vorrangig durch Naturverjüngung erfolgen;
9.  innerhalb der Waldflächen sollen mindestens fünf Stück Altbäume je Hektar mit einem Brusthöhendurchmesser von mindestens 30 Zentimetern ohne Rinde in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden.
    Als Altbäume gelten über 80 Jahre alte Nadelbäume sowie über 120 Jahre alte Laubbäume;
10.  bei der Freihaltung der Hochspannungsleitungen soll das anfallende Gehölz-Schnittgut von der Fläche beräumt werden;
11.  die Ackerflächen sollen extensiv, ohne den Einsatz chemisch-synthetischer Dünger, genutzt werden; an den Schlagrändern sollen Schon- und Blühstreifen angelegt werden;
12.  in den Wald- und Forstflächen außerhalb der Zone 2 sollen in der Zeit vom 1.
    März bis 31.
    Juli eines jeden Jahres keine Holzfällarbeiten durchgeführt werden.

### § 7  
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

### § 8  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

### § 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere die §§ 31 bis 33 und § 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

### § 10  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

### § 11   
Inkrafttreten, Außerkrafttreten

(1) § 5 Absatz 1 Nummer 1 Buchstabe b tritt am 1.
Juli 2013 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig treten außer Kraft:

1.  Anordnung Nummer 1 über Naturschutzgebiete vom 30.
    März 1961 (GBl.
    II Nr. 27 S. 166), zuletzt geändert durch Anordnung Nummer 4 vom 28.
    November 1983 (GBl.
    II Nr. 38 S. 431), als Brandenburgisches Landesrecht für fortgeltend erklärt durch das Gesetz zur Bereinigung des zu Landesrecht gewordenen Rechts der ehemaligen Deutschen Demokratischen Republik vom 3.
    September 1997 (GVBl.
    I S. 104), Anordnung über das Naturschutzgebiet „Sergener Luch“ vom 4.
    Mai 1961 (GBl.
    II Nr. 27 S. 166);
2.  Beschluss Nummer 75/81 des Bezirkstages Cottbus vom 25.
    Mai 1981 über die Bestätigung von Naturschutzgebieten im Bezirk Cottbus, soweit er sich auf das Gebiet „Sergener Luch“ bezieht.

Potsdam, den 12.
Februar 2013

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

### Anlage 1  
(zu § 2 Absatz 1)

![Das Naturschutzgebiet Sergen-Kathlower Teich- und Wiesenlandschaft hat eine Größe von rund 679 Hektar und liegt im Landkreis Spree-Neiße in Bereich der Gemeinden Neuhausen/Spree und Wiesengrund. Es umfasst Teile der Gemarkungen Kathlow, Sergen, Roggosen, Komptendorf, Gablenz, Trebendorf und Jethe.](/br2/sixcms/media.php/69/image002.jpg "Das Naturschutzgebiet Sergen-Kathlower Teich- und Wiesenlandschaft hat eine Größe von rund 679 Hektar und liegt im Landkreis Spree-Neiße in Bereich der Gemeinden Neuhausen/Spree und Wiesengrund. Es umfasst Teile der Gemarkungen Kathlow, Sergen, Roggosen, Komptendorf, Gablenz, Trebendorf und Jethe.")

### Anlage 2  
(zu § 2 Absatz 2)

#### 1.
Topografische Karten im Maßstab 1 : 10 000

**Titel:** Topografische Karte zur Verordnung über das Naturschutzgebiet „Sergen-Kathlower Teich- und Wiesenlandschaft“

Blattnummer

Unterzeichnung

01

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV), am 23.
Oktober 2012

02

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

03

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

04

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

05

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

#### 2.
Liegenschaftskarten im Maßstab 1 : 2 500

**Titel:** Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Sergen-Kathlower Teich- und Wiesenlandschaft“

Blattnummer

Gemarkung

Flur

Unterzeichnung

01

Kathlow

4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

02

Kathlow

4, 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

03

Kathlow

4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

04

Kathlow

3, 4, 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

05

Kathlow  
  
Roggosen

3, 4  
  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

06

Kathlow  
  
Roggosen

1, 3, 4  
  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

07

Roggosen

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

08

Kathlow  
  
Roggosen  
  
Sergen

1  
  
1  
  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

09

Kathlow  
  
Sergen

1  
  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

10

Komptendorf  
  
Sergen

1  
  
1, 3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

11

Komptendorf

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

12

Komptendorf  
  
Sergen

1  
  
3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

13

Gablenz  
  
Sergen

1  
  
3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

14

Gablenz  
  
Jehte  
  
Sergen  
  
Trebendorf

1  
  
6  
  
3  
  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 23.
Oktober 2012

### Anlage 3  
(zu § 2 Absatz 2)

#### Flurstücksliste zur Verordnung über das Naturschutzgebiet „Sergen-Kathlower Teich- und Wiesenlandschaft“

**Landkreis:** Spree-Neiße

Gemeinde

Gemarkung

Flur

Flurstücke

Neuhausen/ Spree

Kathlow

1

105 anteilig, 106/1, 201, 204, 205/1, 205/2, 206 bis 216, 217/1, 217/2, 218 bis 224, 225/1, 225/2, 227 bis 228, 229 anteilig, 239, 243 bis 245, 247, 258 anteilig;

Neuhausen/ Spree

Kathlow

3

34 bis 52, 53 anteilig, 54 bis 57, 69 bis 86, 88 bis 90, 91 anteilig, 92/1, 92/2, 93, 94 anteilig, 96, 97 anteilig, 98 anteilig, 109 bis 111, 137 anteilig, 138 anteilig, 140 anteilig, 141/4 anteilig, 142 anteilig, 143 anteilig, 149 anteilig, 150, 152 anteilig, 154 bis 161 anteilig, 243 bis 245, 246 anteilig, 247, 257 anteilig, 258, 266 bis 268;

Neuhausen/ Spree

Kathlow

4

12/4, 15/5, 16, 17/6, 18 bis 24, 25 anteilig, 26, 27, 28/8, 28/10, 28/12, 29/5, 31, 32, 34, 35, 37 anteilig, 38, 40 anteilig, 43, 45 anteilig, 46 anteilig, 47 anteilig, 49/2 anteilig, 51, 55/5, 56/5, 60/4, 70 bis 124, 125/4, 133, 134 anteilig, 146, 147, 152 bis 154, 157, 158, 162 bis 164, 167, 170, 173, 176, 179, 182, 183, 186;

Neuhausen/ Spree

Kathlow

5

16/22, 17/12 bis 17/14, 19/8, 19/9, 19/10, 20, 26, 117, 131, 132 anteilig;

Neuhausen/ Spree

Sergen

1

67 anteilig, 68 anteilig, 69, 70, 72 bis 99, 110 bis 114, 144, 145, 149 bis 153, 162 anteilig, 164/4, 165, 168 anteilig, 171 anteilig, 172/4 anteilig, 172/6 anteilig, 413 bis 422, 423 anteilig, 424 bis 428, 430 anteilig, 431, 432/1 bis 432/4, 436 bis 444, 446, 477 anteilig, 583 bis 604, 606 bis 608, 623 anteilig, 638, 640, 642 anteilig, 644, 645, 647 anteilig, 650 bis 655;

Neuhausen/ Spree

Sergen

3

57 anteilig, 65, 69, 175, 177, 178 anteilig, 227, 248, 249 anteilig, 250 anteilig, 252 bis 255, 259, 264 anteilig, 279 anteilig, 280, 281, 283, 285, 287, 289, 291, 293, 339 anteilig, 363 anteilig;

Neuhausen/ Spree

Roggosen

1

390, 391/1, 391/2, 392 bis 432, 710;

Neuhausen/ Spree

Komptendorf

1

33 bis 37, 141 bis 147, 157 bis 162, 165 anteilig, 167 anteilig, 171/1, 172/ 3, 173, 174/1 anteilig, 430, 431, 442, 443, 455 anteilig, 468 anteilig;

Neuhausen/ Spree

Gablenz

1

33/48, 33/50, 33/51 anteilig, 33/52 bis 33/62, 33/64 bis 33/74, 33/76 bis 33/79, 34/2, 56, 57;

Wiesengrund

Trebendorf

1

242, 315, 316, 318 anteilig, 344 anteilig, 346 anteilig, 347 anteilig, 348 bis 351, 352 anteilig, 368 anteilig;

Wiesengrund

Jethe

6

138 anteilig, 153 anteilig.

#### Flächen der Zone 1:

**Landkreis:** Spree-Neiße

Gemeinde

Gemarkung

Flur

Flurstücke

Neuhausen/Spree

Kathlow

1

224, 225/1, 225/2, 227, 228 anteilig, 229 anteilig, 247;

Neuhausen/Spree

Kathlow

3

34 bis 50 anteilig, 51, 52, 54 anteilig;

Neuhausen/Spree

Kathlow

4

111 bis 114 anteilig;

Neuhausen/Spree

Sergen

1

68 anteilig, 70 anteilig, 72 anteilig, 73 anteilig, 74 bis 99, 110 bis 114, 164/4, 165 anteilig, 583, 585, 587, 589, 591, 593, 595, 597, 599, 601, 603, 638;

Neuhausen/Spree

Sergen

3

65, 175, 177 anteilig, 178 anteilig, 227, 248, 249, 250 anteilig, 252 bis 254, 259, 264 anteilig, 280, 281, 283, 339 anteilig, 363 anteilig;

Neuhausen/Spree

Komptendorf

1

167 anteilig, 171/1 anteilig, 430, 431 anteilig, 442, 443;

Neuhausen/Spree

Gablenz

1

33/48, 33/51 anteilig, 33/50 anteilig, 34/2, 56, 57;

Wiesengrund

Jethe

6

153 anteilig.

#### Flächen der Zone 2:

**Landkreis:** Spree-Neiße

Gemeinde

Gemarkung

Flur

Flurstücke

Neuhausen/Spree

Kathlow

1

222 anteilig, 229 anteilig;

Neuhausen/Spree

Kathlow

3

88 anteilig, 91 anteilig, 92/1, 92/2, 93 anteilig, 94 anteilig, 243 anteilig, 246 anteilig;

Neuhausen/Spree

Kathlow

4

20 bis 23, 70 anteilig, 71, 72 anteilig, 73 anteilig, 75 bis 88, 89 anteilig, 116 anteilig, 117 bis 119, 120 bis 123 anteilig, 124;

Neuhausen/Spree

Sergen

1

441 bis 444, 446, 477 anteilig, 650, 651, 652;

Neuhausen/Spree

Roggosen

1

391/1 anteilig, 392 bis 410.