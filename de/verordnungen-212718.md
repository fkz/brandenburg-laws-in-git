## Verordnung zur Festsetzung des Wasserschutzgebietes Nauen 

Auf Grund des § 51 Absatz 1 Satz 1 Nummer 1 und Satz 2 und Absatz 2 und des § 52 Absatz 1 des Wasserhaushaltsgesetzes vom 31.
Juli 2009 (BGBl.
I S. 2585) in Verbindung mit § 15 Absatz 1 Satz 1 und Absatz 2 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1  
Allgemeines

(1) Zur Sicherung der öffentlichen Wasserversorgung wird zum Schutz des Grundwassers im Einzugsgebiet des Wasserwerkes Nauen das in § 2 näher umschriebene Wasserschutzgebiet festgesetzt.
Begünstigter ist der Wasser- und Abwasserverband Havelland.

(2) Das Wasserschutzgebiet gliedert sich in den Fassungsbereich (Zone I), in die engere Schutzzone (Zone II) und in die weitere Schutzzone (Zone III).
Die Zone III unterteilt sich in die Zone III A und die Zone III B.

### § 2  
Räumlicher Geltungsbereich

(1) Lage und Größe des Wasserschutzgebietes und der Schutzzonen ergeben sich aus der Übersichtskarte in der Anlage 2 und den in Absatz 2 genannten Karten.

(2) Die Schutzzonen sind in der topografischen Karte im Maßstab 1 : 10 000 in der Anlage 3 und außerdem in der Liegenschaftskarte im Maßstab 1 : 2 500 in der Anlage 4, die aus elf Blättern besteht, dargestellt.
Für die Abgrenzung der Schutzzonen ist die Darstellung in der Liegenschaftskarte maßgebend.

(3) Zu Informationszwecken werden zusätzlich auf Papier ausgefertigte Exemplare der in Absatz 2 genannten Karten bei der unteren Wasserbehörde des Landkreises Havelland und der Stadt Nauen hinterlegt und können dort während der Dienststunden von jedermann kostenlos eingesehen werden.
Diese Karten sind mit Datum vom 1.
Dezember 2012 und mit dem Dienstsiegel des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (Siegelnummer 20) versehen.
Eine weitere so gesiegelte Ausfertigung der Karten befindet sich im Ministerium für Umwelt, Gesundheit und Verbraucherschutz.

(4) Veränderungen der Topografie sowie von Flurstücksgrenzen oder -bezeichnungen berühren den räumlichen Geltungsbereich der Schutzzonen nicht.

### § 3  
Schutz der Zone III B

In der Zone III B sind verboten:

1.  das Düngen mit Gülle, Jauche, Geflügelkot, Festmist, Silagesickersaft oder sonstigen Düngemitteln mit im Sinne des § 2 Nummer 10 der Düngeverordnung wesentlichen Nährstoffgehalten an Stickstoff oder Phosphat,
    1.  wenn die Düngung nicht im Sinne des § 3 Absatz 4 der Düngeverordnung in zeit- und bedarfsgerechten Gaben erfolgt,
    2.  wenn keine jährlichen schlagbezogenen Aufzeichnungen über die Zu- und Abfuhr von Stickstoff und Phosphat erstellt werden,
    3.  auf abgeerntetem Ackerland, wenn nicht im gleichen Jahr Folgekulturen einschließlich Zwischenfrüchte angebaut werden,
    4.  auf Dauergrünland und auf Ackerland vom 15.
        Oktober bis 15.
        Februar, ausgenommen das Düngen mit Festmist ohne Geflügelkot,
    5.  auf Brachland oder stillgelegten Flächen oder
    6.  auf wassergesättigten, gefrorenen oder schneebedeckten Böden,
2.  das Lagern oder Ausbringen von Fäkalschlamm oder Klärschlämmen aller Art einschließlich in Biogasanlagen behandelte Klärschlämme,
3.  das Errichten, Erweitern oder Betreiben von befestigten Dunglagerstätten, ausgenommen mit dichtem Jauchebehälter, der über eine Leckageerkennungseinrichtung verfügt,
4.  das Errichten von Erdbecken zur Lagerung von Gülle, Jauche oder Silagesickersäften,
5.  das Errichten, Erweitern oder Betreiben von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle, ausgenommen Anlagen mit Leckageerkennungseinrichtung und Sammeleinrichtung, wenn der Wasserbehörde
    1.  vor Inbetriebnahme,
    2.  bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung sowie
    3.  wiederkehrend alle fünf Jahreein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit der Sammeleinrichtung vorgelegt wird,
6.  unbefestigte Feldrandzwischenlager für organische oder mineralische Dünger, ausgenommen für Kalk und Kaliumdünger,
7.  das Errichten, Erweitern oder Betreiben von ortsfesten Anlagen für die Silierung von Pflanzen oder die Lagerung von Silage, ausgenommen
    1.  Anlagen mit Silagesickersaft-Sammelbehälter, der über eine Leckageerkennungseinrichtung verfügt und
    2.  Anlagen mit Ableitung in Jauche- oder Güllebehälter,wenn der Wasserbehörde vor Inbetriebnahme, bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung, sowie wiederkehrend alle fünf Jahre ein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit der Behälter und Leitungen vorgelegt wird,
8.  die Silierung von Pflanzen oder Lagerung von Silage außerhalb ortsfester Anlagen, ausgenommen Ballensilage im Wickelverfahren,
9.  das Errichten oder Erweitern von Stallungen für Tierbestände für mehr als 50 Großvieheinheiten gemäß Anlage 1 Nummer 1,
10.  die Anwendung von Pflanzenschutzmitteln,
    1.  wenn die Pflanzenschutzmittel nicht für Wasserschutzgebiete zugelassen sind,
    2.  wenn keine flächenbezogenen Aufzeichnungen nach dem Pflanzenschutzgesetz über den Einsatz auf erwerbsgärtnerisch, land- oder forstwirtschaftlich genutzten Flächen geführt werden,
    3.  in einem Abstand von weniger als 10 Metern zu oberirdischen Gewässern,
    4.  zur Bodenentseuchung oder
    5.  auf Dauergrünland und Grünlandbrachen,
11.  die Beregnung landwirtschaftlich oder erwerbsgärtnerisch genutzter Flächen, wenn die Beregnungshöhe 20 Millimeter pro Tag oder 60 Millimeter pro Woche überschreitet,
12.  der Umbruch von Dauergrünland oder von Grünlandbrachen,
13.  das Anlegen von Schwarzbrache im Sinne der Anlage 1 Nummer 3,
14.  Erstaufforstungen mit Nadelbaumarten oder Robinien,
15.  Aufschlüsse der Erdoberfläche im Sinne des § 49 Absatz 1 des Wasserhaushaltsgesetzes, selbst wenn das Grundwasser nicht aufgedeckt wird, wie zum Beispiel das Errichten oder Erweitern von Kies-, Sand- oder Tongruben, Übertagebergbauen oder Torfstichen, wenn die Schutzfunktion der Deckschichten hierdurch wesentlich gemindert wird, ausgenommen das Errichten von Kleingewässern bis 100 Quadratmeter,
16.  das Errichten oder Erweitern von vertikalen Anlagen zur Gewinnung von Erdwärme,
17.  das Errichten oder Erweitern von Rohrleitungsanlagen für wassergefährdende Stoffe, ausgenommen Rohrleitungsanlagen im Sinne des § 62 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes,
18.  das Errichten von Anlagen zur behälterlosen Lagerung oder Ablagerung von Stoffen im Untergrund,
19.  das Behandeln, Lagern oder Ablagern von Abfall, tierischen Nebenprodukten oder bergbaulichen Rückständen, ausgenommen
    1.  die vorübergehende Lagerung in dichten Behältern,
    2.  die ordnungsgemäße kurzzeitige Zwischenlagerung von vor Ort angefallenem Abfall zur Abholung durch den Entsorgungspflichtigen und
    3.  die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen,
20.  das Ein- oder Aufbringen von Abfällen, bergbaulichen Rückständen oder Ersatzbaustoffen in oder auf Böden oder deren Einbau in bodennahe technische Bauwerke,
21.  das Errichten von Anlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden radioaktiver Stoffe im Sinne des Atomgesetzes, ausgenommen für medizinische Anwendungen sowie für Mess-, Prüf- und Regeltechnik,
22.  das Errichten von Industrieanlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden wassergefährdender Stoffe in großem Umfang, wie zum Beispiel in Raffinerien, Metallhütten oder chemischen Fabriken,
23.  das Errichten von Kraftwerken oder Heizwerken, die der Genehmigungspflicht nach Bundesimmissionsschutzrecht unterliegen, ausgenommen mit Gas, Sonnenenergie oder Windkraft betriebene Anlagen,
24.  das Errichten oder Erweitern von Biogasanlagen, ausgenommen für im Wasserschutzgebiet liegende Betriebsstandorte, die Wirtschaftsdünger und Biomasse im Regelbetrieb aus eigenem Aufkommen des Betriebes verwerten,
25.  das Errichten oder Erweitern von Abwasserbehandlungsanlagen, ausgenommen
    1.  die Sanierung bestehender Abwasserbehandlungsanlagen zugunsten des Gewässerschutzes sowie
    2.  Abwasservorbehandlungsanlagen, wie Fett-, Leichtflüssigkeits- oder Amalgamabscheider,
26.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik eingehalten werden,
27.  das Errichten oder Erweitern von Abwassersammelgruben, ausgenommen
    1.  Anlagen mit allgemeiner bauaufsichtlicher Zulassung des Deutschen Institutes für Bautechnik und
    2.  monolithische Sammelgruben aus Beton, die den allgemein anerkannten Regeln der Technik entsprechen,
28.  das Betreiben oder Unterhalten von Abwassersammelgruben, wenn der Wasserbehörde nicht
    
    1.  vor Inbetriebnahme,
    2.  bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung sowie
    3.  wiederkehrend alle fünf Jahre
    
    ein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit vorgelegt wird,
    
29.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten, ausgenommen Anlagen mit dichtem Behälter,
30.  das Einleiten von Abwasser – mit Ausnahme von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 Nummer 4 – in oberirdische Gewässer, sofern die Einleitung nicht schon zum Zeitpunkt des Inkrafttretens dieser Verordnung wasserrechtlich zugelassen war,
31.  das Ausbringen von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes,
32.  das Einleiten oder Versickern von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 **oder des § 54 Absatz 1 Satz 2** des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser,
33.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen das großflächige Versickern von Niederschlagswasser über die belebte Bodenzone,
34.  das Anwenden von Auftaumitteln auf Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen auf der Bundesstraße 5 sowie bei Extremwetterlagen wie Eisregen,
35.  das Errichten oder Erweitern von Straßen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik für bautechnische Maßnahmen an Straßen in Wasserschutzgebieten eingehalten werden,
36.  das Errichten von Rangier- oder Güterbahnhöfen,
37.  das Verwenden wassergefährdender, auslaug- oder auswaschbarer Materialien (zum Beispiel Schlacke, Bauschutt, Teer, Imprägniermittel) zum Wege- oder Wasserbau,
38.  das Einrichten oder Betreiben von öffentlichen Freibädern oder Zeltplätzen sowie Camping aller Art, ausgenommen
    1.  Einrichtungen mit ordnungsgemäßer Abfall- und Abwasserentsorgung und
    2.  das Zelten von Fuß-, Rad-, Reit- und Wasserwanderern abseits von Zelt- und Campingplätzen für eine Nacht,
39.  das Einrichten oder Betreiben von Sportanlagen, ausgenommen Anlagen mit ordnungsgemäßer Abfall- und Abwasserentsorgung,
40.  das Errichten oder Erweitern von Motorsportanlagen,
41.  das Errichten von Schießständen oder Schießplätzen für Feuerwaffen, ausgenommen Schießstände in geschlossenen Räumen,
42.  das Errichten von Golfanlagen,
43.  das Errichten von Flugplätzen im Sinne des § 6 Absatz 1 Satz 1 des Luftverkehrsgesetzes,
44.  das Starten oder Landen motorgetriebener Luftfahrzeuge, mit Ausnahme in Fällen des § 25 Absatz 2 des Luftverkehrsgesetzes,
45.  das Errichten von militärischen Anlagen, Standort- oder Truppenübungsplätzen,
46.  das Durchführen von militärischen Übungen, ausgenommen das Durchfahren auf klassifizierten Straßen,
47.  Bergbau einschließlich die Erkundung oder Gewinnung von Erdöl oder Erdgas,
48.  das Durchführen von Sprengungen, sofern die Gefahr besteht, dass dabei das Grundwasser aufgedeckt wird,
49.  die Ausweisung neuer Industriegebiete.

### § 4  
Schutz der Zone III A

Die Verbote der Zone III B gelten auch in der Zone III A.
In der Zone III A sind außerdem verboten:

1.  das Errichten, Erweitern oder Betreiben von Tiefbehältern zur Lagerung von Gülle, Jauche oder Silagesickersäften,
2.  das Errichten oder Erweitern von Stallungen für Tierbestände, ausgenommen für Kleintierhaltung zur Eigenversorgung,
3.  die Freilandtierhaltung im Sinne der Anlage 1 Nummer 2, wenn die Ernährung der Tiere nicht im Wesentlichen aus der genutzten Weidefläche erfolgt oder wenn die Grasnarbe flächig verletzt wird, ausgenommen Kleintierhaltung für die Eigenversorgung,
4.  das Errichten oder Erweitern von Gartenbaubetrieben oder Kleingartenanlagen, ausgenommen Gartenbaubetriebe, die in geschlossenen Systemen produzieren,
5.  die Neuanlage oder Erweiterung von Baumschulen oder forstlichen Pflanzgärten, Weihnachtsbaumkulturen sowie von gewerblichem Wein-, Hopfen-, Gemüse-, Obst- oder Zierpflanzenanbau, ausgenommen Gemüse- sowie Zierpflanzenanbau unter Glas in geschlossenen Systemen und Containerproduktion von Baumschulprodukten auf versiegelten Flächen,
6.  Erdaufschlüsse im Sinne des § 49 Absatz 1 des Wasserhaushaltsgesetzes, selbst wenn Grundwasser nicht aufgedeckt wird, ausgenommen das Verlegen von Ver- und Entsorgungsleitungen und die Herstellung von Baugruben und Bohrungen,
7.  das Errichten, Erweitern oder Erneuern von Tiefenbohrungen über 100 Meter, Grundwassermessstellen oder Brunnen, ausgenommen das Erneuern von Brunnen für Entnahmen mit wasserrechtlicher Erlaubnis oder Bewilligung,
8.  das Errichten oder Erweitern von Anlagen zum Umgang mit wassergefährdenden Stoffen, ausgenommen doppelwandige Anlagen mit Leckanzeigegerät und ausgenommen Anlagen, die mit einem Auffangraum ausgerüstet sind, der das maximal in der Anlage vorhandene Volumen wassergefährdender Stoffe aufnehmen kann, und soweit
    1.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 1 das für die Anlage maßgebende Volumen von 1 000 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 1 die für die Anlage maßgebende Masse von 1 000 Tonnen,
    2.  in oberirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 2 das für die Anlage maßgebende Volumen von 100 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 2 die für die Anlage maßgebende Masse von 100 Tonnen,
    3.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 2 das für die Anlage maßgebende Volumen von 10 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen derWassergefährdungsklasse 2 die für die Anlage maßgebende Masse von 10 Tonnen,
    4.  in oberirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 3 das für die Anlage maßgebende Volumen von 10 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 3 die für die Anlage maßgebende Masse von 10 Tonnen,
    5.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 3 das für die Anlage maßgebende Volumen von 1 Kubikmeter beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 3 die für die Anlage maßgebende Masse von 1 Tonnenicht überschritten wird,
9.  das Errichten von Regen- oder Mischwasserentlastungsbauwerken,
10.  das Errichten von Bahnhöfen oder Schienenwegen der Eisenbahn,
11.  das Abhalten oder Durchführen von Märkten, Volksfesten oder Großveranstaltungen außerhalb der dafür vorgesehenen Anlagen,
12.  das Durchführen von Motorsportveranstaltungen, ausgenommen das Durchfahren auf klassifizierten Straßen,
13.  Bestattungen, ausgenommen innerhalb bereits bei Inkrafttreten dieser Verordnung bestehender Friedhöfe,
14.  die Darstellung von neuen Bauflächen oder Baugebieten im Rahmen der vorbereitenden Bauleitplanung, wenn darin eine Neubebauung bisher unbebauter Gebiete vorgesehen wird,
15.  die Festsetzung von neuen Baugebieten im Rahmen der verbindlichen Bauleitplanung, wenn dies zu einer Erhöhung der zulässigen Grundfläche im Sinne des § 19 Absatz 2 der Baunutzungsverordnung führt, ausgenommen
    1.  Gebiete, die im zum Zeitpunkt des Inkrafttretens dieser Verordnung gültigen Flächennutzungsplan als Bauflächen oder Baugebiete dargestellt sind, und
    2.  die Überplanung von Bestandsgebieten, wenn dies zu keiner wesentlichen Erhöhung der zulässigen Grundfläche im Sinne des § 19 Absatz 2 der Baunutzungsverordnung führt.

### § 5  
Schutz der Zone II

Die Verbote der Zonen III B und III A gelten auch in der Zone II.
In der Zone II sind außerdem verboten:

1.  das Düngen mit Gülle, Jauche oder Festmist oder sonstigen organischen Düngern sowie die Anwendung von Silagesickersaft,
2.  das Errichten von Dunglagerstätten,
3.  das Errichten von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle,
4.  die Silierung von Pflanzen oder Lagerung von Silage,
5.  die Freilandtierhaltung im Sinne der Anlage 1 Nummer 2,
6.  die Beweidung,
7.  die Anwendung von Pflanzenschutzmitteln,
8.  die Beregnung landwirtschaftlich oder erwerbsgärtnerisch genutzter Flächen, ausgenommen die Bewässerung von Hausgärten,
9.  das Errichten, Erweitern oder Erneuern von Dränungen oder Entwässerungsgräben,
10.  der Einsatz von forstwirtschaftlichen Kraftfahrzeugen abseits von Straßen, Wegen oder forstwirtschaftlichen Rückegassen,
11.  das Vergraben, Lagern oder Ablagern von Tierkörpern oder Teilen davon,
12.  das Errichten oder Betreiben von Wildfütterungen, Kirrungen oder Luderplätzen,
13.  das Errichten von Anlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden wassergefährdender Stoffe,
14.  der Einsatz von mineralischen Schmierstoffen zur Verlustschmierung oder mineralischen Schalölen,
15.  das Lagern, Abfüllen oder Umschlagen wassergefährdender Stoffe, ausgenommen haushaltsübliche Kleinstmengen,
16.  das Befahren mit Fahrzeugen mit wassergefährdender Ladung,
17.  das Errichten von Transformatoren oder Stromleitungen mit flüssigen wassergefährdenden Kühl- oder Isoliermitteln,
18.  das Behandeln, Lagern oder Ablagern von Abfall, tierischen Nebenprodukten oder bergbaulichen Rückständen, ausgenommen
    1.  die ordnungsgemäße kurzzeitige Bereitstellung von in der Zone II angefallenem Abfall zum Abtransport durch den Entsorgungspflichtigen und
    2.  die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen,
19.  der Umgang mit radioaktiven Stoffen im Sinne des Atomgesetzes,
20.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, ausgenommen Anlagen, die zur Entsorgung vorhandener Bebauung dienen und wenn hierbei die allgemein anerkannten Regeln der Technik eingehalten werden,
21.  das Errichten von Abwassersammelgruben,
22.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten,
23.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen das großflächige Versickern von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 Nummer 4 über die belebte Bodenzone,
24.  das Errichten oder Erweitern von Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen
    1.  Baumaßnahmen an vorhandenen Straßen zur Anpassung an den Stand der Technik,
    2.  Baumaßnahmen an vorhandenen Straßen zur Verbesserung der Verkehrssicherheit unter Einhaltung der allgemein anerkannten Regeln der Technik für bautechnische Maßnahmen an Straßen in Wasserschutzgebieten sowie
    3.  Wege mit großflächiger Versickerung der Niederschlagswasserabflüsse über die belebte Bodenzone,
25.  das Errichten von öffentlichen Freibädern oder Zeltplätzen sowie Camping aller Art,
26.  das Errichten von Sportanlagen,
27.  das Abhalten oder Durchführen von Sportveranstaltungen, Märkten, Volksfesten oder Großveranstaltungen,
28.  das Errichten oder Erweitern von Baustelleneinrichtungen oder Baustofflagern,
29.  das Durchführen von Bohrungen, ausgenommen Maßnahmen zur Abwehr von Gefahren für das Grundwasser unter Beachtung der Sicherheitsvorkehrungen zum Grundwasserschutz,
30.  das Durchführen von unterirdischen Sprengungen,
31.  das Errichten oder Erweitern von baulichen Anlagen, ausgenommen Veränderungen in Gebäuden und Instandhaltungsmaßnahmen.

### § 6  
Schutz der Zone I

Die Verbote der Zonen III B, III A und II gelten auch in der Zone I.
In der Zone I sind außerdem verboten:

1.  das Betreten oder Befahren,
2.  land-, forst- oder gartenbauliche Nutzung,
3.  Veränderungen oder Aufschlüsse der Erdoberfläche.

### § 7  
Maßnahmen zur Wassergewinnung

Die Verbote des § 3 Nummer 32, des § 4 Nummer 7, des § 5 Nummer 13, 19, 28 bis 31 sowie des § 6 Nummer 1 und 3 gelten nicht für Maßnahmen zur Wassergewinnung, die durch diese Verordnung geschützt ist.

### § 8  
Widerruf von Befreiungen

(1) Befreiungen nach § 52 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes sind widerruflich und bedürfen der Schriftform.
Abweichend von Satz 1 ist eine Befreiung von den Verboten gemäß § 4 Nummer 14 und 15 nicht widerruflich.

(2) Im Fall des Widerrufs einer Befreiung kann die untere Wasserbehörde vom Grundstückseigentümer verlangen, dass der frühere Zustand wiederhergestellt wird, sofern es das Wohl der Allgemeinheit, insbesondere der Schutz der Wasserversorgung, erfordert.

### § 9  
Sicherung und Kennzeichnung des Wasserschutzgebietes

(1) Die Zone I ist vom Begünstigten auf Anordnung der unteren Wasserbehörde gegen unbefugtes Betreten, zum Beispiel durch eine Umzäunung, zu sichern.

(2) Der Begünstigte hat auf Anordnung der Wasserbehörde zur Kennzeichnung des Wasserschutzgebietes im Bereich öffentlicher Verkehrsflächen bei der Straßenverkehrsbehörde die Anordnung des Verkehrszeichens 354 zu beantragen und im Bereich nichtöffentlicher Flächen in Abstimmung mit der Gemeinde nichtamtliche Hinweiszeichen aufzustellen.

### § 10  
Duldungspflichten

(1) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet haben die Überwachung des Wasserschutzgebietes, insbesondere hinsichtlich der Beachtung dieser Verordnung und der nach ihr getroffenen Anordnungen, sowie das Beobachten der Gewässer und des Bodens durch die zuständigen Wasserbehörden, den Begünstigten oder deren Beauftragte zu dulden.

(2) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet sind auf Anordnung der unteren Wasserbehörde verpflichtet,

1.  das Errichten und Unterhalten von Einrichtungen zur Sicherung der Zone I gegen unbefugtes Betreten,
2.  das Aufstellen, Unterhalten und Beseitigen von Hinweis-, Warn-, Gebots- und Verbotszeichen,
3.  das Betreten der Grundstücke durch Bedienstete der zuständigen Behörden, den Begünstigten oder deren Beauftragte zum Beobachten, Messen und Untersuchen des Grundwassers und zur Entnahme von Boden- und Vegetationsproben sowie
4.  das Anlegen und Betreiben von Grundwassermessstellen

zu dulden.
Die Anordnung erfolgt durch schriftlichen Bescheid gegenüber den betroffenen Eigentümern oder Nutzungsberechtigten.
Soweit bergrechtliche Belange berührt sind, ergeht die Entscheidung im Benehmen mit der zuständigen Bergbehörde.

### § 11  
Übergangsregelung

Für bei Inkrafttreten dieser Verordnung errichtete und betriebene Anlagen gilt das Verbot des Betreibens gemäß § 3 Nummer 3, 5 und 7 nach zwei Jahren nach Inkrafttreten dieser Verordnung.

### § 12  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 20.
Dezember 2011 in Kraft.
Gleichzeitig treten die Verordnung zur Festsetzung des Wasserschutzgebietes Nauen vom 1.
Dezember 2011 (GVBl.
II Nr. 81) und das mit Beschluss Nummer 0057/76 vom 22.
Juli 1976 des Kreistages Nauen festgesetzte Trinkwasserschutzgebiet Nauen außer Kraft.

Potsdam, den 11.
Januar 2013

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

### Anlagen

1

[WSGNauen-Anlg-1](/br2/sixcms/media.php/68/GVBl_II_08_2013-Anlage%201.pdf "WSGNauen-Anlg-1") 117.3 KB

2

[WSGNauen-Anlg-2](/br2/sixcms/media.php/68/GVBl_II_08_2013-Anlage%202.pdf "WSGNauen-Anlg-2") 661.1 KB

3

[WSGNauen-Anlg-3](/br2/sixcms/media.php/68/GVBl_II_08_2013-Anlage%203.pdf "WSGNauen-Anlg-3") 3.0 MB

4

[WSGNauen-Anlg-4-Blatt-01](/br2/sixcms/media.php/68/GVBl_II_08_2013-Anlage%204-Blatt-01.pdf "WSGNauen-Anlg-4-Blatt-01") 2.1 MB

5

[WSGNauen-Anlg-4-Blatt-02](/br2/sixcms/media.php/68/GVBl_II_08_2013-Anlage%204-Blatt-02.pdf "WSGNauen-Anlg-4-Blatt-02") 1.8 MB

6

[WSGNauen-Anlg-4-Blatt-03](/br2/sixcms/media.php/68/GVBl_II_08_2013-Anlage%204-Blatt-03.pdf "WSGNauen-Anlg-4-Blatt-03") 2.0 MB

7

[WSGNauen-Anlg-4-Blatt-04](/br2/sixcms/media.php/68/GVBl_II_08_2013-Anlage%204-Blatt-04.pdf "WSGNauen-Anlg-4-Blatt-04") 1.6 MB

8

[WSGNauen-Anlg-4-Blatt-05](/br2/sixcms/media.php/68/GVBl_II_08_2013-Anlage%204-Blatt-05.pdf "WSGNauen-Anlg-4-Blatt-05") 1.7 MB

9

[WSGNauen-Anlg-4-Blatt-06](/br2/sixcms/media.php/68/GVBl_II_08_2013-Anlage%204-Blatt-06.pdf "WSGNauen-Anlg-4-Blatt-06") 1.8 MB

10

[WSGNauen-Anlg-4-Blatt-07](/br2/sixcms/media.php/68/GVBl_II_08_2013-Anlage%204-Blatt-07.pdf "WSGNauen-Anlg-4-Blatt-07") 1.6 MB

11

[WSGNauen-Anlg-4-Blatt-08](/br2/sixcms/media.php/68/GVBl_II_08_2013-Anlage%204-Blatt-08.pdf "WSGNauen-Anlg-4-Blatt-08") 1.4 MB

12

[WSGNauen-Anlg-4-Blatt-09](/br2/sixcms/media.php/68/GVBl_II_08_2013-Anlage%204-Blatt-09.pdf "WSGNauen-Anlg-4-Blatt-09") 1.7 MB

13

[WSGNauen-Anlg-4-Blatt-10](/br2/sixcms/media.php/68/GVBl_II_08_2013-Anlage%204-Blatt-10.pdf "WSGNauen-Anlg-4-Blatt-10") 1.7 MB

14

[WSGNauen-Anlg-4-Blatt-11](/br2/sixcms/media.php/68/GVBl_II_08_2013-Anlage%204-Blatt-11.pdf "WSGNauen-Anlg-4-Blatt-11") 1.5 MB