## Verordnung über das Landschaftsschutzgebiet „Notte-Niederung“

Auf Grund des § 22 Absatz 1 und 2 und des § 26 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2, § 22 Absatz 2, § 29 Absatz 3 sowie § 78 Absatz 1 Satz 3 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1  
Erklärung zum Schutzgebiet
--------------------------------

Die in § 2 näher bezeichnete Fläche in den Landkreisen Dahme-Spreewald und Teltow-Fläming wird als Landschaftsschutzgebiet festgesetzt.
Das Landschaftsschutzgebiet trägt die Bezeichnung „Notte-Niederung“.

§ 2  
Schutzgegenstand
----------------------

(1) Das Landschaftsschutzgebiet hat eine Größe von rund 18 013 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Landkreis:**

**Stadt/Gemeinde:**

**Gemarkung:**

**Flur:**

Dahme-Spreewald

Bestensee

Bestensee

1, 2, 7, 8, 9, 14, 15;

 

Königs Wusterhausen

Deutsch Wusterhausen

1 bis 3;

 

 

Zeesen

8;

 

Mittenwalde

Brusendorf

1, 3, 4;

 

 

Gallun

1 bis 5;

 

 

Mittenwalde

1 bis 15;

 

 

Motzen

1 bis 7;

 

 

Ragow

1 bis 5, 7;

 

 

Schenkendorf

1 bis 4;

 

 

Telz

1 bis 8;

 

 

Töpchin

2, 4 bis 6;

 

Teupitz

Egsdorf

1 bis 3;

 

 

Teupitz

1;

 

Groß Köris

Groß Köris

 

Teltow-Fläming

Am Mellensee

Klausdorf

3, 5;

 

 

Mellensee

1 bis 4;

 

 

Saalow

3;

 

Blankenfelde-Mahlow

Dahlewitz

1, 4, 5;

 

 

Jühnsdorf

1 bis 6;

 

Ludwigsfelde

Genshagen

1;

 

 

Groß Schulzendorf

1 bis 4, 6, 7;

 

 

Kerzendorf

1;

 

 

Löwenbruch

1 bis 4;

 

 

Wietstock

2, 3;

 

Rangsdorf

Groß Machnow

1 bis 4;

 

 

Klein Kienitz

1, 2;

 

 

Rangsdorf

1 bis 3, 6, 7, 21;

 

Zossen

Dabendorf

1 bis 3, 7, 8;

 

 

Glienick

3, 5;

 

 

Horstfelde

1, 2;

 

 

Kallinchen

1 bis 3, 6;

 

 

Nächst Neuendorf

1;

 

 

Schöneiche

1;

 

 

Wünsdorf

1 bis 3, 5, 7, 8;

 

 

Zehrensdorf

9;

 

 

Zesch am See

1, 2;

 

 

Zossen

1 bis 14.

 

 

 

 

Eine Kartenskizze zur Orientierung über die Lage des Landschaftsschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Landschaftsschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte Übersichtskarte im Maßstab 1 : 50 000 dient der räumlichen Einordnung des Landschaftsschutzgebietes.
Die in Anlage 2 Nummer 2 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 20 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 3 mit den Blattnummern 1 bis 113 aufgeführten Liegenschaftskarten.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie bei den Landkreisen Dahme-Spreewald und Teltow-Fläming, untere Naturschutzbehörden, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

§ 3  
Schutzzweck
-----------------

Schutzzweck des Landschaftsschutzgebietes ist

1.  die Erhaltung, Entwicklung oder Wiederherstellung der Leistungsfähigkeit des Naturhaushaltes, insbesondere
    1.  der Lebensraumfunktionen der landschaftstypischen, abwechslungsreichen und teilweise gefährdeten Ufer- und Feuchtwiesengesellschaften, Wärme liebenden Staudenfluren und Eichenwaldgesellschaften, Sandtrockenrasen sowie Offenlandbereichen, die in einem kleinflächigen Mosaik von Feldgehölzen und Säumen durchzogen sind,
    2.  der Funktionsfähigkeit der Böden durch Sicherung und Förderung der natürlichen Vielfalt der Bodeneigenschaften, den Schutz des Bodens vor Überbauung, Verdichtung, Erosion und Abbau,
    3.  der Qualität der Gewässer,
    4.  der klimatischen Ausgleichsfunktionen beispielsweise als Frischluftentstehungsgebiet für den Ballungsraum Berlin,
    5.  der Lebensräume teilweise gefährdeter Vogelarten, die auch als Brut- und Überwinterungsgebiet von Bedeutung sind,
    6.  der aquatischen Lebensräume gefährdeter Säugetiere und Amphibien,
    7.  des regional übergreifenden Biotopverbundes;
2.  die Erhaltung oder Wiederherstellung der Regenerationsfähigkeit und nachhaltigen Nutzungsfähigkeit der Naturgüter, insbesondere
    1.  des weitgehend unbeeinträchtigten Wasserhaushaltes als Voraussetzung für die Grundwasserneubildung mit teilweise hohen Grundwasserständen in den Niederungsgebieten und als Grundlage für die Ausbildung seltener, feuchtigkeitsgeprägter Standorte,
    2.  der Seen und Fließgewässer, Röhrichtbereiche, Verlandungsbereiche, Erlenbrüche, Niedermoore, Frisch- und Feuchtwiesen, Dünenbereiche und Wälder;
3.  die Erhaltung, Entwicklung und Wiederherstellung der Vielfalt, Eigenart und Schönheit dieses für Mittelbrandenburg charakteristischen Landschaftsbildes
    1.  eines vorwiegend eiszeitlich gebildeten Landschaftsbereichs mit einem Mosaik aus gewässerreichen, zum großen Teil moorreichen Niederungen, Grundmoränenplatten und Endmoränenerhebungen sowie Sandern und einzelnen Dünen,
    2.  der historisch geprägten, vielseitig strukturierten Kulturlandschaft mit ihrem typischen Wechsel von Äckern, Wiesen, Weiden und sonstigem Offenland, Wäldern, Gehölzgruppen und -reihen und Einzelbäumen sowie stehenden Gewässern und Fließgewässern,
    3.  mit seiner weiträumigen Siedlungsstruktur mit charakteristischen Dorfanlagen, Gehöften und Alleen und gewachsenen Dorfrändern mit Obstwiesen;
4.  die Erhaltung oder Entwicklung des Gebietes wegen seiner besonderen Bedeutung  für die naturnahe Erholung in Nähe der Ballungsräume Potsdam und Berlin, insbesondere auf Grund seiner landschaftlichen Vielgestaltigkeit und Strukturiertheit mit einem hohen Anteil an Gewässerflächen, auf Grund seiner kulturhistorischen Besonderheiten sowie seines reizvollen Landschaftsbildes und der Möglichkeiten für ein vielfältiges Landschaftserleben;
5.  die Entwicklung des Gebietes im Hinblick auf eine naturverträgliche, nachhaltige Landnutzung.

§ 4  
Verbote, Genehmigungsvorbehalte
-------------------------------------

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Landschaftsschutzgebiet gemäß § 22 Absatz 3 des Brandenburgischen Naturschutzgesetzes folgende Handlungen verboten:

1.  Bodenbestandteile abzubauen;
2.  Niedermoorstandorte umzubrechen oder in anderer Weise zu beeinträchtigen; ausgenommen ist eine den Moortypen (Norm-, Mulm-, Erdniedermoor) angepasste Bewirtschaftung, wobei eine weitere Degradierung des Moorkörpers so weit wie möglich auszuschließen ist;
3.  Quellbereiche sowie Kleingewässer, natürliche oder naturnahe Fließgewässer, Alt- oder Totarme nachteilig zu verändern, zu beschädigen oder zu zerstören;
4.  Bäume außerhalb des Waldes, Hecken, Gebüsche, Feld- oder Ufergehölze, Ufervegetation oder Schwimmblattgesellschaften zu beschädigen oder zu beseitigen;
5.  in Röhrichte einzudringen oder sich diesen wasserseitig dichter als 5 Meter zu nähern.

(2) Sonstige Handlungen, die geeignet sind, den Charakter des Gebietes zu verändern, den Naturhaushalt zu schädigen, das Landschaftsbild zu verunstalten oder sonst dem besonderen Schutzzweck zuwiderzulaufen, bedürfen der Genehmigung.
 Der Genehmigung bedarf insbesondere, wer beabsichtigt,

1.  bauliche Anlagen, die einer öffentlich-rechtlichen Zulassung oder Anzeige bedürfen, zu errichten oder wesentlich zu verändern;
2.  die Bodengestalt zu verändern, die Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen; ausgenommen zur saisonalen Direktvermarktung landwirtschaftlicher und forstwirtschaftlicher Produkte;
4.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen anzulegen, Leitungen zu verlegen oder solche Anlagen wesentlich zu verändern;
5.  außerhalb öffentlich-rechtlich zugelassener und gekennzeichneter Plätze sowie von Hausgärten Wohnwagen aufzustellen; ausgenommen zur Ernte und saisonalen Direktvermarktung landwirtschaftlicher Produkte sowie zur Holzernte;
6.  Veranstaltungen mit motorbetriebenen Fahrzeugen durchzuführen;
7.  Grünland in eine andere Nutzungsart zu überführen;
8.  die Bodenbedeckung auf Acker- und Grünland abzubrennen;
9.  außerhalb des Waldes standortfremde oder landschaftsuntypische Gehölzpflanzungen vorzunehmen;
10.  außerhalb von öffentlich-rechtlich zugelassenen und gekennzeichneten Plätzen sowie Hausgärten, Kleingärten und Ferien- und Wochenendhausgrundstücken offene Feuerstätten zu errichten oder zu betreiben.

(3) Die Genehmigung nach Absatz 2 ist, unbeschadet anderer Rechtsvorschriften, auf Antrag von der unteren Naturschutzbehörde zu erteilen, wenn die beabsichtigte Handlung den Charakter des Gebietes nicht verändert, den Naturhaushalt nicht schädigt oder dem Schutzzweck nach § 3 nicht oder nur unerheblich zuwiderläuft.
Die Genehmigung kann mit Nebenbestimmungen versehen werden.

(4) Die Absätze 1 bis 3 gelten nicht für Flächen im Geltungsbereich eines Bauleitplans, für die eine bauliche oder sonstige dem Schutzzweck widersprechende Nutzung dargestellt oder festgesetzt ist, sofern das für Naturschutz und Landschaftspflege zuständige Ministerium diesen Darstellungen oder Festsetzungen zugestimmt hat.
Diese Flächen sind im Bauleitplan in geeignetem Maßstab kartografisch darzustellen.

§ 5  
Zulässige Handlungen
--------------------------

(1) Entgegen § 4 bleiben zulässig:

1.  die den in § 1b Absatz 4 des Brandenburgischen Naturschutzgesetzes genannten Anforderungen und Grundsätzen entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass § 4 Absatz 1 Nummer 2 und 4 sowie § 4 Absatz 2 Nummer 7 bis 9 gilt;
2.  die den in § 1b Absatz 5 des Brandenburgischen Naturschutzgesetzes genannten Anforderungen in Verbindung mit dem Waldgesetz des Landes Brandenburg entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass Höhlenbäume erhalten bleiben;
3.  die rechtmäßige Ausübung der Jagd;
4.  die den in § 1b Absatz 6 des Brandenburgischen Naturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung sowie die Angelfischerei auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  § 4 Absatz 1 Nummer 5 gilt, wobei für Fischereiberechtigte und Fischereiausübungsberechtigte das Betreten zum Zwecke des Einsetzens, der Kontrolle und des Entfernens von Fanggeräten und zur ökologisch verträglichen Nutzung abgestorbener Teile von Schilf und Rohrbeständen gemäß § 3 Absatz 1 Satz 2 des Fischereigesetzes für das Land Brandenburg gestattet bleibt,
    2.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass ein Einschwimmen und eine Gefährdung des Fischotters weitgehend ausgeschlossen sind;
5.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, die den in § 3 aufgeführten Schutzgütern nicht entgegensteht, mit der Maßgabe, dass
    
    1.  Maßnahmen zeitlich und räumlich derart durchzuführen sind, dass ein vielfältiger und standortgerechter Pflanzen- und Tierbestand erhalten bleibt oder sich neu entwickeln kann,
    2.  bei erforderlichen Wasserbaumaßnahmen möglichst natürliche Baustoffe und ingenieurbiologische Methoden verwendet werden,
    3.  keine Pflanzenschutzmittel verwendet werden;
6.  nach Inkrafttreten dieser Verordnung im Benehmen mit der unteren Naturschutzbehörde wasserrechtlich zugelassene Gewässerbenutzungen;
7.  die Anlage und Änderung von Straßen und Wegen im Rahmen von Bodenordnungs- oder Flurneuordnungsverfahren im Einvernehmen mit der unteren Naturschutzbehörde sowie die ordnungsgemäße Unterhaltung der rechtmäßig bestehenden Anlagen einschließlich der dem öffentlichen Verkehr gewidmeten Straßen und Wege im Benehmen mit der unteren Naturschutzbehörde.
    Der Herstellung des Benehmens bedarf es nicht, soweit es sich um unaufschiebbare Maßnahmen handelt;
8.  Maßnahmen der Modernisierung, Instandsetzung sowie der notwendigen Anpassung der Infrastruktur an umweltrechtliche Erfordernisse auf räumlich abgegrenzten landwirtschaftlichen Betriebsstandorten, die als solche im Liegenschaftskataster bezeichnet sind.
    Soweit diese Maßnahmen eine Errichtung beziehungsweise Erweiterung von Baukörpern, die einer öffentlich-rechtlichen Genehmigung oder Anzeige bedürfen, darstellen, ist das Einvernehmen mit der unteren Naturschutzbehörde erforderlich;
9.  Handlungen nach § 4 Absatz 1 Nummer 4 und Absatz 2 Nummer 9 in rechtmäßig bestehenden Baumschulen, Gärten, Friedhöfen, Park- und Gartenanlagen;
10.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
11.  Schutz-, Pflege- Entwicklungs- und Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
12.  Maßnahmen zur Untersuchung von Altlastenverdachtsflächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Benehmen mit der unteren Naturschutzbehörde.
    Der Herstellung des Benehmens bedarf es nicht, soweit es sich um unaufschiebbare Maßnahmen handelt;
13.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl.
    S.
     1734) an Straßen und Wegen freigestellt;
14.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 Absatz 1 Nummer 5 für das Betreten und Befahren des Landschaftsschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums.
Der Genehmigungsvorbehalt nach § 19 Absatz 3 des Landeswaldgesetzes bleibt unberührt.

§ 6  
Pflege- und Entwicklungsmaßnahmen
---------------------------------------

Es werden folgende Maßnahmen als Zielvorgabe für die Pflege und Entwicklung des Gebietes benannt:

1.  die gegenwärtigen Gebietswasserverhältnisse sollen weitestgehend gesichert und verbessert werden; das Regenerationsvermögen und damit die Wasserqualität der Gewässer soll durch den Erhalt und die Förderung einer standortgemäßen Ufervegetation kontinuierlich verbessert werden.
    In einem Bereich von mindestens 10 Metern beidseitig der Uferränder soll auf den Einsatz von mineralischen Düngemitteln, Gülle und Pflanzenschutzmitteln verzichtet werden;
2.  Feuchtwiesen und deren Auflassungsstadien sowie Wiesen auf Niedermoor sollen durch Maßnahmen, die zu standortspezifischen Grundwasserverhältnissen führen und durch extensive Nutzung, regelmäßige Pflege sowie Entbuschung entwickelt werden.
    Auf die Anwendung von Pflanzenschutzmitteln und Düngern soll verzichtet werden;
3.  die Wälder sollen in naturnahe Waldgesellschaften überführt werden;
4.  Sandtrockenrasen sollen durch geeignete Pflegemaßnahmen, wie Entbuschung oder Mahd, erhalten werden;
5.  zur Erhöhung der Lebensraumeignung für den Fischotter sollen die Uferränder stärker durch Gehölzanpflanzungen strukturiert werden.
    Es wird angestrebt, die Fließgewässer in ihrem Profil naturnah zu gestalten.
    Neue Brücken sollen durch offene Brückenprofile und Bankette otterfreundlich gestaltet werden;
6.  die Zülowniederung soll unter Beibehaltung der land- und forstwirtschaftlichen Nutzung entwickelt werden;
7.  die Erholungsnutzung soll naturraumorientiert durch geeignete Lenkungsmaßnahmen (Rad-, Wander-, Reitwegenetz) entwickelt und die Lebensräume von empfindlichen, bestandsbedrohten Tier- und Pflanzenarten vor Störungen geschützt werden.

§ 7  
Befreiungen
-----------------

Von den Verboten kann die zuständige Naturschutzbehörde auf Antrag gemäß § 72 des Brandenburgischen Naturschutzgesetzes Befreiung gewähren.
Dies gilt auch im Fall der Versagung einer Genehmigung nach § 4 Absatz 2 und 3.

§ 8   
Ordnungswidrigkeiten
---------------------------

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig

1.  den Vorschriften des § 4 Absatz 1 Nummer 1 bis 5 zuwiderhandelt;
2.  Handlungen nach § 4 Absatz 2 Satz 2 Nummer 1 bis 10 ohne die erforderliche Genehmigung vornimmt;
3.  den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

§ 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen
--------------------------------------------------------------------------------

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungs- sowie Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Soweit für den Bereich des Landschaftsschutzgebietes weiter gehende naturschutzrechtliche Vorschriften bestehen, bleiben diese unberührt.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 31 bis 35 des Brandenburgischen Naturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 39 bis 55 des Bundesnaturschutzgesetzes und §§ 37 bis 43a des Brandenburgischen Naturschutzgesetzes) unberührt.

§ 10  
Geltendmachen von Rechtsmängeln
--------------------------------------

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach der Verkündung schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

§ 11  
Inkrafttreten, Außerkrafttreten
--------------------------------------

(1) Diese Verordnung tritt mit Wirkung vom 28.
Oktober 2009 in Kraft.

(2) Gleichzeitig treten außer Kraft:

1.  Beschluss Nummer 149-14/66 des Rates des Bezirkes Potsdam vom 20.
    Juli 1966 in Verbindung mit dem Beschluss Nummer 18/72 des Rates des Bezirkes Potsdam vom 19.
    Oktober 1972 über die Erklärung zum Landschaftsschutzgebiet „Teupitz-Köriser Seengebiet“ für den Geltungsbereich dieser Verordnung und für Ortslagen und ortsnahe Bereiche, die allseitig vom Landschaftsschutzgebiet „Notte-Niederung“ umschlossen sind, einschließlich der in der in § 2 Absatz 2 genannten Übersichtskarte und der topografischen Karte mit den Blattnummern 13, 14, 17, 18 und 20 schraffierten Flächen;
2.  Verordnung über das Landschaftsschutzgebiet „Pfählingsee-Prierowsee“ des Landrates Teltow vom 16.
    März 1938;
3.  Verordnung zum Schutze von Landschaftsteilen in den Gemeinden Krummensee, Schenkendorf und Zeesen (Landschaftsschutzgebiet „Krummensee“) vom 12.
    August 1941;
4.  Verordnung zum Schutze von Landschaftsteilen im Bereich der Gemeinden Bestensee und Krummensee (Landschaftsschutzgebiet „Die Sutschke“) vom 15.
    August 1938;
5.  Verordnung über das Landschaftsschutzgebiet „Notte-Niederung“ vom 22.
     September 2009 (GVBl.
    II S.
    718).

Potsdam, den 23.
Januar 2012

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

Anlage 1  
(zu § 2 Absatz 2)
----------------------------

![Das Landschaftsschutzgebiet Notte-Niederung hat eine Größe von rund 18013 Hektar. Es umfasst Teile der Ortslagen der Gemeinden Bestensee, Königs Wusterhausen, Mittenwalde, Teupitz und Groß Köris im Landkreis Dahme-Spreewald sowie Teile der Ortslagen der Gemeinden Am Mellensee, Blankenfelde-Mahlow, Ludwigsfelde, Rangsdorf und Zossen im Landkreis Teltow-Fläming.](/br2/sixcms/media.php/69/Nr.%2075-2.JPG "Das Landschaftsschutzgebiet Notte-Niederung hat eine Größe von rund 18013 Hektar. Es umfasst Teile der Ortslagen der Gemeinden Bestensee, Königs Wusterhausen, Mittenwalde, Teupitz und Groß Köris im Landkreis Dahme-Spreewald sowie Teile der Ortslagen der Gemeinden Am Mellensee, Blankenfelde-Mahlow, Ludwigsfelde, Rangsdorf und Zossen im Landkreis Teltow-Fläming.")

Anlage2  
(zu § 2 Absatz 2)
---------------------------

**  
1.
Übersichtskarte im Maßstab 1 : 50 000  
**

**Titel:  Übersichtskarte zur Verordnung über das Landschaftsschutzgebiet „Notte-Niederung“**

**Lfd.
Nummer**

**Unterzeichnung**

1

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 2.
August 2012

**  
2.
Topografische Karten im Maßstab 1 : 10 000  
**

**Titel:**

Topografische Karte zur Verordnung über das Landschaftsschutzgebiet „Notte-Niederung“

**Lfd.
Nummer**

**Kartenblatt**

**Unterzeichnung**

01

3645 - SO

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

02

3646 - SW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

03

3646 - SO

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 17.
Januar 2012

04

3647 - SW

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 2.
August 2012

05

3647 - SO

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 2.
August 2012

06

3745 - NO

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

07

3746 - NW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

08

3746 - NO

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 17.
Januar 2012

09

3747 - NW

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 2.
August 2012

10

3747 - NO

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

11

3746 - SW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

12

3746 - SO

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 17.
Januar 2012

13

3747 - SW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

14

3747 - SO

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

15

3846 - NW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

16

3846 - NO

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

17

3847 - NW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

18

3847 - NO

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

19

3847 - SW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

20

3847 - SO

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

  
**3.
Flurkarten/Liegenschaftskarten  
**

**Titel:**                           Liegenschaftskarte zur Verordnung über das Landschaftsschutzgebiet „Notte-Niederung“

**Lfd.
Nummer**

**Gemarkung**

**Flur**

**Unterzeichnung**

1

Klein Kienitz  
Brusendorf

2  
1,3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

2

Brusendorf  
Ragow

3  
1

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 2.
August 2012

5

Löwenbruch

1,3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

6

Genshagen  
Löwenbruch  
Groß Schulzendorf

1  
1,2,3  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

7

Genshagen  
Jühnsdorf  
Groß Schulzendorf

1  
1,2,6  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

8

Jühnsdorf  
Rangsdorf

1,2,3,4,5  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

9

Jühnsdorf  
Dahlewitz  
Rangsdorf

4  
1,4,5  
1,6,7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

10

Dahlewitz  
Klein Kienitz  
Groß Machnow

5  
1,2  
2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

11

Klein Kienitz

1,2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

12

Klein Kienitz  
Brusendorf

1,2  
1,3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

13

Brusendorf  
Ragow  
Mittenwalde

1,3  
1,4  
1

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 2.
August 2012

14

Ragow  
Deutsch Wusterhausen

1,2,3,4  
3

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 2.
August 2012

15

Ragow  
Deutsch Wusterhausen

2  
1,3

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 2.
August 2012

16

Löwenbruch

3,4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

17

Löwenbruch  
Wietstock  
Groß Schulzendorf

2,3,4  
2  
1,2,7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

18

Jühnsdorf  
Groß Schulzendorf

1,5,6  
1,2,3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

19

Jühnsdorf  
Rangsdorf  
Groß Schulzendorf

1,4,5,6  
1  
3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

20

Rangsdorf

1,6

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

21

Klein Kienitz  
Groß Machnow

1  
2,3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 17.
Januar 2012

22

Klein Kienitz  
Groß Machnow

1  
2,3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

23

Klein Kienitz  
Groß Machnow  
Brusendorf  
Mittenwalde

1  
2,3  
1,4  
1,2

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 2.
August 2012

24

Brusendorf  
Mittenwalde  
Ragow

1  
1,2  
4,5

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 2.
August 2012

25

Ragow  
Deutsch Wusterhausen

3,4,5  
3

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 2.
August 2012

26

Deutsch Wusterhausen

2,3

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 2.
August 2012

27

Kerzendorf  
Löwenbruch  
Wietstock

1  
4  
2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

28

Löwenbruch  
Wietstock  
Groß Schulzendorf

4  
2,3  
2,6,7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

29

Glienick  
Groß Schulzendorf

3  
2,3,4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

30

Rangsdorf  
Glienick  
Groß Schulzendorf

1,2,3  
3  
3, 4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

31

Rangsdorf  
Glienick  
Groß Machnow

1,2,3,21  
3  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

32

Rangsdorf  
Groß Machnow

21  
1,2,3,4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 17.
Januar 2012

33

Groß Machnow

2,3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

34

Groß Machnow  
Brusendorf  
Mittenwalde

3  
4  
2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

35

Groß Machnow  
Mittenwalde  
Ragow

3  
2,3,6,7  
5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

36

Ragow  
Mittenwalde  
Schenkendorf

3,5  
6,7,8  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

37

Ragow  
Schenkendorf  
Deutsch Wusterhausen

3  
1,2  
2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

38

Wietstock

2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

39

Wietstock

2,3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

40

Glienick  
Groß Schulzendorf

3  
4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

41

Glienick  
Rangsdorf  
Dabendorf

3,5  
2,3  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

42

Rangsdorf  
Glienick  
Groß Machnow  
Dabendorf

3  
3  
1,4  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

43

Groß Machnow  
Telz

1,3,4  
1,2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

44

Groß Machnow  
Telz

3,4  
2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

45

Groß Machnow  
Telz  
Mittenwalde

3,4  
2  
3,4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

46

Groß Machnow  
Mittenwalde

3  
3,4,5,6,9

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

47

Mittenwalde  
Schenkendorf

6,7,8,13  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

48

Mittenwalde  
Schenkendorf

13,15  
1,3,4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

49

Schenkendorf  
Zeesen

3,4  
8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

50

Glienick

3,5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

51

Glienick  
Dabendorf

3,5  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

52

Glienick  
Dabendorf  
Groß Machnow

5  
1,2,7  
1,4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

53

Dabendorf  
Groß Machnow  
Telz

7,8  
4  
1,2,7,8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

54

Telz

2,7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

55

Telz  
Mittenwalde

2,3,4,5,6,7  
4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

56

Telz  
Gallun  
Mittenwalde

3,4,5  
1  
4,5,9,10,11,12,14

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

57

Gallun  
Mittenwalde

1, 2  
8,10,12,13,14,15

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

58

Mittenwalde  
Schenkendorf  
Gallun  
Bestensee

15  
4  
2  
14

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

59

Schenkendorf  
Bestensee

4  
1,14

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

60

Glienick  
Nächst Neuendorf

5  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

61

Glienick  
Dabendorf  
Nächst Neuendorf

5  
1,2,3  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 17.
Januar 2012

62

Dabendorf  
Zossen  
Telz

8  
1,2  
7,8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

63

Telz

7,8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

64

Telz  
Schöneiche

4,5,6,7,8  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

65

Telz

4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

66

Glienick  
Horstfelde  
Nächst Neuendorf

5  
1,2  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

67

Dabendorf  
Zossen  
Horstfelde  
Nächst Neuendorf

3  
2,11,14  
2  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

68

Zossen  
Schöneiche  
Telz

2,3,14  
1  
8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

69

Telz  
Zossen  
Schöneiche

8  
3  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

70

Telz  
Zossen  
Schöneiche

8  
3  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

71

Horstfelde  
Zossen

1,2  
10

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

72

Horstfelde  
Zossen  
Nächst Neuendorf

2  
9,10,11,14  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

73

Zossen

5,11,12,13,14

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

74

Zossen

3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

75

Zossen

3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

76

Horstfelde  
Zossen  
Saalow  
Mellensee

2  
10  
3  
1,2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

77

Zossen  
Mellensee

6,8,9,10,11  
2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

78

Zossen

5,6,8,11

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 17.
Januar 2012

79

Saalow  
Mellensee

3  
1,2,3,4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

80

Mellensee  
Zossen  
Wünsdorf

2,3  
8,9  
2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

81

Zossen  
Wünsdorf

7,8  
2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

82

Mellensee  
Klausdorf  
Wünsdorf

3,4  
5  
8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

83

Mellensee  
Zossen  
Wünsdorf

3,4  
8  
1,2,8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

84

Wünsdorf  
Zossen

1,2,3  
7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

85

Klausdorf  
Wünsdorf

3  
1,2,7,8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

86

Klausdorf  
Wünsdorf

3,5  
8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

87

Wünsdorf

1,2,5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

88

Mittenwalde  
Gallun

14  
1,2,3,4,5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

89

Gallun  
Bestensee

2,3,4,5  
1,8,9,14,15

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

90

Schenkendorf  
Bestensee

4  
1,2,7,8,9,14

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

91

Kallinchen  
Gallun  
Motzen

1,2,6  
3,4  
1,5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

92

Gallun  
Bestensee  
Motzen

4,5  
8,9  
1,2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

93

Bestensee

7,8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

94

Kallinchen  
Motzen

2,6  
1,5,7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

95

Kallinchen  
Bestensee  
Motzen

6  
8  
2,3,5,7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

96

Motzen  
Bestensee

3  
8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

97

Kallinchen

3,6

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

98

Kallinchen  
Motzen

3,6  
3,4,5,6

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

99

Motzen  
Bestensee

3,4  
8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

100

Kallinchen  
Motzen  
Töpchin  
Groß Köris

3,6  
4,5,6  
2  
4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

101

Motzen  
Groß Köris

4  
4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

102

Töpchin

5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

103

Töpchin  
Groß Köris

2,5,6  
3,4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

104

Groß Köris

3,4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

105

Töpchin

5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

106

Töpchin  
Groß Köris  
Teupitz

5,6  
3  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

107

Groß Köris  
Teupitz

1,3  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

108

Groß Köris

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

109

Zehrensdorf  
Töpchin  
Egsdorf

9  
4,5,6  
2,3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

110

Töpchin  
Teupitz  
Egsdorf

5,6  
1  
3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

111

Teupitz

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

112

Zehrensdorf  
Zesch am See  
Töpchin  
Egsdorf

9  
1,2  
6  
1,2,3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

113

Egsdorf

3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

114

Zesch am See  
Egsdorf

2  
1,2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009

115

Egsdorf

1,3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MLUV, am 18.
September 2009