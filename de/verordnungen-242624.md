## Verordnung zur Zuständigkeitsregelung im Bescheinigungsverfahren nach dem Umsatzsteuergesetz

Auf Grund des § 6 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28 S. 2) geändert worden ist, in Verbindung mit § 4 Nummer 21 Buchstabe a Doppelbuchstabe bb des Umsatzsteuergesetzes in der Fassung der Bekanntmachung vom 21.
Februar 2005 (BGBl. I S. 386) verordnet die Landesregierung:

#### § 1 Zuständigkeit

Zuständige Landesbehörden für Bescheinigungen nach § 4 Nummer 21 Buchstabe a Doppelbuchstabe bb des Umsatzsteuergesetzes sind:

1.  für den Geschäftsbereich des Ministeriums für Arbeit, Soziales, Gesundheit, Frauen und Familie das
    1.  Landesamt für Soziales und Versorgung für  
          
        
        aa)
        
        Bildungseinrichtungen, soweit diese auf einen Beruf oder auf eine – vor einer juristischen Person des öffentlichen Rechts abzulegende – Prüfung im Bereich der gewerblichen Wirtschaft vorbereiten,
        
        bb)
        
        alle Ausbildungs- und Weiterbildungseinrichtungen von Berufen für Altenpflegerinnen und Altenpfleger und für Altenpflegehelferinnen und Altenpflegehelfer und
        
        cc)
        
        die Weiterbildungseinrichtungen von sozialen Berufen mit Ausnahme der Berufe Erzieherin und Erzieher, Sonderpädagogin und Sonderpädagoge, Sozialpädagogin und Sozialpädagoge;
        
    2.  Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit  
          
        Abteilung Gesundheit für  
          
        
        aa)
        
        alle Ausbildungs- und Weiterbildungseinrichtungen von Fachberufen des Gesundheitswesens mit Ausnahme der Berufe in der Altenpflege und
        
        bb)
        
        Heilpraktikerschulen;
        
2.  für den Geschäftsbereich des Ministeriums für Bildung, Jugend und Sport
    1.  Ministerium für Bildung, Jugend und Sport für  
          
        
        aa)
        
        alle Ausbildungs- und Weiterbildungseinrichtungen der Bereiche der Erzieherinnen und Erzieher, der Sonderpädagoginnen und Sonderpädagogen, der Sozialpädagoginnen und Sozialpädagogen sowie für Angebote in Kindertageseinrichtungen und Schulen  
          
        einschließlich  
          
        selbstständiger Kunstpädagoginnen und Kunstpädagogen, soweit diese vorwiegend Bildungsleistungen im Rahmen der Jugendsozialarbeit erbringen, und
        
        bb)
        
        Tanzschulen, Tanzakademien, Tanzstudios, insbesondere nach dem ADTV-Zertifizierungssystem;
        
    2.  Staatliches Schulamt Frankfurt (Oder) für  
          
        Träger anerkannter Ergänzungsschulen;  
          
        
    3.  Staatliche Schulämter gemäß ihrer regionalen Zuständigkeiten für  
          
        alle ordnungsgemäß angezeigten freien Einrichtungen, in denen Nachhilfeunterricht erteilt wird;
3.  das Ministerium der Finanzen für
    1.  Aus- und Fortbildungsmaßnahmen im eigenen Geschäftsbereich und
    2.  Bildungseinrichtungen, die für die Prüfung zur Steuerberaterin und zum Steuerberater, zur Steuerfachwirtin und zum Steuerfachwirt und zur Steuerfachgehilfin und zum Steuerfachgehilfen vorbereiten;
4.  für den Geschäftsbereich des Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft das  
      
    Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung für  
      
    Privatschulen und Bildungseinrichtungen, soweit sie auf Berufe der Landwirtschaft und der Hauswirtschaft oder auf eine staatliche Prüfung im Bereich der Land- und Forstwirtschaft vorbereiten;
5.  für den Geschäftsbereich des Ministeriums für Infrastruktur und Landesplanung das  
      
    Landesamt für Bauen und Verkehr für  
    1.  Fahrschulen und
    2.  Bildungseinrichtungen, die auf einen Beruf oder eine staatliche Prüfung im Bereich des Verkehrswesens vorbereiten oder Schulungen zum Erwerb gesetzlich geforderter Sachkunde durchführen;
6.  das Gemeinsame Juristische Prüfungsamt der Länder Berlin und Brandenburg für
    1.  Bildungseinrichtungen, die auf die rechtswissenschaftlichen Staatsprüfungen vorbereiten, und
    2.  Bildungseinrichtungen, die der beruflichen Aus- und Fortbildung in der Justiz dienen;
7.  das Ministerium für Wissenschaft, Forschung und Kultur für
    1.  Einrichtungen zur Vorbereitung auf Prüfungen in oder nach einem Hochschulstudium, für staatlich anerkannte Hochschulen sowie für Einrichtungen, die der Aufsicht des Ministeriums für Wissenschaft, Forschung und Kultur unterliegen,
    2.  Musikschulen und selbstständige Musikpädagoginnen und Musikpädagogen,
    3.  Kunstschulen und selbstständige Kunstpädagoginnen und Kunstpädagogen, sofern diese vorwiegend künstlerisch-ästhetische Bildung vermitteln,
    4.  Ballettschulen und sonstige tanzpädagogische Einrichtungen sowie selbstständige Tanzpädagoginnen und Tanzpädagogen, sofern im Unterrichtskonzept und in den Unterrichtsangeboten der künstlerische Aspekt, also die Hinwendung zum Bühnentanz, wie er an Theatern in Musiktheater- sowie Tanz- und Ballettaufführungen zum Tragen kommt, den Schwerpunkt bildet,
    5.  Musical- und Schauspielschulen,
    6.  selbstständige Künstlerinnen und Künstler im Bereich von bildender, darstellender und angewandter Kunst, die kulturelle Bildungsarbeit leisten, und
    7.  Aus- und Fortbildungsmaßnahmen des eigenen Geschäftsbereichs;
8.  für den Geschäftsbereich des Ministeriums des Innern und für Kommunales
    1.  Polizeipräsidium für  
          
        Bildungseinrichtungen, die der beruflichen Aus- und Fortbildung der Bediensteten des Polizeipräsidiums dienen;
    2.  Zentraldienst der Polizei Wünsdorf für  
          
        Bildungseinrichtungen, die der beruflichen Aus- und Fortbildung der Bediensteten im Bereich des Zentraldienstes der Polizei dienen;
    3.  Brandenburgischer IT-Dienstleister für  
          
        Bildungseinrichtungen, die der beruflichen Aus- und Fortbildung des öffentlichen Dienstes des Landes Brandenburg im Bereich der IT und des E-Government dienen.

#### § 2 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 29.
Juni 2017

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

  
Der Minister der Finanzen

Christian Görke