## Verordnung zur Übertragung der Zuständigkeit zur Bestimmung der Ärztin oder des Arztes für Gesundheitsuntersuchungen nach § 62 Absatz 1 Satz 2 des Asylverfahrensgesetzes (Zuständigkeitsverordnung nach dem Asylverfahrensgesetz für Gesundheitsuntersuchungen - ZVAsylVfG)

Auf Grund des § 9 Absatz 1 und des § 16 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S.
186) in Verbindung mit § 62 Absatz 1 Satz 2 des Asylverfahrensgesetzes in der Fassung der Bekanntmachung vom 2.
September 2008 (BGBl.
I S.
1798) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz :

§ 1  
Übertragung der Zuständigkeit
-----------------------------------

(1) Die Gesundheitsuntersuchung nach § 62 des Asylverfahrensgesetzes erfolgt grundsätzlich vor der landesinternen Verteilung nach § 50 Absatz 1 des Asylverfahrensgesetzes.
Auf die Gesundheitsuntersuchung kann verzichtet werden, wenn die letzte Untersuchung nach § 62 des Asylverfahrensgesetzes nicht länger als ein Jahr zurückliegt und keine Anhaltspunkte für eine zwischenzeitlich eingetretene meldepflichtige Erkrankung im Sinne des § 6 des Infektionsschutzgesetzes bekannt geworden sind.

(2) Die Landkreise als Träger des Öffentlichen Gesundheitsdienstes, in deren örtlichem Zuständigkeitsbereich sich die Erstaufnahmeeinrichtung beziehungsweise die Unterkunft für das Asylverfahren nach § 18a des Asylverfahrensgesetzes befindet, bestimmen nach § 62 Absatz 1 Satz 2 des Asylverfahrensgesetzes jeweils die Ärztin oder den Arzt, die oder der die ärztlichen Untersuchungen auf übertragbare Krankheiten einschließlich einer Röntgenaufnahme der Atmungsorgane nach § 62 Absatz 1 Satz 1 des Asylverfahrensgesetzes durchführt.

(3) Die Aufgabe nach Absatz 2 wird als Pflichtaufgabe zur Erfüllung nach Weisung übertragen.
Sonderaufsichtsbehörde über die Landkreise als Träger des Öffentlichen Gesundheitsdienstes ist das für Gesundheit zuständige Ministerium.
 Für die Sonderaufsichtsbehörde ist § 121 Absatz 2 bis 4 der Kommunalverfassung des Landes Brandenburg anwendbar.
Das Recht, besondere Weisungen zu erteilen, ist nicht auf den Bereich der Gefahrenabwehr beschränkt.
Einzelanweisungen sind zulässig.

§ 2  
Kosten
------------

Das Land erstattet dem die Gesundheitsuntersuchung durchführenden Landkreis die Kosten der Untersuchung durch einen pauschalen Abgeltungsbetrag.
Dieser beträgt 138 Euro pro Untersuchung.
Sind auf Grund klinischer oder epidemiologischer Anhaltspunkte in Abstimmung mit dem Land weitere Untersuchungen erforderlich, werden die den pauschalen Abgeltungsbetrag übersteigenden Mehrkosten auf Antrag erstattet.

§ 3  
Inkrafttreten, Außerkrafttreten
-------------------------------------

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2014 in Kraft und am 30.
Juni 2015 außer Kraft.

Potsdam, den 29.
Juli 2014

Die Ministerin für Umwelt, Gesundheit und Verbraucherschutz

Anita Tack