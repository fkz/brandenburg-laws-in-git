## Verordnung über das Naturschutzgebiet „Damerower Wald, Schlepkower Wald und Jagenbruch“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S.
2542) in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr.
3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nr.
43) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1  
Erklärung zum Schutzgebiet
--------------------------------

Die in § 2 näher bezeichnete Fläche im Landkreis Uckermark wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Damerower Wald, Schlepkower Wald und Jagenbruch“.

§ 2  
Schutzgegenstand
----------------------

(1) Das Naturschutzgebiet hat eine Größe von rund 671 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Gemeinde:**

**Gemarkung:**

**Flur:**

Uckerland

Wolfshagen

1, 3, 4;

Ottenhagen

1;

Schlepkow

2, 3, 5;

Nordwestuckermark

Fürstenwerder

7, 8;

Damerow

1;

Kraatz

2, 3.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte Übersichtskarte im Maßstab 1 : 20 000 dient der räumlichen Einordnung des Naturschutzgebietes.
Die in Anlage 2 Nummer 2 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 01 bis 03 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 3 mit den Blattnummern 01 bis 14 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Innerhalb des Naturschutzgebietes wird gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes eine Zone 1 als Naturentwicklungsgebiet festgesetzt, die der direkten menschlichen Einflussnahme entzogen ist und in der Lebensräume und Lebensgemeinschaften langfristig ihrer natürlichen Entwicklung überlassen bleiben.
Die Zone 1 umfasst zwei Teilflächen mit insgesamt rund 146 Hektar und liegt in folgenden Fluren:

**Gemeinde:**

**Gemarkung:**

**Flur:**

Nordwestuckermark

Damerow

1;

Uckerland

Ottenhagen

1;

Wolfshagen

3.

Innerhalb des Naturschutzgebietes sind weiterhin eine Zone 2 mit rund 224 Hektar und eine Zone 3 mit rund 19 Hektar mit weitergehenden Beschränkungen der landwirtschaftlichen Bodennutzung festgesetzt.

Die Grenzen der Zonen sind in der in Anlage 2 Nummer 1 genannten Übersichtskarte, in den in Anlage 2 Nummer 2 genannten topografischen Karten sowie in den in Anlage 2 Nummer 3 genannten Liegenschaftskarten mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Uckermark, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

§ 3  
Schutzzweck
-----------------

(1) Schutzzweck des Naturschutzgebietes als naturnaher Ausschnitt des Uckermärkischen Hügellandes ist

1.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Kleingewässer, der Röhrichte und Großseggenriede, der nährstoffreichen Moore, der Bruch- und Laubmischwälder, der Laubgebüsche, der Waldmäntel, der Hecken, des Feucht- und Frischgrünlandes und der Trockenrasen;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Wasser-Schwertlilie (Iris pseudacorus), Weiße Seerose (Nymphaea alba), Gelbe Teichrose (Nuphar lutea), Fieberklee (Menyanthes trifoliata), Kammfarn (Dryopteris cristata) und Breitblättrige Stendelwurz (Epipactis palustris);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Säugetiere, Vögel, Lurche, Kriechtiere, Käfer, Hautflügler, Schmetterlinge und Libellen, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Großer Abendsegler (Nyctalus noctula), Mückenfledermaus (Pipistrellus pygmaeus), Raubwürger (Lanius excubitor), Kleinspecht (Dendrocopus minor), Braunkehlchen (Saxicola rubetra), Teichmolch (Triturus vulgaris), Laubfrosch (Hyla arborea), Erdkröte (Bufo bufo), Ringelnatter (Natrix natrix), Zauneidechse (Lacerta agilis), Waldeidechse (Lacerta vivipara), Körniger Laufkäfer (Carabus granulatus), Kurzgewölbter Laufkäfer (Carabus convexus), Hornisse (Vespa crabro), Kleines Wiesenvögelchen (Coenonympha pamphilus), Gemeiner Bläuling (Polyommatus icarus) und Plattbauch (Libellula depressa);
4.  die Erhaltung eines Verlandungs- und Durchströmungsmoores sowie einer stark bewegten Moränenlandschaft mit naturnahen Wald- und Offenlandbiotopen wegen ihrer Seltenheit, strukturellen Vielfalt, besonderen Eigenart und hervorragenden Schönheit;
5.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des regionalen Biotopverbundes zwischen dem Dammsee und dem Haussee sowie zwischen den Waldgebieten Kiecker, Kutzerower Heide, Hildebrandshagen und Wolfshagener Park.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung

1.  eines Teiles des Europäischen Vogelschutzgebietes „Uckermärkische Seenlandschaft“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) in seiner Funktion  
    1.  als Lebensraum von Arten nach Anhang I der Richtlinie 2009/147/EG, insbesondere Schreiadler (Aquila pomarina), Schwarzstorch (Ciconia nigra), Wespenbussard (Pernis apivorus), Seeadler (Haliaeetus albicilla), Fischadler (Pandion haliaetus), Kranich (Grus grus), Weißstorch (Ciconia ciconia), Mittelspecht (Dendrocopus medius), Zwergschnäpper (Ficedula parva), Neuntöter (Lanius collurio) und Eisvogel (Alcedo atthis) einschließlich ihrer Brut- und Nahrungsbiotope,
    2.  als Vermehrungs- und Rastgebiet für im Gebiet regelmäßig auftretende Zugvogelarten wie Schellente (Bucephala clangula), Waldwasserläufer (Tringa ochropus), Bekassine (Gallinago gallinago) und Zwergtaucher (Tachybaptus ruficollis);
2.  des Gebietes von gemeinschaftlicher Bedeutung „Damerower Wald-Schlepkower Wald-Jagenbruch“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinem Vorkommen von
    1.  Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, von Flüssen der planaren bis montanen Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, von Feuchten Hochstaudenfluren der planaren und montanen bis alpinen Stufe, Waldmeister-Buchenwald (Asperulo-Fagetum) und Subatlantischem oder mitteleuropäischem Stieleichenwald oder Hainbuchenwald (Carpinion betuli) (Stellario carpinetum) als Biotope von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG),
    2.  Moorwäldern, Birken-Moorwäldern und Auen-Wäldern mit Alnus glutinosa (Schwarz-Erle) und Fraxinus excelsior (Gewöhnliche Esche) (Alno-Padion, Alnion incanae, Salicion albae) als prioritäre Biotope („prioritäre Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG),
    3.  Großem Mausohr (Myotis myotis), Fischotter (Lutra lutra), Kamm-Molch (Triturus cristatus) und Rotbauchunke (Bombina bombina) als Tierarten von gemeinschaftlichem Interesse (im Sinne des Anhangs II der Richtlinie 92/43/EWG), einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

(3) Darüber hinaus ist besonderer Schutzzweck der Zone 1 (Naturentwicklungsgebiet) der ungestörte Ablauf natürlicher Prozesse in den Lebensgemeinschaften von Torfstich-Gewässern, Birken-Moorwald, Erlen-Bruchwald, Erlen-Eschen-Wald, Waldmeister-Buchenwald, Stieleichen-Hainbuchenwald und naturnahem Laubwald reicher Standorte.

§ 4  
Verbote
-------------

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten.
    Ausgenommen ist außerhalb der Zone 1 das Betreten zum Zweck des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 8 ab dem 1.
    September eines jeden Jahres;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit Fahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
12.  Wasserfahrzeuge aller Art zu benutzen;
13.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
14.  Hunde frei laufen zu lassen;
15.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
16.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
17.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
18.  Tiere zu füttern oder Futter bereitzustellen;
19.  Tiere auszusetzen oder Pflanzen anzusiedeln;
20.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
21.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
22.  Pflanzenschutzmittel jeder Art anzuwenden;
23.  Wiesen, Weiden oder sonstiges Grünland umzubrechen oder neu anzusäen.

§ 5  
Zulässige Handlungen
--------------------------

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung außerhalb der Zone 1 auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland in Zone 2 als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle oder Sekundärrohstoffdünger wie zum Beispiel Abwasser, Klärschlamm und Bioabfälle einzusetzen,
    2.  Grünland in Zone 3 als Wiese oder Weide mit einer Besatzdichte von maximal 1,4 Raufutter verwertenden Großvieheinheiten (RGV) pro Hektar im Jahresmittel genutzt wird und § 4 Absatz 2 Nummer 16 gilt,
    3.  auf Grünland in den Zonen 2 und 3 § 4 Absatz 2 Nummer 22 gilt
    4.  bei der Beweidung Gehölze in geeigneter Weise gegen Verbiss und sonstige Beschädigungen sowie Ränder von Gewässern wirksam gegen Trittschäden von weidenden Nutztieren zu schützen sind,
    5.  § 4 Absatz 2 Nummer 23 gilt;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung außerhalb der Zone 1 auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  eine Nutzung nur einzelstammweise bis truppweise erfolgt; im Schwarzerlenwald im Jagenbruch bleiben eine horstweise Nutzung und Kulissenhiebe bis zu einer Fläche von 0,25 Hektar zulässig,
    2.  nur Baumarten der potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Baumarten in gesellschaftstypischen Anteilen unter Ausschluss eingebürgerter Arten zu verwenden sind,
    3.  mindestens fünf Stämme je Hektar mit einem Mindestdurchmesser von 30 Zentimetern in 1,30 MeterHöhe über dem Stammfuß bis zum Absterben aus der Nutzung genommen sein müssen,
    4.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 30 Zentimeter Durchmesser in1,30 Meter über Stammfuß nicht gefällt werden und liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimeter am stärksten Ende) im Bestand verbleibt,
    5.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    6.  hydromorphe Böden nur bei Frost sowie Böden mit einem hohen Anteil an feinkörnigem Substrat nur bei Frost oder Trockenperioden befahren werden,
    7.  das Befahren des Waldes nur auf Waldwegen und Rückegassen erfolgt; im Schwarzerlenwald im Jagenbruch bleibt das Befahren außerhalb von Waldwegen und Rückegassen bei Frost zulässig,
    8.  die Bewirtschaftung in der Zeit vom 1.
        März bis zum 31.
        August eines jeden Jahres unzulässig ist;Reparaturen an Wildschutzzäunen und Kulturpflegearbeiten ohne Maschineneinsatz bleiben mit Zustimmung der unteren Naturschutzbehörde zulässig,
    9.  § 4 Absatz 2 Nummer 22 gilt;
3.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung in Zone 1 auf den Flurstücken 16, 77 und 89 der Gemarkung Damerow, Flur 1, nach den Maßgaben des § 5 Absatz 1 Nummer 2 Buchstabe a bis i;
4.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung außerhalb der Zone 1 in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters weitgehend ausgeschlossen ist,
    2.  Fischbesatz nur mit heimischen Arten erfolgt; § 13 der Fischereiordnung des Landes Brandenburg bleibt unberührt,
    3.  Hegepläne einvernehmlich mit der unteren Naturschutzbehörde abzustimmen sind,
    4.  § 4 Absatz 2 Nummer 18 gilt;
5.  die rechtmäßige Ausübung der Angelfischerei an den Gewässern beidseitig der Straße Bülowssiege – Hildebrandshagen (Gemarkung Fürstenwerder, Flur 7, Flurstücke 476, 487 und 497) mit der Maßgabe, dass
    
    1.  das Betreten von Röhrichten und Verlandungszonen unzulässig ist,
    2.  § 4 Absatz 2 Nummer 12, 18 und 19 gilt.  
          
        
    
    An den übrigen Gewässern bleibt die Ausübung der Angelfischerei unzulässig;  
      
    erforderliche Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg innerhalb der Zone 1 im Sinne eines Monitorings mit Genehmigung der unteren Naturschutzbehörde.
    Die Genehmigung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird;
6.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass  
        aa)  in Zone 1 die Jagd in der Zeit vom 1.
        März bis zum 31.
        August eines jeden Jahres ruht,  
        bb)  außerhalb der Zone 1 in der Zeit vom 1.
        März bis zum 31.
        August eines jeden Jahres die Jagd nur vom Ansitz aus erfolgt,  
        cc)   die Jagd auf Federwild verboten ist,  
        dd)  die Fallenjagd nur mit Lebendfallen erfolgt,  
        ee)  keine Baujagd in einem Abstand von 100 Metern zu Gewässerufern vorgenommen wird,
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd außerhalb der Zone 1 mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.  
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Errichtung anzuzeigen.
        Die Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        Die Entscheidung hierzu soll unverzüglich erfolgen.
    
    Im Übrigen bleiben die Anlage von Ablenkfütterungen, Kirrungen, Ansaatwildwiesen und Wildäckern innerhalb der Zone 1 und gesetzlich geschützter Biotope unzulässig.
    Jagdrechtliche Regelungen nach § 41 des Brandenburgischen Jagdgesetzes bleiben unberührt;
7.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch außerhalb der Zone 1 ab dem 1.
    September eines jeden Jahres;
8.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter die Nummer 11 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
9.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
10.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen(Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
11.  Nutzung, Ausbau und Unterhaltung der Löschwasserentnahmestelle am Landgraben in der Gemarkung Wolfshagen, Flur 1, Flurstück 193/2;
12.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
13.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
14.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
15.  biotopeinrichtende Maßnahmen in der Zone 1, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind, innerhalb eines Zeitraums von zehn Jahren nach Inkrafttreten dieser Verordnung;
16.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007(ABl.
    S.
    1734) an Straßen und Wegen freigestellt;
17.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

§ 6  
Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen
------------------------------------------------------------

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  Kleingewässer und ihre Umgebung sollen als Reproduktions- und Lebensräume für Amphibien gepflegt, entwickelt und wiederhergestellt werden, indem insbesondere
    1.  in ausreichender Größe unbeschattete Wasserflächen durch Gehölz- und Schlammentnahmen freigehalten oder wiederhergestellt werden,
    2.  der Landlebensraum am Gewässerrand in einem Streifen von 20 Metern amphibiengerecht genutzt wird, insbesondere im Zeitraum von Juli bis August eines jeden Jahres keine Mahd durchgeführt wird und bei einer Weidenutzung ausreichend schützende Vegetation verbleibt,
    3.  an den Gewässern Winterlebensräume, insbesondere Gehölzflächen mit Totholz, Laub-, Reisig- und Lesesteinhaufen, entwickelt werden;
2.  bei der Bewirtschaftung der Wälder sollen
    1.  Nadelholzkulturen in naturnahe Mischwälder überführt werden,
    2.  Bodenbearbeitungen nur zur Unterstützung der angestrebten Verjüngung kleinflächig und ohne Eingriff in den Mineralboden erfolgen,
    3.  nicht heimische Gehölze aus dem lebenden Bestand entfernt werden;
3.  die im Gebiet liegenden Ackerflächen in der Gemarkung Fürstenwerder, Flur 7, auf den Flurstücken 584, 602 bis 604, 609 bis 612, 614/2, 616 und 617 sollen dauerhaft in Grünland umgewandelt oder stillgelegt werden;
4.  der Wasserhaushalt des Gebietes soll in Abstimmung mit den zuständigen Wasserbehörden und den Gewässerunterhaltungspflichtigen durch geeignete Maßnahmen, wie Rückbau des Entwässerungssystems, Reduzierung der Oberflächenabflüsse durch angepasste Bodennutzung, Renaturierung begradigter Fließgewässerabschnitte und Sohlanhebung, verbessert werden.

§ 7  
Befreiungen
-----------------

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

§ 8  
Ordnungswidrigkeiten
--------------------------

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

§ 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen
--------------------------------------------------------------------------------

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 17 und 18 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 29 Absatz 3 und § 30 des Bundesnaturschutzgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes), über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 54 Absatz 7 des Bundesnaturschutzgesetzes) unberührt.

§ 10  
Geltendmachen von Rechtsmängeln
--------------------------------------

Eine Verletzung der in den §§ 9 und 10 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
 Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

§ 11  
Inkrafttreten
--------------------

§ 5 Absatz 1 Nummer 1 Buchstabe a, b und c tritt am 1.
Januar 2014 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 11.
Juni 2013

  

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

Anlage 1  
(zu § 2 Absatz 1)
----------------------------

  

![Das Naturschutzgebiet "Damerower Wald, Schlepkower Wald und Jagenbruch" hat eine Größe von rund 671 Hektar. Es umfasst Flächen der Gemarkungen Wolfshagen, Ottenhagen und Schlepkow in der Gemeinde Uckerland sowie Flächen der Gemarkungen Fürstenwerder, Damerow und Kraatz in der Gemeinde Nordwestuckemark. ](/br2/sixcms/media.php/69/image002.jpg "Das Naturschutzgebiet "Damerower Wald, Schlepkower Wald und Jagenbruch" hat eine Größe von rund 671 Hektar. Es umfasst Flächen der Gemarkungen Wolfshagen, Ottenhagen und Schlepkow in der Gemeinde Uckerland sowie Flächen der Gemarkungen Fürstenwerder, Damerow und Kraatz in der Gemeinde Nordwestuckemark. ")

  

Anlage 2  
(zu § 2 Absatz 2)
----------------------------

### 1.
Übersichtskarte Maßstab 1 : 20 000

**Titel:**

Übersichtskarte zur Verordnung über das Naturschutzgebiet „Damerower Wald, Schlepkower Wald und Jagenbruch“, Blattnummer 01, unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV), am 2.
April 2013

### 2.
Topografische Karten im Maßstab 1 : 10 000

**Titel:**

Topografische Karte zur Verordnung über das Naturschutzgebiet „Damerower Wald, Schlepkower Wald und Jagenbruch“

Blattnummer

Unterzeichnung

01

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV), am 2.
April 2013

02

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
April 2013

03

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
April 2013

### 3.
Liegenschaftskarten im Maßstab 1 : 2 500

**Titel:**

Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Damerower Wald, Schlepkower Wald und Jagenbruch“

Blattnummer

Gemarkung

Flur

Unterzeichnung

01

Wolfshagen

1, 4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
April 2013

02

Wolfshagen

1, 4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
April 2013

03

Wolfshagen  
  
Ottenhagen

1, 3, 4  
  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
 April 2013

04

Wolfshagen

1, 3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
 April 2013

05

Wolfshagen  
  
Ottenhagen  
  
Fürstenwerder

3  
  
1  
  
7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
April 2013

06

Ottenhagen  
  
Schlepkow

1  
  
2, 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
 April 2013

07

Schlepkow

2, 3, 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
 April 2013

08

Fürstenwerder

7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
 April 2013

09

Fürstenwerde  
  
Damerow  
  
Ottenhagen

7  
  
1  
  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
 April 2013

10

Damerow  
  
Ottenhagen  
  
Schlepkow  
  
Kraatz

1  
  
1  
  
5  
  
3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
 April 2013

11

Damerow  
  
Kraatz  
  
Schlepkow

1  
  
3  
  
3, 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
 April 2013

12

Fürstenwerder

7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
 April 2013

13

Damerow  
  
Fürstenwerder

1  
  
7, 8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
 April 2013

14

Damerow  
  
Kraatz  
  
Fürstenwerder

1  
  
2, 3  
  
8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
April 2013

Anlage 3  
(zu § 2 Absatz 2)
----------------------------

### Flurstücksliste zur Verordnung über das Naturschutzgebiet „Damerower Wald, Schlepkower Wald und Jagenbruch“

**Landkreis:** Uckermark

Gemeinde

Gemarkung

Flur

Flurstücke

Uckerland

Ottenhagen

1

19/2 (anteilig), 20/2 (anteilig), 23/1 (anteilig), 24, 25, 26 (anteilig), 27/2 (anteilig), 28/2, 38/2, 43, 47, 48, 49/1, 57/2, 63/1, 65, 69/1, 70, 74/1, 75/1, 77 bis 79, 83, 84/1, 85 bis 92, 94/1, 98 bis 111, 113/1, 114 bis 126, 127/2, 128, 129, 150 (anteilig), 153 (anteilig), 154 bis 156, 157 (anteilig), 158 (anteilig), 159 bis 162;

 

Wolfshagen

1

127 (anteilig), 129 (anteilig), 152 bis 156, 158/1, 160 bis 167, 169/1, 173/1 (anteilig), 174 bis 176, 181 bis 186, 193/2 (anteilig);

 

 

3

1, 2/1, 17, 18 bis 110;

 

 

4

23 (anteilig), 25, 26/6, 27, 29/2 (anteilig), 30, 33/1, 38, 39;

 

Schlepkow

2

53 (anteilig), 57 (anteilig), 61 bis 66 (jeweils anteilig), 67, 68 bis 70 (jeweils anteilig), 72 (anteilig), 78 (anteilig), 113 (anteilig), 114, 115, 127 (anteilig), 129, 130 (anteilig), 131 bis 140, 143 (anteilig), 145 (anteilig), 147;

 

 

3

1 (anteilig), 2 bis 5, 7, 8, 40, 41, 42 (anteilig), 49 (anteilig), 50 bis 52, 54 bis 65, 68, 71 bis 74;

 

 

5

1 bis 12, 13 (anteilig), 14 bis 45, 46 (anteilig), 47 bis 99;

Nordwest-uckermark

Fürstenwerder

7

453/1, 454 (anteilig), 455 bis 467, 469 bis 477, 478 (anteilig), 479/1, 479/2 (anteilig), 480, 484 bis 488, 489/1, 489/2, 490/1, 490/2, 491(anteilig), 492 bis 498, 499 (anteilig), 500 (anteilig), 541 (anteilig), 551 (anteilig), 556 bis 558 (jeweils anteilig), 574 bis 575 (jeweils anteilig), 576 bis 580, 581/1, 581/2, 582 bis 587, 599 bis 613, 614/2 (anteilig), 616 bis 620, 621 (anteilig), 625/2 (anteilig), 626;

 

 

8

8 (anteilig), 9, 10, 11 (anteilig), 13 (anteilig), 14, 16, 32 (anteilig), 38/2 (anteilig), 56 (anteilig), 57;

 

Damerow

1

1 (anteilig), 2 bis 52, 56 bis 106, 114 bis 115 (jeweils anteilig), 120 (anteilig), 167 (anteilig), 178 (anteilig), 185 (anteilig), 199 (anteilig), 200 bis 239, 240 (an ttwidth="10%"eilig), 243 bis 245, 246 (anteilig), 247, 257 (anteilig), 260 bis 266, 268 bis 314, 321 bis 330, 335 bis 343, 344 (anteilig), 345 bis 364;

 

Kraatz

2

48 (anteilig), 69 (anteilig), 71 (anteilig), 72 bis 87, 89 (anteilig), 105 (anteilig), 106;

 

 

3

7 (anteilig), 8 bis 10, 11 (anteilig), 12 (anteilig), 13 bis 15, 18 (anteilig), 22 (anteilig).

### Flächen der Zone 1:

Gemeinde

Gemarkung

Flur

Flurstücke

Nordwest-uckermark

Damerow

1

2 bis 47, 56 bis 106;

Uckerland

Ottenhagen

1

84/1, 86 bis 92, 98 bis 111, 116 bis 126, 128, 129 (anteilig), 154, 159, 160;

 

Wolfshagen

3

106 bis 109, 110 (anteilig).

### Flächen der Zone 2:

Gemeinde

Gemarkung

Flur

Flurstücke

Uckerland

Ottenhagen

1

25, 26 (anteilig), 43, 47, 48, 49/1, 57/2, 69/1 (anteilig), 70, 74/1, 75/1, 77, 78, 114, 115 (anteilig), 150 (anteilig), 153 (anteilig), 155 (anteilig), 156, 157 (anteilig), 158 (anteilig), 161, 162;

 

Wolfshagen

1

154 (anteilig), 156, 158/1, 160 bis 167, 169/1, 173/1 (anteilig), 174 bis 176, 181 bis 186, 193/2 (anteilig);

 

 

3

1, 2/1, 17 (anteilig);

 

 

4

23 (anteilig), 25, 26/6, 27, 29/2 (anteilig), 30, 33/1, 39;

 

Schlepkow

3

2 bis 5, 40 (anteilig), 73, 74;

Nordwest-uckermark

Fürstenwerder

7

453/1, 454 (anteilig), 455 bis 467, 469 bis 477, 478 (anteilig), 479/1, 479/2 (anteilig), 480, 484 bis 488, 489/1, 489/2, 490/1, 490/2, 491 (anteilig), 492 bis 498, 499 (anteilig), 500 (anteilig), 541 (anteilig), 551 (anteilig), 556 bis 558 (jeweils anteilig);

 

 

8

8 (anteilig), 9, 10, 11 (anteilig), 13 (anteilig), 14, 16, 32 (anteilig), 38/2 (anteilig), 56 (anteilig), 57;

 

Damerow

1

167 (anteilig), 178 (anteilig), 185 (anteilig), 199 (anteilig), 200 bis 230, 232 bis 239, 240 (anteilig), 243 bis 245, 246 (anteilig), 247, 257 (anteilig), 260 bis 266, 280;

 

Kraatz

2

48 (anteilig), 69 (anteilig), 71 (anteilig), 72 bis 87, 89 (anteilig), 105 (anteilig), 106;

 

 

3

7 (anteilig), 8 bis 10, 11 (anteilig), 12 (anteilig), 13 bis 15, 18 (anteilig), 22 (anteilig).

### Flächen der Zone 3:

Gemeinde

Gemarkung

Flur

Flurstücke

Uckerland

Ottenhagen

1

63/1, 65, 69/1 (anteilig), 79, 83, 85, 94/1, 113/1, 115 (anteilig), 127/2, 129 (anteilig).