## Verordnung über die Polizeibeiräte im Land Brandenburg  (Brandenburgische Polizeibeiräteverordnung - BbgPolBeiratV)

Auf Grund des § 85 des Brandenburgischen Polizeigesetzes vom 19.
März 1996 (GVBl.
I S. 74), der durch Artikel 2 des Gesetzes vom 20.
Dezember 2010 (GVBl.
I Nr. 42) geändert worden ist, verordnet der Minister des Innern:

### § 1  
**Polizeibeiräte**

(1) Bei den Polizeidirektionen (Polizeibezirke im Sinne des § 84 Absatz 1 des Brandenburgischen Polizeigesetzes) werden aus den von den Kreistagen und den Stadtverordnetenversammlungen der kreisfreien Städte gewählten Mitgliedern Polizeibeiräte gebildet; dabei entsenden

1.  die Landkreise Oberhavel, Ostprignitz-Ruppin und Prignitz Mitglieder in den Polizeibeirat der Polizeidirektion Nord,
2.  die Landkreise Uckermark, Barnim, Märkisch-Oderland, Oder-Spree und die kreisfreie Stadt Frankfurt (Oder) Mitglieder in den Polizeibeirat der Polizeidirektion Ost,
    
3.  die Landkreise Dahme-Spreewald, Spree-Neiße, Oberspreewald-Lausitz, Elbe-Elster und die kreisfreie Stadt Cottbus Mitglieder in den Polizeibeirat der Polizeidirektion Süd,
4.  die Landkreise Teltow-Fläming, Potsdam-Mittelmark, Havelland und die kreisfreien Städte Brandenburg an der Havel und Potsdam Mitglieder in den Polizeibeirat der Polizeidirektion West.

(2)Die Anzahl der von den Kreistagen und den Stadtverordnetenversammlungen der kreisfreien Städte zu wählenden Mitglieder des Polizeibeirates beträgt:

Einwohnerzahl

Zahl der Mitglieder

bis zu 100 000

1

mehr als 100 000 bis zu 150 000

2

mehr als 150 000

3

(3)Für die Mitglieder ist jeweils eine Stellvertreterin oder ein Stellvertreter zu wählen.

(4)Die Polizeibeiräte bestimmen je zwei Mitglieder aus den mit der gewerblichen Schifffahrt verbundenen Kreisen der Bevölkerung.

### § 2  
Aufgaben der Polizeibeiräte

(1)Der Polizeibeirat ist Bindeglied zwischen Bevölkerung, kommunaler Gebietskörperschaft und Polizei.
Er soll das vertrauensvolle Verhältnis zwischen ihnen fördern, die Tätigkeit der Polizei unterstützen sowie Anregungen und Wünsche der Bevölkerung an die Polizei herantragen.

(2)Der Polizeibeirat berät mit der Leiterin oder dem Leiter der Polizeidirektion polizeiliche Angelegenheiten, die für die Bevölkerung oder für die kommunalen Gebietskörperschaften von Bedeutung sind.
Dazu gehören auch an die Polizei gerichtete Beschwerden und Angelegenheiten, deren Bedeutung über den Einzelfall hinausgeht oder an deren Behandlung ein öffentliches Interesse besteht.

(3)Die Leiterin oder der Leiter der Polizeidirektion berichtet im Rahmen der Beratungen des Polizeibeirates zu den Tagesordnungspunkten und beurteilt den Stand der öffentlichen Sicherheit und Ordnung im Zuständigkeitsbereich der Polizeidirektion aus polizeilicher Sicht.

(4)Der Polizeibeirat ist vor der Planung baulicher Maßnahmen für die Polizei, vor der Auflösung und Einrichtung von Polizeidienststellen sowie vor der Veränderung ihrer Dienstbezirke zu hören.

### § 3  
Sitzungen des Polizeibeirates, Vorsitz, Geschäftsordnung und Geschäftsführung

(1)Der Polizeibeirat wählt aus seiner Mitte eine Vorsitzende oder einen Vorsitzenden, deren Stellvertreterin oder dessen Stellvertreter und eine Schriftführerin oder einen Schriftführer.

(2)Der Polizeibeirat gibt sich eine Geschäftsordnung.

(3)Der Polizeibeirat wird von seiner Vorsitzenden oder seinem Vorsitzenden unter Bekanntgabe der Tagesordnung einberufen.
Er ist unverzüglich einzuberufen, wenn ein Viertel seiner Mitglieder es verlangt.
Dies gilt auch für den Antrag, eine bestimmte Angelegenheit auf die Tagesordnung zu setzen.

(4)Die Leiterin oder der Leiter der Polizeidirektion unterrichtet den Polizeibeirat so früh wie möglich über das Vorliegen von Angelegenheiten im Sinne des § 2 Absatz 2 und 4.
Auf Verlangen des Polizeibeirates ist die Leiterin oder der Leiter der Polizeidirektion verpflichtet, zu diesen Angelegenheiten zu berichten.

(5)Die Sitzungen des Polizeibeirates sind in der Regel nicht öffentlich.
Auf Beschluss des Polizeibeirates können Sitzungen öffentlich durchgeführt werden.

(6)Gäste können an den Sitzungen der Polizeibeiräte teilnehmen, wenn der Beirat ihre Teilnahme für erforderlich hält.

(7)Die Geschäfte des Polizeibeirates werden von der Polizeidirektion wahrgenommen.

### § 4  
Wahl der Polizeibeiräte

(1)Die Mitglieder der Polizeibeiräte sind innerhalb von drei Monaten nach den Wahlen zu den Vertretungen der Landkreise und kreisfreien Städte neu zu wählen.

(2)Für die Abberufung und Bestellung von Mitgliedern finden die §§ 40 und 41 der Kommunalverfassung des Landes Brandenburg entsprechende Anwendung.
Die für die mit der gewerblichen Schifffahrt verbundenen Kreise der Bevölkerung bestimmten Mitglieder können von den Polizeibeiräten abberufen werden.

(3)Beamtinnen und Beamte sowie Tarifbeschäftigte der Polizei des Landes Brandenburg können nicht Mitglieder eines Polizeibeirates sein.

(4)Die Mitglieder des Polizeibeirates dürfen an der Übernahme und Ausübung ihrer Tätigkeit nicht gehindert oder hierdurch in ihrem Amt oder Arbeitsverhältnis benachteiligt werden.

(5)Bis zur Wahl der neuen Polizeibeiräte üben die Mitglieder der bisherigen Polizeibeiräte ihre Tätigkeit weiter aus.
Dabei entsenden

1.  in den Polizeibeirat bei der Polizeidirektion Nord
    1.  der Landkreis Oberhavel drei Mitglieder,
    2.  der Landkreis Ostprignitz-Ruppin zwei Mitglieder,
    3.  der Landkreis Prignitz zwei Mitglieder;
2.  in den Polizeibeirat bei der Polizeidirektion Ost
    
    1.  der Landkreis Uckermark zwei Mitglieder,
    2.  der Landkreis Barnim zwei Mitglieder,
    3.  der Landkreis Märkisch-Oderland drei Mitglieder,
    4.  der Landkreis Oder-Spree drei Mitglieder,
    5.  die kreisfreie Stadt Frankfurt (Oder) ein Mitglied;
3.  in den Polizeibeirat bei der Polizeidirektion Süd
    
    1.  der Landkreis Dahme-Spreewald zwei Mitglieder,
    2.  der Landkreis Spree-Neiße zwei Mitglieder,
    3.  der Landkreis Oberspreewald-Lausitz zwei Mitglieder,
    4.  der Landkreis Elbe-Elster zwei Mitglieder,
    5.  die kreisfreie Stadt Cottbus ein Mitglied;
4.  in den Polizeibeirat bei der Polizeidirektion West
    
    1.  der Landkreis Teltow-Fläming drei Mitglieder,
    2.  der Landkreis Potsdam-Mittelmark drei Mitglieder,
    3.  der Landkreis Havelland drei Mitglieder,
    4.  die kreisfreie Stadt Brandenburg an der Havel zwei Mitglieder,
    5.  die Landeshauptstadt Potsdam drei Mitglieder.

Die bisher für die mit der gewerblichen Schifffahrt verbundenen Kreise der Bevölkerung bestimmten Mitglieder gehören dem Polizeibeirat der Polizeidirektion West an; die Polizeibeiräte der Polizeidirektionen Nord, Ost und Süd bestellen jeweils drei Vertreter aus den mit der gewerblichen Schifffahrt verbundenen Kreisen der Bevölkerung.

(6)Mitglieder von Polizeibeiräten bei Polizeidirektionen, deren Zuständigkeitsbereich sich ändert, treten dabei bis zur Neuwahl nach Absatz 1 zu dem Polizeibeirat der Polizeidirektion über, der der Landkreis oder die kreisfreie Stadt künftig angehört.

### § 5  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 25.
Juni 2012

Der Minister des Innern

Dr.
Dietmar Woidke