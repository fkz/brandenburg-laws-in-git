## Verordnung zur Regelung der Zuständigkeiten und der Ermächtigung zum Erlass von Rechtsverordnungen auf dem Gebiet des Pflanzenschutzes (Pflanzenschutzzuständigkeitsverordnung - PflSchZV)

Auf Grund des §6 Absatz 3 Satz 2, des § 9 Absatz 7 Satz 2, des § 10 Satz 3, des § 14 Absatz 4 Satz 2, des § 16 Absatz 5 Satz 3, des § 24 Absatz 1 Satz 3, des § 29 Absatz 2 Satz 2 des Pflanzenschutzgesetzes vom 6.
Februar 2012 (BGBl.
I S.
148, 1281) sowie des § 9 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S.
186) und des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl.
I S.
602) verordnet die Landesregierung:

§ 1  
Zuständigkeiten
---------------------

(1) Zuständige Behörde im Sinne des § 59 Absatz 1 des Pflanzenschutzgesetzes ist das Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung.

(2) Das Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung ist auch zuständig für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 68 des Pflanzenschutzgesetzes im Rahmen der Zuständigkeit nach Absatz 1.

§ 2  
Übertragung von Verordnungsermächtigungen
-----------------------------------------------

Die Ermächtigung der Landesregierung zum Erlass von Rechtsverordnungen nach § 6 Absatz 3 Satz 1, § 9 Absatz 7 Satz 1, § 10 Satz 2, § 14 Absatz 4 Satz 1, § 16 Absatz 5 Satz 1 und 2, § 24 Absatz 1 Satz 2 und § 29 Absatz 2 Satz 1 des Pflanzenschutzgesetzes wird auf das für Landwirtschaft zuständige Mitglied der Landesregierung übertragen.
Dieses kann seine Befugnis zum Erlass von Rechtsverordnungen nach § 6 Absatz 3 Satz 2 und § 16 Absatz 5 Satz 3 des Pflanzenschutzgesetzes durch Rechtsverordnung auf nachgeordnete oder seiner Aufsicht unterstehende Behörden übertragen.

§ 3  
Inkrafttreten, Außerkrafttreten
-------------------------------------

(1) § 1 tritt mit Wirkung vom 14.
Februar 2012 in Kraft.
Gleichzeitig tritt die Pflanzenschutzzuständigkeitsverordnung vom 30.
Januar 1993 (GVBl.
II S.
51), die zuletzt durch die Verordnung vom 16.
Februar 2001 (GVBl.
II S.
40) geändert worden ist, außer Kraft.

(2) Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.
 Gleichzeitig tritt die Verordnung zur Übertragung von Ermächtigungen nach dem Pflanzenschutzgesetz vom 16.
Februar 2001 (GVBl.
II S.
40) außer Kraft.

Potsdam, den 11.
Februar 2014

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister für Infrastruktur und Landwirtschaft

Jörg Vogelsänger