## Verordnung zur Bestimmung der sachlichen Zuständigkeit der Personalausweisbehörden  (Personalausweiszuständigkeitsverordnung - PAuswZustV)

Auf Grund des § 7 Absatz 1 des Personalausweisgesetzes vom 18.
Juni 2009 (BGBl.
I S. 1346) in Verbindung mit § 9 Absatz 2 und § 16 Absatz 2 Satz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186) verordnet die Landesregierung:

#### § 1  
Zuständigkeit

Personalausweisbehörden sind die kreisfreien Städte, die Ämter und die amtsfreien Gemeinden als örtliche Ordnungsbehörden.

#### § 2   
Inkrafttreten

Diese Verordnung tritt am 1.
 November 2010 in Kraft.

Potsdam, den 19.
Oktober 2010

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident  
Matthias Platzeck

Der Minister des Innern  
Dr.
Dietmar Woidke