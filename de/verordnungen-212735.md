## Verordnung über das Naturschutzgebiet „Schwemmpfuhl“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2 und § 21 Absatz 1 Satz 2 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1  
**Erklärung zum Schutzgebiet**

Die in § 2 näher bezeichnete Fläche im Landkreis Uckermark wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Schwemmpfuhl“.

§ 2  
**Schutzgegenstand**

(1) Das Naturschutzgebiet hat eine Größe von rund 126 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Gemeinde:**

**Gemarkung:**

**Flur:**

Gerswalde

Gerswalde

1;

Gerswalde

Kaakstedt

1, 2.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte topografische Karte im Maßstab 1 : 10 000 ermöglicht die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 01 bis 03 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Innerhalb des Naturschutzgebietes ist eine Zone 1 mit weiter gehenden Regelungen der Grünlandnutzung festgelegt.
Die Zone 1 umfasst rund 37 Hektar und liegt in folgenden Fluren:

**Gemeinde:**

**Gemarkung:**

**Flur:**

Gerswalde

Gerswalde

1;

Gerswalde

Kaakstedt

1.

Die Grenze der Zone 1 ist in der in Anlage 2 Nummer 1 genannten topografischen Karte sowie in den in Anlage 2 Nummer 2 genannten Liegenschaftskarten mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Uckermark, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

§ 3  
**Schutzzweck**

(1) Schutzzweck des Naturschutzgebietes, das ein Mosaik verschiedener Offenlandbiotope mit eingestreuten Kleingewässern und Gehölzstrukturen in einer reliefreichen jungpleistozänen Grund- und Endmoränenlandschaft umfasst, ist

1.  die Erhaltung, Wiederherstellung und Entwicklung des Gebietes als Lebensraum wild lebender Pflanzengesellschaften, insbesondere der Trocken- und Halbtrockenrasen, des frischen bis feuchten Grünlandes, der feuchten und trockenwarmen Hochstaudenfluren, der Kleingewässer, der Schwimmblattfluren, der Röhrichte, der Feldgehölze, der Laubgebüsche und der naturnahen Wälder;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 10 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Wiesenschlüsselblume (Primula veris), Kartäusernelke (Dianthus carthusianorum), Ähriger Blauweiderich (Veronica spicata), Gemeine Grasnelke (Armeria maritima ssp.
    elongata), Sandstrohblume (Helichrysum arenarium), Körnchensteinbrech (Saxifraga granulata) und Wasserschwertlilie (Iris pseudacorus) sowie Strauchflechten der Gattung Cladonia;
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Säugetiere, Vögel, Amphibien, Reptilien, Fische und Insekten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Wasserfledermaus (Myotis daubentonii), Neuntöter (Lanius collurio), Bekassine (Gallinago gallinago), Rothalstaucher (Podiceps griseigena), Ringelnatter (Natrix natrix), Laubfrosch (Hyla arborea), Knoblauchkröte (Pelobates fuscus) und Schwalbenschwanz (Papilio machaon);
4.  die Erhaltung der unter Nummer 1 aufgeführten Lebensräume zur Umweltbeobachtung und wissenschaftlichen Untersuchung ökologischer Zusammenhänge;
5.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit der von vielfältigen Grünlandflächen, gliedernden Hecken und Feldgehölzen, Lesesteinhaufen sowie unterschiedlich ausgebildeten Kleingewässern geprägten Hügellandschaft;
6.  die Erhaltung und Entwicklung des Gebietes als Trittstein im regionalen Biotopverbund der Trockenrasen westlich der Uckerseen.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teiles des Gebietes von gemeinschaftlicher Bedeutung „Schwemmpfuhl“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinen Vorkommen von

1.  Mageren Flachland-Mähwiesen (Alopecurus pratensis, Sanguisorba officinalis) und Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions als Biotope von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG);
2.  Trockenen, kalkreichen Sandrasen und Subpannonischen Steppentrockenrasen (Festucetalia vallesiacae) als prioritäre Biotope („prioritäre Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG);
3.  Fischotter (Lutra lutra), Rotbauchunke (Bombina bombina) und Kamm-Molch (Triturus cristatus) als Tierarten von gemeinschaftlichem Interesse (im Sinne des Anhangs II der Richtlinie 92/43/EWG) einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

§ 4  
**Verbote**

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten außerhalb von Gewässerufern und Röhrichtflächen zum Zweck der Erholung sowie des nichtgewerblichen Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 9 nach dem 30.
    Juni eines jeden Jahres sowie das Betreten während winterlicher Frostperioden für Freizeitaktivitäten wie das Schlittenfahren;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 51 des Brandenburgischen Naturschutzgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
13.  Hunde frei laufen zu lassen;
14.  Be- und Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
15.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (zum Beispiel solche aus Abwasser, Klärschlamm und aus Bioabfällen) zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
16.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
17.  Tiere zu füttern oder Futter bereitzustellen;
18.  Tiere auszusetzen oder Pflanzen anzusiedeln;
19.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
20.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
21.  Pflanzenschutzmittel jeder Art anzuwenden;
22.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen, wobei bei Narbenschäden eine umbruchlose Nachsaat zulässig ist.

§ 5  
**Zulässige Handlungen**

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  Grünland außerhalb der Zone 1 als Wiese oder Weide mit einer Besatzdichte von maximal 1,4 Raufutter verwertenden Großvieheinheiten (RGV) pro Hektar im Jahresmittel genutzt wird und § 4 Absatz 2 Nummer 15 gilt,
    2.  Grünland in der Zone 1 als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und Sekundärrohstoffdünger wie zum Beispiel Abwasser, Klärschlamm und Bioabfälle einzusetzen,
    3.  die Nutzung der Grünlandflächen in der Zone 1 vor dem 16.
        Juni eines Jahres unzulässig ist,  
        
    4.  Gehölze in geeigneter Weise gegen Verbiss und sonstige Beschädigungen sowie Ränder von Gewässern wirksam gegen Trittschäden von weidenden Nutztieren geschützt werden,
    5.  auf Grünland § 4 Absatz 2 Nummer 21 und 22 gilt;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  nur Arten der potenziell natürlichen Buchenwaldgesellschaften mittlerer bis armer Standorte (Perlgras-, Flattergras- und Hainrispengras-Hainbuchen-Buchenwald) eingebracht werden dürfen, wobei nur heimische Baumarten in gesellschaftstypischen Anteilen unter Ausschluss eingebürgerter Arten zu verwenden sind,
    2.  mindestens fünf Stämme je Hektar mit einem Durchmesser von mehr als 40 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum natürlichen Absterben und Zerfall aus der Nutzung genommen sein müssen,
    3.  stehendes Totholz mit mehr als 35 Zentimetern Stammdurchmesser in 1,30 Meter Höhe über dem Stammfuß nicht gefällt wird und liegendes Totholz an Ort und Stelle verbleibt,
    4.  das Befahren des Waldes nur auf Waldwegen und Rückegassen erfolgt,
    5.  § 4 Absatz 2 Nummer 21 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  § 4 Absatz 2 Nummer 17 gilt,
    2.  der Fischbesatz nur mit gebietsheimischen Arten erfolgt; § 13 der Brandenburgischen Fischereiordnung bleibt unberührt,
    3.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung von Fischottern weitgehend ausgeschlossen ist;
4.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass § 4 Absatz 2 Nummer 18 gilt und die Angelfischerei vom Ufer aus nur außerhalb des Röhrichtgürtels betrieben wird;
5.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Jagd in der Zeit vom 1.
        März bis zum 30.
        Juni eines Jahres ausschließlich vom Ansitz aus erfolgt,
        
        bb)
        
        die Fallenjagd mit Lebendfallen erfolgt,
        
    2.  die Errichtung jagdlicher Einrichtungen zur Ansitzjagd,
    3.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und der in § 3 Absatz 2 Nummer 1 und 2 genannten Lebensraumtypen.
    
    Im Übrigen bleiben Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und die Anlage und Unterhaltung von Wildäckern unzulässig.
    Jagdrechtliche Regelungen nach § 41 des Brandenburgischen Jagdgesetzes bleiben unberührt;
6.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter Nummer 8 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
7.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
8.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
9.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch jeweils nach dem 30.
    Juni eines jeden Jahres;
10.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
11.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundesbodenschutzgesetz sowie Maßnahmen der Munitionsräumung nach Anzeige gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes bei der unteren Naturschutzbehörde;
12.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
13.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen; darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl.
    S. 1734) an Straßen und Wegen freigestellt;
14.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
 Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

§ 6  
**Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen**

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  die Trocken- und Halbtrockenrasen sollen nach Möglichkeit mit Schafen und Ziegen beweidet werden; die Beweidung soll entsprechend einem regelmäßig fortzuschreibenden, mit der unteren Naturschutzbehörde abgestimmten Weideplan durchgeführt werden;
2.  Kleingewässer sollen in ihrer Habitatfunktion für Rotbauchunke, Kamm-Molch und Laubfrosch erhalten und verbessert werden; dazu sollen unter anderem besonnte Wasserflächen erhalten oder wiederhergestellt werden;
3.  der Erhalt der Kleingewässer und die Regeneration der Moorstandorte soll durch abflussmindernde Maßnahmen gesichert werden;
4.  die Nutzung des Grünlandes soll mosaikartig erfolgen;
5.  bei erforderlichen Nachsaaten von Grünland soll nach Möglichkeit Saatgut standorttypischer, im Naturraum heimischer Arten und Sorten verwendet werden;
6.  bei der Anpflanzung von Flurgehölzen und Hecken sollen landschaftstypische und gebietsheimische Arten wie Stiel- und Trauben-Eiche, Rotbuche, Hainbuche, Winter-Linde, Eberesche, Schlehe, Eingriffeliger Weißdorn, Kreuzdorn, Hunds- und Weinrose, an Feuchtstandorten auch Schwarz-Erle, Bruch- und Grau-Weide verwendet werden;
7.  es sollen geeignete Einrichtungen zur Besucherlenkung und -information geschaffen werden.

§ 7  
**Befreiungen**

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

§ 8  
**Ordnungswidrigkeiten**

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

§ 9  
**Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen**

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere die §§ 31 bis 33 und § 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

§ 10  
**Geltendmachen von Rechtsmängeln**

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

§ 11  
**Inkrafttreten**

§ 5 Absatz 1 Nummer 1 Buchstabe a bis c tritt am 1.
Juli 2013 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 30.
April 2013

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

 

**Anlage 1  
**(zu § 2 Absatz 1)

![Das Naturschutzgebiet Schwemmpfuhl im Landkreis Uckermark hat eine Größe von rund 126 Hektar und umfasst Flächen in den Gemarkungen Gerswalde, Flur 1, und Kaakstedt, Flure 1 und 2, in der Gemeinde Gerswalde. ](/br2/sixcms/media.php/69/Nr.%2035-1.GIF "Das Naturschutzgebiet Schwemmpfuhl im Landkreis Uckermark hat eine Größe von rund 126 Hektar und umfasst Flächen in den Gemarkungen Gerswalde, Flur 1, und Kaakstedt, Flure 1 und 2, in der Gemeinde Gerswalde. ")

 

**Anlage 2  
**(zu § 2 Absatz 2)

**1.
Topografische Karte im Maßstab 1 : 10 000**

**Titel:**

Topografische Karte zur Verordnung über das Naturschutzgebiet „Schwemmpfuhl“

**Blattnummer**

**Unterzeichnung**

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV), am 2.
April 2013

**2.
Liegenschaftskarten im Maßstab 1 : 2 500**

**Titel:**

Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Schwemmpfuhl“

**Blattnummer**

**Gemarkung**

**Flur**

**Unterzeichnung**

01

Gerswalde  
  
Kaakstedt

1

1, 2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
 April 2013

02

Kaakstedt

1, 2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
April 2013

03

Gerswalde  
  
Kaakstedt

1

2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
April 2013

**Anlage 3** (zu § 2 Absatz 2)

**Flurstücksliste zur Verordnung über das Naturschutzgebiet „Schwemmpfuhl“**

**Landkreis:** Uckermark

Gemeinde

Gemarkung

Flur

Flurstück

Gerswalde

Gerswalde

1

1 anteilig, 2 bis 11, 12 anteilig, 14 anteilig, 15 anteilig, 16 bis 19, 20 anteilig, 21, 22 anteilig, 23 anteilig, 24 bis 26, 27 anteilig, 28/1 anteilig, 28/2 anteilig, 28/3, 29, 30 anteilig, 31 anteilig, 34 anteilig, 37 bis 40 jeweils anteilig, 82 anteilig, 83 anteilig, 84 bis 86, 87 anteilig, 88 anteilig, 89, 90/1, 90/2, 91 bis 93, 95 bis 105, 106 anteilig, 112/2 anteilig, 113, 114 bis 118 jeweils anteilig, 153 anteilig, 268 bis 270, 271 anteilig;

Gerswalde

Kaakstedt

1

19, 20 bis 22 jeweils anteilig, 23, 24 anteilig;

Gerswalde

Kaakstedt

2

2 bis 6 jeweils anteilig, 8 bis 9 jeweils anteilig.

**Flächen der Zone 1:**

**Gemeinde**

Gemarkung

Flur

Flurstück

Gerswalde

Gerswalde

1

4 anteilig, 5 bis 9, 11 anteilig, 12 anteilig, 104 anteilig, 106 anteilig, 113 bis 115 jeweils anteilig, 270;

Gerswalde

Kaakstedt

1

19, 20 bis 22 jeweils anteilig, 24 anteilig.