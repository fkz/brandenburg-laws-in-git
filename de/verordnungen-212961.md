## Verordnung über den Braunkohlenplan Tagebau Welzow-Süd, Weiterführung in den räumlichen Teilabschnitt II und Änderung im räumlichen Teilabschnitt I (Brandenburgischer Teil)

Auf Grund des § 19 des Gesetzes zur Regionalplanung und zur Braunkohlen- und Sanierungsplanung in der Fassung der Bekanntmachung vom 12.
 Dezember 2002 (GVBl.
2003 I S. 2), der zuletzt durch Artikel 2 des Gesetzes vom 21.
September 2011 (GVBl.
I Nr. 21) geändert worden ist, verordnet die Landesregierung:

§ 1 
----

Der in den Anlagen 1 und 2 zu dieser Verordnung veröffentlichte Braunkohlenplan Tagebau Welzow-Süd, Weiterführung in den räumlichen Teilabschnitt II und Änderung im räumlichen Teilabschnitt I (Brandenburgischer Teil), wird hiermit erlassen.
Die Anlagen sind Bestandteil dieser Verordnung.

§ 2 
----

Der Braunkohlenplan Tagebau Welzow-Süd, Weiterführung in den räumlichen Teilabschnitt II und Änderung im räumlichen Teilabschnitt I (Brandenburgischer Teil) wird mit seiner Begründung bei der Gemeinsamen Landesplanungsabteilung Berlin-Brandenburg und bei den Landkreisen Spree-Neiße und Oberspreewald-Lausitz zur Einsichtnahme für jedermann niedergelegt und kann zusätzlich im Internet über das Webportal der Gemeinsamen Landesplanungsabteilung Berlin-Brandenburg eingesehen werden.

§ 3 
----

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 21.
August 2014

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Hinweis:

Eine Verletzung der für die Braunkohlenplanung geltenden Verfahrens- und Formvorschriften bei der Aufstellung des Braunkohlenplans Welzow-Süd, Weiterführung in den räumlichen Teilabschnitt II und Änderung im räumlichen Teilabschnitt I (Brandenburgischer Teil) wird unbeachtlich, wenn sie nicht innerhalb eines Jahres seit Verkündung der Verordnung geltend gemacht worden ist.

### Anlagen

1

[Braunkohlenplan Welzow-Süd-Anlg-1](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-1") 2.5 MB

2

[Braunkohlenplan Welzow-Süd-Anlg-1-Karte.Anlg-1](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-1-Karte.Anlage-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-1-Karte.Anlg-1") 11.5 MB

3

[Braunkohlenplan Welzow-Süd-Anlg-1-Karte.Anlg-2](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-1-Karte.Anlage-2.pdf "Braunkohlenplan Welzow-Süd-Anlg-1-Karte.Anlg-2") 11.0 MB

4

[Braunkohlenplan Welzow-Süd-Anlg-1-Karte.Anlg-3](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-1-Karte.Anlage-3.pdf "Braunkohlenplan Welzow-Süd-Anlg-1-Karte.Anlg-3") 10.4 MB

5

[Braunkohlenplan Welzow-Süd-Anlg-1-Karte.Anlg-4](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-1-Karte.Anlage-4.pdf "Braunkohlenplan Welzow-Süd-Anlg-1-Karte.Anlg-4") 12.9 MB

6

[Braunkohlenplan Welzow-Süd-Anlg-1-Karte.Anlg-5](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-1-Karte.Anlage-5.pdf "Braunkohlenplan Welzow-Süd-Anlg-1-Karte.Anlg-5") 25.9 MB

7

[Braunkohlenplan Welzow-Süd-Anlg-1-Karte.Anlg-6](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-1-Karte.Anlage-6.pdf "Braunkohlenplan Welzow-Süd-Anlg-1-Karte.Anlg-6") 11.0 MB

8

[Braunkohlenplan Welzow-Süd-Anlg-1-Karte.Anlg-6a](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-1-Karte.Anlage-6a.pdf "Braunkohlenplan Welzow-Süd-Anlg-1-Karte.Anlg-6a") 10.3 MB

9

[Braunkohlenplan Welzow-Süd-Anlg-2](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2.pdf "Braunkohlenplan Welzow-Süd-Anlg-2") 18.4 MB

10

[Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-01.1](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Anhang-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-01.1") 760.6 KB

11

[Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-01.2](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Anhang-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-01.2") 3.8 MB

12

[Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-02](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Anhang-2.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-02") 1.1 MB

13

[Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-03](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Anhang-3.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-03") 797.2 KB

14

[Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-04](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Anhang-4.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-04") 913.7 KB

15

[Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-05](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Anhang-5.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-05") 1.5 MB

16

[Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-06](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Anhang-6.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-06") 1.0 MB

17

[Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-07](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Anhang-7.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-07") 1.3 MB

18

[Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-08](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Anhang-8.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-08") 4.7 MB

19

[Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-09](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Anhang-9.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-09") 1.2 MB

20

[Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-10](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Anhang-10.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Anhang-10") 3.0 MB

21

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.01.1](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.01.1") 9.1 MB

22

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.01.2](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.01.2") 9.1 MB

23

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.01.3](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.01.3") 9.2 MB

24

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.02](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.02") 8.1 MB

25

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.03](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.03") 7.8 MB

26

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.04](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.04") 7.8 MB

27

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.05](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.05") 7.8 MB

28

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.06](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.06") 7.7 MB

29

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.07](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.07") 8.5 MB

30

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.08](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.08") 8.7 MB

31

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.09](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.09") 8.8 MB

32

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.10](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-1.10") 9.1 MB

33

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-2.1](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-2.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-2.1") 9.0 MB

34

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-2.2](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-2.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-2.2") 10.6 MB

35

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-3.1](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-3.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-3.1") 9.5 MB

36

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-3.2](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-3.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-3.2") 9.0 MB

37

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-4.1](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-4.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-4.1") 26.2 MB

38

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-4.2](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-4.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-4.2") 12.6 MB

39

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-4.3](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-4.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-4.3") 9.6 MB

40

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-5.1](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-5_1.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-5.1") 1.8 MB

41

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-6.1](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-6.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-6.1") 7.6 MB

42

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-7.1](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-7.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-7.1") 8.9 MB

43

[Braunkohlenplan Welzow-Süd-Anlg-2-Karte-8.1](/br2/sixcms/media.php/68/GVBl_II_58_2014_Anlage-2-Karte-8.pdf "Braunkohlenplan Welzow-Süd-Anlg-2-Karte-8.1") 1.2 MB