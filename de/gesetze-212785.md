## Gesetz über die Feststellung des Haushaltsplanes des Landes Brandenburg für die Haushaltsjahre 2013 und 2014  (Haushaltsgesetz 2013/2014 - HG 2013/2014)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1  
Feststellung des Haushaltsplanes

Der diesem Gesetz als Anlage beigefügte Haushaltsplan des Landes Brandenburg für die Haushaltsjahre 2013 und 2014 wird in Einnahmen und Ausgaben festgestellt auf:

1.  10 318 623 500 Euro für das Haushaltsjahr 2013,
2.  10 469 231 300 Euro für das Haushaltsjahr 2014.
    

Der Gesamtbetrag der Verpflichtungsermächtigungen wird festgestellt auf:

1.  1 989 575 700 Euro für das Haushaltsjahr 2013,
2.  1 369 692 800 Euro für das Haushaltsjahr 2014.

#### § 2  
Kreditermächtigungen

(1) Das Ministerium der Finanzen wird ermächtigt, zur Deckung von Ausgaben im Haushaltsjahr 2013 Kredite aufzunehmen bis zur Höhe von 80 958 800 Euro.
Die Inanspruchnahme der Kreditaufnahme ist nur zulässig zur Deckung von Zuweisungen an die Flughafen Berlin-Brandenburg GmbH.

(2) Der Kreditermächtigung nach Absatz 1 wachsen die Beträge zur Tilgung von in den Haushaltsjahren 2013 und 2014 fällig werdenden Krediten zu, deren Höhe sich aus den Finanzierungsübersichten ergibt.
Das Ministerium der Finanzen wird ermächtigt, zum Aufbau von Eigenbeständen Kredite bis zur Höhe von 500 000 000 Euro aufzunehmen.
Auf die Kreditermächtigung nach Satz 2 sind die Beträge anzurechnen, die aufgrund von Ermächtigungen früherer Haushaltsgesetze aufgenommen worden sind oder sich bereits im Eigenbestand befinden.

(3) Über die Kreditermächtigung nach Absatz 1 hinaus darf das Ministerium der Finanzen zur Vorfinanzierung von Ausgaben, die aus den Fonds der Europäischen Union nachträglich erstattet werden, Kredite bis zur Höhe von insgesamt 200 000 000 Euro aufnehmen.
Die nach Satz 1 aufgenommenen Kredite sind mit den Erstattungen aus den Fonds zu tilgen.

(4) Im Rahmen der Kreditfinanzierung kann das Ministerium der Finanzen auch ergänzende Vereinbarungen treffen, die der Begrenzung von Zinsänderungsrisiken, der Erzielung günstigerer Konditionen und ähnlichen Zwecken bei neuen Krediten und bestehenden Schulden dienen.
Der Umfang derartiger Vereinbarungen darf keine Nettoforderungen aus diesen Vereinbarungen begründen, die höher als 100 000 000 Euro im jeweiligen Haushaltsjahr sind.
Auf diese Grenze werden Verträge nicht angerechnet, für die ein Besicherungsvertrag nach Absatz 7 besteht.
Das Ministerium der Finanzen wird ermächtigt, Darlehen vorzeitig zu tilgen oder Kredite mit unterjähriger Laufzeit aufzunehmen, soweit dies im Zuge von Zinsanpassungen oder zur Erlangung günstigerer Konditionen notwendig wird.
Diese Ermächtigung gilt auch, soweit Geschäfte getätigt werden, deren Einnahmen die Ausgaben für das jeweilige Kreditgeschäft übersteigen.
Die Kreditermächtigung nach Absatz 1 erhöht sich in Höhe der nach Satz 4 getilgten Beträge.
Diese Ermächtigung gilt auch für die im Wirtschaftsplan des Landeswohnungsbauvermögens vorgesehene Kreditaufnahme.

(5) Der Zeitpunkt der Kreditaufnahme ist nach der Kassenlage, den jeweiligen Kapitalmarktverhältnissen und gesamtwirtschaftlichen Erfordernissen zu bestimmen.

(6) Das Ministerium der Finanzen wird ermächtigt, zur Aufrechterhaltung einer ordnungsgemäßen Kassenwirtschaft in den Haushaltsjahren 2013 und 2014 bis zur Höhe von 12 Prozent des in § 1 Satz 1 festgestellten Betrages zuzüglich der nach Absatz 1 noch nicht in Anspruch genommenen Kreditermächtigungen Kassenverstärkungsmittel aufzunehmen.
Soweit diese Kredite zurückgezahlt sind, kann die Ermächtigung wiederholt in Anspruch genommen werden.
Zahlungen für Sicherheiten im Sinne von Absatz 7 werden auf die Ermächtigung nicht angerechnet.

(7) Das Ministerium der Finanzen wird ermächtigt, Sicherheiten in Form verzinster Barmittel zu stellen sowie entgegenzunehmen oder durch Wertpapierhinterlegung zu empfangen oder zu stellen.

#### § 3  
Bürgschaften und Rückbürgschaften

(1) Das Ministerium der Finanzen wird ermächtigt, in den Jahren 2013 und 2014 Bürgschaften für Kredite an die Wirtschaft, die freien Berufe sowie die Land- und Forstwirtschaft bis zur Höhe von insgesamt 425 000 000 Euro zu übernehmen.

(2) Das Ministerium der Finanzen wird ermächtigt, in den Jahren 2013 und 2014 Bürgschaften für Kredite zur Förderung des Wohnungsbaus und des Stadtumbaus bis zur Höhe von 3 000 000 Euro zu übernehmen.

(3) Das Ministerium der Finanzen wird ermächtigt, in den Jahren 2013 und 2014 Bürgschaften im Falle eines unvorhergesehenen und unabweisbaren Bedürfnisses, insbesondere für Notmaßnahmen im Land Brandenburg, bis zur Höhe von 30 000 000 Euro zu übernehmen.
Überschreitet die aufgrund dieser Ermächtigung zu übernehmende Bürgschaft im Einzelfall den Betrag von 5 000 000 Euro, bedarf es der Einwilligung des Ausschusses für Haushalt und Finanzen des Landtages.

(4) Bürgschaften gemäß den Absätzen 1 und 2 dürfen nur für Kredite übernommen werden, deren Rückzahlung durch den Schuldner bei normalem wirtschaftlichem Ablauf innerhalb der für den einzelnen Kredit vereinbarten Zahlungstermine erwartet werden kann.

#### § 4  
Garantien und sonstige Gewährleistungen

(1) Das Ministerium der Finanzen wird ermächtigt, in den Jahren 2013 und 2014 im Interesse der Kapitalversorgung kleiner und mittelständischer Unternehmen Garantien bis zur Höhe von 65 000 000 Euro für die Übernahme von Kapitalbeteiligungen zu übernehmen.
Diese Garantien können auch als Rückgarantien gegenüber Kreditinstituten übernommen werden.

(2) Das Ministerium der Finanzen wird ermächtigt, in den Jahren 2013 und 2014 Garantien zur Finanzierung von Film- und Fernsehproduktionen sowie Projektentwicklungen im Medienbereich bis zur Höhe von 20 000 000 Euro zu übernehmen.

(3) Das Ministerium der Finanzen wird ermächtigt, unter Anrechnung auf die Ermächtigungen gemäß den Absätzen 1 und 2 Garantien zur Finanzierung von Produktionen, Projektentwicklungen und Existenzgründungen im Bereich der Kultur- und Kreativwirtschaft zu übernehmen.

(4) Das Ministerium der Finanzen wird ermächtigt, in den Jahren 2013 und 2014 zur Absicherung von Risiken, die sich aus dem Betrieb von kerntechnischen Anlagen und dem Umgang mit radioaktiven Stoffen in Forschungseinrichtungen des Landes ergeben, Gewährleistung bis zur Höhe von 10 000 000 Euro zu übernehmen.

(5) Das Ministerium der Finanzen wird ermächtigt, in den Jahren 2013 und 2014 zur Deckung des Haftpflichtrisikos von Zuwendungsempfängern des Landes aus der Haftung für Leihgaben im Bereich Kunst und Kultur sowie für wissenschaftliche Forschungsinstitute, die vom Bund und vom Land gemeinsam getragen werden, Garantien bis zum Höchstbetrag von 80 000 000 Euro zu übernehmen.

(6) Das Ministerium der Finanzen wird ermächtigt, in den Jahren 2013 und 2014 zur Absicherung von Risiken, die sich aus der Tätigkeit der Ethikkommission der Landesärztekammer Brandenburg nach _§ 7 Absatz 1 des Heilberufsgesetzes_ ergeben, Gewährleistungen bis zur Höhe von 10 000 000 Euro zu übernehmen.

(7) Haftungsfreistellungen und Garantien gemäß den Absätzen 1 und 2 dürfen nur unter den in § 3 Absatz 4 genannten Voraussetzungen übernommen werden.

#### § 5  
Grundsätze für neue Steuerungsinstrumente

(1) In den Einzelplänen 02 bis 12 werden aus den Personalausgaben je Einzelplan Personalbudgets gebildet.
 In den Einzelplänen 02 bis 12 sowie im Einzelplan 20 werden aus den sächlichen Verwaltungsausgaben, den Ausgaben für den Erwerb beweglicher Sachen und den Verwaltungseinnahmen je Einzelplan Verwaltungsbudgets gebildet.
Werden die Ausgaben des Personalbudgets und des Verwaltungsbudgets beim Jahresabschluss unterschritten, kann der Betrag in Höhe der Unterschreitung anteilig einer Rücklage zugeführt werden.
Die Rücklagenbildung erfolgt grundsätzlich in Höhe von 50 Prozent der Unterschreitung.
Das Ministerium der Finanzen kann einen höheren Rücklagesatz bestimmen.
Die Bestimmung eines geringeren Rücklagensatzes ist nur zur Vermeidung oder Begrenzung eines ansonsten entstehenden Fehlbetrages nach § 25 Absatz 1 der Landeshaushaltsordnung zulässig.

(2) Das Personalbudget umfasst mit Ausnahme der Gruppen 432 und 453 die Ausgaben der Hauptgruppe 4.
 Diese sind innerhalb des Einzelplans gegenseitig deckungsfähig.
Rücklagen aus dem Vorjahr dürfen zur Verstärkung der Ausgaben verwendet werden; vorgezogene Entnahmen im Vorjahr sind durch Minderausgaben im laufenden Haushaltsjahr auszugleichen.
Wird das Personalbudget beim Jahresabschluss über- oder unterschritten, kann der Betrag bis zur Höhe der Über- oder Unterschreitung auf das Personalbudget für den nächsten Haushalt vorgetragen werden.
Aus Drittmitteln oder zweckgebundenen Sonderabgaben gedeckte Personalausgaben können vom Personalbudget ausgenommen werden.

(3) Die Ausgaben der Gruppe 453 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.
 Das jeweilige Personalbudget ist einseitig deckungsfähig zugunsten der Ausgaben der Gruppe 453.
Die Ausgaben der Gruppe 432 sind über alle Einzelpläne gegenseitig deckungsfähig.

(4) Das Verwaltungsbudget umfasst die Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Titel 518 25 und der Gruppe 529, und der Obergruppe 81 und die Einnahmen der Obergruppen 11 bis 13.
Die Ausgaben sind innerhalb des Einzelplans gegenseitig deckungsfähig.
Das jeweilige Verwaltungsbudget ist einseitig deckungsfähig zugunsten des Titels 518 25.
Rücklagen aus Vorjahren dürfen zur Verstärkung der Ausgaben verwendet werden.
Wird das Verwaltungsbudget beim Jahresabschluss über- oder unterschritten, kann der Betrag bis zur Höhe der Über- oder Unterschreitung auf das Verwaltungsbudget für den nächsten Haushalt vorgetragen werden.
Einzelne Einnahmen und Ausgaben können vom Verwaltungsbudget ausgenommen werden.

(5) Mehreinnahmen bei den Obergruppen 11 bis 13 können zur Verstärkung der Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Gruppe 529, und der Obergruppe 81 im Rahmen des Verwaltungsbudgets verwendet werden, wenn ein verwaltungsmäßiger oder sachlicher Zusammenhang besteht oder eine wirtschaftliche und sparsame Verwendung gefördert wird.
Minderausgaben beim Personalbudget können zur Verstärkung der Ausgaben des Verwaltungsbudgets im jeweiligen Einzelplan verwendet werden, soweit sich daraus keine Überschreitung des Personalbudgets beim Jahresabschluss ergibt.

(6) Minderausgaben beim Verwaltungsbudget können zur Verstärkung der Ausgaben bei Kapitel 12 020 Titel 519 61 - Größere Unterhaltungsarbeiten an Grundstücken, Gebäuden und Räumen - und Titel 891 61 - Zuführungen für Investitionen - herangezogen werden.

(7) Die allein aus Landesmitteln finanzierten und nicht zur Komplementärfinanzierung von Drittmitteln bestimmten Ausgaben der Hauptgruppe 6 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.
Ebenso sind die allein aus Landesmitteln finanzierten und nicht zur Komplementärfinanzierung von Drittmitteln bestimmten Ausgaben der Obergruppen 83 bis 89 innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.

(8) Für die Wirtschaftspläne der Landesbetriebe nach § 26 Absatz 1 der Landeshaushaltsordnung gelten die vorstehenden Absätze entsprechend, soweit keine besonderen Regelungen getroffen sind.

(9) Die im Einzelplan 06 veranschlagten Universitäten und Fachhochschulen werden jeweils nur mit ihrem Zuschussbedarf veranschlagt.
Die Einnahmen und Ausgaben dieser Einrichtungen werden in Wirtschaftsplänen veranschlagt, die dem Haushaltsplan als Erläuterungen beigefügt sind.
Für die Bewirtschaftung gelten die Absätze 1 bis 6 entsprechend, soweit keine besonderen Regelungen getroffen sind.

(10) Das Nähere regelt das Ministerium der Finanzen.

(11) Die Ausgaben des Titels 919 35 sind über alle Einzelpläne gegenseitig deckungsfähig.

#### § 6  
Neue Steuerungsinstrumente im Bereich des Landtages, Verfassungsgerichts und Landesrechnungshofes

(1) Gegenseitig deckungsfähig sind innerhalb der Einzelpläne 01, 13 und 14 die Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Titel 518 25 und der Gruppe 529, und der Obergruppe 81.
Das jeweilige Verwaltungsbudget ist einseitig deckungsfähig zugunsten des Titels 518 25.
Werden die Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Gruppe 529, und der Obergruppe 81 beim Jahresabschluss unterschritten, kann der Betrag in Höhe der Unterschreitung einer Rücklage zugeführt werden.
Sofern es beim Jahresabschluss zu einer Überschreitung kommt, kann der Betrag in Höhe der Überschreitung in den nächsten Haushalt vorgetragen werden.
Rücklagen aus dem Vorjahr dürfen zur Verstärkung der entsprechenden Ausgaben verwendet werden.

(2) Nicht verausgabte Mittel der Titelgruppe 99 - Kosten für Datenverarbeitung - können bei Unterschreitung der veranschlagten Ausgaben in Höhe der Unterschreitung einer Rücklage zugeführt werden.
Auf die Bildung dieser Rücklage ist Absatz 1 nicht anzuwenden.
Innerhalb der Titelgruppe 99 dürfen Einnahmen, die der für Datenverarbeitung gebildeten Rücklage entnommen werden, zur Deckung von Mehrausgaben verwendet werden.

(3) Für die Ausgaben der Hauptgruppe 4, mit Ausnahme der Ausgaben der Gruppe 411 - Aufwendungen für Abgeordnete - im Kapitel 01 010 und der Gruppen 432 und 453, wird innerhalb des jeweiligen Einzelplans ein Personalbudget gebildet.
Die Ausgaben sind innerhalb des Personalbudgets gegenseitig deckungsfähig.
Rücklagen aus dem Vorjahr dürfen zur Verstärkung der Ausgaben verwendet werden; vorgezogene Entnahmen im Vorjahr sind durch Minderausgaben im laufenden Haushaltsjahr auszugleichen.
Wird das Personalbudget beim Jahresabschluss über- oder unterschritten, kann der Betrag bis zur Höhe der Über- oder Unterschreitung auf das Personalbudget für den nächsten Haushalt vorgetragen werden.

(4) Die Ausgaben der Gruppe 453 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.
 Das jeweilige Personalbudget ist einseitig deckungsfähig zugunsten der Ausgaben der Gruppe 453.
Die Ausgaben der Gruppe 432 sind über alle Einzelpläne gegenseitig deckungsfähig.

(5) Mehreinnahmen bei den Obergruppen 11 bis 13 können zur Verstärkung der Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Gruppe 529, und der Obergruppe 81 verwendet werden, wenn ein verwaltungsmäßiger oder sachlicher Zusammenhang besteht oder eine wirtschaftliche und sparsame Verwendung gefördert wird.
Minderausgaben beim Personalbudget können zur Verstärkung der in Satz 1 bezeichneten Ausgaben im jeweiligen Einzelplan verwendet werden, soweit sich daraus keine Überschreitung des Personalbudgets beim Jahresabschluss ergibt.

#### § 7  
Besondere Regelungen für den Brandenburgischen Landesbetrieb für Liegenschaften und Bauen (BLB)

(1) Das Ministerium der Finanzen wird ermächtigt, nach Bestätigung des Wirtschaftsplans für den Landesbetrieb Einnahmen, Ausgaben, Verpflichtungsermächtigungen, Planstellen und Stellen in den Landesbetrieb umzusetzen, soweit weitere Liegenschaften in die Teilnahme am Vermieter-Mieter-Modell überführt werden.

(2) Die Ansätze bei den Titeln 518 25 sind bis zum Abschluss der jeweiligen Mietverträge mit dem BLB gesperrt.
Von dieser Sperre sind Ausgaben nicht erfasst, die im Zusammenhang mit der Bewirtschaftung der Liegenschaften stehen.

(3) Nicht veranschlagte Ausgaben für Mieten nach dem Vermieter-Mieter-Modell beim Titel 518 25 stellen keine Mehrausgaben nach § 37 der Landeshaushaltsordnung dar.
Sie können vom Ministerium der Finanzen zugelassen werden, wenn sie durch Minderausgaben oder Mehreinnahmen an anderer Stelle gedeckt sind.

(4) Die Ansätze des Titels 518 25 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.

(5) Vom BLB zurückgezahlte Beträge aus der Abrechnung von Betriebs- und Nebenkosten sind bei Titel 518 25 und bei Kapitel 12 020 bei Titel 518 61 abzusetzen.

#### § 8  
Mehrausgaben, Komplementärmittel

(1) Der gemäß § 37 Absatz 1 Satz 4 der Landeshaushaltsordnung festzulegende Betrag wird auf 7 500 000 Euro Landesmittel festgesetzt, für Verpflichtungsermächtigungen (§ 38 Absatz 1 Satz 3 der Landeshaushaltsordnung) als Jahresbetrag.
Überschreiten diese Mehrausgaben im Einzelfall den Betrag von 5 000 000 Euro Landesmittel, bei Verpflichtungsermächtigungen als jährlich fällig werdender Betrag, ist die Einwilligung des Ausschusses für Haushalt und Finanzen des Landtages einzuholen.
 Mehrausgaben (bei Verpflichtungsermächtigungen als jährlich fällig werdender Betrag) zur Erfüllung von Rechtsverpflichtungen, die einen Betrag von 15 000 000 Euro Landesmittel überschreiten, sind vor Einwilligung des Ministeriums der Finanzen dem Ausschuss für Haushalt und Finanzen des Landtages zur Unterrichtung vorzulegen, sofern nicht aus zwingenden Gründen eine Ausnahme geboten ist.
 Überplanmäßige oder außerplanmäßige Verpflichtungsermächtigungen gemäß § 38 Absatz 1 Satz 3 der Landeshaushaltsordnung, bei denen Ausgabemittel von anderer Seite zweckgebunden zur Verfügung gestellt werden und ein Betrag von 15 000 000 Euro überschritten wird, bedürfen der Einwilligung des Ausschusses für Haushalt und Finanzen des Landtages.

(2) Eines Nachtragshaushaltsgesetzes bedarf es zudem nicht, wenn

1.  Komplementärmittel von der Europäischen Union oder vom Bund unvorhergesehen bereitgestellt werden, die eine zusätzliche anteilige Finanzierung durch das Land erforderlich machen, oder
2.  Umschichtungen innerhalb eines Fonds der Europäischen Union oder zwischen den Fonds, einschließlich der Kofinanzierung durch das Land, erforderlich sind.

In den Fällen des Satzes 1 Nummer 2 bedarf es der Einwilligung des Ausschusses für Haushalt und Finanzen, wenn die Umschichtungen im Einzelfall 5 000 000 Euro EU- und Landesmittel, bei Verpflichtungsermächtigungen als jährlich fällig werdender Betrag, überschreiten.

(3) Veranschlagte Landesmittel und Verpflichtungsermächtigungen, die nicht mehr zur Kofinanzierung von Leistungen Dritter für die gemäß Haushaltsplan vorgesehenen Zwecke erforderlich sind, sind gesperrt.
Die Aufhebung der Sperre bedarf der Zustimmung des Ministeriums der Finanzen.
Das Ministerium der Finanzen wird ermächtigt, die Vorfinanzierung von Maßnahmen, für die die Leistung von Dritten vorgesehen ist, zuzulassen.

(4) Im Bereich der Fonds der Europäischen Union dürfen mit Einwilligung des Ministeriums der Finanzen Mehrausgaben bis zur Höhe der Minderausgaben aus Vorjahren geleistet werden, soweit die zugehörigen Erstattungsanträge an die EU-Kommission bis spätestens zum II.
Quartal des Folgejahres gestellt werden oder die Mehrausgaben zur Kofinanzierung von Mitteln aus den Fonds dienen.

(5) Die dem Land Brandenburg gemäß § 1 Absatz 1 Nummer 3 bis 4 der Aufbauhilfeverordnung vom 16.
August 2013 (BGBl.
I S.
3233) zustehenden Mittel dürfen vom Land bis zur Höhe von 42 919 000 Euro vorfinanziert werden.

#### § 9  
Sonderfinanzierungen

(1) Durch den Abschluss von Leasing-, Mietkauf- und ähnlichen Verträgen (Sonderfinanzierungen) für Bauinvestitionen dürfen Verpflichtungen zulasten künftiger Haushaltsjahre eingegangen werden.
Diese Befugnis gilt auch bei Umsetzung von Bauinvestitionen im Rahmen von Öffentlich Privaten Partnerschaften, die auch die Betriebsphase umfassen (Lebenszyklusansatz).
Das Ministerium der Finanzen wird ermächtigt, mit Zustimmung des Ausschusses für Haushalt und Finanzen des Landtages Sonderfinanzierungen zuzulassen; § 38 Absatz 1 der Landeshaushaltsordnung bleibt unberührt.

(2) Verpflichtungsermächtigungen für Investitionsfinanzierungen dürfen abweichend von § 8 Absatz 1 bis zu der Höhe überschritten werden, in der sie für Maßnahmen nach Absatz 1 Satz 1 benötigt werden.

(3) Die Wirtschaftlichkeit von Sonderfinanzierungen ist in jedem Einzelfall zu belegen.

#### § 10  
Industrieansiedlungsverträge

Soweit die veranschlagten Ausgaben bei voller Ausschöpfung der Deckungsfähigkeit und die Verpflichtungsermächtigungen nicht ausreichen, Industrieansiedlungsverträge mit finanziellen Verpflichtungen für das Land abzuschließen, ist das Ministerium für Wirtschaft und Europaangelegenheiten ermächtigt, über Industrieansiedlungsverträge zu verhandeln und - bei Zustimmung des Ministeriums der Finanzen und nach Einwilligung des Ausschusses für Haushalt und Finanzen im Benehmen mit dem Ausschuss für Wirtschaft des Landtages - zusätzliche Verpflichtungen zulasten des Landes einzugehen.

#### § 11  
Besondere Regelungen für Zuwendungen

(1) Ausgaben und Verpflichtungsermächtigungen für Zuwendungen im Sinne des § 23 der Landeshaushaltsordnung zur Deckung der gesamten Ausgaben oder eines nicht abgegrenzten Teils der Ausgaben einer Stelle außerhalb der Landesverwaltung (institutionelle Förderung) sind gesperrt, bis der Haushalts- oder Wirtschaftsplan des Zuwendungsempfängers von dem zuständigen Ministerium gebilligt worden ist.

(2) Die in Absatz 1 genannten Zuwendungen zur institutionellen Förderung dürfen nur mit der Auflage bewilligt werden, dass der Zuwendungsempfänger seine Beschäftigten nicht besser stellt als vergleichbare Bedienstete des Landes; vorbehaltlich einer abweichenden tarifvertraglichen Regelung dürfen deshalb keine günstigeren Arbeitsbedingungen vereinbart werden, als sie für Bedienstete des Landes jeweils vorgesehen sind.
Entsprechendes gilt bei Zuwendungen zur Projektförderung, wenn die Gesamtausgaben des Zuwendungsempfängers überwiegend aus Zuwendungen der öffentlichen Hand bestritten werden.
Das Ministerium der Finanzen kann bei Vorliegen zwingender Gründe Ausnahmen zulassen.

(3) Die in den Erläuterungen zu den Titeln, aus denen Zuwendungen im Sinne des § 23 der Landeshaushaltsordnung zur institutionellen Förderung geleistet werden, für andere als Projektaufgaben ausgebrachte Planstellen für Beamte sowie Stellen für Arbeitnehmer sind hinsichtlich der Gesamtzahl und der Zahl der für die einzelnen Besoldungs- und Entgeltgruppen ausgebrachten Planstellen und Stellen verbindlich.
Das Ministerium der Finanzen wird ermächtigt, Ausnahmen von der Verbindlichkeit der Stellenpläne zuzulassen.
Die Wertigkeit außertariflicher Stellen ist durch die Angabe der entsprechenden Besoldungsgruppe zu kennzeichnen.
Das Ministerium der Finanzen kann Abweichungen in den Wertigkeiten der Stellen zulassen.
Sind im Wirtschaftsplan Stellen außerhalb der Anlagen B 2 und B 3 zum Tarifvertrag der Länder (TV-L) ohne Angaben des Entgelts ausgebracht, bedarf die Festsetzung des Entgelts in jedem Einzelfall der vorherigen Zustimmung des Ministeriums der Finanzen.
Sonstige Abweichungen bedürfen der Einwilligung des Ministeriums der Finanzen und setzen eine Tätigkeitsdarstellung voraus.

#### § 12  
Personalwirtschaftliche Regelungen

(1) Zur Einhaltung des Stellenplans gemäß der gültigen Personalbedarfsplanung des Landes Brandenburg und des Personalbudgets sind die Ressorts verpflichtet, alle Möglichkeiten zur Einsparung von Planstellen, Stellen, Beschäftigungspositionen und Personalausgaben zu nutzen.
Dazu können abweichend von § 50 Absatz 1 Satz 1 der Landeshaushaltsordnung auch Mittel oder Planstellen und Stellen umgesetzt werden, ohne dass Aufgaben von einer Verwaltung auf eine andere Verwaltung übergehen.
Das Nähere regelt das Ministerium der Finanzen.

(2) Die Erläuterungen zu den Titeln der Gruppe 422 für Stellen der Beamten auf Probe bis zur Anstellung und zu den Titeln der Gruppe 428 sind hinsichtlich der zulässigen Zahl der für die einzelnen Besoldungs- und Entgeltgruppen ausgebrachten Stellen verbindlich.
 Die den Wirtschaftsplänen der Landesbetriebe nach § 26 Absatz 1 der Landeshaushaltsordnung beigefügten Stellenübersichten sind verbindlich.
Das Ministerium der Finanzen kann Ausnahmen von der Verbindlichkeit der Stellenpläne für die Landesbetriebe zulassen.

(3) Abweichend von § 49 der Landeshaushaltsordnung können auf Planstellen auch beamtete Hilfskräfte und Arbeitnehmer geführt werden.

(4) Einnahmen aus Zuschüssen für die berufliche Eingliederung behinderter Menschen und für Arbeitsbeschaffungsmaßnahmen fließen den entsprechenden Ansätzen für Personalausgaben zu.
Innerhalb der einzelnen Kapitel fließen die Einnahmen den Ausgaben bei folgenden Titeln - einschließlich den entsprechenden Titeln - in Titelgruppen zu:

1.  Gruppe 428 aus Erstattungen der Förderleistungen der Bundesagentur für Arbeit in Bezug auf das Altersteilzeitgesetz,
2.  Gruppen 422, 428, 441, 443 und 446 aus Schadensersatzleistungen Dritter.

(5) Planstellen und Stellen können für Zeiträume, in denen Stelleninhaber vorübergehend nicht oder nicht vollbeschäftigt sind, innerhalb des jeweiligen Einzelplans im Umfang der nicht in Anspruch genommenen Planstellen- oder Stellenanteile für die Beschäftigung von beamteten Hilfskräften und Kräften in zeitlich befristeten Arbeitsverträgen in Anspruch genommen werden.

(6) Das Ministerium der Finanzen wird ermächtigt, Planstellen für Lehrkräfte zur Besetzung mit Beamten, für die die Einstufung nach Anlage 1 des Brandenburgischen Besoldungsgesetzes nicht gilt, nach Maßgabe des Bundesbesoldungsgesetzes in der am 31. August 2006 geltenden Fassung zu heben.

#### § 13  
Besondere Regelungen für Planstellen und Stellen

(1) Planstellen und Stellen, die einen kw-Vermerk tragen, können nach ihrem Freiwerden mit schwer behinderten Menschen wiederbesetzt werden, wenn die gesetzliche Pflichtquote gemäß § 71 Absatz 1 des Neunten Buches Sozialgesetzbuch bei den Planstellen und Stellen in der Landesverwaltung nicht erreicht wird.
In diesem Fall ist der schwer behinderte Mensch auf der nächsten freiwerdenden Planstelle oder Stelle der betreffenden oder nächsthöheren Besoldungs- oder Entgeltgruppe innerhalb des Einzelplans zu führen.
Das Ministerium der Finanzen kann Ausnahmen zulassen.

(2) Das Ministerium der Finanzen wird ermächtigt zuzulassen, dass von einem kw-Vermerk mit Datumsangabe abgewichen wird, wenn die Planstelle oder Stelle weiter benötigt wird, weil sie nicht rechtzeitig frei wird; in diesem Fall ist der Stelleninhaber auf der nächsten freiwerdenden Planstelle oder Stelle der betreffenden oder nächsthöheren Besoldungs- oder Entgeltgruppe innerhalb des Einzelplans zu führen.

(3) Das Ministerium der Finanzen wird ermächtigt, mit Einwilligung des Ausschusses für Haushalt und Finanzen des Landtages Planstellen für Beamte, Richter und Stellen für Arbeitnehmer zusätzlich auszubringen, wenn hierfür ein unabweisbares, auf andere Weise nicht zu befriedigendes Bedürfnis besteht.

(4) Mit Einwilligung des Ministeriums der Finanzen können nach Änderungen im Besoldungs- oder Tarifrecht Planstellen- und Stellenveränderungen vorgenommen werden.
Stellenveränderungen sind mit Einwilligung des Ministeriums der Finanzen auch dann möglich, wenn tarifrechtliche Ansprüche bestehen.

(5) Arbeitnehmer, die vor der Überleitung aus dem BAT/BAT-O und dem MTArb/MTArb-O in den TV-L einen Bewährungs- oder Fallgruppenaufstieg gemäß den §§ 23a, 23b BAT/BAT-O beziehungsweise den vergleichbaren Bestimmungen für Arbeiter vollzogen haben oder bei denen nach den bisherigen tarifrechtlichen Bestimmungen ein Bewährungs- oder Fallgruppenaufstieg in der jeweiligen Fallgruppe vorgesehen war, sowie nach dem 1.
November 2006 neu eingestellte oder neu eingruppierte Arbeitnehmer mit einem höherwertigen Tarifanspruch gemäß Anlage 4 TVÜ-Länder können bis zum Wirksamwerden neuer Eingruppierungsvorschriften für den TV-L oder bis zum Ausscheiden auf einer niedrigwertigeren TV-L-Stelle geführt werden, die der ursprünglichen Stelle in der Struktur des durch den TV-L ersetzten BAT/BAT-O und des MTArb/MTArb-O entspricht.

(6) Das Nähere regelt das Ministerium der Finanzen.

#### § 14  
Ausbringung zusätzlicher Leerstellen

(1) Werden planmäßige Beamte, Richter und Arbeitnehmer im dienstlichen Interesse des Landes mit Zustimmung der obersten Dienstbehörde im Dienst einer öffentlichen zwischenstaatlichen oder überstaatlichen Einrichtung, einer Bundesbehörde oder einer kommunalen Gebietskörperschaft oder für eine Tätigkeit bei einer Fraktion oder einer Gruppe des Landtages, des Deutschen Bundestages oder einer zwischenstaatlichen Einrichtung unter Wegfall der Dienstbezüge länger als ein Jahr verwendet und besteht ein unabweisbares Bedürfnis, die Planstellen und Stellen neu zu besetzen, so kann das Ministerium der Finanzen dafür gleichwertige Leerstellen ausbringen.
Das Gleiche gilt für eine Verwendung bei sonstigen landesunmittelbaren und -mittelbaren juristischen Personen des öffentlichen Rechts sowie bei juristischen Personen des Privatrechts, soweit diese vom Land institutionell gefördert werden oder das Land mehrheitlich beteiligt ist.

(2) Absatz 1 findet entsprechend Anwendung, wenn

1.  Beamte nach § 80 Absatz 1 Satz 1 Nummer 2 des Landesbeamtengesetzes länger als ein Jahr beurlaubt werden oder
2.  Beamte aus dem Beamtenverhältnis entlassen sind und nach § 121 Absatz 6 des Landesbeamtengesetzes oder nach § 63 Absatz 5, § 64 Absatz 1, § 65 Absatz 4 des Brandenburgischen Hochschulgesetzes einen Anspruch auf Übernahme in das frühere Dienstverhältnis haben oder
3.  Beamte aus sonstigen persönlichen Gründen länger als ein Jahr beurlaubt werden oder
4.  die Rechte und Pflichten aus dem Dienstverhältnis nach § 72 Satz 1 des Landesbeamtengesetzes ruhen.

(3) Für planmäßige Beamte außerhalb der Schulkapitel, die nach § 71 des Landesbeamtengesetzes länger als ein Jahr ohne Unterbrechung Elternzeit nehmen, gilt vom Beginn der Beurlaubung an eine Leerstelle der entsprechenden Besoldungsgruppe als ausgebracht.
Satz 1 gilt auch für die Beurlaubung von Richtern aus familiären Gründen gemäß § 4 Absatz 1 Nummer 2 des Brandenburgischen Richtergesetzes.

(4) Die Absätze 2 und 3 gelten entsprechend für Richter und Arbeitnehmer.

(5) Für planmäßige Beamte, Richter und Arbeitnehmer, die im Rahmen der Umsetzung der Altersteilzeitregelung am Blockmodell teilnehmen, gilt vom Beginn der Freistellungsphase an eine Leerstelle der entsprechenden Besoldungs- und Entgeltgruppe als ausgebracht.
Zum Zeitpunkt des Übergangs in den Ruhestand fällt diese Leerstelle weg.
Diese Beschäftigten sind bis zum Ausscheiden auf diesen Leerstellen zu führen.

(6) Über den weiteren Verbleib der nach den Absätzen 1 bis 5 ausgebrachten Leerstellen ist im nächsten Haushaltsplan zu entscheiden.

#### § 15  
Vergabe leistungsbezogener Besoldungselemente an Beamte

(1) Für die Vergabe von Leistungsstufen ist die Brandenburgische Leistungsstufenverordnung sowie für die Vergabe von Leistungsprämien und Leistungszulagen die Brandenburgische Leistungsprämien- und -zulagenverordnung anzuwenden.

(2) Innerhalb eines Kapitels dürfen Zulagen für eine befristete Übertragung einer herausgehobenen Funktion nach § 45 des Bundesbesoldungsgesetzes für Beamte bis zur Höhe von 0,1 Prozent der Ausgaben der Titel 422 10 geleistet werden.
In den Einzelplänen 02 bis 12 dürfen die Zulagen nur im Einvernehmen mit dem Ministerium der Finanzen gewährt werden.
Das Ministerium der Finanzen kann hinsichtlich der Ausgabenhöhe in Satz 1 Ausnahmen zulassen.

(3) Die für die Vergabe leistungsbezogener Besoldungselemente anfallenden Ausgaben sind aus Einsparungen bei anderen Titeln der Hauptgruppe 4 im jeweiligen Einzelplan (ausgenommen Gruppen 432 und 453) oder durch Entnahmen aus der Rücklage Personalbudget zu decken.

#### § 16  
Verbilligte Veräußerung und Nutzungsüberlassung von Grundstücken

(1) Grundstücke des Allgemeinen Grundvermögens dürfen gemäß § 63 Absatz 3 Satz 2 und Absatz 4 der Landeshaushaltsordnung

1.  bei der Nutzungsbindung von mindestens 15 Jahren für Einrichtungen des Sozial-, Kinder- und Jugendwesens in gemeinnütziger Trägerschaft um bis zu 25 Prozent unter dem vollen Wert veräußert werden;
2.  bebaut (mit besonderem Sanierungsaufwand) und unbebaut bei einer Belegungsbindung von mindestens 15 Jahren um bis zu 40 Prozent unter dem vollen Wert veräußert werden, wenn sichergestellt ist, dass sie für Maßnahmen der sozialen Wohnraumförderung nach § 2 des Wohnraumförderungsgesetzes verwendet werden;
3.  bei einer Belegungsbindung von mindestens 15 Jahren um bis zu 50 Prozent unter dem vollen Wert veräußert werden, wenn sichergestellt ist, dass sie im Rahmen des vom Land geförderten Studentenwohnraumbaus zur Schaffung von Studentenwohnungen oder einer vergleichbaren Förderung verwendet werden.
    Unter den gleichen Voraussetzungen können bebaute und unbebaute Grundstücke an Studentenwerke unentgeltlich abgegeben werden;
4.  bei Sicherstellung der dauerhaften Zugänglichkeit für die Öffentlichkeit im Falle von Seegrundstücken nebst Umgriff, die nicht dem Forstvermögen zuzuordnen sind und nicht dauerhaft für Landeszwecke benötigt werden, um bis zu 100 Prozent unter dem vollen Wert an kommunale Träger, Naturschutzeinrichtungen oder gemeinnützige Träger veräußert werden, wenn der Ausschuss für Haushalt und Finanzen des Landtages in die Veräußerung einwilligt;
5.  im Wege der Bestellung eines Erbbaurechts vergeben werden, wobei der Erbbauzins je nach dem zu fördernden Zweck für die Dauer der Nutzungs- und Belegungsbindung abgesenkt werden darf, und zwar
    1.  für die gemeinnützigen außeruniversitären Forschungseinrichtungen auf 0 Prozent, wobei der Erbbauzins nach Ablauf von jeweils zehn Jahren um jeweils 1 Prozent erhöht werden kann,
    2.  in den Fällen der Nummer 1 auf 2 Prozent,
    3.  in den Fällen der Nummer 2 auf 3 Prozent und
    4.  in den Fällen von Nummer 3 Satz 2 auf 0 Prozent für die ersten zehn Jahre, 1 Prozent für die folgenden zehn Jahre und so fortlaufend bis zu 3 Prozent nach 30 Jahren ausgehend vom Bodenwert.
        In den Fällen von Nummer 3 Satz 1 auf 3 Prozent vom Bodenwert;
6.  vom Land institutionell geförderten außeruniversitären Forschungseinrichtungen gegen Übernahme der Betriebs- und zumutbaren Bauunterhaltungskosten unentgeltlich zur Nutzung überlassen werden.

(2) Für die nach dem Gesetz über die Verwertung der Liegenschaften der Westgruppe der Truppen in der Titelgruppe 65 „WGT-Liegenschaftsvermögen im AGV“ im Kapitel 20 630 ausgewiesene Vermögensmasse gilt über die Regelung des Absatzes 1 hinaus, dass bebaute und unbebaute Grundstücke um bis zu 25 Prozent unter dem vollen Wert veräußert oder im Erbbaurecht vergeben werden dürfen, die für unmittelbare Verwaltungszwecke vom Land sowie für kommunale Infrastrukturmaßnahmen von den Kreisen und den Gemeinden dauerhaft genutzt werden können.

(3) Über die Verbilligungen gemäß Absatz 1 hinaus wird gemäß § 63 Absatz 3 Satz 2 und Absatz 4 der Landeshaushaltsordnung zugelassen, dass landeseigene bebaute und unbebaute Grundstücke an Gebietskörperschaften für die im Bundeshaushalt aufgeführten Zwecke bis zu dem Prozentsatz unter dem vollen Wert veräußert, im Wege der Erbbaurechtsbestellung zur Verfügung gestellt, vermietet, verpachtet oder zur Nutzung überlassen werden, zu dem der Bund dem Land Verbilligungen bei der Veräußerung, Zurverfügungstellung im Wege des Erbbaurechts, Vermietung, Verpachtung oder Nutzungsüberlassung von bundeseigenen Grundstücken für gleiche Zwecke einräumt.
Vom Gegenseitigkeitserfordernis nach Satz 1 sind die Liegenschaften, die in der Titelgruppe 65 „WGT-Liegenschaftsvermögen im AGV“ im Kapitel 20 630 ausgewiesen sind, ausgenommen.

(4) Gemäß § 61 Absatz 1 Satz 1, § 63 Absatz 3 Satz 2 und Absatz 4 der Landeshaushaltsordnung wird die vorübergehende oder dauernde Abgabe von Grundstücken des Allgemeinen Grundvermögens an das Verwaltungsgrundvermögen ohne Werterstattung zugelassen; dies gilt nicht für Grundstücke, die zur in der Titelgruppe 65 „WGT-Liegenschaftsvermögen im AGV“ im Kapitel 20 630 ausgewiesenen Vermögensmasse gehören.

#### § 17  
Besondere Regelungen für geheim zu haltende Ausgaben

(1) Aus zwingenden Gründen des Geheimschutzes wird die Bewilligung von Ausgaben, die nach einem geheim zu haltenden Wirtschaftsplan bewirtschaftet werden sollen, von der Billigung des Wirtschaftsplans durch die Parlamentarische Kontrollkommission nach § 23 des Brandenburgischen Verfassungsschutzgesetzes abhängig gemacht.
Die Mitglieder dieser Kontrollkommission sind zur Geheimhaltung aller Angelegenheiten verpflichtet, die ihnen bei dieser Tätigkeit bekannt geworden sind.

(2) Die Präsidentin oder der Präsident des Landesrechnungshofes prüft in den Fällen des Absatzes 1 nach § 9 des Landesrechnungshofgesetzes und unterrichtet die Parlamentarische Kontrollkommission sowie die zuständige oberste Landesbehörde und das Ministerium der Finanzen über das Ergebnis der Prüfung der Jahresrechnung sowie der Haushalts- und Wirtschaftsführung.
§ 97 Absatz 4 der Landeshaushaltsordnung bleibt unberührt.

#### § 18  
Berichtspflichten gegenüber dem Ausschuss für Haushalt und Finanzen des Landtages

(1) Das Ministerium der Finanzen berichtet dem Ausschuss für Haushalt und Finanzen des Landtages

1.  mit Stand 30. Juni 2013 und 30. Juni 2014 im Rahmen eines Berichtes über wesentliche Kenngrößen der bereinigten Gesamteinnahmen und -ausgaben des Landes sowie über den aktuellen Mittelabfluss aus dem Landeshaushalt.
    In diesem Bericht sollen auch Angaben zur Entwicklung der Einnahmearten und der Ausgabearten insbesondere zur Umsetzung der EU-Fonds und zum Stand der Verschuldung sowie Prognosedaten der weiteren Entwicklung bis zum Jahresende enthalten sein;
2.  über die Jahresabschlüsse 2013 und 2014 im Rahmen eines Berichtes wie in Nummer 1 allerdings ohne Prognoseaussage;
3.  mit Stand 31.
    Dezember 2013 für das Haushaltsjahr 2013 und mit Stand 31.
    Dezember 2014 für das Haushaltsjahr 2014 über die Gewährung und Inanspruchnahme von Bürgschaften, Rückbürgschaften, Garantien und sonstigen Gewährleistungen durch das Land gemäß den §§ 3 und 4;
4.  mit Stand 31.
    Dezember 2013 und mit Stand 31.
    Dezember 2014 über die nach § 2 Absatz 4 abgeschlossenen Optimierungsgeschäfte.
    Der Bericht enthält eine Risikobewertung und eine Darstellung der anfallenden Kosten für das Land;
5.  mit Stand 31.
    Dezember 2013 im Rahmen eines Berichtes über die Beteiligungen des Landes.

(2) Die Ministerien berichten dem Ausschuss für Haushalt und Finanzen des Landtages

1.  zu den in Absatz 1 Nummer 1 und 2 genannten Stichtagen im Rahmen eines Berichtes über den Stand der Bewilligungen bei sämtlichen Titeln der Hauptgruppen 6 und 8 mit einem Ansatz ab 1 000 000 Euro und den aktuellen Mittelabfluss,
2.  zu den in Absatz 1 Nummer 1 und 2 genannten Stichtagen im Rahmen eines Berichtes über die Inanspruchnahme von Verpflichtungsermächtigungen,
3.  zu den in Absatz 1 Nummer 1 und 2 genannten Stichtagen im Rahmen eines Berichtes über die Inanspruchnahme von Ausgaberesten bei sämtlichen Titeln der Hauptgruppen 6 und 8 mit einem Ansatz ab 1 000 000 Euro,
4.  mit Stand 31.
    Mai 2013 und mit Stand 31.
    Mai 2014 im Rahmen eines Berichtes über die Besetzung der Planstellen und Stellen.

(3) Die Ministerien berichten dem Ausschuss für Haushalt und Finanzen des Landtages

1.  mit Stand 30.
    Juni 2013 und mit Stand 30.
    Juni 2014 im Rahmen eines Berichtes über den Stand der Entgeltzahlungen an die Investitionsbank des Landes Brandenburg im Zusammenhang mit der Wahrnehmung der Geschäftsbesorgung für die Bewilligung, Gewährung von Zuwendungen und zur Verwendungsnachweisprüfung,
2.  mit Stand 31.
    Dezember 2013 und mit Stand 31.
    Dezember 2014 im Rahmen eines Berichtes wie in Nummer 1.

(4) Das Ministerium für Wirtschaft und Europaangelegenheiten berichtet dem Ausschuss für Haushalt und Finanzen des Landtages

1.  zum 30. Juni 2013 und zum 30. Juni 2014 im Rahmen eines Berichtes über den Stand der Bewilligung von Fördermitteln aus der Gemeinschaftsaufgabe „Verbesserung der regionalen Wirtschaftsstruktur“.
    Der Bericht erfolgt in Form einer Übersicht der bewilligten Einzelförderungen mit einem Förderbetrag von mehr als 1 000 000 Euro.
    In der Übersicht sind die der Bewilligung zugrunde gelegten Kriterien und der Fördersatz anzugeben;
2.  zum 30. September 2013 und zum 30. September 2014 im Rahmen eines Berichtes wie in Nummer 1;
3.  zum 31. Dezember 2013 und zum 31. Dezember 2014 im Rahmen eines Berichtes wie in Nummer 1.

#### § 19  
Weitergeltung von Vorschriften und Ermächtigungen

Die Vorschriften und Ermächtigungen in den §§ 3, 4, 5, 6, 8 Absatz 1 und 2, §§ 11 bis 15 und 17 gelten bis zur Verkündung des Haushaltsgesetzes 2015 weiter.

#### § 20  
Inkrafttreten

Dieses Gesetz tritt am 1.
 Januar 2013 in Kraft.

Potsdam, den 18.
Dezember 2012

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

#### Anlage

#### Haushaltsplan des Landes Brandenburg  
für die Haushaltsjahre 2013 und 2014

#### Gesamtplan

I.
Haushaltsübersicht                                                (§ 13 Absatz 4 Nummer 1 LHO)

A. Zusammenfassung der Einnahmen und Ausgaben der Einzelpläne

B. Zusammenfassung der Verpflichtungsermächtigungen der Einzelpläne und deren Inanspruchnahme

II. Finanzierungsübersicht                                          (§ 13 Absatz 4 Nummer 2 LHO)

III.
Kreditfinanzierungsplan                                        (§ 13 Absatz 4 Nummer 3 LHO)

 

##### Teil I Haushaltsübersicht 2013

A.
 Zusammenfassung der Einnahmen und Ausgaben der Einzelpläne

Einzelplan

Einnahmen

Ausgaben

0

Einnahmen aus  
Steuern und  
steuerähnlichen  
Abgaben

1

Verwaltungseinnahmen,  
Einnahmen aus  
Schuldendienst  
und dgl.

2

Einnahmen aus  
Zuweisungen  
und Zuschüssen  
mit Ausnahme  
für  
Investitionen

3

Einnahmen aus  
Schuldenauf-  
nahmen, aus  
Zuweisungen  
und Zuschüssen  
für Investitionen,  
besondere  
Finanzierungs-  
einnahmen

Summe  
  
Einnahmen

4

Personalausgaben

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

1

2

3

4

5

6

7

01

24.500

128.500

135.000

288.000

23.291.600

02

13.600

123.700

137.300

11.815.300

03

51.139.000

13.051.300

64.190.300

434.814.500

**mehr(+) / weniger(-)**

**+253.600**

**03 neuer Ansatz**

**51.139.000**

**13.051.300**

**64.190.300**

**435.068.100**

04

103.206.300

27.667.200

212.700

131.086.200

248.350.600

05

2.620.000

23.104.400

8.981.300

34.705.700

996.687.000

06

7.996.300

128.800.700

71.117.200

207.914.200

33.976.200

07

15.349.800

118.551.000

22.683.600

156.584.400

47.756.800

08

12.472.700

1.786.100

295.114.400

309.373.200

24.439.900

10

10.000.000

26.793.600

3.135.100

55.364.500

95.293.200

80.159.400

11

1.060.000

12.423.700

531.173.300

318.363.900

863.020.900

64.159.500

12

21.226.800

19.263.200

3.071.700

43.561.700

183.524.700

**mehr(+) / weniger(-)**

**12 neuer Ansatz**

**21.226.800**

**19.263.200**

**3.071.700**

**43.561.700**

**183.524.700**

13

17.000

1.000.000

1.017.000

10.315.300

14

457.500

20

5.981.800.000

95.553.300

2.212.619.900

461.209.400

8.751.182.600

189.989.200

**mehr(+) / weniger(-)**

**\-339.731.200**

**\-339.731.200**

**\-572.400**

**20 neuer Ansatz**

**5.981.800.000**

**95.553.300**

**2.212.619.900**

**121.478.200**

**8.411.451.400**

**189.416.800**

Summe 2013

5.992.860.000

348.836.600

3.079.404.400

1.237.253.700

10.658.354.700

2.349.737.500

**mehr(+) / weniger(-)**

**\-339.731.200**

**\-339.731.200**

**\-318.800**

**Summe 2013 neu**

**5.992.860.000**

**348.836.600**

**3.079.404.400**

**897.522.500**

**10.318.623.500**

**2.349.418.700**

Summe 2012

5.689.560.000

364.285.000

3.040.542.600

1.097.176.100

10.191.563.700

2.267.520.400

**Vgl.
 zu 2012 neu**

**+303.300.000**

**\-15.448.400**

**+38.861.800**

**\-199.653.600**

**+127.059.800**

**+81.898.300**

 

##### Teil I Haushaltsübersicht 2013

A.
 Zusammenfassung der Einnahmen und Ausgaben der Einzelpläne

Einzelplan

Ausgaben

\+ Überschuss  
  
\- Zuschuss

5

Sächliche  
Verwaltungs-  
ausgaben und  
Ausgaben für  
den  
Schuldendienst

6

Ausgaben für  
Zuweisungen  
und Zuschüsse  
mit Ausnahme  
für  
Investitionen

7

Baumaßnahmen

8

Sonstige  
Ausgaben für  
Investitionen  
und  
Investitions-  
förderungs-  
maßnahmen

9

Besondere  
Finanzierungs-  
ausgaben

 Summe  
  
Ausgaben

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

1

8

9

10

11

12

13

14

01

4.612.700

7.010.500

3.809.900

206.300

38.931.000

\-38.643.000

02

3.153.700

211.800

119.000

46.900

15.346.700

\-15.209.400

03

165.490.800

9.109.400

9.471.200

19.042.600

6.063.000

643.991.500

\-579.801.200

**mehr(+) / weniger(-)**

**+600.000**

**+447.600**

**+1.301.200**

**\-1.301.200**

**03 neuer Ansatz**

**166.090.800**

**9.109.400**

**9.471.200**

**19.490.200**

**6.063.000**

**645.292.700**

**\-581.102.400**

04

157.181.400

29.729.800

418.500

4.645.200

3.163.700

443.489.200

\-312.403.000

05

13.696.400

455.297.100

12.424.000

23.129.100

1.501.233.600

\-1.466.527.900

06

9.526.700

598.988.100

90.501.800

5.274.400

738.267.200

\-530.353.000

07

11.467.500

604.981.100

3.635.000

20.428.900

688.269.300

\-531.684.900

08

17.152.500

117.110.200

1.300.000

320.280.200

290.400

480.573.200

\-171.200.000

10

45.974.800

77.861.700

15.035.500

158.785.300

\-15.269.800

362.546.900

\-267.253.700

11

45.483.300

775.977.600

41.912.000

414.600.000

\-14.643.500

1.327.488.900

\-464.468.000

12

48.056.500

25.866.700

108.554.500

1.596.400

367.598.800

\-324.037.100

**mehr(+) / weniger(-)**

**\-220.000**

**\-220.000**

**+220.000**

**12 neuer Ansatz**

**48.056.500**

**25.866.700**

**108.334.500**

**1.596.400**

**367.378.800**

**\-323.817.100**

13

1.264.000

2.000

518.900

19.500

12.119.700

\-11.102.700

14

214.900

672.400

\-672.400

20

696.494.100

2.568.328.100

2.000.000

564.038.000

16.976.900

4.037.826.300

+4.713.356.300

**mehr(+) / weniger(-)**

**\-140.700.000**

**+24.310.000**

**\-223.850.000**

**\-340.812.400**

**+1.081.200**

**20 neuer Ansatz**

**555.794.100**

**2.592.638.100**

**2.000.000**

**340.188.000**

**16.976.900**

**3.697.013.900**

**+4.714.437.500**

Summe 2013

1.219.769.300

5.270.474.100

70.137.200

1.700.954.400

47.282.200

10.658.354.700

0

**mehr(+) / weniger(-)**

**\-140.100.000**

**+24.310.000**

**\-223.622.400**

**\-339.731.200**

**0**

**Summe 2013 neu**

**1.079.669.300**

**5.294.784.100**

**70.137.200**

**1.477.332.000**

**47.282.200**

**10.318.623.500**

**0**

Summe 2012

1.198.135.800

5.218.782.300

106.201.100

1.384.476.200

16.447.900

10.191.563.700

0

**Vgl.
 zu 2012 neu**

**\-118.466.500**

**+76.001.800**

**\-36.063.900**

**+92.855.800**

**+30.834.300**

**+127.059.800**

**0**

 

##### **Teil I Haushaltsübersicht 2014**

A.
 Zusammenfassung der Einnahmen und Ausgaben der Einzelpläne

Einzelplan

Einnahmen

Ausgaben

0

Einnahmen aus  
Steuern und  
steuerähnlichen  
Abgaben

1

Verwaltungs-  
einnahmen,  
Einnahmen aus  
Schuldendienst  
und dgl.

2

Einnahmen aus  
Zuweisungen  
und Zuschüssen  
mit Ausnahme  
für  
Investitionen

3

Einnahmen aus  
Schuldenauf-  
nahmen, aus  
Zuweisungen  
und Zuschüssen  
für Investitionen,  
besondere  
Finanzierungs-  
einnahmen

Summe  
  
Einnahmen

4

Personal-  
ausgaben

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

1

2

3

4

5

6

7

01

24.500

128.500

600.000

753.000

25.154.600

02

13.600

87.700

101.300

11.886.900

03

51.259.000

7.758.600

59.017.600

434.465.800

**mehr(+) / weniger(-)**

**+1.176.400**

**03 neuer Ansatz**

**51.259.000**

**7.758.600**

**59.017.600**

**435.642.200**

04

103.193.500

27.231.500

212.700

130.637.700

246.517.200

**mehr(+) / weniger(-)**

**+12.000**

**04 neuer Ansatz**

**103.193.500**

**27.231.500**

**212.700**

**130.637.700**

**246.529.200**

05

2.500.500

22.856.700

25.357.200

985.087.500

**mehr(+) / weniger(-)**

**+16.250.000**

**05 neuer Ansatz**

**2.500.500**

**22.856.700**

**25.357.200**

**1.001.337.500**

06

7.996.300

131.648.000

70.393.700

210.038.000

35.017.800

07

15.262.900

150.724.000

20.042.600

186.029.500

46.800.800

**mehr(+) / weniger(-)**

**07 neuer Ansatz**

**15.262.900**

**150.724.000**

**20.042.600**

**186.029.500**

**46.800.800**

08

12.165.700

1.602.200

285.448.100

299.216.000

24.776.700

**mehr(+) / weniger(-)**

**+29.000.000**

**+29.000.000**

**08 neuer Ansatz**

**12.165.700**

**1.602.200**

**314.448.100**

**328.216.000**

**24.776.700**

10

10.000.000

26.727.300

2.140.500

55.364.500

94.232.300

79.546.100

11

1.060.000

14.536.200

490.546.900

212.432.800

718.575.900

63.966.800

**mehr(+) / weniger(-)**

**11 neuer Ansatz**

**1.060.000**

**14.536.200**

**490.546.900**

**212.432.800**

**718.575.900**

**63.966.800**

12

21.248.000

17.643.200

1.225.700

40.116.900

182.687.200

**mehr(+) / weniger(-)**

**12 neuer Ansatz**

**21.248.000**

**17.643.200**

**1.225.700**

**40.116.900**

**182.687.200**

13

8.000

500.000

508.000

10.578.300

14

20.100

20.100

492.300

20

6.197.700.000

84.789.700

2.118.676.900

59.867.900

8.461.034.500

248.361.700

**mehr(+) / weniger(-)**

**+43.800.000**

**\-22.010.000**

**+192.803.300**

**+214.593.300**

**\-7.205.600**

**20 neuer Ansatz**

**6.241.500.000**

**84.789.700**

**2.096.666.900**

**252.671.200**

**8.675.627.800**

**241.156.100**

Summe 2014

6.208.760.000

339.725.200

2.971.044.700

706.108.100

10.225.638.000

2.395.339.700

**mehr(+) / weniger(-)**

**+43.800.000**

**\-22.010.000**

**+221.803.300**

**+243.593.300**

**+10.232.800**

**Summe 2014 neu**

**6.252.560.000**

**339.725.200**

**2.949.034.700**

**927.911.400**

**10.469.231.300**

**2.405.572.500**

Summe 2013

5.992.860.000

348.836.600

3.079.404.400

897.522.500

10.318.623.500

2.349.418.700

Vgl.
 zu 2013 neu

**+259.700.000**

**\-9.111.400**

**\-130.369.700**

**+30.388.900**

**+150.607.800**

**+56.153.800**

 

##### **Teil I Haushaltsübersicht 2014**

A.
 Zusammenfassung der Einnahmen und Ausgaben der Einzelpläne

 Einzelplan

Ausgaben

\+ Überschuss  
  
\- Zuschuss

5

Sächliche  
Verwaltungs-  
ausgaben und  
Ausgaben für  
den  
Schuldendienst

6

Ausgaben für  
Zuweisungen  
und Zuschüsse  
mit Ausnahme  
für  
Investitionen

7

Baumaßnahmen

8

Sonstige  
Ausgaben für  
Investitionen  
und  
Investitions-  
förderungs-  
maßnahmen

9

Besondere  
Finanzierungs-  
ausgaben

Summe  
  
Ausgaben

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

1

8

9

10

11

12

13

14

01

4.742.500

6.836.500

1.045.000

206.300

37.984.900

\-37.231.900

02

3.470.600

451.800

35.300

46.900

15.891.500

\-15.790.200

03

157.145.700

11.882.200

171.700

5.821.500

8.040.500

617.527.400

\-558.509.800

**mehr(+) / weniger(-)**

**+7.575.000**

**+288.700**

**+9.040.100**

**\-9.040.100**

**03 neuer Ansatz**

**164.720.700**

**11.882.200**

**171.700**

**6.110.200**

**8.040.500**

**626.567.500**

**\-567.549.900**

04

156.416.900

29.954.800

203.400

4.582.900

4.301.800

441.977.000

\-311.339.300

**mehr(+) / weniger(-)**

**+12.000**

**\-12.000**

**04 neuer Ansatz**

**156.416.900**

**29.954.800**

**203.400**

**4.582.900**

**4.301.800**

**441.989.000**

**\-311.351.300**

05

14.047.700

458.071.000

2.142.700

29.866.700

1.489.215.600

\-1.463.858.400

**mehr(+) / weniger(-)**

**+693.600**

**+16.943.600**

**\-16.943.600**

**05 neuer Ansatz**

**14.047.700**

**458.764.600**

**2.142.700**

**29.866.700**

**1.506.159.200**

**\-1.480.802.000**

06

12.305.700

602.842.100

91.736.400

6.284.600

748.186.600

\-538.148.600

07

11.496.300

608.400.200

3.663.000

17.830.700

688.191.000

\-502.161.500

**mehr(+) / weniger(-)**

**+23.214.700**

**+23.214.700**

**\-23.214.700**

**07 neuer Ansatz**

**11.496.300**

**631.614.900**

**3.663.000**

**17.830.700**

**711.405.700**

**\-525.376.200**

08

15.758.600

132.366.900

1.000.000

276.392.800

290.400

450.585.400

\-151.369.400

**mehr(+) / weniger(-)**

**+29.000.000**

**+29.000.000**

**0**

**08 neuer Ansatz**

**15.758.600**

**132.366.900**

**1.000.000**

**305.392.800**

**290.400**

**479.585.400**

**\-151.369.400**

10

44.879.500

76.055.700

13.211.800

153.401.600

\-14.722.200

352.372.500

\-258.140.200

11

45.121.000

706.847.700

14.635.200

325.421.600

\-15.563.600

1.140.428.700

\-421.852.800

**mehr(+) / weniger(-)**

**+500.000**

**+500.000**

**\-500.000**

**11 neuer Ansatz**

**45.121.000**

**707.347.700**

**14.635.200**

**325.421.600**

**\-15.563.600**

**1.140.928.700**

**\-422.352.800**

12

46.122.200

25.056.400

88.431.500

2.252.800

344.550.100

\-304.433.200

**mehr(+) / weniger(-)**

**\-13.831.500**

**\-13.831.500**

**+13.831.500**

**12 neuer Ansatz**

**46.122.200**

**25.056.400**

**74.600.000**

**2.252.800**

**330.718.600**

**\-290.601.700**

13

877.900

2.000

198.100

19.500

11.675.800

\-11.167.800

14

224.400

716.700

\-696.600

20

728.809.000

2.579.068.100

2.000.000

318.604.000

9.492.000

3.886.334.800

+4.574.699.700

**mehr(+) / weniger(-)**

**\-139.970.000**

**+68.330.000**

**+10.000.000**

**+247.560.000**

**+178.714.400**

**+35.878.900**

**20 neuer Ansatz**

**588.839.000**

**2.647.398.100**

**12.000.000**

**566.164.000**

**9.492.000**

**4.065.049.200**

**+4.610.578.600**

Summe 2014

1.241.418.000

5.237.835.400

31.222.100

1.271.476.400

48.346.400

10.225.638.000

0

**mehr(+) / weniger(-)**

**\-132.395.000**

**+92.738.300**

**+10.000.000**

**+263.017.200**

**+243.593.300**

**Summe 2014 neu**

**1.109.023.000**

**5.330.573.700**

**41.222.100**

**1.534.493.600**

**48.346.400**

**10.469.231.300**

**0**

Summe 2013

1.079.669.300

5.294.784.100

70.137.200

1.477.332.000

47.282.200

10.318.623.500

0

**Vgl.
 zu 2013 neu**

**+29.353.700**

**+35.789.600**

**\-28.915.100**

**+57.161.600**

**+1.064.200**

**+150.607.800**

**0**

 

##### **Teil I Haushaltsübersicht 2013**

B.
 Zusammenfassung der Verpflichtungsermächtigungen der Einzelpläne und deren Inanspruchnahme

Einzel-  
plan

 Bezeichnung

 Ver-  
pflichtungs-  
ermächti-  
gungen

durch die Verpflichtungsermächtigung  
entstehende Rechtsverpflichtungen

2013

2014

2015

2016

2017 ff.

1.000 EUR

1

2

3

4

5

6

7

**01**

Landtag

**02**

Ministerpräsident und Staatskanzlei

350,0

350,0

**03**

Ministerium des Innern

130,0

70,0

60,0

**04**

Ministerium der Justiz

803,6

401,8

401,8

**05**

Ministerium für Bildung, Jugend und Sport

9.615,0

3.455,0

5.620,0

520,0

20,0

**06**

Ministerium für Wissenschaft, Forschung und Kultur

24.389,2

19.539,2

650,0

1.900,0

2.300,0

**07**

Ministerium für Arbeit, Soziales, Frauen und Familie

60.250,5

50.682,5

8.083,0

1.485,0

**08**

Ministerium für Wirtschaft und Europaangelegenheiten

212.306,4

76.142,9

68.905,2

67.258,3

**Es treten hinzu (+) oder fallen weg (-)**

**+43.000,0**

**+29.000,0**

**+14.000,0**

**Neuer Haushaltsansatz VE**

**255.306,4**

**105.142,9**

**82.905,2**

**67.258,3**

**10**

Ministerium für Umwelt, Gesundheit und Verbraucherschutz

68.664,5

41.511,6

16.612,9

8.040,0

2.500,0

**11**

Ministerium für Infrastruktur und Landwirtschaft

1.370.553,5

323.603,1

181.463,0

30.044,2

835.443,2

**12**

Ministerium der Finanzen

96.263,0

44.680,0

33.813,0

17.770,0

**Es treten hinzu (+) oder fallen weg (-)**

**\-40.200,0**

**\-14.600,0**

**\-15.900,0**

**\-9.700,0**

**Neuer Haushaltsansatz VE**

**56.063,0**

**30.080,0**

**17.913,0**

**8.070,0**

**13**

Landesrechnungshof

**14**

Verfassungsgericht des Landes Brandenburg

**20**

Allgemeine Finanzverwaltung

143.450,0

129.650,0

11.800,0

2.000,0

Zusammen

1.986.775,7

690.086,1

327.408,9

129.017,5

840.263,2

**Es treten hinzu (+) oder fallen weg (-)**

**+2.800,0**

**+14.400,0**

**\-1.900,0**

**\-9.700,0**

**Neuer Haushaltsansatz VE**

**1.989.575,7**

**704.486,1**

**325.508,9**

**119.317,5**

**840.263,2**

 

##### **Teil I Haushaltsübersicht 2014**

B.
 Zusammenfassung der Verpflichtungsermächtigungen der Einzelpläne und deren Inanspruchnahme

7

Einzel-  
plan

Bezeichnung

Verpflichtungs-  
ermächtigungen

durch die  
Verpflichtungsermächtigung  
entstehende Rechtsverpflichtungen

2013

2014

2015

2016

2017 ff.

1.000 EUR

1

2

3

4

5

6

7

**01**

Landtag

**02**

Ministerpräsident und Staatskanzlei

350,0

300,0

300,0

**03**

Ministerium des Innern

130,0

130,0

70,0

60,0

**04**

Ministerium der Justiz

803,6

900,0

600,0

300,0

**05**

Ministerium für Bildung, Jugend und Sport

9.615,0

9.495,0

3.455,0

5.520,0

520,0

**06**

Ministerium für Wissenschaft, Forschung und Kultur

24.389,2

19.089,2

17.989,2

400,0

700,0

**Es treten hinzu (+) oder fallen weg (-)**

**+22.265,0**

**+2.226,5**

**+2.226,5**

**+17.812,0**

**Neuer Haushaltsansatz VE**

**24.389,2**

**41.354,2**

**20.215,7**

**2.626,5**

**18.512,0**

**07**

Ministerium für Arbeit, Soziales, Frauen und Familie

60.250,5

37.433,7

29.035,7

5.803,0

2.595,0

**08**

Ministerium für Wirtschaft und Europaangelegenheiten

212.306,4

391.663,4

110.024,6

164.072,5

117.566,3

**Es treten hinzu (+) oder fallen weg (-)**

**+43.000,0**

**Neuer Haushaltsansatz VE**

**255.306,4**

**391.663,4**

**110.024,6**

**164.072,5**

**117.566,3**

**10**

Ministerium für Umwelt, Gesundheit und Verbraucherschutz

68.664,5

83.241,5

47.488,6

21.752,9

14.000,0

**11**

Ministerium für Infrastruktur und Landwirtschaft

1.370.553,5

750.912,0

185.124,8

66.082,0

499.705,2

**Es treten hinzu (+) oder fallen weg (-)**

**+500,0**

**+500,0**

**Neuer Haushaltsansatz VE**

**1.370.553,5**

**751.412,0**

**185.624,8**

**66.082,0**

**499.705,2**

**12**

Ministerium der Finanzen

96.263,0

22.163,0

10.850,0

11.313,0

**Es treten hinzu (+) oder fallen weg (-)**

**\-40.200,0**

**Neuer Haushaltsansatz VE**

**56.063,0**

**22.163,0**

**10.850,0**

**11.313,0**

**13**

Landesrechnungshof

**14**

Verfassungsgericht des Landes Brandenburg

**20**

Allgemeine Finanzverwaltung

143.450,0

31.600,0

17.800,0

11.800,0

2.000,0

Zusammen

1.986.775,7

1.346.927,8

422.737,9

287.103,4

637.086,5

**Es treten hinzu (+) oder fallen weg (-)**

**+2.800,0**

**+22.765,0**

**+2.726,5**

**+2.226,5**

**+17.812,0**

**Neuer Haushaltsansatz VE**

**1.989.575,7**

**1.369.692,8**

**425.464,4**

**289.329,9**

**654.898,5**

 

**Teil II Finanzierungsübersicht 2013**

Bisheriger  
Haushalts-  
ansatz  
2013  
(Mio EUR)

Es treten  
hinzu (+)  
oder  
fallen weg (-)  
(Mio EUR)

Neuer  
Haushalts-  
ansatz  
2013  
(Mio EUR)

**I.**

**HAUSHALTSVOLUMEN**

**10.658,4**

**\-339,7**

**10.318,6**

**II.**

**ERMITTLUNG DES FINANZIERUNGSSALDOS**

**1.**

**Ausgaben**

**10.580,6**

**\-339,7**

**10.240,9**

(ohne Ausgaben zur Schuldentilgung am Kreditmarkt, Zuführungen an Rücklagen, Ausgaben zur Deckung eines kassenmäßigen Fehlbetrags und haushaltstechnische Verrechnungen)

**2.**

**Einnahmen**

**10.160,9**

**\--**

**10.160,9**

(ohne Einnahmen aus Krediten vom Kreditmarkt, Entnahmen aus Rücklagen,  
Einnahmen aus kassenmäßigen Überschüssen und haushaltstechnischen Verrechnungen)

**3.**

**Finanzierungssaldo**

**\-419,7**

**+339,7**

**\-80,0**

**III.**

**AUSGLEICH DES FINANZIERUNGSSALDOS**

**4.**

**Nettoneuverschuldung am Kreditmarkt**

**330,0**

**\-249,1**

**81,0**

4.1

Einnahmen aus Krediten vom Kreditmarkt (brutto)

4.819,9

\-249,1

4.570,9

4.2

Ausgaben zur Schuldentilgung am Kreditmarkt

\-4.489,9

\--

\-4.489,9

4.21

planmäßige Tilgungen

\-3.289,9

\--

\-3.289,9

4.22

mögliche vorzeitige Tilgungen

\-500,0

\--

\-500,0

4.23

Tilgungen kurzfristiger Schulden

\-700,0

\--

\-700,0

**5.**

**Rücklagenbewegung**

**89,7**

**\-90,7**

**\-0,9**

5.1

Entnahmen aus Rücklagen

166,3

\-90,7

75,7

5.2

Zuführungen an Rücklagen

\-76,6

\--

\-76,6

**6.**

**Abwicklung der Vorjahre**

**\--**

**\--**

**\--**

6.1

Ausgaben zur Deckung kassenmäßiger Fehlbeträge

\--

\--

\--

6.2

Einnahmen aus kassenmäßigen Überschüssen

\--

\--

\--

**7.**

**Haushaltstechnische Verrechnungen**

**\--**

**\--**

**\--**

7.1

Ausgaben

\-1,1

\--

\-1,1

7.2

Einnahmen

1,1

\--

1,1

**Zusammen**

**419,7**

**\-339,7**

**80,0**

Abweichungen in den Summen ergeben sich durch Runden der Zahlen.

 

**Teil II Finanzierungsübersicht 2014**

Bisheriger  
Haushalts-  
ansatz  
2014  
(Mio EUR)

Es treten  
hinzu (+)  
oder  
fallen weg (-)  
(Mio EUR)

Neuer  
Haushalts-  
ansatz  
2014  
(Mio EUR)

**I.**

**HAUSHALTSVOLUMEN**

**10.225,6**

**+243,6**

**10.469,2**

**II.**

**ERMITTLUNG DES FINANZIERUNGSSALDOS**

**1.**

**Ausgaben**

**10.146,5**

**+243,6**

**10.390,1**

(ohne Ausgaben zur Schuldentilgung am Kreditmarkt, Zuführungen an Rücklagen, Ausgaben zur Deckung eines kassenmäßigen Fehlbetrags und haushaltstechnische Verrechnungen)

**2.**

**Einnahmen**

**10.133,5**

**+81,0**

**10.214,5**

(ohne Einnahmen aus Krediten vom Kreditmarkt, Entnahmen aus Rücklagen,  
Einnahmen aus kassenmäßigen Überschüssen und haushaltstechnischen Verrechnungen)

**3.**

**Finanzierungssaldo**

**\-13,0**

**\-162,6**

**\-175,6**

**III.**

**AUSGLEICH DES FINANZIERUNGSSALDOS**

**4.**

**Nettoneuverschuldung am Kreditmarkt**

**\--**

**\--**

**\--**

4.1

Einnahmen aus Krediten vom Kreditmarkt (brutto)

4.414,8

\--

4.414,8

4.2

Ausgaben zur Schuldentilgung am Kreditmarkt

\-4.414,8

\--

\-4.414,8

4.21

planmäßige Tilgungen

\-2.214,8

\--

\-2.214,8

4.22

mögliche vorzeitige Tilgungen

\-1.000,0

\--

\-1.000,0

4.23

Tilgungen kurzfristiger Schulden

\-1.200,0

\--

\-1.200,0

**5.**

**Rücklagenbewegung**

**13,0**

**+162,6**

**175,6**

5.1

Entnahmen aus Rücklagen

91,1

+162,6

253,7

5.2

Zuführungen an Rücklagen

\-78,1

\--

\-78,1

**6.**

**Abwicklung der Vorjahre**

**\--**

**\--**

**\--**

6.1

Ausgaben zur Deckung kassenmäßiger Fehlbeträge

\--

\--

\--

6.2

Einnahmen aus kassenmäßigen Überschüssen

\--

\--

\--

**7.**

**Haushaltstechnische Verrechnungen**

**\--**

**\--**

**\--**

7.1

Ausgaben

\-1,0

\--

\-1,0

7.2

Einnahmen

1,0

\--

1,0

**Zusammen**

**13,0**

**+162,6**

**175,6**

Abweichungen in den Summen ergeben sich durch Runden der Zahlen.

 

**Teil III Kreditfinanzierungsplan 2013**

Bisheriger  
Haushalts-  
ansatz  
2013  
(Mio EUR)

Es treten  
hinzu (+)  
oder  
fallen weg (-)  
(Mio EUR)

Neuer  
Haushalts-  
ansatz  
2013  
(Mio EUR)

**I.**

**EINNAHMEN AUS KREDITEN**

bei Gebietskörperschaften, Sondervermögen usw.

\--

\--

\--

vom Kreditmarkt

4.819,9

\-249,1

4.570,9

Zusammen

4.819,9

\-249,1

4.570,9

**II.**

**TILGUNGSAUSGABEN FÜR KREDITE**

bei Gebietskörperschaften, Sondervermögen usw.

\--

\--

\--

vom Kreditmarkt

4.489,9

\--

4.489,9

Zusammen

4.489,9

\--

4.489,9

**III.**

**NETTONEUVERSCHULDUNG insgesamt**

bei Gebietskörperschaften, Sondervermögen usw.

\--

\--

\--

vom Kreditmarkt

330,0

\-249,1

81,0

Zusammen

330,0

\-249,1

81,0

Abweichungen in den Summen ergeben sich durch Runden der Zahlen.

 

**Teil III Kreditfinanzierungsplan 2014**

Bisheriger  
Haushalts-  
ansatz  
2014  
(Mio EUR)

Es treten  
hinzu (+)  
oder  
fallen weg (-)  
(Mio EUR)

Neuer  
Haushalts-  
ansatz  
2014  
(Mio EUR)

**I.**

**EINNAHMEN AUS KREDITEN**

bei Gebietskörperschaften, Sondervermögen usw.

\--

\--

\--

vom Kreditmarkt

4.414,8

\--

4.414,8

Zusammen

4.414,8

\--

4.414,8

**II.**

**TILGUNGSAUSGABEN FÜR KREDITE**

bei Gebietskörperschaften, Sondervermögen usw.

\--

\--

\--

vom Kreditmarkt

4.414,8

\--

4.414,8

Zusammen

4.414,8

\--

4.414,8

**III.**

**NETTONEUVERSCHULDUNG insgesamt**

bei Gebietskörperschaften, Sondervermögen usw.

\--

\--

\--

vom Kreditmarkt

\--

\--

\--

Zusammen

\--

\--

\--

Abweichungen in den Summen ergeben sich durch Runden der Zahlen.