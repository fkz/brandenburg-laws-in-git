## Verordnung über die Bestimmung der Landeshauptkasse als Vollstreckungsbehörde nach dem Justizbeitreibungsgesetz

Auf Grund des § 2 Absatz 1 Satz 2 des Justizbeitreibungsgesetzes in der Fassung der Bekanntmachung vom 27.
Juni 2017 (BGBl.
I S. 1926) in Verbindung mit § 6 Absatz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28 S. 2) geändert worden ist, verordnet die Landesregierung:

#### § 1 

Die Landeshauptkasse wird für diejenigen Ansprüche, deren Beitreibung nach dem Justizbeitreibungsgesetz den Gerichtskassen obliegt, als Vollstreckungsbehörde bestimmt.

#### § 2 

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über die Bestimmung der Landeshauptkasse als Vollstreckungsbehörde nach der Justizbeitreibungsordnung vom 18.
Oktober 2006 (GVBl.
II S. 447) außer Kraft.

Potsdam, den 23.
April 2018

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

  
Der Minister der Finanzen

Christian Görke