## Verordnung zur Übertragung von Ermächtigungen zur Durchführung des Forstvermehrungsgutgesetzes

Auf Grund des § 7 Absatz 4 Satz 2 des Forstvermehrungsgutgesetzes vom 22.
Mai 2002 (BGBl.
I S. 1658), des § 9 Absatz 4 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
 I S. 186) und des § 36 Absatz 2 Satz 2 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl.
I S. 602) verordnet die Landesregierung:

#### § 1   
Übertragung von Ermächtigungen

Die Ermächtigungen der Landesregierung zum Erlass von Rechtsverordnungen nach § 7 Absatz 4 Satz 1 des Forstvermehrungsgutgesetzes, nach § 9 Absatz 2 des Landesorganisationsgesetzes und nach § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten werden auf das für Forstwirtschaft zuständige Mitglied der Landesregierung übertragen.

#### § 2  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 23.
 Juli 2010

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident  
Matthias Platzeck

Der Minister für Infrastruktur und Landwirtschaft  
In Vertretung  
Rainer Bretschneider