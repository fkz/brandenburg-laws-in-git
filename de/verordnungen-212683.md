## Verordnung über Ausnahmen von Vorschriften des Jugendarbeitsschutzgesetzes für jugendliche Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte (JASchGPV)

### § 1 

Für jugendliche Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte, die während ihres Berufspraktikums zu Ausbildungszwecken dem Polizeipräsidium zugewiesen werden, werden die nachfolgenden Ausnahmen von den Vorschriften des Jugendarbeitsschutzgesetzes zugelassen, soweit dies zum Erreichen des Ausbildungszieles erforderlich ist.

### § 2 

(1) Die regelmäßige tägliche Arbeitszeit darf höchstens zehneinhalb Stunden betragen.
Bei einer Arbeitszeit von mehr als acht Stunden täglich sind mindestens zwei Ruhepausen von insgesamt 90 Minuten, davon eine Pause von mindestens 45 Minuten Dauer, vorzusehen.
Die Ruhepausen sind nicht auf die Arbeitszeit anzurechnen und müssen in angemessener zeitlicher Lage gewährt werden, nach Möglichkeit frühestens zwei Stunden nach Beginn und spätestens zwei Stunden vor Ende der Arbeitszeit.

(2) Während des Berufspraktikums können jugendliche Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte im Schichtdienst eingesetzt werden.
Der Schichtdienst darf einschließlich der Ruhepausen maximal zwölf Stunden dauern.

(3) Eine Beschäftigung in der Nacht in der Zeit von 20 Uhr bis 6 Uhr ist zulässig.
Im Anschluss an eine Beschäftigung in der Nacht ist eine ununterbrochene Freizeit von mindestens zwölf Stunden zu gewähren.

(4) Von der Fünf-Tage-Woche sowie der wöchentlichen Höchstarbeitszeit von 40 Stunden kann im Rahmen festgelegter Schichtpläne abgewichen werden, sofern die regelmäßige wöchentliche Arbeitszeit im Durchschnitt 44 Stunden nicht überschreitet.
Mehrarbeit, die jugendliche Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte über die Arbeitszeit nach § 8 Absatz 1 des Jugendarbeitsschutzgesetzes hinaus leisten, ist innerhalb von vier Wochen durch Dienstbefreiung auszugleichen.
Unbeschadet der Regelung in Satz 2 muss bis zum Ende des Praktikums jede Mehrarbeit durch Dienstbefreiung abgegolten sein.

(5) Die Beschäftigung von jugendlichen Polizeivollzugsbeamtinnen und Polizeivollzugsbeamten an Samstagen, Sonntagen und Feiertagen ist im Rahmen festgelegter Schichtpläne grundsätzlich zulässig.
An mindestens zwei Wochenenden im Monat dürfen die jugendlichen Polizeivollzugsbeamtinnen und Polizeivollzugsbeamten nicht beschäftigt werden.
Die gesetzlichen Feiertage sollen beschäftigungsfrei bleiben.

### § 3 

(1) Auf die psychische und physische Leistungsfähigkeit der jugendlichen Polizeivollzugsbeamtinnen und Polizeivollzugsbeamten ist neben dem Ausbildungsstand besonders Rücksicht zu nehmen.

(2) Die Beauftragung jugendlicher Polizeivollzugsbeamtinnen und Polizeivollzugsbeamter mit Dienstgeschäften, bei denen Leben, Gesundheit oder die körperliche oder seelisch-geistige Entwicklung gefährdet werden können, ist nur zulässig, wenn dies zum Erreichen des Ausbildungszieles erforderlich und der Schutz der Jugendlichen durch die Aufsicht eines Fachkundigen sichergestellt ist.