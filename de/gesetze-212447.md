## Gesetz über die Feststellung des Haushaltsplanes des Landes Brandenburg für das Haushaltsjahr 2010 (Haushaltsgesetz 2010 - HG 2010)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1  
Feststellung des Haushaltsplanes

Der diesem Gesetz als Anlage beigefügte Haushaltsplan des Landes Brandenburg für das Haushaltsjahr 2010 wird in Einnahmen und Ausgaben festgestellt auf 10 511 488 700 Euro.
Der Gesamtbetrag der Verpflichtungsermächtigungen wird festgestellt auf 3 053 457 800 Euro.

#### § 2  
Kreditermächtigungen

(1) Das Ministerium der Finanzen wird ermächtigt, zur Deckung von Ausgaben Kredite aufzunehmen bis zur Höhe von 650 856 200 Euro.

(2) Der Kreditermächtigung nach Absatz 1 wachsen die Beträge zur Tilgung von im Haushaltsjahr 2010 fällig werdenden Krediten zu, deren Höhe sich aus den Finanzierungsübersichten ergibt.

(3) Über die Kreditermächtigung nach Absatz 1 hinaus darf das Ministerium der Finanzen zur Vorfinanzierung von Ausgaben, die aus den Fonds der Europäischen Union nachträglich erstattet werden, Kredite bis zur Höhe von insgesamt 200 000 000 Euro aufnehmen.
Die nach Satz 1 aufgenommenen Kredite sind mit den Erstattungen aus den Fonds zu tilgen.

(4) Im Rahmen der Kreditfinanzierung kann das Ministerium der Finanzen auch ergänzende Vereinbarungen treffen, die der Begrenzung von Zinsänderungsrisiken, der Erzielung günstigerer Konditionen und ähnlichen Zwecken bei neuen Krediten und bestehenden Schulden dienen.
Das Ministerium der Finanzen wird ermächtigt, Darlehen vorzeitig zu tilgen oder Kredite mit unterjähriger Laufzeit aufzunehmen, soweit dies im Zuge von Zinsanpassungen oder zur Erlangung günstigerer Konditionen notwendig wird.
Die Kreditermächtigung nach Absatz 1 erhöht sich in Höhe der nach Satz 2 getilgten Beträge.
Diese Ermächtigung gilt auch für die im Wirtschaftsplan des Landeswohnungsbauvermögens vorgesehene Kreditaufnahme.

(5) Das Ministerium der Finanzen wird ermächtigt, ab Oktober des Haushaltsjahres im Vorgriff auf die Ermächtigung des nächsten Haushaltsjahres Kredite bis zur Höhe von 8 Prozent des in § 1 Satz 1 festgestellten Betrages aufzunehmen.
Die hiernach aufgenommenen Kredite sind auf die Kreditermächtigung des nächsten Haushaltsjahres anzurechnen.

(6) Der Zeitpunkt der Kreditaufnahme ist nach der Kassenlage, den jeweiligen Kapitalmarktverhältnissen und gesamtwirtschaftlichen Erfordernissen zu bestimmen.

(7) Das Ministerium der Finanzen wird ermächtigt, zur Aufrechterhaltung einer ordnungsgemäßen Kassenwirtschaft im Haushaltsjahr 2010 bis zur Höhe von 12 Prozent des in § 1 Satz 1 festgestellten Betrages zuzüglich der nach Absatz 1 noch nicht in Anspruch genommenen Kreditermächtigungen Kassenverstärkungsmittel aufzunehmen.
Soweit diese Kredite zurückgezahlt sind, kann die Ermächtigung wiederholt in Anspruch genommen werden.

#### § 3  
Bürgschaften und Rückbürgschaften

(1) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2010 Bürgschaften für Kredite an die Wirtschaft, die freien Berufe und Einrichtungen der freien Wohlfahrtspflege sowie die Land- und Forstwirtschaft bis zur Höhe von insgesamt 200 000 000 Euro zu übernehmen.

(2) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2010 Bürgschaften für Kredite zur Förderung des Wohnungsbaus und des Stadtumbaus bis zur Höhe von 10 000 000 Euro zu übernehmen.

(3) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2010 zur Absicherung von Krediten an Dritte für Investitionen des Landes im Rahmen von Sonderfinanzierungen nach § 9 Bürgschaften oder Sicherheitserklärungen bis zu einer Höhe von 30 000 000 Euro zugunsten der Investitionsbank des Landes Brandenburg oder der finanzierenden Einrichtungen zu übernehmen.

(4) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2010 Bürgschaften im Falle eines unvorhergesehenen und unabweisbaren Bedürfnisses, insbesondere für Notmaßnahmen im Land Brandenburg, bis zur Höhe von 15 000 000 Euro zu übernehmen.
Überschreitet die aufgrund dieser Ermächtigung zu übernehmende Bürgschaft im Einzelfall den Betrag von 5 000 000 Euro, bedarf es der Einwilligung des Ausschusses für Haushalt und Finanzen des Landtages.

(5) Bürgschaften gemäß den Absätzen 1 bis 3 dürfen nur für Kredite übernommen werden, deren Rückzahlung durch den Schuldner bei normalem wirtschaftlichem Ablauf innerhalb der für den einzelnen Kredit vereinbarten Zahlungstermine erwartet werden kann.

#### § 4  
Garantien und sonstige Gewährleistungen

(1) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2010 im Interesse der Kapitalversorgung kleiner und mittelständischer Unternehmen Garantien bis zur Höhe von 20 000 000 Euro für die Übernahme von Kapitalbeteiligungen zu übernehmen.
Diese Garantien können auch als Rückgarantien gegenüber Kreditinstituten übernommen werden.

(2) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2010 Garantien zur Finanzierung von Film- und Fernsehproduktionen sowie Projektentwicklungen im Medienbereich bis zur Höhe von 15 000 000 Euro zu übernehmen.

(3) Das Ministerium der Finanzen wird ermächtigt, unter Anrechnung auf die Ermächtigungen gemäß Absatz 1 und 2 Garantien zur Finanzierung von Produktionen, Projektentwicklungen und Existenzgründungen im Bereich der Kultur- und Kreativwirtschaft zu übernehmen.

(4) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2010 zur Absicherung von Risiken, die sich aus dem Betrieb von kerntechnischen Anlagen und dem Umgang mit radioaktiven Stoffen in Forschungseinrichtungen des Landes ergeben, Gewährleistung bis zur Höhe von 5 000 000 Euro zu übernehmen.

(5) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2010 zur Deckung des Haftpflichtrisikos von Zuwendungsempfängern des Landes aus der Haftung für Leihgaben im Bereich Kunst und Kultur sowie für wissenschaftliche Forschungsinstitute, die vom Bund und vom Land gemeinsam getragen werden, Garantien bis zum Höchstbetrag von 5 000 000 Euro zu übernehmen.

(6) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2010 zur Absicherung von Risiken, die sich aus der Tätigkeit der Ethikkommission der Landesärztekammer Brandenburg nach _§ 7 Absatz 1 Heilberufsgeset_z ergeben, Gewährleistungen bis zur Höhe von 5 000 000 Euro zu übernehmen.

(7) Haftungsfreistellungen und Garantien gemäß den Absätzen 1 und 2 dürfen nur unter den in § 3 Absatz 5 genannten Voraussetzungen übernommen werden.

#### § 5  
Grundsätze für neue Steuerungsinstrumente

(1) In den Einzelplänen 02 bis 12 werden aus den Personalausgaben je Einzelplan Personalbudgets gebildet.
In den Einzelplänen 02 bis 12 sowie im Einzelplan 20 werden aus den sächlichen Verwaltungsausgaben, den Ausgaben für den Erwerb beweglicher Sachen und den Verwaltungseinnahmen je Einzelplan Verwaltungsbudgets gebildet.

(2) Das Personalbudget umfasst mit Ausnahme der Gruppen 432 und 453 die Ausgaben der Hauptgruppe 4.
Diese sind innerhalb des Einzelplans gegenseitig deckungsfähig, davon ausgenommen ist das Kapitel 05 302 (Personalkostenausgleichsfonds).
Rücklagen aus dem Vorjahr dürfen zur Verstärkung der Ausgaben verwendet werden; vorgezogene Entnahmen im Vorjahr sind durch Minderausgaben im laufenden Haushaltsjahr auszugleichen.
Wird das Personalbudget beim Jahresabschluss über- oder unterschritten, kann der Betrag bis zur Höhe der Über- oder Unterschreitung auf das Personalbudget für den nächsten Haushalt vorgetragen werden.

(3) Die Ausgaben der Gruppe 453 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.
Das jeweilige Personalbudget ist einseitig deckungsfähig zugunsten der Ausgaben der Gruppe 453.
Die Ausgaben der Gruppe 432 sind über alle Einzelpläne gegenseitig deckungsfähig.

(4) Das Verwaltungsbudget umfasst die Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Titel 518 25 und der Gruppe 529, und der Obergruppe 81 und die Einnahmen der Obergruppen 11 bis 13.
Die Ausgaben sind innerhalb des Einzelplans gegenseitig deckungsfähig.
Das jeweilige Verwaltungsbudget ist einseitig deckungsfähig zugunsten des Titels 518 25.
 Rücklagen aus Vorjahren dürfen zur Verstärkung der Ausgaben verwendet werden.
 Wird das Verwaltungsbudget beim Jahresabschluss über- oder unterschritten, kann der Betrag bis zur Höhe der Über- oder Unterschreitung auf das Verwaltungsbudget für den nächsten Haushalt vorgetragen werden.
Einzelne Einnahmen und Ausgaben können vom Verwaltungsbudget ausgenommen werden.

(5) Mehreinnahmen bei den Obergruppen 11 bis 13 können zur Verstärkung der Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Gruppe 529, und der Obergruppe 81 im Rahmen des Verwaltungsbudgets verwendet werden, wenn ein verwaltungsmäßiger oder sachlicher Zusammenhang besteht oder eine wirtschaftliche und sparsame Verwendung gefördert wird.
Minderausgaben beim Personalbudget können zur Verstärkung der Ausgaben des Verwaltungsbudgets im jeweiligen Einzelplan verwendet werden, soweit sich daraus keine Überschreitung des Personalbudgets beim Jahresabschluss ergibt.

(6) Minderausgaben beim Verwaltungsbudget können zur Verstärkung der Ausgaben bei Kapitel 12 020 Titel 891 61 - Zuführungen für Investitionen - herangezogen werden.

(7) Die allein aus Landesmitteln finanzierten und nicht zur Komplementärfinanzierung von Drittmitteln bestimmten Ausgaben der Hauptgruppe 6 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.
Ebenso sind die allein aus Landesmitteln finanzierten und nicht zur Komplementärfinanzierung von Drittmitteln bestimmten Ausgaben der Obergruppen 83 bis 89 innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.

(8) Für die Wirtschaftspläne der Landesbetriebe nach § 26 Absatz 1 der Landeshaushaltsordnung gelten die vorstehenden Absätze entsprechend, soweit keine besonderen Regelungen getroffen sind.

(9) Die im Einzelplan 06 veranschlagten Universitäten und Fachhochschulen werden jeweils nur mit ihrem Zuschussbedarf veranschlagt.
Die Einnahmen und Ausgaben dieser Einrichtungen werden in Wirtschaftsplänen veranschlagt, die dem Haushaltsplan als Erläuterungen beigefügt sind.
Für die Bewirtschaftung gelten die Absätze 1 bis 6 entsprechend, soweit keine besonderen Regelungen getroffen sind.

(10) Das Nähere regelt das Ministerium der Finanzen.

#### § 6  
Neue Steuerungsinstrumente im Bereich des Landtages, Verfassungsgerichts und Landesrechnungshofes

(1) Gegenseitig deckungsfähig sind innerhalb der Einzelpläne 01, 13 und 14 die Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Titel 518 25 und der Gruppe 529, und der Obergruppe 81.
Das jeweilige Verwaltungsbudget ist einseitig deckungsfähig zugunsten des Titels 518 25.
Werden die Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Gruppe 529, und der Obergruppe 81 beim Jahresabschluss unterschritten, kann der Betrag in Höhe der Unterschreitung einer Rücklage zugeführt werden.
Sofern es beim Jahresabschluss zu einer Überschreitung kommt, kann der Betrag in Höhe der Überschreitung in den nächsten Haushalt vorgetragen werden.
Rücklagen aus dem Vorjahr dürfen zur Verstärkung der entsprechenden Ausgaben verwendet werden.

(2) Nicht verausgabte Mittel der Titelgruppe 99 - Kosten für Datenverarbeitung - können bei Unterschreitung der veranschlagten Ausgaben in Höhe der Unterschreitung einer Rücklage zugeführt werden.
Auf die Bildung dieser Rücklage ist Absatz 1 nicht anzuwenden.
Innerhalb der Titelgruppe 99 dürfen Einnahmen, die der für Datenverarbeitung gebildeten Rücklage entnommen werden, zur Deckung von Mehrausgaben verwendet werden.

(3) Für die Ausgaben der Hauptgruppe 4, mit Ausnahme der Ausgaben der Gruppe 411 - Aufwendungen für Abgeordnete - im Kapitel 01 010 und der Gruppen 432 und 453, wird innerhalb des jeweiligen Einzelplans ein Personalbudget gebildet.
Die Ausgaben sind innerhalb des Personalbudgets gegenseitig deckungsfähig, davon ausgenommen sind die Ausgaben der Gruppe 453.
 Rücklagen aus dem Vorjahr dürfen zur Verstärkung der Ausgaben verwendet werden; vorgezogene Entnahmen im Vorjahr sind durch Minderausgaben im laufenden Haushaltsjahr auszugleichen.
Wird das Personalbudget beim Jahresabschluss über- oder unterschritten, kann der Betrag bis zur Höhe der Über- oder Unterschreitung auf das Personalbudget für den nächsten Haushalt vorgetragen werden.

(4) Die Ausgaben der Gruppe 453 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.
Das jeweilige Personalbudget ist einseitig deckungsfähig zugunsten der Ausgaben der Gruppe 453.
Die Ausgaben der Gruppe 432 sind über alle Einzelpläne gegenseitig deckungsfähig.

(5) Mehreinnahmen bei den Obergruppen 11 bis 13 können zur Verstärkung der Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Gruppe 529, und der Obergruppe 81 verwendet werden, wenn ein verwaltungsmäßiger oder sachlicher Zusammenhang besteht oder eine wirtschaftliche und sparsame Verwendung gefördert wird.
Minderausgaben beim Personalbudget können zur Verstärkung der in Satz 1 bezeichneten Ausgaben im jeweiligen Einzelplan verwendet werden, soweit sich daraus keine Überschreitung des Personalbudgets beim Jahresabschluss ergibt.

#### § 7  
Besondere Regelungen für den Brandenburgischen Landesbetrieb für Liegenschaften und Bauen (BLB)

(1) Das Ministerium der Finanzen wird ermächtigt, nach Bestätigung des Wirtschaftsplans für den Landesbetrieb Einnahmen, Ausgaben, Verpflichtungsermächtigungen, Planstellen und Stellen in den Landesbetrieb umzusetzen, soweit weitere Liegenschaften in die Teilnahme am Vermieter-Mieter-Modell überführt werden.

(2) Die Ansätze bei den Titeln 518 25 sind bis zum Abschluss der jeweiligen Mietverträge mit dem BLB gesperrt.
Von dieser Sperre sind Ausgaben nicht erfasst, die im Zusammenhang mit der Bewirtschaftung der Liegenschaften stehen.

(3) Nicht veranschlagte Ausgaben für Mieten nach dem Vermieter-Mieter-Modell beim Titel 518 25 stellen keine Mehrausgaben nach § 37 der Landeshaushaltsordnung dar.
Sie können vom Ministerium der Finanzen zugelassen werden, wenn sie durch Minderausgaben oder Mehreinnahmen an anderer Stelle gedeckt sind.

(4) Die Ansätze des Titels 518 25 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.

(5) Vom BLB zurückgezahlte Beträge aus der Abrechnung von Betriebs- und Nebenkosten sind bei Titel 518 25 abzusetzen.

#### § 8  
Mehrausgaben, Komplementärmittel

(1) Der gemäß § 37 Absatz 1 Satz 4 der Landeshaushaltsordnung festzulegende Betrag wird auf 7 500 000 Euro Landesmittel festgesetzt, für Verpflichtungsermächtigungen (§ 38 Absatz 1 Satz 3 der Landeshaushaltsordnung) als Jahresbetrag.
Überschreiten Mehrausgaben im Einzelfall den Betrag von 5 000 000 Euro Landesmittel, bei Verpflichtungsermächtigungen als jährlich fällig werdender Betrag, ist die Einwilligung des Ausschusses für Haushalt und Finanzen des Landtages einzuholen.

(2) Eines Nachtragshaushaltsgesetzes bedarf es zudem nicht, wenn

1.  Komplementärmittel von der Europäischen Union oder vom Bund unvorhergesehen bereitgestellt werden, die eine zusätzliche anteilige Finanzierung durch das Land erforderlich machen, oder
    
2.  Umschichtungen innerhalb eines Fonds der Europäischen Union oder zwischen den Fonds, einschließlich der Kofinanzierung durch das Land, erforderlich sind.
    

In den Fällen des Satzes 1 Nummer 2 bedarf es der Einwilligung des Ausschusses für Haushalt und Finanzen, wenn die Umschichtungen im Einzelfall 5 000 000 Euro EU- und Landesmittel, bei Verpflichtungsermächtigungen als jährlich fällig werdender Betrag, überschreiten.

(3) Veranschlagte Landesmittel und Verpflichtungsermächtigungen, die nicht mehr zur Kofinanzierung von Leistungen Dritter für die gemäß Haushaltsplan vorgesehenen Zwecke erforderlich sind, sind gesperrt.
Die Aufhebung der Sperre bedarf der Zustimmung des Ministeriums der Finanzen.
Das Ministerium der Finanzen wird ermächtigt, die Vorfinanzierung von Maßnahmen, für die die Leistung von Dritten vorgesehen ist, zuzulassen.

(4) Im Bereich der Fonds der Europäischen Union dürfen mit Einwilligung des Ministeriums der Finanzen Mehrausgaben bis zur Höhe der Minderausgaben aus Vorjahren geleistet werden, soweit die zugehörigen Erstattungsanträge an die EU-Kommission bis spätestens zum II.
Quartal des Folgejahres gestellt werden oder die Mehrausgaben zur Kofinanzierung von Mitteln aus den Fonds dienen.

#### § 9  
Sonderfinanzierungen

(1) Durch den Abschluss von Leasing-, Mietkauf- und ähnlichen Verträgen (Sonderfinanzierungen) für Bauinvestitionen dürfen Verpflichtungen zulasten künftiger Haushaltsjahre eingegangen werden.
Diese Befugnis gilt auch bei Umsetzung von Bauinvestitionen im Rahmen von Öffentlich Privaten Partnerschaften, die auch die Betriebsphase umfassen (Lebenszyklusansatz).
Das Ministerium der Finanzen wird ermächtigt, mit Zustimmung des Ausschusses für Haushalt und Finanzen des Landtages Sonderfinanzierungen zuzulassen; § 38 Absatz 1 der Landeshaushaltsordnung bleibt unberührt.

(2) Verpflichtungsermächtigungen für Investitionsfinanzierungen dürfen abweichend von § 8 Absatz 1 bis zu der Höhe überschritten werden, in der sie für Maßnahmen nach Absatz 1 Satz 1 benötigt werden.

(3) Die Wirtschaftlichkeit von Sonderfinanzierungen ist in jedem Einzelfall zu belegen.

#### § 10  
Industrieansiedlungsverträge

Soweit die veranschlagten Ausgaben bei voller Ausschöpfung der Deckungsfähigkeit und die Verpflichtungsermächtigungen nicht ausreichen, Industrieansiedlungsverträge mit finanziellen Verpflichtungen für das Land abzuschließen, ist das Ministerium für Wirtschaft und Europaangelegenheiten ermächtigt, über Industrieansiedlungsverträge zu verhandeln und - bei Zustimmung des Ministeriums der Finanzen und nach Einwilligung des Ausschusses für Haushalt und Finanzen im Benehmen mit dem Ausschuss für Wirtschaft des Landtages - zusätzliche Verpflichtungen zulasten des Landes einzugehen.

#### § 11   
Besondere Regelungen für Zuwendungen

(1) Ausgaben und Verpflichtungsermächtigungen für Zuwendungen im Sinne des § 23 der Landeshaushaltsordnung zur Deckung der gesamten Ausgaben oder eines nicht abgegrenzten Teils der Ausgaben einer Stelle außerhalb der Landesverwaltung (institutionelle Förderung), bei der gemessen am Gesamtetat der Einrichtung der Zuwendungsbedarf vom Land zu mindestens 50 Prozent gedeckt wird, sind gesperrt, bis der Haushalts- oder Wirtschaftsplan des Zuwendungsempfängers von dem zuständigen Ministerium gebilligt worden ist.

(2) Die in Absatz 1 genannten Zuwendungen zur institutionellen Förderung dürfen nur mit der Auflage bewilligt werden, dass der Zuwendungsempfänger seine Beschäftigten nicht besser stellt als vergleichbare Bedienstete des Landes; vorbehaltlich einer abweichenden tarifvertraglichen Regelung dürfen deshalb keine günstigeren Arbeitsbedingungen vereinbart werden, als sie für Bedienstete des Landes jeweils vorgesehen sind.
Entsprechendes gilt bei Zuwendungen zur Projektförderung, wenn die Gesamtausgaben des Zuwendungsempfängers überwiegend aus Zuwendungen der öffentlichen Hand bestritten werden.
Das Ministerium der Finanzen kann bei Vorliegen zwingender Gründe Ausnahmen zulassen.

(3) Die in den Erläuterungen zu den Titeln, aus denen Zuwendungen im Sinne des § 23 der Landeshaushaltsordnung zur institutionellen Förderung geleistet werden, für andere als Projektaufgaben ausgebrachte Planstellen für Beamte sowie Stellen für Arbeitnehmer sind hinsichtlich der Gesamtzahl und der Zahl der für die einzelnen Besoldungs- und Entgeltgruppen ausgebrachten Planstellen und Stellen verbindlich.
Das Ministerium der Finanzen wird ermächtigt, Ausnahmen von der Verbindlichkeit der Stellenpläne zuzulassen.
 Die Wertigkeit außertariflicher Stellen ist durch die Angabe der entsprechenden Besoldungsgruppe zu kennzeichnen.
Das Ministerium der Finanzen kann Abweichungen in den Wertigkeiten der Stellen zulassen.
Sind im Wirtschaftsplan Stellen außerhalb der Anlagen B 2 und B 3 zum Tarifvertrag der Länder (TV-L) ohne Angaben des Entgelts ausgebracht, bedarf die Festsetzung des Entgelts in jedem Einzelfall der vorherigen Zustimmung des Ministeriums der Finanzen.
Sonstige Abweichungen bedürfen der Einwilligung des Ministeriums der Finanzen und setzen eine Tätigkeitsdarstellung voraus.

#### § 12  
Personalwirtschaftliche Regelungen

(1) Zur Einhaltung des Stellenplans gemäß der gültigen Personalbedarfsplanung des Landes Brandenburg und des Personalbudgets sind die Ressorts verpflichtet, alle Möglichkeiten zur Einsparung von Planstellen, Stellen, Beschäftigungspositionen und Personalausgaben zu nutzen.
Dazu können abweichend von § 50 Absatz 1 Satz 1 der Landeshaushaltsordnung auch Mittel oder Planstellen und Stellen umgesetzt werden, ohne dass Aufgaben von einer Verwaltung auf eine andere Verwaltung übergehen.
Das Nähere regelt das Ministerium der Finanzen.

(2) Die Erläuterungen zu den Titeln der Gruppe 422 für Stellen der Beamten auf Probe bis zur Anstellung und zu den Titeln der Gruppe 428 sind hinsichtlich der zulässigen Zahl der für die einzelnen Besoldungs- und Entgeltgruppen ausgebrachten Stellen verbindlich.
Die den Wirtschaftsplänen der Landesbetriebe nach § 26 Absatz 1 der Landeshaushaltsordnung beigefügten Stellenübersichten sind verbindlich.
Das Ministerium der Finanzen kann Ausnahmen von der Verbindlichkeit der Stellenpläne für die Landesbetriebe zulassen.

(3) Abweichend von § 49 der Landeshaushaltsordnung können auf Planstellen auch beamtete Hilfskräfte und Arbeitnehmer geführt werden.

(4) Einnahmen aus Zuschüssen für die berufliche Eingliederung behinderter Menschen und für Arbeitsbeschaffungsmaßnahmen fließen den entsprechenden Ansätzen für Personalausgaben zu.
Innerhalb der einzelnen Kapitel fließen die Einnahmen den Ausgaben bei folgenden Titeln - einschließlich den entsprechenden Titeln - in Titelgruppen zu:

1.  Gruppe 428 aus Erstattungen der Förderleistungen der Bundesagentur für Arbeit in Bezug auf das Altersteilzeitgesetz,
    
2.  Gruppen 422, 428, 441, 443 und 446 aus Schadensersatzleistungen Dritter.
    

(5) Planstellen und Stellen können für Zeiträume, in denen Stelleninhaber vorübergehend nicht oder nicht vollbeschäftigt sind, innerhalb des jeweiligen Einzelplans im Umfang der nicht in Anspruch genommenen Planstellen- oder Stellenanteile für die Beschäftigung von beamteten Hilfskräften und Kräften in zeitlich befristeten Arbeitsverträgen in Anspruch genommen werden.

(6) Das Ministerium der Finanzen wird ermächtigt, Planstellen für Lehrkräfte zur Besetzung mit Beamten, für die die Einstufung nach den Brandenburgischen Besoldungsordnungen nicht gilt, nach Maßgabe des Bundesbesoldungsgesetzes in der am 31. August 2006 geltenden Fassung zu heben.

#### § 13  
Besondere Regelungen für Planstellen und Stellen

(1) Planstellen und Stellen, die einen kw-Vermerk tragen, können nach ihrem Freiwerden mit schwer behinderten Menschen wiederbesetzt werden, wenn die gesetzliche Pflichtquote gemäß § 71 Absatz 1 des Neunten Buches Sozialgesetzbuch bei den Planstellen und Stellen in der Landesverwaltung nicht erreicht wird.
In diesem Fall ist der schwer behinderte Mensch auf der nächsten freiwerdenden Planstelle oder Stelle der betreffenden oder nächsthöheren Besoldungs- oder Entgeltgruppe innerhalb des Einzelplans zu führen.
Das Finanzministerium kann Ausnahmen zulassen.

(2) Das Ministerium der Finanzen wird ermächtigt zuzulassen, dass von einem kw-Vermerk mit Datumsangabe abgewichen wird, wenn die Planstelle oder Stelle weiter benötigt wird, weil sie nicht rechtzeitig frei wird; in diesem Fall ist der Stelleninhaber auf der nächsten freiwerdenden Planstelle oder Stelle der betreffenden oder nächsthöheren Besoldungs- oder Entgeltgruppe innerhalb des Einzelplans zu führen.

(3) Das Ministerium der Finanzen wird ermächtigt, mit Einwilligung des Ausschusses für Haushalt und Finanzen des Landtages Planstellen für Beamte, Richter und Stellen für Arbeitnehmer zusätzlich auszubringen, wenn hierfür ein unabweisbares, auf andere Weise nicht zu befriedigendes Bedürfnis besteht.

(4) Mit Einwilligung des Ministeriums der Finanzen können nach Änderungen im Besoldungs- oder Tarifrecht Planstellen- und Stellenveränderungen vorgenommen werden.
Stellenveränderungen sind mit Einwilligung des Ministeriums der Finanzen auch dann möglich, wenn tarifrechtliche Ansprüche bestehen.

(5) Arbeitnehmer, die vor der Überleitung aus dem BAT/BAT-O und dem MTArb/MTArb-O in den TV-L einen Bewährungs- oder Fallgruppenaufstieg gemäß den §§ 23a, 23b BAT/BAT-O beziehungsweise den vergleichbaren Bestimmungen für Arbeiter vollzogen haben oder bei denen nach den bisherigen tarifrechtlichen Bestimmungen ein Bewährungs- oder Fallgruppenaufstieg in der jeweiligen Fallgruppe vorgesehen war, sowie nach dem 1.
November 2006 neu eingestellte oder neu eingruppierte Arbeitnehmer mit einem höherwertigen Tarifanspruch gemäß Anlage 4 TVÜ-Länder können bis zum Wirksamwerden neuer Eingruppierungsvorschriften für den TV-L oder bis zum Ausscheiden auf einer niedrigwertigeren TV-L-Stelle geführt werden, die der ursprünglichen Stelle in der Struktur des durch den TV-L ersetzten BAT/BAT-O und des MTArb/MTArb-O entspricht.

(6) Das Nähere regelt das Ministerium der Finanzen.

#### § 14  
Ausbringung zusätzlicher Leerstellen

(1) Werden planmäßige Beamte, Richter und Arbeitnehmer im dienstlichen Interesse des Landes mit Zustimmung der obersten Dienstbehörde im Dienst einer öffentlichen zwischenstaatlichen oder überstaatlichen Einrichtung, einer Bundesbehörde oder einer kommunalen Gebietskörperschaft oder für eine Tätigkeit bei einer Fraktion oder einer Gruppe des Landtages, des Deutschen Bundestages oder einer zwischenstaatlichen Einrichtung unter Wegfall der Dienstbezüge länger als ein Jahr verwendet und besteht ein unabweisbares Bedürfnis, die Planstellen und Stellen neu zu besetzen, so kann das Ministerium der Finanzen dafür gleichwertige Leerstellen ausbringen.
Das Gleiche gilt für eine Verwendung bei sonstigen landesunmittelbaren und -mittelbaren juristischen Personen des öffentlichen Rechts sowie bei juristischen Personen des Privatrechts, soweit diese vom Land institutionell gefördert werden oder das Land mehrheitlich beteiligt ist.

(2) Absatz 1 findet entsprechend Anwendung, wenn Beamte nach § 80 Absatz 1 Satz 1 Nummer 2 des Landesbeamtengesetzes länger als ein Jahr beurlaubt werden oder wenn die Rechte und Pflichten aus dem Dienstverhältnis nach § 72 Satz 1 des Landesbeamtengesetzes ruhen.

(3) Für planmäßige Beamte außerhalb der Schulkapitel, die nach § 71 des Landesbeamtengesetzes länger als ein Jahr ohne Unterbrechung Elternzeit nehmen, gilt vom Beginn der Beurlaubung an eine Leerstelle der entsprechenden Besoldungsgruppe als ausgebracht.
Satz 1 gilt auch für die Beurlaubung von Richtern aus familiären Gründen gemäß § 5 des Brandenburgischen Richtergesetzes.

(4) Die Absätze 2 und 3 gelten entsprechend für Richter und Arbeitnehmer.

(5) Für planmäßige Beamte, Richter und Arbeitnehmer, die im Rahmen der Umsetzung der Altersteilzeitregelung am Blockmodell teilnehmen, gilt vom Beginn der Freistellungsphase an eine Leerstelle der entsprechenden Besoldungs- und Entgeltgruppe als ausgebracht.
Zum Zeitpunkt des Übergangs in den Ruhestand fällt diese Leerstelle weg.
Die Ressorts berichten dem Ministerium der Finanzen jährlich zum 31. Dezember über die Anzahl und Wertigkeit der ausgebrachten Leerstellen.

(6) Über den weiteren Verbleib der nach den Absätzen 1 bis 5 ausgebrachten Leerstellen ist im nächsten Haushaltsplan zu entscheiden.

#### § 15  
Vergabe leistungsbezogener Besoldungselemente an Beamte

(1) Für die Vergabe von Leistungsstufen ist die Brandenburgische Leistungsstufenverordnung sowie für die Vergabe von Leistungsprämien und Leistungszulagen die Brandenburgische Leistungsprämien- und -zulagenverordnung anzuwenden.

(2) Innerhalb eines Kapitels dürfen Zulagen für eine befristete Übertragung einer herausgehobenen Funktion nach § 45 des Bundesbesoldungsgesetzes für Beamte bis zur Höhe von 0,1 Prozent der Ausgaben der Titel 422 10 geleistet werden.
In den Einzelplänen 02 bis 12 dürfen die Zulagen nur im Einvernehmen mit dem Ministerium der Finanzen gewährt werden.

(3) Die für die Vergabe leistungsbezogener Besoldungselemente anfallenden Ausgaben sind aus Einsparungen bei anderen Titeln der Hauptgruppe 4 im jeweiligen Einzelplan (ausgenommen Gruppen 432 und 453) oder durch Entnahmen aus der Rücklage Personalbudget zu decken.

#### § 16  
Verbilligte Veräußerung und Nutzungsüberlassung von Grundstücken

(1) Grundstücke des Allgemeinen Grundvermögens dürfen gemäß § 63 Absatz 3 Satz 2 und Absatz 4 der Landeshaushaltsordnung

1.  bei der Nutzungsbindung von mindestens 15 Jahren für Einrichtungen des Sozial-, Kinder- und Jugendwesens in gemeinnütziger Trägerschaft um bis zu 25 Prozent unter dem vollen Wert veräußert werden;
    
2.  bebaut (mit besonderem Sanierungsaufwand) und unbebaut bei einer Belegungsbindung von mindestens 15 Jahren um bis zu 40 Prozent unter dem vollen Wert veräußert werden, wenn sichergestellt ist, dass sie für Maßnahmen der sozialen Wohnraumförderung nach § 2 des Wohnraumförderungsgesetzes verwendet werden;
    
3.  bei einer Belegungsbindung von mindestens 15 Jahren um bis zu 50 Prozent unter dem vollen Wert veräußert werden, wenn sichergestellt ist, dass sie im Rahmen des vom Land geförderten Studentenwohnraumbaus zur Schaffung von Studentenwohnungen oder einer vergleichbaren Förderung verwendet werden.
    Unter den gleichen Voraussetzungen können bebaute und unbebaute Grundstücke an Studentenwerke unentgeltlich abgegeben werden;
    
4.  im Wege der Bestellung eines Erbbaurechts vergeben werden, wobei der Erbbauzins je nach dem zu fördernden Zweck für die Dauer der Nutzungs- und Belegungsbindung abgesenkt werden darf, und zwar
    
    1.  für die gemeinnützigen außeruniversitären Forschungseinrichtungen auf 0 Prozent, wobei der Erbbauzins nach Ablauf von jeweils zehn Jahren um jeweils 1 Prozent erhöht werden kann,
        
    2.  in den Fällen der Nummer 1 auf 2 Prozent,
        
    3.  in den Fällen der Nummer 2 auf 3 Prozent und
        
    4.  in den Fällen von Nummer 3 Satz 2 auf 0 Prozent für die ersten zehn Jahre, 1 Prozent für die folgenden zehn Jahre und so fortlaufend bis zu 3 Prozent nach 30 Jahren ausgehend vom Bodenwert.
        In den Fällen von Nummer 3 Satz 1 auf 3 Prozent vom Bodenwert;
        
5.  vom Land institutionell geförderten außeruniversitären Forschungseinrichtungen gegen Übernahme der Betriebs- und zumutbaren Bauunterhaltungskosten unentgeltlich zur Nutzung überlassen werden.
    

(2) Für die nach dem Gesetz über die Verwertung der Liegenschaften der Westgruppe der Truppen in der Titelgruppe 65 „WGT-Liegenschaftsvermögen im AGV“ im Kapitel 20 630 ausgewiesene Vermögensmasse gilt über die Regelung des Absatzes 1 hinaus, dass bebaute und unbebaute Grundstücke um bis zu 25 Prozent unter dem vollen Wert veräußert oder im Erbbaurecht vergeben werden dürfen, die für unmittelbare Verwaltungszwecke vom Land sowie für kommunale Infrastrukturmaßnahmen von den Kreisen und den Gemeinden dauerhaft genutzt werden können.

(3) Über die Verbilligungen gemäß Absatz 1 hinaus wird gemäß § 63 Absatz 3 Satz 2 und Absatz 4 der Landeshaushaltsordnung zugelassen, dass landeseigene bebaute und unbebaute Grundstücke an Gebietskörperschaften für die im Bundeshaushalt aufgeführten Zwecke bis zu dem Prozentsatz unter dem vollen Wert veräußert, im Wege der Erbbaurechtsbestellung zur Verfügung gestellt, vermietet, verpachtet oder zur Nutzung überlassen werden, zu dem der Bund dem Land Verbilligungen bei der Veräußerung, Zurverfügungstellung im Wege des Erbbaurechts, Vermietung, Verpachtung oder Nutzungsüberlassung von bundeseigenen Grundstücken für gleiche Zwecke einräumt.
Vom Gegenseitigkeitserfordernis nach Satz 1 sind die Liegenschaften, die in der Titelgruppe 65 „WGT-Liegenschaftsver-mögen im AGV“ im Kapitel 20 630 ausgewiesen sind, ausgenommen.

(4) Gemäß § 61 Absatz 1 Satz 1, § 63 Absatz 3 Satz 2 und Absatz 4 der Landeshaushaltsordnung wird die vorübergehende oder dauernde Abgabe von Grundstücken des Allgemeinen Grundvermögens an das Verwaltungsgrundvermögen ohne Werterstattung zugelassen; dies gilt nicht für Grundstücke, die zur in der Titelgruppe 65 „WGT-Liegen-schaftsvermögen im AGV“ im Kapitel 20 630 ausgewiesenen Vermögensmasse gehören.

#### § 17  
Besondere Regelungen für geheim zu haltende Ausgaben

(1) Aus zwingenden Gründen des Geheimschutzes wird die Bewilligung von Ausgaben, die nach einem geheim zu haltenden Wirtschaftsplan bewirtschaftet werden sollen, von der Billigung des Wirtschaftsplans durch die Parlamentarische Kontrollkommission nach § 23 des Brandenburgischen Verfassungsschutzgesetzes abhängig gemacht.
Die Mitglieder dieser Kontrollkommission sind zur Geheimhaltung aller Angelegenheiten verpflichtet, die ihnen bei dieser Tätigkeit bekannt geworden sind.

(2) Der Präsident des Landesrechnungshofes prüft in den Fällen des Absatzes 1 nach § 9 des Landesrechnungshofgesetzes und unterrichtet die Parlamentarische Kontrollkommission sowie die zuständige oberste Landesbehörde und das Ministerium der Finanzen über das Ergebnis ihrer Prüfung der Jahresrechnung sowie der Haushalts- und Wirtschaftsführung.
§ 97 Absatz 4 der Landeshaushaltsordnung bleibt unberührt.

#### § 18  
Berichtspflichten gegenüber dem Ausschuss für Haushalt und Finanzen des Landtages

(1) Das Ministerium der Finanzen berichtet dem Ausschuss für Haushalt und Finanzen des Landtages

1.  mit Stand 30.
    Juni 2010 im Rahmen eines Berichtes über wesentliche Kenngrößen der bereinigten Gesamteinnahmen und -ausgaben des Landes sowie über den aktuellen Mittelabfluss aus dem Landeshaushalt.
    In diesem Bericht sollen auch Angaben zur Entwicklung der Einnahmearten und der Ausgabearten insbesondere zur Umsetzung der EU-Fonds und zum Stand der Verschuldung sowie Prognosedaten der weiteren Entwicklung bis zum Jahresende enthalten sein,
    
2.  über den Jahresabschluss 2010 im Rahmen eines Berichtes wie in Nummer 1 allerdings ohne Prognoseaussage,
    
3.  mit Stand 31.
    Dezember 2010 bis zum 31.
    März 2011 über die Gewährung und Inanspruchnahme von Bürgschaften, Rückbürgschaften, Garantien und sonstigen Gewährleistungen durch das Land gemäß den §§ 3 und 4.
    

(2) Die Ministerien berichten dem Ausschuss für Haushalt und Finanzen des Landtages

1.  zu den in Absatz 1 Nummer 1 und 2 genannten Stichtagen im Rahmen eines Berichtes über den Stand der Bewilligungen bei sämtlichen Titeln der Hauptgruppen 6 und 8 mit einem Ansatz ab 1 000 000 Euro und den aktuellen Mittelabfluss,
    
2.  zu den in Absatz 1 Nummer 1 und 2 genannten Stichtagen im Rahmen eines Berichtes über die Inanspruchnahme von Verpflichtungsermächtigungen,
    
3.  zu den in Absatz 1 Nummer 1 und 2 genannten Stichtagen im Rahmen eines Berichtes über die Inanspruchnahme von Ausgaberesten bei sämtlichen Titeln der Hauptgruppen 6 und 8 mit einem Ansatz ab 1 000 000 Euro,
    
4.  mit Stand 31.
    Mai 2010 im Rahmen eines Berichtes über die Besetzung der Planstellen und Stellen.
    

(3) Die Ministerien berichten dem Ausschuss für Haushalt und Finanzen des Landtages

1.  mit Stand 30.
    Juni 2010 zum 1.
    August 2010 im Rahmen eines Berichtes über den Stand der Entgeltzahlungen an die Investitionsbank des Landes Brandenburg im Zusammenhang mit der Wahrnehmung der Geschäftsbesorgung für die Bewilligung, Gewährung von Zuwendungen und zur Verwendungsnachweisprüfung,
    
2.  mit Stand 31.
    Dezember 2010 zum 1.
    Februar 2011 im Rahmen eines Berichtes wie in Nummer 1.
    

(4) Das Ministerium für Wirtschaft und Europaangelegenheiten berichtet dem Ausschuss für Haushalt und Finanzen des Landtages

1.  zum 30.
    Juni 2010 im Rahmen eines Berichtes über den Stand der Bewilligung von Fördermitteln aus der Gemeinschaftsaufgabe „Verbesserung der regionalen Wirtschaftsstruktur“.
    Der Bericht erfolgt in Form einer Übersicht der bewilligten Einzelförderungen mit einem Förderbetrag von mehr als 1 000 000 Euro.
    In der Übersicht sind die der Bewilligung zugrunde gelegten Kriterien und der Fördersatz anzugeben,
    
2.  zum 30.
    September 2010 im Rahmen eines Berichtes wie in Nummer 1,
    
3.  zum 31.
    Dezember 2010 im Rahmen eines Berichtes wie in Nummer 1.
    

#### § 19  
Weitergeltung von Vorschriften und Ermächtigungen

Die Vorschriften und Ermächtigungen in den §§ 3, 4, 5, 6, 8 Absatz 1 und 2, §§ 11 bis 15 und 17 gelten bis zur Verkündung des Haushaltsgesetzes 2011 weiter.

#### § 20  
Inkrafttreten

Dieses Gesetz tritt mit Wirkung vom 1. Januar 2010 in Kraft.

Potsdam, den 11.
 Mai 2010

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

Anlage 
-------

### Haushaltsplan des Landes Brandenburg für das Haushaltsjahr 2010  
**Gesamtplan**

I.   Haushaltsübersicht       (§ 13 Absatz 4 Nummer 1 LHO)

A.   Zusammenfassung der Einnahmen und Ausgaben je Einzelplan

B.   Zusammenfassung der Verpflichtungsermächtigungen je Einzelplan

II.  Finanzierungsübersicht       (§ 13 Absatz 4 Nummer 2 LHO)

III.  Kreditfinanzierungsplan       (§ 13 Absatz 4 Nummer 3 LHO)

  
 

**Teil I Haushaltsübersicht 2010**

A.
Zusammenfassung der Einnahmen und Ausgaben der Einzelpläne

**Einnahmen**

**Ausgaben**

**0**

**1**

**2**

**3**

**4**

**Einzelplan /  
Ressort**

**Einnahmen aus  
Steuern und  
steuerähnlichen  
Abgaben**

**Verwaltungs-  
einnahmen,  
Einnahmen aus  
Schuldendienst  
und dgl.**

**Einnahmen aus  
Zuweisungen  
und Zuschüssen  
mit Ausnahme  
für  
Investitionen**

**Einnahmen aus  
Schuldenauf-  
nahmen, aus  
Zuweisungen  
und Zuschüssen  
für Investitionen,  
besondere  
Finanzierungs-  
einnahmen**

**Summe  
  
Einnahmen**

**Personal-  
ausgaben**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**1**

**2**

**3**

**4**

**5**

**6**

**7**

**01 / LT**

43.500

275.000

**318.500**

22.004.200

**02 / Stk**

13.600

83.700

145.000

**242.300**

10.721.100

**03 / MI**

46.083.400

4.750.000

**50.833.400**

444.400.000

**04 / MdJ**

110.026.900

24.085.700

5.903.200

**140.015.800**

244.305.800

**05 / MBJS**

2.656.000

22.410.600

9.562.000

**34.628.600**

965.319.200

**06 / MWFK**

7.148.600

91.967.000

47.172.500

**146.288.100**

28.144.200

**07 / MASF**

13.464.800

145.187.300

16.223.500

**174.875.600**

48.764.400

**08 / MWE**

16.889.600

1.749.700

346.404.800

**365.044.100**

24.211.500

**10 / MUGV**

30.332.100

798.100

55.364.500

**86.494.700**

77.306.300

**11 / MIL**

664.600

6.791.600

521.737.100

355.366.600

**884.559.900**

60.829.100

**12 / MdF**

20.093.900

23.031.000

4.672.500

**47.797.400**

173.106.500

**13 / LRH**

  17.500

**17.500**

9.618.900

**14 / LVG**

600

**600**

368.100

**20 / AFV**

4.805.300.000

80.648.800

2.415.796.200

1.278.627.200

**8.580.372.200**

114.493.600

**Summe 2010**

**4.805.964.600**

**334.210.900**

**3.251.596.400**

**2.119.716.800**

**10.511.488.700**

**2.223.592.900**

**Summe 2009**

**5.549.964.600**

**335.278.300**

**3.265.208.300**

**902.515.400**

**10.052.966.600**

**2.081.107.900**

Vgl.
zu 2009

\-744.000.000

\-1.067.400

\-13.611.900

+1.217.201.400

+458.522.100

+142.485.000

  
 

**Teil I Haushaltsübersicht 2010**

A.
Zusammenfassung der Einnahmen und Ausgaben der Einzelpläne

**Ausgaben**

**5**

**6**

**7**

**8**

**9**

**Einzelplan /  
Ressort**

**Sächliche  
Verwaltungs-  
ausgaben und  
Ausgaben für  
den  
Schuldendienst**

**Ausgaben für  
Zuweisungen  
und Zuschüsse  
mit Ausnahme  
für  
Investitionen**

**Baumaßnahmen**

**Sonstige  
Ausgaben für  
Investitionen  
und  
Investitions-  
förderungs-  
maßnahmen**

**Besondere  
Finanzierungs-  
ausgaben**

**Summe  
  
Ausgaben**

**\+ Überschuss  
  
\- Zuschuss**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**\- EUR -**

**1**

**8**

**9**

**10**

**11**

**12**

**13**

**14**

**01 / LT**

2.876.600

6.751.100

 30.000

580.000

53.900

**32.295.800**

\-31.977.300

**02 / Stk**

3.341.900

551.300

63.500

**14.677.800**

\-14.435.500

**03 / MI**

139.711.100

14.141.900

2.832.700

19.986.500

1.507.000

**622.579.200**

\-571.745.800

**04 / MdJ**

156.326.200

31.344.800

3.584.400

1.598.100

**437.159.300**

\-297.143.500

**05 / MBJS**

11.814.800

370.960.300

12.370.100

8.260.200

**1.368.724.600**

\-1.334.096.000

**06 / MWFK**

9.417.900

526.795.800

67.120.200

\-2.068.900

**629.409.200**

\-483.121.100

**07 / MASF**

8.023.000

565.571.500

4.386.000

13.202.900

**639.947.800**

\-465.072.200

**08 / MWE**

14.570.600

89.391.400

1.100.000

383.200.000

66.500

**512.540.000**

\-147.495.900

**10 / MUGV**

37.057.700

90.511.500

20.513.000

161.295.700

\-9.453.200

**377.231.000**

\-290.736.300

**11 / MIL**

31.798.400

791.454.000

35.710.700

496.304.100

\-1.893.500

**1.414.202.800**

\-529.642.900

**12 / MdF**

37.123.800

40.571.700

126.539.700

424.200

**377.765.900**

\-329.968.500

**13 / LRH**

1.319.500

2.000

228.800

17.100

**11.186.300**

\-11.168.800

**14 / LVG**

200.200

**568.300**

\-567.700

**20 / AFV**

788.694.800

2.292.992.300

500.290.000

376.730.000

**4.073.200.700**

+4.507.171.500

**Summe 2010**

**1.242.276.500**

**4.821.039.600**

**60.186.400**

**1.775.949.000**

**388.444.300**

**10.511.488.700**

**0**

**Summe 2009**

**1.380.375.500**

**4.782.719.500**

**17.350.000**

**1.755.031.200**

**36.382.500**

**10.052.966.600**

**0**

Vgl.
zu 2009

\-138.099.000

+38.320.100

+42.836.400

+20.917.800

+352.061.800

+458.522.100

0

  
 

**Teil I Haushaltsübersicht 2010**

B.
Zusammenfassung der Verpflichtungsermächtigungen der Einzelpläne und deren Inanspruchnahme

**Einzel-  
plan**

**Bezeichnung**

**Verpflich-  
tungs-  
ermächti-  
gungen**

**durch die Verpflichtungsermächtigung  
entstehende Rechtsverpflichtungen**

**2010**

**2011**

**2012**

**2013**

**2014 ff.**

**1.000 EUR**

**1**

**2**

**3**

**4**

**5**

**6**

**7**

**01**

Landtag

**02**

Ministerpräsident und Staatskanzlei

1.900,0

1.000,0

900,0

**03**

Ministerium des Innern

166.576,0

155.442,0

7.264,0

587,0

3.283,0

**04**

Ministerium der Justiz

2.400,0

1.200,0

1.200,0

**05**

Ministerium für Bildung, Jugend und Sport

20.185,0

7.105,0

9.450,0

2.690,0

940,0

**06**

Ministerium für Wissenschaft, Forschung und Kultur

23.293,0

15.005,0

4.944,0

3.044,0

300,0

**07**

Ministerium für Arbeit, Soziales, Frauen und Familie

127.332,6

74.732,6

31.820,0

20.780,0

**08**

Ministerium für Wirtschaft und Europaangelegenheiten

526.592,6

197.278,0

191.448,7

137.865,9

**10**

Ministerium für Umwelt, Gesundheit und Verbraucherschutz

108.879,9

56.960,0

23.847,7

12.529,9

15.542,3

**11**

Ministerium für Infrastruktur und Landwirtschaft

1.766.441,9

308.287,7

132.721,1

128.559,1

1.196.874,0

**12**

Ministerium der Finanzen

224.350,0

108.210,0

100.340,0

13.800,0

2.000,0

**13**

Landesrechnungshof

**14**

Verfassungsgericht des Landes Brandenburg

**20**

Allgemeine Finanzverwaltung

85.506,8

71.506,8

12.000,0

2.000,0

**Zusammen**

**3.053.457,8**

**996.727,1**

**515.935,5**

**321.855,9**

**1.218.939,3**

  
 

**Teil II Finanzierungsübersicht 2010**

**Insgesamt  
2010  
(Mio EUR)**

**I.**

**HAUSHALTSVOLUMEN**

  **10.511,5**

  **II.**

**ERMITTLUNG DES FINANZIERUNGSSALDOS**

**1.**

**Ausgaben**

**10.105,0**

(ohne Ausgaben zur Schuldentilgung am Kreditmarkt, Zuführungen an Rücklagen,  
Ausgaben zur Deckung eines kassenmäßigen Fehlbetrags und haushaltstechnische Verrechnungen)

**2.**

**Einnahmen**

**9.343,2**

(ohne Einnahmen aus Krediten vom Kreditmarkt, Entnahmen aus Rücklagen,  
Einnahmen aus kassenmäßigen Überschüssen und haushaltstechnischen Verrechnungen)

**3.**

**Finanzierungssaldo**

**\-761,8**

**III.**

**AUSGLEICH DES FINANZIERUNGSSALDOS**

**4.**

**Nettoneuverschuldung am Kreditmarkt**

**650,9**

4.1

Einnahmen aus Krediten vom Kreditmarkt (brutto)

3.504,4

4.2

Ausgaben zur Schuldentilgung am Kreditmarkt

\-2.853,5

4.21

planmäßige Tilgungen

\-2.853,5

4.22

mögliche vorzeitige Tilgungen

0,0

4.23

Tilgungen kurzfristiger Schulden

0,0

**5.**

**Rücklagenbewegung**

**258,9**

5.1

Entnahmen aus Rücklagen

516,4

5.2

Zuführungen an Rücklagen

\-257,4

**6.**

**Abwicklung der Vorjahre**

**\-148,0**

6.1

Ausgaben zur Deckung kassenmäßiger Fehlbeträge

\-148,0

6.2

  Einnahmen aus kassenmäßigen Überschüssen

  --

**7.**

**Haushaltstechnische Verrechnungen**

**0,0**

7.1

Ausgaben

\-1,1

7.2

Einnahmen

1,1

**zusammen**

**761,8**

Abweichungen in den Summen ergeben sich durch Runden der Zahlen

  
 

**Teil III Kreditfinanzierungsplan 2010**

**Insgesamt  
2010  
(Mio EUR)**

**I.**

**EINNAHMEN AUS KREDITEN**

bei Gebietskörperschaften, Sondervermögen usw.

\--

vom Kreditmarkt

3.504,4

Zusammen

3.504,4

**II.**

**TILGUNGSAUSGABEN FÜR KREDITE**

bei Gebietskörperschaften, Sondervermögen usw.

  --

vom Kreditmarkt

2.853,5

Zusammen

2.853,5

**III.**

**NETTONEUVERSCHULDUNG insgesamt**

bei Gebietskörperschaften, Sondervermögen usw.

\--

vom Kreditmarkt

650,9

Zusammen

650,9

Abweichungen in den Summen ergeben sich durch Runden der Zahlen