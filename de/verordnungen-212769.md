## Verordnung über den Mehrbelastungsausgleich für den Vollzug des Betreuungsgeldgesetzes (Betreuungsgeldgesetz-Mehrbelastungsausgleichsverordnung - BetrGeldGMAV)

Auf Grund des § 12 Absatz 1 Satz 1 des Bundeselterngeld- und Elternzeitgesetzes vom 5.
Dezember 2006 (BGBl.
I S. 2748) in Verbindung mit § 1a Satz 1 der Verordnung über die Zuständigkeiten zur Durchführung des Bundeselterngeld- und Elternzeitgesetzes vom 5.
Januar 2007 (GVBl.
II S. 11), der durch Verordnung vom 11.
September 2013 (GVBl.
II Nr. 71) eingefügt worden ist, verordnet der Minister für Arbeit, Soziales, Frauen und Familie:

§ 1  
Mehrbelastungsausgleich
-----------------------------

(1) Die nach § 1 Absatz 1 der Verordnung über die Zuständigkeiten zur Durchführung des Bundeselterngeld- und Elternzeitgesetzes zuständigen Behörden (Betreuungsgeldstellen) erhalten zum Ausgleich der für den Vollzug des Betreuungsgeldes nach Artikel 1 des Betreuungsgeldgesetzes vom 15.
Februar 2013 (BGBl.
I S. 254) notwendigen Mehrbelastungen eine Verwaltungskostenpauschale für jeden erstmals bewilligten oder abgelehnten Betreuungsgeldantrag.

(2) Die Verwaltungskostenpauschale wird zum Stichtag 31.
Juli 2014 auf einen Betrag in Höhe von 32,85 Euro festgesetzt.

(3) Der Personalkostenanteil der Verwaltungskostenpauschale wird erstmals zum Stichtag 31.
Juli 2015 und sodann regelmäßig entsprechend dem jeweiligen Tarifabschluss für den öffentlichen Dienst der Kommunen im Land Brandenburg angepasst.
Die neu errechnete Verwaltungskostenpauschale ist im Amtsblatt für Brandenburg jeweils bis zum 31.
Juli des laufenden Kalenderjahres bekannt zu machen.

§ 2   
Zuständigkeit, Verfahren
-------------------------------

(1) Zuständige Behörde ist das Landesamt für Soziales und Versorgung.

(2) Die Erstattung der Verwaltungskostenpauschale ist mit zahlenmäßiger Angabe der bis zum Stichtag 31.
Juli erstmals bewilligten oder abgelehnten Betreuungsgeldanträge als Gesamtbetrag jährlich zum 31.
August gegenüber der zuständigen Behörde schriftlich geltend zu machen.
Von der zuständigen Behörde können die Unterlagen der Betreuungsgeldstellen zur Berechnung des Gesamtbetrages eingesehen oder angefordert werden.
Die Auszahlung des Gesamtbetrages soll einen Monat nach Geltendmachung erfolgen.

§ 3   
Inkrafttreten
--------------------

Diese Verordnung tritt mit Wirkung vom 1.
August 2013 in Kraft.

Potsdam, den 7.
Juli 2014

Der Minister für Arbeit, Soziales, Frauen und Familie

Günter Baaske