## Verordnung über die Mitwirkung von Bewohnerinnen und Bewohnern in Einrichtungen und den Einrichtungen gleichgestellten Wohnformen nach dem Brandenburgischen Pflege- und Betreuungswohngesetz  (Einrichtungsmitwirkungsverordnung - EMitwV)

Auf Grund des § 16 Absatz 7 des Brandenburgischen Pflege- und Betreuungswohngesetzes vom 8.
Juli 2009 (GVBl.
 I S. 298) verordnet der Minister für Arbeit, Soziales, Frauen und Familie im Einvernehmen mit dem Minister des Innern:

§ 1  
Wahrnehmung der gemeinschaftlichen Mitwirkung
---------------------------------------------------

(1) Die Bewohnerinnen und Bewohner von Einrichtungen nach § 4 Absatz 1 und 2 des Brandenburgischen Pflege- und Betreuungswohngesetzes wirken unmittelbar und über einen von ihnen gewählten Bewohnerschaftsrat in Fragen des gemeinschaftlichen Zusammenlebens mit.
Der Bewohnerschaftsrat vertritt die Interessen der Bewohnerinnen und Bewohner gegenüber dem Leistungsanbieter und der Leitung.

(2) Ein Bewohnerschaftsrat kann für einen Teil einer Einrichtung gebildet werden, wenn dadurch die gemeinschaftliche Mitwirkung besser gewährleistet wird.
Auf begründeten Antrag des Leistungsanbieters kann die zuständige Behörde die Bildung eines Bewohnerschaftsrates für mehrere Einrichtungen genehmigen.

(3) In Einrichtungen mit bis zu zehn Bewohnerinnen oder Bewohnern kann auf die Wahl eines Bewohnerschaftsrates verzichtet werden, wenn alle Bewohnerinnen und Bewohner die gemeinschaftliche Mitwirkung gemeinsam wahrnehmen.
In diesem Fall haben die Bewohnerinnen und Bewohner dieselben Aufgaben und Rechte wie ein Bewohnerschaftsrat.

(4) Auf die Bildung eines Bewohnerschaftsrates kann verzichtet werden, wenn dies durch Umstände, die vom Leistungsanbieter nicht zu vertreten sind, nicht möglich ist.
In diesem Fall hat der Leistungsanbieter Maßnahmen nach dem allgemein anerkannten Stand sozialpädagogischer Erkenntnisse zur Sicherung der gemeinschaftlichen Mitwirkungsrechte anzuwenden.

§ 2  
Aufgaben des Bewohnerschaftsrates
---------------------------------------

Der Bewohnerschaftsrat hat folgende Aufgaben:

1.  er achtet darauf, dass die Bewohnerinnen und Bewohner in den jeweiligen Wohnbereichen ihre Rechte im unmittelbaren Mitwirkungsbereich wahrnehmen können und unterstützen diese dabei; dazu zählen gemäß § 16 Absatz 2 Satz 1 des Brandenburgischen Pflege- und Betreuungswohngesetzes
    1.  die Alltags- und Freizeitgestaltung,
    2.  die Gestaltung von Gemeinschaftsräumen,
    3.  Fragen der Verpflegung und
    4.  Regelungen zum Zugang zu gemeinschaftlich genutzten Wohn- und Aufenthaltsräumen;er wirkt in diesen Angelegenheiten direkt bei Entscheidungen und Maßnahmen der Einrichtung mit, wenn sie das gemeinschaftliche Leben in der gesamten Einrichtung betreffen,
2.  er wirkt bei Entscheidungen und Maßnahmen der Einrichtung in den Angelegenheiten des erweiterten Mitwirkungsbereichs mit; dieser umfasst gemäß § 16 Absatz 2 Satz 2 des Brandenburgischen Pflege- und Betreuungswohngesetzes
    1.  Aufstellung oder Änderung der Musterverträge für Bewohnerinnen und Bewohner und der Hausordnung,
    2.  Änderung der Entgelte, soweit diese nicht ausschließlich durch Anpassung der Vereinbarungen nach dem Elften Buch Sozialgesetzbuch oder Zwölften Buch Sozialgesetzbuch bedingt ist,
    3.  Maßnahmen zur Sicherung einer angemessenen hauswirtschaftlichen Versorgung,
    4.  umfassende bauliche Veränderungen oder Instandsetzungen der Einrichtung,
    5.  Erweiterung, Einschränkung oder Einstellung des Betriebes und
    6.  Maßnahmen zur Verhütung von Unfällen,
3.  er kann bei der Leitung und dem Leistungsanbieter Maßnahmen des Einrichtungsbetriebes beantragen, die den Bewohnerinnen oder Bewohnern der Einrichtung dienen,
4.  er nimmt Anregungen und Beschwerden von Bewohnerinnen und Bewohnern entgegen und wirkt erforderlichenfalls durch Verhandlungen mit der Leitung oder in besonderen Fällen mit dem Leistungsanbieter auf ihre Erledigung hin,
5.  er hilft neuen Bewohnerinnen und Bewohnern, sich in der Einrichtung einzuleben,
6.  er schlägt Personen vor, die als Ombudspersonen benannt werden sollen,
7.  er bestellt vor Ablauf seiner Amtszeit einen Wahlausschuss und
8.  er soll mindestens einmal im Jahr eine Versammlung mit den Bewohnerinnen und Bewohnern durchführen und dort über seine Tätigkeit berichten.

§ 3  
Aufgaben des Leistungsanbieters und der Leitung
-----------------------------------------------------

(1) Der Leistungsanbieter und die Leitung der Einrichtung haben dafür zu sorgen, dass

1.  Entscheidungen, die den unmittelbaren Mitwirkungsbereich betreffen, gemeinsam mit den Bewohnerinnen und Bewohnern in den jeweiligen Wohnbereichen getroffen werden,
2.  die Bewohnerinnen und Bewohner über ihre gemeinschaftlichen Mitwirkungsrechte informiert sind und zu deren Ausübung befähigt werden,
3.  das Interesse und die Bereitschaft von Bewohnerinnen und Bewohnern an der Mitarbeit im Bewohnerschaftsrat aktiv befördert werden und
4.  den Bewohnerinnen und Bewohnern die für die Einrichtung benannten Ombudspersonen bekannt sind.

(2) Die Mitwirkung soll im gegenseitigen Vertrauen und Verständnis zwischen Bewohnerschaftsrat, Leitung und Leistungsanbieter erfolgen.
Hierbei haben der Leistungsanbieter und die Leitung der Einrichtung insbesondere folgende Aufgaben:

1.  der Bewohnerschaftsrat wird rechtzeitig über alle Dinge, die der Mitwirkung unterliegen, informiert und kann fachlich beraten werden; der Bewohnerschaftsrat muss insbesondere nachvollziehen und im angemessenen Umfang nachprüfen können, wie die gemeinschaftliche Mitwirkung in den einzelnen Wohnbereichen stattfindet,
2.  Anträge, Anregungen oder Beschwerden des Bewohnerschaftsrates sind in angemessener Frist, längstens binnen vier Wochen, zu beantworten; die Antwort ist schriftlich zu begründen, wenn das Anliegen des Bewohnerschaftsrates bei der Entscheidung nicht berücksichtigt wurde,
3.  der Bewohnerschaftsrat wird bei der Organisation der Versammlung mit den Bewohnerinnen und Bewohnern unterstützt.

(3) Der Leistungsanbieter stellt dem Bewohnerschaftsrat für seine Sitzungen unentgeltlich Räume zur Verfügung.
 Unterlagen und Arbeitsmittel müssen sicher aufbewahrt werden können.
Er trägt die angemessenen Kosten für den Bewohnerschaftsrat.
Der Bewohnerschaftsrat bekommt einen festen Platz, um Informationen in der Einrichtung zu veröffentlichen.
Er bekommt auch die Möglichkeit, Mitteilungen an die Bewohnerinnen und Bewohner zu versenden.
Die Sätze 4 und 5 gelten auch für Ombudspersonen, um Informationen und Mitteilungen aus der Gemeinde oder dem Stadtteil den Bewohnerinnen und Bewohnern zugänglich zu machen.

(4) Die Leitung der Einrichtung hat die Vorbereitung und Durchführung der Wahl des Bewohnerschaftsrates in dem erforderlichen Maße personell und sächlich zu unterstützen und die hierfür erforderlichen Auskünfte zu erteilen.
Sie hat die ordnungsgemäße Durchführung der Wahl sicherzustellen, wenn zu dem in § 6 Absatz 1 Satz 3 bestimmten Zeitpunkt kein Wahlausschuss gebildet wurde.
In diesem Fall können auch Mitarbeiterinnen oder Mitarbeiter der Einrichtung dem Wahlausschuss angehören.
Der Leistungsanbieter übernimmt die erforderlichen Kosten der Wahl des Bewohnerschaftsrates.

(5) Die Einrichtungsleitung hat die Wahl eines Bewohnerschaftsrates unverzüglich der zuständigen Behörde mitzuteilen.
Kann kein Bewohnerschaftsrat gewählt werden, hat sie auch das unter Angabe der Gründe der zuständigen Behörde mitzuteilen.

§ 4  
Zusammensetzung des Bewohnerschaftsrates
----------------------------------------------

(1) Die Zahl der Mitglieder des Bewohnerschaftsrates bestimmt sich wie folgt:

1.  drei bei bis zu 50 Bewohnerinnen und Bewohnern,
2.  fünf bei mehr als 50 Bewohnerinnen und Bewohnern und
3.  sieben bei mehr als 150 Bewohnerinnen und Bewohnern.

(2) Mitglied des Bewohnerschaftsrates kann sein, wer in der Einrichtung wohnt, aber auch Angehörige, rechtliche Betreuungspersonen und sonstige Vertrauenspersonen, etwa Mitglieder von örtlichen Seniorenvertretungen oder Behindertenorganisationen.
Die Bewohnerinnen und Bewohner müssen im Bewohnerschaftsrat immer die Mehrheit bilden.
Ist ein Bewohnerschaftsrat für mehrere Einrichtungen zuständig, muss aus jeder Einrichtung mindestens eine Bewohnerin oder ein Bewohner Mitglied sein.
Mitglied kann nicht sein, wer beim Leistungsanbieter der Einrichtung entgeltlich beschäftigt ist, wer für die Finanzierung der Einrichtung unmittelbar zuständig ist, oder wer bei einer Überwachungsbehörde beschäftigt ist, die die Einrichtung kontrolliert.

(3) Die Mitgliedschaft im Bewohnerschaftsrat endet durch

1.  Ablauf der Amtszeit,
2.  Rücktritt vom Amt oder
3.  Wegfall der Voraussetzungen für die Mitgliedschaft nach Absatz 2.

(4) Jeder Bewohnerschaftsrat hat eine Vorsitzende oder einen Vorsitzenden.
Den Vorsitz führt, wer von den Mitgliedern des Bewohnerschaftsrates in der ersten Sitzung mit einfacher Mehrheit gewählt wird.
Wer den Vorsitz führt, soll in der Einrichtung wohnen.
Wird ein Bewohnerschaftsrat für mehrere Einrichtungen gebildet, soll der Vorsitzende in einer vertretenen Einrichtung wohnen.
Die Vorsitzende oder der Vorsitzende hat die Aufgabe, die Interessen des Bewohnerschaftsrates zu vertreten.

(5) Die Mitglieder des Bewohnerschaftsrates führen ihr Amt unentgeltlich und ehrenamtlich aus.
Die Mitglieder dürfen wegen ihrer Tätigkeit im Bewohnerschaftsrat nicht benachteiligt oder begünstigt werden.
Dies gilt auch für Bewohnerinnen und Bewohner, deren Angehörige oder Vertrauenspersonen im Bewohnerschaftsrat oder als Ombudspersonen tätig sind.

§ 5  
Wahl des Bewohnerschaftsrates
-----------------------------------

(1) Der Bewohnerschaftsrat wird in freier, gleicher, geheimer und unmittelbarer Wahl für zwei Jahre, in Einrichtungen für Menschen mit Behinderungen für vier Jahre gewählt.

(2) Die Wahl findet statt, wenn

1.  kein Bewohnerschaftsrat besteht, obwohl dieser erforderlich und dessen Bildung möglich wäre,
2.  die Amtszeit des Bewohnerschaftsrates nach Absatz 1 endet oder
3.  die Zahl der Mitglieder um mehr als die Hälfte der vorgeschriebenen Zahl gesunken ist.

(3) Wählen dürfen alle, die am Wahltag in der Einrichtung wohnen.
Jede Bewohnerin und jeder Bewohner hat so viele Stimmen wie Mitglieder in den Bewohnerschaftsrat zu wählen sind.

§ 6  
Vorbereitung und Durchführung der Wahl
--------------------------------------------

(1) Die Wahl des Bewohnerschaftsrates wird durch einen Wahlausschuss vorbereitet und durchgeführt.
Der Wahlausschuss besteht aus drei Wahlberechtigten.
Sie werden vom Bewohnerschaftsrat spätestens sechs Wochen vor Ablauf seiner Amtszeit bestellt.
Der Wahlausschuss entscheidet mit einfacher Mehrheit.

(2) Der Wahlausschuss bestimmt, wie und wann gewählt wird und informiert hierüber die Bewohnerinnen und Bewohner spätestens vier Wochen vor der Wahl.
Der Wahlausschuss sammelt die Wahlvorschläge der Bewohnerinnen und Bewohner ein.
Die Kandidatinnen und Kandidaten müssen mit ihrer Aufstellung zur Wahl einverstanden sein.
Der Wahlausschuss erstellt eine Liste mit den Kandidatinnen und Kandidaten und gibt diese Liste und den weiteren Gang der Wahl allen Bewohnerinnen und Bewohnern bekannt.

(3) Die Wahl kann schriftlich oder in einer Wahlversammlung stattfinden.
Eine Bewohnerin oder ein Bewohner kann je Kandidatin oder je Kandidat nur eine Stimme abgegeben.
Wird in einer Wahlversammlung gewählt, ist den Bewohnerinnen und Bewohnern, die hieran nicht teilnehmen können, innerhalb einer angemessenen Frist Gelegenheit zur Abgabe ihrer Stimmen zu geben.
Der Wahlausschuss entscheidet, ob die Leitung der Einrichtung an der Wahlversammlung teilnehmen darf.
Sie hat teilzunehmen, wenn es der Wahlausschuss bestimmt.
Der Wahlausschuss überwacht, dass die Wahl ordnungsgemäß durchgeführt wird.

(4) Gewählt ist jeweils, wer die meisten Stimmen erhält.
Bei Stimmengleichheit ist gewählt, wer in der Einrichtung wohnt.
Im Übrigen entscheidet das Los.
Die Kandidatinnen und Kandidaten, die nicht gewählt wurden, kommen auf eine Ersatzliste.
Wenn Mitglieder aus dem Bewohnerschaftsrat ausscheiden oder verhindert sind, rückt von ihnen nach, wer bei der letzten Wahl die meisten Stimmen erhalten hat.
Über Einwände gegen das Wahlergebnis entscheidet die zuständige Behörde.

(5) Ist ein Bewohnerschaftsrat neu gewählt, lädt der Wahlausschuss zur ersten Sitzung des Bewohnerschaftsrates ein.
Dies gilt auch, wenn über Einwände zu dem Wahlergebnis noch nicht entschieden ist.
 Zwischen der Einladung und der ersten Sitzung sollen nicht mehr als 14 Tage liegen.
Der Wahlausschuss informiert mit seiner Einladung zur ersten Sitzung des Bewohnerschaftsrates auch über das Wahlergebnis.

§ 7  
Sitzungen des Bewohnerschaftsrates
----------------------------------------

(1) Die oder der Vorsitzende des Bewohnerschaftsrates lädt zu den Sitzungen ein, legt die Tagesordnung fest und leitet die Sitzungen.
Sitzungen zu einem bestimmten Thema finden auch statt, wenn

1.  ein Viertel der Mitglieder des Bewohnerschaftsrates das wünschen,
2.  ein Viertel der Bewohnerinnen und Bewohner das beantragen oder
3.  die Leitung der Einrichtung das beantragt.

(2) Die Leitung der Einrichtung muss von dem Zeitpunkt der Sitzung rechtzeitig erfahren und teilnehmen, wenn sie eingeladen wurde.

(3) Zur Wahrnehmung seiner Aufgaben kann der Bewohnerschaftsrat auch beschließen, dass zu seiner Sitzung Fachleute zu einem bestimmten Thema oder andere Personen eingeladen werden.
Der Leistungsanbieter übernimmt die Auslagen dieser Personen in angemessenem Umfang.
Sie enthalten keine Vergütung.

§ 8  
Entscheidungen des Bewohnerschaftsrates
---------------------------------------------

(1) Der Bewohnerschaftsrat entscheidet durch Beschlüsse.
Beschlüsse werden mit einfacher Mehrheit der anwesenden Mitglieder gefasst, wenn mindestens die Hälfte seiner Mitglieder an der Sitzung teilnimmt.
 Sollte die Anzahl an Stimmen gleich sein, hat die oder der Vorsitzende eine zweite Stimme.

(2) Beschlüsse des Bewohnerschaftsrates müssen den Bewohnerinnen und Bewohnern der Einrichtung in geeigneter Form bekannt gegeben werden.

§ 9  
Ombudspersonen
--------------------

(1) Die kreisfreie Stadt, die amtsfreie Gemeinde oder das Amt, in deren Gebiet sich die Einrichtung befindet, kann für die Einrichtung eine oder mehrere Ombudspersonen benennen.
Wird hiervon kein Gebrauch gemacht, kann die Benennung durch die zuständige Behörde erfolgen.
Die Benennung erfolgt in der Regel auf bestimmte Zeit.
Sie kann auch auf unbestimmte Zeit erfolgen.
Die Benennung kann jederzeit mit Wirkung für die Zukunft widerrufen werden.
Eine erneute Benennung ist zulässig.
Bei der Benennung sind ehrenamtlich engagierte Personen und Organisationen, insbesondere die Senioren- und Behindertenbeiräte, sowie die Vorschläge des Bewohnerschaftsrates zu berücksichtigen.
Nachvollziehbare Einwände des Leistungsanbieters sollen berücksichtigt werden.

(2) Ombudspersonen fördern die Beteiligung der Bewohnerinnen und Bewohner am gesellschaftlichen Leben in der Gemeinde oder im Stadtteil.
Sie unterstützen auch den Bewohnerschaftsrat bei der Wahrnehmung der Interessen der Bewohnerinnen und Bewohner in diesem Bereich.

(3) Soweit der Bewohnerschaftsrat es beschließt, können die Mitwirkungsrechte im erweiterten Mitwirkungsbereich durch den Bewohnerschaftsrat und die Ombudspersonen gemeinsam wahrgenommen werden.
In diesem Fall gilt § 7 Absatz 3 Satz 2 und 3 entsprechend.

(4) Der Leistungsanbieter ist verpflichtet, die Wahrnehmung der Aufgaben der Ombudspersonen zu ermöglichen.
Der Kontakt zu den Bewohnerinnen und Bewohnern der Einrichtung und der Zutritt zu den Gemeinschaftsräumen darf durch den Leistungsanbieter zu den üblichen Geschäftszeiten nicht beschränkt werden.

§ 10  
Ordnungswidrigkeiten
---------------------------

Ordnungswidrig im Sinne von § 25 Absatz 3 Nummer 1 des Brandenburgischen Pflege- und Betreuungswohngesetzes handelt, wer vorsätzlich oder fahrlässig

1.  entgegen § 3 Absatz 5 Satz 1 oder Satz 2 eine Mitteilung unterlässt,
2.  entgegen § 4 Absatz 5 Satz 2 oder Satz 3 ein Mitglied des Bewohnerschaftsrates oder eine Bewohnerin oder einen Bewohner benachteiligt oder begünstigt oder
3.  entgegen § 5 Absatz 1 die Wahl des Bewohnerschaftsrates behindert oder durch Zufügung oder Androhung von Nachteilen oder Gewährung oder Versprechen von Vorteilen beeinflusst.

§ 11  
Übergangsregelungen
--------------------------

Heimbeiräte und Bewohnerschaftsräte, die vor Inkrafttreten dieser Verordnung gewählt worden sind und deren Amtszeit im Sinne des § 5 Absatz 1 noch nicht beendet ist, müssen nicht neu gewählt werden.

§ 12  
Inkrafttreten
--------------------

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 6.
Februar 2012

Der Minister für Arbeit,  
Soziales, Frauen und Familie

Günter Baaske