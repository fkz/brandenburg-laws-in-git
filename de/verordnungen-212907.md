## Verordnung zur Änderung der typisierenden Bezeichnung der Hochschule für  Film und Fernsehen Potsdam-Babelsberg

Auf Grund des § 2 Absatz 1 Satz 2 und des § 31 Absatz 1 Satz 2 des Brandenburgischen Hochschulgesetzes vom  28.
April 2014 (GVBl.
I Nr. 18) verordnet die Ministerin für Wissenschaft, Forschung und Kultur:

§ 1   
Änderung der typisierenden Bezeichnung
---------------------------------------------

Die Hochschule für Film und Fernsehen Potsdam-Babelsberg ist mit Inkrafttreten dieser Verordnung Universität.

§ 2   
Fortgeltung bestehender Rechtsvorschriften
-------------------------------------------------

Die für Kunsthochschulen und die Hochschule für Film und Fernsehen Potsdam-Babelsberg geltenden Regelungen im Brandenburgischen Hochschulgesetz und in anderen Gesetzen, Verordnungen und sonstigen Rechtsvorschriften finden auf diese Universität weiterhin Anwendung, bis im Brandenburgischen Hochschulgesetz besondere Bestimmungen getroffen werden.

§ 3   
Promotion
----------------

Die Universität hat das Promotionsrecht für den Studiengang Medienwissenschaft.

§ 4   
Inkrafttreten
--------------------

Diese Verordnung tritt mit Wirkung vom 1.
Juli 2014 in Kraft.

Potsdam, den 4.
Juli 2014

Die Ministerin für Wissenschaft, Forschung und Kultur

Prof.
Dr.-Ing.
Dr.
Sabine Kunst