## Verordnung über den Schutzwald „Ziltendorfer Düne“

Auf Grund des § 12 des Waldgesetzes des Landes Brandenburg vom 20.April 2004 (GVBl.
I S.
137) verordnet der Minister für Infrastruktur und Landwirtschaft:

#### § 1  
Erklärung zum Schutzwald

Die in § 2 näher bezeichneten Waldflächen im Landkreis Oder-Spree werden zum Schutzwald erklärt.
Der Schutzwald trägt die Bezeichnung „Ziltendorfer Düne“ und wird in das Register der geschützten Waldgebiete aufgenommen.

#### § 2  
Schutzgegenstand

(1) Der Schutzwald hat eine Größe von rund 4 Hektar.
Er umfasst folgende Flurstücke:

**Gemeinde:**

**Gemarkung:**

**Flur:**

**Flurstück:**

Ziltendorf

Ziltendorf

2

366, 367, 684, 674.

(2) Zur Orientierung ist dieser Verordnung eine Kartenskizze über die Lage des Schutzwaldes als Anlage beigefügt.

(3) Die Grenze des Schutzwaldes ist in der „Topografischen Karte zur Verordnung über den Schutzwald ‚Ziltendorfer Düne‘“, Maßstab 1 : 10 000 und in der „Liegenschaftskarte zur Verordnung über den Schutzwald ‚Ziltendorfer Düne‘“, Maßstab 1 : 2 500 mit ununterbrochener Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Maßgeblich ist die Einzeichnung in der Liegenschaftskarte.
Die Karten sind mit dem Dienstsiegel des Ministeriums für Infrastruktur und Raumordnung, Siegelnummer 26, versehen und vom Siegelverwalter am 27.
Juni 2012 unterschrieben worden.

(4) Die Verordnung mit Karten kann beim Ministerium für Infrastruktur und Landwirtschaft des Landes Brandenburg in Potsdam, oberste Forstbehörde, sowie beim Landesbetrieb Forst Brandenburg in Potsdam, untere Forstbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3  
Schutzzweck

(1) Schutzzweck des Schutzwaldes, der auf einem Dünenzug innerhalb einer weitgehend ebenen Talsandfläche am westlichen Rand des Odertals liegt, ist

1.  die Erhaltung, Entwicklung und Wiederherstellung eines durch historische Nutzungsform entstandenen Wald- und Heidebiotopkomplexes aus Kiefernwäldern trockenwarmer Standorte mit trockenen kalkreichen Sandrasen;
2.  die Erhaltung und Entwicklung des Gebietes als Lebensraum gefährdeter Pflanzenarten der trockenwarmen Wälder, Heiden und Magerrasen mäßig saurer bis basenreicher Standorte.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Ziltendorfer Düne“ mit der Gebietsnummer DE 3753-301 im Sinne des § 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes mit seinem Vorkommen von

1.  „Kiefernwäldern der sarmatischen Steppe“ und „alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur“ als Biotope von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG);
2.  „Trockenen kalkreichen Sandrasen“ als prioritäres Biotop von gemeinschaftlichem Interesse („prioritärer Lebensraumtyp“ im Sinne des Anhangs I der Richtlinie 92/43/EWG).

#### § 4  
Verbote, Maßgaben zur forstwirtschaftlichen Bodennutzung

(1) Vorbehaltlich der nach § 6 zulässigen Handlungen sind im Schutzwald gemäß § 12 Absatz 6 des Waldgesetzes des Landes Brandenburg alle Handlungen verboten, die dem in § 3 genannten Schutzzweck zuwiderlaufen und das Gebiet oder einzelne seiner Bestandteile nachhaltig stören, verändern, beschädigen oder zerstören können.

(2) Es ist insbesondere verboten:

1.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
2.  das Gebiet außerhalb der Waldwege zu betreten;
3.  im Gebiet mit motorisierten Fahrzeugen und Gespannen zu fahren oder diese dort abzustellen;
4.  zu lagern, zu zelten, Wohnwagen aufzustellen, zu rauchen und Feuer zu entzünden;
5.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
6.  die Bodengestalt zu verändern, Böden zu verfestigen und zu versiegeln;
    
7.  Düngemittel einschließlich Wirtschaftsdünger und Sekundärrohstoffdünger zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
8.  Pflanzenschutzmittel jeder Art oder Holzschutzmittel anzuwenden;
9.  wild lebende Pflanzen oder Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten.

(3) Die ordnungsgemäße forstwirtschaftliche Bodennutzung gemäß den in § 4 des Waldgesetzes des Landes Brandenburg genannten Anforderungen und Grundsätzen bleibt auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe zulässig, dass

1.  die Holznutzung ausschließlich durch Einzelstammentnahme mit dem Ziel der Erhaltung und Entwicklung der in § 3 genannten Biotope erfolgt;
2.  bei Holzerntemaßnahmen Schäden am Bestand und Boden vermieden werden sowie kein flächiges Befahren der Bestände erfolgt;
3.  die Waldverjüngung ausschließlich durch Naturverjüngung mit standortheimischen Baumarten der potenziellen natürlichen Waldgesellschaft erfolgt;
4.  keine flächige tiefgreifende in den Mineralboden eingreifende Bodenverwundung erfolgt.

#### § 5  
Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  die in den lichten Kiefernwäldern vorkommenden geschützten Trockenbiotope sollen durch Entnahme nicht lebensraumtypischer sowie bedrängender Gehölze freigestellt und in ihrer Entwicklung gefördert werden;
2.  die Kiefernbestände sollen unter Rücksichtnahme auf die im Unterstand vorkommenden Trockenbiotope regelmäßig durchforstet werden, um langfristig eine lichtdurchlässige Schirmstellung zu erreichen, die für die Entwicklung der Trockenbiotope erforderlich ist;
3.  der weiteren Ausbreitung der Robinie soll durch geeignete Maßnahmen entgegengewirkt werden, um ein Verdrängen der lebensraumtypischen Baum- und Straucharten sowie vorkommender seltener Pflanzen der Magerrasen zu verhindern;
4.  ankommende Naturverjüngung von Baumarten der potenziell natürlichen Vegetation soll erhalten, gepflegt und entwickelt werden.

#### § 6  
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen;
2.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Forstbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Schutzwaldes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Forstbehörden und sonstige von den Forstbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer und Nutzer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren.

#### § 7  
Ausnahmen, Befreiungen

(1) Ausgenommen von den Verboten des § 4 bleiben Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit dienen.
Die untere Forstbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
 Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Aus Gründen des Waldschutzes und zur Nutzung nach Naturereignissen wie Sturm oder Waldbrand kann die untere Forstbehörde Ausnahmen von den Verboten dieser Verordnung zulassen, sofern der Schutzzweck nicht beeinträchtigt wird.

(3) Wenn überwiegende Gründe des Gemeinwohls es erfordern, kann die untere Forstbehörde im Einvernehmen mit der unteren Naturschutzbehörde auf Antrag Befreiungen von den Verboten dieser Verordnung gewähren.

#### § 8  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 37 Absatz 1 Nummer 7 des Waldgesetzes des Landes Brandenburg handelt, wer vorsätzlich oder fahrlässig den Verboten oder den Maßgaben des § 4 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 37 Absatz 3 des Waldgesetzes des Landes Brandenburg mit einer Geldbuße bis zu zwanzigtausend Euro geahndet werden.

#### § 9  
Verhältnis zu anderen rechtlichen Bestimmungen

(1) Die Regelungen naturschutzrechtlicher Schutzgebietsausweisungen im Bereich des in § 2 genannten Gebietes bleiben unberührt.

(2) Die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 31 bis 35 des Brandenburgischen Naturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 39 bis 55 des Bundesnaturschutzgesetzes, §§ 37 bis 43 des Brandenburgischen Naturschutzgesetzes) bleiben unberührt.

#### § 10  
Geltendmachung von Rechtsmängeln

Eine Verletzung der in § 1 der Waldschutzgebietsverfahrensverordnung genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber der obersten Forstbehörde geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 8.
August 2012

Der Minister für Infrastruktur und Landwirtschaft

Jörg Vogelsänger

#### Anlage  
(zu § 2 Absatz 2)

### Kartenskizze zur Verordnung über den Schutzwald „Ziltendorfer Düne“

  

![Die Anlage zu § 2 Absatz 2 bildet eine Kartenskizze zur Verordnung über den Schutzwald „Ziltendorfer Düne“ im Maßstab 1 : 10 000 ab. Es wird die Grenze des Schutzwaldes mit ununterbrochener Linie dargestellt. Der Schutzwald hat eine Gesamtfläche von rund vier Hektar.](/br2/sixcms/media.php/69/Nr.%2068-1.JPG "Die Anlage zu § 2 Absatz 2 bildet eine Kartenskizze zur Verordnung über den Schutzwald „Ziltendorfer Düne“ im Maßstab 1 : 10 000 ab. Es wird die Grenze des Schutzwaldes mit ununterbrochener Linie dargestellt. Der Schutzwald hat eine Gesamtfläche von rund vier Hektar.")