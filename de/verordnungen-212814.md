## Verordnung über die Gewährung von Prämien und Zulagen für besondere Leistungen im Land Brandenburg (Brandenburgische Leistungsprämien- und -zulagenverordnung - BbgLPZV)

Auf Grund des § 43 Absatz 1 des Brandenburgischen Besoldungsgesetzes vom 20.
November 2013 (GVBl.
I Nr.
32 S.
2, Nr.
34) verordnet die Landesregierung:

§ 1  
Geltungsbereich
---------------------

Diese Verordnung regelt die Gewährung von Leistungsprämien und Leistungszulagen an Beamtinnen und Beamte in Besoldungsgruppen der Besoldungsordnungen A und B im Geltungsbereich des Landesbeamtengesetzes.
Sie gilt nicht für Beamtinnen und Beamte in der Probezeit nach § 18 oder § 120 des Landesbeamtengesetzes sowie für Beamtinnen und Beamte auf Zeit.

§ 2  
Allgemeines
-----------------

(1) Eine Leistungsprämie oder Leistungszulage kann gewährt werden, wenn eine Beamtin oder ein Beamter herausragende besondere Leistungen erbringt oder erbracht hat.
Erfüllt eine Gruppe mehrerer Bediensteter insgesamt die Voraussetzungen nach Satz 1, so kann jede Beamtin und jeder Beamte als Gruppenmitglied eine Leistungsprämie oder Leistungszulage erhalten, wenn festgestellt wird, dass sie oder er an der Erstellung des Arbeitsergebnisses der Gruppe wesentlich beteiligt war oder ist.
 Leistungsprämien und Leistungszulagen im Sinne des Satzes 2 dürfen zusammen 250 Prozent des in § 3 Absatz 2 Satz 1 und § 4 Absatz 2 Satz 1 geregelten Umfangs nicht übersteigen; maßgeblich ist die höchste Besoldungsgruppe der an der Leistung wesentlich beteiligten Beamtinnen und Beamten.
Sie gelten zusammen als eine Leistungsprämie oder Leistungszulage im Sinne des § 5 Absatz 1 Satz 1.
Für Teilprämien und Teilzulagen, die sich für die einzelnen Beamtinnen und Beamten ergeben, gelten § 3 Absatz 2 Satz 1 und § 4 Absatz 2 Satz 1 entsprechend.

(2) Die Gewährung einer Leistungsprämie oder Leistungszulage und die Vergabe einer Leistungsstufe nach der Brandenburgischen Leistungsstufenverordnung dürfen nicht mit demselben Sachverhalt begründet werden.
Leistungsprämien und Leistungszulagen können nicht gewährt werden, wenn die Beamtin oder der Beamte für die besondere Leistung eine Zulage nach § 44 des Brandenburgischen Besoldungsgesetzes, eine Vergütung gemäß § 46 oder § 47 des Brandenburgischen Besoldungsgesetzes oder eine andere erfolgsabhängige Leistung erhält.

(3) Leistungsprämien und Leistungszulagen können nur im Rahmen besonderer haushaltsrechtlicher Regelungen gewährt werden.
Durch eine herausragende besondere Leistung entsteht kein Anspruch auf die Gewährung.

(4) Leistungsprämien und Leistungszulagen sind nicht ruhegehaltfähig.
Sie sind auf Überleitungs- und Ausgleichszulagen nicht anzurechnen.

§ 3  
Leistungsprämie
---------------------

(1) Die Gewährung von Leistungsprämien dient insbesondere der Anerkennung herausragender Einzelleistungen; sie soll in engem zeitlichen Zusammenhang mit der besonderen Leistung stehen.

(2) Die Leistungsprämie wird in einem Einmalbetrag bis zur Höhe des Anfangsgrundgehalts der Besoldungsgruppe gewährt, der die Beamtin oder der Beamte zum Zeitpunkt der Entscheidung angehört.
Die Höhe der Leistungsprämie ist nach dem Grad der besonderen Leistung zu bemessen.
Bei Teilzeitbeschäftigten ist das entsprechend § 6 des Brandenburgischen Besoldungsgesetzes geminderte Anfangsgrundgehalt maßgebend.

(3) Mehrere Leistungsprämien dürfen an eine Beamtin oder einen Beamten innerhalb eines Jahres insgesamt nur bis zur Höhe des Anfangsgrundgehalts gemäß Absatz 2 gewährt werden.

§ 4  
Leistungszulage
---------------------

(1) Die Gewährung einer monatlichen Leistungszulage dient der Anerkennung einer über einen längeren Zeitraum von mindestens drei Monaten erbrachten und auch weiterhin zu erwartenden herausragenden besonderen Leistung sowie dem Anreiz, diese Leistung auch weiterhin zu erbringen.

(2) Die Leistungszulage beträgt höchstens 7 Prozent des Anfangsgrundgehalts der Besoldungsgruppe der Beamtin oder des Beamten im Zeitpunkt der Entscheidung.
§ 3 Absatz 2 Satz 2 und 3 gilt entsprechend.
Die Leistungszulage wird von dem auf die Leistungsfeststellung folgenden Monat an monatlich nachträglich zusammen mit den Dienstbezügen gezahlt, längstens jedoch für ein Jahr.
Sie kann bis zu drei Monaten rückwirkend gewährt werden.

(3) Die Neubewilligung einer Leistungszulage ist frühestens ein Jahr nach Ablauf des Gewährungszeitraums zulässig.
Die Jahresfrist gilt auch nach der Gewährung einer Leistungsprämie.

(4) Die Gewährung einer Leistungszulage ist bei erheblichem Leistungsabfall für die Zukunft zu widerrufen; für die Leistungsfeststellung gilt § 6 Absatz 2 sinngemäß.
Die Zahlung der Leistungszulage endet bei Ausscheiden aus der bisherigen Verwendung, durch einen Wechsel der Verwendung oder durch Zeiten ohne Anspruch auf Dienstbezüge.
Bei zusammenhängender, mehr als sechs Wochen andauernder Abwesenheit vom Dienst endet die Zahlung der Leistungszulage rückwirkend zum ersten Tag der Abwesenheit.

§ 5  
Zahl der Empfängerinnen und Empfänger
===========================================

(1) Leistungsprämien und Leistungszulagen dürfen in einem Kalenderjahr an insgesamt höchstens 15 Prozent der Beamtinnen und Beamten in Besoldungsgruppen der Besoldungsordnungen A und B im Bereich eines Dienstherrn gewährt werden.
Die Entscheidungsberechtigten nach § 6 Absatz 1 können Leistungsprämien und Leistungszulagen an jeweils bis zu 15 Prozent der ihnen unterstellten Beamtinnen und Beamten in Besoldungsgruppen der Besoldungsordnungen A und B gewähren; dabei darf die nach Satz 1 insgesamt für den Bereich des Dienstherrn geltende Zahl der Empfängerinnen und Empfänger nicht überschritten werden.
Maßgebend ist die Zahl der vorhandenen Beamtinnen und Beamten am 1.
Januar des Kalenderjahres.
Eine Überschreitung des Prozentsatzes nach den Sätzen 1 und 2 ist in dem Umfang zulässig, in dem von der Möglichkeit der Vergabe von Leistungsstufen nach § 5 Absatz 1 der Brandenburgischen Leistungsstufenverordnung kein Gebrauch gemacht wird.
Bei der Vergabe sollen alle Laufbahngruppen berücksichtigt werden.

(2) Bei Dienstherren mit weniger als sieben Beamtinnen und Beamten in Besoldungsgruppen der Besoldungsordnungen A und B kann abweichend von Absatz 1 in jedem Kalenderjahr einer Beamtin oder einem Beamten eine Leistungsprämie oder Leistungszulage gewährt werden.

§ 6  
Zuständigkeit und Verfahren
---------------------------------

(1) Die Entscheidung über die Gewährung von Leistungsprämien und über die Gewährung und den Widerruf von Leistungszulagen trifft der Dienstvorgesetzte; die übrigen Vorgesetzten sind zu beteiligen.
Bei obersten Landesbehörden können die Befugnisse nach Satz 1 auf andere Stellen übertragen werden.

(2) Die Begründung für die Gewährung einer Leistungsprämie oder -zulage ist aktenkundig zu machen; dabei ist die herausragende besondere Leistung im Einzelnen darzustellen.

(3) Die Entscheidungen nach Absatz 1 Satz 1 sind der Beamtin oder dem Beamten schriftlich mitzuteilen.

§ 7  
Inkrafttreten, Außerkrafttreten
=====================================

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2014 in Kraft.
Gleichzeitig tritt die Brandenburgische Leistungsprämien- und -zulagenverordnung in der Fassung der Bekanntmachung vom 12.
Dezember 2008 (GVBl.
2009 II S.
34) außer Kraft.

Potsdam, den 7.
August 2014

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister der Finanzen

Christian Görke