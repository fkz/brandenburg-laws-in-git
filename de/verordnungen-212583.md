## Verordnung zur Einführung der elektronischen Aktenführung in Ordnungswidrigkeitenverfahren und über den elektronischen Rechtsverkehr in behördlichen Ordnungswidrigkeitenverfahren

Auf Grund des § 110a Absatz 2 Satz 1 und 2 und des § 110b Absatz 1 Satz 2 und 3 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl.
I S. 602), die durch Artikel 7 des Gesetzes vom 22.
März 2005 (BGBl.
I S. 837, 849) eingefügt worden sind, verordnet die Landesregierung:

### § 1  
Zulassung der elektronischen Aktenführung

In behördlichen Verfahren können Akten elektronisch geführt werden.
Den Zeitpunkt, von dem an die Akten bei Staatsanwaltschaften und Gerichten elektronisch geführt werden, bestimmt das für Justiz zuständige Mitglied der Landesregierung durch Rechtsverordnung; es bestimmt auch die hierfür geltenden organisatorisch-technischen Rahmenbedingungen.

### § 2  
Erstellung und Führung elektronischer Akten

(1) Werden Akten ausschließlich elektronisch geführt (elektronische Akte), so sind sämtliche zu den Akten gehörenden Dokumente in die elektronische Form zu überführen.
Interne Verfügungen sind in elektronischer Form zu erstellen.
In Verwahrung zu nehmende oder in anderer Weise sicherzustellende Urschriften oder andere Beweisstücke, die als Beweismittel von Bedeutung sind oder der Einziehung oder dem Verfall unterliegen, sind nach ihrer Digitalisierung zu Beweiszwecken bis zum Abschluss des Verfahrens aufzubewahren.

(2) Posteingänge sind in die elektronische Form zu übernehmen, soweit sie nicht als solche eingereicht wurden.
Bei der Überführung von Schriftstücken in die elektronische Form ist sicherzustellen, dass das elektronische Dokument exakt das übertragene Dokument darstellt.
Hierfür ist ein Dokumentenvermerk zu fertigen, der festhält, mit welchem technischen Verfahren, durch wen und wann das Dokument übertragen wurde.
Die Dokumente sind so zu speichern, dass ein nachträgliches Ändern des Inhaltes nicht mehr möglich ist.

(3) Bei der Führung elektronischer Akten müssen die Grundsätze einer ordnungsgemäßen Datenverarbeitung eingehalten werden.
Dazu zählen insbesondere Vorkehrungen gegen Datenverlust sowie gegen missbräuchliche Veränderung der Akten.
Die elektronische Akte ist revisionssicher zu führen, so dass nachträgliche Änderungen nicht mehr gespeichert werden, sondern nur als weitere Version der elektronischen Akte hinzugefügt werden können.

(4) Alle an der elektronischen Akte vorgenommenen Arbeitsschritte sollen zusätzlich in einer Vorgangshistorie gespeichert werden, in der dokumentiert wird, welche Bearbeiterin oder welcher Bearbeiter zu welchem Zeitpunkt welche Änderung vorgenommen hat.

(5) In Behörden des Landes sind elektronische Akten über geeignete Fachverfahren wie DMS/VBS standardisiert zu führen.

### § 3  
Technische Rahmenbedingungen

(1) Für die Verarbeitung von Dokumenten im Rahmen der elektronischen Aktenführung muss eines der folgenden Formate und Codierungen verwendet werden:

1.  ASC II (American Standard Code for Information Interchange) als reiner Text ohne Formatierungscodes und ohne Sonderzeichen,
2.  Unicode,
3.  Microsoft RTF (Rich Text Format),
4.  Adobe PDF (Portable Document Format),
5.  XML (Extensible Markup Language),
6.  TIFF (Tagged Image File Format),
7.  JPEG,
8.  Microsoft Word und Excel beziehungsweise konvertierbare Open Source Produkte, soweit keine aktiven Komponenten (zum Beispiel Makros) verwendet werden.

Sofern die verfahrensinterne Abarbeitung in einem verfahrensspezifischen Format erfolgt, ist sicherzustellen, dass der Datenimport und Datenexport in die genannten Formate und Codierungen möglich ist.

(2) Die in Absatz 1 genannten Formate und Codierungen gelten analog für die im behördlichen Verfahren eingereichten elektronischen Dokumente.

(3) Elektronische Dokumente sind grundsätzlich im Original zum Vorgang zu nehmen.
Darüber hinaus kann für Zwecke der Akteneinsicht eine konvertierte Version im handelsüblichen Format zur Akte genommen werden.

(4) In Behörden des Landes sind die in der Landesverwaltung geltenden Informationstechnik- und Sicherheitsstandards zu beachten.

(5) Die Behörden des Landes und der Kommunen legen jeweils auf ihrer Internetseite weitere Voraussetzungen für die Einreichung und Verarbeitung von Dokumenten, insbesondere zu verwendbaren Signaturen und geeigneten Versionen der in Absatz 1 genannten Formatstandards fest.

### § 4  
Datenübermittlung

(1) Die Datenübermittlung im behördlichen Verfahren erfolgt durch Datenübertragung über verwaltungseigene Kommunikationsnetze oder über das Internet.
Bei Behörden des Landes ist der Datenaustausch nach dem aktuell vorgegebenen IT-Landesstandard und der IT-Sicherheitsleitlinie in der jeweils geltenden Fassung vorzunehmen.
 Die Übertragung der Daten über das Internet ist mittels Verfahren zu verschlüsseln, die vom Bundesamt für Sicherheit in der Informationstechnik als sicher eingestufte Methoden und Schlüssellängen gelten.
§ 110d Absatz 3 des Gesetzes über Ordnungswidrigkeiten ist zu beachten.

(2) Soweit eine Übermittlung auf elektronischem Wege nicht erfolgen kann oder soll, sind Aktenausdrucke oder die die Dokumente enthaltenden elektronischen Speichermedien zu übermitteln.
Die Einzelheiten sind zwischen übermittelnder und empfangender Behörde abzustimmen.
Sofern eine Einigung nicht erzielt werden kann, ist ein Aktenausdruck zu übermitteln.

(3) Für Zwecke der Akteneinsicht können zusätzlich konvertierte Versionen von elektronischen Dokumenten übermittelt werden.

### § 5  
Aufbewahrung der Akten

(1) Bei Vorliegen der Voraussetzungen des § 110b Absatz 4 des Gesetzes über Ordnungswidrigkeiten können die Urschriften vor Ergehen der das Verfahren abschließenden Entscheidung vernichtet werden, soweit es sich nicht um in Verwahrung zu nehmende oder in anderer Weise sicherzustellende Urschriften handelt, die als Beweismittel von Bedeutung sind oder der Einziehung oder dem Verfall unterliegen.

(2) Mit Bestandskraft der das Verfahren abschließenden Entscheidung sind die elektronischen Akten einschließlich der zum Nachweis ihrer Richtigkeit erforderlichen Hinweise gesondert aufzubewahren und durch technische und organisatorische Maßnahmen bis zu ihrer Aussonderung besonders zu sichern.
Die Aktenspeicherung ist nur in folgenden Formaten zulässig:

1.  PDF/A,
2.  TIFF,
3.  XML.

(3) Nach Ablauf der Aufbewahrungsfrist sind die elektronischen Akten gemeinsam mit den zugehörigen Urschriften dem zuständigen öffentlichen Archiv anzubieten und, sofern sie archivwürdig sind, zu übergeben; ansonsten sind diese zu löschen.

(4) Sofern nach § 4 Absatz 2 elektronische Speichermedien übermittelt werden, sind deren Inhalte unmittelbar nach Übernahme in die elektronischen Akten des Empfängers zu löschen.

### § 6  
Erstellung und Einreichung elektronischer Dokumente

Den Zeitpunkt, von dem an formgebundene und andere elektronische Dokumente bei den Behörden eingereicht werden können, bestimmt das jeweils zuständige Mitglied der Landesregierung für seinen Bereich durch Rechtsverordnung.

### § 7  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 6.
Oktober 2011

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident  
Matthias Platzeck

Der Minister des Innern  
Dr.
Dietmar Woidke