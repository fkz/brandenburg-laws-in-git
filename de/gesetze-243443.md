## Gesetz zu dem Abkommen über die Errichtung und Finanzierung der Akademie für öffentliches Gesundheitswesen in Düsseldorf

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem Beitritt des Landes Brandenburg zu dem Abkommen der Länder Berlin, Bremen, Hamburg, Hessen, Niedersachsen, Nordrhein-Westfalen und Schleswig-Holstein über die Errichtung und Finanzierung der Akademie für öffentliches Gesundheitswesen in Düsseldorf zum 1. Januar 2018 wird zugestimmt.
Das Abkommen wird nachstehend veröffentlicht.

#### § 2 

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 28.
November 2017

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Abkommen](/de/vertraege-243453)

* * *

### Anlagen

1

[Abkommen über die Errichtung und Finanzierung der Akademie für öffentliches Gesundheitswesen in Düsseldorf](/br2/sixcms/media.php/68/GVBl_I_26_2017-Anlage.pdf "Abkommen über die Errichtung und Finanzierung der Akademie für öffentliches Gesundheitswesen in Düsseldorf") 689.7 KB