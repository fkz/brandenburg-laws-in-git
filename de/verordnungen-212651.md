## Verordnung zur Festsetzung des Wasserschutzgebietes für das Wasserwerk Potsdam - Wildpark

Auf Grund des § 51 Absatz 1 Satz 1 Nummer 1 und Satz 2 und Absatz 2 und des § 52 Absatz 1 des Wasserhaushaltsgesetzes vom 31.
Juli 2009 (BGBl.
I S.
2585) in Verbindung mit § 15 Absatz 1 Satz 1 und Absatz 2 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr.
 20) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1  
Allgemeines

(1) Zur Sicherung der öffentlichen Wasserversorgung wird zum Schutz des Grundwassers im Einzugsgebiet des Wasserwerkes Potsdam - Wildpark das in § 2 näher umschriebene Wasserschutzgebiet festgesetzt.
Begünstigte ist die Energie und Wasser Potsdam GmbH.

(2) Das Wasserschutzgebiet gliedert sich in den Fassungsbereich (Zone I), in die engere Schutzzone (Zone II) und in die weitere Schutzzone (Zone III).
Die Zone III unterteilt sich in die Zone III A und die Zone III B.

### § 2  
Räumlicher Geltungsbereich

(1) Lage und Größe des Wasserschutzgebietes und der Schutzzonen ergeben sich aus der Übersichtskarte in der Anlage 2 und den in Absatz 2 genannten Karten.

(2) Die Schutzzonen sind in der topografischen Karte im Maßstab 1 : 10 000 in der Anlage 3 und außerdem in der Liegenschaftskarte im Maßstab 1 : 2 500 in der Anlage 4, die aus 18 Blättern besteht, dargestellt.
Für die Abgrenzung der Schutzzonen ist die Darstellung in der Liegenschaftskarte maßgebend.

(3) Zu Informationszwecken werden zusätzlich auf Papier ausgefertigte Exemplare der in Absatz 2 genannten Karten bei der unteren Wasserbehörde des Landkreises Potsdam-Mittelmark und der kreisfreien Stadt Potsdam sowie bei der Gemeinde Schwielowsee hinterlegt und können dort während der Dienststunden von jedermann kostenlos eingesehen werden.
Diese Karten sind mit dem Dienstsiegel des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (Siegelnummer 20) versehen.
Eine weitere so gesiegelte Ausfertigung der Karten befindet sich im Ministerium für Umwelt, Gesundheit und Verbraucherschutz.

(4) Veränderungen der Topografie sowie von Flurstücksgrenzen oder -bezeichnungen berühren den räumlichen Geltungsbereich der Schutzzonen nicht.

### § 3  
Schutz der Zone III B

In der Zone III B sind verboten:

1.  das Düngen mit Gülle, Jauche, Geflügelkot, Festmist, Siliersaft oder sonstigen Düngemitteln mit im Sinne des § 2 Nummer 10 der Düngeverordnung wesentlichen Nährstoffgehalten an Stickstoff oder Phosphat,
    1.  wenn die Düngung nicht im Sinne des § 3 Absatz 4 der Düngeverordnung in zeit- und bedarfsgerechten Gaben erfolgt,
    2.  wenn keine jährlichen schlagbezogenen Aufzeichnungen über die Zu- und Abfuhr von Stickstoff und Phosphat erstellt werden,
    3.  auf abgeerntetem Ackerland, wenn nicht im gleichen Jahr Folgekulturen einschließlich Zwischenfrüchte angebaut werden,
    4.  auf Dauergrünland und auf Ackerland vom 15.
        Oktober bis 15.
        Februar, ausgenommen das Düngen mit Festmist ohne Geflügelkot,
    5.  auf Brachland oder stillgelegten Flächen oder
    6.  auf wassergesättigten, gefrorenen oder schneebedeckten Böden,
2.  das Lagern oder Ausbringen von Fäkalschlamm oder Klärschlämmen aller Art einschließlich in Biogasanlagen behandelte Klärschlämme,
3.  das Errichten, Erweitern oder Betreiben von befestigten Dunglagerstätten, ausgenommen mit dichtem Jauchebehälter, der über eine Leckageerkennungseinrichtung verfügt,
4.  das Errichten von Erdbecken zur Lagerung von Gülle, Jauche oder Silagesickersäften,
5.  das Errichten, Erweitern oder Betreiben von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle, ausgenommen Anlagen mit Leckageerkennungseinrichtung und Sammeleinrichtung, wenn der Wasserbehörde
    1.  vor Inbetriebnahme,
    2.  bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung sowie
    3.  wiederkehrend alle fünf Jahreein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit der Sammeleinrichtung vorgelegt wird,
6.  unbefestigte Feldrandzwischenlager für organische oder mineralische Dünger, ausgenommen für Kalk und Kaliumdünger,
7.  das Errichten von ortsfesten Anlagen für die Silierung von Pflanzen oder die Lagerung von Silage, ausgenommen
    1.  Anlagen mit Siliersaft-Sammelbehälter, der über eine Leckageerkennungseinrichtung verfügt und
    2.  Anlagen mit Ableitung in Jauche- oder Güllebehälter,wenn der Wasserbehörde vor Inbetriebnahme, bei bestehenden Anlagen innerhalb eines Jahres nach Inkraft-treten dieser Verordnung, sowie wiederkehrend alle fünf Jahre ein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit der Behälter und Leitungen vorgelegt wird,
8.  die Silierung von Pflanzen oder Lagerung von Silage außerhalb ortsfester Anlagen, ausgenommen Ballensilage im Wickelverfahren,
9.  das Errichten oder Erweitern von Stallungen für Tierbestände für mehr als 50 Großvieheinheiten gemäß Anlage 1 Nummer 1,
10.  die Anwendung von Pflanzenschutzmitteln,
    1.  wenn die Pflanzenschutzmittel nicht für Wasserschutzgebiete zugelassen sind,
    2.  wenn keine flächenbezogenen Aufzeichnungen gemäß § 6 Absatz 4 des Pflanzenschutzgesetzes über den Einsatz auf erwerbsgärtnerisch, land- oder forstwirtschaftlich genutzten Flächen geführt werden,
    3.  in einem Abstand von weniger als 10 Metern zu oberirdischen Gewässern,
    4.  zur Bodenentseuchung oder
    5.  auf Dauergrünland und Grünlandbrachen,
11.  die Beregnung landwirtschaftlich oder erwerbsgärtnerisch genutzter Flächen, wenn die Beregnungshöhe 20 Millimeter pro Tag oder 60 Millimeter pro Woche überschreitet,
12.  der Umbruch von Dauergrünland oder von Grünlandbrachen,
13.  das Anlegen von Schwarzbrache im Sinne der Anlage 1 Nummer 3,
14.  Erstaufforstungen mit Nadelbaumarten oder Robinien,
15.  die Umwandlung von Wald in eine andere Nutzungsart, wenn die Waldumwandlungsfläche größer als 1 000 Quadratmeter ist,
16.  Holzerntemaßnahmen, die eine gleichmäßig verteilte Überschirmung von weniger als 40 Prozent des Waldbodens oder Freiflächen größer als 1 000 Quadratmeter erzeugen, ausgenommen Femel- oder Saumschläge,
17.  Aufschlüsse der Erdoberfläche im Sinne des § 49 Absatz 1 des Wasserhaushaltsgesetzes, selbst wenn das Grundwasser nicht aufgedeckt wird, wie zum Beispiel das Errichten oder Erweitern von Kies- oder Sandgruben, Übertagebergbauen oder Torfstichen, wenn die Schutzfunktion der Deckschichten hierdurch wesentlich gemindert wird, ausgenommen das Errichten von Kleingewässern bis 100 Quadratmeter,
18.  das Errichten oder Erweitern von vertikalen Anlagen zur Gewinnung von Erdwärme,
19.  das Errichten von Rohrleitungsanlagen für wassergefährdende Stoffe, ausgenommen Rohrleitungsanlagen im Sinne des § 62 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes,
20.  das Errichten von Anlagen zur behälterlosen Lagerung oder Ablagerung von Stoffen im Untergrund,
21.  das Behandeln, Lagern oder Ablagern von Abfall, tierischen Nebenprodukten oder bergbaulichen Rückständen, ausgenommen
    1.  die vorübergehende Lagerung in dichten Behältern,
    2.  die ordnungsgemäße kurzzeitige Zwischenlagerung von vor Ort angefallenem Abfall zur Abholung durch den Entsorgungspflichtigen und
    3.  die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen,
22.  das Ein- oder Aufbringen von Abfällen, bergbaulichen Rückständen oder Ersatzbaustoffen in oder auf Böden oder deren Einbau in bodennahe technische Bauwerke,
23.  das Errichten oder Erweitern von Anlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden radioaktiver Stoffe im Sinne des Atomgesetzes, ausgenommen für medizinische Anwendungen sowie für Mess-, Prüf- und Regeltechnik,
24.  das Errichten oder Erweitern von Industrieanlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden wassergefährdender Stoffe in großem Umfang, wie zum Beispiel in Raffinerien, Metallhütten oder chemischen Fabriken,
25.  das Errichten von Kraftwerken oder Heizwerken, die der Genehmigungspflicht nach Bundesimmissionsschutzrecht unterliegen, ausgenommen mit Gas, Sonnenenergie oder Windkraft betriebene Anlagen,
26.  das Errichten von Biogasanlagen,
27.  das Errichten oder Erweitern von Abwasserbehandlungsanlagen, ausgenommen
    1.  die Sanierung bestehender Abwasserbehandlungsanlagen zugunsten des Gewässerschutzes und
    2.  Abwasservorbehandlungsanlagen, wie Fett-, Leichtflüssigkeits- oder Amalgamabscheider,
28.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik eingehalten werden,
29.  das Errichten oder Erweitern von Abwassersammelgruben, ausgenommen
    1.  Anlagen mit allgemeiner bauaufsichtlicher Zulassung des Deutschen Institutes für Bautechnik und
    2.  monolithische Sammelgruben aus Beton, die den allgemein anerkannten Regeln der Technik entsprechen,
30.  das Betreiben oder Unterhalten von Abwassersammelgruben, wenn der Wasserbehörde nicht
    1.  vor Inbetriebnahme,
    2.  bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung sowie
    3.  wiederkehrend alle fünf Jahreein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit vorgelegt wird,
31.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten, ausgenommen Anlagen mit dichtem Behälter,
32.  das Einleiten von Abwasser – mit Ausnahme von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 Nummer 4 – in oberirdische Gewässer, sofern die Einleitung nicht schon zum Zeitpunkt des Inkrafttretens dieser Verordnung wasserrechtlich zugelassen war,
33.  das Ausbringen von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes,
34.  das Einleiten oder Versickern von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser,
35.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen das großflächige Versickern von Niederschlagswasser über die belebte Bodenzone,
36.  das Anwenden von Auftaumitteln auf Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen
    1.  auf Bundes-, Landes- und Kreisstraßen,
    2.  auf den Straßen „Am Alten Mörtelwerk“, „Am Neuen Palais“, „Altes Rad“ „Im Bogen“, dem Kuhpforter Damm, der Forststraße, der Maulbeerallee, der Amundsenstraße, der Kaiser-Friedrich-Straße, der Pappelallee, der Kirschallee, der Reiherbergstraße, der Ehrenpfortenbergstraße und der Roßkastanienstraße sowie
    3.  bei Extremwetterlagen wie Eisregen,
37.  das Errichten oder Erweitern von Straßen oder Wegen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik für bautechnische Maßnahmen an Straßen in Wasserschutzgebieten eingehalten werden,
38.  das Errichten von Rangier- oder Güterbahnhöfen,
39.  das Verwenden wassergefährdender, auslaug- oder auswaschbarer Materialien (zum Beispiel Schlacke, Bauschutt, Teer, Imprägniermittel) zum Wege- oder Wasserbau,
40.  das Einrichten von öffentlichen Freibädern oder Zeltplätzen sowie Camping aller Art, ausgenommen<
    1.  Einrichtungen mit ordnungsgemäßer Abfall- und Abwasserentsorgung,
    2.  das Zelten von Fuß-, Rad-, Reit- und Wasserwanderern abseits von Zelt- und Campingplätzen für eine Nacht,
41.  das Einrichten, Erweitern oder Betreiben von Sportanlagen, ausgenommen Anlagen mit ordnungsgemäßer Abfall- und Abwasserentsorgung,
42.  das Errichten von Motorsportanlagen,
43.  das Errichten oder Erweitern von Schießständen oder Schießplätzen für Feuerwaffen, ausgenommen Schießstände in geschlossenen Räumen,
44.  das Errichten von Golfanlagen,
45.  das Errichten oder Erweitern von Flugplätzen im Sinne des § 6 Absatz 1 Satz 1 des Luftverkehrsgesetzes,
46.  das Starten oder Landen motorgetriebener Luftfahrzeuge außerhalb bei Inkrafttreten dieser Verordnung genehmigter Flugbetriebsflächen, mit Ausnahme in Fällen des § 25 Absatz 2 des Luftverkehrsgesetzes,
47.  das Errichten oder Erweitern von militärischen Anlagen, Standort- oder Truppenübungsplätzen,
48.  das Durchführen von militärischen Übungen, ausgenommen das Durchfahren auf klassifizierten Straßen und in militärischen Anlagen,
49.  Bergbau einschließlich Erdöl- oder Erdgasgewinnung,
50.  das Durchführen von Sprengungen, sofern die Gefahr besteht, dass dabei das Grundwasser aufgedeckt wird,
51.  die Ausweisung neuer Industriegebiete.

### § 4  
Schutz der Zone III A

Die Verbote der Zone III B gelten auch in der Zone III A.
In der Zone III A sind außerdem verboten:

1.  das Errichten von Tiefbehältern zur Lagerung von Gülle, Jauche oder Silagesickersäften,
2.  das Errichten oder Erweitern von Stallungen für Tierbestände, ausgenommen für Kleintierhaltung zur Eigenversorgung,
3.  die Freilandtierhaltung im Sinne der Anlage 1 Nummer´2, wenn die Ernährung der Tiere nicht im Wesentlichen aus der genutzten Weidefläche erfolgt oder wenn die Grasnarbe flächig verletzt wird, ausgenommen Kleintierhaltung für die Eigenversorgung,
4.  das Errichten oder Erweitern von Gartenbaubetrieben oder Kleingartenanlagen, ausgenommen Gartenbaubetriebe, die in geschlossenen Systemen produzieren,
5.  die Neuanlage oder Erweiterung von Baumschulen, forstlichen Pflanzgärten, Weihnachtsbaumkulturen sowie von gewerblichem Wein-, Hopfen-, Gemüse-, Obst- oder Zierpflanzenanbau, ausgenommen Gemüse- sowie Zierpflanzenanbau unter Glas in geschlossenen Systemen und Containerproduktion von Baumschulprodukten auf versiegelten Flächen,
6.  das Einrichten oder Erweitern von dauerhaften Holzlagerplätzen über 100 Raummeter,
7.  Erdaufschlüsse im Sinne des § 49 Absatz 1 des Wasserhaushaltsgesetzes, selbst wenn Grundwasser nicht aufgedeckt wird, ausgenommen das Verlegen von Ver- und Entsorgungsleitungen und die Herstellung von Baugruben und Bohrungen,
8.  das Errichten, Erweitern oder Erneuern von Tiefenbohrungen über 100 Meter, Grundwassermessstellen oder Brunnen, ausgenommen das Erneuern von Brunnen für Entnahmen mit wasserrechtlicher Erlaubnis oder Bewilligung,
9.  das Errichten oder Erweitern von Anlagen zum Umgang mit wassergefährdenden Stoffen, ausgenommen doppelwandige Anlagen mit Leckanzeigegerät und ausgenommen Anlagen, die mit einem Auffangraum ausgerüstet sind, der das maximal in der Anlage vorhandene Volumen wassergefährdender Stoffe aufnehmen kann, und soweit
    1.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 1 das für die Anlage maßgebende Volumen von 1 000 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 1 die für die Anlage maßgebende Masse von 1 000 Tonnen,
    2.  in oberirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 2 das für die Anlage maßgebende Volumen von 100 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 2 die für die Anlage maßgebende Masse von 100 Tonnen,
    3.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 2 das für die Anlage maßgebende Volumen von 10 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 2 die für die Anlage maßgebende Masse von 10 Tonnen,
    4.  in oberirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 3 das für die Anlage maßgebende Volumen von 10 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 3 die für die Anlage maßgebende Masse von 10 Tonnen,
    5.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 3 das für die Anlage maßgebende Volumen von 1 Kubikmeter beziehungsweise bei festen oder gasförmigen Stoffen der Wasser-gefährdungsklasse 3 die für die Anlage maßgebende Masse von 1 Tonnenicht überschritten wird,
10.  das Errichten oder Erweitern von Regen- oder Mischwasserentlastungsbauwerken,
11.  das Errichten oder Erweitern von Bahnhöfen oder Schienenwegen der Eisenbahn, ausgenommen Baumaßnahmen an vorhandenen Anlagen zur Anpassung an den Stand der Technik und zum Erhalt oder zur Verbesserung der Verkehrssicherheit und der Verkehrsabwicklung,
12.  das Abhalten oder Durchführen von Märkten, Volksfesten oder Großveranstaltungen außerhalb der dafür vorgesehenen Anlagen,
13.  das Durchführen von Motorsportveranstaltungen, ausgenommen das Durchfahren auf klassifizierten Straßen,
14.  Bestattungen, ausgenommen innerhalb bereits bei Inkrafttreten dieser Verordnung bestehender Friedhöfe,
15.  die Darstellung von neuen Bauflächen oder Baugebieten im Rahmen der vorbereitenden Bauleitplanung, wenn darin eine Neubebauung bisher unbebauter Gebiete vorgesehen wird,
16.  die Festsetzung von neuen Baugebieten im Rahmen der verbindlichen Bauleitplanung, wenn dies zu einer Erhöhung der zulässigen Grundfläche im Sinne des § 19 Absatz 2 der Baunutzungsverordnung führt, ausgenommen
    1.  Gebiete, die im zum Zeitpunkt des Inkrafttretens dieser Verordnung gültigen Flächennutzungsplan als Bauflächen oder Baugebiete dargestellt sind und
    2.  die Überplanung von Bestandsgebieten, wenn dies zu keiner wesentlichen Erhöhung der zulässigen Grundfläche im Sinne des § 19 Absatz 2 der Baunutzungsverordnung führt.

### § 5  
Schutz der Zone II

Die Verbote der Zonen III B und III A gelten auch in der Zone II.
In der Zone II sind außerdem verboten:

1.  das Düngen mit Gülle, Jauche oder Festmist oder sonstigen organischen Düngern sowie die Anwendung von Siliersaft,
2.  das Errichten von Dunglagerstätten,
3.  das Errichten von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle,
4.  die Silierung von Pflanzen oder Lagerung von Silage,
5.  die Freilandtierhaltung im Sinne der Anlage 1 Nummer 2, ausgenommen Kleintierhaltung für die Eigenversorgung, sofern diese bereits bei Inkrafttreten dieser Verordnung ausgeübt wurde,
6.  die Beweidung,
7.  die Anwendung von Pflanzenschutzmitteln,
8.  das Errichten von Dränungen oder Entwässerungsgräben,
9.  der Einsatz von forstwirtschaftlichen Kraftfahrzeugen abseits von Straßen, Wegen oder forstwirtschaftlichen Rückegassen,
10.  das Vergraben, Lagern oder Ablagern von Tierkörpern oder Teilen davon,
11.  das Errichten oder Betreiben von Wildfütterungen, Kirrungen oder Luderplätzen,
12.  das Errichten von Anlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden wassergefährdender Stoffe,
13.  der Einsatz von mineralischen Schmierstoffen zur Verlustschmierung oder mineralischen Schalölen,
14.  das Lagern, Abfüllen oder Umschlagen wassergefährdender Stoffe, ausgenommen haushaltsübliche Kleinstmengen,
15.  das Befahren mit Fahrzeugen mit wassergefährdender Ladung, ausgenommen das Befahren mit Schienenfahrzeugen der Eisenbahn,
16.  das Errichten oder Erweitern von Transformatoren oder Stromleitungen mit flüssigen wassergefährdenden Kühl- oder Isoliermitteln,
17.  die Lagerung von Abfall oder bergbaulichen Rückständen in dichten Behältern,
18.  der Umgang mit radioaktiven Stoffen im Sinne des Atomgesetzes,
19.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, ausgenommen Anlagen, die zur Entsorgung vorhandener Bebauung dienen und wenn hierbei die allgemein anerkannten Regeln der Technik eingehalten werden,
20.  das Errichten von Abwassersammelgruben,
21.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten,
22.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen das großflächige Versickern von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 Nummer 4 über die belebte Bodenzone,
23.  das Errichten oder Erweitern von Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen
    1.  Baumaßnahmen an vorhandenen Straßen zur Anpassung an den Stand der Technik und Verbesserung der Verkehrssicherheit unter Einhaltung der allgemein anerkannten Regeln der Technik für bautechnische Maßnahmen an Straßen in Wasserschutzgebieten sowie
    2.  Wege mit großflächiger Versickerung der Niederschlagswasserabflüsse über die belebte Bodenzone,
24.  das Errichten von öffentlichen Freibädern oder Zeltplätzen sowie Camping aller Art,
25.  das Errichten von Sportanlagen,
26.  das Abhalten oder Durchführen von Sportveranstaltungen, Märkten, Volksfesten oder Großveranstaltungen,
27.  das Errichten oder Erweitern von Baustelleneinrichtungen oder Baustofflagern,
28.  das Durchführen von Bohrungen, ausgenommen Maßnahmen zur Abwehr von Gefahren für das Grundwasser unter Beachtung der Sicherheitsvorkehrungen zum Grundwasserschutz,
29.  das Durchführen von unterirdischen Sprengungen,
30.  das Errichten oder Erweitern von baulichen Anlagen, ausgenommen Veränderungen in Gebäuden und Instandhaltungsmaßnahmen.

### § 6  
Schutz der Zone I

Die Verbote der Zonen III B, III A und II gelten auch in der Zone I.
In der Zone I sind außerdem verboten:

1.  das Betreten oder Befahren,
2.  land-, forst- oder gartenbauliche Nutzung,
3.  Veränderungen oder Aufschlüsse der Erdoberfläche.

### § 7  
Maßnahmen zur Wassergewinnung

Die Verbote des § 3 Nummer 34, des § 4 Nummer 8, des § 5 Nummer 14, 18, 27 bis 30 sowie des § 6 Nummer 1 und 3 gelten nicht für Maßnahmen zur Wassergewinnung, die durch diese Verordnung geschützt ist.

### § 8  
Widerruf von Befreiungen

(1) Befreiungen nach § 52 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes sind widerruflich; sie können mit Bedingungen und Auflagen verbunden werden und bedürfen der Schriftform.
Abweichend von Satz 1 ist eine Befreiung von dem Verbot gemäß § 3 Nummer 51 und von den Verboten gemäß § 4 Nummer 15 und 16 nicht widerruflich.

(2) Im Fall des Widerrufs einer Befreiung kann die untere Wasserbehörde vom Grundstückseigentümer verlangen, dass der frühere Zustand wiederhergestellt wird, sofern es das Wohl der Allgemeinheit, insbesondere der Schutz der Wasserversorgung, erfordert.

### § 9  
Sicherung und Kennzeichnung des Wasserschutzgebietes

(1) Die Zone I ist von der Begünstigten auf Anordnung der unteren Wasserbehörde gegen unbefugtes Betreten, zum Beispiel durch eine Umzäunung, zu sichern.

(2) Das Wasserschutzgebiet ist von der Begünstigten auf Anordnung der unteren Wasserbehörde durch eine entsprechende Beschilderung ausreichend zu kennzeichnen.

### § 10  
Duldungspflichten

(1) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet haben die Überwachung des Wasserschutzgebietes, insbesondere hinsichtlich der Beachtung dieser Verordnung und der nach ihr getroffenen Anordnungen, sowie das Beobachten der Gewässer und des Bodens durch die zuständigen Wasserbehörden, die Begünstigte oder deren Beauftragte zu dulden.

(2) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet sind auf Anordnung der unteren Wasserbehörde verpflichtet,

1.  das Errichten und Unterhalten von Einrichtungen zur Sicherung der Zone I gegen unbefugtes Betreten,
2.  das Aufstellen, Unterhalten und Beseitigen von Hinweis-, Warn-, Gebots- und Verbotszeichen,
3.  das Betreten der Grundstücke durch Bedienstete der zuständigen Behörden, die Begünstigte oder deren Beauftragte zum Beobachten, Messen und Untersuchen des Grundwassers und zur Entnahme von Boden- und Vegetationsproben sowie
4.  das Anlegen und Betreiben von Grundwassermessstellen

zu dulden.
Die Anordnung erfolgt durch schriftlichen Bescheid gegenüber den betroffenen Eigentümern oder Nutzungsberechtigten.
Soweit bergrechtliche Belange berührt sind, ergeht die Entscheidung im Benehmen mit der zuständigen Bergbehörde.

### § 11  
Übergangsregelung

Für bei Inkrafttreten dieser Verordnung errichtete und betriebene Anlagen gilt das Verbot des Betreibens gemäß § 3 Nummer 3 und 5 nach einem Jahr nach Inkrafttreten dieser Verordnung.

### § 12  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig wird das mit Beschluss Nummer 35/4/75 vom 29.
Januar 1975 der Stadtverordnetenversammlung Potsdam festgesetzte und durch die Beschlüsse der Stadtverordnetenversammlung Potsdam vom 21.
Dezember 1981 und vom 4.
Juli 1990 veränderte Trinkwasserschutzgebiet Potsdam-Wildpark sowie dessen Teil im ehemaligen Kreis Potsdam-Land, der durch Ergänzung vom 16.
Juni 1982 des Beschlusses Nummer 84-13/81 vom 6.
Mai 1981 als Trinkwasserschutzgebiet „WW Wildpark-West – Schutzzonenerweiterung“ festgesetzt wurde, aufgehoben.

Potsdam, den 2.
Mai 2012

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

### Anlagen

1

[Potsdam-Wildp-Anlg-1](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__1.pdf "Potsdam-Wildp-Anlg-1") 134.0 KB

2

[Potsdam-Wildp-Anlg-2](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__2.pdf "Potsdam-Wildp-Anlg-2") 2.5 MB

3

[Potsdam-Wildp-Anlg-3](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__3.pdf "Potsdam-Wildp-Anlg-3") 5.3 MB

4

[Potsdam-Wildp-Anlg-4-Blatt-01](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-01.pdf "Potsdam-Wildp-Anlg-4-Blatt-01") 1.7 MB

5

[Potsdam-Wildp-Anlg-4-Blatt-02](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-02.pdf "Potsdam-Wildp-Anlg-4-Blatt-02") 2.1 MB

6

[Potsdam-Wildp-Anlg-4-Blatt-03](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-03.pdf "Potsdam-Wildp-Anlg-4-Blatt-03") 1.6 MB

7

[Potsdam-Wildp-Anlg-4-Blatt-04](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-04.pdf "Potsdam-Wildp-Anlg-4-Blatt-04") 2.8 MB

8

[Potsdam-Wildp-Anlg-4-Blatt-05](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-05.pdf "Potsdam-Wildp-Anlg-4-Blatt-05") 2.1 MB

9

[Potsdam-Wildp-Anlg-4-Blatt-06](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-06.pdf "Potsdam-Wildp-Anlg-4-Blatt-06") 2.3 MB

10

[Potsdam-Wildp-Anlg-4-Blatt-07](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-07.pdf "Potsdam-Wildp-Anlg-4-Blatt-07") 1.8 MB

11

[Potsdam-Wildp-Anlg-4-Blatt-08](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-08.pdf "Potsdam-Wildp-Anlg-4-Blatt-08") 1.7 MB

12

[Potsdam-Wildp-Anlg-4-Blatt-09](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-09.pdf "Potsdam-Wildp-Anlg-4-Blatt-09") 1.9 MB

13

[Potsdam-Wildp-Anlg-4-Blatt-10](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-10.pdf "Potsdam-Wildp-Anlg-4-Blatt-10") 1.1 MB

14

[Potsdam-Wildp-Anlg-4-Blatt-11](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-11.pdf "Potsdam-Wildp-Anlg-4-Blatt-11") 1.8 MB

15

[Potsdam-Wildp-Anlg-4-Blatt-12](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-12.pdf "Potsdam-Wildp-Anlg-4-Blatt-12") 2.7 MB

16

[Potsdam-Wildp-Anlg-4-Blatt-13](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-13.pdf "Potsdam-Wildp-Anlg-4-Blatt-13") 1.5 MB

17

[Potsdam-Wildp-Anlg-4-Blatt-14](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-14.pdf "Potsdam-Wildp-Anlg-4-Blatt-14") 1.1 MB

18

[Potsdam-Wildp-Anlg-4-Blatt-15](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-15.pdf "Potsdam-Wildp-Anlg-4-Blatt-15") 1.8 MB

19

[Potsdam-Wildp-Anlg-4-Blatt-16](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-16.pdf "Potsdam-Wildp-Anlg-4-Blatt-16") 2.4 MB

20

[Potsdam-Wildp-Anlg-4-Blatt-17](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-17.pdf "Potsdam-Wildp-Anlg-4-Blatt-17") 2.1 MB

21

[Potsdam-Wildp-Anlg-4-Blatt-18](/br2/sixcms/media.php/68/WSGPotsdam_Wildp__an__anlg__4-Blatt-18.pdf "Potsdam-Wildp-Anlg-4-Blatt-18") 1.3 MB