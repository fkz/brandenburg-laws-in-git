## Verordnung zur Übertragung von Ermächtigungen zum Erlass von Rechtsverordnungen auf dem Gebiet des Naturschutzrechts  (Naturschutzermächtigungsübertragungsverordnung -  NatSchEÜV )

Auf Grund des § 9 Absatz 4 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
 I S. 186) und des § 36 Absatz 2 Satz 2 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl.
I S. 602) verordnet die Landesregierung:

§ 1 
----

Die Ermächtigungen der Landesregierung zum Erlass von Rechtsverordnungen nach § 9 Absatz 2 des Landesorganisationsgesetzes in Verbindung mit § 70 Nummer 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542), das zuletzt durch Artikel 7 des Gesetzes vom 21.
Januar 2013 (BGBl.
I S. 95) geändert worden ist, sowie nach § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten für Ordnungswidrigkeiten nach dem Brandenburgischen Naturschutzausführungsgesetz vom 21.
Januar 2013 (GVBl.
I Nr. 3) werden auf das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung übertragen.

§ 2 
----

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 21.
Mai 2013

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Matthias Platzeck