## Gesetz zur Errichtung des Polizeipräsidiums

#### § 1  
Errichtung

Durch Zusammenführung der Polizeipräsidien Frankfurt (Oder) und Potsdam, des Landeskriminalamtes und der Landeseinsatzeinheit der Polizei wird ein Polizeipräsidium als Landesoberbehörde nach § 10 des Landesorganisationsgesetzes errichtet.

#### § 2  
Aufgabenüberleitung

Die Aufgaben und Befugnisse der Polizeipräsidien Frankfurt (Oder) und Potsdam, des Landeskriminalamtes und der Landeseinsatzeinheit der Polizei gehen auf das Polizeipräsidium über.

#### § 3   
Personalüberleitung

Die Beamtinnen und Beamten sowie die Beschäftigten der Polizeipräsidien Frankfurt (Oder) und Potsdam, des Landeskriminalamtes und der Landeseinsatzeinheit der Polizei werden dem Polizeipräsidium zugeordnet.