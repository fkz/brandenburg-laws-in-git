## Verordnung über das Naturschutzgebiet „Neudorfer Wald“

Auf Grund der §§ 22, 23 und 32 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2 und § 21 Absatz 1 Satz 2 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
 Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

#### § 1  
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Prignitz wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Neudorfer Wald“.

#### § 2  
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 50 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Gemeinde:**  

**Gemarkung:**  

**Flur:**

Groß Pankow  

Helle  

4.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.
Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 2 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 und 2 mit dem Titel „Topografische Karte zur Verordnung über das Naturschutzgebiet ‚Neudorfer Wald‘“ und der Liegenschaftskarte im Maßstab 1 : 2 500 mit dem Titel „Liegenschaftskarte zur Verordnung über das Naturschutzgebiet ‚Neudorfer Wald‘“ mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die topografischen Karten ermöglichen die Verortung im Gelände.
 Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Liegenschaftskarte.
 Die Karten sind unterzeichnet von dem Siegelverwahrer, Siegelnummer 22, des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 11.
August 2010.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Prignitz, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3  
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere natur-naher Wälder mit an die potenziell natürliche Vegetation angepassten Beständen wie Eichen-Hainbuchenwälder, Hainsimsen-Buchenwälder, Eichenmischwälder bodensaurer Standorte und Waldmeister-Buchenwälder sowie reich strukturierter Waldränder;
    
2.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Fledermäuse und Vögel, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten;
    
3.  die Erhaltung und Entwicklung der besonderen Eigenart des Gebietes mit einem Mosaik aus reich strukturierten Waldflächen mit hohem Anteil an Altholz, alten Einzelbäumen, stehendem und liegendem Totholz sowie an Höhlenbäumen;
    
4.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des regionalen Biotopverbundes zwischen den Niederungen der Kümmernitz und der Dömnitz.
    

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teiles des Europäischen Vogelschutzgebietes „Agrarlandschaft Prignitz-Stepenitz“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) in seiner Funktion als Lebensraum von Arten nach Anhang I der Richtlinie 2009/147/EG, insbesondere Mittelspecht (Dendrocopus medius), Schwarzspecht (Dryocopus martius), Schwarzstorch (Ciconia nigra) und Wespenbussard (Pernis apivorus) einschließlich ihrer Brut- und Nahrungsbiotope.

#### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
    
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
    
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
    
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
    
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
    
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
    
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
    
8.  die Ruhe der Natur durch Lärm zu stören;
    
9.  das Gebiet außerhalb der dem öffentlichen Verkehr gewidmeten Wege zu betreten; ausgenommen ist das Betreten von Wegen zum Zweck der Erholung und des Sammelns von Pilzen und Waldfrüchten gemäß § 5 Absatz 1 Nummer 4 in der Zeit jeweils nach dem 31.
    August eines jeden Jahres bis zum 28.
    Februar des Folgejahres;
    
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 51 des Brandenburgischen Naturschutzgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
    
11.  mit Fahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
    
12.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
    
13.  Hunde frei laufen zu lassen;
    
14.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
    
15.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (zum Beispiel Abwasser, Klärschlamm und Bioabfälle) zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
    
16.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
    
17.  Tiere zu füttern oder Futter bereitzustellen;
    
18.  Tiere auszusetzen oder Pflanzen anzusiedeln;
    
19.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
    
20.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
    
21.  Pflanzenschutzmittel jeder Art anzuwenden.
    

#### § 5  
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  forstwirtschaftliche Maßnahmen in der Zeit vom 1.
        März bis zum 31.
        August eines jeden Jahres unzulässig sind,
        
    2.  nur heimische und standortgerechte Baumarten unter Ausschluss eingebürgerter Arten eingebracht werden, wobei die Entwicklung von Reinbeständen unzulässig ist und die in § 3 Absatz 1 Nummer 1 aufgeführten Waldgesellschaften zu erhalten sind,
        
    3.  mindestens fünf Stämme Altholz je Hektar mit einem Mindestdurchmesser von 40 Zentimetern in  
        1,30 Meter Höhe über dem Stammfuß aus der Nutzung zu nehmen und dauerhaft zu markieren sind.
        In Jungbeständen ist ein solcher Altholzanteil zu entwickeln,
        
    4.  eine naturnahe Waldentwicklung mit einem Totholzanteil von mindestens 10 Prozent des aktuellen Bestandesvorrates zu gewährleisten ist, wobei mindestens fünf Stück stehendes Totholz je Hektar mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß und einer Mindesthöhe von  
        5 Metern, sofern vorhanden, zu erhalten sind,
        
    5.  eine Nutzung der in § 3 Absatz 1 Nummer 1 genannten Waldgesellschaften nur einzelstamm- bis truppweise erfolgt; auf den übrigen Waldflächen sind Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von 0,5 Hektar zulässig,
        
    6.  Horst- und Höhlenbäume nicht entfernt werden,
        
    7.  § 4 Absatz 2 Nummer 21 gilt;
        
2.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass diese im Gebietsteil südwestlich der Bahntrasse in der Zeit vom 1.
        März bis zum 31.
        August eines jeden Jahres unzulässig ist.
        Freigestellt ist in diesem Zeitraum die Bejagung der außerhalb des Naturschutzbietes liegenden Flächen von einem 5 Meter breiten Randstreifen innerhalb des Naturschutzgebietes aus,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Errichtung anzuzeigen.
        Die Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        
    
    Im Übrigen bleiben Wildfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern im gesamten Gebiet und die Anlage von Kirrungen in den geschützten Biotopen unzulässig;
    
3.  erforderliche Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne einer Fischbestandskontrolle, -regulierung und -förderung nach vorheriger Anzeige bei der unteren Naturschutzbehörde.
    Die untere Naturschutzbehörde kann die Maßnahmen verbieten, oder mit Nebenbestimmungen versehen, wenn sie dem Schutzzweck entgegenstehen;
    
4.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch nach dem 31.
    August eines jeden Jahres bis zum 28.
    Februar des Folgejahres;
    
5.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
    
6.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
    
7.  der Betrieb von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
     Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
    
8.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
    
9.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz  
    sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
    
10.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der unteren Naturschutzbehörde gebilligt oder angeordnet worden sind;
    
11.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweis-zeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl.
    S.
    1734) an Straßen und Wegen freigestellt;
    
12.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
 Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

#### § 6  
Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  Fichten und andere nicht der potenziell natürlichen Vegetation entsprechende Gehölzarten sollen aus den Beständen entfernt und Nadelholzforste in naturnahe Laubwälder umgewandelt werden;
    
2.  naturferne Abschnitte der Fließe sollen beispielsweise durch Förderung der natürlichen Gewässerdynamik renaturiert werden.
    

#### § 7  
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundes-naturschutzgesetzes Befreiung gewähren.

#### § 8  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu 50 000 (in Worten: fünfzigtausend) Euro geahndet werden.

#### § 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 31 bis 33 und 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

#### § 10  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 17.
Januar 2011

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

**Anlage 1  
**(zu § 2 Absatz 1)

![Das rund 50 Hektar große Naturschutzgebiet "Neudorfer Wald" liegt in der Gemeinde Groß Pankow im Landkreis Prignitz. Es umfasst Teile der Gemarkung Helle und liegt in Nähe der Ortslagen Jakobsdorf und Neudorf.](/br2/sixcms/media.php/69/Nr.%209-1.GIF "Das rund 50 Hektar große Naturschutzgebiet "Neudorfer Wald" liegt in der Gemeinde Groß Pankow im Landkreis Prignitz. Es umfasst Teile der Gemarkung Helle und liegt in Nähe der Ortslagen Jakobsdorf und Neudorf.")

**Anlage 2** (zu § 2 Absatz 1)

Flurstücksliste zur Verordnung über das Naturschutzgebiet „Neudorfer Wald“

Landkreis: Prignitz

Gemeinde

Gemarkung

Flur

Flurstück

Geteilt[\*](#*)

Groß Pankow (Prignitz)

Helle

4

21

Ja

Groß Pankow (Prignitz)

Helle

4

22/1

Nein

Groß Pankow (Prignitz)

Helle

4

22/2

Ja

Groß Pankow (Prignitz)

Helle

4

23

Ja

Groß Pankow (Prignitz)

Helle

4

24

Ja

Groß Pankow (Prignitz)

Helle

4

36

Ja

Groß Pankow (Prignitz)

Helle

4

97

Ja

Groß Pankow (Prignitz)

Helle

4

98/1

Nein

Groß Pankow (Prignitz)

Helle

4

98/2

Nein

Groß Pankow (Prignitz)

Helle

4

98/3

Nein

Groß Pankow (Prignitz)

Helle

4

98/4

Nein

Groß Pankow (Prignitz)

Helle

4

98/5

Nein

Groß Pankow (Prignitz)

Helle

4

98/6

Nein

Groß Pankow (Prignitz)

Helle

4

98/7

Nein

Groß Pankow (Prignitz)

Helle

4

98/8

Nein

Groß Pankow (Prignitz)

Helle

4

98/9

Nein

Groß Pankow (Prignitz)

Helle

4

98/10

Nein

Groß Pankow (Prignitz)

Helle

4

98/11

Nein

Groß Pankow (Prignitz)

Helle

4

98/12

Nein

Groß Pankow (Prignitz)

Helle

4

98/13

Nein

Groß Pankow (Prignitz)

Helle

4

98/14

Nein

Groß Pankow (Prignitz)

Helle

4

98/15

Nein

Groß Pankow (Prignitz)

Helle

4

98/16

Nein

Groß Pankow (Prignitz)

Helle

4

98/17

Nein

Groß Pankow (Prignitz)

Helle

4

98/18

Nein

Groß Pankow (Prignitz)

Helle

4

98/19

Nein

Groß Pankow (Prignitz)

Helle

4

98/20

Nein

Groß Pankow (Prignitz)

Helle

4

98/21

Nein

Groß Pankow (Prignitz)

Helle

4

98/22

Nein

Groß Pankow (Prignitz)

Helle

4

98/23

Ja

Groß Pankow (Prignitz)

Helle

4

99

Ja

Groß Pankow (Prignitz)

Helle

4

269

Ja

\* Flurstück liegt vollständig im NSG = Nein / Flurstück liegt teilweise im NSG = Ja