## Verordnung über die Höchstsätze für den pauschalierten Ersatz des Verdienstausfalls der ehrenamtlichen Feuerwehrangehörigen (Verdienstausfallverordnung - VaV)

Auf Grund des § 49 Absatz 2 Nummer 4 in Verbindung mit § 27 Absatz 2 Satz 5 und Absatz 3 des Brandenburgischen Brand- und Katastrophenschutzgesetzes vom 24.
Mai 2004 (GVBl.
 I S.
197) verordnet der Minister des Innern im Einvernehmen mit dem Minister der Finanzen:

§ 1  
Grundsatz
---------------

(1) Ehrenamtlichen Feuerwehrangehörigen, die beruflich selbstständig oder freiberuflich tätig sind, wird auf Antrag der entstandene Verdienstausfall für die Teilnahme an Einsätzen, Übungen, Lehrgängen und sonstigen Ausbildungsveranstaltungen erstattet.
Dies gilt auch während einer Arbeitsunfähigkeit, die auf den Dienst in der Feuerwehr zurückzuführen ist, für die Dauer von bis zu sechs Wochen.

(2) Für ehrenamtliche Helferinnen und Helfer in Einheiten und Einrichtungen des Katastrophenschutzes gilt diese Verordnung aufgrund von § 19 Absatz 1 Satz 2 des Brandenburgischen Brand- und Katastrophenschutzgesetzes entsprechend.

§ 2  
Höchstbetrag
------------------

(1) Der Ersatz des Verdienstausfalls wird in Form pauschalierter Stundenbeträge je angefangene Stunde gewährt.

(2) Der pauschalierte Stundenbetrag als Ersatz für Verdienstausfall kann bis zu einem Höchstbetrag von 35 Euro und für höchstens zehn Stunden pro Tag gewährt werden.
Für die Teilnahme an Lehrgängen und sonstigen Ausbildungsveranstaltungen wird Verdienstausfall für höchstens acht Stunden gewährt.

§ 3  
Berechnung
----------------

(1) Der Verdienstausfall wird nach Stunden der tatsächlich versäumten Arbeitszeit berechnet.
Arbeitszeit ist die regelmäßige Arbeitszeit.
Diese ist individuell zu ermitteln.

(2) Eine Zahlung wegen Verdienstausfalls entfällt, wenn keine finanziellen Nachteile entstanden sind.
Entgangener Verdienst aus Nebentätigkeiten und Verdienst, der außerhalb der regelmäßigen Arbeitszeit hätte erzielt werden können, bleiben außer Betracht.
Kosten für eine Ersatzkraft werden nicht erstattet.
Entgangener Gewinn wird nicht ersetzt.

§ 4  
Inkrafttreten, Außerkrafttreten
-------------------------------------

Diese Verordnung tritt mit Wirkung vom 15.
August 2014 in Kraft.
Gleichzeitig tritt die Verordnung über die Höchstsätze für den Ersatz von Verdienstausfall nach dem Gesetz über den Brandschutz und die Hilfeleistung bei Unglücksfällen und öffentlichen Notständen vom 28.
Dezember 1992 (GVBl.
1993 II S.
14), die durch Artikel 7 der Verordnung vom 28.
November 2001 (GVBl.
II S.
638, 640) geändert worden ist, außer Kraft.

Potsdam, den 15.
September 2014

Der Minister des Innern

Ralf Holzschuher