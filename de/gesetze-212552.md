## Gesetz zu dem Staatsvertrag zwischen den Ländern Berlin, Brandenburg und Sachsen-Anhalt über die Übertragung der Zuständigkeit in Staatsschutz-Strafsachen

Der Landtag hat das folgende Gesetz beschlossen:

### § 1 

Dem am 8.
November 2010 unterzeichneten Staatsvertrag zwischen den Ländern Berlin, Brandenburg und Sachsen-Anhalt über die Übertragung der Zuständigkeit in Staatsschutz-Strafsachen wird zugestimmt.
Der Vertrag wird nachstehend veröffentlicht.

### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel 5 Absatz 2 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 17.
Februar 2011

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Staatsvertrag](/de/vertraege-237682)