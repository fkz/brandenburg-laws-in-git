## Gesetz zum Ersten Staatsvertrag zur Änderung des Vertrags über die Errichtung des IT-Planungsrats und über die Grundlagen der Zusammenarbeit beim Einsatz der Informationstechnologie in den Verwaltungen von Bund und Ländern (Vertrag zur Ausführung von Artikel 91c GG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem in Berlin am 15.
März 2019 vom Land Brandenburg unterzeichneten Ersten Staatsvertrag zur Änderung des Vertrags über die Errichtung des IT-Planungsrats und über die Grundlagen der Zusammenarbeit beim Einsatz der Informationstechnologie in den Verwaltungen von Bund und Ländern (Vertrag zur Ausführung von Artikel 91c GG) wird zugestimmt.
Der Vertrag wird nachstehend veröffentlicht.

#### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel 3 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 5.
Juni 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Vertrag](/de/vertraege-242585) - Vertrag über die Errichtung des IT-Planungsrats und über die Grundlagen der Zusammenarbeit beim Einsatz der Informationstechnologie in den Verwaltungen von Bund und Ländern - Vertrag zur Ausführung von Artikel 91c GG (IT-Staatsvertrag)

* * *

### Anlagen

1

[Erster Staatsvertrag zur Änderung des Vertrags über die Errichtung des IT-Planungsrats und über die Grundlagen der Zusammenarbeit beim Einsatz der Informationstechnologie in den Verwaltungen von Bund und Ländern – Vertrag zur Ausführung von Artikel 91c GG](/br2/sixcms/media.php/68/GVBl_I_27_2019-Anlage.pdf "Erster Staatsvertrag zur Änderung des Vertrags über die Errichtung des IT-Planungsrats und über die Grundlagen der Zusammenarbeit beim Einsatz der Informationstechnologie in den Verwaltungen von Bund und Ländern – Vertrag zur Ausführung von Artikel 91c GG") 368.3 KB