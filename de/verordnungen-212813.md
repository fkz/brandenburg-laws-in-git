## Verordnung über das leistungsabhängige Aufsteigen in den Grundgehaltsstufen im Land Brandenburg (Brandenburgische Leistungsstufenverordnung - BbgLStV)

Auf Grund des § 25 Absatz 4 Satz 3 und Absatz 5 Satz 3 des Brandenburgischen Besoldungsgesetzes vom 20.
November 2013 (GVBl.
I Nr. 32 S. 2, Nr. 34) verordnet die Landesregierung :

§ 1  
Geltungsbereich
---------------------

(1) Das Aufsteigen in den Stufen des Grundgehalts in den Besoldungsgruppen der Besoldungsordnung A erfolgt gemäß § 25 Absatz 1 Satz 2 des Brandenburgischen Besoldungsgesetzes nach bestimmten Zeiten mit dienstlicher Erfahrung, in denen anforderungsgerechte Leistungen erbracht wurden (Erfahrungszeiten).

(2) Diese Verordnung regelt das leistungsabhängige Aufsteigen und das Verbleiben in den Grundgehaltsstufen bei Beamtinnen und Beamten in Besoldungsgruppen der Besoldungsordnung A im Geltungsbereich des Landesbeamtengesetzes.
Sie gilt nicht für Beamtinnen und Beamte in der Probezeit nach § 18 oder § 120 des Landesbeamtengesetzes sowie für Beamtinnen und Beamte auf Zeit.

§ 2  
Leistungsstufe
--------------------

(1) Das Grundgehalt der nächsthöheren Stufe kann für den Zeitraum bis zum Erreichen der nächsten Stufe gezahlt werden (Leistungsstufe), wenn eine Beamtin oder ein Beamter dauerhaft herausragende Leistungen erbringt und zu erwarten ist, dass dies auch in Zukunft der Fall sein wird.
Durch eine dauerhaft herausragende Leistung entsteht kein Anspruch auf die Vergabe einer Leistungsstufe.

(2) Nach Ablauf der Zeit, um die die Erhöhung des Grundgehalts vorgezogen worden ist, bestimmt sich die weitere Zuordnung zu den Stufen wieder nach der dienstlichen Erfahrung und der Leistung.
Die Vergabe einer Leistungsstufe ist unwiderruflich.

(3) Das höhere Grundgehalt wird von dem auf die Entscheidung über die Vergabe einer Leistungsstufe folgenden Monat an gezahlt.

(4) Nach der Verleihung eines Amtes mit höherem Endgrundgehalt soll in den folgenden zwölf Monaten eine Leistungsstufe nicht vergeben werden.

§ 3  
Verbleiben in der Stufe
-----------------------------

(1) Eine Beamtin oder ein Beamter verbleibt in der bisherigen Stufe des Grundgehalts, wenn und solange die Leistungen den mit dem Amt verbundenen Anforderungen nicht genügen (Aufstiegshemmung).

(2) Verbleibt eine Beamtin oder ein Beamter in der bisherigen Stufe, so ist in halbjährlichen Abständen, beginnend mit dem Wirksamwerden der Maßnahme, zu prüfen, ob die Leistungen inzwischen den mit dem Amt verbundenen Anforderungen genügen.
In diesem Fall erfolgt vom ersten Tag des auf die erneute Leistungsfeststellung folgenden Monats an eine Zuordnung zur nächsthöheren Stufe.
Eine darüberliegende Stufe bis zu der Stufe, in der die Beamtin oder der Beamte sich ohne die Hemmung des Aufstiegs befinden würde, kann jeweils frühestens nach Ablauf eines Jahres aufgrund erneuter Leistungsfeststellung festgesetzt werden, wenn auch in diesem Zeitraum anforderungsgerechte Leistungen erbracht worden sind.

§ 4   
Leistungsfeststellung
----------------------------

(1) Die Leistungsstufe wird auf der Grundlage einer aktuellen Leistungsfeststellung, die die dauerhaft herausragenden Leistungen darstellt, oder der letzten dienstlichen Beurteilung vergeben; eine Leistungsstufe soll jedoch nicht aufgrund einer dienstlichen Beurteilung vergeben werden, die bereits Grundlage einer Verleihung eines Amtes mit höherem Endgrundgehalt war.
 Die aktuelle Leistungsfeststellung kann auf diejenigen Beamtinnen und Beamten beschränkt werden, bei denen tatsächliche Anhaltspunkte bestehen, dass sie dauerhaft herausragende Leistungen erbringen.

(2) Absatz 1 gilt entsprechend für die Feststellung des Verbleibens einer Beamtin oder eines Beamten in der bisherigen Stufe (§ 3 Absatz 1) mit der Maßgabe, dass eine Aktualisierung vorzunehmen ist, wenn die dienstliche Beurteilung oder die gesonderte Leistungsfeststellung älter als zwölf Monate ist.
Es können nur Minderungen der Leistungen berücksichtigt werden, auf die die Beamtin oder der Beamte vor der Feststellung hingewiesen worden ist und für deren Beseitigung ihr oder ihm eine angemessene Frist eingeräumt wurde.

§ 5   
Zahl der Empfängerinnen und Empfänger
============================================

(1) Leistungsstufen können an insgesamt höchstens 15 Prozent der Beamtinnen und Beamten in Besoldungsgruppen der Besoldungsordnung A im Bereich des jeweiligen Dienstherrn gewährt werden, die das Endgrundgehalt noch nicht erreicht haben; maßgebend ist die Zahl der vorhandenen Beamtinnen und Beamten am 1.
Januar des Kalenderjahres.
Die Entscheidungsberechtigten nach § 6 Absatz 1 können Leistungsstufen an jeweils bis zu 15 Prozent der ihnen unterstellten Beamtinnen und Beamten vergeben; dabei darf die nach Satz 1 insgesamt für den Bereich des Dienstherrn geltende Zahl der Empfängerinnen und Empfänger nicht überschritten werden.

(2) Bei der Vergabe sollen alle Laufbahngruppen berücksichtigt werden.

(3) Bei Dienstherren mit weniger als sieben Beamtinnen und Beamten im Sinne des Absatzes 1 Satz 1 kann abweichend von Absatz 1 in jedem Kalenderjahr an eine Beamtin oder einen Beamten eine Leistungsstufe vergeben werden.

(4) Bei den Entscheidungen nach den Absätzen 1 und 2 sind die haushaltsrechtlichen Möglichkeiten zu beachten.

§ 6  
Zuständigkeit und Verfahren
---------------------------------

(1) Die Entscheidung über die Vergabe einer Leistungsstufe und über die Feststellung des Verbleibens in der bisherigen Stufe trifft der Dienstvorgesetzte; die übrigen Vorgesetzten sind zu beteiligen.
Bei obersten Landesbehörden können die Befugnisse nach Satz 1 auf andere Stellen übertragen werden.

(2) Die Entscheidung nach Absatz 1 Satz 1 ist der Beamtin oder dem Beamten schriftlich mitzuteilen.
Widerspruch und Anfechtungsklage haben keine aufschiebende Wirkung (§ 25 Absatz 4 Satz 4 des Brandenburgischen Besoldungsgesetzes).

§ 7  
Inkrafttreten, Außerkrafttreten
-------------------------------------

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2014 in Kraft.
Gleichzeitig tritt die Brandenburgische Leistungsstufenverordnung in der Fassung der Bekanntmachung vom 12.
Dezember 2008 (GVBl.
2009 II S. 33) außer Kraft.

Potsdam, den 7.
August 2014

Die Landesregierung des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister der Finanzen

Christian Görke