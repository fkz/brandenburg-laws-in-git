## Verordnung zur Übertragung von Ermächtigungen nach § 74 des Personenstandsgesetzes

Die Ermächtigung der Landesregierung zum Erlass von Rechtsverordnungen nach § 74 Absatz 1 des Personenstandsgesetzes wird auf das für das Personenstandswesen zuständige Mitglied der Landesregierung übertragen.