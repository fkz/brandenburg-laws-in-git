## Gesetz über die Organisation der ordentlichen Gerichte und Staatsanwaltschaften im Land Brandenburg (Brandenburgisches Gerichtsorganisationsgesetz - BbgGerOrgG)

Abschnitt 1  
Gerichte und Staatsanwaltschaften
-----------------------------------------------

### § 1  
Amtsgerichte

(1) Die Amtsgerichte haben ihren Sitz in Bad Freienwalde (Oder), Bad Liebenwerda, Bernau bei Berlin, Brandenburg an der Havel, Cottbus, Eberswalde, Eisenhüttenstadt, Frankfurt (Oder), Fürstenwalde/Spree, Königs Wusterhausen, Luckenwalde, Lübben (Spreewald), Nauen, Neuruppin, Oranienburg, Perleberg, Potsdam, Prenzlau, Rathenow, Schwedt/Oder, Senftenberg, Strausberg, Zehdenick und Zossen.
Sie werden nach dem jeweiligen Namen der Gemeinde benannt, in der sie ihren Sitz haben.
Das für Justiz zuständige Mitglied der Landesregierung macht die Veränderungen, die sich bei Änderung des Gemeindenamens ergeben, öffentlich bekannt.

(2) Das für Justiz zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung die Einrichtung und Aufhebung von Zweigstellen der Amtsgerichte und das Abhalten auswärtiger Sitzungstage zu regeln.

### § 2  
Amtsgerichtsbezirke

(1) Die Amtsgerichtsbezirke bestehen aus den Gebieten der folgenden Gemeinden:

 

Amtsgericht

Gemeinden

1.

Bad Freienwalde (Oder)

Alt Tucheband, Bad Freienwalde (Oder), Beiersdorf-Freudenberg, Bleyen-Genschmar, Bliesdorf, Falkenberg (16259), Golzow (15328), Gusow-Platkow, Heckelberg-Brunow, Höhenland, Küstriner Vorland, Letschin, Märkische Höhe, Neuhardenberg, Neulewin, Neutrebbin, Oderaue, Prötzel, Reichenow-Möglin,Wriezen, Zechin

2.

Bad Liebenwerda

Bad Liebenwerda, Crinitz, Doberlug-Kirchhain, Elsterwerda, Falkenberg/Elster, Fichtwald, Finsterwalde, Gorden-Staupitz, Gröden, Großthiemig, Heideland, Herzberg (Elster), Hirschfeld, Hohenbucko, Hohenleipisch, Kremitzaue, Lebusa, Lichterfeld-Schacksdorf, Massen-Niederlausitz, Merzdorf, Mühlberg/Elbe, Plessa, Röderland, Rückersdorf, Sallgast, Schilda, Schlieben, Schönborn, Schönewalde, Schraden, Sonnewalde, Tröbitz, Uebigau-Wahrenbrück

3.

Bernau bei Berlin

Ahrensfelde, Bernau bei Berlin, Biesenthal, Breydin, Marienwerder, Melchow, Panketal, Rüdnitz, Sydower Fließ, Wandlitz, Werneuchen

4.

Brandenburg an der Havel

Beetzsee, Beetzseeheide, Bad Belzig, Bensdorf, Borkheide, Borkwalde, Brandenburg an der Havel, Brück, Buckautal, Golzow (14778), Görzke, Gräben, Groß Kreutz (Havel), Havelsee, Kloster Lehnin, Linthe, Mühlenfließ, Niemegk, Päwesin, Planebruch, Planetal, Rabenstein/Fläming, Rosenau, Roskow, Treuenbrietzen, Wenzlow, Wiesenburg/Mark, Wollin, Wusterwitz, Ziesar

5.

Cottbus

Briesen (03096), Burg (Spreewald), Cottbus, Dissen-Striesow, Döbern, Drachhausen, Drebkau, Drehnow, Felixsee, Forst (Lausitz), Groß Schacksdorf-Simmersdorf, Guben, Guhrow, Heinersbrück, Hornow-Wadelsdorf, Jämlitz-Klein Düben, Jänschwalde, Kolkwitz, Neiße-Malxetal, Neuhausen/Spree, Peitz, Schenkendöbern, Schmogrow-Fehrow, Spremberg, Tauer, Teichland, Tschernitz, Turnow-Preilack, Welzow, Werben, Wiesengrund

6.

Eberswalde

Althüttendorf, Britz, Chorin, Eberswalde, Friedrichswalde, Hohenfinow, Joachimsthal, Liepe, Lunow-Stolzenhagen, Niederfinow, Oderberg, Parsteinsee, Schorf­heide, Ziethen

7.

Eisenhüttenstadt

Brieskow-Finkenheerd, Eisenhüttenstadt, Groß Lindow, Grunow-Dammendorf, Lawitz, Mixdorf, Müllrose, Neißemünde, Neuzelle, Ragow-Merz, Schlaubetal, Siehdichum, Vogelsang, Wiesenau, Ziltendorf

8.

Frankfurt (Oder)

Falkenhagen (Mark), Fichtenhöhe, Frankfurt (Oder), Lebus, Lietzen, Lindendorf, Podelzig, Reitwein, Seelow, Treplin, Vierlinden, Zeschdorf

9.

Fürstenwalde/Spree

Bad Saarow, Beeskow, Berkenbrück, Briesen (Mark) (15518), Diensdorf-Radlow, Erkner, Friedland, Fürstenwalde/Spree, Gosen-Neu Zittau, Grünheide (Mark), Jacobsdorf, Langewahl, Madlitz-Wilmersdorf, Rauen, Reichenwalde, Rietz-Neuendorf, Schöneiche bei Berlin, Spreenhagen, Steinhöfel, Storkow (Mark), Tauche, Wendisch Rietz, Woltersdorf

10.

Königs Wusterhausen

Bestensee, Eichwalde, Groß Köris, Halbe, Heidesee, Königs Wusterhausen, Märkisch Buchholz, Mittenwalde (15749), Münchehofe, Schönefeld, Schulzendorf, Schwerin, Teupitz, Wildau, Zeuthen

11.

Luckenwalde

Dahme/Mark, Dahmetal, Ihlow, Jüterbog, Luckenwalde, Niederer Fläming, Niedergörsdorf, Nuthe-Urstromtal, Trebbin

12.

Lübben (Spreewald)

Alt Zauche-Wußwerk, Bersteland, Byhleguhre-Byhlen, Drahnsdorf, Golßen, Heideblick, Jamlitz, Kasel-Golzig, Krausnick-Groß Wasserburg, Lieberose, Lübben (Spreewald), Lübbenau/Spreewald, Luckau, Märkische Heide, Neu Zauche, Rietzneuendorf-Staakow, Schlep­zig, Schönwald, Schwielochsee, Spreewaldheide, Steinreich, Straupitz, Unterspreewald

13.

Nauen

Brieselang, Dallgow-Döberitz, Falkensee, Ketzin/Havel, Nauen, Schönwalde-Glien, Wustermark

14.

Neuruppin

Breddin, Dabergotz, Dreetz, Fehrbellin, Heiligengrabe, Herzberg (Mark) (16835), Kyritz, Lindow (Mark), Märkisch Linden, Neuruppin, Neustadt (Dosse), Rheinsberg, Rüthnick, Sieversdorf-Hohenofen, Storbeck-Frankendorf, Stüdenitz-Schönermark, Temnitzquell, Temnitztal, Vielitzsee, Walsleben, Wittstock/Dosse, Wusterhausen/Dosse, Zernitz-Lohm

15.

Oranienburg

Birkenwerder, Glienicke/Nordbahn, Hennigsdorf, Hohen Neuendorf, Kremmen, Leegebruch,Mühlenbecker Land, Oberkrämer, Oranienburg, Velten

16.

Perleberg

Bad Wilsnack, Berge, Breese, Cumlosen, Gerdshagen, Groß Pankow (Prignitz), Gülitz-Reetz, Gumtow, Halenbeck-Rohlsdorf, Karstädt, Kümmernitztal, Lanz, Legde/Quitzöbel, Lenzen (Elbe), Lenzerwische, Marienfließ, Meyenburg, Perleberg, Pirow, Plattenburg, Pritzwalk, Putlitz, Rühstädt, Triglitz, Weisen, Wittenberge

17.

Potsdam

Beelitz, Kleinmachnow, Michendorf, Nuthetal, Potsdam, Schwielowsee, Seddiner See, Stahnsdorf, Teltow, Werder (Havel)

18.

Prenzlau

Boitzenburger Land, Brüssow, Carmzow-Wallmow, Flieth-Stegelitz, Gerswalde, Göritz, Gramzow, Grünow, Lychen, Milmersdorf, Mittenwalde (17268), Nordwestuckermark, Oberuckersee, Prenzlau, Randowtal, Schenkenberg, Schönfeld, Temmen-Ringenwalde, Templin, Uckerfelde, Uckerland, Zichow

19.

Rathenow

Friesack, Gollenberg, Großderschau, Havelaue, Kleßen-Görne, Kotzen, Märkisch Luch, Milower Land, Mühlenberge, Nennhausen, Paulinenaue, Pessin, Premnitz, Rathenow, Retzow, Rhinow, Seeblick, Stechow-Ferchesar, Wiesenaue

20.

Schwedt/Oder

Angermünde, Berkholz-Meyenburg, Casekow, Gartz (Oder), Hohenselchow-Groß Pinnow, Mark Landin, Mescherin, Passow, Pinnow, Schöneberg, Schwedt/Oder, Tantow

21.

Senftenberg

Altdöbern, Bronkow, Calau, Frauendorf (01945), Großkmehlen, Großräschen, Grünewald, Guteborn, Hermsdorf, Hohenbocka, Kroppen, Lauchhammer, Lindenau, Luckaitztal, Neupetershain, Neu-Seeland, Ortrand, Ruhland, Schipkau, Schwarzbach, Schwarzheide, Senftenberg, Tettau, Vetschau/Spreewald

22.

Strausberg

Altlandsberg, Buckow (Märkische Schweiz), Fredersdorf-Vogelsdorf, Garzau-Garzin, Hoppegarten, Müncheberg, Neuenhagen bei Berlin, Oberbarnim, Petershagen/Eggers­dorf, Rehfelde, Rüdersdorf bei Berlin, Strausberg, Waldsieversdorf

23.

Zehdenick

Fürstenberg/Havel, Gransee, Großwoltersdorf, Liebenwalde, Löwenberger Land, Schönermark, Sonnenberg, Stechlin, Zehdenick

24.

Zossen

Am Mellensee, Baruth/Mark, Blankenfelde-Mahlow, Großbeeren, Ludwigsfelde, Rangsdorf, Zossen

(2) Zum Amtsgerichtsbezirk gehört das jeweilige gesamte Gebiet der zugeordneten Gemeinde.
Wird eine neue Gemeinde aus Gemeinden oder Teilen von Gemeinden gebildet, die mehreren Amtsgerichtsbezirken angehören, so wird die neu gebildete Gemeinde dem Amtsgerichtsbezirk zugeordnet, in dem zur Zeit der Gebietsänderung die Mehrheit der Einwohnerinnen und Einwohner der neu gebildeten Gemeinde ihren Wohnsitz hat; bei gleicher Anzahl ist die größere Fläche maßgebend.
Das für Justiz zuständige Mitglied der Landesregierung macht die Veränderungen, die sich nach den Sätzen 1 und 2 ergeben, öffentlich bekannt.

### § 3  
Landgerichte

Die Landgerichte haben ihren Sitz in Cottbus, Frankfurt (Oder), Neuruppin und Potsdam.
Sie werden nach dem jeweiligen Namen der Gemeinde benannt, in der sie ihren Sitz haben.
Das für Justiz zuständige Mitglied der Landesregierung macht die Veränderungen, die sich bei Änderung des Gemeindenamens ergeben, öffentlich bekannt.

### § 4   
Landgerichtsbezirke

Die Landgerichtsbezirke bestehen aus den Gebieten der folgenden Amtsgerichtsbezirke:

 

Landgericht

Amtsgerichte

1.

Cottbus

Bad Liebenwerda, Cottbus, Königs Wusterhausen, Lübben (Spreewald), Senftenberg

2.

Frankfurt (Oder)

Bad Freienwalde (Oder), Bernau bei Berlin, Eberswalde, Eisenhüttenstadt, Frankfurt (Oder), Fürstenwalde/Spree, Strausberg

3.

Neuruppin

Neuruppin, Oranienburg, Perleberg, Prenzlau, Schwedt/Oder, Zehdenick

4.

Potsdam

Brandenburg an der Havel, Luckenwalde, Nauen, Potsdam, Rathenow, Zossen

### § 5  
Oberlandesgericht

Das Oberlandesgericht hat seinen Sitz in Brandenburg an der Havel.
Es führt die Bezeichnung „Brandenburgisches Oberlandesgericht“.
Sein Bezirk umfasst das jeweilige Gebiet des gesamten Landes.

### § 6  
Zahl der Abteilungen und Spruchkörper

(1) Die Zahl der ständigen Abteilungen eines Amtsgerichts wird nach Anhörung des Präsidiums mit Zustimmung des Präsidenten des Landgerichts von dem Direktor des Amtsgerichts bestimmt.
Ist ein Amtsgericht mit einem Präsidenten besetzt, bestimmt er die Zahl der ständigen Abteilungen nach Anhörung des Präsidiums mit Zustimmung des Präsidenten des Oberlandesgerichts.

(2) Die Zahl der ständigen Spruchkörper eines Gerichts wird nach Anhörung des Präsidiums von dem Präsidenten des Gerichts im Einvernehmen mit dem für Justiz zuständigen Mitglied der Landesregierung bestimmt.

### § 7  
Staatsanwaltschaften

(1) Bei dem Oberlandesgericht besteht die Generalstaatsanwaltschaft.
Sie führt die Bezeichnung „Generalstaatsanwaltschaft des Landes Brandenburg“.

(2) Bei den Landgerichten bestehen Staatsanwaltschaften, die zugleich die staatsanwaltschaftlichen Geschäfte bei den Amtsgerichten des Landgerichtsbezirks wahrnehmen.
Die Staatsanwaltschaften werden nach dem jeweiligen Namen der Gemeinde benannt, in der sie ihren Sitz haben.

(3) Das für Justiz zuständige Mitglied der Landesregierung kann für den Bezirk eines oder mehrerer Amtsgerichte eines Landgerichtsbezirks Zweigstellen der Staatsanwaltschaft einrichten und aufheben.

Abschnitt 2  
Dienstaufsicht
----------------------------

### § 8   
Dienstaufsicht

(1) Oberste Dienstaufsichtsbehörde für die Gerichte und Staatsanwaltschaften ist das für Justiz zuständige Mitglied der Landesregierung.
Im Übrigen üben die Dienstaufsicht aus:

1.  der Präsident des Oberlandesgerichts und die Präsidenten der Landgerichte über die Gerichte ihres Bezirks,
2.  die Präsidenten oder Direktoren der Amtsgerichte über ihr Gericht, die Direktoren jedoch nicht über die Richterinnen und Richter ihres Gerichts,
3.  der Generalstaatsanwalt über die Staatsanwaltschaften,
4.  die Leitenden Oberstaatsanwälte über ihre Staatsanwaltschaft.

(2) Das Amtsgericht Potsdam ist mit einem Präsidenten besetzt.
Es untersteht nicht der Dienstaufsicht des Präsidenten des Landgerichts.

Abschnitt 3   
Ergänzende Zuständigkeitsregelungen
--------------------------------------------------

### § 9   
Zuständigkeiten der Landgerichte und Landgerichtspräsidenten

(1) Soweit der ordentliche Rechtsweg gegeben und gesetzlich nichts anderes bestimmt ist, sind die Landgerichte ohne Rücksicht auf den Wert des Streitgegenstandes ausschließlich zuständig

1.  für Ansprüche gegen den Staat oder eine Körperschaft des öffentlichen Rechts wegen Verfügungen der Verwaltungsbehörden,
2.  für Ansprüche wegen öffentlicher Abgaben.

(2) Soweit der ordentliche Rechtsweg gegeben und gesetzlich nichts anderes bestimmt ist, ist für die Verfahren nach den §§ 14 und 14a des Entschädigungsgesetzes in der Fassung des Gesetzes zur Anpassung von Regelungen über Rechtsmittel der Bürger und zur Festlegung der gerichtlichen Zuständigkeit für die Nachprüfung von Verwaltungsentscheidungen vom 14. Dezember 1988 (GBl.
I Nr. 28 S. 329) das Landgericht, Kammer für Baulandsachen, zuständig.
Für diese Verfahren gelten die §§ 217 bis 231 des Baugesetzbuches entsprechend.

(3) Der Präsident des Landgerichts ist für die Beglaubigung von Unterschriften zum Zwecke der Legalisation von gerichtlichen, staatsanwaltschaftlichen, notariellen und sonstigen Urkunden aus dem Bereich der Justiz zuständig.

### § 10  
Gerichtsvollzieherinnen und Gerichtsvollzieher

(1) Die Gerichtsvollzieherinnen und Gerichtsvollzieher sind auch zuständig

1.  für die Aufnahme von Vermögensverzeichnissen und Inventaren im Auftrag des Gerichts,
2.  für Siegelungen und Entsiegelungen im Auftrag des Gerichts,
3.  für die Aufnahme von Wechsel- und Scheckprotesten,
4.  für die Durchführung freiwilliger Versteigerungen von beweglichen Sachen und von Früchten, die vom Boden noch nicht getrennt sind,
5.  für die Niederschrift über das tatsächliche Angebot einer Leistung oder das tatsächliche Angebot der geschuldeten Leistung,
6.  für Zustellungen und Vollstreckungshandlungen im Auftrag des Gerichts.

(2) Gerichtsvollzieherinnen und Gerichtsvollzieher können Aufträge zur freiwilligen Versteigerung nach ihrem Ermessen ablehnen.

(3) § 155 des Gerichtsverfassungsgesetzes gilt entsprechend.

Abschnitt 4  
Handelsrichterinnen und Handelsrichter
----------------------------------------------------

### § 11  
Ernennung, Vereidigung

(1) Die ehrenamtlichen Richterinnen und Richter als Beisitzer einer Kammer für Handelssachen (Handelsrichterinnen und Handelsrichter) werden auf Vorschlag der jeweils zuständigen Industrie- und Handelskammern vom Präsidenten des Oberlandesgerichts ernannt.

(2) Die Handelsrichterinnen und Handelsrichter erhalten über ihre Ernennung eine Urkunde.
Sie werden vor ihrer ersten Heranziehung in öffentlicher Sitzung des Spruchkörpers, dem sie angehören, durch die Vorsitzende oder den Vorsitzenden vereidigt.

Abschnitt 5  
Ehrenamtliche Richterinnen und Richter in Landwirtschaftssachen
-----------------------------------------------------------------------------

### § 12   
Vorschlagslisten

(1) Die Vorschlagslisten für die ehrenamtlichen Richterinnen und Richter in Landwirtschaftssachen gemäß § 4 Absatz 1 des Gesetzes über das gerichtliche Verfahren in Landwirtschaftssachen in der im Bundesgesetzblatt Teil III, Gliederungsnummer 317-1, veröffentlichten bereinigten Fassung, das zuletzt durch Artikel 43 des Gesetzes vom 17. Dezember 2008 (BGBl.
I S. 2586, 2707) geändert worden ist, in der jeweils geltenden Fassung werden von dem für Landwirtschaft zuständigen Mitglied der Landesregierung nach Anhörung der land- und forstwirtschaftlichen Berufsverbände aufgestellt.
Für das Oberlandesgericht und die Amtsgerichte sind gesonderte Listen aufzustellen.
Wer zur ehrenamtlichen Richterin oder zum ehrenamtlichen Richter beim Oberlandesgericht vorgeschlagen wird, darf nicht zugleich zur ehrenamtlichen Richterin oder zum ehrenamtlichen Richter bei einem Amtsgericht vorgeschlagen werden.

(2) In der Vorschlagsliste sollen in angemessener Zahl landwirtschaftliche Pächterinnen, Pächter, Verpächterinnen und Verpächter enthalten sein.

(3) Für jede vorgeschlagene Person sind anzugeben:

1.  Name und Vorname,
2.  Anschrift,
3.  Geburtsdatum und Geburtsort,
4.  Stellung im Beruf, insbesondere, ob und in welchem Umfang sie Land als selbstwirtschaftende Eigentümerin, selbstwirtschaftender Eigentümer, Verpächterin, Verpächter, Pächterin oder Pächter besitzt oder zuletzt besessen hat,
5.  ob und für welches Gericht sie bereits früher als ehrenamtliche Richterin oder ehrenamtlicher Richter in Landwirtschaftssachen berufen oder vorgeschlagen war.
    

(4) Lässt sich aus den vorgeschlagenen Personen die erforderliche Anzahl von ehrenamtlichen Richterinnen und Richtern nicht berufen, so kann der Präsident des Oberlandesgerichts eine Ergänzungsliste anfordern.
Er bestimmt dabei, wie viele Personen vorzuschlagen sind.
Die Absätze 2 und 3 gelten entsprechend.

Abschnitt 6  
Zuständigkeit der Gerichte bei Änderung der Gerichtseinteilung
----------------------------------------------------------------------------

§ 13  
Verfahrensübergang
-------------------------

(1) Wird ein Gericht aufgehoben und sein gesamter Bezirk dem Bezirk eines anderen Gerichts zugeordnet, so tritt dieses Gericht an die Stelle des aufgehobenen Gerichts.

(2) Wird ein Gericht aufgehoben und sein Bezirk auf die Bezirke mehrerer Gerichte aufgeteilt, so gehen die anhängigen Verfahren auf das Gericht über, das zuständig sein würde, wenn die Angelegenheit erst nach der Aufhebung des Gerichts anhängig geworden wäre.
Lässt sich ein zuständiges Gericht nach Satz 1 nicht bestimmen, so geht die Zuständigkeit auf das Gericht über, dem der Sitz des aufgehobenen Gerichts zugeordnet wird.

(3) Ist im Zeitpunkt der Aufhebung eines Gerichts die Hauptverhandlung in einer Strafsache noch nicht beendet, so kann sie vor dem nach den Absätzen 1 und 2 zuständigen Gericht fortgesetzt werden, wenn dieselben Richterinnen und Richter weiterhin an ihr teilnehmen.

### § 14  
Rechtsmittel

(1) Wird ein Gericht einem anderen übergeordneten Gericht zugeordnet, so ist für die Entscheidung über Rechtsmittel, die sich gegen eine vor Inkrafttreten der Änderung erlassene Entscheidung richten, das Gericht zuständig, das dem erkennenden Gericht vor dem Inkrafttreten der Änderung übergeordnet war.
Für die Entscheidung über Rechtsmittel, die sich gegen die Entscheidung eines aufgehobenen Gerichts richten, ist das Gericht zuständig, das dem aufgehobenen Gericht übergeordnet war.

(2) Ist das übergeordnete Gericht, das für die Entscheidung über ein Rechtsmittel nach Absatz 1 zuständig sein würde, aufgehoben und ist sein gesamter Bezirk dem Bezirk eines anderen Gerichts zugeordnet, so tritt dieses Gericht an die Stelle des aufgehobenen Gerichts.
Wird der Bezirk des aufgehobenen übergeordneten Gerichts auf die Bezirke mehrerer Gerichte aufgeteilt, so ist das Gericht für die Entscheidung über die Rechtsmittel zuständig, das zuständig sein würde, wenn das Rechtsmittel erst nach der Aufhebung des Gerichts eingelegt worden wäre.

### § 15  
Anträge, Erklärungen, Rechtsmittel

(1) Ist der Eintritt von Rechtswirkungen davon abhängig, dass ein Antrag oder eine Erklärung innerhalb einer bestimmten Frist bei Gericht eingeht, so gilt der Antrag oder die Erklärung als beim zuständigen Gericht eingegangen, wenn der Antrag oder die Erklärung vor Fristablauf bei dem Gericht eingeht, das zu Beginn der Frist örtlich zuständig war und die Zuständigkeit während des Laufs der Frist durch eine Änderung der Gerichtsbezirke verloren hat.

(2) Ein Rechtsmittel gilt als bei dem zuständigen Gericht eingelegt, auch wenn es

1.  bei einem anderen der Gerichte eingelegt ist, auf die der Bezirk des aufgehobenen Gerichts aufgeteilt ist (§ 13 Absatz 2, § 14 Absatz 2 Satz 2),
2.  bei dem nach Inkrafttreten der Änderung der Zuordnung zuständigen Gericht eingelegt ist (§ 14 Absatz 1 Satz 1),
3.  bei einem Gericht eingelegt ist, das dem Gericht übergeordnet ist, das das aufgehobene Gericht ersetzt (§ 14 Absatz 1 Satz 2).

(3) Das unzuständige Gericht hat die Sache von Amts wegen an das zuständige Gericht abzugeben.

### § 16  
Kostenbefreiung

Ändert sich die Zuständigkeit eines Gerichts durch die Aufhebung des bisher zuständigen Gerichts oder durch eine Änderung der Gerichtsbezirke oder der Zuordnung, so werden für die dadurch veranlassten gerichtlichen Handlungen Kosten nicht erhoben.

### § 17   
Kreis- und Bezirksgerichte

Weisen Rechtsvorschriften den Gerichten Aufgaben zu oder bezeichnen sie Gerichte, treten an die Stelle der Kreisgerichte die Amtsgerichte, an die Stelle der Bezirksgerichte die Landgerichte und an die Stelle der besonderen Senate bei den Bezirksgerichten das Oberlandesgericht, soweit keine besondere Bestimmung getroffen ist.

Abschnitt 7  
Übergangsvorschriften
-----------------------------------

### § 18   
Schöffinnen und Schöffen des Amtsgerichts Guben

(1) Die zur Zeit des Inkrafttretens dieses Gesetzes für das Amtsgericht Guben gewählten Hauptschöffinnen und Hauptschöffen, die in den Gemeinden Guben, Jänschwalde und Schenkendöbern ihren Wohnsitz haben, werden für den Rest ihrer Amtszeit der Zweigstelle Guben des Amtsgerichts Cottbus zugewiesen.
Die weiteren zur Zeit des Inkrafttretens dieses Gesetzes für das Amtsgericht Guben gewählten Hauptschöffinnen und Hauptschöffen werden für den Rest ihrer Amtszeit dem Amtsgericht Lübben (Spreewald) zugewiesen.
Die zur Zeit des Inkrafttretens dieses Gesetzes für das Amtsgericht Guben gewählten Hilfsschöffinnen und Hilfsschöffen werden für den Rest ihrer Amtszeit der Zweigstelle Guben des Amtsgerichts Cottbus zugewiesen.

(2) Wird während des Jahres 2012 bei dem Amtsgericht Lübben (Spreewald) ein weiteres Schöffengericht gebildet, so werden zur Auslosung der dafür benötigten Hauptschöffinnen und Hauptschöffen die nach Absatz 1 Satz 2 zugewiesenen Schöffinnen und Schöffen zur Hilfsschöffinnen- und Hilfsschöffenliste hinzugesetzt (§ 46 Satz 1 des Gerichtsverfassungsgesetzes).

(3) Nach Absatz 1 Satz 2 dem Amtsgericht Lübben (Spreewald) zugewiesene Schöffinnen und Schöffen, die an einer Hauptverhandlung des Amtsgerichts Guben teilnehmen, die vor dem Inkrafttreten dieses Gesetzes begonnen hat und an der Zweigstelle Guben des Amtsgerichts Cottbus fortgesetzt wird, bleiben für die Dauer der Hauptverhandlung auch der Zweigstelle Guben des Amtsgerichts Cottbus zugewiesen.

### § 19  
Verfahren des Amtsgerichts Guben

Abweichend von § 13 Absatz 2 gehen die zur Zeit des Inkrafttretens dieses Gesetzes bei dem Amtsgericht Guben anhängigen Verfahren sämtlich auf das Amtsgericht Cottbus über.
§ 13 Absatz 3 bleibt unberührt.

### § 20  
Schöffinnen und Schöffen des Landgerichts Frankfurt (Oder)

(1) Die am 1.
Januar 2013 vom Amtsgericht Schwedt/Oder für das Landgericht Frankfurt (Oder) gewählten Schöffinnen und Schöffen werden für den Rest ihrer Amtszeit dem Landgericht Neuruppin zugewiesen.

(2) Nach Absatz 1 dem Landgericht Neuruppin zugewiesene Schöffinnen und Schöffen, die an einer Hauptverhandlung des Landgerichts Frankfurt (Oder) teilnehmen, die vor dem 1.
Januar 2013 begonnen hat und dort fortgesetzt wird, bleiben für die Dauer der Hauptverhandlung auch dem Landgericht Frankfurt (Oder) zugewiesen.

### § 21  
Handelsrichterinnen und Handelsrichter des Landgerichts Frankfurt (Oder)

Die am 1.
Januar 2013 bei dem Landgericht Frankfurt (Oder) ernannten ehrenamtlichen Richterinnen und Richter der Kammern für Handelssachen, die im Bezirk dieses Gerichts weder wohnen noch eine Handelsniederlassung haben noch einem Unternehmen angehören, das in diesem Bezirk seinen Sitz oder seine Niederlassung hat, werden für den Rest ihrer Amtszeit dem Landgericht Neuruppin zugewiesen, wenn sie im Bezirk des Amtsgerichts Schwedt/Oder wohnen oder eine Handelsniederlassung haben oder einem Unternehmen angehören, das in diesem Bezirk seinen Sitz oder seine Niederlassung hat.

### § 22  
Ehrenamtliche Richterinnen und Richter in Landwirtschaftssachen des Amtsgerichts Frankfurt (Oder)

Die am 1.
Januar 2013 bei dem Amtsgericht Frankfurt (Oder) berufenen ehrenamtlichen Richterinnen und Richter in Landwirtschaftssachen (§ 1 des Gesetzes über gerichtliche Verfahren in Landwirtschaftssachen und § 65 des Landwirtschaftsanpassungsgesetzes), die im Bezirk des Amtsgerichts Schwedt/Oder die Landwirtschaft ausüben, werden für den Rest ihrer Amtszeit dem Amtsgericht Neuruppin zugewiesen.

### § 23  
Schöffinnen und Schöffen des Landgerichts Potsdam

(1) Die am 1.
Januar 2013 vom Amtsgericht Königs Wusterhausen für das Landgericht Potsdam gewählten Schöffinnen und Schöffen werden für den Rest ihrer Amtszeit dem Landgericht Cottbus zugewiesen.

(2) Nach Absatz 1 dem Landgericht Cottbus zugewiesene Schöffinnen und Schöffen, die an einer Hauptverhandlung des Landgerichts Potsdam teilnehmen, die vor dem 1.
Januar 2013 begonnen hat und dort fortgesetzt wird, bleiben für die Dauer der Hauptverhandlung auch dem Landgericht Potsdam zugewiesen.

### § 24  
Handelsrichterinnen und Handelsrichter des Landgerichts Potsdam

Die am 1.
Januar 2013 bei dem Landgericht Potsdam ernannten ehrenamtlichen Richterinnen und Richter der Kammern für Handelssachen, die im Bezirk dieses Gerichts weder wohnen noch eine Handelsniederlassung haben noch einem Unternehmen angehören, das in diesem Bezirk seinen Sitz oder seine Niederlassung hat, werden für den Rest ihrer Amtszeit dem Landgericht Cottbus zugewiesen, wenn sie im Bezirk des Amtsgerichts Königs Wusterhausen wohnen oder eine Handelsniederlassung haben oder einem Unternehmen angehören, das in diesem Bezirk seinen Sitz oder seine Niederlassung hat.

### § 25  
Ehrenamtliche Richterinnen und Richter in Landwirtschaftssachen des Amtsgerichts Königs Wusterhausen

(1) Die am 1.
Januar 2013 bei dem Amtsgericht Königs Wusterhausen berufenen ehrenamtlichen Richterinnen und Richter in Landwirtschaftssachen (§ 1 des Gesetzes über gerichtliche Verfahren in Landwirtschaftssachen und § 65 des Landwirtschaftsanpassungsgesetzes), die im Bezirk dieses Gerichts die Landwirtschaft ausüben, werden für den Rest ihrer Amtszeit auch dem Amtsgericht Cottbus zugewiesen.

(2) Die am 1.
Januar 2013 bei dem Amtsgericht Königs Wusterhausen berufenen ehrenamtlichen Richterinnen und Richter in Landwirtschaftssachen (§ 1 des Gesetzes über gerichtliche Verfahren in Landwirtschaftssachen und § 65 des Landwirtschaftsanpassungsgesetzes), die nicht nach Absatz 1 dem Amtsgericht Cottbus zugewiesen sind, werden für den Rest ihrer Amtszeit auch dem Amtsgericht Rathenow zugewiesen.