## Neunte Verordnung zur Übertragung der Befugnis für den Erlass und die Aufhebung von Rechtsverordnungen zur Festsetzung von Naturschutz- und Landschaftsschutzgebieten

#### § 1 

Die Befugnis des für Naturschutz und Landschaftspflege zuständigen Mitgliedes der Landesregierung zum Erlass von Rechtsverordnungen zur Festsetzung von Naturschutzgebieten und Landschaftsschutzgebieten nach den §§ 22, 23 und 26 des Bundesnaturschutzgesetzes und des § 8 Absatz 1 des Brandenburgischen Naturschutzausführungsgesetzes wird

1.  für die im Landkreis Märkisch-Oderland geplanten Landschaftsschutzgebiete
    1.  „Oderhänge Seelow-Lebus“,
    2.  „Trepliner Seen, Booßener und Altzeschdorfer Mühlenfließ“,
    3.  „Seenkette des Platkower Mühlenfließes/Heidelandschaft Worin/Bergvorwerk“auf den Landkreis Märkisch-Oderland als untere Naturschutzbehörde,
2.  für das im Landkreis Potsdam-Mittelmark geplante Naturschutzgebiet „Bogendüne Renneberge“ auf den Landkreis Potsdam-Mittelmark als untere Naturschutzbehörde

übertragen.

#### § 2 

Die Befugnis des für Naturschutz und Landschaftspflege zuständigen Mitgliedes der Landesregierung zur Aufhebung von Rechtsverordnungen zur Festsetzung von Landschaftsschutzgebieten nach den §§ 22 und 26 des Bundesnaturschutzgesetzes und des § 8 Absatz 1 des Brandenburgischen Naturschutzausführungsgesetzes wird für die folgenden im Landkreis Märkisch-Oderland gelegenen Landschaftsschutzgebiete auf den Landkreis Märkisch-Oderland als untere Naturschutzbehörde übertragen:

1.  „Oderhänge Seelow-Lebus“,
2.  „Trepliner Seen, Booßener und Altzeschdorfer Mühlenfließ“,
3.  „Seenkette des Platkower Mühlenfließes/Heidelandschaft Worin“,
4.  „Odervorland Groß Neuendorf-Lebus“.