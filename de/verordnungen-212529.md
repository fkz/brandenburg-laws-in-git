## Verordnung über das Naturschutzgebiet „Saugberge“

Auf Grund der §§ 22, 23 und § 32 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

#### § 1  
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Prignitz wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Saugberge“.

#### § 2   
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 80 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Gemeinde:**  

**Gemarkung:**  

**Flur:**

Pritzwalk  

Schönhagen (P)  

1;

Pritzwalk  

Steffenshagen  

2.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 bei-gefügt.
Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 2 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in der topografischen Karte im Maßstab 1 : 10 000 mit dem Titel „Topografische Karte zur Verordnung über das Naturschutzgebiet ‚Saugberge‘“ und in der Liegenschaftskarte im Maßstab 1 : 2 500 mit dem Titel „Liegenschaftskarte zur Verordnung über das Naturschutzgebiet ‚Saugberge‘“ mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die topografische Karte ermöglicht die Verortung im Gelände.
 Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Liegenschaftskarte.
 Die Karten sind unterzeichnet von dem Siegelverwahrer, Siegelnummer 22, des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz am 11.
August 2010.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Prignitz, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3  
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere naturnaher Wälder mit hohem Alt- und Totholzanteil wie Stieleichen-Hainbuchenwälder, Eichen-Buchenmischwälder, Eichenmischwälder bodensaurer Standorte und reich strukturierter Waldränder, arten- und blütenreicher Frischwiesen, Staudenfluren frischer bis trockener Standorte sowie ruderaler Magerrasen;
    
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützter Arten, insbesondere Wald-Schlüsselblume (Primula elatior);
    
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Fledermäuse und Amphibien;
    
4.  die Erhaltung und Entwicklung der besonderen Eigenart und Vielfalt des Gebietes mit reich strukturierten Wald- und Wiesenflächen.
    

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teiles des Europäischen Vogelschutzgebietes „Agrarlandschaft Prignitz-Stepenitz“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) in seiner Funktion als Lebensraum von Arten nach Anhang I der Richtlinie 2009/147/EG**_,_** insbesondere Baumfalke (Falco subbuteo), Neuntöter (Lanius collurio), Rotmilan (Milvus milvus), Schwarzspecht (Dryocopus martius) und Schwarzstorch (Ciconia nigra) einschließlich ihrer Brut- und Nahrungsbiotope.

#### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
    
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
    
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
    
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
    
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
    
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
    
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
    
8.  die Ruhe der Natur durch Lärm zu stören;
    
9.  das Gebiet außerhalb der dem öffentlichen Verkehr gewidmeten Wege zu betreten, ausgenommen ist das Betreten von Wegen zum Zwecke der Erholung und des Sammelns von Pilzen und Waldfrüchten gemäß § 5 Absatz 1 Nummer 7 in der Zeit jeweils vom 1.
    August eines jeden Jahres bis zum 28.
    Februar des Folgejahres;
    
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 51 des Brandenburgischen Naturschutzgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
    
11.  mit Fahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
    
12.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
    
13.  Hunde frei laufen zu lassen;
    
14.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
    
15.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (zum Beispiel Abwasser, Klärschlamm und Bioabfälle) zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
    
16.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
    
17.  Tiere zu füttern oder Futter bereitzustellen;
    
18.  Tiere auszusetzen oder Pflanzen anzusiedeln;
    
19.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
    
20.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
    
21.  Pflanzenschutzmittel jeder Art anzuwenden;
    
22.  Wiesen, Weiden oder sonstiges Grünland umzubrechen, nachzusäen oder neu anzusäen.
    

#### § 5  
Zulässige Handlungen

(1)  Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den Grünlandflächen der Gemarkung Schönhagen, Flur 1, Flurstücke 49 bis 52 mit der Maßgabe, dass
    
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel und Sekundärrohstoffdünger wie zum Beispiel Abwasser, Klärschlamm und Bioabfälle einzusetzen.
        Der Einsatz von Kartoffelfruchtwasser bleibt zulässig,  
        
    2.  § 4 Absatz 2 Nummer 21 und 22 gilt, wobei bei Narbenschäden mit Zustimmung der unteren Naturschutzbehörde eine umbruchlose Nachsaat zulässig ist;  
        
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  forstwirtschaftliche Maßnahmen in der Zeit vom 1.
        März bis zum 31.
        August eines jeden Jahres unzulässig sind,
        
    2.  nur heimische und standortgerechte Baumarten unter Ausschluss eingebürgerter Arten eingebracht werden, wobei die Entwicklung von Reinbeständen unzulässig ist und die in § 3 Absatz 1 Nummer 1 aufgeführten Waldgesellschaften zu erhalten sind,
        
    3.  mindestens fünf Stämme Altholz je Hektar mit einem Mindestdurchmesser von 40 Zentimetern in  
        1,30 Meter Höhe über dem Stammfuß aus der Nutzung zu nehmen und dauerhaft zu markieren sind.
        In Jungbeständen ist ein solcher Altholzanteil zu entwickeln,
        
    4.  eine naturnahe Waldentwicklung mit einem Totholzanteil von mindestens zehn Prozent des aktuellen Bestandesvorrates zu gewährleisten ist, wobei mindestens fünf Stück stehendes Totholz je Hektar mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß und einer Mindesthöhe von fünf Metern, sofern vorhanden, zu erhalten sind,
        
    5.  eine Nutzung der in § 3 Absatz 1 Nummer 1 genannten Waldgesellschaften nur einzelstamm- bis truppweise erfolgt; auf den übrigen Waldflächen sind Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von 0,5 Hektar zulässig,
        
    6.  Horst- und Höhlenbäume nicht entfernt werden,
        
    7.  § 4 Absatz 2 Nummer 21 gilt;
        
3.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass diese in der Zeit vom 1.
        März bis 31.
         August jeden Jahres unzulässig ist,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Errichtung anzuzeigen.
        Die Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        
    
    Im Übrigen bleiben Wildfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern im gesamten Gebiet und die Anlage von Kirrungen in den geschützten Biotopen unzulässig;
    
4.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
    
5.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, die den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
    
6.  der Betrieb von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
     Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
    
7.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch jeweils vom 1.
    August eines jeden Jahres bis zum 28.
    Februar des Folgejahres;
    
8.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
    
9.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
    
10.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der unteren Naturschutzbehörde gebilligt oder angeordnet worden sind;
    
11.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl.
    S.
    1734) an Straßen und Wegen freigestellt;
    
12.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
 Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

#### § 6  
Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  ehemals militärisch genutzte bauliche Anlagen und Wege sollen zurückgebaut und geeignete Teile dieser Anlagen baulich gesichert und als Winterquartier für Fledermäuse eingerichtet werden;
    
2.  die Offenflächen der Gemarkung Schönhagen, Flur 1, Flurstücke 49 bis 52 sollen durch einschürige Mahd gepflegt werden.
    

#### § 7  
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu 50 000 (in Worten: fünfzigtausend) Euro geahndet werden.

#### § 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere §§ 31 bis 33 und § 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

#### § 10  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11   
Inkrafttreten

§ 5 Absatz 1 Nummer 1 Buchstabe a und b tritt am 1.
Juli 2011 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 18.
November 2010

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

 

**Anlage 1  
**(zu § 2 Absatz 1)

![Das rund 80 Hektar große Naturschutzgebiet "Saugberge" liegt in der Gemeinde Pritzwalk im Landkreis Prignitz. Es umfasst Teile der Gemarkungen Schönhagen (Pritzwalk) und Steffenshagen und liegt in Nähe der Ortslagen Schönhagen und Steffenshagen.](/br2/sixcms/media.php/69/Saugberge.gif "Das rund 80 Hektar große Naturschutzgebiet "Saugberge" liegt in der Gemeinde Pritzwalk im Landkreis Prignitz. Es umfasst Teile der Gemarkungen Schönhagen (Pritzwalk) und Steffenshagen und liegt in Nähe der Ortslagen Schönhagen und Steffenshagen.")

**Anlage 2  
**(zu § 2 Absatz 1)

Flurstücksliste zur Verordnung über das Naturschutzgebiet „Saugberge“

Landkreis: Prignitz

Gemeinde

Gemarkung

Flur

Flurstück

Geteilt[\*](#*)

Pritzwalk

Schönhagen (P)

1

19

Ja

Pritzwalk

Schönhagen (P)

1

21/1

Nein

Pritzwalk

Schönhagen (P)

1

23/1

Ja

Pritzwalk

Schönhagen (P)

1

24/1

Ja

Pritzwalk

Schönhagen (P)

1

25/1

Ja

Pritzwalk

Schönhagen (P)

1

26/1

Ja

Pritzwalk

Schönhagen (P)

1

27/1

Nein

Pritzwalk

Schönhagen (P)

1

27/2

Ja

Pritzwalk

Schönhagen (P)

1

29/1

Nein

Pritzwalk

Schönhagen (P)

1

29/2

Ja

Pritzwalk

Schönhagen (P)

1

30

Ja

Pritzwalk

Schönhagen (P)

1

31

Ja

Pritzwalk

Schönhagen (P)

1

32/1

Ja

Pritzwalk

Schönhagen (P)

1

32/2

Ja

Pritzwalk

Schönhagen (P)

1

33

Nein

Pritzwalk

Schönhagen (P)

1

34

Nein

Pritzwalk

Schönhagen (P)

1

35

Nein

Pritzwalk

Schönhagen (P)

1

36

Nein

Pritzwalk

Schönhagen (P)

1

37

Nein

Pritzwalk

Schönhagen (P)

1

38

Nein

Pritzwalk

Schönhagen (P)

1

39

Nein

Pritzwalk

Schönhagen (P)

1

40

Nein

Pritzwalk

Schönhagen (P)

1

41

Nein

Pritzwalk

Schönhagen (P)

1

42/1

Nein

Pritzwalk

Schönhagen (P)

1

43

Nein

Pritzwalk

Schönhagen (P)

1

44

Nein

Pritzwalk

Schönhagen (P)

1

45

Nein

Pritzwalk

Schönhagen (P)

1

46

Nein

Pritzwalk

Schönhagen (P)

1

47

Nein

Pritzwalk

Schönhagen (P)

1

48

Nein

Pritzwalk

Schönhagen (P)

1

49

Nein

Pritzwalk

Schönhagen (P)

1

50

Nein

Pritzwalk

Schönhagen (P)

1

51

Nein

Pritzwalk

Schönhagen (P)

1

52

Nein

Pritzwalk

Schönhagen (P)

1

53

Nein

Pritzwalk

Schönhagen (P)

1

54

Nein

Pritzwalk

Schönhagen (P)

1

55

Nein

Pritzwalk

Schönhagen (P)

1

56

Nein

Pritzwalk

Schönhagen (P)

1

57

Ja

Pritzwalk

Schönhagen (P)

1

59/1

Ja

Pritzwalk

Schönhagen (P)

1

59/2

Ja

Pritzwalk

Schönhagen (P)

1

60

Nein

Pritzwalk

Steffenshagen

2

53/2

Ja

Pritzwalk

Steffenshagen

2

53/4

Ja

\* Flurstück liegt vollständig im NSG = Nein / Flurstück liegt teilweise im NSG = Ja