## Staatsvertrag zwischen den Ländern Berlin, Brandenburg und Sachsen-Anhalt über die Übertragung der Zuständigkeit in Staatsschutz-Strafsachen

Die Länder Brandenburg und Sachsen-Anhalt schließen mit dem Land Berlin den nachstehenden Staatsvertrag:

#### Artikel 1  
Übertragung der Zuständigkeit

Die Länder Brandenburg und Sachsen-Anhalt übertragen für ihr Gebiet die in § 120 Absatz 1 bis 4 des Gerichtsverfassungsgesetzes den Oberlandesgerichten zugewiesenen Aufgaben dem Kammergericht.

#### Artikel 2  
Kostenerstattung

Soweit das Land Berlin in Strafsachen, für die das Kammergericht auf Grund von Artikel 1 zuständig ist, Verfahrenskosten und Auslagen von Verfahrensbeteiligten zu tragen oder Entschädigungen zu leisten hat, kann es, soweit nicht der Bund zur Erstattung verpflichtet ist, von dem Land Erstattung verlangen, das ohne die Regelung des Artikel 1 zuständig wäre.
Soweit im Falle der Kostenpflicht der oder des Verurteilten von der Strafvollstreckungsbehörde eingezogene Kosten nicht der Bundeskasse verbleiben, stehen sie der Landeskasse des jeweils erstattungspflichtigen Landes zu.
Die Einzelheiten der Kostenerstattung für diese Verfahren werden in einer Verwaltungsvereinbarung geregelt.

#### Artikel 3  
Übergangsregelung

Ist zum Zeitpunkt des Inkrafttretens dieses Staatsvertrages die öffentliche Klage beim Brandenburgischen Oberlandesgericht oder beim Oberlandesgericht Naumburg erhoben, geht die Strafsache auf das Kammergericht über.
Hat die Hauptverhandlung bereits begonnen, verbleibt es bei der bisherigen Zuständigkeit.

#### Artikel 4  
Kündigung

Dieser Staatsvertrag kann von dem Land Brandenburg oder dem Land Sachsen-Anhalt durch schriftliche Erklärung gegenüber dem Land Berlin und von dem Land Berlin durch schriftliche Erklärung gegenüber dem Land Brandenburg oder dem Land Sachsen-Anhalt mit einer Frist von sechs Monaten zum Jahresende gekündigt werden.
Bei Kündigung des Landes Berlin gegenüber nur einem der beiden anderen Länder oder bei Kündigung nur eines dieser Länder gegenüber dem Land Berlin gilt der Staatsvertrag zwischen dem Land Berlin und dem jeweils anderen Land fort.

#### Artikel 5  
Inkrafttreten

(1) Dieser Staatsvertrag bedarf der Ratifikation.
Die Ratifikationsurkunden werden bei der Senatskanzlei des Landes Berlin hinterlegt.

(2) Dieser Staatsvertrag tritt mit Ablauf des Kalendervierteljahres in Kraft, in dem die letzte Ratifikationsurkunde hinterlegt worden ist.

Berlin, 8.
November 2010

Für das Land Berlin  
In Vertretung des Regierenden Bürgermeisters  
Die Senatorin für Justiz

Gisela von der Aue

Für das Land Brandenburg  
Der Ministerpräsident  
vertreten durch den Minister der Justiz

Volkmar Schöneburg

Für das Land Sachsen-Anhalt  
In Vertretung des Ministerpräsidenten  
Die Ministerin der Justiz des Landes Sachsen-Anhalt

Angela Kolb

[zum Gesetz](/de/gesetze-212552)