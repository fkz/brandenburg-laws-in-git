## Dritte Verordnung über die Aufhebung von Wasserschutzgebieten

Auf Grund des § 15 Absatz 3 Satz 4 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1  
Aufhebung von Wasserschutzgebieten
----------------------------------------

(1) Folgende, auf der Grundlage des § 28 des Landeskulturgesetzes vom 14.
Mai 1970 (GBl.
I Nr. 12 S. 67), des § 28 des Wassergesetzes vom 17.
April 1963 (GBl.
I Nr. 5 S. 77) und der Verordnung über die Festlegung von Schutzgebieten für die Wasserentnahme aus dem Grund- und Oberflächenwasser zur Trinkwassergewinnung vom 11.
Juli 1974 (GBl.
I Nr. 37 S. 349) festgesetzte und nach § 46 des Wassergesetzes vom 2.
Juli 1982 (GBl.
I Nr. 26 S. 467) aufrechterhaltene Wasserschutzgebiete werden aufgehoben:

1.  die mit Beschluss K 55/75 vom 18.
    Dezember 1975 des Kreistages Finsterwalde für die Wasserwerke Kleinkrausnik und Prießen festgesetzten Wasserschutzgebiete,
2.  das mit Beschluss Nr. 25-06/76 vom 9.
    Dezember 1976 des Kreistages Lübben für das Kleinstwasserwerk Pretschen festgesetzte Wasserschutzgebiet,
3.  das mit Beschluss Nr. 7/77 vom 20.
    Januar 1977 des Kreistages Guben für das Wasserwerk Sembten Kreis Guben festgesetzte Wasserschutzgebiet,
4.  das mit Beschluss Nr. 9/79 vom 6.
    September 1979 des Kreistages Guben für das Wasserwerk Groß Drewitz festgesetzte Wasserschutzgebiet und
5.  das mit Beschluss K 82/82 vom 4.
    März 1982 des Kreistages Finsterwalde für das Wasserwerk Werenzhain festgesetzte Wasserschutzgebiet.

(2) Folgende, auf der Grundlage des § 29 des Wassergesetzes vom 2.
Juli 1982 und der Dritten Durchführungsverordnung zum Wassergesetz – Schutzgebiete und Vorbehaltsgebiete – vom 2.
Juli 1982 (GBl.
I Nr. 26 S. 487) festgesetzte Wasserschutzgebiete werden aufgehoben:

1.  das mit Beschluss über die Festlegung eines Trinkwasserschutzgebietes für das Wasserwerk Lebusa vom 4.
    November 1982 des Kreistages Herzberg festgesetzte Wasserschutzgebiet,
2.  das mit Beschluss K 70/86 vom 11.
    Dezember 1986 durch den Kreistag Finsterwalde für das Wasserwerk des VEG Obstbau Cottbus, BT Pahlsdorf festgesetzte Wasserschutzgebiet und
3.  das mit Beschuss Nr. 175/87 vom 1.
    Oktober 1987 durch den Kreistag Lübben für das Wasserwerk Glietz festgesetzte Wasserschutzgebiet.

§ 2  
Inkrafttreten
-------------------

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 17.
Juli 2012

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack