## Verordnung zur Einrichtung einer Kommission gemäß § 4 Absatz 2 und 3 des Brandenburgischen Vergabegesetzes  (Brandenburgische Vergabegesetz-Kommissionsverordnung - BbgVergGKV)

Auf Grund des § 4 Absatz 2 Satz 1 und Absatz 3 Satz 2 des Brandenburgischen Vergabegesetzes vom 21.
September 2011 (GVBl.
I Nr. 19) in Verbindung mit § 1 der Brandenburgischen Vergabegesetz-Zuständigkeitsübertragungsverordnung vom 29.
März 2012 (GVBl.
II Nr.
22) verordnet der Minister für Arbeit, Soziales, Frauen und Familie:

§ 1  
Einrichtung der Kommission
--------------------------------

(1) Das für Arbeit zuständige Ministerium richtet die in § 4 Absatz 2 des Brandenburgischen Vergabegesetzes vorgesehene Kommission unabhängiger Mitglieder ein.

(2) Das für Arbeit zuständige Ministerium führt die Geschäfte und den Vorsitz der Kommission.
Die vorsitzende Person ist nicht stimmberechtigt.

§ 2  
Vorschlag und Berufung der Mitglieder
-------------------------------------------

(1) Das für Wirtschaft zuständige Ministerium schlägt ein eigenes Mitglied, die beiden Mitglieder aus der Gruppe der Arbeitgeber sowie ein Mitglied aus der Wissenschaft einschließlich deren Stellvertretungen vor.
Die Auswahl der übrigen Mitglieder und deren Stellvertretungen sowie die Berufung aller Mitglieder der Kommission obliegen dem für Arbeit zuständigen Ministerium.
Die Spitzenorganisationen der Gewerkschaften und der Arbeitgebervereinigungen des Landes Brandenburg werden von den Vorschlagsberechtigten an der Auswahl der Mitglieder aus der Gruppe der abhängig Beschäftigten sowie der Arbeitgeber beteiligt.

(2) Zur Gewährleistung der gleichberechtigten Teilhabe von Frauen und Männern gemäß § 4 Absatz 2 Satz 3 des Brandenburgischen Vergabegesetzes sind von den Vorschlagsberechtigten jeweils mindestens zur Hälfte Frauen vorzuschlagen.
Dies gilt auch für die Stellvertretungen und Nachbesetzungen während der laufenden Amtsperiode. Im Rahmen ihrer Beteiligung nach Absatz 1 Satz 3 benennen die Spitzenorganisationen den Vorschlagsberechtigten jeweils mindestens ebenso viele Frauen wie Männer.
Die Sätze 1 bis 3 finden keine Anwendung, soweit den Spitzenorganisationen der Gewerkschaften und der Arbeitgebervereinigungen oder den Vorschlagsberechtigten die Einhaltung dieser Vorgaben aus rechtlichen oder tatsächlichen Gründen nicht möglich ist; sie haben dem für Arbeit zuständigen Ministerium diese Gründe nachvollziehbar darzulegen.

(3) Die Mitglieder der Kommission und deren Stellvertretungen werden für die Dauer von vier Jahren berufen.
Für Sitzungen der Kommission können weitere sachverständige Personen beigezogen werden; sie haben jedoch kein Stimmrecht.

(4) Die Mitglieder sind ehrenamtlich tätig.
Sie werden für Reisekosten nach Maßgabe der §§ 4 bis 7 des Bundesreisekostengesetzes sowie für nachgewiesenen Verdienstausfall entschädigt.

§ 3  
Verfahren
---------------

(1) Die Kommission ist bei Bedarf oder auf Verlangen von mindestens drei Mitgliedern von dem für Arbeit zuständigen Ministerium einzuberufen.

(2) Die Sitzungen sind nicht öffentlich.
Die Kommission ist entscheidungsfähig, wenn alle Mitglieder anwesend oder vertreten sind.
Sie entscheidet über ihren Vorschlag zur Anpassung des Entgeltsatzes durch die Mehrheit der abgegebenen Stimmen.
Der Vorschlag ist schriftlich zu begründen.
Kommt keine Mehrheit zustande, sind stattdessen die unterschiedlichen Positionen ausführlich schriftlich darzulegen.

(3) Das für Arbeit zuständige Ministerium legt im Rahmen der Geschäftsführung für die Kommission deren Vorschlag der Landesregierung zur Entscheidung gemäß § 4 Absatz 1 Satz 1 des Brandenburgischen Vergabegesetzes vor.

§ 4  
Inkrafttreten
-------------------

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 6.
Juli 2012

Der Minister für Arbeit,  
Soziales, Frauen und Familie

Günter Baaske