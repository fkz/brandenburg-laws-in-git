## Verordnung über die Erstattung der Kosten zum Brandenburgischen Zensusausführungsgesetz  (Zensuskostenverordnung - ZensV)

Auf Grund des § 11 Absatz 4 Satz 1 des Brandenburgischen Zensusausführungsgesetzes vom 22.
September 2010 (GVBl.
I Nr. 29) verordnet der Minister des Innern:

§ 1  
Abrechnende Stelle
------------------------

Zuständige Stelle für die Verwaltung und Abrechnung der Mittel, die vom Land den kreisfreien Städten und Landkreisen gemäß § 11 Absatz 1 Satz 1 des Brandenburgischen Zensusausführungsgesetzes gewährt werden, ist das Amt für Statistik Berlin-Brandenburg.

§ 2  
Bemessungsgrundlagen für die Erstattung und Erstattungspauschalen
-----------------------------------------------------------------------

(1) Die mit dem Zensusausführungsgesetz verbundenen Mehrbelastungen werden den kreisfreien Städten und Landkreisen unbeschadet der Regelungen nach § 3 Absatz 2 und 3 gemäß dem in Anlage 2 jeweils ausgewiesenen Betrag erstattet.
Bemessungsgrundlagen für die Erstattung der Mehraufwendungen an die in § 2 Absatz 1 des Brandenburgischen Zensusausführungsgesetzes genannten kommunalen Körperschaften in Verbindung mit den in Anlage 1 festgelegten örtlichen Erhebungsstellen sind:

1.  die Personalaufwendungen, die in den örtlichen Erhebungsstellen für die Erfüllung der Aufgaben nach § 5 des Brandenburgischen Zensusausführungsgesetzes entstehen, sowie die damit verbundenen allgemeinen Verwaltungskosten (Verwaltungsgemeinkosten),
2.  die Sachaufwendungen für die Einrichtungen und den Betrieb der örtlichen Erhebungsstellen und
3.  aufgabengebundene Sachaufwendungen als Aufwandsentschädigungen der ehrenamtlichen Erhebungsbeauftragten gemäß der Anzahl der im Rahmen
    1.  der postalischen Gebäude- und Wohnungszählung nach § 6 des Zensusgesetzes 2011 bearbeiteten Anschriften,
    2.  der Haushaltebefragung auf Stichprobenbasis nach § 7 Absatz 2 des Zensusgesetzes 2011 festgestellten Personen,
    3.  der Erhebungen an Sonderanschriften nach § 8 des Zensusgesetzes 2011 in nicht sensiblen Sonderbereichen festgestellten Personen,
    4.  der Erhebungen an Sonderanschriften nach § 8 des Zensusgesetzes 2011 bearbeiteten sensiblen Sonderanschriften,
    5.  der Mehrfachfalluntersuchung nach § 15 des Zensusgesetzes 2011 bearbeiteten Anschriften,
    6.  der Befragung zur Klärung von Unstimmigkeiten nach § 16 des Zensusgesetzes 2011 bearbeiteten Anschriften.

(2) Die Aufwandsentschädigungen der ehrenamtlich tätigen Erhebungsbeauftragten nach Absatz 1 Nummer 3 betragen

1.  für die in den Buchstaben a und d bis f genannten Erhebungen 15 Euro je Anschrift sowie
2.  für die in den Buchstaben b und c genannten Erhebungen 7,50 Euro je Person für erfolgreich durchgeführte Erhebungen beziehungsweise 2,50 Euro für vergebliche Kontaktbemühungen sowie für Personen, die ihrer Auskunftspflicht auf schriftlichem oder elektronischem Wege nachkommen.

§ 3  
Erstattungsverfahren
--------------------------

(1) Die Zahlung der Finanzzuweisung gemäß § 2 erfolgt in drei Teilbeträgen.
Zum 15.
Juni 2011 erfolgt eine Abschlagszahlung in Höhe von 75 Prozent der in Anlage 2 jeweils ausgewiesenen Erstattungssumme, in der die Pauschalbeträge nach § 11 Absatz 2 des Brandenburgischen Zensusausführungsgesetzes eingerechnet sind.
 Zum 30.
November 2012 erfolgt eine weitere Abschlagszahlung in Höhe des Differenzbetrages zwischen bereits geleisteten Abschlagszahlungen und der in Anlage 2 genannten Erstattungssumme insgesamt.
Die Restzahlung erfolgt nach Feststellung der Abschlussrechnung, spätestens jedoch zum 31.
 Januar 2013.

(2) Die abrechnende Stelle gemäß § 1 fertigt die Abschlussrechnung insbesondere anhand der tatsächlichen Fallzahlen gemäß § 11 Absatz 4 Satz 2 des Brandenburgischen Zensusausführungsgesetzes unter Berücksichtigung der Bemessungsgrundlagen nach § 2.
Die Abschlussrechnung wird durch das für Inneres zuständige Mitglied der Landesregierung festgestellt.
Aufwendungen, die gemäß Absatz 1 noch keinen Ausgleich gefunden haben, werden erstattet.
 Dies gilt auch für Aufwendungen, die mit der projektbedingten Verlängerung der Betriebsdauer der örtlichen Erhebungsstellen verbunden sind.
Überzahlungen sind an die abrechnende Stelle gemäß § 1 zurückzuzahlen.

(3) Notwendige Mehrbelastungen gemäß § 11 Absatz 4 Satz 3 des Brandenburgischen Zensusausführungsgesetzes, die nicht bereits nach Absatz 2 Satz 1 berücksichtigt sind, sind spätestens zur Abschlussrechnung nachzuweisen und in dieser zu berücksichtigen.

§ 4  
Inkrafttreten, Außerkrafttreten
-------------------------------------

Diese Verordnung tritt am Tag nach der Verkündung in Kraft und am 31.
Dezember 2015 außer Kraft.

Potsdam, den 2.
März 2011

Der Minister des Innern  
Dr.
Dietmar Woidke

**Anlage 1** (zu § 2 Absatz 1)

Örtliche Erhebungsstellen der kreisfreien Städte und der Landkreise

Gemeinden des dazugehörigen Erhebungsbereiches

Brandenburg an der Havel

Brandenburg an der Havel

Cottbus

Cottbus

Frankfurt (Oder)

Frankfurt (Oder)

Potsdam

Potsdam

Barnim

 

 

Bernau bei Berlin

Ahrensfelde, Bernau bei Berlin, Panketal, Wandlitz, Werneuchen

 

Eberswalde

Althüttendorf, Biesenthal, Breydin, Britz, Chorin, Eberswalde, Friedrichswalde, Hohenfinow, Joachimsthal, Liepe, Lunow-Stolzenhagen, Marienwerder, Melchow, Niederfinow, Oderberg, Parsteinsee, Rüdnitz, Schorfheide, Sydower Fließ, Ziethen

Dahme-Spreewald

 

 

Lübben (Spreewald)

Alt Zauche-Wußwerk, Bersteland, Bestensee, Byhleguhre-Byhlen, Drahnsdorf, Eichwalde, Golßen, Groß Köris, Halbe, Heidesee, Heideblick, Jamlitz, Kasel-Golzig, Königs Wusterhausen, Krausnick-Groß Wasserburg, Lieberose, Lübben (Spreewald), Luckau, Märkisch Buchholz, Märkische Heide, Mittenwalde, Münchehofe, Neu Zauche, Rietzneuendorf-Staakow, Schlepzig, Schönefeld, Schönwald, Schulzendorf, Schwerin, Schwielochsee, Spreewaldheide, Steinreich, Straupitz, Teupitz, Unterspreewald, Wildau, Zeuthen

Elbe-Elster

 

 

Finsterwalde

Bad Liebenwerda, Crinitz, Doberlug-Kirchhain, Elsterwerda, Falkenberg/Elster, Fichtwald, Finsterwalde, Gorden-Staupitz, Gröden, Großthiemig, Heideland, Herzberg (Elster), Hirschfeld, Hohenbucko, Hohenleipisch, Kremitzaue, Lebusa, Lichterfeld-Schacksdorf, Massen-Niederlausitz, Merzdorf, Mühlberg/Elbe, Plessa, Röderland, Rückersdorf, Sallgast, Schilda, Schlieben, Schönborn, Schönewalde, Schraden, Sonnewalde, Tröbitz, Uebigau-Wahrenbrück

Havelland

 

 

Nauen

Brieselang, Dallgow-Döberitz, Falkensee, Ketzin, Nauen, Schönwalde-Glien, Wustermark

 

Rathenow

Friesack, Gollenberg, Großderschau, Havelaue, Wiesenaue, Kleßen-Görne, Kotzen, Märkisch Luch, Milower Land, Mühlenberge, Nennhausen, Paulinenaue, Pessin, Premnitz, Rathenow, Retzow, Rhinow, Seeblick, Stechow-Ferchesar

Märkisch-Oderland

 

 

Seelow

Alt-Tucheband, Bad Freienwalde (Oder), Beiersdorf-Freudenberg, Bleyen-Genschmar, Bliesdorf, Buckow (Märkische Schweiz), Falkenberg, Falkenhagen (Mark), Fichtenhöhe, Garzau-Garzin, Golzow, Gusow-Platkow, Heckelberg-Brunow, Höhenland, Küstriner Vorland, Lebus, Letschin, Lietzen, Lindendorf, Märkische Höhe, Müncheberg, Neuhardenberg, Neulewin, Neutrebbin, Oberbarnim, Oderaue, Podelzig, Prötzel, Rehfelde, Reichenow-Möglin, Reitwein, Seelow, Treplin, Vierlinden, Waldsieversdorf, Wriezen, Zechin, Zeschdorf

 

Strausberg

Altlandsberg, Fredersdorf-Vogelsdorf, Hoppegarten, Neuenhagen bei Berlin, Petershagen/Eggersdorf, Rüdersdorf bei Berlin, Strausberg

Oberhavel

 

 

Oranienburg

Birkenwerder, Glienicke/Nordbahn, Kremmen, Leegebruch, Mühlenbecker Land, Oranienburg

 

Hennigsdorf

Hennigsdorf, Hohen Neuendorf, Oberkrämer, Velten

 

Gransee

Fürstenberg/Havel, Gransee, Großwoltersdorf, Liebenwalde, Löwenberger Land, Schönermark, Sonnenberg, Stechlin, Zehdenick

Oberspreewald-Lausitz

 

 

Senftenberg

Frauendorf, Großkmehlen, Grünewald, Guteborn, Hermsdorf, Hohenbocka, Kroppen, Lauchhammer, Lindenau, Ortrand, Ruhland, Schipkau, Schwarzbach, Schwarzheide, Senftenberg, Tettau

 

Lübbenau

Altdöbern, Bronkow, Calau, Großräschen, Lübbenau/Spreewald, Luckaitztal, Neu-Seeland, Neupetershain, Vetschau/Spreewald

Oder-Spree

 

 

Eisenhüttenstadt

Beeskow, Brieskow-Finkenheerd, Eisenhüttenstadt, Friedland, Groß Lindow, Grunow-Dammendorf, Lawitz, Mixdorf, Müllrose, Neißemünde, Neuzelle, Ragow-Merz, Schlaubetal, Siehdichum, Tauche, Vogelsang, Wiesenau, Ziltendorf

 

Fürstenwalde/Spree

Bad Saarow, Berkenbrück, Briesen (Mark), Diensdorf-Radlow, Erkner, Fürstenwalde/Spree, Gosen-Neu Zittau, Grünheide (Mark), Jacobsdorf, Langewahl, Madlitz-Wilmersdorf, Rauen, Reichenwalde, Rietz-Neuendorf, Schöneiche bei Berlin, Spreenhagen, Steinhöfel, Storkow (Mark), Wendisch Rietz, Woltersdorf

Ostprignitz-Ruppin

 

 

Kyritz

Breddin, Dabergotz, Dreetz, Fehrbellin, Heiligengrabe, Herzberg (Mark), Kyritz, Lindow (Mark), Märkisch Linden, Neuruppin, Neustadt (Dosse), Rheinsberg, Rüthnick, Sieversdorf-Hohenofen, Storbeck-Frankendorf, Stüdenitz-Schönermark, Temnitzquell, Temnitztal, Vielitzsee, Walsleben, Wittstock/Dosse, Wusterhausen/Dosse, Zernitz-Lohm

Potsdam-Mittelmark

 

 

Bad Belzig

Beelitz, Bad Belzig, Bensdorf, Borkheide, Borkwalde, Brück, Buckautal, Golzow, Görzke, Gräben, Linthe, Mühlenfließ, Niemegk, Planebruch, Planetal, Rabenstein/Fläming, Rosenau, Treuenbrietzen, Wenzlow, Wiesenburg/Mark, Wollin, Wusterwitz, Ziesar

 

Teltow

Kleinmachnow, Michendorf, Nuthetal, Stahnsdorf, Teltow

 

Werder (Havel)

Beetzsee, Beetzseeheide, Groß Kreutz (Havel), Havelsee, Kloster Lehnin, Päwesin, Roskow, Schwielowsee, Seddiner See, Werder (Havel)

Prignitz

 

 

Perleberg

Bad Wilsnack, Berge, Breese, Cumlosen, Gerdshagen, Groß Pankow (Prignitz), Gülitz-Reetz, Gumtow, Halenbeck-Rohlsdorf, Karstädt, Kümmernitztal, Lanz, Legde/Quitzöbel, Lenzen (Elbe), Lenzerwische, Marienfließ, Meyenburg, Perleberg, Pirow, Plattenburg, Pritzwalk, Putlitz, Rühstädt, Triglitz, Weisen, Wittenberge

Spree-Neiße

 

 

Forst (Lausitz)

Briesen, Burg (Spreewald), Dissen-Striesow, Drachhausen, Drehnow, Forst (Lausitz), Guben, Guhrow, Heinersbrück, Jänschwalde, Peitz, Schenkendöbern, Schmogrow-Fehrow, Tauer, Teichland, Turnow-Preilack, Werben

 

Sellessen (Spremberg)

Döbern, Drebkau, Felixsee, Groß Schacksdorf-Simmersdorf, Hornow-Wadelsdorf, Jämlitz-Klein Düben, Kolkwitz, Neiße-Malxetal, Neuhausen/Spree, Spremberg, Tschernitz, Welzow, Wiesengrund

Teltow-Fläming

 

 

Luckenwalde

Am Mellensee, Baruth/Mark, Dahme/Mark, Dahmetal, Ihlow, Jüterbog, Luckenwalde, Niedergörsdorf, Niederer Fläming, Nuthe-Urstromtal, Zossen

 

Ludwigsfelde

Blankenfelde-Mahlow, Großbeeren, Ludwigsfelde, Rangsdorf, Trebbin

Uckermark

 

 

Prenzlau

Boitzenburger Land, Brüssow, Carmzow-Wallmow, Flieth-Stegelitz, Gerswalde, Göritz, Gramzow, Grünow, Lychen, Milmersdorf, Mittenwalde, Nordwestuckermark, Oberuckersee, Prenzlau, Randowtal, Schenkenberg, Schönfeld, Temmen-Ringenwalde, Templin, Ucker-felde, Uckerland, Zichow

 

Schwedt/Oder

Angermünde, Berkholz-Meyenburg, Casekow, Gartz (Oder), Hohenselchow-Groß Pinnow, Mark Landin, Mescherin, Pinnow, Schöneberg, Schwedt/Oder, Tantow, Passow

**Anlage 2  
**(zu § 2 Absatz 1 und § 3 Absatz 1)

Örtliche Erhebungsstelle

Erstattungssumme insgesamt  
(vorbehaltlich Schlussrechnung nach § 3 Absatz 2 und 3)

Erstattungssumme abzgl.
bereits gewährter Zahlungen nach § 11 Absatz 2 ZensusAGBbg

Auszahlungsbetrag am 15.
Juni 2011

 

in Euro

in Euro

in Euro

Brandenburg an der Havel

275 426

198 026

148 520

Cottbus

362 109

284 709

213 532

Frankfurt (Oder)

321 019

243 619

182 714

Potsdam

417 973

340 573

255 430

Barnim

 

 

 

 

Bernau bei Berlin

438 064

360 664

270 498

 

Eberswalde

342 962

265 562

199 172

Dahme-Spreewald

 

 

 

 

Lübben (Spreewald)

582 108

504 708

378 531

Elbe-Elster

 

 

 

 

Finsterwalde

454 540

377 140

282 855

Havelland

 

 

 

 

Rathenow

276 117

198 717

149 038

 

Nauen

407 736

330 336

247 752

Märkisch-Oderland

 

 

 

 

Strausberg

481 719

404 319

303 239

 

Seelow

308 547

231 147

173 360

Oberhavel

 

 

 

 

Oranienburg

391 138

313 738

235 303

 

Hennigsdorf

394 489

317 089

237 817

 

Gransee

262 028

184 628

138 471

Oberspreewald-Lausitz

 

 

 

 

Senftenberg

346 144

268 744

201 558

 

Lübbenau

327 377

249 977

187 483

Oder-Spree

 

 

 

 

Fürstenwalde/Spree

448 818

371 418

278 564

 

Eisenhüttenstadt

291 022

213 622

160 216

Ostprignitz-Ruppin

 

 

 

 

Kyritz

403 666

326 266

244 699

Potsdam-Mittelmark

 

 

 

 

Teltow

399 412

322 012

241 509

 

Werder (Havel)

352 477

275 077

206 308

 

Bad Belzig

343 713

266 313

199 735

Prignitz

 

 

 

 

Perleberg

406 709

329 309

246 982

Spree-Neiße

 

 

 

 

Forst (Lausitz)

339 506

262 106

196 580

 

Sellessen (Spremberg)

279 614

202 214

151 660

Teltow-Fläming

 

 

 

 

Ludwigsfelde

383 763

306 363

229 772

 

Luckenwalde

405 276

327 876

245 907

Uckermark

 

 

 

 

Schwedt/Oder

329 004

251 604

188 703

 

Prenzlau

353 080

275 680

206 760

**Insgesamt**

**11 125 557**

**8 803 557**

**6 602 668**