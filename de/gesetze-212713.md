## Gesetz zu dem Staatsvertrag über die Einrichtung einer Gemeinsamen elektronischen Überwachungsstelle der Länder

Der Landtag hat das folgende Gesetz beschlossen:

### § 1 

Dem Beitritt des Landes Brandenburg zum Staatsvertrag der Länder Baden-Württemberg, Freistaat Bayern, Hessen und Nordrhein-Westfalen über die Einrichtung einer Gemeinsamen elektronischen Überwachungsstelle der Länder wird zugestimmt.
Der Vertrag wird nachstehend veröffentlicht.

### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Vertrag nach seinem Artikel 9 Absatz 2 Satz 2 für das Land Brandenburg in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 29.
November 2012

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Staatsvertrag](/de/vertraege-237644)