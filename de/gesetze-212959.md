## Gesetz über den Vollzug des Jugendarrestes im Land Brandenburg (Brandenburgisches Jugendarrestvollzugsgesetz - BbgJAVollzG)

Inhaltsübersicht
----------------

Abschnitt 1  
Allgemeine Bestimmungen
-------------------------------------

[§ 1 Anwendungsbereich](#1)  
[§ 2 Ziel des Arrestes](#2)  
[§ 3 Stellung der Arrestierten, Mitwirkung](#3)  
[§ 4 Grundsätze der Arrestgestaltung](#4)  
[§ 5 Grundsätze der Förderung](#5)  
[§ 6 Zusammenarbeit](#6)  
[§ 7 Soziale Hilfe](#7)

Abschnitt 2  
Aufnahmeverfahren, Planung und Gestaltung des Arrestes
--------------------------------------------------------------------

[§ 8 Aufnahmeverfahren](#8)  
[§ 9 Arrestkonzeption](#9)  
[§ 10 Ermittlung des Förderbedarfs, Förderplan](#10)  
[§ 11 Freizeit- und Kurzarrest](#11)  
[§ 12 Nichtbefolgungsarrest](#12)  
[§ 13 Jugendarrest neben Jugendstrafe](#13)  
[§ 14 Aufenthalte außerhalb der Anstalt](#14)

Abschnitt 3  
Unterbringung, Versorgung und Freizeit
----------------------------------------------------

[§ 15 Unterbringung, Aufenthalt während der Aufschlusszeiten](#15)  
[§ 16 Gewahrsam an Gegenständen](#16)  
[§ 17 Kleidung](#17)  
[§ 18 Verpflegung und Einkauf](#18)  
[§ 19 Freizeit, Sport](#19)

Abschnitt 4  
Gesundheitsfürsorge
---------------------------------

[§ 20 Gesundheitsschutz und Hygiene](#20)  
[§ 21 Zwangsmaßnahmen auf dem Gebiet der Gesundheitsfürsorge](#21)

Abschnitt 5  
Verkehr mit Personen außerhalb der Anstalt
--------------------------------------------------------

[§ 22 Besuche, Telefongespräche, Schriftwechsel und Pakete](#22)

Abschnitt 6  
Religionsausübung
-------------------------------

[§ 23 Seelsorge, religiöse Veranstaltungen, Weltanschauungsgemeinschaften](#23)

Abschnitt 7  
Sicherheit und Ordnung
------------------------------------

[§ 24 Grundsatz](#24)  
[§ 25 Allgemeine Verhaltenspflichten](#25)  
[§ 26 Durchsuchung, Absuchung](#26)  
[§ 27 Besondere Sicherungsmaßnahmen](#27)

Abschnitt 8  
Unmittelbarer Zwang
---------------------------------

[§ 28 Begriffsbestimmungen, allgemeine Voraussetzungen](#28)  
[§ 29 Grundsatz der Verhältnismäßigkeit](#29)  
[§ 30 Androhung](#30)

Abschnitt 9  
Reaktionen auf Pflichtverstöße, einvernehmliche Streitbeilegung
-----------------------------------------------------------------------------

[§ 31 Reaktionen auf Pflichtverstöße](#31)  
[§ 32 Einvernehmliche Streitbeilegung](#32)

Abschnitt 10  
Entlassung, Nachsorge
------------------------------------

[§ 33 Einleitung nachsorgender Maßnahmen, Entlassungsbeihilfe](#33)  
[§ 34 Schlussbericht, Entlassungsgespräch](#34)  
[§ 35 Verbleib oder Aufnahme auf freiwilliger Grundlage](#35)  
[§ 36 Nachgehende Betreuung](#36)

Abschnitt 11  
Beschwerde
-------------------------

[§ 37 Beschwerderecht](#37)

Abschnitt 12  
Kriminologische Forschung
----------------------------------------

[§ 38 Kriminologische Forschung, Berichtspflicht](#38)

Abschnitt 13  
Aufbau und Organisation der Anstalt
--------------------------------------------------

[§ 39 Einrichtung und Ausstattung der Anstalt](#39)  
[§ 40 Festsetzung der Belegungsfähigkeit, Einzelbelegung](#40)  
[§ 41 Leitung der Anstalt und des Arrestes](#41)  
[§ 42 Bedienstete, ärztliche und seelsorgerische Versorgung](#42)  
[§ 43 Hausordnung](#43)

Abschnitt 14  
Aufsicht, Beirat
-------------------------------

[§ 44 Aufsichtsbehörde, Vollstreckungsplan, Vollzugsgemeinschaften](#44)  
[§ 45 Beirat](#45)

Abschnitt 15  
Datenschutz
--------------------------

[§ 46 Entsprechende Anwendung des Brandenburgischen Justizvollzugsgesetzes](#46)

Abschnitt 16  
Schlussbestimmung
--------------------------------

[§ 47 Einschränkung von Grundrechten](#47)

Abschnitt 1  
Allgemeine Bestimmungen
-------------------------------------

### § 1   
Anwendungsbereich

Dieses Gesetz regelt den Vollzug des Jugendarrestes (Arrest) in einer Jugendarrestanstalt (Anstalt).

### § 2   
Ziel des Arrestes

Der Arrest dient dem Ziel, den Arrestierten das von ihnen begangene Unrecht und ihre Verantwortung hierfür bewusst zu machen und ihnen Hilfen für eine Lebensführung ohne Straftaten aufzuzeigen und zu vermitteln.

### § 3   
Stellung der Arrestierten, Mitwirkung

(1) Die Persönlichkeit der Arrestierten ist zu achten.
Ihnen ist zugewandt, verlässlich und konsequent zu begegnen.
Maßnahmen sind ihnen regelmäßig zu erläutern.

(2) Die Arrestierten unterliegen den in diesem Gesetz vorgesehenen Beschränkungen ihrer Freiheit.
Soweit das Gesetz eine besondere Regelung nicht enthält, dürfen ihnen nur Beschränkungen auferlegt werden, die zur Aufrechterhaltung der Sicherheit oder zur Abwendung einer schwerwiegenden Störung der Ordnung der Anstalt unerlässlich sind.
Sie müssen in einem angemessenen Verhältnis zum Zweck der Anordnung stehen und dürfen die Arrestierten nicht mehr und nicht länger als notwendig beeinträchtigen.

(3) Die Arrestierten wirken an der Erreichung des Arrestziels mit.
Ihre Bereitschaft hierzu ist zu wecken und zu fördern.

### § 4   
Grundsätze der Arrestgestaltung

(1) Der Arrest ist auf die Auseinandersetzung der Arrestierten mit ihren Straftaten, deren Ursachen und deren Folgen auszurichten.
Das Bewusstsein für die den Opfern zugefügten Schäden soll geweckt werden.

(2) Der Arrest ist auf die Förderung der Arrestierten auszurichten und wird sozialpädagogisch ausgestaltet.

(3) Die Maßnahmen des Arrestes werden in landesweit bestehende Hilfesysteme eingebunden.

(4) Schädlichen Folgen des Freiheitsentzugs ist entgegenzuwirken.

(5) Die unterschiedlichen Bedürfnisse der Arrestierten, insbesondere im Hinblick auf Alter, Geschlecht, Herkunft, Religion, Behinderung und sexuelle Identität werden bei der Arrestgestaltung berücksichtigt.

### § 5   
Grundsätze der Förderung

(1) Die Förderung erfolgt durch Maßnahmen und Programme zur Entwicklung und Stärkung der Fähigkeiten und Fertigkeiten der Arrestierten im Hinblick auf ein künftiges Leben ohne Straftaten.

(2) Die Maßnahmen und Programme sind auf die Ausgestaltung des Dauerarrestes ausgerichtet.
Ihre Durchführung kann auch Externen, insbesondere freien Trägern, übertragen werden.

(3) Einzel- und Gruppenmaßnahmen richten sich insbesondere auf die Auseinandersetzung mit den Straftaten, deren Ursachen und Folgen, die Unterstützung der schulischen und beruflichen Qualifizierung, die verantwortliche Gestaltung des alltäglichen Zusammenlebens und der freien Zeit sowie die Vermittlung unterstützender Außenkontakte.
Hierzu hat die Anstalt Angebote vorzuhalten.

(4) Zur Strukturierung ihres Tages erhalten die Arrestierten einen Tagesplan, der alle für sie vorgesehenen Maßnahmen beinhaltet.

(5) Die Personensorgeberechtigten und die Eltern von volljährigen Arrestierten mit deren Einverständnis sind in die Planung und Gestaltung des Arrestes einzubeziehen, soweit dies möglich ist und dem Arrestziel nicht zuwiderläuft.

### § 6   
Zusammenarbeit

Die Anstalt arbeitet eng mit externen Einrichtungen und Organisationen sowie Personen und Vereinen zusammen, um das Ziel des Arrestes zu erreichen und eine Weiterführung der für erforderlich erachteten Maßnahmen nach der Entlassung sicherzustellen.
Dies gilt insbesondere in Bezug auf die Jugendämter und die Sozialen Dienste der Justiz sowie für freie regionale Träger.

### § 7   
Soziale Hilfe

(1) Die Arrestierten werden darin unterstützt, ihre persönlichen, wirtschaftlichen und sozialen Schwierigkeiten zu beheben.
Sie sollen dazu angeregt und in die Lage versetzt werden, ihre Angelegenheiten selbst zu regeln.
 Die zuständige Jugendgerichtshelferin oder der zuständige Jugendgerichtshelfer hält auch während des Dauerarrestes Kontakt zu den Arrestierten und beteiligt sich an der Vermittlung von weiterführenden Hilfen.

(2) Die Beratung der Arrestierten soll auch die Vermittlung des Kontaktes zu Stellen und Einrichtungen außerhalb der Anstalt umfassen, die die Arrestierten am Wohnort begleiten und fördern können.
Mit dem Einverständnis der Arrestierten stellt die Anstalt den Kontakt zu freien Trägern her, die sie nach der Entlassung an ihrem Wohnort sofort unterstützen können.

Abschnitt 2   
Aufnahmeverfahren, Planung und Gestaltung des Arrestes
---------------------------------------------------------------------

### § 8   
Aufnahmeverfahren

(1) Mit den Arrestierten wird unverzüglich nach der Aufnahme ein Zugangsgespräch geführt, in dem der aktuelle Hilfebedarf festgestellt wird und sie insbesondere über ihre Rechte und Pflichten im Arrest in einer für sie verständlichen Form unterrichtet werden.
Den Arrestierten wird ein Exemplar der Hausordnung ausgehändigt.
Die wesentlichen Ergebnisse des Zugangsgesprächs werden dokumentiert.

(2) Während des Aufnahmeverfahrens dürfen andere Arrestierte nicht zugegen sein.

(3) Die Personensorgeberechtigten, das Jugendamt und im Falle einer Bewährungsunterstellung die Bewährungshilfe werden von der Aufnahme der Arrestierten unverzüglich benachrichtigt.
Dies gilt auch für die Vollstreckungsleiterin oder den Vollstreckungsleiter, wenn der Leiterin oder dem Leiter der Anstalt diese Aufgabe nicht obliegt.

(4) Die Arrestierten werden nach der Aufnahme alsbald ärztlich untersucht.
Im Freizeit- und Kurzarrest soll eine ärztliche Untersuchung nur erfolgen, wenn Anhaltspunkte für eine Arrestuntauglichkeit der Arrestierten bestehen.

### § 9   
Arrestkonzeption

Die Arrestleiterin oder der Arrestleiter (§ 41 Absatz 2) erstellt Konzeptionen für die Durchführung der verschiedenen Arrestarten des Jugendgerichtsgesetzes und gestaltet den Arrest entsprechend.
Die Aufsichtsbehörde kann sich die Genehmigung der Konzeptionen vorbehalten.

### § 10   
Ermittlung des Förderbedarfs, Förderplan

(1) Nach der Aufnahme führt die Arrestleiterin oder der Arrestleiter mit den Arrestierten alsbald ein ausführliches Gespräch, in dem deren aktuelle Lebenssituation erörtert und weiterer Hilfebedarf festgestellt wird.
 Erforderlichenfalls können mit Zustimmung der Arrestierten oder der Personensorgeberechtigten externe Fachkräfte hinzugezogen werden.

(2) Auf Grundlage der Ergebnisse dieses Gesprächs und der Erkenntnisse aus den Vollstreckungsunterlagen und der Jugendgerichtshilfe, wird unter Einbeziehung der Arrestierten im Vollzug des Dauerarrestes ein Förderplan erstellt.
Er zeigt den Arrestierten die zur Erreichung des Arrestziels erforderlichen Maßnahmen auf und enthält weitere Hilfsangebote und Empfehlungen.
 Vorstellungen der Arrestierten, die dem Arrestziel entsprechen, sollen berücksichtigt werden.

(3) Der Förderplan und seine Fortschreibungen enthalten insbesondere folgende Angaben:

1.  Wesentliche Erkenntnisse der Anstalt zum Förderbedarf,
    
2.  Teilnahme an Trainingsmaßnahmen zur Verbesserung der sozialen Kompetenz, namentlich zu den Schwerpunkten Gewalt, Sucht und Schulden,
    
3.  Teilnahme an Maßnahmen zur beruflichen und schulischen Förderung,
    
4.  Teilnahme an Sportangeboten und Maßnahmen zur strukturierten Gestaltung der Freizeit,
    
5.  Förderung von Außenkontakten,
    
6.  Aufenthalte außerhalb der Anstalt,
    
7.  Vermittlung in nachsorgende und weiterführende Maßnahmen und Hilfen, nachgehende Betreuung, Anregung von Auflagen und Weisungen,
    
8.  Vermittlung einer intensiven fachlichen Einzelbetreuung,
    
9.  Maßnahmen zur Erfüllung von Auflagen und zur Befolgung von Weisungen.
    

(4) Der Förderplan wird wöchentlich fortgeschrieben.
Hierzu führt die Arrestleiterin oder der Arrestleiter eine Konferenz mit den in der Anstalt an der Förderung maßgeblich Beteiligten durch.
Der Förderplan wird den Arrestierten in der Konferenz erläutert und alsbald ausgehändigt.
Er wird den Personensorgeberechtigten auf Verlangen übersandt.

### § 11   
Freizeit- und Kurzarrest

Im Freizeit- und Kurzarrest sind den Arrestierten zur Erreichung des Arrestziels in intensiven Gesprächen insbesondere ihre Straftaten und ihre gegenwärtige Lebenssituation bewusst zu machen.
Ausgehend von den Erkenntnissen des Aufnahmegesprächs werden die Arrestierten über externe Hilfsangebote unterrichtet.
Sie erhalten Gelegenheit, an den Maßnahmen nach § 5 teilzunehmen.

### § 12   
Nichtbefolgungsarrest

(1) Für den Nichtbefolgungsarrest gilt je nach dessen Dauer § 5 oder § 11 entsprechend.

(2) Im Nichtbefolgungsarrest sollen die Arrestierten auch dazu angehalten und motiviert werden, die ihnen auferlegten Weisungen zu befolgen und ihre Auflagen zu erfüllen.

### § 13   
Jugendarrest neben Jugendstrafe

(1) Die zuständige Bewährungshelferin oder der zuständige Bewährungshelfer hält während des Arrestes Kontakt zu den Arrestierten und beteiligt sich an der Planung und Einleitung nachsorgender Hilfen.

(2) In den Fällen des § 16a Absatz 1 Nummer 2 des Jugendgerichtsgesetzes gestattet die Arrestleiterin oder der Arrestleiter Kontakte der Arrestierten zu Personen des sozialen Umfeldes nur dann, wenn schädliche Einflüsse nicht zu befürchten sind.

(3) In den Fällen des § 16a Absatz 1 Nummer 3 des Jugendgerichtsgesetzes erfolgt eine auf die individuelle Problematik besonders zugeschnittene pädagogische Einwirkung auf die Arrestierten.

### § 14   
Aufenthalte außerhalb der Anstalt

(1) Aufenthalte außerhalb der Anstalt können den Arrestierten zur Erreichung des Arrestziels, insbesondere zur Durchführung von Maßnahmen der Förderung, zur Teilnahme an externen Hilfs- und Beratungsangeboten sowie zur Erfüllung der ihnen auferlegten Weisungen und Auflagen, gewährt werden, namentlich

1.  das Verlassen der Anstalt für mehrere Stunden in Begleitung einer von der Anstalt zugelassenen Person oder ohne Begleitung,
    
2.  der Aufenthalt in Einrichtungen freier Träger.
    

Vor Gewährung eines Aufenthaltes außerhalb der Anstalt nach Satz 1 Nummer 2 wird die Jugendrichterin oder der Jugendrichter gehört, die oder der die Arrestierten verurteilt hat.

(2) Aufenthalte außerhalb der Anstalt können auch aus wichtigem Anlass gewährt werden.
Wichtige Anlässe sind insbesondere die Teilnahme an gerichtlichen Terminen, die medizinische Behandlung der Arrestierten sowie eine schwere Erkrankung naher Angehöriger.

(3) Aufenthalte außerhalb der Anstalt dürfen nicht gewährt werden, wenn zu erwarten ist, dass die Arrestierten sich dem Arrest entziehen oder sie zu Straftaten missbrauchen werden.

(4) Durch Aufenthalte außerhalb der Anstalt wird die Vollstreckung des Jugendarrestes nicht unterbrochen.

(5) Für Aufenthalte außerhalb der Anstalt können die nach den Umständen des Einzelfalles erforderlichen Weisungen erteilt werden.

(6) Liegen die Aufenthalte außerhalb der Anstalt ausschließlich im Interesse der Arrestierten, können ihnen die Kosten auferlegt werden.
Sind sie nicht in der Lage, die Kosten zu tragen, kann die Anstalt diese in begründeten Fällen in angemessenem Umfang übernehmen.

Abschnitt 3   
Unterbringung, Versorgung und Freizeit
-----------------------------------------------------

### § 15   
Unterbringung, Aufenthalt während der Aufschlusszeiten

(1) Die Arrestierten werden in Arresträumen einzeln untergebracht.

(2) Weibliche und männliche Arrestierte werden getrennt voneinander untergebracht.

(3) Während der Aufschlusszeiten halten sich die Arrestierten grundsätzlich in Gemeinschaft auf.
Der gemeinschaftliche Aufenthalt kann eingeschränkt werden, wenn

1.  es die Sicherheit oder Ordnung der Anstalt erfordert,
    
2.  ein schädlicher Einfluss auf andere Arrestierte zu befürchten ist,
    
3.  es aus pädagogischen Gründen dringend geboten ist.
    

### § 16   
Gewahrsam an Gegenständen

Die Arrestierten dürfen Gegenstände nur mit Zustimmung der Anstalt einbringen oder in Gewahrsam haben.
Die Anstalt kann die Zustimmung verweigern, wenn die Gegenstände geeignet sind, die Sicherheit oder Ordnung der Anstalt oder die Erreichung des Arrestziels zu gefährden.

### § 17   
Kleidung

(1) Die Arrestierten tragen eigene Kleidung.
Soweit es zur Gewährleistung der Sicherheit oder Ordnung der Anstalt unerlässlich ist, kann dieses Recht eingeschränkt werden.

(2) Bei Bedarf stellt die Anstalt den Arrestierten Kleidung zur Verfügung.
 Diese haben für die Reinigung ihrer Kleidung selbst zu sorgen.
Die Einzelheiten regelt die Arrestleiterin oder der Arrestleiter.

### § 18   
Verpflegung und Einkauf

(1) Zusammensetzung und Nährwert der Verpflegung entsprechen den Anforderungen an eine gesunde Ernährung junger Menschen und werden ärztlich überwacht.
Auf ärztliche Anordnung wird besondere Verpflegung gewährt.
Den Arrestierten ist zu ermöglichen, Speisevorschriften ihrer Religionsgemeinschaft zu befolgen oder sich vegetarisch zu ernähren.

(2) Die Arrestierten können aus einem von der Anstalt vermittelten Angebot, das auf ihre Wünsche und Bedürfnisse Rücksicht nimmt, einkaufen.

### § 19   
Freizeit, Sport

(1) Zur Ausgestaltung der Freizeit hat die Anstalt insbesondere Angebote zur sportlichen und kulturellen Betätigung und Bildungsangebote vorzuhalten.
Sie stellt eine angemessen ausgestattete Mediathek sowie Zeitungen und Zeitschriften zur Verfügung.

(2) Dem Sport kommt bei der Gestaltung des Arrestes besondere Bedeutung zu.
 Die Anstalt bietet täglich Maßnahmen oder andere Möglichkeiten zur sportlichen Betätigung an.
Sie fördert die Bereitschaft der Arrestierten, sich sportlich zu betätigen.

(3) Der Hörfunkempfang ist in den Arresträumen gestattet.
Der Empfang von Fernsehprogrammen und die Nutzung von Geräten der Informations- und Unterhaltungselektronik sind nur in den hierfür vorgesehenen Gemeinschaftsräumen zulässig.

Abschnitt 4  
Gesundheitsfürsorge
---------------------------------

### § 20   
Gesundheitsschutz und Hygiene

(1) Die Anstalt unterstützt die Arrestierten bei der Erhaltung ihrer körperlichen, geistigen und seelischen Gesundheit.
Sie fördert das Bewusstsein für gesunde Ernährung und Lebensführung.
Die Arrestierten haben die notwendigen Anordnungen zum Gesundheitsschutz und zur Hygiene zu befolgen.

(2) Den Arrestierten wird ermöglicht, sich täglich mindestens eine Stunde im Freien aufzuhalten.

(3) Arrestierte, die nicht krankenversichert sind, haben einen Anspruch auf notwendige, ausreichende und zweckmäßige medizinische Leistungen unter Beachtung des Grundsatzes der Wirtschaftlichkeit und unter Berücksichtigung des allgemeinen Standards der gesetzlichen Krankenversicherung und der Dauer des Arrestes.

### § 21   
Zwangsmaßnahmen auf dem Gebiet der Gesundheitsfürsorge

(1) Zum Gesundheitsschutz und zur Hygiene ist die zwangsweise körperliche Untersuchung zulässig, wenn sie nicht mit einem körperlichen Eingriff verbunden ist.

(2) Die Maßnahmen dürfen nur von der Leiterin oder dem Leiter der Anstalt auf der Grundlage einer ärztlichen Stellungnahme angeordnet werden.
Durchführung und Überwachung unterstehen ärztlicher Leitung.
Unberührt bleibt die Leistung erster Hilfe für den Fall, dass eine Ärztin oder ein Arzt nicht rechtzeitig erreichbar und mit einem Aufschub Lebensgefahr verbunden ist.

Abschnitt 5   
Verkehr mit Personen außerhalb der Anstalt
---------------------------------------------------------

### § 22   
Besuche, Telefongespräche, Schriftwechsel und Pakete

(1) Die Arrestierten dürfen Besuch empfangen und Telefongespräche führen, wenn dies dem Arrestziel nicht entgegensteht und die Sicherheit oder Ordnung der Anstalt hierdurch nicht gefährdet wird.
Sie haben das Recht, Schreiben abzusenden und zu empfangen.
Besuche, Telefongespräche und Schriftwechsel werden nicht überwacht.

(2) Besuche von Personensorgeberechtigten sowie von Verteidigerinnen oder Verteidigern, von Beiständen nach § 69 des Jugendgerichtsgesetzes, von Rechtsanwältinnen oder Rechtsanwälten und Notarinnen oder Notaren in einer die Arrestierten betreffenden Rechtssache sind zu gestatten.
Dies gilt auch für Telefongespräche mit diesen Personen.

(3) Aus Gründen der Sicherheit können Besuche davon abhängig gemacht werden, dass sich die Besucherinnen und Besucher durchsuchen oder mit technischen Hilfsmitteln absuchen lassen.
Besuche dürfen beaufsichtigt werden.
Sie dürfen abgebrochen werden, wenn die Sicherheit oder Ordnung der Anstalt gefährdet würde.
Gegenstände dürfen beim Besuch nur mit Erlaubnis übergeben werden.

(4) Die Arrestleiterin oder der Arrestleiter kann den Arrestierten in begründeten Ausnahmefällen gestatten, Pakete zu empfangen.
Die Anstalt kann die Annahme von Paketen, deren Einbringung nicht gestattet ist, ablehnen oder solche Pakete an die Absenderin oder den Absender zurücksenden.

(5) Die Arrestierten haben das Absenden und den Empfang ihrer Schreiben durch die Anstalt vermitteln zu lassen, soweit nichts anderes gestattet ist.
Ein- und ausgehende Schreiben werden in Anwesenheit der Arrestierten auf verbotene Gegenstände kontrolliert und sind unverzüglich weiterzuleiten.
Pakete sind in Gegenwart der Arrestierten zu öffnen, an die sie adressiert sind.

(6) Die Kosten der Telefongespräche und des Schriftwechsels tragen die Arrestierten.
Sind sie dazu nicht in der Lage, kann die Anstalt die Kosten in begründeten Fällen in angemessenem Umfang übernehmen.

Abschnitt 6   
Religionsausübung
--------------------------------

### § 23   
Seelsorge, religiöse Veranstaltungen, Weltanschauungsgemeinschaften

(1) Den Arrestierten darf religiöse Betreuung durch eine Seelsorgerin oder einen Seelsorger nicht versagt werden.
Auf ihren Wunsch ist ihnen zu helfen, mit einer Seelsorgerin oder einem Seelsorger ihrer Religionsgemeinschaft in Verbindung zu treten.

(2) Die Arrestierten dürfen grundlegende religiöse Schriften sowie in angemessenem Umfang Gegenstände des religiösen Gebrauchs besitzen.
Diese dürfen ihnen nur bei grobem Missbrauch entzogen werden.

(3) Die Arrestierten haben das Recht, an religiösen Veranstaltungen ihres Bekenntnisses in der Anstalt teilzunehmen.
Die Zulassung zu religiösen Veranstaltungen einer anderen Religionsgemeinschaft bedarf der Zustimmung der Seelsorgerin oder des Seelsorgers der Religionsgemeinschaft.

(4) Für Angehörige weltanschaulicher Bekenntnisse gelten die Absätze 1 bis 3 entsprechend.

Abschnitt 7   
Sicherheit und Ordnung
-------------------------------------

### § 24   
Grundsatz

(1) Sicherheit und Ordnung der Anstalt bilden die Grundlage des auf die Förderung der Arrestierten ausgerichteten Anstaltslebens und tragen dazu bei, dass in der Anstalt ein von gegenseitiger Akzeptanz geprägtes gewaltfreies Klima herrscht.

(2) Auf eine einvernehmliche Streitbeilegung ist hinzuwirken.
Das Bewusstsein der Arrestierten hierfür ist zu entwickeln und zu stärken.

(3) Die Pflichten und Beschränkungen, die den Arrestierten zur Aufrechterhaltung der Sicherheit oder Ordnung der Anstalt auferlegt werden, sind so zu wählen, dass sie in einem angemessenen Verhältnis zu ihrem Zweck stehen und die Arrestierten nicht mehr und nicht länger als notwendig beeinträchtigen.

### § 25   
Allgemeine Verhaltenspflichten

(1) Die Arrestierten sind für ein sozialverträgliches Miteinander verantwortlich.
Ihr Bewusstsein hierfür ist zu wecken und zu fördern.

(2) Die Arrestierten dürfen durch ihr Verhalten das geordnete Zusammenleben in der Anstalt nicht stören.
Sie sind verpflichtet, die Anordnungen der Bediensteten zu befolgen.

(3) Ihre Arresträume und die ihnen von der Anstalt überlassenen Sachen müssen die Arrestierten selbst in Ordnung halten und schonend behandeln.

(4) Die Arrestierten haben Umstände, die eine Gefahr für das Leben oder eine erhebliche Gefahr für die Gesundheit einer Person bedeuten, unverzüglich zu melden.

### § 26   
Durchsuchung, Absuchung

(1) Die Arrestierten, ihre Sachen und die Arresträume dürfen durchsucht und mit technischen Mitteln abgesucht werden.
Die Durchsuchung männlicher Arrestierter darf nur von Männern, die Durchsuchung weiblicher Arrestierter nur von Frauen vorgenommen werden.
Das Schamgefühl ist zu schonen.

(2) Nur bei Gefahr im Verzug ist es auf Anordnung der Leiterin oder des Leiters der Anstalt im Einzelfall zulässig, eine mit einer Entkleidung verbundene körperliche Durchsuchung Arrestierter vorzunehmen.
Sie darf bei männlichen Arrestierten nur in Gegenwart von Männern, bei weiblichen Arrestierten nur in Gegenwart von Frauen erfolgen.
Sie ist in einem geschlossenen Raum durchzuführen.
Andere Arrestierte dürfen nicht anwesend sein.
 Die Anordnung ist zu begründen.
Anordnung, Durchführung und Ergebnis der Durchsuchung sind aktenkundig zu machen.

### § 27   
Besondere Sicherungsmaßnahmen

(1) Gegen Arrestierte können besondere Sicherungsmaßnahmen angeordnet werden, wenn nach ihrem Verhalten oder aufgrund ihres seelischen Zustandes in erhöhtem Maße die Gefahr von Gewalttätigkeiten gegen Personen oder Sachen, der Selbsttötung oder der Selbstverletzung besteht.

(2) Als besondere Sicherungsmaßnahmen sind zulässig:

1.  der Entzug oder die Vorenthaltung von Gegenständen,
    
2.  die Trennung von den anderen Arrestierten (Absonderung) für bis zu 24 Stunden.
    

(3) Besondere Sicherungsmaßnahmen ordnet die Leiterin oder der Leiter der Anstalt an.
Erforderlichenfalls können auch andere Bedienstete diese Maßnahmen vorläufig anordnen; die Entscheidung der Leiterin oder des Leiters der Anstalt ist unverzüglich einzuholen.

(4) Die Entscheidung wird den Arrestierten von der Leiterin oder dem Leiter der Anstalt mündlich eröffnet und mit einer kurzen Begründung schriftlich abgefasst.
Die Anordnung ist aktenkundig zu machen.

(5) Besondere Sicherungsmaßnahmen dürfen nur so weit aufrechterhalten werden, wie es ihr Zweck erfordert.
Sie sind in angemessenen Abständen daraufhin zu überprüfen, ob und in welchem Umfang sie aufrechterhalten werden müssen.
Das Ergebnis der Überprüfungen und die Durchführung der Maßnahmen einschließlich einer Beteiligung des ärztlichen Dienstes sind aktenkundig zu machen.

(6) Während der Absonderung sind die Arrestierten in besonderem Maße zu betreuen.

Abschnitt 8   
Unmittelbarer Zwang
----------------------------------

### § 28   
Begriffsbestimmungen, allgemeine Voraussetzungen

(1) Unmittelbarer Zwang ist die Einwirkung auf Personen oder Sachen durch körperliche Gewalt.

(2) Körperliche Gewalt ist jede unmittelbare körperliche Einwirkung auf Personen oder Sachen.

(3) Bedienstete dürfen unmittelbaren Zwang anwenden, wenn sie Maßnahmen des Arrestes rechtmäßig durchführen und der damit verfolgte Zweck nicht auf andere Weise erreicht werden kann.

(4) Gegen andere Personen als Arrestierte darf unmittelbarer Zwang angewendet werden, wenn sie es unternehmen, Arrestierte zu befreien oder widerrechtlich in die Anstalt einzudringen, oder wenn sie sich unbefugt darin aufhalten.

(5) Das Recht zu unmittelbarem Zwang aufgrund anderer Regelungen bleibt unberührt.

### § 29   
Grundsatz der Verhältnismäßigkeit

(1) Unter mehreren möglichen und geeigneten Maßnahmen des unmittelbaren Zwangs sind diejenigen zu wählen, die den Einzelnen und die Allgemeinheit voraussichtlich am wenigsten beeinträchtigen.

(2) Unmittelbarer Zwang unterbleibt, wenn ein durch ihn zu erwartender Schaden erkennbar außer Verhältnis zu dem angestrebten Erfolg steht.

### § 30   
Androhung

Unmittelbarer Zwang ist vorher anzudrohen.
Die Androhung darf nur dann unterbleiben, wenn die Umstände sie nicht zulassen oder unmittelbarer Zwang sofort angewendet werden muss, um eine rechtswidrige Tat, die den Tatbestand eines Strafgesetzes erfüllt, zu verhindern oder eine gegenwärtige Gefahr abzuwenden.

Abschnitt 9   
Reaktionen auf Pflichtverstöße, einvernehmliche Streitbeilegung
------------------------------------------------------------------------------

### § 31   
Reaktionen auf Pflichtverstöße

(1) Verstöße der Arrestierten gegen Pflichten, die ihnen durch oder aufgrund dieses Gesetzes auferlegt sind, sind unverzüglich im Gespräch aufzuarbeiten.

(2) Daneben können Maßnahmen angeordnet werden, die geeignet sind, den Arrestierten ihr Fehlverhalten bewusst zu machen.
Als solche Maßnahmen kommen namentlich die Erteilung von Weisungen und Auflagen, die Beschränkung oder der Entzug einzelner Gegenstände für die Freizeitbeschäftigung und der Ausschluss von gemeinsamer Freizeit oder einzelnen Freizeitveranstaltungen in Betracht.

(3) Die Arrestleiterin oder der Arrestleiter legt fest, welche Bediensteten befugt sind, die Gespräche zu führen und die Maßnahmen anzuordnen.

(4) Es sollen solche Maßnahmen angeordnet werden, die mit der Verfehlung in Zusammenhang stehen.

### § 32   
Einvernehmliche Streitbeilegung

(1) Zur Abwendung von Maßnahmen nach § 31 Absatz 2 können in geeigneten Fällen im Wege einvernehmlicher Streitbeilegung Vereinbarungen getroffen werden.
 Insbesondere kommen die Wiedergutmachung des Schadens, die Entschuldigung beim Geschädigten, die Erbringung von Leistungen für die Gemeinschaft und das vorübergehende Verbleiben im Arrestraum in Betracht.
Die Vereinbarungen sind aktenkundig zu machen.

(2) Erfüllen die Arrestierten die Vereinbarung, so ist die Anordnung von Maßnahmen nach § 31 Absatz 2 unzulässig.

Abschnitt 10   
Entlassung, Nachsorge
-------------------------------------

### § 33   
Einleitung nachsorgender Maßnahmen, Entlassungsbeihilfe

(1) Die Anstalt unterstützt und berät die Arrestierten in enger Zusammenarbeit mit dem Jugendamt sowie freien Trägern bei der Einleitung von nachsorgenden Maßnahmen.

(2) Die Entlassung kann am Tag des Ablaufs der Arrestzeit vorzeitig, im Freizeitarrest auch schon am Abend zuvor, erfolgen, wenn die Arrestierten aus schulischen oder beruflichen Gründen hierauf angewiesen sind.

(3) Bedürftigen Arrestierten kann eine Entlassungsbeihilfe in Form eines Reisekostenzuschusses, angemessener Kleidung oder einer sonstigen notwendigen Unterstützung gewährt werden.

### § 34   
Schlussbericht, Entlassungsgespräch

(1) Die Leiterin oder der Leiter der Anstalt erstellt zum Ende des Arrestes einen Schlussbericht, der insbesondere folgende regelmäßig von der Arrestleiterin oder dem Arrestleiter vorzuschlagende Angaben enthält:

1.  Übersicht über den Arrestverlauf, insbesondere über die durchgeführten Maßnahmen,
    
2.  Aussagen zur Persönlichkeit und zu den gegenwärtigen Lebensumständen der Arrestierten sowie zu ihrer Mitwirkung an der Erreichung des Arrestziels,
    
3.  Darlegung des Förderungs- und Betreuungsbedarfs der Arrestierten sowie Empfehlung von weiteren externen Hilfsangeboten,
    
4.  Vorschläge zu Auflagen und Weisungen im Falle einer Bewährungsunterstellung.
    

(2) Im Nichtbefolgungsarrest enthält der Schlussbericht zudem Angaben über die Befolgung von Weisungen oder die Erfüllung von Auflagen während des Arrestes.

(3) Die Leiterin oder der Leiter der Anstalt erläutert den Arrestierten den Inhalt des Schlussberichts in einem Entlassungsgespräch.

(4) Der Schlussbericht ist für die Arrest- und Strafakten bestimmt.
Eine Ausfertigung des Berichts ist der Jugendgerichtshilfe und bei unter Bewährungsaufsicht stehenden Arrestierten der Bewährungshilfe sowie den Arrestierten und den Personensorgeberechtigten auf deren Wunsch zuzuleiten.

(5) Im Freizeit- und Kurzarrest wird ein Schlussbericht nur dann erstellt, wenn dies aus besonderen Gründen erforderlich ist.

### § 35   
Verbleib oder Aufnahme auf freiwilliger Grundlage

(1) Sofern es die Belegungssituation zulässt, können die Arrestierten auf ihren Antrag ausnahmsweise vorübergehend in der Anstalt verbleiben oder wieder aufgenommen werden, wenn ihre Wohnsituation ungeklärt und ein Aufenthalt in der Anstalt aus diesem Grunde gerechtfertigt ist.
Der Aufenthalt soll eine Woche nicht überschreiten.

(2) Die Unterbringung erfolgt auf vertraglicher Basis.
Gegen die in der Anstalt untergebrachten Entlassenen dürfen Maßnahmen des Arrestes nicht mit unmittelbarem Zwang durchgesetzt werden.

(3) Bei Störung des Anstaltsbetriebes durch die Entlassenen oder aus organisatorischen Gründen kann die Unterbringung jederzeit beendet werden.

(4) Erforderlichenfalls unterrichtet die Anstalt das Jugendamt unverzüglich über die Notwendigkeit der Unterbringung der Arrestierten in einem Heim der Jugendhilfe.

### § 36   
Nachgehende Betreuung

Mit Zustimmung der Leiterin oder des Leiters der Anstalt können Bedienstete in besonders gelagerten Ausnahmefällen an der nachgehenden Betreuung entlassener Arrestierter mit deren Einverständnis mitwirken.
Die nachgehende Betreuung kann auch außerhalb der Anstalt erfolgen.
In der Regel ist sie auf den ersten Monat nach der Entlassung begrenzt.

Abschnitt 11   
Beschwerde
--------------------------

### § 37   
Beschwerderecht

(1) Die Arrestierten erhalten Gelegenheit, sich mit Wünschen, Anregungen und Beschwerden in Angelegenheiten, die sie selbst betreffen oder von gemeinsamem Interesse sind, an die Leiterin oder den Leiter der Anstalt zu wenden.

(2) Besichtigen Vertreterinnen oder Vertreter der Aufsichtsbehörde (§ 44 Absatz 1) die Anstalt, so ist zu gewährleisten, dass die Arrestierten sich in Angelegenheiten, die sie selbst betreffen, an diese wenden können.

(3) Die Möglichkeiten der Dienstaufsichtsbeschwerde und des gerichtlichen Rechtsschutzes bleiben unberührt.

Abschnitt 12   
Kriminologische Forschung
-----------------------------------------

### § 38   
Kriminologische Forschung, Berichtspflicht

(1) Programme zur Förderung der Arrestierten sind auf der Grundlage wissenschaftlicher Erkenntnisse zu konzipieren, zu standardisieren und auf ihre Wirksamkeit hin zu überprüfen.

(2) Der Arrest, insbesondere seine Gestaltung sowie die Programme und deren Wirkungen auf die Erreichung des Arrestziels, soll regelmäßig von dem Kriminologischen Dienst, von einer Hochschule oder von einer anderen Stelle wissenschaftlich begleitet werden.

(3) Die Berichte an den Rechtsausschuss des Landtages nach § 106 Absatz 3 des Brandenburgischen Justizvollzugsgesetzes haben auch die mit diesem Gesetz gemachten Erfahrungen einzubeziehen.

Abschnitt 13   
Aufbau und Organisation der Anstalt
---------------------------------------------------

### § 39   
Einrichtung und Ausstattung der Anstalt

(1) Der Jugendarrest wird in einer selbstständigen Anstalt der Justizverwaltung vollzogen.

(2) Die Gestaltung der Anstalt muss sozialpädagogischen Erfordernissen entsprechen und soziale Gruppenmaßnahmen ermöglichen.

(3) Es sind bedarfsgerechte Einrichtungen für pädagogische Gruppen- und Einzelmaßnahmen vorzusehen.
Gleiches gilt für Besuche, Freizeit, Sport und Seelsorge.

(4) Arrest-, Funktions-, Gemeinschafts- und Besuchsräume sind wohnlich und zweckentsprechend auszustatten.

### § 40   
Festsetzung der Belegungsfähigkeit, Einzelbelegung

(1) Die Aufsichtsbehörde setzt die Belegungsfähigkeit der Anstalt so fest, dass eine angemessene Unterbringung der Arrestierten gewährleistet ist.
§ 39 Absatz 3 ist zu berücksichtigen.

(2) Arresträume dürfen nur mit einer oder einem Arrestierten belegt werden.

### § 41   
Leitung der Anstalt und des Arrestes

(1) Die Leiterin oder der Leiter der Anstalt trägt die Verantwortung für den gesamten Arrest, soweit sich nicht aus Absatz 2 Abweichendes ergibt, und vertritt die Anstalt nach außen.
Sie oder er kann einzelne Aufgabenbereiche und Befugnisse auf andere Bedienstete übertragen.
Die Aufsichtsbehörde kann sich die Zustimmung zur Übertragung vorbehalten.

(2) Die Arrestleiterin oder der Arrestleiter trägt die Verantwortung für die sozialpädagogische Ausgestaltung und Organisation des Arrestes, leitet die Bediensteten fachlich an und vertritt die Leiterin oder den Leiter der Anstalt.

(3) Die Aufsichtsbehörde überträgt die Leitung der Anstalt der Jugendrichterin oder dem Jugendrichter am Ort.
Ist dort eine Jugendrichterin oder ein Jugendrichter nicht oder sind dort mehrere Jugendrichterinnen oder Jugendrichter tätig, bestimmt die Aufsichtsbehörde eine Jugendrichterin oder einen Jugendrichter zur Leiterin oder zum Leiter der Anstalt.

(4) Die Aufsichtsbehörde kann in begründeten Ausnahmefällen abweichend von Absatz 3 eine Beamtin oder einen Beamten des höheren Dienstes zur Leiterin oder zum Leiter der Anstalt bestellen.
In diesem Fall bleibt die Regelung des § 85 Absatz 1 des Jugendgerichtsgesetzes unberührt mit der Maßgabe, dass für die Abgabe der Vollstreckung an die Stelle der oder des als Vollzugsleiterin oder Vollzugsleiter zuständigen Jugendrichterin oder Jugendrichters die oder der am Ort des Vollzuges nach der Geschäftsverteilung des betreffenden Amtsgerichts zuständige Jugendrichterin oder Jugendrichter tritt.

### § 42   
Bedienstete, ärztliche und seelsorgerische Versorgung

(1) Die Anstalt wird mit dem für die Erreichung des Arrestziels und für die Erfüllung ihrer Aufgaben erforderlichen Personal ausgestattet.
Die Bediensteten müssen für die pädagogische Gestaltung des Arrestes geeignet und qualifiziert sein.

(2) Fortbildung sowie Praxisberatung und -begleitung werden regelmäßig durchgeführt.

(3) Die ärztliche Versorgung und die seelsorgerische Betreuung der Arrestierten sind sicherzustellen.

### § 43   
Hausordnung

Die Leiterin oder der Leiter der Anstalt erlässt auf Vorschlag der Arrestleiterin oder des Arrestleiters zur Gestaltung und Organisation des Arrestalltags auf der Grundlage dieses Gesetzes eine Hausordnung.
Die Aufsichtsbehörde kann sich die Genehmigung vorbehalten.

Abschnitt 14   
Aufsicht, Beirat
--------------------------------

### § 44   
Aufsichtsbehörde, Vollstreckungsplan, Vollzugsgemeinschaften

(1) Das für den Justizvollzug zuständige Mitglied der Landesregierung führt die Aufsicht über die Anstalt (Aufsichtsbehörde).

(2) An der Aufsicht über die Gesundheitsfürsorge sowie die Förderung der Arrestierten sind medizinische, pädagogische und sozialpädagogische Fachkräfte zu beteiligen.

(3) Die Aufsichtsbehörde regelt die örtliche und sachliche Zuständigkeit der Anstalt in einem Vollstreckungsplan.

(4) Im Rahmen von Vollzugsgemeinschaften kann der Arrest auch in selbstständigen Anstalten der Justizverwaltungen anderer Länder vorgesehen werden.

### § 45   
Beirat

(1) Bei der Anstalt ist ein Beirat zu bilden.
Auf eine ausgewogene Besetzung mit Frauen und Männern wird hingewirkt.
Bedienstete dürfen nicht Mitglieder des Beirats sein.

(2) Die Mitglieder des Beirats wirken beratend bei der Gestaltung des Arrestes und der Vermittlung der Arrestierten in nachsorgende Maßnahmen mit.
Sie fördern das Verständnis für den Arrest und seine gesellschaftliche Akzeptanz und vermitteln Kontakte zu öffentlichen und privaten Einrichtungen.

(3) Der Beirat steht der Leiterin oder dem Leiter der Anstalt, den Bediensteten und den Arrestierten als Ansprechpartner zur Verfügung.

(4) Die Mitglieder des Beirats können sich über die Unterbringung der Arrestierten und die Gestaltung des Arrestes sowie die Arbeitsbedingungen der Bediensteten unterrichten und die Anstalt besichtigen.
Sie können die Arrestierten in ihren Räumen aufsuchen.

(5) Die Mitglieder des Beirats sind verpflichtet, außerhalb ihres Amtes über alle Angelegenheiten, die ihrer Natur nach vertraulich sind, besonders über Namen und Persönlichkeit der Arrestierten, Verschwiegenheit zu bewahren.
Dies gilt auch nach Beendigung ihres Amtes.

Abschnitt 15   
Datenschutz
---------------------------

### § 46   
Entsprechende Anwendung des Brandenburgischen Justizvollzugsgesetzes

Die §§ 121 bis 125, 126 Absatz 2 Satz 1 bis 4, §§ 127, 128, 129 Absatz 5, §§ 131, 132 Absatz 1, §§ 133 bis 137, 138 Absatz 2, §§ 139 und 140 des Brandenburgischen Justizvollzugsgesetzes gelten mit folgenden Maßgaben entsprechend:

1.  Der Zweck der Förderung der Arrestierten steht den vollzuglichen Zwecken des § 122 Absatz 2 des Brandenburgischen Justizvollzugsgesetzes gleich.
    
2.  Abweichend von § 126 Absatz 2 Satz 1 des Brandenburgischen Justizvollzugsgesetzes ist eine Videoüberwachung auf das Anstaltsgelände, die Außenbereiche des Anstaltsgebäudes und den Eingangsbereich der Anstalt beschränkt.
    
3.  Eine Datenübermittlung im Sinne des § 128 Absatz 1 des Brandenburgischen Justizvollzugsgesetzes ist auch zulässig an Justizvollzugsanstalten für die Durchführung eines Diagnoseverfahrens gemäß § 13 des Brandenburgischen Justizvollzugsgesetzes.
    
4.  Justizvollzugsanstalten stehen den Anstalten des § 131 Absatz 1 Nummer 1 des Brandenburgischen Justizvollzugsgesetzes gleich.
    

Abschnitt 16   
Schlussbestimmung
---------------------------------

### § 47   
Einschränkung von Grundrechten

Durch dieses Gesetz werden die Rechte auf körperliche Unversehrtheit und Freiheit der Person (Artikel 2 Absatz 2 des Grundgesetzes, Artikel 8 Absatz 1 und Artikel 9 Absatz 1 der Verfassung des Landes Brandenburg), auf Unverletzlichkeit des Brief-, Post- und Fernmeldegeheimnisses (Artikel 10 Absatz 1 des Grundgesetzes, Artikel 16 Absatz 1 der Verfassung des Landes Brandenburg) und auf Datenschutz (Artikel 11 der Verfassung des Landes Brandenburg) eingeschränkt.