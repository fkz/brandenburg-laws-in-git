## Achte Verordnung zur Übertragung der Befugnis für den Erlass von Rechtsverordnungen zur Festsetzung von Landschaftsschutzgebieten

Auf Grund der §§ 22 und 26 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) und des § 19, des § 22 Absatz 2 und des § 78 Absatz 1 Satz 3 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S. 350) in Verbindung mit § 9 Absatz 1 des Landesorganisationsgesetzes in der Fassung der Bekanntmachung vom 24.
Mai 2004 (GVBl.
I S. 186) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1 
----

Die Befugnis des für Naturschutz und Landschaftspflege zuständigen Mitgliedes der Landesregierung zum Erlass von Rechtsverordnungen zur Festsetzung von Landschaftsschutzgebieten nach den §§ 22 und 26 des Bundesnaturschutzgesetzes und den §§ 19 und 22 Absatz 2 des Brandenburgischen Naturschutzgesetzes wird

1.  für die im Landkreis Potsdam-Mittelmark geplanten Landschaftsschutzgebiete „Wittbrietzener Feldflur“ und „Ruhlsdorfer Rieselfelder“ auf den Landkreis Potsdam-Mittelmark als untere Naturschutzbehörde,
2.  für das im Landkreis Teltow-Fläming geplante Landschaftsschutzgebiet „Wierachteiche-Zossener Heide“ auf den Landkreis Teltow-Fläming als untere Naturschutzbehörde und
3.  für das im Landkreis Oberhavel geplante Landschaftsschutzgebiet „Liebenberg“ auf den Landkreis Oberhavel als untere Naturschutzbehörde

übertragen.

§ 2
---

Die Befugnis des für Naturschutz und Landschaftspflege zuständigen Mitgliedes der Landesregierung zur Aufhebung von Rechtsverordnungen nach § 22 Absatz 2 und § 78 Absatz 1 Satz 3 des Brandenburgischen Naturschutzgesetzes wird

1.  für das im Landkreis Prignitz gelegene Landschaftsschutzgebiet „Osergebiet bei Perleberg einschließlich Golmer Berg“ (Beschluss Nummer 54 des Rates des Bezirkes Schwerin vom 15.
    Februar 1960 und Nummer 13 vom 1.
    Juni 1972) auf den Landkreis Prignitz als untere Naturschutzbehörde und
2.  für das im Landkreis Oberhavel gelegene Landschaftsschutzgebiet „Liebenberg“ (Beschluss Nummer 8/4/92 des Kreistages des Landkreises Gransee vom 28.
    April 1992) auf den Landkreis Oberhavel als untere Naturschutzbehörde

übertragen.

§ 3 
----

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 18.
April 2012

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack