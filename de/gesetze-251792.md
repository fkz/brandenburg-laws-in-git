## Gesetz zu dem Staatsvertrag zwischen dem Land Brandenburg und der Freien und Hansestadt Hamburg über die Führung des Registers für Binnenschiffe und des Registers für Schiffsbauwerke

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem in Potsdam am 9.
März 2021 unterzeichneten Staatsvertrag zwischen dem Land Brandenburg und der Freien und Hansestadt Hamburg über die Führung des Registers für Binnenschiffe und des Registers für Schiffsbauwerke wird zugestimmt.
Der Vertrag wird nachstehend veröffentlicht.

#### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel 6 Absatz 2 Satz 1 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 5.
Mai 2021

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke

* * *

[zum Staatsvertrag](/vertraege/stv_register_binnenschiffe_schiffsbauwerke)

* * *

### Anlagen

1

[Staatsvertrag zwischen dem Land Brandenburg und der Freien und Hansestadt Hamburg über die Führung des Registers für Binnenschiffe und des Registers für Schiffsbauwerke](/br2/sixcms/media.php/68/GVBl_I_10_2021-Anlage.pdf "Staatsvertrag zwischen dem Land Brandenburg und der Freien und Hansestadt Hamburg über die Führung des Registers für Binnenschiffe und des Registers für Schiffsbauwerke") 162.2 KB