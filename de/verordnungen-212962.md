## Verordnung über den Bau von Betriebsräumen für elektrische Anlagen im Land Brandenburg (BbgEltBauV)

Auf Grund des § 80 Absatz 1 Nummer 1 und Absatz 2 der Brandenburgischen Bauordnung in der Fassung der Bekanntmachung vom 17. September 2008 (GVBl. I S. 226) verordnet der Minister für Infrastruktur und Landwirtschaft:

§ 1   
Geltungsbereich
----------------------

Diese Verordnung gilt für die Aufstellung von

1.  Transformatoren und Schaltanlagen für Nennspannungen über ein Kilovolt,
    
2.  ortsfesten Stromerzeugungsaggregaten für bauordnungsrechtlich vorgeschriebene sicherheitstechnische Anlagen und Einrichtungen und
    
3.  zentralen Batterieanlagen für bauordnungsrechtlich vorgeschriebene sicherheitstechnische Anlagen und Einrichtungen
    

in Gebäuden.

§ 2  
Begriffsbestimmung
------------------------

Betriebsräume für elektrische Anlagen (elektrische Betriebsräume) sind Räume, die ausschließlich zur Unterbringung von Einrichtungen im Sinne des § 1 dienen.

§ 3   
Allgemeine Anforderungen
-------------------------------

Innerhalb von Gebäuden müssen elektrische Anlagen nach § 1 in jeweils eigenen elektrischen Betriebsräumen untergebracht sein.
Ein elektrischer Betriebsraum ist nicht erforderlich für die in § 1 Nummer 1 genannten elektrischen Anlagen in

1.  freistehenden Gebäuden und
    
2.  durch Brandwände abgetrennten Gebäudeteilen,
    

wenn diese nur die in § 1 Nummer 1 aufgezählten elektrischen Anlagen enthalten.

§ 4   
Anforderungen an elektrische Betriebsräume
-------------------------------------------------

(1) Elektrische Betriebsräume müssen so angeordnet sein, dass sie im Gefahrenfall von allgemein zugänglichen Räumen oder vom Freien leicht und sicher erreichbar sind und durch nach außen aufschlagende Türen jederzeit ungehindert verlassen werden können.
Sie dürfen von notwendigen Treppenräumen nicht unmittelbar zugänglich sein.
Der Rettungsweg innerhalb elektrischer Betriebsräume bis zu einem Ausgang darf nicht länger als 35 Meter sein.

(2) Elektrische Betriebsräume müssen so groß sein, dass die elektrischen Anlagen ordnungsgemäß errichtet und betrieben werden können.
Sie müssen eine lichte Höhe von mindestens 2 Metern haben.
Über Bedienungs- und Wartungsgängen muss eine Durchgangshöhe von mindestens 1,80 Metern vorhanden sein.

(3) Elektrische Betriebsräume müssen den betrieblichen Anforderungen entsprechend wirksam be- und entlüftet werden.

(4) In elektrischen Betriebsräumen dürfen Leitungen und Einrichtungen, die nicht zum Betrieb der jeweiligen elektrischen Anlagen erforderlich sind, nicht vorhanden sein.
Satz 1 gilt nicht für die zur Sicherheitsstromversorgung aus der Batterieanlage erforderlichen Installationen in elektrischen Betriebsräumen nach § 1 Nummer 3.

§ 5   
Zusätzliche Anforderungen an elektrische Betriebsräume für Transformatoren und Schaltanlagen mit Nennspannungen über ein Kilovolt
----------------------------------------------------------------------------------------------------------------------------------------

(1) Raumabschließende Bauteile elektrischer Betriebsräume für Transformatoren und Schaltanlagen mit Nennspannungen über ein Kilovolt, ausgenommen Außenwände, sind feuerbeständig auszuführen.
Der erforderliche Raumabschluss zu anderen Räumen darf durch einen Druckstoß aufgrund eines Kurzschlusslichtbogens nicht gefährdet werden.

(2) Türen müssen mindestens feuerhemmend, selbstschließend und rauchdicht sein sowie im Wesentlichen aus nichtbrennbaren Baustoffen bestehen; soweit sie ins Freie führen, genügen selbstschließende Türen aus nichtbrennbaren Baustoffen.
An den Türen muss außen ein Hochspannungswarnschild angebracht sein.

(3) Bei elektrischen Betriebsräumen für Transformatoren mit Mineralöl oder einer synthetischen Flüssigkeit mit einem Brennpunkt ≤ 300 °C als Kühlmittel muss mindestens ein Ausgang unmittelbar ins Freie oder über einen Vorraum ins Freie führen.
Der Vorraum darf auch mit dem Schaltraum, jedoch nicht mit anderen Räumen in Verbindung stehen.

(4) Elektrische Betriebsräume nach Absatz 3 Satz 1 dürfen sich nicht in Geschossen befinden, deren Fußboden mehr als 4 Meter unter der festgelegten Geländeoberfläche liegt.
Sie dürfen auch nicht in Geschossen über dem Erdgeschoss liegen.

(5) Elektrische Betriebsräume müssen unmittelbar oder über eigene Lüftungsleitungen wirksam aus dem Freien belüftet und in das Freie entlüftet werden.
Lüftungsleitungen, die durch andere Räume führen, sind feuerbeständig herzustellen.
Öffnungen von Lüftungsleitungen zum Freien müssen Schutzgitter haben.

(6) Fußböden müssen aus nicht brennbaren Baustoffen bestehen.
Dies gilt nicht für Fußbodenbeläge.

(7) Unter Transformatoren muss auslaufende Isolier- und Kühlflüssigkeit sicher aufgefangen werden können.
Für höchstens drei Transformatoren mit jeweils bis zu 1 000 Liter Isolierflüssigkeit in einem elektrischen Betriebsraum genügt es, wenn die Wände in der erforderlichen Höhe sowie der Fußboden undurchlässig ausgebildet sind.
An den Türen müssen entsprechend hohe und undurchlässige Schwellen vorhanden sein.

§ 6   
Zusätzliche Anforderungen an elektrische Betriebsräume für ortsfeste Stromerzeugungsaggregate
----------------------------------------------------------------------------------------------------

(1) Raumabschließende Bauteile von elektrischen Betriebsräumen für ortsfeste Stromerzeugungsaggregate zur Versorgung bauordnungsrechtlich vorgeschriebener sicherheitstechnischer Anlagen und Einrichtungen, ausgenommen Außenwände, müssen in einer dem erforderlichen Funktionserhalt der zu versorgenden Anlagen entsprechenden Feuerwiderstandsfähigkeit ausgeführt sein.
§ 5 Absatz 5 Satz 1 und 3 und Absatz 6 gilt sinngemäß.
Für Lüftungsleitungen, die durch andere Räume führen, gilt Satz 1 entsprechend.
Die Feuerwiderstandsfähigkeit der Türen muss derjenigen der raumabschließenden Bauteile entsprechen; die Türen müssen selbstschließend sein.

(2) Elektrische Betriebsräume nach Absatz 1 Satz 1 müssen frostfrei sein oder beheizt werden können.

§ 7   
Zusätzliche Anforderungen an Batterieräume
-------------------------------------------------

(1) Raumabschließende Bauteile von elektrischen Betriebsräumen für zentrale Batterieanlagen zur Versorgung bauordnungsrechtlich vorgeschriebener sicherheitstechnischer Anlagen und Einrichtungen, ausgenommen Außenwände, müssen in einer dem erforderlichen Funktionserhalt der zu versorgenden Anlagen entsprechenden Feuerwiderstandsfähigkeit ausgeführt sein.
§ 5 Absatz 5 Satz 1 und 3 und § 6 Absatz 2 gelten sinngemäß.
Für Lüftungsleitungen, die durch andere Räume führen, gilt Satz 1 entsprechend.
Die Feuerwiderstandsfähigkeit der Türen muss derjenigen der raumabschließenden Bauteile entsprechen; die Türen müssen selbstschließend sein.
An den Türen muss ein Schild „Batterieraum“ angebracht sein.

(2) Fußböden von elektrischen Betriebsräumen nach Absatz 1 Satz 1, in denen geschlossene Zellen aufgestellt werden, müssen an allen Stellen für elektrostatische Ladungen einheitlich und ausreichend ableitfähig sein.

§ 8   
Zusätzliche Bauvorlagen
------------------------------

Die Bauvorlagen müssen Angaben über die Lage der elektrischen Betriebsräume und die Art der elektrischen Anlagen enthalten.

§ 9   
Inkrafttreten
--------------------

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 15.
August 2014

Der Minister für Infrastruktur und Landwirtschaft

Jörg Vogelsänger

[1](#1a)) Die Verpflichtungen aus der Richtlinie 98/34/EG des Europäischen Parlaments und des Rates vom 22.
Juni 1998 über ein Informationsverfahren auf dem Gebiet der Normen und technischen Vorschriften und der Vorschriften für die Dienste der Informationsgesellschaft (ABl. L 204 vom 21.7.1998, S.
37), die zuletzt durch die Richtlinie 2006/96/EG vom 20. November 2006 (ABl.
L 363 vom 20.12.2006, S.
81) geändert worden ist, sind beachtet worden.