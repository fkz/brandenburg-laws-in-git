## Verordnung über den Aufstieg in die Laufbahn des gehobenen Polizeivollzugsdienstes des Landes Brandenburg  (Polizeiaufstiegsverordnung - PolAV)

Inhaltsübersicht
----------------

### Abschnitt 1  
Allgemeine Vorschriften

[§ 1       Geltungsbereich](#1)  
[§ 2       Ziel und Dauer der Ausbildung](#2)  
[§ 3       Urlaub](#3)  
[§ 4       Noten und Bewertungsgrundsätze](#4)  
[§ 5       Prüfungsakte](#5)  
[§ 6       Ablauf der Ausbildung](#6)

### Abschnitt 2  
Aufstiegsprüfung

[§ 7       Zweck, Inhalt, Ablauf](#7)  
[§ 8       Prüfungskommission](#8)  
[§ 9       Prüfungsfächer](#9)  
[§ 10     Durchführung der Prüfung](#10)  
[§ 11     Prüfungsvergünstigungen](#11)  
[§ 12     Schriftliche Prüfung](#12)  
[§ 13     Aufsicht](#13)  
[§ 14     Bewertung](#14)  
[§ 15     Mündliche Prüfung](#15)  
[§ 16     Niederschrift](#16)  
[§ 17     Gesamtergebnis](#17)  
[§ 18     Bekanntgabe, Prüfungszeugnis, Mitteilung](#18)  
[§ 19     Wiederholung der Aufstiegsprüfung](#19)  
[§ 20     Rechtsfolge bei endgültig nicht bestandener Aufstiegsprüfung](#20)

### Abschnitt 3  
Schlussvorschriften

[§ 21     Übergangsregelung](#21)

### Abschnitt 1  
Allgemeine Vorschriften

#### [](#01)§ 1  
Geltungsbereich

Diese Verordnung regelt die Ausbildung und Prüfung für den Aufstieg der Beamtinnen und Beamten des mittleren Polizeivollzugsdienstes in die Laufbahn des gehobenen Polizeivollzugsdienstes (Kommissarbewerberinnen und Kommissarbewerber).

#### [](#02)§ 2   
Ziel und Dauer der Ausbildung

(1) Ziel der Ausbildung für den Aufstieg in den gehobenen Polizeivollzugsdienst (Aufstiegsausbildung) ist es, den Kommissarbewerberinnen und Kommissarbewerbern die Befähigung für den gehobenen Polizeivollzugsdienst zu vermitteln.

(2) Die Aufstiegsausbildung und die Aufstiegsprüfung werden an der Fachhochschule der Polizei des Landes Brandenburg durchgeführt.
Sie dauert regelmäßig ein halbes Jahr und endet mit dem Bestehen oder endgültigen Nichtbestehen der Aufstiegsprüfung für den gehobenen Polizeivollzugsdienst (II.
Fachprüfung).

(3) Wird die Ausbildung wegen Krankheit, durch Zeiten eines Beschäftigungsverbotes nach den Bestimmungen über den Mutterschutz oder wegen der Inanspruchnahme von Elternzeit in einem Maße unterbrochen, dass wesentliche Teile der Ausbildung nicht wahrgenommen oder nicht erfolgreich abgeschlossen werden können, entscheidet die Fachhochschule der Polizei des Landes Brandenburg, ob und in welchem Umfang im Einzelfall von dem Ausbildungsgang abgewichen werden kann oder ob eine Wiederholung notwendig ist.

(4) Wird gemäß Absatz 3 eine Wiederholung als notwendig angesehen, beginnt diese mit der nächsten Aufstiegsausbildung neu.
Bei der Wiederholung der Aufstiegsprüfung (§ 19) verlängert sich die Aufstiegsausbildung in dem vorgeschriebenen Maße.

#### [](#03)§ 3  
Urlaub

Der Erholungsurlaub ist grundsätzlich für die Kommissarbewerberinnen und Kommissarbewerber eines Aufstiegslehrganges geschlossen in den ausbildungsfreien Zeiten zu gewähren.
 Über Ausnahmen entscheidet die Fachhochschule der Polizei des Landes Brandenburg.

#### [](#04)§ 4  
Noten und Bewertungsgrundsätze

(1) Einzelleistungen dürfen nur wie folgt und nur unter Verwendung von vollen Punktzahlen bewertet werden:

sehr gut (1)

\= 15 bis 14 Punkte

eine Leistung, die den Anforderungen in besonderem Maße entspricht;

gut (2)

\= 13 bis 11 Punkte

eine Leistung, die den Anforderungen voll entspricht;

befriedigend (3)

\= 10 bis 8 Punkte

eine Leistung, die im Allgemeinen den Anforderungen entspricht;

ausreichend (4)

\= 7 bis 5 Punkte

eine Leistung, die zwar Mängel aufweist, aber im Ganzen den Anforderungen noch entspricht;

mangelhaft (5)

\= 4 bis 2 Punkte

eine Leistung, die den Anforderungen nicht entspricht, jedoch erkennen lässt, dass die notwendigen Grundkenntnisse vorhanden sind und die Mängel in absehbarer Zeit behoben werden könnten;

ungenügend (6)

\= 1 bis 0 Punkte

eine Leistung, die den Anforderungen nicht entspricht und bei der die Grundkenntnisse so lückenhaft sind, dass die Mängel in absehbarer Zeit nicht behoben werden könnten.

(2) Die Bewertung von Einzelleistungen hat insbesondere die Richtigkeit der Aussagen, die praktische Anwendbarkeit des Ergebnisses, die Art und Folgerichtigkeit der Argumentation sowie je nach Art der Einzelleistung die Gliederung, die Ausdrucksweise, die Rechtschreibung und Grammatik zu berücksichtigen.

(3) Durchschnitts- und Gesamtpunktwerte sind jeweils auf zwei Dezimalstellen hinter dem Komma zu berechnen; die dritte Dezimalstelle bleibt unberücksichtigt.
Der Notenwert ist wie folgt abzugrenzen:

von 15,00 bis 14,00 Punkte

\= sehr gut

von 13,99 bis 11,00 Punkte

\= gut

von 10,99 bis 8,00 Punkte

\= befriedigend

von 7,99 bis 5,00 Punkte

\= ausreichend

von 4,99 bis 2,00 Punkte 

\= mangelhaft

von 1,99 bis 0,00 Punkte

\= ungenügend.

#### [](#05)§ 5  
Prüfungsakte

(1) Für jede Kommissarbewerberin und jeden Kommissarbewerber ist eine Prüfungsakte anzulegen.
 Die von der Fachhochschule der Polizei des Landes Brandenburg geführte Prüfungsakte beinhaltet insbesondere die bewerteten Prüfungsklausuren der schriftlichen Prüfung (§ 12), die Niederschrift (§ 16) sowie eine Abschrift des Prüfungszeugnisses oder der schriftlichen Mitteilung (§ 18 Absatz 2).

(2) Der Kommissarbewerberin und dem Kommissarbewerber ist auf Antrag unter Aufsicht Einsicht in seine Prüfungsakte zu gewähren.
Nach Bekanntgabe der Ergebnisse der Prüfungsklausuren ist der Kommissarbewerberin und dem Kommissarbewerber innerhalb von zwei Wochen gemäß Satz 1 Einsicht zu gewähren.

(3) Die Prüfungsakten sind von der Fachhochschule der Polizei des Landes Brandenburg mindestens fünf Jahre, vom Tag nach Beendigung der Aufstiegsausbildung an gerechnet, aufzubewahren.

#### [](#06)§ 6  
Ablauf der Ausbildung

(1) Die Ausbildung besteht aus einem Ausbildungsabschnitt.
Im fünften und sechsten Monat der Ausbildung findet die II.
Fachprüfung statt.
Teile der Ausbildung können ohne Anwesenheitspflicht vermittelt werden (Fernausbildung).

(2) Die Ausbildung wird insbesondere in folgenden Fächern und Fächerkomplexen durchgeführt:

1              Einsatz- und Führungslehre, Management:  
1.1           Einsatzlehre  
1.2           Führungslehre  
2              Rechtswissenschaften:  
2.1           Staats- und Verfassungsrecht  
2.2           Straf- und Ordnungswidrigkeitenrecht  
2.3           Eingriffsrecht  
2.4           Öffentliches Dienstrecht  
3              Kriminal- und Sozialwissenschaften:  
3.1           Kriminalistik  
3.2           Kriminologie  
4              Verkehrsrecht und Verkehrslehre:  
4.1           Verkehrsrecht  
4.2           Verkehrslehre.

Die Einzelheiten der Ausbildung werden in einem Lehrplan bestimmt.

(3) Die Kommissarbewerberinnen und Kommissarbewerber sind verpflichtet, an den vorgeschriebenen Lehrveranstaltungen teilzunehmen und sich den dort geforderten Leistungsnachweisen zu unterziehen.

(4) Weitere Fächer in Ergänzung zu Absatz 2 Satz 1 können im Lehrplan festgelegt werden.
Er wird von der Fachhochschule der Polizei des Landes Brandenburg aufgestellt.

### Abschnitt 2  
Aufstiegsprüfung

#### [](#07)§ 7  
Zweck, Inhalt, Ablauf

(1) Mit der Aufstiegsprüfung wird festgestellt, ob die Kommissarbewerberinnen und Kommissarbewerber für die Laufbahn des gehobenen Polizeivollzugsdienstes befähigt sind.

(2) Die Aufstiegsprüfung besteht aus einem schriftlichen und einem mündlichen Prüfungsteil.
Der schriftliche Prüfungsteil geht dem mündlichen voraus.

#### [](#08)§ 8  
Prüfungskommission

(1) Die Fachhochschule der Polizei des Landes Brandenburg bestellt nach Bedarf mindestens eine Prüfungskommission.
Die Prüfungskommissionen bestehen grundsätzlich aus Lehrkräften der Fachhochschule der Polizei des Landes Brandenburg.
Zudem werden bei der Besetzung der Prüfungskommissionen Vertreterinnen oder Vertreter des Ministeriums des Innern, der Polizeibehörde sowie der Polizeieinrichtungen angemessen berücksichtigt.
Jede Prüfungskommission besteht aus fünf Mitgliedern, und zwar in der Regel aus einer Beamtin oder einem Beamten des höheren Dienstes als Vorsitzende oder Vorsitzenden sowie vier Beamtinnen oder Beamten des höheren oder des gehobenen Dienstes oder vergleichbaren Beschäftigten als Beisitzerinnen oder Beisitzern.
Es können stellvertretende Mitglieder bestellt werden.
Die Kommissionsmitglieder und deren Vertreterinnen und Vertreter werden für die Dauer von zwei Jahren bestellt.
Die Wiederbestellung ist zulässig.
Sie sind in ihrer Prüfungstätigkeit unabhängig und an Weisungen nicht gebunden.

(2) Die Prüfungskommission ist beschlussfähig, wenn mindestens drei ihrer Mitglieder anwesend sind.
Sie entscheiden mit Stimmenmehrheit.
Bei Stimmengleichheit gibt die Stimme der Vorsitzenden oder des Vorsitzenden den Ausschlag.
Eine Stimmenthaltung ist nicht zulässig.

(3) Die Sitzungen der Prüfungskommission sind nicht öffentlich.
Bei den Beratungen der Prüfungskommission dürfen nur ihre Mitglieder anwesend sein.

#### [](#09)§ 9  
Prüfungsfächer

Die schriftliche und mündliche Prüfung erfolgt jeweils in drei der nachstehenden Fächer:

1.  Einsatzlehre,
    
2.  Straf- und Ordnungswidrigkeitenrecht,
    
3.  Kriminalistik und Kriminologie,
    
4.  Verkehrsrecht und Verkehrslehre,
    
5.  Eingriffsrecht und
    
6.  Staats- und Verfassungsrecht.
    

#### [](#010)§ 10  
Durchführung der Prüfung

(1) Die Fachhochschule der Polizei des Landes Brandenburg setzt die Termine der schriftlichen und mündlichen Prüfung fest und gibt diese spätestens drei Wochen vor dem Beginn des schriftlichen Teils der Aufstiegsprüfung bekannt.
Spätestens eine Woche vor den Prüfungsterminen teilt die Fachhochschule der Polizei des Landes Brandenburg den Kommissarbewerberinnen und den Kommissarbewerbern die in den Prüfungen zulässigen Hilfsmittel mit.

(2) Ist eine Kommissarbewerberin oder ein Kommissarbewerber durch Krankheit oder sonstige von ihr oder ihm nicht zu vertretende Umstände an der Ablegung der Prüfung oder von Prüfungsteilen gehindert, so hat sie oder er dies bei Krankheit durch ein ärztliches Attest zu belegen.

(3) Bricht eine Kommissarbewerberin oder ein Kommissarbewerber aus einem in Absatz 2 genannten Grund die Prüfung ab, so wird die Prüfung an einem von der Fachhochschule der Polizei des Landes Brandenburg zu bestimmenden Termin fortgesetzt.
Die Fachhochschule der Polizei des Landes Brandenburg entscheidet, ob und in welchem Umfang bereits erbrachte Prüfungsleistungen angerechnet werden.

(4) Erscheint eine Kommissarbewerberin oder ein Kommissarbewerber an einem Prüfungstag nicht oder tritt sie oder er ohne Genehmigung der Prüfungskommission von der Prüfung zurück, ohne dass ein ausreichender Entschuldigungsgrund vorliegt, gilt die Aufstiegsprüfung als nicht bestanden.
Die Entscheidung, ob ein ausreichender Entschuldigungsgrund vorliegt, trifft die Prüfungskommission durch Beschluss.

#### [](#011)§ 11  
Prüfungsvergünstigungen

(1) Schwerbehinderte Personen können auf Antrag entsprechend der Schwere der nachgewiesenen Prüfungsbehinderung in der schriftlichen Prüfung eine Verlängerung der Bearbeitungszeit bis zu einem Viertel der normalen Bearbeitungszeit gewährt werden.
In Fällen besonders weitgehender Prüfungsbehinderung kann auf Antrag der schwerbehinderten Person die Bearbeitungszeit um bis zur Hälfte der normalen Bearbeitungszeit verlängert werden.
Schwerbehinderte Personen können neben oder anstelle einer Verlängerung der Bearbeitungszeit andere angemessene Erleichterungen gewährt werden, soweit diese den Wettbewerb nicht beeinträchtigen.

(2) Anträge auf Prüfungsvergünstigungen sind spätestens zwei Wochen vor Beginn der schriftlichen Prüfung einzureichen.
Liegen die Voraussetzungen für die Gewährung einer Prüfungsvergünstigung erst zu einem späteren Zeitpunkt vor, so ist der Antrag unverzüglich zu stellen.
Der Nachweis der Prüfungsbehinderung ist durch ein amts- oder polizeiärztliches Attest zu führen.
Die Begutachtung durch einen weiteren Arzt kann angeordnet werden.

(3) Für die mündliche Prüfung können auf Antrag der schwerbehinderten Person angemessene Erleichterungen gewährt werden.
Absatz 2 gilt entsprechend.

#### [](#012)§ 12  
Schriftliche Prüfung

Die schriftliche Prüfung umfasst drei Prüfungsklausuren.
Die Themen der Klausuraufgaben werden durch die Fachhochschule der Polizei des Landes Brandenburg aus den Fächern nach § 9 gestellt.
Die Bearbeitungszeit und die zulässigen Hilfsmittel sind in der Klausuraufgabe anzugeben.
In jedem Prüfungsfach ist eine Klausur zu fertigen.
 Die Bearbeitungszeit beträgt in den einzelnen Fächern drei Zeitstunden.
Die Klausuraufgaben sind getrennt in verschlossenen Umschlägen aufzubewahren und erst jeweils zu Prüfungsbeginn in Gegenwart der Kommissarbewerberinnen und der Kommissarbewerber zu öffnen.
Die Prüfungsklausuren dürfen keinen Hinweis auf die Kommissarbewerberinnen und die Kommissarbewerber enthalten.
Die Entschlüsselung der Prüfungsklausuren darf durch die Prüfungskommission erst nach ihrer endgültigen Bewertung vorgenommen werden.

#### [](#013)§ 13  
Aufsicht

(1) Die Fachhochschule der Polizei des Landes Brandenburg bestimmt, wer die Aufsicht führt.

(2) Die aufsichtführende Person belehrt die Kommissarbewerberinnen und die Kommissarbewerber und fertigt eine Niederschrift.
In der Niederschrift wird jede Abweichung von dem Prüfungsablauf und der Zeitpunkt der Abgabe der Prüfungsklausuren vermerkt.

(3) Macht sich eine Kommissarbewerberin oder ein Kommissarbewerber einer Täuschungshandlung oder nach einmaliger Ermahnung einer erheblichen Störung des Prüfungsablaufs schuldig, hat die aufsichtführende Person dies in ihrer Niederschrift zu vermerken und die Fachhochschule der Polizei des Landes Brandenburg unverzüglich davon zu unterrichten.
Die aufsichtführende Person kann Kontrollen des Arbeitsplatzes einschließlich der mitgebrachten und zugelassenen Arbeitsmittel durchführen.
Eine Kommissarbewerberin oder ein Kommissarbewerber, die oder der bei Klausurterminen erheblich den Prüfungsablauf stört, kann von der aufsichtführenden Person von der Fortsetzung dieser Prüfungsklausur ausgeschlossen werden.

(4) Die Prüfungsklausuren sind spätestens mit Ablauf der Bearbeitungszeit bei der aufsichtführenden Person abzugeben.
Nach Ablauf der Bearbeitungszeit vermerkt sie oder er in ihrer oder seiner Niederschrift, wer seine Prüfungsklausur verspätet oder keine Prüfungsklausur abgegeben hat.
Die Prüfungsklausuren und die Niederschrift hat sie oder er in einem Umschlag zu verschließen und der Vorsitzenden oder dem Vorsitzenden oder einem von dieser oder diesem bestimmten Mitglied der Prüfungskommission unmittelbar zuzuleiten.

#### [](#014)§ 14  
Bewertung

(1) Die Prüfungsklausuren sind nacheinander von einer Erst- und einer Zweitkorrektorin oder einem Erst- und einem Zweitkorrektor, die in der Regel Mitglieder der zuständigen Prüfungskommission sind, gemäß § 4 Absatz 1 und 2 zu bewerten.
Neben den stellvertretenden Mitgliedern können auch Mitglieder anderer Prüfungskommissionen zur Korrektur der Prüfungsklausuren herangezogen werden.

(2) Unterscheidet sich die Bewertung der Prüfungsklausuren um drei oder mehr Punkte, erfolgt eine Drittkorrektur durch ein weiteres Mitglied der Prüfungskommission.
Die Drittkorrektorin oder der Drittkorrektor legt im Rahmen der Bewertung der Erst- und Zweitkorrektur die endgültige Note fest.
Beträgt der Unterschied in der Bewertung der Prüfungsklausuren weniger als drei Punkte, ist der Durchschnitt gemäß § 4 Absatz 3 aus der Bewertung der Erst- und Zweitkorrektur zu bilden.
Zur Feststellung des Ergebnisses der schriftlichen Prüfung wird der Durchschnitt aus den bewerteten Leistungen in den einzelnen Prüfungsfächern errechnet.
Die Prüfungskommission stellt das Ergebnis der schriftlichen Prüfung gemäß § 4 Absatz 3 fest.
Anschließend sind die Prüfungsklausuren und die dazugehörigen Bewertungen unverzüglich zur Prüfungsakte zu nehmen.
Eine einmal getroffene Entscheidung über die Bewertung der Prüfungsleistung kann durch die Prüfungskommission nicht mehr geändert werden.
§ 17 Absatz 4 bleibt unberührt.

(3) Prüfungsklausuren, zu denen eine Kommissarbewerberin oder ein Kommissarbewerber ohne ausreichende Entschuldigung nicht erscheint oder die sie oder er ohne ausreichende Entschuldigung ungelöst zurückgibt, werden mit der Note „ungenügend“ (0 Punkte) bewertet.

(4) Über die Folgen einer erheblichen Störung des Prüfungsablaufs oder einer Täuschungshandlung entscheidet die Prüfungskommission.
Sie kann die vorliegende Prüfungsklausur mit der Note „ungenügend“ (0 Punkte) bewerten.
In besonderen Fällen kann sie nach dem Grad der Verfehlung die Wiederholung dieser Prüfungsklausur anordnen oder die schriftliche Prüfung für nicht bestanden erklären.

(5) Eine Kommissarbewerberin oder ein Kommissarbewerber ist zur mündlichen Prüfung zuzulassen, wenn mindestens zwei Prüfungsklausuren mit der Note „ausreichend“ bewertet wurden und insgesamt im Durchschnitt aller drei Klausuren mindestens die Note „ausreichend“ erreicht wird, andernfalls ist die Aufstiegsprüfung nicht bestanden.

(6) Spätestens eine Woche vor der mündlichen Prüfung gibt die Fachhochschule der Polizei des Landes Brandenburg der Kommissarbewerberin und dem Kommissarbewerber die Ergebnisse der schriftlichen Prüfung und die Prüfungsfächer der mündlichen Prüfung bekannt.

#### [](#015)§ 15  
Mündliche Prüfung

(1) Die mündliche Prüfung findet nach Abschluss der schriftlichen Prüfung statt.
Die Vorsitzende oder der Vorsitzende der Prüfungskommission bestimmt die Gebiete aus den Prüfungsfächern (§ 9), auf die sich die mündliche Prüfung erstreckt.
Dabei sollen die Fächer vorrangig Berücksichtigung finden, die in der schriftlichen Prüfung nicht geprüft worden sind.
Innerhalb der Fächer nach § 9 können Lehrinhalte, die bis zum Zeitpunkt der mündlichen Prüfung vermittelt wurden, in die Prüfung eingehen.

(2) Die Vorsitzende oder der Vorsitzende der Prüfungskommission leitet die mündliche Prüfung.
Sie oder er hat darauf hinzuwirken, dass die Kommissarbewerberinnen und Kommissarbewerber in geeigneter Weise befragt werden.
Sie oder er ist berechtigt, jederzeit in die mündliche Prüfung einzugreifen.
Beauftragte des Ministeriums des Innern, die Präsidentin oder der Präsident und die Leiterin oder der Leiter des Prüfungsamtes der Fachhochschule der Polizei des Landes Brandenburg sind berechtigt, bei der mündlichen Prüfung zugegen zu sein.
Die Prüfungskommission kann anderen Personen, bei denen ein dienstliches Interesse vorliegt, gestatten, bei der mündlichen Prüfung anwesend zu sein.
Die Vorschriften des Landespersonalvertretungsgesetzes bleiben unberührt.

(3) Die Prüfungskommission kann im Einvernehmen mit der Fachhochschule der Polizei des Landes Brandenburg auch ein Mitglied einer anderen Prüfungskommission zur Prüfung hinzuziehen.

(4) Die mündliche Prüfung einer Kommissarbewerberin und eines Kommissarbewerbers soll in der Regel 30 Minuten dauern.
Sie soll in Gruppenprüfungen mit höchstens vier Kommissarbewerberinnen oder Kommissarbewerbern durchgeführt werden.

(5) Die Prüfungskommission bewertet die Leistungen der mündlichen Prüfung gemäß § 4 Absatz 1 und 2.
Zur Feststellung des Ergebnisses der mündlichen Prüfung bewertet die Prüfungskommission die in den einzelnen Prüfungsfächern erbrachte Leistung durch Bildung einer Gesamtnote.
Die Prüfungskommission stellt das Ergebnis der mündlichen Prüfung gemäß § 4 Absatz 3 fest.

(6) Eine Kommissarbewerberin oder ein Kommissarbewerber, dessen Gesamtleistung nach Absatz 5 Satz 2 nicht mit mindestens „ausreichend“ bewertet wurde, hat die Aufstiegsprüfung nicht bestanden.

#### [](#016)§ 16  
Niederschrift

Über den Prüfungsverlauf ist für jede Kommissarbewerberin und jeden Kommissarbewerber eine Niederschrift zu fertigen.
Die Niederschrift ist zur Prüfungsakte zu geben.

#### [](#017)§ 17  
Gesamtergebnis

(1) Nach Bestehen der mündlichen Prüfung stellt die Prüfungskommission das Gesamtergebnis (Abschlussnote) fest.
Bei der Feststellung werden

1.  das Ergebnis der schriftlichen Prüfung mit 60 Prozent und
2.  das Ergebnis der mündlichen Prüfung mit 40 Prozent

berücksichtigt.

(2) Zur Feststellung des Gesamtergebnisses werden jeweils die Punktwerte der Ergebnisse der schriftlichen und mündlichen Prüfung entsprechend dem in Absatz 1 angegebenen Anteilsverhältnis zu einem Gesamtpunktwert zusammengefasst.
Die Prüfungskommission kann in begründeten Fällen den Gesamtpunktwert um bis zu einem Punkt erhöhen, wenn hierdurch der Gesamtleistungsstand zutreffender gekennzeichnet wird und die Abweichung auf das Bestehen der Aufstiegsprüfung keinen Einfluss hat.
Die Abschlussnote wird gemäß § 4 Absatz 3 festgestellt.

(3) Ist die Abschlussnote schlechter als „ausreichend“, hat die Kommissarbewerberin oder der Kommissarbewerber die Aufstiegsprüfung nicht bestanden.

(4) Wird erst nach Aushändigung des Zeugnisses bekannt, dass die Kommissarbewerberin oder der Kommissarbewerber bei der Prüfung getäuscht hat, so kann die Prüfungskommission innerhalb einer Frist von drei Jahren nach der mündlichen Prüfung auch nachträglich Prüfungsleistungen für ungenügend erklären und das Gesamtergebnis entsprechend neu festsetzen oder die Prüfung für nicht bestanden erklären.
Falls die Prüfungskommission, die die Prüfung abgenommen hat, nicht mehr zusammentreten kann, entscheidet eine andere Prüfungskommission, die von dem Prüfungsamt der Fachhochschule der Polizei des Landes Brandenburg bestellt wird.

#### [](#018)§ 18  
Bekanntgabe, Prüfungszeugnis, Mitteilung

(1) Nach Abschluss der mündlichen Prüfung gibt die Vorsitzende oder der Vorsitzende der Prüfungskommission das Ergebnis der mündlichen Prüfung und bei bestandener mündlicher Prüfung die Abschlussnote bekannt.

(2) Über das Ergebnis der bestandenen Aufstiegsprüfung erteilt die Fachhochschule der Polizei des Landes Brandenburg ein Prüfungszeugnis.
Wer die Aufstiegsprüfung nicht bestanden hat, erhält darüber eine schriftliche Mitteilung von der Fachhochschule der Polizei des Landes Brandenburg.
Je eine Ausfertigung des Prüfungszeugnisses oder der Mitteilung ist zur Prüfungsakte und zur Personalakte zu geben.

#### [](#019)§ 19  
Wiederholung der Aufstiegsprüfung

(1) Eine nicht bestandene Aufstiegsprüfung kann einmal wiederholt werden.

(2) Kommissarbewerberinnen und Kommissarbewerber, die die Aufstiegsprüfung erstmalig nicht bestanden haben, haben gegenüber der Fachhochschule der Polizei des Landes Brandenburg innerhalb von zwei Wochen nach Zugang der schriftlichen Mitteilung gemäß § 18 Absatz 2 schriftlich zu erklären, ob sie die Wiederholung der Aufstiegsprüfung wünschen.
 Kommissarbewerberinnen und Kommissarbewerber, die die Prüfung gemäß § 15 Absatz 6 nicht bestanden haben, wiederholen nur den mündlichen Teil der Prüfung.

(3) Die Wiederholungsprüfung soll frühestens vier und spätestens acht Wochen nach Bekanntgabe des Nichtbestehens der Prüfung beginnen.

(4) Die Fachhochschule der Polizei des Landes Brandenburg bestimmt, welche Hilfen zur Vorbereitung auf die Prüfung angeboten werden.

#### [](#020)§ 20  
Rechtsfolge bei endgültig nicht bestandener Aufstiegsprüfung

Die Aufstiegsprüfung ist endgültig nicht bestanden, wenn die Kommissarbewerberinnen oder Kommissarbewerber von der Möglichkeit der Wiederholung der Prüfung gemäß § 19 keinen Gebrauch machen oder die Aufstiegsprüfung auch nach Wiederholung nicht bestanden haben.
Die Kommissarbewerberinnen oder Kommissarbewerber verbleiben in den bisherigen Rechtsstellungen als Beamtin oder Beamter des mittleren Polizeivollzugsdienstes.

### Abschnitt 3  
Schlussvorschriften

#### [](#021)§ 21   
Übergangsregelung

Kommissarbewerberinnen und Kommissarbewerber, die vor Inkrafttreten dieser Verordnung nach der Aufstiegsordnung gehobener Polizeivollzugsdienst vom 16.
Juni 1998 (GVBl.
II S. 460), die durch Artikel 3 der Verordnung vom 9.
September 2011 (GVBl.
II Nr. 52) geändert worden ist, ihre Ausbildung für den Aufstieg in die Laufbahn des gehobenen Polizeivollzugsdienstes begonnen haben, schließen die Ausbildung nach bisherigem Recht ab.