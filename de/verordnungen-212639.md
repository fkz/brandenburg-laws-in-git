## Verordnung über den Schutzwald „Naturwald Heidekrug“

Auf Grund des § 12 Absatz 1 des Waldgesetzes des Landes Brandenburg vom 20.
April 2004(GVBl.
I S. 137) verordnet der Ministerfür Infrastruktur und Landwirtschaft:

§ 1  
Erklärung zum Schutzwald
------------------------------

Die in § 2 näher bezeichneten Waldflächen mit überwiegend besonderer Schutzfunktion als Naturwald werden zum Schutzwald erklärt.
Der Schutzwald trägt die Bezeichnung „Naturwald Heidekrug“ und wird in das Register der geschützten Waldgebiete aufgenommen.

§ 2  
Schutzgegenstand
----------------------

(1) Der Schutzwald befindet sich im Landkreis Märkisch-Oderland und hat eine Größe von rund 26 Hektar.
Er umfasst folgende Flurstücke:

Gemeinde:

Gemarkung:

Flur:

Flurstück:

Prötzel

Prötzel

1

62, 63 und 65 (jeweils teilweise).

(2) Zur Orientierung ist dieser Verordnung eine Kartenskizze über die Lage des Schutzwaldes als Anlage beigefügt.

(3) Die Grenze des Schutzwaldes ist in der „Topografischen Karte zur Verordnung über den Schutzwald ‚Naturwald Heidekrug‘“, Maßstab 1 : 10 000 und in der „Liegenschaftskarte zur Verordnung über den Schutzwald ‚Naturwald Heidekrug‘“, Maßstab 1 : 7 000 mit ununterbrochener Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Maßgeblich ist die Einzeichnung in der Liegenschaftskarte.
Die Karten sind mit dem Dienstsiegel des Ministeriums für Infrastruktur und Landwirtschaft des Landes Brandenburg, Siegelnummer 26, versehen und vom Siegelverwahrer am 17.
Februar 2012 unterschrieben worden.

(4) Die Verordnung mit Karten kann beim Ministerium für Infrastruktur und Landwirtschaft in Potsdam, oberste Forstbehörde, sowie beim Landesbetrieb Forst Brandenburg in Potsdam, untere Forstbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

§ 3  
Schutzzweck
-----------------

(1) Schutzzweck des Schutzwaldes ist

1.  Die Erhaltung des Waldes zum Zwecke der wissenschaftlichen Beobachtung und Erforschung der natürlichen Entwicklung des Traubeneichen-Winterlinden-Hainbuchenwaldes;
2.  die Bewahrung seiner besonderen Eigenart und hervorragenden Schönheit;
3.  die Erhaltung und Wiederherstellung der Leistungsfähigkeit des Naturhaushaltes.

(2) Der besondere Schutzzweck ist der Erhalt des Naturwaldes insbesondere zur

1.  langfristigen wissenschaftlichen Erforschung der durch den Menschen nicht direkt beeinflussten Waldentwicklung;
2.  Erforschung der Waldstruktur, des Bodens, der Flora und der Fauna;
3.  Nutzung als lokale und regionale Weiserfläche zur Ableitung und exemplarischen Veranschaulichung von Erkenntnissen für die Waldbaupraxis und für die forstliche Lehre;
4.  Erhaltung und Regeneration forstgenetischer Ressourcen;
5.  Erhaltung der floristischen und faunistischen Artenvielfalt in sich natürlich entwickelnden Lebensgemeinschaften.

§ 4  
Verbote
-------------

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind im Schutzwald gemäß § 12 Absatz 6 des Waldgesetzes des Landes Brandenburg alle Handlungen verboten, die dem in § 3 genannten Schutzzweck zuwiderlaufen und das Gebiet oder einzelne seiner Bestandteile nachhaltig stören, verändern, beschädigen oder zerstören können.

(2) Es ist insbesondere verboten:

1.  das Gebiet forstwirtschaftlich zu nutzen;
2.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
3.  das Gebiet außerhalb der Waldwege zu betreten;
4.  im Gebiet mit motorisierten Fahrzeugen und Gespannen zu fahren oder diese dort abzustellen;
5.  zu lagern, zu zelten, Wohnwagen aufzustellen, zu rauchen und Feuer zu entzünden;
6.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
7.  die Bodengestalt zu verändern, Böden zu verfestigen und zu versiegeln;
8.  Düngemittel einschließlich Wirtschaftsdünger und Sekundärrohstoffdünger zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
9.  Pflanzenschutzmittel jeder Art oder Holzschutzmittel anzuwenden;
10.  wild lebende Pflanzen oder Teile und Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
11.  Vieh weiden zu lassen und dazu erforderliche Einrichtungen zu schaffen.

§ 5   
Zulässige Handlungen
---------------------------

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen;
2.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Forstbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Schutzwaldes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Forstbehörden und sonstige von den Forstbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer und Nutzer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums.

§ 6  
Ausnahmen, Befreiungen
----------------------------

(1) Aus Gründen des Waldschutzes und zur Nutzung nach Naturereignissen kann die untere Forstbehörde auf Antrag Ausnahmen von den Verboten dieser Verordnung zulassen, sofern der Schutzzweck nicht beeinträchtigt wird.

(2) Wenn überwiegende Gründe des Gemeinwohls es erfordern, kann die untere Forstbehörde auf Antrag Be-freiungen von den Verboten dieser Verordnung gewähren.

(3) Die Beerntung des Saatgutes der Hainbuche (Carpinus betulus L.) im zugelassenen Saatgutbestand mit der Registernummer 12 3 80602 0212 (Forstort 4247 a1, a2 und a5) kann die untere Forstbehörde auf Antrag in Abhängigkeit vom Behang zulassen, mit der Maßgabe, dass die Früchte ausschließlich manuell durch Abstreifen geerntet werden, wobei die Bäume mit Leitern zu besteigen sind oder Seilklettertechnik angewendet wird und der Schutzwald nicht maschinell befahren wird.

§ 7  
Ordnungswidrigkeiten
--------------------------

(1) Ordnungswidrig im Sinne des § 37 Absatz 1 Nummer 7 des Waldgesetzes des Landes Brandenburg handelt, wer vorsätzlich oder fahrlässig den Regelungen der §§ 4 und 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 37 Absatz 3 des Waldgesetzes des Landes Brandenburg mit einer Geldbuße bis zu zwanzigtausend Euro geahndet werden.

§ 8  
Verhältnis zu anderen rechtlichen Vorschriften
----------------------------------------------------

(1) Die Regelungen naturschutzrechtlicher Schutzgebietsausweisungen im Bereich des in § 2 genannten Gebietes bleiben unberührt.

(2) Die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 31 bis 35 des Brandenburgischen Naturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 39 bis 55 des Bundesnaturschutzgesetzes, §§ 37 bis 43 des Brandenburgischen Naturschutzgesetzes) bleiben unberührt.

§ 9  
Geltendmachen von Rechtsmängeln
-------------------------------------

Eine Verletzung der in § 1 der Waldschutzgebietsverfahrensverordnung genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber der obersten Forstbehörde geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsprozess sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

§ 10  
Inkrafttreten
--------------------

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 16.
März 2012

Der Minister für Infrastruktur und Landwirtschaft

Jörg Vogelsänger

**Anlage**  
(zu § 2 Absatz 2)

### **Kartenskizze zur Verordnung über den Schutzwald „Naturwald Heidekrug“**

![Die Anlage zu § 2 Absatz 2 bildet eine Kartenskizze zur Verordnung über den Schutzwald „Naturwald Heidekrug“  im Maßstab 1 : 10 000 ab. Es wird die Grenze des Schutzwaldes mit ununterbrochener Linie dargestellt. Der Schutzwald hat eine Gesamtfläche von rund 26 Hektar. Die südliche Grenze des Schutzwaldes beginnt bei Kilometer 4,68 an der Landstraße 23 im Abschnitt 220 zwischen Gielsdorf und Tiefensee. Sie verläuft etwa 585 Meter entlang einem Gestellweg in Richtung Osten. Die östliche Grenze verläuft etwa 258 Meter in Richtung Norden entlang dem Gestellweg östlich der Forstabteilung 4246. Danach biegt die Grenze auf einer Teilflächengrenze auf einer Länge von etwa 82 Meter in westliche, dann etwa 120 Meter in nördliche Richtung ab. In der Folge verläuft die Grenze 45 Meter in westliche, darauf folgend 95 Meter in nördliche und schließlich wieder 70 Meter in westliche Richtung. Von hier verläuft die nördliche Grenze in nordwestlicher Richtung auf einer Teilflächengrenze bis zur Landesstraße 23, Abschnitt 220, Kilometer 5,24. Dann folgt die westliche Grenze des Schutzwaldes der Landesstraße 23 Richtung Süden zum Ausgangspunkt der Beschreibung bei Kilometer 4,68. ](/br2/sixcms/media.php/69/Nr.%2020-1.JPG "Die Anlage zu § 2 Absatz 2 bildet eine Kartenskizze zur Verordnung über den Schutzwald „Naturwald Heidekrug“  im Maßstab 1 : 10 000 ab. Es wird die Grenze des Schutzwaldes mit ununterbrochener Linie dargestellt. Der Schutzwald hat eine Gesamtfläche von rund 26 Hektar. Die südliche Grenze des Schutzwaldes beginnt bei Kilometer 4,68 an der Landstraße 23 im Abschnitt 220 zwischen Gielsdorf und Tiefensee. Sie verläuft etwa 585 Meter entlang einem Gestellweg in Richtung Osten. Die östliche Grenze verläuft etwa 258 Meter in Richtung Norden entlang dem Gestellweg östlich der Forstabteilung 4246. Danach biegt die Grenze auf einer Teilflächengrenze auf einer Länge von etwa 82 Meter in westliche, dann etwa 120 Meter in nördliche Richtung ab. In der Folge verläuft die Grenze 45 Meter in westliche, darauf folgend 95 Meter in nördliche und schließlich wieder 70 Meter in westliche Richtung. Von hier verläuft die nördliche Grenze in nordwestlicher Richtung auf einer Teilflächengrenze bis zur Landesstraße 23, Abschnitt 220, Kilometer 5,24. Dann folgt die westliche Grenze des Schutzwaldes der Landesstraße 23 Richtung Süden zum Ausgangspunkt der Beschreibung bei Kilometer 4,68. ")