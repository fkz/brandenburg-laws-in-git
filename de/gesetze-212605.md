## Gesetz über die Feststellung des Haushaltsplanes des Landes Brandenburg für das Haushaltsjahr 2012 (Haushaltsgesetz 2012 - HG 2012)

Der Landtag hat das folgende Gesetz beschlossen:

### § 1  
Feststellung des Haushaltsplanes

Der diesem Gesetz als Anlage beigefügte Haushaltsplan des Landes Brandenburg für das Haushaltsjahr 2012 wird in Einnahmen und Ausgaben festgestellt auf 10 191 563 700 Euro.
Der Gesamtbetrag der Verpflichtungsermächtigungen wird festgestellt auf 1 539 178 400 Euro.

### § 2  
Kreditermächtigungen

(1) Das Ministerium der Finanzen wird ermächtigt, zur Deckung von Ausgaben Kredite aufzunehmen bis zur Höhe von 270 000 000 Euro.

(2) Der Kreditermächtigung nach Absatz 1 wachsen die Beträge zur Tilgung von im Haushaltsjahr 2012 fällig werdenden Krediten zu, deren Höhe sich aus den Finanzierungsübersichten ergibt.
Das Ministerium der Finanzen wird ermächtigt, zum Aufbau von Eigenbeständen Kredite bis zur Höhe von 500 000 000 Euro aufzunehmen.
Auf die Kreditermächtigung nach Satz 2 sind die Beträge anzurechnen, die aufgrund von Ermächtigungen früherer Haushaltsgesetze aufgenommen worden sind oder sich bereits im Eigenbestand befinden.

(3) Über die Kreditermächtigung nach Absatz 1 hinaus darf das Ministerium der Finanzen zur Vorfinanzierung von Ausgaben, die aus den Fonds der Europäischen Union nachträglich erstattet werden, Kredite bis zur Höhe von insgesamt 200 000 000 Euro aufnehmen.
Die nach Satz 1 aufgenommenen Kredite sind mit den Erstattungen aus den Fonds zu tilgen.

(4) Im Rahmen der Kreditfinanzierung kann das Ministerium der Finanzen auch ergänzende Vereinbarungen treffen, die der Begrenzung von Zinsänderungsrisiken, der Erzielung günstigerer Konditionen und ähnlichen Zwecken bei neuen Krediten und bestehenden Schulden dienen.
Das Ministerium der Finanzen wird ermächtigt, Darlehen vorzeitig zu tilgen oder Kredite mit unterjähriger Laufzeit aufzunehmen, soweit dies im Zuge von Zinsanpassungen oder zur Erlangung günstigerer Konditionen notwendig wird.
Diese Ermächtigung gilt auch, soweit Geschäfte getätigt werden, deren Einnahmen die Ausgaben für das jeweilige Kreditgeschäft übersteigen.
Die Kreditermächtigung nach Absatz 1 erhöht sich in Höhe der nach Satz 2 getilgten Beträge.
Diese Ermächtigung gilt auch für die im Wirtschaftsplan des Landeswohnungsbauvermögens vorgesehene Kreditaufnahme.

(5) Das Ministerium der Finanzen wird ermächtigt, ab Oktober des Haushaltsjahres im Vorgriff auf die Ermächtigung des nächsten Haushaltsjahres Kredite bis zur Höhe von 8 Prozent des in § 1 Satz 1 festgestellten Betrages aufzunehmen.
Die hiernach aufgenommenen Kredite sind auf die Kreditermächtigung des nächsten Haushaltsjahres anzurechnen.

(6) Der Zeitpunkt der Kreditaufnahme ist nach der Kassenlage, den jeweiligen Kapitalmarktverhältnissen und gesamtwirtschaftlichen Erfordernissen zu bestimmen.

(7) Das Ministerium der Finanzen wird ermächtigt, zur Aufrechterhaltung einer ordnungsgemäßen Kassenwirtschaft im Haushaltsjahr 2012 bis zur Höhe von 12 Prozent des in § 1 Satz 1 festgestellten Betrages zuzüglich der nach Absatz 1 noch nicht in Anspruch genommenen Kreditermächtigungen Kassenverstärkungsmittel aufzunehmen.
Soweit diese Kredite zurückgezahlt sind, kann die Ermächtigung wiederholt in Anspruch genommen werden.

(8) Das Ministerium der Finanzen wird ermächtigt, Sicherheiten in Form verzinster Barmittel zu stellen sowie entgegenzunehmen oder durch Wertpapierhinterlegung zu empfangen oder zu stellen.

### § 3  
Bürgschaften und Rückbürgschaften

(1) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2012 Bürgschaften für Kredite an die Wirtschaft, die freien Berufe sowie die Land- und Forstwirtschaft bis zur Höhe von insgesamt 200 000 000 Euro zu übernehmen.

(2) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2012 Bürgschaften für Kredite zur Förderung des Wohnungsbaus und des Stadtumbaus bis zur Höhe von 5 000 000 Euro zu übernehmen.

(3) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2012 Bürgschaften im Falle eines unvorhergesehenen und unabweisbaren Bedürfnisses, insbesondere für Notmaßnahmen im Land Brandenburg, bis zur Höhe von 15 000 000 Euro zu übernehmen.
Überschreitet die aufgrund dieser Ermächtigung zu übernehmende Bürgschaft im Einzelfall den Betrag von 5 000 000 Euro, bedarf es der Einwilligung des Ausschusses für Haushalt und Finanzen des Landtages.

(4) Bürgschaften gemäß den Absätzen 1 und 2 dürfen nur für Kredite übernommen werden, deren Rückzahlung durch den Schuldner bei normalem wirtschaftlichem Ablauf innerhalb der für den einzelnen Kredit vereinbarten Zahlungstermine erwartet werden kann.

### § 4  
Garantien und sonstige Gewährleistungen

(1) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2012 im Interesse der Kapitalversorgung kleiner und mittelständischer Unternehmen Garantien bis zur Höhe von 20 000 000 Euro für die Übernahme von Kapitalbeteiligungen zu übernehmen.
Diese Garantien können auch als Rückgarantien gegenüber Kreditinstituten übernommen werden.

(2) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2012 Garantien zur Finanzierung von Film- und Fernsehproduktionen sowie Projektentwicklungen im Medienbereich bis zur Höhe von 10 000 000 Euro zu übernehmen.

(3) Das Ministerium der Finanzen wird ermächtigt, unter Anrechnung auf die Ermächtigungen gemäß den Absätzen 1 und 2 Garantien zur Finanzierung von Produktionen, Projektentwicklungen und Existenzgründungen im Bereich der Kultur- und Kreativwirtschaft zu übernehmen.

(4) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2012 zur Absicherung von Risiken, die sich aus dem Betrieb von kerntechnischen Anlagen und dem Umgang mit radioaktiven Stoffen in Forschungseinrichtungen des Landes ergeben, Gewährleistungen bis zur Höhe von 5 000 000 Euro zu übernehmen.

(5) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2012 zur Deckung des Haftpflichtrisikos von Zuwendungsempfängern des Landes aus der Haftung für Leihgaben im Bereich Kunst und Kultur sowie für wissenschaftliche Forschungsinstitute, die vom Bund und vom Land gemeinsam getragen werden, Garantien bis zum Höchstbetrag von 5 000 000 Euro zu übernehmen.

(6) Das Ministerium der Finanzen wird ermächtigt, im Jahr 2012 zur Absicherung von Risiken, die sich aus der Tätigkeit der Ethikkommission der Landesärztekammer Brandenburg nach _§ 7 Absatz 1 des Heilberufsgeset_zes ergeben, Gewährleistungen bis zur Höhe von 5 000 000 Euro zu übernehmen.

(7) Haftungsfreistellungen und Garantien gemäß den Absätzen 1 und 2 dürfen nur unter den in § 3 Absatz 4 genannten Voraussetzungen übernommen werden.

### § 5  
Grundsätze für neue Steuerungsinstrumente

(1) In den Einzelplänen 02 bis 12 werden aus den Personalausgaben je Einzelplan Personalbudgets gebildet.
In den Einzelplänen 02 bis 12 sowie im Einzelplan 20 werden aus den sächlichen Verwaltungsausgaben, den Ausgaben für den Erwerb beweglicher Sachen und den Verwaltungseinnahmen je Einzelplan Verwaltungsbudgets gebildet.
Werden die Ausgaben des Personalbudgets und des Verwaltungsbudgets beim Jahresabschluss unterschritten, kann der Betrag in Höhe der Unterschreitung anteilig einer Rücklage zugeführt werden.
Die Rücklagenbildung erfolgt grundsätzlich in Höhe von 50 Prozent der Unterschreitung.
Das Ministerium der Finanzen kann einen höheren Rücklagensatz bestimmen.
Die Bestimmung eines geringeren Rücklagensatzes ist nur zur Vermeidung oder Begrenzung eines ansonsten entstehenden Fehlbetrages nach § 25 Absatz 1 der Landeshaushaltsordnung zulässig.

(2) Das Personalbudget umfasst mit Ausnahme der Gruppen 432 und 453 die Ausgaben der Hauptgruppe 4.
Diese sind innerhalb des Einzelplans gegenseitig deckungsfähig.
 Rücklagen aus dem Vorjahr dürfen zur Verstärkung der Ausgaben verwendet werden; vorgezogene Entnahmen im Vorjahr sind durch Minderausgaben im laufenden Haushaltsjahr auszugleichen.
Wird das Personalbudget beim Jahresabschluss über- oder unterschritten, kann der Betrag bis zur Höhe der Über- oder Unterschreitung auf das Personalbudget für den nächsten Haushalt vorgetragen werden.

(3) Die Ausgaben der Gruppe 453 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.
Das jeweilige Personalbudget ist einseitig deckungsfähig zugunsten der Ausgaben der Gruppe 453.
Die Ausgaben der Gruppe 432 sind über alle Einzelpläne gegenseitig deckungsfähig.

(4) Das Verwaltungsbudget umfasst die Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Titel 518 25 und der Gruppe 529, und der Obergruppe 81 und die Einnahmen der Obergruppen 11 bis 13.
Die Ausgaben sind innerhalb des Einzelplans gegenseitig deckungsfähig.
Das jeweilige Verwaltungsbudget ist einseitig deckungsfähig zugunsten des Titels 518 25.
Rücklagen aus Vorjahren dürfen zur Verstärkung der Ausgaben verwendet werden.
Wird das Verwaltungsbudget beim Jahresabschluss über- oder unterschritten, kann der Betrag bis zur Höhe der Über- oder Unterschreitung auf das Verwaltungsbudget für den nächsten Haushalt vorgetragen werden.
Einzelne Einnahmen und Ausgaben können vom Verwaltungsbudget ausgenommen werden.

(5) Mehreinnahmen bei den Obergruppen 11 bis 13 können zur Verstärkung der Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Gruppe 529, und der Obergruppe 81 im Rahmen des Verwaltungsbudgets verwendet werden, wenn ein verwaltungsmäßiger oder sachlicher Zusammenhang besteht oder eine wirtschaftliche und sparsame Verwendung gefördert wird.
Minderausgaben beim Personalbudget können zur Verstärkung der Ausgaben des Verwaltungsbudgets im jeweiligen Einzelplan verwendet werden, soweit sich daraus keine Überschreitung des Personalbudgets beim Jahresabschluss ergibt.

(6) Minderausgaben beim Verwaltungsbudget können zur Verstärkung der Ausgaben bei Kapitel 12 020 Titel 891 61 - Zuführungen für Investitionen - herangezogen werden.

(7) Die allein aus Landesmitteln finanzierten und nicht zur Komplementärfinanzierung von Drittmitteln bestimmten Ausgaben der Hauptgruppe 6 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.
Ebenso sind die allein aus Landesmitteln finanzierten und nicht zur Komplementärfinanzierung von Drittmitteln bestimmten Ausgaben der Obergruppen 83 bis 89 innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.

(8) Für die Wirtschaftspläne der Landesbetriebe nach § 26 Absatz 1 der Landeshaushaltsordnung gelten die vorstehenden Absätze entsprechend, soweit keine besonderen Regelungen getroffen sind.

(9) Die im Einzelplan 06 veranschlagten Universitäten und Fachhochschulen werden jeweils nur mit ihrem Zuschussbedarf veranschlagt.
Die Einnahmen und Ausgaben dieser Einrichtungen werden in Wirtschaftsplänen veranschlagt, die dem Haushaltsplan als Erläuterungen beigefügt sind.
Für die Bewirtschaftung gelten die Absätze 1 bis 6 entsprechend, soweit keine besonderen Regelungen getroffen sind.

(10) Das Nähere regelt das Ministerium der Finanzen.

(11) Die Ausgaben des Titels 919 35 sind über alle Einzelpläne gegenseitig deckungsfähig.

### § 6  
Neue Steuerungsinstrumente im Bereich des Landtages, Verfassungsgerichts und Landesrechnungshofes

(1) Gegenseitig deckungsfähig sind innerhalb der Einzelpläne 01, 13 und 14 die Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Titel 518 25 und der Gruppe 529, und der Obergruppe 81.
Das jeweilige Verwaltungsbudget ist einseitig deckungsfähig zugunsten des Titels 518 25.
Werden die Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Gruppe 529, und der Obergruppe 81 beim Jahresabschluss unterschritten, kann der Betrag in Höhe der Unterschreitung einer Rücklage zugeführt werden.
Sofern es beim Jahresabschluss zu einer Überschreitung kommt, kann der Betrag in Höhe der Überschreitung in den nächsten Haushalt vorgetragen werden.
Rücklagen aus dem Vorjahr dürfen zur Verstärkung der entsprechenden Ausgaben verwendet werden.

(2) Nicht verausgabte Mittel der Titelgruppe 99 - Kosten für Datenverarbeitung - können bei Unterschreitung der veranschlagten Ausgaben in Höhe der Unterschreitung einer Rücklage zugeführt werden.
Auf die Bildung dieser Rücklage ist Absatz 1 nicht anzuwenden.
Innerhalb der Titelgruppe 99 dürfen Einnahmen, die der für Datenverarbeitung gebildeten Rücklage entnommen werden, zur Deckung von Mehrausgaben verwendet werden.

(3) Für die Ausgaben der Hauptgruppe 4, mit Ausnahme der Ausgaben der Gruppe 411 - Aufwendungen für Abgeordnete - im Kapitel 01 010 und der Gruppen 432 und 453, wird innerhalb des jeweiligen Einzelplans ein Personalbudget gebildet.
Die Ausgaben sind innerhalb des Personalbudgets gegenseitig deckungsfähig.
Rücklagen aus dem Vorjahr dürfen zur Verstärkung der Ausgaben verwendet werden; vorgezogene Entnahmen im Vorjahr sind durch Minderausgaben im laufenden Haushaltsjahr auszugleichen.
Wird das Personalbudget beim Jahresabschluss über- oder unterschritten, kann der Betrag bis zur Höhe der Über- oder Unterschreitung auf das Personalbudget für den nächsten Haushalt vorgetragen werden.

(4) Die Ausgaben der Gruppe 453 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.
Das jeweilige Personalbudget ist einseitig deckungsfähig zugunsten der Ausgaben der Gruppe 453.
Die Ausgaben der Gruppe 432 sind über alle Einzelpläne gegenseitig deckungsfähig.

(5) Mehreinnahmen bei den Obergruppen 11 bis 13 können zur Verstärkung der Ausgaben der Obergruppen 51 bis 54, ausgenommen die Ausgaben der Gruppe 529, und der Obergruppe 81 verwendet werden, wenn ein verwaltungsmäßiger oder sachlicher Zusammenhang besteht oder eine wirtschaftliche und sparsame Verwendung gefördert wird.
Minderausgaben beim Personalbudget können zur Verstärkung der in Satz 1 bezeichneten Ausgaben im jeweiligen Einzelplan verwendet werden, soweit sich daraus keine Überschreitung des Personalbudgets beim Jahresabschluss ergibt.

### § 7  
Besondere Regelungen für den Brandenburgischen Landesbetrieb für Liegenschaften und Bauen (BLB)

(1) Das Ministerium der Finanzen wird ermächtigt, nach Bestätigung des Wirtschaftsplans für den Landesbetrieb Einnahmen, Ausgaben, Verpflichtungsermächtigungen, Planstellen und Stellen in den Landesbetrieb umzusetzen, soweit weitere Liegenschaften in die Teilnahme am Vermieter-Mieter-Modell überführt werden.

(2) Die Ansätze bei den Titeln 518 25 sind bis zum Abschluss der jeweiligen Mietverträge mit dem BLB gesperrt.
Von dieser Sperre sind Ausgaben nicht erfasst, die im Zusammenhang mit der Bewirtschaftung der Liegenschaften stehen.

(3) Nicht veranschlagte Ausgaben für Mieten nach dem Vermieter-Mieter-Modell beim Titel 518 25 stellen keine Mehrausgaben nach § 37 der Landeshaushaltsordnung dar.
Sie können vom Ministerium der Finanzen zugelassen werden, wenn sie durch Minderausgaben oder Mehreinnahmen an anderer Stelle gedeckt sind.

(4) Die Ansätze des Titels 518 25 sind innerhalb des jeweiligen Einzelplans gegenseitig deckungsfähig.

(5) Vom BLB zurückgezahlte Beträge aus der Abrechnung von Betriebs- und Nebenkosten sind bei Titel 518 25 und bei Kapitel 12 020 bei Titel 518 61 abzusetzen.

### § 8  
Mehrausgaben, Komplementärmittel

(1) Der gemäß § 37 Absatz 1 Satz 4 der Landeshaushaltsordnung festzulegende Betrag wird auf 7 500 000 Euro Landesmittel festgesetzt, für Verpflichtungsermächtigungen (§ 38 Absatz 1 Satz 3 der Landeshaushaltsordnung) als Jahresbetrag.
 Überschreiten Mehrausgaben im Einzelfall den Betrag von 5 000 000 Euro Landesmittel, bei Verpflichtungsermächtigungen als jährlich fällig werdender Betrag, ist die Einwilligung des Ausschusses für Haushalt und Finanzen des Landtages einzuholen.

(2) Eines Nachtragshaushaltsgesetzes bedarf es zudem nicht, wenn

1.  Komplementärmittel von der Europäischen Union oder vom Bund unvorhergesehen bereitgestellt werden, die eine zusätzliche anteilige Finanzierung durch das Land erforderlich machen, oder
2.  Umschichtungen innerhalb eines Fonds der Europäischen Union oder zwischen den Fonds, einschließlich der Kofinanzierung durch das Land, erforderlich sind.

In den Fällen des Satzes 1 Nummer 2 bedarf es der Einwilligung des Ausschusses für Haushalt und Finanzen, wenn die Umschichtungen im Einzelfall 5 000 000 Euro EU- und Landesmittel, bei Verpflichtungsermächtigungen als jährlich fällig werdender Betrag, überschreiten.

(3) Veranschlagte Landesmittel und Verpflichtungsermächtigungen, die nicht mehr zur Kofinanzierung von Leistungen Dritter für die gemäß Haushaltsplan vorgesehenen Zwecke erforderlich sind, sind gesperrt.
Die Aufhebung der Sperre bedarf der Zustimmung des Ministeriums der Finanzen.
Das Ministerium der Finanzen wird ermächtigt, die Vorfinanzierung von Maßnahmen, für die die Leistung von Dritten vorgesehen ist, zuzulassen.

(4) Im Bereich der Fonds der Europäischen Union dürfen mit Einwilligung des Ministeriums der Finanzen Mehrausgaben bis zur Höhe der Minderausgaben aus Vorjahren geleistet werden, soweit die zugehörigen Erstattungsanträge an die EU-Kommission bis spätestens zum II.
Quartal des Folgejahres gestellt werden oder die Mehrausgaben zur Kofinanzierung von Mitteln aus den Fonds dienen.

### § 9  
Sonderfinanzierungen

(1) Durch den Abschluss von Leasing-, Mietkauf- und ähnlichen Verträgen (Sonderfinanzierungen) für Bauinvestitionen dürfen Verpflichtungen zulasten künftiger Haushaltsjahre eingegangen werden.
Diese Befugnis gilt auch bei Umsetzung von Bauinvestitionen im Rahmen von Öffentlich Privaten Partnerschaften, die auch die Betriebsphase umfassen (Lebenszyklusansatz).
Das Ministerium der Finanzen wird ermächtigt, mit Zustimmung des Ausschusses für Haushalt und Finanzen des Landtages Sonderfinanzierungen zuzulassen; § 38 Absatz 1 der Landeshaushaltsordnung bleibt unberührt.

(2) Verpflichtungsermächtigungen für Investitionsfinanzierungen dürfen abweichend von § 8 Absatz 1 bis zu der Höhe überschritten werden, in der sie für Maßnahmen nach Absatz 1 Satz 1 benötigt werden.

(3) Die Wirtschaftlichkeit von Sonderfinanzierungen ist in jedem Einzelfall zu belegen.

### § 10  
Industrieansiedlungsverträge

Soweit die veranschlagten Ausgaben bei voller Ausschöpfung der Deckungsfähigkeit und die Verpflichtungsermächtigungen nicht ausreichen, Industrieansiedlungsverträge mit finanziellen Verpflichtungen für das Land abzuschließen, ist das Ministerium für Wirtschaft und Europaangelegenheiten ermächtigt, über Industrieansiedlungsverträge zu verhandeln und - bei Zustimmung des Ministeriums der Finanzen und nach Einwilligung des Ausschusses für Haushalt und Finanzen im Benehmen mit dem Ausschuss für Wirtschaft des Landtages - zusätzliche Verpflichtungen zulasten des Landes einzugehen.

### § 11  
Besondere Regelungen für Zuwendungen

(1) Ausgaben und Verpflichtungsermächtigungen für Zuwendungen im Sinne des § 23 der Landeshaushaltsordnung zur Deckung der gesamten Ausgaben oder eines nicht abgegrenzten Teils der Ausgaben einer Stelle außerhalb der Landesverwaltung (institutionelle Förderung) sind gesperrt, bis der Haushalts- oder Wirtschaftsplan des Zuwendungsempfängers von dem zuständigen Ministerium gebilligt worden ist.

(2) Die in Absatz 1 genannten Zuwendungen zur institutionellen Förderung dürfen nur mit der Auflage bewilligt werden, dass der Zuwendungsempfänger seine Beschäftigten nicht besser stellt als vergleichbare Bedienstete des Landes; vorbehaltlich einer abweichenden tarifvertraglichen Regelung dürfen deshalb keine günstigeren Arbeitsbedingungen vereinbart werden, als sie für Bedienstete des Landes jeweils vorgesehen sind.
Entsprechendes gilt bei Zuwendungen zur Projektförderung, wenn die Gesamtausgaben des Zuwendungsempfängers überwiegend aus Zuwendungen der öffentlichen Hand bestritten werden.
Das Ministerium der Finanzen kann bei Vorliegen zwingender Gründe Ausnahmen zulassen.

(3) Die in den Erläuterungen zu den Titeln, aus denen Zuwendungen im Sinne des § 23 der Landeshaushaltsordnung zur institutionellen Förderung geleistet werden, für andere als Projektaufgaben ausgebrachte Planstellen für Beamte sowie Stellen für Arbeitnehmer sind hinsichtlich der Gesamtzahl und der Zahl der für die einzelnen Besoldungs- und Entgeltgruppen ausgebrachten Planstellen und Stellen verbindlich.
Das Ministerium der Finanzen wird ermächtigt, Ausnahmen von der Verbindlichkeit der Stellenpläne zuzulassen.
Die Wertigkeit außertariflicher Stellen ist durch die Angabe der entsprechenden Besoldungsgruppe zu kennzeichnen.
Das Ministerium der Finanzen kann Abweichungen in den Wertigkeiten der Stellen zulassen.
Sind im Wirtschaftsplan Stellen außerhalb der Anlagen B 2 und B 3 zum Tarifvertrag der Länder (TV-L) ohne Angaben des Entgelts ausgebracht, bedarf die Festsetzung des Entgelts in jedem Einzelfall der vorherigen Zustimmung des Ministeriums der Finanzen.
Sonstige Abweichungen bedürfen der Einwilligung des Ministeriums der Finanzen und setzen eine Tätigkeitsdarstellung voraus.

### § 12  
Personalwirtschaftliche Regelungen

(1) Zur Einhaltung des Stellenplans gemäß der gültigen Personalbedarfsplanung des Landes Brandenburg und des Personalbudgets sind die Ressorts verpflichtet, alle Möglichkeiten zur Einsparung von Planstellen, Stellen, Beschäftigungspositionen und Personalausgaben zu nutzen.
Dazu können abweichend von § 50 Absatz 1 Satz 1 der Landeshaushaltsordnung auch Mittel oder Planstellen und Stellen umgesetzt werden, ohne dass Aufgaben von einer Verwaltung auf eine andere Verwaltung übergehen.
Das Nähere regelt das Ministerium der Finanzen.

(2) Die Erläuterungen zu den Titeln der Gruppe 422 für Stellen der Beamten auf Probe bis zur Anstellung und zu den Titeln der Gruppe 428 sind hinsichtlich der zulässigen Zahl der für die einzelnen Besoldungs- und Entgeltgruppen ausgebrachten Stellen verbindlich.
Die den Wirtschaftsplänen der Landesbetriebe nach § 26 Absatz 1 der Landeshaushaltsordnung beigefügten Stellenübersichten sind verbindlich.
Das Ministerium der Finanzen kann Ausnahmen von der Verbindlichkeit der Stellenpläne für die Landesbetriebe zulassen.

(3) Abweichend von § 49 der Landeshaushaltsordnung können auf Planstellen auch beamtete Hilfskräfte und Arbeitnehmer geführt werden.

(4) Einnahmen aus Zuschüssen für die berufliche Eingliederung behinderter Menschen und für Arbeitsbeschaffungsmaßnahmen fließen den entsprechenden Ansätzen für Personalausgaben zu.
Innerhalb der einzelnen Kapitel fließen die Einnahmen den Ausgaben bei folgenden Titeln - einschließlich den entsprechenden Titeln in Titelgruppen - zu:

1.  Gruppe 428 aus Erstattungen der Förderleistungen der Bundesagentur für Arbeit in Bezug auf das Altersteilzeitgesetz,
2.  Gruppen 422, 428, 441, 443 und 446 aus Schadensersatzleistungen Dritter.

(5) Planstellen und Stellen können für Zeiträume, in denen Stelleninhaber vorübergehend nicht oder nicht vollbeschäftigt sind, innerhalb des jeweiligen Einzelplans im Umfang der nicht in Anspruch genommenen Planstellen- oder Stellenanteile für die Beschäftigung von beamteten Hilfskräften und Kräften in zeitlich befristeten Arbeitsverträgen in Anspruch genommen werden.

(6) Das Ministerium der Finanzen wird ermächtigt, Planstellen für Lehrkräfte zur Besetzung mit Beamten, für die die Einstufung nach den Brandenburgischen Besoldungsordnungen nicht gilt, nach Maßgabe des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung zu heben.

### § 13  
Besondere Regelungen für Planstellen und Stellen

(1) Planstellen und Stellen, die einen kw-Vermerk tragen, können nach ihrem Freiwerden mit schwer behinderten Menschen wiederbesetzt werden, wenn die gesetzliche Pflichtquote gemäß § 71 Absatz 1 des Neunten Buches Sozialgesetzbuch bei den Planstellen und Stellen in der Landesverwaltung nicht erreicht wird.
In diesem Fall ist der schwer behinderte Mensch auf der nächsten freiwerdenden Planstelle oder Stelle der betreffenden oder nächsthöheren Besoldungs- oder Entgeltgruppe innerhalb des Einzelplans zu führen.
Das Ministerium der Finanzen kann Ausnahmen zulassen.

(2) Das Ministerium der Finanzen wird ermächtigt zuzulassen, dass von einem kw-Vermerk mit Datumsangabe abgewichen wird, wenn die Planstelle oder Stelle weiter benötigt wird, weil sie nicht rechtzeitig frei wird; in diesem Fall ist der Stelleninhaber auf der nächsten freiwerdenden Planstelle oder Stelle der betreffenden oder nächsthöheren Besoldungs- oder Entgeltgruppe innerhalb des Einzelplans zu führen.

(3) Das Ministerium der Finanzen wird ermächtigt, mit Einwilligung des Ausschusses für Haushalt und Finanzen des Landtages Planstellen für Beamte, Richter und Stellen für Arbeitnehmer zusätzlich auszubringen, wenn hierfür ein unabweisbares, auf andere Weise nicht zu befriedigendes Bedürfnis besteht.

(4) Mit Einwilligung des Ministeriums der Finanzen können nach Änderungen im Besoldungs- oder Tarifrecht Planstellen- und Stellenveränderungen vorgenommen werden.
 Stellenveränderungen sind mit Einwilligung des Ministeriums der Finanzen auch dann möglich, wenn tarifrechtliche Ansprüche bestehen.

(5) Arbeitnehmer, die vor der Überleitung aus dem BAT/BAT-O und dem MTArb/MTArb-O in den TV-L einen Bewährungs- oder Fallgruppenaufstieg gemäß den §§ 23a, 23b BAT/BAT-O beziehungsweise den vergleichbaren Bestimmungen für Arbeiter vollzogen haben oder bei denen nach den bisherigen tarifrechtlichen Bestimmungen ein Bewährungs- oder Fallgruppenaufstieg in der jeweiligen Fallgruppe vorgesehen war, sowie nach dem 1.
November 2006 neu eingestellte oder neu eingruppierte Arbeitnehmer mit einem höherwertigen Tarifanspruch gemäß Anlage 4 TVÜ-Länder können bis zum Wirksamwerden neuer Eingruppierungsvorschriften für den TV-L oder bis zum Ausscheiden auf einer niedrigwertigeren TV-L-Stelle geführt werden, die der ursprünglichen Stelle in der Struktur des durch den TV-L ersetzten BAT/BAT-O und des MTArb/MTArb-O entspricht.

(6) Das Nähere regelt das Ministerium der Finanzen.

### § 14  
Ausbringung zusätzlicher Leerstellen

(1) Werden planmäßige Beamte, Richter und Arbeitnehmer im dienstlichen Interesse des Landes mit Zustimmung der obersten Dienstbehörde im Dienst einer öffentlichen zwischenstaatlichen oder überstaatlichen Einrichtung, einer Bundesbehörde oder einer kommunalen Gebietskörperschaft oder für eine Tätigkeit bei einer Fraktion oder einer Gruppe des Landtages, des Deutschen Bundestages oder einer zwischenstaatlichen Einrichtung unter Wegfall der Dienstbezüge länger als ein Jahr verwendet und besteht ein unabweisbares Bedürfnis, die Planstellen und Stellen neu zu besetzen, so kann das Ministerium der Finanzen dafür gleichwertige Leerstellen ausbringen.
Das Gleiche gilt für eine Verwendung bei sonstigen landesunmittelbaren und -mittelbaren juristischen Personen des öffentlichen Rechts sowie bei juristischen Personen des Privatrechts, soweit diese vom Land institutionell gefördert werden oder das Land mehrheitlich beteiligt ist.

(2) Absatz 1 findet entsprechend Anwendung, wenn Beamte nach § 80 Absatz 1 Satz 1 Nummer 2 des Landesbeamtengesetzes länger als ein Jahr beurlaubt werden oder wenn die Rechte und Pflichten aus dem Dienstverhältnis nach § 72 Satz 1 des Landesbeamtengesetzes ruhen oder wenn sie aus sonstigen persönlichen Gründen länger als ein Jahr beurlaubt werden.

(3) Für planmäßige Beamte außerhalb der Schulkapitel, die nach § 71 des Landesbeamtengesetzes länger als ein Jahr ohne Unterbrechung Elternzeit nehmen, gilt vom Beginn der Beurlaubung an eine Leerstelle der entsprechenden Besoldungsgruppe als ausgebracht.
Satz 1 gilt auch für die Beurlaubung von Richtern aus familiären Gründen gemäß § 4 Absatz 1 Nummer 2 des Brandenburgischen Richtergesetzes.

(4) Die Absätze 2 und 3 gelten entsprechend für Richter und Arbeitnehmer.

(5) Für planmäßige Beamte, Richter und Arbeitnehmer, die im Rahmen der Umsetzung der Altersteilzeitregelung am Blockmodell teilnehmen, gilt vom Beginn der Freistellungsphase an eine Leerstelle der entsprechenden Besoldungs- und Entgeltgruppe als ausgebracht.
Zum Zeitpunkt des Übergangs in den Ruhestand fällt diese Leerstelle weg.
Diese Beschäftigten sind bis zum Ausscheiden auf diesen Leerstellen zu führen.

(6) Über den weiteren Verbleib der nach den Absätzen 1 bis 5 ausgebrachten Leerstellen ist im nächsten Haushaltsplan zu entscheiden.

### § 15  
Vergabe leistungsbezogener Besoldungselemente an Beamte

(1) Für die Vergabe von Leistungsstufen ist die Brandenburgische Leistungsstufenverordnung sowie für die Vergabe von Leistungsprämien und Leistungszulagen die Brandenburgische Leistungsprämien- und -zulagenverordnung anzuwenden.

(2) Innerhalb eines Kapitels dürfen Zulagen für eine befristete Übertragung einer herausgehobenen Funktion nach § 45 des Bundesbesoldungsgesetzes für Beamte bis zur Höhe von 0,1 Prozent der Ausgaben der Titel 422 10 geleistet werden.
In den Einzelplänen 02 bis 12 dürfen die Zulagen nur im Einvernehmen mit dem Ministerium der Finanzen gewährt werden.
Das Ministerium der Finanzen kann hinsichtlich der Ausgabenhöhe in Satz 1 Ausnahmen zulassen.

(3) Die für die Vergabe leistungsbezogener Besoldungselemente anfallenden Ausgaben sind aus Einsparungen bei anderen Titeln der Hauptgruppe 4 im jeweiligen Einzelplan (ausgenommen Gruppen 432 und 453) oder durch Entnahmen aus der Rücklage Personalbudget zu decken.

### § 16  
Verbilligte Veräußerung und Nutzungsüberlassung von Grundstücken

(1) Grundstücke des Allgemeinen Grundvermögens dürfen gemäß § 63 Absatz 3 Satz 2 und Absatz 4 der Landeshaushaltsordnung

1.  bei der Nutzungsbindung von mindestens 15 Jahren für Einrichtungen des Sozial-, Kinder- und Jugendwesens in gemeinnütziger Trägerschaft um bis zu 25 Prozent unter dem vollen Wert veräußert werden;
2.  bebaut (mit besonderem Sanierungsaufwand) und unbebaut bei einer Belegungsbindung von mindestens 15 Jahren um bis zu 40 Prozent unter dem vollen Wert veräußert werden, wenn sichergestellt ist, dass sie für Maßnahmen der sozialen Wohnraumförderung nach § 2 des Wohnraumförderungsgesetzes verwendet werden;
3.  bei einer Belegungsbindung von mindestens 15 Jahren um bis zu 50 Prozent unter dem vollen Wert veräußert werden, wenn sichergestellt ist, dass sie im Rahmen des vom Land geförderten Studentenwohnraumbaus zur Schaffung von Studentenwohnungen oder einer vergleichbaren Förderung verwendet werden.
    Unter den gleichen Voraussetzungen können bebaute und unbebaute Grundstücke an Studentenwerke unentgeltlich abgegeben werden;
4.  im Wege der Bestellung eines Erbbaurechts vergeben werden, wobei der Erbbauzins je nach dem zu fördernden Zweck für die Dauer der Nutzungs- und Belegungsbindung abgesenkt werden darf, und zwar
    1.  für die gemeinnützigen außeruniversitären Forschungseinrichtungen auf 0 Prozent, wobei der Erbbauzins nach Ablauf von jeweils zehn Jahren um jeweils 1 Prozent erhöht werden kann,
    2.  in den Fällen der Nummer 1 auf 2 Prozent,
    3.  in den Fällen der Nummer 2 auf 3 Prozent und
    4.  in den Fällen von Nummer 3 Satz 2 auf 0 Prozent für die ersten zehn Jahre, 1 Prozent für die folgenden zehn Jahre und so fortlaufend bis zu 3 Prozent nach 30 Jahren ausgehend vom Bodenwert.
        In den Fällen von Nummer 3 Satz 1 auf 3 Prozent vom Bodenwert;
5.  vom Land institutionell geförderten außeruniversitären Forschungseinrichtungen gegen Übernahme der Betriebs- und zumutbaren Bauunterhaltungskosten unentgeltlich zur Nutzung überlassen werden.

(2) Für die nach dem Gesetz über die Verwertung der Liegenschaften der Westgruppe der Truppen in der Titelgruppe 65 „WGT-Liegenschaftsvermögen im AGV“ im Kapitel 20 630 ausgewiesene Vermögensmasse gilt über die Regelung des Absatzes 1 hinaus, dass bebaute und unbebaute Grundstücke um bis zu 25 Prozent unter dem vollen Wert veräußert oder im Erbbaurecht vergeben werden dürfen, die für unmittelbare Verwaltungszwecke vom Land sowie für kommunale Infrastrukturmaßnahmen von den Kreisen und den Gemeinden dauerhaft genutzt werden können.

(3) Über die Verbilligungen gemäß Absatz 1 hinaus wird gemäß § 63 Absatz 3 Satz 2 und Absatz 4 der Landeshaushaltsordnung zugelassen, dass landeseigene bebaute und unbebaute Grundstücke an Gebietskörperschaften für die im Bundeshaushalt aufgeführten Zwecke bis zu dem Prozentsatz unter dem vollen Wert veräußert, im Wege der Erbbaurechtsbestellung zur Verfügung gestellt, vermietet, verpachtet oder zur Nutzung überlassen werden, zu dem der Bund dem Land Verbilligungen bei der Veräußerung, Zurverfügungstellung im Wege des Erbbaurechts, Vermietung, Verpachtung oder Nutzungsüberlassung von bundeseigenen Grundstücken für gleiche Zwecke einräumt.
Vom Gegenseitigkeitserfordernis nach Satz 1 sind die Liegenschaften, die in der Titelgruppe 65 „WGT-Liegenschaftsvermögen im AGV“ im Kapitel 20 630 ausgewiesen sind, ausgenommen.

(4) Gemäß § 61 Absatz 1 Satz 1, § 63 Absatz 3 Satz 2 und Absatz 4 der Landeshaushaltsordnung wird die vorübergehende oder dauernde Abgabe von Grundstücken des Allgemeinen Grundvermögens an das Verwaltungsgrundvermögen ohne Werterstattung zugelassen; dies gilt nicht für Grundstücke, die zur in der Titelgruppe 65 „WGT-Liegenschaftsvermögen im AGV“ im Kapitel 20 630 ausgewiesenen Vermögensmasse gehören.

### § 17  
Besondere Regelungen für geheim zu haltende Ausgaben

(1) Aus zwingenden Gründen des Geheimschutzes wird die Bewilligung von Ausgaben, die nach einem geheim zu haltenden Wirtschaftsplan bewirtschaftet werden sollen, von der Billigung des Wirtschaftsplans durch die Parlamentarische Kontrollkommission nach § 23 des Brandenburgischen Verfassungsschutzgesetzes abhängig gemacht.
Die Mitglieder dieser Kontrollkommission sind zur Geheimhaltung aller Angelegenheiten verpflichtet, die ihnen bei dieser Tätigkeit bekannt geworden sind.

(2) Der Präsident des Landesrechnungshofes prüft in den Fällen des Absatzes 1 nach § 9 des Landesrechnungshofgesetzes und unterrichtet die Parlamentarische Kontrollkommission sowie die zuständige oberste Landesbehörde und das Ministerium der Finanzen über das Ergebnis ihrer Prüfung der Jahresrechnung sowie der Haushalts- und Wirtschaftsführung.
§ 97 Absatz 4 der Landeshaushaltsordnung bleibt unberührt.

### § 18  
Berichtspflichten gegenüber dem Ausschuss für Haushalt und Finanzen des Landtages

(1) Das Ministerium der Finanzen berichtet dem Ausschuss für Haushalt und Finanzen des Landtages

1.  mit Stand 30.
    Juni 2012 im Rahmen eines Berichtes über wesentliche Kenngrößen der bereinigten Gesamteinnahmen und -ausgaben des Landes sowie über den aktuellen Mittelabfluss aus dem Landeshaushalt.
    In diesem Bericht sollen auch Angaben zur Entwicklung der Einnahmearten und der Ausgabearten insbesondere zur Umsetzung der EU-Fonds und zum Stand der Verschuldung sowie Prognosedaten der weiteren Entwicklung bis zum Jahresende enthalten sein;
2.  über den Jahresabschluss 2012 im Rahmen eines Berichtes wie in Nummer 1 allerdings ohne Prognoseaussage;
3.  mit Stand 31.
    Dezember 2012 bis zum 31.
    März 2013 über die Gewährung und Inanspruchnahme von Bürgschaften, Rückbürgschaften, Garantien und sonstigen Gewährleistungen durch das Land gemäß den §§ 3 und 4;
4.  mit Stand 31.
    Dezember 2012 über die nach § 2 Absatz 4 des Haushaltsgesetzes 2012 abgeschlossenen Optimierungsgeschäfte.
    Der Bericht enthält eine Risikobewertung und eine Darstellung der anfallenden Kosten für das Land.

(2) Die Ministerien berichten dem Ausschuss für Haushalt und Finanzen des Landtages

1.  zu den in Absatz 1 Nummer 1 und 2 genannten Stichtagen im Rahmen eines Berichtes über den Stand der Bewilligungen bei sämtlichen Titeln der Hauptgruppen 6 und 8 mit einem Ansatz ab 1 000 000 Euro und den aktuellen Mittelabfluss,
2.  zu den in Absatz 1 Nummer 1 und 2 genannten Stichtagen im Rahmen eines Berichtes über die Inanspruchnahme von Verpflichtungsermächtigungen,
3.  zu den in Absatz 1 Nummer 1 und 2 genannten Stichtagen im Rahmen eines Berichtes über die Inanspruchnahme von Ausgaberesten bei sämtlichen Titeln der Hauptgruppen 6 und 8 mit einem Ansatz ab 1 000 000 Euro,
4.  mit Stand 31.
    Mai 2012 im Rahmen eines Berichtes über die Besetzung der Planstellen und Stellen.

(3) Die Ministerien berichten dem Ausschuss für Haushalt und Finanzen des Landtages

1.  mit Stand 30.
    Juni 2012 zum 1.
    August 2012 im Rahmen eines Berichtes über den Stand der Entgeltzahlungen an die Investitionsbank des Landes Brandenburg im Zusammenhang mit der Wahrnehmung der Geschäftsbesorgung für die Bewilligung, Gewährung von Zuwendungen und zur Verwendungsnachweisprüfung,
2.  mit Stand 31.
    Dezember 2012 zum 1.
    Februar 2013 im Rahmen eines Berichtes wie in Nummer 1.

(4) Das Ministerium für Wirtschaft und Europaangelegenheiten berichtet dem Ausschuss für Haushalt und Finanzen des Landtages

1.  zum 30.
    Juni 2012 im Rahmen eines Berichtes über den Stand der Bewilligung von Fördermitteln aus der Gemeinschaftsaufgabe "Verbesserung der regionalen Wirtschaftsstruktur".
    Der Bericht erfolgt in Form einer Übersicht der bewilligten Einzelförderungen mit einem Förderbetrag von mehr als 1 000 000 Euro.
    In der Übersicht sind die der Bewilligung zugrunde gelegten Kriterien und der Fördersatz anzugeben;
2.  zum 30.
    September 2012 im Rahmen eines Berichtes wie in Nummer 1;
3.  zum 31.
    Dezember 2012 im Rahmen eines Berichtes wie in Nummer 1.

### § 19  
Weitergeltung von Vorschriften und Ermächtigungen

Die Vorschriften und Ermächtigungen in den §§ 3, 4, 5, 6, 8 Absatz 1 und 2, §§ 11 bis 15 und 17 gelten bis zur Verkündung des Haushaltsgesetzes 2013 weiter.

### § 20  
Inkrafttreten

Dieses Gesetz tritt am 1.
 Januar 2012 in Kraft.

Potsdam, den 19.
Dezember 2011

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

 

Anlage 
-------

### Haushaltsplan des Landes Brandenburg für das Haushaltsjahr 2012

### Gesamtplan

I.             Haushaltsübersicht                                                (§ 13 Absatz 4 Nummer 1 LHO)

A.            Zusammenfassung der Einnahmen und Ausgaben je Einzelplan

B.            Zusammenfassung der Verpflichtungsermächtigungen je Einzelplan

II.            Finanzierungsübersicht                                          (§ 13 Absatz 4 Nummer 2 LHO)

III.           Kreditfinanzierungsplan                                         (§ 13 Absatz 4 Nummer 3 LHO)

 

### Teil I Haushaltsübersicht 2012

A.
Zusammenfassung der Einnahmen und Ausgaben der Einzelpläne

 

Einnahmen

Ausgaben

 

0

1

2

3

 

4

Einzelplan / Ressort

Einnahmen aus  
Steuern und  
steuerähnlichen  
Abgaben

Verwaltungseinnahmen,  
Einnahmen aus  
Schuldendienst  
und dgl.

Einnahmen aus  
Zuweisungen  
und Zuschüssen  
mit Ausnahme  
für  
Investitionen

Einnahmen aus  
Schuldenaufnahmen, aus  
Zuweisungen und Zuschüssen  
für Investitionen,  
besondere  
Finanzierungseinnahmen

Summe Einnahmen

Personalausgaben

 

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

**1**

**2**

**3**

**4**

**5**

**6**

**7**

**01**

 

18.500

 

 

**18.500**

22.609.900

**02**

 

13.600

83.700

 

**97.300**

11.290.600

**03**

 

46.285.900

3.190.000

 

**49.475.900**

439.684.700

**04**

 

108.731.300

25.141.800

1.600.000

**135.473.100**

251.457.900

**05**

 

2.615.100

24.223.700

26.874.000

**53.712.800**

1.006.094.000

**06**

 

6.869.800

125.378.100

71.319.300

**203.567.200**

33.364.700

**07**

 

14.981.100

116.663.500

22.449.800

**154.094.400**

47.973.100

**08**

 

16.676.400

2.061.400

315.164.000

**333.901.800**

24.485.300

**10**

 

35.122.800

685.600

55.364.500

**91.172.900**

80.093.400

**11**

1.060.000

10.708.800

538.035.000

300.736.800

**850.540.600**

64.331.400

**12**

 

21.371.400

18.940.900

10.348.700

**50.661.000**

176.034.700

**13**

 

24.000

 

89.000

**113.000**

10.484.800

**14**

 

 

 

 

 

384.400

**20**

5.688.500.000

100.866.300

2.186.138.900

293.230.000

**8.268.735.200**

99.231.500

**Summe 2012**

**5.689.560.000**

**364.285.000**

**3.040.542.600**

**1.097.176.100**

**10.191.563.700**

**2.267.520.400**

**Summe 2011**

**5.133.164.600**

**345.378.300**

**3.092.734.500**

**1.568.709.700**

**10.139.987.100**

**2.232.582.900**

Vgl.
zu 2011

+556.395.400

+18.906.700

\-52.191.900

\-471.533.600

+51.576.600

+34.937.500

 

### Teil I Haushaltsübersicht 2012

A.
Zusammenfassung der Einnahmen und Ausgaben der Einzelpläne

 

Ausgaben

 

 

5

6

7

8

9

 

 

Einzelplan / Ressort

Sächliche Verwaltungs-  
ausgaben und Ausgaben  
für den Schuldendienst

Ausgaben für  
Zuweisungen  
und Zuschüsse  
mit Ausnahme  
für Investitionen

Baumaßnahmen

Sonstige Ausgaben für  
Investitionen und  
Investitions-  
förderungs-  
maßnahmen

Besondere  
Finanzierungsausgaben

Summe  
Ausgaben

\+ Überschuss  
\- Zuschuss

 

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

\- EUR -

1

8

9

10

11

12

13

14

**01**

3.397.100

7.050.500

 

680.000

163.100

**33.900.600**

\-33.882.100

**02**

3.127.200

451.300

 

51.400

 

**14.920.500**

\-14.823.200

**03**

145.350.400

20.250.600

8.867.100

37.689.300

4.460.300

**656.302.400**

\-606.826.500

**04**

149.824.700

31.640.700

91.000

3.720.400

2.685.000

**439.419.700**

\-303.946.600

**05**

12.809.000

433.172.500

 

11.309.200

17.495.000

**1.480.879.700**

\-1.427.166.900

**06**

8.554.600

594.298.700

 

83.928.400

\-7.236.400

**712.910.000**

\-509.342.800

**07**

10.584.500

588.437.200

 

6.167.000

17.977.700

**671.139.500**

\-517.045.100

**08**

16.610.200

125.537.400

700.000

321.344.100

\-4.804.200

**483.872.800**

\-149.971.000

**10**

42.105.300

81.197.300

12.298.000

155.091.000

\-14.797.900

**355.987.100**

\-264.814.200

**11**

41.748.400

783.342.500

53.745.000

403.855.800

\-14.593.600

**1.332.429.500**

\-481.888.900

**12**

37.449.900

34.710.600

 

105.815.000

1.018.600

**355.028.800**

\-304.367.800

**13**

1.249.700

2.000

 

156.000

46.300

**11.938.800**

\-11.825.800

**14**

246.600

 

 

 

 

**631.000**

\-631.000

**20**

728.286.200

2.515.483.000

30.500.000

254.668.600

14.034.000

**3.642.203.300**

+4.626.531.900

**Summe 2012**

**1.201.343.800**

**5.215.574.300**

**106.201.100**

**1.384.476.200**

**16.447.900**

**10.191.563.700**

**0**

**Summe 2011**

**1.268.423.200**

**4.886.677.000**

**51.016.600**

**1.653.882.800**

**47.404.600**

**10.139.987.100**

**0**

Vgl.
zu 2011

\-67.079.400

+328.897.300

+55.184.500

\-269.406.600

\-30.956.700

+51.576.600

0

### Teil I Haushaltsübersicht 2012

B.
Zusammenfassung der Verpflichtungsermächtigungen der Einzelpläne und deren Inanspruchnahme

 

 

 

 

 

 

 

Einzelplan

Bezeichnung

Verpflichtungsermächtigungen

durch die Verpflichtungsermächtigung  
entstehende Rechtsverpflichtungen

 

 

2012

2013

2014

2015

2016 ff.

 

 

1.000 EUR

1

2

3

4

5

6

7

**01**

Landtag

 

 

 

 

 

**02**

Ministerpräsident und Staatskanzlei

100,0

100,0

 

 

 

**03**

Ministerium des Innern

33.650,1

8.536,5

3.344,7

3.307,5

18.461,4

**04**

Ministerium der Justiz

900,0

600,0

300,0

 

 

**05**

Ministerium für Bildung, Jugend und Sport

14.315,0

7.905,0

5.720,0

650,0

40,0

**06**

Ministerium für Wissenschaft, Forschung und Kultur

30.619,2

24.869,2

5.050,0

400,0

300,0

**07**

Ministerium für Arbeit, Soziales, Frauen und Familie

97.446,3

64.502,1

24.337,4

8.606,8

 

**08**

Ministerium für Wirtschaft und Europaangelegenheiten

429.197,6

148.036,8

214.091,0

67.069,8

 

**10**

Ministerium für Umwelt, Gesundheit und Verbraucherschutz

68.634,4

41.092,0

17.867,4

8.255,0

1.420,0

**11**

Ministerium für Infrastruktur und Landwirtschaft

705.540,8

305.045,1

140.959,7

111.962,0

147.574,0

**12**

Ministerium der Finanzen

123.775,0

57.515,0

42.210,0

24.050,0

 

**13**

Landesrechnungshof

 

 

 

 

 

**14**

Verfassungsgericht des Landes Brandenburg

 

 

 

 

 

**20**

Allgemeine Finanzverwaltung

35.000,0

18.000,0

13.500,0

3.500,0

 

 

**Zusammen**

**1.539.178,4**

**676.201,7**

**467.380,2**

**227.801,1**

**167.795,4**

 

### Teil II Finanzierungsübersicht 2012

 

Insgesamt  
2012  
(Mio EUR)

**I.**

**HAUSHALTSVOLUMEN**

**10.191,6**

 

 

 

 

**II.**

**ERMITTLUNG DES FINANZIERUNGSSALDOS**

 

 

**1.**

**Ausgaben**

**10.128,3**

 

 

(ohne Ausgaben zur Schuldentilgung am Kreditmarkt, Zuführungen an Rücklagen, Ausgaben zur Deckung eines kassenmäßigen Fehlbetrags und haushaltstechnische Verrechnungen)

 

 

 

 

 

 

**2.**

**Einnahmen**

**9.833,7**

 

 

(ohne Einnahmen aus Krediten vom Kreditmarkt, Entnahmen aus Rücklagen,  
Einnahmen aus kassenmäßigen Überschüssen und haushaltstechnischen Verrechnungen)

 

 

 

 

 

 

**3.**

**Finanzierungssaldo**

**\-294,7**

 

 

 

 

**III.**

**AUSGLEICH DES FINANZIERUNGSSALDOS**

 

 

**4.**

**Nettoneuverschuldung am Kreditmarkt**

**270,0**

 

4.1

Einnahmen aus Krediten vom Kreditmarkt (brutto)

3.933,5

 

4.2

Ausgaben zur Schuldentilgung am Kreditmarkt

\-3.663,5

 

4.21

planmäßige Tilgungen

\-3.163,5

 

4.22

mögliche vorzeitige Tilgungen

0,0

 

4.23

Tilgungen kurzfristiger Schulden

\-500,0

 

 

 

 

 

**5.**

**Rücklagenbewegung**

**24,6**

 

5.1

Entnahmen aus Rücklagen

86,8

 

5.2

Zuführungen an Rücklagen

\-62,2

 

**6.**

**Abwicklung der Vorjahre**

**0,0**

 

6.1

Ausgaben zur Deckung kassenmäßiger Fehlbeträge

0,0

 

6.2

Einnahmen aus kassenmäßigen Überschüssen

\--

 

**7.**

**Haushaltstechnische Verrechnungen**

**0,1**

 

7.1

Ausgaben

\-1,0

 

7.2

Einnahmen

1,1

 

**Zusammen**

**294,7**

Abweichungen in den Summen ergeben sich durch Runden der Zahlen

### Teil III Kreditfinanzierungsplan 2012

 

Insgesamt  
2012  
(Mio EUR)

**I.**

**EINNAHMEN US KREDITEN**

 

 

bei Gebietskörperschaften, Sondervermögen usw.

\--

 

vom Kreditmarkt

3.933,5

 

Zusammen

3.933,5

**II.**

**TILGUNGSAUSGABEN FÜR KREDITE**

 

 

bei Gebietskörperschaften, Sondervermögen usw.

**\--**

 

vom Kreditmarkt

3.663,5

 

Zusammen

3.663,5

**III.**

**NETTONEUVERSCHULDUNG insgesamt**

 

 

bei Gebietskörperschaften, Sondervermögen usw.

**\--**

 

vom Kreditmarkt

270,0

 

Zusammen

270,0

Abweichungen in den Summen ergeben sich durch Runden der Zahlen