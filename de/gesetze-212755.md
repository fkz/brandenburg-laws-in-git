## Gesetz zu dem Staatsvertrag über die abschließende Aufteilung des Finanzvermögens gemäß Artikel 22 des Einigungsvertrages zwischen dem Bund, den neuen Ländern und dem Land Berlin 

Der Landtag hat das folgende Gesetz beschlossen:

§ 1 

Dem am 6.
Dezember 2012 vom Land Brandenburg unterzeichneten Staatsvertrag über die abschließende Aufteilung des Finanzvermögens gemäß Artikel 22 des Einigungsvertrages zwischen dem Bund, den neuen Ländern und dem Land Berlin (Finanzvermögen-Staatsvertrag) wird zugestimmt.
Der Finanzvermögen-Staatsvertrag wird nachstehend veröffentlicht.

§ 2 

(1) Dieses Gesetz tritt am Tag nach seiner Verkündung in Kraft.

(2) Der Tag, an dem der Finanzvermögen-Staatsvertrag nach seinem Artikel 9 Satz 2 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu machen.

Potsdam, den 4.
April 2013

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Staatsvertrag](/de/vertraege-237643)