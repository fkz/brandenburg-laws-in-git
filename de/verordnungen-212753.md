## Verordnung über die Studentenwerke  (Studentenwerksverordnung - StWV)

Auf Grund des § 76 Absatz 3 Nummer 1 bis 3 des Brandenburgischen Hochschulgesetzes vom 18.
Dezember 2008 (GVBl.
I S. 318) verordnet die Ministerin für Wissenschaft, Forschung und Kultur:

#### § 1  
Zuständigkeit

Die Studentenwerke sind für folgende Hochschulen zuständig:

1.  das Studentenwerk Potsdam für
    
    *   die Universität Potsdam,
        
    *   die Hochschule für Film und Fernsehen „Konrad-Wolf“ Potsdam Babelsberg,
        
    *   die Fachhochschule Brandenburg,
        
    *   die Fachhochschule Potsdam und
        
    *   die Technische Hochschule Wildau (FH);
        
2.  das Studentenwerk Frankfurt (Oder) für
    
    *   die Stiftung Europa-Universität Viadrina Frankfurt (Oder),
        
    *   die Hochschule für nachhaltige Entwicklung Eberswalde (FH),
        
    *   die Brandenburgische Technische Universität Cottbus-Senftenberg.
        

#### § 2  
Aufgaben

(1) Die Studentenwerke erbringen Dienstleistungen für die Studierenden der dem Studentenwerk zugeordneten Hochschulen auf sozialem, wirtschaftlichem, gesundheitlichem und kulturellem Gebiet.
Nutzungsberechtigt sind ferner Studierende, die bei anderen Studentenwerken ihren Sozialbeitrag entrichtet haben.
Die Studentenwerke können zusätzlich zu ihren gesetzlichen Aufgaben weitere Aufgaben übernehmen, soweit die Erfüllung der Aufgaben nach Satz 1 nicht beeinträchtigt wird.
Die Wahrnehmung dieser Aufgaben durch die Studentenwerke begründet keine zusätzlichen staatlichen Zuweisungen.

(2) Die Studentenwerke können die Dienstleistungen nach Absatz 1 auch für Studierende an nichtstaatlichen Hochschulen und Auszubildende an Berufsakademien erbringen.
 Über die zu erbringenden Dienstleistungen und deren Vergütungen sind Vereinbarungen mit den nichtstaatlichen Hochschulen und Berufsakademien zu treffen, die der Zustimmung der für Hochschulen zuständigen obersten Landesbehörde bedürfen.
Das Nähere regelt die Satzung.

(3) Die Studentenwerke gestatten ihren Beschäftigten und den Beschäftigten der Hochschulen, die in die Zuständigkeit des Studentenwerkes einbezogen sind, die Benutzung ihrer Einrichtungen, soweit die Erfüllung der ihnen übertragenen Aufgaben dadurch nicht beeinträchtigt wird.
Anderen Personen kann gegen kostendeckendes Entgelt die Benutzung gestattet werden.
Das Nähere regelt die Satzung.

(4) Die Studentenwerke können die Errichtung und Bewirtschaftung von Wohneinrichtungen für Gastwissenschaftler und Neuberufene sowie Gäste der Hochschulen als weitere Aufgabe übernehmen.
Zusätzlich können Sie das Angebot von Verpflegungsdienstleistungen für Dritte übernehmen.

#### § 3  
Organe

Organe des Studentenwerkes sind:

1.  der Verwaltungsrat nach § 77 des Brandenburgischen Hochschulgesetzes,
    
2.  der Geschäftsführer nach § 78 des Brandenburgischen Hochschulgesetzes.
    

#### § 4  
Zusammensetzung des Verwaltungsrates

(1) Dem Verwaltungsrat gehören mit beschließender Stimme an:

1.  sechs Studierende,
    
2.  fünf nichtstudentische Hochschulmitglieder oder -angehörige, von denen mindestens zwei Hochschullehrer sein sollen,
    
3.  eine Persönlichkeit des öffentlichen Lebens mit einschlägigen Fachkenntnissen auf wirtschaftlichem, rechtlichem oder sozialem Gebiet,
    
4.  ein Vertreter der für die Hochschulen zuständigen obersten Landesbehörde.
    

Die Zusammensetzung ist durch Satzung so zu bestimmen, dass die im Zuständigkeitsbereich des Studentenwerkes befindlichen Hochschulen angemessen vertreten sind.
Dem Verwaltungsrat gehören je Hochschule mindestens ein Vertreter der Studierendenschaft und ein nichtstudentischer Hochschulvertreter an.

(2) Dem Verwaltungsrat gehören mit beratender Stimme an:

1.  die Kanzler der Hochschulen, soweit sie nicht bereits Mitglied nach Absatz 1 Satz 1 Nummer 2 sind,
    
2.  ein Beschäftigter des Studentenwerkes.
    

(3) Der Geschäftsführer nimmt an den Sitzungen des Verwaltungsrates mit beratender Stimme teil.
Auf Beschluss des Verwaltungsrates nimmt der Geschäftsführer an Beratungen, die seine Person betreffen, nicht teil.

(4) Der Verwaltungsrat wählt mit der absoluten Mehrheit der stimmberechtigten Mitglieder aus seiner Mitte ein Hochschulmitglied, das entweder Hochschulpräsident, Vertreter eines Hochschulpräsidenten oder Hochschullehrer ist, als Vorsitzenden und für den Fall der Verhinderung des Vorsitzenden einen Stellvertreter.

(5) Die Mitglieder des Verwaltungsrates sind ehrenamtlich tätig.

#### § 5  
Bildung des Verwaltungsrates

(1) Die studentischen Mitglieder des Verwaltungsrates nach § 4 Absatz 1 Nummer 1 werden von dem obersten beschlussfassenden Organ der Studierendenschaft der jeweiligen Hochschulen im Zuständigkeitsbereich des Studentenwerkes gewählt.

(2) Die nichtstudentischen Mitglieder des Verwaltungsrates nach § 4 Absatz 1 Nummer 2 werden von dem in der jeweiligen Grundordnung bestimmten Zentralen Hochschulorgan gewählt.
Die Studierenden sind hierbei nicht wahlberechtigt.

(3) Die Person des öffentlichen Lebens nach § 4 Absatz 1 Nummer 3 wird durch die anderen stimmberechtigten Mitglieder des Verwaltungsrates gewählt.

(4) Das für die Hochschulen zuständige Mitglied der Landesregierung bestellt einen Vertreter nach § 4 Absatz 1 Nummer 4.

(5) Der Vertreter der Beschäftigten nach § 4 Absatz 2 Nummer 2 wird von den Beschäftigten des Studentenwerkes gewählt.

(6) Die Amtszeit der gewählten Mitglieder des Verwaltungsrates beträgt zwei Jahre.
Eine Wiederwahl ist zulässig.
Ist bei Ablauf der Amtszeit noch kein neues Mitglied gewählt, übt das bisherige Mitglied sein Amt bis zur Neuwahl weiter aus.
Für jedes stimmberechtigte Mitglied nach § 4 Absatz 1 Nummer 1 bis 3 ist ein Ersatzmitglied zu wählen.
Bei vorzeitigem Ausscheiden eines stimmberechtigten Mitgliedes nach § 4 Absatz 1 Nummer 1 bis 3 rückt das Ersatzmitglied als Mitglied nach.
Scheidet auch das nachgerückte Mitglied aus, erfolgt für den Rest der Amtsperiode des Verwaltungsrates eine Neuwahl.

#### § 6  
Verfahrensgrundsätze

(1) Der Vorsitzende des Verwaltungsrates beruft die Sitzungen ein, leitet sie und vertritt die Beschlüsse des Verwaltungsrates gegenüber dem Geschäftsführer und nach außen.

(2) Auf Verlangen von vier stimmberechtigten Mitgliedern des Verwaltungsrates oder auf Verlangen des Geschäftsführers muss der Verwaltungsrat einberufen werden.
Das schriftliche Verlangen ist an den Vorsitzenden oder an den Geschäftsführer zu richten.

(3) Der Verwaltungsrat ist beschlussfähig, wenn die Mehrheit der stimmberechtigten Mitglieder anwesend ist.
Beschlüsse werden mit einfacher Mehrheit der anwesenden Mitglieder gefasst.
Bei Stimmgleichheit entscheidet der Vorsitzende.
Zur Beschlussfassung über die Wahl und Abberufung des Geschäftsführers sowie den Erlass und die Änderung der Satzung und der Beitragsordnung sind acht Stimmen der stimmberechtigten Mitglieder notwendig.
Wahl und Abberufung des Geschäftsführers bedürfen der geheimen Abstimmung.

(4) Die Mitglieder des Verwaltungsrates haben das Gesamtinteresse des Studentenwerkes wahrzunehmen.
Sie sind bei der Ausübung des Stimmrechts nicht an Weisungen gebunden.

(5) Der Verwaltungsrat tagt in hochschulöffentlicher Sitzung.
Die Öffentlichkeit ist in Personal- und Grundstücksangelegenheiten auszuschließen.
Der Verwaltungsrat kann mit einfacher Mehrheit der anwesenden Mitglieder die Hochschulöffentlichkeit ausschließen.

(6) Der Verwaltungsrat gibt sich eine Geschäftsordnung.

#### § 7  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Studentenwerksverordnung vom 13.
November 2003 (GVBl.
II S. 676) außer Kraft.

Potsdam, den 15.
Juni 2010

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Dr.
Martina Münch