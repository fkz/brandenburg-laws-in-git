## Verordnung über Verwaltungsverfahren auf bundesgesetzlicher Grundlage zur Umsetzung der Richtlinie 2006/123/EG im Land Brandenburg  (Brandenburgische Dienstleistungsrichtlinienverordnung - BbgDLRLV)

Auf Grund des § 5 des Gesetzes über den Einheitlichen Ansprechpartner für das Land Brandenburg vom 7.
Juli 2009 (GVBl.
I S. 262) sowie des § 6b Satz 2 der Gewerbeordnung in der Fassung der Bekanntmachung vom 22.
Februar 1999 (BGBl.
I S. 202), von denen § 6b Satz 2 durch Artikel 1 Nummer 4 des Gesetzes vom 17.
Juli 2009 (BGBl.
I S. 2091) eingefügt worden ist, verordnet die Landesregierung und auf Grund des § 9 Absatz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186) in Verbindung mit § 7 Absatz 1 des Batteriegesetzes vom 25.
Juni 2009 (BGBl.
I S. 1582) sowie des § 42 Absatz 3 des Brandenburgischen Abfall- und Bodenschutzgesetzes vom 6.
Juni 1997 (GVBl.
I S. 40), von denen § 42 Absatz 3 durch Artikel 1 Nummer 29 des Gesetzes vom 27.
Mai 2009 (GVBl.
I S. 175) neu gefasst worden ist, verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

#### § 1   
Anordnung des Verfahrens über den Einheitlichen Ansprechpartner für das Land Brandenburg

Folgende Verfahren können über den Einheitlichen Ansprechpartner für das Land Brandenburg abgewickelt werden:

1.  Verfahren nach § 4 des Energiewirtschaftsgesetzes vom 7.
    Juli 2005 (BGBl.
    I S. 1970, 3621), das zuletzt durch Artikel 2 des Gesetzes vom 21.
    August 2009 (BGBl.
    I S. 2870) geändert worden ist,
    
2.  Verfahren nach § 10 des Eichgesetzes in der Fassung der Bekanntmachung vom 23.
    März 1992 (BGBl.
    I S. 711), das zuletzt durch Artikel 2 des Gesetzes vom 3.
    Juli 2008 (BGBl.
    I S. 1185) geändert worden ist,
    
3.  Verfahren nach den §§ 64a, 65 und 72 der Eichordnung vom 12.
    August 1988 (BGBl.
    I S. 1657), die zuletzt durch Artikel 3 § 14 des Gesetzes vom 13.
    Dezember 2007 (BGBl.
    I S. 2930) geändert worden ist,
    
4.  Verfahren nach § 5 Absatz 1 der Verordnung über Heizkostenabrechnung in der Fassung der Bekanntmachung vom 5.
    Oktober 2009 (BGBl.
    I S. 3250),
    
5.  Verfahren nach § 13 der Markscheider-Bergverordnung vom 19.
    Dezember 1986 (BGBl.
    I S. 2631), die zuletzt durch Artikel 4 der Verordnung vom 10.
    August 1998 (BGBl.
    I S. 2093) geändert worden ist.
    

Das Gesetz über den Einheitlichen Ansprechpartner für das Land Brandenburg vom 7.
Juli 2009 (GVBl.
I S. 262) in der jeweils geltenden Fassung sowie § 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg in Verbindung mit den §§ 71a bis 71e des Verwaltungsverfahrensgesetzes finden Anwendung.

#### § 2  
Abweichende Regelungen zur Anordnung des Verfahrens über  
eine einheitliche Stelle in Vorschriften des Bundes

Folgende Verfahren sind abweichend von Vorschriften auf bundesgesetzlicher Grundlage nicht über eine einheitliche Stelle gemäß den §§ 71a bis 71e des Verwaltungsverfahrensgesetzes abzuwickeln:

1.  mit Ausnahme der Verfahren nach § 7 Absatz 1 und § 20 Absatz 1 alle übrigen Verfahren nach dem Sprengstoffgesetz in der Fassung der Bekanntmachung vom 10.
     September 2002 (BGBl.
    I S. 3518), das zuletzt durch Artikel 4 des Gesetzes vom 11.
    August 2009 (BGBl.
    I S. 2723) geändert worden ist,
    
2.  mit Ausnahme des Verfahrens nach § 32 Absatz 1 alle übrigen Verfahren nach der Ersten Verordnung zum Sprengstoffgesetz in der Fassung der Bekanntmachung vom 31.
     Januar 1991 (BGBl.
    I S. 169), die zuletzt durch Artikel 2 des Gesetzes vom 17.
    Juli 2009 (BGBl.
    I S. 2062) geändert worden ist,
    
3.  Verfahren nach der Zweiten Verordnung zum Sprengstoffgesetz in der Fassung der Bekanntmachung vom  
    10.
    September 2002 (BGBl.
    I S. 3543), die zuletzt durch Artikel 6 Absatz 1 der Verordnung vom 6.
    März 2007 (BGBl.
    I S. 261) geändert worden ist,
    
4.  Verfahren nach der Dritten Verordnung zum Sprengstoffgesetz vom 23.
    Juni 1978 (BGBl.
    I S. 783),
    
5.  mit Ausnahme der Verfahren nach § 18 Absatz 1, § 21 Absatz 1 sowie den §§ 21a, 34 Absatz 2, 4 und 5 alle übrigen Verfahren des Waffengesetzes vom 11.
    Oktober 2002 (BGBl.
    I S. 3970, 4592; 2003 I S. 1957), das  
    zuletzt durch Artikel 3 Absatz 5 des Gesetzes vom 17.
    Juli 2009 (BGBl.
    I S. 2062) geändert worden ist,
    
6.  mit Ausnahme der Verfahren nach § 10 Absatz 2 und § 22 Absatz 2 und 3 alle übrigen Verfahren der Allgemeinen Waffengesetz-Verordnung vom 27.
    Oktober 2003 (BGBl.
    I S. 2123), die zuletzt durch Artikel 3  
    Absatz 6 des Gesetzes vom 17.
    Juli 2009 (BGBl.
    I S. 2062) geändert worden ist,
    
7.  Verfahren nach den §§ 30, 33c, 33d, 33i, 34, 34a, 34c Absatz 1 Satz 1 Nummer 1a, 2 und 3, den §§ 34d, 34e, 55b Absatz 2 und § 60a der Gewerbeordnung in der Fassung der Bekanntmachung vom 22.
    Februar 1999 (BGBl.
    I S. 202), die zuletzt durch Artikel 4 Absatz 14 des Gesetzes vom 29.
    Juli 2009 (BGBl.
    I S. 2258)  
    geändert worden ist.
    

#### § 3   
Anordnung einer Entscheidungsfrist

(1) In folgenden Verfahren ist über den Genehmigungsantrag innerhalb von drei Monaten zu entscheiden:

1.  Verfahren nach § 7 Absatz 1 und § 20 Absatz 1 des Sprengstoffgesetzes,
    
2.  Verfahren nach § 32 Absatz 1 der Ersten Verordnung zum Sprengstoffgesetz,
    
3.  Verfahren nach § 10 des Eichgesetzes,
    
4.  Verfahren nach den §§ 65 und 72 der Eichordnung,
    
5.  Verfahren nach § 13 der Markscheider-Bergverordnung.
    

(2) In folgenden Verfahren ist über den Genehmigungsantrag innerhalb von sechs Monaten zu entscheiden:

1.  Verfahren nach § 18 Absatz 1, § 21 Absatz 1 sowie § 21a des Waffengesetzes,
    
2.  Verfahren nach § 4 des Energiewirtschaftsgesetzes,
    
3.  Verfahren nach § 5 Absatz 1 der Verordnung über Heizkostenabrechnung.
    

(3) Auf die Verfahren nach den Absätzen 1 und 2 findet § 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg in Verbindung mit § 42a Absatz 2 Satz 2 bis 4 des Verwaltungsverfahrensgesetzes Anwendung.