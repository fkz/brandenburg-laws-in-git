## Verordnung über das Verfahren der Feststellung von Veränderungen des angestammten Siedlungsgebietes der Sorben/Wenden

Auf Grund des § 13c Absatz 2 des Sorben/Wenden-Gesetzes vom 7.
Juli 1994 (GVBl.
I S.
294), der durch Artikel 1 Nummer 17 des Gesetzes vom 11.
Februar 2014 (GVBl.
I Nr.
7) eingefügt worden ist, verordnet die Ministerin für Wissenschaft, Forschung und Kultur im Benehmen mit dem Hauptausschuss des Landtages und dem Rat für An-gelegenheiten der Sorben/Wenden:

### § 1 

Als angestammtes Siedlungsgebiet der Sorben/Wenden nach § 3 Absatz 2 des Gesetzes über die Rechte der Sorben/Wenden im Land Brandenburg gelten die kreisfreie Stadt Cottbus/Chóśebuz sowie diejenigen Gemeinden und Gemeindeteile in den Landkreisen Dahme-Spreewald, Oberspreewald-Lausitz und Spree-Neiße/Sprjewja-Nysa, in denen eine kontinuierliche sprachliche oder kulturelle Tradition bis zur Gegenwart nachweisbar ist.
Das für die Angelegenheiten der Sorben/Wenden zuständige Ministerium kann Veränderungen des angestammten Siedlungsgebietes feststellen, wenn die Voraussetzungen nach Satz 1 in Gemeinden oder Gemeindeteilen nachgewiesen werden, die nicht in der als Anlage zum Sorben/Wenden-Gesetz aufgenommenen Liste enthalten sind.

### § 2 

(1) Die Feststellung der Zugehörigkeit zum angestammten Siedlungsgebiet erfolgt auf Antrag.
Antragsberechtigt sind der Rat für Angelegenheiten der Sorben/Wenden beim Landtag und die Gemeinden in den Landkreisen Dahme-Spreewald, Oberspreewald-Lausitz und Spree-Neiße/Sprjewja-Nysa, die oder deren von Anträgen betroffene Gemeindeteile nicht in der als Anlage zum Sorben/Wenden-Gesetz aufgenommenen Liste enthalten sind.

(2) Die Anträge sind bis zum 31.
Mai 2016 zu stellen.
Nach diesem Zeitpunkt eingehende Anträge sind unzulässig.

(3) Wird der Antrag von mehreren Antragsberechtigten gemeinsam oder übereinstimmend gestellt, so sind der jeweilige Landkreis und die anerkannten Dachverbände der Sorben/Wenden nach § 4a des Sorben/Wenden-Gesetzes zu hören.
Wird der Antrag nur von einem Antragsberechtigten gestellt, ist für den anderen Antragsberechtigten ein Anhörungsverfahren mit einer Frist von vier Monaten durchzuführen.

(4) Beabsichtigt der andere Antragsberechtigte im Fall der alleinigen Antragstellung, in Bezug auf die Gemeinde einen eigenen Antrag zu stellen oder eine Unterstützung eines bereits gestellten Antrags durch Beitritt zum Antrag oder durch Beibringung zusätzlicher Belege nach § 3 zu prüfen, so kann er zur Vorbereitung dieser Entscheidung eine Aussetzung des Verfahrens bis zur Dauer von sechs Monaten beantragen.
In begründeten Ausnahmefällen kann das Verfahren bis zu zwölf Monaten ausgesetzt werden.

(5) Werden in Bezug auf eine Gemeinde ganz oder teilweise übereinstimmende Anträge gestellt, sind die Verfahren zur gemeinsamen Prüfung und Entscheidung zu verbinden.

### § 3 

(1) Der Antrag ist in Schriftform zu stellen.
Er ist auf die Feststellung gerichtet, dass eine Gemeinde oder Ge-meindeteile nach § 2 Absatz 2 Satz 2 zum angestammten Siedlungsgebiet des sorbischen/wendischen Volkes gehört oder gehören.
Wurde der Antrag nur in Bezug auf einen oder mehrere Gemeindeteile gestellt, kann er bis zum Ergehen einer Entscheidung nach § 5 Absatz 1 erweitert werden.
Nach diesem Zeitraum ist eine Antragstellung in Bezug auf Gemeindeteile einer Gemeinde, die bereits Gegenstand eines Verfahrens nach dieser Verordnung war, nur innerhalb der Frist nach § 2 Absatz 2 Satz 1 und nur dann möglich, wenn der Antragsteller an der Berücksichtigung dieser Gemeindeteile im ersten Verfahren aus von ihm nicht zu vertretenden Gründen verhindert war.
Sind die nach § 2 Absatz 3 zu Hörenden im Zeitpunkt der Antragserweiterung bereits angehört worden, so ist das Anhörungsverfahren nach § 2 Absatz 3 erneut durchzuführen.

(2) Der Antrag ist mit Gründen und Belegen zu versehen.
Das für Angelegenheiten der Sorben/Wenden zuständige Ministerium kann die Antragsteller zur Beibringung weiterer Belege auffordern.
Es ist zudem gehalten, eigenständig ihm zugängliche Beweismittel heranzuziehen und Hinweise auf das Vorliegen der Voraussetzungen nach § 1 zu erkunden.
Es schöpft seine Erkenntnisse aus freier Überzeugung ohne Bindung an beigebrachte Beweismittel, Anträge und Wertungen.

(3) Ist der Antrag auf Feststellung der Zugehörigkeit eines ganzen Gebietes einer Gemeinde gerichtet, kann die Feststellung auf einen oder mehrere Gemeindeteile beschränkt und der Antrag im Übrigen zurückgewiesen werden, wenn die Voraussetzungen in anderen Gemeindeteilen nicht vorliegen.
Ebenso kann verfahren werden, wenn der Antrag auf eine Mehrzahl von Gemeindeteilen gerichtet ist, die Voraussetzungen der Zugehörigkeit zum angestammten Siedlungsgebiet aber nicht in allen Gemeindeteilen festgestellt werden können.
Die Feststellung der Zugehörigkeit von Gemeindeteilen, die von den gestellten Anträgen nicht umfasst sind, ist nicht möglich.

### § 4 

Die Feststellung, dass eine kontinuierliche sprachliche oder kulturelle sorbische/wendische Tradition bis zur Gegenwart nachweisbar ist, ist aufgrund einer Abwägung aller Umstände des Einzelfalls und angemessener Gewichtung der Ausdrucksformen sorbischer/wendischer Sprache oder Kultur zu treffen.
Art, Dauer, Intensität, Ausstrahlung, Anlass, Bindungswirkung und Aussagekraft einzelner Anhaltspunkte sind in angemessenen Bezug zueinander zu setzen.

### § 5 

(1) Das für Angelegenheiten der Sorben/Wenden zuständige Ministerium entscheidet durch Verwaltungsakt, der zu begründen ist.
Die Entscheidung ergeht gebührenfrei.

(2) Beabsichtigt das für Angelegenheiten der Sorben/Wenden zuständige Ministerium, eine Veränderung des angestammten Siedlungsgebietes und die Zugehörigkeit einer Gemeinde zu diesem Gebiet festzustellen, hat es das Einvernehmen mit dem Hauptausschuss des Landtages herzustellen.
Beabsichtigt es die Zurückweisung des Antrages, hat es den Landtag hierüber zu informieren.
Zugleich ist der Antragsteller über die beabsichtigte Einvernehmensherstellung oder Zurückweisung zu unterrichten.
Ist die betroffene Gemeinde nicht Antragstellerin, so ist auch sie zu unterrichten.

### § 6 

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 8.
September 2014

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Prof.
Dr.-Ing.
Dr.
Sabine Kunst