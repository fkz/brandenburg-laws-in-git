## Brandenburgisches Hinterlegungsgesetz (BbgHintG)

Inhaltsübersicht
----------------

### Abschnitt 1  
Allgemeine Bestimmungen

[§ 1  Hinterlegungsstellen, Hinterlegungskassen](#1)  
[§ 2  Übertragung der Aufgaben](#2)  
[§ 3  Abgabe an eine andere Hinterlegungsstelle](#3)  
[§ 4  Einsichtsrecht](#4)  
[§ 5  Begründung und Überprüfung von Entscheidungen](#5)

### Abschnitt 2  
Annahme

[§ 6  Hinterlegungsfähige Gegenstände](#6)  
[§ 7  Annahme zur Hinterlegung](#7)  
[§ 8  Antrag der hinterlegenden Person](#8)  
[§ 9  Einzahlungen oder Einlieferungen vor Stellung des Annahmeantrages](#9)  
[§ 10 Verfahren nach Erlass der Annahmeanordnung](#10)

### Abschnitt 3  
Verwaltung der Hinterlegungsmasse

[§ 11 Zahlungsmittel](#11)  
[§ 12 Wertpapiere, Urkunden, Kostbarkeiten](#12)  
[§ 13 Vornahme von Wertpapiergeschäften während der Hinterlegung](#13)

### Abschnitt 4  
Benachrichtigungen

[§ 14 Gläubigerbenachrichtigung](#14)  
[§ 15 Benachrichtigung der das Sparbuch ausstellenden Person](#15)  
[§ 16 Benachrichtigung des Nachlassgerichts](#16)  
[§ 17 Benachrichtigung des Betreuungs- und Familiengerichts](#17)  
[§ 18 Benachrichtigung der Staatsanwaltschaft](#18)  
[§ 19 Benachrichtigung der Hinterlegungskasse von Abtretungen, Pfändungen und ähnlichen Veränderungen](#19)

### Abschnitt 5  
Herausgabe

[§ 20 Herausgabeanordnung](#20)  
[§ 21 Antrag auf Herausgabe, Nachweis der Berechtigung](#21)  
[§ 22 Bescheinigung, öffentliche Beglaubigung](#22)  
[§ 23 Herausgabeersuchen von Behörden](#23)  
[§ 24 Frist zur Klage](#24)  
[§ 25 Herausgabeort, Haftung nach der Herausgabe](#25)

### Abschnitt 6  
Erlöschen des Anspruchs auf Herausgabe

[§ 26 Einunddreißigjährige Frist](#26)  
[§ 27 Dreißigjährige Frist](#27)  
[§ 28 Erneuter Fristbeginn](#28)  
[§ 29 Verfall der Hinterlegungsmasse](#29)

### Abschnitt 7  
Hinterlegung in besonderen Fällen

[§ 30 Genehmigung der Aufsichtsbehörde einer Stiftung](#30)

### Abschnitt 8  
Kosten

[§ 31 Anwendung des Justizverwaltungskostengesetzes, Gebührenverzeichnis](#31)  
[§ 32 Festsetzung der Gebührenhöhe](#32)  
[§ 33 Auslagen](#33)  
[§ 34 Kosten](#34)

### Abschnitt 9  
Übergangs- und Schlussbestimmungen

[§ 35 Anhängige Hinterlegungssachen](#35)  
[§ 36 Einschränkung eines Grundrechts](#36)

### Abschnitt 1   
Allgemeine Bestimmungen

#### § 1  
Hinterlegungsstellen, Hinterlegungskassen

(1) Die Hinterlegungsgeschäfte werden von Hinterlegungsstellen und Hinterlegungskassen wahrgenommen.

(2) Hinterlegungsstelle ist das Amtsgericht.

(3) Hinterlegungskasse ist die Landeshauptkasse.

(4) Das für Justiz zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung ein Amtsgericht als Hinterlegungsstelle für die Bezirke mehrerer Amtsgerichte zu bestimmen.

#### § 2  
Übertragung der Aufgaben

Die Geschäfte der Hinterlegungsstelle werden der Rechtspflegerin oder dem Rechtspfleger übertragen.
Die §§ 5 bis 11 des Rechtspflegergesetzes sind nicht anzuwenden.

#### § 3  
Abgabe an eine andere Hinterlegungsstelle

(1) Die Hinterlegungsstelle kann eine bei ihr anhängige Sache aus wichtigem Grund an eine andere Hinterlegungsstelle abgeben, wenn diese zur Übernahme bereit ist.
 Einigen sich die Stellen nicht, entscheidet die gemeinsame Aufsichtsbehörde.
Von der Abgabe einer Sache an eine andere Hinterlegungsstelle hat die neue Hinterlegungsstelle die Beteiligten zu benachrichtigen.

(2) Ist die Miete oder Pacht bei einer anderen Hinterlegungsstelle hinterlegt worden als der, in deren Bezirk das Grundstück liegt, so ist die Sache an die Stelle abzugeben, in deren Bezirk das Grundstück liegt.

#### § 4  
Einsichtsrecht

Den Beteiligten ist Einsicht in die Hinterlegungsakten zu gestatten, soweit nicht schwerwiegende Interessen einer beteiligten Person entgegenstehen.

#### § 5  
Begründung und Überprüfung von Entscheidungen

(1) Entscheidungen, durch die Anträge auf Annahme zur Hinterlegung oder auf Herausgabe hinterlegter Gegenstände abgelehnt werden, sowie Entscheidungen, die auf Beschwerden ergehen, sind schriftlich zu begründen.
Anderen Entscheidungen ist eine Begründung nur beizufügen, wenn dies nach Lage der Sache erforderlich erscheint.

(2) Beschwerden gegen Entscheidungen der Hinterlegungsstelle werden im Aufsichtsweg erledigt.
 Die Beschwerde ist bei der Hinterlegungsstelle schriftlich oder zu Protokoll der Geschäftsstelle einzulegen.

(3) Gegen die Entscheidung der Präsidentin oder des Präsidenten des Land- oder Amtsgerichts ist der Antrag auf gerichtliche Entscheidung nach § 23 des Einführungsgesetzes zum Gerichtsverfassungsgesetz statthaft.

(4) Ist durch die Entscheidung der Präsidentin oder des Präsidenten des Land- oder Amtsgerichts ein Antrag auf Herausgabe abgelehnt worden, ist für eine Klage auf Herausgabe gegen das Land nur der ordentliche Rechtsweg gegeben.
Für die Klage ist ohne Rücksicht auf den Wert des Streitgegenstandes das Landgericht zuständig, in dessen Bezirk die Hinterlegungsstelle liegt.

### Abschnitt 2   
Annahme

#### § 6  
Hinterlegungsfähige Gegenstände

Zur Hinterlegung werden Geld, Wertpapiere und sonstige Urkunden sowie Kostbarkeiten angenommen.

#### § 7  
Annahme zur Hinterlegung

Die Annahme zur Hinterlegung bedarf einer Verfügung der Hinterlegungsstelle (Annahmeanordnung).
Die Verfügung ergeht:

1.  auf Antrag der hinterlegenden Person, wenn sie die Tatsachen angibt, welche die Hinterlegung rechtfertigen, oder wenn sie nachweist, dass sie durch Entscheidung oder Anordnung der zuständigen Behörde zur Hinterlegung für berechtigt oder verpflichtet erklärt ist,
    
2.  auf Ersuchen der zuständigen Behörde.
    

#### § 8  
Antrag der hinterlegenden Person

(1) Der Antrag der hinterlegenden Person nach § 7 Satz 2 Nummer 1 ist schriftlich oder zu Protokoll der Geschäftsstelle zu stellen; er ist doppelt einzureichen.
Der Antrag soll enthalten:

1.  bei natürlichen Personen den Vor- und Familiennamen, die Anschrift, andere die hinterlegende Person deutlich kennzeichnende Merkmale, und, falls eine Vertreterin oder ein Vertreter hinterlegt, die entsprechenden Angaben für diese Person; bei juristischen Personen und Handelsgesellschaften die Firma, die Anschrift, die zur gesetzlichen Vertretung berufenen Personen, sowie gegebenenfalls Handelsregisternummer und Sitz des Amtsgerichts, bei dem die juristische Person oder die Handelsgesellschaft eingetragen ist;
    
2.  die bestimmte Angabe der Tatsachen, welche die Hinterlegung rechtfertigen, insbesondere die Bezeichnung der Sache, der Behörde oder des Gerichts und des Aktenzeichens, wenn die Angelegenheit, in der hinterlegt wird, bei einer Behörde oder einem Gericht anhängig ist;
    
3.  bei Hinterlegung von Geld den Betrag und, falls andere als gesetzliche und gesetzlich zugelassene Zahlungsmittel hinterlegt werden, die Geldsorten;
    
4.  bei Hinterlegung von Wertpapieren:
    
    1.  Zinssatz, Gattung, Jahrgang, Reihe, Buchstaben, Nummer, Nennbetrag und etwa sonst vorhandene Unterscheidungsmerkmale,
        
    2.  Angaben über die zu den Wertpapieren etwa gehörigen Erneuerungs-, Zins- oder Gewinnanteilsscheine; werden Scheine hinterlegt, die zu bereits hinterlegten Wertpapieren gehören, soll auf den wegen der Wertpapiere selbst gestellten Antrag hingewiesen werden;
        
5.  bei Hinterlegung von sonstigen Urkunden die genaue Bezeichnung und den etwa angegebenen Wertbetrag;
    
6.  bei Hinterlegung von Kostbarkeiten Gattung, Stoff und etwa sonst vorhandene Unterscheidungsmerkmale sowie den Wert.
    

Geldbeträge sind in Ziffern und in Buchstaben anzugeben.

(2) In dem Antrag sind, soweit möglich, die Personen, die als Empfangsberechtigte in Frage kommen, entsprechend Absatz 1 Satz 2 Nummer 1 zu bezeichnen und deren Konten anzugeben.
Wird zur Befreiung einer Schuldnerin oder eines Schuldners von einer Verbindlichkeit hinterlegt, ist in dem Antrag ferner die Gläubigerin oder der Gläubiger, für die oder den hinterlegt wird, mit den in Absatz 1 Satz 2 Nummer 1 aufgeführten Angaben zu bezeichnen; bei Ungewissheit über die Gläubigerin oder den Gläubiger sind alle in Frage kommenden Personen aufzuführen.
Außerdem ist anzugeben, warum die Schuldnerin oder der Schuldner die Verbindlichkeit nicht oder nicht mit Sicherheit erfüllen kann.
Wird das Recht der Gläubigerin oder des Gläubigers zum Empfang des hinterlegten Gegenstandes von der Bewirkung einer Gegenleistung abhängig gemacht, ist die Gegenleistung anzugeben.
Bei einer Hinterlegung für unbekannte Erben ist auch die Person der Erblasserin oder des Erblassers entsprechend Absatz 1 Satz 2 Nummer 1 zu bezeichnen, zusätzlich ist das Sterbedatum und der letzte Wohnsitz der Erblasserin oder des Erblassers anzugeben.

(3) In den Fällen des § 1171 des Bürgerlichen Gesetzbuches und des § 67 des Gesetzes über Rechte an eingetragenen Schiffen und Schiffsbauwerken ist dem Antrag auf Annahme der Nachweis beizufügen, dass das Aufgebotsverfahren eingeleitet ist.

(4) Ist die Antrag stellende Person durch eine Behörde zur Hinterlegung für berechtigt oder verpflichtet erklärt, so ist dem Antrag die Entscheidung oder Anordnung in Urschrift, Ausfertigung oder Abschrift beizufügen.
Geht die Entscheidung oder Anordnung von dem Gericht aus, zu dem die Hinterlegungsstelle gehört, genügt die Bezugnahme auf dessen Akten.

(5) Der Antrag nach Absatz 1 kann zu Protokoll der Geschäftsstelle eines jeden Amtsgerichts erklärt werden.
Die Geschäftsstelle hat den Antrag unverzüglich an die Hinterlegungsstelle zu übermitteln, an die der Antrag gerichtet ist.

(6) Bei weiteren Hinterlegungen in derselben Angelegenheit kann auf den ersten Antrag Bezug genommen werden.

#### § 9  
Einzahlungen oder Einlieferungen vor Stellung des Annahmeantrages

(1) Ist eingezahlt oder eingeliefert und liegt noch kein Annahmeantrag vor, so hat die Hinterlegungsstelle der einzahlenden oder einliefernden Person zur Stellung des Antrages eine Frist mit dem Hinweis zu bestimmen, dass nach Ablauf der Frist der Betrag zurückgezahlt oder die Sachen zurückgesandt werden.
Das Gleiche gilt, wenn der Antrag nicht den Anforderungen entspricht.

(2) Die Rücksendung wird von der Hinterlegungsstelle angeordnet.

#### § 10  
Verfahren nach Erlass der Annahmeanordnung

(1) Die Hinterlegungsstelle hat die hinterlegende Person von dem Erlass der Annahmeanordnung zu benachrichtigen, sofern nicht bereits eingezahlt oder eingeliefert ist.
Zugleich ist die hinterlegende Person aufzufordern, die zu hinterlegenden Gegenstände innerhalb einer bestimmten Frist bei der zuständigen Hinterlegungskasse unter Vorlegung der Nachricht entgeltfrei einzuzahlen oder einzuliefern.
Die Hinterlegungsstelle und das Aktenzeichen der Hinterlegungssache sind anzugeben.
In die Aufforderung ist der Hinweis aufzunehmen, dass nach Fristablauf der Antrag als zurückgenommen behandelt wird.
 Die Hinterlegungskasse ist in der Nachricht mit ihrer Anschrift und im Fall einer Geldhinterlegung mit ihrer Bankverbindung anzugeben.

(2) In der Annahmeanordnung ist die Hinterlegungskasse zu ersuchen, die Anordnung zurückzugeben, falls nicht innerhalb der Frist eingezahlt oder eingeliefert wird.

### Abschnitt 3  
Verwaltung der Hinterlegungsmasse

#### § 11   
Zahlungsmittel

(1) Gesetzliche oder gesetzlich zugelassene Zahlungsmittel gehen in das Eigentum des Landes über.

(2) Andere Zahlungsmittel werden unverändert aufbewahrt.
Sie können mit Zustimmung der Beteiligten in gesetzliche oder gesetzlich zugelassene Zahlungsmittel umgewechselt werden.
Der Reinerlös geht in das Eigentum des Landes über.

(3) Geld, das nach Absatz 1 in das Eigentum des Landes übergegangen ist, sowie Beträge, die aus der Einlösung von Wertpapieren, Zins- und Gewinnanteilsscheinen oder in ähnlicher Weise anfallen, werden nicht verzinst.

#### § 12  
Wertpapiere, Urkunden, Kostbarkeiten

(1) Wertpapiere können als stückelose Wertpapiere hinterlegt oder während der Hinterlegung in stückelose Wertpapiere umgewandelt werden.
Sonstige Urkunden und Kostbarkeiten werden unverändert aufbewahrt.

(2) Die Hinterlegungsstelle kann durch Sachverständige den Wert von Kostbarkeiten schätzen oder ihre Beschaffenheit feststellen lassen.
Die Kosten trägt die hinterlegende Person.

#### § 13  
Vornahme von Wertpapiergeschäften während der Hinterlegung

(1) Hinterlegte Wertpapiere sind einem geeigneten Kreditinstitut zur Verwaltung und Verwahrung zu übergeben, wenn zu erwarten ist, dass die Hinterlegung länger als drei Monate dauern wird oder die Hinterlegungsstelle die Abgabe anordnet.

(2) Hat die Hinterlegung von Wertpapieren drei Monate angedauert, so erfolgt durch die Hinterlegungsstelle eine Verwaltung der Wertpapiere nach den folgenden Vorschriften.
Die Hinterlegungsstelle kann auf Antrag einer beteiligten Person einen früheren Zeitpunkt für den Beginn der Verwaltung bestimmen.
Eine abweichende Bestimmung ist regelmäßig dann zu treffen, wenn die Antrag stellende Person für eine frühere Verwaltung zwingende Gründe, insbesondere einen drohenden Rechtsverlust, darlegt.
Dauert die Hinterlegung länger als drei Monate, so sind die Geschäfte, die in der Zwischenzeit nicht erledigt wurden, alsbald nachzuholen.

(3) Im Rahmen der Verwaltung nach Absatz 1 werden während der Hinterlegung vorgenommen

1.  die Einlösung von Wertpapieren, die ausgelost, gekündigt oder aus einem anderen Grunde fällig sind, sowie der Umtausch, die Abstempelung oder dergleichen bei Wertpapieren, die hierzu aufgerufen sind; ist die Einlösung neben anderen Möglichkeiten vorgesehen, so wird die Einlösung besorgt; ist ein Spitzenbetrag vorhanden, dessen Umtausch oder dergleichen nicht möglich ist, kann die Hinterlegungsstelle seine bestmögliche Verwertung anordnen;
    
2.  die Einlösung fälliger Zins- und Gewinnanteilsscheine;
    
3.  die Beschaffung von neuen Zins- und Gewinnanteilsscheinen sowie von Erneuerungsscheinen dazu.
    

Ist die Vornahme eines Geschäfts nach Satz 1 Nummer 1 oder Nummer 2 bei ausländischen Wertpapieren mit unverhältnismäßigen Schwierigkeiten oder Kosten verbunden, kann die Hinterlegungsstelle stattdessen die bestmögliche Verwertung anordnen.

(4) Die bezeichneten Geschäfte werden nur dann ausgeführt, wenn

1.  die Notwendigkeit zu ihrer Vornahme aus dem Bundesanzeiger oder einer von der Justizverwaltung bestimmten Verlosungstabelle hervorgeht oder
    
2.  die Notwendigkeit zu ihrer Vornahme aus den Wertpapieren selbst hervorgeht oder
    
3.  eine beteiligte Person die Vornahme eines dieser Geschäfte beantragt und die Voraussetzungen für die Vornahme dargetan hat.
    

Die Hinterlegungsstelle kann anordnen, dass die Vornahme der Geschäfte unterbleibt, wenn besondere Bedenken entgegenstehen; in diesem Fall hat sie die Personen, die zur Zeit der Anordnung an der Hinterlegung beteiligt sind, hiervon alsbald zu benachrichtigen, soweit dies ohne unverhältnismäßige Schwierigkeiten möglich ist.

(5) Die Hinterlegungsstelle kann auf Antrag einer beteiligten Person

1.  eine von Absatz 3 abweichende Regelung treffen,
    
2.  anordnen, dass bei Wertpapieren weitere Geschäfte ausgeführt werden, wenn ein besonderes Bedürfnis hierfür vorgetreten ist,
    
3.  anordnen, dass hinterlegtes Geld zum Ankauf von bestimmten Wertpapieren verwendet wird.
    

Sie hat vorher die übrigen Beteiligten zu hören, soweit dies ohne unverhältnismäßige Schwierigkeiten möglich ist.

### Abschnitt 4  
Benachrichtigungen

#### § 14   
Gläubigerbenachrichtigung

(1) Ist zur Befreiung einer Schuldnerin oder eines Schuldners von einer Verbindlichkeit hinterlegt, soll die Hinterlegungsstelle die Schuldnerin oder den Schuldner unter Bezugnahme auf § 382 des Bürgerlichen Gesetzbuches zu dem Nachweis auffordern, dass und wann die Gläubigerin oder der Gläubiger die in § 374 Absatz 2 des Bürgerlichen Gesetzbuches vorgeschriebene Anzeige von der Hinterlegung empfangen hat.
Führt die Schuldnerin oder der Schuldner den Nachweis nicht innerhalb von drei Monaten nach der Aufforderung, ist die Hinterlegungsstelle ermächtigt, in ihrem oder seinem Namen und auf ihre oder seine Kosten der Gläubigerin oder dem Gläubiger die Anzeige zu machen; die Aufforderung muss einen Hinweis auf diese Rechtsfolge enthalten.

(2) Die Aufforderung an die Schuldnerin oder den Schuldner soll alsbald abgesandt werden.
Die Anzeige an die Gläubigerin oder den Gläubiger kann die Hinterlegungsstelle bis zum Ablauf eines Jahres seit der Hinterlegung aussetzen.

(3) Die Aufforderung und die Anzeige sind nach den für die Zustellung von Amts wegen geltenden Vorschriften der Zivilprozessordnung bekannt zu machen.
Erscheint die Schuldnerin oder der Schuldner zur Stellung des Hinterlegungsantrages persönlich, soll ihr oder ihm die Aufforderung sogleich nach § 173 der Zivilprozessordnung zugestellt werden.

#### § 15  
Benachrichtigung der das Sparbuch ausstellenden Person

Von der Hinterlegung eines Sparbuchs benachrichtigt die Hinterlegungsstelle die das Sparbuch ausstellende Person.

#### § 16   
Benachrichtigung des Nachlassgerichts

Die Hinterlegungsstelle benachrichtigt außer bei Hinterlegungen nach § 1960 des Bürgerlichen Gesetzbuches das zuständige Nachlassgericht von einer Hinterlegung für unbekannte Erben, wenn aus den Hinterlegungsakten nicht ersichtlich ist, dass dem Nachlassgericht die Hinterlegung bereits bekannt ist, und teilt sämtliche in den Hinterlegungsakten enthaltenen Angaben über die Person der Erblasserin oder des Erblassers mit.

#### § 17   
Benachrichtigung des Betreuungs- und Familiengerichts

Erfolgt die Hinterlegung im Rahmen eines Betreuungsverfahrens oder für eine minderjährige Person, benachrichtigt die Hinterlegungsstelle das jeweils zuständige Gericht.
Die Hinterlegungsstelle benachrichtigt das Betreuungs- oder Familiengericht von einer Hinterlegung für eine betreute oder minderjährige Person, wenn diese nicht im Zusammenhang mit einem Rechtsstreit steht und nicht auf einer Anordnung des Betreuungs- oder Familiengerichts beruht.

#### § 18   
Benachrichtigung der Staatsanwaltschaft

Wird eine Sicherheit nach den §§ 116 und 116a der Strafprozessordnung hinterlegt, ist unverzüglich die zuständige Staatsanwaltschaft zu benachrichtigen.

#### § 19   
Benachrichtigung der Hinterlegungskasse von Abtretungen, Pfändungen und ähnlichen Veränderungen

Die Hinterlegungsstelle benachrichtigt unverzüglich die Hinterlegungskasse von Abtretungen, Pfändungen, Gesamtvollstreckungen und ähnlichen Veränderungen.
Sie hat die Kasse auch von deren Erledigung zu benachrichtigen.

### Abschnitt 5   
Herausgabe

#### § 20  
Herausgabeanordnung

(1) Die Herausgabe bedarf einer Verfügung der Hinterlegungsstelle (Herausgabeanordnung).

(2) Soll die Herausgabe einer Sache von der Zahlung der Kosten nach § 34 Absatz 3 Nummer 3 abhängig gemacht werden, ist die Herausgabeanordnung erst zu erlassen, wenn die Kosten eingezahlt sind.

#### § 21  
Antrag auf Herausgabe, Nachweis der Berechtigung

(1) Die Herausgabeanordnung ergeht auf Antrag, wenn die Berechtigung der Empfängerin oder des Empfängers nachgewiesen ist.

(2) Der Antrag auf Herausgabe ist schriftlich oder zu Protokoll der Geschäftsstelle zu stellen.
 Soweit hinterlegtes Geld herausgegeben werden soll, soll eine Bankverbindung der oder des Empfangsberechtigten angegeben werden.
Befindet sich der Nachweis der Empfangsberechtigung bei den Akten des Gerichts, zu dem die Hinterlegungsstelle gehört, genügt die Bezugnahme auf diese Akten.

(3) Der Nachweis ist namentlich als geführt anzusehen:

1.  wenn die Beteiligten die Herausgabe an die Empfängerin oder den Empfänger schriftlich oder zur Niederschrift der Hinterlegungsstelle, eines Gerichts oder einer Urkundsbeamtin oder eines Urkundsbeamten der Geschäftsstelle bewilligen oder die Empfangsberechtigung in gleicher Weise anerkannt haben;
    
2.  wenn die Berechtigung der Empfängerin oder des Empfängers durch rechtskräftige Entscheidung mit Wirkung gegen die Beteiligten oder gegen das Land festgestellt ist.
    

Aus einem nachträglich entstandenen Grund kann auch in diesen Fällen die Berechtigung beanstandet werden.

(4) Kann die Herausgabeanordnung nicht ausgeführt werden, weil die Empfängerin oder der Empfänger die Annahme verweigert oder weil die Sendung als unzustellbar zurückkommt, hat die Hinterlegungsstelle eine erneute Annahmeanordnung zu erlassen.

(5) Die Hinterlegungsstelle kann die Herausgabeanordnung zurücknehmen, wenn nach ihrem Erlass Umstände bekannt werden, die ihrer Ausführung entgegenstehen.

#### § 22  
Bescheinigung, öffentliche Beglaubigung

(1) Die für den Nachweis der Empfangsberechtigung wesentliche Erklärung einer beteiligten Person ist schriftlich abzugeben.
Die Hinterlegungsstelle kann verlangen, dass die Echtheit der Unterschrift durch eine zur Führung eines öffentlichen Siegels berechtigte Person mittels einer gesiegelten oder gestempelten Urkunde bescheinigt wird.
Sie kann auch verlangen, dass die Unterschrift öffentlich beglaubigt wird.

(2) Das Gleiche gilt, wenn eine Vollmachtsurkunde eingereicht wird.

#### § 23  
Herausgabeersuchen von Behörden

(1) Wenn die zuständige Behörde um Herausgabe an sich selbst oder an eine von ihr bezeichnete Stelle oder Person ersucht, ist eine Herausgabeanordnung nach § 20 Absatz 1 zu erlassen.
Geht das Ersuchen von einer obersten Bundes- oder Landesbehörde oder von einer ihr unmittelbar unterstellten höheren Bundes- oder Landesbehörde aus, ist deren Zuständigkeit von der Hinterlegungsstelle nicht zu prüfen.
Das Gleiche gilt, wenn das Ersuchen von einem Gericht ausgeht.

(2) Ergeben sich gegen die Berechtigung der Empfängerin oder des Empfängers Bedenken, die die ersuchende Behörde nicht berücksichtigt hat, ist ihr dies mitzuteilen; die Verfügung nach § 20 Absatz 1 ist auszusetzen.
Hält die Behörde ihr Ersuchen gleichwohl aufrecht, ist diesem stattzugeben.

#### § 24  
Frist zur Klage

(1) Ist ein Antrag auf Herausgabe gestellt, kann die Hinterlegungsstelle Beteiligten, welche die Herausgabe nicht bewilligt und auch die Empfangsberechtigung nicht anerkannt haben, eine Frist von mindestens einem Monat setzen, binnen deren sie ihr die Erhebung der Klage wegen ihrer Ansprüche nachzuweisen haben.
Sie soll jedoch von dieser Möglichkeit nur Gebrauch machen, wenn es unbillig wäre, von der Antrag stellenden Person weitere Nachweise zu verlangen.

(2) Die Bestimmung der Frist ist der Person, die die Herausgabe beantragt hat, und den Personen, an die sie sich richtet, nach den Vorschriften der Zivilprozessordnung über die Zustellung von Amts wegen bekannt zu geben.
Sie unterliegt der Beschwerde, die binnen zwei Wochen seit dem Zeitpunkt der Zustellung bei der Hinterlegungsstelle einzulegen ist.
Die Hinterlegungsstelle ist aufgrund der Beschwerde zu einer Änderung ihrer Entscheidung befugt.
Hilft sie nicht ab, hat sie die Beschwerde der Präsidentin oder dem Präsidenten des Land- oder Amtsgerichts vorzulegen.

(3) Die Entscheidung der Präsidentin oder des Präsidenten des Land- oder Amtsgerichts ist nach Absatz 2 Satz 1 bekannt zu geben.
Eine weitere Beschwerde ist nicht zulässig.
§ 5 Absatz 2 bleibt unberührt.

(4) Eine verspätet eingelegte Beschwerde kann, solange noch nicht herausgegeben ist, von der Präsidentin oder dem Präsidenten des Land- oder Amtsgerichts zugelassen werden.

(5) Die Frist nach Absatz 1 Satz 1 beginnt mit der Rechtskraft der sie bestimmenden Verfügung.
Nach Ablauf dieser Frist gilt die Herausgabe als bewilligt, wenn nicht inzwischen der Hinterlegungsstelle die Erhebung der Klage nachgewiesen ist.

#### § 25  
Herausgabeort, Haftung nach der Herausgabe

(1) Das Land ist nicht verpflichtet, die Hinterlegungsmasse an einem anderen Ort als dem Sitz der Hinterlegungsstelle herauszugeben.

(2) Nach der Herausgabe kann das Land nur aufgrund der Vorschriften über die Haftung für Amtspflichtverletzungen der Beamtinnen und Beamten in Anspruch genommen werden.

### Abschnitt 6  
Erlöschen des Anspruchs auf Herausgabe

#### § 26   
Einunddreißigjährige Frist

(1) In den Fällen der §§ 382 und 1171 des Bürgerlichen Gesetzbuches, des § 67 des Gesetzes über Rechte an eingetragenen Schiffen und Schiffsbauwerken und in den Fällen des § 117 Absatz 2 und der §§ 120, 121, 124, 126 des Gesetzes über die Zwangsversteigerung und die Zwangsverwaltung erlischt der Anspruch auf Herausgabe mit dem Ablauf von 31 Jahren, wenn nicht zu diesem Zeitpunkt ein begründeter Antrag auf Herausgabe vorliegt.

(2) Die Frist beginnt

1.  im Fall des § 382 des Bürgerlichen Gesetzbuches mit dem Zeitpunkt, in dem die Gläubigerin oder der Gläubiger die Anzeige von der Hinterlegung empfangen hat, oder, falls die Anzeige untunlich war und deshalb unterblieben ist, mit der Hinterlegung;
    
2.  in den Fällen des § 1171 Absatz 3 des Bürgerlichen Gesetzbuches sowie des § 67 des Gesetzes über Rechte an eingetragenen Schiffen und Schiffsbauwerken mit dem Erlass des Beschlusses, durch den die Gläubigerin oder der Gläubiger mit ihrem oder seinem Recht ausgeschlossen ist; das Gericht hat den Ausschließungsbeschluss der Hinterlegungsstelle mitzuteilen;
    
3.  in den Fällen des § 117 Absatz 2 und der §§ 124, 126 des Gesetzes über die Zwangsversteigerung und die Zwangsverwaltung mit der Hinterlegung;
    
4.  in den Fällen der §§ 120, 121 des Gesetzes über die Zwangsversteigerung und die Zwangsverwaltung mit dem Zeitpunkt, in dem die Bedingung eingetreten ist, unter der hinterlegt ist; kann der Eintritt der Bedingung nicht ermittelt werden, beginnt die Frist mit dem Ablauf von zehn Jahren seit der Hinterlegung oder, wenn die Bedingung erst in einem späteren Zeitpunkt eintreten konnte, mit dem Ablauf von zehn Jahren seit diesem Zeitpunkt.
    

#### § 27  
Dreißigjährige Frist

(1) In den übrigen Fällen erlischt der Anspruch auf Herausgabe mit dem Ablauf von 30 Jahren nach der Hinterlegung, wenn nicht zu diesem Zeitpunkt ein begründeter Antrag auf Herausgabe vorliegt.

(2) Bei Hinterlegungen aufgrund der §§ 1667, 1814, 1818 und 1915 des Bürgerlichen Gesetzbuches müssen außerdem 20 Jahre seit dem Zeitpunkt abgelaufen sein, in dem die elterliche Sorge, die Betreuung, die Vormundschaft oder Pflegschaft beendet ist.
In den Fällen der Abwesenheitspflegschaft genügt der Ablauf der in Absatz 1 bestimmten Frist.

#### § 28  
Erneuter Fristbeginn

Hat eine beteiligte Person in den Fällen des § 27 innerhalb der Frist angezeigt und nachgewiesen, dass die Veranlassung zur Hinterlegung fortbesteht, beginnt die Frist mit dem Zeitpunkt, in dem die Anzeige eingegangen ist, von neuem.

#### § 29   
Verfall der Hinterlegungsmasse

Mit dem Erlöschen des Anspruchs auf Herausgabe fällt die Hinterlegungsmasse dem Land zu.

### Abschnitt 7  
Hinterlegung in besonderen Fällen

#### § 30   
Genehmigung der Aufsichtsbehörde einer Stiftung

In Fällen, in denen Gegenstände, die zu dem Vermögen einer Stiftung gehören, aufgrund stiftungsrechtlicher Vorschriften oder Anordnungen hinterlegt sind, ist zur Herausgabe die Genehmigung der Aufsichtsbehörde der Stiftung erforderlich; zur Herausgabe von Erträgen bedarf es dieser Genehmigung nicht.
Die Aufsichtsbehörde der Stiftung kann etwas anderes bestimmen.

### Abschnitt 8   
Kosten

#### § 31   
Anwendung des Justizverwaltungskostengesetzes, Gebührenverzeichnis

(1) In Hinterlegungssachen werden Kosten (Gebühren und Auslagen) nach dem Justizverwaltungskostengesetz erhoben.
Nummer 2001 und der Hauptabschnitt 1 Abschnitt 4 des Kostenverzeichnisses zum Justizverwaltungskostengesetz finden keine Anwendung; Nummer 2000 Unternummer 2 und Nummer 2002 des Kostenverzeichnisses zum Justizverwaltungskostengesetz finden keine Anwendung, soweit die Überlassung oder Bereitstellung gerichtlicher Entscheidungen zur Veröffentlichung in Entscheidungssammlungen und Fachzeitschriften beantragt wird.

(2) Ergänzend gelten die §§ 32 bis 34 und das anliegende Gebührenverzeichnis (Anlage).

#### § 32  
Festsetzung der Gebührenhöhe

Die Höhe der Gebühr setzt bei den Gebühren nach Nummer 1 des Gebührenverzeichnisses die Hinterlegungsstelle, bei den Gebühren nach den Nummern 3 und 4 des Gebührenverzeichnisses die Stelle, die über die Beschwerde zu entscheiden hat, fest.

#### § 33   
Auslagen

Neben den Auslagen nach dem Justizverwaltungskostengesetz werden als Auslagen erhoben:

1.  die Beträge, die bei der Umwechslung von Zahlungsmitteln nach § 11 Absatz 2 Satz 2 oder bei der Vornahme von Geschäften nach § 13 an Banken oder an andere Stellen zu zahlen sind,
    
2.  Dokumentenpauschalen für Abschriften, die anzufertigen sind, weil ein Antrag auf Annahme nicht in der erforderlichen Anzahl gemäß § 8 Absatz 1 Satz 1 vorgelegt worden ist,
    
3.  sonstige Auslagen nach den Nummern 31001 bis 31006, 31008, 31009, 31013 und 31014 des Kostenverzeichnisses zum Gerichts- und Notarkostengesetz.
    

#### § 34  
Kosten

(1) Die Kosten werden bei der Hinterlegungsstelle angesetzt.

(2) Zuständig für Entscheidungen nach § 22 des Justizverwaltungskostengesetzes ist das Amtsgericht, bei dem die Hinterlegungsstelle eingerichtet ist.
Das Gleiche gilt für Einwendungen gegen Maßnahmen nach Absatz 3 Nummer 2 und 3.

(3) Im Übrigen gilt für die Kosten Folgendes:

1.  Zur Zahlung der Kosten ist auch die empfangsberechtigte Person, an die oder für deren Rechnung die Herausgabe verfügt wurde, sowie diejenige Person verpflichtet, in deren Interesse eine Behörde um die Hinterlegung ersucht hat.
    
2.  Die Kosten können der Masse entnommen werden, soweit es sich um Geld handelt, das in das Eigentum des Landes übergegangen ist.
    
3.  Die Herausgabe hinterlegter Sachen kann von der Zahlung der Kosten abhängig gemacht werden.
    
4.  Die Nummern 1 bis 3 sind auf Kosten, die für das Verfahren über Beschwerden erhoben werden, nur anzuwenden, soweit diejenige Person, der die Kosten dieses Verfahrens auferlegt worden sind, empfangsberechtigt ist.
    
5.  Kosten sind nicht zu erheben oder sind, falls sie erhoben wurden, zu erstatten, wenn die Hinterlegung aufgrund der §§ 116 und 116a der Strafprozessordnung erfolgte, um eine beschuldigte Person von der Untersuchungshaft zu verschonen, und die beschuldigte Person rechtskräftig außer Verfolgung gesetzt oder freigesprochen oder das Verfahren gegen sie eingestellt wird; ist der Verfall der Sicherheit rechtskräftig ausgesprochen worden, so werden bereits erhobene Kosten nicht erstattet.
    
6.  Ist bei Vormundschaften sowie bei Betreuungen, Pflegschaften für Minderjährige und in den Fällen des § 1667 des Bürgerlichen Gesetzbuches aufgrund gesetzlicher Verpflichtung oder Anordnung des Betreuungs- oder Familiengerichts hinterlegt, gilt die Vorbemerkung 1.1 Absatz 1 des Kostenverzeichnisses zum Gerichts- und Notarkostengesetz entsprechend.
    Die Verjährung des Anspruchs auf Zahlung der Kosten hindert das Land nicht, nach den Nummern 2 und 3 zu verfahren.
    
7.  § 4 Absatz 3 des Justizverwaltungskostengesetzes findet keine Anwendung.
    

### Abschnitt 9  
Übergangs- und Schlussbestimmungen

#### § 35  
Anhängige Hinterlegungssachen

Hinterlegungssachen, die bei Inkrafttreten dieses Gesetzes nach Maßgabe der Hinterlegungsordnung anhängig sind, werden nach Maßgabe dieses Gesetzes weitergeführt.
Gleiches gilt für anhängige Rechtsbehelfe und Rechtsmittel.

#### § 36   
Einschränkung eines Grundrechts

Durch die §§ 14 bis 19 wird das Grundrecht auf Datenschutz (Artikel 11 der Verfassung des Landes Brandenburg) eingeschränkt.

**Anlage  
**(zu § 31 Absatz 2)

#### Gebührenverzeichnis

Nr.

Gegenstand

Gebühren

1

Hinterlegung von Wertpapieren, sonstigen Urkunden, Kostbarkeiten und von unverändert aufzubewahrenden Zahlungsmitteln (§ 11 Absatz 2 Satz 1) in jeder Angelegenheit, in der eine besondere Annahmeanordnung ergeht

8 bis 255 €

2

Anzeige gemäß § 14 Absatz 1 Satz 2

Anmerkung:

Neben der Gebühr für die Anzeige werden nur die Auslagen nach den Nummern 31002 und 31003 des Kostenverzeichnisses zum Gerichts- und Notarkostengesetz erhoben.

8 €

3

Zurückweisung der Beschwerde

8 bis 255 €

4

Zurücknahme der Beschwerde

8 bis 65 €