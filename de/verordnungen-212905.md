## Verordnung über den Schutzwald „Naturwald Zootzen“

Auf Grund des § 12 des Waldgesetzes des Landes Brandenburg vom 20.
April 2004 (GVBl.
I S.
137) verordnet der Minister für Infrastruktur und Landwirtschaft:

§ 1  
Erklärung zum Schutzwald
------------------------------

Die in § 2 näher bezeichneten Waldflächen mit besonderer Schutzfunktion als Naturwald werden zum Schutzwald erklärt.
Der Schutzwald trägt die Bezeichnung „Naturwald Zootzen“ und wird in das Register der geschützten Waldgebiete aufgenommen.

§ 2  
Schutzgegenstand
----------------------

(1) Das geschützte Waldgebiet befindet sich im Landkreis Havelland und hat eine Größe von rund 45 Hektar.
Es umfasst folgende Flurstücke:

Gemeinde:

Gemarkung:

Flur:

Flurstück:

Friesack

Zootzen

8

4/1, 7 (anteilig), 28/1.

(2) Zur Orientierung ist dieser Verordnung eine Kartenskizze über die Lage des Schutzwaldes als Anlage beigefügt.

(3) Die Grenzen des Schutzwaldes sind in der „Topografischen Karte zur Verordnung über den Schutzwald ‚Naturwald Zootzen‘“, Maßstab 1 : 10000 und in der „Liegenschaftskarte zur Verordnung über den Schutzwald ‚Naturwald Zootzen‘“, Maßstab 1 : 10000 mit ununterbrochener Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Maßgeblich ist die Einzeichnung in der Liegenschaftskarte.
 Die Karten sind mit dem Dienstsiegel des Ministeriums für Infrastruktur und Landwirtschaft, Siegelnummer 26, versehen und vom Siegelverwahrer am 12.
Mai 2014 unterschrieben worden.

(4) Die Verordnung mit Karten kann beim Ministerium für Infrastruktur und Landwirtschaft in Potsdam, oberste Forstbehörde, sowie beim Landesbetrieb Forst Brandenburg in Potsdam, untere Forstbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

§ 3  
Schutzzweck
-----------------

(1) Schutzzweck des Schutzwaldes ist

1.  die Erhaltung und Entwicklung eines grundwasserbeeinflussten Laubmischwaldes zum Zwecke der wissenschaftlichen Beobachtung und Erforschung der naturnahen Entwicklung des Waldes;
    
2.  die Erhaltung und Wiederherstellung der Leistungsfähigkeit des Naturhaushaltes, insbesondere des Wasserhaushaltes des vom alten Rhin gespeisten Waldgebietes;
    
3.  die Erhaltung und Entwicklung des Gebietes als Lebensraum gefährdeter Pflanzen- und Tierarten.
    

(2) Der besondere Schutzzweck ist der Erhalt des Naturwaldes insbesondere zur

1.  Repräsentation eines besonders wertvollen Restbestandes eines grundwasserbeeinflussten eutrophen Laubmischwaldes im nördlichen Havelland;
    
2.  Erforschung der Waldstruktur, des Bodens, der Flora und der Fauna, sowie der natürlichen Selbstorganisationsprozesse des Naturwaldes, welcher im Jahr 1961 teilweise aus der forstlichen Nutzung entlassen wurde;
    
3.  Nutzung als lokale und regionale Weiserfläche zur Ableitung und exemplarischen Veranschaulichung von Erkenntnissen für die Waldbaupraxis und forstliche Lehre;
    
4.  Erhaltung und Regeneration forstgenetischer Ressourcen;
    
5.  Erhaltung der floristischen und faunistischen Artenvielfalt, insbesondere der Fledermausvorkommen angrenzender Waldgebiete, in sich natürlich entwickelnden Lebensgemeinschaften.
    

(3) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teils des Gebietes von gemeinschaftlicher Bedeutung „Friesacker Zootzen“ mit der Gebietsnummer DE 3241-301 im Sinne des § 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes mit seinem Vorkommen von subatlantischem oder mitteleuropäischem Stieleichenwald oder Hainbuchenwald (Carpinion betuli) \[Stellario-Carpinetum\] und Auenwäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion, Alnion incanae, Salicion albae) als Biotope von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG).

(4) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teils des Europäischen Vogelschutzgebietes „Rhin-Havelluch“ mit der Gebietsnummer DE 3242-421 im Sinne des § 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes in seiner Funktion als Lebensraum von Arten nach Anhang I der Vogelschutz-Richtlinie, beispielsweise Rotmilan (Milvus milvus), Schwarzmilan (Milvus migrans), Wespenbussard (Pernis apivorus), Zwergschnäpper (Ficedula parva), Schwarzspecht (Dryocopus martius) und Mittelspecht (Dendrocopos medius) einschließlich ihrer Brut- und Nahrungsbiotope.

§ 4  
Verbote
-------------

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind im Schutzwald gemäß § 12 Absatz 6 des Waldgesetzes des Landes Brandenburg alle Handlungen verboten, die dem in § 3 genannten Schutzzweck zuwiderlaufen und das Gebiet oder einzelne seiner Bestandteile nachhaltig stören, verändern, beschädigen oder zerstören können.

(2) Es ist insbesondere verboten:

1.  das Gebiet forstwirtschaftlich zu nutzen;
    
2.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
    
3.  das Gebiet außerhalb der Waldwege zu betreten;
    
4.  im Gebiet mit motorisierten Fahrzeugen und Gespannen zu fahren oder diese dort abzustellen;
    
5.  zu zelten, zu lagern, Wohnwagen aufzustellen, zu rauchen und Feuer anzuzünden;
    
6.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
    
7.  die Bodengestalt zu verändern, Böden zu verfestigen oder zu versiegeln;
    
8.  Düngemittel einschließlich Wirtschaftsdünger und Sekundärrohstoffdünger zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
    
9.  Pflanzenschutzmittel jeder Art oder Holzschutzmittel anzuwenden;
    
10.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten.
    

§ 5  
Zulässige Handlungen
--------------------------

(1) Ausgenommen von den Verboten des §4 bleiben folgende Handlungen:

1.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen;
    
2.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Forstbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    

(2) Die in § 4 für das Betreten und Befahren des Schutzwaldes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Forstbehörden und sonstige von den Forstbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer und Nutzer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums.

§ 6  
Pflege- und Entwicklungsmaßnahmen
---------------------------------------

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  Auf den Teilflächen 7442 a1, a2, a9, a10, b1, b5 ist bis Ende des Jahres 2018 die Spätblühende Traubenkirsche (Prunus serotina) in Abstimmung mit der unteren Naturschutzbehörde mechanisch zu entfernen.
    
2.  Um biotopbeeinträchtigende Mineralisierungsprozesse zu vermeiden, sollen Maßnahmen zur Gewährleistung eines ausreichenden Wasserstandes umgesetzt werden.
    

§ 7  
Befreiungen
-----------------

Wenn überwiegende Gründe des Gemeinwohls es erfordern, kann die untere Forstbehörde im Einvernehmen mit der unteren Naturschutzbehörde auf Antrag Befreiungen von den Verboten dieser Verordnung gewähren.

§ 8  
Ordnungswidrigkeiten
--------------------------

(1) Ordnungswidrig im Sinne des § 37 Absatz 1 Nummer 7 des Waldgesetzes des Landes Brandenburg handelt, wer vorsätzlich oder fahrlässig den Verboten oder den Maßgaben des § 4 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 37 Absatz 3 des Waldgesetzes des Landes Brandenburg mit einer Geldbuße bis zu zwanzigtausend Euro geahndet werden.

§ 9  
Verhältnis zu anderen rechtlichen Bestimmungen
----------------------------------------------------

(1) Die Regelungen naturschutzrechtlicher Schutzgebietsausweisungen im Bereich des in § 2 genannten Gebietes bleiben unberührt.

(2) Die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) bleiben unberührt.

§ 10  
Geltendmachung von Rechtsmängeln
---------------------------------------

Eine Verletzung der in § 1 der Waldschutzgebietsverfahrensverordnung genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber der obersten Forstbehörde geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

§ 11  
Inkrafttreten
--------------------

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 19.
Juni 2014

Der Minister für Infrastruktur und Landwirtschaft

Jörg Vogelsänger

### Anlagen

1

[Kartenskizze zur Verordnung über den Schutzwald "Naturwald Zootzen"](/br2/sixcms/media.php/68/GVBl_II_36_2014-Anlage.pdf "Kartenskizze zur Verordnung über den Schutzwald ") 774.4 KB