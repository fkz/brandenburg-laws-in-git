## Gesetz über den Mehrbelastungsausgleich für kommunale Straßenausbaumaßnahmen

#### § 1 Mehrbelastungsausgleich

(1) Das Land gewährt den Gemeinden gemäß Artikel 97 Absatz 3 Satz 2 und 3 der Verfassung des Landes Brandenburg die entsprechenden Mittel zum vollständigen Ausgleich der Mehrbelastungen, die durch das Erhebungsverbot für Straßenbaubeiträge nach § 8 Absatz 1 Satz 2 des Kommunalabgabengesetzes für das Land Brandenburg ab dem 1.
Januar 2019 entstehen.
Der Mehrbelastungsausgleich erfolgt auf der Grundlage einer nach § 2 zu erlassenden Rechtsverordnung und soll gemäß den Angaben (zu den gewidmeten Gemeindestraßen) in den amtlichen Nachweisen der Geotopographie zum 31.
Dezember des Vorjahres über jährliche pauschalierte Zahlungen gewährt werden.

(2) Das Land erstattet zudem den Gemeinden auf Antrag die Rückzahlungen von Straßenbaubeiträgen und Vorausleistungen, die sie aufgrund von § 20 Absatz 4 und 5 des Kommunalabgabengesetzes für das Land Brandenburg geleistet haben.
Die Erstattung erfolgt zuzüglich einer Verwaltungskostenpauschale in Höhe von 10 Prozent des Erstattungsbetrages nach Satz 1.

(3) Soweit die pauschalierte Zahlung nach Absatz 1 die entstehende Mehrbelastung einer Gemeinde nicht vollständig deckt, gleicht das Land dieser Gemeinde den Fehlbetrag auf Antrag aus.
Im Antrag ist die Höhe der Mehrbelastung im Einzelnen nachzuweisen.
Maßgeblich für die Berechnung der Mehrbelastung einer Gemeinde ist die entsprechende Satzung für Straßenbaubeiträge der jeweiligen Gemeinde in der am 31.
Dezember 2018 geltenden Fassung.

(4) Die Regelungen zum Mehrbelastungsausgleich nach den Absätzen 1 und 3 sind im Jahr 2023 zu evaluieren.

#### § 2 Freiwilliger anwohnerfinanzierter Straßenausbau

§ 8 Absatz 1 Satz 2 des Kommunalabgabengesetzes lässt die freiwillige Übernahme von Straßenausbaukosten durch die Anwohnerinnen und Anwohner unberührt.
Freiwilliger anwohnerfinanzierter Straßenausbau ist zulässig.

#### § 3 Verordnungsermächtigung

Das für Straßenwesen zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit den für Inneres und für Finanzen zuständigen Mitgliedern der Landesregierung durch Rechtsverordnung zu dem nach Artikel 97 Absatz 3 Satz 2 und 3 der Verfassung des Landes Brandenburg gebotenen Ausgleich der Mehrbelastung der Gemeinden infolge des Erhebungsverbots für Straßenbaubeiträge nach § 8 Absatz 1 Satz 2 des Kommunalabgabengesetzes für das Land Brandenburg Regelungen zu treffen

1.  über die zuständige Stelle für die Prüfung und Gewährung des Mehrbelastungsausgleichs nach § 1,
2.  für den pauschalen Mehrbelastungsausgleich nach § 1 Absatz 1
    1.  über den Einnahmeausfall aus Straßenbaubeiträgen,
    2.  über die Höhe der pauschalen Zahlungen für den Mehrbelastungsausgleich und ihre jeweilige Anpassung an die Kostenentwicklung,
3.  für die Erstattung von Rückzahlungen nach § 1 Absatz 2
    1.  über das Antrags- und Nachweisverfahren,
    2.  über die Einbeziehung von Rückzahlungen von Beträgen, die aufgrund von Vereinbarungen zur Ablösung von Beiträgen für Maßnahmen im Sinne des § 20 Absatz 4 des Kommunalabgabengesetzes für das Land Brandenburg abgeschlossen wurden,
4.  für den Ausgleichsbetrag nach § 1 Absatz 3
    1.  über das Antrags- und Nachweisverfahren,
    2.  über die Berücksichtigung von pauschalen Mehrbelastungsausgleichen anderer Jahre,
5.  über die Auskunftspflichten der Gemeinden zur Ermittlung des pauschalen Mehrbelastungsausgleichs nach § 1 Absatz 1 und die Durchführung der Evaluierung nach § 1 Absatz 4.