## Verordnung über das Naturschutzgebiet „Jakobsdorfer Feuchtland“

§ 1Auf Grund der §§ 22, 23 und § 32 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

#### § 1   
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Prignitz wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Jakobsdorfer Feuchtland“.

#### § 2  
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 138 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Gemeinde:**  

**Gemarkung:**  

**Flur:**

Groß Pankow  

Helle  

1;

Putlitz  

Laaske  

3, 4 und 5.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 bei-gefügt.
Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 3 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte topografische Karte im Maßstab 1 : 10 000 ermöglicht die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 4 aufgeführten Liegenschaftskarten.

(3) Innerhalb des Naturschutzgebietes sind eine Zone 1a mit rund 13 Hektar sowie eine Zone 1b mit rund 5 Hektar mit besonderen Beschränkungen der landwirtschaftlichen Nutzung festgelegt.

Die Zone 1a liegt in folgender Flur:

**Gemeinde:**  

**Gemarkung:**  

**Flur:**

Putlitz  

Laaske  

5.

Die Zone 1b liegt in folgenden Fluren:

**Gemeinde:**  

**Gemarkung:**  

**Flur:**

Putlitz  

Laaske  

3 und 5.

Die Grenzen der Zonen 1a und 1b sind in der in Anlage 2 Nummer 1 genannten topografischen Karte und in den in Anlage 2 Nummer 2 aufgeführten Liegenschaftskarten mit den Blattnummern 1 bis 4 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Prignitz, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3  
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung und Entwicklung der Lebensräume wild lebender Pflanzengesellschaften, insbesondere verschiedenartiger naturnaher Kleingewässer mit ausgedehnten Verlandungszonen, arten- und blütenreicher, extensiv genutzter Frisch- und Feuchtwiesen, strukturreicher, teilweise mit Bäumen überschirmter Hecken sowie Baumreihen;
    
2.  die Erhaltung und Entwicklung der Lebensräume wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten;
    
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Vögel, Amphibien und Insekten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Braunkehlchen (Saxicola rubetra), Kiebitz (Vanellus vanellus), Rebhuhn (Perdix perdix), Moorfrosch (Rana arvalis), Laubfrosch (Hyla arborea), Fischotter (Lutra lutra), Prächtiger Bläuling (Polyomatus amandus), Schachbrett-Falter (Melanargia galathea), Große Pechlibelle (Isch­nura elegans), Kleine Pechlibelle (Ischnura pumilio), Kleines Granatauge (Erythromma viridulum), Schwarze Heidelibelle (Sympetrum danae), Fledermaus-Azurjungfer (Coenagrion pulchellum) und Speer-Azurjungfer (Coenagrion hastulatum);
    
4.  die Erhaltung und Entwicklung des zentralen Feuchtgebietes mit seinen naturnahen Gewässern wegen seiner regionalen Bedeutung als Schlaf- und Rastplatz der Kraniche;
    
5.  die Erhaltung und Entwicklung einer strukturreichen Agrarlandschaft mit einem hohen Anteil an Strukturelementen wie Hecken, Baumreihen, Söllen, Gräben und Randstreifen wegen ihrer Vielfalt;
    
6.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Bestandteil des regionalen Biotopverbundes zwischen der Kümmernitzniederung und dem Gewässersystem der Stepenitz.
    

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teiles des Europäischen Vogelschutzgebietes „Agrarlandschaft Prignitz-Stepenitz“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) in seiner Funktion

1.  als Lebensraum von Arten nach Anhang I der Richtlinie 2009/147/EG, insbesondere Schwarzspecht (Dryocopus martius), Kranich (Grus grus), Rotmilan (Milvus milvus) und Schwarzstorch (Ciconia nigra) einschließlich ihrer Brut- und Nahrungsbiotope,
    
2.  als störungsarmes Rast- und Überwinterungsgebiet für im Gebiet regelmäßig auftretende Zugvogelarten wie Kiebitz (Vanellus vanellus), Kranich (Grus grus) und nordische Gänse.
    

#### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
    
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
    
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
    
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
    
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
    
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
    
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
    
8.  die Ruhe der Natur durch Lärm zu stören;
    
9.  das Gebiet außerhalb von Wegen zu betreten, darüber hinaus ist es verboten die Zone 1a in der Zeit vom 1.
    Februar bis zum 15.
    April eines jeden Jahres zu betreten;
    
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 51 des Brandenburgischen Naturschutzgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
    
11.  mit Fahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen; ausgenommen ist das Befahren von Wegen mit nicht motorisierten Fahrzeugen in der Zeit vom 16.
    April eines jeden Jahres bis zum 31.
    Januar des Folgejahres.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzgesetzes und des Waldgesetzes des Landes Brandenburg;
    
12.  Wasserfahrzeuge aller Art zu benutzen;
    
13.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
    
14.  Hunde frei laufen zu lassen;
    
15.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
    
16.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (zum Beispiel Abwasser, Klärschlamm und Bioabfälle) zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
    
17.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
    
18.  Tiere zu füttern oder Futter bereitzustellen;
    
19.  Tiere auszusetzen oder Pflanzen anzusiedeln;
    
20.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
    
21.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
    
22.  Pflanzenschutzmittel jeder Art anzuwenden;
    
23.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.
    

#### § 5   
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  Grünland innerhalb der Zone 1a und 1b als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel und Sekundärrohstoffdünger wie zum Beispiel Abwasser, Klärschlamm und Bioabfälle einzusetzen.
        Der Einsatz von Kartoffelfruchtwasser bleibt zulässig,  
        
    2.  auf landwirtschaftlich genutzten Flächen der Anbau schnellwüchsiger Gehölze im Kurzumtrieb nur mit Genehmigung der unteren Naturschutzbehörde zulässig ist,
        
    3.  in der Zone 1a und 1b auf Grünland § 4 Absatz 2 Nummer 22 und 23 gilt, wobei bei Narbenschäden mit Zustimmung der unteren Naturschutzbehörde eine umbruchlose Nachsaat zulässig ist;
        
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  Horst- und Höhlenbäume nicht entfernt werden,
        
    2.  Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von 0,5 Hektar zulässig sind,
        
    3.  § 4 Absatz 2 Nummer 22 gilt;
        
3.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass diese in der Zeit vom 1.
        Februar bis zum 15.
        April eines jeden Jahres unzulässig ist, davon ausgenommen bleibt die Bejagung von Schwarzwild zur Verhütung von Schäden an den im Gebiet liegenden Ackerflächen im Abstand von 100 Metern zur Zone 1a, sofern dies nicht zu einer erheblichen Störung des Kranichschlafplatzes führt;
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Errichtung anzuzeigen.
        Die Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht,
        
    3.  die Anlage von Kirrungen außerhalb geschützter Biotope und außerhalb der Offenflächen im zentralen, von Hecken umschlossenen Bereich der Zone 1a.
        
    
    Im Übrigen bleiben Wildfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern im gesamten Gebiet unzulässig;
    
4.  erforderliche Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne einer Kontrolle, Regulierung und Förderung des Fischbestands nach vorheriger Anzeige bei der unteren Naturschutzbehörde.
    Die untere Naturschutzbehörde kann die Maßnahmen verbieten oder mit Nebenbestimmungen versehen, wenn sie dem Schutzzweck entgegenstehen;
    
5.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
    
6.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
    
7.  der Betrieb von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
     Die ordnungsgemäße Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen kann durch Abstimmung eines Unterhaltungsplanes erteilt werden;
    
8.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
    
9.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
    
10.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der unteren Naturschutzbehörde gebilligt oder angeordnet worden sind;
    
11.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen. Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 an Straßen und Wegen freigestellt;
    
12.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
 Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Ge­stattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

#### § 6  
Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  die Offenflächen im zentralen, von Hecken umschlossenen Bereich der Zone 1a sollen durch einschürige Mahd gepflegt werden;
    
2.  Hecken und Baumreihen sollen durch Nachpflanzungen und Lückenschließungen erhalten werden, wobei heimische, standortgerechte Gehölze verwendet werden sollen;
    
3.  im Bereich des Kranich-Schlafplatzes in der Zone 1a soll die Wasserhaltung durch die Nutzung vorhandener Regulierungseinrichtungen so durchgeführt werden, dass sich ab dem 1.
    November eines jeden Jahres bis zum 30.
    April des Folgejahres oberflächennahe Grundwasserstände mit Blänkenbildung einstellen können;
    
4.  naturferne Abschnitte des Laasker Vorfluters sollen renaturiert werden, beispielsweise durch Förderung der natürlichen Gewässerdynamik.
    

#### § 7  
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu 50 000 (in Worten: fünfzigtausend) Euro geahndet werden.

#### § 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere §§ 31 bis 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

#### § 10  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11  
Inkrafttreten

§ 5 Absatz 1 Nummer 1 Buchstabe a tritt am 1.
Juli 2011 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 5.
Januar 2011

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

**Anlage 1** (zu § 2 Absatz 1)

![Das Naturschutzgebiet Jakobsdorfer Feuchtland liegt in der Gemeinde Groß Pankow, Gemarkung Helle, und der Gemeinde Putlitz, Gemarkung Laaske.  Das Gebiet liegt in der Nähe der Ortslage Jakobsdorf. ](/br2/sixcms/media.php/69/Nr.%205-1.JPG "Das Naturschutzgebiet Jakobsdorfer Feuchtland liegt in der Gemeinde Groß Pankow, Gemarkung Helle, und der Gemeinde Putlitz, Gemarkung Laaske.  Das Gebiet liegt in der Nähe der Ortslage Jakobsdorf. ")

**Anlage 2  
**(zu § 2 Absatz 2)

**1.
 Topografische Karten Maßstab 1 : 10 000**

**Titel:** Topografische Karte zur Verordnung über das Naturschutzgebiet „Jakobsdorfer Feuchtland“

**Blattnummer**

**Unterzeichnung**

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV), am 2.
November 2010

**2.Liegenschaftskarten im Maßstab 1 : 2 500**

**Titel:** Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Jakobsdorfer Feuchtland“

**Blattnummer**

**Gemarkung**

**Flur**

**Unterzeichnung**

1

Helle

Laaske

1

3 und 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
November 2010

2

Laaske

3, 4 und 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
November 2010

3

Helle

Laaske

1

5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
November 2010

4

Laaske

5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
November 2010

**Anlage 3  
**(zu § 2 Absatz 1)

**Flurstücksliste zur Verordnung über das Naturschutzgebiet „Jakobsdorfer Feuchtland“**

**Landkreis: Prignitz**

**Gemeinde**

**Gemarkung**

**Flur**

**Flurstück**

**Kartenblattnummer**

**Geteilt\***

Groß Pankow (Prignitz)

Helle

1

146

01

Ja

Groß Pankow (Prignitz)

Helle

1

148

01; 03

Ja

Groß Pankow (Prignitz)

Helle

1

149

01; 03

Nein

Groß Pankow (Prignitz)

Helle

1

150

01; 03

Nein

Groß Pankow (Prignitz)

Helle

1

151

01; 03

Nein

Groß Pankow (Prignitz)

Helle

1

152

03

Ja

Groß Pankow (Prignitz)

Helle

1

153

03

Nein

Groß Pankow (Prignitz)

Helle

1

154

03

Nein

Groß Pankow (Prignitz)

Helle

1

155

03

Nein

Groß Pankow (Prignitz)

Helle

1

157/5

03

Ja

Groß Pankow (Prignitz)

Helle

1

157/6

03

Ja

Groß Pankow (Prignitz)

Helle

1

159

03

Ja

Groß Pankow (Prignitz)

Helle

1

160

03

Ja

Putlitz

Laaske

3

92

02

Ja

Putlitz

Laaske

3

100

01; 02

Nein

Putlitz

Laaske

3

101

01; 02

Ja

Putlitz

Laaske

3

102

02

Ja

Putlitz

Laaske

3

103

02

Ja

Putlitz

Laaske

3

104

02

Nein

Putlitz

Laaske

3

105

02

Nein

Putlitz

Laaske

3

106

02

Nein

Putlitz

Laaske

3

107

01; 02

Nein

Putlitz

Laaske

3

108

01; 02

Nein

Putlitz

Laaske

3

109

02

Nein

Putlitz

Laaske

3

110

02

Nein

Putlitz

Laaske

3

111

02

Nein

Putlitz

Laaske

3

112

02

Nein

Putlitz

Laaske

3

113

02

Nein

Putlitz

Laaske

4

159

02

Ja

Putlitz

Laaske

4

160

02

Nein

Putlitz

Laaske

4

161

02

Ja

Putlitz

Laaske

4

162

02

Ja

Putlitz

Laaske

4

163

02

Ja

Putlitz

Laaske

4

164

02

Ja

Putlitz

Laaske

4

175

02

Ja

Putlitz

Laaske

5

20

02

Ja

Putlitz

Laaske

5

21

02

Ja

Putlitz

Laaske

5

22

02

Nein

Putlitz

Laaske

5

23

02

Nein

Putlitz

Laaske

5

24

02

Nein

Putlitz

Laaske

5

27

02

Ja

Putlitz

Laaske

5

28

02

Ja

Putlitz

Laaske

5

29

02

Nein

Putlitz

Laaske

5

30

02

Nein

Putlitz

Laaske

5

31

02

Nein

Putlitz

Laaske

5

32/1

02

Ja

Putlitz

Laaske

5

33

02

Ja

Putlitz

Laaske

5

95

04

Nein

Putlitz

Laaske

5

96

04

Nein

Putlitz

Laaske

5

109

03

Ja

Putlitz

Laaske

5

112

03

Ja

Putlitz

Laaske

5

113

03

Nein

Putlitz

Laaske

5

114

03

Nein

Putlitz

Laaske

5

115/1

03

Ja

Putlitz

Laaske

5

115/2

01; 03

Nein

Putlitz

Laaske

5

116

03

Ja

Putlitz

Laaske

5

117

03

Ja

Putlitz

Laaske

5

118

03

Nein

Putlitz

Laaske

5

119

03

Nein

Putlitz

Laaske

5

120

03

Nein

Putlitz

Laaske

5

121

01; 03

Nein

Putlitz

Laaske

5

122

01

Nein

Putlitz

Laaske

5

123

01

Nein

Putlitz

Laaske

5

124

01

Nein

Putlitz

Laaske

5

125

01

Nein

Putlitz

Laaske

5

126

01

Nein

Putlitz

Laaske

5

127

01

Nein

Putlitz

Laaske

5

128

01

Nein

Putlitz

Laaske

5

129

01

Nein

Putlitz

Laaske

5

130

01

Nein

Putlitz

Laaske

5

131

01

Nein

Putlitz

Laaske

5

132

01

Nein

Putlitz

Laaske

5

133

01

Nein

Putlitz

Laaske

5

134

01

Nein

Putlitz

Laaske

5

135

01; 02

Nein

Putlitz

Laaske

5

136

01; 02

Nein

Putlitz

Laaske

5

137

01; 02

Nein

Putlitz

Laaske

5

138

01

Nein

Putlitz

Laaske

5

139

01

Nein

Putlitz

Laaske

5

140

01; 03

Nein

Putlitz

Laaske

5

141

01; 02; 03

Nein

Putlitz

Laaske

5

142

01; 02; 03; 04

Nein

Putlitz

Laaske

5

143

02; 03; 04

Nein

Putlitz

Laaske

5

144

02; 03; 04

Nein

Putlitz

Laaske

5

145

04

Nein

Putlitz

Laaske

5

146/2

02; 04

Nein

Putlitz

Laaske

5

147

02; 04

Nein

Putlitz

Laaske

5

148

02; 04

Nein

Putlitz

Laaske

5

149

02

Nein

Putlitz

Laaske

5

150

02; 04

Nein

Putlitz

Laaske

5

151

02; 04

Nein

Putlitz

Laaske

5

152

02

Nein

Putlitz

Laaske

5

153

02

Nein

Putlitz

Laaske

5

154

02

Nein

Putlitz

Laaske

5

155

02

Nein

Putlitz

Laaske

5

156

01; 02

Nein

Putlitz

Laaske

5

157

01; 02

Nein

Putlitz

Laaske

5

158

02

Nein

Putlitz

Laaske

5

159

02

Nein

Putlitz

Laaske

5

160

02

Nein

Putlitz

Laaske

5

161

02

Nein

Putlitz

Laaske

5

162

02

Nein

Putlitz

Laaske

5

163

02

Nein

Putlitz

Laaske

5

164

02

Nein

Putlitz

Laaske

5

165

02

Nein

Putlitz

Laaske

5

166

02

Nein

Putlitz

Laaske

5

167

02

Nein

Putlitz

Laaske

5

168

02

Nein

Putlitz

Laaske

5

169

02

Nein

Putlitz

Laaske

5

170

02

Nein

Putlitz

Laaske

5

171

02

Nein

Putlitz

Laaske

5

172

02

Nein

Putlitz

Laaske

5

173

02

Nein

Putlitz

Laaske

5

174

02; 04

Nein

Putlitz

Laaske

5

175

02

Ja

Putlitz

Laaske

5

176

04

Nein

Putlitz

Laaske

5

177

04

Ja

Putlitz

Laaske

5

178

02

Ja

Putlitz

Laaske

5

180

02

Ja

Putlitz

Laaske

5

182

02

Ja

Putlitz

Laaske

5

183

02

Ja

Putlitz

Laaske

5

184

02

Nein

\* Flurstück liegt vollständig im NSG = Nein / Flurstück liegt teilweise im NSG = Ja

Flächen der Zone 1a:

**Landkreis: Prignitz**

**Gemeinde**

**Gemarkung**

**Flur**

**Flurstück**

**Kartenblattnummer**

**Geteilt\***

Putlitz

Laaske

5

136

01; 02

Ja

Putlitz

Laaske

5

137

01; 02

Ja

Putlitz

Laaske

5

138

01

Ja

Putlitz

Laaske

5

141

01; 02

Ja

Putlitz

Laaske

5

142

02

Ja

Putlitz

Laaske

5

143

02

Ja

Putlitz

Laaske

5

144

02; 04

Ja

Putlitz

Laaske

5

146/2

02; 04

Ja

Putlitz

Laaske

5

147

02; 04

Nein

Putlitz

Laaske

5

148

02; 04

Nein

Putlitz

Laaske

5

149

02

Nein

Putlitz

Laaske

5

150

02; 04

Nein

Putlitz

Laaske

5

151

02

Ja

Putlitz

Laaske

5

152

02

Nein

Putlitz

Laaske

5

153

02

Nein

Putlitz

Laaske

5

154

02

Nein

Putlitz

Laaske

5

155

02

Nein

Putlitz

Laaske

5

156

01; 02

Ja

Putlitz

Laaske

5

157

02

Ja

Putlitz

Laaske

5

158

02

Nein

Putlitz

Laaske

5

159

02

Nein

\* Flurstück liegt vollständig in Zone 1a = Nein / Flurstück liegt teilweise im NSG = Ja

Flächen der Zone 1b:

**Landkreis: Prignitz**

**Gemeinde**

**Gemarkung**

**Flur**

**Flurstück**

**Kartenblattnummer**

**Geteilt\***

Putlitz

Laaske

3

101

01

Ja

Putlitz

Laaske

5

123

01

Ja

Putlitz

Laaske

5

128

01

Ja

Putlitz

Laaske

5

129

01

Nein

Putlitz

Laaske

5

130

01

Nein

Putlitz

Laaske

5

131

01

Nein

Putlitz

Laaske

5

132

01

Nein

Putlitz

Laaske

5

133

01

Nein

Putlitz

Laaske

5

134

01

Nein

\* Flurstück liegt vollständig in Zone 1b = Nein / Flurstück liegt teilweise im NSG = Ja