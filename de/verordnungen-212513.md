## Verordnung über den Schutzwald „Melzower Buchennaturwald“

Auf Grund des § 12 Absatz 1 des Waldgesetzes des Landes Brandenburg vom 20.
April 2004 (GVBl.
I S.
137) verordnet der Minister für Infrastruktur und Landwirtschaft:

#### § 1  
Erklärung zum Schutzwald

Die in § 2 näher bezeichneten Waldflächen mit überwiegend besonderer Schutzfunktion als Naturwald werden zum Schutzwald erklärt.
Der Schutzwald trägt die Bezeichnung „Melzower Buchennaturwald“ und wird in das Register der geschützten Waldgebiete aufgenommen.

#### § 2  
Schutzgegenstand

(1) Das geschützte Waldgebiet befindet sich im Landkreis Uckermark und hat eine Größe von rund 24 Hektar.
Es umfasst folgende Flurstücke:

Gemeinde:

Gemarkung:

Flur:

Flurstück:

Oberuckersee

Melzow

4

2, 82, 252, 253.

Die Waldfläche mit besonderer Schutzfunktion als Naturwald besteht aus einem Rotbuchen-Naturwald und hat eine Größe von rund 21 Hektar.
Sie umfasst folgende Flurstücke:

Gemeinde:

Gemarkung:

Flur:

Flurstück:

Oberuckersee

Melzow

4

2, 82, 252.

Zur Orientierung ist dieser Verordnung eine Kartenskizze über die Lage des Schutzwaldes und des Naturwaldes als Anlage beigefügt.

(2) Die Grenzen des Schutzwaldes und des Naturwaldes sind in der „Topografischen Karte zur Verordnung über den Schutzwald ‚Melzower Buchennaturwald‘“, Maßstab 1 : 10 000 und in der „Liegenschaftskarte zur Verordnung über den Schutzwald ‚Melzower Buchennaturwald‘“, Maßstab 1 : 2 500 mit ununterbrochener Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Maßgeblich ist die Einzeichnung in der Liegenschaftskarte.
Die Karten sind mit dem Dienstsiegel des Ministeriums für Infrastruktur und Landwirtschaft des Landes Brandenburg, Siegelnummer 26, versehen und vom Siegelverwahrer am 18.
Juni 2010 unterschrieben worden.

(3) Die Verordnung mit Karten kann beim Ministerium für Infrastruktur und Landwirtschaft in Potsdam, oberste Forstbehörde, sowie beim Landesbetrieb Forst Brandenburg in Potsdam, untere Forstbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3  
Schutzzweck

(1) Schutzzweck des Schutzwaldes ist

1.  die Erhaltung und Entwicklung des natürlich entstandenen Buchenwaldes zum Zwecke der wissenschaftlichen Beobachtung und Erforschung der naturnahen Entwicklung des Waldes;
    
2.  die Erhaltung und Wiederherstellung der Leistungsfähigkeit des Naturhaushaltes;
    
3.  die Erhaltung und Entwicklung des Gebietes als Lebensraum gefährdeter Pflanzenarten.
    

(2) Der besondere Schutzzweck ist der Erhalt des im Schutzwald gelegenen Naturwaldes insbesondere zur

1.  Repräsentation eines besonders wertvollen Restbestandes eines eutrophen Buchenwaldes unter Beteiligung über 300-jähriger Rotbuchen;
    
2.  langfristigen wissenschaftlichen Erforschung der durch den Menschen nicht direkt beeinflussten Waldentwicklung eines eutrophen Buchenmischwaldes, welcher 1938 aus der forstlichen Nutzung entlassen wurde;
    
3.  Erforschung der Waldstruktur, des Bodens, der Flora und der Fauna sowie der natürlichen Selbstorganisationsprozesse des eutrophen Buchenmischwaldes;
    
4.  Nutzung als lokale und regionale Weiserfläche zur Ableitung und exemplarischen Veranschaulichung von Erkenntnissen für die Waldbaupraxis und forstliche Lehre;
    
5.  Erhaltung und Regeneration forstgenetischer Ressourcen;
    
6.  Erhaltung der floristischen und faunistischen Artenvielfalt in sich natürlich entwickelnden Lebensgemeinschaften.
    

(3) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teils des Gebietes von gemeinschaftlicher Bedeutung „Melzower Forst“ mit der Gebietsnummer DE 2849-302 im Sinne des § 2a Absatz 1 Nummer 8 des Brandenburgischen Naturschutzgesetzes mit seinem Vorkommen von „Waldmeister-Buchenwald (Asperulo-Fagetum)“ als Biotop von gemeinschaftlichem Interesse („natürlicher Lebensraumtyp“ im Sinne des Anhangs I der Richtlinie 92/43/EWG).

(4) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teils des Europäischen Vogelschutzgebietes „Schorfheide-Chorin“ mit der Gebietsnummer DE 2948-401 im Sinne des § 2a Absatz 1 Nummer 9 des Brandenburgischen Naturschutzgesetzes in seiner Funktion als Lebensraum von Vogelarten nach Anhang I der Richtlinie 79/409/EWG, einschließlich ihrer Brut- und Nahrungshabitate.

#### § 4  
Verbote, Maßgaben zur forstwirtschaftlichen Bodennutzung

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind im Schutzwald gemäß § 12 Absatz 6 des Waldgesetzes des Landes Brandenburg alle Handlungen verboten, die dem in § 3 genannten Schutzzweck zuwiderlaufen und das Gebiet oder einzelne seiner Bestandteile nachhaltig stören, verändern, beschädigen oder zerstören können.

(2) Es ist insbesondere verboten:

1.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
    
2.  das Gebiet außerhalb der Waldwege zu betreten;
    
3.  im Gebiet mit motorisierten Fahrzeugen und Gespannen zu fahren oder diese dort abzustellen;
    
4.  zu lagern, zu zelten, Wohnwagen aufzustellen, zu rauchen und Feuer zu entzünden;
    
5.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
    
6.  die Bodengestalt zu verändern, Böden zu verfestigen und zu versiegeln;
    
7.  Düngemittel einschließlich Wirtschaftsdünger und Sekundärrohstoffdünger zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
    
8.  Pflanzenschutzmittel jeder Art oder Holzschutzmittel anzuwenden;
    
9.  wild lebende Pflanzen oder Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
    
10.  Vieh weiden zu lassen und dazu erforderliche Einrichtungen zu schaffen.
    

(3) Über die Verbote der Absätze 1 und 2 hinaus ist im Naturwald die forstwirtschaftliche Nutzung verboten.

(4) Eine ordnungsgemäße forstwirtschaftliche Bodennutzung gemäß den in § 4 des Waldgesetzes des Landes Brandenburg genannten Anforderungen und Grundsätzen bleibt auf den bisher rechtmäßig dafür genutzten Flächen außerhalb des Naturwaldes zulässig mit der Maßgabe, dass

1.  die Holznutzung einzelstamm- bis gruppenweise erfolgt;
    
2.  eine Mischungsregulierung zugunsten der Baumarten der natürlichen Waldgesellschaft des Waldmeister-Buchenwaldes erfolgt;
    
3.  die Waldverjüngung kleinflächig erfolgt und sich an den standortheimischen Baumarten der potenziell natürlichen Waldgesellschaft orientiert;
    
4.  mindestens fünf Stämme je Hektar mit einem Mindestdurchmesser von 40 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum natürlichen Absterben und Zerfall aus der Nutzung genommen werden;
    
5.  Bäume mit Horsten oder Höhlen nicht gefällt werden;
    
6.  keine flächige tiefgreifende in den Mineralboden eingreifende Bodenbearbeitung erfolgt;
    
7.  kein flächiger Einsatz von Maschinen erfolgt.
    

#### § 5  
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen;
    
2.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Forstbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    

(2) Die in § 4 für das Betreten und Befahren des Schutzwaldes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Forstbehörden und sonstige von den Forstbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer und Nutzer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums.

#### § 6  
Ausnahmen, Befreiungen

(1) Aus Gründen des Waldschutzes und zur Nutzung nach Naturereignissen wie Sturm oder Waldbrand kann die untere Forstbehörde außerhalb des Naturwaldes Ausnahmen von den Verboten dieser Verordnung zulassen, sofern der Schutzzweck nicht beeinträchtigt wird.

(2) Wenn überwiegende Gründe des Gemeinwohls es erfordern, kann die untere Forstbehörde im Einvernehmen mit der unteren Naturschutzbehörde auf Antrag Befreiungen von den Verboten dieser Verordnung gewähren.

#### § 7  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 37 Absatz 1 Nummer 7 des Waldgesetzes des Landes Brandenburg handelt, wer vorsätzlich oder fahrlässig den Verboten oder den Maßgaben des § 4 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 37 Absatz 3 des Waldgesetzes des Landes Brandenburg mit einer Geldbuße bis zu 20 000 (in Worten: zwanzigtausend) Euro geahndet werden.

#### § 8  
Verhältnis zu anderen rechtlichen Bedingungen

(1) Die Regelungen naturschutzrechtlicher Schutzgebietsausweisungen im Bereich des in § 2 genannten Gebietes bleiben unberührt.

(2) Die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 31 bis 35 des Brandenburgischen Naturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 39 bis 55 des Bundesnaturschutzgesetzes, §§ 37 bis 43 des Brandenburgischen Naturschutzgesetzes) bleiben unberührt.

#### § 9  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 1 der Waldschutzgebietsverfahrensverordnung genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber der obersten Forstbehörde geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 10  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 17.
August 2010

Der Minister für Infrastruktur und Landwirtschaft

Jörg Vogelsänger

![Die Anlage 1 zu § 2 Absatz 1 bildet eine Kartenskizze zur Verordnung über den Schutzwald „Melzower Buchennaturwald“ im Maßstab 1:10000 ab. Es wird die Grenze des Schutzwaldes mit ununterbrochener Linie dargestellt. Der Schutzwald befindet sich nördlich der Ortslage Pfingstberg. Der Schutzwald hat eine Gesamtfläche von rund 24 Hektar und besteht aus zwei Teilflächen, die durch die Bahnlinie Berlin-Stralsund getrennt wird.   Der westlich der Bahnlinie gelegene Teil mit einer Größe von rund 21 Hektar wird durch seine besonderen Schutzfunktion als Naturwald bezeichnet und ist schraffiert dargestellt. Die Grenze verläuft 525 Meter parallel zur Bahnstrecke nach Norden. Danach verläuft die Grenze auf einer Länge von 250 Metern in nordwestlicher Richtung zur Waldkante. Vor der Waldkante verläuft sie nahezu parallel zu dieser auf einer Länge von 750 Metern in Richtung Süden zum Aalgastsee. Dann nimmt die Grenze ihren Verlauf auf einer Länge von 325 Metern in Richtung Osten zur Bahnlinie. ](/br2/sixcms/media.php/69/Nr.%2055-1.JPG "Die Anlage 1 zu § 2 Absatz 1 bildet eine Kartenskizze zur Verordnung über den Schutzwald „Melzower Buchennaturwald“ im Maßstab 1:10000 ab. Es wird die Grenze des Schutzwaldes mit ununterbrochener Linie dargestellt. Der Schutzwald befindet sich nördlich der Ortslage Pfingstberg. Der Schutzwald hat eine Gesamtfläche von rund 24 Hektar und besteht aus zwei Teilflächen, die durch die Bahnlinie Berlin-Stralsund getrennt wird.   Der westlich der Bahnlinie gelegene Teil mit einer Größe von rund 21 Hektar wird durch seine besonderen Schutzfunktion als Naturwald bezeichnet und ist schraffiert dargestellt. Die Grenze verläuft 525 Meter parallel zur Bahnstrecke nach Norden. Danach verläuft die Grenze auf einer Länge von 250 Metern in nordwestlicher Richtung zur Waldkante. Vor der Waldkante verläuft sie nahezu parallel zu dieser auf einer Länge von 750 Metern in Richtung Süden zum Aalgastsee. Dann nimmt die Grenze ihren Verlauf auf einer Länge von 325 Metern in Richtung Osten zur Bahnlinie. ")