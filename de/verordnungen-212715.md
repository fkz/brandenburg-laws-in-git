## Verordnung über die Erstattung der Bearbeitungskosten für den Vollzug des Brandenburgischen Vergabegesetzes (Brandenburgische Vergabegesetz-Erstattungsverordnung - BbgVergGErstV)

Auf Grund des § 14 Absatz 3 des Brandenburgischen Vergabegesetzes vom 21.
September 2011 (GVBl.
I Nr.
19) verordnet die Landesregierung:

§ 1  
Erstattung der Bearbeitungskosten in Fallpauschalen
---------------------------------------------------------

(1) Der den Kommunen nach § 14 des Brandenburgischen Vergabegesetzes in Fallpauschalen in Euro zu erstattende Bearbeitungsaufwand wird durch Multiplikation der Summe der Zeitwerte der Tätigkeiten nach Absatz 2 mit dem Vergütungsfaktor nach Absatz 3 gebildet.

(2) Der Zeitwert für die insbesondere zu berücksichtigenden Tätigkeiten umfasst:

1.  für die Ermittlung der Anwendbarkeit des Brandenburgischen Vergabegesetzes 15 Minuten je Vergabeverfahren; in Fällen, in denen eine Mindestlohnregelung Ausnahmen vorsieht, 120 Minuten;
2.  für das Beifügen der nach dem Brandenburgischen Vergabegesetz erforderlichen Vertragserklärungen zu den Vergabeunterlagen einschließlich der Rücklaufkontrolle 40 Minuten je Vergabeverfahren; in Vergabeverfahren mit mehr als sechs Bewerbern oder mehr als zwei Nachforderungen von Unterlagen 60 Minuten;
3.  für die Nachfrage nach Eintragungen in die Liste der Auftragssperren bei der Informationsstelle nach § 12 des Brandenburgischen Vergabegesetzes 40 Minuten je Vergabeverfahren; in Vergabeverfahren mit mehr als sechs Bewerbern oder mehr als zwei Nachforderungen von Unterlagen 60 Minuten;
4.  für die vertiefte Prüfung der Angemessenheit von Angebotspreisen nach § 7 des Brandenburgischen Vergabegesetzes mit Anforderung von Stellungnahmen der Bieter und deren Auswertung 10 Minuten zuzüglich 90 Minuten je vertiefter Prüfung; in Fällen, in denen ausschließlich für die vertiefte Prüfung ein Aufklärungsgespräch erforderlich ist, 290 Minuten;
5.  für die regelmäßige Kontrolle nach § 8 Absatz 1 des Brandenburgischen Vergabegesetzes in Verbindung mit § 3 der Brandenburgischen Vergabegesetz-Durchführungsverordnung vom 16.
    Oktober 2012 (GVBl.
    II Nr.
    85) 30 Minuten je Auftragnehmer bei Bauaufträgen; in allen anderen Fällen und bei Bauaufträgen, die nicht anhand einer Bescheinigung einer Sozialkasse des Baugewerbes geprüft werden können, 90 Minuten;
6.  für die Kontrollen nach § 8 Absatz 1 des Brandenburgischen Vergabegesetzes in Verbindung mit den §§ 4 und 5 der Brandenburgischen Vergabegesetz-Durchführungsverordnung und deren Dokumentation bei Kontrollen am Einsatzort 180 Minuten; bei Kontrollen am Betriebsort 360 Minuten;
7.  für Entscheidungen über die Verhängung von Sanktionen nach § 9 des Brandenburgischen Vergabegesetzes einschließlich der Vorbereitung dieser Entscheidungen 170 Minuten je Vorgang;
8.  für die Meldung der Entscheidung über eine Auftragssperre zur Sperrliste nach § 11 Absatz 2 und 4 des Brandenburgischen Vergabegesetzes sowie für Entscheidungen nach § 11 Absatz 5 des Brandenburgischen Vergabegesetzes 170 Minuten je Vorgang;
9.  für den erhöhten Aufwand in Fällen des § 3 Absatz 2 des Brandenburgischen Vergabegesetzes 60 Minuten in den Fällen der Nummer 2; in den Fällen der Nummern 5 und 6 je 240 Minuten;
10.  für die Bearbeitung des Antrags auf Kostenerstattung nach § 14 des Brandenburgischen Vergabegesetzes in Verbindung mit dieser Verordnung 300 Minuten bei Gemeinden und Ämtern mit einer Einwohnerzahl bis  
    zu 20 000 sowie Zweckverbänden und anderen Gemeindeverbänden (außer Landkreisen), 360 Minuten bei einer Einwohnerzahl über 20 000 und 480 Minuten bei kreisfreien Städten und Landkreisen.

(3) Der Vergütungsfaktor beträgt für das Jahr 2012 0,6973 Euro je Minute.
Ab dem Jahr 2013 beträgt der Vergütungsfaktor 0,7154 Euro je Minute.

(4) Die zu erstattenden Kosten werden gemindert durch vereinnahmte Schadenersatzleistungen der Auftragnehmer.
Der hierbei zu berücksichtigende Schaden muss aus Kosten von Tätigkeiten nach Absatz 1 in der Höhe bestehen, in der sie erstattet werden.
Die Minderung erfolgt mit dem nächsten Antrag oder auf Hinweis des Zahlungsempfängers bei der nächsten Zahlung einer Pauschale oder Vorauszahlung.
Insoweit erfolgen Erstattungszahlungen unter dem Vorbehalt der Rückforderung.

§ 2  
Erstattung der Schulungskosten
------------------------------------

Der Kommune werden auf der Grundlage von Abrechnungen folgende Kosten der zur Anwendung des Brandenburgischen Vergabegesetzes notwendigen Schulungen des Personals erstattet:

1.  die Kosten, die für die Teilnahme an einer Veranstaltung außerhalb der Dienststelle oder des Arbeitsorts oder für die Durchführung einer Veranstaltung einer Schulung des Personals innerhalb der Dienststelle entstehen, ohne Raumkosten;
2.  die Kosten, für die auf die Schulung entfallende Arbeitszeit der teilnehmenden Beschäftigten der Kommune;
3.  die mit der Schulung zusammenhängenden notwendigen, verauslagten Reisekosten nach Maßgabe der für die Kommune geltenden reisekostenrechtlichen Bestimmungen.

§ 3  
Erstattung weiterer Mehrkosten
------------------------------------

(1) Weitere Mehrkosten werden auf begründeten Antrag der Kommune erstattet, wenn und soweit die nach § 1 ermittelte Fallpauschale eines Beschaffungsvorgangs die zur Bearbeitung notwendig anfallenden Kosten nicht abdeckt.
Die Mehrkosten müssen auch bei einer kostenbewussten Wahrnehmung der Aufgabe unvermeidbar sein.
Der Antrag muss die Kosten darlegen und eine Erklärung über das Fehlen von kostengünstigeren Alternativen enthalten.
Werden Drittkosten geltend gemacht, sind entsprechende Nachweise beizufügen.

(2) Zu den weiteren Mehrkosten zählen auch die Kosten einer Dienstreise, soweit die Dienstreise zur Durchführung einer Kontrolle nach § 8 Absatz 1 des Brandenburgischen Vergabegesetzes in Verbindung mit § 8 Absatz 3, 4 oder Absatz 5 der Brandenburgischen Vergabegesetz-Durchführungsverordnung erforderlich ist.
Die Erstattung dieser Mehrkosten erfolgt in entsprechender Anwendung von § 2 Nummer 3.

§ 4  
Verfahren der Kostenerstattung
------------------------------------

(1) Anträge auf Erstattung der Kosten in Fallpauschalen nach § 1 sind in der Regel einmal kalenderjährlich für zwölf Monate beim für Wirtschaft zuständigen Ministerium als Erstattungsstelle einzureichen.
Erstanträge sind mindestens für neun Monate und höchstens für 21 Monate zu stellen.
Die Anträge sollen zusammen mit Anträgen nach § 3 für denselben Zeitraum eingereicht werden.

(2) Anträge auf Kostenerstattung nach § 2 können bei der Erstattungsstelle zusammen mit den Anträgen nach Absatz 1 oder jederzeit nach der jeweiligen Schulungsmaßnahme einzeln eingereicht werden.

(3) Die Voraussetzungen der Erstattung sind glaubhaft zu machen.
 Hierzu sind dem Antrag eine Aufstellung der der Kommune entstandenen Kosten nach den Zeitwerten des § 1 Absatz 2 und eine Aufstellung der Anzahl der Beschaffungsvorgänge beizufügen.
Die Anzahl der Beschaffungsvorgänge ist nach den Wertgrenzen des § 1 Absatz 1 des Brandenburgischen Vergabegesetzes zu unterteilen.
Bei Rückfragen der Erstattungsstelle sind Erläuterungen mündlich oder auf Verlangen schriftlich abzugeben.

(4) Mit der Antragstellung bestätigt die Kommune, dass die Tätigkeiten, für deren Kosten eine Erstattung beantragt wird, in dem genannten Umfang nur durch die Umsetzung des Brandenburgischen Vergabegesetzes erbracht und nur die sich aus dem Antrag ergebenden Schadenersatzleistungen vereinnahmt worden sind.

(5) Die Erstattungsstelle kann Stichproben vornehmen.
Dazu kann sie Unterlagen anfordern oder sich beim Antragsteller vorlegen lassen.

§ 5  
Periodische Pauschalen; Vorauszahlungen
---------------------------------------------

(1) Die Erstattungsstelle kann auf Antrag der Kommune eine Jahrespauschale festsetzen, wenn die Zahl der Auftragsvergaben in den letzten zwei abgelaufenen Kalenderjahren

1.  um höchstens 10 Prozent schwankt oder
2.  die Zahl der Auftragsvergaben in diesem Zeitraum nur zunimmt.

Die Jahrespauschale wird auf der Grundlage der Summen der Zeitwerte bemessen, aus denen die Fallpauschalen der drei nach Satz 1 berücksichtigten Kalenderjahre berechnet worden sind.
Sie wird auf Antrag vierteljährlich in gleichen Beträgen ausbezahlt.
Bereits abgelaufene Zahlungstermine können in einer ersten Auszahlung nachgeholt werden.

(2) Die Jahrespauschale ersetzt die Feststellung und Festsetzung der Fallpauschalen nach § 1 für das Jahr, für das sie bezahlt wird.
Die §§ 2 und 3 Absatz 2 bleiben unberührt.

(3) In der Festsetzungsentscheidung wird die Kommune verpflichtet, Aufzeichnungen über die Zahl von Auftragsvergaben in verschiedenen nach Auftragswerten vorzunehmenden Gruppierungen jährlich mit dem Antrag auf Auszahlung der Pauschalen für das neue Jahr vorzulegen.
Die Festsetzung der Jahrespauschale kann für die Zeit ab dem folgenden Kalenderjahr aufgehoben werden, wenn die Aufzeichnungen und die für die beiden Vorjahre vorliegenden Zahlen nach Absatz 1 nicht zur Festsetzung einer Jahrespauschale führen würden.
 Sie kann teilweise aufgehoben werden, wenn zum Zeitpunkt der Aufhebung nur eine niedrigere Jahrespauschale festgesetzt werden könnte.

(4) Die Jahrespauschale kann erhöht werden, wenn der vorherigen Festsetzung nach Absatz 3 gruppierte Zahlen für mindestens eines der drei Jahre nach Absatz 1 Satz 1 zugrunde lagen.
Es muss ein Vergleich mit Durchschnittskosten möglich sein, die aus den Fallpauschalen früherer Jahre des Antragstellers ermittelt und nach den Gruppierungen der Auftragswerte der Aufstellung der Anzahl der Beschaffungsvorgänge nach § 4 Absatz 3 verteilt werden.

(5) Eine Kommune kann beantragen, für das laufende Jahr Vorauszahlungen bis zur Höhe von 80 Prozent der Summe der Fallpauschalen des Vorjahres zu erhalten.
Die Vorauszahlungen werden vierteljährlich zur Mitte des letzten Monats des jeweiligen Vierteljahres in gleichen Beträgen ausgezahlt.
Über die Vorauszahlung ist unverzüglich im ersten Vierteljahr des auf die Zahlungen folgenden Kalenderjahres anhand der Fallpauschalen für das Vorauszahlungsjahr abzurechnen.
 Überzahlungen werden mit der nächsten fälligen Zahlung verrechnet.

§ 6  
Übergangsvorschrift
-------------------------

Nach den Bestimmungen dieser Verordnung werden auch die Kosten erstattet,

1.  die in der Zeit zwischen dem 21.
    September 2011 und dem 31.
    Dezember 2011 zur Vorbereitung auf das Inkrafttreten des Brandenburgischen Vergabegesetzes entstanden sind;
2.  die in der Zeit vom 1.
    Januar 2012 bis zum Inkrafttreten dieser Verordnung entstanden sind; bei der Anwendung von § 4 Absatz 3 für Tätigkeiten im Jahr 2012 können Einzeltätigkeiten auf der Basis von § 1 Absatz 2 Nummer 1 bis 9 mit einem gemittelten Zeitwert abgerechnet werden, wenn ein unterschiedlicher Bearbeitungsaufwand durch verschiedene Zeitwerte abgedeckt ist.

§ 7  
Inkrafttreten
-------------------

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 14.
Januar 2013

Die Landesregierung des Landes Brandenburg

Der Ministerpräsident

Matthias Platzeck

Der Minister für Wirtschaft und Europaangelegenheiten

Ralf Christoffers