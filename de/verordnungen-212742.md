## Verordnung über Zuständigkeiten im Umweltrecht und zur Übertragung der Ermächtigung zum Erlass von Rechtsverordnungen zur Ausweisung von Wasserschutzgebieten  (Umweltrechtszuständigkeitsverordnung - UmweltrZV)

#### § 1  
Zuständigkeiten nach dem Umweltschadensgesetz[**\[1\]**](#_ftn1)

(1) Zuständige Behörden für die Aufgaben nach dem Umweltschadensgesetz vom 10.
Mai 2007 (BGBl.
I S. 666), das zuletzt durch Artikel 14 des Gesetzes vom 31.
Juli 2009 (BGBl.
I S. 2585, 2618) geändert worden ist, sind bei Vorliegen eines Umweltschadens oder der Gefahr eines solchen nach

1.  § 2 Nummer 1 Buchstabe a des Umweltschadensgesetzes das Landesamt für Umwelt, Gesundheit und Verbraucherschutz als Fachbehörde für Naturschutz und Landschaftspflege nach § 52 Satz 1 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
    Mai 2004 (GVBl.
    I S. 350), das zuletzt durch Artikel 2 Absatz 9 des Gesetzes vom 15.
    Juli 2010 (GVBl.
    I Nr. 28, S. 3) geändert worden ist,
    
2.  § 2 Nummer 1 Buchstabe b des Umweltschadensgesetzes die Landkreise und kreisfreien Städte als untere  
    Wasserbehörden nach § 124 Absatz 2 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 8.
    Dezember 2004 (GVBl.
    I S. 50), das zuletzt durch Artikel 2 Absatz 12 des Gesetzes vom 15.
    Juli 2010 (GVBl.
    I Nr. 28, S. 3) geändert worden ist,
    
3.  § 2 Nummer 1 Buchstabe c die unteren Bodenschutzbehörden nach § 42 Absatz 2 Satz 2 in Verbindung mit § 43 Absatz 2 des Brandenburgischen Abfall- und Bodenschutzgesetzes vom 6.
    Juni 1997 (GVBl.
    I S. 40), das zuletzt durch Artikel 2 Absatz 11 des Gesetzes vom 15.
    Juli 2010 (GVBl.
    I Nr. 28, S. 3) geändert worden ist.
    

(2) Abweichend von Absatz 1 ist in den Fällen, in denen die beruflichen Tätigkeiten nach § 3 Absatz 1 Nummer 1 oder Nummer 2 des Umweltschadensgesetzes der Bergaufsicht nach § 69 des Bundesberggesetzes vom 13.
August 1980 (BGBl.
I S. 1310), das zuletzt durch Artikel 15a des Gesetzes vom 31.
Juli 2009 (BGBl.
I S. 2585, 2619) geändert worden ist, unterliegen, das Landesamt für Bergbau, Geologie und Rohstoffe zuständige Behörde.

#### § 2  
_(außer Kraft getreten)_

#### § 3  
Zuständigkeiten nach dem Umwelt-Rechtsbehelfsgesetz

Anerkennung einer inländischen Vereinigung nach § 3 Absatz 3 des Umwelt-Rechtsbehelfsgesetzes erfolgt durch das für den Umweltschutz zuständige Ministerium.

#### § 4  
_(außer Kraft getreten)_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\[1\]](#_ftnref1) § 1 dieser Verordnung dient der Umsetzung der Richtlinie 2004/35/EG des Europäischen Parlaments und des Rates vom 21.
April 2004 über die Umwelthaftung zur Vermeidung und Sanierung von Umweltschäden (ABl.
L 149 vom 30.4.2004, S.
56), die zuletzt durch die Richtlinie 2009/31/EG vom 23.
April 2009 (ABl.
L 140 vom 5.6.2009, S.
114) geändert worden ist.