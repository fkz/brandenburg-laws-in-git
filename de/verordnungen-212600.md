## Verordnung zur Festsetzung des Wasserschutzgebietes Briesen

Auf Grund des § 51 Absatz 1 Satz 1 Nummer 1 und Satz 2 und Absatz 2 und des § 52 Absatz 1 des Wasserhaushaltsgesetzes vom 31.
Juli 2009 (BGBl.
I S. 2585) in Verbindung mit § 15 Absatz 1 Satz 1 und Absatz 2 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1  
Allgemeines

(1) Zur Sicherung der öffentlichen Wasserversorgung wird zum Schutz des Grundwassers im Einzugsgebiet des Wasserwerkes Briesen das in § 2 näher umschriebene Wasserschutzgebiet festgesetzt.
Begünstigte ist die Frankfurter Wasser- und Abwassergesellschaft mbH.

(2) Das Wasserschutzgebiet gliedert sich in den Fassungsbereich (Zone I), in die engere Schutzzone (Zone II) und in die weitere Schutzzone (Zone III).

### § 2  
Räumlicher Geltungsbereich

(1) Lage und Größe des Wasserschutzgebietes und der Schutzzonen ergeben sich aus der Übersichtskarte in der Anlage 2 und den in Absatz 2 genannten Karten.

(2) Die Schutzzonen sind in der topografischen Karte im Maßstab 1 : 10 000 in der Anlage 3 und außerdem in der Liegenschaftskarte im Maßstab 1 : 2 500 in der Anlage 4, die aus elf Blättern besteht, dargestellt.
Für die Abgrenzung der Schutzzonen ist die Darstellung in der Liegenschaftskarte maßgebend.

(3) Zu Informationszwecken werden zusätzlich auf Papier ausgefertigte Exemplare der in Absatz 2 genannten Karten bei der unteren Wasserbehörde des Landkreises Oder-Spree, der Amtsverwaltung Odervorland und der Gemeindeverwaltung Rietz-Neuendorf hinterlegt und können dort während der Dienststunden von jedermann kostenlos eingesehen werden.
Diese Karten sind **mit Datum vom 25.
November 2011 und** mit dem Dienstsiegel des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (Siegelnummer 20) versehen.
Eine weitere so gesiegelte Ausfertigung der Karten befindet sich im Ministerium für Umwelt, Gesundheit und Verbraucherschutz.

(4) Veränderungen der Topografie sowie von Flurstücksgrenzen oder -bezeichnungen berühren den räumlichen Geltungsbereich der Schutzzonen nicht.

### § 3  
Schutz der Zone III

In der Zone III sind verboten:

1.  das Düngen mit Gülle, Jauche, Geflügelkot, Festmist, Silagesickersaft oder sonstigen Düngemitteln mit im Sinne des § 2 Nummer 10 der Düngeverordnung wesentlichen Nährstoffgehalten an Stickstoff oder Phosphat,
    1.  wenn die Düngung nicht im Sinne des § 3 Absatz 4 der Düngeverordnung in zeit- und bedarfsgerechten Gaben erfolgt,
    2.  wenn keine jährlichen schlagbezogenen Aufzeichnungen über die Zu- und Abfuhr von Stickstoff und Phosphat erstellt werden,
    3.  auf abgeerntetem Ackerland, wenn nicht im gleichen Jahr Folgekulturen einschließlich Zwischenfrüchte angebaut werden,
    4.  auf Dauergrünland und auf Ackerland vom 15.
        Oktober bis 15.
        Februar, ausgenommen das Düngen mit Festmist ohne Geflügelkot,
    5.  auf Brachland oder stillgelegten Flächen oder
    6.  auf wassergesättigten, gefrorenen oder schneebedeckten Böden,
2.  das Lagern oder Ausbringen von Fäkalschlamm oder Klärschlämmen aller Art einschließlich in Biogasanlagen behandelte Klärschlämme,
3.  das Errichten von befestigten Dunglagerstätten, ausgenommen mit dichtem Jauchebehälter, der über eine Leckageerkennungseinrichtung verfügt,
4.  das Errichten von Erdbecken zur Lagerung von Gülle, Jauche oder Silagesickersäften,
5.  das Errichten von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle, ausgenommen Hochbehälter mit Leckageerkennungseinrichtung und Sammeleinrichtungen, wenn der Wasserbehörde vor Inbetriebnahme sowie wiederkehrend alle fünf Jahre ein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit der Sammeleinrichtungen vorgelegt wird,
6.  unbefestigte Feldrandzwischenlager für organische oder mineralische Dünger, ausgenommen für Kalk und Kaliumdünger,
7.  das Errichten von ortsfesten Anlagen für die Silierung von Pflanzen oder die Lagerung von Silage, ausgenommen
    1.  Anlagen mit dichtem Silagesickersaft-Sammelbehälter, der über einer Leckageerkennungseinrichtung verfügt und
    2.  Anlagen mit Ableitung in Jauche- oder Güllebehälter,wenn der Wasserbehörde vor Inbetriebnahme sowie wiederkehrend alle fünf Jahre ein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit der Behälter und Leitungen vorgelegt wird,
8.  die Silierung von Pflanzen oder Lagerung von Silage außerhalb ortsfester Anlagen, ausgenommen Ballensilage im Wickelverfahren,
9.  das Errichten von Stallungen für Tierbestände,
10.  die Freilandtierhaltung im Sinne der Anlage 1 Nummer 1, wenn die Ernährung der Tiere nicht im Wesentlichen aus der genutzten Weidefläche erfolgt oder wenn die Grasnarbe flächig verletzt wird, ausgenommen Kleintierhaltung für die Eigenversorgung,
11.  die Anwendung von Pflanzenschutzmitteln,
    1.  wenn die Pflanzenschutzmittel nicht für Wasserschutzgebiete zugelassen sind,
    2.  wenn keine flächenbezogenen Aufzeichnungen gemäß § 6 Absatz 4 des Pflanzenschutzgesetzes über den Einsatz auf erwerbsgärtnerisch, land- oder forstwirtschaftlich genutzten Flächen geführt werden,
    3.  in einem Abstand von weniger als 10 Metern zu oberirdischen Gewässern,
    4.  zur Bodenentseuchung oder
    5.  auf Dauergrünland und Grünlandbrachen,
12.  die Beregnung landwirtschaftlich oder erwerbsgärtnerisch genutzter Flächen, wenn die Beregnungshöhe 20 Millimeter pro Tag oder 60 Millimeter pro Woche überschreitet,
13.  das Errichten von Gartenbaubetrieben oder Kleingartenanlagen, ausgenommen Gartenbaubetriebe, die in geschlossenen Systemen produzieren,
14.  die Neuanlage von Baumschulen, forstlichen Pflanzgärten, Weihnachtsbaumkulturen sowie von gewerblichem Wein-, Hopfen-, Gemüse-, Obst- oder Zierpflanzenanbau, ausgenommen Gemüse- und Zierpflanzenanbau unter Glas in geschlossenen Systemen und Containerproduktion von Baumschulprodukten auf versiegelten Flächen,
15.  der Umbruch von Dauergrünland oder von Grünlandbrachen,
16.  das Anlegen von Schwarzbrache im Sinne der Anlage 1 Nummer 2,
17.  Erstaufforstungen mit Nadelbaumarten oder Robinien,
18.  die Umwandlung von Wald in eine andere Nutzungsart,
19.  Holzerntemaßnahmen, die eine gleichmäßig verteilte Überschirmung von weniger als 40 Prozent des Waldbodens oder Freiflächen größer als 1 000 Quadratmeter erzeugen, ausgenommen Femel- oder Saumschläge,
20.  das Einrichten oder Erweitern von dauerhaften Holzlagerplätzen über 100 Raummeter,
21.  Erdaufschlüsse im Sinne des § 49 Absatz 1 des Wasserhaushaltsgesetzes, selbst wenn Grundwasser nicht aufgedeckt wird, ausgenommen das Verlegen von Ver- und Entsorgungsleitungen und die Herstellung von Baugruben und Bohrungen,
22.  das Errichten, Erweitern oder Erneuern von Tiefenbohrungen über 100 Meter, Grundwassermessstellen oder Brunnen, ausgenommen das Erneuern von Brunnen für Entnahmen mit wasserrechtlicher Erlaubnis oder Bewilligung,
23.  das Errichten oder Erweitern von vertikalen Anlagen zur Gewinnung von Erdwärme,
24.  das Errichten oder Erweitern von Anlagen zum Umgang mit wassergefährdenden Stoffen, ausgenommendoppelwandige Anlagen mit Leckanzeigegerät und ausgenommen Anlagen, die mit einem Auffangraum ausgerüstet sind, der das maximal in der Anlage vorhandene Volumen wassergefährdender Stoffe aufnehmen kann, und soweit
    1.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 1 das für die Anlage maßgebende Volumen von 1 000 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 1 die für die Anlage maßgebende Masse von 1 000 Tonnen,
    2.  in oberirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 2 das für die Anlage maßgebende Volumen von 100 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der assergefährdungsklasse 2 die für die Anlage maßgebende Masse von 100 Tonnen,
    3.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 2 das für die Anlage maßgebende Volumen von 10 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 2 die für die Anlage maßgebende Masse von 10 Tonnen,
    4.  in oberirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 3 das für die Anlage maßgebende Volumen von 10 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 3 die für die Anlage maßgebende Masse von 10 Tonnen,
    5.  in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 3 das für die Anlage maßgebende Volumen von 1 Kubikmeter beziehungsweise bei festen oder gasförmigen Stoffen der Wasser-gefährdungsklasse 3 die für die Anlage maßgebende Masse von 1 Tonnenicht überschritten wird,
25.  das Errichten von Rohrleitungsanlagen für wassergefährdende Stoffe,
26.  das Errichten von Anlagen zur behälterlosen Lagerung oder Ablagerung von Stoffen im Untergrund,
27.  das Behandeln, Lagern oder Ablagern von Abfall, tierischen Nebenprodukten oder bergbaulichen Rückständen, ausgenommen
    1.  die ordnungsgemäße kurzzeitige Zwischenlagerung von vor Ort angefallenem Abfall zur Abholung durch den Entsorgungspflichtigen und
    2.  die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen,
28.  das Ein- oder Aufbringen von Abfällen, bergbaulichen Rückständen oder Ersatzbaustoffen in oder auf Böden oder deren Einbau in bodennahe technische Bauwerke,
29.  das Errichten von Anlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden radioaktiver Stoffe im Sinne des Atomgesetzes, ausgenommen für medizinische Anwendungen sowie für Mess-, Prüf- und Regeltechnik,
30.  das Errichten von Industrieanlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden wassergefährdender Stoffe in großem Umfang, wie zum Beispiel in Raffinerien, Metallhütten oder chemischen Fabriken,
31.  das Errichten von Kraftwerken oder Heizwerken, die der Genehmigungspflicht nach Bundesimmissionsschutzrecht unterliegen, ausgenommen mit Gas, Sonnenenergie oder Windkraft betriebene Anlagen,
32.  das Errichten von Biogasanlagen,
33.  das Errichten von Abwasserbehandlungsanlagen,
34.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik eingehalten werden,
35.  das Errichten oder Erweitern von Regen- oder Mischwasserentlastungsbauwerken,
36.  das Errichten oder Erweitern von Abwassersammelgruben, ausgenommen
    1.  Anlagen mit allgemeiner bauaufsichtlicher Zulassung des Deutschen Institutes für Bautechnik und
    2.  monolithische Sammelgruben aus Beton, die den allgemein anerkannten Regeln der Technik entsprechen,
37.  das Betreiben oder Unterhalten von Abwassersammelgruben, wenn der Wasserbehörde nicht
    1.  vor Inbetriebnahme,
    2.  bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung sowie
    3.  wiederkehrend alle fünf Jahreein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit vorgelegt wird,
38.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten, ausgenommen Anlagen mit dichtem Behälter,
39.  das Einleiten von Abwasser – mit Ausnahme von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 Nummer 3 – in oberirdische Gewässer, sofern die Einleitung nicht schon zum Zeitpunkt des Inkrafttretens dieser Verordnung wasserrechtlich zugelassen war,
40.  das Ausbringen von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes,
41.  das Einleiten oder Versickern von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser,
42.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen das großflächige Versickern von Niederschlagswasser über die belebte Bodenzone,
43.  das Anwenden von Auftaumitteln auf Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen auf der Bundesautobahn 12 sowie ausgenommen bei Extremwetterlagen wie Eisregen,
44.  das Errichten oder Erweitern von Straßen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik für bautechnische Maßnahmen an Straßen in Wasserschutzgebieten eingehalten werden,
45.  das Errichten von Bahnhöfen oder Schienenwegen der Eisenbahn,
46.  das Verwenden wassergefährdender, auslaug- oder auswaschbarer Materialien (zum Beispiel Schlacke, Bauschutt, Teer, Imprägniermittel) zum Wege- oder Wasserbau,
47.  das Einrichten oder Betreiben von öffentlichen Freibädern oder Zeltplätzen sowie Camping aller Art, ausgenommen
    1.  Einrichtungen mit ordnungsgemäßer Abfall- und Abwasserentsorgung und
    2.  das Zelten von Fuß-, Rad-, Reit- und Wasserwanderern abseits von Zelt- und Campingplätzen für eine Nacht,
48.  das Einrichten oder Betreiben von Sportanlagen, ausgenommen Anlagen mit ordnungsgemäßer Abfall- und Abwasserentsorgung,
49.  das Errichten von Motorsportanlagen,
50.  das Errichten von Schießständen oder Schießplätzen für Feuerwaffen, ausgenommen Schießstände in geschlossenen Räumen,
51.  das Errichten von Golfanlagen,
52.  das Abhalten oder Durchführen von Märkten, Volksfesten oder Großveranstaltungen außerhalb der dafür vorgesehenen Anlagen,
53.  das Durchführen von Motorsportveranstaltungen, ausgenommen das Durchfahren auf klassifizierten Straßen,
54.  Bestattungen,
55.  das Errichten von Flugplätzen im Sinne des § 6 Absatz 1 Satz 1 des Luftverkehrsgesetzes,
56.  das Starten oder Landen motorgetriebener Luftfahrzeuge, mit Ausnahme in Fällen des § 25 Absatz 2 des Luftverkehrsgesetzes,
57.  das Errichten von militärischen Anlagen, Standort- oder Truppenübungsplätzen,
58.  das Durchführen von militärischen Übungen, ausgenommen das Durchfahren auf klassifizierten Straßen,
59.  Bergbau einschließlich die Aufsuchung oder Gewinnung von Erdöl oder Erdgas,
60.  das Durchführen von Sprengungen, sofern die Gefahr besteht, dass dabei das Grundwasser aufgedeckt wird,
61.  die Darstellung von neuen Bauflächen oder Baugebieten im Rahmen der vorbereitenden Bauleitplanung, wenn darin eine Neubebauung bisher unbebauter Gebiete vorgesehen wird,
62.  die Festsetzung von neuen Baugebieten im Rahmen der verbindlichen Bauleitplanung, wenn dies zu einer Erhöhung der zulässigen Grundfläche im Sinne des § 19 Absatz 2 der Baunutzungsverordnung führt, ausgenommen
    1.  Gebiete, die im zum Zeitpunkt des Inkrafttretens dieser Verordnung gültigen Flächennutzungsplan als Bauflächen oder Baugebiete dargestellt sind, und
    2.  die Überplanung von Bestandsgebieten, wenn dies zu keiner wesentlichen Erhöhung der zulässigen Grundfläche im Sinne des § 19 Absatz 2 der Baunutzungsverordnung führt.

### § 4  
Schutz der Zone II

Die Verbote der Zone III gelten auch in der Zone II.
In der Zone II sind außerdem verboten:

1.  das Düngen mit Gülle, Jauche oder Festmist oder sonstigen organischen Düngern sowie die Anwendung von Silagesickersaft,
2.  das Errichten von Dunglagerstätten,
3.  das Errichten von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle,
4.  die Silierung von Pflanzen oder Lagerung von Silage,
5.  die Freilandtierhaltung im Sinne der Anlage 1 Nummer 1,
6.  die Beweidung,
7.  die Anwendung von Pflanzenschutzmitteln,
8.  das Errichten, Erweitern oder Erneuern von Dränungen oder Entwässerungsgräben,
9.  der Einsatz von forstwirtschaftlichen Kraftfahrzeugen abseits von Straßen, Wegen oder forstwirtschaftlichen Rückegassen,
10.  das Vergraben, Lagern oder Ablagern von Tierkörpern oder Teilen davon,
11.  das Errichten oder Betreiben von Wildfütterungen, Kirrungen oder Luderplätzen,
12.  das Errichten, Erweitern oder Betreiben von Anlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden wassergefährdender Stoffe,
13.  der Einsatz von mineralischen Schalölen oder mineralischen Schmierstoffen zur Verlustschmierung,
14.  das Lagern, Abfüllen oder Umschlagen wassergefährdender Stoffe, ausgenommen haushaltsübliche Kleinstmengen,
15.  das Befahren mit Fahrzeugen mit wassergefährdender Ladung,
16.  das Errichten oder Erweitern von Transformatoren oder Stromleitungen mit flüssigen wassergefährdenden Kühl- oder Isoliermitteln,
17.  der Umgang mit radioaktiven Stoffen im Sinne des Atomgesetzes,
18.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, ausgenommen Anlagen, die zur Entsorgung vorhandener Bebauung dienen und wenn hierbei die allgemein anerkannten Regeln der Technik eingehalten werden,
19.  das Errichten oder Erweitern von Abwassersammelgruben,
20.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten,
21.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen das großflächige Versickern von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 Nummer 3 über die belebte Bodenzone,
22.  das Errichten oder Erweitern von Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen Baumaßnahmen an vorhandenen Straßen zur Anpassung an den Stand der Technik und Verbesserung der Verkehrssicherheit unter Einhaltung der allgemein anerkannten Regeln der Technik für bautechnische Maßnahmen an Straßen in Wasserschutzgebieten,
23.  das Errichten von öffentlichen Freibädern oder Zeltplätzen sowie Camping aller Art,
24.  das Errichten von Sportanlagen,
25.  das Abhalten oder Durchführen von Sportveranstaltungen, Märkten, Volksfesten oder Großveranstaltungen,
26.  das Errichten oder Erweitern von Baustelleneinrichtungen oder Baustofflagern,
27.  das Durchführen von Bohrungen, ausgenommen Maßnahmen zur Abwehr von Gefahren für das Grundwasser unter Beachtung der Sicherheitsvorkehrungen zum Grundwasserschutz,
28.  das Durchführen von unterirdischen Sprengungen,
29.  das Errichten oder Erweitern von baulichen Anlagen, ausgenommen Veränderungen in Gebäuden und Instandhaltungsmaßnahmen.

### § 5  
Schutz der Zone I

Die Verbote der Zonen III B, III A und II gelten auch in der Zone I.
In der Zone I sind außerdem verboten:

1.  das Betreten oder Befahren,
2.  land-, forst- oder gartenbauliche Nutzung,
3.  Veränderungen oder Aufschlüsse der Erdoberfläche.

### § 6  
Maßnahmen zur Wassergewinnung

Die Verbote des § 3 Nummer 22 und 41, des § 4 Nummer 14, 17, 26 bis 29 sowie des § 5 Nummer 1 und 3 gelten nicht für Maßnahmen zur Wassergewinnung, die durch diese Verordnung geschützt ist.

### § 7  
Widerruf von Befreiungen

(1) Befreiungen nach § 52 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes sind widerruflich und bedürfen der Schriftform.
Abweichend von Satz 1 ist eine Befreiung von den Verboten gemäß § 3 Nummer 61 und 62 nicht widerruflich.

(2) Im Fall des Widerrufs einer Befreiung kann die untere Wasserbehörde vom Grundstückseigentümer verlangen, dass der frühere Zustand wiederhergestellt wird, sofern es das Wohl der Allgemeinheit, insbesondere der Schutz der Wasserversorgung, erfordert.

### § 8  
Sicherung und Kennzeichnung des Wasserschutzgebietes

(1) Die Zone I ist von der Begünstigten auf Anordnung der unteren Wasserbehörde gegen unbefugtes Betreten, zum Beispiel durch eine Umzäunung, zu sichern.

(2) Die Begünstigte hat auf Anordnung der Wasserbehörde zur Kennzeichnung des Wasserschutzgebietes im Bereich öffentlicher Verkehrsflächen bei der Straßenverkehrsbehörde die Anordnung des Verkehrszeichens 354 zu beantragen und im Bereich nichtöffentlicher Flächen in Abstimmung mit der Gemeinde nichtamtliche Hinweiszeichen aufzustellen.

### § 9  
Duldungspflichten

(1) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet haben die Überwachung des Wasserschutzgebietes, insbesondere hinsichtlich der Beachtung dieser Verordnung und der nach ihr getroffenen Anordnungen, sowie das Beobachten der Gewässer und des Bodens durch die zuständigen Wasserbehörden, die Begünstigte oder deren Beauftragte zu dulden.

(2) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet sind auf Anordnung der unteren Wasserbehörde verpflichtet,

1.  das Errichten und Unterhalten von Einrichtungen zur Sicherung der Zone I gegen unbefugtes Betreten,
2.  das Aufstellen, Unterhalten und Beseitigen von Hinweis-, Warn-, Gebots- und Verbotszeichen,
3.  das Betreten der Grundstücke durch Bedienstete der zuständigen Behörden, die Begünstigte oder deren Beauftragte zum Beobachten, Messen und Untersuchen des Grundwassers und zur Entnahme von Boden- und Vegetationsproben sowie
4.  das Anlegen und Betreiben von Grundwassermessstellen

zu dulden.
Die Anordnung erfolgt durch schriftlichen Bescheid gegenüber den betroffenen Eigentümern oder Nutzungsberechtigten.
Soweit bergrechtliche Belange berührt sind, ergeht die Entscheidung im Benehmen mit der zuständigen Bergbehörde.

### § 10  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 20.
Dezember 2011 in Kraft.
Gleichzeitig treten die Verordnung zur Festsetzung des Wasserschutzgebietes Briesen vom 25.
November 2011 (GVBl.
II Nr. 83) und die mit Beschluss Nummer 123/23/83 vom 2.
März 1983 des Kreistages Fürstenwalde/Spree festgesetzten Trinkwasserschutzgebiete für das Wasserwerk Briesen-Kersdorf und die Brunnenfassung Spreebogen außer Kraft.

Potsdam, den 5.
November 2012

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

### Anlagen

1

[WSGBriesen-Anlg-1](/br2/sixcms/media.php/68/WSGBriesen_neu__an__anlg__1.pdf "WSGBriesen-Anlg-1") 95.3 KB

2

[WSGBriesen-Anlg-2](/br2/sixcms/media.php/68/WSGBriesen_neu__an__anlg__2.pdf "WSGBriesen-Anlg-2") 719.6 KB

3

[WSGBriesen-Anlg-3](/br2/sixcms/media.php/68/WSGBriesen_neu__an__anlg__3.pdf "WSGBriesen-Anlg-3") 2.2 MB

4

[WSGBriesen-Anlg-4-Blatt-01](/br2/sixcms/media.php/68/WSGBriesen_neu__an__anlg__4-Blatt-01.pdf "WSGBriesen-Anlg-4-Blatt-01") 729.0 KB

5

[WSGBriesen-Anlg-4-Blatt-02](/br2/sixcms/media.php/68/WSGBriesen_neu__an__anlg__4-Blatt-02.pdf "WSGBriesen-Anlg-4-Blatt-02") 1.1 MB

6

[WSGBriesen-Anlg-4-Blatt-03](/br2/sixcms/media.php/68/WSGBriesen_neu__an__anlg__4-Blatt-03.pdf "WSGBriesen-Anlg-4-Blatt-03") 933.8 KB

7

[WSGBriesen-Anlg-4-Blatt-04](/br2/sixcms/media.php/68/WSGBriesen_neu__an__anlg__4-Blatt-04.pdf "WSGBriesen-Anlg-4-Blatt-04") 738.4 KB

8

[WSGBriesen-Anlg-4-Blatt-05](/br2/sixcms/media.php/68/WSGBriesen_neu__an__anlg__4-Blatt-05.pdf "WSGBriesen-Anlg-4-Blatt-05") 1.0 MB

9

[WSGBriesen-Anlg-4-Blatt-06](/br2/sixcms/media.php/68/WSGBriesen_neu__an__anlg__4-Blatt-06.pdf "WSGBriesen-Anlg-4-Blatt-06") 1.0 MB

10

[WSGBriesen-Anlg-4-Blatt-07](/br2/sixcms/media.php/68/WSGBriesen_neu__an__anlg__4-Blatt-07.pdf "WSGBriesen-Anlg-4-Blatt-07") 648.8 KB

11

[WSGBriesen-Anlg-4-Blatt-08](/br2/sixcms/media.php/68/WSGBriesen_neu__an__anlg__4-Blatt-08.pdf "WSGBriesen-Anlg-4-Blatt-08") 732.1 KB

12

[WSGBriesen-Anlg-4-Blatt-09](/br2/sixcms/media.php/68/WSGBriesen_neu__an__anlg__4-Blatt-09.pdf "WSGBriesen-Anlg-4-Blatt-09") 1.1 MB

13

[WSGBriesen-Anlg-4-Blatt-10](/br2/sixcms/media.php/68/WSGBriesen_neu__an__anlg__4-Blatt-10.pdf "WSGBriesen-Anlg-4-Blatt-10") 941.8 KB

14

[WSGBriesen-Anlg-4-Blatt-11](/br2/sixcms/media.php/68/WSGBriesen_neu__an__anlg__4-Blatt-11.pdf "WSGBriesen-Anlg-4-Blatt-11") 838.3 KB