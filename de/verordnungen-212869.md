## Verordnung zur Übertragung von Zuständigkeiten im Geschäftsbereich des Präsidenten des Landtages für die Berechnung und Zahlung von Reisekosten sowie die Bewilligung, Berechnung und Zahlung von Trennungsgeld auf die Zentrale Bezügestelle des Landes Brandenburg (Reisekosten-Trennungsgeldzuständigkeitsübertragungsverordnung Landtag - RkTrZÜVLT)

Auf Grund des § 63 Absatz 3 Satz 2 des Landesbeamtengesetzes vom 3. April 2009 (GVBl.
I S.
26) verordnet der Präsident des Landtages im Einvernehmen mit dem Minister der Finanzen:

### § 1  
Übertragung von Aufgaben

(1) Die Zuständigkeit für die Berechnung und Zahlung von Reisekosten im Sinne des § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes wird für den Geschäftsbereich des Präsidenten des Landtages und der Beauftragten des Landes Brandenburg zur Aufarbeitung der Folgen der kommunistischen Diktatur auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

(2) Ebenso wird die Zuständigkeit für die Bewilligung, Berechnung und Zahlung von Trennungsgeld nach § 1 Satz 1 der Brandenburgischen Trennungsgeldverordnung vom 5.
April 2005 (GVBl.
II S.
155), die durch die Verordnung vom 7.
Juli 2009 (GVBl.
II S.
381) geändert worden ist, in Verbindung mit § 9 Absatz 3 der Trennungsgeldverordnung in der Fassung der Bekanntmachung vom 29.
Juni 1999 (BGBl.
I S.
1533), die zuletzt durch Artikel 3 Absatz 38 der Verordnung vom 12.
Februar 2009 (BGBl.
I S.
320) geändert worden ist, auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

### § 2  
Vertretung bei Klagen

Im Rahmen der Übertragung der Zuständigkeit nach § 1 wird die Zentrale Bezügestelle des Landes Brandenburg ermächtigt, den Präsidenten des Landtages und die Beauftragte des Landes Brandenburg zur Aufarbeitung der Folgen der kommunistischen Diktatur in verwaltungs- und arbeitsgerichtlichen Streitigkeiten zu vertreten.
Dies gilt auch für Anträge in einstweiligen Rechtsschutzverfahren.

### § 3  
Übergangsvorschrift

Für Anträge auf Berechnung und Zahlung von Reisekosten sowie auf Bewilligung, Berechnung und Zahlung von Trennungsgeld, die vor dem Inkrafttreten dieser Verordnung eingegangen sind und über die noch nicht abschließend entschieden worden ist, verbleibt es bei der Zuständigkeit der bisher zuständigen Stellen.
Dies gilt auch für die Vertretung in zu diesem Zeitpunkt bereits anhängigen Rechtsstreitigkeiten.

### § 4  
Inkrafttreten

Diese Verordnung tritt am 1.
März 2014 in Kraft.

Potsdam, den 18.
Februar 2014

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch