## Brandenburgisches Patientenmobilitätsumsetzungsgesetz (BbgPat-MobUG)

§ 1  
Gegenstand, Anwendungsbereich
-----------------------------------

(1) Dieses Gesetz dient der Erleichterung des Zugangs zu einer sicheren und hochwertigen grenzüberschreitenden Gesundheitsversorgung innerhalb der Mitgliedstaaten der Europäischen Union.

(2) Dieses Gesetz gilt für jegliche Erbringung von Gesundheitsdienstleistungen an Patientinnen und Patienten unabhängig davon, wie diese organisiert, erbracht oder finanziert wird.

(3) Dieses Gesetz gilt nicht für

1.  Dienstleistungen im Bereich der Langzeitpflege, deren Ziel darin besteht, Personen zu unterstützen, die auf Hilfe bei routinemäßigen, alltäglichen Verrichtungen angewiesen sind,
    
2.  Zuteilung von und Zugang zu Organen zum Zweck der Organtransplantation,
    
3.  Öffentliche Impfprogramme gegen Infektionskrankheiten, die ausschließlich dem Gesundheitsschutz der Bevölkerung dienen und die mit gezielten Planungs- und Durchführungsmaßnahmen verbunden sind.
    

§ 2   
Begriffsbestimmungen
---------------------------

(1) Unter Gesundheitsdienstleistungen sind alle Leistungen zu verstehen, die von Gesundheitsdienstleisterinnen und Gesundheitsdienstleistern gegenüber Patientinnen und Patienten erbracht werden, um deren Gesundheitszustand zu beurteilen, zu erhalten oder wiederherzustellen, einschließlich der Verschreibung, Abgabe und Bereitstellung von Arzneimitteln und Medizinprodukten.

(2) Gesundheitsdienstleisterinnen und Gesundheitsdienstleister sind alle Angehörigen der Gesundheitsberufe und alle juristischen Personen, die Gesundheitsdienstleistungen auf der Grundlage einer staatlichen Erlaubnis entweder persönlich oder durch bei ihnen beschäftigte Personen gegenüber Patientinnen und Patienten erbringen.

(3) Angehörige der Gesundheitsberufe sind Ärztinnen und Ärzte, Gesundheits- und Krankenpflegerinnen und Gesundheits- und Krankenpfleger, Zahnärztinnen und Zahnärzte, Hebammen und Entbindungspfleger, Apothekerinnen und Apotheker oder andere Fachkräfte, die im Gesundheitswesen Tätigkeiten ausüben, die einem reglementierten Beruf im Sinne von Artikel 3 Absatz 1 Buchstabe a der Richtlinie 2005/36/EG vorbehalten sind oder Personen, die nach den Vorschriften des Bundes und der Länder als Angehörige eines geregelten Gesundheitsberufes gelten.

(4) Patientin oder Patient ist jede natürliche Person, die Gesundheitsdienstleistungen in Anspruch nimmt oder nehmen möchte.

§ 3  
Informationspflichten
---------------------------

(1) Gesundheitsdienstleisterinnen und –leister sind verpflichtet, auf Nachfrage von Patientinnen und Patienten diesen einschlägige Informationen zu erteilen, um ihnen eine sachkundige Entscheidung zur Inanspruchnahme der nachgefragten Gesundheitsdienstleistung zu ermöglichen.
Hierzu zählen insbesondere Informationen über Behandlungsoptionen, Verfügbarkeit, Qualität und Sicherheit der erbrachten Gesundheitsversorgung, ihren Zulassungs- und Registrierungsstatus und ihren Versicherungsschutz nach § 4 sowie klare Preisinformationen und Rechnungen.

(2) Auf abhängig Beschäftigte findet Absatz 1 keine Anwendung.
Soweit Gesundheitsdienstleisterinnen und -leister den im Behandlungsstaat ansässigen Patientinnen und Patienten bereits einschlägige Informationen hierzu zur Verfügung stellen, sind sie nicht verpflichtet, Patientinnen und Patienten aus anderen Mitgliedstaaten ausführlichere Informationen zur Verfügung zu stellen.

§ 4  
Absicherung von Schadensersatzansprüchen
----------------------------------------------

(1) Gesundheitsdienstleisterinnen und -dienstleister müssen zur Deckung von Schadensersatzansprüchen eine Berufshaftpflichtversicherung abschließen oder durch eine Garantie oder ähnliche Regelung, die im Hinblick auf ihren Zweck gleichwertig oder im Wesentlichen vergleichbar und nach Art, Umfang dem Risiko angemessen ist, abgesichert sein.
Dies gilt auch für Personen, die die Heilkunde aufgrund einer Erlaubnis nach § 1 Absatz 1 des Heilpraktikergesetzes in der im Bundesgesetzblatt Teil III, Gliederungsnummer 2122-2, veröffentlichten bereinigten Fassung, das zuletzt durch Artikel 15 des Gesetzes vom 23.
Oktober 2001 (BGBl.
I S. 2702, 2705) geändert worden ist, ausüben.

(2) Auf abhängig Beschäftigte findet Absatz 1 keine Anwendung.

§ 5   
Informationsübermittlung und Verwaltungszusammenarbeit
-------------------------------------------------------------

(1) Gesundheitsdienstleisterinnen und –dienstleister sind verpflichtet, zur Erfüllung der der nationalen Kontaktstelle gemäß Artikel 6 Absatz 3 der Richtlinie 2011/24/EU obliegenden Informationspflichten, auf Nachfrage den in § 219d Absatz 1 Satz 3 des Fünften Buches Sozialgesetzbuch genannten Organisationen die erforderlichen Informationen zur Verfügung zu stellen.
Das gilt nicht, soweit die nationale Kontaktstelle nach § 219d des Fünften Buches Sozialgesetzbuch diese Informationen bereits anderweitig erhalten hat.

(2) Die Bereitstellung von Informationen nach Artikel 10 Absatz 4 der Richtlinie 2011/24/EU im Binnenmarktinformationssystem erfolgt durch die jeweiligen für die Gesundheitsberufe zuständigen Landesämter.

[1](#_ftnref1))  Artikel 1 dieses Gesetzes dient mit Ausnahme von § 4 Absatz 1 Satz 2 der Umsetzung der Richtlinie 2011/24/EU des Europäischen Parlaments und des Rates vom 9.
März 2011 über die Ausübung der Patientenrechte in der grenzüberschreitenden Gesundheitsversorgung (ABl.
L 88 vom 4.4.2011, S.
 45), die durch die Richtlinie 2013/64/EU (ABl.
L 353 vom 28.12.2013, S.
 8) geändert worden ist.