## Staatsvertrag zwischen dem Land Berlin und dem Land Brandenburg über die Errichtung und den Betrieb der Justizvollzugsanstalt Heidering

Das Land Berlin und das Land Brandenburg schließen folgenden Vertrag:

### Artikel 1  
Errichtung, anzuwendendes Landesrecht

(1) Das Land Berlin errichtet und betreibt in der Gemeinde Großbeeren, Landkreis Teltow-Fläming, die Justizvollzugsanstalt Heidering (im Folgenden: Anstalt) als Anstalt nach Berliner Landesrecht für Gefangene des Landes Berlin.
Für die Anstalt gilt das Vollzugsrecht des Landes Berlin, soweit nicht Bundesrecht Anwendung findet.
Die Anstalt unterliegt der Aufsicht des Landes Berlin, die von der Senatsverwaltung für Justiz wahrgenommen wird.

(2) Die Bediensteten der Anstalt stehen in einem Dienst- bzw.
Arbeitsverhältnis zum Land Berlin, für das ausschließlich die im Land Berlin geltenden Vorschriften gelten.

(3) Sonstige behördliche Zuständigkeiten nach dem Recht des Landes Brandenburg bleiben unberührt.

### Artikel 2  
Gerichtliche Zuständigkeit

Zuständig sind die Strafvollstreckungskammer und die Jugendkammer bei dem Landgericht, das nach dem Landesrecht Berlins für den Sitz der jeweiligen Aufsichtsbehörde örtlich zuständig ist (§ 78a Absatz 3 des Gerichtsverfassungsgesetzes, § 92 Absatz 2 Satz 3 des Jugendgerichtsgesetzes).
Die Vollstreckungsleitung obliegt dem Jugendrichter oder der Jugendrichterin des Amtsgerichts, das nach dem Landesrecht Berlins örtlich zuständig ist (§ 85 Absatz 3 des Jugendgerichtsgesetzes).

### Artikel 3  
Kosten

(1) Die mit der Errichtung und dem Betrieb der Anstalt verbundenen Kosten trägt das Land Berlin.

(2) Aufwendungen, die dem Land Brandenburg durch den Betrieb der Anstalt entstehen, werden vom Land Berlin erstattet, soweit keine Gebühren nach dem Gebührenrecht des Landes Brandenburg erhoben werden.
Das Nähere kann durch eine Verwaltungsvereinbarung geregelt werden.
Eine Erstattung in Form von Aufwendungspauschalen ist zulässig.

### Artikel 4  
Laufzeit

Dieser Vertrag wird auf unbestimmte Zeit geschlossen.
Er kann nur einvernehmlich geändert werden.

### Artikel 5   
Inkrafttreten

Dieser Vertrag bedarf der Ratifikation.
Er tritt zum Ersten des Monats in Kraft, der auf den Austausch der Ratifikationsurkunden folgt.

Berlin, den 25.
August 2011

Für das Land Berlin  
In Vertretung des Regierenden Bürgermeisters  
Die Senatorin für Justiz

Gisela von der Aue

Für das Land Brandenburg  
Der Ministerpräsident  
vertreten durch den Minister der Justiz

Dr.
Volkmar Schöneburg

[zum Gesetz](/de/gesetze-212613)