## Verordnung über das Naturschutzgebiet „Oderinsel Küstrin-Kietz“

Auf Grund der §§ 22, 23 und § 32 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S.
2542) in Verbindung mit § 19 Absatz 1 und 2 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S.
350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

#### § 1  
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Märkisch-Oderland wird als Naturschutzgebiet festgesetzt.
 Das Naturschutzgebiet trägt die Bezeichnung „Oderinsel Küstrin-Kietz“.

#### § 2   
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 213 Hektar.
Es umfasst Flächen in folgender Flur:

**Gemeinde:**  

**Gemarkung:**  

**Flur:**

Küstriner Vorland  

Küstrin-Kietz  

1.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte topografische Karte im Maßstab 1 : 10 000 ermöglicht die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 5 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Innerhalb des Naturschutzgebietes wird gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes eine Zone 1 als Naturentwicklungsgebiet festgesetzt, das der direkten menschlichen Einflussnahme entzogen ist und in dem Lebensräume und Lebensgemeinschaften langfristig ihrer natürlichen Entwicklung überlassen bleiben.
Die Zone 1 besteht aus zwei Teilflächen und umfasst rund 73 Hektar in folgender Flur:

**Gemeinde:**  

**Gemarkung:**  

**Flur:**

Küstriner Vorland  

Küstrin-Kietz  

1.

Die Grenze der Zone 1 ist in der in Absatz 1 genannten Kartenskizze und in der in Anlage 2 Nummer 1 genannten topografischen Karte sowie in den in Anlage 2 Nummer 2 genannten Liegenschaftskarten mit den Blattnummern 1 bis 5 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Märkisch-Oderland, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3  
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes, das einen Flussabschnitt der Oder mit einer Insel und der gegenüberliegenden Vorlandfläche umfasst, ist

1.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere von Sumpf-, Ried- und Röhrichtgesellschaften, von wechselfeuchtem Auengrünland sowie von artenreichen Saum- und naturnahen Laubwaldgesellschaften;
    
2.  die Erhaltung und Entwicklung von Lebensstätten wild lebender Pflanzenarten, darunter zahlreiche im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Arten der Stromtäler, beispielsweise die gebietsheimische Schwarz-Pappel (Populus nigra), Sumpfwolfsmilch (Euphorbia palustris), Gottes-Gnadenkraut (Gratiola officinalis), Wasserschwertlilie (Iris pseudacorus) sowie Kantiger Lauch (Allium angulosa);
    
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Säugetiere, Vögel, Reptilien, Amphibien, Fische und Libellen, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Wasserfledermaus (Myotis daubentoni), Großer Abendsegler (Nyctalus noctula), Wasserspitzmaus (Neomys fodiens), Flussuferläufer (Actitis hypoleucos), Wiedehopf (Upupa epops), Gänsesäger (Mergus merganser), Rotschenkel (Tringa totanus), Löffelente (Anas clypeata), Blässgans (Anser albifrons), Ringelnatter (Natrix natrix), Wechselkröte (Bufo viridis), Zährte (Vimba vimba), Schmerle (Neomacheilus barbatulus) und Gebänderte Prachtlibelle (Calopteryx splendens);
    
4.  die Erhaltung naturnaher Waldlebensgemeinschaften zur Beobachtung und Erforschung;
    
5.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit des Gebietes mit seinen auengeprägten Grünland- und Gehölzbeständen an der Oder sowie deren Alt- und Nebenarmen in ihrer natürlichen Überflutungsdynamik;
    
6.  die Erhaltung des Gebietes als wesentlicher Bestandteil des überregionalen Biotopverbundes innerhalb des Odertals.
    

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung

1.  eines Teiles des Europäischen Vogelschutzgebietes „Mittlere Oderniederung“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) in seiner Funktion
    
    1.  als Lebensraum von Arten nach Anhang I der Richtlinie 2009/147/EG, insbesondere Seeadler (Haliaeetus albicilla), Schwarzstorch (Ciconia nigra), Weißstorch (Ciconia ciconia), Kranich (Grus grus), Rohrweihe (Circus aeruginosus), Eisvogel (Alcedo atthis) und Neuntöter (Lanius collurio) einschließlich ihrer Brut- und Nahrungsbiotope,
        
    2.  als Vermehrungs-, Rast-, Mauser- und Überwinterungsgebiet für im Gebiet regelmäßig auftretende Zugvogelarten wie Flussseeschwalbe (Sterna hirundo), Silberreiher (Egretta alba), Zwergsäger (Mergus albellus), Schwarzmilan (Milvus migrans) und Fischadler (Pandion haliaetus);
        
2.  des Gebietes von gemeinschaftlicher Bedeutung „Oderinsel Kietz“ sowie eines Teiles des Gebietes von gemeinschaftlicher Bedeutung „Oder-Neiße Ergänzung“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinem Vorkommen von
    
    1.  natürlich eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Flüssen der planaren Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Flüssen mit Schlammbänken mit Vegetation des Chenopodion rubri p.p.
        und des Bidention p.p., feuchten Hochstaudenfluren der planaren Stufe und Hartholzauenwäldern mit Quercus robur (Stiel-Eiche), Ulmus laevis (Flatter-Ulme), Ulmus minor (Feld-Ulme), Fraxinus excelsior (Gewöhnliche Esche) oder Fraxinus angustifolia (Schmalblättrige Esche) (Ulmenion minoris) als Biotope von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG),
        
    2.  Auen-Wäldern mit Alnus glutinosa (Schwarz-Erle) und Fraxinus excelsior (Gewöhnliche Esche) (Alno-Padion, Alnion incanae, Salicion albae) als prioritäres Biotop („prioritärer Lebensraumtyp“ im Sinne des Anhangs I der Richtlinie 92/43/EWG),
        
    3.  Biber (Castor fiber), Fischotter (Lutra lutra), Rotbauchunke (Bombina bombina), Rapfen (Aspius aspius), Steinbeißer (Cobitis taenia), Flussneunauge (Lampetra fluviatilis), Bachneunauge (Lampetra planeri), Schlammpeitzger (Misgurnus fossilis), Bitterling (Rhodeus sericeus amarus), Weißflossiger Gründling (Gobio albipinnatus) und Grüner Keiljungfer (Ophiogomphus cecilia) als Tierarten von gemeinschaftlichem Interesse (im Sinne des Anhangs II der Richtlinie 92/43/EWG) einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume,
        
    4.  Eremit (Osmoderma eremita) als prioritärer Tierart von gemeinschaftlichem Interesse (im Sinne des Anhangs II der Richtlinie 92/43/EWG) einschließlich seiner für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.
        

(3) Darüber hinaus ist besonderer Schutzzweck der Zone 1 (Naturentwicklungsgebiet) die Entwicklung natürlicher Auwaldökosysteme sowie der Schutz der Gesamtheit ökologischer Prozesse in ihrer natürlichen Dynamik.

#### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
    
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
    
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
    
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
    
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
    
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
    
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
    
8.  die Ruhe der Natur durch Lärm zu stören;
    
9.  das Gebiet außerhalb der Wege zu betreten;
    
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 51 des Brandenburgischen Naturschutzgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
    
11.  mit Fahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
    
12.  zu baden oder zu tauchen; das Baden und Tauchen in der Bundeswasserstraße Oder bleibt mit Ausnahme des Vorflutkanals zulässig;
    
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen außerhalb der Bundeswasserstraße Strom-Oder zu benutzen;
    
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
    
15.  Hunde frei laufen zu lassen;
    
16.  Entwässerungsmaßnahmen durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
    
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (zum Beispiel Abwasser, Klärschlamm und Bioabfälle) zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
    
18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
    
19.  Tiere zu füttern oder Futter bereitzustellen;
    
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
    
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
    
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
    
23.  Pflanzenschutzmittel jeder Art anzuwenden;
    
24.  Wiesen, Weiden oder sonstiges Grünland (nachzusäen), umzubrechen oder neu anzusäen.
    

#### § 5   
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen außerhalb der Zone 1 mit der Maßgabe, dass
    
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Dünger inklusive Exkrementen von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Äquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und Sekundärrohstoffdünger einzusetzen,  
        
    2.  bei Beweidung ab einer Weidebesatzdichte von 0,5 RGV je Hektar pro Weidegang Gehölze gegen Verbiss und sonstige Beeinträchtigungen sowie Ränder von Gewässern gegen Trittschäden geschützt werden,
        
    3.  das Grünland auf den in der in § 2 Absatz 2 genannten topografischen Karte und in der Liegenschaftskarte gekennzeichneten Flurstücke 1303 (teilweise), 1304 (teilweise), 1305 (teilweise), 1307, 1308 und 1655 (teilweise) nicht vor dem 16.
         Juni eines jeden Jahres genutzt wird.
        Dies gilt nicht bei ausschließlicher Nutzung durch Beweidung mit einer maximalen Weidebesatzdichte von 0,5 RGV je Hektar pro Weidegang,  
        
    4.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt;
        
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen außerhalb der Zone 1 mit der Maßgabe, dass
    
    1.  eine Nutzung ausschließlich einzelstammweise erfolgt,
        
    2.  nur Arten der potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Gehölzarten unter Ausschluss eingebürgerter Arten zu verwenden sind.
         Nebenbaumarten dürfen dabei nicht als Hauptbaumart eingesetzt werden,
        
    3.  ein Altholzanteil von mindestens zehn vom Hundert am aktuellen Bestandesvorrat zu sichern ist,
        
    4.  mindestens fünf Stämme je Hektar mit einem Mindestdurchmesser von 30 Zentimetern in 1,30 Meter über Stammfuß bis zum Absterben aus der Nutzung zu nehmen sind,
        
    5.  Totholz nicht entfernt wird,
        
    6.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
        
    7.  § 4 Absatz 2 Nummer 23 gilt;
        
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung auf den bisher rechtmäßig dafür genutzten Flächen außerhalb der Zone 1 mit der Maßgabe, dass
    
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass ein Einschwimmen und eine Gefährdung des Bibers und Fischotters weitgehend ausgeschlossen sind,
        
    2.  die Nutzung des Odernebenarms vom 1.
        April bis zum 30.
        September eines jeden Jahres ausschließlich in Form der stillen Fischerei wie Stellnetz- und Reusenfischerei unter Ausschluss der Elektrofischerei vom Wasser aus erfolgt,
        
    3.  der Fischbesatz nur mit heimischen Fischarten erfolgt und dabei eine Gefährdung der in § 3 Absatz 2 Nummer 2 Buchstabe c genannten Arten ausgeschlossen ist; § 13 der Fischereiordnung für das Land Brandenburg bleibt unberührt,
        
    4.  § 4 Absatz 2 Nummer 19 gilt;
        
4.  die rechtmäßige Ausübung der Angelfischerei außerhalb der Zone 1 vom Vorland am Vorflutkanal und an der Strom-Oder mit der Maßgabe, dass § 4 Absatz 2 Nummer 13 gilt.
    Auf der Oderinsel ist das Angeln innerhalb des Schutzgebietes unzulässig;
    
5.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)  die Jagd in der Zeit vom 1.
        März bis 30.
        Juni eines jeden Jahres ausschließlich vom Ansitz aus erfolgt,
        
        bb)  die Jagd auf Federwild unzulässig ist,
        
        cc)   die Fallenjagd mit Lebendfallen erfolgt,
        
        dd)     die Baujagd in einem Abstand von 100 Metern bis zu den Gewässerufern unzulässig ist,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Aufstellung anzuzeigen.
        Die untere Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten.
        Die Entscheidung hierüber soll unverzüglich erfolgen,
        
    3.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope.
        
    
    Unzulässig bleiben Wildfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern.
    
6.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
    
7.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer sowie die ordnungsgemäße Unterhaltung der im Gebiet gelegenen Bundeswasserstraße Strom-Oder, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
    
8.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen.
    Die ordnungsgemäße Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
    
9.  die ordnungsgemäße Unterhaltung der Anlagen an Bundeswasserstraßen sowie der Bau und die wesentliche Änderung von Anlagen, sofern sie nicht einer Planfeststellung unterliegen, im Benehmen mit der unteren Naturschutzbehörde.
    Gegenstand der Benehmensherstellung ist dabei auch die Prüfung der Verträglichkeit mit den in § 3 Absatz 2 genannten Erhaltungszielen des Gebietes.
    Soll bei der Durchführung der Maßnahme von der Stellungnahme der unteren Naturschutzbehörde abgewichen werden, entscheidet hierüber die Wasserschifffahrtsdirektion im Benehmen mit der Fachbehörde für Naturschutz und Landschaftspflege;
    
10.  Maßnahmen zur ordnungsgemäßen Unterhaltung der angrenzenden Hochwasserschutzanlagen nach Anzeige gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes bei der unteren Naturschutzbehörde.
    Eine Anzeige ist nicht erforderlich, wenn den Maßnahmen ein abgestimmter Unterhaltungsplan zugrunde liegt.
    
    Ausgenommen von den Verboten des § 4 bleiben weiterhin Maßnahmen des Hochwasserschutzes zur Beseitigung von Abflusshindernissen im Deichvorland, wie zum Beispiel Stammholz, Äste, Treibgut, wenn dadurch der Schutzzweck nicht gefährdet wird.
    Die Maßnahmen sind der unteren Naturschutzbehörde gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes schriftlich anzuzeigen;
    
11.  das Ein- und Aussetzen von muskelkraftbetriebenen Booten am Wasserwanderrastplatz Küstriner Vorland am Ufer des Vorflutkanals sowie an der Oder in dem in der topografischen Karte gekennzeichneten Bereich stromaufwärts der Eisenbahnbrücke;
    
12.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
    
13.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
    
14.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der unteren Naturschutzbehörde angeordnet oder zugelassen worden sind;
    
15.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen;
    
16.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten unbeschadet anderer Regelungen nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Sie gelten weiterhin nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
 Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

#### § 6  
Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  im gesamten natürlichen Überschwemmungsbereich soll die Flussauendynamik erhalten und durch geeignete Maßnahmen gefördert werden.
    Verwallte Flächen sollen wieder in das Überschwemmungsgebiet eingegliedert werden;
    
2.  die Müll- und Schuttverfüllungen in den temporären Kleingewässern auf dem nördlich gelegenen Inselteil sollen beräumt werden;
    
3.  die natürlichen Verjüngungsstandorte der Schwarzpappel sollen, falls erforderlich, vorübergehend durch Einzäunungen vor Verbiss geschützt werden;
    
4.  für die Auwaldflächen werden niedrige Schalenwildbestände angestrebt;
    
5.  die Beweidung des Grünlands soll mit landschaftsangepassten Rassen erfolgen;
    
6.  die Habitatbedingungen des Fledermauswinterquartiers in der Lünette B (ehemalige Außenfestungsanlage Küstrin östlich des Vorfluters) sollen optimiert werden;
    
7.  gebietsfremde Gehölze im Bereich der Lünette B sollen entnommen werden;
    
8.  für die Besucherlenkung und zur Förderung des Naturerlebens soll eine Beobachtungsplattform auf der erhöhten Fläche nordwestlich des Oderaltarms errichtet sowie ein Naturlehrpfadkonzept entlang des Kronmühlendamms bis zur Lünette B und vom Wehr im Vorflutkanal bis nördlich der Mündung des Oderaltarms eingerichtet werden.
    

#### § 7  
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu 50 000 (in Worten: fünfzigtausend) Euro geahndet werden.

#### § 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit  
§ 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere §§ 31 bis 33 und § 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

#### § 10  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11   
Inkrafttreten

§ 5 Absatz 1 Nummer 1 Buchstabe a, c und d tritt am 1.
Juli 2011 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 12.
November 2010

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

 

**Anlage 1  
**(zu § 2 Absatz 1)

![Das rund 213 Hektar große Naturschutzgebiet "Oderinsel Küstrin-Kietz" liegt in der Gemeinde Küstriner Vorland (Landkreis Märkisch-Oderland). Es umfasst Teile der Gemarkung Küstrin-Kietz und liegt in unmittelbarer Nähe östlich der Ortslage Küstrin-Kietz.](/br2/sixcms/media.php/69/K%C3%BCstrin-2gif.gif "Das rund 213 Hektar große Naturschutzgebiet "Oderinsel Küstrin-Kietz" liegt in der Gemeinde Küstriner Vorland (Landkreis Märkisch-Oderland). Es umfasst Teile der Gemarkung Küstrin-Kietz und liegt in unmittelbarer Nähe östlich der Ortslage Küstrin-Kietz.")

 

**Anlage 2**  
(zu § 2 Absatz 2)

1.
 Topografische Karten Maßstab 1 : 10 000

**Titel:** Topografische Karte zur Verordnung über das Naturschutzgebiet „Oderinsel Küstrin-Kietz“

**Blattnummer**

**Unterzeichnung**

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV) am 28.
Juni 2010

   
2.
 Liegenschaftskarten Maßstab 1 : 2 500

**Titel:** Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Oderinsel Küstrin-Kietz“

**Blattnummer**

**Gemarkung**

**Flur**

**Unterzeichnung**

1

Küstriner Vorland

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MUGV am 28.
Juni 2010

2

Küstriner Vorland

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MUGV am 28.
Juni 2010

3

Küstriner Vorland

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MUGV am 28.
Juni 2010

4

Küstriner Vorland

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MUGV am 28.
Juni 2010

5

Küstriner Vorland

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 7 des MUGV am 28.
Juni 2010

**Anlage 3  
**(zu § 2 Absatz 2)

Flurstücksliste zur Verordnung über das Naturschutzgebiet „Oderinsel Küstrin-Kietz“

Landkreis: Märkisch-Oderland

Gemeinde

Gemarkung

Flur

Flurstücke

Küstriner Vorland

Küstrin-Kietz

1

589 bis 600, 1133/1, 1231/2, 1242/2, 1243/2, 1253 teilweise, 1254/1, 1254/2, 1255, 1256, 1257/2, 1258 bis 1261, 1268 bis 1271, 1292 teilweise, 1293 teilweise, 1298 bis 1305, 1307, 1308, 1310 teilweise, 1373 teilweise, 1377, 1546 bis 1550, 1552, 1650 teilweise, 1655 teilweise, 1784.

Flächen der Zone 1:

Landkreis: Märkisch-Oderland

Gemeinde

Gemarkung

Flur

Flurstücke

Küstriner Vorland

Küstrin-Kietz

1

1253 teilweise, 1254/1, 1254/2 teilweise, 1255, 1256, 1257/2 teilweise, 1292 teilweise, 1302, 1303 teilweise, 1304 teilweise, 1305 teilweise, 1655 teilweise.