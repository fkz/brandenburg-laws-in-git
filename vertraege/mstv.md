## Medienstaatsvertrag (MStV)

Inhaltsübersicht

[Präambel](#1a)

### I.
Abschnitt  
Anwendungsbereich, Begriffsbestimmungen

[§ 1 Anwendungsbereich](#1)

[§ 2 Begriffsbestimmungen](#2)

### II.
Abschnitt  
Allgemeine Bestimmungen

#### 1.
Unterabschnitt  
Rundfunk

[§ 3 Allgemeine Grundsätze](#3)

[§ 4 Informationspflichten, Verbraucherschutz](#4)

[§ 5 Auskunftsrechte](#5)

[§ 6 Sorgfaltspflichten](#6)

[§ 7 Barrierefreiheit](#7)

[§ 8 Werbegrundsätze, Kennzeichnungspflichten](#8)

[§ 9 Einfügung von Rundfunkwerbung und Teleshopping](#9)

[§ 10 Sponsoring](#10)

[§ 11 Gewinnspiele](#11)

[§ 12 Datenverarbeitung zu journalistischen Zwecken, Medienprivileg](#12)

[§ 13 Übertragung von Großereignissen](#13)

[§ 14 Kurzberichterstattung](#14)

[§ 15 Europäische Produktionen, Eigen-, Auftrags- und Gemeinschaftsproduktionen](#15)

[§ 16 Auskunftspflicht und zuständige Behörden nach dem Europäischen Übereinkommen über das grenzüberschreitende Fernsehen](#16)

#### 2.
Unterabschnitt  
Telemedien

[§ 17 Allgemeine Grundsätze, Zulassungs- und Anmeldefreiheit](#17)

[§ 18 Informationspflichten und Auskunftsrechte](#18)

[§ 19 Sorgfaltspflichten](#19)

[§ 20 Gegendarstellung](#20)

[§ 21 Barrierefreiheit](#21)

[§ 22 Werbung, Sponsoring, Gewinnspiele](#22)

[§ 23 Datenverarbeitung zu journalistischen Zwecken, Medienprivileg](#23)

[§ 24 Telemediengesetz, Öffentliche Stellen](#24)

[§ 25 Notifizierung](#25)

### III.
Abschnitt  
Besondere Bestimmungen für den öffentlich-rechtlichen Rundfunk

[§ 26 Auftrag](#26)

[§ 27 Angebote](#27)

[§ 28 Fernsehprogramme](#28)

[§ 29 Hörfunkprogramme](#29)

[§ 30 Telemedienangebote](#30)

[§ 31 Satzungen, Richtlinien, Berichtspflichten](#31)

[§ 32 Telemedienkonzepte](#32)

[§ 33 Jugendangebot](#33)

[§ 34 Funktionsgerechte Finanzausstattung, Grundsatz des Finanzausgleichs](#34)

[§ 35 Finanzierung](#35)

[§ 36 Finanzbedarf des öffentlich-rechtlichen Rundfunks](#36)

[§ 37 Berichterstattung der Rechnungshöfe](#37)

[§ 38 Zulässige Produktplatzierung](#38)

[§ 39 Dauer der Rundfunkwerbung, Sponsoring](#39)

[§ 40 Kommerzielle Tätigkeiten](#40)

[§ 41 Beteiligung an Unternehmen](#41)

[§ 42 Kontrolle der Beteiligung an Unternehmen](#42)

[§ 43 Kontrolle der kommerziellen Tätigkeiten](#43)

[§ 44 Haftung für kommerziell tätige Beteiligungsunternehmen](#44)

[§ 45 Richtlinien](#45)

[§ 46 Änderung der Werbung](#46)

[§ 47 Ausschluss von Teleshopping](#47)

[§ 48 Versorgungsauftrag](#48)

[§ 49 Veröffentlichung von Beanstandungen](#49)

### IV.
Abschnitt  
Besondere Bestimmungen für den privaten Rundfunk

#### 1.
Unterabschnitt  
Anwendungsbereich, Programmgrundsätze

[§ 50 Anwendungsbereich](#50)

[§ 51 Programmgrundsätze](#51)

#### 2.
Unterabschnitt  
Zulassung

[§ 52 Grundsatz](#52)

[§ 53 Erteilung einer Zulassung für Veranstalter von bundesweit ausgerichtetem Rundfunk](#53)

[§ 54 Zulassungsfreie Rundfunkprogramme](#54)

[§ 55 Grundsätze für das Zulassungsverfahren](#55)

[§ 56 Auskunftsrechte und Ermittlungsbefugnisse](#56)

[§ 57 Publizitätspflicht und sonstige Vorlagepflichten](#57)

[§ 58 Vertraulichkeit](#58)

#### 3.
Unterabschnitt  
Sicherung der Meinungsvielfalt

[§ 59 Meinungsvielfalt, regionale Fenster](#59)

[§ 60 Sicherung der Meinungsvielfalt im Fernsehen](#60)

[§ 61 Bestimmung der Zuschaueranteile](#61)

[§ 62 Zurechnung von Programmen](#62)

[§ 63 Veränderung von Beteiligungsverhältnissen](#63)

[§ 64 Vielfaltssichernde Maßnahmen](#64)

[§ 65 Sendezeit für unabhängige Dritte](#65)

[§ 66 Programmbeirat](#66)

[§ 67 Richtlinien](#67)

[§ 68 Sendezeit für Dritte](#68)

#### 4.
Unterabschnitt  
Finanzierung, Werbung

[§ 69 Finanzierung](#69)

[§ 70 Dauer der Fernsehwerbung](#70)

[§ 71 Teleshopping-Fenster und Eigenwerbekanäle](#71)

[§ 72 Satzungen und Richtlinien](#72)

[§ 73 Ausnahmen für regionale und lokale Fernsehprogramme](#73)

### V.
Abschnitt  
Besondere Bestimmungen für einzelne Telemedien

#### 1.
Unterabschnitt  
Rundfunkähnliche Telemedien

[§ 74 Werbung, Gewinnspiele](#74)

[§ 75 Kurzberichterstattung](#75)

[§ 76 Barrierefreiheit](#76)

[§ 77 Europäische Produktionen](#77)

#### 2.
Unterabschnitt  
Medienplattformen und Benutzeroberflächen

[§ 78 Anwendungsbereich](#78)

[§ 79 Allgemeine Bestimmungen](#79)

[§ 80 Signalintegrität, Überlagerungen und Skalierungen](#80)

[§ 81 Belegung von Medienplattformen](#81)

[§ 82 Zugang zu Medienplattformen](#82)

[§ 83 Zugangsbedingungen zu Medienplattformen](#83)

[§ 84 Auffindbarkeit in Benutzeroberflächen](#84)

[§ 85 Transparenz](#85)

[§ 86 Vorlage von Unterlagen, Zusammenarbeit mit der Regulierungsbehörde für Telekommunikation](#86)

[§ 87 Bestätigung der Unbedenklichkeit](#87)

[§ 88 Satzungen, Richtlinien](#88)

[§ 89 Überprüfungsklausel](#89)

[§ 90 Bestehende Zulassungen, Zuordnungen, Zuweisungen, Anzeige von bestehenden Medienplattformen oder Benutzeroberflächen](#90)

#### 3.
Unterabschnitt  
Medienintermediäre

[§ 91 Anwendungsbereich](#91)

[§ 92 Inländischer Zustellungsbevollmächtigter](#92)

[§ 93 Transparenz](#93)

[§ 94 Diskriminierungsfreiheit](#94)

[§ 95 Vorlage von Unterlagen](#95)

[§ 96 Satzungen und Richtlinien](#96)

#### 4.
Unterabschnitt  
Video-Sharing-Dienste

[§ 97 Anwendungsbereich](#97)

[§ 98 Werbung](#98)

[§ 99 Schlichtungsstelle](#99)

### VI.
Abschnitt  
Übertragungskapazitäten, Weiterverbreitung

[§ 100 Grundsatz](#100)

[§ 101 Zuordnung von drahtlosen Übertragungskapazitäten](#101)

[§ 102 Zuweisung von drahtlosen Übertragungskapazitäten an private Anbieter durch die zuständige Landesmedienanstalt](#102)

[§ 103 Weiterverbreitung](#103)

### VII.
Abschnitt  
Medienaufsicht

[§ 104 Organisation](#104)

[§ 105 Aufgaben](#105)

[§ 106 Zuständige Landesmedienanstalt](#106)

[§ 107 Verfahren bei Zulassung, Zuweisung und Anzeige](#107)

[§ 108 Rücknahme, Widerruf von Zulassungen und Zuweisungen](#108)

[§ 109 Maßnahmen bei Rechtsverstößen](#109)

[§ 110 Vorverfahren](#110)

[§ 111 Zusammenarbeit mit anderen Behörden](#111)

[§ 112 Finanzierung besonderer Aufgaben](#112)

[§ 113 Datenschutzaufsicht bei Telemedien](#113)

### VIII.
Abschnitt  
Revision, Ordnungswidrigkeiten

[§ 114 Revision zum Bundesverwaltungsgericht](#114)

[§ 115 Ordnungswidrigkeiten](#115)

### IX.
Abschnitt  
Übergangs- und Schlussvorschriften

[§ 116 Kündigung](#116)

[§ 117 Übergangsbestimmung für Produktplatzierungen](#117)

[§ 118 Übergangsbestimmung für Telemedienkonzepte](#118)

[§ 119 Übergangsbestimmung für Zulassungen und Anzeigen](#119)

[§ 120 Übergangsbestimmung zur Bestimmung der Zuschaueranteile](#120)

[§ 121 Übergangsbestimmung für Benutzeroberflächen](#121)

[§ 122 Regelung für Bayern](#122)

[Anlage (zu § 28 Abs.
1 Nr.
2 des Medienstaatsvertrages)](#aa)

[Anlage (zu § 28 Abs.
3 Nr.
2 des Medienstaatsvertrages)](#ab)

[Anlage (zu § 29 Abs.
3 Nr.
3 des Medienstaatsvertrages)](#ac)

[Anlage (zu § 30 Abs.
5 Satz 1 Nr.
4 des Medienstaatsvertrages)](#ad)

[Anlage (zu § 33 Abs.
5 Satz 1 des Medienstaatsvertrages)](#ae)

**Präambel**

Dieser Staatsvertrag der Länder enthält grundlegende Regelungen für die Veranstaltung und das Angebot, die Verbreitung und die Zugänglichmachung von Rundfunk und Telemedien in Deutschland.
Er trägt der europäischen und technischen Entwicklung der Medien Rechnung.

Öffentlich-rechtlicher Rundfunk und privater Rundfunk sind der freien individuellen und öffentlichen Meinungsbildung sowie der Meinungsvielfalt verpflichtet.
Beide Säulen des dualen Rundfunksystems müssen in der Lage sein, den Anforderungen des nationalen und des internationalen Wettbewerbs zu entsprechen.

Für den öffentlich-rechtlichen Rundfunk sind Bestand und Entwicklung zu gewährleisten.
Dazu gehört seine Teilhabe an allen neuen technischen Möglichkeiten in der Herstellung und zur Verbreitung sowie die Möglichkeit der Veranstaltung neuer Angebotsformen und Nutzung neuer Verbreitungswege.
Seine finanziellen Grundlagen einschließlich des dazugehörigen Finanzausgleichs sind zu erhalten und zu sichern.

Den privaten Veranstaltern werden Ausbau und Fortentwicklung eines privaten Rundfunksystems, vor allem in technischer und programmlicher Hinsicht, ermöglicht.
Dazu sollen ihnen ausreichende Sendekapazitäten zur Verfügung gestellt und angemessene Einnahmequellen erschlossen werden.

Die Vermehrung der Medienangebote (Rundfunk und Telemedien) in Europa durch die Möglichkeiten der fortschreitenden Digitalisierung stärkt die Informationsvielfalt und das kulturelle Angebot auch im deutschsprachigen Raum.
Gleichzeitig bedarf es auch und gerade in einer zunehmend durch das Internet geprägten Medienwelt staatsvertraglicher Leitplanken, die journalistische Standards sichern und kommunikative Chancengleichheit fördern.
Für die Angebote des dualen Rundfunksystems sowie der Presse bedarf es hierbei auch Regeln, die den Zugang zu Verbreitungswegen und eine diskriminierungsfreie Auffindbarkeit sicherstellen.

Dieser Staatsvertrag dient, neben weiteren Regelungen und Förderungsvorhaben in Deutschland, der nachhaltigen Unterstützung neuer europäischer Film- und Fernsehproduktionen.

Den Landesmedienanstalten obliegt es, unter dem Gesichtspunkt der Gleichbehandlung privater Veranstalter und Anbieter und der besseren Durchsetzbarkeit von Entscheidungen verstärkt zusammenzuarbeiten.

### I.
Abschnitt  
Anwendungsbereich, Begriffsbestimmungen

##### § 1  
Anwendungsbereich

(1) Dieser Staatsvertrag gilt für die Veranstaltung und das Angebot, die Verbreitung und die Zugänglichmachung von Rundfunk und Telemedien in Deutschland.

(2) Soweit dieser Staatsvertrag keine anderweitigen Regelungen für die Veranstaltung und Verbreitung von Rundfunk enthält oder solche Regelungen zulässt, sind die für die jeweilige Rundfunkanstalt oder den jeweiligen privaten Veranstalter geltenden landesrechtlichen Vorschriften anzuwenden.

(3) Für Fernsehveranstalter gelten dieser Staatsvertrag und die landesrechtlichen Vorschriften, wenn sie in Deutschland niedergelassen sind.
Ein Fernsehveranstalter gilt als in Deutschland niedergelassen, wenn

1.  die Hauptverwaltung in Deutschland liegt und die redaktionellen Entscheidungen über das Programm dort getroffen werden,
2.  die Hauptverwaltung in Deutschland liegt und die redaktionellen Entscheidungen über das Programm in einem anderen Mitgliedstaat der Europäischen Union getroffen werden, jedoch
    1.  ein wesentlicher Teil des mit der Durchführung programmbezogener Tätigkeiten betrauten Personals in Deutschland tätig ist oder
    2.  ein wesentlicher Teil des mit der Ausübung sendungsbezogener Tätigkeiten betrauten Personals sowohl in Deutschland als auch dem anderen Mitgliedstaat der Europäischen Union tätig ist oder
    3.  ein wesentlicher Teil des mit sendungsbezogenen Tätigkeiten betrauten Personals weder in Deutschland noch dem anderen Mitgliedstaat der Europäischen Union tätig ist, aber der Fernsehveranstalter in Deutschland zuerst seine Tätigkeit begonnen hat und eine dauerhafte und tatsächliche Verbindung mit der Wirtschaft Deutschlands fortbesteht, oder
3.  die Hauptverwaltung in Deutschland liegt und die redaktionellen Entscheidungen über das Programm in einem Drittstaat getroffen werden oder umgekehrt und vorausgesetzt, ein wesentlicher Teil des mit der Durchführung programmbezogener Tätigkeiten betrauten Personals ist in Deutschland tätig.

(4) Für Fernsehveranstalter, sofern sie nicht bereits aufgrund der Niederlassung der Rechtshoheit Deutschlands oder eines anderen Mitgliedstaats der Europäischen Union unterliegen, gelten dieser Staatsvertrag und die landesrechtlichen Vorschriften auch, wenn sie

1.  eine in Deutschland gelegene Satelliten-Bodenstation für die Aufwärtsstrecke nutzen oder
2.  zwar keine in einem Mitgliedstaat der Europäischen Union gelegene Satelliten-Bodenstation für die Aufwärtsstrecke nutzen, aber eine Deutschland zugewiesene Übertragungskapazität eines Satelliten nutzen.
    Liegt keines dieser beiden Kriterien vor, gelten dieser Staatsvertrag und die landesrechtlichen Vorschriften auch für Fernsehveranstalter, wenn sie in Deutschland gemäß den Artikeln 49 bis 55 des Vertrags über die Arbeitsweise der Europäischen Union (ABl. C 202 vom 7.
    Juni 2016, S. 47), niedergelassen sind.

(5) Dieser Staatsvertrag und die landesrechtlichen Vorschriften gelten nicht für Programme von Fernsehveranstaltern, die

1.  ausschließlich zum Empfang in Drittländern bestimmt sind und
2.  nicht unmittelbar oder mittelbar von der Allgemeinheit mit handelsüblichen Verbraucherendgeräten in einem Staat innerhalb des Geltungsbereichs der Richtlinie 2010/13/EU des Europäischen Parlaments und des Rates vom 10. März 2010 zur Koordinierung bestimmter Rechts- und Verwaltungsvorschriften der Mitgliedstaaten über die Bereitstellung audiovisueller Mediendienste (Richtlinie über audiovisuelle Mediendienste) (ABl. L 95 vom 15.4.2010, S.
    1), die durch die Richtlinie (EU) 2018/1808 (ABl.
    L 303 vom 28.11.2018, S.
    69) geändert worden ist, empfangen werden.

(6) Die Bestimmungen des II.
und IV.
Abschnitts gelten für Teleshoppingkanäle nur, sofern dies ausdrücklich bestimmt ist.

(7) Für Anbieter von Telemedien gilt dieser Staatsvertrag, wenn sie nach den Vorschriften des Telemediengesetzes in Deutschland niedergelassen sind.

(8) Abweichend von Absatz 7 gilt dieser Staatsvertrag für Medienintermediäre, Medienplattformen und Benutzeroberflächen, soweit sie zur Nutzung in Deutschland bestimmt sind.
Medienintermediäre, Medienplattformen oder Benutzeroberflächen sind dann als zur Nutzung in Deutschland bestimmt anzusehen, wenn sie sich in der Gesamtschau, insbesondere durch die verwendete Sprache, die angebotenen Inhalte oder Marketingaktivitäten, an Nutzer in Deutschland richten oder in Deutschland einen nicht unwesentlichen Teil ihrer Refinanzierung erzielen.
Für die Zwecke der §§ 97 bis 99 gilt dieser Staatsvertrag für Video-Sharing-Dienste im Anwendungsbereich der Richtlinie 2010/13/EU, wenn sie nach den Vorschriften des Telemediengesetzes in Deutschland niedergelassen sind; im Übrigen gilt Satz 1.

(9) Fernsehveranstalter sind verpflichtet, die nach Landesrecht zuständige Stelle über alle Änderungen zu informieren, die die Feststellung der Rechtshoheit nach den Absätzen 3 und 4 berühren könnten.
Die Landesmedienanstalten erstellen eine Liste der der Rechtshoheit Deutschlands unterworfenen privaten Fernsehveranstalter, halten die Liste auf dem neuesten Stand und geben an, auf welchen der in den Absätzen 3 und 4 genannten Kriterien die Rechtshoheit beruht.
Die Liste und alle Aktualisierungen dieser Liste werden der Europäischen Kommission mitsamt der Liste der öffentlich-rechtlichen Fernsehveranstalter übermittelt.

##### § 2  
Begriffsbestimmungen

(1) Rundfunk ist ein linearer Informations- und Kommunikationsdienst; er ist die für die Allgemeinheit und zum zeitgleichen Empfang bestimmte Veranstaltung und Verbreitung von journalistisch-redaktionell gestalteten Angeboten in Bewegtbild oder Ton entlang eines Sendeplans mittels Telekommunikation.
Der Begriff schließt Angebote ein, die verschlüsselt verbreitet werden oder gegen besonderes Entgelt empfangbar sind.
Telemedien sind alle elektronischen Informations- und Kommunikationsdienste, soweit sie nicht Telekommunikationsdienste nach § 3 Nr. 24 des Telekommunikationsgesetzes sind, die ganz in der Übertragung von Signalen über Telekommunikationsnetze bestehen, oder telekommunikationsgestützte Dienste nach § 3 Nr. 25 des Telekommunikationsgesetzes oder Rundfunk nach Satz 1 und 2 sind.

(2) Im Sinne dieses Staatsvertrages ist

1.  Rundfunkprogramm eine nach einem Sendeplan zeitlich geordnete Folge von Inhalten,
2.  Sendeplan die auf Dauer angelegte, vom Veranstalter bestimmte und vom Nutzer nicht veränderbare Festlegung der inhaltlichen und zeitlichen Abfolge von Sendungen,
3.  Sendung ein unabhängig von seiner Länge inhaltlich zusammenhängender, geschlossener, zeitlich begrenzter Einzelbestandteil eines Sendeplans oder Katalogs,
4.  Vollprogramm ein Rundfunkprogramm mit vielfältigen Inhalten, in welchem Information, Bildung, Beratung und Unterhaltung einen wesentlichen Teil des Gesamtprogramms bilden,
5.  Spartenprogramm ein Rundfunkprogramm mit im wesentlichen gleichartigen Inhalten,
6.  Regionalfensterprogramm ein zeitlich und räumlich begrenztes Rundfunkprogramm mit im wesentlichen regionalen Inhalten im Rahmen eines Hauptprogramms,
7.  Werbung jede Äußerung, die der unmittelbaren oder mittelbaren Förderung des Absatzes von Waren und Dienstleistungen, einschließlich unbeweglicher Sachen, Rechte und Verpflichtungen, oder des Erscheinungsbilds natürlicher oder juristischer Personen, die einer wirtschaftlichen Tätigkeit nachgehen, dient und gegen Entgelt oder eine ähnliche Gegenleistung oder als Eigenwerbung im Rundfunk oder in einem Telemedium aufgenommen ist.
    Werbung ist insbesondere Rundfunkwerbung, Sponsoring, Teleshopping und Produktplatzierung; § 8 Abs. 9 und § 22 Abs.
    1 Satz 3 bleiben unberührt,
8.  Rundfunkwerbung jede Äußerung bei der Ausübung eines Handels, Gewerbes, Handwerks oder freien Berufs, die im Rundfunk von einem öffentlich-rechtlichen oder einem privaten Veranstalter oder einer natürlichen Person entweder gegen Entgelt oder eine ähnliche Gegenleistung oder als Eigenwerbung gesendet wird, mit dem Ziel, den Absatz von Waren oder die Erbringung von Dienstleistungen, einschließlich unbeweglicher Sachen, Rechte und Verpflichtungen, gegen Entgelt zu fördern,
9.  Schleichwerbung die Erwähnung oder Darstellung von Waren, Dienstleistungen, Namen, Marken oder Tätigkeiten eines Herstellers von Waren oder eines Erbringers von Dienstleistungen in Sendungen, wenn sie vom Veranstalter absichtlich zu Werbezwecken vorgesehen ist und mangels Kennzeichnung die Allgemeinheit hinsichtlich des eigentlichen Zweckes dieser Erwähnung oder Darstellung irreführen kann.
    Eine Erwähnung oder Darstellung gilt insbesondere dann als zu Werbezwecken beabsichtigt, wenn sie gegen Entgelt oder eine ähnliche Gegenleistung erfolgt,
10.  Sponsoring jeder Beitrag einer natürlichen oder juristischen Person oder einer Personenvereinigung, die an Rundfunktätigkeiten, der Bereitstellung von rundfunkähnlichen Telemedien oder Video-Sharing-Diensten oder an der Produktion audiovisueller Werke nicht beteiligt ist, zur direkten oder indirekten Finanzierung von Rundfunkprogrammen, rundfunkähnlichen Telemedien, Video-Sharing-Diensten, nutzergenerierten Videos oder einer Sendung, um den Namen, die Marke, das Erscheinungsbild der Person oder Personenvereinigung, ihre Tätigkeit oder ihre Leistungen zu fördern,
11.  Teleshopping die Sendung direkter Angebote an die Öffentlichkeit für den Absatz von Waren oder die Erbringung von Dienstleistungen, einschließlich unbeweglicher Sachen, Rechte und Verpflichtungen, gegen Entgelt in Form von Teleshoppingkanälen, -fenstern und -spots,
12.  Produktplatzierung jede Form der Werbung, die darin besteht, gegen Entgelt oder eine ähnliche Gegenleistung ein Produkt, eine Dienstleistung oder die entsprechende Marke einzubeziehen oder darauf Bezug zu nehmen, sodass diese innerhalb einer Sendung oder eines nutzergenerierten Videos erscheinen.
    Die kostenlose Bereitstellung von Waren oder Dienstleistungen ist Produktplatzierung, sofern die betreffende Ware oder Dienstleistung von bedeutendem Wert ist,
13.  rundfunkähnliches Telemedium ein Telemedium mit Inhalten, die nach Form und Gestaltung hörfunk- oder fernsehähnlich sind und die aus einem von einem Anbieter festgelegten Katalog zum individuellen Abruf zu einem vom Nutzer gewählten Zeitpunkt bereitgestellt werden (Audio- und audiovisuelle Mediendienste auf Abruf); Inhalte sind insbesondere Hörspiele, Spielfilme, Serien, Reportagen, Dokumentationen, Unterhaltungs-, Informations- oder Kindersendungen,
14.  Medienplattform jedes Telemedium, soweit es Rundfunk, rundfunkähnliche Telemedien oder Telemedien nach § 19 Abs.
    1 zu einem vom Anbieter bestimmten Gesamtangebot zusammenfasst.
    Die Zusammenfassung von Rundfunk, rundfunkähnlichen Telemedien oder Telemedien nach § 19 Abs.
    1 ist auch die Zusammenfassung von softwarebasierten Anwendungen, welche im Wesentlichen der unmittelbaren Ansteuerung von Rundfunk, rundfunkähnlichen Telemedien, Telemedien nach § 19 Abs.
    1 oder Telemedien im Sinne des Satz 1 dienen.
    Keine Medienplattformen in diesem Sinne sind
    1.  Angebote, die analog über eine Kabelanlage verbreitet werden,
    2.  das Gesamtangebot von Rundfunk, rundfunkähnlichen Telemedien oder Telemedien nach § 19 Abs. 1, welches ausschließlich in der inhaltlichen Verantwortung einer oder mehrerer öffentlich-rechtlicher Rundfunkanstalten oder eines privaten Anbieters von Rundfunk, rundfunkähnlichen Telemedien oder Telemedien nach § 19 Abs.
        1 oder von Unternehmen, deren Programme ihm nach § 62 zuzurechnen sind, stehen; Inhalte aus nach § 59 Abs.
        4 aufgenommenen Fensterprogrammen oder Drittsendezeiten im Sinne des § 65 sind unschädlich,
15.  Benutzeroberfläche die textlich, bildlich oder akustisch vermittelte Übersicht über Angebote oder Inhalte einzelner oder mehrerer Medienplattformen, die der Orientierung dient und unmittelbar die Auswahl von Angeboten, Inhalten oder softwarebasierten Anwendungen, welche im Wesentlichen der unmittelbaren Ansteuerung von Rundfunk, rundfunkähnlichen Telemedien oder Telemedien nach § 19 Abs.
    1 dienen, ermöglicht.
    Benutzeroberflächen sind insbesondere
    1.  Angebots- oder Programmübersichten einer Medienplattform,
    2.  Angebots- oder Programmübersichten, die nicht zugleich Teil einer Medienplattform sind,
    3.  visuelle oder akustische Präsentationen auch gerätegebundener Medienplattformen, sofern sie die Funktion nach Satz 1 erfüllen,
16.  Medienintermediär jedes Telemedium, das auch journalistisch-redaktionelle Angebote Dritter aggregiert, selektiert und allgemein zugänglich präsentiert, ohne diese zu einem Gesamtangebot zusammenzufassen,
17.  Rundfunkveranstalter, wer ein Rundfunkprogramm unter eigener inhaltlicher Verantwortung anbietet,
18.  Anbieter rundfunkähnlicher Telemedien, wer über die Auswahl der Inhalte entscheidet und die inhaltliche Verantwortung trägt,
19.  Anbieter einer Medienplattform, wer die Verantwortung für die Auswahl der Angebote einer Medienplattform trägt,
20.  Anbieter einer Benutzeroberfläche, wer über die Gestaltung der Übersicht abschließend entscheidet,
21.  Anbieter eines Medienintermediärs, wer die Verantwortung für die Aggregation, Selektion und allgemein zugängliche Präsentation von Inhalten trägt,
22.  Video-Sharing-Dienst ein Telemedium, bei dem der Hauptzweck des Dienstes oder eines trennbaren Teils des Dienstes oder eine wesentliche Funktion des Dienstes darin besteht, Sendungen mit bewegten Bildern oder nutzergenerierte Videos, für die der Diensteanbieter keine redaktionelle Verantwortung trägt, der Allgemeinheit bereitzustellen, wobei der Diensteanbieter die Organisation der Sendungen oder der nutzergenerierten Videos, auch mit automatischen Mitteln oder Algorithmen, bestimmt,
23.  Video-Sharing-Diensteanbieter, wer einen Video-Sharing-Dienst betreibt,
24.  nutzergeneriertes Video eine von einem Nutzer erstellte Abfolge von bewegten Bildern mit oder ohne Ton, die unabhängig von ihrer Länge einen Einzelbestandteil darstellt und die von diesem oder einem anderen Nutzer auf einen Video-Sharing-Dienst hochgeladen wird,
25.  unter Information insbesondere Folgendes zu verstehen: Nachrichten und Zeitgeschehen, politische Information, Wirtschaft, Auslandsberichte, Religiöses, Sport, Regionales, Gesellschaftliches, Service und Zeitgeschichtliches,
26.  unter Bildung insbesondere Folgendes zu verstehen: Wissenschaft und Technik, Alltag und Ratgeber, Theologie und Ethik, Tiere und Natur, Gesellschaft, Kinder und Jugend, Erziehung, Geschichte und andere Länder,
27.  unter Kultur insbesondere Folgendes zu verstehen: Bühnenstücke, Musik, Fernsehspiele, Fernsehfilme und Hörspiele, bildende Kunst, Architektur, Philosophie und Religion, Literatur und Kino,
28.  unter Unterhaltung insbesondere Folgendes zu verstehen: Kabarett und Comedy, Filme, Serien, Shows, Talk-Shows, Spiele, Musik,
29.  unter öffentlich-rechtlichen Telemedienangeboten zu verstehen: von den in der Arbeitsgemeinschaft der öffentlich-rechtlichen Rundfunkanstalten der Bundesrepublik Deutschland (ARD) zusammengeschlossenen Landesrundfunkanstalten, dem Zweiten Deutschen Fernsehen (ZDF) und dem Deutschlandradio jeweils nach Maßgabe eines nach § 32 Abs. 4 durchgeführten Verfahrens angebotene Telemedien, die journalistisch-redaktionell veranlasst und journalistisch-redaktionell gestaltet sind, Bild, Ton, Bewegtbild, Text und internetspezifische Gestaltungsmittel enthalten können und diese miteinander verbinden.

(3) Kein Rundfunk sind Angebote, die aus Sendungen bestehen, die jeweils gegen Einzelentgelt freigeschaltet werden.

### II.
Abschnitt  
Allgemeine Bestimmungen

#### 1.
Unterabschnitt  
Rundfunk

##### § 3  
Allgemeine Grundsätze

Die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF, das Deutschlandradio und alle Veranstalter bundesweit ausgerichteter privater Rundfunkprogramme haben in ihren Angeboten die Würde des Menschen zu achten und zu schützen; die sittlichen und religiösen Überzeugungen der Bevölkerung sind zu achten.
Die Angebote sollen dazu beitragen, die Achtung vor Leben, Freiheit und körperlicher Unversehrtheit, vor Glauben und Meinungen anderer zu stärken.
Weitergehende landesrechtliche Anforderungen an die Gestaltung der Angebote sowie § 51 bleiben unberührt.

##### § 4  
Informationspflichten, Verbraucherschutz

(1) Rundfunkveranstalter haben folgende Informationen im Rahmen ihres Gesamtangebots leicht, unmittelbar und ständig zugänglich zu machen:

1.  Name und geografische Anschrift,
2.  Angaben, die eine schnelle und unmittelbare Kontaktaufnahme und eine effiziente Kommunikation ermöglichen, einschließlich ihrer E-Mail-Adresse oder ihrer Webseite,
3.  die zuständige Aufsicht und
4.  den Mitgliedstaat, deren Rechtshoheit sie unterworfen sind.

(2) Mit Ausnahme seiner §§ 2, 9 und 12 gelten die Regelungen des EG\-Verbraucherschutzdurchsetzungsgesetzes hinsichtlich der Bestimmungen dieses Staatsvertrages zur Umsetzung der Artikel 9, 10, 11 und Artikel 19 bis 26 der Richtlinie 2010/13/EU des Europäischen Parlaments und des Rates vom 10.
März 2010 zur Koordinierung bestimmter Rechts- und Verwaltungsvorschriften der Mitgliedstaaten über die Bereitstellung audiovisueller Mediendienste (Richtlinie über audiovisuelle Mediendienste) (ABl.
L 95 vom 15.4.2010, S.
1), bei innergemeinschaftlichen Verstößen entsprechend.
Satz 1 gilt auch für Teleshoppingkanäle.

##### § 5  
Auskunftsrechte

(1) Rundfunkveranstalter haben gegenüber Behörden ein Recht auf Auskunft.
Auskünfte können verweigert werden, soweit

1.  durch die Auskunftserteilung die sachgemäße Durchführung eines schwebenden Verfahrens vereitelt, erschwert, verzögert oder gefährdet werden könnte oder
2.  Vorschriften über die Geheimhaltung entgegenstehen oder
3.  ein überwiegendes öffentliches oder schutzwürdiges privates Interesse verletzt würde oder
4.  ihr Umfang das zumutbare Maß überschreitet.

(2) Allgemeine Anordnungen, die einer Behörde Auskünfte an Rundfunkveranstalter verbieten, sind unzulässig.

(3) Rundfunkveranstalter können von Behörden verlangen, dass sie bei der Weitergabe von amtlichen Bekanntmachungen im Verhältnis zu anderen Bewerbern gleichbehandelt werden.

##### § 6  
Sorgfaltspflichten

(1) Berichterstattung und Informationssendungen haben den anerkannten journalistischen Grundsätzen, auch beim Einsatz virtueller Elemente, zu entsprechen.
Sie müssen unabhängig und sachlich sein.
Nachrichten sind vor ihrer Verbreitung mit der nach den Umständen gebotenen Sorgfalt auf Wahrheit und Herkunft zu prüfen.
Kommentare sind von der Berichterstattung deutlich zu trennen und unter Nennung des Verfassers als solche zu kennzeichnen.

(2) Bei der Wiedergabe von Meinungsumfragen, die von Rundfunkveranstaltern durchgeführt werden, ist ausdrücklich anzugeben, ob sie repräsentativ sind.

##### § 7  
Barrierefreiheit

(1) Die Veranstalter nach § 3 Satz 1 sollen über ihr bereits bestehendes Engagement hinaus im Rahmen der technischen und ihrer finanziellen Möglichkeiten barrierefreie Angebote aufnehmen und den Umfang solcher Angebote stetig und schrittweise ausweiten.

(2) Die Veranstalter bundesweit ausgerichteter privater Fernsehprogramme erstatten der jeweils zuständigen Landesmedienanstalt, die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF sowie das Deutschlandradio ihren jeweiligen Aufsichtsgremien mindestens alle drei Jahre Bericht über die getroffenen Maßnahmen nach Absatz 1.
Die Berichte werden anschließend der Europäischen Kommission übermittelt.

##### § 8  
Werbegrundsätze, Kennzeichnungspflichten

(1) Werbung darf nicht

1.  die Menschenwürde verletzen,
2.  Diskriminierungen aufgrund von Geschlecht, Rasse oder ethnischer Herkunft, Staatsangehörigkeit, Religion oder Glauben, Behinderung, Alter oder sexueller Orientierung beinhalten oder fördern,
3.  irreführen oder den Interessen der Verbraucher schaden oder
4.  Verhaltensweisen fördern, die die Gesundheit oder Sicherheit sowie in hohem Maße den Schutz der Umwelt gefährden.

(2) Rundfunkwerbung ist Teil des Programms.
Rundfunkwerbung oder Werbetreibende dürfen das übrige Programm inhaltlich und redaktionell nicht beeinflussen.
Die Sätze 1 und 2 gelten für Teleshopping-Spots, Teleshopping-Fenster und deren Anbieter entsprechend.

(3) Werbung muss als solche leicht erkennbar und vom redaktionellen Inhalt unterscheidbar sein.
In der Werbung dürfen keine Techniken der unterschwelligen Beeinflussung eingesetzt werden.
Auch bei Einsatz neuer Werbetechniken müssen Rundfunkwerbung und Teleshopping dem Medium angemessen durch optische oder akustische Mittel oder räumlich eindeutig von anderen Sendungsteilen abgesetzt sein.

(4) Eine Teilbelegung des ausgestrahlten Bildes mit Rundfunkwerbung ist zulässig, wenn die Rundfunkwerbung vom übrigen Programm eindeutig optisch getrennt und als solche gekennzeichnet ist.
Diese Rundfunkwerbung wird auf die Dauer der Spotwerbung nach den §§ 39 und 70 angerechnet.
§ 9 Abs. 1 gilt entsprechend.

(5) Dauerwerbesendungen sind zulässig, wenn der Werbecharakter erkennbar im Vordergrund steht und die Werbung einen wesentlichen Bestandteil der Sendung darstellt.
Sie müssen zu Beginn als Dauerwerbesendung angekündigt und während ihres gesamten Verlaufs als solche gekennzeichnet werden.

(6) Die Einfügung virtueller Werbung in Sendungen ist zulässig, wenn

1.  am Anfang und am Ende der betreffenden Sendung darauf hingewiesen wird und
2.  durch sie eine am Ort der Übertragung ohnehin bestehende Werbung ersetzt wird.

Andere Rechte bleiben unberührt.

(7) Schleichwerbung und Themenplatzierung sowie entsprechende Praktiken sind unzulässig.
Produktplatzierung ist gestattet, außer in Nachrichtensendungen und Sendungen zur politischen Information, Verbrauchersendungen, Regionalfensterprogrammen nach § 59 Abs. 4, Fensterprogrammen nach § 65, Sendungen religiösen Inhalts und Kindersendungen.
Sendungen, die Produktplatzierung enthalten, müssen folgende Voraussetzungen erfüllen:

1.  die redaktionelle Verantwortung und Unabhängigkeit hinsichtlich Inhalt und Platzierung im Sendeplan müssen unbeeinträchtigt bleiben,
2.  die Produktplatzierung darf nicht unmittelbar zu Kauf, Miete oder Pacht von Waren oder Dienstleistungen anregen, insbesondere nicht durch spezielle verkaufsfördernde Hinweise auf diese Waren oder Dienstleistungen, und
3.  das Produkt darf nicht zu stark herausgestellt werden; dies gilt auch für kostenlos zur Verfügung gestellte geringwertige Güter.

Auf eine Produktplatzierung ist eindeutig hinzuweisen.
Sie ist zu Beginn und zum Ende einer Sendung sowie bei deren Fortsetzung nach einer Werbeunterbrechung oder im Hörfunk durch einen gleichwertigen Hinweis angemessen zu kennzeichnen.
Die Kennzeichnungspflicht entfällt für Sendungen, die nicht vom Veranstalter selbst oder von einem mit dem Veranstalter verbundenen Unternehmen produziert oder in Auftrag gegeben worden sind, wenn nicht mit zumutbarem Aufwand ermittelbar ist, ob Produktplatzierung enthalten ist; hierauf ist hinzuweisen.
Die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und die Landesmedienanstalten legen eine einheitliche Kennzeichnung fest.

(8) In der Fernsehwerbung und beim Teleshopping im Fernsehen dürfen keine Personen auftreten, die regelmäßig Nachrichtensendungen oder Sendungen zum politischen Zeitgeschehen vorstellen.

(9) Werbung politischer, weltanschaulicher oder religiöser Art ist unzulässig.
Unentgeltliche Beiträge im Dienst der Öffentlichkeit einschließlich von Spendenaufrufen zu Wohlfahrtszwecken gelten nicht als Werbung im Sinne von Satz 1.
§ 68 bleibt unberührt.

(10) Werbung für alkoholische Getränke darf den übermäßigen Genuss solcher Getränke nicht fördern.

(11) Die nichtbundesweite Verbreitung von Rundfunkwerbung oder anderen Inhalten in einem bundesweit ausgerichteten oder zur bundesweiten Verbreitung beauftragten oder zugelassenen Programm ist nur zulässig, wenn und soweit das Recht des Landes, in dem die nichtbundesweite Verbreitung erfolgt, dies gestattet.
Die nichtbundesweit verbreitete Rundfunkwerbung oder andere Inhalte privater Veranstalter bedürfen einer gesonderten landesrechtlichen Zulassung; diese kann von gesetzlich zu bestimmenden inhaltlichen Voraussetzungen abhängig gemacht werden.

(12) Die Absätze 1 bis 11 gelten auch für Teleshoppingkanäle.

##### § 9  
Einfügung von Rundfunkwerbung und Teleshopping

(1) Übertragungen von Gottesdiensten sowie Sendungen für Kinder dürfen nicht durch Rundfunkwerbung oder Teleshopping unterbrochen werden.

(2) Einzeln gesendete Werbe- und Teleshopping-Spots im Fernsehen müssen die Ausnahme bleiben; dies gilt nicht bei der Übertragung von Sportveranstaltungen.
Die Einfügung von Werbe- oder Teleshopping-Spots im Fernsehen darf den Zusammenhang von Sendungen unter Berücksichtigung der natürlichen Sendeunterbrechungen sowie der Dauer und der Art der Sendung nicht beeinträchtigen noch die Rechte von Rechteinhabern verletzen.

(3) Filme mit Ausnahme von Serien, Reihen und Dokumentarfilmen sowie Kinofilme und Nachrichtensendungen dürfen für jeden programmierten Zeitraum von mindestens 30 Minuten einmal für Fernsehwerbung oder Teleshopping unterbrochen werden.

(4) Richten sich Rundfunkwerbung oder Teleshopping in einem Fernsehprogramm eigens und häufig an Zuschauer eines anderen Staates, der das Europäische Übereinkommen über das grenzüberschreitende Fernsehen ratifiziert hat und nicht Mitglied der Europäischen Union ist, dürfen die für die Fernsehwerbung oder das Teleshopping dort geltenden Vorschriften nicht umgangen werden.
Satz 1 gilt nicht, wenn die Vorschriften dieses Staatsvertrages über die Rundfunkwerbung oder das Teleshopping strenger sind als jene Vorschriften, die in dem betreffenden Staat gelten, ferner nicht, wenn mit dem betroffenen Staat Übereinkünfte auf diesem Gebiet geschlossen wurden.

##### § 10  
Sponsoring

(1) Auf das Bestehen einer Sponsoring-Vereinbarung muss eindeutig hingewiesen werden; bei Sendungen, die ganz oder teilweise gesponsert werden, muss zu Beginn oder am Ende auf die Finanzierung durch den Sponsor in vertretbarer Kürze und in angemessener Weise deutlich hingewiesen werden; der Hinweis ist in diesem Rahmen auch durch Bewegtbild möglich.
Neben oder anstelle des Namens des Sponsors kann auch dessen Firmenemblem oder eine Marke, ein anderes Symbol des Sponsors, ein Hinweis auf seine Produkte oder Dienstleistungen oder ein entsprechendes unterscheidungskräftiges Zeichen eingeblendet werden.

(2) Der Inhalt eines gesponserten Rundfunkprogramms oder einer gesponserten Sendung und der Programmplatz einer Sendung dürfen vom Sponsor nicht in der Weise beeinflusst werden, dass die redaktionelle Verantwortung und Unabhängigkeit des Rundfunkveranstalters beeinträchtigt werden.

(3) Gesponserte Sendungen dürfen nicht zum Verkauf, zum Kauf oder zur Miete oder Pacht von Erzeugnissen oder Dienstleistungen des Sponsors oder eines Dritten, vor allem durch entsprechende besondere Hinweise, anregen.

(4) Nachrichtensendungen und Sendungen zur politischen Information dürfen nicht gesponsert werden.
In Kindersendungen und Sendungen religiösen Inhalts ist das Zeigen von Sponsorenlogos untersagt.

(5) Die Absätze 1 bis 4 gelten auch für Teleshoppingkanäle.

(6) § 8 Abs.
3 Satz 3 und Abs.
8 bis 10 gilt entsprechend.

##### § 11  
Gewinnspiele

(1) Gewinnspielsendungen und Gewinnspiele sind zulässig.
Sie unterliegen dem Gebot der Transparenz und des Teilnehmerschutzes.
Sie dürfen nicht irreführen und den Interessen der Teilnehmer nicht schaden.
Insbesondere ist im Programm über die Kosten der Teilnahme, die Teilnahmeberechtigung, die Spielgestaltung sowie über die Auflösung der gestellten Aufgabe zu informieren.
Die Belange des Jugendschutzes sind zu wahren.
Für die Teilnahme darf nur ein Entgelt bis zu 0,50 Euro verlangt werden; § 35 Satz 3 bleibt unberührt.

(2) Der Veranstalter hat der für die Aufsicht zuständigen Stelle auf Verlangen alle Unterlagen vorzulegen und Auskünfte zu erteilen, die zur Überprüfung der ordnungsgemäßen Durchführung der Gewinnspielsendungen und Gewinnspiele erforderlich sind.

(3) Die Absätze 1 und 2 gelten auch für Teleshoppingkanäle.

##### § 12  
Datenverarbeitung zu journalistischen Zwecken, Medienprivileg

(1) Soweit die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF, das Deutschlandradio oder private Rundfunkveranstalter personenbezogene Daten zu journalistischen Zwecken verarbeiten, ist es den hiermit befassten Personen untersagt, diese personenbezogenen Daten zu anderen Zwecken zu verarbeiten (Datengeheimnis).
Diese Personen sind bei der Aufnahme ihrer Tätigkeit auf das Datengeheimnis zu verpflichten.
Das Datengeheimnis besteht auch nach Beendigung ihrer Tätigkeit fort.
Im Übrigen finden für die Datenverarbeitung zu journalistischen Zwecken von der Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates vom 27. April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr und zur Aufhebung der Richtlinie 95/46/EG (Datenschutz-Grundverordnung) (ABl.
L 119 vom 4.5.2016, S.
1; L 314 vom 22.11.2016, S.
72; L 127 vom 23.5.2018, S.
2) außer den Kapiteln I, VIII, X und XI nur die Artikel 5 Abs.
1 Buchst. f in Verbindung mit Abs.
2, Artikel 24 und Artikel 32 Anwendung.
Artikel 82 und 83 der Verordnung (EU) 2016/679 gelten mit der Maßgabe, dass nur für eine Verletzung des Datengeheimnisses gemäß den Sätzen 1 bis 3 sowie für unzureichende Maßnahmen nach Artikel 5 Abs.
1 Buchst.
f, Artikel 24 und 32 der Verordnung (EU) 2016/679 gehaftet wird.
Die Sätze 1 bis 5 gelten entsprechend für die zu den in Satz 1 genannten Stellen gehörenden Hilfs- und Beteiligungsunternehmen.
Die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF, das Deutschlandradio und andere Rundfunkveranstalter sowie ihre Verbände und Vereinigungen können sich Verhaltenskodizes geben, die in einem transparenten Verfahren erlassen und veröffentlicht werden.
Den betroffenen Personen stehen nur die in den Absätzen 2 und 3 genannten Rechte zu.

(2) Führt die journalistische Verarbeitung personenbezogener Daten zur Verbreitung von Gegendarstellungen der betroffenen Person oder zu Verpflichtungserklärungen, Beschlüssen oder Urteilen über die Unterlassung der Verbreitung oder über den Widerruf des Inhalts der Daten, so sind diese Gegendarstellungen, Verpflichtungserklärungen und Widerrufe zu den gespeicherten Daten zu nehmen und dort für dieselbe Zeitdauer aufzubewahren wie die Daten selbst sowie bei einer Übermittlung der Daten gemeinsam mit diesen zu übermitteln.

(3) Wird jemand durch eine Berichterstattung in seinem Persönlichkeitsrecht beeinträchtigt, kann die betroffene Person Auskunft über die der Berichterstattung zugrunde liegenden, zu ihrer Person gespeicherten Daten verlangen.
Die Auskunft kann nach Abwägung der schutzwürdigen Interessen der Beteiligten verweigert werden, soweit

1.  aus den Daten auf Personen, die bei der Vorbereitung, Herstellung oder Verbreitung von Rundfunksendungen mitwirken oder mitgewirkt haben, geschlossen werden kann,
2.  aus den Daten auf die Person des Einsenders oder des Gewährsträgers von Beiträgen, Unterlagen und Mitteilungen für den redaktionellen Teil geschlossen werden kann oder
3.  durch die Mitteilung der recherchierten oder sonst erlangten Daten die journalistische Aufgabe durch Ausforschung des Informationsbestandes beeinträchtigt würde.

Die betroffene Person kann die unverzügliche Berichtigung unrichtiger personenbezogener Daten im Datensatz oder die Hinzufügung einer eigenen Darstellung von angemessenem Umfang verlangen.
Die weitere Speicherung der personenbezogenen Daten ist rechtmäßig, wenn dies für die Ausübung des Rechts auf freie Meinungsäußerung und Information oder zur Wahrnehmung berechtigter Interessen erforderlich ist.

(4) Für die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF, das Deutschlandradio und private Rundfunkveranstalter sowie zu diesen gehörende Beteiligungs- und Hilfsunternehmen wird die Aufsicht über die Einhaltung der geltenden datenschutzrechtlichen Bestimmungen durch Landesrecht bestimmt.
Regelungen dieses Staatsvertrages bleiben unberührt.

(5) Die Absätze 1 bis 4 gelten auch für Teleshoppingkanäle.

##### § 13  
Übertragung von Großereignissen

(1) Die Ausstrahlung im Fernsehen von Ereignissen von erheblicher gesellschaftlicher Bedeutung (Großereignisse) in Deutschland verschlüsselt und gegen besonderes Entgelt ist nur zulässig, wenn der Fernsehveranstalter selbst oder ein Dritter zu angemessenen Bedingungen ermöglicht, dass das Ereignis zumindest in einem frei empfangbaren und allgemein zugänglichen Fernsehprogramm in Deutschland zeitgleich oder, sofern wegen parallel laufender Einzelereignisse nicht möglich, geringfügig zeitversetzt ausgestrahlt werden kann.
Besteht keine Einigkeit über die Angemessenheit der Bedingungen, sollen die Parteien rechtzeitig vor dem Ereignis ein schiedsrichterliches Verfahren nach den §§ 1025 ff.
der Zivilprozessordnung vereinbaren; kommt die Vereinbarung eines schiedsrichterlichen Verfahrens aus Gründen, die der Fernsehveranstalter oder der Dritte zu vertreten haben, nicht zustande, gilt die Übertragung nach Satz 1 als nicht zu angemessenen Bedingungen ermöglicht.
Als allgemein zugängliches Fernsehprogramm gilt nur ein Programm, das in mehr als zwei Drittel der Haushalte tatsächlich empfangbar ist.

(2) Großereignisse im Sinne dieser Bestimmung sind:

1.  Olympische Sommer- und Winterspiele,
2.  bei Fußball-Europa- und -Weltmeisterschaften alle Spiele mit deutscher Beteiligung sowie unabhängig von einer deutschen Beteiligung das Eröffnungsspiel, die Halbfinalspiele und das Endspiel,
3.  die Halbfinalspiele und das Endspiel um den Vereinspokal des Deutschen Fußball-Bundes,
4.  Heim- und Auswärtsspiele der deutschen Fußballnationalmannschaft,
5.  Endspiele der europäischen Vereinsmeisterschaften im Fußball (Champions League, Europa League) bei deutscher Beteiligung.

Bei Großereignissen, die aus mehreren Einzelereignissen bestehen, gilt jedes Einzelereignis als Großereignis.
Die Aufnahme oder Herausnahme von Ereignissen in diese Bestimmung ist nur durch Staatsvertrag aller Länder zulässig.

(3) Teilt ein Mitgliedstaat der Europäischen Union seine Bestimmungen über die Ausstrahlung von Großereignissen nach Artikel 14 Abs.
2 der Richtlinie 2010/13/EU der Europäischen Kommission mit und erhebt die Kommission nicht binnen drei Monaten seit der Mitteilung Einwände und werden die Bestimmungen des betreffenden Mitgliedstaates im Amtsblatt der Europäischen Union veröffentlicht, ist die Ausstrahlung von Großereignissen verschlüsselt und gegen Entgelt für diesen Mitgliedstaat nur zulässig, wenn der Fernsehveranstalter nach den im Amtsblatt veröffentlichten Bestimmungen des betreffenden Mitgliedstaates eine Übertragung in einem frei zugänglichen Programm ermöglicht.

(4) Sind Bestimmungen eines Staates, der das Europäische Übereinkommen über das grenzüberschreitende Fernsehen in der Fassung des Änderungsprotokolls vom 9. September 1998 ratifiziert hat, nach dem Verfahren nach Artikel 9a Abs. 3 des Übereinkommens veröffentlicht, gilt diese Regelung für Veranstalter in Deutschland nach Maßgabe des Satzes 4, es sei denn, die Regierungschefinnen und Regierungschefs der Länder versagen der Regelung innerhalb einer Frist von sechs Monaten durch einstimmigen Beschluss die Anerkennung.
Die Anerkennung kann nur versagt werden, wenn die Bestimmungen des betreffenden Staates gegen das Grundgesetz oder die Europäische Konvention zum Schutze der Menschenrechte und Grundfreiheiten verstoßen.
Die für Veranstalter in Deutschland nach dem vorbezeichneten Verfahren geltenden Bestimmungen sind in den amtlichen Veröffentlichungsblättern der Länder bekannt zu machen.
Mit dem Tag der letzten Bekanntmachung in den Veröffentlichungsblättern der Länder ist die Ausstrahlung von Großereignissen verschlüsselt und gegen Entgelt für diesen betreffenden Staat nur zulässig, wenn der Fernsehveranstalter nach den veröffentlichten Bestimmungen des betreffenden Staates eine Übertragung dort in einem frei zugänglichen Programm ermöglicht.

(5) Verstößt ein Veranstalter gegen die Bestimmungen der Absätze 3 und 4, kann die Zulassung widerrufen werden.
Statt des Widerrufs kann die Zulassung mit Nebenbestimmungen versehen werden, soweit dies ausreicht, den Verstoß zu beseitigen.

##### § 14  
Kurzberichterstattung

(1) Das Recht auf unentgeltliche Kurzberichterstattung über Veranstaltungen und Ereignisse, die öffentlich zugänglich und von allgemeinem Informationsinteresse sind, steht jedem in Europa zugelassenen Fernsehveranstalter zu eigenen Sendezwecken zu.
Dieses Recht schließt die Befugnis zum Zugang, zur kurzzeitigen Direktübertragung, zur Aufzeichnung, zu deren Auswertung zu einem einzigen Beitrag und zur Weitergabe unter den Voraussetzungen der Absätze 2 bis 12 ein.

(2) Anderweitige gesetzliche Bestimmungen, insbesondere solche des Urheberrechts und des Persönlichkeitsschutzes, bleiben unberührt.

(3) Auf die Kirchen und auf andere Religionsgemeinschaften sowie deren Einrichtungen mit entsprechender Aufgabenstellung findet Absatz 1 keine Anwendung.

(4) Die unentgeltliche Kurzberichterstattung ist auf eine dem Anlass entsprechende nachrichtenmäßige Kurzberichterstattung beschränkt.
Die zulässige Dauer bemisst sich nach der Länge der Zeit, die notwendig ist, um den nachrichtenmäßigen Informationsgehalt der Veranstaltung oder des Ereignisses zu vermitteln.
Bei kurzfristig und regelmäßig wiederkehrenden Veranstaltungen vergleichbarer Art beträgt die Obergrenze der Dauer in der Regel eineinhalb Minuten.
Werden Kurzberichte über Veranstaltungen vergleichbarer Art zusammengefasst, muss auch in dieser Zusammenfassung der nachrichtenmäßige Charakter gewahrt bleiben.

(5) Das Recht auf Kurzberichterstattung muss so ausgeübt werden, dass vermeidbare Störungen der Veranstaltung oder des Ereignisses unterbleiben.
Der Veranstalter kann die Übertragung oder die Aufzeichnung einschränken oder ausschließen, wenn anzunehmen ist, dass sonst die Durchführung der Veranstaltung infrage gestellt oder das sittliche Empfinden der Veranstaltungsteilnehmer gröblich verletzt würde.
Das Recht auf Kurzberichterstattung ist ausgeschlossen, wenn Gründe der öffentlichen Sicherheit und Ordnung entgegenstehen und diese das öffentliche Interesse an der Information überwiegen.
Unberührt bleibt im Übrigen das Recht des Veranstalters, die Übertragung oder die Aufzeichnung der Veranstaltung insgesamt auszuschließen.

(6) Für die Ausübung des Rechts auf Kurzberichterstattung kann der Veranstalter das allgemein vorgesehene Eintrittsgeld verlangen; im Übrigen ist ihm Ersatz seiner notwendigen Aufwendungen zu leisten, die durch die Ausübung des Rechts entstehen.

(7) Für die Ausübung des Rechts auf Kurzberichterstattung über berufsmäßig durchgeführte Veranstaltungen kann der Veranstalter ein dem Charakter der Kurzberichterstattung entsprechendes billiges Entgelt verlangen.
Wird über die Höhe des Entgelts keine Einigkeit erzielt, soll ein schiedsrichterliches Verfahren nach den §§ 1025 ff. der Zivilprozessordnung vereinbart werden.
Das Fehlen einer Vereinbarung über die Höhe des Entgelts oder über die Durchführung eines schiedsrichterlichen Verfahrens steht der Ausübung des Rechts auf Kurzberichterstattung nicht entgegen; dasselbe gilt für einen bereits anhängigen Rechtsstreit über die Höhe des Entgelts.

(8) Die Ausübung des Rechts auf Kurzberichterstattung setzt eine Anmeldung des Fernsehveranstalters bis spätestens zehn Tage vor Beginn der Veranstaltung beim Veranstalter voraus.
Dieser hat spätestens fünf Tage vor dem Beginn der Veranstaltung den anmeldenden Fernsehveranstaltern mitzuteilen, ob genügend räumliche und technische Möglichkeiten für eine Übertragung oder Aufzeichnung bestehen.
Bei kurzfristigen Veranstaltungen und bei Ereignissen haben die Anmeldungen zum frühestmöglichen Zeitpunkt zu erfolgen.

(9) Reichen die räumlichen und technischen Gegebenheiten für eine Berücksichtigung aller Anmeldungen nicht aus, haben zunächst die Fernsehveranstalter Vorrang, die vertragliche Vereinbarungen mit dem Veranstalter oder dem Träger des Ereignisses geschlossen haben.
Darüber hinaus steht dem Veranstalter oder dem Träger des Ereignisses ein Auswahlrecht zu.
Dabei sind zunächst solche Fernsehveranstalter zu berücksichtigen, die eine umfassende Versorgung des Landes sicherstellen, in dem die Veranstaltung oder das Ereignis stattfindet.

(10) Fernsehveranstalter, die die Kurzberichterstattung wahrnehmen, sind verpflichtet, das Signal und die Aufzeichnung unmittelbar denjenigen Fernsehveranstaltern gegen Ersatz der angemessenen Aufwendungen zur Verfügung zu stellen, die nicht zugelassen werden konnten.

(11) Trifft der Veranstalter oder der Träger eines Ereignisses eine vertragliche Vereinbarung mit einem Fernsehveranstalter über eine Berichterstattung, hat er dafür Sorge zu tragen, dass mindestens ein anderer Fernsehveranstalter eine Kurzberichterstattung wahrnehmen kann.

(12) Die für die Kurzberichterstattung nicht verwerteten Teile sind spätestens drei Monate nach Beendigung der Veranstaltung oder des Ereignisses zu vernichten; die Vernichtung ist dem betreffenden Veranstalter oder Träger des Ereignisses schriftlich mitzuteilen.
Die Frist wird durch die Ausübung berechtigter Interessen Dritter unterbrochen.

##### § 15  
Europäische Produktionen, Eigen-, Auftrags- und Gemeinschaftsproduktionen

(1) Die Fernsehveranstalter tragen zur Sicherung von deutschen und europäischen Film- und Fernsehproduktionen als Kulturgut sowie als Teil des audiovisuellen Erbes bei.

(2) Zur Darstellung der Vielfalt im deutschsprachigen und europäischen Raum und zur Förderung von europäischen Film- und Fernsehproduktionen sollen die Fernsehveranstalter den Hauptteil ihrer insgesamt für Spielfilme, Fernsehspiele, Serien, Dokumentarsendungen und vergleichbare Produktionen vorgesehenen Sendezeit europäischen Werken entsprechend dem europäischen Recht vorbehalten.

(3) Fernsehvollprogramme sollen einen wesentlichen Anteil an Eigenproduktionen sowie Auftrags- und Gemeinschaftsproduktionen aus dem deutschsprachigen und europäischen Raum enthalten.
Das gleiche gilt für Fernsehspartenprogramme, soweit dies nach ihren inhaltlichen Schwerpunkten möglich ist.

(4) Im Rahmen seines Programmauftrages und unter Berücksichtigung der Grundsätze von Wirtschaftlichkeit und Sparsamkeit ist der öffentlich-rechtliche Rundfunk zur qualitativen und quantitativen Sicherung seiner Programmbeschaffung berechtigt, sich an Filmförderungen zu beteiligen, ohne dass unmittelbar eine Gegenleistung erfolgen muss.
Weitere landesrechtliche Regelungen bleiben unberührt.

##### § 16  
Auskunftspflicht und zuständige Behörden nach dem Europäischen Übereinkommen über das grenzüberschreitende Fernsehen

(1) Die Rundfunkanstalten des Landesrechts sind verpflichtet, der nach Landesrecht zuständigen Behörde gemäß Artikel 6 Abs.
2 des Europäischen Übereinkommens über das grenzüberschreitende Fernsehen die dort aufgeführten Informationen auf Verlangen zur Verfügung zu stellen.
Gleiches gilt für private Fernsehveranstalter, die auf Verlangen die Informationen der Landesmedienanstalt des Landes zur Verfügung zu stellen haben, in dem die Zulassung erteilt wurde oder in dem der Fernsehveranstalter im Sinne des § 54 seinen Sitz, Wohnsitz oder in Ermangelung dessen seinen ständigen Aufenthalt hat.
Diese leitet die Informationen an ihre rechtsaufsichtsführende Behörde weiter.

(2) Die Regierungschefinnen und Regierungschefs der Länder bestimmen durch Beschluss eine oder mehrere der in Absatz 1 genannten Behörden, welche die Aufgaben nach Artikel 19 Abs.
2 und 3 des Europäischen Übereinkommens über das grenzüberschreitende Fernsehen wahrnehmen.
Diesen Behörden sind zur Durchführung ihrer Aufgaben alle erforderlichen Informationen durch die zuständigen Behörden der einzelnen Länder zu übermitteln.

(3) Die Absätze 1 und 2 gelten entsprechend, soweit rechtsverbindliche Berichtspflichten der Länder zum Rundfunk gegenüber zwischenstaatlichen Einrichtungen oder internationalen Organisationen bestehen.
Satz 1 gilt auch für Teleshoppingkanäle.

#### 2.
Unterabschnitt  
Telemedien

##### § 17  
Allgemeine Grundsätze, Zulassungs- und Anmeldefreiheit

Telemedien sind im Rahmen der Gesetze zulassungs- und anmeldefrei.
Für die Angebote gilt die verfassungsmäßige Ordnung.
Die Vorschriften der allgemeinen Gesetze und die gesetzlichen Bestimmungen zum Schutz der persönlichen Ehre sind einzuhalten.

##### § 18  
Informationspflichten und Auskunftsrechte

(1) Anbieter von Telemedien, die nicht ausschließlich persönlichen oder familiären Zwecken dienen, haben folgende Informationen leicht erkennbar, unmittelbar erreichbar und ständig verfügbar zu halten:

1.  Name und Anschrift sowie
2.  bei juristischen Personen auch Name und Anschrift des Vertretungsberechtigten.

(2) Anbieter von Telemedien mit journalistisch-redaktionell gestalteten Angeboten, in denen insbesondere vollständig oder teilweise Inhalte periodischer Druckerzeugnisse in Text oder Bild wiedergegeben werden, haben zusätzlich zu den Angaben nach den §§ 5 und 6 des Telemediengesetzes einen Verantwortlichen mit Angabe des Namens und der Anschrift zu benennen.
Werden mehrere Verantwortliche benannt, ist kenntlich zu machen, für welchen Teil des Dienstes der jeweils Benannte verantwortlich ist.
Als Verantwortlicher darf nur benannt werden, wer

1.  seinen ständigen Aufenthalt im Inland hat,
2.  die Fähigkeit, öffentliche Ämter zu bekleiden, nicht durch Richterspruch verloren hat,
3.  unbeschränkt geschäftsfähig ist und
4.  unbeschränkt strafrechtlich verfolgt werden kann.

Satz 3 Nr.
3 und 4 gilt nicht für Jugendliche, die Telemedien verantworten, die für Jugendliche bestimmt sind.

(3) Anbieter von Telemedien in sozialen Netzwerken sind verpflichtet, bei mittels eines Computerprogramms automatisiert erstellten Inhalten oder Mitteilungen den Umstand der Automatisierung kenntlich zu machen, sofern das hierfür verwandte Nutzerkonto seinem äußeren Erscheinungsbild nach für die Nutzung durch natürliche Personen bereitgestellt wurde.
Dem Inhalt oder der Mitteilung ist der Hinweis gut lesbar bei- oder voranzustellen, dass dieser oder diese unter Einsatz eines das Nutzerkonto steuernden Computerprogrammes automatisiert erstellt und versandt wurde.
Ein Erstellen im Sinne dieser Vorschrift liegt nicht nur vor, wenn Inhalte und Mitteilungen unmittelbar vor dem Versenden automatisiert generiert werden, sondern auch, wenn bei dem Versand automatisiert auf einen vorgefertigten Inhalt oder eine vorprogrammierte Mitteilung zurückgegriffen wird.

(4) Für Anbieter von Telemedien nach Absatz 2 Satz 1 gilt § 5 entsprechend.

##### § 19  
Sorgfaltspflichten

(1) Telemedien mit journalistisch-redaktionell gestalteten Angeboten, in denen insbesondere vollständig oder teilweise Inhalte periodischer Druckerzeugnisse in Text oder Bild wiedergegeben werden, haben den anerkannten journalistischen Grundsätzen zu entsprechen.
Gleiches gilt für andere geschäftsmäßig angebotene, journalistisch-redaktionell gestaltete Telemedien, in denen regelmäßig Nachrichten oder politische Informationen enthalten sind und die nicht unter Satz 1 fallen.
Nachrichten sind vom Anbieter vor ihrer Verbreitung mit der nach den Umständen gebotenen Sorgfalt auf Inhalt, Herkunft und Wahrheit zu prüfen.

(2) Bei der Wiedergabe von Meinungsumfragen, die von Anbietern von Telemedien durchgeführt werden, ist ausdrücklich anzugeben, ob sie repräsentativ sind.

(3) Anbieter nach Absatz 1 Satz 2, die nicht der Selbstregulierung durch den Pressekodex und der Beschwerdeordnung des Deutschen Presserates unterliegen, können sich einer nach den Absätzen 4 bis 8 anerkannten Einrichtung der Freiwilligen Selbstkontrolle anschließen.
Anerkannte Einrichtungen der Freiwilligen Selbstkontrolle überprüfen die Einhaltung der Pflichten nach den Absätzen 1 und 2 bei den ihnen angeschlossenen Anbietern.
Sie sind verpflichtet, gemäß ihrer Verfahrensordnung nach Absatz 4 Nr. 4 Beschwerden über die ihnen angeschlossenen Anbieter unverzüglich nachzugehen.

(4) Eine Einrichtung ist als Einrichtung der Freiwilligen Selbstkontrolle im Sinne des Absatzes 3 anzuerkennen, wenn

1.  die Unabhängigkeit und Sachkunde ihrer benannten Prüfer gewährleistet ist und dabei auch Vertreter aus gesellschaftlichen Gruppen berücksichtigt sind, die sich in besonderer Weise mit Fragen des Journalismus befassen,
2.  eine sachgerechte Ausstattung sichergestellt ist,
3.  Vorgaben für die Entscheidungen der Prüfer bestehen, die in der Spruchpraxis die Einhaltung der Vorgaben der Absätze 1 und 2 zu gewährleisten geeignet sind,
4.  eine Verfahrensordnung besteht, die den Umfang und Ablauf der Prüfung sowie mögliche Sanktionen regelt und die Möglichkeit der Überprüfung von Entscheidungen vorsieht,
5.  gewährleistet ist, dass die betroffenen Anbieter vor einer Entscheidung gehört werden, die Entscheidung schriftlich begründet und den Beteiligten mitgeteilt wird,
6.  eine Beschwerdestelle eingerichtet ist und
7.  die Einrichtung für den Beitritt weiterer Anbieter offensteht.

(5) Die Entscheidung über die Anerkennung trifft die zuständige Landesmedienanstalt.

(6) Die Anerkennung kann ganz oder teilweise widerrufen oder mit Nebenbestimmungen versehen werden, wenn die Voraussetzungen für die Anerkennung nachträglich entfallen sind oder die Spruchpraxis der Einrichtung nicht mit den Bestimmungen dieses Staatsvertrages übereinstimmt.
Eine Entschädigung für Vermögensnachteile durch den Widerruf der Anerkennung wird nicht gewährt.

(7) Die anerkannten Einrichtungen der Freiwilligen Selbstkontrolle sollen sich über die Anwendung der Absätze 1 und 2 abstimmen.

(8) Die zuständige Landesmedienanstalt kann Entscheidungen einer anerkannten Einrichtung der Freiwilligen Selbstkontrolle, die die Grenzen des Beurteilungsspielraums überschreiten, beanstanden und ihre Aufhebung verlangen.
Kommt eine anerkannte Einrichtung der Freiwilligen Selbstkontrolle ihren Aufgaben und Pflichten nicht nach, kann die zuständige Landesmedienanstalt verlangen, dass sie diese erfüllt.
Eine Entschädigung für hierdurch entstehende Vermögensnachteile wird nicht gewährt.

##### § 20  
Gegendarstellung

(1) Anbieter von Telemedien mit journalistisch-redaktionell gestalteten Angeboten, in denen insbesondere vollständig oder teilweise Inhalte periodischer Druckerzeugnisse in Text oder Bild wiedergegeben werden, sind verpflichtet, unverzüglich eine Gegendarstellung der Person oder Stelle, die durch eine in ihrem Angebot aufgestellte Tatsachenbehauptung betroffen ist, ohne Kosten für den Betroffenen in ihr Angebot ohne zusätzliches Abrufentgelt aufzunehmen.
Die Gegendarstellung ist ohne Einschaltungen und Weglassungen in gleicher Aufmachung wie die Tatsachenbehauptung anzubieten.
Die Gegendarstellung ist so lange wie die Tatsachenbehauptung in unmittelbarer Verknüpfung mit ihr anzubieten.
Wird die Tatsachenbehauptung nicht mehr angeboten oder endet das Angebot vor Aufnahme der Gegendarstellung, ist die Gegendarstellung an vergleichbarer Stelle so lange anzubieten, wie die ursprünglich angebotene Tatsachenbehauptung.
Eine Erwiderung auf die Gegendarstellung muss sich auf tatsächliche Angaben beschränken und darf nicht unmittelbar mit der Gegendarstellung verknüpft werden.

(2) Eine Verpflichtung zur Aufnahme der Gegendarstellung gemäß Absatz 1 besteht nicht, wenn

1.  der Betroffene kein berechtigtes Interesse an der Gegendarstellung hat,
2.  der Umfang der Gegendarstellung unangemessen über den der beanstandeten Tatsachenbehauptung hinausgeht,
3.  die Gegendarstellung sich nicht auf tatsächliche Angaben beschränkt oder einen strafbaren Inhalt hat oder
4.  die Gegendarstellung nicht unverzüglich, spätestens sechs Wochen nach dem letzten Tage des Angebots des beanstandeten Textes, jedenfalls jedoch drei Monate nach der erstmaligen Einstellung des Angebots, dem in Anspruch genommenen Anbieter schriftlich und von dem Betroffenen oder seinem gesetzlichen Vertreter unterzeichnet zugeht.

(3) Für die Durchsetzung des vergeblich geltend gemachten Gegendarstellungsanspruchs ist der ordentliche Rechtsweg gegeben.
Auf dieses Verfahren sind die Vorschriften der Zivilprozessordnung über das Verfahren auf Erlass einer einstweiligen Verfügung entsprechend anzuwenden.
Eine Gefährdung des Anspruchs braucht nicht glaubhaft gemacht zu werden.
Ein Verfahren zur Hauptsache findet nicht statt.

(4) Eine Verpflichtung zur Gegendarstellung besteht nicht für wahrheitsgetreue Berichte über öffentliche Sitzungen der übernationalen parlamentarischen Organe, der gesetzgebenden Organe des Bundes und der Länder sowie derjenigen Organe und Stellen, bei denen das jeweilige Landespressegesetz eine presserechtliche Gegendarstellung ausschließt.

##### § 21  
Barrierefreiheit

Anbieter von Telemedien sollen im Rahmen der technischen und ihrer finanziellen Möglichkeiten den barrierefreien Zugang zu Fernsehprogrammen und fernsehähnlichen Telemedien unterstützen.

##### § 22  
Werbung, Sponsoring, Gewinnspiele

(1) Werbung muss als solche klar erkennbar und vom übrigen Inhalt der Angebote eindeutig getrennt sein.
In der Werbung dürfen keine unterschwelligen Techniken eingesetzt werden.
Bei Werbung politischer, weltanschaulicher oder religiöser Art muss auf den Werbetreibenden oder Auftraggeber in angemessener Weise deutlich hingewiesen werden; § 10 Abs.
1 Satz 2 gilt entsprechend.

(2) Für Sponsoring bei Fernsehtext gilt § 10 entsprechend.

(3) Für Gewinnspiele in Telemedien nach § 19 Abs.
1 gilt § 11 entsprechend.

##### § 23  
Datenverarbeitung zu journalistischen Zwecken, Medienprivileg

(1) Soweit die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF, das Deutschlandradio, private Rundfunkveranstalter oder Unternehmen und Hilfsunternehmen der Presse als Anbieter von Telemedien personenbezogene Daten zu journalistischen Zwecken verarbeiten, ist es den hiermit befassten Personen untersagt, diese personenbezogenen Daten zu anderen Zwecken zu verarbeiten (Datengeheimnis).
Diese Personen sind bei der Aufnahme ihrer Tätigkeit auf das Datengeheimnis zu verpflichten.
Das Datengeheimnis besteht auch nach Beendigung ihrer Tätigkeit fort.
Im Übrigen finden für die Datenverarbeitung zu journalistischen Zwecken außer den Kapiteln I, VIII, X und XI der Verordnung (EU) 2016/679 nur die Artikel 5 Abs.
1 Buchst.
f in Verbindung mit Abs.
2, Artikel 24 und Artikel 32 der Verordnung (EU) 2016/679 Anwendung.
Artikel 82 und 83 der Verordnung (EU) 2016/679 gelten mit der Maßgabe, dass nur für eine Verletzung des Datengeheimnisses gemäß den Sätzen 1 bis 3 sowie für unzureichende Maßnahmen nach Artikel 5 Abs.
1 Buchst.
f, Artikel 24 und 32 der Verordnung (EU) 2016/679 gehaftet wird.
Kapitel VIII der Verordnung (EU) 2016/679 findet keine Anwendung, soweit Unternehmen, Hilfs- und Beteiligungsunternehmen der Presse der Selbstregulierung durch den Pressekodex und der Beschwerdeordnung des Deutschen Presserates unterliegen.
Die Sätze 1 bis 6 gelten entsprechend für die zu den in Satz 1 genannten Stellen gehörenden Hilfs- und Beteiligungsunternehmen.
Den betroffenen Personen stehen nur die in den Absätzen 2 und 3 genannten Rechte zu.

(2) Werden personenbezogene Daten von einem Anbieter von Telemedien zu journalistischen Zwecken gespeichert, verändert, übermittelt, gesperrt oder gelöscht und wird die betroffene Person dadurch in ihrem Persönlichkeitsrecht beeinträchtigt, kann sie Auskunft über die zugrunde liegenden, zu ihrer Person gespeicherten Daten verlangen.
Die Auskunft kann nach Abwägung der schutzwürdigen Interessen der Beteiligten verweigert werden, soweit

1.  aus den Daten auf Personen, die bei der Vorbereitung, Herstellung oder Verbreitung mitgewirkt haben, geschlossen werden kann,
2.  aus den Daten auf die Person des Einsenders oder des Gewährsträgers von Beiträgen, Unterlagen und Mitteilungen für den redaktionellen Teil geschlossen werden kann oder
3.  durch die Mitteilung der recherchierten oder sonst erlangten Daten die journalistische Aufgabe des Anbieters durch Ausforschung des Informationsbestandes beeinträchtigt würde.

Die betroffene Person kann die unverzügliche Berichtigung unrichtiger personenbezogener Daten im Datensatz oder die Hinzufügung einer eigenen Darstellung von angemessenem Umfang verlangen.
Die weitere Speicherung der personenbezogenen Daten ist rechtmäßig, wenn dies für die Ausübung des Rechts auf freie Meinungsäußerung und Information oder zur Wahrnehmung berechtigter Interessen erforderlich ist.
Die Sätze 1 bis 3 gelten nicht für Angebote von Unternehmen, Hilfs- und Beteiligungsunternehmen der Presse, soweit diese der Selbstregulierung durch den Pressekodex und der Beschwerdeordnung des Deutschen Presserates unterliegen.

(3) Führt die journalistische Verarbeitung personenbezogener Daten zur Verbreitung von Gegendarstellungen der betroffenen Person oder zu Verpflichtungserklärungen, Beschlüssen oder Urteilen über die Unterlassung der Verbreitung oder über den Widerruf des Inhalts der Daten, sind diese Gegendarstellungen, Verpflichtungserklärungen und Widerrufe zu den gespeicherten Daten zu nehmen und dort für dieselbe Zeitdauer aufzubewahren wie die Daten selbst sowie bei einer Übermittlung der Daten gemeinsam mit diesen zu übermitteln.

##### § 24  
Telemediengesetz, Öffentliche Stellen

(1) Für Telemedien, die den Bestimmungen dieses Staatsvertrages oder den Bestimmungen der übrigen medienrechtlichen Staatsverträge der Länder unterfallen, gelten im Übrigen die Bestimmungen des Telemediengesetzes in seiner jeweils geltenden Fassung.
Absatz 2 bleibt unberührt.

(2) Für die öffentlichen Stellen der Länder gelten neben den vorstehenden Bestimmungen die Bestimmungen des Telemediengesetzes in seiner jeweils geltenden Fassung entsprechend.

(3) Die Aufsicht über die Einhaltung der Bestimmungen des Telemediengesetzes richtet sich nach Landesrecht.

##### § 25  
Notifizierung

Änderungen dieses Unterabschnitts sowie des V.
Abschnitts unterliegen der Notifizierungspflicht gemäß der Richtlinie (EU) 2015/1535 des Europäischen Parlaments und des Rates vom 9. September 2015 über ein Informationsverfahren auf dem Gebiet der technischen Vorschriften und der Vorschriften für die Dienste der Informationsgesellschaft (ABl.
L 241 vom 17.9.2015, S.
1).

### III.
Abschnitt  
Besondere Bestimmungen für den öffentlich-rechtlichen Rundfunk

##### § 26  
Auftrag

(1) Auftrag der öffentlich-rechtlichen Rundfunkanstalten ist, durch die Herstellung und Verbreitung ihrer Angebote als Medium und Faktor des Prozesses freier individueller und öffentlicher Meinungsbildung zu wirken und dadurch die demokratischen, sozialen und kulturellen Bedürfnisse der Gesellschaft zu erfüllen.
Die öffentlich-rechtlichen Rundfunkanstalten haben in ihren Angeboten einen umfassenden Überblick über das internationale, europäische, nationale und regionale Geschehen in allen wesentlichen Lebensbereichen zu geben.
Sie sollen hierdurch die internationale Verständigung, die europäische Integration und den gesellschaftlichen Zusammenhalt in Bund und Ländern fördern.
Ihre Angebote haben der Bildung, Information, Beratung und Unterhaltung zu dienen.
Sie haben Beiträge insbesondere zur Kultur anzubieten.
Auch Unterhaltung soll einem öffentlich-rechtlichen Angebotsprofil entsprechen.

(2) Die öffentlich-rechtlichen Rundfunkanstalten haben bei der Erfüllung ihres Auftrags die Grundsätze der Objektivität und Unparteilichkeit der Berichterstattung, die Meinungsvielfalt sowie die Ausgewogenheit ihrer Angebote zu berücksichtigen.

(3) Die öffentlich-rechtlichen Rundfunkanstalten arbeiten zur Erfüllung ihres Auftrages zusammen; die Zusammenarbeit regeln sie in öffentlich-rechtlichen Verträgen.

(4) Die öffentlich-rechtlichen Rundfunkanstalten sind mit der Erbringung von Dienstleistungen von allgemeinem wirtschaftlichem Interesse im Sinne des Artikels 106 Abs.
2 des Vertrages über die Arbeitsweise der Europäischen Union auch betraut, soweit sie zur Erfüllung ihres Auftrags gemäß Absatz 1 bei der Herstellung und Verbreitung von Angeboten im Sinne des § 27 zusammenarbeiten.
Die Betrauung gilt insbesondere für die Bereiche Produktion, Produktionsstandards, Programmrechteerwerb, Programmaustausch, Verbreitung und Weiterverbreitung von Angeboten, Beschaffungswesen, Sendernetzbetrieb, informationstechnische und sonstige Infrastrukturen, Vereinheitlichung von Geschäftsprozessen, Beitragsservice und allgemeine Verwaltung.
Von der Betrauung nicht umfasst sind kommerzielle Tätigkeiten nach § 40 Abs.
1 Satz 2.

##### § 27  
Angebote

(1) Angebote des öffentlich-rechtlichen Rundfunks sind Rundfunkprogramme (Hörfunk- und Fernsehprogramme) und Telemedienangebote nach Maßgabe dieses Staatsvertrages und der jeweiligen landesrechtlichen Regelungen.
Der öffentlich-rechtliche Rundfunk kann programmbegleitend Druckwerke mit programmbezogenem Inhalt anbieten.

(2) Rundfunkprogramme, die über unterschiedliche Übertragungswege zeitgleich verbreitet werden, gelten zahlenmäßig als ein Angebot.

##### § 28  
Fernsehprogramme

(1) Die in der ARD zusammengeschlossenen Landesrundfunkanstalten veranstalten gemeinsam folgende Fernsehprogramme:

1.  das Vollprogramm „Erstes Deutsches Fernsehen (Das Erste)“,
2.  zwei Programme als Zusatzangebote nach Maßgabe der als Anlage beigefügten Konzepte, und zwar die Programme
    1.  „tagesschau24“ und
    2.  „EinsFestival“.

(2) Folgende Fernsehprogramme von einzelnen oder mehreren in der ARD zusammengeschlossenen Landesrundfunkanstalten werden nach Maßgabe ihres jeweiligen Landesrechts veranstaltet:

1.  die Dritten Fernsehprogramme einschließlich regionaler Auseinanderschaltungen, und zwar jeweils
    1.  des Bayerischen Rundfunks (BR),
    2.  des Hessischen Rundfunks (HR),
    3.  des Mitteldeutschen Rundfunks (MDR),
    4.  des Norddeutschen Rundfunks (NDR),
    5.  von Radio Bremen (RB),
    6.  vom Rundfunk Berlin-Brandenburg (RBB),
    7.  des Südwestrundfunks (SWR),
    8.  des Saarländischen Rundfunks (SR) und
    9.  des Westdeutschen Rundfunks (WDR),
2.  das Spartenprogramm „ARD-alpha“ mit dem Schwerpunkt Bildung vom BR.

(3) Das ZDF veranstaltet folgende Fernsehprogramme:

1.  das Vollprogramm „Zweites Deutsches Fernsehen (ZDF)“,
2.  zwei Programme als Zusatzangebote nach Maßgabe der als Anlage beigefügten Konzepte, und zwar die Programme
    1.  „ZDFinfo“ und
    2.  „ZDFneo“.

(4) Die in der ARD zusammengeschlossenen Landesrundfunkanstalten und das ZDF veranstalten gemeinsam folgende Fernsehprogramme:

1.  das Vollprogramm „3sat“ mit kulturellem Schwerpunkt unter Beteiligung öffentlich-rechtlicher europäischer Veranstalter,
2.  das Vollprogramm „arte - Der Europäische Kulturkanal“ unter Beteiligung öffentlich-rechtlicher europäischer Veranstalter,
3.  das Spartenprogramm „PHOENIX - Der Ereignis- und Dokumentationskanal“ und
4.  das Spartenprogramm „KI.KA - Der Kinderkanal“.

(5) Die analoge Verbreitung eines bislang ausschließlich digital verbreiteten Programms ist unzulässig.

##### § 29  
Hörfunkprogramme

(1) Die in der ARD zusammengeschlossenen Landesrundfunkanstalten veranstalten Hörfunkprogramme einzeln oder zu mehreren für ihr jeweiliges Versorgungsgebiet auf Grundlage des jeweiligen Landesrechts; bundesweit ausgerichtete Hörfunkprogramme finden nicht statt.
Ausschließlich im Internet verbreitete Hörfunkprogramme sind nur nach Maßgabe eines nach § 32 durchgeführten Verfahrens zulässig.

(2) Die Gesamtzahl der terrestrisch verbreiteten Hörfunkprogramme der in der ARD zusammengeschlossenen Landesrundfunkanstalten darf die Zahl der zum 1.
April 2004 terrestrisch verbreiteten Hörfunkprogramme nicht übersteigen.
Das Landesrecht kann vorsehen, dass die jeweilige Landesrundfunkanstalt zusätzlich so viele digitale terrestrische Hörfunkprogramme veranstaltet, wie sie Länder versorgt.
Das jeweilige Landesrecht kann vorsehen, dass terrestrisch verbreitete Hörfunkprogramme gegen andere terrestrisch verbreitete Hörfunkprogramme, auch gegen ein Kooperationsprogramm, ausgetauscht werden, wenn dadurch insgesamt keine Mehrkosten entstehen und sich die Gesamtzahl der Programme nicht erhöht.
Kooperationsprogramme werden jeweils als ein Programm der beteiligten Anstalten gerechnet.
Regionale Auseinanderschaltungen von Programmen bleiben unberührt.
Der Austausch eines in digitaler Technik verbreiteten Programms gegen ein in analoger Technik verbreitetes Programm ist nicht zulässig.

(3) Das Deutschlandradio veranstaltet folgende Hörfunkprogramme mit den Schwerpunkten in den Bereichen Information, Bildung und Kultur:

1.  das Programm „Deutschlandfunk“,
2.  das Programm „Deutschlandfunk Kultur“,
3.  das in digitaler Technik verbreitete Programm „Deutschlandfunk Nova“ nach Maßgabe des als Anlage beigefügten Konzepts, insbesondere unter Rückgriff auf die Möglichkeiten nach § 5 Abs. 2 des Deutschlandradio-Staatsvertrages; die in der ARD zusammengeschlossenen Landesrundfunkanstalten kooperieren hierzu mit dem Deutschlandradio,
4.  ausschließlich im Internet verbreitete Hörfunkprogramme mit Inhalten aus den in den Nummern 1 bis 3 aufgeführten Programmen nach Maßgabe eines nach § 32 durchgeführten Verfahrens.

(4) Die in der ARD zusammengeschlossenen Landesrundfunkanstalten und das Deutschlandradio veröffentlichen in den amtlichen Verkündungsblättern der Länder jährlich zum 1.
Januar eine Auflistung der von allen Anstalten insgesamt veranstalteten Hörfunkprogramme.

##### § 30  
Telemedienangebote

(1) Die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und das Deutschlandradio bieten Telemedienangebote nach Maßgabe des § 2 Abs.
2 Nr.
29 an.

(2) Der Auftrag nach Absatz 1 umfasst insbesondere

1.  Sendungen ihrer Programme auf Abruf vor und nach deren Ausstrahlung sowie eigenständige audiovisuelle Inhalte,
2.  Sendungen ihrer Programme auf Abruf von europäischen Werken angekaufter Spielfilme und angekaufter Folgen von Fernsehserien, die keine Auftragsproduktionen sind, bis zu dreißig Tage nach deren Ausstrahlung, wobei die Abrufmöglichkeit grundsätzlich auf Deutschland zu beschränken ist,
3.  Sendungen ihrer Programme auf Abruf von Großereignissen gemäß § 13 Abs.
    2 sowie von Spielen der 1.
    und 2. Fußball-Bundesliga bis zu sieben Tage danach,
4.  zeit- und kulturgeschichtliche Archive mit informierenden, bildenden und kulturellen Telemedien.

Im Übrigen bleiben Angebote nach Maßgabe der §§ 40 bis 44 unberührt.

(3) Durch die zeitgemäße Gestaltung der Telemedienangebote soll allen Bevölkerungsgruppen die Teilhabe an der Informationsgesellschaft ermöglicht, Orientierungshilfe geboten, Möglichkeiten der interaktiven Kommunikation angeboten sowie die technische und inhaltliche Medienkompetenz aller Generationen und von Minderheiten gefördert werden.
Diese Gestaltung der Telemedienangebote soll die Belange von Menschen mit Behinderungen besonders berücksichtigen, insbesondere in Form von Audiodeskription, Bereitstellung von Manuskripten oder Telemedien in leichter Sprache.

(4) Die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und das Deutschlandradio bieten ihre Angebote in möglichst barrierefrei zugänglichen elektronischen Portalen an und fassen ihre Programme unter elektronischen Programmführern zusammen.
Soweit dies zur Erreichung der Zielgruppe aus journalistisch-redaktionellen Gründen geboten ist, können sie Telemedien auch außerhalb des dafür jeweils eingerichteten eigenen Portals anbieten.
Die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und das Deutschlandradio sollen ihre Telemedien, die aus journalistisch-redaktionellen Gründen dafür geeignet sind, miteinander vernetzen, insbesondere durch Verlinkung.
Sie sollen auch auf Inhalte verlinken, die Einrichtungen der Wissenschaft und Kultur anbieten und die aus journalistisch-redaktionellen Gründen für die Telemedienangebote geeignet sind.

(5) Nicht zulässig sind in Telemedienangeboten:

1.  Werbung mit Ausnahme von Produktplatzierung,
2.  das Angebot auf Abruf von angekauften Spielfilmen und angekauften Folgen von Fernsehserien, die keine Auftragsproduktionen sind mit Ausnahme der in Absatz 2 Satz 1 Nr.
    2 genannten europäischen Werke,
3.  eine flächendeckende lokale Berichterstattung,
4.  die in der Anlage zu diesem Staatsvertrag aufgeführten Angebotsformen.

Für Produktplatzierung nach Satz 1 Nr.
1 gelten § 8 Abs.
7 und § 38 entsprechend.

(6) Werden Telemedien von den in der ARD zusammengeschlossenen Landesrundfunkanstalten, dem ZDF oder dem Deutschlandradio außerhalb des von ihnen jeweils eingerichteten eigenen Portals verbreitet, sollen sie für die Einhaltung des Absatzes 5 Satz 1 Nr.
1 Sorge tragen.
Durch die Nutzung dieses Verbreitungswegs dürfen sie keine Einnahmen durch Werbung und Sponsoring erzielen.

(7) Die Telemedienangebote dürfen nicht presseähnlich sein.
Sie sind im Schwerpunkt mittels Bewegtbild oder Ton zu gestalten, wobei Text nicht im Vordergrund stehen darf.
Angebotsübersichten, Schlagzeilen, Sendungstranskripte, Informationen über die jeweilige Rundfunkanstalt und Maßnahmen zum Zweck der Barrierefreiheit bleiben unberührt.
Unberührt bleiben ferner Telemedien, die der Aufbereitung von Inhalten aus einer konkreten Sendung einschließlich Hintergrundinformationen dienen, soweit auf für die jeweilige Sendung genutzte Materialien und Quellen zurückgegriffen wird und diese Angebote thematisch und inhaltlich die Sendung unterstützen, begleiten und aktualisieren, wobei der zeitliche und inhaltliche Bezug zu einer bestimmten Sendung im jeweiligen Telemedienangebot ausgewiesen werden muss.
Auch bei Telemedien nach Satz 4 soll nach Möglichkeit eine Einbindung von Bewegtbild oder Ton erfolgen.
Zur Anwendung der Sätze 1 bis 5 soll von den öffentlich-rechtlichen Rundfunkanstalten und den Spitzenverbänden der Presse eine Schlichtungsstelle eingerichtet werden.

##### § 31  
Satzungen, Richtlinien, Berichtspflichten

(1) Die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und das Deutschlandradio erlassen jeweils Satzungen oder Richtlinien zur näheren Durchführung ihres jeweiligen Auftrags sowie für das Verfahren zur Erstellung von Konzepten für Telemedienangebote und das Verfahren für neue Telemedienangebote oder wesentliche Änderungen.
Die Satzungen oder Richtlinien enthalten auch Regelungen zur Sicherstellung der Unabhängigkeit der Gremienentscheidungen.
Die Satzungen oder Richtlinien sind im Internetauftritt der in der ARD zusammengeschlossenen Landesrundfunkanstalten, des ZDF oder des Deutschlandradios zu veröffentlichen.

(2) Die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und das Deutschlandradio veröffentlichen alle zwei Jahre einen Bericht über die Erfüllung ihres jeweiligen Auftrages, über die Qualität und Quantität der bestehenden Angebote sowie die Schwerpunkte der jeweils geplanten Angebote.

(3) In den Geschäftsberichten der in der ARD zusammengeschlossenen Landesrundfunkanstalten, des ZDF und des Deutschlandradios ist auch der Umfang der Produktionen mit von diesen gesellschaftsrechtlich abhängigen und unabhängigen Produktionsunternehmen darzustellen.
Dabei ist auch darzustellen, in welcher Weise der Protokollerklärung aller Länder zu § 11d Abs.
2 des Rundfunkstaatsvertrages im Rahmen des 22.
Rundfunkänderungsstaatsvertrages Rechnung getragen wird.

##### § 32  
Telemedienkonzepte

(1) Die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und das Deutschlandradio konkretisieren die inhaltliche Ausrichtung ihrer geplanten Telemedienangebote nach § 30 jeweils in Telemedienkonzepten, die Zielgruppe, Inhalt, Ausrichtung, Verweildauer, die Verwendung internetspezifischer Gestaltungsmittel sowie die Maßnahmen zur Einhaltung des § 30 Abs. 7 Satz 1 näher beschreiben.
Es sind angebotsabhängige differenzierte Befristungen für die Verweildauern vorzunehmen mit Ausnahme der Archive nach § 30 Abs.
2 Satz 1 Nr.
4, die unbefristet zulässig sind.
Sollen Telemedien auch außerhalb des eingerichteten eigenen Portals angeboten werden, ist dies zu begründen.
Die insoweit vorgesehenen Maßnahmen zur Berücksichtigung des Jugendmedienschutzes, des Datenschutzes sowie des § 30 Abs.
6 Satz 1 sind zu beschreiben.

(2) Die Beschreibung aller Telemedienangebote muss einer Nachprüfung des Finanzbedarfs durch die Kommission zur Überprüfung und Ermittlung des Finanzbedarfs der Rundfunkanstalten (KEF) ermöglichen.

(3) Die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und das Deutschlandradio legen in den Satzungen oder Richtlinien übereinstimmende Kriterien fest, in welchen Fällen ein neues oder die wesentliche Änderung eines Telemedienangebots vorliegt, das nach dem nachstehenden Verfahren der Absätze 4 bis 7 zu prüfen ist.
Eine wesentliche Änderung liegt insbesondere vor, wenn die inhaltliche Gesamtausrichtung des Telemedienangebots oder die angestrebte Zielgruppe verändert wird.
Das Verfahren der Absätze 4 bis 7 bezieht sich bei wesentlichen Änderungen allein auf die Abweichungen von den bisher veröffentlichten Telemedienkonzepten.

(4) Ist ein neues Telemedienangebot nach Absatz 1 oder die wesentliche Änderung eines bestehenden Telemedienangebots nach Absatz 3 geplant, hat die Rundfunkanstalt gegenüber ihrem zuständigen Gremium darzulegen, dass das geplante, neue Telemedienangebot oder die wesentliche Änderung vom Auftrag umfasst ist.
Es sind Aussagen darüber zu treffen,

1.  inwieweit das neue Telemedienangebot oder die wesentliche Änderung den demokratischen, sozialen und kulturellen Bedürfnissen der Gesellschaft entspricht,
2.  in welchem Umfang durch das neue Telemedienangebot oder die wesentliche Änderung in qualitativer Hinsicht zum publizistischen Wettbewerb beigetragen wird und
3.  welcher finanzielle Aufwand für das neue Telemedienangebot oder die wesentliche Änderung erforderlich ist.

Dabei sind Quantität und Qualität der vorhandenen frei zugänglichen Telemedienangebote, die Auswirkungen auf alle relevanten Märkte des geplanten, neuen Telemedienangebots oder der wesentlichen Änderung sowie jeweils deren meinungsbildende Funktion angesichts bereits vorhandener vergleichbarer frei zugänglicher Telemedienangebote, auch des öffentlich-rechtlichen Rundfunks, zu berücksichtigen.

(5) Zu den Anforderungen des Absatzes 4 ist vor Aufnahme eines neuen Telemedienangebots oder einer wesentlichen Änderung durch das zuständige Gremium Dritten in geeigneter Weise, insbesondere im Internet, Gelegenheit zur Stellungnahme zu geben.
Die Gelegenheit zur Stellungnahme besteht innerhalb einer Frist von mindestens sechs Wochen nach Veröffentlichung des Vorhabens.
Das zuständige Gremium der Rundfunkanstalt hat die eingegangenen Stellungnahmen zu prüfen.
Das zuständige Gremium kann zur Entscheidungsbildung gutachterliche Beratung durch unabhängige Sachverständige auf Kosten der jeweiligen Rundfunkanstalt in Auftrag geben; zu den Auswirkungen auf alle relevanten Märkte ist gutachterliche Beratung hinzuzuziehen.
Der Name des Gutachters ist bekanntzugeben.
Der Gutachter kann weitere Auskünfte und Stellungnahmen einholen; ihm können Stellungnahmen unmittelbar übersandt werden.

(6) Die Entscheidung, ob die Aufnahme eines neuen Telemedienangebots oder einer wesentlichen Änderung den Voraussetzungen des Absatzes 4 entspricht, bedarf der Mehrheit von zwei Dritteln der anwesenden Mitglieder, mindestens der Mehrheit der gesetzlichen Mitglieder des zuständigen Gremiums.
Die Entscheidung ist zu begründen.
In den Entscheidungsgründen muss unter Berücksichtigung der eingegangenen Stellungnahmen und eingeholten Gutachten dargelegt werden, ob das neue Telemedienangebot oder die wesentliche Änderung vom Auftrag umfasst ist.
Die jeweilige Rundfunkanstalt hat das Ergebnis ihrer Prüfung einschließlich der eingeholten Gutachten unter Wahrung von Geschäftsgeheimnissen in gleicher Weise wie die Veröffentlichung des Vorhabens bekannt zu machen.

(7) Der für die Rechtsaufsicht zuständigen Behörde sind vor der Veröffentlichung alle für eine rechtsaufsichtliche Prüfung notwendigen Auskünfte zu erteilen und Unterlagen zu übermitteln.
Nach Abschluss des Verfahrens nach den Absätzen 5 und 6 und nach Prüfung durch die für die Rechtsaufsicht zuständige Behörde ist die Beschreibung des neuen Telemedienangebots oder der wesentlichen Änderung im Internetauftritt der in der ARD zusammengeschlossenen Landesrundfunkanstalten, des ZDF oder des Deutschlandradios zu veröffentlichen.
In den amtlichen Verkündungsblättern der betroffenen Länder ist zugleich auf die Veröffentlichung im Internetauftritt der jeweiligen Rundfunkanstalt hinzuweisen.

##### § 33  
Jugendangebot

(1) Die in der ARD zusammengeschlossenen Landesrundfunkanstalten und das ZDF bieten gemeinsam ein Jugendangebot an, das Rundfunk und Telemedien umfasst.
Das Jugendangebot soll inhaltlich die Lebenswirklichkeit und die Interessen junger Menschen als Zielgruppe in den Mittelpunkt stellen und dadurch einen besonderen Beitrag zur Erfüllung des öffentlich-rechtlichen Auftrags nach § 26 leisten.
Zu diesem Zweck sollen die in der ARD zusammengeschlossenen Landesrundfunkanstalten und das ZDF insbesondere eigenständige audiovisuelle Inhalte für das Jugendangebot herstellen oder herstellen lassen und Nutzungsrechte an Inhalten für das Jugendangebot erwerben.
Das Jugendangebot soll journalistisch-redaktionell veranlasste und journalistisch-redaktionell gestaltete interaktive Angebotsformen aufweisen und Inhalte anbieten, die die Nutzer selbst zur Verfügung stellen.

(2) Zur Erfüllung der demokratischen, sozialen und kulturellen Bedürfnisse der Zielgruppe ist das Jugendangebot inhaltlich und technisch dynamisch und entwicklungsoffen zu gestalten und zu verbreiten.
Dazu soll auch durch eine zielgruppengerechte interaktive Kommunikation mit den Nutzern sowie durch verstetigte Möglichkeiten ihrer Partizipation beigetragen werden.

(3) Andere Angebote der in der ARD zusammengeschlossenen Landesrundfunkanstalten und des ZDF nach Maßgabe dieses Staatsvertrages sollen mit dem Jugendangebot inhaltlich und technisch vernetzt werden.
Wird ein eigenständiger Inhalt des Jugendangebots auch in einem anderen Angebot der in der ARD zusammengeschlossenen Landesrundfunkanstalten oder des ZDF genutzt, sind die für das andere Angebot geltenden Maßgaben dieses Staatsvertrages einschließlich eines eventuellen Telemedienkonzepts zu beachten.

(4) Die Verweildauer der Inhalte des Jugendangebots ist von den in der ARD zusammengeschlossenen Landesrundfunkanstalten und dem ZDF so zu bemessen, dass sie die Lebenswirklichkeit und die Interessen junger Menschen abbilden und die demokratischen, sozialen und kulturellen Bedürfnisse der jeweils zur Zielgruppe gehörenden Generationen erfüllen.
Die Grundsätze der Bemessung der Verweildauer sind von den in der ARD zusammengeschlossenen Landesrundfunkanstalten und dem ZDF regelmäßig zu prüfen.
Die Verweildauer von angekauften Spielfilmen und angekauften Folgen von Fernsehserien, die keine Auftragsproduktionen sind, ist zeitlich angemessen zu begrenzen.

(5) Werbung mit Ausnahme von Produktplatzierung nach Maßgabe von § 8 Abs.
7 und § 38, flächendeckende lokale Berichterstattung, nicht auf das Jugendangebot bezogene presseähnliche Angebote, ein eigenständiges Hörfunkprogramm und die für das Jugendangebot in der Anlage zu diesem Staatsvertrag genannten Angebotsformen sind im Jugendangebot nicht zulässig.
Ist zur Erreichung der Zielgruppe aus journalistisch-redaktionellen Gründen die Verbreitung des Jugendangebots außerhalb des von den in der ARD zusammengeschlossenen Landesrundfunkanstalten und dem ZDF für das Jugendangebot eingerichteten eigenen Portals geboten, sollen die in der ARD zusammengeschlossenen Landesrundfunkanstalten und das ZDF für die Einhaltung der Bedingungen des Satzes 1 Sorge tragen.
Sie haben für diesen Verbreitungsweg übereinstimmende Richtlinien, insbesondere zur Konkretisierung des Jugendmedienschutzes und des Datenschutzes, zu erlassen.
Das Jugendangebot darf nicht über Rundfunkfrequenzen (Kabel, Satellit, Terrestrik) verbreitet werden.

(6) Die in der ARD zusammengeschlossenen Landesrundfunkanstalten und das ZDF haben gemeinsam in Bezug auf das Jugendangebot in dem nach § 31 Abs.
2 zu veröffentlichenden Bericht insbesondere darzustellen:

1.  den besonderen Beitrag des Jugendangebots zur Erfüllung des öffentlich-rechtlichen Auftrags,
2.  das Erreichen der Zielgruppe, die zielgruppengerechte Kommunikation sowie die verstetigten Möglichkeiten der Partizipation der Zielgruppe,
3.  das Ergebnis der Prüfung der Verweildauer nach Absatz 4,
4.  die Nutzung des Verbreitungswegs außerhalb des für das Jugendangebot eingerichteten eigenen Portals nach Absatz 5 Satz 2 und 3,
5.  den jeweiligen Anteil der in Deutschland und in Europa für das Jugendangebot hergestellten Inhalte und
6.  den jeweiligen Anteil an Eigenproduktionen, Auftragsproduktionen und erworbenen Nutzungsrechten für angekaufte Spielfilme und angekaufte Folgen von Fernsehserien für das Jugendangebot.

##### § 34  
Funktionsgerechte Finanzausstattung, Grundsatz des Finanzausgleichs

(1) Die Finanzausstattung hat den öffentlich-rechtlichen Rundfunk in die Lage zu versetzen, seine verfassungsmäßigen und gesetzlichen Aufgaben zu erfüllen; sie hat insbesondere den Bestand und die Entwicklung des öffentlich-rechtlichen Rundfunks zu gewährleisten.

(2) Der Finanzausgleich unter den Landesrundfunkanstalten ist Bestandteil des Finanzierungssystems der ARD; er stellt insbesondere eine funktionsgerechte Aufgabenerfüllung der Anstalten Saarländischer Rundfunk und Radio Bremen sicher.
Der Umfang der Finanzausgleichsmasse und ihre Anpassung an den Rundfunkbeitrag bestimmen sich nach dem Rundfunkfinanzierungsstaatsvertrag.

##### § 35  
Finanzierung

Der öffentlich-rechtliche Rundfunk finanziert sich durch Rundfunkbeiträge, Einnahmen aus Rundfunkwerbung und sonstige Einnahmen; vorrangige Finanzierungsquelle ist der Rundfunkbeitrag.
Programme und Angebote im Rahmen seines Auftrags gegen besonderes Entgelt sind unzulässig; ausgenommen hiervon sind Begleitmaterialien.
Einnahmen aus dem Angebot von Telefonmehrwertdiensten dürfen nicht erzielt werden.

##### § 36  
Finanzbedarf des öffentlich-rechtlichen Rundfunks

(1) Der Finanzbedarf des öffentlich-rechtlichen Rundfunks wird regelmäßig entsprechend den Grundsätzen von Wirtschaftlichkeit und Sparsamkeit, einschließlich der damit verbundenen Rationalisierungspotentiale, auf der Grundlage von Bedarfsanmeldungen der in der ARD zusammengeschlossenen Landesrundfunkanstalten, des ZDF und der Körperschaft des öffentlichen Rechts „Deutschlandradio“ durch die unabhängige KEF geprüft und ermittelt.

(2) Bei der Überprüfung und Ermittlung des Finanzbedarfs sind insbesondere zugrunde zu legen

1.  die wettbewerbsfähige Fortführung der bestehenden Rundfunkprogramme sowie die durch Staatsvertrag aller Länder zugelassenen Fernsehprogramme (bestandsbezogener Bedarf),
2.  nach Landesrecht zulässige neue Rundfunkprogramme, die Teilhabe an den neuen rundfunktechnischen Möglichkeiten in der Herstellung und zur Verbreitung von Rundfunkprogrammen sowie die Möglichkeit der Veranstaltung neuer Formen von Rundfunk (Entwicklungsbedarf),
3.  die allgemeine Kostenentwicklung und die besondere Kostenentwicklung im Medienbereich,
4.  die Entwicklung der Beitragserträge, der Werbeerträge und der sonstigen Erträge,
5.  die Anlage, Verzinsung und zweckbestimmte Verwendung der Überschüsse, die dadurch entstehen, dass die jährlichen Gesamterträge der in der ARD zusammengeschlossenen Landesrundfunkanstalten, des ZDF oder des Deutschlandradios die Gesamtaufwendungen für die Erfüllung ihres Auftrags übersteigen.

(3) Bei der Überprüfung und Ermittlung des Finanzbedarfs soll ein hoher Grad der Objektivierbarkeit erreicht werden.

(4) Die Beitragsfestsetzung erfolgt durch Staatsvertrag.

##### § 37  
Berichterstattung der Rechnungshöfe

Der für die Durchführung der Prüfung zuständige Rechnungshof teilt das Ergebnis der Prüfung einer Landesrundfunkanstalt, des ZDF oder des Deutschlandradios einschließlich deren Beteiligungsunternehmen dem jeweils zuständigen Intendanten, den jeweils zuständigen Aufsichtsgremien der Rundfunkanstalt und der Geschäftsführung des geprüften Beteiligungsunternehmens sowie der KEF mit.
Er gibt dem Intendanten der jeweiligen Rundfunkanstalt und der Geschäftsführung des Beteiligungsunternehmens Gelegenheit zur Stellungnahme zu dem Ergebnis der Prüfung und berücksichtigt die Stellungnahmen.
Den auf dieser Grundlage erstellten abschließenden Bericht über das Ergebnis der Prüfung teilt der zuständige Rechnungshof den Landtagen und den Landesregierungen der die Rundfunkanstalt tragenden Länder sowie der KEF mit und veröffentlicht ihn anschließend.
Dabei hat der Rechnungshof darauf zu achten, dass die Wettbewerbsfähigkeit des geprüften Beteiligungsunternehmens nicht beeinträchtigt wird und insbesondere Betriebs- und Geschäftsgeheimnisse gewahrt werden.

##### § 38  
Zulässige Produktplatzierung

Über die Anforderungen nach § 8 Abs.
7 Satz 2 hinaus ist Produktplatzierung in Kinofilmen, Filmen und Serien, Sportsendungen und Sendungen der leichten Unterhaltung nur dann zulässig,

1.  wenn diese nicht vom Veranstalter selbst oder von einem mit dem Veranstalter verbundenen Unternehmen produziert oder in Auftrag gegeben wurden oder
2.  wenn kein Entgelt geleistet wird, sondern lediglich bestimmte Waren oder Dienstleistungen, wie Produktionshilfen und Preise, im Hinblick auf ihre Einbeziehung in eine Sendung kostenlos bereitgestellt werden.

Keine Sendungen der leichten Unterhaltung sind insbesondere Sendungen, die neben unterhaltenden Elementen im Wesentlichen informierenden Charakter haben, und Ratgebersendungen mit Unterhaltungselementen.

##### § 39  
Dauer der Rundfunkwerbung, Sponsoring

(1) Die Gesamtdauer der Rundfunkwerbung beträgt im Ersten Fernsehprogramm der ARD und im Programm „Zweites Deutsches Fernsehen“ jeweils höchstens 20 Minuten werktäglich im Jahresdurchschnitt.
Nicht angerechnet werden auf die zulässigen Werbezeiten Sendezeiten mit Produktplatzierungen und Sponsorhinweise.
Nicht vollständig genutzte Werbezeit darf höchstens bis zu fünf Minuten werktäglich nachgeholt werden.
Nach 20.00 Uhr sowie an Sonntagen und im ganzen Bundesgebiet anerkannten Feiertagen dürfen Werbesendungen nicht ausgestrahlt werden.
§ 46 bleibt unberührt.

(2) In weiteren Fernsehprogrammen von ARD und ZDF sowie in den Dritten Fernsehprogrammen findet Rundfunkwerbung nicht statt.

(3) Im Fernsehen darf die Dauer der Spotwerbung innerhalb eines Zeitraums von einer Stunde 20 vom Hundert nicht überschreiten.

(4) Hinweise der Rundfunkanstalten auf Sendungen, Rundfunkprogramme oder rundfunkähnliche Telemedien des öffentlich-rechtlichen Rundfunks und auf Begleitmaterialien, die direkt von diesen Programmen und Sendungen abgeleitet sind, unentgeltliche Beiträge im Dienst der Öffentlichkeit einschließlich von Spendenaufrufen zu Wohlfahrtszwecken, gesetzliche Pflichthinweise und neutrale Einzelbilder zwischen redaktionellen Inhalten und Fernsehwerbe- oder Teleshoppingspots sowie zwischen einzelnen Spots gelten nicht als Werbung.

(5) Die Länder sind berechtigt, den Landesrundfunkanstalten bis zu 90 Minuten werktäglich im Jahresdurchschnitt Werbung im Hörfunk einzuräumen; ein am 1.
Januar 1987 in den Ländern abweichender zeitlicher Umfang der Rundfunkwerbung und ihre tageszeitliche Begrenzung kann beibehalten werden.

(6) Sponsoring findet nach 20.00 Uhr sowie an Sonntagen und im ganzen Bundesgebiet anerkannten Feiertagen im Fernsehen nicht statt; dies gilt nicht für das Sponsoring der Übertragung von Großereignissen nach § 13 Abs.
2.

##### § 40  
Kommerzielle Tätigkeiten

(1) Die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und das Deutschlandradio sind berechtigt, kommerzielle Tätigkeiten auszuüben.
Kommerzielle Tätigkeiten sind Betätigungen, bei denen Leistungen auch für Dritte im Wettbewerb angeboten werden, insbesondere Werbung und Sponsoring, Verwertungsaktivitäten, Merchandising, Produktion für Dritte und die Vermietung von Senderstandorten an Dritte.
Diese Tätigkeiten dürfen nur unter Marktbedingungen erbracht werden.
Die kommerziellen Tätigkeiten sind durch rechtlich selbständige Tochtergesellschaften zu erbringen.
Bei geringer Marktrelevanz kann eine kommerzielle Tätigkeit durch die Rundfunkanstalt selbst erbracht werden; in diesem Fall ist eine getrennte Buchführung vorzusehen.
Die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und das Deutschlandradio haben sich bei den Beziehungen zu ihren kommerziell tätigen Tochterunternehmen marktkonform zu verhalten und die entsprechenden Bedingungen, wie bei einer kommerziellen Tätigkeit, auch ihnen gegenüber einzuhalten.

(2) Die Tätigkeitsbereiche sind von den zuständigen Gremien der Rundfunkanstalten vor Aufnahme der Tätigkeit zu genehmigen.
Die Prüfung umfasst folgende Punkte:

1.  die Beschreibung der Tätigkeit nach Art und Umfang, die die Einhaltung der marktkonformen Bedingungen begründet (Marktkonformität) einschließlich eines Fremdvergleichs,
2.  den Vergleich mit Angeboten privater Konkurrenten,
3.  Vorgaben für eine getrennte Buchführung und
4.  Vorgaben für eine effiziente Kontrolle.

##### § 41  
Beteiligung an Unternehmen

(1) An einem Unternehmen, das einen gewerblichen oder sonstigen wirtschaftlichen Zweck zum Gegenstand hat, dürfen sich die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und das Deutschlandradio unmittelbar oder mittelbar beteiligen, wenn

1.  dies im sachlichen Zusammenhang mit ihren gesetzlichen Aufgaben steht,
2.  das Unternehmen die Rechtsform einer juristischen Person besitzt und
3.  die Satzung oder der Gesellschaftsvertrag des Unternehmens einen Aufsichtsrat oder ein entsprechendes Organ vorsieht.

Die Voraussetzungen nach Satz 1 müssen nicht erfüllt sein, wenn die Beteiligung nur vorübergehend eingegangen wird und unmittelbaren Programmzwecken dient.

(2) Bei Beteiligungsunternehmen haben sich die Rundfunkanstalten in geeigneter Weise den nötigen Einfluss auf die Geschäftsleitung des Unternehmens, insbesondere eine angemessene Vertretung im Aufsichtsgremium, zu sichern.
Eine Prüfung der Betätigung der Anstalten bei dem Unternehmen unter Beachtung kaufmännischer Grundsätze durch einen Wirtschaftsprüfer ist auszubedingen.

(3) Die Absätze 1 und 2 gelten entsprechend für juristische Personen des Privatrechts, die von den Rundfunkanstalten gegründet werden und deren Geschäftsanteile sich ausschließlich in ihrer Hand befinden.

(4) Die Absätze 1 und 2 gelten entsprechend für Beteiligungen der Rundfunkanstalten an gemeinnützigen Rundfunkunternehmen und Pensionskassen.

##### § 42  
Kontrolle der Beteiligung an Unternehmen

(1) Die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und das Deutschlandradio haben ein effektives Controlling über ihre Beteiligungen nach § 41 einzurichten.
Der Intendant hat das jeweils zuständige Aufsichtsgremium der Rundfunkanstalt regelmäßig über die wesentlichen Vorgänge in den Beteiligungsunternehmen, insbesondere über deren finanzielle Entwicklung, zu unterrichten.

(2) Der Intendant hat dem jeweils zuständigen Aufsichtsgremium jährlich einen Beteiligungsbericht vorzulegen.
Dieser Bericht schließt folgende Bereiche ein:

1.  die Darstellung sämtlicher unmittelbarer und mittelbarer Beteiligungen und ihrer wirtschaftlichen Bedeutung für die Rundfunkanstalt,
2.  die gesonderte Darstellung der Beteiligungen mit kommerziellen Tätigkeiten und den Nachweis der Erfüllung der staatsvertraglichen Vorgaben für kommerzielle Tätigkeiten und
3.  die Darstellung der Kontrolle der Beteiligungen einschließlich von Vorgängen mit besonderer Bedeutung.

Der Bericht ist den jeweils zuständigen Rechnungshöfen und der rechtsaufsichtsführenden Landesregierung zu übermitteln.

(3) Die für die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und das Deutschlandradio zuständigen Rechnungshöfe prüfen die Wirtschaftsführung bei solchen Unternehmen des Privatrechts, an denen die Anstalten unmittelbar, mittelbar, auch zusammen mit anderen Anstalten oder Körperschaften des öffentlichen Rechts, mit Mehrheit beteiligt sind und deren Gesellschaftsvertrag oder Satzung diese Prüfungen durch die Rechnungshöfe vorsieht.
Die Anstalten sind verpflichtet, für die Aufnahme der erforderlichen Regelungen in den Gesellschaftsvertrag oder die Satzung des Unternehmens zu sorgen.

(4) Sind mehrere Rechnungshöfe für die Prüfung zuständig, können sie die Prüfung einem dieser Rechnungshöfe übertragen.

##### § 43  
Kontrolle der kommerziellen Tätigkeiten

(1) Bei Mehrheitsbeteiligungen im Sinne von § 42 Abs.
3 der in der ARD zusammengeschlossenen Landesrundfunkanstalten, des ZDF und des Deutschlandradios oder bei Gesellschaften, bei denen ein Prüfungsrecht der zuständigen Rechnungshöfe besteht, sind die Rundfunkanstalten zusätzlich zu den allgemein bestehenden Prüfungsrechten der Rechnungshöfe verpflichtet darauf hinzuwirken, dass die Beteiligungsunternehmen den jährlichen Abschlussprüfer nur im Einvernehmen mit den zuständigen Rechnungshöfen bestellen.
Die Rundfunkanstalten haben dafür Sorge zu tragen, dass das Beteiligungsunternehmen vom Abschlussprüfer im Rahmen der Prüfung des Jahresabschlusses auch die Marktkonformität seiner kommerziellen Tätigkeiten auf der Grundlage zusätzlicher von den jeweils zuständigen Rechnungshöfen festzulegender Fragestellungen prüfen lässt und den Abschlussprüfer ermächtigt, das Ergebnis der Prüfung zusammen mit dem Abschlussbericht den zuständigen Rechnungshöfen mitzuteilen.
Diese Fragestellungen werden von dem für die Prüfung zuständigen Rechnungshof festgelegt und umfassen insbesondere den Nachweis der Einhaltung der staatsvertraglichen Vorgaben für kommerzielle Aktivitäten.
Die Rundfunkanstalten sind verpflichtet, für die Aufnahme der erforderlichen Regelungen in den Gesellschaftsvertrag oder die Satzung des Beteiligungsunternehmens zu sorgen.
Die Wirtschaftsprüfer testieren den Jahresabschluss der Beteiligungsunternehmen und berichten den zuständigen Rechnungshöfen auch hinsichtlich der in Satz 2 und 3 genannten Fragestellungen.
Sie teilen das Ergebnis und den Abschlussbericht den zuständigen Rechnungshöfen mit.
Die zuständigen Rechnungshöfe werten die Prüfung aus und können in jedem Einzelfall selbst Prüfmaßnahmen bei den betreffenden Beteiligungsunternehmen ergreifen.
Die durch die ergänzenden Prüfungen zusätzlich entstehenden Kosten tragen die jeweiligen Beteiligungsunternehmen.

(2) Bei kommerziellen Tätigkeiten mit geringer Marktrelevanz nach § 40 Abs.
1 Satz 5 sind die Rundfunkanstalten auf Anforderung des zuständigen Rechnungshofes verpflichtet, für ein dem Absatz 1 Satz 2, 3 und 5 bis 8 entsprechendes Verfahren Sorge zu tragen.
Werden Verstöße gegen die Bestimmungen zur Marktkonformität bei Prüfungen von Beteiligungsunternehmen oder der Rundfunkanstalten selbst festgestellt, findet auf die Mitteilung des Ergebnisses § 37 Anwendung.

##### § 44  
Haftung für kommerziell tätige Beteiligungsunternehmen

Für kommerziell tätige Beteiligungsunternehmen dürfen die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und das Deutschlandradio keine Haftung übernehmen.

##### § 45  
Richtlinien

Die in der ARD zusammengeschlossenen Landesrundfunkanstalten und das ZDF erlassen Richtlinien zur Durchführung der §§ 8 bis 11, 38 und 39.
In der Richtlinie zu § 11 sind insbesondere die Bedingungen zur Teilnahme Minderjähriger näher zu bestimmen.
Die in der ARD zusammengeschlossenen Landesrundfunkanstalten und das ZDF stellen hierzu das Benehmen mit den Landesmedienanstalten her und führen einen gemeinsamen Erfahrungsaustausch in der Anwendung dieser Richtlinien durch.
In der Richtlinie zu § 8 Abs.
7 und § 38 ist näher zu bestimmen, unter welchen Voraussetzungen, in welchen Formaten und in welchem Umfang unentgeltliche Produktplatzierung stattfinden kann, wie die Unabhängigkeit der Produzenten und Redaktionen gesichert und eine ungebührliche Herausstellung des Produkts vermieden wird.
Die Sätze 1 bis 4 gelten für die Richtlinien des Deutschlandradios zur Durchführung der §§ 8, 11 und 38 entsprechend.

##### § 46  
Änderung der Werbung

Die Länder können Änderungen der Gesamtdauer der Werbung, der tageszeitlichen Begrenzung der Werbung und ihrer Beschränkung auf Werktage im öffentlich-rechtlichen Rundfunk vereinbaren.

##### § 47  
Ausschluss von Teleshopping

Teleshopping findet mit Ausnahme von Teleshopping-Spots im öffentlich-rechtlichen Rundfunk nicht statt.

##### § 48  
Versorgungsauftrag

Die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF und das Deutschlandradio können ihrem gesetzlichen Auftrag durch Nutzung geeigneter Übertragungswege nachkommen.
Bei der Auswahl des Übertragungswegs sind die Grundsätze der Wirtschaftlichkeit und Sparsamkeit zu beachten.
Die analoge Verbreitung bisher ausschließlich digital verbreiteter Programme ist unzulässig.

##### § 49  
Veröffentlichung von Beanstandungen

Die zuständigen Aufsichtsgremien der in der ARD zusammengeschlossenen Landesrundfunkanstalten, des ZDF und des Deutschlandradios können vom Intendanten verlangen, dass er bei Rechtsverstößen Beanstandungen der Gremien im Programm veröffentlicht.

### IV.
Abschnitt  
Besondere Bestimmungen für den privaten Rundfunk

#### 1.
Unterabschnitt  
Anwendungsbereich, Programmgrundsätze

##### § 50  
Anwendungsbereich

Die §§ 51, 53 bis 68 gelten nur für bundesweit ausgerichtete Angebote.
Die §§ 52 bis 55 Abs.
1 und § 58 gelten auch für Teleshoppingkanäle.
Eine abweichende Regelung durch Landesrecht ist nicht zulässig.
Die Entscheidungen der Kommission zur Ermittlung der Konzentration im Medienbereich (KEK, § 104 Abs.
2 Satz 1 Nr.
3) sind den Zuweisungen von Übertragungskapazitäten nach diesem Staatsvertrag und durch die zuständige Landesmedienanstalt auch bei der Entscheidung über die Zuweisung von Übertragungskapazitäten nach Landesrecht zugrunde zu legen.

##### § 51  
Programmgrundsätze

(1) Für die Rundfunkprogramme gilt die verfassungsmäßige Ordnung.
Die Rundfunkprogramme haben die Würde des Menschen sowie die sittlichen, religiösen und weltanschaulichen Überzeugungen anderer zu achten.
Sie sollen die Zusammengehörigkeit im vereinten Deutschland sowie die internationale Verständigung fördern und auf ein diskriminierungsfreies Miteinander hinwirken.
Die Vorschriften der allgemeinen Gesetze und die gesetzlichen Bestimmungen zum Schutz der persönlichen Ehre sind einzuhalten.

(2) Die Rundfunkvollprogramme sollen zur Darstellung der Vielfalt im deutschsprachigen und europäischen Raum mit einem angemessenen Anteil an Information, Kultur und Bildung beitragen; die Möglichkeit, Spartenprogramme anzubieten, bleibt hiervon unberührt.

#### 2.
Unterabschnitt  
Zulassung

##### § 52  
Grundsatz

(1) Private Veranstalter bedürfen zur Veranstaltung von Rundfunkprogrammen einer Zulassung.
§ 54 bleibt unberührt.
Die Zulassung eines Veranstalters nicht bundesweit ausgerichteten Rundfunks richtet sich nach Landesrecht.
Für die Zulassung eines Veranstalters bundesweit ausgerichteten Rundfunks gelten die Vorschriften dieses Unterabschnitts; im Übrigen gilt Landesrecht.

(2) Die Zulassung eines Fernsehveranstalters kann versagt oder widerrufen werden, wenn

1.  sich das Programm des Veranstalters ganz oder in wesentlichen Teilen an die Bevölkerung eines anderen Staates richtet, der das Europäische Übereinkommen über das grenzüberschreitende Fernsehen ratifiziert hat und
2.  der Veranstalter sich zu dem Zweck in Deutschland niedergelassen hat, die Bestimmungen des anderen Staates zu umgehen und
3.  die Bestimmungen des anderen Staates, die der Veranstalter zu umgehen bezweckt, Gegenstand des Europäischen Übereinkommens über das grenzüberschreitende Fernsehen sind.

Statt der Versagung oder des Widerrufs der Zulassung kann diese auch mit Nebenbestimmungen versehen werden, soweit dies ausreicht, die Umgehung nach Satz 1 auszuschließen.

##### § 53  
Erteilung einer Zulassung für Veranstalter von bundesweit ausgerichtetem Rundfunk

(1) Eine Zulassung darf nur an eine natürliche oder juristische Person erteilt werden, die

1.  unbeschränkt geschäftsfähig ist,
2.  die Fähigkeit, öffentliche Ämter zu bekleiden, nicht durch Richterspruch verloren hat,
3.  das Grundrecht der freien Meinungsäußerung nicht nach Artikel 18 des Grundgesetzes verwirkt hat,
4.  als Vereinigung nicht verboten ist,
5.  ihren Wohnsitz oder Sitz in Deutschland, einem sonstigen Mitgliedstaat der Europäischen Union oder einem anderen Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum hat und gerichtlich verfolgt werden kann und
6.  die Gewähr dafür bietet, dass sie unter Beachtung der gesetzlichen Vorschriften und der auf dieser Grundlage erlassenen Verwaltungsakte Rundfunk veranstaltet.

(2) Die Voraussetzungen nach Absatz 1 Nr.
1 bis 3 und Nr.
6 müssen bei juristischen Personen von den gesetzlichen oder satzungsmäßigen Vertretern erfüllt sein.
Einem Veranstalter in der Rechtsform einer Aktiengesellschaft darf nur dann eine Zulassung erteilt werden, wenn in der Satzung der Aktiengesellschaft bestimmt ist, dass die Aktien nur als Namensaktien oder als Namensaktien und stimmrechtslose Vorzugsaktien ausgegeben werden dürfen.

(3) Eine Zulassung darf nicht erteilt werden an juristische Personen des öffentlichen Rechts mit Ausnahme von Kirchen und Hochschulen, an deren gesetzliche Vertreter und leitende Bedienstete sowie an politische Parteien und Wählervereinigungen.
Gleiches gilt für Unternehmen, die im Verhältnis eines verbundenen Unternehmens im Sinne des § 15 des Aktiengesetzes zu den in Satz 1 Genannten stehen.
Die Sätze 1 und 2 gelten für ausländische öffentliche oder staatliche Stellen entsprechend.

##### § 54  
Zulassungsfreie Rundfunkprogramme

(1) Keiner Zulassung bedürfen Rundfunkprogramme,

1.  die nur geringe Bedeutung für die individuelle und öffentliche Meinungsbildung entfalten, oder
2.  die im Durchschnitt von sechs Monaten weniger als 20.000 gleichzeitige Nutzer erreichen oder in ihrer prognostizierten Entwicklung erreichen werden.

Die zuständige Landesmedienanstalt bestätigt die Zulassungsfreiheit auf Antrag durch Unbedenklichkeitsbescheinigung.

(2) Die Landesmedienanstalten regeln das Nähere zur Konkretisierung der Zulassungsfreiheit nach Absatz 1 durch Satzung.

(3) Vor dem Inkrafttreten dieses Staatsvertrages angezeigte, ausschließlich im Internet verbreitete Hörfunkprogramme gelten als zugelassene Programme nach § 52.

(4) Auf zulassungsfreie Rundfunkprogramme finden die Vorschriften der §§ 15, 57 und 68 keine Anwendung.
§ 53 findet mit Ausnahme seines Absatzes 1 Nr.
1 entsprechende Anwendung.
Die zuständige Landesmedienanstalt kann von Veranstaltern von Rundfunkprogrammen im Sinne des Absatzes 1 die in den §§ 55 und 56 genannten Informationen und Unterlagen verlangen.

##### § 55  
Grundsätze für das Zulassungsverfahren

(1) In dem Zulassungsantrag sind Name und Anschrift des Antragstellers, Programminhalt, Programmkategorie (Voll- oder Spartenprogramm), Programmdauer, Übertragungstechnik und geplantes Verbreitungsgebiet anzugeben.

(2) Sofern erforderlich, hat die zuständige Landesmedienanstalt Auskunft und die Vorlage weiterer Unterlagen zu verlangen, die sich insbesondere erstrecken auf

1.  eine Darstellung der unmittelbaren und mittelbaren Beteiligungen im Sinne des § 62 an dem Antragsteller sowie der Kapital- und Stimmrechtsverhältnisse bei dem Antragsteller und den mit ihm im Sinne des Aktiengesetzes verbundenen Unternehmen,
2.  die Angabe über Angehörige im Sinne des § 15 der Abgabenordnung unter den Beteiligten nach Nummer 1, gleiches gilt für Vertreter der Person oder Personengesellschaft oder des Mitglieds eines Organs einer juristischen Person,
3.  den Gesellschaftsvertrag und die satzungsrechtlichen Bestimmungen des Antragstellers,
4.  Vereinbarungen, die zwischen an dem Antragsteller unmittelbar oder mittelbar im Sinne des § 62 Beteiligten bestehen und sich auf die gemeinsame Veranstaltung von Rundfunk sowie auf Treuhandverhältnisse und nach den §§ 60 und 62 erhebliche Beziehungen beziehen,
5.  eine schriftliche Erklärung des Antragstellers, dass die nach den Nummern 1 bis 4 vorgelegten Unterlagen und Angaben vollständig sind.

(3) Ist für die Prüfung im Rahmen des Zulassungsverfahrens ein Sachverhalt bedeutsam, der sich auf Vorgänge außerhalb des Geltungsbereichs dieses Staatsvertrages bezieht, hat der Antragsteller diesen Sachverhalt aufzuklären und die erforderlichen Beweismittel zu beschaffen.
Er hat dabei alle für ihn bestehenden rechtlichen und tatsächlichen Möglichkeiten auszuschöpfen.
Der Antragsteller kann sich nicht darauf berufen, dass er Sachverhalte nicht aufklären oder Beweismittel nicht beschaffen kann, wenn er sich nach Lage des Falles bei der Gestaltung seiner Verhältnisse die Möglichkeit dazu hätte beschaffen oder einräumen lassen können.

(4) Die Verpflichtungen nach den Absätzen 1 bis 3 gelten für natürliche und juristische Personen oder Personengesellschaften, die an dem Antragsteller unmittelbar oder mittelbar im Sinne von § 62 beteiligt sind oder zu ihm im Verhältnis eines verbundenen Unternehmens stehen oder sonstige Einflüsse im Sinne der §§ 60 und 62 auf ihn ausüben können, entsprechend.

(5) Kommt ein Auskunfts- oder Vorlagepflichtiger seinen Mitwirkungspflichten nach den Absätzen 1 bis 4 innerhalb einer von der zuständigen Landesmedienanstalt bestimmten Frist nicht nach, kann der Zulassungsantrag abgelehnt werden.

(6) Die im Rahmen des Zulassungsverfahrens Auskunfts- und Vorlagepflichtigen sind verpflichtet, jede Änderung der maßgeblichen Umstände nach Antragstellung oder nach Erteilung der Zulassung unverzüglich der zuständigen Landesmedienanstalt mitzuteilen.
Die Absätze 1 bis 5 finden entsprechende Anwendung.
§ 63 bleibt unberührt.

(7) Unbeschadet anderweitiger Anzeigepflichten sind der Veranstalter und die an ihm unmittelbar oder mittelbar im Sinne von § 62 Beteiligten jeweils nach Ablauf eines Kalenderjahres verpflichtet, unverzüglich der zuständigen Landesmedienanstalt gegenüber eine Erklärung darüber abzugeben, ob und inwieweit innerhalb des abgelaufenen Kalenderjahres bei den nach § 62 maßgeblichen Beteiligungs- und Zurechnungstatbeständen eine Veränderung eingetreten ist.

##### § 56  
Auskunftsrechte und Ermittlungsbefugnisse

(1) Die zuständige Landesmedienanstalt kann alle Ermittlungen durchführen und alle Beweise erheben, die zur Erfüllung ihrer sich aus den §§ 60 bis 67 und 120 ergebenden Aufgaben erforderlich sind.
Sie bedient sich der Beweismittel, die sie nach pflichtgemäßem Ermessen zur Ermittlung des Sachverhalts für erforderlich hält.
Sie kann insbesondere

1.  Auskünfte einholen,
2.  Beteiligte im Sinne des § 13 des Verwaltungsverfahrensgesetzes anhören, Zeugen und Sachverständige vernehmen oder die schriftliche Äußerung von Beteiligten, Sachverständigen und Zeugen einholen,
3.  Urkunden und Akten beiziehen,
4.  den Augenschein einnehmen.

Andere Personen als die Beteiligten sollen erst dann zur Auskunft herangezogen werden, wenn die Sachverhaltsaufklärung durch die Beteiligten nicht zum Ziel führt oder keinen Erfolg verspricht.

(2) Für Zeugen und Sachverständige besteht eine Pflicht zur Aussage oder zur Erstattung von Gutachten.
Die Vorschriften der Zivilprozessordnung über die Pflicht, als Zeuge auszusagen oder als Sachverständiger ein Gutachten zu erstatten, über die Ablehnung von Sachverständigen sowie über die Vernehmung von Angehörigen des öffentlichen Dienstes als Zeugen oder Sachverständige gelten entsprechend.
Die Entschädigung der Zeugen und Sachverständigen erfolgt in entsprechender Anwendung des Justizvergütungs- und -entschädigungsgesetzes.

(3) Zur Glaubhaftmachung der Vollständigkeit und Richtigkeit der Angaben darf die zuständige Landesmedienanstalt die Vorlage einer eidesstattlichen Versicherung von denjenigen verlangen, die nach § 55 Abs.
1 und 4 auskunfts- und vorlagepflichtig sind.
Eine Versicherung an Eides statt soll nur gefordert werden, wenn andere Mittel zur Erforschung der Wahrheit nicht vorhanden sind, zu keinem Ergebnis geführt haben oder einen unverhältnismäßigen Aufwand erfordern.

(4) Die von der zuständigen Landesmedienanstalt mit der Durchführung der sich aus den §§ 60 bis 67 und § 120 ergebenden Aufgaben betrauten Personen dürfen während der üblichen Geschäfts- und Arbeitszeiten die Geschäftsräume und -grundstücke der in § 55 Abs.
1, 3 und 4 genannten Personen und Personengesellschaften betreten und die in Absatz 5 genannten Unterlagen einsehen und prüfen.
Das Grundrecht des Artikels 13 des Grundgesetzes wird insoweit eingeschränkt.

(5) Die in § 55 Abs.
1, 3 und 4 genannten Personen oder Personengesellschaften haben auf Verlangen Aufzeichnungen, Bücher, Geschäftspapiere und andere Urkunden, die für die Anwendung der §§ 60 bis 67 und § 120 erheblich sein können, vorzulegen, Auskünfte zu erteilen und die sonst zur Durchführung der Maßnahmen nach Absatz 4 erforderlichen Hilfsdienste zu leisten.
Vorkehrungen, die die Maßnahmen hindern oder erschweren, sind unzulässig.

(6) Der zur Erteilung einer Auskunft Verpflichtete kann die Auskunft auf solche Fragen verweigern, deren Beantwortung ihn selbst oder einen der in § 383 Abs.
1 Nr.
1 bis 3 der Zivilprozessordnung bezeichneten Angehörigen der Gefahr strafrechtlicher Verfolgung oder eines Verfahrens nach dem Gesetz über Ordnungswidrigkeiten aussetzen würde.

(7) Durchsuchungen dürfen nur aufgrund einer Anordnung des Amtsrichters, in dessen Bezirk die Durchsuchung erfolgen soll, vorgenommen werden.
Bei Gefahr im Verzug können die in Absatz 4 bezeichneten betrauten Personen während der Geschäftszeit die erforderlichen Durchsuchungen ohne richterliche Anordnung vornehmen.
An Ort und Stelle ist eine Niederschrift über Grund, Zeit und Ort der Durchsuchung und ihr wesentliches Ergebnis aufzunehmen, aus der sich, falls keine richterliche Anordnung ergangen ist, auch die Tatsachen ergeben, die zur Annahme einer Gefahr im Verzug geführt haben.

(8) Der Inhaber der tatsächlichen Gewalt über die zu durchsuchenden Räume darf der Durchsuchung beiwohnen.
Ist er abwesend, soll sein Vertreter oder ein anderer Zeuge hinzugezogen werden.
Dem Inhaber der tatsächlichen Gewalt über die durchsuchten Räume oder seinem Vertreter ist auf Verlangen eine Durchschrift der in Absatz 7 Satz 3 genannten Niederschrift zu erteilen.

##### § 57  
Publizitätspflicht und sonstige Vorlagepflichten

(1) Jeder Veranstalter hat unabhängig von seiner Rechtsform jährlich nach Maßgabe der Vorschriften des Handelsgesetzbuches, die für große Kapitalgesellschaften gelten, einen Jahresabschluss samt Anhang und einen Lagebericht spätestens bis zum Ende des neunten auf das Ende des Geschäftsjahres folgenden Monats zu erstellen und bekannt zu machen.
Satz 1 findet auf an dem Veranstalter unmittelbar Beteiligte, denen das Programm des Veranstalters nach § 62 Abs.
1 Satz 1, und mittelbar Beteiligte, denen das Programm nach § 62 Abs.
1 Satz 2 zuzurechnen ist, entsprechende Anwendung.

(2) Innerhalb derselben Frist hat der Veranstalter eine Aufstellung der Programmbezugsquellen für den Berichtszeitraum der zuständigen Landesmedienanstalt vorzulegen.

##### § 58  
Vertraulichkeit

Jenseits des Anwendungsbereichs der Verordnung (EU) 2016/679 dürfen Angaben über persönliche und sachliche Verhältnisse einer natürlichen oder juristischen Person oder einer Personengesellschaft sowie Betriebs- oder Geschäftsgeheimnisse, die den Landesmedienanstalten, ihren Organen, ihren Bediensteten oder von ihnen beauftragten Dritten im Rahmen der Durchführung ihrer Aufgaben anvertraut oder sonst bekannt geworden sind, nicht unbefugt offenbart werden.

#### 3.
Unterabschnitt  
Sicherung der Meinungsvielfalt

##### § 59  
Meinungsvielfalt, regionale Fenster

(1) Im privaten Rundfunk ist inhaltlich die Vielfalt der Meinungen im Wesentlichen zum Ausdruck zu bringen.
Die bedeutsamen politischen, weltanschaulichen und gesellschaftlichen Kräfte und Gruppen müssen in den Vollprogrammen angemessen zu Wort kommen; Auffassungen von Minderheiten sind zu berücksichtigen.
Die Möglichkeit, Spartenprogramme anzubieten, bleibt hiervon unberührt.

(2) Ein einzelnes Programm darf die Bildung der öffentlichen Meinung nicht in hohem Maße ungleichgewichtig beeinflussen.

(3) Im Rahmen des Zulassungsverfahrens soll die Landesmedienanstalt darauf hinwirken, dass an dem Veranstalter auch Interessenten mit kulturellen Programmbeiträgen beteiligt werden.
Ein Rechtsanspruch auf Beteiligung besteht nicht.

(4) In den beiden bundesweit verbreiteten reichweitenstärksten Fernsehvollprogrammen sind mindestens im zeitlichen und regional differenzierten Umfang der Programmaktivitäten zum 1. Juli 2002 nach Maßgabe des jeweiligen Landesrechts Fensterprogramme zur aktuellen und authentischen Darstellung der Ereignisse des politischen, wirtschaftlichen, sozialen und kulturellen Lebens in dem jeweiligen Land aufzunehmen.
Der Hauptprogrammveranstalter hat organisatorisch sicherzustellen, dass die redaktionelle Unabhängigkeit des Fensterprogrammveranstalters gewährleistet ist.
Dem Fensterprogrammveranstalter ist eine gesonderte Zulassung zu erteilen.
Fensterprogrammveranstalter und Hauptprogrammveranstalter sollen zueinander nicht im Verhältnis eines verbundenen Unternehmens nach § 62 stehen, es sei denn, zum 31.
Dezember 2009 bestehende landesrechtliche Regelungen stellen die Unabhängigkeit in anderer Weise sicher.
Zum 31. Dezember 2009 bestehende Zulassungen bleiben unberührt.
Eine Verlängerung ist zulässig.
Mit der Organisation der Fensterprogramme ist zugleich deren Finanzierung durch den Hauptprogrammveranstalter sicherzustellen.
Die Landesmedienanstalten stimmen die Organisation der Fensterprogramme in zeitlicher und technischer Hinsicht unter Berücksichtigung der Interessen der betroffenen Veranstalter ab.

##### § 60  
Sicherung der Meinungsvielfalt im Fernsehen

(1) Ein Unternehmen (natürliche oder juristische Person oder Personenvereinigung) darf in Deutschland selbst oder durch ihm zurechenbare Unternehmen bundesweit im Fernsehen eine unbegrenzte Anzahl von Programmen veranstalten, es sei denn, es erlangt dadurch vorherrschende Meinungsmacht nach Maßgabe der nachfolgenden Bestimmungen.

(2) Erreichen die einem Unternehmen zurechenbaren Programme im Durchschnitt eines Jahres einen Zuschaueranteil von 30 vom Hundert, so wird vermutet, dass vorherrschende Meinungsmacht gegeben ist.
Gleiches gilt bei Erreichen eines Zuschaueranteils von 25 vom Hundert, sofern das Unternehmen auf einem medienrelevanten verwandten Markt eine marktbeherrschende Stellung hat oder eine Gesamtbeurteilung seiner Aktivitäten im Fernsehen und auf medienrelevanten verwandten Märkten ergibt, dass der dadurch erzielte Meinungseinfluss dem eines Unternehmens mit einem Zuschaueranteil von 30 vom Hundert im Fernsehen entspricht.
Bei der Berechnung des nach Satz 2 maßgeblichen Zuschaueranteils kommen vom tatsächlichen Zuschaueranteil zwei Prozentpunkte in Abzug, wenn in dem dem Unternehmen zurechenbaren Vollprogramm mit dem höchsten Zuschaueranteil Fensterprogramme gemäß § 59 Abs.
4 aufgenommen sind; bei gleichzeitiger Aufnahme von Sendezeit für Dritte nach Maßgabe des Absatzes 5 kommen vom tatsächlichen Zuschaueranteil weitere drei Prozentpunkte in Abzug.

(3) Hat ein Unternehmen mit den ihm zurechenbaren Programmen vorherrschende Meinungsmacht erlangt, darf für weitere diesem Unternehmen zurechenbare Programme keine Zulassung erteilt oder der Erwerb weiterer zurechenbarer Beteiligungen an Veranstaltern nicht als unbedenklich bestätigt werden.

(4) Hat ein Unternehmen mit den ihm zurechenbaren Programmen vorherrschende Meinungsmacht erlangt, schlägt die zuständige Landesmedienanstalt durch die KEK dem Unternehmen folgende Maßnahmen vor:

1.  das Unternehmen kann ihm zurechenbare Beteiligungen an Veranstaltern aufgeben, bis der zurechenbare Zuschaueranteil des Unternehmens hierdurch unter die Grenze nach Absatz 2 Satz 1 fällt, oder
2.  es kann im Falle des Absatzes 2 Satz 2 seine Marktstellung auf medienrelevanten verwandten Märkten vermindern oder ihm zurechenbare Beteiligungen an Veranstaltern aufgeben, bis keine vorherrschende Meinungsmacht nach Absatz 2 Satz 2 mehr gegeben ist, oder
3.  es kann bei ihm zurechenbaren Veranstaltern vielfaltssichernde Maßnahmen im Sinne der §§ 64 bis 66 ergreifen.

Die KEK erörtert mit dem Unternehmen die in Betracht kommenden Maßnahmen mit dem Ziel, eine einvernehmliche Regelung herbeizuführen.
Kommt keine Einigung zustande oder werden die einvernehmlich zwischen dem Unternehmen und der KEK vereinbarten Maßnahmen nicht in angemessener Frist durchgeführt, sind von der zuständigen Landesmedienanstalt nach Feststellung durch die KEK die Zulassungen von so vielen dem Unternehmen zurechenbaren Programmen zu widerrufen, bis keine vorherrschende Meinungsmacht durch das Unternehmen mehr gegeben ist.
Die Auswahl trifft die KEK unter Berücksichtigung der Besonderheiten des Einzelfalles.
Eine Entschädigung für Vermögensnachteile durch den Widerruf der Zulassung wird nicht gewährt.

(5) Erreicht ein Veranstalter mit einem Vollprogramm oder einem Spartenprogramm mit Schwerpunkt Information im Durchschnitt eines Jahres einen Zuschaueranteil von 10 vom Hundert, hat er binnen sechs Monaten nach Feststellung und Mitteilung durch die zuständige Landesmedienanstalt Sendezeit für unabhängige Dritte nach Maßgabe von § 65 einzuräumen.
Erreicht ein Unternehmen mit ihm zurechenbaren Programmen im Durchschnitt eines Jahres einen Zuschaueranteil von 20 vom Hundert, ohne dass eines der Vollprogramme oder Spartenprogramme mit Schwerpunkt Information einen Zuschaueranteil von zehn vom Hundert erreicht, trifft die Verpflichtung nach Satz 1 den Veranstalter des dem Unternehmen zurechenbaren Programms mit dem höchsten Zuschaueranteil.
Trifft der Veranstalter die danach erforderlichen Maßnahmen nicht, ist von der zuständigen Landesmedienanstalt nach Feststellung durch die KEK die Zulassung zu widerrufen.
Absatz 4 Satz 5 gilt entsprechend.

(6) Die Landesmedienanstalten veröffentlichen gemeinsam alle drei Jahre oder auf Anforderung der Länder einen Bericht der KEK über die Entwicklung der Konzentration und über Maßnahmen zur Sicherung der Meinungsvielfalt im privaten Rundfunk unter Berücksichtigung von

1.  Verflechtungen zwischen Fernsehen und medienrelevanten verwandten Märkten,
2.  horizontalen Verflechtungen zwischen Rundfunkveranstaltern in verschiedenen Verbreitungsgebieten und
3.  internationalen Verflechtungen im Medienbereich.

Der Bericht soll auch zur Anwendung der §§ 60 bis 66 und zu erforderlichen Änderungen dieser Bestimmungen Stellung nehmen.

(7) Die Landesmedienanstalten veröffentlichen jährlich eine von der KEK zu erstellende Programmliste.
In die Programmliste sind alle Programme, ihre Veranstalter und deren Beteiligte aufzunehmen.

##### § 61  
Bestimmung der Zuschaueranteile

(1) Die Landesmedienanstalten ermitteln durch die KEK den Zuschaueranteil der jeweiligen Programme unter Einbeziehung aller deutschsprachigen Programme des öffentlich-rechtlichen Rundfunks und des bundesweit empfangbaren privaten Rundfunks.
Für Entscheidungen maßgeblich ist der bei Einleitung des Verfahrens im Durchschnitt der letzten zwölf Monate erreichte Zuschaueranteil der einzubeziehenden Programme.

(2) Die Landesmedienanstalten beauftragen nach Maßgabe einer Entscheidung der KEK ein Unternehmen zur Ermittlung der Zuschaueranteile; die Vergabe des Auftrags erfolgt nach den Grundsätzen von Wirtschaftlichkeit und Sparsamkeit.
Die Ermittlung muss aufgrund repräsentativer Erhebungen bei Zuschauern ab Vollendung des dritten Lebensjahres nach allgemein anerkannten wissenschaftlichen Methoden durchgeführt werden.
Die Landesmedienanstalten sollen mit dem Unternehmen vereinbaren, dass die anlässlich der Ermittlung der Zuschaueranteile nach Absatz 1 Satz 1 erhobenen Daten vertraglich auch von Dritten genutzt werden können.
In diesem Fall sind die auf die Landesmedienanstalten entfallenden Kosten entsprechend zu mindern.

(3) Die Veranstalter sind bei der Ermittlung der Zuschaueranteile zur Mitwirkung verpflichtet.
Kommt ein Veranstalter seiner Mitwirkungspflicht nicht nach, kann die Zulassung widerrufen werden.

##### § 62  
Zurechnung von Programmen

(1) Einem Unternehmen sind sämtliche Programme zuzurechnen, die es selbst veranstaltet oder die von einem anderen Unternehmen veranstaltet werden, an dem es unmittelbar mit 25 vom Hundert oder mehr an dem Kapital oder an den Stimmrechten beteiligt ist.
Ihm sind ferner alle Programme von Unternehmen zuzurechnen, an denen es mittelbar beteiligt ist, sofern diese Unternehmen zu ihm im Verhältnis eines verbundenen Unternehmens im Sinne von § 15 des Aktiengesetzes stehen und diese Unternehmen am Kapital oder an den Stimmrechten eines Veranstalters mit 25 vom Hundert oder mehr beteiligt sind.
Die im Sinne der Sätze 1 und 2 verbundenen Unternehmen sind als einheitliche Unternehmen anzusehen, und deren Anteile am Kapital oder an den Stimmrechten sind zusammenzufassen.
Wirken mehrere Unternehmen aufgrund einer Vereinbarung oder in sonstiger Weise derart zusammen, dass sie gemeinsam einen beherrschenden Einfluss auf ein beteiligtes Unternehmen ausüben können, so gilt jedes von ihnen als herrschendes Unternehmen.

(2) Einer Beteiligung nach Absatz 1 steht gleich, wenn ein Unternehmen allein oder gemeinsam mit anderen auf einen Veranstalter einen vergleichbaren Einfluss ausüben kann.
Als vergleichbarer Einfluss gilt auch, wenn ein Unternehmen oder ein ihm bereits aus anderen Gründen nach Absatz 1 oder Absatz 2 Satz 1 zurechenbares Unternehmen

1.  regelmäßig einen wesentlichen Teil der Sendezeit eines Veranstalters mit von ihm zugelieferten Programmteilen gestaltet oder
2.  aufgrund vertraglicher Vereinbarungen, satzungsrechtlicher Bestimmungen oder in sonstiger Weise eine Stellung innehat, die wesentliche Entscheidungen eines Veranstalters über die Programmgestaltung, den Programmeinkauf oder die Programmproduktion von seiner Zustimmung abhängig macht.

(3) Bei der Zurechnung nach den Absätzen 1 und 2 sind auch Unternehmen einzubeziehen, die ihren Sitz außerhalb des Geltungsbereichs dieses Staatsvertrages haben.

(4) Bei der Prüfung und Bewertung vergleichbarer Einflüsse auf einen Veranstalter sind auch bestehende Angehörigenverhältnisse einzubeziehen.
Hierbei finden die Grundsätze des Wirtschafts- und Steuerrechts Anwendung.

##### § 63  
Veränderung von Beteiligungsverhältnissen

Jede geplante Veränderung von Beteiligungsverhältnissen oder sonstigen Einflüssen ist bei der zuständigen Landesmedienanstalt vor ihrem Vollzug schriftlich anzumelden.
Anmeldepflichtig sind der Veranstalter und die an dem Veranstalter unmittelbar oder mittelbar im Sinne von § 62 Beteiligten.
Die Veränderungen dürfen nur dann von der zuständigen Landesmedienanstalt als unbedenklich bestätigt werden, wenn unter den veränderten Voraussetzungen eine Zulassung erteilt werden könnte.
Wird eine geplante Veränderung vollzogen, die nicht nach Satz 3 als unbedenklich bestätigt werden kann, ist die Zulassung zu widerrufen.
Für den Widerruf gilt § 108 Abs.
2 und 3.
Für geringfügige Veränderungen von Beteiligungsverhältnissen oder sonstigen Einflüssen kann die KEK durch Richtlinien Ausnahmen für die Anmeldepflicht vorsehen.

##### § 64  
Vielfaltssichernde Maßnahmen

Stellen die vorgenannten Vorschriften auf vielfaltssichernde Maßnahmen bei einem Veranstalter oder Unternehmen ab, gelten als solche Maßnahmen:

1.  die Einräumung von Sendezeit für unabhängige Dritte (§ 65),
2.  die Einrichtung eines Programmbeirats (§ 66).

##### § 65  
Sendezeit für unabhängige Dritte

(1) Ein Fensterprogramm, das aufgrund der Verpflichtung zur Einräumung von Sendezeit nach den vorstehenden Bestimmungen ausgestrahlt wird, muss unter Wahrung der Programmautonomie des Hauptveranstalters einen zusätzlichen Beitrag zur Vielfalt in dessen Programm, insbesondere in den Bereichen Kultur, Bildung und Information, leisten.
Die Gestaltung des Fensterprogramms hat in redaktioneller Unabhängigkeit vom Hauptprogramm zu erfolgen.

(2) Die Dauer des Fensterprogramms muss wöchentlich mindestens 260 Minuten, davon mindestens 75 Minuten in der Sendezeit von 19.00 Uhr bis 23.30 Uhr betragen.
Auf die wöchentliche Sendezeit werden Regionalfensterprogramme bis höchstens 150 Minuten pro Woche mit höchstens 80 Minuten pro Woche auf die Drittsendezeit außerhalb der in Satz 1 genannten Sendezeit angerechnet; bei einer geringeren wöchentlichen Sendezeit für das Regionalfenster vermindert sich die anrechenbare Sendezeit von 80 Minuten entsprechend.
Die Anrechnung ist nur zulässig, wenn die Regionalfensterprogramme in redaktioneller Unabhängigkeit veranstaltet werden und insgesamt bundesweit mindestens 50 vom Hundert der Fernsehhaushalte erreichen.
Eine Unterschreitung dieser Reichweite ist im Zuge der Digitalisierung der Übertragungswege zulässig.

(3) Der Fensterprogrammanbieter nach Absatz 1 darf nicht in einem rechtlichen Abhängigkeitsverhältnis zum Hauptprogrammveranstalter stehen.
Rechtliche Abhängigkeit im Sinne von Satz 1 liegt vor, wenn das Hauptprogramm und das Fensterprogramm nach § 62 demselben Unternehmen zugerechnet werden können.

(4) Ist ein Hauptprogrammveranstalter zur Einräumung von Sendezeit für unabhängige Dritte verpflichtet, schreibt die zuständige Landesmedienanstalt nach Erörterung mit dem Hauptprogrammveranstalter das Fensterprogramm zur Erteilung einer Zulassung aus.
Die zuständige Landesmedienanstalt überprüft die eingehenden Anträge auf ihre Vereinbarkeit mit den Bestimmungen dieses Staatsvertrages sowie der sonstigen landesrechtlichen Bestimmungen und teilt dem Hauptprogrammveranstalter die zulassungsfähigen Anträge mit.
Sie erörtert mit dem Hauptprogrammveranstalter die Anträge mit dem Ziel, eine einvernehmliche Auswahl zu treffen.
Kommt eine Einigung nicht zustande und liegen der zuständigen Landesmedienanstalt mehr als drei zulassungsfähige Anträge vor, unterbreitet der Hauptprogrammveranstalter der zuständigen Landesmedienanstalt einen Dreiervorschlag.
Die zuständige Landesmedienanstalt kann unter Vielfaltsgesichtspunkten bis zu zwei weitere Vorschläge hinzufügen, die sie erneut mit dem Hauptprogrammveranstalter mit dem Ziel, eine einvernehmliche Auswahl zu treffen, erörtert.
Kommt eine Einigung nicht zustande, wählt sie aus den Vorschlägen denjenigen Bewerber aus, dessen Programm den größtmöglichen Beitrag zur Vielfalt im Programm des Hauptprogrammveranstalters erwarten lässt und erteilt ihm die Zulassung.
Bei drei oder weniger Anträgen trifft die zuständige Landesmedienanstalt die Entscheidung unmittelbar.

(5) Ist ein Bewerber für das Fensterprogramm nach Absatz 4 ausgewählt, schließen der Hauptprogrammveranstalter und der Bewerber eine Vereinbarung über die Ausstrahlung des Fensterprogramms im Rahmen des Hauptprogramms.
In diese Vereinbarung ist insbesondere die Verpflichtung des Hauptprogrammveranstalters aufzunehmen, dem Fensterprogrammveranstalter eine ausreichende Finanzierung seines Programms zu ermöglichen.
Die Vereinbarung muss ferner vorsehen, dass eine Kündigung während der Dauer der Zulassung nach Absatz 6 nur wegen schwerwiegender Vertragsverletzungen oder aus einem wichtigen Grund mit einer Frist von sechs Monaten zulässig ist.

(6) Auf der Grundlage einer Vereinbarung zu angemessenen Bedingungen nach Absatz 5 ist dem Fensterprogrammveranstalter durch die zuständige Landesmedienanstalt die Zulassung zur Veranstaltung des Fensterprogramms zu erteilen.
In die Zulassung des Haupt- und des Fensterprogrammveranstalters sind die wesentlichen Verpflichtungen aus der Vereinbarung nach Absatz 5 als Bestandteil der Zulassungen aufzunehmen.
Eine Entschädigung für Vermögensnachteile durch den teilweisen Widerruf der Zulassung des Hauptprogrammveranstalters wird nicht gewährt.
Die Zulassung für den Fensterprogrammveranstalter ist auf die Dauer von fünf Jahren zu erteilen; sie erlischt, wenn die Zulassung des Hauptprogrammveranstalters endet, nicht verlängert oder nicht neu erteilt wird.

##### § 66  
Programmbeirat

(1) Der Programmbeirat hat die Programmverantwortlichen, die Geschäftsführung des Programmveranstalters und die Gesellschafter bei der Gestaltung des Programms zu beraten.
Der Programmbeirat soll durch Vorschläge und Anregungen zur Sicherung der Meinungsvielfalt und Pluralität des Programms (§ 59) beitragen.
Mit der Einrichtung eines Programmbeirats durch den Veranstalter ist dessen wirksamer Einfluss auf das Fernsehprogramm durch Vertrag oder Satzung zu gewährleisten.

(2) Die Mitglieder des Programmbeirats werden vom Veranstalter berufen.
Sie müssen aufgrund ihrer Zugehörigkeit zu gesellschaftlichen Gruppen in ihrer Gesamtheit die Gewähr dafür bieten, dass die wesentlichen Meinungen in der Gesellschaft vertreten sind.

(3) Der Programmbeirat ist über alle Fragen, die das veranstaltete Programm betreffen, durch die Geschäftsführung zu unterrichten.
Er ist bei wesentlichen Änderungen der Programmstruktur, der Programminhalte, des Programmschemas sowie bei programmbezogenen Anhörungen durch die zuständige Landesmedienanstalt und bei Programmbeschwerden zu hören.

(4) Der Programmbeirat kann zur Erfüllung seiner Aufgaben Auskünfte von der Geschäftsführung verlangen und hinsichtlich des Programms oder einzelner Beiträge Beanstandungen gegenüber der Geschäftsführung aussprechen.
Zu Anfragen und Beanstandungen hat die Geschäftsführung innerhalb angemessener Frist Stellung zu nehmen.
Trägt sie den Anfragen und Beanstandungen zum Programm nach Auffassung des Programmbeirats nicht ausreichend Rechnung, kann er in dieser Angelegenheit einen Beschluss des Kontrollorgans über die Geschäftsführung, sofern ein solches nicht vorhanden ist der Gesellschafterversammlung, verlangen.
Eine Ablehnung der Vorlage des Programmbeirats durch die Gesellschafterversammlung oder durch das Kontrollorgan über die Geschäftsführung bedarf einer Mehrheit von 75 vom Hundert der abgegebenen Stimmen.

(5) Bei Änderungen der Programmstruktur, der Programminhalte oder des Programmschemas oder bei der Entscheidung über Programmbeschwerden ist vor der Entscheidung der Geschäftsführung die Zustimmung des Programmbeirats einzuholen.
Wird diese verweigert oder kommt eine Stellungnahme binnen angemessener Frist nicht zustande, kann die Geschäftsführung die betreffende Maßnahme nur mit Zustimmung des Kontrollorgans über die Geschäftsführung, sofern ein solches nicht vorhanden ist, der Gesellschafterversammlung, für die eine Mehrheit von 75 vom Hundert der abgegebenen Stimmen erforderlich ist, treffen.
Der Veranstalter hat das Ergebnis der Befassung des Programmbeirats oder der Entscheidung nach Satz 2 der zuständigen Landesmedienanstalt mitzuteilen.

(6) Handelt es sich bei dem Veranstalter, bei dem ein Programmbeirat eingerichtet werden soll, um ein einzelkaufmännisch betriebenes Unternehmen, gelten die Absätze 4 und 5 mit der Maßgabe, dass der Programmbeirat statt der Gesellschafterversammlung oder des Kontrollorgans über die Geschäftsführung die zuständige Landesmedienanstalt anrufen kann, die über die Maßnahme entscheidet.

##### § 67  
Richtlinien

Die Landesmedienanstalten erlassen gemeinsame Richtlinien zur näheren Ausgestaltung der §§ 59, 65 und 66.
In den Richtlinien zu § 66 sind insbesondere Vorgaben über Berufung und Zusammensetzung des Programmbeirats zu machen.

##### § 68  
Sendezeit für Dritte

(1) Den Evangelischen Kirchen, der Katholischen Kirche und den Jüdischen Gemeinden sind auf Wunsch angemessene Sendezeiten zur Übertragung religiöser Sendungen einzuräumen; die Veranstalter können die Erstattung ihrer Selbstkosten verlangen.

(2) Parteien ist während ihrer Beteiligung an den Wahlen zum Deutschen Bundestag gegen Erstattung der Selbstkosten angemessene Sendezeit einzuräumen, wenn mindestens eine Landesliste für sie zugelassen wurde.
Ferner haben Parteien und sonstige politische Vereinigungen während ihrer Beteiligung an den Wahlen der Abgeordneten aus der Bundesrepublik Deutschland für das Europäische Parlament gegen Erstattung der Selbstkosten Anspruch auf angemessene Sendezeit, wenn mindestens ein Wahlvorschlag für sie zugelassen wurde.

#### 4.
Unterabschnitt  
Finanzierung, Werbung

##### § 69  
Finanzierung

Private Veranstalter können ihre Rundfunkprogramme durch Einnahmen aus Werbung, durch sonstige Einnahmen, insbesondere durch Entgelte der Teilnehmer (Abonnements oder Einzelentgelte), sowie aus eigenen Mitteln finanzieren.
Eine Finanzierung privater Veranstalter aus dem Rundfunkbeitrag ist unzulässig.
§ 112 bleibt unberührt.

##### § 70  
Dauer der Fernsehwerbung

(1) Der Anteil an Sendezeit für Fernsehwerbespots und Teleshopping-Spots darf in den Zeiträumen von 6.00 Uhr bis 18.00 Uhr, von 18.00 Uhr bis 23.00 Uhr sowie von 23.00 Uhr bis 24.00 Uhr jeweils 20 vom Hundert dieses Zeitraums nicht überschreiten.
Satz 1 gilt nicht für Produktplatzierungen und Sponsorhinweise.

(2) Hinweise des Rundfunkveranstalters auf eigene Sendungen und auf Begleitmaterialien, die direkt von diesen Sendungen abgeleitet sind, oder auf Sendungen, Rundfunkprogramme oder rundfunkähnliche Telemedien anderer Teile derselben Sendergruppe, unentgeltliche Beiträge im Dienst der Öffentlichkeit einschließlich von Spendenaufrufen zu Wohlfahrtszwecken, gesetzliche Pflichthinweise und neutrale Einzelbilder zwischen redaktionellen Inhalten und Fernsehwerbe- oder Teleshoppingspots sowie zwischen einzelnen Spots gelten nicht als Werbung.

(3) Die Absätze 1 und 2 sowie § 9 gelten nicht für reine Werbekanäle.

##### § 71  
Teleshopping-Fenster und Eigenwerbekanäle

(1) Teleshopping-Fenster, die in einem Programm gesendet werden, das nicht ausschließlich für Teleshopping bestimmt ist, müssen eine Mindestdauer von 15 Minuten ohne Unterbrechung haben.
Sie müssen optisch und akustisch klar als Teleshopping-Fenster gekennzeichnet sein.

(2) Für Eigenwerbekanäle gelten die §§ 8 und 10 entsprechend.
Die §§ 9 und 70 gelten nicht für Eigenwerbekanäle.

##### § 72  
Satzungen und Richtlinien

Die Landesmedienanstalten erlassen gemeinsame Satzungen oder Richtlinien zur Durchführung der §§ 8 bis 11, 70 und 71; in der Satzung oder Richtlinie zu § 11 sind insbesondere die Ahndung von Verstößen und die Bedingungen zur Teilnahme Minderjähriger näher zu bestimmen.
Die Landesmedienanstalten stellen hierbei das Benehmen mit den in der ARD zusammengeschlossenen Landesrundfunkanstalten und dem ZDF her und führen einen gemeinsamen Erfahrungsaustausch in der Anwendung dieser Richtlinien durch.

##### § 73  
Ausnahmen für regionale und lokale Fernsehprogramme

Für regionale und lokale Fernsehprogramme können von § 8 Absatz 4 Satz 2, § 9 Absatz 3 und § 70 Absatz 1 nach Landesrecht abweichende Regelungen getroffen werden.

### V.
Abschnitt  
Besondere Bestimmungen für einzelne Telemedien

#### 1.
Unterabschnitt  
Rundfunkähnliche Telemedien

##### § 74  
Werbung, Gewinnspiele

Für rundfunkähnliche Telemedien gelten die §§ 8, 10, 11 und 72 entsprechend.
Für Angebote nach § 2 Abs. 3 und sonstige linear verbreitete fernsehähnliche Telemedien gelten die §§ 3 bis 16 und § 72 entsprechend.

##### § 75  
Kurzberichterstattung

Für fernsehähnliche Telemedien gilt § 14 entsprechend, wenn die gleiche Sendung von demselben Fernsehveranstalter zeitversetzt angeboten wird.

##### § 76  
Barrierefreiheit

Für fernsehähnliche Telemedien gilt § 7 entsprechend.

##### § 77  
Europäische Produktionen

Zur Darstellung der Vielfalt im deutschsprachigen und europäischen Raum und zur Förderung von europäischen Film- und Fernsehproduktionen stellen Anbieter fernsehähnlicher Telemedien sicher, dass der Anteil europäischer Werke in ihren Katalogen mindestens 30 vom Hundert entspricht.
Satz 1 gilt nicht für Anbieter fernsehähnlicher Telemedien mit geringen Umsätzen oder geringen Zuschauerzahlen oder wenn dies wegen der Art oder des Themas des fernsehähnlichen Telemediums undurchführbar oder ungerechtfertigt ist.
Werke nach Satz 1 sind in den Katalogen herauszustellen.
Die Landesmedienanstalten regeln die Einzelheiten zur Durchführung der Sätze 1 bis 3 durch eine gemeinsame Satzung.

#### 2.
Unterabschnitt  
Medienplattformen und Benutzeroberflächen

##### § 78  
Anwendungsbereich

Die nachstehenden Regelungen gelten für alle Medienplattformen und Benutzeroberflächen.
Mit Ausnahme der §§ 79, 80, 86 Abs.
1 und § 109 gelten sie nicht für

1.  infrastrukturgebundene Medienplattformen mit in der Regel weniger als 10.000 angeschlossenen Wohneinheiten und deren Benutzeroberflächen oder
2.  nicht infrastrukturgebundene Medienplattformen und Benutzeroberflächen, die keine Benutzeroberflächen von Medienplattformen nach Nummer 1 sind, mit in der Regel weniger als 20.000 tatsächlichen täglichen Nutzern im Monatsdurchschnitt.

Die Landesmedienanstalten legen in den Satzungen und Richtlinien nach § 88 unter Berücksichtigung der regionalen und lokalen Verhältnisse Kriterien für die Ermittlung der Schwellenwerte fest.

##### § 79  
Allgemeine Bestimmungen

(1) Eine infrastrukturgebundene Medienplattform darf nur betreiben, wer den Anforderungen des § 53 Abs. 1 und 2 Satz 1 genügt.
Im Übrigen hat ein Anbieter einer Medienplattform oder ein Anbieter einer Benutzeroberfläche oder ein von diesem jeweils benannter Bevollmächtigter die Anforderungen des § 53 Abs. 1 und 2 Satz 1 zu erfüllen.

(2) Anbieter, die eine Medienplattform oder Benutzeroberfläche anbieten wollen, müssen dies mindestens einen Monat vor Inbetriebnahme der zuständigen Landesmedienanstalt anzeigen.
Die Anzeige hat zu enthalten:

1.  Angaben nach Absatz 1,
2.  Angaben zur technischen und voraussichtlichen Nutzungsreichweite.

Bei wesentlichen Änderungen gelten die Sätze 1 und 2 entsprechend.

(3) Für die Angebote in Medienplattformen und Benutzeroberflächen gilt die verfassungsmäßige Ordnung.
Die Vorschriften der allgemeinen Gesetze und die gesetzlichen Bestimmungen zum Schutz der persönlichen Ehre sind einzuhalten.

(4) Anbieter von Medienplattformen und Benutzeroberflächen sind für eigene Angebote verantwortlich.
Bei Verfügungen der Aufsichtsbehörden gegen Angebote oder Inhalte Dritter, die über die Medienplattform verbreitet werden oder in Benutzeroberflächen enthalten sind, sind diese zur Umsetzung dieser Verfügung verpflichtet.
Sind Maßnahmen gegenüber dem Verantwortlichen von Angeboten oder Inhalten nach Satz 2 nicht durchführbar oder nicht Erfolg versprechend, können Maßnahmen zur Verhinderung des Zugangs von Angeboten oder Inhalten auch gegen den Anbieter der Medienplattform oder Benutzeroberfläche gerichtet werden, sofern eine Verhinderung technisch möglich und zumutbar ist.

##### § 80  
Signalintegrität, Überlagerungen und Skalierungen

(1) Ohne Einwilligung des jeweiligen Rundfunkveranstalters oder Anbieters rundfunkähnlicher Telemedien dürfen dessen Rundfunkprogramme, einschließlich des HbbTV-Signals, rundfunkähnliche Telemedien oder Teile davon

1.  inhaltlich und technisch nicht verändert,
2.  im Zuge ihrer Abbildung oder akustischen Wiedergabe nicht vollständig oder teilweise mit Werbung, Inhalten aus Rundfunkprogrammen oder rundfunkähnlichen Telemedien, einschließlich Empfehlungen oder Hinweisen hierauf, überlagert oder ihre Abbildung zu diesem Zweck skaliert oder
3.  nicht in Angebotspakete aufgenommen oder in anderer Weise entgeltlich oder unentgeltlich vermarktet oder öffentlich zugänglich gemacht

werden.

(2) Abweichend von Absatz 1 Nr.
1 sind technische Veränderungen, die ausschließlich einer effizienten Kapazitätsnutzung dienen und die Einhaltung des vereinbarten oder, im Fall, dass keine Vereinbarung getroffen wurde, marktüblichen Qualitätsstandards nicht beeinträchtigen, zulässig.
Abweichend von Absatz 1 Nr.
2 sind Überlagerungen oder Skalierungen zulässig zum Zweck der Inanspruchnahme von Diensten der Individualkommunikation oder wenn sie durch den Nutzer im Einzelfall veranlasst sind.
Satz 2 gilt nicht für Überlagerung oder Skalierungen zum Zweck der Werbung, es sei denn, es handelt sich um Empfehlungen oder Hinweise auf Inhalte von Rundfunkprogrammen oder rundfunkähnliche Telemedien.

(3) Bei einer Überlagerung oder Skalierung zum Zweck der Werbung finden außer in den Fällen des Absatzes 2 Satz 2 die für das überlagerte oder skalierte Angebot geltenden Beschränkungen entsprechende Anwendung.

##### § 81  
Belegung von Medienplattformen

(1) Für infrastrukturgebundene Medienplattformen gelten die nachfolgenden Bestimmungen.

(2) Der Anbieter einer Medienplattform

1.  hat sicherzustellen, dass innerhalb einer technischen Kapazität im Umfang von höchstens einem Drittel der für die digitale Verbreitung von Fernsehprogrammen zur Verfügung stehenden Gesamtkapazität
    1.  die erforderlichen Kapazitäten für die bundesweiten gesetzlich bestimmten beitragsfinanzierten Programme sowie für die Dritten Programme des öffentlich-rechtlichen Rundfunks einschließlich programmbegleitender Dienste zur Verfügung stehen; für die im Rahmen der Dritten Programme verbreiteten Landesfenster gilt dies nur innerhalb der Länder, für die sie gesetzlich bestimmt sind,
    2.  die Kapazitäten für die privaten Fernsehprogramme, die Regionalfenster gemäß § 59 enthalten, einschließlich programmbegleitender Dienste, zur Verfügung stehen; die Fernsehprogramme sind einschließlich der für die jeweilige Region gesetzlich bestimmten Regionalfenster zu verbreiten,
    3.  die Kapazitäten für die im jeweiligen Land zugelassenen regionalen und lokalen Fernsehprogramme sowie die Offenen Kanäle zur Verfügung stehen; dies gilt nur innerhalb des Gebiets, für das sie jeweils bestimmt sind; die landesrechtlichen Sondervorschriften für Offene Kanäle und vergleichbare Angebote bleiben unberührt,
    4.  die technischen Kapazitäten nach den Buchstaben a bis c im Verhältnis zu anderen digitalen Kapazitäten technisch gleichwertig sind,
2.  trifft selbst innerhalb einer weiteren technischen Kapazität im Umfang der Kapazität nach Nummer 1 die Entscheidung über die Belegung mit in digitaler Technik verbreiteten Fernsehprogrammen einschließlich programmbegleitender Dienste, soweit er darin unter Einbeziehung der Interessen der angeschlossenen Teilnehmer eine Vielzahl von Programmveranstaltern sowie ein vielfältiges Programmangebot an Vollprogrammen, nicht entgeltfinanzierten Programmen, Spartenprogrammen mit Schwerpunkt Nachrichten, sonstigen Spartenprogrammen und Fremdsprachenprogrammen einbezieht sowie Teleshoppingkanäle angemessen berücksichtigt,
3.  trifft innerhalb der darüber hinausgehenden technischen Kapazitäten die Entscheidung über die Belegung nach Maßgabe des § 82 Abs.
    2 und der allgemeinen Gesetze.

Reicht die Kapazität zur Belegung nach Satz 1 Nr.
1 nicht aus, sind die Grundsätze des Satzes 1 entsprechend der zur Verfügung stehenden Gesamtkapazität anzuwenden; dabei haben die für das jeweilige Verbreitungsgebiet gesetzlich bestimmten beitragsfinanzierten Programme und programmbegleitende Dienste des öffentlich-rechtlichen Rundfunks Vorrang unbeschadet der angemessenen Berücksichtigung der Angebote nach Satz 1 Nr.
1 Buchst.
b und c.

(3) Der Anbieter einer Medienplattform

1.  hat sicherzustellen, dass innerhalb einer technischen Kapazität im Umfang von höchstens einem Drittel der für die digitale Verbreitung von Hörfunk zur Verfügung stehenden Gesamtkapazität
    1.  die erforderlichen Kapazitäten für die in dem jeweiligen Verbreitungsgebiet gesetzlich bestimmten beitragsfinanzierten Programme und programmbegleitenden Dienste des öffentlich-rechtlichen Rundfunks zur Verfügung stehen,
    2.  die Kapazitäten für die im jeweiligen Land zugelassenen Hörfunkprogramme sowie die Offenen Kanäle zur Verfügung stehen; die landesrechtlichen Sondervorschriften für Offene Kanäle und vergleichbare Angebote bleiben unberührt,
2.  trifft selbst innerhalb einer weiteren technischen Übertragungskapazität im Umfang der Kapazität nach Nummer 1 die Entscheidung über die Belegung mit in digitaler Technik verbreiteten Hörfunkprogrammen und programmbegleitenden Diensten, soweit er darin unter Einbeziehung der Interessen der angeschlossenen Teilnehmer ein vielfältiges Angebot und insbesondere eine Vielfalt der für das jeweilige Verbreitungsgebiet bestimmten Angebote angemessen berücksichtigt,
3.  trifft innerhalb der darüber hinausgehenden technischen Kapazitäten die Entscheidung über die Belegung nach Maßgabe des § 82 Abs.
    2 und der allgemeinen Gesetze.

Absatz 2 Satz 2 gilt entsprechend.

(4) Der Anbieter einer Medienplattform ist von den Anforderungen nach den Absätzen 2 und 3 befreit, soweit

1.  der Anbieter der zuständigen Landesmedienanstalt nachweist, dass er selbst oder ein Dritter den Empfang der entsprechenden Angebote auf einem gleichartigen Übertragungsweg und demselben Endgerät unmittelbar und ohne zusätzlichen Aufwand ermöglicht, oder
2.  das Gebot der Meinungsvielfalt und Angebotsvielfalt bereits im Rahmen der Zuordnungs- oder Zuweisungsentscheidung nach den §§ 101 oder 102 berücksichtigt wurde.

(5) Programme, die dem Anbieter einer Medienplattform gemäß § 62 zugerechnet werden können oder von ihm exklusiv vermarktet werden, bleiben bei der Erfüllung der Anforderungen nach Absatz 2 Satz 1 Nr. 1 und 2 und Absatz 3 Satz 1 Nr.
1 und 2 außer Betracht.
Der Anbieter einer Medienplattform hat die Belegung von Rundfunkprogrammen der zuständigen Landesmedienanstalt auf deren Verlangen unverzüglich mitzuteilen.
Werden die Voraussetzungen der Absätze 2 bis 4 nicht erfüllt, erfolgt die Auswahl der zu verbreitenden Rundfunkprogramme nach Maßgabe dieses Staatsvertrages und des Landesrechts durch die zuständige Landesmedienanstalt.
Zuvor ist dem Anbieter einer Medienplattform eine angemessene Frist zur Erfüllung der gesetzlichen Voraussetzungen zu setzen.

(6) Für regionale und lokale Medienplattformen, die Hörfunk- und Fernsehprogramme ausschließlich terrestrisch verbreiten, kann das Landesrecht abweichende Regelungen vorsehen.

##### § 82  
Zugang zu Medienplattformen

(1) Anbieter von Medienplattformen haben zu gewährleisten, dass die eingesetzte Technik ein vielfältiges Angebot ermöglicht.

(2) Zur Sicherung der Meinungsvielfalt und Angebotsvielfalt dürfen Rundfunk, rundfunkähnliche Telemedien und Telemedien nach § 19 Abs.
1 beim Zugang zu Medienplattformen nicht unmittelbar oder mittelbar unbillig behindert und gegenüber gleichartigen Angeboten nicht ohne sachlich gerechtfertigten Grund unterschiedlich behandelt werden; dies gilt insbesondere in Bezug auf

1.  Zugangsberechtigungssysteme,
2.  Schnittstellen für Anwendungsprogramme,
3.  sonstige technische Vorgaben zu den Nummern 1 und 2 auch gegenüber Herstellern digitaler Rundfunkempfangsgeräte,
4.  die Ausgestaltung von Zugangsbedingungen, insbesondere Entgelte und Tarife.

(3) Die Verwendung eines Zugangsberechtigungssystems oder einer Schnittstelle für Anwendungsprogramme und die Entgelte hierfür sind der zuständigen Landesmedienanstalt unverzüglich anzuzeigen.
Satz 1 gilt für Änderungen entsprechend.
Der zuständigen Landesmedienanstalt sind auf Verlangen die erforderlichen Auskünfte zu erteilen.

##### § 83  
Zugangsbedingungen zu Medienplattformen

(1) Die Zugangsbedingungen, insbesondere Entgelte und Tarife, sind gegenüber der zuständigen Landesmedienanstalt offenzulegen.

(2) Entgelte und Tarife sind im Rahmen des Telekommunikationsgesetzes so zu gestalten, dass auch regionale und lokale Angebote zu angemessenen Bedingungen verbreitet werden können.
Die landesrechtlichen Sondervorschriften für Offene Kanäle und vergleichbare Angebote bleiben unberührt.

(3) Können sich die betroffenen Anbieter nicht über die Aufnahme eines Angebots in eine Medienplattform oder die Bedingungen der Aufnahme einigen, kann jeder der Beteiligten die zuständige Landesmedienanstalt anrufen.
Die zuständige Landesmedienanstalt wirkt unter den Beteiligten auf eine sachgerechte Lösung hin.

##### § 84  
Auffindbarkeit in Benutzeroberflächen

(1) Die nachstehenden Regelungen gelten, soweit Benutzeroberflächen Rundfunk, rundfunkähnliche Telemedien und Telemedien nach § 19 Abs.
1, Teile davon oder softwarebasierte Anwendungen, die im Wesentlichen der unmittelbaren Ansteuerung von Rundfunk, rundfunkähnlichen Telemedien und Telemedien nach § 19 Abs.
1 dienen, hierzu abbilden oder akustisch vermitteln.

(2) Gleichartige Angebote oder Inhalte dürfen bei der Auffindbarkeit, insbesondere der Sortierung, Anordnung oder Präsentation in Benutzeroberflächen, nicht ohne sachlich gerechtfertigten Grund unterschiedlich behandelt werden; die Auffindbarkeit darf nicht unbillig behindert werden.
Zulässige Kriterien für eine Sortierung oder Anordnung sind insbesondere Alphabet, Genres oder Nutzungsreichweite.
Alle Angebote müssen mittels einer Suchfunktion diskriminierungsfrei auffindbar sein.

(3) Der in einer Benutzeroberfläche vermittelte Rundfunk hat in seiner Gesamtheit auf der ersten Auswahlebene unmittelbar erreichbar und leicht auffindbar zu sein.
Innerhalb des Rundfunks haben die gesetzlich bestimmten beitragsfinanzierten Programme, die Rundfunkprogramme, die Fensterprogramme (§ 59 Abs.
4) aufzunehmen haben, sowie die privaten Programme, die in besonderem Maß einen Beitrag zur Meinungs- und Angebotsvielfalt im Bundesgebiet leisten, leicht auffindbar zu sein.
Werden Rundfunkprogramme abgebildet oder akustisch vermittelt, die Fensterprogramme (§ 59 Abs.
4) aufzunehmen haben, sind in dem Gebiet, für das die Fensterprogramme zugelassen oder gesetzlich bestimmt sind, die Hauptprogramme mit Fensterprogramm gegenüber dem ohne Fensterprogramm ausgestrahlten Hauptprogramm und gegenüber den Fensterprogrammen, die für andere Gebiete zugelassen oder gesetzlich bestimmt sind, vorrangig darzustellen.

(4) Die in einer Benutzeroberfläche vermittelten gemeinsamen Telemedienangebote der in der ARD zusammengeschlossenen Landesrundfunkanstalten, die Telemedienangebote des ZDF sowie des Deutschlandradios oder vergleichbare rundfunkähnliche Telemedienangebote oder Angebote nach § 2 Abs. 2 Nr.
14 Buchst.
b privater Anbieter, die in besonderem Maß einen Beitrag zur Meinungs- und Angebotsvielfalt im Bundesgebiet leisten, oder softwarebasierte Anwendungen, die ihrer unmittelbaren Ansteuerung dienen, haben im Rahmen der Präsentation rundfunkähnlicher Telemedien oder der softwarebasierten Anwendungen, die ihrer unmittelbaren Ansteuerung dienen, leicht auffindbar zu sein.

(5) Die privaten Angebote im Sinne des Absatzes 3 Satz 2 und des Absatzes 4 werden durch die Landesmedienanstalten für die Dauer von jeweils drei Jahren bestimmt und in einer Liste im Onlineauftritt der Landesmedienanstalten veröffentlicht.
In die Entscheidung sind folgende Kriterien einzubeziehen:

1.  der zeitliche Anteil an nachrichtlicher Berichterstattung über politisches und zeitgeschichtliches Geschehen,
2.  der zeitliche Anteil an regionalen und lokalen Informationen,
3.  das Verhältnis zwischen eigen‐ und fremdproduzierten Programminhalten,
4.  der Anteil an barrierefreien Angeboten,
5.  das Verhältnis zwischen ausgebildeten und auszubildenden Mitarbeitern, die an der Programmerstellung beteiligt sind,
6.  die Quote europäischer Werke und
7.  der Anteil an Angeboten für junge Zielgruppen.

Die Landesmedienanstalten bestimmen unverzüglich Beginn und Ende einer Ausschlussfrist, innerhalb derer Anbieter schriftliche Anträge auf Aufnahme in die Liste stellen können.
Beginn und Ende der Antragsfrist, das Verfahren und die wesentlichen Anforderungen an die Antragsstellung sind von den Landesmedienanstalten im Rahmen der Ausschreibung festzulegen; die Ausschreibung ist in geeigneter Weise zu veröffentlichen.

(6) Die Sortierung oder Anordnung von Angeboten oder Inhalten muss auf einfache Weise und dauerhaft durch den Nutzer individualisiert werden können.

(7) Absatz 2 Satz 3 sowie die Absätze 3, 4 und 6 gelten für Benutzeroberflächen nicht, soweit der Anbieter nachweist, dass eine auch nachträgliche Umsetzung technisch nicht oder nur mit unverhältnismäßigem Aufwand möglich ist.

(8) Die Einzelheiten der Absätze 2 bis 7 regeln die Landesmedienanstalten durch gemeinsame Satzungen und Richtlinien.

##### § 85  
Transparenz

Die einer Medienplattform oder Benutzeroberfläche zugrunde liegenden Grundsätze für die Auswahl von Rundfunk, rundfunkähnlichen Telemedien und Telemedien nach § 19 Abs.
1 und für ihre Organisation sind vom Anbieter transparent zu machen.
Dies umfasst die Kriterien, nach denen Inhalte sortiert, angeordnet und präsentiert werden, wie die Sortierung oder Anordnung von Inhalten durch den Nutzer individualisiert werden kann und nach welchen grundlegenden Kriterien Empfehlungen erfolgen und unter welchen Bedingungen Rundfunk oder rundfunkähnliche Telemedien nach § 80 nicht in ihrer ursprünglichen Form dargestellt werden.
Informationen hierzu sind den Nutzern in leicht wahrnehmbarer, unmittelbar erreichbarer und ständig verfügbarer Weise zur Verfügung zu stellen.

##### § 86  
Vorlage von Unterlagen, Zusammenarbeit mit der Regulierungsbehörde für Telekommunikation

(1) Anbieter von Medienplattformen und Benutzeroberflächen sind verpflichtet, die erforderlichen Informationen und Unterlagen der zuständigen Landesmedienanstalt auf Verlangen unverzüglich vorzulegen.
Die §§ 55, 56 und 58 gelten entsprechend.

(2) Ob ein Verstoß gegen § 82 Abs.
2 Nr.
1, 2 oder 4 oder § 83 Abs.
2 vorliegt, entscheidet bei Anbietern von Medienplattformen, die zugleich Anbieter der Telekommunikationsdienstleistung sind, die zuständige Landesmedienanstalt im Benehmen mit der Regulierungsbehörde für Telekommunikation.

(3) Anbieter von Medienplattformen oder Benutzeroberflächen haben auf Nachfrage gegenüber Anbietern von Rundfunk, rundfunkähnlichen Telemedien und Telemedien nach § 19 Abs.
1 die tatsächliche Sortierung, Anordnung und Abbildung von Angeboten und Inhalten, die Verwendung ihrer Metadaten sowie im Rahmen eines berechtigten Interesses Zugangsbedingungen nach § 83 Abs. 1 mitzuteilen.

##### § 87  
Bestätigung der Unbedenklichkeit

Im Hinblick auf die Anforderungen der §§ 81 bis 85 sind Anbieter von Medienplattformen oder Benutzeroberflächen berechtigt, bei der zuständigen Landesmedienanstalt einen Antrag auf Unbedenklichkeit zu stellen.
Die Bestätigung der Unbedenklichkeit kann mit Nebenbestimmungen versehen werden.

##### § 88  
Satzungen, Richtlinien

Die Landesmedienanstalten regeln durch gemeinsame Satzungen und Richtlinien Einzelheiten zur Konkretisierung der sie betreffenden Bestimmungen dieses Unterabschnitts.
Dabei ist die Bedeutung für die öffentliche Meinungsbildung für den Empfängerkreis in Bezug auf den jeweiligen Übertragungsweg, die jeweilige Medienplattform oder die jeweilige Benutzeroberfläche zu berücksichtigen.

##### § 89  
Überprüfungsklausel

Die Bestimmungen dieses Unterabschnitts sowie die ergänzenden landesrechtlichen Regelungen werden regelmäßig alle fünf Jahre, erstmals zum 1.
Oktober 2025, entsprechend Artikel 114 Abs. 2 der Richtlinie (EU) 2018/1972 des Europäischen Parlaments und des Rates vom 11.
Dezember 2018 über den europäischen Kodex für die elektronische Kommunikation (ABl.
L 321 vom 17.12.2018, S. 36) überprüft.

##### § 90  
Bestehende Zulassungen, Zuordnungen, Zuweisungen, Anzeige von bestehenden Medienplattformen oder Benutzeroberflächen

(1) Bestehende Zulassungen, Zuordnungen und Zuweisungen für bundesweite Anbieter gelten bis zu deren Ablauf fort.
Bestehende Zulassungen und Zuweisungen für Fensterprogrammveranstalter sollen bis zum 31.
Dezember 2009 unbeschadet von Vorgaben des § 59 Abs.
4 Satz 4 verlängert werden.

(2) Anbieter von Medienplattformen und Benutzeroberflächen, die bei Inkrafttreten dieses Staatsvertrages bereits in Betrieb aber nicht angezeigt sind, müssen die Anzeige nach § 79 Abs. 2 spätestens sechs Monate nach Inkrafttreten dieses Staatsvertrages vornehmen.

#### 3.
Unterabschnitt  
Medienintermediäre

##### § 91  
Anwendungsbereich

(1) Die nachstehenden Regelungen gelten auch dann, wenn die intermediäre Funktion in die Angebote Dritter eingebunden wird (integrierter Medienintermediär).

(2) Mit Ausnahme des § 95 gelten sie nicht für Medienintermediäre, die

1.  im Durchschnitt von sechs Monaten in Deutschland weniger als eine Million Nutzer pro Monat erreichen oder in ihrer prognostizierten Entwicklung erreichen werden,
2.  auf die Aggregation, Selektion und Präsentation von Inhalten mit Bezug zu Waren oder Dienstleistungen spezialisiert sind oder
3.  ausschließlich privaten oder familiären Zwecken dienen.

##### § 92  
Inländischer Zustellungsbevollmächtigter

Anbieter von Medienintermediären haben im Inland einen Zustellungsbevollmächtigten zu benennen und in ihrem Angebot in leicht erkennbarer und unmittelbar erreichbarer Weise auf ihn aufmerksam zu machen.
An diese Person können Zustellungen in Verfahren nach § 115 bewirkt werden.
Das gilt auch für die Zustellung von Schriftstücken, die solche Verfahren einleiten oder vorbereiten.

##### § 93  
Transparenz

(1) Anbieter von Medienintermediären haben zur Sicherung der Meinungsvielfalt nachfolgende Informationen leicht wahrnehmbar, unmittelbar erreichbar und ständig verfügbar zu halten:

1.  die Kriterien, die über den Zugang eines Inhalts zu einem Medienintermediär und über den Verbleib entscheiden,
2.  die zentralen Kriterien einer Aggregation, Selektion und Präsentation von Inhalten und ihre Gewichtung einschließlich Informationen über die Funktionsweise der eingesetzten Algorithmen in verständlicher Sprache.

(2) Anbieter von Medienintermediären, die eine thematische Spezialisierung aufweisen, sind dazu verpflichtet, diese Spezialisierung durch die Gestaltung ihres Angebots wahrnehmbar zu machen.
§ 91 Abs. 2 Nr.
2 bleibt unberührt.

(3) Änderungen der in Absatz 1 genannten Kriterien sowie der Ausrichtung nach Absatz 2 sind unverzüglich in derselben Weise wahrnehmbar zu machen.

(4) Anbieter von Medienintermediären, die soziale Netzwerke anbieten, haben dafür Sorge zu tragen, dass Telemedien im Sinne von § 18 Abs.
3 gekennzeichnet werden.

##### § 94  
Diskriminierungsfreiheit

(1) Zur Sicherung der Meinungsvielfalt dürfen Medienintermediäre journalistisch-redaktionell gestaltete Angebote, auf deren Wahrnehmbarkeit sie besonders hohen Einfluss haben, nicht diskriminieren.

(2) Eine Diskriminierung im Sinne des Absatzes 1 liegt vor, wenn ohne sachlich gerechtfertigten Grund von den nach § 93 Abs.
1 bis 3 zu veröffentlichenden Kriterien zugunsten oder zulasten eines bestimmten Angebots systematisch abgewichen wird oder diese Kriterien Angebote unmittelbar oder mittelbar unbillig systematisch behindern.

(3) Ein Verstoß kann nur von dem betroffenen Anbieter journalistisch-redaktioneller Inhalte bei der zuständigen Landesmedienanstalt geltend gemacht werden.
In offensichtlichen Fällen kann der Verstoß von der zuständigen Landesmedienanstalt auch von Amts wegen verfolgt werden.

##### § 95  
Vorlage von Unterlagen

Anbieter von Medienintermediären sind verpflichtet, die erforderlichen Unterlagen der zuständigen Landesmedienanstalt auf Verlangen vorzulegen.
Die §§ 56 und 58 gelten entsprechend.

##### § 96  
Satzungen und Richtlinien

Die Landesmedienanstalten regeln durch gemeinsame Satzungen und Richtlinien Einzelheiten zur Konkretisierung der sie betreffenden Bestimmungen dieses Unterabschnitts.
Dabei ist die Orientierungsfunktion der Medienintermediäre für die jeweiligen Nutzerkreise zu berücksichtigen.

#### 4.
Unterabschnitt  
Video-Sharing-Dienste

##### § 97  
Anwendungsbereich

Dieser Unterabschnitt gilt für Video-Sharing-Dienste im Sinne von § 2 Abs.
2 Nr.
22.
Weitere Anforderungen nach dem V.
Abschnitt bleiben unberührt.

##### § 98  
Werbung

(1) Für Werbung in Video-Sharing-Diensten gelten § 8 Abs.
1, Abs.
3 Satz 1 und 2, Abs.
7 und 10 dieses Staatsvertrages sowie § 6 Abs.
2 und 7 des Jugendmedienschutz-Staatsvertrages.

(2) Der Anbieter eines Video-Sharing-Dienstes hat sicherzustellen, dass Werbung, die von ihm vermarktet, verkauft oder zusammengestellt wird, den Vorgaben des Absatzes 1 entspricht.

(3) Der Anbieter eines Video-Sharing-Dienstes hat nachfolgende Maßnahmen zu ergreifen, um sicherzustellen, dass Werbung die nicht von ihm selbst vermarktet, verkauft oder zusammengestellt wird, die Vorgaben des Absatzes 1 erfüllt:

1.  Aufnahme und Umsetzung von Bestimmungen in seinen Allgemeinen Geschäftsbedingungen, die zur Einhaltung der Vorgaben des Absatzes 1 verpflichten,
2.  Bereitstellung einer Funktion zur Kennzeichnung von Werbung nach § 6 Abs.
    3 des Telemediengesetzes.

##### § 99  
Schlichtungsstelle

(1) Die Landesmedienanstalten richten eine gemeinsame Stelle ein für die Schlichtung von Streitigkeiten zwischen den Beschwerdeführern oder von der Beschwerde betroffenen Nutzern und Anbietern von Video-Sharing-Diensten über Maßnahmen, die Anbieter von Video-Sharing-Diensten im Verfahren nach den §§ 10 a und b des Telemediengesetzes getroffen oder unterlassen haben.

(2) Die Landesmedienanstalten regeln die weiteren Einzelheiten über die Organisation, das Schlichtungsverfahren und die Kostentragung in einer im Internet zu veröffentlichenden gemeinsamen Satzung.

### VI.
Abschnitt  
Übertragungskapazitäten, Weiterverbreitung

##### § 100  
Grundsatz

Die Entscheidung über die Zuordnung, Zuweisung und Nutzung der Übertragungskapazitäten, die zur Verbreitung von Rundfunk und rundfunkähnlichen Telemedien dienen, erfolgt nach Maßgabe dieses Staatsvertrages und des jeweiligen Landesrechts.

##### § 101  
Zuordnung von drahtlosen Übertragungskapazitäten

(1) Über die Anmeldung bei der für Telekommunikation zuständigen Regulierungsbehörde für bundesweite Versorgungsbedarfe an nicht leitungsgebundenen (drahtlosen) Übertragungskapazitäten entscheiden die Länder einstimmig.
Für länderübergreifende Bedarfsanmeldungen gilt Satz 1 hinsichtlich der betroffenen Länder entsprechend.

(2) Über die Zuordnung von Übertragungskapazitäten für bundesweite Versorgungsbedarfe an die in der ARD zusammengeschlossenen Landesrundfunkanstalten, das ZDF, das Deutschlandradio oder die Landesmedienanstalten entscheiden die Regierungschefinnen und Regierungschefs der Länder durch einstimmigen Beschluss.

(3) Für die Zuordnung gelten insbesondere die folgenden Grundsätze:

1.  zur Verfügung stehende freie Übertragungskapazitäten sind den in der ARD zusammengeschlossenen Landesrundfunkanstalten, dem ZDF oder dem Deutschlandradio und den Landesmedienanstalten bekannt zu machen;
2.  reichen die Übertragungskapazitäten für den geltend gemachten Bedarf aus, sind diese entsprechend zuzuordnen;
3.  reichen die Übertragungskapazitäten für den geltend gemachten Bedarf nicht aus, wirken die Regierungschefinnen und Regierungschefs der Länder auf eine Verständigung zwischen den Beteiligten hin; Beteiligte sind für private Anbieter die Landesmedienanstalten;
4.  kommt eine Verständigung zwischen den Beteiligten nicht zu Stande, entscheiden die Regierungschefinnen und Regierungschefs der Länder, welche Zuordnung unter Berücksichtigung der Besonderheiten der Übertragungskapazität sowie unter Berücksichtigung des Gesamtangebots die größtmögliche Vielfalt des Angebotes sichert; dabei sind insbesondere folgende Kriterien zu berücksichtigen:
    1.  Sicherung der Grundversorgung mit Rundfunk und Teilhabe des öffentlich-rechtlichen Rundfunks an neuen Techniken und Programmformen,
    2.  Belange des privaten Rundfunks und der Anbieter von Telemedien.

Die Zuordnung der Übertragungskapazität erfolgt für die Dauer von längstens 20 Jahren.

(4) Der oder die Vorsitzende der Konferenz der Regierungschefinnen und Regierungschefs der Länder ordnet die Übertragungskapazität gemäß der Entscheidung der Regierungschefinnen und Regierungschefs der Länder nach Absatz 2 zu.

(5) Wird eine zugeordnete Übertragungskapazität nach Ablauf von 18 Monaten nach Zugang der Zuordnungsentscheidung nicht für die Realisierung des Versorgungsbedarfs genutzt, kann die Zuordnungsentscheidung durch Beschluss der Regierungschefinnen und Regierungschefs der Länder widerrufen werden; eine Entschädigung wird nicht gewährt.
Auf Antrag des Zuordnungsempfängers kann die Frist durch Entscheidung der Regierungschefinnen und Regierungschefs der Länder verlängert werden.

(6) Die Regierungschefinnen und Regierungschefs der Länder vereinbaren zur Durchführung der Absätze 2 bis 5 Verfahrensregelungen.

##### § 102  
Zuweisung von drahtlosen Übertragungskapazitäten an private Anbieter durch die zuständige Landesmedienanstalt

(1) Übertragungskapazitäten für drahtlose bundesweite Versorgungsbedarfe privater Anbieter können Rundfunkveranstaltern, Anbietern von Telemedien oder Anbietern von Medienplattformen durch die zuständige Landesmedienanstalt zugewiesen werden.

(2) Werden den Landesmedienanstalten Übertragungskapazitäten zugeordnet, bestimmen sie unverzüglich Beginn und Ende einer Ausschlussfrist, innerhalb der schriftliche Anträge auf Zuweisung von Übertragungskapazitäten gestellt werden können.
Beginn und Ende der Antragsfrist, das Verfahren und die wesentlichen Anforderungen an die Antragstellung, insbesondere wie den Anforderungen dieses Staatsvertrages zur Sicherung der Meinungsvielfalt und Angebotsvielfalt genügt werden kann, sind von den Landesmedienanstalten zu bestimmen und in geeigneter Weise zu veröffentlichen (Ausschreibung).

(3) Kann nicht allen Anträgen auf Zuweisung von Übertragungskapazitäten entsprochen werden, wirkt die zuständige Landesmedienanstalt auf eine Verständigung zwischen den Antragstellern hin.
Kommt eine Verständigung zustande, legt sie diese ihrer Entscheidung über die Aufteilung der Übertragungskapazitäten zu Grunde, wenn nach den vorgelegten Unterlagen erwartet werden kann, dass in der Gesamtheit der Angebote die Vielfalt der Meinungen und Angebotsvielfalt zum Ausdruck kommt.

(4) Lässt sich innerhalb der von der zuständigen Landesmedienanstalt zu bestimmenden angemessenen Frist keine Einigung erzielen oder entspricht die vorgesehene Aufteilung voraussichtlich nicht dem Gebot der Meinungsvielfalt und Angebotsvielfalt, weist die zuständige Landesmedienanstalt dem Antragsteller die Übertragungskapazität zu, der am ehesten erwarten lässt, dass sein Angebot

1.  die Meinungsvielfalt und Angebotsvielfalt fördert,
2.  auch das öffentliche Geschehen, die politischen Ereignisse sowie das kulturelle Leben darstellt und
3.  bedeutsame politische, weltanschauliche und gesellschaftliche Gruppen zu Wort kommen lässt.

In die Auswahlentscheidung ist ferner einzubeziehen, ob das Angebot wirtschaftlich tragfähig erscheint sowie Nutzerinteressen und -akzeptanz hinreichend berücksichtigt.
Für den Fall, dass die Übertragungskapazität einem Anbieter einer Medienplattform zugewiesen werden soll, ist des Weiteren zu berücksichtigen, inwieweit sichergestellt ist, dass das Angebot den Vorgaben der §§ 82 und 83 genügt.

(5) Die Zuweisung von Übertragungskapazitäten erfolgt für die Dauer von zehn Jahren.
Eine einmalige Verlängerung um zehn Jahre ist zulässig.
Die Zuweisung ist sofort vollziehbar.
Wird eine zugewiesene Übertragungskapazität nach Ablauf von zwölf Monaten nach Zugang der Zuweisungsentscheidung nicht genutzt, kann die zuständige Landesmedienanstalt die Zuweisungsentscheidung nach § 108 Abs. 2 Nr.
2 Buchst.
b widerrufen.
Auf Antrag des Zuweisungsempfängers kann die Frist verlängert werden.

##### § 103  
Weiterverbreitung

(1) Die Weiterverbreitung von bundesweit empfangbaren Angeboten, die in rechtlich zulässiger Weise in einem anderen Mitgliedstaat der Europäischen Union in Übereinstimmung mit Artikel 2 der Richtlinie 2010/13/EU oder in einem Staat, der das Europäische Übereinkommen über das grenzüberschreitende Fernsehen ratifiziert hat, und nicht Mitglied der Europäischen Union ist, in Übereinstimmung mit den Bestimmungen des Europäischen Übereinkommens über das grenzüberschreitende Fernsehen veranstaltet werden, ist zulässig.
Die Weiterverbreitung der in Satz 1 genannten Angebote aus einem anderen Mitgliedstaat der Europäischen Union kann nur in Übereinstimmung mit Artikel 3 der Richtlinie 2010/13/EU, die Weiterverbreitung der in Satz 1 genannten Angebote aus einem Staat, der das Europäische Übereinkommen über das grenzüberschreitende Fernsehen ratifiziert hat, und nicht Mitglied der Europäischen Union ist, nur in Übereinstimmung mit den Bestimmungen des Europäischen Übereinkommens über das grenzüberschreitende Fernsehen ausgesetzt werden.

(2) Veranstalter anderer als der in Absatz 1 genannten Fernsehprogramme haben die Weiterverbreitung mindestens einen Monat vor Beginn bei der Landesmedienanstalt anzuzeigen, in deren Geltungsbereich die Programme verbreitet werden sollen.
Die Anzeige kann auch der Anbieter einer Medienplattform vornehmen.
Die Anzeige muss die Nennung eines Programmverantwortlichen, eine Beschreibung des Programms und die Vorlage einer Zulassung oder eines vergleichbaren Dokuments beinhalten.
Die Weiterverbreitung ist dem Anbieter einer Medienplattform zu untersagen, wenn das Rundfunkprogramm nicht den Anforderungen des § 3 oder des Jugendmedienschutz-Staatsvertrages entspricht oder wenn der Veranstalter nach dem geltenden Recht des Ursprungslandes zur Veranstaltung von Rundfunk nicht befugt ist oder wenn das Programm nicht inhaltlich unverändert verbreitet wird.

(3) Landesrechtliche Regelungen zur analogen Kanalbelegung für Rundfunk sind zulässig, soweit sie zur Erreichung klar umrissener Ziele von allgemeinem Interesse erforderlich sind.
Sie können insbesondere zur Sicherung einer pluralistischen, am Angebot der Meinungsvielfalt und Angebotsvielfalt orientierten Medienordnung getroffen werden.
Einzelheiten, insbesondere die Rangfolge bei der Belegung der Kabelkanäle, regelt das Landesrecht.

(4) Ferner können angemessene Maßnahmen in Übereinstimmung mit Artikel 4 Abs.
3 der Richtlinie 2010/13/EU unter Wahrung der sonstigen Regelungen ihres Artikels 4 gegen den Mediendiensteanbieter ergriffen werden, der der Rechtshoheit eines anderen Mitgliedstaats unterworfen ist und einen audiovisuellen Mediendienst erbringt, der ganz oder vorwiegend auf Deutschland ausgerichtet ist, soweit die Bundesrepublik Deutschland im öffentlichen Interesse liegende ausführlichere oder strengere Bestimmungen nach Artikel 4 Absatz 1 der Richtlinie 2010/13/EU erlassen hat.

### VII.
Abschnitt  
Medienaufsicht

##### § 104  
Organisation

(1) Soweit nichts anderes bestimmt ist, überprüft die zuständige Landesmedienanstalt die Einhaltung der Bestimmungen nach diesem Staatsvertrag.
Sie trifft entsprechend den Bestimmungen dieses Staatsvertrages die jeweiligen Entscheidungen.
Satz 1 und 2 gelten nicht für Angebote der in der ARD zusammengeschlossenen Landesrundfunkanstalten, des ZDF und des Deutschlandradios.

(2) Zur Erfüllung der Aufgaben nach § 105 und nach den Bestimmungen des Jugendmedienschutz-Staatsvertrages bestehen:

1.  die Kommission für Zulassung und Aufsicht (ZAK),
2.  die Gremienvorsitzendenkonferenz (GVK),
3.  die Kommission zur Ermittlung der Konzentration im Medienbereich (KEK) und
4.  die Kommission für Jugendmedienschutz (KJM).

Diese dienen der jeweils zuständigen Landesmedienanstalt als Organe bei der Erfüllung ihrer Aufgaben nach § 105.

(3) Die Landesmedienanstalten entsenden jeweils den nach Landesrecht bestimmten gesetzlichen Vertreter in die ZAK; eine Vertretung im Fall der Verhinderung ist durch den ständigen Vertreter zulässig.
Die Tätigkeit der Mitglieder der ZAK ist unentgeltlich.

(4) Die GVK setzt sich zusammen aus dem jeweiligen Vorsitzenden des plural besetzten Beschlussgremiums der Landesmedienanstalten; eine Vertretung im Fall der Verhinderung durch den stellvertretenden Vorsitzenden ist zulässig.
Die Tätigkeit der Mitglieder der GVK ist unentgeltlich.

(5) Die KEK besteht aus

1.  sechs Sachverständigen des Rundfunk- und des Wirtschaftsrechts, von denen drei die Befähigung zum Richteramt haben müssen, und
2.  sechs nach Landesrecht bestimmten gesetzlichen Vertretern der Landesmedienanstalten.

Die Mitglieder nach Satz 1 Nr.
1 der KEK und zwei Ersatzmitglieder für den Fall der Verhinderung eines dieser Mitglieder werden von den Regierungschefinnen und Regierungschefs der Länder für die Dauer von fünf Jahren einvernehmlich berufen.
Von der Mitgliedschaft nach Satz 2 ausgeschlossen sind Mitglieder und Bedienstete der Institutionen der Europäischen Union, der Verfassungsorgane des Bundes und der Länder, Gremienmitglieder und Bedienstete von Landesrundfunkanstalten der ARD, des ZDF, des Deutschlandradios, des Europäischen Fernsehkulturkanals „arte“, der Landesmedienanstalten, der privaten Rundfunkveranstalter und Anbieter einer Medienplattform sowie Bedienstete von an ihnen unmittelbar oder mittelbar im Sinne von § 62 beteiligten Unternehmen.
Scheidet ein Mitglied nach Satz 2 aus, berufen die Regierungschefinnen und Regierungschefs der Länder einvernehmlich ein Ersatzmitglied oder einen anderen Sachverständigen für den Rest der Amtsdauer als Mitglied; Entsprechendes gilt, wenn ein Ersatzmitglied ausscheidet.
Die Mitglieder nach Satz 2 erhalten für ihre Tätigkeit eine angemessene Vergütung und Ersatz ihrer notwendigen Auslagen.
Das Vorsitzland der Rundfunkkommission schließt die Verträge mit diesen Mitgliedern.
Der Vorsitzende der KEK und sein Stellvertreter sind aus der Gruppe der Mitglieder nach Satz 1 Nr.
1 zu wählen.
Die sechs Vertreter der Landesmedienanstalten und zwei Ersatzmitglieder für den Fall der Verhinderung eines dieser Vertreter werden durch die Landesmedienanstalten für die Amtszeit der KEK gewählt.

(6) Ein Vertreter der Landesmedienanstalten darf nicht zugleich der KEK und der KJM angehören; Ersatzmitgliedschaft oder stellvertretende Mitgliedschaft sind zulässig.

(7) Die Landesmedienanstalten bilden für die Organe nach Absatz 2 Satz 1 eine gemeinsame Geschäftsstelle.

(8) Die Mitglieder der ZAK, der GVK und der KEK sind bei der Erfüllung ihrer Aufgaben nach diesem Staatsvertrag an Weisungen nicht gebunden.
§ 58 gilt für die Mitglieder der ZAK und GVK entsprechend.
Die Verschwiegenheitspflicht nach § 58 gilt auch im Verhältnis der Mitglieder der Organe nach Absatz 2 Satz 1 zu anderen Organen der Landesmedienanstalten.

(9) Die Organe nach Absatz 2 Satz 1 fassen ihre Beschlüsse mit der Mehrheit ihrer gesetzlichen Mitglieder.
Bei Beschlüssen der KEK entscheidet im Fall der Stimmengleichheit die Stimme des Vorsitzenden, bei seiner Verhinderung die Stimme des stellvertretenden Vorsitzenden.
Die Beschlüsse sind zu begründen.
In der Begründung sind die wesentlichen tatsächlichen und rechtlichen Gründe mitzuteilen.
Die Beschlüsse sind gegenüber den anderen Organen der zuständigen Landesmedienanstalt bindend.
Die zuständige Landesmedienanstalt hat die Beschlüsse im Rahmen der von den Organen nach Absatz 2 Satz 1 gesetzten Fristen zu vollziehen.

(10) Die Landesmedienanstalten stellen den Organen nach Absatz 2 Satz 1 die notwendigen personellen und sachlichen Mittel zur Verfügung.
Die Organe erstellen jeweils einen Wirtschaftsplan nach den Grundsätzen der Wirtschaftlichkeit und Sparsamkeit.
Die Kosten für die Organe nach Absatz 2 Satz 1 werden aus dem Anteil der Landesmedienanstalten nach § 10 des Rundfunkfinanzierungsstaatsvertrages gedeckt.
Näheres regeln die Landesmedienanstalten durch übereinstimmende Satzungen.

(11) Von den Verfahrensbeteiligten sind durch die zuständigen Landesmedienanstalten Kosten in angemessenem Umfang zu erheben.
Näheres regeln die Landesmedienanstalten durch übereinstimmende Satzungen.

(12) Den Organen nach Absatz 2 Satz 1 stehen die Verfahrensrechte nach den §§ 55 und 56 zu.

##### § 105  
Aufgaben

(1) Die ZAK ist für folgende Aufgaben zuständig:

1.  Aufsichtsmaßnahmen gegenüber privaten bundesweiten Veranstaltern, soweit nicht die KEK nach Absatz 3 zuständig ist,
2.  Aufsichtsmaßnahmen gegenüber privaten bundesweiten Anbietern nach den §§ 18 bis 22 sowie nach den §§ 74 bis 77,
3.  Anerkennung von Einrichtungen der Freiwilligen Selbstkontrolle nach § 19 Abs.
    4 sowie Rücknahme oder Widerruf der Anerkennung nach § 19 Abs.
    6,
4.  Aufsicht über Entscheidungen der Einrichtungen der Freiwilligen Selbstkontrolle nach § 19 Abs.
    8,
5.  Zulassung, Rücknahme oder Widerruf der Zulassung bundesweiter Veranstalter nach §§ 53, 108 Abs.
    1 Nr.
    1 und Abs.
    2 Nr.
    1,
6.  Entscheidungen über ein Zulassungserfordernis im Falle des § 54 Abs.
    1,
7.  Feststellung des Vorliegens der Voraussetzungen für Regionalfensterprogramme nach § 59 Abs.
    4 Satz 1 und für Sendezeit für Dritte nach § 65 Abs.
    2 Satz 4,
8.  Anzeige des Betriebs einer Medienplattform oder Benutzeroberfläche nach § 79 Abs.
    2,
9.  Aufsicht über Medienplattformen und Benutzeroberflächen nach den §§ 79 bis 87 sowie § 103 Abs.
    1 und 2, soweit nicht die GVK nach Absatz 2 zuständig ist,
10.  Aufsicht über Medienintermediäre nach den §§ 92 bis 94,
11.  Aufsicht über Video-Sharing-Dienste nach § 98,
12.  Wahrnehmung der Aufgaben nach § 101 Abs.
    3 Satz 1 Nr.
    1 und 3,
13.  Zuweisung von Übertragungskapazitäten für bundesweite Versorgungsbedarfe und deren Rücknahme oder Widerruf nach §§ 102 und 108 Abs.
    1 Nr.
    2 und Abs.
    2 Nr.
    2, soweit die GVK nicht nach Absatz 2 zuständig ist,
14.  Befassung mit Mitteilungen nach § 109 Abs.
    5.

Die ZAK kann Prüfausschüsse für die Aufgaben nach Satz 1 Nr.
1 und 2 einrichten.
Die Prüfausschüsse entscheiden jeweils bei Einstimmigkeit anstelle der ZAK.
Zu Beginn der Amtsperiode der ZAK wird die Verteilung der Verfahren von der ZAK festgelegt.
Das Nähere ist in der Geschäftsordnung der ZAK festzulegen.

(2) Die GVK ist zuständig für Auswahlentscheidungen bei den Zuweisungen von Übertragungskapazitäten nach § 102 Abs.
4 und für die Entscheidung über die Belegung von Plattformen nach § 81 Abs.
5 Satz 3.
Die ZAK unterrichtet die GVK fortlaufend über ihre Tätigkeit.
Sie bezieht die GVK in grundsätzlichen Angelegenheiten, insbesondere bei der Erstellung von Satzungen und Richtlinienentwürfen, ein.

(3) Die KEK ist zuständig für die abschließende Beurteilung von Fragestellungen der Sicherung von Meinungsvielfalt im Zusammenhang mit der bundesweiten Veranstaltung von Fernsehprogrammen.
Sie ist im Rahmen des Satzes 1 insbesondere zuständig für die Prüfung solcher Fragen bei der Entscheidung über eine Zulassung oder Änderung einer Zulassung, bei der Bestätigung von Veränderungen von Beteiligungsverhältnissen als unbedenklich und bei Maßnahmen nach § 60 Abs.
4.
Für Fälle, die für die Sicherung von Meinungsvielfalt nur geringe Bedeutung entfalten können, legt die KEK fest, unter welchen Voraussetzungen auf eine Vorlage nach § 107 Abs.
1 verzichtet werden kann.
Auf Anforderung einer Landesmedienanstalt ist sie zur Prüfung von Einzelfällen verpflichtet.
Die KEK ermittelt die den Unternehmen jeweils zurechenbaren Zuschaueranteile.

(4) Die Auswahl und Zulassung von Regionalfensterprogrammveranstaltern nach § 59 Abs.
4 und Fensterprogrammveranstaltern nach § 65 Abs.
4 sowie die Aufsicht über diese Programme obliegen dem für die Zulassung nicht bundesweiter Angebote zuständigen Organ der zuständigen Landesmedienanstalt.
Bei Auswahl und Zulassung der Veranstalter nach Satz 1 ist zuvor das Benehmen mit der KEK herzustellen.

##### § 106  
Zuständige Landesmedienanstalt

(1) Soweit nachfolgend nichts anderes bestimmt ist, ist für bundesweit ausgerichtete Angebote die Landesmedienanstalt des Landes zuständig, in dem der betroffene Veranstalter, Anbieter, Bevollmächtigte nach § 79 Abs.
1 Satz 2 oder Verantwortliche nach § 18 Abs.
2 seinen Sitz, Wohnsitz oder in Ermangelung dessen seinen ständigen Aufenthalt hat.
Sind nach Satz 1 mehrere Landesmedienanstalten zuständig oder hat der Veranstalter oder Anbieter seinen Sitz im Ausland, entscheidet die Landesmedienanstalt, die zuerst mit der Sache befasst worden ist.

(2) Zuständig in den Fällen des § 105 Abs.
1 Satz 1 Nr.
1, 7, 9 und 14 sowie in den Fällen der Rücknahme oder des Widerrufs der Zulassung oder der Zuweisung ist die Landesmedienanstalt, die dem Veranstalter die Zulassung erteilt, die Zuweisung vorgenommen oder die Anzeige entgegengenommen hat; im Übrigen gilt Absatz 1.
Zuständig im Fall des § 105 Abs.
1 Satz 1 Nr. 10 ist die Landesmedienanstalt des Landes, in dem der Zustellungsbevollmächtigte nach § 92 seinen Sitz hat.
Solange kein Zustellungsbevollmächtigter benannt worden ist, gilt Absatz 1.
Die zuständige Landesmedienanstalt legt die Sache unverzüglich zur Prüfung und Entscheidung der ZAK vor.
Zuständig ist in den Fällen des § 105 Abs.
1 Satz 1 Nr.
3 und 4 die Landesmedienanstalt des Landes, in dem die Einrichtung der Freiwilligen Selbstkontrolle ihren Sitz hat.
Ergibt sich danach keine Zuständigkeit, ist diejenige Landesmedienanstalt zuständig, bei der der Antrag auf Anerkennung gestellt wurde.

(3) Im Übrigen richtet sich die Zuständigkeit nach Landesrecht.

##### § 107  
Verfahren bei Zulassung, Zuweisung und Anzeige

(1) Geht ein Antrag oder eine Anzeige nach § 105 Abs.
1 Satz 1 Nr.
5, 6, 8 oder 13 bei der zuständigen Landesmedienanstalt ein, legt der nach Landesrecht bestimmte gesetzliche Vertreter unverzüglich den Antrag oder die Anzeige sowie die vorhandenen Unterlagen der ZAK und in den Fällen des § 105 Abs.
1 Satz 1 Nr.
5 zusätzlich der KEK vor.

(2) Kann nicht allen Anträgen nach § 105 Abs.
1 Satz 1 Nr.
13 entsprochen werden, entscheidet die GVK.

(3) Absatz 1 gilt entsprechend für die Beurteilung von Fragestellungen der Sicherung von Meinungsvielfalt durch die KEK im Rahmen ihrer Zuständigkeit in anderen Fällen als dem der Zulassung eines bundesweiten privaten Veranstalters.

##### § 108  
Rücknahme, Widerruf von Zulassungen und Zuweisungen

(1) Die Zulassung nach § 53 oder die Zuweisung nach § 102 werden jeweils zurückgenommen, wenn

1.  bei der Zulassung eine Zulassungsvoraussetzung gemäß § 53 Abs.
    1 oder 2 nicht gegeben war oder eine Zulassungsbeschränkung gemäß § 53 Abs.
    3 nicht berücksichtigt wurde oder
2.  bei der Zuweisung die Vorgaben gemäß § 102 Abs.
    4 nicht berücksichtigt wurden

und innerhalb eines von der zuständigen Landesmedienanstalt bestimmten Zeitraums keine Abhilfe erfolgt.

(2) Zulassung und Zuweisung werden jeweils widerrufen, wenn

1.  im Fall der Zulassung
    1.  nachträglich eine Zulassungsvoraussetzung gemäß § 53 Abs.
        1 oder 2 entfällt oder eine Zulassungsbeschränkung gemäß § 53 Abs.
        3 eintritt und innerhalb des von der zuständigen Landesmedienanstalt bestimmten angemessenen Zeitraums keine Abhilfe erfolgt oder
    2.  der Rundfunkveranstalter gegen seine Verpflichtungen aufgrund dieses Staatsvertrages oder des Jugendmedienschutz-Staatsvertrages wiederholt schwerwiegend verstoßen und die Anweisungen der zuständigen Landesmedienanstalt innerhalb des von ihr bestimmten Zeitraums nicht befolgt hat;
2.  im Fall der Zuweisung
    1.  nachträglich wesentliche Veränderungen des Angebots eingetreten und vom Anbieter zu vertreten sind, nach denen das Angebot den Anforderungen des § 102 Abs.
        4 nicht mehr genügt und innerhalb des von der zuständigen Landesmedienanstalt bestimmten Zeitraums keine Abhilfe erfolgt oder
    2.  das Angebot aus Gründen, die vom Anbieter zu vertreten sind, innerhalb des dafür vorgesehenen Zeitraums nicht oder nicht mit der festgesetzten Dauer begonnen oder fortgesetzt wird.

(3) Der Anbieter wird für einen Vermögensnachteil, der durch die Rücknahme oder den Widerruf nach den Absätzen 1 oder 2 eintritt, nicht entschädigt.
Im Übrigen gilt für die Rücknahme und den Widerruf das Verwaltungsverfahrensgesetz des Sitzlandes der jeweils zuständigen Landesmedienanstalt.

##### § 109  
Maßnahmen bei Rechtsverstößen

(1) Stellt die zuständige Landesmedienanstalt einen Verstoß gegen die Bestimmungen dieses Staatsvertrages mit Ausnahme von § 17, § 18 Abs.
2 und 4, § 20 und § 23 Abs.
2 fest, trifft sie die erforderlichen Maßnahmen.
Maßnahmen sind insbesondere Beanstandung, Untersagung, Sperrung, Rücknahme und Widerruf.
Die Bestimmungen des Jugendmedienschutz-Staatsvertrages bleiben unberührt.
Satz 1 gilt nicht für Verstöße gegen § 19 Abs.
1 und 2 von Anbietern,

1.  im Sinne des § 19 Abs.
    1 Satz 1,
2.  die der Selbstregulierung durch den Pressekodex und der Beschwerdeordnung des deutschen Presserates unterliegen oder
3.  die einer anerkannten Einrichtung der Freiwilligen Selbstkontrolle im Sinne des § 19 Abs.
    3 angeschlossen sind.

(2) Eine Untersagung darf nicht erfolgen, wenn die Maßnahme außer Verhältnis zur Bedeutung des Angebots für den Anbieter und die Allgemeinheit steht.
Eine Untersagung darf nur erfolgen, wenn ihr Zweck nicht in anderer Weise erreicht werden kann.
Die Untersagung ist, soweit ihr Zweck dadurch erreicht werden kann, auf bestimmte Arten und Teile von Angeboten oder zeitlich zu beschränken.
Bei journalistisch-redaktionell gestalteten Angeboten, in denen ausschließlich vollständig oder teilweise Inhalte periodischer Druckerzeugnisse in Text oder Bild wiedergegeben werden, ist eine Sperrung nur unter den Voraussetzungen des § 97 Abs.
5 Satz 2 und des § 98 der Strafprozessordnung zulässig.
Die Befugnisse der Aufsichtsbehörden zur Durchsetzung der Vorschriften der allgemeinen Gesetze und der gesetzlichen Bestimmungen zum Schutz der persönlichen Ehre bleiben unberührt.

(3) Erweisen sich Maßnahmen gegenüber dem Veranstalter oder Anbieter als nicht durchführbar oder nicht Erfolg versprechend, können Maßnahmen zur Sperrung von Angeboten nach Absatz 1 auch gegen Dritte unter Beachtung der Vorgaben des Telemediengesetzes gerichtet werden, sofern eine Sperrung technisch möglich und zumutbar ist.
§ 7 Abs.
2 des Telemediengesetzes bleibt unberührt.

(4) Der Abruf von Angeboten im Rahmen der Aufsicht ist unentgeltlich.
Diensteanbieter haben dies sicherzustellen.
Der Anbieter darf seine Angebote nicht gegen den Abruf durch die zuständige Aufsichtsbehörde sperren.

(5) Jede Landesmedienanstalt kann der zuständigen Landesmedienanstalt mitteilen, dass ein bundesweit ausgerichtetes Angebot gegen die Bestimmungen dieses Staatsvertrages verstößt.
Geht eine Mitteilung nach Satz 1 bei der zuständigen Landesmedienanstalt ein, legt der nach Landesrecht bestimmte gesetzliche Vertreter unverzüglich die Mitteilung sowie die vorhandenen Unterlagen dem nach den §§ 104 und 105 zuständigen Organ vor.

##### § 110  
Vorverfahren

Bei Rechtsmitteln gegen Entscheidungen nach § 104 Abs.
2 und § 105 findet ein Vorverfahren nach § 68 Abs. 1 der Verwaltungsgerichtsordnung nicht statt.

##### § 111  
Zusammenarbeit mit anderen Behörden

(1) Die Landesmedienanstalten arbeiten im Rahmen der Erfüllung ihrer Aufgaben mit der Regulierungsbehörde für Telekommunikation und mit dem Bundeskartellamt zusammen.
Die Landesmedienanstalten haben auf Anfrage der Regulierungsbehörde für Telekommunikation oder des Bundeskartellamtes Erkenntnisse zu übermitteln, die für die Erfüllung von deren Aufgaben erforderlich sind.

(2) Absatz 1 gilt für die Zusammenarbeit der Landesmedienanstalten mit den Landeskartellbehörden und den Glücksspielaufsichtsbehörden entsprechend.

##### § 112  
Finanzierung besonderer Aufgaben

(1) Der in § 10 des Rundfunkfinanzierungsstaatsvertrages bestimmte Anteil kann für die Finanzierung folgender Aufgaben verwendet werden:

1.  Zulassungs- und Aufsichtsfunktionen der Landesmedienanstalten einschließlich hierfür notwendiger planerischer, insbesondere technischer Vorarbeiten,
2.  die Förderung offener Kanäle.

Mittel aus dem Anteil nach Satz 1 können aufgrund besonderer Ermächtigung durch den Landesgesetzgeber auch für die Finanzierung folgender Aufgaben verwendet werden:

1.  Förderung von landesrechtlich gebotener technischer Infrastruktur zur Versorgung des Landes und zur Förderung von Projekten für neuartige Rundfunkübertragungstechniken und
2.  Formen der nichtkommerziellen Veranstaltung von lokalem und regionalem Rundfunk und Projekte zur Förderung der Medienkompetenz.

(2) Das Recht des Landesgesetzgebers, der Landesmedienanstalt nur einen Teil des Anteils nach Absatz 1 zuzuweisen, bleibt unberührt.

(3) Soweit der Anteil nach Absatz 1 nicht in Anspruch genommen wird, steht er den jeweiligen Landesrundfunkanstalten zu.
Eine landesgesetzliche Zweckbestimmung ist zulässig.

##### § 113  
Datenschutzaufsicht bei Telemedien

Die nach den allgemeinen Datenschutzgesetzen des Bundes und der Länder zuständigen Aufsichtsbehörden überwachen für ihren Bereich die Einhaltung der allgemeinen Datenschutzbestimmungen und des § 23.
Die für den Datenschutz im journalistischen Bereich beim öffentlich-rechtlichen Rundfunk und bei den privaten Rundfunkveranstaltern zuständigen Stellen überwachen für ihren Bereich auch die Einhaltung der Datenschutzbestimmungen für journalistisch-redaktionell gestaltete Angebote bei Telemedien.
Eine Aufsicht erfolgt, soweit Unternehmen, Hilfs- und Beteiligungsunternehmen der Presse nicht der Selbstregulierung durch den Pressekodex und der Beschwerdeordnung des Deutschen Presserates unterliegen.

### VIII.
Abschnitt  
Revision, Ordnungswidrigkeiten

##### § 114  
Revision zum Bundesverwaltungsgericht

In einem gerichtlichen Verfahren kann die Revision zum Bundesverwaltungsgericht auch darauf gestützt werden, dass das angefochtene Urteil auf der Verletzung der Bestimmungen dieses Staatsvertrages beruhe.

##### § 115  
Ordnungswidrigkeiten

(1) Ordnungswidrig handelt, wer als Veranstalter von bundesweit ausgerichtetem privaten Rundfunk vorsätzlich oder fahrlässig

1.  entgegen § 1 Abs.
    9 die nach Landesrecht zuständige Stelle nicht über alle Änderungen informiert, die die Feststellung der Rechtshoheit nach § 1 Abs.
    3 und 4 berühren könnten,
2.  entgegen § 4 Abs.
    1 die dort genannten Informationen im Rahmen des Gesamtangebots nicht leicht, unmittelbar und ständig zugänglich macht,
3.  entgegen § 8 Abs.
    3 Satz 2 in der Werbung Techniken zur unterschwelligen Beeinflussung einsetzt,
4.  entgegen § 8 Abs.
    3 Satz 3 Rundfunkwerbung oder Teleshopping nicht dem Medium angemessen durch optische oder akustische Mittel oder räumlich eindeutig von anderen Sendungsteilen absetzt,
5.  entgegen § 8 Abs.
    4 Satz 1 eine Teilbelegung des ausgestrahlten Bildes mit Rundfunkwerbung vornimmt, ohne die Werbung vom übrigen Programm eindeutig optisch zu trennen und als solche zu kennzeichnen,
6.  entgegen § 8 Abs.
    5 Satz 2 eine Dauerwerbesendung nicht zu Beginn als Dauerwerbesendung ankündigt oder während ihres gesamten Verlaufs als solche kennzeichnet,
7.  entgegen § 8 Abs.
    6 Satz 1 virtuelle Werbung in Sendungen einfügt,
8.  entgegen § 8 Abs.
    7 Satz 1 Schleichwerbung, Themenplatzierung oder entsprechende Praktiken betreibt,
9.  entgegen § 8 Abs.
    7 Satz 2 Produktplatzierung in Nachrichtensendungen, Sendungen zur politischen Information, Verbrauchersendungen, Regionalfensterprogrammen nach § 59 Abs. 4, Fensterprogrammen nach § 65, Sendungen religiösen Inhalts oder Kindersendungen betreibt,
10.  entgegen § 8 Abs.
    7 Satz 4 oder 5 auf eine Produktplatzierung nicht eindeutig hinweist oder sie nicht zu Beginn und zum Ende einer Sendung oder bei deren Fortsetzung nach einer Werbeunterbrechung oder im Hörfunk durch einen gleichwertigen Hinweis angemessen kennzeichnet,
11.  entgegen § 8 Abs.
    9 Werbung politischer, weltanschaulicher oder religiöser Art verbreitet,
12.  entgegen § 9 Abs.
    1 Übertragungen von Gottesdiensten oder Sendungen für Kinder durch Rundfunkwerbung oder Teleshopping unterbricht,
13.  entgegen den in § 9 Abs.
    3 genannten Voraussetzungen Filme mit Ausnahme von Serien, Reihen und Dokumentarfilmen sowie Kinofilme und Nachrichtensendungen durch Fernsehwerbung oder Teleshopping unterbricht,
14.  entgegen § 10 Abs.
    1 Satz 1 nicht eindeutig auf das Bestehen einer Sponsoring-Vereinbarung hinweist oder nicht eindeutig zu Beginn oder am Ende der gesponserten Sendung auf den Sponsor hinweist,
15.  entgegen § 10 Abs.
    3 und 4 unzulässig gesponserte Sendungen verbreitet,
16.  entgegen § 13 Abs.
    1 oder 3 Großereignisse verschlüsselt und gegen besonderes Entgelt ausstrahlt,
17.  entgegen § 16 Abs.
    1 Satz 2 der Informationspflicht nicht nachkommt,
18.  entgegen § 52 Abs.
    1 Satz 1 ohne Zulassung ein Rundfunkprogramm veranstaltet,
19.  entgegen § 52 Abs.
    1 Satz 1 in Verbindung mit § 53 ein zulassungspflichtiges, aber nicht zulassungsfähiges Rundfunkprogramm veranstaltet,
20.  entgegen § 54 Abs.
    4 Satz 2 in Verbindung mit § 53 ein Rundfunkprogramm veranstaltet,
21.  entgegen § 57 Abs.
    2 in Verbindung mit Abs.
    1 nicht fristgemäß die Aufstellung der Programmbezugsquellen für den Berichtszeitraum der zuständigen Landesmedienanstalt vorlegt,
22.  entgegen § 70 Abs.
    1 Satz 1 die zulässige Dauer der Werbung überschreitet,
23.  entgegen § 71 Abs.
    1 Satz 1 Teleshopping-Fenster verbreitet, die keine Mindestdauer von 15 Minuten ohne Unterbrechung haben oder entgegen § 71 Abs.
    1 Satz 2 Teleshopping-Fenster verbreitet, die nicht optisch und akustisch klar als solche gekennzeichnet sind oder
24.  entgegen § 120 Absatz 1 Satz 2 die bei ihm vorhandenen Daten über Zuschaueranteile auf Anforderung der KEK nicht zur Verfügung stellt.

Ordnungswidrig handelt auch, wer

1.  entgegen § 18 Abs.
    1 bei Telemedien den Namen oder die Anschrift oder bei juristischen Personen den Namen oder die Anschrift des Vertretungsberechtigten nicht oder nicht richtig verfügbar hält,
2.  entgegen § 18 Abs.
    3 bei Telemedien die erforderliche Kenntlichmachung nicht vornimmt,
3.  entgegen § 22 Abs.
    1 Satz 1 Werbung nicht als solche klar erkennbar macht oder nicht eindeutig vom übrigen Inhalt der Angebote trennt,
4.  entgegen § 22 Abs.
    1 Satz 2 in der Werbung unterschwellige Techniken einsetzt,
5.  entgegen § 22 Abs.
    1 Satz 3 bei Werbung politischer, weltanschaulicher oder religiöser Art auf den Werbetreibenden oder Auftraggeber nicht in angemessener Weise deutlich hinweist,
6.  entgegen § 55 Abs.
    6 eine Änderung der maßgeblichen Umstände nach Antragstellung oder nach Erteilung der Zulassung nicht unverzüglich der zuständigen Landesmedienanstalt mitteilt,
7.  entgegen § 55 Abs.
    7 nicht unverzüglich nach Ablauf eines Kalenderjahres der zuständigen Landesmedienanstalt gegenüber eine Erklärung darüber abgibt, ob und inwieweit innerhalb des abgelaufenen Kalenderjahres bei den nach § 62 maßgeblichen Beteiligungs- und Zurechnungstatbeständen eine Veränderung eingetreten ist,
8.  entgegen § 57 Abs.
    1 seinen Jahresabschluss samt Anhang und Lagebericht nicht fristgemäß erstellt oder bekannt macht,
9.  entgegen § 63 Satz 1 es unterlässt, eine geplante Veränderung von Beteiligungsverhältnissen oder sonstigen Einflüssen bei der zuständigen Landesmedienanstalt vor ihrem Vollzug schriftlich anzumelden,
10.  einer Satzung nach § 72 Satz 1 in Verbindung mit § 11 zuwiderhandelt, soweit die Satzung für einen bestimmten Tatbestand auf diese Bußgeldvorschrift verweist,
11.  entgegen § 74 Satz 1 oder 2 in Verbindung mit § 8 Abs.
    3 Satz 2 in der Werbung Techniken zur unterschwelligen Beeinflussung einsetzt,
12.  entgegen § 74 Satz 1 oder 2 in Verbindung mit § 8 Abs.
    3 Satz 3 Rundfunkwerbung entsprechende Werbung oder Teleshopping nicht dem Medium angemessen durch optische oder akustische Mittel oder räumlich eindeutig von anderen Angebotsteilen absetzt,
13.  entgegen § 74 in Verbindung mit § 8 Abs.
    4 das verbreitete Bewegtbildangebot durch die Einblendung von Rundfunkwerbung entsprechender Werbung ergänzt, ohne die Werbung eindeutig optisch zu trennen und als solche zu kennzeichnen,
14.  entgegen § 74 Satz 1 oder 2 in Verbindung mit § 8 Abs.
    5 Satz 2 ein Bewegtbildangebot nicht zu Beginn als Dauerwerbesendung ankündigt oder während ihres gesamten Verlaufs als solche kennzeichnet,
15.  entgegen § 74 Satz 1 oder 2 in Verbindung mit § 8 Abs.
    6 Satz 1 virtuelle Werbung in seine Angebote einfügt,
16.  entgegen § 74 Satz 1 oder 2 in Verbindung mit § 8 Abs.
    7 Satz 1 Schleichwerbung, Themenplatzierung oder entsprechende Praktiken betreibt,
17.  entgegen § 74 Satz 1 oder 2 in Verbindung mit § 8 Abs.
    7 Satz 2 Produktplatzierung in Nachrichtensendungen, Sendungen zur politischen Information, Verbrauchersendungen, Regionalfensterprogrammen nach § 59 Abs.
    4, Fensterprogrammen nach § 65, Sendungen religiösen Inhalts oder Kindersendungen betreibt,
18.  entgegen § 74 Satz 1 oder 2 in Verbindung mit § 8 Abs.
    7 Satz 4 oder 5 auf eine Produktplatzierung nicht eindeutig hinweist oder sie nicht zu Beginn und zum Ende einer Sendung oder bei deren Fortsetzung nach einer Werbeunterbrechung oder im Hörfunk durch einen gleichwertigen Hinweis angemessen kennzeichnet,
19.  entgegen § 74 Satz 1 oder 2 in Verbindung mit § 8 Abs.
    9 Werbung politischer, weltanschaulicher oder religiöser Art verbreitet,
20.  entgegen § 74 Satz 1 oder 2 in Verbindung mit § 9 Abs.
    1 das Bewegtbildangebot eines Gottesdienstes oder ein Bewegtbildangebot für Kinder durch Rundfunkwerbung entsprechende Werbung oder durch Teleshopping unterbricht,
21.  entgegen den in § 74 Satz 1 oder 2 in Verbindung mit § 9 Abs.
    3 genannten Voraussetzungen Filme mit Ausnahme von Serien, Reihen und Dokumentarfilmen sowie Kinofilme und Nachrichtensendungen durch Fernsehwerbung entsprechende Werbung oder durch Teleshopping unterbricht,
22.  entgegen § 74 Satz 1 oder 2 in Verbindung mit § 10 Abs.
    1 Satz 1 bei einem gesponserten Bewegtbildangebot nicht eindeutig auf das Bestehen einer Sponsoring-Vereinbarung hinweist oder nicht eindeutig zu Beginn oder am Ende der gesponserten Sendung auf den Sponsor hinweist,
23.  entgegen § 74 Satz 1 oder 2 in Verbindung mit § 10 Abs.
    3 und 4 unzulässig gesponserte Bewegtbildangebote verbreitet,
24.  entgegen § 79 Abs.
    2 Satz 1 oder 2 den Betrieb einer Medienplattform oder Benutzeroberfläche nicht, nicht rechtzeitig oder nicht vollständig anzeigt oder entgegen § 79 Abs.
    2 Satz 3 in Verbindung mit Satz 1 oder 2 eine wesentliche Änderung nicht, nicht rechtzeitig oder nicht vollständig anzeigt,
25.  entgegen § 80 Abs.
    1 in Verbindung mit Abs.
    2 Rundfunkprogramme, einschließlich des HbbTV-Signals, rundfunkähnliche Telemedien oder Teile davon inhaltlich oder technisch verändert, im Zuge ihrer Abbildung oder akustischen Wiedergabe vollständig oder teilweise mit Werbung, Inhalten aus Rundfunkprogrammen oder rundfunkähnlichen Telemedien, einschließlich Empfehlungen oder Hinweisen hierauf, überlagert oder ihre Abbildung zu diesem Zweck skaliert oder einzelne Rundfunkprogramme oder Inhalte in Angebotspakete aufnimmt oder in anderer Weise entgeltlich oder unentgeltlich vermarktet oder öffentlich zugänglich macht,
26.  entgegen § 81 Abs.
    2 bis 4 die erforderlichen Übertragungskapazitäten für die zu verbreitenden Programme nicht oder in nicht ausreichendem Umfang oder nicht zu den vorgesehenen Bedingungen zur Verfügung stellt oder entgegen § 81 Abs.
    5 Satz 2 auf Verlangen der zuständigen Landesmedienanstalt die Belegung nicht, nicht rechtzeitig oder nicht vollständig anzeigt,
27.  entgegen § 82 Abs.
    2 Rundfunk, rundfunkähnliche Telemedien und Telemedien nach § 19 Abs. 1 beim Zugang zu Medienplattformen unmittelbar oder mittelbar unbillig behindert oder gegenüber gleichartigen Angeboten ohne sachlich gerechtfertigten Grund unterschiedlich behandelt,
28.  entgegen § 82 Abs.
    3 Satz 1 oder 2 die Verwendung oder Änderung eines Zugangsberechtigungssystems oder einer Schnittstelle für Anwendungsprogramme und die Entgelte hierfür der zuständigen Landesmedienanstalt nicht unverzüglich anzeigt oder entgegen § 82 Abs.
    3 Satz 3 der zuständigen Landesmedienanstalt auf Verlangen die erforderlichen Auskünfte nicht erteilt,
29.  entgegen § 83 Abs.
    1 Zugangsbedingungen nicht oder nicht vollständig gegenüber der zuständigen Landesmedienanstalt offenlegt,
30.  entgegen § 83 Abs.
    2 Entgelte oder Tarife nicht so gestaltet, dass auch regionale und lokale Angebote zu angemessenen Bedingungen verbreitet werden können,
31.  entgegen § 84 Abs.
    2 Satz 1 und 2 gleichartige Angebote oder Inhalte bei der Auffindbarkeit, insbesondere der Sortierung, Anordnung oder Präsentation in Benutzeroberflächen, ohne sachlich gerechtfertigten Grund unterschiedlich behandelt oder ihre Auffindbarkeit unbillig behindert oder entgegen § 84 Abs.
    2 Satz 3 nicht alle Angebote mittels einer Suchfunktion diskriminierungsfrei auffindbar macht, soweit der Nachweis nach § 84 Abs.
    7 nicht erbracht ist,
32.  entgegen § 84 Abs.
    3 Satz 1 den in einer Benutzeroberfläche vermittelten Rundfunk nicht in seiner Gesamtheit auf der ersten Auswahlebene unmittelbar erreichbar und leicht auffindbar macht, soweit der Nachweis nach § 84 Abs.
    7 nicht erbracht ist,
33.  entgegen § 84 Abs.
    3 Satz 2 die gesetzlich bestimmten beitragsfinanzierten Programme, die Rundfunkprogramme, die Fensterprogramme (§ 59 Abs.
    4) aufzunehmen haben sowie die privaten Programme, die in besonderem Maß einen Beitrag zur Meinungs- und Angebotsvielfalt im Bundesgebiet leisten, nicht leicht auffindbar macht, soweit der Nachweis nach § 84 Abs. 7 nicht erbracht ist,
34.  entgegen § 84 Abs.
    3 Satz 3 Hauptprogramme mit Fensterprogramm nicht gegenüber dem ohne Fensterprogramm ausgestrahlten Hauptprogramm und gegenüber den Fensterprogrammen, die für andere Gebiete zugelassen oder gesetzlich bestimmt sind, vorrangig darstellt, soweit der Nachweis nach § 84 Abs.
    7 nicht erbracht ist,
35.  entgegen § 84 Abs.
    4 in einer Benutzeroberfläche vermittelte gemeinsame Telemedienangebote der in der ARD zusammengeschlossenen Landesrundfunkanstalten, Telemedienangebote des ZDF sowie des Deutschlandradios oder vergleichbare rundfunkähnliche Telemedienangebote oder Angebote nach § 2 Abs.
    2 Nr.
    14 Buchst.
    b privater Anbieter, die in besonderem Maß einen Beitrag zur Meinungs- und Angebotsvielfalt im Bundesgebiet leisten, oder softwarebasierte Anwendungen, die ihrer unmittelbaren Ansteuerung dienen, im Rahmen der Präsentation rundfunkähnlicher Telemedien oder der softwarebasierten Anwendungen, die ihrer mittelbaren Ansteuerung dienen, nicht leicht auffindbar macht, soweit der Nachweis nach § 84 Abs.
    7 nicht erbracht ist,
36.  entgegen § 84 Abs.
    6 nicht dafür Sorge trägt, dass die Sortierung oder Anordnung von Angeboten oder Inhalten auf einfache Weise und dauerhaft durch den Nutzer individualisiert werden kann, soweit der Nachweis nach § 84 Abs. 7 nicht erbracht ist,
37.  entgegen § 85 Satz 1 die einer Medienplattform oder Benutzeroberfläche zugrunde liegenden Grundsätze für die Auswahl von Rundfunk, rundfunkähnlichen Telemedien und Telemedien nach § 19 Abs.
    1 und für ihre Organisation nicht transparent macht oder entgegen § 85 Satz 3 Informationen hierzu den Nutzern nicht in leicht wahrnehmbarer, unmittelbar erreichbarer und ständig verfügbarer Weise zur Verfügung stellt,
38.  entgegen § 86 Abs.
    1 Satz 1 der zuständigen Landesmedienanstalt auf Verlangen die erforderlichen Unterlagen nicht unverzüglich vorlegt,
39.  entgegen § 86 Abs.
    3 auf Nachfrage gegenüber Anbietern von Rundfunk, rundfunkähnlichen Telemedien oder Telemedien nach § 19 Abs.
    1 die tatsächliche Sortierung, Anordnung und Abbildung von Angeboten und Inhalten, die Verwendung ihrer Metadaten sowie im Rahmen eines berechtigten Interesses Zugangsbedingungen nach § 83 Abs.
    1 nicht mitteilt,
40.  entgegen § 90 Abs.
    2 nicht spätestens sechs Monate nach Inkrafttreten dieses Staatsvertrags die Anzeige nach § 79 Abs.
    2 vornimmt, soweit die Medienplattform oder Benutzeroberfläche bei Inkrafttreten dieses Staatsvertrages bereits in Betrieb aber nicht angezeigt ist,
41.  entgegen § 92 Satz 1 als Anbieter eines Medienintermediärs keinen Zustellungsbevollmächtigten im Inland benennt,
42.  entgegen § 93 Abs.
    1 als Anbieter eines Medienintermediärs die erforderlichen Informationen nicht oder nicht in der vorgeschriebenen Weise verfügbar hält,
43.  entgegen § 93 Abs.
    2 als Anbieter eines Medienintermediärs, der eine thematische Spezialisierung aufweist, diese Spezialisierung durch die Gestaltung seines Angebots nicht wahrnehmbar macht,
44.  entgegen § 93 Abs.
    3 als Anbieter eines Medienintermediärs Änderungen nicht unverzüglich in derselben Weise wahrnehmbar macht,
45.  entgegen § 93 Abs.
    4 als Anbieter eines Medienintermediärs, der soziale Netzwerke anbietet, nicht dafür Sorge trägt, dass Telemedien im Sinne von § 18 Abs.
    3 gekennzeichnet werden,
46.  entgegen § 94 Abs.
    1 als Anbieter eines Medienintermediärs journalistisch-redaktionell gestaltete Angebote, auf deren Wahrnehmbarkeit er besonders hohen Einfluss hat, diskriminiert,
47.  entgegen § 95 als Anbieter eines Medienintermediärs die erforderlichen Unterlagen der zuständigen Landesmedienanstalt auf Verlangen nicht vorlegt,
48.  entgegen § 103 Abs.
    2 Satz 1 oder 3 die Weiterverbreitung von Fernsehprogrammen nicht, nicht rechtzeitig oder nicht vollständig bei der Landesmedienanstalt, in deren Geltungsbereich die Programme verbreitet werden sollen, anzeigt soweit die Anzeige nicht nach § 103 Abs. 2 Satz 2 durch den Anbieter einer Medienplattform vorgenommen wird,
49.  entgegen einer vollziehbaren Anordnung durch die zuständige Aufsichtsbehörde nach § 109 Abs. 1 Satz 2, auch in Verbindung mit Abs.
    4 Satz 1 ein Angebot nicht sperrt oder
50.  entgegen § 109 Abs.
    4 Satz 3 Angebote gegen den Abruf durch die zuständige Aufsichtsbehörde sperrt.

Weitere landesrechtliche Bestimmungen über Ordnungswidrigkeiten bleiben unberührt.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße von bis zu 500.000 Euro, im Falle des Absatzes 1 Satz 2 Nr.
1 mit einer Geldbuße bis zu 50.000 Euro und im Falle des Absatzes 1 Satz 2 Nr. 49 und 50 mit einer Geldbuße bis zu 250.000 Euro geahndet werden.

(3) Zuständige Verwaltungsbehörde im Sinne des § 36 Abs.
1 Nr.
1 des Gesetzes über Ordnungswidrigkeiten ist die nach § 106 zuständige Landesmedienanstalt.
Über die Einleitung eines Verfahrens hat die zuständige Verwaltungsbehörde die übrigen Landesmedienanstalten unverzüglich zu unterrichten.
Soweit ein Verfahren nach dieser Vorschrift in mehreren Ländern eingeleitet wurde, stimmen sich die beteiligten Behörden über die Frage ab, welche Behörde das Verfahren fortführt.

(4) Die Landesmedienanstalt, die einem Veranstalter eines bundesweit ausgerichteten Rundfunkprogramms die Zulassung erteilt hat, kann bestimmen, dass Beanstandungen nach einem Rechtsverstoß gegen Regelungen dieses Staatsvertrages sowie rechtskräftige Entscheidungen in einem Verfahren wegen Ordnungswidrigkeiten nach Absatz 1 von dem betroffenen Veranstalter in seinem Rundfunkprogramm verbreitet werden.
Inhalt und Zeitpunkt der Bekanntgabe sind durch diese Landesmedienanstalt nach pflichtgemäßem Ermessen festzulegen.
Absatz 3 Satz 2 und 3 gilt entsprechend.

(5) Die Verfolgung der in Absatz 1 genannten Ordnungswidrigkeiten verjährt in sechs Monaten.

### IX.
Abschnitt  
Übergangs- und Schlussvorschriften

##### § 116  
Kündigung

(1) Dieser Staatsvertrag gilt für unbestimmte Zeit.
Der Staatsvertrag kann von jedem der vertragschließenden Länder zum Schluss des Kalenderjahres mit einer Frist von einem Jahr gekündigt werden.
Die Kündigung kann erstmals zum 31. Dezember 2022 erfolgen.
Wird der Staatsvertrag zu diesem Termin nicht gekündigt, kann die Kündigung mit gleicher Frist jeweils zu einem zwei Jahre späteren Termin erfolgen.
Die Kündigung ist gegenüber der oder dem Vorsitzenden der Konferenz der Regierungschefinnen und Regierungschefs der Länder schriftlich zu erklären.
Kündigt ein Land diesen Staatsvertrag, kann es zugleich den Rundfunkbeitragsstaatsvertrag und den Rundfunkfinanzierungsstaatsvertrag zum gleichen Zeitpunkt kündigen; jedes andere Land kann daraufhin innerhalb von sechs Monaten nach Eingang der Kündigungserklärung dementsprechend ebenfalls zum gleichen Zeitpunkt kündigen.
Zwischen den übrigen Ländern bleiben diese Staatsverträge in Kraft.

(2) Im Falle der Kündigung verbleibt es bei der vorgenommenen Zuordnung der Satellitenkanäle, solange für diese Kanäle noch Berechtigungen bestehen.
Die §§ 27 bis 30 bleiben im Falle der Kündigung einzelner Länder unberührt.

(3) § 13 Abs.
1 und 2 kann von jedem der vertragschließenden Länder auch gesondert zum Schluss des Kalenderjahres mit einer Frist von einem Jahr gekündigt werden.
Die Kündigung kann erstmals zum 31.
Dezember 2022 erfolgen.
Wird § 13 Abs.
1 und 2 zu diesem Zeitpunkt nicht gekündigt, kann die Kündigung mit gleicher Frist jeweils zu einem zwei Jahre späteren Zeitpunkt erfolgen.
Die Kündigung ist gegenüber der oder dem Vorsitzenden der Konferenz der Regierungschefinnen und Regierungschefs der Länder schriftlich zu erklären.
Kündigt ein Land, kann jedes Land innerhalb von drei Monaten nach Eingang der Kündigungserklärung § 13 Abs.
1 und 2 zum gleichen Zeitpunkt kündigen.
Die Kündigung eines Landes lässt die gekündigten Bestimmungen dieses Staatsvertrages im Verhältnis der übrigen Länder zueinander unberührt.

(4) § 34 Abs.
2 kann von jedem der vertragsschließenden Länder auch gesondert zum Schluss des Kalenderjahres mit einer Frist von einem Jahr gekündigt werden.
Die Kündigung kann erstmals zum 31. Dezember 2022 erfolgen.
Wird § 34 Abs.
2 zu diesem Zeitpunkt nicht gekündigt, kann die Kündigung mit gleicher Frist jeweils zu einem zwei Jahre späteren Zeitpunkt erfolgen.
Die Kündigung ist gegenüber der oder dem Vorsitzenden der Konferenz der Regierungschefinnen und Regierungschefs der Länder schriftlich zu erklären.
Kündigt ein Land, kann jedes Land innerhalb von drei Monaten nach Eingang der Kündigungserklärung den Rundfunkstaatsvertrag, den ARD-Staatsvertrag, den ZDF-Staatsvertrag, den Staatsvertrag über die Körperschaft des öffentlichen Rechts „Deutschlandradio“, den Rundfunkfinanzierungsstaatsvertrag und den Rundfunkbeitragsstaatsvertrag zum gleichen Zeitpunkt kündigen.
Die Kündigung eines Landes lässt die gekündigten Bestimmungen dieses Staatsvertrages und die in Satz 5 aufgeführten Staatsverträge im Verhältnis der übrigen Länder zueinander unberührt.

(5) § 39 Abs.
1, 2 und 5 kann von jedem der vertragsschließenden Länder auch gesondert zum Schluss des Kalenderjahres, das auf die Ermittlung des Finanzbedarfs des öffentlich-rechtlichen Rundfunks gemäß § 36 folgt, mit einer Frist von sechs Monaten gekündigt werden, wenn der Rundfunkfinanzierungsstaatsvertrag nicht nach der Ermittlung des Finanzbedarfs gemäß § 36 aufgrund einer Rundfunkbeitragserhöhung geändert wird.
Die Kündigung kann erstmals zum 31. Dezember 2022 erfolgen.
Wird § 39 Abs.
1, 2 und 5 zu einem dieser Termine nicht gekündigt, kann die Kündigung mit gleicher Frist jeweils zu einem zwei Jahre späteren Termin erfolgen.
Die Kündigung ist gegenüber der oder dem Vorsitzenden der Konferenz der Regierungschefinnen und Regierungschefs der Länder schriftlich zu erklären.
Kündigt ein Land, kann jedes Land innerhalb von drei Monaten nach Eingang der Kündigungserklärung den Rundfunkbeitragsstaatsvertrag und den Rundfunkfinanzierungsstaatsvertrag zum gleichen Zeitpunkt kündigen.
In diesem Fall kann jedes Land außerdem innerhalb weiterer drei Monate nach Eingang der Kündigungserklärung nach Satz 5 die §§ 36 und 46 hinsichtlich einzelner oder sämtlicher Bestimmungen zum gleichen Zeitpunkt kündigen.
Zwischen den übrigen Ländern bleiben die gekündigten Bestimmungen dieses Staatsvertrages und die in Satz 5 aufgeführten Staatsverträge in Kraft.

##### § 117  
Übergangsbestimmung für Produktplatzierungen

§ 8 Abs.
7 und § 38 gelten nicht für Sendungen, die vor dem 19.
Dezember 2009 produziert wurden.

##### § 118  
Übergangsbestimmung für Telemedienkonzepte

Die zum 1.
Mai 2019 nach § 32 Abs.
7 veröffentlichten Telemedienkonzepte bleiben unberührt.

##### § 119  
Übergangsbestimmung für Zulassungen und Anzeigen

(1) Bei Zulassungen, die vor Inkrafttreten dieses Staatsvertrages erteilt wurden, und Zulassungsverlängerungen bleibt die zulassungserteilende Landesmedienanstalt zuständig.
Gleiches gilt für Medienplattformen und Benutzeroberflächen, die vor Inkrafttreten dieses Staatsvertrages angezeigt wurden.

(2) Absatz 1 gilt nur für bundesweit ausgerichtete Angebote.

##### § 120  
Übergangsbestimmung zur Bestimmung der Zuschaueranteile

(1) Bis zur ersten Bestimmung der Zuschaueranteile nach § 61 sind für die Beurteilung von Fragestellungen der Sicherung der Meinungsvielfalt in Zusammenhang mit der bundesweiten Veranstaltung von Fernsehprogrammen die vorhandenen Daten über Zuschaueranteile zugrunde zu legen.
Die Veranstalter sind verpflichtet, bei ihnen vorhandene Daten über Zuschaueranteile auf Anforderung der KEK zur Verfügung zu stellen.
Die Landesmedienanstalten haben durch Anwendung verwaltungsverfahrensrechtlicher Regelungen unter Beachtung der Interessen der Beteiligten sicherzustellen, dass Maßnahmen nach diesem Staatsvertrag, die aufgrund von Daten nach Satz 1 ergehen, unverzüglich an die sich aufgrund der ersten Bestimmung der Zuschaueranteile nach § 61 ergebende Sach- und Rechtslage angepasst werden können.

(2) Absatz 1 gilt nur für bundesweit ausgerichtete Angebote.

##### § 121  
Übergangsbestimmung für Benutzeroberflächen

§ 84 Abs.
3 bis 6 gilt ab dem 1.
September 2021.

##### § 122  
Regelung für Bayern

Der Freistaat Bayern ist berechtigt, eine Verwendung des Anteils am Rundfunkbeitrag nach § 112 zur Finanzierung der landesgesetzlich bestimmten Aufgaben der Bayerischen Landeszentrale für Neue Medien im Rahmen der öffentlich-rechtlichen Trägerschaft vorzusehen.
Im Übrigen finden die für private Veranstalter geltenden Bestimmungen dieses Staatsvertrages auf Anbieter nach bayerischem Recht entsprechende Anwendung.
Abweichende Regelungen zu § 8 Absatz 9 Satz 1 1.
Variante zur Umsetzung von Vorgaben der Landesverfassung sind zulässig.

**Anlage  
**(zu § 28 Abs.
1 Nr.
2 des Medienstaatsvertrages)

**Programmkonzept Digitale Fernsehprogramme der ARD**

**I.
Einleitung**

§ 11b Rundfunkstaatsvertrag (Fernsehprogramme) legt in Abs.
1 Nr.
2 fest, dass die in der ARD zusammengeschlossenen Landesrundfunkanstalten drei Spartenfernsehprogramme veranstalten, und zwar die Programme „EinsExtra“, „EinsPlus" und „EinsFestival“.
Auf diese Programme bezieht sich das nachfolgend dargestellte Programmkonzept.
Die Notwendigkeit hierzu ergibt sich aus der Entscheidung der Europäischen Kommission vom 24.
April 2007 in dem Beihilfeverfahren über die Finanzierung des öffentlich-rechtlichen Rundfunks in Deutschland.
In dieser Entscheidung vertritt die Kommission die Auffassung, dass die den öffentlich-rechtlichen Rundfunkanstalten eingeräumte Möglichkeit, digitale Zusatzkanäle im Fernsehen anzubieten, nach dem zum Zeitpunkt der Entscheidung geltenden Recht nicht hinreichend präzise abgegrenzt sei ( Rdnr. 228).
Deswegen verlangt die Kommission, dass durch die Vorgabe allgemeiner rechtlicher Anforderungen und die Entwicklung hinreichend konkreter Programmkonzepte gewährleistet wird, dass der Umfang des öffentlich-rechtlichen Auftrags der Rundfunkanstalten in Bezug auf die digitalen Zusatzkanäle klar bestimmt ist (Rdnr.
309).
Schließlich sieht die Kommission die Entwicklung von Programmkonzepten durch die öffentlich-rechtlichen Rundfunkanstalten auf staatsvertraglicher Grundlage als geeignet für eine hinreichend konkrete Auftragsbestimmung im Sinne des europäischen Rechts an (Rdnr.
360).
Vor diesem Hintergrund präzisiert die ARD das Konzept für ihre digitalen Zusatzkanäle wie nachstehend ausgeführt.

**II.
Gemeinsame Grundsätze für die digitalen Fernsehkanäle der ARD**

Mit EinsExtra, EinsPlus und EinsFestival verfügt die ARD über drei digitale Kanäle, die eine größere Vielfalt und höhere themenorientierte Qualität des öffentlich-rechtlichen Programmangebots gewährleisten.
Dem Zuschauer wird durch die verstärkte Diversifizierung, ergänzt durch verschiedene interaktive Dienste und Zusatzangebote, ein deutlicher komplementärer programmlicher Mehrwert geboten.
Die hochwertigen Angebote der Digitalkanäle richten sich grundsätzlich an alle Altersund Zielgruppen.
Durch die Digitalisierung der Verbreitungstechniken im dualen Rundfunksystem verändert sich die Fernsehnutzung.
Dem tragen die ARD-Digitalkanäle durch klar profilierte Angebote Rechnung, die im Rahmen eines Vollprogramms nicht möglich sind.
EinsExtra, EinsFestival und EinsPlus erreichen mit einem entsprechend profilierten Programm und begleitet durch ein relevantes Angebot im Bereich der Telemedien auch jüngere Zuschauer.
So bietet EinsExtra ein 24-stündiges Informationsangebot mit einem hohen tagesaktuellen Anteil.
EinsPlus nutzt die Dialog- und Partizipationsmöglichkeiten des Internets, nach der TV\-Ausstrahlung sind Service- und Wissensangebote für die Nutzer crossmedial auch auf anderen Plattformen zeit- und ortsunabhängig verfügbar.
EinsFestival ist ein innovatives, kulturell orientiertes öffentlichrechtliches Angebot, das einen wichtigen Beitrag dazu leistet, jüngere Zielgruppen anzusprechen.
Das Erreichen jüngerer Zuschauerinnen und Zuschauer ist also eine wesentliche Zielsetzung bei der Erfüllung des öffentlich-rechtlichen Auftrags in der digitalen Welt.
Vor allem jüngere Menschen fragen öffentlich-rechtliche Qualitätsangebote zunehmend im Internet ab.
Um sie in relevantem Maße zu erreichen, muss eine multimediale Vernetzung gewährleistet sein.
Die dynamische technische Entwicklung, vor allem die Nutzung des Internetprotokolls für die Verbreitung von Rundfunk- und Fernsehprogrammen, erfordert ein entsprechendes Angebot.

Eine wichtige Voraussetzung im Rahmen seines Funktionsauftrages und für die Akzeptanz des öffentlich-rechtlichen Rundfunks in der Informations- und Wissensgesellschaft ist auch eine entsprechende zeitunabhängige Bereitstellung der Angebote, da er nur so seiner gesellschaftlichen Verantwortung, insbesondere gegenüber einer jüngeren Zielgruppe, gerecht werden kann.
Dazu gehören die Verbindung von Text, Bild und Ton, aber auch sendungsbezogene beziehungsweise an Programm- oder Sendermarken ausgerichtete interaktive Angebote wie z.B. redaktionell begleitete Chats, Foren, Rankings, Bewertungen und sendungsbezogene spielerische Elemente.
So bieten EinsFestival und EinsPlus mit jeweils vollwertigen Teletextangeboten bzw.
mit der Info-Leiste bei EinsExtra, vertiefende fernsehbasierte Begleitdienste, die durch interaktiv nutzbare programm- und sendungsbezogene Vorschau-Angebote ergänzt werden.
Hinzu kommt, dass die Nutzer im Internet neue Formen der Partizipation erwarten, z.B.
Communities, Weblogs und Plattformen für den Austausch von Inhalten.

Orientiert an den staatsvertraglichen Vorgaben bieten die Digitalkanäle im Internet einen Kommunikationsraum für die Vertiefung von Themen, die im Programm gesetzt worden sind.
Dies ist ein frei zugängliches Angebot für jedermann, während viele kommerziell betriebene Bereiche des Internet sich nur nach Zahlung von Entgelten nutzen lassen.
Die Online-Angebote der ARD-Digitalkanäle sind an dem jeweiligen Programmangebot ausgerichtet, wobei sich die Inhalte am Erwartungshorizont der Zielgruppe orientieren.
Das Verweildauerkonzept richtet sich nach den zukünftigen staatsvertraglichen Vorgaben und dem auf deren Grundlage zu erstellenden Telemedienkonzept.

Als Testfläche und Probebühne innovativer Formate erfüllen die Digitalkanäle zugleich eine weitere wichtige Aufgabe:

Nur der öffentlich-rechtliche Rundfunk kann jenseits kommerzieller Interessen das Fernsehen dramaturgisch und ästhetisch unabhängig sowie im Interesse der Zuschauer weiterentwickeln.
Der Austausch mit den Hörfunkwellen der Landesrundfunkanstalten führt darüber hinaus zu Synergien, z.B.
in der Themenfindung, in der Formatentwicklung und durch Zusammenarbeit von Programmmitarbeiterinnen und -mitarbeitern.

**III.
Die Konzepte der einzelnen digitalen Fernsehprogramme**

1.  **EinsFestival**
    1.  **Grundkonzeption**
        
        EinsFestival ist - wie in den ARD-Programmleitlinien 07/08 beschrieben - ein innovatives, kulturell orientiertes Angebot mit jüngerer Ausrichtung.
        Das Programmangebot ist zwar grundsätzlich an einen breiten Zuschauerkreis gerichtet, es orientiert sich strukturell und inhaltlich aber an der Alltagskultur eines jüngeren Publikums und hat insofern nicht den Anspruch eines Vollprogramms.
        EinsFestival leistet damit einen wichtigen Beitrag dazu, bei jüngeren Menschen mehr Aufmerksamkeit für öffentlich-rechtliche Programmangebote zu erreichen.
        
        In einem Fernsehangebot, das an die Lebenswelt junger Menschen anknüpft, wird deren große Bandbreite abwechslungsreich dargestellt und präsentiert.
        Der Programmgestaltung von EinsFestival liegt daher ein breiter Kulturbegriff zugrunde.
        Sie wird besonders durch Film, Musik, Sport, Wissen, Medien und Kommunikation geprägt.
        Tagesaktuelle vertiefende lnformationsangebote, z.B.
        aus den Bereichen Innen- und Außenpolitik, Wirtschafts- und Finanzpolitik, klassische Service- und Ratgeberangebote, Kinderprogramme und regelmäßige Berichterstattung von Sportveranstaltungen sind nicht Bestandteil der regulären Programmgestaltung von EinsFestival.
        
        Bei EinsFestival steht vor allem der Wunsch nach Orientierung und einem eigenen Lebensstil jüngerer Menschen im Mittelpunkt.
        Deshalb muss ein Angebot für diese Zielgruppe alle adäquaten Gestaltungsformen des Mediums nutzen und durch eine attraktive Online- Präsenz ergänzen und vertiefen.
        Der Einsatz jüngerer Moderatorinnen und Moderatoren ist ein weiterer Teil des Programmkonzepts.
        
    2.  **Programminhalte**
        
        **(1) Film und Serie**
        
        Fiktionale Angebote sind wesentlicher Bestandteil des Programmprofils von Eins-Festival.
        Für die jüngere Kulturgeschichte ist die Entwicklung in den Bereichen Film und Fernsehen zentral.
        Fernsehfilmen, Spielfilmen, nationalen und internationale Serien sowie innovativen, unterhaltenden Formaten kommt in der Alltagskultur jüngerer Menschen eine besondere Bedeutung zu.
        
        **(2) Dokumentation und Reportage**
        
        Darüber hinaus haben hochwertige Dokumentationen und Reportagen sowie Magazinformate einen wichtigen Stellenwert.
        In allen Fällen kommt es darauf an, relevante und teilweise schwer zu vermittelnde Themen durch eine entsprechende und junge Erzählweise auch jüngeren Menschen zu erschließen.
        Kein Medium eignet sich dafür besser als das Fernsehen.
        EinsFestival gibt insoweit Orientierung durch die Auswahl der Themen und erleichtert den Zugang durch eine große Bandbreite klassischer und innovativer Vermittlungsformen.
        
        **(3) Musik und Unterhaltung**
        
        Unterhaltung bei EinsFestival ist innovativ und zielgruppenspezifisch.
        Sie findet ihre Anknüpfungspunkte in der Alltagskultur jüngerer Menschen und grenzt sich dadurch von Unterhaltungsangeboten der Vollprogramme ab, die den Anspruch haben, ein breites Mainstream-Publikum anzusprechen.
        
        EinsFestival nutzt auch Schätze aus den Archiven.
        Zum Spektrum des Programms gehören auch Kabarett- und Comedysendungen, Unterhaltungsshows und Unterhaltungsgalas aus allen Jahrzehnten, die Fernsehgeschichte geschrieben haben und somit fest zum Repertoire moderner Fernsehkultur und damit auch zum Kulturverständnis einer jüngeren Zielgruppe gehören.
        Das gilt auch für den Bereich Musik, insbesondere für die verschiedenen Richtungen moderner Pop- und Rock-Musik.
        
        **(4) Sport**
        
        Viele Sportarten kommen gar nicht auf den Bildschirm, obwohl Rechte und Bilder vorliegen.
        EinsFestival sendet auch im Fernsehen ansonsten weniger populäre Sportereignisse, in der Vergangenheit zum Beispiel den America's Cup.
        Bei sportlichen Großereignissen dient EinsFestival als ,,Überlaufbecken“ für die Übertragung des ERSTEN, in der Vergangenheit zum Beispiel anlässlich der Fußball EM 2008 oder der Olympischen Spiele in Turin und in Peking.
        
        **(5 ) Wissen**
        
        EinsFestival bietet Orientierung in der modernen Wissensgesellschaft.
        Der Kanal greift wichtige Themen aus der Lebenswirklichkeit junger Menschen auf, ordnet ein und bietet damit eine wertvolle Grundlage für eine kritische und freie Meinungsbildung.
        Dieser Teil des öffentlich-rechtlichen Programmauftrags wird gerade von jüngeren Menschen besonders genutzt und eingefordert.
        
    3.  **Organisation und Entscheidungsstrukturen**
        
        EinsFestival wird vom Westdeutschen Rundfunk Köln federführend für die ARD betrieben.
        
2.  **EinsPlus**
    1.  **Grundkonzeption**
        
        EinsPlus ist ein Fernsehprogramm mit Service-Charakter, das grundsätzliches Wissen über wissenschaftliche, gesellschaftliche und ökonomische Zusammenhänge vermittelt.
        
        In jüngerer Zeit wurde es zu einem öffentlich-rechtlichen Service-, Ratgeber- und Wissensangebot weiterentwickelt, das schnell Akzeptanz bei den Fernsehzuschauern gefunden hat.
        EinsPlus positioniert sich als modernes, generationsübergreifendes Familienprogramm, das während des ganzen Jahres ,,Public Value" und praktischen Mehrwert bietet.
        
        Ziel von EinsPlus ist, im Sinne des öffentlich-rechtlichen Auftrages, Orientierung und Lebenshilfe zu geben, Wissen zu vermitteln, das den Alltag meistern hilft und die Zuschauer zu mündigen Bürgen und Verbrauchern macht.
        
        In einer unübersichtlicher werdenden Programmwelt stellt EinsPlus damit für den Zuschauer einen wichtigen und verlässlichen Qualitätsanker dar.
        EinsPIus bündelt die gesamte Kompetenz der ARD auf dem Programmfeld Service-, Ratgeber- und Wissensformate und entwickelt originäre Formate als zusätzlichen programmlichen Mehrwert.
        
    2.  **Programminhalte**
        
        **(1) Service und Ratgeber**
        
        Information und Orientierung, unabhängig von kommerziellen Interessen, sind von zentraler Bedeutung für Zusammenhalt und demokratische Entwicklung unserer Gesellschaft.
        
        EinsPlus dient den Zuschauern als unabhängige Plattform zum Austausch über das ihr Leben mitbestimmende Geschehen auf Märkten, steht für eine kritische und freie Meinungsbildung auch in der Welt der Waren und Dienstleistungen.
        
        Als Begleiter des Zuschauers durch den Alltag greift EinsPlus Themen aus der Lebenswirklichkeit der Menschen auf: Gesundheit, Reise, Technik, Ernährung/Kochen, Natur, Leben, Wissen - und verbindet grundsätzliches Wissen mit konkreten Problemlösungsstrategien.
        Das Angebot hebt sich deutlich von dem der kommerziellen Konkurrenz ab.
        Sendungen wie ,,Servicezeit: Familie" (WDR), „ARDRatgeber: Technik" (NDR), „Hauptsache gesund" (MDR), „Schätze der Welt" (SWR), „Plusminus" (BR, HR, MDR, NDR, SR, SWR, WDR), „frauTV“ (WDR) und „Service: Familie" (HR) sind Beispiele für unabhängigen, professionellen Journalismus mit praktischem Mehrwert.
        
        **(2) Wissen**
        
        Als Service-, Ratgeber- und Wissenskanal leistet EinsPlus einen Beitrag zur Entwicklung einer modernen Wissensgesellschaft.
        Mit Sendungen wie „Odysso" (SWR), „Faszination Wissen" (BR), „Planet Wissen" (SWR, WDR, BR), „W wie Wissen" (BR, HR, NDR, SWR, WDR) und ,,Ozon“ (RBB) erweitert EinsPIus Wissenshorizonte, trägt zum Verständnis der modernen Welt bei und unterstützt die Menschen auf ihrem Weg in die moderne Wissensgesellschaft.
        EinsPlus-Sendungen vermitteln zudem Kenntnisse der neuen digitalen Kommunikationstechnologien, hinterfragen kritisch auch deren Risiken - wie den leichtfertigen Exhibitionismus Jugendlicher im Internet - und tragen zur Medienkompetenz der Zuschauer bei.
        EinsPlus stellt auf diese Weise ein Wissensportal für die ganze Familie dar.
        Gezielt kooperiert EinsPlus mit Institutionen aus dem Bildungs- und Wissenschaftssektor.
        
    3.  **Programmstruktur**
        
        **(1) Grundstruktur**
        
        Die Grundstruktur des Programms setzt sich aus unterschiedlichen thematischen Bausteinen zusammen: Gesundheit, Natur, Reise, ErnährungIKochen, Leben, Wissen und Technik.
        In diesen Themenfeldern bietet EinsPlus jeweils eine Auswahl hochwertiger Produktionen des Ersten und der Dritten Programme der ARD.
        Um auf die speziellen Publikumserwartungen und Sehgewohnheiten am Wochenende einzugehen, präsentiert EinsPlus sonntags lineares Programm mit 3-4-stündigen thematischen Schwerpunkten und Reihen wie z.B.: „DeutschlandTour", EuropaTour" oder „Geschichte der 0lympischen Spiele", „Faszination Berge", ,,Museen der Welt", ,,Inseln", „Straßen der Welt" oder „Die Donau".
        
        Thementage und Themenwochen profilieren das Angebot zusätzlich und schaffen mehr Aufmerksamkeit für das Programm.
        Die Eins-Plus-Schwerpunkte zu Themen, zum Teil von hoher gesellschaftlicher Relevanz sind vielfältig, lebensnah und prägnant gestaltet: Reportagen, vertiefende Diskussionen, Doku-Serien, Dokumentarspiele, unterhaltende Sendungen mit eindeutigem Wissensbezug und hochwertige fiktionale Produktionen, die politisches und geschichtliches Wissen transportieren, gehören zum Formatspektrum.
        
        **(2) Originäre EinsPlus-Produktionen**
        
        EinsPlus verfolgt eine klare Mehrwertstrategie im digitalen Markt und stellt dabei den unmittelbaren Nutzen für den Fernsehzuschauer in den Mittelpunkt.
        Originäre, profilbildende Programm-Marken („Leuchttürme") sind in diesem Zusammenhang zum Beispiel:
        
        *   ein Servicemagazin mit wertvollen Hintergrundinformationen, kreativen Anregungen und praktischen Tipps für Zuschauer aller Altersschichten,
        *   ein Programmformat für Werte-, Glaubens- und Lebensberatungsthemen,
        *   ein Wissensformat, das Wissensthemen aus verschiedenen, teils ungewöhnlichen Blickwinkeln beleuchtet und Wissenschaft unkompliziert und spannend vermittelt.
        
        Dazu kommen eigene Produktionen zu Themen wie Erziehung, Tiere und Natur, Umwelt und Energie, Kochen.
        So ist EinsPlus in seiner Programmierung aktuell und exklusiv.
        
    4.  **Organisation**
        
        Die Federführung für das ARD-Gemeinschaftsprogramm EinsPlus liegt beim Südwestrundfunk, der dafür in Baden-Baden eine Redaktion unterhält.
        
3.  **EinsExtra**
    1.  **Grundkonzeption**
        
        „EinsExtra“ ist der digitale Informationskanal der ARD.
        Ziel ist es, „EinsExtra“ unter dem organisatorischen Dach von ARD-aktuell und unter der Qualitäts-Marke „Tagesschau“ zu einem umfassenden Informationsprogramm mit einem verlässlichen Nachrichtenservice für alle Nutzungsformen und Verbreitungswege weiter zu entwickeln.
        Kein anderer Programmanbieter verfügt über ein vergleichbares Netz von Reportern und Korrespondenten wie die ARD.
        Ihre aktuellen Berichte werden unter Nutzung von Synergien in einer integrierten Nachrichtenredaktion multimedial und plattformgerecht aufbereitet und verfügbar gemacht.
        Die ARD nimmt damit im öffentlich-rechtlichen Kernbereich „Information“ ihre Aufgabe und Verantwortung wahr, jederzeit frei verfügbare, zeitgemäße, dem hohen Anspruch von ARD-aktuell entsprechende Nachrichtenangebote für alle bereitzustellen.
        
    2.  **Tragende Programmelemente**
        
        Kernangebot des Kanals ,,EinsExtra“ ist das Nachrichtenangebot ,,EinsExtra aktuell", das seine Nachrichten zur Zeit im Viertelstundentakt anbietet, weil nach Erkenntnissen der Medienforschung informationsinteressierte Zuschauer entsprechende Programme nur kurz, dafür aber häufiger am Tag einschalten.
        
        Mit .Hilfe digitaler Technik bereitet ARD-aktuell Reporter-Beiträge aus „Tagesschau", „Tagesthemen" und „Nachtmagazin" auf.
        Eigenproduzierte Berichte und Interviews ergänzen die Berichterstattung über das Tagesgeschehen.
        Zudem werden für „EinsExtra aktuell"auch die Medien vernetzt und die Ressourcen des Hörfunks genutzt.
        Beiträge der Nachrichtenwellen wie NDRlnfo, mdrInfo oder B5aktuell werden bebildert und dann gesendet.
        
        Jede Viertelstunde in „EinsExtra Aktuell" beginnt zurzeit mit einem Nachrichtenüberblick in 100 Sekunden - und wird abgerundet von den Ressorts.
        Sie bieten Hintergründe und vertiefende Informationen zu Themenbereichen wie Kultur, Wirtschaft, Sport oder Europa.
        Dabei greift EinsExtra auf bereits gesendete Berichte aus Sendungen der Landesrundfunkanstalten zurück.
        
        Die Digitalisierung der Programme ermöglicht aber auch Angebote, die über das herkömmliche Programmangebot hinausgehen, also einen Mehrwert für den Zuschauer bilden.
        EinsExtra bietet deshalb - in Zusammenarbeit mit der Internetredaktion der Tagesschau - ständig aktualisierte Informationen auch außerhalb des speziellen Nachrichtenformats an.
        Sie werden in einer sogenannten „Infoleiste“ zusätzlich zum Programm eingeblendet und ebenfalls ständig von tagesschau.de aktualisiert.
        
    3.  **Einzelne Sendungen / Formate**
        
        In der Primetime und am Wochenende wird dieses Informationsangebot zurzeit durch die Übernahme aller Tagesschauausgaben, von Tagesthemen, Nachtmagazin und Wochenspiegel ergänzt.
        Am Morgen wird das ARD Morgenmagazin gesendet.
        Durch die Übernahme von Brennpunkten und aktuellen politischen Sondersendungen im „Ersten“ oder in den Dritten Programmen, in der Regel live, wird das Informationsprofil von EinsExtra weiter geschärft.
        
        Über die aktuelle Berichterstattung in den Nachrichten hinaus nutzt EinsExtra die für „Das Erste" und die Dritten Programme produzierten Politik- und Wirtschafts-Magazine sowie Gesprächssendungen, um aktuelle politische Hintergrund-Informationen aufzuarbeiten.
        
        Das „EinsExtra -Thema“ am Samstag und Sonntag in der Zeit von 18 bis 20 Uhr wendet sich zur Zeit an Zuschauer, die an weiterführenden und einordnenden Informationen zu aktuellen Ereignissen, Jahrestagen etc.
        interessiert sind.
        Hierzu werden die hochwertigen für „Das Erste" oder die Dritten Programme produzierten Reportagen genutzt und durch Gesprächsendungen ergänzt.
        Auch die Feiertagsprogramme beschäftigen sich inhaltlich-thematisch mit politischen Ereignissen.
        
        Die regionale Berichterstattung bildet eine wichtige Säule im EinsExtra-Programm.
        Regionalmagazine der Landesrundfunkanstalten werden am Wochenende und auf der Nachtschiene gesendet.
        
        Formate wie „EinsExtra Info" oder „EinsExtra unkommentiert" dienen der Aktualisierung des Programms und erlauben gleichzeitig Lücken zu schließen, die sich aus der Struktur des Programms ergeben.
        
        Die Ausstrahlung der „Tagesschau vor 20 Jahren" bietet dem Zuschauer darüber hinaus einen historischen Nachrichtenrückblick.
        
        So ergänzt und umschließt das Rahmenprogramm den Nachrichtenkern von EinsExtra und macht das Programm zu einem vollwertigen politischen Informationsangebot.
        
    4.  **Organisation**
        
        EinsExtra wird vom Norddeutschen Rundfunk federführend für die ARD betrieben und von der Hauptabteilung ARD-Aktuell redaktionell betreut.
        

**IV.
Produktion**

Die Digitalkanäle sind insgesamt auch ein wichtiges Versuchsfeld für technische Innovationen innerhalb der ARD.
Beispielsweise gibt es bereits heute auf Eins- Festival HDTV-Testausstrahlungen im Rahmen von Showcases.
Entsprechend werden verstärkt Rechte an HD-Produktionen erworben und Archiv-Schätze in HD-Qualität aufbereitet.
Die Digitalkanäle nutzen Synergien durch effiziente trimediale Zusammenarbeit.
Gerade in der digitalen Medienwelt lässt sich Mehrwert durch intelligente Vernetzung von Inhalten sowie Fernseh- und Netzstandards schaffen.
Entsprechende Produktionsmodelle führen zu einer erhöhten Wirtschaftlichkeit der Arbeitsprozesse.
Durch die Prüfung und Implementierung von Low-Cost-Produktionssystemen verstehen sich die Digitalkanäle als wichtiger Innovationsfaktor innerhalb der ARD.

**V.
Verbreitung**

Die Digitalkanäle „EinsExtra", „EinsPlus" und „EinsFestival" sind über Satellit (DVB-S), Kabel (DVB-C), in einigen Regionen Deutschlands auch terrestrisch (DVB-T), über IPTV sowie als Web-TV (z.B.
Zattoo.com) empfangbar.
Die Sendevorbereitung und -abwicklung erfolgen für die drei Programme ebenso wie die Bereitstellung von programmbegleitenden Diensten durch das ARD Play-Out- Center in Potsdam.

**Anlage  
**(zu § 28 Abs.
3 Nr.
2 des Medienstaatsvertrages)

**Konzepte für die Zusatzangebote des ZDF**

**I.
Vorbemerkung**

Das ZDF ist gemäß § 11d Abs.
3 Ziffer 2 12.
Rundfunkänderungsstaatsvertrag beauftragt, drei Digitalprogramme als Zusatzangebote zu veranstalten.
Dazu legt das ZDF die folgenden Programmkonzepte vor.
Die Bezeichnung der Programme im Staatsvertrag schließt ihre Präsentation unter einem noch zu findenden Namen nicht aus.
Das ZDF legt sich daher auf die nachstehend beschriebene programmliche Ausrichtung der Zusatzangebote fest, unbeschadet deren konkreter Benennung.

**II.
ZDFinfokanal**

1.  **Ausgangslage / Zielsetzung**
    
    Die digitale Welt ist geprägt durch die technologische Konvergenz von Fernsehen und Internet, die Verschmelzung von linearen und nicht-linearen Diensten, die Ergänzung von Echtzeitfernsehen durch zeitsouveränes Abruffernsehen.
    An die Stelle einzelner TV-Programme treten digitale Angebotsbouquets, die aus mehreren miteinander vernetzten TV-Programmen, Abruffernsehen und Onlinediensten bestehen.
    Diese werden über viele Verbreitungswege und Plattformen auf unterschiedliche Endgeräte distribuiert und ermöglichen somit eine weitgehend orts- und zeitsouveräne Nutzung.
    
    Mit den neuen Möglichkeiten der digitalen Welt verändern sich gerade auch im Bereich der Informationsbeschaffung Verhalten und Ansprüche der Nutzer.
    Für immer mehr Menschen wird es zur Selbstverständlichkeit, sich zu jeder Zeit und an jedem Ort souverän mit den neuesten Nachrichten und wichtigsten Informationen versorgen zu können.
    
    Diesen gewandelten Ansprüchen nach orts- und zeitsouveräner Nutzung muss die Weiterentwicklung der linearen und nicht-linearen Informationsangebote des ZDF Rechnung tragen, wenn das ZDF auch künftig seinem Auftrag gerecht werden will, die Bürger zu informieren und damit zur politischen Meinungs- und Willensbildung beizutragen.
    
    Das ZDF-Hauptprogramm alleine kann diesen Anspruch nicht mehr erfüllen.
    Das Informationsbedürfnis der Menschen ist nicht mehr auf vorgegebene Tageszeiten festgelegt.
    Die Tagesabläufe sind individualisiert, Sendezeiten von Nachrichtensendungen oder Magazinen sind keine Fixpunkte mehr in der Zeitplanung unserer Zuschauer.
    
    Aus diesem Grund ist es mehr denn je notwendig, das Hauptprogramm des ZDF um einen Kanal zu ergänzen, der unter der Dachmarke des ZDF das neue Informationsbedürfnis der Zuschauer befriedigen kann.
    
2.  **Gegenstand des Angebots**
    
    Der ZDFinfokanal ist ein digitaler Sparten-Kanal, der unter der Dachmarke des ZDF Angebote aus den Bereichen aktuelle Information, Hintergrund und Service bündelt und gezielt durch eigene Angebote ergänzt und vertieft.
    
    Seit 1997 gehört dieser digital verbreitete Kanal zum Angebot des ZDF.
    Sein Programm war zunächst geprägt durch unmoderierte vierstündige Wiederholungsschleifen, deren Programminhalte zum größten Teil aus Einzelbeiträgen des Hauptprogramms und von 3sat bestanden.
    
    Vor dem Hintergrund steigender technischer Reichweiten und veränderter Zuschauerbedürfnisse hat das ZDF bereits im Sommer 2007 mit Zustimmung des Fernsehrates das Programmkonzept weiterentwickelt.
    Neben der zeitversetzten Wiederholung von Sendungen wurde das Angebot an aktuellen Kurznachrichten erweitert.
    Dieser Weg soll in den kommenden Jahren fortgesetzt werden.
    
3.  **Beitrag zur Aufgabenerfüllung**
    
    Es zählt zu den Kernaufgaben des öffentlich-rechtlichen Rundfunks, aktuell und hintergründig zu informieren sowie Orientierung zu geben.
    Mit den neuen Möglichkeiten der digitalen Welt verändern sich jedoch die klassischen Wege der Informationsbeschaffung grundlegend: Für immer mehr Menschen wird es zur Selbstverständlichkeit, sich zu jeder Zeit und an jedem Ort souverän mit den neuesten Nachrichten und wichtigsten Informationen versorgen zu können.
    Diesen gewandelten Ansprüchen nach orts- und zeitsouveräner Nutzung muss sich das Fernsehen in seiner Informationsvermittlung stellen – in Formatierung, Sprache und in den Verbreitungswegen seiner Angebote.
    
    Der ZDFinfokanal leistet dazu in Verbindung mit dem Online-Angebot und dem ZDF-Hauptprogramm einen adäquaten Beitrag.
    Durch das spezifische Angebot von gebündelter Information im Infokanal kann das ZDF dem veränderten Nutzungsverhalten in seinem linearen Programmangebot gerecht werden und die vom Gesetzgeber verlangte Grundversorgung der Gesellschaft mit Informationen sicherstellen.
    
4.  **Programmkonzept**
    
    Sachverhalte zu erklären, Hintergründe auszuleuchten und Orientierung zu geben, gehört zu den Stärken des ZDF-Informationsprogramms.
    Auf diesen Stärken ist das Profil des ZDFinfokanals aufgebaut.
    Dabei wird der ZDFinfokanal mit begrenztem Aufwand unter Nutzung des Programmstocks des ZDF als Ergänzungsangebot betrieben und weiter entwickelt.
    
    Folgende Elemente kennzeichnen das Schema des ZDFinfokanals:
    
    *   Regelmäßige Nachrichten auch an Wochenenden und Feiertagen
    *   Mehrfache Wiederholungen von wochenaktuellen Magazinen
    *   Kompaktversionen eines Teils der wochenaktuellen Magazine als regelmäßige Wiederholungen
    *   Unmoderierte Kurzmagazine, die Material des Programmstocks thematisch als kompakte Service- und Informationsangebote neu bündeln.
        Hierin werden die bisherigen Angebote des ZDFinfokanals in aktueller Form fortgeführt
    *   Kurzreportagen und -dokumentationen, die politische, wirtschaftliche, wissenschaftliche und gesellschaftliche Themen aufgreifen und vertiefen
    *   Flächen für Programminnovationen und neue Produktionsformen, wie etwa für die Arbeit der Videojournalisten im ZDF.
    
    Der ZDFinfokanal bündelt in der derzeitigen Ausbaustufe alle informationsorientierten Inhalte des Hauptprogramms und der Partnerkanäle – Magazine, Reportagen, Dokumentationen und Gesprächssendungen –, stellt sie neu zusammen und bietet sie als thematische Schwerpunkte an.
    
    Das Gerüst des Programmschemas bilden die Nachrichtensendungen, die auch an Wochenenden und Feiertagen ausgestrahlt werden: Aus dem Hauptprogramm werden die „heute“-Sendungen um 15.00 und 19.00 Uhr parallel übernommen, das „heutejournal“ wird zeitversetzt um 23.00 Uhr wiederholt.
    Darüber hinaus werden in den „100 Sekunden“ Kurznachrichten stündlich von 8.00 bis 20.00 Uhr aktualisiert.
    Bei unvorhersehbaren Ereignissen von besonderem Nachrichtenwert können die „100 Sekunden“ auch kurzfristig, abweichend vom stündlichen Rhythmus, ins Programm eingesetzt werden.
    
    Die Kurznachrichten des ZDFinfokanals stärken die Nachrichtenkompetenz des ZDF insgesamt: Sie schließen die Nachrichtenlücke, die an Wochenenden und Feiertagen sowie in den ARD-Sendewochen am Vormittag im ZDF-Hauptprogramm besteht.
    Mit der redaktionellen und produktionellen Infrastruktur des Infokanals ist es möglich, auch zu diesen Zeiten bei besonderen Vorkommnissen schnell zu agieren und bei Bedarf ins Hauptprogramm aufschalten zu können.
    
    Die kurzen und fortlaufend aktualisierten Nachrichtenformate entsprechen in besonderer Weise auch dem Bedürfnis von Online-Nutzern nach Orts- und Zeitsouveränität.
    Die „100 Sekunden“ stehen deshalb auch in den Online-Diensten des ZDF zum Abruf bereit.
    Sie sind darüber hinaus für mobile Nutzungen auf Mobiltelefonen und iPods verfügbar.
    Damit wird der Informationsauftritt des ZDF im Internet bedarfsgerecht gestärkt und eine seiner Kernkompetenzen in die multimediale Welt verlängert.
    
    Neben den Nachrichtenleisten stellen Wiederholungen von Magazinen des ZDF-Hauptprogramms (sowie von 3sat) ein besonderes Serviceangebot an die Zuschauer des ZDFinfokanals dar.
    Wer eine wichtige Informationssendung im ZDF-Programm verpasst hat, weil er zur Zeit der Erstausstrahlung nicht sehbereit war, der kann dies eine Woche lang zu wechselnden Terminen im ZDFinfokanal nachholen.
    
    Neben der Wiederholung kompletter Magazine gibt es kürzere Versionen der jeweiligen Sendungen.
    Diese Zusammenfassungen beinhalten die wichtigsten, für diese Form geeigneten Beiträge des jeweiligen Magazins.
    Zusammen mit unmoderierten Kurzmagazinen, die vor allem Schwerpunkte mit Service-Charakter haben, bilden sie die kurz getakteten Leisten des Schemas.
    In der 15-minütigen Rubrik „Das Thema“ werden zudem relevante Themen des Tages aktuell und hintergründig beleuchtet.
    Hierfür werden bereits vorhandene Berichte aus den Nachrichten- und Magazinsendungen des ZDF neu zusammengestellt.
    
    Kurzreportagen und -dokumentationen, die politische, wirtschaftliche, wissenschaftliche und gesellschaftliche Themen aufgreifen, ergänzen die kompakte Programmleiste.
    Sie können auch zur Stützung von Programmschwerpunkten des Hauptprogramms oder zur Akzentuierung des eigenen Angebots genutzt werden.
    Das Programm für Wochenenden und Feiertage im ZDFinfokanal setzt – vor dem Hintergrund der spezifischen Sehgewohnheiten an diesen Tagen – stärker auf Repertoire-Angebote wie Reportagen, Features und Dokumentationen.
    Gleichwohl finden auch hier kurz getaktete Angebote und aktuelle Informationen ihren Platz.
    
    Im Gegensatz zu PHÖNIX wird die Ereignisberichterstattung keine zentrale Rolle im Programm des ZDFinfokanals spielen.
    Lediglich fallweise und zeitlich begrenzt wird der ZDFinfokanal auch live über wichtige Ereignisse aus Politik, Gesellschaft, Sport und Wissenschaft berichten.
    Der ZDFinfokanal ist zudem kein special-interest-Angebot für Börsen- und Wirtschaftsberichterstattung.
    Eine Entwicklung in diese Richtung ist auch in Zukunft nicht vorgesehen.
    
    Der ZDFinfokanal bietet für das ZDF zudem eine Plattform, um neue Angebotsformen und journalistische Formate testen zu können.
    Perspektivisch ist vorgesehen, spezifische Informationsformate zu entwickeln, die insbesondere auf jüngere Zuschauergruppen zugeschnitten sind.
    Erste Schritte wurden mit der Pilotsendung „Wirtschaftswunder“ unternommen: Das 15-minütige moderierte Servicemagazin, das Ende Juni 2008 erstmals ausgestrahlt wurde, wendet sich an junge Konsumenten und informiert über Themen rund ums Geld.
    Auch die Rubrik „Mojo“ bietet Raum für formale Innovationen: In 15-minütigen Reportagen greifen Videojournalisten lebensnahe Themen des Alltags auf und entwickeln neue Handschriften.
    
    Besonderes Augenmerk wird auf die Entwicklung plattformübergreifender Formate gerichtet, die innovativ die Möglichkeiten des Fernsehens mit denen des Online-Angebots verbinden und durch Interaktivität das Publikum einbinden.
    Damit ist die Erwartung verbunden, Altersgruppen, die die klassische TV-Plattform nicht mehr nutzen, leichteren Zugang zu ZDF-Angeboten zu bieten.
    
    Das ZDF hat sich verpflichtet, die vielfältigen Veränderungen des Fernsehens, die sich durch die Digitalisierung ergeben, aktiv zu gestalten.
    Auf dem Weg dorthin kann und wird der ZDFinfokanal durch seine synergetischen Arbeitsabläufe und plattformübergreifenden Ansätze eine wichtige Rolle spielen.
    
5.  **Verbreitung**
    
    Der ZDFinfokanal wird digital verbreitet und ist über Kabel, Satellit und Antenne (DVBT) empfangbar.
    Die Nutzung und Begleitung von Sendungen im Internetangebot des ZDF erfolgt im Rahmen der medienrechtlichen Vorgaben.
    

**III.
ZDFkulturkanal**

1.  **Ausgangslage / Zielsetzung**
    
    Entsprechend der Ermächtigung durch die Ministerpräsidenten der Länder und der Genehmigung durch den Fernsehrat strahlt das ZDF seit dem 9.
    Dezember 1999 den digitalen Theaterkanal via Kabel und Satellit aus.
    
    Im Zuge der Anpassung an die Herausforderungen des digitalen Markts soll das bisherige Schleifenprogramm mit Mehrfachwiederholungen Zug um Zug durch ein strukturiertes Ganztagesprogramm ersetzt und damit der ZDFtheaterkanal zu einem Kulturkanal fortentwickelt werden.
    
2.  **Gegenstand des Angebots**
    
    Der digitale ZDFkulturkanal wird ein Genre-Spartenkanal sein, der sich in besonderer Weise der Darstellung der kulturellen Vielfalt widmet.
    Er bündelt unter der Dachmarke des ZDF das breite Spektrum der produktionellen Anstrengungen des Gesamtunternehmens ZDF und seiner Partnerprogramme im kulturellen Bereich und verstärkt diese durch gezielte Eigenangebote.
    Das Feld „Performing Arts“, also insbesondere die Übertragung und Berichterstattung über Theater- und Opernaufführungen, Konzerte und Kleinkunst, soll als zentrales Merkmal erhalten bleiben.
    Ziel ist es, dem gestiegenen Bedürfnis nach kultureller Orientierung in einer vielfältig aufgestellten Gesellschaft Rechnung zu tragen und damit einen Beitrag zur Integration zu leisten, indem der digitale ZDFkulturkanal intelligent informiert und unterhält.
    
3.  **Beitrag zur Aufgabenerfüllung**
    
    Die Darstellung und Vermittlung von Kultur gehört unstrittig zu den Kernaufgaben des öffentlich-rechtlichen Rundfunks.
    Der digitale ZDFkulturkanal richtet sich an Zuschauer aller Altersgruppen, die sich im weitesten Sinn für Kultur interessieren.
    Er wird auch Spezialangebote für ein jüngeres Publikum auf regelmäßigen Sendeleisten vorhalten, um diese für die Kultur und einen geistvollen Mediengebrauch im weiteren Sinne zu gewinnen.
    Der digitale ZDFkulturkanal stellt das kulturelle Leben in großer Breite und Ausführlichkeit dar.
    Er legt dabei auf die Qualität der fernsehmäßigen Umsetzung höchsten Wert.
    
    Der digitale ZDFkulturkanal sollte auch als Veranstaltungspartner bei Festivals und als Wegweiser für Qualitätsangebote auftreten und kulturelle Projekte würdigen und fördern, die der Bewahrung und Entwicklung des kulturellen Erbes dienen.
    Dies gilt auch für den Denkmalschutz und den öffentlich finanzierten Kulturbetrieb allgemein.
    
4.  **Programmkonzept**
    
    Im Mittelpunkt des ZDFkulturkanals steht die Pflege der deutschen Sprache und Literatur, des Theaters, der Bildenden Künste und der Musik sowie die Darstellung von Formen der Alltagskultur und die Themenbereiche Bildung und Erziehung, Lebensqualität, urbane Lebenswelten, Pop, Avantgarde und Philosophie.
    Aber auch die aus dem Medium selbst erwachsene Film- und Fernsehkultur wird in ausgewählten Premium-Produktionen aus den Bereichen Dokumentar- und Spielfilm bis hin zum anspruchsvollen Fernsehspiel Gegenstand des Angebots des digitalen ZDFkulturkanals sein.
    
    Das Angebot beinhaltet insgesamt Übertragungen von Bühnenereignissen aus den Bereichen Theater, Ballett, Musiktheater, Konzert, Performance-Kultur, die umfassende Darstellung bedeutender regionaler Kulturfestivals, die Berichterstattung über und Darstellung von herausragenden kulturellen Wettbewerben.
    Außerdem wird der digitale ZDFkulturkanal Kulturmagazine und genre-spezifischen Dokumentationen und Gesprächssendungen der ZDF-Programmfamilie gebündelt und zu bester Sendezeit und zum Teil neu aufbereitet präsentieren.
    
    Ein Angebot im Bereich Jugendkultur mit einem Schwerpunkt Jugend, Musik und Jugend-Lebensart wird zu den unverwechselbaren Kennzeichen des digitalen ZDFkulturkanals gehören.
    Er unterstreicht den Anspruch des Kanals, auch ein jüngeres Publikum an öffentlich-rechtliches Qualitätsprogramm heranzuführen.
    Spielerische Präsentationsformen und Publikumsansprache, Edutainment und ein frischer Umgang mit Form, Farben und Sounds sind dabei wesentliche Mittel.
    Der digitale ZDFkulturkanal wird außerdem dem gewachsenen Interesse an Bildung Rechnung tragen und auch die Diskussion um die Fortentwicklung der Bildungsinstitutionen widerspiegeln.
    Er wird sich aktiv an Motivationsaktionen für Schülerinnen und Schüler beteiligen und mit Aktionen wie dem Schülertheaterfestival und einem neuen Format, in dem Jugendliche mit Leistungsträgern unserer Gesellschaft zusammentreffen und Fragen zu deren beruflichen Werdegang, aber auch zum Thema Moral und Verantwortung stellen.
    
    Da das ZDF mit seinem Hauptprogramm, Phoenix und dem Infokanal bereits über Flächen für die aktuelle politische Berichterstattung verfügt, wird der ZDFkulturkanal keine eigene Nachrichtenberichterstattung (mit der Ausnahme der Übernahme einer Hauptnachrichtensendung), keine politischen Magazine, und keine Übertragung von politischen Ereignissen einplanen.
    Außerdem wird der ZDFkulturkanal keinen Sport und keine Wirtschafts- und Ratgebersendungen vorsehen.
    
    Das Verhältnis zum ZDF-Hauptprogramm und zu den Partnerkanälen baut auf den gewachsenen und gelebten Erfahrungen des ZDFtheaterkanals im Programmverbund auf.
    Dabei sind programmübergreifende Kulturschwerpunkte denkbar, die zu Spitzenereignissen auch im Hauptprogramm bzw.
    in den Partnerkanälen aufscheinen, etwa bei der Berlinale, beim Berliner Theatertreffen, den Bayreuther und Salzburger Festspielen oder großen Pop- und Rockfestivals sowie in bewusst mehrkanalig operierenden Programmen wie unlängst beim Cirque du Soleil mit einer Übertragung vor und hinter der Bühne auf zwei Kanälen.
    
    Der digitale ZDFkulturkanal baut auf Erfahrungen des ZDFtheaterkanals auf und verinnerlicht dessen besondere und in der Medienlandschaft einzigartige Aufmerksamkeit für die Darstellenden Künste in ihrer Vielfalt als weltweit beispielloses konstituierendes Element deutscher Kultur.
    Dabei spielen auch Repertoireangebote eine Rolle, die vor allem im Tagesprogramm, aber auch im Zusammenwirken mit der ZDF-Mediathek weiterhin vorgehalten werden sollten.
    Dabei sind auch programmbegleitende und sendungsergänzende Angebote im Internet nötig, insbesondere im Hinblick auf das besondere Augenmerk des digitalen ZDFkulturkanals für das jüngere Publikum.
    
5.  **Verbreitung**
    
    Der ZDFkulturkanal wird digital verbreitet und ist derzeit über Kabel und Satellit empfangbar.
    Die Nutzung und Begleitung von Sendungen im Internetangebot des ZDF erfolgt im Rahmen der medienrechtlichen Vorgaben.
    Der digitale ZDFkulturkanal sollte diskriminierungsfrei verbreitet werden, d.h., auf allen digitalen Plattformen gut auffindbar sein.
    

**IV.
ZDF-Familienkanal**

1.  **Ausgangslage/Zielsetzung**
    
    Das ZDF kann nur von bleibendem Wert für die Gesellschaft sein, wenn es alle relevanten Teile der Gesellschaft erreicht.
    Angesichts des sich immer stärker diversifizierenden Fernsehmarktes und der sich verändernden Nutzungsgewohnheiten der jüngeren Zielgruppen wird dies zusehends schwieriger.
    Das ZDF-Hauptprogramm erreicht vor allem Zuschauer, die älter als 60 Jahre sind.
    In den jüngeren Altersgruppen ist das ZDF unterdurchschnittlich vertreten.
    Besorgniserregend ist in diesem Zusammenhang, dass sich auch und gerade die jungen Familien immer mehr den Privatsendern zuwenden.
    Die Erfahrung zeigt zudem, dass die Zuschauer mit steigendem Alter nicht im gewünschten  Maße zum ZDF zurückkehren werden.
    
    Bei allem gesellschaftlichen Wandel bleibt die Familie die kleinste und zugleich bedeutendste Einheit eines verbindlichen Miteinanders unterschiedlicher Generationen.
    Sie ermöglicht das Erlernen, Leben und Weitergeben grundlegender Regeln der Gesellschaft.
    Umso mehr hat das ZDF als nationaler öffentlich-rechtlicher Sender hier in besonderem Maße Verantwortung.
    
    Es muss das Ziel des ZDF sein, diese Zuschauergruppen wieder zurückzugewinnen und dauerhaft zu binden.
    Dies kann nur gelingen, wenn das ZDF ein Programm anbietet, das sich an der Lebenssituation, den Bedürfnissen und der medialen Sozialisation junger Familien orientiert.
    Die Entwicklung des Fernsehmarktes sowie die veränderten Sehgewohnheiten zeigen, dass dies nur mit einem eigenen, passgenauen Angebot möglich ist.
    
    Der ZDFdokukanal soll deshalb konsequent zu einem Programm weiterentwickelt werden, das sich insbesondere an junge Familien richtet.
    Der ZDF-Familienkanal soll Zuschauer zwischen 25 und 50 Jahren sowie deren Kinder ansprechen.
    Er soll die öffentlich-rechtliche Alternative zu den in dieser Altersgruppe vorherrschend genutzten Programmangeboten werden.
    
2.  **Gegenstand des Angebots**
    
    Der ZDF-Familienkanal bietet ein Programm mit vielfältigen Inhalten aus den Bereichen Bildung, Kultur, Wissenschaft, Beratung, Information und Unterhaltung.
    Er bedient sich aller wichtigen Genres wie Dokumentation, Reportage, Fernsehfilm, Serie, Spielfilm, Magazin sowie Show/Talk und beschäftigt sich insbesondere mit Inhalten aus den Bereichen Gesellschaft und Erziehung, Ratgeber und Service, Wissenschaft und Natur, Geschichte und Zeitgeschehen sowie Kultur.
    
    Im Mittelpunkt der Zuschaueransprache des ZDF-Familienkanals steht eine realitätsnahe Orientierungs- und Ratgeberfunktion.
    Auch das Bedürfnis, angesichts der zunehmenden Fragmentierung des Alltags auf anspruchsvolle Weise Entspannung und intelligente Unterhaltung zu finden, wird angemessen berücksichtigt.
    Hier sind die Kernkompetenzen des öffentlich-rechtlichen Rundfunks gefordert, die mit Hilfe eines familienorientierten Angebots der adressierten Altersgruppe vermittelt werden können.
    
3.  **Beitrag zur Aufgabenerfüllung**
    
    Der deutsche Fernsehmarkt hat sich zu einem der wettbewerbsstärksten auf der Welt entwickelt.
    Ein Haushalt hat im Schnitt 63 Programme auf der Fernbedienung programmiert.
    In keinem anderen europäischen Land können so viele Zuschauer so viele Programme sehen.
    Die großen Sender konkurrieren zunehmend auch mit den kleinen Anbietern.
    In Zukunft werden noch mehr Programme um Marktanteile konkurrieren.
    Gleichzeitig weist die Marktanteilsentwicklung des ZDF-Hauptprogramms der letzten 15 Jahre überproportionale Verluste bei den jüngeren Zuschauern auf.
    Es werden aktuell nur noch 38 % des Marktanteils von 1992 erreicht, während es bei über 50-Jährigen noch 70 % des damaligen Niveaus sind.
    
    Deshalb hat der ZDF-Familienkanal die Aufgabe, die Zuschauer, die sich aufgrund ihres Alters, ihrer Lebensgewohnheiten und ihrer medialen Sozialisation im Rahmen des ZDF-Hauptprogramms nur teilweise mit ihren spezifischen Bedürfnissen wieder finden, an ein wertehaltiges öffentlich-rechtliches Programmangebot heranzuführen und sie dauerhaft zu binden.
    Schema-, Programm- und Formatgestaltung sollen passgenau den Tagesablauf sowie die Sehbedürfnisse junger Familien berücksichtigen.
    
    Der ZDF-Familienkanal ist komplementär zum Hauptprogramm.
    Er bündelt und ergänzt dessen Angebote unter inhaltlichen, demografischen und soziologischen Gesichtspunkten und erweitert sie um Programminnovationen, die sich den zentralen Fragen des Alltags junger Familien zuwenden.
    Der ZDF-Familienkanal soll inhaltlich, aber auch in Bezug auf die Formatentwicklung zum Innovationsmotor für die ZDF-Familie werden.
    
    Gleichzeitig nutzt der Familienkanal die Programmvorräte der ZDF-Familie neu und gewinnbringend und leistet durch wirtschaftliche und inhaltliche Synergien einen wichtigen Beitrag zur Amortisation kostbarer Ressourcen im Gesamtunternehmen.
    Qualität und Modernität zahlreicher ZDF-Programme, die von vielen Jüngeren im Hauptprogramm nicht mehr vermutet werden, kommen zu neuer Geltung.
    Das ZDF-Hauptprogramm profitiert selbst wiederum von den neuen Erfahrungen bei der Ansprache jüngerer Zuschauer.
    Der Digitalkanal kann die Programmvielfalt, die vorliegenden Lizenzen und die Stärke des Hauptprogramms nutzen.
    
    Dabei unterscheidet sich der ZDF-Familienkanal erkennbar von den Zielgruppenangeboten der kommerziellen Anbieter.
    Sein Ziel ist die Vermittlung von Wissen und Werten, die zu einer positiven Gestaltung der eigenen wie der gesellschaftlichen Lebenswirklichkeit befähigen.
    Der ZDF-Familienkanal möchte mit seinem Programmangebot dazu beitragen, das Vertrauen speziell der Familien in ihre eigene Zukunft zu festigen und die Wertschätzung der jungen Familie in der Gesellschaft zu verstärken.
    
4.  **Programmkonzept**
    
    Im Mittelpunkt des ZDF-Familienkanals stehen eine realitätsnahe Orientierungs- und Ratgeberfunktion sowie die Möglichkeit, angesichts der zunehmenden Fragmentierung des Alltags auf anspruchsvolle Weise Entspannung und intelligente Unterhaltung zu finden.
    Der ZDF-Familienkanal ist somit eine konsequente Weiterentwicklung des ZDF.dokukanals.
    Die Stärke des ZDF.dokukanals, mit Dokumentationen und Reportagen Orientierung zu bieten und Hintergrund zu vermitteln, soll weiter ausgebaut werden.
    Im Vordergrund stehen dabei folgende Funktionen, die für die Erfüllung des Programmauftrages zentral sind: Wissensvermittlung, Lebens- und Alltagsbewältigung, politische und (zeit-)geschichtliche Bildung sowie anspruchsvolle Unterhaltung.
    
    Der ZDF-Familienkanal wird sich weiterhin an der Bedeutung, die Wissenschaft und kontinuierlicher Wissenserwerb gerade für jüngere Zuschauer haben, ausrichten.
    Wissen(schaft)s- und Natursendungen werden einen wichtigen inhaltlichen Akzent setzen ebenso wie die generationenverbindenden Programm-Marken „Terra X“ und „Abenteuer Wissen“.
    Dokumentationen und Reportagen entsprechen ohnedies einem Grundbedürfnis vieler Menschen, in einer immer unübersichtlicheren Welt Orientierung zu erfahren, Überblick zu gewinnen, die notwendigen Hintergrundinformationen zu erhalten.
    Fortgesetzt werden soll auch die erfolgreiche „Tagesdoku“.
    Dokumentationen und Reportagen beleuchten von Montag bis Freitag ein Thema der Woche in unterschiedlichsten Facetten und vermitteln auf diese Weise abwechslungsreiches und differenziertes Hintergrundwissen.
    Als Programminnovation geplant ist eine Wissenssendung für junge Familien.
    
    Der ZDF-Familienkanal soll Orientierungshilfe für die alltägliche Lebensbewältigung sein.
    Zentral sind hier die Themen „Schule“, „Bildung“ sowie „Fragen nach der Vereinbarkeit von Familie und Beruf“.
    Sendereihen wie beispielsweise „37°“, „Babystation“, „S.O.S.
    Schule“, „Zeit der Wunder“ oder „Mädchengeschichten“ haben in diesem Kontext einen festen Platz.
    Darüber hinaus soll praxisnahen Fragen aus dem Alltag junger Familien wie z.B.
    zu den Themen „Hausbau“, „Finanzen“, „Versicherungen“ nachgegangen werden.
    Als Programminnovationen sind Ratgebersendungen, Ombudsmagazine und neue dokumentarische Erzählformen angedacht.
    
    Der ZDF-Familienkanal setzt darauf, seinen Zuschauern Anregungen für die aktuelle politische Diskussion und zur zeitgeschichtlichen Meinungsbildung zu liefern.
    Ein besonderes Augenmerk gilt deshalb vor allem den historischen und zeitgeschichtlichen Dokumentationen, bei denen das ZDF über eine breite internationale Reputation verfügt.
    In diesem Zusammenhang seien die großen zeitgeschichtlichen Fernsehereignisse wie „Dresden“ oder „Die Gustloff“ erwähnt, die der ZDF-Familienkanal ins Zentrum seiner Programmschwerpunkte setzen wird.
    Eine Programminnovation im Bereich der politischen Bildung soll die Entwicklung einer neuen Wahlsendung für junge Familien darstellen.
    Auch bei der Nachrichtenberichterstattung will der ZDF-Familienkanal neue Wege gehen, indem er sich auf eine erfolgreiche Programmentwicklung des Hauptprogramms stützt: eine Adaption der „Logo“-Nachrichten für junge Familien.
    
    Gleichzeitig soll der Familienkanal auf anspruchsvolle Weise Entspannung und intelligente Unterhaltung bieten.
    Vor dem Hintergrund der Pluralisierung von Lebensformen findet sich ein Alltag, der in erster Linie von der Fragmentierung familiärer und gesellschaftlicher Zusammenhänge und damit einem Gefühl ständiger Überbelastung durch die Anforderungen des täglichen Lebens gekennzeichnet ist.
    Umso mehr tritt neben der Suche nach Sinn und Orientierung auch der Wunsch nach Entspannung und Entlastung.
    Einen eigenen Stellenwert im Programm des ZDF-Familienkanals haben deshalb fiktionale Sendungen vom Fernsehfilm über die Serie bis hin zum Spielfilm.
    Aufgrund ihres hohen Identifikationspotentials eignen sie sich ganz besonders für die Vermittlung komplexer Zusammenhänge oder vorbildhafter Wertesysteme, besonders dann, wenn sie sich erkennbar am Alltag und der Lebenswirklichkeit ihrer Zuschauer orientieren.
    
    Für die Hauptsendezeit des neuen ZDF-Digitalkanals ist so eine Mischung aus hochwertiger Fiktion und erstklassigen Dokumentationen geplant.
    Sie wird von erfolgreichen Reportagen und Dokumentationen, thematisch ausgerichteten Programmschwerpunkten sowie preisgekrönter nationaler und internationaler Fiktion geprägt sein.
    Aufgabe der Programmschwerpunkte und Themenabende wird es sein, die Vorteile der Verschränkung von dokumentarischem Informationsprogramm und emotionalinvolvierender Fiktion so zu verbinden, dass den Zuschauern ein attraktiver, breiter und nachhaltiger Zugang zu wichtigen Themenstellungen auch bei komplexen Sachverhalten möglich wird.
    Auf diese Weise sollen fiktionale Programme nicht nur unterhalten, sondern auch Anregung zur Reflexion individueller wie gesellschaftlicher Verhaltensweisen, Themen und Prozesse bieten.
    
    Der ZDF-Familienkanal kann sich hierbei auf ein breites Fundament erstklassiger Fernsehfilme und Spielfilme stützen, die für seine Zuschauer zum Teil allein deshalb „Premieren“ sind, weil sie im ZDF-Hauptprogramm zu Sendezeiten laufen, die mit dem Lebensrhythmus dieser Altersgruppe nicht kompatibel sind oder auch, weil sie von ihnen gar nicht im ZDF-Hauptprogramm vermutet werden.
    Einen ganz eigenen Akzent will der ZDF-Familienkanal auch bei der Entwicklung und Pflege des filmischen Nachwuchses setzen und dabei an die gelebte Tradition des „Kleinen Fernsehspiels“, der Filmredaktion 3sat sowie der Innovationswerkstatt „Quantum“ anknüpfen.
    Gefragt sind Sendungen, die den Dingen des täglichen Lebens auf den Grund gehen und ihren Wert in einer ganz praktischen Alltags- und Lebenshilfe haben.
    Auch die fiktionalen Serien können modellhafte Lebensbewältigung und Persönlichkeitsentwicklung mit vielfältigen Facetten über einen langen Zeitraum begleiten und damit realitätsnahe Problembewältigungsstrategien vermitteln.
    Im Rahmen des Vorabendprogramms sollen deshalb beispielsweise „Familienserien“ zum Einsatz kommen, die den Alltag junger Menschen mit all seinen Brüchen, Widersprüchen und Reibungsflächen zum Thema haben.
    
    Mit eigenen Formatentwicklungen soll der ZDF-Familienkanal zur Entwicklungsplattform und zum Innovationsmotor für die ZDF-Programmfamilie werden.
    Systematisch sollen von Beginn an Formate, Genres und Protagonisten getestet werden, die bei Erfolg auch im Hauptprogramm Verwendung finden können.
    Auf diese Weise profitiert nicht nur der ZDF-Familienkanal von den Erfahrungen und Programmvorräten des Hauptprogramms, sondern auch das Hauptprogramm und die ZDF-Partnerkanäle von den Entwicklungen des digitalen Kanals.
    
    Das Programmschema orientiert sich am Tagesablauf der 25- bis 50-Jährigen, vor allem der jungen Familien.
    Eine besondere Herausforderung für die Programmierung ist der unregelmäßige, zum Teil nicht planbare Tagesablauf sowie die Parallelität mehrerer individueller Tagesverläufe gerade in jungen Familien, die das Programmschema durch zeitversetzte Wiederholung zentraler Sendungsangebote über den ganzen Tag berücksichtigen wird.
    
    In der Hauptsendezeit wird die Zuschaueransprache und Schemagestaltung des ZDF-Familienkanals von folgenden Leitgedanken geprägt:
    
    *   Die Vorabend-Zeit zwischen 17.00 Uhr und 21.00 Uhr ist dezidierte Familienzeit:  
        In knapp 26 % der Familien-Haushalte wird zwischen 17.00 Uhr und 20.15 Uhr gemeinsam ferngesehen – und dies, obwohl 44 % der Kinder einen eigenen Fernseher besitzen.
        82 % der Eltern hätten die gemeinsame Fernsehzeit gerne häufiger; es fehlen entsprechende Programmangebote zur richtigen Zeit.
        Und es fehlen – gerade mit Blick auf die Kinder - werbefreie Fernsehangebote.
    *   Im Zentrum des Programmschemas steht der Primetime-Beginn um 21.00 Uhr.
        Er trägt der Tatsache Rechnung, dass für die meisten Eltern erst jetzt eine eigene „Freizeit“ beginnt, aber auch die 25-50-Jährigen ohne Kinder können um diese Uhrzeit fernsehen (der Höhepunkt in der Fernsehnutzung von Eltern liegt um 21.30 Uhr, bei Erwachsenen ohne Kinder um 21.00 Uhr).
    
    Bei dem ZDF-Familienkanal handelt es sich nicht um ein klassisches Vollprogramm.
    Das Programmschema des ZDF-Familienkanals verdeutlicht vielmehr, dass mit dem ZDF-Familienkanal ein Spartenprogramm geplant ist, das sich auf eine bestimmte Zielgruppe im Fernsehmarkt fokussiert, ähnlich wie auch der Kinderkanal sich unter Anwendung zahlreicher Genres an eine bestimmte Zielgruppe wendet.
    Im neuen ZDF-Familienkanal werden im Unterschied zu klassischen Vollprogrammen keine regelmäßigen Nachrichtensendungen, keine festen Programmplätze für Sport und keine Boulevard-Showsendungen im Schema Eingang finden.
    
    Das ZDF wird in allen Genres die öffentlich-rechtlichen Qualitätsansprüche einlösen.
    Die Unterhaltung ist Teil des Konzeptes, weil das ZDF beabsichtigt, auch im Genre Unterhaltung neuartige Formate zu erproben und im Familienkanal zu pilotieren.
    
    Der ZDF-Familienkanal soll gerade jüngere Zuschauer, d.h.
    die Altersgruppe der etwa 25 bis 50jährigen, ansprechen.
    Dazu soll insbesondere im Ratgeberbereich das Publikum mit neuen Ausdrucks- und Programmformen und einer gegenüber dem Hauptprogramm selbständigen Art der Ansprache durch informierende und orientierende Programme gewonnen werden.
    
    Die Unterhaltungsangebote sollen dabei ein spezifisch öffentlich-rechtliches Profil aufweisen.
    Dazu werden sie einerseits berücksichtigen, dass Entspannung und Anregung Zuschauerbedürfnisse sind.
    Zugleich können aber auch Unterhaltungsangebote Information und Orientierung vermitteln.
    Der ZDF-Familienkanal wird in seinen Unterhaltungsangeboten insbesondere auch die jüngeren Zuschauer ansprechen und ihnen die Auseinandersetzung mit Themen aus dem Alltag, dem Wissensfundus der Gesellschaft und der Kultur ebenso ermöglichen wie die Reflexion von Themen unserer Zeit.
    Er soll also die Zuschauer für Qualität, Information und Orientierung gewinnen.
    
    Der Sender wird sich bereits dadurch von anderen Programmen, vor allem kommerziellen Sendern, abheben, weil er zu rund 80 % mit Wiederholungen aus dem ZDF Hauptprogramm (darunter insbesondere Sendungen, die beim jüngeren Publikum erfolgreich sind) bestückt wird.
    Von den meisten kommerziellen Digital-Spartenkanälen unterscheidet er sich außerdem dadurch, dass er nicht monothematisch angelegt ist.
    Anders als diese verfolgt der ZDF-Familienkanal nämlich einen dem öffentlichrechtlichen Rundfunk gemäßen Ansatz.
    Er leistet mit einer großen Bandbreite an Themen, Genres und Handschriften, mit Mehrheiten- und Minderheitenprogrammen einen Beitrag zum Zusammenhalt der Gesellschaft.
    Im Gegensatz zu monothematischen Kanälen ermöglicht er dem Zuschauer, sich durch Vielfalt und Unterschiedlichkeit ein differenziertes Weltbild zu verschaffen.
    Von den bestehenden öffentlich-rechtlichen Programmen unterscheidet sich der Familienkanal aufgrund seiner spezifischen Programmmischung und Zielgruppenausrichtung deutlich.
    
    Strukturell wird sich der ZDF-Familienkanal von den bestehenden kommerziellen Digitalfernsehangeboten durch das Fehlen von Werbung und Sponsoring abheben.
    Er wird sich darüber hinaus dadurch unterscheiden, dass keine Konzentration auf Inhalte stattfindet, die sich gewinnbringend vermarkten lassen.
    Das Programm steht –wie alle Angebote des ZDF – nicht im Dienst des kommerziellen Erfolgs und verzichtet daher auf Sendungen, die vor allem dem Zweck dienen, Begleitdienste und -produkte (etwa Downloads, CDs, DVDs) abzusetzen oder durch die Beteiligung der Zuschauer an Abstimmungen o.ä.
    Erlöse zu erzielen.
    
    Der ZDF-Familienkanal ist damit ein durch und durch öffentlich-rechtliches Programmangebot, das als klar unterscheidbare Alternative zu den privaten Programmen positioniert werden soll.
    
    Der Auf- und Ausbau des ZDF-Digitalkanals erfolgt – parallel zur wachsenden technischen Verbreitung – in zeitlich gestaffelten Ausbaustufen.
    In der ersten Stufe ab 2009 profitiert das Programmschema vom Rückgriff auf den breiten Fundus des Programmarchivs sowie von aktuellen Übernahmen aus dem ZDF-Hauptprogramm.
    Erste Programminnovationen dienen der Positionierung auf dem Fernsehmarkt und der Etablierung des neuen Kanalprofils.
    Das besondere Augenmerk wird dabei auf der Erzeugung einer großen Zuverlässigkeit im Angebot von hochwertigen Dokumentationen und Reportagen, thematisch relevanten Wissens- und Ratgeberformaten, entspannenden, anregenden und dem realen Alltag junger Familien affinen unterhaltenden Sendungen sowie einem breiten, inhaltlich diskursiven Spektrum in Form von Programmschwerpunkten liegen.
    Beginnend mit 2010 werden einige Wiederholungsleisten durch weitere Programm-Innovationen ersetzt, die auch die Aufgabe haben, das Profil des Senders zu schärfen.
    In der dritten Ausbaustufe ab 2012 soll der ZDF-Familienkanal in einer synergetischen Mischung aus Verwertung des ZDF-Programmvorrats und vermehrt eigenproduzierter bzw.
    erworbener Programmware sein eigenständiges Programmprofil festigen und verstärken.
    
5.  **Empfangbarkeit**
    
    Der ZDF-Familienkanal wird ausschließlich digital verbreitet und ist über Kabel, Satellit und Antenne empfangbar.
    Angestrebt wird dabei eine 24 Stunden-Verbreitung über DVB-T, die bisher aus Kapazitätsgründen noch auf eine Sendezeit zwischen 21.00 Uhr und 6.00 Uhr beschränkt ist.
    Eine sendungsbezogene Programmbegleitung im Rahmen des Online-Angebotes des ZDF sowie das Bereitstellen von Sendungen in der ZDF-Mediathek sind ebenfalls vorgesehen.
    Gerade für jüngere Zuschauer ist das Zusammenspiel der Medien von entscheidender Bedeutung.
    Vertiefende Informationen und Hintergründe zu den im Familienkanal angebotenen Themen im online-Bereich sind deshalb essentiell für die angestrebte Publikumsbindung.
    Insbesondere bei Programmschwerpunkten und Themenabenden soll diese Verknüpfung von Fernsehen und Internet zum Tragen kommen.
    Bei der Entwicklung von Programminnovationen sollen zudem die Möglichkeiten der crossmedialen Verbindung berücksichtigt werden.
    

**V.
Finanzierung**

Die Zusatzangebote werden in der Gebührenperiode ab 2009 aus dem Bestand finanziert.
Auch für die Gebührenperiode ab 2013 hat sich das ZDF verpflichtet, keine gesonderten Mittel anzumelden, sondern die Zusatzangebote aus dem Bestand zu finanzieren.

**Anlage  
**(zu § 29 Abs.
3 Nr.
3 des Medienstaatsvertrages)

**Programmkonzept DRadio Wissen**

1.  **Ausgangslage**
    
    Der Eintritt ins digitale Zeitalter geht einher mit Unsicherheiten künftiger Mediennutzung, von denen auch die Qualitätsangebote im Hörfunk betroffen sind.
    Wer eine junge anspruchsvolle Zielgruppe erreichen will, muss ein an den inhaltlichen und formalen Ansprüchen sowie den Rezeptionsgewohnheiten dieser Zielgruppe orientiertes Radioformat entwickeln.
    
    Gerade die Zielgruppe der jungen Erwachsenen, die mit _DRadio Wissen_ vorrangig angesprochen werden soll und die durch andere anspruchsvolle Angebote nicht angemessen erreicht wird, zeichnet sich durch ein großes Informationsbedürfnis aus und ist durch das Internet an hohe Aktualitätsstandards gewöhnt.
    Ausgehend von der Zielgruppe junger Erwachsener wird das Programmangebot von _DRadio Wissen_ seinen Inhalten entsprechend generationsübergreifend und integrativ angelegt.
    
    Ein erfolgreiches Radioprogramm muss ein breites Interessenspektrum seiner Zielgruppe befriedigen, um Hördauer und langfristige Hörerbindung und damit Akzeptanz am Markt zu erreichen.
    
    Erfolgreiches Radio muss sich durch ein einprägsames, leicht „erlernbares“ Sendeschema und kreative Programmformen auszeichnen.
    Der Hörfunkrat des Deutschlandradios hat am 11. September 2008 den „Bericht über programmliche Leistungen und Perspektiven des Nationalen Hörfunks 2008-2010“, (HR 5/2008) verabschiedet.
    Er verpflichtet das Deutschlandradio darin auf Qualitätsstandards, die für den öffentlich-rechtlichen Rundfunk verbindlich sein sollten.
    Dies betrifft u.a.
    
    *   einen hohen Anteil an Eigenproduktionen,
    *   ein verlässliches Nachrichtenraster,
    *   Innovationsfähigkeit und
    *   die Eigenentwicklung von Formaten für die spezifischen Bedürfnisse der jeweiligen Hörerschaft.
    
    Repräsentative Hörerumfragen belegen, dass die Deutschlandradio-Programme sich wegen dieser Merkmale einer hohen Akzeptanz bei der Hörerschaft erfreuen.
    Ein Drittel der insgesamt 4,8 Millionen Hörer der 22 gehobenen Programme in Deutschland werden allein durch die beiden Angebote des Nationalen Hörfunks Deutschlandfunk und Deutschlandradio Kultur generiert – und dies trotz unzureichender bundesweiter Frequenzausstattung.
    
    Der hohe Anteil der Hörerschaften legt nahe, dass ein erfolgreiches, sich an den vorgegebenen Qualitätsmerkmalen orientierendes _DRadio Wissen_ die Zahl der Hörer dieses anspruchsvollen Programmsegments insgesamt erhöhen und damit weiter zur Anerkennung öffentlich-rechtlicher Qualitätsleistungen beitragen kann.
    Insoweit ist das Angebot von strategischer Bedeutung für den öffentlich-rechtlichen Rundfunk insgesamt.
    
    Deutschlandradio Kultur ist es gelungen, mit seinen innovativen Angeboten sowohl das jüngste Durchschnittsalter der Hörer der sogenannten gehobenen Programme (Deutschlandradio Kultur MA 2008 II 50 Jahre, generell 55 Jahre) als auch ein ausgeglichenes Verhältnis von weiblichen und männlichen Hörern zu erzielen.
    Diese Erfahrungen bilden eine tragfähige Grundlage für die Entwicklung eines neuen Qualitätsangebots, das sich dem Thema Wissen widmet.
    
    Ein solches Wissensprogramm wird weder öffentlich-rechtlich noch kommerziell angeboten.
    Es entspricht von seinen Inhalten dem Kern des öffentlich-rechtlichen Auftrags.
    Es tritt nicht in Konkurrenz zu bestehenden Angeboten.
    Der Nationale Hörfunk ist der angemessene Veranstalter, weil er wegen seiner überregionalen Struktur und seiner Präsenz in allen Ländern über enge Kontakte zu den Bildungs- und Wissenschaftseinrichtungen verfügt, zum Teil schon jetzt mit ihnen zusammenarbeitet und ihnen eine bundesweite publizistische Aufmerksamkeit verschaffen kann.
    Dies ist nicht nur von medienpolitischer, sondern auch von wissenschafts- wie gesellschaftspolitischer Bedeutung.
    _DRadio Wissen_ ist ein publizistischer Integrator in der föderalen Wissenschafts- und Bildungslandschaft.
    
    Inhaltlich und formal wird sich _DRadio Wissen_ als ein wortorientiertes Programm an den vorgegebenen Qualitätsstandards ausrichten.
    
2.  **Zielgruppe und inhaltliches Angebot**
    
    Deutschlandradio wird mit _DRadio Wissen_ ein digitales, werbefreies Vollprogramm veranstalten.
    Es verknüpft die Verlässlichkeit der Marke Deutschlandradio mit Kürze und Fasslichkeit der Darbietung sowie einem eindeutigen Nützlichkeitsversprechen.
    Es soll neben Deutschlandfunk und Deutschlandradio Kultur treten, prinzipiell alle Altersgruppen ansprechen, sich aber vor allem an die Zielgruppe „junge Erwachsene“ richten.
    Das Profil „Wissen“ ist jugendaffin.
    Jungen Leuten ist bewusst, dass Bildung, Ausbildung und Wissenserwerb Voraussetzungen für soziale Sicherheit, gesellschaftliche Anerkennung und beruflichen Aufstieg sind.
    
    Der Wissensbegriff ist weit gefächert.
    Er umfasst Forschungsergebnisse aus den Natur- und Geisteswissenschaften, Bildung und Beruf, Geschichte und Literatur, Gesundheit und Ernährung, Umwelt und Verbraucherschutz, Religion und Web-Wissen.
    Ein Programmangebot „Wissen“ steht im Einklang mit der von Politik, Wirtschaft, Gewerkschaften und allen kulturellen Institutionen getragenen Überzeugung, dass die Zukunftssicherung unseres Landes davon abhängt, in welchem Maße es gelingt, die Gesellschaft zu einer „Wissensgesellschaft“ zu formen.
    Ein digitales Wissens-Angebot, bei dem das Internet als Plattform gleichberechtigt neben das lineare Programm tritt, kommt den medialen Nutzungsgewohnheiten des jüngeren Publikums entgegen.
    Es fördert außerdem die dringend benötigte Akzeptanz digitaler Verbreitungswege für das Radio.
    
    Das neue Programm basiert nicht auf der Parallel-Ausstrahlung von auf anderen Kanälen zeitgleich gesendeten Formaten.
    _DRadio Wissen_ ist ein innovatives Vollprogramm.
    Es kann auf eine Fülle von Inhalten aus Deutschlandfunk und Deutschlandradio Kultur zurückgreifen.
    Die beiden Programme zeichnen sich durch einen unvergleichlich hohen Anteil an Eigenproduktionen/Erstsendungen aus (über 60 Prozent).
    Wissensangebote unterschiedlicher Genres sind in hohem Maße vorhanden.
    Viele können unverändert übernommen, andere müssen umformatiert werden.
    Als Beispiele für Sendungen deren Inhalte für _DRadio Wissen_ aufbereitet werden könnten, dienen etwa: Forschung aktuell, Campus & Karriere, PISAplus und Elektronische Welten.
    
    Der bereits generierte Stoff muss durch einzelne, speziell für _DRadio Wissen_ produzierte Beiträge ergänzt werden.
    Geeignete Inhalte aus den Programmen der Landesrundfunkanstalten der ARD sind über den vertraglich vereinbarten Programmaustausch (_Kooperationsvereinbarung zwischen ARD und Deutschlandradio vom 06.12.1994 auf der Grundlage von § 5 Deutschlandradio-Staatsvertrag)_ verfügbar und werden das Angebot bereichern.
    Deutschlandradio hat darüber hinaus interessierte Landesrundfunkanstalten eingeladen, innovative Formate zu entwickeln, die sie in den eigenen Programmen  
    ausstrahlen und für das nationale Wissensprogramm bereitstellen können.
    Diese Sendungen können in _DRadio Wissen_ integriert werden.
    Das Volumen der durch den Programmaustausch zur Verfügung gestellten Inhalte und die daraus zu gewinnenden Synergien beeinflussen das von Deutschlandradio für _DRadio Wissen_ zu planende Budget.
    
3.  **Programmstruktur**
    
    Eine besondere Herausforderung ist die Strukturierung des Programms.
    Es muss ebenso aktuelle Informationen aus allen Wissensbereichen wie vertiefende Berichterstattung anbieten.
    Es wird aus den von Deutschlandfunk und Deutschlandradio Kultur übernommenen, von anderen zugelieferten oder aus neu produzierten Beiträgen in Modulen gebündelt.
    Dieses Strukturprinzip gilt vornehmlich für die Hauptsendezeiten von 07.00 Uhr bis 20.00 Uhr.
    
    Ein wissensaffines Publikum erwartet eine klare und verlässliche Nachrichtenstruktur mit hoher Frequenz.
    Deutschlandradio kann sich dabei auf eine von ihm in Auftrag gegebene Nutzerstudie stützen _(Ergebnisse einer Elitenbefragung unter Politikern, Journalisten, Wirtschaftsmanagern und Führungskräften aus dem Bereich Kultur in Deutschland.
    Juni 2008, tns emnid)_.
    In dieser repräsentativen Studie setzen achtzig Prozent der Befragten ausführliche Nachrichtensendungen an die erste Stelle des von ihnen erwarteten idealtypischen Inhaltsprofils eines bundesweiten Informations- und Kulturprogramms.
    So wird _DRadio Wissen_ zwischen 07.00 Uhr und 20.00 Uhr ein durch Nachrichtenblöcke strukturiertes Programm nach der Stundenuhr anbieten.
    In einem 15- bis 20-minütigen Rhythmus werden aktuelle politische Nachrichten, Wissens- und Kulturnachrichten die Stunde gliedern.
    Für die Flächen zwischen den Nachrichtenblöcken werden themenbezogene Beitragsmodule erstellt.
    Thematisch folgt dies den Beschreibungen unter Punkt 2.
    Bildungspolitische und bildungspraktische Themen zum Primär-, Sekundär- und Tertiär-Bereich werden wegen des hohen Nutzwerts für die Zielgruppe eine herausragende Rolle spielen.
    
    Wissen bedeutet auch, fit zu sein für den Tag.
    Insoweit wird _DRadio Wissen_ im Rahmen dieser Beitragsmodule auch auf wichtige, politische, wirtschaftliche oder kulturelle Tagesereignisse einstimmen, zentrale Themen und Begriffe der Agenda erläutern und auf geeignete Sendungen von ARD, ZDF, arte und 3sat hinweisen und damit auch zum Programmführer für Wissenssendungen im öffentlich-rechtlichen Fernsehen werden.
    Für den aktuellen Bereich werden Eigenproduktionen nötig sein.
    Dabei kann auch die Form des Interviews gewählt werden, zumal dramaturgische Abwechslung innerhalb der Stundenuhr geboten ist.
    Die inhaltlichen Blöcke werden über den Tag rotieren, um den individuellen Nutzungsgewohnheiten und -möglichkeiten der beruflich gebundenen Hörerschaft entgegenzukommen.
    Die aktuellen Nachrichtensendungen sowie die Formatierung der Module setzen den Einsatz sachkundigen Personals voraus.
    
    Nach der schon zitierten Studie liegen kulturelle und politische Features (neben Interviews) mit sechzig Prozent an zweiter Stelle des von den Nutzern gewünschten Inhaltsprofils.
    Die Zeit nach 20 Uhr kann und wird unter Zurückstellung des engen Nachrichtentaktes Raum für Features und Dokumentationen sowie für längere Gesprächsformen bieten.
    Bis auf ein (eingeschränktes) aktuelles Nachrichtenangebot wird die Nachtstrecke vornehmlich für Wiederholungen genutzt.
    Die Programmgestaltung des Wochenendes wird durch entsprechende längere Formen dominiert.
    
    Im Bereich von Features und Dokumentationen kann Deutschlandradio auf einen Fundus eigener Beiträge und im Rahmen des Programmaustausches auch auf Sendungen der Landesrundfunkanstalten zurückgreifen.
    Gerade im Wissensbereich muss eine genaue Prüfung erfolgen, ob die in den Sendungen gemachten Aussagen noch dem aktuellen Kenntnisstand entsprechen.
    Dies kann die Nutzung dieses Repertoires einschränken und es setzt einen entsprechenden Personalaufwand für Auswahl, Bearbeitung und Kommentierung voraus.
    
    _DRadio Wissen_ bildet das lineare digitale Audio-Programmangebot.
    _DRadio Wissen_ gelangt ausschließlich auf digitalem Weg zu den Hörerinnen und Hörern.
    Die Verbreitungswege werden der Satellit, das Kabel, die digitale Terrestrik und der über das Internet verbreitete Livestream sein.
    Neben der Rotation inhaltlich bestimmter Module läßt sich mit der gezielten und zeitunabhängigen Nutzung des Internets eine Verstärkung der Nachhaltigkeit erreichen.
    Das Internet soll eindeutig sendungsbezogen auch als Plattform für Interaktion und Partizipation genutzt werden.
    Dafür sollen neue Formate erprobt werden.
    So bieten sich chats mit Redakteuren und Experten aus den verschiedenen Wissensgebieten an.
    Mit seinem „Blogspiel mit Radioanschluss“ hat Deutschlandradio Kultur bereits wertvolle Erfahrungen mit interaktiven Programmformaten gesammelt.
    
4.  **Kooperationen und Crossmedialität**
    
    DRadio Wissen arbeitet crossmedial.
    Die Inhalte des linearen Programms werden als audio, zum Teil verschriftet im Internet angeboten.
    DRadio Wissen verweist mit Programmtips, Interviews mit Autoren und Redakteuren von ARD, ZDF, arte und 3sat im Rahmen seiner Themenmodule auf anspruchsvolle Fernsehsendungen.
    Es erweitert damit sein eigenes inhaltliches Angebot und gibt den Hörern Hinweise auf ergänzende und vertiefende Informationen im öffentlich-rechtlichen Fernsehen und unterstreicht damit dessen Rolle als Qualitätsproduzent.
    
    Deutschlandradio arbeitet schon heute im Rahmen seines Informations- und Kulturauftrages mit einer Reihe von Stiftungen, Wissenschafts- und Bildungsinstitutionen zusammen, z.B.
    mit der Bundeszentrale für politische Bildung (Veranstaltungen zum Prager Frühling), mit dem Goethe-Institut, der Berlin-Brandenburgischen Akademie der Wissenschaften (ZEIT-Forum der Wissenschaft) oder mit dem Deutschen Museumsbund (Regionalmuseen-Sendereihe über 1 ½ Jahre).
    Diese Kooperationen beziehen sich auf einzelne Sendungen, auf Reihen und öffentliche Veranstaltungen.
    _DRadio Wissen_ wird diese Kooperationen ausbauen und kann unter Nutzung von Veranstaltungen dieser Institutionen neue auf dem Wissensmarkt vorhandene Informationen generieren und für sein Programm nutzen.
    Das Interesse dieser Institutionen an einer Zusammenarbeit mit dem Nationalen Hörfunk ist erkennbar groß.
    Bislang konnte nur ein überschaubares Angebot von Kooperationswünschen berücksichtigt werden.
    Bei _DRadio Wissen_ ergeben sich für beide Seiten und zum Nutzen der Hörerzielgruppen neue erweiterte Möglichkeiten der Zusammenarbeit.
    
    Deutschlandradio arbeitet schon zur Zeit intensiv mit Printmedien zusammen.
    Aufgrund der bisherigen Konzentration der Programme auf Information (Politik, Wirtschaft) und Kultur beschränkte sich die Zusammenarbeit weitgehend auf die Politik-Ressorts und das Feuilleton.
    Fachkundige Redakteure der Printmedien kommen im Deutschlandfunk und Deutschlandradio Kultur zu Wort.
    Beiträge aus den Programmen des Nationalen Hörfunks werden in den Printmedien abgedruckt.
    DRadio Wissen bietet die Möglichkeit, diese Kooperation auf die Ressorts Natur und Technik, Wissenschaft, Wissens-Seiten und auf entsprechende Periodika auszudehnen.
    Dabei können die bereits jetzt genutzten Kooperationsmodelle auf die Themengebiete von DRadio Wissen übertragen werden.
    
5.  **Wettbewerbssituation**
    
    DRadio Wissen ist als sinnhafte Ergänzung der medialen Angebots-Palette projektiert.
    Neben den Periodika bieten eine Reihe von Zeitungen Wissenssupplements oder zumeist wöchentlich erscheinende Wissens-Seiten an.
    In den meisten Fällen wird Wissen mit Forschung übersetzt.
    Auch im öffentlich-rechtlichen Rundfunk finden sich Sendungen und Rubriken mit Wissenscharakter.
    Hingegen existiert ein tägliches umfassendes Wissensangebot weder im Printbereich noch in den elektronischen Medien (Vollprogramm).
    _DRadio Wissen_ tritt also zu keinem vergleichbaren Angebot in Konkurrenz und ist ein Unikat.
    Es kann durch Kooperationen dazu beitragen, die Themen der Wissensgesellschaft stärker in der Öffentlichkeit zu verankern und den Bildungsinstitutionen und ihren Nutzern ein kontinuierliches Angebot zur Orientierung und zur Wissenserweiterung zu bieten.
    Dies ist auch von hohem Nutzwert für Bildungsinstitutionen (Schulen, Volkshochschulen, Universitäten, Weiterbildungseinrichtungen).
    Deutschlandradio schafft public value und nimmt öffentlich-rechtliche Verantwortung wahr.
    
6.  **Finanzierung und Verbreitung**
    
    Das neue, digitale Programm soll im Kölner Funkhaus des Nationalen Hörfunks produziert und von dort aus gesendet werden.
    Die Entscheidung für den Standort Köln wurde deshalb getroffen, weil hier aufgrund der baulichen Gegebenheiten nur geringe Aufwendungen für die Schaffung von Büro- und Studioraum anfallen werden und weil hier die größten Synergiegewinne zu erzielen sind.
    In Köln sitzt die Zentrale Nachrichtenredaktion von Deutschlandradio.
    Auch der Web-Auftritt von Deutschlandradio wird in Köln produziert.
    Deutschlandradio Kultur, das Berliner Programm, wird wichtige Stoffelemente zuliefern.
    
    Als finanzieller Rahmen wird für DRadio Wissen die Summe von rund 6 Mio.
    € p.a.
    kalkuliert.
    Als Starttermin ist der 1.
    Januar 2010 vorgesehen.
    Ab diesem Zeitpunkt werden die Kosten in voller Höhe anfallen.
    Das Programm soll über das bestehende DAB-Netz, über digitales Kabel und digitalen Satellit sowie als Internet-Livestream verbreitet werden.
    

**Anlage**  
(zu § 30 Abs.
5 Satz 1 Nr.
4 des Medienstaatsvertrages)

Negativliste öffentlich-rechtlicher Telemedien
----------------------------------------------

1.  Anzeigenrubriken, Anzeigen oder Kleinanzeigen,
2.  Branchenregister und -verzeichnisse,
3.  Preisvergleichsrubriken sowie Berechnungsprogramme (z.B.
    Preisrechner, Versicherungsrechner),
4.  Rubriken für die Bewertung von Dienstleistungen, Einrichtungen und Produkten,
5.  Partner-, Kontakt-, Stellen-, Tauschbörsen,
6.  Ratgeberrubriken ohne Bezug zu Sendungen,
7.  Business-Networks,
8.  Telekommunikationsdienstleistungen im Sinne von § 3 Nr.
    24 des Telekommunikationsgesetzes,
9.  Wetten im Sinne von § 762 des Bürgerlichen Gesetzbuches,
10.  Softwareangebote, soweit nicht zur Wahrnehmung des eigenen Angebots erforderlich,
11.  Routenplaner,
12.  Verlinkungen ohne redaktionelle Prüfung und Verlinkungen, die unmittelbar zu Kaufaufforderungen führen mit der Ausnahme von Verlinkungen auf eigene audiovisuelle Inhalte kommerzieller Tochtergesellschaften,
13.  Musikdownload von kommerziellen Fremdproduktionen; dies gilt nicht soweit es sich um ein zeitlich befristetes aktionsbezogenes Angebot zum Download von Musiktiteln handelt,
14.  Spieleangebote ohne Bezug zu einer Sendung,
15.  Fotodownload ohne Bezug zu einer Sendung,
16.  Veranstaltungskalender (auf eine Sendung bezogene Hinweise auf Veranstaltungen sind zulässig),
17.  Foren, Chats ohne Bezug zu Sendungen und redaktionelle Begleitung; Foren, Chats unter Programm- oder Sendermarken sind zulässig.
    Foren und Chats dürfen nicht inhaltlich auf Angebote ausgerichtet sein, die nach den Nummern 1 bis 16 unzulässig sind.

**Anlage**  
(zu § 33 Abs.
5 Satz 1 des Medienstaatsvertrages)

Negativliste Jugendangebot
--------------------------

1.  Anzeigenrubriken, Anzeigen oder Kleinanzeigen,
2.  Branchenregister und -verzeichnisse,
3.  Preisvergleichsrubriken sowie Berechnungsprogramme (zum Beispiel Preisrechner, Versicherungsrechner),
4.  Rubriken für die Bewertung von Dienstleistungen, Einrichtungen und Produkten,
5.  Partner-, Kontakt-, Stellen-, Tauschbörsen,
6.  Ratgeberrubriken ohne journalistisch-redaktionellen Bezug zum Jugendangebot,
7.  Business-Networks,
8.  Telekommunikationsdienste im Sinne von § 3 Nr.
    24 des Telekommunikationsgesetzes,
9.  Wetten im Sinne von § 762 des Bürgerlichen Gesetzbuches,
10.  Softwareangebote, soweit nicht zur Wahrnehmung des eigenen Angebots erforderlich,
11.  Routenplaner,
12.  Verlinkungen ohne redaktionelle Prüfung und Verlinkungen, die unmittelbar zu Kaufaufforderungen führen,
13.  Musikdownload von kommerziellen Fremdproduktionen, soweit es sich um ein zeitlich unbefristetes nicht-aktionsbezogenes Angebot zum Download von Musiktiteln handelt,
14.  Spieleangebote ohne journalistisch-redaktionellen Bezug zum Jugendangebot,
15.  Fotodownload ohne journalistisch-redaktionellen Bezug zum Jugendangebot,
16.  Veranstaltungskalender ohne journalistisch-redaktionellen Bezug zum Jugendangebot,
17.  Foren und Chats ohne redaktionelle Begleitung.
    Im Übrigen dürfen Foren und Chats nicht inhaltlich auf Angebote ausgerichtet sein, die nach den Nummern 1 bis 16 unzulässig sind.

[zum Gesetz](/gesetze/modernisierung_medienordnung)

**Protokollerklärung aller Länder zum Staatsvertrag zur Modernisierung der Medienordnung in Deutschland**

Der Medienstaatsvertrag ist die Antwort der Länder als Mediengesetzgeber auf zentrale Fragen und Herausforderungen einer digitalisierten Medienwelt.
Die Länder sind sich einig, dass die Anpassung des Rechtsrahmens an die digitale Transformation mit dem vorliegenden Staatsvertrag nicht abgeschlossen ist.
Die Länder werden zu den nachfolgenden Themen weitergehende Reformvorschläge erarbeiten und haben dazu Arbeitsgruppen eingerichtet.

1.
Barrierefreiheit

Artikel 21 des Übereinkommens der Vereinten Nationen über die Rechte von Menschen mit Behinderungen (UN-Behindertenrechtskonvention) verpflichtet die Konventionsstaaten, „geeignete Maßnahmen zu treffen, damit Menschen mit Behinderung ihr Recht auf Meinungsäußerung und Meinungsfreiheit gleichberechtigt mit anderen durch die von ihnen gewählten Formen der Kommunikation ausüben können“.
Ziel der Länder ist es daher, durch den Ausbau barrierefreier Medienangebote allen Menschen die Teilhabe am medialen Diskurs und an der Gesellschaft insgesamt zu ermöglichen.
Im Zuge der Umsetzung der Richtlinie über audiovisuelle Mediendienste (AVMD-Richtlinie) geht der Medienstaatsvertrag hier wichtige Schritte zur Weiterentwicklung der Barrierefreiheit in den Medien.
Darüber hinausgehende Maßnahmen wollen die Länder unter weiterer Einbeziehung der Verbände, der Beauftragten der Landesregierungen und des Bundes sowie der Anbieter erarbeiten.
Angesichts der fortgeschrittenen technischen Möglichkeiten erwarten die Länder von allen Medienanbietern indes bereits heute verstärkte Anstrengungen beim Ausbau barrierefreier Angebote – ungeachtet gesetzlicher Verpflichtungen.

2.
Jugendmedienschutz

Die Länder setzen sich dafür ein, Kindern und Jugendlichen ein sicheres Heranwachsen in der Mediengesellschaft zu ermöglichen.
Dies bedeutet einerseits Schutz vor schädlichen Inhalten und Angeboten, andererseits die aktive Teilhabe von Kindern und Jugendlichen an der Mediennutzung.
Über die mit dem vorliegenden Staatsvertrag vorgenommene Umsetzung der AVMD-Richtlinie hinaus wollen die Länder zeitnah entschlossene Schritte für eine umfassende Reform des Jugendmedienschutzes in Deutschland angehen.
Hierzu bedarf es neuer Ansätze und Ideen, insbesondere auch mit Blick auf die Möglichkeiten des technischen Jugendmedienschutzes.
Ziel der Länder ist dabei ein kohärenter und mit der Gesetzgebung des Bundes abgestimmter Rechtsrahmen, der für Anbieter, Eltern und Kinder gleichermaßen Klarheit und Sicherheit bietet.

3.
Regionale Vielfalt

Die Länder setzen sich für eine vielfältige, lokal und regional ausdifferenzierte Medienlandschaft in Deutschland ein.
Ihre Gewährleistung ist Voraussetzung für eine ausgewogene nationale, regionale und lokale Meinungsbildung und damit auch Fundament unserer pluralistischen Gesellschaft; ihr Funktionieren ermöglicht die Beteiligung am öffentlichen Leben.
Mit dem Ziel, auch künftig eine differenzierte, professionelle und relevante Berichterstattung aus allen Teilen der Bundesrepublik zu erhalten, werden die Länder – über die bereits im Zusammenhang mit dem Medienstaatsvertrag getroffenen Vereinbarungen hinaus – Maßnahmen zur Sicherung der regionalen und lokalen Medienvielfalt prüfen.
Neben tradierten Medienhäusern sollen in diesen Prozess auch weitere Akteure (u.a.
Medienplattformen und -intermediäre) einbezogen werden.

4.
Rundfunkzulassung

Die Länder setzen sich dafür ein, die aktive Teilnahme am medialen Diskurs ohne unnötige Hürden zu ermöglichen.
Gleichzeitig betonen die Länder die Bedeutung zentraler Werte und Standards – insbesondere im Bereich des Jugend- und Verbraucherschutzes sowie bei der Sicherung der Meinungsvielfalt.
Dies schließt wesentlich auch die Benennbarkeit verantwortlicher Personen und deren Haftbarmachung ein.
Mit der teilweisen Abschaffung der Zulassungspflicht für Rundfunkprogramme haben die Länder mit dem Medienstaatsvertrag für eine Vielzahl von Angeboten spürbare Erleichterungen und Verfahrensvereinfachungen geschaffen.
Ob und wie eine vollständige Abschaffung der Zulassungspflicht – beispielsweise zugunsten einer abgestuften Anzeigepflicht – sinnvoll ist, wollen die Länder im Weiteren prüfen.
Bei diesen Überlegungen soll auch das Ziel möglichst gleichwertiger Wettbewerbsbedingungen zwischen Rundfunk und Telemedien hinreichende Berücksichtigung finden.

5.
Medienkonzentrationsrecht

Die Länder setzen sich für ein zukunftsfähiges Medienkonzentrationsrecht ein.
Dieses muss den real bestehenden Gefahren für die Meinungsvielfalt wirksam begegnen können.
Die Medienmärkte haben in den letzten Jahren eine Öffnung erfahren, die neben dem Fernsehen auch andere Mediengattungen, die möglichen Folgen crossmedialer Zusammenschlüsse und auch solcher auf vor- und nachgelagerten Märkten verstärkt in den Fokus rückt.
Ein reformiertes Medienkonzentrationsrecht muss daher alle medienrelevanten Märkte in den Blick nehmen.