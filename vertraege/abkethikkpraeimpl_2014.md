## Abkommen zwischen den Ländern Brandenburg, Freie Hansestadt Bremen, Freie und Hansestadt Hamburg, Mecklenburg-Vorpommern, Niedersachsen und Schleswig-Holstein über die gemeinsame Einrichtung einer Ethikkommission für Präimplantationsdiagnostik bei der Ärztekammer Hamburg

Das Land Brandenburg,  
vertreten durch den Ministerpräsidenten,  
dieser vertreten durch die Ministerin für Umwelt, Gesundheit und Verbraucherschutz,

die Freie Hansestadt Bremen,  
vertreten durch den Senat,  
dieser vertreten durch den Senator für Gesundheit,

die Freie und Hansestadt Hamburg,  
vertreten durch den Senat,  
dieser vertreten durch die Präses der Behörde für Gesundheit und Verbraucherschutz,

das Land Mecklenburg-Vorpommern,  
vertreten durch den Ministerpräsidenten,  
dieser vertreten durch die Ministerin für Arbeit, Gleichstellung und Soziales,

das Land Niedersachsen,  
vertreten durch den Ministerpräsidenten,  
dieser vertreten durch die Ministerin für Soziales, Frauen, Familie, Gesundheit und Integration

und

das Land Schleswig-Holstein,  
endvertreten durch die Ministerin für Soziales, Gesundheit, Familie und Gleichstellung,  
schließen vorbehaltlich der Zustimmung ihrer verfassungsmäßig berufenen Organe nachstehendes Abkommen:

### § 1  
Grundlage und Zweck des Abkommens

Die Durchführung der Präimplantationsdiagnostik ist gemäß § 3 a des Embryonenschutzgesetzes vom 13.
Dezember 1990 (BGBl.
I S. 2746), das zuletzt durch Artikel 1 des Gesetzes vom 21.
November 2011 (BGBl.
I S. 2228) geändert worden ist, an konkrete Voraussetzungen geknüpft.
Hierzu gehört die Beteiligung einer Ethikkommission, die vor Durchführung der Maßnahme eine zustimmende Bewertung abgegeben haben muss.
Die an diesem Abkommen beteiligten Länder richten auf der Grundlage des § 4 Absatz 1 der Verordnung zur Regelung der Präimplantationsdiagnostik (Präimplantationsdiagnostikverordnung - PIDV) vom 21. Februar 2013 (BGBl.
I S. 323) gemeinsam eine Ethikkommission für Präimplantationsdiagnostik als unselbständige Einrichtung bei der Ärztekammer Hamburg ein.

### § 2  
Aufgabe und Zuständigkeit der Ethikkommission für Präimplantationsdiagnostik

Die Ethikkommission hat die Aufgabe der Prüfung von Anträgen auf Durchführung einer Präimplantationsdiagnostik nach § 5 Absatz 1 PIDV, soweit die Antragsberechtigte beabsichtigt, diese Maßnahme in einem Zentrum durchführen zu lassen, das seinen Sitz in einem der am Abkommen beteiligten Länder hat und das von diesem nach § 3 Absatz 1 PIDV zugelassen worden ist.

### § 3  
Zusammensetzung der Ethikkommission

Der Ethikkommission gehören acht Mitglieder an.
Frauen und Männer haben zu gleichen Teilen Berücksichtigung zu finden.
Als Sachverständige der Fachrichtung Medizin gemäß § 4 Absatz 1 Satz 3 PIDV sind eine Humangenetikerin oder ein Humangenetiker, eine Fachärztin oder ein Facharzt für Frauenheilkunde und Geburtshilfe, eine Pädiaterin oder ein Pädiater und eine ärztliche Psychotherapeutin oder ein ärztlicher Psychotherapeut zu berufen.
Darüber hinaus sind jeweils eine Sachverständige oder ein Sachverständiger der Fachrichtung Ethik und der Fachrichtung Recht zu berufen.
Als weitere Mitglieder gehören der Ethikkommission jeweils eine Vertreterin oder ein Vertreter der für die Wahrnehmung der Interessen der Patientinnen und Patienten und der Selbsthilfe der Menschen mit Behinderungen an, die sich in einer in den Mitgliedsländern des Abkommens hierfür maßgeblichen Organisation engagieren.

#### § 4  
Benennung und Berufung der Mitglieder

(1) Die Benennung der ärztlichen Mitglieder und deren Vertreterinnen oder Vertreter erfolgt durch die am Abkommen beteiligten Länder auf der Grundlage eines Benennungsvorschlags der Ärztekammer Hamburg.
Diese hat die anderen im Geltungsbereich des Abkommens ansässigen Landesärztekammern bei der Erstellung des Benennungsvorschlags zu beteiligen.
Nach Herstellung des Einvernehmens unter den am Abkommen beteiligten Ländern über die zu benennenden Personen erfolgt deren Berufung durch die Ärztekammer Hamburg.

(2) Für die Auswahl der weiteren Mitglieder und deren Vertreterinnen und Vertreter unterbreiten die am Abkommen beteiligten Länder der für das Gesundheitswesen zuständigen Behörde der Freien und Hansestadt Hamburg Benennungsvorschläge.
Nach Herstellung des Einvernehmens unter den am Abkommen beteiligten Ländern über die zu benennenden Personen erfolgt deren Berufung durch die Ärztekammer Hamburg.

(3) Für jedes Mitglied der Ethikkommission können maximal zwei Vertreterinnen oder Vertreter berufen werden.

(4) Die Mitglieder der Ethikkommission werden für die Dauer von fünf Jahren berufen.
Eine einmalige Wiederberufung ist möglich.

(5) Die in die Ethikkommission berufenen Mitglieder sowie ihre Vertreterinnen und Vertreter sind namentlich in den jeweiligen amtlichen Verkündungsblättern der am Abkommen beteiligten Länder bekannt zu machen.

### § 5  
Berichtspflicht und Informationsaustausch

(1) Die Ethikkommission berichtet jährlich gegenüber der für das Gesundheitswesen zuständigen Behörde der Freien und Hansestadt Hamburg über die Anzahl der mit Zustimmung versehenen und der abgelehnten Anträge.
Der Bericht hat auch Angaben darüber zu enthalten, welche Erbkrankheiten den Anträgen zugrunde lagen.
Die am Abkommen beteiligten Länder erhalten von der für das Gesundheitswesen zuständigen Behörde der Freien und Hansestadt Hamburg eine Ausfertigung des Berichts.

(2) Die am Abkommen beteiligten Länder treffen sich mindestens einmal jährlich, um sich über die Entwicklung der Präimplantationsdiagnostik fachlich auszutauschen.
Zu diesen Treffen können sachverständige Personen eingeladen werden.

### § 6  
Finanzierung der Ethikkommission

Die Finanzierung der Tätigkeit der Ethikkommission erfolgt ausschließlich über Gebühren.
Die Ärztekammer Hamburg erlässt auf der Grundlage des § 7 Absatz 1 Nummer 5 die notwendigen gebührenrechtlichen Bestimmungen für eine kostendeckende Finanzierung.

### § 7  
Satzung der Ärztekammer Hamburg und Genehmigung

(1) Die Ärztekammer Hamburg erlässt für die Tätigkeit der Ethikkommission eine Satzung, in der insbesondere zu regeln sind

1.  die Einrichtung einer Geschäftsstelle,
    
2.  das Verfahren zur Bestimmung des oder der Vorsitzenden und seiner oder ihrer Aufgaben,
    
3.  eine Verfahrensordnung,
    
4.  die Entschädigung der Mitglieder,
    
5.  die Kosten für die Antragsberechtigten einschließlich der im Rahmen der Prüfung anfallenden Auslagen.
    

(2) Die Genehmigung der Satzung erfolgt auf der Grundlage des § 57 des Hamburgischen Kammergesetzes für die Heilberufe vom 14.
Dezember 2005 (HmbGVBl.
2005 S. 495, 2006 S. 35), zuletzt geändert am 19.
Juni 2012 (HmbGVBl.
S. 254, 260), durch die Aufsichtsbehörde mit der Maßgabe, zuvor das Benehmen mit den anderen am Abkommen beteiligten Ländern herzustellen.

### § 8  
Rechtsmittel gegen die Entscheidung der Ethikkommission

Gegen ablehnende Entscheidungen von Anträgen auf Durchführung einer Präimplantationsdiagnostik steht den Antragsberechtigten der Rechtsweg zu den Verwaltungsgerichten offen.
Ein Vorverfahren im Sinne des § 68 der Verwaltungsgerichtsordnung findet nicht statt.

### § 9  
Gesamtschuldnerische Haftung

Für Ansprüche aus Schadenersatzforderungen gegenüber der Ethikkommission haften die am Abkommen beteiligten Länder gesamtschuldnerisch im Verhältnis zueinander entsprechend den jeweiligen Länderanteilen des Königsteiner Schlüssels in der jeweils geltenden Fassung.

### § 10  
Beitritt weiterer Länder

(1) Weitere Länder können diesem Abkommen im Einvernehmen mit den bereits am Abkommen beteiligten Ländern beitreten.
Der Beitritt erfolgt durch schriftliche Erklärung des Beitritts gegenüber der für das Gesundheitswesen zuständigen Behörde der Freien und Hansestadt Hamburg und, soweit erforderlich, mit Zustimmung der gesetzgebenden Körperschaft des beitretenden Landes.
Über den Eingang der Beitrittserklärung unterrichtet die für das Gesundheitswesen zuständige Behörde der Freien und Hansestadt Hamburg die übrigen am Abkommen beteiligten Länder.

(2) Für das beitretende Land treten die Regelungen dieses Abkommens am Tag nach dem Eingang der Beitrittserklärung und, soweit erforderlich, der Anzeige der Zustimmung seiner gesetzgebenden Körperschaft in Kraft.

### § 11  
Geltungsdauer und Kündigung

(1) Das Abkommen wird für unbestimmte Zeit geschlossen.

(2) Das Abkommen ist unter Einhaltung einer Frist von einem Jahr zum Ende eines Kalenderjahres durch schriftliche Erklärung unter Angabe der maßgeblichen Gründe gegenüber allen am Abkommen beteiligten Länder kündbar.
Die Kündigung eines am Abkommen beteiligten Landes berührt den Fortbestand des Abkommens nicht.
Dies gilt nicht im Falle einer Kündigung durch die Freie und Hansestadt Hamburg.

### § 12  
Inkrafttreten, Außerkrafttreten

Dieses Abkommen tritt am 1.
Februar 2014 in Kraft.
Die Vertragsurkunden der am Abkommen beteiligten Länder werden bei der für das Gesundheitswesen zuständigen Behörde der Freien und Hansestadt Hamburg hinterlegt.
Für am Abkommen beteiligte Länder, deren Vertragsurkunde nach dem 1.
Februar 2014 hinterlegt wird, wird das Abkommen an dem Tag wirksam, der der Hinterlegung der Vertragsurkunde folgt.

Für das Land Brandenburg  
Der Ministerpräsident  
Vertreten durch die Ministerin für Umwelt, Gesundheit und Verbraucherschutz

Potsdam, den 07.11.2013            Anita Tack

Für die Freie Hansestadt Bremen  
Für den Senat  
Der Senator für Gesundheit

Bremen, den 26.11.2013              Dr.
Hermann Schulte-Sasse

Für die Freie und Hansestadt Hamburg  
Für den Senat  
Die Präses der Behörde für Gesundheit und Verbraucherschutz

Hamburg, den 05.11.2013            Cornelia Prüfer-Storcks

Für das Land Mecklenburg-Vorpommern  
Für den Ministerpräsidenten  
Die Ministerin für Arbeit, Gleichstellung und Soziales

Schwerin, den 13.11.2013            Manuela Schwesig

Für das Land Niedersachsen  
In Vertretung des Ministerpräsidenten  
Die Ministerin für Soziales, Frauen, Familie, Gesundheit und Integration

Hannover, den 03.12.2013           Cornelia Rundt

Für das Land Schleswig-Holstein  
Endvertreten durch die Ministerin für Soziales, Gesundheit, Familie und Gleichstellung

Kiel, den  29.11.2013                    Kristin Alheit

#### Protokollerklärung des Landes Brandenburg zu § 9 des Abkommens

Im Falle einer Inanspruchnahme nach § 9 des Abkommens behält sich Brandenburg ein jederzeitiges Prüfungsrecht im Sinne des § 39 Absatz 3 Landeshaushaltsordnung Brandenburg vor.
Das Prüfungsrecht beinhaltet, auf Verlangen der zuständigen Landesbehörde und ihrer Beauftragten alle bei der Ethikkommission vorhandenen Unterlagen, die den Haftungssachverhalt betreffen, vorzulegen.
Dies gilt entsprechend bei einem Verlangen des Landesrechnungshofs und den von diesem Beauftragten.

Das Land Brandenburg ist bei Vorliegen der gesetzlichen Voraussetzungen berechtigt, unmittelbar von den Anspruchstellern Auskünfte über die mit der gewährten Haftung zusammenhängenden Fragen zu verlangen.

Unterliegen Unterlagen, die zu Prüfungszwecken herausgegeben werden sollen, der ärztlichen Schweigepflicht, so sind diese Unterlagen vor der Herausgabe zu anonymisieren.

[zum Gesetz](/gesetze/ethikkpraeimpl_g_2014)