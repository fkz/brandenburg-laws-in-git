## Staatsvertrag zwischen dem Land Berlin und dem Land Brandenburg über die Einrichtung und den Betrieb eines klinischen Krebsregisters nach § 65c des Fünften Buches Sozialgesetzbuch

Das Land Berlin

und

das Land Brandenburg

schließen nachstehenden Staatsvertrag:

**Inhaltsverzeichnis**

[Präambel](#0)

### Abschnitt 1  
Organisation, Beleihung, Aufgaben und Finanzierung

[Artikel 1 Einrichtung und Einzugsgebiete des klinischen Krebsregisters](#1)  
[Artikel 2 Organisation des klinischen Krebsregisters](#2)  
[Artikel 3 Begriffsbestimmungen](#3)  
[Artikel 4 Beleihung](#4)  
[Artikel 5 Finanzierung](#5)  
[Artikel 6 Aufgaben des klinischen Krebsregisters](#6)  
[Artikel 7 Wissenschaftlicher Beirat](#7)  
[Artikel 8 Regionale Qualitätskonferenzen und Gemeinsame Qualitätskonferenz](#8)

### Abschnitt 2  
Aufsicht und Prüfrechte

[Artikel 9 Aufsicht](#9)  
[Artikel 10 Prüfrecht der Rechnungshöfe und Finanzkontrolle](#10)

### Abschnitt 3  
Meldungen

[Artikel 11 Meldepflichten](#11)  
[Artikel 12 Meldeanlässe](#12)  
[Artikel 13 Inhalt und Form der Meldungen](#13)  
[Artikel 14 Informationspflichten der meldepflichtigen Personen und der Meldestellen gegenüber Patientinnen und Patienten](#14)

### Abschnitt 4  
Rechte der Patientinnen und Patienten

[Artikel 15 Widerspruchsrecht](#15)  
[Artikel 16 Widerspruch gegen die Speicherung medizinischer Daten im klinischen Krebsregister](#16)  
[Artikel 17 Auskunftsrecht](#17)  
[Artikel 18 Löschung und Sperrung des Direktabrufs](#18)

### Abschnitt 5  
Abrechnungsverfahren

[Artikel 19 Grundsätze der Abrechnung](#19)  
[Artikel 20 Abrechnung mit den Krankenkassen und den Ersatzkassen](#20)  
[Artikel 21 Abrechnung mit den privaten Krankenversicherungen](#21)  
[Artikel 22 Abrechnung mit den Trägern der Kosten in Krankheits-, Pflege- und Geburtsfällen nach beamtenrechtlichen Vorschriften](#22)  
[Artikel 23 Abrechnung mit den meldepflichtigen Personen und den Meldestellen](#23)

### Abschnitt 6  
Datenverarbeitung

[Artikel 24 Versorgungsbereich](#24)  
[Artikel 25 Auswertungsbereich](#25)  
[Artikel 26 Landesauswertungsstelle](#26)  
[Artikel 27 Koordinierungsstelle](#27)  
[Artikel 28 Geheimhaltungspflichten](#28)

### Abschnitt 7  
Datenaustausch mit Dritten

[Artikel 29 Datenaustausch mit dem Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen](#29)  
[Artikel 30 Datenaustausch mit anderen klinischen Krebsregistern](#30)  
[Artikel 31 Patientenbezogene Datenabfrage durch meldepflichtige Personen und Meldestellen](#31)  
[Artikel 32 Datenübermittlung für die Versorgungsforschung](#32)  
[Artikel 33 Datenübermittlung für Zwecke der Krankenhausplanung](#33)

### Abschnitt 8  
Straftaten und Ordnungswidrigkeiten

[Artikel 34 Straftaten](#34)  
[Artikel 35 Ordnungswidrigkeiten](#35)

### Abschnitt 9  
Übergangsvorschriften

[Artikel 36 Übergangsregelungen für meldepflichtige Personen und Meldestellen mit Sitz im Land Berlin](#36)  
[Artikel 37 Altfallregelung für das Land Brandenburg](#37)

### Abschnitt 10  
Schlussvorschriften

[Artikel 38 Geltungsdauer und Beendigung](#38)  
[Artikel 39 Einschränkung von Grundrechten](#39)  
[Artikel 40 Ratifikation und Inkrafttreten](#40)

#### Präambel

Der Staatsvertrag dient der Umsetzung der mit § 65c des Fünften Buches Sozialgesetzbuch - Gesetzliche Krankenversicherung - (Artikel 1 des Gesetzes vom 20.
Dezember 1988, BGBl.
I S. 2477, 2482), das zuletzt durch Artikel 2 des Gesetzes vom 17.
Februar 2016 (BGBl.
I S.
203, 231) geändert worden ist, für die Länder geschaffenen Verpflichtung zur Einrichtung flächendeckender klinischer Krebsregister.
Es ist gemeinsamer Wille der Länder Berlin und Brandenburg, durch die Einrichtung und den Betrieb eines länderübergreifenden klinischen Krebsregisters beider Länder die flächendeckende Qualitätssicherung der onkologischen Versorgung sicherzustellen.

### Abschnitt 1  
Organisation, Beleihung, Aufgaben und Finanzierung

#### Artikel 1  
Einrichtung und Einzugsgebiete des klinischen Krebsregisters

(1) Die Länder Berlin und Brandenburg richten zur Verbesserung der Qualität der onkologischen Versorgung nach § 65c des Fünften Buches Sozialgesetzbuch ein klinisches Krebsregister ein.
Von ihm werden die Daten von Tumorpatientinnen und -patienten nach Maßgabe des Artikels 11 Absatz 4 erfasst, die ihren Hauptwohnsitz in einem der beiden Länder haben oder von einer Ärztin oder einem Arzt, einer Zahnärztin oder einem Zahnarzt oder einer Psychologischen Psychotherapeutin oder einem Psychologischen Psychotherapeuten mit Sitz in einem der beiden Länder behandelt werden.

(2) Einzugsgebiete des klinischen Krebsregisters sind jeweils das Land Berlin und das Land Brandenburg.

#### Artikel 2  
Organisation des klinischen Krebsregisters

(1) Das klinische Krebsregister der Länder Berlin und Brandenburg wird in alleiniger Trägerschaft der Landesärztekammer Brandenburg errichtet.

(2) Das klinische Krebsregister besteht aus der Koordinierungsstelle mit Sitz im Land Brandenburg, einer dezentralen Registerstelle im Land Berlin sowie mehreren dezentralen Registerstellen im Land Brandenburg.

(3) Das klinische Krebsregister ist in Bezug auf die Datenverarbeitung in einen Versorgungsbereich (Artikel 24), einen Auswertungsbereich (Artikel 25) und die Koordinierungsstelle (Artikel 27), die zugleich die Aufgaben der Landesauswertungsstelle nach Artikel 26 wahrnimmt, untergliedert.
Der Versorgungsbereich umfasst alle dezentralen Registerstellen.
Der Auswertungsbereich umfasst mindestens eine dezentrale Registerstelle je Einzugsgebiet.
Die in Satz 1 genannten Organisationseinheiten sind Daten verarbeitende Stellen im Sinne von § 4 Absatz 3 Nummer 1 des Berliner Datenschutzgesetzes und § 3 Absatz 4 Nummer 1 des Brandenburgischen Datenschutzgesetzes und sowohl räumlich als auch organisatorisch und personell voneinander zu trennen.
Die Trennung nach Satz 4 besteht unabhängig von der unternehmensorganisatorischen Struktur des klinischen Krebsregisters.

#### Artikel 3  
Begriffsbestimmungen

(1) Identitätsdaten sind folgende Angaben über die persönlichen Verhältnisse der Patientin oder des Patienten, die eine Identifizierung der Patientin oder des Patienten unmittelbar oder mittelbar ermöglichen:

1.  Familienname, Vornamen, frühere Namen,
2.  Geschlecht,
3.  Geburtsdatum,
4.  Anschriften der Hauptwohnung (derzeitige und letzte frühere),
5.  Datum der ersten Tumordiagnose,
6.  Sterbedatum und
7.  Krankenversichertennummer und Name der Krankenkasse bei gesetzlich krankenversicherten Personen, Name der Versicherung und Versicherungs- oder Vertragsnummer bei privat krankenversicherten Personen sowie Bezeichnung der Festsetzungsstelle und für die Leistungsbeantragung notwendige individuelle Nummer der beihilfeberechtigten oder berücksichtigungsfähigen Person bei Personen mit einem Anspruch gegen Träger der Kosten in Krankheits-, Pflege- und Geburtsfällen nach beamtenrechtlichen Vorschriften.

(2) Epidemiologische Daten sind abweichend von § 2 Absatz 2 des Krebsregistergesetzes vom 4. November 1994 (BGBl.
I S. 3351), das gemäß Artikel 13 Absatz 1 des Staatsvertrages über das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen vom 20./24.
November 1997 als Landesrecht fort gilt, folgende Angaben:

1.  Geschlecht,
2.  Monat und Jahr der Geburt,
3.  Wohnort oder Gemeindekennziffer,
4.  Tumordiagnose nach dem Schlüssel der Internationalen Klassifikation der Krankheiten (ICD) in der jeweiligen vom Deutschen Institut für medizinische Dokumentation und Information im Auftrag des Bundesministeriums für Gesundheit in Kraft gesetzten und im Bundesanzeiger veröffentlichten Fassung,
5.  Histologie nach dem Schlüssel der Internationalen Klassifikation der onkologischen Krankheiten (ICD-O) in der jeweiligen vom Deutschen Institut für medizinische Dokumentation und Information veröffentlichten Fassung,
6.  Lokalisation des Tumors, einschließlich der Angabe der Seite bei paarigen Organen,
7.  Monat und Jahr der ersten Tumordiagnose,
8.  früheres Tumorleiden,
9.  Stadium der Erkrankung (insbesondere TNM-Schlüssel zur Darstellung der Größe und des Metastasierungsgrades der Tumoren),
10.  Sicherung der Diagnose (klinischer Befund, Histologie, Zytologie, Obduktion und andere),
11.  Art der Therapie (kurative oder palliative Operationen, Strahlen-, Chemo- oder andere Therapiearten),
12.  Sterbemonat und -jahr,
13.  Todesursache (Grundleiden) sowie
14.  durchgeführte Autopsie.

(3) Klinische Daten sind:

1.  epidemiologische Daten im Sinne des Absatzes 2 Nummer 1, 2 und 4 bis 14 sowie
2.  alle im nach § 65c Absatz 1 Satz 3 des Fünften Buches Sozialgesetzbuch bundesweit einheitlichen Datensatz der Arbeitsgemeinschaft Deutscher Tumorzentren und der Gesellschaft der epidemiologischen Krebsregister in Deutschland zur Basisdokumentation für Tumorkranke und in ihn ergänzenden Modulen aufgeführten Merkmale in der jeweils gültigen und im Bundesanzeiger veröffentlichten Fassung, soweit sie nicht in Absatz 1 Nummer 1 bis 4 und 7 sowie den Absätzen 5 und 6 Nummer 7 bis 11 aufgeführt sind.

(4) Medizinische Daten sind alle in den Absätzen 1 bis 3 sowie 5 und 6 aufgeführten Daten mit Ausnahme der Daten des Absatzes 1 Nummer 1 bis 4.

(5) Daten zur meldepflichtigen Person oder Meldestelle sind:

1.  Name und Vorname der meldepflichtigen Person,
2.  Institution der meldepflichtigen Person einschließlich Abteilung, Station und Fachgebiet sowie
3.  Anschrift, Telefon- und Faxnummer sowie E-Mail-Adresse der Praxis oder Institution.

(6) Für die Abrechnung notwendige zusätzliche Daten sind:

1.  Institutionskennzeichen des Krankenhauses oder für nach § 108 des Fünften Buches Sozialgesetzbuch nicht zugelassene Krankenhäuser Name und Anschrift des Krankenhauses,
2.  Kennzeichen des letztbehandelnden Standortes, soweit das Krankenhaus nach dem jeweiligen Krankenhausplan über mehrere Standorte verfügt,
3.  lebenslange Arztnummer,
4.  Betriebsstättennummer des Vertragsarztsitzes,
5.  Name, Vorname und Anschrift der meldepflichtigen Person, sofern eine privatärztliche oder privatzahnärztliche Abrechnung erfolgt,
6.  Zahnarztnummer,
7.  Tumordiagnose und Seitenlokalisation nach dem Schlüssel der Internationalen Klassifikation der Krankheiten (ICD) in der jeweiligen vom Deutschen Institut für medizinische Dokumentation und Information im Auftrag des Bundesministeriums für Gesundheit in Kraft gesetzten und im Bundesanzeiger veröffentlichten Fassung,
8.  Datum des Meldeanlasses,
9.  Art des Meldeanlasses,
10.  Krankenversichertennummer und Name der Krankenkasse bei gesetzlich krankenversicherten Personen, Name der Versicherung und Versicherungs- oder Vertragsnummer bei privat krankenversicherten Personen sowie Bezeichnung der Festsetzungsstelle und für die Leistungsbeantragung notwendige individuelle Nummer der beihilfeberechtigten oder berücksichtigungsfähigen Person bei Personen mit einem Anspruch gegen Träger der Kosten in Krankheits-, Pflege- und Geburtsfällen nach beamtenrechtlichen Vorschriften sowie
11.  Kontoinhaberin oder Kontoinhaber und Bankverbindung der meldepflichtigen Person oder der Meldestelle.

(7) Best-of-Datensatz ist der Datensatz, in dem die besten Informationen aus mehreren Meldungen zu Diagnose, Therapie und dem Verlauf eines Tumors einer Patientin oder eines Patienten mit dem Ziel einer möglichst validen Beschreibung von Diagnose, Therapie und Verlauf ihrer oder seiner Krebserkrankung zusammengeführt werden.
Neben den klinischen Daten enthält der Datensatz Angaben zu denjenigen meldepflichtigen Personen, aus deren Meldungen die Angaben jeweils entnommen wurden.

#### Artikel 4  
Beleihung

(1) Die Durchführung der Aufgaben nach diesem Staatsvertrag wird von den Ländern Berlin und Brandenburg im Wege der Beleihung der „Klinisches Krebsregister für Brandenburg und Berlin GmbH“ („GmbH“) mit deren Einverständnis übertragen.
Die GmbH ist als Zweckbetrieb nach § 65 der Abgabenordnung in der Fassung der Bekanntmachung vom 1.
Oktober 2002 (BGBl.
I S. 3866; 2003 I S. 61), die zuletzt durch Artikel 5 des Gesetzes vom 3.
Dezember 2015 (BGBl.
I S. 2178, 2181) geändert worden ist, in der jeweils geltenden Fassung auszugestalten und zu führen und hat ausschließlich steuerbegünstigte Zwecke nach dem Dritten Abschnitt des Zweiten Teiles der Abgabenordnung zu verfolgen.
Die GmbH ist nicht befugt, Aufgaben nach Artikel 6 ganz oder teilweise Dritten zu übertragen.

(2) Alleingesellschafterin der GmbH ist die Landesärztekammer Brandenburg.
Diese unterliegt bezüglich ihrer Stellung als Gesellschafterin der GmbH der Fachaufsicht gemäß Artikel 9.
Die Landesärztekammer Brandenburg ist unbeschadet der Regelungen des Artikels 38 Absatz 3 nicht befugt, ihre Anteile an der GmbH ganz oder teilweise an Dritte zu veräußern oder zu verpfänden oder Dritte mit der Ausübung ihrer Stimmrechte zu bevollmächtigen.

#### Artikel 5  
Finanzierung

(1) Die vertragschließenden Länder tragen die nicht aus Einnahmen nach § 65c Absatz 4 und 6 des Fünften Buches Sozialgesetzbuch sowie sonstigen Einnahmen gedeckten notwendigen Betriebskosten, die der GmbH unter Beachtung der Grundsätze der Wirtschaftlichkeit und Sparsamkeit durch die Erfüllung der Aufgaben nach Artikel 6 entstehen.
Betriebskosten, die durch Umsetzung einer Weisung oder mit Zustimmung der Fachaufsicht entstehen, gelten stets als notwendig im Sinne von Satz 1.

(2) Für das Jahr 2016 erhält die GmbH von den vertragschließenden Ländern innerhalb von 20 Tagen nach Inkrafttreten dieses Staatsvertrages einen Abschlag in Höhe von 13,89 Euro für jede bis zum 31. Dezember 2016 zu erwartende und nach § 65c Absatz 4 Satz 2 bis 4 des Fünften Buches Sozialgesetzbuch vergütungsfähige Meldung zu einem Neuerkrankungsfall sowie vom Land Brandenburg die notwendigen Finanzmittel für die nach § 65c Absatz 1 Satz 4 des Fünften Buches Sozialgesetzbuch vorzunehmende landesbezogene Auswertung der Daten des Jahres 2015 der von den Nachsorgeleitstellen der onkologischen Schwerpunktkrankenhäuser in Neuruppin, Schwedt, Frankfurt (Oder), Cottbus und Potsdam im Auftrag des Landes Brandenburg tatsächlich verarbeiteten vergütungsfähigen Meldungen zu Neuerkrankungsfällen.
Die Zahl der zu erwartenden Meldungen zu Neuerkrankungsfällen ermittelt sich bezogen auf das jeweilige Einzugsgebiet nach Artikel 1 Absatz 2 für das Land Berlin anhand der aktuellsten Schätzung der jährlichen Krebsneuerkrankungszahlen des Zentrums für Krebsregisterdaten nach § 2 Nummer 3 Buchstabe a Bundeskrebsregisterdatengesetz vom 10.
August 2009 (BGBl.
I S. 2702, 2707) in der jeweils geltenden Fassung und für das Land Brandenburg nach der Anzahl der im Jahr 2015 von den Nachsorgeleitstellen der onkologischen Schwerpunktkrankenhäuser in Neuruppin, Schwedt, Frankfurt (Oder), Cottbus und Potsdam im Auftrag des Landes Brandenburg tatsächlich verarbeiteten vergütungsfähigen Meldungen zu Neuerkrankungsfällen.
Der Ausgleich etwaiger Über- oder Unterzahlungen infolge des Abweichens der prognostizierten von den tatsächlichen Betriebskosten erfolgt auf der Basis des geprüften Jahresabschlusses für das Geschäftsjahr 2016 mit den ergänzenden Zuschüssen des Folgejahres.
Sofern der Jahresabschluss bis zum 1.
Juni 2017 noch nicht in geprüfter Form vorliegt, sind an dessen Stelle die ungeprüften Ist-Zahlen vorzulegen.

(3) Ab dem Jahr 2017 gewähren die vertragschließenden Länder ihre Kostenbeteiligung in Form ergänzender Zuschüsse zu den notwendigen Betriebskosten, die jeweils zum 15.
Januar und zum 15. Juni eines jeden Jahres fällig sind.
Die ergänzenden Zuschüsse sind so zu bemessen, dass sie den für das folgende Halbjahr sowie den zu erwartenden und voraussichtlich nicht durch Einnahmen gedeckten notwendigen Betriebskosten entsprechen.
Die zu erwartenden und voraussichtlich nicht durch Einnahmen gedeckten notwendigen Betriebskosten werden dabei anhand der im selben Zeitraum des Vorjahres verarbeiteten und nach § 65c Absatz 4 Satz 2 bis 4 des Fünften Buches Sozialgesetzbuch vergüteten Meldungen zu Neuerkrankungsfällen ermittelt.
Der Ausgleich etwaiger Über- oder Unterzahlungen infolge des Abweichens der prognostizierten von den tatsächlichen Betriebskosten erfolgt auf der Basis des geprüften Jahresabschlusses und des Lageberichts mit der zweiten Rate des jeweiligen Folgejahres.
Sofern Jahresabschluss und Lagebericht bis zum 1.
Juni des jeweiligen Folgejahres noch nicht in geprüfter Form vorliegen, sind an deren Stelle die ungeprüften Ist-Zahlen vorzulegen.

(4) Die vertragschließenden Länder tragen von den ergänzenden Zuschüssen:

1.  die auf Tumorpatientinnen und -patienten mit Hauptwohnsitz in ihrem Land entsprechend § 65c Absatz 4 Satz 6 des Fünften Buches Sozialgesetzbuch entfallenden Anteile jeweils selbst,
2.  die auf Tumorpatientinnen und -patienten mit Hauptwohnsitz im Einzugsgebiet eines anderen klinischen Krebsregisters entsprechend § 65c Absatz 4 Satz 6 des Fünften Buches Sozialgesetzbuch entfallenden Anteile jeweils nach dem Sitz der meldepflichtigen Person oder der Meldestelle,
3.  die Aufwendungen für Auswertungen auf Landesebene nach § 65c Absatz 1 Satz 4 des Fünften Buches Sozialgesetzbuch jeweils zur Hälfte.

(5) Kosten der Erst- und Wiederbeschaffung sowie Ergänzung von Anlagegütern tragen, sofern die Anschaffung mit ihnen einvernehmlich abgestimmt wurde, die vertragschließenden Länder.
Soweit nicht durch Verwaltungsvereinbarung eine gesonderte Quote vereinbart wird, tragen die vertragschließenden Länder

1.  die der Koordinierungsstelle eindeutig zuzuordnenden Investitionskosten hälftig und
2.  die einer dezentralen Registerstelle eindeutig zuzuordnenden Investitionskosten nach deren Standort.

#### Artikel 6  
Aufgaben des klinischen Krebsregisters

(1) Das klinische Krebsregister nimmt für die Länder Berlin und Brandenburg die in diesem Artikel genannten Aufgaben wahr.
Bei der Aufgabenerledigung sind die vom Spitzenverband Bund der Krankenkassen auf der Grundlage des § 65c Absatz 2 des Fünften Buches Sozialgesetzbuch beschlossenen Kriterien zur Förderung klinischer Krebsregister vom 20.
Dezember 2013 zu beachten.

(2) Die Koordinierungsstelle hat folgende Aufgaben:

1.  Administration des klinischen Krebsregisters und Vertretung nach außen,
2.  Bearbeitung von Rechtsangelegenheiten einschließlich Datenschutzangelegenheiten,
3.  Verfolgung von Ordnungswidrigkeiten im Sinne des Artikels 35 Absatz 2 Nummer 1 und 2,
4.  Administration der Informationstechnik,
5.  Berufung der Mitglieder und Wahrnehmung der Aufgabe der Geschäftsstelle des Beirats nach Artikel 7,
6.  Initiierung, Unterstützung und Koordination der gemeinsamen Qualitätskonferenz unter der Schirmherrschaft beider Länder nach Artikel 8,
7.  Entscheidung über Anträge zu Vorhaben der Versorgungsforschung nach Artikel 32,
8.  Abschluss von Kooperationsvereinbarungen zur Zusammenarbeit mit Zentren der Onkologie für Aufgaben, die über tumorbezogene Auswertungen nach Absatz 4 Nummer 1 und die patientenbezogene Datenabfrage nach Artikel 31 hinausgehen sowie
9.  Abschluss von Vereinbarungen bei einer über die patientenbezogene Datenabfrage nach Artikel 31 hinausgehenden Begleitung von interdisziplinären und gegebenenfalls sektorenübergreifenden Tumorkonferenzen, sofern die Einwilligung der Patientin oder des Patienten hierfür vorliegt.

Werden im Rahmen von Vereinbarungen nach Satz 1 Nummer 8 oder Nummer 9 über die in Absatz 4 Nummer 1 und Absatz 5 genannten hinausgehende Leistungen vereinbart, ist dem klinischen Krebsregister der damit verbundene zusätzliche Aufwand zu erstatten.

(3) Die Koordinierungsstelle fungiert darüber hinaus als Landesauswertungsstelle mit folgenden Aufgaben:

1.  jährliche landesbezogene Auswertung und Veröffentlichung des Berichts nach § 65c Absatz 1 Satz 4 des Fünften Buches Sozialgesetzbuch,
2.  Übermittlung von Daten auf Anforderung an den Gemeinsamen Bundesausschuss für bundesweite Auswertungen nach § 65c Absatz 7 des Fünften Buches Sozialgesetzbuch und
3.  Zulieferung der Auswertungen, die zur Erstellung des vom Spitzenverband Bund der Krankenkassen ab dem Jahr 2018 alle fünf Jahre zu veröffentlichenden Berichts über die bundesweiten Ergebnisse der klinischen Krebsregistrierung nach § 65c Absatz 10 Satz 3 des Fünften Buches Sozialgesetzbuch erforderlich sind.

Der Bericht nach Satz 1 Nummer 1 enthält Aussagen, jeweils nach den Einzugsgebieten nach Artikel 1 Absatz 2 getrennt,

1.  zu den Leistungsdaten des klinischen Krebsregisters des Vorjahres in aggregierter Form wie insbesondere Anzahl der Erstmeldungen, Folgemeldungen, Anzahl der wohnortbezogenen Meldungen und der behandlungsortbezogenen Meldungen sowie Angaben über die Bevölkerung im Einzugsgebiet und
2.  zur landesbezogenen aggregierten Auswertung aller für einen Tumor relevanten Daten und Qualitätsindikatoren, sowohl behandlungsort- als auch wohnortbezogen.

Bei der Auswertung nach Satz 2 Nummer 2 sind bundeseinheitliche Rechenregeln anzuwenden, die darzulegen sind.
Der Bericht ist vor der Veröffentlichung den obersten Landesgesundheitsbehörden der Länder Berlin und Brandenburg vorzulegen.

(4) Der Auswertungsbereich hat folgende Aufgaben:

1.  regelmäßige tumorbezogene Auswertung erfasster klinischer Daten und Rückmeldung der Ergebnisse an die einzelnen meldepflichtigen Personen oder Meldestellen in Form aggregierter tumorspezifischer Auswertungen (§ 65c Absatz 1 Satz 2 Nummer 2 des Fünften Buches Sozialgesetzbuch),
2.  Bereitstellung von tumorspezifischen Analysen nach Maßgabe des Artikels 8 Absatz 2 Satz 1,
3.  Übermittlung von Daten für Zwecke der Versorgungsforschung nach Artikel 32 Absatz 1,
4.  Übermittlung von Daten für die Zwecke der Krankenhausplanung nach Artikel 33,
5.  jährliche Übermittlung des Gesamtdatensatzes an die Landesauswertungsstelle nach Artikel 25 Absatz 1 Nummer 9,
6.  Beteiligung an der einrichtungs- und sektorenübergreifenden Qualitätssicherung des Gemeinsamen Bundesausschusses nach § 65c Absatz 8 des Fünften Buches Sozialgesetzbuch, soweit hierfür patientenidentifizierende Daten nicht erforderlich sind,
7.  Zusammenarbeit mit Zentren der Onkologie (§ 65c Absatz 1 Satz 2 Nummer 6 des Fünften Buches Sozialgesetzbuch) entsprechend den Kooperationsvereinbarungen nach Absatz 2 Satz 1 Nummer 8, soweit hierfür patientenbezogene Daten nicht erforderlich sind,
8.  Entwicklung von Dokumentationsstandards und Durchführung von Schulungen der Beschäftigten des Versorgungsbereiches und
9.  Analysen der Haupteinflussfaktoren des Behandlungserfolges selbst oder in Kooperation durchzuführen und die Ergebnisse den meldepflichtigen Personen und Meldestellen zur Verfügung zu stellen.

(5) Der Versorgungsbereich hat folgende Aufgaben:

1.  Erfassung und Vollständigkeits- und Plausibilitätsprüfung der gemeldeten Daten, Klärung von unvollständigen oder nicht plausiblen Daten mit den meldepflichtigen Personen oder den Meldestellen,
2.  langfristige Speicherung von Identitätsdaten der Patientinnen und Patienten,
3.  Übermittlung von pseudonymisierten klinischen Daten und von meldendenbezogenen Angaben an den Auswertungsbereich,
4.  gesonderte Speicherung von Daten nach Artikel 16 Absatz 1 Satz 1 im Falle eines Widerspruchs nach Artikel 15,
5.  Auskunftserteilung nach Artikel 17,
6.  Löschung und Sperrung von Daten nach Maßgabe des Artikels 18,
7.  regelmäßige Übermittlung der Identitätsdaten und der epidemiologischen Daten an das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen nach Artikel 29 Absatz 1,
8.  Übernahme der vom Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen nach Artikel 29 Absatz 2 Satz 3 aus dem Abgleich mit den Daten der Melderegister übermittelten Daten in den eigenen Datenbestand,
9.  Übernahme der vom Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen Daten der Leichenschauscheine nach Artikel 29 Absatz 2 und 3 in den eigenen Datenbestand,
10.  Übermittlung von Daten zu der letzten behandelnden Ärztin oder Zahnärztin oder zu dem letzten behandelnden Arzt oder Zahnarzt nach Artikel 29 Absatz 4,
11.  Datenaustausch mit anderen klinischen Krebsregistern bei Abweichung von Wohn- und Behandlungsort der Patientinnen und Patienten nach Artikel 30,
12.  Übermittlung des Best-of-Datensatzes an meldepflichtige Personen oder Meldestellen nach Artikel 31,
13.  Bereitstellung von personenidentifizierenden Daten für Vorhaben der Versorgungsforschung nach Artikel 32 Absatz 2 und Einholung von Einwilligungen nach Artikel 32 Absatz 4 Satz 4,
14.  Ermittlung von Ordnungswidrigkeiten im Sinne des Artikels 35 Absatz 2 Nummer 1 und 2,
15.  Abrechnung der Registerpauschale und der Meldevergütungen mit den Kostenträgern und den meldepflichtigen Personen oder den Meldestellen nach den Artikeln 19 bis 23,
16.  Zusammenarbeit mit Zentren der Onkologie (§ 65c Absatz 1 Satz 2 Nummer 6 des Fünften Buches Sozialgesetzbuch) entsprechend den Kooperationsvereinbarungen nach Absatz 2 Satz 1 Nummer 8, soweit hierfür patientenbezogene Daten erforderlich sind,
17.  Initiierung und Begleitung von interdisziplinären und gegebenenfalls sektorenübergreifenden Tumorkonferenzen (§ 65c Absatz 1 Satz 2 Nummer 4 des Fünften Buches Sozialgesetzbuch) entsprechend den Vereinbarungen nach Absatz 2 Satz 1 Nummer 9 ,
18.  Durchführung der Pseudonymisierung für Beteiligung an der einrichtungs- und sektorenübergreifenden Qualitätssicherung des Gemeinsamen Bundesausschusses (§ 65c Absatz 8 des Fünften Buches Sozialgesetzbuch), in Erfüllung der Aufgabe der Vertrauensstelle im Sinne des § 299 des Fünften Buches Sozialgesetzbuch,
19.  regelmäßige Durchführung der Kontrollen zur Vollständigkeit der Registrierungen sowie
20.  regelmäßige Durchführung der Kontrollen zur Vollzähligkeit der Registrierungen unter Heranziehung der dazu vom Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen bereitgestellten Zahlen sowie den Abgleich dieser Zahlen mit der Zahl der gesondert gespeicherten Widersprüche nach Artikel 16 Absatz 1 Satz 1.

#### Artikel 7  
Wissenschaftlicher Beirat

(1) Dem klinischen Krebsregister wird zur wissenschaftlichen und fachlichen Beratung und Begleitung ein Beirat zur Seite gestellt.
Der Beirat gibt Empfehlungen ab, insbesondere

1.  zu Fragen des Datennutzungs- und Datenschutzkonzeptes,
2.  zur Förderung des Zusammenwirkens aller mit der Sicherung der Qualität und Weiterentwicklung der onkologischen Versorgung befassten Akteurinnen und Akteure,
3.  zur Schaffung und Aufrechterhaltung von Akzeptanz für die flächendeckende klinische Krebsregistrierung und
4.  zur Bereitstellung von Krebsregisterdaten in Zusammenhang mit Anträgen zur Versorgungsforschung nach Artikel 32 im Rahmen eines Datennutzungskonzepts.

Darüber hinaus soll der Beirat das klinische Krebsregister bei der Öffentlichkeitsarbeit unterstützen.

(2) Der Beirat besteht aus Vertretungen

1.  der Krankenhausgesellschaften,
2.  der Kassenärztlichen Vereinigungen,
3.  der Ärztekammern,
4.  der Zahnärztekammern,
5.  der Dachverbände der Tumorzentren,
6.  der Landesverbände der Krankenkassen und der Ersatzkassen,
7.  der Landesausschüsse des Verbandes der privaten Krankenversicherung e.V.
8.  der medizinischen Fakultäten und
9.  der obersten Landesgesundheitsbehörden

jeweils beider Länder.
Die in Satz 1 genannten Stellen schlagen dem klinischen Krebsregister jeweils ein Mitglied und zwei Stellvertretungen vor.
Das klinische Krebsregister beruft diese im Einvernehmen mit der Landesärztekammer Brandenburg und den vertragschließenden Ländern für die Dauer von vier Jahren.
Bei der Besetzung des Beirates sollen weibliche und männliche Personen gleichermaßen berücksichtigt werden.
Eine wiederholte Berufung ist zulässig.
Scheidet ein Mitglied oder eine Stellvertretung während der Amtsperiode aus, wird für die restliche Dauer der Amtsperiode eine Nachfolge berufen.
Die Mitglieder des Beirates und ihre Stellvertretungen dürfen in keinem Beschäftigungs- oder sonstigen Dienstleistungsverhältnis zur GmbH stehen.
Sie sind im Verhältnis zur GmbH fachlich und persönlich unabhängig.
Mitglieder des Beirats wirken nicht an Empfehlungen nach Absatz 1 Satz 2 Nummer 4 zu Anträgen mit, zu denen sie direkt oder indirekt in Beziehung stehen.
Auslagen der Mitglieder des Beirates und ihrer Stellvertretungen sowie ihrer Dienstherren oder Auftraggeber werden von der GmbH nicht erstattet.

(3) Auf Vorschlag der in Absatz 2 Satz 1 genannten Stellen können außerdem Vertretungen von anderen Stellen und Organisationen berufen werden.
Dabei sollen vorzugsweise Vertretungen solcher Stellen und Organisationen berufen werden, die sich in den vertragschließenden Ländern maßgeblich für die Wahrnehmung der Interessen der Patientinnen und Patienten mit onkologischen Erkrankungen einsetzen.
Absatz 2 Satz 2 bis 5 gilt entsprechend mit der Maßgabe, dass auf Antrag Reisekosten nach dem Bundesreisekostengesetz und der im Land Brandenburg hierzu geltenden Vorschriften von der GmbH erstattet werden.

(4) Der Beirat gibt sich eine Geschäftsordnung, in der insbesondere zu regeln ist:

1.  die Aufgaben der beim klinischen Krebsregister einzurichtenden Geschäftsstelle,
2.  das Verfahren zur Bestimmung der oder des Vorsitzenden,
3.  die Aufgaben der oder des Vorsitzenden,
4.  das Verfahren zur Beschlussfassung und
5.  die Hinzuziehung von Sachverständigen und Gästen.

Die Geschäftsordnung ist von der Aufsichtsbehörde über das klinische Krebsregister zu genehmigen.

#### Artikel 8  
Regionale Qualitätskonferenzen und Gemeinsame Qualitätskonferenz

(1) Unter Schirmherrschaft beider Länder wird bei der Koordinierungsstelle zur Initiierung, Unterstützung und Koordination einrichtungsinterner und einrichtungsübergreifender regionaler Qualitätszirkel oder interdisziplinärer Arbeitsgruppen (regionale Qualitätskonferenzen) eine gemeinsame Qualitätskonferenz eingerichtet.
Regionale Qualitätskonferenzen können sich der gemeinsamen Qualitätskonferenz als Unterarbeitsgruppen zuordnen lassen und hierbei auch neue regionale und tumorspezifische Unterarbeitsgruppen bilden.
Die gemeinsame Qualitätskonferenz stellt einmal jährlich der Fachöffentlichkeit und interessierten Patientenvertretungen die aktuellen landesbezogenen Auswertungen nach Artikel 6 Absatz 3 Satz 1 Nummer 1 vor und dient dabei der Entwicklung von Lösungsansätzen für aktuelle Fragen der onkologischen Versorgung.

(2) Die regionalen Qualitätskonferenzen führen regelmäßig mithilfe der ihnen vom Auswertungsbereich nach Artikel 6 Absatz 4 Nummer 2 auf Anfrage bereitgestellten Auswertungen tumorspezifische Analysen und Maßnahmen zur regionalen und einrichtungsbezogenen Versorgungsqualität durch und fördern die interdisziplinäre sektorübergreifende Zusammenarbeit.
Fordern regionale Qualitätskonferenzen dabei auch Auswertungen von nicht an ihnen teilnehmenden meldepflichtigen Personen oder Meldestellen an, bedarf die Bereitstellung dieser Daten der Einwilligung der betreffenden meldepflichtigen Personen oder Meldestellen.
Die regionalen Qualitätskonferenzen übermitteln ihre Ergebnisse und Feststellungen mindestens einmal jährlich an die nach Artikel 6 Absatz 3 zur Koordinierungsstelle gehörenden Landesauswertungsstelle.

(3) Die Landesauswertungsstelle führt die übermittelten Ergebnisse und Feststellungen der regionalen Qualitätskonferenzen zusammen und prüft, ob diese in die jährliche landesbezogene Auswertung nach Artikel 6 Absatz 3 Satz 1 Nummer 1 aufzunehmen sind.

### Abschnitt 2  
Aufsicht und Prüfrechte

#### Artikel 9  
Aufsicht

(1) Die GmbH und diejenigen Beschäftigten, die eine oder mehrere Aufgaben nach Absatz 3 Satz 1 wahrnehmen, unterliegen der Fachaufsicht der obersten Landesgesundheitsbehörde des Landes Brandenburg (Aufsichtsbehörde).
Die Aufsicht ist im Einvernehmen mit der obersten Landesgesundheitsbehörde des Landes Berlin auszuüben.

(2) Änderungen des Gesellschaftsvertrages der GmbH nach § 53 des Gesetzes betreffend die Gesellschaften mit beschränkter Haftung und die Ausübung der Bestimmungsrechte der Gesellschafterin nach § 46 Nummer 1 und 5 bis 8 des Gesetzes betreffend die Gesellschaften mit beschränkter Haftung bedürfen der vorherigen Zustimmung der Aufsichtsbehörde.

(3) Die Anstellung von Personen bei der GmbH, denen eine oder mehrere der folgenden Aufgaben übertragen werden sollen, bedarf der vorherigen Zustimmung und ihrer Bestellung durch die Aufsichtsbehörde:

1.  Geschäftsführung,
2.  Prokuristin oder Prokurist,
3.  Verfolgung und Ahndung von Ordnungswidrigkeiten,
4.  Behördliche Beauftragte oder Behördlicher Beauftragter für den Datenschutz,
5.  Leitung der Administration der Informationstechnik und
6.  Leitungen der dezentralen Registerstellen.

Satz 1 gilt für die Übertragung einer oder mehrerer der vorgenannten Aufgaben auf bereits bei der GmbH beschäftigte Personen entsprechend.
Zustimmungen und Bestellungen nach Satz 1 oder Satz 2 dürfen nur erfolgen, wenn die betreffenden Personen die erforderliche Sachkunde für die von ihr wahrzunehmenden Aufgaben nachweislich besitzen und keine Anhaltspunkte dafür bestehen, dass ihnen die erforderliche Zuverlässigkeit fehlt; die Bestellung erfolgt widerruflich.
Die Aufsichtsbehörde kann allgemeine Anforderungen an die Auswahl der Beschäftigten der GmbH festlegen.
Entgeltliche Nebentätigkeiten von nach Satz 1 oder Satz 2 bestellten Personen bedürfen der Zustimmung durch die Aufsichtsbehörde.

(4) Die Aufsichtsbehörde kann der GmbH allgemeine Weisungen zur Erfüllung der Aufgaben nach Artikel 6 erteilen.
Im Einzelfall können Weisungen erteilt werden, wenn die Aufgaben nach Artikel 6 nicht im Einklang mit den Gesetzen oder den vom Spitzenverband Bund der Krankenkassen auf der Grundlage des § 65c Absatz 2 des Fünften Buches Sozialgesetzbuch beschlossenen Kriterien zur Förderung klinischer Krebsregister vom 20.
Dezember 2013 wahrgenommen oder erteilte allgemeine Weisungen nicht befolgt werden.
Die GmbH ist verpflichtet, der Aufsichtsbehörde jederzeit Auskunft zu erteilen und Einsicht in Akten und sonstige Schriftstücke zu gewähren.
Werden Akten und sonstige Schriftstücke ausschließlich elektronisch geführt, erfolgt die Akteneinsicht durch Erteilung eines Aktenausdrucks, Darstellung auf dem Bildschirm oder durch Übermittlung eines elektronischen Dokuments.
Eine Offenbarung von Identitätsdaten im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 4 ist nicht zulässig.

(5) Kommt die GmbH einer Weisung der Aufsichtsbehörde nicht fristgemäß nach, kann die Aufsichtsbehörde anstelle und auf Kosten der GmbH tätig werden oder Dritte tätig werden lassen.
Sie kann das Selbsteintrittsrecht auch durch Weisungen gegenüber den Beschäftigten der GmbH ausüben.

#### Artikel 10  
Prüfrecht der Rechnungshöfe und Finanzkontrolle

(1) Die Rechnungshöfe der vertragschließenden Länder sind berechtigt, die Haushalts- und Wirtschaftsführung der GmbH im Rahmen der Durchführung dieses Staatsvertrages zu prüfen.
Eine Offenbarung patientenidentifizierender Daten gemäß Artikel 3 Absatz 1 Nummer 1 bis 4 ist nicht zulässig.
Sie sollen Prüfvereinbarungen auf der Grundlage von § 93 der Landeshaushaltsordnung des Landes Berlin und von § 93 der Landeshaushaltsordnung des Landes Brandenburg treffen.
Die Alleingesellschafterin hat für die Aufnahme entsprechender Vorschriften in den Gesellschaftsvertrag oder die Satzung der GmbH zu sorgen.

(2) Die Rechnungshöfe können eine Wirtschaftsprüfungsgesellschaft mit der Prüfung der Haushalts- und Wirtschaftsführung sowie mit der Rechnungslegung zu Lasten der GmbH beauftragen.
Die Prüfung nach § 317 des Handelsgesetzbuches und der Inhalt des Prüfberichts nach § 321 des Handelsgesetzbuches müssen auch die Prüfinhalte nach § 53 Absatz 1 Nummer 1 und 2 des Haushaltsgrundsätzegesetzes umfassen.
Eine Offenbarung patientenidentifizierender Daten gemäß Artikel 3 Absatz 1 Nummer 1 bis 4 ist nicht zulässig.

### Abschnitt 3  
Meldungen

#### Artikel 11  
Meldepflichten

(1) In den Ländern Berlin oder Brandenburg tätige Ärztinnen, Ärzte, Zahnärztinnen und Zahnärzte (meldepflichtige Personen) sind abweichend von § 3 Absatz 1 Satz 1 des Krebsregistergesetzes verpflichtet, die in Artikel 3 Absatz 1 bis 6 genannten oder in Bezug genommenen Angaben zu ihnen und den von ihnen behandelten Patientinnen und Patienten mit Hauptwohnsitz in der Bundesrepublik Deutschland, bei denen sie Tumorerkrankungen im Sinne des Absatzes 4 diagnostizieren, behandeln oder nachsorgen, bei den in Artikel 12 genannten Meldeanlässen dem Versorgungsbereich zu übermitteln, soweit sie darüber verfügen.
Soweit der einheitliche Datensatz der Arbeitsgemeinschaft Deutscher Tumorzentren und der Gesellschaft der epidemiologischen Krebsregister in Deutschland und ihn ergänzende Module nach § 65c Absatz 1 Satz 3 des Fünften Buches Sozialgesetzbuch psychotherapeutische Behandlungsmaßnahmen vorsehen, besteht die Meldepflicht nach Satz 1 auch für Psychologische Psychotherapeutinnen und Psychologische Psychotherapeuten.

(2) Die Übermittlung hat innerhalb von vier Wochen nach dem Eintritt des jeweiligen Meldeanlasses zu erfolgen.

(3) Die meldepflichtigen Personen sind von ihrer Verschwiegenheitspflicht befreit, soweit dies zur Erfüllung der Meldepflicht nach Absatz 1 erforderlich ist.
Unter der Voraussetzung des Absatzes 1 Satz 2 gilt dies auch für Psychologische Psychotherapeutinnen und Psychologische Psychotherapeuten.

(4) Meldepflichtig sind bösartige Neubildungen einschließlich ihrer Frühstadien und gutartige Tumoren des zentralen Nervensystems nach Kapitel II der Internationalen statistischen Klassifikation der Krankheiten und verwandter Gesundheitsprobleme (ICD) mit Ausnahme von Erkrankungsfällen, die an das Deutsche Kinderkrebsregister zu melden sind, und nicht-melanotischer Hautkrebsarten und ihrer Frühstadien.
Abweichend von § 3 Absatz 1 Satz 1 des Krebsregistergesetzes besteht eine Meldepflicht ausschließlich gegenüber dem klinischen Krebsregister.
Für Erkrankungsfälle, die an das Deutsche Kinderkrebsregister zu melden sind, und für nicht-melanotische Hautkrebsarten und ihre Frühstadien besteht abweichend von § 3 Absatz 1 Satz 1 des Krebsregistergesetzes die Meldepflicht zum Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen.

(5) Zwei oder mehr gemeinsam tätige meldepflichtige Personen und die ärztliche Leitung einer Stelle, bei der meldepflichtige Personen angestellt sind, bilden Meldestellen.
Die Meldestellen haben sicherzustellen, dass bei jedem Meldeanlass eine Meldung erfolgt.

(6) Meldepflichtige Personen und Meldestellen können einzelne einrichtungsbezogene Krebsregister mit Sitz im Land Berlin oder im Land Brandenburg mit der Meldung betrauen und die erforderlichen Patientendaten diesen gegenüber offenbaren, wenn eine fristgerechte Meldung und die Einhaltung der datenschutzrechtlichen Vorschriften gewährleistet ist.
In einer solchen Meldung sind der Name und die Anschrift der meldepflichtigen Person oder der Meldestelle, für die die Meldung erfolgt, anzugeben.

(7) Meldepflichtige Personen und Meldestellen können die GmbH mit der Durchführung von Meldungen nach Absatz 1 Satz 1 beauftragen, wenn die meldepflichtige Person oder die Meldestelle technisch und organisatorisch sicherstellt und nachweist, dass der Zugriff auf die für die klinische Krebsregistrierung erforderlichen Daten beschränkt ist.

#### Artikel 12  
Meldeanlässe

Meldeanlässe sind:

1.  die Diagnose einer Tumorerkrankung,
2.  die histologische, zytologische oder labortechnische Sicherung der Diagnose,
3.  der Beginn und der Abschluss einer therapeutischen Maßnahme (insbesondere Operation, Strahlentherapie, systemische Therapie),
4.  jede Änderung im Verlauf einer Tumorerkrankung, wie beispielsweise das Auftreten von Rezidiven, Metastasen, das Voranschreiten der Tumorerkrankung, teilweise oder vollständige Tumorremission und Nebenwirkungen, sowie
5.  der Tod der Patientin oder des Patienten.

#### Artikel 13  
Inhalt und Form der Meldungen

(1) Der Inhalt der nach Artikel 11 Absatz 1 Satz 1 zu meldenden Daten bestimmt sich unbeschadet des Artikels 11 Absatz 4 nach Artikel 3 Absatz 1 bis 6.
Die GmbH veröffentlicht die amtliche Fundstelle der nach Artikel 3 Absatz 3 Nummer 2 in Verbindung mit § 65c Absatz 1 Satz 3 des Fünften Buches Sozialgesetzbuch geltenden Datensatzes in geeigneter Form.
Zusätzlich muss jede Meldung die Angabe enthalten, ob die Informationspflichten nach Artikel 14 erfüllt wurden oder, falls dies nicht erfolgt ist, den Grund hierfür.
Die Regelungen zu den im Falle einer Meldung durch eine diagnostizierende Einrichtung ohne Patientenkontakt zu übermittelnden Daten nach Absatz 3 oder zu den bei einer Ausübung des Widerspruchsrechts durch eine Patientin oder einen Patienten zu übermittelnden Daten nach Absatz 4 bleiben unberührt.

(2) Die Meldungen sind, soweit die Sätze 4 und 6 nichts anderes bestimmen, in strukturierter elektronischer Form an den Versorgungsbereich unter Verwendung der vom klinischen Krebsregister veröffentlichten Meldeformulare oder anderer von ihm vorgegebener elektronischer Formate zu übermitteln.
Dabei sind technische und organisatorische Maßnahmen nach den jeweils geltenden Sicherheitsstandards vorzunehmen, die geeignet und erforderlich sind, den Zugriff unberechtigter Dritter auf die Daten während ihrer Übertragung oder ihrer Zwischenspeicherung auf Systemen, die für Übermittlung und Empfang der Meldungen verwendet werden, zu verhindern.
Die elektronische leitungsgebundene Meldung ist nur mit elektronischem Heilberufsausweis zulässig; dabei sind dessen Funktionen für die Authentifikation der übermittelnden Person und die Signatur der zu übermittelnden Daten einzusetzen.
Abweichend von Satz 1 können die Meldungen bis zum 31.
Dezember 2020 auch in anderer Form, insbesondere durch Übermittlung ärztlicher Befundberichte oder mit maschinell verwertbaren Datenträgern, erfolgen.
Dabei ist sicherzustellen, dass nur die für die klinische Krebsregistrierung erforderlichen Daten übermittelt werden.
Zur Vermeidung unbilliger Härten kann das klinische Krebsregister auf Antrag auch nach dem 31.
Dezember 2020 Ausnahmen zulassen.

(3) Im Falle einer Meldung durch eine diagnostizierende Einrichtung ohne Patientenkontakt sind mit der Meldung auch Angaben nach Artikel 3 Absatz 5 zu der meldepflichtigen Person oder der Meldestelle, die das diagnostische Tätigwerden veranlasst hat, zu übermitteln.

(4) Legt eine Patientin oder ein Patient bei der meldepflichtigen Person oder der Meldestelle Widerspruch nach Artikel 15 Absatz 1 oder Absatz 2 ein, ist dem klinischen Krebsregister die Tatsache und die Art des Widerspruchs zu übermitteln.
Die im Falle eines Widerspruchs nach Artikel 15 Absatz 1 zu übermittelnden Daten bestimmen sich nach Artikel 16 Absatz 1 Satz 1 und Absatz 5.

#### Artikel 14  
Informationspflichten der meldepflichtigen Personen und der Meldestellen gegenüber Patientinnen und Patienten

(1) Meldepflichtige Personen und Meldestellen sind verpflichtet, die wegen einer Tumorerkrankung von ihnen behandelten Patientinnen und Patienten vor der ersten Übermittlung ihrer Daten über die beabsichtigte Meldung, die Kategorien der hierbei verarbeiteten Daten, den Zweck der Meldung, die Aufgaben des klinischen Krebsregisters, den Meldeempfänger, die Weiterleitung von Daten an das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen und dessen Aufgaben sowie über die Rechte nach den Artikeln 15 bis 17 zu informieren.
Die meldepflichtigen Personen und die Meldestellen sind auch verpflichtet, die behandelten Patientinnen und Patienten unverzüglich über eine Information nach Absatz 4 zu informieren und die Information nach Satz 1 nachzuholen.
Von der Information nach Satz 1 vor einer Meldung darf nur in den Fällen der Absätze 3 und 4 abgesehen werden.
Die meldepflichtige Person oder die Meldestelle hat eine schriftliche Bestätigung des Erhalts und der Kenntnisnahme der Information nach den Sätzen 1 und 2 von der betreffenden Patientin oder dem betreffenden Patienten einzuholen und zur Patientenakte zu nehmen.
Die Informationspflichten der meldepflichtigen Personen und Meldestellen gelten auch gegenüber den gesetzlichen Vertreterinnen oder Vertretern oder Bevollmächtigten für die Gesundheitssorge der von ihnen wegen einer Tumorerkrankung behandelten Patientinnen und Patienten.

(2) Zur Information ist ein vom klinischen Krebsregister kostenlos in elektronischer Form in deutscher Sprache und in anderen Sprachen zur Verfügung gestelltes Informationsblatt für Patientinnen und Patienten zu verwenden.
Bei der Erstellung des Informationsblattes sind die oder der Landesbeauftragte für den Datenschutz und für das Recht auf Akteneinsicht des Landes Brandenburg und geeignete Patientenorganisationen zu beteiligen.

(3) Abweichend von Absatz 1 dürfen behandelnde meldepflichtige Personen und Meldestellen von der Information der von ihnen behandelten Patientin oder des von ihnen behandelten Patienten absehen, wenn ihre Information oder Aufklärung über das Vorliegen einer Krebserkrankung wegen der Gefahr einer anderenfalls eintretenden erheblichen Gesundheitsverschlechterung unterblieben ist.
Die Gründe für das Absehen von der Information sind aufzuzeichnen.
Wird die behandelte Patientin oder der behandelte Patient nach der Übermittlung ihrer Daten über das Vorliegen einer Krebserkrankung aufgeklärt, ist die Information nach Absatz 1 Satz 1 unverzüglich nachzuholen.

(4) Diagnostizierende meldepflichtige Personen oder Meldestellen ohne direkten Patientenkontakt haben die meldepflichtige Person oder die Meldestelle, die das diagnostische Tätigwerden veranlasst hat, über eine vorgenommene Meldung an das klinische Krebsregister zu informieren.
Die in den diagnostizierenden Einrichtungen ärztlich oder zahnärztlich tätigen Personen sind insoweit von ihrer Verschwiegenheitspflicht befreit.

### Abschnitt 4  
Rechte der Patientinnen und Patienten

#### Artikel 15  
Widerspruchsrecht

(1) Patientinnen und Patienten, ihre gesetzlichen Vertreterinnen und Vertreter sowie die von ihnen für die Gesundheitssorge bevollmächtigten Personen haben jederzeit ein Recht auf Widerspruch.
Der Widerspruch kann sich auf die Speicherung medizinischer Daten der Patientin oder des Patienten aus einzelnen oder aus allen Meldungen im klinischen Krebsregister beziehen.

(2) Patientinnen und Patienten mit Hauptwohnsitz im Land Brandenburg haben unabhängig vom Widerspruchsrecht nach Absatz 1 das Recht, nur der Übermittlung von epidemiologischen Daten nach Artikel 29 Absatz 1 durch das klinische Krebsregister an das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen zu widersprechen.
Patientinnen und Patienten mit Hauptwohnsitz im Land Berlin steht das Recht auf Widerspruch gegen die Übermittlung von epidemiologischen Daten nach Artikel 29 Absatz 1 durch das klinische Krebsregister an das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen abweichend von § 3 Absatz 2 Satz 2 des Krebsregistergesetzes nicht zu.

(3) Der Widerspruch kann bei der meldepflichtigen Person oder der Meldestelle oder bei dem Versorgungsbereich erhoben werden.

(4) Die Vorschriften zum Widerspruchsrecht sind bis zum 31.
Dezember 2019 zu evaluieren.

#### Artikel 16  
Widerspruch gegen die Speicherung medizinischer Daten im klinischen Krebsregister

(1) Im Fall eines Widerspruchs gegen die Speicherung medizinischer Daten aus einer Meldung oder aus allen Meldungen im klinischen Krebsregister nach Artikel 15 Absatz 1 sind lediglich die Daten der Patientin oder des Patienten im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 4 und die Tatsache und die Art des Widerspruchs im Versorgungsbereich gesondert zu speichern.
Die Daten im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 4 dürfen nur für Zwecke nach Absatz 4 verarbeitet werden.
Andere als die in Satz 1 genannten Daten und Angaben dürfen zu diesen Patientinnen und Patienten durch das klinische Krebsregister vorbehaltlich des Absatzes 5 nicht verarbeitet werden.
Hat die Patientin oder der Patient Widerspruch gegen die Speicherung medizinischer Daten aus allen Meldungen erhoben, sind auch die bereits zu dieser Person gespeicherten medizinischen Daten zu löschen, und die Daten im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 4 sind in den gesonderten Datenbestand nach Satz 1 zu überführen.

(2) Meldepflichtige Personen und Meldestellen, die Meldungen zu Patientinnen oder Patienten veranlassen, die einen Widerspruch gegen die Speicherung medizinischer Daten aus allen Meldungen erhoben haben, sind umgehend über den Widerspruch zu informieren.
Ein Vergütungsanspruch nach Artikel 23 besteht nicht für Meldungen, die nach Erteilung der Information nach Satz 1 bei dem klinischen Krebsregister eingehen.

(3) Die Daten im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 4 nehmen am regelmäßigen Abgleich mit den Melderegisterdaten und den Leichenschauscheinen nach Artikel 29 Absatz 2 und 3 teil.
Sie sind nach Bekanntwerden des Todes der Patientin oder des Patienten aufgrund des Leichenschauscheins unverzüglich zu löschen, soweit und solange sie nicht für die in Absatz 4 genannten Zwecke erforderlich sind.
Liegen die Daten dem klinischen Krebsregister allein aufgrund des Hauptwohnsitzes der Patientin oder des Patienten vor, sind sie bei einem Wechsel des Hauptwohnsitzes der Patientin oder des Patienten in das Einzugsgebiet eines anderen regionalen klinischen Krebsregisters nach Übermittlung an das für den neuen Hauptwohnsitz zuständige klinische Krebsregister nach Artikel 30 Absatz 1 Satz 3 unverzüglich zu löschen.

(4) Die Daten im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 4 dürfen vorbehaltlich des Absatzes 5 nur für folgende Zwecke verarbeitet werden:

1.  zur Durchsetzung des Widerspruchrechts nach Artikel 15 Absatz 1,
2.  zur regelmäßigen Prüfung der Vollständigkeit nach Artikel 24 Nummer 6,
3.  zur regelmäßigen Prüfung der Vollzähligkeit nach Artikel 24 Nummer 7,
4.  zur Verfolgung und Ahndung einer Straftat im Sinne des Artikels 34 oder einer Ordnungswidrigkeit im Sinne des Artikels 35 sowie
5.  zur Übermittlung im Rahmen des Datenaustausches mit einem anderen klinischen Krebsregister nach Artikel 30 Absatz 1 Satz 3.

(5) Für Patientinnen und Patienten mit Hauptwohnsitz im Land Berlin sind zusätzlich zu den in Absatz 1 Satz 1 genannten Daten und Angaben die Daten im Sinne des Artikels 3 Absatz 1 Nummer 5 und 6 sowie Absatz 2 Nummer 4 bis 13 zu melden, soweit sie der meldepflichtigen Person oder der Meldestelle vorliegen.
Die in Satz 1 genannten Angaben und Daten sind vom klinischen Krebsregister nach Artikel 29 Absatz 1 an das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen zu übermitteln.
Sie sind sechs Monate nach der Übermittlung zu löschen.

#### Artikel 17  
Auskunftsrecht

(1) Der Versorgungsbereich hat auf Antrag, der bei diesem schriftlich oder zur Niederschrift zu stellen ist, einer Patientin oder einem Patienten Auskunft über die im klinischen Krebsregister zu ihrer oder seiner Person gespeicherten Daten im Sinne des Artikels 3 Absatz 1 bis 4 gebührenfrei zu erteilen.
Auch über das Nichtvorliegen von Daten im Sinne des Artikels 3 Absatz 1 bis 4 ist Auskunft zu erteilen.
Vor der Auskunftserteilung ist die Identität zwischen der antragstellenden Person und der Person, über die Auskunft erteilt werden soll, vom Versorgungsbereich festzustellen.
Weichen die von der antragstellenden Person angegebenen Daten im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 4 von den im klinischen Krebsregister gespeicherten Daten ab, hat die antragstellende Person die dadurch entstehenden Zweifel an ihrer Auskunftsberechtigung durch Vorlage geeigneter Nachweise auszuräumen, bevor die Auskunft erteilt werden darf.
Der Antrag auf Erteilung einer Auskunft kann auch durch eine gesetzliche Vertreterin oder einen gesetzlichen Vertreter einer auskunftsberechtigten Person oder eine von dieser für die Gesundheitssorge bevollmächtigte Person (Vertreterin oder Vertreter) gestellt werden.
Vor der Auskunftserteilung hat die Vertreterin oder der Vertreter die Vertretungsbefugnis für die auskunftsberechtigte Person dem Versorgungsbereich durch Vorlage geeigneter Nachweise zu belegen.

(2) Die Auskunft nach Absatz 1 soll durch eine Ärztin oder Zahnärztin oder einen Arzt oder Zahnarzt erteilt werden, die oder der von der auskunftsberechtigten Person oder ihrer Vertreterin oder ihrem Vertreter gegenüber dem Versorgungsbereich schriftlich benannt worden ist.
Der Versorgungsbereich hat die zu der auskunftsberechtigten Person gespeicherten Daten der nach Satz 1 benannten Person schriftlich mitzuteilen.
Benennt die auskunftsberechtigte Person oder ihre Vertreterin oder ihr Vertreter keine Ärztin oder Zahnärztin und keinen Arzt oder Zahnarzt, hat der Versorgungsbereich die Auskunft unmittelbar der auskunftsberechtigten Person oder ihrer Vertreterin oder ihrem Vertreter schriftlich zu erteilen.

#### Artikel 18  
Löschung und Sperrung des Direktabrufs

(1) Die Daten im Sinne des Artikels 3 Absatz 6 Nummer 10 und 11 sind sechs Monate nach Abschluss des Abrechnungsverfahrens zu löschen, soweit und solange sie nicht im Einzelfall für ein durchzuführendes Ordnungswidrigkeitsverfahren nach Artikel 35 benötigt werden.
Das Abrechnungsverfahren umfasst die Erledigung von Beanstandungen durch die Kostenträger im Sinne des Artikels 19 Absatz 1 einschließlich ihrer nachträglichen Prüfrechte und die Zahlung der Meldevergütung an die meldepflichtige Person oder die Meldestelle durch das klinische Krebsregister nach Artikel 23.

(2) Die Daten im Sinne des Artikels 3 Absatz 1 Nummer 1, 3 und 4 sind zehn Jahre nach dem Tod oder spätestens 130 Jahre nach der Geburt der Patientin oder des Patienten zu löschen.

(3) Bei Daten, die im automatisierten Verfahren mit der Möglichkeit des Direktabrufs gespeichert sind, ist die Möglichkeit des Direktabrufs zu sperren, sobald dem klinischen Krebsregister im Wege des Leichenschauscheinabgleiches nach Artikel 29 Absatz 3 bekannt wird, dass die betreffende Patientin oder der betreffende Patient verstorben ist, soweit und solange die Daten nicht mehr für das Abrechnungsverfahren oder für ein durchzuführendes Strafverfahren nach Artikel 34 oder ein Ordnungswidrigkeitsverfahren nach Artikel 35 benötigt werden.

### Abschnitt 5  
Abrechnungsverfahren

#### Artikel 19  
Grundsätze der Abrechnung

(1) Der Versorgungsbereich führt die Abrechnung mit den Krankenkassen und den Ersatzkassen, den privaten Krankenversicherungen und denjenigen Trägern der Kosten in Krankheits-, Pflege- und Geburtsfällen nach beamtenrechtlichen Vorschriften, die sich an den Kosten der klinischen Krebsregistrierung nach § 65c des Fünften Buches Sozialgesetzbuch beteiligen (Kostenträger) sowie mit den meldepflichtigen Personen und den Meldestellen durch.

(2) Eine Abrechnungsbeanstandung durch Kostenträger hat das klinische Krebsregister zunächst anhand der bei ihm vorliegenden Daten zu prüfen.
Sofern durch die Prüfung nach Satz 1 eine Klärung der Beanstandung nicht herbeigeführt werden kann, ist das klinische Krebsregister berechtigt, die notwendigen Daten einschließlich der sicher verschlüsselten personenbezogenen Daten zur weiteren Prüfung und Klärung inhaltlicher Fragen im Rahmen der Abrechnung an die meldepflichtige Person oder die Meldestelle weiterzuleiten, deren Meldung der jeweiligen Abrechnung zugrunde liegt.

#### Artikel 20  
Abrechnung mit den Krankenkassen und den Ersatzkassen

(1) Der Versorgungsbereich darf zur Abrechnung der Registerpauschale nach § 65c Absatz 4 des Fünften Buches Sozialgesetzbuch und der Meldevergütung nach § 65c Absatz 6 des Fünften Buches Sozialgesetzbuch für Versicherte der Krankenkassen und der Ersatzkassen die dafür notwendigen Daten im Rahmen eines bundeseinheitlichen elektronischen Datenaustauschverfahrens verarbeiten.
Die für die Abrechnung erforderlichen Daten und die inhaltlichen und technischen Anforderungen des elektronischen Abrechnungsverfahrens richten sich nach der Technischen Anlage zur elektronischen Abrechnung der klinischen Krebsregister gemäß den Fördervoraussetzungen des Spitzenverbandes Bund der Krankenkassen nach § 65c Absatz 2 des Fünften Buches Sozialgesetzbuch in der jeweils aktuellen Fassung.

(2) Sofern und soweit von beiden vertragschließenden Ländern eine gemeinsame Vereinbarung mit den Landesverbänden der Krankenkassen und der Ersatzkassen nach § 65c Absatz 5 Satz 3 des Fünften Buches Sozialgesetzbuch geschlossen wurde, die von der Technischen Anlage im Sinne des Absatzes 1 Satz 2 abweichende Regelungen zur Abrechnung enthält, treten die Anforderungen der Vereinbarung an die der Technischen Anlage.

#### Artikel 21  
Abrechnung mit den privaten Krankenversicherungen

(1) Das klinische Krebsregister darf zur Abrechnung der Registerpauschale nach § 65c Absatz 4 des Fünften Buches Sozialgesetzbuch und der Meldevergütung nach § 65c Absatz 6 des Fünften Buches Sozialgesetzbuch für substitutiv privat krankenversicherte Patientinnen und Patienten die dafür notwendigen Daten im Rahmen eines bundeseinheitlichen Abrechnungsverfahrens verarbeiten.
Die für die Abrechnung erforderlichen Daten und die inhaltlichen und technischen Anforderungen des Abrechnungsverfahrens richten sich nach der von beiden vertragschließenden Ländern nach Maßgabe des Absatzes 2 abgeschlossenen bundeseinheitlichen Vereinbarung mit dem Verband der Privaten Krankenversicherung in der jeweils aktuellen Fassung.

(2) Die Vereinbarung nach Absatz 1 Satz 2 mit dem Verband der privaten Krankenversicherung für den Geltungsbereich dieses Staatsvertrages wird von den obersten Landesgesundheitsbehörden der Länder Berlin und Brandenburg abgeschlossen.
Die obersten Landesgesundheitsbehörden der Länder Berlin und Brandenburg können das klinische Krebsregister einvernehmlich zum Abschluss der Vereinbarung mit dem Verband der Privaten Krankenversicherung ermächtigen.

#### Artikel 22  
Abrechnung mit den Trägern der Kosten in Krankheits-, Pflege- und Geburtsfällen nach beamtenrechtlichen Vorschriften

(1) Das klinische Krebsregister darf zur Abrechnung der Registerpauschale nach § 65c Absatz 4 des Fünften Buches Sozialgesetzbuch und der Meldevergütung nach § 65c Absatz 6 des Fünften Buches Sozialgesetzbuch die für die einzelfallbezogene Abrechnung mit den Trägern der Kosten in Krankheits-, Pflege- und Geburtsfällen nach beamtenrechtlichen Vorschriften notwendigen Daten der Patientin oder des Patienten verarbeiten.
Die für die Abrechnung erforderlichen Daten und die inhaltlichen und technischen Anforderungen des Abrechnungsverfahrens richten sich nach der Vereinbarung zwischen dem Bund und dem klinischen Krebsregister in der jeweils aktuellen Fassung, soweit der für die jeweilige Patientin oder den Patienten zuständige Träger der Kosten in Krankheits-, Pflege- und Geburtsfällen nach beamtenrechtlichen Vorschriften dieser Vereinbarung beigetreten ist.
Ergänzend sind inhaltliche und technische Vorgaben der für die jeweilige Patientin oder den jeweiligen Patienten zuständigen Beihilfe- oder Festsetzungsstelle zu beachten.

(2) Ist dem klinischen Krebsregister bei der Abrechnung eines Einzelfalls nicht bekannt, ob der für diese Patientin oder diesen Patienten zuständige Träger der Kosten in Krankheits-, Pflege- und Geburtsfällen nach beamtenrechtlichen Vorschriften sich an den Kosten der klinischen Krebsregistrierung nach § 65c des Fünften Buches Sozialgesetzbuch beteiligt, hat das klinische Krebsregister dies mit diesem Träger zunächst ohne Nennung von personenbezogenen Daten der Patientin oder des Patienten zu klären.

(3) Das klinische Krebsregister hat der Aufsichtsbehörde jährlich bis zum 1.
Juni für das zurückliegende Jahr eine bezogen auf den Behandlungsort nach den Ländern Berlin und Brandenburg aufgeteilte Aufstellung über die Zahl der Fälle aufgeschlüsselt nach Trägern der Kosten in Krankheits-, Pflege- und Geburtsfällen nach beamtenrechtlichen Vorschriften zu übermitteln, die nicht nach Absatz 1 abgerechnet werden konnten.

#### Artikel 23  
Abrechnung mit den meldepflichtigen Personen und den Meldestellen

(1) Für jede vollständige Meldung, die aus einem Meldeanlass nach Artikel 12 erfolgt ist und gegen die ein Widerspruch nach Artikel 15 Absatz 1 in Verbindung mit Artikel 16 nicht erhoben worden ist, zahlt das klinische Krebsregister der meldepflichtigen Person oder der Meldestelle als Entschädigung für den mit der Meldung verbundenen Aufwand eine Meldevergütung, deren jeweilige Höhe sich nach § 65c Absatz 6 des Fünften Buches Sozialgesetzbuch richtet.
Eine Meldung ist vollständig, wenn sie die Voraussetzungen nach Artikel 13 Absatz 1 oder Absatz 3 erfüllt.
Die Zahlung einer Meldevergütung nach Satz 1 ist ausgeschlossen, wenn die in der Meldung enthaltenen Informationen dem klinischen Krebsregister bereits durch eine andere meldepflichtige Person oder eine andere Meldestelle vollständig gemeldet wurden.
In den Fällen des Artikels 11 Absatz 7 besteht kein Anspruch auf eine Meldevergütung nach Satz 1.
Der Ausschluss der Meldevergütung nach § 65c Absatz 9 Satz 4 des Fünften Buches Sozialgesetzbuch bleibt unberührt.

(2) Besteht ein Anspruch auf eine Meldevergütung nach Absatz 1 Satz 1, zahlt das klinische Krebsregister die Meldevergütung spätestens sechs Monate nach Eingang der Meldung im Versorgungsbereich.

### Abschnitt 6  
Datenverarbeitung

#### Artikel 24  
Versorgungsbereich

Der Versorgungsbereich hat

1.  die gemeldeten Daten entgegenzunehmen und getrennt nach Identitätsdaten und medizinischen Daten langfristig zu speichern,
2.  die Identitätsdaten im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 4 zur Feststellung des Vorliegens eines Widerspruchs nach Artikel 15 mit der Widerspruchsdatenbank abzugleichen,
3.  bei Vorliegen eines Widerspruchs die auf die jeweilige Person bezogenen medizinischen Daten nach Maßgabe des Artikels 16 unverzüglich zu löschen und die Identitätsdaten im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 4 sowie die Art des Widerspruchs in die Widerspruchsdatenbank nach Artikel 16 Absatz 1 Satz 1 einzutragen,
4.  in den Fällen, in denen ein Widerspruch nicht vorliegt,
    1.  innerhalb von sechs Wochen die gemeldeten Daten personenbezogen elektronisch zu erfassen und auf Validität, Plausibilität und Vollständigkeit sowie auf Konsistenz zu bereits vorhandenen Daten zu einer Patientin oder einem Patienten zu überprüfen,
    2.  die gemeldeten Daten, soweit erforderlich, durch Rückfrage bei der meldepflichtigen Person oder der Meldestelle zu berichtigen und zu ergänzen sowie
    3.  die gemeldeten und gegebenenfalls berichtigten oder ergänzten Daten mit bereits gespeicherten Daten fallbezogen zu einem Best-of-Datensatz zusammenzuführen oder einen neuen fallbezogenen Datensatz unter einem neuen mit einem eindeutigen patientenbezogenen und einem auf den Wohnort in einem der Einzugsgebiete nach Artikel 1 Absatz 2 bezogenen Ordnungsmerkmal, das langfristig zu speichern ist, anzulegen,
5.  die Abrechnung der Registerpauschale und der Meldevergütung mit den Kostenträgern, den meldepflichtigen Personen und den Meldestellen nach den Artikeln 19 bis 23 durchzuführen und die dafür notwendigen Daten zu speichern, zu nutzen und zu übermitteln,
6.  Daten für regelmäßige Kontrollen zur Vollständigkeit der Registrierungen zu nutzen,
7.  Daten für regelmäßige Kontrollen zur Vollzähligkeit der Registrierungen unter Heranziehung der dazu vom Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen bereitgestellten Zahlen sowie den Abgleich dieser Zahlen mit der Zahl der gesondert gespeicherten Widersprüche nach Artikel 16 Absatz 1 Satz 1 zu nutzen,
8.  aus Anlass einer Auswertung des Auswertungsbereichs festgestellte Inkonsistenzen in einem Datensatz durch Rückfrage bei der meldepflichtigen Person oder der Meldestelle aufzuklären und gegebenenfalls Ermittlungen wegen einer Ordnungswidrigkeit im Sinne des Artikels 35 Absatz 2 Nummer 1 aufzunehmen,
9.  die Möglichkeit des Direktabrufs der Identitätsdaten nach Maßgabe des Artikels 18 Absatz 3 zu sperren,
10.  Daten nach Maßgabe des Artikels 18 Absatz 1 und 2 zu löschen,
11.  Verstöße im Sinne des Artikels 35 Absatz 2 Nummer 1 und 2 zu ermitteln und das Ermittlungsergebnis zur weiteren Bearbeitung an die für die Verfolgung von Ordnungswidrigkeiten nach Artikel 6 Absatz 2 Satz 1 Nummer 3 zuständige Koordinierungsstelle zu übermitteln,
12.  1.  interdisziplinäre und gegebenenfalls sektorenübergreifende Tumorkonferenzen nach Maßgabe einer Vereinbarung im Sinne des Artikels 6 Absatz 2 Satz 1 Nummer 9 zu initiieren und zu begleiten,
    2.  zu prüfen, ob die nach Artikel 6 Absatz 2 Satz 1 Nummer 9 notwendigen Einwilligungen der Patientinnen und Patienten vorliegen sowie
    3.  bei Wahrnehmung durch ärztliche Beschäftigte des Auswertungsbereichs nach Artikel 25 Absatz 2 Satz 2 diesen die im Einzelfall für die jeweilige Tumorkonferenz notwendigen patientenbezogenen Daten zu übermitteln,
13.  die Daten im Sinne des Artikels 3 Absatz 3, 5 und 6 Nummer 1 bis 9 mit einem patientenbezogenen eindeutigen Ordnungsmerkmal zu pseudonymisieren, dieses zu speichern und mit dem auf den Wohnort in einem der Einzugsgebiete nach Artikel 1 Absatz 2 bezogenen Ordnungsmerkmal an den Auswertungsbereich zu übermitteln,
14.  Auskunft nach Artikel 17 über die im klinischen Krebsregister gespeicherten Angaben zu einer Patientin oder einem Patienten zu erteilen,
15.  dem Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen nach Maßgabe des Artikels 29 Absatz 1 und des Artikels 36 Absatz 3 regelmäßig die epidemiologischen Daten zu übermitteln,
16.  das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen nach Artikel 29 Absatz 1 Satz 4 über einen nachträglich erhobenen Widerspruch zu informieren,
17.  dem Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen nach Artikel 29 Absatz 2 eine Liste mit den Angaben im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 4 zu allen in seinen Einzugsgebieten mit Hauptwohnsitz erfassten Patientinnen und Patienten einschließlich derjenigen Patientinnen und Patienten, die einen Widerspruch gegen die Speicherung medizinischer Daten erhoben haben, für den Melderegisterabgleich und den Leichenschauscheinabgleich zu übermitteln,
18.  die vom Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen nach Artikel 29 Absatz 2 und 3 übermittelten Daten aus dem regelmäßigen Melderegisterabgleich und dem Leichenschauscheinabgleich wie eine Meldung zu verarbeiten und, soweit erforderlich, nach Rückfrage bei der Ärztin oder dem Arzt, die oder der den Leichenschauschein ausgestellt hat, oder bei der zuletzt behandelnden Ärztin oder dem zuletzt behandelnden Arzt Berichtigungen vorzunehmen,
19.  dem Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen nach Artikel 29 Absatz 4 die Daten zu der letzten behandelnden Ärztin oder Zahnärztin oder zu dem letzten behandelnden Arzt oder Zahnarzt zu übermitteln,
20.  die Daten von Patientinnen und Patienten, deren Hauptwohnsitz und Behandlungsort in Einzugsgebieten verschiedener klinischer Krebsregister liegen, nach Maßgabe des Artikels 30 an andere regionale klinische Krebsregister zu übermitteln sowie die durch andere klinische Krebsregister übermittelten Daten wie eine Meldung zu verarbeiten und bei Vorliegen der Voraussetzungen des Artikels 30 Absatz 2 Satz 2 zu löschen,
21.  der meldepflichtigen Person oder der Meldestelle auf Abfrage patientenbezogene Rückmeldungen des gesamten registrierten Krankheitsverlaufs nach Maßgabe des Artikels 31 zur Verfügung zu stellen,
22.  personenbezogene Daten für Vorhaben der Versorgungsforschung nach Artikel 32 Absatz 2 bereitzustellen und zu übermitteln sowie in den Fällen des Artikels 32 Absatz 4 Satz 4 die Einwilligung einzuholen und
23.  sich als Datenannahmestelle und gegebenenfalls als Vertrauensstelle nach § 299 Absatz 2 des Fünften Buches Sozialgesetzbuch an der einrichtungs- und sektorenübergreifenden Qualitätssicherung des Gemeinsamen Bundesausschusses nach § 65c Absatz 1 Satz 2 Nummer 5 des Fünften Buches Sozialgesetzbuch in Verbindung mit § 65c Absatz 8 des Fünften Buches Sozialgesetzbuch zu beteiligen.

#### Artikel 25  
Auswertungsbereich

(1) Der Auswertungsbereich hat

1.  die klinischen Daten, das vom Versorgungsbereich nach Artikel 24 Nummer 13 übermittelte patientenbezogene und wohnortbezogene Ordnungsmerkmal sowie das von ihm zu bildende meldendenbezogene Ordnungsmerkmal nach Nummer 8 langfristig zu speichern,
2.  die erfassten klinischen Daten tumorspezifisch mit den Ergebnissen aller relevanten Daten und derjenigen aktuell veröffentlichten leitlinienbasierten Qualitätsindikatoren, die mittels des in § 65c Absatz 1 Satz 3 des Fünften Buches Sozialgesetzbuch vorgeschriebenen Datensatzes abbildbar sind, auszuwerten und die aggregierten Ergebnisse der Auswertung den an der Behandlung beteiligten meldepflichtigen Personen oder Meldestellen, die Daten an das Register gemeldet haben, regelmäßig zur Verfügung zu stellen,
3.  auf Anfrage tumorspezifische Auswertungen für einrichtungsinterne und einrichtungsübergreifende Qualitätszirkel oder interdisziplinäre Arbeitsgruppen nach Artikel 8 Absatz 2 zur Verfügung zu stellen,
4.  die für Analysen der Haupteinflussfaktoren des Behandlungserfolgs selbst oder in Kooperation notwendigen Daten zu nutzen und die Ergebnisse den meldepflichtigen Personen und Meldestellen zur Verfügung zu stellen,
5.  im Zuge der Auswertung festgestellte Inkonsistenzen in einem Datensatz dem Versorgungsbereich zur Aufklärung nach Artikel 24 Nummer 8 unter Angabe des patientenbezogenen Ordnungsmerkmals mitzuteilen,
6.  Daten für Vorhaben der Versorgungsforschung nach Artikel 32 Absatz 1 bereitzustellen und zu übermitteln,
7.  die nach Artikel 33 für Zwecke der Krankenhausplanung erforderlichen Auswertungsdaten zu erstellen und zu übermitteln,
8.  die Daten zu den meldepflichtigen Personen und den Meldestellen vor Übermittlung der Daten an die Landesauswertungsstelle zu pseudonymisieren,
9.  der Landesauswertungsstelle einmal im Jahr einen den Anforderungen des Artikels 6 Absatz 3 Satz 2 Nummer 2 entsprechenden Gesamtdatensatz und dafür pro Erkrankungsfall den anonymisierten Best-of-Datensatz sowie auf Anforderung die für die Aufgaben nach Artikel 6 Absatz 3 Satz 1 Nummer 2 und 3 benötigten Daten zu übermitteln und
10.  einzelfallbezogene Nachfragen aus dem Versorgungsbereich zur korrekten Dokumentationsweise unter Bezugnahme auf das patientenbezogene Ordnungsmerkmal zu beantworten.

(2) Die Kenntnisnahme von Identitätsdaten im Sinne des Artikels 3 Absatz 1 im Zuge der Aufgabenerfüllung ist auszuschließen.
Abweichend hiervon ist mit Einwilligung der betreffenden Patientinnen und Patienten die Begleitung von interdisziplinären und gegebenenfalls sektorenübergreifenden Tumorkonferenzen aufgrund von Vereinbarungen nach Artikel 6 Absatz 2 Satz 1 Nummer 9 auch durch ärztliche Beschäftigte des Auswertungsbereichs zulässig.
Diese Tätigkeiten erfolgen ohne Zugriff auf die Datenbank des Versorgungsbereichs.
Die notwendigen Patientendaten werden den ärztlichen Beschäftigten einzelfallbezogen vom Versorgungsbereich nach Artikel 24 Nummer 12 Buchstabe c zur Verfügung gestellt.

#### Artikel 26  
Landesauswertungsstelle

Die Landesauswertungsstelle nutzt, soweit es sich um patientenbezogene Daten handelt, ausschließlich anonymisierte Daten, die ihr vom Auswertungsbereich nach Artikel 25 Absatz 1 Nummer 9 für ihre Aufgaben nach Artikel 6 Absatz 3 Satz 1 zur Verfügung gestellt werden.
Zu den meldepflichtigen Personen und den Meldestellen nutzt die Landesauswertungsstelle die ihr vom Auswertungsbereich übermittelten pseudonymisierten Daten.
Ein Zugang zur Datenbank des klinischen Krebsregisters ist auszuschließen.

#### Artikel 27  
Koordinierungsstelle

Beschäftigte der Koordinierungsstelle haben Zugang zu personenbezogenen Daten von Patientinnen und Patienten nur, soweit dies zur Erfüllung ihrer Aufgaben nach Artikel 6 Absatz 2 Satz 1 Nummer 2 und 3 im Einzelfall erforderlich oder nach anderen Rechtsvorschriften erlaubt ist.

#### Artikel 28  
Geheimhaltungspflichten

(1) Die dem klinischen Krebsregister oder den dort Beschäftigten im Zusammenhang mit der Erfüllung der Aufgaben nach Artikel 6 bekannt gewordenen Daten und Datensätze im Sinne des Artikels 3 sowie die ihnen bekannt gewordenen Betriebs- und Geschäftsgeheimnisse der meldepflichtigen Personen und der Meldestellen sind geheim zu halten, soweit sie nicht offenkundig sind.
Die Geheimhaltungspflicht gilt auch über das Ende des Beschäftigungsverhältnisses oder den Tod der betroffenen Person hinaus.Sonstige gesetzliche oder berufsrechtliche Geheimhaltungspflichten bleiben unberührt.

(2) Die Daten und die Betriebs- und Geschäftsgeheimnisse im Sinne des Absatzes 1 dürfen nur für die Erfüllung der Aufgaben des klinischen Krebsregisters nach Artikel 6 verwendet und nach Maßgabe dieses Staatsvertrages verarbeitet werden, es sei denn, es besteht eine gesetzliche Befugnis zu ihrer anderweitigen Verwendung und Verarbeitung oder die betroffene Person, ihre gesetzliche Vertreterin oder ihr gesetzlicher Vertreter oder eine von ihr für die Gesundheitssorge bevollmächtigte Person hat hierzu eine Einwilligung schriftlich erteilt.

(3) Die Absätze 1 und 2 gelten auch im Verhältnis der Bereiche des klinischen Krebsregisters nach Artikel 2 und der ihnen nach dem Geschäftsverteilungsplan der GmbH zugewiesenen Beschäftigten.
Die Geheimhaltungspflicht gilt nicht gegenüber Beschäftigten des klinischen Krebsregisters, denen Aufgaben der bereichsübergreifenden IT-Administration, des behördlichen Datenschutzes oder der Verfolgung und Ahndung von Ordnungswidrigkeiten übertragen wurden, sofern und soweit die Preisgabe der Information für die Erfüllung der den Beschäftigten übertragenen Aufgaben jeweils notwendig ist.

(4) Die von der GmbH zur Wartung ihrer Datenverarbeitungssysteme herangezogenen Auftragnehmer unterliegen hierbei den sich aus den Absätzen 1 bis 3 ergebenden Geheimhaltungspflichten.
Beschäftigte nicht-öffentlicher Stellen sind vor der Aufnahme ihrer Tätigkeit für die GmbH vom Auftragnehmer auf die Einhaltung dieser Pflichten und die Konsequenzen aus Artikel 34 und Artikel 35 hinzuweisen.
Die GmbH ist nicht befugt, Auftragnehmer mit der Verarbeitung von Daten und Datensätzen im Sinne des Artikels 3 zu beauftragen.

### Abschnitt 7  
Datenaustausch mit Dritten

#### Artikel 29  
Datenaustausch mit dem Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen

(1) Der Versorgungsbereich ist verpflichtet, der Vertrauensstelle des Gemeinsamen Krebsregisters der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen mindestens quartalsweise die erfassten und geprüften Daten im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 6 und Absatz 2 zu übermitteln.
Die Meldung ersetzt die Meldung nach § 3 Absatz 1 Satz 1 des Krebsregistergesetzes.
Ist ein Widerspruch nach Artikel 15 Absatz 2 Satz 1 erhoben worden, ist die Übermittlung unzulässig.
Wird ein Widerspruch nach Artikel 15 Absatz 2 Satz 1 oder Artikel 15 Absatz 1 in Verbindung mit Artikel 16 erhoben, nachdem bereits Daten zu dieser Patientin oder diesem Patienten an die Vertrauensstelle des Gemeinsamen Krebsregisters der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen übermittelt wurden, ist abweichend von § 3 Absatz 2 Satz 6 des Krebsregistergesetzes die Vertrauensstelle des Gemeinsamen Krebsregisters der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen im Rahmen der regelmäßigen Übermittlung nach Satz 1 über den Widerspruch zu informieren.
Der Versorgungsbereich hat die meldepflichtige Person oder die Meldestelle über die ihm abweichend von § 3 Absatz 2 Satz 7 des Krebsregistergesetzes mitgeteilte Löschung von Daten durch das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen schriftlich zu unterrichten; die meldepflichtige Person oder die Meldestelle hat die Unterrichtung an die Patientin oder den Patienten weiterzugeben.

(2) Der Versorgungsbereich übermittelt dem Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen mindestens halbjährlich eine Liste mit den Angaben im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 4 zu allen in seinen Einzugsgebieten nach Artikel 1 Absatz 2 mit Hauptwohnsitz erfassten Patientinnen und Patienten zur Durchführung des regelmäßigen Abgleichs mit den Daten der Meldebehörden und mit den Leichenschauscheinen.
Der Versorgungsbereich ist für Patientinnen und Patienten mit Hauptwohnsitz im Land Brandenburg auch zur Verarbeitung einer einmaligen Datenübermittlung zu zurückliegenden Kalenderjahren berechtigt.
Er verarbeitet die vom Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen in diesem Verfahren übermittelten Daten wie eine Meldung.

(3) Der Versorgungsbereich erhält von dem Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen im Rahmen des regelmäßigen Abgleichs mit den Leichenschauscheinen die darin enthaltenen Angaben zu den Identitätsdaten im Sinne des Artikels 3 Absatz 1, taggenauem Sterbedatum, Todesursachen und der Ärztin oder dem Arzt, die die verstorbene Person zuvor behandelt oder untersucht oder die Leiche obduziert hat, auch zu solchen nicht namentlich benannten Patientinnen und Patienten mit Hauptwohnsitz in den Einzugsgebieten des klinischen Krebsregisters, bei denen sich aus dem Leichenschauschein als Todesursache eine Erkrankung im Sinne des Artikels 11 Absatz 4 Satz 1 ergibt.
Der Versorgungsbereich verarbeitet die vom Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen in diesem Verfahren übermittelten Daten wie eine Meldung.

(4) Der Versorgungsbereich ist berechtigt, der Vertrauensstelle des Gemeinsamen Krebsregisters der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen die Daten im Sinne des Artikels 3 Absatz 5 zu der letzten behandelnden Ärztin oder Zahnärztin oder zu dem letzten behandelnden Arzt oder Zahnarzt zu übermitteln, soweit dies zur Wahrnehmung der Aufgabe nach § 8 Absatz 2 des Krebsregistergesetzes erforderlich ist.

(5) Für den Zeitraum bis zum Vorliegen eines den Anforderungen des Bundesamtes für Sicherheit in der Informationstechnik entsprechenden leitungsgebundenen Verfahrens für den elektronischen Datenaustausch erfolgt der Datenaustausch zwischen dem klinischen Krebsregister und dem Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen mittels verschlüsselter Datenträger.
Die Verschlüsselung hat den allgemein anerkannten Regeln der Technik zu entsprechen.

#### Artikel 30  
Datenaustausch mit anderen klinischen Krebsregistern

(1) Der Versorgungsbereich ist verpflichtet, alle gespeicherten Daten aus den Meldungen nach Artikel 13 über Personen, die im Geltungsbereich dieses Staatsvertrages behandelt werden oder wurden und im Einzugsgebiet eines anderen klinischen Krebsregisters ihren Hauptwohnsitz haben, regelmäßig, jedoch mindestens halbjährlich im März und im September an dieses andere klinische Krebsregister zur Erfüllung der diesem Register übertragenen Aufgaben zu übermitteln.
Der Versorgungsbereich ist verpflichtet, alle gespeicherten Daten aus den Meldungen nach Artikel 13 über Personen mit Hauptwohnsitz im Geltungsbereich dieses Staatsvertrages, die im Einzugsgebiet eines anderen regionalen klinischen Krebsregisters behandelt werden oder wurden, regelmäßig, jedoch mindestens halbjährlich im März und September an das zuständige Behandlungsortregister zu übermitteln.
Ist ein Widerspruch gegen die Speicherung medizinischer Daten nach Artikel 15 Absatz 1 in Verbindung mit Artikel 16 erhoben worden, sind dem anderen klinischen Krebsregister die Daten der Patientin oder des Patienten im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 4 und die Art des Widerspruchs zu übermitteln.

(2) Der Versorgungsbereich ist verpflichtet, zur Vervollständigung des Datensatzes des klinischen Krebsregisters und zur Qualitätssicherung die von einem anderen klinischen Krebsregister übermittelten Daten wie eine Meldung zu verarbeiten.
Wird von einem anderen klinischen Krebsregister ein Widerspruch übermittelt, bei dem die Identitätsdaten mit einer Kontrollnummer pseudonymisiert sind, ist dieser Datensatz zu löschen.

#### Artikel 31  
Patientenbezogene Datenabfrage durch meldepflichtige Personen und Meldestellen

(1) Der Versorgungsbereich übermittelt meldepflichtigen Personen und Meldestellen mit Sitz in den Einzugsgebieten des klinischen Krebsregisters auf ihre Abfrage hin zur Förderung der interdisziplinären Zusammenarbeit, zur Begleitung von Tumorkonferenzen oder im Rahmen der Zusammenarbeit mit Zentren der Onkologie, zur Verbesserung der Qualität bei der Behandlung oder zur Diagnose und Behandlung der betreffenden Patientin oder des betreffenden Patienten zu den von ihnen gemeldeten Patientinnen und Patienten personenbezogen den im klinischen Krebsregister gespeicherten Best-of-Datensatz zu allen Tumorerkrankungen einer Patientin oder eines Patienten.
Für eine Abfrage nach Satz 1 hat die meldepflichtige Person oder die Meldestelle

1.  den Zweck der Abfrage und Art und Umfang der hierfür benötigten Daten,
2.  die Identitätsdaten der Patientin oder des Patienten im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 4 sowie
3.  die auf sie oder ihn zutreffenden identifizierenden Daten im Sinne des Artikels 3 Absatz 5 und 6 Nummer 1 bis 6

zu übermitteln.
Erfolgt die Abfrage auf elektronischem Weg, ist diese nur mit dem elektronischen Heilberufeausweis zulässig; dabei sind dessen Funktionen für die Authentifikation der abrufenden Person beziehungsweise die Signatur des Abrufsbegehrens einerseits und die Entschlüsselung der bereitgestellten Daten andererseits einzusetzen.

(2) Der Versorgungsbereich hat jede schriftliche Abfrage zu protokollieren.
Das Protokoll ist bis zum Ende des auf die Protokollierung folgenden Kalenderjahres aufzubewahren und danach zu löschen oder zu vernichten.

(3) Der im Zusammenhang mit einer Abfrage nach Absatz 1 übermittelte Best-of-Datensatz muss einen deutlichen Hinweis darauf enthalten, dass er nicht alleinige Grundlage individueller Therapieentscheidungen sein darf.

#### Artikel 32  
Datenübermittlung für die Versorgungsforschung

(1) Über die nach Artikel 6 Absatz 4 Nummer 1 übermittelten und nach Artikel 6 Absatz 3 Satz 2 Nummer 1 veröffentlichten Daten hinaus darf das klinische Krebsregister auf Antrag gespeicherte Daten in anonymisierter Form für ein dem Stand der Wissenschaft entsprechendes Vorhaben der Versorgungsforschung übermitteln.
Sofern die Daten aufgrund ihrer Art oder ohne eine Gefährdung des Zweckes des Vorhabens nach Satz 1 nicht anonymisiert werden können, sindsie vor ihrer Übermittlung zu pseudonymisieren.

(2) Sofern und soweit einer Anonymisierung und Pseudonymisierung von Daten nach Absatz 1 zwingende wissenschaftliche Gründe eines Vorhabens der Versorgungsforschung entgegenstehen, darf der Versorgungsbereich auf Antrag und nur nach schriftlicher Einwilligung der betroffenen Patientinnen und Patienten, ihrer gesetzlichen Vertreterinnen oder Vertreter oder der von ihnen für die Gesundheitssorge bevollmächtigten Personen Identitätsdaten übermitteln.

(3) Der Beirat ist nach Artikel 7 Absatz 1 Satz 2 Nummer 4 vor einer Entscheidung über die Übermittlung von Daten nach Absatz 1 oder Absatz 2 zu beteiligen.
Die oder der Landesbeauftragte für den Datenschutz und für das Recht auf Akteneinsicht des Landes Brandenburg ist in den Fällen des Absatzes 1 Satz 2 und des Absatzes 2 anzuhören.

(4) Die schriftliche Einwilligung nach Absatz 2 ist dem klinischen Krebsregister von der antragstellenden Person oder Einrichtung mit dem Antrag auf Datenübermittlung vorzulegen.
Ist die Patientin oder der Patient verstorben, ohne dass eine wirksame Einwilligung vorliegt, ist eine Einwilligung der oder des nächsten Angehörigen von der antragstellenden Person oder Einrichtung vorzulegen.
Als nächste Angehörige gelten in dieser Reihenfolge:

1.  Ehegatten und Lebenspartner,
2.  volljährige Kinder,
3.  Eltern,
4.  volljährige Geschwister.

Das klinische Krebsregister kann die Einwilligung auf Antrag auch selbst einholen, wenn die antragstellende Person oder Einrichtung den damit verbundenen Aufwand erstattet.
Sollen die Daten länger als zwei Jahre gespeichert werden, ist die einwilligende Person durch die antragstellende Person oder Einrichtung oder in dem Fall des Satzes 4 durch das klinische Krebsregister bereits bei Einholung der Einwilligung darauf hinzuweisen.

(5) Die nach Absatz 1 oder Absatz 2 übermittelten Daten dürfen von der antragstellenden Person oder Einrichtung nur für den im Antrag angegebenen Zweck verarbeitet werden.
Eine Übermittlung der Daten an Dritte ist unzulässig.
Die Daten sind zu löschen, wenn sie für die Durchführung des Vorhabens der Versorgungsforschung nicht mehr erforderlich sind, spätestens jedoch mit seinem Abschluss.
Das klinische Krebsregister ist umgehend über die erfolgte Löschung zu unterrichten.

#### Artikel 33  
Datenübermittlung für Zwecke der Krankenhausplanung

Der Auswertungsbereich übermittelt den obersten Landesgesundheitsbehörden der vertragschließenden Länder für Zwecke der Krankenhausplanung einmal jährlich zum 30.
September jeweils landesbezogen alle registrierten Krankenhausbehandlungsfälle des vorangegangenen Kalenderjahres, jeweils mit den von den obersten Landesgesundheitsbehörden festgelegten Datensatzmerkmalen im Sinne des Artikels 3 Absatz 3, jedoch zu den meldepflichtigen Personen und den Meldestellen nur die Daten im Sinne des Artikels 3 Absatz 6 Nummer 1 und 2.
Maßgeblich für die Zuordnung zum Kalenderjahr ist das Datum der Krankenhausbehandlung der jeweiligen Patientin oder des jeweiligen Patienten.

### Abschnitt 8  
Straftaten und Ordnungswidrigkeiten

#### Artikel 34  
Straftaten

(1) Wer entgegen den Vorschriften dieses Staatsvertrages

1.  personenbezogene Daten, die nicht offenkundig sind, verarbeitet oder dies veranlasst und dadurch das Recht auf informationelle Selbstbestimmung einer Person verletzt oder
2.  eine Regel, die der Pseudonymisierung von Daten im Sinne des Artikels 3 dient, unbefugt offenbart oder nutzt,

wird mit Freiheitsstrafe bis zu einem Jahr oder mit Geldstrafe bestraft.
Gesetze, die eine Strafbarkeit wegen Verletzung eines Berufs- oder Amtsgeheimnisses begründen, bleiben unberührt.

(2) Handelt der Täter gegen Entgelt oder in der Absicht, sich oder einem anderen einen Vorteil zu verschaffen oder einen anderen zu schädigen, so ist die Strafe Freiheitsstrafe bis zu zwei Jahren oder Geldstrafe.

(3) Die Tat wird nur auf Antrag verfolgt.
Antragsberechtigt sind die betroffene Person, die GmbH und die oder der Landesbeauftragte für den Datenschutz und für das Recht auf Akteneinsicht des Landes Brandenburg.
Ist die Tat im Land Berlin begangen worden, ist auch die oder der Berliner Beauftragte für Datenschutz und Informationsfreiheit antragsberechtigt.

#### Artikel 35  
Ordnungswidrigkeiten

(1) Ordnungswidrig handelt, wer eine der in Artikel 34 Absatz 1 bezeichneten Handlungen fahrlässig begeht.

(2) Ordnungswidrig handelt, wer vorsätzlich oder fahrlässig als meldepflichtige Person oder Meldestelle

1.  entgegen Artikel 11 Absatz 1 oder Absatz 2 in Verbindung mit den Artikeln 12 und 13 eine Meldung an den Versorgungsbereich nicht, nicht rechtzeitig, nicht richtig oder nicht vollständig übermittelt,
2.  entgegen Artikel 13 Absatz 4 Satz 1 es unterlässt, die Tatsache und die Art eines Widerspruchs nach Artikel 15 Absatz 1 zu übermitteln, oder
3.  entgegen Artikel 14 Absatz 1 Satz 1 die Information nicht, nicht rechtzeitig, nicht richtig oder nicht vollständig erteilt.

(3) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

(4) Verwaltungsbehörde im Sinne des § 36 Absatz 1 Nummer 1 des Gesetzes über Ordnungswidrigkeiten ist

1.  bei Ordnungswidrigkeiten nach Absatz 1 und 2 Nummer 3 die oder der Berliner Beauftragte für Datenschutz und Informationsfreiheit oder die oder der Landesbeauftragte für den Datenschutz und für das Recht auf Akteneinsicht des Landes Brandenburg,
2.  bei Ordnungswidrigkeiten nach Absatz 2 Nummer 1 und 2 das klinische Krebsregister.

### Abschnitt 9  
Übergangsvorschriften

#### Artikel 36  
Übergangsregelungen für meldepflichtige Personen und Meldestellen mit Sitz im Land Berlin

(1) Meldepflichtige Personen und Meldestellen mit Sitz im Land Berlin unterliegen der Meldepflicht nach Artikel 11 in Verbindung mit den Artikeln 12 bis 14 nur für im Land Berlin behandelte Neuerkrankungen, die ab dem Tag des Inkrafttretens dieses Staatsvertrages auftreten.
Für Neuerkrankungen im Sinne des Satzes 1, die bis zum Ende des dritten Monats nach Inkrafttreten dieses Staatsvertrages auftreten, beginnt die Meldefrist von vier Wochen abweichend von Artikel 11 Absatz 2 drei Monate nach dem Tag des Inkrafttretens dieses Staatsvertrages.
Für Meldungen im Sinne des Satzes 2 beginnt der Zeitraum von sechs Monaten abweichend von Artikel 23 Absatz 2 mit dem 1.
Oktober 2016.

(2) Für vor dem Tag des Inkrafttretens dieses Staatsvertrages bereits bestehende Erkrankungen von Patientinnen und Patienten mit Wohnsitz im Land Berlin sind meldepflichtige Personen und Meldestellen mit Sitz im Land Berlin abweichend von Absatz 1 verpflichtet, dem klinischen Krebsregister die Daten im Sinne des Artikels 3 Absatz 1 Nummer 1 bis 6 und Absatz 2 nach dem 1. Oktober 2016 zu melden.
Artikel 11 Absatz 2, Artikel 13 Absatz 1 Satz 1 und 2, die Artikel 23 sowie 35 Absatz 2 sind auf diese Meldungen nicht anzuwenden.
Die Meldungen werden durch das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen auf der Grundlage von § 3 Absatz 4 des Krebsregistergesetzes vergütet.
Das klinische Krebsregister übermittelt dem Gemeinsamen Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen mit den nach Satz 1 gemeldeten Daten abweichend von Artikel 29 Absatz 1 Satz 1 auch die Daten des Artikels 3 Absatz 5.
Die Daten sind im klinischen Krebsregister nach der Übermittlung an das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen zu löschen.

#### Artikel 37  
Altfallregelung für das Land Brandenburg

Das klinische Krebsregister darf die am Tag des Inkrafttretens dieses Staatsvertrages von den Nachsorgeleitstellen im Land Brandenburg bereits erhobenen und gespeicherten Datenbestände wie Daten verwenden, die von ihm auf der Grundlage dieses Staatsvertrages zu erheben und zu verarbeiten sind.

### Abschnitt 10  
Schlussvorschriften

#### Artikel 38  
Geltungsdauer und Beendigung

(1) Dieser Staatsvertrag wird auf unbestimmte Zeit geschlossen.

(2) Dieser Staatsvertrag kann von jedem der vertragschließenden Länder zum Schluss eines Kalenderjahres mit einer Frist von zwei Jahren, frühestens aber zum 31.
Dezember 2022, ordentlich gekündigt werden.
Die Kündigung aus wichtigem Grund bleibt unberührt.
Die Kündigung nach Satz 2 soll mit einer angemessenen Frist erklärt werden.
Die Kündigung ist schriftlich zu erklären.
Satz 4 gilt auch für die Aufhebung dieses Staatsvertrages.

(3) Soweit nicht etwas anderes von den vertragschließenden Ländern mit der Alleingesellschafterin vereinbart wird, hat die GmbH bei Wirksamwerden der Kündigung oder bei Aufhebung dieses Staatsvertrages die von beiden Ländern bereitgestellten Investitionsmittel für die Erst- und Wiederbeschaffung sowie die Ergänzung von Anlagegütern gemäß der jeweils geleisteten Anteile zu erstatten.
Die Pflicht zur Erstattung mindert sich entsprechend dem Umfang der abgelaufenen betriebsgewöhnlichen Nutzungsdauer der Anlagegüter.
Sie besteht nur bis zur jeweiligen Höhe des Liquidationswertes der Anlagegüter.

(4) Die bei Wirksamwerden der Kündigung oder bei Aufhebung dieses Staatsvertrages im klinischen Krebsregister gespeicherten Daten werden nach Maßgabe der folgenden Regelungen einem Land oder beiden Ländern zugewiesen und an die dort für den Vollzug des § 65c Absatz 1 des Fünften Buches Sozialgesetzbuch nach Landesrecht zuständige Behörde oder, sofern und solange eine solche noch nicht eingerichtet ist, an eine von dem jeweiligen Land benannte öffentliche Stelle übermittelt:

1.  Daten, die Patientinnen und Patienten betreffen, deren letzter im klinischen Krebsregister verzeichneter Hauptwohnsitz oder Behandlungsort im Land Berlin liegt, werden dem Land Berlin zugewiesen.
2.  Daten, die Patientinnen und Patienten betreffen, deren letzter im klinischen Krebsregister verzeichneter Hauptwohnsitz oder Behandlungsort im Land Brandenburg liegt, werden dem Land Brandenburg zugewiesen.
3.  Daten, die Patientinnen und Patienten betreffen, die dem klinischen Krebsregister allein aufgrund des Hauptwohnsitzes vorliegen und deren Hauptwohnsitz wegen Umzuges im Zeitpunkt der Zuweisung der Daten in keinem der Einzugsgebiete nach Artikel 1 Absatz 2 mehr liegt und die bis dahin noch nicht an das für ihren neuen Hauptwohnsitz zuständige klinische Krebsregister nach Artikel 30 übermittelt wurden, werden dem Land zugewiesen, in dem ihr letzter im klinischen Krebsregister verzeichneter Hauptwohnsitz in einem der Einzugsgebiete nach Artikel 1 Absatz 2 lag.

(5) Kopien der nach Absatz 4 zugewiesenen und übermittelten Daten und Datensätze sind nach erfolgreicher Übermittlung unverzüglich im klinischen Krebsregister zu löschen und zu vernichten.
Die Öffentlichkeit ist über die Übermittlung, die genaue Bezeichnung und Anschrift der Datenempfänger sowie die Löschung und Vernichtung der Daten und Datensätze in angemessener Weise zu informieren.

#### Artikel 39  
Einschränkung von Grundrechten

Durch diesen Staatsvertrag werden das Grundrecht auf Datenschutz (Artikel 33 der Verfassung von Berlin und Artikel 11 Absatz 1 der Verfassung des Landes Brandenburg) sowie das Grundrecht auf Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### Artikel 40  
Ratifikation und Inkrafttreten

Dieser Staatsvertrag bedarf der Ratifikation der verfassungsmäßig zuständigen Organe der vertragschließenden Länder.
Er tritt am ersten Tag des auf den Austausch der Ratifikationsurkunden folgenden Monats in Kraft.

Potsdam, den 12.
April 2016

Für das Land Berlin  
Der Regierende Bürgermeister,            
vertreten durch den Senator  
für Gesundheit und Soziales  
  
Mario Czaja

Für das Land Brandenburg  
Der Ministerpräsident,  
vertreten durch die Ministerin  
für Arbeit, Soziales, Gesundheit, Frauen und Familie  
  
Diana Golze

[zum Gesetz](/gesetze/krebsregister_be_bb_2016)