## Staatsvertrag über die Verteilung von Versorgungslasten bei bund- und länderübergreifenden Dienstherrenwechseln (Versorgungslastenteilungs-Staatsvertrag)

Die Bundesrepublik Deutschland,

das Land Baden-Württemberg,

der Freistaat Bayern,

das Land Berlin,

das Land Brandenburg,

die Freie Hansestadt Bremen,

die Freie und Hansestadt Hamburg,

das Land Hessen,

das Land Mecklenburg-Vorpommern,

das Land Niedersachsen,

das Land Nordrhein-Westfalen,

das Land Rheinland-Pfalz,

das Saarland,

der Freistaat Sachsen,

das Land Sachsen-Anhalt,

das Land Schleswig-Holstein und

der Freistaat Thüringen

schließen nachstehenden Staatsvertrag:

Präambel

Mit dem Gesetz zur Änderung des Grundgesetzes vom 28.
August 2006 wurden die Gesetzgebungszuständigkeiten im Dienstrecht neu geordnet.
Die Versorgungslastenteilung bei bund- und länderübergreifenden Dienstherrenwechseln kann nicht mehr bundesgesetzlich geregelt werden.
Gleichwohl sind einheitliche Regelungen für eine verursachungsgerechte Verteilung der Versorgungslasten erforderlich, um im Interesse der Mobilität auch in Zukunft an der Einheitlichkeit des Beamtenverhältnisses festzuhalten und einvernehmliche Dienstherrenwechsel zu ermöglichen.
Zu diesem Zweck wird dieser Staatsvertrag geschlossen.
Das bislang in § 107b des Beamtenversorgungsgesetzes (BeamtVG) und in § 92b des Soldatenversorgungsgesetzes (SVG) geregelte Erstattungsmodell wird durch ein pauschalierendes Abfindungsmodell ersetzt, wonach die Versorgungsanwartschaften zum Zeitpunkt des Dienstherrenwechsels abgegolten werden.

Abschnitt 1  
Allgemeines
-------------------------

### § 1   
Geltungsbereich

Dieser Staatsvertrag gilt für den Bund, die Länder sowie die Gemeinden, Gemeindeverbände und sonstigen, unter der Aufsicht des Bundes oder der Länder stehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts.

### § 2   
Dienstherrenwechsel

Ein Dienstherrenwechsel liegt vor, wenn eine Person, die in einem Beamten-, Soldaten- oder Richterverhältnis zu einem in § 1 genannten Dienstherrn steht, bei diesem Dienstherrn ausscheidet und in ein Beamten-, Soldaten- oder Richterverhältnis zu einem anderen, in § 1 genannten Dienstherrn tritt.
Ausgenommen sind Beamtinnen und Beamte auf Widerruf.
Für landes- und bundesinterne Dienstherrenwechsel gilt der Staatsvertrag nur, wenn dies gesetzlich bestimmt ist.

Abschnitt 2   
Versorgungslastenteilung
---------------------------------------

### § 3   
Voraussetzungen

(1) Eine Versorgungslastenteilung findet bei einem Dienstherrenwechsel statt, wenn der abgebende Dienstherr dem Dienstherrenwechsel zugestimmt hat und zwischen dem Ausscheiden und dem Eintritt keine zeitliche Unterbrechung liegt.

(2) Die Zustimmung muss vor dem Wirksamwerden des Dienstherrenwechsels schriftlich gegenüber dem aufnehmenden Dienstherrn erklärt werden.
Sie darf nur aus dienstlichen Gründen verweigert werden.

(3) Die Zustimmung gilt als erteilt, wenn Professorinnen und Professoren beim abgebenden Dienstherrn eine Dienstzeit von drei Jahren abgeleistet haben, wenn Beamtinnen und Beamten auf Zeit oder Soldatinnen und Soldaten auf Zeit mit Ablauf ihrer Dienst- oder Amtszeit bei einem neuen Dienstherrn eintreten oder wenn eine Wahl Voraussetzung für die Begründung des Beamtenverhältnisses ist.

(4) Eine zeitliche Unterbrechung ist unschädlich, wenn Personen aufgrund einer gesetzlichen Verpflichtung übernommen werden und keine Nachversicherung durchgeführt wurde.

### § 4  
Abfindung

(1) Die Versorgungslastenteilung erfolgt durch Zahlung einer Abfindung.

(2) Die Abfindung ist das Produkt aus den Bezügen (§ 5), den in vollen Monaten ausgedrückten Dienstzeiten (§ 6) und einem Bemessungssatz.
Der Bemessungssatz ist vom Lebensalter der wechselnden Person zum Zeitpunkt des Ausscheidens beim abgebenden Dienstherrn abhängig und beträgt

1.  bis Vollendung des 30.
    Lebensjahrs: 15 %,
2.  bis Vollendung des 50.
    Lebensjahrs: 20 %,
3.  nach Vollendung des 50.
    Lebensjahrs: 25 %.

Bei Professorinnen und Professoren beträgt der Bemessungssatz unabhängig vom Lebensalter 25 %.

(3) Maßgebend sind die tatsächlichen und rechtlichen Verhältnisse beim abgebenden Dienstherrn zum Zeitpunkt des Ausscheidens; Nachberechnungen finden nicht statt.

(4) Bei Beamtinnen und Beamten auf Zeit, die nach Ablauf ihrer beim abgebenden Dienstherrn begründeten Dienst- und Amtszeit nicht in den Ruhestand zu versetzen wären, ist eine Abfindung in Höhe der Kosten zu zahlen, die im Falle des Ausscheidens zum Zeitpunkt des Dienstherrenwechsels für eine Nachversicherung der bei ihm zurückgelegten Zeiten in der gesetzlichen Rentenversicherung angefallen wären.
Hat der abgebende Dienstherr aufgrund eines früheren Dienstherrenwechsels eine Abfindung nach diesem Staatsvertrag erhalten, so hat er diesen Betrag zuzüglich Zinsen in Höhe von 4,5 % pro Jahr ab dem Zeitpunkt des Erhalts der Zahlung neben der Abfindung nach Satz 1 an den aufnehmenden Dienstherrn zu bezahlen.
Bei Soldatinnen und Soldaten auf Zeit ist eine Abfindung nach Satz 1 unter Zugrundelegung eines Beitragssatzes in Höhe von 15 % zu zahlen.

### § 5  
Bezüge

(1) Bezüge sind die monatlichen ruhegehaltfähigen Bezüge einschließlich Sonderzahlung.

(2) Für die Ermittlung der monatlichen ruhegehaltfähigen Bezüge kommt es auf die Erfüllung von Mindestdienst- oder -bezugszeiten nicht an.

(3) Eine Sonderzahlung ist zu berücksichtigen, wenn und soweit sie der wechselnden Person im Jahr ihres Ausscheidens zusteht oder ohne Dienstherrenwechsel zustehen würde.
Sie ist als Monatsbetrag anzusetzen.

### § 6  
Dienstzeiten

(1) Dienstzeiten sind die Zeiten, die beim abgebenden Dienstherrn und bei früheren Dienstherren in einem Rechtsverhältnis der in § 2 genannten Art zurückgelegt wurden, soweit sie ruhegehaltfähig sind.
Als Dienstzeiten gelten auch die im Status einer Soldatin auf Zeit oder eines Soldaten auf Zeit zurückgelegten Zeiten.
Ausgenommen sind Zeiten in einem Beamtenverhältnis auf Widerruf sowie Zeiten, für die eine Nachversicherung durchgeführt wurde.

(2) Dem Dienstherrenwechsel unmittelbar vorangehende Abordnungszeiten beim aufnehmenden Dienstherrn sind diesem zuzurechnen, es sei denn, der aufnehmende Dienstherr hat hierfür einen Versorgungszuschlag an den abgebenden Dienstherrn entrichtet.

### § 7  
Weitere Zahlungsansprüche

(1) Liegt ein Dienstherrenwechsel ohne die Voraussetzungen des § 3 vor und hat der abgebende Dienstherr aufgrund eines früheren Dienstherrenwechsels eine Abfindung nach diesem Staatsvertrag erhalten, so hat er diesen Betrag zuzüglich Zinsen in Höhe von 4,5 % pro Jahr ab dem Zeitpunkt des Erhalts der Zahlung an den aufnehmenden Dienstherrn zu bezahlen, wenn nicht bereits eine Nachversicherung durchgeführt wurde.

(2) Hat der aufnehmende Dienstherr aufgrund eines Dienstherrenwechsels eine Abfindung erhalten und scheidet die wechselnde Person beim aufnehmenden Dienstherrn ohne Versorgungsansprüche aus, hat der aufnehmende Dienstherr dem abgebenden Dienstherrn die Kosten einer Nachversicherung zu erstatten.
Anstelle der Erstattung nach Satz 1 hat der aufnehmende Dienstherr im Falle einer nach § 4 Abs.
4 Satz 3 gezahlten Abfindung oder eines bestehenden Versorgungsanspruchs gegenüber dem abgebenden Dienstherrn die erhaltene Abfindung zuzüglich Zinsen in Höhe von 4,5 % pro Jahr ab dem Zeitpunkt des Erhalts der Zahlung an den abgebenden Dienstherrn zurückzuzahlen.

### § 8  
Dokumentationspflichten und Zahlungsmodalitäten

(1) Der zahlungspflichtige Dienstherr hat die Berechnung des Zahlungsbetrages durchzuführen und dem berechtigten Dienstherrn gegenüber nachzuweisen.

(2) Die Abfindung ist innerhalb von sechs Monaten nach Aufnahme beim neuen Dienstherrn zu leisten.
In Fällen des § 3 Abs.
4 beginnt die Frist nach Mitteilung der Aufnahme durch den neuen Dienstherrn.

(3) Die beteiligten Dienstherren können abweichende Zahlungsregelungen vereinbaren.

(4) Die Abwicklung kann auf andere Stellen übertragen werden.

Abschnitt 3  
Übergangsregelungen
---------------------------------

### § 9   
Ersetzung von § 107b BeamtVG

§ 107b BeamtVG wird durch diesen Staatsvertrag ersetzt.
Für Erstattungsansprüche, die nach dieser Vorschrift aufgrund eines Dienstherrenwechsels vor Inkrafttreten des Staatsvertrages begründet sind, gelten für die Zeit nach Inkrafttreten des Staatsvertrages ausschließlich die Regelungen der §§ 10 bis 12.

### § 10   
Laufende Erstattungen nach § 107b BeamtVG

(1) Ist in Fällen des § 9 der Versorgungsfall vor Inkrafttreten des Staatsvertrages eingetreten, besteht der Erstattungsanspruch mit folgenden Maßgaben fort:

1.  Der zuletzt vor Inkrafttreten des Staatsvertrages geleistete jährliche Erstattungsbetrag wird festgeschrieben.
2.  Der Erstattungsbetrag erhöht oder vermindert sich jeweils um die Vom-Hundert-Sätze der linearen Anpassungen der Versorgungsbezüge nach dem Recht des erstattungspflichtigen Dienstherrn.
3.  Bei Eintritt der Hinterbliebenenversorgung vermindert sich der Erstattungsbetrag auf den Betrag, der sich aus dem Vom-Hundert-Satz der Hinterbliebenenversorgung nach dem Recht des erstattungspflichtigen Dienstherrn ergibt.

(2) Die beteiligten Dienstherren unterrichten sich unverzüglich über eine Änderung erstattungsrelevanter Umstände.

### § 11  
Dienstherrenwechsel ohne laufende Erstattungen nach § 107b BeamtVG

(1) Ist in Fällen des § 9 der Versorgungsfall nicht vor Inkrafttreten des Staatsvertrages eingetreten, ist anstelle der Erstattung nach § 107b BeamtVG von dem oder den zahlungspflichtigen Dienstherren jeweils eine Abfindung an den berechtigten Dienstherrn zu leisten.

(2) Die Abfindung wird nach §§ 4 bis 6 mit folgenden Maßgaben berechnet:

1.  Abweichend von § 4 Abs.
    3 sind die Bezüge nach § 5 bis zum Inkrafttreten des Staatsvertrages entsprechend den linearen Anpassungen beim zahlungspflichtigen Dienstherrn zu dynamisieren.
2.  Liegen mehrere Dienstherrenwechsel vor, die die Voraussetzungen nach § 107b BeamtVG erfüllen, sind abweichend von § 6 die Zeiten bei anderen zahlungspflichtigen Dienstherren nicht zu berücksichtigen.
3.  Dienstzeiten bei weiteren Dienstherren, die nicht nach § 107b BeamtVG zur Erstattung verpflichtet sind, werden den zahlungspflichtigen Dienstherren und dem berechtigten Dienstherrn anteilig zugerechnet (Quotelung); die Aufteilung erfolgt nach dem Verhältnis der Zeiten, die die wechselnde Person bei den zahlungspflichtigen Dienstherren und dem berechtigten Dienstherrn abgeleistet hat; abweichend hiervon werden die Zeiten dem nachfolgenden zahlungspflichtigen Dienstherrn zugerechnet, wenn er die wechselnde Person ohne Zustimmung übernommen hat.

(3) Die Abfindung ist innerhalb von sechs Monaten nach Unterrichtung der zahlungspflichtigen Dienstherren über den Eintritt des Versorgungsfalls durch den berechtigten Dienstherrn an diesen zu zahlen.
Sie kann von jedem zahlungspflichtigen Dienstherrn vor Eintritt des Versorgungsfalls geleistet werden.
Bei Zahlung vor Eintritt des Versorgungsfalls ist im Rahmen der Quotelung für den berechtigten Dienstherrn die Zeit bis zum Erreichen der für die wechselnde Person gültigen gesetzlichen Altersgrenze nach dessen Recht anzusetzen.

(4) Der Abfindungsbetrag ist vom Zeitpunkt des Inkrafttretens des Staatsvertrages mit 4,5 % pro Jahr zu verzinsen.

(5) Die beteiligten Dienstherren unterrichten sich gegenseitig über die für die Abfindung relevanten Umstände.
§ 7 Abs.
2 sowie § 8 Abs.
1, 3 und 4 gelten entsprechend.

### § 12  
Erneuter Dienstherrenwechsel nach Inkrafttreten des Staatsvertrages

Erfolgt in Fällen des § 11 nach Inkrafttreten des Staatsvertrages ein weiterer Dienstherrenwechsel, der die Voraussetzungen des § 3 erfüllt, gilt für die nach § 107b BeamtVG erstattungspflichtigen Dienstherren § 11 mit der Maßgabe, dass die Abfindung an den aufnehmenden Dienstherrn abweichend von § 11 Abs.
3 Satz 1 innerhalb von sechs Monaten nach Unterrichtung der zahlungspflichtigen Dienstherren über den letzten Dienstherrenwechsel durch den aufnehmenden Dienstherrn an diesen zu leisten ist.
Die Berechnung der vom letzten abgebenden Dienstherrn zu leistenden Abfindung bestimmt sich nach §§ 4 bis 6 mit der Maßgabe, dass ihm abweichend von § 6 die Zeiten nicht zugerechnet werden, für die eine Abfindung nach Satz 1 geleistet wird; § 11 Abs.
2 Nr.
3 und Abs.
3 Satz 3 gilt entsprechend.

### § 13   
Quotelung ohne Erstattungspflicht nach § 107b BeamtVG

Haben vor Inkrafttreten des Staatsvertrages Dienstherrenwechsel stattgefunden, die die Voraussetzungen des § 107b BeamtVG in der jeweiligen Fassung nicht erfüllen, sind abweichend von § 6 die Zeiten, die bei den nicht erstattungspflichtigen Dienstherren abgeleistet wurden, den zur Zahlung eines Abfindungsbetrags verpflichteten Dienstherren und dem berechtigten Dienstherrn entsprechend § 11 Abs.
2 Nr.
3 und Abs.
3 Satz 3 zuzurechnen; dies gilt nicht, wenn die Erstattungspflicht nach § 107b BeamtVG an der fehlenden Zustimmung des abgebenden Dienstherrn scheiterte.
Satz 1 gilt nur für Dienstherrenwechsel, die nach Inkrafttreten des Staatsvertrages bis zum 31.
Dezember 2016 erfolgen.

### § 14   
Entsprechende Anwendung auf § 92b SVG

Die Regelungen der §§ 9 bis 13 gelten entsprechend für § 92b SVG.

### § 15   
Fortgeltung der § 107c BeamtVG und § 92c SVG

§ 107c BeamtVG und § 92c SVG in der am 31.
August 2006 geltenden Fassung finden weiter Anwendung.

Abschnitt 4   
Schlussvorschriften
----------------------------------

### § 16   
Kündigung

Dieser Staatsvertrag kann von jeder Vertragspartei zum Schluss des Kalenderjahres mit einer Frist von einem Jahr gekündigt werden.
Die Kündigung ist gegenüber dem Vorsitzenden der Ministerpräsidentenkonferenz schriftlich zu erklären, der sie unverzüglich den übrigen Vertragsparteien übermittelt.
Die Kündigung einer Partei lässt das Vertragsverhältnis unter den übrigen Parteien unberührt.

### § 17   
Inkrafttreten

(1) Dieser Staatsvertrag tritt am 1.
Januar 2011 für die Parteien in Kraft, deren Ratifikationsurkunden bis zum 30. September 2010 bei der Staats- oder Senatskanzlei des Vorsitzenden der Ministerpräsidentenkonferenz hinterlegt sind.
Für die übrigen Parteien tritt er mit Wirkung zum Beginn des dritten Folgemonats ab Hinterlegung der Ratifikationsurkunde bei der Staats- oder Senatskanzlei des Vorsitzenden der Ministerpräsidentenkonferenz in Kraft.

(2) Die Staats- oder Senatskanzlei des Vorsitzenden der Ministerpräsidentenkonferenz teilt den Parteien die Hinterlegung der Ratifikationsurkunden unverzüglich mit.

Für die Bundesrepublik Deutschland  
Berlin, den 26.
Januar 2010  
Thomas de Maizière

Für das Land Baden-Württemberg  
Berlin, den 16.
Dezember 2009  
Günther H.
Oettinger

Für den Freistaat Bayern  
Berlin, den 16.
Dezember 2009  
Horst Seehofer

Für das Land Berlin  
Berlin, den 16.
Dezember 2009  
Harald Wolf

Für das Land Brandenburg  
Berlin, den 16.
Dezember 2009  
Matthias Platzeck

Für die Freie Hansestadt Bremen  
Berlin, den 16.
Dezember 2009  
Jens Böhrnsen

Für die Freie und Hansestadt Hamburg  
Berlin, den 16.
Dezember 2009  
Ole von Beust

Für das Land Hessen  
Berlin, den 16.
Dezember 2009  
R.
Koch

Für das Land Mecklenburg-Vorpommern  
Berlin, den 16.
Dezember 2009  
Erwin Sellering

Für das Land Niedersachsen  
Berlin, den 16.
Dezember 2009  
Christian Wulff

Für das Land Nordrhein-Westfalen  
Berlin, den 16.
Dezember 2009  
Jürgen Rüttgers

Für das Land Rheinland-Pfalz  
Berlin, den 16.
Dezember 2009  
Kurt Beck

Für das Saarland  
Berlin, den 16.
Dezember 2009  
Peter Müller

Für den Freistaat Sachsen  
Berlin, den 16.
Dezember 2009  
St.
Tillich

Für das Land Sachsen-Anhalt  
Berlin, den 16.
Dezember 2009  
Böhmer

Für das Land Schleswig-Holstein  
Berlin, den 16.
Dezember 2009  
Peter Harry Carstensen

Für den Freistaat Thüringen  
Berlin, den 16.
Dezember 2009  
Ch.
Lieberknecht

[zum Gesetz](/gesetze/versorgungslastenteilungsstv_g_2010)