## Verwaltungsabkommen über die Wahrnehmung bestimmter Aufgaben nach dem Energiewirtschaftsgesetz

zwischen

der Bundesrepublik Deutschland, vertreten durch den Bundesminister für Wirtschaft und Technologie (Bund),

und

dem Land Brandenburg, vertreten durch den Ministerpräsidenten, dieser vertreten durch den Minister für Wirtschaft und Europaangelegenheiten (Land)

### Artikel 1  
(Organleihe)

(1) Der Bund stellt dem Land zur Wahrnehmung der dem Land nach § 54 Absatz 2 des Energiewirtschaftsgesetzes (EnWG) vom 7.
Juli 2005 obliegenden Verwaltungsaufgaben die Bundesnetzagentur für Elektrizität, Gas, Telekommunikation, Post und Eisenbahnen (Bundesnetzagentur) nach Maßgabe des Satzes 2 zur Verfügung (Organleihe).
Die Organleihe umfasst die Wahrnehmung der gesetzlichen Aufgaben nach § 54 Absatz 2 EnWG einschließlich aller zur Wahrnehmung der Aufgaben notwendigen Befugnisse nach Teil 8 des Energiewirtschaftsgesetzes, insbesondere Aufsichtsmaßnahmen nach § 65 EnWG, die Durchführung von Anhörungen und Ermittlungen, die Vertretung der Landesregulierungsbehörde in Beschwerde-, Rechtsbeschwerde- und Nichtzulassungsbeschwerdeverfahren, die Erhebung von Kosten, Zwangsgeldern und Bußgeldern sowie die Vollstreckung, soweit die Befugnisse nicht der Bundesnetzagentur als Bundesbehörde ausschließlich zugewiesen sind.

(2) Die Organleihe erfolgt aus verwaltungspraktischen und -ökonomischen Erwägungen zur Entlastung der Behörden des Landes.

### Artikel 2  
(Organisation)

(1) Dem für den Vollzug des Energiewirtschaftsgesetzes zuständigen Ministerium für Wirtschaft und Europaangelegenheiten des Landes Brandenburg (Aufsichtsbehörde) steht gegenüber der Bundesnetzagentur die Aufsicht über die rechtmäßige Wahrnehmung der im Rahmen der nach Artikel 1 Absatz 1 übertragenen Aufgaben zu (Rechtsaufsicht).
In Angelegenheiten allgemeiner Art oder von besonderer Bedeutung wird das Bundesministerium für Wirtschaft und Technologie durch die Aufsichtsbehörde unverzüglich durch Übermittlung einer schriftlichen Fassung der Weisung unterrichtet.

(2) Aufbau, Innere Ordnung und Personalangelegenheiten der Bundesnetzagentur bleiben Aufgabe des Bundes (Dienstaufsicht).

### Artikel 3  
(Haushalts- und Verwaltungsverfahrensrecht)

Für den nach Artikel 1 Absatz 1 übertragenen Aufgabenbereich ist das Landesrecht, insbesondere das Haushalts-, Verwaltungsgebühren- und Verwaltungsverfahrensrecht des Landes anzuwenden, soweit sich aus dem Energiewirtschaftsgesetz und den auf Grund des Energiewirtschaftsgesetzes erlassenen Rechtsverordnungen nicht etwas anderes ergibt.

### Artikel 4  
(Verwaltungskosten)

(1) Die dem Bund für die Bereitstellung der personellen und sachlichen Verwaltungsmittel entstehenden Kosten trägt das Land.

(2) Für die Wahrnehmung derjenigen Aufgaben nach Artikel 1 Absatz 1, bei denen es sich nach der Energiewirtschaftskostenverordnung des Bundes um kostenpflichtige Amtshandlungen handelt, stellt der Bund dem Land die Kosten in der Höhe in Rechnung, wie er sie bei einer Aufgabenwahrnehmung in eigener Zuständigkeit gegenüber dem jeweiligen Kostenschuldner auf der Grundlage der Energiewirtschaftskostenverordnung des Bundes in der jeweils geltenden Fassung festgesetzt hätte.
Fälle der Uneinbringbarkeit der Kosten oder einer Ermäßigung der Kosten gegenüber dem Kostenschuldner aus Billigkeitsgründen mindern den Anspruch des Bundes nicht.

(3) Für die Abrechnung der Kosten für die Wahrnehmung derjenigen Aufgaben nach Artikel 1 Absatz 1, die nicht nach der Energiewirtschaftskostenverordnung des Bundes kostenpflichtig sind, finden die folgenden Kostensätze Anwendung:

1.  für die Überwachung eines Energieversorgungsunternehmens, an dessen Elektrizitätsverteilernetz weniger als 10 000 Kunden unmittelbar oder mittelbar angeschlossen sind, 1 500 Euro pro Jahr,
2.  für die Überwachung eines Energieversorgungsunternehmens, an dessen Elektrizitätsverteilernetz mindestens 10 000, jedoch weniger als 100 000 Kunden unmittelbar oder mittelbar angeschlossen sind, 3 000 Euro pro Jahr,
3.  für die Überwachung eines Energieversorgungsunternehmens nach Nr. 1 und 2, welches Teil eines vertikal integrierten Energieversorgungsunternehmens nach § 3 Nr. 38 EnWG ist, auf welches die Regelungen des Teils 2 des Energiewirtschaftsgesetzes unbeschränkt Anwendung finden, 4 700 Euro pro Jahr.

Satz 1 gilt für die Überwachung von Gasverteilernetzen entsprechend.

(4) Das Land leistet vierteljährliche Abschlagszahlungen der Kosten nach Absatz 3.
Die quartalsweise zu leistenden Beträge erfolgen bis zum 5.
Werktag des darauf folgenden Monats.
Mehr- oder Minderbeträge, die sich aus der jährlichen Endabrechnung ergeben, werden mit der Abschlagszahlung für das 3.
Quartal des Folgejahres ausgeglichen.
Die Kosten nach Absatz 2 werden dem Land jeweils zum Ende eines Quartals in Rechnung gestellt.
Die vom Land zu leistenden Beträge sind ab dem Zeitpunkt, in dem das Land mit der Zahlung in Verzug ist, mit fünf Prozentpunkten über dem Basiszinssatz jährlich zu verzinsen.

(5) Die von der Bundesnetzagentur im Zusammenhang mit der Aufgabendurchführung nach Artikel 1 Absatz 1 erhobenen Einnahmen werden jeweils zum Ende des Quartals an das Land abgeführt.

### Artikel 5  
(Inkrafttreten und Geltungsdauer)

(1) Dieses Verwaltungsabkommen tritt am Tage nach der Veröffentlichung im Gesetz- und Verordnungsblatt für das Land Brandenburg (Teil I - Gesetze) in Kraft.
Gleichzeitig tritt das Verwaltungsabkommen über die Wahrnehmung bestimmter Aufgaben nach dem Energiewirtschaftsgesetz vom 22.
Februar 2011 (GVBl.
I Nr. 8) außer Kraft.

(2) Die Bundesnetzagentur überprüft die Angemessenheit der Kostensätze nach Artikel 4 Absatz 3 Satz 1 und 2 anhand ihrer Kosten- und Leistungsrechnung unter Zugrundelegung ihrer Vollkostenrechnung und legt bis zum 31. März 2016 einen Vorschlag für eine Anpassung der Kostensätze vor, soweit dies angemessen ist.

(3) Das Verwaltungsabkommen kann jährlich zum 31.
Dezember gekündigt werden.
Die Kündigung bedarf der Schriftform.
Voraussetzung einer Kündigung nach Satz 1 ist, dass diese dem Vertragspartner mindestens sechs Monate vor Ablauf der Frist nach Satz 1 zugeht.

Berlin, den 09.12.2013

Der Bundesminister für Wirtschaft und Technologie  
In Vertretung

Stefan Kapferer

Potsdam, den 27.11.2013

Der Minister für Wirtschaft und Europaangelegenheiten

Ralf Christoffers

[zum Gesetz](/gesetze/vwabkenergiewg_g_2014)