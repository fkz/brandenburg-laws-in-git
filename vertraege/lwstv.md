## Staatsvertrag der Länder Berlin und Brandenburg auf dem Gebiet der Landwirtschaft (Landwirtschaftsstaatsvertrag - LwStV)

Das Land Berlin, vertreten durch den Regierenden Bürgermeister und das Land Brandenburg, vertreten durch den Ministerpräsidenten, schließen folgenden Staatsvertrag:

**Inhaltsübersicht**

**Präambel**

### Erster Abschnitt  
Übertragung von Aufgaben im Bereich der Fonds EGFL, ELER und EMFF

[Artikel 1 Aufgabenübertragung von Berlin auf Brandenburg](#1)  
[Artikel 2 Zuständige Behörde, Bescheinigende Stelle, EU-Zahlstelle und Verwaltungsbehörde; Behörden des EMFF](#2)  
[Artikel 3 Finanzkorrekturen der Europäischen Union (Anlastungen)](#3)  
[Artikel 4 Verpflichtungen im Bereich des ELER](#4)  
[Artikel 5 Durchführung von Kontrollen zur Einhaltung von Cross-Compliance](#5)

### Zweiter Abschnitt  
Übertragung von Aufgaben für weitere Fördermaßnahmen und von Ordnungsaufgaben

[Artikel 6 Übertragung von Aufgaben für weitere Fördermaßnahmen](#6)  
[Artikel 7 Übertragung von Ordnungsaufgaben](#7)

### Dritter Abschnitt  
Allgemeine Regelungen

[Artikel 8 Delegation; Geltendmachung von Rechten](#8)  
[Artikel 9 Amtshandlungen](#9)  
[Artikel 10 Anzuwendendes Recht](#10)  
[Artikel 11 Länderübergreifende Zusammenarbeit](#11)  
[Artikel 12 Datenschutz und Akteneinsicht](#12)  
[Artikel 13 Haushalt](#13)  
[Artikel 14 Finanzkontrolle](#14)  
[Artikel 15 Verwaltungsvereinbarungen](#15)  
[Artikel 16 Fortentwicklung des Staatsvertrages](#16)  
[Artikel 17 Finanzieller Ausgleich](#17)  
[Artikel 18 Geltungsdauer, Kündigung und salvatorische Klausel](#18)  
[Artikel 19 Inkrafttreten](#19)

**Präambel**

Die Länder Berlin und Brandenburg bilden auf dem Gebiet der Landwirtschaft und der Entwicklung des ländlichen Raums eine Region mit engen Verflechtungen bei der landwirtschaftlichen Produktion und beim Absatz landwirtschaftlicher Produkte.
Mit dem Ziel, durch die Bündelung von Verwaltungsaufgaben den Aufwand für die Landwirtschaftsverwaltung in der gesamten Region effizienter zu gestalten, kommen die Länder Berlin und Brandenburg überein, den nachfolgenden Vertrag über die Zusammenarbeit auf dem Gebiet der Landwirtschaft und der Entwicklung des ländlichen Raums zu schließen:

### Erster Abschnitt  
Übertragung von Aufgaben im Bereich der Fonds EGFL, ELER und EMFF

#### Artikel 1  
Aufgabenübertragung von Berlin auf Brandenburg

(1) Das Land Berlin überträgt dem Land Brandenburg alle Aufgaben im Zusammenhang mit der Planung und Durchführung der Förderprogramme und Beihilfen im Rahmen des Europäischen Garantiefonds für die Landwirtschaft (EGFL), des Europäischen Landwirtschaftsfonds für die Entwicklung des ländlichen Raums (ELER) und des Europäischen Meeres- und Fischereifonds (EMFF).
Diese Aufgabenübertragung umfasst auch die Planung und Durchführung von Sonderstützungsmaßnahmen.
Den in Bezug auf die übertragenen Aufgaben erlassenen Verordnungen der Europäischen Union in der jeweils gültigen Fassung, Leitlinien und Arbeitspapieren der Kommission sowie nationalen Vorschriften einschließlich Verwaltungsvorschriften ist dabei ebenso Rechnung zu tragen wie etwaigen Programmen, die sich auf weitere Förderperioden beziehen.

(2) Die Programmplanung im Rahmen der Fonds für die Förderperioden der Europäischen Union ab 2021 wird von der für Landwirtschaft zuständigen obersten Landesbehörde des Landes Brandenburg im Benehmen mit der für Landwirtschaft zuständigen Senatsverwaltung des Landes Berlin erstellt.
Das Land Berlin unterbreitet dem Land Brandenburg die inhaltlichen Vorschläge für die Maßnahmen im Rahmen des ELER für das Gebiet des Landes Berlin.
Die Förderung erfolgt auf der Grundlage des gemeinsamen Entwicklungsprogramms für die Entwicklung des ländlichen Raums unter Berücksichtigung länderspezifischer Belange beziehungsweise auf der Grundlage etwaiger Folgeprogramme und Folgepläne.

(3) Das Land Berlin stellt dem Land Brandenburg für die Durchführung der Aufgaben nach Artikel 1 Absatz 1 Mittel zur Kofinanzierung für Maßnahmen im Land Berlin nach Maßgabe des jeweiligen Haushaltsplans rechtzeitig zur Verfügung; der finanzielle Ausgleich nach Artikel 17 bleibt davon unberührt.

#### Artikel 2  
Zuständige Behörde, Bescheinigende Stelle, EU-Zahlstelle und Verwaltungsbehörde; Behörden des EMFF

(1) Die zuständige Behörde des Landes Brandenburg gemäß Artikel 1 und 2 der Durchführungsverordnung (EU) Nr. 908/2014 der Kommission vom 6.
August 2014 mit Durchführungsbestimmungen zur Verordnung (EU) Nr. 1306/2013 des Europäischen Parlaments und des Rates hinsichtlich der Zahlstellen und anderen Einrichtungen, der Mittelverwaltung, des Rechnungsabschlusses und der Bestimmungen für Kontrollen, Sicherheiten und Transparenz (ABl. L 255 vom 28.8.2014, S. 59) oder einer entsprechenden Nachfolgeverordnung lässt die EU-Zahlstelle Brandenburg-Berlin zu und überprüft die Zulassung im Rahmen der ständigen Aufsicht.

(2) Die zuständige Behörde des Landes Brandenburg benennt die Bescheinigende Stelle im Sinne der Verordnung (EU) Nr.
1306/2013 des Europäischen Parlaments und des Rates vom 17.
Dezember 2013 über die Finanzierung, die Verwaltung und das Kontrollsystem der Gemeinsamen Agrarpolitik und zur Aufhebung der Verordnungen (EWG) Nr. 352/78, (EG) Nr.
165/94, (EG) Nr.
2799/98, (EG) Nr. 814/2000, (EG) Nr.
1290/2005 und (EG) Nr.
485/2008 des Rates (ABl.
L 347 vom 20.12.2013, S. 549) oder einer entsprechenden Nachfolgeverordnung.

(3) EU-Zahlstelle für die Bereiche des EGFL und des ELER für das Land Berlin und das Land Brandenburg ist die EU-Zahlstelle des Landes Brandenburg.
Sie führt die Bezeichnung EU-Zahlstelle Brandenburg-Berlin.

(4) Alle für die Bereiche des EGFL und des ELER ab dem 16.
Oktober 2020 vorzunehmenden Zahlungen des Landes Berlin und des Landes Brandenburg werden über die EU-Zahlstelle Brandenburg-Berlin abgewickelt.
Dies gilt auch für die vorzunehmenden Zahlungen im Bereich der Sonderstützungsmaßnahmen.
Die Jahresrechnungen für die Region Brandenburg-Berlin werden von der EU-Zahlstelle Brandenburg-Berlin erstellt.

(5) Zuständige Verwaltungsbehörde nach Artikel 65 der Verordnung (EU) Nr.
1305/2013 des Europäischen Parlaments und des Rates vom 17.
Dezember 2013 über die Förderung der ländlichen Entwicklung durch den Europäischen Landwirtschaftsfonds für die Entwicklung des ländlichen Raums (ELER) und zur Aufhebung der Verordnung (EG) Nr. 1698/2005 (ABl.
L 347 vom 20.12.2013, S. 487) oder einer entsprechenden Nachfolgeverordnung für den Bereich des ELER für das Land Berlin ist die für den ELER zuständige Verwaltungsbehörde des Landes Brandenburg.

(6) Zuständige Behörden im Sinne der Verordnung (EU) Nr.
508/2014 des Europäischen Parlaments und des Rates vom 15.
Mai 2014 über den Europäischen Meeres- und Fischereifonds und zur Aufhebung der Verordnungen (EG) Nr. 2328/2003, (EG) Nr.
861/2006, (EG) Nr.
1198/2006 und (EG) Nr. 791/2007 des Rates und der Verordnung (EU) Nr. 1255/2011 des Europäischen Parlaments und des Rates (ABl.
L 149 vom 20.5.2014, S.
1) und der Verordnung (EU) Nr.
1303/2013 des Europäischen Parlaments und des Rates vom 17.
Dezember 2013 mit gemeinsamen Bestimmungen über den Europäischen Fonds für regionale Entwicklung, den Europäischen Sozialfonds, den Kohäsionsfonds, den Europäischen Landwirtschaftsfonds für die Entwicklung des ländlichen Raums und den Europäischen Meeres- und Fischereifonds sowie mit allgemeinen Bestimmungen über den Europäischen Fonds für regionale Entwicklung, den Europäischen Sozialfonds, den Kohäsionsfonds und den Europäischen Meeres- und Fischereifonds und zur Aufhebung der Verordnung (EG) Nr. 1083/2006 des Rates (ABl.
L 347 vom 20.12.2013, S.
320) oder einer entsprechenden Nachfolgeverordnung für den Bereich des EMFF für das Land Berlin sind die für den EMFF zuständigen Verwaltungs-, Bescheinigungs- und Prüfbehörden des Landes Brandenburg.

#### Artikel 3  
Finanzkorrekturen der Europäischen Union (Anlastungen)

Anlastungen durch die Europäische Union werden von den vertragsschließenden Ländern gemeinsam getragen und zwar im Verhältnis der an die Berliner und brandenburgischen Begünstigten ausgezahlten Beihilfen.
Das Verhältnis wird entsprechend der angelasteten Haushaltslinien an die Berliner und brandenburgischen Begünstigten ausgezahlten Beträge ermittelt.
Anlastungen, die nach Artikel 104a Absatz 6 des Grundgesetzes in der jeweils geltenden Fassung von Bund und Ländern gemeinsam zu tragen sind, bleiben hiervon unberührt.
In Anwendungsfällen des Artikels 104a Absatz 6 des Grundgesetzes ermittelt die EU-Zahlstelle Brandenburg-Berlin die von Berliner und brandenburgischen Begünstigten erhaltenen Mittel getrennt je Land und jedes Land trägt die auf es selbst entfallenden Finanzkorrekturen wie gemäß Artikel 104a Absatz 6 des Grundgesetzes vorgesehen.

#### Artikel 4  
Verpflichtungen im Bereich des ELER

Für die Einhaltung von Verpflichtungen im Bereich des ELER, die in dem jeweiligen Entwicklungsprogramm für die Entwicklung des ländlichen Raums und etwaigen Folgeprogrammen und Folgeplänen festgeschrieben sind, sowie für das Stellen von Änderungsanträgen für das Entwicklungsprogramm ist auch für das Land Berlin die für den ELER zuständige Verwaltungsbehörde des Landes Brandenburg zuständig.

#### Artikel 5  
Durchführung von Kontrollen zur Einhaltung von Cross-Compliance

(1) Die Durchführung der von der Europäischen Kommission geforderten Vor-Ort-Kontrollen einschließlich der Auswahl der Kontrollstichproben sowie der Berichterstattung zur Umsetzung von Cross-Compliance-Vorschriften beziehungsweise etwaigen Folgevorschriften erfolgt für die Berliner Begünstigten durch die jeweils zuständigen Behörden des Landes Brandenburg, soweit diesbezüglich keine anderen Regelungen getroffen worden sind.
Zentrale Ansprech- und Koordinierungsstelle ist die EU-Zahlstelle Brandenburg-Berlin.

(2) Die Aufgaben der zuständigen Kontrollbehörde nach den Artikeln 67 und 68 der Durchführungsverordnung (EU) Nr.
809/2014 der Kommission vom 17.
Juli 2014 mit Durchführungsbestimmungen zur Verordnung (EU) Nr.
1306/2013 des Europäischen Parlaments und des Rates hinsichtlich des integrierten Verwaltungs- und Kontrollsystems, der Maßnahmen zur Entwicklung des ländlichen Raums und der Cross-Compliance (ABl.
L 227 vom 31.7.2014, S.
69) oder einer entsprechenden Nachfolgeverordnung (Durchführung der „systematischen“ Kontrollen) werden bei den Berliner Begünstigten nach Maßgabe des jeweiligen Landeszuständigkeitsrechts hinsichtlich der Grundanforderungen an die Betriebsführung und Standards für die Erhaltung von Flächen in einem guten landwirtschaftlichen und ökologischen Zustand nach den Artikeln 93 und 94 in Verbindung mit Anhang II der Verordnung (EU) Nr.
1306/2013 oder einer entsprechenden Nachfolgeverordnung in Bezug auf Lebensmittel- und Futtermittelsicherheit, zur Tierhaltung und zum Tierschutz, Pflanzen- und Wasserschutz sowie zum FFH- und Vogelschutz von den Berliner Behörden, im Übrigen von den Behörden des Landes Brandenburg, wahrgenommen.
Gleiches gilt für die Durchführung anlassbezogener Kontrollen bei den Berliner Begünstigten.

### Zweiter Abschnitt  
Übertragung von Aufgaben für weitere Fördermaßnahmen und von Ordnungsaufgaben

#### Artikel 6  
Übertragung von Aufgaben für weitere Fördermaßnahmen

(1) Das Land Berlin überträgt dem Land Brandenburg die Planung, Durchführung, Abrechnung und Berichterstattung der nationalen Fördermaßnahmen im Rahmen der Gemeinschaftsaufgabe „Verbesserung der Agrarstruktur und des Küstenschutzes“ (GAK).

(2) Das Land Berlin stellt dem Land Brandenburg für die Durchführung der Fördermaßnahmen nach Absatz 1 Mittel zur Kofinanzierung für Maßnahmen im Land Berlin nach Maßgabe des jeweiligen Haushaltsplans rechtzeitig zur Verfügung; der finanzielle Ausgleich nach Artikel 17 bleibt davon unberührt.

(3) Weitere durch Bundes- oder Landesmittel oder durch beide zugleich finanzierte Fördermaßnahmen, Zuwendungen und Billigkeitsleistungen können durch Verwaltungsvereinbarung gemäß Artikel 15 übertragen werden.

#### Artikel 7  
Übertragung von Ordnungsaufgaben

Das Land Berlin überträgt dem Land Brandenburg die Erfüllung der Ordnungsaufgaben nach

1.  dem Saatgutverkehrsgesetz in der Fassung der Bekanntmachung vom 16.
    Juli 2004 (BGBl.
    I S.
    1673), das zuletzt durch Artikel 1 des Gesetzes vom 20.
    Dezember 2016 (BGBl.
    I S.
    3041) geändert worden ist, in der jeweils geltenden Fassung,
2.  dem Sortenschutzgesetz in der Fassung der Bekanntmachung vom 19.
    Dezember 1997 (BGBl.
    I S.
    3164), das zuletzt durch Artikel 6 Absatz 37 des Gesetzes vom 13.
    April 2017 (BGBl.
    I S.
    872) geändert worden ist, in der jeweils geltenden Fassung,
3.  dem Düngegesetz vom 9.
    Januar 2009 (BGBl.
    I S.
    54, 136), das zuletzt durch Artikel 1 des Gesetzes vom 5. Mai 2017 (BGBl.
    I S.
    1068) geändert worden ist, in der jeweils geltenden Fassung,
4.  dem Milch- und Fettgesetz in der im Bundesgesetzblatt Teil III, Gliederungsnummer 7842-1, veröffentlichten bereinigten Fassung, das zuletzt durch Artikel 397 der Verordnung vom 31. August 2015 (BGBl.
    I S.
    1474) geändert worden ist, in der jeweils geltenden Fassung,
5.  dem Milch- und Margarinegesetz vom 25.
    Juli 1990 (BGBl.
    I S.
    1471), das zuletzt durch Artikel 2 des Gesetzes vom 18.
    Januar 2019 (BGBl.
    I S.
    33) geändert worden ist, in der jeweils geltenden Fassung,
6.  dem Fleischgesetz vom 9.
    April 2008 (BGBl.
    I S.
    714, 1025), das zuletzt durch Artikel 102 des Gesetzes vom 20. November 2019 (BGBl.
    I S.
    1626) geändert worden ist, in der jeweils geltenden Fassung,
7.  dem Tierzuchtgesetz vom 18.
    Januar 2019 (BGBl.
    I S.
    18) in der jeweils geltenden Fassung,
8.  dem Flurbereinigungsgesetz in der Fassung der Bekanntmachung vom 16.
    März 1976 (BGBl.
    I S.
    546), das zuletzt durch Artikel 17 des Gesetzes vom 19.
    Dezember 2008 (BGBl.
    I S.
    2794) geändert worden ist, in der jeweils geltenden Fassung und
9.  dem Landwirtschaftsanpassungsgesetz in der Fassung der Bekanntmachung vom 3.
    Juli 1991 (BGBl.
    I S.
    1418), das zuletzt durch Artikel 40 des Gesetzes vom 23.
    Juli 2013 (BGBl. I S. 2586) geändert worden ist, in der jeweils geltenden Fassung

sowie für die Aufgaben nach den auf Grund dieser Gesetze erlassenen Rechtsverordnungen.
Satz 1 gilt nicht für die den Berliner Bezirken zum Zeitpunkt der Unterzeichnung dieses Staatsvertrages zugewiesenen Aufgaben.

### Dritter Abschnitt  
Allgemeine Regelungen

#### Artikel 8  
Delegation; Geltendmachung von Rechten

(1) Die für Landwirtschaft zuständige oberste Landesbehörde des Landes Brandenburg ist berechtigt, nach Herstellung des Benehmens mit der für Landwirtschaft zuständigen Senatsverwaltung des Landes Berlin die Wahrnehmung der mit diesem Staatsvertrag für das Land Berlin übernommenen Aufgaben durch Rechtsverordnung auf andere Behörden des Landes Brandenburg zu übertragen.

(2) Zur Wahrnehmung der übertragenen Aufgaben ermächtigt das Land Berlin das Land Brandenburg einschließlich der zuständigen Behörden des Landes Brandenburg, jegliche Rechte und Ansprüche im Zusammenhang mit den übertragenen Aufgaben einschließlich einer eventuell erforderlichen Prozessführung im eigenen Namen geltend zu machen.

#### Artikel 9  
Amtshandlungen

Die Bediensteten der Behörden des Landes Brandenburg sind berechtigt, zur Wahrnehmung der mit diesem Staatsvertrag auf das Land Brandenburg übertragenen Aufgaben, Amtshandlungen im Land Berlin vorzunehmen.

#### Artikel 10  
Anzuwendendes Recht

Für die Wahrnehmung der im Rahmen dieses Staatsvertrages übertragenen Aufgaben gilt das Recht des Landes Brandenburg, soweit nicht Unionsrecht oder Bundesrecht vorgehen.

#### Artikel 11  
Länderübergreifende Zusammenarbeit

Die Behörden der vertragschließenden Länder sind zur gegenseitigen Unterstützung bei der Durchführung dieses Staatsvertrages verpflichtet.
Die Unterstützung beinhaltet für die gemäß Artikel 1, Artikel 6 und Artikel 7 übertragenen Aufgaben die jederzeitige Erteilung von Auskünften, die gegenseitige Unterrichtung, die Übermittlung von Erkenntnissen sowie die Erhebung, Aufbereitung und Bereitstellung statistischer Daten.

#### Artikel 12  
Datenschutz und Akteneinsicht

(1) Für die Verarbeitung einschließlich der Erhebung personenbezogener Daten und die Akteneinsicht gilt das Recht des Landes Brandenburg, soweit nicht Unionsrecht oder Bundesrecht vorgehen.

(2) Die oder der Landesbeauftragte für den Datenschutz und für das Recht auf Akteneinsicht Brandenburg überwacht im Benehmen mit der oder dem Berliner Beauftragten für Datenschutz und Informationsfreiheit die Einhaltung der Bestimmungen zum Datenschutz und zur Akteneinsicht.

#### Artikel 13  
Haushalt

Die vertragschließenden Länder verpflichten sich, jeweils rechtzeitig die Haushaltsvoraussetzungen für die Durchführung dieses Staatsvertrages zu schaffen.
Die für das jeweilige Land zur Verfügung gestellten Mittel stehen allein für Maßnahmen in dem jeweiligen Land zur Verfügung.

#### Artikel 14  
Finanzkontrolle

Die Rechnungshöfe der vertragschließenden Länder sind berechtigt, die Haushalts- und Wirtschaftsführung der zuständigen Behörden im Rahmen der Durchführung dieses Vertrages zu prüfen.
Sie sollen Prüfvereinbarungen auf der Grundlage des § 93 der Landeshaushaltsordnungen treffen.
Die Prüfungsrechte der Kommission, des Europäischen Rechnungshofes und des Bundesrechnungshofes bleiben unberührt.

#### Artikel 15  
Verwaltungsvereinbarungen

Die für Landwirtschaft zuständigen Mitglieder der Landesregierungen der vertragschließenden Länder regeln das Nähere zur Durchführung dieses Staatsvertrages durch Verwaltungsvereinbarungen.

#### Artikel 16  
Fortentwicklung des Staatsvertrages

Die vertragschließenden Länder verpflichten sich, insbesondere im Hinblick auf die Fortentwicklung des einschlägigen Unions- und Bundesrechts, erforderliche Änderungen des Staatsvertrages herbeizuführen.

#### Artikel 17  
Finanzieller Ausgleich

(1) Das Land Berlin zahlt an das Land Brandenburg jährlich zum 16.
Oktober eines Jahres einen finanziellen Ausgleich für den Aufwand infolge der Übernahme der gemäß Artikel 1, Artikel 6 und Artikel 7 übertragenen Aufgaben.
Die Höhe des finanziellen Ausgleichs wird durch Verwaltungsvereinbarung gemäß Artikel 15 geregelt.

(2) Die Höhe des vereinbarten finanziellen Ausgleichs soll mindestens alle drei Jahre überprüft und bei Bedarf einvernehmlich durch Änderung der Verwaltungsvereinbarung gemäß Artikel 15 neu festgelegt werden.
Abweichend von Satz 1 soll die Höhe des vereinbarten finanziellen Ausgleichs hinsichtlich des Aufwands für die Übernahme von Ordnungsaufgaben gemäß Artikel 7 erstmalig bereits nach Ablauf von 18 Monaten nach Inkrafttreten dieses Staatsvertrages überprüft werden.

(3) Sind über die gemäß Artikel 1, Artikel 6 und Artikel 7 übertragenen Aufgaben hinaus neue Aufgaben durch Einzel-Maßnahmen, Zuwendungen, Billigkeitsleistungen oder Sonderstützungsmaßnahmen von Behörden des Landes Brandenburg abzuwickeln, die einen erhöhten, zusätzlichen Personalaufwand nach sich ziehen, vereinbaren die vertragsschließenden Länder für die betreffenden Jahre ab Übernahme der hinzukommenden Aufgaben über den finanziellen Ausgleich hinaus einen zusätzlichen Betrag und legen ihn in der Verwaltungsvereinbarung gemäß Artikel 15 fest.

#### Artikel 18  
Geltungsdauer, Kündigung und salvatorische Klausel

(1) Dieser Staatsvertrag gilt für unbestimmte Zeit.

(2) Dieser Staatsvertrag kann von jedem vertragschließenden Land mit einer Frist von drei Jahren zum Ende eines EU-Haushaltsjahres gekündigt werden.
Die Kündigung bedarf der Schriftform.
Sofern die Kündigung zeitlich nicht mit dem Ende einer Förderperiode zusammenfällt, kann sie nur im vorherigen Benehmen mit der Europäischen Kommission erfolgen.

(3) Sollten einzelne Bestimmungen dieses Staatsvertrages unwirksam sein oder werden, so berührt dies nicht die Gültigkeit seiner übrigen Bestimmungen.
Die vertragsschließenden Länder verpflichten sich, unwirksame Bestimmungen durch neue Bestimmungen zu ersetzen, die den Regelungszielen der unwirksamen Bestimmungen in rechtlich zulässiger Weise gerecht werden.
Entsprechendes gilt für im Staatsvertrag enthaltene Regelungslücken.

#### Artikel 19  
Inkrafttreten

Dieser Vertrag bedarf der Ratifikation und tritt am 16.
Oktober 2020 in Kraft.

Berlin, den 10.
Juli 2020

Für das Land Berlin:  
Der Regierende Bürgermeister

Michael Müller

Potsdam, den 3.
August 2020

Für das Land Brandenburg:  
Der Ministerpräsident

Dr.
Dietmar Woidke

[zum Gesetz](/gesetze/lw_stv)