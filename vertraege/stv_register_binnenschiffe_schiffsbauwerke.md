## Staatsvertrag zwischen dem Land Brandenburg und der Freien und Hansestadt Hamburg über die Führung des Registers für Binnenschiffe und des Registers für Schiffsbauwerke

Das Land Brandenburg,

vertreten durch den Ministerpräsidenten

dieser vertreten durch die Ministerin der Justiz,

und die Freie und Hansestadt Hamburg, vertreten durch den Senat,

schließen vorbehaltlich der Zustimmung ihrer verfassungsmäßig zuständigen Organe nachstehenden Staatsvertrag:

### Artikel 1

(1) Die Führung des Registers für Binnenschiffe im Sinne des § 3 Absatz 3 der Schiffsregisterordnung in der Fassung der Bekanntmachung vom 26.
Mai 1994 (BGBl.
I S. 1133), die zuletzt durch Artikel 17 des Gesetzes vom 20.
November 2019 (BGBl.
I S.
1724) geändert worden ist, und des Registers für Schiffsbauwerke im Sinne der §§ 65 bis 74 der Schiffsregisterordnung (im Folgenden: das Binnenschiffs- und das Schiffsbauwerkeregister) wird für das Gebiet des Landes Brandenburg dem Amtsgericht Hamburg übertragen.

(2) Das Binnenschiffs- und das Schiffsbauwerkeregister werden nach den in der Freien und Hansestadt Hamburg geltenden Bestimmungen geführt.

### Artikel 2

(1) Das Binnenschiffs- und das Schiffsbauwerkeregister werden beim Amtsgericht Hamburg in maschineller Form als automatisiertes Dateisystem geführt.

(2) Das Amtsgericht Hamburg ist für sämtliche unerledigte Anträge und Verfahren beim Binnenschiffs- und beim Schiffsbauwerkeregister des Landes Brandenburg ab Inkrafttreten dieses Staatsvertrags gemäß Artikel 6 Absatz 2 Satz 1 zuständig.

(3) Die Abwicklung der Übertragung richtet sich nach den §§ 12, 12a der Verordnung zur Durchführung der Schiffsregisterordnung (SchRegDV) in der Fassung der Bekanntmachung vom 30. November 1994 (BGBl.1994 I S.
3631, 1995 I S.
249), die zuletzt durch Artikel 18 des Gesetzes vom 20. November 2019 (BGBl. I S.
1724) geändert worden ist.
Die bis zum Inkrafttreten dieses Staatsvertrags geschlossenen Registerblätter und die dazugehörigen Registerakten verbleiben bei dem Amtsgericht Brandenburg an der Havel.

(4) Bei dem Amtsgericht Hamburg werden die übertragenen Registerblätter gemäß § 59 SchRegDV in Verbindung mit der Verordnung über die Einführung des maschinell geführten Schiffsregisters vom 22. Januar 2020 (HmbGVBl. S. 82) in der jeweils geltenden Fassung durch Umschreibung, Neufassung oder Umstellung in das maschinelle Schiffsregister überführt.

### Artikel 3

Das Land Brandenburg verpflichtet sich, darauf hinzuwirken, dass ab der Unterzeichnung dieses Staatsvertrags und bis zur Übertragung der Register die Verfahren nach § 22 der Schiffsregisterordnung (Löschung von Amts wegen) nach Möglichkeit vorrangig betrieben werden.

### Artikel 4

Das Land Brandenburg und die Freie und Hansestadt Hamburg verzichten gegenseitig auf Kostenausgleichsansprüche.
Das gilt auch für den Ausgleich von Verwaltungs- und IT-Kosten.
Die Freie und Hansestadt Hamburg erhält die Einnahmen aus den dem Amtsgericht Hamburg übertragenen Angelegenheiten einschließlich der übertragenen Anträge und Verfahren, die zum Zeitpunkt des Inkrafttretens dieses Staatsvertrags noch nicht abgeschlossen sind.

### Artikel 5

(1) Dieser Staatsvertrag gilt ab Inkrafttreten zunächst für fünf Jahre.

(2) Danach verlängert er sich jeweils automatisch um vier Jahre, wenn er nicht von einem der Vertragspartner mit einer Frist von einem Jahr schriftlich gekündigt wird.

(3) Soweit die Parteien keine abweichende Vereinbarung treffen, richtet sich die Rückübertragung des Binnenschiffs- und des Schiffsbauwerkeregisters bei einer Kündigung oder anderweitigen Beendigung dieses Staatsvertrags nach den zu diesem Zeitpunkt geltenden gesetzlichen oder durch den Verordnungsgeber normierten Vorgaben für die Übertragung.
Artikel 2 Absatz 3 Satz 2 gilt entsprechend.

### Artikel 6

(1) Dieser Staatsvertrag bedarf der Ratifikation.

(2) Er tritt am ersten Tag des zweiten Monats in Kraft, der auf den Monat folgt, in dem die Ratifikationsurkunden ausgetauscht worden sind.
Jede Partei bestätigt der anderen Partei unverzüglich schriftlich das Datum des Eingangs der Ratifikationsurkunde.

9.
März 2021

Für das Land Brandenburg  
Der Ministerpräsident

vertreten durch die Ministerin der Justiz                         Susanne Hoffmann

26.
Februar 2021

Für den Senat der Freien und Hansestadt Hamburg       Anna Gallina  
                                                                                 Senatorin für Justiz und Verbraucherschutz

[zum Gesetz](/de/gesetze-251792)