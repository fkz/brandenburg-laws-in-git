## Erster Staatsvertrag zur Änderung des Staatsvertrages zum Glücksspielwesen in Deutschland  (Erster Glücksspieländerungsstaatsvertrag - Erster GlüÄndStV)

Das Land Baden-Württemberg,  
der Freistaat Bayern,  
das Land Berlin,  
das Land Brandenburg,  
die Freie Hansestadt Bremen,  
die Freie und Hansestadt Hamburg,  
das Land Hessen,  
das Land Mecklenburg-Vorpommern,  
das Land Niedersachsen,  
das Land Nordrhein-Westfalen,  
das Land Rheinland-Pfalz,  
das Saarland,  
der Freistaat Sachsen,  
das Land Sachsen-Anhalt und  
der Freistaat Thüringen

schließen nachstehenden Staatsvertrag:

### Artikel 1  
Staatsvertrag zum Glückspielswesen in Deutschland  
(Glücksspielstaatsvertrag - GlüStV)

#### Erster Abschnitt  
Allgemeine Vorschriften

##### § 1  
Ziele des Staatsvertrages

Ziele des Staatsvertrages sind gleichrangig

1.  das Entstehen von Glücksspielsucht und Wettsucht zu verhindern und die Voraussetzungen für eine wirksame Suchtbekämpfung zu schaffen,
2.  durch ein begrenztes, eine geeignete Alternative zum nicht erlaubten Glücksspiel darstellendes Glücksspielangebot den natürlichen Spieltrieb der Bevölkerung in geordnete und überwachte Bahnen zu lenken sowie der Entwicklung und Ausbreitung von unerlaubten Glücksspielen in Schwarzmärkten entgegenzuwirken,
3.  den Jugend- und den Spielerschutz zu gewährleisten,
4.  sicherzustellen, dass Glücksspiele ordnungsgemäß durchgeführt, die Spieler vor betrügerischen Machenschaften geschützt, die mit Glücksspielen verbundene Folge- und Begleitkriminalität abgewehrt werden und
5.  Gefahren für die Integrität des sportlichen Wettbewerbs beim Veranstalten und Vermitteln von Sportwetten vorzubeugen.

Um diese Ziele zu erreichen, sind differenzierte Maßnahmen für die einzelnen Glücksspielformen vorgesehen, um deren spezifischen Sucht-, Betrugs-, Manipulations- und Kriminalitätsgefährdungspotentialen Rechnung zu tragen.

##### § 2  
Anwendungsbereich

(1) Die Länder regeln mit diesem Staatsvertrag die Veranstaltung, die Durchführung und die Vermittlung von öffentlichen Glücksspielen.

(2) Für Spielbanken gelten nur die §§ 1 bis 3, 4 Abs. 1 bis 4, §§ 5 bis 8, 20 und 23 sowie die Vorschriften des Neunten Abschnitts.

(3) Für Spielhallen, soweit sie Geld- oder Warenspielgeräte mit Gewinnmöglichkeit bereithalten, gelten nur die §§ 1 bis 3, 4 Abs. 1, 3 und 4, §§ 5 bis 7 sowie die Vorschriften des Siebten und Neunten Abschnitts.
Als Geld- oder Warenspielgeräte mit Gewinnmöglichkeit gelten auch Erprobungsgeräte.

(4) Für Gaststätten (Schank- und Speisewirtschaften und Beherbergungsbetriebe) und Wettannahmestellen der Buchmacher, soweit sie Geld- oder Warenspielgeräte mit Gewinnmöglichkeit bereithalten, gelten nur die §§ 1 bis 3, 4 Abs. 3 und 4, §§ 5 bis 7 sowie die Vorschriften des Neunten Abschnitts.

(5) Für Pferdewetten gelten nur die §§ 1 bis 3, 5 bis 7 sowie die Vorschriften des Achten und Neunten Abschnitts.

(6) Für Gewinnspiele im Rundfunk (§ 2 Abs. 1 Satz 1 und 2 des Rundfunkstaatsvertrages) gilt nur § 8a des Rundfunkstaatsvertrages.

##### § 3  
Begriffsbestimmungen

(1) Ein Glücksspiel liegt vor, wenn im Rahmen eines Spiels für den Erwerb einer Gewinnchance ein Entgelt verlangt wird und die Entscheidung über den Gewinn ganz oder überwiegend vom Zufall abhängt.
Die Entscheidung über den Gewinn hängt in jedem Fall vom Zufall ab, wenn dafür der ungewisse Eintritt oder Ausgang zukünftiger Ereignisse maßgeblich ist.
Wetten gegen Entgelt auf den Eintritt oder Ausgang eines zukünftigen Ereignisses sind Glücksspiele.
Sportwetten sind Wetten zu festen Quoten auf den Ausgang von Sportereignissen oder Abschnitten von Sportereignissen.
Pferdewetten sind Wetten aus Anlass öffentlicher Pferderennen und anderer öffentlicher Leistungsprüfungen für Pferde.

(2) Ein öffentliches Glücksspiel liegt vor, wenn für einen größeren, nicht geschlossenen Personenkreis eine Teilnahmemöglichkeit besteht oder es sich um gewohnheitsmäßig veranstaltete Glücksspiele in Vereinen oder sonstigen geschlossenen Gesellschaften handelt.

(3) Ein Glücksspiel im Sinne des Absatzes 1, bei dem einer Mehrzahl von Personen die Möglichkeit eröffnet wird, nach einem bestimmten Plan gegen ein bestimmtes Entgelt die Chance auf einen Geldgewinn zu erlangen, ist eine Lotterie.
Die Vorschriften über Lotterien gelten auch, wenn anstelle von Geld Sachen oder andere geldwerte Vorteile gewonnen werden können (Ausspielung).

(4) Veranstaltet und vermittelt wird ein Glücksspiel dort, wo dem Spieler die Möglichkeit zur Teilnahme eröffnet wird.

(5) Annahmestellen und Lotterieeinnehmer sind in die Vertriebsorganisation von Veranstaltern nach § 10 Abs. 2 und 3 eingegliederte Vermittler.

(6) Gewerbliche Spielvermittlung betreibt, wer, ohne Annahmestelle, Lotterieeinnehmer oder Wettvermittlungsstelle zu sein,

1.  einzelne Spielverträge an einen Veranstalter vermittelt oder
2.  Spielinteressenten zu Spielgemeinschaften zusammenführt und deren Spielbeteiligung dem Veranstalter - selbst oder über Dritte - vermittelt,

sofern dies jeweils in der Absicht geschieht, durch diese Tätigkeit nachhaltig Gewinn zu erzielen.

(7) Eine Spielhalle ist ein Unternehmen oder Teil eines Unternehmens, das ausschließlich oder überwiegend der Aufstellung von Spielgeräten im Sinne des § 33c Abs. 1 Satz 1, der Veranstaltung anderer Spiele im Sinne des § 33d Abs. 1 Satz 1 der Gewerbeordnung in der Fassung vom 22. Februar 1999 (BGBl.
I S. 202; zuletzt geändert durch Art. 4 Abs. 14 des Gesetzes vom 29. Juli 2009 BGBl.
I S. 2258) oder der gewerbsmäßigen Aufstellung von Unterhaltungsspielen ohne Gewinnmöglichkeit dient.

##### § 4  
Allgemeine Bestimmungen

(1) Öffentliche Glücksspiele dürfen nur mit Erlaubnis der zuständigen Behörde des jeweiligen Landes veranstaltet oder vermittelt werden.
Das Veranstalten und das Vermitteln ohne diese Erlaubnis (unerlaubtes Glücksspiel) sowie die Mitwirkung an Zahlungen im Zusammenhang mit unerlaubtem Glücksspiel sind verboten.

(2) Die Erlaubnis ist zu versagen, wenn das Veranstalten oder das Vermitteln des Glücksspiels den Zielen des § 1 zuwiderläuft.
Die Erlaubnis darf nicht für das Vermitteln nach diesem Staatsvertrag nicht erlaubter Glücksspiele erteilt werden.
Auf die Erteilung der Erlaubnis besteht kein Rechtsanspruch.

(3) Das Veranstalten und das Vermitteln von öffentlichen Glücksspielen darf den Erfordernissen des Jugendschutzes nicht zuwiderlaufen.
Die Teilnahme von Minderjährigen ist unzulässig.
Die Veranstalter und die Vermittler haben sicherzustellen, dass Minderjährige von der Teilnahme ausgeschlossen sind.
Testkäufe oder Testspiele mit minderjährigen Personen dürfen durch die Glücksspielaufsichtsbehörden in Erfüllung ihrer Aufsichtsaufgaben durchgeführt werden.

(4) Das Veranstalten und das Vermitteln öffentlicher Glücksspiele im Internet ist verboten.

(5) Abweichend von Absatz 4 können die Länder zur besseren Erreichung der Ziele des § 1 den Eigenvertrieb und die Vermittlung von Lotterien sowie die Veranstaltung und Vermittlung von Sportwetten im Internet erlauben, wenn keine Versagungsgründe nach § 4 Abs. 2 vorliegen und folgende Voraussetzungen erfüllt sind:

1.  Der Ausschluss minderjähriger oder gesperrter Spieler wird durch Identifizierung und Authentifizierung gewährleistet.
2.  Der Höchsteinsatz je Spieler darf grundsätzlich einen Betrag von 1 000 Euro pro Monat nicht übersteigen.
    In der Erlaubnis kann zur Erreichung der Ziele des § 1 ein abweichender Betrag festgesetzt werden.
    Gewinne dürfen nicht mit Einsätzen der Spieler verrechnet werden.
    Die Beachtung des Kreditverbots ist sichergestellt.
    Bei der Registrierung sind die Spieler dazu aufzufordern, ein individuelles tägliches, wöchentliches oder monatliches Einzahlungs- oder Verlustlimit festzulegen (Selbstlimitierung).
    Darüber hinaus ist den Spielern zu jeder Zeit die Möglichkeit einzuräumen, tägliche, wöchentliche oder monatliche Einzahlungs- und Verlustlimits neu festzulegen.
    Will ein Spieler das Einzahlungs- oder Verlustlimit erhöhen, so wird die Erhöhung erst nach einer Schutzfrist von sieben Tagen wirksam.
    Wenn Einzahlungs- oder Verlustlimits verringert werden, greifen die neuen Limits für neue Spieleinsätze sofort.
3.  Besondere Suchtanreize durch schnelle Wiederholung sind ausgeschlossen.
4.  Ein an die besonderen Bedingungen des Internets angepasstes Sozialkonzept nach § 6 ist zu entwickeln und einzusetzen; seine Wirksamkeit ist wissenschaftlich zu evaluieren.
5.  Wetten und Lotterien werden weder über dieselbe Internetdomain angeboten noch wird auf andere Glücksspiele verwiesen oder verlinkt.

(6) Die Veranstalter und Vermittler von Lotterien und Sportwetten im Internet haben der Geschäftsstelle und dem Glücksspielkollegium vierteljährlich die Zahl der Spieler und die Höhe der Einsätze jeweils geordnet nach Spielen und Ländern zum Zwecke der Evaluierung zu übermitteln.

##### § 4a  
Konzession

(1) Soweit § 10 Abs. 6 im Rahmen der Experimentierklausel für Sportwetten nach § 10a nicht anwendbar ist, dürfen die dort den Veranstaltern nach § 10 Abs. 2 und 3 vorbehaltenen Glücksspiele nur mit einer Konzession veranstaltet werden.
§ 4 Abs. 1 Satz 2 ist entsprechend anzuwenden.

(2) Die Konzession wird für alle Länder von der zuständigen Behörde für eine in der Konzession festzulegende Dauer erteilt.
Auf die Erteilung der Konzession besteht kein Rechtsanspruch.

(3) Die Zahl der Konzessionen wird für die Dauer der Experimentierphase nicht beschränkt.

(4) Die Konzession darf nur erteilt werden, wenn

1.  (erweiterte Zuverlässigkeit)
    1.  die Inhaber- und Beteiligungsverhältnisse beim Konzessionsnehmer vollständig offengelegt sind; bei Personengesellschaften sind die Identität und die Adressen aller Gesellschafter, Anteilseigner oder sonstiger Kapitalgeber, bei juristischen Personen des Privatrechts von solchen, die mehr als fünf v.
        H. des Grundkapitals halten oder mehr als fünf v.
        H.
        der Stimmrechte ausüben, sowie generell alle Treuhandverhältnisse anzugeben
    2.  der Konzessionsnehmer und die von ihm beauftragten verantwortlichen Personen die für die Veranstaltung öffentlicher Glücksspiele erforderliche Zuverlässigkeit und Sachkunde besitzen und die Gewähr dafür bieten, dass die Veranstaltung ordnungsgemäß und für die Spieler sowie die Erlaubnisbehörde nachvollziehbar durchgeführt wird; bei juristischen Personen und Personengesellschaften müssen alle vertretungsbefugten Personen die Voraussetzungen der Zuverlässigkeit und Sachkunde besitzen
    3.  die rechtmäßige Herkunft der für die Veranstaltung öffentlicher Glücksspiele erforderlichen Mittel dargelegt ist
2.  (Leistungsfähigkeit)
    1.  der Konzessionsnehmer über genügend Eigenmittel für eine dauerhafte Geschäftstätigkeit verfügt und zugleich Gewähr für ein einwandfreies Geschäftsverhalten bietet
    2.  die Wirtschaftlichkeit des beabsichtigten Glücksspielangebots unter Berücksichtigung der Abgaben dargelegt ist
    3.  die erforderlichen Sicherheitsleistungen vorbereitet und die zum weitergehenden Schutz der Spieler notwendigen Versicherungen abgeschlossen sind
3.  (Transparenz und Sicherheit des Glücksspiels)
    1.  die Transparenz des Betriebs sichergestellt sowie gewährleistet ist, dass eine Überwachung des Vertriebsnetzes jederzeit möglich ist und nicht durch Dritte oder am Betrieb Beteiligte vereitelt werden kann
    2.  der Konzessionsnehmer einen Sitz in einem Mitgliedstaat der Europäischen Union oder einem Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum hat
    3.  der Konzessionsnehmer, sofern er über keinen Sitz im Inland verfügt, der zuständigen Behörde einen Empfangs- und Vertretungsbevollmächtigten im Inland benennt, der die Zuverlässigkeit im Sinne von Nummer 1 Buchst. b besitzt
    4.  bei Angeboten im Internet auf der obersten Stufe eine Internetdomäne „.de“ errichtet ist
    5.  der Konzessionsnehmer für alle Spiel- und Zahlungsvorgänge in Deutschland eine eigene Buchführung einrichtet und spielbezogene Zahlungsvorgänge über ein Konto im Inland oder bei einem in einem Mitgliedstaat der Europäischen Union beheimateten Kreditinstitut abwickelt
    6.  der Konzessionsnehmer Schnittstellen zur Prüfung aller Spielvorgänge in Echtzeit zur Verfügung stellt und
    7.  gewährleistet ist, dass vom Spieler eingezahlte Beträge unmittelbar nach Eingang der Zahlung beim Erlaubnisinhaber auf dem Spielkonto gutgeschrieben werden, ein etwaiges Guthaben dem Spieler auf Wunsch jederzeit ausgezahlt wird, die auf den Spielkonten deponierten Kundengelder vom sonstigen Vermögen getrennt verwaltet und nicht zum Risikoausgleich verwendet werden, sowie das gesamte Kundenguthaben jederzeit durch liquide Mittel gedeckt ist.

§ 4 Abs. 2 Satz 1 ist entsprechend anzuwenden.

##### § 4b  
Konzessionsverfahren

(1) Die Konzession wird nach Aufruf zur Bewerbung und Durchführung eines transparenten, diskriminierungsfreien Verfahrens erteilt.
Die Bekanntmachung ist im Amtsblatt der Europäischen Union zu veröffentlichen.

(2) Die Bewerbung bedarf der Schriftform.
Sie muss alle Angaben, Auskünfte, Nachweise und Unterlagen in deutscher Sprache enthalten, die in der Bekanntmachung bezeichnet sind, welche für die Prüfung der Voraussetzungen nach § 4a Abs. 4 erforderlich sind.
Dazu gehören insbesondere:

1.  eine Darstellung der unmittelbaren und mittelbaren Beteiligungen sowie der Kapital- und Stimmrechtsverhältnisse bei dem Bewerber und den mit ihm im Sinne des Aktiengesetzes verbundenen Unternehmen sowie Angaben über Angehörige im Sinne des § 15 Abgabenordnung unter den Beteiligten; gleiches gilt für Vertreter der Person oder Personengesellschaft oder des Mitglieds eines Organs einer juristischen Person.
    Daneben sind der Gesellschaftsvertrag und die satzungsrechtlichen Bestimmungen des Bewerbers sowie Vereinbarungen, die zwischen an dem Bewerber unmittelbar oder mittelbar Beteiligten bestehen und sich auf die Veranstaltung von Glücksspielen beziehen, vorzulegen,
2.  eine Darstellung der Maßnahmen zur Gewährleistung der öffentlichen Sicherheit und Ordnung und der sonstigen öffentlichen Belange unter besonderer Berücksichtigung der IT- und Datensicherheit (Sicherheitskonzept),
3.  ein Sozialkonzept einschließlich der Maßnahmen zur Sicherstellung des Ausschlusses minderjähriger und gesperrter Spieler,
4.  eine Darstellung der Wirtschaftlichkeit unter Berücksichtigung der Abgabenpflichten (Wirtschaftlichkeitskonzept),
5.  eine Erklärung der Übernahme der Kosten für die Überprüfung des Sicherheits-, Sozial- und Wirtschaftlichkeitskonzepts und, soweit erforderlich, sonstiger Unterlagen durch einen von der zuständigen Behörde beigezogenen Sachverständigen oder Wirtschaftsprüfer,
6.  eine Verpflichtungserklärung des Bewerbers, weder selbst noch durch verbundene Unternehmen unerlaubtes Glücksspiel in Deutschland zu veranstalten oder zu vermitteln und
7.  eine Erklärung des Bewerbers, dass die vorgelegten Unterlagen und Angaben vollständig sind.

Nachweise und Unterlagen aus einem anderen Mitgliedstaat der Europäischen Union oder einem anderen Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum stehen inländischen Nachweisen und Unterlagen gleich, wenn aus ihnen hervorgeht, dass die Anforderungen der in Satz 2 genannten Voraussetzungen erfüllt sind.
Die Unterlagen sind auf Kosten des Antragstellers in beglaubigter Kopie und beglaubigter deutscher Übersetzung vorzulegen.

(3) Die zuständige Behörde kann die Bewerber zur Prüfung der in Absatz 2 Satz 2 genannten Voraussetzungen unter Fristsetzung zur Ergänzung und zur Vorlage weiterer Angaben, Nachweise und Unterlagen in deutscher Sprache auffordern.
Sie ist befugt, Erkenntnisse der Sicherheitsbehörden des Bundes und der Länder, insbesondere zu den Voraussetzungen nach § 4a Abs. 4 Satz 1 Nr. 1 Buchst.
c, abzufragen.
Ist für die Prüfung im Konzessionsverfahren ein Sachverhalt bedeutsam, der sich auf Vorgänge außerhalb des Geltungsbereiches dieses Staatsvertrages bezieht, so hat der Bewerber diesen Sachverhalt aufzuklären und die erforderlichen Beweismittel zu beschaffen.
Er hat dabei alle für ihn bestehenden rechtlichen und tatsächlichen Möglichkeiten auszuschöpfen.
Der Bewerber kann sich nicht darauf berufen, dass er Sachverhalte nicht aufklären oder Beweismittel nicht beschaffen kann, wenn er sich nach Lage des Falles bei der Gestaltung seiner Verhältnisse die Möglichkeit dazu hätte beschaffen oder einräumen lassen können.

(4) Die im Rahmen des Konzessionsverfahrens Auskunfts- und Vorlagepflichtigen haben jede Änderung der maßgeblichen Umstände nach Bewerbung unverzüglich der zuständigen Behörde mitzuteilen und geplante Veränderungen von Beteiligungsverhältnissen oder sonstigen Einflüssen während des Konzessionsverfahrens der zuständigen Behörde schriftlich anzuzeigen.

##### § 4c  
Konzessionserteilung

(1) Die Konzession wird schriftlich erteilt.
Sie darf nur nach Zustimmung der zuständigen Behörde einem Dritten übertragen oder zur Ausübung überlassen werden.

(2) In der Konzession sind die Inhalts- und Nebenbestimmungen festzulegen, die zur dauernden Sicherstellung der Konzessionsvoraussetzungen sowie zur Einhaltung und Überwachung der nach diesem Staatsvertrag bestehenden und im Angebot übernommenen Pflichten erforderlich sind.

(3) Die Erteilung der Konzession setzt voraus, dass der Konzessionsnehmer zur Sicherstellung von Auszahlungsansprüchen der Spieler und von staatlichen Zahlungsansprüchen eine Sicherheitsleistung in Form einer unbefristeten selbstschuldnerischen Bankbürgschaft eines Kreditinstituts mit Sitz in der Europäischen Union oder in einem Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum erbringt.
Die Sicherheitsleistung beläuft sich auf fünf Millionen Euro.
Sie kann von der Behörde, die die Konzession erteilt, bis zur Höhe des zu erwartenden Durchschnittsumsatzes zweier Wochen, maximal auf 25 Millionen Euro, erhöht werden.

##### § 4d  
Konzessionsabgabe

(1) Es wird eine Konzessionsabgabe erhoben.
Der Konzessionsnehmer ist verpflichtet, diese an die zuständige Behörde des Landes Hessen zu entrichten.

(2) Die Konzessionsabgabe beträgt 5 v.
H.
des Spieleinsatzes.
Sie wird von der zuständigen Behörde nach Absatz 1 vereinnahmt, gesondert ausgewiesen und nach dem Königsteiner Schlüssel auf die Länder verteilt.
Sie ist in den Anlagen zum jeweiligen Haushaltsplan gesondert auszuweisen.

(3) Der Konzessionsnehmer hat der zuständigen Behörde nach Absatz 1 spätestens innerhalb von zehn Werktagen nach Ablauf eines Kalendermonats die in diesem Kalendermonat erzielten Spieleinsätze mitzuteilen und die daraus berechnete monatliche Konzessionsabgabe zu entrichten.

(4) Auf Antrag eines Konzessionsnehmers kann die zuständige Behörde nach Absatz 1 die Abrechnung zum Ende eines Quartals zulassen.
Der Konzessionsnehmer hat zu diesem Termin die erzielten Spieleinsätze mitzuteilen und die daraus berechnete Konzessionsabgabe zu entrichten.

(5) Der Konzessionsnehmer hat der zuständigen Behörde nach Absatz 1 auf Verlangen seine Bücher und Aufzeichnungen vorzulegen und Auskünfte zu erteilen, die für die Feststellung der Höhe der Konzessionsabgabe erforderlich sind.

(6) Zur Sicherung der Ansprüche auf Zahlung der Konzessionsabgabe kann die zuständige Behörde nach Absatz 1 vom Konzessionsnehmer Sicherheit in Form einer unbefristeten selbstschuldnerischen Bankbürgschaft verlangen.
Anstelle der Bürgschaft kann auch eine gleichwertige Sicherheit anderer Art geleistet werden.

(7) Vom Konzessionsnehmer in Ausübung der Konzession gezahlte Steuern auf der Grundlage des Rennwett- und Lotteriegesetzes sind auf die Konzessionsabgabe anzurechnen.

(8) Auf die Konzessionsabgabe sind ergänzend die Vorschriften der Abgabenordnung über die Führung von Büchern und Aufzeichnungen (§§ 140 bis 148), über Steuererklärungen (§§ 149 bis 153), über die Steuerfestsetzung (§§ 155 bis 168), über die Festsetzungsverjährung (§ 169 Abs. 1, 2 Satz 1 Nr. 2, Satz 2 und 3, §§ 170 und 171), über die Bestandskraft (§§ 172 bis 177), über das Erhebungsverfahren (§§ 218 bis 222, 224, 234, 240 bis 248), über die Vollstreckung (§§ 249 bis 346) und des Umsatzsteuergesetzes über Aufzeichnungspflichten (§ 22) sinngemäß anzuwenden.

##### § 4e  
Konzessionspflichten; Aufsichtliche Maßnahmen

(1) Der Konzessionsnehmer ist verpflichtet, jede Änderung der für die Erteilung der Konzession maßgeblichen Umstände unverzüglich der zuständigen Behörde mitzuteilen.
§ 4b Abs. 2 bis 4 finden entsprechende Anwendung.
Die Aufhebung eines Vertretungsverhältnisses nach § 4a Abs. 4 Satz 1 Nr. 3 Buchst.
c erlangt gegenüber den zuständigen Behörden erst durch die Bestellung eines neuen Empfangs- und Vertretungsbevollmächtigten und schriftliche Mitteilung Wirksamkeit.

(2) Bei Personengesellschaften ist jede geplante Veränderung von Beteiligungsverhältnissen oder sonstigen Einflüssen, bei juristischen Personen nur solche, die mehr als fünf v.
H.
des Grundkapitals oder des Stimmrechts betreffen, der zuständigen Behörde schriftlich anzuzeigen.
Anzeigepflichtig ist der Konzessionsnehmer und die an ihm unmittelbar oder mittelbar Beteiligten.
Die Veränderungen dürfen nur dann von der zuständigen Behörde als unbedenklich bestätigt werden, wenn unter den veränderten Voraussetzungen eine Konzession erteilt werden könnte.
Wird eine geplante Veränderung vollzogen, die nicht nach Satz 3 als unbedenklich bestätigt werden kann, ist die Konzession zu widerrufen; das Nähere des Widerrufs richtet sich nach Landesrecht.
Unbeschadet der Anzeigepflichten nach Satz 1 ist der Konzessionsnehmer und die an ihm unmittelbar oder mittelbar Beteiligten jeweils nach Ablauf eines Kalenderjahres verpflichtet, unverzüglich der zuständigen Behörde gegenüber eine Erklärung darüber abzugeben, ob und inwieweit innerhalb des abgelaufenen Kalenderjahres bei den Beteiligungs- und Zurechnungstatbeständen eine Veränderung eingetreten ist.

(3) Der Konzessionsnehmer hat abweichend von Nummer 1 Buchst.
b des Anhangs („Richtlinien zur Vermeidung und Bekämpfung von Glücksspielsucht“) jährlich zu berichten.
Die Richtigkeit der Erhebung und Übermittlung der Daten kann in regelmäßigen Abständen durch eine unabhängige Stelle überprüft werden.
Mit dem Bericht ist auch der Prüfbericht einer geeigneten externen und unabhängigen Stelle über die Einhaltung der technischen Standards und die Wirksamkeit der im Sicherheitskonzept vorgesehenen und in der Konzession vorgeschriebenen Sicherheitsmaßnahmen vorzulegen.
Auf Anforderung der zuständigen Behörde hat der Konzessionsnehmer zudem Kontodaten zur Verfügung zu stellen, soweit die Umsätze nicht über ein inländisches Konto abgewickelt werden.

(4) Verletzt ein Konzessionsnehmer eine nach Absatz 1, Absatz 2 Satz 5 und Absatz 3 bestehende Mitteilungspflicht, die nach § 4c Abs. 2 festgelegten Inhalts- und Nebenbestimmungen der Konzession oder eine nach § 4d bestehende Pflicht, kann die zuständige Behörde ihn unter Setzung einer angemessenen Frist zur Einhaltung der Pflichten auffordern.
Werden nach Ablauf der Frist die Pflichten nicht oder nicht vollständig erfüllt, kann die zuständige Behörde unter Berücksichtigung der Schwere des Verstoßes insbesondere folgende Maßnahmen ergreifen:

1.  öffentliche Abmahnung mit erneuter Fristsetzung,
2.  Aussetzung der Konzession für drei Monate,
3.  Reduzierung der Dauer der Konzession um ein Viertel der gesamten Laufzeit oder
4.  Widerruf der Konzession.

Gleiches gilt für den Fall, dass der Konzessionsnehmer selbst oder ein mit ihm verbundenes Unternehmen im Geltungsbereich dieses Staatsvertrages unerlaubte Glücksspiele veranstaltet oder vermittelt.
Die § 49 des Verwaltungsverfahrensgesetzes entsprechenden Vorschriften der Verwaltungsverfahrensgesetze der Länder bleiben anwendbar.
§ 9 Abs. 4 Satz 3 ist anzuwenden.

##### § 5  
Werbung

(1) Art und Umfang der Werbung für öffentliches Glücksspiel ist an den Zielen des § 1 auszurichten.

(2) Sie darf sich nicht an Minderjährige oder vergleichbar gefährdete Zielgruppen richten.
Irreführende Werbung für öffentliches Glücksspiel, insbesondere solche, die unzutreffende Aussagen über die Gewinnchancen oder Art und Höhe der Gewinne enthält, ist verboten.

(3) Werbung für öffentliches Glücksspiel ist im Fernsehen (§ 7 des Rundfunkstaatsvertrages), im Internet sowie über Telekommunikationsanlagen verboten.
Davon abweichend können die Länder zur besseren Erreichung der Ziele des § 1 Werbung für Lotterien und Sport- und Pferdewetten im Internet und im Fernsehen unter Beachtung der Grundsätze nach den Absätzen 1 und 2 erlauben.
Werbung für Sportwetten im Fernsehen unmittelbar vor oder während der Live-Übertragung von Sportereignissen auf dieses Sportereignis ist nicht zulässig.
§ 9a ist anzuwenden.

(4) Die Länder erlassen gemeinsame Auslegungsrichtlinien zur Konkretisierung von Art und Umfang der nach den Absätzen 1 bis 3 erlaubten Werbung (Werberichtlinie).
Sie stützen sich auf die vorliegenden wissenschaftlichen Erkenntnisse zur Wirkung von Werbung auf jugendliche sowie problematische und pathologische Spieler.
Vor Erlass und wesentlicher Änderung der Werberichtlinie ist den beteiligten Kreisen Gelegenheit zur Stellungnahme zu geben.
§ 9a Abs. 6 bis 8 ist entsprechend anzuwenden.
Die Werberichtlinie ist in allen Ländern zu veröffentlichen.

(5) Werbung für unerlaubte Glücksspiele ist verboten.

##### § 6  
Sozialkonzept

Die Veranstalter und Vermittler von öffentlichen Glücksspielen sind verpflichtet, die Spieler zu verantwortungsbewusstem Spiel anzuhalten und der Entstehung von Glücksspielsucht vorzubeugen.
Zu diesem Zweck haben sie Sozialkonzepte zu entwickeln, ihr Personal zu schulen und die Vorgaben des Anhangs „Richtlinien zur Vermeidung und Bekämpfung von Glücksspielsucht“ zu erfüllen.
In den Sozialkonzepten ist darzulegen, mit welchen Maßnahmen den sozialschädlichen Auswirkungen des Glücksspiels vorgebeugt werden soll und wie diese behoben werden sollen.

##### § 7  
Aufklärung

(1) Die Veranstalter und Vermittler von öffentlichen Glücksspielen haben den Spielern vor der Spielteilnahme die spielrelevanten Informationen zur Verfügung zu stellen, sowie über die Suchtrisiken der von ihnen angebotenen Glücksspiele, das Verbot der Teilnahme Minderjähriger und Möglichkeiten der Beratung und Therapie aufzuklären.
Als spielrelevante Informationen kommen insbesondere in Betracht:

1.  alle Kosten, die mit der Teilnahme veranlasst sind,
2.  die Höhe aller Gewinne,
3.  wann und wo alle Gewinne veröffentlicht werden,
4.  der Prozentsatz der Auszahlungen für Gewinne vom Einsatz (Auszahlungsquote),
5.  Informationen zu den Gewinn- und Verlustwahrscheinlichkeiten,
6.  der Annahmeschluss der Teilnahme,
7.  das Verfahren, nach dem der Gewinner ermittelt wird, insbesondere die Information über den Zufallsmechanismus, der der Generierung der zufallsabhängigen Spielergebnisse zu Grunde liegt,
8.  wie die Gewinne zwischen den Gewinnern aufgeteilt werden,
9.  die Ausschlussfrist, bis wann Gewinner Anspruch auf ihren Gewinn erheben müssen,
10.  der Name des Erlaubnisinhabers sowie seine Kontaktdaten (Anschrift, E-Mail, Telefon),
11.  die Handelsregisternummer (soweit vorhanden),
12.  wie der Spieler Beschwerden vorbringen kann und
13.  das Datum der ausgestellten Erlaubnis.

Spieler und Behörden müssen leichten Zugang zu diesen Informationen haben.

(2) Lose, Spielscheine, Spielquittungen und vergleichbare Bescheinigungen müssen Hinweise auf die von dem jeweiligen Glücksspiel ausgehende Suchtgefahr und Hilfsmöglichkeiten enthalten.

##### § 8  
Spielersperre

(1) Zum Schutz der Spieler und zur Bekämpfung der Glücksspielsucht wird ein übergreifendes Sperrsystem (§ 23) unterhalten.

(2) Spielbanken und Veranstalter von Sportwetten und Lotterien mit besonderem Gefährdungspotential sperren Personen, die dies beantragen (Selbstsperre) oder von denen sie aufgrund der Wahrnehmung ihres Personals oder aufgrund von Meldungen Dritter wissen oder aufgrund sonstiger tatsächlicher Anhaltspunkte annehmen müssen, dass sie spielsuchtgefährdet oder überschuldet sind, ihren finanziellen Verpflichtungen nicht nachkommen oder Spieleinsätze riskieren, die in keinem Verhältnis zu ihrem Einkommen oder Vermögen stehen (Fremdsperre).

(3) Die Sperre beträgt mindestens ein Jahr.
Die Veranstalter teilen die Sperre dem betroffenen Spieler unverzüglich schriftlich mit.

(4) Die Veranstalter haben die in § 23 Abs. 1 genannten Daten in eine Sperrdatei einzutragen.
Ein Eintrag ist auch zulässig, wenn nicht alle Daten erhoben werden können.

(5) Eine Aufhebung der Sperre ist frühestens nach einem Jahr und nur auf schriftlichen Antrag des Spielers möglich.
Über diesen entscheidet der Veranstalter, der die Sperre verfügt hat.

(6) Zum Schutz der Spieler und zur Bekämpfung der Glücksspielsucht sind die Vermittler von öffentlichen Glücksspielen verpflichtet, an dem übergreifenden Sperrsystem (§ 23) mitzuwirken.
Zu diesem Zweck übermitteln die Vermittler die bei ihnen eingereichten Anträge auf Selbstsperren unverzüglich an den Veranstalter nach § 10 Abs. 2, in dessen Geltungsbereich der Spieler seinen Wohnsitz hat.

#### Zweiter Abschnitt  
Aufgaben des Staates

##### § 9  
Glücksspielaufsicht

(1) Die Glücksspielaufsicht hat die Aufgabe, die Erfüllung der nach diesem Staatsvertrag bestehenden oder auf Grund dieses Staatsvertrages begründeten öffentlich-rechtlichen Verpflichtungen zu überwachen sowie darauf hinzuwirken, dass unerlaubtes Glücksspiel und die Werbung hierfür unterbleiben.
Die zuständige Behörde des jeweiligen Landes kann die erforderlichen Anordnungen im Einzelfall erlassen.
Sie kann insbesondere

1.  jederzeit Auskunft und Vorlage aller Unterlagen und Nachweise verlangen, die zur Prüfung im Rahmen des Satzes 1 erforderlich sind, sowie zum Zwecke dieser Prüfung während der üblichen Geschäfts- und Arbeitszeiten die Geschäftsräume und -grundstücke betreten, in denen öffentliches Glücksspiel veranstaltet oder vermittelt wird,
2.  Anforderungen an die Veranstaltung, Durchführung und Vermittlung öffentlicher Glücksspiele und die Werbung hierfür sowie an die Entwicklung und Umsetzung des Sozialkonzepts stellen,
3.  die Veranstaltung, Durchführung und Vermittlung unerlaubter Glücksspiele und die Werbung hierfür untersagen und
4.  den am Zahlungsverkehr Beteiligten, insbesondere den Kredit- und Finanzdienstleistungsinstituten, nach vorheriger Bekanntgabe unerlaubter Glücksspielangebote die Mitwirkung an Zahlungen für unerlaubtes Glücksspiel und an Auszahlungen aus unerlaubtem Glücksspiel untersagen.

Sofern unerlaubtes Glücksspiel in mehreren Ländern veranstaltet oder vermittelt wird oder dafür in mehreren Ländern geworben oder in sonstiger Weise gegen öffentlich-rechtliche Verpflichtungen im Sinne des Satzes 1 verstoßen wird, kann jedes betroffene Land die zuständige Behörde eines anderen Landes ermächtigen, auch mit Wirkung für das betroffene Land die erforderlichen Anordnungen im Einzelfall zu erlassen und zu vollstrecken.
Die Vollstreckung richtet sich nach dem Recht des ermächtigten Landes.

(2) Widerspruch und Klage gegen diese Anordnungen haben keine aufschiebende Wirkung.
Im Falle der Vollstreckung von Anordnungen nach Absatz 1 mittels Zwangsgeld soll dieses das wirtschaftliche Interesse, das der Pflichtige an der Vornahme oder am Unterbleiben der Handlung hat, erreichen.
Reicht das gesetzliche Höchstmaß hierzu nicht aus, so kann es überschritten werden.
Das wirtschaftliche Interesse des Pflichtigen ist nach pflichtgemäßem Ermessen zu schätzen.

(3) Die Länder arbeiten bei der Glücksspielaufsicht zusammen; sie können auch mit den zuständigen Aufsichtsbehörden der Mitgliedstaaten der Europäischen Union und der Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zusammenarbeiten und zu diesem Zweck Daten austauschen, soweit dies zur Erfüllung ihrer Aufgaben erforderlich ist.
Soweit nach diesem Staatsvertrag nichts anderes bestimmt ist, stimmen die Länder die Erlaubnisse für die in § 10 Abs. 2 genannten Veranstalter im Benehmen ab.

(4) Die Erlaubnis wird von der zuständigen Behörde für das Gebiet des jeweiligen Landes oder einen Teil dieses Gebietes erteilt.
Sie ist widerruflich zu erteilen und zu befristen.
Sie kann, auch nachträglich, mit Nebenbestimmungen versehen werden.
Die Erlaubnis ist weder übertragbar noch kann sie einem Anderen zur Ausübung überlassen werden.

(5) Die Erlaubnis zur Einführung neuer Glücksspielangebote durch die in § 10 Abs. 2 und 3 genannten Veranstalter setzt voraus, dass

1.  der Fachbeirat (§ 10 Abs. 1 Satz 2) zuvor die Auswirkungen des neuen Angebotes unter Berücksichtigung der Ziele des § 1 auf die Bevölkerung untersucht und bewertet hat und
2.  der Veranstalter im Anschluss an die Einführung dieses Glücksspiels der Erlaubnisbehörde über die sozialen Auswirkungen des neuen Angebotes berichtet.
    Neuen Glücksspielangeboten steht die Einführung neuer oder die erhebliche Erweiterung bestehender Vertriebswege durch Veranstalter oder Vermittler gleich.

(6) Angaben über persönliche und sachliche Verhältnisse einer natürlichen oder juristischen Person oder einer Personengesellschaft sowie Betriebs- oder Geschäftsgeheimnisse, die den zuständigen Behörden, ihren Organen, ihren Bediensteten oder von ihnen beauftragten Dritten im Rahmen der Durchführung ihrer Aufgabenerfüllung anvertraut oder sonst bekannt geworden sind, dürfen nicht unbefugt offenbart werden.
Soweit personenbezogene Daten verarbeitet werden, finden die landesrechtlichen Datenschutzbestimmungen Anwendung.

(7) Die Glücksspielaufsicht darf nicht durch eine Behörde ausgeübt werden, die für die Finanzen des Landes oder die Beteiligungsverwaltung der in § 10 Abs. 2 und 3 genannten Veranstalter zuständig ist.

#### § 9a  
Ländereinheitliches Verfahren

(1) Der Anstalt nach § 10 Abs. 3 sowie deren Lotterie-Einnehmern wird die Erlaubnis von der zuständigen Glücksspielaufsichtsbehörde des Landes, in dessen Gebiet die Anstalt ihren Sitz hat, für das Gebiet aller Länder erteilt (Freie und Hansestadt Hamburg).

(2) Unbeschadet des Absatzes 1 erteilt die Glücksspielaufsichtsbehörde eines Landes für alle Länder

1.  die Erlaubnis für Werbung für Lotterien und Sportwetten im Internet und im Fernsehen nach § 5 Abs. 3 das Land Nordrhein-Westfalen,
2.  die Erlaubnisse für eine gemeinsam geführte Anstalt nach § 10 Abs. 2 Satz 1 das Land Baden-Württemberg,
3.  die Konzession nach § 4a und die Erlaubnis nach § 27 Abs. 2 das Land Hessen und
4.  die Erlaubnis nach § 12 Abs. 3 Satz 1 das Land Rheinland-Pfalz.

Bei unerlaubten Glücksspielen, die in mehr als einem Land angeboten werden, ist für Maßnahmen nach § 9 Abs. 1 Satz 3 Nr. 4 die Glücksspielaufsichtsbehörde des Landes Niedersachsen zuständig.

(3) Die nach den Absätzen 1 und 2 zuständigen Behörden üben gegenüber den Erlaubnis- und Konzessionsnehmern auch die Aufgaben der Glücksspielaufsicht nach § 9 Abs. 1 mit Wirkung für alle Länder aus; sie können die erforderlichen Anordnungen im Einzelfall erlassen und nach ihrem jeweiligen Landesrecht vollstrecken sowie dazu Amtshandlungen in anderen Ländern vornehmen.
Die zuständige Behörde nach Absatz 2 Satz 1 überwacht insbesondere die Einhaltung der Inhalts- und Nebenbestimmungen der Konzession und entscheidet über Maßnahmen nach §§ 4a bis 4e.
§ 9 Abs. 2 gilt entsprechend.

(4) Die nach den Absätzen 1 und 2 zuständigen Behörden erheben für Amtshandlungen in Erfüllung der Aufgaben nach den Absätzen 1 bis 3 Kosten (Gebühren und Auslagen).
Für die Erteilung einer Erlaubnis oder Konzession für das Veranstalten eines Glücksspiels wird bei genehmigten oder voraussichtlichen Spiel- oder Wetteinsätzen

1.  bis zu 30 Millionen Euro eine Gebühr in Höhe von 1,0 v.
    T. der Spiel- oder Wetteinsätze, mindestens 50 Euro,
2.  über 30 Millionen Euro bis 50 Millionen Euro eine Gebühr in Höhe von 30 000 Euro zuzüglich 0,8 v.
    T.
    der 30 Millionen Euro übersteigenden Spiel- oder Wetteinsätze,
3.  über 50 Millionen Euro bis 100 Millionen Euro eine Gebühr in Höhe von 46 000 Euro zuzüglich 0,5 v.
    T.
    der 50 Millionen Euro übersteigenden Spiel- oder Wetteinsätze,
4.  über 100 Millionen Euro eine Gebühr in Höhe von 71 000 Euro zuzüglich 0,3 v.
    T.
    der 100 Millionen Euro übersteigenden Spiel- oder Wetteinsätze

erhoben; zugrunde zu legen ist die Summe der genehmigten oder voraussichtlichen Spiel- oder Wetteinsätze in allen beteiligten Ländern.
Wird die Erlaubnis oder Konzession für mehrere aufeinanderfolgende Jahre oder Veranstaltungen erteilt, erfolgt die Berechnung gesondert für jedes Jahr und jede Veranstaltung, wobei sich die Gebühr nach Satz 2 für jedes Folgejahr oder jede Folgeveranstaltung um 10 v.
H ermäßigt.
Für die Erteilung einer Erlaubnis für das Vermitteln eines Glücksspiels wird eine Gebühr in Höhe von 50 v.
H.
der Gebühr nach Satz 2 erhoben; Satz 3 ist entsprechend anzuwenden.
Für Anordnungen zur Beseitigung oder Beendigung rechtswidriger Zustände sowie für sonstige Anordnungen der Glücksspielaufsichtsbehörden wird eine Gebühr von 500 Euro bis 500 000 Euro erhoben; dabei sind der mit der Amtshandlung verbundene Verwaltungsaufwand aller beteiligten Behörden und Stellen zu berücksichtigen.
Im Übrigen gelten die Kostenvorschriften des jeweiligen Sitzlandes der handelnden Behörde.

(5) Zur Erfüllung der Aufgaben nach den Absätzen 1 bis 3 besteht das Glücksspielkollegium der Länder.
Hierbei dient das Glücksspielkollegium den Ländern zur Umsetzung einer gemeinschaftlich auszuübenden Aufsicht der jeweiligen obersten Glücksspielaufsichtsbehörden.

(6) Das Glücksspielkollegium der Länder besteht aus 16 Mitgliedern.
Jedes Land benennt durch seine oberste Glücksspielaufsichtsbehörde je ein Mitglied sowie dessen Vertreter für den Fall der Verhinderung.
Das Glücksspielkollegium gibt sich einvernehmlich eine Geschäftsordnung.
§ 9 Abs. 6 gilt entsprechend.

(7) Die Länder bilden für das Glücksspielkollegium eine Geschäftsstelle im Land Hessen.
Die Finanzierung der Behörden nach Absatz 2, des Glücksspielkollegiums und der Geschäftsstelle sowie die Verteilung der Einnahmen aus Verwaltungsgebühren nach § 9a werden in einer Verwaltungsvereinbarung der Länder geregelt.

(8) Das Glücksspielkollegium fasst seine Beschlüsse mit einer Mehrheit von mindestens zwei Drittel der Stimmen seiner Mitglieder.
Die Beschlüsse sind zu begründen.
In der Begründung sind die wesentlichen tatsächlichen und rechtlichen Gründe mitzuteilen.
Die Beschlüsse sind für die nach den Absätzen 1 bis 3 zuständigen Behörden und die Geschäftsstelle bindend; sie haben die Beschlüsse innerhalb der von dem Glücksspielkollegium gesetzten Frist zu vollziehen.

##### § 10  
Sicherstellung eines ausreichenden Glücksspielangebotes

(1) Die Länder haben zur Erreichung der Ziele des § 1 die ordnungsrechtliche Aufgabe, ein ausreichendes Glücksspielangebot sicherzustellen.
Sie werden dabei von einem Fachbeirat beraten.
Dieser setzt sich aus Personen zusammen, die im Hinblick auf die Ziele des § 1 über besondere wissenschaftliche oder praktische Erfahrungen verfügen.

(2) Auf gesetzlicher Grundlage können die Länder diese öffentliche Aufgabe selbst, durch eine von allen Vertragsländern gemeinsam geführte öffentliche Anstalt, durch juristische Personen des öffentlichen Rechts oder durch privatrechtliche Gesellschaften, an denen juristische Personen des öffentlichen Rechts unmittelbar oder mittelbar maßgeblich beteiligt sind, erfüllen.
Auf der Grundlage eines Verwaltungsabkommens ist auch eine gemeinschaftliche Aufgabenerfüllung oder eine Aufgabenerfüllung durch die Unternehmung eines anderen Landes möglich, das die Voraussetzungen des Satzes 1 erfüllt.

(3) Klassenlotterien dürfen nur von einer von allen Vertragsländern gemeinsam getragenen Anstalt des öffentlichen Rechts veranstaltet werden.

(4) Die Länder begrenzen die Zahl der Annahmestellen zur Erreichung der Ziele des § 1.

(5) Es ist sicherzustellen, dass ein erheblicher Teil der Einnahmen aus Glücksspielen zur Förderung öffentlicher oder gemeinnütziger, kirchlicher oder mildtätiger Zwecke verwendet wird.

(6) Anderen als den in den Absätzen 2 und 3 Genannten darf nur die Veranstaltung von Lotterien und Ausspielungen nach den Vorschriften des Dritten Abschnitts erlaubt werden.

##### § 10a  
Experimentierklausel für Sportwetten

(1) Um eine bessere Erreichung der Ziele des § 1, insbesondere auch bei der Bekämpfung des in der Evaluierung festgestellten Schwarzmarktes, zu erproben, wird § 10 Abs. 6 auf das Veranstalten von Sportwetten bis zum 30.
Juni 2021 nicht angewandt.
Im Falle einer Fortgeltung des Staatsvertrages nach § 35 Absatz 2 verlängert sich die Frist bis zum 30.
Juni 2024.

(2) Sportwetten dürfen in diesem Zeitraum nur mit einer Konzession (§§ 4a bis 4e) veranstaltet werden.

(3) Die Konzession gibt dem Konzessionsnehmer nach Maßgabe der gemäß § 4c Abs. 2 festgelegten Inhalts- und Nebenbestimmungen das Recht, abweichend vom Verbot des § 4 Abs. 4 Sportwetten im Internet zu veranstalten und zu vermitteln.
§ 4 Abs. 5 und 6 ist entsprechend anzuwenden.
Der Geltungsbereich der Konzession ist auf das Gebiet der Bundesrepublik Deutschland und der Staaten, die die deutsche Erlaubnis für ihr Hoheitsgebiet anerkennen, beschränkt.

(4) Die Länder begrenzen die Zahl der Wettvermittlungsstellen zur Erreichung der Ziele des § 1.
Die Vermittlung von Sportwetten in diesen Stellen bedarf der Erlaubnis nach § 4 Abs. 1 Satz 1; § 29 Abs. 2 Satz 2 ist entsprechend anzuwenden.

##### § 11  
Suchtforschung

Die Länder stellen die wissenschaftliche Forschung zur Vermeidung und Abwehr von Suchtgefahren durch Glücksspiele sicher.

#### Dritter Abschnitt  
Lotterien mit geringerem Gefährdungspotential

##### § 12  
Erlaubnis

(1) Die Erlaubnis gemäß § 4 Abs. 1 darf nur erteilt werden, wenn

1.  der Veranstaltung keine Versagungsgründe nach § 13 entgegenstehen,
2.  die in §§ 14, 15 Abs. 1 und 2, § 16 Abs. 3 genannten Voraussetzungen vorliegen,
3.  mit der Veranstaltung keine wirtschaftlichen Zwecke verfolgt werden, die über den mit dem Hinweis auf die Bereitstellung von Gewinnen verbundenen Werbeeffekt hinausgehen, und
4.  nicht zu erwarten ist, dass durch die Veranstaltung selbst oder durch die Verwirklichung des Veranstaltungszwecks oder die Verwendung des Reinertrages die öffentliche Sicherheit oder Ordnung gefährdet wird oder die Beziehungen der Bundesrepublik Deutschland zu anderen Staaten beeinträchtigt werden.

Satz 1 Nr. 3 gilt nicht für Lotterien in der Form des Gewinnsparens, wenn von einem Teilnahmebetrag ein Teilbetrag von höchstens 25 v.
H.
als Losanteil für die Gewinnsparlotterie verwendet wird.

(2) In der Erlaubnis ist auch zu entscheiden, inwieweit die Anforderungen der §§ 6 und 7 zu erfüllen sind.

(3) Soll eine Lotterie mit einem einheitlichen länderübergreifenden Spielplan in allen Ländern veranstaltet werden, so wird die Erlaubnis zu deren Durchführung ländereinheitlich erteilt.
Soll eine Lotterie mit einem einheitlichen länderübergreifenden Spielplan nur in einigen Ländern veranstaltet werden, so kann das Land, in dem der Veranstalter seinen Sitz hat, die Erlaubnis auch mit Wirkung für die Länder erteilen, die dazu ermächtigt haben.

##### § 13  
Versagungsgründe

(1) Eine Erlaubnis darf nicht erteilt werden, wenn die Veranstaltung § 4 Abs. 2 bis 6 widerspricht.
Dies ist vor allem der Fall, wenn nicht auszuschließen ist, dass die Veranstaltung der Lotterie wegen des insgesamt bereits vorhandenen Glücksspielangebotes, insbesondere im Hinblick auf die Zahl der bereits veranstalteten Glücksspiele oder deren Art oder Durchführung den Spieltrieb in besonderer Weise fördert.

(2) Eine Erlaubnis darf insbesondere nicht erteilt werden, wenn

1.  der Spielplan vorsieht, dass
    1.  die Bekanntgabe der Ziehungsergebnisse öfter als zweimal wöchentlich erfolgt,
    2.  der Höchstgewinn einen Wert von 2 Millionen Euro übersteigt oder
    3.  Teile des vom Spieler zu entrichtenden Entgeltes zu dem Zweck angesammelt werden, Gewinne für künftige Ziehungen zu schaffen (planmäßiger Jackpot), oder
2.  eine interaktive Teilnahme in Rundfunk und Telemedien mit zeitnaher Gewinnbekanntgabe ermöglicht wird.

##### § 14  
Veranstalter

(1) Eine Erlaubnis darf nur erteilt werden, wenn der Veranstalter

1.  die Voraussetzungen des § 5 Abs. 1 Nr. 9 des Körperschaftsteuergesetzes erfüllt und
2.  zuverlässig ist, insbesondere die Gewähr dafür bietet, dass die Veranstaltung ordnungsgemäß und für die Spieler sowie die Erlaubnisbehörde nachvollziehbar durchgeführt und der Reinertrag zweckentsprechend verwendet wird.

Satz 1 Nr. 1 gilt nicht für die von den in § 10 Abs. 2 und 3 genannten Veranstaltern und von der Körperschaft des öffentlichen Rechts „Bayerisches Rotes Kreuz“ veranstalteten Lotterien und für Veranstaltungen in der Form des Gewinnsparens (§ 12 Abs. 1 Satz 2).

(2) Soll die Veranstaltung ganz oder überwiegend von einem Dritten durchgeführt werden, darf die Erlaubnis nur erteilt werden, wenn nicht die Gefahr besteht, dass durch die Durchführung die Transparenz und Kontrollierbarkeit der Veranstaltung beeinträchtigt wird und der Dritte

1.  die Anforderungen des Absatzes 1 Nr. 2 erfüllt und
2.  hinsichtlich der Durchführung der Veranstaltung den Weisungen des Veranstalters unterliegt und keinen maßgeblichen rechtlichen oder tatsächlichen Einfluss auf den Veranstalter hat.

##### § 15  
Spielplan, Kalkulation und Durchführung der Veranstaltung

(1) Nach dem Spielplan müssen der Reinertrag, die Gewinnsumme und die Kosten in einem angemessenen Verhältnis zueinander stehen; die Kosten der Veranstaltung sind so gering wie möglich zu halten.
Reinertrag ist der Betrag, der sich aus der Summe der Entgelte nach Abzug von Kosten, Gewinnsumme und Steuern ergibt.
Für den Reinertrag und die Gewinnsumme sollen im Spielplan jeweils mindestens 30 v.
H.
der Entgelte vorgesehen sein und es darf kein Grund zu der Annahme bestehen, dass diese Anteile nicht erreicht werden.
Bei der Antragstellung ist eine Kalkulation vorzulegen, aus der sich die voraussichtlichen Kosten der Veranstaltung, die Gewinnsumme, die Steuern und der Reinertrag ergeben.
Zeigt sich nach Erteilung der Erlaubnis, dass die kalkulierten Kosten voraussichtlich überschritten werden, ist dies der Erlaubnisbehörde unverzüglich anzuzeigen und eine neue Kalkulation vorzulegen.

(2) In den Kosten der Lotterie dürfen Kosten von Dritten im Sinne des § 14 Abs. 2 nach Art und Umfang nur insoweit berücksichtigt werden, als sie den Grundsätzen wirtschaftlicher Betriebsführung entsprechen.
Die Vergütung des Dritten soll nicht abhängig vom Umsatz berechnet werden.

(3) Der Veranstalter hat der zuständigen Behörde alle Unterlagen vorzulegen und alle Auskünfte zu erteilen, die zur Überprüfung der ordnungsgemäßen Durchführung der Lotterie erforderlich sind.
Insbesondere hat er eine Abrechnung vorzulegen, aus der sich die tatsächliche Höhe der Einnahmen, des Reinertrages, der Gewinnausschüttung und der Kosten der Veranstaltung ergibt.

(4) Die zuständige Behörde kann auf Kosten des Veranstalters einen staatlich anerkannten Wirtschaftsprüfer beauftragen oder dessen Beauftragung vom Veranstalter verlangen, damit ein Gutachten zur Überprüfung der ordnungsgemäßen Planung oder Durchführung der Lotterie, insbesondere zur Angemessenheit der Kosten der Lotterie, erstattet und der Behörde vorgelegt wird.
Die Kosten des Gutachtens sind Kosten der Lotterie.

##### § 16  
Verwendung des Reinertrages

(1) Der Reinertrag der Veranstaltung muss zeitnah für den in der Erlaubnis festgelegten Zweck verwendet werden.

(2) Will der Veranstalter den Reinertrag für einen anderen als den in der Erlaubnis festgelegten gemeinnützigen, kirchlichen oder mildtätigen Zweck verwenden oder kann der Verwendungszweck nicht oder nicht zeitnah verwirklicht werden, hat der Veranstalter dies der zuständigen Behörde unverzüglich anzuzeigen.
Diese kann nach Anhörung des Veranstalters den Verwendungszweck neu festlegen.

(3) Ein angemessener Anteil des Reinertrages soll in dem Land verwendet werden, in dem die Lotterie veranstaltet wird.

##### § 17  
Form und Inhalt der Erlaubnis

Die Erlaubnis wird schriftlich erteilt.
In ihr sind insbesondere festzulegen

1.  der Veranstalter sowie im Fall des § 14 Abs. 2 der Dritte,
2.  Art, Ort oder Gebiet sowie Beginn und Dauer der Veranstaltung,
3.  der Verwendungszweck des Reinertrages, die Art und Weise des Nachweises der Verwendung und der Zeitpunkt, zu dem der Nachweis zu erbringen ist,
4.  der Spielplan und
5.  die Vertriebsform.

##### § 18  
Kleine Lotterien

Die Länder können von den Regelungen des Staatsvertrages für Lotterien abweichen, bei denen

1.  die Summe der zu entrichtenden Entgelte den Betrag von 40 000 Euro nicht übersteigt,
2.  der Reinertrag ausschließlich und unmittelbar für gemeinnützige, kirchliche oder mildtätige Zwecke verwandt wird und
3.  der Reinertrag und die Gewinnsumme jeweils mindestens 25 v.H.
    der Entgelte betragen.

#### Vierter Abschnitt  
Gewerbliche Spielvermittlung

##### § 19  
Gewerbliche Spielvermittlung

(1) Neben den §§ 4 bis 8 und unbeschadet sonstiger gesetzlicher Regelungen gelten für die Tätigkeit des gewerblichen Spielvermittlers folgende Anforderungen:

1.  Der gewerbliche Spielvermittler hat mindestens zwei Drittel der von den Spielern vereinnahmten Beträge für die Teilnahme am Spiel an den Veranstalter weiterzuleiten.
    Dies hat er durch einen zur unabhängigen Ausübung eines rechts- oder steuerberatenden Berufs befähigten Beauftragten zur Vorlage bei der Erlaubnisbehörde bestätigen zu lassen.
    Er hat die Spieler vor Vertragsabschluss in Textform klar und verständlich auf den für die Spielteilnahme an den Veranstalter weiterzuleitenden Betrag hinzuweisen sowie ihnen unverzüglich nach Vermittlung des Spielauftrages den Veranstalter mitzuteilen.
2.  Gewerbliche Spielvermittler und von ihnen oder den Spielinteressenten im Sinne des § 3 Abs. 6 beauftragte Dritte sind verpflichtet, bei jeder Spielteilnahme dem Veranstalter die Vermittlung offen zu legen.
3.  Gewerbliche Spielvermittler sind verpflichtet, dafür Sorge zu tragen, dass bei Vertragsabschluss ein zur unabhängigen Ausübung eines rechts- oder steuerberatenden Berufes befähigter Treuhänder mit der Verwahrung der Spielquittungen und der Geltendmachung des Gewinnanspruches gegenüber dem Veranstalter beauftragt wird.
    Dem Spieler ist bei Vertragsabschluss ein Einsichtsrecht an den Spielquittungen, die in seinem Auftrag vermittelt worden sind, einzuräumen.
    Wird ein Gewinnanspruch vom Spieler nicht innerhalb einer Frist von drei Monaten beim Treuhänder geltend gemacht, so ist der Gewinnbetrag an den Veranstalter abzuführen.

(2) Werden gewerbliche Spielvermittler in allen oder mehreren Ländern tätig, so werden die Erlaubnisse nach § 4 Abs. 1 Satz 1 gebündelt von der zuständigen Glücksspielaufsichtsbehörde des Landes Niedersachsen erteilt.
§ 9a Abs. 3, 5 bis 8 ist hierbei anzuwenden.

(3) § 4 Abs. 6 ist entsprechend anzuwenden.

#### Fünfter Abschnitt  
Besondere Vorschriften

##### § 20  
Spielbanken

(1) Zur Erreichung der Ziele des § 1 ist die Anzahl der Spielbanken in den Ländern zu begrenzen.

(2) Gesperrte Spieler dürfen am Spielbetrieb in Spielbanken nicht teilnehmen.
Die Durchsetzung des Verbots ist durch Kontrolle des Ausweises oder eine vergleichbare Identitätskontrolle und Abgleich mit der Sperrdatei zu gewährleisten.

##### § 21  
Sportwetten

(1) Wetten können als Kombinationswetten oder Einzelwetten auf den Ausgang von Sportereignissen oder Abschnitten von Sportereignissen erlaubt werden.
In der Erlaubnis sind Art und Zuschnitt der Sportwetten im Einzelnen zu regeln.

(2) In einem Gebäude oder Gebäudekomplex, in dem sich eine Spielhalle oder eine Spielbank befindet, dürfen Sportwetten nicht vermittelt werden.

(3) Die Veranstaltung und Vermittlung von Sportwetten muss organisatorisch, rechtlich, wirtschaftlich und personell getrennt sein von der Veranstaltung oder Organisation von Sportereignissen und dem Betrieb von Einrichtungen, in denen Sportveranstaltungen stattfinden.
Beteiligte, die direkt oder indirekt auf den Ausgang eines Wettereignisses Einfluss haben, sowie von diesen Personen beauftragte Dritte, dürfen keine Sportwetten auf den Ausgang oder den Verlauf des Sportereignisses abschließen, noch Sportwetten durch andere fördern.
Die zuständige Behörde kann weitere geeignete Maßnahmen zur Vermeidung von Wettmanipulationen wie die Einrichtung eines Frühwarnsystems verlangen.

(4) Die Verknüpfung der Übertragung von Sportereignissen in Rundfunk und Telemedien mit der Veranstaltung oder Vermittlung von Sportwetten ist nicht zulässig.
Wetten während des laufenden Sportereignisses sind unzulässig.
Davon abweichend können Sportwetten, die Wetten auf das Endergebnis sind, während des laufenden Sportereignisses zugelassen werden (Endergebniswetten); Wetten auf einzelne Vorgänge während des Sportereignisses (Ereigniswetten) sind ausgeschlossen.

(5) Gesperrte Spieler dürfen an Wetten nicht teilnehmen.
Die Durchsetzung des Verbots ist durch Kontrolle des Ausweises oder eine vergleichbare Identitätskontrolle und Abgleich mit der Sperrdatei zu gewährleisten.

##### § 22  
Lotterien mit planmäßigem Jackpot

(1) Die Höhe planmäßiger Jackpots ist zur Erreichung der Ziele des § 1 in der Erlaubnis zu begrenzen.
Lotterien mit planmäßigem Jackpot dürfen nicht häufiger als zweimal pro Woche veranstaltet werden.
Die Veranstaltung von Lotterien mit planmäßigem Jackpot ist auch in Kooperation mit anderen Lotterieveranstaltern grenzüberschreitend zulässig.
Die Auswirkungen auf die Bevölkerung sind mit einer wissenschaftlichen Begleituntersuchung zu evaluieren.

(2) Gesperrte Spieler dürfen an Lotterien der in § 10 Abs. 2 genannten Veranstalter, die häufiger als zweimal pro Woche veranstaltet werden, nicht teilnehmen.
Die Durchsetzung dieses Verbots ist durch Kontrolle des Ausweises oder eine vergleichbare Identitätskontrolle und Abgleich mit der Sperrdatei zu gewährleisten.

#### Sechster Abschnitt  
Datenschutz

##### § 23  
Sperrdatei, Datenverarbeitung

(1) Mit der Sperrdatei, die zentral von der zuständigen Behörde des Landes Hessen geführt wird, werden die für eine Sperrung erforderlichen Daten verarbeitet und genutzt.
Es dürfen folgende Daten gespeichert werden:

1.  Familiennamen, Vornamen, Geburtsnamen,
2.  Aliasnamen, verwendete Falschnamen,
3.  Geburtsdatum,
4.  Geburtsort,
5.  Anschrift,
6.  Lichtbilder,
7.  Grund der Sperre,
8.  Dauer der Sperre und
9.  meldende Stelle.

Daneben dürfen die Dokumente, die zur Sperrung geführt haben, gespeichert werden.

(2) Die gespeicherten Daten sind im erforderlichen Umfang an die Stellen zu übermitteln, die Spielverbote zu überwachen haben.
Die Datenübermittlung kann auch durch automatisierte Abrufverfahren erfolgen.

(3) Datenübermittlungen an öffentliche Stellen, insbesondere an Strafverfolgungsbehörden und Gerichte, sind nach den gesetzlichen Vorschriften zulässig.

(4) Erteilte Auskünfte und Zugriffe im elektronischen System sind zu protokollieren.

(5) Die Daten sind sechs Jahre nach Ablauf der Sperre zu löschen.
Es ist zulässig, die Löschung am Ende des sechsten Jahres vorzunehmen.

(6) Soweit in diesem Staatsvertrag nichts anderes bestimmt ist, sind die jeweiligen Vorschriften für den Schutz personenbezogener Daten anzuwenden, auch wenn die Daten nicht in Dateien verarbeitet oder genutzt werden.

#### Siebter Abschnitt  
Spielhallen

##### § 24  
Erlaubnisse

(1) Unbeschadet sonstiger Genehmigungserfordernisse bedürfen die Errichtung und der Betrieb einer Spielhalle einer Erlaubnis nach diesem Staatsvertrag.

(2) Die Erlaubnis ist zu versagen, wenn die Errichtung und der Betrieb einer Spielhalle den Zielen des § 1 zuwiderlaufen.
Sie ist schriftlich zu erteilen und zu befristen.
Die Erlaubnis kann, auch nachträglich, mit Nebenbestimmungen versehen werden.

(3) Das Nähere regeln die Ausführungsbestimmungen der Länder.

##### § 25  
Beschränkungen von Spielhallen

(1) Zwischen Spielhallen ist ein Mindestabstand einzuhalten (Verbot von Mehrfachkonzessionen).
Das Nähere regeln die Ausführungsbestimmungen der Länder.

(2) Die Erteilung einer Erlaubnis für eine Spielhalle, die in einem baulichen Verbund mit weiteren Spielhallen steht, insbesondere in einem gemeinsamen Gebäude oder Gebäudekomplex untergebracht ist, ist ausgeschlossen.

(3) Die Länder können die Anzahl der in einer Gemeinde zu erteilenden Erlaubnisse begrenzen.

##### § 26  
Anforderungen an die Ausgestaltung und den Betrieb von Spielhallen

(1) Von der äußeren Gestaltung der Spielhalle darf keine Werbung für den Spielbetrieb oder die in der Spielhalle angebotenen Spiele ausgehen oder durch eine besonders auffällige Gestaltung ein zusätzlicher Anreiz für den Spielbetrieb geschaffen werden.

(2) Die Länder setzen für Spielhallen zur Sicherstellung der Ziele des § 1 Sperrzeiten fest, die drei Stunden nicht unterschreiten dürfen.

#### Achter Abschnitt  
Pferdewetten

##### § 27  
Pferdewetten

(1) Pferdewetten dürfen nur mit einer Erlaubnis nach dem Rennwett- und Lotteriegesetz veranstaltet oder vermittelt werden.
Für die Vermittlung von Pferdewetten darf eine Erlaubnis nur erteilt werden, wenn die zuständigen deutschen Behörden den Abschluss dieser Pferdewetten im Inland oder den Betrieb eines Totalisators für diese Pferdewetten im Inland erlaubt haben.
§ 4 Abs. 2 Satz 1 und Abs. 3 sind anwendbar.

(2) § 4 Absatz 4 ist anwendbar.
Abweichend von Satz 1 kann das Veranstalten und Vermitteln von nach Absatz 1 erlaubten Pferdewetten im Internet unter den in § 4 Abs. 5 genannten Voraussetzungen im ländereinheitlichen Verfahren erlaubt werden.

(3) Auf Festquotenwetten finden § 8 Abs. 6 und § 21 Abs. 5 entsprechende Anwendung.

#### Neunter Abschnitt  
Übergangs- und Schlussbestimmungen

##### § 28  
Regelungen der Länder

Die Länder erlassen die zur Ausführung dieses Staatsvertrages notwendigen Bestimmungen.
Sie können weitergehende Anforderungen insbesondere zu den Voraussetzungen des Veranstaltens und Vermittelns von Glücksspielen festlegen.
In ihren Ausführungsgesetzen können sie auch vorsehen, dass Verstöße gegen die Bestimmungen dieses Staatsvertrages mit Geldbuße oder Strafe geahndet werden.

##### § 29  
Übergangsregelungen

(1) Die bis zum Inkrafttreten dieses Staatsvertrages erteilten Erlaubnisse der Veranstalter im Sinne des § 10 Abs. 2 und 3 und die ihnen nach Landesrecht gleichstehenden Befugnisse gelten - auch wenn im Bescheid eine kürzere Frist festgelegt ist - bis zum 31.
Dezember 2012 als Erlaubnis mit der Maßgabe fort, dass die Regelungen dieses Staatsvertrages - abgesehen vom Erlaubniserfordernis nach § 4 Abs. 1 Satz 1 - Anwendung finden.
Die Veranstalter nach § 10 Abs. 2 und 3 haben spätestens zum 1. Januar 2013 eine neue Erlaubnis nach § 4 Abs. 1 einzuholen.

(2) Absatz 1 findet entsprechende Anwendung auf die Vermittler von erlaubten öffentlichen Glücksspielen (einschließlich der Lotterie-Einnehmer der Klassenlotterien und der gewerblichen Spielvermittler).
Soweit Vermittler in die Vertriebsorganisation eines Veranstalters eingegliedert sind, stellt der Veranstalter den Antrag auf Erteilung der Erlaubnis nach § 4 Abs. 1 für die für ihn tätigen Vermittler.

(3) Die zuständige Behörde übernimmt die Führung der Sperrdatei nach § 23 Abs. 1 Satz 1 spätestens zum 1.
Juli 2013.
Zu diesem Zweck übermitteln die bislang für die Führung der Sperrdatei der Veranstalter nach § 10 Abs. 2 zuständigen Stellen die bei ihnen gespeicherten Spielersperren im Sinne des § 8 Abs. 2.
Bis zur Übernahme bleiben deren bislang bestehende Aufgaben unberührt; die Veranstalter nach § 10 Abs. 2 stellen die Berücksichtigung der nach § 8 Abs. 6 übermittelten Anträge auf Selbstsperren sicher.
Die Veranstalter nach § 10 Abs. 2 übernehmen jeweils hinsichtlich der Spieler, deren Wohnsitz in ihrem Geltungsbereich liegt, die Aufgabe nach § 8 Abs. 5 Satz 2, wenn der Veranstalter, der die Sperre verfügt hat, seine Erlaubnis oder Konzession nicht mehr nutzt.

(4) Die Regelungen des Siebten Abschnitts finden ab Inkrafttreten dieses Staatsvertrags Anwendung.
Spielhallen, die zum Zeitpunkt des Inkrafttretens dieses Staatsvertrags bestehen und für die bis zum 28.
Oktober 2011 eine Erlaubnis nach § 33i Gewerbeordnung erteilt worden ist, deren Geltungsdauer nicht innerhalb von fünf Jahren nach Inkrafttreten dieses Vertrages endet, gelten bis zum Ablauf von fünf Jahren nach Inkrafttreten dieses Vertrags als mit §§ 24 und 25 vereinbar.
Spielhallen, für die nach dem 28.
Oktober 2011 eine Erlaubnis nach § 33i Gewerbeordnung erteilt worden ist, gelten bis zum Ablauf von einem Jahr nach Inkrafttreten dieses Staatsvertrags als mit §§ 24 und 25 vereinbar.
Die für die Erteilung einer Erlaubnis nach § 24 zuständigen Behörden können nach Ablauf des in Satz 2 bestimmten Zeitraums eine Befreiung von der Erfüllung einzelner Anforderungen des § 24 Abs. 2 sowie § 25 für einen angemessenen Zeitraum zulassen, wenn dies zur Vermeidung unbilliger Härten erforderlich ist; hierbei sind der Zeitpunkt der Erteilung der Erlaubnis gemäß § 33i Gewerbeordnung sowie die Ziele des § 1 zu berücksichtigen.
Das Nähere regeln die Ausführungsbestimmungen der Länder.

(5) Buchmachererlaubnisse nach dem Rennwett- und Lotteriegesetz gelten im bisherigen Umfang bis zum Ablauf eines Jahres nach Inkrafttreten dieses Staatsvertrages fort.

##### § 30  
Weitere Regelungen

(1) Die zuständige Behörde kann eine Lotterie, die bei Inkrafttreten dieses Vertrages von mehreren Veranstaltern in allen Ländern durchgeführt wird und bei der der Reinertrag ausschließlich zur Erfüllung der in § 10 Abs. 5 genannten Zwecke verwandt wird, abweichend von § 12 Abs. 1 Satz 1 Nr. 3, § 13 Abs. 2, § 14 Abs. 1 Nr. 1 und § 15 Abs. 1 Satz 3 erlauben.

(2) Der Reinertrag von Veranstaltungen in der Form des Gewinnsparens muss mindestens 25 v.H.
der Entgelte betragen.
Der Reinertrag ist für gemeinnützige, kirchliche oder mildtätige Zwecke zu verwenden.
Erlaubnisse können allgemein erteilt werden.

##### § 31  
Verhältnis zu weiteren staatsvertraglichen Regelungen für die Klassenlotterien

(1) Soweit die Regelungen des Staatsvertrages zwischen den Ländern Baden-Württemberg, Bayern, Hessen, Rheinland-Pfalz, Sachsen und Thüringen über eine Staatliche Klassenlotterie vom 26. Mai 1992 (SKL-Staatsvertrag) oder die Regelungen des Staatsvertrages zwischen den Ländern Nordrhein-Westfalen, Niedersachsen, Schleswig-Holstein, Freie und Hansestadt Hamburg, Freie Hansestadt Bremen, Saarland, Berlin, Brandenburg, Mecklenburg-Vorpommern und Sachsen-Anhalt über eine Staatliche Klassenlotterie vom 30.Juni/1.September 2008 (NKL-Staatsvertrag) sowie die Regelungen des Staatsvertrages der Länder über die Gemeinsame Klassenlotterie vom 15. Dezember 2011 (GKL-Staatsvertrag) im Widerspruch zu Regelungen dieses Staatsvertrags stehen, sind die Regelungen dieses Staatsvertrags vorrangig anzuwenden.

(2) Mit Inkrafttreten dieses Staatsvertrages gehen die der Süddeutschen Klassenlotterie und der Nordwestdeutschen Klassenlotterie erteilten Erlaubnisse zur Veranstaltung von Klassenlotterien auf die Gemeinsame Klassenlotterie über.
Erlaubnisse nach § 4 werden Klassenlotterien abweichend von den jeweiligen Staatsverträgen von der nach diesem Staatsvertrag zuständigen Behörde erteilt.

##### § 32  
Evaluierung

Die Auswirkungen dieses Staatsvertrages, insbesondere der §§ 4a bis 4e, 9, 9a und 10a, auf die Entwicklung und Ausbreitung von unerlaubten Glücksspielen in Schwarzmärkten, sind von den Glücksspielaufsichtsbehörden der Länder unter Mitwirkung des Fachbeirats zu evaluieren.
Ein zusammenfassender Bericht ist fünf Jahre nach Inkrafttreten des Staatsvertrages vorzulegen.

##### § 33  
Revision zum Bundesverwaltungsgericht

In einem gerichtlichen Verfahren kann die Revision zum Bundesverwaltungsgericht auch darauf gestützt werden, dass das angefochtene Urteil auf der Verletzung der Bestimmungen dieses Staatsvertrages beruhe.

##### § 34  
Sprachliche Gleichstellung

Personen- und Funktionsbezeichnungen in diesem Staatsvertrag gelten jeweils in männlicher und weiblicher Form.

##### § 35  
Befristung, Fortgelten

(1) Die Ministerpräsidentenkonferenz kann aufgrund der Ergebnisse der Evaluierung (§ 32) mit mindestens 13 Stimmen die Befristung der Experimentierklausel in § 10a Abs. 1 aufheben.

(2) Dieser Staatsvertrag tritt mit Ablauf des 30.
Juni 2021 außer Kraft, sofern nicht die Ministerpräsidentenkonferenz mit mindestens 13 Stimmen das Fortgelten des Staatsvertrages beschließt.
In diesem Fall gilt der Staatsvertrag unter den Ländern fort, die dem Beschluss zugestimmt haben.

(3) Der Staatsvertrag kann von jedem der Länder, in denen er fortgilt, zum Schluss eines Kalenderjahres gekündigt werden.
Die Kündigung ist schriftlich gegenüber dem Vorsitzenden der Ministerpräsidentenkonferenz zu erklären.
Die Kündigung eines Landes lässt das zwischen den übrigen Ländern bestehende Vertragsverhältnis unberührt, jedoch kann jedes der übrigen Länder das Vertragsverhältnis binnen einer Frist von drei Monaten nach Eingang der Benachrichtigung über die gegenüber dem Vorsitzenden der Ministerpräsidentenkonferenz erfolgte Kündigungserklärung zum selben Zeitpunkt kündigen.

### Artikel 2  
Inkrafttreten, Außerkrafttreten, Neubekanntmachung

(1) Dieser Staatsvertrag tritt am 1.
Juli 2012 in Kraft.
Sind bis zum 30.
Juni 2012 nicht mindestens 13 Ratifikationsurkunden bei der Staatskanzlei des Landes Sachsen-Anhalt hinterlegt, wird der Staatsvertrag gegenstandslos.

(2) Die Staatskanzlei des Landes Sachsen-Anhalt teilt den Ländern die Hinterlegung der Ratifikationsurkunden mit.

(2a) Andere Länder können diesem Vertrag beitreten.
Der Beitritt erfolgt durch schriftliche Erklärung des Beitritts gegenüber der Staatskanzlei des Landes Sachsen-Anhalt und, soweit die Zustimmung der gesetzgebenden Körperschaft des beitretenden Landes erforderlich ist, mit deren Zustimmung.
Über den Eingang der Beitrittserklärung unterrichtet die Staatskanzlei des Landes Sachsen-Anhalt die übrigen vertragsschließenden Länder.
Die Regelungen dieses Vertrags treten für das beitretende Land am Tage nach dem Eingang der Beitrittserklärung bei der Staatskanzlei des Landes Sachsen-Anhalt in Kraft.
Soweit die Zustimmung der gesetzgebenden Körperschaft des beitretenden Landes erforderlich ist, treten die Regelungen für das beitretende Land am Tag nach dem Eingang der Anzeige dieser Zustimmung bei der Staatskanzlei des Landes Sachsen-Anhalt in Kraft.

(3) Mit Inkrafttreten dieses Staatsvertrages tritt der Staatsvertrag über die Regionalisierung von Teilen der von den Unternehmen des Deutschen Lotto- und Totoblocks erzielten Einnahmen in der Fassung der Bekanntmachung vom 20.
Juni 2004 außer Kraft.

(4) Mit Inkrafttreten dieses Staatsvertrages endet die Fortgeltung der Regelungen des Staatsvertrages zum Glücksspielwesen in Deutschland (Glücksspielstaatsvertrag – GlüStV) vom 30. Januar 2007/31. Juli 2007 nach den Ausführungsgesetzen der Länder.

### Anhang “Richtlinien zur Vermeidung und Bekämpfung von Glücksspielsucht”

Zur Vermeidung und Bekämpfung von Glücksspielsucht gelten die folgenden Richtlinien:

1.  Die Veranstalter
    1.  benennen Beauftragte für die Entwicklung von Sozialkonzepten,
    2.  erheben Daten über die Auswirkungen der von ihnen angebotenen Glücksspiele auf die Entstehung von Glücksspielsucht und berichten hierüber sowie über den Erfolg der von ihnen zum Spielerschutz getroffenen Maßnahmen alle zwei Jahre den Glücksspielaufsichtsbehörden,
    3.  schulen das für die Veranstaltung, Durchführung und gewerbliche Vermittlung öffentlichen Glücksspiels eingesetzte Personal in der Früherkennung problematischen Spielverhaltens, zum Beispiel dem plötzlichen Anstieg des Entgelts oder der Spielfrequenz,
    4.  schließen das in den Annahmestellen beschäftigte Personal vom dort angebotenen Glücksspiel aus,
    5.  ermöglichen es den Spielern, ihre Gefährdung einzuschätzen, und
    6.  richten eine Telefonberatung mit einer bundesweit einheitlichen Telefonnummer ein.
2.  Eine Information über Höchstgewinne ist mit der Aufklärung über die Wahrscheinlichkeit von Gewinn und Verlust zu verbinden.
3.  Die Vergütung der leitenden Angestellten von Glücksspielveranstaltern darf nicht abhängig vom Umsatz berechnet werden.

Für das Land Baden-Württemberg  
Berlin, den 15.
Dezember 2011  

  
Winfried Kretschmann

Für den Freistaat Bayern  
Berlin, den 15.
Dezember 2011  

  
Horst Seehofer

Für das Land Berlin  
Berlin, den 15.
Dezember 2011  

  
Klaus Wowereit

Für das Land Brandenburg  
Potsdam, den 15.
Dezember 2011  

  
Matthias Platzeck

Für die Freie Hansestadt Bremen  
Berlin, den 15.
Dezember 2011  

  
Jens Böhrnsen

Für die Freie und Hansestadt Hamburg  
Berlin, den 15.
Dezember 2011  

  
Olaf Scholz

Für das Land Hessen   
Berlin, den 15.
Dezember 2011  

  
Volker Bouffier

Für das Land Mecklenburg-Vorpommern   
Berlin, den 15.
Dezember 2011  

  
Erwin Sellering

Für das Land Niedersachsen   
Berlin, den 15.
Dezember 2011  

  
David McAllister

Für das Land Nordrhein-Westfalen  
Berlin, den 15.
Dezember 2011  

  
Hannelore Kraft

Für das Land Rheinland-Pfalz  
Berlin, den 15.
Dezember 2011  

  
Kurt Beck

Für das Saarland  
Berlin, den 15.
Dezember 2011  

  
Annegret Kramp-Karrenbauer

Für den Freistaat Sachsen  
Berlin, den 15.
Dezember 2011  

  
Stanislaw Tillich

Für das Land Sachsen-Anhalt  
Berlin, den 15.
Dezember 2011  

  
Reiner Haseloff

Für den Freistaat Thüringen  
Berlin, den 15.
Dezember 2011  

  
Christine Lieberknecht

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
1 Die Verpflichtungen aus der Richtlinie 98/34/EG des Europäischen Parlaments und des Rates vom 22.
Juni 1998 über ein Informationsverfahren auf dem Gebiet der Normen und technischen Vorschriften und der Vorschriften für die Dienste der Informationsgesellschaft (ABl. L 204 vom 21.
Juli 1998, S. 37), die zuletzt durch Richtlinie 2006/96/EG (ABl.
L 363 vom 20. Dezember 2006, S. 81) geändert worden ist, sind beachtet worden.

[zum Gesetz](/gesetze/glueaendstv_3)