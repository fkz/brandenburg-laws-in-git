## Verordnung über das Naturschutzgebiet „Reitweiner Sporn mit Priesterschlucht, Mühlen- und Zeisigberg“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29. Juli 2009 (BGBl. I S. 2542), von denen § 23 durch Artikel 2 des Gesetzes vom 4. August 2016 (BGBl. I S. 1972) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 und § 42 Absatz 2 Satz 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21. Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27. Mai 2013 (GVBl.
II Nr. 43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Märkisch-Oderland wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Reitweiner Sporn mit Priesterschlucht, Mühlen- und Zeisigberg“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 117 Hektar.
Es umfasst drei Teilflächen in folgenden Fluren:

Gemeinde:

Gemarkung: 

Flur: 

Flurstücke: 

Reitwein 

Reitwein 

9 

116 (anteilig), 117 bis 120, 121 (anteilig), 122 bis 153, 154 (anteilig); 

Podelzig 

Podelzig 

8 

64 (anteilig), 69 (anteilig), 70 (anteilig), 88, 205 (anteilig), 208 (anteilig),  
210 (anteilig), 303 (anteilig), 305 (anteilig), 306, 307 (anteilig), 308, 309,  
310 (anteilig), 331 bis 334, 336 (anteilig), 337 (anteilig), 339 (anteilig); 

Lindendorf 

Libbenichen 

8 

33 (anteilig), 36 (anteilig), 37, 38, 39 (anteilig).

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 3 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 6 aufgeführten Liegenschaftskarten.

(3) Für die in § 5 Absatz 1 Nummer 2 Buchstabe h benannten Flurstücke wird eine wirtschaftliche Nutzung ausgeschlossen.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Märkisch-Oderland, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes als Ausschnitt des steil abfallenden Geländes von der Lebuser Platte in das Oderbruch ist

1.  die Erhaltung, Entwicklung und Wiederherstellung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der basiphilen Trocken- und Halbtrockenrasen (Steppenrasen), Sandtrockenrasen, trockenen Sandheiden, Gras- und Staudenfluren, Laubgebüsche und Feldgehölze, naturnahen Laubwälder, Hutewälder, Quellen, Bäche, Gräben und extensiv genutzten Äcker;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Grauscheidiges Federgras (Stipa pennata), Haar-Pfriemengras (Stipa capillata), Ebensträußiges Gipskraut (Gypsophila fastigiata), Wiesen-Kuhschelle (Pulsatilla pratensis subspecies nigricans), Frühlings-Adonisröschen (Adonis vernalis), Ähriger Blauweiderich (Veronica spicata), Rispige Graslilie (Anthericum ramosum), Karthäuser-Nelke (Dianthus carthusianorum) und Sand-Strohblume (Helichrysum arenarium);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Insekten, Lurche, Kriechtiere, Vögel und Säugetiere, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Bibernell-Widderchen (Zygaena minos), Silbergrüner Bläuling (Polyommatus coridon), Veränderliches Widderchen (Zygaena ephialtes), Beilfleck-Widderchen (Zygaena loti), Rotbraunes Wiesenvögelchen (Coenonympha glycerion), Moorfrosch (Rana arvalis), Zauneidechse (Lacerta agilis), Grauammer (Emberiza calandra), Heidelerche (Lullula arborea), Neuntöter (Lanius collurio), Sperbergrasmücke (Sylvia nisoria), Mittelspecht (Dendrocopos medius), Schwarzspecht (Dryocopus martius), Wespenbussard (Pernis apivorus), Wiedehopf (Upupa epops), Rauhhautfledermaus (Pipistrellus nathusii), Braunes Langohr (Plecotus auritus) und Großer Abendsegler (Nyctalus noctula);
4.  die Erhaltung der steil abfallenden, überwiegend bewaldeten und teilweise als Weide genutzten Odertalhänge mit tiefen Erosionstälern wegen ihrer besonderen Eigenart und hervorragenden Schönheit;
5.  die Erhaltung der Priesterschlucht aus naturgeschichtlichen Gründen und des Reitweiner Burgwalls aus landeskundlichen Gründen;
6.  die Erhaltung der Hangwälder aus wissenschaftlichen Gründen zur Beobachtung und Erforschung ihrer Regeneration unter dem Einfluss der Robinie;
7.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Trockenrasen-Biotopverbundes am Rand des Odertales.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Reitweiner Sporn mit Priesterschlucht, Mühlen- und Zeisigberg“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), das die ehemaligen Gebiete von gemeinschaftlicher Bedeutung „Priesterschlucht“, „Zeisigberg“ und einen Teil des Gebietes von gemeinschaftlicher Bedeutung „Trockenrasen am Oderbruch“ umfasst, mit seinen Vorkommen von

1.  Trockenen europäischen Heiden als natürlichem Lebensraumtyp von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Trockenen, kalkreichen Sandrasen, Subpannonischem Steppen-Trockenrasen und Schlucht- und Hangmischwäldern (Tilio-Acerion) als prioritären natürlichen Lebensraumtypen im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes.

(3) Darüber hinaus ist besonderer Schutzzweck auf dem Flurstück 306 der Gemarkung Podelzig, Flur 8, auf den Flurstücken 118, 130, 141, 143 und 148 der Gemarkung Reitwein, Flur 9, die Erhaltung und die unbeeinflusste Entwicklung von Lebensstätten wild lebender Tierarten und Lebensgemeinschaften mit besonderer Bindung an Alt- und Totholz und die ungestörte Entwicklung naturnaher Laubwälder, insbesondere der Schlucht- und Hangmischwälder gemäß § 3 Absatz 2.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet bis zum 31.
    Juli eines jeden Jahres außerhalb der Wege und auf dem Zeisigberg und in der Priesterschlucht außerhalb der Pfade zu betreten;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
13.  Hunde frei laufen zu lassen;
14.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
15.  Düngemittel aller Art zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
16.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
17.  Tiere zu füttern oder Futter bereitzustellen;
18.  Tiere auszusetzen oder Pflanzen anzusiedeln;
19.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
20.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
21.  Pflanzenschutzmittel jeder Art anzuwenden;
22.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide mit einer Besatzdichte von maximal 1,4 Raufutter verwertenden Großvieheinheiten (RGV) pro Hektar im Jahresmittel genutzt wird und § 4 Absatz 2 Nummer 15, 21 und 22 gilt,
    2.  die Errichtung ortsunveränderlicher Anlagen zur Weidehaltung mit Zustimmung der unteren Naturschutzbehörde erfolgt;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung mit der Maßgabe, dass
    1.  eine Nutzung nur einzelstammweise bis truppweise erfolgt, wobei in Waldbereichen mit Dominanz der Robinie eine Nutzung ausschließlich einzelstammweise erfolgt,
    2.  nur Arten der potenziell natürlichen Vegetation in lebensraumtypischer Zusammensetzung eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
    3.  mindestens fünf dauerhaft markierte Stämme der lebensraumtypischen Baumarten je Hektar mit einem Durchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum Absterben und natürlichen Zerfall aus der Nutzung genommen sein müssen,
    4.  soweit möglich je Hektar mindestens fünf Stück dauerhaft markiertes stehendes Totholz der lebensraumtypischen Baumarten (mehr als 30 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß) nicht gefällt werden; starkes liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimeter am stärksten Ende) verbleibt im Bestand,
    5.  auf den Entwicklungsflächen des in § 3 Absatz 2 genannten Schlucht- und Hangmischwaldes (Tilio-Acerion) auf den Flurstücken 303 und 332 bis 334 der Gemarkung Podelzig, Flur 8 und auf den Flurstücken 116, 122, 125, 127, 133 bis 140, 149, 150 und 152 der Gemarkung Podelzig, Flur 9, eine naturnahe Waldentwicklung mit einem Totholzanteil von mindestens 10 Prozent des aktuellen Bestandesvorrates zu sichern ist,
    6.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    7.  § 4 Absatz 2 Nummer 21 gilt,
    8.  auf dem Flurstück 306 der Gemarkung Podelzig, Flur 8 und auf den Flurstücken 118, 130, 141, 143 und 148 der Gemarkung Reitwein, Flur 9, die forstwirtschaftliche Bodennutzung unzulässig ist;
3.  Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne einer Fischbestandskontrolle, -regulierung und -förderung mit Zustimmung der unteren Naturschutzbehörde; die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird;
4.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd,
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde; die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    3.  transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Errichtung anzuzeigen.
        Die Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht; die Entscheidung hierzu soll unverzüglich erfolgen,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope.
    
    Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und die Anlage und Unterhaltung von Wildäckern sind unzulässig.
    Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
    
5.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
6.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
7.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
8.  die Nutzung und Unterhaltung des Rast- und Festplatzes an der „Osterquelle“ Libbenichen (Gemeinde Lindendorf, Gemarkung Libbenichen, Flur 8, Flurstück 38) einschließlich der Durchführung eines Osterfeuers;
9.  die jährliche Durchführung eines Gottesdienstes der Kirchengemeinde in der Priesterschlucht;
10.  die Imbissversorgung geführter Wandergruppen in der Priesterschlucht am Talweg im Westteil des Flurstücks 210 der Gemarkung Podelzig, Flur 8, jährlich zur Blütezeit des Adonisröschens in Abstimmung mit dem Flächennutzer;
11.  die Unterhaltung vorhandener hölzerner Treppenstufen, Geländer, Sitzbänke und Informationstafeln im Einvernehmen mit der unteren Naturschutzbehörde;
12.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und in bisherigem Umfang;
13.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
14.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der zuständigen Naturschutzbehörde zugelassen oder angeordnet worden sind;
15.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Tourismus im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hin​weiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl. S. 1734), die durch die Bekanntmachung vom 1.
    Oktober 2013 (ABl.
    S. 2811) geändert worden ist, an Straßen und Wegen freigestellt;
16.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungserfordernissen, die sich aus anderen Vorgaben ergeben, unberührt.

#### § 6 Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  Trockenrasenbiotope sollen mindestens zweimal jährlich mit Schafen und Ziegen in Koppelhaltung im Kurzumtrieb, nötigenfalls in Hütehaltung, auf Grundlage eines regelmäßig zu aktualisierenden Weideregimes beweidet werden.
    Alternativ können ein- bis zweischürige Mahd oder Mähweide durchgeführt werden;
2.  als Initialmaßnahme sollen auf verbuschten Trockenrasen Gehölze für eine Beweidung in erforderlichem Umfang entfernt und die Krautschicht gemäht und abgeräumt oder abgeflämmt werden;
3.  Teile vorhandener Wald- oder Gehölzflächen sollen gemäß des Managementplans oder nach einvernehmlicher Abstimmung mit der Forstbehörde zu Hutewald entwickelt und Streuschichtauflagen, soweit erforderlich, entfernt werden;
4.  bei der Bewirtschaftung der Waldflächen sollen
    1.  in Abstimmung mit der unteren Naturschutzbehörde nichtheimische invasive Baumarten, insbesondere die Robinie, durch Endnutzung auf Kleinkahlschlägen in einem Umfang von 0,5 bis 1 Hektar mit nachfolgender Pflanzung von Baumarten der natürlichen Waldgesellschaft oder durch entsprechende Voranbauten nach Auflichtung (flächenhaft, als Trupps oder Nester) zurückgedrängt werden; um Stock- und Wurzelausschläge der Robinie nach der Ernte gering zu halten, soll deren Rinde zunächst bis auf einen verbleibenden Steg geringelt und die Stämme erst im Folgejahr entnommen werden,
    2.  der Laubholzunter- beziehungsweise -zwischenstand aus Baumarten der natürlichen Waldgesellschaft durch Freistellung begünstigt und in die nächste Bestandesgeneration übernommen werden,
    3.  die vorhandene Naturverjüngung von Baumarten der natürlichen Waldgesellschaft bei der Bewirtschaftung berücksichtigt werden,
    4.  die Baumarten der natürlichen Waldgesellschaften, insbesondere Hainbuche und Winterlinde, soweit möglich auch durch Saat ergänzt werden,
    5.  die Verjüngungen von nichtheimischen invasiven Baumarten möglichst frühzeitig beseitigt werden, um den Aufwand einer späteren Entnahme der invasiven Art gering zu halten;
5.  der Bereich des Grabens an der „Osterquelle“ am Ostrand der Teilfläche Mühlenberg (Gemarkung Libbenichen, Flur 8, Flurstück 33) soll als Amphibienlebensraum erhalten und entwickelt werden, indem insbesondere
    1.  ausreichend große unbeschattete Wasserflächen mit geringer Fließgeschwindigkeit erhalten oder entwickelt werden und entsprechend den Ansprüchen des Lebensraums schützende Vegetation, Laub und Totholz im und am Gewässer verbleiben,
    2.  der Rast- und Festplatz auf der Westseite des Grabens auf dem Flurstück 38 der Gemarkung Libbenichen, Flur 8, amphibiengerecht unterhalten wird.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten, Außerkrafttreten

(1) § 5 Absatz 1 Nummer 1 Buchstabe a tritt am 1.
Januar 2018 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig tritt die Anordnung Nummer 3 über Naturschutzgebiete vom 11. September 1967 für die Naturschutzgebiete „Priesterschlucht“ und „Zeisigberg bei Wuhden“, veröffentlicht im Gesetzblatt der DDR Teil II Nummer 95 am 19.
Oktober 1967, außer Kraft.

Potsdam, den 15.
März 2017

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes "Reitweiner Sporn mit Priesterschlucht, Mühlen- und Zeisigberg"](/br2/sixcms/media.php/68/GVBl_II_16_2017-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes ") 1.5 MB

2

[Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten](/br2/sixcms/media.php/68/GVBl_II_16_2017-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten") 601.1 KB