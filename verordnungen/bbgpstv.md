## Brandenburgische Verordnung zur Ausführung des Personenstandsgesetzes (Brandenburgische Personenstandsverordnung - BbgPStV)

Abschnitt 1  
Standesbeamtinnen und Standesbeamte
-------------------------------------------------

#### § 1  
Bestellung

(1) Die Standesbeamtinnen und Standesbeamten werden von den Aufgabenträgern, die nach dem Personenstandsausführungsgesetz ein Standesamt führen, durch Aushändigung einer Urkunde bestellt.
Arbeiten Aufgabenträger nach den Bestimmungen über die kommunale Zusammenarbeit durch Vereinbarung zusammen, obliegt die Bestellung dem Aufgabenträger, der die Aufgabe durchführt.

(2) Die Bestellung ist auf Widerruf auszusprechen.
Sie kann mit Nebenbestimmungen versehen werden.
Eine Beschränkung der Bestellung auf bestimmte Aufgaben ist nicht zulässig.

(3) Für jedes Standesamt sind Standesbeamtinnen oder Standesbeamte in der erforderlichen Anzahl, mindestens jedoch zwei Personen zu bestellen.
Die erforderliche Anzahl richtet sich nach den örtlichen Gegebenheiten, insbesondere nach der Größe des örtlichen Zuständigkeitsbereichs und der Anzahl der Beurkundungen.
Standesbeamtinnen und Standesbeamte, die gemäß Absatz 4 bestellt oder gemäß § 3a beauftragt wurden, dürfen nicht in die Berechnung der erforderlichen Anzahl nach den Sätzen 1 und 2 einbezogen werden.

(4) Zur Gewährleistung der Arbeitsfähigkeit des Standesamtes auch in Ausnahmesituationen kann ein Aufgabenträger nach Absatz 1 ergänzend zu den Bestellungen nach Absatz 3 Standesbeamtinnen oder Standesbeamte eines anderen Aufgabenträgers bestellen.
Sofern eine außergewöhnliche Notsituation dies erfordert, können mit vorheriger Zustimmung durch die untere Fachaufsichtsbehörde zeitlich befristet auch Standesbeamtinnen oder Standesbeamte weiterer Aufgabenträger bestellt werden.
Eine Verlängerung der Frist ist mit Zustimmung der unteren Fachaufsicht möglich, wenn die außergewöhnliche Notsituation andauert.
Einzelheiten der Bestellungen nach den Sätzen 1 und 2 regeln die beteiligten Aufgabenträger durch öffentlich-rechtlichen Vertrag.

(5) Die Bestellung ist der unteren Fachaufsichtsbehörde mitzuteilen.
Der unteren Fachaufsichtsbehörde ist gleichzeitig das Vorliegen der fachlichen Voraussetzungen nach § 2 Absatz 1 und 2 nachzuweisen.
§ 2 Absatz 1 Satz 2 bleibt unberührt.

#### § 2  
Fachliche Eignung

(1) Die nach § 2 Absatz 3 des Personenstandsgesetzes für die Bestellung erforderliche fachliche Eignung besitzt in der Regel, wer die Befähigung für die Laufbahn des gehobenen allgemeinen Verwaltungsdienstes oder als Tarifbeschäftigte oder als Tarifbeschäftigter eine vergleichbare Qualifikation besitzt und sich während einer mindestens sechsmonatigen Tätigkeit als Sachbearbeiterin oder Sachbearbeiter im Standesamt bewährt hat.
Werden diese Voraussetzungen nicht erfüllt, so ist im Ausnahmefall die Bestellung mit Zustimmung der unteren Fachaufsichtsbehörde zulässig.
Die Zustimmung darf nur erteilt werden, wenn die fachliche Eignung auf andere Weise nachgewiesen oder durch Nebenbestimmungen gemäß § 1 Absatz 2 Satz 2 sichergestellt wird.

(2) Die Standesbeamtinnen und Standesbeamten müssen vor ihrer Bestellung mit Erfolg an einem Grundseminar für neu zu bestellende Standesbeamtinnen und Standesbeamte teilgenommen haben.

(3) Die Standesbeamtinnen und Standesbeamten sind verpflichtet, sich zur Aufrechterhaltung der fachlichen Eignung regelmäßig auf den Gebieten des Personenstands-, Familien-, Namens- und Staatsangehörigkeitsrechts sowie des internationalen Privatrechts fortzubilden.
Die Aufgabenträger regeln und gewährleisten die fachbezogene Fortbildung; sie können deren Art und Umfang festlegen.
Insbesondere können sie die Teilnahme an halbjährlich stattfindenden eintägigen Schulungen sowie an mehrtägigen Seminaren innerhalb von längstens vier Jahren vorschreiben.
Gegenüber der unteren Fachaufsichtsbehörde ist ein Nachweis über die absolvierten Fortbildungen zu führen.

#### § 3  
Beendigung der Bestellung

(1) Die Bestellung erlischt, wenn die Standesbeamtin oder der Standesbeamte aus dem Dienst- oder Beschäftigungsverhältnis zu dem bestellenden Aufgabenträger ausscheidet oder in den Ruhestand oder die Freistellungsphase der Altersteilzeit eintritt.
In den Fällen des § 1 Absatz 4 Satz 2 endet die Bestellung mit Ablauf der Frist.

(2) Die Bestellung kann jederzeit schriftlich widerrufen werden.
In den Fällen des § 1 Absatz 4 Satz 2 ist die Bestellung zu widerrufen, wenn die untere oder oberste Fachaufsichtsbehörde vor Ablauf der Frist feststellt, dass die außergewöhnliche Notsituation beendet ist.

(3) Erweist sich eine Standesbeamtin oder ein Standesbeamter persönlich oder fachlich als ungeeignet, ist die Bestellung zu widerrufen.
Die Bestellung ist zu widerrufen, wenn die Standesbeamtin oder der Standesbeamte die verpflichtenden Fortbildungsveranstaltungen nach § 2 Absatz 3 über einen Zeitraum von zwei Jahren nicht wahrgenommen oder in einem Zeitraum von mehr als einem Jahr keine Eintragung in einem Personenstandsregister beurkundet hat.
Von dem Widerruf nach Satz 2 kann nur im Ausnahmefall mit Zustimmung der unteren Fachaufsichtsbehörde abgesehen werden.

(4) Die untere oder die oberste Fachaufsichtsbehörde kann in den Fällen des Absatzes 3 den Widerruf der Bestellung anordnen.

(5) Die Beendigung der Bestellung ist der unteren Fachaufsichtsbehörde mitzuteilen.

#### § 3a  
Beauftragung im Notfall

(1) Ist die Arbeitsfähigkeit des Standesamtes trotz öffentlich-rechtlicher Verträge nach § 1 Absatz 4 oder wegen des Fehlens solcher Verträge nicht gewährleistet, kann die untere Fachaufsichtsbehörde auf Antrag des betroffenen Aufgabenträgers eine Standesbeamtin oder einen Standesbeamten eines anderen Aufgabenträgers mit dessen Zustimmung mit der Wahrnehmung der Geschäfte beauftragen.
Der antragstellende Aufgabenträger hat dem anderen Aufgabenträger die durch die Beauftragung entstandenen Kosten zu erstatten.
Jeder Aufgabenträger teilt der unteren Fachaufsichtsbehörde mindestens eine Standesbeamtin oder einen Standesbeamten mit, die oder der gemäß Satz 1 beauftragt werden kann.

(2) Die untere Fachaufsichtsbehörde beendet die Beauftragung nach Absatz 1 Satz 1, wenn die Arbeitsfähigkeit des Standesamtes nach Mitteilung des Aufgabenträgers, für den die Beauftragung erfolgt ist, wieder gewährleistet ist.
Die Beauftragung ist ebenfalls zu beenden, sofern der andere Aufgabenträger darlegt, dass seine Arbeitsfähigkeit andernfalls gefährdet ist.

Abschnitt 2   
Elektronische Registerführung
--------------------------------------------

### Unterabschnitt 1  
Zentrales elektronisches Personenstandsregister

#### § 4  
Einrichtung und Betrieb eines zentralen elektronischen Personenstandsregisters

(1) Bei der Stadt Cottbus/Chóśebuz kann ein zentrales elektronisches Personenstandsregister im Sinne des § 67 Absatz 1 des Personenstandsgesetzes zwischen denjenigen Aufgabenträgern eingerichtet und betrieben werden, mit denen die Stadt Cottbus/Chóśebuz eine entsprechende Vereinbarung trifft.
Die Einrichtung bedarf der Feststellung durch das Ministerium des Innern.
Die Feststellung wird am Tag nach ihrer Veröffentlichung im Amtsblatt für Brandenburg wirksam.

(2) In der Vereinbarung nach Absatz 1 ist zu regeln, dass Aufgabenträger, deren Standesämter an dem zentralen elektronischen Personenstandsregister teilnehmen, die bei der Stadt Cottbus im Rahmen der Auftragsdatenverarbeitung gespeicherten Registereinträge ihrer Standesämter gegenseitig im Sinne des § 67 Absatz 3 des Personenstandsgesetzes zur Verfügung stellen.

(3) Das zentrale elektronische Personenstandsregister wird von der Stadt Cottbus/Chóśebuz als automatisiertes Verfahren zum Abruf personenbezogener Daten aus den Personenstandsregistern der teilnehmenden Aufgabenträger betrieben.

#### § 5  
Zugriffe auf Einträge des zentralen elektronischen Personenstandsregisters

(1) Die Standesämter der teilnehmenden Aufgabenträger erhalten lesenden Zugriff gemäß § 14 Absatz 1 Satz 1 Nummer 3 der Personenstandsverordnung auf die Gesamtheit der Personenstandseinträge, um die Benutzung im Sinne des § 67 Absatz 3 des Personenstandsgesetzes zu ermöglichen.

(2) Die Zugriffsberechtigung wird durch ein elektronisches Authentifizierungsverfahren nachgewiesen, das dem hohen Schutzbedarf der Daten angemessen ist und dem jeweiligen Stand der Technik entspricht.
Zugriffsberechtigte Personen identifizieren sich durch die Angabe des Standesamts und einer personengebundenen Kennung.

(3) Ein Zugriff auf mit Sperrvermerk versehene Personenstandseinträge ist nicht zulässig.
Anfragende Standesämter erhalten lediglich einen Hinweis auf das Register führende Standesamt.

(4) Der Zugriff erfolgt über das Landesverwaltungsnetz.
Im Ausnahmefall kann ein anderer Verbindungsweg mit dem Betreiber des zentralen elektronischen Personenstandsregisters vereinbart werden.
In beiden Fällen sind geeignete, dem hohen Schutzbedarf der übertragenen Daten und dem Stand der Technik entsprechende Maßnahmen zur Sicherung der Vertraulichkeit, Integrität, Authentizität und Revisionsfähigkeit vorzusehen.

#### § 6  
_(aufgehoben)_

#### § 7  
Protokollierung

(1) Abrufe sind durch den Betreiber zu protokollieren.
Die Protokolle sind vier Jahre nach Ablauf des Kalenderjahres, in dem der Zugriff erfolgt ist, aufzubewahren.
Die Protokolldatenbestände müssen eine automatisierte Auswertung nach zugreifender Stelle, abgefragtem Standesamt, betroffener Person und Zeitpunkt des Zugriffs zulassen.

(2) Zum Zweck der Kontrolle der Rechtmäßigkeit von Datenabrufen und zur Gewährleistung von Betroffenenrechten sind dem verantwortlichen Aufgabenträger die hierfür erforderlichen Protokolldaten auf Anfrage zur Verfügung zu stellen.

(3) Wurden konkrete Anhaltspunkte für unerlaubte Zugriffe festgestellt, sind die Leitung und die oder der Datenschutzbeauftragte des registerführenden sowie des abrufenden Aufgabenträgers unverzüglich zu informieren.
Ergänzend zu den Artikeln 33 und 34 der Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates von 27.
April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr und zur Aufhebung der Richtlinie 95/46/EG (Datenschutz-Grundverordnung) (ABl. L 119 vom 4.5.2016, S. 1; L 314 vom 22.11.2016, S. 72; L 127 vom 23.5.2018, S.
2) ist die jeweils zuständige untere Fachaufsichtsbehörde sowie der Betreiber des zentralen elektronischen Personenstandsregisters zu informieren.

#### § 8  
Verantwortlichkeiten

(1) Der Betreiber ist zuständig für die Einhaltung der für die Einrichtung und den Betrieb des zentralen elektronischen Personenstandsregisters geltenden Vorschriften.
Soweit der Betreiber dabei personenbezogene Daten verarbeitet, ist er Verantwortlicher im Sinne von Artikel 4 Nummer 7 der Verordnung (EU) 2016/679.

(2) Rufen die teilnehmenden Aufgabenträger personenbezogene Daten mittels des zentralen elektronischen Personenstandsregisters ab, sind sie insoweit Verantwortliche im Sinne von Artikel 4 Nummer 7 der Verordnung (EU) 2016/679.
Stellt ein Aufgabenträger Verfahrensmängel fest, hat er den Betreiber unverzüglich darüber zu informieren.

(3) Um sicherzustellen, dass der Aufgabenträger seine Verantwortung nach Absatz 2 Satz 1 wahrnehmen kann, gewährleistet der Betreiber

1.  die Bereitstellung der für das Verzeichnis von Verarbeitungstätigkeiten und notwendige Datenschutz-Folgenabschätzungen des Aufgabenträgers nach Artikel 30 Absatz 1 und Artikel 35 Absatz 1 der Verordnung (EU) 2016/679 erforderlichen Angaben sowie sonstiger, für die Erfüllung der Verpflichtungen des Verantwortlichen benötigter, Informationen auf Anfrage,
2.  die Bereitstellung der für das Sicherheitskonzept des Aufgabenträgers nach § 16 Absatz 1 Satz 2 des Brandenburgischen E-Government-Gesetzes vom 23.
    November 2018 (GVBl. I Nr. 28) in der jeweils geltenden Fassung erforderlichen Informationen und
3.  die unverzügliche Information des Aufgabenträgers über bekannt gewordene Verfahrensmängel bei der Verarbeitung personenbezogener Daten und die voraussichtliche Dauer der Mängelbeseitigung.

(4) Die in den Absätzen 1 und 2 genannten Stellen sind, soweit nach diesen Vorschriften eine Verantwortlichkeit besteht, auch für die Einhaltung der in den Artikeln 12 bis 21 sowie 33 und 34 der Verordnung (EU) 2016/679 geregelten Verpflichtungen zuständig.
Der abrufende Aufgabenträger und gegebenenfalls der Betreiber übermitteln auf Anforderung die hierfür notwendigen Angaben.

### Unterabschnitt 2  
Allgemeine Vorschriften zur elektronischen Registerführung

#### § 9  
Datenschutz-Folgenabschätzung

Werden personenbezogene Daten in elektronischen Personenstandsregistern oder aufgrund dieser Verordnung verarbeitet, ist eine Datenschutz-Folgenabschätzung auf der Grundlage einer Risikobewertung nur in Bezug auf die in Artikel 35 Absatz 7 Buchstabe d der Verordnung (EU) 2016/679 genannten technischen und organisatorischen Maßnahmen durchzuführen.
§ 4 des Brandenburgischen Datenschutzgesetzes vom 8.
Mai 2018 (GVBl.
I Nr.
7), das durch Artikel 7 des Gesetzes vom 19.
Juni 2019 (GVBl.
I Nr.
43 S.
38) geändert worden ist, in der jeweils geltenden Fassung bleibt unberührt.

#### § 10  
Elektronische Sammelakte

(1) Werden Sammelakten elektronisch geführt, ist eine Verknüpfung mit den elektronischen Personenstandsregistern zu gewährleisten.

(2) Sammelakten sind nicht Bestandteil des zentralen elektronischen Personenstandsregisters.

#### § 11  
Untere Fachaufsichtsbehörden

(1) Den unteren Fachaufsichtsbehörden ist zur Wahrnehmung ihrer Aufsichtsaufgaben lesender Zugriff auf die elektronischen Personenstandsregister zu gewähren.

(2) Werden Sammelakten elektronisch geführt, gilt Absatz 1 entsprechend.

(3) Für den Zugriff gilt § 5 Absatz 2 und 4 entsprechend.

Abschnitt 3  
Übergangsvorschriften
-----------------------------------

#### § 12  
Weitergeltung von Bestellungen

Die vor Inkrafttreten dieser Verordnung erfolgten Bestellungen von Standesbeamtinnen und Standesbeamten gelten fort.
Für die Beendigung gilt § 3.