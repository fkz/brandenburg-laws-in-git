## Verordnung über die Ausbildung und Prüfung für den gehobenen nichttechnischen Dienst bei der Deutschen Rentenversicherung Berlin-Brandenburg (APOgehDDRVBln-Bbg)

Auf Grund des § 26 Absatz 1 und des § 12 Absatz 4 des Landesbeamtengesetzes vom 3. April 2009 (GVBl.
I S.
26) verordnet der Minister für Arbeit, Soziales, Frauen und Familie im Einvernehmen mit dem Minister des Innern und dem Minister der Finanzen:

### § 1  
Geltungsbereich

(1) Die Verordnung regelt die Ausbildung und Prüfung für den gehobenen nichttechnischen Dienst bei der Deutschen Rentenversicherung Berlin-Brandenburg.

(2) Die Durchführung der Ausbildung und Prüfung richtet sich nach der Verordnung über den Vorbereitungsdienst für den gehobenen nichttechnischen Dienst des Bundes in der Sozialversicherung vom 20.
November 2014 (BGBl.
I S.
1752) in der jeweils geltenden Fassung, sofern nicht nachfolgend Abweichendes geregelt ist.

### § 2  
Einstellung in den Vorbereitungsdienst

(1) Der Vorbereitungsdienst für den gehobenen nichttechnischen Dienst bei der Deutschen Rentenversicherung Berlin-Brandenburg wird als Studiengang „Sozialversicherungsrecht LL.B.“ am Fachbereich Sozialversicherung der Hochschule des Bundes für öffentliche Verwaltung abgeleistet.

(2) Einstellungsbehörde ist die Deutsche Rentenversicherung Berlin-Brandenburg.
Ihr obliegt die Entscheidung über die Einstellung in den Vorbereitungsdienst und die Betreuung der Studierenden.

(3) Der Entscheidung über die Einstellung geht ein von der Deutschen Rentenversicherung Berlin-Brandenburg bestimmtes Auswahlverfahren voraus.
Die Deutsche Rentenversicherung Berlin-Brandenburg regelt, welches Verfahren bei der Auswahl anzuwenden ist.

### § 3  
Dauer des Vorbereitungsdienstes

(1) Der Vorbereitungsdienst dauert in der Regel drei Jahre und schließt mit der Bachelorprüfung als Laufbahnprüfung ab.

(2) Die praktische Ausbildung kann bis auf sechs Monate gekürzt werden, soweit Zeiten einer geeigneten berufspraktischen Ausbildung oder für die Laufbahnbefähigung gleichwertige berufliche Tätigkeiten nachgewiesen worden sind.
Tätigkeiten von Beschäftigten im öffentlichen Dienst können berücksichtigt werden, wenn sie denjenigen von Beamtinnen und Beamten des gehobenen Dienstes gleichwertig sind.

(3) Der Vorbereitungsdienst endet abweichend von Absatz 1, wenn die nach der Studienordnung vorgesehenen Prüfungen endgültig nicht bestanden sind.

(4) Der Vorbereitungsdienst ist im Einzelfall zu verlängern, wenn die Ausbildung

1.  wegen längerer Krankheit,
2.  wegen Zeiten eines Beschäftigungsverbotes oder einer Schutzfrist nach den geltenden Bestimmungen über den Mutterschutz oder einer Elternzeit,
3.  durch Ableisten des Grundwehrdienstes oder eines Ersatzdienstes oder
4.  aus anderen zwingenden Gründen unterbrochen worden und die zielgerechte Fortsetzung des Vorbereitungsdienstes nicht gewährleistet ist.

Über die Verlängerung und deren Umfang entscheidet die Einstellungsbehörde.

### § 4  
Laufbahnbefähigung

(1) Mit Bestehen der Laufbahnprüfung erwerben die Studierenden die Befähigung für die Laufbahn des gehobenen nichttechnischen Dienstes bei der Deutschen Rentenversicherung Berlin-Brandenburg.

(2) Das auszuhändigende Abschlusszeugnis enthält abweichend von § 29 Absatz 2 Nummer 1 der Verordnung über den Vorbereitungsdienst für den gehobenen nichttechnischen Dienst des Bundes in der Sozialversicherung die Feststellung, dass der oder die Studierende die Laufbahnprüfung bestanden und die Befähigung für die Laufbahn des gehobenen nichttechnischen Dienstes bei der Deutschen Rentenversicherung Berlin-Brandenburg erlangt hat.

(3) Die Befähigung für die Laufbahn des gehobenen nichttechnischen Dienstes bei der Deutschen Rentenversicherung Berlin-Brandenburg besitzt auch, wer den Studiengang „Sozialversicherung LL.B.“ am Fachbereich Sozialversicherung der Hochschule des Bundes für öffentliche Verwaltung außerhalb des in dieser Verordnung geregelten Vorbereitungsdienstes erfolgreich abgeschlossen hat.

### § 5  
Übergangsregelungen

(1) Für Studierende, die mit dem Vorbereitungsdienst bei der Deutschen Rentenversicherung Berlin-Brandenburg vor dem 1.
September 2014 begonnen haben, richtet sich die Durchführung der Ausbildung und Prüfung nach der Verordnung über die Ausbildung und Prüfung für den gehobenen nichttechnischen Dienst des Bundes in der Sozialversicherung vom 22. November 2010 (BGBl. I S. 1625) mit der Maßgabe, dass § 31 Absatz 1 der Verordnung über den Vorbereitungsdienst für den gehobenen nichttechnischen Dienst des Bundes in der Sozialversicherung anzuwenden ist.

(2) Für Studierende, die mit dem Vorbereitungsdienst bei der Deutschen Rentenversicherung Berlin-Brandenburg nach dem 31.
August 2014, aber vor dem 1.
September 2015 begonnen haben, richtet sich die Durchführung der Ausbildung und Prüfung nach der Verordnung über die Ausbildung und Prüfung für den gehobenen nichttechnischen Dienst des Bundes in der Sozialversicherung vom 22. November 2010 (BGBl.
I S.
1625) mit der Maßgabe, dass § 31 Absatz 2 der Verordnung über den Vorbereitungsdienst für den gehobenen nichttechnischen Dienst des Bundes in der Sozialversicherung anzuwenden ist.

### § 6  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über die Ausbildung und Prüfung für die Laufbahn des gehobenen nichttechnischen Dienstes bei der Deutschen Rentenversicherung Berlin-Brandenburg vom 11.
März 2002 (GVBl.
II S.
166), die durch Artikel 3 des Gesetzes vom 8.
März 2006 (GVBl.
I S.
38) geändert worden ist, außer Kraft.

Potsdam, den 10.
August 2011

Der Minister für Arbeit, Soziales, Frauen und Familie

Günter Baaske