## Verordnung zur Durchführung des Vermögensgesetzes, des Entschädigungsgesetzes, des Ausgleichsleistungsgesetzes und des DDR-Entschädigungserfüllungsgesetzes (Vermögensgesetzdurchführungsverordnung - VermGDV)

_Hinweis: Die Verordnung vom 7.
März 2022 (GVBl.
II Nr.
26) tritt am 1.
Juni 2022 in Kraft._

Auf Grund des § 23 Absatz 2, des § 25 Absatz 2 und des § 28 Absatz 2 des Vermögensgesetzes in der Fassung der Bekanntmachung vom 9.
Februar 2005 (BGBl.
I S.
205) auch in Verbindung mit § 9 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S.
186), des § 12 Absatz 1 Satz 1 des Entschädigungsgesetzes in der Fassung der Bekanntmachung vom 13.
Juli 2004 (BGBl. I S. 1658), des § 6 Absatz 2 des Ausgleichsleistungsgesetzes in der Fassung der Bekanntmachung vom 13.
Juli 2004 (BGBl.
I S.
1665) in Verbindung mit § 12 Absatz 1 Satz 2 des Entschädigungsgesetzes, des § 4 Satz 4 und des § 6 Satz 1 des DDR-Entschädigungserfüllungsgesetzes vom 10.
Dezember 2003 (BGBl.
I S.
2471, 2473; 2004 I S. 1654) jeweils in Verbindung mit § 23 Absatz 2, § 25 Absatz 2 und § 28 Absatz 2 des Vermögensgesetzes und mit § 16 Absatz 2 des Landesorganisationsgesetzes verordnet die Landesregierung:

#### § 1  
Zuständigkeit

(1) Für Rückübertragungsverfahren nach § 3 des Vermögensgesetzes sind die Landkreise und die kreisfreien Städte für ihr Kreis- oder Stadtgebiet zuständig, soweit nicht nachfolgend etwas anderes geregelt ist.
Der Landkreis Oder-Spree nimmt neben den Aufgaben für sein Gebiet die Aufgaben für die Gebiete der Landkreise Barnim, Elbe-Elster, Havelland, Märkisch-Oderland, Oberhavel, Oberspreewald-Lausitz, Ostprignitz-Ruppin, Potsdam-Mittelmark, Prignitz, Spree-Neiße und Uckermark sowie der kreisfreien Städte Brandenburg an der Havel, Cottbus und Frankfurt (Oder) wahr.
Die Landeshauptstadt Potsdam nimmt neben den Aufgaben für ihr Stadtgebiet die Auf-gaben für das Gebiet des Landkreises Teltow-Fläming wahr.
Der Landkreis Oberhavel bleibt zuständig für seine am 31.
Dezember 2005 anhängigen Gerichtsverfahren.

(2) In Verfahren nach Absatz 1, in denen die Rückübertragung ausgeschlossen ist oder der Berechtigte Entschädigung gewählt hat (Singularentschädigungsverfahren), sind für den Vollzug des Entschädigungsgesetzes und des Ausgleichsleistungsgesetzes der Landkreis Dahme-Spreewald für sein Gebiet und der Landkreis Oder-Spree für sein Gebiet und die Gebiete der Landkreise Barnim, Elbe-Elster, Havelland, Märkisch-Oderland, Oberhavel, Oberspreewald-Lausitz, Ostprignitz-Ruppin, Potsdam-Mittelmark, Prignitz, Spree-Neiße, Teltow-Fläming und Uckermark sowie der kreisfreien Städte Brandenburg an der Havel, Cottbus, Frankfurt (Oder) und der Landeshauptstadt Potsdam zuständig.

(3) Für Singularentschädigungsverfahrennach dem DDR-Entschädigungserfüllungsgesetz ist der Landkreis Oder-Spree und für Verfahren nach dem DDR-Entschädigungserfüllungsgesetz in den Fällen des § 25 Absatz 1 Satz 2 des Vermögensgesetzes das Landesamt zur Regelung offener Vermögensfragen für das Gebiet des Landes Brandenburg zuständig.

(4) Für Verfahren nach dem Entschädigungsgesetz, dem Ausgleichsleistungsgesetz und dem DDR-Entschädigungserfüllungsgesetz jeweils in den Fällen des § 25 Absatz 2 des Vermögensgesetzes ist der Landkreis Oder-Spree für das Gebiet des Landes Brandenburg zuständig.

(5) Für Anmeldeauskünfte des Landkreises Ostprignitz-Ruppin nach § 3 Absatz 5 des Vermögensgesetzes ist der Landkreis Dahme-Spreewald zuständig.
Im Übrigen bleibt die Regelung des § 3 Absatz 5 des Vermögensgesetzes von Zuständigkeitsübertragungen nach dieser Verordnung unberührt.

(6) Die Aufgaben nach den Absätzen 1 und 5 werden als Pflichtaufgabe zur Erfüllung nach Weisung und die Aufgaben nach den Absätzen 2 bis 4 im Rahmen der Bundesauftragsverwaltung wahrgenommen.

(7) Die Zuständigkeit des Landesamtes zur Regelung offener Vermögensfragen wird zum 1. Januar 2016 auf das Ministerium der Finanzen übertragen.

#### § 2  
Kostenerstattung

Das Land erstattet anteilig die personellen und sächlichen Verwaltungsausgaben nach Maßgabe des Haushaltsplans.

#### § 3  
Sonderaufsicht

(1) Sonderaufsichtsbehörde ist das für Finanzen zuständige Ministerium.

(2) Für den Inhalt der Sonderaufsicht gelten die §§ 121 und 131 der Kommunalverfassung des Landes Brandenburg entsprechend.

#### § 4  
Ermächtigungsübertragung

Die Ermächtigung der Landesregierung nach § 23 Absatz 2 Satz 1 des Vermögensgesetzes, die Zuständigkeit für Verfahren nach dem Vermögensgesetz, dem Entschädigungsgesetz und dem Ausgleichsleistungsgesetz durch Rechtsverordnung zu regeln, wird auf das für Finanzen zuständige Mitglied der Landesregierung übertragen; die Verordnung ist im Einvernehmen mit dem für Inneres zuständigen Mitglied der Landesregierung zu erlassen.

#### § 5  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2009 in Kraft.
Gleichzeitig treten die Zweite Vermögensgesetzdurchführungsverordnung vom 20.
September 2005 (GVBl.
II S.
478), die zuletzt durch Artikel 2 der Verordnung vom 25.
November 2008 (GVBl.
II S.
470) geändert worden ist, und die Dritte Vermögensgesetzdurchführungsverordnung vom 25.
November 2008 (GVBl.
II S.
470) außer Kraft.

Potsdam, den 11.
Januar 2010

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident  
Matthias Platzeck

Der Minister der Finanzen  
Dr.
Helmuth Markov