## Verordnung zur Festsetzung des Wasserschutzgebietes für die Wasserfassung Strausberg – Spitzmühle-Ost

Auf Grund des § 51 Absatz 1 Satz 1 Nummer 1 und Satz 2 und Absatz 2 und des § 52 Absatz 1 des Wasserhaushaltsgesetzes vom 31.
Juli 2009 (BGBl.
I S. 2585) in Verbindung mit § 15 Absatz 1 Satz 1 und Absatz 2 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Allgemeines

(1) Zur Sicherung der öffentlichen Wasserversorgung wird zum Schutz des Grundwassers im Einzugsgebiet der Wasserfassungen des Wasserwerkes Strausberg – Spitzmühle-Ost das in § 2 näher umschriebene Wasserschutzgebiet festgesetzt.
Begünstigter ist der Wasserverband Strausberg-Erkner.

(2) Das Wasserschutzgebiet gliedert sich in den Fassungsbereich (Zone I), in die engere Schutzzone (Zone II) und in die weitere Schutzzone (Zone III).

#### § 2 Räumlicher Geltungsbereich

(1) Lage und Größe des Wasserschutzgebietes und der Schutzzonen ergeben sich aus der Übersichtskarte in der Anlage 2 und den in Absatz 2 genannten Karten.

(2) Die Schutzzonen sind in der topografischen Karte im Maßstab 1 : 10 000 in der Anlage 3 und außerdem in der Liegenschaftskarte im Maßstab 1 : 2 500 in der Anlage 4, die aus 15 Blättern besteht, dargestellt.
Für die Abgrenzung der Schutzzonen ist die Darstellung in der Liegenschaftskarte maßgebend.

(3) Zu Informationszwecken werden zusätzlich auf Papier ausgefertigte Exemplare der in Absatz 2 genannten Karten bei der unteren Wasserbehörde des Landkreises Märkisch-Oderland und in den Stadtverwaltungen von Strausberg und Altlandsberg hinterlegt und können dort während der Dienststunden von jedermann kostenlos eingesehen werden.
Diese Karten sind mit dem Dienstsiegel des Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft (Siegelnummer 1) versehen.
Eine weitere so gesiegelte Ausfertigung der Karten befindet sich im Ministerium für Ländliche Entwicklung, Umwelt und Landwirtschaft.

(4) Veränderungen der Topografie sowie von Flurstücksgrenzen oder -bezeichnungen berühren den räumlichen Geltungsbereich der Schutzzonen nicht.

#### § 3 Schutz der Zone III

In der Zone III sind verboten:

1.  das Düngen mit Gülle, Jauche, Geflügelkot, Festmist, Silagesickersaft, Gärresten, Wirtschaftsdüngern aus pflanzlichen Stoffen, Bodenhilfsstoffen, Kultursubstraten, Pflanzenhilfsmitteln, gütegesicherten Grünabfall- und Bioabfallkomposten und Abfällen aus der Herstellung oder Verarbeitung landwirtschaftlicher Erzeugnisse oder sonstigen Düngemitteln mit im Sinne des § 2 Nummer 10 der Düngeverordnung wesentlichen Nährstoffgehalten an Stickstoff oder Phosphat,
    
    a)
    
    wenn die Düngung nicht im Sinne des § 3 Absatz 4 der Düngeverordnung in betriebsspezifisch analysierten zeit- und bedarfsgerechten Gaben und nicht durch Geräte, die den allgemein anerkannten Regeln der Technik entsprechen, erfolgt,
    
    b)
    
    wenn keine schlagbezogenen Aufzeichnungen über die Zu- und Abfuhr von Stickstoff und Phosphat erstellt und mindestens sieben Jahre lang nach Ablauf des Düngejahres aufbewahrt werden,
    
    c)
    
    auf abgeerntetem Ackerland, wenn nicht unmittelbar Folgekulturen einschließlich Zwischenfrüchte angebaut werden,
    
    d)
    
    auf landwirtschaftlich oder erwerbsgärtnerisch genutzten Flächen vom 1. Oktober bis 15.
    Februar,
    
    e)
    
    auf Brachland oder stillgelegten Flächen oder
    
    f)
    
    auf wassergesättigten, oberflächlich oder in der Tiefe gefrorenen oder schneebedeckten Böden,
    
2.  das Lagern oder Ausbringen von Fäkalschlamm oder Klärschlämmen aller Art einschließlich in Biogasanlagen behandelter Klärschlämme, Abfällen aus der Herstellung und Verarbeitung nichtlandwirtschaftlicher Erzeugnisse und von nicht gütegesicherten Grünabfall- und Bioabfallkomposten, ausgenommen die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen und Ausbringung im Garten,
3.  das Errichten, Erweitern oder Betreiben von Dunglagerstätten, ausgenommen befestigte Dunglagerstätten mit Sickerwasserfassung und dichtem Jauchebehälter, der über eine Leckageerkennungseinrichtung verfügt,
4.  das Errichten von Erdbecken zur Lagerung von Gülle, Jauche oder Silagesickersäften,
5.  das Errichten, Erweitern oder Betreiben von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle, Jauche, Silagesickersaft, Gärresten und flüssigem Kompost, ausgenommen Hochbehälter, bei denen Undichtigkeiten am Fußpunkt zwischen Behältersohle und aufgehender Wand sofort erkennbar sind und die über eine Leckageerkennungseinrichtung und Sammeleinrichtungen verfügen, wenn der unteren Wasserbehörde
    
    a)
    
    vor Inbetriebnahme,
    
    b)
    
    bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung sowie
    
    c)
    
    wiederkehrend alle fünf Jahre
    
    ein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit der Sammeleinrichtungen vorgelegt wird,
    
6.  das Lagern von organischen oder mineralischen Düngemitteln auf unbefestigten Lagerflächen oder auf nicht baugenehmigten Anlagen, ausgenommen das Lagern von Kompost aus dem eigenen Haushalt oder Garten,
7.  das Errichten, Erweitern oder Betreiben von ortsfesten Anlagen für die Silierung von Pflanzen oder die Lagerung von Silage, ausgenommen
    
    a)
    
    Anlagen mit dichtem Silagesickersaft-Sammelbehälter, der über eine Leckageerkennungseinrichtung verfügt, und
    
    b)
    
    Anlagen mit Ableitung in Jauche- oder Güllebehälter,
    
    wenn der unteren Wasserbehörde vor Inbetriebnahme, bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung, sowie wiederkehrend alle fünf Jahre ein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit der Behälter und Leitungen vorgelegt wird,
    
8.  die Silierung von Pflanzen oder Lagerung von Silage außerhalb ortsfester Anlagen, ausgenommen Ballensilage im Wickelverfahren,
9.  das Errichten oder Erweitern von Stallungen oder Unterständen für Tierbestände, ausgenommen für die Kleintierhaltung zur Eigenversorgung,
10.  die Freilandtierhaltung im Sinne der Anlage 1 Nummer 1, wenn die Ernährung der Tiere nicht im Wesentlichen aus der jeweils beweideten Grünlandfläche erfolgt oder wenn die Grasnarbe flächig verletzt wird, ausgenommen Kleintierhaltung für die Eigenversorgung,
11.  die Anwendung von Pflanzenschutzmitteln oder von Biozidprodukten,
    
    a)
    
    wenn die Pflanzenschutzmittel nicht für Wasserschutzgebiete zugelassen sind,
    
    b)
    
    wenn die Zulassungs- und Anwendungsbestimmungen nicht eingehalten werden,
    
    c)
    
    wenn der Einsatz von Pflanzenschutzmitteln nicht durch Anwendung der allgemeinen Grundsätze des integrierten Pflanzenschutzes und der Einsatz von Biozidprodukten in entsprechender Weise auf das notwendige Maß beschränkt wird,
    
    d)
    
    wenn keine flächenbezogenen Aufzeichnungen für Pflanzenschutzmittel nach dem Pflanzenschutzgesetz und für Biozidprodukte in entsprechender Weise über den Einsatz auf erwerbsgärtnerisch, land- oder forstwirtschaftlich genutzten Flächen geführt und mindestens sieben Jahre lang nach dem Einsatz aufbewahrt werden,
    
    e)
    
    in einem Abstand von weniger als 10 Metern zu oberirdischen Gewässern,
    
    f)
    
    zur Bodenentseuchung oder
    
    g)
    
    auf Dauergrünland und Grünlandbrachen,
    
12.  die Beregnung landwirtschaftlich oder erwerbsgärtnerisch genutzter Flächen, wenn die Beregnungshöhe 20 Millimeter pro Tag oder 60 Millimeter pro Woche überschreitet,
13.  das Errichten oder Erweitern von Gartenbaubetrieben oder Kleingartenanlagen, ausgenommen Gartenbaubetriebe, die in geschlossenen Systemen produzieren,
14.  die Erstanlage oder Erweiterung von Baumschulen, forstlichen Pflanzgärten, Weihnachtsbaumkulturen sowie von gewerblichem Wein-, Hopfen-, Gemüse-, Obst- oder Zierpflanzenanbau, ausgenommen Gemüse- und Zierpflanzenanbau unter Glas in geschlossenen Systemen und Containerproduktion von Baumschulprodukten auf versiegelten Flächen,
15.  der Umbruch von Dauergrünland oder von Grünlandbrachen,
16.  das Anlegen von Schwarzbrache im Sinne der Anlage 1 Nummer 2,
17.  die Intensivierung der bisherigen Art und des bisherigen Umfangs der fischereiwirtschaftlichen Flächennutzung, wie zum Beispiel durch Zufütterung, das Betreiben von Aquakulturen oder die Kalkung der Gewässer,
18.  Erstaufforstungen mit Nadelbaumarten oder Robinien,
19.  die Umwandlung von Wald in eine andere Nutzungsart,
20.  Holzerntemaßnahmen, die eine gleichmäßig verteilte Überschirmung von weniger als 60 Prozent des Waldbodens oder Freiflächen größer als 1 000 Quadratmeter erzeugen, ausgenommen Femel- oder Saumschläge,
21.  das Einrichten oder Erweitern von Holzlagerplätzen über 100 Raummeter, die dauerhaft oder unter Einsatz von Nassholzkonservierung betrieben werden,
22.  Erdaufschlüsse im Sinne des § 49 Absatz 1 des Wasserhaushaltsgesetzes, selbst wenn Grundwasser nicht aufgedeckt wird, ausgenommen das Verlegen von Ver- und Entsorgungsleitungen und die Herstellung von Baugruben und Bohrungen,
23.  das Errichten, Erweitern oder Erneuern von
    
    a)
    
    Bohrungen, welche die gering leitende Deckschicht über oder unter dem genutzten Grundwasserleiter verletzen können,
    
    b)
    
    Grundwassermessstellen oder
    
    c)
    
    Brunnen,
    
    ausgenommen das Erneuern von Brunnen für Entnahmen mit zum Zeitpunkt des Inkrafttretens dieser Verordnung rechtskräftiger wasserrechtlicher Erlaubnis oder Bewilligung,
    
24.  das Errichten oder Erweitern von vertikalen Anlagen zur Gewinnung von Erdwärme,
25.  das Errichten oder Erweitern von Anlagen zum Umgang mit wassergefährdenden Stoffen, ausgenommen doppelwandige Anlagen mit Leckanzeigegerät und ausgenommen Anlagen, die mit einem Auffangraum ausgerüstet sind, der das maximal in der Anlage vorhandene Volumen wassergefährdender Stoffe aufnehmen kann, und soweit
    
    a)
    
    in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 1 das für die Anlage maßgebende Volumen von 1 000 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 1 die für die Anlage maßgebende Masse von 1 000 Tonnen,
    
    b)
    
    in oberirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 2 das für die Anlage maßgebende Volumen von 100 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 2 die für die Anlage maßgebende Masse von 100 Tonnen,
    
    c)
    
    in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 2 das für die Anlage maßgebende Volumen von 10 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 2 die für die Anlage maßgebende Masse von 10 Tonnen,
    
    d)
    
    in oberirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 3 das für die Anlage maßgebende Volumen von 10 Kubikmetern beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 3 die für die Anlage maßgebende Masse von 10 Tonnen,
    
    e)
    
    in unterirdischen Anlagen für flüssige Stoffe der Wassergefährdungsklasse 3 das für die Anlage maßgebende Volumen von 1 Kubikmeter beziehungsweise bei festen oder gasförmigen Stoffen der Wassergefährdungsklasse 3 die für die Anlage maßgebende Masse von 1 Tonne
    
    nicht überschritten wird,
    
26.  der Umgang mit wassergefährdenden Stoffen im Sinne des § 62 Absatz 3 des Wasserhaushaltsgesetzes außerhalb von zugelassenen Anlagen, Vorrichtungen und Behältnissen, aus denen ein Eindringen in den Boden nicht möglich ist, ausgenommen
    
    a)
    
    der Umgang mit Jauche, Gülle, Silagesickersaft sowie Dünge- und Pflanzenschutzmitteln im Rahmen ordnungsgemäßer Landwirtschaft entsprechend dieser Verordnung sowie
    
    b)
    
    der Umgang mit haushaltsüblichen Kleinstmengen,
    
27.  das Einleiten oder Einbringen von wassergefährdenden Stoffen im Sinne des § 62 Absatz 3 des Wasserhaushaltsgesetzes in den Untergrund oder in Gewässer,
28.  das Errichten oder Erweitern von Rohrleitungsanlagen für wassergefährdende Stoffe, ausgenommen Rohrleitungsanlagen im Sinne des § 62 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes,
29.  das Errichten, Erweitern oder Betreiben von Anlagen zur behälterlosen Lagerung oder Ablagerung von Stoffen im Untergrund,
30.  das Behandeln, Lagern oder Ablagern von Abfall, tierischen Nebenprodukten oder bergbaulichen Rückständen, ausgenommen
    
    a)
    
    die vorübergehende Lagerung in dichten Behältern,
    
    b)
    
    die ordnungsgemäße kurzzeitige Bereitstellung von vor Ort angefallenem Abfall zum Abtransport durch den Entsorgungspflichtigen,
    
    c)
    
    die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen,
    
31.  das Ein- oder Aufbringen von Abfällen, bergbaulichen Rückständen oder Ersatzbaustoffen in oder auf Böden oder deren Einbau in bodennahe technische Bauwerke,
32.  das Errichten von Anlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden radioaktiver Stoffe im Sinne des Atomgesetzes, ausgenommen für medizinische Anwendungen sowie für Mess-, Prüf- und Regeltechnik,
33.  das Errichten von Industrieanlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden wassergefährdender Stoffe in großem Umfang, wie zum Beispiel in Raffinerien, Metallhütten oder chemischen Fabriken,
34.  das Errichten von Kraftwerken oder Heizwerken, die der Genehmigungspflicht nach Bundesimmissionsschutzrecht unterliegen, ausgenommen mit Gas, Sonnenenergie oder Windkraft betriebene Anlagen,
35.  das Errichten von Biogasanlagen,
36.  das Errichten oder Erweitern von Abwasserbehandlungsanlagen, ausgenommen
    
    a)
    
    die Sanierung bestehender Abwasserbehandlungsanlagen zugunsten des Gewässerschutzes und
    
    b)
    
    Abwasservorbehandlungsanlagen wie Fett-, Leichtflüssigkeits- oder Amalgamabscheider,
    
37.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik eingehalten werden,
38.  das Errichten von Niederschlagswasser- oder Mischwasserentlastungsbauwerken,
39.  das Errichten oder Erweitern von Abwassersammelgruben, ausgenommen
    
    a)
    
    Anlagen mit allgemeiner bauaufsichtlicher Zulassung des Deutschen Institutes für Bautechnik und
    
    b)
    
    monolithische Sammelgruben aus Beton, die den allgemein anerkannten Regeln der Technik entsprechen,
    
40.  das Betreiben oder Unterhalten von Abwassersammelgruben, wenn der unteren Wasserbehörde nicht
    
    a)
    
    vor Inbetriebnahme,
    
    b)
    
    bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung sowie
    
    c)
    
    wiederkehrend alle fünf Jahre
    
    ein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit vorgelegt wird,
    
41.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten, ausgenommen Anlagen mit dichtem Behälter,
42.  das Einleiten von Abwasser – mit Ausnahme von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 Nummer 3 – in oberirdische Gewässer, sofern die Einleitung nicht schon zum Zeitpunkt des Inkrafttretens dieser Verordnung wasserrechtlich zugelassen war,
43.  das Ausbringen von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes,
44.  das Einleiten oder Versickern von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser,
45.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen
    
    a)
    
    das breitflächige Versickern von Niederschlagswasserabflüssen von gering belasteten Herkunftsflächen im Sinne der Anlage 1 Nummer 3 über die belebte Bodenzone einer ausreichend mächtigen und bewachsenen Oberbodenschicht gemäß den allgemein anerkannten Regeln der Technik oder
    
    b)
    
    mit wasserrechtlicher Erlaubnis,
    
    sofern die Versickerung außerhalb von Altlasten, Altlastenverdachtsflächen oder Flächen mit schädlichen Bodenveränderungen und nur auf Flächen mit einem zu erwartenden Flurabstand des Grundwassers von 100 Zentimetern oder größer erfolgt,
    
46.  das Anwenden von Auftaumitteln auf Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen auf Landes- und Kreisstraßen sowie bei Extremwetterlagen wie Eisregen,
47.  das Errichten sowie der Um- oder Ausbau von Straßen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik für bautechnische Maßnahmen an Straßen in Wasserschutzgebieten eingehalten werden,
48.  das Errichten von Bahnhöfen oder Schienenwegen der Eisenbahn,
49.  das Verwenden von Baustoffen, Böden oder anderen Materialien, die auslaug- und auswaschbare wassergefährdende Stoffe enthalten (zum Beispiel Schlacke, Bauschutt, Teer, Imprägniermittel) für Bau- und Unterhaltungsmaßnahmen, zum Beispiel im Straßen-, Wege-, Wasser-, Landschafts- und Tiefbau,
50.  das Einrichten von öffentlichen Freibädern oder Zeltplätzen sowie Camping aller Art, ausgenommen
    
    a)
    
    Einrichtungen mit ordnungsgemäßer Abfall- und Abwasserentsorgung und
    
    b)
    
    das Zelten von Fuß-, Rad-, Reit- und Wasserwanderern abseits von Zelt- und Campingplätzen für eine Nacht,
    
51.  das Einrichten von Sportanlagen, ausgenommen Anlagen mit ordnungs​gemäßer Abfall- und Abwasserentsorgung,
52.  das Errichten von Motorsportanlagen,
53.  das Errichten oder Erweitern von Schießständen oder Schießplätzen für Feuerwaffen, ausgenommen Schießstände in geschlossenen Räumen,
54.  das Errichten von Golfanlagen,
55.  das Abhalten oder Durchführen von Märkten, Volksfesten oder Großveranstaltungen außerhalb der dafür vorgesehenen Anlagen,
56.  das Durchführen von Motorsportveranstaltungen, ausgenommen das Durchfahren auf klassifizierten Straßen,
57.  Bestattungen, ausgenommen innerhalb bereits bei Inkrafttreten dieser Verordnung bestehender Friedhöfe,
58.  das Errichten von Flugplätzen im Sinne des § 6 Absatz 1 Satz 1 des Luftverkehrsgesetzes,
59.  das Starten oder Landen motorgetriebener Luftfahrzeuge, mit Ausnahme in Fällen des § 25 Absatz 2 des Luftverkehrsgesetzes,
60.  das Errichten von militärischen Anlagen, Standort- oder Truppenübungsplätzen,
61.  das Durchführen von militärischen Übungen, ausgenommen das Durchfahren auf klassifizierten Straßen,
62.  Bergbau einschließlich der Aufsuchung oder Gewinnung von Erdöl oder Erdgas, ausgenommen im Geltungsbereich der bei Inkrafttreten dieser Verordnung rechtskräftigen bergrechtlichen Betriebspläne, wenn hierdurch keine nachteiligen Veränderungen von Gewässereigenschaften zu besorgen sind,
63.  das Durchführen von Sprengungen, sofern die Gefahr besteht, dass dabei das Grundwasser aufgedeckt wird,
64.  die Darstellung von neuen Bauflächen oder Baugebieten im Rahmen der vorbereitenden Bauleitplanung, wenn darin eine Neubebauung bisher unbebauter Gebiete vorgesehen wird,
65.  die Festsetzung von neuen Baugebieten im Rahmen der verbindlichen Bauleitplanung, ausgenommen
    
    a)
    
    Gebiete, die im zum Zeitpunkt des Inkrafttretens dieser Verordnung gültigen Flächennutzungsplan als Bauflächen oder Baugebiete dargestellt sind, und
    
    b)
    
    die Überplanung von Bestandsgebieten, wenn dies zu keiner wesentlichen Erhöhung der zulässigen Grundfläche im Sinne des § 19 Absatz 2 der Baunutzungsverordnung führt.
    

#### § 4 Schutz der Zone II

Die Verbote der Zone III gelten auch in der Zone II.
In der Zone II sind außerdem verboten:

1.  das Düngen mit Gülle, Jauche, Festmist, Gärresten, Wirtschaftsdüngern aus pflanzlichen Stoffen, Bodenhilfsstoffen, Kultursubstraten, Pflanzenhilfsmitteln, gütegesicherten Grünabfall- oder Bioabfallkomposten, Abfällen aus der Herstellung oder Verarbeitung landwirtschaftlicher Erzeugnisse oder sonstigen organischen Düngern sowie die Anwendung von Silagesickersaft,
2.  das Errichten von Dunglagerstätten,
3.  das Errichten von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle, Jauche, Silagesickersaft, Gärresten und flüssigem Kompost,
4.  die Silierung von Pflanzen oder Lagerung von Silage,
5.  die Freilandtierhaltung im Sinne der Anlage 1 Nummer 1, ausgenommen Kleintierhaltung für die Eigenversorgung,
6.  die Beweidung,
7.  die Anwendung von Biozidprodukten außerhalb geschlossener Gebäude oder von Pflanzenschutzmitteln,
8.  die Beregnung,
9.  der Einsatz von forstwirtschaftlichen Kraftfahrzeugen abseits von Straßen, Wegen oder forstwirtschaftlichen Rückegassen,
10.  das Vergraben, Lagern oder Ablagern von Tierkörpern oder Teilen davon,
11.  das Errichten oder Betreiben von Wildfütterungen, Kirrungen oder Luderplätzen,
12.  das Errichten von Anlagen zum Lagern, Abfüllen, Umschlagen, Herstellen, Behandeln oder Verwenden wassergefährdender Stoffe,
13.  der Einsatz von mineralischen Schmierstoffen zur Verlustschmierung oder von mineralischen Schalölen,
14.  das Lagern, Abfüllen oder Umschlagen wassergefährdender Stoffe, ausgenommen haushaltsübliche Kleinstmengen,
15.  das Befahren mit Fahrzeugen mit wassergefährdender Ladung, nachdem die Anordnung des entsprechenden Vorschriftzeichens 269 durch die Straßenverkehrsbehörde erfolgte,
16.  das Errichten von Transformatoren oder Stromleitungen mit flüssigen wassergefährdenden Kühl- oder Isoliermitteln,
17.  das Behandeln, Lagern oder Ablagern von Abfall, bergbaulichen Rückständen oder tierischen Nebenprodukten, ausgenommen
    
    a)
    
    die ordnungsgemäße kurzzeitige Bereitstellung von in der Zone II angefallenem Abfall zum Abtransport durch den Entsorgungspflichtigen und
    
    b)
    
    die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen,
    
18.  der Umgang mit radioaktiven Stoffen im Sinne des Atomgesetzes,
19.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, ausgenommen Anlagen, die zur Entsorgung aus vorhandener Bebauung dienen und wenn hierbei die allgemein anerkannten Regeln der Technik eingehalten werden,
20.  das Errichten oder Betreiben von Abwassersammelgruben,
21.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten,
22.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen das breitflächige Versickern von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 Nummer 3 über die belebte Bodenzone einer mindestens 20 Zentimeter mächtigen und bewachsenen Oberbodenschicht,
23.  das Errichten sowie der Um- oder Ausbau von Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen
    
    a)
    
    Baumaßnahmen an vorhandenen Straßen zur Anpassung an den Stand der Technik und zur Verbesserung der Verkehrssicherheit unter Einhaltung der allgemein anerkannten Regeln der Technik,
    
    b)
    
    Wege mit breitflächiger Versickerung der Niederschlagswasserabflüsse über die belebte Bodenzone einer mindestens 20 Zentimeter mächtigen und bewachsenen Oberbodenschicht,
    
24.  das Errichten, Erweitern oder Betreiben von öffentlichen Freibädern oder Zeltplätzen sowie Camping aller Art,
25.  das Errichten von Sportanlagen,
26.  das Abhalten oder Durchführen von Sportveranstaltungen, Märkten, Volksfesten oder Großveranstaltungen,
27.  das Errichten oder Erweitern von Baustelleneinrichtungen oder Baustofflagern,
28.  das Durchführen von Bohrungen, ausgenommen Maßnahmen zur Abwehr von Gefahren für das Grundwasser unter Beachtung der Sicherheitsvorkehrungen zum Grundwasserschutz,
29.  das Durchführen von unterirdischen Sprengungen,
30.  das Errichten oder Erweitern von baulichen Anlagen, ausgenommen Veränderungen in Gebäuden und Instandhaltungsmaßnahmen.

#### § 5 Schutz der Zone I

Die Verbote der Zonen III und II gelten auch in der Zone I.
In der Zone I sind außerdem verboten:

1.  das Betreten oder Befahren,
2.  landwirtschaft-, forstwirtschaft- oder gartenbauliche Nutzung,
3.  Veränderungen oder Aufschlüsse der Erdoberfläche.

#### § 6 Maßnahmen zur Wassergewinnung

Die Verbote des § 3 Nummer 23, 42 und 44, des § 4 Nummer 14, 18, 27 bis 30 sowie des § 5 Nummer 1 und 3 gelten nicht für Maßnahmen zur Wassergewinnung, die durch diese Verordnung geschützt ist.

#### § 7 Widerruf von Befreiungen

(1) Befreiungen nach § 52 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes sind widerruflich und bedürfen der Schriftform.
Abweichend von Satz 1 ist eine Befreiung von den Verboten gemäß § 3 Nummer 64 und 65 nicht widerruflich.

(2) Im Fall des Widerrufs einer Befreiung kann die untere Wasserbehörde vom Grundstückseigentümer verlangen, dass der frühere Zustand wiederhergestellt wird, sofern es das Wohl der Allgemeinheit, insbesondere der Schutz der Wasserversorgung, erfordert.

#### § 8 Sicherung und Kennzeichnung des Wasserschutzgebietes

(1) Die Zone I ist vom Begünstigten auf Anordnung der unteren Wasserbehörde gegen unbefugtes Betreten, zum Beispiel durch eine Umzäunung, zu sichern.

(2) Der Begünstigte hat auf Anordnung der unteren Wasserbehörde zur Kennzeichnung des Wasserschutzgebietes im Bereich öffentlicher Verkehrsflächen bei der Straßenverkehrsbehörde die Anordnung des Richtzeichens 354 und des Vorschriftzeichens 269 zu beantragen und im Bereich nicht öffentlicher Flächen in Abstimmung mit der Gemeinde nicht amtliche Hinweiszeichen aufzustellen.

#### § 9 Duldungspflichten

(1) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet haben die Überwachung des Wasserschutzgebietes, insbesondere hinsichtlich der Beachtung dieser Verordnung und der nach ihr getroffenen Anordnungen, sowie das Beobachten der Gewässer und des Bodens durch die zuständigen Wasserbehörden, den Begünstigten oder deren Beauftragte zu dulden.

(2) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet sind auf Anordnung der unteren Wasserbehörde verpflichtet,

1.  das Errichten und Unterhalten von Einrichtungen zur Sicherung der Zone I gegen unbefugtes Betreten,
2.  das Aufstellen, Unterhalten und Beseitigen von Hinweis-, Warn-, Gebots- und Verbotszeichen,
3.  das Betreten der Grundstücke durch Bedienstete der zuständigen Behörden, den Begünstigten oder deren Beauftragte zum Beobachten, Messen und Untersuchen des Grundwassers und zur Entnahme von Boden- und Vegetationsproben sowie
4.  das Anlegen und Betreiben von Grundwassermessstellen

zu dulden.
Die Anordnung erfolgt durch schriftlichen Bescheid gegenüber den betroffenen Eigentümern oder Nutzungsberechtigten.
Soweit bergrechtliche Belange berührt sind, ergeht die Entscheidung im Benehmen mit der zuständigen Bergbehörde.

(3) Auf Verlangen der unteren Wasserbehörde ist Einsicht in die Aufzeichnungen nach § 3 Nummer 1 Buchstabe b und Nummer 11 Buchstabe d dieser Verordnung zu gewähren oder diese unverzüglich vorzulegen.

#### § 10 Übergangsregelung

(1) Für bei Inkrafttreten dieser Verordnung errichtete und betriebene Anlagen gilt das Verbot des Betreibens gemäß § 3 Nummer 3, 5 und 7 nach einem Jahr nach Inkrafttreten dieser Verordnung.

(2) Für bei Inkrafttreten dieser Verordnung bestehende Einleitungen oder Versickerungen von Niederschlagswasserabflüssen von mittel oder hoch belasteten Herkunftsflächen in den Untergrund ohne wasserrechtliche Erlaubnis gelten die Verbote des § 3 Nummer 45 nach einem Jahr nach Inkrafttreten dieser Verordnung.

#### § 11 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 103 Absatz 1 Nummer 7a des Wasserhaushaltsgesetzes handelt, wer vorsätzlich oder fahrlässig eine nach den §§ 3, 4 oder 5 verbotene Handlung ohne eine Befreiung gemäß § 52 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes vornimmt, ausgenommen das Verbot nach § 4 Nummer 15 dieser Verordnung.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

#### § 12 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt das mit Beschluss-Nummer 100/82 vom 10.
Februar 1982 des Kreistages Strausberg festgesetzte Trinkwasserschutzgebiet Strausberg – Wasserwerk Bötzsee außer Kraft.

Potsdam, den 13.
Juli 2015

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

### Anlagen

1

[WSGStrausberg-Spitzmühle-Ost-Anlage-1](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-1.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-1") 589.2 KB

2

[WSGStrausberg-Spitzmühle-Ost-Anlage-2](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-2.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-2") 2.9 MB

3

[WSGStrausberg-Spitzmühle-Ost-Anlage-3](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-3.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-3") 6.9 MB

4

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-01](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-01.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-01") 2.0 MB

5

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-02](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-02.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-02") 2.6 MB

6

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-03](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-03.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-03") 2.7 MB

7

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-04](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-04.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-04") 3.7 MB

8

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-05](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-05.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-05") 2.2 MB

9

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-06](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-06.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-06") 2.4 MB

10

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-07](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-07.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-07") 2.3 MB

11

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-08](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-08.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-08") 2.2 MB

12

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-09](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-09.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-09") 3.1 MB

13

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-10](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-10.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-10") 2.9 MB

14

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-11](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-11.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-11") 2.4 MB

15

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-12](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-12.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-12") 2.2 MB

16

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-13](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-13.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-13") 1.8 MB

17

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-14](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-14.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-14") 2.1 MB

18

[WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-15](/br2/sixcms/media.php/68/GVBl_II_30_2015-Anlage-4-Blatt-15.pdf "WSGStrausberg-Spitzmühle-Ost-Anlage-4-Blatt-15") 3.1 MB