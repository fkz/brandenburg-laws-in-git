## Verordnung zur Durchführung des Jagdgesetzes für das Land Brandenburg (BbgJagdDV)

Auf Grund des § 7 Absatz 1, des § 23 Absatz 4, des § 26 Absatz 1, des § 29 Absatz 10, des § 31 Absatz 1, des § 35 Absatz 5, des § 41 Absatz 8, des § 45 Absatz 2 und des § 52 Absatz 2 des Jagdgesetzes für das Land Brandenburg vom 9. Oktober 2003 (GVBl. I S. 250), von denen § 31 Absatz 1 durch Artikel 3 des Gesetzes vom 10. Juli 2014 (GVBl. I Nr. 33) geändert worden ist, verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft im Einvernehmen mit dem Minister der Finanzen und nach Anhörung des Ausschusses für Ländliche Entwicklung, Umwelt und Landwirtschaft und der Landesvereinigung der Jäger:

#### § 1 Eigenjagdbezirke<br>(zu §&nbsp;7 Absatz&nbsp;1 BbgJagdG)

(1) Die Mindestgröße von Eigenjagdbezirken kann unter folgenden Voraussetzungen bis auf 75 Hektar herabgesetzt werden:

1.  Die Fläche muss eine zusammenhängende Form aufweisen, die eine sichere Bejagung zulässt.
2.  Für die Herstellung des Zusammenhangs müssen die Grundflächen mindestens eine Verbindung von 100 Meter Breite haben.
3.  Flächen, die in ihrer äußeren Gestalt langgezogene schmale Streifen bilden, müssen eine Mindestbreite von 300 Metern haben.

(2) Bei Flächen, die in gemeinschaftlichen Jagdbezirken liegen, wird die Genehmigung zur Verringerung der Mindestgröße erst zum Ende des jeweiligen Pachtvertrages wirksam, es sei denn, die Jagdgenossenschaften und die Jagdpächter stimmen einem früheren Zeitpunkt zu.

#### § 2 Jagdabgabe<br>(zu §&nbsp;23 Absatz&nbsp;4 BbgJagdG)

(1) Die mit der Gebühr für den Jagdschein zu zahlende Jagdabgabe beträgt pro Jagdjahr:

1.  für den Jahresjagdschein und den Jahresfalknerjagdschein, in beiden Fällen auch für Jugendliche, jeweils 25 Euro,
2.  für den Tagesjagdschein und den Tagesfalknerjagdschein jeweils 5 Euro.

(2) Wird der Falknerschein zusätzlich zu einem Jagdschein oder ein Jagdschein zusätzlich zu einem Falknerjagdschein erworben, wird die Jagdabgabe nur einmal erhoben.

#### § 3 Sachliche Gebote und Verbote<br>(zu §&nbsp;26 Absatz&nbsp;1 BbgJagdG)

(1) Abweichend von § 19 Absatz 1 Nummer 5a des Bundesjagdgesetzes ist es erlaubt, Nachtzielgeräte, die einen Bildwandler oder eine elektronische Verstärkung besitzen und für Schusswaffen bestimmt sind, sowie künstliche Lichtquellen zum Anstrahlen oder Beleuchten des Zieles beim Erlegen von Schwarzwild zu verwenden.
Die waffenrechtlichen Bestimmungen sind zu beachten und bleiben davon unberührt.

(2) Die Erteilung einer Erlaubnis nach § 19 Absatz 1 Nummer 11 des Bundesjagdgesetzes ist nur in besonderen Härtefällen möglich, in denen Menschen mit körperlicher Beeinträchtigung oder Menschen mit Behinderung ausschließlich auf das Kraftfahrzeug angewiesen sind und ansonsten von einer Jagdausübung gänzlich ausgeschlossen wären.
Die zuständige Behörde stellt durch Nebenbestimmungen sicher, dass es hierbei keine Einschränkungen bei der Wahrung des Tierschutzes (zum Beispiel Fangschuss) gibt.

#### § 4 Regelung der Bejagung<br>(zu §&nbsp;29 Absatz&nbsp;10 BbgJagdG)

(1) Die Jagdausübungsberechtigten haben den von ihnen für ihren Jagdbezirk vorgeschlagenen Abschussplan je Jagdjahr vom 1. März bis spätestens 1. April der unteren Jagdbehörde nach einem von der obersten Jagdbehörde bestimmten Muster vorzulegen.
Bei der Abschussplanung ist der Wildschadenssituation und der Körperentwicklung des Wildes Rechnung zu tragen.
Eine erhöhte Wildschadenssituation im Wald liegt in der Regel dann vor, wenn der Wildbestand eine flächige, mindestens einen Hektar große künstliche Verjüngung oder eine natürliche Verjüngung der Gemeinen Kiefer, Rotbuche, Stieleiche, Traubeneiche, Gemeinen Birke sowie Eberesche nicht zulässt und daher die gemäß der einschlägigen forstlichen Förderrichtlinie vorgesehenen Mindestpflanzenzahlen nicht erreicht werden.
Die Feststellung von erhöhten Wildschäden erfolgt durch die Inhaber der Eigenjagdbezirke und der Eigenjagdbezirke des Landes sowie die Jagdvorstände der Jagdgenossenschaften.
In Zweifelsfällen entscheidet die untere Jagdbehörde.
Bei Wildschäden im Wald soll die untere Forstbehörde beteiligt werden.
Bei der Nichterfüllung von Abschussplänen sind die nicht erlegten Stücke dem Abschussplan des nachfolgenden Jagdjahres hinzuzurechnen.

(2) Gruppenabschusspläne sind zulässig.
Sie sind nur insgesamt für beide Geschlechter und alle Altersklassen der jeweiligen Wildart zulässig.
Gruppenabschusspläne sind gemeinsame Abschusspläne von zusammenhängenden Jagdbezirken innerhalb einer anerkannten Hegegemeinschaft.
Auf Einzelabschusspläne kann in der Hegegemeinschaft verzichtet werden, wenn die Hegegemeinschaft dies in ihrer Satzung beschließt.
Gruppenabschusspläne sind auch gemeinsame Abschusspläne von Jagdbezirken in Wildlebensräumen ohne Mitgliedschaft in einer anerkannten Hegegemeinschaft.
Die untere Jagdbehörde hat zur Bestätigung dieser Gruppenabschusspläne das Votum der Hegegemeinschaft einzuholen.

(3) Für die Abschussplanung von Rot-, Dam-, Muffel- und Schwarzwild gelten die Klassifizierung und der Abschussanteil der Anlage dieser Verordnung als Richtwert.
Die untere Jagdbehörde kann im Einzelfall einen anderen Abschussanteil festsetzen.

(4) Für Rot-, Dam- und Muffelwild gilt der Abschussplan für die Altersklasse 0 und 1 als Mindestabschuss.

(5) Für Schwarzwild erfolgt die Abschussplanung als Mindestabschuss.

(6) Bei einer erhöhten Wildschadenssituation gemäß Absatz 1 erfolgt die Bestätigung oder Festsetzung von Abschussplänen für weibliches Rot-, Dam- und Muffelwild als Mindestabschussplan.

(7) Die Streckenliste ist nach einem von der obersten Jagdbehörde bestimmten Muster zu erstellen.
Der Nachweis über das erlegte Schalenwild ist getrennt nach Geschlecht und Altersklasse zu führen.

(8) Für die Überprüfung der Richtigkeit der Streckenliste haben die Jagdausübungsberechtigten der unteren Jagdbehörde die erforderlichen Nachweise, insbesondere Wildursprungsscheine und Protokolle zum körperlichen Nachweis, zu erbringen und Auskünfte zu erteilen.

(9) Bei Rot-, Dam- und Muffelwild kann im Rahmen des Abschussplanes und nach Geschlechtern getrennt jeweils statt einem Stück einer höheren Altersklasse auch ein Stück einer geringeren Altersklasse erlegt werden.
Ausgenommen sind die Altersklassen 0, 3 und 4 soweit die Hegegemeinschaft keine andere Regelung getroffen hat.

(10) Die Erlegung von krankem Wild über den Abschussplan hinaus ist zulässig.
Der unteren Jagdbehörde ist die Erlegung unverzüglich mitzuteilen.

(11) Büchsenmunition ist für die Jagd auf Schalenwild nur geeignet, wenn sie eine zuverlässige Tötungswirkung erzielt und eine hinreichende ballistische Präzision gewährleistet.
Ferner darf verwendete Büchsenmunition auf der Jagd ab dem Jagdjahr 2021/2022 nicht mehr Blei als nach dem jeweiligen Stand der Technik unter gleichzeitiger Wahrung der Anforderungen des Satzes 1 unvermeidbar an den Wildkörper abgeben.

(12) Bei der Jagd auf Wasserfederwild an und über Gewässern ist zum Schutz des Wasser- und des Naturhaushaltes die Verwendung bleihaltiger Schrotmunition verboten.
Für die Verwendung zur Jagd auf Wasserwild an Gewässern und in Feuchtgebieten ist Schrotmunition nur geeignet, wenn sie über die Anforderungen nach Absatz 11 hinaus möglichst kein Blei an die Umwelt abgibt.

#### § 5 Jagd- und Schonzeiten<br>(zu §&nbsp;31 Absatz&nbsp;1 BbgJagdG)

(1) Über die in § 2 Absatz 1 des Bundesjagdgesetzes genannten Tierarten hinaus werden Mink, Marderhund, Waschbär, Nutria, Bisam, Nilgans, Rabenkrähe, Nebelkrähe sowie Elster zu Tierarten, die dem Jagdrecht unterliegen, erklärt.

(2) Abweichend von den in der Verordnung über die Jagdzeiten vom 2. April 1977 (BGBl. I S. 531) in der jeweils geltenden Fassung festgesetzten Jagdzeiten darf die Jagd ausgeübt werden auf:

Tierarten

Jagdzeiten

Rotwild und Damwild

Schmalspießer und Schmaltiere

Hirsche, Alttiere und Kälber

  

vom 16. April bis 15. Januar

vom 1. August bis 15. Januar

Rehwild

Ricken, Kitze

Rehböcke und Schmalrehe

  

vom 1. August bis 15. Januar

vom 16. April bis 15. Januar

Muffelwild

Jährlinge und Schmalschafe

Widder, Schafe, Lämmer

  

vom 16. April bis 15. Januar

vom 1. August bis 15. Januar

Schwarzwild

ganzjährig (unter Berücksichtigung des § 22 Absatz 4 BJagdG)

Als „nicht für die Aufzucht notwendiges Elterntier“ im Sinne des § 22 Absatz 4 des Bundesjagdgesetzes ist eine Bache anzusehen, die von der Muttermilch unabhängige Frischlinge führt.
Dieses liegt regelmäßig dann vor, wenn Frischlinge nach dem Wechsel zum Winterhaar (Verlust der Streifen) in ihrer Ernährung nicht mehr auf die Muttermilch angewiesen sind.

Feldhasen

vom 1. Oktober bis 31. Dezember

Dachs

1. August bis 31. Januar

Fuchs

Jungfüchse

vom 1. Juli bis 31. Januar

ganzjährig

Steinmarder

vom 1. September bis 31. Januar

Mink, Marderhund, Waschbär, Nutria, Bisam

ganzjährig (unter Berücksichtigung des § 22 Absatz 4 BJagdG)

Nilgans

vom 1. September bis 31. Januar

Rabenkrähe, Nebelkrähe, Elster

vom 1. Oktober bis 31. Dezember

Graugänse

vom 1.
August bis 31.
Januar, mit der Maßgabe, dass die Jagd in der Zeit vom 1. September bis 31. Oktober sowie vom 16. Januar bis 31. Januar nur zur Schadensabwehr auf gefährdeten Ackerkulturen ausgeübt werden darf

Kanadagänse, Blässgänse

vom 16. September bis 31. Januar, mit der Maßgabe, dass die Jagd in der Zeit vom 16. September bis 31. Oktober sowie vom 16. Januar bis 31. Januar nur zur Schadensabwehr auf gefährdeten Ackerkulturen ausgeübt werden darf.
Die Jagd auf Blässgänse ist nur mit Büchsenmunition zulässig.

(3) Folgende Wildarten sind ganzjährig mit der Jagd zu verschonen:

1.  Baummarder,
2.  Iltis,
3.  Hermelin,
4.  Mauswiesel,
5.  Saatgänse einschließlich Waldsaatgänse,
6.  alle Enten, außer Stockenten, Tafelenten und Krickenten.

(4) Unabhängig von den geltenden Jagdzeiten ist es verboten, in Jagdbezirken oder Teilen von Jagdbezirken die Jagd auf Fasane, Rebhühner und Wildenten im gleichen Jagdjahr auszuüben, in dem diese in diesen Jagdbezirken ausgesetzt wurden.
Als Aussetzen gilt nicht, wenn Wildtiere oder Gelege der Natur entnommen werden müssen, um sie aufzuziehen, gesund zu pflegen oder auszubrüten und sie anschließend wieder in die freie Wildbahn zu entlassen.

#### § 6 Bestätigte Schweißhundeführer<br>(zu §&nbsp;35 Absatz&nbsp;5 BbgJagdG)

(1) Die Jagdscheininhaber können auf Antrag durch die untere Jagdbehörde als Schweißhundeführer oder Schweißhundeführerinnen bestätigt werden, wenn sie mindestens drei Jahre einen Jagdgebrauchshund bei den Nachsuchen geführt haben und einen auf Schweiß brauchbaren Jagdhund gemäß der Jagdhundebrauchbarkeitsverordnung führen.

(2) Die persönliche Bestätigung erlischt bei Wegfall der Voraussetzungen.
Die Bestätigungen anderer Bundesländer werden anerkannt.

(3) Die grenzüberschreitende Nachsuche gemäß § 35 Absatz 2 Satz 1 des Jagdgesetzes für das Land Brandenburg ist ohne Vereinbarung zulässig, wenn es sich nach der Beurteilung des Schweißhundeführers oder der Schweißhundeführerin um Schussverletzungen handelt, die erfahrungsgemäß dem Wild längere Qualen bereiten (zum Beispiel Laufschüsse, Weidwundschüsse, Äser- oder Gebrechschüsse) und die Nachsuche unverzüglich aufgenommen wurde.
Handelt es sich um Schussverletzungen, die eine eindeutige Totsuche erwarten lassen, ist die grenzüberschreitende Nachsuche nur unter den Voraussetzungen des § 35 Absatz 1 des Jagdgesetzes für das Land Brandenburg zulässig.

(4) Die bestätigten Schweißhundeführer und Schweißhundeführerinnen dürfen erforderlichenfalls bis zu zwei Hilfskräfte hinzuziehen.
Sofern die Vereinbarung nach § 35 Absatz 1 des Jagdgesetzes für das Land Brandenburg keine weitergehenden Regelungen vorsieht, dürfen die Schweißhundeführerinnen und Schweißhundeführer sowie deren Hilfskräfte bei grenzüberschreitenden Nachsuchen Schusswaffen führen.

(5) Grenzüberschreitende Nachsuchen sind nur bei ausreichend natürlichen Sichtverhältnissen zulässig.

#### § 7 Natürliche Äsung und Fütterung des Wildes<br>(zu §&nbsp;41 Absatz&nbsp;8 BbgJagdG)

(1) Die Fütterung von Schalenwild in festgesetzten Notzeiten darf nur eine Erhaltungsfütterung sein.
Beim wiederkäuenden Schalenwild darf nur Raufutter und Saftfutter verwendet werden.
Die Fütterung von Kraftfutter ist untersagt.
Bei der Fütterung einer bestimmten Wildart ist eine Futteraufnahme durch andere Wildarten auszuschließen.
Die ausgebrachten Futtermengen dürfen nur den unbedingt notwendigen Umfang zur Überbrückung der Notzeit umfassen.
Eine Fütterung in Naturschutzgebieten darf nur erfolgen, wenn hierfür im jeweiligen Jagdbezirk keine anderen geeigneten Standorte zur Verfügung stehen.
In diesem Fall sind die Maßgaben der jeweils geltenden Schutzgebietsverordnung zu berücksichtigen.

(2) Ablenkfütterungen sind nur für Schwarzwild zulässig.
An den Ablenkfütterungen ruht die Jagd.

(3) Fütterungen und Ablenkfütterungen dürfen nicht in geschützten Biotopen gemäß § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 Absatz 1 des Brandenburgischen Naturschutzausführungsgesetzes angelegt werden.

(4) Das Anlocken von Schalenwild mit Hilfe von Futter als Bejagungshilfe (Kirrung) ist erlaubt.
Kirrmaterial darf nur in geringer Menge ausgebracht werden, wenn die zuvor ausgebrachte Menge weitgehend aufgenommen worden ist.
Es ist nur artgerechtes, unverarbeitetes Futter wie Getreide, Eicheln, Bucheckern, Kastanien und Ähnliches zu verwenden.
Kirrungen dürfen nicht in geschützten Biotopen gemäß § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 Absatz 1 des Brandenburgischen Naturschutzausführungsgesetzes angelegt werden.
Im Übrigen sind die jeweils geltenden Schutzgebietsverordnungen zu beachten.

(5) Aufbrüche von erlegtem Wild und erlegtes Raubwild sind von den Erlegenden so zu beseitigen, dass eine Aufnahme durch Greifvögel nicht möglich ist.
Das Vergraben ist zulässig.
Jagdbezirke, in denen ausschließlich Büchsenmunition gemäß § 4 Absatz 11 zum Einsatz kommt, sind von Satz 1 nicht berührt.

(6) In Gebieten mit festgesetzter Notzeit ist jegliche Jagdausübung verboten.
Aus Gründen des Tierschutzes erforderliche Abschüsse bleiben davon unberührt.

#### § 8 Wildschäden in Forstkulturen, Flurgehölzen und Obstplantagen<br>(zu §&nbsp;45 Absatz&nbsp;1 BbgJagdG)

(1) Forstkulturen bedürfen keiner Schutzvorrichtung bei einer flächigen, mindestens einen Hektar großen künstlichen Verjüngung oder bei natürlicher Verjüngung, wenn in ihnen überwiegend Hauptholzarten enthalten sind.
Hauptholzarten sind Gemeine Kiefer, Rotbuche, Stieleiche, Traubeneiche, Gemeine Birke und Eberesche.
Forstkulturen umfassen den Zeitraum der Begründung bis zum Dickungsstadium, in der Regel zehn Jahre.

(2) Als übliche Schutzvorrichtungen zur Abwendung des Schadens (vergleiche § 32 Absatz 2 des Bundesjagdgesetzes) sind insbesondere anzusehen:

1.  Drahtgeflechtzaun
    1.  gegen Rotwild und Muffelwild in Höhe von 1,80 Meter,
    2.  gegen Damwild und Rehwild in Höhe von 1,50 Meter,
    3.  gegen Schwarzwild in Höhe von 1,50 Meter,der am Boden so befestigt ist, dass er nicht hochgehoben werden kann,
2.  Drahtgeflechtzaun gegen Wildkaninchen von 25 Millimeter Maschenbreite in Höhe von 1,30 Meter über der Erde und 0,20 Meter in die Erde eingegraben.

(3) Einem Drahtgeflechtzaun nach Absatz 2 steht ein Zaun anderer Art gleich, wenn er die gleiche Schutzwirkung hat.

(4) Bei Alleen, einzelnstehenden Bäumen, Flurholzanpflanzungen und Forstkulturen mit anderen als den im Jagdbezirk vorkommenden Hauptholzarten sind anerkannte Bestäubungs- und Streichmittel oder Manschetten für den Zeitraum der Aufwuchsphase ausreichend.

#### § 9 Kosten des Vorverfahrens<br>(zu §&nbsp;52 Absatz&nbsp;2 BbgJagdG)

Die Schätzer erhalten für ihre Tätigkeit und den damit verbundenen Zeitverlust eine Vergütung in Höhe von 40 Euro für jede angefangene Stunde, höchstens 200 Euro für einen Tag und Ersatz ihrer Reisekosten nach den für Beamtinnen und Beamte der Reisekostenstufe B geltenden Vorschriften des Reisekostenrechts des Landes Brandenburg.

#### § 10 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung zur Durchführung des Jagdgesetzes für das Land Brandenburg vom 2. April 2004 (GVBl. II S. 305), die zuletzt durch die Verordnung vom 29. September 2014 (GVBl. II Nr. 74) geändert worden ist, außer Kraft.

Potsdam, den 28.
Juni 2019

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage (zu § 4 Absatz 3)](/br2/sixcms/media.php/68/GVBl_II_45_2019-Anlage.pdf "Anlage (zu § 4 Absatz 3)") 188.1 KB