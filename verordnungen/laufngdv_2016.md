## Verordnung über die Durchführung des Landesaufnahmegesetzes (Landesaufnahmegesetz-Durchführungsverordnung - LAufnGDV)

Auf Grund des § 6 Absatz 7, des § 7 Absatz 3, des § 9 Absatz 6, des § 10 Absatz 3 und des § 12 Absatz 3 Satz 1 des Landesaufnahmegesetzes vom 15.
März 2016 (GVBl.
I Nr. 11) verordnet die Ministerin für Arbeit, Soziales, Gesundheit, Frauen und Familie im Einvernehmen mit dem Minister des Innern und für Kommunales und dem Minister der Finanzen:

### Abschnitt 1  
Verteilungsverfahren und landesinterne Umverteilung

#### § 1 Verteilungsverfahren in Zuständigkeit des Landesamtes für Soziales und Versorgung

(1) Das Landesamt für Soziales und Versorgung verteilt Personen nach § 4 Nummer 1 und 2 des Landesaufnahmegesetzes nach der jeweiligen Aufnahmequote des Verteilerschlüssels der Anlage 1 und weist diese den Landkreisen und kreisfreien Städten (Kommunen) durch elektronischen oder schriftlichen Bescheid zur Aufnahme zu.

(2) Der Verteilerschlüssel des Absatzes 1 ist anhand der Bevölkerungsstärken und der Katasterflächen der Kommunen an der Gesamtbevölkerung und der Gesamtkatasterfläche des Landes Brandenburg jeweils in Prozent auszurichten.
Zur Berechnung des Verteilerschlüssels sind die Bevölkerungsstärken mit dem Faktor 0,9 und die Katasterflächen der Kommunen mit dem Faktor 0,1 zu berücksichtigen.
Zusätzlich ist der Anteil der sozialversicherungspflichtig Beschäftigten der jeweiligen Kommunen an der Gesamtbevölkerungsquote im Land Brandenburg zu berücksichtigen.
Dazu sind die für die Kommunen errechneten Produkte des Satzes 2 mit den jeweils aktuell verfügbaren Beschäftigtenzahlen zu multiplizieren und durch die Gesamtbeschäftigtenzahl des Landes Brandenburg zu dividieren.

(3) Grundlage der Berechnung nach Absatz 2 sind die jeweils verfügbaren aktuellen Daten des Amtes für Statistik Berlin-Brandenburg bezüglich der Bevölkerungsstärken und der Beschäftigtenzahlen sowie bezüglich der Katasterflächen der Landesvermessung und Geobasisinformation Brandenburg.
Der Verteilerschlüssel des Absatzes 1 ist alle drei Jahre zu überprüfen, gegebenenfalls anzupassen und im Amtsblatt für Brandenburg zu veröffentlichen.

(4) Die Kommunen können über ihre jeweilige Aufnahmequote hinaus weitere Personen aufnehmen oder untereinander durch öffentlich-rechtliche Verträge vereinbaren, dass die ihnen zugewiesenen Personen auf dem Gebiet einer anderen Kommune untergebracht werden.

(5) Abweichungen von der Aufnahmequote einer Kommune nach Absatz 1 sind mit Zustimmung des für Soziales zuständigen Ministeriums insbesondere zulässig, wenn

1.  dadurch Leerstand in bereits bestehenden Wohnungsverbünden oder Übergangswohnungen vermieden werden kann,
2.  eine zeitnahe Verteilung anders nicht möglich ist oder
3.  die Aufnahme bestimmter Gruppen von Zugewanderten die vorrangige Verteilung in bestimmte Kommunen, mit denen vorher ein Einvernehmen erzielt wurde, rechtfertigt.

(6) Personen, die als jüdische Zugewanderte aufgenommen werden, sollen vorrangig Kommunen zugewiesen werden, in denen bereits jüdische Gemeinden vorhanden sind oder sich im Aufbau befinden.

(7) Das Landesamt für Soziales und Versorgung teilt den Kommunen die Zahl der monatlichen Zugänge von Personen, die nach § 4 Nummer 1 und 2 des Landesaufnahmegesetzes aufgenommen und verteilt wurden, spätestens im auf die Verteilung folgenden Monat mit.

#### § 2 Verteilungsverfahren in Zuständigkeit der Zentralen Ausländerbehörde

(1) Ist für die übrigen nach § 4 des Landesaufnahmegesetzes aufzunehmenden Personen ein Verteilungsverfahren durchzuführen oder werden diese gemäß § 6 Absatz 1 Satz 3 des Landesaufnahmegesetzes einbezogen, erfolgt die Verteilung nach der jeweiligen Aufnahmequote des Verteilerschlüssels der Anlage 2.
Die Anzahl der den Kommunen durch schriftlichen oder elektronischen Bescheid zur Aufnahme zuzuweisenden Personen entspricht dem Aufnahmesoll im Sinne von § 6 Absatz 5 Satz 1 des Landesaufnahmegesetzes, sofern keine Abweichungen im Sinne des § 4 zur Anwendung kommen.

(2) Der Verteilerschlüssel des Absatzes 1 ist anhand der Bevölkerungsstärken und der Katasterflächen der Kommunen an der Gesamtbevölkerung und der Gesamtkatasterfläche des Landes Brandenburg jeweils in Prozent auszurichten.
Zur Berechnung des Verteilerschlüssels sind die Bevölkerungsstärken mit dem Faktor 0,9 und die Katasterflächen der Kommunen mit dem Faktor 0,1 zu berücksichtigen.
§ 1 Absatz 3 gilt entsprechend.

(3) Die Zentrale Ausländerbehörde teilt bis zum letzten Tag jedes Monats den Kommunen die Zahl der Zugänge des Vormonats in den Erstaufnahmeeinrichtungen des Landes mit und informiert rechtzeitig, mindestens eine Woche zuvor, über den Umfang der beabsichtigten Zuweisungen von Personen zur Aufnahme in die Kommune.
Dabei soll die Anzahl der Personen nicht über die Freimeldungen hinausgehen, sofern die Freimeldungen der Kommune den Voraussetzungen des § 5 Absatz 1 entsprechen.

(4) Für das Verteilungsverfahren in Zuständigkeit der Zentralen Ausländerbehörde gelten im Übrigen die §§ 3 bis 5.

#### § 3 Aufnahmesoll

(1) Das jährliche Aufnahmesoll einer Kommune ist auf der Grundlage der aktuellen Mitteilung zur voraussichtlichen Anzahl der Zugänge von Asylbegehrenden nach § 44 Absatz 2 des Asylgesetzes zu berechnen.
Für die Berechnung sind

1.  die Höhe der prognostizierten Aufnahmeverpflichtung des Landes Brandenburg nach der Aufnahmequote für das Land Brandenburg nach dem Königsteiner Schlüssel zu ermitteln,
2.  von dem Ergebnis nach Nummer 1
    
    1.  eine im Einvernehmen mit dem für Inneres zuständigen Ministerium bestimmte Anzahl von Personen, die voraussichtlich direkt aus der Erstaufnahmeeinrichtung zurückgeführt werden, und
    2.  eine im Einvernehmen mit dem für Inneres zuständigen Ministerium bestimmte Anzahl von Personen, die voraussichtlich im Jahr der Aufnahme nicht mehr aus der Erstaufnahmeeinrichtung verteilt werden können,
    
    in Abzug zu bringen,
    
3.  eine im Einvernehmen mit dem für Inneres zuständigen Ministerium bestimmte Anzahl von Personen, die in der Erstaufnahmeeinrichtung im vorangegangenen Kalenderjahr aufgenommen, jedoch im Jahr der Aufnahme aus das Erstaufnahmeverfahren oder das Asylverfahren betreffenden Gründen nicht verteilt wurden, hinzuzurechnen und
4.  das nach den Nummern 1 bis 3 ermittelte Ergebnis mit der Aufnahmequote der Kommune nach dem Verteilerschlüssel des § 2 Absatz 1 zu multiplizieren.

(2) Weichen die tatsächlichen Zugangszahlen im Bereich der Erstaufnahme von der prognostizierten Anzahl nach Absatz 1 Satz 1 ab oder liegt für das jeweilige Kalenderjahr keine aktuelle Mitteilung zur voraussichtlichen Anzahl der Zugänge von Asylbegehrenden nach § 44 Absatz 2 des Asylgesetzes vor, ist abweichend von Absatz 1 Satz 1 das jährliche Aufnahmesoll einer Kommune auf der Grundlage einer einvernehmlichen Zugangseinschätzung des für Soziales zuständigen Ministeriums mit dem für Inneres und dem für Finanzen zuständigen Ministerium zu ermitteln.
Für die Berechnung des Aufnahmesolls gilt Absatz 1 Satz 2 Nummer 2 bis 4 entsprechend.

(3) Die erste Mitteilung des jährlichen Aufnahmesolls gemäß § 6 Absatz 5 Satz 1 des Landesaufnahmegesetzes erfolgt jeweils bis spätestens zum 31.
März des Kalenderjahres.
Das Aufnahmesoll der Kommunen ist wenigstens kalenderhalbjährlich zu überprüfen.
Unter- und Überschreitungen des Aufnahmesolls einer Kommune aus dem jeweiligen Vorjahr sind zu verrechnen.

(4) Die Kommunen haben auf die Erfüllung ihres Aufnahmesolls nach § 6 Absatz 5 Satz 4 und § 7 Absatz 2 des Landesaufnahmegesetzes anzurechnende Personen dem für Soziales zuständigen Ministerium oder einer von ihm bestimmten Behörde unverzüglich zu melden.

#### § 4 Abweichungen von dem Aufnahmesoll

(1) Werden auf dem Gebiet einer Kommune am 31.
Dezember des Vorjahres Erstaufnahmeeinrichtungen des Landes oder deren Außenstellen betrieben, gilt das Aufnahmesoll der Kommune in Höhe von jeweils 5 Prozent je 400 Unterbringungsplätze als erfüllt.
Beträgt die durchschnittliche Belegung im Betriebszeitraum des Vorjahres weniger als 75 Prozent, gilt abweichend von Satz 1 das Aufnahmesoll der Kommune in Höhe von jeweils 3,75 Prozent als erfüllt.
Beträgt die durchschnittliche Belegung im Betriebszeitraum des Vorjahres weniger als 50 Prozent, gilt abweichend von Satz 1 das Aufnahmesoll der Kommune in Höhe von jeweils 2,5 Prozent als erfüllt.
Beträgt die durchschnittliche Belegung im Betriebszeitraum des Vorjahres weniger als 25 Prozent, erfolgt abweichend von Satz 1 keine Anrechnung auf die Erfüllung des Aufnahmesolls der Kommune.
Die nach den Sätzen 1 bis 3 angerechnete Personenzahl ist dem Aufnahmesoll der übrigen Kommunen im Verhältnis ihrer Aufnahmequoten zueinander hinzuzurechnen.
Hiervon kann das für Soziales zuständige Ministerium oder die von ihm bestimmte Behörde Kommunen ausnehmen, auf deren Gebiet zum Stichtag des Satzes 1 Erstaufnahmeeinrichtungen des Landes oder deren Außenstellen mit einer durchschnittlichen Belegung im Zeitraum des Betriebes von mindestens 100 Unterbringungsplätzen betrieben werden.

(2) Die Kommunen können über ihr jeweiliges Aufnahmesoll hinaus weitere Personen aufnehmen.
Ferner können sie untereinander durch öffentlich-rechtliche Verträge vereinbaren, dass Personen, die aufgrund der Verteilungsentscheidung der Zentralen Ausländerbehörde ihnen zur Aufnahme zugewiesen werden sollen, auf dem Gebiet einer anderen Kommune untergebracht werden.
Vereinbarungen nach Satz 2 werden bei der Zuweisungsentscheidung der Zentralen Ausländerbehörde nur berücksichtigt, sofern diese rechtzeitig darüber schriftlich oder elektronisch informiert wurde.

(3) Zudem sind Abweichungen von der Anzahl der nach dem Aufnahmesoll aufzunehmenden Personen unterjährig, insbesondere bezogen auf das monatliche Aufnahmesoll, zulässig,

1.  wenn hierdurch die Errichtung neuer Einrichtungen der vorläufigen Unterbringung oder Leerstand in bereits bestehenden Einrichtungen vermieden werden kann oder
2.  eine zeitnahe Verteilung anders nicht möglich ist.

#### § 5 Bereitstellung von Unterbringungsplätzen

(1) Die Kommunen informieren die Zentrale Ausländerbehörde und das für Soziales zuständige Ministerium spätestens am Monatsende über die im Folgemonat belegbaren Unterbringungsplätze, deren Anzahl dem Umfang des monatlichen Aufnahmesolls des Folgemonats (Freimeldungen) zu entsprechen hat.

(2) Ein Unterbringungsplatz soll zur erneuten Belegung freigemeldet werden, wenn dieser von einem Bewohner oder einer Bewohnerin im Zeitpunkt der Freimeldung 30 Tage nicht in Anspruch genommen wurde und die Abwesenheit nicht durch Krankheit oder vergleichbare Gründe gerechtfertigt ist.
Dies gilt entsprechend, wenn über diesen Zeitraum die tatsächliche Nutzung des Unterkunftsplatzes weniger als ein Drittel des Zeitraums betrug oder aus den Umständen hervorgeht, dass der Bewohner oder die Bewohnerin künftig den Unterbringungsplatz nicht mehr beanspruchen wird.

#### § 6 Voraussetzungen der landesinternen Umverteilung

(1) Ein öffentliches Interesse für eine landesinterne Umverteilung nach § 7 Absatz 1 Satz 1 des Landesaufnahmegesetzes besteht insbesondere

1.  bei Vorliegen der in Absatz 2 genannten Gründe der öffentlichen Sicherheit und Ordnung oder
2.  wenn dadurch die Rückführung der betroffenen Person erheblich erleichtert wird.

(2) Gründe der öffentlichen Sicherheit und Ordnung im Sinne des § 7 Absatz 1 Satz 1 des Landesaufnahmegesetzes liegen insbesondere vor

1.  wenn durch die landesinterne Umverteilung die Begehung von Straftaten unterbunden wird,
2.  in Fällen von schwerwiegenden Gewalttaten gegen Personen, bei häuslicher Gewalt, bei konkreten oder abstrakten besonderen Gefährdungslagen für bestimmte Personen oder Personengruppen oder zur Sicherstellung von Schutz und Hilfe insbesondere für Frauen und Kinder vor seelischer und körperlicher Gewalt,
3.  wenn wegen konkreter oder allgemeiner Erkenntnisse zu bestimmten Personen oder Personengruppen zu vermuten ist, dass durch die gleichzeitige Unterbringung mit Personen oder Personengruppen insbesondere anderer Staatsangehörigkeit oder ethnischer Zugehörigkeit erhöhte Sicherheitsrisiken bestehen oder
4.  wenn wegen konkreter oder allgemeiner Erkenntnisse zu bestimmten Personen oder Personengruppen zu vermuten ist, dass durch die Belegung der Einrichtung der vorläufigen Unterkunft deren innere Ordnung oder die internen Betriebsabläufe beeinträchtigt werden.

(3) Humanitäre Gründe von vergleichbarem Gewicht im Sinne des § 7 Absatz 1 Satz 1 des Landesaufnahmegesetzes liegen insbesondere vor

1.  in den Fällen des § 7 Absatz 1 Satz 3 Nummer 1 bis 3 des Landesaufnahmegesetzes,
2.  bei alleinstehenden volljährigen Kindern, die das 27.
    Lebensjahr noch nicht vollendet haben, zur Ermöglichung der Haushaltsgemeinschaft mit ihren Eltern und
3.  bei sonstigen Familienangehörigen, wenn diese oder die bereits im Land Brandenburg untergebrachten Familienangehörigen auf die besondere Betreuung durch die jeweils anderen Familienangehörigen angewiesen sind, oder
4.  wenn durch die Umverteilung dem besonderen Bedarf einer schutzbedürftigen Person im Sinne des Artikels 21 der Richtlinie 2013/33/EU entsprochen werden kann.

#### § 7 Verfahren der landesinternen Umverteilung

(1) Die nach § 7 Absatz 1 Satz 2 des Landesaufnahmegesetzes zuständige Ausländerbehörde informiert die Ausländerbehörde, zu der die Umverteilung erfolgen soll, nach ihrer Entscheidung unverzüglich über die Gründe der Umverteilung.

(2) In den Fällen des § 7 Absatz 1 Satz 3 des Landesaufnahmegesetzes hat die Ausländerbehörde, zu der die Umverteilung erfolgen soll, das Einvernehmen unverzüglich zu erteilen.

(3) Die nach Absatz 1 zuständige Ausländerbehörde unterrichtet die für die Aufgabenwahrnehmung nach § 2 Absatz 1 des Landesaufnahmegesetzes zuständigen Stellen unverzüglich über eine einvernehmliche Umverteilungsentscheidung nach § 7 Absatz 1 des Landesaufnahmegesetzes.

(4) Die für die Aufgabenwahrnehmung nach § 2 Absatz 1 des Landesaufnahmegesetzes zuständigen Behörden informieren das für Soziales zuständige Ministerium oder die von ihm bestimmte Behörde über die erfolgte Umverteilung.

### Abschnitt 2  
Aufnahme und Mindestbedingungen der vorläufigen Unterbringung

#### § 8 Personenbezogene Anforderungen an die vorläufige Unterbringung

(1) Alleinstehende Personen sowie Alleinerziehende mit ihren minderjährigen Kindern sind in Einrichtungen der vorläufigen Unterbringung nach Geschlechtern räumlich getrennt unterzubringen.
Bei der Unterbringung ist der Haushaltsgemeinschaft von Familienangehörigen und sonstigen humanitären Gründen von vergleichbarem Gewicht Rechnung zu tragen.

(2) Wer Gemeinschaftsunterkünfte oder Wohnungsverbünde betreibt, soll fachliche Handlungsleitlinien insbesondere zum Schutz von Kindern und Frauen vor Gewalt (Gewaltschutzkonzepte) bedarfsgerecht entwickeln und anwenden.
Zur Verhinderung von Übergriffen, insbesondere geschlechtsbezogener Gewalt einschließlich sexueller Übergriffe und Belästigungen, sind geeignete Maßnahmen, beispielsweise nach Geschlechtern getrennte Nutzung von Sanitäreinrichtungen und die Abschließbarkeit der Wohn- und Schlafzwecken dienenden Zimmer sowie der Sanitärräume, zu ergreifen.

(3) Personen nach § 4 Nummer 3 bis 8 des Landesaufnahmegesetzes können unter den Voraussetzungen des § 9 Absatz 2 des Landesaufnahmegesetzes vorübergehend außerhalb von Einrichtungen der vorläufigen Unterbringung untergebracht werden, insbesondere wenn

1.  für sie kein geeigneter regulärer Unterbringungsplatz in Einrichtungen der vorläufigen Unterbringung zur Verfügung steht oder
2.  anderweitig bei der Aufnahme eine gemeinsame Unterbringung von Haushaltsgemeinschaften von durch Ehe oder eingetragene Lebenspartnerschaft Verbundenen sowie von personensorgeberechtigten Erwachsenen minderjähriger lediger Kinder mit diesen Kindern oder von aus sonstigen humanitären Gründen von vergleichbarem Gewicht nur gemeinsam unterzubringenden Personen nicht möglich ist.

(4) Schutzbedürftige Personen nach § 4 Nummer 4 und 7 des Landesaufnahmegesetzes im Sinne des Artikels 21 der Richtlinie 2013/33/EU dürfen bei ihrer Aufnahme nur vorübergehend außerhalb von Einrichtungen der vorläufigen Unterbringung untergebracht werden, sofern aus der Schutzbedürftigkeit resultierende besondere Belange der gewählten Unterbringung nicht entgegenstehen.

(5) Bei der vorläufigen Unterbringung von Personen nach § 4 Nummer 4 und 7 des Landesaufnahmegesetzes ist ferner sicherzustellen, dass

1.  der Schutz ihres Familienlebens gewährleistet wird,
2.  die untergebrachte Person mit den in Artikel 18 Absatz 2 Buchstabe c der Richtlinie 2013/33/EU genannten Personen in Verbindung treten kann und diesen Personen Zugang zu der untergebrachten Person gewährt wird, sofern der Zugang nicht aus Gründen der Sicherheit einzuschränken ist,
3.  der Situation von schutzbedürftigen Personen entsprochen wird, sowie
4.  weitere geschlechts- sowie altersspezifische Belange berücksichtigt werden.

#### § 8a Aufnahme und vorläufige Unterbringung des nach dem Landesaufnahmeprogramm Nordirak aufzunehmenden Personenkreises

(1) Personen, denen auf Grundlage der Allgemeinen Weisung des Ministeriums des Innern und für Kommunales vom 24.
Januar 2019 (AW-AuslR 2019.02) eine Aufenthaltserlaubnis gemäß § 23 Absatz 1 des Aufenthaltsgesetzes erteilt worden ist, sind nach § 9 Absatz 1 des Landesaufnahmegesetzes vorläufig unterzubringen.

(2) Der Personenkreis nach Absatz 1 soll unter Beachtung seiner besonderen Bedürfnisse bei der Aufnahme und vorläufigen Unterbringung zur Sicherstellung einer zielgruppenspezifischen angemessenen sozialen Betreuung durch eine personell verstärkte Migrationssozialarbeit zunächst zusammen in einer Gemeinschaftsunterkunft oder einem Wohnungsverbund untergebracht werden.
Andere Personen können in derselben Einrichtung der vorläufigen Unterbringung grundsätzlich nur untergebracht werden, wenn die gemeinschaftliche Unterbringung der Integration des Personenkreises nach Absatz 1 förderlich ist.
§ 8 bleibt unberührt.

#### § 9 Mindestbedingungen für Gemeinschaftsunterkünfte

(1) Um den Bewohnerinnen und Bewohnern von Gemeinschaftsunterkünften die Teilhabe am gesellschaftlichen Leben zu ermöglichen, sollen Einrichtungen der sozialen Infrastruktur gut erreichbar sein.

(2) Gemeinschaftsunterkünfte sollen unter Berücksichtigung der örtlichen Gegebenheiten mit für die jeweilige Bewohnerschaft geeigneten Außenanlagen zur Freizeitgestaltung ausgestattet werden.
Sofern die Unterbringung von Minderjährigen vorgesehen ist, sind kindgerechte Spiel- und Schutzräume einzurichten sowie altersgerechte Aktivitäten im Freien zu ermöglichen.

(3) In den Gemeinschaftsunterkünften ist sicherzustellen, dass im Gefahrenfall eine unverzügliche Alarmierung der zuständigen Stellen gewährleistet ist.

(4) Die Gemeinschaftsunterkünfte müssen durch bauliche, technische und organisatorische Maßnahmen gegen unbefugtes Eindringen und gegen Angriffe von außen geschützt sein.
Wer eine Gemeinschaftsunterkunft betreibt, ist zu verpflichten, im Rahmen der Inbetriebnahme der Gemeinschaftsunterkunft ein von der zuständigen Polizeidienststelle bestätigtes Sicherheitskonzept zu erstellen, das die eigenen Sicherheitsmaßnahmen, wie Einsatz von geeignetem Wachpersonal, Telefonanschluss, Meldewege bei Angriffen von außen, bauliche und technische Sicherheitsmaßnahmen, sowie die polizeilichen Präventions- und Schutzmaßnahmen festlegt.

(5) In Gemeinschaftsunterkünften sind durch geeignete Maßnahmen die Abschließbarkeit, Nichteinsehbarkeit sowie nach Geschlechtern getrennte Nutzung der Sanitärräume zu gewährleisten.
Die Wohn- und Schlafzwecken dienenden Zimmer müssen von innen abschließbar sein und eine Möglichkeit der Zugangskontrolle vorhalten.
Weitere Mindestbedingungen für Gemeinschaftsunterkünfte ergeben sich aus der Anlage 3.

(6) Wer eine Gemeinschaftsunterkunft betreibt, hat zu gewährleisten, dass der Bewohnerschaft die unterbringungsnahe Unterstützung durch Migrationssozialarbeit nach den Bestimmungen des Abschnitts 3 und der Anlage 4 zugänglich ist.

#### § 10 Mindestbedingungen für Wohnungsverbünde

(1) Sofern die nachfolgenden Absätze keine anderweitigen Bestimmungen treffen, sind § 9 und Anlage 3 entsprechend auf Wohnungsverbünde anzuwenden.

(2) Von § 9 Absatz 5 Satz 1 kann in begründeten Fällen abgewichen werden.

(3) Anlage 3 findet auf Wohnungsverbünde mit der Maßgabe Anwendung, dass

1.  Nummer 5 keine Anwendung findet,
2.  von der Mindestfläche in Nummer 1 in begründeten Fällen, insbesondere bei Unterbringung von Haushaltsgemeinschaften oder anderen Personengruppen im Sinne von § 6 Absatz 2 des Landesaufnahmegesetzes, in den einzelnen abgeschlossenen Wohneinheiten des Wohnungsverbundes abgewichen werden kann,
3.  in Wohnungsverbünden entgegen Nummer 7 keine Gemeinschaftsverpflegung zulässig ist und
4.  von den Vorgaben der Nummern 9 und 10 abgewichen werden kann.

#### § 11 Mindestbedingungen für Übergangswohnungen

(1) Sofern Absatz 2 keine anderweitige Bestimmung trifft, gilt § 10 entsprechend für Übergangswohnungen.

(2) Bei Übergangswohnungen finden Nummer 1 mit Ausnahme der Mindestwohnfläche sowie die Nummern 8 bis 16 der Anlage 3 keine Anwendung.

#### § 12 Abweichen von Mindestbedingungen

(1) In besonderen Zugangssituationen, insbesondere zur Vermeidung von Unterbringungen in Turnhallen, Zelten oder ähnlichen Notunterkünften, kann das Landesamt für Soziales und Versorgung, soweit dies erforderlich ist, landesweit oder auf Antrag einer Kommune für eine bestimmte Einrichtung der vorläufigen Unterbringung befristet Abweichungen von den Mindestbedingungen nach den Vorschriften dieses Abschnitts zulassen und die Bedingungen hierfür festlegen.

(2) Der Antrag nach Absatz 1 ist in schriftlicher oder elektronischer Form zu stellen, dabei ist die Notwendigkeit des Abweichens von den Mindestbedingungen zu begründen.
Im Fall landesweiter Festlegungen im Sinne des Absatzes 1 genügt eine schriftliche oder elektronische Anzeige der Kommune über das konkrete Abweichen von den Mindestbedingungen im Einzelfall, sofern das Landesamt für Soziales und Versorgung nichts anderes bestimmt.

(3) Liegen die Voraussetzungen für das befristete Abweichen von den Unterbringungsstandards bei Ablauf der Befristung weiterhin vor, kann das Landesamt für Soziales und Versorgung die jeweilige Frist für den Zeitraum verlängern, in dem weiterhin die Notwendigkeit des Abweichens von den Mindestbedingungen bestehen wird.
Die Absätze 1 und 2 gelten entsprechend.

### Abschnitt 3  
Soziale Unterstützung durch Migrationssozialarbeit

#### § 13 Ziele und Aufgaben

(1) Die soziale Unterstützung durch Migrationssozialarbeit zielt darauf ab, den nach dem Landesaufnahmegesetz in den Kommunen aufgenommenen Personen eine selbstverantwortliche Lebensgestaltung einschließlich der notwendigen Inanspruchnahme der sozialen und integrativen Unterstützungssysteme zu ermöglichen.
Dabei sind die Integrationsbereitschaft der aufgenommenen Person und die Aufnahmebereitschaft sowie Aufnahmefähigkeit des Gemeinwesens zu befördern.

(2) Die Aufgaben der Migrationssozialarbeit umfassen sozialarbeiterische Hilfestellungen, Vermittlung von Informationen und weitergehenden Hilfsangeboten

1.  zu Fragen des Flüchtlingsschutzes, insbesondere des Asylverfahrens und den damit jeweils verbundenen Aufenthaltsstatus in der Bundesrepublik Deutschland,
2.  zur Entwicklung einer Lebensperspektive während des Aufenthalts in der Bundesrepublik Deutschland einschließlich der Familienzusammenführung, zur Weiterwanderung in ein Drittland oder zur Rückkehr in das Herkunftsland, einschließlich aufenthaltsrechtlicher und Verfahrensfragen,
3.  zur Aufnahme in den Kommunen einschließlich einschlägiger Verwaltungsabläufe, leistungsrechtlicher Fragen sowie des Zugangs zu zielgruppenspezifischen Angeboten und sozialen Regeldiensten und -angeboten,
4.  zur Ermöglichung eines gelingenden und selbstbestimmten Lebens unter den jeweiligen wohnformspezifischen Bedingungen der Einrichtung der vorläufigen Unterbringung sowie im Gemeinwesen,
5.  zur Förderung des gegenseitigen Verständnisses und eines gelingenden Miteinanders zwischen den aufgenommenen Personen und der Aufnahmegesellschaft,
6.  für schutzbedürftige Personen im Sinne des Artikels 21 der Richtlinie 2013/33/EU und
7.  zur Bewältigung von persönlichen Problemlagen, Konflikten und Alltagsproblemen.

(3) Zur Beförderung der Teilhabe am gesellschaftlichen Leben vor Ort sollen gemeinwesenorientierte Angebote zur interkulturellen Sensibilisierung und zur Unterstützung von Begegnungsmöglichkeiten unter Einbeziehung des bürgerschaftlichen Engagements durchgeführt werden.

(4) Zu den Aufgaben der Migrationssozialarbeit gehören Aktivitäten zur Unterstützung ehrenamtlich Tätiger sowie von Willkommensinitiativen und die Kooperation mit Migrantenorganisationen.

(5) Die Migrationssozialarbeit umfasst die Vernetzung und Kooperation mit migrationsspezifischen und allgemeinen Unterstützungsangeboten und fördert die interkulturelle Öffnung der nicht migrationsspezifischen Dienste und Institutionen.

(6) Davon unberührt bleiben die Bestimmungen des Rechtsdienstleistungsgesetzes.

#### § 14 Trägerschaft

(1) Nimmt eine Kommune Aufgaben der Migrationssozialarbeit in eigener Trägerschaft wahr, ist zu gewährleisten, dass die Aufgabenwahrnehmung unabhängig von der sonstigen behördlichen Aufgabenerfüllung erfolgt.

(2) Bei der Übertragung der Aufgabenwahrnehmung auf geeignete Dritte, in der Regel nichtstaatliche Träger der Sozialen Arbeit, ist sicherzustellen, dass diese die in diesem Abschnitt einschließlich Anlage 4 normierten Anforderungen erfüllen.
Die Aufgabenübertragung setzt ein öffentlich-rechtliches Auftragsverhältnis voraus.
Hierzu sind mindestens Vereinbarungen zu Art und Umfang der Leistung, der Qualität des Leistungsangebots, zur Entgelthöhe und zum Datenschutz zu treffen.
Durch die Entgeltvereinbarung ist sicherzustellen, dass die nach diesem Abschnitt erforderliche qualitative und quantitative Personalausstattung zuzüglich der jeweils benötigten Sachmittel dem jeweiligen Träger zur Aufgabenwahrnehmung zur Verfügung stehen.
Das Nähere zur Finanzierung bei der Aufgabenübertragung auf geeignete Dritte ergibt sich aus Anlage 4.

#### § 15 Anforderungen an die Aufgabenwahrnehmung

(1) Grundlage der Aufgabenwahrnehmung der Migrationssozialarbeit ist ein von den Kommunen zu erstellendes Umsetzungskonzept zur Migrationssozialarbeit.
Das Umsetzungskonzept kann Bestandteil eines kommunalen Integrationskonzepts sein und regionale Kooperationen mit anderen Kommunen beinhalten.
Das Umsetzungskonzept kann unter Einbeziehung anderer Träger, insbesondere bestehender oder geplanter Angebote der Migrationssozialarbeit, erarbeitet werden.

(2) Die Aufgabenwahrnehmung der Migrationssozialarbeit soll unterbringungsnahe, wohnformspezifische Unterstützungsangebote (Migrationssozialarbeit als unterbringungsnahe soziale Unterstützung) und regional zugängliche, zielgruppen- und fachspezifische Angebote (Migrationssozialarbeit als Fachberatungsdienst) umfassen.

(3) Die Träger der Migrationssozialarbeit haben sich von Personen, die mit Tätigkeiten betraut sind, die geeignet sind, Kontakt zu Minderjährigen aufzunehmen, vor deren Einstellung oder Aufnahme einer ehrenamtlichen Tätigkeit und in regelmäßigen Abständen ein Führungszeugnis nach § 30 Absatz 5 und § 30a Absatz 1 des Bundeszentralregistergesetzes vorlegen zu lassen.
§ 44 Absatz 3 Satz 4 bis 8 des Asylgesetzes gilt entsprechend.
Personen, die rechtskräftig wegen einer Straftat nach den §§ 171, 174 bis 174c, 176 bis 180a, 181a, 182 bis 184g, 225, 232 bis 233a, 234, 235 oder 236 des Strafgesetzbuches verurteilt worden sind, dürfen nicht im Aufgabengebiet der Migrationssozialarbeit beschäftigt oder mit ehrenamtlichen Tätigkeiten betraut werden.

(4) Weitere strukturelle und konzeptionelle Anforderungen an die Aufgabenwahrnehmung sowie fachliche und personelle Standards ergeben sich aus Anlage 4.

#### § 16 Datenübermittlung

Die Datenübermittlung an mit der Aufgabe Migrationssozialarbeit befasste Dritte nach § 19 Absatz 2 des Landesaufnahmegesetzes ist nur zulässig,

1.  wenn eine Erhebung der für die jeweilige Aufgabenerfüllung im Einzelfall erforderlichen personenbezogenen Daten bei der betroffenen Person nicht oder nicht zuverlässig möglich ist oder einen unverhältnismäßigen Aufwand erfordern würde und keine Anhaltspunkte dafür bestehen, dass schutzwürdige Interessen der betroffenen Person beeinträchtigt werden, oder
2.  wenn die Erhebung bei der betroffenen Person
    1.  den Zugang zur Hilfe ernsthaft gefährden oder
    2.  gesundheitliche Belange beeinträchtigen könnte.

### Abschnitt 4  
Sonstige Bestimmungen

#### § 17 Übergangsvorschrift zu Abschnitt 3

(1) Hat eine Kommune Aufgaben der sozialen Unterstützung durch Migrationssozialarbeit nach § 12 Absatz 2 Satz 1 des Landesaufnahmegesetzes bereits vor dem Inkrafttreten dieser Verordnung auf Dritte übertragen, kann von dem Grundsatz des Vorrangs der Aufgabenübertragung auf nichtstaatliche Träger der Sozialen Arbeit abgewichen werden.

(2) Das Umsetzungskonzept zur Migrationssozialarbeit nach § 15 Absatz 1 und Nummer 1.1 der Anlage 4 ist dem für Soziales zuständigen Ministerium spätestens zum 30.
Juni 2017 vorzulegen.

(3) Die in § 15 Absatz 2 und Anlage 4 normierten strukturellen, fachlichen und personellen Anforderungen an die Aufgabenwahrnehmung sind spätestens am 1.
Januar 2018 zu erfüllen.
Dies gilt nicht für die in den Nummern 1.3, 3.1, 3.2, 3.3, 3.4, 3.5, 3.7 und 3.9 der Anlage 4 genannten Anforderungen, die mit dem Inkrafttreten dieser Verordnung zu erfüllen sind.
Im Fall der Aufgabenübertragung auf geeignete Dritte nach § 14 Absatz 2 gilt Satz 1 entsprechend.

(4) Nimmt eine Kommune Aufgaben der Migrationssozialarbeit bereits vor dem Inkrafttreten dieser Verordnung selbst wahr oder hat diese auf geeignete Dritte übertragen, gilt Absatz 3 entsprechend.

#### § 18 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verteilungsverordnung vom 19.
Oktober 2010 (GVBl.
II Nr. 68) außer Kraft.

Potsdam, den 19.
Oktober 2016

Die Ministerin für Arbeit, Soziales,  
Gesundheit, Frauen und Familie

Diana Golze

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#a) Diese Verordnung dient der Umsetzung der Richtlinie 2013/33/EU des Europäischen Parlaments und des Rates vom 26.
Juni 2013 zur Festlegung von Normen für die Aufnahme von Personen, die internationalen Schutz beantragen (ABl. L 180 vom 29.6.2013, S. 96).

* * *

### Anlagen

1

[Anlage 1 - Verteilerschlüssel in Zuständigkeit des Landesamtes für Soziales und Versorgung](/br2/sixcms/media.php/68/GVBl_II_55_2016-Anlage-1.pdf "Anlage 1 - Verteilerschlüssel in Zuständigkeit des Landesamtes für Soziales und Versorgung") 602.4 KB

2

[Anlage 2 - Verteilerschlüssel in Zuständigkeit der Zentralen Ausländerbehörde](/br2/sixcms/media.php/68/GVBl_II_55_2016-Anlage-2.pdf "Anlage 2 - Verteilerschlüssel in Zuständigkeit der Zentralen Ausländerbehörde") 601.9 KB

3

[Anlage 3 - Mindestbedingungen für den Betrieb von Gemeinschaftsunterkünften](/br2/sixcms/media.php/68/GVBl_II_55_2016-Anlage-3.pdf "Anlage 3 - Mindestbedingungen für den Betrieb von Gemeinschaftsunterkünften") 611.2 KB

4

[Anlage 4 - Anforderungen an die Aufgabenwahrnehmung der sozialen Unterstützung durch Migrationssozialarbeit](/br2/sixcms/media.php/68/LaufnGDV-Anlage-4.pdf "Anlage 4 - Anforderungen an die Aufgabenwahrnehmung der sozialen Unterstützung durch Migrationssozialarbeit") 106.5 KB