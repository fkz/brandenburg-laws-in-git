## Weiterbildungs- und Prüfungsverordnung für Gesundheits- und Krankenpflegerinnen und Gesundheits- und Krankenpfleger sowie Gesundheits- und Kinderkrankenpflegerinnen und Gesundheits- und Kinderkrankenpfleger für Hygiene in der Pflege (Hygienefachkraft-Weiterbildungsverordnung - HygWBV)

Auf Grund des § 9 des Gesetzes über die Weiterbildung und Fortbildung in den Fachberufen des Gesundheitswesens vom 18.
März 1994 (GVBl.
I S. 62), der zuletzt durch Artikel 6 des Gesetzes vom 17. Dezember 2015 (GVBl.
I Nr. 38 S. 15) geändert worden ist, verordnet die Ministerin für Arbeit, Soziales, Gesundheit, Frauen und Familie:

#### § 1 Ziele der Weiterbildung

Die Weiterbildung soll spezielle Kenntnisse und Fertigkeiten in der Hygiene sowie die dazu erforderlichen Einstellungen und Verhaltensweisen vermitteln.
Sie soll dazu befähigen, die Aufgaben von Hygienefachkräften gemäß § 6 Absatz 1 der Verordnung über die Hygiene und Infektionsprävention in medizinischen Einrichtungen vom 6.
Februar 2012 (GVBl.
II Nr. 8), die durch die Verordnung vom 21. März 2016 (GVBl.
II Nr. 13) geändert worden ist, in der jeweils geltenden Fassung umfassend wahrzunehmen.

#### § 2 Form, Dauer und Inhalt der Weiterbildung

(1) Die Weiterbildung wird in berufsbegleitenden Lehrgängen oder in Lehrgängen mit Vollzeitunterricht durchgeführt.
Die Gestaltung der Weiterbildung in modularer Form ist möglich.

(2) Die berufsbegleitende Weiterbildung dauert mindestens zwei Jahre; sie darf vier Jahre nicht überschreiten.
In Vollzeitlehrgängen beträgt die Dauer der Weiterbildung zwölf Monate.
Sofern die Weiterbildung in modularer Form durchgeführt wird, darf sie vier Jahre nicht überschreiten.

(3) Die Weiterbildung umfasst berufsbegleitend und in Vollzeitform:

1.  900 Stunden theoretische Weiterbildung gemäß Anlage 1 Teil A, davon 800 Stunden theoretischer Unterricht von je 45 Minuten Dauer und 100 Stunden selbstgesteuertes Lernen von je 45 Minuten Dauer,
2.  850 Stunden angeleitete praktische Weiterbildung von je 60 Minuten Dauer gemäß Anlage 1 Teil B,
3.  die Prüfungen.

(4) Die theoretische Weiterbildung soll mit der angeleiteten praktischen Weiterbildung inhaltlich und zeitlich abgestimmt sein.
In den in der Anlage 1 Teil A aufgeführten Themenkomplexen sind Leistungsüberprüfungen durchzuführen.
Die Weiterbildungsstätte hat über die Teilnahme an der theoretischen Weiterbildung und über die Ergebnisse der Leistungsüberprüfungen Nachweise zu führen.

(5) Die praktische Weiterbildung in den in der Anlage 1 Teil B genannten Praktika muss unter fachkundiger Anleitung erfolgen.
Sie wird durch Lehrkräfte der Weiterbildungsstätte angemessen und regelmäßig begleitet.
Die Leistungen in den praktischen Einsatzbereichen gemäß Anlage 1 Teil B Nummer 1 bis 3 sind von der für die Anleitung zuständigen Fachkraft schriftlich zu bewerten.
Die absolvierten Stunden in den praktischen Einsatzbereichen gemäß Anlage 1 Teil B Nummer 4 und 5 sind nachzuweisen.

(6) Auf die Dauer der Weiterbildung werden Unterbrechungen gemäß § 6 Absatz 4 und 5 des Gesetzes über die Weiterbildung und Fortbildung in den Fachberufen des Gesundheitswesens angerechnet.
Die Weiterbildungsstätte kann in besonders begründeten Einzelfällen auf Antrag auch über Satz 1 hinausgehende Fehlzeiten berücksichtigen, soweit eine besondere Härte vorliegt und das Erreichen des Weiterbildungsziels durch die Anrechnung nicht gefährdet wird.

(7) Die Leitung der Weiterbildungsstätte kann auf Antrag Abschnitte anderer Weiterbildungen auf die Dauer der Weiterbildung anrechnen, wenn sie dem in Anlage 1 vorgeschriebenen Inhalt und Umfang im Wesentlichen entsprechen und das Erreichen des Weiterbildungsziels dadurch nicht gefährdet wird.

(8) Die Anrechnung anderer Weiterbildungen gemäß Absatz 7 ist mindestens sechs Wochen vor Beginn der Weiterbildung bei der Leitung der Weiterbildungsstätte zu beantragen.
Diese entscheidet nach Prüfung der Unterlagen über die anrechnungsfähigen Weiterbildungsabschnitte.

#### § 3 Weiterbildungsstätten

(1) Die Weiterbildung wird an Weiterbildungsstätten durchgeführt, die für das Weiterbildungsgebiet Hygienefachkraft staatlich anerkannt sind.

(2) Eine Weiterbildungsstätte wird auf Antrag bei der zuständigen Behörde gemäß der Verordnung zur Bestimmung der Zuständigkeiten für Fachberufe im Gesundheitswesen vom 25.
Januar 2016 (GVBl.
I Nr. 5 S. 24) in der jeweils geltenden Fassung für die Weiterbildung zur Hygienefachkraft staatlich anerkannt, wenn sie

1.  an mindestens ein Krankenhaus angegliedert ist oder mit mindestens einem Krankenhaus zusammenarbeitet, das die Anforderungen an die angeleitete praktische Weiterbildung gemäß § 2 Absatz 3 Nummer 2 und Absatz 5 erfüllt,
2.  für die angeleitete praktische Weiterbildung gemäß Anlage 1 Teil B eine ausreichende Anzahl an Weiterbildungsplätzen mit Hygienefachkräften für die Praxisanleitung nachweist, die mindestens ein Jahr Berufserfahrung als Hygienefachkraft haben und nach Möglichkeit eine berufspädagogische Zusatzqualifikation für die Praxisanleitung nachweisen,
3.  auf der Grundlage der Anlage 1 für die theoretische Weiterbildung und für die angeleitete praktische Weiterbildung inhaltlich und zeitlich differenzierte Lehrpläne und Praktikumsprogramme vorlegt und
4.  die Voraussetzungen der §§ 4 und 5 erfüllt.

(3) Die Organisation der Weiterbildung obliegt der Leitung der Weiterbildungsstätte.

(4) Die Teilnehmerzahl für eine Weiterbildung soll 25 Personen nicht überschreiten.

(5) Vor Beginn der Weiterbildung ist den Bewerberinnen oder den Bewerbern eine persönliche Beratung anzubieten.

(6) Mit der Teilnehmerin oder dem Teilnehmer ist ein Weiterbildungsvertrag abzuschließen, in dem die Rechte und Pflichten der Weiterbildungsstätte und der Teilnehmerin oder des Teilnehmers und des Trägers geregelt sind.
Darüber hinaus sind die Vereinbarungen über Weiterbildungszeiten, Weiterbildungsunterbrechungen, Weiterbildungsabbruch und Kündigungen, Teilnahmegebühren und Zahlungsmodalitäten zu treffen.

(7) Jede Veränderung der tatsächlichen Umstände ist der zuständigen Behörde anzuzeigen.
Bei Rücknahme oder Widerruf der staatlichen Anerkennung gelten die Regelungen des Verwaltungsverfahrensgesetzes für das Land Brandenburg vom 7.
Juli 2009 (GVBl.
I S. 262, 264), das zuletzt durch Artikel 3 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 32 S. 23) geändert worden ist, in der jeweils geltenden Fassung in Verbindung mit dem Verwaltungsverfahrensgesetz in der Fassung der Bekanntmachung vom 23.
Januar 2003 (BGBl. I S. 102), das zuletzt durch Artikel 20 des Gesetzes vom 18.
Juli 2016 (BGBl. I S. 1679, 1708) geändert worden ist, in der jeweils geltenden Fassung.

(8) Erfüllt die Weiterbildungsstätte einzelne Voraussetzungen nach Absatz 2 Nummer 2 oder den §§ 4 und 5 nicht oder nicht in vollem Umfang, können auf Antrag bei der zuständigen Behörde in begründeten Einzelfällen, insbesondere wenn es das öffentliche Interesse erfordert, Ausnahmen zugelassen werden.

#### § 4 Personelle Voraussetzungen der Weiterbildungsstätte

(1) Die Weiterbildungsstätte muss von einer Fachkraft hauptamtlich geleitet werden.
Die Leitung der Weiterbildungsstätte muss folgende Voraussetzungen erfüllen:

1.  Nachweis über die Erlaubnis zum Führen der Berufsbezeichnung nach dem Krankenpflegegesetz,
2.  Nachweis über die Berechtigung zum Führen der Weiterbildungsbezeichnung „Fachgesundheits- und Krankenpflegerin oder Fachgesundheits- und Krankenpfleger für Hygiene in der Pflege“ oder „Fachgesundheits- und Kinderkrankenpflegerin oder Fachgesundheits- und Kinderkrankenpfleger für Hygiene in der Pflege“ oder Nachweis über die Berechtigung zum Führen einer entsprechenden Weiterbildungsbezeichnung, die in anderen Bundesländern aufgrund gesetzlicher oder allgemein anerkannter Regelungen erworben worden ist,
3.  Nachweis über mindestens zwei Jahre Berufserfahrung im jeweiligen Berufsfeld als Hygienefachkraft in stationären Einrichtungen und
4.  Nachweis eines Masterabschlusses, der zur Lehre in Gesundheitsberufen befähigt, oder eines gleichwertigen Hochschulabschlusses für die Lehre in Gesundheitsberufen, insbesondere auf dem Gebiet der Medizinpädagogik oder Pflegepädagogik.

(2) Die Leitung der Weiterbildungsstätte kann auch aus zwei geeigneten Personen bestehen, die gemeinsam die Voraussetzungen gemäß Absatz 1 erfüllen.

(3) Die Weiterbildungsstätte muss über fachlich und pädagogisch geeignete Lehrkräfte für die in der Anlage 1 Teil A genannten Themenkomplexe verfügen, davon mindestens

1.  eine fachweitergebildete Hygienefachkraft, die in einem Krankenhaus oder einer medizinischen Einrichtung tätig ist, und
2.  eine Krankenhaushygienikerin oder ein Krankenhaushygieniker gemäß § 7 Absatz 2 Satz 1 der Verordnung über die Hygiene und Infektionsprävention in medizinischen Einrichtungen.

#### § 5 Räumliche und sächliche Voraussetzungen der Weiterbildungsstätte

(1) Die Weiterbildungsstätte muss über geeignete Räume für die Weiterbildung verfügen.
Dazu gehören mindestens ein Unterrichtsraum mit einer Grundfläche von mindestens 2 Quadratmetern für jede Teilnehmerin und jeden Teilnehmer zuzüglich 10 Quadratmeter für die Lehrkraft, ein Raum für den Gruppenunterricht, ein Pausenraum sowie ausreichende sanitäre Einrichtungen.
Die erforderlichen Lehr- und Lernmittel müssen vorhanden sein, insbesondere aktuelle Fachliteratur sowie PC-Arbeitsplätze mit Internetzugang.

(2) Die Weiterbildungsstätte ist verpflichtet, die einschlägigen Anforderungen der Bau-, Brand-, Gesundheits- und Arbeitsschutzbestimmungen einzuhalten und entsprechende Nachweise vorzulegen.

#### § 6 Zugangsvoraussetzungen

(1) Voraussetzungen für den Zugang zur Weiterbildung sind die Erlaubnis zum Führen der Berufsbezeichnung nach dem Krankenpflegegesetz und eine mindestens zweijährige Berufserfahrung.

(2) Bei Unterbrechungen zwischen der erforderlichen Berufsausübung und der Weiterbildung gelten die Regelungen gemäß § 6 Absatz 2 und 3 des Gesetzes über die Weiterbildung und Fortbildung in den Fachberufen des Gesundheitswesens.

(3) Über den Zugang zur Weiterbildung entscheidet die Leitung der Weiterbildungsstätte auf Antrag.
Dem Antrag sind ein Identitätsnachweis und die Nachweise der Voraussetzungen gemäß Absatz 1 in beglaubigter Form beizufügen.

#### § 7 Prüfungsausschuss

(1) Die Weiterbildungsstätte richtet einen Prüfungsausschuss ein, der aus folgenden Mitgliedern besteht:

1.  der Leitung der Weiterbildungsstätte als Vorsitzende oder Vorsitzender,
2.  einer von der zuständigen Behörde gemäß der Verordnung zur Bestimmung der Zuständigkeiten für Fachberufe im Gesundheitswesen beauftragten Person als stellvertretende Vorsitzende oder stellvertretender Vorsitzender,
3.  einer Lehrkraft mit fachlicher Qualifikation gemäß § 4 Absatz 3 Nummer 2 und
4.  einer weiteren Lehrkraft, die den überwiegenden Teil der Themenkomplexe gemäß Anlage 1 Teil A unterrichtet haben soll.

Für jedes Mitglied des Prüfungsausschusses, mit Ausnahme der Vorsitzenden oder des Vorsitzenden, ist eine Vertretung zu benennen.

(2) Die Zusammensetzung des Prüfungsausschusses ist der zuständigen Behörde spätestens zwölf Wochen vor Beginn der Prüfung anzuzeigen.

(3) Die oder der Vorsitzende des Prüfungsausschusses bestimmt die Prüfungstermine und Prüfungsorte.
Sie oder er ist zuständig für die Zulassung zur Prüfung sowie für die Auswahl der Prüfungsaufgaben und der Hilfsmittel nach den Vorschlägen der Prüferinnen oder Prüfer.
Die oder der Vorsitzende des Prüfungsausschusses leitet die Prüfung, sorgt für ihren ordnungsgemäßen Ablauf und verkündet die Prüfungsnoten.

#### § 8 Zulassung zur Prüfung

(1) Die oder der Vorsitzende des Prüfungsausschusses entscheidet auf Antrag des Prüflings über die Zulassung zur Prüfung und setzt die Prüfungstermine fest.
Der Antrag auf Zulassung zur Prüfung ist acht Wochen vor Ende der Weiterbildung zu stellen.
Die Prüfungstermine und die Zulassung sind dem Prüfling spätestens zwei Wochen vor Prüfungsbeginn schriftlich mitzuteilen; eine Ablehnung ist zu begründen.

(2) Die Zulassung zur Prüfung wird auf Antrag erteilt, wenn

1.  die Bescheinigung über die Teilnahme an der theoretischen Weiterbildung nach dem Muster der Anlage 2 belegt, dass der erforderliche Mindestumfang der Weiterbildung gemäß § 2 Absatz 3 Nummer 1 in Verbindung mit § 2 Absatz 6 und 7 abgeleistet wurde und die einzelnen Themenkomplexe erfolgreich mit mindestens einem Durchschnitt von „ausreichend“ absolviert wurden, sowie
2.  die Bescheinigung über die Teilnahme an der angeleiteten praktischen Weiterbildung nach dem Muster der Anlage 3 belegt, dass der erforderliche Mindestumfang der Weiterbildung gemäß § 2 Absatz 3 Nummer 2 in Verbindung mit § 2 Absatz 6 abgeleistet wurde.

#### § 9 Prüfung

Die Prüfung besteht aus einem schriftlichen und einem mündlichen Teil.
Prüfungsteile können miteinander verbunden werden.
Zwischen den einzelnen Prüfungsteilen muss mindestens ein prüfungsfreier Tag liegen.
Die Prüfung darf frühestens vier Wochen vor Abschluss der Weiterbildung beginnen.

#### § 10 Schriftliche Prüfung

(1) Der schriftliche Teil der Prüfung besteht aus einer Aufsichtsarbeit.
Die Fragen oder Themen sind aus den Themenkomplexen der Anlage 1 Teil A zu wählen.

(2) In der Aufsichtsarbeit hat der Prüfling einzelne Fragen im Antwort-Auswahl-Verfahren oder frei formuliert zu beantworten oder eines aus drei zur Auswahl gestellten Themen abzuhandeln.
Kombinationen sind möglich.
Die Aufsichtsarbeit dauert 180 Minuten.
Bei Vorliegen einer Behinderung sind entsprechende Nachteilsausgleiche einzuräumen.

(3) Anstelle der Aufsichtsarbeit kann eine Facharbeit zu einem praxisbezogenen Thema verlangt werden, die innerhalb von zwei Monaten zu fertigen ist.
Der Umfang der Hausarbeit ist themenabhängig zu begrenzen.
Der Prüfling hat die benutzten Hilfsmittel anzugeben und schriftlich zu versichern, dass er die Arbeit eigenständig angefertigt hat.

(4) Die Aufgaben des schriftlichen Teils der Prüfung werden von der Vorsitzenden oder dem Vorsitzenden des Prüfungsausschusses auf Vorschlag der Prüferinnen oder Prüfer festgelegt.

(5) Die schriftlichen Arbeiten sind von mindestens zwei Mitgliedern des Prüfungsausschusses unabhängig voneinander zu bewerten.
Aus den Noten bildet die oder der Vorsitzende des Prüfungsausschusses im Benehmen mit den Prüferinnen oder Prüfern die Note für den schriftlichen Teil der Prüfung.

#### § 11 Mündliche Prüfung

(1) Der mündliche Teil der Prüfung erstreckt sich auf die Themenkomplexe der Anlage 1 Teil A.
Der Prüfungsinhalt soll sich auf konkrete praktische Aufgaben beziehen.

(2) Die Prüflinge werden einzeln oder in Gruppen bis zu drei Prüflingen geprüft.
Die Prüfungszeit soll für den einzelnen Prüfling insgesamt 30 Minuten nicht überschreiten.

(3) Aus den Noten der Prüferinnen oder Prüfer und im Benehmen mit ihnen bildet die oder der Vorsitzende des Prüfungsausschusses die Note für den mündlichen Teil der Prüfung.

#### § 12 Benotung

Die Leistungen während der Weiterbildung und jede einzelne Prüfungsleistung werden wie folgt benotet:

1.  „sehr gut“ (1), wenn die Leistung den Anforderungen in besonderem Maße entspricht,
2.  „gut“ (2), wenn die Leistung den Anforderungen voll entspricht,
3.  „befriedigend“ (3), wenn die Leistung im Allgemeinen den Anforderungen entspricht,
4.  „ausreichend“ (4), wenn die Leistung zwar Mängel aufweist, aber im Ganzen den Anforderungen noch entspricht,
5.  „mangelhaft“ (5), wenn die Leistung den Anforderungen nicht entspricht, jedoch erkennen lässt, dass die notwendigen Grundkenntnisse vorhanden sind und die Mängel in absehbarer Zeit behoben werden können,
6.  „ungenügend“ (6), wenn die Leistung den Anforderungen nicht entspricht und selbst die Grundkenntnisse so lückenhaft sind, dass die Mängel in absehbarer Zeit nicht behoben werden können.

#### § 13 Bestehen und Wiederholen der Prüfung

(1) Die Gesamtnote der Prüfung wird zu gleichen Teilen aus den Ergebnissen der schriftlichen und der mündlichen Prüfung ermittelt.
Die Prüfung ist bestanden, wenn jeder Prüfungsteil mindestens mit „ausreichend“ bewertet wird.

(2) Die oder der Vorsitzende des Prüfungsausschusses erteilt über die bestandene Prüfung ein Zeugnis nach dem Muster der Anlage 4 und bescheinigt die Berechtigung zum Führen der Weiterbildungsbezeichnung.

(3) Über das Nichtbestehen der Prüfung erteilt die oder der Vorsitzende des Prüfungsausschusses einen schriftlichen Bescheid, in dem die Prüfungsnoten anzugeben sind.

(4) Jeder Teil der Prüfung kann auf Antrag bei der oder dem Vorsitzenden des Prüfungsausschusses einmal wiederholt werden, wenn der Prüfling die Note „mangelhaft“ oder „ungenügend“ erhalten hat.

(5) Hat der Prüfling alle Teile der Prüfung nicht bestanden, darf er zur Wiederholungsprüfung nur zugelassen werden, wenn er an einer weiteren Weiterbildung teilgenommen hat, deren Dauer und Inhalt von der oder dem Vorsitzenden des Prüfungsausschusses bestimmt wird.
Die Frist bis zur erneuten Prüfung beträgt mindestens drei und höchstens zwölf Monate.
Der Antrag auf Zulassung zur Wiederholungsprüfung ist bei der oder dem Vorsitzenden des Prüfungsausschusses zu stellen.
Ein Nachweis über die Erfüllung der Auflagen ist dem Antrag beizufügen.

#### § 14 Rücktritt von der Prüfung, Prüfungsversäumnis

(1) Nach Zulassung zur Prüfung ist ein Rücktritt nur mit Genehmigung der oder des Vorsitzenden des Prüfungsausschusses zulässig.
Der Prüfling hat die Gründe für seinen Rücktritt unverzüglich schriftlich mitzuteilen und glaubhaft zu machen.
Genehmigt die oder der Vorsitzende des Prüfungsausschusses den Rücktritt von der gesamten Prüfung oder von einem Prüfungsteil, so gilt die Prüfung als nicht unternommen.
Die Genehmigung ist nur zu erteilen, wenn ein wichtiger Grund vorliegt.
Wird der Rücktritt von der Prüfung oder von einem Prüfungsteil nicht genehmigt, so gilt die Prüfung als nicht bestanden.

(2) Absatz 1 gilt entsprechend, wenn der Prüfling einen Prüfungstermin versäumt oder die Aufsichtsarbeit oder die Hausarbeit nicht oder nicht fristgerecht abgibt oder die Prüfung unterbricht.

(3) Der Prüfling wird im Fall der Genehmigung des Rücktritts zum nächsten Prüfungstermin geladen.

#### § 15 Täuschungsversuche, Störung der Prüfung

Die oder der Vorsitzende des Prüfungsausschusses kann bei Prüflingen, die die ordnungsgemäße Durchführung der Prüfung in erheblichem Maße gestört oder sich eines Täuschungsversuchs schuldig gemacht haben, den betreffenden Teil der Prüfung für nicht bestanden erklären; § 13 Absatz 4 gilt entsprechend.
Eine solche Entscheidung ist im Fall der Störung der Prüfung nur bis zum Abschluss der gesamten Prüfung, im Fall eines Täuschungsversuchs nur innerhalb von drei Jahren nach Abschluss der Prüfung zulässig.

#### § 16 Prüfungsniederschrift, Prüfungsunterlagen

(1) Über die Prüfung ist für jeden Prüfling eine Niederschrift anzufertigen, die von allen Mitgliedern des Prüfungsausschusses zu unterschreiben ist.
Sie muss den Namen des Prüflings, die Prüfungsgebiete, die Prüfungstage und Prüfungszeiten, besondere Vorkommnisse, die einzelnen Noten sowie die Gesamtnote enthalten.

(2) Auf Antrag ist dem Prüfling nach Abschluss der Prüfung Einsicht in seine Prüfungsunterlagen zu gewähren.

(3) Schriftliche Prüfungsarbeiten sind drei Jahre, Anträge auf Zulassung zur Prüfung und Prüfungsniederschriften zehn Jahre aufzubewahren.
Die Aufbewahrungs- oder Speicherungsfrist beginnt mit der Erteilung der Berechtigung zum Führen der Weiterbildungsbezeichnung der betroffenen Teilnehmerin oder des betroffenen Teilnehmers.

#### § 17 Weiterbildungsbezeichnung

(1) Die Berechtigung zum Führen der Weiterbildungsbezeichnung „Fachgesundheits- und Krankenpflegerin oder Fachgesundheits- und Krankenpfleger für Hygiene in der Pflege“ oder „Fachgesundheits- und Kinderkrankenpflegerin oder Fachgesundheits- und Kinderkrankenpfleger für Hygiene in der Pflege“ erhält, wer die vorgeschriebene Weiterbildung abgeschlossen und die Prüfung bestanden hat.

(2) Die Weiterbildungsbezeichnung darf nur in Verbindung mit der Erlaubnis zum Führen der Berufsbezeichnung geführt werden.

(3) Die Berechtigung zum Führen der Weiterbildungsbezeichnung wird durch die Weiterbildungsstätte im Zeugnis nach dem Muster der Anlage 4 und nach Maßgabe von § 7 Absatz 4 des Gesetzes über die Weiterbildung und Fortbildung in den Fachberufen des Gesundheitswesens bescheinigt.

#### § 18 Übergangsbestimmungen

(1) Abweichend von § 3 Absatz 1 dürfen für eine Übergangszeit von höchstens zwei Jahren die bei Inkrafttreten dieser Verordnung betriebenen Weiterbildungsstätten ohne staatliche Anerkennung die Weiterbildung zur Hygienefachkraft durchführen.
Für diese Übergangszeit kann von den Voraussetzungen nach § 3 Absatz 2 abgesehen werden.

(2) Eine vor Inkrafttreten dieser Verordnung begonnene Weiterbildung zur Hygienefachkraft an einer Weiterbildungsstätte ohne staatliche Anerkennung kann fortgeführt und abgeschlossen werden.

(3) Weiterbildungen zur Hygienefachkraft, die im Land Brandenburg vor Inkrafttreten dieser Verordnung erfolgreich abgeschlossen wurden, und Weiterbildungen, die vor Inkrafttreten dieser Verordnung begonnen und nach Inkrafttreten dieser Verordnung erfolgreich abgeschlossen werden, sind als gleichwertig anzuerkennen, wenn diese aufgrund gesetzlicher Regelungen anderer Bundesländer oder auf der Grundlage von Richtlinien oder Empfehlungen entsprechender Fachgesellschaften durchgeführt wurden und die Durchführung der Weiterbildung mit dem für Gesundheit zuständigen Mitglied der Landesregierung abgestimmt ist.
Auf Antrag wird die Berechtigung zum Führen der Weiterbildungsbezeichnung nach dem Muster der Anlage 5 bescheinigt.
§ 17 gilt entsprechend mit der Maßgabe, dass die Bescheinigung nach Satz 1 durch die Leitung einer anerkannten und von der zuständigen Behörde hierzu beauftragten Weiterbildungsstätte ausgestellt wird.

(4) Weiterbildungsbezeichnungen, die in den übrigen Bundesländern aufgrund gesetzlicher oder allgemein anerkannter Regelungen der Weiterbildung zur Hygienefachkraft erworben worden sind, dürfen im Land Brandenburg gemäß § 4 Absatz 3 des Gesetzes über die Weiterbildung und Fortbildung in den Fachberufen des Gesundheitswesens geführt werden.

#### § 19 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 29.
Mai 2017

Die Ministerin für Arbeit, Soziales,  
Gesundheit, Frauen und Familie

Diana Golze

* * *

### Anlagen

1

[Anlage 1 - Teil A Theoretische Weiterbildung (900 Stunden), Teil B Angeleitete praktische Weiterbildung (850 Stunden)](/br2/sixcms/media.php/68/GVBl_II_33_2017-Anlage-1.pdf "Anlage 1 - Teil A Theoretische Weiterbildung (900 Stunden), Teil B Angeleitete praktische Weiterbildung (850 Stunden)") 708.0 KB

2

[Anlage 2 - Bescheinigung über die Teilnahme an der theoretischen Weiterbildung](/br2/sixcms/media.php/68/GVBl_II_33_2017-Anlage-2.pdf "Anlage 2 - Bescheinigung über die Teilnahme an der theoretischen Weiterbildung") 579.8 KB

3

[Anlage 3 - Bescheinigung über die Teilnahme an der praktischen Weiterbildung](/br2/sixcms/media.php/68/GVBl_II_33_2017-Anlage-3.pdf "Anlage 3 - Bescheinigung über die Teilnahme an der praktischen Weiterbildung") 578.3 KB

4

[Anlage 4 - Zeugnis](/br2/sixcms/media.php/68/GVBl_II_33_2017-Anlage-4.pdf "Anlage 4 - Zeugnis") 579.4 KB

5

[Anlage 5 - Berechtigung zum Führen der Weiterbildungsbezeichnung gemäß § 18 Absatz 3 der Hygienefachkraft-Weiterbildungsverordnung](/br2/sixcms/media.php/68/GVBl_II_33_2017-Anlage-5.pdf "Anlage 5 - Berechtigung zum Führen der Weiterbildungsbezeichnung gemäß § 18 Absatz 3 der Hygienefachkraft-Weiterbildungsverordnung") 590.5 KB