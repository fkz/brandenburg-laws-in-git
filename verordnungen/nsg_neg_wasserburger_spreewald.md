## Verordnung über das Naturschutzgebiet „Naturentwicklungsgebiet Wasserburger Spreewald“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl. I S. 2542_),_ von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl. I S. 1972) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nr. 43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Dahme-Spreewald wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Naturentwicklungsgebiet Wasserburger Spreewald“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 140 Hektar und besteht aus drei Teilflächen.
Es befindet sich innerhalb des Geltungsbereiches der Verordnung über die Festsetzung von Naturschutzgebieten und einem Landschaftsschutzgebiet von zentraler Bedeutung mit der Gesamtbezeichnung „Biosphärenreservat Spreewald“ vom 12. September 1990 (GBl.
Sonderdruck Nr. 1473) und umfasst folgende Flächen:

Gemeinde:

Gemarkung:

Flur:

Flurstücke:

Krausnick-Groß Wasserburg

Groß Wasserburg

4

1/2, 2/3 jeweils anteilig, 3 bis 5 vollständig, 8, 9,  
13, 15 jeweils anteilig;

Schlepzig

Schlepzig

17

18/2, 21 bis 23, 24/8 jeweils anteilig.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten mit den Blattnummern 1 und 2 im Maßstab 1 : 10 000 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 5 aufgeführten Liegenschaftskarten.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Dahme-Spreewald, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

Schutzzweck des Naturschutzgebietes ist es, die Funktion einer Kernzone innerhalb des „Biosphärenreservates Spreewald“ zu erfüllen.
Insbesondere werden naturnahe Erlenbruch-, Erlen-Eschen- und Stieleichen-Hainbuchen-Wälder auf Niedermoor-, Anmoor- und Gley-Standorten sowie Buchenwälder auf sandigen Standorten der direkten menschlichen Einflussnahme entzogen und der natürlichen Entwicklung überlassen.

#### § 4 Verbote, Zulässige Handlungen

(1) Im Naturschutzgebiet „Naturentwicklungsgebiet Wasserburger Spreewald“ ist jegliche wirtschaftliche Nutzung verboten.
Die Bestandsregulierung von wild lebenden Tierarten entsprechend den Zielsetzungen für das Biosphärenreservat nach Maßgabe der Biosphärenreservatsverwaltung und die Ausübung der traditionellen Spreewaldfischerei bleiben zulässig.

(2) Im Übrigen gelten weiterhin die Regelungen für die Schutzzone II gemäß § 4 Absatz 3 Satz 1 und Satz 2 Nummer 8 (Naturschutzgebiet „Innerer Unterspreewald“) in Verbindung mit den §§ 6, 7 und 9 der Verordnung über die Festsetzung von Naturschutzgebieten und einem Landschaftsschutzgebiet von zentraler Bedeutung mit der Gesamtbezeichnung „Biosphärenreservat Spreewald“.

#### § 5 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 6 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 7 Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 17 und 18 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 29 Absatz 3 und § 30 des Bundesnaturschutzgesetzes), über das Netz „Natura 2000“ (§§ 30 und 34 des Bundesnaturschutzgesetzes) und über den Schutz wild lebender Tier- und Pflanzenarten, ihrer Lebensstätten und Biotope (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 54 Absatz 7 des Bundesnaturschutzgesetzes) unberührt.

#### § 8 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 9 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 28.
April 2017

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes "Naturentwicklungsgebiet Wasserburger Spreewald"](/br2/sixcms/media.php/68/GVBl_II_25_2017-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes ") 1.2 MB

2

[Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten](/br2/sixcms/media.php/68/GVBl_II_25_2017-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten") 604.9 KB