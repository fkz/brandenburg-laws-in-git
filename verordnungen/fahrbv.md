## Verordnung über die Erteilung einer Fahrberechtigung an Angehörige der Freiwilligen Feuerwehren, des Technischen Hilfswerks und sonstiger Einheiten des Katastrophenschutzes (Fahrberechtigungsverordnung - FahrBV)

Auf Grund des § 6 Absatz 2 in Verbindung mit § 12 Absatz 1 Satz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), von denen durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28 S. 2) § 6 Absatz 2 geändert und § 12 Absatz 1 Satz 2 eingefügt worden ist, sowie § 6 Absatz 5 Satz 1 und 2 des Straßenverkehrsgesetzes in der Fassung der Bekanntmachung vom 5.
März 2003 (BGBl. I S. 310, 919), der durch Artikel 1 des Gesetzes vom 23. Juni 2011 (BGBl. I S. 1213) neu gefasst worden ist, verordnet die Landesregierung:

#### § 1 Fahrberechtigung

(1) Angehörigen der Freiwilligen Feuerwehren, des Technischen Hilfswerks und sonstiger Einheiten des Katastrophenschutzes, die ihre Tätigkeit ehrenamtlich ausüben und seit mindestens zwei Jahren eine gültige Fahrerlaubnis der Klasse B besitzen, kann auf Antrag eine Fahrberechtigung erteilt werden, die zum Führen von Einsatzfahrzeugen auf öffentlichen Straßen bis zu einer zulässigen Gesamtmasse von 4,75 t, auch mit Anhänger, sofern die zulässige Gesamtmasse der Fahrzeugkombination 4,75 t nicht übersteigt, berechtigt.

(2) Die Fahrberechtigung darf nur erteilt werden, wenn der Antragsteller oder die Antragstellerin eine Ausbildung nach § 2 absolviert hat, die das Erlernen von Fähigkeiten und Verhaltensweisen zum Führen von Fahrzeugen bis zu einer Gesamtmasse von 4,75 t oder einer Fahrzeugkombination, deren Gesamtmasse 4,75 t nicht übersteigt, zum Gegenstand hat, und seine Befähigung in einer praktischen Prüfung nach § 3 nachgewiesen hat.

(3) Die Absätze 1 und 2 gelten entsprechend für die Erteilung einer Fahrberechtigung zum Führen von Einsatzfahrzeugen auf öffentlichen Straßen bis zu einer zulässigen Gesamtmasse von 7,5 t, auch mit Anhänger, wenn die zulässige Gesamtmasse der Fahrzeugkombination 7,5 t nicht übersteigt.

(4) Die Fahrberechtigung nach den Absätzen 1 bis 3 wird durch Aushändigung eines Nachweises nach Anlage 1 erteilt.

#### § 2 Ausbildung

(1) Ziel der Ausbildung ist die Befähigung zum sicheren Führen eines Fahrzeugs oder einer Fahrzeugkombination bis zu einer Gesamtmasse von 4,75 t oder 7,5 t.
Inhalt, Umfang und Durchführung der Ausbildung richten sich nach Anlage 2.

(2) Ausbildungsbefugt sind Inhaberinnen und Inhaber einer Fahrlehrerlaubnis im Sinne des Fahrlehrergesetzes in der jeweils geltenden Fassung, die zur Ausbildung in der Fahrerlaubnisklasse C1 berechtigt, sowie Personen, die

1.  das 30.
    Lebensjahr vollendet haben,
2.  mindestens seit fünf Jahren eine gültige Fahrerlaubnis der Klasse C1 besitzen,
3.  zum Zeitpunkt der Ausbildung im Fahreignungsregister mit nicht mehr als einem Punkt belastet sind,
4.  einem ausbildenden Aufgabenträger für den örtlichen Brandschutz und die örtliche Hilfeleistung oder einer im Katastrophenschutz mitwirkenden Organisation, die Ausbildungen durchführt, angehören.

Der ausbildende Aufgabenträger für den örtlichen Brandschutz und die örtliche Hilfeleistung oder die im Katastrophenschutz mitwirkende Organisation, die Ausbildungen durchführt, kann zur Prüfung der Voraussetzung nach Satz 1 Nummer 3 die Vorlage einer Auskunft aus dem Verkehrszentralregister verlangen.

(3) Die praktische Ausbildung darf erst im öffentlichen Straßenverkehr durchgeführt werden, wenn sich die ausbildungsberechtigte Person davon überzeugt hat, dass die Bewerberin oder der Bewerber das Führen des jeweiligen Ausbildungsfahrzeugs gemäß Nummer 3 der Anlage 2 beherrscht.

(4) Der Abschluss der Ausbildung wird in einer Ausbildungsbescheinigung bestätigt, die den Anforderungen der Anlage 3 entsprechen muss.

#### § 3 Praktische Prüfung

(1) Die Befähigung zum Führen von Einsatzfahrzeugen ist in einer praktischen Prüfung nach Anlage 4 nachzuweisen.
Die praktische Prüfung hat im öffentlichen Straßenverkehr zu erfolgen.
Personen, die die Befähigung zum Führen von Einsatzfahrzeugen prüfen (Prüfpersonen), werden von den in § 1 Absatz 1 genannten Organisationen bestimmt und nehmen die Prüfung ab.
§ 2 Absatz 2 gilt entsprechend.
Prüfperson und ausbildende Person dürfen nicht identisch sein.

(2) Das Bestehen der praktischen Prüfung wird in einer Prüfungsbescheinigung nach Anlage 5 bestätigt.

(3) Die Ausbildungsbescheinigung und die Prüfungsbescheinigung sind der für die Erteilung der Fahrberechtigung zuständigen Stelle vorzulegen.

#### § 4 Erlöschen und Ruhen der Fahrberechtigung

(1) Die Fahrberechtigung erlischt

1.  mit der unanfechtbaren oder sofort vollziehbaren Entziehung der allgemeinen Fahrerlaubnis,
2.  im Fall des Verzichts auf die Fahrerlaubnis der Klasse B.

In diesen Fällen ist die Fahrberechtigung zurückzugeben.

(2) Während der Dauer eines Fahrverbots nach § 25 des Straßenverkehrsgesetzes darf von der Fahrberechtigung kein Gebrauch gemacht werden.

#### § 5 Zuständigkeiten

Zuständig für die Erteilung der Fahrberechtigung nach § 1 Absatz 1, 3 und 4 sowie deren Widerruf und Einzug sind

1.  der Aufgabenträger für den örtlichen Brandschutz und die örtliche Hilfeleistung für die Angehörigen der Freiwilligen Feuerwehr in seinem Gebiet,
2.  der Landkreis oder die kreisfreie Stadt für die Angehörigen des Technischen Hilfswerks und für die Angehörigen sonstiger Einheiten, die nach § 18 des Brandenburgischen Brand- und Katastrophenschutzgesetzes im Katastrophenschutz in ihrem Bereich mitwirken.

#### § 6 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 8.
Februar 2018

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter

* * *

### Anlagen

1

[Anlage 1 (zu § 1 Absatz 4) - Nachweis der Fahrberechtigung zum Führen eines Einsatzfahrzeugs der Freiwilligen Feuerwehren, des Technischen Hilfswerks und sonstiger Einheiten des Katastrophenschutzes](/br2/sixcms/media.php/68/GVBl_II_14_2018-Anlage-1.pdf "Anlage 1 (zu § 1 Absatz 4) - Nachweis der Fahrberechtigung zum Führen eines Einsatzfahrzeugs der Freiwilligen Feuerwehren, des Technischen Hilfswerks und sonstiger Einheiten des Katastrophenschutzes") 188.1 KB

2

[Anlage 2 (zu § 2 Absatz 1 und 3) - Ausbildung](/br2/sixcms/media.php/68/GVBl_II_14_2018-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 1 und 3) - Ausbildung") 205.9 KB

3

[Anlage 3 (zu § 2 Absatz 4) - Ausbildungsbescheinigung zum Führen von Einsatzfahrzeugen mit einer zulässigen Gesamtmasse von mehr als 3,5 t bis zu 7,5 t](/br2/sixcms/media.php/68/GVBl_II_14_2018-Anlage-3.pdf "Anlage 3 (zu § 2 Absatz 4) - Ausbildungsbescheinigung zum Führen von Einsatzfahrzeugen mit einer zulässigen Gesamtmasse von mehr als 3,5 t bis zu 7,5 t") 195.9 KB

4

[Anlage 4 (zu § 3 Absatz 1) - Fahrberechtigungsprüfung für Einsatzfahrzeuge mit einer zulässigen Gesamtmasse von mehr als 3,5 t bis 7,5 t](/br2/sixcms/media.php/68/GVBl_II_14_2018-Anlage-4.pdf "Anlage 4 (zu § 3 Absatz 1) - Fahrberechtigungsprüfung für Einsatzfahrzeuge mit einer zulässigen Gesamtmasse von mehr als 3,5 t bis 7,5 t") 208.3 KB

5

[Anlage 5 (zu § 3 Absatz 2) - Prüfungsbescheinigung zum Führen von Einsatzfahrzeugen mit einer zulässigen Gesamtmasse von mehr als 3,5 t bis zu 7,5 t](/br2/sixcms/media.php/68/GVBl_II_14_2018-Anlage-5.pdf "Anlage 5 (zu § 3 Absatz 2) - Prüfungsbescheinigung zum Führen von Einsatzfahrzeugen mit einer zulässigen Gesamtmasse von mehr als 3,5 t bis zu 7,5 t") 197.8 KB