## Verordnung zur Abwendung erheblicher fischereiwirtschaftlicher Schäden durch Kormorane sowie zum Schutz der natürlich vorkommenden Tierwelt (Brandenburgische Kormoranverordnung - BbgKorV)

Auf Grund des § 45 Absatz 7 Satz 4 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl. I S. 2542) in Verbindung mit § 30 Absatz 4 Satz 1 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 1 Absatz 2 Satz 2 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nr. 43) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1  
**Tötung von Kormoranen**

(1) Zur Abwendung erheblicher fischereiwirtschaftlicher Schäden und zum Schutz der natürlich vorkommenden Tierwelt dürfen Kormorane (Phalacrocorax carbo sinensis) abweichend von § 44 Absatz 1 Nummer 1 des Bundesnaturschutzgesetzes nach Maßgabe des Absatzes 2 sowie der §§ 3 bis 4 mit einer für die Jagd zugelassenen Schusswaffe getötet werden.
Bleischrot darf als Munition nicht verwendet werden.

(2) Die Tötung von Kormoranen nach Absatz 1 ist nur in der Zeit von Sonnenaufgang bis Sonnenuntergang zulässig auf, über oder näher als 500 Meter an

1.  bewirtschafteten Anlagen der Teichwirtschaft oder Fischzucht und -haltung oder
2.  einem Gewässer, an dem ein Fischereirecht nach § 3 Absatz 1 des Fischereigesetzes für das Land Brandenburg vom 13.
    Mai 1993 (GVBl.
    I S. 178), das zuletzt durch Artikel 3 des Gesetzes vom 15.
    Juli 2010 (GVBl.
    INr.
    28 S. 7) geändert worden ist, besteht.

(3) Im Zeitraum vom 16.
März bis zum 15.
August eines jeden Jahres dürfen nur immatur gefärbte, nicht am Brutgeschäft beteiligte Kormorane getötet werden.

(4) Für die Tötung nach Absatz 1 gelten § 1 Absatz 3 des Bundesjagdgesetzes in der Fassung der Bekanntmachung vom 29.
September 1976 (BGBl.
I S. 2849), das zuletzt durch Artikel 1 des Gesetzes vom 29.
Mai 2013 (BGBl.
I S. 1386) geändert worden ist, sowie die §§ 34, 36 und 37 des Jagdgesetzes für das Land Brandenburg vom 9.
Oktober 2003 (GVBl.
I S. 250), das zuletzt durch Artikel 22 des Gesetzes vom 13.
März 2012 (GVBl.
I Nr.
16 S. 9) geändert worden ist, entsprechend.

(5) Die Inbesitznahme von nach Absatz 1 getöteten Kormoranen ist gemäß § 45 Absatz 1 Nummer 1 Buchstabe a des Bundesnaturschutzgesetzes von den Besitzverboten des § 44 Absatz 2 Satz 1 Nummer 1 des Bundesnaturschutzgesetzes ausgenommen.
Werden getötete Tiere nicht auf dieser Grundlage in Besitz genommen, sind sie ordnungsgemäß zu entsorgen.
Die Vermarktungsverbote des § 44 Absatz 2 Satz 1 Nummer 2 des Bundesnaturschutzgesetzes bleiben unberührt.

(6) Abweichend von Absatz 5 Satz 2 sind nach Absatz 1 getötete Kormorane auf vorheriges Verlangen des Landesamtes für Umwelt unter Angabe der genauen Erlegungszeit (Datum, Uhrzeit) und des genauen Erlegungsortes (Gewässer, Gewässerabschnitt oder Teichwirtschaftsbetrieb) für Forschungszwecke zur Verfügung zu stellen.

§ 2  
**Brutkolonien und Schlafplätze**

(1) Abweichend von § 44 Absatz 1 Nummer 3 des Bundesnaturschutzgesetzes wird den Bewirtschafterinnen und Bewirtschaftern von Gewässern oder Anlagen im Sinne des § 1 Absatz 2 gestattet, die Neugründung von Brutkolonien des Kormorans im Bereich der von ihnen genutzten Gewässer oder Anlagen innerhalb der ersten zwei Jahre ihres Bestehens durch gezielte Störungen zu verhindern.
Zulässig sind auch das Entfernen von Nestern und die Tötung von Kormoranen nach den Maßgaben dieser Verordnung.
Die nach Satz 1 Berechtigten dürfen andere Personen mit der Durchführung von Maßnahmen nach Satz 1 beauftragen.
Mit Zustimmung des Grundstückseigentümers darf das Entstehen von Kolonieneugründungen auch außerhalb der in Satz 1 genannten Bereiche verhindert werden.

(2) Absatz 1 gilt nicht im Zeitraum vom 1.
April bis zum 15.
August eines jeden Jahres.

(3) Absatz 1 gilt entsprechend für das Verhindern von Schlafplatzneugründungen des Kormorans.

§ 3  
**Zur Tötung von Kormoranen berechtigte Personen**

Zur Tötung nach § 1 ist berechtigt, wer einen gültigen Jagdschein besitzt und

1.  in dem jeweiligen Bereich jagdausübungsberechtigt ist,
2.  von der in dem jeweiligen Bereich jagdausübungsberechtigten Person zum Abschuss ermächtigt wurde,
3.  das jeweilige Gewässer fischereiwirtschaftlich nutzt oder die jeweilige Anlage fischereilich bewirtschaftet oder
4.  von den nach Nummer 3 Berechtigten mit dem Abschuss an von ihnen bewirtschafteten Gewässern oder Anlagen beauftragt wurde.

Die Tötung von Kormoranen durch eine in Satz 1 Nummer 3 oder 4 genannte Person darf nur im Bereich von Grundstücksflächen erfolgen, die sich im Eigentum der nach Nummer 3 Berechtigten befinden oder von diesen gepachtet sind.
Die in dem jeweiligen Bereich jagdausübungsberechtigte Person ist über die vorgesehene Tötung von Kormoranen vorab zu informieren.

§ 4  
**Einschränkungen und Aussetzungen**

(1) § 1 Absatz 1 und § 2 gelten nicht

1.  für Naturschutzgebiete und Nationalparks sowie Gebiete, die als Naturschutzgebiet einstweilig sichergestellt sind oder gemäß § 11 des Brandenburgischen Naturschutzausführungsgesetzes einer Veränderungssperre zwecks Ausweisung als Naturschutzgebiet unterliegen, es sei denn, dass insoweit eine nach der jeweiligen Schutzgebietsverordnung oder dem jeweiligen Gesetz erforderliche flächenschutzrechtliche Befreiung gewährt worden ist,
2.  für Europäische Vogelschutzgebiete (§ 15 Absatz 1 des Brandenburgischen Naturschutzausführungsgesetzes).

(2) § 1 Absatz 1 gilt ferner nicht

1.  für befriedete Bezirke im Sinne von § 5 des Jagdgesetzes für das Land Brandenburg,
2.  für Brutkolonien einschließlich der umgebenden Flächen im Radius von 500 Metern gemessen von deren Randbereichen im Zeitraum vom 16.
    März bis zum 15.
    August eines jeden Jahres.

(3) Das Landesamt für Umwelt soll im Einvernehmen mit dem Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung die Tötung von Kormoranen nach § 1 Absatz 1 an bestimmten Gewässern, Gewässerstrecken oder Anlagen der Teichwirtschaft aussetzen, wenn

1.  eine weitere Zulassung der Tötung nicht mehr erforderlich ist oder
2.  dies
    1.  zum Schutz der Vögel in der Brut- und Aufzuchtzeit oder
    2.  zur Abwehr von Gefahren für die öffentliche Sicherheit und Ordnung erforderlich ist.

Kommt ein Einvernehmen nicht zustande, entscheidet das für Naturschutz zuständige Mitglied der Landesregierung über die Aussetzung der Tötung.

§ 5  
**Unberührtheit anderer Rechtsvorschriften**

(1) Unberührt von dieser Verordnung bleiben die übrigen Verbote des § 44 Absatz 1 Nummer 1 und 2 des Bundesnaturschutzgesetzes, die Bestimmungen über verbotene Fangmethoden, Verfahren und Geräte nach § 4 Absatz 1 der Bundesartenschutzverordnung vom 16.
Februar 2005 (BGBl.
I S. 258, 896), die zuletzt durch Artikel 10 des Gesetzes vom 21.
Januar 2013 (BGBl.
I S. 95) geändert worden ist, sowie § 19 des Brandenburgischen Naturschutzausführungsgesetzes.
Bei der Durchführung von Maßnahmen auf Grund dieser Verordnung ist die Beeinträchtigung anderer besonders geschützter Arten zu vermeiden.

(2) Die Durchführung von Maßnahmen auf Grund dieser Verordnung hat zu unterbleiben, wenn hierbei entgegen § 44 Absatz 1 Nummer 2 des Bundesnaturschutzgesetzes wild lebende Tiere der streng geschützten Arten oder der europäischen Vogelarten so erheblich gestört werden, dass sich der Erhaltungszustand der lokalen Population der jeweiligen Art dadurch verschlechtern kann.

§ 6  
**Berichts- und Beobachtungspflichten der Fachbehörde für Naturschutz und Landschaftspflege**

(1) Bis zum 31.
Januar eines jeden Jahres hat dem Landesamt für Umwelt Bericht zu erstatten,

1.  wer von der Zulassung nach § 2 Gebrauch gemacht hat über die Anzahl der unterbundenen Kolonieneugründungen unter Angabe von Art und Umfang der angewandten Maßnahmen, der Tage (Datum, Uhrzeit), an denen diese angewandt wurden, sowie des genauen Ortes (Gewässer, Gewässerabschnitt oder Teichwirtschaftsbetrieb),
2.  wer von der Zulassung oder Gestattung nach § 1 Absatz 1 Gebrauch gemacht hat über
    1.  die Anzahl der getöteten Kormorane,
    2.  das jeweilige Datum unter Angabe von genauer Erlegungszeit und genauem Erlegungsort (Gewässer, Gewässerabschnitt oder Teichwirtschaftsbetrieb), an dem die Kormorane getötet wurden,
    3.  die Ringnummer bei beringten Kormoranen.

Abweichend von Satz 1 ist zur Berichterstattung verpflichtet, wer andere Personen gemäß § 2 Absatz 1 Satz 3 mit der Durchführung von Maßnahmen nach § 2 beauftragt oder gemäß § 3 Satz 1 Nummer 2 zur Tötung ermächtigt sowie gemäß § 3 Satz 1 Nummer 4 mit der Tötung beauftragt.

(2) Das Landesamt für Umwelt informiert die unteren Naturschutzbehörden bis zum 1.
März eines jeden Jahres, soweit in deren jeweiligen Zuständigkeitsbereich im Vorjahreszeitraum Maßnahmen auf Grund dieser Verordnung durchgeführt wurden.
Es beobachtet den Bestand des Kormorans in Brandenburg und berichtet der obersten Naturschutzbehörde bis zum 30.
Juni eines jeden Jahres über die aktuelle Bestandsentwicklung.

§ 7  
**Inkrafttreten**

Diese Verordnung tritt am 1.
Oktober 2013 in Kraft.

Potsdam, den 27.
September 2013

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack