## Verordnung zur europäischen Verwaltungszusammenarbeit im Rahmen der Richtlinie 2006/123/EG und in anderen Fällen (DLRL-Verwaltungszusammenarbeitsverordnung - DLRLVerwV)

Auf Grund des § 9 Absatz 1 Satz 1 und 2 des Gesetzes über den Einheitlichen Ansprechpartner für das Land Brandenburg vom 7.
Juli 2009 (GVBl.
I S. 262) verordnet die Landesregierung:

### § 1  
Binnenmarktinformationssystem

(1) Das Binnenmarkt-Informationssystem (IMI) ist gemäß der Entscheidung 2009/739/EG der Kommission vom 2.
Oktober 2009 zur Festlegung der praktischen Regelungen für den Informationsaustausch auf elektronischem Wege zwischen den Mitgliedstaaten gemäß Kapitel VI der Richtlinie 2006/123/EG des Europäischen Parlaments und des Rates über Dienstleistungen im Binnenmarkt (ABl.
L 263 vom 7.10.2009, S. 32) das gemeinsame elektronische System der Europäischen Kommission und der Mitgliedstaaten zur Erfüllung der in Kapitel VI der Richtlinie 2006/123/EG des Europäischen Parlaments und des Rates vom 12.
Dezember 2006 über Dienstleistungen im Binnenmarkt (ABl.
L 376 vom 27.12.2006, S. 36) festgelegten Bestimmungen zur Verwaltungszusammenarbeit, welche Folgendes vorsehen:

1.  Ersuchen um Informationen und um Durchführung von Überprüfungen, Kontrollen und Untersuchungen sowie deren Beantwortung gemäß Kapitel VI der Richtlinie 2006/123/EG,
2.  Vorwarnungen gemäß Artikel 29 Absatz 3 und Artikel 32 Absatz 1 der Richtlinie 2006/123/EG,
3.  Ersuchen und Mitteilungen im Einzelfall gemäß dem Verfahren nach Artikel 35 Absatz 2, 3 und 6 der Richtlinie 2006/123/EG.

(2) Das Binnenmarkt-Informationssystem (IMI) kann auch für die Amtshilfe nach den §§ 4 bis 8 des Verwaltungsverfahrensgesetzes in Verbindung mit § 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg genutzt werden, wenn es sich um Amtshandlungen im Sinne von Absatz 1 Nummer 1 handelt.
Dies gilt auch für Amtshandlungen in Bezug auf inländische Dienstleistungserbringer und Dienstleistungserbringerinnen.
§ 4 findet in diesen Fällen keine Anwendung.

(3) Absatz 1 ist entsprechend anzuwenden auf die Verwaltungszusammenarbeit mit Drittstaaten, denen die Europäische Union oder die Bundesrepublik Deutschland vertraglich einen entsprechenden Rechtsanspruch eingeräumt hat.

### § 2  
Zuständige Behörden

(1) Zuständige Behörde im Sinne dieser Verordnung ist jede Stelle, die öffentliche Aufgaben wahrnimmt, die in den Anwendungsbereich der Richtlinie 2006/123/EG fallen.

(2) Die europäische Verwaltungszusammenarbeit erfolgt unmittelbar zwischen den zuständigen Behörden, sofern nicht die §§ 3 bis 5 Abweichendes regeln.
Aufsichtsrechtliche Regelungen bleiben unberührt.

### § 3  
Aufgaben der IMI-Koordination

(1) Die Aufgaben des IMI-Koordinators ergeben sich aus Artikel 6 Absatz 1 in Verbindung mit Absatz 2 der Verordnung (EU) Nr.
1024/2012 des Europäischen Parlaments und des Rates vom 25. Oktober 2012 über die Verwaltungszusammenarbeit mit Hilfe des Binnenmarkt-Informationssystems und zur Aufhebung der Entscheidung 2008/49/EG der Kommission („IMI-Verordnung“) (ABl.
L 316 vom 14.11.2012, S.
1), die zuletzt durch die Richtlinie 2014/67/EU (ABl.
L 159 vom 28.5.2014, S.
11) geändert worden ist, und sind insbesondere:

1.  die Registrierung und bei Selbstregistrierung die Identitätsbestätigung der zuständigen Behörden;
2.  die Funktion als Hauptanlaufstelle für IMI-Akteure des Landes in Fragen, die das Binnenmarkt-Informationssystem (IMI) betreffen, einschließlich der Bereitstellung oder Vermittlung von Informationen zu Aspekten, die den Schutz personenbezogener Daten im Einklang mit der IMI-Verordnung betreffen;
3.  die Funktion als Ansprechpartner der Kommission oder des nationalen IMI-Koordinators in Fragen, die das Binnenmarkt-Informationssystem (IMI) betreffen, einschließlich der Bereitstellung von Informationen zu Aspekten, die den Schutz personenbezogener Daten im Einklang mit der IMI-Verordnung betreffen;
4.  das Bereitstellen von Wissen, Schulungen und Unterstützung für IMI-Akteure des Landes;
5.  die Gewährleistung des effizienten Funktionierens des Binnenmarkt-Informationssystems (IMI), soweit es dem Einfluss des IMI-Koordinators unterliegt, einschließlich der rechtzeitigen und angemessenen Beantwortung von Ersuchen um Verwaltungszusammenarbeit durch IMI-Akteure des Landes Brandenburg.

IMI-Akteure des Landes Brandenburg sind alle Stellen im Land Brandenburg, die als IMI-Akteure nicht einem anderen Land der Bundesrepublik Deutschland, dem Bund oder der Europäischen Union oder einem anderen Mitgliedstaat zuzurechnen sind.

(2) In weiteren Fällen der europäischen Verwaltungszusammenarbeit, die in anderen Rechtsakten der Europäischen Union als der Richtlinie 2006/123/EG geregelt sind, und diese ohne Wahlmöglichkeit vorsehen, arbeiten die in dieser Verordnung genannten Stellen des Landes Brandenburg in entsprechender Anwendung der Regelungen dieser Verordnung zusammen, soweit nationale Vorschriften keine abweichenden Regelungen enthalten.
Zuständige Behörden sind in diesen Fällen die Stellen, die öffentliche Aufgaben wahrnehmen, die in den Anwendungsbereich des entsprechenden europäischen Rechtsaktes fallen, einer im Rechtsakt enthaltenen Beschreibung der zuständigen Behörde oder Stelle entsprechen oder, wenn zuständige Behörden durch nationale Rechtsnormen im selben Sachzusammenhang als solche bestimmt sind, nur diese.

### § 4   
Verbindungsstelle

(1) Die Aufgaben der Verbindungsstelle nach Artikel 28 Absatz 2 der Richtlinie 2006/123/EG nimmt die jeweils fachlich zuständige oberste Landesbehörde für ihren Geschäftsbereich sowie für die sonstigen ihrer Aufsicht unterstehenden Stellen wahr.
Die Mitglieder der Landesregierung werden jeweils ermächtigt, durch Rechtsverordnung die Aufgaben auf eine andere Behörde in ihrem Geschäftsbereich oder eine ihrer Aufsicht unterstehende Stelle zu übertragen, soweit dieser durch gesonderte Rechtsvorschrift Aufsichtsbefugnisse übertragen sind.

(2) Die Verbindungsstelle wird insbesondere tätig:

1.  zur Unterstützung beim Ermitteln der zuständigen Behörde bei Anfragen aus den Mitgliedstaaten,
2.  zur Unterstützung beim Ermitteln von zuständigen Behörden in den Mitgliedstaaten,
3.  bei der Lösung von Schwierigkeiten nach Artikel 28 Absatz 5 der Richtlinie 2006/123/EG,
4.  bei der Unterrichtung der Kommission nach Artikel 28 Absatz 8 der Richtlinie 2006/123/EG,
5.  bei der Unterstützung der für die Erteilung einer Genehmigung zuständigen Behörde nach Artikel 10 Absatz 3 Satz 2 der Richtlinie 2006/123/EG.

(3) Die Verbindungsstelle nimmt ebenfalls die Funktion der Berufungsstelle nach Artikel 9 Absatz 6 der Entscheidung 2008/49/EG wahr.

### § 5  
Vorwarnungsmechanismus und Vorwarnungskoordination

(1) Im Rahmen des Vorwarnungsmechanismus nach Artikel 32 der Richtlinie 2006/123/EG obliegt den zuständigen Behörden:

1.  die Vorbereitung und Initiierung von Unterrichtungen nach Artikel 29 Absatz 3 und Artikel 32 Absatz 1 der Richtlinie 2006/123/EG (Vorwarnungen) und Weiterleitung an die mit der Vorwarnungskoordination betraute Stelle,
2.  der Versand zusätzlicher Informationen sowie deren Ersuchen nach Artikel 3 Absatz 1 Buchstabe b der Entscheidung 2009/739/EG,
3.  die Berichtigung von in Vorwarnungen enthaltenen Informationen nach Artikel 3 Absatz 1 Buchstabe d der Entscheidung 2009/739/EG,
4.  die Vorbereitung von Vorschlägen zur Schließung von Vorwarnungen und der Erhebung von Einwänden gegen Vorschläge zur Schließung von Vorwarnungen nach Artikel 3 Absatz 1 Buchstabe e und f der Entscheidung 2009/739/EG und Weiterleitung an die mit der Vorwarnungskoordination betraute Stelle,
5.  die Unterrichtung der Betroffenen nach § 8d Absatz 2 des Verwaltungsverfahrensgesetzes in Verbindung mit § 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg.

(2) Die Aufgaben der Vorwarnungskoordination nach Artikel 3 Absatz 2 der Entscheidung 2009/739/EG nimmt die jeweils fachlich zuständige oberste Landesbehörde für ihren Geschäftsbereich sowie für die sonstigen ihrer Aufsicht unterstehenden Stellen wahr.
Die Mitglieder der Landesregierung werden jeweils ermächtigt, durch Rechtsverordnung die Aufgaben auf eine andere Behörde in ihrem Geschäftsbereich oder eine ihrer Aufsicht unterstehende Stelle zu übertragen, soweit dieser durch gesonderte Rechtsvorschrift Aufsichtsbefugnisse übertragen sind.
Der Vorwarnungskoordination obliegt insbesondere:

1.  die Prüfung der rechtlichen Voraussetzungen für das Versenden, Schließen oder Widerrufen von Vorwarnungen nach Artikel 3 Absatz 1 Buchstabe g und Absatz 2 der Entscheidung 2009/739/EG,
2.  das Versenden und Empfangen von Vorwarnungen und einschlägigen Informationen sowie den Widerruf von Vorwarnungen an und von anderen Mitgliedstaaten nach Artikel 3 Absatz 2 der Entscheidung 2009/739/EG.

(3) Für den Empfang eingehender Vorwarnungen aus anderen Mitgliedstaaten übernimmt der IMI-Koordinator für das Land Brandenburg die Aufgabe der elektronischen Poststelle.
Er leitet eingehende Vorwarnungen unverzüglich an die jeweils mit der Vorwarnungskoordination betraute, zuständige Stelle weiter.
Diese unterrichtet unverzüglich die zuständigen Behörden oder sonstigen ihrer Aufsicht unterstehenden Stellen ihres Geschäftsbereichs.
Sofern in der Vorwarnung eines Mitgliedstaates bereits zuständige Behörden aufgeführt werden, wird mit der Weiterleitung an die jeweils mit der Vorwarnungskoordination betraute, zuständige Stelle automatisch die Vorwarnung an diese Behörden weitergeleitet.

(4) Zuständige Behörde im Sinne des Artikels 56a der Richtlinie 2005/36/EG des Europäischen Parlaments und des Rates vom 7.
September 2005 über die Anerkennung von Berufsqualifikationen (ABl.
L 255 vom 30.9.2005, S.
22), die zuletzt durch die Richtlinie 2013/55/EU (ABl.
L 354 vom 28.12.2013, S.
132) geändert worden ist, ist auch jedes Gericht, bei dem ein Verfahren über die Untersagung oder Beschränkung der Berufsausübung anhängig ist, jede Vollstreckungsbehörde, die eine gerichtliche Entscheidung über ein Berufsverbot zu vollstrecken hat und die oberste Landesbehörde, die für die betreffende Gerichtsbarkeit zuständig ist, und die als Vorwarnungskoordinator tätig wird.
Auf der Grundlage von Artikel 56a der Richtlinie 2005/36/EG eingehende Vorwarnungen werden von der für Wirtschaft zuständigen obersten Landesbehörde in ihrer Funktion als IMI-Koordinator für das Land Brandenburg an die jeweilige oberste Landesbehörde in ihrer Funktion als Vorwarnungskoordinator für ihren Geschäftsbereich weitergeleitet, sofern die für den Empfang der betreffenden Vorwarnung zuständige Behörde diese nicht bereits direkt über das Binnenmarkt-Informationssystem (IMI) erhalten hat.
Die obersten Landesbehörden, die für Aufgaben der Anerkennung von Berufsqualifikationen zuständig sind, melden der für Wirtschaft zuständigen obersten Landesbehörde in ihrer Funktion als IMI-Koordinator für das Land Brandenburg die zuständigen Behörden, Gerichte und Einrichtungen ihres Geschäftsbereiches und den zuständigen Vorwarnungskoordinator für eingehende und ausgehende Vorwarnungen.

### § 6  
Verfahren bei Ausnahmen im Einzelfall

(1) Den zuständigen Behörden obliegen nach Artikel 35 der Richtlinie 2006/123/EG folgende Aufgaben:

1.  die Versendung eines Ersuchens an den Niederlassungsmitgliedstaat gemäß Artikel 35 Absatz 2 der Richtlinie 2006/123/EG,
2.  die Feststellung, Überprüfung und Mitteilung nach Artikel 35 Absatz 2 der Richtlinie 2006/123/EG und
3.  die Versendung einer Mitteilung an die Kommission und den Niederlassungsmitgliedstaat gemäß Artikel 35 Absatz 3 und 6 der Richtlinie 2006/123/EG.

(2) Die zuständige Behörde unterrichtet die Verbindungsstelle über Ersuchen und Mitteilungen nach Absatz 1.

### § 7  
Datenschutz

Die Verarbeitung personenbezogener Daten durch die jeweiligen Behörden ist zulässig, soweit dies zur Erfüllung der Aufgaben, die sich aus Artikel 10 Absatz 3 Satz 2 und Kapitel VI der Richtlinie 2006/123/EG sowie aus dieser Verordnung ergeben, erforderlich ist.
Es gelten die Vorschriften des Brandenburgischen Datenschutzgesetzes in der Fassung der Bekanntmachung vom 15.
Mai 2008 (GVBl.
I S. 114), das zuletzt durch Artikel 1 des Gesetzes vom 25.
Mai 2010 (GVBl.
I Nr. 21) geändert worden ist, in der jeweils geltenden Fassung.

### § 8  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 9.
Februar 2011

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Matthias Platzeck

Der Minister für Wirtschaft  
und Europaangelegenheiten

Ralf Christoffers

[\[1\]](#_ftnref1)) Diese Verordnung dient der Umsetzung der Richtlinie 2006/123/EG des Europäischen Parlaments und des Rates vom 12.
Dezember 2006 über Dienstleistungen im Binnenmarkt (ABl.
L 376 vom 27.12.2006, S.
36).