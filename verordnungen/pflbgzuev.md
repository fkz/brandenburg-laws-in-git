## Verordnung zur Übertragung von Ermächtigungen nach dem Pflegeberufegesetz (Pflegeberufezuständigkeitsübertragungsverordnung - PflBGZÜV)

#### § 1 Übertragung der Ermächtigung in § 36 des Pflegeberufegesetzes

Die in § 36 Absatz 5 des Pflegeberufegesetzes enthaltene Ermächtigung zum Erlass der Rechtsverordnung wird auf das für Soziales und Gesundheit zuständige Mitglied der Landesregierung übertragen.

#### § 2 Übertragung weiterer Ermächtigungen

Die in § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten und § 6 Absatz 2 des Landesorganisationsgesetzes in Verbindung mit § 26 Absatz 6 Satz 1 und 2 und § 49 des Pflegeberufegesetzes enthaltenen Ermächtigungen zum Erlass von Rechtsverordnungen wird auf das für Soziales und Gesundheit zuständige Mitglied der Landesregierung übertragen.