## Verordnung zur Bemessung der Förderbeträge nach dem Brandenburgischen  Musik- und Kunstschulgesetz (Musik- und Kunstschulförderverordnung - MKSchulFV)

Auf Grund des § 6 Absatz 3 des Brandenburgischen Musik- und Kunstschulgesetzes vom 11. Februar 2014 (GVBl.
I Nr. 5) verordnet die Ministerin für Wissenschaft, Forschung und Kultur:

### § 1  
Aufteilung des Zuschusses

Der Zuschuss gemäß § 6 Absatz 2 Satz 1 des Brandenburgischen Musik- und Kunstschulgesetzes wird wie folgt aufgeteilt:

1.  Musikschulen: 4 727 000 Euro,
    
2.  Kunstschulen einschließlich Fachbereiche gemäß § 3 Absatz 4 Nummer 1 des Brandenburgischen Musik- und Kunstschulgesetzes an Musikschulen: 400 000 Euro.
    

### § 2  
Verteilungsquotienten

(1) Die Verteilungsquotienten gemäß § 6 Absatz 2 Satz 2 und 3 des Brandenburgischen Musik- und Kunstschulgesetzes werden vorbehaltlich des Absatzes 2 wie folgt festgelegt (Unterrichtsstunden : Schülerzahlen):

1.  Musikschulen: 2 : 1,
    
2.  Kunstschulen einschließlich Fachbereiche gemäß § 3 Absatz 4 Nummer 1 des Brandenburgischen Musik- und Kunstschulgesetzes an Musikschulen: 1 : 3.
    

(2) Abweichend von Absatz 1 Nummer 1 beträgt der Verteilungsquotient im Förderjahr 2014 für die Musikschulen 3 : 1.

### § 3  
Ermittlung der Bemessungskennzahlen

(1) Eine Unterrichtsstunde gemäß § 6 Absatz 2 Satz 2 und 3 des Brandenburgischen Musik- und Kunstschulgesetzes ist eine von der Leitung der Musikschule oder Kunstschule angeordnete Unterrichtseinheit von 45 Minuten, sofern die Unterrichtseinheit innerhalb der Fachbereiche gemäß § 3 Absatz 2 Nummer 2 und Absatz 4 Nummer 1 des Brandenburgischen Musik- und Kunstschulgesetzes Fächern oder Kursen zugeordnet werden kann, die durch die Musikschule oder Kunstschule auf mindestens ein Schuljahr angelegt sind (regulärer Unterrichtsbetrieb).
Als Schuljahr gilt ein Unterrichtszeitraum vom ersten Unterrichtstag an allgemein bildenden Schulen nach dem Ende der Sommerferien im Land Brandenburg bis zum letzten Ferientag der Sommerferien des Folgejahres.
Dem Schuljahr werden 38 Unterrichtswochen zugrunde gelegt.
Gegenstand der Unterrichtseinheit ist die Vermittlung von Fachwissen, Fähigkeiten und Fertigkeiten in den gemäß § 3 Absatz 2 Nummer 2 und Absatz 4 Nummer 1 des Brandenburgischen Musik- und Kunstschulgesetzes genannten Fachbereichen und Ausbildungsstufen durch mindestens eine pädagogische Lehrkraft gegenüber einer Schülerin oder einem Schüler oder mehreren Schülerinnen und Schülern.

(2) Zur Ermittlung der Anzahl der vertraglich gebundenen Schülerinnen und Schüler gemäß § 6 Absatz 2 Satz 2 und 3 des Brandenburgischen Musik- und Kunstschulgesetzes erfolgt jeweils eine Stichtagszählung zum 1.
April und 1.
Oktober des dem Förderjahr vorausgegangenen Kalenderjahres.
Dabei ist der hälftige Anteil aus der Summe der zu den jeweiligen Stichtagen gemäß Satz 1 ermittelten Anzahl der vertraglich gebundenen Schülerinnen und Schüler, für die zum Stichtag ein auf eine Laufzeit von mindestens einem halben Schuljahr geschlossener Vertrag bestand, der Bemessung zugrunde zu legen.
Ein Beginn der Vertragslaufzeit spätestens einen Monat nach Beginn des Schulhalbjahres oder ein Ende der Vertragslaufzeit frühestens einen Monat vor Ablauf des Schulhalbjahres ist zulässig.
Als Ende des ersten Schulhalbjahres gilt der letzte Tag der Winterferien an allgemein bildenden Schulen im Land Brandenburg.
Als Beginn des zweiten Schulhalbjahres gilt der erste Unterrichtstag an allgemein bildenden Schulen nach dem Ende der Winterferien im Land Brandenburg.
Schülerinnen und Schüler, die mehrere Unterrichtsverträge mit der Musikschule oder Kunstschule geschlossen haben, dürfen zu den jeweiligen Stichtagen gemäß Satz 1 nur einmal gezählt werden.

### § 4  
Bewilligungsverfahren

(1) Anträge auf Bewilligung eines Landeszuschusses sind durch die Musikschulen und Kunstschulen mit den erforderlichen Nachweisen vorbehaltlich des Absatzes 2 bis spätestens zum 1.
März des Förderjahres bei der für Kultur zuständigen obersten Landesbehörde schriftlich einzureichen.
Zum Nachweis der Wahrung der Ausschlussfrist gilt der Posteingangsstempel der Bewilligungsbehörde.
Die Bewilligungsbehörde kann die Frist gemäß Satz 1 aus besonderen Gründen um bis zu drei Monate verlängern.
Die Verlängerung der Frist ist durch die für Kultur zuständige oberste Landesbehörde im Amtsblatt für Brandenburg öffentlich bekannt zu machen.
Der Inhalt der Bekanntmachung gemäß Satz 3 wird zusätzlich auf der Internetseite der für Kultur zuständigen obersten Landesbehörde veröffentlicht.

(2) Abweichend von Absatz 1 Satz 1 sind Bewilligungsanträge der Musikschulen bezogen auf die Fachbereiche gemäß § 3 Absatz 2 Nummer 2 des Brandenburgischen Musik- und Kunstschulgesetzes für das Förderjahr 2014 spätestens vier Wochen nach Verkündung dieser Verordnung bei der für Kultur zuständigen obersten Landesbehörde einzureichen.
Abweichend von Absatz 1 Satz 1 sind Bewilligungsanträge der Kunstschulen und verbundenen Musik- und Kunstschulen bezogen auf die Fachbereiche gemäß § 3 Absatz 4 Nummer 1 des Brandenburgischen Musik- und Kunstschulgesetzes für das Förderjahr 2014 bis zum 29.
August 2014 bei der für Kultur zuständigen obersten Landesbehörde einzureichen.

### § 5  
Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2014 in Kraft.

Potsdam, den 28.
Juli 2014

Die Ministerin für Wissenschaft, Forschung und Kultur

Prof.
Dr.-Ing.
Dr.
Sabine Kunst