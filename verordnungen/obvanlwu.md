## Ordnungsbehördliche Verordnung über das Anlegen von Wundstreifen auf landwirtschaftlich genutzten Flächen am Waldrand

Auf Grund des § 25 in Verbindung mit § 30 Absatz 1 des Ordnungsbehördengesetzes in der Fassung der Bekanntmachung vom 21. August 1996 (GVBl. I S. 266), von denen § 25 durch Artikel 15 des Gesetzes vom 25. Januar 2016 (GVBl. I Nr. 5) geändert worden ist, in Verbindung mit § 31 Nummer 1 und § 35 des Waldgesetzes des Landes Brandenburg vom 20. April 2004 (GVBl. I S. 137) verordnet der Minister für Landwirtschaft, Umwelt und Klimaschutz im Benehmen mit dem Minister des Innern und für Kommunales:

#### § 1 Geltungsbereich

Diese Verordnung gilt für das gesamte Gebiet des Landes Brandenburg.

#### § 2 Gebot zur Anlage von Wundstreifen

(1) Bei der Ernte von leicht entzündlichen Ernteerzeugnissen (zum Beispiel Mähdruschfrüchten oder Pflanzen zur Gewinnung von Energie) ist während der Waldbrandgefahrenstufen 4 und 5 auf Ackerflächen mit einem Abstand von weniger als 50 Metern zum Waldrand unmittelbar nach Anschnitt des Ernteerzeugnisses auf der dem Wald zugekehrten Erntefläche ein sechs Meter breiter Wundstreifen anzulegen.
Wundstreifen sind von brennbarem Material und humosem Oberboden freizuhaltende Flächen.

(2) Das Gebot des Absatzes 1 gilt entsprechend bei der Ablagerung von Stroh und anderen brennbaren landwirtschaftlichen Erzeugnissen.

(3) Die Gebote der Absätze 1 und 2 gelten nach Festlegung der unteren Forstbehörde nicht, wenn die zwischen dem Erntefeld oder der Lagerfläche und dem Wald liegende Fläche wegen ihrer Beschaffenheit ein Übergreifen eines Feldfeuers auf den Wald verhindert.

(4) Zur Anlage des Wundstreifens gemäß Absatz 1 und 2 ist bei eigengenutzten Flächen der Eigentümer und bei verpachteten Flächen der Pächter der Landwirtschaftsfläche verpflichtet.

#### § 3 Ordnungswidrigkeiten

(1) Ordnungswidrig handelt, wer vorsätzlich oder fahrlässig entgegen § 2 Absatz 1 und 2 das Anlegen eines Wundstreifens unterlässt.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu 1 000 Euro geahndet werden.

(3) Zuständige Verwaltungsbehörde im Sinne des § 36 Absatz 1 Nummer 1 des Gesetzes über Ordnungswidrigkeiten ist die untere Forstbehörde.

#### § 4 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt eine Woche nach dem Tag der Verkündung in Kraft.
Sie tritt zehn Jahre nach dem Inkrafttreten außer Kraft.

Potsdam, den 23.
Februar 2021

Der Minister für Landwirtschaft,  
Umwelt und Klimaschutz

Axel Vogel