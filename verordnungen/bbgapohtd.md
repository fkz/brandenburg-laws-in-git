## Verordnung über die Ausbildung und Prüfung für die Laufbahn des höheren technischen Verwaltungsdienstes im Land Brandenburg (Brandenburgische Ausbildungs- und Prüfungsordnung höherer technischer Dienst - BbgAPOhtD)

Auf Grund des § 26 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S. 26), der durch Artikel 1 des Gesetzes vom 5.
Dezember 2013 (GVBl.
I Nr. 36) geändert worden ist, verordnet der Minister des Innern und für Kommunales im Einvernehmen mit dem Minister der Finanzen, der Minister der Finanzen im Einvernehmen mit dem Minister des Innern und für Kommunales und die Ministerin für Infrastruktur und Landesplanung im Einvernehmen mit dem Minister des Innern und für Kommunales und dem Minister der Finanzen:

**Inhaltsübersicht**

Kapitel 1  
Vorbereitungsdienst und Laufbahnprüfung
---------------------------------------------------

### Abschnitt 1  
Technisches Referendariat

[§ 1 Zweck, Ziel und Fachrichtungen des technischen Referendariats](#1)

[§ 2 Einstellungsvoraussetzungen](#2)

[§ 3 Einstellungsverfahren](#3)

[§ 4 Ernennung, Bezüge](#4)

[§ 5 Ausbildungsbehörde und Ausbildungsstellen](#5)

[§ 6 Dauer und Gliederung des technischen Referendariats](#6)

[§ 7 Inhalt und Gestaltung der Ausbildung](#7)

[§ 8 Begleitung und Überwachung der Ausbildung](#8)

[§ 9 Beurteilung während der Ausbildung](#9)

[§ 10 Urlaub, Berücksichtigung der Belange behinderter Referendarinnen und Referendare](#10)

[§ 11 Entlassung aus dem technischen Referendariat](#11)

### Abschnitt 2  
Staatsexamen, Prüfungsordnung

[§ 12 Zweck des Staatsexamens](#12)

[§ 13 Abnahme durch das Oberprüfungsamt](#13)

[§ 14 Zulassung zum Staatsexamen](#14)

[§ 15 Gliederung und Inhalt](#15)

[§ 16 Häusliche Prüfungsarbeit](#16)

[§ 17 Schriftliche Arbeiten unter Aufsicht](#17)

[§ 18 Mündliche Prüfung](#18)

[§ 19 Unterbrechung, Rücktritt](#19)

[§ 20 Bewertung der Prüfungsleistungen im Einzelnen](#20)

[§ 21 Abschließende Bewertung, Gesamturteil](#21)

[§ 22 Prüfungszeugnis](#22)

[§ 23 Wiederholung](#23)

[§ 24 Täuschung, Verstoß gegen die Prüfungsordnung](#24)

[§ 25 Prüfungsakte](#25)

[§ 26 Ausführungsbestimmungen](#26)

### Abschnitt 3  
Sondervorschriften der Fachrichtungen

[§ 27 Sondervorschriften der Fachrichtung Architektur](#27)

[§ 28 Sondervorschriften der Fachrichtung Geodäsie und Geoinformation](#28)

[§ 29 Sondervorschriften der Fachrichtung Maschinen- und Elektrotechnik in der Verwaltung](#29)

[§ 30 Sondervorschriften der Fachrichtung Städtebau](#30)

[§ 31 Sondervorschriften der Fachrichtung Straßenwesen](#31)

Kapitel 2  
Aufstieg in die Laufbahn des höheren technischen Verwaltungsdienstes  
in der Fachrichtung Geodäsie und Geoinformation
----------------------------------------------------------------------------------------------------------------------------------

[§ 32 Geltungsbereich](#32)

[§ 33 Auswahlverfahren, Zulassung](#33)

[§ 34 Einführung in die Aufgaben der neuen Laufbahn (Qualifizierung), Beurteilungen](#34)

[§ 35 Prüfungsausschuss, Prüfungskommission](#35)

[§ 36 Aufstiegsprüfung](#36)

[§ 37 Unterbrechung, Rücktritt](#37)

[§ 38 Bewertung, Gesamturteil](#38)

[§ 39 Wiederholung](#39)

[§ 40 Prüfungsakte](#40)

Kapitel 3  
Übergangs- und Schlussvorschriften
----------------------------------------------

[§ 41 Übergangsbestimmungen](#41)

[§ 42 Inkrafttreten, Außerkrafttreten](#42)

[Anlage 1 Ausbildungsnachweis  
Anlage 2 Übersicht über das technische Referendariat  
Anlage 3 Beurteilung  
Anlage 4 Antrag auf Zulassung zum Staatsexamen  
Anlage 5 Prüfungszeugnis (Muster)  
Anlage 6a Gliederung der Ausbildung in der Fachrichtung Architektur; Ausbildungsplan  
Anlage 6b Prüfungsfächer und Prüfungszeiten in der Fachrichtung Architektur  
Anlage 6c Prüfstoffverzeichnis in der Fachrichtung Architektur  
Anlage 7a Gliederung der Ausbildung in der Fachrichtung Geodäsie und Geoinformation; Ausbildungsplan  
Anlage 7b Prüfungsfächer und Prüfungszeiten in der Fachrichtung Geodäsie und Geoinformation  
Anlage 7c Prüfstoffverzeichnis in der Fachrichtung Geodäsie und Geoinformation  
Anlage 8a Gliederung der Ausbildung in der Fachrichtung Maschinen- und Elektrotechnik in der Verwaltung; Ausbildungsplan  
Anlage 8b Prüfungsfächer und Prüfungszeiten in der Fachrichtung Maschinen- und Elektrotechnik in der Verwaltung  
Anlage 8c Prüfstoffverzeichnis in der Fachrichtung Maschinen- und Elektrotechnik in der Verwaltung  
Anlage 9a Gliederung der Ausbildung in der Fachrichtung Städtebau; Ausbildungsplan  
Anlage 9b Prüfungsfächer und Prüfungszeiten in der Fachrichtung Städtebau  
Anlage 9c Prüfstoffverzeichnis in der Fachrichtung Städtebau  
Anlage 10a Gliederung der Ausbildung in der Fachrichtung Straßenwesen; Ausbildungsplan  
Anlage 10b Prüfungsfächer und Prüfungszeiten in der Fachrichtung Straßenwesen  
Anlage 10c Prüfstoffverzeichnis in der Fachrichtung Straßenwesen](#Anlagen)

Kapitel 1  
Vorbereitungsdienst und Laufbahnprüfung
---------------------------------------------------

### Abschnitt 1  
Technisches Referendariat

#### § 1 Zweck, Ziel und Fachrichtungen des technischen Referendariats

(1) Die Vorschriften dieses Kapitels regeln die Ausbildung und Prüfung des höheren technischen Verwaltungsdienstes im Land Brandenburg (technisches Referendariat).
Das technische Referendariat ist der Vorbereitungsdienst dieser Laufbahn.

(2) Zweck und Ziel des technischen Referendariats ist es, Hochschulabsolventinnen und Hochschulabsolventen wissenschaftlich-technischer Studiengänge als Führungskräfte zu qualifizieren und sie auf Leitungsfunktionen praxisgerecht vorzubereiten.

(3) Die Ausbildung soll sich darauf erstrecken, in praktischer Anwendung und aufbauend auf dem auf der Hochschule erworbenen technischen Fachwissen, umfassende Kenntnisse vor allem im Management und für Führungsaufgaben sowie im öffentlichen und privaten Recht zu vermitteln.
Dabei sind Verantwortungsbereitschaft und Initiative zu wecken und zu fördern.
Staatspolitische, wirtschaftliche, ökologische, kulturelle und soziale Belange sind zu berücksichtigen.

(4) Das technische Referendariat umfasst folgende Fachrichtungen:

1.  Architektur,
2.  Geodäsie und Geoinformation,
3.  Maschinen- und Elektrotechnik in der Verwaltung,
4.  Städtebau und
5.  Straßenwesen.

(5) Das technische Referendariat schließt mit dem Staatsexamen ab, das gleichzeitig Laufbahnprüfung für den höheren technischen Verwaltungsdienst in der jeweiligen Fachrichtung ist.

#### § 2 Einstellungsvoraussetzungen

Für das technische Referendariat können Bewerberinnen und Bewerber eingestellt werden, die

1.  die gesetzlichen Voraussetzungen für die Berufung in das Beamtenverhältnis erfüllen und
2.  als Bildungsvoraussetzung den erfolgreichen Abschluss
    
    1.  eines Master-Studiengangs an einer Hochschule mit einer Regelstudienzeit von zehn Fachsemestern (einschließlich Praxis- und Prüfungssemester sowie Masterarbeit), die inhaltlich-fachlich aufeinander aufbauen, oder
    2.  eines Diplom-Studienganges an einer Universität oder einer gleichgestellten Hochschule mit einer Regelstudienzeit von acht Fachsemestern (ohne Zeiten für Praxis- und Prüfungssemester sowie Diplomarbeit)
    
    gemäß den Sondervorschriften der Fachrichtungen einschließlich des in den Sondervorschriften festgelegten Wissensspektrums nachweisen.
    Dieser Nachweis ist durch persönlich qualifizierende Prüfungen anhand eines Abschlusszeugnisses sowie ein Diploma Supplement zu erbringen.
    Die mit diesem Abschluss vorauszusetzende Fähigkeit, selbständig Fachwissen zu beherrschen und wissenschaftsmethodisch anzuwenden, ist darüber hinaus durch eine das Studium abschließende, qualifizierende Master- oder Diplomarbeit zu belegen.
    

§ 4 Absatz 1 Satz 2 bleibt unberührt.

#### § 3 Einstellungsverfahren

(1) Die Bewerbung auf Einstellung für das technische Referendariat ist bei der Einstellungsbehörde einzureichen.

(2) Der Bewerbung sind beizufügen:

1.  Lebenslauf,
2.  Zeugnis über den Nachweis der Hochschulreife,
3.  Zeugnisse über die Hochschulprüfungen (Bachelor- und Masterprüfung oder Diplom-Vorprüfung und Diplom-Hauptprüfung) in einem wissenschaftlichen Studiengang, der die Kriterien gemäß § 2 Nummer 2 erfüllt, sowie gegebenenfalls über Zusatz- oder andere Prüfungen,
4.  Urkunde über die Verleihung des akademischen Grades,
5.  Nachweise über eine etwaige berufliche Tätigkeit nach Ablegung des Hochschulexamens.

Die Vorlage eines Lichtbildes ist freiwillig.

(3) Vor der Einstellung sind auf Anforderung der Einstellungsbehörde vorzulegen:

1.  die Geburtsurkunde, gegebenenfalls Eheurkunde, Lebenspartnerschaftsurkunde und Geburtsurkunden von Kindern,
2.  ein Nachweis der deutschen Staatsangehörigkeit gemäß Artikel 116 des Grundgesetzes oder der Staatsangehörigkeit eines anderen Mitgliedsstaates der Europäischen Union, eines anderen Vertragsstaates des Abkommens über den Europäischen Wirtschaftsraum oder eines Drittstaates, dem Deutschland und die Europäische Union vertraglich einen entsprechenden Anspruch auf Anerkennung von Berufsqualifikationen eingeräumt haben,
3.  ein amtsärztliches Gesundheitszeugnis, das auch über das Seh-, Farbunterscheidungs- und Hörvermögen Auskunft gibt,
4.  eine persönliche schriftliche Erklärung, ob gerichtliche Strafen vorliegen oder ein gerichtliches Strafverfahren oder ein Ermittlungsverfahren der Staatsanwaltschaft anhängig ist,
5.  ein behördliches Führungszeugnis nach § 30 Absatz 5 des Bundeszentralregistergesetzes.

(4) Über die Einstellung für das technische Referendariat entscheidet die Einstellungsbehörde.
Sie teilt den ausgewählten Bewerberinnen und Bewerbern den Termin für die Einstellung mit.
Kommt eine Bewerberin oder ein Bewerber diesem Termin ohne triftigen Grund nicht nach, verliert die Zusage der Einstellung ihre Gültigkeit.

(5) Aus der Einstellung kann kein Anspruch auf eine spätere Verwendung im öffentlichen Dienst hergeleitet werden.

#### § 4 Ernennung, Bezüge

(1) Für das technische Referendariat einzustellende Bewerberinnen und Bewerber werden unter Berufung in das Beamtenverhältnis auf Widerruf

1.  in den Fachrichtungen nach § 1 Absatz 4 Nummer 1 und 3 bis 5 zu Baureferendarinnen und Baureferendaren und
2.  in der Fachrichtung nach § 1 Absatz 4 Nummer 2 zu Vermessungsreferendarinnen und Vermessungsreferendaren

ernannt.
Soweit die Voraussetzungen für eine Berufung in das Beamtenverhältnis auf Widerruf in der Fachrichtung Geodäsie und Geoinformation nicht vorliegen, erfolgt eine Anstellung in einem öffentlich-rechtlichen Ausbildungsverhältnis.
Für die gemäß Satz 2 in das technische Referendariat aufgenommenen Personen sind die für Beamtinnen und Beamte geltenden Vorschriften sinngemäß anzuwenden.

(2) Die Referendarinnen und Referendare erhalten nach den hierfür geltenden Vorschriften Anwärterbezüge für Beamtinnen und Beamte im Vorbereitungsdienst oder, bei Anstellung in einem öffentlich-rechtlichen Ausbildungsverhältnis, Unterhaltsgeld in Höhe der Anwärterbezüge für Beamtinnen und Beamte im Vorbereitungsdienst.

#### § 5 Ausbildungsbehörde und Ausbildungsstellen

(1) Die Referendarinnen und Referendare werden von der Einstellungsbehörde, sofern sie die Ausbildung nicht selbst übernimmt, einer Ausbildungsbehörde zugewiesen.
Ausbildungsbehörden sind die in den Sondervorschriften für die einzelnen Fachrichtungen genannten Stellen.

(2) Die Ausbildungsbehörde weist die Referendarinnen und Referendare den Ausbildungsstellen zu.

(3) Die Referendarinnen und Referendare können in einzelnen Abschnitten auch bei Verwaltungen, die dem Oberprüfungsamt nicht angeschlossen sind, oder bei sonstigen geeigneten Stellen ausgebildet werden.

#### § 6 Dauer und Gliederung des technischen Referendariats

(1) Das technische Referendariat umfasst die Ausbildung und das Staatsexamen und dauert 24 Monate.
Es kann bis auf 12 Monate gekürzt werden, soweit für die Ausbildung förderliche Zeiten nachgewiesen werden.
Förderlich sind solche Tätigkeiten, die geeignet sind, die Ausbildung in einzelnen Abschnitten ganz oder teilweise zu ersetzen, beispielsweise bei Berufstätigkeit in leitender Stellung oder verantwortlicher Projektleitung.
Ein entsprechender Antrag der Referendarin oder des Referendars ist spätestens zwei Monate nach Beginn des technischen Referendariats vorzulegen.

(2) Das technische Referendariat soll sich im Fall von § 16 Absatz 4 oder 5 um sechs Wochen verkürzen.

(3) Das technische Referendariat kann um bis zu sechs Monate verlängert werden, wenn das Ziel der Ausbildung in einzelnen Ausbildungsabschnitten oder insgesamt nicht erreicht wird oder wenn Dienstbefreiung nach der Erholungsurlaubs- und Dienstbefreiungsverordnung gewährt wird und die Dienstbefreiung die Dauer von einem Monat innerhalb eines Jahres überschreitet.
Es soll bei einem Beschäftigungsverbot nach dem Mutterschutzgesetz, einer Elternzeit oder bei Dienstunfähigkeit in entsprechendem Umfang verlängert werden, wenn die Unterbrechung einen Monat innerhalb eines Jahres überschreitet.

(4) Über eine Verkürzung oder eine Verlängerung des technischen Referendariats entscheidet die Ausbildungsbehörde im Benehmen mit der Einstellungsbehörde, im Falle der Anrechnung von förderlichen Tätigkeiten nach Absatz 1 zusätzlich im Benehmen mit der Direktorin oder dem Direktor des Oberprüfungsamtes.

(5) Das technische Referendariat gliedert sich in Ausbildungsabschnitte, deren Anzahl, Dauer und Inhalt in den Sondervorschriften der Fachrichtungen geregelt sind.
Vorzugsweise sind Ausbildungsstationen in längere Ausbildungsabschnitte von mindestens 16 Wochen zu bündeln, um die notwendige exemplarische Ausbildungstiefe zu erreichen.

(6) Den Referendarinnen und Referendaren soll nach Möglichkeit die Gelegenheit gegeben werden, innerhalb der Ausbildungsabschnitte Wahlstationen (Hospitationen) von mindestens einem Monat Dauer auf anderen staatlichen Ebenen, in anderen Institutionen, im kommunalen Bereich und in der Wirtschaft zu durchlaufen.

#### § 7 Inhalt und Gestaltung der Ausbildung

(1) Die Referendarinnen und Referendare werden nach den Sondervorschriften ihrer Fachrichtung ausgebildet.
Wenn bei der Ausbildung erhebliche Abweichungen von den Sondervorschriften beabsichtigt werden, ist hierzu vorher die Zustimmung des Kuratoriums des Oberprüfungsamtes einzuholen.

(2) Als Einführung in das technische Referendariat soll den Referendarinnen und Referendaren ein Überblick in das allgemeine Verwaltungsgeschehen sowie über den öffentlichen Dienst und die besonderen Aufgaben ihrer Fachverwaltung vermittelt werden.
Dabei sollen ihnen das Ziel der Ausbildung erläutert und Hinweise zur Gliederung der Ausbildung, zum Ausbildungsstoff in den einzelnen Ausbildungsabschnitten und zum Staatsexamen gegeben werden.

(3) Die Ausbildung soll durch Lehrgänge, Seminare, Planspiele, computergestütztes Lernen (e-Learning), integriertes Lernen (Blended-Learning), Arbeitsgemeinschaften und Übungen in freier Rede sowie durch Exkursionen vertieft werden.
Lehrgangsinhalte für die Prüfungsfächer Allgemeine Rechts- und Verwaltungsgrundlagen sowie Führungsaufgaben und Wirtschaftlichkeit sollen fachrichtungsübergreifend vermittelt werden.

(4) Die Referendarinnen und Referendare sind so frühzeitig wie möglich in die praktischen Arbeitsabläufe der Ausbildungsstellen eigenverantwortlich mit einzubeziehen.
Der Schwerpunkt der Ausbildung liegt in der Vermittlung von methodischen Fähigkeiten im ganzheitlichen Arbeitsprozess.
In der Ausbildung sind Management- und Kommunikationsqualifikationen sowie soziale Kompetenz in Theorie und Praxis zu vermitteln.
Besonderer Wert ist darauf zu legen, dass Mechanismen und Techniken in den Bereichen Motivation, Delegation, Gesprächsführung, Konfliktbewältigung, Rhetorik, Visualisierung und Moderation erlernt werden.
Die Ausbildung in den Bereichen Betriebswirtschaft, Führung, Organisation, Projektmanagement und Recht soll nach Möglichkeit fachrichtungsübergreifend ausgebildet erfolgen, ebenso in den Bereichen Umweltverträglichkeit, Flächenbeanspruchung und Sozialverträglichkeit.

(5) Die Kompetenz im Umgang mit den Regelungen und Abläufen der Europäischen Union ist zu stärken.
Geeignet dafür sind auch Hospitationen bei europäischen Institutionen und in europäischen Mitgliedsstaaten nach § 6 Absatz 6.

#### § 8 Begleitung und Überwachung der Ausbildung

(1) Die Ausbildungsbehörde bestellt zur Ausbildungsleiterin oder zum Ausbildungsleiter eine geeignete Bedienstete oder einen geeigneten Bediensteten der Behörde, die oder der das technische Referendariat durchlaufen und das Staatsexamen erfolgreich abgelegt hat.
Die Ausbildungsleiterin oder der Ausbildungsleiter lenkt und überwacht die gesamte Ausbildung.
Die Ausbildung im Einzelnen obliegt jeweils der Leiterin oder dem Leiter der Ausbildungsstelle oder der von ihr oder ihm beauftragten Person.

(2) Die Ausbildungsbehörde stellt für die Referendarinnen und Referendare einen Ausbildungsplan auf, der die Abschnitte, Zeiten und Ausbildungsstellen sowie den Ausbildungsinhalt im Einzelnen festlegt.
Wünsche der Referendarinnen und Referendare können berücksichtigt werden.

(3) Die Ausbildungsbehörde ist dafür verantwortlich, dass der Ausbildungsplan eingehalten wird.
Abweichungen sind in begründeten Fällen zulässig.

(4) Die Referendarinnen und Referendare haben einen Ausbildungsnachweis (Anlage 1) zu führen und darin eine Übersicht über ihre wesentlichen Tätigkeiten zu geben.
Der Nachweis ist grundsätzlich monatlich der Leiterin oder dem Leiter der Ausbildungsstelle und anschließend der Ausbildungsbehörde zur Prüfung und Bescheinigung vorzulegen.

(5) Die Ausbildungsbehörde führt für die Referendarinnen und Referendare eine Übersicht über das technische Referendariat (Anlage 2).

(6) Für längere Ausbildungsabschnitte wird den Referendarinnen und Referendaren eine persönliche Ausbildungsbetreuerin oder ein persönlicher Ausbildungsbetreuer zugeteilt, die oder der hauptamtlich Führungsfunktionen ausübt.
Diese oder dieser begleitet die Ausbildung der Referendarinnen und Referendare in dem Ausbildungsabschnitt durch regelmäßige Feedback-Gespräche.

#### § 9 Beurteilung während der Ausbildung

(1) Jede Ausbildungsstelle beurteilt die Referendarinnen und Referendare nach Abschluss des bei ihr abgeleisteten Ausbildungsabschnittes oder -teilabschnittes unter Angabe der Art und Dauer der Beschäftigung nach ihren Leistungen (Arbeitsgüte, Arbeitsmenge, Arbeitsweise, Führungsverhalten) und Befähigungen (Denk- und Urteilsvermögen, Organisationsvermögen, Befähigung zur Kommunikation und Zusammenarbeit, Führungsfähigkeit).
Die Beurteilung (Anlage 3) muss erkennen lassen, ob das Ziel des Ausbildungsabschnittes oder -teilabschnittes erreicht ist.
Besondere Fähigkeiten oder Mängel sind zu vermerken.

(2) Erreicht die Ausbildungszeit bei einer Ausbildungsstelle nicht die volle Dauer von sechs Wochen, bestätigt die Ausbildungsstelle nur die Art und Dauer der Beschäftigung sowie die Erreichung des Zieles des Ausbildungsabschnittes oder -teilabschnittes.
Die in Absatz 1 geforderte Beurteilung entfällt.

(3) Die Ausbildungsbehörde fertigt am Ende der Ausbildung eine abschließende Beurteilung über die gesamte Dauer des technischen Referendariats.
Absatz 1 gilt entsprechend.

(4) Die Beurteilungen nach Absatz 1 und 3 sind den Referendarinnen und Referendaren zu eröffnen und mit ihnen zu besprechen.
Die Eröffnungen sind aktenkundig zu machen und mit den Beurteilungen zu den Ausbildungsakten zu nehmen.

#### § 10 Urlaub, Berücksichtigung der Belange behinderter Referendarinnen und Referendare

(1) Erholungsurlaub ist in den Ausbildungsplan nach § 8 Absatz 2 im gegenseitigen Benehmen einzuarbeiten.

(2) Während der Zeit für die Anfertigung der häuslichen Prüfungsarbeit darf Erholungsurlaub grundsätzlich nicht gewährt werden.
Ausnahmsweise ist Urlaub im Einvernehmen mit dem Oberprüfungsamt zulässig.
Die Frist für die Abgabe der häuslichen Prüfungsarbeit verlängert sich entsprechend.

(3) Im Rahmen der Ausbildung und beim Staatsexamen sind die besonderen Belange behinderter Referendarinnen und Referendare zu berücksichtigen.
Schwerbehinderten und den ihnen gleichgestellten Menschen im Sinne des § 2 Absatz 2 und 3 des Neunten Buches Sozialgesetzbuch sind die ihrer Behinderung angemessenen Erleichterungen zu gewähren.
Art und Umfang der zu gewährenden Erleichterungen sind rechtzeitig mit den Betroffenen zu erörtern.
Die Erleichterungen dürfen nicht dazu führen, dass die Anforderungen herabgesetzt werden.

#### § 11 Entlassung aus dem technischen Referendariat

Die Einstellungsbehörde kann Referendarinnen und Referendare unter Widerruf des Beamtenverhältnisses oder Beendigung des öffentlich-rechtlichen Ausbildungsverhältnisses aus dem technischen Referendariat gemäß § 23 Absatz 4 des Beamtenstatusgesetzes entlassen, wenn ein wichtiger Grund vorliegt.
Dies ist insbesondere der Fall, wenn

1.  sie sich durch Verletzung der beamtenrechtlichen Pflichten oder durch sonstige tadelhafte Führung unwürdig erweisen, im Dienst belassen zu werden,
2.  zu erkennen ist, dass sie das Ziel der Ausbildung nicht erreichen werden, oder
3.  sie es schuldhaft versäumen, die Zulassung zum Staatsexamen (§ 14 Absatz 2) oder die Zulassung zur Wiederholung des Staatsexamens (§ 23 Absatz 4) fristgemäß zu beantragen.

### Abschnitt 2  
Staatsexamen, Prüfungsordnung

#### § 12 Zweck des Staatsexamens

Im Staatsexamen haben die Referendarinnen und Referendare ihre Führungsqualifikation und die Befähigung für den höheren technischen Verwaltungsdienst in ihrer Fachrichtung nachzuweisen.
Im Einzelnen sollen sie zeigen, dass sie ihre an einer Hochschule erworbenen Kenntnisse in der Praxis anzuwenden verstehen, dass sie mit den Aufgaben der Verwaltungen ihrer Fachrichtung, mit den einschlägigen Rechts-, Verwaltungs- und technischen Vorschriften vertraut sind und dass sie über wirtschaftliches Denken und Managementkenntnisse verfügen.

#### § 13 Abnahme durch das Oberprüfungsamt

(1) Für die Abnahme des Staatsexamens ist das Oberprüfungsamt für das technische Referendariat (Oberprüfungsamt) zuständig.
Rechtsgrundlage ist das Übereinkommen über die Errichtung eines gemeinschaftlichen Oberprüfungsamtes deutscher Länder und Verwaltungen vom 16.
September 1948 in der Fassung vom 1.
Oktober 2016.

(2) Die mündliche Prüfung des Staatsexamens findet grundsätzlich am Sitz des Oberprüfungsamtes statt.
Die Direktorin oder der Direktor des Oberprüfungsamtes kann diese auch an anderen Orten abhalten lassen.

(3) Beim Oberprüfungsamt werden Prüfungsausschüsse für die einzelnen Fachrichtungen eingerichtet.
Die oder der Vorsitzende des Kuratoriums des Oberprüfungsamtes bestellt die Vorsitzenden, deren Stellvertreterinnen oder Stellvertreter und die weiteren Mitglieder der Prüfungsausschüsse.
Es sollen Führungskräfte aus Verwaltung und Wirtschaft bestellt werden, die das technische Referendariat durchlaufen und das Staatsexamen erfolgreich abgelegt haben.
Das Kuratorium des Oberprüfungsamtes kann in Sonderfällen Ausnahmen zulassen.

(4) Das Staatsexamen wird in den in § 1 Absatz 4 genannten Fachrichtungen von Prüfungskommissionen abgenommen, die von der Direktorin oder dem Direktor des Oberprüfungsamtes aus den Mitgliedern der Prüfungsausschüsse gebildet werden.
Die Prüfungskommissionen setzen sich jeweils zusammen aus der oder dem Vorsitzenden und mindestens drei Prüferinnen oder Prüfern, wobei die Besetzung der Prüfungskommissionen je nach Prüfungsfächern personell wechseln kann.
Ein Mitglied der Prüfungskommission soll nach Möglichkeit der Verwaltung des Landes angehören, in der die zu prüfende Referendarin oder der zu prüfende Referendar überwiegend ausgebildet worden ist.

(5) Die Mitglieder der Prüfungskommissionen sind bei ihrer Tätigkeit unabhängig und an Weisungen nicht gebunden.
Alle mit der Behandlung von Prüfungsangelegenheiten befassten Personen sind zur Verschwiegenheit in allen die Vorbereitung und Durchführung des Staatsexamens betreffenden Angelegenheiten verpflichtet.

(6) Die oder der Vorsitzende des zuständigen Prüfungsausschusses oder die entsprechende Vertretung leitet die Prüfungskommission.
Die Prüfungskommission ist bei ihrer Entscheidung beschlussfähig, wenn die oder der Vorsitzende und zwei weitere Prüferinnen oder Prüfer anwesend sind.
Soweit über die Leistungen in der mündlichen Prüfung entschieden wird, müssen die beschließenden Prüferinnen und Prüfer an der mündlichen Prüfung teilgenommen haben.
Die Prüfungskommissionen entscheiden mit Stimmenmehrheit.
Bei Stimmengleichheit gibt die Stimme der oder des Vorsitzenden den Ausschlag.
Stimmenthaltung ist nicht zulässig.

(7) Die Direktorin oder der Direktor des Oberprüfungsamtes sorgt für den ordnungsgemäßen Prüfungsablauf.
Sie oder er wacht darüber, dass in allen Fachrichtungen gleich hohe Prüfungsanforderungen gestellt und gleiche Beurteilungsmaßstäbe angelegt werden.
Zur Wahrnehmung dieser Aufgaben kann sie oder er sich an den einzelnen Prüfungsteilen des Staatsexamens beteiligen und gilt in diesem Falle von Amts wegen als weiteres Mitglied der Prüfungskommission.

#### § 14 Zulassung zum Staatsexamen

(1) Zum Staatsexamen können nur Referendarinnen und Referendare zugelassen werden, die die Ausbildung ordnungsgemäß abgeleistet haben.

(2) Referendarinnen und Referendare haben ihren Antrag auf Zulassung zum Staatsexamen (Anlage 4) innerhalb von zwei Wochen nach Aufforderung durch die Ausbildungsbehörde zu stellen.
Die Ausbildungsbehörde hat den Referendarinnen und Referendaren den Termin für den Antrag unter Hinweis auf die Folgen eines Versäumnisses (§ 11) schriftlich mitzuteilen.

(3) Die Ausbildungsbehörde leitet den Antrag auf Zulassung zum Staatsexamen mit den darin aufgeführten Unterlagen so rechtzeitig dem Oberprüfungsamt zu, dass er dem Oberprüfungsamt zwei Monate vor Aushändigung der Aufgabe für die häusliche Prüfungsarbeit vorliegt.

(4) Die Direktorin oder der Direktor des Oberprüfungsamtes entscheidet aufgrund der mit dem Zulassungsantrag vorgelegten Unterlagen über die Zulassung zum Staatsexamen.

(5) Das Oberprüfungsamt leitet den Zulassungsbescheid zusammen mit der Aufgabe für die häusliche Prüfungsarbeit der Ausbildungsbehörde zur fristgerechten Aushändigung an die Referendarin oder den Referendar zu.
Die dem Zulassungsantrag beigefügten Unterlagen werden gleichzeitig zurückgegeben.
Sie sind zu vervollständigen und dem Oberprüfungsamt mit der abschließenden Beurteilung (§ 9 Absatz 3) spätestens zwei Wochen vor dem Termin der mündlichen Prüfung wieder zuzuleiten.

(6) Wird die Referendarin oder der Referendar nicht zum Staatsexamen zugelassen, regelt die Einstellungsbehörde die Dauer und Gestaltung des weiteren technischen Referendariats.
§ 23 Absatz 4 gilt entsprechend.

#### § 15 Gliederung und Inhalt

Das Staatsexamen besteht aus

1.  der häuslichen Prüfungsarbeit,
2.  den schriftlichen Arbeiten unter Aufsicht und
3.  der mündlichen Prüfung.

#### § 16 Häusliche Prüfungsarbeit

(1) Die Referendarin oder der Referendar soll durch die häusliche Prüfungsarbeit zeigen, dass sie oder er eine Aufgabe aus der Praxis richtig erfassen, methodisch bearbeiten und das Ergebnis klar darstellen kann.
In der Aufgabenstellung sollen Managementaspekte einen hohen Stellenwert erhalten.

(2) Die Referendarin oder der Referendar hat die häusliche Prüfungsarbeit innerhalb von sechs Wochen zu fertigen und im Original unmittelbar beim Oberprüfungsamt einzureichen.
Bei Vorliegen triftiger Gründe kann die Direktorin oder der Direktor des Oberprüfungsamtes die Frist um höchstens vier Wochen verlängern.
Die Referendarin oder der Referendar hat in diesem Fall unverzüglich einen Antrag durch die Ausbildungsbehörde, die dazu Stellung nimmt, an das Oberprüfungsamt zu richten.
Soweit eine Verlängerung nach Satz 2 zur Bearbeitung nicht ausreicht, wird eine neue Aufgabe gestellt, die innerhalb der Frist des Satzes 1 zu bearbeiten ist.

(3) Die Referendarin oder der Referendar hat die Aufgabe in allen ihren Teilen ohne fremde Hilfe zu bearbeiten und alle benutzten Quellen und Hilfsmittel anzugeben.
Dieses ist in einer dem Textteil der Arbeit vorzuheftenden Erklärung zu versichern.
Alle Ausarbeitungen sind von der Referendarin oder dem Referendar eigenhändig zu unterschreiben.

(4) Auf Antrag der Referendarin oder des Referendars kann die Direktorin oder der Direktor des Oberprüfungsamtes im Einvernehmen mit der Leiterin oder dem Leiter des Prüfungsausschusses eine während der Ausbildungszeit zu verfassende Abschnitts- oder Projektarbeit als häusliche Prüfungsarbeit zulassen, wenn die Aufgabe unter Beteiligung einer Prüferin oder eines Prüfers des Oberprüfungsamtes gestellt worden ist und einer häuslichen Prüfungsarbeit entspricht.
Der Antrag ist vor Ausgabe der Abschnitts- oder Projektaufgabe zur Entscheidung vorzulegen.
Die Arbeit wird unabhängig von ihrer Begutachtung im Ausbildungsabschnitt von Prüferinnen oder Prüfern des Oberprüfungsamtes beurteilt.

(5) Hat die Referendarin oder der Referendar an einem vom Architekten- und Ingenieurverein zu Berlin ausgeschriebenen „Schinkel-Wettbewerb“ teilgenommen, so kann die Wettbewerbsarbeit auf Antrag als häusliche Prüfungsarbeit durch die Direktorin oder den Direktor des Oberprüfungsamtes im Einvernehmen mit dem Leiter oder der Leiterin des Prüfungsausschusses anerkannt werden, wenn die Wettbewerbsaufgabe unter Beteiligung einer Prüferin oder eines Prüfers des Oberprüfungsamtes gestellt worden ist und einer häuslichen Prüfungsarbeit entspricht.
Der Antrag ist mit dem Zulassungsantrag zu stellen.
Die Arbeit wird unabhängig von ihrer Bewertung im Wettbewerb von Prüferinnen oder Prüfern des Oberprüfungsamtes beurteilt.

(6) Die häusliche Prüfungsarbeit ist angenommen, wenn die Erst- und Zweitbewertung nach § 20 Absatz 1 jeweils mit mindestens „ausreichend“ erfolgt.
Wird die häusliche Prüfungsarbeit mit der Erstbewertung oder der Zweitbewertung nicht mit mindestens „ausreichend“ bewertet, entscheidet die oder der Vorsitzende des zuständigen Prüfungsausschusses, ob die häusliche Prüfungsarbeit angenommen wird.
Wird die häusliche Prüfungsarbeit nicht angenommen, ist das Staatsexamen nicht bestanden.

#### § 17 Schriftliche Arbeiten unter Aufsicht

(1) Die Referendarin oder der Referendar soll durch die schriftlichen Arbeiten unter Aufsicht zeigen, dass sie oder er Aufgaben rasch und sicher erfassen, in kurzer Frist mit den zugelassenen Hilfsmitteln lösen und das Ergebnis knapp und übersichtlich darstellen kann.
Managementaspekte sollen in der Aufgabenstellung einen hohen Stellenwert erhalten.

(2) Ist die häusliche Prüfungsarbeit angenommen worden (§ 16 Absatz 6), so wird die Referendarin oder der Referendar vom Oberprüfungsamt zu den schriftlichen Arbeiten unter Aufsicht spätestens zwei Wochen vorher unter Angabe von Zeit und Ort ihrer Fertigung geladen.

(3) Insgesamt ist an vier aufeinander folgenden Werktagen aus vier Prüfungsfächern je eine schriftliche Arbeit unter Aufsicht in jeweils sechs Stunden zu fertigen.
Mindestens eine schriftliche Arbeit unter Aufsicht ist dabei aus den Prüfungsfächern Allgemeine Rechts- und Verwaltungsgrundlagen oder Führungsaufgaben und Wirtschaftlichkeit zu stellen.
Die zugelassenen Hilfsmittel werden in der Regel zur Verfügung gestellt.
Wenn die Referendarin oder der Referendar selbst Hilfsmittel mitbringen soll, werden diese in der Ladung nach Absatz 2 ausdrücklich benannt.
Andere mitgeführte Hilfsmittel sind vor Aushändigung der Aufgabe bei der Aufsicht führenden Person zu hinterlegen.

(4) Das Oberprüfungsamt leitet die Aufgaben in verschlossenen Umschlägen der Ausbildungsbehörde zu.
Diese gibt sie einzeln ungeöffnet am Fertigungstag an die Aufsicht führende Person weiter, die sie zu Beginn der Prüfung der Referendarin oder dem Referendar aushändigt.
Mit der Aufsicht soll eine Bedienstete oder ein Bediensteter beauftragt werden, die oder der das technische Referendariat durchlaufen und das Staatsexamen erfolgreich abgelegt hat.

(5) Spätestens mit Ablauf der Bearbeitungsfrist hat die Referendarin oder der Referendar die schriftliche Arbeit unter Aufsicht unterschrieben und mit allen Zwischenrechnungen und Konzepten sowie dem Aufgabentext der Aufsicht führenden Person abzugeben.

(6) Die schriftlichen Arbeiten unter Aufsicht werden grundsätzlich mit PC bearbeitet, wenn die oder der Vorsitzende des Prüfungsausschusses dem grundsätzlich zustimmt und die Ausbildungsbehörde für die Prüfung eine anforderungsgerechte Ausstattung gewährleistet.
Die Referendarin oder der Referendar kann auf Antrag bei ihrer oder seiner Ausbildungsbehörde eine handschriftliche Bearbeitung verlangen.

(7) Über den Verlauf der schriftlichen Arbeiten unter Aufsicht fertigt die Aufsicht führende Person jeweils eine Niederschrift unter Verwendung des vom Oberprüfungsamt dafür vorgesehenen Formulars an.
Die Niederschriften sind zu sammeln und am letzten Fertigungstag dem Oberprüfungsamt zu übersenden.
Die gefertigten Arbeiten sind noch am jeweiligen Fertigungstag zusammen mit den Aufgabentexten mit Einlieferungsnachweis der Erstprüferin oder dem Erstprüfer, wie vom Oberprüfungsamt benannt, zur Bewertung zuzuleiten.

(8) Sind die schriftlichen Arbeiten unter Aufsicht als nicht bestanden bewertet (§ 21 Absatz 5 und 6), wird die Referendarin oder der Referendar nicht zur mündlichen Prüfung zugelassen.
Die Entscheidung trifft das Oberprüfungsamt aufgrund der Bewertungen nach § 20.
Wird die Referendarin oder der Referendar nicht zugelassen, ist das Staatsexamen nicht bestanden.

#### § 18 Mündliche Prüfung

(1) In der mündlichen Prüfung soll die Referendarin oder der Referendar neben dem Wissen und Können in der jeweiligen Fachrichtung vor allem Verständnis für Management und Führung sowie für technische, wirtschaftliche und rechtliche Zusammenhänge erkennen lassen.
Dabei sollen auch Urteilsvermögen, Sicherheit im Auftreten und Ausdrucksfähigkeit bewiesen werden.

(2) Die Referendarin oder der Referendar wird zur mündlichen Prüfung, die sich auf zwei Tage erstreckt, vom Oberprüfungsamt schriftlich geladen.
Bis zu drei Referendarinnen oder Referendare können in einer Gruppe gemeinsam geprüft werden.

(3) Die mündliche Prüfung erstreckt sich auf die sechs Prüfungsfächer der Fachrichtung, deren Prüfstoff dem Prüfstoffverzeichnis zu entnehmen ist.
Die Prüfungsdauer beträgt bei gemeinsamer Prüfung von drei Referendarinnen oder Referendaren in einer Gruppe in der Regel insgesamt sechseinhalb Stunden, mindestens aber insgesamt sechs Stunden.
Werden weniger als drei Referendarinnen oder Referendare geprüft, wird die Prüfungsdauer angemessen gekürzt.
Die Prüfungskommission kann die Prüfungsdauer verlängern, wenn dies zur Beurteilung der Leistungen einer Referendarin oder eines Referendars notwendig ist.
Die Verlängerung soll 15 Minuten je Prüfungsfach nicht überschreiten.

(4) Die Regelprüfungszeit bei einer Prüfung von drei Referendarinnen oder Referendaren beträgt bei einer Gesamtprüfungsdauer von sechseinhalb Stunden für zwei Prüfungsfächer jeweils 75 Minuten; eines dieser beiden Fächer ist das Prüfungsfach Führungsaufgaben und Wirtschaftlichkeit.
Die Regelprüfungszeit der vier anderen Prüfungsfächer beträgt in diesem Fall jeweils eine Stunde.
Bei einer Gesamtprüfungsdauer von sechs Stunden beträgt bei drei Referendarinnen oder Referendaren die Regelprüfungszeit für jedes Prüfungsfach jeweils eine Stunde.

(5) Am zweiten Prüfungstag hat die Referendarin oder der Referendar einen Vortrag von mindestens fünf und längstens zehn Minuten zu halten.
Das Thema wird aus der Fachrichtung der Referendarin oder des Referendars oder einem sonst interessierenden Gebiet entnommen und ist etwa 20 Minuten vor Beginn des Vortrags mitzuteilen.

(6) Die mündliche Prüfung und die Beratung sind nicht öffentlich.
Während der mündlichen Prüfung, nicht dagegen bei der Festsetzung der Prüfungsnoten, können nach vorheriger Absprache mit dem Oberprüfungsamt die Ausbildungsleiterin oder der Ausbildungsleiter der Referendarin oder des Referendars, in begründeten Fällen auch eine Vertreterin oder ein Vertreter der Einstellungsbehörde zugegen sein.

#### § 19 Unterbrechung, Rücktritt

(1) Kann die Referendarin oder der Referendar nicht zur Fertigung der schriftlichen Arbeiten unter Aufsicht oder zur mündlichen Prüfung erscheinen oder muss sie oder er das Staatsexamen abbrechen, so ist unverzüglich das Oberprüfungsamt unter Angabe des Grundes zu verständigen und der Nachweis der Verhinderung zu erbringen.
Erkennt die Direktorin oder der Direktor des Oberprüfungsamtes den Grund als triftig an, so gelten bei einer Unterbrechung die bis dahin abgeschlossenen Prüfungsteile als abgelegt.
Das Staatsexamen ist zum nächstmöglichen Termin fortzusetzen.

(2) Absatz 1 gilt entsprechend, wenn die Referendarin oder der Referendar bei Vorliegen eines triftigen Grundes mit Zustimmung des Oberprüfungsamtes vom Staatsexamen zurücktritt.

#### § 20 Bewertung der Prüfungsleistungen im Einzelnen

(1) Die häusliche Prüfungsarbeit und die schriftlichen Arbeiten unter Aufsicht werden von jeweils zwei Prüferinnen oder Prüfern als Erstprüferin oder Erstprüfer und als Zweitprüferin oder Zweitprüfer bewertet (Erst- und Zweitbewertung).
Die Leistungen in den Prüfungsfächern der mündlichen Prüfung werden von den jeweiligen Prüferinnen oder Prüfern für das betreffende Prüfungsfach bewertet.

(2) Die häusliche Prüfungsarbeit und die schriftlichen Arbeiten unter Aufsicht sind mit schriftlicher Begründung zu bewerten.

(3) Die einzelnen Prüfungsleistungen, einschließlich des Vortrages nach § 18 Absatz 6, sind mit folgenden Noten und Punktzahlen zu bewerten:

1,0/1,3

\=

sehr gut;

1,7/2,0

\=

gut;

2,3/2,7

\=

vollbefriedigend;

3,0/3,3

\=

befriedigend;

3,7/4,0

\=

ausreichend;

5,0

\=

mangelhaft.

Andere Punktzahlen oder Zwischennoten dürfen nicht verwendet werden.
Dabei bedeutet die Note:

sehr gut

\=

eine Leistung, die den Anforderungen in außergewöhnlichem Maße entspricht;

gut

\=

eine Leistung, die den Anforderungen in erheblichem Maße entspricht;

vollbefriedigend

\=

eine Leistung, die den Anforderungen voll entspricht;

befriedigend

\=

eine Leistung, die im Allgemeinen den Anforderungen entspricht;

ausreichend

\=

eine Leistung, die zwar Mängel aufweist, aber im Ganzen den Anforderungen noch entspricht;

mangelhaft

\=

eine Leistung, die den Anforderungen nicht entspricht.

#### § 21 Abschließende Bewertung, Gesamturteil

(1) Die Punktzahlen und Noten der angenommenen häuslichen Prüfungsarbeit und der schriftlichen Arbeiten unter Aufsicht sowie die der Leistungen in jedem Prüfungsfach der mündlichen Prüfung werden unabhängig voneinander von der zuständigen Prüfungskommission als Einzelnoten festgesetzt.
Die Prüfungskommission entscheidet mit Stimmenmehrheit (§ 13 Absatz 6).

(2) Für die Bildung des für das Gesamturteil maßgebenden Mittelwertes wird

die Punktzahl der häuslichen Prüfungsarbeit

mit zwei (= 20 Prozent),

die Durchschnittspunktzahl aller schriftlichen Arbeiten unter Aufsicht

mit drei (= 30 Prozent) und

die Durchschnittspunktzahl aller Fächer der mündlichen Prüfung

mit fünf (= 50 Prozent)

multipliziert und die hieraus gebildete Summe durch zehn dividiert.
Eine dritte Stelle hinter dem Komma wird bei allen Rechenvorgängen nicht berücksichtigt.

(3) Für das Gesamturteil gelten die folgenden Noten:

sehr gut;

gut;

vollbefriedigend;

befriedigend;

ausreichend;

nicht bestanden.

(4) Das Staatsexamen ist bestanden mit

dem „Prädikat sehr gut“

bei einem Mittelwert von 1,00 bis 1,49,

dem „Prädikat gut“

bei einem Mittelwert von 1,50 bis 2,29,

dem „Prädikat vollbefriedigend“  

bei einem Mittelwert von 2,30 bis 2,99,

„befriedigend“

bei einem Mittelwert von 3,00 bis 3,49,

„ausreichend“

bei einem Mittelwert von 3,50 bis 4,00.

(5) Das Staatsexamen ist nicht bestanden, wenn

1.  die häusliche Prüfungsarbeit nicht angenommen ist (§ 16 Absatz 6) oder
2.  der nach Absatz 2 errechnete Mittelwert 4.01 oder schlechter lautet oder
3.  die Noten in zwei Prüfungsfächern der schriftlichen Arbeiten unter Aufsicht „mangelhaft“ sind oder
4.  die Note in einem Prüfungsfach der schriftlichen Arbeiten unter Aufsicht „mangelhaft“ ist und dabei die Durchschnittspunktzahl aller schriftlichen Arbeiten unter Aufsicht 4.01 oder schlechter lautet oder
5.  die Noten in drei Prüfungsfächern der mündlichen Prüfung „mangelhaft“ sind oder
6.  die Noten in bis zu zwei Prüfungsfächern der mündlichen Prüfung „mangelhaft“ sind und nicht durch andere Noten in Fächern der mündlichen Prüfung ausgeglichen werden; ein Ausgleich ist je Prüfungsfach durch zwei Noten „befriedigend“ oder „vollbefriedigend“ oder eine Note „gut“ oder „sehr gut“ gegeben.

(6) Das Staatsexamen gilt als nicht bestanden, wenn die Referendarin oder der Referendar

1.  die häusliche Prüfungsarbeit nicht rechtzeitig einreicht oder ohne vom Oberprüfungsamt anerkannten Grund zu den schriftlichen Arbeiten unter Aufsicht oder zur mündlichen Prüfung nicht erscheint oder einen dieser Prüfungsteile abbricht (§ 19 Absatz 1) oder
2.  nach § 24 Absatz 1 oder 2 von der weiteren Teilnahme an der Prüfung ausgeschlossen ist.

(7) Über den Prüfungshergang ist eine Niederschrift anzufertigen, in der die Besetzung der zuständigen Prüfungskommission, der Name der Referendarin oder des Referendars, die Einzelnoten der schriftlichen Arbeiten unter Aufsicht und der mündlichen Prüfung, die Gesamtnote und die Bewertung des Vortrags festgehalten werden.
Die Niederschrift ist von der oder dem Vorsitzenden der zuständigen Prüfungskommission und den an der mündlichen Prüfung beteiligten Prüferinnen und Prüfern zu unterzeichnen.
Sie ist wie die schriftlichen Bewertungen der häuslichen Prüfungsarbeit und der schriftlichen Arbeiten unter Aufsicht Bestandteil der Prüfungsakten.

(8) Im Anschluss an die mündliche Prüfung wird der Referendarin oder dem Referendar das Ergebnis des Staatsexamens bekanntgegeben.

(9) Bei Nichtbestehen des Staatsexamens erhält die Referendarin oder der Referendar hierüber vom Oberprüfungsamt einen Bescheid mit Rechtsbehelfsbelehrung.

#### § 22 Prüfungszeugnis

(1) Mit Bestehen des Staatsexamens erwirbt die Referendarin oder der Referendar die Befähigung zum höheren technischen Verwaltungsdienst.
Sie oder er ist berechtigt, die Berufsbezeichnung „Technische Assessorin“ oder „Technischer Assessor“ zu führen.
Hierüber erteilt das Oberprüfungsamt ein Prüfungszeugnis, das die Einzelnoten und das Gesamturteil enthält.
Das Prüfungszeugnis wird nach dem Muster der Anlage 5 gefertigt, von der Direktorin oder dem Direktor des Oberprüfungsamtes unterzeichnet und mit dem Siegel versehen.
Das Prüfungszeugnis wird mit einem Bescheid des Oberprüfungsamtes samt Rechtsbehelfsbelehrung und mit einem Zertifikat ausgehändigt oder übersandt.

(2) Findet die mündliche Prüfung nach § 13 Absatz 2 nicht am Dienstsitz des Oberprüfungsamtes statt, wird der Referendarin oder dem Referendar mit Bestehen des Staatsexamens eine Bescheinigung des Oberprüfungsamtes ausgehändigt, die auch Angaben über die Berufsbezeichnung beinhaltet.
Das Prüfungszeugnis nach Absatz 1 wird der Referendarin oder dem Referendar übersandt.

#### § 23 Wiederholung

(1) Hat die Referendarin oder der Referendar das Staatsexamen nicht bestanden, so kann sie oder er das Staatsexamen einmal wiederholen.

(2) Die Wiederholungsprüfung erstreckt sich

1.  wenn die häusliche Prüfungsarbeit nicht rechtzeitig eingereicht oder vom Prüfungsausschuss nicht angenommen wurde, auf die Fertigung einer neuen häuslichen Prüfungsarbeit, auf die Fertigung der schriftlichen Arbeiten unter Aufsicht und auf die mündliche Prüfung,
2.  auf die mit „mangelhaft“ bewerteten Prüfungsfächer der schriftlichen Arbeiten unter Aufsicht und auf die mündliche Prüfung,
3.  auf die mit „mangelhaft“ bewerteten Prüfungsfächer der mündlichen Prüfung.

Darüber hinaus kann der Prüfungsausschuss bei überwiegend mangelhaften Leistungen die Wiederholung aller Prüfungsfächer der schriftlichen Arbeiten unter Aufsicht, die Wiederholung aller Prüfungsfächer der mündlichen Prüfung oder die Wiederholung aller Prüfungsfächer der schriftlichen Arbeiten unter Aufsicht und aller Prüfungsfächer der mündlichen Prüfung beschließen.

(3) Hat die Referendarin oder der Referendar die häusliche Prüfungsarbeit nicht rechtzeitig eingereicht oder ist diese nicht angenommen worden (§ 16 Absatz 6), hat sie oder er innerhalb von vier Wochen nach Erhalt des entsprechenden Bescheides des Oberprüfungsamtes eine neue Aufgabe zu beantragen.

(4) Der Prüfungsausschuss befindet darüber, in welchen Abschnitten die Ausbildung einer Ergänzung bedarf und schlägt der Einstellungsbehörde die Dauer der zusätzlichen Ausbildung vor.
Sie soll mindestens zwei, höchstens sechs Monate betragen.
Die Referendarin oder der Referendar hat sechs Wochen vor Beendigung der zusätzlichen Ausbildung die Zulassung zur Wiederholung des Staatsexamens zu beantragen.

#### § 24 Täuschung, Verstoß gegen die Prüfungsordnung

(1) Referendarinnen oder Referendare, die zu täuschen versuchen, die insbesondere die Versicherung der selbständigen Bearbeitung der häuslichen Prüfungsarbeit unrichtig abgeben (§ 16 Absatz 3) oder die bei den schriftlichen Arbeiten unter Aufsicht andere als die zugelassenen Hilfsmittel mit sich führen (§ 17 Absatz 3) oder die sich sonst eines Verstoßes gegen die Prüfungsordnung schuldig machen, soll die Fortsetzung des Staatsexamens unter Vorbehalt gestattet werden.
Der Vorbehalt ist aktenkundig zu machen.
Bei einer erheblichen Störung soll die weitere Teilnahme an dem betreffenden Prüfungsteil versagt werden.

(2) Über die Folgen eines Vorfalls nach Absatz 1 oder einer Täuschung, die nach Abgabe der häuslichen Prüfungsarbeit oder einer schriftlichen Arbeit unter Aufsicht festgestellt wird, entscheidet die Direktorin oder der Direktor des Oberprüfungsamtes im Einvernehmen mit der oder dem Vorsitzenden des zuständigen Prüfungsausschusses, bei einer Täuschung oder einem Ordnungsverstoß während der mündlichen Prüfung die jeweils zuständige Prüfungskommission.
Je nach Schwere der Verfehlung kann die Wiederholung einzelner oder mehrerer Prüfungsteile angeordnet, die Referendarin oder der Referendar von dem weiteren Staatsexamen ausgeschlossen oder das Staatsexamen für nicht bestanden erklärt werden.
Die Referendarin oder der Referendar erhält hierüber einen schriftlichen Bescheid, der mit einer Rechtsbehelfsbelehrung versehen ist.

(3) Wird eine Täuschung erst nach Aushändigung des Prüfungszeugnisses bekannt, ist das Oberprüfungsamt unverzüglich zu unterrichten.
Die Direktorin oder der Direktor des Oberprüfungsamtes kann im Benehmen mit dem Kuratorium des Oberprüfungsamtes das Staatsexamen nachträglich für nicht bestanden erklären.
Diese Maßnahme ist zulässig innerhalb einer Frist von fünf Jahren nach dem letzten Tag der mündlichen Prüfung.

(4) Die oder der Betroffene ist vor der Entscheidung nach Absatz 2 oder Absatz 3 zu hören.

#### § 25 Prüfungsakte

(1) Wer am Staatsexamen teilgenommen hat, kann auf Antrag seine Prüfungsakte in der Geschäftsstelle des Oberprüfungsamtes einsehen.
Der Antrag nach Satz 1 ist schriftlich an die Direktorin oder den Direktor des Oberprüfungsamtes zu stellen.

(2) Nach fünf Jahren wird die Prüfungsakte vernichtet.

#### § 26 Ausführungsbestimmungen

Die weitere Ausgestaltung des Staatsexamens regelt die Direktorin oder der Direktor des Oberprüfungsamtes im Benehmen mit dem Kuratorium des Oberprüfungsamtes in Ausführungsbestimmungen, die öffentlich zugänglich gemacht und den Referendarinnen und Referendaren auf Anforderung vom Oberprüfungsamt zur Verfügung gestellt werden.

### Abschnitt 3  
Sondervorschriften der Fachrichtungen

#### § 27 Sondervorschriften der Fachrichtung Architektur

(1) Zulassungsvoraussetzung für das technische Referendariat ist unter den Vorgaben von § 2 der erfolgreiche Abschluss eines wissenschaftlichen Studiums des Studienganges Architektur.
Eine Zulassung für das technische Referendariat ist grundsätzlich nur dann möglich, wenn das nachfolgend aufgeführte Wissensspektrum (Studieninhalte) nachgewiesen wird:

1.  Allgemeine Fächer:
    1.  Architektur- und Stadtbaugeschichte
    2.  Planungs- und Architekturtheorie
    3.  Rechtliche und ökonomische Grundlagen der Stadt- und Objektplanung
    4.  Kostenermittlung
    5.  Projektorganisation
2.  Gestaltung und Darstellung:
    1.  Darstellende Geometrie und Technische Darstellung
    2.  Künstlerische und funktionsorientierte Gestaltung
    3.  Künstlerische Darstellung und Entwurfspräsentation
    4.  Informations- und datentechnische Architekturdarstellung (CAD)
3.  Konstruktionsplanung:
    1.  Konstruktionslehre
    2.  Methoden des Konstruierens
    3.  Baukonstruktion
    4.  Tragwerkslehre
    5.  Bauphysik
    6.  Baustoffkunde
    7.  Technische Gebäudeausrüstung
4.  Gebäudeplanung:
    1.  Gebäudelehre
    2.  Entwurfsmethodik
    3.  Bauaufnahme
    4.  Objektplanung
5.  Grundzüge der Stadtplanung und des Städtebaues

(2) Ausbildungsbehörde (§ 5 Absatz 1) ist der Brandenburgische Landesbetrieb für Liegenschaften und Bauen.

(3) Die Ausbildung gliedert sich in mehrere Ausbildungsabschnitte.
Die Dauer der Ausbildungsabschnitte, sonstige Vorschriften für die Ausbildung und der Ausbildungsplan sind in Anlage 6a geregelt.

(4) Die Prüfungsfächer nach § 17 Absatz 3 und § 18 Absatz 4, die fächerbezogenen Prüfungszeiten in der mündlichen Prüfung und das Prüfstoffverzeichnis sind in Anlage 6b und c geregelt.

#### § 28 Sondervorschriften der Fachrichtung Geodäsie und Geoinformation

(1) Zulassungsvoraussetzung für das technische Referendariat ist unter den Vorgaben von § 2 der erfolgreiche Abschluss eines wissenschaftlichen Studiums des Studienganges Geodäsie und Geoinformatik/Geoinformation oder eines vergleichbaren Studienganges im Fachgebiet Geodäsie.
Eine Zulassung für das technische Referendariat ist grundsätzlich nur dann möglich, wenn das nachfolgend aufgeführte Wissensspektrum (Studieninhalte) nachgewiesen wird:

1.  Grundlegende Fachkenntnisse und die Befähigung zu dessen wissenschaftsmethodischer Anwendung sind in mindestens folgenden Fächern nachzuweisen:
    1.  Höhere Mathematik
    2.  Geometrie
    3.  Physik einschließlich der fachbezogenen Bereiche
    4.  Statistik und Parameterschätzung
    5.  Informatik
2.  Fachkenntnisse sowie die Fähigkeit zur Lösung von Fachaufgaben nach wissenschaftlichen Grundsätzen sind in den folgenden geodätischen Schwerpunktdisziplinen nachzuweisen, und zwar in einem für das konsekutive Masterstudium vorgegebenen Mindestumfang der Module:
    1.  Vermessungskunde
    2.  Referenz- und Raumbezugssysteme
    3.  Ausgleichungsrechnung
    4.  Photogrammetrie und Fernerkundung
    5.  Topographie und Kartographie
    6.  Ingenieurgeodäsie
    7.  Liegenschaftskataster und Grundbuch
    8.  Landentwicklung
    9.  Planung und Bodenordnung
    10.  Immobilienwertermittlung
    11.  Geoinformatik
    12.  Physikalische Geodäsie
    13.  Satellitenpositionierung

(2) Ausbildungsbehörde (§ 5 Absatz 1) ist der Landesbetrieb Landesvermessung und Geobasisinformation Brandenburg.

(3) Die Ausbildung gliedert sich in mehrere Ausbildungsabschnitte.
Die Dauer der Ausbildungsabschnitte, sonstige Vorschriften für die Ausbildung und der Ausbildungsplan sind in Anlage 7a geregelt.

(4) Die Prüfungsfächer nach § 17 Absatz 3 und § 18 Absatz 4, die fächerbezogenen Prüfungszeiten in der mündlichen Prüfung und das Prüfstoffverzeichnis sind in Anlage 7b und c geregelt.

(5) Die mit dem Staatsexamen erworbene Befähigung für den höheren technischen Verwaltungsdienst in der Fachrichtung Geodäsie und Geoinformation entspricht der Befähigung für den höheren vermessungstechnischen Verwaltungsdienstes im Sinne des Brandenburgischen Vermessungsgesetzes und des Brandenburgischen ÖbVI-Gesetzes.

#### § 29 Sondervorschriften der Fachrichtung Maschinen- und Elektrotechnik in der Verwaltung

(1) Zulassungsvoraussetzung für das technische Referendariat ist unter den Vorgaben von § 2 der erfolgreiche Abschluss eines wissenschaftlichen Studiums oder eine vergleichbare Kombination von Studiengängen an einer technischen Hochschule oder Universität oder an einer Hochschule mit gleichwertigem, wissenschaftlichem Studienangebot im Laufbahnzweig

1.  Maschinenbau,
2.  Elektrotechnik,
3.  Versorgungstechnik,
4.  Wirtschaftsingenieurwesen mit technischen Vertiefungen in den vorgenannten Fachrichtungen,
5.  oder auf der Basis von Mathematik, Physik, Chemie, Mechanik vergleichbare Studiengänge.

Einzelne Vorgaben zum Wissensspektrum (Studieninhalte) werden bei Bedarf festgelegt.

(2) Ausbildungsbehörde (§ 5 Absatz 1) ist der Brandenburgische Landesbetrieb für Liegenschaften und Bauen.

(3) Die Ausbildung gliedert sich in mehrere Ausbildungsabschnitte.
Die Dauer der Ausbildungsabschnitte, sonstige Vorschriften für die Ausbildung und der Ausbildungsplan sind in Anlage 8a geregelt.

(4) Die Prüfungsfächer nach § 17 Absatz 3 und § 18 Absatz 4, die fächerbezogenen Prüfungszeiten in der mündlichen Prüfung und das Prüfstoffverzeichnis sind in Anlage 8b und c geregelt.

#### § 30 Sondervorschriften der Fachrichtung Städtebau

(1) Zulassungsvoraussetzung für das technische Referendariat ist unter den Vorgaben von § 2 der erfolgreiche Abschluss eines wissenschaftlichen Studiums der Studiengänge Raumplanung mit Schwerpunkt Städtebau/Stadtplanung, Stadtplanung oder Stadt- und Regionalplanung.
Eine Zulassung für das technische Referendariat ist grundsätzlich nur dann möglich, wenn das nachfolgend aufgeführte Wissensspektrum (Studieninhalte) nachgewiesen wird:

1.  Ökonomische und soziologische Grundlagen einer nachhaltigen Stadt-, Regional- und Landesplanung:
    1.  Regionale Strukturpolitik
    2.  Soziologische Grundlagen
    3.  Einzel- und gesamtwirtschaftliche Grundlagen
    4.  Developer-Rechnung
    5.  Immobilienmärkte und Immobilienentwicklung
2.  Theorie und Kontext der räumlichen Planung:
    1.  Aufgaben der räumlichen Planung im gesellschaftlichen Kontext
    2.  Politische Entscheidungen und räumliche Steuerung
    3.  Politik und Verwaltung in Mehrebenensystemen
3.  Methoden, Verfahren und Instrumente der räumlichen Planung:
    1.  Methoden der Raumplanung
    2.  Verfahren und Instrumente (zur nachhaltigen Stadtentwicklung)
    3.  Management und Kommunikation
4.  Städtebaulicher Entwurf:
    1.  Städtebauliche Gestaltung und ihre Darstellung
    2.  Bebauungsplanung
    3.  Morphologie und Typologie
    4.  Visualisierung von Planungen
5.  Geschichte der Siedlungsentwicklung und des Städtebaus:
    1.  Geschichte der Siedlungsentwicklung und des Städtebaus in Stadt und Land
    2.  Denkmalpflege
6.  Rechtliche Grundlagen:
    1.  Allgemeines Verfassungsrecht
    2.  Allgemeines Verwaltungsrecht
    3.  Bau- und Planungsrecht
    4.  Raumordnungsrecht
    5.  Bodenrecht
    6.  Fachplanungsrecht
    7.  Besonderes Städtebaurecht (insbesondere Stadterneuerung)
    8.  Europäisches Raumplanungsrecht
7.  Natürliche Voraussetzungen und technische Elemente der Stadt-, Regional- und Landesplanung:
    1.  Grundlagen des Ökosystems
    2.  Landschaft und Umwelt
    3.  Umwelt und Ressourcen, unter anderem Energie
    4.  Verkehr und Mobilität, Logistik und Wirtschaftsverkehr
    5.  Immobilienmärkte und Immobilienentwicklung
    6.  Gebäudelehre
8.  Statistik und E-Planning:
    1.  Empirische Erhebungsmethoden
    2.  Qualitative und quantitative Methoden der Datenerhebung
    3.  Deskriptive Statistik
    4.  Internetgestützte Planungskommunikation

(2) Ausbildungsbehörde (§ 5 Absatz 1) ist das Landesamt für Bauen und Verkehr.

(3) Die Ausbildung gliedert sich in mehrere Ausbildungsabschnitte.
Die Dauer der Ausbildungsabschnitte, sonstige Vorschriften für die Ausbildung und der Ausbildungsplan sind in Anlage 9a geregelt.

(4) Die Prüfungsfächer nach § 17 Absatz 3 und § 18 Absatz 4, die fächerbezogenen Prüfungszeiten in der mündlichen Prüfung und das Prüfstoffverzeichnis sind in Anlage 9b und c geregelt.

#### § 31 Sondervorschriften der Fachrichtung Straßenwesen

(1) Zulassungsvoraussetzung für das technische Referendariat ist unter den Vorgaben von § 2 der erfolgreiche Abschluss eines wissenschaftlichen Studiums des Studienganges Bauingenieurwesen oder eines vergleichbaren Studienganges.
Eine Zulassung für das technische Referendariat ist grundsätzlich nur dann möglich, wenn das nachfolgend aufgeführte Wissensspektrum (Studieninhalte) nachgewiesen wird:

1.  Grundlegende Fachkenntnisse und die Befähigung zu dessen wissenschaftsmethodischer Anwendung sind in mindestens folgenden Fächern nachzuweisen:
    1.  Höhere Mathematik
    2.  Mechanik
    3.  Physik einschließlich der fachbezogenen Bereiche
    4.  Informatik
2.  Fachkenntnisse sowie die Fähigkeit zur Lösung von Fachaufgaben nach wissenschaftlichen Grundsätzen sind in den folgenden Schwerpunktdisziplinen nachzuweisen:
    1.  Grundbau und Bodenmechanik
    2.  Baustatik
    3.  Vermessungskunde
    4.  Baustoffkunde
    5.  Baukonstruktionslehre
    6.  Grundzüge des Konstruktiven Ingenieurbaus oder Stahlbau oder Massivbau
    7.  Straßenbau
    8.  Straßenentwurf
    9.  Grundzüge des Verkehrswesens
3.  Fachbezogenes Ergänzungswissen: Das Studium muss (zum Beispiel durch Wahlmodule) die Möglichkeit bieten, ergänzende Grundkenntnisse in folgenden Bereichen zu erwerben:
    1.  Führungstechnik/Management
    2.  Betriebswirtschaft
    3.  Rechtswissenschaft
    4.  Umweltschutz
    5.  Sprachen

(2) Ausbildungsbehörde (§ 5 Absatz 1) ist der Landesbetrieb Straßenwesen.

(3) Die Ausbildung gliedert sich in mehrere Ausbildungsabschnitte.
Die Dauer der Ausbildungsabschnitte, sonstige Vorschriften für die Ausbildung und der Ausbildungsplan sind in Anlage 10a geregelt.

(4) Die Prüfungsfächer nach § 17 Absatz 3 und § 18 Absatz 4, die fächerbezogenen Prüfungszeiten in der mündlichen Prüfung und das Prüfstoffverzeichnis sind in Anlage 10b und c geregelt.

Kapitel 2  
Aufstieg in die Laufbahn des höheren technischen Verwaltungsdienstes in der Fachrichtung Geodäsie und Geoinformation
--------------------------------------------------------------------------------------------------------------------------------

#### § 32 Geltungsbereich

Die Vorschriften dieses Kapitels regeln die Dauer und die Ausgestaltung der Ausbildung sowie die Beurteilung der Leistungen während des Aufstiegs aus der Laufbahn des gehobenen vermessungstechnischen Verwaltungsdienstes in die Laufbahn des höheren technischen Verwaltungsdienstes in der Fachrichtung Geodäsie und Geoinformation und die Aufstiegsprüfung.

#### § 33 Auswahlverfahren, Zulassung

(1) Für das Auswahlverfahren und die Zulassung für den Aufstieg gelten die Regelungen der Laufbahnverordnung mit folgenden Maßgaben:

1.  Das Auswahlverfahren wird durch die oberste Dienstbehörde durchgeführt.
2.  Teilzeitbeschäftigte können zum Aufstieg zugelassen werden.
3.  Beamtinnen und Beamte des gehobenen vermessungstechnischen Verwaltungsdienstes können mehrmals an einem Auswahlverfahren teilnehmen.
    Eine weitere Teilnahme ist frühestens nach drei Jahren möglich.

(2) Beamtinnen und Beamte des gehobenen vermessungstechnischen Verwaltungsdienstes können zum Aufstieg zugelassen werden, wenn sie

1.  geeignet sind,
2.  sich in einer laufbahnrechtlichen Dienstzeit von mindestens zehn Jahren seit der Ernennung zur Beamtin oder zum Beamten auf Lebenszeit im gehobenen vermessungstechnischen Dienst bewährt haben und
3.  mindestens ein Jahr ein Amt der Besoldungsgruppe A 12, Beamtinnen und Beamte der Gemeinden und Gemeindeverbände mindestens das erste Beförderungsamt, innehaben.

Die zum Aufstieg zugelassenen Personen werden nachfolgend als Aufstiegsbewerberinnen und Aufstiegsbewerber bezeichnet.

#### § 34 Einführung in die Aufgaben der neuen Laufbahn (Qualifizierung), Beurteilungen

(1) Nach der Zulassung zum Aufstieg werden die Aufstiegsbewerberinnen und Aufstiegsbewerber in die Aufgaben der Laufbahn des höheren technischen Verwaltungsdienstes in der Fachrichtung Geodäsie und Geoinformation eingeführt.
Die Einführung in die Aufgaben der neuen Laufbahn (Qualifizierung) dauert mindestens ein Jahr und sechs Monate.
Sie erfolgt auf mindestens zwei Dienstposten in unterschiedlichen Aufgabenbereichen, wobei die Dauer von sechs Monaten je Dienstposten nicht unterschritten werden soll.
Bei Teilzeitbeschäftigten sind die Zeiten entsprechend zu verlängern.

(2) Die oberste Dienstbehörde hat in Abstimmung mit der Aufstiegsbewerberin oder dem Aufstiegsbewerber und unter Berücksichtigung ihrer oder seiner bisherigen Tätigkeiten auf der Grundlage eines von der Laufbahnordnungsbehörde erlassenen Rahmenqualifizierungsplanes einen individuellen Qualifizierungsplan aufzustellen.
In diesem Qualifizierungsplan sind die vorgesehenen Dienstposten zu beschreiben und die Behörden, bei denen diese Dienstposten eingerichtet sind (Qualifizierungsbehörden), zu benennen.
Die Teilnahme an den Seminaren und Lehrgängen des technischen Referendariats in der Fachrichtung Geodäsie und Geoinformation ist im Qualifizierungsplan vorzusehen.

(3) Die Umsetzung des Qualifizierungsplans erfolgt auf der Grundlage einer öffentlich-rechtlichen Vereinbarung zwischen der obersten Dienstbehörde, den Qualifizierungsbehörden und der Ausbildungsbehörde nach § 28 Absatz 2.
Die Vereinbarung ist der Laufbahnordnungsbehörde vor Beginn der Qualifizierung zur Genehmigung vorzulegen.

(4) Die oberste Dienstbehörde bestellt zur Aufstiegsbegleiterin oder zum Aufstiegsbegleiter eine geeignete Bedienstete oder einen geeigneten Bediensteten.
Die Aufstiegsbegleiterin oder der Aufstiegsbegleiter überwacht die gesamte Qualifizierungsphase.
Die Betreuung der Aufstiegsbewerberinnen und Aufstiegsbewerber obliegt jeweils der Leiterin oder dem Leiter der Qualifizierungsbehörde oder einer von ihr oder ihm beauftragten Person (Qualifizierungsbetreuerin oder Qualifizierungsbetreuer).
Aufstiegsbegleiterin oder Aufstiegsbegleiter und Qualifizierungsbetreuerin oder Qualifizierungsbetreuer müssen die Befähigung für den höheren technischen Verwaltungsdienst in der Fachrichtung Geodäsie und Geoinformation besitzen.

(5) Die Aufstiegsbewerberinnen und Aufstiegsbewerber haben einen Qualifizierungsnachweis zu führen und darin eine Übersicht über ihre wesentlichen Tätigkeiten zu geben.
Der Nachweis ist quartalsweise der Qualifizierungsbetreuerin oder dem Qualifizierungsbetreuer und anschließend der Aufstiegsbegleiterin oder dem Aufstiegsbegleiter vorzulegen.

(6) Jede Qualifizierungsbehörde beurteilt die Aufstiegsbewerberinnen und Aufstiegsbewerber nach Abschluss des bei ihr abgeleisteten Qualifizierungsabschnittes unter Angabe der Art und Dauer der Beschäftigung nach ihren Leistungen (Arbeitsgüte, Arbeitsmenge, Arbeitsweise, Führungsverhalten) und Befähigungen (Denk- und Urteilsvermögen, Organisationsvermögen, Befähigung zur Kommunikation und Zusammenarbeit, Führungsfähigkeit).
Die Beurteilung muss erkennen lassen, ob das Ziel des Qualifizierungsabschnittes erreicht ist.
Besondere Fähigkeiten oder Mängel sind zu vermerken.

(7) Die Beurteilung nach Absatz 6 ist den Aufstiegsbewerberinnen und Aufstiegsbewerbern zu eröffnen und mit ihnen zu besprechen.
Die Eröffnung ist aktenkundig zu machen und mit der Beurteilung zu den Qualifizierungsakten zu nehmen.

(8) Hält die oberste Dienstbehörde die Qualifizierung für erfolgreich abgeschlossen, ist die Aufstiegsprüfung abzulegen.

#### § 35 Prüfungsausschuss, Prüfungskommission

(1) Für die Abnahme der Aufstiegsprüfung ist ein Prüfungsausschuss einzurichten.
Der Prüfungsausschuss besteht aus der oder dem Vorsitzenden, der oder dem stellvertretenden Vorsitzenden und mindestens drei weiteren Mitgliedern.
Alle müssen die Befähigung für den höheren technischen Verwaltungsdienst in der Fachrichtung Geodäsie und Geoinformation besitzen.
Sie sollen eine langjährige Berufserfahrung vorweisen.
Die Mitglieder des Prüfungsausschusses werden von der für das Vermessungswesen zuständigen obersten Landesbehörde im Einvernehmen mit der für die Flurneuordnung zuständigen obersten Landesbehörde für die Dauer von fünf Jahren berufen.
Eine wiederholte Berufung ist zulässig.

(2) Der Prüfungsausschuss bedient sich einer Geschäftsstelle, die beim Landesbetrieb Landesvermessung und Geobasisinformation Brandenburg eingerichtet ist.

(3) Die Aufstiegsprüfung wird von einer Prüfungskommission abgenommen, deren Zusammensetzung von der oder dem Vorsitzenden des Prüfungsausschusses festgelegt wird.
Eine Prüfungskommission besteht aus der oder dem Vorsitzenden und mindestens zwei weiteren Prüferinnen oder Prüfern, wobei die Besetzung der Prüfungskommission je Prüfungsfach personell wechseln kann.
Die oder der Vorsitzende ist vom Wechsel ausgenommen.
Die oder der Vorsitzende des Prüfungsausschusses bestimmt, wer den Vorsitz in der Prüfungskommission führt.
§ 13 Absatz 5 gilt entsprechend.

(4) Die Prüfungskommission ist beschlussfähig, wenn die oder der Vorsitzende und zwei weitere Prüferinnen oder Prüfer anwesend sind.
Die Prüfungskommission entscheidet mit Stimmenmehrheit.
Bei Stimmengleichheit gibt die Stimme der oder des Vorsitzenden den Ausschlag.
Stimmenthaltung ist nicht zulässig.

#### § 36 Aufstiegsprüfung

(1) Die Anmeldung zur Aufstiegsprüfung ist von der obersten Dienstbehörde unmittelbar nach Abschluss der Qualifizierung bei der Geschäftsstelle des Prüfungsausschusses einzureichen.
Der Anmeldung sind neben der Qualifizierungsakte der Qualifizierungsplan gemäß § 34 Absatz 2, der Qualifizierungsnachweis gemäß § 34 Absatz 5 und die Beurteilungen gemäß § 34 Absatz 6 beizufügen.

(2) Die Aufstiegsbewerberin oder der Aufstiegsbewerber wird zur Aufstiegsprüfung von der Geschäftsstelle des Prüfungsausschusses schriftlich geladen.

(3) Die Aufstiegsprüfung besteht aus Prüfungsgesprächen in den Prüfungsfächern

1.  Allgemeine Rechts- und Verwaltungsgrundlagen, Führungsaufgaben und Wirtschaftlichkeit,
2.  Liegenschaftskataster und Landesvermessung, Geobasisinformationssystem, Geodatenmanagement und Geodateninfrastruktur,
3.  Landentwicklung, Landesplanung und Städtebau,

deren Prüfstoff sich unter Berücksichtigung der Einsatzgebiete während der Qualifizierung an dem Prüfstoffverzeichnis des Staatsexamens in der Fachrichtung Geodäsie und Geoinformation (Anlage 7c) orientiert.
Die Prüfungsdauer beträgt je Prüfungsfach 45 Minuten.
Die Prüfungskommission kann die Prüfungsdauer um bis zu 15 Minuten je Prüfungsfach verlängern, wenn dies zur Beurteilung der Leistungen der Aufstiegsbewerberin oder des Aufstiegsbewerbers notwendig ist.

(4) Die Aufstiegsprüfung und die Beratung der Prüfungskommission sind nicht öffentlich.
Während der Aufstiegsprüfung, nicht dagegen bei der Bewertung der Prüfungsgespräche, können nach vorheriger Absprache mit der Geschäftsstelle des Prüfungsausschusses Beauftragte der obersten Dienstbehörde der Aufstiegsbewerberin oder des Aufstiegsbewerbers teilnehmen.

(5) Über den Prüfungshergang ist eine Niederschrift anzufertigen, in der die Besetzung der Prüfungskommission, der Name der Aufstiegsbewerberin oder des Aufstiegsbewerbers, die Bewertungen der Prüfungsgespräche und das Gesamturteil festgehalten werden.
Die Niederschrift ist von der oder dem Vorsitzenden der Prüfungskommission und den an der Aufstiegsprüfung beteiligten Prüferinnen und Prüfern zu unterzeichnen.
Sie ist Bestandteil der Prüfungsakten.

#### § 37 Unterbrechung, Rücktritt

(1) Kann die Aufstiegsbewerberin oder der Aufstiegsbewerber nicht zur Aufstiegsprüfung erscheinen oder muss sie oder er die Aufstiegsprüfung abbrechen, so ist unverzüglich die Geschäftsstelle des Prüfungsausschusses unter Angabe des Grundes zu verständigen und der Nachweis der Verhinderung zu erbringen.
Erkennt die oder der Vorsitzende des Prüfungsausschusses den Grund als triftig an, so gelten bei einer Unterbrechung die bis dahin abgeschlossenen Prüfungsgespräche als abgelegt.
Die Entscheidung der oder des Vorsitzenden zur Anerkennung beziehungsweise Nichtanerkennung des Grundes ist schriftlich festzuhalten.
Sie ist Bestandteil der Prüfungsakte.
Die Aufstiegsprüfung ist zum nächstmöglichen Termin fortzusetzen.

(2) Absatz 1 gilt entsprechend, wenn die Aufstiegsbewerberin oder der Aufstiegsbewerber bei Vorliegen eines triftigen Grundes mit Zustimmung der Geschäftsstelle des Prüfungsausschusses von der Aufstiegsprüfung zurücktritt.

#### § 38 Bewertung, Gesamturteil

(1) Die Bewertung der Prüfungsgespräche in den Prüfungsfächern erfolgt durch die jeweilige Prüfungskommission unter Berücksichtigung der Beurteilungen nach § 34 Absatz 6.

(2) Die Aufstiegsprüfung ist bestanden, wenn alle Prüfungsgespräche als bestanden bewertet werden.
Sie ist nicht bestanden, wenn mindestens ein Prüfungsgespräch als nicht bestanden bewertet wird.

(3) Die Aufstiegsprüfung gilt als nicht bestanden, wenn die Aufstiegsbewerberin oder der Aufstiegsbewerber ohne von der oder dem Vorsitzenden des Prüfungsausschusses anerkannten Grund nicht erscheint oder die Aufstiegsprüfung abbricht (§ 37 Absatz 1).

(4) Im Anschluss an die Aufstiegsprüfung gibt die oder der Vorsitzende der Prüfungskommission der Aufstiegsbewerberin oder dem Aufstiegsbewerber das Ergebnis der Aufstiegsprüfung bekannt.
Über das Bestehen oder Nichtbestehen der Aufstiegsprüfung erhält die Aufstiegsbewerberin oder der Aufstiegsbewerber von der Geschäftsstelle des Prüfungsausschusses einen Bescheid mit Rechtsbehelfsbelehrung.

(5) Mit Bestehen der Aufstiegsprüfung erwirbt die Aufstiegsbewerberin oder der Aufstiegsbewerber die Befähigung für den höheren technischen Verwaltungsdienst in der Fachrichtung Geodäsie und Geoinformation.
§ 28 Absatz 5 gilt entsprechend.

#### § 39 Wiederholung

(1) Hat die Aufstiegsbewerberin oder der Aufstiegsbewerber die Aufstiegsprüfung nicht bestanden, so kann sie oder er die Aufstiegsprüfung einmal wiederholen.
Sofern die Mehrzahl der Prüfungsfächer der Aufstiegsprüfung als bestanden bewertet wurde, werden diese für die Wiederholung der Aufstiegsprüfung anerkannt.

(2) Die Prüfungskommission befindet darüber, in welchen Abschnitten die Qualifizierung einer Ergänzung bedarf und schlägt der obersten Dienstbehörde die Dauer der zusätzlichen Qualifizierung vor.
Sie soll mindestens zwei, höchstens sechs Monate betragen.
Die oberste Dienstbehörde hat unmittelbar nach Beendigung der zusätzlichen Qualifizierung die Anmeldung zur Wiederholung der Aufstiegsprüfung einzureichen.

#### § 40 Prüfungsakte

(1) Wer an der Aufstiegsprüfung teilgenommen hat, kann auf Antrag seine Prüfungsakte in der Geschäftsstelle des Prüfungsausschusses einsehen.
Der Antrag nach Satz 1 ist schriftlich zu stellen.

(2) Nach fünf Jahren wird die Prüfungsakte vernichtet.

Kapitel 3  
Übergangs- und Schlussvorschriften
----------------------------------------------

#### § 41 Übergangsbestimmungen

(1) Für Referendarinnen und Referendare, die sich zum Zeitpunkt des Inkrafttretens dieser Verordnung bereits im Vorbereitungsdienst befinden, ist grundsätzlich die bisher gültige Ausbildungs- und Prüfungsordnung anzuwenden.
Auf Antrag der Referendarin oder des Referendars kann die Ausbildungsbehörde im Benehmen mit der Einstellungsbehörde und im Einvernehmen mit der Direktorin oder dem Direktor des Oberprüfungsamtes die neue Ausbildungs- und Prüfungsordnung anwenden, wenn sich alle in den Sondervorschriften der Fachrichtungen aufgeführten Ausbildungsabschnitte und Ausbildungsinhalte noch in den Ausbildungsplan (§ 8 Absatz 2) integrieren lassen.

(2) Für Aufstiegsbewerberinnen und Aufstiegsbewerber, die sich zum Zeitpunkt des Inkrafttretens dieser Verordnung bereits im Aufstieg befinden, ist die bisher gültige Regelung anzuwenden.

#### § 42 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über die Ausbildung und Prüfung für die Laufbahnen des höheren technischen Verwaltungsdienstes im Land Brandenburg vom 29.
März 2001 (GVBl.
II S.
90), die zuletzt durch Artikel 26 des Gesetzes vom 13. März 2012 (GVBl.
I Nr.
16 S.
10) geändert worden ist, außer Kraft.

Potsdam, den 9.
Oktober 2018

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter

Der Minister für Finanzen

Christian Görke

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider

* * *

### Anlagen

1

[Anlage 1 (zu § 8 Absatz 4) - Ausbildungsnachweis](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-1.pdf "Anlage 1 (zu § 8 Absatz 4) - Ausbildungsnachweis") 157.6 KB

2

[Anlage 2 (zu § 8 Absatz 5) - Übersicht über das technische Referendariat](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-2.pdf "Anlage 2 (zu § 8 Absatz 5) - Übersicht über das technische Referendariat") 154.4 KB

3

[Anlage 3 (zu § 9 Absatz 1) - Beurteilung](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-3.pdf "Anlage 3 (zu § 9 Absatz 1) - Beurteilung") 253.0 KB

4

[Anlage 4 (zu § 14 Absatz 2) - Antrag auf Zulassung zum Staatsexamen](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-4.pdf "Anlage 4 (zu § 14 Absatz 2) - Antrag auf Zulassung zum Staatsexamen") 155.9 KB

5

[Anlage 5 (zu § 22 Absatz 1) - Prüfungszeugnis (Muster)](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-5.pdf "Anlage 5 (zu § 22 Absatz 1) - Prüfungszeugnis (Muster)") 190.7 KB

6

[Anlage 6a (zu § 27 Absatz 3) - Gliederung der Ausbildung in der Fachrichtung Architektur; Ausbildungsplan](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-6a.pdf "Anlage 6a (zu § 27 Absatz 3) - Gliederung der Ausbildung in der Fachrichtung Architektur; Ausbildungsplan") 225.4 KB

7

[Anlage 6b (zu § 27 Absatz 4) - Prüfungsfächer und Prüfungszeiten in der Fachrichtung Architektur](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-6b.pdf "Anlage 6b (zu § 27 Absatz 4) - Prüfungsfächer und Prüfungszeiten in der Fachrichtung Architektur") 142.3 KB

8

[Anlage 6c (zu § 27 Absatz 4) - Prüfstoffverzeichnis in der Fachrichtung Architektur](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-6c.pdf "Anlage 6c (zu § 27 Absatz 4) - Prüfstoffverzeichnis in der Fachrichtung Architektur") 898.3 KB

9

[Anlage 7a (zu § 28 Absatz 3) - Gliederung der Ausbildung in der Fachrichtung Geodäsie und Geoinformation; Ausbildungsplan](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-7a.pdf "Anlage 7a (zu § 28 Absatz 3) - Gliederung der Ausbildung in der Fachrichtung Geodäsie und Geoinformation; Ausbildungsplan") 860.6 KB

10

[Anlage 7b (zu § 28 Absatz 4) - Prüfungsfächer und Prüfungszeiten in der Fachrichtung Geodäsie und Geoinformation](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-7b.pdf "Anlage 7b (zu § 28 Absatz 4) - Prüfungsfächer und Prüfungszeiten in der Fachrichtung Geodäsie und Geoinformation") 622.3 KB

11

[Anlage 7c (zu § 28 Absatz 4) - Prüfstoffverzeichnis in der Fachrichtung Geodäsie und Geoinformation](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-7c.pdf "Anlage 7c (zu § 28 Absatz 4) - Prüfstoffverzeichnis in der Fachrichtung Geodäsie und Geoinformation") 958.9 KB

12

[Anlage 8a (zu § 29 Absatz 3) - Gliederung der Ausbildung in der Fachrichtung Maschinen- und Elektrotechnik in der Verwaltung; Ausbildungsplan](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-8a.pdf "Anlage 8a (zu § 29 Absatz 3) - Gliederung der Ausbildung in der Fachrichtung Maschinen- und Elektrotechnik in der Verwaltung; Ausbildungsplan") 690.4 KB

13

[Anlage 8b (zu § 29 Absatz 4) - Prüfungsfächer und Prüfungszeiten in der Fachrichtung Maschinen- und Elektrotechnik in der Verwaltung](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-8b.pdf "Anlage 8b (zu § 29 Absatz 4) - Prüfungsfächer und Prüfungszeiten in der Fachrichtung Maschinen- und Elektrotechnik in der Verwaltung") 622.9 KB

14

[Anlage 8c (zu § 29 Absatz 4) - Prüfstoffverzeichnis in der Fachrichtung Maschinen- und Elektrotechnik in der Verwaltung](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-8c.pdf "Anlage 8c (zu § 29 Absatz 4) - Prüfstoffverzeichnis in der Fachrichtung Maschinen- und Elektrotechnik in der Verwaltung") 810.5 KB

15

[Anlage 9a (zu § 30 Absatz 3) - Gliederung der Ausbildung in der Fachrichtung Städtebau; Ausbildungsplan](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-9a.pdf "Anlage 9a (zu § 30 Absatz 3) - Gliederung der Ausbildung in der Fachrichtung Städtebau; Ausbildungsplan") 681.8 KB

16

[Anlage 9b (zu § 30 Absatz 4) - Prüfungsfächer und Prüfungszeiten in der Fachrichtung Städtebau](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-9b.pdf "Anlage 9b (zu § 30 Absatz 4) - Prüfungsfächer und Prüfungszeiten in der Fachrichtung Städtebau") 621.4 KB

17

[Anlage 9c (zu § 30 Absatz 4) - Prüfstoffverzeichnis in der Fachrichtung Städtebau](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-9c.pdf "Anlage 9c (zu § 30 Absatz 4) - Prüfstoffverzeichnis in der Fachrichtung Städtebau") 841.6 KB

18

[Anlage 10a (zu § 31 Absatz 3) - Gliederung der Ausbildung in der Fachrichtung Straßenwesen; Ausbildungsplan](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-10a.pdf "Anlage 10a (zu § 31 Absatz 3) - Gliederung der Ausbildung in der Fachrichtung Straßenwesen; Ausbildungsplan") 801.0 KB

19

[Anlage 10b (zu § 31 Absatz 4) - Prüfungsfächer und Prüfungszeiten in der Fachrichtung Straßenwesen](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-10b.pdf "Anlage 10b (zu § 31 Absatz 4) - Prüfungsfächer und Prüfungszeiten in der Fachrichtung Straßenwesen") 622.6 KB

20

[Anlage 10c (zu § 31 Absatz 4) - Prüfstoffverzeichnis in der Fachrichtung Straßenwesen](/br2/sixcms/media.php/68/GVBl_II_68_2018-Anlage-10c.pdf "Anlage 10c (zu § 31 Absatz 4) - Prüfstoffverzeichnis in der Fachrichtung Straßenwesen") 863.4 KB