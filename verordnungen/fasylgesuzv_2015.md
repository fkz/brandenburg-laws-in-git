## Verordnung zur Übertragung der Zuständigkeit zur Bestimmung der Ärztin oder des Arztes für Gesundheitsuntersuchungen nach § 62 Absatz 1 Satz 2 des Asylverfahrensgesetzes im Rahmen des Flughafenasylverfahrens (Flughafen-Asyl-Gesundheitsuntersuchungs-Zuständigkeitsverordnung - FAsylGesuZV)

Auf Grund des § 6 Absatz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 14 S. 2) geändert worden ist, des § 2 Absatz 3 Satz 1 der Kommunalverfassung des Landes Brandenburg vom 18.
Dezember 2007 (GVBl.
I S. 286) in Verbindung mit § 62 Absatz 1 Satz 2 des Asylverfahrensgesetzes in der Fassung der Bekanntmachung vom 2.
September 2008 (BGBl.
I S. 1798) verordnet die Ministerin für Arbeit, Soziales, Gesundheit, Frauen und Familie:

#### § 1 Übertragung der Zuständigkeit bei Einreise auf dem Luftweg

(1) Die Zuständigkeit für die Bestimmung der Ärztin oder des Arztes, die oder der die ärztlichen Untersuchungen auf übertragbare Krankheiten einschließlich einer Röntgenaufnahme der Atmungsorgane durchführt, wird nach § 62 Absatz 1 Satz 2 des Asylverfahrensgesetzes auf den Landkreis Dahme-Spreewald übertragen.

(2) Die Gesundheitsuntersuchung von Asylsuchenden nach § 62 des Asylverfahrensgesetzes erfolgt bei Einreise auf dem Luftweg im Sinne des § 18a des Asylverfahrensgesetzes grundsätzlich vor der landesinternen Verteilung nach § 50 Absatz 1 des Asylverfahrensgesetzes am Flughafen Berlin-Schönefeld im Landkreis Dahme-Spreewald.

(3) Die Aufgabe nach Absatz 2 wird dem Landkreis Dahme-Spreewald als Pflichtaufgabe zur Erfüllung nach Weisung übertragen.
Sonderaufsichtsbehörde über den Landkreis als Träger des Öffentlichen Gesundheitsdienstes ist das für Gesundheit zuständige Ministerium.
Für die Sonderaufsichtsbehörde ist § 121 Absatz 2 bis 4 der Kommunalverfassung des Landes Brandenburg anwendbar.
Das Recht, besondere Weisungen zu erteilen, ist nicht auf den Bereich der Gefahrenabwehr beschränkt.
Einzelanweisungen sind zulässig.

(4) Die Zuständigkeiten nach dem Infektionsschutzgesetz bleiben von diesen Regelungen unberührt.

#### § 2 Kosten

Das Land erstattet dem Landkreis Dahme-Spreewald die aus der Aufgabenübertragung entstehenden Kosten durch eine Kostenpauschale.
Insbesondere sind dies die Kosten der Untersuchungen auf übertragbare Krankheiten einschließlich einer Röntgenaufnahme der Atmungsorgane.
Die Kostenpauschale beträgt 138 Euro pro Untersuchung.
Das Land überprüft regelmäßig, spätestens jedoch alle zwei Jahre, ob die Kostenpauschale nach Satz 1 geeignet ist, die Kosten der Untersuchungen vollständig auszugleichen.
Sind auf Grund klinischer oder epidemiologischer Anhaltspunkte in Abstimmung mit dem Land weitere Untersuchungen erforderlich, werden die die Kostenpauschale übersteigenden Mehrkosten auf Antrag erstattet.

#### § 3 Inkrafttreten

Diese Verordnung tritt am 1.
Juli 2015 in Kraft.

Potsdam, den 11.
Juni 2015

Die Ministerin für Arbeit, Soziales,  
Gesundheit, Frauen und Familie

Diana Golze