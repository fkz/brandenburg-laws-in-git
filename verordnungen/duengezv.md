## Verordnung über die Zuständigkeiten auf dem Gebiet des Düngerechts (Dünge-Zuständigkeitsverordnung - DüngeZV)

Auf Grund des § 6 Absatz 2 und des § 12 Absatz 1 des Landesorganisationsgesetzes vom 24. Mai 2004 (GVBl.
I S. 186), von denen durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl. I Nr. 28 S.
2) § 6 Absatz 2 geändert und § 12 Absatz 1 eingefügt worden ist, und des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19. Februar 1987 (BGBl.
I S.
602) verordnet die Landesregierung und auf Grund des Artikels 7 des Landwirtschaftsstaatsvertrages in Verbindung mit § 1 des Gesetzes zu dem Landwirtschaftsstaatsvertrag vom 20.
April 2004 (GVBl.
I S.
165) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft im Benehmen mit der Senatsverwaltung für Justiz, Verbraucherschutz und Antidiskriminierung des Landes Berlin:

#### § 1 Zuständige Behörden

(1) Für die Überwachung der Einhaltung des Düngerechtes im Sinne von § 12 Absatz 1 des Düngegesetzes vom 9.
Januar 2009 (BGBl.
I S.
54, 136), das zuletzt durch Artikel 1 des Gesetzes vom 5.
Mai 2017 (BGBl.
I S.
1068) geändert worden ist, und für die Erteilung behördlicher Anordnungen nach § 13 des Düngegesetzes sind, soweit nicht in Absatz 4 eine andere Zuständigkeit bestimmt ist, die Landkreise und kreisfreien Städte auf dem Gebiet des Landes Brandenburg und das Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung auf dem Gebiet des Landes Berlin zuständig.

(2) Die Landkreise und kreisfreien Städte nehmen diese Aufgaben nach Absatz 1 als Pflichtaufgaben zur Erfüllung nach Weisung wahr.

(3) Die Aufsicht führt das für Landwirtschaft zuständige Ministerium.
Für die Durchführung der Aufsicht über die Landkreise und kreisfreien Städte gilt § 121 der Kommunalverfassung des Landes Brandenburg.

(4) Für den Vollzug der nachstehend genannten Regelungen im Land Brandenburg ist ausschließlich das Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung zuständig:

1.  Überwachung des Inverkehrbringens von Stoffen nach § 2 Nummer 10 in Verbindung mit § 5 Absatz 1 und nach § 6 des Düngegesetzes,
2.  Erteilung von behördlichen Anordnungen nach § 13 Satz 2 Nummer 2 bis 4 des Düngegesetzes,
3.  Anerkennung, Überwachung und Aufhebung der Anerkennung der Träger der Qualitätssicherung nach § 13a des Düngegesetzes,
4.  Bereitstellung von Daten zur Ermittlung der Nährstoffgehalte gemäß § 3 Absatz 4 Nummer 2 der Düngeverordnung vom 26.
    Mai 2017 (BGBl.
    I S.
    1305),
5.  Bereitstellung der anzusetzenden Werte für die Ausnutzung des Stickstoffs im Jahr des Aufbringens gemäß § 3 Absatz 5 Satz 2 der Düngeverordnung,
6.  Herausgabe von Stickstoffbedarfswerten nach § 4 Absatz 1 Satz 5 der Düngeverordnung für Kulturen, die nicht in Anlage 4 Tabelle 1 bis 7 der Düngeverordnung genannt sind,
7.  Zulassung anderer Methoden oder Verfahren zur Ermittlung des Düngebedarfs nach § 4 Absatz 1 Satz 3 der Düngeverordnung,
8.  Herausgabe von Empfehlungen über die im Boden verfügbaren Stickstoffmengen nach § 4 Absatz 4 Satz 1 Nummer 1 Buchstabe b der Düngeverordnung,
9.  Erlass von Vorgaben für die Probennahme und Untersuchung der im Boden verfügbaren Nährstoffmengen nach § 4 Absatz 4 Satz 3 der Düngeverordnung,
10.  Genehmigung anderer als bodennaher Verfahren für die Aufbringung von flüssigen organischen und flüssigen organisch-mineralischen Düngemitteln einschließlich flüssiger Wirtschaftsdünger nach § 6 Absatz 3 Satz 3 und 4 der Düngeverordnung,
11.  Herausgabe von Stickstoff- und Phosphatgehalten nach § 8 Absatz 2 Satz 2 und 3 der Düngeverordnung für Kulturen, die nicht in Anlage 7 Tabelle 1 bis 3 der Düngeverordnung genannt sind,
12.  Herausgabe von Werten für die Gehalte an Stickstoff, Phosphor oder Phosphat für die Ermittlung der dem Betrieb zugeführten Nährstoffmengen gemäß § 4 Absatz 2 Satz 3 und 4 der Stoffstrombilanzverordnung vom 14.
    Dezember 2017 (BGBl.
    I S.
    3942; 2018 I S.
    360) und der von dem Betrieb abgegebenen Nährstoffmengen gemäß § 5 Absatz 2 Satz 2 und 3 der Stoffstrombilanzverordnung im Falle von Stoffen oder Tierarten, die nicht in den in diesen Vorschriften genannten Anlagen erfasst sind,
13.  Herausgabe von Vorgaben zur Berücksichtigung unvermeidlicher Verluste und erforderlicher Zuschläge in den Fällen des § 8 Absatz 5 Satz 1 der Düngeverordnung und des § 6 Absatz 2 Satz 3 der Stoffstrombilanzverordnung sowie Abstimmung im Einzelfall,
14.  Beratung nach § 9 Absatz 4 der Düngeverordnung und des § 6 Absatz 5 der Stoffstrombilanzverordnung und Anerkennung der Beratung,
15.  Übermittlung der nach § 4 Absatz 2 der Verordnung über das Inverkehrbringen und Befördern von Wirtschaftsdünger vom 21.
    Juli 2010 (BGBl.
    I S.
    1062), die zuletzt durch Artikel 2 der Verordnung vom 26.
    Mai 2017 (BGBl.
    I S.
    1305, 1348) geändert worden ist, gemeldeten Stoffe bis zum 31.
    Mai eines jeden Jahres an das nach Absatz 3 aufsichtführende Ministerium.

#### § 2 Zuständigkeit für die Verfolgung und Ahndung von Ordnungswidrigkeiten

Zuständige Behörden für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 14 des Düngegesetzes und der aufgrund dieses Gesetzes erlassenen Rechtsverordnungen sind die in § 1 genannten Behörden.

#### § 3 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2018 in Kraft.
Gleichzeitig tritt die Verordnung über die Zuständigkeiten auf dem Gebiet des Düngerechts vom 26.
November 2009 (GVBl.
II Nr.
44) außer Kraft.

Potsdam, den 22.
Januar 2019

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger