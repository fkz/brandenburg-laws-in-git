## Siebente Verordnung zur Aufhebung von Baubeschränkungsgebieten

Auf Grund des § 107 Absatz 4 des Bundesberggesetzes vom 13.
August 1980 (BGBl. I S. 1310), der mit der Maßgabe der Anlage I Kapitel V Sachgebiet D Abschnitt III Nummer 1 Buchstabe i des Einigungsvertrages vom 31.
August 1990 in Verbindung mit Artikel 1 des Gesetzes vom 23. September 1990 (BGBl. II S. 885, 1003) im Beitrittsgebiet gilt, verordnet die Landesregierung:

#### § 1 Aufhebung Baubeschränkungsgebiet

(1) Das im Landkreis Märkisch-Oderland gelegene und in der Anlage zu dieser Verordnung dargestellte Baubeschränkungsgebiet „Sandlagerstätte Wildermann“ wird aufgehoben.

(2) Die Karten und Pläne, die Bestandteil der Aufhebung des unter § 1 genannten Baubeschränkungsgebietes sind, werden gemäß § 107 Absatz 2 des Bundesberggesetzes zu jedermanns Einsicht archivmäßig gesichert beim Landesamt für Bergbau, Geologie und Rohstoffe niedergelegt.

#### § 2 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 6.
April 2016

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister für Wirtschaft und Energie

Albrecht Gerber

### Anlagen

1

[Anlage (zu § 1 Absatz 1) - Übersichtskarte zur Aufhebung des Baubeschränkungsgebietes "Sandlagerstätte Wildermann"](/br2/sixcms/media.php/68/GVBl_II_19_2016-Anlage.pdf "Anlage (zu § 1 Absatz 1) - Übersichtskarte zur Aufhebung des Baubeschränkungsgebietes ") 2.1 MB