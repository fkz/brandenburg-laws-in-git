## Verordnung über die Schiedsstelle nach § 36 des Pflegeberufegesetzes (Pflegeberufe-Schiedsstellenverordnung - PflBSchV)

Auf Grund des § 36 Absatz 5 des Pflegeberufegesetzes vom 17.
Juli 2017 (BGBl. I S. 2581) in Verbindung mit § 1 der Pflegeberufezuständigkeitsübertragungsverordnung vom 22. Januar 2019 (GVBI. II Nr. 8) verordnet die Ministerin für Arbeit, Soziales, Gesundheit, Frauen und Familie:

#### § 1 Zuständigkeit und Zusammensetzung der Schiedsstelle nach §&nbsp;36 des Pflegeberufegesetzes

(1) Die Schiedsstelle nach § 36 des Pflegeberufegesetzes ist zuständig für Entscheidungen über

1.  die Festlegung von Pauschalbudgets nach § 30 Absatz 2 des Pflegeberufegesetzes,
2.  die Festlegung von Individualbudgets nach § 31 Absatz 3 des Pflegeberufegesetzes,
3.  die Festlegung von Verfahrensregelungen im Zusammenhang mit der Einzahlung von Finanzierungsmitteln und den in Rechnung zu stellenden Zuschlägen nach § 33 Absatz 6 des Pflegeberufegesetzes.

(2) Die Schiedsstelle nach § 36 des Pflegeberufegesetzes besteht nach § 36 Absatz 2 des Pflegeberufegesetzes aus einer oder einem neutralen Vorsitzenden, aus drei die Kranken- und Pflegekassen vertretenden Personen, aus zwei die Krankenhäuser vertretenden Personen, aus einer die ambulanten Pflegedienste vertretenden Person und aus einer die stationären Pflegeeinrichtungen vertretenden Person sowie aus einer das Land vertretenden Person.
Der Schiedsstelle nach § 36 des Pflegeberufegesetzes gehört auch eine Person an, die vom Landesausschuss des Verbandes der Privaten Krankenversicherung bestellt wird und auf die Zahl der die Kranken- und Pflegekassen vertretenden Personen angerechnet wird.

(3) Bei Schiedsverfahren zu den Pauschalen der Pflegeschulen nach § 30 des Pflegeberufegesetzes oder den individuellen Ausbildungsbudgets der Pflegeschulen nach § 31 des Pflegeberufegesetzes gilt die Zusammensetzung nach § 36 Absatz 3 des Pflegeberufegesetzes.

(4) Das vorsitzende Mitglied hat eine Stellvertretung.
Die übrigen Mitglieder nach Absatz 2 und 3 haben jeweils mindestens zwei Stellvertretungen.
Bei Verhinderung des Mitglieds nimmt eine Stellvertretung dessen Rechte und Pflichten wahr.

#### § 2 Bestellung der Mitglieder

(1) Die Geschäftsstelle nach § 6 fordert spätestens zwei Monate vor Beginn der neuen Amtsperiode der Schiedsstelle nach § 36 des Pflegeberufegesetzes die Organisationen nach Absatz 3 Nummer 1 bis 6 auf, die sie vertretenden Personen und Stellvertretungen zu bestellen.
Gleichzeitig sind die beteiligten Organisationen aufzufordern, Kandidatinnen und Kandidaten für das Amt des vorsitzenden Mitglieds und seine Stellvertretung zu benennen.
Im Falle der vorzeitigen Amtsniederlegung hat die Geschäftsstelle zu veranlassen, dass innerhalb von zwei Monaten nach Zugang der Erklärung nach § 4 Absatz 4 die Neubestellung erfolgt.

(2) Das vorsitzende Mitglied und seine Stellvertretung werden von den beteiligten Organisationen nach Absatz 3 Nummer 1 bis 6 gemeinsam bestellt; kommt eine Einigung nicht zustande, entscheidet das Los.

(3) Die übrigen Mitglieder und ihre Stellvertretungen werden bestellt:

1.  für die Kranken- und Pflegekassen von den Landesverbänden der Kranken- und Pflegekassen aufgrund einer zwischen den betroffenen Landesverbänden abgestimmten Entscheidung,
2.  für die private Krankenversicherung vom Landesausschuss des Verbandes der Privaten Krankenversicherung,
3.  für das Land von der weiteren Behörde nach § 26 Absatz 6 Satz 2 des Pflegeberufegesetzes,
4.  für die Krankenhäuser von der Landeskrankenhausgesellschaft,
5.  für die Pflegeeinrichtungen von den Landesverbänden der Pflegeeinrichtungen aufgrund einer zwischen den betroffenen Landesverbänden abgestimmten Entscheidung,
6.  für die Pflegeschulen von den Landesverbänden der Interessenvertretungen der Pflegeschulen aufgrund einer zwischen den betroffenen Landesverbänden abgestimmten Entscheidung.

(4) Soweit die beteiligten Organisationen nach Absatz 3 Nummer 1 bis 6 keine Vertretung bestellen oder im Verfahren nach Absatz 2 keine Kandidatin oder keinen Kandidaten für das Amt des vorsitzenden Mitgliedes benennen, bestellt die zuständige Behörde nach § 14 auf Antrag einer der beteiligten Organisationen die Vertretung und benennt die Kandidatinnen oder Kandidaten.
Satz 1 gilt für die Stellvertretungen entsprechend.

(5) Die Benennung und die Bestellung sind der Geschäftsstelle nach § 6 schriftlich bekannt zu geben; die Einverständniserklärungen der Personen nach Absatz 1 Satz 1 sind beizufügen.
Die Geschäftsstelle unterrichtet schriftlich oder elektronisch die beteiligten Organisationen nach Absatz 3 Nummer 1 bis 6, die bestellten Mitglieder und deren Stellvertretungen sowie die zuständige Behörde nach § 14.
Mit erfolgter Unterrichtung ist die Bestellung wirksam.

#### § 3 Amtsperiode

(1) Die Amtsperiode der Mitglieder der Schiedsstelle nach § 36 des Pflegeberufegesetzes beträgt jeweils vier Jahre.
Die erste Amtsperiode beginnt mit dem Tag des Inkrafttretens der Verordnung.
Die wirksame Bestellung nach § 2 Absatz 5 erfolgt in der ersten Amtsperiode bis zum 30. April 2019.

(2) Das Amt der Mitglieder der Schiedsstelle nach § 36 des Pflegeberufegesetzes endet mit dem Ablauf der Amtsperiode.
Sie führen jedoch die Geschäfte bis zur Bestellung neuer Mitglieder weiter.

(3) Scheidet eines der übrigen Mitglieder vor Ablauf seiner für ihn maßgebenden Amtsperiode aus, so wird die nachfolgende Person für den Rest der Amtsperiode nach § 2 bestellt.
Satz 1 gilt für die Stellvertretung entsprechend.

(4) Eine erneute Bestellung ist möglich.

#### § 4 Abberufung und Amtsniederlegung

(1) Die beteiligten Organisationen nach § 2 Absatz 3 Nummer 1 bis 6 können gemeinsam das vorsitzende Mitglied aus wichtigem Grund abberufen.
Kommt eine Einigung nicht zustande, kann jede beteiligte Organisation bei der zuständigen Behörde nach § 14 die Abberufung des vorsitzenden Mitgliedes beantragen.
Bis zur Bestellung eines neuen vorsitzenden Mitgliedes führt die Stellvertretung die Geschäfte.
Die Sätze 1 und 2 gelten für die Stellvertretung entsprechend.

(2) Legt das vorsitzende Mitglied sein Amt nieder, übernimmt die Stellvertretung ohne Neubestellung die Geschäfte bis zum Ende der Amtsperiode.

(3) Die übrigen Mitglieder der Schiedsstelle nach § 36 des Pflegeberufegesetzes und deren Stellvertretung können von den Organisationen nach § 2 Absatz 3 Nummer 1 bis 6 abberufen werden, von denen sie bestellt wurden.
Wurde die betroffene Person von der zuständigen Behörde nach § 2 Absatz 4 bestellt, so wird die Abberufung erst mit der Bestellung der Nachfolgerin oder des Nachfolgers wirksam.

(4) Die übrigen Mitglieder der Schiedsstelle nach § 36 des Pflegeberufegesetzes und ihre Stellvertretung können durch schriftliche oder elektronische Erklärung gegenüber der Geschäftsstelle ihr Amt niederlegen.

(5) Die Geschäftsstelle unterrichtet die beteiligten Organisationen gemäß § 2 Absatz 3 Nummer 1 bis 6, die Mitglieder und deren Stellvertretung sowie die zuständige Behörde nach § 14 schriftlich oder elektronisch von der Abberufung oder der Niederlegung des Amtes.

#### § 5 Amtsführung

Die Mitglieder sind verpflichtet, an den Sitzungen der Schiedsstelle nach § 36 des Pflegeberufegesetzes teilzunehmen.
Ist ein Mitglied an der Sitzungsteilnahme verhindert, hat es unverzüglich seine Stellvertretung und die Geschäftsstelle zu benachrichtigen.
Die Sätze 1 und 2 gelten für die Stellvertretung entsprechend.
In der Einladung soll auf diese Pflicht hingewiesen werden.

#### § 6 Geschäftsstelle

(1) Die Geschäfte der Schiedsstelle nach § 36 des Pflegeberufegesetzes werden beim Landesamt für Soziales und Versorgung des Landes Brandenburg geführt.

(2) Die Beschäftigten der Geschäftsstelle unterliegen den fachlichen Weisungen des vorsitzenden Mitgliedes.

#### § 7 Antrag

(1) Das Schiedsverfahren ist einzuleiten, wenn eine der Vertragsparteien nach § 30 Absatz 1 Satz 1 und 2 oder nach § 31 Absatz 1 Satz 1 des Pflegeberufegesetzes die Entscheidung der Schiedsstelle nach § 36 des Pflegeberufegesetzes schriftlich oder elektronisch beantragt.
In den Fällen des § 30 Absatz 2 und des § 31 Absatz 3 des Pflegeberufegesetzes entscheidet die Schiedsstelle nach § 36 des Pflegeberufegesetzes innerhalb von sechs Wochen.

(2) Bei Pauschalbudgetverhandlungen nach § 30 Absatz 2 des Pflegeberufegesetzes und bei Verfahren nach § 33 Absatz 6 des Pflegeberufegesetzes sind der Antrag und alle weiteren Unterlagen bei der Geschäftsstelle mit zwölf Abschriften einzureichen.
Im Falle der elektronischen Antragstellung entfallen die Abschriften, jedoch nicht die Einreichung eines schriftlichen Originals.

(3) Bei Individualbudgetverhandlungen nach § 31 Absatz 3 des Pflegeberufegesetzes sind der Antrag und die Verhandlungsunterlagen nach § 31 Absatz 2 Satz 2 und 3 des Pflegeberufegesetzes bei der Geschäftsstelle mit zwölf Abschriften einzureichen.
Im Falle der elektronischen Antragstellung entfallen die Abschriften.
Jedoch entfällt nicht die Einreichung eines schriftlichen Originals.

(4) Der Antrag muss die Vertragsparteien bezeichnen, den Sachverhalt und das Ergebnis der vorangegangenen Verhandlungen darlegen sowie die Gegenstände aufführen, über die eine Vereinbarung nicht zustande gekommen ist.
Die in den Verhandlungen vorgelegten Nachweise und für die Entscheidungsfindung sonstigen erforderlichen Unterlagen sind beizufügen.

(5) Der Antrag kann ohne Einwilligung der anderen Vertragsparteien jederzeit zurückgenommen werden.

(6) Das vorsitzende Mitglied leitet den Vertragsparteien und den übrigen Mitgliedern eine Ausfertigung des Antrages elektronisch zu.
Es fordert die Vertragsparteien auf, innerhalb einer von ihm zu bestimmenden Frist zu dem Antrag Stellung zu nehmen.
Äußern sich eine oder mehrere Vertragsparteien innerhalb der gesetzten Frist nicht, kann auch ohne schriftliche oder elektronische Stellungnahme über den Antrag entschieden werden.

#### § 8 Vorbereitung und Leitung der Sitzung

(1) Das vorsitzende Mitglied legt Ort, Zeit und Gegenstand der Sitzungen der Schiedsstelle nach § 36 des Pflegeberufegesetzes fest.

(2) Die Einladungsfrist beträgt mindestens zwei Wochen.
Bei Eilbedürftigkeit kann sie auf eine Woche verkürzt werden.
Die Verkürzung der Frist ist in der Einladung zu begründen.
Die Einladung enthält Angaben über Ort und Zeit der Schiedsstellensitzung, die Tagesordnung und die Anträge sowie alle weiteren Unterlagen der Vertragsparteien.

(3) Die Sitzungen der Schiedsstelle nach § 36 des Pflegeberufegesetzes werden von dem vorsitzenden Mitglied so vorbereitet, dass über den Antrag möglichst in einem Termin entschieden werden kann.
Es trifft die hierzu erforderlichen Maßnahmen.
Auf Verlangen des vorsitzenden Mitglieds sind die Vertragsparteien verpflichtet, zusätzliche Unterlagen vorzulegen und Auskünfte zu erteilen, die für die Entscheidung der Schiedsstelle nach § 36 des Pflegeberufegesetzes erforderlich sind.

(4) Die Sitzungen der Schiedsstelle nach § 36 des Pflegeberufegesetzes werden von dem vorsitzenden Mitglied geleitet.

#### § 9 Mündliche Verhandlung

(1) Die Schiedsstelle nach § 36 des Pflegeberufegesetzes entscheidet über den Antrag auf Grund mündlicher Verhandlung.
Dabei ist eine gütliche Einigung der Vertragsparteien anzustreben.

(2) Zu der mündlichen Verhandlung sind die Vertragsparteien zu laden.
Die Einladungsfrist beträgt zwei Wochen.
Es kann in Abwesenheit von Vertragsparteien verhandelt und entschieden werden, wenn mit der Einladung darauf hingewiesen wurde oder die Vertragspartei selbst darauf verzichtet hat.

(3) Die mündliche Verhandlung ist nicht öffentlich.
Das vorsitzende Mitglied kann Zuhörende zulassen.
Eine Person des für Soziales zuständigen Ministeriums ist berechtigt, an der Verhandlung teilzunehmen.
Den zu den Sitzungen zugelassenen Personen steht kein Rede- und Stimmrecht zu.

(4) Zeugen und Sachverständige können auf Beschluss der Schiedsstelle nach § 36 des Pflegeberufegesetzes zu Verhandlungen hinzugezogen werden.

(5) Über die mündliche Verhandlung ist eine Niederschrift zu fertigen.
Das vorsitzende Mitglied kann Beschäftigte der Geschäftsstelle zur Fertigung der Niederschrift bestimmen.
Die Niederschrift muss Angaben enthalten über:

1.  den Ort und den Tag der Verhandlung,
2.  die Namen der Verhandlungsleitung, der Vertretung der erschienenen Vertragsparteien, der anwesenden Mitglieder, der schriftführenden Person, gegebenenfalls der Zeugen und Sachverständigen sowie der weiteren teilnehmenden Personen nach Absatz 3,
3.  den behandelten Verfahrensgegenstand und die gestellten Anträge,
4.  den wesentlichen Inhalt der Aussagen der Zeugen,
5.  den wesentlichen Inhalt der Aussagen der Sachverständigen für den Fall, dass die Aussagen über ein schriftlich vorgelegtes Gutachten hinausgehen,
6.  den Inhalt der Einigung oder den gefassten Beschluss der Schiedsstelle nach § 36 des Pflegeberufegesetzes.

Die Niederschrift ist von dem vorsitzenden Mitglied und, soweit eine schriftführende Person hinzugezogen worden ist, auch von dieser zu unterzeichnen.
Anlagen, auf die in der Verhandlungsniederschrift verwiesen wird, sind Gegenstand der Niederschrift.

(6) Die Mitglieder der Schiedsstelle nach § 36 des Pflegeberufegesetzes sowie deren Stellvertretung haben vertraulich mit den dem Verfahren zugrunde liegenden Daten der Vertragsparteien umzugehen und sind zur Verschwiegenheit verpflichtet.
Sie sind nicht befugt, Unterlagen ohne Zustimmung der jeweiligen Vertragspartei an Dritte weiterzugeben.
Die Mitglieder der Schiedsstelle nach § 36 des Pflegeberufegesetzes haben auch nach Beendigung ihrer Amtszeit über die ihnen dabei bekannt gewordenen Angelegenheiten Verschwiegenheit zu wahren.

#### § 10 Beratung und Entscheidung

(1) Über den Antrag wird durch Beschluss entschieden.
Der Beschluss enthält auch die Entscheidung des vorsitzenden Mitgliedes über die Höhe der Verfahrensgebühr nach § 12 Absatz 1 Satz 1 und über die Kostentragungspflicht nach § 12 Absatz 2.

(2) Die Schiedsstelle nach § 36 des Pflegeberufegesetzes ist beschlussfähig, wenn sämtliche Mitglieder ordnungsgemäß geladen sind und neben dem vorsitzenden Mitglied mindestens drei der nach § 2 Absatz 3 Nummer 1 bis 3 sowie

1.  bei Schiedsverfahren nach § 36 Absatz 2 des Pflegeberufegesetzes mindestens drei der nach § 2 Absatz 3 Nummer 4 und 5 bestellten Mitglieder oder
2.  bei Schiedsverfahren nach § 36 Absatz 3 des Pflegeberufegesetzes mindestens drei der nach § 2 Absatz 3 Nummer 6 bestellten Mitglieder

oder deren Stellvertretungen anwesend sind.

(3) Die Schiedsstelle nach § 36 des Pflegeberufegesetzes berät und entscheidet in Abwesenheit der Vertragsparteien nicht öffentlich.
Sie trifft Entscheidungen mit Stimmenmehrheit der Anwesenden.
Jedes Mitglied hat eine Stimme.
Stimmenthaltung ist nicht zulässig.
Bei Stimmengleichheit entscheidet die Stimme des vorsitzenden Mitgliedes.

(4) Die Entscheidung ist schriftlich zu erlassen und zu begründen sowie von dem vorsitzenden Mitglied zu unterzeichnen.
Sie ist den Vertragsparteien durch die Geschäftsstelle zuzustellen.
Klagen sind gegen die Entscheidung der Schiedsstelle nach § 36 des Pflegeberufegesetzes zu richten.
Das vorsitzende Mitglied vertritt die Schiedsstelle nach § 36 des Pflegeberufegesetzes im verwaltungsgerichtlichen Verfahren.

(5) Bei Einvernehmen der Vertragsparteien kann die Schiedsstelle nach § 36 des Pflegeberufegesetzes auf Vorschlag des vorsitzenden Mitgliedes ohne mündliche Verhandlung entscheiden.
Die Absätze 1 bis 3 gelten entsprechend.

#### § 11 Entschädigung

(1) Für entstandene Reisekosten erhält das vorsitzende Mitglied eine Erstattung entsprechend dem Bundesreisekostengesetz vom 26.
Mai 2005 (BGBl. I S. 1418), das zuletzt durch Artikel 3 des Gesetzes vom 20.
Februar 2013 (BGBl. I S. 285) geändert worden ist, in der jeweils geltenden Fassung.

(2) Das vorsitzende Mitglied hat Anspruch auf Entschädigung für den Zeitaufwand je Schiedsverfahren und für die Tätigkeit bei verwaltungsgerichtlichen Verfahren sowie auf eine Erstattung der Barauslagen.
Zu Beginn einer Amtsperiode legen die übrigen Mitglieder der Schiedsstelle nach § 36 des Pflegeberufegesetzes einvernehmlich mit Zustimmung der zuständigen Behörde nach § 14 die Höhe der Aufwandsentschädigung nach Satz 1 fest.
Kommt eine Festlegung nicht zustande, entscheidet die zuständige Behörde nach § 14.

(3) Zeugen und Sachverständige, die auf Beschluss der Schiedsstelle nach § 36 des Pflegeberufegesetzes hinzugezogen worden sind, erhalten auf Antrag eine Entschädigung entsprechend dem Justizvergütungs- und Entschädigungsgesetz vom 5.
Mai 2004 (BGBl. I S. 718, 776), das zuletzt durch Artikel 5 Absatz 2 des Gesetzes vom 11.
Oktober 2016 (BGBl. I S. 2222) geändert worden ist, in der jeweils geltenden Fassung.

(4) Ansprüche auf Entschädigungen nach den Absätzen 1 bis 3 sind bei der Geschäftsstelle binnen acht Wochen, nachdem sie angefallen sind, geltend zu machen.

(5) Die übrigen Mitglieder der Schiedsstelle nach § 36 des Pflegeberufegesetzes erhalten Reisekosten sowie Ersatz für sonstige Barauslagen und Zeitaufwand von den Organisationen, die sie bestellt haben, nach deren Regelungen.
Die Entschädigung ist von der entsendenden Organisation zu tragen.

(6) Die Absätze 1, 2, 4 und 5 gelten für die Stellvertretungen entsprechend.

#### § 12 Verfahrensgebühren

(1) Für das Verfahren der Schiedsstelle nach § 36 des Pflegeberufegesetzes setzt das vorsitzende Mitglied entsprechend der Bedeutung und der Schwierigkeit des Falles eine Gebühr von mindestens 500 Euro und höchstens 5 000 Euro fest.
Mit dem Einreichen des Antrags ist ein Vorschuss auf die festzusetzende Gebühr in Höhe von 250 Euro zu leisten.
Wird der Antrag vor der mündlichen Verhandlung oder der Entscheidung nach § 10 Absatz 4 zurückgenommen, ist eine Gebühr von 250 Euro zu entrichten.

(2) Die Gebühr des Verfahrens trägt die unterliegende Vertragspartei.
Bei teilweisem Unterliegen sowie im Vergleichsfall teilt das vorsitzende Mitglied die Gebühr anteilmäßig zwischen den Vertragsparteien auf.
Über die Kostentragungspflicht der beteiligten Vertragsparteien bei Rücknahme des Antrags entscheidet das vorsitzende Mitglied nach billigem Ermessen.

(3) In den Fällen, in denen kein Beschluss der Schiedsstelle nach § 36 des Pflegeberufegesetzes gemäß § 10 Absatz 1 ergeht, entscheidet das vorsitzende Mitglied über die Höhe der Verfahrensgebühr nach Absatz 1 und der Kostentragungspflicht nach Absatz 2 durch Beschluss.
Der Beschluss ist den beteiligten Vertragsparteien durch die Geschäftsstelle zuzustellen.

(4) Die Gebühr wird mit der Bekanntgabe des Beschlusses nach § 10 Absatz 1 oder des Beschlusses nach Absatz 3 fällig.

#### § 13 Kostendeckung

(1) Die Kosten der Schiedsstelle nach § 36 des Pflegeberufegesetzes gemäß § 11 Absatz 1 bis 3 sowie die Kosten der Geschäftsstelle sollen vorrangig durch die Gebühren nach § 12 gedeckt werden.

(2) Die Deckung der Kosten nach Absatz 1, die nach Abzug der Gebühren nach § 12 verbleiben, bestimmt sich nach § 36 Absatz 5 Satz 2 des Pflegeberufegesetzes.
Der Geschäftsstelle obliegt das Abrechnungswesen mit den beteiligten Organisationen nach § 2 Absatz 3 Nummer 1 bis 6.

(3) Die Geschäftsstelle legt der Schiedsstelle nach § 36 des Pflegeberufegesetzes und nachrichtlich der zuständigen Behörde nach § 14 bis zum 31.
März eines jeden Jahres eine Aufstellung über die Einnahmen und Ausgaben des Vorjahres vor.

#### § 14 Zuständige Behörde

Zuständige Behörde im Sinne dieser Verordnung ist das für Soziales zuständige Ministerium.

#### § 15 Übergangsregelung

Von der Frist nach § 2 Absatz 1 Satz 1 kann vor der ersten Amtsperiode abgewichen werden.

#### § 16 Verarbeitung personenbezogener Daten

(1) Die Geschäftsstelle ist berechtigt, die zur Durchführung des § 2 Absatz 4 und 5, des § 4 Absatz 1, 4 und 5, des § 8 Absatz 2 sowie des § 9 Absatz 5 erhaltenen personenbezogenen Daten zu verarbeiten.

(2) Die Geschäftsstelle darf personenbezogene Daten nur verarbeiten, soweit dies zur Durchführung nach dieser Verordnung erforderlich ist.
Die personenbezogenen Daten sind zehn Jahre nach Ende des Schiedsverfahrens aufzubewahren.
Danach sind sie zu vernichten und bei elektronischer Speicherung zu löschen.

#### § 17 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündigung in Kraft.

Potsdam, den 25.
März 2019

Die Ministerin für Arbeit, Soziales,  
Gesundheit, Frauen und Familie

Susanna Karawanskij