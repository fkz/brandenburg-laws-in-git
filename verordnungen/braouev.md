## Verordnung zur Übertragung von Aufgaben und Befugnissen nach der Bundesrechtsanwaltsordnung

Auf Grund des § 33 Absatz 2 Satz 2 der Bundesrechtsanwaltsordnung, der durch Artikel 1 Nummer 13 des Gesetzes vom 30.
Juli 2009 (BGBl.
I S. 2449) neu gefasst worden ist, in Verbindung mit § 1 Nummer 37 der Justiz-Zuständigkeitsübertragungsverordnung vom 9.
April 2014 (GVBl. II Nr. 23) verordnet der Minister der Justiz und für Europa und Verbraucherschutz:

#### § 1 

Auf den Präsidenten des Brandenburgischen Oberlandesgerichts werden folgende Aufgaben und Befugnisse übertragen:

1.  die Aufsicht über das Anwaltsgericht und den Anwaltsgerichtshof nach § 92 Absatz 3 und § 100 Absatz 1 Satz 2 der Bundesrechtsanwaltsordnung zu führen,
2.  die Geschäftsordnungen des Anwaltsgerichts und des Anwaltsgerichtshofes nach § 98 Absatz 4 Satz 2 und § 105 Absatz 2 der Bundesrechtsanwaltsordnung zu bestätigen.

#### § 2 

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung zur Übertragung von Aufgaben und Befugnissen nach der Bundesrechtsanwaltsordnung und dem Gesetz über die Tätigkeit europäischer Rechtsanwälte in Deutschland vom 29.
April 2002 (GVBl.
II S. 255), die durch Artikel 6 des Gesetzes vom 19.
Dezember 2011 (GVBl.
I Nr.
32 S.
12) geändert worden ist, außer Kraft.

Potsdam, den 27.
August 2019

Der Minister der Justiz  
und für Europa und Verbraucherschutz

Stefan Ludwig