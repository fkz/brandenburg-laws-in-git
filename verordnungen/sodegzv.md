## Verordnung über die Zuständigkeit für die Aufgaben nach dem Sozialdienstleister-Einsatzgesetz (Sozialdienstleister-Einsatzgesetz-Zuständigkeitsverordnung - SodEGZV)

Auf Grund des § 5 Satz 1 des Sozialdienstleister-Einsatzgesetzes vom 27.
März 2020 (BGBl. I S. 575, 578) in Verbindung mit § 6 Absatz 2 und 4 sowie § 12 Absatz 1 Satz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S.
186), von denen durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28 S. 2) § 6 geändert und § 12 Absatz 1 Satz 2 eingefügt worden ist, verordnet die Landesregierung:

#### § 1 Zuständigkeit der Landkreise und kreisfreien Städte

Die Landkreise und kreisfreien Städte nehmen die Aufgaben nach dem Sozialdienstleister-Einsatzgesetz

1.  in Verbindung mit § 2 Absatz 1 und § 3 des Gesetzes zur Ausführung des Neunten Buches Sozialgesetzbuch vom 18.
    Dezember 2018 (GVBl.
    I Nr.
    38), das durch Artikel 2 des Gesetzes vom 18.
    Dezember 2018 (GVBl.
    I Nr.
    38 S.
    8) geändert worden ist, als örtliche Träger der Eingliederungshilfe,
2.  in Verbindung mit § 2 Absatz 1 und § 4 Absatz 1 des Gesetzes zur Ausführung des Zwölften Buches Sozialgesetzbuch vom 3.
    November 2010 (GVBl.
    I Nr.
    36), das zuletzt durch Artikel 6 des Gesetzes vom 18.
    Dezember 2018 (GVBl.
    I Nr.
    38 S.
    12) geändert worden ist, als örtliche Träger der Sozialhilfe und
3.  in Verbindung mit § 1 Absatz 1 des Ersten Gesetzes zur Ausführung des Achten Buches Sozialgesetzbuch – Kinder- und Jugendhilfe – in der Fassung der Bekanntmachung vom 26. Juni 1997 (GVBl.
    I S.
    87), das zuletzt durch das Gesetz vom 22.
    Januar 2020 (GVBl. I Nr. 1) geändert worden ist, als örtliche Träger der Jugendhilfe

wahr.

#### § 2 Zuständigkeit des Landesamtes für Soziales und Versorgung

Das Landesamt für Soziales und Versorgung nimmt die Aufgaben nach dem Sozialdienstleister-Einsatzgesetz

1.  als Versorgungsamt, Hauptfürsorgestelle und orthopädische Versorgungsstelle im Recht der sozialen Entschädigung nach § 1 Absatz 1 der Versorgungsverwaltungszuständigkeitsverordnung vom 11.
    August 2006 (GVBl.
    II S.
    349) und
2.  als Integrationsamt für Leistungen zur Rehabilitation und Teilhabe an behinderte Menschen nach § 1 Absatz 2 der Versorgungsverwaltungszuständigkeitsverordnung

wahr.

#### § 3 Zuständigkeit des Ministeriums für Bildung, Jugend und Sport

Das Ministerium für Bildung, Jugend und Sport nimmt die Aufgaben nach dem Sozialdienstleister-Einsatzgesetz als überörtlicher Träger der Jugendhilfe nach § 8 Absatz 1 und 2 des Ersten Gesetzes zur Ausführung des Achten Buches Sozialgesetzbuch – Kinder- und Jugendhilfe – wahr.

#### § 4 Kostentragung und Verordnungsermächtigung

(1) Die §§ 15, 16 und 17 des Gesetzes zur Ausführung des Neunten Buches Sozialgesetzbuch und die §§ 10 und 11 des Gesetzes zur Ausführung des Zwölften Buches Sozialgesetzbuch gelten entsprechend.

(2) Das für Soziales zuständige Mitglied der Landesregierung wird im Einvernehmen mit dem für Jugend und dem für Finanzen zuständigen Mitglied der Landesregierung ermächtigt, durch Rechtsverordnung den Ausgleich der durch die Aufgabenwahrnehmung nach dem Sozialdienstleister-Einsatzgesetz resultierenden Mehrbelastungen entsprechend den Belastungen der Landkreise und kreisfreien Städte als örtliche Träger der Eingliederungshilfe, als örtliche Träger der Sozialhilfe und als örtliche Träger der Kinder- und Jugendhilfe zu regeln.
Eine entsprechende Regelung ist bis zum 31. Dezember 2020 mit Wirkung vom Inkrafttreten dieser Verordnung zu erlassen.
Das Land leistet auf der Grundlage der bis zum 30.
September 2020 bewilligten Anträge auf einen Zuschuss nach § 3 des Sozialdienstleister-Einsatzgesetzes an Sozialdienstleister nach dem Neunten Buch Sozialgesetzbuch und dem Zwölften Buch Sozialgesetzbuch den nach § 1 zuständigen Behörden vorläufig auf Anforderung einmalige Abschläge für Verwaltungskosten in Höhe von 4,15 Prozent der Nettoaufwendungen für die Zuschüsse abzüglich des kommunalen Anteils.
Für entsprechend bewilligte Zuschüsse an Sozialdienstleister nach dem Achten Buch Sozialgesetzbuch leistet das Land den nach § 1 zuständigen Behörden vorläufig auf Anforderung Abschläge für Verwaltungskosten in Höhe von 200 Euro je bewilligtem Antrag zuzüglich pauschal 1 500 Euro monatlich je Landkreis oder kreisfreier Stadt.
Die Abschlagszahlungen werden auf die Leistungen gemäß der Rechtsverordnung nach Satz 1 angerechnet.

#### § 5 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 24.
April 2020

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

  
Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

Ursula Nonnemacher