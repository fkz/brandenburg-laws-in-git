## Verordnung über die Anpassung der Landeszuschüsse nach § 16 Absatz 6 des Kindertagesstättengesetzes (Landeszuschussanpassungsverordnung -  LAZAV)

Auf Grund des § 23 Absatz 1 Nummer 4 des Kindertagesstättengesetzes in der Fassung der Bekanntmachung vom 27.
Juni 2004 (GVBl.
I S. 384) verordnet der Minister für Bildung, Jugend und Sport im Einvernehmen mit dem Ausschuss für Bildung, Jugend und Sport des Landes Brandenburg, mit dem Minister der Finanzen und dem Minister des Inneren und für Kommunales:

#### § 1 Grundsätze der Berechnung

Die Landeszuschüsse gemäß § 16 Absatz 6 Satz 2 und 4 des Kindertagesstättengesetzes für eine zweijährige Zuschussperiode – beginnend mit der Zuschussperiode 2015 und 2016 – werden ermittelt, indem die Landeszuschüsse der vorangegangenen zweijährigen Zuschussperiode mit den Anpassungsfaktoren multipliziert werden, die sich aus den §§ 2, 3 und 4 ergeben.
Ausgangspunkt für die Anpassungen sind die Zuschüsse des Landes im Jahr 2014 in Höhe von 174 165 000 Euro zur Finanzierung der Kindertagesbetreuung und die zusätzlichen zweckgebundenen 5 243 000 Euro zum Ausgleich der Aufgaben gemäß § 1 Absatz 2 Satz 3 und § 3 Absatz 1 Satz 6 und 7 des Kindertagesstättengesetzes.

#### § 2 Anpassungsfaktor „Kinderzahlentwicklung“

(1) Der Anpassungsfaktor „Kinderzahlentwicklung“ für die Landeszuschüsse einer Zuschussperiode nach § 16 Absatz 6 Satz 2 des Kindertagesstättengesetzes entspricht dem Verhältnis der Anzahl der Kinder im Alter bis zur Vollendung des zwölften Lebensjahres im ersten Jahr der vorangegangenen Zuschussperiode zu der Anzahl im ersten Jahr der davor liegenden Zuschussperiode.

(2) Maßgeblich sind die jeweiligen Kinderzahlen der amtlichen Statistik des Amtes für Statistik Berlin-Brandenburg zum Stichtag 31.
Dezember.
Liegen zum Zeitpunkt der Ermittlung der Landeszuschüsse lediglich vorläufige Zahlen der amtlichen Statistik des Amtes für Statistik Berlin-Brandenburg für die relevanten Stichtage vor, sind diese maßgeblich.

#### § 3 Anpassungsfaktor „Personalkostenentwicklung“

Der Anpassungsfaktor „Personalkostenentwicklung“ für die Landeszuschüsse einer Zuschussperiode ergibt sich aus der Multiplikation der jährlichen prozentualen Tarifänderung nach dem Tarifvertrag für den Öffentlichen Dienst (Bund und Kommunen) für die Tarifstelle gemäß § 5 Absatz 3 Satz 1 der Kindertagesstätten-Betriebskosten- und Nachweisverordnung für das erste Jahr der vorangegangenen Zuschussperiode und für das Jahr davor.

#### § 4 Anpassungsfaktor „Umfang des Tagesbetreuungsangebotes“

(1) Der Anpassungsfaktor „Umfang des Tagesbetreuungsangebotes“ für eine Zuschussperiode wird aus der Betreuungsquote als Maß der relativen Inanspruchnahme und dem Differenzierungsgrad als Maß des Zeitumfangs der Platzbelegung gebildet.
Er basiert auf dem Verhältnis der Betreuungsquote im ersten Jahr der vorangegangenen Zuschussperiode zur Betreuungsquote im ersten Jahr der davor liegenden Zuschussperiode sowie dem Verhältnis der Differenzierungsgrade in den entsprechenden Jahren.
Der Durchschnitt beider Verhältnisse bildet den Anpassungsfaktor „Umfang des Tagesbetreuungsangebotes“.
Maßgeblich sind die der obersten Landesjugendbehörde gemäß Kindertagesstätten-Betriebskosten- und Nachweisverordnung gemeldeten belegten Plätze im Jahresmittel sowie die jeweiligen Kinderzahlen der amtlichen Statistik des Amtes für Statistik Berlin-Brandenburg.

(2) Die Betreuungsquote des jeweiligen Jahres entspricht dem Verhältnis der Anzahl der belegten Plätze in der Kindertagesbetreuung zur Anzahl der Kinder im Alter bis zur Vollendung des zwölften Lebensjahres der amtlichen Statistik des Amtes für Statistik Berlin-Brandenburg zum Stichtag 31. Dezember des Vorjahres.

(3) Der Differenzierungsgrad des jeweiligen Jahres ergibt sich aus dem Personalbedarf der tatsächlich belegten Plätze in Kindertagesstätten im Jahresmittel im Verhältnis zu dem Personalbedarf, der sich ergäbe, wenn für alle belegten Plätze verlängerte Betreuungszeiten gewährt würden.
Der jeweilige Personalbedarf bemisst sich nach § 10 Absatz 1 des Kindertagesstättengesetzes.

#### § 5 Veröffentlichung der Ergebnisse der Berechnung

Das für Jugend zuständige Mitglied der Landesregierung veröffentlicht im Amtsblatt des Ministeriums für Bildung, Jugend und Sport

1.  für jede Zuschussperiode die Anpassungsfaktoren gemäß den §§ 2, 3 und 4 und die Höhe der Landeszuschüsse und
2.  für jedes Jahr die Verteilung des Zuschusses an die Landkreise gemäß § 16 Absatz 6 Satz 4 des Kindertagesstättengesetzes.

#### § 6 Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2015 in Kraft.

Potsdam, den 3.
November 2015

Der Minister für Bildung,  
Jugend und Sport

Günter Baaske