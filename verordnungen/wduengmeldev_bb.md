## Verordnung über Aufzeichnungs- und Meldepflichten beim Inverkehrbringen und der Übernahme von Wirtschaftsdünger (Wirtschaftsdüngermeldeverordnung - WDüngMeldeV BB)

Auf Grund des § 15 Absatz 6 in Verbindung mit §§ 4 und 14 Absatz 2 Nummer 1 Buchstabe b des Düngegesetzes vom 9.
Januar 2009 (BGBl.
I S. 54, 136), die durch Artikel 1 des Gesetzes vom 5. Mai 2017 (BGBl.
I S.
1068) geändert worden sind, in Verbindung mit § 6 der Verordnung über das Inverkehrbringen und Befördern von Wirtschaftsdünger vom 21.
Juli 2010 (BGBl.
I S.
1062) und auf Grund des § 1 der Dünge-Zuständigkeitsverordnung vom 22.
Januar 2019 (GVBl.
II Nr. 9) sowie § 1 Nummer 1 der Verordnung zur Übertragung von Ermächtigungen auf dem Gebiet des Düngerechts vom 31. August 2020 (GVBl.
II Nr.
76) verordnet der Minister für Landwirtschaft, Umwelt und Klimaschutz:

#### § 1 Aufzeichnungspflicht

(1) Abgeber und Empfänger von Wirtschaftsdüngern sowie von Stoffen, die als Ausgangsstoff oder Bestandteil Wirtschaftsdünger enthalten (sonstige Stoffe), haben Aufzeichnungen nach § 3 Absatz 2 Satz 2 der Verordnung über das Inverkehrbringen und Befördern von Wirtschaftsdünger der zuständigen Behörde innerhalb eines Monats ab dem Tag der Abgabe oder Übernahme vollständig vorzulegen.
Die Vorlage erfolgt durch elektronische Übermittlung der in § 3 Absatz 1 der Verordnung über das Inverkehrbringen und Befördern von Wirtschaftsdünger genannten Angaben in der nach § 1 Absatz 4 der Dünge-Zuständigkeitsverordnung von der zuständigen Behörde zur Verfügung gestellten Datenbank.

(2) Als Registrier- oder Betriebsnummer in der nach Absatz 1 genannten Datenbank ist anzugeben:

1.  die Betriebsnummer nach § 17 Absatz 1 der InVeKoS-Verordnung vom 24. Februar 2015 (BGBl. I S. 166), die zuletzt durch Artikel 2 der Verordnung vom 22. Februar 2019 (BGBl. I S. 170) geändert worden ist, in der jeweils geltenden Fassung,
2.  die Registriernummer nach § 26 der Viehverkehrsverordnung in der Fassung der Bekanntmachung vom 26.
    Mai 2020 (BGBl.
    I S.
    1170), in der jeweils geltenden Fassung oder,
3.  wenn keine der unter den Nummern 1 bis 2 benannten Betriebs- und Registriernummern vorliegt, eine von der zuständigen Behörde auf Anforderung zugeteilte Betriebsnummer.

#### § 2 Meldepflicht

Bei der Verbringung von Stoffen gemäß § 1 Absatz 1 nach Brandenburg haben Empfänger dieser Stoffe die Angaben nach § 4 der Verordnung über das Inverkehrbringen und Befördern von Wirtschaftsdünger innerhalb eines Monats ab dem Tag der Übernahme der zuständigen Stelle auf der von ihr zur Verfügung gestellten Datenbank elektronisch zu melden.

#### § 3 Datenspeicherung

Die nach den §§ 1 und 2 gemeldeten Daten sind sieben Jahre nach dem Ablauf des Düngejahres in der Datenbank zu speichern.

#### § 4 Ordnungswidrigkeiten

Ordnungswidrig im Sinne des § 14 Absatz 2 Nummer 1 Buchstabe b des Düngegesetzes handelt, wer vorsätzlich oder fahrlässig

1.  entgegen § 1 eine Aufzeichnung nicht, nicht richtig, nicht vollständig oder nicht rechtzeitig vorlegt,
2.  entgegen § 2 eine Meldung nicht, nicht richtig, nicht vollständig oder nicht rechtzeitig macht.

#### § 5 Inkrafttreten

Diese Verordnung tritt am 1.
Januar 2021 in Kraft.

Potsdam, den 23.
November 2020

Der Minister für Landwirtschaft,  
Umwelt und Klimaschutz

Axel Vogel