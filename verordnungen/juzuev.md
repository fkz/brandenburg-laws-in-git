## Verordnung zur Übertragung von Zuständigkeiten zum Erlass von Rechtsverordnungen auf das für Justiz zuständige Mitglied der Landesregierung (Justiz-Zuständigkeitsübertragungsverordnung - JuZÜV)

Auf Grund

1.  des § 391 Absatz 2 Satz 2 der Abgabenordnung in der Fassung der Bekanntmachung vom 1. Oktober 2002 (BGBl.
    I S. 3866, 2003 I S.
    61);
2.  des § 14 Absatz 4 Satz 3, § 15 Absatz 2 Satz 2, § 17 Absatz 3, § 34 Absatz 2 Satz 2, § 35 Absatz 3 Satz 2 in Verbindung mit § 17 Absatz 3, § 46c Absatz 2 Satz 2 und § 46e Absatz 1 Satz 3 des Arbeitsgerichtsgesetzes, von denen § 14 Absatz 4 Satz 3, § 15 Absatz 2 Satz 2, § 17 Absatz 3, § 34 Absatz 2 Satz 2 durch Artikel 1 Nummer 3, 4, 5 und 11 des Gesetzes vom 30.
    März 2000 (BGBl.
    I S.
    333) zuletzt geändert, § 35 Absatz 3 Satz 2 durch Artikel 1 Nummer 13 des Gesetzes vom 26.
    Juni 1990 (BGBl.
    I S.
    1206) neu gefasst und § 46c Absatz 2 Satz 2 und § 46e Absatz 1 Satz 3 durch Artikel 4 Nummer 3 des Gesetzes vom 30. Oktober 2008 (BGBl.
    I S.
    2122, 2127) geändert worden sind;
3.  des § 22 Absatz 1 Satz 3 des Außenwirtschaftsgesetzes in der Fassung der Bekanntmachung vom 6.
    Juni 2013 (BGBl.
    I S.
    1482);
4.  des § 219 Absatz 2 Satz 2 des Baugesetzbuchs in der Fassung der Bekanntmachung vom 23. September 2004 (BGBl.
    I S.
    2414);
5.  des § 54b Absatz 3 Satz 3 des Beurkundungsgesetzes, der durch Artikel 2 Nummer 6 des Gesetzes vom 31.
    August 1998 (BGBl.
    I S.
    2585) eingefügt worden ist;
6.  des § 4 Absatz 1 Satz 3 des Gesetzes über das gerichtliche Verfahren in Binnenschifffahrtssachen in der im Bundesgesetzblatt Teil III, Gliederungsnummer 310-5, veröffentlichten bereinigten Fassung;
7.  des § 55a Absatz 1 Satz 3, § 79 Absatz 5 Satz 4, § 979 Absatz 1b Satz 2 zweiter Halbsatz, § 1059a Absatz 1 Nummer 2 Satz 5 und § 1558 Absatz 2 Satz 2 des Bürgerlichen Gesetzbuchs in der Fassung der Bekanntmachung vom 2.
    Januar 2002 (BGBl.
    I S.
    42, 2909; 2003 I S.
    738), von denen § 979 Absatz 1b Satz 2 zweiter Halbsatz durch Artikel 4 Nummer 3 des Gesetzes vom 30.
    Juli 2009 (BGBl.
    I S.
    2474, 2476) eingefügt, § 1059a Absatz 1 Nummer 2 Satz 5 durch Artikel 123 Nummer 3 des Gesetzes vom 19.
    April 2006 (BGBl. I S. 866, 882) geändert und § 1558 Absatz 2 Satz 2 durch Artikel 123 Nummer 4 desselben Gesetzes neu gefasst worden ist;
8.  des § 52 Absatz 2 Satz 2 und des § 63 Absatz 2 Satz 2 des Designgesetzes vom 12. März 2004 (BGBl.
    I S.
    390, 2013 I S.
    3786);
9.  des § 14 Absatz 4 Satz 3, § 107 Absatz 3 Satz 2, § 292 Absatz 2 Satz 4, § 376 Absatz 2 Satz 2 und § 387 Absatz 1 Satz 2 des Gesetzes über das Verfahren in Familiensachen und in den Angelegenheiten der freiwilligen Gerichtsbarkeit vom 17.
    Dezember 2008 (BGBl. I S. 2586);
10.  des § 52a Absatz 1 Satz 5 und § 52b Absatz 1 Satz 4 der Finanzgerichtsordnung, die durch Artikel 3 Nummer 2 des Gesetzes vom 22.
    März 2005 (BGBl.
    I S.
    837, 844) eingefügt worden sind;
11.  des § 27 Absatz 2 Satz 2 des Gebrauchsmustergesetzes in der Fassung der Bekanntmachung vom 28.
    August 1986 (BGBl.
    I S.
    1455);
12.  des § 156 Absatz 1 Satz 1 des Genossenschaftsgesetzes in Verbindung mit § 8a Absatz 2 Satz 3 des Handelsgesetzbuchs, von denen § 156 Absatz 1 Satz 1 des Genossenschaftsgesetzes durch Artikel 3 Nummer 12 des Gesetzes vom 10. November 2006 (BGBl.
    I S.
    2553, 2563) zuletzt geändert und § 8a Absatz 2 Satz 3 des Handelsgesetzbuchs durch Artikel 1 Nummer 2 des Gesetzes vom 10.
    November 2006 (BGBl.
    I S.
    2553) neu gefasst worden ist;
13.  des § 1 der Genossenschaftsregisterverordnung in Verbindung mit § 54 Absatz 1 Satz 1 der Handelsregisterverordnung und § 9 Absatz 4 des Landesorganisationsgesetzes vom 24. Mai 2004 (GVBl.
    I S.
    186), von denen § 1 der Genossenschaftsregisterverordnung durch Artikel 5 Absatz 4 und § 54 Absatz 1 Satz 1 der Handelsregisterverordnung durch Artikel 5 Absatz 2 des Gesetzes vom 10.
    November 2006 (BGBl.
    I S.
    2553, 2565) zuletzt geändert worden sind;
14.  des § 22c Absatz 2, § 23d Satz 2, § 58 Absatz 1 Satz 2, § 71 Absatz 4 Satz 2, § 72 Absatz 2 Satz 3, § 74c Absatz 3 Satz 2, § 74d Satz 2, § 78 Absatz 1 Satz 3, § 78a Absatz 2 Satz 3, § 93 Absatz 2, § 116 Absatz 3, § 143 Absatz 5 Satz 2, § 152 Absatz 2 Satz 3 und § 157 Absatz 2 Satz 2 des Gerichtsverfassungsgesetzes in der Fassung der Bekanntmachung vom 9.
    Mai 1975 (BGBl.
    I S.
    1077), von denen § 23d Satz 2 durch Artikel 22 Nummer 10 des Gesetzes vom 17.
    Dezember 2008 (BGBl.
    I S.
    2586, 2694), § 58 Absatz 1 Satz 2 durch § 179 Nummer 1 des Gesetzes vom 16.
    März 1976 (BGBl.
    I S.
    581, 604), § 72 Absatz 2 Satz 3 durch Artikel 22 Nummer 12 des Gesetzes vom 17.
    Dezember 2008 (BGBl.
    I S.
    2586, 2694) und § 78a Absatz 2 Satz 3 durch § 78 Nummer 1 des Gesetzes vom 23.
    Dezember 1982 (BGBl.
    I S.
    2071, 2086) geändert und § 22c Absatz 2 durch Artikel 3 Nummer 1 des Gesetzes vom 24.
    Juni 1994 (BGBl.
    I S.
    1374), § 74c Absatz 3 Satz 2 durch Artikel 2 Nummer 7 des Gesetzes vom 5.
    Oktober 1978 (BGBl.
    I S.
    1645, 1650) und § 93 Absatz 2 durch Artikel 17 Nummer 3 des Gesetzes vom 19.
    April 2006 (BGBl.
    I S.
    866, 868) neu gefasst und § 71 Absatz 4 Satz 2 durch Artikel 22 Nummer 11 des Gesetzes vom 17.
    Dezember 2008 (BGBl.
    I S.
    2586, 2694), § 116 Absatz 3 durch Artikel 17 Nummer 5 des Gesetzes vom 19.
    April 2006 (BGBl.
    I S.
    866, 868), § 143 Absatz 5 Satz 2 durch Artikel 14 des Gesetzes vom 3.
    Mai 2000 (BGBl.
    I S.
    632, 633) und § 157 Absatz 2 Satz 2 durch Artikel 2 Nummer 4 des Gesetzes vom 3.
    Dezember 1976 (BGBl.
    I S.
    3281, 3297) eingefügt worden sind;
15.  des § 16a Absatz 3 Satz 2 des Einführungsgesetzes zum Gerichtsverfassungsgesetz, der durch Artikel 21 des Gesetzes vom 23.
    Juli 2002 (BGBl.
    I S.
    2850, 2856) eingefügt worden ist;
16.  des § 12a Absatz 4 des Gerichtsvollzieherkostengesetzes, der durch Artikel 6 Absatz 1 des Gesetzes vom 23.
    Juli 2013 (BGBl.
    I S.
    2586, 2677) eingefügt worden ist;
17.  des § 1 Absatz 3 Satz 2, § 7 Absatz 3 Satz 3, § 81 Absatz 4 Satz 4, § 126 Absatz 1 Satz 3 und § 127 Absatz 1 Satz 4 der Grundbuchordnung in der Fassung der Bekanntmachung vom 26.
    Mai 1994 (BGBl.
    I S.
    1114), von denen § 7 Absatz 3 Satz 3 durch Artikel 1 Nummer 4 des Gesetzes vom 1.
    Oktober 2013 (BGBl.
    I S.
    3719) eingefügt und § 81 Absatz 4 Satz 4 durch Artikel 1 Nummer 13 des Gesetzes vom 11.
    August 2009 (BGBl.
    I S.
    2713) und § 127 Absatz 1 Satz 4 durch Artikel 1 Nummer 15 des Gesetzes vom 1.
    Oktober 2013 (BGBl.
    I S. 3719) neu gefasst worden sind;
18.  des § 74 Absatz 1 Satz 3, § 93 Satz 2 und § 101 Satz 2 der Grundbuchverfügung in der Fassung der Bekanntmachung vom 24.
    Januar 1995 (BGBl.
    I S.
    114), von denen § 93 Satz 2 durch Artikel 2 Nummer 35 des Gesetzes vom 1.
    Oktober 2013 (BGBl.
    I S.
    3719, 3721) neu gefasst und § 101 Satz 2 durch Artikel 2 Nummer 9 des Gesetzes vom 11.
    August 2009 (BGBl.
    I S.
    2713, 2717) eingefügt worden ist;
19.  des § 8a Absatz 2 Satz 3 und § 9 Absatz 1 Satz 3 zweiter Halbsatz des Handelsgesetzbuchs, die durch Artikel 1 Nummer 2 des Gesetzes vom 10.
    November 2006 (BGBl.
    I S.
    2553) neu gefasst worden sind;
20.  des § 54 Absatz 1 Satz 1 der Handelsregisterverordnung in Verbindung mit § 9 Absatz 4 des Landesorganisationsgesetzes, von denen § 54 Absatz 1 Satz 1 der Handelsregisterverordnung durch Artikel 5 Absatz 2 des Gesetzes vom 10.
    November 2006 (BGBl.
    I S.
    2553, 2565) zuletzt geändert worden ist;
21.  des § 2 Absatz 2 Satz 2 und § 5 Absatz 4 Satz 4 der Insolvenzordnung vom 5.
    Oktober 1994 (BGBl.
    I S.
    2866), von denen § 5 Absatz 4 Satz 4 durch Artikel 1 Nummer 1 des Gesetzes vom 13.
    April 2007 (BGBl.
    I S.
    509) eingefügt worden ist;
22.  des Artikels 102 § 1 Absatz 3 Satz 3 des Einführungsgesetzes zur Insolvenzordnung, der durch Artikel 1 des Gesetzes vom 14.
    März 2003 (BGBl.
    I S.
    345) neu gefasst worden ist;
23.  des § 33 Absatz 3 Satz 2, § 85 Absatz 2 Satz 3 und Absatz 3 Satz 4 des Jugendgerichtsgesetzes, von denen § 33 Absatz 3 Satz 2 durch Artikel 7 Nummer 1 des Gesetzes vom 11.
    Januar 1993 (BGBl.
    I S.
    50, 52) geändert und § 85 Absatz 2 Satz 3 und Absatz 3 Satz 4 durch Artikel 1 Nummer 36 des Gesetzes vom 30.
    August 1990 (BGBl.
    I S. 1853) eingefügt worden ist;
24.  des § 1 Absatz 6 Satz 2 der Justizbeitreibungsordnung, der durch Artikel 5 Absatz 2 des Gesetzes vom 7.
    Juli 1986 (BGBl.
    I S.
    977, 982) eingefügt worden ist;
25.  des § 11 Absatz 3 Satz 2 und Absatz 4 Satz 2 des Kapitalanleger-Musterverfahrensgesetzes vom 19.
    Oktober 2012 (BGBl.
    I S.
    2182);
26.  des § 8 Satz 3 des Gesetzes über das gerichtliche Verfahren in Landwirtschaftssachen in der im Bundesgesetzblatt Teil III, Gliederungsnummer 317-1, veröffentlichten bereinigten Fassung;
27.  des § 15 Absatz 5 Satz 4 der Luftfahrzeugpfandrechtsregisterverordnung, der durch Artikel 5 Absatz 6 des Gesetzes vom 10.
    November 2006 (BGBl.
    I S.
    2553, 2565) geändert worden ist;
28.  des § 125e Absatz 3 Satz 2 und § 140 Absatz 2 Satz 2 des Markengesetzes vom 25. Oktober 1994 (BGBl.
    I S.
    3082; 1995 I S.
    156; 1996 I S.
    682), von denen § 125e Absatz 3 Satz 2 durch Artikel 1 Nummer 5 des Gesetzes vom 19.
    Juli 1996 (BGBl.
    I S.
    1014) eingefügt worden ist;
29.  des § 38 Absatz 1 Satz 3 des Gesetzes zur Durchführung der Gemeinsamen Marktorganisationen und der Direktzahlungen in der Fassung der Bekanntmachung vom 24. Juni 2005 (BGBl.
    I S.
    1847);
30.  des § 6 Absatz 4 Satz 2, § 7 Absatz 5 Satz 2, § 9 Absatz 1 Satz 2, § 25 Absatz 2 Satz 1, § 67 Absatz 3 Nummer 3 Satz 4 und § 112 Satz 2 der Bundesnotarordnung in der im Bundesgesetzblatt Teil III, Gliederungsnummer 303-1, veröffentlichten bereinigten Fassung, von denen § 6 Absatz 4 Satz 2 durch Artikel 1 Nummer 1 des Gesetzes vom 2.
    April 2009 (BGBl.
    I S.
    696) und § 25 Absatz 2 Satz 1 durch Artikel 1 Nummer 22 des Gesetzes vom 31. August 1998 (BGBl.
    I S.
    2585) eingefügt, § 9 Absatz 1 Satz 2 durch Artikel 1 Nummer 6 des Gesetzes vom 31.
    August 1998 (BGBl.
    I S.
    2585) und § 112 Satz 2 durch Artikel 3 Nummer 20 des Gesetzes vom 30.
    Juli 2009 (BGBl.
    I S.
    2449, 2464) neu gefasst und § 7 Absatz 5 Satz 2 durch Artikel 1 Nummer 4 des Gesetzes vom 29.
    Januar 1991 (BGBl.
    I S. 150) und § 67 Absatz 3 Nummer 3 Satz 4 durch Artikel 1 Nummer 35 des Gesetzes vom 31. August 1998 (BGBl.
    I S.
    2585) geändert worden sind;
31.  des § 9 Absatz 2 Satz 2 des Gesetzes zum Schutz des olympischen Emblems und der olympischen Bezeichnungen vom 31.
    März 2004 (BGBl.
    I S.
    479);
32.  des § 68 Absatz 3 Satz 3 und § 110a Absatz 2 Satz 2 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
    Februar 1987 (BGBl.
    I S. 602), von denen § 110a Absatz 2 Satz 2 durch Artikel 7 Nummer 6 des Gesetzes vom 22. März 2005 (BGBl.
    I S.
    837, 849) eingefügt worden ist;
33.  des § 1 Absatz 1 der Partnerschaftsregisterverordnung in Verbindung mit § 54 Absatz 1 Satz 1 der Handelsregisterverordnung in Verbindung mit § 9 Absatz 4 des Landesorganisationsgesetzes, von denen § 1 Absatz 1 der Partnerschaftsregisterverordnung durch Artikel 2 Nummer 1 der Verordnung vom 11.
    Dezember 2001 (BGBl.
    I S.
    3688, 3695) geändert und § 54 Absatz 1 Satz 1 der Handelsregisterverordnung durch Artikel 5 Absatz 2 des Gesetzes vom 10.
    November 2006 (BGBl.
    I S.
    2553, 2565) zuletzt geändert worden ist;
34.  des § 5 Absatz 2 erster Halbsatz des Partnerschaftsgesellschaftsgesetzes in Verbindung mit § 8a Absatz 2 Satz 3 des Handelsgesetzbuchs, von denen § 5 Absatz 2 erster Halbsatz des Partnerschaftsgesellschaftsgesetzes durch Artikel 12 Absatz 12 des Gesetzes vom 10. November 2006 (BGBl.
    I S.
    2553) zuletzt geändert und § 8a Absatz 2 Satz 3 des Handelsgesetzbuchs durch Artikel 1 Nummer 2 des Gesetzes vom 10.
    November 2006 (BGBl. I S. 2553) neu gefasst worden ist;
35.  des § 143 Absatz 2 Satz 2 des Patentgesetzes in der Fassung der Bekanntmachung vom 16. Dezember 1980 (BGBl.
    1981 I S.
    1);
36.  des Artikels II § 12 Satz 3 des Gesetzes über internationale Patentübereinkommen vom 21. Juni 1976 (BGBl.
    II S.
    649);
37.  des § 33 Absatz 2 Satz 3 der Bundesrechtsanwaltsordnung, der durch Artikel 1 Nummer 13 des Gesetzes vom 30.
    Juli 2009 (BGBl.
    I S.
    2449) neu gefasst worden ist;
38.  des § 41 Absatz 1 Satz 2 und Absatz 2 Satz 2 des Gesetzes über die Tätigkeit europäischer Rechtsanwälte in Deutschland, von denen Absatz 1 Satz 2 durch Artikel 2 des Gesetzes vom 30.
    Juli 2009 (BGBl.
    I S.
    2449, 2460) neu gefasst und Absatz 2 Satz 2 durch Artikel 2 Nummer 4 des Gesetzes vom 26.
    März 2007 (BGBl.
    I S.
    358, 364) geändert worden ist;
39.  des § 19 Absatz 2 Satz 2 des Rechtsdienstleistungsgesetzes vom 12.
    Dezember 2007 (BGBl. I S.
    2840);
40.  des § 19 Absatz 1 Satz 2, § 24b Absatz 2 und § 36b Absatz 1 Satz 2 des Rechtspflegergesetzes in der Fassung der Bekanntmachung vom 14.
    April 2013 (BGBl.
    I S. 778);
41.  des § 1 Absatz 2 Satz 2, § 2 Absatz 3 Satz 2, § 65 Absatz 1 in Verbindung mit § 1 Absatz 2 Satz 2 und § 2 Absatz 3 Satz 2, § 89 Absatz 4 Satz 4 und § 93 Satz 1 der Schiffsregisterordnung in der Fassung der Bekanntmachung vom 26.
    Mai 1994 (BGBl.
    I S. 1133), von denen § 89 Absatz 4 Satz 4 durch Artikel 4 Absatz 5 des Gesetzes vom 11. August 2009 (BGBl.
    I S.
    2713, 2720) neu gefasst worden ist, in Verbindung mit § 126 Absatz 1 Satz 3 der Grundbuchordnung;
42.  des § 38 Absatz 2 Satz 2 des Sortenschutzgesetzes in der Fassung der Bekanntmachung vom 19.
    Dezember 1997 (BGBl.
    I S.
    3164);
43.  des § 9 Absatz 2 und § 30 Absatz 2 des Sozialgerichtsgesetzes jeweils in Verbindung mit § 9 Absatz 4 des Landesorganisationsgesetzes, des § 65a Absatz 1 Satz 5 und § 65b Absatz 1 Satz 4 des Sozialgerichtsgesetzes, von denen § 9 Absatz 2 und § 30 Absatz 2 durch Artikel 1 Nummer 3 und 13 des Gesetzes vom 17.
    August 2001 (BGBl.
    I S.
    2144) neu gefasst und § 65a Absatz 1 Satz 5 und § 65b Absatz 1 Satz 4 durch Artikel 4 Nummer 3 des Gesetzes vom 22.
    März 2005 (BGBl.
    I S.
    837, 846) eingefügt worden sind;
44.  des Artikels 293 Absatz 1 Satz 4 des Einführungsgesetzes zum Strafgesetzbuch, der durch Artikel 4 Nummer 1 des Gesetzes vom 13.
    April 1986 (BGBl.
    I S.
    393, 396) neu gefasst worden ist;
45.  des § 41a Absatz 2 Satz 2 der Strafprozessordnung, der durch Artikel 6 Nummer 3 des Gesetzes vom 22.
    März 2005 (BGBl.
    I S.
    837, 849) eingefügt worden ist;
46.  des § 2 Absatz 1 Satz 5 des Transsexuellengesetzes vom 10.
    September 1980 (BGBl.
    I S. 1654);
47.  des § 13 Absatz 2 Satz 2 des Gesetzes gegen den unlauteren Wettbewerb in der Fassung der Bekanntmachung vom 3.
    März 2010 (BGBl.
    I S.
    254);
48.  des § 6 Absatz 2 Satz 2 des Unterlassungsklagengesetzes in der Fassung der Bekanntmachung vom 27.
    August 2002 (BGBl.
    I S.
    3422, 4346);
49.  des § 105 Absatz 3 des Urheberrechtsgesetzes vom 9.
    September 1965 (BGBl.
    I S.
    1273);
50.  des § 38 Absatz 1 Satz 1 der Vereinsregisterverordnung vom 10.
    Februar 1999 (BGBl.
    I S.
    147) in Verbindung mit § 9 Absatz 4 des Landesorganisationsgesetzes;
51.  des § 55a Absatz 1 Satz 5 und § 55b Absatz 1 Satz 4 der Verwaltungsgerichtsordnung, die durch Artikel 2 Nummer 2 des Gesetzes vom 22.
    März 2005 (BGBl.
    I S.
    837, 841) eingefügt worden sind;
52.  des § 66 Absatz 3 Satz 3 des Wertpapiererwerbs- und Übernahmegesetzes vom 20. Dezember 2001 (BGBl.
    I S.
    3822);
53.  des § 89 Absatz 1 Satz 2 des Gesetzes gegen Wettbewerbsbeschränkungen in der Fassung der Bekanntmachung vom 26.
    Juni 2013 (BGBl.
    I S.
    1750);
54.  des § 13 Absatz 1 Satz 3 des Wirtschaftsstrafgesetzes 1954 in der Fassung der Bekanntmachung vom 3.
    Juni 1975 (BGBl.
    I S.
    1313);
55.  des § 1 Absatz 1 Satz 2 des Gesetzes über den Zahlungsverkehr mit Gerichten und Justizbehörden vom 22.
    Dezember 2006 (BGBl.
    I S.
    3416);
56.  des § 32b Absatz 2 Satz 2, § 130a Absatz 2 Satz 2, § 298a Absatz 1 Satz 3, § 689 Absatz 3 Satz 3, § 703c Absatz 3 zweiter Halbsatz, § 802k Absatz 3 Satz 2, § 814 Absatz 3 Satz 2, § 882h Absatz 2 Satz 2 in Verbindung mit § 802k Absatz 3 Satz 2, § 1074 Absatz 4 und § 1077 Absatz 1 Satz 3 der Zivilprozessordnung in der Fassung der Bekanntmachung vom 5. Dezember 2005 (BGBl.
    I S.
    3202; 2006 I S.
    431; 2007 I S.
    1781), von denen § 802k Absatz 3 Satz 2 und § 882h Absatz 2 Satz 2 durch Artikel 1 Nummer 7 und 17 des Gesetzes vom 29.
    Juli 2009 (BGBl.
    I S.
    2258) und § 814 Absatz 3 Satz 2 durch Artikel 1 Nummer 1 des Gesetzes vom 30.
    Juli 2009 (BGBl.
    I S.
    2474) eingefügt worden sind;
57.  des § 1 Absatz 2 Satz 2 des Gesetzes über die Zwangsversteigerung und die Zwangsverwaltung in der im Bundesgesetzblatt Teil III, Gliederungsnummer 310-14, veröffentlichten bereinigten Fassung;
58.  des § 76 Absatz 1 Satz 3 des Landesbeamtengesetzes vom 3.
    April 2009 (GVBl.
    I S.
    26)

verordnet die Landesregierung:

### § 1

Die nachstehenden Ermächtigungen zum Erlass von Rechtsverordnungen werden im Umfang ihrer jeweiligen Fassung auf das für Justiz zuständige Mitglied der Landesregierung übertragen:

1.  Abgabenordnung:  
    die Ermächtigung nach § 391 Absatz 2 Satz 1;
2.  Arbeitsgerichtsgesetz:  
    die Ermächtigungen nach § 14 Absatz 4 Satz 2, § 15 Absatz 2 Satz 1, § 17 Absatz 2, § 34 Absatz 2 Satz 1, § 35 Absatz 3 Satz 2 in Verbindung mit § 17 Absatz 2 und § 46e Absatz 1 Satz 2;
3.  Außenwirtschaftsgesetz:  
    die Ermächtigung nach § 22 Absatz 1 Satz 2;
4.  Baugesetzbuch:  
    die Ermächtigung nach § 219 Absatz 2 Satz 1;
5.  Beurkundungsgesetz:  
    die Ermächtigung nach § 54b Absatz 3 Satz 3;
6.  Gesetz über das gerichtliche Verfahren in Binnenschifffahrtssachen:  
    die Ermächtigungen nach § 4 Absatz 1 Satz 1 und 2;
7.  Bürgerliches Gesetzbuch:  
    die Ermächtigungen nach § 55a Absatz 1 Satz 1, § 79 Absatz 5 Satz 3, § 979 Absatz 1b Satz 2 erster Halbsatz, § 1059a Absatz 1 Nummer 2 Satz 4 und § 1558 Absatz 2 Satz 1;
8.  Designgesetz:  
    die Ermächtigungen nach § 52 Absatz 2 Satz 1 und § 63 Absatz 2 Satz 1;
9.  Gesetz über das Verfahren in Familiensachen und in den Angelegenheiten der freiwilligen Gerichtsbarkeit:  
    die Ermächtigungen nach § 14 Absatz 4 Satz 1 und 2, § 107 Absatz 3 Satz 1, § 292 Absatz 2 Satz 1, § 376 Absatz 2 Satz 1 und § 387 Absatz 1 Satz 1;
10.  Finanzgerichtsordnung:  
    die Ermächtigung nach § 52b Absatz 1 Satz 2;
11.  Gebrauchsmustergesetz:  
    die Ermächtigung nach § 27 Absatz 2 Satz 1;
12.  Genossenschaftsgesetz:  
    die Ermächtigungen nach § 156 Absatz 1 Satz 1 in Verbindung mit § 8a Absatz 2 Satz 1 des Handelsgesetzbuchs;
13.  Genossenschaftsregisterverordnung:  
    die Ermächtigungen nach § 1 in Verbindung mit § 54 Absatz 1 Satz 1 der Handelsregisterverordnung in Verbindung mit § 6 Absatz 2 des Landesorganisationsgesetzes;
14.  Gerichtsverfassungsgesetz:  
    die Ermächtigungen nach § 22c Absatz 1, § 23d Satz 1, § 58 Absatz 1 Satz 1, § 71 Absatz 4 Satz 1, § 72 Absatz 2 Satz 2, § 74c Absatz 3 Satz 1, § 74d Satz 1, § 78 Absatz 1 Satz 1, § 78a Absatz 2 Satz 1 und 2, § 93 Absatz 1, § 116 Absatz 2, § 143 Absatz 5 Satz 1, § 152 Absatz 2 Satz 1 und § 157 Absatz 2 Satz 1;
15.  Einführungsgesetz zum Gerichtsverfassungsgesetz:  
    die Ermächtigung nach § 16a Absatz 3 Satz 1;
16.  Gerichtsvollzieherkostengesetz:  
    die Ermächtigung nach § 12a Absatz 1;
17.  Grundbuchordnung:  
    die Ermächtigungen nach § 1 Absatz 3 Satz 1, § 7 Absatz 3 Satz 1 und 2, § 81 Absatz 4 Satz 1 und 2, § 126 Absatz 1 Satz 1, § 127 Absatz 1 Satz 1;
18.  Grundbuchverfügung:  
    die Ermächtigungen nach § 74 Absatz 1 Satz 3, § 93 Satz 1 und § 101 Satz 1;
19.  Handelsgesetzbuch:  
    die Ermächtigungen nach § 8a Absatz 2 Satz 1 und § 9 Absatz 1 Satz 3 erster Halbsatz;
20.  Handelsregisterverordnung:  
    die Ermächtigung nach § 54 Absatz 1 Satz 1 in Verbindung mit § 6 Absatz 2 des Landesorganisationsgesetzes;
21.  Insolvenzordnung:  
    die Ermächtigungen nach § 2 Absatz 2 Satz 1 und § 5 Absatz 4 Satz 2;
22.  Einführungsgesetz zur Insolvenzordnung:  
    die Ermächtigung nach Artikel 102 § 1 Absatz 3 Satz 2;
23.  Jugendgerichtsgesetz:  
    die Ermächtigungen nach § 33 Absatz 3 Satz 1, § 85 Absatz 2 Satz 2 und Absatz 3 Satz 3;
24.  Justizbeitreibungsgesetz:  
    die Ermächtigung nach § 1 Absatz 6 Satz 1;
25.  Kapitalanleger-Musterverfahrensgesetz:  
    die Ermächtigungen nach § 11 Absatz 3 Satz 1 und Absatz 4 Satz 1;
26.  Gesetz über das gerichtliche Verfahren in Landwirtschaftssachen:  
    die Ermächtigung nach § 8 Satz 1;
27.  Luftfahrzeugpfandrechtsregisterverordnung:  
    die Ermächtigung nach § 15 Absatz 5 Satz 3;
28.  Markengesetz:  
    die Ermächtigungen nach § 125e Absatz 3 Satz 1 und § 140 Absatz 2 Satz 1;
29.  Gesetz zur Durchführung der Gemeinsamen Marktorganisationen und der Direktzahlungen:  
    die Ermächtigung nach § 38 Absatz 1 Satz 2;
30.  Bundesnotarordnung:  
    die Ermächtigungen nach § 6 Absatz 4 Satz 1, § 7 Absatz 5 Satz 2, § 9 Absatz 1 Satz 2, § 25 Absatz 2 Satz 1 und § 67 Absatz 3 Nummer 3 Satz 4 und § 112 Satz 1;
31.  Gesetz zum Schutz des olympischen Emblems und der olympischen Bezeichnungen:  
    die Ermächtigung nach § 9 Absatz 2 Satz 1;
32.  Gesetz über Ordnungswidrigkeiten:  
    die Ermächtigung nach § 68 Absatz 3 Satz 1 und die Ermächtigung nach § 110a Absatz 2 Satz 1 für seinen Geschäftsbereich sowie die Ermächtigung nach § 36 Absatz 2 Satz 1 für die Verfolgung und Ahndung von durch Rechtsanwälte, Kammerrechtsbeistände oder Notare begangene Ordnungswidrigkeiten nach dem Geldwäschegesetz;
33.  Partnerschaftsregisterverordnung:  
    die Ermächtigungen nach § 1 Absatz 1 in Verbindung mit § 54 Absatz 1 Satz 1 der Handelsregisterverordnung in Verbindung mit § 6 Absatz 2 des Landesorganisationsgesetzes;
34.  Partnerschaftsgesellschaftsgesetz:  
    die Ermächtigungen nach § 5 Absatz 2 erster Halbsatz in Verbindung mit § 8a Absatz 2 Satz 1 des Handelsgesetzbuchs;
35.  Patentgesetz:  
    die Ermächtigung nach § 143 Absatz 2 Satz 1;
36.  Gesetz über internationale Patentübereinkommen:  
    die Ermächtigung nach Artikel II § 12 Satz 2;
37.  Bundesrechtsanwaltsordnung:  
    die Ermächtigung nach § 33 Absatz 2 Satz 2;
38.  Gesetz über die Tätigkeit europäischer Rechtsanwälte in Deutschland:  
    die Ermächtigungen nach § 41 Absatz 1 Satz 1 und Absatz 2 Satz 1;
39.  Rechtsdienstleistungsgesetz:  
    die Ermächtigung nach § 19 Absatz 2 Satz 1;
40.  Rechtspflegergesetz:  
    die Ermächtigungen nach § 19 Absatz 1 Satz 1, § 24b Absatz 1 und § 36b Absatz 1 Satz 1;
41.  Schiffsregisterordnung:  
    die Ermächtigungen nach § 1 Absatz 2 Satz 1, § 2 Absatz 3 Satz 1, § 65 Absatz 1 in Verbindung mit § 1 Absatz 2 Satz 1 und § 2 Absatz 3 Satz 1, § 89 Absatz 4 Satz 1 und 2 sowie nach § 93 Satz 1 in Verbindung mit § 126 Absatz 1 Satz 1 der Grundbuchordnung;
42.  Sortenschutzgesetz:  
    die Ermächtigung nach § 38 Absatz 2 Satz 1;
43.  Sozialgerichtsgesetz:  
    die Ermächtigungen nach § 9 Absatz 2 und § 30 Absatz 2, jeweils in Verbindung mit § 6 Absatz 2 des Landesorganisationsgesetzes, sowie nach § 65b Absatz 1 Satz 2;
44.  Einführungsgesetz zum Strafgesetzbuch:  
    die Ermächtigung nach Artikel 293 Absatz 1 Satz 1;
45.  Strafprozessordnung:  
    die Ermächtigung nach § 41a Absatz 2 Satz 1;
46.  Transsexuellengesetz:  
    die Ermächtigungen nach § 2 Absatz 1 Satz 3 und 4;
47.  Gesetz gegen den unlauteren Wettbewerb:  
    die Ermächtigung nach § 13 Absatz 2 Satz 1;
48.  Unterlassungsklagengesetz:  
    die Ermächtigung nach § 6 Absatz 2 Satz 1;
49.  Urheberrechtsgesetz:  
    die Ermächtigungen nach § 105 Absatz 1 und 2;
50.  Vereinsregisterverordnung:  
    die Ermächtigung nach § 38 Absatz 1 Satz 1 in Verbindung mit § 6 Absatz 2 des Landesorganisationsgesetzes;
51.  Verwaltungsgerichtsordnung:  
    die Ermächtigung nach § 55b Absatz 1 Satz 2;
52.  Wertpapiererwerbs- und Übernahmegesetz:  
    die Ermächtigung nach § 66 Absatz 3 Satz 1;
53.  Gesetz gegen Wettbewerbsbeschränkungen:  
    die Ermächtigung nach § 89 Absatz 1 Satz 1;
54.  Wirtschaftsstrafgesetz 1954:  
    die Ermächtigung nach § 13 Absatz 1 Satz 2;
55.  Gesetz über den Zahlungsverkehr mit Gerichten und Justizbehörden:  
    die Ermächtigung nach § 1 Absatz 1 Satz 1;
56.  Zivilprozessordnung:  
    die Ermächtigungen nach § 32b Absatz 2 Satz 1, § 298a Absatz 1 Satz 2, § 689 Absatz 3 Satz 1, § 703c Absatz 3 erster Halbsatz, § 802k Absatz 3 Satz 1, § 814 Absatz 3 Satz 1, § 882h Absatz 2 Satz 1, § 1074 Absatz 2 und 3 Satz 1, § 1077 Absatz 1 Satz 2 und § 1104a Satz 1;
57.  Gesetz über die Zwangsversteigerung und die Zwangsverwaltung:  
    die Ermächtigung nach § 1 Absatz 2 Satz 1;
58.  Landesbeamtengesetz:  
    die Ermächtigung nach § 76 Absatz 1 Satz 1 für die Beamtengruppe der Staatsanwälte, Amtsanwälte und Wirtschaftsreferenten bei den Staatsanwaltschaften;
59.  Asylgesetz:  
    die Ermächtigung nach § 83 Absatz 3 Satz 1;
60.  Geldwäschegesetz:  
    die Ermächtigung nach § 50 Nummer 9 in Verbindung mit § 6 Absatz 2 des Landesorganisationsgesetzes für den Personenkreis nach § 2 Absatz 1 Nummer 11.

### § 2

(1) Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig tritt die Justiz-Zuständigkeitsübertragungsverordnung vom 28.
November 2006 (GVBl. II S.
479), die zuletzt durch die Verordnung vom 5.
Dezember 2012 (GVBl.
II Nr. 103) geändert worden ist, außer Kraft.

(3) Soweit andere Vorschriften des Landesrechts auf der aufgehobenen Verordnung beruhen oder auf sie verweisen, treten an deren Stelle die Bestimmungen dieser Verordnung.

Potsdam, den 9.
April 2014

Die Landesregierung des Landes Brandenburg

Der Ministerpräsident  
Dr.
Dietmar Woidke

Der Minister der Justiz  
Dr.
Helmuth Markov