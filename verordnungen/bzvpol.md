## Verordnung zur Übertragung beamtenrechtlicher Zuständigkeiten auf die Polizei des Landes Brandenburg (Brandenburgische Beamtenrechtszuständigkeitsverordnung Polizei - BZVPol)

### § 1  
Übertragung von Befugnissen nach der Ernennungsverordnung

(1) Dem Polizeipräsidium wird für seinen Zuständigkeitsbereich die Ausübung der Befugnis zur Ernennung der Beamtinnen und Beamten aller Laufbahnen des mittleren, gehobenen und höheren Dienstes bis einschließlich zur Besoldungsgruppe A 14 übertragen.

(2) Im Polizeipräsidium wird abweichend von Absatz 1 den Polizeidirektionen für den jeweiligen Zuständigkeitsbereich die Befugnis zur Ernennung von Beamtinnen und Beamten aller Laufbahnen des mittleren und gehobenen Dienstes bis einschließlich zur Besoldungsgruppe A 11 übertragen.

(3) Die Ausübung der Befugnis zur Ernennung der Beamtinnen und Beamten der Laufbahnen des mittleren und gehobenen Dienstes des eigenen Zuständigkeitsbereichs wird auf die Fachhochschule der Polizei und den Zentraldienst der Polizei übertragen.

### § 2  
Übertragung von Befugnissen nach dem Landesbeamtengesetz

(1) Den in § 1 genannten Stellen werden für den jeweiligen Zuständigkeitsbereich folgende Zuständigkeiten übertragen:

1.  die Entscheidung nach § 32 Absatz 1 Satz 1 des Landesbeamtengesetzes über die Entlassung kraft Gesetzes gemäß § 22 Absatz 1, 2 oder 3 des Beamtenstatusgesetzes,
2.  die Entscheidung nach § 54 Absatz 1 des Landesbeamtengesetzes über das Verbot der Führung der Dienstgeschäfte gemäß § 39 des Beamtenstatusgesetzes,
3.  die Entscheidung nach § 57 Absatz 1 Satz 2 des Landesbeamtengesetzes über die Ausnahme vom Verbot der Annahme von Belohnungen, Geschenken und sonstigen Vorteilen gemäß § 42 Absatz 1 Satz 2 des Beamtenstatusgesetzes,
4.  die Entscheidung nach § 66 Absatz 4 Satz 1 des Landesbeamtengesetzes über den Ersatz von Sachschäden,

4a.

die Entscheidung nach § 67a Absatz 3 Satz 2 erster Halbsatz des Landesbeamtengesetzes über die Erfüllungsübernahme durch den Dienstherrn bei Schmerzensgeldansprüchen,

5.  die Entscheidung nach § 69 Absatz 5 Satz 1 des Landesbeamtengesetzes über die Erteilung einer Erlaubnis für entlassene Beamtinnen und Beamte, die Amtsbezeichnung mit dem Zusatz „außer Dienst“ („a.
    D.“) sowie die mit dem Amt verliehenen Titel zu führen, und die Entscheidung über die Rücknahme der Erlaubnis nach § 69 Absatz 5 Satz 2 des Landesbeamtengesetzes,
6.  die Entscheidung auf dem Gebiet des Nebentätigkeitsrechts gemäß § 84, § 85 Absatz 4 Satz 4, § 86 Absatz 3, § 88 Absatz 5 Satz 1, die Entscheidung gemäß § 89 des Landesbeamtengesetzes, bei der Ausübung der Nebentätigkeit Einrichtungen, Personal oder Material des Dienstherrn in Anspruch nehmen zu dürfen,
7.  die Entscheidung nach § 92 Absatz 2 des Landesbeamtengesetzes über den Ausspruch des Verbots einer Beschäftigung oder Erwerbstätigkeit nach Beendigung des Beamtenverhältnisses gemäß § 41 Satz 2 des Beamtenstatusgesetzes.

(2) Dem Polizeipräsidium wird die Zuständigkeit für die Entscheidung nach § 50 des Landebeamtengesetzes über die Versetzung in den Ruhestand wegen Dienstunfähigkeit gemäß § 26 Absatz 1 und § 28 des Beamtenstatusgesetzes übertragen.

(3) Den in § 1 Absatz 1 und 3 genannten Stellen wird die Entscheidung nach § 56 Absatz 1 Satz 3 des Landesbeamtengesetzes über die Versagung der Aussagegenehmigung gemäß § 37 Absatz 3 des Beamtenstatusgesetzes übertragen.
Das Polizeipräsidium kann die in Satz 1 genannte Entscheidung auf die Stellen des § 1 Absatz 2 delegieren.

(4) Die beamtenrechtlichen Entscheidungen über die persönlichen Angelegenheiten derjenigen Beamtinnen und Beamten, die eine mit der Besoldungsgruppe A 15 bewertete Funktion innehaben, unabhängig davon, ob sie das statusrechtliche Amt der Besoldungsgruppe A 15 bereits erreicht haben, verbleiben bei dem für das öffentliche Dienstrecht der Polizei zuständigen Ministerium.

### § 3  
_(aufgehoben)_

### § 4  
Übertragung von Befugnissen nach dem Brandenburgischen Besoldungsgesetz

Der Fachhochschule der Polizei des Landes Brandenburg wird die Befugnis zur Kürzung der Bezüge für die Anwärterinnen und Anwärter gemäß § 58 des Brandenburgischen Besoldungsgesetzes übertragen.

### § 5  
Übertragung von Befugnissen des Landesdisziplinargesetzes

(1) Die Disziplinarbefugnisse gegenüber Ruhestandsbeamtinnen und Ruhestandsbeamten werden durch die Leiterin oder den Leiter der in § 1 Absatz 1 und 3 genannten Dienststellen ausgeübt, wenn die Dienstvorgesetzteneigenschaft im Zeitpunkt der Beendigung des Beamtenverhältnisses bei der Polizeibehörde oder bei einer Polizeieinrichtung lag, im Übrigen durch das für das öffentliche Dienstrecht der Polizei zuständige Ministerium.

(2) Den Leiterinnen oder den Leitern der in § 1 Absatz 1 und 3 genannten Stellen werden in dem jeweiligen Zuständigkeitsbereich folgende Zuständigkeiten übertragen:

1.  die Zuständigkeit nach § 34 Absatz 3 Nummer 1 des Landesdisziplinargesetzes über die Kürzung der Dienstbezüge bis zum Höchstmaß,
2.  die Zuständigkeit nach § 35 Absatz 2 Satz 1 des Landesdisziplinargesetzes über die Erhebung der Disziplinarklage.

(3) Den in § 1 Absatz 1 und 3 genannten Stellen wird für den eigenen Zuständigkeitsbereich die Zuständigkeit nach § 42 Absatz 2 Satz 1 des Landesdisziplinargesetzes über den Erlass des Widerspruchsbescheids vor Erhebungder Klage übertragen.