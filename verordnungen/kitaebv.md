## Kitaelternbeiratsverordnung (KitaEBV)

Auf Grund des § 23 Absatz 1 Nummer 8 und 13 des Kindertagesstättengesetzes in der Fassung der Bekanntmachung vom 27.
Juni 2004 (GVBl.
I S. 384), der zuletzt durch Artikel 1 des Gesetzes vom 1. April 2019 (GVBl.
I Nr. 8) geändert worden ist, verordnet die Ministerin für Bildung, Jugend und Sport im Einvernehmen mit dem Minister der Finanzen, dem Minister des Innern und dem Ausschuss für Bildung, Jugend und Sport des Landtags:

### Abschnitt 1  
Allgemeine Vorschriften

#### § 1 Anwendungsbereich

Diese Verordnung regelt die Zusammensetzung, den Rahmen für die Arbeitsweise, die Beteiligungsrechte sowie die Bereitstellung der erforderlichen Mittel zur Wahrnehmung der gesetzlichen Aufgaben des Landeskitaelternbeirates nach § 6a Absatz 4 des Kindertagesstättengesetzes.
Daneben regelt diese Verordnung den Ausgleich der Mehraufwendungen der Landkreise und kreisfreien Städte aufgrund der Änderung des § 6a des Kindertagesstättengesetzes durch das Brandenburgische Gute-KiTa-Gesetz vom 1. April 2019 (GVBl. I Nr. 8).

### Abschnitt 2  
Landeskitaelternbeirat

#### § 2 Zusammensetzung

Der Landeskitaelternbeirat besteht aus den nach § 6a Absatz 4 Satz 2 des Kindertagesstättengesetzes von den Kreiskitaelternbeiräten der Landkreise und kreisfreien Städten gewählten Elternvertretungen.
Jeder vom Landkreis oder von der kreisfreien Stadt anerkannte Kreiskitaelternbeirat des Landkreises oder der kreisfreien Stadt kann eine Elternvertretung und eine Stellvertretung in den Landeskitaelternbeirat entsenden.

#### § 3 Ehrenamtliche Aufgabenwahrnehmung

Die Mitglieder des Landeskitaelternbeirates sind ehrenamtlich tätig.

#### § 4 Mitgliederversammlung

(1) Die in den Landeskitaelternbeirat gewählten Elternvertretungen bilden die Mitgliederversammlung.

(2) Die Mitgliederversammlung wählt bei seiner ersten Sitzung einen Vorstand.
Bei der Wahl entscheidet die einfache Mehrheit der abgegebenen Stimmen.
Die Mitgliederversammlung kann den Vorstand jederzeit durch Beschluss abberufen.
Der Beschluss über die Abberufung des Vorstands erfordert drei Viertel der Stimmen der Mitglieder.
Bei Abberufung des Vorstands ist umgehend ein neuer Vorstand zu wählen.

(3) Der Landeskitaelternbeirat gibt sich eine Geschäftsordnung, über die die Mitgliederversammlung beschließt.
Änderungen der Geschäftsordnung erfordern eine Zweidrittelmehrheit der anwesenden stimmberechtigten Mitglieder.

(4) Die Mitgliederversammlung tritt mindestens einmal im Jahr zusammen.
Sie ist ohne Rücksicht auf die Zahl der erschienenen Mitglieder beschlussfähig.
Der Vorstand ist zur Einberufung einer außerordentlichen Mitgliederversammlung verpflichtet, wenn mindestens ein Drittel der Mitglieder dies verlangt.

(5) Zu Beginn der Mitgliederversammlung wird eine Schriftführerin oder ein Schriftführer gewählt.

(6) Jede gewählte Elternvertretung des Landeskitaelternbeirates hat eine Stimme.
Das Stimmrecht kann nur persönlich ausgeübt werden.
Ist die gewählte Elternvertretung verhindert, so übt die nach § 2 Satz 2 in den Landeskitaelternbeirat entsandte Stellvertretung das Stimmrecht für die gewählte Elternvertretung aus.

(7) Bei Abstimmungen und Wahlen entscheidet grundsätzlich die einfache Mehrheit der abgegebenen Stimmen, soweit diese Verordnung nichts anderes bestimmt.

(8) Über die Beschlüsse der Mitgliederversammlung ist ein Protokoll anzufertigen.

#### § 5 Vorstand

(1) Der Vorstand wird von der Mitgliederversammlung gewählt und besteht aus mindestens drei Elternvertretungen und bis zu zwei Stellvertretungen.
Ihm können nur gewählte Elternvertretungen der Mitgliederversammlung angehören.

(2) Dem Vorstand obliegt die Einberufung der Mitgliederversammlung.
Er bereitet die Tagesordnung und die zu fassenden Beschlüsse vor.
Die Mitgliederversammlung wird von einem Mitglied des Vorstands geleitet.

(3) Bei Abstimmungen und Wahlen gelten die Vorschriften für die Mitgliederversammlung entsprechend.

(4) Der Vorstand wählt aus seiner Mitte eine Sprecherin oder einen Sprecher, die oder der den Landeskitaelternbeirat nach außen vertritt, sowie eine Stellvertretung.
Der Vorstand kann jederzeit eine neue Sprecherin oder einen neuen Sprecher und deren oder dessen Stellvertretung wählen.
Die Wahl der Sprecherin oder des Sprechers und deren oder dessen Stellvertretung wird der obersten Landesjugendbehörde bekanntgegeben.

(5) Endet die Mitgliedschaft im Landeskitaelternbeirat, so endet auch das Amt als Vorstand.

(6) Scheidet ein Vorstandsmitglied aus, so findet umgehend eine Nachwahl des Vorstandsmitglieds statt.

#### § 6 Beteiligung

(1) Der Landeskitaelternbeirat für Kindertagesbetreuung wird gemäß § 6a Absatz 4 Satz 4 bis 6 des Kindertagesstättengesetzes von den für Kindertagesbetreuung und Schulangelegenheiten zuständigen Ministerien in allen wesentlichen, die Kindertagesbetreuung betreffenden Fragen, insbesondere zu den in § 6a Absatz 4 Satz 5 und 6 des Kindertagesstättengesetzes genannten Themengebieten, angehört.
Er gibt gemäß § 6a Absatz 4 Satz 7 des Kindertagesstättengesetzes seine Stellungnahmen gegenüber den für Kindertagesbetreuung und Schulangelegenheiten zuständigen Ministerien ab.

(2) Die für Kindertagesbetreuung und Schulangelegenheiten zuständigen Ministerien informieren die Sprecherin oder den Sprecher des Landeskitaelternbeirates über alle die Kindertagesbetreuung betreffenden Angelegenheiten, die zur Erfüllung der Aufgaben des Landeskitaelternbeirates nach § 6a Absatz 4 des Kindertagesstättengesetzes erforderlich sind, und übersendet ihr oder ihm die für die Aufgabenwahrnehmung benötigten Dokumente.
Dies umfasst insbesondere Entwürfe von Gesetzen, Rechtsverordnungen und Berichten der Landesregierung für den Landtag, die in diesem Zusammenhang stehen.
Die Sprecherin oder der Sprecher des Landeskitaelternbeirates leitet die nach Satz 1 erhaltenen Dokumente an den Vorstand und die Mitgliederversammlung weiter.

#### § 7 Unterstützung durch das Land

(1) Die oberste Landesjugendbehörde stellt dem Landeskitaelternbeirat die zur Wahrnehmung seiner Aufgaben nach § 6a Absatz 4 des Kindertagesstättengesetzes erforderlichen

1.  Räume,
2.  Sachmittel und
3.  Verwaltungsmittel, insbesondere für einen Internetauftritt sowie für ein E-Mailfunktionspostfach

bereit.
Die Bereitstellung erfolgt als Verwaltungs- und Sachleistung, soweit sich aus den nachfolgenden Bestimmungen nichts anderes ergibt.

(2) Die Mitglieder des Landeskitaelternbeirates erhalten für die mit ihrer Tätigkeit im Landeskitaelternbeirat verbundenen Aufwendungen eine angemessene Entschädigung, wenn ihnen nicht eine Entschädigung nach anderen Rechtsvorschriften gewährt wird oder zusteht.

(3) Aufwendungen im Sinne des Absatzes 2 sind Fahrtkosten, Wegstreckenentschädigungen, Übernachtungsgeld, Nebenkosten und die Kosten für Verpflegungsmehraufwendungen.
Sie werden erstattet für die Teilnahme an

1.  Beratungen des Landeskitaelternbeirates,
2.  Beratungen des Vorstands des Landeskitaelternbeirates,
3.  Beratungen und Anhörungen auf Einladung der obersten Landesjugendbehörde.

(4) Nimmt ein stellvertretendes Mitglied neben dem zu vertretenden Mitglied an einer Beratung nach Absatz 3 Satz 2 Nummer 1 oder Nummer 2 teil, kann dem stellvertretenden Mitglied die Aufwandsentschädigung gewährt werden, wenn die Vertretung in mindestens einem Tagesordnungspunkt notwendig war.
Über die Notwendigkeit entscheidet dieoberste Landesjugendbehörde.

#### § 8 Fahrtkosten, Wegstreckenentschädigung

(1) Erstattet werden die notwendigen Kosten für die Hin- und Rückfahrt zwischen Wohnung und Tagungsort unter Berücksichtigung der für die Bediensteten des Landes Brandenburg geltenden reisekostenrechtlichen Bestimmungen entsprechend der §§ 4 und 5 des Bundesreisekostengesetzes.
Bei mehrtägigen Veranstaltungen werden Fahrtkosten oder Wegstreckenentschädigungen für die tägliche Hin- und Rückfahrt nur insoweit erstattet, als hierdurch keine höheren Gesamtkosten als beim Verbleiben am Veranstaltungsort entstehen.

(2) Für Strecken, die mit regelmäßig verkehrenden Beförderungsmitteln zurückgelegt werden, werden die entstandenen notwendigen Kosten, insbesondere Fahrpreis, Zuschlag und Platzkarte, erstattet.
Für Bahnfahrten ist die Kostenerstattung auf die zweite Klasse beschränkt.
Kosten für die erste Klasse können im Ausnahmefall erstattet werden, insbesondere wenn eine amtlich festgestellte Erwerbsminderung von mindestens 50 Prozent vorliegt.
Fahrpreisermäßigungen sind zu berücksichtigen.

(3) Bei Benutzung privater Kraftfahrzeuge wird eine Wegstreckenentschädigung in Höhe von 20 Cent je Kilometer zurückgelegter Strecke gewährt.

#### § 9 Übernachtungsgeld

(1) Für die Teilnahme an Veranstaltungen nach § 7 Absatz 3 Satz 2 Nummer 1 und 3, bei denen eine Übernachtung notwendig ist, wird ein Übernachtungsgeld bis zur Höhe des Satzes gewährt, der Landesbeamtinnen und Landesbeamten nach den reisekostenrechtlichen Bestimmungen gemäß § 7 Absatz 1 des Bundesreisekostengesetzes zusteht.

(2) Höhere Übernachtungskosten im Sinne des § 7 Absatz 1 des Bundesreisekostengesetzes können bei Nachweis erstattet werden, sofern sie angemessen und notwendig sind.

(3) Mitglieder des Landeskitaelternbeirates, die am Tagungsort einschließlich seines Einzugsgebietes gemäß § 3 Absatz 1 Nummer 1 Buchstabe c des Bundesumzugskostengesetzes wohnen, haben keinen Anspruch auf Übernachtungsgeld.

#### § 10 Nebenkosten

(1) Nebenkosten, zum Beispiel Telefonkosten oder Parkgebühren, im Sinne der für die Bediensteten des Landes Brandenburg geltenden reisekostenrechtlichen Bestimmungen gemäß § 10 Absatz 1 des Bundesreisekostengesetzes können auf Antrag bei Vorliegen besonderer Gründe erstattet werden.

(2) Dem Vorstand des Landeskitaelternbeirates können notwendige Telefon-, Porto- und Kopierkosten erstattet werden.

(3) Die Verfahrensweise wird durch die oberste Landesjugendbehörde entsprechend geltender haushaltsrechtlicher Regelungen festgelegt.

#### § 11 Erstattung von Verpflegungsmehraufwendungen

(1) Zur Abgeltung des durch die Teilnahme an den Beratungen gemäß § 7 Absatz 3 Satz 2 entstehenden Mehraufwandes für die Verpflegung wird je Beratungstag eine Verpflegungspauschale in folgender Höhe gewährt:

Bei einer Dauer

1.  von mindestens acht aber weniger als 14 Stunden sechs Euro,
2.  von mindestens 14 aber weniger als 24 Stunden zwölf Euro,
3.  von 24 Stunden 24 Euro.

(2) Bei Bereitstellung einer unentgeltlichen Verpflegung wird keine Verpflegungspauschale gewährt.

#### § 12 Nachweise und Bescheinigung

(1) Der Nachweis für den Anspruch auf Aufwandsentschädigung ist die Teilnehmerliste in Verbindung mit den entsprechenden Belegen.
Der Anspruch verfällt, wenn er nicht innerhalb eines Jahres nach Ende der Veranstaltung mit den Belegen unter Angabe einer Bankverbindung bei der obersten Landesjugendbehörde geltend gemacht wird.

(2) Eine Bescheinigung über die gezahlten Entschädigungen für Lohn- und Einkommenssteuerzwecke wird auf Antrag für das abgelaufene Kalenderjahr ausgestellt.

### Abschnitt 3  
Kreiskitaelternbeiräte

#### § 13 Ausgleich von Mehraufwendungen

(1) Zum Ausgleich der Mehraufwendungen, die den Landkreisen und kreisfreien Städten durch die Änderung des § 6a des Kindertagesstättengesetzes durch das Brandenburgische Gute-KiTa-Gesetz vom 1.
April 2019 (GVBl.
I Nr.
8) dadurch entstehen, dass ab dem 1.
August 2019 in jedem Landkreis und in jeder kreisfreien Stadt ein Kreiskitaelternbeirat gebildet werden muss, der von den Jugendämter jeweils zur ersten Sitzung dieses Gremiums eingeladen werden muss und vom Landkreis oder der kreisfreien Stadt in allen die Kindertagesbetreuung betreffenden Fragen anzuhören ist, zahlt das Land an die Landkreise und kreisfreien Städte einen Mehraufwendungsausgleich.

(2) Der Mehraufwendungsausgleich beträgt für jeden Landkreis und jede kreisfreie Stadt jeweils 5 000 Euro im Kalenderjahr.
Auf Antrag des Landkreises oder der kreisfreien Stadt gleicht das Land nachgewiesene höhere Mehraufwendungen aus.
Der Antrag ist bis zum 1.
November für das abgelaufene Kalenderjahr zu stellen.

(3) Der Mehraufwendungsausgleich und der Ausgleich höherer Mehraufwendungen werden bis zum 1. Februar für das Kalenderjahr an die Landkreise und kreisfreien Städte ausgereicht.
Im Jahr 2019 wird der Ausgleich anteilig für fünf Monate bis zum 1.
Dezember 2019 ausgereicht.

#### § 14 Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
August 2019 in Kraft.

Potsdam, den 16.
August 2019

Die Ministerin für Bildung,  
Jugend und Sport

Britta Ernst