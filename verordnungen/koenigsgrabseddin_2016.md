## Verordnung über das Grabungsschutzgebiet „Siedlungs- und Ritualraum Königsgrab Seddin“

Auf Grund des § 5 des Brandenburgischen Denkmalschutzgesetzes vom 24.
Mai 2004 (GVBl.
I S. 215) verordnet die Landesregierung:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Prignitz wird zum Grabungsschutzgebiet erklärt.
Das Grabungsschutzgebiet trägt die Bezeichnung „Siedlungs- und Ritualraum Königsgrab Seddin“.

#### § 2 Schutzgegenstand

(1) Das Grabungsschutzgebiet hat eine Größe von rund 5 661 Hektar.
Es umfasst Flächen in folgenden Fluren:

Gemeinde

Gemarkung

Flur

Groß Pankow (Prignitz)

Baek

3

Groß Pankow (Prignitz)

Hohenvier

1 bis 3

Groß Pankow (Prignitz)

Strigleben

2

Groß Pankow (Prignitz)

Tangendorf

2

Groß Pankow (Prignitz)

Klein Gottschow

4

Groß Pankow (Prignitz)

Groß Pankow

1, 2

Groß Pankow (Prignitz)

Helle

2 bis 4

Groß Pankow (Prignitz)

Retzin

1 bis 3

Groß Pankow (Prignitz)

Klein Linde

1, 2

Groß Pankow (Prignitz)

Kreuzburg

1, 2

Groß Pankow (Prignitz)

Rohlsdorf (R)

1 bis 3

Groß Pankow (Prignitz)

Wolfshagen

1 bis 11

Groß Pankow (Prignitz)

Seddin

1 bis 5

Groß Pankow (Prignitz)

Tacken

1.

Die Lage des Grabungsschutzgebietes ist in der als Anlage 1 beigefügten Übersichtskarte mit den Blattnummern 1 bis 3 im Maßstab 1 : 50 000 dargestellt.

(2) Die Grenze des Grabungsschutzgebietes ist in den in Anlage 2 aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Im Zusammenhang bebaute Ortslagen und ausgewiesene Gebiete mit bestehender landwirtschaftlicher Bebauung im Außenbereich sind nicht Gegenstand des Schutzgebietes.
Die in Anlage 2 Nummer 1 aufgeführten 15 topografischen Karten im Maßstab 1 : 10 000 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den laufenden Blattnummern 1 bis 165 aufgeführten Liegenschaftskarten.

(3) Die Verordnung mit Karten kann beim Ministerium für Wissenschaft, Forschung und Kultur des Landes Brandenburg, oberste Denkmalschutzbehörde, in Potsdam sowie beim Landkreis Prignitz, untere Denkmalschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck ist

1.  die dauerhafte Erhaltung der im Boden und in den Gewässern liegenden beweglichen und unbeweglichen Bodendenkmale, insbesondere der Bestattungsplätze (Grabhügel, Flachgräberfelder), der unbefestigten und befestigten Siedlungen, der Wirtschafts-, Verkehrs- und Kultanlagen sowie der sonstigen menschlichen Zeugnisse, zum Beispiel von Religion und Krieg, insbesondere der Bronzezeit, aber auch der vorhergehenden und nachfolgenden Perioden;
2.  die Erhaltung aller an und unter der Erd- oder Wasseroberfläche erhaltenen Reste und Spuren menschlicher Aktivitäten, wie Funde und Befunde einschließlich der zwischen ihnen bestehenden räumlichen und zeitlichen Kontexte;
3.  die Erhaltung der auf der Geländeoberkante situierten oder leicht eingegrabenen großen Steine mit eingearbeiteten Schälchen (Schälchensteine) als besondere Zeugnisse der Religionsausübung;
4.  die Erforschung komplexer menschlicher Daseins- und Organisationsformen am Rande der bronzezeitlichen Hochkulturen.

(2) Die Unterschutzstellung dient

1.  wegen der besonders hohen Anzahl von sicht- und erkennbaren Bodendenkmalen, vor allem von raumprägenden Grabhügeln, gemeinsam mit den ausschließlich im Untergrund verborgenen Bodendenkmalen dem Schutz der räumlichen Beziehungen zwischen den Objekten.
    Darin eingeschlossen ist das Erscheinungsbild und die Raumwirkung der sicht- und erkennbaren Bodendenkmale;
2.  der Nachvollziehbarkeit von Aufbau und Struktur der einzelnen Denkmalkomplexe (zum Beispiel Königsgrab, Burgwall Horst, Bestattungsplatz Teufelsberg).

(3) Die umfängliche Erhaltung von bronzezeitlichen raumbedeutsamen Bauwerken, die durch die Kenntnis abgegangener Strukturen ergänzt wird, ermöglicht es in einmaliger Weise, die räumlichen Bezüge und raumwirksamen Gestaltungsansätze der ehemals bestimmenden Akteure in dieser Region zu erforschen.
Die einmalige Ausprägung und Lesbarkeit der archäologischen Ressourcen und der naturräumlichen Komponenten bedingen ein außerordentliches Forschungs- und Erkenntnispotenzial zum bronzezeitlichen Natur-, Sozial-, Wirtschafts-, Siedlungs-, Begräbnis- und Ritualraum.

#### § 4 Erlaubnispflichtige Maßnahmen

(1) Im Grabungsschutzgebiet bedarf gemäß § 9 Absatz 1 Nummer 5 des Brandenburgischen Denkmalschutzgesetzes einer Erlaubnis, wer die bisherige Bodennutzung verändern will.
Unter einer Veränderung der Bodennutzung ist jegliche Änderung der Nutzung von Flächen zu verstehen.
Dazu zählen zum Beispiel Eingrabungen, Abgrabungen, Auffüllungen und Aufschüttungen, tiefergehende Bodenbearbeitung durch Tiefpflügen, die Umnutzung bisher land- oder forstwirtschaftlicher Flächen in Bauland oder die Umnutzung von Dauergrünland in Ackerland und von Dauergrün- und Ackerland in forstwirtschaftliche Flächen.
Im Übrigen bleiben die Regelungen über erlaubnispflichtige Maßnahmen gemäß den §§ 9 und 10 des Brandenburgischen Denkmalschutzgesetzes unberührt.
Unberührt bleibt auch die bisherige land-, forst- und fischereiwirtschaftliche Nutzung.

(2) Die bestehende Verkehrsinfrastruktur, das heißt Straßen gemäß § 1 Absatz 4 des Bundesfernstraßengesetzes beziehungsweise § 2 Absatz 2 des Brandenburgischen Straßengesetzes (insbesondere Fahrbahnen, Böschungen, Entwässerungsanlagen, Ver- und Entsorgungsleitungen/-anlagen sowie Alleenpflanzungen) sowie Eisenbahn, fällt nicht unter den Geltungsbereich der Grabungsschutzverordnung.
Die bestehende Verkehrsinfrastruktur mit dem sich entwickelnden Verkehr genießt Bestandsschutz und wird in ihrer bestimmungsgerechten Nutzung nicht beeinträchtigt.
Dies gilt insbesondere für die Unterhaltung, Überwachung und Grunderneuerung von Verkehrsinfrastruktur.

#### § 5 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 12.
Juli 2016

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Dr.
Martina Münch

### Anlagen

1

[Anlage 1 - Übersichtskarte zu § 2 Absatz 1 der Verordnung über das Grabungsschutzgebiet Siedlungs- und Ritualraum Königsgrab Seddin, Blatt 1](/br2/sixcms/media.php/68/GVBl_II_40_2016_Anlage-1-Blatt-1.pdf "Anlage 1 - Übersichtskarte zu § 2 Absatz 1 der Verordnung über das Grabungsschutzgebiet Siedlungs- und Ritualraum Königsgrab Seddin, Blatt 1") 3.8 MB

2

[Anlage 1 - Übersichtskarte zu § 2 Absatz 1 der Verordnung über das Grabungsschutzgebiet Siedlungs- und Ritualraum Königsgrab Seddin, Blatt 2](/br2/sixcms/media.php/68/GVBl_II_40_2016_Anlage-1-Blatt-2.pdf "Anlage 1 - Übersichtskarte zu § 2 Absatz 1 der Verordnung über das Grabungsschutzgebiet Siedlungs- und Ritualraum Königsgrab Seddin, Blatt 2") 4.0 MB

3

[Anlage 1 - Übersichtskarte zu § 2 Absatz 1 der Verordnung über das Grabungsschutzgebiet Siedlungs- und Ritualraum Königsgrab Seddin, Blatt 3](/br2/sixcms/media.php/68/GVBl_II_40_2016_Anlage-1-Blatt-3.pdf "Anlage 1 - Übersichtskarte zu § 2 Absatz 1 der Verordnung über das Grabungsschutzgebiet Siedlungs- und Ritualraum Königsgrab Seddin, Blatt 3") 3.9 MB

4

[Anlage 2 (zu § 2 Absatz 2)](/br2/sixcms/media.php/68/GVBl_II_40_2016_Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2)") 890.9 KB