## Verordnung zur Übertragung der Ermächtigung zum Erlass von Rechtsverordnungen nach dem Gemeindefinanzreformgesetz  (Gemeindefinanzen-Ermächtigungsübertragungsverordnung - GemFinEÜV)

Auf Grund des § 8 des Gemeindefinanzreformgesetzes in der Fassung der Bekanntmachung vom 10. März 2009 (BGBl. I S. 502) verordnet die Landesregierung:

#### § 1 

Die Ermächtigung der Landesregierung zum Erlass von Rechtsverordnungen nach den §§ 2, 4 Absatz 2, §§ 5, 5a Absatz 3 Satz 3, § 5d Absatz 2 und § 6 Absatz 8 des Gemeindefinanzreformgesetzes in der Fassung der Bekanntmachung vom 10. März 2009 (BGBl. I S. 502), das zuletzt durch Artikel 6 des Gesetzes vom 17. Dezember 2018 (BGBl. I S. 2522, 2523) geändert worden ist, wird auf das Ministerium der Finanzen übertragen.

#### § 2 

Diese Verordnung tritt am Tag nach der Verkündigung in Kraft.
Gleichzeitig tritt die Verordnung zur Übertragung der Ermächtigung zum Erlass von Rechtsverordnungen nach dem Gemeindefinanzreformgesetz vom 23. September 2003 (GVBl.
II S. 579) außer Kraft.

Potsdam, den 9.
Juli 2019

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter  
  

Der Minister der Finanzen

Christian Görke