## Verordnung über Zuständigkeiten nach dem Energieverbrauchskennzeichnungsgesetz (EnVKG-Zuständigkeitsverordnung - EnVKG-ZV)

Auf Grund des § 9 Absatz 2 und 3 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl. I S. 186) und des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl.
I S. 602) verordnet die Landesregierung:

### § 1   
Zuständigkeiten

(1) Das Ministerium für Arbeit, Soziales, Gesundheit, Frauen und Familie ist oberste Landesbehörde im Sinne des § 6 Absatz 2 sowie des § 12 Absatz 1 und 2 des Energieverbrauchskennzeichnungsgesetzes vom 10. Mai 2012 (BGBl.
I S. 1070).

(2) Das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit ist:

1.  zuständige Behörde für die Durchführung des Energieverbrauchskennzeichnungsgesetzes in Verbindung mit den auf Grund dieses Gesetzes erlassenen Rechtsverordnungen und den Verordnungen der Europäischen Union im Sinne des § 2 Nummer 2 des Energieverbrauchskennzeichnungsgesetzes, soweit nicht Rechtsvorschriften die Zuständigkeit anderer Behörden begründen, sowie
    
2.  zuständig für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 15 Absatz 1 des Energieverbrauchskennzeichnungsgesetzes.
    

### § 2   
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über Zuständigkeiten nach der Energieverbrauchskennzeichnungsverordnung, der Energieverbrauchshöchstwerteverordnung sowie der Pkw-Energieverbrauchskennzeichnungsverordnung vom 6.
Dezember 2005 (GVBl.
II S. 582, 583) außer Kraft.

Potsdam, den 30.
Mai 2014

Die Landesregierung des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister für Arbeit, Soziales, Frauen und Familie

Günter Baaske