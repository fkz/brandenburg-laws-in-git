## Verordnung über die beamtenrechtlichen Zuständigkeiten im Geschäftsbereich des Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft (Beamtenzuständigkeitsverordnung MLUL - BZVMLUL)

Auf Grund

*   des § 4 Absatz 1 Satz 2 des Landesbeamtengesetzes vom 3.
    April 2009 (GVBl.
    I S. 26) in Verbindung mit § 1 Absatz 1 Satz 2 und Absatz 3 Satz 1 der Ernennungsverordnung vom 1. August 2004 (GVBl.
    II S. 742),
*   des § 32 Absatz 1, des § 38 Satz 2, des § 54 Absatz 1, des § 56 Absatz 1 Satz 5, des § 57 Absatz 1 Satz 2, des § 69 Absatz 5 Satz 1, des § 84 Satz 2, des § 85 Absatz 2 Satz 1, des § 86 Absatz 1 Satz 3, des § 87 Satz 4, des § 88 Satz 5, des § 89 Satz 3, des § 92 Absatz 2 zweiter Halbsatz, des § 103 Absatz 2 des Landesbeamtengesetzes,
*   des § 17 Absatz 2 Satz 2, des § 34 Absatz 5, des § 35 Absatz 2 Satz 2 und des § 42 Absatz 2 Satz 2 des Landesdisziplinargesetzes vom 18.
    Dezember 2001 (GVBl.
    I S. 254),
*   des § 64 Absatz 1 des Landesbeamtengesetzes, der durch das Gesetz vom 11.
    März 2010 (GVBl.
    I Nr.
    13) geändert worden ist, in Verbindung mit § 8 Absatz 1 der Verordnung über die Gewährung von Jubiläumszuwendungen an Beamte und Richter des Bundes in der Fassung der Bekanntmachung vom 13.
    März 1990 (BGBl.
    I S. 487) und in Verbindung mit § 9 Absatz 1 des Landesorganisationsgesetzes vom 24.
    Mai 2004 (GVBl.
    I S. 186),
*   des § 54 Absatz 3 Satz 2 des Beamtenstatusgesetzes vom 17.
    Juni 2008 (BGBl.
    I S. 1010) in Verbindung mit § 9 Absatz 1 des Landesorganisationsgesetzes,
*   des § 25 des Landesbeamtengesetzes in Verbindung mit § 39 Absatz 1 der Laufbahnverordnung vom 16.
    September 2009 (GVBl.
    II S. 622) und § 9 Absatz 1 des Landesorganisationsgesetzes,
*   des § 71 des Landesbeamtengesetzes in Verbindung mit § 8 Absatz 6 des Mutterschutzgesetzes vom 20.
    Juni 2002 (BGBl.
    I S. 2318),
*   des § 12 Absatz 2 Satz 3 des Bundesbesoldungsgesetzes in der Fassung der Bekanntmachung vom 19.
    Juni 2009 (BGBl.
    I S. 1434) in Verbindung mit § 9 Absatz 1 des Landesorganisationsgesetzes

verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1  
Übertragung der Ernennungsbefugnis

Dem Landesamt für Umwelt, dem Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung sowie dem Landesbetrieb Forst Brandenburg wird die Befugnis zur Ernennung der Beamten, denen ein Amt des einfachen, des mittleren, des gehobenen Dienstes oder des höheren Dienstes der Besoldungsgruppen A 13, A 14 und A 15 verliehen wird, übertragen.

### § 2  
Übertragung weiterer Befugnisse

(1) Den in § 1 genannten Dienststellen werden für ihren Geschäftsbereich folgende weitere beamtenrechtliche Zuständigkeiten übertragen:

1.  Entscheidungen über das Verbot der Führung der Dienstgeschäfte gemäß § 54 Absatz 1 des Landesbeamtengesetzes,
2.  Entscheidungen über das Vorliegen der Voraussetzungen der Entlassung kraft Gesetzes gemäß § 32 Absatz 1 des Landesbeamtengesetzes,
3.  Entscheidungen über die Versetzung eines Beamten auf Probe in den Ruhestand gemäß § 38 des Landesbeamtengesetzes,
4.  Entscheidungen über die Versagung der Aussagegenehmigung gemäß § 56 des Landesbeamtengesetzes, wobei die Versagung der Aussagegenehmigung der Zustimmung der obersten Dienstbehörde bedarf,
5.  Nebentätigkeitsangelegenheiten und Untersagungen von Tätigkeiten nach Beendigung des Beamtenverhältnisses gemäß den §§ 84 bis 89 und 92 des Landesbeamtengesetzes, soweit nicht die Entscheidung gemäß § 87 Satz 1 des Landesbeamtengesetzes dem für Ländliche Entwicklung, Umwelt und Landwirtschaft zuständigen Ministerium vorbehalten ist,
6.  die Erteilung der Zustimmung zur Annahme von Belohnungen und Geschenken gemäß § 57 des Landesbeamtengesetzes mit der Maßgabe, dass die Entscheidung dem jeweiligen Leiter der betreffenden Dienststelle oder dessen Stellvertreter persönlich obliegt,
7.  die Genehmigung der Führung der Amtsbezeichnung mit dem Zusatz „außer Dienst“ („a.D.“) gemäß § 69 des Landesbeamtengesetzes,
8.  die Ausübung der Disziplinarbefugnis bei Ruhestandsbeamten nach § 17 Absatz 2 des Landesdisziplinargesetzes,
9.  die Befugnis zur Gewährung und Versagung der Jubiläumszuwendung gemäß § 8 der Verordnung über die Gewährung von Jubiläumszuwendungen an Beamte und Richter des Bundes,
10.  Entscheidungen über die Feststellung der Befähigung für eine Laufbahn besonderer Fachrichtung des mittleren und des gehobenen Dienstes nach § 39 der Laufbahnverordnung,
11.  Entscheidungen gemäß § 71 des Landesbeamtengesetzes in Verbindung mit § 1 Absatz 1 der Mutterschutz- und Elternzeitverordnung,
12.  die Zustimmung zum Absehen von der Rückforderung von Bezügen nach § 12 Absatz 2 Satz 3 des Bundesbesoldungsgesetzes,
13.  die Kürzung der Dienstbezüge bis zum Höchstmaß nach § 34 Absatz 3 Nummer 1 des Landesdisziplinargesetzes.

(2) Dem Leiter der in § 1 genannten Dienststellen wird die Befugnis zur Erhebung der Disziplinarklage nach § 35 des Landesdisziplinargesetzes übertragen.

### § 3  
Befugnis zum Erlass von Widerspruchsbescheiden

Die Entscheidung über den Widerspruch von Beamten, Beamten im Ruhestand, früheren Beamten sowie der Hinterbliebenen gegen den Erlass oder die Ablehnung eines das Beamtenverhältnis betreffenden Verwaltungsaktes oder gegen die Ablehnung des Anspruchs auf eine Leistung aus dem Beamtenverhältnis wird dem Leiter der in § 1 genannten Dienststellen übertragen, soweit diese Dienststelle die mit dem Widerspruch angefochtene Entscheidung erlassen hat.

### § 4  
Vertretung bei Klagen aus dem Beamtenverhältnis

Die Vertretung des Landes vor den Gerichten der allgemeinen Verwaltungsgerichtsbarkeit wird den in § 1 genannten Dienststellen übertragen, soweit sie über den Widerspruch entschieden hat.
Satz 1 ist in Verfahren des einstweiligen Rechtsschutzes (§§ 80 bis 80b und 123 der Verwaltungsgerichtsordnung) entsprechend anzuwenden.

### § 5  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Beamtenzuständigkeitsverordnung MLUR vom 27.
August 2001 (GVBl.
II S. 550), die zuletzt durch die Verordnung vom 5.
Mai 2005 (GVBl.
II S. 238) geändert worden ist, außer Kraft, soweit sie den Geschäftsbereich des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz betrifft.

Potsdam, den 23.
Januar 2012

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack