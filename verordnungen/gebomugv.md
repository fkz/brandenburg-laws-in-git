## Gebührenordnung des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (GebOMUGV)

_**Hinweis: Artikel 6 der Verordnung vom 31.
Januar 2022 (GVBl.
II Nr.
19 S.
7) tritt am 1. Januar 2029 in Kraft**_.

Auf Grund des § 3 Absatz 1 des Gebührengesetzes für das Land Brandenburg vom 7. Juli 2009 (GVBl. I S. 246) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1  
Gebührentarif

Für die in den Anlagen 1 und 2 (außer Tarifstelle 1) genannten Amtshandlungen werden die dort genannten Verwaltungsgebühren erhoben.
Für die in der Anlage 2, Tarifstelle 1 genannte Inanspruchnahme öffentlicher Einrichtungen und Anlagen werden die dort genannten Benutzungsgebühren erhoben.

### § 2  
Mehrfache Amtshandlungen

Zur Abgeltung mehrfacher Amtshandlungen, die denselben Schuldner und dieselbe Tarifstelle betreffen, können die Gebühren für einen im Voraus zu bestimmenden Zeitraum von höchstens einem Jahr auf Antrag pauschal festgesetzt werden.

### § 3   
Gebührenbemessung

(1) Soweit Gebühren nach dem erforderlichen Zeitaufwand zu berechnen sind, sind der Gebührenberechnung als Stundensätze zugrunde zu legen:

a.

für Beamtinnen oder Beamte des höheren Dienstes und vergleichbare Beschäftigte

65,00 EUR

b.

für Beamtinnen oder Beamte des gehobenen Dienstes und vergleichbare Beschäftigte

51,00 EUR

c.

für Beamtinnen oder Beamte des mittleren Dienstes und vergleichbare Beschäftigte

41,00 EUR

d.

für Beamtinnen oder Beamte des einfachen Dienstes und vergleichbare Beschäftigte

32,00 EUR.

Bei der Ermittlung der Zeitgebühren ist die Zeit anzusetzen, die unter regelmäßigen Verhältnissen von einer entsprechend ausgebildeten Fachkraft benötigt wird.
Die Zeit für Ortsbesichtigungen, einschließlich der An- und Abreise, ist einzurechnen.

(2) Soweit die Leistungen nach dieser Gebührenordnung umsatzsteuerpflichtig sind, wird zu der Gebühr die Umsatzsteuer in der jeweils gesetzlichen Höhe hinzugerechnet.

### § 4  
Gebührenfreiheit

Bei anerkannten Vereinigungen im Sinne des § 3 des Umwelt-Rechtsbehelfsgesetzes soll gemäß § 3 Absatz 2 Nummer 2 des Gebührengesetzes für das Land Brandenburg von der Gebühren- und Auslagenerhebung für öffentliche Leistungen ganz abgesehen werden, wenn die in diesem Zusammenhang wahrgenommene Tätigkeit der anerkannten Vereinigung ihrem satzungsgemäßen Zweck unmittelbar dient.

### § 5   
Einschränkung der persönlichen Gebührenfreiheit

(1) Zur Zahlung von Gebühren für Amtshandlungen der Wasserbehörden nach Tarifstelle 5.1.5.1 der Anlage 2 sowie für die Amtshandlungen nach Tarifstelle 5.1.6 der Anlage 2 für die mit den vorgenannten Amtshandlungen zugelassenen Anlagen bleiben die Gemeinden und Gemeindeverbände gemäß § 3 Absatz 2 Nummer 1 des Gebührengesetzes für das Land Brandenburg verpflichtet.
Zur Zahlung von Gebühren für Amtshandlungen der Gesundheitsämter nach den Tarifstellen 9.32 und 9.33 der Anlage 2 bleiben die Gemeinden und Gemeindeverbände und für Amtshandlungen des Landesamtes für Umwelt, Gesundheit und Verbraucherschutz Brandenburg nach der Tarifstelle 2.2.11 der Anlage 2 die Träger der Baulast ebenfalls verpflichtet.
Zur Zahlung von Gebühren für Amtshandlungen der Wasserbehörden nach den Tarifstellen der Anlage 2 bleiben die Gewässerunterhaltungsverbände verpflichtet.

(2) Zur Zahlung von Gebühren für Amtshandlungen der Gesundheitsämter nach den Tarifstellen 7.8.4 und 7.9 der Anlage 2 bleiben die in § 8 Absatz 1 des Gebührengesetzes für das Land Brandenburg genannten juristischen Personen des öffentlichen Rechts verpflichtet.

### § 6  
Inkrafttreten, Außerkrafttreten

(1) Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig treten die §§ 4 und 6 sowie Anlage 2 Tarifstelle 1 bis 6, 13.1 bis 13.9, 13.11 bis 13.25 und 13.27 bis 13.34 der Gebührenordnung des Ministeriums für Ländliche Entwicklung, Umwelt und Verbraucherschutz vom 17.
Juli 2007 (GVBl.
II S. 314), die zuletzt durch Artikel 2 der Verordnung vom 28.
März 2011 (GVBl.
II Nr. 18) geändert worden ist, außer Kraft.

Potsdam, den 22.
November 2011

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

* * *

**Anlage** 

Inhaltsübersicht des Gebührentarifs der Anlagen 1 und 2 (Überschriften in Kurzform):

### Anlage 1  
Allgemeine Gebühren

**1**

**Allgemeine Gebühren**

1.1

Beglaubigungen, Bescheinigungen und Urkunden, Zeugnisse

1.2

Anfertigung von Zweitschriften, Kopien, Computerausdrucken

1.3

Vervielfältigung von Karten

1.4

Nutzung von Diensträumen

1.5

Sonstiges

### Anlage 2  
Gebühren für die Bereiche Immissionsschutz, Abfall- und Wasserrecht, Boden- und Naturschutz, Verbraucherschutz, Gesundheit, allgemeiner Umweltschutz

**1**

**Annahme und Verwahrung von radioaktiven Stoffen (Benutzungsgebühren)**

1.1

Annahme und Verwahrung von radioaktiven Abfällen

1.2

Verwahrung von sonstigen Strahlenquellen

 

 

**2**

**Immissionsschutzrechtliche Angelegenheiten**

2.1

Genehmigungsbedürftige Anlagen

2.2

Sonstige Amtshandlungen nach dem BImSchG

2.3

Amtshandlungen nach den Verordnungen zur Durchführung des BImSchG

2.4

Landesimmissionsschutzgesetz

2.5

Technische Anleitung zur Reinhaltung der Luft

2.6

Chemikalienrechtliche Angelegenheiten

2.7

Gentechnikrechtliche Angelegenheiten

2.8

Atomrechtliche Angelegenheiten

2.9

Messung von Radioaktivität und elektromagnetischen Feldern

2.10

Überwachungserleichterungen

2.11

Gesetz über die Umweltverträglichkeitsprüfung (UVPG)

2.12

Treibhausgas-Emissionshandelsgesetz

2.13

Erneuerbare-Energien-Gesetz

2.14

Gesetz zum Schutz gegen Fluglärm

 

 

**3**

**Abfall- und bodenschutzrechtliche Angelegenheiten**

3.1

Amtshandlungen nach dem Kreislaufwirtschaftsgesetz

3.2

Amtshandlungen nach der Klärschlammverordnung

3.3

Ausnahmegenehmigungen nach der Altölverordnung

3.4

Verpackungsverordnung

3.5

Nachweisverordnung (NachwV)

3.6

POP-Abfall-Überwachungs-Verordnung (POP-Abfall-ÜberwV)

3.7

Verordnung über Entsorgungsfachbetriebe

3.8

Richtlinie für die Tätigkeit und die Anerkennung von Entsorgergemeinschaften

3.9

Abfallkompost- und Verbrennungsverordnung

3.10

Umweltrahmengesetz der DDR vom 29.
Juni 1990 (URG)

3.11

Brandenburgisches Abfall- und Bodenschutzgesetz

3.12

Altfahrzeugverordnung

3.13

Batteriegesetz

3.14

Bioabfallverordnung

3.15

Verordnung über Betriebsbeauftragte für Abfall

3.16

Bundes-Bodenschutzgesetz

3.17

Anzeige- und Erlaubnisverordnung (AbfAEV)

3.18

Gewinnungsabfallverordnung

3.19

Abfallverzeichnis-Verordnung

3.20

Altholzverordnung

3.21

Deponieverordnung

3.22

Gewerbeabfallverordnung

3.23

Verordnung (EG) Nr. 1013/2006 des Europäischen Parlaments und des Rates über die Verbringung von Abfällen, Abfallverbringungsgesetz

3.24

Persistente organische Schadstoffe

3.25

Elektro- und Elektronikgerätegesetz

 

 

**4**

**Naturschutzrechtliche Angelegenheiten**

4.1

Schutz bestimmter Teile von Natur und Landschaft

4.2

Eingriff

4.3

Besondere Genehmigungen, Prüfungen und Maßnahmen

4.4

Sonstige Entscheidungen und Maßnahmen 

4.5

Besonderer Artenschutz

4.6

Eingeschlossene oder ersetzte naturschutzrechtliche Entscheidungen

 

 

**5**

**Wasserrechtliche Angelegenheiten**

5.1

Amtshandlungen auf Grund des WHG und des BbgWG

5.2

Amtshandlungen nach der Indirekteinleiterverordnung

5.3

Bescheinigung nach § 7 der Sachenrechts-Durchführungsverordnung

 

 

**6**

**Teilnahme an Ringversuchen**

6.1

Grundgebühr

6.2

Probengebühr

6.3

Parametergruppengebühr

 

 

**7 bis 7.13**

**_  
außer Kraft getreten_**

 

 

**8**

**_Nicht besetzt_**

 

 

**9**

**Veterinärwesen, Lebens- und Futtermittelüberwachung sowie Wasserhygiene**

9.1

Gebühren in Bezug auf das Berufs- und Standesrecht

9.2

Gebühren für Beratungstätigkeit und die Erstellung von Gutachten

9.3

Gebühren auf Grund des Tiererzeugnisse-Handels-Verbotsgesetzes und der zu diesem Gesetz erlassenen bundes- oder landesrechtlichen Vorschriften

9.4

_Nicht besetzt_

9.5

Gebühren auf Grund des Tierseuchengesetzes und der zu diesem Gesetz erlassenen bundes- oder landesrechtlichen Vorschriften

9.6

Gebühren für Amtshandlungen nach der Verordnung (EG) Nr. 1069/2009 in der jeweils geltenden Fassung sowie nach den auf Grund dieser Verordnung oder zu ihrer Durchführung ergangenen Rechtsakten der Europäischen Union, des Bundes und des Landes Brandenburg

9.7

Gebühren auf Grund des Tierschutzgesetzes und der zu diesem Gesetz erlassenen bundes- oder landesrechtlichen Vorschriften

9.8

Gebühren auf Grund des Arzneimittelgesetzes und der zu diesem Gesetz erlassenen bundes- oder landesrechtlichen Vorschriften im Bereich der Tierarzneimittel

9.9

_Nicht besetzt_

9.10

_Nicht besetzt_

9.11

Gebühren im Bereich Lebensmittelüberwachung

9.12

Erhebung von Gebühren nach Artikel 27 Absatz 2 der Verordnung (EG) Nr. 882/2004 für die in Anhang IV Abschnitt A der Verordnung genannten Tätigkeiten

9.13

Gebühr für sonstige Amtshandlungen

9.14

Trichinenuntersuchung von Tieren, die keiner Schlacht- und Fleischuntersuchung nach EG-Recht unterliegen

9.15

Probenahme zwecks sonstiger Untersuchungen von Tieren

9.16

Erhebung von Gebühren nach Artikel 27 Absatz 2 der Verordnung (EG) Nr. 882/2004 für die in Anhang V Abschnitt A der Verordnung genannten Tätigkeiten im Zusammenhang mit den amtlichen Kontrollen von Waren und lebenden Tieren, die in die Gemeinschaft eingeführt werden

9.17

Amtshandlungen nach der Lebensmitteleinfuhr-Verordnung

9.18

Amtshandlungen anlässlich der Rücksendung oder unschädlichen Beseitigung von Sendungen

9.19

Amtshandlungen im Zusammenhang mit Quarantänemaßnahmen

9.20

Weitere Amtshandlungen im Lebensmittelrecht

9.21

Entscheidungen und sonstige Amtshandlungen im Anwendungsbereich der Verordnung (EG) Nr. 853/2004 und anderer Vorschriften

9.22

Entscheidungen und sonstige Amtshandlungen außerhalb des Anwendungsbereichs der Verordnung (EG) Nr. 853/2004

9.23

Anordnung oder Aussetzung der Milchanlieferung nach Anhang IV Kapitel II Nummer 2 der Verordnung (EG) Nr. 854/2004

9.24

Gebühren nach der Mineral- und Tafelwasser-Verordnung

9.25

Ausnahmegenehmigung nach § 9 Absatz 7 des Vorläufigen Biergesetzes

9.26

_Besondere_  Amtshandlungen im Weinrecht

9.27

Amtshandlungen nach der Lebensmittelkontrolleur-Verordnung

9.28

Gebühren im Bereich Futtermittelüberwachung

9.29

Entscheidung über den Antrag auf Zulassung eines Prüflabors nach § 4 der Tabakprodukt-Verordnung

9.30

Entscheidung über Anträge auf Erteilung einer Registriernummer nach § 5a der Kosmetikverordnung

9.31

Besondere Grundsätze der Tarifstelle 9

9.32

Gebühren auf Grund des Infektionsschutzgesetzes (IfSG) in Verbindung mit der Trinkwasserverordnung (TrinkwV 2001)

9.33

Gebühren auf Grund des Infektionsschutzgesetzes (IfSG) in Verbindung mit der DIN 19643

9.34

Amtshandlungen auf Grund des Brandenburgischen Gesundheitsdienstgesetzes (BbgGDG)

 

 

**10**

**Sachverständigenwesen**

10.1

Antragsgebühr

10.2

Bestellungsgebühr

10.3

Wiederbestellung früherer Sachverständiger

10.4

Sachkundenachweise

10.5

Bestellung als Probenehmer

10.6

Verlängerung der Bestellung als Probenehmer

 

 

**11**

**Gebühren für die Abnahme von Prüfungen und sonstige Angelegenheiten nach dem Berufsbildungsgesetz**

11.1

Berufsabschlussprüfungen

11.2

Prüfungen gemäß Ausbilder-Eignungsverordnung

11.3

Fortbildungsprüfungen

11.4

Wiederholung von Prüfungen oder Prüfungsmodulen

11.5

Anerkennung der Gleichwertigkeit von Berufsabschlüssen

 

 

**12**

**Amtshandlungen nach dem Verbraucherinformationsgesetz (VIG)**

12.1

Gebühren für Amtshandlungen nach dem Verbraucherinformationsgesetz

12.2

Auslagen

 

 

**13**

**Amtshandlungen auf der Grundlage des Einkommensteuergesetzes**

 

 

**14**

**Vollzug allgemeiner umweltrechtlicher Vorschriften**

### Anlage 1  
Allgemeine Gebühren

Tarifstelle

Gegenstand

Gebühr (EUR)

**1**

**Allgemeine Gebühren**

 

**1.1**

**Beglaubigungen, Bescheinigungen und Urkunden, Zeugnisse**

 

1.1.1

Beglaubigungen von Unterschriften oder Handzeichen

2,50 bis 25

1.1.2

Beglaubigungen von Abschriften, Ablichtungen usw., je Seite

1 bis 2,50

1.1.3

Sonstige Bescheinigungen, Urkunden

1 bis 51

1.1.4

Zeugnisse (z.
B.
Ursprungszeugnisse)

1 bis 25

**1.2**

**Anfertigung von Zweitschriften, Kopien, Computerausdrucken**

 

1.2.1

für die ersten 50 Seiten DIN-A4, schwarz-weiß, je Seite

0,50

1.2.2

jede weitere Seite

0,15

1.2.3

für Seiten im Format DIN-A3, je Seite

1

1.2.4

Farbkopien, je Seite

1 bis 5

1.2.5

Erstellen von Fotodokumentationen/Lichtbildmappen 

nach Aufwand,  
mindestens aber 10

**1.3**

**Vervielfältigung von Karten**

 

1.3.1

als Schwarz-Weiß-Kopie  
  
DIN-A4  
DIN-A3  
DIN-A2  
DIN-A1  
DIN-A0

 0,5  
1  
2  
4  
8

1.3.2

als Farbkopie oder Computerausdruck  
  
bis DIN-A3  
größer DIN-A3

   
  
5  
8

**1.4**

**Nutzung von Diensträumen, je angefangene Stunde**

**15,5**

 

Anfallende Reinigungskosten sind besonders in Rechnung zu stellen.
Für die Nutzung von technischen Geräten sind je nach Ausstattung und Grad der Beanspruchung weitere Zuschläge zu erheben.

 

**1.5**

**Sonstiges**

 

1.5.1

Einsatz von Dienstkraftfahrzeugen für Dritte

Gebühr richtet sich hinsichtlich der Fahrkosten je Kilometer nach den Sätzen gemäß Anlage 5 zu Nummer 10.3 der Dienstkraftfahrzeug-Richtlinie

1.5.2

_Nicht besetzt_

 

1.5.3

Rechtsbehelfe 

 

1.5.3.1

bei Drittwidersprüchen

26 bis 1 023

1.5.3.2

bei Widersprüchen gegen Kostenentscheidungen

Gebühr richtet sich nach § 18 Absatz 1 GebGBbg

1.5.3.3

bei Widerspruch durch den Adressaten der Sachentscheidung

Gebühr richtet sich nach § 18 Absatz 1 GebGBbg 

1.5.4

Entscheidung über die aufschiebende Wirkung von Rechtsbehelfen, soweit nicht mit der Hauptentscheidung verbunden

26 bis 2 556

1.5.5

Sonstige Amtshandlungen

 

1.5.5.1

Entscheidung über die Anerkennung einer Untersuchungsstelle im Bereich der Umweltanalytik (soweit keine spezielle Tarifstelle Anwendung findet)

256 bis 2 556

 

– soweit sich die Tätigkeit der Untersuchungsstelle lediglich auf die Probenahme bezieht

51 bis 256 

1.5.5.2

Amtshandlungen, für die keine andere Tarifstelle vorgesehen ist und die nicht einem von der handelnden Behörde wahrzunehmenden besonderen Interesse dienen

0 bis 10 000

### Anlage 2  
Gebühren für die Bereiche Immissionsschutz, Abfall- und Wasserrecht, Boden- und Naturschutz, Verbraucherschutz, Gesundheit, allgemeiner Umweltschutz

Tarifstelle

Gegenstand

Gebühr (EUR)

**1**

**Annahme und Verwahrung von radioaktiven Stoffen (Benutzungsgebühren)**

 

**1.1**

**Annahme und Verwahrung von radioaktiven Abfällen (§ 9a Absatz 3 des Atomgesetzes in Verbindung mit der Strahlenschutzverordnung)**

 

1.1.1

Verwahrung von

 

1.1.1.1

umschlossenen Strahlenquellen, Prüfstrahlern, Präparaten

Preis auf Anfrage (abhängig von Nuklid und Aktivität)

1.1.1.2

festen, nicht brennbaren Abfällen, die nicht umschlossene Strahlenquellen sind, bis zu einer Aktivität von einschließlich 1 MBq und bis zu einer Nettomasse von einschließlich 1 kg

Aktivität je weiteres angefangenes MBq zusätzlich

Nettomasse je weiteres angefangenes kg zusätzlich

 

350  
  
  
5  
  
30

1.1.1.3

je 70 Liter Fass

 ab 2 500

1.1.1.4

je 200 Liter Fass

ab 5 000

1.1.1.5

sonstigen Endlagergebinden, bis 1 m3

bis 17 500

1.1.1.6

Endlagergebinden größer als 1 m3

ab 17 500

1.1.2

Vorausleistungen für die Endlagerung von

 

1.1.2.1

umschlossenen Strahlenquellen, Prüfstrahlern, Präparaten, je Stück

Preis auf Anfrage (abhängig von Nuklid und Aktivität)

1.1.2.2

je 70 Liter Fass

ab 1 200

1.1.2.3

je 200 Liter Fass

ab 3 000

**1.2**

**Verwahrung von sonstigen Strahlenquellen (§§ 76, 78 der Strahlenschutzverordnung – StrlSchV)**

**Preis auf Anfrage (abhängig von Nuklid und Aktivität)**

**2**

**Immisssionsschutzrechtliche Angelegenheiten**

 

**2.1**

**Genehmigungsbedürftige Anlagen**[\*)](#_ftn2)

2.1.1

Immissionsschutzrechtliche Entscheidung über die

*   Genehmigung nach den §§ 4, 6 des Bundes-Immissionsschutzgesetzes (BimSchG),
*   Teilgenehmigung nach § 8 BImSchG oder
*   Genehmigung einer Änderung nach § 16 BImSchG

einer im Anhang der Verordnung über genehmigungsbedürftige Anlagen (4.
BImSchV) genannten Anlage mit Errichtungskosten (E)

1.  Entscheidung über die Genehmigung

> wird ein Sachverständigengutachten im Sinne des § 13 Absatz 2 Satz 2 der 9.
> BImSchV beauftragt

180 + 0,5 Prozent von E

reduziert sich die Gebühr nach Buchstabe a um 3 Prozent

2.  ist ausschließlich die Regelung des Betriebes Gegenstand eines Teil- oder Änderungsverfahrens 

170 bis 3 500

3.  wird im Genehmigungsverfahren ein Erörterungstermin (§ 10 Absatz 6 BImSchG) durchgeführt, erhöht sich die Gebühr nach Buchstabe a um  
      
     

> wird hierbei auf Kosten des Antragstellers für die Vor- und Nachbereitung (technische Organisation, Zusammenfassung von Einwendungen, Erstellen von Einwendungslisten, Einlasskontrolle beim Termin, Fertigen der Niederschrift) ein externes Projektmanagement eingesetzt

170 je Stunde, höchstens jedoch 900 für jeden Tag, an dem Erörterungen stattgefunden haben

reduziert sich die Gebühr nach Buchstabe c um 10 bis 50 Prozent

4.  wird im Genehmigungsverfahren eine Prüfung der Umweltverträglichkeit vorgenommen  
      
      
      
     

> kann der Umfang der Prüfung der Umweltverträglichkeit beschränkt werden, weil ihr ein Raumordnungsverfahren (§ 16 Absatz 3 UVPG) oder ein Bebauungsplan oder anderes Satzungsverfahren (§ 17 Satz 3 UVPG) vorausgegangen ist

10 Prozent des sich aus den Buchstaben a und b ergebenden Betrages, mindestens jedoch 700, höchstens 27 000

reduziert sich die Gebühr nach Buchstabe d um 30 bis 50 Prozent

5.  wird im Genehmigungsverfahren eine Vorprüfung über die Feststellung der UVP-Pflicht im Einzelfall gemäß § 3c UVPG mit negativem Ergebnis vorgenommen

3 Prozent des sich aus den Buchstaben a und b ergebenden Betrages, mindestens jedoch 170, höchstens 9 000

6.  wird vor Beginn eines Genehmigungsverfahrens auf Ersuchen des Vorhabensträgers eine Unterrichtung über den Umfang beizubringender Unterlagen nach § 2a der 9.
    BImSchV durchgeführt

3 Prozent des sich aus den Buchstaben a und b ergebenden Betrages, mindestens jedoch 170, höchstens 9 000

Wird ein Genehmigungsverfahren durchgeführt, so entfällt die Gebühr für die Unterrichtung über den Umfang beizubringender Unterlagen nach § 2a der 9.
BImSchV vor Beginn des Genehmigungsverfahrens.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Genehmigungsverfahren anzurechnen.

7.  wird vor Beginn eines Genehmigungsverfahrens auf Antrag des Vorhabensträgers oder anlässlich eines Ersuchens nach § 5 UVPG eine Vorprüfung für die UVP-Pflicht im Einzelfall gemäß § 3c in Verbindung mit § 3a UVPG durchgeführt

3 Prozent des sich aus den Buchstaben a und b ergebenden Betrages, mindestens jedoch 170, höchstens 9 000

Wird ein Genehmigungsverfahren durchgeführt, so entfällt die Gebührenpflicht für die positive Feststellung der UVP-Pflicht vor Beginn des Genehmigungsverfahrens.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Genehmigungsverfahren anzurechnen.

8.  wird im Genehmigungsverfahren eine Verträglichkeits- oder Ausnahmeprüfung nach § 34 BNatSchG vorgenommen

5 Prozent bei Anwendung von Buchstabe d,  
2 Prozent des sich aus dem Buchstaben a ergebenden Betrages, mindestens jedoch 350,  
höchstens 15 000

9.  wird im Genehmigungsverfahren die Prüfung eines Sicherheitsberichtes oder von Teilen eines Sicherheitsberichtes gemäß § 4b der Verordnung über das Genehmigungsverfahren (9.
    BImSchV) erforderlich und wird kein Sachverständigengutachten gemäß § 13 der 9.
    BImSchV eingeholt, erhöht sich die Gebühr nach Buchstabe a und b um

3 000 bis 30 000

10.  Entgegennahme und Prüfung des Ausgangszustandsberichts gemäß § 10 Absatz 1a BImSchG

200 bis 2 000

Ergänzend gilt:

1.  Errichtungskosten (E) sind die voraussichtlichen Gesamtkosten der Anlage oder derjenigen Anlagenteile, die nach der (Teil-, Änderungs-)Genehmigung errichtet werden dürfen, einschließlich Mehrwertsteuer.
    Maßgeblich sind die voraussichtlichen Gesamtkosten im Zeitpunkt der Erteilung der (Teil-, Änderungs-)Genehmigung, es sei denn, diese sind niedriger als zum Zeitpunkt der Antragstellung.
    Als Errichtungskosten gelten auch Kosten, die durch den Austausch von Anlagenteilen entstehen.

> Gründungskosten und Kosten für Erdaushubarbeiten sind insoweit einzubeziehen, als diese Maßnahmen aus Anlass der Errichtung oder Änderung der Anlage durchgeführt werden.

> Aufwendungen für die Entwicklung und Vorplanung, für den Erwerb des unbebauten Grundstücks sowie für Zubehör, auf das sich die Genehmigung nicht erstreckt, sind nicht einzubeziehen.

2.  Ergehen mehrere Teilgenehmigungen, ist jede gesondert abzurechnen.
    Im Einzelfall, insbesondere wenn der Prüfaufwand sehr viel niedriger war als im herkömmlichen Genehmigungsverfahren, kann unter den Voraussetzungen des § 20 Nummer 1 des Gebührengesetzes für das Land Brandenburg eine Reduzierung aus Billigkeitsgründen vorgenommen werden.

3.  Ist ein Vorbescheid vorausgegangen oder wird er gleichzeitig mit einer Teilgenehmigung erteilt, werden unabhängig von Gegenstand und Reichweite des Vorbescheides bis insgesamt 10 Prozent der Gebühr nach Tarifstelle 2.1.3 auf die entstehende und gegebenenfalls die nächste(n) anfallende(n) Gebühr(en) nach Tarifstelle 2.1.1 angerechnet.

4.  Gebühren oder Auslagen für die Prüfung bautechnischer Nachweise und für besondere bauaufsichtliche Maßnahmen werden von den Bauaufsichtsbehörden gesondert erhoben.

5.  Reisekosten von Angehörigen der Genehmigungsbehörde oder der Behörden, die durch die Genehmigungsbehörde beteiligt werden, gelten als in die Gebühr einbezogen.
    Satz 1 gilt nicht für Auslandsdienstreisen.

6.  Eine nach Tarifstelle 2.1.5 entrichtete Gebühr wird zu 90 Prozent angerechnet.

2.1.2

Immissionsschutzrechtliche Entscheidung über die Zulassung des vorzeitigen Beginns (§ 8a BlmSchG)

50 Prozent der Gebühr nach Tarifstelle 2.1.1 bezogen auf den Wert des Gegenstandes der Entscheidung

2.1.3

Immissionsschutzrechtliche Entscheidung über einen Antrag auf Erteilung eines Vorbescheides (§ 9 BImSchG)

20 bis 50 Prozent der Gebühr nach Tarifstelle 2.1.1

2.1.4

Immissionsschutzrechtliche Entscheidung über eine Verlängerung der Frist des § 9 Absatz 2 BImSchG

10 Prozent der Gebühr  
nach Tarifstelle 2.1.3,  
mindestens 70

2.1.5

Immissionsschutzrechtliche Prüfung und Bescheidung einer Anzeige zur Änderung der Anlage (§ 15 Absatz 1 und 2 BImSchG)

20 Prozent der Gebühr  
nach Tarifstelle 2.1.1,  
mindestens 70

Ergänzend gilt zu den Tarifstellen 2.1.1 bis 2.1.3 und 2.1.5:

Bei Anlagen, die Teil eines registrierten Standortes nach der Verordnung (EG) Nr. 761/2001 des Europäischen Parlamentes und des Rates vom 19. März 2001 über die freiwillige Beteiligung von Organisationen an einem Gemeinschaftssystem für das Umweltmanagement und die Umweltbetriebsprüfung (EMAS) (ABl.
L 114 vom 24.4.2001, S. 1) sind, soll die Gebühr um 20 Prozent vermindert werden.
Der Betreiber hat die zuständige Behörde über die Registrierung zu unterrichten.

2.1.6

Nachträgliche immissionsschutzrechtliche Anordnung gemäß § 17 Absatz 1, 4, 4a, 4b und 5 BImSchG

180 bis 8 000

2.1.7

Immissionsschutzrechtliche Entscheidung über eine Verlängerung der Frist zur Errichtung oder zum Betrieb der Anlage (§ 18 Absatz 3 BImSchG)

20 Prozent der Gebühr  
nach Tarifstelle 2.1.1,  
mindestens 70

2.1.8

Immissionsschutzrechtliche Untersagung des Betriebes einer Anlage gemäß § 20 Absatz 1 und/oder Absatz 3 Satz 1 BImSchG

300 bis 3 000

2.1.9

Immissionsschutzrechtliche Anordnung der Stilllegung oder Beseitigung einer Anlage gemäß § 20 Absatz 2 BImSchG

700 bis 12 000

2.1.10

Immissionsschutzrechtliche Entscheidung über die Erlaubnis zum Betrieb durch eine zuverlässige Person gemäß § 20 Absatz 3 Satz 2 BImSchG

130 bis 200

2.1.11

Widerruf einer Genehmigung gemäß § 21 BImSchG

300 bis 3 000

**2.2**

**Sonstige Amtshandlungen nach dem BImSchG**

2.2.1

Anordnung gemäß § 24 BImSchG

70 bis 1 400

2.2.2

Untersagung der Errichtung oder des Betriebes einer Anlage gemäß § 25 BImSchG

180 bis 1 800

2.2.3

Entscheidung über die Bekanntgabe einer Messstelle oder einer Stelle zur Überprüfung des ordnungsgemäßen Einbaus, der Funktion und für die Kalibrierung kontinuierlich arbeitender Messgeräte (§ 26 BImSchG)

350 bis 6 700

2.2.4

Entscheidung über die Zulassung des Immissionsschutzbeauftragten zur Durchführung von Ermittlungen (§ 28 Satz 2 BImSchG)

70 bis 700

2.2.5

Anordnung von Messungen gemäß den §§ 26, 28, 29 BImSchG

1.  bei genehmigungsbedürftigen Anlagen

180 bis 1 800

2.  bei nicht genehmigungsbedürftigen Anlagen

70 bis 700

2.2.6

Entscheidung über die Bekanntgabe eines Sach-verständigen (§ 29b Absatz 1 BImSchG)

400 bis 4 500

2.2.7

Entscheidung über die Gestattung von Prüfungen durch den Störfallbeauftragten oder einen Sachverständigen (§ 29a Absatz 1 Satz 2 BImSchG)

180 bis 1 400

2.2.8

Anordnung sicherheitstechnischer Prüfungen gemäß § 29a BImSchG

180 bis 1 800

2.2.9

Ausnahme vom Verbot oder der Beschränkung des Kraftfahrzeugverkehrs gemäß § 40 Absatz 1 Satz 2 BImSchG

1.  für PKW, PKW-Kombi, Krafträder sowie Wohnmobile

22

2.  für LKW und Kraftomnibusse bis 7,5 t des zulässigen Gesamtgewichts

30

3.  für LKW und Kraftomnibusse über 7,5 t des zulässigen Gesamtgewichts

42

2.2.10

Nicht besetzt

2.2.11

Festsetzung der Entschädigung gemäß § 42 Absatz 3 BImSchG

1 Prozent der festgesetzten Entschädigung,  
mindestens 70

2.2.12

Maßnahmen zur Überwachung auf Grund von § 52 Absatz 1 bis 1b BImSchG

1.  erstmalige Begehung und Revision einer neu errichteten oder geänderten genehmigungsbedürftigen Anlage nach Erteilung einer immissionsschutzrechtlichen Genehmigung

10 Prozent der nach Tarifstelle 2.1.1 festgesetzten Gebühr, mindestens 70

2.  Überprüfung einer Anzeige nach § 12 Absatz 2b BImSchG

70 bis 3 200

3.  Überprüfung einer Anzeige nach § 15 Absatz 3 BImSchG

140 bis 3 200

4.  Prüfung der Messberichte von Messungen nach den §§ 26, 28 oder 29 BImSchG unter Einbeziehung des Aufwandes für die Messplanung, Messdurchführung und rechnerische Auswertung der Ergebnisse oder von sicherheitstechnischen Prüfungen oder Unterlagen, soweit nicht nach § 52 Absatz 4 Satz 3 BImSchG kostenfrei

70 bis 700

5.  Prüfung einer Emissionserklärung (§ 27 BImSchG in Verbindung mit der Emissionserklärungsverordnung \[11. BImSchV\])

135 bis 1 600

6.  Überprüfung des Sicherheitsberichts außerhalb von Genehmigungsverfahren (§ 9 Absatz 4 in Verbindung mit § 13 der Störfall-Verordnung \[12.
    BImSchV\] gegebenenfalls in Verbindung mit landesrechtlicher Verweisung)

Zeitgebühr zuzüglich Auslagen für Gutachter (§ 10 GebGBbg)

7.  Vor-Ort-Inspektionen, Bericht und Festlegung von Folgemaßnahmen gemäß § 16 der 12.
    BImSchV (gegebenenfalls in Verbindung mit landesrechtlicher Verweisung), soweit nicht nach § 52 Absatz 4 Satz 3 BImSchG kostenfrei

70 bis 17 500 zuzüglich Auslagen für Gutachter (§ 10 GebGBbg)

8.  Begehung und Revision einer genehmigungsbedürftigen Anlage in anderen Fällen als denen nach Buchstabe a

70 bis 5 000

9.  Begehung und Revision einer nicht genehmigungsbedürftigen Anlage, soweit nicht nach § 52 Absatz 4 Satz 3 BImSchG kostenfrei

35 bis 350

10.  Prüfung von Kalibrierungsberichten und von Funktionsprüfberichten zur erstmaligen, wiederkehrenden oder kontinuierlichen Emissionsermittlung

70 bis 350

11.  sonstige Maßnahme

30 bis 1 000

Ergänzend gilt:

Wird die Überwachungsmaßnahme aufgrund eines Verdachts oder einer Beschwerde vorgenommen, so sind Gebühren nicht zu erheben, wenn alle Auflagen und Anordnungen erfüllt und Auflagen und Anordnungen nicht geboten sind.“

**2.3**

**Amtshandlungen nach den Verordnungen zur Durchführung des Bundes-Immissionsschutzgesetzes**

 

2.3.1

Verordnung über kleine und mittlere Feuerungsanlagen (1.
BImSchV)

 

2.3.1.1

1.  Bekanntgabe einer Stelle nach § 17a Absatz 2 der 1.
    BImSchV

  
nach Tarifstelle 2.2.3

 

2.  Entgegennahme und Prüfung von Bescheinigungen über den ordnungsgemäßen Einbau, die Berichte über das Ergebnis der Kalibrierung und der Prüfung der Funktionstauglichkeit nach § 17a Absatz 2 Satz 3 der 1.
    BImSchV

  
50 bis 200

 

3.  Entgegennahme und Prüfung der Auswertung kontinuierlicher Messungen nach § 17a Absatz 3 Satz 1 der 1.
    BImSchV

  
50 bis 200

 

4.  Entgegennahme und Prüfung von Messberichten nach § 17a Absatz 5 Satz 1 der 1.
    BImSchV

  
50 bis 200

2.3.1.2

Entgegennahme und Prüfung einer Anzeige vor Inbetriebnahme der Anlage nach § 18a der 1.
BImSchV

30 Prozent der Gebühr nach Tarifstelle 2.1.1

2.3.1.3

Anordnung anderer oder weitergehender Anforderungen nach § 19 der 1.
BImSchV

30 Prozent der Gebühr nach Tarifstelle 2.1.6, mindestens 50

2.3.1.4

Entscheidung über die Zulassung von Ausnahmen nach § 20 der 1.
BImSchV

51 bis 511

2.3.2

Verordnung zur Emissionsbegrenzung von leichtflüchtigen halogenierten organischen Verbindungen (2.
BImSchV)

 

2.3.2.1

Entscheidung über die Bekanntgabe einer Stelle (§ 12 Absatz 7 der 2.
BImSchV)

nach Tarifstelle 2.2.3

2.3.2.2

Entscheidung über die Zulassung einer Ausnahme nach § 17 von

 

 

1.  § 2 Absatz 2 Satz 1 der 2.
    BImSchV

  
51 bis 256

 

2.  § 2 Absatz 2 Satz 4 der 2.
    BImSchV

  
51 bis 256

 

3.  §§ 3, 4 oder 5 der 2.
    BImSchV

  
26 bis 256

 

4.  §§ 10, 11, 12, 13, 14 oder 15 der 2.
    BImSchV

  
15 bis 153

 

Werden mehrere Ausnahmen für dieselbe Anlage gleichzeitig erteilt, ist lediglich eine Gebühr nach dem höchsten anzuwendenden Gebührenrahmen festzusetzen.

 

2.3.3

_aufgehoben_

 

2.3.4

Verordnung über genehmigungsbedürftige Anlagen (4.
BImSchV)

 

2.3.4.1

Entscheidung über eine Verlängerung der Befristung der Genehmigung einer Versuchsanlage gemäß § 2 Absatz 3 Satz 1, 2.
Halbsatz der 4.
BImSchV

10 Prozent der Gebühr nach Tarifstelle 2.1.1, mindestens 51

2.3.5

Verordnung über Immissionsschutz- und Störfallbeauftragte (5.
BImSchV)

 

2.3.5.1

Gestattung der Bestellung eines für den Konzernbereich zuständigen Immissionsschutz- oder Störfallbeauftragten gemäß § 4 der 5.
BImSchV, je Person

51 bis 511

2.3.5.2

Gestattung der Bestellung eines oder mehrerer nicht betriebsangehöriger Immissionsschutz- oder Störfallbeauftragter gemäß § 5 der 5.
BImSchV, je Person

51 bis 511

2.3.5.3

Befreiung von der Verpflichtung zur Bestellung eines Immissionsschutz- oder Störfallbeauftragten gemäß § 6 der 5.
BImSchV

102

2.3.5.4

Anerkennung von Lehrgängen zur Vermittlung der Fachkunde für Immissionsschutzbeauftragte und Störfallbeauftragte nach § 7 Nummer 2 der 5.
BImSchV, je Lehrgang

102

2.3.5.5

Anerkennung einer Ausbildung als den Anforderungen an die Fachkunde gleichwertig gemäß § 8 der 5.
BImSchV

51

2.3.6

Verordnung zur Auswurfbegrenzung von Holzstaub (7.
BImSchV)

 

2.3.6.1

Zulassung von Ausnahmen nach § 6 der 7.
BImSchV

15 bis 153

2.3.7

Verordnung über die Beschaffenheit und die Auszeichnung der Qualitäten von Kraftstoffen (10.
BImSchV)

 

2.3.7.1

Entscheidung über die Bewilligung einer Ausnahme nach § 16 Absatz 1 der 10.
BImSchV

600 bis 6 000

2.3.7.2

Entscheidung über die Bewilligung einer Ausnahme nach § 16 Absatz 3 der 10.
BImSchV

600 bis 12 000

2.3.8

Verordnung über Emissionserklärungen (11.
BlmSchV)

 

2.3.8.1

Festlegung von Vereinfachungen der Emissionserklärung nach § 3 Absatz 2 Satz 1 und 2 der 11.
BImSchV

55 bis 165

2.3.8.2

Festlegungen von abweichenden Regelungen auf Antrag des Betreibers nach § 3 Absatz 3 Satz 2 der 11.
BImSchV

55 bis 165

2.3.8.3

Fristverlängerung nach § 4 Absatz 2 Satz 2 der 11.
BImSchV

30 bis 90

2.3.8.4

Befreiung nach § 6 der 11.
BImSchV

55 bis 165

2.3.9

Störfall-Verordnung (12.
BImSchV), auch in Verbindung mit landesrechtlicher Verweisung

 

2.3.9.1

Entgegennahme und Prüfung einer Anzeige (§ 7 der 12.
BImSchV)

51 bis 5 113

2.3.9.2

Entscheidung über Zulassung von Beschränkungen beim Sicherheitsbericht (§ 9 Absatz 6 der 12.
BImSchV)

256 bis 2 556

2.3.9.3

Entscheidung über die Zustimmung zur Auslegung eines geänderten Sicherheitsberichtes (§ 11 Absatz 3 Satz 2 und 3 der 12.
BImSchV)

102 bis 5 113

2.3.9.4

Befreiung vom anlagenbezogenen Sicherheitsbericht (§ 18 Absatz 2 der 12.
BImSchV)

102 bis 5 113

2.3.9.5

Inspektion, Untersuchung und Einholung erforderlicher Informationen, Maßnahmen sowie Empfehlungen (§ 19 Absatz 3 der 12.
BImSchV)

Zeitgebühr

2.3.10

Verordnung über Großfeuerungs- und Gasturbinenanlagen (13.
BImSchV)

 

2.3.10.1

Prüfung des Nachweises der Einhaltung der Betriebszeiten (§ 4 Absatz 7 Satz 2 sowie § 6 Absatz 7 Satz 2, Absatz 10 Satz 2 und Absatz 11 Satz 2 der 13.
BImSchV)

50 bis 500

2.3.10.2

Prüfung des Nachweises über die Einhaltung des Massenstromes (§ 6 Absatz 9 Satz 3 der 13.
BImSchV)

50 bis 500

2.3.10.3

Zulassung eines Emissionsgrenzwertes für SO² als über die Abgasvolumenströme gewichteten Durchschnittswert (§ 8 Absatz 3 Satz 2 der 13.
BImSchV)

50 bis 250

2.3.10.4

Entscheidung über die Bekanntgabe einer Stelle (§ 14 Absatz 2 und Absatz 3 Satz 1 der 13.
BImSchV)

nach Tarifstelle 2.2.3

2.3.10.5

Verzicht auf kontinuierliche Messung (§ 15 Absatz 2 Satz 2, Absatz 3 Satz 1, Absatz 9 der 13.
BImSchV)

100 bis 1 000

2.3.10.6

Prüfung eines Messberichts (§ 16 Absatz 2, § 18 Absatz 1 der 13.
BImSchV)

50 bis 500

2.3.10.7

Zulassung von Ausnahmen von den einzelnen Anforderungen der Verordnung (§ 21 der 13.
BImSchV), soweit es sich

 

 

1.  um unbefristete Ausnahmen von der Einhaltung einzelner Emissionsgrenzwerte

  
1 100 bis 10 300

 

2.  um befristete Ausnahmen von der Einhaltung einzelner Emissionsgrenzwerte

  
520 bis 5 200

 

3.  um Ausnahmen von sonstigen Anforderungen

  
110 bis 2 600

 

handelt

 

2.3.11

Verordnung über die Verbrennung und Mitverbrennung von Abfällen (17.
BImSchV)

 

2.3.11.1

Entscheidung über die Bekanntgabe einer Stelle nach § 10 Absatz 2 oder Absatz 3 oder § 13 Absatz 2 der 17.
BImSchV

nach Tarifstelle 2.2.3

2.3.11.2

Entgegennahme und Prüfung eines Messberichts (§ 14 Absatz 1 der 17.
BImSchV)

51 bis 511

2.3.11.3

Zulassung von Ausnahmen von einzelnen Anforderungen der Verordnung (§ 4 Absatz 3 und 7, § 5a Absatz 4 Satz 1, § 11 Absatz 1 Satz 3, Absatz 2 und 6, § 13 Absatz 2a und § 19 der 17.
BImSchV), soweit es sich

 

 

1.  um unbefristete Ausnahmen von der Einhaltung einzelner Emissionsgrenzwerte

  
1 534 bis 15 339

 

2.  um befristete Ausnahmen von der Einhaltung einzelner Emissionsgrenzwerte

  
511 bis 10 226

 

3.  um Ausnahmen von sonstigen Anforderungen

  
256 bis 5 113

 

handelt

 

2.3.11.4

Untersagung des Betriebs wegen Nichteignung (§ 20a der 17.
BImSchV)

250 bis 2 500

2.3.12

Sportanlagenlärmschutzverordnung (18.
BImSchV)

 

2.3.12.1

Zulassung von Ausnahmen (§ 6 der 18.
BImSchV)

100 bis 1 200

2.3.13

_aufgehoben_

 

2.3.14

Verordnung zur Begrenzung der Emissionen flüchtiger organischer Verbindungen beim Umfüllen und Lagern von Ottokraftstoffen (20.
BImSchV)

 

2.3.14.1

Zulassung von Ausnahmen (§ 11 Absatz 1 Satz 1 und Absatz 2)

100 bis 1 200

2.3.15

Verordnung zur Begrenzung der Kohlenwasserstoffemissionen bei der Betankung von Kraftfahrzeugen (21.
BImSchV)

 

2.3.15.1

Zulassung von Ausnahmen von einzelnen Anforderungen der Verordnung (§ 7 der 21.
BImSchV), soweit es sich

 

 

1.  um unbefristete Ausnahmen von der Einhaltung einzelner Anforderungen

  
256 bis 2 556

 

2.  um befristete Ausnahmen von der Einhaltung einzelner Anforderungen

  
128 bis 2 556

 

handelt

 

2.3.16

Verordnung über elektromagnetischer Felder (26.
BImSchV)

 

2.3.16.1

Prüfung einer Anzeige über die Inbetriebnahme oder wesentliche Änderung

 

 

1.  einer Hochfrequenz-Anlage (§ 7 Absatz 1 der 26.
    BImSchV) oder

  
25 bis 250

 

2.  einer Niederfrequenz-Anlage (§ 7 Absatz 2 der 26.
    BImSchV)

  
25 bis 250

2.3.16.2

Zulassung von Ausnahmen (§ 8 der 26.
BImSchV)

25 bis 1 000

2.3.17

Verordnung über Anlagen zur Feuerbestattung (27.
BImSchV)

 

2.3.17.1

Entgegennahme einer Anzeige zur Inbetriebnahme einer Anlage (§ 6 der 27.
BImSchV)

20 Prozent der Gebühren nach Tarifstelle 2.1.1

2.3.17.2

Bekanntgabe einer Stelle nach § 7 Absatz 3 Satz 1 der 27.
BImSchV

nach Tarifstelle 2.2.3

2.3.17.3

Entgegennahme und Prüfung einer Bescheinigung und von Berichten (§ 7 Absatz 3 Satz 3 der 27.
BImSchV)

26 bis 256

2.3.17.4

Entgegennahme und Prüfung eines Messberichts (§ 8 Absatz 2 der 27.
BImSchV)

26 bis 256

2.3.17.5

Entgegennahme und Prüfung eines Messberichts (§ 10 Absatz 1 der 27.
BImSchV)

51 bis 511

2.3.17.6

Entscheidung über die Zulassung einer Ausnahme (§ 12 der 27.
BImSchV)

51 bis 511

2.3.18

Verordnung über Anlagen zur biologischen Behandlung von Abfällen (30.
BImSchV)

 

2.3.18.1

Entscheidung über die Bekanntgabe einer Stelle nach § 8 Absatz 3 und 4 der 30.
BImSchV

nach Tarifstelle 2.2.3

2.3.18.2

Entgegennahme und Prüfung eines Messberichts nach § 12 der 30.
BImSchV

51 bis 1 790

2.3.18.3

Entscheidung über die Zulassung einer Ausnahme nach § 16 der 30.
BImSchV

256 bis 1 790

2.3.19

Verordnung zur Begrenzung der Emissionen flüchtiger organischer Verbindungen bei der Verwendung organischer Lösemittel in bestimmten Anlagen (31.
BImSchV)

 

2.3.19.1

Entgegennahme und Prüfung einer Anzeige vor Inbetriebnahme der Anlage nach § 5 Absatz 2 Satz 1 der 31.
BImSchV

20 Prozent der Gebühr nach Tarifstelle 2.1.1

2.3.19.2

Entscheidung über die Bekanntgabe einer Stelle nach § 5 Absatz 4 und/oder Anhang VI Nummer 2.1 (zu den §§ 5 und 6) der 31.
BImSchV

nach Tarifstelle 2.2.3

2.3.19.3

Entscheidung über die Zulassung von Ausnahmen nach § 11 der 31.
BImSchV

256 bis 5 113

2.3.20

Geräte- und Maschinenlärmschutzverordnung (32.
BImSchV)

 

2.3.20.1

Entgegennahme und Prüfung der Konformitätserklärung gemäß § 4 der 32.
BImSchV

15 bis 500

2.3.20.2

Zulassung von Ausnahmen von den Einschränkungen des § 7 Absatz 1 gemäß § 7 Absatz 2 Satz 1 der 32.
BImSchV

20 bis 1 000

**2.4**

**Landesimmissionsschutzgesetz (LImSchG)**

 

2.4.1

Entscheidung über die Erteilung einer Ausnahmegenehmigung zur Durchführung von Motorsportveranstaltungen oder anderen öffentlichen Veranstaltungen gemäß § 3 Absatz 6 Satz 1 und 2 LImSchG

51 bis 511

2.4.2

Entscheidung über die Zulassung von Ausnahmen vom Verbot des Verbrennens im Freien gemäß § 7 Absatz 2 LImSchG

10 bis 77

2.4.3

Entscheidung über Ausnahmen vom Verbot von Betätigungen, welche die Nachtruhe zu stören geeignet sind gemäß § 10 Absatz 2 (Einzelverfügung) und Absatz 3 LImSchG

10 bis 767

2.4.4

Entscheidung über Ausnahmen vom Verbot der Benutzung von Tongeräten gemäß § 11 Absatz 4 LImSchG

10 bis 102

2.4.5

Entscheidung über Erlaubnisse im Zusammenhang mit dem Abbrennen von Feuerwerken oder Feuerwerkskörpern, sowie Ausnahmen bezüglich der Dauer eines Feuerwerks nach § 12 LImSchG

10 bis 102

2.4.6

Anordnung im Einzelfall gemäß § 15 LImSchG

51 bis 1 023

**2.5**

**Technische Anleitung zur Reinhaltung der Luft (TA Luft)**

 

2.5.1

Entscheidung über die Bekanntgabe einer Stelle nach Nummer 5.3.3.4 oder 5.3.3.6 der TA Luft

nach Tarifstelle 2.2.3

**2.6**

**Chemikalienrechtliche Angelegenheiten**

 

2.6.1

Amtshandlungen nach dem Chemikaliengesetz (ChemG)

 

 

Durchführung einer Überwachung über die Einhaltung der Grundsätze der Guten Laborpraxis (GLP) gemäß § 19 Absatz 3 ChemG in Verbindung mit der Verwaltungsvorschrift zum Verfahren der behördlichen Überwachung der Einhaltung der Grundsätze der Guten Laborpraxis (ChemVwVGLP)  
  
Die Erteilung einer GLP-Bescheinigung erfolgt gebührenfrei.

nach Zeitaufwand, jedoch nicht mehr als 12 000

2.6.2

Anordnungen im Einzelfall gemäß § 23 Absatz 1 ChemG

30 bis 5 200

2.6.3

Amtshandlungen nach der Chemikalienverbotsverordnung (ChemVerbotsV)

 

2.6.3.1

Entscheidung über die Zulassung einer Ausnahme von dem Verbot nach § 1 Absatz 1 und 3 ChemVerbotsV

300 bis 2 600

2.6.3.2

Entscheidung über die Erteilung einer Erlaubnis zum Inverkehrbringen gemäß § 2 Absatz 1 und 4 ChemVerbotsV

30 bis 2 600

2.6.3.3

Durchführung der Sachkundeprüfung und Entscheidung über die Ausstellung eines Prüfungszeugnisses, Entgegennahme und Prüfung des Nachweises über die Gleichwertigkeit gemäß § 5 Absatz 2 und 3 ChemVerbotsV

20 bis 120

2.6.4

Amtshandlungen gemäß Chemikalien-Ozonschichtverordnung (ChemOzonSchichtV)

 

2.6.4.1

Anerkennung von Fortbildungsveranstaltungen nach § 5 Absatz 2 Nummer 1 ChemOzonSchichtV

350

2.6.4.2

Anerkennung anderer Befähigungsnachweise nach § 5 Absatz 2 Nummer 4 ChemOzonSchichtV

20

2.6.5

Amtshandlungen nach der Gefahrstoffverordnung (GefStoffV)

 

2.6.5.1

Entscheidung über Zulassung einer Ausnahme von den Vorschriften über das Inverkehrbringen von gefährlichen Stoffen und Zubereitungen nach § 20 GefStoffV

30 bis 2 600

2.6.6

Amtshandlungen nach der Chemikalien-Klimaschutzverordnung (ChemKlimaschutzV)

 

2.6.6.1

Fristverlängerung nach § 3 Absatz 1 Satz 5 ChemKlimaschutzV

50

2.6.6.2

Anerkennungen nach § 5 Absatz 3 ChemKlimaschutzV

300 bis 1 000

2.6.6.3

Zertifizierung von Betrieben nach § 6 ChemKlimaschutzV

50 bis 150

**2.7**

**Gentechnikrechtliche Angelegenheiten**

 

2.7.1

Amtshandlungen nach dem Gentechnikgesetz (GenTG)

 

2.7.1.1

Anzeigen und Anmeldungen

 

2.7.1.1.1

Prüfung und Bescheidung einer Anzeige oder Anmeldung

 

 

1.  zur Errichtung und zum Betrieb von gentechnischen Anlagen (§ 8 Absatz 2 GenTG)

  
50 Prozent des sich aus Tarifstelle 2.7.1.2.1 ergebenden Betrages

 

2.  nur zum Betrieb einer gentechnischen Anlage (§ 8 Absatz 2 GenTG)

  
160 bis 800

2.7.1.1.2

Prüfung und Bescheidung einer Anzeige oder Anmeldung zu wesentlichen Änderungen (§ 8 Absatz 4 Satz 2 in Verbindung mit § 8 Absatz 2 GenTG)

50 Prozent des sich aus Tarifstelle 2.7.1.1.1 ergebenden Betrages

2.7.1.1.3

Prüfung und Bescheidung einer Anzeige zu weiteren gentechnischen Arbeiten (§ 9 Absatz 2 GenTG) 

50 bis 600

2.7.1.1.4

Entscheidung über die Zustimmung zum vorzeitigen Beginn (§ 12 Absatz 5 Satz 1 GenTG)

100 zusätzlich zu den Gebühren nach Tarifstelle 2.7.1.1.1 oder 2.7.1.1.2

2.7.1.1.5

Entscheidung über die Untersagung angezeigter oder angemeldeter gentechnischer Arbeiten (§ 12 Absatz 5a Satz 2, Absatz 7 GenTG)

100 bis 1 600

2.7.1.2

Genehmigungen

 

2.7.1.2.1

Entscheidung über die

*   Genehmigung (§ 11 Absatz 1 GenTG)
*   Teilgenehmigung (§ 11 Absatz 2 in Verbindung mit § 8 Absatz 3 GenTG)

oder

*   Genehmigung einer wesentlichen Änderung (§ 11 Absatz 1 in Verbindung mit § 8 Absatz 4 GenTG)

 

 

einer gentechnischen Anlage mit Errichtungskosten (E)

 

 

1.  bis zu 52 000 EUR

  
180 + 0,0095 × E

 

2.  bis zu 512 000 EUR

  
700 + 0,0095 × (E – 52 000)

 

3.  bis zu 51 130 000 EUR

  
3 850 + 0,0035 × (E – 512 000)

 

4.  über 51 130 000 EUR

  
184 600 + 0,003 × (E – 51 130 000) mindestens die höchste Gebühr, die für eine nach § 22 GenTG eingeschlossene behördliche Entscheidung zu entrichten gewesen wäre, wenn diese selbstständig erteilt worden wäre

 

5.  Prüfung und Bescheidung einer Genehmigung zu weiteren gentechnischen Arbeiten der Sicherheitsstufen 2, 3 oder 4 (§ 11 Absatz 1 in Verbindung mit § 9 Absatz 2 Satz 2 und Absatz 3 GenTG)

  
50 bis 1 020

 

6.  ist ausschließlich die Regelung des Betriebes Gegenstand eines Teil- oder Änderungsgenehmigungsverfahrens

160 bis 800

 

7.  wird im Genehmigungsverfahren ein Anhörungsverfahren (§ 18 Absatz 1 GenTG) durchgeführt, erhöht sich die Gebühr nach den Buchstaben a bis e um

  
153 je Stunde, höchstens jedoch 767 für jeden Tag, an dem Erörterungen stattgefunden haben

 

ergänzend gilt:

 

 

1.  Errichtungskosten (E) sind die voraussichtlichen Gesamtkosten der Anlage oder derjenigen Anlagenteile, die nach der (Teil-, Änderungs-)Genehmigung errichtet werden dürfen, einschließlich Mehrwertsteuer.
    Maßgeblich sind die voraussichtlichen Gesamtkosten im Zeitpunkt der Erteilung der (Teil-, Änderungs-)Genehmigung, es sei denn, diese sind niedriger als zum Zeitpunkt der Antragstellung.
    Als Errichtungskosten gelten auch Kosten, die durch den Austausch von Anlagenteilen entstehen.
    Gründungskosten und Kosten für Erdaushubarbeiten sind insoweit einzubeziehen, als diese Maßnahmen aus Anlass der Errichtung oder Änderung der Anlage durchgeführt werden.
    Aufwendungen für die Entwicklung und Vorplanung, für den Erwerb des unbebauten Grundstücks sowie für Zubehör, auf das sich die Genehmigung nicht erstreckt, sind nicht einzubeziehen.

 

 

2.  Ergehen mehrere Teilgenehmigungen, ist jede gesondert abzurechnen.

 

 

3.  Gebühren oder Auslagen für die Prüfung bautechnischer Nachweise und für besondere bauaufsichtliche Maßnahmen (§§ 83, 84 BbgBO) werden von den Bauaufsichtsbehörden gesondert erhoben.

 

 

4.  Reisekosten von Angehörigen der Genehmigungsbehörde oder der Behörden, die durch die Genehmigungsbehörde beteiligt werden, gelten als in die Gebühr mit einbezogen.
    Satz 1 gilt nicht für Auslandsdienstreisen.

 

2.7.1.3

Sonstige Amtshandlungen nach dem GenTG

 

2.7.1.3.1

Maßnahmen zur Überwachung auf Grund von

 

 

1.  § 25 in Verbindung mit § 16b, § 16c, § 17b und

  
Zeitgebühr nach Aufwand: 153 je Stunde, höchstens jedoch 1 224 für den Tag

 

2.  § 25 GenTG – Begehung einer Freisetzung

  
400

2.7.1.3.2

Anordnung im Einzelfall gemäß § 26 Absatz 1 GenTG

120 bis 6 000

2.7.1.3.3

Untersagung des Anlagenbetriebes gemäß § 26 Absatz 2 GenTG

260 bis 6 000

2.7.1.3.4

Anordnung der Stilllegung oder Beseitigung einer Anlage gemäß § 26 Absatz 3 GenTG

120 bis 6 000

2.7.1.3.5

Entscheidung über eine Verlängerung der Frist zur Errichtung oder zum Betrieb der gentechnischen Anlage (§ 27 Absatz 3 GenTG)

20 Prozent der Gebühr nach Tarifstellen 2.7.1.1 und 2.7.1.2

2.7.1.3.6

Anordnung nachträglicher Auflagen (§ 19 Satz 3 GenTG)

100 bis 1 500

2.7.1.3.7

Anordnung der einstweiligen Einstellung der Tätigkeit gemäß § 20 Absatz 1 GenTG

100 bis 1 500

2.7.1.3.8

Erteilung von Auskünften auf Verlangen des Geschädigten (§ 35 Absatz 2 GenTG)

25 bis 500

2.7.2

Amtshandlungen nach den Verordnungen zur Durchführung des Gentechnikgesetzes

 

2.7.2.1

Verordnung über die Sicherheitsstufen und Sicherheitsmaßnahmen bei gentechnischen Arbeiten in gentechnischen Anlagen (Gentechniksicherheitsverordnung – GenTSV)

 

2.7.2.1.1

Entscheidung über den Verzicht auf Vorlage der Bescheinigung nach § 15 Absatz 2 Satz 1 Nummer 3 GenTSV gemäß § 15 Absatz 2 Satz 4 GenTSV

50

2.7.2.1.2

Entscheidung über die Anerkennung einer anderen Aus-, Fort- oder Weiterbildung (§ 15 Absatz 3 GenTSV)

50 bis 120

2.7.2.1.3

Entscheidung über die Anerkennung anderer Veranstaltungen (§ 15 Absatz 4 Satz 2 GenTSV)

250

2.7.2.1.4

Entscheidung über die Gestattung der Bestellung eines oder mehrerer nicht betriebsangehöriger Beauftragter für die biologische Sicherheit (§ 16 Absatz 2 GenTSV)

50

2.7.2.2

Verordnung über die Erstellung von außerbetrieblichen Notfallplänen und über Informations-, Melde- und Unterrichtungspflichten (GenTNotfV)

 

2.7.2.2.1

Erstellung eines außerbetrieblichen Notfallplanes (§ 3 Absatz 1 Satz 1 GenTNotfV)

Zeitgebühr

**2.8**

**Atomrechtliche Angelegenheiten**

Die Gebührenordnung des Ministeriums für Arbeit, Soziales, Frauen und Familie findet entsprechende Anwendung.

**2.9**

**Messung von Radioaktivität und elektromagnetischen Feldern**

 

2.9.1

Radioaktivitätsbestimmungen durch die Landesmessstelle

 

2.9.1.1

Vorbereitung der Probenahme, Probenahmebegleitung (Vor-Ort-Einsatz, Ortsbesichtigung und dergleichen)

Zeitgebühr

2.9.1.2

Probenahme

 

2.9.1.2.1

Probenahme mit einfachen Hilfsmitteln

 

2.9.1.2.1.1

Einfachprobe

46

2.9.1.2.1.2

jede weitere Probe am gleichen Ort

23

2.9.1.2.1.3

Mehrfachprobe, je angefangene 30 Minuten

23

2.9.1.2.2

Probenahme mit besonderem Aufwand (Schutzmaßnahmen, aufwändige technische Ausstattung), je angefangene 30 Minuten

46

2.9.1.3

_Nicht besetzt_

 

2.9.2

Messung elektromagnetischer Felder gemäß Durchführungshinweisen zur 26.
BImSchV (Grundgebühren ohne Fahrtkosten)

 

2.9.2.1

Niederfrequente Felder

 

2.9.2.1.1

Ermittlung der elektrischen Feldstärke und magnetischen Flussdichte  
  
erster Messpunkt  
jeder weitere Messpunkt

  
  
332  
71

2.9.2.2

Hochfrequente Felder

 

2.9.2.2.1

Breitbandige Messung

 

2.9.2.2.1.1

Ermittlung der elektrischen Feldstärke im Fernfeld  
  
erster Messpunkt  
jeder weitere Messpunkt

  
  
299  
47

2.9.2.2.1.2

Ermittlung der elektrischen und magnetischen Feldstärke im Nahfeld  
  
erster Messpunkt  
jeder weitere Messpunkt

  
  
345  
47

2.9.2.2.2

Frequenzselektive Messung der elektrischen Feldstärke  
  
erster Messpunkt  
jeder weitere Messpunkt

  
  
505  
118

2.9.3

Einsatz von Kraftfahrzeugen

 

2.9.3.1

Einsatz des Landesmesswagens, Fahrten, je angefangener Kilometer

1,28

2.9.3.2

Einsatz sonstiger Kraftfahrzeuge, Fahrten, je angefangener Kilometer

0,77

2.9.4

Personalkosten, soweit nichts Anderes bestimmt (Fahrtzeiten, Begutachtungen, schriftliche Beratungen, Stellungnahmen außerhalb von Verwaltungsverfahren)

Zeitgebühr

**2.10**

**Verordnung über immissionsschutz- und abfallrechtliche Überwachungserleichterungen für nach der Verordnung (EG) Nr. 761/2001 registrierte Standorte und Organisationen (EMAS-Privilegierungs-Verordnung – EMASPrivilegV)**

 

2.10.1

Gestattung von Messungen gemäß § 4 Satz 2 und § 5 Absatz 1 EMASPrivilegV mit eigenem Personal

51 bis 511

2.10.2

Gestattung von Funktionsprüfungen nach § 5 Absatz 2 und sicherheitstechnischen Prüfungen nach § 6 EMASPrivilegV mit eigenem Personal

128 bis 3 068

**2.11**

**Gesetz über die Umweltverträglichkeitsprüfung (UVPG)**

 

  
 

Planfeststellung und Plangenehmigung der Errichtung, des Betriebes und der Änderung von Rohrleitungsanlagen zum Befördern von Stoffen gemäß § 3a des Chemikaliengesetzes (§§ 20, 21 UVPG in Verbindung mit Nummer 19.6 der Anlage 1 zum UVPG)

*   für die ersten 26 000 EUR Baukostenwert
*   für die weiteren 26 000 EUR Baukostenwert
*   für den 52 000 EUR übersteigenden Teil

  
  
  
1,5 Prozent  
0,5 Prozent  
0,2 Prozent, mindestens 153

 

Zusatz für Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung:

 

 

1.  wird im Zulassungsverfahren eine Prüfung der Umweltverträglichkeit vorgenommen

Erhöhung der Gebühr um 10 Prozent

 

2.  wird im Zulassungsverfahren eine Vorprüfung zur Feststellung der UVP-Pflicht im Einzelfall gemäß § 3c UVPG vorgenommen und führt die Vorprüfung zur Ablehnung der UVP-Pflicht

100 bis 1 000

 

3.  wird vor Beginn eines Zulassungsverfahrens auf Antrag des Vorhabensträgers oder anlässlich eines Ersuchens nach § 5 UVPG die UVP-Pflicht im Einzelfall gemäß § 3c in Verbindung mit § 3a UVPG festgestellt

100 bis 1 000  
Wird ein Zulassungsverfahren durchgeführt, so entfällt die Gebührenpflicht für die positive Feststellung der UVP-Pflicht vor Beginn des Zulassungsverfahrens.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Zulassungsverfahren anzurechnen.

 

4.  Unterrichtung über voraussichtlich beizubringende Unterlagen bei UVP-pflichtigen Vorhaben auf Ersuchen des Vorhabensträgers

100 bis 1 000  
Wird ein Zulassungsverfahren durchgeführt, so entfällt die Gebühr für die Unterrichtung über den Umfang beizubringender Unterlagen vor Beginn des Zulassungsverfahrens.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Zulassungsverfahren anzurechnen.

 

Im Falle der Durchführung einer Verträglichkeits- oder Ausnahmeprüfung nach § 34 BNatSchG bzw.
§ 26d BNatSchG zusätzlich

5 Prozent der Gebühr nach Tarifstelle 2.11, mindestens 51

**2.12**

**Treibhausgas-Emissionshandelsgesetz (TEHG)**

 

2.12.1

Entscheidung über die Bekanntgabe als sachverständige Stelle nach § 5 Absatz 3 Satz 2 TEHG

 

 

1.  soweit bereits eine Bekanntgabe durch ein anderes Bundesland vorliegt

  
102 bis 256

 

2.  Wiederholungsbekanntgabe nach Ablauf der Befristung

  
102 bis 256

 

3.  in allen übrigen Fällen

  
256 bis 3 068

2.12.2

Genehmigung von Monitoringkonzepten

350 bis 3 500

**2.13**

**Erneuerbare-Energien-Gesetz (EEG)**

 

2.13.1

Bescheinigung über die Einhaltung von Formaldehydgrenzwerten nach § 27 Absatz 5 und § 66 Absatz 1 Nummer 4 a EEG

50 bis 300

**2.14**

**Gesetz zum Schutz gegen Fluglärm**

 

2.14.1

Entscheidung über Ausnahmen vom Bauverbot nach § 5 Absatz 1 des Gesetzes zum Schutz gegen Fluglärm

50 bis 5 000

2.14.2

Prüfung der Schallschutzanforderungen bei zulässigen baulichen Anforderungen nach § 6 des Gesetzes zum Schutz gegen Fluglärm

300 bis 5 000

 

 

 

**3**

**Abfall- und bodenschutzrechtliche Angelegenheiten**

 

**3.1**

**Amtshandlungen nach dem Kreislaufwirtschaftsgesetz (KrWG)**

 

3.1.1

Entscheidung über die Anerkennung als Träger der Qualitätssicherung nach § 12 Absatz 5 KrWG

120 bis 2 550

3.1.2

Bearbeiten von Anzeigen für gemeinnützige Sammlungen nach § 18 Absatz 1 bis 5 KrWG

50 bis 4 000

3.1.3

Bearbeitung von Anzeigen für gewerbliche Sammlungen nach § 18 Absatz 1 bis 6 KrWG

50 bis 7 000

3.1.4

Entscheidung über die Zustimmung zum Ausschluss von Abfällen – und ihrem Widerruf – durch öffentlich-rechtliche Entsorgungsträger (§ 20 Absatz 2 KrWG)

60 bis 650

3.1.5

Prüfung von Anträgen zur Feststellung und Einrichtung von Rücknahmesystemen bei Rechtsverordnungen nach den §§ 24 und 25 KrWG

500 bis 25 000

3.1.6

Entscheidung über Ausnahmen von Nachweis- und Transportgenehmigungspflichten bei der freiwilligen Rücknahme von Abfällen (§ 26 Absatz 3 KrWG)

110 bis 1 000

3.1.7

Feststellung der Abfallrücknahme im Rahmen der Produktverantwortung auf Antrag des Herstellers oder Vertreibers (§ 26 Absatz 6 KrWG)

100 bis 5 000

3.1.8

Entscheidung über die Zulassung der Abfallentsorgung außerhalb zugelassener Anlagen (§ 28 Absatz 2 KrWG)

60 bis 2 100

3.1.9

Aufgaben im Zusammenhang mit der Durchführung der Abfallbeseitigung (§ 29 KrWG)

 

3.1.9.1

Anordnung auf Antrag eines zur Abfallentsorgung Verpflichteten, diesem die Mitbenutzung einer Abfallbeseitigungsanlage zu gestatten, Festsetzung des Entgeltes soweit erforderlich (§ 29 Absatz 1 KrWG)

110 bis 5 200

3.1.9.2

Entscheidung über die Übertragung von Abfallbeseitigungspflichten auf den Betreiber einer Abfallbeseitigungsanlage (§ 29 Absatz 2 KrWG)

550 bis 5 200

3.1.9.3

Entscheidung über die Anordnung zur Duldung von Abfallbeseitigungsmaßnahmen auf Grundstücken, die zur Mineralölgewinnung genutzt werden, soweit die Anordnung auf Antrag erfolgt sowie Festsetzung der Kosten für die Beseitigungspflichten, soweit erforderlich (§ 29 Absatz 3 KrWG)

260 bis 5 200

3.1.10

Entscheidung über die Planfeststellung oder Plangenehmigung einer Abfalldeponie oder einer wesentlichen Änderung (§ 35 Absatz 2 und 3 KrWG) mit Errichtungskosten (E)

 

 

Bei Errichtungskosten (E):

 

 

1.  bis zu 52 000 EUR

  
112 + 0,009 × E

 

2.  bis zu 512 000 EUR

  
581 + 0,006 × (E – 52 000)

 

3.  bis zu 51 130 000 EUR

  
3 350 + 0,0035 × (E – 512 000)

 

4.  über 51 130 000 EUR

  
184 065 + 0,003 × (E – 51 130 000), mindestens die höchste Gebühr, die für eine nach § 75 Absatz 1 VwVfG konzentrierte behördliche Entscheidung zu entrichten gewesen wäre, wenn diese selbstständig erteilt worden wäre

 

5.  ist Gegenstand des Planfeststellungs- oder Plangenehmigungsverfahrens eine Maßnahme, die keine Errichtungsmaßnahmen oder Errichtungsmaßnahmen nur zu einem unwesentlichen Teil umfasst

  
256 bis 25 565

 

6.  wird im Planfeststellungsverfahren ein Erörterungstermin (§ 73 Absatz 6 VwVfG) durchgeführt, erhöht sich die Gebühr nach Buchstaben a bis e um

  
153 je Stunde, höchstens jedoch 767 für jeden Tag, an dem Erörterungen stattgefunden haben

 

7.  wird in dem Zulassungsverfahren eine Prüfung der Umweltverträglichkeit gemäß dem Gesetz über die Umweltverträglichkeitsprüfung vorgenommen

  
Erhöhung des sich aus den Buchstaben a bis e ergebenden Betrages um 10 Prozent, mindestens jedoch um 511, höchstens um 25 565

 

8.  wird im Zulassungsverfahren eine Vorprüfung über die Feststellung der UVP-Pflicht im Einzelfall gemäß § 3c UVPG mit negativem Ergebnis vorgenommen

  
3 Prozent des sich aus den Buchstaben a bis d ergebenden Betrages, mindestens jedoch 153, höchstens 7 670

 

9.  wird vor Beginn eines Zulassungsverfahrens auf Ersuchen des Vorhabensträgers eine Unterrichtung über den Umfang beizubringender Unterlagen nach § 5 UVPG durchgeführt

  
3 Prozent des sich aus den Buchstaben a bis d ergebenden Betrages, mindestens jedoch 153, höchstens 7 670

 

 

Wird ein Zulassungsverfahren durchgeführt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht vor Beginn des Zulassungsverfahrens.

 

 

Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Zulassungsverfahren anzurechnen.

 

10.  wird vor Beginn eines Zulassungsverfahrens auf Antrag des Vorhabensträgers oder anlässlich eines Ersuchens nach § 5 UVPG eine Vorprüfung für die UVP-Pflicht im Einzelfall gemäß § 3c in Verbindung mit § 3a UVPG durchgeführt

  
3 Prozent des sich aus den Buchstaben a bis d ergebenden Betrages, mindestens jedoch 153, höchstens 7 670

 

 

Wird ein Zulassungsverfahren durchgeführt, so entfällt die Gebührenpflicht für die positive Feststellung der UVP-Pflicht vor Beginn des Zulassungsverfahrens.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Zulassungsverfahren anzurechnen.

 

11.  wird im Zulassungsverfahren eine Verträglichkeits- oder Ausnahmeprüfung nach § 34 BNatSchG bzw.
    § 16 BbgNatSchAG vorgenommen

  
5 Prozent, bei Anwendung von Buchstabe g 2 Prozent des sich aus den Buchstaben a bis e ergebenden Betrages, mindestens jedoch 256, höchstens 12 782

 

Ergänzend gilt:

 

 

1.  Errichtungskosten (E) sind die voraussichtlichen Gesamtkosten der Anlage oder derjenigen Anlagenteile, die nach der Planfeststellung oder Plangenehmigung errichtet werden dürfen, einschließlich Mehrwertsteuer.
    Maßgeblich sind die voraussichtlichen Gesamtkosten im Zeitpunkt der Erteilung der Planfeststellung oder Plangenehmigung, es sei denn, diese sind niedriger als zum Zeitpunkt der Antragstellung.
    Nicht zu den Errichtungskosten zählen solche, die durch Stilllegungs- und Nachsorgemaßnahmen für Deponien verursacht werden, insbesondere zur Herstellung eines Oberflächenabdichtungssystems einschließlich Rekultivierungsschicht (unberührt davon bleiben Maßnahmen, die im Rahmen der Errichtung der Deponie gefordert sind, wie z.
    B.  die Herstellung einer geologischen Barriere, eines Basisabdichtungssystems oder von Grundwassermessstellen).

 

 

> Gründungskosten und Kosten für Erdaushubarbeiten sind insoweit einzubeziehen, als diese Maßnahmen aus Anlass der Errichtung oder Änderung der Deponie durchgeführt werden.
> Aufwendungen für die Entwicklung und Vorplanung, für den Erwerb des unbebauten Grundstücks sowie für Zubehör, auf das sich die Planfeststellung oder Plangenehmigung nicht erstreckt, sind nicht einzubeziehen.

 

 

2.  Reisekosten von Angehörigen der Genehmigungsbehörde oder der Behörden, die durch die Genehmigungsbehörde beteiligt werden, gelten als in die Gebühr einbezogen.
    Satz 1 gilt nicht für Auslandsdienstreisen.

 

 

3.  Etwaige Kosten der Prüfung der Standsicherheitsnachweise oder sonstiger bautechnischer Nachweise durch das Bautechnische Prüfamt oder einen Prüfingenieur für Baustatik sind als Auslagen zu erheben.

 

 

4.  Eine nach Tarifstelle 3.1.11 entrichtete Gebühr wird zu 90 Prozent angerechnet.

 

3.1.11

Prüfung und Bescheidung einer Anzeige zur Änderung der Anlage (§ 35 Absatz 4 KrWG) in Verbindung mit § 15 Absatz 1 und 2 BImSchG

20 Prozent der Gebühr  
nach Tarifstelle 3.1.10,  
mindestens 100

3.1.12

Entscheidung über nachträgliche Maßgaben bei zugelassenen Abfalldeponien (§ 36 Absatz 4 KrWG)

3 Prozent des sich aus  
Tarifstelle 3.1.10  
Buchstabe a bis e  
ergebenden Betrages,  
mindestens jedoch 300,  
höchstens 5 000

3.1.13

Zulassung des vorzeitigen Beginns für die Errichtung und den Betrieb von ortsfesten Abfallbeseitigungsanlagen (§ 37 KrWG) – die Gebühr für die Hauptentscheidung bleibt davon unberührt

50 Prozent der Gebühr für  
die Hauptentscheidung

3.1.14

Nachträgliche Anordnungen und die vollständige oder teilweise Untersagung des Betriebes von Deponien, die schon vor dem 1. Juli 1990 betrieben wurden oder mit deren Einrichtung begonnen worden war (§ 39 KrWG)

500 bis 5 200 

3.1.15

Amtshandlungen im Rahmen der Stilllegung (§ 40 KrWG)

 

3.1.15.1

Verpflichtung des Inhabers einer stillgelegten Abfalldeponie zur Rekultivierung, zu sonstigen Vorkehrungen und zur Meldung der Überwachungsergebnisse (§ 40 Absatz 2 KrWG)

500 bis 5 200 

3.1.15.2

Feststellung des Abschlusses der Stilllegung (§ 40 Absatz 3 KrWG)

1.  soweit Festlegungen zur Stilllegung durch Zulassung erfolgen

2.  soweit Festlegungen zur Stilllegung nicht durch die Zulassung getroffen wurden

  
a. 10 Prozent der Gebühr für die Entscheidung nach Tarifstelle 3.1.10 (§ 35 Absatz 2 und 3 KrWG)  
  
b. hilfsweise 20 Prozent der Gebühr(en) für die Entscheidung(en) nach Tarifstelle 3.1.15.1 (§ 40 Absatz 2 KrWG) mindestens jedoch 200, höchstens 5 000

3.1.15.3

Feststellung des Abschlusses der Nachsorgephase (§ 40 Absatz 5 KrWG)

1.  soweit Festlegungen zur Nachsorge durch Zulassung erfolgen

2.  soweit Festlegungen zur Nachsorge nicht durch die Zulassung getroffen wurden

  
a.  10 Prozent der Gebühr für die Entscheidung nach Tarifstelle 3.1.10 (§ 35 Absatz 2 und 3 KrWG)  
  
b.  hilfsweise 20 Prozent der Gebühr(en) für die Entscheidung(en) nach Tarifstelle 3.1.15.1 (§ 40 Absatz 2 KrWG) mindestens jedoch 150, höchstens 3 000

3.1.16

Amtshandlungen im Rahmen der Abfallberatung und allgemeinen Überwachung (§§ 46 und 47 KrWG)

 

3.1.16.1

Auskunft über vorhandene geeignete Abfallbeseitigungsanlagen (§ 46 Absatz 2 KrWG), soweit sie nicht an Körperschaften des öffentlichen Rechts ergeht

50 bis 300

3.1.17

Überwachungsmaßnahmen, soweit sie durch einen Verstoß des Kostenschuldners gegen bestehende Gesetze, Rechtsverordnungen oder Nebenbestimmungen zum Verwaltungsakt veranlasst waren (§ 47 Absatz 1 KrWG)

60 bis 3 000

3.1.18

Entscheidung über die Einstufung von Abfällen gemäß § 48 KrWG in Verbindung mit § 3 Absatz 3 der Abfall-Verzeichnis-Verordnung

100 bis 1 000

3.1.19

Verpflichtung zur Register- und Nachweisführung gemäß § 51 Absatz 1 KrWG

50 bis 500

3.1.20

Amtshandlungen im Zusammenhang mit Anzeigen von Sammlern, Beförderern, Händlern und Maklern von Abfällen (§ 53 KrWG)

 

3.1.20.1

Entgegennahme, Bearbeitung und Bestätigung von Anzeigen von Sammlern, Beförderern, Händlern und Maklern von Abfällen oder deren Änderung (§ 53 Absatz 1 KrWG)

50 bis 500

3.1.20.2

Anordnungen gemäß § 53 Absatz 3 Satz 1 und 3 KrWG

200 bis 1 500

3.1.21

Amtshandlungen im Zusammenhang mit der Erteilung von Erlaubnissen für das Sammeln, Befördern, Handeln und Makeln von gefährlichen Abfällen (§ 54 KrWG)

 

3.1.21.1

Entscheidung über die Erteilung oder die Änderung einer Erlaubnis für Sammler, Beförderer, Händler und Makler von gefährlichen Abfällen

100 bis 5 000

3.1.21.2

Widerruf der Erlaubnis für Sammler, Beförderer, Händler und Makler

200 bis 1 500

3.1.22

Entscheidung über die Zustimmung zu Überwachungsverträgen sowie deren Änderung bzw.
Erweiterung (§ 56 Absatz 5 KrWG)

50 bis 5 000

3.1.23

Entscheidung im Zusammenhang mit Entsorgergemeinschaften (§ 56 KrWG)

 

3.1.23.1

Entscheidung über die Zustimmung zu Überwachungsverträgen sowie deren Änderung bzw.
Erweiterung (§ 56 Absatz 5 Satz 3 KrWG)

50 bis 5 000

3.1.23.2

Entscheidung über die Anerkennung von Entsorgergemeinschaften (§ 56 Absatz 6 KrWG)

2 600 bis 41 000

3.1.23.3

Entziehung des Zertifikats und/oder der Berechtigung zum Führen des Überwachungszeichens (§ 56 Absatz 8 Satz 2 KrWG)

500 bis 2 000

3.1.24

Anordnung zur Bestellung von Betriebsbeauftragten für Abfall (§ 59 Absatz 2 KrWG)

50 bis 500

3.1.25

Anordnungen zur Durchführung des Kreislaufwirtschaftsgesetzes und der danach ergangenen Verordnungen (§ 62 KrWG)

50 bis 5 000

**3.2**

**Amtshandlungen nach der Klärschlammverordnung (AbfKlärV)**

 

3.2.1

Anordnung und Entscheidung in Bezug auf Boden-, Klärschlamm-, Klärschlammgemisch- und Klärschlammkompostuntersuchungen nach § 4 Absatz 3, 5 und 7, § 5 Absatz 5, § 6 Absatz 2, § 9 Absatz 1 und 3

70 – 530

3.2.2

Prüfung der vorgelegten Untersuchungsergebnisse nach § 5 Absatz 4 und § 6 Absatz 1 Satz 3

70 – 340

3.2.3

Anordnung und Entscheidung in Bezug auf bodenbezogene Grenzwerte nach § 7 Absatz 2 und 3

70 – 530

3.2.4

Entscheidung über die Zulassung des Auf- und Einbringens in Naturschutzgebieten, Nationalparks, nationalen Naturmonumenten, Naturdenkmälern, geschützten Landschaftsbestandteilen und gesetzlich geschützten Biotopen nach § 15 Absatz 6 Satz 2

70 – 1 000

3.2.5

Entscheidung über die Zulassung eines anderen Flächennachweises nach § 16 Absatz 1 Satz 2 und § 30 Absatz 2 Satz 2

70 – 340

3.2.6

Prüfung einer Anzeige nach § 16 Absatz 2

1.  im elektronischen (digitalen) Format
2.  im konventionellen (Papier-) Format

  
  
70 – 530  
120 – 700

3.2.7

Entscheidung über die Zulassung der späteren Anzeige nach § 16 Absatz 2 Satz 3

70 – 340

3.2.8

Prüfung der nach § 17 Absatz 7 und § 18 Absatz 7 vorgelegten Lieferscheine, der nach § 31 Absatz 2 Satz 3 vorgelegten Unterlagen sowie der nach § 32 Absatz 5 vorgelegten Untersuchungsergebnisse

70 – 340

3.2.9

Entscheidung über die Anerkennung als Träger der Qualitätssicherung nach § 20

140 – 2 820

3.2.10

Überwachung des Trägers der Qualitätssicherung nach § 24

40 – 500

3.2.11

Widerruf der Anerkennung nach § 25 Absatz 1

40 – 200

3.2.12

Entscheidung über die befristete erneute Anerkennung nach § 25 Absatz 2 Satz 2

40 – 500

3.2.13

Entscheidung über die Genehmigung der befristeten weiteren Führung eines Qualitätszeichens nach § 25 Absatz 3 Satz 2

40 – 80

3.2.14

Entscheidung in Bezug auf die Vorlage von Untersuchungsergebnissen nach § 31 Absatz 1 Satz 1 Nummer 4

40 – 220

3.2.15

Entscheidung über die Befreiung vom Regelverfahren nach § 31 Absatz 2 und die Befreiung von der Pflicht zur Erstellung und Übersendung des Lieferscheins nach § 31 Absatz 4

70 – 530

3.2.16

Widerruf der Befreiung nach § 31 Absatz 2 Satz 3

70 – 530

3.2.17

Prüfung eines Antrags auf Notifizierung von Untersuchungsstellen nach § 33 Absatz 2

140 – 2 820

3.2.18

Zusätzlich zu Ziff.
3.2.17:  
Prüfung der Gleichwertigkeit von Nachweisen aus einem anderen Mitgliedstaat der Europäischen Union oder aus einem anderen Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum nach § 33 Absatz 4 Satz 2

140 – 1 410

3.2.19

Prüfung der Gleichwertigkeit von Notifizierungen aus einem anderen Mitgliedstaat der Europäischen Union oder aus einem anderen Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum nach § 33 Absatz 4 Satz 1

140 – 2 820

3.2.20

Entscheidung über die Verwendung gleichwertiger Analysemethoden nach Anlage 2 Nummer 1.3 Satz 3 und Nummer 2.3 Satz 11 und über die Festlegung der Analysemethode nach Anlage 2 Nummer 1.3 Satz 4 und Nummer 2.3 Satz 13

70 – 340

**3.3**

**Ausnahmegenehmigungen nach § 4 Absatz 2 Satz 2 der Altölverordnung (AltölV)**

 

3.3.1

Entscheidung über Ausnahmegenehmigungen nach § 4 Absatz 2 Satz 2 AltölV und Bestimmung einer Untersuchungsstelle nach § 5 Absatz 2 Satz 2 AltölV

51 bis 256

**3.4**

**Verpackungsverordnung (VerpackV)**

 

3.4.1

Entscheidung über die Feststellung der flächendeckenden Einrichtung nach § 6 Absatz 5 VerpackV

5 113 bis 25 565

3.4.2

Entscheidung über die Anpassung der Höhe der Sicherheitsleistung nach § 6 Absatz 5 VerpackV

50 bis 200

3.4.3

Vollständiger oder teilweiser Widerruf der Feststellung nach § 6 Absatz 6 VerpackV

2 556 bis 10 226

3.4.4

Entgegennahme und Prüfung der Bescheinigung nach § 6 Absatz 3 und Nummer 4 des Anhangs I VerpackV

102 bis 511

3.4.5

Entgegennahme und Prüfung des testierten Nachweises nach § 6 Absatz 3 und Nummer 2 Absatz 3 des Anhangs I VerpackV

511 bis 2 556

**3.5**

**Nachweisverordnung (NachwV)**

 

3.5.1

Entscheidung über die Freistellung eines Abfallentsorgers von der Bestätigungspflicht für Entsorgungsnachweise (§ 7 Absatz 3 NachwV)

256 bis 5 113

3.5.2

Anordnung der Nachweisführung, auch in Verbindung mit einem Widerruf der Freistellung (§ 8 NachwV)

51 bis 600

3.5.3

Entscheidung über die Zulassung besonderer Formen der Nachweisführung gegenüber privaten Entsorgungsträgern (§ 14 NachwV)

50 bis 200

3.5.4

Anordnungen bei Störungen des Kommunikationssystems (§ 22 NachwV)

 

3.5.4.1

Anordnung der Prüfung von Nachweisvorgängen bei Störung des Kommunikationssystems (§ 22 Absatz 2 Nummer 1 NachwV)

50 bis 400

3.5.4.2

Anordnung der Prüfung des Kommunikationssystems des Nachweispflichtigen (§ 22 Absatz 2 Nummer 2 NachwV)

50 bis 400

3.5.4.3

Anordnung der Nachweisführung mittels Formblätter (§ 22 Absatz 2 Nummer 3 NachwV)

50 bis 400

3.5.5

_Nicht besetzt_

 

3.5.6

Entscheidung über die Befreiung von Nachweis- und Registerpflichten und Anforderung anderer geeigneter Nachweise (§ 26 Absatz 1 NachwV)

102 bis 2 556

3.5.7

Anordnung der Registrierung weiterer Angaben (§ 26 Absatz 2 NachwV)

25 bis 200

3.5.8

Anordnung zur bestimmten Verwendung der Nachweise (§ 27 Absatz 2 NachwV)

25 bis 500

3.5.9

Vergabe und Änderung von Kennnummern (Erteilung oder Änderung der Identifikations-, Erzeuger-, Beförderer-, Sammler-, Händler-, Makler- und Entsorgernummern oder der Freistellungs- und Registriernummern, § 28 Absatz 1 und 2 NachwV)

25 bis 100

**3.6**

**POP-Abfall-Überwachungs-Verordnung (POP-Abfall-ÜberwV)**

 

3.6.1

Aufgaben im Zusammenhang mit dem Nachweis über die Zulässigkeit der vorgesehenen Entsorgung

Es gelten die Gebühren für entsprechende Gebührentatbestände nach Tarifstelle 3.5 (NachwV)

**3.7**

**Verordnung über Entsorgungsfachbetriebe (EfbV)**

 

3.7.1

Entscheidung über die Anerkennung von Lehrgängen zur Fachkunde nach § 9 Absatz 2 Nummer 3 EfbV gegenüber dem Lehrgangsträger

256 bis 511

3.7.2

Entscheidung über die Anerkennung von Lehrgängen zur Fachkunde nach § 11 Absatz 2 EfbV gegenüber dem Lehrgangsträger

102 bis 256

3.7.3

Widerruf der Zustimmung zum Überwachungsvertrag (§ 15 Absatz 4 EfbV)  
  
\-           soweit der Widerruf einen einzelnen Überwachungsvertrag betrifft

256 bis 2 556  
  
51 bis 511

3.7.4

Verpflichtung zum Entzug von Überwachungszertifikat und Überwachungszeichen (§ 14 Absatz 4 Nummer 2 EfbV)

256 bis 511

3.7.5

Entscheidung über einen Antrag auf Gestattung der weiteren Führung des Überwachungszertifikates bei Unwirksamkeit des Überwachungsvertrages (§ 16 EfbV)

102 bis 511

**3.8**

**Richtlinie für die Tätigkeit und die Anerkennung von Ent-sorgergemeinschaften (Entsorgergemeinschaftenrichtlinie)**

 

3.8.1

Entscheidung über die Anerkennung und den Widerruf einer Entsorgergemeinschaft (§ 11 der Entsorgergemeinschaftenrichtlinie)

nach den Tarifstellen 3.1.23.2 und 3.1.23.3

3.8.2

Verpflichtung zum Entzug von Überwachungszertifikat und Überwachungszeichen (§ 8 Absatz 1 Nummer 2 der Entsorgergemeinschaftenrichtlinie)

256 bis 511

3.8.3

Entscheidung über einen Antrag auf Gestattung der weiteren Führung des Überwachungszertifikates und -zeichens bei Unwirksamkeit der Anerkennung der Entsorgergemeinschaft (§ 12 der Entsorgergemeinschaftenrichtlinie)

102 bis 511

**3.9**

**Abfallkompost- und Verbrennungsverordnung (AbfKompVbrV)**

 

3.9.1

Entscheidung über eine Genehmigung zum Verbrennen pflanzlicher Abfälle nach § 3 Absatz 1 AbfKompVbrV

51 bis 256

**3.10**

**Umweltrahmengesetz der DDR vom 29.
Juni 1990 (URG)**

 

3.10.1

Entscheidung über die Freistellung von der Verantwortung für vor dem 1.
Juli 1990 verursachten Schäden nach Artikel 1 § 4 Absatz 3 URG

256 bis 25 565

**3.11**

**Brandenburgisches Abfall- und Bodenschutzgesetz (BbgAbfBodG)**

 

3.11.1

Entscheidung über die Zulassung von Ausnahmen von einer Veränderungssperre (§ 19 Absatz 6 BbgAbfBodG)

15 bis 153

3.11.2

Anordnungen bei unzulässiger Abfallbehandlung, -lagerung oder -ablagerung (§ 24 BbgAbfBodG)

26 bis 2 556

**3.12**

**Altfahrzeugverordnung (AltfahrzeugV)**

 

3.12.1

Erlaubnis Restkarossen nach § 4 Absatz 4 Satz 2 AltfahrzeugV einer sonstigen Anlage zu überlassen

200 bis 2 000

**3.13**

**Batteriegesetz**

 

3.13.1

Genehmigung eines herstelleigenen Rücknahmesystems nach § 7 Absatz  1 Batteriegesetz

500 bis 7 500

**3.14**

**Bioabfallverordnung (BioAbfV)**

 

3.14.1

Entscheidung über einen Antrag auf Bestimmung als Untersuchungsstelle nach § 3 Absatz 8a BioAbfV

128 bis 1 278 

3.14.2

Entscheidung über Ausnahmen nach § 3 Absatz 3 Satz 2 und 4, § 3 Absatz 6 Satz 3 und 4, § 4 Absatz 3 Satz 4, § 4 Absatz 5 Satz 2 und 3, § 6 Absatz 1 Satz 3, § 6 Absatz 2 Satz 1, § 6 Absatz 3, § 9 Absatz 3 Satz 2, § 9 Absatz 4 Satz 1 und § 10 Absatz 2 Satz 1 BioAbfV

26 bis 383

3.14.3

Anordnungen nach § 3 Absatz 7 Satz 3, § 4 Absatz 5 Satz 3, § 6 Absatz 2 Satz 1, § 7 Absatz 4 Satz 2 und § 9 Absatz 2 Satz 5 BioAbfV

26 bis 383

3.14.4

Zustimmung nach § 9a Absatz 1 Satz 1 BioAbfV

26 bis 383

3.14.5

Befreiung nach § 11 Absatz 3 Satz 1 BioAbfV

26 bis 383

3.14.6

Befristung nach § 13b Absatz 2 Satz 2 BioAbfV

26 bis 100

**3.15**

**Verordnung über Betriebsbeauftragte für Abfall (AbfBeauftrV)**

 

3.15.1

Gestattung der Bestellung eines nicht betriebsangehörigen Betriebsbeauftragten für Abfall (§ 4 AbfBeauftrV), je Person

51 bis 511

3.15.2

Gestattung der Bestellung eines Betriebsbeauftragten für einen Konzernbereich (§ 5 AbfBeauftrV), je Person

51 bis 511

3.15.3

Befreiung von der Verpflichtung zur Bestellung eines Betriebsbeauftragten für Abfall (§ 6 AbfBeauftrV)

102

**3.16**

**Bundes-Bodenschutzgesetz (BBodSchG)**

 

3.16.1

Anordnungen zur Entsiegelung (§ 5 Satz 2 BBodSchG)

102 bis 2 045

3.16.2

Anordnungen zur Durchführung von Untersuchungen durch die in § 4 Absatz 3, 5 und 6 BBodSchG genannten Personen bei Verdacht auf schädliche Bodenveränderungen oder Altlasten (§ 9 Absatz 2 BBodSchG)

51 bis 1 800

3.16.3

Anordnung zur Durchführung von Untersuchungen zur Entscheidung über Art und Umfang der erforderlichen Maßnahmen (§ 30 Absatz 1 BbgAbfBodG in Verbindung mit § 13 Absatz 1 BBodSchG)

51 bis 1 800

3.16.4

Abschluss einer öffentlich-rechtlichen Untersuchungsvereinbarung (§ 54 Satz 2 VwVfG in Verbindung mit § 30 Absatz 1 BbgAbfBodG und § 10 Absatz 1, § 9 Absatz 2 oder § 13 Absatz 1 BBodSchG)

51 bis 1 800

3.16.5

Anordnung der notwendigen Maßnahmen zur Erfüllung der Pflichten aus den §§ 4 und 7 und den auf Grund von § 5 Satz 1, § 6 und § 8 erlassenen Rechtsverordnungen gegenüber den Verpflichteten (§ 10 Absatz 1 BBodSchG) 

102 bis 2 045  
 

3.16.6

Abschluss einer öffentlich-rechtlichen Sanierungsvereinbarung (§ 54 Satz 2 VwVfG in Verbindung mit § 10 Absatz 1, § 4 Absatz 3, 5 oder 6 BBodSchG)

102 bis 2 045

3.16.7

Anordnung zur Vorlage eines Sanierungsplans (§ 30 Absatz 1 BbgAbfBodG in Verbindung mit  § 13 Absatz 1 BBodSchG)

51 bis 1 800

3.16.8

_Nicht besetzt_

 

3.16.9

Verbindlichkeitserklärung des Sanierungsplans (§ 30 Absatz 1 in Verbindung mit  § 13 Absatz 6 BBodSchG)

51 bis 1 800

3.16.10

Anordnung von Eigenkontrollmaßnahmen; sonstige Anordnungen zur Erfüllung der Pflichten gemäß § 30 Absatz 1 BbgAbfBodG in Verbindung mit  § 15 Absatz 2 und 3 BBodSchG  

51 bis 767

3.16.11

Bestimmung von geeigneten Sachverständigen und Untersuchungsstellen (§ 18 BBodSchG)  
  
\-  soweit die Tätigkeit der Untersuchungsstelle sich lediglich auf die Probenahme bezieht

128 bis 1 278  
  
50 bis 250

3.16.12

Festsetzung eines Ausgleichsbetrages (§ 25 Absatz 1 BBodSchG)

51 bis 767

**3.17**

**Verordnung über das Anzeige- und Erlaubnisverfahren für Sammler, Beförderer, Händler und Makler von Abfällen (Anzeige- und Erlaubnisverordnung – AbfAEV)**

 

3.17.1

Anordnung zur Teilnahme an einem Lehrgang und/oder regelmäßige entsprechende Fortbildung

50 bis 500

3.17.2

Anerkennung von Lehrgängen (§ 5 Absatz 3 AbfAEV)

100 bis 5 000

3.17.3

Anordnung zur Erstellung eines schriftlichen Einarbeitungsplanes und/oder zu dessen Vorlage bei der Behörde (§ 6 Satz 3 AbfAEV)

100 bis 2 000

3.17.4

Aufforderung zur Vorlage oder Ergänzung der Anzeige (§ 7 Absatz 1 und 4, § 5 Absatz 3 AbfAEV) oder Erlaubnis gemäß § 9 Absatz 4, § 5 Absatz 3 und § 10 Absatz 2 AbfAEV

50 bis 500

3.17.5

Anordnung der Durchführung eines Erlaubnisverfahrens (§ 12 Absatz 2 AbfAEV)

500 bis 3 000

3.17.6

Entscheidung über den Antrag auf Freistellung von der Kennzeichnungspflicht gemäß § 13a AbfAEV, Forderung einer anderen geeigneten Kennzeichnung

100 bis 500

**3.18**

**Verordnung zur Umsetzung der Richtlinie 2006/21/EG des Europäischen Parlaments und des Rates vom 15.
März 2006 über die Bewirtschaftung von Abfällen aus der mineralgewinnenden Industrie und zur Änderung der Richtlinie 2004/35/EG (Gewinnungsabfallverordnung – GewinnungsAbfV)**

 

3.18.1

Nachweisprüfung und Entscheidungen im Zusammenhang mit den sonstigen Anforderungen nach § 3 Satz 4 GewinnungsAbfV

nach Tarifstelle 3.21.1 bis 3.21.13

3.18.2

Prüfung und Entscheidung über einen Antrag auf Errichtung, wesentliche Änderung und Betrieb einer Beseitigungsanlage für Gewinnungsabfälle, Durchführung des Zulassungsverfahrens  nach § 8 GewinnungsAbfV

nach Tarifstelle 3.1.10

**3.19**

**Verordnung über das europäische Abfallverzeichnis (Abfallverzeichnis-Verordnung – AVV)**

 

3.19.1

Umstellungsanordnung nach § 2 Absatz 3 AVV

25,50 bis 128

3.19.2

Entscheidung über eine Einstufung eines Abfalls, die von der Einstufung nach § 3 Absatz 1 AVV abweicht (§ 3 Absatz 3 Satz 1 AVV) bzw.
Entscheidung über Einstufung als gefährliche Abfälle (§ 3 Absatz 3 Satz 2 AVV)  
  
(gegebenenfalls anfallende Kosten einer Untersuchung des Abfalls sind als Auslagen gesondert zu berechnen)

  
  
  
51 bis 256

**3.20**

**Verordnung über die Entsorgung von Altholz (Altholzverordnung – AltholzV)**

 

3.20.1

Zustimmung zum einfachen Prüfverfahren nach § 6 Absatz 3 AltholzV

100 bis 1 000

3.20.2

Entscheidung über die Bekanntgabe als Stelle nach § 6 Absatz 6 Satz 1 AltholzV  
  
\-           soweit sich die Tätigkeit der Untersuchungsstelle nur auf die Probenahme bezieht

130 bis 1 300  
  
50 bis 300

3.20.3

Prüfung der Ergebnisse der Fremdkontrolle nach § 6 Absatz 6 AltholzV

20 bis 400

**3.21**

**Verordnung über Deponien und Langzeitlager (Deponieverordnung – DepV)**

 

3.21.1

Zulassung von Ausnahmen sowie Herabsetzung der Anforderungen nach § 3 Absatz 1 und 3 DepV

100 bis 1 000

3.21.2

Abnahme der für den Betrieb der Deponie erforderlichen Einrichtungen nach § 5 Satz 1 DepV

100 bis 1 000

3.21.3

Zustimmung zur abweichenden Ablagerung nach § 6 Absatz 6 DepV

100 bis 1 500

3.21.4

Genehmigung des Nachweises nach Artikel 7 Absatz 4 Buchstabe b Ziffer i) der Verordnung (EG) Nummer 850/2004 nach § 8 Absatz 1 Nummer 11 DepV 

100 bis 500

3.21.5

Entgegennahme und Prüfung des Nachweises bei nicht erforderlichen Abfalluntersuchungen nach § 8 Absatz 2 DepV

50 bis 100

3.21.6

Zustimmung zur Reduzierung von Beprobungen nach § 8 Absatz 3 DepV  

100 bis 800

3.21.7

Festlegung einer höheren Anzahl oder Reduzierung von Kontrolluntersuchungen nach § 8 Absatz 5 und § 17 Absatz 1 DepV 

100 bis 800

3.21.8

Zulassung von Ausnahmen für Betreiber von Monodeponien und für Betreiber von Deponien der Deponieklasse 0 nach § 8 Absatz 8 und § 12 Absatz 1 Satz 2 und Absatz 3 DepV 

100 bis 2 000

3.21.9

Zulassung von Ausnahmen für Betreiber von Monodeponien und für Betreiber von Deponien der Deponieklasse 0 nach § 8 Absatz 9 DepV

100 bis 750

3.21.10

Entscheidung über den Antrag auf endgültige Stilllegung der Deponie nach § 10 Absatz 2 DepV sowie Feststellung des Abschlusses der Nachsorgephase nach § 11 Absatz 2 DepV

nach Tarifstelle 3.1.15.2

3.21.11

Festlegungen von Auslöseschwellen und Grundwasser-Messstellen sowie Zulassung von Ausnahmen nach § 12 Absatz 1 DepV

50 bis 800

3.21.12

Zulassung von Ausnahmen für Betreiber von Monodeponien und für Betreiber von Deponien der Deponieklasse 0 nach § 12 Absatz 1 Satz 2 und Absatz 3 Satz 3 DepV

100 bis 750

3.21.13

Zustimmung zu den Maßnahmeplänen, Entgegennahme der Informationen bei Überschreitung der Auslöseschwellen nach § 12 Absatz 4 DepV

100 bis 800

3.21.14

Anordnung der Ermittlung von Emissionen und Bestimmung der Stellen nach § 12 Absatz 5 DepV

100 bis 1 500

3.21.15

Zulassung von Ausnahmen nach § 13 Absatz 2, § 17 Absatz 2 und § 25 Absatz 3 DepV

100 bis 1 500

3.21.16

Bestimmung eines Sachverständigen nach § 21 Absatz 4 und § 24 DepV

50 bis 250

3.21.17

Entscheidung über einen Antrag über ergänzende Anforderungen nach § 25 Absatz 4 DepV

100 bis 800

3.21.18

Zustimmung der Behörde zu der Überschreitung von Grenzwerten nach § 6 Absatz 2 bis 5, § 8 Absatz 1, 3 und 5, § 14 Absatz 3, § 15, § 23, § 25 Absatz 1 DepV

100 bis 1 500

**3.22**

**Verordnung über die Entsorgung von gewerblichen Siedlungsabfällen und von bestimmten Bau- und Abbruchabfällen (Gewerbeabfallverordnung – GewAbfV)**

 

3.22.1

1.  Prüfung nach § 3 Absatz 2 Satz 3 GewAbfV, soweit der Erzeuger bzw.
    Besitzer die Anforderungen nach Satz 1 nicht erfüllt hat

  
100 bis 1 000

 

2.  Prüfung nach § 3 Absatz 3 Satz 2 GewAbfV, soweit die Ausnahmeanforderung nach Absatz 3 nicht erfüllt sind

  
100 bis 1 000

3.22.2

Zulassung von Ausnahmen nach § 3 Absatz 4 Satz 1 und 3 GewAbfV

100 bis 1 000

3.22.3

Verlängerung der versuchsweisen Vorbehandlung gemäß § 3 Absatz 4 Satz 4 GewAbfV

15 Prozent der Gebühr nach Tarifstelle 3.22.2, mindestens 50

3.22.4

Prüfung bei Unterschreitung der Verwertungsquote nach § 5 Absatz 4 Satz 2 und 3 GewAbfV

50 bis 800

3.22.5

Prüfung der Ergebnisse der Fremdkontrolle nach § 9 Absatz 6 Satz 4 bzw.
bei Entsorgungsfachbetrieben der Ergebnisse der Überwachung nach § 9 Absatz 6 Satz 6 GewAbfV

20 bis 400

3.22.6

Entscheidung über die Bekanntgabe als Stelle nach § 9 Absatz 6 Satz 1 GewAbfV

130 bis 1 300

**3.23**

**Verordnung (EG) Nr. 1013/2006 des Europäischen Parlaments und des Rates über die Verbringung von Abfällen; Abfallverbringungsgesetz (AbfVerbrG)**

 

3.23.1

Entscheidung über eine Einzel- oder Sammelnotifizierung oder eine Zustimmung nach den Artikeln 4 bis 17, 35, 38, 41, 42, 43 und 46 der Verordnung Nr. 1013/2006

100 bis 15 000

3.23.2

Überwachungsmaßnahmen (z.
B.  Entnahme und Untersuchung von Proben) nach Artikel 50 der Verordnung Nr. 1013/2006 in Verbindung mit den §§ 11 und 12 AbfVerbrG, soweit sie durch einen Verstoß des Notifizierenden gegen bestehende Rechtsvorschriften oder behördliche Entscheidungen veranlasst waren

100 bis 4 000

3.23.3

Anordnungen im Einzelfall nach § 13 AbfVerbrG (z.
B.  zur Erfüllung der Rücknahmepflichten)

100 bis 4 000

3.23.4

Sonstige Amtshandlungen nach dem AbfVerbrG in Verbindung mit der Verordnung Nummer 1013/2006 für die keine andere, insbesondere auch keine bundesrechtliche Tarifstelle vorgesehen ist

25 bis 2 000

3.23.5

Widerruf einer Zustimmung zu einer grenzüberschreitenden Abfallverbringung aufgrund der Verordnung 1013/2006/EG

die für die Zustimmung festgesetzte Gebühr

**3.24**

**Verordnung über persistente organische Schadstoffe (Verordnung \[EG\] Nr. 850/2004)**

 

3.24.1

Zulassung von Ausnahmen nach Artikel 7 Absatz 4 Buchstabe b der Verordnung über persistente organische Schadstoffe

50 bis 1 500

**3.25**

**Elektro- und Elektronikgerätegesetz (ElektroG)**

 

3.25.1

Anordnungen (§ 2 Absatz 3 ElektroG in Verbindung mit § 62 KrWG) zur Durchsetzung der Anforderungen des Elektro- und Elektronikgerätegesetzes

nach Tarifstelle 3.1.25

3.25.2

Festsetzen der Kosten für die Sammlung, Sortierung und Entsorgung von Altgeräten (§ 5 Absatz 2 Satz 2 ElektroG)

100 bis 5 000

**4**

**Naturschutzrechtliche Angelegenheiten**

 

**4.1**

**Schutz bestimmter Teile von Natur und Landschaft**

 

4.1.1

Entscheidung über die Befreiung gemäß § 67 Absatz 1 und 2 des Bundesnaturschutzgesetzes (BNatSchG) sowie Entscheidung über die Befreiung vom Veränderungsverbot nach § 22 Absatz 3 BNatSchG bei Verfügungen oder Verordnungen zur einstweiligen Sicherstellung (§ 22 Absatz 3 BNatSchG) oder bei Unterschutzstellungsverfahren (§ 28 Absatz 2 Satz 3 BbgNatSchG)

30 bis 5 000

4.1.2

Entscheidung über die Genehmigung von Handlungen im Sinne des § 19 Absatz 2 Satz 4 und 5 BbgNatSchG

30 bis 5 000

4.1.3

Entscheidung über die Genehmigung und die Befreiung gemäß § 19 BbgNatSchG und § 67 BNatSchG von Schutzvorschriften, die nach § 78 BbgNatSchG übergeleitet wurden

30 bis 5 000

4.1.4

Entscheidung über die Ausnahme gemäß § 34 Absatz 3 bis 5 BNatSchG auch in Verbindung mit § 33 Absatz 1 Satz 2 und Absatz 2 Satz 1 BNatSchG und § 36 BNatSchG

30 bis 5 000

4.1.5

Prüfung einer Anzeige zur Durchführung eines Projektes sowie Anordnung von Beschränkungen, der vorläufigen Einstellung oder der Untersagung des Projektes gemäß § 34 Absatz 6 Sätze 1, 2, 4 und 5 BNatSchG

30 bis 5 000

4.1.6

Entscheidung über die Ausnahme nach § 72 Absatz 1 BbgNatSchG von den Verboten des § 33 BbgNatSchG

30 bis 5 000

4.1.7

Entscheidung über die Ausnahme nach § 30 Absatz 3 BNatSchG von den Verboten des § 30 Absatz 2 BNatSchG und § 32 Absatz 1 Nummer 3 bis 5 BbgNatSchG

30 bis 5 000&

4.1.8

Entscheidung über die Ausnahme nach § 72 Absatz 2 BbgNatSchG von den Verboten der §§ 31 und 24 Absatz 4 BbgNatSchG und § 29 Absatz 2 BNatSchG bei Rechtsverordnungen oder Satzungen zum Schutz von Baumreihen entlang von Straßen und Wegen

30 bis 5 000

**4.2**

**Eingriff**

 

4.2.1

Entscheidung über die Genehmigung eines Eingriffs gemäß § 17 Absatz 3 BNatSchG

30 bis 5 000

4.2.2

Anordnung des Widerrufs der Zulassung, der Einstellung des Vorhabens, der Untersagung der Nutzung, der Wiederherstellung des früheren Zustands oder der Anordnung von Maßnahmen im Sinn des § 15 BNatSchG gemäß § 17 Absatz 8 BNatSchG und §§ 17 Absatz 5 und 72 Absatz 10 BbgNatSchG 

30 bis 5 000

 

Zusatz für Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung:

 

 

1.  wird bei der Genehmigung der Verwendung von Ödland oder naturnahen Flächen zu intensiver Landwirtschaftsnutzung oder der Errichtung von Skipisten gemäß § 17 Absatz 3 BNatSchG eine Umweltverträglichkeitsprüfung vorgenommen

  
Zuschlag bis zu 50 Prozent der nach Tarifstelle 4.2.1 festgesetzten Gebühr 

 

2.  wird bei der Genehmigung der Verwendung von Ödland oder naturnahen Flächen zu intensiver Landwirtschaftsnutzung oder der Errichtung von Skipisten gemäß § 17 Absatz 3 BNatSchG eine Vorprüfung zur Feststellung der UVP-Pflicht vorgenommen und führt die Vorprüfung zur Ablehnung der UVP-Pflicht

  
Zuschlag bis zu 15 Prozent der nach Tarifstelle 4.2.1 festgesetzten Gebühr

 

3.  wird vor der Genehmigung der Verwendung von Ödland oder naturnahen Flächen zu intensiver Landwirtschaftsnutzung oder der Errichtung von Skipisten gemäß § 17 Absatz 3 BNatSchG auf Antrag des Vorhabensträgers die UVP-Pflicht für ein vorprüfungspflichtiges Vorhaben gemäß § 3a UVPG festgestellt

  
30 bis 1 000

 

> Wird ein Genehmigungsverfahren durchgeführt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht vor Beginn des Genehmigungsverfahrens.
> Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Genehmigungsverfahren anzurechnen.

 

 

4.  wird vor der Genehmigung der Verwendung von Ödland oder naturnahen Flächen zu intensiver Landwirtschaftsnutzung oder der Errichtung von Skipisten gemäß § 17 Absatz 3 BNatSchG auf Ersuchen des Vorhabensträgers eine Unterrichtung über den Umfang beizubringender Unterlagen nach § 5 UVPG durchgeführt

  
30 bis 1 000

 

> Wird ein Genehmigungsverfahren durchgeführt, so entfällt die Gebührenpflicht für die Unterrichtung über voraussichtlich beizubringende Unterlagen vor Beginn des Genehmigungsverfahrens.
> Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Genehmigungsverfahren anzurechnen.

 

**4.3**

**Besondere Genehmigungen, Prüfungen und Maßnahmen**

 

4.3.1

Erteilung einer Genehmigung gemäß § 39 Absatz 4 BNatSchG

30 bis 1 500

4.3.2

Entscheidung über die Genehmigung zur Errichtung, Erweiterung, wesentlichen Änderung oder zum Betrieb eines Zoos gemäß § 42 Absatz 2 BNatSchG

100 bis 5 000

4.3.3

Entscheidung über die Anordnung von Maßnahmen nach § 42 Absatz 7 und 8 BNatSchG

30 bis 5 000

4.3.4

Prüfung einer Anzeige zur Errichtung, Erweiterung, wesentlichen Änderung oder zum Betrieb eines Tiergeheges gemäß § 43 Absatz 3 Satz 1 BNatSchG sowie Entscheidung über die Anordnung von Maßnahmen nach § 43 Absatz 3 Satz 2 bis 4 BNatSchG

30 bis 5 000

4.3.5

Entscheidung über die Genehmigung zur Sperrung von Wegen oder Flächen gemäß § 46 BbgNatSchG

30 bis 3 000

4.3.6

Entscheidung über die Ausnahme vom Bauverbot an Gewässern gemäß § 61 Absatz 3 BNatSchG

50 bis 5 000

**4.4**

**Sonstige Entscheidungen und Maßnahmen**

 

4.4.1

Entscheidung über die Anordnung der Durchführung von Schutz- Pflege- und Entwicklungsmaßnahmen gemäß § 7 Absatz 6 Satz 5 BbgNatSchG

30 bis 1 500

4.4.2

Entscheidung über die Zertifizierung von Flächen- oder Maßnahmenpools gemäß § 14 Absatz 2 BbgNatSchG in Verbindung mit § 2 Absatz 4 FPV

300 bis 3 000

4.4.3

Entscheidungen über Anträge auf Anerkennung von Agenturen gemäß § 14 Absatz 2 Nummer 3 BbgNatSchG in Verbindung mit § 4 Absatz 1 und 4 FPV

300 bis 3 000

4.4.4

Entscheidungen über Voranfragen und Anträge auf Änderungen oder Aufhebung von Rechtsverordnungen gemäß § 28 Absatz 7 BbgNatSchG

300 bis 3 000

4.4.5

Entscheidung über die Überprüfung und Änderung von Horstschutzzonen oder -schutzfristen gemäß § 33 Absatz 2 BbgNatSchG

30 bis 3 000

4.4.6

Entscheidung über Maßnahmen gemäß § 3 Absatz 2 BNatSchG

30 bis 5 000

4.4.7

Entscheidung über die Erteilung einer Bescheinigung (Negativzeugnis) über das Vorkaufsrecht gemäß § 69 BbgNatSchG und § 66 BNatSchG

30 bis 150

4.4.8

Entscheidung über einen Anspruch auf Entschädigung nach § 68 BNatSchG und § 71 BbgNatSchG sowie nach § 72 Absatz 11 BbgNatSchG

30 bis 3 000

4.4.9

Erklärungen der für die Überwachung der „Natura 2000“-Gebiete zuständigen Behörde gemäß § 34 BNatSchG in Verbindung  mit Anträgen auf finanzielle Beteiligung gemäß Artikel 39 bis 41 der Verordnung (EG) Nr. 1083/2006 EFRE/Kohäsionsfonds und Artikel 6 Absatz 3 FFH-Richtlinie für Projekte

300 bis 3 000

**4.5**

**Besonderer Artenschutz**

 

4.5.1

Anordnung von Bewirtschaftungsvorgaben gemäß § 44 Absatz 5 Satz 3 BNatSchG

30 bis 5 000

4.5.2

Entscheidung über die Ausnahme von den Verboten des § 44 BNatSchG gemäß § 45 Absatz 7 BNatSchG

30 bis 1 500

4.5.3

Entscheidung über die Genehmigung, Tiere und Pflanzen gebietsfremder oder standortfremder Arten auszusetzen oder in der freien Natur anzusiedeln gemäß § 40 Absatz 4 BNatSchG

30 bis 1 500

4.5.4

Entscheidung über die Ausnahme für verbotene Handlungen, Verfahren und Geräte gemäß § 4 Absatz 3 der Bundesartenschutzverordnung (BArtSchV)

30 bis 5 000

4.5.5

Entscheidung über Ausnahmen gemäß

 

4.5.5.1

§ 2 Absatz 1 Satz 2 oder Absatz 2 BArtSchV

30 bis 1 500

4.5.5.2

§ 6 Absatz 1 Satz 4 BArtSchV

30 bis 1 500

4.5.5.3

§ 7 Absatz 3 Satz 2 BArtSchV

30 bis 1 500

4.5.5.4

§ 14 Absatz 1 Satz 2 BArtSchV

5 bis 1 500

4.5.6

Amtshandlungen nach § 13 Absatz 1 Satz 4 bis 8 BArtSchV

5 bis 1 500

4.5.7

Amtshandlungen nach § 47 BNatSchG

50 bis 3 000

4.5.8

Amtshandlungen auf Grund der Verordnung (EG) Nr. 338/97 des Rates über den Schutz von Exemplaren wildlebender Tier- und Pflanzenarten durch Überwachung des Handels vom 9. Dezember 1996 (ABl.
L 61 vom 3.3.1997, S.
1) in Verbindung mit der Verordnung (EG) Nr. 1808/2001 der Kommission mit Durchführungsbestimmungen zur Verordnung (EG) Nr. 338/97, dem Washingtoner Artenschutzübereinkommen und dem Bundesnaturschutzgesetz in der jeweils geltenden Fassung

5 bis 3 000

 

Anmerkung zu der Tarifstelle 4.5:

 

 

Soweit Ausnahmen oder Befreiungen von den Verboten des besonderen Artenschutzes für Teile und Erzeugnisse von Exemplaren mit einem Warenwert bis zur Höhe von 130 EUR (Bagatellgrenze) beantragt werden, werden zur Vermeidung von Härten Gebühren nicht erhoben.
Die Bagatellgrenze ist auf den jeweiligen Geschäftsvorgang und nicht auf Einzelteile einer zusammenhängenden Sendung anzuwenden.

 

**4.6**

**Naturschutzrechtliche Entscheidungen, soweit sie in Zulassungen auf Grund anderer Gesetze eingeschlossen oder ersetzt werden**

**90 Prozent der nach Tarifstellen 4.1 bis 4.5 festgesetzten Gebühr**

 

 

 

**5** 

**Wasserrechtliche Angelegenheiten** 

 

**5.1**

**Amtshandlungen auf Grund des Wasserhaushaltsgesetzes (WHG) und des Brandenburgischen Wassergesetzes (BbgWG)**

 

5.1.1

Bewilligung oder Erlaubnis mit Verfahren nach den Anforderungen des UVPG (§§ 8 und 11 WHG und § 129a Absatz 2 BbgWG) und gehobene Erlaubnis (§ 15 WHG)

 

 

Anmerkung: Entscheidung im förmlichen Verfahren

 

 

1.  für die Entnahme und das Einleiten von Wasser oder das Einleiten von Stoffen in oberirdische Gewässer oder das Grundwasser (§ 9 Absatz 1 Nummer 1, 4 und 5 WHG sowie § 129a Absatz 2 Nummer 1, 2, 3 und 4 BbgWG) nach der Menge je m3 Nutzungsumfang

 

 

> *   bis 100 000 m3 zugelassene Jahresmenge

1,15 je angefangene 100 m3

 

> *   für die weiteren 900 000 m3

0,57 je angefangene 100 m3

 

> *   für den 1 Mio. m3 übersteigenden Teil

0,11 je angefangene 100 m3

 

 

zusätzlich für jedes weitere Jahr der Geltungsdauer der Bewilligung oder Erlaubnis 2,15 v. H.
der berechneten Gebühr, mindestens 230

 

2.  für sonstige Benutzungen oder Benutzungen nach Nummer 1, für die eine Berechnung nach Nummer 1 nicht in Betracht kommt, z.
    B.
    für Aufstauen, Absenken von Gewässern, Entnahme fester Stoffe aus einem Gewässer, sowie den Bau einer Wasserkraftanlage (§ 129a Absatz 2 Nummer 5 BbgWG) nach dem Wert der Anlage oder nach dem Zeitwert der Stoffe

 

 

> *   bis 52 000 EUR Wert

1,15 v.
H., mindestens 230

 

> *   für die weiteren 461 000 EUR Wert

0,57 v.
H.

 

> *   für den 513 000 EUR übersteigenden Teil

0,11 v.
H.

 

Zusatz für Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung:

 

 

1.  wird bei der Bewilligung eine Prüfung der Umweltverträglichkeit vorgenommen

Erhöhung der Gebühr um 10 v.
H.

 

2.  wird bei Bewilligungen eine Vorprüfung zur Feststellung der UVP-Pflicht vorgenommen und führt die Vorprüfung zur Ablehnung der UVP-Pflicht

100 bis 1 000

 

3.  Feststellung der UVP-Pflicht der Vorhaben, für die eine Vorprüfung gemäß § 3c UVPG durchzuführen ist, vor Beginn des Verfahrens auf Antrag des Vorhabensträgers (§ 3a UVPG)

100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

4.  Unterrichtung über voraussichtlich beizubringende Unterlagen bei UVP-pflichtigen Vorhaben auf Ersuchen des Vorhabensträgers vor Beginn des Verfahrens

100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

Im Falle der Durchführung einer Verträglichkeits- oder Ausnahmeprüfung nach § 34 BNatSchG bzw.
§ 26d BbgNatSchG zusätzlich

5 v.
H.
der Gebühr nach Nummer 1 oder 2, mindestens 102

 

Im Falle der Ausnahmeprüfung nach § 31 Absatz 2 und § 47 Absatz 3 in Verbindung mit § 31 Absatz 2 des Wasserhaushaltsgesetzes

bis 20 Prozent der Zulassungsgebühr

5.1.2

Erteilung einer Erlaubnis ohne förmliches Verfahren

 

 

Im Falle der Ausnahmeprüfung nach § 31 Absatz 2 und § 47 Absatz 3 in Verbindung mit § 31 Absatz 2 des Wasserhaushaltsgesetzes

bis 20 Prozent der Zulassungsgebühr

5.1.2.1

für die Einleitung von Abwasser mit gefährlichen Stoffen, für das es Anforderungen für den Ort des Anfalles oder vor der Vermischung gibt

Gebühr nach Tarifstelle 5.1.1 Nummer 1

5.1.2.2

für alle sonstigen Gewässerbenutzungen gemäß § 9 WHG

60 v.
H.
der Gebühr nach Tarifstelle 5.1.1, mindestens 115

 

Zusatz für Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung (betrifft die Tarifstellen 5.1.2.1 und 5.1.2.2):

 

 

1.  wird im Trägerverfahren eine Vorprüfung zur Feststellung der UVP-Pflicht vorgenommen und führt die Vorprüfung zur Ablehnung der UVP-Pflicht

  
100 bis 1 000 

 

2.  Feststellung der UVP-Pflicht der Vorhaben, für die eine Vorprüfung gemäß § 3c UVPG durchzuführen ist, vor Beginn des Verfahrens auf Antrag des Vorhabensträgers (§ 3a UVPG)

  
100 bis 1 000 

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

Im Falle der Durchführung einer Verträglichkeits- oder Ausnahmeprüfung nach § 34 BNatSchG bzw.
§ 26d BbgNatSchG zusätzlich

5 Prozent der Gebühr nach Tarifstellen 5.1.2.1 und 5.1.2.2, mindestens 51

5.1.3

Planfeststellung nach § 68 Absatz 1 WHG oder Plangenehmigung nach § 68 Absatz 2 WHG für Gewässerausbau und Deichbau und Vorhaben nach § 129a Absatz 1 Nummer 3, 4, 8, 9 BbgWG

,5 Prozent der Baukosten, mindestens 256  
 

 

Zusatz für Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung:

 

 

1.  wird im Trägerverfahren eine Prüfung der Umweltverträglichkeit vorgenommen

  
Erhöhung der Gebühr um 10 Prozent

 

2.  wird im Trägerverfahren eine Vorprüfung zur Feststellung der UVP-Pflicht vorgenommen und führt die Vorprüfung zur Ablehnung der UVP-Pflicht

  
100 bis 1 000

 

3.  Feststellung der UVP-Pflicht der Vorhaben, für die eine Vorprüfung gemäß § 3c UVPG durchzuführen ist, vor Beginn des Verfahrens auf Antrag des Vorhabensträgers (§ 3a UVPG)

  
100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

4.  Unterrichtung über voraussichtlich beizubringende Unterlagen bei UVP-pflichtigen Vorhaben auf Ersuchen des Vorhabensträgers vor Beginn des Verfahrens

  
100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

Im Falle der Durchführung einer Verträglichkeits- oder Ausnahmeprüfung nach § 34 BNatSchG bzw.
§ 26d BbgNatSchG zusätzlich

5 Prozent der Gebühr nach Tarifstelle 5.1.3, mindestens 180

 

Im Falle der Ausnahmeprüfung nach § 31 Absatz 2 und § 47 Absatz 3 in Verbindung mit § 31 Absatz 2 des Wasserhaushaltsgesetzes

bis 20 Prozent der Zulassungsgebühr

5.1.4

Zulassung des vorzeitigen Beginns einer Gewässerbenutzung, des Ausbaues eines Gewässers oder des Deichbaues nach §§ 17 WHG, 69 WHG 

25 Prozent der Gebühr nach Tarifstellen 5.1.1, 5.1.2.1, 5.1.2.2 oder 5.1.3, mindestens 51

 

Im Falle der Durchführung einer Verträglichkeits- oder Ausnahmeprüfung nach § 34 BNatSchG bzw.
§ 26d BbgNatSchG zusätzlich

5 Prozent der Gebühr nach Tarifstelle 5.1.4, mindestens 153

 

Im Falle der Ausnahmeprüfung nach § 31 Absatz 2 und § 47 Absatz 3 in Verbindung mit § 31 Absatz 2 des Wasserhaushaltsgesetzes

bis 20 Prozent der Zulassungsgebühr

5.1.5

Anlagenzulassungen, Anzeige der Errichtung und des Betriebes von Anlagen

 

5.1.5.1

Abwasseranlagen

 

5.1.5.1.1

Genehmigung von Abwasserbehandlungsanlagen mit Umweltverträglichkeitsprüfung (§ 60 Absatz 3 WHG)  

 

 

*   für die ersten 52 000 EUR Baukostenwert

  
1,2 Prozent, mindestens 256

 

*   für die weiteren 461 000 EUR Baukostenwert

  
0,4 Prozent

 

*   für die weiteren 4 602 000 EUR Baukostenwert

  
0,2 Prozent

 

*   für die weiteren 46 017 000 EUR Baukostenwert

  
0,02 Prozent

 

*   für den 51 132 000 EUR übersteigenden Teil

  
0,002 Prozent

 

Zusatz für Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung:

 

 

1.  Feststellung der UVP-Pflicht der Vorhaben, für die eine Vorprüfung gemäß § 3c UVPG durchzuführen ist, vor Beginn des Verfahrens auf Antrag des Vorhabensträgers (§ 3a UVPG)

  
100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

2.  Unterrichtung über voraussichtlich beizubringende Unterlagen bei UVP-pflichtigen Vorhaben auf Ersuchen des Vorhabensträgers vor Beginn des Verfahrens

  
100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

Im Falle der Durchführung einer Verträglichkeits- oder Ausnahmeprüfung nach § 34 BNatSchG bzw.
§ 26d BbgNatSchG zusätzlich

5 Prozent der Gebühr nach Tarifstelle 5.1.5.1.1, mindestens 128

5.1.5.1.2

Genehmigung von Abwasserbehandlungsanlagen (§ 71 Absatz 2 BbgWG)

 

 

*   für die ersten 52 000 EUR Baukostenwert

  
1 v.
H., mindestens 180

 

*   für die weiteren 461 000 EUR Baukostenwert

  
0,2 v.
H.

 

*   für die weiteren 4 602 000 EUR Baukostenwert

  
0,1 v.
H.

 

*   für die weiteren 46 017 000 EUR Baukostenwert

  
0,01 v.
H.

 

*   für den 51 132 000 EUR übersteigenden Teil

  
0,001 v.
H.

 

Sofern es sich nur um die Genehmigung des Betriebes einer bestehenden Abwasserbehandlungsanlage handelt

Zeitgebühr

5.1.5.1.3

Prüfung einer Anzeige eines Kanalisationsnetzes für die öffentliche Abwasserbeseitigung (§ 71 Absatz 1 BbgWG)

200 bis 2 500

5.1.5.1.4

Entscheidung über die Zulassung des vorzeitigen Baubeginns (§ 17 WHG i. V. m.
§ 60 Absatz 3 Satz 3 WHG)

25 v.
H.
der für die Genehmigung nach Tarifstelle 5.1.5.1.1 zu erhebenden Gebühr

 

Für Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung oder der Durchführung einer FFH-Verträglichkeits- und Ausnahmeprüfung gelten die Festlegungen in Tarifstelle 5.1.5.1.1.
Die hierfür festgesetzte Gebühr wird auf die gemäß Tarifstelle 5.1.5.1.1 im Genehmigungsverfahren festzusetzende Gebühr für Handlungen im Zusammenhang mit der Umweltverträglichkeitsprüfung oder die Verträglichkeits- und Ausnahmeprüfung angerechnet.

 

5.1.5.2

Planfeststellung und Plangenehmigung eines Hafens oder eines Landungssteges nach § 129a Absatz 1 Nummer 5, 6 und 7 BbgWG

 

 

*   für die ersten 52 000 EUR Baukostenwert

  
1,2 Prozent

 

*   für die weiteren 461 000 EUR Baukostenwert

  
0,4 Prozent

 

*   für den 513 000 EUR übersteigenden Teil

  
0,2 Prozent

 

Zusatz für Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung:

 

 

1.  Feststellung der UVP-Pflicht der Vorhaben, für die eine Vorprüfung gemäß § 3c UVPG durchzuführen ist, vor Beginn des Verfahrens auf Antrag des Vorhabensträgers (§ 3a UVPG)

  
100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

2.  Unterrichtung über voraussichtlich beizubringende Unterlagen bei UVP-pflichtigen Vorhaben auf Ersuchen des Vorhabensträgers vor Beginn des Verfahrens

  
100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

Im Falle der Durchführung einer Verträglichkeits- oder Ausnahmeprüfung nach § 34 BNatSchG bzw.
§ 26d BbgNatSchG zusätzlich

5 Prozent, bei Durchführung einer UVP 2 Prozent nach Tarifstelle 5.1.5.2, mindestens 128

 

Im Falle der Ausnahmeprüfung nach § 31 Absatz 2 und § 47 Absatz 3 in Verbindung mit § 31 Absatz 2 des Wasserhaushaltsgesetzes

bis 20 Prozent der Zulassungsgebühr

5.1.5.3

Genehmigung der Errichtung oder der wesentlichen Veränderung von Anlagen in und an Gewässern (§ 87 BbgWG)

 

 

*   für die ersten 52 000 EUR Baukostenwert

  
1,1 v.
H., mindestens 85

 

*   für die weiteren 461 000 EUR Baukostenwert

  
0,22 v.
H.

 

*   für den 513 000 EUR übersteigenden Teil

  
0,11 v.
H.

 

Zusatz für Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung:

 

 

1.  wird im Trägerverfahren eine Vorprüfung zur Feststellung der UVP-Pflicht vorgenommen und führt die Vorprüfung zur Ablehnung der UVP-Pflicht

  
100 bis 1 000

 

2.  Feststellung der UVP-Pflicht der Vorhaben, für die eine Vorprüfung gemäß § 3c UVPG durchzuführen ist, vor Beginn des Verfahrens auf Antrag des Vorhabensträgers (§ 3a UVPG)

  
100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

Im Falle der Durchführung einer Verträglichkeits- oder Ausnahmeprüfung nach § 34 BNatSchG bzw.
§ 26d BbgNatSchG zusätzlich

5 v.
H.
der Gebühr nach Tarifstelle 5.1.5.3, mindestens 82

 

Im Falle der Ausnahmeprüfung nach § 31 Absatz 2 und § 47 Absatz 3 in Verbindung mit § 31 Absatz 2 des Wasserhaushaltsgesetzes

bis 20 Prozent der Zulassungsgebühr

5.1.5.4

Planfeststellung oder Plangenehmigung des Baus eines Stauwerkes oder einer sonstigen Anlage zur Zurückhaltung oder dauerhaften Speicherung von Wasser (§ 129a Absatz 1 Nummer 2 BbgWG)

 

 

*   für die ersten 250 000 EUR Baukostenwert

  
0,5 Prozent mindestens 180

 

*   für die weiteren 750 000 EUR Baukostenwert

  
0,2 Prozent

 

*   für den 1 000 000 EUR übersteigenden Teil

  
0,1 Prozent

 

Zusatz für Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung:

 

 

1.  wird im Trägerverfahren eine Vorprüfung zur Feststellung der UVP-Pflicht vorgenommen und führt die Vorprüfung zur Ablehnung der UVP-Pflicht

  
100 bis 1 000

 

2.  Feststellung der UVP-Pflicht der Vorhaben, für die eine Vorprüfung gemäß § 3c UVPG durchzuführen ist, vor Beginn des Verfahrens auf Antrag des Vorhabensträgers (§ 3a UVPG)

  
100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

3.  Unterrichtung über voraussichtlich beizubringende Unterlagen bei UVP-pflichtigen Vorhaben auf Ersuchen des Vorhabensträgers vor Beginn des Verfahrens

100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

Im Falle der Durchführung einer Verträglichkeits- oder Ausnahmeprüfung nach § 34 BNatSchG bzw.
§ 26d BbgNatSchG zusätzlich

5 Prozent der Gebühr nach Tarifstelle 5.1.5.4, mindestens 128

 

Im Falle der Ausnahmeprüfung nach § 31 Absatz 2 und § 47 Absatz 3 in Verbindung mit § 31 Absatz 2 des Wasserhaushaltsgesetzes

bis 20 Prozent der Zulassungsgebühr

5.1.5.5

Planfeststellung oder Plangenehmigung zur Errichtung und zum Betrieb eines künstlichen Wasserspeichers (§ 129a Absatz 1 Nummer 13 BbgWG, § 20 UVPG i.
V.
m.
Nummer 19.9 der Anlage 1 UVPG)

Gebühr nach Tarifstelle 5.1.5.4

 

*   für die ersten 250 000 EUR Baukostenwert

  
0,3 Prozent, mindestens 180

 

*   für die weiteren 750 000 EUR Baukostenwert

  
0,1 Prozent

 

*   für den 1 000 000 EUR übersteigenden Teil

  
0,05 Prozent

 

Zusatz für Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung:

 

 

1.  wird im Trägerverfahren eine Vorprüfung zur Feststellung der UVP-Pflicht vorgenommen und führt die Vorprüfung zur Ablehnung der UVP-Pflicht

  
100 bis 1 000

 

2.  Feststellung der UVP-Pflicht der Vorhaben, für die eine Vorprüfung gemäß § 3c UVPG durchzuführen ist, vor Beginn des Verfahrens auf Antrag des Vorhabensträgers (§ 3a UVPG)

100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

Im Falle der Durchführung einer Verträglichkeits- oder Ausnahmeprüfung nach § 34 BNatSchG bzw.
§ 26d BbgNatSchG zusätzlich

5 Prozent der Gebühr nach Tarifstelle 5.1.5.5, mindestens 128

5.1.5.6

Planfeststellung, Plangenehmigung oder Genehmigung der Errichtung, des Betriebes und der Änderung von Rohrleitungsanlagen zum Befördern wassergefährdender Stoffe (§ 20 UVPG in Verbindung mit Nummer 19.3 der Anlage 1 UVPG)  

 

 

*   für die ersten 26 000 EUR Baukostenwert

  
1,5 Prozent

 

*   für die weiteren 26 000 EUR Baukostenwert

  
0,5 Prozent

 

*   für den 52 000 EUR übersteigenden Teil

  
0,2 Prozent

 

Zusatz für Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung:

 

 

1.  wird im Trägerverfahren eine Prüfung der Umweltverträglichkeit vorgenommen

Erhöhung der Gebühr um 10 Prozent

 

2.  wird im Trägerverfahren eine Vorprüfung zur Feststellung der UVP-Pflicht vorgenommen und führt die Vorprüfung zur Ablehnung der UVP-Pflicht

  
100 bis 1 000

 

3.  Feststellung der UVP-Pflicht der Vorhaben, für die eine Vorprüfung gemäß § 3c UVPG durchzuführen ist, vor Beginn des Verfahrens auf Antrag der Vorhabensträgers (§ 3a UVPG)

  
100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

4.  Unterrichtung über voraussichtlich beizubringende Unterlagen bei UVP-pflichtigen Vorhaben auf Ersuchen des Vorhabensträgers

 

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

Im Falle der Durchführung einer Verträglichkeits- oder Ausnahmeprüfung nach § 34 BNatSchG bzw.
§ 26d BbgNatSchG zusätzlich

5 Prozent der Gebühr nach Tarifstelle 5.1.5.6, mindestens 51

5.1.5.7

Planfeststellung, Plangenehmigung der Errichtung, des Betriebes oder der Änderung einer Wasserfernleitung (§ 20 UVPG in Verbindung mit Nummer 19.8 der Anlage 1 UVPG)

 

 

*   für die ersten 250 000 EUR Baukostenwert

  
0,2 Prozent, mindestens 153

 

*   für die weiteren 750 000 EUR Baukostenwert

  
0,1 Prozent

 

*   für den 1 000 000 EUR übersteigenden Teil

  
0,05 Prozent

 

Zusatz für Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung:

 

 

1.  wird im Trägerverfahren eine Prüfung der Umweltverträglichkeit vorgenommen

  
Erhöhung der Gebühr um 10 Prozent

 

2.  wird im Trägerverfahren eine Vorprüfung zur Feststellung der UVP-Pflicht vorgenommen und führt die Vorprüfung zur Ablehnung der UVP-Pflicht

  
100 bis 1 000

 

3.  Feststellung der UVP-Pflicht der Vorhaben, für die eine Vorprüfung gemäß § 3c UVPG durchzuführen ist, vor Beginn des Verfahrens auf Antrag des Vorhabensträgers (§ 3a UVPG)

  
100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

4.  Unterrichtung über voraussichtlich beizubringende Unterlagen bei UVP-pflichtigen Vorhaben auf Ersuchen des Vorhabensträgers vor Beginn des Verfahrens

  
100 bis 1 000

 

 

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

Im Falle der Durchführung einer Verträglichkeits- oder Ausnahmeprüfung nach § 34 BNatSchG bzw.
§ 26d BbgNatSchG zusätzlich

5 Prozent der Gebühr nach Tarifstelle 5.1.5.7, mindestens 51

5.1.6

Durchführung einer Bauabnahme (§ 106 Absatz 3 BbgWG)

41 bis 256

5.1.7

Amtshandlungen zum Umgang mit wassergefährdenden Stoffen

 

5.1.7.1

1.  Eignungsfeststellung (§ 63 Absatz 1 WHG)

102 bis 2 556

 

2.  Entscheidungen und Prüfungen nach § 41 Absatz 2 und  3 AwSV

51 bis 1 280

5.1.7.2

1.  Prüfung einer Anzeige zum Umgang mit wassergefährdenden Stoffen

 

 

> Anlage zum Lagern oder Abfüllen von Jauche, Gülle oder Silagesickersäften gemäß § 13 Absatz 3, Anlage 7 AwSV sowie sonstige Anlage zum Umgang mit allgemein wassergefährdenden Stoffen gemäß § 3 Absatz 2 AwSV

102

 

> sonstige Anlage gemäß § 40 AwSV nach dem Gefährdungspotenzial der Anlage (gemäß § 39 AwSV)

 

 

*   Gefährdungsstufe A

76,5

 

*   Gefährdungsstufe B

102

 

*   Gefährdungsstufe C

204,5

 

*   Gefährdungsstufe D

307

 

2.  Anordnungen und sonstige Entscheidungen nach AwSV außerhalb von Anzeigeverfahren (Feststellung gemäß § 1 Absatz 4 AwSV; abweichende Einstufung gemäß § 9 Absatz 1 oder § 10 Absatz 4 AwSV, Anordnungen gemäß § 16 Absatz 1 und 2, § 68 Absatz 4 oder § 69 AwSV; Befreiungen gemäß § 16 Absatz 3, § 46 Absatz 4)

50 bis 100

 

3.  Befreiungen in Schutzgebieten gemäß § 49 Absatz 4, § 50 AwSV

wie Tarifstelle 5.1.7.2 Buchstabe a

5.1.7.3

Anordnungen und Entscheidungen nach Tarifstellen 5.1.7.1 und 5.1.7.2, die in nicht wasserrechtlichen Entscheidungen getroffen werden

90 Prozent der Tarifstelle 5.1.7.2

5.1.8

Entscheidungen zu Maßnahmen in Gewässerrandstreifen, Schutzgebieten, in oder an hochwasserrelevanten Flächen und Anlagen und in Planungsgebieten nach § 86 WHG

 

5.1.8.1

Befreiung vom Gewässerrandstreifen nach § 38 Absatz 5 WHG

25 bis 1 000

5.1.8.2

Anordnung in Wasserschutzgebieten (§ 52 Absatz 1 WHG), vorläufige Anordnung in Wasserschutzgebieten (§ 52 Absatz 1, 2 WHG) und Anordnung außerhalb von Wasserschutzgebieten (§ 52 Absatz 1, 3 WHG)

0 bis 1 000

5.1.8.3

Befreiung von besonderen Anforderungen in einem Wasserschutzgebiet (§ 52 Absatz 1 Satz 2 und 3 WHG), von vorläufigen Anordnungen in einem Wasserschutzgebiet (§ 52 Absatz 1 Satz 2 und 3 WHG i.
V.
m.
§ 52 Absatz 2 Satz 1 WHG), von Anordnungen außerhalb eines Wasserschutzgebietes (§ 52 Absatz 1 Satz 2, 3 WHG i.
V.
m.
§ 52 Absatz 3 WHG) oder Genehmigung oder Befreiung aufgrund einer Wasserschutzgebietsverordnung oder sonstigen nach BbgWG bestehenden Schutzgebietsverordnung

25 bis 1 050

5.1.8.4

Zulassung, Genehmigung und Maßnahme nach § 78 Absatz 2, 3 und 4 WHG in festgesetzten Überschwemmungsgebieten

50 bis 2 600

5.1.8.5

Zulassung von Ausnahmen von einer Veränderungssperre (§ 86 Absatz 4 WHG)

0,2 v.
H.
des Wertes der Maßnahme, mindestens 25

5.1.8.6

Ausnahmegenehmigung von Verboten auf Deichen und in Deichschutzstreifen (§ 98 Absatz 3 BbgWG)

25 bis 1 050

5.1.8.7

Anordnung zur Nutzung von Vorländern (§ 102 Absatz 2 Satz 2 BbgWG)

25 bis 1 000 

5.1.8.8

Festsetzung einer Ausgleichszahlung nach § 52 Absatz 5 WHG, § 16 BbgWG

0,55 v.
H.
des festgesetzten Betrages

5.1.9

Ausgleich von Rechten und Befugnissen (§ 34 BbgWG in Verbindung mit § 22 WHG)  
  
Anmerkung:  
Der Wert des Vorteils ist gemäß § 136 Nummer 1 BbgWG zu ermitteln.

0,5 Prozent des ermittelten Vorteils, mindestens 26

5.1.10

Erteilung von Zwangsrechten nach den §§ 116, 117 BbgWG

0,5 Prozent des Gegen-standswertes, mindestens 26

5.1.11

Festsetzung der Entschädigung bei Wassergefahr (§ 113 BbgWG)

0,5 Prozent der festgesetzten Entschädigung, mindestens 10

5.1.12 

Feststellung oder Übertragung der Unterhaltungspflicht

30 bis 600

5.1.13

Festsetzung des Kostenanteils oder -beitrages bei der Unterhaltung von Anlagen (§ 82 BbgWG), der Beseitigung von Hindernissen (§ 83 BbgWG), der Unterhaltung von Gewässern (§ 85 BbgWG), dem Ausbau oberirdischer Gewässer (§ 91 BbgWG)

26 bis 511

5.1.14

Festsetzung des Schadenersatzes oder der Entschädigung (§ 90 Absatz 2, § 97 Absatz 2 Satz 3 BbgWG und § 41 Absatz 4, § 52 Absatz 4, § 95, § 98 Absatz 2 WHG)

0,55 v.
H.
des festgesetzten Betrages

5.1.15

Festsetzung und Bezeichnung der Uferlinie (§ 8 BbgWG)

 

 

*   für die ersten 100 Meter, je Meter

  
1, mindestens 26

 

*   für jeden weiteren Meter

  
0,5

5.1.16

Setzen, Erneuern, Versetzen oder Berichtigen einer Staumarke (§ 50 BbgWG)

26 bis 511

5.1.17

Außerbetriebsetzen und Beseitigen von Benutzungsanlagen

 

 

1.  Genehmigung des Außerbetriebsetzens nach § 37 Absatz 1 BbgWG oder Anordnung nach § 86 Absatz 3 BbgWG

  
bis 50 Prozent der Gebühr für die Zulassung der Benutzung

 

2.  Entscheidung über Höhe der zu erbringenden Leistungen

  
100 bis 300

5.1.18

Zulassung des Befahrens nicht schiffbarer Gewässer (§ 43 Absatz 3 BbgWG)

26 bis 256

5.1.19

Befreiung von der Duldungspflicht als Anlieger (§ 49 BbgWG)

26 bis 256

5.1.20

Entscheidung über die Feststellung des Inhalts und Umfangs alter Rechte und alter Befugnisse (§ 21 i.
V. m.
§ 20 WHG, § 147 BbgWG)  
  
Anmerkung: Gebühr für die Eintragung ins Wasserbuch siehe Tarifstelle 5.1.34

20 Prozent der für die zulassende Amtshandlung festzusetzenden Gebühr, mindestens 51

5.1.21

Änderungen einer Bewilligung, Erlaubnis, Genehmigung oder Zulassung

 

 

1.  Umschreibung auf einen Rechtsnachfolger oder sonstigen Dritten

  
10 Prozent der für die zulassende Amtshandlung festzusetzenden Gebühr

 

2.  Verlängerung der Geltungsdauer einer Bewilligung, Erlaubnis, Genehmigung oder Zulassung

  
50 Prozent der für die zulassende Amtshandlung festzusetzenden Gebühr

 

3.  sonstige Änderung

  
Zeitgebühr

 

Im Falle der Ausnahmeprüfung nach § 31 Absatz 2 und § 47 Absatz 3 in Verbindung mit § 31 Absatz 2 des Wasserhaushaltsgesetzes

bis 20 Prozent der Zulassungsgebühr

 

4.  Entgegenahme und Prüfung der Anzeige nach § 30 BbgWG

  
30 bis 100

5.1.22

Nachträgliche Entscheidung über Auflagen oder Festsetzung einer Entschädigung (§ 14 Absatz 5 und Absatz 6 WHG)

0,5 Prozent des Wertes der nachteiligen Wirkungen bzw.
des Entschädigungsbetrages

5.1.23

Anerkennung von Sachverständigenorganisationen und Güte- und Überwachungsgemeinschaften nach der Rechtsverordnung gemäß § 62 Absatz 4 WHG (AwSV)

26 bis 2 556

5.1.24

Zulassung von Stellen zur Untersuchung von Rohwasser

256 bis 2 556

5.1.25

Zulassung der Untersuchung von Rohwasser durch das Unternehmen selbst (§ 62 Absatz 3 Satz 2 BbgWG)

102 bis 511

5.1.26

Übertragung der Pflicht zur Abwasserbeseitigung auf Antrag eines Nutzers (§ 66 Absatz 3 Satz 3 BbgWG)

102 bis 1 023

5.1.27

Befreiung eines Abwassereinleiters von der Pflicht zur qualifizierten Selbstüberwachung (§ 73 Absatz 1 Satz 2 BbgWG)

26 bis 51

5.1.28

Zulassung von Stellen zur Untersuchung von Abwasser

256 bis 2 556

5.1.29

Zulassung von Stellen zur Untersuchung der Gewässergüte von Grund- und Oberflächenwasser

256 bis 2 556

5.1.30

Einzelanordnungen der Wasserbehörden nach dem Brandenburgischen Wassergesetz und zur Durchführung dieses Gesetzes, des Wasserhaushaltsgesetzes und der danach ergangenen Verordnungen (außer im öffentlichen Interesse ergehende Duldungsanordnungen), sofern keine andere Tarifstelle gilt

10 bis 1 000

5.1.31

Durchführung der Überwachung von Abwassereinleitungen einschließlich Probeanalytik (§ 110 BbgWG)  
  
Anmerkung:  
Werden mit der Analyse der Proben Dritte beauftragt, sind deren Auslagen zu erstatten.

Zeitgebühr und nach Sachaufwand

5.1.32

Prüfung einer Anzeige von Erdaufschlüssen nach § 49 Absatz 1 WHG

26 bis 511

5.1.33

Prüfung einer Anzeige von Grundwasserentnahmen (§ 55 Absatz 3 BbgWG)

20 Prozent der Gebühr der Tarifstelle 5.1.1, mindestens 50

 

Für die Prüfung der signifikanten nachteiligen Veränderungen kann die Gebühr um bis zu 50 Prozent der Gebühr der Tarifstelle 5.1.1 erhöht werden.  
  
Zusatz für Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung:

 

 

1.  wird eine Vorprüfung zur Feststellung der UVP-Pflicht vorgenommen und führt die Prüfung zur Ablehnung der UVP-Pflicht

  
100 bis 1 000

 

2.  Feststellung der UVP-Pflicht für Vorhaben, für die eine Vorprüfung gemäß § 3c UVPG durchzuführen ist, vor Beginn des Verfahrens auf Antrag des Vorhabenträgers (§ 3a UVPG)

  
100 bis 1 000  
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

 

Im Falle der Ausnahmeprüfung nach § 31 Absatz 2 und § 47 Absatz 3 in Verbindung mit § 31 Absatz 2 des Wasserhaushaltsgesetzes

bis 20 Prozent der Zulassungsgebühr

5.1.34

Eintragung, Änderung oder Löschung im Wasserbuch nach § 1 Absatz 1 BbgWaBuV in Verbindung mit § 87 WHG im Zusammenhang mit der Erteilung, Änderung oder Aufhebung eines einzutragenden Rechts oder einer einzutragenden Befugnis, auch wenn die Entscheidung über das Recht oder die Befugnis in einem anderen Verfahren konzentriert wird, sowie die Eintragung alter Rechte und alter Befugnisse ins Wasserbuch im Zusammenhang mit der Anmeldung oder der Entscheidung über die Feststellung des Inhalts und Umfangs alter Rechte und alter Befugnisse (§ 21 i.
V.
m.
§ 20 WHG, § 147 BbgWG)

30 bis 100

**5.2**

**Amtshandlungen im Zusammenhang mit Indirekteinleitungen**

 

5.2.1

Genehmigungen einer Indirekteinleitung von Abwasser

Gebühr nach Tarifstelle 5.1.1

5.2.2

Prüfung einer Anzeige einer Indirekteinleitung

102

5.2.3

Änderung einer Genehmigung

Gebühr entsprechend der Tarifstelle 5.1.21

**5.3**

**Erteilung einer Bescheinigung gemäß § 7 der Sachenrechts-Durchführungsverordnung**

**51 bis 511**

 

 

 

**6**

**Teilnahme an Ringversuchen des Landesamtes für Umwelt, Gesundheit und Verbraucherschutz im Zusammenhang mit der Bestimmung als Untersuchungsstelle im Sinne des § 3 Absatz 2, 5 oder 6 AbfKlärV bzw.
im Sinne des § 3 Absatz 8, § 4 Absatz 9 und § 9 Absatz 2 BioAbfV oder im Zusammenhang mit dem Vollzug anderer umweltrechtlicher Vorschriften**

36 je Untersuchungsparameter und zu untersuchender Probe, mindestens 215  
 

**6.1**

**Grundgebühr für die Teilnahme an den Ringversuchen**

**100 bis 200**

**6.2**

**Probengebühr je Anzahl der im Ringversuch bearbeiteten Proben**

**30 bis 100**

**6.3**

**Parametergruppengebühr je Anzahl der von den teilnehmenden Laboratorien zu untersuchenden Parametergruppen**

**50**

 

 

 

**7 bis 7.13**

_**außer Kraft getreten**_

 

 

 

 

**8**

**_Nicht besetzt_**

 

 

 

 

**9**

**Veterinärwesen, Lebens- und Futtermittelüberwachung sowie Wasserhygiene**

 

**9.1**

**Gebühren in Bezug auf das Berufs- und Standesrecht**

 

9.1.1

Approbation

 

9.1.1.1

Erteilung der Approbation für Tierärzte nach den §§ 4 und 15a der Bundes-Tierärzteordnung

102 bis 256

9.1.1.2

Anordnung des Ruhens der Approbation nach § 8 Absatz 1 der Bundes-Tierärzteordnung

51 bis 102

9.1.1.3

Aufhebung der Anordnung des Ruhens der Approbation nach § 8 Absatz 2 der Bundes-Tierärzteordnung

102

9.1.2

Berufserlaubnis

 

9.1.2.1

Erteilung der Berufserlaubnis für Tierärzte nach § 11 Absatz 1 der Bundes-Tierärzteordnung

102

9.1.2.2

Verlängerung der Berufserlaubnis für Tierärzte nach § 11 Absatz 2 und 3 der Bundes-Tierärzteordnung

51

9.1.2.3

Bescheinigung nach § 11a Absatz 4 der Bundes-Tierärzteordnung

25,5

9.1.2.4

Befähigungszeugnis für den tierärztlichen Staatsdienst nach § 17 Absatz 1 der Amtstierärzteprüfungsverordnung

102

9.1.2.5

Abnahme der Prüfung für den tierärztlichen Dienst in der Veterinärverwaltung

153,5

9.1.2.6

Anerkennung der Gleichwertigkeit eines außerhalb des Landes Brandenburg erworbenen Befähigungszeugnisses für den tierärztlichen Staatsdienst

76,5

9.1.2.7

_Nicht besetzt_

 

9.1.2.8

Amthandlungen nach dem Gesetz zum Schutz der Berufsbezeichnung „Staatlich geprüfte Lebensmittelchemikerin“ und „Staatlich geprüfter Lebensmittelchemiker“(LMChemG)

 

9.1.2.8.1

Erteilung der Erlaubnis zum Führen der Berufsbezeichnung „Staatlich geprüfte Lebensmittelchemikerin“ und „Staatlich geprüfter Lebensmittelchemiker“ (§ 2 Absatz 1 LMChemG)

30

9.1.2.8.2

Widerruf der Erlaubnis

50 bis 80

9.1.2.8.3

Erteilung der Erlaubnis in den in § 7 LMChemG genannten Fällen

80 bis 100

9.1.2.9

Bescheinigung über eine abgeschlossene Ausbildung, Fortbildung und bestandene Prüfung

25,5

9.1.2.10

Anerkennung als Hufbeschlagschmied/Hufbeschlagschmiedin nach § 1 Absatz 1 HufBeschIV

150

9.1.2.11

Anerkennung als Hufbeschlaglehrschmied/Hufbeschlaglehrschmiedin nach § 2 Absatz 1 HufBeschIV

150

9.1.2.12

Anerkennung als staatlich anerkannte Hufbeschlagschule nach § 3 HufBeschIV

500

9.1.2.13

Anerkennung eines Einführungslehrganges nach § 6 Absatz 4 HufBeschIV

150

9.1.2.14

Zulassung zur und Abnahme der Prüfung

 

 

*   als Hufbeschlagschmied/Hufbeschlagschmiedin einschließlich Ausstellung des Prüfungszeugnisses nach § 5 Absatz 5 und 8 in Verbindung mit § 12 Absatz 1 HufBeschIV

  
200

 

*   als Hufbeschlaglehrschmied/Hufbeschlaglehrschmiedin einschließlich Ausstellung des Prüfungszeugnisses nach § 17 Absatz 5 in Verbindung mit  § 19 Absatz 2

  
450

 

Anmerkung zu Tarifstelle 9.1.2.14:  
  
Mit Beginn der Prüfung ist unabhängig von deren weiterem Verlauf die Gesamtgebühr für die Prüfung zu begleichen.

 

9.1.2.15

Wiederholungsprüfungen

100 bis 450

9.1.2.16

Anerkennung der Gleichstellung oder Feststellung der Gleichwertigkeit des Abschlusses als Hufbeschlagschmied/Hufbeschlagschmiedin einschließlich Ausstellung einer Anerkennungsurkunde nach § 3 HufBeschlAnerkennV

50

 

Anerkennung der Gleichstellung oder Feststellung der Gleichwertigkeit des Abschlusses als Hufbeschlaglehrschmied/Hufbeschlaglehrschmiedin einschließlich Ausstellung einer Anerkennungsurkunde nach § 4 HufBeschlAnerkennV

50

9.1.3

Ausstellung einer Ersatzurkunde

51

**9.2**

**Gebühren für Beratungstätigkeit und die Erstellung von Gutachten**

 

9.2.1

einfache Bescheinigung, einfache Befundung, einfache schriftliche Erläuterung

4 bis 20

9.2.2

Beratungstätigkeit ohne Untersuchung

20 bis 128

9.2.3

Gutachten, Untersuchungsbericht, je angefangene Seite

25, mindestens jedoch 50

9.2.4

umfangreiche wissenschaftliche Gutachten

51 bis 358

**9.3**

**Gebühren auf Grund des Tiererzeugnisse-Handels-Verbotsgesetzes und der zu diesem Gesetz erlassenen bundes- oder landesrechtlichen Vorschriften**

 

9.3.1

Amtshandlungen nach § 2 Absatz 1 Satz 1 und 2 des Tiererzeugnisse-Handels-Verbotsgesetzes

nach Zeitaufwand

9.3.1.1

Gebühren anlässlich der Beschlagnahmung eines Hunde- oder Katzenfells oder eines Produkts, das solche Felle enthält, oder eines Robbenerzeugnisses (Aufbewahrung einschließlich der Kosten für die Be- und Entladung)

pro Tag und je kg Ware

Die Gebühr verdoppelt sich an Sonnabenden, Sonn- und Feiertagen.

10 bis 26

**9.4**

**_Nicht besetzt_**

 

**9.5**

**Gebühren auf Grund des Tierseuchengesetzes und der zu diesem Gesetz erlassenen bundes- oder landesrechtlichen Vorschriften**

 

9.5.1

Zulassung von wissenschaftlichen Versuchen außerhalb wissenschaftlicher Institute nach § 17c Absatz 4 des Tierseuchengesetzes

26 bis 153

9.5.2

Erlaubnis zum Verkehr mit Tierseuchenerregern nach § 17 Absatz 1 Nummer 16 des Tierseuchengesetzes

77 bis 511

9.5.3

Änderung oder Erweiterung der Erlaubnis zum Verkehr mit Tierseuchenerregern nach § 17 Absatz 1 Nummer 16 des Tierseuchengesetzes; Kontrolle bzw.
Überwachung der zugelassenen Betriebe/Einrichtungen durch die Zulassungsbehörde

30 bis 200

9.5.4

Erteilung einer Ausnahmegenehmigung nach § 17c des Tierseuchengesetzes

51 bis 511

9.5.5

Änderung oder Erweiterung der Genehmigung nach § 17c des Tierseuchengesetzes

30 bis 200

9.5.6

Erlaubnis zur Herstellung von Sera, Impfstoffen und Antigenen nach § 17d des Tierseuchengesetzes

150 bis 5 000

9.5.7

Änderung oder Erweiterung der Genehmigung nach § 17d des Tierseuchengesetzes

50 bis 500

9.5.8

Untersuchung von Tieren, tierischen Teilen und Erzeugnissen

 

9.5.8.1

Einhufer, Rinder und Großwild

13 bis 128

 

Einzelgebühr

2

9.5.8.2

Kälber, Schweine über 25 kg, Schafe

13 bis 102

 

Einzelgebühr

2

9.5.8.3

Schweine unter 25 kg, Ziegen, Edelpelztiere, Kaninchen, Affen, Halbaffen, ICP-MS, jedes weitere Wild quantitativ vergleichbarer Größe, andere Kleintiere

5 bis 77

 

Einzelgebühr

0,5

9.5.8.4

Hunde, Hauskatzen

5 bis 51

 

Einzelgebühr

2

9.5.8.5

Ziervögel, die keine Psittaciden sind

5 bis 51

 

Einzelgebühr

1,5

9.5.8.6

Psittaciden

10 bis 77

 

Einzelgebühr

2,5

9.5.8.7

Reisebrieftauben  
  
bis 99 Tiere  
ab 100 Tiere

  
  
  
8 bis 18  
20

9.5.8.8

sonstiges Geflügel  
  
bis 99 Tiere  
ab 100 Tiere

  
  
  
8 bis 77  
79

9.5.8.9

Wanderschafherden  
  
bis 199 Tiere  
ab 200 Tiere

  
  
  
10 bis 20  
22

9.5.8.10

Wanderbienenvölker gemäß § 5 der Bienenseuchenverordnung

5 bis 51

9.5.8.11

Zierfische, Fische, je Haltungseinheit

13 bis 51

 

Einzelgebühr

5

9.5.8.12

tierische Teile oder Erzeugnisse, soweit keine Lebensmittel, je Sendung

2,50 bis 100

9.5.8.13

Ausstellen einer Bescheinigung über das Ergebnis der nach den Tarifstellen 9.5.8.1 bis 9.5.8.12 vorgenommenen Amtshandlung

1 bis 51

9.5.9

amtstierärztliche Bestätigung der Identität eines Tieres

2,5

9.5.10

Kennzeichnung von Tieren durch Ohrmarken oder Tätowierungen, je Kennzeichnung

2,5

9.5.11

Beaufsichtigung von Betrieben, Einrichtungen und Veranstaltungen nach dem Tierseuchengesetz

 

9.5.11.1

Viehmärkte, Absatzveranstaltungen

20 bis 300

9.5.11.2

Tierschauen, Tierversteigerungen, Sportveranstaltungen mit Tieren, Tierausstellungen

20 bis 300

9.5.11.3

öffentliche Schlachthöfe, gewerbliche Schlachthäuser, Geflügelschlächtereien, Molkereien, Besamungsstationen, gewerbliche Mästereien, Embryo-Transfereinrichtungen, Massentierhaltungen, Zuchttierhaltungen, Zoologische Gärten, Zoologische Handlungen, Quarantäneeinrichtungen, Anlagen zur Futtermittelherstellung

20 bis 300

9.5.11.4

Betriebe und Einrichtungen, die Sera, Impfstoffe oder Antigene herstellen nach § 17c des Tierseuchengesetzes

150 bis 5 000

9.5.11.5

Prüfung der Sachkunde von Züchtern und Händlern für Psittaciden nach § 17g des Tierseuchengesetzes

13 bis 77

9.5.11.6

Prüfung der räumlichen Voraussetzungen für die Zucht bzw.
Haltung oder Handel von Psittaciden nach § 17g des Tierseuchengesetzes

13 bis 77

9.5.11.7

Bescheinigung über die Seuchenfreiheit, Unbedenklichkeit oder Desinfektion, insbesondere von Beständen, Herkunftsgebieten, Gegenständen, Fahrzeugen, Packmaterial ohne Untersuchung

5 bis 26

9.5.11.8

Untersuchung eines Tieres zur Genehmigung der Einsperrung sowie für jede weitere Untersuchung während der Beobachtungszeit im Rahmen der Tollwutbekämpfung

5 bis 15

 

Einzelgebühr

5

9.5.11.9

Untersuchung von Pferden bei Beschälseuchengefahr zwecks Zulassung zur Begattung oder zur Ausfuhr aus Beobachtungsgebieten, je Pferd

5 bis 15

 

Einzelgebühr

5

9.5.11.10

Untersuchung von Tieren, die zur Impfstoffgewinnung gedient haben, zur Veräußerung oder anderweitigen Verwendung, je Tier

7,5

9.5.12

_Nicht besetzt_

 

9.5.13

_Nicht besetzt_

 

9.5.14

Genehmigungen für Einfuhr, Durchfuhr und das Verbringen von lebenden Tieren, tierischen Rohstoffen, tierischen Erzeugnissen nach tierseuchenrechtlichen Vorschriften

bei Ausfuhren  
nach Zeitaufwand

9.5.14.1

Lebende Tiere

 

9.5.14.1.1

Rinder, Einhufer und andere Großtiere bis zu 100 Tieren, je Tier

 

 

Einzelgebühr  
weitere Tiere, je Tier  
Mindestgebühr  
Höchstgebühr

0,7  
0,35  
10  
256

9.5.14.1.2

Schweine, Wildschweine, Kälber bis zu 100 Tieren, je Tier

 

 

Einzelgebühr  
weitere Tiere, je Tier  
Mindestgebühr  
Höchstgebühr

0,35  
0,2  
10  
256

9.5.14.1.3

Schafe, Ziegen, Rehe, Muffelwild, Ferkel bis zu 200 Tieren, je Tier

 

 

Einzelgebühr  
weitere Tiere, je Tier  
Mindestgebühr  
Höchstgebühr

0,2  
0,08  
10  
256

9.5.14.1.4

Hunde und Hauskatzen, je Tier

 

 

Einzelgebühr  
Mindestgebühr  
Höchstgebühr

0,5  
10  
153

9.5.14.1.5

Affen, Halbaffen, je Tier

 

 

Einzelgebühr  
Mindestgebühr  
Höchstgebühr

1  
10  
153

9.5.14.1.6

Hasen, Kaninchen, Frettchen, Füchse und Nerze, je Tier

 

 

Einzelgebühr  
Mindestgebühr  
Höchstgebühr

0,25  
10  
153

9.5.14.1.7

Geflügel

 

9.5.14.1.7.1

Haus- und Wildgeflügel bis zu 1 000 Tieren, je Tier

 

 

Einzelgebühr  
weitere Tiere, je Tier  
Mindestgebühr  
Höchstgebühr

0,02  
0,01  
10  
179

9.5.14.1.7.2

Eintagsküken bis zu 1 000 Tieren, je Tier

 

 

Einzelgebühr  
weitere Tiere, je Tier  
Mindestgebühr  
Höchstgebühr

0,02  
0,01  
10  
205

9.5.14.1.8

Psittaciden

 

9.5.14.1.8.1

Wellensittiche und sonstige Kleinsittiche, je Tier

 

 

Einzelgebühr  
Mindestgebühr  
Höchstgebühr

0,08  
10  
153

9.5.14.1.8.2

Papageien und andere Groß-Psittaciden, je Tier

 

 

Einzelgebühr  
Mindestgebühr  
Höchstgebühr

0,15  
10  
153

9.5.14.1.9

sonstige Vögel, je Tier

 

 

Einzelgebühr  
Mindestgebühr  
Höchstgebühr

0,15  
10  
153

9.5.14.1.10

Bienen

 

9.5.14.1.10.1

Bienenköniginnen mit Volk, je Volk

 

 

Einzelgebühr  
Mindestgebühr  
Höchstgebühr

0,7  
10  
51

9.5.14.1.10.2

Bienenköniginnen mit Begleitbienen, je 10 Bienenköniginnen

 

 

Einzelgebühr  
Mindestgebühr  
Höchstgebühr

0,7  
10  
51

9.5.14.1.11

Fische, je Tonne

 

 

Einzelgebühr  
Mindestgebühr  
Höchstgebühr

10  
10  
153

9.5.14.2

Waren von geschlachteten und erlegten Tieren 

 

9.5.14.2.1

Fleisch für den menschlichen Verzehr, je kg

 

 

Einzelgebühr  
Mindestgebühr  
Höchstgebühr

0,01  
10  
205

9.5.14.2.2

tierische Teile zur Herstellung von Tiernahrung, je kg

 

 

Einzelgebühr  
Mindestgebühr  
Höchstgebühr

0,01  
10  
205

9.5.14.2.3

tierische Teile für pharmazeutische oder technische Zwecke, je kg

 

 

Einzelgebühr  
Mindestgebühr  
Höchstgebühr

0,01  
10  
205

9.5.14.3

Bruteier, je 100 Stück

 

 

Einzelgebühr  
Mindestgebühr  
Höchstgebühr

0,15  
10  
153

9.5.14.4

Sperma, Embryonen, Eizellen, je 100 Portionen/Stück

 

 

Einzelgebühr  
Mindestgebühr  
Höchstgebühr

10  
10  
153

9.5.14.5

Sera, Impfstoffe, Tierseuchenerreger, sonstige Stoffe

10 bis 77

9.5.14.6

sonstige Ein- und Durchfuhrgenehmigungen sowie Genehmigungen für das innergemeinschaftliche Verbringen

10 bis 256

9.5.15

Zulassung eines Betriebes zum innergemeinschaftlichen Handelsverkehr

 

9.5.15.1

_Nicht besetzt_

 

9.5.15.2

nach den §§ 13, 13a, 15, 35 und 36a der Binnenmarkt-Tierseuchenschutzverordnung

1 200 bis 2 200

9.5.15.3

nach § 10 der Fischseuchen-Verordnung

51 bis 205

9.5.15.4

Änderung der Zulassung eines Betriebes zum innergemeinschaftlichen Handelsverkehr gemäß den Tarifstellen 9.5.15.1, 9.5.15.2 und 9.5.15.3; Kontrolle bzw.
Überwachung der zugelassenen Betriebe/Einrichtungen durch die Zulassungsbehörde

30 bis 200  
 

9.5.16

Probennahme, Impfung, allergischer Test

2,50 bis 5

9.5.17

Ausgabe von Ohrmarken und Tierpässen, je Stück

2,50 bis 3

9.5.18

Zulassung eines Betriebes nach den §§ 12, 13 oder 14 der Viehverkehrsverordnung

51 bis 205

9.5.19

Kennzeichnung eines Pferdes nach § 44 der Viehverkehrsverordnung

80

9.5.20

Genehmigung einer Freilandhaltung gemäß § 4 Absatz 3 der Schweinehaltungshygieneverordnung (SchHaltHygV)

25 bis 100

9.5.21

Erteilung einer Ausnahmegenehmigung im Rahmen von Eilverordnungen

20 bis 30

9.5.22

Nachkontrollen bei Beanstandungen gemäß § 73 des Tierseuchengesetzes

25 bis 200

9.5.23

Abhilfemaßnahmen zur Beseitigung eines festgestellten Verstoßes (im Sinne des Artikels 54 der Verordnung (EG) Nr. 882/2004)

50 bis 5 000

9.5.24

Bestätigung einer Anzeige gemäß § 7 Absatz 3 der Geflügelpest-Verordnung einschließlich Kontrolle des Bestandes

26 bis 75

**9.6**

**Gebühren für Amtshandlungen nach der Verordnung (EG) Nr. 1069/2009 in der jeweils geltenden Fassung sowie nach den auf Grund dieser Verordnung oder zu ihrer Durchführung ergangenen Rechtsakten der Europäischen Union, des Bundes und des Landes Brandenburg**

 

9.6.1

Verordnung (EG) Nr. 1069/2009 des Europäischen Parlaments und des Rates mit Hygienevorschriften für nicht für den menschlichen Verzehr bestimmte tierische Nebenprodukte

 

9.6.1.1

Entscheidung über die Zulassung von Anlagen oder Betrieben

 

9.6.1.1.1

Zulassung von Anlagen oder Betrieben nach Artikel 24 in Verbindung mit Artikel 44

100 bis 2 000

9.6.1.1.2

Erteilung einer vorläufigen oder bedingten Zulassung nach Artikel 44 Absatz 2

50 bis 1 000

9.6.1.1.3

Verlängerung einer bedingten Zulassung nach Artikel 44 Absatz 2

25 bis 500

9.6.1.1.4

Widerruf oder Ruhenlassen einer Zulassung nach Artikel 46 Absatz 1

25 bis 500

9.6.1.2

Entgegennahme der Anzeige zur Registrierung von Anlagen oder Betrieben nach Artikel 23 einschließlich Überprüfung des Betriebes

50 bis 1 000

9.6.1.3

Untersagung der Ausübung einer zugelassenen oder registrierten Tätigkeit nach Artikel 46 Absatz 2

50 bis 1 000

9.6.1.4

Beurteilung von Anträgen zur Genehmigung alternativer Verfahren nach Artikel 20 Absatz 2

nach Zeitaufwand

9.6.1.5

Entscheidung über Ausnahmen von den Bestimmungen der Verordnung (EG) Nr.
1069/2009

 

9.6.1.5.1

Erteilung von Genehmigungen nach Artikel 16 Buchstabe f bis h

20 bis 100

9.6.1.5.2

Zulassung von Ausnahmen bezüglich der Verwendung tierischer Nebenprodukte nach den Artikeln 17 und 18

20 bis 100

9.6.1.5.3

Zulassung von Ausnahmen bezüglich der Beseitigung tierischer Nebenprodukte nach Artikel 19

20 bis 100

9.6.2

Tierisches Nebenprodukte-Beseitigungsgesetz (TierNebG) und Tierische Nebenprodukte-Beseitigungs-Verordnung (TierNebV)

 

9.6.2.1

Erteilung von Genehmigungen über Ausnahmen von der Überlassungspflicht nach § 4 TierNebG

20 bis 100

9.6.2.2

Übertragung der Beseitigungspflicht nach § 3 Absatz 2 TierNebG

100 bis 500

9.6.2.3

Zulassung einer Anlage zur Pasteurisierung nach § 11 Absatz 1 Satz 1 TierNebV

100 bis 1 000

9.6.2.4

Registrierung einer Biogasanlage nach § 13 Absatz 1 Satz 2  
TierNebV oder einer Kompostierungsanlage nach § 17 Absatz 1 Satz 2 TierNebV

50 bis 500

9.6.2.5

Zulassung von Plätzen, an denen Heimtiere vergraben werden können (Tierfriedhöfe), nach § 27 Absatz 3 Satz 1

50 bis 500

9.6.2.6

Genehmigung von Ausnahmen nach § 27 Absatz 1 TierNebV

20 bis 50

9.6.3

Gesetz zur Ausführung des Tierische Nebenproduktebeseitigungsgesetzes des Landes Brandenburg

 

9.6.3.1

Entscheidung über die Genehmigung von Entgeltlisten

nach Zeitaufwand

9.6.4

Überwachungsmaßnahmen

nach Zeitaufwand

**9.7**

**Gebühren auf Grund des Tierschutzgesetzes und der zu diesem Gesetz erlassenen bundes- oder landesrechtlichen Vorschriften**

 

9.7.1

Erteilung einer Ausnahmegenehmigung für ein betäubungsloses Schlachten (Schächten) nach § 4a Absatz 2 Nummer 2 des Tierschutzgesetzes

 

 

gewerblicher Antragsteller  
nicht gewerblicher Antragsteller für das erste Tier gebührenfrei, für jedes weitere Tier

50 bis 200  
10

9.7.2

rteilung einer Ausnahmegenehmigung für die Betäubung warmblütiger Tiere durch Nichttierärzte nach § 5 Absatz 1 Satz 3 des Tierschutzgesetzes

25,5

9.7.3

Erteilung einer Erlaubnis nach § 6 Absatz 3 des Tierschutzgesetzes

50 bis 100

9.7.4

Erteilung einer Genehmigung zur Durchführung eines Tierversuchsvorhabens nach § 8 Absatz 1 des Tierschutzgesetzes

50 bis 300

 

*   soweit dieses im öffentlichen Interesse liegt 

  
gebührenfrei

9.7.5

Erteilung einer Ausnahmegenehmigung für die Bestellung zum Tierschutzbeauftragten nach § 8b Absatz 2 Satz 3 des Tierschutzgesetzes

25,5

9.7.6

Erteilung einer Ausnahmegenehmigung zur Durchführung von Tierversuchen nach § 9 Absatz 1 Satz 4 des Tierschutzgesetzes

25,5

9.7.7

Erteilung einer Ausnahmegenehmigung zur Verwendung von nicht speziell für Tierversuche gezüchteten Tieren nach § 9 Absatz 2 Nummer 7 Satz 2 des Tierschutzgesetzes

102

9.7.8

Erlaubnis nach § 11 Absatz 1 des Tierschutzgesetzes

26 bis 102

 

Prüfung der Sachkunde nach § 11 Absatz 2 Nummer 1 und Absatz 2a des Tierschutzgesetzes

25 bis 200

 

Prüfung der räumlichen Voraussetzungen für die Zucht und Haltung von Tieren nach § 11 Absatz 2 Nummer 3 des Tierschutzgesetzes

25 bis 50

9.7.9

Erteilung einer Genehmigung zur Einfuhr von Wirbeltieren als Versuchstiere nach § 11a Absatz 4 des Tierschutzgesetzes

25,5

9.7.10

Abnahme der Prüfung der Sachkunde nach § 4 Absatz 4 der Tierschutz-Schlachtverordnung oder § 17 Absatz 3 der Tierschutz-Nutztierhaltungsverordnung

50

9.7.11

Ausstellung einer Sachkundebescheinigung nach § 4 Absatz 3 der Tierschutz-Schlachtverordnung oder § 17 Absatz 2 der Tierschutz-Nutztierhaltungsverordnung

25,5

9.7.12

Befristete Zulassung von Betäubungs- und Tötungsverfahren nach § 14 Nummer 1 und 3 der Tierschutz-Schlachtverordnung

50 bis 100

9.7.13

Kontrolle eines Tiertransportes gemäß Verordnung (EG) Nr. 615/98 bzw.
(EG) Nr. 639/2003

26 bis 51

9.7.14

Ausnahmegenehmigung zur künstlichen Beleuchtung nach § 13 Absatz 3 Satz 3 der Tierschutz-Nutztierhaltungsverordnung

25 bis 50

9.7.15

Erlaubnis zur Einschränkung der Zugangsöffnung nach § 13 Absatz 3 Satz 3 der Tierschutz-Nutztierhaltungsverordnung

25 bis 50

9.7.16 

Ausnahmegenehmigung zur Erprobung neuer Haltungseinrichtungen nach § 15 der Tierschutz-Nutztierhalteverordnung 

25 bis 100

9.7.17

Nachkontrollen bei Beanstandungen oder Kontrollen aus besonderem Anlass gemäß § 16 des Tierschutzgesetzes (z.
B.
Ausnahmegenehmigungen)

25 bis 200

 

*   Überwachung der tierschutzrechtlichen Anforderungen beim betäubungslosen Schlachten 

  
nach Zeitaufwand 

9.7.18

Amtshandlungen auf Grund der Verordnung (EG) Nr. 1/2005 über den Schutz von Tieren beim Transport

 

9.7.18.1

Prüfung der Transportpapiere im Rahmen des Artikels 4 Absatz 2 der Verordnung (EG) Nr. 1/2005

nach Zeitaufwand

9.7.18.2

Entscheidung über einen Antrag auf Zulassung als Tiertransportunternehmen nach Artikel 10 Absatz 1 der Verordnung (EG) Nr. 1/2005

20 bis 500

9.7.18.3

Entscheidung über einen Antrag auf Zulassung als Tiertransportunternehmen nach Artikel 11 Absatz 1 der Verordnung (EG) Nr. 1/2005

20 bis 500

9.7.18.4

Durchführung von Kontrollen vor langen Beförderungen nach Artikel 14 Absatz 1 der Verordnung (EG) Nr. 1/2005

nach Zeitaufwand

9.7.18.5

Entscheidung über einen Antrag auf Zulassung und Registrierung eines Straßentransportmittels nach Artikel 18 Absatz 1 und Absatz 3 der Verordnung (EG) Nr. 1/2005

25 bis 100

9.7.18.6

Entscheidung über einen Antrag auf Zulassung und Registrierung eines Tiertransportschiffes nach Artikel 19 Absatz 1 und Absatz 4 der Verordnung (EG) Nr. 1/2005

25 bis 100

9.7.18.7

Änderungen, Ergänzungen usw.
für Betriebe, die unter die Amtshandlungen nach den Tarifstellen 9.7.18.2, 9.7.18.3, 9.7.18.5 und 9.7.18.6 fallen

10 bis 100

9.7.18.8

Ausstellen eines Befähigungsnachweises nach Artikel 17 Absatz 2 der Verordnung (EG) Nr. 1/2005 oder nach § 4 Absatz 1 und 2 der Tierschutztransportverordnung

25

9.7.18.9

Abnahme der theoretischen und praktischen Sachkundeprüfung anlässlich des Ausstellens eines Befähigungsnachweises nach Artikel 17 Absatz 2 der Verordnung (EG) Nr. 1/2005

50 bis 200

9.7.18.10 

Entscheidung über den Entzug eines Befähigungsnachweises

25

9.7.19

Amtshandlungen auf dem Gebiet des Tierschutzes nach der Verordnung (EG) Nr. 882/2004

 

9.7.19.1

Durchführung zusätzlicher amtlicher Kontrollen im Sinne des Artikels 28 Satz 1 und 3 der Verordnung (EG) Nr. 882/2004

25 bis 1 000

9.7.19.2

Abhilfemaßnahmen zur Beseitigung eines festgestellten Verstoßes im Sinne des Artikels 54 der Verordnung (EG) Nr. 882/2004

50 bis 5 000

9.7.20

Zulassung von Abweichungen zur Installation und Instandhaltung der Tränkvorrichtungen nach § 18 Absatz 1 Satz 2 der Tierschutz-Nutztierhaltungsverordnung

25 bis 50

9.7.21

Zulassung von Abweichungen zur Installation und Instandhaltung der Fütterungseinrichtungen nach § 18 Absatz 2 Satz 2 der Tierschutz-Nutztierhaltungsverordnung

25 bis 50

**9.8**

**Gebühren auf Grund des Arzneimittelgesetzes und der zu diesem Gesetz erlassenen bundes- oder landesrechtlichen Vorschriften im Bereich der Tierarzneimittel**

 

9.8.1

Tierärztliche Hausapotheken

 

9.8.1.1

Bescheinigung nach § 47 Absatz 1a in Verbindung mit § 67 des Arzneimittelgesetzes

50 bis 100

9.8.1.2

Nachkontrolle oder Kontrolle aus besonderem Anlass einer tierärztlichen Hausapotheke nach § 64 Absatz 1 und 3 des Arzneimittelgesetzes

300 bis 1 000

9.8.1.3

Genehmigung einer Untereinheit nach § 3 Absatz 4 Nummer 2 TÄHAV

150 bis 1 000

9.8.2

Pharmazeutische Unternehmen

 

9.8.2.1

Erlaubnis zur Herstellung nach § 13 Absatz 1 des Arzneimittelgesetzes

300 bis 10 000

9.8.2.2

Änderung der Erlaubnis zur Herstellung nach § 13 Absatz 1 des Arzneimittelgesetzes in Verbindung mit § 17 Absatz 2 des Arzneimittelgesetzes

200 bis 5 000

9.8.2.2.1

Abnahmebesichtigung/Nachbesichtigung zu überwachender Betriebe oder Einrichtungen nach § 64 AMG  
  
(außer Besichtigung von öffentlichen Apotheken und Großhändlern mit Arzneimitteln)

400 bis 9 000

9.8.2.2.2

Ausstellung des GMP-Zertifikates nach § 64 Absatz 3 AMG

100 bis 200

9.8.2.3

Erstellung eines Inspektionsberichtes lt.
Anlage (PIC Dokument PH 6/91) zur „Bekanntmachung einer Anleitung für die Erstellung von Informationen gemäß Artikel 2 der Pharmazeutischen Inspektions-Convention (PIC)“ vom 6. Januar 1992 (Bundesanzeiger Nr. 18 vom 28. Dezember 1992, S. 468) unter Berücksichtigung des PIC-Dokumentes PH 8/92

500 bis 1 100

9.8.2.4

Betriebsbesichtigung eines pharmazeutischen Unternehmers zum Zweck der Ausstellung eines Zertifikats über Arzneimittelherstellung, die den GMP-Richtlinien entsprechen

500 bis 9 000

9.8.2.5

Kontrolle, Nachkontrolle oder Kontrolle aus besonderem Anlass eines pharmazeutischen Unternehmers oder von Betriebsteilen nicht im Land Brandenburg ansässiger pharmazeutischer Unternehmer nach § 64 des Arzneimittelgesetzes (Kontrollen im Rahmen von Amtshilfe)

500 bis 9 000

9.8.2.6

Zulassung einer Ausnahme nach § 60 Absatz 4 des Arzneimittelgesetzes

100 bis 500

9.8.3

Sonstige Betriebe, die Arzneimittel herstellen, prüfen, lagern, verpacken oder in den Verkehr bringen

 

9.8.3.1

Erlaubnis für das Betreiben eines Großhandels mit Arzneimitteln gemäß § 52a Absatz 1 des Arzneimittelgesetzes

300 bis 2 500

9.8.3.1.1

Änderung der Erlaubnis zum Großhandel in Verbindung mit § 17 Absatz 2 des Arzneimittelgesetzes ohne Inspektion

100 bis 200

9.8.3.1.2

Änderung der Erlaubnis zum Großhandel in Verbindung mit § 17 Absatz 2 des Arzneimittelgesetzes mit Inspektion

400 bis 9 000

9.8.3.2

Nachkontrolle oder Kontrolle aus besonderem Anlass von Arzneimittelgroßhändlern nach § 64 Absatz 1 und 3 des Arzneimittelgesetzes

250 bis 2 000

9.8.3.3

Nachkontrolle oder Kontrolle aus besonderem Anlass nach § 64 des Arzneimittelgesetzes von Betrieben, die Stoffe nach § 59c des Arzneimittelgesetzes beziehen, lagern oder abgeben

300 bis 1 500

9.8.3.4

Nachkontrolle oder Kontrolle aus besonderem Anlass des Einzelhandels mit freiverkäuflichen Arzneimitteln nach § 64 Absatz 3 des Arzneimittelgesetzes

25 bis 600

9.8.4

**_aufgehoben_**

 

9.8.5

Erteilung einer Ein-/Ausfuhrerlaubnis

 

9.8.5.1

Erteilung einer Einfuhrerlaubnis nach § 72 des Arzneimittelgesetzes

150 bis 8 000

9.8.5.2

Änderung der Erlaubnis nach Tarifstelle 9.8.5.1 in Verbindung mit § 17 Absatz 2 des Arzneimittelgesetzes

150 bis 8 000

9.8.5.3

Ausstellung einer Bescheinigung für zollamtliche Abfertigung (Einfuhr) bei Vorliegen der Bedingungen des § 72a Absatz 1 Nummer 2 oder Nummer 3 des Arzneimittelgesetzes

50 bis 200

9.8.5.4

Ausstellung einer Bescheinigung für die zollamtliche Abfertigung nach § 73 Absatz 6 des Arzneimittelgesetzes

50 bis 500

9.8.5.5

Erteilung eines Zertifikats entsprechend dem Zertifikationssystems der WHO für die Ausfuhr von Arzneimitteln nach § 73a Absatz 2 des Arzneimittelgesetzes

50 bis 200

9.8.6

Ausstellen einer Bescheinigung für die Anzeige einer klinischen Prüfung von Tierarzneimitteln

50 bis 250

9.8.6.1

Ausstellung von Duplikaten für Erlaubnisse, Zertifikate und Bescheinigungen

50

9.8.6.2

Wiederholungsausstellung bei Verlust von Erlaubnissen, Zertifikaten und Bescheinigungen

70

9.8.6.3

Änderungen auf Zertifikaten und Bescheinigungen

50

9.8.6.4

Untersuchungen pro einzelne Arzneispezialität oder sonstige Arzneimittel nach § 65 des Arzneimittelgesetzes, soweit diese Untersuchungen Maßnahmen nach § 69 des Arzneimittelgesetzes nach sich ziehen

1 100 bis 1 500

9.8.6.5

Bescheid über das Verbot des Inverkehrbringens eines Arzneimittels nach dem Arzneimittelgesetz ohne Inspektion

600

9.8.6.6

Bescheid über das Verbot des Inverkehrbringens eines Arzneimittels nach dem Arzneimittelgesetz mit Inspektion

400 bis 9 000

9.8.6.7

Durchführung von Maßnahmen gemäß § 64 und § 69 des Arzneimittelgesetzes

70 bis 3 000

9.8.6.8

Rechtsbehelfe  
  
Erteilung von Bescheiden über Widersprüche – wenn und soweit sie zurückgewiesen werden

*   von Dritten, die sich durch die Sachentscheidung beschwert fühlen
*   gegen Kosten- und/oder Gebührenentscheidungen

70 bis 600

**9.9**

_Nicht besetzt_

 

**9.10**

_Nicht besetzt_

 

**9.11**

**Gebühren im Bereich Lebensmittelüberwachung**

 

9.11.1

Zulassung von Lebensmittelunternehmen nach Artikel 6 Absatz 3 Buchstabe a oder Buchstabe c der Verordnung (EG) Nr. 852/2004, inklusive Vor-Ort-Kontrolle(n)  
Die Zulassung erfolgt nach mindestens einer Kontrolle an Ort und Stelle.

200 bis 4 000

9.11.2

Zulassung von Betrieben, die mit Lebensmitteln tierischen Ursprungs umgehen, nach Artikel 6 Absatz 3 Buchstabe b der Verordnung (EG) Nr. 852/2004 in Verbindung mit Artikel 4 Absatz 1 Buchstabe b, Absatz 2 und Absatz 3 Buchstabe a der Verordnung (EG) Nr. 853/2004 und Artikel 3 der Verordnung (EG) Nr. 854/2004

200 bis 4 000

9.11.3

Zulassung von Betrieben nach Artikel 4 Absatz 2 und Absatz 3 Buchstabe a der Verordnung (EG) Nr. 853/2004 in Verbindung mit Artikel 3 der Verordnung (EG) Nr. 854/2004, inklusive Vor-Ort-Kontrolle(n) (§ 9 TierLMHV)

200 bis 4 000

9.11.4

Erteilung einer vorläufigen/bedingten Zulassung nach Artikel 4 Absatz 2 und Absatz 3 Buchstabe b der Verordnung (EG) Nr. 853/2004 in Verbindung mit Artikel 3 der Verordnung (EG) Nr. 854/2004 (§ 9 TierLMHV)

200 bis 4 000

9.11.5

Widerruf oder Ruhenlassen einer Zulassung sowie Verlängerung einer vorläufigen Zulassung

55 bis 1 000

9.11.6

Änderungen, Ergänzungen usw.
für Betriebe, die unter die Amtshandlungen nach den Tarifstellen 9.11.1 bis 9.11.5 fallen

50 bis 2 000

9.11.7

Erteilung einer Ausnahmegenehmigung nach § 15 Absatz 1 AVV Lebensmittelhygiene gemäß Anhang I, Kapitel 3, Nr. 3.2 der Verordnung (EG) Nr. 2073/2005

30 bis 300

**9.12**

**Erhebung von Gebühren nach Artikel 27 Absatz 2 der Verordnung (EG) Nr. 882/2004 für die in Anhang IV Abschnitt A der Verordnung genannten Tätigkeiten**

 

9.12.1

Mindestgebühren bzw.
Kostenbeiträge im Zusammenhang mit der Schlachttier- und Fleischuntersuchung (Anhang IV Abschnitt B Kapitel I der Verordnung)

 

9.12.1.1

Rindfleisch

 

 

1.  ausgewachsene Rinder, je Tier

  
5

 

2.  Jungrinder, je Tier

  
2

9.12.1.2

Einhufer-/Equidenfleisch, je Tier

3

9.12.1.3

Schweinefleisch: Tiere mit einem Schlachtgewicht von

 

 

1.  weniger als 25 kg, je Tier

  
0,50

 

2.  mindestens 25 kg, je Tier

  
1

 

3.  Mindestgebühr Trichinenuntersuchung Einzelansatz

  
5,5 

9.12.1.4

Schaf- und Ziegenfleisch: Tiere mit einem Schlachtgewicht von

 

 

1.  weniger als 12 kg, je Tier

  
0,15

 

2.  mindestens 12 kg, je Tier

  
0,25

9.12.1.5

Geflügelfleisch

 

 

1.  Haushuhn und Perlhuhn, je Tier

  
0,005

 

2.  Enten und Gänse, je Tier

  
0,01

 

3.  Truthühner, je Tier

  
0,025

 

4.  Zuchtkaninchen, je Tier

  
0,005

9.12.2

Mindestgebühren bzw.
Kostenbeiträge im Zusammenhang mit der Kontrolle von Zerlegungsbetrieben (Anhang IV Abschnitt B Kapitel II der Verordnung), je Tonne Fleisch:

 

 

1.  Rindfleisch, Kalbfleisch, Schweinefleisch, Einhufer-/Equidenfleisch, Schaf- und Ziegenfleisch

2

 

2.  Geflügelfleisch und uchtkaninchenfleisch

1,50

 

3.  Zuchtwildfleisch und Wildfleisch – kleines Federwild und Haarwild

1,50

 

4.  Zuchtwildfleisch und Wildfleisch – Laufvögel (Strauß, Emu, Nandu)

3

 

5.  Zuchtwildfleisch und Wildfleisch – Schwarzwild und Wildwiederkäuer

2

9.12.3

Mindestgebühren bzw.
Kostenbeiträge im Zusammenhang mit Wildverarbeitungsbetrieben (Anhang IV Abschnitt B Kapitel III der Verordnung)

 

9.12.3.1

Kleines Federwild, je Tier

0,005

9.12.3.2

Kleines Haarwild, je Tier

0,01

9.12.3.3

Laufvögel, je Tier

0,50

9.12.3.4

Landsäugetiere

 

 

1.  Schwarzwild, je Tier

  
1,50

 

2.  Wildwiederkäuer, je Tier

  
1,00

 

3.  Sonstige Wildtiere, je Tier

  
1,00

9.12.4

Mindestgebühren bzw.
Kostenbeiträge im Zusammenhang mit der Milcherzeugung (Anhang IV Abschnitt B Kapitel IV der Verordnung)

 

 

1.  je 30 Tonnen

  
1

 

2.  danach je Tonne

  
0,50

9.12.5

Mindestgebühren bzw.
Kostenbeiträge im Zusammenhang mit der Erzeugung und Vermarktung von Fischereierzeugnissen und Erzeugnissen der Aquakultur (Anhang IV Abschnitt B Kapitel V der Verordnung)

 

 

1.  Erste Vermarktung von Fischereierzeugnissen und Erzeugnissen der Aquakultur:  
      
    für die ersten 50 Tonnen im Monat, je Tonne  
    danach je Tonne

  
  
  
1  
0,50

 

2.  Erster Verkauf auf dem Fischmarkt:  
      
    für die ersten 50 Tonnen im Monat, je Tonne  
    danach je Tonne

   
  
  
0,50  
0,25

 

3.  Erster Verkauf im Fall fehlender oder unzureichender Sortierung nach Frischegrad und/oder Größe gemäß den Verordnungen (EWG) Nr. 103/76 und Nr. 104/76:  
      
    für die ersten 50 Tonnen im Monat, je Tonne  
    danach je Tonne  
      
    Die Gebühren, die für die in Anhang II der Verordnung (EWG) Nr. 3703/85 der Kommission genannten Arten erhoben werden, dürfen 50 Euro je Sendung nicht übersteigen.

  
  
  
1  
0,50

 

4.  Verarbeitung von Fischereierzeugnissen und Erzeugnissen der Aquakultur, je Tonne

  
0,50

9.12.6

Sind die Aufwendungen für die Amtshandlungen im Sinne der Tarifstellen 9.12.1 bis 9.12.5 durch die Gebühren dieser Tarifstellen nicht kostendeckend durchzuführen, so können Gebühren in Höhe der tatsächlichen Aufwendungen erhoben werden.

nach Zeitaufwand

**9.13**

**Gebühr für sonstige Amtshandlungen  
Für Kontrollen und Untersuchungen in sonstigen Betrieben im Zusammenhang mit Frischfleischhygiene oder eingelagertem Fleisch wird die Gebühr nach tatsächlichem Aufwand der Amtshandlungen erhoben.**

nach Zeitaufwand

**9.14**

**Trichinenuntersuchung von Tieren, die keiner Schlachttier- und Fleischuntersuchung nach EG-Recht unterliegen auf der Grundlage der Verordnung (EG) Nr. 2075/2005 vom 5.
Dezember 2005 (ABl.
L 338 vom 22.12.2005, S. 60)**

2 bis 10

**9.15**

**Probenahme zwecks sonstiger Untersuchungen von Tieren (z.
B.
BSE, bakteriologische Untersuchung)**

8 bis 25

 

Hinweis:  
Trichinenuntersuchungen sowie bakteriologische Fleischuntersuchungen sind der Gemeinschaftsgebühr nach der Richtlinie 85/73/EWG und damit den in Anhang IV Abschnitt A der Verordnung (EG) Nr. 882/2004 genannten Tätigkeiten zuzuordnen.

 

**9.16**

**Erhebung von Gebühren nach Artikel 27 Absatz 2 der Verordnung (EG) Nr. 882/2004 für die in Anhang V Abschnitt A der Verordnung genannten Tätigkeiten im Zusammenhang mit den amtlichen Kontrollen von Waren und lebenden Tieren, die in die Gemeinschaft eingeführt werden**

 

9.16.1

Mindestgebühren bzw.
Kostenbeiträge im Zusammenhang mit eingeführtem Fleisch (Anhang V Abschnitt B Kapitel I der Verordnung)

 

 

1.  je Sendung bis 6 Tonnen und

  
55

 

2.  je Tonne danach bis 46 Tonnen, oder

  
9

 

3.  je Sendung über 46 Tonnen

  
420

9.16.2

Mindestgebühren bzw.
Kostenbeiträge im Zusammenhang mit eingeführten Fischereierzeugnissen (Anhang V Abschnitt B Kapitel II der Verordnung)

 

 

1.  je Sendung bis 6 Tonnen und

  
55

 

2.  je Tonne danach bis 46 Tonnen, oder

  
9

 

3.  je Sendung über 46 Tonnen

  
420

9.16.3

Mindestgebühren bzw.
Kostenbeiträge im Zusammenhang mit eingeführten Fleischerzeugnissen, Geflügelfleisch, Wildfleisch, Kaninchenfleisch, Zuchtwildfleisch, Nebenerzeugnissen und Futtermitteln tierischen Ursprungs (Anhang V Abschnitt B Kapitel III der Verordnung)

 

9.16.3.1

Mindestgebühr für die amtliche Kontrolle bei der Einfuhr einer Sendung von Erzeugnissen tierischen Ursprungs, die nicht in Anhang V Abschnitt B Kapitel I und II der Verordnung aufgeführt sind, einer Sendung von Nebenprodukten tierischen Ursprungs oder einer Futtermittelsendung

 

 

1.  je Sendung bis 6 Tonnen und

  
55

 

2.  je Tonne danach bis 46 Tonnen, oder

  
9

 

3.  je Sendung über 46 Tonnen

  
420

9.16.3.2

Mindestgebühr für die unter Tarifstelle 9.16.3.1 beschriebenen Waren bei Stückgutverschiffung

 

 

1.  je Schiff mit einer Ladung bis 500 Tonnen,

  
600

 

2.  je Schiff mit einer Ladung bis 1 000 Tonnen,

  
1 200

 

3.  je Schiff mit einer Ladung bis 2 000 Tonnen,

  
2 400

 

4.  je Schiff mit einer Ladung von mehr als 2 000 Tonnen

  
3 600

9.16.4

Mindestgebühren bzw.
Kostenbeiträge im Zusammenhang mit der Durchfuhr von Waren und lebenden Tieren durch die Gemeinschaft (Anhang V Abschnitt B Kapitel IV der Verordnung)

 

 

1.  für den Beginn der Kontrolle und

  
30

 

2.  je Viertelstunde für jede für die Kontrolle eingesetzte Person

  
20

9.16.5

Mindestgebühren bzw.
Kostenbeiträge im Zusammenhang mit eingeführten lebenden Tieren (Anhang V Abschnitt B Kapitel V der Verordnung)

 

9.16.5.1

Rinder, Einhufer, Schweine, Schafe, Ziegen, Geflügel, Kaninchen und Kleinwild (Feder- und Haarwild) und Landsäugetiere der Gattung Wildschweine und Wiederkäuer:

 

 

1.  je Sendung bis 6 Tonnen und

  
55

 

2.  je Tonne danach bis 46 Tonnen, oder

  
9

 

3.  je Sendung über 46 Tonnen

  
420

9.16.5.2

Tierarten gemäß der Entscheidung 97/794/EWG (ohne Geflügel und Kaninchen):

 

 

1.  Füchse, Hasentiere, Nerze, andere Pelztiere, Zoo- und Zirkustiere einschließlich Paarhufer und Equiden  
      
    je Tier  
    mindestens jedoch je Sendung  
    höchstens jedoch je Sendung

  
  
  
5  
30  
140

 

2.  Vögel, Bienen und sonstige Insekten,Nagetiere, Reptilien, Amphibien, Wirbellose  
      
    je Haltungseinheit  
    mindestens jedoch je Sendung  
    höchstens jedoch je Sendung

  
  
  
5  
30  
140

 

3.  Tiere der Aquakultur (ohne Zierfische)  
      
    je Tonne  
    mindestens jedoch je Sendung  
    höchstens jedoch je Sendung

  
  
  
5  
30  
140

9.16.5.3

Für andere Tierarten, die nicht unter die Tarifstellen 9.16.5.1 und 9.16.5.2 fallen:

 

 

1.  Hunde, Katzen, Frettchen, Affen, Halbaffen  
      
    mindestens jedoch je Sendung bis 46 Tonnen  
    mindestens jedoch je Sendung über 46 Tonnen

  
  
  
55  
420

 

2.  Zierfische  
      
    je Tonne  
    mindestens jedoch je Sendung bis 46 Tonnen  
    mindestens jedoch je Sendung über 46 Tonnen

  
  
  
5  
55  
420

9.16.6

Kontrollpflichtige Lebensmittel und Waren pflanzlicher Herkunft

 

 

1.  je Sendung bis 6 Tonnen und

  
55

 

2.  je Tonne danach bis 46 Tonnen, oder

  
9

 

3.  je Sendung über 46 Tonnen

  
420

 

In der Gebühr sind die ggf.
erforderlichen Analysekosten nicht enthalten.

 

9.16.7

Sind die Aufwendungen für die Amtshandlungen im Sinne der Tarifstellen 9.16.1 bis 9.16.6 durch die Gebühren dieser Tarifstellen nicht kostendeckend durchzuführen, so können Gebühren in Höhe der tatsächlichen Kontrollkosten nach der Dauer der Amtshandlung erhoben werden.

nach Zeitaufwand

9.16.8

Ausstellen einer Bescheinigung über das Ergebnis der nach Anhang V Abschnitt A der Verordnung (EG) Nr. 882/2004 vorgenommenen Tätigkeiten

5 bis 110

**9.17**

**Amtshandlungen nach der Lebensmitteleinfuhr-Verordnung (LMEV)**

 

9.17.1

Entscheidung über die Erteilung einer Genehmigung nach § 15 Absatz 3 LMEV

 

 

1.  für 6 Monate bei wiederholten Sendungen

  
100

 

2.  für Einzelsendungen

  
20

 

3.  für Messen und Ausstellungen, je nach Warenumfang

  
50 bis 150

9.17.2

Freigabe von Sendungen entsprechend der Genehmigung nach Tarifstelle 9.17.1

30

**9.18**

**Amtshandlungen anlässlich der Rücksendung oder unschädlichen Beseitigung von Sendungen**

 

9.18.1

Amtshandlungen anlässlich der Rücksendung oder unschädlichen Beseitigung von Sendungen sowie deren Lagerung bis zur Rücksendung oder unschädlichen Beseitigung, wenn die Erzeugnisse nicht den Einfuhrbedingungen entsprechen oder Unregelmäßigkeiten vorliegen oder bis zum Vorliegen von Untersuchungsergebnissen zu verwahren sind (einschließlich der Kosten für Transport, Be- und Entladen, aber ohne Untersuchungskosten)  
pro Tag und je kg Ware  
Die Gebühr verdoppelt sich an Sonnabenden, Sonn- und Feiertagen.

10 bis 26

9.18.2

Einziehung und Vernichtung beschlagnahmter Sendungen in persönlichem Reisegepäck  
pro kg  
mindestens

  
3  
30

**9.19**

**Amtshandlungen im Zusammenhang mit Quarantänemaßnahmen (einschließlich Unterbringung, Haltung und Pflege der Tiere, aber ohne Untersuchungskosten)**  
  
Gebühren pro Tier und Tag für:

 

 

1.  Einhufer

  
13

 

2.  Rinder, Wildklauentiere

  
7,50

 

3.  Jungrinder

  
5

 

4.  Kälber, Schafe, Schweine

  
3

 

5.  Hunde  
      
    bis 10 kg  
    11 bis 30 kg  
    über 30 kg

  
  
  
5  
7  
8,50

 

6.  Katze, Füchse, Nerze, Frettchen

  
4

 

7.  Kaninchen, Hasen

  
1,50

 

8.  Wellensittiche und andere kleine Vögel

  
0,80

 

9.  größere Vögel (Papageien, Raubvögel), Geflügel

  
1

 

10.  andere Tiere

  
5 bis 140

 

Die Gebühr verdoppelt sich an Sonnabenden, Sonn- und Feiertagen.

 

**9.20**

**Weitere Amtshandlungen im Lebensmittelrecht**

 

9.20.1

Ausstellung von Bescheinigungen für Exporte von Lebensmitteln 

50 bis 200

9.20.2

Amtshandlungen nach § 39 Absatz 2 des Lebensmittel- und Futtermittelgesetzbuches (LFGB) sowie Durchführung zusätzlicher amtlicher Kontrollen im Sinne des Artikel 28 Satz 1 und 3 der Verordnung (EG) Nr. 882/2004

25 bis 10 000

9.20.3

Zulassung von privaten Sachverständigen für die Untersuchung amtlich zurückgelassener Proben

200 bis 500

9.20.4

Entscheidung über die Verkehrsfähigkeit einer Sendung bei der Zolleinfuhr (§ 55 Absatz 1 Nummer 3 LFGB)

50 bis 650

9.20.5

Erteilung einer Ausnahmegenehmigung nach den §§ 68, 69 LFGB

50 bis 650

9.20.6

Beurteilung eines Lebensmittels, Tabakerzeugnisses, kosmetischen Mittels oder eines Bedarfsgegenstandes

50 bis 650

9.20.7

Ausstellen einer Bescheinigung über das Ergebnis der nach Tarifstelle 9.20.6 vorgenommenen Amtshandlung

1 bis 51

9.20.8

Entscheidung über die Erteilung einer Genehmigung für die Herstellung von Nitritpökelsalz nach § 5 Absatz 5 Satz 1 der Zusatzstoff-Verkehrsordnung

30 bis 300

9.20.9

Entscheidung über Anträge auf Erteilung einer Genehmigung zum Herstellen von jodiertem Kochsalzersatz, anderen diätetischen Lebensmitteln mit einem Zusatz von Jodverbindungen oder diätetische Lebensmittel, die zur Verwendung als bilanzierte Diät bestimmt sind, nach § 11 Absatz 1 der Diätverordnung

30 bis 300

9.20.10

Entscheidung über den Antrag auf Zulassung einer Einrichtung zur Bestrahlung von Lebensmitteln einschließlich Erteilung einer Referenznummer nach § 4 der Lebensmittelbestrahlungsverordnung

30 bis 3 000

**9.21**

**Entscheidungen und sonstige Amtshandlungen im Anwendungsbereich der Verordnung (EG) Nr. 853/2004 und anderer Vorschriften**

 

9.21.1

Entscheidung über die Genehmigung des Verarbeitens von Rohmilch zur Herstellung von Käse mit einer Reifezeit von mindestens 60 Tagen (Artikel 10 Absatz 8 Buchstabe b der Verordnung (EG) Nr. 853/2004, § 19 der Tierischen Lebensmittel-Hygieneverordnung)

55 bis 1 000

9.21.2

Entscheidung über einen Antrag auf Schlachtung von Geflügel im Haltungsbetrieb (Anhang III Abschnitt II Kapitel VI Satz 1 der Verordnung (EG) Nr. 853/2004)

55 bis 1 000

9.21.3

Entscheidung über einen Antrag auf Schlachtung von in Wildfarmen gehaltenen Laufvögeln und Huftieren gemäß Anhang III Abschnitt III Nummer 3 der Verordnung (EG) Nr. 853/2004

55 bis 1 000

9.21.4

Befähigungsnachweis über die erfolgreiche Prüfung für amtliche Fachassistenten nach Anhang I Abschnitt III Kapitel IV Buch-stabe B Nummer 5 der Verordnung (EG) Nr. 854/2004 (§ 3 Absatz 1 Nummer 4a der Tierischen Lebensmittel-Überwachungsverordnung)

150

9.21.5

Ausstellung einer Ersatzbescheinigung

30

9.21.6

Genehmigung der Mitwirkung durch Personal eines Schlachthofes bei der amtlichen Überwachung der Herstellung von Fleisch von Geflügel und Hasentieren gemäß Anhang I Abschnitt III Kapitel III Buchstabe A der Verordnung (EG) Nr. 854/2004 (§ 4 Absatz 1 Nummer 1, Absatz 2 der Tierischen Lebensmittel-Überwachungsverordnung)

50 bis 150

9.21.7

Genehmigung einschließlich erforderlichenfalls weiterer Maßnahmen von Schlachtungen im Rahmen von Programmen zur Tilgung oder Bekämpfung von Tierseuchen oder Zoonoseerregern im Sinne von Anhang I Abschnitt II Kapitel III Nummer 7 der Verordnung (EG) Nr. 854/2004 (§ 5 der Tierischen Lebensmittel-Überwachungsverordnung)

50 bis 150

9.21.8

Entscheidung über Anträge auf Erteilung einer Ausnahmegenehmigung im Sinne des Artikels 5 der Verordnung (EG) Nr. 2073/2005

100 bis 250

**9.22**

**Entscheidungen und sonstige Amtshandlungen außerhalb des Anwendungsbereiches der Verordnung (EG) Nr. 853/2004**

 

9.22.1

Schlachttieruntersuchung bei der Abgabe kleiner Mengen Fleisch von Geflügel oder Hasentieren (§ 7 der Tierischen Lebensmittel-Überwachungsverordnung)

50 bis 150

9.22.2

Schulung und Beauftragung (inklusive Bescheinigung) zur Entnahme von Trichinenproben bei Schwarzwild durch Jäger

15 bis 50

**9.23**

**Anordnung der Aussetzung der Milchanlieferung nach Anhang IV Kapitel II Nummer 2 der Verordnung (EG) Nr. 854/2004 bzw.
deren Aufhebung (§ 9 der Tierischen Lebensmittel-Überwachungsverordnung)**

50 bis 150

**9.24**

**Gebühren nach der Mineral- und Tafelwasser-Verordnung**

 

9.24.1

Erteilung einer amtlichen Anerkennung von natürlichem Mineralwasser

250 bis 550

9.24.2

Erteilung einer amtlichen Anerkennung von natürlichem Mineralwasser aus dem Boden eines nicht der EU angehörenden Landes

250 bis 550

9.24.3

Erteilung einer Nutzungsgenehmigung für Quellen, aus denen natürliches Mineralwasser gewonnen wird

250 bis 550

**9.25**

**Ausnahmegenehmigung nach § 9 Absatz 7 des Vorläufigen Biergesetzes**

50 bis 260

**9.26**

**Besondere Amtshandlungen im Weinrecht**

 

9.26.1

Erteilung einer Ausnahmegenehmigung nach § 28 der Weinverordnung (WeinV 1995)

60 bis 220

9.26.2

Erteilung einer Ausnahmegenehmigung nach § 2 Absatz 1 der Wein-Überwachungsverordnung (WeinÜV)

60 bis 220

9.26.3

Amtshandlungen nach der Verordnung (EG) Nr. 884/2001 über die Begleitdokumente für die Beförderung von Weinbauerzeugnissen und Ein- und Ausgangsbücher im Weinsektor

 

9.26.3.1

Erteilung einer Bezugsnummer und des Sichtvermerks im Begleitpapier (Artikel 3 Absatz 4)

10 bis 30

9.26.3.2

Bescheinigung der Ursprungsbezeichnung der Qualitätsweine b.A.
und der Herkunftsangabe bei Qualitätsweinen b.A., die mit einer geografischen Angabe versehen werden können (Artikel 7 Absatz 1 und 2)

10 bis 30

9.26.3.3

Genehmigung eines Buchführungsverfahrens nach Artikel 12 Absatz 1 Unterabsatz 1 der Verordnung (EG) Nr. 884/2001 (§ 12 WeinÜV)

50 bis 150

**9.27**

**Amtshandlungen nach der Lebensmittelkontrolleur-Verordnung (LKonV)**

 

9.27.1

Befähigungsnachweis über die erfolgreiche Prüfung für Lebensmittelkontrolleure

150

9.27.2

Ausstellen einer Ersatzbescheinigung

30

**9.28**

**Gebühren im Bereich Futtermittelüberwachung**

 

9.28.1

Erhebung von Gebühren nach Artikel 27 Absatz 2 der Verordnung (EG) Nr. 882/2004 für die in Anhang V Abschnitt A der Verordnung genannten Tätigkeiten

 

9.28.1.1

Mindestgebühren bzw.
Kostenbeiträge für die amtliche Kontrolle bei der Einfuhr von Futtermittelsendungen tierischen Ursprungs (Anhang V Abschnitt B Kapitel III der Verordnung)

 

 

1.  je Sendung bis 6 Tonnen und

  
55

 

2.  je Tonne danach bis 46 Tonnen, oder

  
9

 

3.  je Sendung über 46 Tonnen

  
420

9.28.1.2

Sind die Aufwendungen für die Grenzkontrollen im Sinne der Tarifstelle 9.28.1.1 durch die Gebühren dieser Tarifstellen nicht kostendeckend durchzuführen, so können Gebühren in Höhe der tatsächlichen Kontrollkosten nach der Dauer der Amtshandlung erhoben werden.

nach Zeitaufwand

9.28.1.3

Ausstellen einer Bescheinigung über das Ergebnis der nach Anhang V Abschnitt A der Verordnung (EG) Nr. 882/2004 vorgenommenen Tätigkeiten

5 bis 110

9.28.2 

Amtshandlungen auf Grund der Verordnung (EG) Nr. 999/2001 

 

9.28.2.1

Entscheidung über die Zulassung von Betrieben, die Fischmehl, Dicalciumphosphat, Tricalciumphosphat, Blutmehl oder Blutprodukte enthaltende Futtermittel für Nichtwiederkäuer herstellen (Anhang IV Teil II Abschnitt B, C und D der Verordnung (EG) Nr. 999/2001)

200 bis 700

9.28.2.2

Entscheidung über die Gestattung der Verwendung und Lagerung von Futtermitteln, die Fischmehl, Dicalciumphosphat, Tricalciumphosphat, Blutmehl oder Blutprodukte enthalten, in landwirtschaftlichen Betrieben, in denen Wiederkäuer gehalten werden (Anhang IV Teil II Abschnitt B, C und D der Verordnung (EG) Nr. 999/2001)

30 bis 2 000

9.28.2.3

Entscheidung über die Genehmigung eines Verfahrens zur Reinigung der Fahrzeuge, in denen nach dem Transport von Futtermitteln, die Fischmehl, Dicalciumphosphat, Tricalciumphosphat, Blutmehl oder Blutprodukte enthalten, für Wiederkäuer bestimmte Futtermittel transportiert werden sollen (Anhang IV Teil II Abschnitt B Buchstabe e, Abschnitt C Buchstabe c und Abschnitt D Buchstabe e der Verordnung (EG) Nr. 999/2001)

300 bis 1 000

9.28.2.4

Registrierung von Selbstmischern, die Alleinfuttermittel für Tiere, die keine Wiederkäuer sind, aus Futtermitteln herstellen, die Fischmehl, Dicalciumphosphat, Tricalciumphosphat, Blutmehl oder Blutprodukte enthalten, inklusive Vor-Ort-Kontrolle(n) (Anhang IV Kapitel II Abschnitt B, C und D der Verordnung (EG) Nr. 999/2001)

50 bis 250

9.28.3

Entscheidung über die Zulassung von Futtermittelbetrieben nach Artikel 10 der Verordnung (EG) Nr. 183/2005

 

9.28.3.1

Zulassung von Betrieben, die in Anhang IV Kapitel 1 der Verordnung (EG) Nr. 183/2005 genannte Erzeugnisse herstellen und/oder in den Verkehr bringen (Artikel 10 Nummer 1a der Verordnung (EG) Nr. 183/2005)

 

 

1.  bei erstmaliger Entscheidung

  
150 bis 2 500

 

2.  bei erneuter Prüfung der Zulassungsvoraussetzungen auf Grund von im Betrieb sich ergebenden Änderungen, auch bei Widerruf oder Verlängerung einer Zulassung sowie Änderungen auf Antrag

  
50 bis 1 500

9.28.3.2

Zulassung von Betrieben, die Vormischungen unter Verwendung der in Anhang IV Kapitel 2 der Verordnung (EG) Nr. 183/2005 genannten Futtermittelzusatzstoffe herstellen und/oder in den Verkehr bringen (Artikel 10 Nummer 1b der Verordnung (EG) Nr. 183/2005)

 

 

1.  bei erstmaliger Entscheidung

  
150 bis 2 500

 

2.  bei erneuter Prüfung der Zulassungsvoraussetzungen auf Grund von im Betrieb sich ergebenden Änderungen, auch bei Widerruf oder Verlängerung einer Zulassung sowie Änderungen auf Antrag

  
50 bis 1 500

9.28.3.3

Zulassung von Betrieben, die Mischfuttermittel für das Inverkehrbringen herstellen oder ausschließlich für den Bedarf des eigenen landwirtschaftlichen Betriebes erzeugen, die Futtermittelzusatzstoffe oder Vormischungen mit in Anhang IV Kapitel 3 der Verordnung (EG) Nr. 183/2005 genannten Futtermittelzusatzstoffe enthalten (Artikel 10 Nummer 1c der Verordnung (EG) Nr. 183/2005)

 

 

1.  bei erstmaliger Entscheidung

  
150 bis 2 500

 

2.  bei erneuter Prüfung der Zulassungsvoraussetzungen auf Grund von im Betrieb sich ergebenden Änderungen, auch bei Widerruf oder Verlängerung einer Zulassung sowie Änderungen auf Antrag

  
50 bis 1 500

9.28.4

Amtshandlungen nach der Futtermittelverordnung (FuttMV)

 

9.28.4.1

Entscheidung über die Zulassung als Vertreter des Herstellers für Einfuhren nach § 28 Absatz 3 FuttMV

 

 

1.  bei erstmaliger Entscheidung

  
150 bis 750

 

2.  bei erneuter Prüfung der Anerkennungsvoraussetzungen auf Grund von im Betrieb sich ergebenden wesentlichen Änderungen

  
50 bis 500

9.28.4.2

Zulassung von Betrieben, die Futtermittel dekontaminieren, § 28 Absatz 1 FuttMV

100 bis 500

9.28.4.3

Zulassung von Betrieben, die Grünfutter, Lebensmittel oder Lebensmittelreste zum Zwecke der Herstellung eines Einzelfuttermittels oder Mischfuttermittels unter direkter Einwirkung der Verbrennungsgase trocknen, § 28 Absatz 2 FuttMV

100 bis 500

9.28.4.4

Entscheidung über die Registrierung als Vertreter des Herstellers nach § 30 Satz 2 Nummer 1 FuttMV

 

 

1.  bei erstmaliger Entscheidung

  
150 bis 750

 

2.  bei erneuter Prüfung der Anerkennungsvoraussetzungen auf Grund von im Betrieb sich ergebenden wesentlichen Änderungen

  
50 bis 500

9.28.4.5

Rücknahme, Widerruf, Ruhen und Erlöschen der Zulassung oder Registrierung, § 32 FuttMV

  
20 bis 500

9.28.5

Amtshandlungen auf Grund des Lebensmittel- und Futtermittelgesetzbuches (LFGB)

 

9.28.5.1

Durchführung zusätzlicher amtlicher Kontrollen (im Sinne des Artikels 28 Satz 1 und 3 der Verordnung (EG) Nr. 882/2004)

25 bis 5 000

9.28.5.2

Amtshandlungen nach § 39 Absatz 2 LFGB

50 bis 10 000

9.28.5.3

Entscheidung über Anträge auf Erteilung einer Ausnahmegenehmigung nach den §§ 68, 69 LFGB

50 bis 500

9.28.5.4

Ausstellung von Bescheinigungen für Exporte von Futtermitteln, Vormischungen oder Zusatzstoffen

50 bis 200

9.28.6

Amtshandlungen nach der Futtermittelkontrolleur-Verordnung (FuttMKontrV)

 

9.28.6.1

Befähigungsnachweis über die erfolgreiche Schulung und Prüfung für Futtermittelkontrolleure

100

9.28.6.2

Ausstellen einer Ersatzbescheinigung

30

**9.29**

**Entscheidung über den Antrag auf Zulassung eines Prüflabors nach § 4 der Tabakprodukt-Verordnung**

**60 bis 600**

**9.30**

**Entscheidung über Anträge auf Erteilung einer Registriernummer nach § 5a der Kosmetikverordnung**

**200 bis 2 000**

**9.31**

**Besondere Grundsätze der Tarifstelle 9** 

 

9.31.1

Die Tarifstellen 9.2, 9.20.2 und 9.20.6 gelten auch für freiwillige Untersuchungen oder Untersuchungen auf Antrag, die nicht im überwiegenden öffentlichen Interesse durchgeführt werden.
Die Gebühren werden 21 Kalendertage nach Bekanntgabe des Gebührenbescheides fällig.

 

9.31.2

Zuschläge für Amtshandlungen außerhalb der gewöhnlichen Dienstzeit:

 

 

1.  Für Amtshandlungen, deren Gebührenhöhe sich nach dem erforderlichen Zeitaufwand berechnet, kann ein Zeitzuschlag entsprechend § 8 TVöD erhoben werden.

  
 

 

2.  Für Amtshandlungen, für die eine Festgebühr vorgesehen ist, tritt an die Stelle der Festgebühr ein Rahmensatz von der jeweiligen Festgebühr (als Untergrenze) bis zum doppelten Betrag der jeweiligen Festgebühr (als Obergrenze).

  
 

 

3.  Für Amtshandlungen mit einem Gebührenrahmen tritt an die Stelle des Gebührenrahmens ein Rahmensatz von der jeweiligen Untergrenze bis zum doppelten Betrag der Obergrenze des jeweiligen Gebührenrahmens.

  
 

 

Als regelmäßige Dienstzeit gilt werktags außer Samstag von 6:00 bis 20:00 Uhr.

  
 

9.31.3

Kann eine Amtshandlung aus Gründen, die der Behördenbedienstete nicht zu vertreten hat, nicht durchgeführt werden oder verzögert sich ihre Durchführung, so kann unbeschadet der sonstigen Gebührenpflicht eine Versäumnisgebühr erhoben werden für jede angefangene halbe Stunde des Zeitverlustes.

30

9.31.4

Anfallende Kosten für Probentransporte sind in der jeweiligen Gebühr enthalten.

  
 

**9.32**

**Gebühren auf Grund des Infektionsschutzgesetzes in Verbindung mit der Trinkwasserverordnung (TrinkwV 2001)**

  
 

9.32.1

Maßnahmen im Falle der Nichteinhaltung von Grenzwerten und Anforderungen nach § 9 TrinkwV 2001

  
 

 

1.  Anordnung von Maßnahmen bei Nichteinhaltung von Grenzwerten und Anforderungen nach § 9 Absatz 1

10 bis 200

 

2.  Anordnung einer anderweitigen Versorgung oder Weiterführung mit Auflagen nach § 9 Absatz 2

10 bis 200

 

3.  Anordnung der Unterbrechung der Wasserversorgung nach § 9 Absatz 3

10 bis 200

 

4.  Anordnung von Abhilfemaßnahmen zur Wiederherstellung der Trinkwasserqualität nach § 9 Absatz 4

10 bis 200

 

5.  Anordnung von Maßnahmen oder Festlegungen bei Nichteinhaltung oder Nichterfüllung von Grenzwerten oder Anforderungen nach § 9 Absatz 5

  
10 bis 1 000

 

6.  Festlegung für chemische Stoffe oder Mikroorganismen, für die keine Grenzwerte aufgeführt sind, nach § 9 Absatz 6

  
10 bis 200

 

7.  Anordnung von Maßnahmen bei Nichteinhaltung oder Nichterfüllung von festgelegten Grenzwerten oder Anforderungen, die auf die Trinkwasserinstallation oder deren unzulängliche Instandhaltung zurückzuführen ist sowie Beratungstätigkeit, nach § 9 Absatz 7

  
50 bis 500

 

8.  Aufforderung zum Nachkommen von besonderen Handlungspflichten und Überprüfung oder Anordnung von Maßnahmen nach § 9 Absatz 8

50 bis 200

 

9.  Festlegung bei Nichteinhaltung oder Nichterfüllung von Grenzwerten oder Anforderungen nach § 9 Absatz 9

10 bis 200

9.32.2

Zulassung der Abweichung von Grenzwerten für chemische Parameter nach § 10 TrinkwV 2001

50 bis 120

 

1.  Zulassung der Abweichung von Grenzwerten für chemische Parameter nach Anlage 2 für eine 30-Tage-Frist nach § 10 Absatz 1

50 bis 200

 

2.  Erste Zulassung der Abweichung von Grenzwerten nach § 10 Absatz 2 für chemische Parameter nach Anlage 2

50 bis 200

 

3.  Zweite Zulassung der Abweichung von Grenzwerten nach § 10 Absatz 5 für chemische Parameter nach Anlage 2

50 bis 500

 

4.  Dritte Zulassung der Abweichung von Grenzwerten nach § 10 Absatz 6 für chemische Parameter nach Anlage 2

50 bis 1 000

9.32.3

Prüfung einer Beeinträchtigung der Genusstauglichkeit des Enderzeugnisses und Zulassung der Ausnahme hinsichtlich der Qualität des verwendeten Wassers in einem Lebensmittelbetrieb nach § 18 Absatz 1 Satz 3 TrinkwV 2001

50 bis 200

9.32.4

Überprüfung der Erfüllung der Anforderungen an Untersuchungsstellen und/oder Aufnahme in die Landesliste nach § 15 Absatz 5 in Verbindung mit § 15 Absatz 4 TrinkwV 2001

200 bis 1 000

9.32.5

Überwachung nach den §§ 18 und 19 TrinkwV 2001

In die Gebühr sind grundsätzlich alle im Zusammenhang mit der Amtshandlung notwendigen Auslagen einbezogen.
Ausgenommen hiervon sind die Kosten für die Vergütung von Leistungen Dritter.

 

 

1.  Wasserversorgungsanlagen nach § 3 Absatz 1 Nummer 2 Buchstabe a TrinkwV 2001 (zentrale Wasserwerke)

50 bis 1000

 

2.  Wasserversorgungsanlagen nach § 3 Absatz 1 Nummer 2 Buchstabe b TrinkwV 2001 (dezentrale kleine Wasserwerke)

50 bis 200

 

3.  Wasserversorgungsanlagen nach § 3 Absatz 1 Nummer 2 Buchstabe c TrinkwV 2001 (Kleinanlagen zur Eigenversorgung)

10 bis 100

 

4.  Wasserversorgungsanlagen nach § 3 Absatz 1 Nummer 2 Buchstabe d TrinkwV 2001 (mobile Versorgungs-anlagen)

50 bis 200

 

5.  Wasserversorgungsanlagen nach § 3 Absatz 1 Nummer 2 Buchstabe e TrinkwV 2001 (ständige Wasserverteilung)

50 bis 500

 

6.  Wasserversorgungsanlagen nach § 3 Absatz 1 Nummer 2 Buchstabe f TrinkwV 2001 (zeitweise Wasserverteilung)

50 bis 200

9.32.6

Anordnung nach § 19 Absatz 3 TrinkwV 2001

10 bis 100

9.32.7

Anordnung nach § 20 TrinkwV 2001

10 bis 500

**9.33**

**Amtshandlungen auf Grund von § 37 des Infektionsschutzgesetzes (IfSG) in Verbindung mit der DIN 19643**

 

9.33.1

Überwachung der Qualität von Wasser in Schwimm- und Badebecken nach § 37 Absatz 2 und 3 IfSG sowie in künstlichen Badeteichen nach Stand der Technik.

In die Gebühr sind grundsätzlich alle im Zusammenhang mit der Amtshandlung notwendigen Auslagen einbezogen.
Ausgenommen hiervon sind die Kosten für die Vergütung von Leistungen Dritter. 

10 bis 500

**9.34**

**Amtshandlungen auf Grund des Brandenburgischen Gesundheitsdienstgesetzes (BbgGDG)**

 

9.34.1

Überwachung, Besichtigung und Überprüfung von Einrichtungen und deren Leistungen auf die Einhaltung der Anforderungen an die Wasserhygiene nach § 3 BbgGDG, die nicht einem von der handelnden Behörde wahrzunehmenden besonderen öffentlichen Interesse dienen.

In die Gebühr sind grundsätzlich alle im Zusammenhang mit der Amtshandlung notwendigen Auslagen einbezogen.
Ausgenommen hiervon sind die Kosten für die Vergütung von Leistungen Dritter.

20 bis 500

 

 

 

**10**

**Sachverständigenwesen**

 

**10.1**

**Antragsgebühr**

**76,5**

**10.2**

**Bestellungsgebühr**

**76,5**

**10.3**

**Wiederbestellung früherer Sachverständiger**

**76,5**

**10.4**

**Gebühr für die Durchführung der Sachkundenachweise bei der erstmaligen Bestellung für ein Fachgebiet**

**153,5**

10.4.1

für jedes weitere Fachgebiet bei der erstmaligen Bestellung erhöht sich die Gebühr je Fachgebiet um

50

10.4.2

Gebühr für die Erweiterung der öffentlichen Bestellung, je Fachgebiet bei bereits bestellten Sachverständigen

100

**10.5**

**Gebühr für die öffentliche Bestellung als Probenehmer**

**125**

**10.6**

**Gebühr für die Verlängerung der öffentlichen Bestellung als Probenehmer**

**30**

 

 

 

**11**

**Gebühren für die Abnahme von Prüfungen und sonstige Angelegenheiten nach dem Berufsbildungsgesetz (BBiG)**

 

**11.1**

**Berufsabschlussprüfungen (außer Regelerstausbildung)**

**153,5**

**11.2**

**Prüfungen gemäß Ausbilder-Eignungsverordnung**

**102**

**11.3**

**Fortbildungsprüfungen**

 

11.3.1

Meisterprüfungen gemäß den §§ 81, 95 BBiG

332,5

11.3.2

Meisterprüfungen gemäß den §§ 81, 95 BBiG ohne berufs- und arbeitspädagogischen (BAP) Teil

230

11.3.3

Fortbildungsprüfungen gemäß § 46 BBiG (außer Lebensmittelkontrolleur/Lebensmittelkontrolleurin)

332,5

**11.4**

**Wiederholung von Prüfungen oder Prüfungsmodulen**

 

11.4.1

Berufsabschlussprüfungen

 

11.4.1.1

Bereich: praktische/betriebliche Prüfung

102

11.4.1.2

Bereich: fachtheoretische (schriftliche/mündliche) Prüfung

51

11.4.2

Prüfungen gemäß Ausbilder-Eignungsverordnung  
(einschließlich des berufs- und arbeitspädagogischen \[BAP\] Teils der Meisterprüfungen)

 

11.4.2.1

Praktischer Teil

76,5

11.4.2.2

Fachtheoretischer Teil

25,5

11.4.3

Fortbildungsprüfungen

 

11.4.3.1

bei insgesamt 3 Prüfungsteilen je Teil (außer BAP-Teil)

115

11.4.3.2

bei insgesamt 4 Prüfungsteilen je Teil (außer BAP-Teil)

76,5

**11.5**

**Anerkennung der Gleichwertigkeit von Berufsabschlüssen nach § 25 BBiG 2005 und von Meisterprüfungen nach den §§ 81 und 95 BBiG**

15,5

 

gebührenfrei:  
Anerkennung der Bildungsnachweise von Berechtigten nach dem Bundesvertriebenengesetz sowie asylberechtigten Personen und anerkannten Flüchtlingen mit dauerndem Bleiberecht

  
 

 

Anmerkung:  
Mit Beginn der Prüfung ist unabhängig von deren weiterem Verlauf die Gesamtgebühr für die Prüfung zu begleichen.

  
 

 

 

 

**12**

**Amtshandlungen nach dem Verbraucherinformationsgesetz (VIG)**

 

**12.1**

**Gebühren für Amtshandlungen nach Maßgabe des § 7 Absatz 1 Satz 1 und 2 des Verbraucherinformationsgesetzes**

**nach Zeitaufwand**

**12.2**

**Auslagen** **Auslagen werden zusätzlich zu den Gebühren nach Tarifstelle 12.1 erhoben.
Bei der Herstellung von Zweitschriften, Kopien und Computerausdrucken in geringem Umfang kann auf die Erhebung der Auslagen verzichtet werden.** 

**gemäß Anlage 1,  
im Übrigen in voller Höhe**

 

 

 

**13**

**Amtshandlungen auf der Grundlage des Einkommensteuergesetzes (EStG)**

 

 

Erstellung einer Bescheinigung nach § 14a Absatz 3 Nummer 2 EStG

31 bis 92

 

 

 

**14**

**Vollzug allgemeiner umweltrechtlicher Vorschriften**

 

 

Amtshandlungen gegenüber dem Verantwortlichen nach dem Umweltschadensgesetz, soweit diese nicht von einer anderen fachspezifischen Tarifstelle erfasst werden

nach Zeitaufwand

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#_ftnref1) Sind Rahmengebühren vorgesehen, richtet sich die Höhe der Gebühr nach dem Aufwand.
Ist eine Gebührenreduktion vorgesehen, richtet sich die Höhe der Reduktion nach der Aufwandserleichterung.
Mehrfachreduktionen sind möglich.