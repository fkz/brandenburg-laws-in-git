## Verordnung zur Festlegung der entgeltfreien Bereitstellung und der Nutzungsbestimmungen für digitale Geobasisinformationen und Geodatendienste (BbgGeoNutzV)

Auf Grund des § 10 Absatz 9 in Verbindung mit § 10 Absatz 2 des Brandenburgischen Vermessungsgesetzes vom 27.
Mai 2009 (GVBl.
I S. 166), der durch Gesetz vom 19. Juni 2019 (GVBl. I Nr. 32) geändert worden ist, verordnet der Minister des Innern und für Kommunales:

#### § 1 Art und Umfang der entgeltfreien Bereitstellung von standardisierten digitalen Geobasisinformationen

(1) Die entgeltfreie Bereitstellung der standardisierten digitalen Geobasisinformationen des amtlichen Vermessungswesens (digitale Standardprodukte) erfolgt vorrangig über Darstellungs- und Downloaddienste, aber auch über andere Downloadmöglichkeiten der Datensätze automatisiert beziehungsweise durch Selbstentnahme aus den Landesportalen.
Abhängig vom Datensatz kann dies durch portionierte Datenpakete erfolgen.

(2) Die digitalen Standardprodukte (Datensätze und Datendienste) können der Anlage entnommen werden.
Die dazugehörigen Produktbeschreibungen sind deren Metadaten oder der Homepage der Landesvermessung und Geobasisinformation Brandenburg zu entnehmen.

(3) Die Fortschreibung des Verzeichnisses der entgeltfrei bereitgestellten digitalen Standardprodukte (Datensätze und Datendienste) der Anlage erfolgt durch deren Aufnahme in das Vermessungsentgeltverzeichnis in der jeweils geltenden Fassung.

(4) Für personenbezogene Geobasisinformationen ist § 10 Absatz 1 und 4 bis 8 des Brandenburgischen Vermessungsgesetzes vom 27.
Mai 2009 (GVBl.
I S.
166), das zuletzt durch Gesetz vom 19.
Juni 2019 (GVBl.
I Nr.
32) geändert worden ist, maßgebend.

#### § 2 Ausnahmen von der Entgeltfreiheit der Datenbereitstellungen

Sofern die Datenbereitstellung nicht über automatisierte Verfahren oder Selbstentnahme erfolgt, kann von der bereitstellenden Behörde ein Entgelt für den entstandenen Aufwand der Datenbereitstellung erhoben werden.
Gleiches gilt für erforderliche Prüfungen des berechtigten Interesses bei personenbezogenen Daten, die manuelle Einrichtung von Abrufverfahren (Nutzerbezogene Bestandsdatenaktualisierung - NBA-Verfahren) oder für eine auf Anforderung vom digitalen Standardprodukt in der Anlage abweichende Datenbereitstellung.
Art und Höhe des Entgeltes sind dem Vermessungsentgeltverzeichnis in der jeweils gültigen Fassung zu entnehmen.

#### § 3 Nutzungen, Datenlizenz und Quellenvermerk

(1) Für die Nutzung der Geobasisinformationen nach § 1 einschließlich zugehöriger Metadaten und Datendienste gelten die durch den IT-Planungsrat im Datenportal für Deutschland (GovData: https://www.govdata.de/dl-de/by-2-0) veröffentlichten einheitlichen Lizenzbedingungen „Datenlizenz Deutschland - Namensnennung“ in der jeweils geltenden Version.

(2) Die Namensnennung des Rechteinhabers und Bereitstellers erfolgt einheitlich im Quellenvermerk mit © GeoBasis-DE/LGB.
Soweit Geobasisinformationen durch eine Katasterbehörde bereitgestellt werden, kann die Katasterbehörde diese Namensnennung durch den Zusatz „/“ und die Bezeichnung der Katasterbehörde ergänzen.

(3) Bei Veröffentlichungen ist ein Quellenvermerk mit der Namensnennung gemäß Absatz 2 erforderlich, soweit die Quelle nicht unmittelbar aus dem Dokument ersichtlich ist.

(4) Die Nutzerinnen und Nutzer haben sicherzustellen, dass

1.  alle in den Metadaten der Geobasisinformationen beigegebenen Quellenvermerke gemäß Absatz 2 und sonstige rechtliche Hinweise erkennbar und in optischem Zusammenhang eingebunden werden;
2.  Veränderungen, Bearbeitungen, neue Gestaltungen oder sonstige Abwandlungen mit einem Veränderungshinweis im beigegebenen Quellenvermerk versehen werden oder, sofern die geodatenhaltende Stelle dies verlangt, der beigegebene Quellenvermerk gelöscht wird.

(5) Die Landesvermessung und Geobasisinformation Brandenburg kann Ausnahmen von den Absätzen 3 und 4 bei einem geringen Anteil der Geobasisinformationen am Geodatenendprodukt der Nutzerinnen oder des Nutzers oder einem untergeordneten Anteil der Nutzung der Geobasisinformationen in den Allgemeinen Geschäfts- und Nutzungsbedingungen der Landesvermessung und Geobasisinformation Brandenburg regeln.

#### § 4 Haftungsbeschränkung

Verletzt die geodatenhaltende Stelle eine ihr gegenüber der Nutzerin oder dem Nutzer obliegende öffentlich-rechtliche Pflicht, so haftet ihr Träger der Nutzerin oder dem Nutzer für den daraus entstehenden Schaden nicht, wenn der geodatenhaltenden Stelle lediglich Fahrlässigkeit zur Last fällt.
Dies gilt nicht im Falle einer Verletzung des Lebens, des Körpers und der Gesundheit.

#### § 5 Besondere Regelungen für Dienstleistungen

(1) Abweichend von § 1 werden für die Bereitstellung von Geobasisinformationen auf Datenträgern oder in anderen als den in der Anlage aufgeführten Datenformaten Entgelte erhoben.
Näheres regelt das Vermessungsentgeltverzeichnis in der jeweils gültigen Fassung.

(2) Für die Bereitstellung und Nutzung von Datenbeständen mit besonderen Qualitäts- und Aktualitätsanforderungen insbesondere auf Anforderung spezifischer Nutzergruppen oder vertraglicher Gestaltung kann ein besonderes Entgelt gemäß dem Vermessungsentgeltverzeichnis in der jeweils gültigen Fassung erhoben werden.

#### § 6 Besondere Regelungen für die Landesverwaltung

(1) Nach § 61 Absatz 3 der Landeshaushaltsordnung in der Fassung der Bekanntmachung vom 21. April 1999 (GVBl.
I S.
106), die zuletzt durch Artikel 1 des Gesetzes vom 5.
Juni 2019 (GVBl. I Nr. 20) geändert worden ist, sind der Wert der abgegebenen Vermögensgegenstände und die Aufwendungen stets zu erstatten, wenn Landesbetriebe oder Sondervermögen des Landes beteiligt sind.
Die Höhe der Aufwendungen für die Bereitstellung der Geobasisinformationen hängen vom Umfang und der Häufigkeit der Bereitstellung ab.

(2) Für die Erstattung gemäß Absatz 1 durch Behörden der Landesverwaltung erhebt die Landesvermessung und Geobasisinformation Brandenburg für die Bereitstellung der Geobasisinformationen nach § 1 inklusive Beratung ein pauschales Entgelt für die umfängliche Nutzung je Geschäftsbereich entsprechend den Regelungen des Vermessungsentgeltverzeichnisses in der jeweils geltenden Fassung.

(3) Soweit aus den Aufgaben eines Geschäftsbereichs ein deutlich verminderter Bedarf an Geobasisinformationen nach Absatz 2 erkennbar ist und sich daraus sowohl ein deutlich verminderter Nutzungsumfang als auch eine deutlich verminderte Nutzungshäufigkeit ergeben, sind verminderte Erstattungen vorzusehen.
Die Landesvermessung und Geobasisinformation Brandenburg kalkuliert die Erstattungshöhe aufgrund von Erfahrungswerten und einem antizipierten Nutzungsverhalten im Einvernehmen mit ihrer obersten Landesbehörde (Aufsichtsbehörde) und im Benehmen mit den jeweiligen Ressorts.
Die verminderten pauschalen Entgelte sind dem Vermessungsentgeltverzeichnis in der jeweils geltenden Fassung zu entnehmen.

(4) Die Erstattungsbeträge werden jährlich erhoben.

#### § 7 Bereitstellung von Geobasisinformationen für Landkreise, Ämter, Verbandsgemeinden, amtsfreie Gemeinden und berechtigte Stellen

(1) Ergänzend zu § 2 erhalten Landkreise, Ämter, Verbandsgemeinden und amtsfreie Gemeinden die Geobasisinformationen des Liegenschaftskatasters mit personenbezogenen Daten (Eigentümerdaten) für ihre Gebietskörperschaft entgeltfrei.

(2) Berechtigte Stellen erhalten die Geobasisinformationen des Liegenschaftskatasters mit personenbezogenen Daten (Eigentümerdaten) entgeltfrei, sofern das berechtigte Interesse an den personenbezogenen Daten ausdrücklich dargelegt wurde.

#### § 8 Übergangsregelung

Wenn für die Bereitstellung und Nutzung von Geobasisinformationen vor Inkrafttreten dieser Verordnung ein Entgelt erhoben wurde, besteht mit Inkrafttreten dieser Verordnung kein Anspruch auf vollständige oder teilweise Erstattung des Entgeltes.

#### § 9 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 23.
Dezember 2019

Der Minister des Innern und für Kommunales

Michael Stübgen

* * *

### Anlagen

1

[Anlage zur BbgGeoNutzV](/br2/sixcms/media.php/68/GVBl_II_02_2020-Anlage.pdf "Anlage zur BbgGeoNutzV") 258.3 KB