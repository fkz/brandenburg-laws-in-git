## Verordnung zur Bestimmung von Gewässern und Gewässerabschnitten für die Ausweisung von Überschwemmungsgebieten (Überschwemmungsgebietsgewässer-Bestimmungsverordnung - ÜSGGewBestV)

Auf Grund des § 100 Absatz 2 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20), der durch Artikel 1 Nummer 42 des Gesetzes vom 4. Dezember 2017 (GVBl.
I Nr.
28) neu gefasst worden ist, verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft des Landes Brandenburg:

#### § 1 Gewässer und Gewässerabschnitte, an denen Überschwemmungsgebiete auszuweisen sind

Gewässer und Gewässerabschnitte, an denen Überschwemmungsgebiete auszuweisen sind, sind die in der Anlage zu dieser Verordnung genannten Gewässer und Gewässerabschnitte.
Die Anlage ist Bestandteil dieser Verordnung.

#### § 2 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung zur Bestimmung hochwassergeneigter Gewässer und Gewässerabschnitte vom 17. Dezember 2009 (GVBl. II Nr. 47) außer Kraft.

Potsdam, den 18.
März 2019

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage (zu § 1) - Gewässer und Gewässerabschnitte, an denen Überschwemmungsgebiete auszuweisen sind](/br2/sixcms/media.php/68/GVBl_II_21_2019-Anlage.pdf "Anlage (zu § 1) - Gewässer und Gewässerabschnitte, an denen Überschwemmungsgebiete auszuweisen sind") 309.7 KB