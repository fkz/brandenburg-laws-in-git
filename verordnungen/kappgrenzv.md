## Verordnung zur Bestimmung der Gebietskulisse zur Senkung der Kappungsgrenze gemäß § 558 Absatz 3 des Bürgerlichen Gesetzbuchs (Kappungsgrenzenverordnung - KappGrenzV)

Auf Grund des § 558 Absatz 3 Satz 3 des Bürgerlichen Gesetzbuchs in der Fassung der Bekanntmachung vom 2.
Januar 2002 (BGBl.
I S. 42, 2909; 2003 I S.
738), der durch Artikel 1 Nummer 7 Buchstabe b des Gesetzes vom 11.
März 2013 (BGBl.
I S.
434) geändert worden ist, verordnet die Landesregierung:

#### § 1 Gebiete

Gemeinden im Sinne des § 558 Absatz 3 Satz 2 des Bürgerlichen Gesetzbuchs, in denen die ausreichende Versorgung der Bevölkerung mit Mietwohnungen zu angemessenen Bedingungen besonders gefährdet und die Kappungsgrenze auf 15 Prozent begrenzt ist, sind:

 

**Gemeinde**

**Kreisfreie Stadt**

Potsdam

**In den Landkreisen**

 

Barnim

Panketal

Dahme-Spreewald

Eichwalde  
Schulzendorf

Havelland

Falkensee

Märkisch-Oderland

Hoppegarten  
Neuenhagen bei Berlin

Oberhavel

Birkenwerder  
Glienicke/Nordbahn  
Hohen Neuendorf  
Mühlenbecker Land

Oder-Spree

Gosen-Neu Zittau  
Schöneiche bei Berlin  
Woltersdorf

Potsdam-Mittelmark

Kleinmachnow  
Stahnsdorf  
Teltow

Teltow-Fläming

Blankenfelde-Mahlow  
Großbeeren

#### § 2 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2021 in Kraft und mit Ablauf des 31. Dezember 2025 außer Kraft.

Potsdam, den 10.
März 2021

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Minister für Infrastruktur und Landesplanung

Guido Beermann