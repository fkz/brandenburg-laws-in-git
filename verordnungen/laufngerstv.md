## Verordnung über die Kostenerstattung nach dem Landesaufnahmegesetz für die Aufnahme von Flüchtlingen, spätausgesiedelten und weiteren aus dem Ausland zugewanderten Personen (Landesaufnahmegesetz-Erstattungsverordnung - LAufnGErstV)

Auf Grund des § 14 Absatz 6, des § 16 Satz 2 und des § 20 Absatz 3 des Landesaufnahmegesetzes vom 15.
März 2016 (GVBl.
I Nr. 11) verordnet die Ministerin für Arbeit, Soziales, Gesundheit, Frauen und Familie im Einvernehmen mit dem Minister des Innern und für Kommunales und dem Minister der Finanzen:

### Abschnitt 1  
Allgemeine Bestimmungen

#### § 1 Erstattungsverfahren

Die Erstattung erfolgt jährlich auf Antrag der Landkreise und kreisfreien Städte durch Erstattungsbescheid der Erstattungsbehörde.
Der Abrechnungszeitraum entspricht dem Kalenderjahr.
Der Antrag ist auf den von der Erstattungsbehörde vorgegebenen Antragsformularen jeweils bis zum 15. April des folgenden Kalenderjahres bei der Erstattungsbehörde einzureichen.

#### § 2 Abschlagszahlungen

(1) Auf Antrag können Abschlagszahlungen gewährt werden.
Der Antrag ist auf den von der Erstattungsbehörde vorgegebenen Antragsformularen unter Einhaltung der dort angegebenen Fristen bei der Erstattungsbehörde zu stellen.

(2) Für die Pauschalen nach den §§ 5 bis 8 erfolgen die Abschlagszahlungen

1.  bezogen auf die §§ 5, 6 und 8 vierteljährlich zur Quartalsmitte in Höhe von 95 Prozent des zu erwartenden Erstattungsbetrages,
2.  bezogen auf § 7 in Höhe von 2,8 Prozent der quartalsweisen Abschlagszahlung für die Pauschalen nach den §§ 5 und 8.

(3) In den Fällen von § 10 Nummer 1 bis 4 beträgt die Abschlagszahlung grundsätzlich pro Quartal und Leistungsempfänger oder Leistungsempfängerin insgesamt 500 Euro.

#### § 3 Prüfungsrecht

Die Landkreise und kreisfreien Städte sind verpflichtet, der Erstattungsbehörde auf Verlangen das Vorliegen der tatsächlichen Erstattungsvoraussetzungen nachzuweisen.
Von der Erstattungsbehörde können zur Feststellung der Ordnungsmäßigkeit der Kostenerstattungsansprüche begründende Unterlagen vor Ort eingesehen oder angefordert werden.

### Abschnitt 2  
Erstattungspauschalen

#### § 4 Einmalige Erstattungspauschale

(1) Die Pauschale nach § 14 Absatz 1 des Landesaufnahmegesetzes beträgt 2 328 Euro.
Die Pauschale wird für jede Person einmalig gewährt.

(2) Für die Gewährung der Pauschale nach Absatz 1 geben die Landkreise und kreisfreien Städte fortlaufend die Anzahl der nach § 4 Nummer 1 und 2 des Landesaufnahmegesetzes neu aufgenommenen Personen an.

#### § 5 Jährliche Erstattungspauschale für Leistungen nach den §§&nbsp;2 und 3 des Asylbewerberleistungsgesetzes

(1) Die jährliche Pauschale nach § 14 Absatz 2 Satz 1 des Landesaufnahmegesetzes wird nach Maßgabe der An-lage 1 gewährt.
Die Pauschale beinhaltet die Leistungen nach den §§ 2 und 3 des Asylbewerberleistungsgesetzes.
Bezogen auf die Leistungen nach § 3 des Asylbewerberleistungsgesetzes sind die notwendigen Bedarfe an

1.  Ernährung,
2.  Unterkunft und Heizung,
3.  Kleidung,
4.  Mitteln zur Gesundheitspflege,
5.  Gebrauchs- und Verbrauchsgütern des Haushalts,
6.  Leistungen zur Deckung persönlicher Bedürfnisse des täglichen Lebens (Taschengeld)

berücksichtigt.
Bei den Unterkünften nach Satz 3 Nummer 2 wird zwischen Gemeinschaftsunterkünften, Wohnungsverbünden und Übergangswohnungen unterschieden.
Bei Gemeinschaftsunterkünften und Wohnungsverbünden beinhaltet die Pauschale die Personalkosten für die Heimleitung und den Service.

(2) Soweit der Beginn des Erstattungszeitraums nicht zum 1.
Januar eines Jahres erfolgt, ist die Pauschale nach Absatz 1 für jeden Tag des Nichtleistungsbezugs um ein Dreihundertsechzigstel zu kürzen.
Soweit die Beendigung des Erstattungszeitraums nicht zum 31.
Dezember eines Jahres erfolgt, findet Satz 1 entsprechende Anwendung.

(3) Soweit Leistungsbeziehende keine Unterkunftsleistung in Einrichtungen zur vorläufigen Unterbringung in Anspruch nehmen, beträgt die jährliche Pauschale nach Absatz 1 pro Person 4 089 Euro (§ 14 Absatz 2 Satz 2 des Landesaufnahmegesetzes).
Bei einer vorübergehenden Abwesenheit von weniger als 31 Tagen erfolgt keine Kürzung der Pauschale.
Dies gilt ebenso, wenn die Abwesenheit durch Krankheit oder vergleichbare Gründe gerechtfertigt ist.

(4) Die Verrechnung der Pauschale nach Absatz 1 ist bei Vorliegen der folgenden Fälle vorzunehmen:

1.  Einnahmen aus Nutzungsentgelten nach § 11 Absatz 2 des Landesaufnahmegesetzes,
2.  Erstattungsleistungen von Leistungsberechtigten,
3.  verringerte Leistungen aufgrund einzusetzendem Einkommen oder Vermögen nach § 7 Absatz 1 des Asylbewerberleistungsgesetzes oder
4.  sonstige Erstattungsansprüche für nach dem Landesaufnahmegesetz erbrachte Leistungen gegenüber Leistungsbeziehenden oder Dritten.

Die Verrechnung erfolgt durch Abzug der realisierten Einnahme von der jeweiligen Pauschale.

(5) Die Landkreise und kreisfreien Städte geben monatlich fortlaufend an:

1.  die Form, die Kapazität und die Belegung der Einrichtungen der vorläufigen Unterbringung,
2.  die Anzahl der Personen, die Leistungen nach dem Asylbewerberleistungsgesetz beziehen, und
3.  das tatsächlich beschäftigte Personal (Tätigkeit, Eingruppierung) nach § 5 Absatz 1 Satz 5.

Abschlagszahlungen nach § 2 werden nicht geleistet, solange diese Angaben nicht vorliegen.

(6) Für die Gewährung der Pauschale sind die in Abschnitt 2 der Landesaufnahmegesetz-Durchführungsverordnung geregelten Mindestbedingungen einzuhalten.
Die Erstattungsbehörde kann die Pauschale in Fällen des Abweichens von den Mindestbedingungen angemessen kürzen.

#### § 6 Erstattungspauschalen für Migrationssozialarbeit

(1) Für die Aufgabenwahrnehmung der sozialen Unterstützung durch Migrationssozialarbeit nach § 12 des Landesaufnahmegesetzes sowie Abschnitt 3 der Landesaufnahmegesetz-Durchführungsverordnung erhalten die Landkreise und kreisfreien Städte nach § 14 Absatz 3 Satz 1 und Absatz 3a des Landesaufnahmegesetzes eine jährliche Pauschale.
Zusätzlich werden Erstattungspauschalen nach § 14 Absatz 3 Satz 2 des Landesaufnahmegesetzes gewährt für

1.  ein kontinuierliches Angebot von Migrationssozialarbeit als Fachberatungsdienst in den Landkreisen und kreisfreien Städten,
2.  die landesweite Beratung von Personen jüdischen Glaubens nach § 4 Nummer 2 des Landesaufnahmegesetzes und
3.  die Migrationssozialarbeit für Personen, die im Rahmen der Allgemeinen Weisung des Ministeriums des Innern und für Kommunales vom 24. Januar 2019 (AW-AuslR 2019.02) im Besitz einer Aufenthaltserlaubnis nach § 23 Absatz 1 des Aufenthaltsgesetzes sind.

Die Pauschalen nach den Sätzen 1 und 2 werden nach Maßgabe der Anlage 2 gewährt.

(2) Für die Gewährung der Pauschalen teilen die Landkreise und kreisfreien Städte der Erstattungsbehörde Folgendes schriftlich mit:

1.  die Anzahl und den Beschäftigungsumfang des in diesem Aufgabengebiet beschäftigten Personals,
2.  die jeweilige Qualifikation entsprechend den Qualifikationsanforderungen in Nummer 3.5 der Anlage 4 der Landesaufnahmegesetz-Durchführungsverordnung und
3.  soweit vorhanden, die mit den beauftragten Dritten getroffenen Vergütungsvereinbarungen mit der vereinbarten Vergütungshöhe für das beschäftigte Personal.

Für die Gewährung der Pauschalen nach Absatz 1 Satz 2 Nummer 3 hat die Mitteilung gesondert zu erfolgen.

(3) Abschlagszahlungen nach § 2 werden nicht geleistet, soweit die Voraussetzungen nach Absatz 2 nicht erfüllt sind.

_(4) (aufgehoben)_

(5) Für die Gewährung der Pauschalen nach Absatz 1 Satz 2 Nummer 3 teilen die Landkreise und kreisfreien Städte der Erstattungsbehörde die Anzahl der in ihren Zuständigkeitsbereich im Erstattungsjahr wohnhaften Personen, die dem Personenkreis des Absatz 1 Satz 2 Nummer 3 angehören, nach den Vorgaben der Erstattungsbehörde mit.
Eine Erstattung kann auch erfolgen, wenn für diesen Personenkreis Aufgaben der Migrationssozialarbeit im Erstattungsjahr trotz Wohnsitzwechsels in den Zuständigkeitsbereich eines anderen Landkreises oder einer anderen kreisfreien Stadt erbracht wurden.
Die Voraussetzungen des Satzes 2 sind der Erstattungsbehörde nach deren Vorgaben nachzuweisen.

(6) Sofern die Landkreise und kreisfreien Städte der Erstattungsbehörde durch Mitteilung nach Absatz 2 Satz 2 nachweisen, dass ihnen zur Vorbereitung der Aufnahme und zielgruppenspezifischen sozialen Unterstützung durch Migrationssozialarbeit des Personenkreises nach Absatz 1 Satz 2 Nummer 3 Personal- und Sachkosten entstanden sind, erhalten diese für jeden vollen Monat jeweils entsprechend der Anzahl der aufzunehmenden Personen ein Zwölftel der Pauschalen nach Nummer 7 der Anlage 2 sowie entsprechend der Anzahl und des Beschäftigungsumfangs des eingesetzten Personals bis zu einem Zwölftel der Pauschale nach Nummer 8 der Anlage 2.
Erfolgt die Aufnahme des Personenkreises nach Absatz 1 Satz 2 Nummer 3 nicht zum 1.
Januar des Erstattungsjahres, werden die Pauschalen nach Nummer 7 und 8 der Anlage 2 bis zum Aufnahmemonat für jeden vollen Monat um ein Zwölftel der Pauschalen gekürzt.

#### § 7 Verwaltungskostenpauschale

Die jährliche Verwaltungskostenpauschale nach § 14 Absatz 4 des Landesaufnahmegesetzes beträgt 2,8 Prozent der Erstattungsleistungen, die nach den §§ 4, 5, 8, 9 und 10 Nummer 2 bis 5 gewährt werden.

#### § 8 Sicherheitspauschale

(1) Die Pauschale nach § 14 Absatz 5 des Landesaufnahmegesetzes bemisst sich nach dem jeweils monatlich pro als Gemeinschaftsunterkunft oder Wohnungsverbund genutzter Liegenschaft erforderlichen Umfang an Sicherheitsmaßnahmen.
Für jede erforderliche Bewachungsstunde werden pauschal 19,22 Euro erstattet.
Die Pauschale berücksichtigt Personal- und Sachkosten.

(2) Die Kostenerstattung erstreckt sich auch auf den Zeitraum vor der Inbetriebnahme als Einrichtung der vorläufigen Unterbringung.
Ein Zeitraum von drei Monaten sollte dabei nicht überschritten werden.

(3) Mit der Antragstellung ist gegenüber der Erstattungsbehörde die Erforderlichkeit der Sicherheitsmaßnahmen darzulegen und nachzuweisen.
Dies erfolgt regelmäßig durch ein von der örtlich zuständigen Polizeidienststelle bestätigtes Sicherheitskonzept.

#### § 9 Investitionspauschale

(1) Die Pauschale nach § 14 Absatz 6 Satz 1 des Landesaufnahmegesetzes beträgt für die erstmalige Bereitstellung von Unterbringungsplätzen in einer Einrichtung der vorläufigen Unterbringung (Gemeinschaftsunterkunft, Wohnungsverbund oder Übergangswohnung) pro Platz einmalig 2 300,81 Euro.

(2) Mit der Antragstellung ist gegenüber der Erstattungsbehörde die erstmalige Bereitstellung der Unterbringungsplätze durch geeignete Unterlagen darzulegen und nachzuweisen.

(3) Die erstmalige Bereitstellung von Unterbringungsplätzen liegt insbesondere dann vor, wenn die Unterbringungsplätze

1.  neu geschaffen werden oder
2.  erstmalig angemietet oder speziell für eine entsprechende Unterbringung hergerichtet werden.

(4) Die Gewährung der Pauschale nach Absatz 1 setzt voraus, dass der Unterbringungsplatz der vorläufigen Unterbringung im Sinne von § 9 Absatz 1 Satz 1 des Landesaufnahmegesetzes dient.
Soweit kein Wechsel des Bewohners oder der Bewohnerin erfolgt, ist eine anderweitige Nutzung des Wohnraums unschädlich.

(5) Zur Schaffung besonderer, beispielsweise behindertengerechter, Unterbringungsplätze oder zur Gewährleistung von Barrierefreiheit im Sinne von § 14 Absatz 6 Satz 2 des Landesaufnahmegesetzes kann auf Antrag pro Platz bis zu 9 500 Euro erstattet werden.
Dies liegt insbesondere bei der Schaffung baulicher Barrierefreiheit vor.
Voraussetzung für die Erstattung ist, dass ein entsprechender Bedarf an zielgruppenspezifischen oder individuellen personenbezogenen Unterbringungsplätzen besteht.

### Abschnitt 3  
Erstattung nach Kostennachweis

#### § 10 Erstattungstatbestände

In den folgenden Fällen werden den Landkreisen und kreisfreien Städten die notwendigen tatsächlichen Kosten erstattet:

1.  Gesundheitskosten nach § 15 Absatz 1 des Landesaufnahmegesetzes,
2.  sonstige Leistungen nach § 15 Absatz 2 des Landesaufnahmegesetzes,
3.  Leistungen für Bildung und Teilhabe nach § 15 Absatz 3 des Landesaufnahmegesetzes,
4.  weitergehende Leistungen aufgrund besonderer Bedarfslagen nach § 15 Absatz 4 des Landesaufnahmegesetzes,
5.  Vorhaltekosten nach § 15 Absatz 5 des Landesaufnahmegesetzes.

#### § 11 Erstattungsverfahren

(1) Die Landkreise und kreisfreien Städte haben die ihnen in den Fällen von § 10 Nummer 1 bis 5 entstandenen Kosten zu erfassen und in einer Übersicht nach den Vorgaben der Erstattungsbehörde darzustellen.

(2) Zur Erstattung der Kosten ist die Übersicht nach Absatz 1 zusammen mit dem Antrag auf Erstattung nach § 1 Satz 3 der Erstattungsbehörde einzureichen.

#### § 12 Gesundheitskosten

(1) Die Erstattung der Kosten nach § 10 Nummer 1 erfolgt für die Übernahme der Krankenbehandlung

1.  nach § 264 Absatz 1 des Fünften Buches Sozialgesetzbuch und
2.  nach § 264 Absatz 2 des Fünften Buches Sozialgesetzbuch

jeweils auf der Grundlage der quartalsweisen Abrechnungen der Krankenkassen und Ersatzkassen.
Diese Abrechnungen sind bei der Erstattungsbehörde zusammen mit dem Antrag nach § 1 Satz 3 einzureichen.

(2) Bei den nach Absatz 1 abgerechneten Leistungen wird widerlegbar vermutet, dass diese notwendig waren.
Soweit den Landkreisen und kreisfreien Städten im Rahmen der Leistungserbringung keine Ermessensausübung zustand, besteht kein Prüfungsvorbehalt der Erstattungsbehörde gegenüber den Landkreisen und kreisfreien Städten.

### Abschnitt 4  
Sonstige Bestimmungen

#### § 13 Fortschreibung der Erstattungspauschalen

(1) Die Pauschale nach § 4 Absatz 1 erhöht sich zum 1.
Januar eines jeden Jahres entsprechend dem durchschnittlichen Verbraucherpreisindex für das Land Brandenburg.

(2) Die Pauschale nach § 5 Absatz 1 wird unter Berücksichtigung

1.  der Fortschreibung der Leistungssätze nach § 3 Absatz 4 des Asylbewerberleistungsgesetzes sowie
2.  der jeweils geltenden Personaldurchschnittskosten für Tarifbeschäftigte des Landes Brandenburg

zum 1.
Januar eines jeden Jahres fortgeschrieben.

(3) Die Pauschale nach § 5 Absatz 3 wird unter Berücksichtigung der Fortschreibung der Leistungssätze nach § 3 Absatz 4 des Asylbewerberleistungsgesetzes zum 1. Januar eines jeden Jahres fortgeschrieben.

(4) Die Pauschalen nach § 6 werden unter Berücksichtigung der jeweils geltenden Personaldurchschnittskosten für Tarifbeschäftigte des Landes Brandenburg zum 1. Januar eines jeden Jahres fortgeschrieben.

(5) Bei der Pauschale nach § 8 Absatz 1 Satz 2 erfolgt eine Anpassung zum 1.
Januar eines jeden Jahres entsprechend dem jeweiligen Abschluss des Entgelttarifvertrages für Sicherheitsdienstleistungen Berlin und Brandenburg.

(6) Die nach den Absätzen 1 bis 5 errechneten Pauschalen werden jährlich bis zum 30.
Juni des laufenden Kalenderjahres im Amtsblatt für Brandenburg bekannt gegeben.

#### § 14 Mehrbelastungsausgleich Landesaufnahmeprogramm Nordirak

Notwendige Mehrkosten, die den Landkreisen und kreisfreien Städten insbesondere zur Sicherstellung der Einhaltung der besonderen Anforderungen an die vorläufige Unterbringung nach § 8a der Landesaufnahmegesetz-Durchführungsverordnung entstanden sind, werden auf Antrag nach Kostennachweis erstattet.

#### § 15 Inkrafttreten, Außerkrafttreten

(1) Diese Verordnung tritt vorbehaltlich der Absätze 2 und 3 mit Wirkung vom 1.
April 2016 in Kraft.

(2) § 6 Absatz 2 tritt am Tag nach der Verkündung der Landesaufnahmegesetz-Durchführungsverordnung in Kraft.

(3) § 13 tritt am 1.
Januar 2017 in Kraft.

(4) Die Erstattungsverordnung vom 29.
Januar 1999 (GVBl.
II S. 99), die zuletzt durch die Verordnung vom 30.
August 2013 (GVBl.
II Nr. 66) geändert worden ist, tritt mit Wirkung vom 1.
April 2016 außer Kraft.

Potsdam, den 20.
Oktober 2016

Die Ministerin für Arbeit, Soziales,  
Gesundheit, Frauen und Familie

Diana Golze

* * *

### Anlagen

1

[Anlage 1 - Jährliche Erstattungspauschale](/br2/sixcms/media.php/68/GVBl_II_56_2016-Anlage-1.pdf "Anlage 1 - Jährliche Erstattungspauschale") 589.0 KB

2

[Anlage 2 - Migrationssozialarbeit](/br2/sixcms/media.php/68/LAufnGErstV-Anlage-2.pdf "Anlage 2 - Migrationssozialarbeit") 258.8 KB