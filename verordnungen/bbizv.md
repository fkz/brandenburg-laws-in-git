## Verordnung über Zuständigkeiten für die Berufsbildung nach dem Berufsbildungsgesetz, der Handwerksordnung, der Ausbilder-Eignungsverordnung und dem Berufsqualifikationsfeststellungsgesetz im Land Brandenburg (Berufsbildungszuständigkeitsverordnung - BBiZV)

Auf Grund

*   des § 7 Absatz 1 Satz 2 und des § 105 des Berufsbildungsgesetzes vom 23.
    März 2005 (BGBl.
    I S. 931),
*   des § 124b Satz 1 der Handwerksordnung in der Fassung der Bekanntmachung vom 24. September 1998 (BGBl.
    I S. 3074; 2006 I S. 2095), der durch Artikel 2 Nummer 33 des Gesetzes vom 23.
    März 2005 (BGBl.
    I S. 931, 952) neu gefasst worden ist,
*   des § 8 Absatz 4 des Berufsqualifikationsfeststellungsgesetzes vom 6.
    Dezember 2011 (BGBl. I S. 2515),
*   des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
    Februar 1987 (BGBl.
    I S. 602),
*   des § 6 Absatz 1 in Verbindung mit § 5 Absatz 1 des Landesorganisationsgesetzes vom 24. Mai 2004 (GVBl.
    I S. 186), die durch Artikel 2 des Gesetzes vom 10.
    Juli 2014 (GVBl. I Nr. 28 S. 2) geändert worden sind, in Verbindung mit § 82 Absatz 2 Satz 3 sowie Absatz 4 Satz 1 des Berufsbildungsgesetzes,
*   des § 6 Absatz 2 und 4 in Verbindung mit § 9 Absatz 1 Satz 3 und Absatz 2 Satz 2, § 12 Absatz 1 Satz 2 und § 13 Absatz 3 des Landesorganisationsgesetzes, von denen durch Artikel 2 des Gesetzes vom 10.
    Juli 2014 (GVBl.
    I Nr. 28 S. 2) § 6 Absatz 2 und 4 und § 13 Absatz 3 geändert und § 9 Absatz 1 Satz 3 und Absatz 2 Satz 2 sowie § 12 Absatz 1 Satz 2 eingefügt worden sind, in Verbindung mit § 40 Absatz 4 Satz 2, § 71 Absatz 8, § 73 Absatz 2, § 77 Absatz 2 und 3 Satz 2 des Berufsbildungsgesetzes sowie in Verbindung mit § 43 Absatz 2 Satz 2 und Absatz 3, § 34 Absatz 7 Satz 2 der Handwerksordnung sowie in Verbindung mit § 4 Absatz 5, § 6 Absatz 3 und 4 und den §§ 7 und 8 der Ausbilder-Eignungsverordnung vom 21.
    Januar 2009 (BGBl.
    I S. 88) sowie in Verbindung mit § 8 Absatz 2 des Berufsqualifikationsfeststellungsgesetzes

verordnet die Landesregierung:

#### § 1 Landesausschuss für Berufsbildung

Das für Arbeit zuständige Ministerium ist für die Festsetzung der Entschädigung und für die Genehmigung der Geschäftsordnung nach § 82 Absatz 2 Satz 3 und Absatz 4 Satz 1 des Berufsbildungsgesetzes des bei ihm errichteten Landessausschusses für Berufsbildung zuständig.

#### § 2 Berufsbildungsausschüsse

(1) Zuständige Stellen für die Errichtung von Berufsbildungsausschüssen nach § 77 Absatz 1 Satz 1 des Berufsbildungsgesetzes sind

1.  das für Inneres zuständige Ministerium für die anerkannten Ausbildungsberufe
    1.  Verwaltungsfachangestellte oder Verwaltungsfachangestellter der Fachrichtungen Landesverwaltung und Kommunalverwaltung,
    2.  Kauffrau oder Kaufmann für Büromanagement,
    3.  Fachangestellte oder Fachangestellter für Bäderbetriebe,
    4.  Vermessungstechnikerin oder Vermessungstechniker und
    5.  Geomatikerin oder Geomatiker;
2.  das für Inneres zuständige Ministerium im Einvernehmen mit dem für Wissenschaft zuständigen Ministerium für den anerkannten Ausbildungsberuf Fachangestellte oder Fachangestellter für Medien- und Informationsdienste;
3.  das für Inneres zuständige Ministerium im Einvernehmen mit dem für Verkehr zuständigen Ministerium für die anerkannten Ausbildungsberufe Straßenwärterin oder Straßenwärter sowie Fachkraft für Straßen- und Verkehrstechnik;
4.  die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts für den anerkannten Ausbildungsberuf Justizfachangestellte oder Justizfachangestellter;
5.  Handwerkskammern sowie Industrie- und Handelskammern für den anerkannten Ausbildungsberuf Verwaltungsfachangestellte oder Verwaltungsfachangestellter der Fachrichtung Handwerksorganisation und Industrie- und Handelskammern;
6.  die Allgemeine Ortskrankenkasse für das Land Brandenburg (AOK Nordost – Die Gesundheitskasse) für den anerkannten Ausbildungsberuf Sozialversicherungsfachangestellte oder Sozialversicherungsfachangestellter in der Fachrichtung gesetzliche Krankenversicherung;
7.  das Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung im Bereich der Land- und Hauswirtschaft.

(2) Zuständige Behörde für die Berufung der Mitglieder des Berufsbildungsausschusses der zuständigen Stellen nach § 77 Absatz 2 des Berufsbildungsgesetzes sowie nach § 43 Absatz 2 Satz 2 der Handwerksordnung ist

1.  im Bereich der Land- und Hauswirtschaft das Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung,
2.  in den übrigen Fällen das jeweils fachlich zuständige Ministerium.

#### § 3 Entschädigung für Berufsbildungs- und Prüfungsausschüsse

(1) Die für den jeweiligen Berufsbildungsausschuss zuständige Stelle setzt mit Genehmigung des Ministeriums, das über sie die Aufsicht führt, die Entschädigung für Tätigkeiten im Berufsbildungsausschuss nach § 77 Absatz 3 Satz 2 des Berufsbildungsgesetzes sowie nach § 43 Absatz 3 der Handwerksordnung fest.

(2) Die für den jeweiligen Prüfungsausschuss zuständige Stelle setzt mit Genehmigung des Ministeriums, das über sie die Aufsicht führt, die Entschädigung für Tätigkeiten im Prüfungsausschuss nach § 40 Absatz 4 Satz 2 des Berufsbildungsgesetzes sowie nach § 34 Absatz 7 Satz 2 der Handwerksordnung fest.

#### § 4 Übertragung der Verordnungsermächtigung nach §&nbsp;7 Absatz&nbsp;1 Satz&nbsp;1 des Berufsbildungsgesetzes

Das für Arbeit zuständige Mitglied der Landesregierung wird ermächtigt, nach Anhörung des Landesausschusses für Berufsbildung durch Rechtsverordnung zu bestimmen, dass der Besuch eines Bildungsganges berufsbildender Schulen oder die Berufsausbildung in einer sonstigen Einrichtung ganz oder teilweise auf die Ausbildungszeit angerechnet wird.

#### § 5 Zuständige Behörde für die Überwachung, Untersagung und Zuerkennung der Eignung

Zuständige Behörde für

*   die Anerkennung der Eignung von Ausbildungsstätten gemäß § 27 Absatz 3 Satz 1 und § 27 Absatz 4 Satz 1 des Berufsbildungsgesetzes,
*   die widerrufliche Zuerkennung der fachlichen Eignung nach § 30 Absatz 6 des Berufsbildungsgesetzes und § 22b Absatz 5 der Handwerksordnung,
*   die Entgegennahme der Mitteilung über die mangelnde Eignung nach § 32 Absatz 2 Satz 2 des Berufsbildungsgesetzes und § 23 Absatz 2 Satz 2 der Handwerksordnung,
*   die Untersagung des Einstellens und des Ausbildens bei fehlender Eignung nach § 33 Absatz 1 und 2 des Berufsbildungsgesetzes und § 24 Absatz 1 und 2 der Handwerksordnung,
*   die Untersagung von Berufsausbildungsvorbereitungen nach § 70 Absatz 1 des Berufsbildungsgesetzes und § 42q der Handwerksordnung

ist für die anerkannten Ausbildungsberufe

1.  im Bereich der Land- und Hauswirtschaft das Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung;
2.  im Bereich der beruflichen Erstausbildung und beruflichen Weiterbildung (ohne Meisterinnen- und Meisterqualifizierung im Handwerk oder der Industrie) für den Bereich des Ministeriums für Arbeit, Soziales, Gesundheit, Frauen und Familie das Landesamt für Soziales und Versorgung;
3.  des öffentlichen Dienstes das Ministerium, das über die für die Überwachung der Eignung nach § 32 des Berufsbildungsgesetzes zuständige Stelle die Aufsicht führt;
4.  Fachangestellte oder Fachangestellter im Bereich der Steuerberatung die nach § 71 Absatz 5 des Berufsbildungsgesetzes zuständige Stelle;
5.  in den übrigen Fällen das Ministerium, das über die jeweils nach § 71 des Berufsbildungsgesetzes zuständige Stelle die Aufsicht führt.

#### § 6 Genehmigung der Prüfungsordnung

Zuständige oberste Landesbehörde für die Genehmigung von Prüfungsordnungen im Sinne des § 47 Absatz 1 Satz 2 des Berufsbildungsgesetzes sowie des § 38 Absatz 1 Satz 2 der Handwerksordnung ist das Ministerium, das für die Aufsicht zuständig ist.
Sofern dieses gemäß § 7 zuständige Stelle für den Erlass der Prüfungsordnung ist, bedarf es entsprechend § 81 Absatz 2 des Berufsbildungsgesetzes keiner Genehmigung.

#### § 7 Zuständige Stellen im öffentlichen Dienst

(1) Zuständige Stellen im Sinne des § 73 Absatz 2 des Berufsbildungsgesetzes sind für die anerkannten Ausbildungsberufe

1.  Verwaltungsfachangestellte oder Verwaltungsfachangestellter der Fachrichtung Landesverwaltung
    1.  das für Inneres zuständige Ministerium für Regelungen zur Durchführung der Berufsausbildung nach § 9 des Berufsbildungsgesetzes,
    2.  die Landesakademie für öffentliche Verwaltung des Landes Brandenburg in den übrigen Fällen;
2.  Verwaltungsfachangestellte oder Verwaltungsfachangestellter der Fachrichtung Kommunalverwaltung
    1.  das für Inneres zuständige Ministerium für Regelungen zur Durchführung der Berufsausbildung nach § 9 des Berufsbildungsgesetzes,
    2.  die Landesakademie für öffentliche Verwaltung des Landes Brandenburg für
        
        aa)
        
        das Einrichten und Führen des Verzeichnisses nach § 34 des Berufsbildungsgesetzes,
        
        bb)
        
        die Auskünfte nach den §§ 86 und 88 des Berufsbildungsgesetzes,
        
    3.  die Gemeinden und Gemeindeverbände, soweit sie Mitglied eines Zweckverbandes für Berufsbildung im Bereich der kommunalen Verwaltung sind, in den übrigen Fällen;
3.  Verwaltungsfachangestellte oder Verwaltungsfachangestellter der Fachrichtung Handwerksorganisation und Industrie- und Handelskammern
    1.  soweit bei Handwerkskammern sowie Industrie- und Handelskammern ausgebildet wird, die Landesakademie für öffentliche Verwaltung des Landes Brandenburg für
        
        aa)
        
        die Überwachung der Eignung, deren Feststellung sowie die Aufforderung zur Behebung von Mängeln nach § 32 des Berufsbildungsgesetzes,
        
        bb)
        
        die Anhörung nach § 33 Absatz 3 des Berufsbildungsgesetzes,
        
        cc)
        
        das Einrichten und Führen des Verzeichnisses nach § 34 des Berufsbildungsgesetzes,
        
        dd)
        
        die Überwachung der Durchführung der Berufsbildung nach § 76 des Berufsbildungsgesetzes,
        
        ee)
        
        die Auskünfte nach den §§ 86 und 88 des Berufsbildungsgesetzes,
        
    2.  soweit bei Kreishandwerkerschaften und Handwerksinnungen ausgebildet wird, die Aufsicht führende Handwerkskammer für
        
        aa)
        
        die Überwachung der Eignung, der Feststellung sowie die Aufforderung zur Behebung von Mängeln nach § 32 des Berufsbildungsgesetzes,
        
        bb)
        
        die Anhörung nach § 33 Absatz 3 des Berufsbildungsgesetzes,
        
        cc)
        
        das Einrichten und Führen des Verzeichnisses nach § 34 des Berufsbildungsgesetzes,
        
        dd)
        
        die Überwachung der Durchführung der Berufsbildung nach § 76 des Berufsbildungsgesetzes,
        
        ee)
        
        die Auskünfte nach den §§ 86 und 88 des Berufsbildungsgesetzes,
        
    3.  die Handwerkskammern sowie Industrie- und Handelskammern in den übrigen Fällen;
4.  Kauffrau oder Kaufmann für Büromanagement
    1.  die Gemeinden und Gemeindeverbände, soweit sie Mitglied eines Zweckverbandes für Berufsbildung im Bereich der kommunalen Verwaltung sind, wenn die Anstellungskörperschaft ein kommunaler Zweckverband, ein Landkreis, eine kreisfreie Stadt, eine Gemeinde oder ein Amt ist,
    2.  die Industrie- und Handelskammern in den übrigen Fällen;
5.  Fachangestellte oder Fachangestellter für Bäderbetriebe
    1.  das für Inneres zuständige Ministerium für Regelungen zur Durchführung der Berufsausbildung nach § 9 des Berufsbildungsgesetzes,
    2.  die Landesakademie für öffentliche Verwaltung des Landes Brandenburg in den übrigen Fällen;
6.  Vermessungstechnikerin oder Vermessungstechniker und Geomatikerin oder Geomatiker
    1.  das für Inneres zuständige Ministerium für Regelungen zur Durchführung der Berufsausbildung nach § 9 des Berufsbildungsgesetzes,
    2.  die Landesvermessung und Geobasisinformation Brandenburg in den übrigen Fällen;
7.  Fachangestellte oder Fachangestellter für Medien- und Informationsdienste
    1.  das für Wissenschaft zuständige Ministerium für
        
        aa)
        
        Regelungen zur Durchführung der Berufsausbildung nach § 9 des Berufsbildungsgesetzes,
        
        bb)
        
        den Erlass von Prüfungsordnungen nach § 47 des Berufsbildungsgesetzes,
        
        cc)
        
        den Erlass von Fortbildungsprüfungsregelungen nach § 54 des Berufsbildungsgesetzes,
        
        dd)
        
        den Erlass von Umschulungsprüfungsregelungen nach § 59 des Berufsbildungsgesetzes,
        
    2.  die Landesakademie für öffentliche Verwaltung des Landes Brandenburg für
        
        aa)
        
        die Errichtung der Prüfungsausschüsse nach § 39 des Berufsbildungsgesetzes,
        
        bb)
        
        die Berufung der Mitglieder der Prüfungsausschüsse nach § 40 Absatz 3 des Berufsbildungsgesetzes,
        
        cc)
        
        die Durchführung und Teilbefreiung von Fortbildungsprüfungen nach § 56 des Berufsbildungsgesetzes,
        
        dd)
        
        die Durchführung und Teilbefreiung von Umschulungsprüfungen nach § 62 Absatz 3 und 4 des Berufsbildungsgesetzes,
        
    3.  die Fachhochschule Potsdam in den übrigen Fällen;
8.  Straßenwärterin oder Straßenwärter und Fachkraft für Straßen- und Verkehrstechnik
    1.  das für Verkehr zuständige Ministerium für Regelungen zur Durchführung der Berufsausbildung nach § 9 des Berufsbildungsgesetzes,
    2.  der Landesbetrieb Straßenwesen in den übrigen Fällen;
9.  Justizfachangestellte oder Justizfachangestellter
    1.  das für Justiz zuständige Ministerium für Regelungen zur Durchführung der Berufsausbildung nach § 9 des Berufsbildungsgesetzes,
    2.  die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts in den übrigen Fällen;
10.  Sozialversicherungsfachangestellte oder Sozialversicherungsfachangestellter in der Fachrichtung gesetzliche Krankenversicherung die Allgemeine Ortskrankenkasse für das Land Brandenburg (AOK Nordost – Die Gesundheitskasse).

(2) Das jeweils fachlich zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung im Einvernehmen mit dem für Inneres zuständigen Mitglied der Landesregierung die zuständigen Stellen im Sinne des § 73 Absatz 2 des Berufsbildungsgesetzes für die anerkannten Ausbildungsberufe, die nicht in Absatz 1 geregelt sind, zu bestimmen.

#### § 8 Zuständige Stelle in den Bereichen der Land- und Hauswirtschaft

Zuständige Stelle im Sinne des Berufsbildungsgesetzes im Bereich der Land- und Hauswirtschaft ist das Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung.

#### § 9 Zuständigkeit bei Ordnungswidrigkeiten im Bereich der Land- und Hauswirtschaft

Zuständige Verwaltungsbehörde für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach dem Berufsbildungsgesetz im Bereich der Land- und Hauswirtschaft ist das Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung.

#### § 10 Zuständige Stellen im Sinne der Ausbilder-Eignungsverordnung

(1) Zuständige Stelle im Sinne des § 4 Absatz 5 der Ausbilder-Eignungsverordnung für den Erlass der Prüfungsordnung ist die Landesakademie für öffentliche Verwaltung des Landes Brandenburg in Abstimmung mit der nach § 7 für den Erlass der Prüfungsordnung zuständigen Stelle.
Dies gilt nicht

1.  für den Ausbildungsberuf Kauffrau oder Kaufmann für Büromanagement, wenn die Industrie- und Handelskammern zuständige Stellen sind,
2.  für den Ausbildungsberuf Sozialversicherungsfachangestellte oder Sozialversicherungsfachangestellter in der Fachrichtung gesetzliche Krankenversicherung und
3.  im Bereich der Land- und Hauswirtschaft,

für die in Absatz 2 die Zuständigkeiten für den Erlass der jeweiligen Prüfungsordnungen geregelt sind.

(2) Zuständige Stellen im Sinne der Ausbilder-Eignungsverordnung sind für die anerkannten Ausbildungsberufe

1.  Verwaltungsfachangestellte oder Verwaltungsfachangestellter der Fachrichtung Landesverwaltung, Fachangestellte oder Fachangestellter für Bäderbetriebe, Vermessungstechnikerin oder Vermessungstechniker und Geomatikerin oder Geomatiker die Landesakademie für öffentliche Verwaltung des Landes Brandenburg;
2.  Verwaltungsfachangestellte oder Verwaltungsfachangestellter der Fachrichtung Kommunalverwaltung die Gemeinden und Gemeindeverbände, soweit sie Mitglied eines Zweckverbandes für Berufsbildung im Bereich der kommunalen Verwaltung sind;
3.  Kauffrau oder Kaufmann für Büromanagement
    1.  die Gemeinden und Gemeindeverbände, soweit sie Mitglied eines Zweckverbandes für Berufsbildung im Bereich der kommunalen Verwaltung sind, wenn die Anstellungskörperschaft ein kommunaler Zweckverband, ein Landkreis, eine kreisfreie Stadt, eine Gemeinde oder ein Amt ist,
    2.  die Industrie- und Handelskammern in den übrigen Fällen;
4.  Fachangestellte oder Fachangestellter für Medien- und Informationsdienste die Landesakademie für öffentliche Verwaltung des Landes Brandenburg im Benehmen mit dem für Wissenschaft zuständigen Ministerium;
5.  Straßenwärterin oder Straßenwärter und Fachkraft für Straßen- und Verkehrstechnik die Landesakademie für öffentliche Verwaltung des Landes Brandenburg im Benehmen mit dem für Verkehr zuständigen Ministerium;
6.  Justizfachangestellte oder Justizfachangestellter die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts;
7.  Sozialversicherungsfachangestellte oder Sozialversicherungsfachangestellter in der Fachrichtung gesetzliche Krankenversicherung die Allgemeine Ortskrankenkasse für das Land Brandenburg (AOK Nordost – Die Gesundheitskasse);
8.  im Bereich der Land- und Hauswirtschaft das Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung.

(3) Das jeweils fachlich zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung im Einvernehmen mit dem für Inneres zuständigen Mitglied der Landesregierung die zuständigen Stellen im Sinne der Ausbilder-Eignungsverordnung für die anerkannten Ausbildungsberufe, die nicht in den Absätzen 1 und 2 geregelt sind, zu bestimmen.

#### § 11 Zuständige Stellen im Sinne des Berufsqualifikationsfeststellungsgesetzes

Zuständige Stellen im Sinne des Teils 2 Kapitel 1 des Berufsqualifikationsfeststellungsgesetzes sind für die anerkannten Ausbildungsberufe

1.  Verwaltungsfachangestellte oder Verwaltungsfachangestellter der Fachrichtung Landesverwaltung und Fachangestellte oder Fachangestellter für Bäderbetriebe die Landesakademie für öffentliche Verwaltung des Landes Brandenburg;
2.  Verwaltungsfachangestellte oder Verwaltungsfachangestellter der Fachrichtung Kommunalverwaltung die Gemeinden und Gemeindeverbände, soweit sie Mitglied eines Zweckverbandes für Berufsbildung im Bereich der kommunalen Verwaltung sind;
3.  Kauffrau oder Kaufmann für Büromanagement
    1.  die Gemeinden und Gemeindeverbände, soweit sie Mitglied eines Zweckverbandes für Berufsbildung im Bereich der kommunalen Verwaltung sind, wenn die anzuerkennende Qualifikation im Schwerpunkt dem Tätigkeitsbereich der Kommunalverwaltung zugeordnet werden kann,
    2.  die Industrie- und Handelskammern in den übrigen Fällen;
4.  Verwaltungsfachangestellte oder Verwaltungsfachangestellter der Fachrichtung Handwerksorganisation und Industrie- und Handelskammern die Handwerkskammern sowie Industrie- und Handelskammern;
5.  Vermessungstechnikerin oder Vermessungstechniker und Geomatikerin oder Geomatiker im öffentlichen Dienst die Landesvermessung und Geobasisinformation Brandenburg;
6.  Fachangestellte oder Fachangestellter für Medien- und Informationsdienste die Fachhochschule Potsdam;
7.  Straßenwärterin oder Straßenwärter und Fachkraft für Straßen- und Verkehrstechnik der Landesbetrieb Straßenwesen;
8.  Justizfachangestellte oder Justizfachangestellter die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts;
9.  Sozialversicherungsfachangestellte oder Sozialversicherungsfachangestellter
    1.  in der Fachrichtung gesetzliche Krankenversicherung die Allgemeine Ortskrankenkasse für das Land Brandenburg (AOK Nordost – Die Gesundheitskasse),
    2.  in den Fachrichtungen gesetzliche Rentenversicherung sowie gesetzliche Unfallversicherung die Deutsche Rentenversicherung Berlin-Brandenburg;
10.  im Bereich der Land- und Hauswirtschaft das Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung.

#### § 12 Abweichende Zuständigkeitsregelungen

Die Zuständigkeiten nach dieser Verordnung gelten unbeschadet anderweitiger Regelungen über diese Zuständigkeiten.

#### § 13 Übergangsvorschrift

Für die bei Inkrafttreten dieser Verordnung bereits bestehenden Ausbildungsverhältnisse gelten die Zuständigkeiten nach dieser Verordnung für die weitere Ausbildung.

#### § 14 Inkrafttreten, Außerkrafttreten

(1) Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig treten die Verordnung über Zuständigkeiten nach dem Berufsbildungsgesetz im öffentlichen Dienst vom 12.
Februar 1993 (GVBl.
II S. 94), die zuletzt durch Artikel 10 des Gesetzes vom 26.
Mai 2004 (GVBl.
I S. 240, 243) geändert worden ist, die Verordnung über die Zuständigkeit für die Ausführung des Berufsbildungsgesetzes im Bereich der Landwirtschaft und der Hauswirtschaft vom 3.
April 1993 (GVBl.
II S. 191), die zuletzt durch Artikel 12 des Gesetzes vom 15.
Juli 2010 (GVBl.
I Nr. 28 S. 13) geändert worden ist, die Verordnung über Zuständigkeiten nach dem Berufsbildungsgesetz und der Handwerksordnung im Bereich der beruflichen Bildung vom 18. Dezember 2012 (GVBl.
2013 II Nr. 1), die Berufsqualifikationsfeststellungsgesetz-Zuständigkeitsverordnung vom 5.
Dezember 2012 (GVBl.
II Nr. 104) sowie die Verordnung über Zuständigkeiten nach § 73 Absatz 2 des Berufsbildungsgesetzes für den Ausbildungsberuf des Geomatikers und der Geomatikerin im öffentlichen Dienst vom 8.
Januar 2013 (GVBl.
II Nr. 7) außer Kraft.

Potsdam, den 27.
Februar 2015

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter

Die Ministerin für Arbeit, Soziales,  
Gesundheit, Frauen und Familie

Diana Golze

Der Minister der Justiz  
und für Europa und Verbraucherschutz

Dr.
Helmuth Markov

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Prof.
Dr.-Ing.
Dr.
Sabine Kunst

Der Minister für Bildung,  
Jugend und Sport

Günter Baaske

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

Der Minister der Finanzen

Christian Görke

Der Minister für Wirtschaft und Energie

Albrecht Gerber