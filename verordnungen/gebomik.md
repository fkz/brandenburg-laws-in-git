## Verordnung über die Gebühren für öffentliche Leistungen im Geschäftsbereich des Ministeriums des Innern und für Kommunales (Gebührenordnung des Ministeriums des Innern und für Kommunales - GebOMIK)

Auf Grund des § 3 Absatz 1 in Verbindung mit Absatz 2, § 7 Absatz 1 Nummer 1, § 9 Satz 2, § 18 Absatz 2 Satz 2 des Gebührengesetzes für das Land Brandenburg vom 7.
Juli 2009 (GVBl. I S. 246) verordnet der Minister des Innern:

#### § 1  
Gebührenpflichtige Leistungen

(1) Für öffentliche Leistungen, die im anliegenden Gebührentarif, der Bestandteil dieser Verordnung ist, genannt sind, werden die dort genannten Gebühren erhoben.

(2) Die für öffentliche Leistungen nach dem Enteignungsgesetz des Landes Brandenburg in der Tarifstelle 3 des Gebührentarifs festgelegten Gebührensätze gelten für die öffentlichen Leistungen, die auf der Grundlage anderer Gesetze in enteignungsrechtlichen Verfahren vorgenommen werden, entsprechend.

(3) Für öffentliche Leistungen, für die keine Tarifstelle vorhanden ist und die nicht ausschließlich im besonderen öffentlichen Interesse liegen, kann eine Gebühr in Höhe von mindestens einem und höchstens 500 Euro erhoben werden.

#### § 2  
Ausnahmen von der persönlichen Gebührenfreiheit

(1) Die nach § 8 Absatz 1 Nummer 1 und 2 des Gebührengesetzes für das Land Brandenburg befreiten juristischen Personen einschließlich ihrer Sondervermögen und Landesbetriebe sind gebührenpflichtig für die in der Tarifstelle 3 des Gebührentarifs festgelegten öffentlichen Leistungen in enteignungsrechtlichen Angelegenheiten.

(2) Die nach § 8 Absatz 1 des Gebührengesetzes für das Land Brandenburg befreiten juristischen Personen sind gebührenpflichtig für die in Tarifstelle 11 des Gebührentarifs festgelegten öffentlichen Leistungen der Aus- und Fortbildung.

#### § 3  
Ausnahmen von der Gebührenpflicht

(1) Die Tarifstelle 1.2 des Gebührentarifs gilt nicht für

1.  Schriftsätze aus Verfahren nach dem Verwaltungsrechtlichen Rehabilitierungsgesetz in der Fassung der Bekanntmachung vom 1.
    Juli 1997 (BGBl.
    I S.
    1620), das zuletzt durch Artikel 2 des Gesetzes vom 2.
    Dezember 2010 (BGBl.
    I S.
    1744) geändert worden ist, oder nach dem Beruflichen Rehabilitierungsgesetz in der Fassung der Bekanntmachung vom 1.
    Juli 1997 (BGBl.
    I S.
    1625), das zuletzt durch Artikel 11 des Gesetzes vom 20.
    November 2015 (BGBl.
    I.
    S.
    2010) geändert worden ist, sowie
    
2.  Verfahren nach Artikel 56 Absatz 3 Satz 2 der Verfassung des Landes Brandenburg vom 20. August 1992 (GVBl.
    I S.
    298)
    

in der jeweils geltenden Fassung.

(2) Die Tarifstelle 7.1 des Gebührentarifs gilt nicht für die Leistungen an altrechtliche Vereine mit Sitz in Brandenburg, die gemeinnützigen, mildtätigen oder kirchlichen Zwecken im Sinne der Abgabenordnung dienen.
Die Tarifstellen 7.2.1 bis 7.2.2.2 gelten nicht für Personen, die eine Stiftung, die steuerbegünstigte Zwecke verfolgt, errichten oder errichten wollen.

(3) Die Tarifstelle 13.2 des Gebührentarifs gilt nicht für die Leistungen an die Gemeinden, Gemeindeverbände und deren Zweckverbände.

#### § 4  
Pauschgebühren

Zur Abgeltung mehrfacher, gleichartiger öffentlicher Leistungen, die denselben Schuldner betreffen, können die Gebühren für einen im Voraus zu bestimmenden Zeitraum von höchstens einem Jahr auf Antrag pauschal festgesetzt werden.

#### § 5   
Zeitgebühr

Soweit Gebühren nach dem Zeitaufwand zu berechnen sind, sind der Gebührenberechnung folgende Stundensätze zugrunde zu legen:

1.  für Bedienstete des höheren Dienstes oder vergleichbare Tarifbeschäftigte 81 Euro,
2.  für Bedienstete des gehobenen Dienstes oder vergleichbare Tarifbeschäftigte 64 Euro,
3.  für Bedienstete des mittleren Dienstes oder vergleichbare Tarifbeschäftigte 51 Euro,
4.  für Bedienstete des einfachen Dienstes oder vergleichbare Tarifbeschäftigte 40 Euro.

#### § 6  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über die Gebühren für Amtshandlungen im Geschäftsbereich des Ministers des Innern vom 8.
Mai 2000 (GVBl. II S.
136), die zuletzt durch Verordnung vom 16.
Dezember 2008 [(GVBl. II S.
508)](http://bravors.lvnbb.de/sixcms/media.php/land_bb_bravors_01.a.111.de/GVBl_II_33_2008.pdf "zum Veröffentlichungsblatt") geändert worden ist, außer Kraft.

Potsdam, den 21.
Juli 2010

Der Minister des Innern  
Rainer Speer

* * *

Anlage 
-------

### Gebührentarif

#### Inhaltsverzeichnis

Tarifstelle  

Gegenstand

1  

Allgemeine Verwaltungsgebühren und Auslagen

2  

Einwohnerwesen

3  

Enteignungsrechtliche Angelegenheiten

4  

Sonn- und feiertagsrechtliche Angelegenheiten

5  

Glücksspielangelegenheiten

6  

Ordensrechtliche Angelegenheiten und Ehrenzeichen

7  

Vereins- und stiftungsrechtliche Angelegenheiten

8  

Angelegenheiten des Allgemeinen Ordnungsrechts

9  

Polizeiliche Angelegenheiten

10  

Stellungnahmen zur Ermittlung der Kampfmittelbelastung eines Grundstücks

11  

Angelegenheiten der Aus- und Fortbildung

12  

Personenstandswesen

13  

Bestattungsrechtliche Angelegenheiten

14  

Waffenrechtliche Angelegenheiten

15

Behördliche Namensänderungen

Tarifstelle

Gegenstand

Gebührensatz in Euro

**1**

**Allgemeine Verwaltungsgebühren und Auslagen**

1.1

Beglaubigungen

 

1.1.1

Beglaubigungen von Unterschriften oder Handzeichen

3,00

1.1.2

Beglaubigungen von Urkunden, Abschriften und Ablichtungen, soweit nicht in anderer Tarifstelle enthalten

2,00 je Seite

1.1.3

Beglaubigungen von Urkunden, die zum Gebrauch im Ausland bestimmt sind

 

1.1.3.1

wenn es sich um Vorbeglaubigungen handelt

gebührenfrei

1.1.3.2

wenn es sich um Endbeglaubigungen handelt

30,00

1.2

Anfertigen und Überlassen von Zweitschriften, Kopien, Computerausdrucken und elektronischen Dateien

 

1.2.1

DIN A4 schwarz-weiß

0,50 je Seite

1.2.2

DIN A3 schwarz-weiß

1,50 je Seite

1.2.3

größer DIN A3 schwarz-weiß

2,00 je Seite

1.2.4

DIN A4 in Farbe

1,50 je Seite

1.2.5

DIN A3 in Farbe

2,00 je Seite

1.2.6

größer DIN A3 in Farbe

2,50 je Seite

1.2.7

Überlassen von elektronischen Dateien

2,50 pro Datei, maximal 41,00 je Vorgang

Anmerkung zu den Tarifstellen 1.1 und 1.2:  
  
Soweit spezielle Gesetze nichts anderes vorsehen, erbringen die Kommunen diese Leistungen im Rahmen ihrer Selbstverwaltung, so dass diese Tarifstellen für sie nicht gelten.

 

1.3

Rechtsbehelfe

 

1.3.1

Zurückweisung und teilweise Zurückweisung von Drittwidersprüchen

10,00 bis 500,00

Anmerkung zu Tarifstelle 1.3.1:  
  
Im Übrigen richtet sich die Gebühr für die Zurückweisung und teilweise Zurückweisung von Widersprüchen nach § 18 des Gebührengesetzes für das Land Brandenburg.

 

1.4

Aufhebung von Zuwendungsbescheiden

 

1.4.1

Rückforderung einer gewährten Geldleistung

10,00 bis 500,00

Anmerkung zu Tarifstelle 1.4.1:  
  
Mit der Gebühr ist der Verwaltungsaufwand für die Anforderung von Zinsen für den Rückforderungsbetrag abgegolten.

 

1.4.1.1

wenn die Rückforderung darauf beruht, dass eine Zuwendung durch nachträglich eingetretene unvorhergesehene Minderungen des Investitionsvolumen oder infolge Zuwendungen von dritter Seite gekürzt werden muss

gebührenfrei

1.4.1.2

wenn der Verwendungszweck aus Gründen, die nicht der Zuwendungsempfänger zu vertreten hat, nicht erreicht worden ist

gebührenfrei

1.4.1.3

wenn die Zuwendung nicht rechtzeitig oder fristgerecht verwendet worden ist, sofern der Zuwendungsempfänger dies nicht zu vertreten hat

gebührenfrei

1.5

Akteneinsicht (§ 29 VwVfG in Verbindung mit § 1 Absatz 1 VwVfGBbg), Übermittlung von Informationen

 

1.5.1

Übersendung einer in Papierform geführten Akte im Original auf Antrag

12,00

1.5.2

Übersendung einer elektronisch geführten Akte in Papierform auf Antrag

12,00 zuzüglich der Gebühr nach Tarifstelle 1.2

1.5.3

Übersendung einer elektronisch geführten Akte als Dateien auf Antrag

entsprechend Tarifstelle 1.2.7

**2**

**Einwohnerwesen**

 

2.1

Melderegisterauskünfte

 

2.1.1

Automatisierte Erteilung einfacher Melderegisterauskünfte

5,00 je nachgefragte Person

2.1.1.1 

Schriftliche Erteilung einfacher Melderegisterauskünfte

10,00 je nachgefragte Person

2.1.2

Erweiterte Melderegisterauskunft

12,00 je nachgefragte Person

2.1.3

Melderegisterauskunft, deren Erteilung einen größeren Verwaltungsaufwand erfordert, insbesondere bei Rückgriff auf die in die kommunalen Archive überführten Karteien

13,00 bis 20,00 je nachgefragte Person

2.1.4

Gruppenauskunft

nach Zeitaufwand,  
jedoch mindestens 50,00 und höchstens 250,00

2.1.4.1

zuzüglich je ausgewählten Einwohner

0,20

2.1.5

Auskünfte an Parteien, Wählergemeinschaften und Einzelbewerber

nach Zeitaufwand,  
jedoch mindestens 50,00 und höchstens 250,00

2.1.5.1

zuzüglich je ausgewählten Einwohner

0,20

2.1.6

Melderegisterauskunft über Alters- und Ehejubiläen

7,00 je Jubiläumsfall

mindestens

14,00

höchstens

800,00

2.1.7

Melderegisterauskunft an Adressbuchverlage

 

2.1.7.1

pauschal für die 1.
bis 200.
Person

260,00

2.1.7.2

für die 201.
bis 1 000.
Person

0,50 je Person

2.1.7.3

für die 1 001.
bis 10 000.
Person

0,40 je Person

2.1.7.4

ab der 10 001.
Person

0,05 je Person

2.2

Meldebescheinigungen

 

2.2.1

Meldebescheinigung und erweiterte Meldebescheinigung

5,00

2.2.2

Meldebescheinigung, deren Erteilung einen größeren Verwaltungsaufwand erfordert, insbesondere bei Rückgriff auf die in die kommunalen Archive überführten Karteien

14,00

**3**             

**Enteignungsrechtliche Angelegenheiten**  
  
Öffentliche Leistungen nach dem Enteignungsgesetz des Landes Brandenburg (EntGBbg)

 

3.1

Enteignungsbeschluss (§ 30 Absatz 1 EntGBbg)

1 500,00 zuzüglich 0,5 Prozent der festgesetzten Entschädigung

3.2

Beurkundung einer Einigung (§ 27 Absatz 2 Satz 1 EntGBbg)

750,00 zuzüglich 0,25 Prozent der beurkundeten Entschädigung

3.3

Beurkundung einer Teileinigung (§ 28 Absatz 1 i.
V.
m.
§ 27 Absatz 2 Satz 1 EntGBbg)

750,00

3.4

Beschluss über vorzeitige Besitzeinweisung (§ 37 Absatz 1 EntGBbg)

300,00 bis 3 000,00

3.5

Selbstständige Entschädigungsfestsetzung (§ 28 Absatz 1 Satz 1 und 3 oder § 38 Absatz 2 Satz 3 EntGBbg)

1 250,00 zuzüglich 0,5 Prozent der festgesetzten Entschädigung

3.6

Vorabentscheidung (§ 29 Absatz 2 Satz 1 EntGBbg)

1 250,00 zuzüglich 0,5 Prozent der festgesetzten Vorauszahlung

3.7

Ausführungsanordnung (§ 33 Absatz 1 Satz 1 EntGBbg)

150,00 

3.8

Verlängerung des Laufs der Verwendungsfrist (§ 31 Absatz 2 EntGBbg)

200,00

3.9

Ermächtigung zur Durchführung von Vorarbeiten (§ 39 Absatz 1 Satz 2 EntGBbg)

200,00

3.10

Planfeststellungsbeschluss (§ 23 Absatz 1 Satz 1 EntGBbg)

2 500,00 bis 100 000,00

3.11

Entschädigungsfestsetzung außerhalb der förmlichen Enteignung (§ 41 Satz 1 EntGBbg)

1 250,00 zuzüglich 0,5 Prozent der festgesetzten Entschädigung

**4**

**Sonn- und feiertagsrechtliche Angelegenheiten**  
  
Öffentliche Leistungen nach dem Feiertagsgesetz (FTG)

 

4.1

Erteilung einer Ausnahmegenehmigung (§ 8 FTG)

nach Zeitaufwand  
je begonnene halbe Stunde

**5**

**Glücksspielangelegenheiten**  
  
Öffentliche Leistungen nach dem Glücksspielausführungsgesetz (BbgGlüAG), dem Spielbankgesetz (SpielbG) und dem Glücksspielstaatsvertrag (GlüStV)

 

5.1

Erteilung, Verlängerung, Rücknahme, Widerruf oder nachträgliche Änderung einer Erlaubnis im Sinne von § 4 GlüStV in Verbindung mit § 3 BbgGlüAG oder mit § 4 SpielbG

nach Zeitaufwand  
je begonnene halbe Stunde

5.2

Anordnungen nach § 9 Absatz 1 GlüStV in Verbindung mit § 14 BbgGlüAG oder mit § 9 SpielbG

nach Zeitaufwand  
je begonnene halbe Stunde

**6**

**Ordensrechtliche Angelegenheiten und Ehrenzeichen**

 

6.1

Genehmigung zum Erwerb von Orden und Ehrenzeichen nach § 14 Absatz 2 des Gesetzes über Titel, Orden und Ehrenzeichen

25,00 bis 100,00

**7**

**Vereins- und stiftungsrechtliche Angelegenheiten**  
  
Öffentliche Leistungen nach dem Bürgerlichen Gesetzbuch (BGB), dem Brandenburgischen Ausführungsgesetz zum BGB (BbgAGBGB) und dem Stiftungsgesetz

 

7.1

Vereinsangelegenheiten

 

7.1.1

Verleihung der Rechtsfähigkeit an einen Verein (§ 22 BGB, § 1 BbgAGBGB)

500,00 bis 5 000,00

7.1.2

Genehmigung der Änderung der Satzung (§ 33 Absatz 2 BGB, § 1 BbgAGBGB)

200,00 bis 600,00

7.1.3

Entziehung der Rechtsfähigkeit eines Vereins (§ 43 BGB, § 1 BbgAGBGB)

nach Zeitaufwand  
je begonnene halbe Stunde

7.1.4

Ausstellung einer Vertretungsbescheinigung (§ 5 BbgAGBGB)

80,00

7.2

Angelegenheiten des Stiftungsrechts, soweit nicht nach § 8 Absatz 1 Nummer 8 GebGBbg oder § 3 Absatz 2 dieser Verordnung eine Gebührenbefreiung besteht.

 

7.2.1

Beratung und Stellungnahmen soweit nicht nur einfache allgemeine Auskünfte erteilt werden

 

7.2.1.1

Durchführung einer Beratung zur Errichtung einer Stiftung ohne Beurteilung eines Stiftungsgeschäftsentwurfs

125,00 

7.2.1.2

Erste Stellungnahme zur Anerkennungsfähigkeit eines Stiftungsgeschäftsentwurfs

100,00 bis 1 600,00 

7.2.1.2.1

Jede weitere Stellungnahme zur Anerkennungsfähigkeit des überarbeiteten Stiftungsgeschäftsentwurfs

55,00 bis 650,00 

7.2.1.3

Erste Stellungnahme zur Genehmigungsfähigkeit einer Satzungsänderung

60,00 bis 550,00 

7.2.1.3.1

Jede weitere Stellungnahme zur Genehmigungsfähigkeit des überarbeiteten Satzungsänderungsentwurfs

40,00 bis 330,00 

7.2.2

Anerkennung der Rechtsfähigkeit

7.2.2.1

wenn zuvor Stellungnahme zur Anerkennungsfähigkeit eingeholt wurde und das Stiftungsgeschäft der Stellungnahme entspricht, so dass keine weitere rechtliche Prüfung erforderlich ist

230,00 

7.2.2.2

wenn zuvor keine Stellungnahme zur Anerkennungsfähigkeit eingeholt worden war oder wenn zuvor Stellungnahme zur Anerkennungsfähigkeit eingeholt wurde und das Stiftungsgeschäft der Stellungnahme nicht entspricht, so dass erneute rechtliche Prüfung erforderlich ist

285,00 bis 3 500,00 

7.2.3

Genehmigung einer Satzungsänderung

 

7.2.3.1

wenn zuvor Stellungnahme zur Genehmigungsfähigkeit eingeholt worden war und die beantragte Änderung der Stellungnahme entspricht, so dass keine weitere inhaltliche Prüfung erforderlich ist

200,00 

7.2.3.2

wenn zuvor keine Stellungnahme zur Genehmigungsfähigkeit eingeholt worden war oder wenn zuvor Stellungnahme zur Genehmigungsfähigkeit eingeholt wurde und die beantragte Änderung weicht von der Stellungnahme ab, so dass erneute rechtliche Prüfung erforderlich ist

220,00 bis 700,00 

7.2.4

Sonstige Genehmigungen

 

7.2.4.1

Entscheidung über die Genehmigung eines Zusammenschlusses von Stiftungen

nach Zeitaufwand,  
höchstens 7 000,00 

7.2.4.2

Entscheidung über die Genehmigung der Auflösung einer Stiftung

nach Zeitaufwand,  
höchstens 3 500,00 

7.2.5

Bescheinigungen

 

7.2.5.1

Erstmalige Ausstellung einer Vertretungsbescheinigung nach Wechsel eines Vorstandsmitgliedes

80,00 

7.2.5.2

Ausstellung einer gesiegelten weiteren Vertretungsbescheinigung (weitere Ausfertigung)

20,00 

7.2.5.3

Gesiegelter Auszug aus dem Stiftungsregister

20,00 

7.2.6

Sonstige Leistungen

 

7.2.6.1

Entscheidung über die Rechtsnatur einer Stiftung

nach Zeitaufwand,  
höchstens 3 500,00 

**8**

**Angelegenheiten des Allgemeinen Ordnungsrechts**

 

8.1

Verwaltung und Verwahrung von Fundsachen

 

8.1.1

im geschätzten Wert von unter 25 Euro

gebührenfrei

8.1.2

im geschätzten Wert von 25 Euro oder mehr

4 Prozent des geschätzten Wertes, mindestens 6,00

8.2

Verwahrung von Fahrzeugen und sonstigen Gegenständen durch die Ordnungsbehörde nach Freigabe

entsprechend Tarifstelle 9.2

8.3

Sonstige öffentliche Leistungen der Ordnungsbehörden

 

8.3.1

Einfangen von Tieren und/oder Veranlassung der Unterbringung

nach Zeitaufwand  
je begonnene halbe Stunde

8.3.2

für jedes eingesetzte Fahrzeug je gefahrenen Kilometer

0,34

8.4

HundehaltungÖffentliche Leistungen nach der Hundehalterverordnung (HundehV)

 

8.4.1

Untersagung des Haltens eines Hundes (§ 5 Absatz 1 HundehV)

25,00 bis 500,00

8.4.2

Erteilung eines Negativzeugnisses (§ 8 Absatz 3 Satz 3 HundehV)

25,00 bis 125,00

8.4.3

Erlaubnis für das Züchten einer gefährlichen Hunderasse (§ 7 Absatz 1 Satz 4 HundehV)

125,00 bis 800,00

8.4.4

Erlaubnis für das Ausbilden oder Abrichten eines gefährlichen Hundes (§ 10 Absatz 1 HundehV)

125,00 bis 800,00

8.4.5

Erlaubnis für das Halten eines gefährlichen Hundes (§ 10 Absatz 1 HundehV)

50,00 bis 500,00

8.4.6

Allgemeine Erlaubnis zum Halten gefährlicher Hunde für Tierheime (§ 10 Absatz 5 HundehV)

25,00 bis 500,00

8.4.7

Rücknahme einer Erlaubnis (§ 7 Absatz 1 Satz 4 und § 10 Absatz 3 Satz 4 HundehV)

entsprechend Tarifstelle 8.4.3, 8.4.4, 8.4.5 oder 8.4.6

8.4.8

Ausgabe einer Ersatzplakette

25,00

8.5

Öffentliche Leistungen im Bereich der Verordnung zum Schutz von Kriegsstätten

 

8.5.1

Befreiung vom Verbot der Suche (§ 3 der Verordnung)

nach Zeitaufwand  
je begonnene Viertelstunde

**9**

**Polizeiliche Angelegenheiten**

 

9.1

Begleitung von Transporten aller Art

 

9.1.1

auf der Straße

 

9.1.1.1

mit Kfz

nach Zeitaufwand  
je begonnene halbe Stunde

 

mindestens je Einsatz

100,00

9.1.1.2

polizeiliche Verkehrsregelungsmaßnahmen,

1.  sofern neben einer angeordneten polizeilichen Transportbegleitung zusätzliche Polizeibedienstete zur Verkehrsregelung eingesetzt werden oder
2.  sofern eine polizeiliche Transportbegleitung nicht angeordnet ist, aber die Anordnung im Genehmigungsverfahren die Durchführung von polizeilichen Verkehrsregelungsmaßnahmen vorsieht

Anmerkungen zu den Tarifstellen 9.1.1.1 und 9.1.1.2:  
  
Die Gebühren werden erhoben je eingesetzte Polizeibedienstete oder eingesetzten Polizeibediensteten.  
  
Zu dem zugrunde zu legenden Zeitaufwand zählen auch der Begleitung vorausgehende Tätigkeiten (z.
B.
Abfahrtkontrolle etc.) sowie Wartezeiten der im Rahmen der Begleitung oder der Verkehrsregelung eingesetzten Polizeibediensteten, sofern sie nicht von der Polizei zu vertreten sind.

nach Zeitaufwand  
je begonnene halbe Stunde

9.1.1.3

für jedes eingesetzte Fahrzeug je gefahrenen Begleitkilometer (einfache Strecke)

0,34

9.1.1.4

An- und Abfahrt (für jedes im Rahmen der Begleitung oder zusätzlicher Verkehrsregelungsmaßnahmen eingesetzte Fahrzeug)

50,00

9.1.1.5

Bearbeitungsgebühr

50,00 bis 125,00

 

Anmerkungen zu Tarifstelle 9.1.1.5:  
  
Die Gebühr von 50,00 Euro wird erhoben, wenn die Maßnahmen der Begleitung und ggf.
zusätzlicher Verkehrsregelung den Bereich einer Polizeidirektion betreffen.
Sie erhöht sich um jeweils 25,00 Euro, wenn eine oder mehrere weitere Polizeidirektionen betroffen sind.  
  
Die Gebühr wird auch erhoben, wenn eine polizeiliche Transportbegleitung nicht angeordnet ist, aber die Anordnung im Genehmigungsverfahren die Durchführung von polizeilichen Verkehrsregelungsmaßnahmen vorsieht.  
  
Diese Gebühr ist auch dann zu entrichten, wenn der Antrag erst innerhalb von 48 Stunden vor dem geplanten Termin zurückgenommen wird oder der Transport aus nicht von der Polizei zu vertretenden Gründen nicht durchgeführt wird.

 

9.1.2

auf dem Wasser

 

9.1.2.1

mit Wasserschutzpolizeistreifenboot (WSP)

entsprechend Tarifstelle 9.1.1.1

9.1.2.2

polizeiliche Verkehrsregelungsmaßnahmen, sofern hierfür über die polizeiliche Begleitung hinaus zusätzliche Polizeibedienstete eingesetzt werden

entsprechend Tarifstelle 9.1.1.2

 

Anmerkungen zu den Tarifstellen 9.1.2.1 und 9.1.2.2:  
  
Die Anmerkungen zu den Tarifstellen 9.1.1.1 und 9.1.1.2 gelten entsprechend.

 

9.1.2.3

für jedes eingesetzte Wasserschutzpolizeistreifenboot (WSP) (einfache Strecke)

45,00je begonnene halbe Betriebsstunde

9.1.2.4

An- und Abfahrt je eingesetztes Wasserschutzpolizeistreifenboot (WSP)

250,00

9.1.2.5

für jedes eingesetzte Kfz je gefahrenen Begleitkilometer (einfache Strecke)

entsprechend Tarifstelle 9.1.1.3

9.1.2.6

An- und Abfahrt (für jedes im Rahmen der Begleitung oder zusätzlicher Verkehrsregelungsmaßnahmen eingesetzte Kfz)

entsprechend Tarifstelle 9.1.1.4

9.1.2.7

Bearbeitungsgebühr

entsprechend Tarifstelle 9.1.1.5

 

Anmerkung zu Tarifstelle 9.1.2.7:  
  
Die Anmerkungen zu Tarifstelle 9.1.1.5 gelten entsprechend.

 

9.2

Verwahrung

 

9.2.1

Verwahrung sichergestellter Fahrzeuge und sonstiger Gegenstände durch die Polizei selbst, nach Freigabe

 

9.2.1.1

für Fahrräder

2,00 je begonnenen Tag

9.2.1.2

für Fahrräder mit Hilfsmotor

2,00 je begonnenen Tag

9.2.1.3

für Krafträder

3,00 je begonnenen Tag

9.2.1.4

für Krafträder mit Beiwagen, motorige Dreiräder (Trike) und vierrädrige Krafträder (Quad)

6,00 je begonnenen Tag

9.2.1.5

für Personenkraftwagen

6,00 je begonnenen Tag

9.2.1.6

für Personenkraftwagen mit Anhänger

7,00 je begonnenen Tag

9.2.1.7

für Zugmaschinen, Lastkraftwagen, Lastkraftwagen-Anhänger, Auflieger, Omnibusse und Fuhrwerke

12,00 je begonnenen Tag

9.2.1.8

für Kanadier, Paddel- und Ruderboote

4,00 je begonnenen Tag

9.2.1.9

für Segel- und Motorboote bis zu 5 Meter Länge

7,00 je begonnenen Tag

9.2.1.10

für Segel- und Motorboote über 5 Meter Länge

10,00 je begonnenen Tag

9.2.1.11

für Arbeitsmaschinen, Fahrzeugteile und Materialien mit einer erforderlichen Lagerfläche von mehr als einem Quadratmeter

2,00 je Tag je begonnenen Quadratmeter Lagerfläche

9.2.1.12

für sonstige Gegenstände (Kleinmaterialien) mit einer erforderlichen Lagerfläche von weniger als einem Quadratmeter und unter einem Meter Länge

10,00 je Vorgang je begonnenen Monat

9.2.2

Veranlassung der Verwahrung ohne Verwahrung durch die Polizei

50,00

9.3

Maßnahmen auf Grund ungerechtfertigten Alarmierens der Polizei oder Vortäuschen einer Gefahrenlage

 

9.3.1

Einsatz von Polizeikräften auf Grund einer Alarmierung durch eine Überfall- und Einbruchmeldeanlage; die Gebührenpflicht besteht nicht, wenn – abgesehen von der Alarmgebung der Anlage – Anhaltspunkte für eine Straftat festgestellt werden

100,00

 

Anmerkungen zu Tarifstelle 9.3.1:  
  
Gebührenschuldner ist

*   bei Anlagen, die an eine Zentrale für Gefahrenmeldungen/Gefahrenmeldeanlagen angeschlossen sind, das Unternehmen, das die Zentrale betreibt,
*   bei Anlagen, die nicht an eine Zentrale angeschlossen sind, der Anlagenbetreiber,
*   bei kombinierten Anlagen das Unternehmen, das die Zentrale betreibt, wenn durch diese zuerst die Polizei benachrichtigt wurde, in den übrigen Fällen der Anlagenbetreiber.

Diese Gebührenregelung gilt nicht für Einsätze der Polizei auf Grund von Alarmierungen durch eine Überfall- und Einbruchmeldeanlage mit Anschluss an die Polizei.

 

9.3.2

Maßnahmen auf Grund ungerechtfertigten Alarmierens der Polizei oder Vortäuschen einer Gefahrenlage

100,00 bis 50 000,00

 

Anmerkungen zu Tarifstelle 9.3.2:  
  
Eine ungerechtfertigte Alarmierung liegt vor, wenn die Polizei zur Abwehr einer Gefahr angefordert wird, die objektiv nicht vorliegt.
Ein Missbrauch ist nicht gegeben, wenn sich die alarmierende Person in einem Irrtum über das Bestehen der Gefahrenlage befindet.  
  
Wer gegenüber einer dritten Person eine Gefahrenlage (Gefahr für Leben, Gesundheit oder Freiheit von Personen oder für bedeutende Sachwerte, z.
B.
durch mündliche oder schriftliche Ankündigung eines Anschlags oder einer Amoktat) vortäuscht, ist gebührenpflichtig, wenn er, unter Berücksichtigung der individuellen Einsichtsfähigkeit, damit rechnen muss, dass die Person bei verständiger Würdigung der vorgetäuschten Gefahrenlage die Polizei alarmiert.

 

9.4

Gewahrsam und Transport von Personen

 

9.4.1

Gewahrsam für hilflose, nicht vorläufig festgenommene Personen, die betrunken sind oder unter der Einwirkung von berauschenden Mitteln stehen

50,00

9.4.2

Transport hilfloser, nicht vorläufig festgenommener Personen, die betrunken sind oder unter Einwirkung berauschender Mittel stehen

entsprechend Tarifstelle 9.1.1.1

 

mindestens je Einsatz

100,00

9.4.3

Transport von Personen auf eigenen Wunsch oder auf Ersuchen anderer Personen mit polizeieigenem Fahrzeug

entsprechend Tarifstelle 9.1.1.1

 

mindestens je Einsatz

100,00

 

Anmerkungen zu den Tarifstellen 9.4.2 und 9.4.3:  
  
Die Gebühren werden erhoben je eingesetzte Polizeibedienstete oder eingesetzten Polizeibediensteten.  
  
Zu dem zugrunde zu legenden Zeitaufwand zählen auch Wartezeiten der eingesetzten Polizeibediensteten, sofern sie nicht von der Polizei zu vertreten sind.  
  
Werden mehrere Personen transportiert, so wird die zu erhebende Gebühr gleichmäßig aufgeteilt.

 

9.4.4

für jedes eingesetzte Fahrzeug je gefahrenen Transportkilometer (einfache Strecke)

entsprechend Tarifstelle 9.1.1.3

9.5

Inanspruchnahme polizeilicher Einrichtungen

 

9.5.1

eines Polizeihubschraubers

500,00 für jede angefangene Viertelstunde

9.6

Reinigung wegen außergewöhnlicher Verschmutzung

 

9.6.1

eines Dienst- oder Gewahrsamraumes

100,00

9.6.2

eines Dienstfahrzeuges

100,00

9.6.3

von Uniformteilen oder sonstigen Einsatzgegenständen

20,00

9.7

Sonstige polizeiliche Leistungen

 

9.7.1

Suche, Rettung oder Bergung von Personen oder Tieren

100,00 bis 50 000,00

9.7.2

Maßnahmen der Sicherung und Verkehrslenkung beim Ausfall von Verkehrsleiteinrichtungen an Baustellen

100,00 bis 50 000,00

9.7.3

Sonstige Anordnungen nach dem Kraftfahrzeugsteuergesetz 1994, der Straßenverkehrszulassungsordnung, der Fahrerlaubnisverordnung oder der Verordnung über internationalen Kraftfahrzeugverkehr

15,00 bis 300,00

**10**

**Stellungnahmen zur Ermittlung der Kampfmittelbelastung eines Grundstücks**

 

10.1

Stellungnahme ohne Luftbildauswertung und Ortsbegehung

 

10.1.1

ohne Archivabfrage

50,00

10.1.2

mit Archivabfrage

100,00

10.2

Stellungnahme mit Luftbildauswertung

 

10.2.1

bei einer Arbeitszeit bis 2 Stunden

200,00

10.2.2

bei einer Arbeitszeit von mehr als 2 Stunden bis 8 Stunden

350,00

10.2.3

bei einer Arbeitszeit von mehr als 8 Stunden

500,00

10.3

Stellungnahme ohne Luftbildauswertung mit Ortsbegehung

 

10.3.1

bei einer Arbeitszeit einschließlich Anfahrt bis 2 Stunden

200,00 

10.3.2

bei einer Arbeitszeit einschließlich Anfahrt von mehr als 2 Stunden bis 3 Stunden

250,00 

10.3.3

bei einer Arbeitszeit einschließlich Anfahrt von mehr als 3 Stunden

300,00 

10.4

Stellungnahme mit Luftbildauswertung und Ortsbegehung

 

10.4.1

bei einer Gesamtarbeitszeit bis 4 Stunden

400,00 

10.4.2

bei einer Gesamtarbeitszeit von mehr als 4 Stunden bis 8 Stunden

600,00 

10.4.3

bei einer Gesamtarbeitszeit von mehr als 8 Stunden

800,00 

**11**

**Angelegenheiten der Aus- und Fortbildung**

 

11.1

Berufsausbildung nach dem Berufsbildungsgesetz (BBiG)

 

11.1.1

Eignungsfeststellung einschließlich widerrufliche Zuerkennung (§ 33 BBiG)

200,00

11.1.2

Aufforderung zur Beseitigung von Mängeln (§ 32 Absatz 2 BBiG)

150,00

11.1.3

Untersagung des Einstellens und Ausbildens (§ 33 Absatz 1, 2 BBiG)

150,00

11.1.4

Eintragung in das Verzeichnis der Berufsbildungsverhältnisse (§ 35 Absatz 1 BBiG)

50,00

11.1.5

Eintragung von Änderungen in das Verzeichnis der Berufsbildungsverhältnisse (§ 35 Absatz 1 BBiG)

25,00

11.1.6

Abkürzung, Verkürzung oder Verlängerung der Ausbildungszeit (§ 8 BBiG)

25,00

11.1.7

Löschung einer Eintragung im Verzeichnis der Berufsausbildungsverhältnisse (§ 35 Absatz 2 BBiG)

25,00

11.1.8

Entscheidung über die Zulassung Externer zur Abschlussprüfung (§ 45 Absatz 2 BBiG) einschließlich Überstellung

50,00

11.1.9

Entscheidung über die Zulassung zur Fortbildungsprüfung zum anerkannten Abschluss „Geprüfter Meister für Bäderbetriebe“ einschließlich Überstellung

50,00

11.2

Prüfung nach dem Berufsbildungsgesetz

 

11.2.1

Durchführung der Zwischenprüfung (§ 48 BBiG)

25,00 bis 100,00

11.2.2

Durchführung der Abschlussprüfung (§ 37 Absatz 1 BBiG)

25,00 bis 250,00

11.2.3

Durchführung der Wiederholung der Abschlussprüfung nach § 37 Absatz 1 BBiG

25,00 bis 150,00

11.2.4

Durchführung der Fortbildungsprüfung (§ 56 Absatz 1 BBiG)

25,00 bis 250,00

11.2.5

Durchführung der Wiederholung der Fortbildungsprüfung (§ 56 Absatz 1 BBiG)

25,00 bis 250,00

11.2.6

Durchführung der Prüfung zum Nachweis berufs- und arbeitspädagogischer Kenntnisse

75,00 bis 150,00

11.2.7

Durchführung der Wiederholungsprüfung zum Nachweis berufs- und arbeitspädagogischer Kenntnisse

25,00 bis 75,00

11.2.8

Bescheinigung über die Befreiung vom Nachweis berufs- und arbeitspädagogischer Kenntnisse

10,00 bis 25,00

11.2.9

Teilbefreiung von der Fortbildungsprüfung (§ 56 Absatz 2 BBiG)

10,00 bis 25,00

11.2.10

Gleichstellung von Abschlusszeugnissen (§ 103 BBiG)

25,00 bis 50,00

11.3

Prüfungen im öffentlich-rechtlichen Dienstverhältnis

 

11.3.1

Durchführung der Zwischenprüfung im Vorbereitungsdienst im mittleren Dienst

20,00 bis 140,00

11.3.2

Durchführung der Zwischenprüfung im Vorbereitungsdienst im gehobenen Dienst

25,00 bis 175,00

11.3.3

Durchführung der Abschlussprüfung zum Erwerb der Laufbahnbefähigung im mittleren Dienst

25,00 bis 150,00

11.3.4

Durchführung der Abschlussprüfung zum Erwerb der Laufbahnbefähigung im gehobenen Dienst

25,00 bis 230,00

11.4

Anerkennung von Berufsqualifikationen nach der EU-Laufbahnbefähigungsanerkennungsverordnung (EU-LBAV)

 

11.4.1

Anerkennung

200,00 

**12**

**Personenstandswesen**  
  
Öffentliche Leistungen nach dem Personenstandsgesetz (PStG) und der Verordnung zur Ausführung des Personenstandsgesetzes (PStV)

 

12.1

Eheschließung

 

12.1.1

Prüfung der Ehevoraussetzungen (§ 13 PStG)

 

12.1.1.1

wenn nur deutsches Recht zu beachten ist

41,00

12.1.1.2

wenn auch ausländisches Recht zu beachten ist, zusätzlich

25,00 je ausländisches Recht

12.1.1.3

wenn eine Überprüfung einer ausländischen Entscheidung in Ehesachen oder in Lebenspartnerschaftssachen durch das Standesamt erfolgt, zusätzlich

25,00 je Person

12.1.1.4

wenn ein Verfahren nach § 1309 Absatz 2 BGB oder § 107 Absatz 1 Satz 1 FamFG erforderlich ist, zusätzlich

10,00 je Person

12.1.2

erneute Prüfung der Ehevoraussetzungen (§ 29 Absatz 2 PStV)

 

12.1.2.1

wenn nur deutsches Recht zu beachten ist

21,00

12.1.2.2

wenn auch ausländisches Recht zu beachten ist, zusätzlich

12,00 je ausländisches Recht

12.1.3

Vornahme der Eheschließung (§ 14 PStG)

 

12.1.3.1

in den Amtsräumen

 

12.1.3.1.1

während der allgemeinen Öffnungszeiten

30,00

12.1.3.1.2

außerhalb der allgemeinen Öffnungszeiten

60,00

12.1.3.2

außerhalb der Amtsräume

 

12.1.3.2.1

während der allgemeinen Öffnungszeiten

90,00

12.1.3.2.2

außerhalb der allgemeinen Öffnungszeiten

120,00

12.1.3.2.3

bei Vorliegen einer lebensgefährlichen Erkrankung (§ 13 Absatz 3 PStG)

30,00

 

Anmerkung zu Tarifstelle 12.1:

1.  Wird anstelle des gewöhnlich für Eheschließungen vorgesehenen Raumes des Standesamtes auf Wunsch der Eheschließenden ein anderer (gewidmeter) Raum genutzt oder
2.  wird eine Dolmetscherin oder Dolmetscher herangezogen,

sind die Aufwendungen als Auslagen nach § 9 des Gebührengesetzes für das Land Brandenburg zu erheben.

 

12.2

Ehefähigkeitszeugnis

 

12.2.1

Ausstellung eines Ehefähigkeitszeugnisses

 

12.2.1.1

wenn nur deutsches Recht zu beachten ist

41,00

12.2.1.2

wenn auch ausländisches Recht zu beachten ist, zusätzlich

25,00 je ausländisches Recht

12.2.1.3

wenn eine Überprüfung einer ausländischen Entscheidung in Ehesachen oder in Lebenspartnerschaftssachen durch das Standesamt erfolgt, zusätzlich

25,00 je Person

12.2.1.4

wenn ein Verfahren nach § 107 Absatz 1 Satz 1 FamFG erforderlich ist, zusätzlich

10,00 je Person

 

Anmerkung zu Tarifstelle 12.2.1:  
  
Soweit im Rahmen zwischenstaatlicher Vereinbarungen vorgesehen, ist die Ausstellung gebührenfrei.

 

12.2.2

Beschaffung eines Ehefähigkeitszeugnisses für eine ausländische Person

41,00

12.3

Begründung einer Lebenspartnerschaft

 

12.3.1

Prüfung der Voraussetzungen für die Begründung einer Lebenspartnerschaft (§ 17 i.
V.
m.
§ 13 PStG)

 

12.3.1.1

wenn nur deutsches Recht zu beachten ist

41,00

12.3.1.2

wenn auch ausländisches Recht zu beachten ist, zusätzlich

25,00 je ausländisches Recht

12.3.1.3

wenn eine Überprüfung einer ausländischen Entscheidung in Ehesachen oder in Lebenspartnerschaftssachen durch das Standesamt erfolgt, zusätzlich

25,00 je Person

12.3.1.4

wenn ein Verfahren nach §107 Absatz 1 Satz 1 FamFG erforderlich ist, zusätzlich

10,00 je Person

12.3.2

erneute Prüfung der Voraussetzungen für die Begründung einer Lebenspartnerschaft (§ 30 Satz 2 i.
V.
m.
§ 29 Absatz 2 PStV)

 

12.3.2.1

wenn nur deutsches Recht zu beachten ist

21,00

12.3.2.2

wenn auch ausländisches Recht zu beachten ist, zusätzlich

12,00 je ausländisches Recht

12.3.3

Mitwirkung an der Begründung einer Lebenspartnerschaft

 

12.3.3.1

in den Amtsräumen

 

12.3.3.1.1

während der allgemeinen Öffnungszeiten

30,00

12.3.3.1.2

außerhalb der allgemeinen Öffnungszeiten

60,00

12.3.3.2

außerhalb der Amtsräume

 

12.3.3.2.1

während der allgemeinen Öffnungszeiten

90,00

12.3.3.2.2

außerhalb der allgemeinen Öffnungszeiten

120,00

12.3.3.2.3

bei Vorliegen einer lebensgefährlichen Erkrankung (§ 17 i.
V.
m.
§ 13 Absatz 3 PStG)

30,00

 

Anmerkung zu Tarifstelle 12.3:

1.  Wird anstelle des gewöhnlich für Begründungen von Lebenspartnerschaften vorgesehenen Raumes des Standesamtes auf Wunsch der Lebenspartner oder Lebenspartnerinnen ein anderer (gewidmeter) Raum genutzt, oder
2.  wird eine Dolmetscherin oder ein Dolmetscher herangezogen,

sind die Aufwendungen als Auslagen nach § 9 des Gebührengesetzes für das Land Brandenburg zu erheben.

 

12.4

Beurkundungsgrundlagen, Beurkundungen, Beglaubigungen, Bescheinigungen

 

12.4.1

Abnahme einer Versicherung an Eides statt (§ 9 Absatz 2 Satz 2 PStG)

31,00

12.4.2

Beurkundung

 

12.4.2.1

einer im Ausland geschlossenen Ehe (§ 34 Absatz 1 PStG)

85,00

12.4.2.1.1

wenn bei Eheschließung mindestens ein Partner Ausländer war, zusätzlich

25,00 je ausländisches Recht

12.4.2.1.2

wenn eine Überprüfung einer ausländischen Entscheidung in Ehesachen oder in Lebenspartnerschaftssachen durch das Standesamt erfolgt, zusätzlich

25,00 je Person

12.4.2.1.3

wenn ein Verfahren nach § 107 Absatz 1 Satz 1 FamFG erforderlich ist, zusätzlich

10,00 je Person

12.4.2.2

einer vor einer ermächtigten Person im Inland geschlossenen Ehe zwischen Ausländern (§ 34 Absatz 2 PStG)

110,00

12.4.2.3

einer im Ausland begründeten Lebenspartnerschaft (§ 35 Absatz 1 PStG)

85,00

12.4.2.3.1

wenn bei Begründung mindestens ein Lebenspartner Ausländer oder eine Lebenspartnerin Ausländerin war, zusätzlich

25,00 je ausländisches Recht

12.4.2.3.2

wenn eine Überprüfung einer ausländischen Entscheidung in Ehesachen oder in Lebenspartnerschaftssachen durch das Standesamt erfolgt, zusätzlich

25,00 je Person

12.4.2.3.3

wenn ein Verfahren nach § 107 Absatz 1 Satz 1 FamFG erforderlich ist, zusätzlich

10,00 je Person

12.4.2.4

einer im Ausland erfolgten Geburt (§ 36 Absatz 1 PStG)

80,00

12.4.2.4.1

wenn eine Adoption im Ausland zu prüfen ist, zusätzlich

60,00

12.4.2.4.2

wenn eine Überprüfung einer ausländischen Entscheidung in Ehesachen oder in Lebenspartnerschaftssachen durch das Standesamt erfolgt, zusätzlich

25,00 je Person

12.4.2.4.3

wenn ein Verfahren nach § 107 Absatz 1 Satz 1 FamFG erforderlich ist, zusätzlich

10,00 je Person

12.4.2.5

eines Sterbefalles im Ausland (§ 36 Absatz 1 PStG)

60,00

12.4.2.6

Veranlassung einer Urkundenprüfung bei der zuständigen deutschen Auslandsvertretung, zusätzlich

10,00

 

Anmerkung zu Tarifstelle 12.4.2.6:  
  
Kosten der Auslandsvertretungen sind als Auslagen nach § 9 des Gebührengesetzes für das Land Brandenburg zu erheben.

 

12.4.3

Beglaubigung oder Beurkundung einer Erklärung

 

12.4.3.1

zur Namensführung von Ehegatten (§ 41 Absatz 1 PStG) nach der Eheschließung oder von Lebenspartnern oder Lebenspartnerinnen nach der Begründung der Lebenspartnerschaft (§ 42 Absatz 1 PStG)

30,00

12.4.3.2

zur Namensführung in der Ehe oder Lebenspartnerschaft einschließlich eines Begleitnamens, wenn der Name bei der Eheschließung oder bei der Begründung der Lebenspartnerschaft bestimmt wird

gebührenfrei

12.4.3.3

zur Namensführung des Kindes (§ 45 Absatz 1 PStG)

30,00

12.4.3.4

zur Bestimmung des Geburtsnamens des Kindes nach § 1617 Absatz 1 Satz 1 BGB, wenn das Kind dadurch erstmals einen Geburtsnamen erhält

gebührenfrei

12.4.3.5

zur Namensangleichung nach Artikel 47 EGBGB oder nach § 94 des Bundesvertriebenengesetzes sowie über die Namenswahl nach Artikel 48 EGBGB (§ 43 Absatz 1 PStG)

30,00

12.4.3.6

zur Anerkennung der Vaterschaft oder der Mutterschaft (§ 44 Absatz 1 und 2 PStG)

30,00

12.4.3.6.1

der erforderlichen Zustimmung, soweit sie nicht mit der Erklärung zur Anerkennung der Vaterschaft oder Mutterschaft abgegeben wird

30,00

12.4.3.7

zur Neubestimmung der Reihenfolge der Vornamen gemäß § 45a PStG

30,00 

12.4.4

Bescheinigung über Erklärungen zur Namensführung (§ 46 PStV)

10,00

12.4.5

Bescheinigung über eine Fehlgeburt (§ 31 Absatz 3 Satz 3 PStV)

10,00

12.4.6

Bescheinigung über die Zurückstellung einer Beurkundung auf Antrag (§ 7 Absatz 2 PStV)

10,00

12.5

Personenstandsurkunden

 

12.5.1

Ausstellung von Personenstandsurkunden

 

12.5.1.1

Ausstellung eines beglaubigten Registerausdrucks, einer Ehe-, Lebenspartnerschafts-, Geburts- oder Sterbeurkunde oder einer beglaubigten Abschrift eines Personenstandseintrags aus einem Altregister oder einer Übergangsbeurkundung (§ 55 Absatz 1, § 76 Absatz 2, § 77 Absatz 3 PStG sowie § 70 Absatz 1 PStV)

10,00

12.5.1.2

Übermittlung von Urkundsdaten durch das registerführende Standesamt an ein anderes Standesamt zur Ausstellung einer dort beantragten Personenstandsurkunde (§ 56 Absatz 4 Satz 1 PStG)

8,00

12.5.1.3

Ausstellung einer Personenstandsurkunde durch ein anderes als das zuständige Standesamt im Wege des Ausdrucks und der Beglaubigung der vom registerführenden Standesamt übermittelten Daten (§ 56 Absatz 4 Satz 2 PStG)

8,00

12.5.1.4

für ein zweites und jedes weitere Stück der Urkunde, wenn es gleichzeitig beantragt und in einem Arbeitsgang hergestellt wird

5,00

12.5.2

Erteilung von Personenstandsurkunden an Behörden und Gerichte (§ 65 PStG)

gebührenfrei

12.5.3

Eintragung in ein internationales Stammbuch der Familie (§ 52 PStV)

10,00

12.5.4

Erteilung einer beglaubigten Abschrift aus dem als Heiratseintrag fortgeführten Familienbuch zum Nachweis der Geburt eines Kindes (§ 49 PStV)

10,00

12.6

Auskunft und Einsichtnahme

 

12.6.1

Auskunft aus einem oder Einsicht in einen Registereintrag (§ 62 Absatz 2 PStG)

 

12.6.1.1

bei Vorsprache und mündlicher Auskunft oder Einsicht

5,00

12.6.1.2

bei schriftlicher oder elektronischer Auskunft

8,00

12.6.2

Auskunft aus den oder Einsicht in die Sammelakten (§ 62 Absatz 2 PStG) oder Auskunft aus anderen Akten des Standesamtes

15,00

12.6.3

Suchen eines Eintrags oder Vorgangs, wenn zum sofortigen Auffinden erforderliche Angaben nicht gemacht werden können

10,00 für jede begonnene Viertelstunde

12.6.4

Auskunft aus einem oder Einsicht in einen Registereintrag oder Auskunft aus den oder Einsicht in die Sammelakten für Behörden und Gerichte einschließlich etwaiger Suche (§ 65 PStG)

gebührenfrei

12.6.5

Auskunft aus einem oder Einsicht in die Personenstandsregister oder Sammelakten oder Gewährung der Durchsicht von Personenstandsregistern oder Sammelakten für wissenschaftliche Zwecke einschließlich etwaiger Suche (§ 66 Absatz 1 PStG)

gebührenfrei

**13**

**Bestattungsrechtliche Angelegenheiten**  
  
Öffentliche Leistungen nach dem Brandenburgischen Bestattungsgesetz (BbgBestG)

 

13.1

Genehmigung des Betriebs der Feuerbestattungsanlage (§ 24 Absatz 5 erste Alternative BbgBestG)

nach Zeitaufwand  
je begonnene halbe Stunde

13.2

Überprüfung des Betriebs der Feuerbestattungsanlage (§ 24 Absatz 5 zweite Alternative BbgBestG)

nach Zeitaufwand  
je begonnene halbe Stunde

13.3

Erteilung einer Ausnahme vom Friedhofszwang (§ 25 Absatz 1 Satz 2 und Absatz 2 Satz 4 BbgBestG)

nach Zeitaufwand  
je begonnene halbe Stunde

**14**

**Waffenrechtliche Angelegenheiten**  
  
Öffentliche Leistungen nach dem Waffengesetz (WaffG) und der Allgemeine-Waffengesetz-Verordnung (AWaffV)

 

14.1

Erwerb und Besitz von Schusswaffen/Munition

 

14.1.1

Ausstellung einer Waffenbesitzkarte

14.1.1.1

für eine natürliche Person (§ 10 Absatz 1 Satz 1 WaffG)

70,00

14.1.1.2

für mehrere Berechtigte einschließlich der Erwerbserlaubnis für die erste Schusswaffe (§ 10 Absatz 2 Satz 1 WaffG)

70,00 und je weitere Person 35,00

14.1.1.3

für eine juristische Person einschließlich der Erwerbserlaubnis für die erste Schusswaffe (§ 10 Absatz 2 Satz 2 WaffG)

75,00

14.1.1.4

für Jäger

14.1.1.4.1

nach § 13 Absatz 2 WaffG einschließlich der Erwerbserlaubnis für die erste Kurzwaffe

40,00

14.1.1.4.2

nach § 13 Absatz 3 Satz 2 einschließlich der Eintragung der ersten Schusswaffe

30,00

14.1.1.5

für Sportschützen

14.1.1.5.1

nach § 10 Absatz 1 i.
V.
m.
§ 14 Absatz 2 für die erste Waffe

45,00

14.1.1.5.2

nach § 10 Absatz 1 i.
V.
m.
§ 14 Absatz 3 WaffG

45,00 je Schusswaffe

14.1.1.5.3

nach § 10 Absatz 1 i.
V.
m.
§ 14 Absatz 4 WaffG

60,00

14.1.1.6

für Brauchtumsschützen  
nach § 10 Absatz 1 i.
V.
m.
§ 16 Absatz 1 WaffG für die erste Waffe

45,00

14.1.1.7

für Waffensammler (§ 17 Absatz 2 WaffG)

240,00

14.1.1.8

für Erwerber einer Waffensammlung infolge Erbfalls (§ 17 Ab-satz 3 WaffG)

150,00

14.1.1.9

für Waffen- oder Munitionssachverständige (§ 18 Absatz 2 Satz 2 WaffG)

50,00 bis 200,00

14.1.1.10

für Erwerber von Waffen infolge Erbfalls (§ 20 Absatz 2 WaffG)

60,00

14.1.1.11

über Waffen nach Anlage 2 Abschnitt 2 Unterabschnitt 3 Num-mer 1.1 WaffG für die erste Waffe

40,00

14.1.2

Eintragung in eine bereits erteilte Waffenbesitzkarte/Austragung

14.1.2.1

einer Berechtigung zum Erwerb oder Besitz einer weiteren Waffe

45,00

14.1.2.2

einer Berechtigung einer weiteren Person zum Erwerb und/oder Besitz einer oder mehrerer in der Waffenbesitzkarte eingetragener Schusswaffen (§ 10 Absatz 2 Satz 1 WaffG)

35,00

14.1.2.3

einer Berechtigung zum Besitz einer weiteren Waffe (§ 13 Absatz 3 Satz 2 WaffG)

15,00

14.1.2.4

einer Berechtigung zum Erwerb und Besitz von Munition für eine in der Waffenbesitzkarte eingetragene Schusswaffe (§ 10 Absatz 3 Satz 1 WaffG)

15,00

14.1.2.5

einer erworbenen Waffe, soweit die Eintragung nicht durch die bei der Ausstellung der Waffenbesitzkarte entrichteten Gebühr abgegolten ist (§ 10 Absatz 1a WaffG)

25,00

14.1.2.6

infolge Erbfalls erworbener Waffen (§ 20 WaffG)

30,00 je Waffe

14.1.2.7

eines Wechsel- oder Austauschlaufes oder einer Wechseltrommel nach Anlage 2 Abschnitt 2 Unterabschnitt 2 Nummer 2 WaffG

15,00

14.1.2.8

Austragung einer oder mehrerer Waffen, eines Wechsel- oder Austauschlaufes oder einer Wechseltrommel

15,00 je Waffe, Lauf, Trommel

14.1.3

Umschreibung einer Waffenbesitzkarte

14.1.3.1

über vereinseigene Schusswaffen beim Übergang der Aufsicht über die Schusswaffen auf ein Vereinsmitglied, das bereits eine waffenrechtliche Erlaubnis besitzt (§ 10 Absatz 2 Satz 2 WaffG)

30,00

14.1.3.2

auf Grund einer Änderung des Sammelthemas bei Waffensammlern (§ 17 Absatz 2 WaffG)

150,00

14.1.4

Erteilung einer Erlaubnis zum Erwerb von erlaubnispflichtigen Schusswaffen (§ 11 Absatz 1 und 2 WaffG)

20,00

14.1.5

Ausstellung eines Munitionserwerbsscheines

14.1.5.1

für eine natürliche Person (§ 10 Absatz 3 Satz 2 WaffG)

30,00

14.1.5.2

für Munitionssammler (§ 17 Absatz 2 WaffG)

180,00

14.1.5.3

für Munitionssachverständige (§ 18 Absatz 2 WaffG)

50,00

14.2

Führen und Schießen

14.2.1

Ausstellung eines Waffenscheines

14.2.1.1

für gefährdete Personen (§ 10 Absatz 4 Satz 1 i.
V.
m.
§ 19 WaffG)

100,00

14.2.1.2

für Bewachungsunternehmer und -personal (§ 10 Absatz 4 Satz 1 i.
V.
m.
§ 28 WaffG)

200,00

14.2.2

Verlängerung der Geltungsdauer eines Waffenscheines

14.2.2.1

für gefährdete Personen (§ 10 Absatz 4 Satz 1 i.
V.
m.
§ 19 WaffG)

80,00

14.2.2.2

für Bewachungsunternehmer und -personal (§ 10 Absatz 4 Satz 1 i.
V.
m.
§ 28 WaffG)

150,00

14.2.3

Zustimmung zur Überlassung von Schusswaffen (§ 28 Absatz 3 WaffG)

30,00 je Person

14.2.4

nachträgliche Aufnahme eines Zusatzes in den Waffenschein (§ 28 Absatz 4 WaffG)

50,00

14.2.5

Ausstellung eines Kleinen Waffenscheines (§ 10 Absatz 4 Satz 1 WaffG)

50,00

14.2.6

Erlaubnis zum Schießen außerhalb von Schießstätten (§ 10 Absatz 5, § 16 Absatz 3 WaffG)

30,00 bis 200,00

14.3

Schießstätten

14.3.1

Erlaubnis zum Betrieb oder zur wesentlichen Änderung einer Schießstätte einschließlich der Abnahmeprüfung durch die zuständige Behörde (§ 27 Absatz 1 Satz 1 WaffG)

50,00 bis 1 000,00

14.3.2

Regel- und Sonderprüfungen von Schießstätten (§ 27 WaffG  
i.
V.
m.
§ 12 Absatz 1 Satz 2 und 3 AWaffV)

50,00 bis 500,00

14.4

Gewerbsmäßige/nichtgewerbsmäßige Waffenherstellung/Waffenhandel

14.4.1

Erlaubnis zur Herstellung, Bearbeitung oder Instandsetzung von Schusswaffen oder Munition (§ 21 Absatz 1 erster Halbsatz WaffG)

100,00 bis 3 000,00

14.4.2

als Stellvertretererlaubnis (§ 21 Absatz 1 erster Halbsatz i.
V.
m.
§ 21a WaffG)

100,00 bis 3 000,00

14.4.3

Erlaubnis zum Handel mit Schusswaffen oder Munition (§ 21 Absatz 1 zweiter Halbsatz WaffG)

100,00 bis 3 000,00

14.4.4

als Stellvertretererlaubnis (§ 21 Absatz 1 zweiter Halbsatz i.
V.
m.
§ 21a WaffG)

100,00 bis 3 000,00

14.4.5

Bewilligung einer Fristverlängerung (§ 21 Absatz 5 Satz 2 oder § 21a i.
V.
m.
§ 21 Absatz 5 WaffG)

25 Prozent der nach den Tarifstellen 14.4.1 bis 14.4.4 festgesetzten Gebühr

14.4.6

Abstempeln von Karteiblättern von Waffenherstellungs- und Waffenhandelsbüchern (§ 17 Absatz 2 Satz 2 AWaffV)

15,00 je angefangene 50 Stück

14.4.7

Erlaubnis zum nichtgewerbsmäßigen Herstellen, Bearbeiten oder Instandsetzen von Schusswaffen (§ 26 Absatz 1 Satz 1 WaffG)

50,00 bis 300,00

14.5

Entscheidungen im Zusammenhang mit dem Verbringen oder der Mitnahme von Waffen in den, durch den oder aus dem Geltungsbereich des WaffG

14.5.1

Erlaubnis zum Verbringen in die Bundesrepublik Deutschland (§ 29 WaffG)

30,00

14.5.2

Erlaubnis zum Verbringen durch die Bundesrepublik Deutschland

30,00

14.5.3

Erlaubnis zum Verbringen in einen anderen EU-Mitgliedstaat (§ 30 Absatz 1 und 2 WaffG)

30,00

14.5.4

_nicht belegt_

 

14.5.5

als allgemeine Erlaubnis für gewerbsmäßige Waffenhersteller oder Waffenhändler (§ 30 Absatz 2 und § 31 Absatz 3 WaffG)

80,00

14.6

Entscheidungen im Zusammenhang mit dem Europäischen Feuerwaffenpass

14.6.1

Erlaubnis zur Mitnahme von erlaubnispflichtigen Schusswaffen und Munition in die oder durch die Bundesrepublik Deutschland durch den Inhaber eines von einem EU-Mitgliedstaat ausgestellten Europäischen Feuerwaffenpasses (§ 32 Absatz 1 und 2 WaffG)

15,00

14.6.2

Ausstellung eines Europäischen Feuerwaffenpasses

50,00

14.6.3

Ein- und Austragung einer oder mehrerer Schusswaffen in den oder aus dem Europäischen Feuerwaffenpass

15,00

14.6.4

Verlängerung der Geltungsdauer eines Europäischen Feuerwaffenpasses (§ 32 Absatz 6 WaffG i.
V.
m.
§ 33 Absatz 1 Satz 2 AWaffV)

10,00

14.6.5

Verlängerung der Geltungsdauer der Einzelgenehmigung im Feld 4 des Europäischen Feuerwaffenpasses (§ 32 Absatz 1 Satz 2 WaffG)

10,00

14.6.6

Änderung der sonstigen Eintragungen im Europäischen Feuerwaffenpass (z.
B.
§ 33 Absatz 1 Satz 3 AWaffV)

10,00

14.7

Zulassungen von Ausnahmen

14.7.1

von waffenrechtlichen Alterserfordernissen (§ 3 Absatz 3 WaffG)

50,00

14.7.2

von den Erlaubnispflichten (§ 12 Absatz 5 WaffG)

40,00 bis 150,00

14.7.3

für Veranstaltungen der Brauchtumspflege (§ 16 Absatz 2 WaffG)

80,00

14.7.4

von der Blockierpflicht (§ 20 Absatz 7 Satz 1 WaffG)

15,00

14.7.5

von der Blockierpflicht bei einer infolge Erbfalls erworbenen Waffensammlung (§ 20 Absatz 7 Satz 2 WaffG)

20,00

14.7.6

Eintragen der Sicherung einer Schusswaffe (§ 20 Absatz 6 WaffG)

10,00

14.7.7

vom Alterserfordernis für das Schießen auf Schießstätten (§ 27 Absatz 4 WaffG)

25,00

14.7.8

von den Beschränkungen des § 9 Absatz 2 AWaffV beim Schießen auf Schießstätten

70,00

14.7.9

von den Handelsverboten (§ 35 Absatz 3 Satz 2 WaffG)

40,00 bis 250,00

14.7.10

von dem Verbot des Führens von Waffen auf öffentlichen Veranstaltungen (§ 42 Absatz 2 WaffG)

40,00 bis 120,00

14.8

Prüfungen/Überprüfungen/Anerkennungen

14.8.1

Regelüberprüfung (§ 4 Absatz 3 WaffG)

25,00 bis 50,00

14.8.2

Überprüfung des Fortbestehens des Bedürfnisses (§ 4 Absatz 4 WaffG)

20,00 bis 40,00

14.8.3

Abnahme einer Sachkundeprüfung (§ 7 WaffG i.
V.
m.
§ 2 AWaffV)

70,00 bis 250,00

14.8.4

Abnahme einer Fachkundeprüfung (§ 22 Absatz 1 WaffG i.
V.
m.
§ 16 AWaffV)

50,00 bis 300,00

14.8.5

Anerkennung von Lehrgängen (§ 7 WaffG i.
V.
m.
§ 3 Absatz 2 AWaffV)

500,00 bis 1 500,00

14.8.6

Kontrolle der Aufbewahrung (§ 36 Absatz 3 WaffG)

75,00

14.9

Gestattungen

14.9.1

Zulassung einer gleichwertigen oder abweichenden Aufbewahrung (§ 13 Absatz 5 bis 8, § 14 AWaffV)

30,00 bis 150,00

14.9.2

Gestattung der Teilnahme am Lehrgang im Verteidigungsschießen (§ 23 Absatz 2 AWaffV)

30,00 bis 150,00

14.10

Anordnungen

14.10.1

_nicht belegt_

 

14.10.2

zur Abwehr von Gefahren (§ 9 Absatz 3 WaffG)

20,00 bis 100,00

14.10.3

zur Kennzeichnungspflicht (§ 25 Absatz 2 WaffG)

20,00 bis 50,00

14.10.4

zur Aufbewahrung (§ 36 Absatz 6 WaffG)

20,00 bis 100,00

14.10.5

nach § 37 Absatz 1 Satz 2 WaffG

gebührenfrei

14.10.6

nach § 39 Absatz 3 WaffG

20,00 bis 100,00

14.10.7

nach § 40 Absatz 5 Satz 2 WaffG

gebührenfrei

14.10.8

nach § 41 Absatz 1 Satz 1 und Absatz 2 WaffG

50,00 bis 220,00

14.10.9

nach § 46 Absatz 2 und 3

20,00 bis 100,00

14.11

Untersagungen  
nach § 10 Absatz 4, nach § 12 Absatz 2 und § 25 Absatz 1 AWaffV

40,00 bis 80,00

14.12

Sicherstellung eines oder mehrerer Gegenstände  
nach § 36 Absatz 1 Satz 2, § 40 Absatz 5 Satz 2, § 46 Absatz 2 Satz 2 und Absatz 3 Satz 2 sowie Absatz 4 Satz 1 WaffG

20,00 bis 100,00

14.13

Einziehungen und Verwertungen  
eines oder mehrerer Gegenstände nach § 37 Absatz 1 Satz 3 und 4 sowie § 46 Absatz 5 Satz 1 WaffG

20,00 bis 50,00

14.14

Sonstige Fälle

14.14.1

Widerruf oder Rücknahme einer waffenrechtlichen Erlaubnis (§ 45 WaffG)

40,00 bis 100,00

14.14.2

Ausstellung einer Ersatzbescheinigung für eine in Verlust geratene, unlesbar oder anderweitig unbrauchbar gewordene waffenrechtliche Erlaubnis

50 Prozent der Gebühr für die jeweilige Erlaubnis

14.14.3

Ausstellung eines Folgedokuments für eine bereits vorhandene waffenrechtliche Erlaubnis (sofern Platz nicht ausreicht)

5,00 bis 50,00

14.14.4

Ausstellung einer Bescheinigung (§ 55 Absatz 2 Satz 1 und § 56 WaffG)

gebührenfrei

14.14.5

öffentliche Leistungen in Bezug auf Waffen und Munition, die im dienstlichen Interesse von öffentlichen Bediensteten verwendet werden

gebührenfrei

**15**

**Behördliche Namensänderungen**  
  
Öffentliche Leistungen nach dem Gesetz über die Änderung von Familiennamen und Vornamen (NamÄndG)

 

15.1

Änderung oder Feststellung eines Familiennamens nach den §§ 1 und 8 des Namensänderungsgesetzes

2,50 bis 1 050,00

15.2

Änderung eines Vornamens nach § 11 des Namensänderungsgesetzes

2,50 bis 275,00