## Verordnung über Vorlagen und Nachweise in bauaufsichtlichen Verfahren im Land Brandenburg (Brandenburgische Bauvorlagenverordnung - BbgBauVorlV)

Auf Grund des § 66 Absatz 1 Satz 1, des § 86 Absatz 2 Satz 4 Nummer 3 und Absatz 3 der Brandenburgischen Bauordnung vom 19.
Mai 2016 (GVBl.
I Nr. 14) verordnet die Ministerin für Infrastruktur und Landesplanung:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeines

[§ 1 Begriff, Beschaffenheit](#1)

[§ 2 Einreichung des Antrages oder der Anzeige](#2)

### Abschnitt 2  
Vorzulegende Bauvorlagen

[§ 3 Bauliche Anlagen](#3)

[§ 4 Werbeanlagen](#4)

[§ 5 Vorbescheid](#5)

[§ 6 Beseitigung von Anlagen](#6)

### Abschnitt 3  
Inhalt der Bauvorlagen

[§ 7 Auszug aus der Liegenschaftskarte, Lageplan](#7)

[§ 8 Bauzeichnungen](#8)

[§ 9 Baubeschreibung, Betriebsbeschreibung](#9)

[§ 10 Standsicherheitsnachweis](#10)

[§ 11 Brandschutznachweis](#11)

[§ 12 Nachweise für Schall-, Erschütterungsschutz sowie für Energieeinsparung](#12)

[§ 13 Übereinstimmungsgebot](#13)

### Abschnitt 4  
Bauzustandsanzeigen

[§ 14 Baubeginnsanzeige](#14)

[§ 15 Anzeige der beabsichtigten Nutzungsaufnahme](#15)

### Abschnitt 5  
Aufbewahrungspflicht

[§ 16 Aufbewahrungspflicht](#16)

### Abschnitt 6  
Prüfung bautechnischer Nachweise

[§ 17 Prüfverzicht](#17)

[§ 18 Prüfung bautechnischer Nachweise, Bauüberwachung](#18)

### Abschnitt 7  
Schlussbestimmungen

[§ 19 Inkrafttreten, Außerkrafttreten](#19)

[Anlage 1 Zeichen und Farben für Bauvorlagen](#20)

[Anlage 2 Kriterienkatalog](#21)

[Anlage 3 Besondere Bauvorlagen](#22)

### Abschnitt 1  
Allgemeines

#### § 1 Begriff, Beschaffenheit

(1) Bauvorlagen sind die einzureichenden Unterlagen, die für die Beurteilung des Bauvorhabens und die Bearbeitung eines Antrages oder einer Anzeige in den Verfahren nach der Brandenburgischen Bauordnung sowie für die Anzeige der beabsichtigten Beseitigung gemäß § 6 erforderlich sind.
Zu den Bauvorlagen zählen auch die besonderen Bauvorlagen für eingeschlossene Entscheidungen gemäß Anlage 3.
Bautechnische Nachweise nach den §§ 10 und 11 sowie die Nachweise für die Energieeinsparung und zur Nutzung Erneuerbarer Energien nach § 12 gelten auch dann als Bauvorlagen, wenn sie der Bauaufsichtsbehörde nicht vorzulegen sind.

(2) Bauvorlagen müssen aus alterungsbeständigem Papier oder gleichwertigem Material lichtbeständig hergestellt sein und dem Format DIN A4 entsprechen oder auf diese Größe gefaltet sein.

(3) Hat die oberste Bauaufsichtsbehörde Vordrucke veröffentlicht, sind diese zu verwenden.

(4) Die Bauaufsichtsbehörde darf ein Modell oder weitere Nachweise verlangen, wenn dies zur Beurteilung des Bauvorhabens erforderlich ist.

(5) Die Bauaufsichtsbehörde soll auf Bauvorlagen verzichten, wenn diese zur Beurteilung des Bauvorhabens nicht erforderlich sind.

(6) Lage- oder Höhenangaben sind im geodätischen Bezugssystem des amtlichen Vermessungswesens anzugeben.

#### § 2 Einreichung des Antrages oder der Anzeige

(1) Der Antrag oder die Anzeige ist schriftlich bei der zuständigen Bauaufsichtsbehörde einzureichen.
§ 1 Absatz 1 Satz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg in Verbindung mit § 3a des Verwaltungsverfahrensgesetzes bleibt unberührt.
Die untere Bauaufsichtsbehörde kann eine elektronische Einreichung des Antrags oder der Anzeige sowie der Bauvorlagen in Textform zulassen und Vorgaben zur elektronischen Einreichung machen.

(2) Der Antrag oder die Anzeige ist mit den erforderlichen Bauvorlagen in dreifacher Ausfertigung bei der zuständigen Bauaufsichtsbehörde einzureichen.
Die Bauaufsichtsbehörde kann weitere Ausfertigungen verlangen, soweit dies zur gleichzeitigen Beteiligung von Stellen nach § 69 Absatz 3 der Brandenburgischen Bauordnung erforderlich ist.
Der Antrag oder die Anzeige muss von der Bauherrin oder dem Bauherrn unterschrieben sein.
Ist die Bauherrin oder der Bauherr nicht Grundstückseigentümerin oder Grundstückseigentümer, so kann die schriftliche Zustimmung der Grundstückseigentümerin oder des Grundstückseigentümers zu dem Bauvorhaben gefordert werden.
Die Bauvorlagen müssen eine Angabe über die Entwurfsverfasserin oder den Entwurfsverfasser enthalten, die Bauvorlageberechtigung ist anzugeben, soweit diese gemäß § 65 Absatz 1 der Brandenburgischen Bauordnung erforderlich ist.
Bauvorlagen gemäß § 54 Absatz 2 der Brandenburgischen Bauordnung müssen eine Angabe über die Fachplanerinnen oder den Fachplaner enthalten.

(3) Zusätzlich sind die Bauvorlagen in elektronischer Form im Portable Document Format (PDF oder PDF/A) vorzulegen.
Dateianlagen innerhalb der PDF-Dateien sind unzulässig.

(4) Mit dem Antrag auf Prüfung der bautechnischen Nachweise durch die Bauaufsichtsbehörde oder einer Prüfingenieurin oder einen Prüfingenieur sind die bautechnischen Nachweise in zweifacher Ausfertigung einzureichen.
Bei Einreichung als elektronisches Dokument kann die Bauaufsichtsbehörde oder die Prüfingenieurin oder der Prüfingenieur eine § 1 Absatz 2 entsprechende Ausfertigung verlangen.

(5) Ist die amtsfreie Gemeinde, das Amt oder die Verbandsgemeinde, die mitverwaltete oder die mitverwaltende Gemeinde als Sonderordnungsbehörde zuständig, so ist der Antrag oder die Anzeige mit den erforderlichen Bauvorlagen in zweifacher Ausfertigung bei der amtsfreien Gemeinde, dem Amt oder der Verbandsgemeinde, der mitverwalteten oder der mitverwaltenden Gemeinde einzureichen.
Absatz 1 Satz 3 gilt entsprechend.

(6) Anträge auf Erteilung einer Ausführungsgenehmigung für Fliegende Bauten gemäß § 76 der Brandenburgischen Bauordnung sind bei der Prüfstelle für Fliegende Bauten einzureichen.
Dem Antrag sind die Bauvorlagen beizufügen, die in der Verwaltungsvorschrift über Ausführungsgenehmigungen für Fliegende Bauten und deren Gebrauchsabnahmen aufgeführt sind.
Die Prüfstelle kann zusätzliche Bauvorlagen verlangen, wenn dies zur Beurteilung des Antrages erforderlich ist.
Absatz 1 Satz 3 gilt entsprechend.

(7) Der Antrag auf Erteilung einer Typenprüfung gemäß § 66 Absatz 4 Satz 3 der Brandenburgischen Bauordnung sowie der Antrag auf Erteilung einer Typengenehmigung gemäß § 72a der Brandenburgischen Bauordnung ist mit den Bauvorlagen nach § 3 Absatz 3 bei der nach § 1 Absatz 2 der Brandenburgischen Bauzuständigkeitsverordnung zuständigen Stelle einzureichen.
Die von der nach § 1 Absatz 2 der Brandenburgischen Bauzuständigkeitsverordnung zuständigen Stelle veröffentlichten Anforderungen und Hinweise für einen Antrag auf Erteilung einer Typenprüfung und einer Typengenehmigung sind zu beachten.
Absatz 1 Satz 3 gilt entsprechend.

### Abschnitt 2  
Vorzulegende Bauvorlagen

#### § 3 Bauliche Anlagen

(1) Vorzulegen sind:

1.  ein Auszug aus der Liegenschaftskarte,
2.  ein amtlicher Lageplan gemäß § 7 Absatz 3, soweit erforderlich,
3.  ein objektbezogener Lageplan gemäß § 7 Absatz 6,
4.  die Bauzeichnungen gemäß § 8,
5.  die Baubeschreibung gemäß § 9,
6.  der Nachweis der Standsicherheit gemäß § 10, soweit er bauaufsichtlich geprüft wird,
7.  der Nachweis des Brandschutzes gemäß § 11, soweit er bauaufsichtlich geprüft wird, oder, im Fall des § 66 Absatz 2 Satz 3 der Brandenburgischen Bauordnung, die Erklärung zum Brandschutznachweis,
8.  die erforderlichen Angaben über die gesicherte Erschließung hinsichtlich der Versorgung mit Wasser und Energie sowie der Entsorgung von Schmutz- und Niederschlagswasser und der verkehrsmäßigen Erschließung, soweit das Bauvorhaben nicht an eine öffentliche Wasser- oder Energieversorgung oder eine öffentliche Abwasserentsorgungsanlage angeschlossen werden kann oder nicht in ausreichender Breite an einer öffentlichen Verkehrsfläche liegt,
9.  bei Bauvorhaben im Geltungsbereich eines Bebauungsplanes, der Festsetzungen zum Maß der baulichen Nutzung enthält, eine Berechnung des zulässigen, des vorhandenen und des geplanten Maßes der baulichen Nutzung,
10.  bei Gebäuden der Erhebungsbogen für die Bautätigkeitsstatistik gemäß Hochbaustatistikgesetz.

(2) Bei gewerblichen Betrieben und Anlagen sowie bei landwirtschaftlichen Betrieben ist dem Bauantrag eine Betriebsbeschreibung gemäß § 9 beizufügen.
Bei Sonderbauten sind dem Bauantrag die zusätzlichen Bauvorlagen beizufügen, die durch eine für den Sonderbau geltende Rechtsverordnung vorgeschrieben sind.

(3) Mit dem Antrag nach § 2 Absatz 7 auf Erteilung einer Typengenehmigung sind insbesondere die Bauvorlagen nach Absatz 1 Nummer 4 bis 7 und, soweit es für die Beurteilung erforderlich ist, die Bauvorlagen nach Absatz 2 vorzulegen.
Ist eine Typengenehmigung nach § 72a der Brandenburgischen Bauordnung erteilt worden und gemäß § 72a Absatz 4 der Brandenburgischen Bauordnung Gegenstand des Verfahrens in der unteren Bauaufsichtsbehörde, ist die Typengenehmigung dem Antrag auf Baugenehmigung beizufügen.

#### § 4 Werbeanlagen

(1) Vorzulegen sind:

1.  ein Auszug aus der Liegenschaftskarte mit Einzeichnung des Standortes,
2.  eine Zeichnung gemäß Absatz 2 und Beschreibung gemäß Absatz 3 oder eine andere geeignete Darstellung der Werbeanlage, wie ein farbiges Lichtbild oder eine farbige Lichtbildmontage,
3.  der Nachweis der Standsicherheit gemäß § 10, soweit er bauaufsichtlich geprüft wird, andernfalls die Erklärung der Tragwerksplanerin oder des Tragwerksplaners nach Maßgabe des Kriterienkataloges der Anlage 2.

(2) Die Zeichnung muss die Darstellung der Werbeanlage und ihre Maße, auch bezogen auf den Standort und auf Anlagen, an denen die Werbeanlage angebracht oder in deren Nähe sie aufgestellt werden soll, sowie Angaben über die Farbgestaltung enthalten.

(3) In der Beschreibung sind die Art und die Beschaffenheit der Werbeanlage, sowie, soweit erforderlich, die Abstände zu öffentlichen Verkehrsflächen anzugeben.

#### § 5 Vorbescheid

Vorzulegen sind diejenigen Bauvorlagen, die zur Beurteilung der durch den Vorbescheid zu entscheidenden Fragen des Bauvorhabens erforderlich sind.

#### § 6 Beseitigung von Anlagen

(1) Vorhaben zur Beseitigung baulicher Anlagen sind der Bauaufsichtsbehörde spätestens einen Monat vor Beginn der Bauarbeiten unter Verwendung des veröffentlichten Vordrucks anzuzeigen.
Eine Anzeigepflicht besteht nicht für die Beseitigung von

1.  baulichen Anlagen sowie anderen Anlagen und Einrichtungen, deren Errichtung nach § 61 der Brandenburgischen Bauordnung genehmigungsfrei ist,
2.  Gebäuden mit nicht mehr als 500 Kubikmeter umbautem Raum und Wohngebäuden mit nicht mehr als 1 000 Kubikmeter umbautem Raum,
3.  ortsfesten Behältern mit nicht mehr als 300 Kubikmeter Behälterinhalt, ausgenommen Behälter zur Lagerung wassergefährdender Stoffe im Sinne des § 62 des Wasserhaushaltsgesetzes.

Satz 2 gilt nicht für Baudenkmäler und für bauliche Anlagen, die unter Verwendung gesundheitsgefährdender Baustoffe errichtet worden sind.

(2) Der Anzeige sind folgende Bauvorlagen beizufügen:

1.  ein aktueller Auszug aus der Liegenschaftskarte im Maßstab 1 : 1 000 mit Kennzeichnung der zu beseitigenden baulichen Anlage,
2.  der Erhebungsbogen für die Bautätigkeitsstatistik gemäß Hochbaustatistikgesetz.

### Abschnitt 3  
Inhalt der Bauvorlagen

#### § 7 Auszug aus der Liegenschaftskarte, Lageplan

(1) Der aktuelle Auszug aus der Liegenschaftskarte muss das Baugrundstück und die benachbarten Grundstücke im Umkreis von mindestens 50 Metern darstellen.
Das Baugrundstück ist zu kennzeichnen.

(2) Der Lageplan ist auf der Grundlage der Liegenschaftskarte zu erstellen.
Dabei ist ein Maßstab von mindestens 1: 500 zu verwenden.
Ein anderer Maßstab ist zu wählen, wenn es für die Beurteilung des Vorhabens erforderlich ist.

(3) Der amtliche Lageplan enthält Tatbestände an Grund und Boden, die durch vermessungstechnische Ermittlungen festgestellt worden sind oder auf solche Ermittlungen zurückgehen und die mit öffentlichem Glauben beurkundet sind.
Der amtliche Lageplan ist von einer Katasterbehörde oder von im Land Brandenburg zugelassenen Öffentlich bestellten Vermessungsingenieuren anzufertigen.
Er muss folgende Angaben enthalten:

1.  Maßstab, Maßstabsleiste und Lage des Grundstücks zur Nordrichtung,
2.  im Grundbuch geführte Bezeichnung des Baugrundstücks und der benachbarten Grundstücke mit den Eigentümerangaben zum Baugrundstück,
3.  katastermäßige Flächengrößen, Flurstücksnummern und die Flurstücksgrenzen des Baugrundstücks und der benachbarten Grundstücke,
4.  Angaben zu nicht festgestellten Grenzen nach dem Liegenschaftskataster,
5.  Höhenlage der Grenzpunkte des Baugrundstücks und bei größeren Grundstücken die Höhenlage des engeren Baufeldes,
6.  angrenzende öffentliche Verkehrsflächen mit Angabe der Breite, der Straßengruppe und der Höhenlage,
7.  Festsetzungen eines Bebauungsplanes oder einer Satzung für das Baugrundstück über Art und Maß der baulichen Nutzung sowie die überbaubaren und nicht überbaubaren Grundstücksflächen,
8.  Flächen auf dem Baugrundstück, die von Dienstbarkeiten oder Baulasten betroffen sind,
9.  durch Rechtsverordnung oder Satzung geschützte Landschaftsbestandteile sowie Wald auf dem Baugrundstück,
10.  vorhandene bauliche Anlagen auf dem Baugrundstück und deren Abstandsflächen sowie die für die Beurteilung des Vorhabens bedeutsamen vorhandenen baulichen Anlagen auf den Nachbargrundstücken und deren Abstandsflächen.

In den amtlichen Lageplan können die geplanten baulichen Anlagen und weitere Angaben aus dem objektbezogenen Lageplan nach Absatz 5 aufgenommen werden.
Diese Eintragungen nehmen an der Beurkundung mit öffentlichem Glauben nicht teil.
Enthält der amtliche Lageplan die zur Beurteilung des Bauvorhabens erforderlichen Angaben, ist ein objektbezogener Lageplan nicht erforderlich.

(4) Für die Angaben nach Absatz 3 Satz 3 Nummer 3 und 10 ist eine örtliche Vermessung nicht erforderlich, wenn die für die Beurteilung des Vorhabens erheblichen

1.  Grundstücksgrenzen im Liegenschaftskataster zuverlässig nachgewiesen sind und
2.  die baulichen Anlagen auf dem Baugrundstück und den benachbarten Grundstücken im Liegenschaftskataster zuverlässig nachgewiesen sind.

Grundstücksgrenzen sind im Liegenschaftskataster in der Regel zuverlässig nachgewiesen, wenn die Grenzen festgestellt sind oder als festgestellt gelten und ihre Grenzpunkte mit der erforderlichen Genauigkeit im geodätischen Bezugssystem des amtlichen Vermessungswesens vorliegen.
Sind die für die bauaufsichtliche Beurteilung erheblichen Grundstücksgrenzen im Liegenschaftskataster nicht zuverlässig nachgewiesen, sind ergänzende vermessungstechnische Untersuchungen erforderlich.
Im amtlichen Lageplan ist das Ergebnis der Grenzuntersuchung darzustellen.

(5) Ein amtlicher Lageplan ist nicht erforderlich, wenn

1.  durch das Vorhaben die Lage und die äußeren Abmessungen eines vorhandenen Gebäudes und die Abstandsflächen nicht geändert werden,
2.  der Eigentümer eines Nachbargrundstücks dem Vorhaben zur Errichtung einer Grenzbebauung nach § 6 Absatz 8 der Brandenburgischen Bauordnung in weniger als 3 Meter Abstand zur Grundstücksgrenze zugestimmt hat.

(6) Der objektbezogene Lageplan ist auf der Grundlage des amtlichen Lageplanes anzufertigen.
Ist ein amtlicher Lageplan nicht erforderlich oder wurde auf ihn nach § 1 Absatz 5 verzichtet, ist der objektbezogene Lageplan auf der Grundlage der Liegenschaftskarte anzufertigen.
Er muss folgende Angaben enthalten, soweit dies für die Beurteilung des Vorhabens erforderlich ist:

1.  Maßstab, Maßstabsleiste und Lage des Grundstücks zur Nordrichtung,
2.  Flurstücksnummern und Flurstücksgrenzen des Baugrundstücks und der benachbarten Grundstücke,
3.  vorhandene bauliche Anlagen auf dem Baugrundstück und den benachbarten Grundstücken mit Angabe der Nutzung, First- und Außenwandhöhe, Dachform und der Art der Außenwände und der Bedachung,
4.  Grundrisse der geplanten baulichen Anlagen unter Angabe der Außenmaße, der Dachform, der Höhenlage des Erdgeschossfußbodens zur Straße,
5.  Aufteilung der nicht überbauten oder bepflanzten Flächen unter Angabe der Lage und Breite der Zu- und Abfahrten, der Anzahl, Lage und Größe der Kinderspielplätze, der Stellplätze, der Abstellplätze für Fahrräder und der Flächen für die Feuerwehr,
6.  Abstände der geplanten baulichen Anlage zu anderen baulichen Anlagen auf dem Baugrundstück und auf den benachbarten Grundstücken, zu den Nachbargrenzen sowie die Abstandsflächen,
7.  Denkmäler auf dem Baugrundstück und den benachbarten Grundstücken,
8.  Flächen auf dem Baugrundstück, für die durch Gesetz, Rechtsverordnung oder Satzung ein Bauverbot, eine Baubeschränkung oder ein Genehmigungs- oder Zustimmungsvorbehalt geregelt ist,
9.  Leitungen, die der öffentlichen Versorgung mit Wasser, Gas, Elektrizität, Wärme, der öffentlichen Abwasserentsorgung, der Telekommunikation und Rohrleitungen, die dem Ferntransport von Stoffen dienen sowie deren Abstände zu der geplanten baulichen Anlage,
10.  Hydranten und andere Wasserentnahmestellen für die Feuerwehr,
11.  Leitungen bis zum Anschluss an die Sammelkanalisation, Kleinkläranlagen und sonstige Abwasserbehandlungsanlagen sowie deren Leitungen, abflusslose Sammelgruben, Ausdehnung und Gefälle befestigter Flächen, Versickerungsanlagen einschließlich Vorreinigungsanlagen, Schächte und Abscheider, Angabe der Gefälle und Leitungsquerschnitte,
12.  Abstände der geplanten baulichen Anlage zur Uferlinie oberirdischer Gewässer.

Wird der objektbezogene Lageplan auf der Grundlage der Liegenschaftskarte angefertigt, muss er ferner die Angaben nach Absatz 3 Satz 3 Nummer 2, 3, 5 bis 9 enthalten, soweit dies für die Beurteilung des Vorhabens erforderlich ist.

(7) Die Angaben nach Absatz 6 Satz 3 Nummer 1 bis 5 sind in einem Außenanlagenplan darzustellen, wenn der Lageplan sonst unübersichtlich würde.

(8) Die Angaben nach Absatz 6 Satz 3 Nummer 1, 2, 10 und 11 sind in einem Grundstücksentwässerungsplan darzustellen, wenn der Lageplan sonst unübersichtlich würde.

(9) Im Lageplan sind die Zeichen und Farben der Anlage 1 zu verwenden; im Übrigen ist die Planzeichenverordnung anzuwenden.
Sonstige Darstellungen sind zu erläutern.

#### § 8 Bauzeichnungen

(1) Für die Bauzeichnungen ist ein Maßstab von mindestens 1 : 100 zu verwenden.
Ein größerer Maßstab ist zu wählen, wenn er zur Darstellung der erforderlichen Eintragung notwendig ist; ein kleinerer Maßstab kann gewählt werden, wenn er dafür ausreicht.

(2) In den Bauzeichnungen sind darzustellen:

1.  die Grundrisse aller Geschosse mit Angabe der vorgesehenen Nutzung der Räume und mit Einzeichnung der
    1.  Treppen,
    2.  lichten Öffnungsmaße der Türen sowie deren Art und Anordnung an und in Rettungswegen,
    3.  Abgasanlagen,
    4.  Räume für die Aufstellung von Feuerstätten unter Angabe der Nennleistung sowie der Räume für die Brennstofflagerung unter Angabe der vorgesehenen Art und Menge des Brennstoffes,
    5.  Aufzugsschächte, Aufzüge einschließlich der nutzbaren Grundflächen der Fahrkörbe von Personenaufzügen,
    6.  Installationsschächte, -kanäle und Lüftungsleitungen, soweit sie raumabschließende Bauteile durchdringen,
    7.  Räume für die Aufstellung von Lüftungsanlagen,
    8.  bei Wohngebäuden die barrierefrei nutzbaren Wohnungen;
2.  die Schnitte, aus denen auch ersichtlich sind
    1.  die Gründung der geplanten baulichen Anlage und, soweit erforderlich, die Gründungen benachbarter baulicher Anlagen,
    2.  der Anschnitt der vorhandenen und der geplanten Geländeoberfläche,
    3.  der höchste gemessene Grundwasserstand (HGW) oder der höchste zu erwartende Grundwasserstand (zeHGW) über NHN,
    4.  die Höhenlage des Erdgeschossfußbodens mit Bezug auf das Höhenbezugssystem gemäß § 1 Absatz 6,
    5.  die Höhe der Fußbodenoberkante des höchstgelegenen Geschosses, in dem ein Aufenthaltsraum möglich ist, über der geplanten Geländeoberfläche im Mittel,
    6.  die lichten Raumhöhen,
    7.  der Verlauf der Treppen und Rampen mit ihrem Steigungsverhältnis,
    8.  die Wandhöhe im Sinne des § 6 Absatz 4 Satz 2 der Brandenburgischen Bauordnung,
    9.  die Dachneigungen;
3.  die Ansichten der geplanten baulichen Anlage mit dem Anschluss an Nachbargebäude unter Angabe von Baustoffen und Farben sowie der vorhandenen und geplanten Geländeoberfläche und des Straßengefälles.

(3) In den Bauzeichnungen sind anzugeben:

1.  der Maßstab, die Maßstabsleiste und die Maße,
2.  die wesentlichen Bauprodukte und Bauarten,
3.  die Rohbaumaße der Fensteröffnungen in Aufenthaltsräumen,
4.  bei Änderung baulicher Anlagen die zu beseitigenden und die geplanten Bauteile.

(4) In den Bauzeichnungen sind die Zeichen und Farben der Anlage 1 zu verwenden.

#### § 9 Baubeschreibung, Betriebsbeschreibung

In der Bau- und Betriebsbeschreibung sind das Bauvorhaben und seine Nutzung zu erläutern, soweit dies zur Beurteilung erforderlich ist und die notwendigen Angaben nicht in den Lageplan und die Bauzeichnungen aufgenommen werden können.
Anzugeben sind die Anzahl, die Brutto-Grundfläche der Nutzungseinheiten und die Gebäudeklasse.
Es sind die Maßnahmen des barrierefreien Bauens zu beschreiben, soweit dies zur Beurteilung erforderlich ist und die notwendigen Angaben nicht in den Lageplan und die Bauzeichnungen aufgenommen werden können.

#### § 10 Standsicherheitsnachweis

(1) Für den Nachweis der Standsicherheit tragender Bauteile einschließlich ihrer Feuerwiderstandsfähigkeit nach § 26 Absatz 2 der Brandenburgischen Bauordnung sind eine Darstellung des gesamten statischen Systems sowie die erforderlichen Konstruktionszeichnungen, Berechnungen und Beschreibungen vorzulegen.

(2) Die statischen Berechnungen müssen die Standsicherheit der baulichen Anlagen und ihrer Teile nachweisen.
Die Beschaffenheit des Baugrundes und seine Tragfähigkeit sowie die Grundwasserverhältnisse sind anzugeben.
Soweit erforderlich, ist nachzuweisen, dass die Standsicherheit anderer baulicher Anlagen und die Tragfähigkeit des Baugrundes der Nachbargrundstücke nicht gefährdet werden.

(3) Die Standsicherheit kann auf andere Weise als durch statische Berechnungen nachgewiesen werden, wenn hierdurch die Anforderungen an einen Standsicherheitsnachweis in gleichem Maße erfüllt werden.

#### § 11 Brandschutznachweis

(1) Für den Nachweis des Brandschutzes sind im Lageplan, in den Bauzeichnungen und in der Baubeschreibung, soweit erforderlich, insbesondere anzugeben:

1.  das Brandverhalten der Baustoffe (Baustoffklasse) und die Feuerwiderstandsfähigkeit der Bauteile (Feuerwiderstandsklasse) entsprechend den Benennungen nach § 26 der Brandenburgischen Bauordnung oder entsprechend den Klassifizierungen nach den Anlagen zur Bauregelliste A Teil 1,
2.  die Bauteile, Einrichtungen und Vorkehrungen, an die Anforderungen hinsichtlich des Brandschutzes gestellt werden, wie Brandwände und Decken, Trennwände, Unterdecken, Installationsschächte und -kanäle, Lüftungsanlagen, Feuerschutzabschlüsse und Rauchschutztüren, Öffnungen zur Rauchableitung, einschließlich der Fenster nach § 35 Absatz 8 Satz 2 Nummer 1 der Brandenburgischen Bauordnung,
3.  die Nutzungseinheiten, die Brand- und Rauchabschnitte,
4.  die aus Gründen des Brandschutzes erforderlichen Abstände innerhalb und außerhalb des Gebäudes,
5.  der erste und zweite Rettungsweg nach § 33 der Brandenburgischen Bauordnung, insbesondere notwendige Treppenräume, Ausgänge, notwendige Flure, mit Rettungsgeräten der Feuerwehr erreichbare Stellen einschließlich der Fenster, die als Rettungswege nach § 33 Absatz 2 Satz 2 der Brandenburgischen Bauordnung dienen, unter Angabe der lichten Maße und Brüstungshöhen sowie die Höhe der Oberkante der Brüstung über Gelände,
6.  die Flächen für die Feuerwehr, Zu- und Durchgänge, Zu- und Durchfahrten, Bewegungsflächen und die Aufstellflächen für Hubrettungsfahrzeuge,
7.  die Löschwasserversorgung.

(2) Bei Sonderbauten, Mittel- und Großgaragen müssen, soweit es für die Beurteilung erforderlich ist, zusätzlich Angaben gemacht werden insbesondere über:

1.  brandschutzrelevante Einzelheiten der Nutzung, insbesondere auch die Anzahl und Art der die bauliche Anlage nutzenden Personen sowie Explosions- oder erhöhte Brandgefahren, Brandlasten, Gefahrstoffe und Risikoanalysen,
2.  Rettungswegbreiten und -längen, Einzelheiten der Rettungswegführung und -ausbildung einschließlich Sicherheitsbeleuchtung und -kennzeichnung,
3.  technische Anlagen und Einrichtungen zum Brandschutz, wie Branderkennung, Brandmeldung, Alarmierung, Brandbekämpfung, Rauchableitung, Rauchfreihaltung einschließlich des sicherheitstechnischen Steuerungskonzepts der Anlagen,
4.  die Sicherheitsstromversorgung,
5.  die Bemessung der Löschwasserversorgung, Einrichtungen zur Löschwasserentnahme sowie die Löschwasserrückhaltung,
6.  betriebliche und organisatorische Maßnahmen zur Brandverhütung, Brandbekämpfung und Rettung von Menschen und Tieren wie Feuerwehrplan, Brandschutzordnung, Werkfeuerwehr, Bestellung von Brandschutzbeauftragten und Selbsthilfekräften.

Anzugeben ist auch, weshalb es der Einhaltung von Vorschriften wegen der besonderen Art oder Nutzung baulicher Anlagen oder Räume oder wegen besonderer Anforderungen gemäß § 51 Absatz 1 Satz 2 der Brandenburgischen Bauordnung nicht bedarf.
Der Brandschutznachweis kann auch gesondert in Form eines objektbezogenen Brandschutzkonzeptes dargestellt werden.

(3) Abweichungen gemäß § 67 Absatz 1 und § 86a Absatz 1 Satz 3 der Brandenburgischen Bauordnung sind im Brandschutznachweis zu benennen und entsprechend zu begründen.

#### § 12 Nachweise für Schall-, Erschütterungsschutz sowie für Energieeinsparung

(1) Die Einhaltung der Anforderungen an den Schall- und Erschütterungsschutz nach den bauordnungsrechtlichen Vorschriften ist nachzuweisen.

(2) Die Einhaltung der Anforderungen zur Einsparung von Energie in Gebäuden sowie die Nutzung erneuerbarer Energien müssen nach den Vorschriften des Gebäudeenergiegesetzes nachgewiesen werden.

#### § 13 Übereinstimmungsgebot

Die Bauzeichnungen, Baubeschreibungen, Betriebsbeschreibungen, Berechnungen und Konstruktionszeichnungen sowie sonstige Zeichnungen, Beschreibungen und Belege, die den bautechnischen Nachweisen zugrunde liegen, müssen miteinander übereinstimmen und gleiche Positionsangaben haben.

### Abschnitt 4  
Bauzustandsanzeigen

#### § 14 Baubeginnsanzeige

(1) Soweit bautechnische Nachweise nicht bauaufsichtlich geprüft werden, ist eine Erklärung der jeweiligen Nachweiserstellerin oder des jeweiligen Nachweiserstellers nach § 66 Absatz 1 Satz 2 oder Absatz 2 der Brandenburgischen Bauordnung über die Erstellung des bautechnischen Nachweises spätestens mit der Baubeginnsanzeige gemäß § 72 Absatz 8 der Brandenburgischen Bauordnung vorzulegen.
Wird das Bauvorhaben abschnittsweise ausgeführt, muss dieErklärung spätestens bei Beginn der Ausführung des jeweiligen Bauabschnitts vorliegen.

(2) Für die nach § 72 Absatz 7 Satz 1 Nummer 3 der Brandenburgischen Bauordnung vorzulegenden Bescheinigungen nach § 66 Absatz 3 der Brandenburgischen Bauordnung gilt Absatz 1 Satz 2 entsprechend.

(3) Muss der Standsicherheitsnachweis bei Bauvorhaben nach § 66 Absatz 3 Satz 1 Nummer 2 der Brandenburgischen Bauordnung nicht bauaufsichtlich geprüft werden, ist spätestens mit der Baubeginnsanzeige eine Erklärung der Tragwerksplanerin oder des Tragwerksplaners hierüber nach Maßgabe des Kriterienkataloges der Anlage 2 vorzulegen.

#### § 15 Anzeige der beabsichtigten Nutzungsaufnahme

Sind bei einem Bauvorhaben wiederkehrende bauaufsichtliche Prüfungen durch Rechtsverordnung nach § 86 Absatz 1 Nummer 5 der Brandenburgischen Bauordnung oder im Einzelfall vorgeschrieben, ist mit der Anzeige nach § 83 Absatz 2 Satz 1 der Brandenburgischen Bauordnung über die in § 83 Absatz 2 Satz 2 der Brandenburgischen Bauordnung benannten Bescheinigungen und Bestätigungen hinaus der Brandschutznachweis nach § 11 vorzulegen, soweit er nicht bauaufsichtlich geprüft ist.

### Abschnitt 5  
Aufbewahrungspflicht

#### § 16 Aufbewahrungspflicht

Die Bauherrin oder der Bauherr und deren Rechtsnachfolger sind verpflichtet,

1.  bei baugenehmigungspflichtigen Bauvorhaben die Baugenehmigung und die Bauvorlagen,
2.  die Bauvorlagen zu einer Bauanzeige,
3.  die Prüfberichte von Prüfingenieurinnen oder Prüfingenieuren und Bauaufsichtsbehörden,
4.  die Bescheinigungen von Prüfsachverständigen,
5.  die Verwendbarkeitsnachweise für Bauprodukte und Bauarten, soweit sie Nebenbestimmungen für den Betrieb oder die Wartung enthalten,

und bis zur Beseitigung der baulichen Anlage oder einer die Genehmigungsfrage insgesamt neu aufwerfenden Änderung oder Nutzungsänderung aufzubewahren und auf Verlangen der Bauaufsichtsbehörde vorzulegen.
Die Bauherrin oder der Bauherr und deren Rechtsnachfolger sind verpflichtet, die Unterlagen nach Satz 1 bei einer rechtsgeschäftlichen Veräußerung des Bauvorhabens an den jeweiligen Rechtsnachfolger weiterzugeben.

### Abschnitt 6  
Prüfung bautechnischer Nachweise

#### § 17 Prüfverzicht

Einer Prüfung der bautechnischen Nachweise für Gebäude der Gebäudeklassen 1 und 2 ohne Aufenthaltsräume mit nicht mehr als 150 Quadratmeter Grundfläche sowie sonstiger baulicher Anlagen mit nicht mehr als 10 Meter Bauhöhe bedarf es nicht.

#### § 18

### Abschnitt 7  
Schlussbestimmungen

#### § 19 Inkrafttreten, Außerkrafttreten

(1) Die §§ 17 und 18 treten mit Wirkung vom 1.
Juli 2016 in Kraft.

(2) Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Brandenburgische Bauvorlagenverordnung vom 28.
Juli 2009 (GVBl.
II S. 494) außer Kraft.

(3) § 18 tritt am 31.
März 2017 außer Kraft.

Potsdam, den 7.
November 2016

Die Ministerin für Infrastruktur und Landesplanung  
Kathrin Schneider

* * *

### Anlagen

1

[Anlage 1 - Zeichen und Farben für Bauvorlagen](/br2/sixcms/media.php/68/GVBl_II_60_2016-Anlage-1.pdf "Anlage 1 - Zeichen und Farben für Bauvorlagen") 620.8 KB

2

[Anlage 2 - Kriterienkatalog](/br2/sixcms/media.php/68/GVBl_II_60_2016-Anlage-2.pdf "Anlage 2 - Kriterienkatalog") 579.0 KB

3

[Anlage 3 - Besondere Bauvorlagen](/br2/sixcms/media.php/68/GVBl_II_60_2016-Anlage-3.pdf "Anlage 3 - Besondere Bauvorlagen") 645.5 KB