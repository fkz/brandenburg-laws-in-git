## Verordnung zur Bemessung der Beiträge für die Gewässerunterhaltungsverbände (Beitragsbemessungsverordnung - BBV)

Auf Grund des § 80 Absatz 1a Satz 1 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20), der durch Artikel 1 des Gesetzes vom 4. Dezember 2017 (GVBl.
I Nr.
28) eingefügt worden ist, verordnet der Minister für Landwirtschaft, Umwelt und Klimaschutz im Benehmen mit dem Ausschuss für Landwirtschaft, Umwelt und Klimaschutz des Landtages:

#### § 1 Regelungsgegenstand, Anwendungsbereich

(1) Die Bemessung der Beiträge für die Gewässerunterhaltungsverbände bestimmt sich nach der Größe der Flächen, mit denen die Mitglieder am Verbandsgebiet beteiligt sind, und nach der Nutzungsartengruppe, der die Flächen im Liegenschaftskataster zugeordnet sind (§ 80 Absatz 1 Satz 1 des Brandenburgischen Wassergesetzes).
Diese Verordnung regelt die Zuordnung der Nutzungsartengruppen, der die beitragspflichtigen Flächen im Liegenschaftskataster zugeordnet sind, zu den Vorteilsgebietstypen gemäß § 80 Absatz 1 Satz 2 und 4 des Brandenburgischen Wassergesetzes und die Höhe der Beitragsbemessungsfaktoren für die Vorteilsgebietstypen.

(2) Die Regelungen gelten für die Erhebung der Beiträge der Gewässerunterhaltungsverbände gegenüber ihren Mitgliedern.
Für die Umlage der Beiträge durch die Gemeinde gilt § 80 Absatz 2 des Brandenburgischen Wassergesetzes.

#### § 2 Zuordnung der Nutzungsartengruppen zu den Vorteilsgebietstypen, Beitragsbemessungsfaktoren

(1) Die Zuordnung der Nutzungsartengruppen zu den drei Vorteilsgebietstypen ergibt sich aus der Anlage.
Die dort genannten Vorteilsgebietstypen sind abschließend.

(2) Die Beitragsbemessungsfaktoren pro Flächeneinheit für die Vorteilsgebietstypen ergeben sich aus der Anlage.

#### § 3 Liegenschaftskataster, Stichtag, mehrere Nutzungsartengruppen auf einem Grundstück

(1) Maßgeblich für die Beitragserhebung im Beitragsjahr sind die am 1.
Juni des Vorjahres im Liegenschaftskataster erfassten Nutzungsartengruppen (§ 80 Absatz 1 Satz 5 des Brandenburgischen Wassergesetzes).
Die tatsächliche Nutzung ist unbeachtlich.
Änderungen des Liegenschaftskatasters nach dem Stichtag werden erst im nachfolgenden Beitragsjahr berücksichtigt.

(2) Alle beitragspflichtigen Flächen sind entsprechend ihrer Zuordnung zu einer Nutzungsartengruppe einem Vorteilsgebietstyp zuzuordnen.
Sind mehrere Nutzungsartengruppen für ein Grundstück im Liegenschaftskataster verzeichnet, ist die Fläche anteilig entsprechend den amtlichen Flächenanteilen im Liegenschaftskataster den jeweiligen Vorteilsgebietstypen zuzuordnen.
Für diese Flächen gelten die Beitragsbemessungsfaktoren für den jeweiligen Vorteilsgebietstyp.

#### § 4 Kosten

Die Kosten für die Beschaffung und Verwendung der zur Beitragserhebung notwendigen Daten sind unselbständiger Bestandteil der Gewässerunterhaltungskosten.

#### § 5 Inkrafttreten

Diese Verordnung tritt am 1.
Januar 2021 in Kraft.

Potsdam, den 7.
Mai 2020

Der Minister für Landwirtschaft,  
Umwelt und Klimaschutz

Axel Vogel

* * *

### Anlagen

1

[Anlage (zu § 2) - Zuordnung der Nutzungsartengruppen zu Vorteilsgebietstypen und Beitragsbemessungsfaktoren](/br2/sixcms/media.php/68/GVBl_II_36_2020-Anlage.pdf "Anlage (zu § 2) - Zuordnung der Nutzungsartengruppen zu Vorteilsgebietstypen und Beitragsbemessungsfaktoren") 157.1 KB