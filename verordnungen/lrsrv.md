## Verordnung über die Förderung von Schülerinnen und Schülern mit besonderen Schwierigkeiten im Lesen und Rechtschreiben oder im Rechnen (Lesen-Rechtschreiben-Rechnen Verordnung - LRSRV)

Auf Grund des § 57 Absatz 4 in Verbindung mit § 13 Absatz 3, § 19 Absatz 5 Nummer 3, § 56, § 58 Absatz 3 und § 60 Absatz 4 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
August 2002 (GVBl.
I S. 78), von denen § 13 Absatz 3 durch Artikel 1 Nummer 10 und § 19 Absatz 5 Nummer 3 durch Artikel 1 Nummer 15 des Gesetzes vom 8. Januar 2007 (GVBl.
I S. 2) und § 56 zuletzt durch Artikel 2 Nummer 18 des Gesetzes vom 14. März 2014 (GVBl.
I Nr. 14) geändert worden sind und § 57 Absatz 4 durch Artikel 1 Nummer 3 des Gesetzes vom 10.
Juli 2017 (GVBl.
I Nr. 16) neu gefasst worden ist, verordnet der Minister für Bildung, Jugend und Sport:

### Abschnitt 1  
Einleitende Regelungen

#### § 1 Geltungsbereich

Diese Verordnung gilt für alle Schülerinnen und Schüler in den Bildungsgängen gemäß § 15 Absatz 3 Nummer 1 bis 3 des Brandenburgischen Schulgesetzes und entsprechend für Studierende in den Bildungsgängen gemäß § 15 Absatz 3 Nummer 5 und 6 des Brandenburgischen Schulgesetzes.

#### § 2 Grundsätze

(1) Aufgabe der Lehrkräfte ist es, jede Schülerin und jeden Schüler beim Erlernen des Lesens, Rechtschreibens oder Rechnens auf der Grundlage der Ergebnisse der jeweiligen individuellen Lernausgangslage zu unterstützen und zu fördern.

(2) Schülerinnen und Schüler mit besonderen Schwierigkeiten beim Erlernen des Lesens, Rechtschreibens oder Rechnens werden zusätzlich gefördert, unabhängig davon, ob diese Schwierigkeiten auf individuellen Lernvoraussetzungen oder auf sozialen und erzieherischen Einflüssen innerhalb und außerhalb der Schule beruhen.

(3) Die Entscheidung über die Einleitung der zusätzlichen Förderung, über Art, Umfang und Dauer dieser Unterstützung trifft die Klassenkonferenz oder die Jahrgangsstufenkonferenz im Rahmen der vorhandenen personellen und sächlichen Voraussetzungen.

(4) Die Einbeziehung einer Schülerin oder eines Schülers in eine zusätzliche Förderung bedarf der Einverständniserklärung der Eltern oder der volljährigen Schülerin oder des volljährigen Schülers (Anlage 1 und 2).
Die betroffenen Eltern sind über die zusätzliche Förderung regelmäßig zu informieren.
Sie sind angehalten, den Verlauf der zusätzlichen Förderung zu begleiten und zu unterstützen.

(5) Grundsätzlich gelten für Schülerinnen, Schüler und Studierende mit besonderen Schwierigkeiten im Lesen und Rechtschreiben oder mit besonderen Schwierigkeiten im Rechnen die für alle Schülerinnen und Schüler geltenden Maßstäbe der Leistungsbewertung.

(6) Für Schülerinnen und Schüler bis zur Jahrgangsstufe 4, die eine zusätzliche Förderung im Bereich Lesen und Rechtschreiben oder Rechnen erhalten, können gemäß § 10 Absatz 4 der Grundschulverordnung schriftliche Informationen zur Lernentwicklung im Bereich Lesen, Rechtschreiben oder Rechnen an die Stelle von Noten treten.

(7) Besondere Schwierigkeiten im Lesen und Rechtschreiben oder besondere Schwierigkeiten im Rechnen allein sind kein Grund, eine Schülerin oder einen Schüler mit ansonsten angemessenen Gesamtleistungen beim Übergang von der Grundschule in einen weiterführenden Bildungsgang als nicht geeignet für den Bildungsgang zum Erwerb der allgemeinen Hochschulreife zu beurteilen.

### Abschnitt 2  
Förderung von Schülerinnen und Schülern mit besonderen Schwierigkeiten im Lesen und Rechtschreiben

#### § 3 Verfahren zur Feststellung

(1) Für die Feststellung besonderer Schwierigkeiten im Lesen und Rechtschreiben ist die Lehrkraft für Deutsch verantwortlich.
Die Feststellung kann in allen Jahrgangsstufen, sollte jedoch so früh wie möglich erfolgen.
Die in der Klasse unterrichtenden Lehrkräfte, insbesondere die Lehrkräfte für Fremdsprachen, sowie die Eltern sind hierbei einzubeziehen.
Hierzu sind informelle und formelle Verfahren, die der Objektivierung und der Leistungsmessung der Lesekompetenz und der Rechtschreibung dienen, anzuwenden.
Zur Unterstützung der Lehrkraft für Deutsch kann die Schulleitung weitere Fachkräfte sowie die schulpsychologische Beratung heranziehen.

(2) Ab Jahrgangsstufe 5 ist in das Verfahren zur Feststellung besonderer Schwierigkeiten im Lesen und Rechtschreiben und zur Festlegung von Fördermaßnahmen die schulpsychologische Beratung einzubeziehen.
Die schulpsychologische Beratung ist vor allem mit der Diagnostizierung der kognitiven Voraussetzungen für schulisches Lernen befasst.

(3) Die Lehrkräfte für das Fach Deutsch und für die Fremdsprachen informieren sich zu Beginn der Sekundarstufe I und II über den Lernentwicklungsstand und die bisher durchgeführte zusätzliche Förderung für die Schülerinnen und Schüler mit besonderen Schwierigkeiten im Lesen und Rechtschreiben.
Die Klassenkonferenz oder Jahrgangsstufenkonferenz entscheidet über die Fortsetzung der zusätzlichen Förderung im Lesen und Rechtschreiben.
Bei der Entscheidungsfindung können mit Einverständnis der Eltern oder der volljährigen Schülerin, des volljährigen Schülers oder Studierenden hierfür geeignete Unterlagen der bisher besuchten Schule mit einbezogen werden.

#### § 4 Fördermaßnahmen

(1) Für Schülerinnen und Schüler mit besonderen Schwierigkeiten im Lesen und Rechtschreiben gelten in der Grundschule die Regelungen des § 6 der Grundschulverordnung.
Die zusätzliche Förderung kann auch parallel zum Regelunterricht der Klasse durchgeführt werden.
Dabei ist zu vermeiden, dass ein Fach durch die parallele Förderung besonders stark betroffen ist.

(2) Eine zusätzliche Förderung im Lesen und Rechtschreiben ist in den Schulen der Sekundarstufe I und II fortzusetzen, wenn die besonderen Schwierigkeiten im Lesen und Rechtschreiben während der Grundschulzeit nicht behoben werden können.
Zusätzlich zum Regelunterricht kann als Förderunterricht gemäß der VV-Unterrichtsorganisation eine zusätzliche Förderung für besondere Schwierigkeiten im Lesen und Rechtschreiben erteilt werden.

#### § 5 Leistungsfeststellung und Leistungsbewertung

(1) In den Jahrgangsstufen 1 bis 10, der Sekundarstufe II und in den Bildungsgängen des zweiten Bildungsweges kann Schülerinnen, Schülern oder Studierenden mit besonderen Schwierigkeiten im Lesen und Rechtschreiben ein Nachteilsausgleich gewährt werden.
Daneben können auf Antrag Abweichungen von den allgemeinen Maßstäben der Leistungsbewertung in einzelnen Fächern zugelassen werden (Anlage 1).

(2) Der Nachteilsausgleich soll die vorhandenen Beeinträchtigungen ausgleichen und der Schülerin oder dem Schüler mit besonderen Schwierigkeiten im Lesen und Rechtschreiben ermöglichen, vorhandene Fähigkeiten, Fertigkeiten und Kenntnisse in den zu erbringenden schriftlichen Leistungen nachzuweisen.
Der Nachteilsausgleich kann

1.  die Ausweitung der Arbeitszeit bei zu erbringenden schriftlichen Leistungen,
2.  die Bereitstellung von technischen und didaktischen Hilfsmitteln und
3.  die Nutzung methodisch-didaktischer Hilfen (zum Beispiel Lesepfeil, größere Schrift, optisch klar strukturierte Tafelbilder und Arbeitsblätter)

umfassen.

(3) Die Abweichungen von den allgemeinen Maßstäben der Leistungsbewertung können

1.  die stärkere Gewichtung mündlicher Leistungen, insbesondere in den Fremdsprachen, und
2.  den Verzicht auf eine Bewertung der Lese- und Rechtschreibleistung, nicht nur im Fach Deutsch,

umfassen.

In der Sekundarstufe II kann eine Abweichung von den allgemeinen Maßstäben der Leistungsbewertung nur zugelassen werden, wenn die besonderen Schwierigkeiten im Lesen und Rechtschreiben durch einen Facharzt für Kinder- und Jugendpsychiatrie im Zusammenwirken mit einer Schulpsychologin oder einem Schulpsychologen attestiert wurden.

(4) Die Entscheidungen gemäß den Absätzen 2 und 3 treffen

1.  in den Jahrgangsstufen 1 bis 10 die Klassenkonferenz,
2.  in der Sekundarstufe II und in den Bildungsgängen des zweiten Bildungsweges die jeweilige Jahrgangskonferenz und
3.  in Prüfungen, insbesondere der Abiturprüfung, der Prüfungsausschuss.

### Abschnitt 3  
Förderung von Schülerinnen und Schülern mit besonderen Schwierigkeiten im Rechnen

#### § 6 Verfahren zur Feststellung und Förderung

(1) Für die Feststellung besonderer Schwierigkeiten im Rechnen ist die Lehrkraft für Mathematik verantwortlich.
Die Feststellung kann in allen Jahrgangsstufen, sollte jedoch so früh wie möglich erfolgen.
Zur Feststellung sind vor allem Verfahren der unterrichtsbegleitenden prozessorientierten Diagnostik anzuwenden.
Hierbei sind die individuellen Lernvoraussetzungen und die Lern- und Lösungsprozesse zu erfassen sowie mathematische Basiskompetenzen zu überprüfen.
Zur Unterstützung der Lehrkraft für Mathematik kann die Schulleitung weitere Fachkräfte sowie die schulpsychologische Beratung heranziehen.

(2) Die zusätzliche Förderung der Schülerinnen und Schüler mit besonderen Schwierigkeiten im Rechnen erfolgt entsprechend den Regelungen des § 4 Absatz 1 und 2.
Dabei hat die Erarbeitung eines grundlegenden Verständnisses von Zahlen und Rechenoperationen Vorrang gegenüber aktuellen Unterrichtsinhalten.
Das Schreiben von Probearbeiten, mit denen exemplarische Übungen zu bestimmten Aufgabenstrukturen des Unterrichtsstoffes ohne Bewertung erfolgen, und das Anfertigen von zusätzlichen schriftlichen Übungen sollen diese Förderung ergänzen.

(3) Die Klassenkonferenz entscheidet über die Fortsetzung der zusätzlichen Förderung im Fach Mathematik.
Bei der Entscheidungsfindung können mit Einverständnis der Eltern oder der volljährigen Schülerin, des volljährigen Schülers oder Studierenden hierfür geeignete Unterlagen der bisher besuchten Schule mit einbezogen werden.

#### § 7 Leistungsfeststellung und Leistungsbewertung

(1) In den Jahrgangsstufen 1 bis 10 kann Schülerinnen und Schülern mit besonderen Schwierigkeiten im Rechnen ein Nachteilsausgleich gewährt werden.
Der Nachteilsausgleich soll die vorhandenen Schwierigkeiten im Rechnen ausgleichen und es diesen Schülerinnen und Schülern ermöglichen, vorhandene Fähigkeiten, Fertigkeiten und Kenntnisse in den zu erbringenden schriftlichen Leistungen nachzuweisen.

(2) Der Nachteilsausgleich kann

1.  die Verlängerung der Arbeitszeit bei zu erbringenden schriftlichen Leistungen,
2.  das Zulassen von Platz für Nebenrechnungen und
3.  den Einsatz besonderer didaktisch-methodischer Hilfsmittel

umfassen.

(3) Die Entscheidungen über die Gewährung eines Nachteilsausgleichs trifft die Klassenkonferenz.

### Abschnitt 4  
Schlussbestimmungen

#### § 8 Regelungen zu außerschulischer Unterstützung, zu Zeugnissen, Abschlüssen und Berechtigungen

(1) Reichen die zusätzlichen schulischen Förderangebote nicht aus und erfolgt eine außerschulische Unterstützung, arbeitet die Schule mit den außerschulischen Maßnahmeträgern zusammen.
Zur Festlegung der geeigneten Hilfen durch die Leistungsträger im Sinne des Sozialgesetzbuches stellt die Schule den Eltern bei Bedarf die hierfür erforderlichen schulischen Unterlagen zur Verfügung.

(2) Soweit Abweichungen von den allgemeinen Maßstäben der Leistungsbewertung gemäß § 5 Absatz 3 vorgenommen werden, ist dies auf allen Zeugnissen zu vermerken.
Die Gewährung eines Nachteilsausgleichs gemäß § 5 Absatz 2 sowie § 7 Absatz 2 ist in den Zeugnissen nicht zu vermerken.

#### § 9 Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
August 2017 in Kraft.

Potsdam, den 17.
August 2017

Der Minister für Bildung,  
Jugend und Sport

Günter Baaske

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 4, § 5 Absatz 1) - Besondere Schwierigkeiten im Lesen und Rechtschreiben](/br2/sixcms/media.php/68/GVBl_II_45_2017-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 4, § 5 Absatz 1) - Besondere Schwierigkeiten im Lesen und Rechtschreiben") 604.6 KB

2

[Anlage 2 (zu § 2 Absatz 4) - Besondere Schwierigkeiten im Rechnen](/br2/sixcms/media.php/68/GVBl_II_45_2017-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 4) - Besondere Schwierigkeiten im Rechnen") 578.9 KB