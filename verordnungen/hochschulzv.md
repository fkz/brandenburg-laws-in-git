## Verordnung zur Übertragung von Aufgaben und Befugnissen einer obersten Dienstbehörde sowie Rechten und Pflichten eines Arbeitgebers und Ausbilders auf die Hochschulen (Hochschulpersonalzuständigkeitsverordnung - HochschulZV)

Auf Grund

*   des § 4 Absatz 1 Satz 2 des Landesbeamtengesetzes vom 3.
    April 2009 (GVBl.
    I S. 26) in Verbindung mit § 1 Absatz 3 Satz 1 der Ernennungsverordnung vom 1.
    August 2004 (GVBl. II S. 742),
*   des § 37 Absatz 1 Satz 2 des Brandenburgischen Hochschulgesetzes vom 28.
    April 2014 (GVBl.
    I Nr. 18),
*   des § 54 Absatz 3 Satz 2 des Beamtenstatusgesetzes vom 17.
    Juni 2008 (BGBl. I S. 1010) in Verbindung mit § 103 Absatz 2 des Landesbeamtengesetzes,
*   des § 66 Absatz 4 zweiter Halbsatz des Landesbeamtengesetzes,
*   des § 63 Absatz 3 Satz 3 des Landesbeamtengesetzes, der durch Artikel 4 Nummer 2 des Gesetzes vom 20.
    November 2013 (GVBl.
    I Nr. 32 S. 123) geändert worden ist, im Einvernehmen mit dem Minister der Finanzen,
*   des § 89 Absatz 1 des Brandenburgischen Beamtenversorgungsgesetzes vom 20. November 2013 (GVBl.
    I Nr. 32), der durch Artikel 3 des Gesetzes vom 10. Juli 2017 (GVBl. I Nr. 14) geändert worden ist, im Einvernehmen mit dem Minister der Finanzen

verordnet die Ministerin für Wissenschaft, Forschung und Kultur:

#### § 1 Anwendungsbereich

Diese Verordnung gilt für die an den Hochschulen des Landes tätigen Angehörigen des öffentlichen Dienstes (Hochschulbediensteten) im Sinne des § 37 Absatz 1 Satz 1 des Brandenburgischen Hochschulgesetzes.
Diese Verordnung findet keine Anwendung auf die Europa-Universität Viadrina Frankfurt (Oder).

#### § 2 Befugnisübertragung

(1) Den Hochschulen wird für ihren jeweiligen Zuständigkeitsbereich die Ausübung der Befugnis zur Ernennung der Beamtinnen und Beamten der Laufbahnen des mittleren und gehobenen Dienstes übertragen.
Die ernennungsrechtlichen Befugnisse werden im Namen des Landes Brandenburg ausgeübt.

(2) Die Ausübung der landesrechtlichen Befugnisse der obersten Dienstbehörde sowie der Rechte und Pflichten eines Arbeitgebers und Ausbilders werden auf die Hochschulen übertragen, soweit Absatz 3 nicht etwas anderes bestimmt.

(3) Von der Übertragung der Befugnisse ausgenommen sind:

1.  die Gewährung von über- oder außertariflichen Leistungen,
2.  Entscheidungen, die nach dem Brandenburgischen Hochschulgesetz von dem für die Hochschulen zuständigen Mitglied der Landesregierung getroffen werden,
3.  die Zuständigkeiten der obersten Dienstbehörde nach dem Landespersonalvertretungsgesetz, die Befugnisse und Zuständigkeiten der obersten Dienstbehörde nach dem Landesdisziplinargesetz und die Antragstellung an den Landespersonalausschuss nach § 52 der Laufbahnverordnung,
4.  die Ausübung der Befugnis zur Ernennung und die Entscheidung über das Hinausschieben des Eintritts in den Ruhestand über die Regelaltersgrenze gemäß § 45 Absatz 3 des Landesbeamtengesetzes für die Beamtinnen und Beamten des höheren Dienstes,
5.  Entscheidungen über Personalmaßnahmen, die nach ihrer Geschäftsordnung von der Landesregierung getroffen werden, und
6.  die Ausübung der Befugnisse nach der Bezügezuständigkeitsverordnung vom 21. Dezember 1993 (GVBl.
    1994 II S. 3), die durch Artikel 2 der Verordnung vom 10. Mai 2004 (GVBl. II S. 329) geändert worden ist, und der Beamtenversorgungs-Zuständigkeitsverordnung vom 28. Januar 1997 (GVBl.
    II S. 53), die durch Artikel 3 der Verordnung vom 10. Mai 2004 (GVBl. II S. 329) geändert worden ist, in den jeweils geltenden Fassungen.

#### § 3 Übertragung von Aufgaben auf die Zentrale Bezügestelle des Landes Brandenburg

(1) Die Zuständigkeit der Hochschulen, mit Ausnahme der Fachhochschule Potsdam, für die Berechnung und Zahlung von Umzugskosten gemäß § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes wird auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

(2) Die Zuständigkeit der Hochschulen für den Ersatz von Sachschäden und die Geltendmachung von übergegangenen Schadensersatzansprüchen gegen Dritte gemäß § 66 Absatz 1 und 3 des Landesbeamtengesetzes sowie die Unfallfürsorgeangelegenheiten gemäß Abschnitt 2 Unterabschnitt 3 des Brandenburgischen Beamtenversorgungsgesetzes wird auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

#### § 4 Befugnis zum Erlass von Widerspruchsbescheiden

(1) Die Zuständigkeit für den Erlass von Widerspruchsbescheiden in beamtenrechtlichen Angelegenheiten der Beamtinnen und Beamten, Ruhestandsbeamtinnen und Ruhestandsbeamten und früheren Beamtinnen und Beamten der Hochschulen wird, soweit die Hochschulen die mit dem Widerspruch angegriffene Maßnahme getroffen oder unterlassen haben, auf die Hochschulen übertragen.

(2) Die Zuständigkeit für den Erlass von Widerspruchsbescheiden in beamtenrechtlichen Angelegenheiten der Beamtinnen und Beamten, Ruhestandsbeamtinnen und Ruhestandsbeamten und früheren Beamtinnen und Beamten der Hochschulen wird auf die Zentrale Bezügestelle des Landes Brandenburg übertragen, soweit diese die mit dem Widerspruch angegriffene Maßnahme getroffen oder unterlassen hat.

#### § 5 Vertretung bei Klagen aus dem Beamtenverhältnis

Die Vertretung des Landes vor den Gerichten der Verwaltungsgerichtsbarkeit gemäß § 103 des Landesbeamtengesetzes wird auf die Zentrale Bezügestelle übertragen, soweit diese über den Widerspruch zu entscheiden hat.
Dies gilt entsprechend für Verfahren des einstweiligen Rechtsschutzes.

#### § 6 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Hochschulpersonalzuständigkeitsverordnung vom 25.
Oktober 2002 (GVBl.
II S. 643), die durch Artikel 2 Absatz 6 des Gesetzes vom 18.
Dezember 2008 (GVBl.
I S. 318, 354) geändert worden ist, außer Kraft.

Potsdam, den 6.
März 2018

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Dr.
Martina Münch