## Verordnung zur Übertragung von Ermächtigungen nach dem Tierzuchtgesetz

Auf Grund des § 25 Absatz 2 des Tierzuchtgesetzes vom 18.
Januar 2019 (BGBl.
I S. 18), des § 6 Absatz 4 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S.
186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28 S.
2) geändert worden ist, und des § 36 Absatz 2 Satz 2 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19. Februar 1987 (BGBl.
I S.
602) verordnet die Landesregierung:

#### § 1 

Die Ermächtigungen der Landesregierung zum Erlass von Rechtsverordnungen nach § 9 Absatz 2 und § 19 Absatz 2 des Tierzuchtgesetzes, nach § 6 Absatz 2 des Landesorganisationsgesetzes in Verbindung mit dem Tierzuchtgesetz und nach § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach dem Tierzuchtgesetz werden auf das für Landwirtschaft zuständige Mitglied der Landesregierung übertragen.

#### § 2 

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung zur Übertragung von Ermächtigungen nach dem Tierzuchtgesetz vom 7. Februar 2013 (GVBl. II Nr. 17) außer Kraft.

Potsdam, den 28.
August 2019

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

  
Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger