## Brandenburgische Verordnung über besondere Anforderungen an die Düngung in belasteten Gebieten (Brandenburgische Düngeverordnung - BbgDüV)

Auf Grund des § 3 Absatz 4 Satz 1 und 2 Nummer 3, Absatz 5 sowie § 15 Absatz 6 Satz 1 des Düngegesetzes vom 9.
Januar 2009 (BGBl.
I S. 54, 136), von denen § 3 Absatz 4 Satz 1 und 2 Nummer 3 und Absatz 5 durch Artikel 1 des Gesetzes vom 5.
Mai 2017 (BGBl.
I S.
1068) geändert worden ist, in Verbindung mit § 13a Absatz 1 Satz 1 Nummer 1 bis 3 und Absatz 3 Satz 3 Nummer 1 und 3 der Düngeverordnung vom 26.
Mai 2017 (BGBl.
I S.
1305), der durch Artikel 1 der Verordnung vom 28.
April 2020 (BGBl.
I S.
846) eingefügt worden ist, sowie der Verordnung zur Übertragung von Ermächtigungen auf dem Gebiet des Düngerechts vom 31.
August 2020 (GVBl.
II Nr. 76) verordnet der Minister für Landwirtschaft, Umwelt und Klimaschutz:

#### § 1 Abweichende Vorschriften nach § 13a Absatz 1 Satz 1 Nummer 1 bis 3 der Düngeverordnung

Für alle im Digitalen Feldblockkataster des Landes Brandenburg erfassten Feldblöcke, die zu mehr als 50 Prozent ihrer Fläche in Gebieten oder Teilgebieten von Grundwasserkörpern im Sinne von § 13a Absatz 1 Satz 1 Nummer 1 bis 3 der Düngeverordnung liegen und gemäß § 2 Absatz 1 bekannt gemacht werden, gelten folgende Anforderungen:

1.  abweichend von § 3 Absatz 4 Satz 1 der Düngeverordnung darf das Aufbringen von Wirtschaftsdüngern sowie von organischen und organisch-mineralischen Düngemitteln, bei denen es sich um Gärrückstände aus dem Betrieb einer Biogasanlage handelt, nur erfolgen, wenn vor dem Aufbringen ihre Gehalte an Gesamtstickstoff, verfügbarem Stickstoff oder Ammoniumstickstoff und Gesamtphosphat auf der Grundlage wissenschaftlich anerkannter Messmethoden vom Betriebsinhaber oder in dessen Auftrag festgestellt worden ist,
2.  abweichend von § 4 Absatz 4 Satz 1 Nummer 1 der Düngeverordnung ist vor dem Aufbringen wesentlicher Mengen an Stickstoff der im Boden verfügbare Stickstoff vom Betriebsinhaber auf jedem Schlag oder jeder Bewirtschaftungseinheit – außer auf Grünlandflächen, Dauergrünlandflächen und Flächen mit mehrschnittigem Feldfutterbau – für den Zeitpunkt der Düngung, mindestens aber einmal jährlich, durch Untersuchung repräsentativer Proben zu ermitteln.

#### § 2 Bekanntmachung der betroffenen Gebiete und Unterrichtung der Betriebe

(1) Das für Landwirtschaft zuständige Mitglied der Landesregierung legt fest, welche Gebiete von § 13a Absatz 1 Satz 1 Nummer 1 bis 3 der Düngeverordnung erfasst werden.
Die Gebiete sind über den Geobroker der Landvermessung und Geobasisinformation Brandenburg [https://geobroker.geobasis-bb.de](https://geobroker.geobasis-bb.de/) in digitaler Form beziehbar.
Nachfolgende Änderungen der betroffenen Gebiete werden ausschließlich im Digitalen Feldblockkataster des Landes Brandenburg bekannt gemacht.

(2) Das für Landwirtschaft zuständige Ministerium unterrichtet die Betriebe jährlich darüber, für welche Feldblöcke die in § 1 festgesetzten Anforderungen gelten.

(3) Änderungen des Zuschnitts von Feldblöcken wirken sich im Hinblick auf die in § 1 genannten Anforderungen erst mit Ablauf des auf die jeweilige Änderung folgenden 1.
Januar aus.

#### § 3 Ordnungswidrigkeiten

Ordnungswidrig im Sinne des § 14 Absatz 2 Nummer 1 Buchstabe a des Düngegesetzes handelt, wer vorsätzlich oder fahrlässig entgegen den in § 1 Nummer 1 und 2 genannten Anforderungen einen dort genannten Stoff aufbringt.

#### § 4 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 1.
Januar 2021 in Kraft.
Gleichzeitig tritt die Brandenburgische Düngeverordnung vom 28.
August 2019 (GVBl.
II Nr.
67) außer Kraft.

Potsdam, den 21.
Dezember 2020

Der Minister für Landwirtschaft,  
Umwelt und Klimaschutz

Axel Vogel