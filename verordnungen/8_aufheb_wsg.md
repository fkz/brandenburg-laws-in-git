## Achte Verordnung über die Aufhebung von Wasserschutzgebieten

Auf Grund des § 15 Absatz 4 Satz 4 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20), der durch Artikel 1 des Gesetzes vom 4. Dezember 2017 (GVBl.
I Nr.
28) neu gefasst worden ist, verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Aufhebung von Wasserschutzgebieten

(1) Folgende, auf der Grundlage des § 28 des Landeskulturgesetzes vom 14.
Mai 1970 (GBl. I Nr. 12 S. 67), des § 28 des Wassergesetzes vom 17.
April 1963 (GBl.
I Nr.
5 S.
77) und der Verordnung über die Festlegung von Schutzgebieten für die Wasserentnahme aus dem Grund- und Oberflächenwasser zur Trinkwassergewinnung vom 11.
Juli 1974 (GBl.
I Nr.
37 S.
349) festgesetzte und nach § 46 des Wassergesetzes vom 2.
Juli 1982 (GBl.
I Nr.
26 S.
467) aufrechterhaltene Wasserschutzgebiete werden aufgehoben:

1.  das mit Beschluss Nummer 035 vom 20.
    November 1975 des Kreistages Wittstock für das Wasserwerk LPG „Frieden“ Freyenstein (Brunnen Massower Weg) festgesetzte Trinkwasserschutzgebiet,
2.  das mit Beschluss Nummer 68-14/81 vom 23.
    September 1981 des Kreistages Belzig für das Wasserwerk GST Belzig festgesetzte Trinkwasserschutzgebiet,
3.  das mit Beschluss Nummer 0061 vom 8.
    Juni 1980 des Kreistages Kyritz für das Wasserwerk Breddin festgesetzte Trinkwasserschutzgebiet,
4.  das mit Beschluss Nummer 77-15/81 vom 26.
    November 1981 des Kreistages Perleberg für das Wasserwerk Bälow festgesetzte Trinkwasserschutzgebiet.

(2) Folgende, auf der Grundlage des § 29 des Wassergesetzes vom 2.
Juli 1982 und der Dritten Durchführungsverordnung zum Wassergesetz – Schutzgebiete und Vorbehaltsgebiete – vom 2. Juli 1982 (GBl.
I Nr.
26 S.
487) festgesetzte Trinkwasserschutzgebiete werden aufgehoben:

1.  die mit Beschluss vom 15.
    Dezember 1982 des Kreistages Kyritz für das Wasserwerk Bantikow und für den Brunnen 2 am Wasserwerk Breddin festgesetzten Trinkwasserschutzgebiete,
2.  die mit Beschlüssen Nummer 0090 vom 20.
    Dezember 1982 und 23.
    Dezember 1982 des Kreistages Wittstock für den Brunnen 3 am Wasserwerk Freyenstein und den Brunnen 2 am Wasserwerk Wernikow festgesetzten Trinkwasserschutzgebiete,
3.  die mit Beschluss Nummer 09/36/85 vom 21.
    August 1985 des Kreistages Beeskow für die Wasserwerke Groß Eichholz, Ranzig und Trebatsch festgesetzten Trinkwasserschutzgebiete,
4.  das mit Beschluss Nummer 15-2/88 vom 5.
    Mai 1988 des Kreistages Forst (Lausitz) für das Wasserwerk Gahry festgesetzte Trinkwasserschutzgebiet.

#### § 2 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 4.
September 2019

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger