## Verordnung über die Bewilligung von Zuschüssen an die Träger von Ersatzschulen (Ersatzschulzuschussverordnung - ESZV)

_**Hinweis: Artikel 2 der Verordnung vom 27.
Juli 2021 (GVBl.
II Nr.
73) tritt am 1. August 2022 in Kraft.**_

Auf Grund des § 124a Absatz 8 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
August 2002 (GVBl.
I S. 78), der durch Artikel 1 des Gesetzes vom 19. Dezember 2011 (GVBl.
I Nr. 35) eingefügt worden ist, verordnet die Ministerin für Bildung, Jugend und Sport:

### § 1  
Geltungsbereich

Diese Verordnung gilt für die Grundlagen und das Verfahren zur Feststellung der Höhe des Betriebskostenzuschusses für die Ersatzschulen sowie über die Verwendungsnachweisprüfung.

### § 2  
Ermittlung des Betriebskostenzuschusses

(1) Der für eine Ersatzschule gewährte Betriebskostenzuschuss ergibt sich aus

1.  dem Produkt aus dem gemäß § 124a Absatz 2 des Brandenburgischen Schulgesetzes ermittelten jährlichen Schülerausgabensatz der jeweiligen Schulform und der maßgeblichen Zahl der Schülerinnen und Schüler gemäß Absatz 3 sowie
2.  den zusätzlichen Zuschüssen gemäß § 124a Absatz 7 des Brandenburgischen Schulgesetzes.

(2) Die Schülerausgabensätze je Schulform und Schuljahr bestimmen sich nach § 3.

(3) Unbeschadet der Regelungen in § 5 Absatz 2 Satz 2 und § 5 Absatz 4 sind für die endgültige Berechnung des Zuschusses gemäß Absatz 1 die tatsächlichen Schülerzahlen nach Schulformen des Zuschusszeitraumes maßgeblich.

(4) Die zusätzlichen Zuschüsse bestimmen sich nach § 4 und der Anlage.

### § 3  
Schülerausgabensatz

(1) Der Betrag der Arbeitgeberkosten je Entgeltgruppe und Schuljahr ohne Berücksichtigung der Kosten für die Unfallversicherung wird durch die Zentrale Bezügestelle des Landes Brandenburg ermittelt.
Er umfasst

1.  das Tabellenentgelt der Entgeltgruppen gemäß § 124a Absatz 3 Satz 4 und 5 des Brandenburgischen Schulgesetzes in der Entwicklungsstufe 4,
2.  die Arbeitgeberanteile zur Sozialversicherung und
3.  die Sonderzahlungen

gemäß den für Arbeitnehmerinnen und Arbeitnehmer in einem Beschäftigungsverhältnis mit dem Land Brandenburg geltenden Rechtsvorschriften in der jeweils zum Stichtag geltenden Fassung.
Die für das sonstige Personal anfallenden Personalkosten werden mit einem Zuschlag von 8 Prozent des durch die Zentrale Bezügestelle des Landes Brandenburg ermittelten Betrages berücksichtigt.
Die Kosten für die Unfallversicherung werden mit einem Zuschlag von 0,5 Prozent des gemäß den Sätzen 1 bis 3 ermittelten Betrages berücksichtigt.

(2) Die Zahl der Lehrerstellen je Schülerin oder je Schüler und Schulform wird auf der Grundlage der für den Zuschusszeitraum jeweils geltenden Bildungsgangverordnung, der gemäß § 109 Absatz 6 des Brandenburgischen Schulgesetzes durch das für Schule zuständige Ministerium für den Bereich der Schulen in öffentlicher Trägerschaft erlassenen Verwaltungsvorschriften und den Absätzen 3 bis 5 ermittelt.

(3) Die Zahl der Unterrichtsstunden je Klasse, Woche und Schulform ist das Produkt aus den nach den Sätzen 2 bis 5 ermittelten Unterrichtsstunden und den jeweiligen Zuschlagsfaktoren gemäß Absatz 4.
Die Unterrichtsstunden sind die sich aus der jeweiligen Kontingent- oder Wochenstundentafel für den gesamten Bildungsgang je Schulstufe ermittelten durchschnittlichen Wochenstundenzahlen.
Für das Schuljahr werden 40 Wochen und für das Schulhalbjahr 20 Wochen zu Grunde gelegt.
Die in den Kontingent- oder Wochenstundentafeln ausgewiesenen Stunden für Praktikum oder praktische Ausbildung gelten nicht als Unterrichtsstunden.
Die in der Berufsfachschulverordnung als Unterricht im Lernbüro ausgewiesenen Wochenstunden werden zweifach berücksichtigt.

(4) Der Zuschlag für Vertretung wird für alle Schulformen auf den Faktor 1,03 festgelegt.
Für die Differenzierung des Unterrichts erhöht sich der Faktor gemäß Satz 1 bei Grundschulen und Primarstufen an Gesamtschulen und an Oberschulen auf 1,07, bei der Sekundarstufe I an Gesamtschulen und an Oberschulen auf 1,27 und bei der Sekundarstufe I an Gymnasien auf 1,06.
Die Faktoren gemäß den Sätzen 1 und 2 erhöhen sich unter Berücksichtigung einer höheren Vertretungsreserve um jeweils 0,005.

(5) Die Zahl der Lehrerstellen je Schülerin oder je Schüler für die gymnasiale Oberstufe und für Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf ist der Quotient aus den für die Schulen in öffentlicher Trägerschaft in Lehrerwochenstunden je Schülerin oder Schüler festgelegten jeweiligen Richtwerten und der Zahl der Unterrichtsstunden je Lehrkraft, Woche und Schulform.
Die Erhöhung der Vertretungsreserve gemäß Absatz 4 Satz 3 wird entsprechend berücksichtigt.

### § 4[\*](#aa)  
Zusätzliche Zuschüsse

(1) Zusätzliche Zuschüsse gemäß § 124a Absatz 7 des Brandenburgischen Schulgesetzes werden für

1.  Ganztagsangebote an Grundschulen, Oberschulen, Gesamtschulen, Gymnasien und Förderschulen,
2.  die Organisation des Unterrichts in der flexiblen Eingangsphase,
3.  die Betreuung der praktischen Ausbildung oder des Praktikums von Bildungsgängen an beruflichen Schulen durch Lehrkräfte und
4.  den Einsatz von sonstigem pädagogischen Personal im Unterricht für Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf

gewährt.

(2) Zusätzliche Zuschüsse für Ganztagsangebote und die Organisation des Unterrichts in der flexiblen Eingangsphase werden gewährt, wenn sie durch das für Schule zuständige Ministerium genehmigt sind.
Zusätzliche Zuschüsse für die Betreuung der praktischen Ausbildung oder des Praktikums werden gewährt, soweit die Bildungsgangverordnungen die Betreuung verbindlich regeln.
Zusätzliche Zuschüsse für den Einsatz von sonstigem pädagogischen Personal werden für diejenigen Förderschwerpunkte gewährt, für die die gemäß § 109 Absatz 6 des Brandenburgischen Schulgesetzes für den Bereich der öffentlich getragenen Schulen erlassenen Verwaltungsvorschriften einen entsprechenden Einsatz vorsehen.

(3) Die zusätzlichen Zuschüsse werden entsprechend § 124a Absatz 2 des Brandenburgischen Schulgesetzes schülerbezogen ermittelt, für Ganztagsangebote und für die Organisation des Unterrichts in der flexiblen Eingangsphase unter Berücksichtigung der Korrekturfaktoren gemäß Absatz 4 (schülerbezogener Betrag).
Dabei werden die Lehrerstellen je Schülerin oder je Schüler für die Zuschüsse gemäß

1.  Absatz 1 Nummer 1 und 4 auf der Grundlage der für die Ausstattung von vergleichbaren Angebote an Schulen in öffentlicher Trägerschaft festgelegten Richtwerte,
2.  Absatz 1 Nummer 2 auf der Grundlage des für die Ausstattung des jahrgangsstufenübergreifenden Unterrichts des vergleichbaren Angebots an Schulen in öffentlicher Trägerschaft festgelegten Richtwerts und
3.  Absatz 1 Nummer 3 jeweils auf der Grundlage der Vorgaben der Bildungsgangverordnungen zur Zahl der Besuche und zum Umfang der Betreuung

in der Anlage festgelegt.
Die zusätzlichen Zuschüsse sind das Produkt aus den jeweils maßgeblichen Schülerzahlen und den entsprechenden schülerbezogenen Beträgen.

(4) Die Korrekturfaktoren für Ganztagsangebote werden je Schulstufe und Form des Ganztagsangebots als Quotient aus dem jeweiligen Anteil der Schülerinnen und Schüler an Schulen mit Ganztagsangeboten an der Schülerzahl an Schulen in öffentlicher Trägerschaft und dem entsprechenden Anteil an Schulen in freier Trägerschaft festgelegt.
Der Korrekturfaktor für Unterricht in der flexiblen Eingangsphase ist der Quotient aus dem Anteil der Schülerinnen und Schüler in der flexiblen Eingangsphase an der Schülerzahl der Jahrgangsstufen 1 und 2 an Grundschulen, Gesamtschulen und Oberschulen in öffentlicher Trägerschaft und dem entsprechenden Anteil an Schulen in freier Trägerschaft.
Sofern die nach den Sätzen 1 und 2 bestimmten Anteile an Schulen in öffentlicher Trägerschaft größer sind als die entsprechenden Anteile an Schulen in freier Trägerschaft, beträgt der Korrekturfaktor eins.
Der Berechnung werden die Schülerzahlen der Schuldatenerhebungen gemäß § 13 der Datenschutzverordnung Schulwesen (Schuldaten) des dem Zuschusszeitraum vorangehenden Schuljahres zu Grunde gelegt.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*](#a) _Am 1.
August 2022 treten Änderungen in § 4 gemäß Artikel 3 Satz 2 der Verordnung vom 27. Juli 2021 ([GVBl. II Nr. 73](https://bravors.brandenburg.de/br2/sixcms/media.php/76/GVBl_II_73_2021.pdf)) in Kraft.
_

### § 5[\*](#bb)  
Zuschussverfahren

(1) Der Betriebskostenzuschuss wird grundsätzlich für die Dauer eines Schuljahres (Zuschusszeitraum) gewährt.
Der Betriebskostenzuschuss wird grundsätzlich in gleichen Monatsbeträgen jeweils bis spätestens zum zehnten Werktag des Monats gezahlt.

(2) Dem Träger der Ersatzschule ist in der Regel bis zum 31.
Mai vor Beginn des Zuschusszeitraums ein Bescheid über den Betriebskostenzuschuss zu erteilen, der auch maschinell erstellt werden kann.
Der Berechnung des Betriebskostenzuschusses werden im Rahmen des Bescheides nach Satz 1 die Zahlen der Schülerinnen und Schüler nach den Schuldaten des dem Zuschusszeitraum vorhergehenden Schuljahres zu Grunde gelegt.
Sonderpädagogischer Förderbedarf für Schülerinnen und Schüler wird bei der Ermittlung des Betriebskostenzuschusses berücksichtigt, wenn die Nachweise gemäß Absatz 3 Nummer 2 erbracht werden.
Schülerinnen und Schüler an Förderschulen, für die kein sonderpädagogischer Förderbedarf festgestellt wurde oder für die die Nachweise gemäß Absatz 3 Nummer 2 nicht vorliegen, werden bei der Ermittlung des Betriebskostenzuschusses nicht berücksichtigt.

(3) Für den jeweiligen Zuschusszeitraum sind dem für Schule zuständigen Ministerium bis zum 1. März vor Beginn des Zuschusszeitraums die Nachweise über

1.  die Gemeinnützigkeit und
2.  die Feststellung durch das staatliche Schulamt über
    1.  einen sonderpädagogischen Förderbedarf oder
    2.  eine schwere Mehrfachbehinderungfür Schülerinnen und Schüler an allgemein bildenden und beruflichen Schulen

vorzulegen.
Für berufliche Schulen gilt Nummer 2 Buchstabe a mit der Maßgabe, dass der Nachweis über den sonderpädagogischen Förderbedarf nur im Förderschwerpunkt „geistige Entwicklung“ zu erbringen ist.

(4) Eine Neufestsetzung des Betriebskostenzuschusses während des Zuschusszeitraums erfolgt, wenn sich

1.  die Schülerzahl der aktuellen Schuldaten gegenüber den Daten, die der Festsetzung des Betriebskostenzuschusses zu Grunde lagen, um mehr als 5 Prozent verändert oder
2.  die Schülerzahl mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“ oder mit einer schweren Mehrfachbehinderung verändert und der Nachweis gemäß Absatz 3 Nummer 2 erfolgt.

(5) Für verbeamtete Lehrkräfte, die auf Antrag des Trägers der Ersatzschule unter Wegfall der Bezüge zum Dienst in einer Ersatzschule beurlaubt sind und denen eine Anwartschaft auf lebenslange Versorgung und Hinterbliebenenversorgung nach beamtenrechtlichen Vorschriften gewährt wird, werden die Betriebskostenzuschüsse für die Ersatzschule um einen Versorgungszuschlag in Höhe von 19 400 Euro pro Lehrkraft und Schuljahr gemindert.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*](#b) _Am 1.
August 2022 treten Änderungen in § 5 gemäß Artikel 3 Satz 2 der Verordnung vom 27. Juli 2021 ([GVBl.
II Nr.
73](https://bravors.brandenburg.de/br2/sixcms/media.php/76/GVBl_II_73_2021.pdf)) in Kraft.
_

### § 6  
Grundsätze der Verwendungsnachweisprüfung

(1) Gegenstand der Verwendungsnachweisprüfung ist die zweckentsprechende Verwendung des gewährten Betriebskostenzuschusses in dem jeweiligen Zuschusszeitraum.
Als Nachweis für die Verwendung können nur die im Zuschusszeitraum tatsächlich geleisteten Ausgaben anerkannt werden.

(2) Im Rahmen der Verwendungsnachweisprüfung wird bezüglich der endgültigen Schülerzahlberechnung die Differenz zwischen der tatsächlichen und der dem Betriebskostenzuschuss zu Grunde gelegten Schülerzahl ermittelt.
Die tatsächliche Schülerzahl ist der Durchschnitt der monatlichen Schülerzahlen im Schuljahr.
Bei den monatlichen Schülerzahlen werden alle Schülerinnen und Schüler der Ersatzschule berücksichtigt, die mehr als die Hälfte des Monats in einem vertraglichen Schulverhältnis stehen, es sei denn,

1.  der Schulbesuch wurde tatsächlich nie aufgenommen,
2.  der Schulbesuch wurde tatsächlich abgebrochen oder
3.  die Schule oder der Bildungsgang wurde unterjährig gewechselt.

Ein Fall gemäß Nummer 2 liegt auch vor, nachdem die Schülerin oder der Schüler dem Unterricht oder sonstigen pflichtigen Veranstaltungen ununterbrochen vier Wochen unentschuldigt ferngeblieben ist.
In Ferienzeiten oder in Zeiten, in denen in Abschlussklassen kein Unterricht mehr stattfindet, sind unentschuldigte Fehlzeiten ausgeschlossen.

(3) Als zweckentsprechende Verwendung des Betriebskostenzuschusses für Sachkosten werden grundsätzlich nur die Aufwendungen gemäß § 110 des Brandenburgischen Schulgesetzes anerkannt.
Soweit Tilgungen und Zinsen im Rahmen der Schulraumbeschaffung oder zur Finanzierung der Wartefrist anfallen, werden diese als Ausgaben anerkannt.
Abschreibungen werden nicht als Ausgaben anerkannt.

### § 7[\*](#cc)  
Verwendungsnachweis

(1) Innerhalb von drei Monaten nach Ablauf des Haushaltsjahres, in dem der Zuschusszeitraum endet, legt der Träger der Ersatzschule den Verwendungsnachweis für den Zuschusszeitraum zur Prüfung vor.

(2) Der Verwendungsnachweis umfasst

1.  die monatlichen Schülerzahlen nach Schulformen einschließlich einer gesonderten Ausweisung der Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf,
2.  Übersichten, aus denen die Daten der jeweiligen tatsächlichen Zu- und Abgänge hervorgehen,
3.  die Bescheide über die Feststellung eines sonderpädagogischen Förderbedarfs einschließlich einer schweren Mehrfachbehinderung,
4.  die monatlichen Schülerzahlen der an Ganztagsangeboten teilnehmenden Klassen,
5.  die monatlichen Schülerzahlen im Unterricht der flexiblen Eingangsphase und
6.  eine Übersicht über die Sach- und Personalkosten.

Der Träger der Ersatzschule ist verpflichtet, dem für Schule zuständigen Ministerium die Nachweise in schriftlicher Form vorzulegen.
Soweit keine Originalunterlagen oder beglaubigte Kopien eingereicht werden können, muss der Träger der Ersatzschule schriftlich erklären, dass diese Unterlagen vollinhaltlich mit dem Original übereinstimmen.

(3) Darüber hinaus sind für die Schülerinnen und Schüler, die die Ersatzschule während des Zuschusszeitraums besucht haben, Klassenlisten vorzuhalten, die zum jeweiligen Namen die Unterschrift der Schülerin oder des Schülers oder deren Eltern enthalten.
Die Klassenlisten sind durch den Träger für mindestens fünf Jahre aufzubewahren und bei einer Prüfung gemäß Absatz 5 vorzulegen.

(4) Der Verwendungsnachweis wird auf Plausibilität geprüft.
Der Träger der Ersatzschule ist verpflichtet, im Rahmen einer etwaigen vertieften Prüfung auf Anforderung weitere Unterlagen zum Nachweis der zweckentsprechenden Verwendung des Betriebskostenzuschusses vorzulegen, insbesondere die

1.  namentliche Zusammenstellung der Schülerinnen und Schüler und deren Adressen sowie nach Einführung deren Schülernummer zum Stichtag der Erhebungen der Schuldaten,
2.  namentliche Zusammenstellung der eingesetzten Lehrkräfte sowie deren Qualifikation, Beschäftigungsverhältnis, Beschäftigungsumfang, Unterrichtseinsatz und gezahltes Entgelt und des sonstigen pädagogischen Personals mit Angaben zum gezahlten Entgelt und
3.  Belege über ausgewählte Ausgabepositionen.

(5) Das für Schule zuständige Ministerium und der Landesrechnungshof Brandenburg sind berechtigt, die Angaben des Trägers der Ersatzschule im Rahmen einer vertieften Prüfung auch vor Ort zu prüfen oder durch einen Beauftragten prüfen zu lassen.
Der Träger der Ersatzschule ist verpflichtet, hierzu jederzeit Einblick in die Bücher und Belege der Schule zu geben, Ablichtungen von Unterlagen zu ermöglichen sowie die geforderten Auskünfte zu erteilen und Nachweise zu erbringen.

(6) Das für Schule zuständige Ministerium kann die Verwendung von Vordrucken für die tabellarischen Darstellungen und deren elektronische Übermittlung verbindlich vorgeben.

(7) Sollten sich im Rahmen der Verwendungsnachweisprüfung Hinweise auf die Nichteinhaltung von Genehmigungsvoraussetzungen ergeben, sind diese Gegenstand von Prüfungen außerhalb der Verwendungsnachweisprüfung.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*](#c) _Am 1.
August 2022 treten Änderungen in § 7 gemäß Artikel 3 Satz 2 der Verordnung vom 27. Juli 2021 ([GVBl.
II Nr.
73](https://bravors.brandenburg.de/br2/sixcms/media.php/76/GVBl_II_73_2021.pdf)) in Kraft.
_

### § 8  
Rückforderung und Nachzahlung

(1) Ist der auf Grund der Angaben im Verwendungsnachweis für den Zuschusszeitraum endgültig zuzubilligende Betrag geringer als der bewilligte und gezahlte Zuschuss, so ist der Differenzbetrag spätestens nach Ablauf von vier Wochen seit der Zustellung des Rückforderungsbescheides zurückzuzahlen.
Andernfalls hat der Träger einer Ersatzschule den überzahlten Betrag mit 5 Prozent über dem jeweils geltenden Basiszinssatz gemäß § 247 des Bürgerlichen Gesetzbuches zu verzinsen.

(2) Ist der auf Grund der Angaben im Verwendungsnachweis für den Zuschusszeitraum endgültig zuzubilligende Betrag höher als der bewilligte und gezahlte Zuschuss, besteht eine Zahlungspflicht der bewilligenden Behörde.

(3) Der Zuschussbescheid kann ganz oder teilweise zurückgenommen oder widerrufen werden, insbesondere wenn die Finanzhilfe nicht zweckentsprechend verwendet wird oder der Träger der Ersatzschule die Nachweise nicht fristgerecht einreicht.

### § 9  
Veröffentlichung

Die Zahl der Unterrichtsstunden je Klasse, Woche und Schulform, die Zahl der Unterrichtsstunden je Lehrkraft, Woche und Schulform, die Zahl der Schülerinnen und Schüler je Klasse und Schulform (Richtwert), die Zahl der Lehrerwochenstunden je Schüler und die Zahl der Lehrerstellen je Schülerin oder je Schüler sowie die Schülerausgabensätze gemäß § 3 und die Korrekturfaktoren und schülerbezogenen Beträge gemäß § 4 werden vor Ermittlung des Betriebskostenzuschusses im Amtsblatt des Ministeriums für Bildung, Jugend und Sport veröffentlicht.

### § 10  
Übergangs- und Schlussvorschriften

(1) Für Schülerinnen und Schüler, für die es an den Schulen in öffentlicher Trägerschaft kein vergleichbares Angebot gibt, kann das für Schule zuständige Ministerium im Einvernehmen mit dem Ministerium der Finanzen mit dem Träger einer Ersatzschule gesonderte Vereinbarungen treffen.

(2) Unabhängig von den Schuldaten sind Nachweise gemäß § 5 Absatz 3 Nummer 2 für den Zuschusszeitraum des Schuljahres 2012/2013 zu berücksichtigen, wenn diese bis zum 1.
Mai 2012 vorgelegt werden und die entsprechenden Schülerinnen und Schüler im Schuljahr 2011/2012 eine Schule in freier Trägerschaft besuchen.
Für das Schuljahr 2012/2013 erfolgt die Neufestsetzung des Betriebskostenzuschusses, wenn die Nachweisführung gemäß Satz 1 für die Schülerinnen und Schüler, die im Schuljahr 2012/2013 eine Ersatzschule besuchen, bis zum 30.
September 2012 erfolgt.
Erfolgt die Nachweisführung nach dem 30.
September 2012, kann das für Schule zuständige Ministerium den Betriebskostenzuschuss im Ausnahmefall auf Antrag neu festsetzen.

(3) Die Verwendungsnachweisprüfung erfolgt bis zum Zuschusszeitraum des Schuljahres 2011/2012 auf der Grundlage der Ersatzschulzuschussverordnung vom 7.
April 2008 (GVBl.
II S.
130).

### § 11  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 2.
Januar 2012 in Kraft.
Gleichzeitig tritt die Ersatzschulzuschussverordnung vom 7.
April 2008 (GVBl.
II S.
130) außer Kraft.

Potsdam, den 17.
April 2012

Die Ministerin für Bildung,  
Jugend und Sport

Dr.
Martina Münch

Anlage   
(zu § 2 Absatz 4)
---------------------------

### Lehrerstellen je Schülerin oder je Schüler für zusätzliche Zuschüsse

1.
Lehrerstellen je Schülerin oder je Schüler gemäß § 4 Absatz 1 Nummer 1 (Ganztagsangebote)

Schulstufe

Schulform

Form des Ganztagsangebots

L/S

Primarstufe

Grundschule, Oberschule, Gesamtschule

Verlässliche Halbtagsgrundschule (VHG)

0,0065

Offene Form

0,0010

Gymnasium

Gebundene Form

0,0066

Offene Form

0,0023

Förderschule mit Förderschwerpunkt „emotionale und soziale Entwicklung“

Verlässliche Halbtagsgrundschule (VHG)

0,0106

Sekundarstufe I

Oberschule, Gesamtschule

Gebundene Form

0,0097

Offene Form

0,0034

Gymnasium

Gebundene Form

0,0066

Offene Form

0,0029

Förderschule mit Förderschwerpunkt „Lernen“

Gebundene Form

0,0213

2.
Lehrerstellen je Schülerin oder je Schüler gemäß § 4 Absatz 1 Nummer 2 (Unterricht in der flexiblen Eingangsphase)

Schulstufe

Schulform

Ausstattung für

L/S

Primarstufe

Grundschule, Oberschule, Gesamtschule

jahrgangsübergreifenden Unterricht in der flexiblen Eingangsphase

0,0086

3.
Lehrerstellen je Schülerin oder je Schüler gemäß § 4 Absatz 1 Nummer 3 (praktische Ausbildung oder Praktikum)

Bildungsgang

L/S

Berufsfachschule Soziales

0.0100

Berufsfachschule sonstige Assistentenberufe

0,0008

Fachoberschule, zweijährig, Vollzeit

0,00085

Fachschule Sozialwesen

> Fachrichtungen Sozialpädagogik und Heilerziehungspflege, Vollzeit

0,0100

> Fachrichtungen Sozialpädagogik und Heilerziehungspflege, Teilzeit

0,0033

> Fachrichtung Heilpädagogik, Aufbaulehrgang, Vollzeit

0,0067

> Fachrichtung Heilpädagogik, Aufbaulehrgang, Teilzeit

0,0040

> Fachrichtung Sonderpädagogik, Aufbaulehrgang, Teilzeit

0,0033

4.
Lehrerstellen je Schülerin oder je Schüler gemäß § 4 Absatz 1 Nummer 4 (sonstiges pädagogisches Personal)

Sonderpädagogischer Förderbedarf

L/S

> Körperliche und motorische Entwicklung

0,0400

> Sehen

0,0200

> Hören

0,0200

> Geistige Entwicklung

0,0400

> Schwer Mehrfachbehindert

0,0400

5.
Lehrerstellen je Schülerin oder je Schüler gemäß § 4 Absatz 1 Nummer 5 (Schülerinnen und Schüler aus Flüchtlingsfamilien an allgemeinbildenden Ersatzschulen)

Schulstufe

Schulform

L/S

Primarstufe

Grundschule, Oberschule, Gesamtschule, Gymnasium

0,0370

Sekundarstufe I

Oberschule, Gesamtschule, Gymnasium

0,0400

Sekundarstufe II

Oberschule, Gesamtschule, Gymnasium

0,0400