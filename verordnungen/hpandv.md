## Verordnung zur Bewältigung der SARS-CoV-2-Pandemie im Hochschulbereich (Hochschulpandemieverordnung - HPandV)

Auf Grund des § 8a Absatz 1 Satz 1 in Verbindung mit § 16 Absatz 6 Satz 3 des Brandenburgischen Hochschulgesetzes vom 28.
April 2014 (GVBl.
I Nr. 18), von denen § 8a Absatz 1 Satz 1 durch Artikel 1 des Gesetzes vom 23.
September 2020 (GVBl.
I Nr.
26) eingefügt worden ist, verordnet die Ministerin für Wissenschaft, Forschung und Kultur im Einvernehmen mit dem für Wissenschaft zuständigen Ausschuss des Landtags und nach Anhörung der Landeskonferenz der Studierendenschaften:

#### § 1 Anwendungsbereich

Diese Verordnung regelt Abweichungen von den im Brandenburgischen Hochschulgesetz genannten Regelstudienzeiten und Fristen für die Erbringung von fachsemestergebundenen Prüfungsleistungen während der SARS-CoV-2-Pandemie im Sommersemester 2020, im Wintersemester 2020/2021, im Sommersemester 2021 und im Wintersemester 2021/2022.

#### § 2 Verlängerung der Regelstudienzeit und der Prüfungsfristen im Sommersemester 2020

(1) Für Studierende, die im Sommersemester 2020 eingeschrieben sind, gilt eine von § 18 Absatz 2 des Brandenburgischen Hochschulgesetzes abweichende, jeweils um ein Semester verlängerte individuelle Regelstudienzeit.
Dies gilt entsprechend für Studierende, die die Regelstudienzeit mit dem Sommersemester 2020 überschritten haben und denen eine Verlängerung der Förderungshöchstdauer gemäß § 15 Absatz 3 des Bundesausbildungsförderungsgesetzes gewährt wurde, im Hinblick auf die Ausbildungsförderung.
Bei beurlaubten Studierenden regelt die Präsidentin oder der Präsident, abhängig von den Beurlaubungsgründen und der Situation an der Hochschule, ob die Verlängerung nach Satz 1 Anwendung findet.

(2) Für Studierende, die im Sommersemester 2020 in einem Studiengang eingeschrieben sind, verlängern sich die in § 21 des Brandenburgischen Hochschulgesetzes genannten Fristen für die Erbringung von fachsemestergebundenen Prüfungsleistungen in diesem Studiengang jeweils um ein Semester.
Sollte es sich hierbei um Prüfungsleistungen handeln, die aus hochschulorganisatorischen Gründen nur in einem Sommersemester beziehungsweise nur in einem Wintersemester erbracht werden können, so umfasst die verlängerte Frist entsprechend das zeitlich nächste Sommersemester beziehungsweise Wintersemester.

#### § 3 Verlängerung der Regelstudienzeit und der Prüfungsfristen im Wintersemester 2020/2021

(1) Für Studierende, die im Wintersemester 2020/2021 eingeschrieben sind, gilt eine von § 18 Absatz 2 des Brandenburgischen Hochschulgesetzes abweichende, jeweils um ein Semester verlängerte individuelle Regelstudienzeit.
Dies gilt entsprechend für Studierende, die die Regelstudienzeit mit dem Wintersemester 2020/2021 überschritten haben und denen eine Verlängerung der Förderungshöchstdauer gemäß § 15 Absatz 3 des Bundesausbildungsförderungsgesetzes gewährt wurde, im Hinblick auf die Ausbildungsförderung.
Bei beurlaubten Studierenden regelt die Hochschule, abhängig von den Beurlaubungsgründen und der Situation an der Hochschule, ob die Verlängerung nach Satz 1 Anwendung findet.

(2) Für Studierende, die im Wintersemester 2020/2021 eingeschrieben sind, verlängern sich die in § 21 des Brandenburgischen Hochschulgesetzes genannten Fristen für die Erbringung von fachsemestergebundenen Prüfungsleistungen in diesem Studiengang jeweils um ein Semester.
Sollte es sich hierbei um Prüfungsleistungen handeln, die aus hochschulorganisatorischen Gründen nur in einem Sommersemester beziehungsweise nur in einem Wintersemester erbracht werden können, so umfasst die verlängerte Frist entsprechend das zeitlich nächste Sommersemester beziehungsweise Wintersemester.

#### § 4 Verlängerung der Regelstudienzeit und der Prüfungsfristen im Sommersemester 2021

(1) Für Studierende, die im Sommersemester 2021 eingeschrieben sind, gilt eine von § 18 Absatz 2 des Brandenburgischen Hochschulgesetzes abweichende, jeweils um ein Semester verlängerte individuelle Regelstudienzeit.
Dies gilt entsprechend für Studierende, die die Regelstudienzeit mit dem Sommersemester 2021 überschritten haben und denen eine Verlängerung der Förderungshöchstdauer gemäß § 15 Absatz 3 des Bundesausbildungsförderungsgesetzes gewährt wurde, im Hinblick auf die Ausbildungsförderung.
Bei beurlaubten Studierenden regelt die Hochschule, abhängig von den Beurlaubungsgründen und der Situation an der Hochschule, ob die Verlängerung nach Satz 1 Anwendung findet.

(2) Für Studierende, die im Sommersemester 2021 eingeschrieben sind, verlängern sich die in § 21 des Brandenburgischen Hochschulgesetzes genannten Fristen für die Erbringung von fachsemestergebundenen Prüfungsleistungen in diesem Studiengang jeweils um ein Semester.
Sollte es sich hierbei um Prüfungsleistungen handeln, die aus hochschulorganisatorischen Gründen nur in einem Sommersemester beziehungsweise nur in einem Wintersemester erbracht werden können, so umfasst die verlängerte Frist entsprechend das zeitlich nächste Sommersemester beziehungsweise Wintersemester.

#### § 5 Verlängerung der Regelstudienzeit und der Prüfungsfristen im Wintersemester 2021/2022

(1) Für Studierende, die im Wintersemester 2021/2022 eingeschrieben sind, gilt eine von § 18 Absatz 2 des Brandenburgischen Hochschulgesetzes abweichende, jeweils um ein Semester verlängerte individuelle Regelstudienzeit.
Dies gilt entsprechend für Studierende, die die Regelstudienzeit mit dem Wintersemester 2021/2022 überschritten haben und denen eine Verlängerung der Förderungshöchstdauer gemäß § 15 Absatz 3 des Bundesausbildungsförderungsgesetzes gewährt wurde, im Hinblick auf die Ausbildungsförderung.
Bei beurlaubten Studierenden regelt die Hochschule, abhängig von den Beurlaubungsgründen und der Situation an der Hochschule, ob die Verlängerung nach Satz 1 Anwendung findet.

(2) Für Studierende, die im Wintersemester 2021/2022 eingeschrieben sind, verlängern sich die in § 21 des Brandenburgischen Hochschulgesetzes genannten Fristen für die Erbringung von fachsemestergebundenen Prüfungsleistungen in diesem Studiengang jeweils um ein Semester.
Sollte es sich hierbei um Prüfungsleistungen handeln, die aus hochschulorganisatorischen Gründen nur in einem Sommersemester beziehungsweise nur in einem Wintersemester erbracht werden können, so umfasst die verlängerte Frist entsprechend das zeitlich nächste Sommersemester beziehungsweise Wintersemester.

#### § 6 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 13.
Oktober 2020

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Dr.
Manja Schüle