## Verordnung zum Ausgleich der Mehrbelastungen der Träger von Kindertagesstätten und der örtlichen Träger der öffentlichen Jugendhilfe infolge der Einführung eines Sockels für die Wahrnehmung pädagogischer Leitungsaufgaben in Kindertagesstätten (Kita-Leitungsausgleichsverordnung - KitaLAV)

Auf Grund des § 23 Absatz 1 Nummer 9 des Kindertagesstättengesetzes in der Fassung der Bekanntmachung vom 27.
Juni 2004 (GVBl.
I S. 384), der durch Artikel 1 des Gesetzes vom 10. Juli 2017 (GVBl.
I Nr. 17) eingefügt worden ist, verordnet die Ministerin für Bildung, Jugend und Sport im Einvernehmen mit dem Ausschuss für Bildung, Jugend und Sport des Landtags Brandenburg, mit dem Minister der Finanzen und dem Minister des Innern und für Kommunales:

#### § 1 Anwendungsbereich

Diese Verordnung regelt den Ausgleich der Mehrbelastungen, die bei den Trägern von Kindertagesstätten und den örtlichen Trägern der öffentlichen Jugendhilfe infolge der Zumessung eines Sockels für die Wahrnehmung pädagogischer Leitungsaufgaben aufgrund von § 5 Absatz 2 Satz 1 der Kita-Personalverordnung entstehen, sowie das Ausgleichsverfahren.

#### § 2 Kostenausgleich

(1) Die örtlichen Träger der öffentlichen Jugendhilfe gleichen die Kosten aus, die in ihrem Gebiet den Trägern von Kindertagesstätten für die Bereitstellung eines Sockels für die Wahrnehmung pädagogischer Leitungsaufgaben aufgrund von § 5 Absatz 2 Satz 1 der Kita-Personalverordnung entstehen.

(2) Das Land gewährt den örtlichen Trägern der öffentlichen Jugendhilfe die erforderlichen Mittel zum Ausgleich der nach Absatz 1 entstehenden Kosten.
Der Ausgleichsbetrag jedes örtlichen Trägers der öffentlichen Jugendhilfe wird auf der Grundlage der Anzahl der Kindertagesstätten in seinem Zuständigkeitsbereich, des Stellenanteils des Sockels für pädagogische Leitungsaufgaben gemäß § 5 Absatz 2 Satz 1 der Kita-Personalverordnung und der erforderlichen Personalkosten für eine Leitungskraft ermittelt.
Dazu wird die aus der Anzahl der Kindertagesstätten und dem Stellenumfang des Sockels für pädagogische Leitungsaufgaben ermittelte Anzahl an Leitungsstellen mit den erforderlichen Personalkosten der Leitungsstellen multipliziert.

(3) Für die Ermittlung des Ausgleichsbetrags ist die Anzahl der am 1.
Januar des jeweiligen Jahres erfassten Kindertagesstätten mit einer Betriebserlaubnis maßgeblich.

#### § 3 Erstattungsfähige Personalkosten

(1) Die den Trägern der Kindertagesstätten durch die Träger der öffentlichen Jugendhilfe zu gewährenden Ausgleichsbeträge richten sich nach den anteiligen unmittelbar entgeltbezogenen Aufwendungen des Arbeitgebers für eine Leitungskraft der fünften Entwicklungsstufe des zutreffenden Tätigkeitsmerkmals der Entgeltordnung für den Sozial- und Erziehungsdienst des Tarifvertrags für den öffentlichen Dienst (Kommunen) einschließlich aller vom Arbeitgeber zu tragenden Entgeltbestandteile und Nebenkosten.
Maßgeblich für die jährliche Ermittlung des zutreffenden Tätigkeitsmerkmals nach Satz 1 ist das Jahresmittel der belegten Plätze der jeweiligen Kindertagesstätte im Vorjahr, ausgehend von den Stichtagen nach § 3 Absatz 1 und 4 oder Absatz 7 der Kindertagesstätten-Betriebskosten- und Nachweisverordnung.
Die der Anzahl der belegten Plätze entsprechenden Ausgleichsbeträge werden im Amtsblatt des Ministeriums für Bildung, Jugend und Sport veröffentlicht.

(2) Für die Bemessung der den örtlichen Trägern der öffentlichen Jugendhilfe vom Land zu gewährenden Ausgleichsbeträge sind die unmittelbar entgeltbezogenen Aufwendungen des Arbeitgebers für eine Leitungskraft der fünften Entwicklungsstufe des Tätigkeitsmerkmals S15 der Entgeltordnung für den Sozial- und Erziehungsdienst des Tarifvertrags für den öffentlichen Dienst (Kommunen) einschließlich aller vom Arbeitgeber zu tragenden Entgeltbestandteile und Nebenkosten maßgeblich.
Dabei werden die am 1.
November des Vorjahres geltenden Tarifstände sowie die zu diesem Zeitpunkt für dieses Kalenderjahr feststehenden Tarifänderungen berücksichtigt.
Für das Jahr 2017 gelten die am 1.
Oktober 2017 feststehenden Daten.

(3) Die oberste Landesjugendbehörde erstattet den örtlichen Trägern der öffentlichen Jugendhilfe auf Antrag nachgewiesene höhere Personalkosten.
Der Antrag ist innerhalb von drei Monaten nach Bekanntgabe des Leistungsbescheids der obersten Landesjugendbehörde gemäß § 5 zu stellen.
Mit dem Antrag sind die in dem Zuständigkeitsbereich des jeweiligen örtlichen Trägers der öffentlichen Jugendhilfe für die Leitungskräfte der Kindertagesstätten geltenden Vergütungsregelungen, die Kindertagesstätten, in denen die Vergütungsregelungen jeweils zum Tragen kommen, sowie die Anzahl der im Jahresmittel des Vorjahres in den einzelnen Kindertagesstätten belegten Plätze nachzuweisen.
Verspätet eingehende Anträge und Nachweise können berücksichtigt werden, wenn dem örtlichen Träger der öffentlichen Jugendhilfe Wiedereinsetzung in den vorigen Stand zu gewähren ist.
Für den Nachweis höherer Personalkosten kann die oberste Landesjugendbehörde verbindliche Vorgaben machen; sie kann ein elektronisches Antrags- und Nachweisverfahren vorgeben.

#### § 4 Auszahlung der Ausgleichsbeträge an die örtlichen Träger der öffentlichen Jugendhilfe

Die Auszahlung der Ausgleichsbeträge an die örtlichen Träger der öffentlichen Jugendhilfe erfolgt zu den in § 5 Absatz 1 Satz 2 der Kindertagesstätten-Betriebskosten- und Nachweisverordnung genannten Terminen.

#### § 5 Verteilung der Ausgleichsbeträge durch die örtlichen Träger der öffentlichen Jugendhilfe

(1) Die örtlichen Träger der öffentlichen Jugendhilfe reichen die den Trägern der Kindertagesstätten zustehenden Ausgleichsbeträge entsprechend den jeweils geltenden Vergütungsregelungen zweckgebunden zu den Zahlungsterminen gemäß § 3 Absatz 5 der Kindertagesstätten-Betriebskosten- und Nachweis​ver​ordnung aus.

(2) Zum Ausgleich des Verwaltungsaufwands für die Weiterleitung des Ausgleichsbetrags an die Träger von Kindertagesstätten erhalten die örtlichen Träger der öffentlichen Jugendhilfe einen Kostenausgleich.
Die Höhe des Ausgleichs ergibt sich aus dem Aufwand zur Auszahlung der Beträge.
Für den Aufwand werden jährlich eine Arbeitsstunde einer Kraft im gehobenen nichttechnischen Verwaltungsdienst der fünften Entwicklungsstufe der Entgeltgruppe E9b des Tarifvertrags für den öffentlichen Dienst (Kommunen) einschließlich aller vom Arbeitgeber zu tragenden Entgeltbestandteile und Nebenkosten für je fünf zu bezuschussende Kindertagesstätten und ein zusätzlicher Gemeinkostenanteil von 20 Prozent angesetzt.
Die Mittel werden mit den Zahlungen nach § 4 ausgereicht.

(3) Übernimmt eine Gemeinde oder ein Amt für den Landkreis die Ausreichung der Ausgleichsbeträge gemäß Absatz 1 an die Träger der Kindertagesstätten, so ist § 12 Absatz 1 des Kindertagesstättengesetzes entsprechend anzuwenden.
§ 3 Absatz 3 bleibt unberührt.

#### § 6 Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Oktober 2017 in Kraft.

Potsdam, den 30.
Oktober 2017

Die Ministerin für Bildung,  
Jugend und Sport

In Vertretung  
Dr.
Thomas Drescher