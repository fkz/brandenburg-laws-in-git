## Verordnung über die Heilfürsorge der Polizeivollzugsbeamtinnen und Polizeivollzugsbeamten des Landes Brandenburg (Brandenburgische Polizei-Heilfürsorgeverordnung - BbgPolHV)

Auf Grund des § 114 Absatz 2 Satz 2 des Landesbeamtengesetzes vom 3. April 2009 (GVBl.
I S. 26) verordnet der Minister des Innern:

Inhaltsübersicht
----------------

[§ 1    Heilfürsorgeberechtigte](#1)  
[§ 2    Umfang der Heilfürsorge](#2)  
[§ 3    Vorbeugende Gesundheitsfürsorge](#3)  
[§ 4    Ambulante ärztliche Behandlung](#4)  
[§ 5    Zahnärztliche Behandlung](#5)  
[§ 6    Krankenhausbehandlung und Anschlussheilbehandlung](#6)  
[§ 7    Häusliche Krankenpflege und Haushaltshilfe](#7)  
[§ 8    Schwangerschaft und Entbindung](#8)  
[§ 9    Künstliche Befruchtung](#9)  
[§ 10  Psychotherapie](#10)  
[§ 11  Stationäre und ambulante Hospizleistungen](#11)  
[§ 12  Vorsorgekuren, Heilkuren und Mutter-/Vater-Kind-Kuren](#12)  
[§ 13  Arznei-, Verband-, Heil- und Hilfsmittel und Rehabilitationssport](#13)  
[§ 14  Notwendige Heilbehandlung außerhalb des Landes](#14)  
[§ 15  Fahrkosten](#15)  
[§ 16  Inkrafttreten, Außerkrafttreten](#16)

#### § 1   
Heilfürsorgeberechtigte

Heilfürsorgeberechtigte sind Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte, die Anspruch auf Heilfürsorge nach Maßgabe des § 114 des Landesbeamtengesetzes in der jeweils geltenden Fassung haben, solange ihnen Besoldung, Elternzeit oder Urlaub nach § 77 Absatz 2 des Landesbeamtengesetzes zusteht.

#### § 2  
Umfang der Heilfürsorge

(1) Der Anspruch auf Heilfürsorge umfasst:

1.  vorbeugende Gesundheitsfürsorge (§ 3),
    
2.  ambulante ärztliche Behandlung (§ 4),
    
3.  zahnärztliche Behandlung (§ 5),
    
4.  Krankenhausbehandlung und Anschlussheilbehandlung (§ 6),
    
5.  häusliche Krankenpflege und Haushaltshilfe (§ 7),
    
6.  Leistungen bei Schwangerschaft und Entbindung (§ 8),
    
7.  Leistungen zur künstlichen Befruchtung (§ 9),
    
8.  Psychotherapie (§ 10),
    
9.  stationäre und ambulante Hospizleistungen (§ 11),
    
10.  Vorsorgekuren, Heilkuren und Mutter-/Vater-Kind-Kuren (§ 12),
    
11.  Arznei-, Verband-, Heil- und Hilfsmittel und Rehabilitationssport (§ 13),
    
12.  notwendige Heilbehandlung außerhalb des Landes (§ 14) sowie
    
13.  Fahrkosten (§ 15).
    

(2) Die Gewährung der Leistungen nach Absatz 1 erfolgt unter Beachtung der Richtlinien des Gemeinsamen Bundesausschusses (§ 91 des Fünften Buches Sozialgesetzbuch – Gesetzliche Krankenversicherung – (Artikel 1 des Gesetzes vom 20.
Dezember 1988, BGBl.
I S. 2477, 2482), der zuletzt durch Artikel 3 des Gesetzes vom 7.
August 2013 (BGBl.
I S. 3108) geändert worden ist), in der jeweils geltenden Fassung sowie der Vereinbarungen zwischen dem Land Brandenburg und den kassenärztlichen oder kassenzahnärztlichen Vereinigungen und sonstiger Vereinbarungen.
Diese sind in der Abrechnungsstelle für die Heilfürsorge, in den Personalstellen, beim Polizeiärztlichen Dienst und im Intranet der Polizei zur Einsichtnahme bereitzuhalten.

#### § 3  
Vorbeugende Gesundheitsfürsorge

(1) Heilfürsorgeberechtigte können sich von einer Polizeiärztin oder einem Polizeiarzt im Rahmen der vorbeugenden Gesundheitsfürsorge untersuchen, beraten und behandeln lassen, um Krankheiten vorzubeugen und die psychische und physische Leistungsfähigkeit zu erhalten und zu fördern.
Erforderliche Schutzimpfungen werden gewährt.

(2) Heilfürsorgeberechtigte können die sozialmedizinische Betreuung durch den Polizeiärztlichen Dienst in Anspruch nehmen.

(3) Gesundheitsuntersuchungen zur Früherkennung von Krankheiten können im Rahmen von § 25 des Fünften Buches Sozialgesetzbuch gewährt werden.

#### § 4  
Ambulante ärztliche Behandlung

(1) Heilfürsorgeberechtigte haben freie Arztwahl.
Soweit sie für eine Behandlung nicht eine Polizeiärztin oder einen Polizeiarzt am Dienst- oder Wohnort wählen, können sie sich von allen Kassenärztinnen oder Kassenärzten behandeln lassen, die der Kassenärztlichen Vereinigung des Landes Brandenburg angehören und in der Nähe des Dienst- oder Wohnortes praktizieren.
Andere Ärztinnen oder Ärzte dürfen nur gewählt werden, wenn diese bereit sind, die Behandlung zu den gleichen Bedingungen oder zu den im Überweisungsschein genannten Bedingungen zu übernehmen und ihre Leistungen über die Abrechnungsstelle der Heilfürsorge abzurechnen.

(2) Unterbringungskosten auf Grund einer notwendigen auswärtigen Behandlung werden bei ambulanten ärztlichen Leistungen bis zu einem Höchstsatz von 26 Euro pro Tag erstattet.
Die Vorschrift findet bei kurähnlichen Leistungen keine Anwendung.

(3) Heilfürsorgeberechtigte oder sonstige Polizeivollzugsbeamtinnen oder Polizeivollzugsbeamte, die sich in geschlossenen Einsätzen und Übungen befinden, werden vom Polizeiärztlichen Dienst betreut.

(4) Die ärztliche Behandlung umfasst die Tätigkeit, die zur Behandlung von Krankheiten nach den Regeln der ärztlichen Kunst ausreichend und zweckmäßig ist.

(5) Heilfürsorgeberechtigte erhalten eine Krankenversichertenkarte, die vor der ärztlichen Behandlung auszuhändigen ist.

(6) Die Krankenversichertenkarte ist zum Zwecke der elektronischen Datenverarbeitung durch die behandelnde Ärztin oder den behandelnden Arzt mit einem Datenträger (Datenchip) gemäß § 33d des Brandenburgischen Datenschutzgesetzes versehen.
Auf dem Datenchip werden Name, Vorname, Geburtsdatum und Anschrift der Heilfürsorgeberechtigten, der Zeitraum der Gültigkeit der Krankenversichertenkarte, die von der Zentralen Bezügestelle vergebene Personalnummer als persönliche Identifikationsnummer sowie die Bezeichnung des Kostenträgers mit einer Kassennummer für den Nachweis der Heilfürsorgeberechtigung gespeichert.

(7) Eine nach Art der Erkrankung notwendige weitere Behandlung wird auf Veranlassung der erstbehandelnden Ärztin oder des erstbehandelnden Arztes durch Ausstellung eines Überweisungsscheines gewährt.

(8) In dringenden Fällen kann eine ärztliche Behandlung auch ohne Krankenversichertenkarte oder Überweisungsschein in Anspruch genommen werden.
Die Heilfürsorgeberechtigten haben die Ärztin oder den Arzt darauf hinzuweisen, dass sie Anspruch auf Heilfürsorge nach dieser Verordnung haben.
Die Krankenversichertenkarte oder der Überweisungsschein ist unverzüglich nachzureichen.

#### § 5  
Zahnärztliche Behandlung

(1) § 4 Absatz 1, 2, 5, 6 und 8 gilt entsprechend.

(2) Die zahnärztliche Behandlung umfasst die Tätigkeit, die zur Verhütung, Früherkennung und Behandlung von Zahn-, Mund- und Kieferkrankheiten nach den Regeln der zahnärztlichen Kunst ausreichend und zweckmäßig ist.

(3) Vor Anfertigung von Zahnersatz und Zahnkronen, vor Beginn einer Parodontosebehandlung und einer kiefer­orthopädischen Behandlung ist der Abrechnungsstelle für Heilfürsorge beim Zentraldienst der Polizei ein Behandlungsplan mit Kostenvoranschlag zur Genehmigung vorzulegen.
Wird ein genehmigter Behandlungsplan geändert, so bedarf die Änderung erneut der Genehmigung.

(4) Kosten für zahnärztliche Leistungen, die über den in Absatz 2 beschriebenen und den nach Absatz 3 genehmigten Umfang hinausgehen, werden nicht übernommen.

#### § 6  
Krankenhausbehandlung und Anschlussheilbehandlung

(1) Heilfürsorgeberechtigte haben Anspruch auf allgemeine Krankenhausleistungen nach der Bundespflegesatzverordnung vom 26.
September 1994 (BGBl.
I S. 2750), die zuletzt durch Artikel 16b des Gesetzes vom 21.
Juli 2014 (BGBl.
I S. 1133) geändert worden ist, in der jeweils geltenden Fassung, wenn Art oder Schwere der Krankheit eine stationäre Behandlung erfordern oder aus diagnostischen Gründen eine stationäre Beobachtung unumgänglich ist.
Bei stationärer Krankenhausbehandlung stellt die Polizeiärztin oder der Polizeiarzt eine Kostenübernahmeerklärung aus.
Heilfürsorgeberechtigte händigen diese zusammen mit dem Überweisungsschein dem Krankenhaus aus.
In dringenden Fällen haben sie darauf hinzuweisen, dass ein Anspruch auf Heilfürsorge nach dieser Verordnung besteht.
Die Kostenübernahmeerklärung ist unverzüglich nachzureichen.
In allen anderen Fällen ist vor Beginn der stationären Behandlung die Zustimmung der Polizeiärztin oder des Polizeiarztes einzuholen.

(2) Bei stationärer Behandlung sind die öffentlichen und freien gemeinnützigen Krankenhäuser in Anspruch zu nehmen.
Ausnahmen sind zulässig, wenn ärztliche Gründe oder Unterbringungsschwierigkeiten die Einweisung in ein anderes Krankenhaus rechtfertigen.

(3) Heilfürsorgeberechtigte haben Anspruch auf medizinische Leistungen zur Rehabilitation, deren unmittelbarer Anschluss an eine Krankenhausbehandlung nach ärztlicher Feststellung medizinisch zwingend notwendig ist (Anschlussheilbehandlung).

#### § 7  
Häusliche Krankenpflege und Haushaltshilfe

(1) Auf ärztliche Verordnung wird häusliche Krankenpflege gewährt, wenn dadurch stationäre Krankenhausbehandlung vermieden und diese Pflege nur durch eine Berufspflegekraft erbracht werden kann.
Die dafür notwendigen Aufwendungen werden bis zur Höhe der Kosten dieser Berufspflegekraft erstattet.

(2) Haushaltshilfe kann gewährt werden, wenn Heilfürsorgeberechtigten wegen einer stationären Krankenhausbehandlung oder einer sonstigen Leistung dieser Verordnung die Weiterführung des Haushalts nicht möglich ist.
Voraussetzung ist, dass im Haushalt ein unterhaltsberechtigtes Kind lebt, das bei Beginn der Haushaltshilfe das zwölfte Lebensjahr noch nicht vollendet hat oder das behindert und auf Hilfe angewiesen ist.
Die Kostenerstattung für Haushaltshilfe ist auf sechs Euro pro Stunde und höchstens 36 Euro täglich begrenzt.
Die Entscheidung über die Gewährung einer Haushaltshilfe trifft die Polizeiärztin oder der Polizeiarzt.

#### § 8  
Schwangerschaft und Entbindung

(1) Die Heilfürsorge umfasst die ärztliche Versorgung während der Schwangerschaft.

(2) Aus Anlass der Entbindung in einer Entbindungs- oder Krankenanstalt werden aus Mitteln der Heilfürsorge Behandlungs- und Pflegekosten gewährt.
Für das Neugeborene werden lediglich die Unterbringungs- und Betreuungskosten aus Heilfürsorgemitteln, längstens jedoch für die Dauer von sechs Tagen nach der Entbindung, übernommen.
Bei Hausentbindungen werden die Kosten für die Hebamme oder den Entbindungspfleger und die Ärztin oder den Arzt übernommen.

(3) Im Übrigen werden die Kosten für Leistungen einer Hebamme oder eines Entbindungspflegers in demselben Umfang übernommen, wie die gesetzliche Krankenversicherung diese Leistungen gewährt.

#### § 9  
Künstliche Befruchtung

Heilfürsorgeberechtigte haben nach Zustimmung durch die Polizeiärztin oder den Polizeiarzt Anspruch auf medizinische Maßnahmen zur Herbeiführung einer Schwangerschaft entsprechend § 27a des Fünften Buches Sozialgesetzbuch.
Die Behandlungskosten werden in Höhe von 50 Prozent übernommen.

#### § 10  
Psychotherapie

(1) Kosten für eine ausreichende, zweckmäßige und wirtschaftliche tiefenpsychologisch fundierte und analytische Psychotherapie werden nur übernommen, wenn eine derartige Behandlung der Heilung oder Besserung einschließlich der medizinischen Rehabilitation einer Krankheit dient und diese Krankheit ärztlich festgestellt wurde.

(2) Vor Beginn der Behandlung ist entsprechend den Richtlinien des Gemeinsamen Bundesausschusses über tiefenpsychologisch fundierte und analytische Psychotherapie ein Antrag auf Kostenübernahme bei der Polizeiärztin oder dem Polizeiarzt zu stellen, in dem die Indikation der gewählten Behandlungsmethode zu begründen ist.

#### § 11  
Stationäre und ambulante Hospizleistungen

Stationäre und ambulante Hospizleistungen können nach Zustimmung durch die Polizeiärztin oder den Polizeiarzt entsprechend § 39a des Fünften Buches Sozialgesetzbuch gewährt werden.
Die dadurch anfallenden Pflege- und Betreuungskosten werden wie bei den Ersatzkassen übernommen.

#### § 12   
Vorsorgekuren, Heilkuren und Mutter-/Vater-Kind-Kuren

(1) Kuren können auf ärztlichen Vorschlag und nach Zustimmung der Polizeiärztin oder des Polizeiarztes durchgeführt werden, wenn dadurch die Gesundheit erhalten (Vorsorgekuren) oder wiederhergestellt werden kann und die Dienstfähigkeit zu erwarten ist (Heilkuren).
In der Regel sind Kurkliniken zu wählen, mit denen Belegungsabsprachen bestehen.

(2) _**(aufgehoben)**_

(3) Vorsorgekuren werden Heilfürsorgeberechtigten nach § 114 Absatz 3 Satz 1 des Landesbeamtengesetzes gewährt bei Vorliegen von Funktionsstörungen, die noch nicht zur Manifestation von organischen Erkrankungen geführt haben.
Diese werden für längstens drei Wochen erbracht.
Die Wiederholung der Vorsorgekuren kann frühestens nach Ablauf von vier Jahren genehmigt werden.
§ 23 Absatz 2 bis 5 des Fünften Buches Sozialgesetzbuch gilt entsprechend.

(4) Heilkuren werden als Wiederholungskur wegen desselben Leidens nur gewährt, wenn durch sie eine endgültige oder langdauernde Wiederherstellung der Dienstfähigkeit zu erwarten ist.

(5) Mutter-/Vater-Kind-Kuren sind aus medizinischen Gründen notwendige Vorsorge- beziehungsweise Rehabilitationsleistungen, die in einer Einrichtung des Müttergenesungswerks oder einer gleichartigen Einrichtung zu erbringen sind.
Sie werden nur gewährt, wenn das dritte Lebensjahr des Kindes noch nicht vollendet ist.
Bei alleinerziehenden Heilfürsorgeberechtigten kann die Kur auch bis zur Vollendung des zwölften Lebensjahres des Kindes gewährt werden.
Die während des Kuraufenthaltes anfallenden Unterbringungs-, Verpflegungs- und Betreuungskosten für das begleitende Kind werden bis zu einem Betrag von 36 Euro pro Tag übernommen.
Die Polizeiärztin oder der Polizeiarzt hat der Maßnahme vorab zuzustimmen.

(6) Kuren werden grundsätzlich nicht bewilligt, wenn Heilfürsorgeberechtigte ihre Entlassung beantragt haben, gegen sie ein Verfahren auf Rücknahme der Ernennung, ein Disziplinarverfahren mit dem Ziel der Entfernung aus dem Beamtenverhältnis bei gleichzeitiger vorläufiger Dienstenthebung oder ein Verfahren mit der Folge des Verlustes der Beamtenrechte schwebt.
Vorsorgekuren dürfen zudem nicht gewährt werden, wenn Heilfürsorgeberechtigte in den nächsten 24 Monaten aus dem Dienst ausscheiden oder sich ohne Dienstbezüge beurlauben lassen.
Über Ausnahmen entscheidet die Leitende Polizeiärztin oder der Leitende Polizeiarzt im Ministerium des Innern und für Kommunales.

(7) Verhalten sich Heilfürsorgeberechtigte nach Feststellung der Kureinrichtung nicht kurgemäß, kann die Bewilligung der Kur bis zu ihrem Abschluss von der oder dem Dienstvorgesetzten widerrufen werden.

#### § 13  
Arznei-, Verband-, Heil- und Hilfsmittel und Rehabilitationssport

(1) Heilfürsorgeberechtigte haben Anspruch auf ärztlich verordnete Arznei-, Verband-, Heil- und Hilfsmittel.
Hiervon ausgenommen sind wissenschaftlich nicht anerkannte Mittel, Mineralwässer, Stärkungsmittel, kosmetische Artikel und Präparate, die als eine besondere Zubereitungsform von Nahrungsmitteln anzusehen sind.
Ein Anspruch auf Sehhilfen liegt nur bei Ausnahmeindikationen entsprechend den Richtlinien des Gemeinsamen Bundesausschusses vor.

(2) Aufwendungen für die Beschaffung, die Instandsetzung und den Ersatz ärztlich verordneter Hilfsmittel, die Heilfürsorgeberechtigte aus dienstlichen Gründen oder wegen der Erfordernisse des täglichen Lebens benötigen, bedürfen der vorherigen Anerkennung durch die Polizeiärztin oder den Polizeiarzt, wenn sie im Einzelfall den Betrag von 200 Euro übersteigen.
Gegenstände, die allgemein zum täglichen Bedarf gehören, sind keine Hilfsmittel im Sinne dieser Verordnung.

(3) Für physikalische Behandlungsmaßnahmen, Massagen und Heilgymnastik ist die vorherige Anerkennung durch die Polizeiärztin oder den Polizeiarzt einzuholen, wenn mehr als zehn Behandlungen je Therapieform verordnet werden.
In dringenden Fällen haben die Heilfürsorgeberechtigten darauf hinzuweisen, dass sie Anspruch auf Heilfürsorge nach dieser Verordnung haben.
Die Kostenübernahmeerklärung ist unverzüglich nachzureichen.
Soweit es nach Lage des Krankheitsfalles zumutbar ist, sind die am Dienst- oder Wohnort oder in deren Nähe gelegenen Einrichtungen zu nutzen.

(4) Die Polizeiärztin oder der Polizeiarzt kann die Teilnahme an bis zu 50 Übungseinheiten Rehabilitationssport einmalig genehmigen.
Die Genehmigung ist vorab einzuholen.

#### § 14  
Notwendige Heilbehandlung außerhalb des Landes

(1) Wird eine Heilbehandlung in einem anderen Bundesland notwendig, werden die Kosten in Höhe der für Heilfürsorgeberechtigte des betreffenden Bundeslandes geltenden Sätze übernommen.
Handelt es sich nicht um eine unaufschiebbare Heilbehandlung, gilt Satz 1 nur nach Zustimmung durch die Polizeiärztin oder den Polizeiarzt.

(2) Wird während eines Auslandsaufenthaltes eine unaufschiebbare Heilbehandlung notwendig, werden die Aufwendungen bis zu der Höhe übernommen, wie sie im Inland nach dieser Verordnung entstanden wären.
Bei einem Aufenthalt im Ausland aus persönlichen Gründen werden die Rücktransportkosten zum Heimatort nicht übernommen.

#### § 15  
Fahrkosten

(1) Die durch die Heilbehandlung entstandenen notwendigen Fahrkosten werden übernommen.
Bei Beförderung mit Taxi, Mietwagen, privatem Fahrzeug oder öffentlichen Verkehrsmitteln werden nur die Fahrkosten erstattet, die 20 Euro je einfache Fahrt übersteigen.
Den Nachweis über die entstandenen Kosten haben die Antragstellerinnen oder Antragsteller zu erbringen.
An- und Abreisekosten auf Grund eines Kuraufenthaltes sind von der Fahrkostenerstattung ausgenommen.

(2) Notwendig sind grundsätzlich nur die Fahrten auf dem direkten Weg zwischen dem jeweiligen Aufenthaltsort und der nächsten erreichbaren Behandlungsmöglichkeit.

(3) Erstattungsgrundlage bei ärztlich verordneten Fahrten mit privatem PKW sind die Regelungen zur Weg­streckenentschädigung nach dem Bundesreisekostengesetz.

(4) Bei Fahrten mit öffentlichen Verkehrsmitteln werden nur die Kosten der niedrigsten Beförderungsklasse unter Ausschöpfung möglicher Fahrpreisermäßigungen erstattet.
Kosten für Fahrpreisauskünfte sind ausgenommen.

(5) Die Polizeiärztin oder der Polizeiarzt kann bei Überschreitung der Belastungsgrenze eine darüber hinausgehende Kostenerstattung genehmigen.
Die Belastungsgrenze beträgt zwei Prozent des jährlichen Einkommens beziehungsweise für chronisch Kranke, die wegen derselben Krankheit in Dauerbehandlung sind, ein Prozent des jährlichen Einkommens.
Die Heilfürsorgeberechtigten haben dem Antrag einen Nachweis über die entstandenen Kosten beizufügen.

#### § 16  
Inkrafttreten, Außerkrafttreten

(1) Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig tritt die Verordnung über die Heilfürsorge der Polizeivollzugsbeamten vom 4. Juni 1993 (GVBl.
II S. 250), die zuletzt durch Verordnung vom 9. Februar 2004 (GVBl.
II S. 141) geändert worden ist, außer Kraft.

Potsdam, den 28.
Juni 2010

Der Minister des Innern  
Rainer Speer