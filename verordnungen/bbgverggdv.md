## Verordnung über Angebotsprüfungen, Kontrollen, Auftragssperren und erleichterte Nachweise nach dem Brandenburgischen Vergabegesetz (Brandenburgische Vergabegesetz-Durchführungsverordnung - BbgVergGDV)

Auf Grund des § 10 Absatz 1 des Brandenburgischen Vergabegesetzes vom 21. September 2011 (GVBl. I Nr. 19) in Verbindung mit § 2 der Brandenburgischen Vergabegesetz-Zuständigkeitsübertragungsverordnung vom 29. März 2012 (GVBl. II Nr. 22) verordnet der Minister für Wirtschaft und Europaangelegenheiten:

### § 1  
Kontrolle durch Arbeitsentgeltnachweise

(1) Zur Kontrolle der Einhaltung der Vereinbarung über die Höhe von Arbeitsentgelten nach § 6 Absatz 2 sowie nach § 4 Absatz 1 des Brandenburgischen Vergabegesetzes fordert der Auftraggeber zu der mit dem Auftragnehmer vereinbarten Vorlage von Belegen über die Abrechnung und Zahlung von Arbeitsentgelten an seine bei der Leistungserbringung eingesetzten Beschäftigten und etwaige Beschäftigte von Nachunternehmern oder Personalverleihern als Unterlage auf.
Die Aufforderung erfolgt zur Vorbereitung einer Abrechnung über vertragliche Leistungen je Vertrag oder, bei längerer Vertragslaufzeit, einer Abrechnung je Kalenderjahr.
Der Auftraggeber kann die Anforderung auf Belege zu Teilleistungen oder Leistungsabschnitten und auf Belege zu einzelnen Beschäftigten oder Gruppen von Beschäftigten beschränken.
Die Belege können anonymisierte oder pseudonymisierte Kopien sein, die die Zugehörigkeit zu jeweils einer Person, aber nicht die Identität dieser Person und deren sonstige persönliche Merkmale erkennen lassen sollen.

(2) Auftraggeber und Auftragnehmer können bei jeder Leistungsart statt der Einreichung von Belegen zu einer Abrechnung über die Leistung auch die Vorlage von Belegen mit Erläuterungen oder im Beisein eines erläuterungsfähigen Vertreters des Auftragnehmers, die Vorlage von Sammelbelegen vereinbaren, wenn der Auftraggeber davon ausgehen kann, dass die von Absatz 1 abweichende Vorgehensweise eine gleichwertige Kontrolle ermöglicht.
Eine Einigung auf eine Vorgehensweise anstelle der vertraglich vorgesehenen erfolgt nach Abschluss des Vertrages im Zuge der Umsetzung des Vertrages, ohne dass es einer besonderen Form bedarf, wenn die Ausführung und deren Dokumentation erfolgt.

(3) Bei Bauleistungen, für die es Sozialkassenbescheinigungen für das Baugewerbe gibt, genügt zur Erfüllung der Anforderungen nach Absatz 1 eine Bescheinigung, die bei Eingang der ersten Rechnung nicht älter als sechs Monate ist.
Liegt eine solche Bescheinigung aus dem Vergabeverfahren vor, wird nur bei längerer Vertragslaufzeit eine weitere Bescheinigung angefordert.
Sonstige vergleichbare Bescheinigungen von freiwilligen Sozialkassen auch anderer Branchen können ebenso verwendet werden, wenn der Auftragnehmer glaubhaft darlegt, dass die zugrunde liegenden Arbeitsentgelte ausnahmslos das nach § 6 Absatz 2 einzuhaltende Entgelt erreichen oder übersteigen.

### § 2  
Kontrollziel, Berechnungsvorgaben

(1) Die Prüfung, ob die vereinbarten Arbeitsentgelte bezahlt werden, erfolgt durch Division des ermittelten Arbeitnehmerbruttoentgelts durch die Zahl der Stunden, für die es bezahlt wird.
Arbeitsvertragliche oder tarifliche Zeitausgleichsmaßnahmen werden berücksichtigt.

(2) Ein Monatsgehalt zuzüglich der auf den Monat umgerechneten unbedingt vereinbarten, nicht monatlich ausgezahlten Bruttoentgeltteile gleich welcher Bezeichnung wird mit drei vervielfältigt und durch dreizehn geteilt.
Das dadurch ermittelte durchschnittliche Gehalt je Woche wird durch die Anzahl der Stunden der vereinbarten regelmäßigen wöchentlichen Arbeitszeit geteilt.

(3) Nicht berücksichtigt werden:

1.  das Arbeitsentgelt nach § 43 des Strafvollzugsgesetzes vom 16.
    März 1976 (BGBl. I S. 581, 2088; 1977 I S.
    436), das zuletzt durch Artikel 10 Absatz 7 des Gesetzes vom 30. Oktober 2017 (BGBl.
    I S.
    3618) geändert worden ist;
2.  das Arbeitsentgelt behinderter Menschen in anerkannten Werkstätten nach § 138 des Neunten Buches Sozialgesetzbuch – Rehabilitation und Teilhabe behinderter Menschen – (Artikel 1 des Gesetzes vom 19. Juni 2001, BGBl. I S. 1046), das zuletzt durch Artikel 165 des Gesetzes vom 29. März 2017 (BGBl.
    I S.
    626) geändert worden ist;
3.  die Ausbildungsvergütung nach § 17 des Berufsbildungsgesetzes vom 23. März 2005 (BGBl. I S. 931), das zuletzt durch Artikel 14 des Gesetzes vom 17.
    Juli 2017 (BGBl. I S. 2581) geändert worden ist;
4.  das Taschengeld nach § 2 Nummer 4 des Bundesfreiwilligendienstgesetzes vom 28. April 2011 (BGBl. I S. 687), das zuletzt durch Artikel 15 Absatz 5 des Gesetzes vom 20. Oktober 2015 (BGBl. I S. 1722) geändert worden ist;
5.  das Taschengeld nach § 2 Nummer 3 des Jugendfreiwilligendienstegesetzes vom 16. Mai 2008 (BGBl. I S. 842), das durch Artikel 30 des Gesetzes vom 20. Dezember 2011 (BGBl. I S. 2854, 2923) geändert worden ist,

und die diesen Entgelten, Vergütungen und Anerkennungen zugrunde liegenden Arbeitszeiten.

### § 3  
Durchführung der Stichprobe

(1) Eine Stichprobe nimmt der Auftraggeber auf Grund konkreter Anhaltspunkte vor, dass der Auftragnehmer seinen Verpflichtungen aus einer Vereinbarung auf der Grundlage des Brandenburgischen Vergabegesetzes nicht nachkommt.
Ohne besonderen Anlass kann der Auftraggeber eine Stichprobe bei einzelnen Vertragsverhältnissen als Zufallsstichprobe vornehmen.
Die Auswahl der Stichprobe ohne besonderen Anlass kann anhand von Kriterien erfolgen, die jeweils vor dem Beginn eines längeren Zeitraums für Aufträge festgelegt werden, die in diesem Zeitraum vereinbart werden.

(2) Die Stichprobe kann sich auf Vertragsteile beschränken, insbesondere einzelne Gewerke und Bauabschnitte einer Baumaßnahme oder einzelne Dienstleistungen im Niedriglohnsektor.
Der Aufwand einer Stichprobe soll in einem angemessenen Verhältnis zum Auftragswert stehen.

(3) Ziel der Stichprobe ist die Einsichtnahme in Unterlagen über Beginn, Ende und Dauer des Personaleinsatzes für die Erfüllung des Auftrags und die Berechnung und Zahlung des dazugehörigen Arbeitsentgelts.
Die Stichprobe kann in der Anforderung von Unterlagen oder in der Einsichtnahme in Unterlagen des Auftragnehmers oder eines Nachunternehmers oder Verleihers erfolgen.
Die Stichprobe durch Einsichtnahme kann am Ort der Leistung, in den Geschäftsräumen oder in der Niederlassung des Auftragnehmers, eines Nachauftragnehmers oder Personalverleihers vorgenommen werden.

(4) Die Kontrolle besteht nur dann in einer Befragung der Beschäftigten, wenn

1.  andere Kontrollen nicht geduldet, ermöglicht oder in einer zum Verständnis der Belege erforderlichen Maße durch den Auftragnehmer unterstützt wurden oder
2.  bei einem Angebot dieser Art der Kontrolle durch den Auftragnehmer keine Bedenken hinsichtlich der Möglichkeit zur freien Äußerung der Beschäftigten bestehen
3.  und die beschäftigte Person einverstanden ist.

Die Befragung nach Satz 1 bezieht sich auf die Person, den Umfang des Arbeitseinsatzes und das dabei erzielte Bruttoarbeitsentgelt, insbesondere auf das Entgelt je Arbeitsstunde und das Entgelt je Überstunde.

(5) Die Kontrolle wird abgebrochen, wenn

1.  der Kontrollperson entgegen der vertraglichen Vereinbarung der Zugang verweigert wird,
2.  Kontrollen nicht geduldet, ermöglicht oder im erforderlichen Maße unterstützt werden oder
3.  Zugang, Duldung und scheinbare Unterstützung unter unzumutbaren Umständen gewährt werden; die Zumutbarkeit bestimmt sich nach dem subjektiven Eindruck der Kontrollperson.

Erfüllt der Auftragnehmer seine gemäß § 9 Absatz 1 Satz 3 des Brandenburgischen Vergabegesetzes mit dem Auftraggeber vereinbarten Kooperationspflichten nicht, sind dem Auftragnehmer die gemäß § 10 des Brandenburgischen Vergabegesetzes vereinbarten Folgen seines Verhaltens schriftlich unter Festsetzung einer angemessenen Frist zur Pflichterfüllung anzudrohen.

(6) Wurde die Kontrolle nach Absatz 5 abgebrochen, wird der Auftragnehmer in Textform zur Vorlage und Erläuterung von Unterlagen binnen einer Frist von höchstens sieben Arbeitstagen aufgefordert.
Dabei wird der Ausschluss von weiteren Vergaben des Auftraggebers und die Entscheidung über die weiteren gemäß § 10 des Brandenburgischen Vergabegesetzes vereinbarten Folgen angedroht für den Fall, dass die Frist ungenutzt abläuft oder die Unterlagen und Erläuterungen die durch den Abbruch der Kontrolle erklärten Zweifel an der Vertragstreue des Auftragnehmers nicht ausräumen.
Eine Fristsetzung ist entbehrlich, wenn es sich um zielgerichtete Verhaltensweisen des Auftragnehmers handelt, die auch nach einem objektiven Maßstab die Vorortkontrolle unzumutbar erscheinen lassen.

(7) Über die Kontrolle fertigt der Auftraggeber ein Protokoll.
Besteht nach der freien Überzeugung auf Grund des Ergebnisses der Kontrolle kein Verdacht einer Verletzung der Vertragsbestimmungen über Arbeitsentgelte, wird die Kontrolle abgeschlossen.
Besteht ein Verdacht der Vertragsverletzung und sind nicht alle Erkenntnismittel ausgeschöpft, wird die Kontrolle mit der Gelegenheit zur Stellungnahme und zur Ausräumung von Zweifeln für den Auftragnehmer fortgesetzt.
Sind weitere Aufklärungen nicht erreichbar, wird ein verbliebener Verdachtsrest niedergeschlagen.

### § 4  
Sonstige Verfahrensweisen bei Stichproben

Auftraggeber können die Stichprobe mit einem anderen Ablauf planen und ausführen, wenn deren Eignung nach ihrer pflichtgemäßen Einschätzung der in § 3 geregelten Vorgehensweise entspricht und weder der Auftraggeber noch die Auftragnehmer zusätzlich belastet werden.
Die Stichprobe muss den mit dem Auftraggeber und dessen Nachauftragnehmern und Personalverleihern getroffenen Vereinbarungen entsprechen oder in der geänderten Form vereinbart werden.

### § 5   
Datenschutz

(1) Personenbezogene Daten werden unkenntlich gemacht, sobald feststeht, dass sie nicht zum sachgerechten Vortrag in einem gerichtlichen Verfahren oder als Beweismittel benötigt werden.

(2) Der Auftraggeber hat die im Rahmen der Kontrolle entgegengenommenen nicht anonymisierten oder pseudonymisierten Belege oder Kopien von Belegen separat und vor allgemeinem Zugriff geschützt aufzubewahren.
Besteht keine Beanstandung wegen einer Vertragsverletzung oder wird von § 3 Absatz 7 Satz 4 Gebrauch gemacht, sind die Dokumente zurückzugeben oder, wenn diese zum endgültigen Verbleib beim Auftraggeber übergeben wurden, zu vernichten.
Sollen sie die Durchführung von Kontrollen nachweisen, sind sie zu anonymisieren.
Aufbewahrungspflichten auf Grund anderer Rechtsvorschriften oder Verpflichtungen bleiben hiervon unberührt.

(3) Soweit Auftraggeber im Sinne von § 2 Absatz 4 des Brandenburgischen Vergabegesetzes Aufträge vergeben, gilt über die Absätze 1 und 2 hinaus das Brandenburgische Datenschutzgesetz in der Fassung der Bekanntmachung vom 15.
Mai 2008 (GVBl.
I S.
114), das zuletzt durch Gesetz vom 27. Juli 2015 (GVBl. I Nr. 22) geändert worden ist, für diese sinngemäß.

### § 6  
Verzeichnisse über geeignete Unternehmen und Sammlungen von Eignungsnachweisen

(1) Als Verzeichnis über geeignete Unternehmen ist die allgemein zugängliche Liste des Vereins für die Präqualifikation von Bauunternehmen e.V. (Präqualifikationsverzeichnis nach § 6b der Vergabe- und Vertragsordnung für Bauleistungen Teil A vom 1.
Juli 2016, BAnz. AT 01.07.2016 B4) von allen Auftraggebern bei der Anwendung der Vergabe- und Vertragsordnung für Bauleistungen Teil A anzuerkennen.

(2) Die Zulassung von Sammlungen von Eignungsnachweisen im Sinne von § 5 Absatz 1 Satz 1 des Brandenburgischen Vergabegesetzes setzt voraus, dass

1.  das Verzeichnis über eine Verbreitung bei mindestens zehn Vergabestellen als Nutzer und mindestens 50 gelisteten Unternehmen verfügt;
2.  die das Verzeichnis führende Stelle von den einzelnen registrierten Unternehmen wirtschaftlich unabhängig ist;
3.  die das Verzeichnis führende Stelle mindestens drei Jahre auf dem Gebiet des öffentlichen Auftragswesens tätig ist oder auf andere Weise Gewähr für einen längerfristigen Bestand bietet;
4.  die das Verzeichnis führende Stelle über eine Haftpflichtdeckung von mindestens 300 000 Euro zugunsten der Auftraggeber verfügt, die das Verzeichnis nutzen;
5.  die Sammlungen den inhaltlichen Anforderungen des § 48 Absatz 8 der Vergabeverordnung entsprechen;
6.  für die eingetragenen Unternehmen die Möglichkeit besteht, nach eigener Wahl weitere eignungsrelevante Daten einzupflegen;
7.  die Einzelnachweise der Unternehmen im Internet für die Vergabestellen einsehbar vorgehalten werden;
8.  die das Verzeichnis führende Stelle regelmäßig Insolvenzrecherchen durchführt;
9.  das Verzeichnis den Entwicklungen im Vergaberecht und in der Vergabepraxis umgehend angepasst wird;
10.  die Eintragung in das Verzeichnis in deutscher Sprache bestätigt wird;
11.  die Bestätigung keinen weitergehenden Inhalt als die Tatsache der Eintragung im Verzeichnis hat und nicht den Eindruck einer Präqualifikation erweckt;
12.  die Eintragung jedem Unternehmen unabhängig vom Sitz zu gleichen Bedingungen möglich ist, zu denen nicht das Erfordernis einer Mitgliedschaft in einem Verein oder einer Körperschaft gehört;
13.  die Nutzung des Verzeichnisses für die Vergabestellen entgeltfrei ist.

(3) Das Vorliegen der Voraussetzungen für eine Zulassung stellt das für Wirtschaft zuständige Mitglied der Landesregierung auf Antrag der das Verzeichnis führenden Stelle anhand der dem Antrag beigefügten oder auf Anforderung nachgereichten Unterlagen kostenfrei für die Dauer von fünf Jahren fest.
Die Zulassung ist im Amtsblatt für Brandenburg bekannt zu machen.
Der nachfolgende Antrag kann im Jahr vor Ablauf der Befristung gestellt werden.

(4) Die Zulassung bedeutet, dass die Belege, die elektronisch einzusehen sind, nicht als Einzelbelege zusätzlich angefordert werden dürfen.

(5) In anderen Ländern der Bundesrepublik Deutschland, EU-Mitgliedstaaten und Vertragsstaaten des einheitlichen Europäischen Wirtschaftsraumes oder des Übereinkommens über das öffentliche Beschaffungswesen in Anhang 4 des Übereinkommens zur Errichtung der Welthandelsorganisation vom 15.
April 1994 (BGBl.
II S.
1625) bestehende Präqualifikations-Register sind als dem nach Absatz 1 zugelassenen Verzeichnis gleichwertig zugelassen, wenn die Register die Beteiligung an Vergabeverfahren um öffentliche Aufträge im jeweiligen Land ermöglichen oder die Zuverlässigkeit oder Eignung bestätigen.
Den Nachweis dieser Zulassung und des Inhalts der Bestätigung hat der Bewerber oder Bieter zu führen, der sich darauf beruft.

### § 7  
Verhängung und Bemessung einer Auftragssperre

(1) Liegen die Voraussetzungen des § 10 des Brandenburgischen Vergabegesetzes vor, ist der Bewerber oder Bieter von öffentlichen Auftraggebern im Sinne des § 2 Absatz 3 des Brandenburgischen Vergabegesetzes, unter Berücksichtigung der Umstände des Einzelfalles in der Regel von der Vergabe auszuschließen.
Die regelmäßige Dauer der Auftragssperre beträgt bei einer Verfehlung mindestens sechs Monate.
Sie nimmt mit der Zahl der Verfehlungen zu bis auf drei Jahre.
Die Zahl der Verfehlungen ist die Summe aus der Anzahl der beeinträchtigten Kontrollen und der Anzahl der betroffenen Entgeltempfänger, für die keine ordnungsgemäße Entgeltzahlung festgestellt werden kann.
Vor der Entscheidung des Auftraggebers erhält der Auftragnehmer Gelegenheit zur Stellungnahme zum ermittelten Sachverhalt und den Gesichtspunkten zur Bemessung der Dauer einer Auftragssperre.

(2) Die Vergabestelle hat im Einzelfall insbesondere anhand der folgenden Kriterien zu prüfen, ob der Auftragnehmer ausnahmsweise nicht oder abweichend von der in Absatz 1 vorgesehenen Regelzeit auszuschließen ist:

1.  die Dauer und Höhe der nicht ordnungsgemäßen Bezahlung;
2.  das Maß der mangelnden Mitwirkung bei Kontrollen;
3.  das Verhalten gegenüber dem Kontrollpersonal;
4.  die Auswirkungen eines Vertragsverstoßes auf den öffentlichen Auftraggeber;
5.  bereits ergriffene organisatorische und personelle Maßnahmen durch den Unternehmer, um weitere Gesetzesverstöße zu vermeiden;
6.  ob der Ausschluss von öffentlichen Aufträgen den Bestand des Unternehmens gefährdet oder
7.  ob der Ausschluss den Bewerber- oder Bieterkreis so sehr verkleinert, dass ein Wettbewerb nicht mehr stattfindet.

(3) In den Fällen des Absatzes 2 Nummer 6 und 7 kann von einer Auftragssperre abgesehen werden, wenn das Unternehmen glaubhaft macht, die Wiederherstellung der Zuverlässigkeit zügig zu betreiben.
Im Fall der Nummer 7 kommt auch bei verhängter Sperre eine Aufforderung in einer beschränkten Ausschreibung auf Grund des beschränkten Bieterkreises im Einzelfall in Betracht.

### § 8  
Wiederherstellung der Zuverlässigkeit und Aufhebung einer Auftragssperre vor ihrem Ablauf

(1) Die Wiederherstellung der Zuverlässigkeit wird auf Antrag eines Unternehmens durch die Vergabestelle geprüft, die die Auftragssperre verhängt hat.
Sie ist in der Regel nachgewiesen, wenn zur freien Überzeugung der Vergabestelle hinreichend belegt wird, dass

1.  die Nachzahlung der Bruttoarbeitsentgelte erfolgt ist oder alle Nachweise erbracht sind, wegen deren Vorenthaltung ein Vertragsverstoß festgestellt wurde;
2.  im letzten Abrechnungsmonat vor der Antragstellung im fachlich betroffenen Unternehmensteil nur Arbeitsentgelte, die mindestens dem Mindestarbeitsentgelt nach § 6 Absatz 2 des Brandenburgischen Vergabegesetzes entsprechen, bezahlt wurden und dies nachgewiesen wurde, und
3.  personelle und organisatorische Maßnahmen zur Verhinderung weiterer Verstöße ergriffen wurden.

(2) Der Nachweis geeigneter personeller und organisatorischer Maßnahmen im Sinne von Absatz 1 Satz 2 Nummer 3 kann insbesondere geführt werden durch:

1.  Personalwechsel bei persönlich Verantwortlichen;
2.  Kopien von Belehrungen der nicht unmittelbar betroffenen Beschäftigten mit Führungsaufgaben durch den Arbeitgeber über die vertragliche Verpflichtung,
    1.  die Bestimmungen der Vereinbarungen auf der Grundlage des Brandenburgischen Vergabegesetzes gegenüber den davon geschützten Beschäftigteninteressen einzuhalten;
    2.  ihre Einhaltung durch andere Beschäftigte zu überwachen;
    3.  keine Preisangebote von Nachunternehmern und Personalverleihern ohne Aufklärung zu akzeptieren, deren geringe Höhe einen Verstoß gegen die Verpflichtung zur Zahlung des Mindestarbeitsentgelts möglich erscheinen lassen;
    4.  der Vergütungsberechnung alle tatsächlich geleisteten Arbeitsstunden zugrunde zu legen, nach Maßgabe für das Unternehmen geltender tariflicher Bestimmungen, die ein Ansparen von Arbeitszeiten zulassen;
    5.  keine von den Buchstaben a bis d abweichenden Anordnungen zu befolgen oder zu erteilen;und
3.  Kopien der Zusage des Arbeitsgebers an die Beschäftigten, keine rechtlichen Nachteile bei Einhaltung der Verpflichtungen nach Nummer 1 Buchstabe a bis e, aber arbeits- oder dienstvertragliche Nachteile bei ihrer Nichteinhaltung und Regressansprüche gewärtigen zu müssen;  
    oder
4.  andere Belege, die die Unterrichtung der Mitarbeiter über den Inhalt ihrer Verpflichtungen aus den nach den Vorgaben des Brandenburgischen Vergabegesetzes geschlossenen Verträgen und die Möglichkeit zur arbeits- oder dienstvertraglichen Ahndung von Verstößen gegen diese Verpflichtungen vergleichbar umfassend und deutlich, gegebenenfalls angepasst an die konkrete Arbeitsaufgabe des Beschäftigten zum Ausdruck bringen; bei kleineren Unternehmen und Handwerksbetrieben mit nur einer Führungsebene genügt die Eigenerklärung des Inhabers oder Geschäftsführers.

(3) Art und Inhalt des Nachweises nach Absatz 1 Satz 2 sind aktenkundig zu machen.
Eine Verkürzung oder Aufhebung der Auftragssperre ist der Stelle zu melden, die die Sperrliste führt.
Die Aufhebung der Auftragssperre des Auftraggebers nach dem Brandenburgischen Vergabegesetz ist unabhängig davon, ob eine Auftragssperre auf der Grundlage einer Bußgeldentscheidung nach dem Arbeitnehmer-Entsendegesetz besteht.

### § 9  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 16.
Oktober 2012

Der Minister für Wirtschaft  
und Europaangelegenheiten

Ralf Christoffers