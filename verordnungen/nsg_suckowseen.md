## Verordnung über das Naturschutzgebiet „Suckowseen“

Auf Grund des § 22 Absatz 1 und 2, der §§ 23 und 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2, § 21 Absatz 1 Satz 2 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1   
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Uckermark wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Suckowseen“.

### § 2   
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 119 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Gemeinde:**

**Gemarkung:**

**Flur:**

Boitzenburger Land

Boitzenburg  
  
Klaushagen  
  
Wichmannsdorf

8, 9;  
  
1, 3;  
  
3.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in der Anlage 2 Nummer 1 aufgeführte topografische Karte im Maßstab 1 : 10 000 ermöglicht die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 2 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Die Verordnung mit Karten und Flurstücksliste kann beim Ministerium für Umwelt, Gesundheit und Verbraucherschutz des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam, sowie beim Landkreis Uckermark, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

### § 3  
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensräume wild lebender Pflanzengesellschaften, insbesondere der Buchenwälder, Grauweiden-Schwarzerlen-Sumpfwaldgesellschaften, Grundrasen- und Tauchflurengesellschaften nährstoffarmer Seen, Schwimmblattgesellschaften, Pflanzengesellschaften der Seggen- und Röhrichtmoore sowie der Staudenfluren;
2.  die Erhaltung und Entwicklung der Lebensräume wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Wasserfeder (Hottonia palustris), Leberblümchen (Hepatica nobilis), Krebsschere (Stratiotes aloides) und Froschbiss (Hydrocharis morsus-ranae);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundes-naturschutzgesetzes besonders und streng geschützte Arten, insbesondere Drosselrohrsänger (Acrocephalus arundinaceus), Moorfrosch (Rana arvalis), Große Goldschrecke (Chrysochraon dispar) und Sibirische Winter-libelle (Sympecma paedisca);
4.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit eines unzerschnittenen, störungsarmen und reich gegliederten Gebietes, geprägt von der Seenkette der Suckowseen mit angrenzenden Wäldern, Offenland, Gebüschen und Feld- und Obstgehölzen;
5.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Biotopverbundes zwischen den Lychener Gewässern und dem Boitzenburger Strom.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung

1.  eines Teils des Europäischen Vogelschutzgebietes „Uckermärkische Seenlandschaft“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) in seiner Funktion als Lebensraum von Arten nach Anhang I der Richtlinie 2009/147/EG, insbesondere Seeadler (Haliaeetus albicilla), Fischadler (Pandion haliaetus), Rohrweihe (Circus aeruginosus), Schwarzstorch (Ciconia nigra), Kranich (Grus grus), Große Rohrdommel (Botaurus stellaris), Schellente (Bucephhala clangula), Schwarzspecht (Dryocopus martius), Mittelspecht (Dendrocopus medius) und Zwergschnäpper (Ficedula parva) einschließlich ihrer Brut- und Nahrungsbiotope; Auen-Wälder mit Alnus glutinosa (Schwarzerle) und Fraxinus excelsior (\[Gewöhnliche Esche\] \[Alno-Padion, Alnion incanae, Salicion albae\]) als prioritärer Biotop („prioritärer Lebensraumtyp“ im Sinne des Anhangs I der Richtlinie 92/43/EWG);
2.  des Gebietes von gemeinschaftlicher Bedeutung „Suckowseen“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), das ehemals einen Teil des Gebietes von gemeinschaftlicher Bedeutung „Stromgewässer“ umfasste, mit seinen Vorkommen von
    1.  natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Hainsimsen-Buchenwäldern (Luzulo-Fagetum) und Waldmeister-Buchenwäldern (Asperulo-Fagetum) als Biotope von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG),
    2.  Fischotter (Lutra lutra), Biber (Castor fiber), Rotbauchunke (Bombina bombina), Kammmolch (Triturus cristatus) als Tierarten von gemeinschaftlichem Interesse (im Sinne des Anhangs II der Richtlinie 92/43/EWG), einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist außerhalb von Verlandungszonen, Röhrichten, Moor- und Bruchwäldern das Betreten zum Zwecke der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 11 jeweils nach dem 30.
    Juni eines jeden Jahres;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der Wege, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können und außerhalb der Waldbrandwundstreifen zu reiten;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  zu baden oder zu tauchen; ausgenommen ist das Baden und Tauchen im Großen Suckowsee von der in den Karten zu dieser Verordnung nach § 2 Absatz 2 (Topografische Karte im Maßstab 1 : 10 000 und Liegenschaftskarte, Blattnummer 2) eingetragenen Badestelle;
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen; ausgenommen ist das Befahren des Großen Suckowsees mit muskelkraftbetriebenen Booten sowie Luftmatratzen außerhalb von Röhrichten, Schwimmblattgesellschaften oder Verlandungsbereichen.
    Das Einsetzen und Anlegen der Boote ist nur an dem in den Karten zu dieser Verordnung nach § 2 Absatz 2 (Topografische Karte im Maßstab 1 : 10 000 und Liegenschaftskarte, Blattnummer 2) eingezeichneten Uferabschnitt zulässig;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle, Rückstände aus Biogasanlagen) und Sekundärrohstoffdünger (zum Beispiel Abwasser, Klärschlamm und Bioabfälle) zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland umzubrechen oder neu anzusäen.

### § 5  
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle, Rückstände aus Biogasanlagen mit Nassvergärung oder Sekundärrohstoffdünger wie zum Beispiel Abwasser, Klärschlamm und Bioabfälle einzusetzen,
    2.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  der in § 3 Absatz 2 Nummer 2 Buchstabe b genannten Lebensraumtyp auf den Flurstücken 167, 168, 169, Flur 3 der Gemarkung Klaushagen nicht bewirtschaftet wird, im Übrigen eine Nutzung der Laubwälder einzelstamm- bis truppweise erfolgt,
    2.  in Misch- und Nadelwäldern Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrates reduzieren, nur bis zu einer Größe von maximal 0,5 Hektar zulässig sind,
    3.  nur Baumarten der potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind.
        Nebenbaumarten dürfen dabei nicht als Hauptbaumarten eingesetzt werden,
    4.  der Boden unter Verzicht auf Pflügen und Umbruch bearbeitet wird; ausgenommen ist eine streifenweise, flachgründige, nicht in den Mineralboden eingreifende Bodenverwundung zur Unterstützung von Verjüngungsmaßnahmen,
    5.  das Befahren des Waldes nur auf Wegen oder Rückegassen erfolgt,
    6.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    7.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß und einer Mindesthöhe von 5 Metern nicht gefällt werden und liegendes Totholz (ganze Bäume mit einem Durchmesser über 65 Zentimeter am stärksten Ende) im Bestand verbleibt; das stehende Totholz ist mit einer dauerhaften Markierung zu versehen,
    8.  § 4 Absatz 2 Nummer 17 und 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Besatzmaßnahmen auf den Suckowseen nur mit Hecht, Wels, Barsch, Schlei, Aal und Kleiner Maräne durchgeführt werden,
    2.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass ein Einschwimmen und eine Gefährdung des Fischotters weitgehend ausgeschlossen sind,
    3.  § 4 Absatz 2 Nummer 19 gilt;
4.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass
    1.  diese am Kleinen Suckowsee von den rechtmäßig errichteten Stegen aus, am Großen Suckowsee von den rechtmäßig errichteten Stegen und von den Booten aus sowie am Mittleren Suckowsee von den rechtmäßig errichteten Stegen und von dem in den Karten zu dieser Verordnung nach § 2 Absatz 2 (Topografische Karte im Maßstab 1 : 10 000 und Liegenschaftskarte, Blattnummer 2) eingezeichneten Uferabschnitt aus zulässig ist,
    2.  § 4 Absatz 2 Nummer 13, 19 und 20 gilt;
5.  für den Bereich der Jagd:
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass  
        aa)   die Jagd auf Wasservögel verboten ist,  
        bb)   die Fallenjagd mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern zum Ufer aller innerhalb des Schutzgebietes liegenden Gewässer verboten ist.
        Die untere Naturschutzbehörde kann eine Genehmigung  
                 für die Fallenjagd mit Lebendfallen innerhalb dieses Abstands erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,  
        cc)    keine Baujagd in einem Abstand von bis zu 100 Metern vom Ufer aller innerhalb des Schutzgebietes liegenden Gewässer vorgenommen wird,
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde erfolgt.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    3.  das Aufstellen transportabler und mobiler Ansitzeinrichtungen,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope.Im Übrigen bleiben Wildfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern unzulässig;
6.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Instandhaltung und Instandsetzung der dem öffentlichen Verkehr gewidmeten Straßen und Wege hinsichtlich der Fahrbahn und des Banketts sowie des zur Badestelle am Großen Suckowsee führenden Weges in der Zeit ab 1.
    Juli eines jeden Jahres, sofern eine Beschädigung des Gehölzbestandes ausgeschlossen ist.
    Die Maßnahmen sind der unteren Naturschutzbehörde vorab anzuzeigen.
    Alle Unterhaltungsmaßnahmen an sonstigen rechtmäßig bestehenden Anlagen und sonstige Maßnahmen an den dem öffentlichen Verkehr gewidmeten Straßen und Wegen außerhalb des genannten Zeitraums bedürfen des Einvernehmens mit der unteren Naturschutzbehörde;
7.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, die den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
8.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen  
    (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen.
    Die ordnungsgemäße Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen kann durch langfristig gültige Vereinbarungen hergestellt werden;
9.  die Nutzung des Flurstückes 167, Flur 1 der Gemarkung Klaushagen im Bereich der Badestelle am Südufer des Großen Suckowsees für die Durchführung von Dorffesten der Gemeinde Boitzenburger Land einschließlich der dafür notwendigen Vorbereitungsmaßnahmen für die Dauer von insgesamt höchstens vier Tagen jeweils nach dem 30.
    Juni eines jeden Jahres;
10.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
11.  das Sammeln von Pilzen und Waldfrüchten in geringen Mengen für den persönlichen Gebrauch jeweils nach dem 30.
    Juni eines jeden Jahres;
12.  Maßnahmen zur Untersuchung von altlastenverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
13.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
14.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 an Straßen und Wegen freigestellt;
15.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutz-behörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

### § 6  
Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  der Stau am Abfluss des Großen Suckowsees soll durch eine Sohlschwelle zur Verbesserung der Wasserrückhaltung und der Durchgängigkeit des Gewässersystems ersetzt werden;
2.  im Moor zwischen Mittlerem und Kleinem Suckowsee soll durch geeignete Maßnahmen ein hoher Wasserstand gewährleistet werden;
3.  am Paddenbruch nördlich des Kleinen Suckowsees wird zur Wasserrückhaltung der Einbau einer neuen Sohlschwelle angestrebt;
4.  die Wiederherstellung von Feldsöllen östlich des Kleinen Suckowsees wird angestrebt;
5.  aus allen Suckowseen sollen nicht heimische Fischarten und der Karpfenbestand vollständig entnommen werden;
6.  der südlich des Großen Suckowsees liegende Acker soll in extensives Grünland umgewandelt werden;
7.  Erosions- und Trittschäden am Südufer des Großen Suckowsees sollen durch geeignete Maßnahmen vermieden werden.
    Es wird an der in § 4 Absatz 2 Nummer 13 Satz 2 genannten Stelle die Errichtung eines Sammelsteges angestrebt;
8.  naturferne Forsten sollen zu naturnahen der potenziell natürlichen Vegetation ausgerichteten Waldgesellschaften entwickelt werden.

### § 7   
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

### § 8  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu 50 000 (in Worten: fünfzigtausend) Euro geahndet werden.

### § 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere §§ 31 bis 33 und § 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

### § 10  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

### § 11  
Inkrafttreten

§ 5 Absatz 1 Nummer 1 Buchstabe a und b tritt am 1.
Juli 2011 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 18.
Mai 2011

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

* * *

**Anlage 1** (zu § 2 Absatz 1)

![Das Naturschutzgebiet Suckowseen befindet sich im Landkreis Uckermark in der Gemeinde Boitzenburger Land und umfasst Teile der Gemarkungen Boitzenburg, Klaushagen und Wichmannsdorf.](/br2/sixcms/media.php/69/Nr.%2027-2.JPG "Das Naturschutzgebiet Suckowseen befindet sich im Landkreis Uckermark in der Gemeinde Boitzenburger Land und umfasst Teile der Gemarkungen Boitzenburg, Klaushagen und Wichmannsdorf.")

**Anlage 2** (zu § 2 Absatz 2)

#### 1.
Topografische Karten Maßstab 1 : 10 000

Titel: Topografische Karte zur Verordnung über das Naturschutzgebiet „Suckowseen“

Blattnummer

Unterzeichnung

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV), am 29.
April 2011

#### 2.
Liegenschaftskarten Maßstab 1 : 2 500

Titel: Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Suckowseen“

Blattnummer

Gemarkung

Flur

Unterzeichnung

1

Boitzenburg  
Klaushagen

9  
1, 3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 29.
April 2011

2

Boitzenburg  
Klaushagen  
Wichmannsdorf

8, 9  
3  
3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 29.
April 2011

**Anlage 3  
**(zu § 2 Absatz 2)

#### Flurstücksliste zur Verordnung über das Naturschutzgebiet „Suckowseen“

**Landkreis: Uckermark**

**Gemeinde: Boitzenburger Land**

**Gemarkung**

**Flur**

**Flurstück**

Boitzenburg

8

38 (anteilig), 39 (anteilig), 40/1, 40/2, 40/3 (anteilig), 41 bis 45, 47 bis 51, 53, 54;

9

12(anteilig), 21, 22 (anteilig), 23 bis 25, 27 bis 30;

Klaushagen

1

167/1, 172/1, 373, 374, 376 bis 386;

3

145/1, 146 bis 149, 166 (anteilig), 167 (anteilig), 168 (anteilig), 169, 170, 171 (anteilig), 172 (anteilig), 176 (anteilig), 177 bis 182, 183 (anteilig), 184 (anteilig), 185 (anteilig);

Wichmannsdorf

3

1 bis 11, 12 (anteilig), 13 (anteilig), 101 bis 110.