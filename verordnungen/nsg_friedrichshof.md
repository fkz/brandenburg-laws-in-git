## Verordnung über das Naturschutzgebiet „Friedrichshof“

Auf Grund des § 22 Absatz 1 und 2, der §§ 23 und 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl.
I S.
1972, 1974) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl. I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl. II Nr. 43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Dahme-Spreewald wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Friedrichshof“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 120 Hektar.
Es besteht aus zwei Teilflächen südlich und nördlich der Ortslage Friedrichshof.
Es umfasst Flächen in folgenden Fluren:

Gemeinde:

Gemarkung:

Flur:

Flurstücke:

Rietzneuendorf-Staakow

Friedrichshof

1

86 bis 99 (jeweils anteilig), 101/1 (anteilig), 102 bis 106  
(jeweils anteilig), 108/1 (anteilig), 120 (anteilig), 121  
(anteilig), 124/1 (anteilig), 124/2 (anteilig), 138 (anteilig),  
139, 140, 141 (anteilig), 143 (anteilig), 144 (anteilig),  
207/1 (anteilig), 246 (anteilig), 247 (anteilig);

Friedrichshof

2

2, 4 (anteilig), 20 (anteilig), 21, 23 (anteilig);

Rietzneuendorf

1

17 (anteilig), 18 (anteilig), 20 (anteilig), 30 (anteilig),  
36 (anteilig), 185 bis 187, 188 (anteilig), 189 bis 201,  
202 (anteilig), 203 (anteilig), 204 bis 207, 208 (anteilig),  
209 (anteilig), 219 (anteilig), 220 (anteilig), 230.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte topografische Karte im Maßstab 1 : 10 000 mit der Blattnummer 1 ermöglicht die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 4 aufgeführten Liegenschaftskarten.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Dahme-Spreewald, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes, als einem von Wald geprägten Teil des Baruther Tales mit Dünen und Niederungsbereichen, ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere von Laubwäldern, naturnahen Fließgewässern, Dünen und Wäldern trockener und nährstoffarmer Standorte sowie Sandtrockenrasen;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Gewöhnliche Grasnelke (Armeria maritima), Sandstrohblume (Helichrysum arenarium), Leberblümchen (Hepatica nobilis), Wasser-Schwertlilie (Iris pseudacorus) und Weiße Waldhyazinthe (Platanthera bifolia);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere für an Waldlebensräume und trockene Lebensräume gebundene Vogel-, Reptilien- und Insektenarten;
4.  die Erhaltung der Dünenstandorte aus naturgeschichtlichen Gründen;
5.  die Erhaltung des Gebietes aufgrund seiner Seltenheit, besonderen Eigenart und hervorragenden Schönheit;
6.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des landesweiten Biotopverbundes im Baruther Tal zwischen dem Fläming und dem ostbrandenburgischen Heide- und Seengebiet.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teils des Gebietes von gemeinschaftlicher Bedeutung „Glashütte/Mochheide“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinen Vorkommen von

1.  Dünen mit offenen Grasflächen mit Corynephorus und Agrostis, Mitteleuropäischem Stieleichenwald oder Eichen-Hainbuchenwald (Carpinion betuli), Alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur sowie Mitteleuropäischen Flechten-Kiefernwäldern als natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Fischotter (Lutra lutra), Mopsfledermaus (Barbastella barbastellus) sowie Heldbock (Cerambyx cerdo) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume;
3.  Eremit (Osmoderma eremita) als prioritäre Art im Sinne von § 7 Absatz 2 Nummer 11 des Bundesnaturschutzgesetzes, einschließlich seiner für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten zum Zweck des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 11;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wegen zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit Fahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
12.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
13.  Hunde frei laufen zu lassen;
14.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
15.  Düngemittel aller Art zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
16.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
17.  Tiere zu füttern oder Futter bereitzustellen;
18.  Tiere auszusetzen oder Pflanzen anzusiedeln;
19.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
20.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
21.  Pflanzenschutzmittel jeder Art anzuwenden;
22.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle, Jauche, flüssige Gärreste und Sekundärrohstoffdünger einzusetzen.
          
          
        Sekundärrohstoffdünger im Sinne dieser Verordnung sind Abwasser, Fäkalien, Klärschlamm und ähnliche Stoffe aus Siedlungsabfällen und vergleichbare Stoffe aus anderen Quellen, jeweils auch weiterbehandelt und in Mischungen untereinander oder mit Düngemitteln, Wirtschaftsdünger, Bodenhilfsstoffen, Kultursubstraten und Pflanzenhilfsmitteln,
    2.  auf Grünland § 4 Absatz 2 Nummer 21 und 22 gilt.
        Bei Wildschäden ist mit Zustimmung der unteren Naturschutzbehörde eine umbruchlose Nachsaat zulässig;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  § 4 Absatz 2 Nummer 15 und 21 gilt,
    2.  in den in § 3 Absatz 2 Nummer 1 genannten Waldlebensraumtypen Alte bodensaure Eichenwälder auf Sandebenen mit Quercus robur und Mitteleuropäischem Flechten-Kiefernwald die Kalkung unzulässig ist,
    3.  in den in § 3 Absatz 2 Nummer 1 genannten Waldlebensraumtypen die Walderneuerung durch Naturverjüngung erfolgt.
        Sofern keine ausreichende Naturverjüngung möglich ist, dürfen ausschließlich lebensraumtypische Baumarten eingebracht werden,
    4.  auf den übrigen Waldflächen nur Arten der potenziell natürlichen Vegetation einzubringen sind.
        Es sind nur heimische Baumarten in gesellschaftstypischer Zusammensetzung unter Ausschluss eingebürgerter Arten zu verwenden,
    5.  in den in § 3 Absatz 2 Nummer 1 genannten Waldlebensraumtypen eine dauerwaldartige Nutzung einzelstamm- bis gruppenweise erfolgt, die einen fließenden Generationenübergang verschiedener Altersstadien gewährleistet,
    6.  auf den übrigen Waldflächen Holzerntemaßnahmen, die den Holzvorrat auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von 0,5 Hektar zulässig sind.
        Hier sind benachbarte Flächen zu berücksichtigen,
    7.  in dem in § 3 Absatz 2 Nummer 1 genannten Waldlebensraumtyp Mitteleuropäischer Flechten-Kiefernwald in der oberen Baumschicht ein Anteil von Gehölzen in der Altersphase mit mindestens 20 Prozent Deckung zu erhalten ist,
    8.  in den in § 3 Absatz 2 Nummer 1 genannten Waldlebensraumtypen Alte bodensaure Eichenwälder auf Sandebenen mit Quercus robur und Mitteleuropäischem Stieleichenwald oder Eichen-Hainbuchenwald (Carpinion betuli) lebensraumtypische Gehölze in der Reifephase mit einer Deckung von mindestens 25 Prozent zu erhalten oder, sofern nicht vorhanden, zu entwickeln sind,
    9.  in dem in § 3 Absatz 2 Nummer 1 genannten Waldlebensraumtyp Mitteleuropäischer Stieleichenwald oder Eichen-Hainbuchenwald (Carpinion betuli) mindestens 21 Kubikmeter je Hektar liegendes oder stehendes Totholz mit einem Durchmesser von mindestens 35 Zentimetern bei Eichen und mindestens 25 Zentimetern bei anderen Baumarten im Bestand zu erhalten, oder sofern nicht vorhanden, zu entwickeln sind,
    10.  in den in § 3 Absatz 2 Nummer 1 genannten Waldlebensraumtypen Alte bodensaure Eichenwälder auf Sandebenen mit Quercus robur und Mitteleuropäischem Flechten-Kiefernwald mindestens 11 Kubikmeter je Hektar liegendes oder stehendes Totholz mit einem Durchmesser von mindestens 35 Zentimetern bei Eichen und mindestens 25 Zentimetern bei anderen Baumarten im Bestand zu erhalten, oder sofern nicht vorhanden, zu entwickeln sind,
    11.  die am südlichen Waldrand der nördlichen Teilfläche stehende Eichenreihe nicht beseitigt werden darf.
        Die einzelnen Eichen sind bis zum Absterben und Zerfall am Standort zu belassen.
        Ihr Standort ist in der in § 2 Absatz 2 genannten topografischen Karte eingezeichnet,
    12.  Horst- und Höhlenbäume nicht gefällt werden dürfen,
    13.  das Befahren des Waldes nur auf Waldwegen und Rückegassen erfolgt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung auf den rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters weitgehend ausgeschlossen ist,
    2.  Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne einer Fischbestandskontrolle, -regulierung und -förderung mit Zustimmung der unteren Naturschutzbehörde erfolgen.
        Die Zustimmung ist zu erteilen, wenn sie dem Schutzzweck nicht entgegensteht,
    3.  § 4 Absatz 2 Nummer 17 gilt;
4.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass § 4 Absatz 2 Nummer 17 und 18 gilt;
5.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Fallenjagd mit Lebendfallen erfolgt und innerhalb des Schutzgebietes in einem Abstand von bis zu 100 Metern vom Gewässerufer des Buschgrabens verboten ist.
        Ausnahmen bedürfen einer Genehmigung der unteren Naturschutzbehörde,
        
        bb)
        
        innerhalb des Schutzgebietes keine Baujagd in einem Abstand von bis zu 100 Metern vom Gewässerufer des Buschgrabens vorgenommen wird,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    3.  das Aufstellen transportabler und mobiler Ansitzeinrichtungen,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und der in § 3 Absatz 2 Nummer 1 genannten Lebensraumtypen.
    
    Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und die Anlage und Unterhaltung von Wildäckern sind unzulässig.
    Im Übrigen bleibt § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
6.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege.
    Die untere Naturschutzbehörde ist rechtzeitig mit dem Ziel einer einvernehmlichen Lösung zu beteiligen;
7.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
8.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
9.  die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern diese nicht unter die Nummern 6 und 8 fallen, im Einvernehmen mit der unteren Naturschutzbehörde;
10.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
11.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch;
12.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
13.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der zuständigen Naturschutzbehörde zugelassen oder angeordnet worden sind;
14.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen;
15.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungserfordernissen, die sich aus anderen Vorgaben ergeben, unberührt.

#### § 6 Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  im Lebensraumtyp Dünen mit offenen Grasflächen sollen offene Sandflächen durch geeignete Pflegemaßnahmen erhalten oder neu geschaffen werden;
2.  die im Gebiet liegenden Trockenrasen sollen in mehrjährigen Abständen gemäht und gegebenenfalls entbuscht werden.
    Auf ruderalisierten Standorten kann eine intensivere Pflege zur Schaffung einer typischen Biotopausprägung erfolgen;
3.  nicht standortgerechte sowie nicht heimische Baum- und Straucharten sollen aus Wäldern sowie aus Gehölzgruppen und -reihen entfernt werden.
    Die Kiefernreinbestände sollen mittel- bis langfristig in naturnahe Laubwald- oder Laub-Nadelwaldbestände umgebaut werden;
4.  die Brutbäume von Eremit und Heldbock sollen teilweise freigestellt werden, um ein geeignetes Mikroklima in den von Käfern besiedelten Baumhöhlungen sicherzustellen.
    Dies soll so erfolgen, dass keine erhöhte Windwurfgefahr eintritt;
5.  zum dauerhaften Erhalt der in der in § 2 Absatz 2 genannten topografischen Karte gekennzeichneten Eichenreihe sollen abgehende Eichen ersetzt werden.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 1 Nummer 1 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

§ 5 Absatz 1 Nummer 1 Buchstabe a und b tritt am 1.
Januar 2019 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 21.
November 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Friedrichshof“](/br2/sixcms/media.php/68/GVBl_II_88_2018-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Friedrichshof“") 550.0 KB

2

[Anlage 2 (zu § 2 Absatz 2) - Topografische Karte, Liegenschaftskarten](/br2/sixcms/media.php/68/GVBl_II_88_2018-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Topografische Karte, Liegenschaftskarten") 190.6 KB