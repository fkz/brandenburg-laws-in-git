## Verordnung über die Gebühren für öffentliche Leistungen im Bereich der Straßenbahnen, Oberleitungsbusse und Eisenbahnen (GebOSOE)

Auf Grund des § 1 Absatz 1, des § 3 Absatz 1 und des § 18 Absatz 2 Satz 2 des Gebührengesetzes für das Land Brandenburg vom 7.
Juli 2009 (GVBl.
I S. 246), von denen § 18 Absatz 2 Satz 2 durch Artikel 5 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 32 S. 27) neu gefasst worden ist, verordnet die Ministerin für Infrastruktur und Landesplanung:

#### § 1 Gebührenpflichtige Leistungen

Für öffentliche Leistungen im Bereich der Eisenbahnen, Parkeisenbahnen, Straßenbahnen und Oberleitungsbusse (Obusse), die im anliegenden Gebührenverzeichnis, welches Bestandteil dieser Verordnung ist, aufgeführt sind, werden die dort genannten Gebühren erhoben.
Auslagen werden nach § 9 des Gebührengesetzes für das Land Brandenburg erhoben.

#### § 2 Zeitgebühr

Soweit Gebühren nach dem Zeitaufwand zu berechnen sind, sind der Gebührenberechnung folgende Stundensätze zugrunde zu legen:

1.  für Bedienstete des höheren Dienstes oder vergleichbare Tarifbeschäftigte 77 Euro,
2.  für Bedienstete des gehobenen Dienstes oder vergleichbare Tarifbeschäftigte 57 Euro,
3.  für Bedienstete des mittleren Dienstes oder vergleichbare Tarifbeschäftigte 46 Euro,
4.  für Bedienstete des einfachen Dienstes oder vergleichbare Tarifbeschäftigte 38 Euro.

Bei der Ermittlung der Zeitgebühr ist die Zeit anzusetzen, die unter regelmäßigen Verhältnissen von einer entsprechend ausgebildeten Fachkraft benötigt wird.
Die Reisezeit für Dienstgeschäfte vor Ort einschließlich An- und Abreise ist anzurechnen.
Die Abrechnung der Zeitgebühr erfolgt nach Viertelstunden.

#### § 3 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig treten die Verordnung zur Erhebung von Gebühren im Straßenpersonenverkehr mit Oberleitungsbussen und Straßenbahnen vom 20. Oktober 1993 (GVBl.
II S. 684), die durch Verordnung vom 21.
Juni 1994 (GVBl.
II S. 620) geändert worden ist, die Verordnung zur Erhebung von Gebühren für Amtshandlungen im Bereich der Bahnaufsicht vom 21.
Juni 1994 (GVBl.
II S. 616) sowie die Verordnung über die Erhebung von Verwaltungsgebühren für Amtshandlungen auf dem Gebiet des Magnetschwebebahnverkehrs vom 9. Oktober 1998 (GVBl.
II S. 602) außer Kraft.

Potsdam, den 7.
November 2016

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider

* * *

### Anlagen

1

[Anlage - Gebührenverzeichnis](/br2/sixcms/media.php/68/GVBl_II_61_2016-Anlage.pdf "Anlage - Gebührenverzeichnis") 112.1 KB