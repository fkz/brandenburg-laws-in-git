## Verordnung über die Übertragung bauaufsichtlicher Zuständigkeiten im Land Brandenburg (Brandenburgische Bauzuständigkeitsverordnung - BbgBauZV)

Auf Grund des § 86 Absatz 2 und Absatz 4 der Brandenburgischen Bauordnung in der Fassung der Bekanntmachung vom 15.
November 2018 (GVBl.
I Nr. 39) verordnet der Minister für Infrastruktur und Landesplanung:

#### § 1 Übertragung von Zuständigkeiten auf das Bautechnische Prüfamt

(1) Bautechnisches Prüfamt ist das Landesamt für Bauen und Verkehr.

(2) Dem Bautechnischen Prüfamt wird die Zuständigkeit für folgende Aufgaben zur landesweit einheitlichen Wahrnehmung übertragen:

1.  die Erteilung einer vorhabenbezogenen Bauartgenehmigung nach § 16a Absatz 2 Satz 1 Nummer 2 der Brandenburgischen Bauordnung in der jeweils geltenden Fassung,
2.  die Festlegung des Verzichts einer vorhabenbezogenen Bauartgenehmigung nach § 16a Absatz 4 der Brandenburgischen Bauordnung in der jeweils geltenden Fassung,
3.  die Erteilung einer Zustimmung zur Verwendung von Bauprodukten im Einzelfall nach § 20 der Brandenburgischen Bauordnung in der jeweils geltenden Fassung,
4.  die Erklärung des Verzichts einer Zustimmung zur Verwendbarkeit von Bauprodukten im Einzelfall nach § 20 der Brandenburgischen Bauordnung in der jeweils geltenden Fassung,
5.  die Erteilung von Typenprüfungen nach § 66 Absatz 4 der Brandenburgischen Bauordnung in der jeweils geltenden Fassung,
6.  die Beratung der unteren Bauaufsichtsbehörden in Fragen der Bautechnik und der Bauprodukte,
7.  die Anerkennung und Überwachung der Prüfingenieurinnen und Prüfingenieure sowie die Aufsicht über die Prüfingenieurinnen und Prüfingenieure,
8.  die Erteilung von Typengenehmigungen nach § 72a Absatz 1 der Brandenburgischen Bauordnung in der jeweils geltenden Fassung.

#### § 2 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Brandenburgische Bauzuständigkeitsverordnung vom 28.
Juli 2009 (GVBl.
II S. 518), die durch Verordnung vom 29. November 2010 (GVBl. II Nr. 82) geändert worden ist, außer Kraft.

Potsdam, den 31.
Januar 2020

Der Minister für Infrastruktur und Landesplanung

Guido Beermann