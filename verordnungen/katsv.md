## Verordnung über die Einheiten und Einrichtungen des Katastrophenschutzes (Katastrophenschutzverordnung - KatSV)

Auf Grund des § 49 Absatz 2 Nummer 3 des Brandenburgischen Brand- und Katastrophenschutzgesetzes vom 24.
Mai 2004 (GVBl.
I S. 197) verordnet der Minister des Innern:

### § 1  
Regelungsbereich

Diese Verordnung regelt in Ausgestaltung der sich aus dem Gesetz ergebenden Aufgaben zur landesweiten Vereinheitlichung die Organisation, die Mindeststärke von Personal, Technik und Ausrüstung sowie die Ausbildung und den Einsatz der Einheiten und Einrichtungen des Katastrophenschutzes.
Die Regelungen gelten, soweit hierfür fachlicher Bedarf besteht.

### § 2  
Organisation

(1) Im Katastrophenschutz des Landes Brandenburg sind folgende Fachdienste vorgesehen, die von den unteren Katastrophenschutzbehörden auf der Grundlage ihrer Gefahren- und Risikoanalyse durch Einheiten und Einrichtungen im Sinne von Absatz 2 untersetzt werden:

1.  Führung,
2.  Brandschutz,
3.  Sanitätsdienst,
4.  Betreuungsdienst,
5.  Gefahrstoffschutz,
6.  Bergung/Instandsetzung einschließlich Wassergefahren und
7.  Versorgung.

(2) Die Landkreise und kreisfreien Städte als untere Katastrophenschutzbehörden stellen gemäß § 37 Absatz 1 Nummer 2 des Brandenburgischen Brand- und Katastrophenschutzgesetzes und gemäß Absatz 1 folgende Einheiten und Einrichtungen des Katastrophenschutzes auf und unterhalten diese:

1.  Katastrophenschutzleitungen (KatSL),
2.  Führungsstäbe (FüSt),
3.  Schnelleinsatzgruppen-Führungsunterstützung (SEG-Fü),
4.  Brandschutzeinheiten (BSE),
5.  Schnelleinsatzeinheiten-Sanität (SEE-San),
6.  Schnelleinsatzgruppen-Betreuung (SEG-Bt),
7.  Teams der psychosozialen Notfallversorgung (PSNV),
8.  Schnelleinsatzgruppen-Verpflegung (SEG-V),
9.  Personenauskunftsstellen (PASt),
10.  Gefahrstoffeinheiten (GSE),
11.  Schnelleinsatzgruppen-Wassergefahren (SEG-W),
12.  Schnelleinsatzeinheiten – Versorgung Energie (SEE-VE) und
13.  Katastrophenschutzlager.

Darüber hinaus können die unteren Katastrophenschutzbehörden auf der Grundlage ihrer Gefahren- und Risikoanalyse weitere Katastrophenschutzeinheiten und -einrichtungen aufstellen und betreiben.
Die Aufstellung ist der obersten Katastrophenschutzbehörde mitzuteilen.

(3) Einheiten werden als strukturierte taktische Gliederungen am Schadensort tätig.
Einrichtungen können in ihrer Ausgestaltung unterschiedlich ausgeprägt sein und werden vom Schadensort abgesetzt betrieben.
In den Einheiten und Einrichtungen des Katastrophenschutzes werden Personen und Sachmittel zur Abwehr und Beseitigung der Folgen von Großschadensereignissen und Katastrophen zusammengefasst.

(4) Die unteren Katastrophenschutzbehörden haben die Einheiten des Katastrophenschutzes in ihrem Zuständigkeitsbereich so aufzustellen, dass die Aufgabenerfüllung im örtlichen Brandschutz und in der örtlichen Hilfeleistung auch bei Katastrophenschutzeinsätzen oder Einsätzen zur Bewältigung von Großschadensereignissen gewährleistet bleibt.

(5) Die Landkreise und kreisfreien Städte können auf der Grundlage von Regelungen über kommunale Zusammenarbeit die Aufgaben der unteren Katastrophenschutzbehörden gemeinsam wahrnehmen.
Insbesondere können sie im Rahmen einer Zusammenarbeit überregionale Einsatzunterstützungskapazitäten bilden und zur Aufgabenerfüllung einsetzen.

### § 3  
Aufgabenerfüllung und Mitwirkung

(1) Zur Erfüllung ihrer Aufgaben gemäß § 2 Absatz 1 Nummer 3 des Brandenburgischen Brand- und Katastrophenschutzgesetzes setzen die unteren Katastrophenschutzbehörden neben den öffentlichen Feuerwehren die in § 18 Absatz 1 des Brandenburgischen Brand- und Katastrophenschutzgesetzes genannten mitwirkenden Hilfsorganisationen ein.
Die Aufgabenträger können die Einheiten auch selbst betreiben (Regieeinheiten).

(2) Die unteren Katastrophenschutzbehörden können insbesondere in den Fachdiensten Versorgung und Bergung/Instandsetzung mit der Bundesanstalt Technisches Hilfswerk gemäß § 18 Absatz 1 Satz 2 des Brandenburgischen Brand- und Katastrophenschutzgesetzes zusammenwirken.

### § 4  
Mindeststärke von Personal, Technik und Ausrüstung

(1) Die Katastrophenschutzeinheiten im Sinne von § 2 Absatz 2 Satz 1 sind auf der Grundlage der in der Anlage festgelegten personellen Mindeststärke aufzustellen sowie mit der gemäß der Anlage vorgesehenen Technik und Ausrüstung auszustatten.

(2) Der Bund stellt für Zwecke des Zivilschutzes gemäß § 13 des Zivilschutz- und Katastrophenhilfegesetzes vom 25.
März 1997 (BGBl.
I S. 726), das zuletzt durch Artikel 2 Nummer 1 des Gesetzes vom 29.
Juli 2009 (BGBl.
I S. 2350) geändert worden ist, in der jeweils geltenden Fassung ergänzende Zivilschutzausstattung zur Verfügung.
Diese ist in die Katastrophenschutzeinheiten im Sinne des § 2 Absatz 2 zu integrieren.
Die ergänzende Zivilschutzausstattung kann auch im Rahmen des örtlichen Brandschutzes und der örtlichen Hilfeleistung verwendet werden.

### § 5  
Ausbildung

(1) Die Ausbildung in den Katastrophenschutzeinheiten und -einrichtungen ist auf Anordnung der Aufgabenträger im Sinne des § 2 Absatz 1 des Brandenburgischen Brand- und Katastrophenschutzgesetzes durchzuführen.

(2) Die ergänzende Zivilschutzaus- und -fortbildung des Bundes gemäß § 13 Absatz 4 und § 14 des Zivilschutz- und Katastrophenhilfegesetzes ist in die Ausbildung zu integrieren.

(3) Einheiten und Einrichtungen des Katastrophenschutzes haben folgende Übungen durchzuführen:

1.  Planübungen zur Schulung der Lagebeurteilung und Entscheidungsfindung anhand von Katastrophenschutzplänen und weiteren Einsatzunterlagen,
2.  Alarmierungsübungen zur Überprüfung der Alarmierungspläne und Alarmierungsbereitschaft,
3.  Marschübungen zur Erprobung der geordneten Verlegung von Einheiten, die größer als ein Zug sind oder mehr als vier Einsatzfahrzeuge umfassen,
4.  Stabsrahmenübungen zur Schulung und Überprüfung des Zusammenwirkens innerhalb der Katastrophenschutzleitung sowie des Katastrophenschutzstabes anhand eines angenommenen Schadensereignisses,
5.  Vollübungen zur Erprobung der Katastrophenschutzpläne, zur Überprüfung der Leistungsfähigkeit der Katastrophenschutzeinheiten und -einrichtungen sowie ihres Zusammenwirkens untereinander und mit weiteren zur Mitwirkung verpflichteten Dritten.

(4) Die Katastrophenschutzbehörden sollen die Übungen nach Absatz 3 Nummer 1 bis 4 mindestens im Abstand von zwei Jahren durchführen.
Übungen nach Absatz 3 Nummer 5 sollen von den unteren Katastrophenschutzbehörden mindestens im Abstand von fünf Jahren durchgeführt werden.

(5) Auf die Durchführung von Übungen gemäß Absatz 3 Nummer 1 bis 4 kann verzichtet werden, wenn Realeinsätze der jeweiligen Einheit oder Einrichtung des Katastrophenschutzes innerhalb der letzten zwei Jahren stattgefunden haben.
Auf die Durchführung von Übungen gemäß Absatz 3 Nummer 5 kann verzichtet werden, wenn Realeinsätze der jeweiligen Einheit oder Einrichtung des Katastrophenschutzes innerhalb der letzten fünf Jahren stattgefunden haben.

### § 6  
Einsatz

(1) Die Katastrophenschutzeinheiten und -einrichtungen führen die notwendigen Einsatzmaßnahmen gemäß § 43 Absatz 1 des Brandenburgischen Brand- und Katastrophenschutzgesetzes auf Anordnung der zuständigen Katastrophenschutzbehörde durch.

(2) Das Koordinierungszentrum Krisenmanagement der Landesregierung kann auf Ersuchen der örtlich zuständigen unteren Katastrophenschutzbehörde den unterstützenden Einsatz von personellen und sächlichen Ressourcen vermitteln.

### § 7  
Übergangsbestimmung

Vorhandene Katastrophenschutzfahrzeuge und -ausrüstungen, die den technischen Standards nicht entsprechen, jedoch über einen vergleichbaren Einsatzwert verfügen, können bis zur Ersatzbeschaffung angerechnet und weiter verwendet werden.

### § 8  
Ausführungsvorschriften

Die oberste Katastrophenschutzbehörde erlässt unter Beteiligung der kommunalen Spitzenverbände die zur Ausführung dieser Verordnung erforderlichen Vorschriften.

### § 9  
Evaluierung

Die Ziele dieser Verordnung werden grundsätzlich in einem fortlaufenden Prozess, spätestens aber im Jahr 2024 evaluiert.

### § 10  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Oktober 2012 in Kraft und am 31.
Dezember 2026 außer Kraft.

Potsdam, den 17.
Oktober 2012

Der Minister des Innern

Dr.
Dietmar Woidke

* * *

### Anlagen

1

[Anlage (zu § 4 Absatz 1) Übersicht zur Mindestausstattung von Fachdiensten und Einheiten der unteren Katastrophenschutzbehörden](/br2/sixcms/media.php/68/KatSV-Anlage.pdf "Anlage (zu § 4 Absatz 1) Übersicht zur Mindestausstattung von Fachdiensten und Einheiten der unteren Katastrophenschutzbehörden") 612.6 KB