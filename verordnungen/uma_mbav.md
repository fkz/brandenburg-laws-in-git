## Verordnung über die Gewährung eines Verwaltungskostenausgleichs für die bei den örtlichen Trägern der öffentlichen Jugendhilfe anfallenden Mehrbelastungen für die Unterbringung, Versorgung und Betreuung ausländischer unbegleiteter Kinder und Jugendlicher sowie junger volljähriger Ausländerinnen und Ausländer des Landes Brandenburg (UmA-Mehrbelastungsausgleichverordnung - UmA-MBAV)

Auf Grund der §§ 24i und 25 Absatz 4 des Ersten Gesetzes zur Ausführung des Achten Buches Sozialgesetzbuch – Kinder- und Jugendhilfe in der Fassung der Bekanntmachung vom 26. Juni 1997 (GVBl. I S. 87), von denen § 24i durch das Gesetz vom 17.
Dezember 2015 (GVBl.
I Nr. 41) und § 25 Absatz 4 durch Artikel 1 des Gesetzes vom 5.
Dezember 2013 (GVBl.
I Nr.
43) eingefügt worden sind, verordnet die Landesregierung im Benehmen mit dem Ausschuss für Bildung, Jugend und Sport des Landtags:

#### § 1 Anwendungsbereich

Diese Verordnung regelt den Ausgleich der Mehrbelastungen für die Verwaltungskosten, gemäß Artikel 97 Absatz 3 Satz 2 und 3 der Verfassung des Landes Brandenburg, die den Landkreisen und kreisfreien Städten als örtliche Träger der öffentlichen Jugendhilfe infolge der Aufgaben aus dem Gesetz zur Verbesserung der Unterbringung, Versorgung und Betreuung ausländischer Kinder und Jugendlicher vom 28.
Oktober 2015 (BGBl.
I S.
1802) und dem Zweiten Gesetz zur Änderung des Ersten Gesetzes zur Ausführung des Achten Buches Sozialgesetzbuch – Kinder- und Jugendhilfe vom 17. Dezember 2015 (GVBl.
I Nr.
41) entstehen, soweit nicht besondere Regelungen gemäß § 24d des Ersten Gesetzes zur Ausführung des Achten Buches Sozialgesetzbuch – Kinder- und Jugendhilfe entgegenstehen und durch das Land auszugleichen sind.

#### § 2 Grundlagen des Ausgleichs der Mehrbelastungen

Der Ausgleichsbetrag jedes Landkreises und der kreisfreien Städte wird auf der Grundlage des Nachweises der tatsächlichen Einzelfälle der vorläufigen Inobhutnahmen, der Inobhutnahmen, der Anschlusshilfen gemäß § 27 des Achten Buches Sozialgesetzbuch aller unbegleiteten minderjährigen Ausländerinnen und Ausländer sowie der Anschlusshilfen für junge volljährige Ausländerinnen und Ausländer nach § 41 des Achten Buches Sozialgesetzbuch berechnet.
Jugendhilfeleistungen gemäß des Achten Buches Sozialgesetzbuch für junge Menschen, die in Gemeinschaftsunterkünften bei Erziehungsberechtigten untergebracht sind, bleiben davon unberührt.

#### § 3 Mehrbelastungsausgleichsverfahren

(1) Die Landkreise und kreisfreien Städte erhalten für den Zeitraum vom 1.
November 2015 bis 31. Dezember 2020 als Ausgleich für ihre Mehrbelastungen gemäß § 1 zweckgebundene Zuwendungen des Landes zur Förderung von Verwaltungskosten im Zusammenhang mit der Inobhutnahme, Unterbringung und Betreuung von unbegleiteten minderjährigen Ausländerinnen und Ausländern sowie jungen volljährigen Ausländerinnen und Ausländern.
Die Zuwendungen werden in Form einer Festbetragsfinanzierung gewährt.
Der Ausgleichsbetrag ergibt sich aus:

1.  Einem Grundbetrag von 6 000 Euro je Monat für die Planung und Koordinierung der Aufgaben für unbegleitete minderjährige sowie junge volljährige Ausländerinnen und Ausländer; dies beruht auf einem Zwölftel einer Eingruppierung nach Entgeltgruppe 11 des Tarifvertrages für den öffentlichen Dienst in Höhe von 5 628 Euro plus einem Gemeinkostenanteil.
2.  Einem Mehrbetrag in Höhe von 200 Euro je Einzelfall nach § 2 und Monat zur Umsetzung der beim örtlichen Träger der öffentlichen Jugendhilfe anfallenden Aufgaben, der sich aus einem Schlüssel von 30 Einzelfällen pro Sozialarbeiterstelle der Entgeltgruppe S 14 des Tarifvertrages für den öffentlichen Dienst Sozial- und Erziehungsdienst in Höhe von 177 Euro und einem Anteil für Sachkosten errechnet.

(2) Zum Ausgleich für die Mehrbelastungen gemäß Absatz 1 erhalten die Landkreise und kreisfreien Städte auf Antrag von der nach § 4 zuständigen Behörde halbjährlich Zuwendungen auf der Grundlage der §§ 23 und 44 der Landeshaushaltsordnung.
Die Landkreise und kreisfreien Städte erhalten für den Zeitraum 1.
Januar bis 30.
Juni sowie für den Zeitraum 1.
Juli bis 31.
Dezember des laufenden Jahres aufgrund der gemeldeten tatsächlichen Fallzahlen der in § 2 genannten Einzelfälle einen Ausgleichsbetrag.
Die Höhe der jeweiligen Zuwendung berechnet sich aus dem in Absatz 4 beschriebenen Nachweis.
Das Verfahren der zweckgebundenen Zuwendungen sowie die Verrechnung mittels Zuwendungsbescheid an die Landkreise und kreisfreien Städte endet zum 31. Dezember 2020.

(3) Ab dem 1.
Januar 2021 erhalten die Landkreise und kreisfreien Städte vom Land für jeden gemeldeten Einzelfall gemäß § 2 einen pauschalen Erstattungsbetrag, der sowohl Personal- als auch Sachkosten umfasst.
Der pauschale Erstattungsbetrag beträgt monatlich 279 Euro pro Person und wird für die gemeldeten Einzelfälle gemäß § 2 gezahlt.
Der pauschale Erstattungsbetrag erhöht sich im Zeitpunkt und im Rahmen der tariflichen Anpassung.
Das Land erstattet den Landkreisen und kreisfreien Städten ihren Mehraufwand als Pauschalbetrag auf der Grundlage der tatsächlichen Anzahl der genannten gemeldeten Einzelfälle gemäß § 2 für den Zeitraum 1.
Januar bis 30.
Juni und den Zeitraum 1.
Juli bis 31.
Dezember eines Jahres.
Die Auszahlung der Erstattungsbeträge an die Landkreise und kreisfreien Städte erfolgt bis spätestens zum 31.
Juli des laufenden Jahres und 31. Januar des darauffolgenden Jahres.

(4) Der Nachweis der in § 2 genannten tatsächlichen Einzelfälle ist vom Landkreis oder der kreisfreien Stadt gegenüber der nach § 4 zuständigen Behörde rückwirkend für den vorangegangenen in Absatz 2 oder Absatz 3 definierten Zeitraum zu erbringen.
Alle in § 2 genannten Einzelfälle, die sich mindestens einen Tag des jeweiligen Monats in der Zuständigkeit des Jugendamtes befanden, sind für den betreffenden Zeitraum monatlich aufzulisten.
Der Nachweis ist bis spätestens zum 15.
Juli für die Monate Januar bis Juni des laufenden Jahres und 15.
Januar für das zurückliegende Halbjahr Juli bis Dezember zu erbringen.
Die nach § 4 zuständige Behörde kann Verfahren und Muster für die Meldung nach den Absätzen 2 und 3 für verbindlich erklären sowie die Angaben des Landkreises oder der kreisfreien Stadt zur Begründung der Ausgleichsbeträge überprüfen.

(5) Nachgewiesene höhere Mehrbelastungen können von der nach § 4 zuständigen Behörde auf Antrag der Landkreise und kreisfreien Städte anerkannt und ausgeglichen werden, sofern sie erforderlich waren.

(6) Ansprüche zum Ausgleich höherer Mehrbelastungen für den nach Absatz 1 geregelten Zeitraum sind innerhalb eines Jahres nach Inkrafttreten dieser Verordnung geltend zu machen.
Zum Ausgleich höherer Mehrbelastungen nach Absatz 3 muss ein Antrag bis spätestens 31.
Dezember des auf den Zeitraum folgenden Jahres gestellt werden.

#### § 4 Zuständigkeit

Zuständige Behörde ist das Ministerium für Bildung, Jugend und Sport.

#### § 5 Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2021 in Kraft.

Potsdam, den 1.
März 2021

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Die Ministerin für Bildung, Jugend und Sport

Britta Ernst