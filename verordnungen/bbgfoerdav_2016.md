## Verordnung über die Feldes- und Förderabgabe im Land Brandenburg (Brandenburgische Förderabgabeverordnung - BbgFördAV)

Auf Grund des § 32 Absatz 1 und 2 des Bundesberggesetzes vom 13. August 1980 (BGBl. I S. 1310) in Verbindung mit § 1 der Verordnung zur Übertragung von Ermächtigungen nach dem Bundesberggesetz vom 25.
Juli 1991 (GVBl.
II S. 357) verordnet der Minister für Wirtschaft und Energie:

**Inhaltsübersicht**

### Abschnitt 1  
Erhebung und Bezahlung von Abgaben sowie Marktwertfestsetzung

[§ 1 Entstehung des Feldesabgabeanspruches; Feldesabgabeerklärung](#1)

[§ 2 Entstehung des Förderabgabeanspruches; Förderabgabeerklärung](#2)

[§ 3 Form, Inhalt und Berichtigung der Erklärungen](#3)

[§ 4 Abgabefestsetzung](#4)

[§ 5 Fälligkeit der festgesetzten Abgabe](#5)

[§ 6 Prüfung](#6)

[§ 7 Anwendung der Abgabenordnung](#7)

[§ 8 Feststellung des Marktwertes; Ermittlung des Bemessungsmaßstabes](#8)

[§ 9 Bodenschatzziffern](#9)

### Abschnitt 2  
Feldesabgabe

[§ 10 Befreiung von der Feldesabgabe](#10)

[§ 11 Abweichende Feldesabgabe](#11)

### Abschnitt 3  
Förderabgabe

[§ 12 Befreiung von der Förderabgabe für Erdwärme](#12)

[§ 13 Abgabesatz und Marktwert für Erdöl](#13)

[§ 14 Feldesbehandlungskosten bei der Förderung von Erdöl](#14)

[§ 15 Abgabesatz und Bemessungsmaßstab für Erdgas und Erdölgas (Naturgas)](#15)

[§ 16 Feldesbehandlungskosten bei der Förderung von Erdgas und Erdölgas (Naturgas)](#16)

[§ 17 Abgabesatz und Marktwert für Steinsalz](#17)

[§ 18 Abgabesatz, Marktwert und Befreiung von der Förderabgabe für Sole](#18)

[§ 19 Abgabesatz und Marktwert für Kiese und Sande im Sinne der Bodenschatzziffer 9.23 und für Quarz- und Spezialsande im Sinne der Bodenschatzziffer 9.26](#19)

[§ 20 Abgabesatz und Marktwert für Natursteine im Sinne der Bodenschatzziffern 9.27, 9.29 und 9.30](#20)

[§ 21 Abgabesatz und Marktwert für tonige Gesteine im Sinne der Bodenschatzziffern 9.18, 9.19, 9.21 und 9.22](#21)

[§ 22 Abgabesatz, Marktwert und Befreiung von der Förderabgabe für Torf einschließlich anfallender Mudde im Sinne der Bodenschatzziffer 5](#22)

### Abschnitt 4  
Ordnungswidrigkeiten, Inkrafttreten, Außerkrafttreten

[§ 23 Ordnungswidrigkeiten](#23)

[§ 24 Inkrafttreten, Außerkrafttreten](#24)

### Abschnitt 1  
Erhebung und Bezahlung von Abgaben sowie Marktwertfestsetzung

#### § 1 Entstehung des Feldesabgabeanspruches; Feldesabgabeerklärung

(1) Der Feldesabgabeanspruch entsteht mit der Wirksamkeit der Erlaubnis zur Aufsuchung von Bodenschätzen zu gewerblichen Zwecken.
Erhebungszeitraum ist das Kalenderjahr.

(2) Die Abgabepflichtigen haben bis zum 31.
Juli eines jeden Jahres für den vorausgegangenen Erhebungszeitraum eine Feldesabgabeerklärung abzugeben und bis zum gleichen Tag die Feldesabgabe zu entrichten.
Das Landesamt für Bergbau, Geologie und Rohstoffe kann die Frist zur Abgabe der Feldesabgabeerklärung aus wichtigem Grund verlängern.

#### § 2 Entstehung des Förderabgabeanspruches; Förderabgabeerklärung

(1) Der Förderabgabeanspruch entsteht mit der Gewinnung des Bodenschatzes.
Erhebungszeitraum ist das Kalenderjahr.

(2) Die Abgabepflichtigen haben bis zum 31.
Juli eines jeden Jahres für den vorausgegangenen Erhebungszeitraum eine Förderabgabeerklärung abzugeben und bis zum gleichen Tag die Förderabgabe zu entrichten.

(3) Das Landesamt für Bergbau, Geologie und Rohstoffe kann die Frist zur Abgabe der Förderabgabeerklärung aus wichtigem Grund verlängern.

#### § 3 Form, Inhalt und Berichtigung der Erklärungen

(1) Die Feldes- und Förderabgabeerklärungen sind nach amtlich vorgeschriebenem Vordruckmuster beim Landesamt für Bergbau, Geologie und Rohstoffe einzureichen.
Die Erklärungen können auch auf geeigneten, in Form und Inhalt dem amtlichen Vordruckmuster entsprechenden Datenträgern erfolgen.
Die Abgabepflichtigen haben die Höhe der Abgabe in den Erklärungen selbst zu berechnen.

(2) Die Abgabepflichtigen haben schriftlich zu versichern, dass die Angaben in den Erklärungen wahrheitsgemäß sind.

(3) Erkennen die Abgabepflichtigen, dass eine von ihnen abgegebene Erklärung unrichtig oder unvollständig ist und dass es dadurch zu einer zu geringen Zahlung von Feldes- oder Förderabgaben kommen kann oder bereits gekommen ist, so sind sie verpflichtet, dies dem Landesamt für Bergbau, Geologie und Rohstoffe unverzüglich anzuzeigen und richtig zu stellen.
Der nachzuentrichtende Betrag ist innerhalb von zwei Wochen nach Anzeige zu zahlen.

#### § 4 Abgabefestsetzung

(1) Die für den Erhebungszeitraum zu entrichtende Feldes- oder Förderabgabe wird durch Abgabebescheid des Landesamtes für Bergbau, Geologie und Rohstoffe festgesetzt.

(2) Geben die Abgabepflichtigen die Feldes- oder Förderabgabeerklärung nicht rechtzeitig ab, hat das Landesamt für Bergbau, Geologie und Rohstoffe nach vorheriger Fristsetzung die Abgabe zu schätzen, wenn ihm die Berechnungsgrundlagen nicht bekannt sind.
Dabei sind alle Umstände zu berücksichtigen, die für die Schätzung von Bedeutung sind.
Die Sätze 1 und 2 sind entsprechend anzuwenden, wenn bei einer Prüfung die Berechnungsgrundlagen nicht ermittelt werden können.

(3) Die Abgabefestsetzung kann, solange die Abgabe für den Erhebungszeitraum nicht abschließend geprüft ist, unter dem Vorbehalt der Nachprüfung erfolgen, ohne dass dies einer Begründung bedarf.
Der Vorbehalt entfällt spätestens fünf Jahre nach Ablauf des Kalenderjahres, in dem der Abgabebescheid wirksam geworden ist.

#### § 5 Fälligkeit der festgesetzten Abgabe

Soweit die festgesetzte Feldes- oder Förderabgabe die auf sie bereits entrichteten Beträge übersteigt, ist sie einen Monat nach Bekanntgabe des Abgabebescheides fällig.
Ein überzahlter Betrag wird den Abgabepflichtigen erstattet.

#### § 6 Prüfung

(1) Das Landesamt für Bergbau, Geologie und Rohstoffe und seine Beauftragten sind berechtigt, die tatsächlichen und rechtlichen Verhältnisse, die für die Berechnung der Abgaben maßgebend sind, zu prüfen.
Die Prüfung soll den Abgabepflichtigen spätestens einen Monat vor Beginn angekündigt werden.

(2) Die Abgabepflichtigen haben bei der Feststellung der Sachverhalte, die für die Berechnung der Abgaben von Bedeutung sein können, mitzuwirken.
Sie haben insbesondere Auskünfte zu erteilen, Aufzeichnungen, Bücher, Geschäftspapiere und andere Urkunden zur Einsicht und Prüfung vorzulegen und die zum Verständnis der Aufzeichnungen erforderlichen Erläuterungen zu geben.
Sie können die Vorlage bei der prüfenden Behörde abwenden, wenn sie der Prüfung während der üblichen Geschäfts- und Arbeitszeit in ihren Geschäftsräumen zustimmen.

(3) Das Ergebnis der Prüfung ist den Abgabepflichtigen schriftlich mitzuteilen.

#### § 7 Anwendung der Abgabenordnung

Bei der Erhebung und Zahlung der Feldes- und Förderabgabe sind, soweit im Verwaltungsverfahrensgesetz in der Fassung der Bekanntmachung vom 23. Januar 2003 (BGBl. I S. 102), das zuletzt durch Artikel 3 des Gesetzes vom 25.
Juli 2013 (BGBl. I S. 2749) geändert worden ist, keine anderweitige Regelung getroffen worden ist, von der Abgabenordnung in der Fassung der Bekanntmachung vom 1.
Oktober 2002 (BGBl. I S. 3866; 2003 I S. 61), die zuletzt durch Artikel 7 des Gesetzes vom 2.
November 2015 (BGBl. I S. 1834) geändert worden ist, ergänzend entsprechend anzuwenden:

1.  von den Vorschriften über die Haftungsbeschränkung für Amtsträger § 32,
2.  von den Vorschriften über den Steuerpflichtigen die §§ 33 bis 36,
3.  von den Vorschriften über das Steuerschuldverhältnis die §§ 40 bis 42, 44 und 45,
4.  von den Vorschriften über die Haftung die §§ 69 bis 71, 73 bis 75 und 77,
5.  von den Vorschriften über die Besteuerungsgrundsätze und Beweismittel die §§ 90, 92, 93 Absatz 1 bis 6, § 96 Absatz 1 bis 7 Satz 2, die §§ 97 bis 99 und 101 bis 107,
6.  von den Vorschriften über die Führung von Büchern und Aufzeichnungen die §§ 145 bis 147,
7.  von den Vorschriften über die Steuererklärungen § 152 Absatz 1 bis 3,
8.  von den Vorschriften über die Steuerfestsetzung § 156 Absatz 2, die §§ 163, 169 mit der Maßgabe, dass die Festsetzungsfrist fünf Jahre beträgt, und die §§ 170 und 171,
9.  von den Vorschriften über die Zahlung und Aufrechnung § 224 Absatz 2 Nummer 2 sowie die §§ 225 und 226,
10.  von den Vorschriften über die Zahlungsverjährung die §§ 228 bis 232,
11.  von den Vorschriften über die Verzinsung die §§ 233 und 233a mit der Maßgabe, dass der Zinslauf abweichend von § 233a Absatz 2 zwei Jahre nach Ablauf des Erhebungszeitraums beginnt und fünf Jahre nach Ablauf des Erhebungszeitraums endet, sowie die §§ 235 und 237 bis 239,
12.  von den Vorschriften über die Säumniszuschläge § 240.

#### § 8 Feststellung des Marktwertes; Ermittlung des Bemessungsmaßstabes

(1) Das für Wirtschaft zuständige Ministerium stellt den Marktwert für Bodenschätze im Sinne des § 31 Absatz 2 des Bundesberggesetzes fest und teilt ihn den Abgabepflichtigen ohne Begründung mit.

(2) Die Abgabepflichtigen haben dem für Wirtschaft zuständigen Ministerium bis zum 31. März eines jeden Jahres die für die Feststellung des Marktwertes erforderlichen Angaben zu machen, insbesondere die für den vorausgegangenen Erhebungszeitraum marktwertbildenden Erlöse, Mengen und Preise mitzuteilen.
§ 3 Absatz 1 Satz 1 und 2, Absatz 2, § 6 Absatz 1 Satz 1 und Absatz 2 und § 7 Nummer 6 sind entsprechend anzuwenden.
Das für Wirtschaft zuständige Ministerium kann die Abgabepflichtigen von der Mitteilungspflicht befreien, wenn die Feststellung des Marktwertes auf andere Weise sichergestellt ist.

(3) Nicht abgabepflichtige natürliche oder juristische Personen, die Naturgas (§ 16) oder Industriesalz aus Steinsalz (§ 17) oder Sole (§ 18) herstellen, sind verpflichtet, dem für Wirtschaft zuständigen Ministerium Auskünfte zu erteilen, soweit dies zur Feststellung des Marktwertes erforderlich ist.

(4) Preis im Sinne dieser Verordnung ist der Quotient aus Erlös und Menge.
Zum Erlös gehören nicht Transportkosten, Umsatzsteuer, Skonti und Rabatte.

(5) Die Ermittlung eines Bemessungsmaßstabes gemäß § 32 Absatz 2 Satz 1 Nummer 3 des Bundesberggesetzes erfolgt durch das Landesamt für Bergbau, Geologie und Rohstoffe.
Die Absätze 1 bis 4 sind entsprechend anzuwenden.

#### § 9 Bodenschatzziffern

Bodenschatzziffern im Sinne dieser Verordnung sind die in der Anlage zur Verordnung über die Verleihung von Bergwerkseigentum vom 15.
August 1990 (GBl.
I Nr. 53 S. 1071) in Verbindung mit Anlage I Kapitel V Sachgebiet D Abschnitt III Nummer 1 Buchstabe a des Einigungsvertrages vom 31. August 1990 (BGBl.
1990 II S. 885, 1004), der durch das Gesetz vom 15.
April 1996 (BGBl. I S. 602) geändert worden ist, aufgeführten Ordnungsnummern.

### Abschnitt 2  
Feldesabgabe

#### § 10 Befreiung von der Feldesabgabe

(1) Die Abgabepflichtigen werden für den Zeitraum von der Entrichtung der Feldesabgabe befreit, für den das Landesamt für Bergbau, Geologie und Rohstoffe einer Unterbrechung der Aufsuchungsarbeiten zugestimmt hat.

(2) Für die Zeit vom 1.
Januar 2016 bis zum 31.
Dezember 2025 werden die Abgabepflichtigen von der Feldesabgabe für Erdwärme befreit.

(3) Für die Zeit vom 1.
Januar 2016 bis zum 31.
Dezember 2025 werden die Abgabepflichtigen von der Feldesabgabe für Sole befreit, soweit die Sole natürlich vorkommt und für balneologische Zwecke verwendet werden soll oder die Sole als Trägermedium für Erdwärme genutzt werden soll.

#### § 11 Abweichende Feldesabgabe

Die Feldesabgabe beträgt vom 1.
Januar 2016 bis zum 31.
Dezember 2025 für Erlaubnisse für Erdöl und Erdgas im ersten Jahr nach der Erteilung 20 Euro je angefangenen Quadratkilometer und erhöht sich für jedes folgende Jahr um weitere 20 Euro bis zum Höchstbetrag von 80 Euro je angefangenen Quadratkilometer.

### Abschnitt 3  
Förderabgabe

#### § 12 Befreiung von der Förderabgabe für Erdwärme

Für die Zeit vom 1.
Januar 2016 bis zum 31.
Dezember 2025 werden die Abgabepflichtigen von der Förderabgabe für Erdwärme befreit.

#### § 13 Abgabesatz und Marktwert für Erdöl

(1) Die Förderabgabe beträgt ab 1.
Januar 2016 bis zum 31.
Dezember 2025 10 Prozent des Marktwertes.

(2) Der Marktwert für Erdöl ist das gewogene Mittel der Preise in Euro je Tonne, die für frei gehandeltes, im Geltungsbereich des Bundesberggesetzes gewonnenes raffineriefähiges Erdöl einer Gruppe im Erhebungszeitraum erzielt worden sind.

(3) Das Erdöl wird folgenden Gruppen zugeordnet:

Gruppe

Dichte in g/cm3 bei 15° C

1

0,839 und kleiner

2

0,840 bis 0,859

3

0,860 bis 0,869

4

0,870 bis 0,879

5

0,880 bis 0,899

6

0,900 und größer

7

unabhängig von der Dichte bei einem Schwefelanteil von 2 Prozent oder mehr.

#### § 14 Feldesbehandlungskosten bei der Förderung von Erdöl

(1) Vom 1.
Januar 2016 bis zum 31.
Dezember 2025 verringert sich die Förderabgabe je Lagerstätte um die im Erhebungszeitraum entstandenen Feldesbehandlungskosten in Höhe des sich nach § 13 ergebenden Prozentsatzes, soweit diese nicht bei der Erhebung der Förderabgabe für einen anderen Bodenschatz berücksichtigt werden.
Eine Berücksichtigung erfolgt nur bis zur Höhe der nach § 13 ermittelten Förderabgabe des in der Lagerstätte geförderten Erdöls.

(2) Feldesbehandlungskosten sind die für eine Erdöl- oder Erdgaslagerstätte bei der Förderung des Erdöls anfallenden

1.  Kosten für den Transport vom Abgangsflansch am Bohrloch bis zur Aufbereitung einschließlich des anteiligen Energieeinsatzes für die Förderpumpen für den horizontalen Transport,
2.  Kosten für die Aufbereitung zur Herstellung eines raffineriefähigen Rohöls,
3.  Kosten für die transportbedingte Lagerung und den Versand bis einschließlich Übergabestation,
4.  Kosten für die Beseitigung des bei der Aufbereitung anfallenden Wassers bis zur Übergabestelle an einen Vorfluter oder an einen Dritten oder durch Versenkung in einen bereits erschlossenen Schluckhorizont, wenn die Versenkung nicht gleichzeitig anderen Zwecken dient, sowie
5.  zentralen Verwaltungsgemeinkosten in Höhe von 20 Prozent der sich aus den Nummern 1 bis 4 ergebenden Kosten.

#### § 15 Abgabesatz und Bemessungsmaßstab für Erdgas und Erdölgas (Naturgas)

(1) Die Förderabgabe beträgt ab 1.
Januar 2016 bis zum 31.
Dezember 2025 10 Prozent des Bemessungsmaßstabes.

(2) Der Bemessungsmaßstab für die Förderabgabe für Erdgas und Erdölgas (Naturgas) ist das gewogene Mittel der vom Statistischen Bundesamt unter der Warennummer 2711 21 00 veröffentlichten monatlichen Grenzübergangspreise für Erdgasimporte im Erhebungszeitraum, umgerechnet in Euro je Kilowattstunde.
Der Wert nach Satz 1 ist mit sechs Stellen hinter dem Komma zu berechnen.

#### § 16 Feldesbehandlungskosten bei der Förderung von Erdgas und Erdölgas (Naturgas)

(1) Vom 1.
Januar 2016 bis zum 31.
Dezember 2025 verringert sich die Förderabgabe je Lagerstätte um die im Erhebungszeitraum entstandenen Feldesbehandlungskosten in Höhe des sich aus § 15 ergebenden Prozentsatzes, soweit diese Kosten nicht bei der Erhebung der Förderabgabe für einen anderen Bodenschatz berücksichtigt werden.
Eine Berücksichtigung erfolgt nur bis zur Höhe der nach § 15 ermittelten Förderabgabe des in der Lagerstätte geförderten Erdgases und Erdölgases (Naturgases).

(2) Feldesbehandlungskosten sind die für eine Erdöl- oder Erdgaslagerstätte bei der Förderung des Naturgases anfallenden

1.  Kosten für den Transport vom Abgangsflansch am Bohrloch bis zur Aufbereitung einschließlich Kompression,
2.  Kosten für die Aufbereitung zur Herstellung qualitätsgerechter Gase und der aus gewinnungstechnischen Gründen mitgewonnenen Bodenschätze,
3.  Kosten für die Beseitigung des bei der Aufbereitung anfallenden Wassers bis zur Übergabestelle an einen Vorfluter oder an einen Dritten oder durch Versenkung in einen bereits erschlossenen Schluckhorizont, wenn die Versenkung nicht gleichzeitig anderen Zwecken dient, sowie
4.  zentralen Verwaltungsgemeinkosten in Höhe von 20 Prozent der sich aus den Nummern 1 bis 3 ergebenden Kosten.

#### § 17 Abgabesatz und Marktwert für Steinsalz

(1) Die Förderabgabe beträgt ab 1.
Januar 2016 bis zum 31.
Dezember 2025 1 Prozent des Marktwertes.
Die Förderabgabe ermäßigt sich auf 0,5 Prozent, soweit das Steinsalz bei der Errichtung eines Untergrundspeichers gewonnen und nicht wirtschaftlich verwertet werden kann.

(2) Der Marktwert für Steinsalz ist das gewogene Mittel der Preise in Euro pro Tonne, die im Erhebungszeitraum im Geltungsbereich des Bundesberggesetzes für freigehandeltes Industriesalz erzielt worden sind.

#### § 18 Abgabesatz, Marktwert und Befreiung von der Förderabgabe für Sole

(1) Die Förderabgabe beträgt ab 1.
Januar 2016 bis zum 31.
Dezember 2025 1 Prozent des Marktwertes.
Die Förderabgabe ermäßigt sich auf 0,5 Prozent, soweit die Sole bei der Errichtung eines Untergrundspeichers gewonnen und nicht wirtschaftlich verwertet werden kann.

(2) Die Feststellung des Marktwertes für Sole erfolgt auf der Grundlage des Steinsalzgehaltes.
§ 17 Absatz 2 gilt entsprechend.

(3) Für die Zeit vom 1.
Januar 2016 bis zum 31.
Dezember 2025 wird der Abgabepflichtige von der Förderabgabe befreit, soweit die Sole natürlich vorkommt und für balneologische Zwecke verwendet wird oder die Sole als Trägermedium bei der Gewinnung von Erdwärme verwendet und nicht wirtschaftlich verwertet wird.

#### § 19 Abgabesatz und Marktwert für Kiese und Sande im Sinne der Bodenschatzziffer 9.23 und für Quarz- und Spezialsande im Sinne der Bodenschatzziffer 9.26

(1) Die Förderabgabe beträgt ab 1.
Januar 2016 bis zum 31.
Dezember 2025 7 Prozent des Marktwertes.

(2) Der Marktwert beträgt 50 Prozent des Quotienten aus dem Produktionswert und der Produktionsmenge der im Erhebungszeitraum erfolgten Produktion in Euro pro Tonne.

(3) Maßgeblich für die Berücksichtigung des Produktionswertes und der Produktionsmenge ist die Summe der vom Statistischen Bundesamt auf der Grundlage des Gesetzes über die Statistik im Produzierenden Gewerbe in Verbindung mit dem Gesetz über die Statistik für Bundeszwecke in den Ergebnissen der Statistik Produzierendes Gewerbe, Fachserie 4, Reihe 3.1 unter den Meldenummern 0812 11 900 und 0812 12 103 für den Erhebungszeitraum veröffentlichten Jahresangaben.

#### § 20 Abgabesatz und Marktwert für Natursteine im Sinne der Bodenschatzziffern 9.27, 9.29 und 9.30

(1) Die Höhe der Förderabgabe beträgt ab 1.
Januar 2016 bis zum 31.
Dezember 2025 5 Prozent des Marktwertes.

(2) Der Marktwert ist der Quotient aus dem Produktionswert und der Produktionsmenge der im Erhebungszeitraum erfolgten Produktion in Euro pro Tonne.

(3) Maßgeblich für die Berücksichtigung des Produktionswertes und der Produktionsmenge sind die vom Statistischen Bundesamt auf der Grundlage des Gesetzes über die Statistik im Produzierenden Gewerbe in Verbindung mit dem Gesetz über die Statistik für Bundeszwecke in den Ergebnissen der Statistik Produzierendes Gewerbe, Fachserie 4, Reihe 3.1 unter der Meldenummer 0812 12 307 für den Erhebungszeitraum veröffentlichten Jahresangaben.

#### § 21 Abgabesatz und Marktwert für tonige Gesteine im Sinne der Bodenschatzziffern 9.18, 9.19, 9.21 und 9.22

(1) Die Höhe der Förderabgabe beträgt ab 1.
Januar 2016 bis zum 31.
Dezember 2025 10 Prozent des Marktwertes.

(2) Der Marktwert beträgt 13 Prozent des Quotienten aus dem Produktionswert und der Produktionsmenge der im Erhebungszeitraum erfolgten Produktion in Euro pro Kubikmeter.

(3) Maßgeblich für die Berücksichtigung des Produktionswertes und der Produktionsmenge ist die Summe der vom Statistischen Bundesamt auf der Grundlage des Gesetzes über die Statistik im Produzierenden Gewerbe in Verbindung mit dem Gesetz über die Statistik für Bundeszwecke in den Ergebnissen der Statistik Produzierendes Gewerbe, Fachserie 4, Reihe 3.1 unter den Meldenummern 2332 11 103, 2332 11 105 und 2332 11 107 für den Erhebungszeitraum veröffentlichten Jahresangaben.

#### § 22 Abgabesatz, Marktwert und Befreiung von der Förderabgabe für Torf einschließlichanfallender Mudde im Sinne der Bodenschatzziffer 5

(1) Die Förderabgabe beträgt ab 1.
Januar 2016 bis zum 31.
Dezember 2025 5 Prozent des Marktwertes.

(2) Der Marktwert ist der Quotient aus dem Produktionswert und der Produktionsmenge der im Erhebungszeitraum erfolgten Produktion in Euro pro Kubikmeter.

(3) Maßgeblich für die Berücksichtigung des Produktionswertes und der Produktionsmenge ist die Summe der vom Statistischen Bundesamt auf der Grundlage des Gesetzes über die Statistik im Produzierenden Gewerbe in Verbindung mit dem Gesetz über die Statistik für Bundeszwecke in den Ergebnissen der Statistik Produzierendes Gewerbe, Fachserie 4, Reihe 3.1 unter den Meldenummern 0892 10 101 und 0892 10 105 für den Erhebungszeitraum veröffentlichten Jahresangaben.

(4) Für die Zeit vom 1.
Januar 2016 bis zum 31.
Dezember 2025 wird der Abgabepflichtige von der Förderabgabe befreit, soweit der Torf für balneologische Zwecke verwendet wird.

### Abschnitt 4  
Ordnungswidrigkeiten, Inkrafttreten, Außerkrafttreten

#### § 23 Ordnungswidrigkeiten

Ordnungswidrig im Sinne des § 145 Absatz 3 Nummer 1 des Bundesberggesetzes vom 13. August 1980 (BGBl. I S. 1310), das zuletzt durch Artikel 303 der Verordnung vom 31.
August 2015 (BGBl. I S. 1474) geändert worden ist, handelt, wer vorsätzlich oder fahrlässig

1.  entgegen § 1 Absatz 2 Satz 1 die erforderliche Feldesabgabeerklärung nicht, nicht rechtzeitig oder nicht vollständig abgibt,
2.  entgegen § 2 Absatz 2 die erforderliche Förderabgabeerklärung nicht, nicht rechtzeitig oder nicht vollständig abgibt,
3.  entgegen § 3 Absatz 3 die erforderliche Anzeige oder Richtigstellung nicht oder nicht rechtzeitig vornimmt,
4.  entgegen § 6 Absatz 2 Satz 1 und 2 nicht oder nicht hinreichend bei der Feststellung der Sachverhalte mitwirkt oder
5.  entgegen § 7 Nummer 6 seiner Aufzeichnungs- und Aufbewahrungspflicht nicht nachkommt.

#### § 24 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 1.
Januar 2016 in Kraft.
Gleichzeitig tritt die Brandenburgische Förderabgabeverordnung vom 26.
Januar 2006 (GVBl.
II S. 30), die zuletzt durch die Verordnung vom 16. Juni 2010 (GVBl.
II Nr. 30) geändert worden ist, außer Kraft.

Potsdam, den 11.
Dezember 2015

Der Minister für Wirtschaft und Energie

Albrecht Gerber