## Verordnung über die Anerkennung von niedrigschwelligen Betreuungs- und Entlastungsangeboten nach § 45b Absatz 4 des Elften Buches Sozialgesetzbuch (Angebotsanerkennungsverordnung - NBEA-AnerkV)

Auf Grund des § 45b Absatz 4 Satz 1 des Elften Buches Sozialgesetzbuch – Soziale Pflegeversicherung – (Artikel 1 des Gesetzes vom 26.
Mai 1994, BGBl. I S. 1014, 1015), der zuletzt durch Artikel 1 des Gesetzes vom 17.
Dezember 2014 (BGBl. I S. 2222) geändert worden ist, verordnet die Landesregierung:

#### § 1 Zweck der Anerkennung

(1) Pflegebedürftige und Versicherte mit erheblichem Bedarf an allgemeiner Beaufsichtigung und Betreuung, die häuslich gepflegt werden (Anspruchsberechtigte), haben nach § 45b des Elften Buches Sozialgesetzbuch einen Anspruch auf zusätzliche Betreuungs- und Entlastungsleistungen.
Sie erhalten hierfür als Leistung der sozialen oder privaten Pflegeversicherung eine Kostenerstattung in Höhe von bis zu 104 Euro monatlich (Grundbetrag) oder bis zu 208 Euro monatlich (erhöhter Betrag).

(2) Die Kostenerstattung umfasst auch Aufwendungen, die durch die Inanspruchnahme von nach Landesrecht anerkannten niedrigschwelligen Betreuungs- und Entlastungsangeboten entstehen.
Das sind Angebote, in denen Helfende unter fachlicher Anleitung

1.  die Betreuung der Anspruchsberechtigten in Gruppen oder im häuslichen Bereich übernehmen sowie pflegende Angehörige und vergleichbar Nahestehende entlasten und beratend unterstützen oder
2.  eine die vorhandenen Ressourcen und Fähigkeiten stärkende oder stabilisierende Alltagsbegleitung der Anspruchsberechtigten einschließlich einer entlastenden Unterstützung in ihrem Haushalt erbringen sowie bei der Bewältigung von allgemeinen oder pflegebedingten Anforderungen des Alltags und bei der eigenverantwortlichen Organisation der individuell bedingten Hilfeleistungen unterstützen oder pflegende Angehörige und vergleichbar Nahestehende in ihrer Eigenschaft als Pflegende beratend, begleitend und entlastend unterstützen.

Zusätzlich können die Anspruchsberechtigten nach § 45b Absatz 3 des Elften Buches Sozialgesetzbuch bei ihrer Pflegekasse eine Umwidmung von bis zu 40 Prozent des je nach Pflegestufe bestehenden Sachleistungsanspruchs beantragen und anschließend zur Kostenerstattung für die Inanspruchnahme der nach Landesrecht anerkannten niedrigschwelligen Betreuungs- und Entlastungsangebote einsetzen.

(3) Voraussetzung für die Erstattungsfähigkeit ist, dass das niedrigschwellige Betreuungs- und Entlastungsangebot nach § 45c des Elften Buches Sozialgesetzbuch gefördert wird oder förderfähig ist sowie nach Landesrecht anerkannt ist.
Das Anerkennungsverfahren hat das Ziel, die Qualität der Betreuungs- und Entlastungsangebote zu sichern.

(4) Die Anerkennung eines niedrigschwelligen Betreuungs- und Entlastungsangebots begründet keinen Anspruch auf öffentliche Förderung.

#### § 2 Niedrigschwellige Betreuungs- und Entlastungsangebote

(1) Die Verordnung gilt für niedrigschwellige Betreuungs- und Entlastungsangebote nach § 45c Absatz 3 und 3a des Elften Buches Sozialgesetzbuch, die im Land Brandenburg tätig sind.

(2) Anerkennungsfähige niedrigschwellige Betreuungs- und Entlastungsangebote sind insbesondere

1.  Angebote zur stundenweisen Betreuung von Anspruchsberechtigten im häuslichen Bereich,
2.  Angebote zur stundenweisen Unterstützung von Anspruchsberechtigten im häuslichen Bereich bei der Bewältigung von allgemeinen und pflegebedingten Anforderungen des Alltags, zum Beispiel bei der hauswirtschaftlichen Versorgung,
3.  Betreuungsgruppen für Anspruchsberechtigte,
4.  Angebote in Gruppen für Anspruchsberechtigte, zum Beispiel Freizeit-, Kultur- und Sportangebote mit und ohne deren pflegende Angehörige oder vergleichbar Nahestehende,
5.  Tagesbetreuung in Kleingruppen,
6.  Unterstützung bei der Organisation individuell benötigter Hilfeleistungen oder
7.  psychosoziale Begleitung der pflegenden Angehörigen oder anderer nahestehender Pflegepersonen.

#### § 3 Anerkennungsvoraussetzungen

Voraussetzung für die Anerkennung ist, dass

1.  auf der Grundlage eines eingereichten fachlichen Konzeptes eine regelmäßige und verlässliche Betreuung oder Entlastung durch Helfende erfolgt, die für ihre Aufgaben geeignet sind sowie fachlich angeleitet werden,
2.  für die Inanspruchnahme des Angebots eine Vergütung verlangt wird, die unterhalb der Preise für vergleichbare Sachleistungen nach § 36 Absatz 1 des Elften Buches Sozialgesetzbuch liegt,
3.  ein Versicherungsschutz gegen Sach- und Personenschäden, welche die Helfenden im Rahmen ihrer Tätigkeit verursachen oder erleiden können, besteht,
4.  die räumlichen Voraussetzungen vorhanden sind, sofern Angebote in Gruppen stattfinden sollen und
5.  das Angebot konzeptionell darauf ausgerichtet ist, seine Leistungen als Teil einer regionalen Versorgungsstruktur zu erbringen und Bereitschaft für die Kommunikation und Kooperation innerhalb eines abgestimmten und vernetzten Versorgungssystems besteht.

#### § 4 Regelmäßigkeit und Verlässlichkeit des Angebots

Eine Regelmäßigkeit des Betreuungs- und Entlastungsangebots liegt vor, wenn dauerhaft und in bestimmten zeitlichen Abständen, in der Regel wöchentlich, Leistungen angeboten werden.
Für die Verlässlichkeit des Betreuungs- und Entlastungsangebots muss die Vertretung der Helfenden zum Beispiel im Fall von Urlaub oder Erkrankung sichergestellt werden können.

#### § 5 Helfende und deren Eignung

(1) Helfende sind ehrenamtliche Helferinnen und Helfer, denen keine oder eine Aufwandsentschädigung geleistet wird, die im Rahmen des Betreuungs- und Entlastungsangebots die Grenzen des § 3 Nummer 26 des Einkommensteuergesetzes nicht überschreitet.
Helfende können auch sozialversicherungspflichtig beschäftigte Mitarbeiterinnen und Mitarbeiter sein.
In diesem Fall sind sie zu den geltenden gesetzlichen Bedingungen sozialversicherungspflichtig zu beschäftigen.

(2) Die persönliche Eignung besitzen Personen nicht, wenn Tatsachen die Annahme rechtfertigen, dass sie sich eines Verhaltens schuldig gemacht haben, aus dem sich die Untauglichkeit zur Erbringung der Leistungen ergibt.

(3) Die fachliche Eignung erwerben die Helfenden durch vorbereitende Schulungen, die hinsichtlich ihres Inhalts und Umfangs auf das jeweilige Betreuungs- und Entlastungsangebot auszurichten sind.
Sie müssen mindestens 30 Stunden umfassen.
Durch die Schulungen sind insbesondere folgende Inhalte fachlich angemessen zu vermitteln:

1.  Basiswissen über Krankheitsbilder- und Behinderungsarten (Ursachen und Symptome) und ihre psychosozialen Folgen, Behandlungsformen und Pflege der zu betreuenden Menschen sowie Möglichkeiten der Hilfen,
2.  Wahrnehmung des sozialen Umfeldes, der psychosozialen Situation von pflegenden Angehörigen und des bestehenden Hilfe- und Unterstützungsbedarfs,
3.  Kommunikation und Umgang mit pflegebedürftigen Menschen und Menschen mit eingeschränkter Alltagskompetenz, Erwerb von Handlungskompetenzen in Bezug auf das Einfühlen in die Erlebniswelt und im Umgang mit Verhaltensauffälligkeiten wie Aggressionen und Widerstände,
4.  Methoden und Möglichkeiten der Betreuung, Beschäftigung und Begleitung der hilfebedürftigen Menschen zu Hause und in Betreuungsgruppen,
5.  Umgang mit akuten Krisen und Notfallsituationen,
6.  Kenntnis von Beratungsangeboten,
7.  Wahrnehmung der Grenzen der eigenen Rolle als nicht professionelle Unterstützung,
8.  rechtliche Rahmenbedingungen und
9.  Methoden der Begleitung und Unterstützung in der Versorgung von hilfebedürftigen Menschen.

#### § 6 Fachliche Anleitung der Helfenden

(1) Für die fachliche Anleitung ist eine kontinuierliche Begleitung und Unterstützung der Helfenden durch eine geeignete Fachkraft erforderlich.
Diese umfassen mindestens

1.  eine aufsuchende Beratung bei der oder dem Anspruchsberechtigten zur Klärung der Leistungszusagen der Pflegekasse und der im Einzelfall geeigneten Form der Betreuung und Entlastung,
2.  ein regelmäßiges Angebot von Team- und Fallbesprechungen für die Helfenden,
3.  bedarfsgerechte Fortbildung der Helfenden sowie
4.  Beratung bei Veränderung der Betreuungs- und Entlastungsbedarfe sowie bei Krisen.

(2) Entsprechend der Ausrichtung des Angebots kommen als Fachkräfte insbesondere Altenpflegerinnen und Altenpfleger, Gesundheits- und Krankenpflegerinnen sowie Gesundheits- und Krankenpfleger, Gesundheits- und Kinderkrankenpflegerinnen sowie Gesundheits- und Kinderkrankenpfleger, Ergotherapeutinnen und Ergotherapeuten, staatlich anerkannte Heilerziehungspflegerinnen und Heilerziehungspfleger, staatlich anerkannte Heilpädagoginnen und Heilpädagogen, staatlich anerkannte Sozialarbeiterinnen/Sozialpädagoginnen und Sozialarbeiter/Sozialpädagogen, staatlich anerkannte Sozialpädagoginnen und Sozialpädagogen, Diplompädagoginnen und Diplompädagogen, Psychologinnen und Psychologen, Ärztinnen und Ärzte, Fachkräfte für die gerontopsychiatrische Betreuung und Pflege, Hauswirtschafterinnen und Hauswirtschafter sowie Fachkräfte mit vergleichbaren Abschlüssen in Betracht.

#### § 7 Anerkennungsverfahren

(1) Die Anerkennung eines niedrigschwelligen Betreuungs- und Entlastungsangebots setzt einen schriftlichen Antrag des Anbieters bei der zuständigen Behörde voraus.
Dem Antrag ist das Konzept des Betreuungs- und Entlastungsangebots und der vorbereitenden Schulungen nach § 5 Absatz 3 beizufügen.
Das Konzept muss insbesondere über Zielgruppe, Umfang und Methode der angebotenen Leistung sowie die Höhe der geforderten Vergütung Auskunft geben.
Darüber hinaus muss es belegen, dass die Anforderungen des § 3 in Verbindung mit den §§ 4, 5 Absatz 1 und 3 sowie § 6 erfüllt sind und die Qualität des Angebots gesichert ist.

(2) Zur Qualitätssicherung sind die Anbieter der anerkannten Angebote verpflichtet, auf Verlangen der zuständigen Behörde Auskunft über das bereitgestellte Angebot zu geben sowie nachzuweisen, dass die in § 3 in Verbindung mit den §§ 4, 5 Absatz 1 und 3 sowie § 6 beschriebenen Anforderungen auch weiterhin erfüllt werden.
Wird bekannt, dass die Anforderungen nicht oder nicht mehr vorliegen, kann die Anerkennung widerrufen werden.

(3) Die Anerkennung bezieht sich auf die im eingereichten Konzept beschriebenen Angebote und wird befristet für einen Zeitraum von längstens fünf Jahren ausgesprochen.

(4) Für die Anerkennung und den Widerruf gelten die Vorschriften über das Verwaltungsverfahren nach dem Zehnten Buch Sozialgesetzbuch.

(5) Zuständige Behörde ist das Landesamt für Soziales und Versorgung.

(6) Die zuständige Behörde übersendet die Anträge den jeweiligen Landkreisen oder kreisfreien Städten, in denen die Leistungen der Betreuungs- und Entlastungsangebote schwerpunktmäßig erbracht werden sollen, mit der Gelegenheit zur Stellungnahme innerhalb einer Frist von vier Wochen.

(7) Zur Gewährleistung der Unterrichtungs- und Beratungspflicht der Pflegekassen nach § 7 Absatz 3 Satz 6 des Elften Buches Sozialgesetzbuch informiert die zuständige Behörde die Landesverbände der Pflegekassen sowie den Verband der Privaten Krankenversicherung e.
V.
über den Abschluss von Anerkennungs- und Widerrufsverfahren.

#### § 8 Übergangsregelung

(1) Für niedrigschwellige Betreuungsangebote, die auf der Grundlage der Verordnung über die Anerkennung von niedrigschwelligen Betreuungsangeboten nach § 45b des Elften Buches Sozialgesetzbuch – Soziale Pflegeversicherung – vom 13.
November 2002 (GVBl.
II S. 644), die durch die Verordnung vom 16.
Juni 2009 (GVBl.
II S. 330) geändert worden ist, anerkannt wurden, gilt § 7 Absatz 2 und 3 mit der Maßgabe, dass die Anerkennung bis zum 31. Dezember 2020 befristet ist.

(2) Unabhängig von Absatz 1 können anerkannte Angebote bei Änderungen in Art und Umfang des Angebots einen neuen Antrag auf Anerkennung stellen.

#### § 9 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über die Anerkennung von niedrigschwelligen Betreuungsangeboten nach § 45b des Elften Buches Sozialgesetzbuch – Soziale Pflegeversicherung – vom 13.
November 2002 (GVBl.
II S. 644), die durch die Verordnung vom 16.
Juni 2009 (GVBl.
II S. 330) geändert worden ist, außer Kraft.

Potsdam, den 4.
Januar 2016

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Die Ministerin für Arbeit, Soziales,  
Gesundheit, Frauen und Familie

Diana Golze