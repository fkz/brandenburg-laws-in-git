## Verordnung zur Bestimmung der zuständigen öffentlich-rechtlichen Stelle, die die Freischaltung eines besonderen elektronischen Bürger- und Organisationenpostfachs veranlasst

Aufgrund des § 11 Absatz 1 der Elektronischer-Rechtsverkehr-Verordnung vom 24.
November 2017 (BGBl.
I S. 3803), der durch Artikel 6 Nummer 5 des Gesetzes vom 5.
Oktober 2021 (BGBl.
I S.
4607) eingefügt worden ist, und des § 6 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S.
186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28) geändert worden ist, verordnet die Landesregierung:

#### § 1 Öffentlich-rechtliche Stelle für die Freischaltung eines besonderen elektronischen Bürger- und Organisationenpostfachs

Öffentlich-rechtliche Stelle gemäß § 11 Absatz 1 der Elektronischer-Rechtsverkehr-Verordnung, die die Freischaltung eines besonderen elektronischen Bürger- und Organisationenpostfachs veranlasst, ist das für Justiz zuständige Ministerium des Landes Brandenburg.

#### § 2 Inkrafttreten

Diese Verordnung tritt am 1.
Januar 2022 in Kraft.

Potsdam, den 27.
Dezember 2021

Die Landesregierung des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Die Ministerin der Justiz

Susanne Hoffmann