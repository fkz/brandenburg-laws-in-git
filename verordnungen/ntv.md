## Verordnung über die Nebentätigkeiten der Beamtinnen und Beamten im Land Brandenburg (Nebentätigkeitsverordnung - NtV)

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeines

[§ 1 Geltungsbereich und Zuständigkeit](#1)

[§ 2 Anzeigepflicht nach Beendigung des Beamtenverhältnisses](#2)

[§ 3 Nebentätigkeit im öffentlichen Dienst](#3)

[§ 4 Öffentliche Ehrenämter](#4)

[§ 5 Allgemein erteilte Genehmigung, Untersagung und Widerruf](#5)

[§ 6 Nachweis- und Auskunftspflichten](#6)

### Abschnitt 2  
Vergütung und Ablieferung

[§ 7 Vergütung](#7)

[§ 8 Vergütung für Nebentätigkeiten im öffentlichen Dienst](#8)

[§ 9 Ablieferungspflicht](#9)

[§ 10 Ausnahmen vom Vergütungsverbot, Höchstbetrag und von der Ablieferungspflicht](#10)

[§ 11 Abrechnung von Nebentätigkeitsvergütungen](#11)

[§ 12 Schätzung, Fälligkeit und Zinsen](#12)

### Abschnitt 3  
Inanspruchnahme von Einrichtungen, Personal oder Material des Dienstherrn

[§ 13 Einrichtungen und Material](#13)

[§ 14 Personal](#14)

[§ 15 Inhalt der Genehmigung und Widerruf](#15)

[§ 16 Grundsätze zur Bemessung des Entgelts](#16)

[§ 17 Allgemeines Entgelt](#17)

[§ 18 Entgelt für ärztliche Nebentätigkeiten](#18)

[§ 19 Festsetzung des Entgelts](#19)

### Abschnitt 4  
Übergangsbestimmungen

[§ 20 Übergangsvorschriften](#20)

### Abschnitt 1  
Allgemeines

#### § 1 Geltungsbereich und Zuständigkeit

(1) Diese Verordnung gilt für die in § 1 des Landesbeamtengesetzes genannten Beamtinnen und Beamten.
Sie gilt auch für Ruhestandsbeamtinnen und Ruhestandsbeamte und frühere Beamtinnen und frühere Beamte hinsichtlich der Nebentätigkeiten, die sie vor Beendigung des Beamtenverhältnisses ausgeübt haben.
Für eine erst nach Beendigung des Beamtenverhältnisses aufgenommene Erwerbstätigkeit findet ausschließlich § 2 Anwendung.

(2) Die Hochschulnebentätigkeitsverordnung bleibt unberührt.

(3) Soweit das Deutsche Richtergesetz, das Brandenburgische Richtergesetz oder die Richternebentätigkeitsverordnung nichts anderes bestimmen, gelten die Vorschriften dieser Verordnung für Richterinnen und Richter entsprechend.

(4) Die für die Genehmigung oder die Entgegennahme der Anzeige zuständige Stelle hat die Maßnahmen und Entscheidungen dieser Verordnung zu treffen.
Ihr gegenüber sind auch die der Beamtin oder dem Beamten nach dieser Verordnung auferlegten Pflichten zu erfüllen.

#### § 2 Anzeigepflicht nach Beendigung des Beamtenverhältnisses

(1) Die Anzeige einer gemäß § 92 Absatz 1 des Landesbeamtengesetzes nach Beendigung des Beamtenverhältnisses aufgenommenen Erwerbstätigkeit oder sonstigen Beschäftigung außerhalb des öffentlichen Dienstes soll mindestens zwei Wochen vor der Aufnahme dieser Tätigkeit erfolgen.

(2) Die Ruhestandsbeamtin oder der Ruhestandsbeamte und die frühere Beamtin oder der frühere Beamte hat dabei insbesondere die Art und den Umfang der Tätigkeit, die Arbeitgeberin oder den Arbeitgeber oder die Auftraggeberin oder den Auftraggeber und den Zusammenhang der Tätigkeit mit der früheren dienstlichen Tätigkeit mitzuteilen.

#### § 3 Nebentätigkeit im öffentlichen Dienst

(1) Nebentätigkeit im öffentlichen Dienst im Sinne des § 83 des Landesbeamtengesetzes ist jede für den Bund, ein Land, eine Gemeinde, einen Gemeindeverband oder eine andere Körperschaft, Anstalt oder Stiftung des öffentlichen Rechts im Bundesgebiet ausgeübte Tätigkeit, mit Ausnahme der Tätigkeit für die öffentlich-rechtlichen Religionsgemeinschaften oder deren Verbände.

(2) Einer Nebentätigkeit im öffentlichen Dienst steht gleich eine Nebentätigkeit für

1.  Vereinigungen, Einrichtungen oder Unternehmen, deren Kapital (Grund- oder Stammkapital) sich unmittelbar oder mittelbar ganz oder überwiegend in öffentlicher Hand befindet oder die fortlaufend ganz oder überwiegend aus öffentlichen Mitteln unterhalten werden,
2.  zwischenstaatliche oder überstaatliche Einrichtungen, an denen eine juristische Person oder ein Verband im Sinne des Absatzes 1 durch Zahlung von Beiträgen oder Zuschüssen oder in anderer Weise beteiligt ist,
3.  natürliche oder juristische Personen, die der Wahrung von Belangen einer juristischen Person oder eines Verbandes im Sinne des Absatzes 1 dient.

#### § 4 Öffentliche Ehrenämter

(1) Öffentliche Ehrenämter im Sinne des § 83 Absatz 4 des Landesbeamtengesetzes, deren Wahrnehmung nicht als Nebentätigkeit gilt, sind insbesondere

1.  die ehrenamtliche Tätigkeit als Mitglied in einer Freiwilligen Feuerwehr,
2.  die ehrenamtliche Tätigkeit als Mitglied in einer im Katastrophen- oder Zivilschutz mitwirkenden Einheit oder Einrichtung öffentlicher Träger,
3.  die ehrenamtliche Mitgliedschaft in Organen der Sozialversicherungsträger und ihrer Verbände sowie der Bundesagentur für Arbeit,
4.  die ehrenamtliche Mitgliedschaft in den Kollegialorganen der öffentlich-rechtlichen Rundfunkanstalten,
5.  die Tätigkeit als ehrenamtliche Richterin oder ehrenamtlicher Richter oder als Schiedsperson,
6.  die Tätigkeit als Mitglied einer Personalvertretung,
7.  die auf behördlicher Bestellung oder Wahl beruhende unentgeltliche Mitwirkung bei der Erfüllung von Aufgaben im öffentlichen Dienst sowie
8.  die sonstigen in Rechtsvorschriften des öffentlichen Rechts als Ehrenämter bezeichneten Tätigkeiten einschließlich der nach der Kommunalverfassung des Landes Brandenburg ausgestalteten ehrenamtlichen Tätigkeiten.

Unentgeltlich im Sinne des Satzes 1 Nummer 7 ist die Wahrnehmung eines öffentlichen Ehrenamtes auch dann, wenn Ersatz der notwendigen Auslagen und des Verdienstausfalls gewährt wird.
Eine Pauschalierung dieser Zahlungen ist für die Unentgeltlichkeit unschädlich, wenn sich die Höhe in einem Rahmen hält, in dem aufgrund tatsächlicher Anhaltspunkte oder tatsächlicher Erhebungen nachvollziehbar ist, dass und in welchem Umfang durch die Ausübung dieser Tätigkeit finanzielle Auslagen und Verdienstausfall typischerweise entstehen.

(2) Die Wahrnehmung eines öffentlichen Ehrenamtes umfasst nur die zum unmittelbaren Aufgabenkreis dieses Amtes gehörenden Tätigkeiten.

#### § 5 Allgemein erteilte Genehmigung, Untersagung und Widerruf

(1) Die Genehmigung für eine nach § 85 Absatz 1 des Landesbeamtengesetzes genehmigungspflichtige entgeltliche Nebentätigkeit gilt allgemein als erteilt, wenn sie

1.  nur gelegentlich und außerhalb der Arbeitszeit ausgeübt wird,
2.  einen geringen Umfang hat und
3.  kein gesetzlicher Versagungsgrund vorliegt.

Der Umfang der Nebentätigkeit ist als gering anzusehen, wenn die Vergütung hierfür insgesamt 100 Euro im Monat nicht übersteigt und die zeitliche Beanspruchung durch eine oder mehrere Nebenbeschäftigungen in der Woche ein Fünftel der regelmäßigen wöchentlichen Arbeitszeit nicht überschreitet.
In diesen Fällen ist die Nebenbeschäftigung der obersten Dienstbehörde oder der von ihr bestimmten Stelle anzuzeigen.

(2) Eine als genehmigt geltende Nebenbeschäftigung ist zu untersagen, wenn ihre Ausübung dienstliche Interessen beeinträchtigt.

(3) Wird eine Genehmigung widerrufen oder eine als genehmigt geltende Nebenbeschäftigung oder eine nicht genehmigungspflichtige Nebentätigkeit untersagt, so soll der Beamtin oder dem Beamten eine angemessene Frist zur Abwicklung der Nebentätigkeit eingeräumt werden, soweit die dienstlichen Interessen dies gestatten.

#### § 6 Nachweis- und Auskunftspflichten

(1) Für den Antrag auf Genehmigung einer Nebentätigkeit nach § 88 Absatz 2 des Landesbeamtengesetzes muss über folgende Angaben der Nachweis geführt werden:

1.  Beginn und Dauer der Nebentätigkeit,
2.  Art der Nebentätigkeit,
3.  Umfang der zeitlichen Inanspruchnahme durch die Nebentätigkeit,
4.  Höhe der voraussichtlichen Entgelte und geldwerten Vorteile und
5.  die Arbeitgeberin oder den Arbeitgeber oder die Auftraggeberin oder den Auftraggeber.

Für den Nachweis hat die Beamtin oder der Beamte die erforderlichen Aufzeichnungen mit den zugehörigen Unterlagen zu führen und diese fünf Jahre ab Erteilung der Genehmigung aufzubewahren.

(2) Bei der Anzeige einer nicht genehmigungspflichtigen oder als genehmigt geltenden Nebentätigkeit hat die Beamtin oder der Beamte die in Absatz 1 Satz 1 aufgeführten Angaben mitzuteilen.

### Abschnitt 2  
Vergütung und Ablieferung

#### § 7 Vergütung

(1) Vergütung für eine Nebentätigkeit ist jede Gegenleistung in Geld oder geldwerten Vorteilen, auch wenn kein Rechtsanspruch auf sie besteht.

(2) Als Vergütung im Sinne des Absatzes 1 gelten nicht

1.  der Ersatz von Fahrtkosten sowie Tage- und Übernachtungsgelder bis zur Höhe des Betrags, den die Reisekostenvorschriften für Beamtinnen und Beamte für den vollen Kalendertag einschließlich Übernachtung vorsehen oder bei Nachweis höherer Mehraufwendungen bis zur Höhe dieses Betrags,
2.  die vereinnahmte Umsatzsteuer,
3.  der Ersatz sonstiger barer Auslagen, wenn keine Pauschalierung vorgenommen wird.

Pauschalierte Aufwandsentschädigungen sind in vollem Umfang, Tage- und Übernachtungsgelder insoweit, als sie die Beträge nach Satz 1 Nummer 1 übersteigen, als Vergütung anzusehen.

#### § 8 Vergütung für Nebentätigkeiten im öffentlichen Dienst

(1) Für eine Nebentätigkeit im öffentlichen oder ihm gleichstehenden Dienst nach § 3 oder eine Nebentätigkeit, die auf Verlangen, Vorschlag oder Veranlassung des Dienstherrn ausgeübt wird, wird grundsätzlich keine Vergütung gewährt.
Ausnahmen können zugelassen werden für

1.  Gutachtertätigkeiten sowie schriftstellerische Tätigkeiten,
2.  Tätigkeiten, für die auf andere Weise eine geeignete Arbeitskraft ohne erheblichen Mehraufwand nicht gewonnen werden kann,
3.  Tätigkeiten, deren unentgeltliche Ausübung der Beamtin oder dem Beamten nicht zugemutet werden kann.

Eine Vergütung darf nicht gewährt werden, soweit zur Ausübung der Nebentätigkeit eine Entlastung im Hauptamt erfolgt.

(2) Werden Vergütungen nach Absatz 1 Satz 2 gewährt, so dürfen sie im Kalenderjahr insgesamt für Beamtinnen und Beamte in den

Besoldungsgruppen

Euro (Bruttobetrag)

bis A 8:

4 500,

A 9 bis A 12:

5 000,

A 13 bis A 16, B 1, C 1 bis C 3, W 1 bis W 3, R 1 und R 2:

5 600,

B 2 bis B 5, C 4, R 3 bis R 5:

6 200,

ab B 6 oder ab R 6:

6 900

nicht übersteigen.
Maßgeblich ist die Besoldungsgruppe am Ende des Kalenderjahres.
Bei Teilzeitbeschäftigung gilt der Höchstbetrag ungeachtet der Arbeitszeitermäßigung.

Innerhalb des Höchstbetrags ist die Vergütung nach dem Umfang und der Bedeutung der Nebentätigkeit abzustufen.
Mit Ausnahme von Tage- und Übernachtungsgeldern dürfen Auslagen nicht pauschaliert werden.

#### § 9 Ablieferungspflicht

(1) Erhält eine Beamtin oder ein Beamter Vergütungen für eine oder mehrere Nebentätigkeiten, die im öffentlichen oder in dem ihm gleichstehenden Dienst im Sinne des § 3 oder auf Verlangen, Vorschlag oder Veranlassung des Dienstherrn ausgeübt werden, so hat die Beamtin oder der Beamte die Vergütung unverzüglich nach Ablauf eines jeden Kalenderjahres insoweit an seinen Dienstherrn im Hauptamt abzuliefern als sie für die in einem Kalenderjahr ausgeübten Tätigkeiten die in § 8 Absatz 2 genannten Bruttobeträge übersteigt.

(2) Maßgeblich für die Ablieferungspflicht nach Absatz 1 ist für das ganze Kalenderjahr die Besoldungsgruppe, in der sich die Beamtin oder der Beamte am Ende des Kalenderjahres oder bei Beendigung des Beamtenverhältnisses befindet.
Vergütungen sind mit dem Bruttobetrag vor Abzug von Steuern und Abgaben zu berücksichtigen.

(3) Vor der Ermittlung des abzuliefernden Betrages sind von der Vergütung die im Zusammenhang mit der Nebentätigkeit entstandenen Aufwendungen für

1.  Fahrtkosten sowie Unterkunft und Verpflegung bis zur Höhe der in § 7 Absatz 2 Satz 1 Nummer 1 genannten Beträge,
2.  die Aufwendungen für die Inanspruchnahme von Einrichtungen, Personal oder Material des Dienstherrn einschließlich Vorteilsausgleich,
3.  sonstige Hilfeleistungen und selbst beschafftes Material

abzusetzen.
Dies gilt nicht, soweit für diese Aufwendungen Auslagenersatz geleistet wurde.

(4) Die Verpflichtungen nach Absatz 1 treffen Ruhestandsbeamtinnen und Ruhestandsbeamte und frühere Beamtinnen und frühere Beamte nur insoweit, als die Vergütungen für Nebentätigkeiten gewährt sind, die vor Beendigung des Beamtenverhältnisses ausgeübt worden sind.

#### § 10 Ausnahmen vom Vergütungsverbot, Höchstbetrag und von der Ablieferungspflicht

Die §§ 8 und 9 sind nicht anzuwenden auf Vergütungen für

1.  Lehr-, Unterrichts-, Vortrags- oder Prüfungstätigkeiten,
2.  Tätigkeiten als vom Gericht oder der Staatsanwaltschaft beauftragte Sachverständige,
3.  Tätigkeiten auf dem Gebiet der wissenschaftlichen Forschung,
4.  eine mit Lehr- oder Forschungsaufgaben zusammenhängende selbständige Gutachtertätigkeit an öffentlichen Hochschulen, wissenschaftlichen Instituten und Anstalten, die nicht unter den von der Hochschulnebentätigkeitsverordnung geregelten Anwendungsbereich fallen,
5.  Gutachtertätigkeiten von Ärztinnen und Ärzten für Versicherungsträger oder für andere juristische Personen des öffentlichen Rechts sowie ärztliche Verrichtungen dieser Personen, für die nach der Gebührenordnung Gebühren zu zahlen sind,
6.  Tätigkeiten, die während eines unter Wegfall der Besoldung gewährten Urlaubs ausgeübt werden,
7.  eine schriftstellerische oder wissenschaftliche Tätigkeit,
8.  die Tätigkeit als unparteiisches Mitglied der Einigungsstelle.

#### § 11 Abrechnung von Nebentätigkeitsvergütungen

(1) Die Beamtin oder der Beamte hat bis spätestens zum 31.
Januar eines jeden Jahres eine Abrechnung über die im abgelaufenen Kalenderjahr zugeflossenen Vergütungen, die der Ablieferung unterliegen, vorzulegen, sofern die Vergütungen insgesamt die in § 8 Absatz 2 genannten Höchstbeträge übersteigen.
Übersteigen die abzurechnenden Vergütungen diese Höchstbeträge nicht, so hat die Beamtin oder der Beamte dies schriftlich zu versichern.

(2) In die Abrechnung hat die Beamtin oder der Beamte alle für die Berechnung des Ablieferungsbetrages erforderlichen Angaben aufzunehmen.
Zu den Angaben gehören insbesondere die bezogenen Vergütungen sowie Beginn, Ende, Umfang und Änderung des Umfangs der Nebentätigkeit.
Für den Nachweis hat die Beamtin oder der Beamte die erforderlichen Aufzeichnungen mit den zugehörigen Unterlagen zu führen.
Auf Verlangen sind die entsprechenden Aufzeichnungen und Unterlagen vorzulegen und Auskünfte zu erteilen.
Die Unterlagen hat die Beamtin oder der Beamte fünf Jahre aufzubewahren.
Die Frist beginnt mit der abschließenden Festsetzung des Ablieferungsbetrages.

(3) In den Fällen des § 9 Absatz 4 gelten die Absätze 1 und 2 für Ruhestandsbeamtinnen und Ruhestandsbeamte und frühere Beamtinnen und frühere Beamte entsprechend.

#### § 12 Schätzung, Fälligkeit und Zinsen

(1) Die abzuführende Vergütung ist im Wege der Schätzung festzusetzen, wenn die Beamtin oder der Beamte

1.  über die Vergütung keine Auskunft gibt oder
2.  über ihre oder seine Angaben keine ausreichende Aufklärung geben kann oder
3.  Aufzeichnungen nicht vorlegt, zu deren Führung sie oder er verpflichtet wurde.

Dabei sind alle Umstände zu berücksichtigen, die nach Lage des Falles für die Schätzung von Bedeutung sind.
Sobald die erforderlichen Angaben vorliegen, ist die Festsetzung zu berichtigen.

(2) Die abzuführende Vergütung wird einen Monat nach der Festsetzung fällig.
Durch die Berichtigung nach Absatz 1 Satz 3 wird die Fälligkeit nicht berührt.

(3) Wird der abzuführende Betrag innerhalb eines Monats nach Fälligkeit nicht entrichtet, so ist der rückständige Betrag ab dem Zeitpunkt der Fälligkeit mit fünf Prozentpunkten über dem Basiszinssatz nach § 247 des Bürgerlichen Gesetzbuches jährlich zu verzinsen.

### Abschnitt 3  
Inanspruchnahme von Einrichtungen, Personal oder Material des Dienstherrn

#### § 13 Einrichtungen und Material

(1) Einrichtungen sind die sächlichen Mittel, insbesondere die Diensträume und deren Ausstattung einschließlich Apparate und Instrumente mit Ausnahme von Bibliotheken und wissenschaftlicher Literatur.

(2) Material sind alle verbrauchbaren Sachen und die Energie.

#### § 14 Personal

(1) Das Personal des Dienstherrn darf nur innerhalb seiner Arbeitszeit und nur im Rahmen seiner üblichen Dienstaufgaben in Anspruch genommen werden.
Aus Anlass der Mitwirkung an der Nebentätigkeit darf Mehrarbeit, Bereitschaftsdienst oder Rufbereitschaft nicht angeordnet, genehmigt und vergütet werden.

(2) Vereinbarungen über eine private Mitarbeit außerhalb der Arbeitszeit bleiben unberührt.

(3) Soweit dem Personal des Dienstherrn aus Anlass der Mitwirkung an einer Nebentätigkeit zusätzliche Vergütung gezahlt wird, ist hierüber Auskunft zu erteilen.

#### § 15 Inhalt der Genehmigung und Widerruf

(1) In der Genehmigung nach § 89 des Landesbeamtengesetzes ist der Umfang der zugelassenen Inanspruchnahme anzugeben.

(2) Die Genehmigung ist ganz oder teilweise zu widerrufen, wenn die Inanspruchnahme zu einer Beeinträchtigung der dienstlichen Interessen führt.
Sie ist ferner zu widerrufen, wenn das Nutzungsentgelt für die Inanspruchnahme nicht entrichtet wird.
Die §§ 48 und 49 des Verwaltungsverfahrensgesetzes bleiben unberührt.

#### § 16 Grundsätze zur Bemessung des Entgelts

(1) Die Höhe des Entgelts für die Inanspruchnahme von Einrichtungen, Material und Personal des Dienstherrn richtet sich unter Berücksichtigung der §§ 17 und 18 nach den Grundsätzen der Kostendeckung und des Vorteilsausgleichs.

(2) Auf die Entrichtung eines Entgelts kann ausnahmsweise verzichtet werden

1.  bei einer unentgeltlichen Nebentätigkeit oder
2.  wenn der Betrag insgesamt 100 Euro brutto im Kalenderjahr nicht übersteigt.

Auf die Entrichtung eines Entgelts soll verzichtet werden, wenn die Nebentätigkeit auf Verlangen, Vorschlag oder Veranlassung des Dienstherrn ausgeübt wird.

(3) Nehmen mehrere Beamtinnen oder Beamte Einrichtungen, Personal oder Material des Dienstherrn gemeinschaftlich in Anspruch, sind sie als Gesamtschuldnerinnen oder Gesamtschuldner zur Entrichtung des Entgelts verpflichtet.

#### § 17 Allgemeines Entgelt

(1) Das Entgelt ist pauschaliert nach einem Prozentsatz der für die Nebentätigkeit bezogenen Bruttovergütung zu bemessen.
Es beträgt im Regelfall

5 Prozent

für die Inanspruchnahme von Einrichtungen,

10 Prozent

für die Inanspruchnahme von Personal,

5 Prozent

für den Verbrauch von Material,

10 Prozent

für den durch die Inanspruchnahme von Einrichtungen, Personal  
oder Material erwachsenen wirtschaftlichen Vorteil.

(2) Die oberste Dienstbehörde kann im Einvernehmen mit dem für Finanzen zuständigen Ministerium abweichend von Absatz 1 Gebührenordnungen und sonstige allgemeine Kostentarife für anwendbar erklären, soweit sie die entstandenen Kosten abdecken und Vorteile ausgleichen.
Bei Körperschaftsbeamtinnen und Körperschaftsbeamten bedarf dies des Einvernehmens mit der Aufsichtsbehörde.

(3) Wird die Nebentätigkeit unentgeltlich ausgeübt, ohne dass nach § 16 Absatz 2 Satz 1 Nummer 1 auf ein Entgelt verzichtet wird, so bemisst sich die Höhe des Entgelts nach dem Wert der Inanspruchnahme von Personal, Einrichtungen oder Material.
Das Entgelt für den wirtschaftlichen Vorteil entfällt.

(4) Wird nachgewiesen, dass das nach den Prozentsätzen nach Absatz 1 berechnete Entgelt offensichtlich um mehr als 25 Prozent niedriger oder höher ist, als es dem Wert der Inanspruchnahme entspricht, so ist es von Amts wegen oder auf Antrag der Beamtin oder des Beamten nach dem Wert

1.  der anteiligen Kosten für die Beschaffung, Unterhaltung und Verwaltung der benutzten Einrichtungen,
2.  der anteiligen Kosten für das in Anspruch genommene Personal einschließlich der Personalnebenkosten und der Gemeinkosten,
3.  der Beschaffungs- und anteiligen Verwaltungskosten für das Material,
4.  des durch die Bereitstellung von Einrichtungen, Personal oder Material erwachsenen wirtschaftlichen Vorteils der Beamtin oder des Beamten

festzusetzen.
Der Nachweis muss innerhalb einer Ausschlussfrist von drei Monaten nach Festsetzung des Entgelts erbracht werden.

#### § 18 Entgelt für ärztliche Nebentätigkeiten

(1) Das Entgelt für ärztliche Nebentätigkeiten in Krankenhäusern und Einrichtungen zur Rehabilitation ist zu pauschalieren, soweit in den Absätzen 2 und 3 nichts anderes bestimmt oder zugelassen wird.
Für ärztliche Nebentätigkeiten in anderen Tätigkeitsbereichen richtet sich die Höhe des Entgelts nach den allgemeinen Bestimmungen des § 17.

(2) Die Höhe der Kostenerstattung bemisst sich nach den von der obersten Dienstbehörde zu erlassenden Bestimmungen, die den Grundsätzen der Kostendeckung entsprechen müssen.

(3) Der Vorteilsausgleich beträgt 20 Prozent der im Kalenderjahr aus der Nebentätigkeit erzielten Einnahmen bis 100 000 Euro, die der Beamtin oder dem Beamten nach Abzug der nach Absatz 2 zu erstattenden Kosten verbleiben, und 30 Prozent von dem darüber hinausgehenden Mehrbetrag.
Bei einem Honorarverzicht ist ein Vorteilsausgleich nicht zu entrichten.

#### § 19 Festsetzung des Entgelts

(1) Das zu zahlende Entgelt wird nach dem Ende der Inanspruchnahme, mindestens jedoch einmal im Jahr festgesetzt.
Das Entgelt wird einen Monat nach der Festsetzung fällig.

(2) Die Beamtin oder der Beamte ist verpflichtet, das Ende der Inanspruchnahme unverzüglich anzuzeigen.
Die Beamtin oder der Beamte hat die für die Berechnung des Entgelts notwendigen Aufzeichnungen zu führen und mit den zur Glaubhaftmachung notwendigen Belegen unverzüglich nach Beendigung, bei fortlaufender Inanspruchnahme einmal jährlich bis zum 31.
Januar eines jeden Kalenderjahres vorzulegen.
Diese Unterlagen hat die Beamtin oder der Beamte fünf Jahre, vom Tage der Festsetzung des Entgelts an gerechnet, aufzubewahren.

### Abschnitt 4  
Übergangsbestimmungen

#### § 20 Übergangsvorschriften

(1) Die vor dem Inkrafttreten dieser Verordnung erteilten und über diesen Zeitpunkt hinaus geltenden Genehmigungen zur Inanspruchnahme von Einrichtungen, Personal oder Material des Dienstherrn gelten weiterhin; sie enden spätestens mit Ablauf des letzten Tages des zwölften Monats, der auf den Monat des Inkrafttretens dieser Verordnung folgt.
Auf sie sind die vor dem Inkrafttreten dieser Verordnung geltenden Bestimmungen über die Entrichtung des Nutzungsentgelts weiter anzuwenden.

(2) Am Tag des Inkrafttretens dieser Verordnung bestehende öffentlich-rechtliche Verträge und Zusicherungen, die Nebentätigkeiten oder die Inanspruchnahme von Einrichtungen, Personal oder Material betreffen, bleiben unberührt.

(3) Für Abrechnungen über die Vergütungen aus Nebentätigkeiten, die der Beamtin oder dem Beamten vor dem 1.
Januar 2019 zugeflossen sind, gelten die vor dem Inkrafttreten dieser Verordnung für die Abrechnung über Nebentätigkeiten geltenden Bestimmungen.