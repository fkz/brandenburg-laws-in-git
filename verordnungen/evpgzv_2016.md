## Verordnung über Zuständigkeiten nach dem Energieverbrauchsrelevante-Produkte-Gesetz (EVPG-Zuständigkeitsverordnung -  EVPG-ZV)

Auf Grund des § 9 Absatz 2 und 3 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl. I S. 186) und des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl.
I S. 602) verordnet die Landesregierung:

#### § 1  
Zuständigkeiten

(1) Das Ministerium für Arbeit, Soziales, Gesundheit, Frauen und Familie ist oberste Landesbehörde im Sinne des § 7 Absatz 2 des Energieverbrauchsrelevante-Produkte-Gesetzes vom 27.
Februar 2008 (BGBl. I S. 258), das durch das Gesetz vom 16.
November 2011 (BGBl.
I S. 2224) geändert worden ist.

(2) Das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit ist:

1.  zuständige Behörde gemäß § 7 Absatz 1, 3, 4, 5, 6 und 7, § 8 Absatz 1, § 9 Absatz 3, § 11 Absatz 2, 3, 4 und 5 und § 12 Absatz 2 des Energieverbrauchsrelevante-Produkte-Gesetzes sowie
2.  zuständig für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 13 des Energieverbrauchsrelevante-Produkte-Gesetzes.

#### § 2  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 11.
Juni 2013

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident  
  
Matthias Platzeck

Der Minister für Arbeit,  
Soziales, Frauen und Familie

Günter Baaske