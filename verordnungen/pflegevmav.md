## Verordnung über den Mehrbelastungsausgleich für den Vollzug der Bußgeldverfahren in der Pflegeversicherung (Pflegeversicherung-Mehrbelastungsausgleichsverordnung - PflegeVMAV)

Auf Grund des § 8 Absatz 4 Satz 9 in Verbindung mit Satz 10 des Landespflegegesetzes vom 29. Juni 2004 (GVBl.
I S. 339), der durch Gesetz vom 12.
Juli 2011 (GVBl.
I Nr. 15) geändert worden ist, verordnet die Ministerin für Arbeit, Soziales, Gesundheit, Frauen und Familie im Benehmen mit dem Minister der Finanzen und dem Minister des Innern und für Kommunales:

#### § 1 Mehrbelastungsausgleich

(1) Die nach § 8 Absatz 4 Satz 1 des Landespflegegesetzes zuständigen Landkreise und kreisfreien Städte erhalten die für die Verfolgung und Ahndung von Ordnungswidrigkeiten gemäß § 121 Absatz 1 Nummer 1, 3 bis 6 des Elften Buches Sozialgesetzbuch entstehenden Mehrbelastungen kalenderjährlich erstattet.
Hierfür erhalten die Landkreise und kreisfreien Städte eine Verwaltungskostenpauschale je tatsächlich im Kalenderjahr bearbeiteten Fall auf der Grundlage der bei dem Landkreis oder der kreisfreien Stadt eingegangenen Meldungen des Bundesversicherungsamtes oder der Pflegekasse, soweit die Mehrbelastungen nicht durch die im gleichen Zeitraum tatsächlich vereinnahmten Bußgelder und Verwaltungsgebühren gedeckt werden können.

(2) Zur Ermittlung der Verwaltungskostenpauschale nach Absatz 1 Satz 2 werden ab dem 1. Januar 2015 pro Fall

1.  ein zeitlicher Bearbeitungsaufwand von 90 Minuten und
2.  die für das Jahr 2015 geltenden Kosten eines Arbeitsplatzes der Entgeltgruppe 8 zu zwei Drittel und der Entgeltgruppe 9 zu einem Drittel des Tarifvertrages für den öffentlichen Dienst nach den Festlegungen der Kommunalen Gemeinschaftsstelle für Verwaltungsmanagement

berücksichtigt und entsprechend Absatz 3 fortgeschrieben.

(3) Gemäß Absatz 2 wird die Verwaltungskostenpauschale

1.  ab dem 1.
    Januar 2015 auf einen Betrag in Höhe von 69,11 Euro und
2.  ab dem 1.
    Januar 2016 auf einen Betrag in Höhe von 70,49 Euro

festgesetzt und sodann regelmäßig entsprechend dem jeweiligen Tarifabschluss für den öffentlichen Dienst der Kommunen im Land Brandenburg im Einvernehmen mit dem für Finanzen zuständigen Ministerium angepasst.
Die neu errechnete Verwaltungskostenpauschale ist im Amtsblatt für Brandenburg jeweils bis zum 31.
Juli des auf den Tarifabschluss folgenden Kalenderjahres bekannt zu geben.

(4) Die nach § 2 Absatz 1 zuständige Behörde erstattet den Landkreisen und kreisfreien Städten auf Antrag den Unterschiedsbetrag zwischen dem Auszahlungsbetrag nach Absatz 1 Satz 2 und den sich unter Berücksichtigung der tatsächlichen notwendigen Kosten ergebenden Mehrbelastungen bei der Wahrnehmung der Aufgabe gemäß § 8 Absatz 4 Satz 1 des Landespflegegesetzes.
Satz 1 gilt nicht, soweit die Landkreise und kreisfreien Städte in den vorangegangenen zwei Abrechnungszeiträumen bei der Wahrnehmung der Aufgabe gemäß § 8 Absatz 4 Satz 1 des Landespflegegesetzes Überschüsse erzielt haben, die den Unterschiedsbetrag gemäß Satz 1 übersteigen und die nicht bereits im Rahmen eines früheren Antrags nach Satz 1 berücksichtigt worden sind.

#### § 2 Zuständigkeit, Verfahren

(1) Zuständige Behörde für die Durchführung des Erstattungsverfahrens ist die Behörde, die nach § 8 Absatz 4 Satz 6 des Landespflegegesetzes die Abrechnung vornimmt.

(2) Die Erstattung der Mehrbelastungen ist als Gesamtbetrag bis zum 30.
September des dem Abrechnungszeitraum folgenden Kalenderjahres bei der zuständigen Behörde schriftlich geltend zu machen.
Bei der Geltendmachung sind anzugeben

1.  die Anzahl der vom 1.
    Januar bis 31.
    Dezember bearbeiteten Fälle und
2.  die Summen der im gleichen Zeitraum vereinnahmten
    1.  Bußgelder,
    2.  Verwarnungsgelder nach § 56 Absatz 1 Satz 1 des Gesetzes über Ordnungswidrigkeiten,
    3.  Verwaltungsgebühren und
    4.  Auslagen nach § 107 Absatz 3 des Gesetzes über Ordnungswidrigkeiten.

Die zuständige Behörde kann Formulare für die Geltendmachung vorgeben.

(3) Die zuständige Behörde setzt nach Abschluss der Prüfung der Abrechnung die Höhe der jeweils zu erstattenden Mehrbelastungen fest.
Den Erstattungsbetrag hat die zuständige Behörde um die Höhe von zu gering angesetzten Bußgeldern und Verwaltungsgebühren zu kürzen, wenn diese auf die Nichtbeachtung von Weisungen oder von Maßgaben des von der Sonderaufsichtsbehörde erlassenen Bußgeldkataloges zurückzuführen sind.

(4) Die für die Festsetzung der Erstattung notwendigen Nachweise sind fünf Jahre nach Ablauf des Jahres, in dem das Erstattungsverfahren beendet wurde, aufzubewahren.
Die Nachweise können zur Aufbewahrung auf einem Bildträger oder auf anderen Datenträgern gespeichert werden.
Dabei muss sichergestellt sein, dass die gespeicherten Daten

1.  mit den Nachweisen und Unterlagen bildlich oder inhaltlich übereinstimmen,
2.  den Grundsätzen einer ordnungsgemäßen Buchführung entsprechen und
3.  während der Dauer der Aufbewahrungsfrist verfügbar sind und jederzeit innerhalb angemessener Frist lesbar gemacht werden können.

(5) Die Absätze 1 bis 4 gelten für das Erstattungsverfahren nach § 1 Absatz 4 entsprechend, wobei die Antragstellerin oder der Antragsteller die tatsächlichen notwendigen Kosten nachzuweisen hat.

#### § 3 Übergangsvorschrift

(1) Für die Erstattungsverfahren in den Jahren 2016 und 2017 gilt § 2 Absatz 2 Satz 1 nicht.
Stattdessen hat die zuständige Behörde die Landkreise und kreisfreien Städte unter angemessener Fristsetzung aufzufordern, die Erstattung von Mehrbelastungen schriftlich geltend zu machen.

(2) Soweit in dem im Jahr 2016 durchgeführten Erstattungsverfahren vorläufige Abschläge ausgezahlt wurden, werden etwaig entstehende Differenzbeträge mit der jeweils nächstfolgenden Erstattung verrechnet bis der Ausgleich erfolgt ist.

(3) Abweichend von § 2 Absatz 5 können Anträge nach § 1 Absatz 4 für die Erstattungsverfahren der Jahre 2016 und 2017 bis zum 31.
März 2018 gestellt werden.

#### § 4 Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2016 in Kraft.

Potsdam, den 22.
Dezember 2017

Die Ministerin für Arbeit, Soziales,  
Gesundheit, Frauen und Familie

Diana Golze