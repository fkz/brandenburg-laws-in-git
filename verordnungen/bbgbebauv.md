## Brandenburgische Verordnung über den Bau und Betrieb von Beherbergungsstätten (Brandenburgische Beherbergungsstättenbau-Verordnung - BbgBeBauV)

Auf Grund des § 86 Absatz 1 Nummer 1 und 4 in Verbindung mit § 85 Absatz 1 Satz 1 Nummer 1 der Brandenburgischen Bauordnung vom 19.
Mai 2016 (GVBl.
I Nr. 14) verordnet die Ministerin für Infrastruktur und Landesplanung:

**Inhaltsübersicht**

[§ 1 Anwendungsbereich](#1)

[§ 2 Begriffe](#2)

[§ 3 Rettungswege](#3)

[§ 4 Tragende Wände, Stützen, Decken](#4)

[§ 5 Trennwände](#5)

[§ 6 Notwendige Flure](#6)

[§ 7 Türen](#7)

[§ 8 Sicherheitsbeleuchtung, Sicherheitsstromversorgung](#8)

[§ 9 Alarmierungseinrichtungen, Brandmeldeanlagen, Brandfallsteuerung von Aufzügen](#9)

[§ 10 Weitergehende Anforderungen](#10)

[§ 11 Barrierefreie Beherbergungsräume](#11)

[§ 12 Freihalten der Rettungswege, Brandschutzordnung, verantwortliche Personen](#12)

[§ 13 Zusätzliche Bauvorlagen](#13)

[§ 14 Anwendung der Vorschriften auf bestehende Beherbergungsstätten](#14)

[§ 15 Ordnungswidrigkeiten](#15)

[§ 16 Inkrafttreten, Außerkrafttreten](#16)

#### § 1 Anwendungsbereich

Die Vorschriften dieser Verordnung gelten für Beherbergungsstätten mit mehr als zwölf Gastbetten.

#### § 2 Begriffe

(1) Beherbergungsstätten sind Gebäude oder Gebäudeteile, die ganz oder teilweise für die Beherbergung von Gästen, ausgenommen die Beherbergung in Ferienwohnungen, bestimmt sind.

(2) Beherbergungsräume sind Räume, die dem Wohnen oder Schlafen von Gästen dienen.
Eine Folge unmittelbar zusammenhängender Beherbergungsräume (Suite) gilt als ein Beherbergungsraum.

(3) Gasträume sind Räume, die für den Aufenthalt von Gästen, jedoch nicht zum Wohnen oder Schlafen bestimmt sind, wie Speiseräume und Tagungsräume.

#### § 3 Rettungswege

(1) Für jeden Beherbergungsraum müssen mindestens zwei voneinander unabhängige Rettungswege vorhanden sein; sie dürfen jedoch innerhalb eines Geschosses über denselben notwendigen Flur führen.
Der erste Rettungsweg muss für Beherbergungsräume, die nicht zu ebener Erde liegen, über eine notwendige Treppe führen, der zweite Rettungsweg über eine weitere notwendige Treppe oder eine Außentreppe.
In Beherbergungsstätten mit insgesamt nicht mehr als 60 Gastbetten genügt als zweiter Rettungsweg eine mit Rettungsgeräten der Feuerwehr erreichbare Stelle des Beherbergungsraumes; dies gilt nicht, wenn in einem Geschoss mehr als 30 Gastbetten vorhanden sind.

(2) An Abzweigungen notwendiger Flure, an den Zugängen zu notwendigen Treppenräumen und an den Ausgängen ins Freie ist durch Sicherheitszeichen auf die Ausgänge hinzuweisen.
Die Sicherheitszeichen müssen beleuchtet sein.

#### § 4 Tragende Wände, Stützen, Decken

(1) Tragende Wände, Stützen und Decken müssen feuerbeständig sein.
Dies gilt nicht für oberste Geschosse von Dachräumen, wenn sich dort keine Beherbergungsräume befinden.

(2) Tragende Wände, Stützen und Decken brauchen nur feuerhemmend zu sein

1.  in Gebäuden mit nicht mehr als zwei oberirdischen Geschossen,
2.  in obersten Geschossen von Dachräumen mit Beherbergungsräumen.

#### § 5 Trennwände

(1) Trennwände müssen feuerbeständig sein

1.  zwischen Räumen einer Beherbergungsstätte und Räumen, die nicht zu der Beherbergungsstätte gehören, sowie
2.  zwischen Beherbergungsräumen und
    1.  Gasträumen,
    2.  Küchen.

Soweit in Beherbergungsstätten die tragenden Wände, Stützen und Decken nur feuerhemmend zu sein brauchen, genügen feuerhemmende Trennwände.

(2) Trennwände zwischen Beherbergungsräumen sowie zwischen Beherbergungsräumen und sonstigen Räumen müssen feuerhemmend sein.

(3) In Trennwänden nach Absatz 1 Satz 1 Nummer 2 und nach Absatz 2 sind Öffnungen unzulässig.
Öffnungen in Trennwänden nach Absatz 1 Satz 1 Nummer 1 müssen feuerhemmende Feuerschutzabschlüsse haben, die auch die Anforderungen an Rauchschutzabschlüsse erfüllen.

#### § 6 Notwendige Flure

(1) § 36 Absatz 1 Satz 2 Nummer 3 der Brandenburgischen Bauordnung ist nicht anzuwenden.

(2) In notwendigen Fluren müssen Bekleidungen, Unterdecken und Dämmstoffe aus nichtbrennbaren Baustoffen bestehen.
Bodenbeläge müssen aus mindestens schwerentflammbaren Baustoffen bestehen.

(3) In notwendigen Fluren mit nur einer Fluchtrichtung (Stichfluren) darf die Entfernung zwischen Türen von Beherbergungsräumen und notwendigen Treppenräumen oder Ausgängen ins Freie nicht länger als 15 Meter sein.

(4) Stufen in notwendigen Fluren müssen beleuchtet sein.

#### § 7 Türen

(1) Feuerhemmende Feuerschutzabschlüsse, die auch die Anforderungen an Rauchschutzabschlüsse erfüllen, müssen vorhanden sein in Öffnungen

1.  von notwendigen Treppenräumen zu anderen Räumen, ausgenommen zu notwendigen Fluren, und
2.  von notwendigen Fluren in Kellergeschossen zu Räumen, die von Gästen nicht benutzt werden.

(2) Rauchschutzabschlüsse müssen vorhanden sein in Öffnungen

1.  von notwendigen Treppenräumen zu notwendigen Fluren,
2.  von notwendigen Fluren zu Beherbergungsräumen und
3.  von notwendigen Fluren zu Gasträumen, wenn an den Fluren in demselben Rauchabschnitt Öffnungen zu Beherbergungsräumen liegen.

#### § 8 Sicherheitsbeleuchtung, Sicherheitsstromversorgung

(1) Beherbergungsstätten müssen

1.  in notwendigen Fluren und in notwendigen Treppenräumen,
2.  in Räumen zwischen notwendigen Treppenräumen und Ausgängen ins Freie,
3.  für Sicherheitszeichen, die auf Ausgänge hinweisen, und
4.  für Stufen in notwendigen Fluren

eine Sicherheitsbeleuchtung haben.

(2) Beherbergungsstätten müssen eine Sicherheitsstromversorgung haben, die bei Ausfall der allgemeinen Stromversorgung den Betrieb der sicherheitstechnischen Anlagen und Einrichtungen übernimmt, insbesondere

1.  der Sicherheitsbeleuchtung,
2.  der Alarmierungseinrichtungen und
3.  der Brandmeldeanlage.

#### § 9 Alarmierungseinrichtungen, Brandmeldeanlagen, Brandfallsteuerung von Aufzügen

(1) Beherbergungsstätten müssen Alarmierungseinrichtungen haben, durch die im Gefahrenfall die Betriebsangehörigen und Gäste gewarnt werden können.
Bei Beherbergungsstätten mit mehr als 60 Gastbetten müssen sich die Alarmierungseinrichtungen bei Auftreten von Rauch in den notwendigen Fluren auch selbsttätig auslösen.
In Beherbergungsräumen nach § 11 muss die Auslösung des Alarms optisch und akustisch erkennbar sein.

(2) In Beherbergungsstätten muss jeder Beherbergungsraum mindestens einen Rauchwarnmelder mit einer fest eingebauten und durch den Melder überwachten Langzeitbatterie haben.
Dieser muss so eingebaut oder angebracht und betrieben werden, dass Brandrauch frühzeitig erkannt und gemeldet wird.
Alternativ kann die Überwachung auch durch automatische Melder der Brandmeldeanlage sichergestellt werden.

(3) Beherbergungsstätten mit mehr als 60 Gastbetten müssen Brandmeldeanlagen mit automatischen Brandmeldern, die auf die Kenngröße Rauch in den notwendigen Fluren ansprechen, sowie mit nichtautomatischen Brandmeldern (Handfeuermelder) zur unmittelbaren Alarmierung der dafür zuständigen Stelle haben.
Die automatischen Brandmeldeanlagen müssen in einer Betriebsart ausgeführt sein, bei der mit technischen Maßnahmen Falschalarme vermieden werden.
Brandmeldungen sind unmittelbar und automatisch zur zuständigen Regionalleitstelle zu übertragen.

(4) Aufzüge von Beherbergungsstätten mit mehr als 60 Gastbetten sind mit einer Brandfallsteuerung auszustatten, die durch die automatische Brandmeldeanlage ausgelöst wird.
Die Brandfallsteuerung hat sicherzustellen, dass die Aufzüge das nicht vom Rauch betroffene Eingangsgeschoss, ansonsten das in Fahrtrichtung davor liegende Geschoss, anfahren und dort mit geöffneten Türen außer Betrieb gehen.

#### § 10 Weitergehende Anforderungen

An Beherbergungsstätten in Hochhäusern können aus Gründen des Brandschutzes weitergehende Anforderungen gestellt werden.

#### § 11 Barrierefreie Beherbergungsräume

Mindestens 10 Prozent der Gastbetten müssen in Beherbergungsräumen liegen, die einschließlich der zugehörigen Sanitärräume den Anforderungen an barrierefrei nutzbare Wohnungen gemäß § 50 Absatz 1 der Brandenburgischen Bauordnung entsprechen.
In Beherbergungsstätten mit mehr als 60 Gastbetten muss mindestens 1 Prozent der Gastbetten in Beherbergungsräumen liegen, die einschließlich der zugehörigen Sanitärräume barrierefrei und uneingeschränkt mit dem Rollstuhl nutzbar und für zwei Gastbetten geeignet sind; die erforderlichen Räume können auf die Räume nach Satz 1 angerechnet werden.

#### § 12 Freihalten der Rettungswege, Brandschutzordnung, verantwortliche Personen

(1) Die Rettungswege müssen frei von Hindernissen sein.
Türen im Zuge von Rettungswegen dürfen nicht versperrt werden und müssen von innen leicht zu öffnen sein.

(2) In jedem Beherbergungsraum sind an dessen Ausgang ein Rettungswegplan und Hinweise zum Verhalten bei einem Brand anzubringen.
Die Hinweise müssen auch in den Fremdsprachen, die der Herkunft der üblichen Gäste Rechnung tragen, abgefasst sein.

(3) Für Beherbergungsstätten mit mehr als 60 Gastbetten sind im Einvernehmen mit der für den Brandschutz zuständigen Dienststelle

1.  eine Brandschutzordnung zu erstellen und
2.  Feuerwehrpläne anzufertigen; die Feuerwehrpläne sind der örtlichen Feuerwehr zur Verfügung zu stellen.

(4) Die Betriebsangehörigen sind bei Beginn des Arbeitsverhältnisses und danach mindestens einmal jährlich über

1.  die Bedienung der Alarmierungseinrichtungen und der Brandmelder zu unterweisen und
2.  die Brandschutzordnung und das Verhalten bei einem Brand und über die Rettung von Menschen mit Behinderung, insbesondere Rollstuhlnutzerinnen und -nutzer,

zu belehren.

(5) Für die Einhaltung der in den Absätzen 1 bis 4 gestellten Anforderungen ist die Betreiberin oder der Betreiber oder die von ihr oder ihm beauftragte Person verantwortlich.

#### § 13 Zusätzliche Bauvorlagen

Die Bauvorlagen müssen zusätzliche Angaben enthalten über

1.  die Sicherheitsbeleuchtung,
2.  die Sicherheitsstromversorgung,
3.  die Alarmierungseinrichtungen,
4.  die Brandmeldeanlage,
5.  die Rettungswege auf dem Grundstück und die Flächen für die Feuerwehr,
6.  die Anzahl der Gastbetten und ihre Zuordnung zu Beherbergungsräumen nach § 11.

#### § 14 Anwendung der Vorschriften auf bestehende Beherbergungsstätten

(1) Auf die zum Zeitpunkt des Inkrafttretens dieser Verordnung bestehenden Beherbergungsstätten sind die Vorschriften des § 12 anzuwenden.

(2) Die zum Zeitpunkt des Inkrafttretens dieser Verordnung bestehenden Beherbergungsstätten sind in Anlehnung an § 48 Absatz 4 Satz 3 der Brandenburgischen Bauordnung bis 31.
Dezember 2020 an die Vorschriften des § 9 Absatz 2 anzupassen.

#### § 15 Ordnungswidrigkeiten

Ordnungswidrig nach § 85 Absatz 1 Satz 1 Nummer 1 der Brandenburgischen Bauordnung handelt, wer vorsätzlich oder fahrlässig

1.  entgegen § 12 Absatz 1 Rettungswege nicht frei von Hindernissen hält, Türen im Zuge von Rettungswegen versperrt oder versperren lässt oder als verantwortliche Person nicht dafür sorgt, dass diese Türen von innen leicht geöffnet werden können,
2.  entgegen § 12 Absatz 2 den Rettungswegplan und Hinweise zum Verhalten bei einem Brand nicht in jedem Beherbergungsraum anbringt oder anbringen lässt.

#### § 16 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Beherbergungsstättenbau-Verordnung vom 15.
Juni 2001 (GVBl.
II S. 216), die durch Artikel 4 der Verordnung vom 23.
März 2005 (GVBl.
II S. 159) geändert worden ist, außer Kraft.

Potsdam, den 8.
November 2017

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#a) Diese Verordnung dient der Umsetzung der Richtlinie (EU) 2015/1535 des Europäischen Parlaments und des Rates vom 9.
September 2015 über ein Informationsverfahren auf dem Gebiet der technischen Vorschriften und der Vorschriften für die Dienste der Informationsgesellschaft (ABl. L 241 vom 17.09.2015, S.
1).