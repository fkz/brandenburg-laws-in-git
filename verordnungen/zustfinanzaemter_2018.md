## Verordnung über die Zuständigkeiten der Finanzämter des Landes Brandenburg

Auf Grund

*   des § 17 Absatz 1 und Absatz 2 Satz 3 des Finanzverwaltungsgesetzes in der Fassung der Bekanntmachung vom 4. April 2006 (BGBl. I S. 846, 1202),
*   des § 2 Absatz 2 Satz 1 und des § 17 Absatz 3 des Finanzverwaltungsgesetzes,
*   des § 387 Absatz 2 Satz 1 und 2 in Verbindung mit § 409 Satz 2 der Abgabenordnung in der Fassung der Bekanntmachung vom 1.
    Oktober 2002 (BGBl. I S. 3866; 2003 I S. 61)

in Verbindung mit § 1 der Verordnung über die Übertragung von Ermächtigungen zum Erlass von Rechtsverordnungen im Bereich der Finanzverwaltung auf den Minister der Finanzen vom 23. August 1991 (GVBl. S.
390), der durch Verordnung vom 20.
November 2017 (GVBl.
II Nr. 63) geändert worden ist, verordnet der Minister der Finanzen:

#### § 1 

(1) Die in Anlage 1 bezeichneten Finanzämter sind jeweils in ihrem Bezirk für die von den Finanzämtern wahrzunehmenden Aufgaben der Steuerverwaltung und für die ihnen sonst übertragenen Aufgaben zuständig, soweit in Absatz 2 nichts anderes bestimmt ist.

(2) Den in Anlage 2 bezeichneten Finanzämtern werden in dem dort beschriebenen Umfang Zuständigkeiten für die Bezirke mehrerer Finanzämter übertragen.

(3) Die Aufgaben des Technischen Finanzamtes Cottbus sind in der Verordnung zur Errichtung des Technischen Finanzamtes vom 27.
April 2004 (GVBl.
II S. 322) in der jeweils geltenden Fassung geregelt.

#### § 2 

Diese Verordnung tritt am 1.
Februar 2018 in Kraft.
Gleichzeitig tritt die Verordnung über die Zuständigkeiten der Finanzämter des Landes Brandenburg vom 9.
September 2015 (GVBl.
II Nr. 43) außer Kraft.

Potsdam, den 19.
Januar 2018

Der Minister der Finanzen

Christian Görke

* * *

### Anlagen

1

[Anlage 1 (zu § 1 Absatz 1)](/br2/sixcms/media.php/68/GVBl_II_05_2018-Anlage-1.pdf "Anlage 1 (zu § 1 Absatz 1)") 1.6 MB

2

[Anlage 2 (zu § 1 Absatz 2)](/br2/sixcms/media.php/68/FinanzZV-Anlage-2.pdf "Anlage 2 (zu § 1 Absatz 2)") 445.9 KB