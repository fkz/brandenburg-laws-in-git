## Verordnung über den Landesentwicklungsplan Berlin-Brandenburg (LEP B-B)

Auf Grund des Artikels 8 Absatz 4 des Landesplanungsvertrages in der Fassung der Bekanntmachung vom 13.
Februar 2012 (GVBl.
I Nr. 14) in Verbindung mit Artikel 1 des Gesetzes zu dem Fünften Staatsvertrag vom 16.
Februar 2011 über die Änderung des Landesplanungsvertrages und zur Änderung weiterer planungsrechtlicher Vorschriften vom 21.
September 2011 (GVBl.
I Nr. 21) und auf Grund des § 12 Absatz 6 und § 28 Absatz 1 Satz 2 des Raumordnungsgesetzes vom 22.
Dezember 2008 (BGBl.
I S. 2986) in Verbindung mit Artikel 8 Absatz 6 des Landesplanungsvertrages in der Fassung der Bekanntmachung vom 10.
Februar 2008 (GVBl.
I S. 42) in Verbindung mit Artikel 1 des Gesetzes zu dem Staatsvertrag der Länder Berlin und Brandenburg über das Landesentwicklungs-programm 2007 (LEPro 2007) und die Änderung des Landesplanungsvertrages vom 18.
Dezember 2007 (GVBl.
I S. 235) und §§ 4 und 3 Absatz 2 des Brandenburgischen Landesplanungsgesetzes in der Fassung der Bekannt-machung vom 12.
Dezember 2002 (GVBl.
2003 I S. 9), insoweit im Benehmen mit dem Ausschuss für Infrastruktur und Landwirtschaft des Landtages, verordnet die Landesregierung:

#### § 1 

Der Landesentwicklungsplan Berlin-Brandenburg (LEP B-B), der als Anlage zu dieser Verordnung veröffentlicht wird, ist Bestandteil dieser Verordnung.
Er besteht aus textlichen und zeichnerischen Festlegungen (Landesentwicklungsplan Berlin-Brandenburg (LEP B-B), Festlegungskarte 1 – Gesamtraum im Maßstab 1 : 250 000 und Festlegungskarte 2 – Städtische Kernbereiche gemäß Plansatz 4.8 (G) Absatz 3 im Maßstab 1 : 250 000).

#### § 2 

Der Landesentwicklungsplan Berlin-Brandenburg (LEP B-B) wird im Land Brandenburg bei der Gemeinsamen Landesplanungsabteilung, bei den Landkreisen, den kreisfreien Städten, amtsfreien Gemeinden und Ämtern zur Einsicht für jedermann niedergelegt.

#### § 3 

Diese Verordnung tritt mit Wirkung vom 15.
Mai 2009 in Kraft.

Potsdam, den 27.
Mai 2015

Die Landesregierung des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider

Hinweis:

Eine Verletzung der für Raumordnungspläne geltenden Verfahrens- und Formvorschriften, die nicht schriftlich innerhalb eines Jahres nach der öffentlichen Bekanntmachung geltend gemacht worden ist, ist unbeachtlich (Artikel 9 Absatz 1 des Landesplanungsvertrages in der Fassung der Bekanntmachung vom 10.
Februar 2008 (GVBl.
I S. 42) in Verbindung mit Artikel 1 des Gesetzes zu dem Staatsvertrag der Länder Berlin und Brandenburg über das Landesentwicklungsprogramm 2007 (LEPro 2007) und die Änderung des Landesplanungsvertrages vom 18.
Dezember 2007 (GVBl.
I S. 235)).

### Anlagen

1

[Festlegungskarte-1](/br2/sixcms/media.php/68/GVBl_II_24_2015-Anlage-Festlegungskarte-1.pdf "Festlegungskarte-1") 11.2 MB

2

[Festlegungskarte-2](/br2/sixcms/media.php/68/GVBl_II_24_2015-Anlage-Festlegungskarte-2.pdf "Festlegungskarte-2") 1.8 MB

3

[Landesentwicklungsplan-Berlin-Brandenburg](/br2/sixcms/media.php/68/GVBl_II_24_2015-Anlage-Landesentwicklungsplan-Berlin-Brandenburg.pdf "Landesentwicklungsplan-Berlin-Brandenburg") 3.5 MB