## Wahlordnung zum Brandenburgischen Richtergesetz (RiWO)

Auf Grund des § 90 Absatz 1 des Brandenburgischen Richtergesetzes vom 12.
Juli 2011 (GVBl. I Nr. 18) verordnet die Landesregierung:

Inhaltsübersicht
----------------

### Abschnitt 1  
Allgemeine Bestimmungen

[§ 1           Anwendungsbereich](#1)  
[§ 2           Wahlvorstand](#2)  
[§ 3           Wählerverzeichnis](#3)  
[§ 4           Einsprüche gegen das Wählerverzeichnis](#4)  
[§ 5           Wahlausschreiben](#5)  
[§ 6           Wahlvorschläge](#6)  
[§ 7           Behandlung der Wahlvorschläge durch den Wahlvorstand, ungültige Wahlvorschläge](#7)  
[§ 8           Bekanntgabe der Wahlvorschläge](#8)  
[§ 9           Sitzungsniederschrift](#9)  
[§ 10         Ausübung des Wahlrechts, Stimmzettel, ungültige Stimmabgaben](#010)  
[§ 11         Wahltag](#011)  
[§ 12         Wahlhandlung](#012)  
[§ 13         Briefwahl](#013)  
[§ 14         Behandlung im Wege der Briefwahl abgegebener Stimmen](#014)  
[§ 15         Ordnung im Wahlraum](#015)  
[§ 16         Auszählung der Stimmen](#016)  
[§ 17         Wahlniederschrift](#017)  
[§ 18         Benachrichtigung der gewählten Personen](#018)  
[§ 19         Bekanntmachung des Wahlergebnisses](#019)  
[§ 20         Bekanntgabe](#020)  
[§ 21         Berechnung der Fristen](#021)

### Abschnitt 2  
Wahl der dem Landtag für die Wahl zum Richterwahlausschuss vorzuschlagenden Personen

[§ 22         Wahlvorstand](#022)  
[§ 23         Bildung des Wahlvorstandes](#023)  
[§ 24         Wahlausschreiben](#024)  
[§ 25         Wahlvorschläge](#025)  
[§ 26         Zusammenstellung der Gesamtwahlvorschläge](#026)  
[§ 27         Bestimmung des Wahltages](#027)  
[§ 28         Wahlniederschrift](#028)  
[§ 29         Wahlergebnis](#029)  
[§ 30         Anzeige des Wahlergebnisses](#030)  
[§ 31         Aufbewahrung der Unterlagen](#031)  
[§ 32         Listen der dem Landtag für den Richterwahlausschuss vorzuschlagenden Personen](#032)

### Abschnitt 3  
Wahl der Richter- und Staatsanwaltsräte

[§ 33         Wahlvorstand](#033)  
[§ 34         Bildung des Wahlvorstandes](#034)  
[§ 35         Zusammensetzung der Richter- und Staatsanwaltsräte](#035)  
[§ 36         Ausübung des Wahlrechts](#036)  
[§ 37         Wahlvorschläge](#037)  
[§ 38         Behandlung der Wahlvorschläge durch den Wahlvorstand, ungültige Wahlvorschläge](#038)  
[§ 39         Nachfrist für die Einreichung von Wahlvorschlägen](#039)  
[§ 40         Bezeichnung der Wahlvorschläge](#040)  
[§ 41         Wahlverfahren bei Vorliegen mehrerer Wahlvorschläge (Verhältniswahl)](#041)  
[§ 42         Wahlverfahren bei Vorliegen eines Wahlvorschlages (Personen- und Mehrheitswahl)](#042)  
[§ 43         Wahl eines Richter- oder Staatsanwaltsrates, der aus einem Mitglied besteht (Personen- und Mehrheitswahl)](#043)  
[§ 44         Aufbewahrung der Wahlunterlagen](#044)

### Abschnitt 4  
Wahl der Gesamtrichterräte und des Gesamtstaatsanwaltsrates

[§ 45         Wahlvorstand, Wahltag](#045)  
[§ 46         Entsprechende Anwendung der Vorschriften über die Wahl der Richter- und Staatsanwaltsräte](#046)  
[§ 47         Gesamtwahlvorstand](#047)  
[§ 48         Wahlvorstand bei gesonderter Wahl des Gesamtrichter- oder Gesamtstaatsanwaltsrates](#048)  
[§ 49         Wahlausschreiben](#049)  
[§ 50         Wahlvorschläge, Stimmzettel](#050)  
[§ 51         Feststellung und Bekanntmachung des Wahlergebnisses](#051)

### Abschnitt 5  
Wahl der Präsidialräte

[§ 52         Wahlvorschläge, anzuwendende Bestimmungen](#052)

### Abschnitt 6  
Schlussvorschriften

[§ 53         Übergangsvorschrift](#053)  
[§ 54         Inkrafttreten, Außerkrafttreten](#054)

### Abschnitt 1   
Allgemeine Bestimmungen

#### [](#01)§ 1   
Anwendungsbereich

Auf die Wahlen der dem Landtag für die Wahl zum Richterwahlausschuss vorzuschlagenden Personen aus der Richterschaft und aus der Staatsanwaltschaft und auf die Wahlen der Richter- und Staatsanwaltsvertretungen sind die Vorschriften dieser Wahlordnung anzuwenden.

#### [](#02)§ 2  
Wahlvorstand

(1) Dem Wahlvorstand sollen Frauen und Männer angehören.

(2) Der Wahlvorstand gibt die Namen seiner Mitglieder und gegebenenfalls der Ersatzmitglieder unverzüglich nach seiner Bestellung oder Wahl in dem Gericht oder der Staatsanwaltschaft durch Aushang bis zum Abschluss der Stimmabgabe bekannt.
Der örtliche Wahlvorstand gibt die Namen der Mitglieder des Gesamtwahlvorstandes und die dienstliche Anschrift seiner oder seines Vorsitzenden durch Aushang bis zum Abschluss der Stimmabgabe bekannt.

(3) Der zuständige Wahlvorstand führt die Wahl durch.
Die Durchführung der Wahl der dem Landtag für die Wahl zum Richterwahlausschuss vorzuschlagenden Personen sowie der Wahlen zu den Gesamtrichterräten, zu dem Gesamtstaatsanwaltsrat und zu den Präsidialräten übernehmen in den einzelnen Gerichten oder Staatsanwaltschaften die örtlichen Wahlvorstände im Auftrag und nach Anordnung des Gesamtwahlvorstandes.

(4) Unverzüglich nach Abschluss der Wahl nimmt der örtliche Wahlvorstand öffentlich die Auszählung der Stimmen vor (§ 16), stellt deren Ergebnis in einer Niederschrift fest (§ 17) und gibt es den Angehörigen der Dienststelle durch Aushang bekannt.
Der Leitung des Gerichts oder der Staatsanwaltschaft und den dort vertretenen Berufsverbänden ist eine Abschrift der Niederschrift zu übersenden.

(5) Die Beschlüsse des Wahlvorstandes werden mit einfacher Stimmenmehrheit der anwesenden Mitglieder gefasst.
Stimmenthaltungen bleiben bei der Ermittlung der Mehrheit außer Betracht.
Bei Stimmengleichheit ist ein Antrag abgelehnt.

(6) Die Leitung des Gerichts oder der Staatsanwaltschaft hat den Wahlvorstand bei der Erfüllung seiner Aufgaben zu unterstützen, insbesondere die notwendigen Unterlagen zur Verfügung zu stellen und die erforderlichen Auskünfte zu erteilen.
Für die Vorbereitung und Durchführung der Wahl hat die Leitung des Gerichts oder der Staatsanwaltschaft im erforderlichen Umfang Räume, den Geschäftsbedarf und Schreibkräfte zur Verfügung zu stellen.

(7) Der örtliche Wahlvorstand kann Wahlberechtigte als Wahlhelferinnen und Wahlhelfer zu seiner Unterstützung bei der Durchführung der Stimmabgabe und bei der Stimmenzählung bestellen.

#### [](#03)§ 3  
Wählerverzeichnis

Die Stimmabgabe erfolgt nach einem alle Wahlberechtigten enthaltenden Wählerverzeichnis, das von dem örtlichen Wahlvorstand aufzustellen ist.
Er hat bis zum Abschluss der Stimmabgabe das Wählerverzeichnis auf dem Laufenden zu halten und kann offenbare Unrichtigkeiten berichtigen.
Das Wählerverzeichnis ist bis zum Abschluss der Stimmabgabe öffentlich zugänglich zu machen.

#### [](#04)§ 4  
Einsprüche gegen das Wählerverzeichnis

Einsprüche gegen die Richtigkeit des Wählerverzeichnisses müssen spätestens am Werktag vor Beginn der Stimmabgabe, 12 Uhr, beim örtlichen Wahlvorstand eingelegt werden.
Über den Einspruch entscheidet der Wahlvorstand unverzüglich.
Die Entscheidung ist der Person, die Einspruch eingelegt hat, unverzüglich, möglichst noch vor Beginn der Stimmabgabe, mitzuteilen.

#### [](#05)§ 5  
Wahlausschreiben

(1) Spätestens sechs Wochen vor dem Tag der Wahl erlässt der zuständige Wahlvorstand ein Wahlausschreiben.
Es ist von sämtlichen Mitgliedern des zuständigen Wahlvorstandes zu unterschreiben.
Das Wahlausschreiben muss enthalten:

1.  Ort und Tag seines Erlasses,
2.  die Grundsätze, nach denen die Wahl stattfindet (§ 88 Absatz 1 des Brandenburgischen Richtergesetzes), und den Hinweis, dass Frauen und Männer bei der Besetzung der zu wählenden Gremien angemessen berücksichtigt werden und zu diesem Zweck ebenso viele Frauen wie Männer vorgeschlagen werden sollen, sofern die Wahl eines Gremiums auf der Grundlage von Vorschlagslisten erfolgt (§ 88 Absatz 2 des Brandenburgischen Richtergesetzes),
3.  die Zahl der jeweils zu wählenden Richterinnen und Richter, Staatsanwältinnen und Staatsanwälte (§ 15 Absatz 1 Satz 2, § 34 Absatz 1 und 2, § 92 Absatz 4 Satz 2 des Brandenburgischen Richtergesetzes),
4.  den Hinweis, dass nur Wahlberechtigte wählen können, die in das Wählerverzeichnis eingetragen sind (§ 10 Absatz 1),
5.  den Hinweis, dass Einsprüche gegen das Wählerverzeichnis bis zum Werktag vor Beginn der Stimmabgabe, 12 Uhr, beim örtlichen Wahlvorstand eingelegt werden müssen (§ 4 Satz 1),
6.  die Mindestzahl von Personen, von denen ein Wahlvorschlag unterzeichnet sein muss (§ 25 Absatz 3, § 37 Absatz 3, §§ 46, 52), und den Hinweis, dass im Fall der Verhältniswahl jede oder jeder Wahlberechtigte nur auf einem Wahlvorschlag vorgeschlagen werden kann (§ 37 Absatz 7, §§ 46, 52),
7.  im Fall der Wahlen zu den Richter- und Staatsanwaltsräten den Hinweis, dass der Wahlvorschlag auch von einem Berufsverband der Richter- oder Staatsanwaltschaft eingebracht werden kann und von zwei beauftragten Wahlberechtigten unterzeichnet sein muss (§ 37 Absatz 2 und 4, § 46),
8.  die Aufforderung, Wahlvorschläge, die den Anforderungen des § 6 Absatz 1 entsprechen müssen, innerhalb von 18 Kalendertagen nach dem Erlass des Wahlausschreibens beim Wahlvorstand einzureichen (§ 6 Absatz 3); der letzte Tag der Einreichungsfrist ist anzugeben,
9.  den Hinweis, dass nur fristgerecht eingereichte Wahlvorschläge berücksichtigt werden können und nur gewählt werden kann, wer in einen solchen Wahlvorschlag aufgenommen ist,
10.  einen Hinweis auf die Möglichkeit der Briefwahl (§ 13),
11.  die Angabe, wo und wann das Wählerverzeichnis, die Wahlvorschläge und diese Wahlordnung eingesehen werden können,
12.  den Ort, an dem die Wahlvorschläge bekannt gegeben werden,
13.  den Ort und die Zeit der Stimmabgabe,
14.  den Ort und die Zeit der Stimmenauszählung und
15.  den Ort, an dem Einsprüche, Wahlvorschläge und andere Erklärungen gegenüber dem Wahlvorstand abzugeben sind.

(2) Offenbare Unrichtigkeiten des Wahlausschreibens können vom zuständigen Wahlvorstand jederzeit berichtigt werden.

(3) Der örtliche Wahlvorstand vermerkt auf dem Wahlausschreiben den ersten und letzten Tag des Aushangs.

(4) Mit dem Werktag, der auf den Erlass des Wahlausschreibens folgt, ist die Wahl eingeleitet.

(5) Der Wahlvorstand hat Abschriften oder einen Abdruck des Wahlausschreibens vom Tage des Erlasses an bis zum Schluss der Stimmabgabe öffentlich zugänglich zu machen.

#### [](#06)§ 6  
Wahlvorschläge

(1) Die Vorgeschlagenen sind mit Vornamen, Familiennamen, Geburtsdatum und Amtsbezeichnung aufzuführen.
Ihre schriftliche Zustimmung zur Aufnahme in den Wahlvorschlag ist beizufügen.
Die Zustimmung kann nicht widerrufen werden.

(2) Nach Einreichung des Wahlvorschlages kann eine darauf geleistete Unterschrift nicht mehr zurückgenommen werden.

(3) Die Wahlvorschläge sind innerhalb von 18 Kalendertagen nach dem Erlass des Wahlausschreibens beim Wahlvorstand einzureichen.

#### [](#07)§ 7  
Behandlung der Wahlvorschläge durch den Wahlvorstand, ungültige Wahlvorschläge

(1) Der Wahlvorstand prüft, ob die auf den Wahlvorschlägen benannten Bewerberinnen und Bewerber nach § 89 Absatz 2 und 3 des Brandenburgischen Richtergesetzes wählbar sind, und streicht diejenigen Personen, deren Nichtwählbarkeit festgestellt wird.
Von solchen Streichungen hat der Wahlvorstand die betroffenen Personen und die oder den zur Vertretung des Vorschlages Berechtigten (§ 37 Absatz 5) unverzüglich schriftlich zu benachrichtigen.

(2) Wahlvorschläge, die ungültig sind, weil sie nicht von der erforderlichen Zahl von Wahlberechtigten unterstützt oder weil sie nicht fristgerecht eingereicht worden sind oder weil sie nur Namen von nichtwählbaren Bewerberinnen und Bewerbern enthalten, gibt der Wahlvorstand unverzüglich nach Eingang unter Angabe der Gründe zurück.

(3) Wahlvorschläge, die den Vorschriften dieser Verordnung nicht entsprechen und nicht gemäß Absatz 2 ungültig sind, hat der Wahlvorstand mit der Aufforderung zurückzugeben, die Mängel innerhalb einer Frist von drei Werktagen zu beseitigen.
Werden die Mängel nicht fristgerecht beseitigt, so sind diese Wahlvorschläge ungültig.
Für den Fall der Verhältniswahl gilt dies nicht, wenn der Mangel nur einzelne Bewerberinnen oder Bewerber betrifft; diese Bewerberinnen oder Bewerber werden gestrichen.

#### [](#08)§ 8  
Bekanntgabe der Wahlvorschläge

(1) Unverzüglich nach Ablauf der in § 6 Absatz 3 genannten Frist oder der in § 26 Absatz 2 Satz 2 und 6 und § 39 Absatz 1 Satz 2 genannten Nachfrist, spätestens jedoch fünf Werktage vor Beginn der Stimmabgabe, gibt der Wahlvorstand die als gültig anerkannten Wahlvorschläge bis zum Abschluss der Stimmabgabe an denselben Stellen wie das Wahlausschreiben bekannt.
Dabei soll auch angegeben werden, wie viele Stimmen der oder die Wahlberechtigte hat.
Die Stimmzettel sollen zu diesem Zeitpunkt vorliegen.

(2) Die Namen der Personen, die den Wahlvorschlag unterzeichnet haben, werden nicht bekannt gegeben.

#### [](#09)§ 9  
Sitzungsniederschrift

Der Wahlvorstand fertigt über jede Sitzung, in der über Einsprüche gegen das Wählerverzeichnis (§ 4), über die Ermittlung der Zahl der zu wählenden Mitglieder des Richter- oder Staatsanwaltsrates (§ 35), über die Zulassung von Wahlvorschlägen (§ 7) und über das Setzen von Nachfristen (§ 26 Absatz 2 Satz 2 und 6 und § 39 Absatz 1 Satz 2) entschieden wird, eine Niederschrift.
Sie ist von sämtlichen Mitgliedern des Wahlvorstandes zu unterzeichnen.

#### [](#10)§ 10  
Ausübung des Wahlrechts, Stimmzettel, ungültige Stimmabgaben

(1) Wählen kann nur, wer in das Wählerverzeichnis eingetragen ist.

(2) Das Wahlrecht wird durch Abgabe eines Stimmzettels in einem Wahlumschlag ausgeübt.
Die Stimmzettel sind so zu gestalten, dass auch erkenntlich wird, für welche Wahl die Stimme abgegeben wird.

(3) Ist nach den Grundsätzen der Verhältniswahl zu wählen, so kann die Stimme nur für den gesamten Wahlvorschlag (Vorschlagsliste) abgegeben werden.
Ist nach den Grundsätzen der Mehrheitswahl zu wählen, so wird die Stimme für die zu wählenden einzelnen Bewerberinnen und Bewerber abgegeben.

(4) Ungültig sind Stimmzettel,

1.  die nicht vom Wahlvorstand ausgegeben worden sind,
2.  aus denen sich der Wille der oder des Wahlberechtigten nicht zweifelsfrei ergibt oder
3.  die ein besonderes Merkmal, einen Zusatz oder einen Vorbehalt enthalten.

#### [](#11)§ 11  
Wahltag

Die Wahlen finden an einem Werktag, der vom zuständigen Wahlvorstand (§§ 27, 33 Satz 2 und § 45 Absatz 2) zu bestimmen ist, bis um 15 Uhr statt.
Der örtliche Wahlvorstand kann bestimmen, dass die Wahlen zusätzlich an dem davor liegenden Werktag stattfinden.
Der Beginn der Wahlzeit wird von dem örtlichen Wahlvorstand bestimmt.

#### [](#12)§ 12  
Wahlhandlung

(1) Der Wahlvorstand trifft Vorkehrungen, dass die oder der Wahlberechtigte den Stimmzettel im Wahlraum unbeobachtet kennzeichnen und in den Wahlumschlag legen kann.
Für die Aufnahme der Wahlumschläge sind Wahlurnen zu verwenden.
Vor Beginn der Stimmabgabe sind die Wahlurnen vom Wahlvorstand zu verschließen.
Sie müssen so eingerichtet sein, dass die eingeworfenen Wahlumschläge nicht vor Öffnung der Urne entnommen werden können.

(2) Solange der Wahlraum zur Stimmabgabe geöffnet ist, müssen mindestens zwei Mitglieder des Wahlvorstandes im Wahlraum anwesend sein; sind Wahlhelferinnen oder Wahlhelfer bestellt, genügt die Anwesenheit eines Mitglieds des Wahlvorstandes und einer Wahlhelferin oder eines Wahlhelfers.

(3) Vor Aushändigung des Stimmzettels ist festzustellen, ob der oder die Wahlberechtigte im Wählerverzeichnis eingetragen ist.
Die Teilnahme an der Wahl ist im Wählerverzeichnis zu vermerken.

(4) Eine wahlberechtigte Person, die wegen einer körperlichen Beeinträchtigung in der Stimmabgabe behindert ist, kann eine Person ihres Vertrauens bestimmen, derer sie sich bei der Stimmabgabe bedienen will, und dies dem Wahlvorstand bekannt geben.
Die Hilfeleistung hat sich auf die Erfüllung der Wünsche der wahlberechtigten Person zur Stimmabgabe zu beschränken.
Die Vertrauensperson darf gemeinsam mit der wahlberechtigten Person die Wahlzelle aufsuchen, soweit dies zur Hilfeleistung erforderlich ist.
Die Vertrauensperson ist zur Geheimhaltung der Kenntnisse verpflichtet, die sie bei der Hilfeleistung erlangt hat.
Wahlbewerberinnen und Wahlbewerber dürfen nicht zur Hilfeleistung herangezogen werden.

(5) Wird die Wahlhandlung unterbrochen oder wird das Wahlergebnis nicht unmittelbar nach Abschluss der Stimmabgabe festgestellt, so hat der Wahlvorstand für die Zwischenzeit die Wahlurne so zu verschließen und aufzubewahren, dass der Einwurf oder die Entnahme von Stimmzetteln ohne Beschädigung des Verschlusses unmöglich ist.
Bei Wiedereröffnung der Wahl oder bei Entnahme der Stimmzettel zur Stimmzählung hat sich der Wahlvorstand davon zu überzeugen, dass der Verschluss unversehrt ist.

(6) Nach Ablauf der für die Durchführung der Wahlhandlung festgesetzten Zeit dürfen nur noch diejenigen Wahlberechtigen abstimmen, die sich in diesem Zeitpunkt im Wahlraum befinden.
Sodann erklärt der Wahlvorstand die Wahlhandlung für beendet.

(7) Der Wahlvorstand kann, soweit ein Bedürfnis vorliegt, im Bereich der Dienststelle verschiedene Wahlräume mit unterschiedlichen Abstimmungszeiten bestimmen.

#### [](#13)§ 13  
Briefwahl

(1) Wahlberechtigte, die ihre Stimme durch Briefwahl abgeben wollen, haben dies dem Wahlvorstand rechtzeitig mitzuteilen.
Der Wahlvorstand leitet ihnen die Stimmzettel und einen Wahlumschlag sowie einen größeren Freiumschlag zu, der die Anschrift des Wahlvorstandes und als Absender die Anschrift der wahlberechtigten Person sowie den Vermerk „Schriftliche Stimmabgabe“ mit dem Zusatz trägt, zu welcher Wahl die Stimme abgegeben wird.
Er übersendet zugleich eine vorgedruckte, von der wahlberechtigten Person abzugebende Erklärung, in der diese dem Wahlvorstand gegenüber versichert, dass sie den Stimmzettel persönlich gekennzeichnet hat.
Die Absendung ist in dem Wählerverzeichnis zu vermerken.
In einem besonderen Schreiben ist zugleich anzugeben, bis zu welchem Zeitpunkt spätestens der Stimmzettel bei dem Wahlvorstand eingegangen sein muss.

(2) Die wahlberechtigte Person gibt ihre Stimme in der Weise ab, dass sie

1.  den Stimmzettel unbeobachtet persönlich kennzeichnet und in den Wahlumschlag legt,
2.  die vorgedruckte Erklärung unter Angabe des Ortes und des Datums unterschreibt und
3.  den Wahlumschlag, in den der Stimmzettel gelegt ist, und die unterschriebene Erklärung (Absatz 1 Satz 3) in dem Freiumschlag verschließt und diesen so rechtzeitig an den Wahlvorstand absendet oder übergibt, dass er vor Abschluss der Stimmabgabe vorliegt.

Die wahlberechtigte Person kann, soweit unter den Voraussetzungen des § 12 Absatz 4 erforderlich, die in Satz 1 Nummer 1 bis 3 bezeichneten Tätigkeiten durch eine Person ihres Vertrauens verrichten lassen.

#### [](#14)§ 14  
Behandlung im Wege der Briefwahl abgegebener Stimmen

Während der Wahlzeit vermerkt ein Mitglied des Wahlvorstandes die Absenderinnen und Absender der bei dem Wahlvorstand eingegangenen Briefe in dem Wählerverzeichnis, entnimmt den Briefen die Wahlumschläge und legt diese ungeöffnet in die Wahlurne.
Die vorgedruckten Erklärungen sind zu den Wahlunterlagen zu nehmen.
Briefe, die ohne die vorgedruckte Erklärung bei dem Wahlvorstand eingehen, sind mit dem darin enthaltenen Wahlumschlag sowie mit einem entsprechenden Vermerk des Wahlvorstandes zu den Wahlunterlagen zu nehmen.
Nach Ablauf der Wahlzeit eingehende Briefe sind unter Vermerk des Eingangszeitpunktes ungeöffnet zu den Wahlunterlagen zu nehmen.
Die Briefumschläge sind einen Monat nach Bekanntgabe des Wahlergebnisses ungeöffnet zu vernichten, wenn die Wahl nicht angefochten worden ist.

#### [](#15)§ 15  
Ordnung im Wahlraum

Jegliche Wahlwerbung im Wahlraum ist unzulässig.
Das vorsitzende Mitglied des Wahlvorstandes, in seiner Abwesenheit das es vertretende Mitglied des Wahlvorstandes, kann jede Person aus dem Wahlraum verweisen, die hiergegen verstößt oder die Ruhe und Ordnung sowie die ordnungsgemäße Durchführung der Wahlhandlung stört.
Das vorsitzende Mitglied des Wahlvorstandes, in seiner Abwesenheit das von ihm betraute Mitglied des Wahlvorstandes, übt das Hausrecht im Sinne des § 123 des Strafgesetzbuches aus.

#### [](#16)§ 16  
Auszählung der Stimmen

(1) Unverzüglich nach Abschluss der Wahl nimmt der örtliche Wahlvorstand öffentlich die Auszählung der Stimmen vor.
Nach Öffnung der Wahlurne entnimmt er den Wahlumschlägen die Stimmzettel und prüft ihre Gültigkeit.

(2) Der örtliche sowie der jeweils zuständige Wahlvorstand zählen

1.  im Fall der Verhältniswahl die auf jede Vorschlagsliste,
2.  im Fall der Personen- und Mehrheitswahl die auf jede einzelne Bewerberin oder jeden einzelnen Bewerber

entfallenen gültigen Stimmen zusammen.

(3) Stimmzettel, über deren Gültigkeit oder Ungültigkeit der Wahlvorstand beschließt, weil sie zu Zweifeln Anlass geben, sind mit fortlaufender Nummer zu versehen und von den übrigen Stimmzetteln gesondert bei den Wahlunterlagen aufzubewahren.

#### [](#17)§ 17  
Wahlniederschrift

(1) Über das Ergebnis der Stimmenauszählung fertigen der örtliche und der jeweils zuständige Wahlvorstand eine Niederschrift an, die von sämtlichen Mitgliedern zu unterzeichnen ist.
Die Niederschrift muss enthalten:

1.  die Zahl der Wahlberechtigten,
2.  die Zahl der gültigen Stimmen,
3.  die Zahl der ungültigen Stimmen,
4.  im Fall der Verhältniswahl die auf jede Vorschlagsliste entfallenen gültigen Stimmen, im Fall der Personen- und Mehrheitswahl die Zahl der auf jede Bewerberin und jeden Bewerber entfallenen gültigen Stimmen,
5.  die für die Gültigkeit oder Ungültigkeit zweifelhafter Stimmen maßgebenden Gründe.

Stellt der Wahlvorstand auch das Wahlergebnis fest, so muss die Niederschrift zudem die Namen der gewählten Bewerberinnen und Bewerber und im Fall der Verhältniswahl die Errechnung der Höchstzahlen und ihre Verteilung auf die Vorschlagslisten enthalten.

(2) Besondere Vorkommnisse bei der Wahlhandlung oder der Feststellung des Wahlergebnisses sind in der Niederschrift zu vermerken.

#### [](#18)§ 18  
Benachrichtigung der gewählten Personen

Der für die Feststellung des Wahlergebnisses zuständige Wahlvorstand benachrichtigt die gewählten Personen unverzüglich schriftlich von ihrer Wahl und fordert sie auf, innerhalb von drei Kalendertagen zu erklären, ob sie die Wahl annehmen.
Wird die Wahl innerhalb dieser Frist nicht abgelehnt, so gilt sie als angenommen.
Nimmt eine Person die Wahl nicht an, so tritt an ihre Stelle im Fall der Personen- und Mehrheitswahl die Person mit der nächsthöchsten Stimmenzahl, im Fall der Verhältniswahl die ihr in der Vorschlagsliste nachfolgende Person.

#### [](#19)§ 19  
Bekanntmachung des Wahlergebnisses

Der für die Feststellung des Wahlergebnisses zuständige Wahlvorstand gibt das Wahlergebnis durch zweiwöchigen Aushang an den gleichen Stellen bekannt, an denen das Wahlausschreiben ausgehängt war.
Die öffentliche Bekanntmachung des Wahlergebnisses muss enthalten:

1.  die Namen der Personen, die ihre Wahl angenommen haben, im Fall der Verhältniswahl auch deren Zugehörigkeit zu einer Vorschlagsliste,
2.  die Zahl der Wahlberechtigten,
3.  die Zahl der Wählerinnen und Wähler,
4.  die Zahl der gültigen und ungültigen Stimmen,
5.  im Fall der Verhältniswahl die Verteilung der Stimmen auf die Vorschlagslisten und im Fall der Personen- und Mehrheitswahl die Verteilung der Stimmen auf die Bewerberinnen und Bewerber sowie
6.  im Fall der Wahl der Richter- und Staatsanwaltsvertretungen die Namen und die Reihenfolge der als Ersatzmitglieder gewählten Bewerberinnen und Bewerber.

#### [](#20)§ 20  
Bekanntgabe

Soweit in dieser Verordnung vorgesehen ist, dass Mitteilungen bekannt zu machen oder Unterlagen öffentlich zugänglich zu machen sind, sind die Mitteilungen und Unterlagen in jedem Gericht oder in jeder Staatsanwaltschaft, ist das Gericht oder die Staatsanwaltschaft in mehreren Gebäuden untergebracht, in jedem Gebäude an geeigneter Stelle auszuhängen oder zur Einsicht auszulegen.

#### [](#21)§ 21  
Berechnung der Fristen

Für die Berechnung der in dieser Verordnung festgelegten Fristen finden die §§ 186 bis 193 des Bürgerlichen Gesetzbuches entsprechende Anwendung.
Werktage im Sinne dieser Verordnung sind die Wochentage Montag bis Freitag mit Ausnahme der gesetzlichen Feiertage.

### Abschnitt 2  
Wahl der dem Landtag für die Wahl zum Richterwahlausschuss vorzuschlagenden Personen

#### [](#22)§ 22  
Wahlvorstand

Die Wahl zur Vorschlagsliste für die ständigen Mitglieder des Richterwahlausschusses (§ 12 Absatz 1 Satz 1 Nummer 2 des Brandenburgischen Richtergesetzes) führt der Landeswahlvorstand durch.
Die Wahl zur Vorschlagsliste für das nichtständige Mitglied des Richterwahlausschusses (§ 12 Absatz 1 Satz 2 des Brandenburgischen Richtergesetzes) führt der jeweilige Gesamtwahlvorstand durch.

#### [](#23)§ 23  
Bildung des Wahlvorstandes

(1) Spätestens zwei Monate vor dem Termin zur Neuwahl des Landtages bestimmen die Präsidentinnen und Präsidenten der oberen Landesgerichte für ihren Gerichtszweig sowie die Generalstaatsanwältin oder der Generalstaatsanwalt des Landes Brandenburg für die Staatsanwaltschaften, durch welche Gerichtsvorstände oder Staatsanwaltsleitungen und für welche Wahlorte Wahlvorstände zu bilden sind.
Sie bestimmen zugleich, welcher Wahlvorstand Gesamtwahlvorstand ist.
Ist in einem Gerichtszweig nur ein Wahlvorstand zu bilden, so ist dieser zugleich Gesamtwahlvorstand.

(2) Die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts bestimmt den Landeswahlvorstand.
Dieser soll aus je einer Richterin oder einem Richter aus der ordentlichen Gerichtsbarkeit, der Verwaltungs-, Finanz-, Arbeits- und Sozialgerichtsbarkeit bestehen.

(3) Die Gerichtsvorstände und Staatsanwaltsleitungen bestellen drei Wahlberechtigte zum Wahlvorstand sowie mindestens ein Ersatzmitglied; bestellen sie mehrere Ersatzmitglieder, legen sie zugleich fest, in welcher Reihenfolge diese bei Verhinderung oder Ausscheiden von Mitgliedern des Wahlvorstandes nachrücken.

#### [](#04)§ 24  
Wahlausschreiben

Der Gesamtwahlvorstand erlässt das Wahlausschreiben für die Wahl der Personen, die als nichtständiges Mitglied des Richterwahlausschusses dem Landtag vorgeschlagen werden sollen, der Landeswahlvorstand erlässt das Wahlausschreiben für die Wahl der Personen, die als ständige Mitglieder des Richterwahlausschusses dem Landtag vorgeschlagen werden sollen.
Das Wahlausschreiben muss die Angaben nach § 5 Absatz 1 Satz 3 Nummer 1 bis 10 enthalten; der örtliche Wahlvorstand ergänzt das Wahlausschreiben durch die Angaben nach § 5 Absatz 1 Satz 3 Nummer 11 bis 15.

#### [](#25)§ 25  
Wahlvorschläge

(1) Für die Vorschlagslisten nach § 12 Absatz 1 Satz 1 Nummer 2 und Satz 2 des Brandenburgischen Richtergesetzes sind getrennte Wahlvorschläge einzureichen.

(2) Die Wahlvorschläge für die Wahl des nichtständigen Mitglieds sind bei dem jeweils zuständigen Gesamtwahlvorstand, die Wahlvorschläge für die Wahl der ständigen Mitglieder aus der gesamten Richterschaft sind bei dem Landeswahlvorstand einzureichen.

(3) Jeder Wahlvorschlag ist von mindestens drei Wahlberechtigten zu unterschreiben.

#### [](#26)§ 26  
Zusammenstellung der Gesamtwahlvorschläge

(1) Der Gesamtwahlvorstand stellt für seinen Gerichtszweig oder für die Staatsanwaltschaften die Namen der für die Wahlen nach § 12 Absatz 1 Satz 2 des Brandenburgischen Richtergesetzes Vorgeschlagenen zu einem Gesamtwahlvorschlag in alphabetischer Reihenfolge zusammen.
Die Wahlvorschläge für die Vorschlagsliste nach § 12 Absatz 1 Satz 1 Nummer 2 des Brandenburgischen Richtergesetzes stellt der Landeswahlvorstand in einem Gesamtwahlvorschlag zusammen.

(2) Die Gesamtwahlvorschläge sollen die Namen von mindestens doppelt so vielen wählbaren Personen enthalten, wie dem Landtag nach § 15 Absatz 1 Satz 2 des Brandenburgischen Richtergesetzes vorzuschlagen sind.
Werden weniger wählbare Personen vorgeschlagen als nach § 15 Absatz 1 Satz 2 des Brandenburgischen Richtergesetzes erforderlich sind, so setzt der Gesamtwahlvorstand, im Fall des Absatzes 1 Satz 2 der Landeswahlvorstand, eine Nachfrist von einer Woche.
Die Nachfrist ist bekannt zu machen.
Wird auch innerhalb der Nachfrist nicht die erforderliche Anzahl von Personen benannt, so schlägt die Präsidentin oder der Präsident des oberen Landesgerichts oder die Generalstaatsanwältin oder der Generalstaatsanwalt des Landes Brandenburg, im Fall des Absatzes 1 Satz 2 die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts, die fehlende Anzahl von Personen mit deren Einverständnis vor.
Es sollen ebenso viele Frauen wie Männer in den Gesamtwahlvorschlägen enthalten sein.
Ist nicht gewährleistet, dass für jedes vom Landtag zu wählende Mitglied mindestens zwei Männer und zwei Frauen vorgeschlagen werden können, so gelten die Sätze 2 bis 4 entsprechend.

#### [](#27)§ 27  
Bestimmung des Wahltages

Der Landeswahlvorstand bestimmt den Wahltag.

#### [](#28)§ 28  
Wahlniederschrift

Die örtlichen Wahlvorstände haben gesonderte Niederschriften über das Ergebnis der Stimmenauszählung für die Wahl der Personen zu erstellen, die als ständige Mitglieder des Richterwahlausschusses vorgeschlagen werden sollen, und über das Ergebnis der Stimmenauszählung für die Wahl der Personen, die als nichtständiges Mitglied des Richterwahlausschusses vorgeschlagen werden sollen.
Die Niederschrift hinsichtlich der Wahl der ständigen Mitglieder ist unverzüglich dem Landeswahlvorstand, die Niederschrift hinsichtlich der Wahl des nichtständigen Mitglieds ist unverzüglich dem Gesamtwahlvorstand zu übersenden.
Die abgegebenen Stimmzettel sind der jeweiligen Niederschrift als Anlage beizufügen.

#### [](#29)§ 29  
Wahlergebnis

(1) Dem Landtag sind für die Wahl zum Richterwahlausschuss diejenigen acht Richterinnen und Richter aus der gesamten Richterschaft und diejenigen vier Richterinnen und Richter aus dem jeweiligen Gerichtszweig sowie diejenigen vier Staatsanwältinnen und Staatsanwälte vorzuschlagen, welche die meisten Stimmen erhalten haben.
Ist bei mehreren Personen die Stimmenzahl gleich, so ist jede von ihnen in die Vorschlagsliste gewählt.

(2) Der Gesamtwahlvorstand stellt anhand der ihm übersandten Niederschriften (§ 28) fest, welche Personen in die Vorschlagsliste für die Wahl des nichtständigen Mitglieds für die jeweilige Gerichtsbarkeit oder die Staatsanwaltschaft aufzunehmen sind, und benachrichtigt die gewählten Personen.

(3) Der Landeswahlvorstand stellt anhand der ihm übersandten Niederschriften (§ 28) fest, welche Personen in die Vorschlagsliste für die Wahl der beiden ständigen Mitglieder aufzunehmen sind, und benachrichtigt die gewählten Personen.

#### [](#30)§ 30  
Anzeige des Wahlergebnisses

Der Gesamtwahlvorstand hat die Namen der Personen, die ihre Wahl angenommen haben, der Präsidentin oder dem Präsidenten des oberen Landesgerichts oder der Generalstaatsanwältin oder dem Generalstaatsanwalt des Landes Brandenburg anzuzeigen.
Der Landeswahlvorstand zeigt die Namen der in seiner Zuständigkeit ermittelten Personen der Präsidentin oder dem Präsidenten des Brandenburgischen Oberlandesgerichts an.

#### [](#31)§ 31  
Aufbewahrung der Unterlagen

Der Gesamtwahlvorstand übersendet die Niederschriften der Präsidentin oder dem Präsidenten des oberen Landesgerichts oder der Generalstaatsanwältin oder dem Generalstaatsanwalt des Landes Brandenburg; der örtliche Wahlvorstand übersendet seine Unterlagen dem Gerichtsvorstand oder der Leitung der Staatsanwaltschaft, der Landeswahlvorstand der Präsidentin oder dem Präsidenten des Brandenburgischen Oberlandesgerichts.
Die Niederschriften und sonstigen Unterlagen sind mindestens bis zur nächsten Wahl aufzubewahren.

#### [](#32)§ 32  
Listen der dem Landtag für den Richterwahlausschuss vorzuschlagenden Personen

Die Präsidentinnen und Präsidenten der oberen Landesgerichte sowie die Generalstaatsanwältin oder der Generalstaatsanwalt des Landes Brandenburg senden die Listen der dem Landtag für die Wahl zum Richterwahlausschuss nach § 12 Absatz 1 Satz 1 Nummer 2 und Satz 2 des Brandenburgischen Richtergesetzes vorzuschlagenden Personen unter Angabe der Reihenfolge des Abstimmungsergebnisses und der auf die Personen entfallenen Stimmenzahl an das Ministerium der Justiz.
Dieses leitet die Vorschlagslisten an die Präsidentin oder den Präsidenten des Landtages weiter.

### Abschnitt 3  
Wahl der Richter- und Staatsanwaltsräte

#### [](#33)§ 33  
Wahlvorstand

Die Wahl der Richter- und Staatsanwaltsräte führt der örtliche Wahlvorstand durch.
In den Fällen der Neuwahl (§ 36 Absatz 1 des Brandenburgischen Richtergesetzes) bestimmt er den Wahltag.

#### [](#34)§ 34  
Bildung des Wahlvorstandes

(1) Im Fall des regelmäßigen Wahltermins (§ 28 Absatz 2 des Brandenburgischen Richtergesetzes) bestellt der Richter- oder Staatsanwaltsrat spätestens bis zum 1.
September drei Wahlberechtigte als Wahlvorstand und eine Person von ihnen als Vorsitzende oder Vorsitzenden.
Für jedes Mitglied soll ein Ersatzmitglied berufen werden.
In den Fällen der Neuwahl (§ 36 Absatz 1 des Brandenburgischen Richtergesetzes) bestellt der Richter- oder Staatsanwaltsrat den Wahlvorstand spätestens einen Monat, nachdem ein Fall des § 36 Absatz 1 Nummer 1 bis 3 des Brandenburgischen Richtergesetzes eingetreten ist.

(2) Besteht bei einem Gericht kein Richterrat oder bei einer Staatsanwaltschaft kein Staatsanwaltsrat, so beruft der Gerichtsvorstand eine Versammlung der Richterinnen und Richter oder die Leitung der Staatsanwaltschaft eine Versammlung der Staatsanwältinnen und Staatsanwälte zur Wahl des Wahlvorstandes ein.
Die Versammlung wählt eine Person, die die Versammlung leitet.
Dasselbe gilt, wenn der Richterrat oder der Staatsanwaltsrat bis zu dem in Absatz 1 Satz 1 oder Absatz 1 Satz 3 genannten Zeitpunkt noch keinen Wahlvorstand bestellt hat und eine Versammlung von mindestens drei Wahlberechtigten oder einem in dem Gericht oder der Staatsanwaltschaft vertretenen Berufsverband beantragt wird.

(3) Findet eine Versammlung nach Absatz 2 nicht statt oder wählt die Versammlung keinen Wahlvorstand, so bestellt ihn die Leitung des Gerichts oder der Staatsanwaltschaft auf Antrag von mindestens drei Wahlberechtigten oder eines in dem Gericht oder der Staatsanwaltschaft vertretenen Berufsverbandes.

#### [](#35)§ 35  
Zusammensetzung der Richter- und Staatsanwaltsräte

Der Wahlvorstand ermittelt die Zahl der zu wählenden Mitglieder des Richter- oder Staatsanwaltsrates (§ 34 Absatz 1 und § 92 Absatz 3 Satz 1 des Brandenburgischen Richtergesetzes).

#### [](#36)§ 36  
Ausübung des Wahlrechts

(1) Die Richterinnen und Richter auf Lebenszeit und auf Zeit üben ihr Wahlrecht bei dem Gericht aus, bei dem ihnen das Richteramt übertragen worden ist.
Die Richterinnen und Richter auf Probe und kraft Auftrags üben ihr Wahlrecht bei dem Gericht aus, bei dem sie beschäftigt sind.

(2) Eine Richterin oder ein Richter verliert das Wahlrecht nicht dadurch, dass sie oder er an ein anderes Gericht abgeordnet wird.
Hat eine Abordnung an ein anderes Gericht des Landes Brandenburg länger als sechs Monate gedauert, so wird das Wahlrecht bei dem Gericht ausgeübt, an das die Richterin oder der Richter abgeordnet ist.

(3) Für die Ausübung des Wahlrechts der Staatsanwältinnen und Staatsanwälte gelten die Absätze 1 und 2 entsprechend.

#### [](#37)§ 37  
Wahlvorschläge

(1) Die Wahlvorschläge sollen Frauen und Männer angemessen berücksichtigen.

(2) Wahlvorschläge können die Wahlberechtigten und die in dem Gericht oder in der Staatsanwaltschaft vertretenen Berufsverbände der Richterschaft und der Staatsanwaltschaft machen.

(3) Bei Gerichten und Staatsanwaltschaften mit bis zu 20 Wahlberechtigten kann jede wahlberechtigte Person einen Wahlvorschlag unterbreiten.
Bei Gerichten und Staatsanwaltschaften mit mehr als 20 Wahlberechtigten müssen die Wahlvorschläge von mindestens einem Zwanzigstel der Wahlberechtigten unterzeichnet sein.
Bruchteile eines Zwanzigstels werden auf ein volles Zwanzigstel aufgerundet.
In jedem Fall genügt die Unterzeichnung durch zehn Wahlberechtigte.

(4) Jeder in dem Gericht oder in der Staatsanwaltschaft vertretene Berufsverband kann nur einen Wahlvorschlag machen, der von zwei beauftragten Wahlberechtigten unterzeichnet sein muss.

(5) Aus dem Wahlvorschlag soll hervorgehen, welche der unterzeichnenden Personen zur Vertretung des Vorschlages gegenüber dem Wahlvorstand und zur Entgegennahme von Erklärungen und Entscheidungen des Wahlvorstandes berechtigt ist.
Fehlt eine Angabe dazu, gilt die Person als berechtigt, die an erster Stelle steht.
Ist der Wahlvorschlag von einem Berufsverband eingereicht worden, so ist dieser zur Vertretung seines Vorschlages gegenüber dem Wahlvorstand und zur Entgegennahme von Erklärungen und Entscheidungen des Wahlvorstandes berechtigt; er kann auf dem Wahlvorschlag auch Personen benennen, die an seiner Stelle hierzu berechtigt sind.

(6) Die nach § 89 Absatz 3 des Brandenburgischen Richtergesetzes nicht wählbaren Personen dürfen keine Wahlvorschläge einreichen oder unterzeichnen.

(7) Jede Bewerberin und jeder Bewerber kann für die Wahl nur auf einem Wahlvorschlag vorgeschlagen werden.

(8) Die Namen der einzelnen Bewerberinnen und Bewerber sind auf dem Wahlvorschlag untereinander aufzuführen und mit fortlaufenden Nummern zu versehen.

(9) Der Wahlvorschlag soll mit einem Kennwort versehen werden.

(10) Eine Verbindung von Wahlvorschlägen ist unzulässig.

#### [](#38)§ 38  
Behandlung der Wahlvorschläge durch den Wahlvorstand, ungültige Wahlvorschläge

(1) Der Wahlvorstand vermerkt auf den Wahlvorschlägen das Datum und die Uhrzeit des Eingangs.
Im Fall des § 7 Absatz 3 ist auch der Zeitpunkt des Eingangs des berichtigten Wahlvorschlages zu vermerken.

(2) Der Wahlvorstand hat eine Bewerberin oder einen Bewerber, die oder der mit ihrer oder seiner schriftlichen Zustimmung auf mehreren Wahlvorschlägen benannt ist, aufzufordern, innerhalb von drei Werktagen zu erklären, auf welchem Wahlvorschlag sie oder er benannt bleiben will.
Wird diese Erklärung nicht fristgerecht abgegeben, so wird die Bewerberin oder der Bewerber von sämtlichen Wahlvorschlägen gestrichen.

#### [](#39)§ 39  
Nachfrist für die Einreichung von Wahlvorschlägen

(1) Ist nach Ablauf der in § 6 Absatz 3 genannten Frist überhaupt kein gültiger Wahlvorschlag eingegangen, so gibt der Wahlvorstand dies sofort durch Aushang an denselben Stellen, an denen das Wahlausschreiben ausgehängt ist, bekannt.
Gleichzeitig fordert er zur Einreichung von Wahlvorschlägen innerhalb einer Nachfrist von fünf Kalendertagen auf.

(2) Der Wahlvorstand weist darauf hin, dass der Richter- oder Staatsanwaltsrat nicht gewählt werden kann, wenn auch innerhalb der Nachfrist kein gültiger Wahlvorschlag eingeht.

(3) Gehen auch innerhalb der Nachfrist keine gültigen Wahlvorschläge ein, so gibt der Wahlvorstand sofort bekannt, dass diese Wahl nicht stattfinden kann und dass das Amt des Wahlvorstandes erloschen ist.

#### [](#40)§ 40  
Bezeichnung der Wahlvorschläge

(1) Der Wahlvorstand versieht die Wahlvorschläge in der Reihenfolge ihres Eingangs mit Ordnungsnummern (Vorschlag 1 usw.).
Die vergebenen Ordnungsnummern bestimmen die Reihenfolge der Wahlvorschläge auf dem Stimmzettel.
Wahlvorschläge, die vor Beginn der Einreichungsfrist (§ 6 Absatz 3) beim Wahlvorstand eingehen, gelten als mit Beginn dieser Frist eingegangen.
Ist ein Wahlvorschlag berichtigt worden, so ist der Zeitpunkt des Eingangs des berichtigten Wahlvorschlages maßgebend.
Sind mehrere Wahlvorschläge gleichzeitig eingegangen, so entscheidet das Los über die Reihenfolge auf dem Stimmzettel.

(2) Der Wahlvorstand bezeichnet die Wahlvorschläge mit dem Familien- und Vornamen der Bewerberin oder des Bewerbers, die oder der an erster Stelle benannt ist.
Bei Wahlvorschlägen, die mit einem Kennwort versehen sind, ist auch das Kennwort anzugeben.

#### [](#41)§ 41  
Wahlverfahren bei Vorliegen mehrerer Wahlvorschläge (Verhältniswahl)

(1) Nach den Grundsätzen der Verhältniswahl ist zu wählen, wenn mehrere gültige Wahlvorschläge eingegangen sind.
In diesen Fällen kann jede wahlberechtigte Person ihre Stimme nur für den gesamten Wahlvorschlag (Vorschlagsliste) abgeben.

(2) Auf den Stimmzetteln sind die Vorschlagslisten in der Reihenfolge der Ordnungsnummern unter Angabe von Familienname, Vorname und Amtsbezeichnung der Bewerberinnen und Bewerber untereinander aufzuführen; bei Vorschlagslisten, die mit einem Kennwort versehen sind, ist auch das Kennwort anzugeben.

(3) Die wahlberechtigte Person hat auf dem Stimmzettel die Vorschlagsliste anzukreuzen, für die sie ihre Stimme abgeben will.

(4) Bei der Verhältniswahl werden die Summen der auf die einzelnen Vorschlagslisten entfallenen Stimmen nebeneinandergestellt und der Reihe nach durch 1, 2, 3 usw.
geteilt.
Auf die jeweils höchste Teilzahl (Höchstzahl) wird so lange ein Sitz zugeteilt, bis alle Sitze verteilt sind.
Ist bei gleichen Höchstzahlen nur noch ein Sitz oder sind bei drei gleichen Höchstzahlen nur noch zwei Sitze zu verteilen, so entscheidet das Los.

(5) Enthält eine Vorschlagsliste weniger Bewerberinnen und Bewerber als ihr nach den Höchstzahlen Sitze zustehen würden, so fallen die überschüssigen Sitze den übrigen Vorschlagslisten in der Reihenfolge der nächsten Höchstzahlen zu.

(6) Innerhalb der Vorschlagslisten werden die Sitze auf die Bewerberinnen und Bewerber in der Reihenfolge ihrer Benennungen (§ 37 Absatz 8) verteilt.

#### [](#42)§ 42  
Wahlverfahren bei Vorliegen eines Wahlvorschlages (Personen- und Mehrheitswahl)

(1) Nach den Grundsätzen der Personen- und Mehrheitswahl ist zu wählen, wenn nur ein gültiger Wahlvorschlag eingegangen ist.
Die Wahlberechtigten dürfen nur solche Bewerberinnen und Bewerber wählen, die in dem Wahlvorschlag aufgeführt sind.

(2) Auf dem Stimmzettel werden die Bewerberinnen und Bewerber aus dem Wahlvorschlag in unveränderter Reihenfolge unter Angabe von Familienname, Vorname, Amtsbezeichnung und Kennwort aufgeführt.
Die wahlberechtigte Person kreuzt auf dem Stimmzettel die Namen der Bewerberinnen und Bewerber an, für die sie ihre Stimme abgeben will.
Sie darf nicht mehr Namen ankreuzen, als Mitglieder des Richter- oder Staatsanwaltsrates zu wählen sind.

(3) Die Bewerberinnen und Bewerber sind in der Reihenfolge der jeweils höchsten auf sie entfallenden Stimmenzahl gewählt.
Bei gleicher Stimmenzahl entscheidet das Los.

#### [](#43)§ 43  
Wahl eines Richter- oder Staatsanwaltsrates, der aus einem Mitglied besteht (Personen- und Mehrheitswahl)

(1) Nach den Grundsätzen der Personen- und Mehrheitswahl ist zu wählen, wenn nur ein Mitglied des Richter- oder Staatsanwaltsrates zu wählen ist.

(2) Auf dem Stimmzettel werden die Bewerberinnen und Bewerber aus den Wahlvorschlägen in alphabetischer Reihenfolge unter Angabe des Familiennamens, des Vornamens und der Amtsbezeichnung aufgeführt.

(3) Die wahlberechtigte Person hat auf dem Stimmzettel den Namen der Bewerberin oder des Bewerbers anzukreuzen, für die oder den sie die Stimme abgeben will.

(4) Gewählt ist die Bewerberin oder der Bewerber, die oder der die meisten Stimmen erhält.
Bei gleicher Stimmenzahl entscheidet das Los.

#### [](#44)§ 44  
Aufbewahrung der Wahlunterlagen

Die Wahlunterlagen (Niederschriften, Bekanntmachungen, Stimmzettel, Freiumschläge für die schriftliche Stimmabgabe usw.) werden vom Richter- oder Staatsanwaltsrat bis zum Abschluss der nächsten Wahl aufbewahrt; sie sollen dann vernichtet werden.

### Abschnitt 4  
Wahl der Gesamtrichterräte und des Gesamtstaatsanwaltsrates

#### [](#45)§ 45  
Wahlvorstand, Wahltag

(1) Die Wahl der Gesamtrichterräte und des Gesamtstaatsanwaltsrates führt der jeweilige Gesamtwahlvorstand durch.

(2) Der bei dem Brandenburgischen Oberlandesgericht gebildete Gesamtwahlvorstand bestimmt den Wahltag für die regelmäßig durchzuführenden Wahlen der Richtervertretungen (§ 28 Absatz 2 des Brandenburgischen Richtergesetzes) im Benehmen mit den übrigen Gesamtwahlvorständen.

#### [](#46)§ 46  
Entsprechende Anwendung der Vorschriften über die Wahl der Richter- und Staatsanwaltsräte

Für die Wahl der Gesamtrichterräte und des Gesamtstaatsanwaltsrates gelten die Vorschriften des Abschnitts 3 entsprechend, soweit sich aus den folgenden Vorschriften nichts anderes ergibt.

#### [](#47)§ 47  
Gesamtwahlvorstand

Der Gesamtwahlvorstand wird, wenn ein Gesamtrichter- oder Gesamtstaatsanwaltsrat nicht besteht, von den Richterräten des Gerichtszweiges oder den Staatsanwaltsräten gewählt.
Findet eine Wahl nicht statt oder bestehen in dem Gerichtszweig keine Richterräte oder in den Staatsanwaltschaften keine Staatsanwaltsräte, so bestellt die Präsidentin oder der Präsident des oberen Landesgerichts oder die Generalstaatsanwältin oder der Generalstaatsanwalt des Landes Brandenburg den Gesamtwahlvorstand.

#### [](#48)§ 48  
Wahlvorstand bei gesonderter Wahl des Gesamtrichter- oder Gesamtstaatsanwaltsrates

Wird der Gesamtrichter- oder Gesamtstaatsanwaltsrat nicht gleichzeitig mit den Richter- oder Staatsanwaltsräten gewählt, wird der örtliche Wahlvorstand auf Ersuchen des Gesamtwahlvorstandes vom Richter- oder Staatsanwaltsrat oder, wenn ein solcher nicht besteht, von der Leitung des Gerichts oder der Staatsanwaltschaft bestellt.

#### [](#49)§ 49  
Wahlausschreiben

(1) Der Gesamtwahlvorstand erlässt das Wahlausschreiben.
Das Wahlausschreiben muss die Angaben nach § 5 Absatz 1 Satz 3 Nummer 1 bis 10 enthalten; der örtliche Wahlvorstand ergänzt das Wahlausschreiben durch die Angaben nach § 5 Absatz 1 Satz 3 Nummer 11 bis 15.

(2) Der örtliche Wahlvorstand macht das Wahlausschreiben in der Dienststelle bis zum Abschluss der Stimmabgabe öffentlich zugänglich.

#### [](#50)§ 50  
Wahlvorschläge, Stimmzettel

Auf den Wahlvorschlägen und den Stimmzetteln für die Wahl des Gesamtrichter- oder Gesamtstaatsanwaltsrates ist auch die Dienststelle der Bewerberin oder des Bewerbers aufzuführen.

#### [](#51)§ 51  
Feststellung und Bekanntmachung des Wahlergebnisses

(1) Der Gesamtwahlvorstand zählt unverzüglich die auf jede Vorschlagsliste oder, wenn Mehrheitswahl stattgefunden hat, die auf jede einzelne Bewerberin und jeden einzelnen Bewerber entfallenen Stimmen zusammen und stellt das Ergebnis der Wahl fest.

(2) Sobald die Namen der als Mitglieder des Gesamtrichter- oder Gesamtstaatsanwaltsrates gewählten Bewerberinnen und Bewerber feststehen, teilt der Gesamtwahlvorstand sie den örtlichen Wahlvorständen mit.
Die örtlichen Wahlvorstände geben sie durch zweiwöchigen Aushang an den gleichen Stellen wie das Wahlausschreiben bekannt.

### Abschnitt 5  
Wahl der Präsidialräte

#### [](#52)§ 52  
Wahlvorschläge, anzuwendende Bestimmungen

Die für die Wahlen zu den Gesamtrichterräten zuständigen Gesamtwahlvorstände führen auch die Wahlen zu den Präsidialräten durch.
Im Übrigen finden die Vorschriften über die Wahl der Gesamtrichterräte entsprechende Anwendung.

### Abschnitt 6  
Schlussvorschriften

#### [](#53)§ 53  
Übergangsvorschrift

(1) Hinsichtlich der Wahl zu der Vorschlagsliste für die Wahl eines nichtständigen Mitglieds aus der Staatsanwaltschaft (§ 101 Satz 2 des Brandenburgischen Richtergesetzes) gilt § 23 Absatz 1 mit der Maßgabe, dass die Wahlvorstände spätestens zwei Wochen nach Inkrafttreten dieser Verordnung zu bilden sind.
Die Bestimmung des Wahltags obliegt dem Gesamtwahlvorstand.

(2) Hinsichtlich der Wahlen der Richter- und Staatsanwaltsvertretungen im Jahr des Inkrafttretens des Brandenburgischen Richtergesetzes vom 12.
Juli 2011 (GVBl.
I Nr. 18) gilt diese Verordnung mit der Maßgabe, dass die Frist nach § 5 Absatz 1 Satz 1 fünf Wochen beträgt und abweichend von § 34 Absatz 1 der Wahlvorstand spätestens zwei Wochen nach Inkrafttreten dieser Verordnung zu bilden ist.

#### [](#54)§ 54  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Richterwahlausschuss-Vorschlagsverordnung vom 16.
Juni 1993 (GVBl.
II S. 264) außer Kraft.

Potsdam, den 22.
September 2011

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident  
Matthias Platzeck

Der Minister der Justiz  
Dr.
Volkmar Schöneburg