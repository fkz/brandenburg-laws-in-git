## Verordnung über das Naturschutzgebiet „Boitzenburger Tiergarten und Strom“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S.
2542), von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl.
I S.
1972) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 und § 42 Absatz 2 Satz 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 1. Januar 2013 (GVBl.
I Nr.
3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27. Mai 2013 (GVBl.
II Nr.
43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Uckermark wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Boitzenburger Tiergarten und Strom“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 1 215 Hektar.
Es umfasst Flächen in folgenden Fluren:

Gemeinde/Stadt:

Gemarkung:

Flur:

Boitzenburger Land

Berkholz

1 bis 3;

 

Boitzenburg

4, 6;

 

Wichmannsdorf

5, 6;

Nordwestuckermark

Gollmitz

1 bis 4, 6;

 

Groß-Sperrenwalde

4;

 

Klein-Sperrenwalde

1;

 

Kröchlendorff

1;

 

Thiesort-Mühle

1;

 

Horst

1;

Prenzlau

Prenzlau

29, 30;

 

Güstow

2.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte Übersichtskarte im Maßstab 1 : 50 000 dient der räumlichen Einordnung des Naturschutzgebietes.
Die in Anlage 2 Nummer 2 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 4 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 3 mit den Blattnummern 1 bis 22 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Innerhalb des Naturschutzgebietes wird eine als Naturentwicklungsgebiet bezeichnete Zone 1 festgesetzt, die der direkten menschlichen Einflussnahme entzogen ist und in der Lebensräume und Lebensgemeinschaften langfristig ihrer natürlichen Entwicklung überlassen bleiben.
Die Zone 1 umfasst rund 46 Hektar und liegt in folgenden Fluren:

Gemeinde/Stadt:

Gemarkung:

Flur:

Boitzenburger Land

Berkholz

1;

Nordwestuckermark

Gollmitz

3;

 

Kröchlendorff

1;

 

Klein-Sperrenwalde

1.

Die Grenze der Zone 1 ist in der in Anlage 2 Nummer 1 genannten Übersichtskarte, in der in Anlage 2 Nummer 2 genannten topografischen Karte mit der Blattnummer 3 und in den in Anlage 2 Nummer 3 genannten Liegenschaftskarten mit den Blattnummern 6, 12 und 13 mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Uckermark, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Erlenbruchwälder und Moorgehölze, der Braunmoos-, Seggen- und Röhrichtmoore, der Quellmoore, des Grünlandes trockener bis nasser Ausprägung sowie der Grundrasen-, Schwimmblatt- und Tauchflurengesellschaften eutropher Seen;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Breitblättriges Knabenkraut (Dactylorhiza majalis), Steifblättriges Knabenkraut (Dactylorhiza incarnata), Trollblume (Trollius europaeus), Zungenhahnenfuß (Ranunculus lingua), Fieberklee (Menyanthes trifoliata), Krebsschere (Stratiotes aloides) und Sandstrohblume (Helichrysum arenarium);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- beziehungsweise Rückzugsraum und potenzielles Wiederausbreitungszentrum wild lebender Tierarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Schellente (Bucephala clangula), Fransenfledermaus (Myotis nattereri), Wasserspitzmaus (Neomys fodiens), Laubfrosch (Hyla arborea), Knoblauchkröte (Pelobates fuscus), Großer Schillerfalter (Apatura iris), Lilagold-Feuerfalter (Lycaena hippothoe), Gemeines Grünwidderchen (Adscita statices), Violetter Feuerfalter (Lycaena alciphron), Gebänderte Prachtlibelle (Calopteryx splendens) und Schmerle (Barbatula barbatula);
4.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit des Gebietes, geprägt durch das naturnahe Fließgewässer „Strom“ mit angrenzenden Biotopkomplexen, wie Bruchwäldern, Eichenhutewäldern, Mooren, Kleingewässern und Teichen sowie extensiv bewirtschafteten Wiesen- und Weideflächen;
5.  die Erhaltung und Entwicklung des Gebietes als überregionaler Biotopverbund zwischen dem Naturschutzgebiet „Jungfernheide“ und der Uckerniederung.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Boitzenburger Tiergarten und Strom“ (ehemals ein Teil des Gebietes von gemeinschaftlicher Bedeutung „Stromgewässer“) (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinen Vorkommen von

1.  Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Flüssen der planaren bis montanen Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Pfeifengraswiesen auf kalkreichem Boden (Molinion caeruleae), Naturnahen Kalk-Trockenrasen und deren Verbuschungsstadien (Festuco-Brometalia), Kalkreichen Niedermooren, Feuchten Hochstaudenfluren, Waldmeister-Buchenwald (Asperulo-Fagetum) sowie Alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur als natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Auenwäldern mit Alnus glutinosa und Fraxinus excelsior als prioritärem natürlichen Lebensraumtyp von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
3.  Fischotter (Lutra lutra), Biber (Castor fiber), Mopsfledermaus (Barbastella barbastellus), Steinbeißer (Cobitis taenia), Bachneunauge (Lampetra planeri), Rotbauchunke (Bombina bombina), Kammmolch (Triturus cristatus), Großem Feuerfalter (Lycaena dispar), Heldbock (Cerambyx cerdo), Schmaler Windelschnecke (Vertigo angustior) sowie Bauchiger Windelschnecke (Vertigo moulinsiana) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume;
4.  Eremit (Osmoderma eremita) als prioritärer Art im Sinne von § 7 Absatz 2 Nummer 11 des Bundesnaturschutzgesetzes, einschließlich seiner für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume;
5.  Sumpfglanzkraut (Liparis loeselii) als Art von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich seiner Lebensräume und den für ihre Reproduktion erforderlichen Standortbedingungen.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten des Gebietes außerhalb von Bruchwäldern, Röhrichten, Feuchtwiesen, Mooren und Fließgewässern zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 13 jeweils nach dem 30.
    Juni eines jeden Jahres;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb von Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wege und außerhalb der Waldbrandwundstreifen zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der für den öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  Wasserfahrzeuge und Schwimmkörper aller Art zu benutzen;
13.  zu baden oder zu tauchen;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel aller Art zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland umzubrechen, nachzusäen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen außerhalb der Zone 1 mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle, Jauche, Gärreste und Sekundärrohstoffdünger einzusetzen.  
          
        Sekundärrohstoffdünger im Sinne dieser Verordnung sind Abwasser, Fäkalien, Klärschlamm und ähnliche Stoffe aus Siedlungsabfällen und vergleichbare Stoffe aus anderen Quellen, jeweils auch weiterbehandelt und in Mischungen untereinander oder mit Düngemitteln, Wirtschaftsdünger, Bodenhilfsstoffen, Kultursubstraten und Pflanzenhilfsmitteln,
    2.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt.
        Bei Narbenschäden ist eine umbruchlose Nachsaat von Grünland zulässig;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen außerhalb der Zone 1 mit der Maßgabe, dass
    1.  eine Nutzung der Laubwälder einzelstamm- bis truppweise erfolgt,
    2.  in Misch- und Nadelwäldern Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von maximal 0,5 Hektar zulässig sind,
    3.  nur Baumarten der potenziell natürlichen Vegetation eingebracht werden, wobei nur heimische Baumarten in gesellschaftstypischen Anteilen unter Ausschluss eingebürgerter Arten zu verwenden sind,
    4.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    5.  das Befahren des Waldes nur auf Wegen oder Rückegassen erfolgt,
    6.  der Boden unter Verzicht auf Pflügen und Umbruch bearbeitet wird; ausgenommen ist eine streifenweise, flachgründige, nicht in den Mineralboden eingreifende Bodenverwundung zur Unterstützung von Verjüngungsmaßnahmen,
    7.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß und einer Mindesthöhe von 5 Metern nicht gefällt werden und liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimeter am stärkeren Ende) im Bestand verbleibt,
    8.  ein Altholzanteil von mindestens 10 Prozent am aktuellen Bestandsvorrat zu sichern ist, sofern vorhanden, mindestens fünf Stämme je Hektar mit einem Brusthöhendurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum Absterben aus der Nutzung genommen sein müssen,
    9.  § 4 Absatz 2 Nummer 17 und 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigem Umfang auf den bisher rechtmäßig dafür genutzten Flächen außerhalb der Zone 1 mit der Maßgabe, dass
    1.  in allen Gewässern Besatzmaßnahmen nur mit heimischen Arten durchgeführt werden und der Besatz mit Karpfen unzulässig ist,
    2.  Fanggeräte und Fangmittel so eingesetzt oder ausgestattet werden, dass eine Gefährdung des Fischotters und des Bibers weitgehend ausgeschlossen ist,
    3.  § 4 Absatz 2 Nummer 19 gilt;
4.  erforderliche Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne eines Monitorings sowie der Fang von Laichfischen der Bachforelle innerhalb der Zone 1 jeweils mit Zustimmung der unteren Naturschutzbehörde.
    Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird;
5.  die rechtmäßige Ausübung der Angelfischerei außerhalb der Zone 1 mit der Maßgabe, dass
    1.  die Angelfischerei am Tanksee nur vom Boot und von den Stegen aus zulässig ist,
    2.  die Angelfischerei am Rummelpforter Mühlenteich sowie an den GroßSperrenwalder Kleingewässern unzulässig ist,
    3.  am „Strom“ im Abschnitt vom Rummelpforter Mühlenteich bis Einmündung in den Gollmitzer Mühlenteich die Angelfischerei unzulässig ist und im übrigen Verlauf des „Stroms“ in Form der Flug- und Spinnangelei mit künstlichen Ködern erfolgt sowie
        
        aa)
        
        der „Strom“ nur zur Bergung von Gerät und Landung von Fischen betreten werden darf,
        
        bb)
        
        die untere Naturschutzbehörde zum Erreichen des Schutzzwecks nach § 3 Absatz 2 Nummer 1 und 2 im Benehmen mit der unteren Fischereibehörde bestimmte Ufer- und Fließgewässerabschnitte für die Ausübung der Angelfischerei sperren kann,
        
    4.  im Übrigen § 4 Absatz 2 Nummer 12, 19 und 20 gilt;
6.  Maßnahmen der Bestandsregulierung in der Zone 1 mit der Maßgabe, dass
    1.  die Bestandsregulierung von Schalenwild durch drei eintägige Gesellschaftsjagden im Zeitraum vom 1.
        Oktober eines jeden Jahres bis zum 31.
        Januar des Folgejahres erfolgt.
        Die Durchführung der Gesellschaftsjagden ist jeweils eine Woche vorher schriftlich bei der unteren Naturschutzbehörde anzuzeigen.
        Sonstige Maßnahmen der Bestandsregulierung sind nach Zulassung durch die untere Naturschutzbehörde zulässig.
        Dazu sind vom Antragsteller Erfordernis, Ziel, Art, Umfang, Zeitpunkt und Ort der Maßnahme darzulegen.
        Die Zulassung ist zu erteilen, wenn die Maßnahme dem Schutzzweck nicht oder nur unerheblich zuwiderläuft,
    2.  das Aufstellen transportabler und mobiler Ansitzeinrichtungen zulässig ist.
Im Übrigen ist die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd sowie die Anlage von Kirrungen, Fütterungen, Ansaatwildwiesen und Wildäckern verboten;7.  für den Bereich der Jagd außerhalb der Zone 1:
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Jagd auf Wasserfederwild verboten ist,
        
        bb)
        
        die Fallenjagd mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern an innerhalb des Schutzgebietes liegenden Gewässerufern verboten ist; Ausnahmen von der Einhaltung dieses Abstandes kann die untere Naturschutzbehörde erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
        cc)
        
        keine Baujagd in einem Abstand von bis zu 100 Metern vom Ufer aller innerhalb des Schutzgebietes liegenden Gewässer vorgenommen wird; Ausnahmen von der Einhaltung dieses Abstandes kann die untere Naturschutzbehörde erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde erfolgt.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    3.  das Aufstellen transportabler und mobiler Ansitzeinrichtungen,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und des in § 3 Absatz 2 Nummer 2 Buchstabe a genannten Lebensraumtyps „Magere Flachland-Mähwiesen“.
Ablenkfütterungen sowie die Anlage und Unterhaltung von Ansaatwildwiesen und Wildäckern sind unzulässig.
Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;9.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege;
10.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
11.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
12.  die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern diese nicht unter die Nummern 9 und 10 fallen, im Einvernehmen mit der unteren Naturschutzbehörde;
13.  die sonstigen bei Inkrafttreten dieser Verordnung aufgrund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
14.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch nach dem 30.
    Juni eines jeden Jahres;
15.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
16.  Schutz-, Pflege- und Entwicklungs- sowie Wiederherstellungsmaßnahmen, die von der zuständigen Naturschutzbehörde zugelassen oder angeordnet worden sind;
17.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Tourismus im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl.
    S.
    1734), die durch die Bekanntmachung vom 1.
    Oktober 2013 (ABl.
    S.
    2811) geändert worden ist, an Straßen und Wegen freigestellt;
18.  Maßnahmen zur Erhaltung und Entwicklung der Rundwanderwege im Boitzenburger Tiergarten, einschließlich der Berkholzer Feldflur;
19.  das Winterrodeln auf dem Weinberg im Boitzenburger Tiergarten;
20.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungserfordernissen, die sich aus anderen Vorgaben ergeben, unberührt.

#### § 6 Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  im Boitzenburger Tiergarten sollen die Hutewälder und naturnah strukturierte Eichen-, Hainbuchen- und Rotbuchenwälder durch forstliche Maßnahmen, insbesondere der Freistellung von Altbäumen, der Pflanzung von Eichen und der Entnahme von Fichten, entsprechend ihren natürlichen Standorten entwickelt werden;
2.  im gesamten Stromtal soll die natürliche Waldentwicklung in Richtung Auen- und Erlen-Eschenwälder und Rotbuchenwälder lehmiger und bodensaurer Standorte sowie im Bereich von Kröchlendorff naturnah strukturierter Stieleichenmischwälder gefördert werden;
3.  die Moore im Stromtal sollen durch den Verschluss von Rohrleitungen, das Setzen von Sohlschwellen am Straßendurchlass Gollmitz und an der Thiesorter Mühle sowie Grabenverfüllungen in Torfbereichen in ihrer natürlichen Funktion wiederhergestellt werden;
4.  an den Trockenhängen bei Berkholz, Gollmitz und Mühlhof sowie in den Horster Bergen sollen Pflegemaßnahmen, insbesondere Entbuschungen, durchgeführt werden;
5.  die Pflege der Pfeifengraswiesen und Sumpfglanzkrautbestände südlich von Berkholz soll durch Mahd mit Beräumung des Mähgutes durchgeführt werden;
6.  zum Schutz des „Stroms“ vor Erosion und Stoffeinträgen wird die Umwandlung von Acker in Grünland angestrebt.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten, Außerkrafttreten

(1) § 5 Absatz 1 Nummer 1 tritt am 1.
Januar 2018 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig tritt der Beschluss des Bezirkstages Neubrandenburg vom 21.
Oktober 1981 über das Naturschutzgebiet „Tiergarten Boitzenburg“ außer Kraft.

Potsdam, den 18.
Oktober 2017

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes "Boitzenburger Tiergarten und Strom"](/br2/sixcms/media.php/68/GVBl_II_55_2017-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes ") 959.5 KB

2

[Anlage 2 (zu § 2 Absatz 2) - Übersichtskarte, Topografische Karten, Liegenschaftskarten](/br2/sixcms/media.php/68/GVBl_II_55_2017-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Übersichtskarte, Topografische Karten, Liegenschaftskarten") 660.0 KB

3

[Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Boitzenburger Tiergarten und Strom“](/br2/sixcms/media.php/68/GVBl_II_55_2017-Anlage-3.pdf "Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Boitzenburger Tiergarten und Strom“") 621.4 KB