## Verordnung über das Naturschutzgebiet „Restsee Tröbitz“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl. I S. 2542) in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nr. 43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Elbe-Elster wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Restsee Tröbitz“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 45 Hektar.
Es umfasst folgende Flächen:

**Gemeinde:**

**Gemarkung:**        

**Flur:**      

**Flurstücke:**

Tröbitz

Tröbitz

4

4, 5, 220;

Uebigau-Wahrenbrück

Wildgrube

3

2/1, 55/12 anteilig, 206.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in insgesamt drei Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die topografischen Karten mit den Blattnummern 1 und 2 im Maßstab 1 : 10 000, unterzeichnet von der Siegelverwahrerin am 25. November 2015, Siegelnummer 13 des Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft, ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Luftbildliegenschaftskarte im Maßstab 1 : 2 500 mit der Blattnummer 1, unterzeichnet von der Siegelverwahrerin am 25.
November 2015, Siegelnummer 13 des Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft.

(3) Innerhalb des Naturschutzgebietes wird gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes eine Zone 1 mit unterschiedlichen Beschränkungen der Nutzung festgesetzt.
Diese Zone umfasst rund 32 Hektar in folgenden Flächen:

**Gemeinde:**

**Gemarkung:**        

**Flur:**      

**Flurstücke:**

Tröbitz

Tröbitz

4

4, 5, 220 anteilig;

Uebigau-Wahrenbrück

Wildgrube

3

2/1, 55/12 anteilig, 206.

Die Grenze der Zone 1 ist in den in Absatz 2 genannten topografischen Karten mit den Blattnummern 1 und 2 sowie in der Luftbildliegenschaftskarte mit der Blattnummer 1 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Luftbildliegenschaftskarte.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Elbe-Elster, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes mit seinem oligotrophen Tagebausee sowie Pionier- und Vorwäldern der Kippenflächen als Elemente einer Bergbaufolgelandschaft an der Westgrenze des Niederlausitzer Braunkohlen-Bergbaureviers ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere von Gewässern, Trockenrasen und Heiden (Silbergrasfluren, Calluna-Heiden, Ginsterfluren), naturnahen Kippenvorwäldern und Forsten mit Potenzial für naturnahe Wälder;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Doldenwinterlieb (Chimaphila umbellata), Geflecktes Knabenkraut (Dactylorhiza maculata), Braunroter Sitter (Epipactis atrorubens), Breitblättriger Sitter (Epipactis helleborine), Karthäuser- und Heide-Nelke (Dianthus carthusianorum und Dianthus deltoides), Großes Zweiblatt (Listera ovata), Keulenbärlapp (Lycopodium clavatum), Weiße Seerose (Nymphaea alba), Mondraute (Botrychium lunaria), Ästiger Rautenfarn (B.
    matricarii-folium) und Wasserschlauch-Arten (Utricularia sp.);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Vögel, Reptilien und Insekten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Brachpieper (Anthus campestris), Rohrweihe (Circus aeruginosus), Schwarzspecht (Dryocopus martius), Kranich (Grus grus), Rotmilan (Milvus milvus), Waldwasserläufer (Tringa ochropus), Zauneidechse (Lacerta agilis) und Blauflügelige Sandschrecke (Sphingonotus caerulans);
4.  die Erhaltung des Gebietes als bedeutendes Brutgebiet von Wat- und Wasservögeln, insbesondere von Flussregenpfeifer (Charadrius dubius), Entenvögeln wie Schellente (Bucephala clangula), Rohrsängern wie Teich- (Acrocephalus scirpaceus) und Drosselrohrsänger (Acrocephalus arundinaceus) und Rallen;
5.  die Erhaltung des Gewässers als wichtiges Rast-, Mauser- und Sammelgewässer für den Frühjahrs- und Herbstzug insbesondere von Entenvögeln und Graugänsen und als Nahrungs- und Überwinterungsgebiet insbesondere für Limikolen, Taucher wie Zwergtaucher (Tachybaptus ruficollis) und Haubentaucher (Podiceps cristatus), Rallen wie Blessralle (Fulica atra) und Wasserralle (Rallus aquaticus), Seeadler (Haliaeetus albicilla) und Fischadler (Pandion haliaetus);
6.  die Erhaltung aus wissenschaftlichen Gründen zur Beobachtung der Vegetationsdynamik in Naturwaldzellen und auf Rohbodenflächen der Kippen des Braunkohletagebaus;
7.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Biotopverbundes zwischen den Bergbaufolgelandschaften des nordwestlichen und westlichen Teils des Niederlausitzer Braunkohlen-Bergbaureviers.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Restsee Tröbitz“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinen Vorkommen von

1.  Oligo- bis mesotrophen stehenden Gewässern mit Vegetation der Littorelleea uniflorae und der Isoeto-Nanojuncetea als natürlichem Lebensraumtyp von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Biber (Castor fiber), Fischotter (Lutra lutra) und Schmalbindigem Breitflügel-Tauchkäfer (Graphoderus bilineatus) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 10 jeweils nach dem 30.
    Juni eines jeden Jahres,
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb von Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, zu reiten;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
12.  in der Zone 1 zu baden oder zu tauchen;
13.  in der Zone 1 Luftmatratzen oder andere Schwimmhilfen, Wasserfahrzeuge aller Art oder Surfbretter zu benutzen;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (wie zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien wie zum Beispiel Gärfutter zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die Durchführung von Maßnahmen auf der Grundlage von Sanierungsplänen nach § 12 des Gesetzes zur Regionalplanung und zur Braunkohlen- und Sanierungsplanung im Land Brandenburg bei sicherheitstechnisch notwendigen Maßnahmen sowie Maßnahmen zur Wiedernutzbarmachung und Oberflächengestaltung im Einvernehmen mit der Fachbehörde für Naturschutz und Landschaftspflege; Maßnahmen zur Aufforstung und Begrünung bleiben unzulässig;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen außerhalb der Zone 1 in der Zeit vom 1.
    August eines Jahres bis zum 31.
    Januar des Folgejahres mit der Maßgabe, dass
    1.  Holzerntemaßnahmen, die den Holzvorrat auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von 0,5 Hektar zulässig sind,
    2.  nur Arten der potenziell natürlichen Vegetation in gesellschaftstypischen Anteilen eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
    3.  Bäume mit Horsten und Höhlen nicht gefällt werden dürfen;
3.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Jagd auf Wasservögel verboten bleibt,
        
        bb)
        
        die Fallenjagd innerhalb der Grenze des Naturschutzgebietes verboten ist.
        Ausnahmen bedürfen einer Genehmigung der unteren Naturschutzbehörde,
        
        cc)
        
        keine Baujagd innerhalb des Naturschutzgebietes in einem Abstand von bis zu 100 Metern vom Gewässerufer des Restloches vorgenommen wird,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    3.  die Aufstellung transportabler und mobiler Ansitzeinrichtungen außerhalb einer 20-Meter-Zone ab Gewässerufer,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope.
    
    Ablenkfütterungen sowie die Anlage von Wildwiesen und Wildäckern sind unzulässig.
    Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Brandenburgischen Jagdgesetzes unberührt;
    
4.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung mit der Maßgabe, dass
    1.  Fangeräte und Fangmittel so einzusetzen und auszustatten sind, dass eine Gefährdung des Fischotters weitgehend ausgeschlossen ist,
    2.  § 4 Absatz 2 Nummer 19 und 20 gilt;
5.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass
    1.  die Angelfischerei nur an der in den in § 2 Absatz 2 genannten topografischen Karten dargestellten Angelstelle ausgeübt wird,
    2.  § 4 Absatz 2 Nummer 19 und 20 gilt;
6.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter die Nummer 6 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
7.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung des Gewässers, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
8.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
9.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
10.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch jeweils nach dem 30.
    Juni eines jeden Jahres;
11.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
12.  Schutz-, Pflege- und Entwicklungsmaßnahmen sowie Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
13.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl.
    S. 1734), die durch die Bekanntmachung vom 1.
    Oktober 2013 (ABl.
    S. 2811) geändert worden ist, an Straßen und Wegen freigestellt;
14.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen; die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestands und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

#### § 6 Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  es soll keine Erhöhung des pH-Wertes durch Kalkung erfolgen;
2.  besonnte Uferabschnitte und Flachwasserbereiche sollen erhalten werden.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 24.
Februar 2016

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

### Anlagen

1

[Anlage - Kartenskizze zur Lage des Naturschutzgebietes "Restsee Tröbitz"](/br2/sixcms/media.php/68/GVBl_II_07_2016-Anlage-zum-Hauptdokument.pdf "Anlage - Kartenskizze zur Lage des Naturschutzgebietes ") 635.7 KB