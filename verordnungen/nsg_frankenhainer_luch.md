## Verordnung über das Naturschutzgebiet „Frankenhainer Luch“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542), von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl.
I S.
1972) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nr.
43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Elbe-Elster wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Frankenhainer Luch“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 178 Hektar.
Es umfasst folgende Flächen:

Stadt:

Gemarkung:

Flur:

Schlieben

Frankenhain

1;

Oelsig

1;

Schlieben

10.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 2 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 4 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Innerhalb des Naturschutzgebietes werden gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes Zonen mit unterschiedlichen Beschränkungen der Nutzung festgesetzt.
Die Zone 1 umfasst rund 15 Hektar mit Beschränkungen der forstwirtschaftlichen Nutzung und liegt in der Gemeinde Schlieben, Gemarkung Frankenhain, Flur 1.
Die Zone 2 umfasst rund einen Hektar mit Beschränkungen der landwirtschaftlichen Nutzung und liegt in der Gemeinde Schlieben, Gemarkung Frankenhain, Flur 1.

Die Grenze der Zonen 1 und 2 ist in der in Absatz 1 genannten Kartenskizze und in den in Absatz 2 genannten topografischen Karten mit den Blattnummern 1 bis 2 sowie in den Liegenschaftskarten mit den Blattnummern 1 bis 4 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Elbe-Elster, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes als eine vielfältig strukturierte und für die Niederlausitz bedeutsame Niedermoorlandschaft ist

1.  die Erhaltung, Entwicklung und Wiederherstellung der Lebensräume wild lebender Pflanzengesellschaften, insbesondere der Feuchtwiesen und Feuchtweiden, Pfeifengraswiesen, der Flutrasen und Staudenfluren feuchter und frischer Standorte und Erlenbruchwälder;
2.  die Erhaltung und Entwicklung der Lebensräume wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Torfmoose (Sphagnidae), Froschbiss (Hydrocharis morsus-ranae), Wasserfeder (Hottonia palustris), Heidenelke (Dianthus deltoides), Sumpf-Sternmiere (Stellaria palustris), Glänzende Wiesenraute (Thalictrum lucidum);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Säugetiere, Vögel, Amphibien und Insekten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, beispielsweise Kranich (Grus grus), Bekassine (Gallinago gallinago), Tüpfelralle (Porzana porzana), Waldwasserläufer (Tringa ochropus), Eisvogel (Alcedo atthis), Kreuzkröte (Bufo calamita), Europäischer Laubfrosch (Hyla arborea), Moorfrosch (Rana arvalis) und Grasfrosch (Rana temporaria);
4.  die Erhaltung und Entwicklung des Moores aus wissenschaftlichen Gründen;
5.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des Biotopverbundes.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Frankenhainer Luch“, eines Teils des ehemaligen Gebietes „Kremitz und Fichtwaldgebiet“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) mit seinem Vorkommen von

1.  Flüssen der planaren bis montanen Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Pfeifengraswiesen auf kalkreichem Boden, torfigen und tonig-schluffigen Böden (Molinion caeruleae), Feuchten Hochstaudenfluren der planaren und montanen bis alpinen Stufe und Alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur als natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Auen-Wäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion, Alnion incanae, Salicion albae) und Moorwäldern als prioritärem natürlichen Lebensraumtyp im Sinne von § 7 Absatz 1 Nummer 5 des Bundes​naturschutzgesetzes;
3.  Biber (Castor fiber), Fischotter (Lutra lutra), Bitterling (Rhodeus amarus) und Grüner Keiljungfer (Ophiogomphus cecilia) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 11 jeweils nach dem 30.
    Juni eines jeden Jahres;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb von Wegen zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  Wasserfahrzeuge aller Art zu benutzen;
13.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
14.  Hunde frei laufen zu lassen;
15.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
16.  Düngemittel aller Art zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
17.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
18.  Tiere zu füttern oder Futter bereitzustellen;
19.  Tiere auszusetzen oder Pflanzen anzusiedeln;
20.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
21.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
22.  Pflanzenschutzmittel jeder Art anzuwenden;
23.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und Gärreste aus Biogasanlagen und Sekundärrohstoffdünger einzusetzen.
          
          
        Sekundärrohstoffdünger im Sinne dieser Verordnung sind Abwasser, Fäkalien, Klärschlamm und ähnliche Stoffe aus Siedlungsabfällen und vergleichbare Stoffe aus anderen Quellen, jeweils auch weiterbehandelt und in Mischungen untereinander oder mit Düngemitteln, Wirtschaftsdünger, Bodenhilfsstoffen, Kultursubstraten und Pflanzenhilfsmitteln,
    2.  Grünland in der Zone 2 darüber hinaus als Wiese oder Weide genutzt wird, § 4 Absatz 2 Nummer 16 gilt und eine Mulchmahd verboten bleibt,
    3.  bei Beweidung Gehölze in geeigneter Weise gegen Verbiss und sonstige Beschädigungen sowie Ränder von Gewässern gegen Trittschäden von weidenden Nutztieren geschützt werden,
    4.  auf Grünland § 4 Absatz 2 Nummer 22 und 23 gilt.
        Bei Narbenschäden ist eine umbruchlose Nachsaat mit Zustimmung der unteren Naturschutzbehörde zulässig;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftlichen Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  in der Zone 1 keine forstwirtschaftlichen Maßnahmen erfolgen,
    2.  auf den übrigen Flächen
        
        aa)
        
        in dem in § 3 Absatz 2 Nummer 1 genannten Waldlebensraumtyp Alte bodensaure Eichenwälder eine Nutzung ausschließlich einzelstamm- bis truppweise erfolgt.
        Auf den übrigen Waldflächen sind Holzerntemaßnahmen, die den Holzvorrat auf weniger als 40 Prozent des üblichen Holzvorrates reduzieren, bis zu einer Größe von 0,5 Hektar zulässig,
        
        bb)
        
        nur Arten der potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
        
        cc)
        
        ein Altholzanteil von mindestens 10 Prozent am aktuellen Bestandesvorrat zu sichern ist.
        Dabei sind, sofern vorhanden, mindestens fünf Stämme je Hektar mit einem Mindestdurchmesser von 30 Zentimetern in 1,30 Metern Höhe über dem Stammfuß bis zum Absterben aus der Nutzung zu nehmen und dauerhaft zu markieren; in Jungbeständen ist ein solcher Altholzanteil zu entwickeln,
        
        dd)
        
        je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 30 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden und liegendes Totholz (ganze Bäume mit über 65 Zentimeter Durchmesser am stärksten Ende) im Bestand verbleibt,
        
        ee)
        
        Bäume mit Horsten oder Höhlen nicht gefällt werden dürfen,
        
    3.  § 4 Absatz 2 Nummer 22 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass Fanggeräte und Fangmittel so eingesetzt oder ausgestattet werden, dass eine Gefährdung des Bibers und des Fischotters weitgehend ausgeschlossen ist;
4.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass § 4 Absatz 2 Nummer 18 und 19 gilt;
5.  für den Bereich der Jagd:
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Jagd in der Zeit vom 1.
        März bis zum 30.
        Juni eines jeden Jahres ausschließlich vom Ansitz aus erfolgt,
        
        bb)
        
        die Fallenjagd mit Lebendfallen erfolgt und bis zu einem Abstand von 100 Meter zum Gewässerufer der Kremitz verboten ist,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
          
          
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Errichtung anzuzeigen.
        Die Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        Die Entscheidung hierzu soll unverzüglich erfolgen,   
          
        
    3.  Ablenkfütterungen, Kirrungen sowie die Anlage von Ansaatwildwiesen und die Anlage und Unterhaltung von Wildäckern in der Zone 1 und innerhalb geschützter Biotope gemäß § 30 des Bundesnaturschutz​gesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes sind unzulässig.
        Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
6.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege.
    Die untere Naturschutzbehörde ist rechtzeitig mit dem Ziel einer einvernehmlichen Lösung zu beteiligen;
7.  die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter die Nummern 8 und 9 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
8.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung des Gewässers, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
9.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
10.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
11.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch jeweils nach dem 30.
    Juni eines jeden Jahres;
12.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
13.  Schutz-, Pflege- und Entwicklungsmaßnahmen sowie Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
14.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen;
15.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen; die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestands und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungsbefugnissen, die sich aus anderen fachrechtlichen Vorgaben ergeben, unberührt.

#### § 6 Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  die Fläche der Feuchten Hochstaudenfluren der planaren und montanen bis alpinen Stufe soll alle zwei bis drei Jahre möglichst im Zeitraum von September bis November gemäht werden.
    Die Lagerung von Schnitt- und Räumgut aus der Gewässerunterhaltung soll auf diesen Flächen vermieden werden;
2.  die Kremitz soll renaturiert und möglichst in das alte Flussbett rückgeführt werden;
3.  auf den Grünlandflächen werden oberflächennahe Wasserstände mit Blänkenbildung bis zum 30. April eines jeden Jahres angestrebt;
4.  bei den Pfeifengraswiesen soll eine Mahd-Weide-Wechselnutzung mit Weide als Nebennutzung erfolgen.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 1 Nummer 1 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

§ 5 Absatz 1 Nummer 1 Buchstabe a, b und d tritt am 1.
Januar 2019 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 5.
Oktober 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes "Frankenhainer Luch"](/br2/sixcms/media.php/68/GVBl_II_67_2018-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes ") 711.6 KB

2

[Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten](/br2/sixcms/media.php/68/GVBl_II_67_2018-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten") 188.3 KB

3

[Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Frankenhainer Luch“](/br2/sixcms/media.php/68/GVBl_II_67_2018-Anlage-3.pdf "Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Frankenhainer Luch“") 172.8 KB