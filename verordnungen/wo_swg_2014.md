## Wahlordnung zum Sorben/Wenden-Gesetz (WO-SWG)

Auf Grund des § 5 Absatz 2 Satz 12 des Sorben/Wenden-Gesetzes vom 7.
Juli 1994 (GVBl.
I S. 294), der durch Artikel 1 Nummer 8 des Gesetzes vom 11.
Februar 2014 (GVBl.
I Nr. 7) neu gefasst worden ist, verordnet der Minister des Innern nach Anhörung des Rates für Angelegenheiten der Sorben/Wenden und im Benehmen mit dem Hauptausschuss des Landtages:

Inhaltsübersicht
----------------

[§ 1 Anwendungsbereich](#1)  
[§ 2 Begriffsbestimmungen](#2)  
[§ 3 Wahlsystem](#3)  
[§ 4 Wahlzeit](#4)  
[§ 5 Wahlorgane](#5)  
[§ 6 Wahlausschuss](#6)  
[§ 7 Briefwahlvorstand](#7)  
[§ 8 Sachliche Voraussetzung für die Wahlberechtigung](#8)  
[§ 9 Förmliche Voraussetzung für die Wahlberechtigung](#9)  
[§ 10 Wählbarkeit](#10)  
[§ 11 Führung des Wählerverzeichnisses](#11)  
[§ 12 Verfahren für die Eintragung in das Wählerverzeichnis auf Antrag](#12)  
[§ 13 Benachrichtigung der in das Wählerverzeichnis eingetragenen Personen und Erteilung der Briefwahlunterlagen](#13)  
[§ 14 Bekanntmachung des Verfahrens der Eintragung in das Wählerverzeichnis und der Erteilung von Briefwahlunterlagen](#14)  
[§ 15 Einsichtnahme in das Wählerverzeichnis](#15)  
[§ 16 Einspruch gegen das Wählerverzeichnis und Beschwerde](#16)  
[§ 17 Abschluss des Wählerverzeichnisses](#17)  
[§ 18 Aufforderung zur Einreichung von Einzelwahlvorschlägen](#18)  
[§ 19 Wahlvorschlagsrecht](#19)  
[§ 20 Inhalt und Form der Einzelwahlvorschläge](#20)  
[§ 21 Bestimmung der Bewerberinnen und Bewerber](#21)  
[§ 22 Rücktritt und Tod von Bewerberinnen und Bewerbern](#22)  
[§ 23 Zurückziehung von Einzelwahlvorschlägen](#23)  
[§ 24 Vorprüfung der Einzelwahlvorschläge; Mängelbeseitigung](#24)  
[§ 25 Zulassung der Einzelwahlvorschläge](#25)  
[§ 26 Stimmzettel](#26)  
[§ 27 Stimmabgabe durch Briefwahl](#27)  
[§ 28 Behandlung der Wahlbriefe](#28)  
[§ 29 Zurückweisung von Wahlbriefen](#29)  
[§ 30 Ungültige Stimmen](#30)  
[§ 31 Feststellung des Wahlergebnisses durch den Briefwahlvorstand](#31)  
[§ 32 Zählung der Wählerinnen und Wähler](#32)  
[§ 33 Zählung der Stimmen](#33)  
[§ 34 Vorläufiges Wahlergebnis](#34)  
[§ 35 Niederschrift](#35)  
[§ 36 Übergabe und Verwahrung der Wahlunterlagen](#36)  
[§ 37 Feststellung des Wahlergebnisses im Land durch den Wahlausschuss](#37)  
[§ 38 Bekanntmachung des endgültigen Wahlergebnisses](#38)  
[§ 39 Benachrichtigung der gewählten Bewerberinnen und Bewerber](#39)  
[§ 40 Annahme der Wahl](#40)  
[§ 41 Verlust der Mitgliedschaft im Rat](#41)  
[§ 42 Berufung von Ersatzpersonen](#42)  
[§ 43 Wahlanfechtung](#43)  
[§ 44 Wiederholungswahl](#44)  
[§ 45 Stimmzettel und Vordrucke](#45)  
[§ 46 Hilfskräfte](#46)  
[§ 47 Sicherung der Wahlunterlagen](#47)  
[§ 48 Vernichtung der Wahlunterlagen](#48)  
[§ 49 Wahlkosten](#49)  
[§ 50 Fristen und Termine sowie Schriftform](#50)  
[§ 51 Inkrafttreten](#51)

### § 1   
Anwendungsbereich

Diese Verordnung gilt für die unmittelbare Wahl des Rates für Angelegenheiten der Sorben/Wenden.

### § 2   
Begriffsbestimmungen

(1) Rat im Sinne dieser Verordnung ist der Rat für Angelegenheiten der Sorben/Wenden nach § 5 Absatz 1 Satz 1 des Sorben/Wenden-Gesetzes.

(2) Dachverband im Sinne dieser Verordnung ist ein Dachverband sorbischer/wendischer Verbände und Vereine nach § 4a des Sorben/Wenden-Gesetzes.

(3) Vereinigungen im Sinne dieser Verordnung sind Vereine oder Verbände, die sich in ihrer Satzung zu sorbischen/wendischen Zielen bekennen.

(4) Geschäftsstelle im Sinne dieser Verordnung ist die gemäß § 5 Absatz 2 Satz 4 des Sorben/Wenden-Gesetzes von den Dachverbänden gemeinsam zu benennende Stelle.

### § 3   
Wahlsystem

(1) Der Rat besteht aus fünf Mitgliedern, die von den wahlberechtigten Sorben/Wenden in freier, gleicher, geheimer und unmittelbarer Wahl nach den Grundsätzen der Mehrheits- und Persönlichkeitswahl nach Einzelwahlvorschlägen der Vereinigungen (§ 2 Absatz 3) gewählt werden.

(2) Jede Wählerin und jeder Wähler hat jeweils fünf Stimmen.
Sie oder er kann einem Einzelwahlvorschlag (einer Bewerberin oder einem Bewerber) nur eine Stimme geben.

(3) Gewählt sind die fünf Bewerberinnen und Bewerber mit den meisten Stimmen.
Bei Stimmengleichheit entscheidet das von der Wahlleiterin oder dem Wahlleiter zu ziehende Los.

(4) Die nicht gewählten Bewerberinnen und Bewerber sind Ersatzpersonen in der Reihenfolge der auf sie entfallenden Stimmenzahlen; Absatz 3 Satz 2 gilt entsprechend.

### § 4   
Wahlzeit

(1) Die Wahl des Rates soll möglichst zeitnah zur Wahl des Landtages erfolgen.
Die Wahl des Rates muss spätestens in dem Jahr stattfinden, das dem Jahr der Wahl des Landtages folgt.

(2) Der Wahlausschuss bestimmt den letzten Tag der Briefwahl mindestens hundert Tage vor diesem Tag.
Er bestimmt zugleich den Zeitpunkt, an dem die Wahlzeit am letzten Tag der Briefwahl endet (Ende der Wahlzeit).

(3) Die Wahlleiterin oder der Wahlleiter macht den letzten Tag der Briefwahl und das Ende der Wahlzeit unverzüglich im Amtsblatt für Brandenburg öffentlich bekannt.

### § 5   
Wahlorgane

(1) Wahlorgane sind

1.  der Wahlausschuss und die Wahlleiterin oder der Wahlleiter sowie
    
2.  mindestens ein Briefwahlvorstand und eine Briefwahlvorsteherin oder ein Briefwahlvorsteher.
    

(2) Die Wahlleiterin oder der Wahlleiter bestimmt die Anzahl der Briefwahlvorstände.

(3) Bewerberinnen und Bewerber sowie Vertrauenspersonen und stellvertretende Vertrauenspersonen dürfen nicht zu Mitgliedern eines Wahlorgans bestellt werden.

(4) Die Mitglieder des Wahlausschusses und der Briefwahlvorstände üben ihre Tätigkeit ehrenamtlich aus.

(5) Die Mitglieder des Wahlausschusses erhalten für die Teilnahme an einer nach § 6 einberufenen Sitzung und die Mitglieder der Briefwahlvorstände für die Ermittlung des Wahlergebnisses ein Erfrischungsgeld nach den Bestimmungen des § 8 Absatz 2 der Brandenburgischen Landeswahlverordnung und, wenn sie außerhalb ihres Wohnortes tätig werden, Auslagenersatz für ihre Kosten entsprechend den Reisekostenregelungen.

### § 6   
Wahlausschuss

(1) Der Wahlausschuss besteht aus mindestens drei und höchstens sieben Mitgliedern.
Die Dachverbände benennen vor jeder Wahl die Mitglieder des Wahlausschusses.
Der Wahlausschuss wählt aus seiner Mitte die Wahlleiterin oder den Wahlleiter als Vorsitzende oder Vorsitzenden sowie eine Stellvertreterin oder einen Stellvertreter.

(2) Der Wahlausschuss verhandelt und entscheidet in öffentlicher Sitzung.
Ort, Zeit und Gegenstand der Sitzung sind durch Aushang am oder im Eingang des Sitzungsgebäudes mit dem Hinweis, dass jede Person Zutritt zu der Sitzung hat, bekannt zu machen.

(3) Die Einberufung zur ersten Sitzung des Wahlausschusses erfolgt durch das älteste Mitglied, zu allen weiteren Sitzungen durch die Wahlleiterin oder den Wahlleiter.
Die Ladungen zu den Sitzungen sollen den Mitgliedern mit einer Frist von mindestens 24 Stunden unter Übersendung der Tagesordnung zugehen.

(4) Der Wahlausschuss ist beschlussfähig, wenn die Vorsitzende oder der Vorsitzende anwesend ist.

(5) Der Wahlausschuss fasst seine Beschlüsse mit Stimmenmehrheit.
Bei Stimmengleichheit gibt die Stimme der Wahlleiterin oder des Wahlleiters den Ausschlag.

(6) Die Wahlleiterin oder der Wahlleiter bestimmt eine Schriftführerin oder einen Schriftführer.
Die Schriftführerin oder der Schriftführer führt über jede Sitzung eine Niederschrift.
Die Schriftführerin oder der Schriftführer ist nur stimmberechtigt, wenn sie oder er Mitglied des Wahlausschusses ist.
Die Wahlleiterin oder der Wahlleiter weist in jeder Sitzung alle Mitglieder und die Schriftführerin oder den Schriftführer auf ihre Verpflichtung zur unparteiischen Wahrnehmung ihres Amtes und zur Verschwiegenheit über die ihnen bei ihrer amtlichen Tätigkeit bekannt gewordenen Angelegenheiten hin.

(7) Der Wahlausschuss kann seine Beschlüsse abändern, wenn ein begründeter Anlass besteht und der jeweilige Stand des Wahlverfahrens dies erlaubt.
Eine Abänderung der Feststellung des Wahlergebnisses muss binnen einer Woche nach der ersten Beschlussfassung erfolgen.

(8) Die Wahlleiterin oder der Wahlleiter führt die Geschäfte des Wahlausschusses am Ort der Geschäftsstelle.

(9) Die Geschäftsstelle macht die Namen der Wahlleiterin oder des Wahlleiters und der Stellvertreterin oder des Stellvertreters sowie die Anschrift der Geschäftsstelle mit Telekommunikationsanschlüssen im Amtsblatt für Brandenburg öffentlich bekannt.

(10) Der Wahlausschuss besteht nach der Wahl fort.
Für ausgeschiedene Mitglieder können die Dachverbände neue Mitglieder benennen.

### § 7   
Briefwahlvorstand

(1) Der Briefwahlvorstand besteht aus mindestens drei und höchstens neun Mitgliedern.

(2) Vor jeder Wahl beruft die Wahlleiterin oder der Wahlleiter die Mitglieder des Briefwahlvorstandes.
Vor ihrer Berufung sollte die Wahlleiterin oder der Wahlleiter die Dachverbände und die im Rat vertretenen Vereinigungen auffordern, innerhalb einer angemessenen Frist geeignete Personen als Mitglied vorzuschlagen.
In der Aufforderung soll auf die Regelung des § 5 Absatz 3 hingewiesen werden.
Nach Ablauf der Vorschlagsfrist beruft die Wahlleiterin oder der Wahlleiter unverzüglich die Briefwahlvorsteherin oder den Briefwahlvorsteher und die Stellvertreterin oder den Stellvertreter sowie die Beisitzerinnen und Beisitzer.
Werden nicht genügend geeignete Personen vorgeschlagen, so beruft die Wahlleiterin oder der Wahlleiter die weiteren Mitglieder nach ihrem oder seinem Ermessen.

(3) Die Briefwahlvorsteherin oder der Briefwahlvorsteher beruft aus den Beisitzerinnen und Beisitzern die Schriftführerin oder den Schriftführer.

(4) Die Wahlleiterin oder der Wahlleiter sorgt dafür, dass die Mitglieder des Briefwahlvorstandes so über ihre Aufgaben unterrichtet werden, dass eine ordnungsgemäße Ermittlung und Feststellung des Wahlergebnisses gesichert ist.
Die Wahlleiterin oder der Wahlleiter weist die Mitglieder des Briefwahlvorstandes auf ihre Verpflichtung zur unparteiischen Wahrnehmung ihres Amtes und zur Verschwiegenheit über die ihnen bei ihrer Tätigkeit bekannt gewordenen Angelegenheiten hin.

(5) Der Briefwahlvorstand wird von der Wahlleiterin oder dem Wahlleiter einberufen.
Die Briefwahlvorsteherin oder der Briefwahlvorsteher leitet die Tätigkeit des Briefwahlvorstandes.

(6) Der Briefwahlvorstand verhandelt und entscheidet in öffentlicher Sitzung.
Bei der Ermittlung und Feststellung des Wahlergebnisses sollen alle Mitglieder des Briefwahlvorstandes anwesend sein.
Der Briefwahlvorstand ist beschlussfähig, wenn mindestens drei Mitglieder, darunter jeweils die Briefwahlvorsteherin oder der Briefwahlvorsteher und die Schriftführerin oder der Schriftführer, anwesend sind.

(7) Der Briefwahlvorstand fasst seine Beschlüsse mit Stimmenmehrheit.
Bei Stimmengleichheit gibt die Stimme der Briefwahlvorsteherin oder des Briefwahlvorstehers den Ausschlag.

### § 8   
Sachliche Voraussetzung für die Wahlberechtigung

Wahlberechtigt sind alle Sorben/Wenden, die am letzten Tag der Briefwahl zur Wahl des Landtages Brandenburg wahlberechtigt sind.

### § 9   
Förmliche Voraussetzung für die Wahlberechtigung

(1) Wählen kann nur die wahlberechtigte Person, die in das Wählerverzeichnis für die Wahl des Rates eingetragen ist.

(2) Eine wahlberechtigte Person wird auf Antrag in das Wählerverzeichnis eingetragen.

(3) Eine im Wählerverzeichnis eingetragene wahlberechtigte Person kann an der Wahl des Rates nur mit Wahlschein durch Briefwahl teilnehmen.

### § 10   
Wählbarkeit

Wählbar sind alle in das Wählerverzeichnis eingetragenen wahlberechtigten Sorben/Wenden, die am letzten Tag der Briefwahl das 18.
Lebensjahr vollendet haben.

### § 11   
Führung des Wählerverzeichnisses

(1) Die Geschäftsstelle (§ 2 Absatz 4) führt das Verzeichnis der wahlberechtigten Personen nach Familiennamen, Vornamen und Geburtsdatum sowie Wohn- oder Erreichbarkeitsanschrift.
Das Wählerverzeichnis kann nach Landkreisen, kreisfreien Städten sowie kreisangehörigen Städten, Gemeinden und Ämtern gegliedert werden.
Das Wählerverzeichnis kann im automatisierten Verfahren geführt werden.

(2) In das Wählerverzeichnis dürfen nur wahlberechtigte Personen eingetragen werden, die ihre Eintragung gemäß § 12 beantragt haben.

### § 12   
Verfahren für die Eintragung in das Wählerverzeichnis auf Antrag

(1) Der Antrag auf Eintragung in das Wählerverzeichnis ist schriftlich bis spätestens zum siebten Tag vor dem letzten Tag der Briefwahl bei der Geschäftsstelle zu stellen.
Die Schriftform gilt auch durch E-Mail, Telefax oder durch sonstige dokumentierbare Übermittlung in elektronischer Form gewahrt.
Eine fernmündliche Antragstellung ist unzulässig.

(2) Der Antrag muss Familienname, Vornamen und Geburtsdatum sowie die Anschrift der wahlberechtigten Person enthalten.
Eine antragstellende Person, für die im Melderegister eine Auskunftssperre eingetragen ist, kann anstelle ihrer Anschrift eine Erreichbarkeitsanschrift angeben.

(3) Eine behinderte wahlberechtigte Person kann sich der Hilfe einer Person ihres Vertrauens (Hilfsperson) bedienen.

(4) Wer den Antrag für eine andere Person stellt, muss durch Vorlage einer schriftlichen Vollmacht nachweisen, dass er dazu berechtigt ist.

(5) Die Geschäftsstelle entscheidet binnen drei Tagen über den Antrag.
Wird dem Antrag stattgegeben, ist der wahlberechtigten Person unverzüglich, jedoch nicht vor Zulassung der Wahlvorschläge (§ 25 Absatz 3 Satz 1) eine Wahlbenachrichtigung nebst Briefwahlunterlagen (§ 13 Absatz 2) zu übersenden.
Eine ablehnende Entscheidung ist der antragstellenden Person schnellstmöglich bekannt zu geben.
Die antragstellende Person kann innerhalb von zwei Tagen nach der Bekanntgabe der ablehnenden Entscheidung Beschwerde an die Wahlleiterin oder den Wahlleiter erheben.
Die Beschwerde ist schriftlich bei der Geschäftsstelle zu erheben.
Die Geschäftsstelle hat die Beschwerde sofort der Wahlleiterin oder dem Wahlleiter vorzulegen.
Die Wahlleiterin oder der Wahlleiter entscheidet spätestens am fünften Tag vor dem letzten Tag der Briefwahl über die Beschwerde.
Die Wahlleiterin oder der Wahlleiter hat dafür zu sorgen, dass die Geschäftsstelle sofort im Besitz der Entscheidung ist.
Die Entscheidung ist der Beschwerdeführerin oder dem Beschwerdeführer sofort mitzuteilen; Satz 2 gilt entsprechend.

(6) Jede Bewerberin und jeder Bewerber muss den Antrag auf Eintragung in das Wählerverzeichnis abweichend von Absatz 1 spätestens bis zum Ablauf der Einreichungsfrist für Einzelwahlvorschläge (§ 19 Absatz 2) stellen.
Die Geschäftsstelle entscheidet sofort über den Antrag.
Wird dem Antrag stattgegeben, ist der wahlberechtigten Person sofort eine Bescheinigung über die Eintragung in das Wählerverzeichnis auszufertigen sowie unverzüglich, jedoch nicht vor Zulassung der Wahlvorschläge (§ 25 Absatz 3 Satz 1) eine Wahlbenachrichtigung nebst Briefwahlunterlagen (§ 13 Absatz 2) zu übersenden.
Eine ablehnende Entscheidung ist der Bewerberin oder dem Bewerber sofort bekannt zu geben.
Die Bewerberin oder der Bewerber kann innerhalb von zwei Tagen nach der Bekanntgabe der ablehnenden Entscheidung Beschwerde an die Wahlleiterin oder den Wahlleiter erheben.
Die Beschwerde ist schriftlich bei der Geschäftsstelle zu erheben.
Die Geschäftsstelle hat die Beschwerde sofort der Wahlleiterin oder dem Wahlleiter vorzulegen.
Die Wahlleiterin oder der Wahlleiter entscheidet sofort über die Beschwerde.
Die Wahlleiterin oder der Wahlleiter hat dafür zu sorgen, dass die Geschäftsstelle sofort im Besitz der Entscheidung ist.
Die Entscheidung ist der Beschwerdeführerin oder dem Beschwerdeführer sofort mitzuteilen; Satz 3 gilt entsprechend.

(7) Werden einer wahlberechtigten Person die Wahlbenachrichtigung und die Briefwahlunterlagen übersandt, so ist im Wählerverzeichnis in der dafür vorgesehenen Spalte der Vermerk „WB“ einzutragen.

(8) Wird eine Person, die bereits Briefwahlunterlagen erhalten hat, im Wählerverzeichnis gestrichen, so ist der erteilte Wahlschein für ungültig zu erklären.
Die Geschäftsstelle führt darüber ein Verzeichnis, in das die Familien- und Vornamen, die Geburtsdaten und die Wohn- oder Erreichbarkeitsanschriften der betroffenen Personen aufzunehmen sind.
Nach Abschluss des Wählerverzeichnisses übergibt die Geschäftsstelle der Wahlleiterin oder dem Wahlleiter das Verzeichnis nach Satz 2.
Die Wahlleiterin oder der Wahlleiter hat dafür Sorge zu tragen, dass jeder Briefwahlvorstand für die Ermittlung und Feststellung des Wahlergebnisses eine Ausfertigung des Verzeichnisses nach Satz 2 erhält.

### § 13   
Benachrichtigung der in das Wählerverzeichnis eingetragenen Personen und Erteilung der Briefwahlunterlagen

(1) Jede wahlberechtigte Person, die auf Antrag in das Wählerverzeichnis eingetragen wird, erhält von der Geschäftsstelle unverzüglich, jedoch nicht vor Zulassung der Einzelwahlvorschläge (§ 25 Absatz 3 Satz 1) eine schriftliche Benachrichtigung über ihre Eintragung in das Wählerverzeichnis (Wahlbenachrichtigung).
Die Wahlbenachrichtigung soll enthalten

1.  die Mitteilung, dass die wahlberechtigte Person in das Wählerverzeichnis eingetragen ist,
    
2.  den Hinweis, dass die wahlberechtigte Person ihr Wahlrecht ausschließlich durch Briefwahl mit den beigefügten Briefwahlunterlagen ausüben kann,
    
3.  die Angabe des letzten Tages der Briefwahl und des Endes der Wahlzeit.
    

(2) Der Wahlbenachrichtigung sind folgende Briefwahlunterlagen beizufügen

1.  ein Wahlschein,
    
2.  ein Wahlbriefumschlag,
    
3.  ein Stimmzettelumschlag,
    
4.  ein Stimmzettel und
    
5.  ein Merkblatt zur Briefwahl.
    

(3) Auf dem Wahlbriefumschlag sind die vollständige Anschrift der Geschäftsstelle und der Vermerk „Wahlbrief“ anzugeben.
Der Wahlbriefumschlag ist von der Geschäftsstelle freizumachen; dies entfällt, wenn die Briefwahlunterlagen an einem außerhalb der Bundesrepublik Deutschland liegenden Ort übersandt werden.

### § 14   
Bekanntmachung des Verfahrens der Eintragung in das Wählerverzeichnis und der Erteilung von Briefwahlunterlagen

(1) Die Geschäftsstelle macht spätestens am 48.
Tag vor dem letzten Tag der Briefwahl öffentlich bekannt,

1.  wie lange und zu welchen Tageszeiten bei der Geschäftsstelle das Wählerverzeichnis nach § 15 eingesehen werden kann,
    
2.  dass jede wahlberechtigte Person nach Maßgabe des § 15 das Recht hat, die Richtigkeit ihrer im Wählerverzeichnis eingetragenen Daten zu überprüfen sowie das Wählerverzeichnis einzusehen,
    
3.  wie lange, in welcher Form und unter welchen Voraussetzungen gemäß § 12 Anträge auf Eintragung in das Wählerverzeichnis gestellt werden und dass antragstellende Personen, für die im Melderegister eine Auskunftssperre eingetragen ist, anstelle ihrer Anschrift eine Erreichbarkeitsanschrift angeben können,
    
4.  dass bei der Geschäftsstelle innerhalb der Einsichtsfrist schriftlich oder durch Erklärung zur Niederschrift Einspruch gegen das Wählerverzeichnis eingelegt werden kann,
    
5.  dass jeder wahlberechtigten Person, die auf Antrag in das Wählerverzeichnis eingetragen worden ist, unverzüglich, jedoch nicht vor Zulassung der Einzelwahlvorschläge (§ 25 Absatz 3 Satz 1) eine Wahlbenachrichtigung und Briefwahlunterlagen übersandt werden sowie
    
6.  wie durch Briefwahl gewählt wird.
    

(2) Die Bekanntmachung nach Absatz 1 ist mindestens im Amtsblatt für Brandenburg und in einer im angestammten Siedlungsgebiet der Sorben/Wenden verbreiteten regionalen Tageszeitung zu veröffentlichen.

### § 15  
Einsichtnahme in das Wählerverzeichnis

Jede wahlberechtigte Person hat das Recht, an den Werktagen vom 20.
bis 16.
Tag vor dem letzten Tag der Briefwahl (Einsichtsfrist) während der allgemeinen Öffnungszeiten der Geschäftsstelle die Richtigkeit oder Vollständigkeit der zu ihrer Person im Wählerverzeichnis eingetragenen Daten zu überprüfen.
Zur Überprüfung der Richtigkeit oder Vollständigkeit der Daten von anderen im Wählerverzeichnis eingetragenen Personen haben wahlberechtigte Personen während der Einsichtsfrist nur dann ein Recht auf Einsicht in das Wählerverzeichnis, wenn sie Tatsachen glaubhaft machen, aus denen sich eine Unrichtigkeit oder Unvollständigkeit des Wählerverzeichnisses ergeben kann.

### § 16   
Einspruch gegen das Wählerverzeichnis und Beschwerde

(1) Wer das Wählerverzeichnis für unrichtig oder unvollständig hält, kann schriftlich oder zur Niederschrift Einspruch gegen das Wählerverzeichnis einlegen.
Der Einspruch ist innerhalb der Einsichtsfrist (§ 15 Satz 1) bei der Geschäftsstelle einzulegen.
Der Einspruch kann auf die Aufnahme einer neuen Eintragung oder auf die Streichung oder Berichtigung einer vorhandenen Eintragung gerichtet sein.
Soweit die behaupteten Tatsachen nicht offenkundig sind, hat die einspruchsführende Person die erforderlichen Beweismittel beizubringen.

(2) Die Geschäftsstelle entscheidet binnen drei Tagen über den Einspruch.
Die Entscheidung ist der einspruchsführenden Person unverzüglich bekannt zu geben.
Einem Antrag auf Streichung einer Person darf im Regelfall erst stattgegeben werden, nachdem ihr Gelegenheit zur Äußerung gegeben worden ist.

(3) Gegen die Entscheidung der Geschäftsstelle kann innerhalb von zwei Tagen nach Bekanntgabe bei ihr Beschwerde an die Wahlleiterin oder den Wahlleiter erhoben werden.
Die Wahlleiterin oder der Wahlleiter entscheidet spätestens am siebten Tag vor dem letzten Tag der Briefwahl über die Beschwerde und trägt dafür Sorge, dass die Geschäftsstelle sofort im Besitz der Entscheidung ist.
Die Entscheidung ist den Beteiligten durch die Geschäftsstelle mitzuteilen und in dem Wählerverzeichnis zu vermerken.

(4) Wird eine dritte Person durch den Einspruch negativ betroffen, so hat die Geschäftsstelle der betroffenen Person dieses unverzüglich mitzuteilen.
Eine dem Einspruch abhelfende Verfügung ist der betroffenen Person sofort mitzuteilen.
Die betroffene Person kann innerhalb von zwei Tagen nach Bekanntgabe der Verfügung bei der Geschäftsstelle Beschwerde erheben; Absatz 3 Satz 2 und 3 gilt entsprechend.

(5) Wird aufgrund eines Einspruchs gegen das Wählerverzeichnis entschieden, dass eine wahlberechtigte Person in das Wählerverzeichnis einzutragen ist, so wird sie nachgetragen; die wahlberechtigte Person erhält unverzüglich, jedoch nicht vor Zulassung der Einzelwahlvorschläge (§ 25 Absatz 3 Satz 1) eine Wahlbenachrichtigung und Briefwahlunterlagen.
Wird entschieden, dass eine eingetragene Person nicht wahlberechtigt ist, so ist ihr Name zu streichen.
Nachträge, Streichungen und alle sonstigen Entscheidungen im Einspruchsverfahren sind im Wählerverzeichnis in der Spalte „Bemerkungen“ zu erläutern sowie mit Datum und Unterschrift der verantwortlichen Person, im automatisierten Verfahren mit einem Hinweis auf die verantwortliche Person zu versehen.

### § 17   
Abschluss des Wählerverzeichnisses

(1) Die Geschäftsstelle schließt das Wählerverzeichnis frühestens am zweiten Tag vor dem letzten Tag der Briefwahl und spätestens am letzten Tag der Briefwahl vor dem Ende der Wahlzeit ab.
Sie stellt dabei die Zahl der in das Wählerverzeichnis eingetragenen wahlberechtigten Personen fest.
Bei automatisierter Führung des Wählerverzeichnisses ist ein Ausdruck herzustellen.
Der Abschluss des Wählerverzeichnisses ist mit Datum und Unterschrift der verantwortlichen Person zu versehen.

(2) Nach Abschluss des Wählerverzeichnisses sind Änderungen nicht mehr zulässig.

### § 18   
Aufforderung zur Einreichung von Einzelwahlvorschlägen

(1) Die Wahlleiterin oder der Wahlleiter fordert durch öffentliche Bekanntmachung zur möglichst frühzeitigen Einreichung von Einzelwahlvorschlägen auf und gibt bekannt, wo und bis zu welchem Zeitpunkt die Einzelwahlvorschläge eingereicht werden müssen, und weist auf die Bestimmungen über den Inhalt und die Form der Einzelwahlvorschläge sowie die mit den Einzelwahlvorschlägen vorzulegenden Erklärungen, Niederschriften und Versicherungen hin.

(2) Die Bekanntmachung nach Absatz 1 ist mindestens im Amtsblatt für Brandenburg und in einer im angestammten Siedlungsgebiet der Sorben/Wenden verbreiteten regionalen Tageszeitung zu veröffentlichen.

### § 19   
Wahlvorschlagsrecht

(1) Jede Vereinigung (§ 2 Absatz 3) kann bis zu zehn Einzelwahlvorschläge einreichen.
Jeder Einzelwahlvorschlag darf nur eine Bewerberin oder einen Bewerber enthalten.
Eine Bewerberin oder ein Bewerber darf nur auf einem Einzelwahlvorschlag benannt werden.

(2) Die Einzelwahlvorschläge sind bis 16 Uhr des 48.
Tages vor dem letzten Tag der Briefwahl bei der Wahlleiterin oder dem Wahlleiter schriftlich einzureichen.

### § 20   
Inhalt und Form der Einzelwahlvorschläge

(1) Die Einzelwahlvorschläge sind schriftlich bei der Wahlleiterin oder dem Wahlleiter einzureichen.
Jeder Einzelwahlvorschlag muss enthalten

1.  den Familiennamen, den Vornamen (bei mehreren Vornamen den oder die Rufnamen), den Beruf oder die Tätigkeit, das Geburtsdatum, den Geburtsort und die Wohnanschrift der Bewerberin oder des Bewerbers,
    
2.  den satzungsgemäßen Namen und, sofern vorhanden, die satzungsgemäße Kurzbezeichnung der einreichenden Vereinigung (§ 2 Absatz 3).
    

(2) Jeder Einzelwahlvorschlag soll Namen, Anschrift und, soweit möglich, den Telekommunikationsanschluss der Vertrauensperson und der stellvertretenden Vertrauensperson enthalten.
Werden keine Vertrauenspersonen benannt, so gilt die Person, die als erste den Einzelwahlvorschlag nach Absatz 3 unterzeichnet hat, als Vertrauensperson, und diejenige, die als zweite unterzeichnet hat, als stellvertretende Vertrauensperson.
Wird nur eine Vertrauensperson benannt, so gilt die Person, die als erste den Einzelwahlvorschlag nach Absatz 3 unterzeichnet hat, als stellvertretende Vertrauensperson.
Soweit in dieser Verordnung nichts anderes bestimmt ist, sind nur die Vertrauensperson und die stellvertretende Vertrauensperson, jede für sich, berechtigt, verbindliche Erklärungen zum Einzelwahlvorschlag abzugeben und entgegenzunehmen.
Es ist zulässig, die Bewerberin oder den Bewerber als Vertrauensperson oder stellvertretende Vertrauensperson zu benennen.
Eine Person kann für mehrere Einzelwahlvorschläge derselben Vereinigung als Vertrauensperson oder stellvertretende Vertrauensperson benannt werden.
Die Vertrauensperson und die stellvertretende Vertrauensperson können jederzeit durch schriftliche Erklärung des Vorstandes, der den Einzelwahlvorschlag unterzeichnet hat, durch andere ersetzt werden; die Erklärung an die Wahlleiterin oder den Wahlleiter ist nur wirksam, wenn sie gemäß Absatz 3 unterzeichnet ist.

(3) Jeder Einzelwahlvorschlag muss von mindestens zwei Mitgliedern des Landesvorstandes, darunter der oder dem Vorsitzenden oder einer Stellvertreterin oder einem Stellvertreter, unterzeichnet sein.
Hat die Vereinigung keinen Landesvorstand, so treten anstelle des Landesvorstandes die Vorstände der nächstniedrigeren Gebietsverbände.
Landesvorstand im Sinne des Satzes 1 ist auch der Vorstand eines Gebietsverbandes, der das angestammte Siedlungsgebiet der Sorben/Wenden in den Ländern Brandenburg und Sachsen umfasst.

(4) In einem Einzelwahlvorschlag darf nur aufgenommen werden, wer seine Zustimmung dazu schriftlich erklärt hat.

(5) Die Bewerberin oder der Bewerber hat gegenüber der Wahlleiterin oder dem Wahlleiter an Eides statt zu versichern, dass sie oder er zur Wahl des Landtages wahlberechtigt ist und das 18.
Lebensjahr vollendet hat.
Die Wahlleiterin oder der Wahlleiter ist für die Abnahme einer solchen Versicherung an Eides statt zuständig; sie oder er gilt als Behörde im Sinne des § 156 des Strafgesetzbuches.

(6) Dem Einzelwahlvorschlag sind beizufügen

1.  die Erklärung der Bewerberin oder des Bewerbers, dass sie oder er ihrer oder seiner Aufnahme in den Einzelwahlvorschlag zustimmt,
    
2.  die Bescheinigung über die Eintragung der Bewerberin oder des Bewerbers in das Wählerverzeichnis nach § 12 Absatz 6 Satz 3,
    
3.  die Versicherung an Eides statt der Bewerberin oder des Bewerbers nach Absatz 5 Satz 1.
    

Jede Vereinigung hat zudem ihren Einzelwahlvorschlägen eine Ausfertigung der Niederschrift über die Bestimmung der Bewerberinnen und Bewerber nach § 21 sowie die Satzung der Vereinigung beizufügen.

### § 21   
Bestimmung der Bewerberinnen und Bewerber

(1) Als Bewerberin oder Bewerber auf einen Einzelwahlvorschlag kann in einem Einzelwahlvorschlag nur benannt werden, wer in einer Mitglieder- oder Delegiertenversammlung hierzu gewählt worden ist.

(2) Mitgliederversammlung im Sinne des Absatzes 1 ist eine Versammlung der im Zeitpunkt ihres Zusammentritts zum Landtag wahlberechtigten Mitglieder der Vereinigung.
Delegiertenversammlung im Sinne des Absatzes 1 ist eine Versammlung der von einer derartigen Mitgliederversammlung aus ihrer Mitte gewählten Delegierten.

(3) Zu den Versammlungen sind die Mitglieder oder Delegierten vom Landesvorstand der Vereinigung oder, wenn kein Landesverband besteht, von den Vorständen der nächstniedrigen Gebietsverbände mit einer mindestens dreitägigen Frist entweder einzeln oder durch öffentliche Ankündigung zu laden.
Jede stimmberechtigte Teilnehmerin und jeder stimmberechtigte Teilnehmer der Versammlung ist für die geheime Wahl der Bewerberinnen und Bewerber sowie der Delegierten vorschlagsberechtigt.
Den Bewerberinnen und Bewerbern ist Gelegenheit zu geben, sich und ihr Programm der Versammlung in angemessener Zeit vorzustellen.
In der Versammlung müssen sich mindestens drei Mitglieder oder Delegierte an der Abstimmung beteiligen.

(4) Eine Ausfertigung der Niederschrift über die Wahl der Bewerberinnen und Bewerber mit Angaben über die Art, den Ort und die Zeit der Versammlung, die Form der Einladung, die Anzahl der erschienenen Mitglieder oder Delegierten sowie die Ergebnisse der Wahlen der Bewerberinnen und Bewerber ist mit den Einzelwahlvorschlägen einzureichen.
Hierbei haben die Leiterin oder der Leiter der Versammlung und zwei weitere Mitglieder oder Delegierte, die an der Versammlung teilgenommen haben, gegenüber der Wahlleiterin oder dem Wahlleiter an Eides statt zu versichern, dass die Anforderungen gemäß Absatz 3 beachtet worden sind.
Die Wahlleiterin oder der Wahlleiter ist zur Abnahme einer solchen Versicherung an Eides statt zuständig; sie oder er gilt als Behörde im Sinne des § 156 des Strafgesetzbuches.

(5) Das Nähere über die Einberufung und Beschlussfähigkeit der Versammlungen, die Wahl der Delegierten für die Delegiertenversammlung sowie das Verfahren für die Wahl der Bewerberinnen und Bewerber regeln die Vereinigungen.

### § 22   
Rücktritt und Tod von Bewerberinnen und Bewerbern

(1) Eine Bewerberin oder ein Bewerber kann bis zur Entscheidung über die Zulassung des Einzelwahlvorschlages (§ 25 Absatz 3 Satz 1) von der Bewerbung zurücktreten.
Der Rücktritt ist der Wahlleiterin oder dem Wahlleiter schriftlich oder zur Niederschrift zu erklären und kann nicht widerrufen werden.

(2) Tritt eine Bewerberin oder ein Bewerber vor der Entscheidung über die Zulassung der Einzelwahlvorschläge (§ 25 Absatz 3 Satz 1) von der Bewerbung zurück oder stirbt sie oder er, so gilt der Einzelwahlvorschlag als nicht eingereicht.

(3) Stirbt eine Bewerberin oder ein Bewerber nach der Entscheidung über die Zulassung der Einzelwahlvorschläge (§ 25 Absatz 3 Satz 1), so ist der Tod für die Durchführung der Wahl ohne Einfluss.
Bei der Feststellung der gewählten Mitglieder und Ersatzpersonen scheidet die verstorbene Bewerberin oder der verstorbene Bewerber aus.

### § 23   
Zurückziehung von Einzelwahlvorschlägen

Ein Einzelwahlvorschlag kann bis zur Entscheidung über die Zulassung des Einzelwahlvorschlages (§ 25 Absatz 3 Satz 1) durch gemeinsame Erklärung der Vertrauensperson und der stellvertretenden Vertrauensperson zurückgenommen werden.
Die Zurückziehung des Einzelwahlvorschlages ist der Wahlleiterin oder dem Wahlleiter schriftlich zu erklären und kann nicht widerrufen werden.

### § 24   
Vorprüfung der Einzelwahlvorschläge; Mängelbeseitigung

(1) Die Wahlleiterin oder der Wahlleiter hat auf jedem Einzelwahlvorschlag den Tag des Eingangs und am letzten Tag der Einreichungsfrist (§ 19 Absatz 2) außerdem die Uhrzeit des Eingangs zu vermerken.
Sie oder er hat die Einzelwahlvorschläge unverzüglich nach Eingang auf Ordnungsmäßigkeit und Vollständigkeit zu prüfen.
Stellt sie oder er Mängel fest, die die Gültigkeit eines Einzelwahlvorschlages berühren, so benachrichtigt sie oder er sofort die Vertrauensperson und fordert sie auf, behebbare Mängel rechtzeitig zu beseitigen.
Die Aufforderung zur Beseitigung der Mängel ist aktenkundig zu machen.

(2) Nach Ablauf der Einreichungsfrist (§ 19 Absatz 2) können nur noch Mängel an sich gültiger Einzelwahlvorschläge behoben werden.
Ein gültiger Einzelwahlvorschlag liegt nicht vor, wenn

1.  die Form oder Frist des § 19 Absatz 2 nicht gewahrt ist,
    
2.  der satzungsgemäße Name der einreichenden Vereinigung fehlt,
    
3.  der Einzelwahlvorschlag nicht gemäß § 20 Absatz 3 unterzeichnet ist,
    
4.  der Einzelwahlvorschlag nicht von einer Vereinigung im Sinne des § 5 Absatz 2 Satz 5 des Sorben/Wenden-Gesetzes eingereicht worden ist,
    
5.  die Satzung der Vereinigung fehlt,
    
6.  die Bewerberin oder der Bewerber mangelhaft bezeichnet ist, sodass ihre oder seine Person nicht feststeht, oder
    
7.  die Zustimmungserklärung der Bewerberin oder des Bewerbers (§ 20 Absatz 6 Satz 1 Nummer 1) fehlt.
    

(3) Nach der Entscheidung über die Zulassung eines Einzelwahlvorschlages (§ 25 Absatz 3 Satz 1) ist jede Mängelbeseitigung ausgeschlossen.

(4) Gegen Verfügungen der Wahlleiterin oder des Wahlleiters im Mängelbeseitigungsverfahren kann die Vertrauensperson den Wahlausschuss anrufen.

### § 25   
Zulassung der Einzelwahlvorschläge

(1) Die Wahlleiterin oder der Wahlleiter lädt die Vertrauenspersonen der Einzelwahlvorschläge zu der Sitzung, in der über die Zulassung der Einzelwahlvorschläge entschieden wird.

(2) Die Wahlleiterin oder der Wahlleiter legt dem Wahlausschuss alle eingegangenen Einzelwahlvorschläge vor und berichtet ihm über das Ergebnis der Vorprüfung.

(3) Der Wahlausschuss entscheidet spätestens am 44.
Tag vor dem letzten Tag der Briefwahl über die Zulassung der Einzelwahlvorschläge.
Vor einer Entscheidung ist der erschienenen Vertrauensperson des betroffenen Einzelwahlvorschlages Gelegenheit zur Äußerung zu geben.

(4) Der Wahlausschuss hat Einzelwahlvorschläge zurückzuweisen, wenn sie

1.  verspätet eingereicht sind oder
    
2.  den Anforderungen nicht entsprechen, die durch § 5 des Sorben/Wenden-Gesetzes und diese Verordnung aufgestellt sind.
    

Die Prüfung organisationsinterner Vorgänge der Vereinigung (§ 2 Absatz 3) ist ausgeschlossen.
Die Entscheidung ist in der Sitzung des Wahlausschusses bekannt zu geben.

(5) Die Wahlleiterin oder der Wahlleiter macht die zugelassenen Einzelwahlvorschläge in der Reihenfolge, wie sie in § 26 Absatz 3 bestimmt ist, unverzüglich im Amtsblatt für Brandenburg öffentlich bekannt.
Die Bekanntmachung enthält für jeden Einzelwahlvorschlag die in § 20 Absatz 1 Satz 2 bezeichneten Angaben; statt des Geburtsdatums und der Wohnanschrift sind jedoch jeweils nur das Geburtsjahr und der Wohnort der Bewerberin oder des Bewerbers anzugeben.

### § 26   
Stimmzettel

(1) Der Stimmzettel enthält die zugelassenen Einzelwahlvorschläge unter Angabe von Familiennamen, Vornamen (bei mehreren Vornamen den oder die Rufnamen), Beruf oder Tätigkeit, Geburtsjahr und Wohnort der Bewerberin oder des Bewerbers, den Namen und die etwaige Kurzbezeichnung der Vereinigung sowie rechts von dem Namen einer jeden Bewerberin und eines jeden Bewerbers einen Kreis für die Kennzeichnung.

(2) Die Wahlleiterin oder der Wahlleiter bestimmt die Farbe und Beschaffenheit der Stimmzettel und sonstigen Briefwahlunterlagen.

(3) Die Reihenfolge der Einzelwahlvorschläge auf dem Stimmzettel richtet sich nach den Stimmenzahlen, die die Bewerberinnen und Bewerber bei der letzten unmittelbaren Wahl des Rates erhalten haben; bei Stimmengleichheit entscheidet die alphabetische Reihenfolge der Namen der Bewerberinnen und Bewerber.
Die übrigen Einzelwahlvorschläge schließen sich in der alphabetischen Reihenfolge der Namen der Bewerberinnen und Bewerber an.

### § 27   
Stimmabgabe durch Briefwahl

(1) Für die Stimmabgabe durch Briefwahl gelten folgende Regelungen:

1.  Die Wählerin oder der Wähler gibt die fünf Stimmen in der Weise ab, dass sie oder er auf dem Stimmzettel die Einzelwahlvorschläge (Bewerberinnen und Bewerber), denen sie oder er jeweils eine Stimme geben will, durch Ankreuzen oder auf andere Weise persönlich und unbeobachtet zweifelsfrei kennzeichnet.
    
2.  Wer nicht lesen kann oder wegen einer körperlichen Behinderung nicht in der Lage ist, die Briefwahl persönlich auszuüben, kann sich der Hilfe einer Person seines Vertrauens (Hilfsperson) bedienen.
    
3.  Der gekennzeichnete Stimmzettel ist in den Stimmzettelumschlag zu legen und zu verschließen.
    
4.  Die Wählerin, der Wähler oder die Hilfsperson unterschreibt unter Angabe des Tages die auf dem Wahlschein vorgedruckten Versicherungen an Eides statt zur Wahlberechtigung und zur Briefwahl (Absatz 2 Satz 1 Nummer 1 und 2).
    
5.  Der verschlossene Stimmzettelumschlag und der unterschriebene Wahlschein sind in den Wahlbriefumschlag zu legen.
    
6.  Der Wahlbriefumschlag ist zu verschließen.
    
7.  Der Wahlbrief ist so rechtzeitig an die Geschäftsstelle zu versenden, dass dieser spätestens am letzten Tag der Briefwahl bis zum Ende der Wahlzeit bei der Geschäftsstelle eingeht.
    

(2) Auf dem Wahlschein hat die Wählerin, der Wähler oder die Hilfsperson an Eides statt zu versichern, dass

1.  die Wählerin oder der Wähler am letzten Tag der Briefwahl zum Landtag wahlberechtigt ist und
    
2.  der Stimmzettel persönlich oder nach dem erklärten Willen der Wählerin oder des Wählers gekennzeichnet worden ist.
    

Die Wahlleiterin oder der Wahlleiter ist zur Abnahme einer solchen Versicherung an Eides statt zuständig; sie oder er gilt als Behörde im Sinne des § 156 des Strafgesetzbuches.

(3) Die Hilfeleistung nach Absatz 1 Nummer 2 hat sich auf die Erfüllung der Wünsche der Wählerin oder des Wählers zu beschränken.
Die Hilfsperson ist zur Geheimhaltung dessen verpflichtet, was sie bei der Hilfeleistung von der Wahl einer anderen Person erfahren hat.

### § 28   
Behandlung der Wahlbriefe

(1) Die Geschäftsstelle sammelt die Wahlbriefe ungeöffnet und hält sie unter Verschluss.
Sie vermerkt auf jedem am letzten Tag der Briefwahl nach Schluss der Wahlzeit eingegangenen Wahlbrief Tag und Uhrzeit des Eingangs, auf den vom nächsten Tag an eingehenden Wahlbriefen nur den Eingangstag.

(2) Die Geschäftsstelle verteilt die Wahlbriefe auf die einzelnen Briefwahlvorstände, übergibt jedem Briefwahl-vorstand das Verzeichnis über die für ungültig erklärten Wahlscheine oder die Mitteilung, dass keine Wahlscheine für ungültig erklärt worden sind, sorgt für die Bereitstellung und Ausstattung geeigneter Räume und stellt den Briefwahlvorständen etwa notwendige Hilfskräfte zur Verfügung.

(3) Verspätet eingegangene Wahlbriefe werden von der Geschäftsstelle angenommen, mit den in Absatz 1 vorgeschriebenen Vermerken versehen und ungeöffnet verpackt.
Das Paket wird von ihr versiegelt, mit Inhaltsangabe versehen und verwahrt, bis die Vernichtung der Wahlbriefe zugelassen ist.
Die Geschäftsstelle hat sicherzustellen, dass das Paket unbefugten Personen nicht zugänglich ist.

### § 29   
Zurückweisung von Wahlbriefen

(1) Ein Wahlbrief ist zurückzuweisen, wenn

1.  der Wahlbrief nicht rechtzeitig eingegangen ist,
    
2.  der Wahlbriefumschlag keinen oder keinen gültigen Wahlschein enthält,
    
3.  dem Wahlbriefumschlag kein Stimmzettelumschlag beigefügt ist,
    
4.  weder der Wahlbriefumschlag noch der Stimmzettelumschlag verschlossen ist,
    
5.  der Wahlbriefumschlag mehrere Stimmzettelumschläge, aber nicht die gleiche Anzahl gültiger und mit den vorgeschriebenen Versicherungen an Eides statt zur Wahlberechtigung und Briefwahl versehener Wahlscheine enthält,
    
6.  die Wählerin, der Wähler oder die Hilfsperson die vorgeschriebenen Versicherungen an Eides statt zur Wahlberechtigung und zur Briefwahl nicht unterschrieben hat,
    
7.  kein von der Geschäftsstelle ausgegebener Stimmzettelumschlag benutzt worden ist oder
    
8.  ein von der Geschäftsstelle ausgegebener Stimmzettelumschlag benutzt worden ist, der offensichtlich in einer das Wahlgeheimnis gefährdenden Weise von den übrigen abweicht oder einen deutlich fühlbaren Gegenstand enthält.
    

(2) Die Einsenderin oder der Einsender eines zurückgewiesenen Wahlbriefes wird nicht als Wählerin oder Wähler gezählt; ihre Stimmen gelten als nicht abgegeben.

### § 30   
Ungültige Stimmen

(1) Ungültig sind Stimmen, wenn der Stimmzettel

1.  nicht von der Geschäftsstelle ausgegeben worden ist,
    
2.  keine oder mehr als fünf Kennzeichnungen enthält,
    
3.  den Willen der Wählerin oder des Wählers nicht zweifelsfrei erkennen lässt,
    
4.  einen Zusatz enthält,
    
5.  einen Vorbehalt enthält oder
    
6.  durchgestrichen, durchgerissen oder durchgeschnitten ist.
    

(2) Enthält ein an sich gültiger Stimmzettel mehrere Kennzeichnungen für eine Bewerberin oder für einen Bewerber, so sind diese Kennzeichnungen als eine Stimme für die betreffende Bewerberin oder den betreffenden Bewerber zu werten.

(3) Enthält der Stimmzettel weniger als fünf Kennzeichnungen, so sind die nicht abgegebenen Stimmen als ungültig zu werten.

(4) Enthält der Stimmzettelumschlag mehrere Stimmzettel, so sind diese Stimmzettel als ein ungültiger Stimmzettel zu werten.
Enthält der Stimmzettelumschlag keinen Stimmzettel, so ist dieser nicht abgegebene Stimmzettel als ein ungültiger Stimmzettel zu werten.

### § 31   
Feststellung des Wahlergebnisses durch den Briefwahlvorstand

(1) Unmittelbar nach dem Ende der Wahlzeit ermittelt der Briefwahlvorstand in öffentlicher Sitzung das Briefwahlergebnis.

(2) Ein Mitglied des Briefwahlvorstandes öffnet die Wahlbriefe nacheinander und entnimmt ihnen den Wahlschein und den Stimmzettelumschlag.
Ist der Wahlschein in dem Verzeichnis für ungültig erklärte Wahlscheine (§ 12 Absatz 8 Satz 2) aufgeführt oder werden Bedenken gegen die Gültigkeit des Wahlscheins erhoben, so sind die betroffenen Wahlbriefe samt Inhalt unter Kontrolle der Briefwahlvorsteherin oder des Briefwahlvorstehers auszusondern und später entsprechend Absatz 3 zu behandeln.
Die aus den übrigen Wahlbriefen entnommenen Stimmzettelumschläge werden ungeöffnet in die Wahlurne geworfen; die Wahlscheine werden gesammelt.

(3) Werden gegen einen Wahlbrief Bedenken erhoben, so beschließt der Briefwahlvorstand über die Zulassung oder Zurückweisung.
Der Wahlbrief ist vom Briefwahlvorstand zurückzuweisen, wenn ein Tatbestand nach § 29 Absatz 1 Nummer 2 bis 8 vorliegt.
Die Zahl der beanstandeten, der nach besonderer Beschlussfassung zugelassenen und die Zahl der zurückgewiesenen Wahlbriefe sind in der Niederschrift zu vermerken.
Die zurückgewiesenen Wahlbriefe sind samt Inhalt auszusondern, mit einem Vermerk über den Zurückweisungsgrund zu versehen, wieder zu verschließen und fortlaufend zu nummerieren.

(4) Nachdem die Stimmzettelumschläge den Wahlbriefen entnommen und in die Wahlurne geworfen worden sind, ermittelt und stellt der Briefwahlvorstand das Ergebnis der Briefwahl fest.
Festzustellen sind

1.  die Zahl der Wählerinnen und Wähler,
    
2.  die Zahl der ungültigen Stimmzettel,
    
3.  die Zahl der gültigen Stimmen,
    
4.  die Zahlen der für die einzelnen Bewerberinnen und Bewerber (Einzelwahlvorschläge) abgegebenen Stimmen.
    

(5) Der Briefwahlvorstand entscheidet über die Gültigkeit der abgegebenen Stimmen sowie über alle sich bei der Ermittlung und Feststellung des Briefwahlergebnisses ergebenden Fragen.
Der Wahlausschuss hat das Recht der Nachprüfung.

### § 32   
Zählung der Wählerinnen und Wähler

Für die Ermittlung der Zahl der Wählerinnen und Wähler werden die Stimmzettelumschläge der Wahlurne (§ 31 Absatz 2 Satz 3 erster Teilsatz) entnommen und ungeöffnet gezählt.
Daneben wird die Zahl der gesammelten Wahlscheine (§ 31 Absatz 2 Satz 3 zweiter Teilsatz) gezählt.
Ergibt sich dabei auch nach wiederholter Zählung keine Übereinstimmung, so ist dies in der Niederschrift zu vermerken und, soweit möglich, zu erläutern.
In diesem Fall gilt die Zahl der Stimmzettelumschläge als die Zahl der Wählerinnen und Wähler.

### § 33   
Zählung der Stimmen

(1) Nachdem die Zahl der Wählerinnen und Wähler ermittelt worden ist, werden die abgegebenen Stimmen gezählt.
Die Briefwahlvorsteherin oder der Briefwahlvorsteher oder ein von ihr oder ihm bestimmtes Mitglied des Briefwahlvorstandes liest aus jedem Stimmzettel vor, für welche Bewerberinnen und Bewerber die Stimmen abgegeben worden sind.
Ausgesondert und bei diesem Zählvorgang nicht berücksichtigt werden Stimmzettel,

1.  die ungültig sind,
    
2.  deren Gültigkeit nicht zweifelsfrei ist oder
    
3.  auf denen mindestens eine einzelne Stimme ungültig oder deren Gültigkeit nicht zweifelsfrei ist.
    

Die ausgesonderten Stimmzettel werden von einem Mitglied des Briefwahlvorstandes in Verwahrung genommen.

(2) Anschließend entscheidet der Briefwahlvorstand über die Gültigkeit der ausgesonderten Stimmzettel und die Gültigkeit der auf ihnen abgegebenen Stimmen.
Die Briefwahlvorsteherin oder der Briefwahlvorsteher gibt die Entscheidung mündlich bekannt.
Sie oder er vermerkt auf der Rückseite des Stimmzettels, ob er für gültig oder für ungültig erklärt worden ist.
Auf der Rückseite eines jeden für gültig erklärten Stimmzettels vermerkt die Briefwahlvorsteherin oder der Briefwahlvorsteher, für welche Bewerberinnen und für welche Bewerber die Stimmen gezählt worden sind.
Die Stimmzettel, über die der Briefwahlvorstand nach Satz 1 besonders entschieden hat, sind mit fortlaufenden Nummern zu versehen und der Niederschrift beizufügen.

(3) Bei den Zählungen der ungültigen Stimmzettel und der gültigen Stimmen sollen Zähllisten geführt werden.
Die Zahl der ungültigen Stimmzettel und die Zahlen der für die einzelnen Bewerberinnen und Bewerber abgegebenen Stimmen werden in die Niederschrift übertragen.

(4) Beantragt ein Mitglied des Briefwahlvorstandes vor Unterzeichnung der Niederschrift eine erneute Zählung der Stimmen, so ist diese nach den Absätzen 1 bis 3 zu wiederholen.
Die Gründe für die erneute Zählung sind in der Niederschrift zu vermerken.

(5) Die Briefwahlvorsteherin oder der Briefwahlvorsteher gibt das vom Briefwahlvorstand festgestellte Ergebnis der Briefwahl mit den in § 31 Absatz 4 genannten Angaben mündlich bekannt.

### § 34  
Vorläufiges Wahlergebnis

(1) Sobald der Briefwahlvorstand das Ergebnis der Briefwahl festgestellt hat, meldet es ein Mitglied des Briefwahlvorstandes der Wahlleiterin oder dem Wahlleiter.

(2) Die Wahlleiterin oder der Wahlleiter ermittelt das vorläufige Wahlergebnis im Land.
Die Wahlleiterin oder der Wahlleiter gibt dieses vorläufige Wahlergebnis mündlich oder in geeigneter anderer Form bekannt.

### § 35   
Niederschrift

(1) Über die Ermittlung und Feststellung des Briefwahlergebnisses ist von der Schriftführerin oder dem Schriftführer eine Niederschrift zu fertigen.
Die Niederschrift ist von den Mitgliedern des Briefwahlvorstandes zu genehmigen und zu unterzeichnen.
Verweigert ein Mitglied des Briefwahlvorstandes die Unterschrift, so ist der Grund hierfür in der Niederschrift zu vermerken.
Der Niederschrift sind beizufügen:

1.  die Wahlbriefe, die der Briefwahlvorstand zurückgewiesen hat,
    
2.  die Stimmzettel und Stimmzettelumschläge, über die der Briefwahlvorstand besonders beschlossen hat,
    
3.  die Wahlscheine, über die der Briefwahlvorstand beschlossen hat, ohne dass die Wahlbriefe zurückgewiesen wurden.
    

(2) Die Briefwahlvorsteherin oder der Briefwahlvorsteher hat die Niederschrift mit den Anlagen unverzüglich der Wahlleiterin oder dem Wahlleiter zu übergeben.

(3) Die Briefwahlvorsteherinnen und die Briefwahlvorsteher, die Geschäftsstelle und die Wahlleiterin oder der Wahlleiter haben sicherzustellen, dass die Niederschriften mit den Anlagen unbefugten Personen nicht zugänglich sind.

### § 36   
Übergabe und Verwahrung der Wahlunterlagen

(1) Hat der Briefwahlvorstand seine Aufgabe erledigt, so verpackt ein Mitglied des Briefwahlvorstandes jeweils getrennt

1.  die Stimmzettel und
    
2.  die Wahlscheine,
    

soweit sie nicht der nach § 35 gefertigten Niederschrift beigefügt sind, versiegelt die einzelnen Pakete, versieht sie mit Inhaltsangabe und übergibt sie der Geschäftsstelle.
Bis zur Übergabe an die Geschäftsstelle hat die Briefwahlvorsteherin oder der Briefwahlvorsteher sicherzustellen, dass die in Satz 1 aufgeführten Unterlagen unbefugten Personen nicht zugänglich sind.

(2) Die Geschäftsstelle hat die Pakete zu verwahren, bis die Vernichtung der Wahlunterlagen zugelassen ist.
Sie hat sicherzustellen, dass die Pakete unbefugten Personen nicht zugänglich sind.

(3) Die Geschäftsstelle hat die in Absatz 1 bezeichneten Unterlagen auf Anforderung der Wahlleiterin oder dem Wahlleiter vorzulegen.

### § 37   
Feststellung des Wahlergebnisses im Land durch den Wahlausschuss

(1) Die Wahlleiterin oder der Wahlleiter prüft die Niederschriften der Briefwahlvorstände und ermittelt mit Hilfe der Niederschriften und des Wählerverzeichnisses das Wahlergebnis im Land.

(2) Nach Berichterstattung durch die Wahlleiterin oder den Wahlleiter ermittelt der Wahlausschuss das endgültige Wahlergebnis im Land.
Er stellt fest

1.  die Zahl der im Wählerverzeichnis eingetragenen Wahlberechtigten,
    
2.  die Zahl der Wählerinnen und Wähler,
    
3.  die Zahl der ungültigen Stimmzettel,
    
4.  die Zahl der gültigen Stimmen,
    
5.  die Zahlen der für die einzelnen Bewerberinnen und Bewerber (Einzelwahlvorschläge) abgegebenen gültigen Stimmen,
    
6.  die gewählten Bewerberinnen und Bewerber,
    
7.  die Ersatzpersonen und ihre Reihenfolge.
    

Der Wahlausschuss ist berechtigt, rechnerische Berichtigungen an den Feststellungen der Briefwahlvorstände vorzunehmen.

(3) Ist eine Losentscheidung erforderlich, so beauftragt der Wahlausschuss eines seiner Mitglieder mit der Herstellung des Loses.
Die Bewerberinnen und Bewerber sowie die Wahlleiterin oder der Wahlleiter dürfen bei der Herstellung des Loses nicht anwesend sein.
Bei der Ziehung des Loses durch die Wahlleiterin oder den Wahlleiter dürfen zwar die Bewerberinnen und Bewerber, jedoch nicht das Ausschussmitglied, das das Los hergestellt hat, anwesend sein.
Die Entscheidung durch das Los ist Bestandteil des Wahlverfahrens.

(4) Im Anschluss an die Ermittlung und Feststellung gibt die Wahlleiterin oder der Wahlleiter das endgütige Wahlergebnis mit den in Absatz 2 Satz 2 bezeichneten Angaben mündlich bekannt.

(5) Über die Sitzung ist eine Niederschrift zu fertigen.

(6) Die Wahlleiterin oder der Wahlleiter teilt der Präsidentin des Landtages oder dem Präsidenten des Landtages die Familiennamen, Vornamen, Anschriften und Stimmenzahlen der gewählten Bewerberinnen und gewählten Bewerber sowie der Ersatzpersonen mit.

#### § 38   
Bekanntmachung des endgültigen Wahlergebnisses

(1) Die Wahlleiterin oder der Wahlleiter macht das vom Wahlausschuss festgestellte endgültige Wahlergebnis mit den in § 37 Absatz 2 Satz 2 bezeichneten Angaben im Amtsblatt für Brandenburg bekannt.

(2) Die Wahlleiterin oder der Wahlleiter übersendet der Präsidentin des Landtages oder dem Präsidenten des Landtages eine Ausfertigung der Bekanntmachung nach Absatz 1.

### § 39   
Benachrichtigung der gewählten Bewerberinnen und Bewerber

Die Wahlleiterin oder der Wahlleiter benachrichtigt die vom Wahlausschuss für gewählt erklärten Bewerberinnen und Bewerber nach der mündlichen Bekanntgabe des endgültigen Wahlergebnisses und weist sie auf die Vorschriften des § 40 Absatz 1 hin.

### § 40   
Annahme der Wahl

(1) Die gewählte Bewerberin oder der gewählte Bewerber hat die Wahl mit dem Eingang der auf die Benachrichtigung nach § 39 erfolgenden schriftlichen Annahmeerklärung angenommen.
Gibt die gewählte Bewerberin oder der gewählte Bewerber binnen einer Woche nach Zugang der Benachrichtigung nach § 39 keine schriftliche Erklärung ab, so gilt die Wahl mit Ablauf dieser Wochenfrist als angenommen.
Eine Erklärung unter Vorbehalt gilt als Ablehnung.
Die Annahme- oder Ablehnungserklärung kann nicht widerrufen werden.

(2) Die Wahlleiterin oder der Wahlleiter teilt der Präsidentin des Landtages oder dem Präsidenten des Landtages unverzüglich mit, welche Bewerberinnen und Bewerber die Wahl angenommen haben und bei welchen Bewerberinnen und Bewerbern die Wahl nach Absatz 1 Satz 2 als angenommen gilt sowie welche Bewerberinnen und Bewerber die Wahl abgelehnt haben.

### § 41   
Verlust der Mitgliedschaft im Rat

(1) Ein Mitglied verliert die Mitgliedschaft im Rat durch

1.  Verzicht,
    
2.  Ungültigkeit des Erwerbs der Mitgliedschaft,
    
3.  Neufeststellung des Wahlergebnisses,
    
4.  Wegfall der Voraussetzung der Wahlberechtigung oder
    
5.  Wegfall der Gründe für die Berufung als Ersatzperson.
    

Verlustgründe nach anderen gesetzlichen Vorschriften bleiben unberührt.

(2) Der Verzicht ist nur wirksam, wenn er der Präsidentin des Landtages oder dem Präsidenten des Landtages mündlich zur Niederschrift oder schriftlich erklärt wird.
Der Verzicht kann nicht widerrufen werden.
Der Verzicht kann auf einen Tag in die Zukunft gerichtet sein.

(3) Die Präsidentin des Landtages oder der Präsident des Landtages stellt den Verlust der Mitgliedschaft im Rat nach Absatz 1 fest, soweit der Verlust nicht durch rechtskräftigen Richterspruch eingetreten ist.

(4) Durch das Ausscheiden eines Mitgliedes wird die Rechtswirksamkeit seiner bisherigen Tätigkeit nicht berührt.

### § 42   
Berufung von Ersatzpersonen

(1) Wenn eine gewählte Bewerberin oder ein gewählter Bewerber stirbt oder die Annahme der Wahl ablehnt oder wenn ein Mitglied des Rates stirbt oder sonst nachträglich aus dem Rat ausscheidet, geht der Sitz auf die nächste noch nicht für gewählt erklärte Ersatzperson über.

(2) Die Reihenfolge der Ersatzpersonen richtet sich nach der Höhe der auf sie entfallenden Stimmenzahlen.
Bei gleichen Stimmenzahlen entscheidet das von der Wahlleiterin oder dem Wahlleiter zu ziehende Los; § 37 Absatz 3 gilt entsprechend.

(3) Die Feststellungen nach den Absätzen 1 und 2 trifft die Präsidentin des Landtages oder der Präsident des Landtages.

(4) Die Präsidentin des Landtages oder der Präsident des Landtages benachrichtigt die Ersatzperson und weist sie auf die Vorschrift des § 40 Absatz 1 hin.

### § 43   
Wahlanfechtung

(1) Jeder Dachverband (§ 2 Absatz 2), jede Vereinigung (§ 2 Absatz 3), die einen Einzelwahlvorschlag eingereicht hat, jede Bewerberin und jeder Bewerber, jede wahlberechtigte Person, die gemäß § 12 einen Antrag auf Eintragung in das Wählerverzeichnis gestellt hat, sowie die Wahlleiterin oder der Wahlleiter können die Wahl beim Verwaltungsgericht anfechten mit der Begründung, dass die Wahl nicht den gesetzlichen Vorschriften entsprechend vorbereitet und durchgeführt oder in anderer unzulässiger Weise in ihrem Ergebnis beeinflusst worden ist.
Die Wahlanfechtung kann nicht darauf gestützt werden, dass ein Einzelwahlvorschlag zu Unrecht zugelassen worden ist.
Die Wahl bleibt gültig, wenn der festgestellte Verstoß das Wahlergebnis nicht oder nur unwesentlich beeinflusst hat.

(2) Die Wahlanfechtung nach Absatz 1 hat frühestens am letzten Tag der Briefwahl und spätestens zwei Wochen nach Bekanntmachung des Wahlergebnisses (§ 38 Absatz 1) zu erfolgen.

(3) Entscheidungen und Maßnahmen, die sich unmittelbar auf das Wahlverfahren beziehen, können nur mit den Rechtsbehelfen, die in dem Sorben/Wenden-Gesetz oder in dieser Verordnung vorgesehen sind, sowie im Wahlanfechtungsverfahren angefochten werden.

(4) Die Wahlanfechtung hat keine aufschiebende Wirkung.

(5) Wird die Wahl für ungültig erklärt, so wird dadurch die Wirksamkeit der bis zur Rechtskraft der Entscheidung gefassten Beschlüsse des Rates nicht berührt.

### § 44   
Wiederholungswahl

(1) Wird die Wahl teilweise oder ganz für ungültig erklärt, so ist die Wahl in dem in der Entscheidung bestimmten Umfang zu wiederholen.
Die Wiederholungswahl soll spätestens sechs Monate nach Rechtskraft der Entscheidung erfolgen.

(2) Für die Wiederholungswahl gelten im Übrigen die Vorschriften des Sorben/Wenden-Gesetzes und dieser Verordnung.

### § 45   
Stimmzettel und Vordrucke

(1) Die Geschäftsstelle beschafft die Stimmzettel, die Umschläge und Merkblätter für die Briefwahl sowie die sonstigen für die Vorbereitung und Durchführung der unmittelbaren Wahl des Rates erforderlichen Vordrucke.

(2) Die Wahlleiterin oder der Wahlleiter kann zum Inhalt und zur Form der Vordrucke nähere Regelungen treffen.
Sie oder er bestimmt, welche Vordrucke auch in sorbischer/wendischer Sprache zu fassen sind.

### § 46   
Hilfskräfte

(1) Die Geschäftsstelle stellt dem Wahlausschuss und den Briefwahlvorständen die für ihre Tätigkeit erforderlichen Hilfskräfte und Hilfsmittel zur Verfügung.
Die Wahlleiterin oder der Wahlleiter weist jede Hilfskraft auf ihre Verpflichtung zur Verschwiegenheit über die ihr bei ihrer Tätigkeit bekannt gewordenen Angelegenheiten hin.

(2) Die Hilfskräfte nach Absatz 1 können bei der Ermittlung des Briefwahlergebnisses sowie bei der Erstellung der Niederschriften mitwirken.

### § 47   
Sicherung der Wahlunterlagen

(1) Wahlunterlagen sind so zu verwahren, dass sie gegen Einsichtnahme durch unbefugte Personen geschützt sind.
Dies gilt insbesondere für

1.  das Wählerverzeichnis und
    
2.  das Verzeichnis über die für ungültig erklärten Wahlscheine nach § 12 Absatz 8 Satz 2.
    

(2) Auskünfte aus dem Wählerverzeichnis und dem Verzeichnis über die für ungültig erklärten Wahlscheine nach § 12 Absatz 8 Satz 2 dürfen nur Gerichten und der Strafverfolgungsbehörde zur Ermittlung einer Straftat im Zusammenhang mit der Wahl erteilt werden.

### § 48   
Vernichtung der Wahlunterlagen

(1) Das Wählerverzeichnis und das Verzeichnis über die für ungültig erklärten Wahlscheine nach § 12 Absatz 8 Satz 2 sind sechs Monate nach der Bekanntmachung des endgültigen Wahlergebnisses (§ 38 Absatz 1) zu vernichten, wenn nicht die Wahlleiterin oder der Wahlleiter mit Rücksicht auf ein schwebendes Wahlanfechtungsverfahren etwas anderes anordnet oder sie für die Strafverfolgungsbehörde zur Ermittlung einer Straftat im Zusammenhang mit der Wahl von Bedeutung sein können.

(2) Die übrigen Wahlunterlagen können 60 Tage vor der Neuwahl vernichtet werden.
Die Wahlleiterin oder der Wahlleiter kann abweichend von Satz 1 zulassen, dass Wahlunterlagen früher vernichtet werden, soweit sie nicht für ein schwebendes Wahlanfechtungsverfahren oder für die Strafverfolgungsbehörde zur Ermittlung einer Straftrat im Zusammenhang mit der Wahl von Bedeutung sein können.

(3) Die Niederschriften des Wahlausschusses und der Briefwahlvorstände zählen nicht zu den Wahlunterlagen nach Absatz 2 Satz 1.

### § 49   
Wahlkosten

Das Land erstattet der Geschäftsstelle die für die Vorbereitung und Durchführung der Wahl veranlassten notwendigen Sachkosten im Wege der Einzelabrechnung.
Zu den notwendigen Sachkosten im Sinne des Satzes 1 zählen auch die Kosten für Übersetzungen von Bekanntmachungen und Vordrucken in sorbischer/wendischer Sprache.
Bei der Festsetzung der Kosten werden laufende sächliche Kosten sowie Kosten für die Benutzung von Räumen der Geschäftsstelle nicht berücksichtigt.

### § 50  
Fristen und Termine sowie Schriftform

(1) Die in dieser Verordnung vorgesehenen Fristen und Termine verlängern und ändern sich nicht dadurch, dass der letzte Tag der Frist oder ein Termin auf einen Sonnabend, einen Sonntag oder einen gesetzlichen Feiertag fällt.
Eine Wiedereinsetzung in den vorherigen Stand ist ausgeschlossen.

(2) Soweit in dieser Verordnung nichts anderes bestimmt ist, müssen vorgeschriebene Erklärungen persönlich und handschriftlich unterzeichnet sein und bei der zuständigen Stelle im Original vorliegen; die elektronische Form ist ausgeschlossen.

### § 51   
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 15.
September 2014

Der Minister des Innern

Ralf Holzschuher

* * *

_**Hinweis der Redaktion:**_

_Die beigefügte Anlage enthält die Übersetzung der Wahlordnung zum Sorben/Wenden-Gesetz in der Fassung vom 15. September 2014 in die niedersorbische Sprache.
Es handelt sich dabei um eine nichtamtliche Textversion als Service der BRAVORS-Redaktion, nicht um den verkündeten Verordnungswortlaut._

[Nichtamtliche Textversion der Wahlordnung zum Sorben/Wenden-Gesetz](/br2/sixcms/media.php/68/WAHLORDNUNG_ZUM_SORBEN_WENDEN-GESETZ_IN_SORBISCHER_SPRACHE.pdf "Nichtamtliche Textversion der Wahlordnung zum Sorben/Wenden-Gesetz")