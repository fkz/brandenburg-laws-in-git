## Verordnung über Zuständigkeiten im Gewerberecht (Gewerberechtszuständigkeitsverordnung - GewRZV)

Auf Grund des § 155 Absatz 2 der Gewerbeordnung in der Fassung der Bekanntmachung vom 22. Februar 1999 (BGBl.
I S. 202) in Verbindung mit § 6 Absatz 2 und 4 und § 12 Absatz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S.
186), die durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28) geändert worden sind, § 2 Absatz 3 und 4 der Kommunalverfassung des Landes Brandenburg vom 18.
Dezember 2007 (GVBl.
I S.
286) und § 2 Nummer 7 der Verordnung über wirtschaftsrechtliche Zuständigkeiten vom 7.
September 2009 (GVBl.
II S.
604) verordnet der Minister für Wirtschaft, Arbeit und Energie:

#### § 1 Zuständigkeit

(1) Für den Vollzug der Gewerbeordnung und der darauf beruhenden Rechtsverordnungen sind die örtlichen Ordnungsbehörden zuständig, soweit in den folgenden Absätzen 3 bis 6 oder in anderen Rechtsvorschriften nichts anderes bestimmt ist.

(2) Neben den örtlichen Ordnungsbehörden haben auch die Kreisordnungsbehörden, das Landeskriminalamt, die Industrie- und Handelskammern, das für Gesundheit zuständige Mitglied der Landesregierung sowie das für Wirtschaft zuständige Mitglied der Landesregierung im Rahmen ihrer Zuständigkeiten die Befugnis zur Entgegennahme von Auskünften und die Beauftragung von Personen nach § 29 der Gewerbeordnung.

(3) Das für Gesundheit zuständige Mitglied der Landesregierung ist zuständig für den Vollzug der §§ 30 Absatz 1, 144 Absatz 1 Nummer 1 Buchstabe b der Gewerbeordnung sowie für Fristverlängerungen gemäß § 49 Absatz 3 der Gewerbeordnung, sofern es sich um eine Erlaubnis nach § 30 der Gewerbeordnung handelt.

(4) Das für Wirtschaft zuständige Mitglied der Landesregierung ist zuständig für den Vollzug des § 34b Absatz 5 der Gewerbeordnung.

(5) Die Kreisordnungsbehörden sind zuständig für den Vollzug

1.  der §§ 69 Absatz 1 und 3, 69a Absatz 2, 69b Absatz 1 und 3 und 71b Absatz 2 Satz 2 der Gewerbeordnung, sofern es sich um Messen (§ 64 der Gewerbeordnung), Ausstellungen (§ 65 der Gewerbeordnung) oder Großmärkte (§ 66 der Gewerbeordnung) handelt und
2.  des § 146 Absatz 2 Nummer 6 und 7 der Gewerbeordnung, sofern es sich um Messen (§ 64 der Gewerbeordnung), Ausstellungen (§ 65 der Gewerbeordnung) oder Großmärkte (§ 66 der Gewerbeordnung) handelt.

(6) Die Industrie- und Handelskammern sind zuständig für den Vollzug des § 46 Absatz 3 der Gewerbeordnung, sofern es sich um eine Erlaubnis nach § 34d der Gewerbeordnung handelt.

(7) Die für die Erteilung einer Erlaubnis, Genehmigung, Konzession oder sonstigen Berechtigung, für eine Festsetzung oder öffentliche Bestellung zuständige Stelle entscheidet auch über deren Versagung, Rücknahme, Widerruf, Entziehung, Änderung, Aufhebung oder Ablehnung.
Sie entscheidet auch über die Ausübung eines Gewerbebetriebes durch einen Stellvertreter.

(8) Ändern sich Zuständigkeiten, so führen die bisher zuständigen Stellen das Verfahren bis zur letzten Verwaltungsentscheidung zu Ende.

#### § 2 Kostenerstattung

Das Land erstattet den zuständigen Behörden die mit der Anwendung dieser Verordnung verbundenen notwendigen Kosten einschließlich der Personal- und Sachkosten, soweit dieser finanzielle Aufwand nicht durch Gebühren ausgeglichen werden kann.
Der eine Gebührenerhebung übersteigende nachgewiesene finanzielle Aufwand wird den zuständigen Behörden nach Ablauf eines Haushaltsjahres von dem für Wirtschaft zuständigen Mitglied der Landesregierung auf Antrag erstattet.

#### § 3 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Gewerberechtszuständigkeitsverordnung vom 17.
August 2009 (GVBl.
II S.
527), die zuletzt durch Verordnung vom 23.
Juli 2018 (GVBl.
II Nr.
47) geändert worden ist, außer Kraft.

Potsdam, den 22.
Juli 2020

Der Minister für Wirtschaft,  
Arbeit und Energie

Prof.
Dr.-Ing.
Jörg Steinbach