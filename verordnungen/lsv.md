## Verordnung über die Anforderungen an das Lehramtsstudium an den Hochschulen im Land Brandenburg  (Lehramtsstudienverordnung - LSV)

Auf Grund des § 3 Absatz 6, des § 4 Absatz 2 und des § 18 Absatz 9 des Brandenburgischen Lehrerbildungsgesetzes vom 18.
Dezember 2012 (GVBl.
I Nr.
45) verordnet die Ministerin für Bildung, Jugend und Sport im Einvernehmen mit der Ministerin für Wissenschaft, Forschung und Kultur:

**Inhaltsübersicht**

Teil 1  
Allgemeine Bestimmungen
--------------------------------

[§ 1           Geltungsbereich](#_1)  
[§ 2           Strukturelle Anforderungen](#_2)  
[§ 3           Akkreditierung](#_3)  
[§ 4           Zugang zum Masterstudium](#_4)  
[§ 5           Inhaltliche Anforderungen](#_5)  
[§ 6           Schulpraktische Studien](#_6)  
[§ 7           Masterzeugnis](#_7)

Teil 2  
Lehramtsbezogene Bestimmungen
--------------------------------------

### Abschnitt 1  
Lehramt für die Primarstufe

[§ 8           Fächer, Studienbereiche](#_8)  
[§ 9           Inklusionspädagogische Schwerpunktbildung](#_9)  
[§ 10         Leistungspunkte, Masterarbeit](#_10)

### Abschnitt 2  
Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer)

[§ 11         Fächer, Studienbereiche](#_11)  
[§ 12         Schwerpunktbildung](#_12)  
[§ 13         Leistungspunkte, Masterarbeit](#_13)

### Abschnitt 3  
Lehramt für die Sekundarstufe II (berufliche Fächer)

[§ 14         Fächer, Studienbereiche](#_14)  
[§ 15         Leistungspunkte, Masterarbeit](#_15)

### Abschnitt 4  
Lehramt für Förderpädagogik

[§ 16         Fächer, Studienbereiche](#_16)  
[§ 17         Leistungspunkte, Masterarbeit](#_17)

Teil 3  
Schlussbestimmungen
----------------------------

[§ 18         Übergangsbestimmungen](#_18)  
[§ 19         Inkrafttreten, Außerkrafttreten](#_19)

[Anlage 1 Verteilung der Leistungspunkte auf die Studienbereiche im Studium für das Lehramt für die Primarstufe](#_anl1)  
[Anlage 2 Verteilung der Leistungspunkte auf die Studienbereiche im Studium für das Lehramt für die Primarstufe mit einer Schwerpunktbildung gemäß § 9](#_anl2)  
[Anlage 3 Verteilung der Leistungspunkte auf die Studienbereiche im Studium für das Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer)](#_anl3)  
[Anlage 4 Verteilung der Leistungspunkte auf die Studienbereiche im Studium für das Lehramt für die Sekundarstufe II (berufliche Fächer)](#_anl4)  
[Anlage 5 Verteilung der Leistungspunkte auf die Studienbereiche im Studium für das Lehramt für Förderpädagogik](#_anl5)

Teil 1  
Allgemeine Bestimmungen
--------------------------------

#### § 1  
Geltungsbereich

Diese Verordnung bestimmt die Anforderungen an die lehramtsbezogenen Studiengänge an den Hochschulen im Land Brandenburg und lehramtsbezogenen Masterabschlüsse, mit denen die Zugangsberechtigung für den Vorbereitungsdienst für Lehrämter an Schulen im Land Brandenburg erworben wird.

#### § 2  
Strukturelle Anforderungen

(1) Das Lehramtsstudium besteht aus einem sechssemestrigen lehramtsbezogenen Bachelorstudium (Bachelorstudium) mit dem Abschluss „Bachelor of Education“ und einem darauf aufbauenden viersemestrigen lehramts-bezogenen Masterstudium (Masterstudium) mit dem Abschluss „Master of Education“.
Für den Zugang zum Masterstudium sind die Voraussetzungen gemäß § 4 nachzuweisen.

(2) Im Lehramtsstudium sind Studien- und Prüfungsleistungen im Umfang von insgesamt 300 Leistungspunkten nach dem European Credit Transfer System (ECTS) nachzuweisen.
Davon entfallen auf das Bachelorstudium 180 Leistungspunkte und auf das Masterstudium 120 Leistungspunkte.

(3) Die lehramtsbezogenen Studiengänge gliedern sich in Module, die studienbegleitend jeweils durch eine Modulabschlussprüfung abzuschließen sind.
In begründeten Ausnahmefällen, insbesondere wenn die inhaltlichen Anforderungen oder der Umfang eines Moduls es erfordern, kann die Modulabschlussprüfung aus Teilprüfungen bestehen.
Soweit ein Modul überwiegend praktische Studien umfasst, kann anstelle der Benotung der zu erbringenden Prüfungsleistung die Feststellung erfolgen, ob sie bestanden oder nicht bestanden wurde.

(4) Die Prüfungsanforderungen und -formen sind an den im jeweiligen Modul zu erreichenden Kompetenzen auszurichten.
In den jeweiligen lehramtsbezogenen Studiengängen sollen die unterschiedlichen Prüfungsformen in einem ausgewogenen Verhältnis zueinander stehen.

(5) Im Übrigen gelten für die Durchführung der lehramtsbezogenen Studiengänge und für den Nachweis der zu erbringenden Studien- und Prüfungsleistungen die auf der Grundlage des Brandenburgischen Hochschulgesetzes erlassenen Rechtsverordnungen und Hochschulordnungen, soweit in dieser Verordnung nichts anderes bestimmt wird.

#### § 3  
Akkreditierung

(1)  Die lehramtsbezogenen Bachelor- und Masterstudiengänge sind im Wege der Programmakkreditierung zu akkreditieren und zu reakkreditieren.
Die Programmakkreditierung lehramtsbezogener Masterstudiengänge bedarf der Zustimmung des für Schule zuständigen Ministeriums.

(2)  Das für Schule zuständige Ministerium ist rechtzeitig in das Programmakkreditierungsverfahren lehramts-bezogener Bachelor- und Masterstudiengänge einzubeziehen und dazu insbesondere über

1.  die geplanten Programmakkreditierungsverfahren,
2.  die Beratungstermine der von der Akkreditierungsagentur bestellten Gutachtergruppe und
3.  die Termine für die Vor-Ort-Begehungen an der Hochschule

zu informieren.
Eine Vertreterin oder ein Vertreter des für Schule zuständigen Ministeriums kann an den internen Beratungen der Gutachtergruppe und an den Vor-Ort-Begehungen teilnehmen und ist dazu rechtzeitig einzuladen.
Ihr oder ihm sind die für das Programmakkreditierungsverfahren maßgeblichen Unterlagen in einer angemessenen Frist vor Beginn des Verfahrens zur Verfügung zu stellen.

(3) Das für Schule zuständige Ministerium kann unabhängig von der Gutachtergruppe für den jeweils zu akkreditierenden lehramtsbezogenen Bachelor- und Masterstudiengang insbesondere

1.  zu den Qualifikationszielen und dem lehramtsspezifischen Profilanspruch,
2.  zur Umsetzung der in dieser Verordnung bestimmten Anforderungen im Konzept des Studiengangs,
3.  zum implementierten Prüfungssystem,
4.  zur personellen Ausstattung sowie
5.  zur Transparenz der Anforderungen

Stellung nehmen.
Bei lehramtsbezogenen Masterstudiengängen beinhaltet die Stellungnahme die Erklärung, ob der Programmakkreditierung des jeweiligen Studienprogramms zugestimmt wird.
Kann die Zustimmung zur Programmakkreditierung nicht erteilt werden, ist dies zu begründen.
Die Akkreditierung ist auf sieben Jahre befristet.
Die Frist beginnt mit dem Tag des Wirksamwerdens des Akkreditierungsbescheids.
Die gemäß den Sätzen 4 und 5 bemessene Frist verlängert sich auf das Ende des zuletzt betroffenen Studienjahres.

(4) Abweichend von Absatz 1 kann für neue lehramtsbezogene Bachelor- und Masterstudiengänge, für die ein Konzept vorliegt, die aber noch nicht Bestandteil des Studienangebots der Hochschule sind, das Konzept akkreditiert werden (Konzeptakkreditierung).
Die Konzeptakkreditierung lehramtsbezogener Masterstudiengänge bedarf der Zustimmung des für Schule zuständigen Ministeriums.
Für das Verfahren gelten die Absätze 2 und 3 entsprechend.
Für konzeptakkreditierte lehramtsbezogene Bachelor- und Masterstudiengänge, die Teil des Studienangebots der Hochschule sind, ist nach Ablauf einer Frist von fünf Jahren eine Programmakkreditierung durchzuführen.
Der Fristlauf beginnt mit dem Tag des Wirksamwerdens des Akkreditierungsbescheides.
Die gemäß den Sätzen 4 und 5 bemessene Frist verlängert sich auf das Ende des zuletzt betroffenen Studienjahres.
Sofern der lehramtsbezogene Bachelor- oder Masterstudiengang nach dem Wirksamwerden des Akkreditierungsbescheids eröffnet wird, beginnt der Fristlauf mit dem Tag seiner Eröffnung.

(5) Für lehramtsbezogene Studiengänge an Hochschulen, deren internes Qualitätssicherungssystem im Bereich Studium und Lehre akkreditiert wurde (Systemakkreditierung), gilt, dass an die Stelle

1.  der Programmakkreditierung eine hochschulinterne Programmakkreditierung und
2.  der Konzeptakkreditierung eine hochschulinterne Konzeptüberprüfung

tritt.
Für die Beteiligung des für Schule zuständigen Ministeriums an den Verfahren und für die Fristen gelten die Absätze 1 bis 4 entsprechend.

#### § 4  
Zugang zum Masterstudium

Der Zugang zu einem Masterstudium setzt den Nachweis über den Abschluss „Bachelor of Education“ gemäß § 2 Absatz 1 Satz 1 oder einen gleichwertigen Abschluss voraus.
Spätestens zu Beginn des Masterstudiums sind von den Studierenden

1.  ein Nachweis über die Teilnahme an Maßnahmen der Hochschule zur Feststellung der individuellen Voraussetzungen für die Tätigkeit als Lehrkraft sowie
2.  ein phoniatrisches Gutachten

zu erbringen.

#### § 5  
Inhaltliche Anforderungen

(1) Im Lehramtsstudium sind Studien- und Prüfungsleistungen

1.  in zwei Fächern gemäß § 3 Absatz 3 Satz 1 des Brandenburgischen Lehrerbildungsgesetzes,
2.  in den Bildungswissenschaften und
3.  in einem lehramtsspezifischen Studienbereich gemäß Teil 2

nachzuweisen.
Weiterhin sind Studienleistungen in den schulpraktischen Studien zu erbringen sowie eine Bachelor- und eine Masterarbeit anzufertigen.
Der Mindestumfang der in den einzelnen Studienbereichen nachzuweisenden Studien- und Prüfungsleistungen wird in Teil 2 näher bestimmt.

(2) Die für das Lehramtsstudium zugelassenen Fächer und ihre Verbindungen sowie die lehramtsspezifischen Studienbereiche werden in Teil 2 näher bestimmt.
Das für Schule zuständige Ministerium kann bei der Zulassung von Fächern und ihren Verbindungen Abweichungen von diesen Bestimmungen zulassen.

(3) Das Studium der Fächer umfasst die fachwissenschaftlichen oder künstlerischen sowie die fachdidaktischen Studien in dem jeweiligen Fach.
Sie sollen auch Aspekte der inklusiven Bildung berücksichtigen.
Beim Lehramt für die Sekundarstufe II (berufliche Fächer) sind die fachdidaktischen Studien den Bildungswissenschaften zugeordnet.

(4) Das Studium in einer modernen Fremdsprache umfasst sprachpraktische Studien und soll Aufenthalte in Ländern der Zielsprache einschließen.
Das Studium in den Fächern Kunst, Musik und Sport umfasst fachpraktische Studien.
Das Nähere dazu bestimmen die Hochschulordnungen.

(5) Im Studium der Bildungswissenschaften sind Studien- und Prüfungsleistungen in den pädagogischen, psychologischen und sozialwissenschaftlichen Grundlagen sowie in den Grundlagen in Schulrecht und Schulverwaltung nachzuweisen.
Darüber hinaus sind Studien- und Prüfungsleistungen in den inklusionspädagogischen und -didaktischen Grundlagen nachzuweisen, wenn im Lehramtsstudium keine inklusionspädagogische Schwerpunktbildung erfolgt oder sich das Studium auf das Lehramt für Förderpädagogik bezieht.
Die bildungswissenschaftlichen und die fachdidaktischen Studien sollen inhaltlich aufeinander bezogen sein.

(6) Für die curriculare Ausgestaltung des Lehramtsstudiums sind die von der Ständigen Konferenz der Kultusminister der Länder in der Bundesrepublik Deutschland beschlossenen Standards für die Lehrerbildung sowie die ländergemeinsamen inhaltlichen Anforderungen für die Lehrerbildung in ihrer jeweils gültigen Fassung verbindlich.
Soweit Vorgaben gemäß Satz 1 für einzelne Studienbereiche oder Fächer nicht vorliegen, bedürfen die curricularen Vorgaben für das Lehramtsstudium der Genehmigung des für Schule zuständigen Ministeriums.

(7) Die Geschichte und Kultur der Sorben (Wenden) ist in geeigneten Studienbereichen in angemessenem Umfang zu berücksichtigen.

#### § 6  
Schulpraktische Studien

(1) Die schulpraktischen Studien sind integrativer Bestandteil des Lehramtsstudiums.
Sie sollen insbesondere

1.  die wissenschaftlichen Studien mit schulpraktischen Erfahrungen verknüpfen und dabei forschungsorientierte Fragestellungen berücksichtigen und
2.  die Grundlagen zur Entwicklung beruflicher Handlungsfähigkeit vermitteln und den Studierenden einen Einblick in die schulischen Handlungsfelder geben.

(2) Im Bachelorstudium sind schulpraktische Studien im Umfang von insgesamt mindestens sechs Wochen nachzuweisen.
Ein Praktikum soll in pädagogisch-psychologischen Handlungsfeldern, die sich nicht auf den obligatorischen oder wahlobligatorischen Unterricht an Schulen beziehen, durchgeführt werden.
Im Rahmen der Betreuung der schulpraktischen Studien sollen Beratungen zum Entwicklungsstand und zur weiteren Entwicklung der individuellen Voraussetzungen für die Tätigkeit als Lehrkraft durchgeführt werden.

(3) Im Masterstudium ist ein Schulpraktikum im Umfang von mindestens 16 Wochen nachzuweisen, das in einer dem angestrebten Lehramt und der gegebenenfalls erfolgten Schwerpunktbildung entsprechenden Schulstufe sowie in den studierten Fächern zu absolvieren ist.
Dabei sollen die Studierenden beginnen, ein eigenes professionelles Selbstkonzept zu entwickeln, indem sie insbesondere

1.  zunächst angeleitet, später zunehmend selbstständig Unterricht planen, durchführen und reflektieren,
2.  Konzepte der Leistungsbeurteilung, der pädagogischen und psychologischen Diagnostik sowie der individuellen Förderung anwenden und reflektieren,
3.  forschungsorientierte Fragestellungen im Handlungsfeld Schule entwickeln und theoriegeleitet reflektieren und
4.  sich an der Umsetzung des Bildungs- und Erziehungsauftrags der Schule beteiligen.

Das Schulpraktikum ist durch geeignete Lehrveranstaltungen der Hochschule vor- und nachzubereiten sowie zu begleiten.

#### § 7  
Masterzeugnis

(1) Das Zeugnis über den Masterabschluss weist neben der Bezeichnung des Abschlusses „Master of Education“ auch den Bezug auf das Lehramt gemäß dem Brandenburgischen Lehrerbildungsgesetz und die gemäß Teil 2 erfolgte Schwerpunktbildung aus.

(2) Auf dem Zeugnis sind insbesondere

1.  die Themenstellung der Masterarbeit, die Note für die Masterarbeit sowie die ihr zugeordneten Leistungspunkte,
2.  die jeweiligen Abschlussnoten für die beiden Fächer, die Bildungswissenschaften und den lehramtsspezifischen Studienbereich mit den ihnen jeweils zugeordneten Leistungspunkten,
3.  die dem Schulpraktikum gemäß § 6 Absatz 3 zugeordneten Leistungspunkte,
4.  die in anderen Studienangeboten der Hochschule erbrachten Studienleistungen und den ihnen jeweils zugeordneten Leistungspunkten,
5.  die Gesamtnote des Masterabschlusses und
6.  Aussagen zur Akkreditierung des jeweiligen Masterstudiengangs gemäß § 3

auszuweisen.

Teil 2  
Lehramtsbezogene Bestimmungen
--------------------------------------

### Abschnitt 1  
Lehramt für die Primarstufe

#### § 8  
Fächer, Studienbereiche

(1) Die zum Studium zugelassenen Fächer gemäß § 5 Absatz 1 Satz 1 Nummer 1 sind Deutsch, Englisch, Kunst, Mathematik, Musik, Sachunterricht, Sorbisch/Wendisch und Sport, wobei mindestens eines der Fächer Deutsch, Englisch oder Mathematik zu studieren ist.

(2) Sofern das Fach Sachunterricht studiert wird, sind im Rahmen dieses Fachstudiums die fachwissenschaftlichen und -didaktischen Grundlagen in einem Bezugsfach zum Sachunterricht zu studieren, die einen Einsatz im jeweiligen Fachunterricht der Jahrgangsstufen 5 und 6 der Primarstufe ermöglichen.
Als Bezugsfach zum Sachunterricht sind

1.  Naturwissenschaften und Wirtschaft-Arbeit-Technik, sofern Mathematik als weiteres Fach studiert wird, und
2.  Gesellschaftswissenschaften und Lebensgestaltung-Ethik-Religionskunde, sofern Deutsch oder Englisch als weiteres Fach studiert wird,

zugelassen.

(3) Der lehramtsspezifische Studienbereich gemäß § 5 Absatz 1 Satz 1 Nummer 3 ist die Grundschulbildung.
In ihr sind Studien- und Prüfungsleistungen in den Teilbereichen

1.  Grundschulpädagogik und -didaktik,
2.  fachwissenschaftliche und -didaktische Grundlagen für den Unterricht in den Fächern Deutsch, Mathematik sowie Englisch (einschließlich sprachliche Kompetenzentwicklung) in der Schuleingangsphase und
3.  fachliche und fachdidaktische Grundlagen für den Sachunterricht sowie für den Unterricht in den Fächern Kunst, Musik und Sport

nachzuweisen.
Sofern Kunst, Musik, Sachunterricht oder Sport als Fach gemäß Absatz 1 studiert wird, sind keine Studien- und Prüfungsleistungen für die fachlichen und didaktischen Grundlagen für das entsprechende Unterrichtsfach gemäß Satz 2 Nummer 3 nachzuweisen.
Das Nähere dazu wird durch die jeweilige Hochschulordnung bestimmt.

#### § 9  
Inklusionspädagogische Schwerpunktbildung

(1) Erfolgt im Studium eine inklusionspädagogische Schwerpunktbildung, tritt an die Stelle der Grundschulbildung die Inklusionspädagogik, in der Studien- und Prüfungsleistungen in den Teilbereichen

1.  allgemeine Inklusionspädagogik und -didaktik und
2.  inklusionspädagogisch orientierte Studien der Fachrichtungen für die Förderschwerpunkte Lernen, Sprache sowie emotionale und soziale Entwicklung

nachzuweisen sind.

(2) Die Studien- und Prüfungsleistungen in den Grundlagen der Grundschulpädagogik und -didaktik sind im Studienbereich Bildungswissenschaften nachzuweisen.

(3) Abweichend von § 8 Absatz 1 sind als Fächer nur Deutsch und Mathematik zugelassen.

#### § 10  
Leistungspunkte, Masterarbeit

(1) In den vorgesehenen Studienbereichen sind mindestens die in den Anlagen 1 und 2 jeweils ausgewiesenen Leistungspunkte nachzuweisen.
Die variablen Leistungspunkte gemäß Anlage 1 Nummer 6 und Anlage 2 Nummer 6 sind durch die Hochschule den vorgesehenen Studienbereichen oder anderen Studienangeboten zuzuordnen.
Dabei hat die Hochschule zu gewährleisten, dass der Umfang der Studien- und Prüfungsleistungen in den fachdidaktischen, bildungswissenschaftlichen und schulpraktischen Studien sowie in der Grundschulbildung zusammen mindestens ein Drittel des Gesamtstudienumfangs beträgt.
Erfolgt keine inklusionspädagogische Schwerpunktbildung, beträgt der Umfang der Studien- und Prüfungsleistungen für die inklusionspädagogischen und -didaktischen Grundlagen mindestens ein Zehntel der für die Bildungswissenschaften vorgesehenen Leistungspunkte.
Soweit variable Leistungspunkte den Fächern zugeordnet werden, sind diese in gleichem Maße zu berücksichtigen.

(2) Für das Studium des Bezugsfachs zum Sachunterricht sind mindestens ein Drittel und höchstens zwei Drittel des für das Fach Sachunterricht insgesamt vorgesehenen Gesamtstudienumfangs nachzuweisen.

(3) Die Masterarbeit ist in der Grundschulpädagogik und -didaktik oder in der Fachdidaktik oder in der Fachwissenschaft eines der studierten Fächer anzufertigen.
Erfolgt im Studium eine inklusionspädagogische Schwerpunktbildung gemäß § 9, ist die Masterarbeit in der Inklusionspädagogik oder in der Fachdidaktik oder der Fachwissenschaft des Fachs Deutsch oder Mathematik anzufertigen.
Wird die Masterarbeit in einer Fachdidaktik angefertigt, so kann das Thema mit fachwissenschaftlichen Bezügen gestellt werden.
Wird die Masterarbeit in einer Fachwissenschaft angefertigt, so ist das Thema mit Bezügen zu mindestens einem der anderen Bereiche gemäß den Sätzen 1 und 2 zu stellen.

### Abschnitt 2  
Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer)

#### § 11  
Fächer, Studienbereiche

Die zum Studium zugelassenen Fächer gemäß § 5 Absatz 1 Satz 1 Nummer 1 sind Biologie, Chemie, Deutsch, Englisch, Französisch, Geografie, Geschichte, Informatik, Kunst, Mathematik, Musik, Physik, Politische Bildung, Polnisch, Russisch, Sorbisch/Wendisch, Spanisch und Sport sowie

1.  Lebensgestaltung-Ethik-Religionskunde und Wirtschaft-Arbeit-Technik bei der Schwerpunktbildung auf die Sekundarstufe I und
2.  Latein und Technik bei der Schwerpunktbildung auf die Sekundarstufe II (allgemeinbildende Fächer).

Die Verbindung der Fächer Geschichte, Lebensgestaltung-Ethik-Religionskunde und Politische Bildung und der Fächer Polnisch, Russisch und Sorbisch/Wendisch ist nicht zulässig.

#### § 12  
Schwerpunktbildung

Spätestens zu Beginn des Masterstudiums erfolgt eine Schwerpunktbildung auf die Sekundarstufe I oder auf die Sekundarstufe II (allgemeinbildende Fächer).
Mit der Schwerpunktbildung soll den unterschiedlichen Anforderungen in den Schulstufen Rechnung getragen werden, wobei insbesondere der jeweilige Bildungsauftrag und die jeweiligen fachwissenschaftlichen und fachdidaktischen Anforderungen durch die Festlegung der Inhalte und der Studienumfänge in den zu studierenden Fächern und in den Bildungswissenschaften, hier insbesondere in den inklusionspädagogischen und -didaktischen Grundlagen, zu berücksichtigen sind.
Bei einer Schwerpunktbildung auf die Sekundarstufe II (allgemeinbildende Fächer) sind in den Fächern jeweils vertiefte fachwissenschaftliche Studien nachzuweisen.

#### § 13  
Leistungspunkte, Masterarbeit

(1) In den vorgesehenen Studienbereichen sind mindestens die jeweils in Anlage 3 ausgewiesenen Leistungspunkte nachzuweisen.
Die variablen Leistungspunkte gemäß Anlage 3 Nummer 5 sind durch die Hochschule den vorgesehenen Studienbereichen oder anderen Studienangeboten zuzuordnen.
Dabei hat die Hochschule zu gewährleisten, dass der Umfang der Studien- und Prüfungsleistungen

1.  in den fachdidaktischen, bildungswissenschaftlichen und schulpraktischen Studien zusammen mindestens ein Drittel des Gesamtstudienumfangs und
2.  für die inklusionspädagogischen und -didaktischen Grundlagen mindestens ein Zehntel der für die Bildungswissenschaften vorgesehenen Studien- und Prüfungsleistungen

beträgt.
Soweit variable Leistungspunkte den Fächern zugeordnet werden, sind diese in gleichem Maße zu berücksichtigen.

(2) Bei einer Schwerpunktbildung auf die Sekundarstufe II (allgemeinbildende Fächer) sind für vertiefende Studien in jedem Fach jeweils mindestens 20 Prozent der zur Verfügung stehenden variablen Leistungspunkte gemäß Anlage 3 Nummer 5 zu verwenden.

(3) Die Masterarbeit ist in den Bildungswissenschaften oder in der Fachdidaktik oder Fachwissenschaft eines der studierten Fächer anzufertigen.
Wird die Masterarbeit in einer Fachdidaktik angefertigt, so kann das Thema mit fachwissenschaftlichen Bezügen gestellt werden.
Wird die Masterarbeit in einer Fachwissenschaft angefertigt, so ist das Thema mit Bezügen zu mindestens einem der anderen Bereiche gemäß Satz 1 zu stellen.

### Abschnitt 3  
Lehramt für die Sekundarstufe II (berufliche Fächer)

#### § 14  
Fächer, Studienbereiche

(1) Die zum Studium zugelassenen Fächer gemäß § 5 Absatz 1 Satz 1 Nummer 1 sind

1.  Agrarwirtschaft, Bautechnik, Biotechnik, Druck- und Medientechnik, Elektrotechnik, Ernährung und Hauswirtschaft, Fahrzeugtechnik, Farbtechnik/Raumgestaltung/Oberflächentechnik, Gesundheit und Körperpflege, Holztechnik, Informations- und Kommunikationstechnik, Labortechnik/Prozesstechnik, Mediendesign und Designtechnik, Metalltechnik, Pflege, Sozialpädagogik, Textil- und Bekleidungstechnik sowie Wirtschaft und Verwaltung als berufliche und
2.  Biologie, Chemie, Deutsch, Englisch, Französisch, Informatik, Kunst, Mathematik, Musik, Physik, Pädagogik, Politische Bildung, Polnisch, Psychologie, Russisch, Sorbisch/Wendisch, Spanisch, Sport und Wirtschaftswissenschaften als allgemeinbildende

Fächer, wobei mindestens ein berufliches Fach zu studieren ist.
Die Verbindung der Fächer Informations- und Kommunikationstechnik und Informatik sowie Wirtschaft und Verwaltung und Wirtschaftswissenschaften ist nicht zulässig.

(2) An die Stelle eines allgemeinbildenden Faches oder einer weiteren beruflichen Fachrichtung kann der Studienbereich Förderpädagogik gemäß § 16 Absatz 2 treten.

(3) Die bildungswissenschaftlichen Studien umfassen schwerpunktmäßig berufs- oder wirtschaftspädagogische Inhalte.

(4) Soweit keine, auf das studierte berufliche Fach bezogene abgeschlossene Berufsausbildung nach Bundes- oder Landesrecht oder eine entsprechende mindestens dreijährige Berufspraxis nachgewiesen wird, ist spätestens vor Abschluss des Masterstudiums zusätzlich eine entsprechende fachpraktische Tätigkeit von zwölf Monaten Dauer nachzuweisen.

#### § 15  
Leistungspunkte, Masterarbeit

(1) In den vorgesehenen Studienbereichen sind mindestens die jeweils in Anlage 4 ausgewiesenen Leistungspunkte nachzuweisen.
Die variablen Leistungspunkte gemäß Anlage 4 Nummer 5 sind durch die Hochschule den vorgesehenen Studienbereichen oder anderen Studienangeboten zuzuordnen.
Dabei hat die Hochschule zu gewährleisten, dass der Umfang der Studien- und Prüfungsleistungen

1.  in den fachdidaktischen, bildungswissenschaftlichen und schulpraktischen Studien zusammen mindestens ein Drittel des Gesamtstudienumfangs und
2.  für die inklusionspädagogischen und -didaktischen Grundlagen mindestens ein Zehntel des für die Bildungswissenschaften vorgesehenen Studienumfangs

beträgt.
Soweit variable Leistungspunkte gemäß Anlage 4 Nummer 5 den Fächern zugeordnet werden, sind diese in gleichem Maße zu berücksichtigen.

(2) Im Fall von § 14 Absatz 2 beträgt der Studienumfang für den Studienbereich Förderpädagogik abweichend von Anlage 5 Nummer 3 mindestens 90 Leistungspunkte, wobei das Verhältnis der ausgewiesenen Studienumfänge seiner Teilbereiche zu wahren ist und die gemäß Absatz 1 Satz 2 Nummer 2 nachzuweisenden Studien- und Prüfungsleistungen nicht erbracht werden müssen.

(3) Die Masterarbeit ist in den Bildungswissenschaften oder in der Fachdidaktik oder Fachwissenschaft eines der studierten Fächer anzufertigen.
Wird die Masterarbeit in einer Fachdidaktik angefertigt, so kann das Thema mit fachwissenschaftlichen Bezügen gestellt werden.
Wird die Masterarbeit in den Fachwissenschaften angefertigt, so ist das Thema mit Bezügen zu mindestens einem der anderen Bereiche gemäß Satz 1 zu stellen.
Im Fall von § 14 Absatz 2 kann die Masterarbeit auch in der Förderpädagogik angefertigt werden.

### Abschnitt 4  
Lehramt für Förderpädagogik

#### § 16  
Fächer, Studienbereiche

(1) Die zum Studium zugelassenen Fächer gemäß § 5 Absatz 1 Satz 1 Nummer 1 sind Biologie, Chemie, Deutsch, Englisch, Französisch, Geografie, Geschichte, Informatik, Kunst, Latein, Lebensgestaltung-Ethik-Religionskunde, Mathematik, Musik, Physik, Politische Bildung, Polnisch, Russisch, Sorbisch/Wendisch, Spanisch, Sport, Technik und Wirtschaft-Arbeit-Technik, von denen gemäß § 3 Absatz 3 Satz 5 Nummer 4 des Brandenburgischen Lehrerbildungsgesetzes ein Fach zu studieren ist.

(2) Der lehramtsspezifische Studienbereich gemäß § 5 Absatz 1 Satz 1 Nummer 3 ist die Förderpädagogik.
In ihr sind Studien- und Prüfungsleistungen

1.  in der allgemeinen Förder- und Inklusionspädagogik und
2.  in zwei Fachrichtungen, die jeweils einem der sonderpädagogischen Förderschwerpunkte Sehen, Hören, geistige Entwicklung, körperliche und motorische Entwicklung, Lernen, Sprache oder emotionale und soziale Entwicklung zugeordnet sind,

nachzuweisen.

#### § 17  
Leistungspunkte, Masterarbeit

(1) In den vorgesehenen Studienbereichen sind mindestens die jeweils in Anlage 5 ausgewiesenen Leistungspunkte nachzuweisen.
Die variablen Leistungspunkte gemäß Anlage 5 Nummer 6 sind durch die Hochschule den vorgesehenen Studienbereichen oder anderen Studienangeboten zuzuordnen.
Dabei hat die Hochschule zu gewährleisten, dass der Umfang der Studien- und Prüfungsleistungen in den fachdidaktischen, bildungswissenschaftlichen und den schulpraktischen Studien zusammen mindestens ein Drittel des Gesamtstudienumfangs beträgt.

(2) Die Masterarbeit ist in der Förderpädagogik oder in den Bildungswissenschaften oder in der Fachdidaktik oder Fachwissenschaft des studierten Faches anzufertigen.
Wird die Masterarbeit in der Fachdidaktik des Faches angefertigt, so muss das Thema förderpädagogische Aspekte berücksichtigen.
Wird die Masterarbeit in einer Fachwissenschaft angefertigt, so ist das Thema mit Bezügen zu mindestens einem der anderen Bereiche gemäß Satz 1 zu stellen.

Teil 3  
Schlussbestimmungen
----------------------------

#### § 18  
Übergangsbestimmungen

(1) Studierende, die sich bei Inkrafttreten des Brandenburgischen Lehrerbildungsgesetzes vom 18. Dezember 2012 (GVBl.
I Nr.
45) in einem Lehramtsstudium gemäß § 5a des Brandenburgischen Lehrerbildungsgesetzes vom 25.
Juni 1999 (GVBl.
I S.
242), das zuletzt durch Artikel 17 des Gesetzes vom 3.
April 2009 (GVBl.
I S.
26, 59) geändert worden ist, befinden, absolvieren das Studium auf der Grundlage der Bachelor-Master-Abschlussverordnung vom 21.
September 2005 (GVBl.
II S.
502), die durch Artikel 4 des Gesetzes vom 11.
Mai 2007 (GVBl.
I S.
86, 92) geändert worden ist, in Verbindung mit der Lehramtsprüfungsordnung vom 31.
Juli 2001 (GVBl.
II S.
494), die zuletzt durch Artikel 5 des Gesetzes vom 11.
Mai 2007 (GVBl.
I S.
86, 92) geändert worden ist.
Das Masterstudium muss bis zum 30.
September 2022 abgeschlossen sein.

(2) Für Studierende, die vor dem 1.
Oktober 2018 ein auf das Lehramt für die Primarstufe bezogenes Bachelor- und Masterstudium mit dem Fach Sachunterricht aufgenommen haben, gilt § 8 Absatz 2 Satz 2 in der bis dahin geltenden Fassung fort.
Das Studium ist jeweils auf der Grundlage der von der Hochschule dazu erlassenen Studien- und Prüfungsordnung abzuschließen.

(3) Abweichend von Absatz 2 Satz 1 kann die Hochschule durch entsprechende Regelungen in der Studien- und Prüfungsordnung für das Fach Sachunterricht vorsehen, dass an die Stelle

1.  des Bezugsfachs Geografie, Geschichte oder Politische Bildung das Bezugsfach Gesellschaftswissenschaften und
2.  des Bezugsfachs Biologie oder Physik das Bezugsfach Naturwissenschaften

treten kann.
Satz 1 gilt mit der Maßgabe, dass der Bezugsfachwechsel den ordnungsgemäßen Abschluss des Bachelor- und Masterstudiums im Fach Sachunterricht mit dem neuen Bezugsfach innerhalb der Regelstudienzeit nicht gefährdet und im Bachelor- und Masterstudium des neuen Bezugsfachs in der Regel mindestens zwei Drittel des dafür vorgesehenen Gesamtstudienumfangs erbracht werden.
Die Möglichkeit der Anrechnung von bisher erbrachten Studien- und Prüfungsleistungen durch die Hochschule bleibt unberührt.

#### § 19  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Juni 2013 in Kraft.
Gleichzeitig treten

1.  die Lehramtsprüfungsordnung vom 31.
    Juli 2001 (GVBl.
    II S.
    494), die zuletzt durch Artikel 5 des Gesetzes vom 11.
    Mai 2007 (GVBl.
    I S.
    86, 92) geändert worden ist, und
2.  die Bachelor-Master-Abschlussverordnung vom 21.
    September 2005 (GVBl.
    II S.
    502), die durch Artikel 4 des Gesetzes vom 11.
    Mai 2007 (GVBl.
    I S.
    86, 92) geändert worden ist,

außer Kraft.

Potsdam, den 6.
Juni 2013

Die Ministerin für Bildung,  
Jugend und Sport

Dr.
Martina Münch 

**Anlage 1** (zu § 10 Absatz 1)

Verteilung der Leistungspunkte auf die Studienbereiche im Studium für das Lehramt für die Primarstufe
-----------------------------------------------------------------------------------------------------

**Studienbereiche und Leistungspunkte (LP)**

**1.
Fächer gemäß § 8 Absatz 1**

**mindestens 55 LP[\*)](#_ftn8)**

     _einschließlich der jeweiligen Fachdidaktik_

_davon mindestens 11 LP__[\*)](#_ftn8)_

**2.
Bildungswissenschaften**

**mindestens 30 LP**

     _einschließlich_  
     _inklusionspädagogische und -didaktische Grundlagen_

  
_davon mindestens 15 LP_

**3.
Grundschulbildung**

**mindestens 90 LP**

     _mit den Teilbereichen:_  
      -  _Grundschulpädagogik und -didaktik_

  
_davon mindestens 35 LP_

      -  _fachwissenschaftliche und fachdidaktische Grundlagen für  
         den Unterricht in den Fächern Deutsch, Englisch und  
         Mathematik in der Schuleingangsphase_

  
  
_davon mindestens 35 LP_

      -  _fachliche und fachdidaktische Grundlagen für den Sachunterricht  
         und den Unterricht in den Fächern Kunst, Musik und Sport_

  
_davon mindestens 20 LP_

**4.
Schulpraktikum im Masterstudium**

**mindestens 20 LP**

**5.
Bachelor- und Masterarbeit insgesamt**

**mindestens 21 LP**

**6.
variable Leistungspunkte**

**höchstens 29 LP**

**Insgesamt**

**300 LP**

**Anlage 2  
**(zu § 10 Absatz 1)

Verteilung der Leistungspunkte auf die Studienbereiche im Studium für das Lehramt für die Primarstufe mit einer Schwerpunktbildung gemäß § 9
--------------------------------------------------------------------------------------------------------------------------------------------

**Studienbereiche und Leistungspunkte (LP)**

**1.
Deutsch und Mathematik**

**mindestens 55 LP****[\*)](#_ftn8)**

     _einschließlich der jeweiligen Fachdidaktik_

_davon mindestens 11 LP__[\*)](#_ftn8)_

**2.
Bildungswissenschaften** 

**mindestens 30 LP**

     _einschließlich_  
     _Grundlagen der Grundschulpädagogik und -didaktik_

  
_davon mindestens 15 LP_

**3.
Inklusionspädagogik**

**mindestens 90 LP**

     _mit den Teilbereichen:_  
      -  _allgemeine Inklusionspädagogik und -didaktik_

  
_davon mindestens 15 LP_

      -  _inklusionspädagogisch orientierte Studien in den Fachrichtungen  
         für die Förderschwerpunkte Lernen, Sprache, emotionale und  
         soziale Entwicklung_

  
  
_davon mindestens 75  LP_

**4.
Schulpraktikum im Masterstudium**

**mindestens 20 LP**

**5.
Bachelor- und Masterarbeit insgesamt**

**mindestens 21 LP**

**6.
variable Leistungspunkte**

**höchstens 29  LP**

**Insgesamt** 

**300 LP**

**Anlage 3  
**(zu § 13 Absatz 1)

Verteilung der Leistungspunkte auf die Studienbereiche im Studium für das Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer)
--------------------------------------------------------------------------------------------------------------------------------------------

**Studienbereiche und Leistungspunkte (LP)**

**1.
Fächer gemäß § 11 Absatz 1**

**mindestens 90 LP****[\*)](#_ftn8)**

     _einschließlich der jeweiligen Fachdidaktik_

_davon mindestens 18 LP__[\*)](#_ftn6)_

**2.
Bildungswissenschaften**

**mindestens 40 LP**

     _einschließlich_  
     _inklusionspädagogische und -didaktische Grundlagen_

  
_davon mindestens 6 LP_

**3.
Schulpraktikum im Masterstudium**

**mindestens 20 LP**

**4.
Bachelor- und Masterarbeit insgesamt**

**mindestens 21 LP**

**5.
variable Leistungspunkte**

**höchstens 39 LP**

**Insgesamt**

**300 LP**

**Anlage 4  
**(zu § 15 Absatz 1)

Verteilung der Leistungspunkte auf die Studienbereiche im Studium für das Lehramt für die Sekundarstufe II (berufliche Fächer)
------------------------------------------------------------------------------------------------------------------------------

**Studienbereiche und Leistungspunkte (LP)**

**1.
Fächer gemäß § 14 Absatz 1**

**mindestens 90 LP****[\*)](#_ftn8)**

     _einschließlich der jeweiligen Fachdidaktik_

_davon mindestens 18 LP__[\*)](#_ftn8)_

**2.
Bildungswissenschaften**

**mindestens 60 LP**

     _einschließlich inklusionspädagogische und -didaktische Grundlagen_

_davon mindestens 6 LP_

**3.
Schulpraktikum im Masterstudium**

**mindestens 20 LP**

**4.
Bachelor- und Masterarbeit insgesamt**

**mindestens 21 LP**

**5.
variable Leistungspunkte**

**höchstens 19 LP**

**Insgesamt** 

**300 LP**

**Anlage 5  
**(zu § 17 Absatz 1)

Verteilung der Leistungspunkte auf die Studienbereiche im Studium für das Lehramt für Förderpädagogik
-----------------------------------------------------------------------------------------------------

**Studienbereiche und Leistungspunkte (LP)**

**1.
Fach gemäß § 16 Absatz 1**

**mindestens 90 LP**

     _einschließlich der Fachdidaktik_

_davon mindestens 18 LP_

**2.
Bildungswissenschaften** 

**mindestens 30 LP**

**3.
Förderpädagogik**

**mindestens 120 LP**

     _mit den Teilbereichen:_  
      -  _allgemeine Förder- und Inklusionspädagogik_

  
_davon mindestens 40 LP_

      -  _zwei Fachrichtungen gemäß § 16 Absatz 2 Satz 2 Nummer 2_

_davon mindestens 80 LP_

**4.
Schulpraktikum im Masterstudium**

**mindestens 20 LP**

**5.
Bachelor- und Masterarbeit insgesamt**

**mindestens 21 LP**

**6.
variable Leistungspunkte**

**höchstens 19 LP**

**Insgesamt** 

**300 LP**

* * *

[\*](#_ftnref1))   Die Angabe bezieht sich jeweils auf ein Fach bzw.
eine Fachdidaktik.