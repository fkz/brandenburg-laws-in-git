## Verordnung zu den Transplantationsbeauftragten in Entnahmekrankenhäusern des Landes Brandenburg (Brandenburgische Transplantationsbeauftragtenverordnung - BbgTPBV)

Auf Grund des § 9b Absatz 4 des Transplantationsgesetzes in der Fassung der Bekanntmachung vom 4. September 2007 (BGBl.
I S. 2206), der durch das Gesetz vom 22. März 2019 (BGBl. I S. 352) geändert worden ist, in Verbindung mit § 24a Absatz 2 des Brandenburgischen Krankenhausentwicklungsgesetzes vom 8. Juli 2009 (GVBl. I S. 310), der durch das Gesetz vom 18. Dezember 2012 (GVBl. I Nr. 44) eingefügt worden ist, verordnet die Ministerin für Soziales, Gesundheit, Integration und Verbraucherschutz:

#### § 1 Grundsätze

(1) Diese Verordnung regelt das Nähere zu Aufgaben, Bestellung und Qualifikation der oder des Transplantationsbeauftragten nach § 9b Absatz 1 und 2 des Transplantationsgesetzes.

(2) Die oder der Transplantationsbeauftragte ist Ansprechperson für das ärztliche und pflegerische Personal in allen Belangen der Organ- und Gewebespende.
Die Aufgaben der oder des Transplantationsbeauftragten ergeben sich insbesondere aus § 9b Absatz 2 des Transplantationsgesetzes.

#### § 2 Bestellung

(1) Jedes Krankenhaus im Sinne des § 9a Absatz 1 des Transplantationsgesetzes (Entnahmekrankenhaus) ist verpflichtet, mindestens eine ärztliche Transplantationsbeauftragte oder einen ärztlichen Transplantationsbeauftragten zu bestellen.
Soweit ein Entnahmekrankenhaus mehr als eine Intensivstation hat, soll für jede Intensivstation eine Transplantationsbeauftragte oder ein Transplantationsbeauftragter bestellt werden.
Dabei kann die oder der Transplantationsbeauftragte auch für mehrere Intensivstationen bestellt werden, wenn sichergestellt ist, dass die insoweit wahrzunehmenden Aufgaben vollumfänglich erfüllt werden.

(2) Hat ein Entnahmekrankenhaus mehrere Transplantationsbeauftragte bestellt, ist es zur Bestimmung einer oder eines koordinierenden Transplantationsbeauftragten verpflichtet.
Die oder der koordinierende Transplantationsbeauftragte ist die erste Ansprechperson für die ärztliche Leitung des Entnahmekrankenhauses, die Koordinierungsstelle der Region Nord-Ost und die zuständigen Behörden.
Die Funktion der oder des koordinierenden Transplantationsbeauftragten kann nur einer oder einem ärztlichen Transplantationsbeauftragten übertragen werden.

(3) Mehrere Entnahmekrankenhäuser können sich zu einem Verbund zusammenschließen und gemeinsam mindestens eine Transplantationsbeauftragte oder einen Transplantationsbeauftragten bestellen.
Das Nähere regelt § 7.

(4) In besonderen Einzelfällen kann die befristete Bestellung einer oder eines Transplantationsbeauftragten, die oder der nicht dem eigenen Entnahmekrankenhaus angehört (externe Person), durch das für Gesundheit zuständige Ministerium genehmigt werden.
Der Antrag auf Genehmigung der Bestellung einer externen Person ist zu begründen.

(5) Die ärztliche Krankenhausleitung kann die Bestellung einer oder eines Transplantationsbeauftragten widerrufen.
Der Widerruf ist zu begründen.

(6) Das Entnahmekrankenhaus ist verpflichtet, Name, Kontaktdaten und die Qualifikation nach § 3 Absatz 1 Satz 2 oder Satz 3 der oder des bestellten Transplantationsbeauftragten, den Namen der oder des koordinierenden Transplantationsbeauftragten nach Absatz 2 Satz 1 sowie jede Änderung unverzüglich dem für Gesundheit zuständigen Ministerium mitzuteilen.

#### § 3 Qualifikation, fachspezifische Fort- und Weiterbildung

(1) Zur oder zum Transplantationsbeauftragten kann nur bestellt werden, wer für die Erfüllung dieser Aufgabe fachlich qualifiziert ist.
Als fachlich qualifiziert gilt grundsätzlich jede approbierte Ärztin und jeder approbierte Arzt.
Pflegedienstkräfte gelten grundsätzlich als fachlich qualifiziert, wenn diese eine Leitungsfunktion in der Intensivpflege oder eine mindestens dreijährige Erfahrung in der Pflege von Intensivpatientinnen und Intensivpatienten vorweisen können.
Pflegedienstkräfte können neben der nach § 2 Absatz 1 Satz 1 bestellten Person oder als deren Vertretung bestellt werden.
Die Feststellung der fachlichen Qualifikation obliegt der ärztlichen Leitung des Entnahmekrankenhauses.

(2) Der erfolgreiche Abschluss einer fachspezifischen Fort- oder Weiterbildung ist Voraussetzung für die Bestellung als Transplantationsbeauftragte oder Transplantationsbeauftragter.
Für bereits bestellte Transplantationsbeauftragte gilt eine Übergangsfrist bis zum 30. Juni 2023.
Dem für Gesundheit zuständigen Ministerium ist ein Nachweis über den Abschluss nach Satz 1 auf Verlangen vorzulegen.

(3) Die oder der Transplantationsbeauftragte hat nach dem Abschluss einer fachspezifischen Fort- oder Weiterbildung gemäß Absatz 2 Satz 1 mindestens alle vier Jahre an einer weiteren fachspezifischen Fort- oder Weiterbildung teilzunehmen.
Absatz 2 Satz 3 gilt entsprechend.

(4) Die Inhalte der fachspezifischen Fort- und Weiterbildung sollen sich an die curricularen Vorgaben der Bundesärztekammer für Transplantationsbeauftragte anlehnen.
Die fachspezifische Fort- und Weiterbildung nach Absatz 2 Satz 1 soll Kenntnisse über die rechtlichen und strukturellen Grundlagen, den Organspendeprozess, die Grundlagen der Transplantationsmedizin und die Feststellung des irreversiblen Hirnfunktionsausfalls sowie die Angehörigenbegleitung vermitteln.

(5) Das Entnahmekrankenhaus trägt die Kosten für die Teilnahme an den fachspezifischen Fort- und Weiterbildungen einschließlich der Reise- und Übernachtungskosten.

#### § 4 Informations- und Auskunftspflichten

(1) Jede und jeder Transplantationsbeauftragte ist berechtigt und verpflichtet, die ärztliche Leitung des Entnahmekrankenhauses, für das sie oder er bestellt ist, unverzüglich über krankenhausinterne Erschwernisse zu informieren, die zur Beeinträchtigung der Organ- und Gewebespende führen können.

(2) Auf Verlangen hat die oder der Transplantationsbeauftragte eines Entnahmekrankenhauses dem für Gesundheit zuständigen Ministerium Auskunft über die Erfüllung der Verpflichtungen nach § 9b des Transplantationsgesetzes und nach dieser Verordnung zu erteilen.
Ist eine koordinierende Transplantationsbeauftragte oder ein koordinierender Transplantationsbeauftragter bestimmt worden, so ist das Auskunftsverlangen an diese Person zu richten.

(3) Die Koordinierungsstelle der Region Nord-Ost erstellt kalenderjährlich einen Bericht über die Entwicklung der Organspende im Land Brandenburg und die Tätigkeit der Transplantationsbeauftragten und übergibt diesen Bericht dem für Gesundheit zuständigen Ministerium.

#### § 5 Krankenhausinterne Informationsveranstaltungen

Erstmals bis zum 30. Juni 2023 und danach mindestens alle vier Jahre sind im Rahmen von krankenhausinternen Informationsveranstaltungen Ärztinnen und Ärzte sowie Pflegekräfte über die Bedeutung und den Prozess der Organ- und Gewebespende von der oder dem Transplantationsbeauftragten aufzuklären und über die festgelegten Verfahrensanweisungen zu informieren.
Ihnen ist die Teilnahme durch die Krankenhausleitung während der Dienstzeit zu ermöglichen.

#### § 6 Begleitung Angehöriger

Die Begleitung Angehöriger von Personen, die als Spenderin oder Spender von Organen oder Gewebe geeignet sind, ist in Erfüllung der in § 9b Absatz 2 Nummer 2 des Transplantationsgesetzes festgeschriebenen Aufgaben von der oder dem Transplantationsbeauftragten wahrzunehmen.
Hierbei sind die Angehörigen insbesondere in angemessener Weise zu begleiten und über eine Organ- und Gewebespende aufzuklären und zu informieren.
Eine Koordinatorin oder ein Koordinator der Koordinierungsstelle der Region Nord-Ost kann hinzugezogen werden.

#### § 7 Bildung von Verbünden

(1) Die Bestellung einer oder eines gemeinsamen Transplantationsbeauftragten ist zulässig, wenn die oder der Transplantationsbeauftragte an einem der am Verbund beteiligten Entnahmekrankenhäuser beschäftigt ist.
Die oder der Transplantationsbeauftragte ist in diesem Fall für alle Krankenhäuser des Verbundes zuständig.

(2) Die Entnahmekrankenhäuser nach Absatz 1 treffen untereinander und mit der oder dem Transplantationsbeauftragen eine schriftliche Vereinbarung, in der mindestens Regelungen zur Freistellung und zu organisatorischen Einzelheiten, insbesondere zu Meldeverpflichtungen, Schulungsangeboten und Haftungsfragen, festzulegen sind.

(3) Die Vereinbarung ist unverzüglich nach ihrem Abschluss dem für Gesundheit zuständigen Ministerium vorzulegen.

#### § 8 Ausnahmen von der Bestellpflicht

(1) In begründeten Einzelfällen können Entnahmekrankenhäuser insbesondere soweit und solange die Realisierung einer Organentnahme wegen der Besonderheiten des Entnahmekrankenhauses ausgeschlossen ist, von der Verpflichtung zur Bestellung einer oder eines Transplantationsbeauftragten durch das für Gesundheit zuständige Ministerium befreit werden.
Die Befreiung ist zu befristen.

(2) Das Entnahmekrankenhaus hat eine Befreiung nach Absatz 1 beim für Gesundheit zuständigen Ministerium zu beantragen.
Der Antrag ist zu begründen.
In dem Antrag ist die besondere Situation des Entnahmekrankenhauses zu belegen.

#### § 9 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Transplantationsgesetzdurchführungsverordnung vom 27. Juni 2016 (GVBl. II Nr. 31) außer Kraft.

Potsdam, den 28.
Oktober 2021

Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

Ursula Nonnemacher