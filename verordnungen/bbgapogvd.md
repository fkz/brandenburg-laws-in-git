## Verordnung über die Ausbildung und Prüfung für die Laufbahn des gehobenen vermessungstechnischen Verwaltungsdienstes im Land Brandenburg (Brandenburgische Ausbildungs- und Prüfungsordnung gehobener vermessungstechnischer Dienst - BbgAPOgvD)

Auf Grund des § 26 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S. 26), der durch Artikel 1 des Gesetzes vom 5.
Dezember 2013 (GVBl.
I Nr. 36) geändert worden ist, verordnet der Minister des Innern und für Kommunales im Einvernehmen mit der Ministerin der Finanzen und für Europa:

**Inhaltsübersicht**

### Abschnitt 1  
Vorbereitungsdienst

[§ 1 Geltungsbereich, Ziel des Vorbereitungsdienstes](#1)

[§ 2 Einstellungsvoraussetzungen](#2)

[§ 3 Einstellungsbehörde und Einstellungsverfahren](#3)

[§ 4 Ernennung, Bezüge](#4)

[§ 5 Ausbildungsbehörde und Ausbildungsstellen](#5)

[§ 6 Dauer der Ausbildung](#6)

[§ 7 Gliederung, Inhalt und Gestaltung der Ausbildung, Urlaub](#7)

[§ 8 Begleitung und Überwachung der Ausbildung](#8)

[§ 9 Beurteilung während der Ausbildung](#9)

[§ 10 Berücksichtigung der Belange behinderter Anwärterinnen und Anwärter](#10)

[§ 11 Beendigung des Beamtenverhältnisses auf Widerruf](#11)

### Abschnitt 2  
Laufbahnprüfung

[§ 12 Zweck und Gliederung der Laufbahnprüfung](#12)

[§ 13 Prüfungsamt, Prüfungsausschuss](#13)

[§ 14 Durchführung der Laufbahnprüfung](#14)

[§ 15 Schriftlicher Prüfungsteil](#15)

[§ 16 Mündlicher Prüfungsteil](#16)

[§ 17 Fernbleiben, Rücktritt](#17)

[§ 18 Bewertung der Prüfungsleistungen im Einzelnen](#18)

[§ 19 Abschließende Bewertung, Gesamtergebnis](#19)

[§ 20 Niederschrift über den Prüfungsablauf](#20)

[§ 21 Prüfungszeugnis](#21)

[§ 22 Wiederholung](#22)

[§ 23 Täuschung, Verstoß gegen die Prüfungsordnung](#23)

[§ 24 Prüfungsakte](#24)

### Abschnitt 3  
Übergangs- und Schlussvorschriften

[§ 25 Übergangsvorschriften](#25)

[§ 26 Inkrafttreten, Außerkrafttreten](#26)

### Abschnitt 1  
Vorbereitungsdienst

#### § 1 Geltungsbereich, Ziel des Vorbereitungsdienstes

(1) Diese Verordnung regelt den Vorbereitungsdienst für die Laufbahn des gehobenen vermessungstechnischen Verwaltungsdienstes.
Dieser umfasst die Ausbildung und die Laufbahnprüfung.

(2) Die Ausbildung soll sich darauf erstrecken, Kenntnisse im öffentlichen und privaten Recht und die Anwendung des auf der Hochschule erworbenen technischen Fachwissens in der Verwaltungspraxis zu vermitteln.

#### § 2 Einstellungsvoraussetzungen

(1) In den Vorbereitungsdienst können Bewerberinnen und Bewerber eingestellt werden, die

1.  die gesetzlichen Voraussetzungen für die Berufung in das Beamtenverhältnis erfüllen und
2.  als Bildungsvoraussetzung den Bachelorgrad oder einen gleichwertigen Abschluss über ein abgeschlossenes Studium an einer Hochschule oder Universität im Bereich Geodäsie und Geoinformation und das Wissensspektrum (Studieninhalte) nach Absatz 2 nachweisen.
    Dieser Nachweis ist durch persönlich qualifizierende Prüfungen anhand eines Abschlusszeugnisses sowie eines Diploma Supplement zu erbringen.

(2) Der Abschluss nach Absatz 1 Nummer 2 soll die folgenden Studieninhalte umfassen:

1.  Grundlagenwissen in den folgenden mathematisch-naturwissenschaftlichen Fächern:
    1.  Mathematik,
    2.  Geometrie,
    3.  Physik,
    4.  Statistik und Parameterschätzung,
    5.  Informatik;
2.  Fachwissen in den folgenden berufsfeldbezogenen Fächern:
    1.  Geoinformation und Geoinformationssysteme,
    2.  Vermessungskunde und Methoden der Ingenieurgeodäsie,
    3.  Ausgleichungsrechnung,
    4.  Photogrammetrie und Fernerkundung,
    5.  Landesvermessung und Liegenschaftskataster,
    6.  Landentwicklung,
    7.  Planung und Bodenordnung,
    8.  Immobilienwertermittlung.

#### § 3 Einstellungsbehörde und Einstellungsverfahren

(1) Die Bewerbung auf Einstellung in den Vorbereitungsdienst ist bei der Einstellungsbehörde einzureichen.
Einstellungsbehörde ist der Landesbetrieb Landesvermessung und Geobasisinformation Brandenburg.

(2) Der Bewerbung sind beizufügen:

1.  Lebenslauf,
2.  Zeugnis über den Nachweis der Hochschulreife,
3.  Zeugnisse über die Hochschulprüfungen (Bachelorprüfung oder Diplomprüfung) in einem Studiengang, der die Kriterien gemäß § 2 Absatz 1 Nummer 2 erfüllt, sowie gegebenenfalls über Zusatz- oder andere Prüfungen,
4.  gegebenenfalls Nachweise über eine berufliche Tätigkeit nach Ablegung des Hochschulexamens.

Die Vorlage eines Lichtbildes ist freiwillig.

(3) Vor der Einstellung sind auf Anforderung der Einstellungsbehörde vorzulegen:

1.  die Geburtsurkunde, gegebenenfalls Eheurkunde, Lebenspartnerschaftsurkunde und Geburtsurkunden von Kindern,
2.  ein Nachweis der deutschen Staatsangehörigkeit gemäß Artikel 116 des Grundgesetzes oder der Staatsangehörigkeit eines anderen Mitgliedsstaates der Europäischen Union, eines anderen Vertragsstaates des Abkommens über den Europäischen Wirtschaftsraum oder eines Drittstaates, dem Deutschland und die Europäische Union vertraglich einen entsprechenden Anspruch auf Anerkennung von Berufsqualifikationen eingeräumt haben,
3.  ein amtsärztliches Gesundheitszeugnis, das auch über das Seh-, Farbunterscheidungs- und Hörvermögen Auskunft gibt,
4.  eine persönliche schriftliche Erklärung, ob gerichtliche Strafen vorliegen oder ein gerichtliches Strafverfahren oder ein Ermittlungsverfahren der Staatsanwaltschaft anhängig ist,
5.  ein behördliches Führungszeugnis.

(4) Über die Einstellung in den Vorbereitungsdienst entscheidet die Einstellungsbehörde.
Sie teilt den ausgewählten Bewerberinnen und Bewerbern den Termin für die Einstellung mit.
Kommt eine Bewerberin oder ein Bewerber diesem Termin ohne triftigen Grund nicht nach, verliert die Zusage der Einstellung ihre Gültigkeit.

(5) Aus der Einstellung kann kein Anspruch auf eine spätere Verwendung im öffentlichen Dienst hergeleitet werden.

#### § 4 Ernennung, Bezüge

(1) In den Vorbereitungsdienst einzustellende Bewerberinnen und Bewerber werden unter Berufung in das Beamtenverhältnis auf Widerruf zu Vermessungsoberinspektoranwärterinnen und Vermessungsoberinspektoranwärtern (Anwärterinnen und Anwärter) ernannt.

(2) Die Anwärterinnen und Anwärter erhalten nach den hierfür geltenden Vorschriften Anwärterbezüge für Beamtinnen und Beamte im Vorbereitungsdienst.

#### § 5 Ausbildungsbehörde und Ausbildungsstellen

Ausbildungsbehörde ist der Landesbetrieb Landesvermessung und Geobasisinformation Brandenburg.
Die Ausbildungsbehörde weist die Anwärterinnen und Anwärter den Ausbildungsstellen zu.

#### § 6 Dauer der Ausbildung

(1) Der Vorbereitungsdienst dauert zwölf Monate.

(2) Der Vorbereitungsdienst kann um bis zu drei Monate verlängert werden, wenn das Ziel der Ausbildung in einzelnen Ausbildungsabschnitten oder insgesamt nicht erreicht wird oder wenn Dienstbefreiung nach der Erholungsurlaubs- und Dienstbefreiungsverordnung vom 16. September 2009 (GVBl.
II S.
618), die zuletzt durch Artikel 2 der Verordnung vom 2.
August 2019 (GVBl.
II Nr.
57 S. 10) geändert worden ist, in der jeweils geltenden Fassung gewährt wird und die Dienstbefreiung die Dauer von einem Monat überschreitet.
Er soll bei einem Beschäftigungsverbot nach dem Mutterschutzgesetz vom 23.
Mai 2017 (BGBl.
I S.
1228), das durch Artikel 57 Absatz 8 des Gesetzes vom 12.
Dezember 2019 (BGBl.
I S.
2652) geändert worden ist, in der jeweils geltenden Fassung, einer Elternzeit oder bei Dienstunfähigkeit entsprechend der Dauer der Unterbrechung verlängert werden, wenn die Unterbrechung einen Monat überschreitet.

(3) Über eine Verlängerung des Vorbereitungsdienstes entscheidet die Einstellungsbehörde.

#### § 7 Gliederung, Inhalt und Gestaltung der Ausbildung, Urlaub

(1) Der Vorbereitungsdienst gliedert sich in Ausbildungsabschnitte, deren Anzahl, Dauer und Inhalt im Rahmenausbildungsplan geregelt sind.
Der Rahmenausbildungsplan enthält zudem Vorgaben zur Gestaltung der Ausbildung.
Er wird von dem für das Vermessungs- und Katasterwesen zuständigen Ministerium erlassen.

(2) Die Anwärterinnen und Anwärter werden grundsätzlich nach dem Rahmenausbildungsplan ausgebildet.
Soll vom Rahmenausbildungsplan erheblich abgewichen werden, ist vorher die Zustimmung des für das Vermessungs- und Katasterwesen zuständigen Ministeriums einzuholen.

(3) Erholungsurlaub ist in den Ausbildungsplan nach § 8 Absatz 2 im Benehmen einzuarbeiten.

#### § 8 Begleitung und Überwachung der Ausbildung

(1) Die Ausbildungsbehörde bestellt zur Ausbildungsleiterin oder zum Ausbildungsleiter (Ausbildungsleitung) eine geeignete Bedienstete oder einen geeigneten Bediensteten, die oder der die Befähigung zum höheren technischen Verwaltungsdienst in der Fachrichtung Geodäsie und Geoinformation besitzt.
Die Ausbildungsleitung lenkt und überwacht die gesamte Ausbildung.

(2) Die Ausbildungsleitung stellt für die Anwärterinnen und Anwärter unter Berücksichtigung des Rahmenausbildungsplans nach § 7 Absatz 1 einen Ausbildungsplan auf, der die Abschnitte, Zeiten und Ausbildungsstellen sowie den Ausbildungsinhalt im Einzelnen festlegt.
Wünsche der Anwärterinnen und Anwärter können berücksichtigt werden.
Die Ausbildungsleitung ist dafür verantwortlich, dass der Ausbildungsplan eingehalten wird.
Abweichungen sind in begründeten Fällen zulässig.

(3) Die Ausbildung im Einzelnen obliegt der Ausbildungsbetreuung, die von der Leiterin oder dem Leiter der jeweiligen Ausbildungsstelle oder einer von ihr oder ihm beauftragten Person wahrgenommen wird.

(4) Die Anwärterinnen und Anwärter haben nach Vorgaben der Ausbildungsbehörde einen Ausbildungsnachweis zu führen und darin eine Übersicht über ihre wesentlichen Tätigkeiten zu geben.
Der Nachweis ist grundsätzlich monatlich der Ausbildungsbetreuung zur Bescheinigung und anschließend der Ausbildungsleitung zur Prüfung vorzulegen.

(5) Ausbildungsleitung und Ausbildungsbetreuung begleiten die Ausbildung der Anwärterinnen und Anwärter durch regelmäßige Feedback-Gespräche.

#### § 9 Beurteilung während der Ausbildung

(1) Jede Ausbildungsbetreuung beurteilt die Anwärterinnen und Anwärter nach Abschluss des bei ihr abgeleisteten Ausbildungsabschnittes oder -teilabschnittes unter Angabe der Art und Dauer der Beschäftigung nach ihren Leistungen (Arbeitsgüte, Arbeitsmenge, Arbeitsweise) und Befähigungen (Denk- und Urteilsvermögen, Organisationsvermögen, Befähigung zur Kommunikation und Zusammenarbeit) unter Verwendung eines Beurteilungsformulars, das von dem für das Vermessungs- und Katasterwesen zuständigen Ministerium vorgegeben wird.
Besondere Fähigkeiten oder Mängel sind zu vermerken.

(2) Beträgt die Ausbildungszeit bei einer Ausbildungsstelle weniger als sechs Wochen, bestätigt die Ausbildungsbetreuung nur die Art und Dauer der Beschäftigung.
Die in Absatz 1 geforderte Beurteilung entfällt.

(3) Die Ausbildungsleitung fertigt am Ende der Ausbildung unter Berücksichtigung des § 18 Absatz 4 eine abschließende Beurteilung über die gesamte Dauer des Vorbereitungsdienstes.
Die Beurteilung muss erkennen lassen, ob das Ziel der Ausbildung erreicht ist.
Absatz 1 gilt entsprechend.

(4) Die Beurteilungen nach den Absätzen 1 und 3 sind den Anwärterinnen und Anwärtern zu eröffnen und mit ihnen zu besprechen.
Die Eröffnungen sind aktenkundig zu machen und mit den Beurteilungen zu den Ausbildungsakten zu nehmen.

#### § 10 Berücksichtigung der Belange behinderter Anwärterinnen und Anwärter

(1) Im Rahmen der Ausbildung und bei der Laufbahnprüfung sind die besonderen Belange behinderter Anwärterinnen und Anwärter zu berücksichtigen.
Schwerbehinderten und ihnen gleichgestellte Menschen im Sinne des § 2 Absatz 2 und 3 des Neunten Buches Sozialgesetzbuch vom 23. Dezember 2016 (BGBl.
I S.
3234), das zuletzt durch Artikel 8 des Gesetzes vom 14. Dezember 2019 (BGBl.
I S.
2789) geändert worden ist, in der jeweils geltenden Fassung sind die ihrer Behinderung angemessenen Erleichterungen zu gewähren.
Art und Umfang der zu gewährenden Erleichterungen sind rechtzeitig mit den Betroffenen zu erörtern.
Die Erleichterungen dürfen nicht dazu führen, dass die fachlichen Anforderungen herabgesetzt werden.

(2) Anwärterinnen und Anwärter, die vorübergehend erheblich körperlich beeinträchtigt sind, können bei der Laufbahnprüfung auf Antrag angemessene Erleichterungen gewährt werden.
Absatz 1 Satz 4 gilt entsprechend.

(3) Anträge auf Prüfungserleichterungen sind zusammen mit der Mitteilung über die Zulassung gemäß § 14 Absatz 1 beim Prüfungsamt einzureichen.
Liegen die Voraussetzungen für die Gewährung einer Prüfungserleichterung erst zu einem späteren Zeitpunkt vor, ist der Antrag unverzüglich zu stellen.
Der Nachweis der Prüfungsbeeinträchtigung ist durch ein ärztliches Attest zu führen, welches Angaben über Art und Form der notwendigen Prüfungserleichterungen enthalten muss.
Das Prüfungsamt kann bestimmen, dass ein amtsärztliches Zeugnis beizubringen ist.

#### § 11 Beendigung des Beamtenverhältnisses auf Widerruf

(1) Das Beamtenverhältnis auf Widerruf endet mit dem Ablauf des Tages, an dem

1.  das Bestehen der Laufbahnprüfung oder
2.  die Nichtzulassung zur Laufbahnprüfung oder
3.  das endgültige Nichtbestehen der Laufbahnprüfung

bekannt gegeben worden ist.
Im Fall von Satz 1 Nummer 1 endet das Beamtenverhältnis jedoch frühestens nach Ablauf der für den Vorbereitungsdienst im Allgemeinen oder im Einzelfall festgesetzten Zeit.

(2) Die Einstellungsbehörde kann Anwärterinnen und Anwärter unter Widerruf des Beamtenverhältnisses aus dem Vorbereitungsdienst gemäß § 23 Absatz 4 des Beamtenstatusgesetzes vom 17.
Juni 2008 (BGBl.
I S.
1010), das zuletzt durch Artikel 10 des Gesetzes vom 20.
November 2019 (BGBl.
I S.
1626) geändert worden ist, entlassen, wenn ein wichtiger Grund vorliegt.
Dies ist insbesondere der Fall, wenn sie sich durch Verletzung der beamtenrechtlichen Pflichten oder durch sonstige tadelhafte Führung unwürdig erweisen, im Dienst belassen zu werden.

### Abschnitt 2  
Laufbahnprüfung

#### § 12 Zweck und Gliederung der Laufbahnprüfung

(1) In der Laufbahnprüfung haben die Anwärterinnen und Anwärter die Befähigung für den gehobenen vermessungstechnischen Verwaltungsdienst nachzuweisen.
Im Einzelnen sollen sie zeigen, dass sie ihre an einer Hochschule erworbenen Kenntnisse in der Praxis anzuwenden verstehen und dass sie mit den Aufgaben der Verwaltungen ihrer Laufbahn sowie den einschlägigen Rechts-, Verwaltungs- und technischen Vorschriften vertraut sind.

(2) Die Laufbahnprüfung besteht aus einem schriftlichen und einem mündlichen Prüfungsteil.
Der Prüfstoff ist dem Prüfstoffverzeichnis zu entnehmen.
Das Prüfstoffverzeichnis wird von dem für das Vermessungs- und Katasterwesen zuständigen Ministerium erlassen.

#### § 13 Prüfungsamt, Prüfungsausschuss

(1) Für die Organisation und Durchführung der Laufbahnprüfung ist das in die Landesakademie für öffentliche Verwaltung Brandenburg eingegliederte Staatliche Prüfungsamt für Verwaltungslaufbahnen (Prüfungsamt) zuständig.

(2) Für die Abnahme der Laufbahnprüfung bedient sich das Prüfungsamt eines Prüfungsausschusses.
Die Mitglieder des Prüfungsausschusses werden von dem für das Vermessungs- und Katasterwesen zuständigen Ministerium im Einvernehmen mit dem für Ländliche Entwicklung zuständigen Ministerium für die Dauer von fünf Jahren berufen.
Die Wiederberufung ist zulässig.
Scheidet ein Mitglied aus, wird die Nachfolge für die restliche Zeitdauer berufen.

(3) Der Prüfungsausschuss besteht aus einer oder einem Vorsitzenden und mindestens neun weiteren Mitgliedern, von denen mindestens eines auch zur oder zum stellvertretenden Vorsitzenden berufen wird.
Die oder der Vorsitzende und die stellvertretenden Vorsitzenden müssen die Befähigung zum höheren technischen Verwaltungsdienst in der Fachrichtung Geodäsie und Geoinformation besitzen.
Die weiteren Mitglieder sollen die Befähigung zum gehobenen vermessungstechnischen Verwaltungsdienst besitzen.

(4) Der mündliche Prüfungsteil wird von einer Prüfungskommission abgenommen.
Die Prüfungskommission setzt sich zusammen aus der oder dem Vorsitzenden oder einer oder einem stellvertretenden Vorsitzenden (die oder der Vorsitzende der Prüfungskommission) und mindestens drei weiteren Mitgliedern des Prüfungsausschusses.
Über die Besetzung der Prüfungskommission entscheidet die oder der Vorsitzende des Prüfungsausschusses.

(5) Die Mitglieder des Prüfungsausschusses sind bei ihrer Tätigkeit unabhängig und an Weisungen nicht gebunden.
Alle mit der Behandlung von Prüfungsangelegenheiten befassten Personen sind zur Verschwiegenheit in allen die Vorbereitung und Durchführung der Laufbahnprüfung betreffenden Angelegenheiten verpflichtet.

(6) Mitglieder des Prüfungsausschusses, bei denen ein Grund vorliegt, der geeignet ist, Misstrauen gegen eine unbefangene Amtsausübung zu rechtfertigen, dürfen bei der Prüfung nicht mitwirken.
Mitglieder des Prüfungsausschusses, die sich befangen fühlen, sowie Anwärterinnen oder Anwärter, die die Besorgnis der Befangenheit geltend machen wollen, haben dies sofort der oder dem Vorsitzenden des Prüfungsausschusses mitzuteilen.
§ 2 Absatz 3 Nummer 2 des Verwaltungsverfahrensgesetzes für das Land Brandenburg vom 7.
Juli 2009 (GVBl.
I S.
262, 264), das zuletzt durch Artikel 6 des Gesetzes vom 8.
Mai 2018 (GVBl.
I Nr.
8 S.
4) geändert worden ist, in Verbindung mit den §§ 20 und 21 des Verwaltungsverfahrensgesetzes in der Fassung der Bekanntmachung vom 23.
Januar 2003 (BGBl.
I S.
102), das zuletzt durch Artikel 5 Absatz 25 des Gesetzes vom 21.
Juni 2019 (BGBl.
I S.
846) geändert worden ist, und § 3 des Verwaltungsverfahrensgesetzes für das Land Brandenburg gelten entsprechend.
Tritt die Besorgnis während des mündlichen Prüfungsteils auf, haben dies die Mitglieder der Prüfungskommission unverzüglich, Anwärterinnen oder Anwärter sofort nach dem Prüfungsteil der Prüfungskommission mitzuteilen.
Die Entscheidung über den Ausschluss trifft die oder der Vorsitzende des Prüfungsausschusses, während der mündlichen Prüfung die Prüfungskommission.

(7) Der Prüfungsausschuss und die Prüfungskommission sind bei ihren Entscheidungen beschlussfähig, wenn die oder der Vorsitzende des Gremiums und drei weitere Mitglieder anwesend sind.
Soweit über die Leistungen im mündlichen Prüfungsteil entschieden wird, müssen die beschließenden Mitglieder an der Prüfung teilgenommen haben.
Prüfungsausschuss und Prüfungskommission entscheiden mit Stimmenmehrheit.
Bei Stimmengleichheit gibt die Stimme der oder des Vorsitzenden des Gremiums den Ausschlag.
Stimmenthaltung ist nicht zulässig.

(8) Der mündliche Prüfungsteil und die Beratungen des Prüfungsausschusses und der Prüfungskommission sind nicht öffentlich.

#### § 14 Durchführung der Laufbahnprüfung

(1) Anwärterinnen und Anwärter sind zum schriftlichen Prüfungsteil zugelassen, wenn die Leistungen in der Ausbildung in entsprechender Anwendung des § 9 Absatz 3 einen Monat vor Beginn der Prüfung durch die Ausbildungsleitung mindestens mit der Note „ausreichend“ beurteilt werden.
Die Ausbildungsbehörde teilt dem Prüfungsamt umgehend die zum schriftlichen Prüfungsteil zugelassenen Anwärterinnen und Anwärter mit.

(2) Die Ausbildungsbehörde übermittelt dem Prüfungsamt spätestens zwei Wochen vor dem Termin des mündlichen Prüfungsteils die abschließende Beurteilung nach § 9 Absatz 3.

(3) Das Prüfungsamt veranlasst die Ladung der zum schriftlichen Prüfungsteil zugelassenen Anwärterinnen und Anwärter und benachrichtigt die Ausbildungsbehörde.
Der Termin und der Ort des schriftlichen und mündlichen Prüfungsteils sind den Anwärterinnen und Anwärtern spätestens zwei Wochen vor dem jeweiligen Prüfungsteil mitzuteilen.

#### § 15 Schriftlicher Prüfungsteil

(1) Der schriftliche Prüfungsteil besteht aus je einer schriftlichen Arbeit in folgenden Prüfungsfächern:

1.  Allgemeine Rechts- und Verwaltungsgrundlagen,
2.  Liegenschaftskataster,
3.  Ländliche Neuordnung, Bodenordnung und Wertermittlung.

Die Dauer von jeweils fünf Stunden ist nicht zu überschreiten.

(2) Insgesamt ist an drei aufeinander folgenden Werktagen je eine schriftliche Arbeit unter Aufsicht zu fertigen.
Die zugelassenen Hilfsmittel werden in der Regel zur Verfügung gestellt.
Wenn die Anwärterin oder der Anwärter selbst Hilfsmittel mitbringen soll, werden diese in der Ladung nach § 14 Absatz 3 ausdrücklich benannt.

(3) Die Aufsicht führende Person öffnet am Fertigungstag zu Beginn der Prüfung den verschlossenen Umschlag mit der Aufgabenstellung und händigt diese der Anwärterin oder dem Anwärter aus.

(4) Spätestens mit Ablauf der Bearbeitungsfrist hat die Anwärterin oder der Anwärter die schriftliche Arbeit bei der Aufsicht führenden Person abzugeben.
Die Anwärterin oder der Anwärter gibt anstelle des Namens auf den Prüfungsarbeiten nur eine vom Prüfungsamt zugeteilte Kennziffer an.
Außer der Kennziffer dürfen die gefertigten Arbeiten keine sonstigen Hinweise auf die Person der Anwärterin oder des Anwärters enthalten.
Die gefertigten Arbeiten sind anschließend der oder dem Vorsitzenden des Prüfungsausschusses oder einem von dieser oder diesem benannten weiteren Mitglied zur Bewertung zuzuleiten.

(5) Die schriftlichen Arbeiten sollen digital bearbeitet werden.
Die Anwärterin oder der Anwärter kann auf Antrag beim Prüfungsamt eine handschriftliche Bearbeitung verlangen.

(6) Über den Verlauf des schriftlichen Prüfungsteils fertigt die Aufsicht führende Person eine Niederschrift unter Verwendung des vom Prüfungsamt dafür vorgesehenen Formulars an.
Die Niederschrift ist dem Prüfungsamt am letzten Fertigungstag zu übermitteln.

#### § 16 Mündlicher Prüfungsteil

(1) Der mündliche Prüfungsteil umfasst folgende Prüfungsfächer:

1.  Allgemeine Rechts- und Verwaltungsgrundlagen,
2.  Liegenschaftskataster,
3.  Ländliche Neuordnung, Bodenordnung und Wertermittlung,
4.  Landesvermessung, Geoinformation.

Bis zu drei Anwärterinnen oder Anwärter können in einer Gruppe gemeinsam geprüft werden.
In Abhängigkeit von der Gruppengröße beträgt die Prüfungsdauer je Prüfungsfach 20 bis 45 Minuten.
Die Gesamtprüfungsdauer des mündlichen Prüfungsteils soll je Anwärterin und Anwärter mindestens eine Zeitstunde betragen.

(2) Die oder der Vorsitzende der Prüfungskommission leitet den mündlichen Prüfungsteil.
Sie oder er hat darauf hinzuwirken, dass die Anwärterinnen und Anwärter in geeigneter Weise befragt werden und ist berechtigt, jederzeit in die Prüfung einzugreifen.

#### § 17 Fernbleiben, Rücktritt

(1) Bleibt eine Anwärterin oder ein Anwärter einer Prüfung oder Teilen derselben ohne Zustimmung des Prüfungsamtes fern oder tritt sie oder er ohne Zustimmung des Prüfungsamtes von der Prüfung oder einem Teil von ihr zurück, wird die Prüfung oder der betreffende Teil mit der Note „ungenügend“ (0 Punkte) bewertet.

(2) Stimmt das Prüfungsamt dem Fernbleiben oder dem Rücktritt zu, gilt die Prüfung oder der betreffende Prüfungsteil als nicht durchgeführt.
Die Zustimmung darf nur erteilt werden, wenn wichtige Gründe vorliegen, insbesondere, wenn die Anwärterin oder der Anwärter aufgrund von Krankheit an der Prüfung oder einem Prüfungsteil nicht teilnehmen kann.
Die Anwärterin oder der Anwärter hat das Vorliegen eines wichtigen Grundes unverzüglich gegenüber dem Prüfungsamt geltend zu machen und nachzuweisen.
Im Krankheitsfall ist ein ärztliches Zeugnis vorzulegen, aus dem sich die Prüfungsunfähigkeit ergibt und welches in der Regel nicht später als am Prüfungstag ausgestellt sein darf.
Das Prüfungsamt kann bestimmen, dass ein amtsärztliches Zeugnis beizubringen ist.

(3) Der Krankheit einer Anwärterin oder eines Anwärters steht die Krankheit eines von ihr oder ihm zu versorgenden Kindes oder die Pflege einer oder eines nahen Angehörigen in einer kurzfristig auftretenden Pflegesituation gleich.
In offensichtlichen Fällen kann auf die Vorlage eines ärztlichen Zeugnisses verzichtet werden.

(4) Hat sich eine Anwärterin oder ein Anwärter in Kenntnis einer gesundheitlichen Beeinträchtigung oder eines anderen Rücktrittsgrundes einer Prüfung oder einem Prüfungsteil unterzogen, kann ein nachträglicher Rücktritt wegen dieses Grundes nicht mehr genehmigt werden.

(5) Für Anwärterinnen und Anwärter, die mit Zustimmung des Prüfungsamtes einer Prüfung oder einem Prüfungsteil ferngeblieben oder davon zurückgetreten sind, bestimmt das Prüfungsamt eine Nachprüfung.
Bereits abgelegte Teile der Prüfung werden bei der Nachprüfung angerechnet.
Ein nicht oder nicht vollständig abgelegter mündlicher Prüfungsteil ist in vollem Umfang nachzuholen.

#### § 18 Bewertung der Prüfungsleistungen im Einzelnen

(1) Die schriftlichen Arbeiten sind jeweils von zwei Mitgliedern des Prüfungsausschusses nacheinander in der von der oder dem Vorsitzenden des Prüfungsausschusses bestimmten Reihenfolge zu beurteilen und mit einer der in Absatz 4 festgesetzten Noten und Punktzahlen mit schriftlicher Begründung zu bewerten.
Weichen die Bewertungen voneinander ab, so entscheidet der Prüfungsausschuss.

(2) Die Ergebnisse des schriftlichen Prüfungsteils sind der Anwärterin oder dem Anwärter vor dem mündlichen Prüfungsteil bekanntzugeben.

(3) Die Leistungen in den Prüfungsfächern des mündlichen Prüfungsteils werden von der Prüfungskommission mit einer der in Absatz 4 festgesetzten Noten und Punktzahlen bewertet.

(4) Die Bewertung ist nach den folgenden Noten und Punktzahlen vorzunehmen:

sehr gut

15 - 14 Punkte

eine den Anforderungen in besonderem Maße entsprechende Leistung;

gut

13 - 11 Punkte

eine den Anforderungen voll entsprechende Leistung;

befriedigend

10 - 8 Punkte

eine im Allgemeinen den Anforderungen entsprechende Leistung;

ausreichend

7 - 5 Punkte

eine Leistung, die zwar Mängel aufweist, aber im Ganzen den Anforderungen noch entspricht;

mangelhaft

4 - 2 Punkte

eine den Anforderungen nicht entsprechende Leistung, die jedoch erkennen lässt, dass die notwendigen Grundkenntnisse vorhanden sind und die Mängel in absehbarer Zeit behoben werden könnten;

ungenügend

1 - 0 Punkte

eine den Anforderungen nicht entsprechende Leistung, bei der selbst die Grundkenntnisse so lückenhaft sind, dass die Mängel in absehbarer Zeit nicht behoben werden könnten.

#### § 19 Abschließende Bewertung, Gesamtergebnis

(1) Nach dem schriftlichen Prüfungsteil stellt das Prüfungsamt aufgrund der Bewertungen nach § 18 fest, ob die Laufbahnprüfung bereits nach § 19 Absatz 5 Nummer 2 oder 3 nicht bestanden ist.
In diesem Fall entfällt der mündliche Prüfungsteil.
§ 21 Absatz 2 gilt entsprechend.

(2) Nach dem mündlichen Prüfungsteil stellt die Prüfungskommission unter Berücksichtigung der Absätze 3 bis 5 das Gesamtergebnis (Abschlussnote) fest und gibt es der Anwärterin oder dem Anwärter bekannt.

(3) Zur Ermittlung der Prüfungsnote werden die für die einzelnen Prüfungsleistungen festgesetzten Punktzahlen rechnerisch zusammengefasst.
Die Durchschnittspunktzahl des schriftlichen Prüfungsteils und die Durchschnittspunktzahl des mündlichen Prüfungsteils sind mit gleichem Gewicht zu berücksichtigen.

(4) Für die Ermittlung des Gesamtergebnisses wird die Punktzahl der Prüfungsnote mit 80 Prozent und die Punktzahl aus der abschließenden Beurteilung nach § 9 Absatz 3 mit 20 Prozent berücksichtigt.
Dem errechneten Punktwert entspricht eine der folgenden Noten:

14,00 bis 15,00 Punkte

sehr gut,

11,00 bis 13,99 Punkte

gut,

8,00 bis 10,99 Punkte

befriedigend,

5,00 bis 7,99 Punkte

ausreichend,

2,00 bis 4,99 Punkte

mangelhaft,

0,00 bis 1,99 Punkte

ungenügend.

Die Prüfungskommission kann bei der Entscheidung über das Gesamtergebnis den errechneten Punktwert um bis zu einem Punkt anheben, wenn der Gesamteindruck über den Leistungsstand, der während der Laufbahnprüfung und dem Vorbereitungsdienst von der Anwärterin oder dem Anwärter gewonnen wurde, dadurch zutreffender gekennzeichnet wird.

(5) Die Laufbahnprüfung ist nicht bestanden, wenn

1.  das Gesamtergebnis mit der Note „mangelhaft“ oder „ungenügend“ bewertet wird oder
2.  die Noten in einer oder mehreren schriftlichen Arbeiten „ungenügend“ oder in zwei oder mehreren schriftlichen Arbeiten „mangelhaft“ sind oder
3.  die Durchschnittspunktzahl des schriftlichen Prüfungsteils 4.99 oder schlechter lautet oder
4.  die Noten in einem oder mehreren Prüfungsfächern des mündlichen Prüfungsteils „ungenügend“ oder in zwei oder mehreren Prüfungsfächern des mündlichen Prüfungsteils „mangelhaft“ sind oder
5.  die Durchschnittspunktzahl des mündlichen Prüfungsteils 4.99 oder schlechter lautet.

#### § 20 Niederschrift über den Prüfungsablauf

Über den Prüfungsablauf ist eine Niederschrift anzufertigen, in der die Besetzung der Prüfungskommission, der Name der Anwärterin oder des Anwärters, die Einzelnoten und Punktzahlen der Prüfungsfächer im schriftlichen und im mündlichen Prüfungsteil, die Prüfungsnote, die Punktzahl aus der abschließenden Beurteilung nach § 9 Absatz 3 und das Gesamtergebnis festgehalten werden.
Die Niederschrift ist von der oder dem Vorsitzenden der Prüfungskommission und den weiteren an der mündlichen Prüfung beteiligten Prüferinnen und Prüfern zu unterzeichnen.

#### § 21 Prüfungszeugnis

(1) Nach bestandener Laufbahnprüfung erhält die Anwärterin oder der Anwärter ein Prüfungszeugnis, das die Einzelnoten und das Gesamtergebnis enthält.

(2) Wer die Laufbahnprüfung endgültig nicht bestanden hat, erhält hierüber einen schriftlichen Bescheid des Prüfungsamtes.

#### § 22 Wiederholung

(1) Hat die Anwärterin oder der Anwärter die Laufbahnprüfung nicht bestanden, so kann sie oder er die Laufbahnprüfung einmal wiederholen.

(2) Die Laufbahnprüfung ist vollständig zu wiederholen.
Auf Antrag der Anwärterin oder des Anwärters sind mindestens ausreichend bewertete Leistungen aus der ersten Prüfung als Prüfungsleistung für die Wiederholungsprüfung anzuerkennen.

(3) Die Prüfungskommission, im Falle des § 19 Absatz 1 jedoch der Prüfungsausschuss, befindet darüber, in welchen Abschnitten die Ausbildung einer Ergänzung bedarf und schlägt der Einstellungsbehörde die Dauer der zusätzlichen Ausbildung vor.
Sie soll mindestens zwei, höchstens vier Monate betragen.

#### § 23 Täuschung, Verstoß gegen die Prüfungsordnung

(1) Anwärterinnen oder Anwärter, die zu täuschen versuchen, die insbesondere bei den schriftlichen Arbeiten andere als die zugelassenen Hilfsmittel mit sich führen (§ 15 Absatz 2) oder die sich sonst eines Verstoßes gegen die Prüfungsordnung schuldig machen, soll die Fortsetzung der Laufbahnprüfung unter Vorbehalt gestattet werden.
Der Vorbehalt ist aktenkundig zu machen.
Bei einer erheblichen Störung soll die weitere Teilnahme an dem betreffenden Prüfungsteil versagt werden.

(2) Über die Folgen eines Vorfalls nach Absatz 1 oder einer Täuschung, die nach Abgabe einer schriftlichen Arbeit festgestellt wird, entscheidet das Prüfungsamt.
Je nach Schwere der Verfehlung kann die Wiederholung einzelner oder mehrerer schriftlicher Arbeiten angeordnet, die Anwärterin oder der Anwärter von der weiteren Laufbahnprüfung ausgeschlossen oder die Laufbahnprüfung für nicht bestanden erklärt werden.
Die Anwärterin oder der Anwärter erhält hierüber einen schriftlichen Bescheid des Prüfungsamtes.

(3) Wird eine Täuschung erst nach Aushändigung des Prüfungszeugnisses bekannt, ist das Prüfungsamt unverzüglich zu unterrichten.
Das Prüfungsamt kann die Laufbahnprüfung nachträglich für nicht bestanden erklären.
Diese Maßnahme ist zulässig innerhalb einer Frist von fünf Jahren nach dem letzten Tag der mündlichen Prüfung.

(4) Die oder der Betroffene ist vor der Entscheidung nach Absatz 2 oder Absatz 3 zu hören.

#### § 24 Prüfungsakte

(1) Zur Prüfungsakte zu nehmen sind insbesondere:

1.  die abschließende Beurteilung nach § 9 Absatz 3,
2.  die schriftlichen Arbeiten nach § 15,
3.  die Niederschrift nach § 20,
4.  eine Ausfertigung des Prüfungszeugnisses oder des Bescheids über die nicht bestandene Laufbahnprüfung nach § 21.

(2) Wer an der Laufbahnprüfung teilgenommen hat, kann auf Antrag seine Prüfungsakte im Prüfungsamt einsehen.
Der Antrag nach Satz 1 ist schriftlich an das Prüfungsamt zu stellen.

(3) Die Prüfungsakte wird nach Beendigung des Vorbereitungsdienstes mindestens fünf Jahre beim Prüfungsamt aufbewahrt.
Sie ist spätestens zehn Jahre nach Beendigung des Vorbereitungsdienstes zu vernichten.
Abweichend davon ist eine Ausfertigung des Prüfungszeugnisses nach Beendigung des Vorbereitungsdienstes mindestens 40 Jahre aufzubewahren.

### Abschnitt 3  
Übergangs- und Schlussvorschriften

#### § 25 Übergangsvorschriften

(1) Für Anwärterinnen und Anwärter, die sich zum Zeitpunkt des Inkrafttretens dieser Verordnung bereits im Vorbereitungsdienst befinden, ist grundsätzlich die bisher gültige Ausbildungs- und Prüfungsverordnung anzuwenden.
Auf Antrag der Anwärterin oder des Anwärters kann die Ausbildungsbehörde die neue Ausbildungs- und Prüfungsordnung anwenden, wenn sich alle im Rahmenausbildungsplan nach § 7 Absatz 1 aufgeführten Ausbildungsabschnitte und Ausbildungsinhalte noch in den Ausbildungsplan nach § 8 Absatz 2 integrieren lassen.

(2) Der nach der bisher gültigen Ausbildungs- und Prüfungsverordnung berufene Prüfungsausschuss für die Laufbahn des gehobenen vermessungstechnischen Verwaltungsdienstes gilt für die Dauer seiner Berufung mit der Maßgabe, dass die als Stellvertreterinnen und Stellvertreter berufenen Mitglieder als weitere Mitglieder dem Prüfungsausschuss angehören, als Prüfungsausschuss nach § 13 Absatz 2 und 3.

#### § 26 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Ausbildungs- und Prüfungsverordnung gehobener vermessungstechnischer und kartographischer Dienst vom 2. April 1996 (GVBl.
II S.
344), die zuletzt durch Artikel 40 des Gesetzes vom 25.
Januar 2016 (GVBl. I Nr. 5 S.
74) geändert worden ist, außer Kraft.

Potsdam, den 29.
September 2020

Der Minister des Innern und für Kommunales

Michael Stübgen