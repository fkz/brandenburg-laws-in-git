## Verordnung zur Regelung von Angelegenheiten auf dem Gebiet des Notarwesens (Notarverordnung - NotV)

Auf Grund

*   des § 6 Absatz 4 Satz 1 der Bundesnotarordnung in der im Bundesgesetzblatt Teil III, Gliederungsnummer 303-1, veröffentlichten bereinigten Fassung, der durch Artikel 1 Nummer 1 des Gesetzes vom 2.
    April 2009 (BGBl.
    I S. 696) eingefügt worden ist, in Verbindung mit § 1 Nummer 30 der Justiz-Zuständigkeitsübertragungsverordnung vom 9.
    April 2014 (GVBl. II Nr. 23);
*   des § 7 Absatz 5 Satz 2 der Bundesnotarordnung, der durch Artikel 1 Nummer 4 des Gesetzes vom 29.
    Januar 1991 (BGBl.
    I S.
    150) geändert worden ist, in Verbindung mit § 1 Nummer 30 der Justiz-Zuständigkeitsübertragungsverordnung;
*   des § 9 Absatz 1 Satz 2 der Bundesnotarordnung, der durch Artikel 1 Nummer 6 des Gesetzes vom 31.
    August 1998 (BGBl.
    I S.
    2585) neu gefasst worden ist, in Verbindung mit § 1 Nummer 30 der Justiz-Zuständigkeitsübertragungsverordnung;
*   des § 25 Absatz 2 Satz 1 der Bundesnotarordnung, der durch Artikel 1 Nummer 22 des Gesetzes vom 31.
    August 1998 (BGBl.
    I S.
    2585) eingefügt worden ist, in Verbindung mit § 1 Nummer 30 der Justiz-Zuständigkeitsübertragungsverordnung;
*   des § 112 Satz 1 der Bundesnotarordnung, der durch Artikel 3 Nummer 20 des Gesetzes vom 30.
    Juli 2009 (BGBl.
    I S.
    2449, 2464) neu gefasst worden ist, in Verbindung mit § 1 Nummer 30 der Justiz-Zuständigkeitsübertragungsverordnung

verordnet der Minister der Justiz und für Europa und Verbraucherschutz:

### Abschnitt 1  
Angelegenheiten der Notarinnen und Notare

#### § 1 Gemeinsame Berufsausübung der Notarinnen und Notare

(1) Zur hauptberuflichen Amtsausübung bestellte Notarinnen und Notare dürfen sich nur mit Genehmigung der Aufsichtsbehörde nach Anhörung der Notarkammer mit am selben Amtssitz bestellten Notarinnen und Notaren zur gemeinsamen Berufsausübung verbinden oder mit ihnen gemeinsame Geschäftsräume haben.
Die Genehmigung kann mit Auflagen verbunden oder befristet werden.

(2) Voraussetzung für die Genehmigung einer Verbindung nach Absatz 1 ist die vertragliche Sicherstellung der Eigenverantwortlichkeit, selbstständigen Amtsführung und inhaltlichen Gleichstellung jeder Partnerin und jedes Partners.

(3) Der Antrag auf Erteilung der Genehmigung nach Absatz 1 ist unter Vorlage des Sozietätsvertrages zu begründen.

(4) Eine Beendigung von Verbindungen nach Absatz 1 oder eine Änderung des Sozietätsvertrages ist der Aufsichtsbehörde anzuzeigen.
Die Fortführung mit einem Dritten bedarf der erneuten Genehmigung.

#### § 2 Beschäftigung von juristischen Mitarbeiterinnen und Mitarbeitern

(1) Die Beschäftigung von Mitarbeiterinnen und Mitarbeitern mit Befähigung zum Richteramt, Laufbahnprüfung für das Amt des Bezirksnotars oder Abschluss als Diplom-Juristin oder Diplom-Jurist (juristische Mitarbeiterinnen und Mitarbeiter) bedarf der Genehmigung der Aufsichtsbehörde.
Vor einer Entscheidung ist die Notarkammer anzuhören; der Aufsichtsbehörde ist der Anstellungsvertrag vorzulegen.

(2) Die Genehmigung ist zu versagen, wenn die für die Beschäftigung vorgesehene juristische Mitarbeiterin oder der für die Beschäftigung vorgesehene juristische Mitarbeiter zur Rechtsanwaltschaft zugelassen ist oder eine rechtsberatende Tätigkeit außerhalb des Anstellungsverhältnisses mit der Notarin oder dem Notar ausübt.

(3) Die Genehmigung kann mit Auflagen verbunden, mit dem Vorbehalt des Widerrufs erteilt oder befristet werden.

#### § 3 Anrechnungszeiten bei erneuter Bestellung

Bei einer erneuten Bestellung zur Notarin oder zum Notar sind die Zeiten einer vorübergehenden Amtsniederlegung nach § 48b der Bundesnotarordnung anzurechnen.
Die Gesamtdauer aller Anrechnungszeiten aus diesem Grund darf 18 Monate nicht überschreiten.
Eine bisherige Amtstätigkeit als Notarin oder Notar im Hauptberuf oder eine Tätigkeit einer Notarin oder eines Notars außer Dienst als Geschäftsführerin oder Geschäftsführer in einer Standesorganisation des Notariats, wegen derer das Amt des Notars niedergelegt worden ist, ist bei einer erneuten Bestellung in vollem Umfang anzurechnen.

#### § 4 Übertragung von Befugnissen der Landesjustizverwaltung

Von den Befugnissen, die der Landesjustizverwaltung nach der Bundesnotarordnung zustehen, werden auf die Präsidentin oder den Präsidenten des Oberlandesgerichts übertragen:

1.  die Entscheidung über die Übertragung der Verwahrung der Akten, Bücher oder Urkunden nach § 51 Absatz 1 Satz 2 der Bundesnotarordnung,
2.  die Erteilung der Genehmigung bei Übernahme von Räumen oder Angestellten nach § 53 Absatz 1 der Bundesnotarordnung,
3.  die Bestellung einer Notariatsverwalterin oder eines Notariatsverwalters und der vorzeitige Widerruf der Bestellung (§ 57 Absatz 2 Satz 1 und § 64 Absatz 1 Satz 3 der Bundesnotarordnung),
4.  die Mitteilung der Beendigung des Amtes eines Notariatsverwalters (§ 64 Absatz 1 Satz 2 der Bundesnotarordnung).

### Abschnitt 2  
Ausbildung der Notarassessorinnen und Notarassessoren

#### § 5 Ziel des Anwärterdienstes

Ziel des Anwärterdienstes ist es, die Notarassessorinnen und Notarassessoren auf die Aufgaben der Notarin oder des Notars als unabhängiger Träger des öffentlichen Amtes auf dem Gebiet der vorsorgenden Rechtspflege vorzubereiten.

#### § 6 Inhalt der Ausbildung

(1) Die Notarassessorin oder der Notarassessor soll in alle Arten notarieller Tätigkeit eingewiesen werden, wobei auf die der Notarin und dem Notar obliegenden Belehrungs-, Beratungs- und Betreuungspflichten besonderes Gewicht zu legen ist.
Die Notarassessorin oder der Notarassessor ist an der Vorbereitung und Abwicklung von Urkundsgeschäften zu beteiligen, bei Gesprächen mit den Parteien hinzuzuziehen und in der Zusammenarbeit mit Gerichten, Grundbuchämtern und sonstigen Dienststellen zu üben.
Sie oder er soll auch im Steuer- und Kostenwesen sowie in der Führung der Urkundenrolle und der sonstigen Bücher und Akten der Notarin und des Notars unterwiesen und mit der Leitung und Organisation einer Notarstelle vertraut gemacht werden.

(2) Die Notarassessorin oder der Notarassessor ist über das Berufsrecht und die Pflichten einer Notarin und eines Notars gegenüber der Notarkammer Brandenburg und der Ländernotarkasse zu unterrichten.
Die Präsidentin oder der Präsident der Notarkammer kann die Notarassessorin oder den Notarassessor verpflichten, Gutachten zu erstellen und Vorträge in Kammerversammlungen zu halten sowie bei anderen Aus- und Fortbildungsveranstaltungen mitzuwirken.

(3) Mit fortschreitender Ausbildungszeit soll die Notarassessorin oder der Notarassessor in vermehrtem Umfang zur Tätigkeit als Notarvertreterin oder Notarvertreter oder Notariatsverwalterin oder Notariatsverwalter herangezogen werden.

(4) Die Notarassessorin oder der Notarassessor muss ohne Rücksicht auf die personelle Besetzung der Notarstelle der Ausbildungsnotarin oder des Ausbildungsnotars auf Anforderung der Präsidentin oder des Präsidenten der Notarkammer zu anderweitigen Dienstleistungen zur Verfügung stehen.

#### § 7 Ausbildungsstellen

(1) In den ersten zwei Jahren des Anwärterdienstes soll die Notarassessorin oder der Notarassessor mindestens zwei Notarinnen oder Notaren zur Ausbildung zugewiesen werden, deren Amtssitze sich nicht am gleichen Ort befinden und deren Ämter unterschiedliche Strukturen aufweisen sollen.
Die Notarassessorin oder der Notarassessor soll insgesamt mindestens 18 Monate des Anwärterdienstes bei Notarinnen oder Notaren oder als Notarvertreterin oder Notarvertreter oder als Notariatsverwalterin oder Notariatsverwalter ableisten.
Sie oder er hat von den Standesorganisationen veranstaltete oder benannte Ausbildungskurse und Fortbildungsveranstaltungen zu besuchen.

(2) Für die Zuweisung einer Notarassessorin oder eines Notarassessors an eine Notarin oder einen Notar ist grundsätzlich maßgebend, ob die Notarstelle und deren Inhaberin oder Inhaber zur Ausbildung von Notarassessorinnen und Notarassessoren geeignet sind.

(3) Anstelle oder neben der Überweisung an eine Notarin oder einen Notar kann die Notarassessorin oder der Notarassessor auch an eine Standesorganisation (zum Beispiel Notarkammer, Ländernotarkasse, Bundesnotarkammer, Deutsches Notarinstitut) oder an eine oberste Landes- oder Bundesbehörde, die eine dem Zweck der Ausbildung dienende Tätigkeit der Notarassessorin oder des Notarassessors gewährleistet, überwiesen werden.
Absatz 1 Satz 2 bleibt unberührt.

(4) Die Notarassessorin oder der Notarassessor kann auch an ausländische Notarinnen oder Notare oder Standesorganisationen abgeordnet werden.
Zeiten eines solchen Auslandspraktikums werden angerechnet, soweit sie sechs Monate nicht überschreiten und ein Beurteilungsbeitrag im Sinne des § 8 Absatz 2 Satz 3 vorliegt.

#### § 8 Beurteilung

(1) Die Notarassessorin oder der Notarassessor ist zu beurteilen

1.  auf Anforderung des für Justiz zuständigen Ministeriums,
2.  bei der ersten Bewerbung um eine freie Notarstelle.

(2) Die Beurteilung der Notarassessorin oder des Notarassessors erteilt die Präsidentin oder der Präsident der Notarkammer.
Jede Notarin und jeder Notar, bei dem eine Notarassessorin oder ein Notarassessor länger als drei Monate beschäftigt war, erstellt bei Ablauf der Zuweisung oder Abordnung einen schriftlichen Beurteilungsbeitrag.
War die Notarassessorin oder der Notarassessor bei einer Stelle im Sinne des § 7 Absatz 3 oder 4 länger als drei Monate tätig, so erstellt die zur Vertretung dieser Stelle berufene Person einen schriftlichen Beurteilungsbeitrag.

(3) Beurteilungsbeiträge und Beurteilungen müssen erkennen lassen, ob die Notarassessorin oder der Notarassessor für das Amt der Notarin oder des Notars geeignet ist.
Sie sollen die Leistungen der Notarassessorin oder des Notarassessors im Vergleich zu anderen Notarassessorinnen und Notarassessoren objektiv darstellen und von ihrer oder seiner Eignung, Befähigung und Leistung ein zutreffendes Bild geben.

(4) Beurteilungsbeiträge und Beurteilungen schließen mit einem Gesamturteil (für das Amt der Notarin/des Notars geeignet; für das Amt der Notarin/des Notars noch nicht geeignet; für das Amt der Notarin/des Notars nicht geeignet) ab.

(5) Die von der Präsidentin oder dem Präsidenten der Notarkammer erstellte Beurteilung wird dem für Justiz zuständigen Ministerium übersandt.
Vor der Übersendung ist die Beurteilung der Notarassessorin oder dem Notarassessor durch Übersendung einer Kopie zu eröffnen.

#### § 9 Anrechnung von Zeiten auf die Dauer des Anwärterdienstes

(1) Zeiten, in denen die Notarassessorin oder der Notarassessor bei einer Ausbildungsstelle nach § 7 Absatz 3 Satz 1 tätig war, werden auf die Dauer des Anwärterdienstes angerechnet.

(2) Auf die Dauer des nach § 6 Absatz 3 Satz 2 der Bundesnotarordnung zu berücksichtigenden Anwärterdienstes einer sich auf eine ausgeschriebene Notarstelle bewerbenden Notarassessorin oder eines sich auf eine ausgeschriebene Notarstelle bewerbenden Notarassessors werden auf Antrag der Notarassessorin oder des Notarassessors angerechnet:

1.  Wehr- und Ersatzdienstzeiten bis zu der für die Notarassessorin oder den Notarassessor bei Antritt dieses Dienstes maßgeblichen gesetzlichen Dauer des Grundwehrdienstes,
2.  Zeiten eines Beschäftigungsverbotes nach Mutterschutzvorschriften während des Anwärterdienstes,
3.  Zeiten der Freistellung wegen der Inanspruchnahme von Elternzeit während des Anwärterdienstes.

Zeiten nach Satz 1 werden nur angerechnet, wenn die Notarassessorin oder der Notarassessor in dem gemäß § 6b Absatz 4 der Bundesnotarordnung maßgeblichen Zeitpunkt mindestens drei Jahre Anwärterdienst geleistet hat.
Sie werden zusammen nur bis zu einer Dauer von 18 Monaten angerechnet.
Der Antrag nach Satz 1 ist schriftlich zu stellen und muss spätestens bis zum Ende der Bewerbungsfrist bei dem für Justiz zuständigen Ministerium eingegangen sein.
Nachweise für anzurechnende Zeiten sind beizufügen.

#### § 10 Dienstunfähigkeit wegen Krankheit

(1) Wird die Notarassessorin oder der Notarassessor wegen Krankheit dienstunfähig, ist dies der Notarkammer durch Vorlage einer ärztlichen Bescheinigung unverzüglich anzuzeigen und die Ausbildungsstelle, bei der die Notarassessorin oder der Notarassessor beschäftigt ist, unverzüglich zu informieren.

(2) Die Notarkammer berichtet dem für Justiz zuständigen Ministerium bei der Bewerbung der Notarassessorin oder des Notarassessors um eine freie Notarstelle, wenn sich aus der Dauer oder der Art der Krankheit einer Notarassessorin oder eines Notarassessors Bedenken gegen deren körperliche Tauglichkeit ergeben.

(3) Die Notarkammer kann zum Nachweis einer Krankheit von der Notarassessorin oder dem Notarassessor, falls dies erforderlich erscheint, die Vorlage einer amtsärztlichen Bescheinigung verlangen.

(4) Dienstunterbrechungen infolge Dienstunfähigkeit wegen Krankheit werden bis zu 30 Tagen jährlich auf die Dauer des Anwärterdienstes angerechnet; dies gilt nicht, wenn die Notarassessorin oder der Notarassessor den nach Absatz 3 geforderten Nachweis nicht erbracht hat.
Über eine weitergehende Anrechnung entscheidet die Präsidentin oder der Präsident der Notarkammer im Einvernehmen mit dem für Justiz zuständigen Ministerium.

#### § 11 Urlaub, Teilzeitbeschäftigung

(1) Die Notarassessorin oder der Notarassessor erhält unter Anrechnung auf den Anwärterdienst Erholungsurlaub von gleicher Dauer wie eine Richterin oder ein Richter auf Probe im Land Brandenburg.
Den Erholungsurlaub erteilt die Präsidentin oder der Präsident der Notarkammer auf Antrag der Notarassessorin oder des Notarassessors.
Der Antrag ist durch die Notarassessorin oder den Notarassessor über die Ausbildungsstelle vorzulegen.

(2) Urlaub aus anderen Anlässen und Dienstbefreiung kann entsprechend den für Richterinnen und Richter auf Probe im Land Brandenburg geltenden Bestimmungen gewährt werden.
Absatz 1 Satz 2 und 3 gilt entsprechend.

(3) Urlaub, der nicht Erholungsurlaub ist, wird bis zu 14 Tagen jährlich auf die Dauer des Anwärterdienstes angerechnet.
Über eine weitergehende Anrechnung entscheidet die Präsidentin oder der Präsident der Notarkammer im Einvernehmen mit dem für Justiz zuständigen Ministerium.

(4) Auf Antrag ist der Notarassessorin oder dem Notarassessor Teilzeitbeschäftigung bis zur Hälfte der regelmäßigen Arbeitszeit zu bewilligen, wenn sie oder er

1.  mindestens ein Kind unter 18 Jahren betreut oder
2.  einen nach einem ärztlichen Gutachten pflegebedürftigen sonstigen Angehörigen tatsächlich betreut oder pflegt und zwingende dienstliche Belange nicht entgegenstehen.

Über den Antrag entscheidet die Notarkammer.

(5) Die Notarkammer kann auch nachträglich die Dauer der Teilzeitbeschäftigung beschränken oder den Umfang der zu leistenden Arbeitszeit erhöhen, soweit zwingende dienstliche Belange dies erfordern.

(6) Eine Teilzeitbeschäftigung wird bei der Berechnung der Regelanwärterdienstzeit nach § 7 Absatz 1 der Bundesnotarordnung im Verhältnis der bewilligten zur regelmäßigen Dienstzeit berücksichtigt.
Im Übrigen wird die Teilzeitbeschäftigung wie eine Vollzeitbeschäftigung berücksichtigt.

### Abschnitt 3  
Schlussvorschrift

#### § 12 Übergangsvorschrift

Auf Notarassessorinnen und Notarassessoren, die den Anwärterdienst vor dem 1.
April 2019 angetreten haben, findet § 9 Absatz 3 in der bis zum 31. März 2019 geltenden Fassung Anwendung.

Potsdam, den 6.
Januar 2015

Der Minister der Justiz und für Europa und Verbraucherschutz

Dr.
Helmuth Markov