## Verordnung zur Regelung der Versteigerung im Internet  (Internetversteigerungsverordnung - IntVerstV)

Auf Grund des § 814 Absatz 3 der Zivilprozessordnung in der Fassung der Bekanntmachung vom 5.
Dezember 2005 (BGBl.
I S. 3202; 2006 I S. 431; 2007 I S. 1781) und des § 979 Absatz 1b des Bürgerlichen Gesetzbuches in der Fassung der Bekanntmachung vom 2.
Januar 2002 (BGBl.
I S. 42, 2909; 2003 I S. 738), von denen durch Gesetz vom 30.
Juli 2009 (BGBl.
I S. 3145) § 814 Absatz 3 durch Artikel 1 eingefügt und § 979 Absatz 1b durch Artikel 4 geändert worden ist, in Verbindung mit § 1 Nummer 9 und 50 der Justiz-Zuständigkeitsübertragungsverordnung vom 28.
November 2006 (GVBl.
II S. 479), der zuletzt durch Verordnung vom 14.
Dezember 2010 (GVBl.
II Nr.
91) geändert worden ist, verordnet der Minister der Justiz:

§ 1  
Zeitpunkt
---------------

Für die Gerichtsvollzieher des Landes Brandenburg ist die Versteigerung im Internet gemäß § 814 Absatz 2 Nummer 2 der Zivilprozessordnung nach Inkrafttreten dieser Verordnung zugelassen.

§ 2  
Versteigerungsplattform
-----------------------------

(1) Versteigerungen durch die Gerichtsvollzieher des Landes Brandenburg im Internet gemäß § 814 Absatz 2 Nummer 2 der Zivilprozessordnung sowie Versteigerungen von an Justizbehörden abgelieferten Fundsachen und im Besitz von Justizbehörden befindlichen unanbringbaren Sachen gemäß § 979 Absatz 1a des Bürgerlichen Gesetzbuches erfolgen ausschließlich über die Versteigerungsplattform „Justiz-Auktion“ ([www.justiz-auktion.de](http://www.justiz-auktion.de/)).

(2) Für Versteigerungen gemäß § 814 Absatz 2 Nummer 2 der Zivilprozessordnung gelten ergänzend die §§ 3 bis 7 dieser Verordnung.

§ 3  
Zulassung und Ausschluss
------------------------------

(1) Zur Teilnahme an der Versteigerung im Internet zugelassen sind nur unbeschränkt geschäftsfähige natürliche Personen, juristische Personen und Personengesellschaften.
Beschränkt geschäftsfähige natürliche Personen oder solche, für die ein Einwilligungsvorbehalt im Aufgabenkreis der Vermögenssorge besteht, sind zugelassen, soweit ihr gesetzlicher Vertreter die Einwilligung zur Teilnahme und zur Abgabe von Geboten im Rahmen der Versteigerung im Internet erklärt hat.
Nicht zur Teilnahme an der Versteigerung im Internet zugelassen sind Personen, denen die Verfügungsbefugnis über den jeweiligen Gegenstand durch Entscheidung in einem strafrechtlichen Verfahren versagt worden ist, sowie der Gerichtsvollzieher und die von ihm zugezogenen Gehilfen (§ 450 des Bürgerlichen Gesetzbuches).

(2) Für die Registrierung sind ein frei wählbarer Benutzername, ein Passwort, der Vor- und Familienname (Firma), Adresse, eine E-Mail-Adresse sowie das Geburtsdatum anzugeben.
Ändern sich die bei der Registrierung angegebenen Daten, ist die teilnehmende Person verpflichtet, die Angaben unverzüglich zu aktualisieren.
Dies gilt auch für die Änderung der E-Mail-Adresse.

(3) Teilnehmende Personen können schriftlich oder per E-Mail die Aufhebung ihrer Registrierung verlangen.
Das Schreiben ist unter Angabe von Vor- und Familienname, Geburtsdatum, E-Mail-Adresse und Benutzername an das Kompetenzzentrum Justiz-Auktion Nordrhein-Westfalen bei dem Generalstaatsanwalt Hamm ([cc-justiz-auktion@gsta-hamm.nrw.de](mailto:cc-justiz-auktion@gsta-hamm.nrw.de)) zu richten.
Die Löschung der Daten erfolgt, sobald sie zur Erfüllung und Abwicklung noch bestehender Rechtsverhältnisse nicht mehr benötigt werden.
Durch die Aufhebung der Registrierung erlischt nicht die Bindung an wirksam abgegebene Höchstgebote bis zum Ablauf der Versteigerung oder dem Schluss der Versteigerung.

(4) Teilnehmende Personen können bei einem Verstoß gegen Absatz 1 und § 5 Absatz 2 Satz 2 von der Versteigerung ausgeschlossen werden.
Im Falle des § 817 Absatz 3 Satz 2 der Zivilprozessordnung sind sie von der Versteigerung auszuschließen.
Über den Ausschluss entscheidet der die Versteigerung durchführende Gerichtsvollzieher.
Die betroffenen Personen werden von dem Ausschluss per E-Mail informiert.
Der Ausschluss ist an das Kompetenzzentrum Justiz-Auktion Nordrhein-Westfalen bei dem Generalstaatsanwalt Hamm mitzuteilen.

(5) Bei mehrfachen Verstößen gemäß Absatz 4 können teilnehmende Personen von sämtlichen Versteigerungen im Anwendungsbereich dieser Verordnung ausgeschlossen werden.
Über den Ausschluss entscheidet das Kompetenzzentrum Justiz-Auktion Nordrhein-Westfalen bei dem Generalstaatsanwalt Hamm.
Die Anhörung kann per E-Mail erfolgen.
Absatz 4 Satz 4 gilt entsprechend.

§ 4  
Beginn, Ende und Abbruch der Versteigerung
------------------------------------------------

(1) Die Versteigerung beginnt und endet zu den von dem Gerichtsvollzieher bestimmten Zeitpunkten.
Beginn und Ende der Versteigerung werden mit der Artikelbeschreibung angezeigt.

(2) Die Versteigerung ist abzubrechen,

1.  wenn die Zwangsvollstreckung einzustellen ist,
2.  wenn die Zwangsvollstreckung zu beschränken ist und von der Beschränkung die Versteigerung der jeweiligen Sache betroffen ist,
3.  sobald der Erlös aus anderen Versteigerungen zur Befriedigung des Gläubigers und zur Deckung der Kosten der Zwangsvollstreckung hinreicht (§ 818 der Zivilprozessordnung),
4.  wenn die Veräußerung des Gegenstands aus Rechtsgründen unzulässig ist,
5.  wenn sich nach Beginn der Versteigerung ergibt, dass die Beschreibung unzutreffend ist.

(3) Die Versteigerung ist abgebrochen, sobald die Versteigerungsplattform „Justiz-Auktion“ vom Betreiber infolge technischer Störungen innerhalb eines Zeitraums von 30 Minuten vor dem Versteigerungsende nicht im Internet zur Verfügung gestellt wird.
Mit dem Abbruch erlöschen die registrierten Gebote.

§ 5  
Versteigerungsbedingungen
-------------------------------

(1) Zur Versteigerung gelangen die auf der Versteigerungsplattform „Justiz-Auktion“ eingestellten Sachen.
Maßgeblich ist die Beschreibung der Sache im Angebot.
Die Beschreibung hat eine Erklärung zu enthalten, ob und inwieweit die Sache auf Mängel, insbesondere auf ihre Funktionstauglichkeit, untersucht worden ist.
Im Angebot sind die Versand- und Zahlungsmodalitäten darzustellen.
Die teilnehmenden Personen sind darüber zu belehren, dass Gewährleistungsansprüche ausgeschlossen sind (§ 806 der Zivilprozessordnung) und ein Widerrufsrecht gemäß § 312g Absatz 1 des Bürgerlichen Gesetzbuches nicht besteht.

(2) Gebote können nur von registrierten Personen abgegeben werden.
Die Abgabe von Geboten mittels nicht von dem Betreiber der Versteigerungsplattform „Justiz-Auktion“ autorisierter Datenverarbeitungsprozesse ist unzulässig.
Eine Erhöhung des Gebots hat mindestens in vom Mindestgebot abhängigen Steigerungsschritten zu erfolgen.
Der nächsthöhere Steigerungsschritt wird automatisch angezeigt.
Ein Gebot erlischt, wenn ein Übergebot abgegeben wird.

(3) Der Zuschlag ist der Person erteilt, die am Ende der Versteigerung (§ 4 Absatz 1) das höchste, wenigstens das nach § 817a Absatz 1 Satz 1 der Zivilprozessordnung zu erreichende Mindestgebot abgegeben hat.
Sie wird von dem Zuschlag per E-Mail benachrichtigt.

§ 6  
Anonymisierung
--------------------

Die Angaben zur Person des Schuldners sind vor ihrer Veröffentlichung zu anonymisieren.
Es ist zu gewährleisten, dass die Daten der Bieter anonymisiert werden.

§ 7  
Verfahren
---------------

Der Meistbietende wird über die Ablieferungs- und Zahlungsmodalitäten per E-Mail erneut informiert.
Das Kaufgeld und anfallende Versandkosten sind spätestens zehn Kalendertage nach Absendung der E-Mail gemäß Satz 1 zu zahlen.
Die zugeschlagene Sache darf nur abgeliefert werden, wenn das Kaufgeld und die anfallenden Versandkosten gezahlt worden sind oder bei Ablieferung gezahlt werden.
Wird die zugeschlagene Sache übersandt, so gilt die Ablieferung mit der Übergabe an die zur Ausführung der Versendung bestimmte Person als bewirkt.
Im Übrigen gelten hinsichtlich des Zuschlags, der Ablieferung und des Mindestgebots die §§ 817 und 817a der Zivilprozessordnung.

§ 8  
Inkrafttreten
-------------------

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2011 in Kraft.

Potsdam, den 8.
Februar 2011

Der Minister der Justiz  
Dr.
Volkmar Schöneburg