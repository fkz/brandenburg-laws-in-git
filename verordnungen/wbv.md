## Verordnung zur Grundversorgung und Förderung nach dem Brandenburgischen Weiterbildungsgesetz (Weiterbildungsverordnung - WBV)

Auf Grund des § 6 Absatz 3 und des § 27 Absatz 4 des Brandenburgischen Weiterbildungsgesetzes vom 15.
Dezember 1993 (GVBl.
I S. 498), von denen § 6 Absatz 3 durch Artikel 3 des Gesetzes vom 4.
Juni 2003 (GVBl.
I S. 172, 173) geändert worden ist, verordnet die Ministerin für Bildung, Jugend und Sport im Einvernehmen mit dem Minister der Finanzen und dem Minister des Innern und für Kommunales mit Zustimmung des für Bildung zuständigen Ausschusses des Landtages:

### Abschnitt 1  
Grundversorgung der Weiterbildung

#### § 1 Gegenstand der Grundversorgung der Weiterbildung

(1) Die Grundversorgung der Weiterbildung umfasst ein staatlich gefördertes Angebot der Weiterbildung im Sinne des § 2 des Brandenburgischen Weiterbildungsgesetzes, das von den Landkreisen und kreisfreien Städten für ihr Gebiet sichergestellt wird und allen Menschen im Land offen steht.

(2) Die Grundversorgung umfasst insbesondere die allgemeine, berufliche, kulturelle und politische Weiterbildung.

(3) Zur Grundversorgung der Weiterbildung zählen nicht Weiterbildungsmaßnahmen, die

1.  der Erholung, Unterhaltung oder Geselligkeit dienen,
2.  gestaltende und künstlerische Praxis vermitteln, soweit sie nicht dem Einführen in eine Fertigkeit dienen,
3.  dem Erwerb von Fahrerlaubnissen, Fischereischeinen oder sonstigen Berechtigungen dienen,
4.  der sportlichen Ausbildung, dem Fitnesstraining dienen oder Praxis in Sport und Gesundheitsbildung vermitteln, soweit sie nicht dem Einführen dienen,
5.  Kenntnisse und Fertigkeiten auf den Gebieten des Feuer- und Katastrophenschutzes, der Ersten Hilfe oder der Pannenhilfe vermitteln,
6.  Nachhilfen, Besuchen von Film-, Konzert- oder Theaterveranstaltungen dienen,
7.  partei- oder verbandspolitischen Charakter haben,
8.  im Rahmen von Exkursionen außerhalb des Landkreises oder der kreisfreien Stadt stattfinden; die Landkreise und kreisfreien Städte können Ausnahmen zulassen,
9.  der durch Rechtsvorschriften geregelten berufs- und arbeitsplatzbezogenen Fortbildung, der Anpassungsqualifizierung oder der Umschulung dienen; dies schließt alle betrieblichen und organisationsinternen Schulungen ein,
10.  durch das Aufenthaltsgesetz in der Fassung der Bekanntmachung vom 25.
    Februar 2008 (BGBl.
    I S.
    162), das zuletzt durch Artikel 1 des Gesetzes vom 12.
    Juli 2018 (BGBl. I S. 1147) geändert worden ist, in der jeweils geltenden Fassung geregelt sind.

#### § 2 Zulassung, Trägervielfalt

(1) Zugelassen zur Grundversorgung der Weiterbildung gemäß § 6 des Brandenburgischen Weiterbildungsgesetzes sind anerkannte Weiterbildungseinrichtungen oder deren anerkannte Außenstellen, die im Landkreis oder der kreisfreien Stadt ansässig sind.
Andere nach dem Brandenburgischen Weiterbildungsgesetz anerkannte Weiterbildungseinrichtungen können bei Bedarf berücksichtigt werden.

(2) Die gemäß § 5 Absatz 1 des Brandenburgischen Weiterbildungsgesetzes von den Landkreisen und kreisfreien Städten zu sichernde Trägervielfalt ist dann gegeben, wenn Weiterbildungseinrichtungen unterschiedlicher Träger in der Grundversorgung tätig sind.

(3) Kann der Trägervielfalt voraussichtlich im folgenden Jahr nicht entsprochen werden, soll dies von dem Landkreis oder der kreisfreien Stadt bis zum 31. Dezember des laufenden Jahres gegenüber dem für Bildung zuständigen Ministerium schriftlich begründet werden.

#### § 3 Verfahren

(1) Für die Genehmigung der Weiterbildungsangebote zur Grundversorgung der Weiterbildung sind ein Antrag und die Vorlage der Programmplanung beim Landkreis oder der kreisfreien Stadt erforderlich.
Termine und weitere Einzelheiten des Verfahrens legt der Landkreis oder die kreisfreie Stadt selbständig fest.

(2) Die Mitglieder des regionalen Weiterbildungsbeirats stimmen die genehmigungsfähigen Weiterbildungsangebote sowie die jeweiligen Anteile der Weiterbildungseinrichtungen am Umfang der Grundversorgung ab.
Sie berücksichtigen dabei möglichst alle Inhaltsbereiche der Grundversorgung gemäß § 2 Absatz 3 des Brandenburgischen Weiterbildungsgesetzes und unterbreiten dem Landkreis oder der kreisfreien Stadt gemäß § 10 Absatz 3 Nummer 5 des Brandenburgischen Weiterbildungsgesetzes einen Vorschlag zur Verteilung der Mittel zur Förderung der Grundversorgung.

(3) Der Landkreis oder die kreisfreie Stadt prüft den Vorschlag des regionalen Weiterbildungsbeirats und entscheidet über die Anteile der einzelnen anerkannten Weiterbildungseinrichtungen am Umfang der Grundversorgung und teilt diese Entscheidung dem regionalen Weiterbildungsbeirat bis spätestens 15. Dezember vor Beginn des Förderzeitraums mit.
Davon unberührt bleibt das Erfordernis, den jeweiligen Antrag gemäß Absatz 1 gesondert zu bescheiden.

(4) Wenn im folgenden Haushaltsjahr ein Inhaltsbereich gemäß § 1 Absatz 2 weniger als 1 Prozent der Unterrichtsstunden der geplanten Grundversorgung für den Landkreis beziehungsweise die kreisfreie Stadt umfasst, ist dies von dem Landkreis oder der kreisfreien Stadt bis zum 31. Dezember des laufenden Jahres gegenüber dem für Bildung zuständigen Ministerium schriftlich zu begründen.

#### § 4 Gestaltung der Grundversorgung

(1) Die Weiterbildungsangebote sollen in organisierter Form und nach erwachsenengemäßen didaktischen Prinzipien von geeigneten Mitarbeiterinnen oder Mitarbeitern der anerkannten Weiterbildungseinrichtungen in eigener pädagogischer Verantwortung zum Zweck der Grundversorgung geplant und durchgeführt werden.
Sie müssen veröffentlicht und frei zugänglich sein.

(2) Als Berechnungsgrundlage für eine Unterrichtsstunde dient die Zeiteinheit von 45 Minuten.
Abweichungen sind entsprechend umzurechnen.

(3) Unterrichtsstunden, die als Bestandteil eines organisierten Weiterbildungsangebots in Form des Blended Learnings als Verbindung von Online-Präsenzunterricht und ortsgebundenem Präsenzunterricht angeboten werden, können bei der Förderung der Grundversorgung berücksichtigt werden.
Dabei muss der Anteil des ortsgebundenen Präsenzunterrichts bei mindestens 25 Prozent der Unterrichtsstunden der Gesamtveranstaltung liegen.
Anteile des Selbstlernens sind nicht förderfähig.
Mit der Antragstellung ist der Umfang der Unterrichtsstunden gemäß Satz 1 darzulegen.

### Abschnitt 2  
Förderung

#### § 5 Förderung der Grundversorgung

Das Land fördert die von den Landkreisen und kreisfreien Städten für ihr Gebiet festgelegte Grundversorgung bis zu einer Höhe von 2 400 Unterrichtsstunden je 40 000 Einwohnerinnen und Einwohner (Grundversorgungsschlüssel).
Voraussetzungen, Höhe und Bemessungsgrundlagen der Förderung werden gemäß § 29 des Brandenburgischen Weiterbildungsgesetzes in Richtlinien geregelt.

#### § 6 Förderung von Veranstaltungen der Heimbildungsstätten

(1) Veranstaltungen von Heimbildungsstätten gemäß § 24 des Brandenburgischen Weiterbildungsgesetzes können gefördert werden.
Nicht berücksichtigt werden Veranstaltungen der beruflichen Weiterbildung gemäß § 1 Absatz 3 des Brandenburgischen Weiterbildungsgesetzes sowie Veranstaltungen der beruflichen Weiterbildung in sonstiger öffentlicher Förderung oder Finanzierung.
Voraussetzungen der Förderung sind die Gleichstellung der Heimbildungsstätte mit einer anerkannten Landesorganisation und der Nachweis der Organisation und Durchführung von anerkannten Veranstaltungen zur Bildungsfreistellung.
Eine Doppelförderung der Personal- und Honorarkosten ist ausgeschlossen.

(2) Die Förderung wird als pauschaler Zuschuss zu den Personalkosten für das im Aufgabenbereich der Bildungsfreistellung hauptberuflich tätige Personal für die Organisation und Durchführung von anerkannten Veranstaltungen zur Bildungsfreistellung im Umfang von mindestens 40 Veranstaltungstagen je Haushaltsjahr gewährt.
Der Zuschuss beträgt für das pädagogische Personal oder die Geschäftsführung und für das Verwaltungspersonal insgesamt bis zu 50 000 Euro jährlich als Grundförderung.

(3) Werden je Haushaltsjahr 60 und mehr Veranstaltungstage organisiert und durchgeführt, erhöht sich die Förderung in folgenden Stufen:

1.  ab 60 Veranstaltungstagen bis zu 75 000 Euro,
2.  ab 80 Veranstaltungstagen bis zu 100 000 Euro,
3.  ab 100 Veranstaltungstagen bis zu 125 000 Euro.

(4) Im Rahmen der stufenförmigen Förderung oberhalb von 40 Veranstaltungstagen werden nur Veranstaltungen der berufsübergreifenden, kulturellen und politischen Weiterbildung berücksichtigt.
Der pauschale Zuschuss kann oberhalb der Grundförderung von bis zu 50 000 Euro auch für in der Lehre tätige Honorarkräfte eingesetzt werden.

(5) Die erstmalige Förderung einer Heimbildungsstätte setzt eine mindestens einjährige kontinuierliche Tätigkeit im Bereich der Bildungsfreistellung nach Gleichstellung mit einer anerkannten Landesorganisation voraus.
Hiervon kann das für Bildung zuständige Ministerium Ausnahmen zulassen, wenn im Vorjahr der Förderung die Organisation und Durchführung von anerkannten Veranstaltungen zur Bildungsfreistellung im Umfang von mindestens 20 Veranstaltungstagen nachgewiesen wird.

(6) Für die Betreuung von Kindern bis zu sechs Jahren von freigestellten Personen während der Unterrichtszeiten der Bildungsfreistellungsmaßnahmen können Zuschüsse gemäß § 25 des Brandenburgischen Weiterbildungsgesetzes gewährt werden.
Der Zuschuss beträgt pro Stunde 30 Euro als Festbetrag und umfasst maximal 100 Kinderbetreuungstage je anerkannter Heimbildungsstätte.
Ein Kinderbetreuungstag umfasst einen Zeitraum von maximal 8 Stunden.

(7) Anträge auf Förderung sind spätestens zwei Wochen vor beabsichtigtem Beginn der Maßnahme beim für Bildung zuständigen Ministerium einzureichen.

#### § 7 Förderung von Modellvorhaben mit aktueller Schwerpunktsetzung

(1) Als Modellvorhaben mit aktueller Schwerpunktsetzung können Projekte gefördert werden, die der Qualitätsentwicklung oder der Auseinandersetzung mit anderen für die Entwicklung der Weiterbildung bedeutenden Themen dienen.
Inhalt, Form und Methode der Modellmaßnahme müssen geeignet sein, neue Konzeptionen oder Methoden in der Weiterbildung zu entwickeln und zu erproben oder bestehende zu überprüfen.
Das Vorhaben muss beispielhaft sein und zur Nachahmung anregen.

(2) Die Förderung wird als Zuschuss in Höhe von bis zu 80 Prozent der förderfähigen Personal- und Sachkosten gewährt.
Für Antragsteller, die nicht in kommunaler Trägerschaft sind, können unter Berücksichtigung der finanziellen Leistungsfähigkeit Zuschüsse von bis zu 90 Prozent der förderfähigen Personal- und Sachkosten gewährt werden.
Für Projekte in freier Trägerschaft, die sich an Zielgruppen richten, die nicht über ausreichend finanzielle Mittel zur Zahlung von Teilnahmegebühren verfügen, kann ebenso ein Zuschuss von bis zu 90 Prozent der förderfähigen Personal- und Sachkosten gewährt werden, wenn das Projekt im besonderen Interesse des zuständigen Ministeriums ist.

(3) Anträge auf Förderung sollen spätestens vier Wochen vor beabsichtigtem Beginn der Maßnahme beim für Bildung zuständigen Ministerium eingereicht werden.

#### § 8 Förderung von anerkannten Landesorganisationen der Weiterbildung

(1) Anerkannte Landesorganisationen der Weiterbildung können zum Zweck der Förderung und Koordination der Weiterbildungsarbeit ihrer Mitglieder gefördert werden.
Hierzu gehören insbesondere die Beratung der Mitglieder in pädagogischen, organisatorischen und finanziellen Fragen, die Förderung der Kooperation der Mitglieder, die Qualitätsentwicklung in den Mitgliedsorganisationen, Angebote der Fortbildung und die Wahrnehmung weiterbildungspolitischer Anliegen.

(2) Gefördert werden nur anerkannte Landesorganisationen, die durch die ihnen angeschlossenen Träger anerkannter Weiterbildungseinrichtungen in mindestens einem Drittel der Landkreise und kreisfreien Städte Weiterbildung organisieren und durchführen.

(3) Die erstmalige Förderung setzt eine mindestens zweijährige kontinuierliche Tätigkeit der Landesorganisation gemäß Absatz 1 nach ihrer Anerkennung voraus.
Hiervon kann das für Bildung zuständige Ministerium Ausnahmen zulassen, wenn mindestens zwei Drittel der Mitglieder der Landesorganisation anerkannte Weiterbildungseinrichtungen oder deren Träger sind und ein aussagekräftiges Konzept für die Tätigkeit gemäß Absatz 1 vorgelegt wird.

(4) Die Förderung wird als pauschaler Zuschuss zu den Personalkosten für das hauptberuflich tätige Personal sowie für Sachkosten gewährt.
Die Förderung bemisst sich nach sechs Stufen.
Diese setzen Mindestsummen im Vorjahr geleisteter Unterrichtsstunden im Sinne des Brandenburgischen Weiterbildungsgesetzes voraus, die von Mitgliedseinrichtungen mit Sitz und Tätigkeitsbereich im Land Brandenburg erbracht wurden.

(5) Die Förderung wird in folgenden Stufen gewährt:

1.  ab 5 000 Unterrichtsstunden bis zu 45 000 Euro,
2.  ab 10 000 Unterrichtsstunden bis zu 65 000 Euro,
3.  ab 40 000 Unterrichtsstunden bis zu 90 000 Euro,
4.  ab 80 000 Unterrichtsstunden bis zu 110 000 Euro,
5.  ab 120 000 Unterrichtsstunden bis zu 130 000 Euro,
6.  ab 160 000 Unterrichtsstunden bis zu 155 000 Euro.

(6) Wenn die angemessene Wahrnehmung der Aufgaben gemäß Absatz 1 im Antrag nicht dargestellt wird, kann die beantragte Förderung reduziert bewilligt werden.
Stellt sich im Verfahren der Prüfung des Nachweises der Verwendung heraus, dass die angemessene Wahrnehmung der Aufgaben gemäß Absatz 1 nicht realisiert wurde, kann die Förderung anteilig zurückgefordert werden.

(7) Eine Förderung gemäß Absatz 5 Nummer 3 bis 6 wird nur Landesorganisationen gewährt, die durch die ihnen angeschlossenen Träger anerkannter Weiterbildungseinrichtungen in über der Hälfte der Landkreise und kreisfreien Städte Weiterbildung organisieren und durchführen.

(8) Eine über den Förderschlüssel hinausgehende Förderung von Personal- und Sachkosten kann für zusätzliche Aktivitäten im besonderen Interesse des zuständigen Ministeriums beantragt werden, insbesondere für umfangreiche konzeptionelle Arbeiten oder die Durchführung von umfangreichen Fortbildungen oder Fachtagungen.

(9) Anträge auf Förderung sind spätestens zwei Wochen vor beabsichtigtem Beginn der Maßnahme beim für Bildung zuständigen Ministerium einzureichen.

#### § 9 Führen von Teilnehmerlisten

Der Nachweis der Erfüllung des Zuwendungszwecks erfolgt in der Grundversorgung gemäß § 5 durch vom für Bildung zuständigen Ministerium vorgegebene Teilnehmerlisten.
In die Teilnehmerlisten dürfen nur diejenigen personenbezogenen Daten aufgenommen werden, die für die Prüfung der zweckentsprechenden Mittelverwendung erforderlich sind.
Die vom für Bildung zuständigen Ministerium vorgegebenen Teilnehmerlisten gelten ebenso in der Förderung von Veranstaltungen der Heimbildungsstätten gemäß § 6 als Teilnachweis der Erfüllung des Zuwendungszwecks.
Bei Förderungen gemäß den §§ 7 und 8 sind Teilnehmerlisten als Teilnachweis der Erfüllung des Zuwendungszwecks bei Weiterbildungsveranstaltungen im Rahmen der Förderung zu führen.

### Abschnitt 3  
Sonstige Vorschriften

#### § 10 Zweckverbände

Die Bestimmungen dieser Verordnung gelten entsprechend für Zweckverbände.

#### § 11 Übergangsvorschrift

Für die Förderung im Haushaltsjahr 2019 gelten § 6 Absatz 4 Satz 1 und Absatz 7, § 7 Absatz 3 sowie § 8 Absatz 9 nicht.

#### § 12 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1. Januar 2019 in Kraft.
Gleichzeitig tritt die Weiterbildungsverordnung vom 9. Dezember 2015 (GVBl.
II Nr.
61) außer Kraft.

Potsdam, den 25.
Juni 2019

Die Ministerin für Bildung,  
Jugend und Sport

In Vertretung

Dr.
Thomas Drescher