## Ordnungsbehördliche Verordnung über den Betrieb von Fluglaternen (Fluglaternenverordnung - FluglatV)

Auf Grund des § 25 Absatz 1 in Verbindung mit § 30 Absatz 1 des Ordnungsbehördengesetzes in der Fassung der Bekanntmachung vom 21.
August 1996 (GVBl.
I S. 266) verordnet der Minister des Innern nach Kenntnisnahme durch den Ausschuss für Inneres des Landtages:

#### § 1  
Geltungsbereich

Die nachfolgenden Regelungen gelten für das gesamte Gebiet des Landes Brandenburg.

#### § 2  
Verbot

Es ist verboten, unbemannte Ballone aufsteigen zu lassen, bei denen die Luft im Balloninneren mit festen, flüssigen oder gasförmigen Brennstoffen erwärmt wird (Fluglaternen).

#### § 3  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 30 Absatz 1 des Ordnungsbehördengesetzes handelt, wer vorsätzlich oder fahrlässig entgegen § 2 eine Fluglaterne aufsteigen lässt.

(2) Eine Ordnungswidrigkeit nach Absatz 1 kann mit einer Geldbuße von bis zu 5 000 Euro geahndet werden.

#### § 4  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 2.
Februar 2010

Der Minister des Innern

Rainer Speer