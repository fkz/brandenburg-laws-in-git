## Verordnung über das Naturschutzgebiet „Lietzener Mühlental“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S.
2542) in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nr.
43) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1   
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Märkisch-Oderland wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Lietzener Mühlental“.

### § 2   
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 132 Hektar.
Es umfasst Flächen in folgenden Fluren:

Gemeinde:

Gemarkung:

Flur:

Lietzen

Lietzen

4, 5, 6;

Falkenhagen (Mark)

Falkenhagen

2.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 3 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 4 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Märkisch-Oderland, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

### § 3   
Schutzzweck

(1) Schutzzweck des im Osten der Lebuser Platte lokalisierten Naturschutzgebietes, das einen Ausschnitt einer subglazialen Schmelzwasserrinne mit dem Platkower Mühlenfließ, durchflossene Stillgewässer, begleitende Feuchtlebensräume sowie Grünland- und Waldflächen auf den Talhängen umfasst, ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Quellfluren, der Schwimmblatt- und Tauchfluren, der Röhrichte und Riede, der eutrophen Niedermoore, der feuchten und trockenwarmen Staudenfluren, der Trocken- und Halbtrockenrasen, Frisch- und Feuchtwiesen mit ihren Brachestadien sowie der naturnahen Wälder und Gebüsche wie Erlenbruchwälder, mesophile Eichen-Hainbuchen- und bodensaure Eichen-Mischwälder und der Weidengebüsche feuchter und Dornstrauch-gebüsche mittlerer bis trockenwarmer Standorte;
    
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Bologneser Glockenblume (Campanula bononiensis), Kartäuser-Nelke (Dianthus carthusianorum), Ähriger Blauweiderich (Veronica spicata), Ästige Graslilie (Anthericum ramosum), Gemeine Grasnelke (Armeria maritima ssp.
    elongata), Schlüsselblume (Primula veris), Breitblättrige Sitter (Epipactis helleborine), Gemeines Tausendgüldenkraut (Centaurium erythraea), Gelbe Teichrose (Nuphar lutea), Weiße Seerose (Nymphaea alba) und Wasser-Schwertlilie (Iris pseudacorus);
    
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederaus-breitungszentrum wild lebender Tierarten, insbesondere der Säugetiere, Vögel, Amphibien, Reptilien und Fische, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Wasserfledermaus (Myotis daubentonii), Großer Abendsegler (Nyctalus noctula), Rauhautfledermaus (Pipistrellus nathusii), Wasserspitzmaus (Neomys fodiens), Zwergtaucher (Tachybaptus ruficollis), Reiherente (Aythya fuligula), Tafelente (Aythya ferina), Graugans (Anser anser), Seeadler (Haliaeetus albicilla), Fischadler (Pandion haliaetus), Rebhuhn (Perdix perdix), Rohrweihe (Circus aeruginosus), Rotmilan (Milvus milvus), Kiebitz (Vanellus vanellus), Bekassine (Gallinago gallinago), Kranich (Grus grus), Eisvogel (Alcedo atthis), Braunkehlchen (Saxicola rubetra), Dros­selrohrsänger (Acrocephalus arundinaceus), Schilfrohr-sänger (Acrocephalus schoenobaenus), Wechselkröte (Bufo viridis), Laubfrosch (Hyla arborea), Moorfrosch (Rana arvalis), Zauneidechse (Lacerta agilis), Ringelnatter (Natrix natrix) sowie Moderlieschen (Leucaspius delineatus) und Schmerle (Neomacheilus barbatulus);
    
4.  die Erhaltung des Gebietes zur Umweltbeobachtung und wissenschaftlichen Untersuchung ökologischer Zusammenhänge;
    
5.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit der Landschaft der eiszeitlichen Rinne, die durch vegetationsreiche Stillgewässer, naturnahe Röhrichte, Riede und Feuchtwälder sowie weitläufige artenreiche Grünlandflächen und strukturreiche Laubmischwälder der Hang- und Kuppenbereiche gekennzeichnet ist;
    
6.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des regionalen Gewässer- und Feuchtgebiets-verbundes entlang des Platkower Mühlenfließes und seiner Zuläufe bis in das Odertal.
    

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Lietzener Mühlental“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes ) mit seinen Vorkommen von

1.  Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Flüssen der planaren Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Mageren Flachland-Mähwiesen (Alopecurus pratensis, Sanguisorba officinalis) sowie Subatlantischem oder mitteleuropäischem Stieleichenwald oder Hainbuchenwald (Carpinion betuli) \[Stellario-Carpinetum\] als Biotope von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG);
    
2.  Trockenen, kalkreichen Sandrasen, Subpannonischen Steppen-Trockenrasen, Auen-Wäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion) als prioritäre Biotope („prioritäre Lebensraumtypen“ im Sinne des Anhangs der Richtlinie 92/43/EWG);
    
3.  Fischotter (Lutra lutra), Elbe-Biber (Castor fiber albicus), Rotbauchunke (Bombina bombina), Kamm-Molch (Triturus cristatus), Steinbeißer (Cobitis taenia), Bitterling (Rhodeus sericeus amarus) und Schlammpeitzger (Misgurnus fossilis) als Tierarten von gemeinschaftlichem Interesse im Sinne des Anhangs II der Richtlinie 92/43/EWG, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.
    

### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
    
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
    
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
    
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
    
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
    
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
    
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
    
8.  die Ruhe der Natur durch Lärm zu stören;
    
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten von Waldbereichen außerhalb von Erlenbruchwäldern und Erlen-Eschenwäldern zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 11 jeweils nach dem 31.
    Juli eines jeden Jahres;
    
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
    
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
    
12.  außerhalb des Diecksees zu baden;
    
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen;
    
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
    
15.  Hunde frei laufen zu lassen;
    
16.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
    
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
    
18.  sonstige Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
    
19.  Tiere zu füttern oder Futter bereitzustellen;
    
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
    
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
    
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
    
23.  Pflanzenschutzmittel jeder Art anzuwenden;
    
24.  Wiesen, Weiden oder sonstiges Grünland umzubrechen oder neu anzusäen.
    

### § 5   
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  Grünland als Wiese oder Weide mit einer Besatzdichte von maximal 1,4 Raufutter verwertenden Großvieheinheiten (RGV) pro Hektar im Jahresmittel genutzt wird und § 4 Absatz 2 Nummer 17 gilt,
        
    2.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 weiter gilt, wobei bei Narbenschäden mit Genehmigung der unteren Naturschutzbehörde eine umbruchlose Nachsaat zulässig ist.
        Die Genehmigung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird;
        
    3.  auf Grünland bei Beweidung Bäume und Feldgehölze, die größer als 0,5 Hektar sind, in geeigneter Weise gegen Verbiss und sonstige Beschädigungen sowie Ränder von Gewässern wirksam gegen Trittschäden von weidenden Nutztieren geschützt werden;
        
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  nur Arten der jeweils potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Gehölzarten in gesellschaftstypischer Zusammensetzung unter Ausschluss eingebürgerter Arten zu verwenden sind,
        
    2.  auf den Flächen der in § 3 Absatz 2 Nummer 1 und 2 genannten Waldgesellschaften eine Nutzung nur einzelstamm- bis truppweise erfolgt und hydromorphe Böden sowie Böden mit einem hohen Anteil an fein-körnigem Substrat jeweils nur bei Frost oder in Trockenperioden auf dauerhaft festgelegten Rückegassen befahren werden,
        
    3.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
        
    4.  mindestens fünf Stämme je Hektar mit einem Durchmesser von mehr als 40 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum natürlichen Absterben und Zerfall aus der Nutzung genommen sein müssen,
        
    5.  je Hektar mindestens fünf Stück lebensraumtypische, abgestorbene, stehende Bäume (Totholz) mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden; liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimeter am stärkeren Ende) bleibt als ganzer Baum im Bestand,
        
    6.  § 4 Absatz 2 Nummer 23 gilt;
        
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung von Bibern und Fischottern weitgehend ausgeschlossen ist,
        
    2.  der Fischbesatz nur mit heimischen Arten erfolgt und dabei eine Gefährdung der in § 3 Absatz 2 Nummer 3 genannten Arten ausgeschlossen ist; § 13 der Fischereiordnung des Landes Brandenburg bleibt unberührt,
        
    3.  die Mahd von Schilf und anderen Uferröhrichten am Diecksee unterbleibt,
        
    4.  § 4 Absatz 2 Nummer 19 gilt;
        
4.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende Teichbewirtschaftung im Sinne der guten fachlichen Praxis gemäß den Leitlinien zur naturschutzgerechten Teichwirtschaft in Brandenburg vom 16.
    März 2011 auf den bisher rechtmäßig dafür genutzten Flächen, sofern der Schutzzweck nach § 3 Absatz 2 nicht gefährdet wird, mit der Maßgabe, dass Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters und des Bibers weitgehend ausgeschlossen ist;
    
5.  im Bereich der fischereilich genutzten Teiche kann die zuständige Naturschutzbehörde Maßnahmen zur Vergrämung und Tötung von Kormoranen genehmigen, sofern hierfür die erforderliche artenschutzrechtliche Ausnahmegenehmigung oder Befreiung vorliegt.
    Die Genehmigung kann mit Auflagen versehen werden.
    Sie ist zu erteilen, wenn der Schutzzweck von der Maßnahme nicht wesentlich beeinträchtigt wird;
    
6.  die rechtmäßige Ausübung der Angelfischerei am Diecksee mit der Maßgabe, dass
    
    1.  das Angeln nur vom Ufer aus zulässig ist,
        
    2.  das Betreten von Röhrrichten und Verlandungszonen mit Ausnahme der in Nummer 7 Buchstabe c genannten Flächen unzulässig ist;
        
7.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa) die Jagd auf Wasservögel vom 1.
        November bis zum 31.
        Dezember eines jeden Jahres zulässig ist,
        
        bb) die Fallenjagd nur mit Lebendfallen erfolgt und bis zu einem Abstand von 100 Metern zu Gewässerufern verboten ist.
        Ausnahmen von der Einhaltung dieses Abstandes kann die untere Naturschutzbehörde erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
        cc) keine Baujagd in einem Abstand von 100 Metern zum Ufer der Fließ- und Stillgewässer vorgenommen wird,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Errichtung anzuzeigen.
        Die untere Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        Die Entscheidung hierzu soll unverzüglich erfolgen,
        
    3.  die Mahd von jährlich maximal fünf Schuss-Schneisen von 50 Quadratmeter Fläche in Schilfbeständen für die Schwarzwildbejagung jeweils ab dem 1.
        August eines jeden Jahres,
        
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und der in § 3 Absatz 2 genannten Mageren Flachlandmähwiesen.
        
    
    Ablenkfütterungen und die Anlage von Ansaatwildwiesen sowie die Anlage und Unterhaltung von Wildäckern sind unzulässig.
    Im Übrigen bleiben Regelungen nach § 41 des Brandenburgischen Jagdgesetzes unberührt;
    
8.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter Nummer 10 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
    
9.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
    
10.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
    
11.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch jeweils nach dem 31.
    Juli eines jeden Jahres;
    
12.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
    
13.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung nach Anzeige gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes bei der unteren Naturschutzbehörde;
    
14.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
    
15.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.
    Ri) vom 24.
    Juli 2007 (ABl.
    S.
    1734), die durch die Bekanntmachung vom 1.
    Oktober 2013 (ABl.
    S.
    2811) geändert worden ist, an Straßen und Wegen freigestellt;
    
16.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

### § 6   
Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  das Platkower Mühlenfließ soll in begradigten Abschnitten renaturiert und seine ökologische Durchgängigkeit wiederhergestellt werden;
    
2.  zum Schutz von Röhrichtbrütern, Watvögeln, Amphibien, wassergebundenen Wirbellosenarten sowie der Gewässervegetation soll bei der Bewirtschaftung des Mühlensees eine frühzeitige Bespannung möglichst bis zum 15.
    März eines jeden Jahres, eine durchgehende Aufrechterhaltung der Bespannung bis zum 31.
    August eines jeden Jahres sowie ein langsames Ablassen mit einer Streckung über einen Zeitraum von mindestens 50 Tagen erfolgen;
    
3.  während des Einstaus des Mühlensees soll ein ökologischer Mindestwasserabfluss zum Unterlauf des Platkower Mühlenfließes von 8 Litern pro Sekunde gewährleistet werden;
    
4.  Trocken- und Halbtrockenrasen sollen vorzugsweise als Weide mit Schafen und Ziegen genutzt werden; die Beweidung soll entsprechend eines mit der zuständigen Naturschutzbehörde abgestimmten und regelmäßig fortzuschreibenden Weideplanes durchgeführt werden;
    
5.  aufgelassene Grünlandflächen mit Restvorkommen artenreicher Grünlandgesellschaften sollen unter Beachtung der Maßgaben von § 5 Absatz 1 Nummer 1 einer regelmäßigen extensiven Nutzung zugeführt werden; dabei können auch lokale Entbuschungsmaßnahmen notwendig werden;
    
6.  vollständig mit Gebüschen und Gehölzen bewachsene ehemalige Grünlandflächen sollen zu naturnahen Waldlebensraumtypen entwickelt werden;
    
7.  Auen- und Bruchwälder sollen aus der forstwirtschaftlichen Nutzung genommen werden;
    
8.  nicht heimische Gehölzarten, wie zum Beispiel Robinie (Robinia pseudacacia), Späte Traubenkirsche (Prunus serotina), Rosskastanie (Aesculus hippocastanum), Rot-Eiche (Quercus rubra), Hybrid- und Balsam-Pappel (Populus x canadensis, P.
    balsamifera) sowie Gemeine Fiche (Picea abies), Douglasie (Pseudotsuga menziesii) und andere Nadelhölzer sollen bei der forstwirtschaftlichen Flächennutzung aus dem Bestand entnommen werden;
    
9.  es sollen strukturreiche Waldmäntel aus standortgerechten, gebietsheimischen Bäumen und Sträuchern und artenreiche Krautsäume erhalten und entwickelt werden;
    
10.  es sollen geeignete Einrichtungen zur Besucherlenkung und -information geschaffen werden.
    

### § 7  
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

### § 8   
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

### § 9   
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

### § 10   
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Form-vorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

### § 11   
Inkrafttreten

§ 5 Absatz 1 Nummer 1 Buchstabe a tritt am 1.
Januar 2015 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 10.
Juni 2014

Die Ministerin für Umwelt, Gesundheit und Verbraucherschutz

Anita Tack

Anlage 1
--------

(zu § 2 Absatz 1)

![Das Naturschutzgebiet Lietzener Mühlental im Landkreis Märkisch-Oderland liegt im Bereich der Gemeinde Lietzen, Gemarkung Lietzen, Flure 4, 5 und 6 sowie der Gemeinde Falkenhagen (Mark), Gemarkung Falkenhagen, Flur 2.](/br2/sixcms/media.php/69/Nr.%200001.gif "Das Naturschutzgebiet Lietzener Mühlental im Landkreis Märkisch-Oderland liegt im Bereich der Gemeinde Lietzen, Gemarkung Lietzen, Flure 4, 5 und 6 sowie der Gemeinde Falkenhagen (Mark), Gemarkung Falkenhagen, Flur 2.")

Anlage 2
--------

(zu § 2 Absatz 2)

**1.
Topografische Karten im Maßstab 1 : 10 000**

**Titel:**       Topografische Karte zur Verordnung über das Naturschutzgebiet „Lietzener Mühlental“

Blattnummer

Unterzeichnung

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV), am 11.
April 2014

2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 11.
April 2014

3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 11.
April 2014

**2.
Liegenschaftskarten im Maßstab 1 : 2 500**

**Titel:**       Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Lietzener Mühlental“

Blattnummer

Gemarkung

Flur

Unterzeichnung

1

Lietzen

4, 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 11.
April 2014

2

Lietzen

4, 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 11.
April 2014

3

Falkenhagen

Lietzen

2

4, 5, 6

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 11.
April 2014

4

Falkenhagen

2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 11.
April 2014

Anlage 3
--------

(zu § 2 Absatz 2)

**Flurstücksliste zur Verordnung über das Naturschutzgebiet „Lietzener Mühlental“**

Landkreis: Märkisch-Oderland

Gemeinde

Gemarkung

Flur

Flurstück

Falkenhagen (Mark)

Falkenhagen

2

69 bis 78, 79/1, 80, 81, 82, 126, 127, 128 (anteilig), 132 (anteilig), 166 (anteilig), 172, 173, 174 (anteilig), 180, 181, 183 (anteilig), 184 (anteilig), 187 (anteilig), 188 (anteilig), 468 bis 503, 504 (anteilig), 505, 506, 507 (anteilig), 508;

Lietzen

Lietzen

4

195/1, 196, 197/1, 197/6 (anteilig), 198 (anteilig), 199/2, 201/1, 201/2, 203/1, 204/1, 204/2, 213/1, 287 (anteilig), 289, 290, 291, 294, 295;

Lietzen

Lietzen

5

63/1 (anteilig), 64 (anteilig), 65/1, 65/2 (anteilig), 66/1, 66/2, 67/1, 67/2, 68/1, 68/2 (anteilig), 69/1, 69/2 (anteilig), 70/1, 70/2 (anteilig), 71/1, 71/2, 72/1, 72/2, 73/1, 73/2, 74/1, 74/2, 75 (anteilig), 76 (anteilig), 77 (anteilig), 78 (anteilig), 200 (anteilig), 203 (anteilig);

Lietzen

Lietzen

6

1 (anteilig), 2, 3 (anteilig), 5 (anteilig), 6 bis 13.