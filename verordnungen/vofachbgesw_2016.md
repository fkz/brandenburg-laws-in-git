## Verordnung zur Bestimmung der Zuständigkeiten für Fachberufe im Gesundheitswesen

#### § 1 

(1) Das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit ist zuständige Behörde für die Durchführung der nachstehenden Gesetze und Verordnungen in der jeweils geltenden Fassung, soweit Landesrecht, insbesondere Hochschulrecht, nichts Abweichendes bestimmt:

1.  Masseur- und Physiotherapeutengesetz vom 26.
    Mai 1994 (BGBl.
    I S. 1084), das zuletzt durch Artikel 45 des Gesetzes vom 6.
    Dezember 2011 (BGBl.
    I S. 2515, 2544) geändert worden ist,
2.  MTA-Gesetz vom 2.
    August 1993 (BGBl.
    I S. 1402), das zuletzt durch Artikel 41 des Gesetzes vom 6.
    Dezember 2011 (BGBl.
    I S. 2515, 2542) geändert worden ist,
3.  Diätassistentengesetz vom 8.
    März 1994 (BGBl.
    I S. 446), das zuletzt durch Artikel 48 des Gesetzes vom 6. Dezember 2011 (BGBl.
    I S. 2515, 2545) geändert worden ist,
4.  Ergotherapeutengesetz vom 25.
    Mai 1976 (BGBl.
    I S. 1246), das zuletzt durch Artikel 50 des Gesetzes vom 6. Dezember 2011 (BGBl.
    I S. 2515, 2546) geändert worden ist,
5.  Gesetz über den Beruf des Logopäden vom 7.
    Mai 1980 (BGBl.
    I S. 529), das zuletzt durch Artikel 52 des Gesetzes vom 6.
    Dezember 2011 (BGBl.
    I S. 2515, 2547) geändert worden ist,
6.  Krankenpflegegesetz vom 16.
    Juli 2003 (BGBl.
    I S. 1442), das zuletzt durch Artikel 35 des Gesetzes vom 6. Dezember 2011 (BGBl.
    I S. 2515, 2537) geändert worden ist,
7.  Hebammengesetz vom 22.
    November 2019 (BGBl.
    I S.
    1759), das durch Artikel 10 des Gesetzes vom 24.
    Februar 2021 (BGBl.
    I S.
    274) geändert worden ist,
8.  Rettungsassistentengesetz vom 10.
    Juli 1989 (BGBl.
    I S. 1384), das zuletzt durch Artikel 19 des Gesetzes vom 2.
    Dezember 2007 (BGBl.
    S. 2686, 2722) geändert worden ist,
9.  Orthoptistengesetz vom 28.
    November 1989 (BGBl.
    I S. 2061), das zuletzt durch Artikel 54 des Gesetzes vom 6.
    Dezember 2011 (BGBl.
    I S. 2515, 2548) geändert worden ist,
10.  Podologengesetz vom 4.
    Dezember 2001 (BGBl.
    I S. 3320), das zuletzt durch Artikel 56 des Gesetzes vom 6. Dezember 2011 (BGBl.
    I S. 2515, 2549) geändert worden ist,
11.  Gesetz über den Beruf des pharmazeutisch-technischen Assistenten in der Fassung der Bekanntmachung vom 23.
    September 1997 (BGBl.
    I S. 2349), das zuletzt durch Artikel 43 des Gesetzes vom 6.
    Dezember 2011 (BGBl.
    I S. 2515, 2543), geändert worden ist,
12.  Notfallsanitätergesetz vom 22.
    Mai 2013 (BGBl.
    I S. 1348),
13.  Anästhesietechnische- und Operationstechnische-Assistenten-Gesetz vom 14. Dezember 2019 (BGBI.
    I S.
    2768), das durch Artikel 11 des Gesetzes vom 24. Februar 2021 (BGBl. I S. 274, 293) geändert worden ist,
14.  Ausbildungs- und Prüfungsverordnung für Physiotherapeuten vom 6.
    Dezember 1994 (BGBl. I S. 3786), die zuletzt durch Artikel 13 der Verordnung vom 2.
    August 2013 (BGBl.
    I S. 3005, 3066) geändert worden ist,
15.  Ausbildungs- und Prüfungsverordnung für Masseure und medizinische Bademeister vom 6. Dezember 1994 (BGBl.
    I S. 3770), die zuletzt durch Artikel 12 der Verordnung vom 2. August 2013 (BGBl.
    I S. 3005, 3062) geändert worden ist,
16.  Ausbildungs- und Prüfungsverordnung für technische Assistenten in der Medizin vom 25. April 1994 (BGBl.
    S. 922), die zuletzt durch Artikel 10 der Verordnung vom 2. August 2013 (BGBl.
    I S. 3005, 3054) geändert worden ist,
17.  Ausbildungs- und Prüfungsverordnung für Diätassistentinnen und Diätassistenten vom 1. August 1994 (BGBl.
    I S. 2088), die zuletzt durch Artikel 11 der Verordnung vom 2. August 2013 (BGBl.
    I S. 3005, 3058) geändert worden ist,
18.  Ergotherapeuten-Ausbildungs- und Prüfungsverordnung vom 2.
    August 1999 (BGBl.
    I S. 1731), die zuletzt durch Artikel 7 der Verordnung vom 2.
    August 2013 (BGBl.
    I S. 3005, 3042) geändert worden ist,
19.  Ausbildungs- und Prüfungsordnung für Logopäden vom 1.
    Oktober 1980 (BGBl.
    I S. 1892), die zuletzt durch Artikel 8 der Verordnung vom 2.
    August 2013 (BGBl.
    I S. 3005, 3046) geändert worden ist,
20.  Ausbildungs- und Prüfungsverordnung für die Berufe in der Krankenpflege vom 10. November 2003 (BGBl.
    S. 2263), die zuletzt durch Artikel 15 der Verordnung vom 2. August 2013 (BGBl.
    I S. 3005, 3074) geändert worden ist,
21.  Studien- und Prüfungsverordnung für Hebammen vom 8.
    Januar 2020 (BGBl. I S. 39),
22.  Ausbildungs- und Prüfungsverordnung für Rettungsassistentinnen und Rettungsassistenten vom 7.
    November 1989 (BGBl.
    S. 1966), die zuletzt durch Artikel 20 des Gesetzes vom 2. Dezember 2007 (BGBl.
    S. 2686, 2725) geändert worden ist,
23.  Ausbildungs- und Prüfungsverordnung für Orthoptistinnen und Orthoptisten vom 21. März 1990 (BGBl.
    I S. 563), die zuletzt durch Artikel 9 der Verordnung vom 2.
    August 2013 (BGBl. I S. 3005, 3050) geändert worden ist,
24.  Ausbildungs- und Prüfungsverordnung für Podologinnen und Podologen vom 18. Dezember 2001 (BGBl. 2002 I S. 12), die zuletzt durch Artikel 14 der Verordnung vom 2. August 2013 (BGBl.
    I S. 3005, 3070) geändert worden ist,
25.  Ausbildungs- und Prüfungsverordnung für pharmazeutisch-technische Assistentinnen und pharmazeutisch-technische Assistenten vom 23.
    September 1997 (BGBl.
    I S. 2352), die zuletzt durch Artikel 6 der Verordnung vom 2.
    August 2013 (BGBl.
    I S. 3005, 3038) geändert worden ist,
26.  Ausbildungs- und Prüfungsverordnung für Notfallsanitäterinnen und Notfallsanitäter vom 16. Dezember 2013 (BGBl.
    I S. 4280),
27.  Anästhesietechnische- und Operationstechnische-Assistenten-Ausbildungs- und -Prüfungsverordnung vom 4.
    November 2020 (BGBl.
    I S.
    2295).

Das Landesamt für Soziales und Versorgung ist zuständige Behörde für die Durchführung des Altenpflegegesetzes in der Fassung der Bekanntmachung vom 25.
August 2003 (BGBl.
I S. 1690), das zuletzt durch Artikel 1 des Gesetzes vom 13.
März 2013 (BGBl.
I S. 446) geändert worden ist, in der jeweils geltenden Fassung und der Altenpflege-Ausbildungs- und Prüfungsverordnung vom 26. November 2002 (BGBl.
I S. 4418, 4429), die zuletzt durch Artikel 38 des Gesetzes vom 6. Dezember 2011 (BGBl.
I S. 2515, 2540) geändert worden ist, in der jeweils geltenden Fassung.

(2) Dem Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit obliegt ferner die Verfolgung und Ahndung von Ordnungswidrigkeiten im Rahmen der Zuständigkeiten nach Absatz 1 Satz 1.
Dem Landesamt für Soziales und Versorgung obliegt ferner die Verfolgung und Ahndung von Ordnungswidrigkeiten im Rahmen der Zuständigkeiten nach Absatz 1 Satz 2.

#### § 2 

Das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit ist zuständige Behörde für die Durchführung der in dem Gesetz über die Weiterbildung und Fortbildung in den Fachberufen des Gesundheitswesens vom 18. März 1994 (GVBl.
I S. 62), das zuletzt durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 38 S. 3) geändert worden ist, in der jeweils geltenden Fassung genannten Aufgaben einschließlich der Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 13 Absatz 1 Nummer 1 des Gesetzes über die Weiterbildung und Fortbildung in den Fachberufen des Gesundheitswesens.
Entscheidungen im Rahmen der Durchführung des Gesetzes über die Weiterbildung und Fortbildung in den Fachberufen des Gesundheitswesens in Verbindung mit der Gerontopsychiatrische Fachkraft-Weiterbildungsverordnung vom 8.
Februar 2004 (GVBl.
II S.
125) in der jeweils geltenden Fassung erfolgen durch das Landesamt für Soziales und Versorgung.