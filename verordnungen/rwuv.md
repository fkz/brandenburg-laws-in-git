## Verordnung über Religionsunterricht und Weltanschauungsunterricht an Schulen  (Religions- und Weltanschauungsunterrichtsverordnung - RWUV)

Auf Grund des § 9 Absatz 6 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
August 2002 (GVBl.
I S. 78) verordnet die Ministerin für Bildung, Jugend und Sport im Benehmen mit dem für Schule zuständigen Ausschuss des Landtages:

Abschnitt 1  
Religionsunterricht
---------------------------------

§ 1  
**Allgemeines zum Religionsunterricht**

(1) Religionsunterricht kann in den Schulen im Land Brandenburg unter Einhaltung der Bestimmungen des § 9 Absatz 2 bis 5 des Brandenburgischen Schulgesetzes in allen Schulformen und -stufen erteilt werden.
Der Religionsunterricht erfolgt nach den Grundsätzen der Kirchen oder Religionsgemeinschaften.

(2) Kirchen, Religionsgemeinschaften und Schulen sowie Schulbehörden arbeiten bei der Durchführung des Religionsunterrichts sowie allen hiermit im Zusammenhang stehenden Fragen zusammen.
Sie informieren sich gegenseitig, soweit sie Informationen über den Religionsunterricht geben oder Entscheidungen treffen, die auf diesen Auswirkungen haben.

§ 2  
**Religionsunterricht**

(1) Der Religionsunterricht wird durch Personen erteilt, die von den Kirchen oder Religionsgemeinschaften bevollmächtigt und beauftragt wurden (Lehrkräfte der Kirchen oder Religionsgemeinschaften) sowie über eine hinreichende Ausbildung verfügen.

(2) Der Religionsunterricht wird nach curricularen Vorgaben der Kirchen oder der Religionsgemeinschaften erteilt, die denen der staatlichen Rahmenlehrpläne gleichwertig sind.
Diese enthalten

1.  allgemeine und fachliche Ziele,
2.  didaktische Grundsätze und
3.  Empfehlungen zu Formen der Leistungsbewertung, die sich an den allgemeinen und fachlichen Zielen orientieren.

§ 3  
**Information der Eltern sowie der Schülerinnen und Schüler über den Religionsunterricht**

Für die Information gemäß § 9 Absatz 2 Satz 3 des Brandenburgischen Schulgesetzes können in Abstimmung mit der Schulleitung Aushänge an der Anschlagtafel der Schule erfolgen, gesonderte Veranstaltungen in der Schule stattfinden und schriftliches Material der Kirchen oder Religionsgemeinschaften ausgegeben werden.
Die Lehrkräfte der Kirchen oder Religionsgemeinschaften können sich den Schülerinnen und Schülern sowie den Eltern in der Schule persönlich vorstellen.

§ 4  
**Teilnahme am Religionsunterricht**

(1) Die Eltern oder religionsmündige Schülerinnen oder Schüler geben die Anmeldung zum Religionsunterricht oder deren Widerruf der Schule zur Weiterleitung an die Kirche oder Religionsgemeinschaft.
Anmeldung und Widerruf sind rechtzeitig vor Ende des Unterrichts im Schulhalbjahr zum nächsten Schulhalbjahr schriftlich zu erklären.
Die Anmeldung gilt auch nach einem Schulwechsel sowie bei Eintritt der Religionsmündigkeit fort.

(2) Die Kirchen oder Religionsgemeinschaften teilen der Schule die Aufnahme in eine Lerngruppe des Religionsunterrichts oder den Wechsel in eine andere Lerngruppe mit.
Sie stellen die für Anmeldung und Widerruf erforderlichen Formulare zur Verfügung.

§ 5  
**Leistungsbewertung im Religionsunterricht**

Die im Religionsunterricht erreichten Leistungen werden nach Maßgabe der Bestimmungen der Kirchen oder Religionsgemeinschaften entsprechend den Grundsätzen der Leistungsbewertung gemäß § 9 Absatz 4 Satz 1 in Verbindung mit § 57 des Brandenburgischen Schulgesetzes und den bildungsgangspezifischen Vorschriften bewertet.
Die Einhaltung dieser Bestimmungen ist durch die Kirchen oder Religionsgemeinschaften zu gewährleisten.
Die Leistungsbewertung wird auf Wunsch der Kirchen oder Religionsgemeinschaften in das Zeugnis gemäß § 58 des Brandenburgischen Schulgesetzes aufgenommen.

§ 6  
**Rechte der Schülerinnen, Schüler und Eltern, Aufsicht, Erziehungs- und Ordnungsmaßnahmen im Religionsunterricht**

(1) Die Kirchen oder Religionsgemeinschaften bestimmen die Rechte und Pflichten der Schülerinnen und Schüler sowie deren Eltern im Religionsunterricht unter Beachtung der Grundsätze gemäß den §§ 44 Absatz 2 bis 5, 46 und 47 Absatz 1 des Brandenburgischen Schulgesetzes.

(2) Es besteht Unfallversicherungsschutz für die Teilnahme am Religionsunterricht gemäß § 2 Absatz 1 Nummer 8 Buchstabe b des Siebten Buches Sozialgesetzbuch in Verbindung mit § 9 Absatz 2 des Brandenburgischen Schulgesetzes.
Dieser umfasst gemäß § 8 Absatz 2 Nummer 1 des Siebten Buches Sozialgesetzbuch in Verbindung mit  
§ 9 Absatz 2 des Brandenburgischen Schulgesetzes auch den Weg von der Wohnung oder Schule zum Religionsunterricht und zurück, wenn dieser außerhalb des Schulgeländes in Räumen der Kirchen oder Religionsgemeinschaften stattfindet.

(3) Für den Religionsunterricht gelten die datenschutzrechtlichen Bestimmungen der Kirchen oder Religionsgemeinschaften.
Bei deren Anwendung ist ein den Bestimmungen über den Datenschutz in der Schule gleichwertiger Datenschutz zu gewährleisten.
Im Übrigen gelten die sonstigen datenschutzrechtlichen Bestimmungen des Landes.

(4) Für die Aufsicht während des Religionsunterrichts sind Lehrkräfte der Kirchen oder Religionsgemeinschaften entsprechend den für den Schulunterricht geltenden Bestimmungen verantwortlich.
Für Lerngruppen, deren Religionsunterricht in der Schule stattfindet, liegt die Aufsicht für die Zeit vor und nach dem Religionsunterricht sowie bei dessen Ausfall bei der Schule.
Die Aufsicht für den Weg von der Schule zum Religionsunterricht in den Räumen der Kirchen oder Religionsgemeinschaften und zurück zur Schule sowie die Aufsicht in den Räumen der Kirchen oder Religionsgemeinschaften bei Ausfall des Religionsunterrichts obliegt den Kirchen oder Religionsgemeinschaften.

(5) Die für den Schulunterricht geltenden Bestimmungen über Konfliktschlichtung, Erziehungs- und Ordnungsmaßnahmen finden im Religionsunterricht Anwendung mit der Maßgabe, dass Ordnungsmaßnahmen durch die Schule in Abstimmung mit den Lehrkräften der Kirchen oder Religionsgemeinschaften erfolgen können.

§ 7  
**Gruppenbildung im Religionsunterricht**

(1) Der Religionsunterricht wird in Lerngruppen von in der Regel mindestens zwölf Schülerinnen und Schülern durchgeführt.
Die Bestimmungen der Verwaltungsvorschriften über die Unterrichtsorganisation zur Klassenbildung in Förderschulen und Förderklassen gelten entsprechend mit der Maßgabe, dass der untere Bandbreitenwert als Mindestgruppengröße gilt.
Über die Bildung von Lerngruppen in eigenen Räumen entscheiden die Kirchen oder Religionsgemeinschaften.

(2) Um diese Mindestgruppengröße zu erreichen, können klassenübergreifende oder jahrgangsstufenübergreifende Lerngruppen gebildet werden.
Jahrgangsstufenübergreifende Lerngruppen sollen im Hinblick auf die Gleichwertigkeit des Unterrichts und der Leistungsbewertung nicht mehr als zwei Jahrgangsstufen umfassen.
In der Primarstufe können in besonderen Fällen drei Jahrgangsstufen umfasst sein.
Wenn es zur Durchführung des Religionsunterrichts erforderlich ist, können schulübergreifende Lerngruppen gebildet werden.

(3) Wenn auch unter Anwendung der Möglichkeiten gemäß Absatz 2 die Mindestgruppengröße am ersten Unterrichtstag des Schulhalbjahres nicht erreicht ist, weil die regionalen Verhältnisse dies in besonderer Weise erschweren, kann die Mindestgröße um bis zur Hälfte unterschritten werden.
Die Möglichkeit der Bildung von Lerngruppen in den Räumen der Kirchen oder Religionsgemeinschaften bleibt unberührt.

(4) Die Entscheidung über die Lerngruppenbildung ist im Benehmen mit der Schule zu treffen und soll für wenigstens ein Schulhalbjahr gelten.

§ 8  
**Einordnung des Religionsunterrichts in den Schulbetrieb**

(1) Schule und Schulbehörden sind im Rahmen ihrer Möglichkeiten verantwortlich für die Einfügung des Religionsunterrichts in den geordneten Schulbetrieb.
Die Schule sieht im Einvernehmen mit den Kirchen oder Religionsgemeinschaften bis zu zwei Wochenstunden für den Religionsunterricht im Stundenplan vor.

(2) Bei der Gestaltung des Stundenplans sieht die Schule unter Nutzung aller schulorganisatorischen Möglichkeiten die Einordnung des in der Schule stattfindenden Religionsunterrichts in die regelmäßige Unterrichtszeit vor.
Der Religionsunterricht soll nicht nur in Randstunden erteilt werden.

(3) Der Stundenplan soll gemäß § 9 Absatz 3 Satz 3 des Brandenburgischen Schulgesetzes zulassen, dass der Besuch des Religionsunterrichts auch zusätzlich zur Teilnahme am Unterricht im Fach Lebensgestaltung-Ethik-Religionskunde möglich ist.

(4) Der Religionsunterricht kann in den für die staatlichen Unterrichtsfächer zulässigen Unterrichtsformen durchgeführt werden.

(5) Die Kirchen oder Religionsgemeinschaften können Religionsunterricht, der jahrgangsstufen- oder schulübergreifend stattfindet, in eigenen Räumen erteilen.
Die Entscheidung ist im Benehmen mit der Schule zu treffen und soll für wenigstens ein Schuljahr gelten.

(6) Die Kirchen oder Religionsgemeinschaften teilen der Schule spätestens zwei Wochen, bei erstmaliger Einrichtung des Religionsunterrichts an einer Schule spätestens vier Wochen nach Beginn des Unterrichts im jeweiligen Schuljahr die beabsichtigte Gruppenbildung und gegebenenfalls die Erteilung des Religionsunterrichts in Räumen der Schule mit.
Sind Schülerinnen oder Schüler mehrerer Schulen an einer Lerngruppe beteiligt, ist zwischen den Schulen und den mit der Erteilung des Religionsunterrichts beauftragten Lehrkräften oder den für den Religionsunterricht zuständigen Stellen der Kirchen oder Religionsgemeinschaften rechtzeitig eine Abstimmung über die zeitliche Festlegung für den Religionsunterricht herbeizuführen.
Das Verfahren wird von der Schulleitung der Schule koordiniert, der voraussichtlich die Mehrzahl der Schülerinnen oder Schüler der Lerngruppe angehört (Stammschule).

(7) Treten bei der Einordnung des Religionsunterrichts in den Schulbetrieb zwischen der Schule und den Lehrkräften der Kirchen oder Religionsgemeinschaften Probleme auf, vermittelt das zuständige staatliche Schulamt unter Einbeziehung der für den Religionsunterricht zuständigen Stellen der Kirchen oder Religionsgemeinschaften.

§ 9  
**Religionsunterricht in schulischen Räumen**

Findet der Religionsunterricht in der Schule statt, soll er bei der Raumverteilung mit den Fächern des staatlichen Unterrichts gleichbehandelt werden.

Abschnitt 2  
Weltanschauungsunterricht
---------------------------------------

§ 10  
**Allgemeines zum Weltanschauungsunterricht**

(1) Weltanschauungsunterricht kann in den Schulen im Land Brandenburg unter Einhaltung der Bestimmungen des § 9 Absatz 2 bis 5 und Absatz 8 des Brandenburgischen Schulgesetzes in allen Schulformen und -stufen erteilt werden.
Der Weltanschauungsunterricht erfolgt nach den Grundsätzen der Vereinigungen zur gemeinschaftlichen Pflege einer Weltanschauung.

(2) Vereinigungen zur gemeinschaftlichen Pflege einer Weltanschauung und Schulen sowie Schulbehörden arbeiten bei der Durchführung des Weltanschauungsunterrichts sowie allen hiermit im Zusammenhang stehenden Fragen zusammen.
Sie informieren sich gegenseitig, soweit sie Informationen über den Weltanschauungsunterricht geben oder Entscheidungen treffen, die auf diesen Auswirkungen haben.

§ 11  
**Weltanschauungsunterricht**

(1) Der Weltanschauungsunterricht wird durch Personen erteilt, die von den Vereinigungen zur gemeinschaftlichen Pflege einer Weltanschauung bevollmächtigt und beauftragt wurden (Lehrkräfte einer Vereinigung zur gemeinschaftlichen Pflege einer Weltanschauung) sowie über eine hinreichende Ausbildung verfügen.

(2) Der Weltanschauungsunterricht wird nach curricularen Vorgaben der Vereinigungen zur gemeinschaftlichen Pflege einer Weltanschauung erteilt, die denen der staatlichen Rahmenlehrpläne gleichwertig sind.
Diese enthalten

1.  allgemeine und fachliche Ziele,
2.  didaktische Grundsätze und
3.  Empfehlungen zu Formen der Leistungsbewertung, die sich an den allgemeinen und fachlichen Zielen orientieren.

§ 12  
**Information der Eltern sowie der Schülerinnen und Schüler über den Weltanschauungsunterricht**

Für die Information gemäß § 9 Absatz 2 Satz 3 des Brandenburgischen Schulgesetzes können in Abstimmung mit der Schulleitung Aushänge an der Anschlagtafel der Schule erfolgen, gesonderte Veranstaltungen in der Schule stattfinden und schriftliches Material der Vereinigungen zur gemeinschaftlichen Pflege einer Weltanschauung ausgegeben werden.
Die Lehrkräfte der Vereinigungen zur gemeinschaftlichen Pflege einer Weltanschauung können sich den Schülerinnen und Schülern sowie den Eltern in der Schule persönlich vorstellen.

§ 13  
**Teilnahme am Weltanschauungsunterricht**

(1) Zur Teilnahme am Weltanschauungsunterricht geben die Eltern die Anmeldung oder deren Widerruf der Schule zur Weiterleitung an die Vereinigungen zur Pflege einer Weltanschauung.
Bei Schülerinnen und Schülern, die das 14.
Lebensjahr vollendet haben, tritt die eigene Erklärung an die Stelle der Erklärung der Eltern.
Anmeldung und Widerruf sind rechtzeitig vor Ende des Unterrichts im Schulhalbjahr zum nächsten Schulhalbjahr schriftlich zu erklären.
Die Anmeldung gilt auch nach einem Schulwechsel oder bei Schülerinnen und Schülern, die das 14.
Lebensjahr vollendet haben.

(2) Die Vereinigungen zur Pflege einer Weltanschauung teilen der Schule die Aufnahme in eine Lerngruppe des Weltanschauungsunterrichts oder den Wechsel in eine andere Lerngruppe mit.
Sie stellen die für Anmeldung und Widerruf erforderlichen Formulare zur Verfügung.

§ 14  
**Leistungsbewertung im Weltanschauungsunterricht**

Die im Weltanschauungsunterricht erreichten Leistungen werden nach Maßgabe der Bestimmungen der Vereinigungen zur Pflege einer Weltanschauung entsprechend den Grundsätzen der Leistungsbewertung gemäß § 9 Absatz 4 Satz 1 in Verbindung mit § 57 des Brandenburgischen Schulgesetzes und den bildungsgangspezifischen Vorschriften bewertet.
Die Einhaltung dieser Bestimmungen ist durch die Vereinigungen zur Pflege einer Weltanschauung zu gewährleisten.
Die Leistungsbewertung wird auf Wunsch der Vereinigungen zur Pflege einer Weltanschauung in das Zeugnis gemäß § 58 des Brandenburgischen Schulgesetzes aufgenommen.

§ 15  
**Rechte der Schülerinnen, Schüler und Eltern, Aufsicht, Erziehungs- und Ordnungsmaßnahmen im Weltanschauungsunterricht**

(1) Die Vereinigungen zur Pflege einer Weltanschauung bestimmen die Rechte und Pflichten der Schülerinnen und Schüler sowie deren Eltern im Weltanschauungsunterricht unter Beachtung der Grundsätze gemäß den §§ 44 Absatz 2 bis 5, 46 und 47 Absatz 1 des Brandenburgischen Schulgesetzes.

(2) Es besteht Unfallversicherungsschutz für die Teilnahme am Weltanschauungsunterricht gemäß § 2 Absatz 1 Nummer 8 Buchstabe b des Siebten Buches Sozialgesetzbuch in Verbindung mit § 9 Absatz 2 des Brandenburgischen Schulgesetzes.
Dieser umfasst gemäß § 8 Absatz 2 Nummer 1 des Siebten Buches Sozialgesetzbuch in Verbindung mit § 9 Absatz 2 des Brandenburgischen Schulgesetzes auch den Weg von der Wohnung oder Schule zum Weltanschauungsunterricht und zurück, wenn dieser außerhalb des Schulgeländes in Räumen von Vereinigungen zur Pflege einer Weltanschauung stattfindet.

(3) Für den Weltanschauungsunterricht gelten die datenschutzrechtlichen Bestimmungen der Vereinigungen zur Pflege einer Weltanschauung.
Bei deren Anwendung ist ein den Bestimmungen über den Datenschutz in der Schule gleichwertiger Datenschutz zu gewährleisten.
Im Übrigen gelten die sonstigen datenschutzrechtlichen Bestimmungen des Landes.

(4) Für die Aufsicht während des Weltanschauungsunterrichts sind Lehrkräfte der Vereinigungen zur Pflege einer Weltanschauung entsprechend den für den Schulunterricht geltenden Bestimmungen verantwortlich.
Für Lern-gruppen, deren Weltanschauungsunterricht in der Schule stattfindet, liegt die Aufsicht für die Zeit vor und nach dem Weltanschauungsunterricht sowie bei dessen Ausfall bei der Schule.
Die Aufsicht für den Weg von der Schule zum Weltanschauungsunterricht in den Räumen der Vereinigungen zur Pflege einer Weltanschauung und zurück zur Schule sowie die Aufsicht in den Räumen der Vereinigungen zur Pflege einer Weltanschauung bei Ausfall des Weltanschauungsunterrichts obliegt den Vereinigungen zur Pflege einer Weltanschauung.

(5) Die für den Schulunterricht geltenden Bestimmungen über Konfliktschlichtung, Erziehungs- und Ordnungsmaßnahmen finden im Weltanschauungsunterricht Anwendung mit der Maßgabe, dass Ordnungsmaßnahmen durch die Schule in Abstimmung mit den Lehrkräften der Vereinigungen zur Pflege einer Weltanschauung erfolgen können.

§ 16  
**Gruppenbildung im Weltanschauungsunterricht**

(1) Der Weltanschauungsunterricht wird in Lerngruppen von in der Regel mindestens zwölf Schülerinnen und Schülern durchgeführt.
Die Bestimmungen der Verwaltungsvorschriften über die Unterrichtsorganisation zur Klassenbildung in Förderschulen und Förderklassen gelten entsprechend mit der Maßgabe, dass der untere Bandbreitenwert als Mindestgruppengröße gilt.
Über die Bildung von Lerngruppen in eigenen Räumen entscheiden die Vereinigungen zur Pflege einer Weltanschauung.

(2) Um diese Mindestgruppengröße zu erreichen, können klassenübergreifende oder jahrgangsstufenübergreifende Lerngruppen gebildet werden.
Jahrgangsstufenübergreifende Lerngruppen sollen im Hinblick auf die Gleichwertigkeit des Unterrichts und der Leistungsbewertung nicht mehr als zwei Jahrgangsstufen umfassen.
In der Primarstufe können in besonderen Fällen drei Jahrgangsstufen umfasst sein.
Wenn es zur Durchführung des Weltanschauungsunterrichts erforderlich ist, können schulübergreifende Lerngruppen gebildet werden.

(3) Wenn auch unter Anwendung der Möglichkeiten gemäß Absatz 2 die Mindestgruppengröße am ersten Unterrichtstag des Schulhalbjahres nicht erreicht ist, weil die regionalen Verhältnisse dies in besonderer Weise erschweren, kann die Mindestgröße um bis zur Hälfte unterschritten werden.
Die Möglichkeit der Bildung von Lerngruppen in den Räumen der Vereinigungen zur Pflege einer Weltanschauung bleibt unberührt.

(4) Die Entscheidung über die Lerngruppenbildung ist im Benehmen mit der Schule zu treffen und soll für wenigstens ein Schulhalbjahr gelten.

§ 17  
**Einordnung des Weltanschauungsunterrichts in den Schulbetrieb**

(1) Schule und Schulbehörden sind im Rahmen ihrer Möglichkeiten verantwortlich für die Einfügung des Weltanschauungsunterrichts in den geordneten Schulbetrieb.
Die Schule sieht im Einvernehmen mit den Vereinigungen zur Pflege einer Weltanschauung bis zu zwei Wochenstunden für den Weltanschauungsunterricht im Stundenplan vor.

(2) Bei der Gestaltung des Stundenplans sieht die Schule unter Nutzung aller schulorganisatorischen Möglichkeiten die Einordnung des in der Schule stattfindenden Weltanschauungsunterrichts in die regelmäßige Unterrichtszeit vor.
Der Weltanschauungsunterricht soll nicht nur in Randstunden erteilt werden.

(3) Der Stundenplan soll gemäß § 9 Absatz 3 Satz 3 des Brandenburgischen Schulgesetzes zulassen, dass der Besuch des Weltanschauungsunterrichts auch zusätzlich zur Teilnahme am Unterricht im Fach Lebensgestaltung-Ethik-Religionskunde möglich ist.

(4) Der Weltanschauungsunterricht kann in den für die staatlichen Unterrichtsfächer zulässigen Unterrichtsformen durchgeführt werden.

(5) Die Vereinigungen zur Pflege einer Weltanschauung können Weltanschauungsunterricht, der jahrgangsstufen- oder schulübergreifend stattfindet, in eigenen Räumen erteilen.
Die Entscheidung ist im Benehmen mit der Schule zu treffen und soll für wenigstens ein Schuljahr gelten.

(6) Die Vereinigungen zur Pflege einer Weltanschauung teilen der Schule spätestens zwei Wochen, bei erstmaliger Einrichtung des Weltanschauungsunterrichts an einer Schule spätestens vier Wochen nach Beginn des Unterrichts im jeweiligen Schuljahr die beabsichtigte Gruppenbildung und gegebenenfalls die Erteilung des Weltanschauungsunterrichts in Räumen der Schule mit.
Sind Schülerinnen oder Schüler mehrerer Schulen an einer Lerngruppe beteiligt, ist zwischen den Schulen und den mit der Erteilung des Weltanschauungsunterrichts beauftragten Lehrkräften oder den für den Weltanschauungsunterricht zuständigen Stellen der Vereinigungen zur Pflege einer Weltanschauung rechtzeitig eine Abstimmung über die zeitliche Festlegung für den Weltanschauungsunterricht herbeizuführen.
Das Verfahren wird von der Schulleitung der Schule koordiniert, der voraussichtlich die Mehrzahl der Schülerinnen oder Schüler der Lerngruppe angehört (Stammschule).

(7) Treten bei der Einordnung des Weltanschauungsunterrichts in den Schulbetrieb zwischen der Schule und den Lehrkräften der Vereinigungen zur Pflege einer Weltanschauung Probleme auf, vermittelt das zuständige staatliche Schulamt unter Einbeziehung der für den Weltanschauungsunterricht zuständigen Stellen der Vereinigungen zur Pflege einer Weltanschauung.

§ 18  
**Weltanschauungsunterricht in schulischen Räumen**

Findet der Weltanschauungsunterricht in der Schule statt, soll er bei der Raumverteilung mit den Fächern des staatlichen Unterrichts gleichbehandelt werden.

Abschnitt 3  
Schlussvorschrift
-------------------------------

§ 19  
**Inkrafttreten, Außerkrafttreten**

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2013 in Kraft.
Gleichzeitig tritt die Religionsunterrichtsverordnung vom 1.
August 2002 (GVBl.
II S. 481) außer Kraft.

Potsdam, den 29.
April 2013

Die Ministerin für Bildung,  
Jugend und Sport

Dr.
Martina Münch