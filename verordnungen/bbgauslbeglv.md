## Brandenburgische Verordnung über die Zuständigkeit für die Beglaubigung inländischer öffentlicher Urkunden zur Verwendung im Ausland (Brandenburgische Auslandsbeglaubigungsverordnung - BbgAuslBeglV)

Auf Grund

*   des Artikels 2 Absatz 1 des Gesetzes zu dem Haager Übereinkommen vom 5. Oktober 1961 zur Befreiung ausländischer öffentlicher Urkunden von der Legalisation vom 21. Juni 1965 (BGBl. II S. 875),
*   des Artikels 2 Absatz 1 des Gesetzes zu dem Vertrag vom 7. Juni 1969 zwischen der Bundesrepublik Deutschland und der Italienischen Republik über den Verzicht auf die Legalisation von Urkunden vom 30. Juli 1974 (BGBl. II S. 1069),
*   des Artikels 2 Absatz 1 des Gesetzes zu dem Abkommen vom 13. Mai 1975 zwischen der Bundesrepublik Deutschland und dem Königreich Belgien über die Befreiung öffentlicher Urkunden von der Legalisation vom 25. Juni 1980 (BGBl. II S. 813) und
*   des § 6 Absatz 2 und des § 8 Absatz 3 des Landesorganisationsgesetzes vom 24. Mai 2004 (GVBl. I S. 186), die durch Artikel 2 des Gesetzes vom 10. Juli 2014 (GVBl. I Nr. 28) geändert worden sind,

verordnet die Landesregierung:

#### § 1 Anwendungsbereich

(1) Diese Verordnung bestimmt die Zuständigkeit für die Beglaubigung öffentlicher Urkunden, die nach dem 3. Oktober 1990 von Behörden, Einrichtungen oder sonstigen öffentlichen Stellen des Landes, von Gemeinden oder Gemeindeverbänden oder sonstigen der Aufsicht des Landes oder der Gemeinden oder Gemeindeverbände unterstehenden juristischen Personen des öffentlichen Rechts oder deren öffentlich-rechtlichen Vereinigungen ausgestellt wurden und im Ausland verwendet werden sollen (Auslandsbeglaubigung).

(2) Die in Absatz 1 genannte Zuständigkeit besteht im Einzelnen dafür,

1.  die Echtheit der Unterschrift, der Eigenschaft, in welcher die Unterzeichnerin oder der Unterzeichner einer Urkunde gehandelt hat, und gegebenenfalls die Echtheit des Siegels oder Stempels, mit dem die Urkunde versehen ist, zu bestätigen und dazu auf Antrag der Inhaberin oder des Inhabers oder der Unterzeichnerin oder des Unterzeichners der Urkunde auf dieser selbst oder auf einem mit ihr verbundenen Blatt anzubringen:
    1.  einen Endbeglaubigungsvermerk
        
        aa)
        
        zur weiteren Bestätigung der Echtheit der Urkunde durch die zuständige Vertretung des ausländischen Staates, in dem sie verwendet werden soll (Legalisation),
        
        bb)
        
        gemäß Artikel 3 des Gesetzes zu dem Vertrag vom 7. Juni 1969 zwischen der Bundesrepublik Deutschland und der Italienischen Republik über den Verzicht auf die Legalisation von Urkunden vom 30. Juli 1974 (BGBl. II S. 1069) oder
        
        cc)
        
        gemäß Artikel 3 des Gesetzes zu dem Abkommen vom 13. Mai 1975 zwischen der Bundesrepublik Deutschland und dem Königreich Belgien über die Befreiung öffentlicher Urkunden von der Legalisation vom 25. Juni 1980 (BGBl. II S. 813),
        
    2.  eine Apostille gemäß Artikel 3 bis 5 des Haager Übereinkommens zur Befreiung ausländischer öffentlicher Urkunden von der Legalisation vom 5. Oktober 1961 (BGBl. 1965 II S. 876) oder
    3.  einen Vorbeglaubigungsvermerk zum Zweck einer Buchstabe a Doppelbuchstabe aa entsprechenden Endbeglaubigung durch das Bundesverwaltungsamt;
2.  gemäß Artikel 7 des Haager Übereinkommens zur Befreiung ausländischer öffentlicher Urkunden von der Legalisation ein Verzeichnis der nach diesem Übereinkommen ausgestellten Apostillen zu führen.

#### § 2 Zuständigkeit

(1) Zuständig für Auslandsbeglaubigungen nach § 1 sind

1.  bei Urkunden aus dem Bereich der Justiz
    1.  die Präsidentinnen und Präsidenten der Landgerichte für ihre eigenen Urkunden und für die in ihrem Geschäftsbereich ausgestellten Urkunden der ordentlichen Gerichtsbarkeit und der Staatsanwaltschaft sowie der Notarinnen und Notare, die ihren Amtssitz im Gerichtsbezirk haben,
    2.  im Übrigen das für Justiz zuständige Ministerium;
2.  bei allen übrigen Urkunden die Oberbürgermeisterin oder der Oberbürgermeister der Landeshauptstadt Potsdam als allgemeine untere Landesbehörde.

(2) Die Fachaufsicht über die Oberbürgermeisterin oder den Oberbürgermeister der Landeshauptstadt Potsdam als allgemeine untere Landesbehörde führt das für Inneres zuständige Ministerium.

#### § 3 Deckung der Kosten

(1) Die Kosten der Aufgabenerfüllung durch die Oberbürgermeisterin oder den Oberbürgermeister der Landeshauptstadt Potsdam als allgemeine untere Landesbehörde sind durch die Gebühr zu decken, die nach Maßgabe der Gebührenordnung des Ministers des Innern und für Kommunales vom 21. Juli 2010 (GVBl. II Nr. 46), die zuletzt durch Verordnung vom 1. Juli 2016 (GVBl. II Nr. 34) geändert worden ist, für die Endbeglaubigung von Urkunden, die zum Gebrauch im Ausland bestimmt sind, erhoben wird.

(2) Ist die in Absatz 1 genannte Gebühr drei Jahre lang unverändert geblieben, überprüft die Aufsichtsbehörde nach § 2 Absatz 2, ob die Gebühr die Kosten der Aufgabenerfüllung noch deckt.

#### § 4 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 1. April 2017 in Kraft.
Gleichzeitig treten die Verordnung zur Regelung der Zuständigkeit für die Erteilung der Apostille vom 4. September 1992 (GVBl. II S. 593) und die Verordnung über die Zuständigkeit nach dem Gesetz zu dem Vertrag vom 7. Juni 1969 zwischen der Bundesrepublik Deutschland und der Italienischen Republik über den Verzicht auf die Legalisation von Urkunden vom 25. Januar 1995 (GVBl. II S. 227) außer Kraft.

Potsdam, den 20.
März 2017

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter  
  

Der Minister der Justiz  
und für Europa und Verbraucherschutz

Stefan Ludwig