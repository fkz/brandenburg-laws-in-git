## Verordnung zur Regelung der Zuständigkeit im Bereich der Textilkennzeichnung (Textilkennzeichnungszuständigkeitsverordnung - TextilKennzZustV)

Auf Grund des § 6 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28 S.
2) geändert worden ist, in Verbindung mit § 6 Absatz 1 Textilkennzeichnungsgesetz vom 15.
Februar 2016 (BGBl.
I S. 198), sowie des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl.
I S.
602) verordnet die Landesregierung:

#### § 1 Zuständigkeit

Zuständige Marktüberwachungsbehörde im Sinne des § 6 Absatz 1 Satz 1 des Textilkennzeichnungsgesetzes vom 15. Februar 2016 (BGBl.
I S.
198) ist das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit.
Die Marktüberwachungsbehörde überwacht die Einhaltung des Textilkennzeichnungsgesetzes in der jeweils geltenden Fassung.

#### § 2 Ordnungswidrigkeiten

Die sachliche Zuständigkeit im Sinne des § 36 Absatz 1 Nummer 2 Buchstabe a des Gesetzes über Ordnungswidrigkeiten wird gemäß § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 12 des Textilkennzeichnungsgesetzes auf die nach § 1 zuständige Marktüberwachungsbehörde übertragen.

#### § 3 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 1.
November 2021

Die Landesregierung des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

Ursula Nonnemacher

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#a) Diese Verordnung dient der Umsetzung der Verordnung (EU) Nr.
1007/2011 des Europäischen Parlaments und des Rates vom 27.
September 2011 über die Bezeichnungen von Textilfasern und die damit zusammenhängende Etikettierung und Kennzeichnung der Faserzusammensetzung von Textilerzeugnissen und zur Aufhebung der Richtlinie 73/44/EWG des Rates und der Richtlinien 96/73/EG und 2008/121/EG des Europäischen Parlaments und des Rates (ABl. L 272 vom 18.10.2011, S. 1), die zuletzt durch die Verordnung (EU) 2018/122 (ABl.
L 22 vom 26.1.2018, S. 3) geändert worden ist.