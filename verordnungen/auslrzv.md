## Verordnung über die Zuständigkeiten im Ausländerrecht (Ausländerrechtszuständigkeitsverordnung - AuslRZV)

Auf Grund

*   des § 6 Absatz 2 des Landesorganisationsgesetzes vom 24.
    Mai 2004 (GVBl.
    I S. 186), der durch Artikel 2 des Gesetzes vom 10.
    Juli 2014 (GVBl.
    I Nr. 28 S.
    2) geändert worden ist, und des § 6 Absatz 1 des Landesorganisationsgesetzes in Verbindung mit § 22 Absatz 2 Satz 1 und § 46 Absatz 5 des Asylgesetzes in der Fassung der Bekanntmachung vom 2. September 2008 (BGBl.
    I S.
    1798) und mit § 24 Absatz 4 Satz 1 des Aufenthaltsgesetzes in der Fassung der Bekanntmachung vom 25.
    Februar 2008 (BGBl.
    I S.
    162) und
*   des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
    Februar 1987 (BGBl.
    I S.
    602)

verordnet die Landesregierung:

#### § 1 Ausländerbehörden

Ausländerbehörden nach § 71 Absatz 1 Satz 1 des Aufenthaltsgesetzes sind, soweit nichts  
Anderes geregelt ist,

1.  die kreisfreien Städte und Landkreise als Kreisordnungsbehörden sowie die Große kreisangehörige Stadt Schwedt/Oder als örtliche Ordnungsbehörde.
    Sie nehmen die Aufgaben der Ausländerbehörden als Pflichtaufgaben zur Erfüllung nach Weisung wahr,
2.  die Zentrale Ausländerbehörde für die in § 3 genannten Aufgaben.

#### § 2 Örtliche Zuständigkeit der Ausländerbehörden

(1) Soweit nicht die Zuständigkeit der Zentralen Ausländerbehörde gegeben ist, ist die Ausländerbehörde zuständig, in deren Bezirk sich die ausländische Person gewöhnlich aufhält oder, soweit kein gewöhnlicher Aufenthalt im Bundesgebiet aufgrund eines Auslandsaufenthalts besteht, sich aufzuhalten beabsichtigt.
Ist der Aufenthalt räumlich beschränkt oder besteht eine Wohnsitzauflage, ist die Ausländerbehörde des Bezirks zuständig, in dem die ausländische Person ihren Wohnsitz zu nehmen verpflichtet ist.

(2) Soweit keine Zuständigkeit nach Absatz 1 begründet ist, ist jede Ausländerbehörde zur Entscheidung über die bei ihr gestellten Anträge zuständig.
Maßnahmen und Entscheidungen, für die keine andere Ausländerbehörde zuständig ist, trifft die Ausländerbehörde, in deren Bezirk sich die Notwendigkeit der Anordnung ergibt.
Abweichend von Absatz 1 Satz 1 ist für unaufschiebbare Maßnahmen und Entscheidungen jede Ausländerbehörde zuständig, in deren Bezirk sich die Notwendigkeit der Anordnung ergibt.

(3) Befindet sich die ausländische Person auf richterliche Anordnung in Haft oder in sonstigem öffentlichem Gewahrsam, bleibt die Ausländerbehörde zuständig, in deren Bezirk sich die Person zuvor gewöhnlich aufgehalten hat oder sich aufzuhalten hatte.
Ist der vorherige gewöhnliche Aufenthalt nicht bekannt, ist die Ausländerbehörde zuständig, in deren Bezirk sich die Hafteinrichtung oder das öffentliche Gewahrsam befindet.
Im Übrigen gilt das Verwaltungsverfahrensgesetz.
Für den Antrag auf Abschiebungshaft ist die Ausländerbehörde zuständig, in deren Bezirk die Person ihren gewöhnlichen Aufenthalt hat oder mangels eines solchen aufgegriffen wurde.

#### § 3 Zentrale Ausländerbehörde

Die Zentrale Ausländerbehörde ist

1.  Aufnahmeeinrichtung des Landes nach § 44 Absatz 1 des Asylgesetzes,
2.  die von der Landesregierung bestimmte Stelle im Sinne des § 22 Absatz 2 und des § 46 Absatz 5 des Asylgesetzes,
3.  zuständige Landesbehörde nach § 50 des Asylgesetzes,
4.  zuständige Behörde nach § 15a Absatz 1 Satz 5 des Aufenthaltsgesetzes sowie zuständige Ausländerbehörde für die Entscheidung über die Aussetzung der Abschiebung oder die Erteilung eines Aufenthaltstitels für in der Erstaufnahmeeinrichtung aufhältige Personen nach § 15a des Aufenthaltsgesetzes,
5.  zuständige Stelle nach § 24 Absatz 4 Satz 1 des Aufenthaltsgesetzes,
6.  zuständig für die Abschiebung vollziehbar ausreisepflichtiger ausländischer Personen, bei denen kein nicht nur vorübergehendes Abschiebungshindernis vorliegt, wenn die Mitteilung der zuständigen Ausländerbehörde nach § 4 vorliegt, sowie
7.  zuständige Behörde für den Betrieb von Einrichtungen nach § 61 Absatz 2, § 62a Absatz 1 und § 62b Absatz 2 des Aufenthaltsgesetzes.

#### § 4 Übergang der Zuständigkeit für Vorbereitung, Koordinierung und Vollzug aufenthaltsbeendender Maßnahmen

Kommt die Ausländerbehörde nach umfassender Einzelfallprüfung zu dem Ergebnis, dass für eine vollziehbar ausreisepflichtige Person kein nicht nur vorübergehendes Abschiebungshindernis vorliegt und deshalb eine Abschiebung geboten ist, teilt sie der Zentralen Ausländerbehörde die für eine Abschiebung erforderlichen Angaben zu der vollziehbar ausreisepflichtigen Person mit.

#### § 4a  
Besondere Zuständigkeit

(1) Die Landrätin oder der Landrat des Landkreises Dahme-Spreewald ist als allgemeine untere Landesbehörde die zuständige Ausländerbehörde nach § 71 Absatz 1 Satz 5 des Aufenthaltsgesetzes.
Der Landrat oder die Landrätin des Landkreises Dahme-Spreewald nimmt die Aufgaben nach § 71 Absatz 1 Satz 5 des Aufenthaltsgesetzes für das Gebiet des Landes Brandenburg wahr.

(2) Die dem Landkreis Dahme-Spreewald durch die Einrichtung der nach Absatz 1 zuständigen Ausländerbehörde entstehenden Kosten werden durch eine einmalige Pauschale in Höhe von 2 000 Euro je eingerichteten Arbeitsplatz abgegolten.
Entstehen nachweisbar höhere Kosten, so sind notwendige Kosten ebenfalls durch das Land zu erstatten.

(3) Für die dem Landkreis Dahme-Spreewald entstehenden Kosten durch die Beteiligung im regulären Visumverfahren erstattet das Land eine Fallpauschale, deren Höhe der Gebühr entspricht, die der Ausländer für die Erteilung eines nationalen Visums nach § 46 Absatz 2 Nummer 1 der Aufenthaltsverordnung vom 25.
November 2004 (BGBl.
I S.
2945), die zuletzt durch Artikel 170 der Verordnung vom 19.
Juni 2020 (BGBl.
I S.
1328, 1348) geändert worden ist, in der jeweils geltenden Fassung an die deutsche Auslandsvertretung zu entrichten hat.
Entstehen nachweisbar höhere Kosten für die Beteiligung im regulären Visumverfahren, so sind notwendige Kosten ebenfalls durch das Land zu erstatten.

(4) Die dem Landkreis Dahme-Spreewald entstehenden Kosten für die Durchführung des beschleunigten Fachkräfteverfahrens nach § 81a des Aufenthaltsgesetzes sind durch die Gebühren zu decken, die nach § 47 Absatz 1 Nummer 15 der Aufenthaltsverordnung erhoben werden.
Entstehen nachweisbar höhere Kosten für die Durchführung des beschleunigten Fachkräfteverfahrens, sind die die Gebühren übersteigenden, notwendigen Kosten durch das Land zu erstatten.
In den ersten zwölf Monaten nach Einrichtung der nach Absatz 1 zuständigen Ausländerbehörde erstattet das Land dem Landkreis Dahme-Spreewald eine zusätzliche Fallpauschale in Höhe von 75 Euro für die Durchführung des beschleunigten Fachkräfteverfahrens.

(5) Die Erstattung der Kosten nach den Absätzen 2 bis 4 erfolgt auf Antrag des Landkreises Dahme-Spreewald im Folgejahr.
Zur Berechnung des Erstattungsbetrages legt der Landkreis dem Ministerium des Innern und für Kommunales eine überprüfbare geschäftsstatistische Aufstellung der Amtshandlungen vor.
Das Ministerium des Innern und für Kommunales zahlt den Erstattungsbetrag binnen drei Monaten nach Vorlage der Aufstellung aus.

(6) Für die vor dem 1.
Oktober 2020 eingeleiteten Verfahren zur Durchführung des regulären Visumverfahrens zu den im § 71 Absatz 1 Satz 5 des Aufenthaltsgesetzes genannten Zwecken bleiben die nach § 1 Nummer 1 zuständigen Behörden zuständig.

(7) Für die vor dem 1.
Oktober 2020 eingeleiteten Verfahren zur Durchführung des beschleunigten Fachkräfteverfahrens nach § 81a des Aufenthaltsgesetzes bleiben die nach § 1 Nummer 1 zuständigen Ausländerbehörden zuständig, wenn eine Vereinbarung nach § 81a Absatz 2 des Aufenthaltsgesetzes bereits vor dem 1.
Oktober 2020 abgeschlossen wurde.

#### § 5 Verfolgung von Ordnungswidrigkeiten

Für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 98 Absatz 1 bis 4 des Aufenthaltsgesetzes und § 86 Absatz 1 des Asylgesetzes ist die Zentrale Ausländerbehörde zuständig, soweit die betroffene Person in einer Aufnahmeeinrichtung wohnt oder zu wohnen verpflichtet ist.
Im Übrigen sind die Ausländerbehörden nach § 1 Nummer 1 zuständig.
Sie sind auch zuständig für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 10 Absatz 2 des Freizügigkeitsgesetzes/EU.

#### § 6 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am ersten Tag des zweiten auf die Verkündung folgenden Kalendermonats in Kraft.
Gleichzeitig tritt die Ausländer- und Asyl-Zuständigkeitsverordnung vom 16. September 1996 (GVBl. II S. 748), die zuletzt durch Artikel 35 des Gesetzes vom 23.
September 2008 (GVBl. I S. 202, 210) geändert worden ist, außer Kraft.

Potsdam, den 9.
Juli 2019

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter