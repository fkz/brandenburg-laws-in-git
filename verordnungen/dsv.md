## Verordnung über den Schutz personenbezogener Daten in Schulen, Schulbehörden sowie nachgeordneten Einrichtungen des für Schule zuständigen Ministeriums im Land Brandenburg (Datenschutzverordnung Schulwesen - DSV)

Auf Grund des § 65 Absatz 11 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
August 2002 (GVBl.
I S. 78), der durch Artikel 1 Nummer 47 des Gesetzes vom 8.
Januar 2007 (GVBl.
I S.
2) geändert worden ist, sowie des § 65a Absatz 4 des Brandenburgischen Schulgesetzes, der durch Artikel 4 Nummer 2 des Gesetzes vom 30. November 2007 (GVBl.
I S.
193) eingefügt worden ist, verordnet die Ministerin für Bildung, Jugend und Sport:

Inhaltsübersicht
----------------

### Abschnitt 1  
Datenschutz in Schulen, Schuldatenerhebungen

[§ 1 Umfang der Datenverarbeitung](#1)  
[§ 2 Grundsätze der Datenverarbeitung](#2)  
[§ 3 Nicht automatisierte Datenverarbeitung](#3)  
[§ 4 Automatisierte Datenverarbeitung innerhalb der Schule](#4)  
[§ 5 Automatisierte Datenverarbeitung außerhalb der Schule](#5)  
[§ 6 Übermittlung an Schulen und Schulbehörden](#6)  
[§ 7 Übermittlung an andere öffentliche Stellen](#7)  
[§ 8 Daten der Mitglieder von Mitwirkungsgremien](#8)  
[§ 8a Daten nach dem Infektionsschutzgesetz](#8a)  
[§ 9 Eintragungsberechtigte](#9)  
[§ 10 Einsichts- und Auskunftsrechte](#10)  
[§ 11 Datenschutzmaßnahmen](#11)  
[§ 12 Einschränkung der Verarbeitung und Löschung personenbezogener Daten](#12)  
[§ 13 Schuldatenerhebungen, Schulstatistik](#13)  
[§ 14 Automatisierte zentrale Schülerdatei](#14)  
[§ 15 Schülerlaufbahnstatistiken](#15)

### Abschnitt 2  
Datenschutz in Schulbehörden und nachgeordneten Einrichtungen

[§ 16 Staatliche Schulämter](#16)  
[§ 17 Schulpsychologinnen und Schulpsychologen](#17)

### Abschnitt 3  
Schlussbestimmungen

[§ 18 Inkrafttreten, Außerkrafttreten](#18)

[Anlage 1: Liste der zur Verarbeitung zugelassenen personenbezogenen Daten](#19)  
[Anlage 2: Schülerstammblatt allgemeinbildendende Schule (Grundschule, Oberschule, Gesamtschule, Gymnasium, berufliches Gymnasium, Förderschule, Schule des Zweiten Bildungsweges)](#20)  
[Anlage 3: Schülerstammblatt Berufsschule (duale Ausbildung)](#21)  
[Anlage 4: Schülerstammblatt Berufsschule, Fachoberschule, Berufsfachschule, Fachschule](#22)  
[Anlage 5: Stammblatt für Lehrkräfte und das sonstige pädagogische Personal an Schulen](#23)  
[Anlage 6: Personenbezogene Merkmale für Schuldatenerhebungen](#24)  
[Anlage 7: Antrag auf Genehmigung der Datenverarbeitung außerhalb der Schule gemäß § 5 Absatz 1](#25)  
[Anlage 8: Musteranweisung für die Verarbeitung personenbezogener Daten in Schulen](#26)  
[Anlage 9: APSIS-Daten der Lehrkräfte, die aus dem Personalverwaltungsprogramm mit den Daten der Schule zusammengeführt werden](#27)

### Abschnitt 1  
Datenschutz in Schulen, Schuldatenerhebungen

#### § 1  
Umfang der Datenverarbeitung

(1) Schulen sind gemäß § 65 des Brandenburgischen Schulgesetzes berechtigt, die in den Anlagen 1 bis 9 aufgeführten personenbezogenen Daten von schulpflichtig werdenden Kindern für die erstmalige Aufnahme in die Schule sowie von den Schülerinnen und Schülern und deren Eltern, von Lehrkräften und dem sonstigen pädagogischen Personal zu verarbeiten.
Nicht in den Anlagen 1 bis 9 aufgeführte personenbezogene Daten dürfen nur verarbeitet werden, soweit sie für die Erfüllung der Aufgaben der Schulen erforderlich sind.
Schülerinnen und Schüler, Eltern, Lehrkräfte und sonstiges pädagogisches Personal sind zur Auskunft verpflichtet, soweit es sich um personenbezogene Daten handelt, die in den Anlagen 1 bis 9 aufgeführt sind.

(2) Daten über gesundheitliche Beeinträchtigungen und körperliche Behinderungen dürfen nur mit Einwilligung der Eltern oder der betroffenen volljährigen Schülerinnen oder Schüler und nur dann verarbeitet werden, wenn Schülerinnen und Schüler einer besonderen Betreuung bedürfen oder wenn das Wissen über die gesundheitliche Beeinträchtigung oder körperliche Behinderung für einzelne schulische Veranstaltungen von Belang ist.
Eine Verarbeitung darf ausnahmsweise ohne Einwilligung der betroffenen Personen erfolgen, wenn eine Übermittlung durch das Gesundheitsamt infolge schulärztlicher Untersuchungen auf Grund einer gesetzlichen Übermittlungsbefugnis zulässig ist.

(3) Die Verarbeitung der in den Anlagen 1 bis 9 genannten personenbezogenen Daten ist in nicht automatisierter Form zulässig.
Eine automatisierte Verarbeitung dieser Daten ist zulässig, sofern sie nicht in den Anlagen 1 bis 9 ausgeschlossen ist.
Dies gilt auch für die Verarbeitung von in schulischen Unterlagen enthaltenen personenbezogenen Einzeldaten.

#### § 2  
Grundsätze der Datenverarbeitung

(1) Die Schulen erheben die zur Erfüllung ihnen zugewiesener Aufgaben und für den jeweils damit verbundenen Zweck erforderlichen personenbezogenen Daten grundsätzlich bei den betroffenen Personen.

(2) Werden personenbezogene Daten gemäß den Anlagen 1 bis 9 bei den betroffenen Personen erhoben, gilt hierfür Artikel 13 der Verordnung (EU) 2016/679.

(3) Die Daten der Lehrkräfte und Personen des sonstigen pädagogischen Personals gemäß Anlage 1 Nummer 6 werden vom staatlichen Schulamt der Schule übermittelt, sofern es sich um Daten handelt, die beim staatlichen Schulamt als personalaktenführende Stelle verwaltet werden.

(4) Schulen und staatliche Schulämter dürfen personenbezogene Daten von Schülerinnen und Schülern sowie deren Eltern verarbeiten, die in den Einrichtungen der Jugendhilfe erstmals erhoben wurden und die für die Aufgabenerfüllung im Hinblick auf

1.  den Beginn der Vollzeitschulpflicht und
2.  die Kontrolle der Teilnahme an Sprachstandsfeststellungen und der Sprachförderung erforderlich sind.

(5) Eintragungen, die unrichtig sind, sind, soweit möglich, von Amts wegen zu berichtigen, anderenfalls zu löschen.
Unzulässig gespeicherte Daten sind zu löschen.
Berichtigung und Löschung haben so zu erfolgen, dass nachvollziehbar ist, wer diese aus welchem Grund vorgenommen hat.

#### § 3  
Nicht automatisierte Datenverarbeitung

Personenbezogene Daten in nicht automatisierten Dateisystemen und in Akten, insbesondere in Schülerakten, Klassen- oder Kursbüchern, Notenbüchern oder Unterlagen über Lehrkräfte und Personen des sonstigen pädagogischen Personals sowie Prüfungsunterlagen, Klassenarbeiten und Klausuren sind in verschlossenen Schränken und Räumen aufzubewahren.
Ein Entfernen der Schülerakten, Klassen- oder Kursbücher, Notenbücher, Prüfungsunterlagen sowie der Unterlagen über Lehrkräfte und Personen des sonstigen pädagogischen Personals vom Aufbewahrungsort darf nur so lange erfolgen, wie dies zur Erfüllung der zugrundeliegenden Aufgaben erforderlich ist.
Außer zum Zweck der Übermittlung dürfen diese das Schulgebäude nicht verlassen.
Klassenbücher können zu schulischen Veranstaltungen außerhalb des Schulgebäudes mitgeführt werden, wenn dies für die Gewährleistung von Eintragungen erforderlich ist.

#### § 4  
Automatisierte Datenverarbeitung innerhalb der Schule

(1) Zur Verarbeitung personenbezogener Daten innerhalb der Schule sind grundsätzlich nur von der Schule zur Verfügung gestellte Datenverarbeitungsgeräte einzusetzen.
Erfolgt die Verarbeitung personenbezogener Daten innerhalb der Schule auf privaten Datenverarbeitungsgeräten, gilt § 5 entsprechend.
Die automatisierte Verarbeitung personenbezogener Daten ist nur zulässig, wenn eine sichere Trennung der in der schulinternen Verwaltung verwendeten Daten von den im Unterricht verwendeten Daten durch technische Maßnahmen gewährleistet ist.

(2) Die Programmentwicklung, Freigabe gemäß § 4 des Brandenburgischen Datenschutzgesetzes, Organisation und Verantwortlichkeit der Datenverarbeitung mit Datenverarbeitungsgeräten sowie deren Kontrolle und Datensicherung gemäß § 11 sind von der Schulleitung verbindlich zu regeln.
Sofern das für Schule zuständige Ministerium für die automatisierte Datenverarbeitung landeseinheitliche Festlegungen trifft, gelten diese auch für Ersatzschulen.

#### § 5  
Automatisierte Datenverarbeitung außerhalb der Schule

(1) Die Schulleiterin oder der Schulleiter kann Lehrkräften und dem sonstigen pädagogischen Personal genehmigen, personenbezogene Daten gemäß Anlage 1 außerhalb der Schule zu verarbeiten, soweit dies nicht gemäß Anlage 1 für bestimmte personenbezogene Daten ausgeschlossen ist.
Die Genehmigung darf nur erteilt werden, wenn

1.  die Datenverarbeitung der konkreten Aufgabenerfüllung im unmittelbaren pädagogischen Verantwortungsbereich der Lehrkraft oder der Person des sonstigen pädagogischen Personals dient,
2.  ein Sicherheitskonzept gemäß § 4 Absatz 1 Satz 2 Nummer 2 des Brandenburgischen Datenschutzgesetzes in Verbindung mit Artikel 32 der Verordnung (EU) 2016/679 existiert, das auch die besonderen Risiken der Datenverarbeitung außerhalb der Schule und auf privaten Geräten berücksichtigt,
3.  die Umsetzung technischer und organisatorischer Maßnahmen nach dem Sicherheitskonzept sowie gemäß Artikel 25 und Artikel 32 Absatz 1 und 2 der Verordnung (EU) 2016/679 nachgewiesen und durch die Schulleiterin oder den Schulleiter bestätigt wurde und
4.  die vorherige schriftliche Einverständniserklärung vorliegt, sich der Kontrolle der Landesbeauftragten für den Datenschutz und für das Recht auf Akteneinsicht zu unterwerfen.
    Die Schule bleibt Verantwortlicher im Sinne von Artikel 4 Nummer 7 der Verordnung (EU) 2016/679.
    Für die Beantragung und Genehmigung ist Anlage 7 zu verwenden.
    In dieser sind der Zweck der Verarbeitung, die eingesetzten Programme, die vorgesehenen Dateien und Auswertungen zu beschreiben.

(2) Wird ein Zugriff unberechtigter Dritter oder ein Verstoß gegen datenschutzrechtliche Bestimmungen festgestellt, ist die Genehmigung unverzüglich zu widerrufen.

#### § 6  
Übermittlung an Schulen und Schulbehörden

(1) Bei der Übermittlung personenbezogener Daten ist zu gewährleisten, dass Unbefugte keine Einsicht erlangen können.
Zwischen den Schulen erfolgt die Übermittlung gemäß den Absätzen 2 bis 5.
Die Übermittlung an die Schulbehörden erfolgt gemäß Absatz 6 und § 16.
Die Übermittlung personenbezogener Daten in landeseinheitlichen Verfahren darf nur kontrolliert über Schnittstellen erfolgen.
Die entsprechenden Berechtigungen sind danach zu bestimmen, ob die Datenverarbeitung für die Aufgabenwahrnehmung erforderlich ist.

(2) Wechselt eine Schülerin oder ein Schüler die Schule, sind das Schülerstammblatt und eine Kopie des letzten Zeugnisses an die aufnehmende Schule zu übermitteln.
Die Übermittlung erfolgt unmittelbar nach Eingang der Aufnahmebestätigung.
Wechselt eine Schülerin oder ein Schüler innerhalb der Primarstufe, der Sekundarstufe I oder der Sekundarstufe II die Schule, ist die gesamte Schülerakte zu übermitteln.
Dies gilt auch für den Übergang von der Primarstufe in die Sekundarstufe I.
Erfolgt der Schulwechsel innerhalb eines Schuljahres sind darüber hinaus die bereits erteilten Leistungsbewertungen in den jeweiligen Fächern der aufnehmenden Schule mitzuteilen.
Die Übermittlung erfolgt unmittelbar nach Eingang der Aufnahmebestätigung.

(3) Wechselt eine Schülerin oder ein Schüler nach Abschluss der Jahrgangsstufe 10 die Schule, sind das Schülerstammblatt und eine Kopie des letzten Zeugnisses an die aufnehmende Schule zu übermitteln.
Die Übermittlung erfolgt unmittelbar nach Eingang der Aufnahmebestätigung.
Wechselt eine Schülerin oder ein Schüler mit sonderpädagogischem Förderbedarf die Schule, sind die Unterlagen aus Förderausschussverfahren zu übermitteln.

(4) Wechselt eine Schülerin oder ein Schüler an eine Schule in einem anderen Land der Bundesrepublik Deutschland, so werden das Schülerstammblatt und eine Kopie des letzten Zeugnisses nur auf Antrag der aufnehmenden Schule übermittelt.
Bei Vorliegen der Einwilligung der Eltern oder der volljährigen Schülerin oder des volljährigen Schülers ist die gesamte Schülerakte zu übermitteln.

(5) Wechselt eine Schülerin oder ein Schüler an eine Schule außerhalb der Bundesrepublik Deutschland, so ist auf Antrag der ausländischen Schule ein pädagogisches Gutachten über den Leistungsstand der Schülerin oder des Schülers zu übermitteln.

(6) Für die Zwecke der Unterrichtsplanung, Personalmaßnahmen, Stellenbewirtschaftung oder allgemeine Maßnahmen der Schulaufsicht sind personenbezogene Daten der Lehrkräfte und des sonstigen pädagogischen Personals gemäß Anlage 1 von den Schulen an die Schulbehörden zu übermitteln.

#### § 7  
Übermittlung an andere öffentliche Stellen

(1) Die Übermittlung personenbezogener Daten der Schülerinnen und Schüler sowie von Eltern an andere öffentliche Stellen erfolgt gemäß § 65 Absatz 6 des Brandenburgischen Schulgesetzes insbesondere dann, wenn

1.  dies zur Abwehr erheblicher Nachteile für das Gemeinwohl oder zur Abwehr einer schwerwiegenden Beeinträchtigung der Rechte einer anderen Schülerin, eines anderen Schülers oder einer dritten sonstigen Person notwendig ist,
2.  sich bei der rechtmäßigen Aufgabenerfüllung Anhaltspunkte für Straftaten oder Ordnungswidrigkeiten von Schülerinnen, Schülern oder Eltern ergeben und die Übermittlung an die für die Verfolgung oder Vollstreckung zuständigen Behörden erfolgt oder
3.  bei dem Vorliegen begründeter Anhaltspunkte die Sorge für das Wohl der Schülerin oder des Schülers es erfordert, das Jugendamt oder andere öffentliche Stellen zu informieren.

Übermittlungsvorgänge sind gemäß § 65 Absatz 6 Satz 5 des Brandenburgischen Schulgesetzes aktenkundig zu machen.
Hierbei ist die Rechtsgrundlage der Übermittlung, der Umfang der übermittelten Daten und die genaue Bezeichnung und Anschrift des Empfängers anzugeben.
Der Vermerk ist zu den Unterlagen der Schulverwaltung (Verwaltungsakten) der Schule zu nehmen.
Erfolgt die Übermittlung auf Grund eines Ersuchens des Empfängers, hat dieser die Rechtsgrundlage anzugeben, die ihn zur Erhebung dieser Daten bei der Schule als öffentliche Stelle berechtigt.
Soweit sich dies nicht aus der Rechtsgrundlage ergibt, hat der Ersuchende zu begründen, weshalb die Daten nicht bei den betroffenen Personen erhoben werden.

(2) Eine Übermittlung von personenbezogenen Daten der Schülerinnen und Schüler sowie von Eltern an das Jugendamt ist zulässig

1.  auf dessen Anforderung gemäß § 62 Absatz 3 des Achten Buches Sozialgesetzbuch,
2.  mit Zustimmung der Eltern oder der volljährigen Schülerin oder des volljährigen Schülers,
3.  zur Inanspruchnahme von sozialpädagogischer Hilfe nach Verhängung einer Ordnungsmaßnahme oder
4.  wenn es die Sorge für das Wohl der Schülerin oder des Schülers gemäß § 4 Absatz 3 Satz 3 des Brandenburgischen Schulgesetzes in Verbindung mit § 4 des Gesetzes zur Kooperation und Information im Kinderschutz erfordert.

Bei begründetem Verdacht einer schwerwiegenden Vernachlässigung oder einer Misshandlung des Kindes in der Familie kann eine Meldung über das staatliche Schulamt an das Amtsgericht (Vormundschaftsgericht) unter Übermittlung der erforderlichen personenbezogenen Daten erfolgen.

#### § 8  
Daten der Mitglieder von Mitwirkungsgremien

Von Mitgliedern der Mitwirkungsgremien sind Name, Vorname, Anschrift sowie bei überschulischen Gremien zusätzlich Name und Anschrift der vertretenen Schule oder bei entsandten Mitgliedern der durch sie vertretenen Stelle in geeigneter Weise zu veröffentlichen.
Hierfür kann die Veröffentlichung im Amtsblatt des Ministeriums für Bildung, Jugend und Sport genutzt werden.
Die Bekanntmachung für schulische Gremien erfolgt durch die Schulleitung, für Gremien auf der Ebene der Landkreise und kreisfreien Städte durch das staatliche Schulamt und für Gremien auf der Ebene des Landes durch das für Schule zuständige Ministerium.
Ein Mitglied kann der Veröffentlichung seiner Anschrift widersprechen.
Die Kandidatinnen und Kandidaten für Mitwirkungsgremien sind vor der Wahl darauf hinzuweisen.

#### § 8a  
Daten nach dem Infektionsschutzgesetz

Schulen dürfen personenbezogene Daten im Zusammenhang mit dem Nachweis gemäß § 20 Absatz 9 des Infektionsschutzgesetzes über einen ausreichenden Impfschutz gegen Masern verarbeiten.

#### § 9  
Eintragungsberechtigte

(1) Eintragungsberechtigte in Schülerakten sind

1.  die Klassenlehrerin oder der Klassenlehrer und die Tutorin oder der Tutor,
2.  die Abteilungsleiterin oder der Abteilungsleiter,
3.  die Schulleiterin oder der Schulleiter und die Stellvertreterin oder der Stellvertreter,
4.  die Oberstufenkoordinatorin oder der Oberstufenkoordinator,
5.  die Primarstufenleiterin oder der Primarstufenleiter sowie
6.  die Mitarbeiterinnen oder die Mitarbeiter des Schulsekretariats sowie weitere Lehrkräfte nach Weisung.

Die Schulleitung hat für die einheitliche Führung der Schülerakten zu sorgen und entscheidet in Zweifelsfällen, ob eine Eintragung in die Schülerakte erfolgt oder Unterlagen zur Schülerakte genommen werden.
Aus der Schülerakte muss hervorgehen, wer eine Eintragung vorgenommen oder die Weisung zur Eintragung in die Schülerakte gegeben hat.

(2) Eintragungsberechtigt in Klassen- oder Kursbücher sind die unterrichtenden Lehrkräfte, die Klassenlehrerin oder der Klassenlehrer und die Schulleitung.
Die Schulleitung sorgt für die regelmäßige Führung der Klassen- oder Kursbücher.

(3) Eintragungsberechtigt in das Notenbuch einer Klasse oder eines Kurses sind die in den jeweiligen Fächern unterrichtenden Lehrkräfte.
Über den Umfang der Eintragungen gemäß Anlage 1 Nummer 3.2 entscheiden die Fachkonferenzen.

(4) Die Eintragungsberechtigung in Prüfungsunterlagen ergibt sich aus den in den Verordnungen zur Ausgestaltung der Prüfungen gemäß § 60 Absatz 4 Satz 1 des Brandenburgischen Schulgesetzes getroffenen Regelungen.

(5) In die Unterlagen über Lehrkräfte und Personen des sonstigen pädagogischen Personals sind die Schulleitung sowie auf Weisung der Schulleiterin oder des Schulleiters die Mitarbeiterinnen und Mitarbeiter des Schulsekretariats eintragungsberechtigt.

#### § 10  
Einsichts- und Auskunftsrechte

(1) Einsichts- und Auskunftsrechte bestimmen sich nach § 65 Absatz 8 des Brandenburgischen Schulgesetzes.
Diese Bestimmungen gelten auch für Einsichts- und Auskunftsrechte ehemaliger Schülerinnen und Schüler.
Die Schule kann die Einsichtnahme in Unterlagen zeitlich beschränken, wenn diese sonst zu einer unzumutbaren Belastung für die Schule führt.

(2) Soweit eine Einsichtnahme zur Erfüllung der dienstlichen Aufgaben erforderlich ist, dürfen in Schülerakten neben den Eintragungsberechtigten gemäß § 9 Absatz 1 Einsicht nehmen

1.  Schulrätinnen und Schulräte,
2.  Schulpsychologinnen und Schulpsychologen sowie
3.  Vertrauenslehrerinnen oder Vertrauenslehrer.

(3) Soweit eine Einsichtnahme zur Erfüllung der dienstlichen Aufgaben erforderlich ist, dürfen in Notenbücher neben den Eintragungsberechtigten gemäß § 9 Absatz 3 Einsicht nehmen

1.  Schulleiterinnen oder Schulleiter,
2.  Abteilungsleiterinnen oder Abteilungsleiter,
3.  Oberstufenkoordinatorinnen oder Oberstufenkoordinatoren,
4.  Klassenlehrerinnen oder Klassenlehrer und Tutorinnen oder Tutoren,
5.  Lehrkräfte, die in der Klasse oder dem Kurs unterrichten,
6.  Schulrätinnen und Schulräte sowie
7.  Schulpsychologinnen und Schulpsychologen.

(4) Die stimmberechtigten Mitglieder von Prüfungsausschüssen können Einsicht in Prüfungsunterlagen nehmen.
Die Einsicht der Schülerinnen und Schüler ist nach Abschluss der Prüfung möglich, sofern die Verordnungen über Prüfungen nichts anderes bestimmen.

(5) In die Unterlagen über Lehrkräfte und Personen des sonstigen pädagogischen Personals dürfen die Schulleitung, die Leitung des staatlichen Schulamtes sowie die für die Schule zuständige Schulrätin oder der für die Schule zuständige Schulrat Einsicht nehmen.

#### § 11  
Datenschutzmaßnahmen

(1) Für die Einhaltung des Datenschutzes in der Schule ist die Schulleiterin oder der Schulleiter verantwortlich.
Sie oder er gibt Hinweise zur Datenverarbeitung und benennt gemäß Artikel 37 Absatz 1 der Verordnung (EU) 2016/679 den Datenschutzbeauftragten.
Der Datenschutzbeauftragte kann nach Maßgabe von Artikel 37 Absatz 3 der Verordnung (EU) 2016/679 für mehrere Schulen benannt werden.
Er ist in diesem Fall von jeder Schule gesondert zu benennen.

(2) Werden personenbezogene Daten automatisiert verarbeitet, sind Maßnahmen zu treffen, die geeignet sind, zu gewährleisten, dass

1.  nur Befugte diese Daten zur Kenntnis nehmen können (Vertraulichkeit),
2.  diese Daten während der Verarbeitung unversehrt, vollständig und aktuell bleiben (Integrität),
3.  diese Daten zeitgerecht zur Verfügung stehen und ordnungsgemäß verarbeitet werden können (Verfügbarkeit),
4.  diese Daten jederzeit ihrem Ursprung zugeordnet werden können (Authentizität),
5.  festgestellt werden kann, wer wann welche personenbezogenen Daten in welcher Weise verarbeitet hat (Revisionsfähigkeit) und
6.  die Verfahrensweisen bei der Verarbeitung dieser Daten vollständig, aktuell und in einer Weise dokumentiert sind, dass sie in zumutbarer Zeit nachvollzogen werden können (Transparenz).

Die Maßnahmen haben für den angestrebten Schutzzweck angemessen zu sein und richten sich nach den im Einzelfall zu betrachtenden Risiken und dem jeweiligen Stand der Technik.
Sie sind vor dem erstmaligen Einsatz sowie bei wesentlichen Änderungen eines Verfahrens im Rahmen eines Sicherheitskonzeptes gemäß § 4 des Brandenburgischen Datenschutzgesetzes in Verbindung mit Artikel 32 der Verordnung (EU) 2016/679 zu beschreiben und umzusetzen.
Die Schulleiterin oder der Schulleiter stellt sicher, dass für die Datenverarbeitung ein Verzeichnis von Verarbeitungstätigkeiten gemäß Artikel 30 der Verordnung (EU) 2016/679 angefertigt wird.

(3) Werden personenbezogene Daten in Akten verarbeitet, ist der Zugriff Unbefugter bei der Bearbeitung, der Aufbewahrung, dem Transport und der Vernichtung zu verhindern.
Soweit Vorentwürfe und Notizen nicht Bestandteil eines Vorganges werden und personenbezogene Daten enthalten, sind diese ordnungsgemäß zu vernichten, wenn sie nicht mehr erforderlich sind.

(4) Für die Einhaltung des Datenschutzes ist eine Dienstanweisung nach dem Muster gemäß Anlage 8 zu erstellen, in der insbesondere festzulegen sind

1.  wie die Sicherung, Einschränkung der Verarbeitung und Löschung der personenbezogenen Daten erfolgt,
2.  welche Personen unter Beachtung der Festlegungen in den §§ 9 und 10 auf diese Daten zugreifen dürfen,
3.  wer diese Daten verändern darf und
4.  von wem, unter welchen Voraussetzungen, in welchem Umfang und an welcher Stelle personenbezogene Daten übermittelt werden dürfen.

Die Datenverarbeitung soll so organisiert werden, dass die Trennung der Daten nach den jeweils verfolgten Zwecken und nach den unterschiedlichen betroffenen Personen möglich ist.
Dies gilt insbesondere bei der Datenübermittlung, der Kenntnisnahme im Rahmen der Aufgabenerfüllung und der Einsichtnahme.

#### § 12  
Einschränkung der Verarbeitung und Löschung personenbezogener Daten

(1) Personenbezogene Daten der Schülerinnen und Schüler, der Eltern, der Lehrkräfte und des sonstigen pädagogischen Personals dürfen in Dateisystemen und in Akten gespeichert und aufbewahrt werden.
Für die Speicherung in Dateisystemen und die Aufbewahrung von Akten in der Schule gelten als Fristen für

1.  Zweitschriften oder Kopien der Abgangs- oder Abschlusszeugnisse 40 Jahre,
2.  Schülerakten zehn Jahre,
3.  Klassen- oder Kurs- sowie Notenbücher drei Jahre,
4.  Klassenarbeiten und Klausuren der gymnasialen Oberstufe zwei Jahre,
5.  Prüfungsunterlagen zehn Jahre,
6.  Schriftverkehr, Unterlagen der Mitwirkungsgremien der Schule und Unterlagen der Schulverwaltung (Verwaltungsakten) fünf Jahre,
7.  Anmeldeunterlagen, Gesprächsprotokolle und Aufnahmeentscheidungen gemäß Anlage 1 Nummer 1.4 ein Jahr,
8.  Bildungsempfehlung und Unterlagen des Förderausschussverfahrens gemäß Anlage 1 Nummer 1.13 sowie individuelle Lernstandsanalysen gemäß Anlage 1 Nummer 1.17.3, individuelle Förderpläne gemäß Anlage 1 Nummer 1.17.4 und Angaben zur Teilnahme an Sprachstandsfeststellungen und Sprachförderung gemäß Anlage 1 Nummer 1.17.5 ein Jahr,
9.  Unterlagen über erteilte Ordnungsmaßnahmen, Androhungen von Ordnungsmaßnahmen und Erziehungsmaßnahmen gemäß § 3 Absatz 2 Nummer 2, 5 und 6 der Verordnung über Konfliktschlichtung, Erziehungs- und Ordnungsmaßnahmen zwei Jahre,
10.  Unterlagen über Lehrkräfte und das sonstige pädagogische Personal zwei Jahre und
11.  alle übrigen Unterlagen fünf Jahre.

Alle in Dateisystemen gespeicherten oder in Akten aufbewahrten sonstigen personenbezogenen Daten von Schülerinnen, Schülern und deren Eltern sind nach Abschluss des Zwecks, für den sie erhoben wurden, zu löschen oder zu vernichten, spätestens zu dem Zeitpunkt, zu dem die Schülerin oder der Schüler die Schule verlässt.
Unterlagen gemäß Nummer 10 sind unmittelbar nach dem Ausscheiden der jeweiligen Person aus der Schule an das zuständige staatliche Schulamt zu übermitteln und dort zu den Personalakten zu nehmen.

(2) Die Aufbewahrungs- oder Speicherungsfrist beginnt für Nachweise

1.  gemäß Absatz 1 Nummer 1, 2 und 5 am Ende des Schuljahres, in dem das jeweilige Schulverhältnis beendet wurde,
2.  gemäß Absatz 1 Nummer 3, 4, 6 und 11 nach Beendigung des Schuljahres, in dem diese erstellt wurden,
3.  gemäß Absatz 1 Nummer 7 mit der Aufnahme in die Schule,
4.  gemäß Absatz 1 Nummer 8 für Unterlagen des Förderausschussverfahrens sowie für individuelle Förderpläne nach Auslaufen des sonderpädagogischen Förderbedarfs und für die übrigen Unterlagen nach Beendigung des Schuljahres, in dem diese erstellt wurden,
5.  gemäß Absatz 1 Nummer 9 mit der Erteilung der Androhung einer Ordnungsmaßnahme, der Ordnungsmaßnahme oder der Erziehungsmaßnahme und
6.  gemäß Absatz 1 Nummer 10 am Ende des Schuljahres nach dem Ausscheiden aus der Schule.

Klassenarbeiten, Vergleichsarbeiten als diagnostische Testverfahren und Klausuren, die nicht von Absatz 1 Nummer 4 erfasst sind, werden nicht in der Schule aufbewahrt und sollen den Schülerinnen und Schülern unmittelbar nach der Auswertung ausgehändigt werden.
Die Schule hat darüber zu informieren, dass die nicht in der Schule aufzubewahrenden Arbeiten von den Eltern oder bei Volljährigkeit von den Schülerinnen und Schülern aufbewahrt werden.
Im Fall der Auflösung einer Schule gelten die Aufbewahrungs- und Speicherungsfristen für das für die Schule zuständige staatliche Schulamt, dem die Dateisysteme und Akten zu übermitteln sind.

(3) Dateisysteme und Akten, deren Speicherungs- und Aufbewahrungsfrist abgelaufen ist, sind gemäß dem Brandenburgischen Archivgesetz dem zuständigen kommunalen Archiv anzubieten.
Dies gilt nicht für Unterlagen gemäß § 1 Absatz 1 Nummer 4 und 5, wenn nach Ablauf der Aufbewahrungsfrist deren Rückgabe von der Schülerin oder dem Schüler gewünscht wird.
Über die mögliche Rückgabe hat die Schule rechtzeitig zu informieren.
Die Löschung der Dateisysteme und die Vernichtung der Akten erfolgt unmittelbar nach der Entscheidung, diese nicht zu archivieren.
Für die ordnungsgemäße Löschung und Vernichtung der Dateisysteme und Akten sowie deren Protokollierung ist die Schulleiterin oder der Schulleiter verantwortlich.

(4) Für die Löschung und Einschränkung der Verarbeitung personenbezogener Daten gelten darüber hinaus Artikel 17 und Artikel 18 der Verordnung (EU) 2016/679.

#### § 13  
Schuldatenerhebungen, Schulstatistik

(1) Für Aufgaben gemäß § 65 Absatz 3 des Brandenburgischen Schulgesetzes werden Schuldatenerhebungen durchgeführt.
Dabei übermitteln Schulen und Ersatzschulen Daten gemäß Anlage 6 an das für Statistik zuständige Amt oder eine von dem für Schule zuständigen Ministerium beauftragte und den Grundsätzen des Brandenburgischen Statistikgesetzes verpflichtete Stelle.
Die Erhebungszeiträume und Stichtage werden durch das für Schule zuständige Ministerium bekannt gegeben.

(2) Die Schuldatenerhebungen gemäß Absatz 1 werden durch die Stelle gemäß Absatz 1 im Rahmen von Geschäftsstatistiken gemäß § 9 des Brandenburgischen Statistikgesetzes aufbereitet (Schulstatistik).

(3) Bei der Aufbereitung von Schülerindividualdaten werden als Hilfsmerkmale der Name, der Vorname sowie die landeseinheitliche Schülernummer verwendet.
Die Hilfsmerkmale sind von den Erhebungsmerkmalen zum frühestmöglichen Zeitpunkt zu trennen und gesondert aufzubewahren.
Die Hilfsmerkmale sind unverzüglich zu löschen, wenn sie nicht mehr erforderlich sind.
Die erhobenen Datensätze sind nach Abschluss der Plausibilisierungen in zentralen Auswertedatenbanken zu speichern.

(4) Die Schuldatenerhebungen gemäß Absatz 1 werden durch das für Schule zuständige Ministerium grundsätzlich als landeseinheitliche Verfahren eingerichtet.
Dabei kann anstelle der Übermittlung der Daten durch die Schulen auch der Abruf der Daten durch die Stelle gemäß Absatz 1 aus den landeseinheitlichen Verfahren treten.

(5) Sofern das für Schule zuständige Ministerium oder eine andere staatliche Schulbehörde Auswertungen als Stelle gemäß Absatz 1 durchführt, sind Regelungen zur statistischen Geheimhaltung schriftlich festzulegen und im Rahmen einer Dienstanweisung zu gewährleisten.
Dabei sind insbesondere die räumliche, organisatorische und personelle Trennung der Stelle gemäß Absatz 1 von anderen Stellen des Verwaltungsvollzugs zu sichern und der Zutritt unbefugter Personen ist auszuschließen.
Weiterhin sind bei der Verarbeitung von Einzelangaben aus den Schuldatenerhebungen die Abschottung dieser Daten von anderen Verwaltungsdaten und ihre Zweckbindung durch technische und organisatorische Maßnahmen zu gewährleisten.
Die mit der Datenaufbereitung befassten Personen dürfen die aus ihrer Tätigkeit gewonnenen personenbezogenen Erkenntnisse nicht in anderen Verfahren oder für andere Zwecke verwenden oder offenbaren.
Sie sind auf die Wahrung des Statistikgeheimnisses und zur Geheimhaltung der aus ihrer Tätigkeit gewonnenen personenbezogenen Erkenntnisse schriftlich zu verpflichten.
Die Verpflichtung gilt auch nach Beendigung der Tätigkeit in der Stelle gemäß Absatz 1.

(6) Den staatlichen Schulämtern und den Schulträgern kann nach Maßgabe des für Schule zuständigen Ministeriums Zugriff auf die Daten der zentralen Auswertungsdatenbanken im Rahmen der jeweiligen Aufgabenzuständigkeit eingeräumt werden.
Die Schule ist für die Richtigkeit der Daten verantwortlich.
Die Plausibilitätsprüfung der Daten erfolgt durch die staatlichen Schulämter und das für Statistik zuständige Amt oder eine andere beauftragte und den Grundsätzen des Brandenburgischen Statistikgesetzes verpflichtete Stelle, für die alle zu diesem Zweck erforderlichen Daten im Rahmen des landeseinheitlichen Verfahrens bereitgestellt werden.

(7) Das für Schule zuständige Ministerium legt gemäß Anlage 9 fest, welche personenbezogenen Daten von Lehrkräften und dem sonstigen pädagogischen Personal von Schulen in öffentlicher Trägerschaft unmittelbar aus dem Personalverwaltungssystem übernommen werden.
Das für Statistik zuständige Amt oder eine andere beauftragte und den Grundsätzen des Brandenburgischen Statistikgesetzes verpflichtete Stelle ist im Rahmen der Erhebungen gemäß Absatz 1 berechtigt, die personenbezogenen Daten der Lehrkräfte und des sonstigen pädagogischen Personals aus dem Personalverwaltungsprogramm und den Schulen zusammenzuführen.

(8) Das für Schule zuständige Ministerium, die staatlichen Schulämter, die Schulen sowie die Schulträger sind berechtigt, statistische Daten für ihren Bereich anonymisiert zu veröffentlichen.
Das für Statistik zuständige Amt oder eine andere beauftragte und den Grundsätzen des Brandenburgischen Statistikgesetzes verpflichtete Stelle ist nach Maßgabe des für Schule zuständigen Ministeriums berechtigt, bildungsstatistische Daten anonymisiert zu veröffentlichen und insbesondere für wissenschaftliche Zwecke sowie für überregionale und internationale Auswertungen zu übermitteln.

#### § 14  
Automatisierte zentrale Schülerdatei

(1) Die automatisierte zentrale Schülerdatei (Schülerdatei) besteht aus einem automatisiert geführten einheitlichen Bestand von Daten der Schulen in öffentlicher Trägerschaft und der Ersatzschulen.
Sie wird von dem für Schule zuständigen Ministerium errichtet.
In der Schülerdatei sind nur die in § 65a Absatz 1 Satz 2 Nummer 1 bis 10 des Brandenburgischen Schulgesetzes bestimmten personenbezogenen Daten zu verarbeiten, wobei für Zwecke der Überwachung der Schulpflicht zu § 65a Absatz 1 Satz 2 Nummer 7 des Brandenburgischen Schulgesetzes die gemäß Absatz 2 bestimmten Daten zu verarbeiten sind.

(2) In der Schülerdatei sind folgende personenbezogene Daten und Merkmale zu verarbeiten:

1.  die landeseindeutige Schülernummer, Name der Schülerin oder des Schülers (Vorname, Name, Geburtsname), Geschlecht, Geburtsdatum, Anschrift (Bundesland, Postleitzahl, Wohnort, Ortsteil, Straße, Hausnummer),
2.  Namen der Eltern (Vorname, Name, Geburtsname, Anrede), Anschrift der Eltern (Postleitzahl, Wohnort, Ortsteil, Straße, Hausnummer), Art der Personensorgeberechtigung (Mutter, Vater, Vormund, Sonstiger),
3.  Schulnummer (derzeit besuchte Schule und zuvor besuchte Schulen),
4.  Merkmale gemäß § 65a Absatz 1 Nummer 7 des Brandenburgischen Schulgesetzes: Status Schulpflicht (vollzeitschulpflichtig, berufsschulpflichtig, nicht schulpflichtig), aktives Schulverhältnis (insbesondere keine längerfristige Beurlaubung, kein Gastschulverhältnis an anderer Schule),
5.  Teilnahme an schulärztlichen und schulzahnärztlichen Untersuchungen,
6.  Teilnahme an der Sprachstandsfeststellung sowie an Sprachförderung,
7.  Schulanmeldung und Schulwechsel: Status im Anmelde- und Zuweisungsverfahren (zum Beispiel Bewerber, abgelehnte Bewerber, aufgenommene Bewerber, Warteliste, Zurückstellung, die für die jeweiligen Übergangsverfahren in die Jahrgangsstufen 5, 7 und 11 als erforderlich geregelten Merkmale),
8.  externer Schulbesuch (die Schülerin oder der Schüler ist einer anderen Schule zugeordnet, besucht aber stundenweise diese Schule), Schulnummer dieser Schule, Beginn und Ende dieses Schulbesuchs,
9.  Schulverhältnis in einem anderen Bundesland, Antrag auf vorgezogene Einschulung gemäß § 37 Absatz 4 Satz 2 des Brandenburgischen Schulgesetzes (einschließlich Ablehnung oder Gestattung), Antrag der Eltern gemäß § 106 Absatz 4 des Brandenburgischen Schulgesetzes (einschließlich Ablehnung oder Gestattung).

(3) Die personenbezogenen Daten der Schülerdatei werden von den Schulen und den für die jeweiligen Schulen zuständigen staatlichen Schulämtern im Rahmen der Zuständigkeit verarbeitet.
Die Datenverarbeitung im Rahmen der Schülerdatei einer Schule bezieht sich nur auf die Schülerinnen und Schüler, für die sie Aufgaben im Sinne der Schulpflichtüberwachung im Rahmen des Schulverhältnisses wahrnimmt.
Dies gilt entsprechend für die Schulen, die im Rahmen der Sprachstandsfeststellungen und Sprachförderung als zuständige Schule gelten.
Technisch organisatorisch ist zu gewährleisten, dass jede Schule nur Zugriff auf die Daten der Personen hat, für die sie zuständig ist.
Das zuständige staatliche Schulamt hat einzelfallbezogene Zugriffsrechte nur in dem zur Erfüllung der Aufgaben erforderlichen Umfang.
Die Erteilung der erforderlichen Zugriffsrechte sowie deren zeitlicher Umfang werden im Rahmen eines Sicherheitskonzeptes gemäß § 4 des Brandenburgischen Datenschutzgesetzes und gemäß Artikel 5 der Verordnung (EU) 2016/679 festgelegt.
Erfolgt der Zugriff im Rahmen schulaufsichtlicher Befugnisse, ist zu gewährleisten, dass ein namentlicher Bezug auf einzelne Personen nicht möglich ist.
Bei der Aufgabenzuständigkeit im Einzelfall dürfen personenbezogene Daten außerhalb der Aufgabenzuständigkeit nicht einsehbar sein.

(4) Für die zentrale Datenhaltung im Rahmen landeseinheitlicher Verfahren ist insbesondere für Schulwechsel eine Änderung der Berechtigung zum Zeitpunkt des Löschens gemäß Absatz 2 vorzusehen.
Ein weiterer datenbezogener Zugriff der bisher zuständigen Schule muss ausgeschlossen sein.
Der automatisierte Abruf aus der Schülerdatei ist nicht zulässig.

(5) Für einzelne Einträge, Ergänzungen, Berichtigungen und Löschungen im Zusammenhang mit der Schülerdatei gemäß Absatz 2 ist die für die Schülerin oder den Schüler zuständige Schule verantwortlich.
Das Personal, das für die Verarbeitung der Daten der Schülerdatei zuständig ist, bestimmt die Schulleiterin oder der Schulleiter.
Unterbleibt die ordnungsgemäße Löschung gemäß Satz 1, ist diese durch das für Schule zuständige Ministerium oder eine von diesem beauftragte Stelle durchzuführen.
Über die Löschung hinaus gehende Rechte des für Schule zuständigen Ministeriums bleiben unberührt.

(6) Die Daten der Schülerinnen und Schüler gemäß § 65a Absatz 1 Nummer 7 bis 10 des Brandenburgischen Schulgesetzes sind zwei Jahre nach Beendigung des Schulverhältnisses zu löschen.
Die Daten der Schülerinnen und Schüler gemäß § 65a Absatz 1 Nummer 1 bis 4 und Nummer 6 des Brandenburgischen Schulgesetzes (Stammdaten) sind zehn Jahre nach Beendigung des Schulverhältnisses zu löschen.
Die Daten der Eltern gemäß § 65a Absatz 1 Nummer 5 des Brandenburgischen Schulgesetzes sind unmittelbar nach Beendigung des Schulverhältnisses zu löschen.
Sofern Daten gemäß § 65a Absatz 1 Satz 1 Nummer 8 und 9 des Brandenburgischen Schulgesetzes von Grundschulen verarbeitet werden, sind diese zwei Jahre nach der Aufnahme zu löschen.
Sind diese Daten in diesem Zeitraum nicht mehr erforderlich, sind sie für Zwecke der Statistik zu anonymisieren.

#### § 15  
Schülerlaufbahnstatistiken

(1) Im Rahmen der Schuldatenerhebung gemäß § 13 Absatz 1 sind für Auswertungen für das für Schule zuständige Ministerium Schülerlaufbahnstatistiken zu Zwecken der Schulaufsicht, der Schulverwaltung und der Qualitätssicherung zu erstellen.
Das vom für Schule zuständigen Ministerium beauftragte Amt für Statistik oder eine andere den Grundsätzen des Statistikgesetzes verpflichtete Stelle erstellt aus den Datensätzen der Schuldatenerhebung Datensätze, um einzelne schulische Bildungsverläufe darzustellen.
Für die Schülerlaufbahndatensätze dürfen keine Daten zusätzlich zu den Daten verarbeitet werden, die gemäß § 65a Absatz 2 Satz 3 Nummer 1 bis 5 des Brandenburgischen Schulgesetzes bestimmt sind.

(2) Nach Überprüfung der Erhebungsmerkmale auf Schlüssigkeit und Vollständigkeit ist von dem beauftragten Amt für Statistik oder einer anderen den Grundsätzen des Statistikgesetzes verpflichteten Stelle für jeden Erhebungsdatensatz durch Einwegverschlüsselung oder andere dem neuesten Stand der Technik entsprechende Verfahren ein Schülerlaufbahndatensatz zu erzeugen, um einen Rückschluss auf die konkrete Person nicht zuzulassen.
Die Aufgaben der datenschutzrechtlich besonders verpflichteten Personen gemäß § 13 Absatz 5 bleiben unberührt.
Anschließend sind die Hilfsmerkmale zu löschen.
Schülerlaufbahndatensätze, die nach Entscheidung des für Schule zuständigen Ministeriums insbesondere für wissenschaftliche Zwecke an andere Einrichtungen oder Stellen übermittelt werden, sind zu anonymisieren.

### Abschnitt 2  
Datenschutz in Schulbehörden und nachgeordneten Einrichtungen

#### § 16  
Staatliche Schulämter

(1) Die staatlichen Schulämter dürfen personenbezogene Daten von Schülerinnen und Schülern sowie von deren Eltern gemäß § 65 Absatz 3 des Brandenburgischen Schulgesetzes verarbeiten.
Für die Einhaltung des Datenschutzes gemäß § 11 ist die Leiterin oder der Leiter des staatlichen Schulamtes verantwortlich.
Sie oder er gibt Hinweise zur Datenverarbeitung und benennt den Datenschutzbeauftragten gemäß Artikel 37 Absatz 1 der Verordnung (EU) 2016/679.

(2) Die staatlichen Schulämter dürfen personenbezogene Daten bereits vor der erstmaligen Begründung eines Schulverhältnisses verarbeiten, soweit diese zur Sicherung

1.  des Aufnahmeverfahrens in die Grundschule,
2.  der Sprachstandsfeststellung sowie der Teilnahme an Sprachförderung und
3.  schulärztlicher Untersuchungen gemäß § 37 Absatz 1 des Brandenburgischen Schulgesetzes

erforderlich sind.

(3) Die staatlichen Schulämter dürfen zur Erfüllung ihrer Aufgaben als Dienststelle für Lehrkräfte und sonstiges pädagogisches Personal deren personenbezogene Daten gemäß den arbeits- und beamtenrechtlichen Vorschriften verarbeiten.

#### § 17  
Schulpsychologinnen und Schulpsychologen

(1) Schulpsychologinnen und Schulpsychologen dürfen durch Einsicht in die Schülerakte gemäß § 10 Absatz 2 und 3 personenbezogene Daten erheben, sofern dies zur Erstellung einer schulpsychologischen Stellungnahme oder eines schulpsychologischen Befundes insbesondere in Vorbereitung von Entscheidungen über

1.  einen sonderpädagogischen Förderbedarf oder Fördermaßnahmen zur Behebung von Lernschwierigkeiten,
2.  die Anwendung von Erziehungs- und Ordnungsmaßnahmen oder
3.  die Leistungsmessung

notwendig ist.
Die betroffenen Personen sind im Vorfeld auf die Rechtsgrundlage hinzuweisen.
Darüber hinaus dürfen personenbezogene Daten der betroffenen Personen nur mit deren Einwilligung oder bei Minderjährigen mit Einwilligung der Eltern verarbeitet werden.

(2) Eine Übermittlung der Ergebnisse schulpsychologischer Untersuchungen mit den in ihnen enthaltenen personenbezogenen Daten erfolgt an die Stelle, welche die schulpsychologische Stellungnahme oder den schulpsychologischen Befund angefordert hat.
An andere öffentliche Stellen erfolgt sie entsprechend den Bestimmungen gemäß § 7.
Die Übermittlung kann nur erfolgen, wenn die anfordernde Stelle eine Erhebungsbefugnis für die angeforderten Daten hat und in ihrer Anforderung benennt.

(3) Die in Stellungnahmen, Befunden und Empfehlungen festgehaltenen Ergebnisse schulpsychologischer Beratungen sollen in der Regel Datum, Namen der Ratsuchenden und weiterer Gesprächsteilnehmender, Beratungsanlass, Gesprächsverlauf und Maßnahmen enthalten.
Diese Aufzeichnungen sind im staatlichen Schulamt unter Berücksichtigung der Datenschutzmaßnahmen gemäß § 11 bis zum Ablauf von drei Jahren nach Beendigung der Schulpflicht der Schülerin oder des Schülers zu verwahren.

### Abschnitt 3  
Schlussbestimmungen

#### § 18  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
August 2011 in Kraft.
Gleichzeitig tritt die Datenschutzverordnung Schulwesen vom 14.
Mai 1997 (GVBl.
II S.
402), die zuletzt durch Verordnung vom 12.
November 2010 (GVBl.
II Nr. 76) geändert worden ist, außer Kraft.

Potsdam, den 15.
August 2012

Die Ministerin für Bildung,  
Jugend und Sport

Dr.
Martina Münch

**Anlage 1**

**Liste der zur Verarbeitung zugelassenen personenbezogenen Daten**

1

Schülerakte

 

Bei Aufnahme einer Schülerin oder eines Schülers sind in einer Schülerakte folgende für die Schullaufbahn sowie die schul- und schulträgerinterne Verwaltung erforderlichen Daten aufzunehmen.

1.1

Schülerstammblatt und die darin enthaltenen Einzeldaten gemäß den Anlagen 2 bis 4

1.2

Durchschriften oder Kopien aller Zeugnisse und des Grundschulgutachtens einschließlich der Gesprächsprotokolle sowie die für die Beurteilung des Arbeits- und Sozialverhaltens erforderlichen Unterlagen

1.3

Kopien der Benachrichtigungen der Eltern bei gefährdeter Versetzung

1.4

Anmeldeunterlagen, Angaben zu Geschwisterkindern, Gesprächsprotokolle, Aufnahmeentscheidungsunterlagen und Angaben über den Schulabgang

1.5

Beurlaubungen vom Schulbesuch1, 4

1.6

Belege über Schulversäumnisse wegen Krankheit oder aus sonstigen Gründen4

1.7

Unterlagen über angeordneten/erteilten Haus-/Krankenhausunterricht4

1.8

Angaben zu gesundheitlichen Beeinträchtigungen soweit gemäß § 1 Absatz 2 zulässig2, 4

1.9

Kopien der Schulbescheinigungen für BAföG und BbgAföG4

1.10

Schriftverkehr zu Schulpflichtverletzungen4

1.11

Unterlagen über eingeleitete und erteilte Ordnungsmaßnahmen und Erziehungsmaßnahmen gemäß § 3 Absatz 2 Nummer 2, 5 und 6 der Verordnung über Konfliktschlichtung, Erziehungs- und Ordnungsmaßnahmen4

1.12

Kopien von Schulbescheinigungen und ausgegebenen Schülerausweisen

1.13

Bildungsempfehlung und Unterlagen eines Förderausschussverfahrens3, 4

1.14

Befreiungen vom Unterricht, insbesondere Befreiung vom Sportunterricht und von Lebensgestaltung-Ethik-Religionskunde

1.15

Angaben zur Schwimmfähigkeit der Schülerinnen und Schüler

1.16

Bildungsvereinbarungen gemäß § 44 Absatz 6 BbgSchulG

1.17

in Schulen der Primarstufe

 

1.17.1

Unterlagen über die Zurückstellung vom Schulbesuch

 

1.17.2

Unterlagen über die vorzeitige Aufnahme in die Grundschule

 

1.17.3

Unterlagen über die individuelle Lernstandsanalyse

 

1.17.4

individueller Förderplan für den zusätzlichen Förderunterricht

 

1.17.5

Angaben zur Teilnahme an Sprachstandsfeststellungen und Sprachförderung sowie Teilnahmebefreiungen

1.18

in der Sekundarstufe I

 

1.18.1

Unterlagen über die Wahl der Prüfungsfächer und freiwilliger Zusatzprüfungen einschließlich der erforderlichen Protokolle

 

1.18.2

Unterlagen über die Durchführung der Prüfungen

 

1.18.3

Unterlagen über die Ermittlung der Gesamtqualifikation

 

1.18.4

Unterlagen über das Praxislernen und das Schülerbetriebspraktikum

 

1.18.5

Ergebnisse diagnostischer Testverfahren

1.19

in Schulen der gymnasialen Oberstufe

 

1.19.1

Unterlagen über die Wahl von Kursen, Klausur- und Abiturprüfungsfächern einschließlich der erforderlichen Protokolle

 

1.19.2

Unterlagen über den Erwerb besonderer Berechtigungen (insbesondere Erwerb des Latinums oder Graecums)

 

1.19.3

Unterlagen über die Durchführung der Prüfungen

 

1.19.4

Unterlagen über die Ermittlung der Gesamtqualifikation

1.20

in Einrichtungen des Zweiten Bildungsweges

 

1.20.1

Angaben zur schulischen und beruflichen Qualifikation

 

1.20.2

Angaben zur beruflichen Tätigkeit einschließlich Anschrift der Arbeits- oder Ausbildungsstätte

 

1.20.3

Unterlagen über den Erwerb besonderer Berechtigungen (insbesondere Erwerb des Latinums oder Graecums)

 

1.20.4

Unterlagen über die Wahl von Kursen, Klausur- und Abiturprüfungsfächern einschließlich der erforderlichen Protokolle (gilt nur für Bildungsgänge der Sekundarstufe II)

 

1.20.5

Unterlagen über die Ermittlung der Gesamtqualifikation

1.21

Angaben zur Teilnahme an schulärztlichen und schulzahnärztlichen Untersuchungen

1.22

Nachweis gemäß § 20 Absatz 9 des Infektionsschutzgesetzes, soweit nach dem Infektionsschutzgesetz eine Verpflichtung besteht (Masernschutz)

**2**

**Klassen- oder Kursbücher**

2.1

das Stundenthema der erteilten Unterrichtsstunden in der Klasse oder dem Kurs

2.2

Thema und Zeitpunkt aller in der Klasse oder dem Kurs angefertigten schriftlichen Arbeiten

2.3

einen Nachweis über die Tage und Stunden der Unterrichtsversäumnisse einzelner Schülerinnen und Schüler4

2.4

Unterlagen über eingeleitete und erteilte Erziehungsmaßnahmen gemäß § 3 Absatz 2 Nummer 1, 3, 7 und 8 der Verordnung über Konfliktschlichtung, Erziehungs- und Ordnungsmaßnahmen4

2.5

Nachweise über Belehrungen mit Angabe des Alters der Schülerinnen und Schüler und fehlender Schülerinnen und Schüler, insbesondere

 

2.5.1

zum Verhalten auf dem Schulgelände

 

2.5.2

zum Verhalten im Sportunterricht

 

2.5.3

zum Verhalten im naturwissenschaftlichen Unterricht

 

2.5.4

über die Brandschutzordnung und die Verhaltensregeln bei Notfällen

 

2.5.5

über das Verhalten im Straßenverkehr

 

2.5.6

über das Verhalten bei Unterrichtsgängen und Schulfahrten

 

2.5.7

über das Verhalten bei Witterungseinflüssen

 

2.5.8

über die Gefahren im Umgang mit pyrotechnischen Erzeugnissen und Waffen

 

2.5.9

über das Verhalten beim Auffinden von Munition

2.6

die Liste der in der Klasse oder dem Kurs zu unterrichtenden Schülerinnen und Schüler, der jeweilige Klassen-/Kursname, die Art der Klasse/des Kurses/der Gruppe und die Anzahl der in den jeweiligen Unterrichtsfächern erteilten Wochenstunden

2.7

Name, Fach und Personalkennzeichen der unterrichtenden Lehrkräfte

**3**

**Notenbuch**

3.1

Schülerliste mit Wohnanschrift und Telefonnummern der Eltern

3.2

für jedes Fach getrennt Seiten oder Blätter für die Eintragung der Noten/Punktbewertung sowie je eine Spalte für Schulhalbjahres- und Schuljahresnoten oder -punkte

3.3

Name der Lehrkraft, die das jeweilige Fach unterrichtet

**4**

**Prüfungsunterlagen**

4.1

Wahl von Prüfungsfächern und Protokolle dazugehöriger Beratungsgespräche

4.2

Unterlagen über Zulassung, Zeitpunkt und Art von Prüfungen

4.3

schriftliche Prüfungsarbeiten mit Aufgabenstellung und Bewertung

4.4

Unterlagen der mündlichen und praktischen Prüfung

**5**

**Sonstige personenbezogene Daten von Schülerinnen, Schülern und Eltern**

5.1

Mitgliedschaft und Funktion in Gremien der Mitwirkung

5.2

sonstige schul- oder ausbildungsbezogene Funktionen, insbesondere Schülerlotsen, AG-Leitung, Auszubildendenvertretung

5.3

Protokolle schulischer Gremien der Mitwirkung soweit sie Angaben über Entscheidungen zu einzelnen Personen enthalten

5.4

Schulname, Schulform, Anschrift und Bundesland bisher von der Schülerin oder dem Schüler besuchter Schulen

5.5

Angaben über den Besuch eines Hortes

5.6

Angaben über die Teilnahme an der Schulspeisung

5.7

Angaben über ausgeliehene Lernmittel

5.8

Angaben über den Schulweg, die Teilnahme an der Schülerbeförderung und die Höhe des Beitrags zur Schülerbeförderung4

5.9

Angaben über die Teilnahme an Fördermaßnahmen sowie die Art des Förderschwerpunktes und die Art der sonderpädagogischen Förderung4

5.10

Angaben über die Teilnahme am Ganztagsbetrieb

5.11

Angaben über die Teilnahme an Maßnahmen im Rahmen der Begabungsförderung

5.12

Unterlagen über meldepflichtige Unfälle4

5.13

Angaben zur Inanspruchnahme von Leistungen aus dem Schulsozialfonds4

5.14

Angaben über die Befreiung vom Eigenanteil für Lernmittel4

5.15

Unterlagen über Beschwerdeverfahren, Widerspruchsverfahren und gerichtliche Verfahren4

5.16

Angaben zur Verkehrssprache in der Familie

5.17

Unterbringung im Wohnheim/Internat

5.18

Angaben zu Lese-, Rechtschreib- oder Rechenschwäche

**6**

**Unterlagen über Lehrkräfte und das sonstige pädagogische Personal**

6.1

Stammblatt gemäß Anlage 54

6.2

Sonstige personenbezogene Daten4

 

6.2.1

Mitgliedschaft in Gremien der Mitwirkung4

 

6.2.2

Angaben zum Einsatz in der Schule (Unterrichtseinsatz, Klassenleiterfunktion und sonstige schulische Aufgaben)4

 

6.2.3

Angaben über erteilten und nicht erteilten Unterricht einschließlich Angaben zu Mehrarbeit4

 

6.2.4

Angaben zur Abwesenheit von der Schule4

 

6.2.5

Angaben über erfolgte Verpflichtungen und Belehrungen4

 

6.2.6

Angaben zu Rettungsschwimmer- und Ersthelferausbildung4

**7**

**Klassenarbeiten und Klausuren**

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
1 Beurlaubungen und Schulversäumnisse unter zwei Monaten sind lediglich im Klassen- oder Kursbuch gemäß Nummer 2.3 zu erfassen.  
2 Eine automatisierte Verarbeitung ist unzulässig.  
3 Diese Unterlagen sind in einem verschlossenen Umschlag aufzubewahren.  
4 Eine Verarbeitung außerhalb der Schule ist unzulässig.
Dies gilt nicht, soweit eine automatisierte Verarbeitung dieser Daten im Rahmen zentraler Fachverfahren erfolgt und insbesondere durch geeignete Datenschutzmaßnahmen gemäß § 11 und durch Vereinbarungen gemäß Artikel 28 Absatz 3 der Verordnung (EU) 2016/679 der Schutz personenbezogener Daten ausreichend gewährleistet ist.

**Anlage 2**

**Schülerstammblatt allgemeinbildende Schule (Grundschule, Oberschule, Gesamtschule, Gymnasium, berufliches Gymnasium, Förderschule, Schule des Zweiten Bildungsweges)**

1.

Kopfbereich

 

a.

Name der Schule, Schulform, Schulnummer (mit Historie)

 

b.

Tag der Aufnahme in die Schule

 

c.

Einzugliederndenmerkmal4

**2.
**

**Schülerindividualdaten**

 

a.

Name

 

b.

Vorname

 

c.

Geschlecht

 

d.

Anschrift und Kontaktdaten (mit Historie)

 

e.

Herkunftsregion

**3.
**

**Individualdaten der Eltern**

 

a.

Name der Eltern und der jeweilige Status der Sorgeberechtigung

 

b.

Anschrift und Kontaktdaten (mit Historie)

**4.
**

**Schullaufbahndaten**

 

a.

bisher erreichter Abschluss bzw.
Gleichwertigkeitsfeststellung

 

b.

Angaben zur Belegung von Fremdsprachen/Sorbisch (Wendisch) mit Angabe von Jahrgangsstufen, in denen die jeweilige Fremdsprache belegt war

 

c.

Kurswahl im Wahlpflichtbereich, differenziert nach Jahrgangsstufen

 

d.

Kurseinstufung in integrativen Oberschulen und in Gesamtschulen (fachscharfe Angaben pro Halbjahr ab 7/I)4

 

e.

Übersicht über Vollzeitschuljahre (bei PS/Sek I):

 

 

i.

Nummer des Vollzeitschuljahres (1-10)

 

 

ii.

Schuljahr, in dem das Vollzeitschuljahr belegt wurde

 

 

iii.

Jahrgangsstufe, die in dem Vollzeitschuljahr besucht wurde

 

 

iv.

Bildungsgang, der in dem Vollzeitschuljahr belegt wurde

 

f.

Teilnahme am bzw.
Befreiung vom Unterricht im Fach Lebensgestaltung-Ethik-Religionskunde (schuljahresscharfe Aufzählung)

 

g.

Teilnahme am Religions- oder Weltanschauungsunterricht (schuljahresscharfe Aufzählung)4

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
4 Eine Verarbeitung außerhalb der Schule ist unzulässig.
Dies gilt nicht, soweit eine automatisierte Verarbeitung dieser Daten im Rahmen zentraler Fachverfahren erfolgt und insbesondere durch geeignete Datenschutzmaßnahmen gemäß § 11 und durch Vereinbarungen gemäß Artikel 28 Absatz 3 der Verordnung (EU) 2016/679 der Schutz personenbezogener Daten ausreichend gewährleistet ist.

**Anlage 3**

**Schülerstammblatt Berufsschule (duale Ausbildung)**

**1.
**

**Kopfbereich**

 

a.

Name der Schule, Schulnummer

 

b.

Tag der Aufnahme in die Schule

 

c.

Gastschülermerkmal

 

d.

Einzugliederndenmerkmal4

 

e.

Ausbildungsberuf

 

f.

Fachrichtung

 

g.

Schlüssel-Nummer laut Schlüsselverzeichnis „g“

 

h.

Art der Ausbildung

**2.
**

**Schülerindividualdaten**

 

a.

Name

 

b.

Vorname

 

c.

Geschlecht

 

d.

Anschrift und Kontaktdaten (mit Historie)

 

e.

Herkunftsregion

**3.
**

**Individualdaten der Eltern**

 

a.

Name der Eltern und der jeweilige Status der Sorgeberechtigung

 

b.

Anschrift und Kontaktdaten (mit Historie)

**4.
**

**Schullaufbahndaten**

 

a.

bisher erreichter Abschluss bzw.
Gleichwertigkeitsfeststellung

 

b.

Art der Schuleinrichtung (Datum des Wechsels, abgebendes OSZ, aufnehmendes OSZ, Klasse, Grund) (mit Historie)

 

c.

Umschülermerkmal

 

d.

Merkmal Ausbildungsvertrag hat vorgelegen

 

e.

Angaben zu den Ausbildungsstätten (mit Historie)

 

 

i.

Name, Anschrift und Kontaktdaten der Ausbildungsstätten

 

 

ii.

Merkmal Ausbildung als

 

 

iii.

Tag des Eintritts in die und des Austritts aus den Ausbildungsstätten

 

f.

Teilnahme am bzw.
Befreiung vom Unterricht im Fach Lebensgestaltung-Ethik-Religionskunde (schuljahresscharfe Aufzählung)

 

g.

Teilnahme am Religions- oder Weltanschauungsunterricht (schuljahresscharfe Aufzählung)4

**5.
**

**Leistungen und Schulbesuch**

 

a.

Unterrichtsfächer

 

b.

fachbezogene Halbjahresnoten und Endnoten

 

c.

Bemerkungen zu den jeweiligen Schulhalbjahren

 

d.

Unterrichtsversäumnisse (versäumte Tage und Einzelstunden)4

 

e.

Angaben über ausgeliehene Lernmittel

 

f.

Angaben über die Ausstellung von Schülerausweisen

 

g.

Angaben über die Ausstellung von Zeugnissen

 

h.

Angabe zur Aushändigung des Abgangs- bzw.
Abschlusszeugnisses

 

i.

Angabe zu Beginn und Ende des Schulbesuchs

 

j.

Angabe zum erfolgreichen Abschluss der Berufsausbildung

 

k.

Ausbildungsberuf

 

l.

Angaben zu zusätzlich besuchten allgemeinbildenden Kursen und Englischkursen

 

m.

Name der besuchten Schule und Schulkurzzeichen

 

n.

erreichter Abschluss einschließlich Gleichwertigkeitsfeststellung

 

o.

Bemerkungen

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
4 Eine Verarbeitung außerhalb der Schule ist unzulässig.
Dies gilt nicht, soweit eine automatisierte Verarbeitung dieser Daten im Rahmen zentraler Fachverfahren erfolgt und insbesondere durch geeignete Datenschutzmaßnahmen gemäß § 11 und durch Vereinbarungen gemäß Artikel 28 Absatz 3 der Verordnung (EU) 2016/679 der Schutz personenbezogener Daten ausreichend gewährleistet ist.

**Anlage 4**

Schülerstammblatt Berufsschule, Fachoberschule, Berufsfachschule, Fachschule

1.

Kopfbereich

 

a.

Name der Schule, Schulnummer

 

b.

Tag der Aufnahme in die Schule

 

c.

Gastschülermerkmal

 

d.

Einzugliederndenmerkmal4

 

e.

Merkmal Orientierung/Vorbereitung (Vollzeit/Teilzeit)

 

f.

Merkmal berufliche Grundbildung (Vollzeit/Teilzeit)

 

g.

Merkmal Fachoberschule (Vollzeit/Teilzeit)

 

h.

Merkmal berufliche Grundbildung (Vollzeit/Teilzeit)

 

i.

Merkmal berufliche Teilqualifikation (Vollzeit/Teilzeit)

 

j.

Merkmal Assistentenberufe (Vollzeit/Teilzeit)

 

k.

Merkmal Fachschule (Vollzeit/Teilzeit)

**2.
**

**Schülerindividualdaten**

 

a.

Name

 

b.

Vorname

 

c.

Geschlecht

 

d.

Anschrift und Kontaktdaten (mit Historie)

 

e.

Herkunftsregion

**3.
**

**Individualdaten der Eltern**

 

a.

Name der Eltern und der jeweilige Status der Sorgeberechtigung

 

b.

Anschrift und Kontaktdaten (mit Historie)

**4.
**

**Schullaufbahndaten**

 

a.

bisher erreichter Abschluss bzw.
Gleichwertigkeitsfeststellung

 

b.

Angaben zum Wechsel des OSZ bei Verbleib im Bildungsgang (Datum des Wechsels, abgebendes OSZ, aufnehmendes OSZ, Klasse, Grund) (mit Historie)

 

c.

Angaben zu Praktika (mit Historie)

 

 

i.

Name der Praktikumsstätte

 

 

ii.

Anschrift und Kontaktdaten der Praktikumsstätte

 

 

iii.

Angabe zur Praktikumstätigkeit

 

 

iv.

Beginn- und Enddatum des Praktikums

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
4 Eine Verarbeitung außerhalb der Schule ist unzulässig.
Dies gilt nicht, soweit eine automatisierte Verarbeitung dieser Daten im Rahmen zentraler Fachverfahren erfolgt und insbesondere durch geeignete Datenschutzmaßnahmen gemäß § 11 und durch Vereinbarungen gemäß Artikel 28 Absatz 3 der Verordnung (EU) 2016/679 der Schutz personenbezogener Daten ausreichend gewährleistet ist.

**Anlage 5**

**Stammblatt für Lehrkräfte und das sonstige pädagogische Personal an Schulen  
**(_Für das sonstige pädagogische Personal ist das Stammblatt nur insoweit auszufüllen, als die Angaben auch auf dieses zutreffen.)_

angelegt am

(anzulegen für Lehrkräfte, die länger als drei Monate an der Schule tätig sind)

Name der Schule

Fremdlehrkraft

Schulnummer

Personalnummer

**Allgemeine Angaben**

Name

Geburtsname

Vornamen1

Namenskürzel

Geburtsdatum

Staatsangehörigkeit

Datum des Eintritts in den Schuldienst

Schwerbehinderung und Grad

Anschrift

PLZ

Wohnort

Telefon/E-Mail

Straße/Nr.

Kreis

Änderung der Anschrift:

PLZ

Wohnort

Telefon/E-Mail

Straße/Nr.

Kreis

Änderung der Anschrift:

PLZ

Wohnort

Telefon/E-Mail

Straße/Nr.

Kreis

**Lehrbefähigung und Funktionen/Sonderaufgaben**

Amtsbezeichnung

Ausbildung

Lehrbefähigung in Fächern

Funktion

von/bis

Funktion

von/bis

Funktion

von/bis

Funktion

von/bis

**Tätigkeit an anderen Schulen**

von/bis

von/bis

von/bis

von/bis

von/bis

von/bis

von/bis

von/bis

Umfang

Umfang

Umfang

Umfang

Umfang

Umfang

Umfang

Umfang

**Fort- und Weiterbildung**2

von/bis

von/bis

von/bis

von/bis

von/bis

von/bis

von/bis

von/bis

Fach

Fach

Fach

Fach

Fach

Fach

Fach

Fach

**Pflichtstundenumfang an der Schule/Anrechnungen (ggf.
Beiblatt benutzen)**

Schuljahr

Pflichtstundenzahl an der Schule3

Anrechnungen

 

von/bis

von/bis

Arten

Umfang

 

Änderungen (mit Datum)

 

von/bis

von/bis

Arten

Umfang

 

Änderungen (mit Datum)

 

von/bis

von/bis

Arten

Umfang

 

Änderungen (mit Datum)

 

von/bis

von/bis

Arten

Umfang

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
1 Rufname ist zu unterstreichen.  
2 Angaben zu privat organisierter Fortbildung sind freiwillig.  
3 als Anteil vom Pflichtstundensoll bei Vollbeschäftigung; geplante Mehrarbeit gesondert angeben

**Anlage 6**

**Personenbezogene Merkmale für Schuldatenerhebungen**

1.

Vor- und Familienname (Hilfsmerkmal), landeseindeutige Schülernummer (Hilfsmerkmal)

2.

Schulnummer, Abteilungsnummer (OSZ)

3.

Geburtsdatum, Geburtsort, Geburtsland, Geschlecht, Zurückstellung vom Schulbesuch, vorzeitige Aufnahme des Schulbesuchs, Jahr der Ersteinschulung, Datum der Aufnahme an der Schule, Wohnort

4.

Staatsangehörigkeit, Migrantenstatus (Ausländer, Aussiedler, Asylbewerber, Flüchtling), Einzugliederndeneigenschaft, Herkunftsland, Herkunftsregion (Bundesland, Landkreis/kreisfreie Stadt), Herkunftsschule (Schulnummer bei Schulwechsel in Brandenburg, Anschrift bei Schulen außerhalb Brandenburgs), Herkunftsschulform, nichtdeutsche Verkehrssprache in der Familie, Jahr des Zuzugs nach Deutschland

5.

Schulform, Jahrgang des aktuellen Schuljahres, Klassenname der besuchten Klassen, Kursname der besuchten Kurse, Bildungsgang, Zeitform, Art der Gruppe/des Kurses, Personalkennzeichen der Lehrkraft, Unterrichtsfach, erteilte Wochenstunden, Status Fremdsprache, Umschüler, Empfehlung der Grundschule, bestandene Eignungsprüfung für Gymnasien, Teilnahme an den einzelnen Unterrichtseinheiten, Unterrichtsbefreiungen (Lebensgestaltung-Ethik-Religionskunde, Sport), Teilnahme am Religions- oder Weltanschauungsunterricht, bilinguale Unterrichtssprache, Fachklasse/Beruf, Berufsfeld, Fachrichtung bei Berufsausbildung, Art des Ausbildungsvertrags, Sitz der Ausbildungsstätte

6.

Teilnahme an der Schulspeisung, Teilnahme am Ganztagsbetrieb, Teilnahme an der Schülerbeförderung, Länge des Schulweges, Teilnahme am Hort, Unterbringung im Wohnheim/Internat

7.

Art der sonderpädagogischen Förderung, schwere Mehrfachbehinderung, Haus-/Krankenhausunterricht, Teilnahme an Fördermaßnahmen, Art des Förderschwerpunkts, Lese-/Rechtschreibschwäche, Rechenschwäche

8.

Jahrgang des vorangegangenen Schuljahres, Bildungsgang des vorangegangenen Schuljahres, bisher erreichter höchster schulischer Abschluss, bisher erreichter berufsbezogener Abschluss, Jahr des letzten Schulbesuches, Art der Wiederholung

9.

Unterrichtsversäumnisse

10.

Art des Abschlusses, Art des Abgangs (versetzt/aufgerückt, nicht versetzt, querversetzt, freiwillige Wiederholung, Schulwechsel), Art des Zeugnisses, Nichtschülerprüfung, schulische Vorbildung der Absolventen/Abgänger, Abgänger vor/nach der Vollendung der Vollzeitschulpflicht

11.

Beurteilung des Arbeits- und Sozialverhaltens, Befreiung vom Eigenanteil für Lernmittel, Leistungen aus dem Schulsozialfonds, Höhe des Beitrags zur Schülerbeförderung

12.

Abitur:  
Abiturprüfungsfächer, erreichte Punktzahl bzw.
Note, zusätzliche mündliche Prüfungen, Grund für die zusätzliche Prüfung, Wiederholungsprüfung, erreichte Punktzahl bzw.
Note, Kursergebnisse in der Qualifikationsphase, erreichte Punktzahl bzw.
Note, Prüfungsergebnisse, erreichte Punktzahl bzw.
Note, Gesamtqualifikation, erreichte Punktzahl bzw.
Note

13.

Prüfung Klasse 10:  
Prüfungsfächer, erreichte Punktzahl bzw.
Note, Wahl der Aufgaben, erreichte Punktzahl bzw.
Note je Aufgabe, Kursbelegung, Jahresnoten in Fächern des vorletzten und letzten Schuljahres

14.

ZVA 6:  
Fächer der zentralen Arbeiten, erreichte Punktzahl bzw.
Note, Wahl der Aufgaben, erreichte Punktzahl bzw.
Note je Aufgabe, Jahresnoten in Fächern des vorletzten und letzten Schuljahres

15.

Erhebungsmerkmale der Zentralen Schülerdatei

16.

Fachhochschulreifeprüfungen (berufliche Schulen), FHR-Prüfungsfächer, Vornoten, Prüfungsnoten, Abschlussnoten, Gesamtqualifikation

**Anlage 7**

[Antrag auf Genehmigung der Datenverarbeitung außerhalb der Schule gemäß § 5 Absatz 1](/br2/sixcms/media.php/68/DSV-Anlage-7.pdf)

**Anlage 8**

[Musteranweisung für die Verarbeitung personenbezogener Daten in Schulen](/br2/sixcms/media.php/68/DSV-Anlage-8.pdf)

**Anlage 9**

**APSIS-Daten der Lehrkräfte, die aus dem Personalverwaltungsprogramm mit den Daten der Schule zusammengeführt werden**

1.

Personalnummer

2.

Geburtsdatum

3.

Geschlecht

4.

Schulnummer der Stammschule

5.

Sonstiges pädagogisches Personal (J/N)

6.

Qualifikation

7.

Ausbildungsfächer, Lehrbefähigungen

8.

Staatsangehörigkeit

9.

Pflichtstunden bei Vollbeschäftigung

10.

Unbefristete Stunden (Arbeitsumfang)

11.

Befristete Stunden (Arbeitsumfang)

12.

Ermäßigungsstunden

13.

Gründe für längere Abwesenheit

14.

Altersteilzeit

15.

Dienstverhältnis (Angestellte/Beamte)