## Verordnung über die Ausbildung und Prüfung für die Laufbahn des allgemeinen Vollzugsdienstes im Justizvollzug des Landes Brandenburg (Ausbildungs- und Prüfungsordnung allgemeiner Vollzugsdienst - APOaVD)

Auf Grund des § 26 Absatz 1 Satz 1 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl. I S. 26), der durch Artikel 1 des Gesetzes vom 5.
Dezember 2013 (GVBl.
I Nr. 36) geändert worden ist, verordnet die Ministerin der Justiz im Einvernehmen mit dem Minister des Innern und für Kommunales und der Ministerin der Finanzen und für Europa:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeine Vorschriften

[§ 1 Ziel und Grundlagen der Ausbildung](#1)

[§ 2 Einstellungsvoraussetzungen](#2)

[§ 3 Zuständigkeit für die Ausbildung](#3)

### Abschnitt 2  
Vorbereitungsdienst

[§ 4 Dauer und Gliederung des Vorbereitungsdienstes](#4)

[§ 5 Fachtheoretische Ausbildung](#5)

[§ 6 Praktische Ausbildung](#6)

[§ 7 Ausbildungsbegleitende Konferenzen](#7)

[§ 8 Berufspraktische Lernzielkontrolle](#8)

[§ 9 Bewertung der Leistungen](#9)

[§ 10 Verlängerung des Vorbereitungsdienstes](#10)

[§ 11 Entlassung aus dem Vorbereitungsdienst](#11)

### Abschnitt 3  
Laufbahnprüfung

[§ 12 Zulassung zur Prüfung](#12)

[§ 13 Prüfungskommission](#13)

[§ 14 Prüfungsverfahren](#14)

[§ 15 Schriftliche Prüfung](#15)

[§ 16 Mündliche Prüfung](#16)

[§ 17 Gesamtnote und Zeugniserteilung](#17)

### Abschnitt 4  
Übergangs- und Schlussvorschriften

[§ 18 Übergangsvorschriften](#18)

[§ 19 Inkrafttreten, Außerkrafttreten](#19)

### Abschnitt 1  
Allgemeine Vorschriften

#### § 1 Ziel und Grundlagen der Ausbildung

(1) Die Ausbildung versetzt die Justizvollzugshauptsekretäranwärterinnen und -anwärter (im Folgenden Anwärterinnen und Anwärter) in einem Theorie und Praxis verbindenden Ausbildungsgang in die Lage, den beruflichen Anforderungen des allgemeinen Vollzugsdienstes gerecht zu werden und mit hoher fachlicher, sozialer und persönlicher Kompetenz zu handeln.

(2) Dazu gehören insbesondere die Fähigkeit und Bereitschaft, eigenverantwortlich und in Zusammenarbeit mit den anderen im Vollzug Tätigen

1.  die Behandlung, Betreuung und Versorgung der Gefangenen durchzuführen,
2.  die Sicherheit und Ordnung in der Anstalt zu gewährleisten,
3.  in schwierigen Situationen Problemlösungsstrategien zu entwerfen und deeskalierend zu wirken,
4.  sich mit hoher Motivation und Flexibilität den Anforderungen im Justizvollzug zu stellen sowie
5.  sich eigeninitiativ weiterzuentwickeln und fortzubilden.

(3) Die Ausbildung muss sich an dem gesetzlich festgelegten Vollzugsziel und den Aufgaben des Vollzugs ausrichten, den gesellschaftlichen Veränderungen Rechnung tragen und neue wissenschaftliche Erkenntnisse im Zusammenhang mit der Resozialisierung von Straf- und Jugendstrafgefangenen berücksichtigen.
Der interdisziplinären Gestaltung der Ausbildung kommt hierbei eine besondere Bedeutung zu.

#### § 2 Einstellungsvoraussetzungen

(1) Zum Vorbereitungsdienst der Laufbahn des mittleren allgemeinen Vollzugsdienstes bei Justizvollzugsanstalten, kann von der jeweiligen Dienstbehörde zugelassen werden, wer die gesetzlichen und laufbahnrechtlichen Voraussetzungen für die Berufung in das Beamtenverhältnis auf Widerruf erfüllt und durch ein Eignungsfeststellungs- und Auswahlverfahren als geeignet befunden wird.

(2) Die Höchstaltersgrenze des § 118 Absatz 2 des Landesbeamtengesetzes gilt nicht für Inhaber eines Eingliederungs- oder Zulassungsscheines sowie in den Fällen des § 7 Absatz 2 des Soldatenversorgungsgesetzes.

#### § 3 Zuständigkeit für die Ausbildung

(1) Die Leitung und Organisation der Ausbildung obliegt der Bildungsstätte für den Justizvollzug (Ausbildungsstelle) in Abstimmung mit den Leitungen der Justizvollzugsanstalten und der Sicherungsverwahrungsvollzugseinrichtung.

(2) Die Fachaufsicht über die Ausbildung erfolgt durch die oberste Dienstbehörde.

(3) Die Ausbildungsstelle setzt im Benehmen mit der obersten Dienstbehörde Lehrkräfte ein, die über umfassende berufliche Erfahrungen in ihren Lehrfächern verfügen und pädagogisch befähigt sind.

(4) Die Justizvollzugsanstalt Brandenburg an der Havel stellt die für die Durchführung der Ausbildung erforderlichen technischen und räumlichen Ressourcen zur Verfügung.

### Abschnitt 2  
Vorbereitungsdienst

#### § 4 Dauer und Gliederung des Vorbereitungsdienstes

(1) Der Vorbereitungsdienst dauert zwei Jahre.
Er gliedert sich in eine im Wechsel stattfindende theoretische und praktische Ausbildung.

(2) Umfang und Gliederung der einzelnen Ausbildungsabschnitte und Lehrgebiete werden in einem Rahmenplan geregelt.
Der Rahmenplan wird von der Ausbildungsstelle unter Beteiligung der Justizvollzugsanstalten und der Sicherungsverwahrungsvollzugseinrichtung regelmäßig überprüft und fortgeschrieben.
Er bedarf der Zustimmung der obersten Dienstbehörde.

(3) An einem Lehrgang sollen in der Regel nicht mehr als 20 Anwärterinnen und Anwärter teilnehmen.

#### § 5 Fachtheoretische Ausbildung

(1) Auf der Grundlage des Rahmenplans erstellt die Ausbildungsstelle Stundenpläne, die insbesondere folgende Schwerpunkte umfassen:

1.  Justizvollzugskunde und Vollzugsrecht,
2.  Sozialwissenschaften und politische Bildung,
3.  Rechts- und Verwaltungskunde,
4.  Erweiterung der Allgemeinbildung einschließlich der sprachlichen und interkulturellen Kompetenzen,
5.  Ethik und Menschenbild, Menschenrechte,
6.  Soziales Training, Psychologie,
7.  Einführung in justizspezifische IT-Fachverfahren,
8.  Deeskalationstechniken, Eingriffs- und Sicherungstechniken sowie Sport,
9.  Erste Hilfe und Brandschutz,
10.  Öffentliches Dienstrecht.

(2) Neben Frontalunterricht kann der Lehrstoff auch in Arbeitsgemeinschaften und Projektgruppen vermittelt werden.
Zudem ist er im Selbststudium zu vertiefen.

#### § 6 Praktische Ausbildung

(1) Die praktische Ausbildung findet in den Justizvollzugsanstalten statt.
Sie soll für einen Zeitraum von 12 Wochen, in der Regel im ersten Abschnitt, in einer anderen Justizvollzugsanstalt als der Stammanstalt erfolgen.

(2) Die in der fachtheoretischen Ausbildung erworbenen Kenntnisse und Fähigkeiten werden in unterschiedlichen Aufgabenfeldern des allgemeinen Vollzugsdienstes angewendet, vertieft und erprobt.
In der praktischen Ausbildung werden die Anwärterinnen und Anwärter mit der eigenständigen Wahrnehmung von Aufgaben betraut, soweit der Ausbildungsstand dies zulässt, es die Ausbildung fördert und die erforderliche Anleitung gewährleistet ist.
Eine Beschäftigung lediglich zur Entlastung anderer ist unzulässig.

(3) Die Leiterin oder der Leiter der Justizvollzugsanstalt trägt die Verantwortung für die praktische Ausbildung und beauftragt für deren Organisation und Durchführung im Einvernehmen mit der Ausbildungsstelle eine Bedienstete oder einen Bediensteten mit der Leitung der Ausbildung.
Die Leitung der Ausbildung darf nur Bediensteten übertragen werden, die über pädagogische Kompetenz und umfassende berufliche Erfahrungen im allgemeinen Vollzugsdienst verfügen.
Mit der Unterweisung und Betreuung der Anwärterinnen und Anwärter gemäß einem jeweils in den Justizvollzugsanstalten zu erstellenden Ausbildungsplan sind nur besonders befähigte Bedienstete zu beauftragen.

#### § 7 Ausbildungsbegleitende Konferenzen

(1) Zu Beginn der Ausbildung beruft die Ausbildungsstelle eine Lehrgangskonferenz zur Abstimmung der Lehrinhalte ein, an der zumindest die Lehrkräfte teilnehmen, die mindestens fünf Doppelstunden in den in § 5 Absatz 1 Nummer 1, 2, 3 und 8 genannten Lehrgebieten unterrichten.
Andere Lehrkräfte können ebenfalls teilnehmen.

(2) Nach Abschluss des ersten theoretischen und praktischen Ausbildungsabschnitts wird den Anwärterinnen und Anwärtern eine Rückmeldung über ihre bisherigen Leistungen gegeben.
Dazu findet eine Leistungsbewertungskonferenz statt, die von der Ausbildungsstelle einberufen und geleitet wird.
In der Konferenz wird über die bisher erbrachten Leistungen der Anwärterinnen und Anwärter beraten.
Das Ergebnis der Beratungen teilt die Ausbildungsstelle den Anwärterinnen und Anwärtern schriftlich mit.
Bei etwaigen Mängeln wird ein Vorschlag zur Behebung erteilt.
An der Leistungsbewertungskonferenz nehmen mindestens die Lehrkräfte teil, die die in § 5 Absatz 1 Nummer 1, 2, 3 und 8 genannten Lehrgebiete unterrichten sowie die mit der Leitung der Ausbildung beauftragten Bediensteten.
Andere Lehrkräfte können ebenfalls teilnehmen.

(3) Bei Bedarf kann die Ausbildungsstelle weitere Leistungsbewertungskonferenzen einberufen.
Absatz 2 findet entsprechende Anwendung.

#### § 8 Berufspraktische Lernzielkontrolle

(1) Zum Ende der berufspraktischen Ausbildung wird festgestellt, über welche vollzugspraktischen Kenntnisse und Fähigkeiten die Anwärterinnen und Anwärter verfügen.
Die berufspraktische Lernzielkontrolle erstreckt sich auf die praktische Ausbildung unter Einbeziehung des theoretischen Lehrstoffs.

(2) Die berufspraktische Lernzielkontrolle wird von Vertretern der ausbildenden Justizvollzugsanstalt abgenommen.
Die Ausbildungsstelle kann sich hieran beteiligen.
Die Aufgabenstellung wird von der Justizvollzugsanstalt im Einvernehmen mit der Ausbildungsstelle bestimmt.

(3) Die berufspraktische Lernzielkontrolle ist bestanden, wenn mindestens 4 Punkte erreicht wurden.
Das Prüfungsergebnis ist den Anwärterinnen und Anwärtern mit Begründung bekannt zu geben.

(4) Erscheint eine Anwärterin oder ein Anwärter unentschuldigt nicht zur Lernzielkontrolle oder tritt zurück, so gilt die Prüfung als nicht bestanden.

(5) Im Falle des Nichtbestehens darf die berufspraktische Lernzielkontrolle nur einmal wiederholt werden.
Eine Wiederholung ist nur zum nächsten ordentlichen Termin der Überprüfung der Lernziele möglich.

(6) Eine Wiederholung der berufspraktischen Lernzielkontrolle darf nur erfolgen, wenn ein Ergänzungsvorbereitungsdienst von mindestens sechs Monaten bis maximal einem Jahr abgeleistet wurde.

(7) Der Antrag auf Aufnahme in den Ergänzungsvorbereitungsdienst ist von der Anwärterin oder dem Anwärter binnen eines Monats nach Empfang der schriftlichen Mitteilung über das erstmalige Nichtbestehen bei der obersten Dienstbehörde zu stellen.

(8) Die Entscheidung über die Aufnahme in den Ergänzungsvorbereitungsdienst obliegt der obersten Dienstbehörde.
Die Zulassung zum Ergänzungsvorbereitungsdienst kann versagt werden, wenn die bisherigen Leistungen ein Bestehen der Wiederholungsprüfung nicht erwarten lassen.
Art und Dauer des Ergänzungsvorbereitungsdienstes bestimmt die Leitung der Ernennungsbehörde in Abstimmung mit der obersten Dienstbehörde sowie der Ausbildungsstelle.

(9) Bei Nichtbestehen der Wiederholungskontrolle ist die berufspraktische Lernzielkontrolle endgültig nicht bestanden.

#### § 9 Bewertung der Leistungen

(1) Die Leistungen der Anwärterinnen und Anwärter sind in der Ausbildung und Prüfung nach einem System von Punktzahlen zu bewerten:

sehr gut (15 – 13 Punkte)

\=

eine Leistung, die den Anforderungen in besonderem Maße entspricht

gut (12 – 10 Punkte)

\=

eine Leistung, die den Anforderungen voll entspricht,

befriedigend (9 – 7 Punkte)

\=

eine Leistung, die im Allgemeinen den Anforderungen entspricht,

ausreichend (6 – 4 Punkte)

\=

eine Leistung, die zwar Mängel aufweist, aber im Ganzen den Anforderungen noch entspricht

mangelhaft (3 – 1 Punkte)

\=

eine Leistung, die den Anforderungen nicht entspricht, jedoch erkennen lässt, dass die notwendigen Grundkenntnisse vorhanden sind und die Mängel in absehbarer Zeit behoben werden könnten,

ungenügend (0 Punkte)

\=

eine Leistung, die den Anforderungen nicht entspricht und bei der selbst die Grundkenntnisse so lückenhaft sind, dass die Mängel in absehbarer Zeit nicht behoben werden könnten.

Eine Leistung wird nur dann mit „ausreichend“ oder besser bewertet, wenn mindestens die Hälfte der maximal möglichen Leistung erbracht wurde.

(2) Die Bewertung der theoretischen Leistungen beruht auf den Ergebnissen der schriftlichen Aufsichtsarbeiten, der mündlichen Mitarbeit und sonstiger Unterrichtsleistungen.

(3) Die Ausbildungsabschnittsnote setzt sich aus den Einzelnoten zusammen, wobei die Durchschnittsnote der theoretischen Leistungen und die Note der praktischen Leistung zu gleichen Teilen berücksichtigt werden.
Für das Bestehen des Ausbildungsabschnitts müssen sowohl die theoretischen als auch die praktischen Einzelleistungen im Durchschnitt mit mindestens vier Punkten bewertet worden sein.
Bei der Berechnung der Durchschnittsnoten werden nur ganze Punktzahlen gebildet.
Ist die erste Dezimalstelle größer als vier, so wird aufgerundet, anderenfalls wird abgerundet.
Eine Aufrundung auf vier Punkte findet nicht statt.

#### § 10 Verlängerung des Vorbereitungsdienstes

(1) Die Dienstbehörde kann im Einvernehmen mit der Ausbildungsstelle den Vorbereitungsdienst um höchstens ein Jahr verlängern, wenn die Anwärterin oder der Anwärter den Leistungsanforderungen nicht genügt und die Aussicht besteht, dass die Ausbildung durch die Verlängerung erfolgreich abgeschlossen werden kann, oder die angefallenen Krankheitszeiten an im Dienstplan vorgesehenen Arbeitstagen zwölf Tage pro Jahr übersteigen und bei Nichtverlängerung zu besorgen ist, dass die Leistungsanforderungen nicht erfüllt werden.
Über die Verlängerung wird auf Antrag der Anwärterin oder des Anwärters entschieden.

(2) Im Falle nicht ausreichender Leistungen in einem Ausbildungsabschnitt (§ 9 Absatz 3) entscheidet die Dienstbehörde im Einvernehmen mit der Ausbildungsstelle über die Beendigung oder Fortsetzung der Ausbildung.
Eine Fortsetzung darf nur erfolgen, wenn durch die Verlängerung der erfolgreiche Abschluss der Ausbildung zu erwarten ist.

(3) Der Vorbereitungsdienst ist im Einzelfall zu verlängern, wenn die Ausbildung

1.  wegen eines Beschäftigungsverbotes nach § 3 des Mutterschutzgesetzes vom 23. Mai 2017 (BGBl. I S. 1228), das durch Artikel 57 Absatz 8 des Gesetzes vom 12. Dezember 2019 (BGBl. I S. 2652) geändert worden ist, in der jeweils geltenden Fassung oder
2.  wegen einer Elternzeit nach den §§ 15 und 16 des Bundeselterngeld- und Elternzeitgesetzes in der Fassung der Bekanntmachung vom 27.
    Januar 2015 (BGBl.
    I S.
    33), das zuletzt durch Artikel 1 des Gesetzes vom 15.
    Februar 2021 (BGBl.
    I S.
    239) geändert worden ist, in der jeweils geltenden Fassung

unterbrochen wurde und die zielgerichtete Fortsetzung des Vorbereitungsdienstes nicht gewährleistet ist.

#### § 11 Entlassung aus dem Vorbereitungsdienst

Die Entlassung aus dem Vorbereitungsdienst erfolgt durch Widerruf des Beamtenverhältnisses, wenn sich die mangelnde Eignung auf Grund schwerwiegender gesundheitlicher, charakterlicher oder fachlicher Mängel erweist oder sonst ein wichtiger Grund vorliegt.
Von einem fachlichen Mangel ist insbesondere bei Nichtvorliegen der in § 10 Absatz 2 Satz 2 genannten Voraussetzungen auszugehen.

### Abschnitt 3  
Laufbahnprüfung

#### § 12 Zulassung zur Prüfung

(1) Die Laufbahnprüfung findet gegen Ende des Vorbereitungsdienstes statt.

(2) Zur Laufbahnprüfung wird zugelassen, wer

1.  in der bis zu diesem Zeitpunkt erzielten Gesamtabschnittsnote (gebildet aus dem Durchschnitt der in den bis dahin absolvierten praktischen und theoretischen Ausbildungsabschnitten erzielten Punktzahlen),  
    und
2.  in dem Schwerpunkt Deeskalationstechniken, Eingriffs- und Sicherungstechniken sowie Sport (§ 5 Absatz 1 Nummer 8)

jeweils mindestens die Note „ausreichend“ erreicht hat.
Über die Zulassung zur Laufbahnprüfung entscheidet die Ausbildungsstelle, die der oder dem Vorsitzenden der Prüfungskommission die Ausbildungsunterlagen übersendet.
Bei Nichtzulassung zur Prüfung kann die Ausbildung unter den in § 10 genannten Voraussetzungen verlängert werden.

#### § 13 Prüfungskommission

(1) Zur Abnahme der Laufbahnprüfung richtet die Ausbildungsstelle im Einvernehmen mit der obersten Dienstbehörde eine Prüfungskommission ein.
Bei Bedarf können mehrere Prüfungskommissionen gebildet werden.
Die Prüfungskommission ist in ihrer Prüftätigkeit unabhängig und an Weisungen nicht gebunden.

(2) Die Prüfungskommission besteht aus drei Mitgliedern:

1.  der oder dem Vorsitzenden, die oder der einer Laufbahn des höheren Dienstes angehören sollte und über Leitungserfahrungen im Justizvollzug verfügt,
2.  einer besonders befähigten Beamtin oder einem besonders befähigten Beamten der Laufbahn des allgemeinen Vollzugsdienstes bei Justizvollzugsanstalten,
3.  einem Vertreter eines Fachs der Fachrichtungen Recht, Soziale Arbeit, Psychologie oder Pädagogik.

(3) Die Kommissionsmitglieder gemäß Satz 1 Nummer 1 und 3 sollen unterschiedlichen Fachrichtungen angehören.

#### § 14 Prüfungsverfahren

(1) Die für einen ordnungsgemäßen Ablauf des Prüfungsverfahrens erforderlichen Maßnahmen trifft die oder der Vorsitzende der Prüfungskommission im Benehmen mit der Ausbildungsstelle.
Sie oder er bestimmt die Termine für die schriftlichen und mündlichen Prüfungen und ordnet für behinderte Anwärterinnen und Anwärter die ihrer Behinderung angemessenen Erleichterungen an.
Die Anwärterinnen und Anwärter sind zu Beginn der Prüfung über die Folgen von Säumnis und Täuschung zu belehren.

(2) Erscheint eine Anwärterin oder ein Anwärter nicht zum Prüfungstermin oder tritt von der Prüfung zurück, so gilt die Prüfung als nicht bestanden, es sei denn, es wird unverzüglich durch Vorlage eines ärztlichen Attestes oder anderweitig geeigneter Unterlagen der Nachweis des Vorliegens zwingender Gründe für die Verhinderung erbracht.

(3) Unternimmt es eine Anwärterin oder ein Anwärter, das Ergebnis einer Prüfungsleistung durch Täuschung, Benutzung nicht zugelassener Hilfsmittel, unzulässiger Hilfe anderer Prüfungsteilnehmerinnen oder Prüfungsteilnehmer oder Dritter oder durch unlautere Einwirkung auf Mitglieder der Prüfungskommission zu beeinflussen, wird die betroffene Prüfungsleistung mit null Punkten bewertet.
In besonders schweren Fällen kann die Prüfungskommission die Anwärterin oder den Anwärter von der weiteren Teilnahme an der Prüfung ausschließen.
In diesem Fall gilt die Prüfung als nicht bestanden.

(4) Die Prüfungskommission entscheidet mit Stimmenmehrheit.
Über den wesentlichen Prüfungsablauf ist eine Niederschrift anzufertigen.

(5) Die Beteiligung der Beschäftigtenvertretungen nach den landesrechtlichen Bestimmungen ist sicherzustellen.

#### § 15 Schriftliche Prüfung

(1) In der schriftlichen Prüfung haben die Anwärterinnen und Anwärter unter Aufsicht jeweils eine Arbeit aus den Lehrgebieten nach § 5 Absatz 1 Nummer 1 bis 3 zu fertigen.

(2) Die Inhalte der schriftlichen Prüfungsarbeiten werden nach Vorschlag der ausbildenden Lehrkräfte von der Prüfungskommission bestimmt.
Die Prüfungskommission benennt auch die zulässigen Hilfsmittel.
Die Bearbeitungszeit für die Prüfungsarbeiten soll in der Regel vier Zeitstunden betragen.

(3) Die Anwärterinnen und Anwärter fertigen die Arbeiten unter einer Kennziffer an, die vor Beginn der Prüfung zugeteilt wird.
Die Namen dürfen den Prüferinnen und Prüfern erst nach der endgültigen Bewertung aller Arbeiten mitgeteilt werden.

(4) Die Erstkorrektur soll von den Fachdozentinnen oder Fachdozenten des Lehrgangs durchgeführt werden.
Die Zweitkorrektoren werden von der Prüfungskommission bestimmt.
Weichen die Bewertungen der Erst- und Zweitkorrektur voneinander ab und kommt eine Einigung nicht zustande, so entscheidet der oder die Vorsitzende der Prüfungskommission.

(5) Die schriftliche Prüfung ist bestanden, wenn jede Aufsichtsarbeit mit mindestens vier Punkten bewertet wurde.

(6) Nach Bekanntgabe des Prüfungsergebnisses erhalten die Anwärterinnen und Anwärter Gelegenheit, binnen eines Monats ihre schriftlichen Arbeiten unter Aufsicht einzusehen.

(7) Im Falle des Nichtbestehens darf die schriftliche Prüfung nur einmal wiederholt werden.
Dabei ist die schriftliche Prüfung vollständig zu wiederholen, einzelne Bestandteile der Prüfung werden nicht erlassen.
Die Wiederholung ist frühestens zum nächsten ordentlichen Prüfungstermin möglich.

(8) Zur Wiederholung der Prüfung kann nur zugelassen werden, wer einen Ergänzungsvorbereitungsdienst von mindestens sechs Monaten bis zu einem Jahr abgeleistet hat.
Der Antrag auf Aufnahme in den Ergänzungsvorbereitungsdienst ist binnen eines Monats nach Empfang der schriftlichen Mitteilung über das erstmalige Nichtbestehen bei der obersten Dienstbehörde zu stellen.
Die Entscheidung über die Aufnahme in den Ergänzungsvorbereitungsdienst obliegt der obersten Dienstbehörde.
Die Zulassung zum Ergänzungsvorbereitungsdienst kann versagt werden, wenn die bisherigen Leistungen ein Bestehen der Wiederholungsprüfung nicht erwarten lassen.
Art und Dauer des Ergänzungsvorbereitungsdienstes bestimmt die Leitung der Ernennungsbehörde in Abstimmung mit der obersten Dienstbehörde sowie der Ausbildungsstelle.
 

(9) Bei Nichtbestehen der Wiederholungsprüfung ist die Laufbahnprüfung endgültig nicht bestanden.

#### § 16 Mündliche Prüfung

(1) Zur mündlichen Prüfung, die sich auf das gesamte Ausbildungsgebiet erstreckt, wird nur zugelassen, wer die schriftliche Prüfung sowie die berufspraktische Lernzielkontrolle und die einzelnen Ausbildungsabschnitte jeweils mit mindestens der Note „ausreichend“ bestanden hat.

(2) Es sollen nicht mehr als fünf Prüflinge gleichzeitig geprüft werden.
Die Dauer der mündlichen Prüfung soll so bemessen sein, dass auf jeden Prüfling etwa 30 Minuten entfallen.
Die Prüfung soll durch angemessene Pausen unterbrochen werden.

(3) Die oder der Vorsitzende der Prüfungskommission kann Personen, die ein dienstliches Interesse nachweisen, die Anwesenheit bei der Prüfung gestatten.

(4) Die mündliche Prüfung ist bei Erreichen von mindestens vier Punkten bestanden.
Die oder der Vorsitzende der Prüfungskommission gibt den Anwärterinnen und Anwärtern das Ergebnis der mündlichen Prüfung mit Begründung bekannt.

(5) Im Falle des Nichtbestehens der mündlichen Prüfung gilt § 15 Absatz 7 Satz 1 und 3 entsprechend.

#### § 17 Gesamtnote und Zeugniserteilung

(1) Nach bestandener schriftlicher und mündlicher Prüfung wird unter Einbeziehung der in den Ausbildungsabschnitten erzielten Leistungen eine Gesamtnote gebildet, die sich wie folgt zusammensetzt:

1.

Gesamtabschnittsnote  
(gebildet aus dem Durchschnitt der in den theoretischen und praktischen Ausbildungsabschnitten erzielten Punktzahlen)

25 Prozent.

2.

Schriftliche Prüfung (§ 15)

40 Prozent.

3.

Mündliche Prüfung (§ 16)

20 Prozent.

4.

Berufspraktische Lernzielkontrolle (§ 8)

15 Prozent.

§ 9 Absatz 3 Satz 3 bis 5 gilt entsprechend.

(2) Über das Ergebnis der Laufbahnprüfung ist ein schriftliches Zeugnis zu erteilen.
Dabei ist die Bewertung der Laufbahnprüfung als „bestanden“ (mit „sehr gut“, „gut“, „befriedigend“, „ausreichend“) oder „nicht bestanden“ auszuweisen.

(3) Das Beamtenverhältnis auf Widerruf der Anwärterinnen und Anwärter, die die Laufbahnprüfung bestehen oder endgültig nicht bestehen, endet mit dem Ablauf des Tages, an dem ihnen

1.  das Bestehen der Laufbahnprüfung oder
2.  das endgültige Nichtbestehen der Laufbahnprüfung oder einer vorgeschriebenen Zwischenprüfung

bekannt gegeben worden ist.
Im Fall von Satz 1 Nummer 1 endet das Beamtenverhältnis jedoch frühestens nach Ablauf der für den Vorbereitungsdienst im Allgemeinen oder im Einzelfall festgesetzten Zeit, § 32 Absatz 3 des Landesbeamtengesetzes.

### Abschnitt 4  
Übergangs- und Schlussvorschriften

#### § 18 Übergangsvorschriften

Für Anwärterinnen und Anwärter, die ihre Ausbildung vor dem Inkrafttreten dieser Verordnung begonnen haben, richten sich Ausbildung und Prüfung nach den bisherigen Vorschriften.

#### § 19 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Ausbildungs- und Prüfungsordnung allgemeiner Vollzugsdienst vom 3.
Dezember 2008 (GVBl.
II S.
490) außer Kraft.

Potsdam, den 8.
November 2021

Die Ministerin der Justiz

Susanne Hoffmann