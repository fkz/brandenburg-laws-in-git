## Verordnung über das Naturschutzgebiet „Oelseniederung mit Torfstichen“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542), von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl.
I S.
1972) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21. Januar 2013 (GVBl. I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl. II Nr. 43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Oder-Spree wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Oelseniederung mit Torfstichen“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 91 Hektar.
Es umfasst Flächen in folgenden Fluren:

Stadt/Gemeinde:

Gemarkung:

Flur:

Beeskow

Schneeberg

1, 3;

Friedland

Oelsen

1, 2, 5;

Grunow-Dammendorf

Grunow

1, 3.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 2 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 4 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Oder-Spree, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes als charakteristischer Teil der eiszeitlich entstandenen Oelse-Chossewitzer Seenrinne mit verschiedenen, durch die Oelse verbundenen Gewässern ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere von naturnahen Fließgewässern, Kleingewässern, Ufer- und Verlandungszonen, nährstoffreichen Mooren, Röhrichten und Seggenrieden, Erlenbruch- und Erlen-Eschenwäldern, Laubmischwäldern und Waldmänteln, gewässerbegleitenden Hochstaudenfluren sowie von Feucht- und Nasswiesen;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützter Arten, insbesondere Sumpf-Schlangenwurz (Calla palustris), Wasserfeder (Hottonia palustris), Wasser-Schwertlilie (Iris pseudacorus), Gewöhnliches Weißmoos (Leucobryum glaucum), Fieberklee (Menyanthes trifoliata), Zungen-Hahnenfuß (Ranunculus lingua) sowie verschiedene Torfmoosarten (Sphagnum ssp.);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Säugetiere, Vögel, Reptilien, Amphibien und Insekten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Biber (Castor fiber), Wasserfledermaus (Myotis daubentoni), Kleiner Abendsegler (Nyctalus leisleri), Rauhautfledermaus (Pipistrellus nathusii), Drosselrohrsänger (Acrocephalus arundinaceus), Eisvogel (Alcedo atthis), Krickente (Anas crecca), Rohrweihe (Circus aeruginosus), Kleinspecht (Dryobates minor), Schwarzspecht (Dryocopus martius), Bekassine (Gallinago gallinago), Kranich (Grus grus), Schwarzmilan (Milvus migrans), Rotmilan (Milvus milvus), Gebirgsstelze (Motacilla cinerea), Braunkehlchen (Saxicola rubetra), Waldwasserläufer (Tringa ochropus), Ringelnatter (Natrix natrix), Moorfrosch (Rana arvalis), Gebänderte Prachtlibelle (Calopteryx splendens) und Großer Feuerfalter (Lycaena dispar) sowie einer artenreichen, an Feuchtgebiete gebundenen Heuschreckenfauna;
4.  die Erhaltung und Entwicklung als störungsarmes Nahrungsgebiet für Greifvögel, insbesondere Seeadler (Haliaeetus albicilla) und Fischadler (Pandion haliaetus) sowie für den Schwarzstorch (Ciconia nigra) und als störungsarmes Rastgebiet von regionaler Bedeutung;
5.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit eines charakteristischen Ausschnitts des eiszeitlich entstandenen, vermoorten Fließtals, das durch die Oelse mit begleitenden Erlen-Eschenwäldern und die Kleingewässer ehemaliger Torfstiche geprägt wird;
6.  die Erhaltung und Entwicklung des Gebietes in seiner Funktion als Wanderungs- und Ausbreitungskorridor gewässergebundener Arten und als wesentlicher Teil des regionalen Biotopverbundes insbesondere zwischen den Gewässersystemen der Oelse, Demnitz und Schlaube sowie der Spreeniederung.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Oelseniederung mit Torfstichen“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinem Vorkommen von

1.  Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions und Hydrocharitions, Flüssen der planaren Stufe mit Vegetation des Ranunculion fluitantis und des Calliticho-Batrachion, Mitteleuropäischem Eichen-Hainbuchenwald (Carpinion betuli ) sowie Alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur als natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Auen-Wäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion) als prioritärem natürlichen Lebensraumtyp von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
3.  Fischotter (Lutra lutra), Rotbauchunke (Bombina bombina), Schmaler Windelschnecke (Vertigo angustior) sowie Bauchiger Windelschnecke (Vertigo moulinsiana) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wegen zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  zu baden oder zu tauchen;
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel aller Art zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle, Jauche, flüssige Gärreste und Sekundärrohstoffdünger einzusetzen,  
          
        Sekundärrohstoffdünger im Sinne dieser Verordnung sind Abwasser, Fäkalien, Klärschlamm und ähnliche Stoffe aus Siedlungsabfällen und vergleichbare Stoffe aus anderen Quellen, jeweils auch weiterbehandelt und in Mischungen untereinander oder mit Düngemitteln, Wirtschaftsdünger, Bodenhilfsstoffen, Kultursubstraten und Pflanzenhilfsmitteln,
    2.  bei Beweidung eine Auszäunung der Gewässerufer entlang der Böschungsoberkante sowie von Gehölzen erfolgt,
    3.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt.
        Bei Narbenschäden ist eine umbruchlose Nachsaat mit Zustimmung der unteren Naturschutzbehörde zulässig;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  die Walderneuerung der in § 3 Absatz 2 Nummer 1 und 2 genannten Waldlebensraumtypen durch Naturverjüngung erfolgt.
        Sofern sich keine ausreichende Naturverjüngung einstellt, dürfen nur lebensraumtypische Arten eingebracht werden.
        In die übrigen Waldgesellschaften dürfen nur Arten der potenziell natürlichen Vegetation eingebracht werden.
        Es sind nur heimische Baumarten in gesellschaftstypischen Anteilen unter Ausschluss eingebürgerter Arten zu verwenden,
    2.  in den in § 3 Absatz 2 Nummer 1 und 2 genannten Waldlebensraumtypen eine Nutzung ausschließlich einzelstamm- bis truppweise erfolgt.
        Auf den übrigen Waldflächen sind Holzerntemaßnahmen, die den Holzvorrat auf weniger als 40 Prozent des üblichen Holzvorrates reduzieren, bis zu einer Größe von 0,5 Hektar zulässig,
    3.  ein Altholzanteil von mindestens 10 Prozent am aktuellen Bestandesvorrat zu sichern ist.
        Dabei sind, sofern vorhanden, mindestens fünf Stämme je Hektar mit einem Mindestdurchmesser von 30 Zentimetern in 1,30 Metern Höhe über dem Stammfuß bis zum Absterben aus der Nutzung zu nehmen und dauerhaft zu markieren; in Jungbeständen ist ein solcher Altholzanteil zu entwickeln,
    4.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 30 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden und liegendes Totholz (ganze Bäume über 65 Zentimeter Durchmesser am stärksten Ende) im Bestand verbleibt,
    5.  Bäume mit Horsten und Höhlen nicht gefällt werden dürfen,
    6.  hydromorphe Böden nur bei Frost sowie Böden mit einem hohen Anteil an feinkörnigem Substrat nur bei Frost oder in Trockenperioden auf dauerhaft festgelegten Rückegassen befahren werden,
    7.  § 4 Absatz 2 Nummer 17 und 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass,
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters und Bibers weitgehend ausgeschlossen ist,
    2.  Besatzmaßnahmen in den Grunower Torfstichen nur nach einem einvernehmlich mit der unteren Naturschutzbehörde abgestimmten Hegeplan zulässig sind.
        Bis zur Vorlage eines Hegeplans sind Besatzmaßnahmen mit Zustimmung der unteren Naturschutzbehörde zulässig.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck, insbesondere der Bestand der Rotbauchunke, nicht beeinträchtigt wird,
    3.  Fischbesatz nur mit heimischen Arten erfolgt; § 13 der Fischereiordnung des Landes Brandenburg bleibt unberührt,
    4.  § 4 Absatz 2 Nummer 19 gilt;
4.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass
    1.  diese an den Grunower Torfstichen nur vom Ostufer aus zulässig und auf den in der topografischen Karte nach § 2 Absatz 2 gekennzeichneten Bereich beschränkt ist,
    2.  das Betreten von Röhrichten und Verlandungszonen unzulässig ist,
    3.  § 5 Absatz 1 Nummer 6 gilt,
    4.  § 4 Absatz 2 Nummer 11, 19, 20 und 22 gilt;
5.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        in der Zeit vom 1.
        März bis 15.
        Juni die Jagd nur vom Ansitz aus erfolgt,
        
        bb)
        
        die Jagd auf Wasservögel verboten ist,
        
        cc)
        
        die Fallen- und die Baujagd verboten sind.
        Mit Zustimmung der unteren Naturschutzbehörde kann die Fallenjagd mit Lebendfallen zur gezielten Reduzierung von Prädatoren erfolgen.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht gefährdet ist,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    3.  der Einsatz transportabler und mobiler Ansitzeinrichtungen.
        Diese sind vor der Aufstellung bei der unteren Naturschutzbehörde anzuzeigen,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und der in § 3 Absatz 2 Nummer 1 und 2 genannten Lebensraumtypen.
    
    Ablenkfütterungen sowie die Neuanlage von Ansaatwildwiesen und die Anlage und Unterhaltung von Wildäckern sind unzulässig.
    Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
    
6.  das Befahren des Oelsener Sees mit durch Muskelkraft betriebenen Booten, wobei zu Verlandungsbereichen, Röhrichten und Schwimmblattgesellschaften ein Mindestabstand von zehn Metern einzuhalten ist;
7.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege.
    Die untere Naturschutzbehörde ist rechtzeitig mit dem Ziel einer einvernehmlichen Lösung zu beteiligen;
8.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
9.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
10.  die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern diese nicht unter die Nummern 7 und 8 fallen, im Einvernehmen mit der unteren Naturschutzbehörde;
11.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
12.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
13.  die militärische Nutzung des Munitionsdepots Schneeberg einschließlich der darauf bezogenen Geländebetreuung von Frei- und Waldflächen sowie Maßnahmen der Schutzbereichsbehörde im Schutzbereich für das Munitionsdepot Schneeberg, im Benehmen mit der unteren Naturschutzbehörde;
14.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
15.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen;
16.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungserfordernissen, die sich aus anderen fachrechtlichen Vorgaben ergeben, unberührt.

#### § 6 Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  zur Wiederherstellung einer natürlichen Fließgewässerdynamik und zur Verbesserung der Gewässergüte sollen Maßnahmen durchgeführt werden.
    Für die Oelse sollte möglichst ein Konzept für den gesamten Gewässerlauf entwickelt werden;
2.  die Passierbarkeit für den Fischotter soll insbesondere an der Oelsener Mühle verbessert beziehungsweise hergestellt werden;
3.  im südlichen Teil des Gebietes soll die Oelse durch die Anlage gewässerbegleitender Gehölzpflanzungen abschnittsweise beschattet werden;
4.  im nördlichen Teil des Gebietes sollen die Ufer- und Saumbiotope der Oelse mit den angrenzenden Grünlandflächen als Lebensraum des Großen Feuerfalters erhalten und entwickelt werden.
    Dafür sollen bei der Gewässerunterhaltung Ampfer-Bestände, insbesondere von Fluss-Ampfer an Gewässerufern geschont werden und die Säume in mehrjährigen Abständen, abschnittsweise und außerhalb der Zeit von Juni bis August gemäht werden;
5.  aufgelassenes Grünland soll durch Pflegemaßnahmen auch als Lebensraum der artenreichen Wirbellosenfauna offen gehalten werden;
6.  unter Berücksichtigung angrenzender Nutzungen sollen im Bereich des Erlenwaldes im Nordwesten geeignete Wasserstände angestrebt werden;
7.  Kiefernforste sollen schrittweise durch Voranbau und Schirmstellung des derzeitigen Bestandes sowie durch die Förderung von Naturverjüngung in naturnahe Kiefern-Traubeneichenwälder umgebaut werden.
    Die Fichtenanpflanzung auf dem Flurstück 12, Flur 3 der Gemarkung Schneeberg soll durch gesellschaftstypische Baumarten der anschließenden Waldbestände ersetzt werden.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 1 Nummer 1 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

§ 5 Absatz 1 Nummer 1 Buchstabe a tritt am 1.
Januar 2019 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 25.
September 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes "Oelseniederung mit Torfstichen"](/br2/sixcms/media.php/68/GVBl_II_65_2018-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes ") 630.7 KB

2

[Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten](/br2/sixcms/media.php/68/GVBl_II_65_2018-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten") 189.9 KB

3

[Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Oelseniederung mit Torfstichen“](/br2/sixcms/media.php/68/GVBl_II_65_2018-Anlage-3.pdf "Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Oelseniederung mit Torfstichen“") 173.1 KB