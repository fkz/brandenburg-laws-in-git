## Verordnung zur Übertragung der Zuständigkeiten nach den Gesetzen zur Ausführung des Neunten Buches Sozialgesetzbuch und des Zwölften Buches Sozialgesetzbuch (Zuständigkeitsübertragungsverordnung AG-SGB IX und AG-SGB XII - ZustÜV AG-SGB IX und AG-SGB XII)

Auf Grund des § 4 Absatz 4 Satz 1 des Gesetzes zur Ausführung des Neunten Buches Sozialgesetzbuch vom 18.
Dezember 2018 (GVBl.
I Nr. 38) und des § 5 Absatz 4 Satz 1 des Gesetzes zur Ausführung des Zwölften Buches Sozialgesetzbuch vom 3.
November 2010 (GVBl. I Nr. 36), der durch Artikel 5 des Gesetzes vom 18.
Dezember 2018 (GVBl.
I Nr.
38 S. 11) geändert worden ist, verordnet die Ministerin für Arbeit, Soziales, Gesundheit, Frauen und Familie im Einvernehmen mit dem Minister des Innern und für Kommunales:

#### § 1 Übertragung der Zuständigkeit nach dem Gesetz zur Ausführung des Neunten Buches Sozialgesetzbuch

Die Zuständigkeit für den Abschluss von Leistungs- und Vergütungsvereinbarungen nach § 125 des Neunten Buches Sozialgesetzbuch wird auf die örtlichen Träger der Eingliederungshilfe übertragen.

#### § 2 Übertragung der Zuständigkeiten nach dem Gesetz zur Ausführung des Zwölften Buches Sozialgesetzbuch

Die nachfolgenden Zuständigkeiten werden auf die örtlichen Träger der Sozialhilfe übertragen:

1.  der Abschluss von Leistungs-, Prüfungs- und Vergütungsvereinbarungen nach dem Zehnten Kapitel des Zwölften Buches Sozialgesetzbuch,
2.  die Mitwirkung bei Abschluss und Kündigung von Versorgungsverträgen nach § 72 Absatz 2 Satz 1 des Elften Buches Sozialgesetzbuch,
3.  der Abschluss von Pflegesatzvereinbarungen nach § 85 Absatz 2 Satz 1 Nummer 2 des Elften Buches Sozialgesetzbuch und
4.  der Abschluss von Vereinbarungen zur Übernahme gesondert berechneter Investitionskosten nach § 75 Absatz 5 Satz 3 des Zwölften Buches Sozialgesetzbuch.

#### § 3 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Zuständigkeitsübertragungsverordnung AG-SGB XII vom 15.
April 2011 (GVBl.
II Nr.
21) außer Kraft.

Potsdam, den 22.
Oktober 2019

Die Ministerin für Arbeit, Soziales,  
Gesundheit, Frauen und Familie

Susanna Karawanskij