## Verordnung zum Ausgleich der Mehrbelastungen der Kommunen infolge des erweiterten Rechtsanspruchs auf Kindertagesbetreuung nach § 24 Absatz 2 des Achten Buches Sozialgesetzbuch (Kita-Mehrbelastungsausgleichsverordnung - Kita-MBAV)

Auf Grund des § 25 Absatz 4 des Ersten Gesetzes zur Ausführung des Achten Buches Sozialgesetzbuch – Kinder und Jugendhilfe in der Fassung der Bekanntmachung vom 26.
Juni 1997 (GVBl.
I S. 87), der durch das Gesetz vom 5.
Dezember 2013 (GVBl.
I Nr. 43) eingefügt worden ist, und des § 16a Absatz 2 des Kindertagesstättengesetzes in der Fassung der Bekanntmachung vom 27.
Juni 2004 (GVBl.
I S. 384), der durch das Gesetz vom 27.
Juli 2015 (GVBl.
I Nr. 21) neu gefasst worden ist, verordnet die Landesregierung:

#### § 1 Anwendungsbereich

Diese Verordnung regelt den Ausgleich der Mehrbelastungen, die bei den Kommunen infolge der Erweiterung des Rechtsanspruchs auf Kindertagesbetreuung von Kindern nach vollendetem ersten bis zur Vollendung des dritten Lebensjahres zum 1.
August 2013 gemäß § 24 Absatz 2 des Achten Buches Sozialgesetzbuch in der am 1.
August 2013 geltenden Fassung in Verbindung mit § 1 Absatz 2 des Kindertagesstättengesetzes in der Fassung der Bekanntmachung vom 27. Juni 2004 (GVBl.
I S. 384), das zuletzt durch das Gesetz vom 27.
Juli 2015 (GVBl.
I Nr. 21) geändert worden ist, entstehen und durch das Land auszugleichen sind (auszugleichende Plätze) sowie das Verfahren, in welcher Weise der Ausgleich erfolgt.

#### § 2 Grundlagen des Ausgleichsbetrags

Der Ausgleichsbetrag jedes örtlichen Trägers der öffentlichen Jugendhilfe wird berechnet auf Grundlage der Zahl der auszugleichenden Plätze sowie der Kosten eines Platzes für Kinder bis zum vollendeten dritten Lebensjahr im zeitlichen Mindestrechtsanspruch gemäß § 1 Absatz 2 des Kindertagesstättengesetzes in Verbindung mit § 1 Absatz 3 Satz 1 des Kindertagesstättengesetzes.

#### § 3 Bemessung der Ausgleichsquoten und der auszugleichenden Plätze

(1) Zur Ermittlung der Zahl der auszugleichenden Plätze wird die Differenz der Zahl der belegten Plätze vor und nach Erweiterung des Rechtsanspruchs nach § 1 für Kinder bis zum vollendeten dritten Lebensjahr im zeitlichen Mindestrechtsanspruch gemäß § 1 Absatz 2 des Kindertagesstättengesetzes in Verbindung mit § 1 Absatz 3 Satz 1 des Kindertagesstättengesetzes gebildet.
In der Kindertagespflege belegte Plätze werden dabei mit dem Faktor 0,65 gewichtet.
Maßgeblich ist jeweils das Mittel der Zahl der belegten Plätze für das Jahr vor der Erweiterung des Rechtsanspruchs nach § 1 an den Stichtagen 1.
September 2012, 1.
Dezember 2012, 1.
März 2013 sowie 1.
Juni 2013 und für das Jahr nach der Erweiterung des Rechtsanspruchs nach § 1 an den Stichtagen 1.
September 2013, 1.
Dezember 2013, 1.
März 2014 sowie 1.
Juni 2014.

(2) Die Differenz der Zahl der belegten Plätze nach Absatz 1 wird zum Mittel der Zahl aller belegten Plätze für Kinder bis zum vollendeten dritten Lebensjahr im zeitlichen Mindestrechtsanspruch an den vier in Absatz 1 Satz 3 genannten Stichtagen nach der Erweiterung des Rechtsanspruchs ins Verhältnis gesetzt (Ausgleichsquote auf Landesebene).
Die so ermittelte Quote beträgt 25 Prozent.
Die Ausgleichsquote des örtlichen Trägers der öffentlichen Jugendhilfe entspricht dem Produkt aus der Ausgleichsquote auf Landesebene und dem Quotienten aus der Frauenerwerbsquote auf Landesebene und der Frauenerwerbsquote des jeweiligen örtlichen Trägers der öffentlichen Jugendhilfe.
Maßgeblich für die Frauenerwerbsquoten sind die jeweils aktuellsten durch das Amt für Statistik Berlin-Brandenburg veröffentlichten Daten.

(3) Das Produkt aus der Ausgleichsquote des örtlichen Trägers der öffentlichen Jugendhilfe und der gewichteten Zahl der durch Kinder bis zum vollendeten dritten Lebensjahr im zeitlichen Mindestrechtsanspruch belegten Plätze des jeweils vorangegangenen Jahres (Basisplätze) ergibt die Zahl der auszugleichenden Plätze des Ausgleichsjahres.
Die maßgeblichen Stichtage für die Ermittlung der Zahl der Basisplätze sind der 1.
Dezember des Vorjahres, der 1.
März, der 1.
Juni und der 1.
September des jeweiligen Jahres.

(4) Die Ausgleichsquoten können auf Verlangen eines örtlichen Trägers der öffentlichen Jugendhilfe oder des Landes erstmals im Jahr 2018 und danach alle drei Jahre daraufhin überprüft werden, ob die sozialräumliche und sozialstrukturelle Situation der Familien mit Kindern der betreffenden Altersgruppe die Annahme begründet, dass die Ausgleichsquoten verändert werden müssen.
Der Umfang der Berücksichtigung der Kindertagespflege kann auf Verlangen eines örtlichen Trägers der öffentlichen Jugendhilfe oder des Landes erstmals im Jahr 2018 und danach alle drei Jahre daraufhin überprüft werden, ob er die aktuelle Kostenrelation abbildet.

#### § 4 Bemessung der Platzkosten

(1) Die Platzkosten werden bemessen auf der Grundlage der Kosten des pädagogischen Personals unter Berücksichtigung eines Anteils für zusätzlich erforderliche Leitungskräfte, eines zusätzlichen Anteils für die übrigen Platzkosten (Bewirtschaftungskosten) und der erhobenen Elternbeiträge.

(2) Zur Ermittlung der Kosten für das notwendige pädagogische Personal werden die Personalkosten einer Fachkraftstelle gemäß § 5 Absatz 3 Satz 1 und 2 der Kindertagesstätten-Betriebskosten- und Nachweisverordnung mit dem Personalschlüssel gemäß § 10 Absatz 1 Satz 2 und 3 des Kindertagesstättengesetzes in der am 30.
September 2010 geltenden Fassung von 0,8 Stellen für je sieben Kinder multipliziert.
Zum Ausgleich der Kosten der zusätzlichen Stellen für Leitungskräfte gemäß § 5 der Kita-Personalverordnung wird das Produkt nach Satz 1 um 3 Prozent erhöht.

(3) Zum Ausgleich der Bewirtschaftungskosten nach Absatz 1 wird der Betrag für die Personalkosten nach Absatz 2 um eine Bewirtschaftungskostenpauschale von 36 Prozent erhöht.
Von dem sich ergebenden Betrag werden 16 Prozent zur Berücksichtigung der Einnahmen aus Elternbeiträgen in Abzug gebracht.

(4) Höhere Platzkosten können von der Obersten Landesjugendbehörde anerkannt und ausgeglichen werden, wenn eine kreisangehörige Gemeinde dies innerhalb von drei Monaten nach Bekanntgabe der ermittelten Platzkosten im Leistungsbescheid unter Beifügung entsprechender Belege beantragt.
Für den Nachweis höherer Platzkosten und ihrer Erforderlichkeit kann die Oberste Landesjugendbehörde verbindliche Vorgaben machen.

(5) Die Ermittlung der Platzkosten kann auf Verlangen eines örtlichen Trägers der öffentlichen Jugendhilfe oder des Landes erstmals im Jahr 2018 und danach alle drei Jahre daraufhin überprüft werden, ob die in Absatz 1 bis 3 bestimmten Parameter die aktuelle Platzkostensituation abbilden.

#### § 5 Ausgleichsbetrag

(1) Zur Ermittlung des Ausgleichsbetrags eines örtlichen Trägers der öffentlichen Jugendhilfe wird die gemäß § 3 Absatz 3 ermittelte Zahl der auszugleichenden Plätze in seinem Zuständigkeitsbereich multipliziert mit den gemäß § 4 ermittelten Platzkosten.

(2) Für die Monate August bis Dezember des Jahres 2013 werden fünf Zwölftel des Ausgleichsbetrags für das Jahr 2014 angesetzt.

(3) Die Auszahlung der Ausgleichsbeträge an die örtlichen Träger der öffentlichen Jugendhilfe durch das Land erfolgt grundsätzlich gemeinsam mit der Auszahlung der Landeszuschüsse gemäß § 16 Absatz 6 und § 16a Absatz 1 des Kindertagesstättengesetzes zu den in § 5 Absatz 1 Satz 2 der Kindertagesstätten-Betriebskosten- und Nachweisverordnung genannten Terminen.

#### § 6 Verteilung des Ausgleichsbetrags in Landkreisen

(1) Von dem Ausgleichsbetrag gemäß § 5 Absatz 1 steht dem Landkreis als örtlichem Träger der öffentlichen Jugendhilfe ein Anteil zum Ausgleich seiner Finanzierung gemäß § 16 Absatz 2 Satz 1 bis 4 des Kindertagesstättengesetzes zu.
Sein Anteil an den Platzkosten gemäß § 4 entspricht seinem Zuschussbetrag für ein betreutes Kind bis zum vollendeten dritten Lebensjahr im zeitlichen Mindestrechtsanspruch gemäß § 16 Absatz 2 Satz 2 des Kindertagesstättengesetzes in Verbindung mit § 3 der Kindertagesstätten-Betriebskosten- und Nachweisverordnung.
Dabei sind eine Personalausstattung für den Platz von 0,8 Stellen einer pädagogischen Fachkraft für jeweils sieben Kinder und ein Zuschusssatz von 84 Prozent der Kosten des hierfür notwendigen pädagogischen Personals anzusetzen.
Zusätzlich steht dem örtlichen Träger der öffentlichen Jugendhilfe ein Ausgleich für geleistete Zahlungen gemäß § 16 Absatz 4 des Kindertagesstättengesetzes an Tagespflegepersonen zu.

(2) Der Ausgleichsbetrag für jede kreisangehörige Gemeinde ergibt sich, indem von den ermittelten Platzkosten gemäß § 4 der Eigenanteil des örtlichen Trägers der öffentlichen Jugendhilfe pro Platz gemäß Absatz 1 Satz 1 bis 3 subtrahiert und die Differenz mit der Zahl der auszugleichenden Plätze der Gemeinde multipliziert wird.
Die auszugleichenden Plätze ergeben sich aus dem Produkt der jeweiligen Ausgleichsquote gemäß § 3 Absatz 2 und der durch Kinder bis zum vollendeten dritten Lebensjahr im zeitlichen Mindestrechtsanspruch belegten Plätze in Kindertagesstätten, für die Zuschüsse gemäß § 16 Absatz 3 oder Absatz 5 des Kindertagesstättengesetzes gezahlt werden und kein Kostenausgleich gemäß § 16 Absatz 5 des Kindertagesstättengesetzes erfolgt.
Die Zahlung an die kreisangehörigen Gemeinden erfolgt zu den in § 3 Absatz 5 der Kindertagesstätten-Betriebskosten- und Nachweisverordnung bestimmten Terminen.

(3) Soweit abweichende Vereinbarungen zwischen dem Landkreis und kreisangehörigen Gemeinden und Ämtern über die Finanzierung gemäß § 12 Absatz 1 Satz 3 des Kindertagesstättengesetzes in Verbindung mit § 3 Absatz 8 der Kindertagesstätten-Betriebskosten- und Nachweisverordnung oder gemäß § 3 Absatz 7 der Kindertagesstätten-Betriebskosten- und Nachweisverordnung getroffen wurden, sind die Bestimmungen der Absätze 1 und 2 sinngemäß anzuwenden.

(4) Zum Ausgleich des Verwaltungsaufwands für die Aufteilung und Zahlung des Ausgleichs an die Gemeinden erhalten die Landkreise einen Kostenausgleich.
Die Höhe des Ausgleichs ergibt sich aus dem Aufwand zur Ermittlung und Auszahlung der Beträge, die gemäß Absatz 2 an die kreisangehörigen Gemeinden auszureichen sind.
Für den Aufwand werden ab dem Jahr 2016 jährlich 100 Arbeitsstunden einer Kraft im gehobenen nichttechnischen Verwaltungsdienst der Entgeltgruppe E 9 des Tarifvertrags für den öffentlichen Dienst (TVöD) im Bereich der Vereinigung der kommunalen Arbeitgeberverbände (VKA) und ein zusätzlicher Gemeinkostenanteil von 20 Prozent angesetzt.
Die Mittel werden mit den Zahlungen nach § 5 Absatz 3 durch das Land ausgereicht.
Kosten, die trotz ordnungsgemäßer Anwendung der Verordnung durch Klageverfahren gegen die Verteilung des Ausgleichsbetrags in Landkreisen entstehen sowie verwaltungsgerichtlich anerkannte höhere Ausgleichsbeträge für Gemeinden, werden vom Land ausgeglichen.

#### § 7 Übergangsvorschriften

(1) Die Platzkosten für die Jahre 2013 und 2014 betragen 6 430 Euro; für das Jahr 2015 betragen sie 6 592 Euro.

(2) Zum Ausgleich des Verwaltungsaufwands, der bei den Landkreisen für die Weiterreichung der Platzkostenanteile an die kreisangehörigen Gemeinden für die Jahre 2013 bis 2015 entsteht, werden mit der ersten Auszahlung gemäß § 5 Absatz 3 einmalig 100 Arbeitsstunden und der Gemeinkostenanteil von 20 Prozent ausgeglichen.

(3) Die Auszahlung der Ausgleichsbeträge für die Jahre 2013, 2014, 2015 und die bereits in der Vergangenheit liegenden Quartale des Jahres 2016 erfolgt zum 15.
September 2016.

#### § 8 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 14.
September 2016

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister für Bildung,  
Jugend und Sport

Günter Baaske