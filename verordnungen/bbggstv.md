## Brandenburgische Verordnung über den Bau von Garagen und Stellplätzen und den Betrieb von Garagen (Brandenburgische Garagen- und Stellplatzverordnung - BbgGStV)

Auf Grund des § 86 Absatz 1 Nummer 1 bis 5, Absatz 3 Satz 1 Nummer 2 bis 5 und Absatz 3 Satz 2 in Verbindung mit § 85 Absatz 1 Satz 1 Nummer 1 der Brandenburgischen Bauordnung vom 19. Mai 2016 (GVBl.
I Nr. 14) verordnet die Ministerin für Infrastruktur und Landesplanung:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeine Vorschriften

[§ 1 Begriffe und allgemeine Anforderungen](#1)

### Abschnitt 2  
Bauvorschriften

[§ 2 Zu- und Abfahrten](#2)

[§ 3 Rampen](#3)

[§ 4 Einstellplätze und Fahrgassen](#4)

[§ 5 Lichte Höhe](#5)

[§ 6 Tragende Wände, Decken, Dächer](#6)

[§ 7 Außenwände](#7)

[§ 8 Trennwände, sonstige Innenwände und Tore](#8)

[§ 9 Gebäudeabschlusswände](#9)

[§ 10 Wände und Decken von Kleingaragen](#10)

[§ 11 Rauchabschnitte, Brandabschnitte](#11)

[§ 12 Verbindungen zu Garagen und zwischen Garagengeschossen](#12)

[§ 13 Rettungswege](#13)

[§ 14 Beleuchtung](#14)

[§ 15 Lüftung](#15)

[§ 16 Feuerlöschanlagen, Rauch- und Wärmeabzug](#16)

[§ 17 Brandmeldeanlagen](#17)

### Abschnitt 3  
Betriebsvorschriften

[§ 18 Betriebsvorschriften für Garagen](#18)

[§ 19 Abstellen von Kraftfahrzeugen in anderen Räumen als Garagen](#19)

### Abschnitt 4  
Bauvorlagen

[§ 20 Bauvorlagen](#20)

### Abschnitt 5  
Schlussvorschriften

[§ 21 Weitergehende Anforderungen](#21)

[§ 22 Ordnungswidrigkeiten](#22)

[§ 23 Übergangsvorschriften](#23)

[§ 24 Inkrafttreten, Außerkrafttreten](#24)

### Abschnitt 1  
Allgemeine Vorschriften

#### § 1 Begriffe und allgemeine Anforderungen

(1) Offene Garagen sind Garagen, die unmittelbar ins Freie führende unverschließbare Öffnungen in einer Größe von insgesamt mindestens einem Drittel der Gesamtfläche der Umfassungswände haben, bei denen mindestens zwei sich gegenüberliegende Umfassungswände mit den ins Freie führenden Öffnungen nicht mehr als 70 Meter voneinander entfernt sind und bei denen eine ständige Querlüftung vorhanden ist.
Stellplätze mit Schutzdächern und ohne Wände (Carports) sind offene Garagen.

(2) Offene Kleingaragen sind Kleingaragen, die unmittelbar ins Freie führende unverschließbare Öffnungen in einer Größe von insgesamt mindestens einem Drittel der Gesamtfläche der Umfassungswände haben.

(3) Geschlossene Garagen sind Garagen, die die Voraussetzungen nach den Absätzen 1 und 2 nicht erfüllen.

(4) Oberirdische Garagen sind Garagen, deren Fußboden im Mittel nicht mehr als 1,50 Meter unter der Geländeoberfläche liegt.

(5) Automatische Garagen sind Garagen ohne Personen- und Fahrverkehr, in denen die Kraftfahrzeuge mit mechanischen Förderanlagen von der Garagenzufahrt zu den Garageneinstellplätzen befördert und ebenso zum Abholen an die Garagenausfahrt zurückbefördert werden.

(6) Ein Einstellplatz ist eine Fläche, die dem Abstellen eines Kraftfahrzeuges in einer Garage oder auf einem Stellplatz dient.

(7) Die Nutzfläche einer Garage ist die Summe aller miteinander verbundenen Flächen der Garageneinstellplätze und der Verkehrsflächen.
Die Nutzfläche einer automatischen Garage ist die Summe der Flächen aller Garageneinstellplätze.
Einstellplätze auf Dächern (Dacheinstellplätze) und die dazugehörigen Verkehrsflächen werden der Nutzfläche nicht zugerechnet, soweit nichts anderes bestimmt ist.

(8) Es sind Garagen mit einer Nutzfläche

1.  bis 100 Quadratmeter Kleingaragen,
2.  über 100 Quadratmeter bis 1 000 Quadratmeter Mittelgaragen,
3.  über 1 000 Quadratmeter Großgaragen.

(9) Soweit in dieser Verordnung nichts Abweichendes geregelt ist, sind auf tragende und aussteifende sowie auf raumabschließende Bauteile von Garagen die Anforderungen der Brandenburgischen Bauordnung an diese Bauteile in Gebäuden der Gebäudeklasse 5 anzuwenden; die Erleichterungen des § 30 Absatz 3 Satz 2, § 31 Absatz 4 Nummer 1 und 2, § 36 Absatz 1 Satz 2 Nummer 2, § 39 Absatz 1 Satz 3 Nummer 4, § 40 Absatz 1 Nummer 1 und 3 sowie des § 41 Absatz 5 Nummer 1 und 3 der Brandenburgischen Bauordnung sind nicht anzuwenden.

### Abschnitt 2  
Bauvorschriften

#### § 2 Zu- und Abfahrten

(1) Zwischen Garagen und öffentlichen Verkehrsflächen müssen Zu- und Abfahrten von mindestens 3 Meter Länge vorhanden sein.
Ausnahmen können gestattet werden, wenn wegen der Sicht auf die öffentliche Verkehrsfläche keine Bedenken bestehen.

(2) Vor den die freie Zufahrt zur Garage zeitweilig hindernden Anlagen, wie Schranken oder Tore, kann ein Stauraum für wartende Kraftfahrzeuge gefordert werden, wenn dies wegen der Sicherheit oder Leichtigkeit des Verkehrs erforderlich ist.

(3) Die Fahrbahnen von Zu- und Abfahrten vor Mittel- und Großgaragen müssen mindestens 2,75 Meter breit sein; der Halbmesser des inneren Fahrbahnrandes muss mindestens 5 Meter betragen.
Für Fahrbahnen im Bereich von Zu- und Abfahrtssperren genügt eine Breite von 2,30 Meter.
Breitere Fahrbahnen können in Kurven mit Innenradien von weniger als 10 Meter verlangt werden, wenn dies wegen der Verkehrssicherheit erforderlich ist.

(4) Großgaragen müssen getrennte Fahrbahnen für Zu- und Abfahrten haben.

(5) Bei Großgaragen ist neben den Fahrbahnen der Zu- und Abfahrten ein mindestens 0,80 Meter breiter Gehweg erforderlich.
Der Gehweg muss gegenüber der Fahrbahn erhöht oder verkehrssicher abgegrenzt werden.

(6) In den Fällen der Absätze 3 bis 5 sind die Dacheinstellplätze und die dazugehörigen Verkehrsflächen der Nutzfläche zuzurechnen.

(7) Für Zu- und Abfahrten von Stellplätzen gelten die Absätze 2 bis 5 sinngemäß.

#### § 3 Rampen

(1) Rampen von Mittel- und Großgaragen dürfen nicht mehr als 15 Prozent geneigt sein.
Die Breite der Fahrbahnen auf diesen Rampen muss mindestens 2,75 Meter, in gewendelten Rampenbereichen mindestens 3,50 Meter betragen.
Gewendelte Rampenteile müssen eine Querneigung von mindestens 3 Prozent haben.
Der Halbmesser des inneren Fahrbahnrandes muss mindestens 5 Meter betragen.

(2) Zwischen öffentlicher Verkehrsfläche und einer Rampe mit mehr als 10 Prozent Neigung muss eine geringer geneigte Fläche von mindestens 3 Meter Länge liegen.

(3) In Großgaragen müssen Rampen, die von Fußgängern benutzt werden, einen mindestens 0,80 Meter breiten Gehweg haben, der gegenüber der Fahrbahn erhöht oder verkehrssicher abgegrenzt ist.
An Rampen, die von Fußgängern nicht benutzt werden dürfen, ist auf das Verbot hinzuweisen.

(4) Für Rampen von Stellplätzen gelten die Absätze 1 bis 3 sinngemäß.

(5) Kraftbetriebene geneigte Hebebühnen sind keine Rampen.

#### § 4 Einstellplätze und Fahrgassen

(1) Ein notwendiger Einstellplatz muss mindestens 5 Meter lang sein.
Die Breite eines Einstellplatzes muss mindestens betragen

1.  2,30 Meter, wenn keine Längsseite,
2.  2,40 Meter, wenn eine Längsseite,
3.  2,50 Meter, wenn jede Längsseite

des Einstellplatzes im Abstand bis zu 0,10 Meter durch Wände, Stützen, andere Bauteile oder Einrichtungen begrenzt ist;

4.  3,50 Meter, wenn der Einstellplatz für Menschen mit Behinderung bestimmt ist.

Einstellplätze auf kraftbetriebenen Hebebühnen brauchen in den Fällen des Satzes 2 Nummer 1 bis 3 nur 2,30 Meter breit zu sein.
Die Sätze 1 und 2 gelten nicht für Einstellplätze auf horizontal verschiebbaren Plattformen und für diese Plattformen.

(2) Fahrgassen müssen, soweit sie unmittelbar der Zu- oder Abfahrt von Einstellplätzen dienen, hinsichtlich ihrer Breite mindestens die Anforderungen der folgenden Tabelle erfüllen; Zwischenwerte sind gradlinig einzuschalten:

Anordnung der Einstellplätze zur Fahrgasse

Erforderliche Fahrgassenbreite (in Meter) bei einer Einstellplatzbreite von

2,30 m

2,40 m

2,50 m

bis 90 °

6,50 m

6,00 m

5,50 m

bis 45 °

3,50 m

3,25 m

3,00 m.

Vor kraftbetriebenen Hebebühnen müssen die Fahrgassen mindestens 8 Meter breit sein, wenn die Hebebühnen Fahrspuren haben oder beim Absenken in die Fahrgasse hineinragen.

(3) Fahrgassen müssen, soweit sie nicht unmittelbar der Zu- oder Abfahrt von Einstellplätzen dienen, mindestens 2,75 Meter breit sein.
Fahrgassen mit Gegenverkehr müssen in Mittel- und Großgaragen mindestens 5 Meter breit sein.

(4) Einstellplätze auf horizontal verschiebbaren Plattformen sind in Fahrgassen zulässig, wenn

1.  eine Breite der Fahrgassen von mindestens 2,75 Meter erhalten bleibt,
2.  die Plattformen nicht vor kraftbetriebenen Hebebühnen angeordnet werden und
3.  in Fahrgassen mit Gegenverkehr kein Durchgangsverkehr stattfindet.

(5) Die einzelnen Einstellplätze und die Fahrgassen sind durch Markierungen am Boden leicht erkennbar und dauerhaft gegeneinander abzugrenzen.
Dies gilt nicht für

1.  Kleingaragen ohne Fahrgassen,
2.  Einstellplätze auf kraftbetriebenen Hebebühnen,
3.  Einstellplätze auf horizontal verschiebbaren Plattformen.

Mittel- und Großgaragen müssen in jedem Geschoss leicht erkennbare und dauerhafte Hinweise auf Fahrtrichtungen und Ausfahrten haben.

(6) Abschlüsse zwischen Fahrgasse und Einstellplätzen sind in Mittel- und Großgaragen nur zulässig, wenn wirksame Löscharbeiten möglich bleiben.

(7) Die Absätze 1 bis 5 gelten nicht für automatische Garagen.

(8) In Großgaragen müssen 5 Prozent der Einstellplätze, mindestens jedoch vier Einstellplätze, als Einstellplätze für die Kraftfahrzeuge behinderter Menschen errichtet und gekennzeichnet werden.

(9) In Großgaragen müssen mindestens 30 Prozent aller Einstellplätze als Einstellplätze für die Kraftfahrzeuge von Frauen gekennzeichnet werden.

(10) Mehrgeschossige Garagen müssen mit einem Aufzug ausgestattet sein, der auch zur Aufnahme von Lasten, Krankentragen und Rollstühlen geeignet ist.

(11) Die Einstellplätze nach den Absätzen 8 und 9 müssen in unmittelbarer Nähe zu den Zu- oder Abfahrten, den Ein- oder Ausgängen oder den Aufzügen gemäß Absatz 10 angeordnet sein.

(12) Abweichend von § 18 Absatz 1 muss die allgemeine elektrische Beleuchtung nach § 14 für die Einstellplätze nach den Absätzen 8 und 9 und die zugehörigen Fahrgassen während der Betriebszeit der Garage ständig in der zweiten Stufe eingeschaltet sein, sofern nicht Tageslicht mit einer entsprechenden Beleuchtungsstärke vorhanden ist.

#### § 5 Lichte Höhe

Mittel- und Großgaragen müssen in zum Begehen bestimmten Bereichen, auch unter Unterzügen, Lüftungsleitungen und sonstigen Bauteilen eine lichte Höhe von mindestens 2 Meter haben.
Dies gilt nicht für kraftbetriebene Hebebühnen.

#### § 6 Tragende Wände, Decken, Dächer

(1) Tragende Wände von Garagen sowie Decken über und unter Garagen und zwischen Garagengeschossen müssen feuerbeständig sein.

(2) Liegen Einstellplätze nicht mehr als 22 Meter über der Geländeoberfläche, so brauchen Wände und Decken nach Absatz 1

1.  bei oberirdischen Mittel- und Großgaragen nur feuerhemmend und aus nichtbrennbaren Baustoffen zu sein, soweit sich aus den §§ 27 und 31 der Brandenburgischen Bauordnung keine weitergehenden Anforderungen ergeben,
2.  bei offenen Mittel- und Großgaragen in Gebäuden, die allein der Garagennutzung dienen, nur aus nichtbrennbaren Baustoffen zu bestehen.

(3) Wände und Decken nach Absatz 1 brauchen bei eingeschossigen oberirdischen Mittel- und Großgaragen auch mit Dacheinstellplätzen, wenn das Gebäude allein der Garagennutzung dient, nur feuerhemmend zu sein oder aus nichtbrennbaren Baustoffen zu bestehen.

(4) Wände und Decken nach Absatz 1 brauchen bei automatischen Garagen nur aus nichtbrennbaren Baustoffen zu bestehen, wenn das Gebäude allein als automatische Garage genutzt wird.

(5) Für befahrbare Dächer von Garagen gelten die Anforderungen an Decken.

(6) Bekleidungen und Dämmschichten unter Decken und Dächern müssen

1.  bei Großgaragen aus nichtbrennbaren,
2.  bei Mittelgaragen aus mindestens schwerentflammbaren

Baustoffen bestehen.
Bei Großgaragen dürfen Bekleidungen aus mindestens schwerentflammbaren Baustoffen bestehen, wenn deren Bestandteile volumenmäßig überwiegend nichtbrennbar sind und deren Abstand zur Decke oder zum Dach höchstens 0,02 Meter beträgt.

(7) Für Pfeiler und Stützen gelten die Absätze 1 bis 6 sinngemäß.

#### § 7 Außenwände

(1) Außenwände von Mittel- und Großgaragen müssen aus nichtbrennbaren Baustoffen bestehen.

(2) Absatz 1 gilt nicht für Außenwände von eingeschossigen oberirdischen Mittel- und Großgaragen, wenn das Gebäude allein der Garagennutzung dient.

#### § 8 Trennwände, sonstige Innenwände und Tore

(1) Trennwände zwischen Garagen und anders genutzten Räumen müssen § 29 Absatz 3 Satz 1 der Brandenburgischen Bauordnung entsprechen.
Wände zwischen Mittel- oder Großgaragen und anderen Gebäuden müssen feuerbeständig sein.

(2) In Mittel- und Großgaragen müssen sonstige Innenwände und Tore, Einbauten, insbesondere Einrichtungen für mechanische Parksysteme, aus nichtbrennbaren Baustoffen bestehen.

#### § 9 Gebäudeabschlusswände

Als Gebäudeabschlusswände nach § 30 Absatz 2 Nummer 1 der Brandenburgischen Bauordnung genügen bei eingeschossigen oberirdischen Mittel- und Großgaragen feuerbeständige Abschlusswände ohne Öffnungen, wenn das Gebäude allein der Garagennutzung dient.

#### § 10 Wände und Decken von Kleingaragen

(1) Für Kleingaragen sind tragende Wände und Decken ohne Feuerwiderstand zulässig.
Für Kleingaragen in sonst anders genutzten Gebäuden gelten die Anforderungen der §§ 27 und 31 der Brandenburgischen Bauordnung für diese Gebäude.

(2) Wände und Decken zwischen geschlossenen Kleingaragen und anderen Räumen müssen feuerhemmend sein und feuerhemmende Abschlüsse haben, soweit sich aus § 29 Absatz 3 sowie § 31 Absatz 1 und 2 der Brandenburgischen Bauordnung keine weitergehenden Anforderungen ergeben.
Abstellräume mit bis zu 20 Quadratmeter Fläche bleiben unberücksichtigt.

(3) Als Gebäudeabschlusswand nach § 30 Absatz 2 Nummer 1 der Brandenburgischen Bauordnung genügen Wände, die feuerhemmend sind oder aus nichtbrennbaren Baustoffen bestehen.
Für offene Kleingaragen ist eine Gebäudeabschlusswand nach § 30 Absatz 2 Nummer 1 der Brandenburgischen Bauordnung nicht erforderlich.

(4) § 8 Absatz 1 gilt nicht für Trennwände

1.  zwischen Kleingaragen und Räumen oder Gebäuden, die nur Abstellzwecken dienen und nicht mehr als 20 Quadratmeter Grundfläche haben,
2.  zwischen offenen Kleingaragen und anders genutzten Räumen oder Gebäuden.

#### § 11 Rauchabschnitte, Brandabschnitte

(1) Geschlossene Garagen, ausgenommen automatische Garagen, müssen durch mindestens feuerhemmende, aus nichtbrennbaren Baustoffen bestehende Wände in Rauchabschnitte unterteilt sein.
Die Nutzfläche eines Rauchabschnitts darf

1.  in oberirdischen geschlossenen Garagen höchstens 5 000 Quadratmeter,
2.  in sonstigen geschlossenen Garagen höchstens 2 500 Quadratmeter

betragen.
Sie darf höchstens doppelt so groß sein, wenn die Garagen Sprinkleranlagen haben.
Ein Rauchabschnitt darf sich auch über mehrere Geschosse erstrecken.

(2) Öffnungen in den Wänden nach Absatz 1 müssen mit Rauchschutzabschlüssen versehen sein.
Abweichend davon sind dicht- und selbstschließende Abschlüsse aus nichtbrennbaren Baustoffen zulässig.
Die Abschlüsse müssen Feststellanlagen haben, die bei Raucheinwirkung ein selbsttätiges Schließen bewirken; sie müssen auch von Hand geschlossen werden können.

(3) Automatische Garagen müssen durch Brandwände nach § 30 Absatz 3 Satz 1 der Brandenburgischen Bauordnung in Brandabschnitte von höchstens 6 000 Kubikmeter Brutto-Rauminhalt unterteilt sein.

(4) § 30 Absatz 2 Nummer 2 der Brandenburgischen Bauordnung gilt nicht für Garagen.

#### § 12 Verbindungen zu Garagen und zwischen Garagengeschossen

(1) Flure, Treppenräume und Aufzugsvorräume, die nicht nur den Benutzerinnen und Benutzern der Garagen dienen, dürfen verbunden sein

1.  mit geschlossenen Mittel- und Großgaragen nur durch Räume mit feuerbeständigen Wänden und Decken sowie feuerhemmenden, rauchdichten und selbstschließenden Türen, die in Fluchtrichtung aufschlagen (Sicherheitsschleusen); zwischen Sicherheitsschleusen und Fluren oder Treppenräumen genügen selbst- und dichtschließende Türen.
    Abweichend davon darf die Sicherheitsschleuse direkt mit einem Aufzug verbunden sein, wenn der Aufzug in einem eigenen, feuerbeständigen Schacht liegt oder direkt ins Freie führt,
2.  mit anderen Garagen unmittelbar nur durch Öffnungen mit mindestens feuerhemmenden und selbstschließenden Türen.

(2) Mittel- und Großgaragen dürfen mit sonstigen nicht zur Garage gehörenden Räumen sowie mit anderen Gebäuden unmittelbar nur durch Öffnungen mit mindestens feuerhemmenden, rauchdichten und selbstschließenden Türen verbunden sein.
Automatische Garagen dürfen mit nicht zur Garage gehörenden Räumen sowie mit anderen Gebäuden nicht verbunden sein.

(3) Die Absätze 1 und 2 gelten nicht für Verbindungen

1.  zu offenen Kleingaragen,
2.  zwischen Kleingaragen und Räumen oder Gebäuden, die nur Abstellzwecken dienen, und nicht mehr als 20 Quadratmeter Grundfläche haben.

(4) Türen zu Treppenräumen, die Garagengeschosse miteinander verbinden, müssen mindestens feuerhemmend, rauchdicht und selbstschließend sein und aus nichtbrennbaren Baustoffen bestehen.

#### § 13 Rettungswege

(1) Jede Mittel- und Großgarage muss in jedem Geschoss mindestens zwei voneinander unabhängige Rettungswege nach § 33 Absatz 1 der Brandenburgischen Bauordnung haben.
In oberirdischen Mittel- und Großgaragen genügt ein Rettungsweg, wenn ein Ausgang ins Freie in höchstens 10 Meter Entfernung erreichbar ist.
Der zweite Rettungsweg darf auch über eine Rampe führen.
Bei oberirdischen Mittel- und Großgaragen, deren Einstellplätze im Mittel nicht mehr als 3 Meter über der Geländeoberfläche liegen, sind Treppenräume für notwendige Treppen nicht erforderlich.

(2) Von jeder Stelle einer Mittel- und Großgarage muss in demselben Geschoss mindestens ein Treppenraum einer notwendigen Treppe oder, wenn ein Treppenraum nicht erforderlich ist, mindestens eine notwendige Treppe oder ein Ausgang ins Freie

1.  bei offenen Mittel- und Großgaragen in einer Entfernung von höchstens 50 Meter,
2.  bei geschlossenen Mittel- und Großgaragen in einer Entfernung von höchstens 30 Meter über Fahrgassen und Gänge erreichbar sein.
    Die Entfernung ist in der Luftlinie, jedoch nicht durch Einstellplätze und Bauteile zu messen.

(3) In Mittel- und Großgaragen müssen dauerhafte und leicht erkennbare Hinweise auf die Ausgänge vorhanden sein.
In Großgaragen müssen die zu den notwendigen Treppen oder zu den Ausgängen ins Freie führenden Wege auf dem Fußboden durch dauerhafte und leicht erkennbare Markierungen sowie an den Wänden durch beleuchtete Hinweise gekennzeichnet sein.

(4) Für Dacheinstellplätze gelten die Absätze 1 bis 3 sinngemäß.

(5) Die Absätze 1 bis 3 gelten nicht für automatische Garagen.

#### § 14 Beleuchtung

(1) In Mittel- und Großgaragen muss eine allgemeine elektrische Beleuchtung vorhanden sein.
Sie muss so beschaffen und mindestens in zwei Stufen derartig schaltbar sein, dass an allen Stellen der Nutzflächen und Rettungswege in der ersten Stufe eine Beleuchtungsstärke von mindestens 1 Lux und in der zweiten Stufe von mindestens 20 Lux erreicht wird.

(2) In geschlossenen Großgaragen und in mehrgeschossigen Mittelgaragen muss zur Beleuchtung der Rettungswege eine Sicherheitsbeleuchtung vorhanden sein.

(3) Die Absätze 1 und 2 gelten nicht für automatische Garagen.

#### § 15 Lüftung

(1) Geschlossene Mittel- und Großgaragen müssen maschinelle Abluftanlagen und so große und so verteilte Zuluftöffnungen haben, dass alle Teile der Garage ausreichend gelüftet werden.
Bei nicht ausreichenden Zuluftöffnungen muss eine maschinelle Zuluftanlage vorhanden sein.

(2) Für geschlossene Mittel- und Großgaragen mit geringem Zu- und Abgangsverkehr, wie Wohnhausgaragen, genügt eine natürliche Lüftung durch Lüftungsöffnungen oder über Lüftungsschächte.
Die Lüftungsöffnungen müssen

1.  einen freien Gesamtquerschnitt von mindestens 1 500 Quadratzentimeter je Garageneinstellplatz haben,
2.  in den Außenwänden oberhalb der Geländeoberfläche in einer Entfernung von höchstens 35 Meter einander gegenüberliegen,
3.  unverschließbar sein und
4.  so über die Garage verteilt sein, dass eine ständige Querlüftung gesichert ist.

Die Lüftungsschächte müssen

1.  untereinander in einem Abstand von höchstens 20 Meter angeordnet sein und
2.  bei einer Höhe bis zu 2 Meter einen freien Gesamtquerschnitt von mindestens 1 500 Quadratzentimeter je Garageneinstellplatz und bei einer Höhe von mehr als 2 Meter einen freien Gesamtquerschnitt von mindestens 3 000 Quadratzentimeter je Garageneinstellplatz haben.

(3) Für geschlossene Mittel- und Großgaragen genügt abweichend von den Absätzen 1 und 2 eine natürliche Lüftung, wenn im Einzelfall nach dem Gutachten eines nach Bauordnungsrecht anerkannten Prüfsachverständigen zu erwarten ist, dass der Mittelwert des Volumengehalts an Kohlenstoffmonoxid in der Luft, gemessen über jeweils eine halbe Stunde und in einer Höhe von 1,50 Meter über dem Fußboden (CO-Halbstundenmittelwert), auch während der regelmäßigen Verkehrsspitzen im Mittel nicht mehr als 100 ppm betragen wird und wenn dies auf der Grundlage der Messungen, die nach Inbetriebnahme der Garage über einen Zeitraum von mindestens einem Monat durchzuführen sind, von einem nach Bauordnungsrecht anerkannten Prüfsachverständigen bestätigt wird.

(4) Die maschinellen Abluftanlagen sind so zu bemessen und zu betreiben, dass der CO-Halbstundenmittelwert unter Berücksichtigung der regelmäßig zu erwartenden Verkehrsspitzen nicht mehr als 100 ppm beträgt.
Diese Anforderungen gelten als erfüllt, wenn die Abluftanlage in Garagen mit geringem Zu- und Abgangsverkehr mindestens 6 Kubikmeter, bei anderen Garagen mindestens 12 Kubikmeter Abluft in der Stunde je Quadratmeter Garagennutzfläche abführen kann; für Garagen mit regelmäßig besonders hohen Verkehrsspitzen kann im Einzelfall ein Nachweis der nach Satz 1 erforderlichen Leistung der Abluftanlage verlangt werden.

(5) Maschinelle Abluftanlagen müssen in jedem Lüftungssystem mindestens zwei gleich große Ventilatoren haben, die bei gleichzeitigem Betrieb zusammen den erforderlichen Gesamtvolumenstrom erbringen.
Jeder Ventilator einer maschinellen Zu- oder Abluftanlage muss aus einem eigenen Stromkreis gespeist werden, an dem andere elektrische Anlagen nicht angeschlossen werden können.
Soll das Lüftungssystem zeitweise nur mit einem Ventilator betrieben werden, müssen die Ventilatoren so geschaltet sein, dass sich bei Ausfall eines Ventilators der andere selbsttätig einschaltet.

(6) Geschlossene Großgaragen mit nicht nur geringem Zu- und Abgangsverkehr müssen CO-Anlagen zur Messung und Warnung (CO-Warnanlagen) haben.
Die CO-Warnanlagen müssen so beschaffen sein, dass die Benutzerinnen und Benutzer der Garagen bei einem CO-Gehalt der Luft von mehr als 250 ppm über Lautsprecher und durch Blinkzeichen dazu aufgefordert werden, die Garage zügig zu verlassen oder im Stand die Motoren abzustellen.
Während dieses Zeitraumes müssen die Garagenausfahrten ständig offen gehalten werden.
Die CO-Warnanlagen müssen an eine Ersatzstromquelle angeschlossen sein.

(7) Die Absätze 1 bis 6 gelten nicht für automatische Garagen.

#### § 16 Feuerlöschanlagen, Rauch- und Wärmeabzug

(1) Nichtselbsttätige Feuerlöschanlagen wie halbstationäre Sprühwasser-Löschanlagen oder Leichtschaum-Löschanlagen müssen vorhanden sein

1.  in geschlossenen Garagen mit mehr als 20 Einstellplätzen auf kraftbetriebenen Hebebühnen, wenn jeweils mehr als zwei Kraftfahrzeuge übereinander angeordnet werden können,
2.  in automatischen Garagen mit nicht mehr als 20 Einstellplätzen.

Die Art der Feuerlöschanlage ist im Einzelfall im Benehmen mit der für den Brandschutz zuständigen Dienststelle festzulegen.

(2) Sprinkleranlagen müssen vorhanden sein

1.  in Geschossen von Großgaragen, wenn der Fußboden der Geschosse mehr als 4 Meter unter der Geländeoberfläche liegt,
2.  in automatischen Garagen mit mehr als 20 Garageneinstellplätzen.

(3) Geschlossene Großgaragen müssen für den erforderlichen Rauch- und Wärmeabzug

1.  Öffnungen ins Freie haben, die insgesamt mindestens 1 000 Quadratzentimeter je Einstellplatz groß, von keinem Einstellplatz mehr als 20 Meter entfernt und im Decken- oder oberen Drittel des Wandbereichs angeordnet sind oder
2.  maschinelle Rauch- und Wärmeabzugsanlagen haben, die sich bei Raucheinwirkung selbsttätig einschalten, mindestens für eine Stunde einer Temperatur von 300 °C standhalten, deren elektrische Leitungsanlagen bei äußerer Brandeinwirkung für mindestens die gleiche Zeit funktionsfähig bleiben und die in der Stunde einen mindestens zehnfachen Luftwechsel gewährleisten; eine ausreichende Versorgung mit Zuluft muss vorhanden sein.

(4) Absatz 3 gilt nicht für Garagen mit Sprinkleranlagen nach Absatz 2 und mit maschinellen Abluftanlagen nach § 15 Absatz 1, die mindestens 12 Kubikmeter Abluft in der Stunde je Quadratmeter Garagennutzfläche abführen können.

#### § 17 Brandmeldeanlagen

Geschlossene Großgaragen müssen Brandmeldeanlagen haben.
Geschlossene Mittelgaragen müssen Brandmeldeanlagen haben, wenn sie mit baulichen Anlagen oder Räumen in Verbindung stehen, für die Brandmeldeanlagen erforderlich sind.

### Abschnitt 3  
Betriebsvorschriften

#### § 18 Betriebsvorschriften für Garagen

(1) In Mittel- und Großgaragen muss die allgemeine elektrische Beleuchtung nach § 14 Absatz 1 während der Benutzungszeit ständig mit einer Beleuchtungsstärke von mindestens 1 Lux eingeschaltet sein, soweit nicht Tageslicht mit einer entsprechenden Beleuchtungsstärke vorhanden ist.

(2) Maschinelle Lüftungsanlagen und CO-Warnanlagen müssen so gewartet werden, dass sie ständig betriebsbereit sind.
CO-Warnanlagen müssen ständig eingeschaltet sein.

(3) In Mittel- und Großgaragen dürfen brennbare Stoffe außerhalb von Kraftfahrzeugen nicht aufbewahrt werden.
In Kleingaragen dürfen bis zu 200 Liter Dieselkraftstoff und bis zu 20 Liter Benzin in dicht verschlossenen, bruchsicheren Behältern aufbewahrt werden.

#### § 19 Abstellen von Kraftfahrzeugen in anderen Räumen als Garagen

(1) Kraftfahrzeuge dürfen in Treppenräumen, Fluren und Kellergängen nicht abgestellt werden.

(2) Kraftfahrzeuge dürfen in sonstigen Räumen, die keine Garagen sind, nur abgestellt werden, wenn

1.  das Gesamtfassungsvermögen der Kraftstoffbehälter aller abgestellten Kraftfahrzeuge nicht mehr als 12 Liter beträgt,
2.  Kraftstoff, außer dem Inhalt der Kraftstoffbehälter abgestellter Kraftfahrzeuge, in diesen Räumen nicht aufbewahrt wird und
3.  diese Räume keine Zündquellen oder leicht entzündlichen Stoffe enthalten und von Räumen mit Feuerstätten oder leicht entzündlichen Stoffen durch Türen abgetrennt sind oder
4.  die Kraftfahrzeuge Arbeitsmaschinen sind.

### Abschnitt 4  
Bauvorlagen

#### § 20 Bauvorlagen

Die Bauvorlagen müssen zusätzliche Angaben enthalten über:

1.  die Zahl, Abmessung und Kennzeichnung der Einstellplätze und Fahrgassen,
2.  die Brandmelde- und Feuerlöschanlagen,
3.  die CO-Warnanlagen,
4.  die maschinellen Lüftungsanlagen,
5.  die Sicherheitsbeleuchtung.

### Abschnitt 5  
Schlussvorschriften

#### § 21 Weitergehende Anforderungen

Weitergehende Anforderungen können zur Erfüllung des § 3 der Brandenburgischen Bauordnung gestellt werden, soweit Garagen oder Stellplätze für Kraftfahrzeuge bestimmt sind, deren Länge mehr als 5 Meter und deren Breite mehr als 2 Meter beträgt.

#### § 22 Ordnungswidrigkeiten

Ordnungswidrig nach § 85 Absatz 1 Satz 1 Nummer 1 der Brandenburgischen Bauordnung handelt, wer vorsätzlich oder fahrlässig

1.  entgegen § 15 Absatz 4 maschinelle Lüftungsanlagen so betreibt, dass der genannte Wert des CO-Gehaltes der Luft überschritten wird,
2.  entgegen § 18 Absatz 1 geschlossene Mittel- und Großgaragen nicht ständig beleuchtet.

#### § 23 Übergangsvorschriften

Auf die zum Zeitpunkt des Inkrafttretens dieser Verordnung bestehenden Garagen sind die Betriebsvorschriften (§ 18) entsprechend anzuwenden.

#### § 24 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Brandenburgische Garagen- und Stellplatzverordnung vom 12.
Oktober 1994 (GVBl.
II S. 948), die zuletzt durch Artikel 5 der Verordnung vom 23.
März 2005 (GVBl.
II S. 159) geändert worden ist, außer Kraft.

Potsdam, den 8.
November 2017

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#*) Diese Verordnung dient der Umsetzung der Richtlinie (EU) 2015/1535 des Europäischen Parlaments und des Rates vom 9.
September 2015 über ein Informationsverfahren auf dem Gebiet der technischen Vorschriften und der Vorschriften für die Dienste der Informationsgesellschaft (ABl. L 241 vom 17.09.2015, S.
1).