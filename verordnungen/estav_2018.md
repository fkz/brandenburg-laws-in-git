## Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Einkommensteuer und die Abführung der Gewerbesteuerumlage für die Haushaltsjahre 2019 und 2020 (Einkommensteueraufteilverordnung - EStAV)

Auf Grund des § 2, des § 4 Absatz 2, des § 5 und des § 6 Absatz 8 des Gemeindefinanzreformgesetzes in der Fassung der Bekanntmachung vom 10.
März 2009 (BGBl. I S. 502) in Verbindung mit § 1 der Verordnung zur Übertragung der Ermächtigung zum Erlass von Rechtsverordnungen nach dem Gemeindefinanzreformgesetz vom 23.
September 2003 (GVBl.
II S. 579) verordnet der Minister der Finanzen:

#### § 1 Verteilungsschlüssel für den Gemeindeanteil an der Einkommensteuer

(1) Der auf die Gemeinden im Land Brandenburg entfallende Gemeindeanteil an der Einkommensteuer wird für die Haushaltsjahre 2019 und 2020 nach dem in der Anlage 1 festgesetzten Schlüssel aufgeteilt.

(2) In Fällen kommunaler Neugliederung ist nach § 4 der Einkommensteuerschlüsselzahlenermittlungsverordnung vom 27. September 2017 (BGBl. I S. 3517) zu verfahren.

#### § 2 Berichtigung von Fehlern

(1) Ausgleichsbeträge nach § 4 Absatz 1 des Gemeindefinanzreformgesetzes werden nach den Anteilen der einzelnen Gemeinden an dem nach § 1 des Gemeindefinanzreformgesetzes auf die Gemeinden entfallenden Steueraufkommen errechnet, um die die in der Anlage 1 genannten Anteile zu hoch oder zu niedrig festgesetzt sind.

(2) Der Ausgleich nach Absatz 1 ist mit der jeweiligen Schlussabrechnung vorzunehmen.
Ein Ausgleich unterbleibt, wenn der auszugleichende Betrag 500 Euro nicht übersteigt.

#### § 3 Berechnung, Anweisung und Auszahlung

(1) Der Gemeindeanteil an der Einkommensteuer nach § 1 ist vom Amt für Statistik Berlin-Brandenburg zu berechnen.

(2) Das Ministerium der Finanzen stellt die anzuweisenden Beträge fest und regelt die Auszahlung an die Gemeinden.

(3) Auf den Gemeindeanteil an der Einkommensteuer für die jeweiligen Haushaltsjahre sind an die Gemeinden vierteljährliche Abschlagszahlungen zu den in Anlage 2 festgesetzten Terminen anzuweisen.
Die Abschlagszahlungen sind unter Berücksichtigung des vierteljährlichen Ist-Aufkommens an Lohnsteuer und an veranlagter Einkommensteuer sowie aus der Abgeltungssteuer zu berechnen.
Die Abschlagszahlung für das jeweils vierte Quartal ist in Höhe der Abschlagszahlung für das jeweils dritte Quartal zu leisten.

#### § 4 Gewerbesteuerumlage

(1) Die Gemeinden haben die auf Grund des § 6 des Gemeindefinanzreformgesetzes abzuführende Gewerbesteuerumlage, die zu leistenden Abschlagszahlungen und die Berechnungsgrundlagen für die Gewerbesteuerumlage dem Amt für Statistik Berlin-Brandenburg zu den nachfolgenden Terminen zu melden:

1.  für das 1.
    Quartal jeweils bis zum Ablauf des 12.
    April,
2.  für das 2.
    Quartal jeweils bis zum Ablauf des 12.
    Juli,
3.  für das 3.
    Quartal jeweils bis zum Ablauf des 12.
    Oktober des laufenden Jahres,
4.  für die Schlussabrechnung jeweils bis zum Ablauf des 15.
    Januar des Folgejahres.

(2) Die Gewerbesteuerumlage ist mit dem Gemeindeanteil an der Einkommensteuer zu verrechnen.
Übersteigt die Gewerbesteuerumlage den Gemeindeanteil an der Einkommensteuer eines Quartals, so erfolgt die Verrechnung der Gewerbesteuerumlage in Höhe des Gemeindeanteils an der Einkommensteuer; ein nicht verrechenbarer Betrag der Gewerbesteuerumlage ist von der Gemeinde bis zu den in § 6 Absatz 7 des Gemeindefinanzreformgesetzes genannten Terminen an die Landeshauptkasse Potsdam abzuführen.

(3) Die Abschlagszahlung für das jeweils vierte Quartal ist in Höhe der Abschlagszahlung für das jeweils dritte Quartal zu leisten, jedoch nicht mehr als der nach § 3 Absatz 3 jeweils anzuweisende Betrag.

(4) Das Amt für Statistik Berlin-Brandenburg bestimmt im Einvernehmen mit dem Ministerium der Finanzen die Form der Meldungen nach Absatz 1.

#### § 5 Berichtigung der Gewerbesteuerumlage

(1) Werden Unrichtigkeiten in den jeweiligen Schlussabrechnungen der Gewerbesteuerumlage festgestellt, so sind dem Amt für Statistik Berlin-Brandenburg die entsprechenden Korrekturmeldungen mit der rechtsverbindlichen Unterschrift des Hauptverwaltungsbeamten oder der Hauptverwaltungsbeamtin oder seiner oder ihrer Vertretung bis jeweils zum nächsten 15. November zuzuleiten.

(2) Zu erstattende oder nachzuzahlende Beträge nach Absatz 1 werden im Rahmen der jeweils jährlichen Schlussabrechnung der Gewerbesteuerumlage, die der Korrekturmeldung folgt, ausgeglichen.

#### § 6 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2018 in Kraft.
Gleichzeitig treten die

1.  Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Einkommensteuer im Haushaltsjahr 1991 vom 3.
    Dezember 1991 (GVBl.
    II S.
    541),
2.  Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Einkommensteuer im Haushaltsjahr 1992 vom 10.
    Juni 1992 (GVBl.
    II S.
    258), die durch die Verordnung vom 9. Oktober 1992 (GVBl.
    II S.
    626) geändert worden ist,
3.  Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Einkommensteuer im Haushaltsjahr 1993 vom 9.
    März 1993 (GVBl.
    II S.
    132),
4.  Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Einkommensteuer im Haushaltsjahr 1994 vom 3.
    März 1994 (GVBl.
    II S.
    146),
5.  Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Einkommensteuer im Haushaltsjahr 1995 vom 22.
    Februar 1995 (GVBl.
    II S.
    252),
6.  Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Einkommensteuer im Haushaltsjahr 1996 vom 26.
    Februar 1996 (GVBl.
    II S.
    194),
7.  Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Einkommensteuer für die Haushaltsjahre 1997, 1998 und 1999 vom 27.
    Juni 1997 (GVBl.
    II S.
    550),
8.  Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Einkommensteuer für die Haushaltsjahre 2000, 2001 und 2002 vom 18.
    Mai 2000 (GVBl.
    II S.
    154),
9.  Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Einkommensteuer und die Abführung der Gewerbesteuerumlage für die Haushaltsjahre 2003, 2004 und 2005 vom 22. Oktober 2003 (GVBl.
    II S.
    619),
10.  Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Einkommensteuer und die Abführung der Gewerbesteuerumlage für die Haushaltsjahre 2006, 2007 und 2008 vom 22. Mai 2006 (GVBl.
    II S.
    194), die durch die Verordnung vom 12.
    Juni 2006 (GVBl.
    II S.
    286) geändert worden ist,
11.  Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Einkommensteuer und die Abführung der Gewerbesteuerumlage für die Haushaltsjahre 2009, 2010 und 2011 vom 24. Oktober 2008 (GVBl.
    II S.
    399),
12.  Einkommensteueraufteilverordnung vom 10.
    September 2012 (GVBl.
    II Nr. 79)

außer Kraft.

Potsdam, den 6.
März 2018

Der Minister der Finanzen

Christian Görke

* * *

### Anlagen

1

[Anlage 1 (zu § 1 Absatz 1, § 2 Absatz 1) - Schlüsselzahlen für den Gemeindeanteil an der Einkommensteuer für die Haushaltsjahre 2019 und 2020](/br2/sixcms/media.php/68/EStAV-Anlage-1.pdf "Anlage 1 (zu § 1 Absatz 1, § 2 Absatz 1) - Schlüsselzahlen für den Gemeindeanteil an der Einkommensteuer für die Haushaltsjahre 2019 und 2020") 209.6 KB

2

[Anlage 2 (zu § 3 Absatz 3) - Termine der vierteljährlichen Abschlagszahlungen für die jeweiligen Haushaltsjahre](/br2/sixcms/media.php/68/EStAV-Anlage-2.pdf "Anlage 2 (zu § 3 Absatz 3) - Termine der vierteljährlichen Abschlagszahlungen für die jeweiligen Haushaltsjahre") 5.5 KB