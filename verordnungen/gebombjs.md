## Verordnung über die Gebühren für öffentliche Leistungen im Geschäftsbereich des Ministeriums für Bildung, Jugend und Sport (Gebührenordnung MBJS - GebOMBJS)

Auf Grund des § 3 des Gebührengesetzes für das Land Brandenburg vom 7.
Juli 2009 (GVBl. I S. 246) verordnet die Ministerin für Bildung, Jugend und Sport:

#### § 1 Gebührenpflichtige Leistungen

(1) Für Amtshandlungen des Ministeriums für Bildung, Jugend und Sport und der nachgeordneten Bereiche werden Gebühren nach anliegendem Gebührentarif, der Bestandteil dieser Verordnung ist, erhoben.

(2) Für Amtshandlungen des Ministeriums für Bildung, Jugend und Sport und der ihm nachgeordneten Bereiche, für die keine Tarifstellen vorhanden sind und die nicht ausschließlich im besonderen öffentlichen Interesse liegen, können Gebühren in Höhe von mindestens 1 Euro und höchstens 500 Euro erhoben werden.

#### § 2 Gebührenbemessung

(1) Soweit Gebühren nach dem erforderlichen Zeitaufwand berechnet sind, sind folgende Stundensätze zugrunde gelegt:

1.

für Beamtinnen oder Beamte des höheren Dienstes und vergleichbare Beschäftigte

80 Euro

2.

für Beamtinnen oder Beamte des gehobenen Dienstes und vergleichbare Beschäftigte

60 Euro

3.

für Beamtinnen oder Beamte des mittleren Dienstes und vergleichbare Beschäftigte

48 Euro

4.

für Beamtinnen oder Beamte des einfachen Dienstes und vergleichbare Beschäftigte

38 Euro.

(2) Bei der Ermittlung der Zeitgebühren ist die Zeit anzusetzen, die unter regelmäßigen Verhältnissen von einer entsprechend ausgebildeten Fachkraft benötigt wird.
Die Zeit für Ortsbesichtigungen einschließlich der An- und Abreise ist einzurechnen.

#### § 3 Ausnahmen von der Gebührenpflicht

Die Tarifstelle 9.4 des Gebührentarifs gilt nicht für Leistungen zur Abiturprüfung an Waldorfschulen.

#### § 4 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Gebührenordnung MBJS vom 6.
November 2012 (GVBl.
II Nr. 94) außer Kraft.

Potsdam, den 22.
Mai 2020

Die Ministerin für Bildung,  
Jugend und Sport

Britta Ernst

* * *

### Anlagen

1

[Anlage - Gebührentarif](/br2/sixcms/media.php/68/GVBl_II_44_2020-Anlage.pdf "Anlage - Gebührentarif") 378.2 KB