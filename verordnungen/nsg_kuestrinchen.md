## Verordnung über das Naturschutzgebiet „Küstrinchen“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 8 Absatz 1 und 3 und § 42 Absatz 2 Satz 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21. Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27. Mai 2013 (GVBl.
II Nr.
43) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1  
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Uckermark wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Küstrinchen“.

### § 2  
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 2 972 Hektar.
Es umfasst zwei Teilflächen in folgenden Fluren:

Stadt/Gemeinde:

Gemarkung:

Flur:

Lychen

Beenz

1 bis 4;

 

Lychen

2 bis 9, 25 bis 29;

 

Rutenberg

5, 6;

Boitzenburger Land

Thomsdorf

3, 9;

 

Warthe

1, 2.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte Übersichtskarte im Maßstab 1 : 35 000 dient der räumlichen Einordnung des Naturschutzgebietes.
Die in Anlage 2 Nummer 2 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 6 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 3 mit den Blattnummern 1 bis 34 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke wird eine Flurstücksliste gemäß Absatz 4 hinterlegt.

(3) Innerhalb des Naturschutzgebietes ist eine Zone 1 mit rund 149 Hektar mit besonderen Beschränkungen der landwirtschaftlichen Nutzung festgesetzt.
Die Grenze der Zone 1 ist in der in Anlage 2 Nummer 1 genannten Übersichtskarte, den in Anlage 2 Nummer 2 genannten topografischen Karten mit den Blattnummern 1 bis 6 und den in Anlage 2 Nummer 3 genannten Liegenschaftskarten mit den Blattnummern 1 bis 34 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten und Flurstücksliste kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Uckermark, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

### § 3  
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der naturnahen Wälder, wie zum Beispiel der Buchen-, Eichen-Hainbuchen- und Birken-Moor- sowie Erlenbruchwälder, Moorgehölze sowie der Grundrasen- und Tauchflurengesellschaften nährstoffarmer Seen, der Schwimmblattvegetation und der Braunmoos-, Torfmoos-, Seggen- und Röhrichtmoore, Quellmoore;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Breitblättriges Knabenkraut (Dactylorhiza majalis), Sumpfsitter (Epipactis palustris), Zungen-Hahnenfuß (Ranunculus lingua), Sumpfporst (Ledum palustre), Fieberklee (Menyanthes trifoliata), Moosglöckchen (Linnaea borealis) Sandstrohblume (Helichrysum arenarium), Krebsschere (Stratiotes aloides), Sumpfherzblatt (Parnassia palustris), Gelbgrüner Frauenmantel (Alchemilla xanthochlora), Quellgras (Catabrosa aquatica), Vielstenglige Sumpf-simse (Eleocharis multicaulis), Sumpfläusekraut (Pedicularis palustris), Gestrecktes Laichkraut (Potamogeton praelongus), Blasenbinse (Scheuchzeria palustris), Acker-Ziest (Stachys arvensis) sowie die Armleuchteralgen (Chara aspera, Chara filiformis, Chara rudis);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Biber (Castor fiber), Fransenfledermaus (Myotis nattereri), Wasserspitzmaus (Neomys fodiens), Ringelnatter (Natrix natrix), Laubfrosch (Hyla arborea), Großer Schillerfalter (Apatura iris), Blauflügel-Prachtlibelle (Calopteryx virgo), Östliche Moosjungfer (Leucorrhinia albifrons), Zierliche Moosjungfer (Leucorrhinia caudalis) und Grüne Mosaikjungfer (Aeshna viridis);
4.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit eines unzerschnittenen, störungsarmen Gebietes mit ausgedehnten Sanderflächen, zahlreichen Klarwasserseen, naturnahen Fließgewässern, Mooren, großflächigen Wäldern sowie einer Kulturlandschaft, die durch ein kleinflächiges Mosaik aus Feuchtwiesen, Mager- und Trockenrasen, artenreichen Äckern, Weiden, Staudenfluren, Seggenrieden und Hecken geprägt ist;
5.  die Erhaltung des Gebietes als wesentlicher Teil des überregionalen Biotopverbundes zwischen dem Lychener Seenkreuz, dem Feldberger Seengebiet und dem Naturschutzgebiet „Brüsenwalde“.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung

1.  eines Teiles des Europäischen Vogelschutzgebietes „Uckermärkische Seenlandschaft“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) in seiner Funktion als Lebensraum von Arten nach Anhang I der Richtlinie 79/409 EWG, insbesondere Seeadler (Haliaeetus albicilla), Fischadler (Pandion haliaetus), Schwarzstorch (Ciconia nigra), Kranich (Grus grus), Rohrdommel (Botaurus stellaris), Schellente (Bucephala clangula), Schwarz- (Dryocopus martius) und Mittelspecht (Dendrocopus medius) sowie Zwergschnäpper (Ficedula parva) einschließlich ihrer Brut- und Nahrungsbiotope;
2.  des Gebietes von gemeinschaftlicher Bedeutung „Küstrinchen“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), das ehemals einen Teil des Gebietes von gemeinschaftlicher Bedeutung „Hardenbeck-Küstrinchen“ umfasste, mit seinen Vorkommen von
    1.  Oligo- bis mesotrophen kalkhaltigen Gewässern mit benthischer Vegetation aus Armleuchteralgen, Oligo- bis mesotrophen stehenden Gewässern mit Vegetation der Littorelletea uniflorae und/oder der Isoeto-Nanojuncetea, Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Dystrophen Seen und Teichen, Flüssen der planaren Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Übergangs- und Schwingrasenmooren, Kalkreichen Niedermooren, Hainsimsen-Buchenwäldern und Waldmeister-Buchenwäldern als natürliche Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes,
    2.  Kalkreichen Sümpfen mit Cladium mariscus und Arten des Caricion davallianae, Moorwäldern, Auen-Wäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion, Anion incanae, Salicion albae) als prioritäre natürliche Lebensraumtypen im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes,
    3.  Biber (Castor fiber), Fischotter (Lutra lutra), Kammmolch (Triturus cristatus), Bachneunauge (Lampetra planeri), Steinbeißer (Cobitis taenia), Schlammpeitzger (Misgurnus fossilis), Bitterling (Rhodeus amarus), Großem Feuerfalter (Lycaena dispar), Schmalbindigem Tauchkäfer (Graphoderus bilineatus), Breitrand (Dytiscus latissimus), Großer Moosjungfer (Leucorrhinia pectoralis), Kleiner Flussmuschel (Unio crassus), Schmaler Windelschnecke (Vertigo angustior), Vierzähniger Windelschnecke (Vertigo geyeri) und Bauchiger Windelschnecke (Vertigo moulinsiana) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume,
    4.  Sumpfglanzkraut (Liparis loeselii) als Art von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich seiner Lebensräume und den für ihre Reproduktion erforderlichen Standortbedingungen.

### § 4   
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen sowie außerhalb der gekennzeichneten Biwakplätze am Küstrinbach zu lagern oder zu zelten;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; zulässig bleibt außerhalb von Röhrichten, Bruchwäldern, Feuchtwiesen und Mooren das Betreten zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 7 jeweils nach dem 30. Juni eines jeden Jahres;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb von Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, und außerhalb der Waldbrandwundstreifen zu reiten;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  Wasserfahrzeuge aller Art, einschließlich Surfbretter oder Luftmatratzen auf dem Großen und Kleinen Kiensee, dem Kleinen Küstrinsee, dem Schnakenpfuhl, dem Tiefen See, dem Faulen See, dem Schwanzsee, der Roten Ranke sowie dem Moosbruch zu benutzen.
    Außerhalb von Röhrichten, Schwimmblattgesellschaften und Verlandungsbereichen bleibt auf den übrigen Gewässern das Befahren mit muskelkraftbetriebenen Booten und Luftmatratzen sowie darüber hinaus auf dem Großen Küstrinsee das Befahren mit elektrisch angetriebenen Booten mit einer maximalen Leistungskraft von 600 Watt mit Genehmigung der unteren Naturschutzbehörde zulässig.
    Das Einsetzen und Anlegen der Boote ist an den Stegen sowie an den Uferabschnitten gestattet, die in den nach § 2 Absatz 2 aufgeführten Karten (Übersichtskarte im Maßstab 1 : 35 000, topografische Karten im Maßstab 1 : 10 000, Blattnummern 2, 3, 4 und 5 und Liegenschaftskarten, Blattnummern 4, 6, 8, 11, 16, 17, 18, 22, 23, 24, 25 und 30) eingezeichnet sind.
    Die Zulässigkeit des Befahrens des Küstrinchener Baches mit muskelkraftbetriebenen Booten richtet sich nach § 5 Absatz 1 Nummer 6;
13.  zu baden.
    Zulässig bleibt das Baden
    1.  von den Stegen aus,
    2.  vom Boot aus auf dem Aalsee, dem Clanssee, dem Großen und Kleinen Dreisee, dem Kolbatzer Mühlenteich, dem Krummen See, dem Großen und Kleinen Kronsee, dem Großen Küstrinsee, dem Lehstsee sowie dem Schreibermühlenteich,
    3.  von den in nach § 2 Absatz 2 zu dieser Verordnung aufgeführten Karten (Übersichtskarte im Maßstab 1 : 35 000, topografische Karten im Maßstab 1 : 10 000 mit den Blattnummern 2, 4 und 5 sowie Liegenschaftskarten mit den Blattnummern 4, 8, 11, 16, 17, 18, 23, 24 und 25) gekennzeichneten Bereichen am Großen und Kleinen Kronsee, dem Großen und Kleinen Dreisee, dem Aalsee, dem Clanssee, dem Tiefen See, dem Faulen See, dem Großen Küstrinsee, dem Krummen See, Oberpfuhlsee sowie dem Kolbatzer Mühlenteich aus;
14.  das Tauchen, ausgenommen von den in nach § 2 Absatz 2 zu dieser Verordnung aufgeführten Karten (Übersichtskarte im Maßstab 1 : 35 000, topografische Karten im Maßstab 1 : 10 000 mit den Blattnummern 2, 4 und 5 und Liegenschaftskarten mit den Blattnummern 4, 8, 18 und 25) gekennzeichneten Stellen am Großen Kronsee, dem Großen Küstrinsee, dem Clanssee sowie dem Kolbatzer Mühlenteich aus;
15.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
16.  Hunde frei laufen zu lassen;
17.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
18.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle, Rückstände aus Biogasanlagen) und Sekundärrohstoffdünger (zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen, wie zum Beispiel Schlempe) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
19.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
20.  Tiere zu füttern oder Futter bereitzustellen;
21.  Tiere auszusetzen oder Pflanzen anzusiedeln;
22.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
23.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
24.  Pflanzenschutzmittel jeder Art anzuwenden;
25.  Wiesen, Weiden oder sonstiges Grünland umzubrechen, nachzusäen oder neu anzusäen.

### § 5   
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und vergleichbare Rückstände aus Biogasanlagen oder Sekundärrohstoffdünger (wie zum Beispiel solche aus Abwasser, Klärschlamm oder Bioabfälle) einzusetzen,
    2.  auf Grünland § 4 Absatz 2 Nummer 24 und 25 gilt, wobei die Nachsaat von Grünland außerhalb der in § 3 Absatz 2 Nummer 2 Buchstabe a genannten Lebensräume zulässig bleibt,
    3.  innerhalb der Zone 1 auf Grünland über die Regelungen gemäß Buchstaben a und b hinaus § 4 Absatz 2 Nummer 18 gilt;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  der in § 3 Absatz 2 Nummer 2 Buchstabe b genannte Lebensraumtyp „Moorwälder“ auf dem Flurstück 1 der Flur 3, dem Flurstück 22 der Flur 4 und dem Flurstück 71 der Flur 5 jeweils der Gemarkung Lychen nicht bewirtschaftet wird, im Übrigen eine Nutzung der Laubwälder einzelstamm- bis truppweise erfolgt,
    2.  in Misch- und Nadelwäldern Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von maximal 0,5 Hektar zulässig sind,
    3.  nur Baumarten der potenziell natürlichen Vegetation in lebensraumtypischen Anteilen eingebracht werden, wobei nur heimische Baumarten in gesellschaftstypischen Anteilen unter Ausschluss eingebürgerter Arten zu verwenden sind,
    4.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    5.  das Befahren des Waldes nur auf Wegen oder Rückegassen erfolgt,
    6.  der Boden unter Verzicht auf Pflügen und Umbruch bearbeitet wird; ausgenommen ist eine streifenweise, flachgründige, nicht in den Mineralboden eingreifende Bodenverwundung zur Unterstützung von Verjüngungsmaßnahmen,
    7.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß und einer Mindesthöhe von 5 Metern nicht gefällt werden und liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimeter am stärkeren Ende) im Bestand verbleibt,
    8.  ein Altholzanteil von mindestens 10 Prozent am aktuellen Bestandsvorrat zu sichern ist und, sofern vorhanden, mindestens fünf Stämme je Hektar mit einem Brusthöhendurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum Absterben aus der Nutzung genommen sein müssen,
    9.  § 4 Absatz 2 Nummer 18 und 24 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Besatzmaßnahmen nur mit heimischen Arten durchgeführt werden und der Besatz mit Karpfen, ausgenommen im Aalsee und am Schwarzen Teich, unzulässig ist,
    2.  der Besatz mit Aalen, ausgenommen im Großen Küstrinsee, dem Kleinen Küstrinsee, dem Kolbatzer Mühlenteich, dem Schreibermühlenteich, dem Aalsee, dem Krummen See, dem Lehstsee, dem Torgelowsee und dem Schwarzen Teich unzulässig ist,
    3.  Fanggeräte und Fangmittel so eingesetzt oder ausgestattet werden, dass ein Einschwimmen und eine Gefährdung des Fischotters und des Bibers weitgehend ausgeschlossen sind,
    4.  § 4 Absatz 2 Nummer 20 gilt;
4.  die rechtmäßige Ausübung der Angelfischerei, ausgenommen am Großen und Kleinen Kiensee, dem Kleinen Küstrinsee, dem Schnakenpfuhl, der Roten Ranke, dem Krüselinfließ und dem Schwanzsee mit der Maßgabe, dass
    1.  die Angelnutzung von den in § 2 Absatz 2 zu dieser Verordnung aufgeführten Karten (Übersichtskarte im Maßstab 1 : 35 000, topografische Karten im Maßstab 1 : 10 000, Blattnummern 2, 4, 5 und 6 und Liegenschaftskarten, Blattnummern 4, 8, 11, 13, 16, 17, 18, 23, 24, 25 und 30) eingezeichneten Uferabschnitten sowie von den Stegen aus erfolgt, wobei das Betreten von Röhrichten und Verlandungszonen unzulässig bleibt,
    2.  das Angeln auf dem Großen Küstrinsee unter Beachtung des § 4 Absatz 2 Nummer 12 zulässig ist;
    3.  im Übrigen § 4 Absatz 2 Nummer 12, 20 und 21 gilt;
5.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Fallenjagd mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern zum Gewässerufer verboten ist; Ausnahmen von der Einhaltung dieses Abstandes kann die untere Naturschutzbehörde erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
        bb)
        
        keine Baujagd in einem Abstand von bis zu 100 Metern vom Ufer aller innerhalb des Schutzgebietes liegenden Gewässer vorgenommen wird,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    3.  das Aufstellen transportabler und mobiler Ansitzeinrichtungen,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und des in § 3 Absatz 2 Nummer 2 Buchstabe a genannten Lebensraumtyps „Magere Flachland-Mähwiesen“.
    
    Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern sind unzulässig.
    Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
    
6.  das Befahren des Küstrinchener Baches mit muskelkraftbetriebenen Booten
    1.  zwischen der Floßschleuse IV (Fegefeuer) und dem Oberpfuhlsee,
    2.  im übrigen Verlauf mit der Maßgabe, dass
        
        aa)
        
        ausschließlich Einer-, Zweier-Kajaks beziehungsweise Einer- bis Dreier-Kanadier benutzt werden,
        
        bb)
        
        bei einem Pegelstand von weniger als 30 Zentimetern am Pegel unterhalb des Küstrinchener Wehres das Befahren untersagt ist.
        Das Befahrensverbot wird an den Ein- und Ausstiegsstellen für die Wasserfahrzeuge bekannt gemacht,
        
        cc)
        
        nicht entgegen der Strömung gefahren werden darf;
        
7.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch nach dem 30. Juni eines jeden Jahres;
8.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Instandhaltung und Instandsetzung der dem öffentlichen Verkehr gewidmeten Straßen und Wege hinsichtlich der Fahrbahn und des Banketts in der Zeit ab 1.
    Juli eines jeden Jahres, sofern eine Beschädigung des Gehölzbestandes ausgeschlossen ist.
    Die Maßnahmen sind der unteren Naturschutzbehörde vorab anzuzeigen;
9.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, die den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
10.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
11.  Unterhaltungsmaßnahmen an rechtmäßig bestehenden Anlagen, sofern diese nicht unter die Nummern 9 und 10 fallen, im Einvernehmen mit der unteren Naturschutzbehörde;
12.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
13.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung nach Anzeige gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes im Einvernehmen mit der unteren Naturschutzbehörde;
14.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der zuständigen Naturschutzbehörde zugelassen oder angeordnet worden sind, beispielsweise Maßnahmen zur Bekämpfung der Spät-blühenden Traubenkirsche;
15.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl. S.
    1734), die durch die Bekanntmachung vom 1.
    Oktober 2013 (ABl.
    S. 2811) geändert worden ist, an Straßen und Wegen freigestellt;
16.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

### § 6   
Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  im Krummen See sollen die faunenfremden Arten abgefischt werden;
2.  zum Schutz indigener Pflanzen sollen Maßnahmen zur Bekämpfung invasiver Neophyten wie zum Beispiel der Spätblühenden Traubenkirsche eingeleitet werden;
3.  die Mahd und Beräumung der Feuchtwiesen am Oberpfuhlmoor wird zur Reduzierung von Nährstoffeinträgen angestrebt.

### § 7   
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

### § 8   
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

### § 9   
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 17 und 18 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 29 Absatz 3 und § 30 des Bundesnaturschutzgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten, ihrer Lebensstätten und Biotope (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 54 Absatz 7 des Bundesnaturschutzgesetzes) unberührt.

### § 10   
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

### § 11   
Inkrafttreten, Außerkrafttreten

(1) § 5 Absatz 1 Nummer 1 Buchstabe a, b und c tritt am 1.
Januar 2015 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig treten außer Kraft:

1.  der Beschluss 86/89 des Bezirkstages Neubrandenburg vom 30.
    März 1989 über die Naturschutzgebiete „Tiefer und Fauler See“, „Clanssee“, „Küstrinchenbach und Oberpfuhlmoor“ und „Kleiner Kronensee“;
2.  der Beschluss Nummer 61/75 des Bezirkstages Neubrandenburg vom 8.
    Januar 1975 über das Naturschutzgebiet „Großer Kernbruch“.

Potsdam, den 1.
Oktober 2014

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

* * *

### Anlagen

1

[NSGKüstrinchen-Anlg-1](/br2/sixcms/media.php/68/GVBl_II_76_2014-Anlage-1.pdf "NSGKüstrinchen-Anlg-1") 688.3 KB

2

[NSGKüstrinchen-Anlg-2](/br2/sixcms/media.php/68/GVBl_II_76_2014-Anlage-2.pdf "NSGKüstrinchen-Anlg-2") 675.4 KB