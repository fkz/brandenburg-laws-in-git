## Verordnung über die Genehmigung wissenschaftlicher Untersuchungen an Schulen (Wissenschaftliche Untersuchungen Verordnung - WissUV)

Auf Grund des § 66 Absatz 5 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
August 2002 (GVBl.
I S. 78) verordnet die Ministerin für Bildung, Jugend und Sport:

#### § 1 Geltungsbereich

(1) Wissenschaftliche Untersuchungen an Schulen in öffentlicher Trägerschaft sind gemäß § 66 Absatz 1 des Brandenburgischen Schulgesetzes unabhängig von der Form der jeweiligen Erhebung vor ihrer Durchführung dem für Schule zuständigen Ministerium zur Genehmigung vorzulegen.
Sie sollen inhaltlich und hinsichtlich der angestrebten Ergebnisse einen Bezug zum Erziehungs- und Bildungsauftrag der Schule haben.
Die Genehmigung wird nach Maßgabe der folgenden Bestimmungen erteilt.
§ 66 Absatz 4 des Brandenburgischen Schulgesetzes bleibt unberührt.

(2) Wissenschaftliche Untersuchungen, die

1.  von dem für Schule zuständigen Ministerium durchgeführt werden oder von diesem beauftragte nachgeordnete Einrichtungen oder wissenschaftliche Institutionen durchführen oder
2.  als Vergleichsstudien auf Beschluss der Ständigen Konferenz der Kultusminister der Länder in der Bundesrepublik Deutschland (KMK) durchgeführt werden,

bedürfen gemäß § 66 Absatz 1 Satz 1 des Brandenburgischen Schulgesetzes nicht der Genehmigung.
Die Durchführenden werden von dem für Schule zuständigen Ministerium auf die Anwendung der datenschutzrechtlichen Bestimmungen verpflichtet.
Für diese wissenschaftlichen Untersuchungen besteht eine Beteiligungspflicht der ausgewählten Schulen.
§ 66 Absatz 2 des Brandenburgischen Schulgesetzes bleibt unberührt.

(3) Nicht der Genehmigungspflicht gemäß § 1 Absatz 1 unterliegen:

1.  Umfragen und Erhebungen, die Schülerinnen oder Schüler an ihrer Schule durchführen sowie
2.  Umfragen und Erhebungen von Eltern von Schülerinnen und Schülern der jeweils von ihnen besuchten Schulen.

(4) Derartige Untersuchungen bedürfen der Genehmigung durch die Schulleiterin oder den Schulleiter der Schule, an der die Untersuchung geplant ist.
Die Teilnahme an der Untersuchung ist freiwillig.
Sollen dabei personenbezogene Daten von Schülerinnen und Schülern oder an der Schule tätigen Personen erhoben werden, kann die Schulleiterin oder der Schulleiter die Genehmigung erteilen, wenn die Einhaltung der datenschutzrechtlichen Bestimmungen gewährleistet ist und die jeweils betroffenen Personen eingewilligt haben.
Ist davon auszugehen, dass die Untersuchungen ganz oder teilweise für Einrichtungen oder Personen gemäß § 2 oder für andere Einrichtungen oder Personen erfolgen, ist auf die Erforderlichkeit der Antragstellung zur Erteilung einer Genehmigung gemäß dieser Verordnung zu verweisen.
§ 5 Absatz 7 Satz 3 gilt entsprechend.

#### § 2 Antragsberechtigte

Anträge auf Genehmigung einer wissenschaftlichen Untersuchung können gestellt werden von

1.  in- oder ausländischen Hochschulen, Fachhochschulen und anderen wissenschaftlichen Einrichtungen,
2.  Studierenden und Lehramtsanwärterinnen und Lehramtsanwärtern, wenn deren Untersuchungen im Rahmen von wissenschaftlichen Haus- und Prüfungsarbeiten erfolgen,
3.  Behörden und Einrichtungen der Bundesrepublik Deutschland, der Länder und Kommunen,
4.  in- oder ausländischen juristischen Personen, die nachgewiesen auf wissenschaftlicher Grundlage arbeiten und ein besonderes fachliches Interesse gemäß § 1 Absatz 1 Satz 2 begründen können,
5.  in- oder ausländischen natürlichen Personen, die aufgrund ihrer wissenschaftlichen Tätigkeit oder Funktion ein besonderes fachliches Interesse gemäß § 1 Absatz 1 Satz 2 begründen können.

#### § 3 Antragsunterlagen

Den Anträgen auf Genehmigung einer wissenschaftlichen Untersuchung sind beizufügen:

1.  eine ausführliche Darstellung des Zwecks, des Inhalts und des Umfangs der wissenschaftlichen Untersuchung, Angaben zu Auftraggebenden sowie die Benennung der für die Untersuchung Verantwortlichen und deren Qualifikation,
2.  verbindliche Hinweise auf die Art der erforderlichen Daten, deren Verarbeitung und Sicherung sowie auf die Art und Weise der beabsichtigten Verwendung der Untersuchungsergebnisse, insbesondere auf deren Übermittlung,
3.  Angaben über die Anzahl der einzubeziehenden Schülerinnen und Schüler, die Jahrgangsstufen, über die einbezogenen Schulen nach Schulformen,
4.  Angaben über die Einbeziehung von Lehrkräften, sonstigem pädagogischen Personal und sonstigem Personal gemäß § 68 des Brandenburgischen Schulgesetzes sowie über die Einbeziehung von Personen der Schulleitungen und der Schulaufsichtsbehörden,
5.  Angaben über den beabsichtigten Umfang der Inanspruchnahme der Personen gemäß den Nummern 3 und 4 sowie über die zeitlichen Umstände der Erhebungen vor Ort,
6.  ein Zeitplan für die wissenschaftliche Untersuchung,
7.  eine vollständige Dokumentation der geplanten Erhebungsinstrumente (zum Beispiel Fragebogenmuster, Fragenkataloge, Angaben zu Form und Inhalt von Bild- und Tonaufnahmen) sowie Angaben über den Zeitpunkt der Anonymisierung und die Vernichtung oder Löschung der erhobenen Daten,
8.  ein Muster des Anschreibens an die Eltern Minderjähriger sowie an die Schülerinnen und Schüler, die einwilligungsfähig sind sowie ein Muster der widerruflichen Einwilligungserklärung der Eltern zur freiwilligen Teilnahme der minderjährigen Kinder an der Befragung sowie ein Muster eines Hinweises an die Schülerinnen und Schüler, dass die Teilnahme an der wissenschaftlichen Untersuchung freiwillig und jederzeit widerrufbar ist und dass bei einer Teilnahme nicht alle Fragen beantwortet werden müssen,
9.  eine Verpflichtungserklärung, dass die Ergebnisse der Untersuchung dem für Schule zuständigen Ministerium unentgeltlich, zeitnah und ohne Aufforderung zur Verfügung gestellt werden sowie
10.  eine Erklärung über die Einhaltung der Bestimmungen gemäß § 66 Absatz 3 des Brandenburgischen Schulgesetzes, des Brandenburgischen Datenschutzgesetzes sowie der Verordnung (EU) 2016/679; Antragstellende, die nicht dem Geltungsbereich der Verordnung (EU) 2016/679 unterliegen, sind auf die Anwendung dieser rechtlichen Regelungen zu verpflichten.

Die zustimmende Stellungnahme der Schulkonferenz gemäß § 91 Absatz 3 Nummer 9 des Brandenburgischen Schulgesetzes der jeweils in die wissenschaftliche Untersuchung einbezogenen Schule muss den Antragstellenden vor Beginn der Untersuchung vorliegen und kann vom für Schule zuständigen Ministerium angefordert werden.
Das Erfordernis der Stellungnahme der Schulkonferenz entfällt bei Untersuchungen gemäß § 2 Nummer 2.

#### § 4 Schutz personenbezogener Daten, Einwilligung

(1) Wissenschaftliche Untersuchungen, bei denen personenbezogene Daten verarbeitet werden, sind auf der Grundlage von § 66 Absatz 3 des Brandenburgischen Schulgesetzes, des Brandenburgischen Datenschutzgesetzes sowie der Verordnung (EU) 2016/679 durchzuführen.

(2) Die Teilnahme von Schülerinnen und Schülern an wissenschaftlichen Untersuchungen erfolgt freiwillig, es sei denn, es handelt sich um eine Untersuchung gemäß § 1 Absatz 2.
Die Teilnahme setzt bei minderjährigen Schülerinnen und Schülern eine Einwilligung der Eltern voraus.
Für minderjährige Schülerinnen und Schüler, die das 14.
Lebensjahr vollendet haben und als einwilligungsfähig gelten, ist zusätzlich zu der Einwilligung der Eltern eine selbstständige Einwilligung vorauszusetzen.
Allein auf der Grundlage der elterlichen Einwilligung können diese Schülerinnen und Schüler nicht verpflichtet werden, an der wissenschaftlichen Untersuchung teilzunehmen, wenn ihrerseits keine Einwilligung vorliegt.
Liegt in diesen Fällen bereits keine Einwilligung der Eltern vor, kommt eine Teilnahme dieser Schülerinnen und Schüler nicht mehr in Betracht.
Bei volljährigen Schülerinnen und Schülern bedarf es einer zusätzlichen Einwilligung der Eltern, wenn im Rahmen der wissenschaftlichen Untersuchungen personenbezogene Daten der Eltern (zum Beispiel Bildungsabschlüsse, Einkommen, Beruf sowie andere Daten gemäß Artikel 9 Absatz 1 der Verordnung (EU) 2016/679) erhoben werden sollen.
Dies gilt auch dann, wenn ein namentlicher Bezug zu den Eltern nicht herstellbar ist.
Die Einwilligung bedarf der Schriftform.

(3) Im Rahmen der Genehmigung sind die Personen, die im Rahmen der wissenschaftlichen Untersuchung berechtigten Zugang zu personenbezogenen Daten haben, auf die Pflicht hinzuweisen, das Datengeheimnis zu wahren und solche Daten nicht unbefugt zu einem anderen als dem zur jeweiligen rechtmäßigen Aufgabenerfüllung gehörenden Zweck zu verarbeiten oder zu offenbaren.
Diese Verpflichtung gilt auch für die Zeit nach Beendigung der Tätigkeit.

(4) Der Schutz vor einer möglichen Bestimmbarkeit Betroffener aus anonymisierten oder pseudonymisierten Daten ist in geeigneter Weise zu sichern.
Kann dies nicht hinreichend sicher gewährleistet werden, ist die Genehmigung nur unter dem Vorbehalt zu erteilen, dass alle Betroffenen dem in Kenntnis dieses Umstandes ausdrücklich zustimmen.

(5) Bild- und Tonaufnahmen sowie andere Rohdaten sind nach erfolgter Verarbeitung, die keine personenbezogenen Daten enthalten darf, zum frühestmöglichen Zeitpunkt zu löschen.
Unbefugte Dritte dürfen keinen Zugang zu Daten der Erhebung haben.

#### § 5 Genehmigungsverfahren

(1) Anträge auf Genehmigung einer wissenschaftlichen Untersuchung sollen spätestens drei Monate vor deren beabsichtigtem Beginn bei dem für Schule zuständigen Ministerium vollständig eingereicht werden.
Die umfassende schriftliche Information der jeweils vorgesehenen Schule insbesondere über den Inhalt, den Umfang, den Zeitraum sowie die einzubeziehenden Personen obliegt den Antragstellenden.

(2) Das für Schule zuständige Ministerium prüft, ob die formalen und inhaltlichen Voraussetzungen für die Genehmigung der wissenschaftlichen Untersuchung vorliegen.
Voraussetzung dieser Prüfung ist die vollständige Einreichung der Antragsunterlagen gemäß § 3.
Wissenschaftliche Untersuchungen sollen genehmigt werden, wenn sie den Bestimmungen dieser Verordnung entsprechen und der Bildungsauftrag der Schulen nicht unangemessen beeinträchtigt wird.
Die Erteilung einer Genehmigung stellt keinen Anspruch der oder des Antragstellenden auf eine Beteiligung der gewählten Schule dar.
Diese entscheidet in der Schulkonferenz gemäß § 91 Absatz 3 Nummer 9 des Brandenburgischen Schulgesetzes unabhängig von der Genehmigung über die Beteiligung.

(3) Untersuchungen von Antragsberechtigten gemäß § 2 Nummer 2 werden genehmigt, wenn die Antragsunterlagen gemäß § 3 eingereicht und keine datenschutzrechtlichen Beanstandungen vorliegen.

(4) Ein Anspruch auf Erteilung der Genehmigung besteht nicht.
Die Genehmigung kann mit Auflagen verbunden werden.
Insbesondere kann sie davon abhängig gemacht werden, dass der Umfang eingeschränkt oder die Untersuchung außerhalb der Unterrichtszeit durchgeführt wird.

(5) Eine Genehmigung der wissenschaftlichen Untersuchung kann insbesondere dann versagt werden, wenn deren Zweck und Inhalt eine Mitwirkung der Schulen zwar voraussetzt, die Schulen aber vorrangig nur als organisatorische Hilfe zum Erreichen einer Zielgruppe dienen sollen (zum Beispiel bei vorrangig medizinisch orientierten Untersuchungen).

(6) Eine Genehmigung ist zu versagen, wenn durch die wissenschaftliche Untersuchung

1.  der Schutz der personenbezogenen Daten auch unter Berücksichtigung von § 4 Absatz 4 nicht hinreichend sicher gewährleistet werden kann,
2.  eine zu erwartende Beeinträchtigung des Schulbetriebs in keinem angemessenen Verhältnis zur Bedeutung des Vorhabens steht,
3.  in die Erziehungsaufgabe der Schule oder das Erziehungsrecht der Eltern eingegriffen wird oder
4.  aufgrund ihres Inhalts, der Gestaltung oder ihres werbenden Charakters gegen Ziele und Grundsätze der Erziehung und Bildung gemäß § 4 des Brandenburgischen Schulgesetzes verstoßen wird.

(7) Genehmigungen, die auf unvollständigen oder falschen Angaben in den Antragsunterlagen beruhen, können zurückgenommen werden.
Sie können widerrufen werden, insbesondere wenn Auflagen gemäß § 5 Absatz 4 nicht erfüllt werden oder die wissenschaftliche Untersuchung von den der Genehmigung zugrunde liegenden Voraussetzungen nicht nur unerheblich abweicht.
Genehmigungen sind zu widerrufen, wenn bei der Durchführung der wissenschaftlichen Untersuchung Belange der Schule in schwerwiegender Weise beeinträchtigt oder bei der Erhebung und Verarbeitung personenbezogener Daten die datenschutzrechtlichen Bestimmungen verletzt werden.

(8) Die Lehrkräfte und das sonstige pädagogische Personal an in wissenschaftliche Untersuchungen einbezogenen Schulen sind für Untersuchungen gemäß § 1 Absatz 2 zur Teilnahme im Rahmen ihrer Dienstzeiten verpflichtet.
Für alle anderen Untersuchungen ist die Teilnahme auch dann freiwillig, wenn die Zustimmung der Schulkonferenz vorliegt.
Satz 1 gilt nicht für das sonstige Personal gemäß § 68 Absatz 1 Satz 3 des Brandenburgischen Schulgesetzes und die in § 68 Absatz 3 des Brandenburgischen Schulgesetzes genannten Personen.

(9) Das für Schule zuständige Ministerium unterrichtet die für die in wissenschaftliche Untersuchungen einbezogenen Schulen zuständigen staatlichen Schulämter über die Genehmigung einer wissenschaftlichen Untersuchung.

#### § 6 Aufbewahrung der Unterlagen

Das für Schule zuständige Ministerium bewahrt die Unterlagen zur Genehmigung von wissenschaftlichen Untersuchungen ab dem Zeitpunkt der Genehmigung oder dem Versagen der Genehmigung mindestens fünf Jahre auf.
Danach werden die Unterlagen dem Landeshauptarchiv angeboten.
Von diesem nicht übernommene Unterlagen werden vernichtet.
Dies gilt entsprechend für die Löschung und das Angebot an das Landeshauptarchiv, wenn das Genehmigungsverfahren insgesamt oder teilweise elektronisch durchgeführt wurde.

#### § 7 Übergangsvorschriften

Die Prüfung und Genehmigung von Anträgen zur Durchführung wissenschaftlicher Untersuchungen, die vor Inkrafttreten dieser Verordnung ordnungsgemäß gestellt worden sind, erfolgt auf der Grundlage der zum Antragszeitpunkt geltenden Vorschriften.

#### § 8 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Wissenschaftliche Untersuchungen Verordnung vom 11.
Dezember 1997 (GVBl.
1998 II S. 118) außer Kraft.

Potsdam, den 15.
Juni 2018

Die Ministerin für Bildung,  
Jugend und Sport

Britta Ernst