## Verordnung über das Naturschutzgebiet „Naturentwicklungsgebiet Wisianka“

Auf Grund des § 22 Absatz 1 und 2, der §§ 23 und 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542), von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl.
I S.
1972, 1974) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl. I Nr. 3), von denen § 8 Absatz 1 durch Artikel 1 des Gesetzes vom 25.
September 2020 (GVBl. I Nr. 28) geändert worden ist, und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27. Mai 2013 (GVBl.
II Nr.
43), der durch Artikel 2 des Gesetzes vom 25.
September 2020 (GVBl. I Nr. 28) geändert worden ist, verordnet der Minister für Landwirtschaft, Umwelt und Klimaschutz:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Dahme-Spreewald wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Naturentwicklungsgebiet Wisianka“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 185 Hektar.
Es befindet sich innerhalb des Geltungsbereichs der Verordnung über die Festsetzung von Naturschutzgebieten und einem Landschaftsschutzgebiet von zentraler Bedeutung mit der Gesamtbezeichnung „Biosphärenreservat Spreewald“ vom 12.
September 1990 (GBl. Sonderdruck Nr. 1473).
Es umfasst folgende Flächen:

Gemeinde:

Gemarkung:

Flur:

Flurstück:

Alt Zauche-Wußwerk

Alt Zauche

10

122 bis 128, 131, 139 jeweils vollständig, 33/1, 57, 129, 132, 135, 136, 138 jeweils anteilig.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in topografischen Karten im Maßstab 1 : 10 000 mit dem Titel „Topografische Karte zur Verordnung über das Naturschutzgebiet ‚Naturentwicklungsgebiet Wisianka‘“ und in Liegen​schaftskarten im Maßstab 1 : 2 500 mit dem Titel „Liegenschaftskarte zur Verordnung über das Naturschutzgebiet ‚Naturentwicklungsgebiet Wisianka‘“ mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die topografischen Karten mit den Blattnummern 1 und 2 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten mit den Blattnummern 1 bis 3.
Die Karten sind von dem Siegelverwahrer, Siegelnummer 15, des Ministeriums für Land​wirtschaft, Umwelt und Klimaschutz am 4. Mai 2021 unterzeichnet worden.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Dahme-Spreewald, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

Schutzzweck des Naturschutzgebietes ist es, die Funktion einer Kernzone innerhalb des „Biosphärenreservates Spreewald“ zu erfüllen.
Es werden insbesondere naturnahe Erlenbruch- und Erlen-Eschen-Wälder auf Niedermoorstandorten der direkten menschlichen Einflussnahme entzogen und der natürlichen Entwicklung überlassen.

#### § 4 Verbote, Zulässige Handlungen

(1) Im Naturschutzgebiet „Naturentwicklungsgebiet Wisianka“ ist jegliche wirt​schaftliche Nutzung verboten.
Die Ausübung der traditionellen Spreewaldfischerei bleibt zulässig.

(2) Im Übrigen gelten weiterhin die Regelungen für die Schutzzone II gemäß § 4 Absatz 3 Nummer 21 (Naturschutzgebiet „Innerer Oberspreewald“) in Verbindung mit § 5 Absatz 1 Nummer 3 und 5 und den §§ 6, 7 und 9 der Verordnung über die Festsetzung von Naturschutzgebieten und einem Landschaftsschutzgebiet von zentraler Bedeutung mit der Gesamtbezeichnung „Biosphärenreservat Spreewald“.

#### § 5 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 6 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 7 Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(2) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 8 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 9 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 4.
Juni 2021

Der Minister für Landwirtschaft,  
Umwelt und Klimaschutz

Axel Vogel

* * *

### Anlagen

1

[Anlage (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Naturentwicklungsgebiet Wisianka“](/br2/sixcms/media.php/68/GVBl_II_59_2021%20Anlage.pdf "Anlage (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Naturentwicklungsgebiet Wisianka“") 604.4 KB