## Verordnung über das Naturschutzgebiet „Thymen“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S.
2542) in Verbindung mit § 19 Absatz 1 und 2 und § 78 Absatz 1 Satz 3 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S.
350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1   
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Oberhavel wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Thymen“.

### § 2   
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 809 Hektar.
Es umfasst Flächen in folgenden Fluren:

Stadt:

Gemarkung:

Flur:

Fürstenberg/Havel

Altthymen

1, 2, 3;

Fürstenberg/Havel

Fürstenberg/Havel

1, 2, 3, 5, 7, 8.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte Übersichtskarte im Maßstab 1 : 20 000 dient der räumlichen Einordnung des Naturschutzgebietes.
Die in Anlage 2 Nummer 2 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 3 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 3 mit den Blattnummern 1 bis 14 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt, die gemäß Absatz 4 hinterlegt wird.

(3) Innerhalb des Naturschutzgebietes wird gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes eine Zone 1 als Naturentwicklungsgebiet festgesetzt, die der direkten menschlichen Einflussnahme entzogen ist und in dem Lebensräume und Lebensgemeinschaften langfristig ihrer natürlichen Entwicklung überlassen bleiben.
Die Zone 1 umfasst rund 142 Hektar und liegt in folgenden Fluren:

Stadt:

Gemarkung:

Flur:

Fürstenberg/Havel

Altthymen

3;

Fürstenberg/Havel

Fürstenberg/Havel

1, 3, 7, 8.

Die Grenze der Zone 1 ist in der in Anlage 2 Nummer 1 genannten Übersichtskarte, in den in Anlage 2 Nummer 2 genannten topografischen Karten mit den Blattnummern 2 und 3 sowie in den in Anlage 2 Nummer 3 aufgeführten Liegenschaftskarten mit den Blattnummern 1, 5, 6, 9, 10, 12, 13 und 14 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten und Flurstücksliste kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam, sowie beim Landkreis Oberhavel, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

### § 3   
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere Buchen-, Eschen-Erlenwälder, Erlen-Bruch-Sumpfwaldgesellschaften, Schwimmblatt- und Tauchflurengesellschaften nährstoffarmer Seen und Gesellschaften der Torfmoos-, Seggen- und Röhrichtmoore und Grünlandgesellschaften;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützter Arten, insbesondere Breitblättriges Knabenkraut (Dactylorhiza majalis), Steifblättriges Knabenkraut (Dactylorhiza incarnata), Rundblättriger Sonnentau (Drosera rotundifolia), Sumpfiris (Iris pseudacorus), Fieberklee (Menyanthes trifoliata), Zungenhahnenfuß (Ranunculus lingua), Krebsschere (Stratiotes aloides) und Sandstrohblume (Helichrysum arenarium);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- beziehungsweise Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Seeadler (Haliaeetus albicilla), Kranich (Grus grus), Große Rohrdommel (Botaurus stellaris), Schellente (Bucephala clangula), Heidelerche (Lullula arborea), Mittelspecht (Dendrocopus medius), Roter Milan (Milvus milvus), Gemeine Keiljungfer (Gomphus vulgatissimus), Grüne Mosaikjungfer (Aeshna viridis), Nordische Moosjungfer (Leucorrhinia rubicunda) und Sumpfschrecke (Stethophyma grossum);
4.  die Erhaltung von Stand- und Fließgewässern, wie nährstoffarme Klarwasserseen, natürlich eutrophe Seen, Moorseen und Sölle mit ihren Binneneinzugsgebieten;
5.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit einer reich gegliederten Landschaft mit ausgedehnten Wäldern und einem Mosaik von Biotopen und Landschaftselementen, wie zum Beispiel Feucht- und Frischwiesen, Mager- und Trockenrasen, artenreichen Äckern, Hecken und Alleen;
6.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Biotopverbundes zwischen den Lychener und Fürstenberger Gewässern sowie den Havelgewässern.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Thymen“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinen Vorkommen von

1.  Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Dystrophen Seen und Teichen, Flüssen der planaren bis montanen Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Pfeifengraswiesen auf kalkreichem Boden, torfigen oder tonig-schluffigen Böden (Molinion caeruleae), Übergangs- und Schwingrasenmooren, Torfmoor-Schlenken (Rhynchosporion) und Kalkreichen Niedermooren als natürliche Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Schlucht- und Hangmischwäldern Tilio-Acerion, Auen-Wäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion, Alnion incanae, Salicion albae) und Moorwäldern als prioritäre natürliche Lebensraumtypen im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
3.  Biber (Castor fiber), Fischotter (Lutra lutra), Bachneunauge (Lampetra planeri), Rapfen (Aspius aspius), Großer Moosjungfer (Leucorrhinia pectoralis), Bauchiger Windelschnecke (Vertigo moulinsiana) und Kleiner Flussmuschel (Unio crassus) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

(3) Darüber hinaus ist besonderer Schutzzweck der Zone 1 (Naturentwicklungsgebiete) die weitgehend eigen-dynamische und störungsfreie Entwicklung naturnaher Waldgesellschaften, Seen und Moore sowie deren wissenschaftliche Untersuchung.

### § 4   
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet, seinen Naturhaushalt oder einzelne seiner Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist außerhalb der Zone 1 und außerhalb von Bruchwäldern, Röhrichten, Feuchtwiesen und Mooren das Betreten zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 11 jeweils nach dem 30.
    Juni eines jeden Jahres;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb von Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, und Waldbrandwundstreifen zu reiten;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen; ausgenommen ist das Befahren des Großen Schwaberowsees und des Thymensees mit muskelkraftbetriebenen Booten außerhalb von Röhrichten und Schwimmblattgesellschaften.
    Das Einsetzen und Anlegen der Boote ist nur an den Stegen sowie an der in § 2 Absatz 2 aufgeführten topografischen Karte mit der Blattnummer 2 und an den in § 2 Absatz 2 aufgeführten Liegenschaftskarten mit den Blattnummern 1 und 6 eingezeichneten Uferabschnitten zulässig;
13.  zu baden oder zu tauchen; am Großen Schwaberowsee und am Thymensee ist das Baden und Tauchen von den in den in § 2 Absatz 2 aufgeführten topografischen Karten mit der Blattnummer 2 und den in § 2 Absatz 2 aufgeführten Liegenschaftskarten mit den Blattnummern 1 und 6 gekennzeichneten Stellen sowie vom Boot und von den Stegen aus zulässig;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle, Rückstände aus Biogasanlagen) und Sekundärrohstoffdünger (zum Beispiel Abwasser, Klärschlamm und Bioabfälle, wie zum Beispiel Schlempe) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland umzubrechen, neu anzusäen oder nachzusäen.

### § 5   
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Anforderungen und Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen außerhalb der Zone 1 mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und vergleichbare Rückstände aus Biogasanlagen oder Sekundärrohstoffdünger, wie zum Beispiel solche aus Abwasser, Klärschlamm oder Bioabfälle, einzusetzen,
    2.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt, wobei bei Narbenschäden eine umbruchlose Nachsaat zulässig bleibt;

2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang außerhalb der Zone 1 auf den bisher rechtmäßig genutzten Flächen mit der Maßgabe, dass
    1.  der in § 3 Absatz 2 Nummer 2 genannte Lebensraumtyp „Moorwälder“ auf den Flurstücken 7, 9, 12 der Flur 1, dem Flurstück 2 der Flur 2 und dem Flurstück 81 der Flur 3 jeweils in der Gemarkung Fürstenberg/Havel sowie dem Flurstück 64/2 der Flur 3 der Gemarkung Altthymen nicht bewirtschaftet wird, im Übrigen eine Nutzung der Laubwälder einzelstamm- bis truppweise erfolgt,
    2.  auf den übrigen Flächen Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrates reduzieren, nur bis zu einer Größe von maximal 0,5 Hektar zulässig sind,
    3.  nur Baumarten der potenziell natürlichen Vegetation in gesellschaftstypischen Anteilen eingebracht werden, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
    4.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    5.  das Befahren des Waldes nur auf Wegen oder Rückegassen erfolgt,
    6.  der Boden unter Verzicht auf Umbruch bearbeitet wird; ausgenommen ist eine streifenweise, flachgründige, nicht in den Mineralboden eingreifende Bodenverwundung zur Unterstützung von Verjüngungsmaßnahmen,
    7.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß und einer Mindesthöhe von 5 Metern nicht gefällt werden und liegendes Totholz (ganze Bäume mit einem Durchmesser über 65 Zentimeter am stärksten Ende) im Bestand verbleibt,
    8.  § 4 Absatz 2 Nummer 17 und 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  innerhalb der Zone 1 nur Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne eines Monitorings mit Zustimmung der unteren Naturschutzbehörde zulässig sind.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    2.  auf den Flächen außerhalb der Zone 1:  
        aa)   Besatzmaßnahmen nur mit heimischen Fischarten erfolgen und der Besatz mit Karpfen unzulässig ist; im Übrigen bleibt § 13 der Fischereiordnung des Landes Brandenburg unberührt,  
        bb)   Fanggeräte und Fangmittel so einzusetzen sind, dass ein Einschwimmen und eine Gefährdung des Fischotters weitgehend ausgeschlossen sind,    
        cc)   § 4 Absatz 2 Nummer 19 gilt;
4.  die rechtmäßige Ausübung der Angelfischerei außerhalb der Zone 1 mit der Maßgabe, dass
    1.  die Angelfischerei im Thymensee und im Großen Schwaberowsee von den Booten, den Stegen sowie von den in der in § 2 Absatz 2 aufgeführten topografischen Karte mit der Blattnummer 2 und den in § 2 Absatz 2 aufgeführten Liegenschaftskarten mit den Blattnummern 1 und 6 eingezeichneten Uferabschnitten aus zulässig ist,
    2.  § 4 Absatz 2 Nummer 12, 19 und 20 gilt;
5.  für den Bereich der Jagd innerhalb der Zone 1:
    1.  Maßnahmen der Wildbestandsregulierung mit der Maßgabe, dass die Bestandsregulierung durch vier eintägige Gesellschaftsjagden im Zeitraum vom 1.
        November eines Jahres bis zum 31.
        Januar des Folgejahres erfolgt.
        Die Durchführung der Gesellschaftsjagden ist jeweils eine Woche vorher schriftlich bei der unteren Naturschutzbehörde anzuzeigen.
        Sonstige Maßnahmen der Bestandsregulierung sind nach Zulassung durch die untere Naturschutzbehörde zulässig.
        Dazu sind vom Antragsteller Erfordernis, Ziel, Art, Umfang, Zeitpunkt und Ort der Maßnahme darzulegen.
        Die Zulassung ist zu erteilen, wenn die Maßnahme dem Schutzzweck nicht oder nur unerheblich zuwiderläuft,
    2.  das Aufstellen transportabler und mobiler Ansitzeinrichtungen nach Anzeige bei der unteren Naturschutzbehörde;
6.  für den Bereich der Jagd außerhalb der Zone 1:
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass  
        aa) die Fallenjagd mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern zum Ufer aller innerhalb des Schutzgebietes liegenden Gewässer verboten ist,  
        bb)  keine Baujagd in einem Abstand von bis zu 100 Metern vom Ufer aller innerhalb des Schutzgebietes liegenden Gewässer vorgenommen wird,
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde erfolgt.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    3.  das Aufstellen transportabler und mobiler Ansitzeinrichtungen,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und der in § 3 Absatz 2 Nummer 1 genannten „Mageren Flachland-Mähwiesen“.Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern sind unzulässig, im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Brandenburgischen Jagdgesetzes unberührt;
7.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Instandhaltung und Instandsetzung der dem öffentlichen Verkehr gewidmeten Straßen und Wege hinsichtlich der Fahrbahn und des Banketts in der Zeit ab 1.
    Juli eines jeden Jahres, sofern eine Beschädigung des Gehölzbestandes ausgeschlossen ist.
    Die Maßnahmen sind der unteren Naturschutzbehörde vorab anzuzeigen.
    Alle Unterhaltungsmaßnahmen an sonstigen rechtmäßig bestehenden Anlagen und sonstige Maßnahmen an den dem öffentlichen Verkehr gewidmeten Straßen und Wegen außerhalb des genannten Zeitraums bedürfen des Einvernehmens mit der unteren Naturschutzbehörde;
8.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
9.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
10.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidungen rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
11.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch außerhalb der Zone 1 nach dem 30.
    Juni eines jeden Jahres;
12.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
13.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind, unter anderem Maßnahmen zur Bekämpfung der Spätblühenden Traubenkirsche;
14.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 an Straßen und Wegen freigestellt;
15.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit Und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

### § 6   
Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  am Auslauf des Thymensees soll zur Wasserstandsanhebung die Errichtung einer Sohlschwelle vorgesehen werden;
2.  in Altthymen wird der Rückbau des Sohlabsturzes des Thymenfließes angestrebt;
3.  im Hegesteinbach, Schwaberowfließ und Thymenfließ wird durch Anschluss von Altarmen, Sohlerhöhungen und Einbringen von Abflusshindernissen die Verbesserung der Strukturgüte angestrebt;
4.  in den Mooren südlich des Weges nach Altthymen sowie westlich Damshöhe, in der Mühlenfließniederung, am Grenzgraben, im Bruchwald westlich Altthymen, im Gemeindebruch und im Seilbruch werden wasserstandsstabilisierende Maßnahmen, insbesondere durch den Verschluss von Abflüssen, Waldumbaumaßnahmen sowie den Einbau von Sohlschwellen, angestrebt;
5.  es sollen Maßnahmen zur Bekämpfung der Spätblühenden Traubenkirsche eingeleitet werden.

### § 7   
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

### § 8   
Ordnungswidrigkeiten

(1)Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 dieser Verordnung zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

### § 9   
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere §§ 31 bis 33 und 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

### § 10   
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde in Potsdam, geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

### § 11   
Inkrafttreten, Außerkrafttreten

(1) § 5 Absatz 1 Nummer 1 dieser Verordnung tritt am 1.
Juli 2013 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig tritt die Verordnung über das Naturschutzgebiet Thymen, Kreis Templin vom 15.
März 1933 (Amtsblatt der Preußischen Regierung in Potsdam, Stück 16 vom 25.
März 1933) außer Kraft.

Potsdam, den 16.
August 2012

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

Anlage 1
--------

(zu § 2 Absatz 1)

![Das Naturschutzgebiet Thymen hat eine Größe von rund 809 Hektar und liegt im Landkreis Oberhavel im Bereich der Stadt Fürstenberg/Havel und umfasst Bereiche der Gemarkungen Altthymen und Fürstenberg/Havel. ](/br2/sixcms/media.php/69/Nr.%2073-1.GIF "Das Naturschutzgebiet Thymen hat eine Größe von rund 809 Hektar und liegt im Landkreis Oberhavel im Bereich der Stadt Fürstenberg/Havel und umfasst Bereiche der Gemarkungen Altthymen und Fürstenberg/Havel. ")

Anlage 2 
---------

(zu § 2 Absatz 2)

### **1.
Übersichtskarte Maßstab 1 : 20 000**

**Titel:** Übersichtskarte zur Verordnung über das Naturschutzgebiet „Thymen“

**Blattnummer**

**Unterzeichnung**

1

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV), am 18.
Mai 2012

### **2.
Topografische Karten Maßstab 1 : 10 000**

**Titel:** Topografische Karte zur Verordnung über das Naturschutzgebiet „Thymen“

Blattnummer

Unterzeichnung

1

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

2

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

3

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

### **3.
Liegenschaftskarten Maßstab 1 : 2 500**

**Titel:** Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Thymen“

Blattnummer

Gemarkung

Flur

Unterzeichnung

1

Fürstenberg/Havel

1

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

2

Fürstenberg/Havel

1

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

3

Altthymen

1, 2, 3

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

4

Altthymen

1

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

5

Fürstenberg/Havel

1

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

6

Altthymen  
Fürstenberg/Havel

3  
1, 2

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

7

Altthymen

1, 2, 3

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

8

Fürstenberg/Havel

1

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

9

Altthymen  
Fürstenberg/Havel

3  
1, 2

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

10

Altthymen  
Fürstenberg/Havel

3  
1, 2, 3

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

11

Fürstenberg/Havel

2, 3

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

12

Fürstenberg/Havel

1, 2, 3, 5, 7, 8

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

13

Fürstenberg/Havel

2, 3, 5

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012

14

Fürstenberg/Havel

5, 7, 8

unterzeichnet vom Siegelverwahrer, Siegelnummer 22 des MUGV, am 18.
Mai 2012