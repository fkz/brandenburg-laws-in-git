## Verordnung über die Übertragung von Ermächtigungen zum Erlass von Rechtsverordnungen im Bereich der Finanzverwaltung auf den Minister der Finanzen (Finanzverwaltung-Ermächtigungsübertragungsverordnung - FVEÜV)

Auf Grund

*   des § 2 Absatz 2 Satz 2 und Absatz 3 Satz 2, § 5 Absatz 1 Satz 1 Nummer 11 Satz 10, § 17 Absatz 2 Satz 4 und Absatz 3 Satz 3 des Finanzverwaltungsgesetzes in der Fassung der Bekanntmachung vom 4.
    April 2006 (BGBl. I S. 846, 1202), von denen § 5 Absatz 1 Nummer 11 Satz 10 durch Artikel 4 des Gesetzes vom 8.
    Dezember 2016 (BGBl. I S. 2835, 2836) geändert worden ist,
*   des § 19 Absatz 5 Satz 2, § 88b Absatz 3 Satz 2, § 387 Absatz 2 Satz 5 und § 409 Satz 2 der Abgabenordnung in der Fassung der Bekanntmachung vom 1.
    Oktober 2002 (BGBl. I S. 3866; 2003 I S. 61), von denen § 88b Absatz 3 Satz 2 durch Artikel 1 des Gesetzes vom 18.
    Juli 2016 (BGBl. I S.1679) eingefügt und § 387 Absatz 2 Satz 5 durch Artikel 5 des Gesetzes vom 3.
    Dezember 2015 (BGBl. I S. 2178, 2181) geändert worden ist,
*   des § 14 Absatz 3 Satz 2 des Fünften Vermögensbildungsgesetzes in der Fassung der Bekanntmachung vom 4.
    März 1994 (BGBl. I S. 406), § 8 Absatz 2 Satz 2 des Wohnungsbau-Prämiengesetzes in der Fassung der Bekanntmachung vom 30.
    Oktober 1997 (BGBl. I S. 2678) und § 29a Absatz 2 des Berlinförderungsgesetzes in der Fassung der Bekanntmachung vom 2.
    Februar 1990 (BGBl. I S. 173) jeweils in Verbindung mit § 387 Absatz 2 Satz 5 sowie § 409 Satz 2 der Abgabenordnung,
*   des § 164 Satz 1 des Steuerberatungsgesetzes in der Fassung der Bekanntmachung vom 4. November 1975 (BGBl. I S. 2735), der zuletzt durch Artikel 9 des Gesetzes vom 18. August 1980 (BGBl. I S. 1537, 1543) geändert worden ist, und § 20 des Berlinförderungsgesetzes jeweils in Verbindung mit § 387 Absatz 2 Satz 5 der Abgabenordnung,
*   des § 31 Absatz 2 Satz 3 des Steuerberatungsgesetzes, der durch Artikel 1 des Gesetzes vom 8.
    April 2008 (BGBl. I S. 666) eingefügt worden ist,
*   des § 131 Absatz 3 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
    Februar 1987 (BGBl. I S. 602) in Verbindung mit § 409 Satz 2 der Abgabenordnung,

verordnet die Landesregierung:

#### § 1 Übertragung von Ermächtigungen

Die Ermächtigungen zum Erlass von Rechtsverordnungen aufgrund

1.  des § 2 Absatz 2 Satz 1, Absatz 3 Satz 1, § 5 Absatz 1 Nummer 11 Satz 8 sowie § 17 Absatz 2 Satz 3 und Absatz 3 Satz 1 des Finanzverwaltungsgesetzes,
2.  des § 19 Absatz 5 Satz 1 und § 88b Absatz 3 Satz 1 der Abgabenordnung,
3.  des § 387 Absatz 2 Satz 1 und 2 und § 409 Satz 2 der Abgabenordnung auch in Verbindung mit § 14 Absatz 3 Satz 2 des Fünften Vermögensbildungsgesetzes, § 8 Absatz 2 Satz 2 des Wohnungsbau-Prämiengesetzes, der §§ 20 und 29a Absatz 2 des Berlinförderungsgesetzes, § 164 Satz 1 des Steuerberatungsgesetzes und § 131 Absatz 3 des Gesetzes über Ordnungswidrigkeiten,
4.  des § 31 Absatz 2 Satz 1 des Steuerberatungsgesetzes

werden auf den Minister der Finanzen übertragen.

#### § 2 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über die Übertragung von Ermächtigungen zum Erlass von Rechtsverordnungen im Bereich der Finanzverwaltung auf den Minister der Finanzen vom 23.
August 1991 (GVBl.
II S. 390), die zuletzt durch Verordnung vom 20.
November 2017 (GVBl.
II Nr. 63) geändert worden ist, außer Kraft.

Potsdam, den 16.
September 2019

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Minister der Finanzen

Christian Görke