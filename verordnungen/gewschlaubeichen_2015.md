## Verordnung über den Schutzwald „Generhaltungswald Schlaubetaler Eichen“

Auf Grund des § 12 des Waldgesetzes des Landes Brandenburg vom 20. April 2004 (GVBl. I S. 137) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzwald

Die in § 2 näher bezeichneten Waldflächen mit teilweise besonderer Schutzfunktion als Naturwald werden zum Schutzwald erklärt.
Der Schutzwald trägt die Bezeichnung „Generhaltungswald Schlaubetaler Eichen“ und wird in das Register der geschützten Waldgebiete aufgenommen.

#### § 2 Schutzgegenstand

(1) Das geschützte Waldgebiet befindet sich im Landkreis Oder-Spree und hat eine Größe von rund 162 Hektar.
Es umfasst folgende Flurstücke:

**Gemeinde:**

**Gemarkung:**

**Flur:**

**Flurstück:**

Schlaubetal

Bremsdorf

4

1, 3 (anteilig), 53 (anteilig), 54, 55 (anteilig), 56 (anteilig), 57, 58 (anteilig);

Siehdichum

Schernsdorf

4

36 (anteilig), 37, 106 (anteilig), 107 (anteilig), 108;

5

45, 48, 49, 50 (anteilig), 53, 82 (anteilig), 117, 155, 211 (anteilig),  
212 (anteilig), 214 bis 218, 251, 253, 255, 257, 259, 261, 263, 269, 272.

Die Waldfläche mit besonderer Schutzfunktion als Naturwald, bezeichnet als „Naturwald Fünfeichen“, besteht aus einem Traubeneichenwald und hat eine Größe von rund 20 Hektar.
Sie umfasst folgendes Flurstück:

**Gemeinde:**

**Gemarkung:**

**Flur:**

**Flurstück:**

Siehdichum

Schernsdorf

5

218 (anteilig).

(2) Zur Orientierung ist dieser Verordnung eine Kartenskizze über die Lage des Schutzwaldes und des Naturwaldes als Anlage beigefügt.

(3) Die Grenze des Schutzwaldes ist in der „Topografischen Karte zur Verordnung über den Schutzwald‚ Generhaltungswald Schlaubetaler Eichen‘“, Maßstab 1 : 10 000 und in der „Liegenschaftskarte zur Verordnung über den Schutzwald ‚Generhaltungswald Schlaubetaler Eichen‘“, Maßstab 1 : 7 500 mit ununterbrochener Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Liegenschaftskarte.
Die Karten sind mit dem Dienstsiegel des Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft, Siegelnummer 19, versehen und vom Siegelverwahrer am 26. Januar 2015 unterschrieben worden.

(4) Die Verordnung mit Karten kann bei dem für Forsten zuständigen Fachministerium des Landes Brandenburg, oberste Forstbehörde, in Potsdam sowie beim Landesbetrieb Forst Brandenburg in Potsdam, untere Forstbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Schutzwaldes ist

1.  die Erhaltung und Entwicklung eines repräsentativen Ausschnitts des größten zusammenhängenden Traubeneichenwaldes Brandenburgs;
2.  die Sicherung der Dauerbeobachtungsfläche des Europäischen Forstlichen Umweltmonitorings (Level II);
3.  die Sicherung der genetischen Ressourcen der autochthonen Traubeneichen;
4.  die Bewahrung seiner besonderen Eigenart und hervorragenden Schönheit;
5.  die Erhaltung und Wiederherstellung der Leistungsfähigkeit des Naturhaushaltes;
6.  die Erhaltung und Entwicklung des Gebietes als Lebensraum gefährdeter Tier- und Pflanzenarten.

(2) Der besondere Schutzzweck ist der Erhalt des Naturwaldes insbesondere zur

1.  Repräsentation eines besonders wertvollen Restbestandes mit über 300-jährigen Traubeneichen;
2.  langfristigen wissenschaftlichen Erforschung der durch den Menschen nicht direkt beeinflussten Waldentwicklung;
3.  Erforschung der Waldstruktur, des Bodens, der Flora und der Fauna;
4.  Nutzung als lokale und regionale Weiserfläche zur Ableitung und exemplarischen Veranschaulichung von Erkenntnissen für die Waldbaupraxis und für die forstliche Lehre;
5.  Erhaltung und natürlichen Regeneration forstgenetischer Ressourcen;
6.  Erhaltung der floristischen und faunistischen Artenvielfalt in sich natürlich entwickelnden Lebensgemeinschaften.

(3) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teils des Gebietes von gemeinschaftlicher Bedeutung „Teufelssee und Urwald Fünfeichen“ mit der Gebietsnummer DE 3852-305 im Sinne des § 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes mit seinem Vorkommen von

1.  alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur als Biotop von gemeinschaftlichem Interesse („natürlicher Lebensraumtyp“ im Sinne des Anhangs I der Richtlinie 92/43/EWG);
2.  Hirschkäfer (Lucanus cervus) als prioritäre Tierart von gemeinschaftlichem Interesse (im Sinne des Anhangs II der Richtlinie 92/43/EWG) einschließlich seiner für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

#### § 4 Verbote, Maßgaben zur forstwirtschaftlichen Bodennutzung

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind im Schutzwald gemäß § 12 Absatz 6 des Waldgesetzes des Landes Brandenburg alle Handlungen verboten, die dem in § 3 genannten Schutzzweck zuwiderlaufen und das Gebiet oder einzelne seiner Bestandteile nachhaltig stören, verändern, beschädigen oder zerstören können.

(2) Es ist insbesondere verboten:

1.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
2.  das Gebiet außerhalb der Waldwege zu betreten;
3.  im Gebiet mit motorisierten Fahrzeugen und Gespannen zu fahren oder diese dort abzustellen;
4.  zu lagern, zu zelten, Wohnwagen aufzustellen, zu rauchen und Feuer zu entzünden;
5.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
6.  die Bodengestalt zu verändern, Böden zu verfestigen und zu versiegeln;
7.  Düngemittel einschließlich Wirtschaftsdünger und Sekundärrohstoffdünger zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
8.  Pflanzenschutzmittel jeder Art oder Holzschutzmittel anzuwenden;
9.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
10.  Vieh weiden zu lassen und dazu erforderliche Einrichtungen zu schaffen.

(3) Über die Verbote der Absätze 1 und 2 hinaus ist im „Naturwald Fünfeichen“ die forstwirtschaftliche Nutzung verboten.

(4) Die ordnungsgemäße forstwirtschaftliche Bodennutzung gemäß den in § 4 des Waldgesetzes des Landes Brandenburg genannten Anforderungen und Grundsätzen bleibt auf den bisher rechtmäßig dafür genutzten Flächen außerhalb des Naturwaldes mit der Maßgabe zulässig, dass

1.  die Holznutzung ausschließlich durch Einzelstammentnahme mit dem Ziel der Erhaltung und Entwicklung der in § 3 Absatz 3 genannten Biotope erfolgt;
2.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden sowie zwei Stück liegendes Totholz mit einem Durchmesser von mehr als 65 Zentimetern am stärksten Ende als ganzer Baum im Bestand verbleibt;
3.  je Hektar mindestens fünf dauerhaft markierte Altbäume mit einem Brusthöhendurchmesser von über 40 Zentimetern bis zum Absterben und Zerfall aus der Nutzung genommen werden;
4.  bei Holzerntemaßnahmen Schäden am Bestand und Boden vermieden werden sowie kein flächiges Befahren der Bestände erfolgt;
5.  die Waldverjüngung kleinflächig, bis maximal ein Hektar Verjüngungsfläche, möglichst unter kurzfristiger Schirmstellung, vorrangig durch Naturverjüngung oder Saat mit zugelassenem Forstvermehrungsgut aus dem Schutzwald erfolgt;
6.  keine flächige tiefgreifend in den Mineralboden eingreifende Bodenverwundung erfolgt,
7.  Stubben nicht gerodet und Bäume mit Horsten und Höhlen nicht gefällt werden.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen;
2.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Forstbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Schutzwaldes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Forstbehörden und sonstige von den Forstbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer und Nutzer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums.

#### § 6 Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  die Waldbewirtschaftung erfolgt mischungsregulierend zugunsten der Erhaltung und Regenerierung der im Schutzwald überwiegenden Waldgesellschaft Waldreitgras-Traubeneichenwald (Calamagrostis-Quercetum petraea HOFMANN 1997) mit der natürlich vorkommenden Baumartenmischung;
2.  Maßnahmen zur Entstehung sowie Erhaltung und Entwicklung von günstigen Habitatstrukturen für gefährdete Pflanzen, Tiere und Pilze des Traubeneichenwaldes sollen durchgeführt werden.

#### § 7 Ausnahmen, Befreiungen

(1) Aus Gründen des Waldschutzes und zur Nutzung nach Naturereignissen kann die untere Forstbehörde auf Antrag Ausnahmen von den Verboten dieser Verordnung zulassen, sofern der Schutzzweck nicht beeinträchtigt wird.

(2) Wenn überwiegende Gründe des Gemeinwohls es erfordern, kann die untere Forstbehörde im Einvernehmen mit der unteren Naturschutzbehörde auf Antrag Befreiungen von den Verboten dieser Verordnung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 37 Absatz 1 Nummer 7 des Waldgesetzes des Landes Brandenburg handelt, wer vorsätzlich oder fahrlässig den Regelungen der §§ 4 und 6 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 37 Absatz 3 des Waldgesetzes des Landes Brandenburg mit einer Geldbuße bis zu zwanzigtausend Euro geahndet werden.

#### § 9 Verhältnis zu anderen rechtlichen Vorschriften

(1) Die Regelungen naturschutzrechtlicher Schutzgebietsausweisungen im Bereich des in § 2 genannten Gebietes bleiben unberührt.

(2) Die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) bleiben unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 1 der Waldschutzgebietsverfahrensverordnung genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber der obersten Forstbehörde geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsprozess sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 2.
März 2015

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

### Anlagen

1

[Schutzwald-GEW-Schlaubetaler-Eichen-Anlage](/br2/sixcms/media.php/68/Schutzw-GEWSchlaubEichen__an__anlg__001Anlage-zum-Hauptdokument.pdf "Schutzwald-GEW-Schlaubetaler-Eichen-Anlage") 833.2 KB