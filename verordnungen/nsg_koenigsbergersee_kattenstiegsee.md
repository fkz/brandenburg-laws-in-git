## Verordnung über das Naturschutzgebiet „Königsberger See, Kattenstiegsee“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl. I S. 2542) in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nr. 43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche in den Landkreisen Ostprignitz-Ruppin und Prignitz wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Königsberger See, Kattenstiegsee“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 225 Hektar.
Es umfasst zwei Teilflächen („Teilfläche 1“ für den Kattenstiegsee und Umgebung und „Teilfläche 2“ für den Königsberger See und Umgebung) in folgenden Fluren:

Landkreis:

Gemeinde:

Gemarkung:

Flur:

Ostprignitz-Ruppin

Heiligengrabe

Herzsprung

3;

 

 

Königsberg

1, 2, 3, 11;

Prignitz

Gumtow

Wutike

3.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 4 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 7 aufgeführten Liegenschaftskarten.
Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 3 beigefügt, die gemäß Absatz 4 hinterlegt wird.

(3) Innerhalb des Naturschutzgebietes werden gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes eine Zone 1 und eine Zone 2 mit Beschränkungen der Nutzung festgesetzt.
Die Grenzen der Zonen 1 und 2 sind in den in Anlage 2 Nummer 1 genannten topografischen Karten mit den Blattnummern 1 bis 4 sowie in den in Anlage 2 Nummer 2 genannten Liegenschaftskarten mit den Blattnummern 1 bis 7 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten und Flurstücksliste kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie bei den Landkreisen Ostprignitz-Ruppin und Prignitz, untere Naturschutzbehörden, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes als Teil einer eiszeitlich entstandenen Schmelzwasserabflussrinne im Übergangsbereich zwischen der Kyritzer Platte und der Dosseniederung ist

1.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere von Seggenrieden, Mooren, Moor- und Bruchwäldern, Laubholzmischwäldern, Waldmänteln, Schwimmblatt- und Verlandungsgesellschaften, Pfeifengraswiesen, Feuchtwiesen sowie der Pflanzengesellschaften an Quellen und naturnahen unverbauten Bachabschnitten;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Fieberklee (Menyanthes trifoliata), Sumpf-Herzblatt (Parnassia palustris), Weiße Seerose (Nymphaea alba), Gelbe Teichrose (Nuphar lutea), Torfmoose (Sphagnum spec.), Sumpf-Schlangenwurz (Calla palustris), Breitblättriges Knabenkraut (Dactylorhiza majalis), Gemeine Grasnelke (Armeria maritima ssp.
    elongata), Sumpf-Stendelwurz (Epipactis palustris) und Sand-Strohblume (Helichrysum arenarium);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Vögel, Reptilien, Amphibien, Insekten und Mollusken, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Eisvogel (Alcedo atthis), Bekassine (Gallinago gallinago), Tüpfelsumpfhuhn (Porzana porzana), Kiebitz (Vanellus vanellus), Kranich (Grus grus), Rohrdommel (Botaurus stellaris), Drosselrohrsänger (Acrocephalus arundinaceus), Rohrschwirl (Locustella luscinioides), Schwarzstorch (Ciconia nigra), Weißstorch (Ciconia ciconia), Seeadler (Haliaeetus albicilla), Fischadler (Pandion haliaetus), Baumfalke (Falco subbuteo), Rohrweihe (Circus aeruginosus), Schwarzmilan (Milvus migrans), Rotmilan (Milvus milvus), Ringelnatter (Natrix natrix), Zauneidechse (Lacerta agilis), Waldeidechse (Lacerta vivipara), Blindschleiche (Anguis fragilis), Teichmolch (Triturus vulgaris), Gebänderte Prachtlibelle (Calopteryx splendens), Große Teichmuschel (Anodonta cygnea) und Gemeine Malermuschel (Unio pictorum);
4.  die Erhaltung und Entwicklung des Gebietes in seiner Funktion als Rastgebiet für Zugvögel, insbesondere nordische Gänse, wie Saat- (Anser fabalis) und Blessgänse (Anser albifrons);
5.  die Erhaltung und Entwicklung des Gebietes in seiner Funktion als Lebens- und Nahrungsraum für überwinternde Wasservogelarten wie Gänse und Gänsesäger (Mergus merganser) sowie für Wiesenweihe (Circus pygargus) und Kornweihe (Circus cyaneus);
6.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Bestandteil des regionalen Biotopverbundes insbesondere für wassergebundene Arten zwischen den Naturschutzgebieten „Bückwitzer See und Rohrlacker Graben“, „Königsfließ“ und „Mühlenteich“ sowie den Fließgewässern Jäglitz, Karthane und Dosse.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teiles des Gebietes von gemeinschaftlicher Bedeutung „Königsberger See, Kattenstiegsee“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinen Vorkommen von

1.  Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions und Hydrocharitions, Flüssen der planaren bis montanen Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Kalkreichen Niedermooren, Übergangs- und Schwingrasenmooren und Feuchten Hochstaudenfluren der planaren Stufe als natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Auenwäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion, Alnion incanae, Salicion albae) als prioritärem natürlichen Lebensraumtyp im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
3.  Fischotter (Lutra lutra), Biber (Castor fiber), Kammmolch (Triturus cristatus), Rotbauchunke (Bombina bombina), Bauchiger Windelschnecke (Vertigo moulinsiana) sowie Schmaler Windelschnecke (Vertigo angustior) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  zu baden, zu tauchen oder zu schnorcheln;
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter und Luftmatratzen zu benutzen;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel aller Art zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide mit einer Besatzdichte von maximal 1,4 Raufutter verwertenden Großvieheinheiten (RGV) pro Hektar im Jahresmittel genutzt wird und § 4 Absatz 2 Nummer 17 gilt,
    2.  Grünland in der Zone 2 als Wiese oder Weide genutzt wird.
        Die Düngung mit Festmist ist zulässig, wobei die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel einschließlich der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht.
    3.  § 4 Absatz 2 Nummer 23 und 24 gilt, wobei eine umbruchlose Nachsaat zulässig bleibt,
    4.  an Gewässerufern bis zu einem Abstand von 2 Metern von der oberen Böschungskante keine Nutzung erfolgt, wovon die Flurstücke 68, 77, 89, 90, 98 bis 103, 105 bis 110, 118, 121, 380, 381, 383, 384, 386 und 388 der Flur 2 in der Gemarkung Königsberg sowie das Flurstück 83 der Flur 3 in der Gemarkung Königsberg ausgenommen sind,
    5.  die Mulchmahd und das Walzen unzulässig sind sowie in Bereichen mit Wiesenbrütern das Schleppen von Grünland im Zeitraum vom 1.
        April bis zur ersten Nutzung eines jeden Jahres unzulässig bleibt,
    6.  in der Zone 1 zusätzlich eine Beweidung mit Rindern und Pferden unzulässig ist und eine Mahd erst ab September eines jeden Jahres und unter Schonung der Torfmoosbestände erfolgt;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  in den in § 3 Absatz 2 genannten Auen-Wäldern mit Alnus glutinosa (Schwarz-Erle) und Fraxinus excelsior (Gewöhnliche Esche) sowie in Erlenbruchwäldern eine Nutzung ausschließlich einzelstamm- bis horstweise erfolgt und die Walderneuerung ausschließlich durch Naturverjüngung und ohne Bodenbearbeitung erfolgt, wobei diese Flächen nur bei Frost befahren werden,
    2.  auf den nicht unter § 5 Absatz 1 Nummer 2 Buchstabe a genannten Waldflächen nur Arten der potenziell natürlichen Vegetation in lebensraumtypischen Anteilen eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
    3.  ein Altholzanteil von mindestens 10 Prozent am aktuellen Bestandesvorrat zu sichern ist.
        Dabei sind, sofern vorhanden, mindestens fünf Stämme je Hektar mit einem Brusthöhendurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum Absterben aus der Nutzung zu nehmen und dauerhaft zu markieren; in Jungbeständen ist ein solcher Altholzanteil zu entwickeln,
    4.  je Hektar mindestens fünf Stück stehendes Totholz (mehr als 30 Zentimeter Brusthöhendurchmesser in 1,30 Meter über dem Stammfuß und eine Mindesthöhe von 5 Metern) nicht gefällt werden; liegendes Totholz verbleibt im Bestand,
    5.  Bäume mit Horsten oder Höhlen nicht gefällt werden dürfen,
    6.  § 4 Absatz 2 Nummer 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass ein Einschwimmen und eine Gefährdung von Biber, Fischotter und tauchenden Vogelarten wie Haubentaucher und Gänsesäger weitgehend ausgeschlossen ist,
    2.  am Lellichowsee Besatzmaßnahmen und die Elektrofischerei nur nach einem einvernehmlich mit der unteren Naturschutzbehörde abgestimmten Hegeplan zulässig sind.
        Bis zur Vorlage des Hegeplans sind Besatzmaßnahmen und die Elektrofischerei mit Zustimmung der unteren Naturschutzbehörde zulässig.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck, insbesondere die Bestände des Kammmolches und der Rotbauchunke, nicht beeinträchtigt werden,
    3.  sich die Nutzung des „Mittleren Wutiker Torflochs“, das in den in § 2 Absatz 2 genannten topografischen Karten mit Buchstabe „B“ gekennzeichnet ist, auf erforderliche Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne einer Fischbestandskontrolle, -regulierung und -förderung mit Zustimmung der unteren Naturschutzbehörde beschränkt; die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    4.  ein in mehrjährigen Abständen durchgeführter Schnitt abgestorbener Teile von Schilf- und Röhrichtbeständen am Westufer und am nordöstlichen Ufer des Königsberger Sees zwischen Badeanstalt und Anlage des Angelsportvereins Königsberg und an den Wutiker Torflöchern auf Eis mit Zustimmung der unteren Naturschutzbehörde zulässig ist, wobei Bereiche zur Erhaltung von Altschilf von der Mahd auszunehmen sind,
    5.  § 4 Absatz 2 Nummer 19 gilt;
4.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass
    1.  diese am Lellichowsee nur vom Ufer aus zulässig ist und die Anzahl der gleichzeitig gültigen Angelkarten auf 25 beschränkt ist,
    2.  diese an den Wutiker Torflöchern nur an den in den in § 2 Absatz 2 genannten topografischen Karten mit Buchstabe „A“ und „B“ gekennzeichneten Torflöchern und dort nur an den gekennzeichneten Angelbereichen vom Ufer aus zulässig ist,
    3.  diese auf dem Königsberger See und auf dem Kattenstiegsee nur vom Boot aus oder nur an den Angel- und Bootsanlegebereichen zulässig ist, die in den in § 2 Absatz 2 genannten topografischen Karten mit einem Symbol gekennzeichnet sind, wobei § 5 Absatz 1 Nummer 6 Buchstabe a und c gilt;
    4.  diese auf dem Kattenstiegsee vom Eis aus und auf dem Königsberger See vom Eis aus auf den mit dem Boot befahrbaren Bereichen gemäß § 5 Absatz 1 Nummer 6 Buchstabe b zulässig ist,
    5.  diese im Radius von 50 Metern um Biberburgen und Fischotterbaue unzulässig ist, wobei die Angelfischerei an den in den in § 2 Absatz 2 genannten topografischen Karten gekennzeichneten Angel- und Bootsanlegebereichen zulässig bleibt,
    6.  das Befahren von Verlandungsbereichen, Röhrichten und Schwimmblattgesellschaften verboten bleibt und am Königsberger See § 5 Absatz 1 Nummer 6 Buchstabe b gilt,
    7.  § 4 Absatz 2 Nummer 19 und 20 gilt;
5.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        in der Zeit vom 1.
        März bis zum 15.
        Juni eines jeden Jahres die Jagd nur vom Ansitz aus erfolgt,
        
        bb)
        
        die Fallenjagd mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern von den Gewässerufern verboten ist,
        
        cc)
        
        keine Baujagd in einem Abstand von bis zu 100 Metern von den Gewässerufern vorgenommen wird,
        
        dd)
        
        in Teilfläche 2 des Naturschutzgebietes die Jagd auf Wasservögel ab dem 15.
        November eines jeden Jahres bis zum Ende der gesetzlich festgelegten Jagdzeit gestattet ist.
        Wird der Königsberger See in diesem Zeitraum von Zug- und Rastvögeln als Schlafplatz genutzt, bleibt die Wasservogeljagd im Zeitraum zwischen einer Stunde vor Sonnenuntergang und einer Stunde nach Sonnenaufgang unzulässig,
        
    2.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und außerhalb der in § 3 Absatz 2 Nummer 1 und 2 genannten Lebensraumtypen,
    3.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Aufstellung anzuzeigen.
        Die untere Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        Die Entscheidung hierüber soll unverzüglich erfolgen.
    
    Im Übrigen bleiben Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern unzulässig.
    Jagdrechtliche Regelungen nach § 41 des Jagdgesetzes des Landes Brandenburg bleiben unberührt;
    
6.  das Befahren des Kattenstiegsees und des Königsberger Sees mit Wasserfahrzeugen mit der Maßgabe, dass
    1.  der Kattenstiegsee ausschließlich von durch Muskelkraft betriebenen Booten befahren wird und die über die Gemeinde Heiligengrabe registrierte Anzahl von 30 anliegenden Booten nicht überschritten wird und das Anlegen nur an den Bootsanlegebereichen zulässig bleibt, die in den in § 2 Absatz 2 genannten topografischen Karten mit einem Symbol gekennzeichnet sind,
    2.  am Königsberger See ganzjährig zu Verlandungsbereichen, Röhrichten und Schwimmblattgesellschaften ein Mindestabstand von 15 Metern eingehalten wird, wobei das Anlegen an den Bootsanlegebereichen zulässig bleibt, die in den in § 2 Absatz 2 genannten topografischen Karten mit einem Symbol gekennzeichnet sind,
    3.  im Westteil des Königsberger Sees entsprechend der Einzeichnung in den in § 2 Absatz 2 genannten topografischen Karten in der Zeit von 1.
        September jeden Jahres bis 30.
        April des Folgejahres das Befahren mit Wasserfahrzeugen aller Art verboten bleibt;
7.  das Baden, das Tauchen und das Schnorcheln sowie die Nutzung von Luftmatratzen im Lellichowsee und im Königsberger See nur an den Badestellen, die in den in § 2 Absatz 2 genannten topografischen Karten mit einem Symbol gekennzeichnet sind;
8.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter die Nummer 10 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
9.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
10.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
11.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
12.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
13.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der zuständigen Naturschutzbehörde zugelassen oder angeordnet worden sind;
14.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Tourismus im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl.
    S. 1734), die durch die Bekanntmachung vom 1.
    Oktober 2013 (ABl.
    S. 2811) geändert worden ist, an Straßen und Wegen freigestellt;
15.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungserfordernissen, die sich aus anderen fachrechtlichen Vorgaben ergeben, unberührt.

#### § 6 Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  die am Ufer des Königsberger Sees liegende Orchideenwiese soll durch späte Mahd beziehungsweise Entbuschung offen gehalten werden;
2.  bei der Mahd des Grünlandes sollen keine Rotationsmähwerke eingesetzt werden;
3.  die Flächen an Gewässerufern bis zu einem Abstand von 2 Metern von der oberen Böschungskante gemäß § 5 Absatz 1 Nummer 1 Buchstabe d sollen durch eine abschnittsweise alternierende Mahd in mehrjährigem Abstand zur Verhinderung der Verbuschung gepflegt werden;
4.  bei der Gewässerunterhaltung soll die Mahd von Gewässer- und Grabenufern möglichst nur in mehrjährigen Abständen und dann abschnittsweise oder einseitig und nach dem 15.
    September eines jeden Jahres erfolgen und das Mähgut beräumt werden;
5.  das Retentionsvermögen des Königsberger Sees soll verbessert werden.
    Ein Wasserstand des Sees und in den unmittelbar westlich an den Königsberger See angrenzenden Grabenabschnitten von mindestens 50,6 Meter über Null (DHHN 92) soll im Rahmen eines wasserrechtlichen Verfahrens unter Berücksichtigung der angrenzenden Nutzungen angestrebt werden;
6.  der Entwässerung des an der Westgrenze der Teilfläche 2 des Naturschutzgebietes gelegenen Auenwaldes, des östlich daran angrenzenden Grünlandes und des in Teilfläche 1 des Naturschutzgebietes liegenden Verlandungs- und Durchströmungsmoorkomplexes soll entgegenwirkt werden;
7.  in den Moorbereichen am Ufer des Königsberger Sees und im Moorkomplex nördlich des Kattenstiegsees sollen bei Bedarf Hagerungsschnitte beziehungsweise Schnitte vordringender Schilfpflanzen und Entbuschungen durchgeführt werden;
8.  das in westlicher und nordöstlicher Richtung an den Lellichowsee anschließende Fließgewässer Klempnitz und der daran angrenzende Auenwald sollen renaturiert werden.
    Anschließende Nadelholzforsten sollen durch Voranbau und Schirmstellung des derzeitigen Bestandes zu naturnahen Laubholzwäldern umgebaut werden.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

§ 5 Absatz 1 Nummer 1 tritt am 1. Januar 2017 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 17.
November 2016

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Königsberger See, Kattenstiegsee“](/br2/sixcms/media.php/68/GVBl_II_65_2016-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Königsberger See, Kattenstiegsee“") 653.3 KB

2

[Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten](/br2/sixcms/media.php/68/GVBl_II_65_2016-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten") 606.8 KB