## Verordnung über Unterricht und Erziehung für Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf (Sonderpädagogik-Verordnung - SopV)

Auf Grund des § 31 in Verbindung mit § 11 Absatz 4, § 13 Absatz 3, § 56, § 57 Absatz 4, § 58 Absatz 3, § 59 Absatz 9, § 60 Absatz 4 und § 61 Absatz 3 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
August 2002 (GVBl.
I S. 78), von denen § 13 Absatz 3 durch Artikel 1 Nummer 10 des Gesetzes vom 8.
Januar 2007 (GVBl.
I S. 2), § 31 zuletzt durch Artikel 7 Nummer 6 des Gesetzes vom 25.
Januar 2016 (GVBl.
I Nr. 5) und § 56 zuletzt durch Artikel 2 Nummer 18 des Gesetzes vom 14.
März 2014 (GVBl.
I Nr. 14) geändert worden sind, verordnet der Minister für Bildung, Jugend und Sport im Benehmen mit dem für Bildung zuständigen Ausschuss des Landtages:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeines

[§ 1 Geltungsbereich, Ziele und Aufgaben](#1)

### Abschnitt 2  
Sonderpädagogische Förder- und Beratungsstellen

[§ 2 Aufgaben und Organisation](#2)

### Abschnitt 3  
Feststellung, Änderung und Beendigung des sonderpädagogischen Förderbedarfs

[§ 3 Feststellungsverfahren](#3)

[§ 4 Förderausschuss](#4)

[§ 5 Entscheidung des staatlichen Schulamtes](#5)

[§ 6 Fortführung, Änderung und Beendigung der sonderpädagogischen Förderung](#6)

### Abschnitt 4  
Gemeinsamer Unterricht

[§ 7 Grundsätze des gemeinsamen Unterrichts](#7)

[§ 8 Rahmenbedingungen, Klassenfrequenz und Lehrkräfteeinsatz](#8)

[§ 9 Unterrichtsorganisation, Stundentafeln und Rahmenlehrpläne](#9)

[§ 10 Aufrücken, Versetzen, Überspringen und Wiederholen](#10)

[§ 11 Leistungsbewertung, Erwerb von Abschlüssen, Berechtigungen, Zeugnisse](#11)

### Abschnitt 5  
Förderschulen und Förderklassen

[§ 12 Allgemeines](#12)

[§ 13 Struktur der Förderschulen und Förderklassen](#13)

[§ 14 Dauer des Schulbesuchs](#14)

[§ 15 Unterrichtsorganisation, Stundentafeln und Rahmenlehrpläne](#15)

[§ 16 Aufrücken, Versetzen, Überspringen und Wiederholen](#16)

[§ 17 Leistungsbewertung, Erwerb von Abschlüssen, Berechtigungen, Zeugnisse](#17)

### Abschnitt 6  
Übergangs- und Schlussvorschriften

[§ 18 Übergangsvorschriften](#18)

[§ 19 Durchführung der Verordnung](#19)

[§ 20 Inkrafttreten, Außerkrafttreten](#20)

[Anlage Wochenstundentafel an Schulen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“](#21)

### Abschnitt 1  
  
Allgemeines

#### § 1 Geltungsbereich, Ziele und Aufgaben

(1) Diese Verordnung gilt für die sonderpädagogische Förderung in Grundschulen, weiterführenden allgemeinbildenden Schulen und Oberstufenzentren (allgemeine Schulen) sowie in Förderschulen und Förderklassen.

(2) Förderung ist ein Grundprinzip pädagogischen Handelns in allen Schulformen.
Es ist Aufgabe aller Lehrkräfte, eine alters- und entwicklungsgerechte Förderung sicherzustellen.

(3) Sonderpädagogische Förderung erweitert die allgemeine Förderung.
Sie soll insbesondere Kinder und Jugendliche durch individuelle Hilfen bei der Entwicklung und Entfaltung ihrer geistigen, emotionalen, körperlichen und sozialen Fähigkeiten, ihrer Begabungen und Neigungen unterstützen.
Sonderpädagogische Förderung hat die Aufgabe Schülerinnen und Schüler unter Berücksichtigung ihrer individuellen Möglichkeiten zum selbstständigen und gemeinsamen Leben, Lernen und Handeln zu befähigen.

(4) Die sonderpädagogische Förderung unterstützt die Schülerinnen und Schüler

1.  ohne sonderpädagogischen Förderbedarf vorbeugend und pädagogisch begleitend in den allgemeinen Schulen, um der Entstehung eines sonderpädagogischen Förderbedarfs entgegenzuwirken,
2.  mit Behinderungen oder Beeinträchtigungen hinsichtlich der Gewährung von Nachteilsausgleichen und
3.  mit festgestelltem sonderpädagogischen Förderbedarf in allen Schulen.

(5) Der Bedarf an sonderpädagogischer Förderung ist bei Schülerinnen und Schülern anzunehmen, die in ihren Bildungs-, Entwicklungs- und Lernmöglichkeiten so stark beeinträchtigt sind, dass sie ohne spezielle Unterstützung nicht hinreichend gefördert werden können.

### Abschnitt 2  
  
Sonderpädagogische Förder- und Beratungsstellen

#### § 2 Aufgaben und Organisation

(1) Sonderpädagogische Förder- und Beratungsstellen (SpFB) nehmen Aufgaben im gemeinsamen Unterricht gemäß Abschnitt 4 wahr und erbringen vorrangig für den schulischen Bereich ein wohnungsnahes sonderpädagogisches Förder- und Beratungsangebot.
Sie leiten und koordinieren das Verfahren zur Feststellung von sonderpädagogischem Förderbedarf (Feststellungsverfahren).
Sie sollen insbesondere mit den Frühförder- und Beratungsstellen, den Gesundheitsämtern, den Kindertagesstätten und der schulpsychologischen Beratung zusammenarbeiten.
Personenbezogene Daten dürfen gemäß § 65 Absatz 3 und 6 des Brandenburgischen Schulgesetzes verarbeitet werden sowie gemäß § 65 Absatz 6 des Brandenburgischen Schulgesetzes zwischen den Schulen, den Schulbehörden sowie den Schulträgern und anderen öffentlichen Stellen übermittelt werden.

(2) Sonderpädagogische Förder- und Beratungsstellen werden nach Maßgabe der zur Verfügung stehenden Stellen und Personalmittel ausgestattet.
Die dort tätigen Lehrkräfte sind jeweils einer Schule zugeordnet.
Für koordinierende Tätigkeiten wird eine Lehrkraft vom staatlichen Schulamt beauftragt (beauftragte Lehrkraft).

### Abschnitt 3  
  
Feststellung, Änderung und Beendigung des sonderpädagogischen Förderbedarfs

#### § 3 Feststellungsverfahren

(1) Das staatliche Schulamt leitet das Feststellungsverfahren zur Feststellung, Änderung oder Beendigung des sonderpädagogischen Förderbedarfs auf Antrag der Eltern, der Schülerin oder des Schülers nach Vollendung des 14.
Lebensjahres oder der Schulleiterin oder des Schulleiters der allgemeinen Schule oder der Förderschule ein.
Das staatliche Schulamt beauftragt die zuständige Sonderpädagogische Förder- und Beratungsstelle mit der Durchführung des Feststellungsverfahrens.
Im Fall der Antragstellung durch die Schulleiterin oder den Schulleiter sind die Eltern und die Schülerin oder der Schüler nach Vollendung des 14.
Lebensjahres rechtzeitig vor Antragstellung zu informieren.

(2) Die Eltern sind verpflichtet, im Rahmen des Feststellungsverfahrens mitzuwirken, insbesondere die für die Feststellung des sonderpädagogischen Förderbedarfs notwendigen Unterlagen beizubringen.

(3) Das Feststellungsverfahren erfolgt durch einen Förderausschuss und gliedert sich in der Regel in

1.  die Grundfeststellung des sonderpädagogischen Förderbedarfs (Stufe I) und
2.  die förderdiagnostische Lernbeobachtung (Stufe II).

In der Stufe I wird geprüft, ob bei der Schülerin oder dem Schüler ein sonderpädagogischer Förderbedarf zu vermuten ist.
Die Feststellung des sonderpädagogischen Förderbedarfs in den Förderschwerpunkten „Sehen“, „Hören“, „körperliche und motorische Entwicklung“, „geistige Entwicklung“ und bei autistischem Verhalten soll in der Stufe I in der Regel abschließend erfolgen.
In den Förderschwerpunkten „Lernen“, „Sprache“ und „emotionale und soziale Entwicklung“ erfolgt die abschließende Feststellung in der Regel in der Stufe II.
Auf Antrag der Eltern kann das Feststellungsverfahren in der Stufe I abgeschlossen werden.

(4) In der Grundschule erfolgt die Feststellung des sonderpädagogischen Förderbedarfs für Schülerinnen und Schüler mit Beeinträchtigungen in den Förderschwerpunkten „Lernen“, „Sprache“ oder „emotionale und soziale Entwicklung“ in der Regel ohne die Durchführung der Stufe I.
In der Regel soll am Ende des zweiten Schulbesuchsjahres das Feststellungsverfahren abgeschlossen und durch den Förderausschuss auf der Grundlage seiner Ergebnisse eine Bildungsempfehlung erstellt worden sein.
Absatz 3 Satz 5 bleibt unberührt.

(5) Die Regelungen des Feststellungsverfahrens gelten entsprechend, wenn die Schülerin oder der Schüler eine Förderschule in freier Trägerschaft besucht oder besuchen möchte oder eine sonderpädagogische Förderung im gemeinsamen Unterricht an Ersatzschulen in allen Förderschwerpunkten sowie bei sonderpädagogischem Förderbedarf im autistischen Verhalten erfolgen soll.

#### § 4 Förderausschuss

(1) Der Förderausschuss erarbeitet eine Bildungsempfehlung.

(2) Mitglieder eines Förderausschusses sind die mit dem Vorsitz beauftragte Lehrkraft der Sonderpädagogischen Förder- und Beratungsstelle und die Eltern.
In der förderdiagnostischen Lernbeobachtung sind eine sonderpädagogisch qualifizierte Lehrkraft und die Klassenlehrkraft weitere Mitglieder des Förderausschusses.

(3) Ein Mitglied der Schulleitung der aufnehmenden oder der besuchten Schule (zuständige Schule) oder eine von ihr beauftragte Lehrkraft ist in das Förderausschussverfahren einzubeziehen.

(4) Für die Entscheidungsfindung zum geeigneten Lernort, insbesondere hinsichtlich der Bereitstellung notwendiger zusätzlicher sächlicher oder personeller Mittel, sind die zuständigen Kostenträger rechtzeitig einzubeziehen und ist das Benehmen herzustellen.

(5) Die oder der Vorsitzende ist nach Lage des Einzelfalles und nach Einwilligung der Eltern berechtigt, weitere Fachleute in den Förderausschuss zu berufen und schulärztliche und andere Stellungnahmen anzufordern.

#### § 5 Entscheidung des staatlichen Schulamtes

(1) Das staatliche Schulamt entscheidet unter Berücksichtigung des Elternwunsches und auf der Grundlage der Bildungsempfehlung des Förderausschusses nach Durchführung der Stufe I oder Stufe II, ob und welcher sonderpädagogischer Förderbedarf vorliegt.
Wenn dies der Fall ist, entscheidet das staatliche Schulamt über

1.  den Lernort,
2.  die Jahrgangsstufe,
3.  den anzuwendenden Rahmenlehrplan,
4.  die Förderinhalte und
5.  soweit erforderlich, ob ein Nachteilsausgleich zu gewähren ist.

Kann das staatliche Schulamt dem Wunsch der Eltern nicht entsprechen, weist es die Schülerin oder den Schüler einer Schule zu.
Mit der Entscheidung des staatlichen Schulamtes ist die Schülerin oder der Schüler an der Schule aufgenommen und das Schulverhältnis begründet.

(2) Für Schülerinnen und Schüler mit einem sonderpädagogischen Förderbedarf in den Förderschwerpunkten „Lernen“, „Sprache“ und „emotionale und soziale Entwicklung“ soll das staatliche Schulamt die Entscheidung gemäß Absatz 1 befristen, wenn

1.  absehbar ist, dass die Beeinträchtigung der Bildungs-, Entwicklungs- und Lernmöglichkeiten nicht auf Dauer bestehen werden oder
2.  auf Grund der individuellen Voraussetzungen der Schülerin oder des Schülers eine erneute Prüfung angezeigt ist.

§ 6 Absatz 2 bleibt unberührt.

#### § 6 Fortführung, Änderung und Beendigung der sonderpädagogischen Förderung

(1) Für die Aufnahme von Schülerinnen oder Schülern mit sonderpädagogischem Förderbedarf in eine weiterführende allgemeinbildende Schule oder in ein Oberstufenzentrum gelten die Regelungen der Sekundarstufe I-Verordnung, der Gymnasiale-Oberstufe-Verordnung oder die Rechtsvorschrift für den jeweiligen Bildungsgang am Oberstufenzentrum.

(2) Das staatliche Schulamt prüft auf Antrag im Rahmen eines erneuten Feststellungsverfahrens, ob die Beeinträchtigungen fortbestehen und weiterhin eine sonderpädagogische Förderung erforderlich ist.
Grundsätzlich wird für Schülerinnen und Schüler mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ zu Beginn der Jahrgangsstufe 6 ein Feststellungsverfahren durchgeführt.

(3) Über den Wechsel von Schülerinnen und Schülern mit sonderpädagogischem Förderbedarf in eine Klasse mit gemeinsamem Unterricht, in eine Förderschule oder Förderklasse sowie über den Wechsel des Bildungsgangs wegen Änderung des sonderpädagogischen Förderbedarfs entscheidet das staatliche Schulamt auf der Grundlage einer Bildungsempfehlung des Förderausschusses.

(4) Ein Wechsel aus einer Förderschule oder einer Förderklasse in eine allgemeine Schule ist vorzunehmen, wenn sonderpädagogischer Förderbedarf nicht mehr besteht und die Bewältigung der künftigen Anforderungen zu erwarten ist.
Über die Beendigung der sonderpädagogischen Förderung entscheidet das staatliche Schulamt auf der Grundlage einer sonderpädagogischen Stellungnahme.
Die Eltern sind vor der Entscheidung anzuhören.
Der Wechsel findet in der Regel am Ende des Schulhalbjahres oder des Schuljahres statt und wird sonderpädagogisch unterstützt.

(5) Wenn bei Schülerinnen und Schülern mit sonderpädagogischem Förderbedarf im gemeinsamen Unterricht ein sonderpädagogischer Förderbedarf nicht mehr besteht, gilt Absatz 4 Satz 2 und 3 entsprechend.

### Abschnitt 4  
  
Gemeinsamer Unterricht

#### § 7 Grundsätze des gemeinsamen Unterrichts

(1) Klassen oder Kurse in allgemeinen Schulen, in denen Schülerinnen und Schüler mit und ohne sonderpädagogischen Förderbedarf gemeinsam unterrichtet werden, sind „Klassen oder Kurse mit gemeinsamem Unterricht“.

(2) Soweit Schulen auf der Grundlage einer vom staatlichen Schulamt genehmigten pädagogischen Konzeption unterrichten, die die Feststellung eines sonderpädagogischen Förderbedarfs gemäß § 5 in den Förderschwerpunkten „Lernen“, „emotionale und soziale Entwicklung“ und „Sprache“ nicht mehr erforderlich macht (Schulen für gemeinsames Lernen), gelten die Regelungen für den gemeinsamen Unterricht entsprechend.

#### § 8 Rahmenbedingungen, Klassenfrequenz und Lehrkräfteeinsatz

(1) Für jede Schülerin oder jeden Schüler mit sonderpädagogischem Förderbedarf stehen neben den Lehrkräftewochenstunden der allgemeinen Schule zusätzliche Lehrkräftewochenstunden insbesondere von sonderpädagogisch qualifizierten Lehrkräften gemäß den Verwaltungsvorschriften über die Unterrichtsorganisation zur Verfügung.

(2) Die Lehrkräfte der allgemeinen Schule und die sonderpädagogisch qualifizierten Lehrkräfte führen den gemeinsamen Unterricht auf der Grundlage der Entscheidung des staatlichen Schulamtes durch.
Soweit erforderlich, kann zur Sicherung der individuellen sonderpädagogischen Förderung neben den Lehrkräften der allgemeinen Schule und den sonderpädagogisch qualifizierten Lehrkräften auch sonstiges pädagogisches Personal eingesetzt werden.

#### § 9 Unterrichtsorganisation, Stundentafeln und Rahmenlehrpläne

(1) Der gemeinsame Unterricht orientiert sich an der Stundentafel der besuchten allgemeinen Schule und ist durch vielfältige didaktische Prinzipien, Methoden, Arbeits- und Sozialformen so zu gestalten, dass er die Leistungsfähigkeit, das Lerntempo, die Belastbarkeit und die Interessen der Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf berücksichtigt.
Für alle Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf erstellt die Klassenlehrkraft gemeinsam mit der sonderpädagogisch qualifizierten Lehrkraft einen individuellen Förderplan, der mindestens halbjährlich aktualisiert wird.

(2) Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf im Förderschwerpunkt „Lernen“ werden im gemeinsamen Unterricht nach den Inhalten und Anforderungen des Rahmenlehrplans für die Jahrgangsstufen 1 bis 10 für den Bildungsgang zum Erwerb des Abschlusses der Schule mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ unterrichtet.
Die schulinternen Curricula sollen gewährleisten, dass den Schülerinnen und Schülern mit sonderpädagogischem Förderbedarf themengleiche Inhalte zieldifferent vermittelt werden können.
Für Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf im Förderschwerpunkt „Lernen“ ist ab Jahrgangsstufe 1 die Begegnung mit fremden Sprachen anzubieten.
Ab Jahrgangsstufe 3 ist die Teilnahme am Fremdsprachenunterricht mit mindestens zwei Wochenstunden verpflichtend.

(3) Schülerinnen und Schüler mit dem sonderpädagogischen Förderbedarf im Förderschwerpunkt „Lernen“, deren bisherige Lernentwicklung und Lernbereitschaft sowie deren erreichter Leistungsstand den Erwerb des Hauptschulabschlusses/der Berufsbildungsreife erwarten lassen, sind in der Regel in den Jahrgangsstufen 7 und 8 zur Erleichterung eines späteren Bildungsgangwechsels durch eine Erhöhung des Anforderungsniveaus, insbesondere in den Fächern Deutsch und Mathematik, sowie durch die Verstärkung des Fremdsprachenunterrichts an die hierfür erforderlichen Inhalte und Anforderungen des Rahmenlehrplans für die Jahrgangsstufen 1 bis 10 heranzuführen.
Diese Schülerinnen und Schüler nehmen am Fremdsprachenunterricht im Umfang der für die Oberschule und Gesamtschule geltenden Kontingentstundentafel teil.

(4) Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf im Förderschwerpunkt „geistige Entwicklung“ werden im gemeinsamen Unterricht nach dem Rahmenlehrplan für den Bildungsgang zum Erwerb des Abschlusses der Schule mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“ oder nach anderen geeigneten curricularen Materialien unterrichtet.
Die Inhalte sollen nach Möglichkeit thematisch an die Unterrichtsinhalte der besuchten Klassen angepasst werden.

(5) Für Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf in den Förderschwerpunkten „Sprache“, „emotionale und soziale Entwicklung“, „Hören“, „Sehen“, „körperliche und motorische Entwicklung“ und mit autistischem Verhalten gelten die Regelungen und Rahmenlehrpläne der besuchten allgemeinen Schule.
Auf Beschluss der Konferenz der Lehrkräfte kann auf Antrag der jeweiligen Fachkonferenzen die Vermittlung der Lerninhalte unter Beibehaltung des Anforderungsniveaus des jeweiligen Rahmenlehrplans behinderungsspezifischen Erfordernissen angepasst werden.

#### § 10 Aufrücken, Versetzen, Überspringen und Wiederholen

(1) Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf im Förderschwerpunkt „Lernen“ rücken in der Regel unabhängig vom Leistungsstand in die nächsthöhere Jahrgangsstufe auf.
In den Jahrgangsstufen 1 bis 9 kann die Klassenkonferenz die Wiederholung einer Jahrgangsstufe empfehlen, wenn Schülerinnen oder Schüler am Ende des jeweiligen Schuljahres auf Grund längerer Fehlzeiten oder aus anderen Gründen nicht hinreichend gefördert werden konnten und daher so erhebliche Lernrückstände aufweisen, dass eine erfolgreiche Teilnahme am Unterricht der nächsthöheren Jahrgangsstufe, auch unter Berücksichtigung der möglichen Fördermaßnahmen, nicht zu erwarten ist.
Die Eltern entscheiden nach vorheriger Beratung durch die Klassenlehrkraft über die Wiederholung.

(2) Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf im Förderschwerpunkt „geistige Entwicklung“ rücken in die nächsthöhere Jahrgangsstufe auf.

(3) Für Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf in den Förderschwerpunkten „Sprache“, „emotionale und soziale Entwicklung“, „Hören“, „Sehen“ und „körperliche und motorische Entwicklung“ finden die für die besuchte allgemeine Schule geltenden Bestimmungen Anwendung.

#### § 11 Leistungsbewertung, Erwerb von Abschlüssen, Berechtigungen, Zeugnisse

(1) Die Leistungen der Schülerinnen und Schüler im Bildungsgang zum Erwerb des Abschlusses der Schule mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ werden nach den für diesen Bildungsgang im Rahmenlehrplan für die Jahrgangsstufen 1 bis 10 geltenden Anforderungen bewertet.
In der Jahrgangsstufe 10 nehmen diese Schülerinnen und Schüler an den vergleichenden Arbeiten in Deutsch und Mathematik nach § 17 Absatz 2 teil.
Die Schülerinnen und Schüler erwerben einen Abschluss entsprechend § 17 Absatz 5.
Die Leistungen der Schülerinnen und Schüler im Bildungsgang zum Erwerb des Abschlusses der Schule mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“ werden nach den Bestimmungen für diesen Bildungsgang bewertet.
Die Schülerinnen und Schüler erwerben einen Abschluss entsprechend § 17 Absatz 8.

(2) Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf im Förderschwerpunkt „Lernen“ erhalten die für die besuchte allgemeine Schule geltenden Zeugnisse.
Auf den Zeugnissen ist zu vermerken, dass der Abschluss auf der Grundlage der für den Bildungsgang zum Erwerb des Abschlusses der Schule mit dem Förderschwerpunkt „Lernen“ geltenden Anforderungen erworben wurde.
Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf im Förderschwerpunkt „geistige Entwicklung“ erhalten die für diesen Förderschultyp geltenden Zeugnisse.
Auf den Zeugnissen ist zu vermerken, dass der Abschluss auf der Grundlage der für den Bildungsgang zum Erwerb des Abschlusses der Schule mit dem Förderschwerpunkt „geistige Entwicklung“ geltenden Anforderungen erworben wurde.

(3) Für Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf in den Förderschwerpunkten „Sprache“, „emotionale und soziale Entwicklung“, „Hören“, „Sehen“ und „körperliche und motorische Entwicklung“ oder mit autistischem Verhalten finden die für die besuchte Schule geltenden Bestimmungen zur Leistungsbewertung, zum Erwerb von Abschlüssen und Berechtigungen sowie zu Zeugnissen Anwendung.
Zum Ausgleich von Nachteilen, die sich aus der Art und dem Umfang der Behinderung ergeben, können individuelle Maßstäbe der Leistungsbewertung unter Beibehaltung des Anforderungsniveaus angelegt werden (Nachteilsausgleich).
Hinweise auf einen gewährten Nachteilsausgleich werden nicht auf dem Zeugnis ausgewiesen.

(4) Jugendliche mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“ im gemeinsamen Unterricht in der Sekundarstufe I, für die keine Maßnahme zur beruflichen Eingliederung nach der Jahrgangsstufe 10 angeboten werden kann, erfüllen die Berufsschulpflicht in der Berufsbildungsstufe einer Schule mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“.

### Abschnitt 5  
  
Förderschulen und Förderklassen

#### § 12 Allgemeines

(1) Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf, die nicht im gemeinsamen Unterricht an einer allgemeinen Schule gefördert werden können oder deren Eltern den Besuch einer Förderschule oder Förderklasse wünschen, werden auf Antrag oder nach Anhörung der Eltern möglichst wohnungsnah in eine ihrem sonderpädagogischen Förderbedarf entsprechende Förderschule oder Förderklasse aufgenommen.

(2) Schülerinnen und Schüler mit einem stark ausgeprägten autistischen Syndrom werden, wenn sie nicht im gemeinsamen Unterricht gefördert werden, an einer geeigneten Förderschule unterrichtet.

#### § 13 Struktur der Förderschulen und Förderklassen

(1) Förderschulen und Förderklassen werden gemäß § 30 Absatz 4 des Brandenburgischen Schulgesetzes nach sonderpädagogischen Förderschwerpunkten gegliedert.
Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Hören“, „Sehen“ oder „körperliche und motorische Entwicklung“ führen den Bildungsgang der Grundschule und die Bildungsgänge der Oberschule.
Diese Schulen können auch den Bildungsgang zum Erwerb des Abschlusses der Schule mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ führen.
Die Schule mit dem sonderpädagogischen Förderschwerpunkt „Sehen“ kann den Bildungsgang zum Erwerb der allgemeinen Hochschulreife führen.
Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ oder „geistige Entwicklung“ führen den Bildungsgang zum Erwerb des jeweiligen Abschlusses dieser Schule.

(2) Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“, „Hören“, „Sehen“, „körperliche und motorische Entwicklung“ und die Schule für Kranke umfassen die Jahrgangsstufen 1 bis 10.
Die Schule mit dem sonderpädagogischen Förderschwerpunkt „Sehen“ kann eine gymnasiale Oberstufe gemäß § 24 Absatz 2 Satz 2 des Brandenburgischen Schulgesetzes führen.

(3) Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Sprache“ oder „emotionale und soziale Entwicklung“ umfassen in der Regel die Jahrgangsstufen 1 bis 6 und führen den Bildungsgang der Grundschule.
Sie sollen die Schülerinnen und Schüler zu einem möglichst frühzeitigen Übergang in die allgemeine Schule befähigen.

(4) Die Schule mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“ gliedert sich in bildungsspezifische Lernstufen:

1.  Primarstufe,
2.  Sekundarstufe I,
3.  Berufsbildungsstufe.

Kinder und Jugendliche mit einer schweren Mehrfachbehinderung sind in die jeweilige Stufe altersgemäß zu integrieren.

(5) Förderschulen können auf der Grundlage eines pädagogischen Konzepts mit Genehmigung des für Schule zuständigen Ministeriums förderschwerpunktübergreifend organisiert werden.

(6) Schulen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ und Oberstufenzentren sollen mit dem Ziel kooperieren, die jeweiligen Unterrichtsinhalte der Bildungsgänge zu einer pädagogischen Einheit zusammenzuführen und Schülerinnen und Schülern den Erwerb eines dem Hauptschulabschluss/der Berufsbildungsreife gleichgestellten Abschlusses innerhalb der beruflichen Ausbildung zu erleichtern.
Dazu können insbesondere in den Jahrgangsstufen 9 und 10 einzelne Unterrichtseinheiten am Oberstufenzentrum durchgeführt und organisatorisch mit dem Unterricht geeigneter beruflicher Bildungsgänge verbunden werden.

#### § 14 Dauer des Schulbesuchs

(1) Die Höchstverweildauer in Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ beträgt zwölf Schulbesuchsjahre.

(2) Die Höchstverweildauer in Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Sprache“, „emotionale und soziale Entwicklung“, „Hören“, „körperliche und motorische Entwicklung“ oder „Sehen“ richtet sich nach den für die jeweilige Schulstufe an allgemeinen Schulen geltenden Bestimmungen.

(3) Der Besuch der Schule mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“ dauert in der Regel bis zum Ende des Schuljahres, in dem die Schülerin oder der Schüler das 18. Lebensjahr vollendet (Ende der Schulpflicht).
Die Lernstufen der Primarstufe und der Sekundarstufe I umfassen zehn Schuljahre, wobei eine Höchstverweildauer von zwölf Schuljahren nicht überschritten werden darf.
Mit deren Besuch wird die Vollzeitschulpflicht erfüllt.
Die Berufsschulpflicht wird durch den Besuch der Berufsbildungsstufe erfüllt.
Sie umfasst in der Regel zwei Schuljahre.
Die Berufsbildungsstufe soll nach insgesamt zwölf Schulbesuchsjahren verlassen werden.
Die Entscheidung über die Berechtigung zum Besuch der Schule mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“ gemäß § 30 Absatz 5 Satz 4 des Brandenburgischen Schulgesetzes trifft das staatliche Schulamt auf Antrag der Eltern und auf der Grundlage der Bildungsempfehlung des Förderausschusses jeweils für ein Schuljahr.
Anträge sind über die Schule an das staatliche Schulamt spätestens bis zum Ende des ersten Halbjahres des Schuljahres zu richten, in dem die Schülerin oder der Schüler das 18.
Lebensjahr vollendet.
Für jede erneute Verlängerung ist die Antragstellung zu wiederholen.

#### § 15 Unterrichtsorganisation, Stundentafeln und Rahmenlehrpläne

(1) Der Unterricht in Förderschulen und Förderklassen erfolgt in der Regel im Klassenverband oder in Kursen.
Für jede Schülerin und für jeden Schüler ist von der Klassenlehrkraft ein individueller Förderplan, der mindestens halbjährlich aktualisiert wird, zu erstellen.

(2) Umfang und Verteilung des Unterrichts in Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Hören“, „Sehen“ und „körperliche und motorische Entwicklung“ richten sich nach der Stundentafel der Grundschule und der Oberschule.
Für Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Sprache“ und „emotionale und soziale Entwicklung“ gilt die Stundentafel der Grundschule.
Unterricht zur behinderungsspezifischen Förderung kann im Rahmen der vorgegebenen Stundentafel und der sächlichen und personellen Voraussetzungen erteilt werden.

(3) Für die Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ gilt die Wochenstundentafel gemäß Anlage.
In den Jahrgangsstufen 7 bis 10 werden die Fächer Deutsch, Mathematik und Fremdsprache zum Lernbereich „Allgemeine Grundlagen“ und die Fächer Biologie, Chemie, Physik, Geografie, Geschichte, Politische Bildung und Wirtschaft-Arbeit-Technik zum Lernbereich „Lebenswelt- und Berufsorientierung“ zusammengefasst.
Ab Jahrgangsstufe 1 ist die Begegnung mit fremden Sprachen anzubieten.
In der Jahrgangsstufe 3 beginnt der Unterricht in einer Fremdsprache.

(4) Schülerinnen und Schüler, deren bisherige Lernentwicklung und Lernbereitschaft sowie deren erreichter Leistungsstand den Erwerb des Hauptschulabschlusses/Berufsbildungsreife erwarten lassen, sind durch eine Erhöhung des Anforderungsniveaus, insbesondere in den Fächern Deutsch und Mathematik und der ersten Fremdsprache auf

1.  den Wechsel an eine Schule der Sekundarstufe I bis spätestens zum Ende der Jahrgangsstufe 8 oder
2.  den Erwerb eines der Berufsbildungsreife entsprechenden Abschlusses gemäß § 17 Absatz 2 des Brandenburgischen Schulgesetzes am Ende der Jahrgangsstufe 10

vorzubereiten.
Schülerinnen und Schüler gemäß Satz 1 erhalten in den Jahrgangsstufen 7 und 8 insgesamt acht Wochenstunden Fremdsprachenunterricht.
Dieser kann innerhalb des für den Lernbereich „Allgemeine Grundlagen“ ausgewiesenen Stundenrahmens oder durch zusätzlichen Unterricht in Höhe von bis zu zwei Wochenstunden je Jahrgangsstufe erfolgen.
Die Entscheidung trifft die Konferenz der Lehrkräfte.

(5) In Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Sprache“, „emotionale und soziale Entwicklung“, „Hören“, „Sehen“ oder „körperliche und motorische Entwicklung“ erfolgt der Unterricht auf der Grundlage der für die Bildungsgänge gemäß § 15 Absatz 3 Nummer 1 und 2 des Brandenburgischen Schulgesetzes geltenden Inhalte und Anforderungen des Rahmenlehrplans für die Jahrgangsstufen 1 bis 10 sowie der Rahmenlehrpläne der gymnasialen Oberstufe und der beruflichen Schulen.
In Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Hören“ wird den Schülerinnen und Schülern, die nicht über die Lautsprache als primäres Kommunikationsmittel verfügen, der Gebrauch der Gebärdensprache und anderer Kommunikationsmittel vermittelt.
Diesen Schülerinnen und Schülern sind die Unterrichtsinhalte in der Gebärdensprache im Rahmen der sächlichen, personellen und organisatorischen Voraussetzungen zu vermitteln.
In Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ erfolgt der Unterricht nach den Inhalten und Anforderungen des Rahmenlehrplans für die Jahrgangsstufen 1 bis 10 für den Bildungsgang zum Erwerb des Abschlusses der Schule mit dem Förderschwerpunkt „Lernen“.
Für Schülerinnen und Schüler, die auf den Erwerb eines der Berufsbildungsreife entsprechenden Abschlusses am Ende der Jahrgangsstufe 10 vorbereitet werden, orientiert sich der Unterricht spätestens in der Jahrgangsstufe 10 an den für den Bildungsgang gemäß § 15 Absatz 3 Nummer 2 Buchstabe a des Brandenburgischen Schulgesetzes geltenden Inhalten und Anforderungen des Rahmenlehrplans für die Jahrgangsstufen 1 bis 10.
In Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“ wird nach dem Rahmenlehrplan sowie anderen geeigneten curricularen Materialien für den Bildungsgang zum Erwerb des Abschlusses der Schule mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“ unterrichtet.

#### § 16 Aufrücken, Versetzen, Überspringen und Wiederholen

(1) In Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ rücken die Schülerinnen und Schüler grundsätzlich in die nächsthöhere Jahrgangsstufe auf, wenn nicht gemäß Absatz 2 entschieden wird.

(2) Für Schülerinnen und Schüler der Jahrgangsstufen 1 bis 9 in Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ kann die Klassenkonferenz die Wiederholung einer Jahrgangsstufe empfehlen, wenn Schülerinnen oder Schüler am Ende des jeweiligen Schuljahres auf Grund längerer Fehlzeiten so erhebliche Lernrückstände aufweisen, dass eine erfolgreiche Teilnahme am Unterricht der nächsthöheren Jahrgangsstufe, auch unter Berücksichtigung der möglichen Fördermaßnahmen, nicht zu erwarten ist.
Eine Wiederholung der Jahrgangsstufe 10 soll für Schülerinnen und Schüler empfohlen werden, die auf den Erwerb eines der Berufsbildungsreife entsprechenden Abschlusses am Ende der Jahrgangsstufe 10 vorbereitet wurden und diesen nicht erreicht haben, wenn zu erwarten ist, dass dieser Abschluss durch die Wiederholung erreicht wird.
Die Eltern entscheiden nach vorheriger Beratung durch die Klassenlehrkraft über die Wiederholung.

(3) In Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“ rücken die Schülerinnen und Schüler in die nächsthöhere bildungsspezifische Lernstufe auf.
Unabhängig von der Art und dem Grad der Behinderung sollen alle Lernstufen besucht werden.

(4) In Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Sprache“, „emotionale und soziale Entwicklung“, „Hören“, „Sehen“ und „körperliche und motorische Entwicklung“ gelten für das Aufrücken, die Versetzung, eine angeordnete Wiederholung, eine Wiederholung auf Elternantrag, das Überspringen oder die Kurseinstufung für die Jahrgangsstufen 1 bis 6 die Bestimmungen der Grundschulverordnung, für die Jahrgangsstufen 7 bis 10 die Bestimmungen der Sekundarstufe I-Verordnung für die Oberschule und für die gymnasiale Oberstufe die Bestimmungen der Gymnasiale-Oberstufe-Verordnung.

(5) Schülerinnen und Schüler im Bildungsgang der Grundschule können auf Antrag der Eltern innerhalb der Jahrgangsstufen 1 bis 6 einmal ein Schuljahr freiwillig wiederholen, um Benachteiligungen, die sich auf Grund ihrer Behinderung ergeben, ausgleichen zu können.
In diesem Fall wird auf Antrag der Eltern, der bis zum Ende der Vollzeitschulpflicht gestellt werden kann, die Wiederholung nicht auf die Dauer der Schulpflicht angerechnet.
Die Eltern sind entsprechend zu beraten.

#### § 17 Leistungsbewertung, Erwerb von Abschlüssen, Berechtigungen, Zeugnisse

(1) In Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Hören“, „Sehen“ oder „körperliche und motorische Entwicklung“ gelten für die Unterrichtsorganisation, Prüfungen, den Erwerb von Abschlüssen und Berechtigungen und für die Leistungsbewertung die Bestimmungen der Grundschulverordnung und der Sekundarstufe I-Verordnung für die Oberschule, soweit keine abweichenden Regelungen getroffen werden.
In der gymnasialen Oberstufe der Schule mit dem sonderpädagogischen Förderschwerpunkt „Sehen“ gelten die Bestimmungen für die gymnasiale Oberstufe an Gesamtschulen.
Für die Leistungsbewertung gelten die Bestimmungen der VV-Leistungsbewertung und die Gymnasiale-Oberstufe-Verordnung.

(2) In Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ fertigen alle Schülerinnen und Schüler in der Jahrgangsstufe 10 in einem Fach eigener Wahl eine Facharbeit oder Leistungsmappe an oder führen ein Projekt durch und präsentieren die Facharbeit, Leistungsmappe oder das Projekt.
Die Facharbeit, Leistungsmappe oder das Projekt sowie die Präsentation werden bewertet.
Die Bewertung kann besonders gewichtet werden.
In der Jahrgangsstufe 10 werden in den Fächern Deutsch und Mathematik zentral erstellte, vergleichende Arbeiten durchgeführt.

(3) In Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ werden für die erbrachten Leistungen in der Begegnungssprache keine Noten erteilt.
Die Teilnahme am Unterricht in der Begegnungssprache ist auf dem Zeugnis zu vermerken.

(4) Sofern in Schulen mit dem sonderpädagogischen Förderschwerpunkt „Hören“, „Sehen“ oder „körperliche und motorische Entwicklung“ der Bildungsgang der Schule mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ geführt wird, kann auch der Abschluss dieser Schule erworben werden.

(5) In Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ wird auf Beschluss der Klassenkonferenz bei durchschnittlich mindestens ausreichenden Leistungen in allen Fächern am Ende der Jahrgangsstufe 10 der Abschluss dieser Schule vergeben.
Bei mangelhaften oder ungenügenden Leistungen in einzelnen Fächern wird der Abschluss nur dann vergeben, wenn Ausgleichsleistungen mit mindestens befriedigenden Leistungen in anderen Fächern vorliegen.

(6) Am Ende der Jahrgangsstufe 10 erwirbt einen der Berufsbildungsreife entsprechenden Abschluss gemäß § 17 Absatz 2 des Brandenburgischen Schulgesetzes, wer spätestens in der Jahrgangsstufe 10 entsprechend den Anforderungen gemäß § 15 Absatz 5 Satz 5 unterrichtet wurde und

1.  in jedem Fach mindestens ausreichende Leistungen erreicht hat,
2.  bei ansonsten ausreichenden Leistungen höchstens eine mangelhafte Leistung aufweist oder
3.  bei ansonsten mindestens ausreichenden Leistungen höchstens zwei mangelhafte Leistungen aufweist und diese durch jeweils eine mindestens befriedigende Leistung in einem anderen Fach ausgleichen kann.
    Dabei müssen in einem der Fächer Deutsch oder Mathematik mindestens ausreichende Leistungen erreicht werden.
    Der Ausgleich für nicht mindestens ausreichende Leistungen in den Fächern Deutsch oder Mathematik muss durch mindestens befriedigende Leistungen in einem Fach des Lernbereichs „Allgemeine Grundlagen“ oder „Lebenswelt- und Berufsorientierung“ erfolgen.

(7) In Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ werden bildungsgangeigene Zeugnisse ausgegeben.
§ 11 Absatz 1 bis 3 der Grundschulverordnung gilt entsprechend.
Soweit der Unterricht in Lernbereichen erteilt wird, erfolgt neben der Benotung der Fächer eine zusammengefasste Benotung des Lernbereichs.
Wer die Voraussetzungen für die Erteilung eines der Berufsbildungsreife entsprechenden Abschlusses erfüllt, erhält ein Zeugnis mit dem Vermerk über den erteilten Abschluss.

(8) In Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“ wird ein eigener Abschluss vergeben.

(9) In Schulen und Klassen mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“ werden bildungsgangeigene Zeugnisse ausgegeben.
Sie werden grundsätzlich zum Schuljahresende erstellt.
Die Schulkonferenz kann eine Ausgabe von Zeugnissen auch zum Schulhalbjahr beschließen.
Ein Zeugnis über den Abschluss der Schule mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“ wird in der Regel nach Durchlaufen der Berufsbildungsstufe des Bildungsgangs erteilt.
Schülerinnen und Schüler, die ihre Berufsschulpflicht nicht an einer Schule mit dem sonderpädagogischen Förderschwerpunkt „geistige Entwicklung“ erfüllen, erhalten nach Vollendung der Vollzeitschulpflicht ein Abschlusszeugnis.

### Abschnitt 6  
  
Übergangs- und Schlussvorschriften

#### § 18 Übergangsvorschriften

(1) § 15 Absatz 3 Satz 4 gilt für die Schülerinnen und Schüler, die ab dem Schuljahr 2019/2020 in die Jahrgangsstufe 3 eintreten.
Für Schülerinnen und Schüler, die sich in den Schuljahren 2017/2018 und 2018/2019 in den Jahrgangsstufen 3 bis 7 einer Schule mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ befinden, beginnt der Fremdsprachenunterricht abweichend von § 15 Absatz 3 Satz 4 in der Jahrgangsstufe 7 verpflichtend mit mindestens zwei Schülerwochenstunden im Lernbereich „Allgemeine Grundlagen“.
Auf Beschluss der Konferenz der Lehrkräfte kann für die Schülerinnen und Schüler gemäß Satz 2 in den Jahrgangsstufen 5 und 6 statt der Begegnung mit fremden Sprachen bis zu zwei Wochenstunden Unterricht in einer Fremdsprache erteilt werden.
Hierzu sind Stunden der sonderpädagogischen Maßnahmen/Förderunterricht und der Schwerpunktgestaltung zu verwenden.

(2) Im Schuljahr 2017/2018 sollen in Klassen mit gemeinsamem Unterricht nicht mehr als 23 Schülerinnen und Schüler unterrichtet werden, von denen nicht mehr als vier Schülerinnen und Schüler einen sonderpädagogischen Förderbedarf haben sollen.
Über Abweichungen entscheidet das zuständige staatliche Schulamt im Benehmen mit der Schulkonferenz und dem Schulträger.

#### § 19 Durchführung der Verordnung

Näheres zur Durchführung dieser Verordnung regeln Verwaltungsvorschriften.

#### § 20 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 1.
August 2017 in Kraft.
Gleichzeitig tritt die Sonderpädagogik-Verordnung vom 2.
August 2007 (GVBl.
II S. 223), die durch Verordnung vom 10.
Juli 2009 (GVBl.
II S. 433) geändert worden ist, außer Kraft.

Potsdam, den 20.
Juli 2017

Der Minister für Bildung,  
Jugend und Sport

Günter Baaske

 

* * *

### Anlagen

1

[Anlage (zu § 15 Absatz 3) - Wochenstundentafel an Schulen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“](/br2/sixcms/media.php/68/GVBl_II_41_2017-Anlage.pdf "Anlage (zu § 15 Absatz 3) - Wochenstundentafel an Schulen mit dem sonderpädagogischen Förderschwerpunkt „Lernen“") 708.0 KB