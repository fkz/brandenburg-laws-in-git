## Sechsundzwanzigste Verordnung zur Festsetzung von Erhaltungszielen und Gebietsabgrenzungen für Gebiete von gemeinschaftlicher Bedeutung (26. Erhaltungszielverordnung - 26. ErhZV)

Auf Grund des § 14 Absatz 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21. Januar 2013 (GVBl.
I Nr. 3) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Festsetzung

Die in Anlage 1 aufgeführten und näher beschriebenen Gebiete werden gemäß Artikel 4 Absatz 4 der Richtlinie 92/43/EWG als Gebiete von gemeinschaftlicher Bedeutung (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) in den in § 3 bestimmten Grenzen festgesetzt.
Sie sind Teil des kohärenten europäischen ökologischen Netzes „Natura 2000“ und liegen vollständig oder anteilig in den Landkreisen Barnim und Uckermark.

#### § 2 Erhaltungsziele

(1) Für das Gebiet von gemeinschaftlicher Bedeutung „Unteres Odertal“ ergeben sich gemäß § 32 Absatz 3 des Bundesnaturschutzgesetzes die Erhaltungsziele aus dem in Anlage 1 aufgeführten Nationalparkgesetz Unteres Odertal vom 9.
November 2006.
Die im Nationalpark zu schützenden Lebensraumtypen und Arten nach der Fauna-Flora-Habitat-Richtlinie sind in Anlage 3 des Nationalparkgesetzes Unteres Odertal benannt.
Die Erhaltungsziele und die dafür erforderlichen Schutz-, Pflege- und Entwicklungsmaßnahmen werden im Nationalparkplan nach § 7 Absatz 2 des Nationalparkgesetzes Unteres Odertal festgelegt.
Das im Gebiet anwendbare Nationalparkgesetz Unteres Odertal ist in Anlage 1 aufgeführt.

(2) Für das Gebiet von gemeinschaftlicher Bedeutung „Koppelberg Altgalow“ ergeben sich gemäß § 32 Absatz 3 des Bundesnaturschutzgesetzes die Erhaltungsziele aus dem in Anlage 1 genannten natürlichen Lebensraumtyp von gemeinschaftlichem Interesse.
In der Anlage 1 werden für das Gebiet die ökologischen Erfordernisse für einen günstigen Erhaltungszustand des natürlichen Lebensraumtyps nach Anhang I der Richtlinie 92/43/EWG beschrieben.

#### § 3 Gebietsabgrenzung

(1) Die Grenzen der Gebiete von gemeinschaftlicher Bedeutung sind in den in Anlage 1 genannten und in Anlage 2 Nummer 2 näher bezeichneten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 17 rot eingetragen.
Als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte Übersichtskarte im Maßstab 1 : 75 000 dient der räumlichen Einordnung der Gebiete von gemeinschaftlicher Bedeutung.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 aufgeführten topografischen Karten.
Die Flächen des Nationalparks Unteres Odertal sind in den in Anlage 2 Nummer 2 aufgeführten topografischen Karten schraffiert eingetragen.
Zur Orientierung werden die Gebiete in Anlage 1 jeweils in einer Kartenskizze dargestellt.

(2) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim jeweils zuständigen Landkreis, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 4 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 22.
Oktober 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#a) Diese Verordnung dient der Umsetzung der Richtlinie 92/43/EWG des Rates vom 21.
Mai 1992 zur Erhaltung der natürlichen Lebensräume sowie der wild lebenden Tiere und Pflanzen (ABl. L 206 vom 22.7.1992, S. 7), die durch die Richtlinie 2013/17/EU vom 13.
Mai 2013 (ABl.
L 158 vom 10.6.2013, S. 193) geändert worden ist.

* * *

### Anlagen

1

[Anlage 1 - Gebiete von gemeinschaftlicher Bedeutung](/br2/sixcms/media.php/68/GVBl_II_73_2018-Anlage-1.pdf "Anlage 1 - Gebiete von gemeinschaftlicher Bedeutung") 1.2 MB

2

[Anlage 2 - Übersichtskarte, Topografische Karten](/br2/sixcms/media.php/68/GVBl_II_73_2018-Anlage-2.pdf "Anlage 2 - Übersichtskarte, Topografische Karten") 140.5 KB