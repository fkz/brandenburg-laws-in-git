## Verordnung über Zuständigkeiten zur Zulassung von Rohrfernleitungen und für den Vollzug der Rohrfernleitungsverordnung (Rohrfernleitungszuständigkeitsverordnung - RohrFLtgZV)

#### § 1 Planfeststellung und Plangenehmigung für bestimmte Leitungsanlagen

(1) Das Landesamt für Bergbau, Geologie und Rohstoffe ist zuständig für die Planfeststellung und Plangenehmigung nach § 20 des Gesetzes über die Umweltverträglichkeitsprüfung bezüglich der

1.  Rohrleitungsanlagen zum Befördern von verflüssigten Gasen im Sinne der Nummer 19.4,
2.  Rohrleitungsanlagen zum Befördern von nichtverflüssigten Gasen im Sinne der Nummer 19.5 und
3.  Rohrleitungsanlagen zum Befördern von Dampf oder Warmwasser im Sinne der Nummer 19.7

der Anlage 1 des Gesetzes über die Umweltverträglichkeitsprüfung.

(2) Das für Umwelt zuständige Landesamt ist zuständig für die Planfeststellung und Plangenehmigung nach § 20 des Gesetzes über die Umweltverträglichkeitsprüfung bezüglich der

1.  Rohrleitungsanlagen zum Befördern wassergefährdender Stoffe im Sinne der Nummer 19.3,
2.  Rohrleitungsanlagen zum Befördern von gefährlichen Stoffen nach § 3a des Chemikaliengesetzes im Sinne der Nummer 19.6 und
3.  Wasserfernleitungen im Sinne der Nummer 19.8

der Anlage 1 des Gesetzes über die Umweltverträglichkeitsprüfung.
Sofern die Leitungsanlage der Bergaufsicht unterliegt, ist abweichend hiervon das Landesamt für Bergbau, Geologie und Rohstoffe zuständig.

#### § 2 Anzeigeverfahren

(1) Das Landesamt für Bergbau, Geologie und Rohstoffe ist zuständig für die Bearbeitung von Anzeigen nach § 4a der Rohrfernleitungsverordnung bezüglich der in § 1 Absatz 1 Nummer 1 und 2 genannten Anlagen, welche die Voraussetzungen des § 2 Absatz 2 Satz 1 Nummer 2 der Rohrfernleitungsverordnung erfüllen.

(2) Das für Umwelt zuständige Landesamt ist zuständig für die Bearbeitung von Anzeigen nach § 4a der Rohrfernleitungsverordnung bezüglich der in § 1 Absatz 2 Satz 1 Nummer 2 genannten Anlagen, welche die Voraussetzungen des § 2 Absatz 2 Satz 1 Nummer 2 der Rohrfernleitungsverordnung erfüllen.

#### § 3 Überwachungsaufgaben

(1) Das Landesamt für Bergbau, Geologie und Rohstoffe ist zuständig für den Vollzug der §§ 3, 4, 5, 7, 8, 8a und 11 der Rohrfernleitungsverordnung bezüglich der in § 1 Absatz 1 Nummer 1 und 2 sowie der in § 2 Absatz 1 genannten Anlagen.

(2) Das für Umwelt zuständige Landesamt ist zuständig für den Vollzug der §§ 3, 4, 5, 7, 8, 8a und 11 der Rohrfernleitungsverordnung bezüglich der in § 1 Absatz 2 Satz 1 Nummer 1 und 2 sowie der in § 2 Absatz 2 genannten Anlagen.

#### § 4 Ordnungswidrigkeiten

Zuständig für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 23 des Gesetzes über die Umweltverträglichkeitsprüfung und § 10 der Rohrfernleitungsverordnung ist die jeweils nach den §§ 1 bis 3 zuständige Behörde.