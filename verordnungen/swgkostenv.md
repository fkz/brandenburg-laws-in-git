## Verordnung über die Erstattung des Zusatzaufwandes aus der Anwendung des Sorben/Wenden-Gesetzes (SWG-Kostenerstattungsverordnung - SWGKostenV)

Auf Grund des § 13b Absatz 4 des Sorben/Wenden-Gesetzes vom 7.
Juli 1994 (GVBl.
I S. 294), der durch Artikel 1 Nummer 17 des Gesetzes vom 11.
Februar 2014 (GVBl.
I Nr. 7) eingefügt worden ist, verordnet die Ministerin für Wissenschaft, Forschung und Kultur im Benehmen mit dem Ausschuss für Wissenschaft, Forschung und Kultur sowie dem Rat für Angelegenheiten der Sorben/Wenden:

#### § 1 Verwaltungskostenpauschale

(1) Ämtern, Verbandsgemeinden, Gemeinden, Mitverwaltungen und Landkreisen im angestammten Siedlungsgebiet der Sorben/Wenden wird eine jährliche Pauschale zur Abgeltung der laufend sowie einmalig entstehenden Verwaltungskosten erstattet.
Zu den laufenden Kosten zählen Übersetzungskosten insbesondere von Wahl- und Abstimmungsbekanntmachungen, Bezeichnungen von Verwaltungsgliederungen, Wegeleitsystemen und Wegweisern, geografischen Bezeichnungen und Straßennamen, Textanteilen in Veröffentlichungen und Bürgerschreiben.
Einmalig fallen in der Regel Kosten an wie die Umstellung von Dienstsiegeln, des Layouts von Briefköpfen und Visitenkarten auf deutsch-niedersorbische Zweisprachigkeit inklusive der dafür notwendigen Übersetzungen oder die Ergänzung von Systemen der Datenverarbeitung und Einzelergänzungen oder -korrekturen von Beschriftungen.

(2) Die Pauschale beträgt für Ämter, Verbandsgemeinden, mitverwaltende Gemeinden, amtsfreie Gemeinden und Landkreise 1 000 Euro und für amtsangehörige Gemeinden, Ortsgemeinden und mitverwaltete Gemeinden 500 Euro je Kalenderjahr.

(3) Bei Verbandsgemeinden, Mitverwaltungen und Ämtern ist für den Anspruch auf die Verwaltungskostenpauschale maßgeblich, ob mindestens ein Teil einer Ortsgemeinde, mitverwalteten oder amtsangehörigen Gemeinde zum angestammten Siedlungsgebiet der Sorben/Wenden gehört.

(4) Bei Gemeinden, deren Zugehörigkeit zum angestammten Siedlungsgebiet der Sorben/Wenden erst nach dem 1.
Juni 2014 festgestellt wurde, wird die Pauschale erst ab der Feststellung der Zugehörigkeit gezahlt und bemisst sich für das Jahr der Feststellung anteilig.

(5) Die Verwaltungskostenpauschalen nach Absatz 1 werden beginnend mit dem Jahr 2021 zu Beginn eines Kalenderjahres durch das Ministerium für Wissenschaft, Forschung und Kultur auf ein von der erstattungsberechtigten Kommune benanntes Konto zugewiesen.
Im Falle einer späteren Entscheidung der Rechtmäßigkeit der Feststellung der Zugehörigkeit zum angestammten Siedlungsgebiet der Sorben/Wenden wird der Gesamtbetrag vom Zeitpunkt der Feststellung bis zum laufenden Kalenderjahr der Entscheidung zugewiesen.
Für das Jahr 2020 ist ein schriftlicher Antrag auf Zahlung der Verwaltungskostenpauschale bei dem für Angelegenheiten der Sorben/Wenden zuständigen Referat des Ministeriums für Wissenschaft, Forschung und Kultur zu stellen.

#### § 2 Erstattung von Zusatzkosten

(1) Auf Antrag werden Ämtern, Verbandsgemeinden, Gemeinden, Mitverwaltungen und Landkreisen im angestammten Siedlungsgebiet durch die Umsetzung des Sorben/Wenden-Gesetzes entstehende zusätzliche Kosten erstattet für:

1.  den Aufwand, der durch die Einsetzung von hauptamtlichen Beauftragten für die Angelegenheiten der Sorben/Wenden nach § 6 Absatz 1 des Sorben/Wenden-Gesetzes entsteht, sofern sie nicht durch die Erstattung nach § 3 abgegolten sind,
2.  den Verwaltungsaufwand durch die Verwendung der niedersorbischen Sprache in Verwaltungen nach § 8 des Sorben/Wenden-Gesetzes, sofern sie nicht durch die Verwaltungskostenpauschale nach § 1 abgegolten sind,
3.  den Aufwand für die zweisprachige Beschriftung von öffentlichen Gebäuden und Einrichtungen, Straßen, Wegen, Plätzen, Brücken und Ortstafeln sowie Hinweisschilder hierauf nach § 11 des Sorben/Wenden-Gesetzes, sofern sie nicht durch die Verwaltungskostenpauschale nach § 1 abgegolten sind.

(2) Voraussetzung für eine Erstattung nach Absatz 1 ist der Nachweis des durch die Verwendung der niedersorbischen Sprache entstandenen Zusatzaufwandes.
Dieser Nachweis über den Zusatzaufwand kann insbesondere durch gesonderte Ausweisung in Rechnungen oder entsprechend nur für den niedersorbischen Sprachanteil ausgestellte Rechnungen erbracht werden.

(3) Die Maßnahme, durch die der Zusatzaufwand entstand, ist im Kostenerstattungsantrag so darzustellen, dass die Kostenarten und die Höhe nachvollziehbar sind.
Eine pauschale Angabe von Gesamtvorhaben und Rechnungssumme ist nicht ausreichend.

(4) Die Anträge nach Absatz 1 sind bis zum 30.
Juni des Folgejahres nach Entstehen der Kosten von den jeweils Antragsberechtigten mit den in den Absätzen 2 und 3 geforderten Nachweisen bei dem für Angelegenheiten der Sorben/Wenden zuständigen Referat des Ministeriums für Wissenschaft, Forschung und Kultur zu stellen.

(5) Ein Anspruch auf Erstattung der Zusatzkosten im laufenden Haushaltsjahr besteht nicht.

(6) Vorhaben, die voraussichtlich Erstattungsansprüche in Höhe von mindestens 5 000 Euro zur Folge haben, sind vor Maßnahmenbeginn gegenüber dem für Angelegenheiten der Sorben/Wenden zuständigen Referat des Ministeriums für Wissenschaft, Forschung und Kultur anzuzeigen.
Eine Nichtanzeige begründet eine Nachrangigkeit der Erstattung gegenüber anderen Erstattungsanträgen.
Für Vorhaben, die voraussichtliche Erstattungsansprüche in Höhe von bis zu 5 000 Euro auslösen werden, kann eine Anzeige nach Satz 1 freiwillig erfolgen.

#### § 3 Erstattung für Personal-, Sach- und Gemeinkosten der hauptamtlichen Beauftragten

(1) Den Landkreisen und der kreisfreien Stadt im angestammten Siedlungsgebiet der Sorben/Wenden werden die ab dem 1.
Januar 2019 tatsächlich anfallenden Personalkosten für die hauptamtlichen Beauftragten nach § 6 Absatz 1 des Sorben/Wenden-Gesetzes erstattet.

(2) Den Landkreisen und der kreisfreien Stadt im angestammten Siedlungsgebiet der Sorben/Wenden werden die ab dem 1.
Januar 2019 im Zusammenhang mit den Stellen der hauptamtlichen Beauftragten nach § 6 Absatz 1 des Sorben/Wenden-Gesetzes anfallenden Sach- und Gemeinkosten jährlich pauschal in Höhe von zusammen 20 Prozent der Bruttopersonalkosten nach Absatz 1 plus 8 800 Euro erstattet.

(3) Die tatsächlich angefallenen Personalkosten nach Absatz 1 werden nach Ablauf des Kalenderjahres auf der Grundlage einer von den Erstattungsberechtigten bei dem für Angelegenheiten der Sorben/Wenden zuständigen Referat des Ministeriums für Wissenschaft, Forschung und Kultur eingereichten tabellarischen Übersicht zusammen mit der sich aus ihnen ergebenden Sach- und Gemeinkostenpauschale nach Absatz 2 erstattet.
Die Anträge zur Erstattung sollen bis zum 31. März des Folgejahres eingereicht werden.

#### § 4 Evaluierung des Erstattungsverfahrens

Die getroffenen Regelungen sollen im Anschluss an die ersten beiden vollständigen Haushaltsjahre nach Inkrafttreten dieser Verordnung evaluiert werden.

#### § 5 Inkrafttreten, Außerkrafttreten

(1) Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über die Erstattung des Zusatzaufwandes aus der Anwendung des Sorben/Wenden-Gesetzes vom 25. Oktober 2016 (GVBl.
II Nr.
57) außer Kraft.

(2) Diese Verordnung wird auch in niedersorbischer Sprache veröffentlicht.

Potsdam, den 17.
September 2020

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Dr.
Manja Schüle

* * *

Póstajenje dla zarownanja pśidatnych wudankow z nałožowanja Serbskeje kazni (Skkostyp)  
wót 17.
septembera 2020
----------------------------------------------------------------------------------------------------------------

Na zakłaźe § 13b wótrězka 4 Serbskeje kazni wót 7.
julija 1994 (GVBl.
I b.
294), kótaryž jo se pśipisał pśez artikel 1 numer 17 kazni wót 11.
februara 2014 (GVBl.
I nr.
7), póstajijo ministaŕka za wědomnosć, slěźenje a kulturu pó konsultaciji Wuběrka za wědomnosć, slěźenje a kulturu a Rady za nastupnosći Serbow slědujuce:

#### § 1 Pawšala zastojnskich kostow 

(1) Amtam, gmejnskim zwěstkam, gmejnam, sobuzastojnstwam a wokrejsam we starodawnem sedleńskem rumje Serbow ma se zarownaś lětna pawšala zastojnskich kostow, ako běžnych tak teke jadnorazowych.
K běžnym kostam lice pśełožowaŕske kosty, wósebnje za znatecynjenja dla wuzwólowanjow a zgłosowanjow, pomjenjenja zastojnskich strukturow, orientaciske systemy a drogowniki, geografiske a drogowe mjenja, tekstowe źělby we publikacijach a wobydlaŕskich listach.
Jano jaden raz nastanu zwětšego kosty za pśestajenje na nimsko-dolnoserbsku dwójorěcnosć słužbnych zygelow, layouta listowych głowow a wizitnych kórtkow inkluziwnje trjebnych pśełožkow, kaž teke kosty za dopołnjenje systemow pśeźěłanja datow a jadnotliwe dopołnjenja abo korektury napismow.

(2) Pawšala za amty, gmejnske zwěstki, sobuzastojece gmejny, bźezamtne gmejny a wokrejse wunjaso 1 000 euro a za amtam pśisłušajuce gmejny, cłonkojske gmejny gmejnskich zwěstkow a sobuzastojane gmejny 500 euro za kalendariske lěto.

(3) Gmejnske zwěstki, sobuzastojnstwa a amty maju pšawo na pawšalu k zarownanju zastojnskich kostow, jo-lic až nanejmjenjej jaden źěł cłonkojskeje gmejny gmejnskego zwěstka, sobuzastojaneje gmejny abo amtoju pśisłušajuceje gmejny słuša do starodawnego sedleńskego ruma Serbow.

(4) Pla gmejnow, kótarychž pśisłušnosć k starodawnemu sedleńskemu rumoju Serbow jo se oficielnje wuznała akle pó 1.
juniju 2014, ma pawšala se płaśiś akle wót casa wuznaśa pśisłušnosći a se woblicyjo za to lěto wuznaśa późělnje.

(5) Pawšale zastojnskich kostow pó mysli wótstawka 1 wupłaśiju se wót Ministaŕstwa za wědomnosć, slěźenje a kulturu na zachopjeńku kalendariskego lěta, zachopjecy wót lěta 2021, na konto, kótarež wopšawnjona gmejna jim znate cyni.
We paźe pózdźejšego wusuźenja, až wuznaśe pśisłušnosći k starodawnemu sedleńskemu rumoju Serbow jo było pšawne, wupłaśijo se wša suma wót casa wuznaśa až do běžecego kalendariskego lěta, we kótaremž to wusuźenje jo se stało.
Za lěto 2020 ma se stajiś pisna pšosba wó zapłaśenje pawšale zastojnskich kostow pla referata za nastupnosći Serbow we Ministaŕstwje za wědomnosć, slěźenje a kulturu.

#### § 2 Zarownanje pśidatnych kostow

(1) Na póžedanje zarownaju se amtam, gmejnskim zwěstkam, gmejnam, sobuzastojnstwam a wokrejsam we starodawnem sedleńskem rumje pśidatne kosty dla nałožowanja Serbskeje kazni, a to za:

1.  wudanki, kótarež nastanu pśez pśistajenje głownoamtskich zagronitych za nastupnosći Serbow pó mysli § 6 wótstawk 1 Serbskeje kazni, njejsu-li wóni južo pó § 3 zarownane,
2.  zastojnske wudanki dla nałožowanja dolnoserbskeje rěcy pó § 8 Serbskeje kazni, njejsu-li wóni južo pśez pawšalu pó § 1 zarownane,
3.  wudanki za dwójorěcne napisma na zjawnych chromach a institucijach, drogach, naměstach, móstach a sedlišćowych toflach kaž teke za pokazowarje k nim pó § 11 Serbskeje kazni, njejsu-li wóni južo pśez pawšalu pó § 1 zarownane.

(2) Wuměnjenje za zarownanje pó wótstawku 1 jo dopokazanje wušych wudankow dla nałožowanja dolnoserbskeje rěcy.
To dopokazanje móžo se staś pśedewšym pśez wósebne wupokazanje na zliceńce abo pśez zliceńku, kótaraž jo wustajona jano za serbskorěcnu źělbu.

(3) Pśedewześe, pśez kótarež su wuše wudanki nastali, ma se we póžedanju zarownanja kostow tak pśedstajiś, až kategorije kostow a jich wusokosć su k rozměśu.
Pawšalne pódaśe cełych pśedewześow a zliceńskeje sumy njejo dosć.

(4) Póžedanja pó wótstawku 1 deje wopšawnjone póžedarje stajiś až do 30.
junija pśiducego lěta pó nastaśu kostow pla referata za nastupnosći Serbow we Ministaŕstwje za wědomnosć, slěźenje a kulturu.
Gromaźe z póžedanim deje se prědkpołožyś dopokaze pó wótstawku 2 a 3.

(5) Pšawo, aby pśidatne kosty se zarownali we běžecem etatowem lěśe, njewobstoj.

(6) Pśedewześa, za kótarež buźo se nejskerjej póžedaś zarownanje we wusokosći 5 000 euro a wěcej, maju se pśed zachopjeńkom napšawy pśipowěźeś referatoju za nastupnosći Serbow we Ministaŕstwje za wědomnosć, slěźenje a kulturu.
Njepśipowěźenje móžo zawinowaś nišu poziciju zarownanja napśeśiwo drugim póžedanjam.
Pśedewześa, za kótarež buźo se nejskerjej póžedaś zarownanje nižej 5 000 euro, mógu se z dobreju wólu kaž we saźe 1 pśipowěźeś.

#### § 3 Zarownanje personalnych, wěcnych a powšyknych kostow głownoamtskich zagronitych

(1) Wokrejsam a bźezwokrejsnemu městoju we starodawnem sedleńskem rumje Serbow zarownaju se wót 1.
januara lěta 2019 wšykne napšawdu nastate personalne kosty za głownoamtske zagronite pó § 6 wótstawk 1 Serbskeje kazni.

(2) Wokrejsam a bźezwokrejsnemu městoju we starodawnem sedleńskem rumje Serbow zarownaju se wót 1.
januara lěta 2019 wěcne a powšykne kosty, zwězane ze źěłowymi městnami głownoamtskich zagronitych pó § 6 wótstawk 1 Serbskeje kazni, kužde lěto pawšalnje we wusokosći 20 % personalnych brutto-kostow pó wótstawku 1 plus 8 800 euro.

(3) Napšawdne personalne kosty pó wótstawku 1 zarownaju se pó kóńcu kalendariskego lěta na zakłaźe tabelariskego pśeglěda, kótaryž wopšawnjone póžedarje deje prědkpołožyś referatoju za nastupnosći Serbow we Ministaŕstwje za wědomnosć, slěźenje a kulturu.
Z tym zrownju se jim wupłaśijo woblicona pawšala za wěcne a powšykne kosty pó wótstawku 2.
Póžedanja za zarownanim deje se stajiś až do 31.
měrca pśiducego lěta.

#### § 4 Ewaluěrowanje zarownańskeje procedury

Toś ta tudy wustajona procedura ma se ewaluěrowaś pó dwěma prědnyma połnyma etatowyma lětoma za tym, ako póstajenje jo nabyło płaśiwosć.

#### § 5 Nabyśe płaśiwosći, kóńc płaśiwosći

(1) Toś to póstajenje nabydnjo płaśiwosć na pśiducem dnju pó wózjawjenju.
Z tym zrownju zgubijo płaśiwosć Póstajenje wó zarownanju pśidatnych wudankow z nałožowanja Serbskeje kazni wót 25. oktobera 2016 (GVBl.
II nr.
57).

(2) Toś to póstajenje ma se teke w dolnoserbskej rěcy wózjawiś.

Pódstupim, dnja 17.
septembera 2020

Ministaŕka za wědomnosć, slěźenje a kulturu

dr.
Manja Schüle