## Verordnung zur Finanzierung der Pflegeausbildung nach dem Pflegeberufegesetz im Land Brandenburg (Brandenburgische Pflegeberufe-Ausbildungsfinanzierungsverordnung - BbgPflAFinV)

Auf Grund des § 3 Nummer 4, 5, 6 und 8 des Brandenburgischen Pflegeberufeumsetzungsgesetzes vom 18.
Dezember 2020 (GVBl.
I Nr. 41) verordnet die Ministerin für Soziales, Gesundheit, Integration und Verbraucherschutz:

#### § 1 Begriffsbestimmungen

(1) Umlagepflichtige Einrichtungen im Sinne dieser Verordnung sind die in § 26 Absatz 3 Nummer 1 und 2 des Pflegeberufegesetzes vom 17.
Juli 2017 (BGBl.
I S. 2581), das zuletzt durch Artikel 13a des Gesetzes vom 24.
Februar 2021 (BGBl.
I S.
274) geändert worden ist, genannten und im Land Brandenburg gelegenen Krankenhäuser sowie stationären und ambulanten Pflegeeinrichtungen.
Diese nehmen an der Finanzierung des Ausgleichsfonds teil.
Hospize sind keine Einrichtungen im Sinne des Satzes 1 und vom Finanzierungsverfahren ausgenommen.

(2) Die mitzuteilenden Daten im Sinne dieser Verordnung sind die Daten, die die Träger der praktischen Ausbildung nach § 8 des Pflegeberufegesetzes, die Pflegeschulen nach § 9 des Pflegeberufegesetzes oder die Rechtsträger der umlagepflichtigen Einrichtungen nach Absatz 1 nach den Maßgaben der §§ 5, 10 Absatz 2 Satz 1, § 11 Absatz 2 bis 4, §§ 16 und 17 Absatz 1 und §§ 21 bis 23 der Pflegeberufe-Ausbildungsfinanzierungsverordnung der zuständigen Stelle mitzuteilen haben.

#### § 2 Ergänzende Bestimmungen zu den umlagepflichtigen Einrichtungen

(1) Grundlage für die Ermittlung der Mehrkosten der Ausbildungsvergütung nach § 27 Absatz 2 des Pflegeberufegesetzes bildet das durchschnittliche Jahresarbeitgeberbruttogehalt aller in der meldenden Einrichtung beschäftigten Pflegefachkräfte ohne Zusatzfunktion oder ohne Leitungsfunktion bezogen auf eine Vollkraft.

(2) Als beschäftigte Pflegefachkräfte im Sinne des § 11 Absatz 2 der Pflegeberufe-Ausbildungsfinanzierungsverordnung gelten Pflegefachkräfte, für die mit der meldenden Pflegeeinrichtung zum Stichtag ein nicht ruhender Beschäftigungsvertrag als Pflegefachkraft bestand, unabhängig davon, ob die Pflegefachkraft zu diesem Stichtag eingesetzt war.
Als eingesetzte Pflegefachkräfte über die Regelung des Satzes 1 hinaus gelten diejenigen Pflegefachkräfte, die im Wege der Arbeitnehmerüberlassung zum angegebenen Stichtag in der Pflegeeinrichtung tätig waren, auch wenn kein Beschäftigungsvertrag mit der Pflegeeinrichtung besteht.

(3) Als Vollbeschäftigung für die Bestimmung eines Vollzeitäquivalentes im Sinne des § 11 Absatz 2 und 3 der Pflegeberufe-Ausbildungsfinanzierungsverordnung sind die Vorgaben des jeweiligen Tarifvertrages oder einer entsprechenden kirchlichen Arbeitsrechtsregelung der meldenden Einrichtung maßgeblich.
Sofern die meldende Einrichtung keinem Tarifvertrag oder einer entsprechenden kirchlichen Arbeitsrechtsregelung unterliegt, wird ein Wochenstundenumfang von durchschnittlich 40 Stunden festgelegt.

(4) Bei Verschmelzungen nach Maßgaben des Umwandlungsgesetzes vom 28.
Oktober 1994 (BGBl. I S. 3210; 1995 I S.
428), das zuletzt durch Artikel 1 des Gesetzes vom 19.
Dezember 2018 (BGBl. I S. 2694) geändert worden ist, in der jeweils geltenden Fassung werden dem Rechtsträger der umlagepflichtigen Einrichtung sämtliche Vortätigkeiten der verschmolzenen Unternehmen zugerechnet.
Im Falle der Abspaltung nach Maßgaben des Umwandlungsgesetzes werden dem Rechtsträger der umlagepflichtigen Einrichtung die Vortätigkeiten des abgespaltenen Unternehmens zugerechnet.
Im Falle eines Betriebsüberganges auf einen neuen Rechtsträger durch Veräußerung, Pacht oder aus sonstigen Gründen wird vermutet, dass der neue Rechtsträger die umlagepflichtigen Einrichtungen in gleichem Umfang weiterbetreibt.
Der neue Rechtsträger kann diese Vermutung durch das Beibringen von geeigneten Nachweisen widerlegen.

(5) Übernimmt ein Rechtsträger eine ambulante Einrichtung im Festsetzungsjahr oder im diesem vorangegangenen Kalenderjahr von einem anderen Rechtsträger im Wege des Betriebsüberganges nach Absatz 4 Satz 3, teilt er der zuständigen Stelle außerdem mit, von welchem Rechtsträger die Einrichtung übernommen wurde und gibt entsprechend die abgerechneten Punkte oder Zeitwerte des bisherigen Rechtsträgers an.

(6) Bei Aufgabe des Betriebs oder der Kündigung des Versorgungsvertrags findet bezogen auf den Zeitpunkt der Einstellung des Betriebs bezüglich der Einzahlungsverpflichtungen und Auszahlungsansprüche des Betriebs gegenüber der zuständigen Stelle eine unverzügliche Abrechnung nach den §§ 7 bis 8 statt.

#### § 3 Ergänzende Bestimmungen zu den Mitteilungspflichten

(1) Die Übermittlung der mitzuteilenden Daten nach § 1 Absatz 2 erfolgt einrichtungsbezogen über das Onlineportal der zuständigen Stelle.

(2) Die Rechtsträger der umlagepflichtigen Einrichtungen nach § 1 Absatz 1, die Träger der praktischen Ausbildung und die Pflegeschulen haben zur Nutzung des Onlineportals sicherzustellen, dass nur über den Rechtsträger legitimierte Personen mitzuteilende Daten melden.
Änderungen hinsichtlich der Legitimation sind zeitnah über das Onlineportal anzugeben.

(3) Die Rechtsträger der umlagepflichtigen Einrichtungen, die Träger der praktischen Ausbildung und die Pflegeschulen haben die von ihnen mitzuteilenden Daten unter Einhaltung der in der Pflegeberufe-Ausbildungsfinanzierungsverordnung bestimmten Fristen zu melden.

(4) Die Träger der praktischen Ausbildung und die Pflegeschulen haben Fehlzeiten nach § 13 Absatz 1 und 2 des Pflegeberufegesetzes, die zu einer Nichtzahlung von Ausbildungsvergütung von mehr als 6 Wochen führen, der zuständigen Stelle unverzüglich mitzuteilen.

(5) Die zuständige Stelle kann im Rahmen der Prüfung der gemeldeten Daten unter angemessener Fristsetzung anordnen, geeignete Nachweise vorzulegen.
Dies gilt auch dann, wenn die Daten nicht oder nicht fristgerecht gemeldet wurden.

(6) Die zuständige Stelle kann verspätet gemeldete Daten und verspätet vorgelegte Nachweise nach Absatz 5 dennoch berücksichtigen, soweit dadurch das gesamte Finanzierungsverfahren nicht verzögert oder gefährdet würde.

(7) Die zuständige Stelle legt die Form der mitzuteilenden Daten nach § 10 Absatz 2 Satz 1 der Pflegeberufe-Ausbildungsfinanzierungsverordnung fest, die die Vertragsparteien nach § 18 Absatz 1 Satz 2 des Krankenhausfinanzierungsgesetzes zu übermitteln haben.
Satz 1 gilt auch für die mitzuteilenden Daten nach § 11 Absatz 1 der Pflegeberufe-Ausbildungsfinanzierungsverordnung der Landesverbände der Pflegekassen.

(8) Die mitzuteilenden Daten nach § 11 Absatz 4 der Pflegeberufe-Ausbildungsfinanzierungsverordnung, die für die Berechnung nach § 12 Absatz 3 Satz 1 der Pflegeberufe-Ausbildungsfinanzierungsverordnung von der jeweiligen ambulanten Pflegeeinrichtung im Vorjahr des Festsetzungsjahres aufgrund des in Brandenburg geltenden Abrechnungssystems, unabhängig der Kostentragung, anzugeben sind, sind alle Leistungen des Elften Buches Sozialgesetzbuch, die nach Punkten abgerechnet werden.
Soweit die Abrechnung der nach Satz 1 erbrachten, ambulanten Pflegeleistungen nicht auf Basis von Punkten in Verbindung mit Punktwerten, sondern anhand von Zeiten in Verbindung mit Zeitvergütungen erfolgt, wird auf der Grundlage des ermittelten Umsatzes, der durch die Zeitvergütung erwirtschaftet wurde, und des individuell vereinbarten Punktwertes, eine fiktive Punktzahl ermittelt.
Ist mit der jeweiligen ambulanten Pflegeeinrichtung ein individueller Punktwert nicht vereinbart, wird der Ermittlung der fiktiven Punktzahl ein landesdurchschnittlicher Punktwert zugrunde gelegt.
Der Landesdurchschnittspunktwert ergibt sich aus der am Stichtag 15. Dezember des Vorjahres mit den im Land Brandenburg zugelassenen ambulanten Pflegeeinrichtungen vereinbarten Punktwerten.
Nutzt eine ambulante Pflegeeinrichtung beide Abrechnungssystematiken, wird die fiktive Punktzahl den tatsächlich abgerechneten Punkten hinzugerechnet.

#### § 4 Schätzbefugnis

Die zuständige Stelle kann entsprechend § 30 Absatz 5, § 31 Absatz 5 des Pflegeberufegesetzes und § 7 Absatz 2 Satz 1 der Pflegeberufe-Ausbildungsfinanzierungsverordnung bei nicht erfolgter, nicht fristgemäßer, fehlerhafter oder unvollständiger Meldung der mitzuteilenden Daten nach § 10 Absatz 2 Satz 1 und § 11 Absatz 2 bis 4 der Pflegeberufe-Ausbildungsfinanzierungsverordnung

1.  die voraussichtliche Anzahl der voll- und teilstationären Behandlungsfälle der Einrichtungen nach § 7 Absatz 1 Nummer 1 des Pflegeberufegesetzes,
2.  die Zahl der beschäftigten oder eingesetzten Pflegefachkräfte der Einrichtungen nach § 7 Absatz 1 Nummer 2 und 3 des Pflegeberufegesetzes,
3.  die Anzahl der abgerechneten Punkte oder Zeitwerte der Einrichtungen nach § 7 Absatz 1 Nummer 3 des Pflegeberufegesetzes sowie
4.  die Anzahl der zum 1.
    Mai des Festsetzungsjahres vorzuhaltenden Pflegefachkräfte der Einrichtungen nach § 7 Absatz 1 Nummer 2 des Pflegeberufegesetzes

anhand der ihr vorliegenden Erkenntnisse schätzen.
Satz 1 gilt entsprechend, wenn der Anordnung zur Vorlage von geeigneten Nachweisen nach § 3 Absatz 6 nicht oder nicht rechtzeitig nachgekommen wird.

#### § 5 Festsetzung und Zahlung der Ausgleichszuweisung

(1) Die Höhe der monatlichen Ausgleichszuweisungen wird nach Maßgabe der §§ 5 bis 8 und des § 14 der Pflegeberufe-Ausbildungsfinanzierungsverordnung ermittelt und festgesetzt.

(2) Soweit Ausbildungskosten nach anderen Vorschriften aufgebracht werden, sind diese unverzüglich an den Ausgleichsfonds zu melden.
Diese zusätzlichen Zahlungen werden nach § 29 Absatz 4 des Pflegeberufegesetzes entsprechend mindernd bei der Festlegung des Ausbildungsbudgets berücksichtigt.
Soweit keine Berücksichtigung nach Satz 2 erfolgte, sind diese zusätzlichen Zahlungen mit der Ausgleichszuweisung nach § 34 Absatz 3 Satz 3 des Pflegeberufegesetzes zu verrechnen.

(3) Bei in Teilzeit ausgeübten Ausbildungen werden bei der Ermittlung der Ausgleichszuweisungen nach Absatz 1 die Ausbildungsbudgets im Sinne des § 29 des Pflegeberufegesetzes anteilig nach dem Umfang der Teilzeit zur regelmäßigen wöchentlichen Arbeitszeit berücksichtigt und festgesetzt.

(4) Die Zahlung von Ausgleichszuweisungen an den Träger der praktischen Ausbildung und an die Pflegeschulen erfolgt nach den Maßgaben des § 15 der Pflegeberufe-Ausbildungsfinanzierungsverordnung.

(5) Die Zahlung von Ausgleichszuweisungen nach Absatz 4 erfolgt nur für in Brandenburg gelegene Träger der praktischen Ausbildung nach § 8 des Pflegeberufegesetzes oder für in Brandenburg gelegene Pflegeschulen nach § 9 des Pflegeberufegesetzes.

(6) Für den Fall, dass aufgrund von Forderungsausfällen und Zahlungsverzügen bei den, bis zum jeweiligen monatlichen Auszahlungstermin nach § 15 Absatz 1 der Pflegeberufe-Ausbildungsfinanzierungsverordnung, eingegangenen Umlagebeträgen der festgesetzte Gesamtfinanzierungsbedarf, unter Berücksichtigung der Liquiditätsreserve nach § 32 Absatz 1 Satz 1 Nummer 2 des Pflegeberufegesetzes, nicht ausreicht, um den Trägern der praktischen Ausbildung und den Pflegeschulen die vollen Ausgleichszuweisungen anzuweisen, sind diese von der zuständigen Stelle anteilig zu kürzen.
Die Verwaltungskostenpauschale nach § 32 Absatz 2 des Pflegeberufegesetzes wird hierbei nicht berücksichtigt.

#### § 6 Festsetzung und Zahlung der Umlagebeträge

(1) Der Finanzierungsbedarf wird nach § 26 Absatz 3 des Pflegeberufegesetzes und § 13 der Pflegeberufe-Ausbildungsfinanzierungsverordnung durch jährliche Direktzahlung des Landes Brandenburg, der sozialen Pflichtversicherung und der privaten Pflege-Pflichtversicherung sowie durch die Erhebung der monatlichen Umlagebeträge von den umlagepflichtigen Einrichtungen nach § 1 Absatz 1 aufgebracht.

(2) Der von den umlagepflichtigen Einrichtungen zu zahlende monatliche Umlagebetrag wird von der zuständigen Stelle nach § 10 Absatz 2 Satz 2 und 3 und § 12 der Pflegeberufe-Ausbildungsfinanzierungsverordnung anhand der nach § 1 Absatz 2 und § 3 mitgeteilten Daten oder der nach § 4 geschätzten Daten berechnet und festgesetzt.

(3) Die Verpflichtung zur Zahlung der monatlichen Umlagebeträge entsteht mit Bekanntgabe zu den im Festsetzungsbescheid nach § 33 Absatz 3 Satz 3 und Absatz 4 Satz 2 des Pflegeberufegesetzes genannten Terminen.

#### § 7 Abrechnung der Ausgleichszuweisungen

(1) Die Träger der praktischen Ausbildung und die Pflegeschulen haben die Abrechnungen nach § 16 Absatz 1 der Pflegeberufe-Ausbildungsfinanzierungsverordnung der zuständigen Stelle unter Einhaltung der dort bestimmten Fristen vorzulegen.

(2) Für die Prüfung der Abrechnung der Ausgleichszuweisungen nach Absatz 1 kann unter angemessener Fristsetzung die Vorlage geeigneter Nachweise nach § 16 Absatz 2 der Pflegeberufe-Ausbildungsfinanzierungsverordnung angefordert werden.

(3) Die zuständige Stelle berücksichtigt bei der Abrechnung der an die Pflegeschulen gezahlten Ausgleichszuweisungen, entsprechend § 14 Absatz 2 der Pflegeberufe-Ausbildungsfinanzierungsverordnung, Änderungen der Schülerzahlen nach Beginn eines Schuljahres nicht.

(4) Die zuständige Stelle berücksichtigt bei der Abrechnung an den Träger der praktischen Ausbildung die in der Ausgleichszuweisung auf Grundlage der Ausbildungsbudgets gemäß § 29 Absatz 1 des Pflegeberufegesetzes in Verbindung mit § 14 Absatz 1 der Pflegeberufe-Ausbildungsfinanzierungsverordnung enthaltenen Mehrkosten der Ausbildungsvergütung sowie die Kosten der praktischen Ausbildung bis zum letzten Tag des Ausbildungsverhältnisses.

#### § 8 Abrechnung der Umlagebeträge

(1) Die Krankenhäuser und Pflegeeinrichtungen nach § 7 Absatz 1 des Pflegeberufegesetzes haben die Abrechnungen und die in Rechnung gestellten Ausbildungszuschläge nach § 17 Absatz 1 der Pflegeberufe-Ausbildungsfinanzierungsverordnung unter Einhaltung der dort bestimmten Frist vorzulegen.
Satz 1 gilt für den mitzuteilenden Differenzbetrag im Sinne des § 17 Absatz 1 Satz 1 der Pflegeberufe-Ausbildungsfinanzierungsverordnung entsprechend.

(2) Für die Prüfung der Abrechnung der Umlagebeträge nach Absatz 1 kann unter angemessener Fristsetzung die Vorlage geeigneter Nachweise angefordert werden.

(3) Die zuständige Stelle prüft den von den Einrichtungen im Sinne des § 7 Absatz 1 des Pflegeberufegesetzes mitgeteilten Differenzbetrag anhand der vorgelegten Abrechnung der im Finanzierungszeitraum geleisteten monatlichen Umlagebeträge und der jeweils in Rechnung gestellten Ausbildungszuschläge nach § 17 Absatz 1 der Pflegeberufe-Ausbildungsfinanzierungsverordnung und gleicht den Differenzbetrag auf dieser Grundlage nach § 17 Absatz 2 der Pflegeberufe-Ausbildungsfinanzierungsverordnung innerhalb des nächsten Finanzierungszeitraumes aus.
Die zuständige Stelle kann dabei in begründeten Einzelfällen verspätet vorgelegte Abrechnungen berücksichtigen.

(4) Der Differenzbetrag nach § 17 Absatz 2 der Pflegeberufe-Ausbildungsfinanzierungsverordnung wird den Einrichtungen durch die zuständige Stelle nur ausgeglichen, sofern diese die Zahlung der Umlagebeträge vorgenommen haben und die jeweiligen leistungsrechtlichen Möglichkeiten einer Refinanzierung der Umlagebeträge ausgeschöpft haben.

#### § 9 Bußgeldvorschriften

Ordnungswidrig handelt, wer vorsätzlich oder fahrlässig

1.  entgegen des § 3 Absatz 3 die mitzuteilenden Daten der zuständigen Stelle nicht fristgerecht meldet oder entgegen des § 5 Absatz 3 der Pflegeberufe-Ausbildungsfinanzierungsverordnung eine Aktualisierung der Angaben nicht mitteilt oder eine Änderung der Angaben nicht unverzüglich mitteilt,
2.  entgegen des § 3 Absatz 3 die zur Prüfung der gemeldeten Daten angeforderten Nachweise nicht fristgerecht vorlegt,
3.  entgegen des § 6 Absatz 2 den festgesetzten monatlichen Umlagebetrag nicht fristgerecht zahlt,
4.  entgegen des § 7 Absatz 1 die Abrechnungen der Ausgleichszuweisungen nicht fristgerecht vorlegt,
5.  entgegen des § 8 Absatz 1 die Abrechnungen und die in Rechnung gestellten Ausbildungszuschläge nicht fristgerecht vorlegt oder entgegen des § 8 Absatz 2 die darüber angeforderten Nachweise nicht fristgerecht vorlegt.

Die Ordnungswidrigkeit kann entsprechend des § 17 Absatz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl. I S. 602), das zuletzt durch Artikel 3 des Gesetzes vom 30.
November 2020 (BGBl.
I S.
2600) geändert worden ist, mit einer Geldbuße von bis zu 1 000 Euro geahndet werden.

#### § 10 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 6.
April 2021

Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

Ursula Nonnemacher