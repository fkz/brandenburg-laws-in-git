## Verordnung über beamtenrechtliche Zuständigkeiten bei dem Zentralen IT-Dienstleister der Justiz des Landes Brandenburg (ZenIT-ZV)

Auf Grund des § 4 Absatz 1 Satz 2 und 3 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl. I S. 26) in Verbindung mit § 1 Absatz 3 Satz 1 der Ernennungsverordnung vom 1.
August 2004 (GVBl. II S. 742), des § 54 Absatz 3 Satz 2 des Beamtenstatusgesetzes vom 17.
Juni 2008 (BGBl. I S. 1010), des § 17 Absatz 2 Satz 1 und 2, des § 34 Absatz 5, des § 35 Absatz 2 Satz 2 und des § 42 Absatz 2 Satz 2 des Landesdisziplinargesetzes vom 18.
Dezember 2001 (GVBl.
I S. 254) und des § 6 Absatz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28 S. 2) geändert worden ist, in Verbindung mit

*   § 32 Absatz 1 Satz 1, § 38 Satz 1 und 2, § 54 Absatz 1, § 57 Absatz 1 Satz 1 und 2, § 69 Absatz 5 Satz 1, § 84 Satz 1 und 2, § 85 Absatz 4 Satz 4, § 86 Absatz 3, § 87 Absatz 1 Satz 2 Nummer 1, § 88 Absatz 5, § 89 Satz 2 und 3 und § 92 Absatz 2 des Landesbeamtengesetzes, von denen § 85 Absatz 4 Satz 4, § 86 Absatz 3, § 87 Absatz 1 Satz 2 Nummer 1, § 88 Absatz 5 neu gefasst und § 89 Satz 2 durch das Gesetz vom 29. Juni 2018 (GVBl.
    I Nr.
    17) geändert worden sind,
*   § 13 Absatz 2 Satz 3 und § 58 Absatz 1 des Brandenburgischen Besoldungsgesetzes vom 20. November 2013 (GVBl.
    I Nr. 32 S. 2, Nr. 34),
*   § 6 Satz 1 und 2 der Dienstjubiläumsverordnung vom 18.
    Dezember 2014 (BGBl. I S. 2267) in Verbindung mit § 64 Absatz 1 Satz 1 des Landesbeamtengesetzes, der durch Gesetz vom 11. März 2010 (GVBl.
    I Nr. 13) neu gefasst worden ist,
*   § 4 Absatz 5 und § 9 Absatz 3 der Trennungsgeldverordnung in der Fassung der Bekanntmachung vom 29.
    Juni 1999 (BGBl. I S. 1533) in Verbindung mit § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes

verordnet die Ministerin der Justiz:

#### § 1 Ernennung der Beamtinnen und Beamten, Folgezuständigkeiten

(1) Die der obersten Dienstbehörde zustehende Befugnis zur Ernennung der Beamtinnen und Beamten wird für ihren oder seinen Geschäftsbereich auf die Direktorin oder den Direktor des Zentralen IT\-Dienstleisters der Justiz des Landes Brandenburg übertragen.

(2) Der Direktorin oder dem Direktor des Zentralen IT-Dienstleisters der Justiz des Landes Brandenburg wird in demselben Umfang auch die Befugnis der obersten Dienstbehörde übertragen für

1.  Entscheidungen nach § 22 Absatz 1, 2 und 3 des Beamtenstatusgesetzes in Verbindung mit § 32 Absatz 1 Satz 1 des Landesbeamtengesetzes (Feststellung der Beendigung des Dienstverhältnisses), nach § 28 Absatz 1 und 2 des Beamtenstatusgesetzes in Verbindung mit § 38 Satz 1 des Landesbeamtengesetzes (Versetzung einer Beamtin oder eines Beamten auf Probe in den Ruhestand) und nach § 39 des Beamtenstatusgesetzes in Verbindung mit § 54 Absatz 1 des Landesbeamtengesetzes (Verbot der Führung der Dienstgeschäfte),
2.  Zustimmungen nach § 13 Absatz 2 Satz 3 des Brandenburgischen Besoldungsgesetzes (Absehen von der Rückforderung von Bezügen),
3.  die Ausübung der Disziplinarbefugnisse bei Ruhestandsbeamtinnen und Ruhestandsbeamten nach § 17 Absatz 2 Satz 1 des Landesdisziplinargesetzes,
4.  die Kürzung der Dienstbezüge bis zum Höchstmaß nach § 34 Absatz 3 Nummer 1, die Erhebung der Disziplinarklage nach § 35 und den Erlass des Widerspruchsbescheids nach § 42 Absatz 2 Satz 1 des Landesdisziplinargesetzes,
5.  die Gewährung und Versagung von Jubiläumszuwendungen nach § 6 Satz 2 der Dienstjubiläumsverordnung in Verbindung mit § 64 Absatz 1 Satz 1 des Landesbeamtengesetzes,
6.  die Entgegennahme von Erklärungen und Entscheidungen in Nebentätigkeitsangelegenheiten, Untersagung von nicht genehmigungspflichtigen Nebentätigkeiten, Geltendmachung von Auskunftsverlangen in Nebentätigkeitsangelegenheiten und Entscheidungen über die Inanspruchnahme von Einrichtungen, Personal oder Material des Dienstherren nach den §§ 84,  85 Absatz 4 Satz 4, § 86 Absatz 2 und 3, § 88 Absatz 5, § 89 des Landesbeamtengesetzes und Untersagung von Tätigkeiten nach Beendigung des Beamtenverhältnisses nach § 41 Satz 2 des Beamtenstatusgesetzes in Verbindung mit § 92 Absatz 2 des Landesbeamtengesetzes,
7.  die Zustimmung zur Annahme von Belohnungen, Geschenken und sonstigen Vorteilen nach § 42 Absatz 1 Satz 2 des Beamtenstatusgesetzes in Verbindung mit § 57 Absatz 1 des Landesbeamtengesetzes,
8.  die Genehmigung der Führung der Amtsbezeichnung mit dem Zusatz „außer Dienst“ nach § 69 Absatz 5 Satz 1 des Landesbeamtengesetzes.

(3) Die Zuständigkeiten des für Inneres zuständigen Ministeriums, des für Finanzen zuständigen Ministeriums und des Landespersonalausschusses bleiben unberührt.

#### § 2 Vorverfahren

(1) Die der obersten Dienstbehörde zustehende Befugnis zur Entscheidung über den Widerspruch einer Beamtin oder eines Beamten, einer Ruhestandsbeamtin oder eines Ruhestandsbeamten, einer früheren Beamtin oder eines früheren Beamten und der Hinterbliebenen gegen den Erlass oder die Ablehnung eines Verwaltungsaktes oder gegen die Ablehnung eines Anspruchs auf eine Leistung wird der Direktorin oder dem Direktor des Zentralen IT-Dienstleisters der Justiz des Landes Brandenburg übertragen, soweit sie oder er selbst die mit dem Widerspruch angefochtene Entscheidung erlassen hat (§ 54 Absatz 3 Satz 2 des Beamtenstatusgesetzes).

(2) In anderen als den in Absatz 1 genannten Fällen ist für die Entscheidung über den Widerspruch das für Justiz zuständige Ministerium zuständig.

#### § 3 Übergangsvorschriften

Soweit vor Inkrafttreten dieser Verordnung andere als die in den §§ 1 und 2 bestimmten Zuständigkeiten bestanden, verbleibt es für die im Zeitpunkt des Inkrafttretens dieser Verordnung anhängigen Verwaltungsverfahren bei den bisherigen Zuständigkeiten.
Gleiches gilt hinsichtlich der Zuständigkeit für die Vertretung in zu diesem Zeitpunkt bereits anhängigen Rechtsstreitigkeiten.

#### § 4 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 6.
Februar 2020

Die Ministerin der Justiz

Susanne Hoffmann