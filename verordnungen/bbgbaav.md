## Verordnung über das Ausschreibungs- und Auswahlverfahren zur bevollmächtigten Bezirksschornsteinfegerin oder zum bevollmächtigten Bezirksschornsteinfeger (Brandenburgische Bezirksschornsteinfeger-Ausschreibungs- und Auswahlverordnung - BbgBAAV)

Auf Grund des § 9 Absatz 5 des Schornsteinfeger-Handwerksgesetzes vom 26. November 2008 (BGBl.
I S. 2242) in Verbindung mit § 2 Nummer 2 der Verordnung über wirtschaftsrechtliche Zuständigkeiten und Zuständigkeiten zur Zulassung von Rohrfernleitungen vom 7. September 2009 (GVBl.
II S.
604) verordnet der Minister für Wirtschaft und Europaangelegenheiten:

### § 1  
Anwendungsbereich, zuständige Behörde

(1) Diese Verordnung regelt die Verfahren zur Ausschreibung, zur Auswahl für die Tätigkeit als bevollmächtigte Bezirksschornsteinfegerin oder als bevollmächtigter Bezirksschornsteinfeger und zur Bestellung.

(2) Die Verfahren müssen sachgerecht, objektiv, transparent und nicht diskriminierend durchgeführt werden.

(3) Zuständige Behörde für die Verfahren ist die jeweilige Kreisordnungsbehörde nach § 1 Absatz 2 der Schornsteinfegerzuständigkeitsverordnung.

### § 2  
Anforderungen

(1) Die Bewerberinnen und Bewerber müssen:

1.  die handwerksrechtlichen Voraussetzungen zur selbstständigen Ausübung des Schornsteinfegerhandwerks besitzen,
2.  über die für die Erfüllung der Aufgaben als bevollmächtigte Bezirksschornsteinfegerin oder als bevollmächtigter Bezirksschornsteinfeger erforderlichen Rechtskenntnisse verfügen,
3.  die für die Ausübung der Tätigkeit erforderliche persönliche und fachliche Zuverlässigkeit besitzen,
4.  in geordneten finanziellen Verhältnissen leben und
5.  die für die Ausübung der Tätigkeit erforderlichen gesundheitlichen Voraussetzungen erfüllen.

(2) Bewerberinnen und Bewerber, die ihre Berufsqualifikation in einem anderen Mitgliedstaat der Europäischen Union oder einem Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum oder der Schweiz erworben haben, müssen über ausreichende Kenntnisse der deutschen Sprache verfügen, die für die Ausübung der Tätigkeit als bevollmächtigte Bezirksschornsteinfegerin oder bevollmächtigter Bezirksschornsteinfeger erforderlich sind.

(3) Die zuständige Behörde kann zur Prüfung der Zuverlässigkeit zusätzlich zu den Bewerbungsunterlagen nach § 4 Absatz 4 ein Führungszeugnis nach § 30 Absatz 5 des Bundeszentralregistergesetzes, eine Auskunft aus dem Gewerbezentralregister zur Vorlage bei der Behörde nach § 150 Absatz 5 der Gewerbeordnung und eine Bescheinigung in Steuersachen von den Bewerberinnen und Bewerbern abfordern.
Ist oder war die Bewerberin oder der Bewerber bereits Inhaber eines Bezirks, kann die zuständige Behörde eine Stellungnahme der für den Bezirk zuständigen Aufsichtsbehörde einholen.

### § 3  
Ausschreibung

(1) Die zuständige Behörde hat die Tätigkeit einer bevollmächtigten Bezirksschornsteinfegerin oder eines bevollmächtigten Bezirksschornsteinfegers für einen freien oder frei werdenden Bezirk im Internet unter www.bund.de auszuschreiben.

(2) Die Ausschreibung der Tätigkeit als bevollmächtigte Bezirksschornsteinfegerin oder bevollmächtigter Bezirksschonsteinfeger muss mindestens enthalten:

1.  eine Beschreibung der örtlichen Lage des ausgeschriebenen Bezirks und die Anzahl der kehr- und überprüfungspflichtigen Gebäude,
2.  den Vergabetermin,
3.  die Dauer der Bestellung unter Hinweis auf die zum Zeitpunkt der Ausschreibung gültige Altersgrenze,
4.  die Einreichungsfrist,
5.  einen Hinweis darauf, dass die Bewerberinnen und Bewerber die handwerksrechtlichen Voraussetzungen zur selbstständigen Ausübung des Schornsteinfegerhandwerks besitzen müssen,
6.  eine Aufzählung der von den Bewerberinnen und Bewerbern nach § 4 einzureichenden Bewerbungsunterlagen,
7.  einen Hinweis, dass die Auswahl zwischen den Bewerberinnen und Bewerbern nach ihrer Eignung, Befähigung und fachlichen Leistung vorgenommen wird,
8.  den Namen, die Anschrift, die Telefonnummer sowie die E-Mail-Adresse der zuständigen Behörde, bei der die Bewerbungsunterlagen einzureichen sind,
9.  einen Hinweis auf die Fundstelle dieser Verordnung,
10.  einen Hinweis auf § 9a Absatz 4 des Schornsteinfeger-Handwerksgesetzes und
11.  einen Hinweis auf die zu entrichtenden Gebühren.

(3) Die Ausschreibung erfolgt in der Regel vier Monate vor dem Zeitpunkt, an dem der Bezirk regelmäßig neu zu besetzen ist (Vergabetermin) oder unverzüglich, wenn aus anderen Gründen ein Bezirk neu zu besetzen ist.
Zu einem Vergabetermin können mehrere Bezirke ausgeschrieben werden.
Die zuständige Behörde kann auch das Statusamt einer bevollmächtigten Bezirksschornsteinfegerin oder eines bevollmächtigten Bezirksschornsteinfegers ausschreiben.
Die Frist für die Bewerbung und die Einsendung der Bewerbungsunterlagen nach § 4 endet drei Wochen nach dem Tag der Veröffentlichung der Ausschreibung (Einreichungsfrist).
Es gilt das Datum des Posteingangs (Posteingangsstempel) bei der zuständigen Behörde.

### § 4  
Bewerbung, Bewerbungsunterlagen

(1) Die Bewerbung für einen oder für mehrere Bezirke zu einem Vergabetermin ist bei der jeweils zuständigen Behörde in einer Ausfertigung schriftlich einzureichen und eigenhändig zu unterzeichnen.

(2) Werden zum selben Vergabetermin mehrere Bezirke ausgeschrieben, muss im Fall einer Mehrfachbewerbung die Bewerbung eine Rangfolge der beantragten Bezirke enthalten.

(3) Werden zu einem Vergabetermin mehrere Bezirke durch verschiedene zuständige Behörden ausgeschrieben, müssen im Fall der Mehrfachbewerbung bei verschiedenen Behörden

1.  gegenüber jeder Behörde eine identische Rangfolge der beantragten Bezirke und
2.  die für diese Bezirke zuständigen Bestellungsbehörden

angegeben werden.

(4) Die Bewerbung muss folgende Angaben und Unterlagen enthalten:

1.  den Familiennamen, die Vornamen, das Geburtsdatum, eine Anschrift und eine Telefonnummer sowie falls vorhanden eine E-Mail-Adresse,
2.  einen tabellarischen Lebenslauf, der lückenlose Angaben über die schulische und berufliche Vorbildung sowie den beruflichen Werdegang enthält und aus dem der Beginn sowie das Ende der jeweiligen Tätigkeiten auf den Tag genau hervorgehen,
3.  einen Nachweis über das Vorliegen der Voraussetzungen zur Eintragung in die Handwerksrolle: Zeugnisse mit Notenangaben über die Gesellenprüfung und die Meisterprüfung oder über jeweils gleichwertige Qualifikationen; im Fall einer in einem anderen Mitgliedstaat der Europäischen Union oder in einem Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum oder in der Schweiz erworbenen Berufsqualifikation die nach § 6 der EU/EWR-Handwerk-Verordnung vorzulegenden Unterlagen und Bescheinigungen,
4.  Nachweise über die bisherigen Schornsteinfegertätigkeiten in Form von Bestellungsurkunden, Arbeitsverträgen, Arbeitsbescheinigungen und Sozialversicherungsnachweisen der letzten zehn Jahre,
5.  Nachweise über
    1.  zusätzliche berufsbezogene Qualifikationen und Abschlüsse,
    2.  zusätzliche berufsbezogene Fort- und Weiterbildungsmaßnahmen der letzten sieben Jahre; die Nachweise müssen jeweils die bestätigte Angabe der Anzahl der Unterrichtsstunden, Datum, Beginn, Ende und Ort der Fort- und Weiterbildungsmaßnahme, den Namen des Referenten und die wesentlichen Inhalte der Fort- und Weiterbildungsmaßnahme enthalten sowie
    3.  gesetzlich vorgeschriebene beziehungsweise vorgesehene Zeiten während der letzten zehn Jahre, insbesondere Grundwehrdienstzeiten, Elternzeiten, Pflegezeiten und Zeiten der Berufsunfähigkeit, wobei maximal zwei Jahre anerkannt werden,
6.  eine unterzeichnete Eigenerklärung, dass die Bewerberin oder der Bewerber die erforderlichen gesundheitlichen Voraussetzungen für die Ausübung der Tätigkeit als bevollmächtigte Bezirksschornsteinfegerin oder als bevollmächtigter Bezirksschornsteinfeger erfüllt,
7.  eine unterzeichnete Eigenerklärung, dass die Bewerberin oder der Bewerber in geordneten finanziellen Verhältnissen lebt,
8.  eine unterzeichnete Eigenerklärung darüber, ob innerhalb der letzten zwölf Monate gegen die Bewerberin oder den Bewerber strafgerichtliche Verurteilungen ergangen sind, ein gerichtliches Strafverfahren anhängig ist oder ein anhängiges Ermittlungsverfahren bekannt ist,
9.  eine unterzeichnete Eigenerklärung der Bewerberinnen oder Bewerber, die ihre Berufsqualifikation in einem anderen Mitgliedstaat der Europäischen Union oder einem Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum oder der Schweiz erworben haben, dass sie über ausreichende Kenntnisse der deutschen Sprache verfügen, die für die Ausübung der Tätigkeit als bevollmächtigte Bezirksschornsteinfegerin oder bevollmächtigter Bezirksschornsteinfeger erforderlich sind,
10.  eine unterzeichnete Eigenerklärung darüber, ob eine dieser Bewerbung vorangegangene Bestellung innerhalb der letzten zehn Jahre vor Beginn der Ausschreibung gemäß § 12 Absatz 1 Nummer 2 des Schornsteinfeger-Handwerksgesetzes aufgehoben, gemäß § 11 Absatz 2 des Schornsteinfegergesetzes widerrufen oder gemäß § 11 Absatz 1 des Schornsteinfegergesetzes zurückgenommen wurde oder ob andere Aufsichtsmaßnahmen im Sinne von § 21 Absatz 3 des Schornsteinfeger-Handwerksgesetzes ergriffen wurden; anzugeben sind jeweils die seinerzeit zuständige Behörde, die genauen Maßnahmen sowie das Aktenzeichen des Verfahrens und
11.  in Fällen, in denen die Bewerberin oder der Bewerber bereits Inhaberin oder Inhaber eines Bezirks außerhalb des Landes Brandenburg ist, den Namen, die Anschrift und die Telefonnummer der für diesen Bezirk zuständigen Aufsichtsbehörde.

(5) Die Bewerbungsunterlagen nach Absatz 4 Nummer 3 bis 5 können der zuständigen Behörde als Kopie eingereicht werden.
Eine Beglaubigung ist nicht erforderlich.
Die Bewerbungsunterlagen nach Absatz 4 Nummer 6 bis 10 dürfen bei ihrer Vorlage nicht älter als drei Monate sein.
Den Bewerbungsunterlagen, die nicht in deutscher Sprache abgefasst sind, ist eine deutsche Übersetzung von einem öffentlich bestellten oder beeidigten Dolmetscher oder Übersetzer beizulegen.
Nachweise nach Absatz 4 Nummer 5b ohne bestätigte Angabe der Anzahl der Unterrichtsstunden werden nur als halbtägige Veranstaltungen anerkannt.

(6) Im Fall fehlender, unvollständiger, veralteter oder nicht fristgemäß eingereichter Bewerbungsunterlagen sowie fehlender deutscher Übersetzungen kann die zuständige Behörde die Vorlage der entsprechenden Unterlagen unter erneuter Fristsetzung nachfordern, wenn hierdurch der Ablauf des Auswahlverfahrens und insbesondere die fristgemäße Bestellung nicht gefährdet werden.

(7) Versuchen Bewerberinnen oder Bewerber sich durch arglistige Täuschung im Auswahlverfahren einen Vorteil zu verschaffen, werden sie von diesem Verfahren ausgeschlossen.

### § 5  
Auswahl

(1) Die Auswahl zwischen den Bewerberinnen und Bewerbern ist nach Eignung, Befähigung und fachlicher Leistung vorzunehmen.

(2) Die Auswahl erfolgt insbesondere auf der Grundlage der nach § 4 eingereichten Bewerbungsunterlagen anhand der in Anlage 2 festgelegten Bewertungskriterien.

(3) Werden zu einem Vergabetermin mehrere Bezirke durch verschiedene zuständige Behörden ausgeschrieben, werden die Bewertungspunkte von der Behörde berechnet, in deren Zuständigkeitsbereich der als erstrangig angegebene Bezirk liegt.
Diese Behörde übermittelt die Bewertungspunkte an diejenigen Behörden zur eigenen Prüfung, bei denen nachrangige Bewerbungen derselben Bewerberin oder desselben Bewerbers vorliegen.

(4) Ist auf der Grundlage der Bewertungspunkte bei Punktegleichstand (0 bis 1 Punkt) keine Entscheidung über die Vergabe des Bezirks möglich, erfolgt die Entscheidung auf Grund der Auswertung vergleichbarer Stellungnahmen nach § 2 Absatz 3 Satz 2 oder vergleichbarer Kehrbuch- oder Bezirksüberprüfungen oder auf Grund von Bewerbungsgesprächen.
Die in diesem Zusammenhang den Bewerberinnen und Bewerbern entstehenden Kosten werden nicht erstattet.

(5) Die Auswahlentscheidung ist von der zuständigen Behörde zu treffen.

(6) Die zuständige Behörde kann vor ihrer Auswahlentscheidung sachkundige Personen unter Beachtung datenschutzrechtlicher Bestimmungen anhören.
Als sachkundige Person scheidet aus, wer nach Maßgabe des § 20 des Verwaltungsverfahrensgesetzes für eine Behörde nicht tätig werden darf.
Die sachkundige Person darf weder an der betreffenden Ausschreibung noch an der Auswahlentscheidung beteiligt sein.

(7) Versucht eine Bewerberin oder ein Bewerber sich durch direkte oder indirekte Beeinflussung einer Person nach Absatz 6 einen Vorteil im Auswahlverfahren zu verschaffen, gilt § 4 Absatz 7 entsprechend.

(8) Das Auswahlverfahren ist durch die zuständige Behörde in geeigneter Form zu dokumentieren.

### § 6  
Verfahren nach der Auswahlentscheidung

(1) Nach Beendigung der Auswahl nach § 5 sind zwischen denjenigen Behörden, denen Bewerbungen derselben Bewerberinnen und Bewerber vorliegen, die jeweiligen Verfahren zweckmäßig zu koordinieren.
Dies betrifft insbesondere die Fälle, in denen jeweils dieselben Bewerberinnen und Bewerber als am besten geeignet ermittelt wurden.

(2) Nach der Koordinierung nach Absatz 1 benachrichtigt die zuständige Behörde unverzüglich die ausgewählte Bewerberin oder den ausgewählten Bewerber, setzt dabei eine angemessene Frist zur schriftlichen Erklärung über die Annahme oder Ablehnung der vorgesehenen Bestellung und informiert über die Möglichkeit der Rücknahme von weiteren Bewerbungen.
Wird die Erklärung über die Annahme auch auf Nachfrage nicht abgegeben, gilt dies als Ablehnung der vorgesehenen Bestellung.

(3) Im Fall der Ablehnung oder des Scheiterns der Bestellung der ausgewählten Person aus ihr zu zurechnenden Gründen wird die jeweils nächste geeignete Bewerberin oder der jeweils nächste geeignete Bewerber durch die zuständige Behörde benachrichtigt.
Die Absätze 1 und 2 gelten entsprechend.

(4) Nach Eingang der Erklärung über die Annahme sendet die zuständige Behörde den nicht ausgewählten Bewerberinnen oder Bewerbern einen Ablehnungsbescheid, wenn von der Möglichkeit der Rücknahme von Bewerbungen kein Gebrauch gemacht wurde.
Nach Ablauf der Rechtsbehelfsfrist bestellt die Behörde die ausgewählte Bewerberin oder den ausgewählten Bewerber für den ausgeschriebenen Bezirk.
Ist die Bewerberin oder der Bewerber bereits Inhaber eines Bezirks, muss zum Vergabetermin die Aufhebung der bisherigen Bestellung erfolgt sein.

### § 7  
Bestellung

(1) Die zuständige Behörde fordert von der nach Nummer 7 der Anlage zu § 1 Absatz 1 und 2 der Schornsteinfegerzuständigkeitsverordnung zuständigen Behörde die Auflistung des Bezirks ab.

(2) Am Tag der Bestellung sind durch die ausgewählte Bewerberin oder den ausgewählten Bewerber alle Bewerbungsunterlagen im Original der zuständigen Behörde vorzulegen und von dieser mit den eingereichten Kopien zu vergleichen.

(3) Am Tag der Bestellung müssen die ausgewählte Bewerberin oder der ausgewählte Bewerber außerdem ein Führungszeugnis und eine Auskunft aus dem Gewerbezentralregister vorlegen.
Bei Bewerberinnen oder Bewerbern, die ihre Berufsqualifikation in einem anderen Mitgliedstaat der Europäischen Union oder einem Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum oder der Schweiz erworben haben, ist eine Bescheinigung der zuständigen Stelle des Herkunftsstaates, dass die Ausübung des Gewerbes nicht wegen Unzuverlässigkeit untersagt worden ist, vorzulegen.
Die Unterlagen dürfen nicht älter als drei Monate sein.

(4) Wird im Herkunftsstaat eine solche Bescheinigung nicht ausgestellt, kann sie durch eine Versicherung an Eides statt oder in Staaten, in denen es eine solche nicht gibt, durch eine vergleichbare Erklärung ersetzt werden, die die Bewerberin oder der Bewerber vor einer zuständigen Behörde, einem Notar oder einer entsprechend bevollmächtigten Berufsorganisation des Herkunftsstaates abgegeben hat und die durch diese Stelle bescheinigt wurde.

(5) Das persönliche Erscheinen zur Bestellung ist erforderlich, da die Bewerberin oder der Bewerber

1.  die Erklärung nach der Anlage 1 zu dieser Verordnung unterzeichnen muss,
2.  auf die gewissenhafte Ausübung der Tätigkeit als bevollmächtigte Bezirksschornsteinfegerin oder bevollmächtigter Bezirksschornsteinfeger per Handschlag verpflichtet wird,
3.  die Bestellungsurkunde und
4.  die Zuweisung zu dem Bezirk sowie die Auflistung des Bezirks erhält.

(6) Die zuständige Behörde hat über die erfolgte Bestellung

1.  die zuständige Handwerkskammer zur Eintragung in das Schornsteinfegerregister und
2.  die jeweilige Schornsteinfegerinnung sowie den gewerkschaftlichen Fachverband zu informieren.

(7) Die Bestellung ist öffentlich bekannt zu machen.

### § 8  
_(aufgehoben)_

### § 9  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 25.
Februar 2014

Der Minister für Wirtschaft und Europaangelegenheiten

Ralf Christoffers

* * *

### Anlagen

1

[Anlage 1 (zu § 7 Absatz 5 Nummer 1) - Erklärung](/br2/sixcms/media.php/68/BbgBAAV-Anlage%201.pdf "Anlage 1 (zu § 7 Absatz 5 Nummer 1) - Erklärung") 278.0 KB

2

[Anlage 2 (zu § 5 Absatz 2) - Bewertungsformular](/br2/sixcms/media.php/68/BbgBAAV-Anlage%202.pdf "Anlage 2 (zu § 5 Absatz 2) - Bewertungsformular") 391.9 KB