## Verordnung zur Durchführung des Weinrechts im Land Brandenburg (Weinrechtsdurchführungsverordnung - WeinRDV)

Auf Grund

*   des § 3 Absatz 4, des § 6 Absatz 5, des § 8a Absatz 1 und 3, des § 8c, des § 9 Absatz 2, des § 12 Absatz 3 Nummer 3 und Absatz 5, des § 17 Absatz 3 und 4, des § 20 Absatz 6, des § 22 Absatz 3, des § 23 Absatz 4, des § 24 Absatz 5 und des § 44 Absatz 1 des Weingesetzes in der Fassung der Bekanntmachung vom 18.
    Januar 2011 (BGBl.
    I S.
    66),
*   des § 5 Absatz 1 Satz 4 und des § 32c Absatz 2 der Weinverordnung in der Fassung der Bekanntmachung vom  21.
    April 2009 (BGBl.
    I S.
    827),
*   des § 11 Absatz 1 Satz 2, des § 12 Absatz 2, des § 13 Absatz 2 Satz 2, des § 14 Absatz 1, des § 23, des § 29 Absatz 3, des § 30 Absatz 2 und 3 und des § 31 der Wein-Überwachungsverordnung in der Fassung der Bekanntmachung vom 14. Mai 2002 (BGBl.
    I S.
    1624), von denen § 12 Absatz 2, § 23, § 29 Absatz 3 und § 30 Absatz 2 und 3 durch Artikel 1 der Verordnung vom 6.
    Dezember 2010 (BGBl.
    I S.
    1828) geändert worden sind, in Verbindung mit § 1 der Verordnung zur Übertragung von Ermächtigungen zum Erlass von Rechtsverordnungen nach dem Weinrecht vom 22.
    Januar 1996 (GVBl.
    II S.
    74), der durch Artikel 14 des Gesetzes vom 15.
    Juli 2010 (GVBl.
    I Nr.
    28) geändert worden ist,

verordnet der Minister für Infrastruktur und Landwirtschaft:

### § 1  
Bestimmte Weinanbaugebiete, Landweingebiete  
(zu § 3 Absatz 4 des Weingesetzes)

(1) Der im Land Brandenburg belegene Teil des bestimmten Anbaugebietes Saale-Unstrut umfasst die zulässigerweise mit Reben bepflanzten oder vorübergehend nicht bepflanzten sowie sonstige nicht mit Reben bepflanzten Flächen in den in Anlage 1 aufgeführten Städten und Gemeinden.

(2) Der im Land Brandenburg belegene Teil des bestimmten Anbaugebietes Sachsen umfasst die zulässigerweise mit Reben bepflanzten oder vorübergehend nicht bepflanzten sowie sonstige nicht mit Reben bepflanzten Flächen in den in Anlage 2 aufgeführten Städten und Gemeinden.

(3) Der im Land Brandenburg belegene Teil des Landweingebietes Sachsen umfasst die zulässigerweise mit Reben bepflanzten oder vorübergehend nicht bepflanzten sowie sonstige nicht mit Reben bepflanzten Flächen in den in Anlage 3 aufgeführten Städten und Gemeinden.

(4) Der im Land Brandenburg belegene Teil des Landweingebietes Brandenburg umfasst die zulässigerweise mit Reben bepflanzten oder vorübergehend nicht bepflanzten sowie sonstige nicht mit Reben bepflanzten Flächen in den in Anlage 4 aufgeführten Landkreisen, Städten und Gemeinden.

### § 2  
Wiederbepflanzung  
(zu § 6 Absatz 2 und 6 des Weingesetzes)

(1) Die zuständige Behörde kann Erzeugern, die sich verpflichtet haben, eine Rebfläche zu roden, auf Antrag genehmigen, die Wiederbepflanzung auf einer anderen als der zu rodenden Fläche vorzunehmen, soweit die Rodung spätestens bis zum Ablauf des vierten Jahres, gerechnet ab dem Zeitpunkt der Anpflanzung der neuen Reben, vorgenommen wird.
Der Erzeuger meldet die erfolgte Rodung innerhalb von vier Wochen an die zuständige Behörde.

(2) Stimmt die wieder zu bepflanzende Fläche mit der gerodeten Fläche überein und wurde kein Antrag nach Absatz 1 gestellt, gilt die Wiederbepflanzung als genehmigt, wenn die Rodung spätestens am Ende des Weinwirtschaftsjahres, in dem die Rodung erfolgt ist, der zuständigen Stelle gemeldet wird und die Wiederbepflanzung innerhalb von drei Jahren ab dem Zeitpunkt der Rodung erfolgt.

### § 3  
Umwandlung bestehender Pflanzungsrechte  
(zu § 6a Absatz 2 des Weingesetzes)

Die zuständige Behörde kann Antragstellern genehmigen, ein umgewandeltes Pflanzrecht auf einer im Antrag nicht bezeichneten Fläche auszuüben, soweit diese Fläche im Betrieb des Antragstellers belegen ist.

### § 4  
Zugelassene Rebsorten  
(zu § 8 Absatz 1 und 2 des Weingesetzes)

(1) Zur Herstellung von Wein sind die in Anlage 5 sowie die in der jeweils geltenden Liste zum Sortenregister des Bundessortenamtes aufgeführten Rebsorten zugelassen.

(2) Versuchsanlagen zur Prüfung der Voraussetzungen für die Festlegung der zur Herstellung von Wein zugelassenen Rebsorten dürfen nur mit Genehmigung der zuständigen Behörde angelegt werden.

(3) Je Versuchsansteller soll nicht mehr als ein Anbaueignungsversuch mit der gleichen Rebsorte genehmigt werden.
Je Anbaueignungsversuch beträgt die Versuchsfläche höchstens einen Hektar und es sind mindestens 300 Rebstöcke der zu prüfenden Rebsorte anzupflanzen.

### § 5  
Zulässiger Hektarertrag, Abgabe von Übermengen bei Betrieben ohne eigene Kellerwirtschaft  
(zu § 9 Absatz 2 und § 12 Absatz 3 Nummer 3 und Absatz 5 des Weingesetzes)

(1) Der zulässige Hektarertrag für Weine, die auf Rebflächen der zum Land Brandenburg gehörenden Teile des bestimmten Anbaugebietes Saale-Unstrut erzeugt werden, wird auf 90 Hektoliter je Hektar Ertragsrebfläche festgesetzt.

(2) Der zulässige Hektarertrag für Weine, die auf Rebflächen der zum Land Brandenburg gehörenden Teile des bestimmten Anbaugebietes Sachsen sowie der zum Land Brandenburg gehörenden Teile des Landweingebietes Sächsischer Landwein erzeugt werden, wird auf 80 Hektoliter je Hektar Ertragsrebfläche festgesetzt.

(3) Der zulässige Hektarertrag für Weine, die auf Rebflächen der zum Land Brandenburg gehörenden Teile des Landweingebietes Brandenburger Landwein erzeugt werden, wird auf 90 Hektoliter je Hektar Ertragsrebfläche festgesetzt.

(4) Weinbaubetriebe, die die gesamte Ernte als Weintrauben oder Traubenmost an andere abgeben und nicht über eigene betriebliche Verarbeitungsmöglichkeiten verfügen, dürfen Mengen, die den Gesamthektarertrag übersteigen, an andere abgeben.
Bei der Abgabe nach Satz 1 ist die Ertragsrebfläche, auf der die Weintrauben geerntet worden sind, dem abnehmenden Betrieb schriftlich mitzuteilen.
Der Nachweis der Abgabe von Übermengen ist drei Jahre aufzubewahren und der zuständigen Behörde auf Verlangen vorzulegen.

### § 6   
Qualitätswein bestimmter Anbaugebiete (b.
A.)  
(zu § 17 Absatz 3 und § 20 Absatz 6 des Weingesetzes)

(1) Die Bewässerung von Rebflächen und die Beregnung zum Frostschutz sind zulässig, wenn die Umweltbedingungen dies rechtfertigen.

(2) Die natürlichen Mindestalkoholgehalte für Qualitätswein, Prädikatswein, Qualitätslikörwein b.
A., Qualitätsperlwein b.
A.
und Sekt b.
A.
für den zu Brandenburg gehörenden Teil des bestimmten Anbaugebietes Saale-Unstrut sind in der Anlage 6 festgesetzt.

(3) Die natürlichen Mindestalkoholgehalte für Qualitätswein, Prädikatswein, Qualitätslikörwein b.
A., Qualitätsperlwein b.
A.
und Sekt b.
A.
für den zu Brandenburg gehörenden Teil des bestimmten Anbaugebietes Sachsen sind in der Anlage 7 festgesetzt.

(4) Für die Zuerkennung des Prädikates „Eiswein“ muss das Erntegut von Hand gelesen worden sein.

(5) Zur Herstellung von Qualitätswein, Prädikatswein, Qualitätslikörwein b.
A., Qualitätsperlwein b.
A.
und Sekt b.
A.
sind die in § 4 Absatz 1 genannten Rebsorten geeignet.

### § 7   
Landwein  
(zu § 22 Absatz 3 Nummer 2 und 3 des Weingesetzes)

(1) Der natürliche Mindestalkoholgehalt wird für die zu Brandenburg gehörenden Teile des Landweingebietes Brandenburger Landwein auf 5,9 Volumenprozent Alkohol (50° Öchsle) und für die zu Brandenburg gehörenden Teile des Landweingebietes Sächsischer Landwein auf 5,9 Volumenprozent Alkohol (50° Öchsle) festgesetzt.

(2) Die Kontrolle der Produktspezifikationen von Landwein wird von der zuständigen Behörde auf Basis der Angaben der Erzeuger aus

1.  der Erntemeldung nach Artikel 8,
2.  der Erzeugungsmeldung nach Artikel 9,
3.  der Bestandsmeldung nach Artikel 11,
4.  den Begleitdokumenten nach Titel III

der Verordnung (EG) Nr.
436/2009 der Kommission vom 26.
Mai 2009 mit Durchführungsbestimmungen zur Verordnung (EG) Nr.
479/2008 des Rates hinsichtlich der Weinbaukartei, der obligatorischen Meldungen und der Sammlung von Informationen zur Überwachung des Marktes, der Begleitdokumente für die Beförderung von Weinbauerzeugnissen und der Ein- und Ausgangsbücher im Weinsektor (ABl.
Nr.
L 128 vom 27.05.2009, S.
15) in der jeweils geltenden Fassung.

(3) Die Abfüllung von Landweinen in Verkaufsverpackungen ist der zuständigen Behörde innerhalb von sieben Arbeitstagen unter Vorlage eines Untersuchungsbefundes mit den in Anlage 10 der Weinverordnung genannten Angaben anzuzeigen.

(4) Zur Herstellung von Landwein sind die in § 4 Absatz 1 genannten Rebsorten geeignet.

### § 8  
Eintragung von geografischen Bezeichnungen in die Weinbergsrolle  
(zu § 23 Absatz 4 des Weingesetzes)

(1) Bei der zuständigen Behörde wird eine Weinbergsrolle geführt.
Neueintragungen, Änderungen oder Löschungen werden auf Antrag vorgenommen.
Antragsberechtigt sind Eigentümer und Nutzungsberechtigte von Rebflächen.

(2) Der Antrag muss

1.  den einzutragenden Lagenamen und die Angabe, ob es sich um einen in das Liegenschaftskataster eingetragenen Namen handelt oder ob er sich an einen solchen Namen anlehnt, und
2.  Angaben über die Größe und die Abgrenzung der Lage durch Einzeichnung in eine Flurkarte, aus der die Flurstücke mit den Flurstücksnummern ersichtlich sind,

enthalten.

(3) Ist der Antrag im Sinne der Weinverordnung begründet, trägt die zuständige Behörde den Namen der Lage unter Beifügung des Antrags und der Flurkarte in die Weinbergsrolle ein.

(4) Die Absätze 2 und 3 gelten entsprechend für Anträge auf Änderung oder Löschung bereits eingetragener Lagen.

(5) Die Eintragung einer Lage ist von Amts wegen zu löschen, wenn der Name zum letzten Mal für einen Wein oder Ausgangsstoff verwendet wurde, der vor mehr als fünf Jahren in der Lage gewonnen wurde.

### § 9  
Anerkannte Erzeuger nach der Verordnung (EG) Nr.
607/2009  
(zu § 24 Absatz 5 des Weingesetzes)

(1) Als anerkannte Erzeuger im Sinne von Artikel 63 Absatz 4 der Verordnung (EG) Nr.
607/2009 der Kommission vom 14.
Juli 2009 mit Durchführungsbestimmungen zur Verordnung (EG) Nr.
479/2008 des Rates hinsichtlich der geschützten Ursprungsbezeichnungen und geografischen Angaben, der traditionellen Begriffe sowie der Kennzeichnung und Aufmachung bestimmter Weinbauerzeugnisse (ABl.
L 193 vom 24.07.2009, S.
60) in der jeweils geltenden Fassung gelten Betriebe, denen auf Antrag eine Betriebsnummer von der zuständigen Behörde zugeteilt wurde.

(2) Die zuständige Behörde ist befugt, zum Zwecke der Durchführung des Zertifizierungs-, Genehmigungs- und Kontrollverfahrens für Weine mit der Angabe einer oder mehrerer Rebsorten oder der Angabe des Erntejahres nach der Verordnung (EU) Nr. 1308/2013 des Europäischen Parlaments und des Rates vom 17. Dezember 2013 über eine gemeinsame Marktorganisation für landwirtschaftliche Erzeugnisse und zur Aufhebung der Verordnungen (EWG) Nr. 922/72, (EWG) Nr. 234/79, (EG) Nr. 1037/2001 und (EG) Nr. 1234/2007 (ABl.
L 347 vom 20.12.2013, S. 671) in der jeweils geltenden Fassung die in § 7 Absatz 2 genannten Meldungen und Dokumente zu verwenden.

(3) Die Abfüllung von Weinen mit Jahrgangs- oder Rebsortenangaben in Verkaufsverpackungen ist der zuständigen Behörde innerhalb von sieben Arbeitstagen unter Vorlage eines Untersuchungsbefundes mit den in Anlage 10 der Weinverordnung genannten Angaben anzuzeigen.

### § 10  
Erhebung der Abgabe für den Deutschen Weinfonds  
(zu § 44 Absatz 1 des Weingesetzes)

(1) Die Abgabe nach § 43 Absatz 1 Nummer 1 des Weingesetzes wird durch die zuständige Behörde erhoben.
Sie entsteht am 1.
Januar eines jeden Jahres.

(2) Die Abgabe wird jährlich erhoben und ist jeweils am 30.
Juni eines jeden Jahres fällig.

(3) Die zuständige Behörde setzt auf der Grundlage der Daten der Weinbaukartei die Höhe der Abgabe fest und teilt diese dem Abgabepflichtigen mit.
Zur abgabepflichtigen Fläche gehören alle bestockten und vorübergehend nicht bestockten Flächen der Abgabepflichtigen.

(4) Die Abgabepflichtigen sind zur Mitwirkung bei der Ermittlung des Sachverhaltes verpflichtet.

### § 11  
Auszeichnungen  
(zu § 24 Absatz 4 Nummer 1 des Weingesetzes)

(1) Die zuständige Behörde kann auf Antrag Auszeichnungen und ähnliche Angaben anerkennen, wenn diese in einem Wettbewerb entsprechend § 30 Absatz 1 bis 3 und 5 der Weinverordnung vergeben werden.
Der Antragsteller legt mit dem Antrag eine Satzung zur Durchführung des Wettbewerbs zur Anerkennung vor.

(2) Soweit es sich um Auszeichnungen und ähnliche Angaben für Erzeugnisse der im Land Brandenburg belegenen Teile der bestimmten Anbaugebiete Saale-Unstrut und Sachsen handelt, ergeht eine Anerkennung nach Absatz 1 durch die zuständige Behörde nur im Einvernehmen mit den Anbauverbänden dieser Anbaugebiete.

### § 12  
(zu § 32c Absatz 2 der Weinverordnung)

(1)  Für die Herstellung von Wein mit der Bezeichnung „Classic“ werden für den

1.  zu Brandenburg gehörenden Teil des bestimmten Anbaugebietes Saale-Unstrut die Rebsorten Weißer Burgunder, Kerner, Müller-Thurgau, Blauer Portugieser, Weißer Riesling, Grüner Silvaner, Blauer Spätburgunder, Traminer,
2.  zu Brandenburg gehörenden Teil des bestimmten Anbaugebietes Sachsen die Rebsorten Weißer Burgunder, Weißer Riesling, Ruländer, Blauer Spätburgunder, Traminer

festgelegt.

(2) Für die Herstellung von Wein mit der Bezeichnung „Selection“ werden für den

1.  zu Brandenburg gehörenden Teil des bestimmten Anbaugebietes Saale-Unstrut die Rebsorten Weißer Burgunder, Weißer Riesling, Grüner Silvaner, Blauer Spätburgunder,
2.  zu Brandenburg gehörenden Teil des bestimmten Anbaugebietes Sachsen die Rebsorten Weißer Burgunder, Weißer Riesling, Ruländer, Blauer Spätburgunder, Traminer

festgelegt.

(3) Synonyme der Rebsorten können zur Bezeichnung verwendet werden.
Die synonymen Bezeichnungen ergeben sich aus der Anlage 5 Spalte 3.

### § 13  
Vereinfachte Weinbuchführung  
(zu § 11 Absatz 1 Satz 2 der Wein-Überwachungsverordnung)

§ 11 Absatz 1 Satz 1 der Wein-Überwachungsverordnung gilt unter den dort genannten Voraussetzungen auch für selbst erzeugten Traubenmost und Wein.

### § 14  
Moderne Weinbuchführung  
(zu § 12 Absatz 2 der Wein-Überwachungsverordnung)

(1) Der Anwender von Buchführungsverfahren auf der Grundlage der automatisierten Datenverarbeitung ist verpflichtet, der zuständigen Behörde innerhalb eines Monats das Verfahren anzuzeigen.
Der Anwender hat der zuständigen Behörde oder den von ihr beauftragten Personen die Prüfung des angewendeten Buchführungsverfahrens zu ermöglichen.
Er hat die erforderlichen Unterlagen bereitzuhalten und die notwendigen Auskünfte zu erteilen.
Im begründeten Einzelfall kann die zuständige Behörde dem Anwender die Anwendung eines Buchführungsverfahrens untersagen oder von der Erfüllung weiterer Auflagen abhängig machen.

(2) Werden die Genehmigungsvoraussetzungen für Buchführungsverfahren auf der Grundlage der automatisierten Datenverarbeitung geändert oder weitere Auflagen erteilt, so sind die Maßnahmen, die zur Erfüllung der Genehmigungsvoraussetzungen oder der weiteren Auflagen notwendig sind, unverzüglich zu ergreifen.
Sind die geänderten Genehmigungsvoraussetzungen oder die weiteren Auflagen nach Ablauf von drei Monaten nicht erfüllt, so kann der Anwender verpflichtet werden, zusätzliche Aufzeichnungen anzufertigen.

(3) Werden die Genehmigungsverfahren für Buchführungsverfahren geändert, so kann der Anwender dieser Buchführungsverfahren die in seinem Besitz befindlichen Bücher und Formulare bis zur Erschöpfung der Bestände verwenden, wenn er die geänderten Genehmigungsvoraussetzungen erfüllt.

### § 15  
Analysenbuchführung  
(zu § 13 Absatz 2 Satz 2 der Wein-Überwachungsverordnung)

Eine Analysenbuchführung auf der Grundlage automatisierter Datenverarbeitung wird auf Antrag des Anwenders von der zuständigen Behörde genehmigt, wenn das Buchführungsverfahren die Anforderungen, die allgemein an eine Buchführung gestellt werden, erfüllt.

### § 16  
Herbstbuch  
(zu § 14 Absatz 1 der Wein-Überwachungsverordnung)

Traubenerzeuger haben die Feststellungen nach § 14 Absatz 1 der Wein-Überwachungsverordnung in ein durchnummeriertes Herbstbuch nach dem Muster der Anlage 8 einzutragen, sofern keine andere Form der Buchführung genehmigt wird.
Die Eintragungen im Herbstbuch sind fünf Jahre aufzubewahren.
Die Aufbewahrungsfrist beginnt mit dem Ablauf des Kalenderjahres, in dem die letzte Eintragung vorgenommen worden ist.

### § 17  
Zusätzliche Angaben in Begleitpapieren  
(zu § 23 der Wein-Überwachungsverordnung)

(1) Ist für die Beförderung von

1.  im Land Brandenburg geernteten Weintrauben oder
2.  nicht abgefülltem Traubenmost, nicht abgefülltem Tafelwein, nicht abgefüllten Erzeugnissen, die für die Herstellung von Schaumwein, Qualitätsschaumwein oder Qualitätsschaumwein b.
    A.
    bestimmt sind, oder nicht abgefülltem Qualitätswein b.
    A., der aus im Land Brandenburg geernteten Weintrauben gewonnen worden ist,

ein Begleitpapier auszustellen, so hat die zur Ausstellung des Begleitpapiers verpflichtete Person in dem Begleitpapier auch die jeweilige Lieferschein- oder Rechnungsnummer anzugeben.

(2) Für die Begleitpapiere sind die von der zuständigen Behörde dafür ausgegebenen Vordrucke zu verwenden.

(3) Die zur Ausstellung des Begleitpapiers verpflichtete Person hat unverzüglich eine Kopie des Begleitpapiers der für den Verladeort zuständigen Behörde zuzuleiten.

### § 18  
Meldungen über Rebflächen und Erntemengen  
(zu § 29 Absatz 3 und § 31 der Wein-Überwachungsverordnung)

(1) Nutzungsberechtigte von Rebflächen erstatten der zuständigen Behörde die Meldung über die Ertragsrebfläche, die Rebsorten und die Stockanzahl sowie über die vorgenommenen Aufgaben, Rodungen, Wiederbepflanzungen, Neuanpflanzungen und die Änderungen von Eigentums- und Pachtverhältnissen der Rebflächen zum Stichtag 31.
Mai auf den dafür ausgegebenen Vordrucken.
Die Meldung muss spätestens am darauf folgenden 10.
Juni bei der zuständigen Behörde eingegangen sein.

(2) Traubenerzeuger erstatten der zuständigen Behörde die Meldung über die Erntemenge, differenziert nach Rebsorte, Herkunft sowie deren vorgesehenen Ausbau in Weine, Landweine, Qualitätsweine und Prädikatsweine nach Abschluss der Ernte auf den dafür ausgegebenen Vordrucken.
Die Meldung muss spätestens am 31.
Dezember bei der zuständigen Behörde eingegangen sein.

(3) Weinbaubetriebe im Sinne des § 9 Absatz 1 Satz 1 des Weingesetzes, bei denen die zuständige Behörde anhand der Rebflächenangaben in der gemeinschaftlichen Weinbaukartei und der Mengenangaben in der Ernte- und Erzeugungsmeldung Übermengen ermittelt und dies den betroffenen Betrieben mitgeteilt hat, haben jeweils zum 31.
Juli der zuständigen Behörde auf den dafür ausgegebenen Vordrucken eine Meldung über die jeweils bis zum 31.
Juli verwendete und verwertete Übermenge zu erstatten.

(4) Weinbaubetriebe erstatten der zuständigen Behörde die Meldung über den Bestand an Traubenmost, konzentriertem Traubenmost, rektifiziertem Traubenmost und Wein, differenziert nach Rebsorte, Herkunft und Qualitätsstufe sowie deren Lagerort.
Die Meldung erfolgt jährlich zum Stichtag 31.
Juli auf den dafür ausgegebenen Vordrucken, sie muss spätestens am darauf folgenden 7.
August bei der zuständigen Behörde eingegangen sein.

### § 19  
Meldungen über önologische Verfahren  
(zu § 30 Absatz 2 und 3 der Wein-Überwachungsverordnung)

(1) Die Weinerzeuger melden der zuständigen Behörde

1.  den Besitz an konzentriertem Traubenmost oder rektifiziertem Traubenmostkonzentrat nach Anhang VIII Abschnitt D Nummer 4 der Verordnung (EU) Nr. 1308/2013,
2.  die Erhöhung des Alkoholgehaltes nach Artikel 12 Absatz 1 der Verordnung (EG) Nr. 606/2009 der Kommission vom 10. Juli 2009 mit Durchführungsbestimmungen zur Verordnung (EG) Nr. 479/2008 des Rates hinsichtlich der Weinbauerzeugniskategorien, der önologischen Verfahren und der diesbezüglichen Einschränkungen (ABl.
    L 193 vom 24.07.2009, S. 1) in der jeweils geltenden Fassung mindestens zwei Tage vor Beginn der Maßnahme,
3.  die Säuerung oder die Entsäuerung nach Artikel 13 Absatz 1 der Verordnung (EG) Nr. 606/2009 spätestens am zweiten Tag nach Durchführung der in einem Wirtschaftsjahr durchgeführten ersten Maßnahme für alle auf das betreffende Wirtschaftsjahr entfallenden Maßnahmen und
4.  die Süßung nach Anhang I D Nummer 5 der Verordnung (EG) Nr. 606/2009 mindestens 48 Stunden vor dem Tag der Vornahme der Arbeiten der Süßung.

(2) Es wird zugelassen, dass

1.  die Meldung nach Absatz 1 Nummer 2 durch eine für den Zeitraum vom Beginn des Weinjahres bis zum folgenden 15.
    März geltende vorherige Meldung,
2.  die Meldung nach Absatz 1 Nummer 4 durch eine für den Zeitraum des gesamten Weinjahres geltende vorherige Meldung erstattet wird.

(3) Die Meldungen nach Absatz 1 Nummer 1 und 3 sind jährlich zum 1.
September der zuständigen Behörde auf den dafür ausgegebenen Vordrucken zu erstatten.

(4) Die in Artikel 43 der Verordnung (EG) Nr.
436/2009 genannten Erzeugnisse und Stoffe sowie die önologischen Verfahren nach Absatz 1 Nummer 2 bis 4 sind in den Ein- und Ausgangsbüchern nachzuweisen.
Soweit ein Begleitdokument nach der Verordnung (EG) Nr.
436/2009 auszustellen ist, muss dieses einen Hinweis auf die önologischen Verfahren nach Absatz 1 Nummer 2 bis 4 enthalten.

### § 20  
Bußgeldvorschriften

Ordnungswidrig im Sinne des § 50 Absatz 2 Nummer 4 des Weingesetzes in Verbindung mit § 40 der Wein-Überwachungsverordnung handelt, wer vorsätzlich oder fahrlässig entgegen § 19 Absatz 1 und 3 die Meldungen nicht, nicht fristgerecht oder nicht vollständig erstattet.

### § 21  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung zur Durchführung des Weinrechts im Land Brandenburg vom 19.
Juni 2006 (GVBl.
II S.
239) außer Kraft.

Potsdam, den 29.
Februar 2012

Der Minister für Infrastruktur und Landwirtschaft  
Jörg Vogelsänger

**Anlage 1**   
(zu § 1 Absatz 1)

### Abgrenzung des zu Brandenburg gehörenden Teils des bestimmten Anbaugebietes Saale-Unstrut

Landkreis

Stadt/Gemeinde

Gemarkung und Flur

Potsdam-Mittelmark

Werder (Havel)

Neu Töplitz Flur 1  
Phöben Flur 6  
Plessow Flur 3  
Werder (Havel) Flur 3, 4, 11, 12

**Anlage 2**   
(zu § 1 Absatz 2)

### Abgrenzung des zu Brandenburg gehörenden Teils des bestimmten Anbaugebietes Sachsen

Landkreis

Stadt/Gemeinde

Elbe-Elster

Schlieben

Oberspreewald-Lausitz

Ortrand

**Anlage 3** (zu § 1 Absatz 3)

### Abgrenzung des zu Brandenburg gehörenden Teils des Landweingebietes Sächsischer Landwein

Landkreis

Stadt/Gemeinde

Elbe-Elster

Schlieben

**Anlage 4** (zu § 1 Absatz 4)

### Abgrenzung des zu Brandenburg gehörenden Teils des Landweingebietes Brandenburger Landwein

Landkreis

Stadt/Gemeinde

 

Brandenburg (Havel)

 

Cottbus

 

Frankfurt (Oder)

 

Potsdam

Barnim

Niederfinow

Dahme-Spreewald

alle Städte und Gemeinden

Elbe-Elster

alle Städte und Gemeinden außer Schlieben

Märkisch-Oderland

Müncheberg

Oberhavel

Meseberg

Oberspreewald-Lausitz

alle Städte und Gemeinden

Oder-Spree

alle Städte und Gemeinden

Ostprignitz-Ruppin

Vielitzsee

Potsdam-Mittelmark

alle Städte und Gemeinden

Spree-Neiße

alle Städte und Gemeinden

Teltow-Fläming

alle Städte und Gemeinden

Uckermark

Templin

Uckermark

Prenzlau

**Anlage 5  
**(zu § 4)

### Rebsorten[\*)](#0), die zur Erzeugung von Wein, Landwein und Qualitätswein der im Land Brandenburg belegenen Teile der nach § 1 abgegrenzten Anbaugebiete zugelassen sind

Nr.

Rebsorte

Synonyme

Traubenfarbe

1

Accent

–

schwarz

2

Acolon

–

schwarz

3

Albalonga

–

weiß

4

Allegro

–

schwarz

5

André

–

schwarz

6

Arnsburger

–

weiß

7

Auxerrois

–

weiß

8

Bacchus

–

weiß

9

Blauburger

–

schwarz

10

Bolero

–

schwarz

11

Bronner

–

weiß

12

Weißer Burgunder

Weißburgunder, Pinot Blanc, Pinot Bianco

weiß

13

Cabernet Blanc

–

weiß

14

Cabernet Carbon

–

schwarz

15

Cabernet Cantor

–

schwarz

16

Cabernet Carol

–

schwarz

17

Cabernet Cortis

–

schwarz

18

Cabernet Cubin

–

schwarz

19

Cabernet Dorio

–

schwarz

20

Cabernet Dorsa

–

schwarz

21

Cabernet Franc

–

schwarz

22

Cabernet Jura

–

schwarz

23

Cabernet Mitos

–

schwarz

24

Cabernet Sauvignon

–

schwarz

25

Cabertin

–

schwarz

26

Chardonnay

–

weiß

27

Cal 6-04

–

weiß

28

Dakapo

–

schwarz

29

Deckrot

–

schwarz

30

Domina

–

schwarz

31

Dornfelder

–

schwarz

32

Dunkelfelder

–

schwarz

33

Ehrenbreitsteiner

–

weiß

34

Ehrenfelser

–

weiß

35

Roter Elbling

–

rot

36

Weißer Elbling

Elbling

weiß

37

Faberrebe

Faber

weiß

38

Findling

–

weiß

39

Freisamer

–

weiß

40

Blauer Frühburgunder

Frühburgunder, Pinot Madelaine,  
Pinot Noir Précose

schwarz

41

Goldriesling

–

weiß

42

Roter Gutedel

Chasselas Rouge

rot

43

Weißer Gutedel

Gutedel, Chasselas

weiß

44

Hegel

–

schwarz

45

Helfensteiner

–

schwarz

46

Helios

–

weiß

47

Heroldrebe

–

schwarz

48

Hibernal

–

weiß

49

Hölder

–

weiß

50

Huxelrebe

–

weiß

51

Johanniter

–

weiß

52

Juwel

–

weiß

53

Kanzler

–

weiß

54

Kerner

–

weiß

55

Kernling

–

weiß

56

Blauer Limberger

Lemberger, Blaufränkisch

schwarz

57

Früher roter Malvasier

Malvasier

rot

58

Mariensteiner

–

weiss

59

Merlot

–

schwarz

60

Merzling

–

weiß

61

Monarch

–

rot

62

Morio Muskat

–

weiß

63

Muscaris

–

weiß

64

Muskat Ottonel

–

weiß

65

Muskat-Trollinger

–

schwarz

66

Gelber Muskateller

Muskateller, Moscato, Muscat

weiß

67

Roter Muskateller

–

rot

68

Müllerrebe

Schwarzriesling, Pinot Meunier

schwarz

69

Müller-Thurgau

Rivaner

weiß

70

Neronet

–

schwarz

71

Nobling

–

weiß

72

Optima

–

weiß

73

Orion

–

weiß

74

Ortega

–

weiß

75

Osteiner

–

weiß

76

Palas

–

schwarz

77

Perle

–

rot

78

Perle von Zala

–

weiß

79

Phoenix

Phönix

weiß

80

Pinotin

–

schwarz

81

Piroso

–

rot

82

Blauer Portugieser

Portugieser

schwarz

83

Prinzipal

–

weiß

84

Prior

–

schwarz

85

Regent

–

schwarz

86

Regner

–

weiß

87

Reichensteiner

–

weiß

88

Rieslaner

–

weiß

89

Riesel

–

weiß

90

Roter Riesling

–

rot

91

Weißer Riesling

Riesling, Rheinriesling, Riesling Renano

weiß

92

Rondo

–

schwarz

93

Rotberger

–

schwarz

94

Rubinet

–

schwarz

95

Ruländer

Grauer Burgunder, Grauburgunder,  
Pinot Gris, Pinot Grigio

grau

96

Saint Laurent

–

schwarz

97

Saphira

–

weiß

98

Sauvignon Blanc

–

weiß

99

Scheurebe

–

weiß

100

Schönburger

–

rose

101

Siegerrebe

–

rose

102

Silcher

–

weiß

103

Blauer Silvaner

–

schwarz

104

Grüner Silvaner

Silvaner

weiß

105

Sirius

–

weiß

106

Solaris

–

weiß

107

Blauer Spätburgunder

Spätburgunder, Pinot Nero, Pinot Noir,  
Samtrot

schwarz

108

Souvignier Gris

–

rose

109

Staufer

–

weiß

110

Tauberschwarz

–

schwarz

111

Roter Traminer

Traminer, Gewürztraminer

rose

112

Blauer Trollinger

Trollinger

schwarz

113

Grüner Veltliner

Veltliner

weiß

114

Villaris

–

weiß

115

Würzer

–

weiß

116

Blauer Zweigelt

Zweigelt

schwarz

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#1) Die nach § 4 Absatz 1 zugelassenen Rebsorten sind für die Herstellung von Landwein, Qualitätswein, Prädikatswein, Qualitätslikörwein b. A., Qualitätsperlwein b. A.
oder Sekt b. A.
nur zulässig, wenn sie in eine Produktspezifikation nach Artikel 71 Absatz 2 der Verordnung (EU) Nr. 1308/2013 in der jeweils geltenden Fassung aufgenommen worden sind.

**Anlage 6** (zu § 6 Absatz 2)

### Natürliche Mindestalkoholgehalte für Qualitätswein, Prädikatswein und Sekt b.
A.
für den zu Brandenburg gehörenden Teil des Anbaugebietes Saale-Unstrut

Weinkategorie

Qualitätswein

Kabinett

Spätlese

Auslese

Beerenauslese, Eiswein

Trockenbeerenauslese

&N

%vol / Grad Oe

%vol / Grad Oe

%vol / Grad Oe

%vol / Grad Oe

%vol / Grad Oe

%vol / Grad Oe

Qualitätswein:  
Weißer Burgunder, Ruländer, Blauer Spätburgunder, Traminer

7,5 / 60

\-

\-

\-

\-

\-

Qualitätswein:  
sonstige Sorten

6,7 / 55

\-

\-

\-

\-

\-

Prädikatswein:  
alle Sorten

\-

9,8 / 75

11,4 / 85

13,0 / 95

16,9 / 120

21,5 / 150

Der natürliche Mindestalkoholgehalt für Sekt b.
A.
wird auf 6,7 Volumenprozent (55 Grad Oe) festgesetzt.

**Anlage 7  
**(zu § 6 Absatz 3)

### Natürliche Mindestalkoholgehalte für Qualitätswein, Prädikatswein und Sekt b.
A.
für den zu Brandenburg gehörenden Teil des bestimmten Anbaugebietes Sachsen

Weinkategorie

Qualitätswein

Kabinett

Spätlese

Auslese

Beerenauslese, Eiswein

Trockenbeerenauslese

 

%vol / Grad Oe

%vol / Grad Oe

%vol / Grad Oe

%vol / Grad Oe

%vol / Grad Oe

%vol / Grad Oe

Weißwein:  
Ruländer, Traminer, Weißburgunder

8,3 / 65

10,3 / 78

11,4 / 85

12,2 / 90

\-

\-

Weißwein:  
übrige Sorten und Weine ohne Sortenangabe

7,5 / 60

9,5 / 73

10,6 / 80

11,9 / 88

\-

\-

Rotwein:  
Portugieser, Dornfelder und Rotweine ohne Sortenangabe

7,5 / 60

9,5 / 73

10,6 / 80

11,9 / 88

\-

\-

Rotwein:  
übrige Sorten

8,3 / 65

10,3 / 78

11,4 / 85

12,2 / 90

\-

\-

Weiß- und Rotwein:  
alle Sorten

\-

\-

\-

\-

15,3 / 110

21,5 / 150

Der natürliche Mindestalkoholgehalt für Sekt b.
A.
wird auf 7,5 Volumenprozent (60 Grad Oe) festgesetzt.

**Anlage 8**  
(zu § 16)

### Muster zur Führung des Herbstbuches

Herbstbuch

Jahrgang

Betrieb

Seite  _(fortlaufend nummerieren)_

Spalten 6 bis 8: wahlweise Angabe der Menge in kg Trauben oder Liter Maische oder Most

 

1

2

3

4

5

6

7

8

9

Lfd.-Nr.

Datum der Lese

Lage

Rebsorte

Grad Oe

Trauben

Maische

Most

Bemerkungen:  
önologische Verfahren, Abgabe oder Verkauf