## Verordnung über beliehene Krankenhäuser zur Unterbringung von psychisch kranken Menschen  (Unterbringungskrankenhausverordnung -  UKV)

Auf Grund des § 10 Absatz 2 Satz 1 des Brandenburgischen Psychisch-Kranken-Gesetzes vom 5. Mai 2009 (GVBl.
I S. 134) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz im Einvernehmen mit dem Minister des Innern:

#### § 1 

(1) Die Träger der im anliegenden Unterbringungsplan benannten Krankenhäuser sind für die Aufnahme der öffentlich-rechtlich unterzubringenden psychisch kranken Menschen nach den Vorschriften des Brandenburgischen Psychisch-Kranken-Gesetzes sachlich zuständig und werden hierfür mit hoheitlicher Gewalt beliehen.

(2) Sie sind örtlich zuständig für die ihnen im Unterbringungsplan jeweils als Aufnahmegebiet zugeordneten Landkreise, kreisfreien Städte, Ämter oder amtsfreien Gemeinden.

#### § 2 

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Unterbringungsverordnung vom 25.
August 1997 (GVBl.
II S. 755) außer Kraft.

Potsdam, den 21.
Oktober 2010

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

* * *

Anlage 
-------

### Unterbringungsplan

I.
Unterbringung von erwachsenen Personen

_**1.
Versorgungsgebiet Neuruppin**_

**Krankenhaus**

**Aufnahmegebiet**

Ruppiner Kliniken GmbH  
Fehrbelliner Str.
38  
16816 Neuruppin

_Landkreis_  
Ostprignitz-Ruppin

Kreiskrankenhaus Prignitz gGmbH  
Dobberziner Str.
112  
19348 Perleberg

_Landkreis_  
Prignitz

Oberhavel Kliniken GmbH  
Klinik Hennigsdorf  
Marwitzer Str.
91  
16761 Hennigsdorf

_amtsfreie Gemeinden_  
Birkenwerder  
Glienicke/Nordbahn  
Leegebruch  
Löwenberger Land  
Mühlenbecker Land  
Oberkrämer

_Ämter_  
Gransee und Gemeinden

_Städte_  
Fürstenberg/Havel  
Hennigsdorf  
Hohen-Neuendorf  
Kremmen  
Liebenwalde  
Oranienburg  
Velten  
Zehdenick

**_2.
Versorgungsgebiet Schwedt_**

**Krankenhaus**

**Aufnahmegebiet**

Krankenhaus Angermünde  
Rudolf-Breitscheid-Str.
37  
16278 Angermünde

_Landkreis_  
Uckermark

Martin Gropius Krankenhaus GmbH  
Oderberger Straße 8  
16225 Eberswalde

_Landkreis_  
Barnim

_Ämter_  
Barnim-Oderbruch  
Falkenberg-Höhe

_Städte_  
Bad Freienwalde (Oder)  
Wriezen

**_3.
Versorgungsgebiet Potsdam_**

**Krankenhaus**

**Aufnahmegebiet**

Klinikum Ernst von Bergmann gGmbH  
Charlottenstr.
72  
14467 Potsdam

_kreisfreie Stadt_  
Landeshauptstadt Potsdam

_amtsfreie Gemeinden_  
Kleinmachnow  
Stahnsdorf

_Städte_  
Teltow 

Havelland Kliniken GmbH  
Klinik Nauen  
Ketziner Str.
21  
14641 Nauen

_amtsfreie Gemeinden_  
Brieselang  
Dallgow-Döberitz  
Schönwalde-Glien  
Wustermark

_Ämter_  
Friesack

_Städte_  
Falkensee  
Ketzin  
Nauen 

Johanniter Krankenhaus im Fläming  
Treuenbrietzen GmbH  
Johanniterstr.
1  
14929 Treuenbrietzen

_amtsfreie Gemeinden_  
Niederer Fläming  
Niedergörsdorf  
Wiesenburg/Mark  
Nuthe-Urstromtal

_Ämter_  
Brück  
Niemegk

_Städte_  
Beelitz  
Bad Belzig  
Jüterbog  
Luckenwalde  
Trebbin  
Treuenbrietzen

Asklepios Fachklinikum Brandenburg  
Anton-Saefkow-Allee 2  
14772 Brandenburg an der Havel

_kreisfreie Stadt_  
Brandenburg an der Havel

_amtsfreie Gemeinden_  
Groß Kreutz/Havel  
Kloster Lehnin  
Michendorf  
Milower Land  
Nuthetal  
Schwielowsee  
Seddiner See

_Ämter_  
Beetzsee  
Nennhausen  
Rhinow  
Wusterwitz  
Ziesar

_Städte_  
Premnitz  
Rathenow  
Werder (Havel)

**_4.
Versorgungsgebiet Cottbus_**

**Krankenhaus**

**Aufnahmegebiet**

Carl-Thiem-Klinikum gGmbH  
Thiemstr.
111  
03048 Cottbus 

_kreisfreie Stadt_  
Cottbus

Klinikum Niederlausitz GmbH  
Calauer Str.
8  
01968 Senftenberg

_amtsfreie Gemeinden_  
Schipkau

_Ämter_  
Ortrand  
Ruhland

_Städte_  
Großräschen  
Lauchhammer  
Schwarzheide  
Senftenberg 

Elbe-Elster Klinikum GmbH  
Kirchhainer Str.
38 a  
03238 Finsterwalde 

_Landkreis_  
Elbe-Elster

Krankenhaus Spremberg  
Karl-Marx-Str.
80  
03130 Spremberg

_Landkreis_  
Spree-Neiße

Asklepios Fachklinikum Lübben  
Luckauer Str.
17  
15907 Lübben

_amtsfreie Gemeinden_  
Heideblick  
Märkische Heide

_Ämter_  
Altdöbern  
Dahme/Mark  
Golßener Land  
Lieberose/Oberspreewald  
Unterspreewald

_Städte_  
Calau  
Lübben (Spreewald)  
Lübbenau (Spreewald)  
Luckau  
Vetschau

Asklepios Fachklinikum Teupitz  
Buchholzer Str.
21  
15755 Teupitz

_amtsfreie Gemeinden_  
Am Mellensee  
Bestensee  
Blankenfelde-Mahlow  
Eichwalde  
Großbeeren  
Heidesee  
Rangsdorf  
Schönefeld  
Schulzendorf  
Wildau  
Zeuthen

_Ämter_  
Schenkenländchen

_Städte_  
Baruth/Mark  
Königs Wusterhausen  
Ludwigsfelde  
Mittenwalde  
Zossen

**_5.
Versorgungsgebiet Frankfurt (Oder)_**

**Krankenhaus**

**Aufnahmegebiet**

Klinikum Frankfurt (Oder) GmbH  
Müllroser Chaussee 7  
15236 Frankfurt (Oder)

_kreisfreie Stadt_  
Frankfurt (Oder)

_amtsfreie Gemeinden_  
Letschin

_Ämter_  
Golzow  
Lebus  
Neuhardenberg  
Seelow-Land

_Städte_  
Seelow

Städtisches Krankenhaus  
Eisenhüttenstadt GmbH  
Friedrich-Engels-Str.
39  
15890 Eisenhüttenstadt

_amtsfreie Gemeinden_  
Rietz-Neuendorf  
Tauche

_Ämter_  
Brieskow-Finkenheerd  
Neuzelle  
Schlaubetal

_Städte_  
Beeskow  
Eisenhüttenstadt  
Friedland  
Storkow (Mark)

Immanuel Klinik Rüdersdorf  
Seebad 82/83  
15562 Rüdersdorf

_amtsfreie Gemeinden_  
Fredersdorf-Vogelsdorf  
Grünheide (Mark)  
Hoppegarten  
Neuenhagen bei Berlin  
Petershagen/Eggersdorf  
Rüdersdorf  
Schöneiche bei Berlin  
Steinhöfel  
Woltersdorf

_Ämter_  
Märkische Schweiz  
Odervorland  
Scharmützelsee  
Spreenhagen

_Städte_  
Altlandsberg  
Erkner  
Fürstenwalde/Spree  
Müncheberg  
Strausberg

II.
Unterbringung von Kindern und Jugendlichen

**_1.
Versorgungsgebiet Neuruppin_**

**Krankenhaus**

**Aufnahmegebiet**

Ruppiner Kliniken GmbH  
Fehrbelliner Str.
38  
16816 Neuruppin 

_Landkreise_  
Ostprignitz-Ruppin  
Prignitz  
Oberhavel

**_2.
Versorgungsgebiet Schwedt_**

**Krankenhaus**

**Aufnahmegebiet**

Martin Gropius Krankenhaus GmbH  
Oderberger Straße 8  
16225 Eberswalde

_Landkreise_  
Barnim  
Uckermark

**_3.
Versorgungsgebiet Potsdam_**

**Krankenhaus**

**Aufnahmegebiet**

Klinikum Ernst von Bergmann gGmbH  
Charlottenstrasse 72  
14467 Potsdam

_Landkreis_  
Teltow-Fläming

_kreisfreie Stadt_  
Landeshauptstadt Potsdam

_amtsfreie Gemeinden_  
Kleinmachnow  
Stahnsdorf

_Stadt_  
Teltow

Asklepios Fachklinikum Brandenburg  
Anton-Saefkow-Allee 2  
14772 Brandenburg an der Havel

_Landkreise_  
Havelland  
Potsdam-Mittelmark mit  
Ausnahme der amtsfreien  
Gemeinden Kleinmachnow  
und Stahnsdorf sowie der  
Stadt Teltow

_kreisfreie Stadt_  
Brandenburg an der Havel

**_4.
Versorgungsgebiet Cottbus_**

**Krankenhaus**

**Aufnahmegebiet**

Asklepios Fachklinikum Lübben  
Luckauer Str.
17  
15907 Lübben

_Landkreise_  
Dahme-Spreewald  
Spree-Neiße  
Oberspreewald-Lausitz  
Elbe-Elster

_kreisfreie Stadt_  
Cottbus

**_5.
Versorgungsgebiet Frankfurt (Oder)_**

**Krankenhaus**

**Aufnahmegebiet**

Klinikum Frankfurt (Oder)  
Müllroser Chaussee 7  
15236 Frankfurt (Oder)

_Landkreise_  
Märkisch-Oderland  
Oder-Spree

_kreisfreie Stadt_  
Frankfurt (Oder)