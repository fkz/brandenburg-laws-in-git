## Verordnung zur Festsetzung des Wasserschutzgebietes für das Wasserwerk Erkner, Wasserfassungen Neu Zittauer und Hohenbinder Straße

Auf Grund des § 51 Absatz 1 Satz 1 Nummer 1, Satz 2 und 3 und Absatz 2 und des § 52 Absatz 1 des Wasserhaushaltsgesetzes vom 31.
Juli 2009 (BGBl.
I S. 2585) in Verbindung mit § 15 Absatz 1 Satz 1 und Absatz 3 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2. März 2012 (GVBl.
I Nr. 20), der durch Artikel 1 des Gesetzes vom 4.
Dezember 2017 (GVBl. I Nr. 28) neu gefasst worden ist, verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Allgemeines

(1) Zur Sicherung der öffentlichen Wasserversorgung wird zum Schutz des Grundwassers im Einzugsgebiet der Wasserfassungen in der Neu Zittauer Straße und der Hohenbinder Straße des Wasserwerkes Erkner das in § 2 näher umschriebene Wasserschutzgebiet festgesetzt.
Begünstigter ist der Wasserverband Strausberg-Erkner.

(2) Das Wasserschutzgebiet gliedert sich in den Fassungsbereich (Zone I), in die engere Schutzzone (Zone II) und in die weitere Schutzzone (Zone III).
Die Zone III unterteilt sich in die Zone III A und die Zone III B.

#### § 2 Räumlicher Geltungsbereich

(1) Lage und Größe des Wasserschutzgebietes und der Schutzzonen ergeben sich aus der Übersichtskarte in der Anlage 2 und den in Absatz 2 genannten Karten.

(2) Die Schutzzonen sind in der topografischen Karte im Maßstab 1 : 10 000 in der Anlage 3 und außerdem in der Liegenschaftskarte im Maßstab 1 : 2 500 in der Anlage 4, die aus 22 Blättern besteht, dargestellt.
Für die Abgrenzung der Schutzzonen ist die Darstellung in der Liegenschaftskarte maßgebend.

(3) Zu Informationszwecken werden zusätzlich auf Papier ausgefertigte Exemplare der in Absatz 2 genannten Karten bei den unteren Wasserbehörden der Landkreise Oder-Spree und Dahme-Spreewald, in der Stadtverwaltung Erkner, im Amt Spreenhagen und in der Gemeinde Grünheide (Mark) hinterlegt und können dort während der Dienststunden von jedermann kostenlos eingesehen werden.
Diese Karten sind mit dem Dienstsiegel des Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft (Siegelnummer 1) versehen.
Eine weitere so gesiegelte Ausfertigung der Karten befindet sich im Ministerium für Ländliche Entwicklung, Umwelt und Landwirtschaft.

(4) Veränderungen der Topografie sowie von Flurstücksgrenzen oder -bezeichnungen berühren den räumlichen Geltungsbereich der Schutzzonen nicht.

#### § 3 Schutz der Zone III B

In der Zone III B sind verboten:

1.  das Düngen mit Gülle, Jauche, Geflügelkot, Festmist, Silagesickersaft, Gärresten, Wirtschaftsdüngern aus pflanzlichen Stoffen, Bodenhilfsstoffen, Kultursubstraten, Pflanzenhilfsmitteln, gütegesicherten Grünabfall- oder Bioabfallkomposten, Abfällen aus der Herstellung oder Verarbeitung landwirtschaftlicher Erzeugnisse oder sonstigen Düngemitteln mit im Sinne des § 2 Nummer 11 der Düngeverordnung wesentlichen Nährstoffgehalten an Stickstoff oder Phosphat,
    1.  wenn die Düngung nicht im Sinne des § 3 Absatz 1 und 2 sowie § 11 der Düngeverordnung in betriebsspezifisch analysierten zeit- und bedarfsgerechten Gaben oder nicht durch Geräte, die den allgemein anerkannten Regeln der Technik entsprechen, erfolgt,
    2.  wenn die Nährstoffzufuhr auf landwirtschaftlichen oder erwerbsgärtnerischen Nutzflächen schlagbezogen mehr als 120 Kilogramm Gesamtstickstoff je Hektar pro Düngejahr aus organischen Düngern, ohne Stall- und Lagerungsverluste, beträgt,
    3.  wenn keine schlagbezogenen Aufzeichnungen über die Zu- und Abfuhr von Stickstoff und Phosphat erstellt und mindestens sieben Jahre lang nach Ablauf des Düngejahres aufbewahrt werden,
    4.  auf abgeerntetem Ackerland, wenn nicht entsprechend der Anforderungen des § 6 Absatz 9 der Düngeverordnung unmittelbar Folgekulturen einschließlich Zwischenfrüchte angebaut werden,
    5.  auf landwirtschaftlich oder erwerbsgärtnerisch genutzten Flächen ab dem Zeitpunkt, ab dem die Ernte der letzten Hauptfrucht abgeschlossen ist bis 15. Februar,
    6.  auf landwirtschaftlich oder erwerbsgärtnerisch genutzten Flächen bei Verwendung von Gülle, Jauche, sonstigen flüssigen organischen oder organisch-mineralischen Düngemitteln, einschließlich Gärresten vom 15.
        September bis 1.
        März,
    7.  auf Brachland oder stillgelegten Flächen,
    8.  auf wassergesättigten, oberflächlich oder in der Tiefe gefrorenen oder schneebedeckten Böden oder
    9.  auf ackerbaulich oder erwerbsgärtnerisch genutzten Flächen mit einem zu erwartenden Flurabstand des Grundwassers von 50 Zentimetern oder weniger,
2.  das Lagern oder Ausbringen von Fäkalschlamm oder Klärschlämmen aller Art einschließlich in Biogasanlagen behandelte Klärschlämme, Abfällen aus der Herstellung und Verarbeitung nicht landwirtschaftlicher Erzeugnisse und von nicht gütegesicherten Grünabfall- und Bioabfallkomposten, ausgenommen die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen und Ausbringung im Garten,
3.  das Errichten, Erweitern oder Betreiben von Dunglagerstätten, ausgenommen befestigte Dunglagerstätten mit Sickerwasserfassung und dichtem Jauchebehälter, der über ein Leckageerkennungssystem verfügt,
4.  das Errichten, Erweitern oder Betreiben von Erdbecken zur Lagerung von Gülle, Jauche, Silagesickersäften oder von Gärresten,
5.  das Errichten, Erweitern oder Betreiben von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle, Jauche, Silagesickersaft, Gärresten oder flüssigem Kompost aus landwirtschaftlicher Herkunft, ausgenommen Anlagen mit Leckageerkennungssystem und Sammeleinrichtung, wenn der unteren Wasserbehörde
    1.  vor Inbetriebnahme,
    2.  bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung sowie
    3.  wiederkehrend alle fünf Jahreein durch einen Sachverständigen geführter Nachweis über die Dichtheit der Sammeleinrichtung vorgelegt wird,
6.  das Lagern von organischen oder mineralischen Düngern auf unbefestigten Flächen oder auf nicht baugenehmigten Anlagen, ausgenommen das Lagern von Kompost aus dem eigenen Haushalt oder Garten,
7.  das Errichten, Erweitern oder Betreiben von ortsfesten Anlagen für die Silierung von Pflanzen oder die Lagerung von Silage, ausgenommen
    1.  Anlagen mit Silagesickersaft-Sammelbehälter, der über ein Leckageerkennungssystem verfügt, und
    2.  Anlagen mit Ableitung in Jauche- oder Güllebehälter,wenn der unteren Wasserbehörde vor Inbetriebnahme, bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung, sowie wiederkehrend alle fünf Jahre ein durch einen Sachverständigen geführter Nachweis über die Dichtheit der Behälter und Leitungen vorgelegt wird,
8.  die Silierung von Pflanzen oder Lagerung von Silage außerhalb ortsfester Anlagen, ausgenommen Ballensilage im Wickelverfahren,
9.  das Errichten oder Erweitern von Stallungen für Tierbestände für mehr als 50 Großvieheinheiten gemäß Anlage 1 Nummer 1 oder von unbefestigten Tierunterständen,
10.  die Anwendung von Pflanzenschutzmitteln, außer auf erwerbsgärtnerisch, land- oder forstwirtschaftlich genutzten Flächen,
    1.  wenn die Zulassungs- und Anwendungsbestimmungen eingehalten werden,
    2.  wenn der Einsatz durch Anwendung der Allgemeinen Grundsätze des integrierten Pflanzenschutzes auf das notwendige Maß beschränkt wird,
    3.  wenn flächenbezogene Aufzeichnungen nach dem Pflanzenschutzgesetz geführt und mindestens sieben Jahre lang nach dem Einsatz aufbewahrt werden,
    4.  wenn ein Abstand von mehr als 10 Metern zu oberirdischen Gewässern eingehalten wird,
    5.  wenn die Anwendung nicht der Bodenentseuchung dient und
    6.  wenn die Anwendung nicht auf Dauergrünland und Grünlandbrachen erfolgt,
11.  die Anwendung von Biozidprodukten, insbesondere aus den Produktarten 8, 14, 18 und 19 des Anhangs V der Verordnung (EU) Nr. 528/2012, wenn ein Eindringen in den Boden oder das Grundwasser nicht ausgeschlossen werden kann, außer auf erwerbsgärtnerisch, land- oder forstwirtschaftlich genutzten Flächen,
    1.  wenn die Zulassungs- und Anwendungsbestimmungen eingehalten werden,
    2.  wenn der Einsatz auf das notwendige Maß beschränkt wird,
    3.  wenn flächenbezogene Aufzeichnungen über den Einsatz geführt und mindestens sieben Jahre lang nach dem Einsatz aufbewahrt werden,
    4.  wenn ein Abstand von mehr als 10 Metern zu oberirdischen Gewässern eingehalten wird,
    5.  wenn die Anwendung nicht der Bodenentseuchung dient und
    6.  wenn die Anwendung nicht auf Dauergrünland und Grünlandbrachen erfolgt,
12.  die Beregnung landwirtschaftlich oder erwerbsgärtnerisch genutzter Flächen, wenn die Beregnungshöhe 20 Millimeter pro Tag oder 60 Millimeter pro Woche überschreitet,
13.  der Umbruch von Dauergrünland oder von Grünlandbrachen,
14.  der Umbruch von Dauerbrachen in der Zeit vom 1.
    Juli bis 1.
    März, ausgenommen bei nachfolgendem Anbau von Winterraps,
15.  das Anlegen von Schwarzbrache im Sinne der Anlage 1 Nummer 3, soweit dies nicht fruchtfolge- oder witterungsbedingt ausgeschlossen ist,
16.  Erstaufforstungen mit Nadelbaumarten oder Robinien,
17.  die Umwandlung von Wald in eine andere Nutzungsart, ausgenommen soweit für die Umsetzung von Vorhaben im Geltungsbereich der zum Zeitpunkt des Inkrafttretens dieser Verordnung rechtskräftigen Bebauungspläne erforderlich,
18.  Holzerntemaßnahmen, die eine gleichmäßig verteilte Überschirmung von weniger als 60 Prozent des Waldbodens oder Freiflächen größer als 1 000 Quadratmeter erzeugen, ausgenommen
    1.  Femel- oder Saumschläge und
    2.  soweit für die Umsetzung von Vorhaben im Geltungsbereich der zum Zeitpunkt des Inkrafttretens dieser Verordnung rechtskräftigen Bebauungspläne erforderlich,
19.  Aufschlüsse der Erdoberfläche im Sinne des § 49 Absatz 1 des Wasserhaushaltsgesetzes, selbst wenn das Grundwasser nicht aufgedeckt wird, wie zum Beispiel das Errichten und Erweitern von Kies-, Sand- oder Tongruben, Steinbrüchen, Übertagebergbauen oder Torfstichen, wenn die Schutzfunktion der Deckschichten hierdurch wesentlich gemindert wird, ausgenommen das Errichten von Kleingewässern bis 100 Quadratmeter,
20.  das Errichten, Erweitern oder Erneuern von
    1.  Bohrungen, welche die gering leitende Deckschicht über oder unter dem genutzten Grundwasserleiter verletzen können,
    2.  Grundwassermessstellen oder
    3.  Brunnen,ausgenommen das Erneuern von Brunnen für Entnahmen mit zum Zeitpunkt des Inkrafttretens dieser Verordnung rechtskräftiger wasserrechtlicher Erlaubnis oder Bewilligung und das Erneuern von erlaubnisfreien Brunnen im Sinne des § 46 des Wasserhaushaltsgesetzes,
21.  das Errichten oder Erweitern von Anlagen mit Erdwärmesonden,
22.  das Errichten oder Erweitern von Rohrleitungsanlagen für wassergefährdende Stoffe, ausgenommen Rohrleitungsanlagen im Sinne des § 62 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes,
23.  das Errichten, Erweitern oder Betreiben von Anlagen zur behälterlosen Lagerung oder Ablagerung von Stoffen im Untergrund,
24.  das Behandeln, Lagern oder Ablagern von Abfall, tierischen Nebenprodukten oder bergbaulichen Rückständen, ausgenommen
    1.  die vorübergehende Lagerung in dichten Behältern,
    2.  die ordnungsgemäße kurzzeitige Bereitstellung von vor Ort angefallenem Abfall zum Abtransport durch den Entsorgungspflichtigen und
    3.  die Kompostierung von aus dem eigenen Haushalt oder Garten stammenden Pflanzenabfällen,
25.  das Ein- oder Aufbringen von Abfällen, bergbaulichen Rückständen oder Ersatzbaustoffen einschließlich Bodenmaterial und Baggergut in oder auf Böden oder deren Einbau in bodennahe technische Bauwerke,
26.  das Errichten oder Erweitern von Anlagen zum Umgang mit radioaktiven Stoffen im Sinne des Atomgesetzes, ausgenommen für medizinische Anwendungen sowie für Mess-, Prüf- und Regeltechnik,
27.  der Umgang mit wassergefährdenden Stoffen im Sinne des § 62 Absatz 3 des Wasserhaushaltsgesetzes außerhalb von zugelassenen Anlagen, Vorrichtungen und Behältnissen, aus denen ein Eindringen in den Boden nicht möglich ist, ausgenommen
    1.  der Umgang mit Jauche, Gülle, Silagesickersaft sowie Dünge- und Pflanzenschutzmitteln im Rahmen ordnungsgemäßer Landwirtschaft entsprechend dieser Verordnung sowie
    2.  der Umgang mit haushaltsüblichen Kleinstmengen,
28.  das Einleiten oder Einbringen von wassergefährdenden Stoffen im Sinne des § 62 Absatz 3 des Wasserhaushaltsgesetzes in den Untergrund oder in Gewässer,
29.  das Errichten oder Erweitern von Industrieanlagen zum Umgang mit wassergefährdenden Stoffen in großem Umfang, wie zum Beispiel in Raffinerien, Metallhütten oder chemischen Fabriken,
30.  das Errichten oder Erweitern von Kraftwerken oder Heizwerken, die der Genehmigungspflicht nach Bundesimmissionsschutzrecht unterliegen, ausgenommen mit Gas, Sonnenenergie oder Windkraft betriebene Anlagen,
31.  das Errichten oder Erweitern von Biogasanlagen, ausgenommen zur Verwertung der Wirtschaftsdünger aus dem in der Zone III B befindlichen landwirtschaftlichen Betriebsstandort,
32.  das Errichten oder Erweitern von Abwasserbehandlungsanlagen, ausgenommen
    1.  die Sanierung bestehender Abwasserbehandlungsanlagen zugunsten des Gewässerschutzes und
    2.  Abwasservorbehandlungsanlagen wie Fett-, Leichtflüssigkeits- oder Amalgamabscheider,
33.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik eingehalten werden,
34.  das Errichten oder Erweitern von Abwassersammelgruben, ausgenommen
    1.  Anlagen mit allgemeiner bauaufsichtlicher Zulassung des Deutschen Institutes für Bautechnik und
    2.  monolithische Sammelgruben aus Beton, die den allgemein anerkannten Regeln der Technik entsprechen,
35.  das Betreiben oder Unterhalten von Abwassersammelgruben, wenn der unteren Wasserbehörde nicht
    1.  vor Inbetriebnahme,
    2.  bei bestehenden Anlagen innerhalb eines Jahres nach Inkrafttreten dieser Verordnung sowie
    3.  wiederkehrend alle fünf Jahre für Sammelgruben mit DIBt-Zulassung sowie Sammelgruben in monolithischer Bauweise oder alle drei Jahre für übrige Sammelgrubenein durch ein unabhängiges fachkundiges Unternehmen geführter Nachweis über die Dichtheit vorgelegt wird,
36.  das Errichten, Erweitern, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten, ausgenommen Anlagen mit dichtem Behälter,
37.  das Einleiten von Abwasser – mit Ausnahme von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 Nummer 4 – in oberirdische Gewässer, sofern die Einleitung nicht schon zum Zeitpunkt des Inkrafttretens dieser Verordnung wasserrechtlich zugelassen war,
38.  das Ausbringen von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes,
39.  das Einleiten oder Versickern von Schmutzwasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 1 oder des § 54 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser,
40.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen
    1.  das breitflächige Versickern von Niederschlagswasserabflüssen von gering belasteten Herkunftsflächen im Sinne der Anlage 1 Nummer 4 über die belebte Bodenzone einer ausreichend mächtigen und bewachsenen Oberbodenschicht gemäß den allgemein anerkannten Regeln der Technik oder
    2.  mit wasserrechtlicher Erlaubnis,sofern die Versickerung außerhalb von Altlasten, Altlastenverdachtsflächen oder Flächen mit schädlichen Bodenveränderungen und nur auf Flächen mit einem zu erwartenden Flurabstand des Grundwassers von 100 Zentimetern oder größer erfolgt,
41.  das Anwenden von Auftaumitteln auf Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen auf Bundesautobahnen, Bundes-, Landes- und Kreisstraßen sowie ausgenommen bei Extremwetterlagen wie Eisregen,
42.  das Errichten sowie der Um- oder Ausbau von Straßen oder Wegen, wenn hierbei nicht die allgemein anerkannten Regeln der Technik für bautechnische Maßnahmen an Straßen in Wasserschutzgebieten eingehalten werden,
43.  das Errichten oder Erweitern von Rangier- oder Güterbahnhöfen, ausgenommen Maßnahmen zur Anpassung an den Stand der Technik,
44.  das Verwenden von Baustoffen, Böden oder anderen Materialien, die auslaug- und auswaschbare wassergefährdende Stoffe enthalten (zum Beispiel Schlacke, Bauschutt, Teer, Imprägniermittel) für Bau- und Unterhaltungsmaßnahmen, zum Beispiel im Straßen-, Wege-, Deich-, Wasser-, Landschafts- oder Tiefbau,
45.  das Einrichten, Erweitern oder Betreiben von öffentlichen Freibädern oder Zeltplätzen sowie Camping aller Art, ausgenommen
    1.  Einrichtungen mit ordnungsgemäßer Abfall- und Abwasserentsorgung und
    2.  das Zelten von Fuß-, Rad-, Reit- und Wasserwanderern abseits von Zelt- und Campingplätzen für eine Nacht,
46.  das Einrichten, Erweitern oder Betreiben von Sportanlagen, ausgenommen Anlagen mit ordnungsgemäßer Abfall- und Abwasserentsorgung,
47.  das Errichten von Motorsportanlagen,
48.  das Errichten von Schießständen oder Schießplätzen für Feuerwaffen, ausgenommen Schießstände in geschlossenen Räumen,
49.  das Errichten von Golfanlagen,
50.  das Errichten von Flugplätzen im Sinne des § 6 Absatz 1 Satz 1 des Luftverkehrsgesetzes,
51.  das Starten oder Landen motorgetriebener Luftfahrzeuge, mit Ausnahme in Fällen des § 25 Absatz 2 des Luftverkehrsgesetzes,
52.  das Errichten von militärischen Anlagen, Standort- oder Truppenübungsplätzen,
53.  das Durchführen von militärischen Übungen, ausgenommen das Durchfahren auf klassifizierten Straßen,
54.  Bergbau einschließlich der Aufsuchung oder Gewinnung von Erdöl oder Erdgas,
55.  das Durchführen von Sprengungen, sofern die Gefahr besteht, dass dabei das Grundwasser aufgedeckt wird,
56.  die Neuausweisung oder Erweiterung von Industriegebieten.

#### § 4 Schutz der Zone III A

Die Verbote der Zone III B gelten auch in der Zone III A.
In der Zone III A sind außerdem verboten:

1.  das Errichten, Erweitern oder Betreiben von Anlagen zur Lagerung von Gülle, Jauche, Silagesickersäften, Gärresten oder flüssigem Kompost, ausgenommen Hochbehälter, bei denen Undichtigkeiten am Fußpunkt zwischen Behältersohle und aufgehender Wand sofort erkennbar sind und die über ein Leckageerkennungssystem und Sammeleinrichtung verfügen,
2.  das Errichten oder Erweitern von Stallungen oder Unterständen für Tierbestände, ausgenommen für Kleintierhaltung zur Eigenversorgung,
3.  die Freilandtierhaltung im Sinne der Anlage 1 Nummer 2, wenn die Ernährung der Tiere nicht im Wesentlichen aus der jeweils beweideten Grünlandfläche erfolgt oder wenn die Grasnarbe flächig verletzt wird, ausgenommen Kleintierhaltung für die Eigenversorgung,
4.  das Errichten oder Erweitern von Gartenbaubetrieben oder Kleingartenanlagen, ausgenommen Gartenbaubetriebe, die in geschlossenen Systemen produzieren,
5.  die Erstanlage oder Erweiterung von Baumschulen oder forstlichen Pflanzgärten, Weihnachtsbaumkulturen sowie von gewerblichem Wein-, Hopfen-, Gemüse-, Obst- oder Zierpflanzenanbau, ausgenommen Gemüse- sowie Zierpflanzenanbau unter Glas in geschlossenen Systemen und Containerproduktion von Baumschulprodukten auf versiegelten Flächen,
6.  das Einrichten oder Erweitern von Holzlagerplätzen über 100 Raummeter, die dauerhaft oder unter Einsatz von Nassholzkonservierung betrieben werden,
7.  Erdaufschlüsse im Sinne des § 49 Absatz 1 des Wasserhaushaltsgesetzes, selbst wenn Grundwasser nicht aufgedeckt wird, ausgenommen das Verlegen von Ver- und Entsorgungsleitungen und die Herstellung von Baugruben und Bohrungen,
8.  das Errichten oder Erweitern von Anlagen zum Umgang mit wassergefährdenden Stoffen im Sinne des § 62 Absatz 3 des Wasserhaushaltsgesetzes, ausgenommen
    1.  Anlagen der Gefährdungsstufen A und B gemäß § 39 Absatz 1 der Verordnung über Anlagen zum Umgang mit wassergefährdenden Stoffen,
    2.  oberirdische Anlagen der Gefährdungsstufe C gemäß § 39 Absatz 1 der Verordnung über Anlagen zum Umgang mit wassergefährdenden Stoffen,wenn diese doppelwandig ausgeführt und mit einem Leckanzeigesystem ausgerüstet sind oder wenn diese mit einem Auffangraum, der das maximal in der Anlage vorhandene Volumen wassergefährdender Stoffe aufnehmen kann, ausgerüstet sind,
9.  das Errichten oder Erweitern von Niederschlagswasser- oder Mischwasserentlastungsbauwerken,
10.  das Errichten oder Erweitern von Bahnhöfen oder Schienenwegen der Eisenbahn, ausgenommen Baumaßnahmen an vorhandenen Anlagen zur Anpassung an den Stand der Technik und zum Erhalt oder zur Verbesserung der Verkehrssicherheit,
11.  das Abhalten oder Durchführen von Märkten, Volksfesten oder Großveranstaltungen außerhalb der dafür vorgesehenen Anlagen,
12.  das Durchführen von Motorsportveranstaltungen, ausgenommen das Durchfahren auf klassifizierten Straßen,
13.  Bestattungen, ausgenommen innerhalb bereits bei Inkrafttreten dieser Verordnung bestehender Friedhöfe,
14.  die Darstellung von neuen Bauflächen oder Baugebieten im Rahmen der vorbereitenden Bauleitplanung, wenn darin eine Neubebauung bisher unbebauter Gebiete vorgesehen wird,
15.  die Festsetzung von neuen Baugebieten im Rahmen der verbindlichen Bauleitplanung, ausgenommen
    1.  Gebiete, die im zum Zeitpunkt des Inkrafttretens dieser Verordnung gültigen Flächennutzungsplan als Bauflächen oder Baugebiete dargestellt sind, und
    2.  die Überplanung von Bestandsgebieten, wenn dies zu keiner wesentlichen Erhöhung der zulässigen Grundfläche im Sinne des § 19 Absatz 2 der Baunutzungsverordnung führt.

#### § 5 Schutz der Zone II

Die Verbote der Zonen III B und III A gelten auch in der Zone II.
In der Zone II sind außerdem verboten:

1.  das Düngen mit Gülle, Jauche, Festmist, Gärresten, Wirtschaftsdüngern aus pflanzlichen Stoffen, Bodenhilfsstoffen, Kultursubstraten, Pflanzenhilfsmitteln, Grünabfall- oder Bioabfallkomposten, Abfällen aus der Herstellung oder Verarbeitung landwirtschaftlicher Erzeugnisse oder sonstigen organischen Düngern sowie die Anwendung von Silagesickersaft,
2.  das Errichten, Erweitern oder Betreiben von Dunglagerstätten,
3.  das Errichten oder Betreiben von Anlagen zum Lagern, Abfüllen oder Verwerten von Gülle, Jauche, Silagesickersaft, Gärresten oder flüssigem Kompost,
4.  die Silierung von Pflanzen oder Lagerung von Silage,
5.  die Freilandtierhaltung im Sinne der Anlage 1 Nummer 2,
6.  die Beweidung,
7.  die Anwendung von Biozidprodukten außerhalb geschlossener Gebäude oder von Pflanzenschutzmitteln,
8.  die Beregnung landwirtschaftlich oder erwerbsgärtnerisch genutzter Flächen,
9.  das Errichten, Erweitern oder Erneuern von Dränungen oder Entwässerungsgräben,
10.  der Einsatz von forstwirtschaftlichen Kraftfahrzeugen abseits von Straßen, Wegen oder forstwirtschaftlichen Rückegassen,
11.  das Vergraben, Lagern oder Ablagern von Tierkörpern oder Teilen davon,
12.  das Errichten oder Betreiben von Wildfütterungen, Kirrungen oder Luderplätzen,
13.  das Errichten, Erweitern oder Betreiben von Anlagen zum Umgang mit wassergefährdenden Stoffen im Sinne des § 62 Absatz 3 des Wasserhaushaltsgesetzes,
14.  der Einsatz von mineralischen Schmierstoffen zur Verlustschmierung oder von mineralischen Schalölen,
15.  der Umgang mit wassergefährdenden Stoffen im Sinne des § 62 Absatz 3 des Wasserhaushaltsgesetzes, ausgenommen haushaltsübliche Kleinstmengen,
16.  das Befahren mit Fahrzeugen mit wassergefährdender Ladung, nachdem die Anordnung des entsprechenden Vorschriftzeichens 269 durch die Straßenverkehrsbehörde erfolgte,
17.  das Errichten oder Erweitern von Transformatoren oder Stromleitungen mit flüssigen wassergefährdenden Kühl- oder Isoliermitteln,
18.  das Behandeln, Lagern oder Ablagern von Abfall, bergbaulichen Rückständen oder tierischen Nebenprodukten,
19.  der Umgang mit radioaktiven Stoffen im Sinne des Atomgesetzes,
20.  das Errichten, Erweitern, Sanieren oder Betreiben von Abwasserkanälen oder -leitungen, ausgenommen Anlagen, die zur Entsorgung vorhandener Bebauung dienen und wenn hierbei die allgemein anerkannten Regeln der Technik eingehalten werden,
21.  das Errichten von Abwassersammelgruben,
22.  das Errichten, Aufstellen oder Verwenden von Trockentoiletten oder Chemietoiletten,
23.  das Einleiten oder Versickern von Niederschlagswasser im Sinne des § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in den Untergrund oder in das Grundwasser, ausgenommen das breitflächige Versickern von Niederschlagswasserabflüssen gering belasteter Herkunftsflächen im Sinne der Anlage 1 Nummer 4 über die belebte Bodenzone einer mindestens 20 Zentimeter mächtigen und bewachsenen Oberbodenschicht,
24.  das Errichten sowie der Um- oder Ausbau von Straßen, Wegen oder sonstigen Verkehrsflächen, ausgenommen
    1.  Baumaßnahmen an vorhandenen Straßen zur Anpassung an den Stand der Technik und zur Verbesserung der Verkehrssicherheit unter Einhaltung der allgemein anerkannten Regeln der Technik für bautechnische Maßnahmen an Straßen in Wasserschutzgebieten sowie
    2.  der Um- oder Ausbau von Geh- oder Radwegen mit breitflächiger Versickerung der Niederschlagswasserabflüsse über die belebte Bodenzone aus einer mindestens 20 Zentimeter mächtigen und bewachsenen Oberbodenschicht,
25.  das Errichten von öffentlichen Freibädern oder Zeltplätzen sowie Camping aller Art,
26.  das Errichten von Sportanlagen,
27.  das Abhalten oder Durchführen von Sportveranstaltungen, Märkten, Volksfesten oder Großveranstaltungen,
28.  das Errichten oder Erweitern von Baustelleneinrichtungen oder Baustofflagern,
29.  das Durchführen von Bohrungen, ausgenommen Maßnahmen zur Abwehr von Gefahren für das Grundwasser unter Beachtung der Sicherheitsvorkehrungen zum Grundwasserschutz,
30.  das Durchführen von unterirdischen Sprengungen,
31.  das Errichten oder Erweitern von baulichen Anlagen, ausgenommen Veränderungen in Gebäuden und Instandhaltungsmaßnahmen.

#### § 6 Schutz der Zone I

Die Verbote der Zonen III B, III A und II gelten auch in der Zone I.
In der Zone I sind außerdem verboten:

1.  das Betreten oder Befahren,
2.  landwirtschaft-, forstwirtschaft- oder gartenbauliche Nutzung,
3.  Veränderungen oder Aufschlüsse der Erdoberfläche.

#### § 7 Maßnahmen zur Wassergewinnung und -verteilung

Die Verbote des § 3 Nummer 20, 37 und 39, des § 5 Nummer 15, 19, 28 bis 31 sowie des § 6 Nummer 1 und 3 gelten nicht für Maßnahmen zur Wassergewinnung und -verteilung aus den Wasserfassungen, die durch diese Verordnung geschützt sind.

#### § 8 Widerruf von Befreiungen

(1) Befreiungen nach § 52 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes sind widerruflich und bedürfen der Schriftform.
Abweichend von Satz 1 ist eine Befreiung von den Verboten gemäß § 3 Nummer 56 und § 4 Nummer 14 und 15 nicht widerruflich.

(2) Im Fall des Widerrufs einer Befreiung kann die untere Wasserbehörde vom Grundstückseigentümer verlangen, dass der frühere Zustand wiederhergestellt wird, sofern es das Wohl der Allgemeinheit, insbesondere der Schutz der Wasserversorgung, erfordert.

#### § 9 Sicherung und Kennzeichnung des Wasserschutzgebietes

(1) Die Zone I ist vom Begünstigten auf Anordnung der unteren Wasserbehörde gegen unbefugtes Betreten, zum Beispiel durch eine Umzäunung, zu sichern.

(2) Der Begünstigte hat auf Anordnung der unteren Wasserbehörde zur Kennzeichnung des Wasserschutzgebietes im Bereich öffentlicher Verkehrsflächen bei der Straßenverkehrsbehörde die Anordnung des Richtzeichens 354 und des Vorschriftzeichens 269 zu beantragen und im Bereich nicht öffentlicher Flächen in Abstimmung mit der Gemeinde nicht amtliche Hinweiszeichen aufzustellen.

#### § 10 Duldungspflichten

(1) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet haben die Überwachung des Wasserschutzgebietes, insbesondere hinsichtlich der Beachtung dieser Verordnung und der nach ihr getroffenen Anordnungen, sowie das Beobachten der Gewässer und des Bodens durch die zuständigen Wasserbehörden, den Begünstigten oder deren Beauftragte zu dulden.

(2) Die Eigentümer oder Nutzungsberechtigten von Grundstücken im Wasserschutzgebiet sind auf Anordnung der unteren Wasserbehörde verpflichtet,

1.  das Errichten und Unterhalten von Einrichtungen zur Sicherung der Zone I gegen unbefugtes Betreten,
2.  das Aufstellen, Unterhalten und Beseitigen von Hinweis-, Warn-, Gebots- und Verbotszeichen,
3.  das Betreten und Befahren der Grundstücke durch Bedienstete der zuständigen Behörden, den Begünstigten oder deren Beauftragte zum Beobachten, Messen und Untersuchen des Grundwassers und zur Entnahme von Boden- und Vegetationsproben sowie
4.  das Anlegen und Betreiben von Grundwassermessstellen

zu dulden.
Die Anordnung erfolgt durch schriftlichen Bescheid gegenüber den betroffenen Eigentümern oder Nutzungsberechtigten.
Soweit bergrechtliche Belange berührt sind, ergeht die Entscheidung im Benehmen mit der zuständigen Bergbehörde.

(3) Auf Verlangen der unteren Wasserbehörde ist Einsicht in die Aufzeichnungen nach § 3 Nummer 1 Buchstabe c, Nummer 10 Buchstabe c und Nummer 11 Buchstabe c zu gewähren oder diese unverzüglich vorzulegen.

#### § 11 Übergangsregelung

(1) Für bei Inkrafttreten dieser Verordnung errichtete und betriebene Anlagen gilt das Verbot des Betreibens gemäß § 3 Nummer 3, 4 und 7 sowie § 4 Nummer 1 nach einem Jahr nach Inkrafttreten dieser Verordnung.

(2) Für bei Inkrafttreten dieser Verordnung bestehende Einleitungen oder Versickerungen von Niederschlagswasserabflüssen von mittel oder hoch belasteten Herkunftsflächen in den Untergrund ohne wasserrechtliche Erlaubnis gilt das Verbot des § 3 Nummer 40 nach einem Jahr nach Inkrafttreten dieser Verordnung.

#### § 12 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 103 Absatz 1 Nummer 7a des Wasserhaushaltsgesetzes handelt, wer vorsätzlich oder fahrlässig eine nach den §§ 3, 4, 5 oder 6 verbotene Handlung ohne eine Befreiung gemäß § 52 Absatz 1 Satz 2 des Wasserhaushaltsgesetzes vornimmt, ausgenommen das Verbot nach § 5 Nummer 16.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

#### § 13 Geltendmachung der Verletzung von Vorschriften

Eine Verletzung der in § 16 Absatz 1 und 3 des Brandenburgischen Wassergesetzes genannten Verfahrens- und Formvorschriften ist unbeachtlich, wenn sie nicht schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, innerhalb eines Jahres nach Inkrafttreten der Rechtsverordnung gegenüber dem Verordnungsgeber geltend gemacht worden ist.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten der Rechtsverordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 14 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig treten das mit Beschluss Nummer 123/23/83 vom 2.
März 1983 des Kreistages Fürstenwalde/Spree festgesetzte Trinkwasserschutzgebiet „Brunnenfassung Neu Zittau“ und das mit Beschluss Nummer 176/20/88 vom 8. Juni 1988 des Kreistages Fürstenwalde/Spree festgesetzte Trinkwasserschutzgebiet „Trinkwassergewinnungsanlage Neu Zittau“ außer Kraft.

Potsdam, den 21.
März 2019

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-1.pdf "Anlage 1") 235.1 KB

2

[Anlage 2](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-2.pdf "Anlage 2") 1.5 MB

3

[Anlage 3](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-3.pdf "Anlage 3") 5.6 MB

4

[Anlage 4 Blatt 1](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-01.pdf "Anlage 4 Blatt 1") 1.5 MB

5

[Anlage 4 Blatt 2](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-02.pdf "Anlage 4 Blatt 2") 1.6 MB

6

[Anlage 4 Blatt 3](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-03.pdf "Anlage 4 Blatt 3") 955.3 KB

7

[Anlage 4 Blatt 4](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-04.pdf "Anlage 4 Blatt 4") 753.8 KB

8

[Anlage 4 Blatt 5](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-05.pdf "Anlage 4 Blatt 5") 1.9 MB

9

[Anlage 4 Blatt 6](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-06.pdf "Anlage 4 Blatt 6") 2.0 MB

10

[Anlage 4 Blatt 7](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-07.pdf "Anlage 4 Blatt 7") 1.4 MB

11

[Anlage 4 Blatt 8](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-08.pdf "Anlage 4 Blatt 8") 967.6 KB

12

[Anlage 4 Blatt 9](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-09.pdf "Anlage 4 Blatt 9") 998.6 KB

13

[Anlage 4 Blatt 10](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-10.pdf "Anlage 4 Blatt 10") 851.1 KB

14

[Anlage 4 Blatt 11](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-11.pdf "Anlage 4 Blatt 11") 1.3 MB

15

[Anlage 4 Blatt 12](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-12.pdf "Anlage 4 Blatt 12") 1.4 MB

16

[Anlage 4 Blatt 13](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-13.pdf "Anlage 4 Blatt 13") 1.5 MB

17

[Anlage 4 Blatt 14](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-14.pdf "Anlage 4 Blatt 14") 1.4 MB

18

[Anlage 4 Blatt 15](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-15.pdf "Anlage 4 Blatt 15") 1.3 MB

19

[Anlage 4 Blatt 16](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-16.pdf "Anlage 4 Blatt 16") 840.5 KB

20

[Anlage 4 Blatt 17](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-17.pdf "Anlage 4 Blatt 17") 926.9 KB

21

[Anlage 4 Blatt 18](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-18.pdf "Anlage 4 Blatt 18") 954.4 KB

22

[Anlage 4 Blatt 19](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-19.pdf "Anlage 4 Blatt 19") 1.2 MB

23

[Anlage 4 Blatt 20](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-20.pdf "Anlage 4 Blatt 20") 878.0 KB

24

[Anlage 4 Blatt 21](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-21.pdf "Anlage 4 Blatt 21") 883.8 KB

25

[Anlage 4 Blatt 22](/br2/sixcms/media.php/68/GVBl_II_24_2019-Anlage-4-Blatt-22.pdf "Anlage 4 Blatt 22") 855.9 KB