## Verordnung über das Naturschutzgebiet „Dolgenseen-Ragollinsee“

Auf Grund der §§ 22, 23 und § 32 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl. I S. 2542) in Verbindung mit § 19 Absatz 1 und 2 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S.
350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1  
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Uckermark wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Dolgenseen-Ragollinsee“.

### § 2  
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 431 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Stadt:**

**Gemarkung:**

**Flur:**

Templin

Herzfelde

2;

Templin

Klosterwalde

4, 5;

Templin

Petznick

1, 3, 4.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.
Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 2 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 3 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 3 Nummer 2 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 3 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 3 Nummer 3 mit den Blattnummern 1 bis 8 aufgeführten Liegenschaftskarten.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Uckermark, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

### § 3  
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Rotbuchenwälder, der Eichen-Hainbuchenwälder, der Erlen-Bruchwaldgesellschaften, der Gesellschaften natürlich eutropher Seen mit ihren Sumpf- und Verlandungszonen, der Quell-, Seggen- und Röhrichtmoore, der Feucht- und Frischwiesen sowie der strukturell und hydrologisch verschieden ausgeprägten Stand- und Fließgewässer einer Moorseenkette;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Krebsschere (Stratiotes aloides), Ähriger Ehrenpreis (Veronica spicata), Warnstorfs Torfmoos (Sphagnum warnstorfii), Rentierflechte (Cladonia rangiferina), Faden-Segge (Carex lasiocarpa), Drachenwurz (Calla palustris), Wiesen-Kuhschelle (Pulsatilla pratensis) und Kümmel-Silge (Selinum carvifolia);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Rohrweihe (Circus aeruginosus), Große Rohrdommel (Botaurus stellaris), Eisvogel (Alcedo atthis), Kranich (Grus grus), Neuntöter (Lanius collurio), Schwarzspecht (Dryocopus martius), Weißstorch (Ciconia ciconia), Zwergschnäpper (Ficedula parva), Schellente (Bucephala clangula), Heidelerche (Lullula arborea), Laubfrosch (Hyla arborea), Moorfrosch (Rana arvalis), Komma-Dickkopffalter (Hesperia comma) und Sumpfhornkleewidderchen (Zygaena trifolii);
4.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit einer weitgehend unzerschnittenen, reich gegliederten und störungsarmen Landschaft typisch eiszeitlicher Prägung, gekennzeichnet durch einen außergewöhnlichen Reichtum an Seen, Moor- und Feuchtwäldern und durch den Wechsel von Wald und Offenland, Alleen, Hecken, Vorwäldern, Gebüschen und Obstgehölzen;
5.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Biotopverbundes zwischen dem Europäischen Vogelschutzgebiet „Uckermärkische Seenlandschaft“ und den Gebieten von gemeinschaftlicher Bedeutung „Platkowsee-Netzowsee-Metzelthin“, „Kuhzer See/Jakobshagen“ sowie dem Biosphärenreservat „Schorfheide-Chorin“.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Dolgenseen-Ragollinsee“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), das ehemals einen Teil des Gebietes von gemeinschaftlicher Bedeutung „Platkowsee-Netzowsee-Metzelthin“ umfasste, mit seinen Vorkommen von

1.  Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Mageren Flachland-Mähwiesen (Alopecurus pratensis, Sanguisorba officinalis), Waldmeister-Buchenwald (Asperulo-Fagetum) als natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Trockenen, kalkreichen Sandrasen als prioritärem natürlichen Lebensraumtyp im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
3.  Biber (Castor fiber), Bitterling (Rhodeus amarus), Fischotter (Lutra lutra), Großem Mausohr (Myotis myotis) und Großer Moosjungfer (Leucorrhinia pectoralis) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist außerhalb von Röhrichten, Bruchwäldern, Feuchtwiesen und Mooren das Betreten zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 10 jeweils nach dem 30.
    Juni eines jeden Jahres;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der Wege, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können und außerhalb der Waldbrandwundstreifen zu reiten;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  zu baden oder zu tauchen;
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen; ausgenommen ist das Befahren der Gewässer mit muskelkraftbetriebenen Booten außerhalb von Röhrichten, Schwimmblattgesellschaften oder Verlandungsbereichen.
    Das Einsetzen und Anlegen der muskelkraftbetriebenen Boote ist nur an den in den Karten zu dieser Verordnung nach § 2 (Kartenskizze, Topografische Karte im Maßstab 1 : 10 000, Blattnummern 1 und 2 und Liegenschaftskarte, Blattnummern 2, 4 und 6) eingezeichneten Uferabschnitten zulässig;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle, Rückstände aus Biogasanlagen) und Sekundärrohstoffdünger (zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen, wie zum Beispiel Schlempe) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

### § 5  
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Großvieheinheiten (GVE) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und vergleichbare Rückstände aus Biogasanlagen oder Sekundärrohstoffdünger, wie zum Beispiel solche aus Abwasser, Klärschlamm oder Bioabfälle, einzusetzen,
    2.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt.
        Bei Narbenschäden ist eine umbruchlose Nachsaat zulässig;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  der in § 3 Absatz 2 Nummer 2 genannte Lebensraumtyp „Moorwälder“ auf den Flurstücken 104, 116, 117, Flur 4 der Gemarkung Petznick, nicht bewirtschaftet wird, im Übrigen eine Nutzung der Laubwälder einzelstamm- bis truppweise erfolgt,
    2.  in Misch- und Nadelwäldern Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von maximal 0,5 Hektar zulässig sind,
    3.  nur Baumarten der potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Baumarten in gesellschaftstypischen Anteilen unter Ausschluss eingebürgerter Arten zu verwenden sind,
    4.  der Boden unter Verzicht auf Pflügen bearbeitet wird; ausgenommen ist eine streifenweise, flachgründige, nicht in den Mineralboden eingreifende Bodenverwundung zur Unterstützung von Verjüngungsmaßnahmen,
    5.  das Befahren des Waldes nur auf Wegen oder Rückegassen erfolgt,
    6.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    7.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 35 Zentimeter Durchmesser in 1, 30 Meter Höhe über dem Stammfuß und einer Mindesthöhe von 5 Metern nicht gefällt werden und liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimeter am stärksten Ende) im Bestand verbleibt,
    8.  ein Altholzanteil von mindestens 10 Prozent am aktuellen Bestandesvorrat zu sichern ist, wobei, sofern vorhanden, mindestens fünf Stämme je Hektar mit einem Brusthöhendurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum Absterben aus der Nutzung genommen sein müssen,
    9.  § 4 Absatz 2 Nummer 17 und 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  der Besatz mit Karpfen, ausgenommen des Mittleren Dolgensees, Kleinen Dolgensees und Klosterwalder Teiches, untersagt ist.
        Für diese Seen ist ein Hegeplan zu erstellen, der insbesondere den Karpfenbesatz regelt und spätestens zwei Jahre nach Inkrafttreten dieser Verordnung in Kraft tritt.
        Der Hegeplan ist einvernehmlich mit der Fachbehörde für Naturschutz abzustimmen.
        Bis zum Inkrafttreten des Hegeplanes ist ein einmaliger Karpfenbesatz mit bis zu 25 Kilogramm pro Hektar bis zur Abfischung des Bestandes innerhalb eines Zeitraumes von drei bis fünf Jahren zulässig.
        Eine Erhöhung durch den Hegeplan ist nicht zulässig,
    2.  § 4 Absatz 2 Nummer 19 gilt,
    3.  Fanggeräte und Fangmittel so eingesetzt oder ausgestattet werden, dass eine Gefährdung des Fischotters und des Bibers weitgehend ausgeschlossen ist,
    4.  die in § 3 Absatz 2 Nummer 3 genannten Fischarten ganzjährig geschont sind;
4.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass
    1.  diese von den rechtmäßig errichteten Stegen und von Booten aus sowie darüber hinaus am Klosterwalder Teich, Ragollinsee, Mittleren Dolgensee und Kleinen Dolgensee von den in den Karten zu dieser Verordnung nach § 2 (Kartenskizze, Topografische Karte im Maßstab 1 : 10 000, Blattnummern 1 und 2 und Liegenschaftskarte, Blattnummern 2, 4 und 6) eingezeichneten Uferabschnitten aus beziehungsweise am Kleinen Dolgensee vom Südufer aus zulässig ist,
    2.  § 4 Absatz 2 Nummer 13, 19 und 20 gilt;
5.  für den Bereich der Jagd:
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass  
        aa) die Fallenjagd mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern zum Gewässerufer verboten ist, Ausnahmen von der Einhaltung dieses Abstandes kann die untere Naturschutzbehörde erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,  
        bb) keine Baujagd in einem Abstand von bis zu 100 Metern vom Ufer aller innerhalb des Schutzgebietes liegenden Gewässer vorgenommen wird,
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    3.  das Aufstellen transportabler und mobiler Ansitzeinrichtungen,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und des in § 3 Absatz 2 Nummer 1 genannten Lebensraumtyps „Magere Flachland-Mähwiesen“.Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern sind unzulässig, im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Brandenburgischen Jagdgesetzes unberührt;
6.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
7.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
8.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
9.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
10.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch nach dem  
    30.
    Juni eines jeden Jahres;
11.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
12.  Schutz-, Pflege- und Entwicklungsmaßnahmen, unter anderem Maßnahmen zur Bekämpfung der Spätblühenden Traubenkirsche, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
13.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 an Straßen und Wegen freigestellt;
14.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist; das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

### § 6  
Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  in der Rinne zwischen Ragollinsee und Dolgensee sowie in der Ablaufrinne zwischen Dolgensee und Großem Dolgensee soll durch Sohlschwellen beziehungsweise durch eingebrachte Hindernisse (Baumstämme) eine Abflusshemmung erzielt werden;
2.  der Abfluss des Moores südwestlich des Ragollinsees soll verschlossen werden;
3.  am Ablauf des Klosterwalder Teiches in Richtung Kleiner Dolgensee soll der Sohlabsturz durch eine Sohlgleite ersetzt werden;
4.  zum Schutz vor der Zurückdrängung indigener Pflanzen sollen Maßnahmen beispielsweise zur Bekämpfung der Spätblühenden Traubenkirsche eingeleitet werden;
5.  die nadelholzdominierten Forsten südlich der Klosterwalder Mühle sollen im Rahmen von Durchforstungs- und Waldumbaumaßnahmen langfristig in naturnah strukturierte Rotbuchenwälder bodensaurer beziehungsweise lehmiger Standorte umgewandelt werden.

### § 7  
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

### § 8  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

### § 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit  
§ 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere §§ 31 bis 33 und § 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

### § 10  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

### § 11  
Inkrafttreten

§ 5 Absatz 1 Nummer 1 dieser Verordnung tritt am 1.
Juli 2012 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 9.
Mai 2012

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

* * *

**Anlage 1**  
(zu § 2 Absatz 1)

![Das rund 431 Hektar große Naturschutzgebiet "Dolgensee-Ragolinsee" liegt in der Stadt Templin, Gemarkungen Herzfelde, Klosterwalde und Petznick.](/br2/sixcms/media.php/69/Nr.36-1.GIF "Das rund 431 Hektar große Naturschutzgebiet "Dolgensee-Ragolinsee" liegt in der Stadt Templin, Gemarkungen Herzfelde, Klosterwalde und Petznick.")

**Anlage 2**  
(zu § 2 Absatz 1)

### Flurstücksliste zur Verordnung über das Naturschutzgebiet „Dolgenseen-Ragollinsee“

**Landkreis: Uckermark**

**Stadt: Templin**

Gemarkung

Flur

Flurstück

Herzfelde

2

109, 113 bis 115, 118, 122, 125 bis 130, 132 teilweise, 134 bis 136, 144 bis 153, 161 teilweise, 162, 163, 165, 166, 294 bis 315;

 

 

 

Klosterwalde

4

139, 141 bis 145, 147 teilweise, 155 bis 157, 161 bis 163, 169 teilweise, 218, 219 teilweise, 220, 222, 223 teilweise, 224, 225, 226 teilweise, 227 bis 233;

 

5

107, 108/1, 108/2 teilweise, 111 bis 116, 117/1, 118 bis 120, 121 teilweise, 123 teilweise, 124 bis 129, 134/1 teilweise, 135, 136, 138/1, 138/2, 139/2, 140 bis 142, 143/2, 144/2, 144/3, 145/1, 149/1, 149/2 teilweise, 156, 157, 158/1 teilweise, 170 teilweise, 180, 181, 182/3, 182/4, 182/5, 182/6, 182/7, 183/3, 183/4, 183/5, 183/6, 183/7, 184, 190, 193, 194, 196/1, 196/2, 196/3, 197, 201, 220 bis 222, 230, 231, 239 teilweise, 241 bis 243, 244 teilweise, 245 bis 291, 293;

 

 

 

Petznick

1

4 teilweise, 202 teilweise, 203 bis 205;

 

3

91 teilweise, 111, 484, 485 teilweise;

 

4

1 bis 12, 42 bis 47, 54 teilweise, 55, 75 bis 78 jeweils teilweise, 82 teilweise, 83 teilweise, 86 teilweise, 97, 104 teilweise, 113 teilweise, 116 bis 123, 124/1, 125 bis 144, 146 bis 166, 168, 169, 180, 181.

**Anlage 3**  
(zu § 2 Absatz 2)

### 1.
Übersichtskarte im Maßstab 1 : 30 000

**Titel:**

Übersichtskarte zur Verordnung über das Naturschutzgebiet „Dolgenseen-Ragollinsee“

Lfd.
Nummer

Unterzeichnung

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV), am 21. März 2012

### 2.
Topografische Karten im Maßstab 1 : 10 000

**Titel:**

Topografische Karte zur Verordnung über das Naturschutzgebiet „Dolgenseen-Ragollinsee“

Lfd.
Nummer

Kartenblatt

Unterzeichnung

01

2847-NW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 21. März 2012

02

2847-NO

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 21. März 2012

03

2847-SW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 21. März 2012

### 3.
Liegenschaftskarten

**Titel:**

Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Dolgenseen-Ragollinsee“

Lfd.
Nummer

Gemarkung

Flur

Unterzeichnung

01

Herzfelde  
Petznick

2  
1, 4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 21.
März 2012

02

Herzfelde  
Petznick

2  
1, 4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 21.
März 2012

03

Klosterwalde  
Petznick

5  
4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 21.
März 2012

04

Herzfelde  
Klosterwalde  
Petznick

2  
5  
1, 4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 21.
März 2012

05

Klosterwalde

4, 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 21.
März 2012

06

Klosterwalde  
Petznick

5  
3, 4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 21.
März 2012

07

Petznick

4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 21.
März 2012

08

Klosterwalde

5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 21.
März 2012