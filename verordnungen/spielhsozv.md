## Verordnung über Inhalt und Form des Sozialkonzeptes und die Anerkennung von Schulungsangeboten zur Früherkennung beim gewerblichen Spiel in Spielhallen (Spielhallensozialkonzeptverordnung - SpielhSozV)

Auf Grund des § 5 Absatz 2 des Brandenburgischen Spielhallengesetzes vom 23.
Juni 2021 (GVBl. I Nr. 22 S. 8) verordnet die Ministerin für Soziales, Gesundheit, Integration und Verbraucherschutz mit Zustimmung des Ministers des Innern und für Kommunales und des Ministers für Wirtschaft, Arbeit und Energie:

#### § 1 Sozialkonzept

(1) Das Sozialkonzept nach § 5 Absatz 1 Satz 2 des Brandenburgischen Spielhallengesetzes hat über die gesetzlich vorgeschriebenen Mindestinhalte hinaus Erläuterungen zu beinhalten,

1.  wie die in § 5 Absatz 1 Satz 2 Nummer 2 und 4 bis 8 des Brandenburgischen Spielhallengesetzes genannten Maßnahmen umgesetzt werden,
2.  wie die Sensibilisierung des Aufsichtspersonals auf die in § 1 des Glücksspielstaatsvertrages 2021 genannten Ziele konkret erfolgt,
3.  wie konkrete Ansprachen an von problematischem Spielverhalten oder von Spielsucht betroffene Spielerinnen und Spieler erfolgen.

(2) Das Sozialkonzept ist in schriftlicher Form bezogen auf die jeweilige Spielhalle zu erstellen.
Es ist von der Betreiberin oder dem Betreiber der Spielhalle zu unterschreiben und den Beschäftigten der Spielhalle zur Kenntnis zu geben.
Die Kenntnisnahme ist von den Beschäftigten schriftlich zu bestätigen.
Dies ist zu dokumentieren.

(3) Änderungen des Sozialkonzepts sind der Erlaubnisbehörde im Rahmen der Berichterstattung nach § 9 Absatz 1 des Brandenburgischen Spielhallengesetzes anzuzeigen.

#### § 2 Inhalt und Umfang der Schulungen

(1) Die Schulungen nach § 5 Absatz 1 Satz 2 Nummer 3 des Brandenburgischen Spielhallengesetzes müssen über die gesetzlich vorgeschriebenen Mindestinhalte hinaus insbesondere folgende Grundlagen und Handlungskompetenzen vermitteln:

1.  Gefährdungspotential und Risikomerkmale von Geldspielgeräten,
2.  anbieterunabhängige Informations-, Beratungs- und Therapieangebote für Betroffene und deren Angehörige,
3.  proaktive Ansprache und Gesprächsführung mit Personen mit auffälligem Spielverhalten,
4.  Verhalten in kritischen Situationen.

(2) Die Schulungsdauer muss mindestens acht Zeitstunden betragen.
Die Zahl der Teilnehmenden einer Schulung soll 15 Personen nicht überschreiten.

(3) Die Teilnahme gilt als erfolgreich, wenn die Schulungsanbieterin oder der Schulungsanbieter bescheinigt, dass die oder der Teilnehmende ohne Fehlzeiten teilgenommen hat und sie oder er sich in geeigneter Weise davon überzeugt hat, dass die oder der Teilnehmende mit den Inhalten nach Absatz 1 vertraut ist.

(4) Die Schulung ist alle zwei Jahre zu wiederholen.
Für die Wiederholungsschulung gelten die Absätze 1 bis 3 mit der Maßgabe, dass die Schulungsdauer mindestens vier Zeitstunden beträgt.

#### § 3 Anerkennungsvoraussetzungen für Schulungsangebote

Schulungsangebote zur Früherkennung problematischen und pathologischen Spielverhaltens sind im Verfahren nach § 4 anzuerkennen, wenn

1.  die Voraussetzungen zu Inhalt und Umfang gemäß § 2 vorliegen und
2.  die Schulungsanbieterin oder der Schulungsanbieter auf Grund entsprechender Erklärungen und Nachweise die Gewähr dafür bietet, dass sie oder er
    1.  Erfahrungen mit der Durchführung von Schulungen im Rahmen von Aus- und Fortbildungen besitzt,
    2.  die Schulung durch fachlich qualifizierte Dozentinnen und Dozenten durchgeführt wird, welche auf Grund mindestens zweijähriger praktischer Erfahrungen und Aktivitäten in der Suchtprävention oder Suchtberatung in der Lage sind, die nach § 2 Absatz 1 erforderlichen Inhalte erfolgreich an die zu schulenden Personen zu vermitteln,
    3.  keine wirtschaftliche Unterstützung durch Betreiberinnen und Betreiber von Spielhallen, Automatenaufstellerinnen und -aufsteller oder andere Veranstalterinnen und Veranstalter von Glücksspielen erhält und nicht in finanzieller Abhängigkeit zu ihnen steht.

#### § 4 Anerkennungsverfahren für Schulungsangebote

(1) Schulungsangebote werden auf schriftlichen Antrag bei Vorliegen der Voraussetzungen nach § 3 durch das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit anerkannt.
Dem Antrag sind entsprechende Nachweise und Erklärungen beizufügen.

(2) Anerkannte Schulungsangebote werden in eine Liste aufgenommen, die auf der Internetseite des Landesamts für Arbeitsschutz, Verbraucherschutz und Gesundheit veröffentlicht wird.

(3) Wird bekannt, dass die in § 3 beschriebenen Anforderungen nicht mehr vorliegen, ist die Anerkennung zu widerrufen.

#### § 5 Zuständigkeit der örtlichen Ordnungsbehörden

Zuständige Behörden im Sinne von § 5 Absatz 1 Satz 2 Nummer 10 des Brandenburgischen Spielhallengesetzes sind die örtlichen Ordnungsbehörden.

#### § 6 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt vier Wochen nach der Verkündung in Kraft.
Gleichzeitig tritt die Spielhallensozialkonzeptverordnung vom 22.
Oktober 2014 (GVBl.
II Nr.
79), die durch Artikel 2 Absatz 14 des Gesetzes vom 25.
Januar 2016 (GVBl.
I Nr.
5) geändert worden ist, außer Kraft.

Potsdam, den 14.
Januar 2022

Die Ministerin für Soziales, Gesundheit, Integration und Verbraucherschutz

Ursula Nonnemacher