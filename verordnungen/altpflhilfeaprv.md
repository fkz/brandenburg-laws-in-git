## Ausbildungs- und Prüfungsverordnung für den Beruf der Altenpflegehelferin und des Altenpflegehelfers im Land Brandenburg (Altenpflegehilfe-Ausbildungs-Prüfungsverordnung - AltPflHilfeAPrV)

Auf Grund des § 7 des Brandenburgischen Altenpflegehilfegesetzes vom 27.
Mai 2009 (GVBl. I S. 154), der durch Artikel 7 des Gesetzes vom 15.
Juli 2010 (GVBl.
I Nr. 28) geändert worden ist, verordnet der Minister für Arbeit, Soziales, Frauen und Familie:

Inhaltsübersicht
----------------

[§ 1 Gliederung der Ausbildung](#1)  
[§ 2 Praktische Ausbildung](#2)  
[§ 3 Staatliche Prüfung](#3)  
[§ 4 Prüfungsausschuss](#4)  
[§ 5 Zulassung zur Prüfung](#5)  
[§ 6 Niederschriften](#6)  
[§ 7 Benotung](#7)  
[§ 8 Bestehen und Wiederholung der Prüfung, Zeugnis](#8)  
[§ 9 Rücktritt von der Prüfung](#9)  
[§ 10 Versäumnisfolgen, Unterbrechung der Prüfung](#10)  
[§ 11 Ordnungsverstöße und Täuschungsversuche](#11)  
[§ 12 Prüfungsunterlagen](#12)  
[§ 12a Schriftlicher Teil der Prüfung](#12a)  
[§ 13 Mündlicher Teil der Prüfung](#13)  
[§ 14 Praktischer Teil der Prüfung](#14)  
[§ 15 Prüfung für Nichtschülerinnen und Nichtschüler](#15)  
[§ 16 Antragstellung und Zulassung](#16)  
[§ 17 Besondere Verfahrensvorschriften für die Prüfung für Nichtschülerinnen und Nichtschüler](#17)  
[§ 18 Erlaubnisurkunde](#18)  
[§ 19 Inkrafttreten](#19)

[Anlage 1 Theoretischer und praktischer Unterricht sowie praktische Ausbildung in der Altenpflegehilfe](#Anlage 1)  
[Anlage 2 Bescheinigung über die Teilnahme an den Ausbildungsveranstaltungen](#Anlage 2)  
[Anlage 3 Zeugnis über die staatliche Prüfung](#Anlage 3)  
[Anlage 4 Urkunde über die Erlaubnis zur Führung der Berufsbezeichnung](#Anlage 4)

### § 1  
Gliederung der Ausbildung

(1) Die einjährige Ausbildung in der Altenpflegehilfe umfasst mindestens den in der Anlage 1 aufgeführten theoretischen und praktischen Unterricht von 750 Stunden und die aufgeführte praktische Ausbildung von mindestens 900 Stunden.
Der Rahmenplan für den theoretischen und den praktischen Unterricht und die praktische Ausbildung in der Altenpflegehilfe im Land Brandenburg ist in der jeweils geltenden Fassung umzusetzen.

(2) Von den 900 Stunden der praktischen Ausbildung entfallen mindestens 700 Stunden auf die Ausbildung in einer Pflegeeinrichtung nach § 3 Absatz 3 des Brandenburgischen Altenpflegehilfegesetzes, mit der ein Ausbildungsverhältnis besteht.
Bis zu 200 Stunden praktische Ausbildung sind in der jeweils anderen Pflegeeinrichtung nach § 3 Absatz 3 des Brandenburgischen Altenpflegehilfegesetzes zu vermitteln.

(3) Die Ausbildung erfolgt im Wechsel von Abschnitten des Unterrichts und der praktischen Ausbildung.
Der zeitliche Umfang der Ausbildungsabschnitte muss mindestens zwei Wochen umfassen und soll vier Wochen nicht überschreiten.
Die Ausbildung in der Teilzeitform kann nach abweichenden zeitlichen Regelungen erfolgen.

(4) Die regelmäßige und erfolgreiche Teilnahme an den Ausbildungsveranstaltungen nach Absatz 1 ist durch eine Bescheinigung nach dem Muster der Anlage 2 nachzuweisen.
Der Jahresurlaub ist in der unterrichtsfreien Zeit zu gewähren.

(5) Auf die Dauer einer Ausbildung nach Absatz 1 werden Unterbrechungen bis zu jeweils 10 Prozent des theoretischen und praktischen Unterrichts und bis zu 10 Prozent der praktischen Ausbildung angerechnet, wenn diese Unterbrechung nachweislich durch Krankheit, Schwangerschaft oder aus anderen nicht von der Schülerin oder dem Schüler zu vertretenden Gründen entstanden ist.
Dazu ist ein geeigneter Nachweis, zum Beispiel in Form einer ärztlichen Bescheinigung, vorzulegen.
Die Unterbrechung innerhalb der praktischen Ausbildung darf sich nicht ausschließlich auf den Abschnitt der praktischen Ausbildung erstrecken, der einen deutlich kleineren zeitlichen Umfang hat, zum Beispiel die praktische Ausbildung in der ambulanten Pflegeeinrichtung.
Sie darf höchstens die Hälfte der nach Satz 1 zugelassenen Unterbrechung betragen.
Soweit eine besondere Härte vorliegt, kann eine über Satz 1 hinausgehende zeitliche Unterbrechung auf Antrag durch die zuständige Behörde angerechnet werden, sofern zu erwarten ist, dass das Ausbildungsziel dennoch erreicht werden kann.
Im Antrag ist zu begründen, worin die besondere Härte besteht und wie das Ausbildungsziel erreicht werden soll.

### § 2  
Praktische Ausbildung

(1) Während der praktischen Ausbildung nach § 1 Absatz 1 sind die Kenntnisse, Fähigkeiten und Fertigkeiten zu vermitteln, die zur Erreichung des Ausbildungsziels nach § 2 des Brandenburgischen Altenpflegehilfegesetzes erforderlich sind.
Es ist Gelegenheit zu geben, die im Unterricht erworbenen Kenntnisse, Fähigkeiten und Fertigkeiten zu vertiefen und zu lernen, sie bei der beruflichen Tätigkeit anzuwenden.

(2) Die Einrichtungen der praktischen Ausbildung im Sinne des § 3 Absatz 3 Nummer 1 und 2 des Brandenburgischen Altenpflegehilfegesetzes stellen die Praxisanleitung der Schülerinnen und Schüler nach § 3 Absatz 4 des Brandenburgischen Altenpflegehilfegesetzes durch geeignete Fachkräfte sicher.
Geeignet ist eine Altenpflegerin oder ein Altenpfleger mit mindestens zweijähriger Berufserfahrung in der Altenpflege in den letzten vier Jahren und der Befähigung zur Praxisanleitung.
Geeignet ist auch eine Gesundheits- und Krankenpflegerin oder ein Gesundheits- und Krankenpfleger mit mindestens zweijähriger Berufserfahrung in der Altenpflege in den letzten vier Jahren und der Befähigung zur Praxisanleitung.
Altenpflegerinnen oder Altenpfleger sind vorrangig für die Praxisanleitung einzusetzen.
Sofern eine Gesundheits- und Krankenpflegerin oder ein Gesundheits- und Krankenpfleger als Praxisanleitung tätig werden soll, ist dies der zuständigen Behörde anzuzeigen und kurz zu begründen.
Die Befähigung zur Praxisanleitung in der Altenpflege ist durch eine geeignete berufspädagogische Zusatzqualifikation im Umfang von mindestens 200 Stunden nachzuweisen.
Das für Soziales zuständige Mitglied der Landesregierung behält sich vor, Einzelheiten zu den Anforderungen an die berufspädagogische Qualifikation zu regeln.

(3) Aufgabe der Praxisanleitung ist es, die Schülerinnen und Schüler schrittweise an die Wahrnehmung der beruflichen Aufgaben heranzuführen und die Verbindung mit der Altenpflegeschule zu gewährleisten.
Die Praxisanleitung erfolgt auf der Grundlage eines Ausbildungsplans, den die Einrichtung der praktischen Ausbildung in Abstimmung mit der Altenpflegeschule erstellt.

(4) Die Altenpflegeschule stellt die Praxisbegleitung der Schülerinnen und Schüler in den Einrichtungen der praktischen Ausbildung nach § 3 Absatz 3 Nummer 1 und 2 des Brandenburgischen Altenpflegehilfegesetzes sicher.
Aufgabe der praxisbegleitenden Lehrkräfte ist es, die Schülerinnen und Schüler in den Einrichtungen durch regelmäßige Besuche zu betreuen und zu beurteilen sowie die für die Praxisanleitung zuständigen Fachkräfte zu beraten.

(5) Die ausbildende Einrichtung erstellt über den bei ihr durchgeführten Ausbildungsabschnitt eine Bescheinigung.
Die Bescheinigung muss Angaben enthalten über die Dauer der Ausbildung, die Ausbildungsbereiche, die vermittelten Kenntnisse, Fähigkeiten und Fertigkeiten und über Fehlzeiten der Schülerin oder des Schülers.
Die Bescheinigung ist der Altenpflegeschule sofort nach Abschluss des Ausbildungsabschnitts vorzulegen.
Der Träger der praktischen Ausbildung nach § 8 des Altenpflegehilfegesetzes und die Schülerin oder der Schüler erhalten Abschriften.

### § 3  
Staatliche Prüfung

(1) Die staatliche Prüfung für die Ausbildung nach § 1 Absatz 1 umfasst einen schriftlichen, einen mündlichen und einen praktischen Teil.

(2) Der schriftliche und der mündliche Teil der Prüfung werden an der Altenpflegeschule abgelegt, an der die Ausbildung abgeschlossen wird.

(3) Die zuständige Behörde kann von der Regelung nach Absatz 2 aus wichtigem Grund Ausnahmen zulassen.
Die vorsitzenden Mitglieder der beteiligten Prüfungsausschüsse sind vorher zu hören.

(4) Der praktische Teil der Prüfung wird abgelegt:

1.  in einer Einrichtung nach § 3 Absatz 3 Nummer 1 des Brandenburgischen Altenpflegehilfegesetzes, in der die Schülerin oder der Schüler ausgebildet worden ist, oder
2.  in der Wohnung einer pflegebedürftigen Person, die von einer Einrichtung nach § 3 Absatz 3 Nummer 2 des Brandenburgischen Altenpflegehilfegesetzes betreut wird und diese Einrichtung die Schülerin oder den Schüler ausgebildet hat.

### § 4  
Prüfungsausschuss

(1) An jeder Altenpflegeschule wird ein Prüfungsausschuss gebildet, der für die ordnungsgemäße Durchführung der Prüfung verantwortlich ist.
Er besteht aus folgenden Mitgliedern:

1.  einer fachlich geeigneten Vertreterin oder einem fachlich geeigneten Vertreter der zuständigen Behörde oder im Ausnahmefall einer von der zuständigen Behörde mit der Wahrnehmung dieser Aufgabe betrauten fachlich geeigneten Person als vorsitzendes Mitglied,
2.  der Leiterin oder dem Leiter der Altenpflegeschule,
3.  Lehrkräften als Fachprüferinnen oder Fachprüfer der Schule und
4.  einer Fachprüferin oder einem Fachprüfer der jeweiligen Praxiseinrichtung für die praktische Prüfung, die oder der als Praxisanleitung nach § 2 Absatz 2 Satz 2 tätig ist.

Als Fachprüferinnen oder Fachprüfer sollen die Lehrkräfte und Personen als Praxisanleitung bestellt werden, die die Schülerin oder den Schüler überwiegend ausgebildet haben.

(2) Die zuständige Behörde bestellt die Mitglieder nach Absatz 1 sowie ihre Stellvertreterinnen und Stellvertreter.
Für jedes Mitglied ist mindestens eine Stellvertreterin oder ein Stellvertreter zu bestimmen.
Die Mitglieder nach Absatz 1 Nummer 3 und 4 und ihre Stellvertreterinnen oder Stellvertreter werden auf Vorschlag der Schulleitung bestimmt.

(3) Die zuständige Behörde kann Sachverständige und Beobachterinnen und Beobachter zur Teilnahme an allen Prüfungen entsenden.

### § 5  
Zulassung zur Prüfung

(1) Das vorsitzende Mitglied des Prüfungsausschusses entscheidet auf schriftlichen Antrag der Schülerin oder des Schülers über die Zulassung zur Prüfung und setzt die Prüfungstermine im Benehmen mit der Schulleitung fest.
Der Prüfungsbeginn soll nicht früher als sechs Wochen vor dem Ende der Ausbildung liegen.

(2) Die Zulassung zur Prüfung wird erteilt, wenn folgende Nachweise vorliegen:

1.  der Personalausweis oder der Reisepass in amtlich beglaubigter Form und
2.  die Bescheinigung nach § 1 Absatz 4 über die Teilnahme an den Ausbildungsveranstaltungen.

(3) Die Zulassung sowie die Prüfungstermine sollen spätestens zwei Wochen vor Prüfungsbeginn schriftlich mitgeteilt werden.

(4) Die besonderen Belange von Prüflingen mit Behinderung sind zur Wahrung ihrer Chancengleichheit bei Durchführung der Prüfungen zu berücksichtigen.

### § 6  
Niederschriften

Über die Prüfung ist eine Niederschrift zu fertigen, aus der Gegenstand, Ablauf und Ergebnisse der Prüfung und etwa vorkommende Unregelmäßigkeiten hervorgehen.

### § 7  
Benotung

Die Leistungen der schriftlichen, der mündlichen und praktischen Prüfung werden wie folgt bewertet:

1.  „sehr gut“ (1),  
    wenn die Leistung den Anforderungen in besonderem Maße entspricht,
2.  „gut“ (2),  
    wenn die Leistung den Anforderungen voll entspricht,
3.  „befriedigend“ (3),  
    wenn die Leistung im Allgemeinen den Anforderungen entspricht,
4.  „ausreichend“ (4),  
    wenn die Leistung zwar Mängel aufweist, aber im Ganzen den Anforderungen noch entspricht,
5.  „mangelhaft“ (5),  
    wenn die Leistung den Anforderungen nicht entspricht, jedoch erkennen lässt, dass die notwendigen Grundkenntnisse vorhanden sind und die Mängel in absehbarer Zeit behoben werden können,
6.  „ungenügend“ (6),  
    wenn die Leistung den Anforderungen nicht entspricht und selbst die Grundkenntnisse so lückenhaft sind, dass die Mängel in absehbarer Zeit nicht behoben werden können.

### § 8  
Bestehen und Wiederholung der Prüfung, Zeugnis

(1) Die Prüfung ist bestanden, wenn jeder der nach § 3 Absatz 1 vorgeschriebenen Prüfungsteile mit mindestens „ausreichend“ bewertet wurde.

(2) Über die bestandene staatliche Prüfung wird ein Zeugnis nach dem Muster der Anlage 3 erteilt.
Über die nicht bestandene staatliche Prüfung erhält die Schülerin oder der Schüler von dem vorsitzenden Mitglied des Prüfungsausschusses eine schriftliche Mitteilung, in der die Prüfungsnoten anzugeben sind.

(3) Die schriftliche Prüfung, die mündliche Prüfung und die praktische Prüfung können jeweils einmal wiederholt werden, wenn die Note „mangelhaft“ oder „ungenügend“ vergeben wurde.
Hat die Schülerin oder der Schüler den praktischen Teil der Prüfung oder mehr als einen Teil der staatlichen Prüfung zu wiederholen, so darf die Zulassung zur Wiederholungsprüfung nur erfolgen, wenn zuvor die Teilnahme an einer weiteren Ausbildung nachgewiesen wurde, deren Dauer und Inhalt von dem vorsitzenden Mitglied des Prüfungsausschusses bestimmt werden.
Sie darf die in § 3 Absatz 1 des Brandenburgischen Altenpflegehilfegesetzes festgelegte Dauer nicht überschreiten.
Ein Nachweis über die weitere Ausbildung ist dem Antrag auf Zulassung zur Wiederholungsprüfung beizufügen.
Die Wiederholungsprüfung muss spätestens zwölf Monate nach der letzten Prüfung abgeschlossen sein; Ausnahmen kann die zuständige Behörde in begründeten Fällen zulassen.

### § 9  
Rücktritt von der Prüfung

(1) Tritt eine Schülerin oder ein Schüler nach der Zulassung von der Prüfung oder einem der Teile der Prüfung zurück, so ist der Grund für den Rücktritt unverzüglich dem vorsitzenden Mitglied des Prüfungsausschusses schriftlich mitzuteilen.
Genehmigt das vorsitzende Mitglied des Prüfungsausschusses den Rücktritt, so gilt die Prüfung oder der entsprechende Teil der Prüfung als nicht unternommen.
Die Genehmigung ist nur zu erteilen, wenn ein wichtiger Grund vorliegt.
Im Fall einer Krankheit kann die Vorlage einer ärztlichen Bescheinigung verlangt werden.

(2) Wird die Genehmigung für den Rücktritt nicht erteilt oder unterlässt es die Schülerin oder der Schüler, den Grund für den Rücktritt unverzüglich mitzuteilen, so gilt die Prüfung oder der entsprechende Teil der Prüfung als nicht bestanden.
§ 8 Absatz 3 gilt entsprechend.

### § 10  
Versäumnisfolgen, Unterbrechung der Prüfung

(1) Versäumt eine Schülerin oder ein Schüler einen Prüfungstermin oder unterbricht die Prüfung, so gilt die Prüfung als nicht bestanden, wenn nicht ein wichtiger Grund vorliegt.
§ 8 Absatz 3 gilt entsprechend.
Liegt ein wichtiger Grund vor, so gilt die Prüfung oder der betreffende Teil der Prüfung als nicht unternommen.

(2) Die Entscheidung darüber, ob ein wichtiger Grund vorliegt, trifft das vorsitzende Mitglied des Prüfungsausschusses.
§ 9 Absatz 1 Satz 1 und 4 gilt entsprechend.

### § 11  
Ordnungsverstöße und Täuschungsversuche

Das vorsitzende Mitglied des Prüfungsausschusses kann bei Schülerinnen und Schülern, die die ordnungsgemäße Durchführung der Prüfung in erheblichem Maße gestört oder sich eines Täuschungsversuches schuldig gemacht haben, den betreffenden Teil der Prüfung für nicht bestanden erklären; § 8 Absatz 3 gilt entsprechend.
Eine solche Entscheidung ist im Fall der Störung der Prüfung nur bis zum Abschluss der gesamten Prüfung, im Fall eines Täuschungsversuches nur innerhalb von drei Jahren nach Abschluss der Prüfung zulässig.

### § 12  
Prüfungsunterlagen

Auf Antrag ist der Schülerin oder dem Schüler nach Abschluss der Prüfung Einsicht in die Prüfungsunterlagen zu gewähren.
Der Antrag auf Zulassung zur Prüfung und die Prüfungsniederschriften sind zehn Jahre aufzubewahren.

### § 12a  
Schriftlicher Teil der Prüfung

(1) Der schriftliche Teil der Prüfung wird in Form von zwei Aufsichtsarbeiten mit der Dauer von je 60 Minuten geleistet.
Die Altenpflegeschule kann stattdessen die Erstellung einer Hausarbeit mit einem Umfang von zehn bis 15 Seiten als schriftlichen Teil der Prüfung festlegen.

(1) Der schriftliche Teil der Prüfung bezieht sich auf die Lernfelder 1.1, 1.2, 1.3 und 1.5 des Lernbereichs 1 sowie auf die Lernfelder des Lernbereichs 2 des Teils A der Anlage 1.

(2) Die Aufgaben für die Aufsichtsarbeiten werden von der zuständigen Behörde auf Vorschlag der Altenpflegeschule oder der Altenpflegeschulen bestimmt.
Jede Aufsichtsarbeit ist von zwei Fachprüferinnen oder Fachprüfern unabhängig voneinander zu benoten.
Bei unterschiedlicher Benotung entscheidet das vorsitzende Mitglied des Prüfungsausschusses im Benehmen mit den Fachprüferinnen oder Fachprüfern.

(3) Das vorsitzende Mitglied des Prüfungsausschusses bildet die Note für den schriftlichen Teil der Prüfung aus dem arithmetischen Mittel der Noten der Fachprüferinnen oder Fachprüfer.

### § 13  
Mündlicher Teil der Prüfung

(1) Der mündliche Teil der Prüfung erstreckt sich auf folgende Lernfelder der Anlage 1:

1.  Mithilfe bei der personen- und situationsbezogenen Pflege alter Menschen und Mithilfe bei der medizinischen Diagnostik und Therapie,
2.  Lebenswelten und soziale Netzwerke alter Menschen beim altenpflegerischen Handeln berücksichtigen, Mithilfe bei der Wohnraum- und Wohnumfeldgestaltung und bei der Haushaltsführung,
3.  berufliches Selbstverständnis entwickeln und berufstypische Probleme erkennen und bewältigen.

In der mündlichen Prüfung hat der Prüfling anwendungsbereite berufliche Kompetenzen nachzuweisen.

(2) Der mündliche Teil der staatlichen Prüfung wird als Einzelprüfung durchgeführt.
Die Prüfung soll für den Prüfling zu jedem in Absatz 1 Nummer 1 bis 3 genannten Prüfungsteil nicht länger als zehn Minuten dauern.
Die Prüfung jedes Prüfungsteils wird von mindestens zwei Fachprüferinnen oder zwei Fachprüfern nach § 4 Absatz 1 abgenommen und bewertet.
Das vorsitzende Mitglied des Prüfungsausschusses ist berechtigt, sich an der Prüfung zu beteiligen und kann selbst prüfen.
Aus den Noten der Fachprüferinnen oder Fachprüfer bildet das vorsitzende Mitglied des Prüfungsausschusses im Benehmen mit diesen die Prüfungsnote für den jeweiligen Prüfungsteil.
Aus den Noten der Prüfungsteile bildet das vorsitzende Mitglied des Prüfungsausschusses die Prüfungsnote für den mündlichen Teil der Prüfung.
Der mündliche Teil der staatlichen Prüfung ist bestanden, wenn die Prüfung mindestens mit „ausreichend“ bewertet wird.

(3) Das vorsitzende Mitglied des Prüfungsausschusses kann mit Zustimmung des Prüflings die Anwesenheit von Zuhörerinnen und Zuhörern beim mündlichen Teil der Prüfung gestatten, wenn ein berechtigtes Interesse besteht.

### § 14  
Praktischer Teil der Prüfung

(1) Der praktische Teil der Prüfung erstreckt sich auf die fachkundige, umfassende Grundpflege eines alten Menschen einschließlich der Förderung seiner Selbstständigkeit, seiner persönlichen Fähigkeiten und der Berücksichtigung seiner individuellen Bedürfnisse, zum Beispiel der Bedürfnisse eines demenzkranken alten Menschen.
Die Schülerin oder der Schüler übernimmt alle Aufgaben der prozessorientierten Altenpflegehilfe.

(2) Die Prüfungsaufgabe besteht aus der praktischen Durchführung der Grundpflege einschließlich der begleitenden Förderung eines alten Menschen und gegebenenfalls der Haushaltsführung auf der Grundlage der Pflegedokumentation und der Pflegeplanung sowie aus der abschließenden Reflexion in Form eines Prüfungsgesprächs.
Die Prüfung soll in einem Werktag vorbereitet, durchgeführt und abgenommen werden.
Ihre Dauer soll in der Regel  
90 Minuten nicht überschreiten.
Die Schülerinnen und Schüler werden einzeln geprüft.
Dabei ist nachzuweisen, dass die während der Ausbildung erworbenen Kompetenzen in der beruflichen Praxis angewendet werden können und die Befähigung zur Bewältigung der Aufgaben in der Altenpflegehilfe nach § 2 des Brandenburgischen Altenpflegehilfegesetzes vorhanden ist.

(3) Mindestens jeweils eine Fachprüferin oder ein Fachprüfer nach § 4 Absatz 1 Nummer 3 und 4 nehmen die Prüfung ab und bewerten die Leistung.
Das vorsitzende Mitglied des Prüfungsausschusses ist berechtigt, sich an der Prüfung zu beteiligen und selbst zu prüfen.
Die Abnahme der praktischen Prüfung einschließlich der Auswahl des alten, pflegebedürftigen Menschen erfolgt durch die Fachprüferinnen oder Fachprüfer in den nach § 3 Absatz 4 festgelegten Einrichtungen.
Die Einbeziehung des alten, pflegebedürftigen Menschen in die Prüfungssituation setzt dessen Einverständnis und die Zustimmung der Pflegedienstleitung voraus.

(4) Das vorsitzende Mitglied des Prüfungsausschusses bildet die Prüfungsnote für den praktischen Teil der Prüfung aus den Noten der Fachprüferinnen oder Fachprüfer.
Der praktische Teil der staatlichen Prüfung ist bestanden, wenn die Prüfung mindestens mit „ausreichend“ bewertet wurde.

### § 15  
Prüfung für Nichtschülerinnen und Nichtschüler

(1) Der Abschluss der Ausbildung in der Altenpflegehilfe kann auch durch eine staatliche Prüfung in Form der Prüfung für Nichtschülerinnen und Nichtschüler nach den Regelungen des § 3 Absatz 1 erworben werden.

(2) Die Prüfung findet an staatlich anerkannten Altenpflegeschulen statt.

(3) Grundsätzlich findet die Prüfung zu den Terminen der regulären Prüfung statt.

(4) Die Prüfung für Nichtschülerinnen und Nichtschüler ist kostenpflichtig.
Die Kosten werden von der mit der Durchführung der staatlichen Prüfung beauftragten Altenpflegeschule erhoben.

### § 16  
Antragstellung und Zulassung

(1) Der Antrag auf Zulassung zur Prüfung für Nichtschülerinnen und Nichtschüler ist jeweils schriftlich bis zum 30.
Juni oder bis zum 31.
Dezember eines Jahres an die zuständige Behörde nach dem Altenpflegehilfegesetz zu richten, sofern das für Soziales zuständige Mitglied der Landesregierung keine anderen Regelungen trifft.
Zugelassen wird, wer

1.  die Zugangsvoraussetzungen nach § 4 Nummer 2 des Brandenburgischen Altenpflegehilfegesetzes erfüllt,
2.  eine mindestens zweijährige berufliche Tätigkeit oder vergleichbare praktische Tätigkeiten in der Altenpflege nachweist,
3.  nicht in gesundheitlicher Hinsicht ungeeignet ist, die Vorbereitung auf die Prüfung für Nichtschülerinnen und Nichtschüler zu absolvieren und die Prüfung abzulegen,
4.  den Nachweis einer ausreichenden Vorbereitung auf die mündliche und praktische Prüfung erbringt,
5.  seinen Wohnsitz im Land Brandenburg hat und
6.  nicht Schülerin oder Schüler einer Schule in öffentlicher oder freier Trägerschaft ist.

(2) Dem Antrag auf Zulassung zur Prüfung sind folgende Unterlagen beizufügen, aus denen sich die Voraussetzungen für die Zulassung zur Prüfung ergeben:

1.  Zeugnisse über schulische Abschlüsse nach § 4 Nummer 2 des Brandenburgischen Altenpflegehilfegesetzes,
2.  Bescheinigung einer Einrichtung der ambulanten oder stationären Altenpflege über den Erwerb praktischer Kenntnisse, Fertigkeiten und Fähigkeiten in der Altenpflege durch eine geeignete berufliche Tätigkeit, gleichwertige Praktika oder andere vergleichbare praktische Tätigkeiten mit einer Dauer von mindestens zwei Jahren in den letzten vier Jahren,
3.  ein aktuelles Gesundheitszeugnis nicht älter als drei Monate,
4.  Zertifikat und Qualifizierungsprogramm über die Vorbereitung auf die Prüfung für Nichtschülerinnen und Nichtschüler in der Altenpflegehilfe an einer staatlich anerkannten Altenpflegeschule im Land Brandenburg,
5.  ein Nachweis, dass sich der Wohnsitz zum Zeitpunkt der Anmeldung zur Nichtschülerprüfung im Land Brandenburg befindet, und
6.  eine Erklärung, dass sie/er nicht Schülerin oder Schüler einer Schule in öffentlicher oder freier Trägerschaft ist.

Die Nachweise für die Nummern 1 bis 5 sind als Kopien in amtlich beglaubigter Form vorzulegen.
Die Vorbereitung auf die Nichtschülerprüfung in der Altenpflegehilfe erfolgt an staatlich anerkannten Altenpflegeschulen im Land Brandenburg im Rahmen der genehmigten Ausbildungskapazitäten.
Das Qualifizierungsprogramm ist an den im gültigen Rahmenplan für die Ausbildung in der Altenpflegehilfe aufgeführten Lernbereichen und Lernfeldern sowie an der Ausbildungs- und Prüfungsordnung Altenpflegehilfe fachlich auszurichten und der zuständigen Behörde vorzulegen.
Es soll einen Umfang von bis zu 350 Unterrichtsstunden für den theoretischen und praktischen Unterricht und von mindestens vier Wochen praktischer Ausbildung haben.

(3) Die zuständige Behörde entscheidet über die Zulassung zur Prüfung und legt fest, welche Altenpflegeschule mit der Durchführung der Prüfung beauftragt wird.
Die Zulassungsentscheidung wird der Antragstellerin oder dem Antragsteller spätestens zwei Wochen vor dem Prüfungstermin schriftlich mitgeteilt.

(4) Im Fall der Nichtzulassung kann der Antrag auf Zulassung erneut nach den in Absatz 1 genannten Regelungen gestellt werden.

(5) Die Zulassung zur Prüfung für Nichtschülerinnen und Nichtschüler ist gebührenpflichtig.
Die Gebühr wird von der zuständigen Behörde erhoben.

### § 17  
Besondere Verfahrensvorschriften für die Prüfung für Nichtschülerinnen und Nichtschüler

(1) Vor Prüfungsbeginn sind durch die Nichtschülerin oder den Nichtschüler folgende Unterlagen in der prüfenden Schule vorzulegen:

1.  der Zulassungsbescheid,
2.  der Personalausweis oder der Reisepass und
3.  ein Nachweis über die bezahlten Prüfungskosten.

Nur bei vollständiger Vorlage vorbezeichneter Nachweise besteht das Recht auf Teilnahme an der Prüfung.
Der Personalausweis oder der Reisepass ist auf Anforderung vor jedem Prüfungsteil vorzuzeigen.

(2) Über die bestandene oder die nicht bestandene staatliche Prüfung wird das Zeugnis oder die entsprechende schriftliche Mitteilung nach § 8 Absatz 2 erteilt mit dem Vermerk „Die Prüfung wurde als Nichtschülerin oder Nichtschüler abgelegt.“ oder „Die Prüfung wurde als Nichtschülerin oder Nichtschüler abgelegt und nicht bestanden.“

(3) Ansonsten gelten alle anderen Regelungen dieser Verordnung auch für Nichtschülerinnen und Nichtschüler.

### § 18  
Erlaubnisurkunde

Liegen die Voraussetzungen nach § 1 des Brandenburgischen Altenpflegehilfegesetzes für die Erteilung der Erlaubnis zur Führung der Berufsbezeichnung „Altenpflegehelferin“ oder „Altenpflegehelfer“ vor, so stellt die zuständige Behörde die Erlaubnisurkunde nach dem Muster der Anlage 4 aus.

### § 19  
Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 16.
Juli 2010 in Kraft.

Potsdam, den 27.
April 2012

Der Minister für Arbeit,  
Soziales, Frauen und Familie

Günter Baaske

Anlage 1  
(zu § 1 Absatz 1)
----------------------------

### Theoretischer und praktischer Unterricht sowie praktische Ausbildung in der Altenpflegehilfe

#### A.
Theoretischer und praktischer Unterricht

Der theoretische und praktische Unterricht umfasst folgende Lernbereiche und Lernfelder:

#### Lernbereich 1: Aufgaben und Konzepte in der Altenpflegehilfe

Lernfeld 1.1: _Theoretische Grundlagen in das altenpflegerische Handeln einbeziehen_

Die Schülerinnen und Schüler besitzen pflegewissenschaftliche Grundkenntnisse und wenden pflegerelevante Kenntnisse der Bezugswissenschaften in ihrem beruflichen Handeln unter Anleitung an.

Die Schülerinnen und Schüler sind zu befähigen,

*   Grundbegriffe aus der Gerontologie, Soziologie und Sozialmedizin zu verstehen und anzuwenden,
*   die Bedeutung der Gesundheitsförderung und Gesundheitsprävention zu erkennen und an deren Umsetzung mitzuwirken,
*   die Bedeutung der Biografiearbeit zu erfassen und diese mit umzusetzen,
*   sich mit ethischen Fragen auseinanderzusetzen.

Lernfeld 1.2: _Mithilfe bei der Planung, Durchführung, Dokumentation und Evaluation der Pflege alter Menschen_

Die Schülerinnen und Schüler sind in der Lage, ihr Pflegehandeln nach dem Pflegeprozess mitzugestalten.

Die Schülerinnen und Schüler sind zu befähigen,

*   die Bedeutung der Wahrnehmung und der exakten Beobachtung alter Menschen als eine wesentliche Voraussetzung ihres pflegerischen Handelns zu erkennen,
*   unter Anleitung Informationen systematisch zu sammeln und weiterzuleiten,
*   die Bedeutung der Pflegebeziehung zu erkennen,
*   die Möglichkeiten eigener Mitwirkung im Pflegeprozess zu erkennen und zu dokumentieren.

Lernfeld 1.3: _Mithilfe bei der personen- und situationsbezogenen Pflege alter Menschen_

Die Schülerinnen und Schüler besitzen pflegerelevante Grundkenntnisse und setzen diese unter Anleitung im altenpflegerischen Handeln um.

Die Schülerinnen und Schüler sind zu befähigen,

*   sich mit pflegerelevanten Grundlagen der Anatomie/Physiologie, der Pathologie, der Geriatrie, der Hygiene, der Ernährungslehre, der Arzneimittellehre und der Psychologie auseinanderzusetzen,
*   eine fachkundige, umfassende Grundpflege durchzuführen,
*   alte Menschen unter Beachtung ihrer Bedürfnisse in ihrer Selbstständigkeit zu unterstützen und zu fördern,
*   die persönlichen Fähigkeiten alter Menschen zu fordern und zu fördern,
*   mitzuhelfen bei gesundheitserhaltenden, gesundheitsfördernden und prophylaktischen Maßnahmen,
*   Funktionseinschränkungen der Sinnesorgane zu erkennen und zu verstehen,
*   andere Wahrnehmungsmuster und Kommunikationswege beim alten Menschen zu nutzen,
*   den alten Menschen Sicherheit zu geben, sie auf mögliche Gefahren hinzuweisen beziehungsweise diesen entgegenzuwirken,
*   die natürlichen und technischen Hilfsmittel zu kennen und unter Anleitung anzuwenden,
*   lebensbedrohliche Situationen und kritische Zustandsveränderungen zu erkennen,
*   Informationen weiterzuleiten und „Erste Hilfe“ zu leisten,
*   sich anatomische und physiologische Grundlagen der Organsysteme anzueignen, bei der Pflege und Betreuung von akut und chronisch Kranken mitzuhelfen,
*   sich anatomische und physiologische Grundlagen des Nervensystems anzueignen,
*   Mithilfe bei der Pflege und Betreuung von neurologisch Erkrankten zu leisten,
*   entsprechende Hilfsmittel unter Anleitung einzusetzen,
*   ausgewählte Rehabilitationskonzepte mit umzusetzen,
*   die Rolle der altenpflegerischen Hilfskraft im multiprofessionellen Team bei der Betreuung und Behandlung gerontopsychiatrisch erkrankter alter Menschen zu reflektieren,
*   Ursachen, Symptome, Komplikationen und Therapien bei ausgewählten gerontopsychiatrischen Krankheitsbildern zu kennen,
*   den pflegerischen Bedarf bei gerontopsychiatrisch erkrankten alten Menschen zu kennen und unter Anleitung bei entsprechenden Maßnahmen mitzuwirken,
*   auf selbstgefährdendes, aggressives und gewalttätiges Verhalten von gerontopsychiatrisch erkrankten alten Menschen adäquat zu reagieren,
*   rechtliche Grundlagen in der Gerontopsychiatrie zu kennen,
*   Infektions- und Übertragungswege zu kennen,
*   prophylaktische und pflegerische Maßnahmen bei ausgewählten Infektionserkrankungen zu kennen,
*   im Umgang mit infektiös erkrankten, alten Menschen weder sich noch ihre Umwelt zu gefährden,
*   unter Anleitung pflegetechnische Maßnahmen unter hygienischen Aspekten mit durchzuführen,
*   Schmerzen als individuelles Phänomen zu erkennen und entsprechende Beobachtungen fachgerecht weiterzuleiten,
*   Mithilfe bei schmerzlindernden Lagerungen zu leisten,
*   sich mit ethischen Fragen hinsichtlich passiver und aktiver Sterbehilfe auseinanderzusetzen und zu reflektieren,
*   individuelle Bedürfnisse Sterbender zu kennen und bei deren Erfüllung mitzuwirken,
*   verbale und nonverbale Kommunikationstechniken zu benutzen und dabei Nähe und Begleitung zu vermitteln,
*   bei der Begleitung von Angehörigen unter Anleitung mitzuarbeiten.

Lernfeld 1.4: _Grundlagen der Kommunikation und Gesprächsführung_

Die Schülerinnen und Schüler beherrschen die beruflich erforderlichen Techniken der verbalen und nonverbalen Kommunikation in Bezug auf die zu betreuenden Menschen, deren Angehörige und das therapeutische Team.

Die Schülerinnen und Schüler sind zu befähigen, Grundlagen der Gesprächsführung zu kennen und anzuwenden, Gesprächssituationen zu gestalten und personenzentriert zu kommunizieren.

Lernfeld 1.5: _Mithilfe bei der medizinischen Diagnostik und Therapie_

Die Schülerinnen und Schüler kennen ausgewählte fachliche Grundlagen der medizinischen Diagnostik und Therapie, um als Mitglied des therapeutischen Teams verantwortungsbewusst assistieren zu können.

Die Schülerinnen und Schüler sind zu befähigen, Hilfeleistungen für Pflegefachkräfte sowie Angehörige anderer Gesundheitsfachberufe durchzuführen und für die jeweiligen medizinischen Maßnahmen die erforderlichen Vor- und Nachbereitungen unter Anleitung zu treffen.

#### Lernbereich 2: Unterstützung alter Menschen bei der Lebensgestaltung

Lernfeld 2.1: _Lebenswelten und soziale Netzwerke alter Menschen beim altenpflegerischen Handeln berücksichtigen_

Die Schülerinnen und Schüler akzeptieren die soziale Situation alter Menschen als individuelle Lebenswelt.
Sie begleiten alte Menschen unter Anleitung bei der Gestaltung eines selbstbestimmten Lebens.

Die Schülerinnen und Schüler sind zu befähigen,

*   Angehörige als Partner des alten Menschen und der eigenen Arbeit zu sehen und zu akzeptieren,
*   soziale Netzwerke alter Menschen zu kennen und in ihre Arbeit einzubeziehen,
*   sich mit den Besonderheiten verschiedener Kulturkreise und Religionen auseinanderzusetzen, sie zu akzeptieren und in die pflegerische Arbeit zu integrieren,
*   sich aktiv mit dem Sinn des Lebens auseinanderzusetzen,
*   Leben als Veränderung zu begreifen und individuelles Altern zu verstehen,
*   individuelle Ressourcen alter Menschen zu erkennen und unter Anleitung zu fördern,
*   die Bedeutung des eigenen Wohnraums für alte Menschen zu verstehen,
*   unter Anleitung Unterstützung bei der Alltagsgestaltung zu geben,
*   Privatsphäre und Individualität zu respektieren,
*   Sexualität im Alter als natürlich anzusehen und Intimität zu ermöglichen.

Lernfeld 2.2: _Mithilfe bei der Wohnraum- und Wohnumfeldgestaltung und bei der Haushaltsführung_

Die Schülerinnen und Schüler unterstützen unter Anleitung die Wohnraum- und Wohnumfeldgestaltung.
Sie helfen alten Menschen bei der Haushaltsführung.

Die Schülerinnen und Schüler sind zu befähigen, den eigenen Wohnraum sowie das Wohnumfeld des alten Menschen als wichtigen Lebensraum zu erkennen und zu akzeptieren, alte Menschen in ihrer Selbstständigkeit zu unterstützen und Hilfestellung bei der Haushaltsführung und Versorgung zu geben.

Lernfeld 2.3: _Mithilfe bei der Tagesgestaltung und bei selbst organisierten Aktivitäten_

Die Schülerinnen und Schüler wirken bei der Gestaltung und Organisation des Alltags alter Menschen mit.

Die Schülerinnen und Schüler sind zu befähigen,

*   die Notwendigkeit einer sinnvollen Beschäftigung mit und für alte Menschen zu erkennen,
*   unter Anleitung Vereinsamung und Isolation vorzubeugen, alte Menschen in die Vorbereitung und Durchführung von Aktivitäten zu integrieren und damit das Selbstwertgefühl zu steigern.

#### Lernbereich 3: Rechtliche Rahmenbedingungen altenpflegerischer Arbeit kennen und berücksichtigen

Die Schülerinnen und Schüler kennen grundlegende rechtliche Rahmenbedingungen und beachten diese in ihrer beruflichen Tätigkeit.

Die Schülerinnen und Schüler sind zu befähigen, gesetzliche Regelungen im altenpflegerischen Handeln zu berücksichtigen und einzuhalten sowie Grenzen der rechtlichen Regelungen zu reflektieren.

#### Lernbereich 4: Berufe in der Altenpflege

Die Schülerinnen und Schüler bestimmen ihre berufliche Rolle als verantwortliches berufliches Arbeiten mit Menschen.
Sie definieren Altenpflegehilfe als berufliche Unterstützungsleistung.
Sie sind in der Lage, Methoden und Techniken der Arbeitsplanung und des berufsbezogenen Lernens zu nutzen.
Die Schülerinnen und Schüler können Ursachen berufstypischer Konflikte und Belastungen unterscheiden und nutzen Möglichkeiten zur Vorbeugung und Gesundheitsförderung.

Lernfeld 4.1: _Berufliches Selbstverständnis entwickeln_

Die Schülerinnen und Schüler besitzen berufliches Selbstverständnis und haben Kenntnis über ihre Stellung im Gefüge der Pflegeberufe.

Die Schülerinnen und Schüler sind zu befähigen,

*   die Berufsgesetze der Altenhilfe zu kennen,
*   Möglichkeiten der Aus- und Fortbildung als Weg zum beruflichen Handeln und zur Planung der eigenen Karriere zu kennen und zu nutzen,
*   an der Teamentwicklung konstruktiv mitzuwirken und sich mit der eigenen Teamfähigkeit auseinanderzusetzen,
*   die eigene Berufswahl zu reflektieren und sich im Sinne des Berufsethos zu verhalten.

Lernfeld 4.2: _Lernen lernen_

Die Schülerinnen und Schüler steuern und gestalten motiviert ihren eigenen Lernprozess.

Die Schülerinnen und Schüler sind zu befähigen,

*   ihr eigenes Lernverhalten zu reflektieren,
*   ihren Lernprozess selbstständig mit effektiven Lernmethoden und Lerntechniken zu organisieren,
*   wesentliche Arbeits- und Präsentationstechniken anzuwenden,
*   Lernergebnisse dauerhaft zu sichern,
*   Ressourcen von Lerngruppen zu nutzen und Unterstützungsangebote bei Lernschwierigkeiten anzunehmen und anzubieten.

Lernfeld 4.3: _Berufstypische Probleme erkennen und bewältigen_

Die Schülerinnen und Schüler kennen Konflikte und typische Belastungssituationen und gehen mit pflegerischen Spannungssituationen adäquat um.

Die Schülerinnen und Schüler sind zu befähigen,

*   Konflikte und berufstypische Belastungssituationen zu erkennen und Hilfe zu nutzen,
*   Anzeichen von Angst und Gewalt in der Pflege zu erkennen,
*   Maßnahmen der Gewaltprävention zu kennen und bei der Entwicklung individueller Lösungsstrategien mitzuwirken.

Lernfeld 4.4: _Persönliche Gesundheitsförderung_

Die Schülerinnen und Schüler verhalten sich gesundheitsbewusst und setzen die Arbeitsschutzrichtlinien um.

Die Schülerinnen und Schüler sind zu befähigen,

*   Gesundheit gefährdendes Verhalten in beruflichen und privaten Handlungsfeldern zu vermeiden,
*   Zeichen von Überlastung zu erkennen und frühzeitig entsprechende Unterstützungsangebote wahrzunehmen.

Verfügungsstunden: 50

Unterrichtsstunden insgesamt: 750.

#### B.
Praktische Ausbildung

Die praktische Ausbildung erfolgt nach § 3 Absatz 3 des Brandenburgischen Altenpflegehilfegesetzes

1.  in stationären Pflegeeinrichtungen im Sinne des § 71 Absatz 2 des Elften Buches Sozialgesetzbuch, wenn es sich dabei um eine Einrichtung für alte Menschen handelt, und
2.  in einer ambulanten Pflegeeinrichtung im Sinne des § 71 Absatz 1 des Elften Buches Sozialgesetzbuch, wenn deren Tätigkeitsbereich die Pflege alter Menschen einschließt.

Besteht ein Ausbildungsverhältnis mit einer stationären Pflegeeinrichtung im oben genannten Sinne, sind von den 900 Stunden der praktischen Ausbildung mindestens 700 in der stationären und bis zu 200 in der ambulanten Pflegeeinrichtung abzuleisten.
Sofern ein Ausbildungsverhältnis mit einer ambulanten Pflegeeinrichtung im oben genannten Sinne besteht, sind von den 900 Stunden der praktischen Ausbildung mindestens 700 in der ambulanten und bis zu 200 in der stationären Pflegeeinrichtung abzuleisten.

Die praktische Ausbildung umfasst alle Lernbereiche und Lernfelder des theoretischen und praktischen Unterrichts.
Die im Abschnitt A.
„Theoretischer und praktischer Unterricht“ aufgeführten Inhalte und Ziele sind auch in der praktischen Ausbildung zu berücksichtigen.

In der praktischen Ausbildung sind die folgenden Kenntnisse, Fertigkeiten und Fähigkeiten zu vermitteln:

#### Lernbereich 1: Aufgaben und Konzepte in der Altenpflegehilfe

Lernfeld 1.1: _Theoretische Grundlagen in das altenpflegerische Handeln einbeziehen_

Die Schülerinnen und Schüler geben alten Menschen beim Erhalt und bei der Wiedergewinnung von Fähigkeiten und sozialen Kontakten Unterstützung.

Die Schülerinnen und Schüler sind zu befähigen,

*   unter Anleitung gerontologische, soziologische und sozialmedizinische Grundlagen in der jeweiligen Berufssituation korrekt anzuwenden,
*   unter Anleitung gesundheitsfördernde und gesundheitspräventive Maßnahmen in pflegerische Verrichtungen zu integrieren,
*   unter Anleitung individuelle biografische Aspekte bei der pflegerischen Tätigkeit zu berücksichtigen und eine biografieorientierte Lebensgestaltung des alten Menschen zu ermöglichen,
*   unter Anleitung den alten Menschen bei einer gesundheitsgerechten und angepassten Lebensführung sowie einer gesellschaftlichen Teilhabe zu unterstützen,
*   ethische Grundprinzipien kennenzulernen und diese unter Anleitung in der Praxis anzuwenden.

Lernfeld 1.2: _Mithilfe bei der Planung, Durchführung, Dokumentation und Evaluation der Pflege alter Menschen_

Die Schülerinnen und Schüler sind in der Lage, eine fachkundige umfassende Grundpflege alter Menschen unter Anleitung zu gestalten.

Die Schülerinnen und Schüler sind zu befähigen,

*   unter Anleitung eine exakte Beobachtung des alten Menschen durchzuführen, Veränderungen der Person wahrzunehmen und diese zu übermitteln,
*   unter Anleitung die in der Theorie erworbenen Kenntnisse hinsichtlich des Pflegeprozesses beim alten Menschen anzuwenden,
*   betriebliche Rahmenbedingungen zu berücksichtigen und interne Pflegestandards einschließlich Dokumentation zu verwenden.

Lernfeld 1.3: _Mithilfe bei der personen- und situationsbezogenen Pflege alter Menschen_

Die Schülerinnen und Schüler besitzen pflegerelevante Grundkenntnisse und setzen diese in ihrem Handeln um.
Die Schülerinnen und Schüler unterstützen alte Menschen bei der Lebensführung.

Die Schülerinnen und Schüler sind zu befähigen,

*   unter Anleitung auf der Grundlage bereits vermittelter theoretischer Kenntnisse die Pflegesituation des alten Menschen hinsichtlich der Anatomie/Physiologie, Geriatrie, Psychologie, Arzneimittellehre, Hygiene, Ernährungslehre zu erkennen und zu erfassen,
*   unter Anleitung die individuellen Ressourcen und den Gesundheitszustand des alten Menschen zu erkennen und die Aufrechterhaltung durch eine aktivierende Pflege, insbesondere durch Haut- und Körperpflege, Betten, Lagern und Transfer, Schlaf und Schlafförderung, Ausscheidung, Nahrungszubereitung und Nahrungsaufnahme, Mobilisation zu unterstützen,
*   die körperlichen, psychischen und sozialen Besonderheiten des alten Menschen kennenzulernen und diese unter Anleitung in der Praxis zu berücksichtigen,
*   die in der Pflege unter Anleitung durch die Krankenbeobachtung gewonnenen Erkenntnisse über den Gesundheitszustand des alten Menschen zu berücksichtigen und adäquat die notwendigen Prophylaxen anzuwenden,
*   unter Anleitung die Pflegesituation des alten Menschen hinsichtlich einer Sehbeeinträchtigung, Hörbeeinträchtigung, Beeinträchtigung des Geschmackssinns, des Geruchssinns, des Haut- und Tastsinns zu erkennen und zu erfassen, 
*   die Bedeutung der sozialen Kontakte für den alten Menschen zu kennen und dessen Selbstständigkeit zu fördern,
*   ihre Kenntnisse und Fähigkeiten bei der sicheren Gestaltung des Umfeldes des alten Menschen zu nutzen,
*   Kenntnisse, Fähigkeiten und Fertigkeiten im Umgang mit alten Menschen zu erwerben, die akut und chronisch erkrankt sind, insbesondere bezüglich Erkrankungen des Herz-Kreislaufsystems, des Atmungssystems, des Bewegungssystems, des Verdauungssystems, des Hormonsystems und des Urogenitalsystems und von Störungen der Thermoregulation,
*   lebensbedrohliche Situationen zu erkennen, Veränderungen des Gesundheitszustandes des alten Menschen festzustellen, diese Informationen weiterzuleiten und gegebenenfalls „Erste Hilfe“ zu leisten,
*   Kenntnisse, Fähigkeiten und Fertigkeiten im Umgang mit alten Menschen zu erwerben, die an Erkrankungen des Zentralnervensystems leiden, insbesondere bezüglich Apoplexia cerebri und Morbus Parkinson,
*   alte Menschen in verschiedenen Pflegebereichen durch Beobachtung der körperlichen, psychischen und sozialen Situation kennenzulernen und die Beobachtungsergebnisse zu übermitteln,
*   bei der Umsetzung des Bobath-Konzepts unter Anleitung mitzuhelfen,
*   Kenntnisse, Fähigkeiten und Fertigkeiten im Umgang mit alten Menschen mit gerontopsychiatrischen Erkrankungen zu erwerben, insbesondere in Bezug auf Demenz, Depression, Abhängigkeitserkrankungen, Suizid,
*   Kenntnisse, Fähigkeiten und Fertigkeiten im Umgang mit den Hygienevorschriften der Pflegeeinrichtung zu erwerben, insbesondere in Bezug auf Hygienerichtlinien, Vermeidung von nosokomialen Infektionen, Selbst- und Fremdschutz, Reinigung und Desinfektion, Isolierungsmaßnahmen,
*   unter Anleitung eine konsequente Schmerzbeobachtung durchzuführen,
*   unter Anleitung schmerzlindernde Therapien anzuwenden und dabei alternative/nicht medikamentöse Maßnahmen zu berücksichtigen, insbesondere physikalische Therapien, darunter Wärme und Kälte, Psychotherapie,
*   Kenntnisse im Umgang mit Sterben und Tod zu erwerben, das heißt unter Anleitung die Pflege und Betreuung von sterbenden Menschen unter Berücksichtigung ihrer größtmöglichen Selbstbestimmung durchzuführen, den Leichnam nach Eintritt des Todes unter Berücksichtigung der jeweiligen Kultur und Religion zu versorgen, die notwendigen administrativen Tätigkeiten durchzuführen und die Angehörigen in den Phasen der Trauer zu begleiten.

Lernfeld 1.4: _Grundlagen der Kommunikation und Gesprächsführung_

Die Schülerinnen und Schüler können Anregungen geben und Begleitung umsetzen im Zusammenhang mit Familien- und Nachbarschaftshilfe.

Die Schülerinnen und Schüler sind zu befähigen,

*   unter Anweisung Anleitungs- und Beratungssituationen zu gestalten,
*   angemessen unter Anleitung mit dem alten Menschen, Angehörigen und Bezugspersonen zu kommunizieren,
*   unter Anleitung die bestehenden Kommunikationsregeln in der Einrichtung zu erfassen und diese in der Pflegesituation umzusetzen.

Lernfeld 1.5: _Mithilfe bei der medizinischen Diagnostik und Therapie_

Die Schülerinnen und Schüler wirken als Mitglieder des Teams verantwortungsbewusst bei der medizinischen Diagnostik und Therapie mit.

Die Schülerinnen und Schüler sind zu befähigen,

*   unter Anleitung an der medizinischen Diagnostik und Therapie mitzuwirken, insbesondere bei der Inhalationstherapie, der Temperaturregulation, der Blutzuckerüberwachung, der Blutdrucküberwachung, der Pulsüberwachung, der Atmungsüberwachung, der Wundbehandlung,
*   unter Anleitung Hilfestellung bei der Arzneimitteleinnahme zu leisten.

#### Lernbereich 2: Unterstützung alter Menschen bei der Lebensgestaltung

Lernfeld 2.1: _Lebenswelten und soziale Netzwerke alter Menschen beim altenpflegerischen Handeln berücksichtigen_

Die Schülerinnen und Schüler akzeptieren die soziale Situation alter Menschen als individuelle Lebenswelt.
Sie begleiten alte Menschen unter Anleitung bei der Gestaltung eines selbstbestimmten Lebens.

Die Schülerinnen und Schüler sind zu befähigen,

*   unter Anleitung im Umgang mit Angehörigen neue Einsichten und Kenntnisse zu erwerben,
*   unter Anleitung den pflegenden Angehörigen Hilfe und Unterstützung anzubieten,
*   die sozialen Netzwerke vor Ort kennenzulernen,
*   unter Anleitung den Lebensraum des alten Menschen unter Beachtung ihrer ethischen, religiösen und interkulturellen Bedürfnisse mitzugestalten,
*   unter Anleitung Kompetenzen im Umgang mit dem subjektiven Erleben des Alterns, einschließlich der Sexualität, zu entwickeln,
*   familienstrukturelle Veränderungen zu erkennen.

Lernfeld 2.2: _Mithilfe bei der Wohnraum- und Wohnumfeldgestaltung und bei der Haushaltsführung_

Die Schülerinnen und Schüler unterstützen alte Menschen bei der Lebensführung und geben Hilfe bei der Haushaltsführung.

Die Schülerinnen und Schüler sind zu befähigen,

*   unter Anleitung altersgerecht fördernde Elemente der Wohnumgebung des alten Menschen, insbesondere bei der Wandgestaltung, der Farbauswahl und im Hinblick auf persönliche Gegenstände zu berücksichtigen,
*   unter Anleitung für Sicherheit zu sorgen und pflegerische Interventionen durchzuführen,
*   verschiedene Wohnformen des alten Menschen kennenzulernen,
*   unter Anleitung bei der Haushaltsführung zu unterstützen und Esskulturen zu beachten.

Lernfeld 2.3: _Mithilfe bei der Tagesgestaltung und bei selbst organisierten Aktivitäten_

Die Schülerinnen und Schüler unterstützen alte Menschen bei der Lebensführung und geben Hilfe beim Erhalt und der Wiedergewinnung von Fähigkeiten und sozialen Kontakten.

Die Schülerinnen und Schüler sind zu befähigen,

*   individuelle Aktivitäten und Aktivitäten in der Gruppe unter Anleitung zu planen, vorzubereiten, durchzuführen sowie Tagesabläufe zu gestalten,
*   die Notwendigkeit einer sinnvollen Lebensgestaltung für den alten Menschen zu erkennen, unter Anleitung die Umsetzung individueller Wünsche zu unterstützen und mitzuhelfen, bedürfnisgerechte interne und externe Angebote zu organisieren, insbesondere mobilitätsfördernde, kulturelle und künstlerische Beschäftigung.

#### Lernbereich 3: Rechtliche Rahmenbedingungen altenpflegerischer Arbeit kennen und berücksichtigen

Die Schülerinnen und Schüler sind zu befähigen, die rechtlichen Rahmenbedingungen altenpflegerischer Arbeit, insbesondere im Vertragsrecht (Behandlungsvertrag), Arbeitsrecht, Haftungsrecht, Betreuungsrecht, Unterbringungsrecht, bei der Schweigepflicht, in Bezug auf Körperverletzung, beim Erbrecht, bei Testamenten, beim Heimrecht, bei der Patientenverfügung und beim Ausbildungsvertrag zu berücksichtigen.

#### Lernbereich 4: Berufe in der Altenpflege

Die Schülerinnen und Schüler besitzen ein berufliches Selbstverständnis und positionieren sich im Team.

Lernfeld 4.1: _Berufliches Selbstverständnis entwickeln_

Die Schülerinnen und Schüler sind zu befähigen,

*   das Wissen über den eigenen Beruf zu erweitern und die Möglichkeiten der beruflichen Entwicklung nach der Ausbildung kennenzulernen,
*   Kenntnis über die Berufsgesetze der Altenpflege zu haben,
*   die Fortbildungsmöglichkeiten für Pflegeberufe zu kennen und daran teilzunehmen,
*   verantwortlich im Team mitzuarbeiten und die Zusammenarbeit im interdisziplinären Team und in Fallbesprechungen mitzugestalten,
*   die Funktion und Arbeitsweise von Berufsverbänden zu kennen.

Lernfeld 4.2: _Lernen lernen_

Die Schülerinnen und Schüler gestalten motiviert ihren individuellen Lernprozess.

Die Schülerinnen und Schüler sind zu befähigen,

*   ihre theoretisch erworbenen Kenntnisse über den Lernprozess in der Praxis zu vertiefen und ihre Lernergebnisse dauerhaft zu sichern,
*   für die Durchführung von Praxisaufträgen verschiedene Präsentationstechniken zu nutzen.

Lernfeld 4.3: _Berufstypische Probleme erkennen und bewältigen_

Die Schülerinnen und Schüler erkennen berufstypische Probleme und lernen damit konstruktiv umzugehen.

Die Schülerinnen und Schüler sind zu befähigen,

*   ihre Handlungen durch Gespräche zu reflektieren, Arbeitsbelastungen zu erkennen und berufliche Stresssituationen zu bewältigen,
*   Probleme anzusprechen und gemeinsam mit dem Team zu lösen,
*   Gewaltpotenziale in der Pflege zu erkennen und bei der Entwicklung individueller Lösungsstrategien mitzuwirken.

Lernfeld 4.4: _Persönliche Gesundheitsförderung_

Die Schülerinnen und Schüler verhalten sich gesundheitsbewusst.

Die Schülerinnen und Schüler sind zu befähigen, die eigene Gesundheit sowie den persönlichen Lebensstil zu reflektieren und gegebenenfalls positive Veränderungen herbeizuführen, insbesondere durch Suchtprävention, gesunde Ernährung und Bewegung, gesundheitsgerechtes Schlafverhalten.

Anlage 2  
(zu § 1 Absatz4)
---------------------------

### Bescheinigung über die Teilnahme an den Ausbildungsveranstaltungen

Frau/Herr                ………………………………………………..……………………………………………………..

geboren am             ………………………………………………..……………………………………………………..

in                            ………………………………………………..……………………………………………………..

hat in der Zeit vom …………………………………… bis ………………….………………………………………......

regelmäßig und mit Erfolg an dem theoretischen und praktischen Unterricht und an der praktischen Ausbildung für Altenpflegehelferinnen und Altenpflegehelfer nach § 3 Absatz 1 des Brandenburgischen Altenpflegehilfegesetzes teilgenommen.

Die Ausbildung ist – nicht – über die nach dem Brandenburgischen Altenpflegehilfegesetz zulässigen Fehlzeiten hinaus – um …....…...
Stunden[\*)](#_ftn1) unterbrochen worden.

……………………………………………………….  
Ort, Datum

……………………………………………………….  
Bezeichnung der Altenpflegeschule

(Stempel der Altenpflegeschule)

……………………………………………………….  
Die Schulleitung  
(Unterschrift)

Anlage 3  
(zu § 8 Absatz 2)
----------------------------

### Zeugnis über die staatliche Prüfung

Frau/Herr                ………………………………………………..………………………………………………….….

geboren am             ………………………………………………..……………………………………………….…….

in                            ………………………………………………..……………………………………………………..

hat am …………………….… die staatliche Prüfung nach § 1 Absatz 2 Nummer 1 des Brandenburgischen Altenpflegehilfegesetzes vor dem staatlichen Prüfungsausschuss bei der

……………………………………………………………………………………………………………

in ………………………………………………………………………………………………………… bestanden.

Die Prüfung wurde als Nichtschülerin/Nichtschüler abgelegt[\*)](#_ftn1).

Sie/Er hat folgende Prüfungsnoten erhalten:

1.  im schriftlichen Teil der Prüfung „ …………………………..“
2.  im mündlichen Teil der Prüfung „ …..………………………“
3.  im praktischen Teil der Prüfung „ …..………………………“.

………………………………………………………………  
Ort, Datum

(Siegel der Behörde)

………………………………………………………………  
Die Vorsitzende/Der Vorsitzende des Prüfungsausschusses  
(Unterschrift)

Anlage 4  
(zu § 18)
--------------------

### Urkunde über die Erlaubnis zur Führung der Berufsbezeichnung

Frau/Herr                ………………………………………………..……………………………………………………..

geboren am             ………………………………………………..……………………………………………………..

in                            ………………………………………………..……………………………………………………..

erhält auf Grund des § 1 Absatz 2 des Brandenburgischen Altenpflegehilfegesetzes mit Wirkung vom heutigen Tage die Erlaubnis, die Berufsbezeichnung

„………………………………………………………………………………“

zu führen.

…………………………………………………………...  
Ort, Datum

(Siegel der Behörde)

………………………………………………………....…...  
Die Vorsitzende/Der Vorsitzende des Prüfungsausschusses  
(Unterschrift)

* * *

[\*)](#_ftnref1) Nichtzutreffendes streichen.