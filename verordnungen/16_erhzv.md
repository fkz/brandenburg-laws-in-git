## Sechzehnte Verordnung zur Festsetzung von Erhaltungszielen und Gebietsabgrenzungen für Gebiete von gemeinschaftlicher Bedeutung (16. Erhaltungszielverordnung - 16. ErhZV)

Auf Grund des § 14 Absatz 3 des Brandenburgischen Naturschutzausführungs​gesetzes vom 21. Januar 2013 (GVBl.
I Nr. 3) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Festsetzung

Die in Anlage 1 aufgeführten und in Anlage 2 näher beschriebenen Gebiete werden gemäß Artikel 4 Absatz 4 der Richtlinie 92/43/EWG als Gebiete von gemeinschaftlicher Bedeutung (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) in den in § 3 bestimmten Grenzen festgesetzt.
Sie sind Teil des kohärenten europäischen ökologischen Netzes „Natura 2000“ und liegen vollständig oder anteilig im Biosphärenreservat Schorfheide-Chorin.

#### § 2 Erhaltungsziele

Die in Anlage 1 genannten Gebiete von gemeinschaftlicher Bedeutung stehen unter besonderem Schutz.
Erhaltungsziel für das jeweilige Gebiet ist die Erhaltung oder Wiederherstellung eines günstigen Erhaltungszustandes (§ 7 Absatz 1 Nummer 10 des Bundesnaturschutzgesetzes) der in Anlage 2 für das jeweilige Gebiet genannten natürlichen Lebensraumtypen oder Tier- und Pflanzenarten von gemeinschaftlichem Interesse.
In den Anlagen 3 und 4 werden für die in Anlage 1 aufgeführten Gebiete die ökologischen Erfordernisse für einen günstigen Erhaltungszustand der natürlichen Lebensraumtypen nach Anhang I und der Tier- und Pflanzenarten nach Anhang II der Richtlinie 92/43/EWG beschrieben.

#### § 3 Gebietsabgrenzung

(1) Die Grenzen der Gebiete von gemeinschaftlicher Bedeutung sind in den in Anlage 2 genannten und in Anlage 5 Nummer 2 näher bezeichneten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 60 rot eingetragen.
Als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 5 Nummer 1 aufgeführte Übersichtskarte im Maßstab 1 : 75 000 dient der räumlichen Einordnung der Gebiete von gemeinschaftlicher Bedeutung.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 5 Nummer 2 aufgeführten topografischen Karten. Zur Orientierung werden die Gebiete in Anlage 2 jeweils in einer Kartenskizze dargestellt.

(2) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim jeweils zuständigen Landkreis, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 4 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 28.
Februar 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#a) Diese Verordnung dient der Umsetzung der Richtlinie 92/43/EWG des Rates vom 21.
Mai 1992 zur Erhaltung der natürlichen Lebensräume sowie der wild lebenden Tiere und Pflanzen (ABl. L 206 vom 22.7.1992, S. 7), die durch die Richtlinie 2013/17/EU vom 13.
Mai 2013 (ABl.
L 158 vom 10.6.2013, S. 193) geändert worden ist.

* * *

### Anlagen

1

[Anlage 1 - Liste der Gebiete von gemeinschaftlicher Bedeutung](/br2/sixcms/media.php/68/GVBl_II_18_2018-Anlage-1.pdf "Anlage 1 - Liste der Gebiete von gemeinschaftlicher Bedeutung") 281.1 KB

2

[Anlage 2 (Gebiete 1 bis 23) - Gebiete von gemeinschaftlicher Bedeutung](/br2/sixcms/media.php/68/GVBl_II_18_2018-Anlage-2-Gebiete-1-23.pdf "Anlage 2 (Gebiete 1 bis 23) - Gebiete von gemeinschaftlicher Bedeutung") 15.3 MB

3

[Anlage 2 (Gebiete 24 bis 44) - Gebiete von gemeinschaftlicher Bedeutung](/br2/sixcms/media.php/68/GVBl_II_18_2018-Anlage-2-Gebiete-24-44.pdf "Anlage 2 (Gebiete 24 bis 44) - Gebiete von gemeinschaftlicher Bedeutung") 14.1 MB

4

[Anlage 3 - Ökologische Erfordernisse für einen günstigen Erhaltungszustand der natürlichen Lebensraumtypen nach Anhang I der Richtlinie 92/43/EWG](/br2/sixcms/media.php/68/GVBl_II_18_2018-Anlage-3.pdf "Anlage 3 - Ökologische Erfordernisse für einen günstigen Erhaltungszustand der natürlichen Lebensraumtypen nach Anhang I der Richtlinie 92/43/EWG") 318.3 KB

5

[Anlage 4 - Ökologische Erfordernisse für einen günstigen Erhaltungszustand von Tier- und Pflanzenarten nach Anhang II der Richtlinie 92/43/EWG](/br2/sixcms/media.php/68/GVBl_II_18_2018-Anlage-4.pdf "Anlage 4 - Ökologische Erfordernisse für einen günstigen Erhaltungszustand von Tier- und Pflanzenarten nach Anhang II der Richtlinie 92/43/EWG") 262.2 KB

6

[Anlage 5 - Übersichtskarte, Topografische Karten](/br2/sixcms/media.php/68/GVBl_II_18_2018-Anlage-5.pdf "Anlage 5 - Übersichtskarte, Topografische Karten") 200.9 KB