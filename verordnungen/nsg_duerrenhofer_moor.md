## Verordnung über das Naturschutzgebiet „Dürrenhofer Moor“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2 und § 21 Absatz 1 Satz 2 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1  
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Dahme-Spreewald wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Dürrenhofer Moor“.

### § 2  
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 14 Hektar.
Es umfasst Flächen in folgenden Flurstücken:

Gemeinde:

Gemarkung:

Flur:

Flurstück:

Märkische Heide

Dürrenhofe

1

73, 75/1 teilweise, 76, 77, 118 teilweise.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in der topografischen Karte im Maßstab 1 : 10 000 mit dem Titel „Topografische Karte zur Verordnung über das Naturschutzgebiet ‚Dürrenhofer Moor‘“ mit der Blattnummer 1 und in der Liegenschaftskarte zur Verordnung im Maßstab 1 : 2 500 mit dem Titel „Liegenschaftskarte zur Verordnung über das Naturschutzgebiet ‚Dürrenhofer Moor‘“ mit der Blattnummer 1 mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die topografische Karte ermöglicht die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Liegenschaftskarte.
Die Karten sind von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz, am 17. Februar 2012 unterzeichnet worden.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Dahme-Spreewald, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

### § 3  
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes, eines Moorkomplexes zwischen Unterspreewald und Landgrabenniederung, ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Moore und angrenzenden Bruch- und Moorwälder;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Sumpf-Calla (Calla palustris), Rundblättriger Sonnentau (Drosera rotundifolia), Kamm-Wurmfarn (Dryopteris cristata), Wasserfeder (Hottonia palustris), Sumpf-Porst (Ledum palustre), Fieberklee (Menyanthes trifoliata), Weiße Seerose (Nymphaea alba und Nymphaea alba var.
    minor), Torfmoos (Sphagnum spec.) und Ockergelber Wasserschlauch (Utricularia ochroleuca);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Säugetiere, Vögel, Reptilien, Amphibien und Libellen, Hummeln und Tagfalter, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Fischotter (Lutra lutra), Großer Abendsegler (Nyctalus noctula), Bartfledermaus (Myotis brandti), Breitflügelfledermaus (Eptesicus serotinus), Wasserfledermaus (Myotis daubentoni), Zwergfledermaus (Pipistrellus pipistrellus), Fransenfledermaus (Myotis nattereri), Braunes Langohr (Plecotus auritus), Sumpfmaus (Microtus oeconomus), Kranich (Grus grus), Bekassine (Gallinago gallinago), Waldeidechse (Lacerta vivipara), Moorfrosch (Rana arvalis), Kleiner Wasserfrosch (Rana lessonae), Kleine Binsenjungfer (Lestes virens vestalis), Torf-Mosaikjungfer (Aeshna juncea), Kleiner Blaupfeil (Orthetrum coerulescens), Schwarze Heidelibelle (Sympetrum danae), Zwerglibelle (Nehalennia speciosa), Heidehummel (Bombus jonellus), Großer Heufalter (Coenonympha tullia), Trauermantel (Nymphalis antiopa), Großer Perlmuttfalter (Argynnis aglaja), Kleiner Waldportier (Hipparchia alcyone) und Moosbeeren-Bläuling (Vaccinia optilete);
4.  die Erhaltung des in einem Toteiskessel entstandenen Moores aus naturgeschichtlichen Gründen;
5.  die Erhaltung des kleinräumigen Lebensraummosaiks wegen seiner Seltenheit, Vielfalt, besonderen Eigenart sowie seiner hervorragenden Schönheit;
6.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des regionalen Biotopverbundes zwischen dem Unterspreewald und der Landgrabenniederung.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Dürrenhofer Moor“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinen Vorkommen von

1.  Dystrophen Seen und Teichen sowie Übergangs- und Schwingrasenmooren als Biotope von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG);
2.  Waldkiefern-Moorwäldern als prioritärem Biotop („prioritärer Lebensraumtyp“ im Sinne des Anhangs I der Richtlinie 92/43/EWG);
3.  Großem Feuerfalter (Lycaena dispar) und Großer Moosjungfer (Leucorrhinia pectoralis) als Tierarten von gemeinschaftlichem Interesse (im Sinne des Anhangs II der Richtlinie 92/43/EWG), einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten außerhalb der Moore, Feuchtgebiete, Bruchwälder, Röhrichte zum Zweck der Erholung sowie des nichtgewerblichen Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 11;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb von Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit Fahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
12.  Wasserfahrzeuge aller Art zu benutzen;
13.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
14.  Hunde frei laufen zu lassen;
15.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
16.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
17.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
18.  Tiere zu füttern oder Futter bereitzustellen;
19.  Tiere auszusetzen oder Pflanzen anzusiedeln;
20.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
21.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
22.  Pflanzenschutzmittel jeder Art anzuwenden;
23.  Wiesen, Weiden oder sonstiges Grünland umzubrechen oder neu anzusäen.

### § 5  
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass auf den Einsatz von chemisch-synthetischen Düngemitteln, von Gülle und von Herbiziden und Insektiziden verzichtet wird.
    Bei einer Umwandlung von Acker in Grünland gilt § 4 Absatz 2 Nummer 22 und 23.
    Zulässig bleibt die Bewirtschaftung von Ackerflächen nach den Kriterien des ökologischen Landbaus;
2.  die den in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Anforderungen entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  uf Mooren und Moorwäldern keine forstwirtschaftlichen Maßnahmen erfolgen,
    2.  ine Nutzung nur einzelstamm- bis truppweise erfolgt,
    3.  uf den Waldflächen nur Arten der potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
    4.  in Altholzanteil von mindestens 10 Prozent des aktuellen Bestandesvorrates zu sichern ist, wobei mindestens fünf Stämme Altholz je Hektar mit einem Mindestdurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß aus der Nutzung zu nehmen und dauerhaft zu markieren sind,
    5.  e Hektar mindestens fünf Stück stehendes Totholz mit einem Durchmesser von mehr als 30 Zentimeter in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden sowie liegendes Totholz im Bestand verbleibt,
    6.  orstbäume und Höhlenbäume nicht gefällt werden,
    7.  as Befahren des Waldes nur auf Waldwegen und Rückegassen erfolgt,
    8.  4 Absatz 2 Nummer 16 und 22 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen des Dürrenhofer Sees mit der Maßgabe, dass
    1.  er Fischbesatz nur mit heimischen Arten erfolgt; § 13 der Fischereiordnung des Landes Brandenburg bleibt unberührt,
    2.  4 Absatz 2 Nummer 18 gilt,
    3.  anggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung insbesondere des Fischotters weitgehend ausgeschlossen ist;
4.  die rechtmäßige Ausübung der Angelfischerei am Dürrenhofer See mit der Maßgabe, dass
    1.  4 Absatz 2 Nummer 18 und 19 gilt,
    2.  as Betreten und das Befahren von Verlandungsbereichen, Röhrichten und Schwimmblattgesellschaften verboten ist,
    3.  usschließlich die in der in § 2 Absatz 2 genannten topografischen Karte eingezeichneten Angelstellen genutzt werden;
5.  für den Bereich der Jagd:
    1.  ie rechtmäßige Ausübung der Jagd außerhalb der Übergangs- und Schwingrasenmoorflächen mit der Maßgabe, dass  
        aa)   die Fallenjagd mit Lebendfallen erfolgt und innerhalb der Grenzen des Naturschutzgebietes in einem Abstand von bis zu 200 Metern zum Ufer von Gewässern verboten ist.
        Die untere Naturschutzbehörde kann eine Genehmigung für die Fallenjagd mit Lebendfallen innerhalb dieses Abstands erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,  
        bb)   keine Baujagd in einem Abstand von 100 Metern zum Gewässerufer vorgenommen wird,
    2.  ie Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.  
          
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Errichtung anzuzeigen.
        Die Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        Die Entscheidung hierzu soll unverzüglich erfolgen.Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern sind unzulässig, im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Brandenburgischen Jagdgesetzes unberührt;
6.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter die Nummer 8 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
7.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
8.  der Betrieb von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
    Die ordnungsgemäße Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen kann durch Abstimmung eines Unterhaltungsplans erteilt werden;
9.  das Betreten, Lagern und das Baden in der bisherigen Art und im bisherigen Umfang an der Badestelle, die in der in § 2 Absatz 2 genannten topografischen Karte dargestellt ist;
10.  das Befahren des Naturschutzgebietes auf dem in der in § 2 Absatz 2 genannten topografischen Karte gekennzeichneten Weg nur bis zu den ausgewiesenen Stellplätzen und das Abstellen von Kraftfahrzeugen auf den in dieser topografischen Karte dargestellten Stellplätzen in der bisherigen Art und im bisherigen Umfang;
11.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch;
12.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
13.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
14.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
15.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 an Straßen und Wegen freigestellt;
16.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

### § 6  
Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  die moortypischen Pflanzen- und Tierarten sollen durch Pflegemaßnahmen, wie zum Beispiel die Beseitigung verstärkt aufkommender Gehölzsukzession, im Lebensraumtyp Übergangs- und Schwingrasenmoore gefördert werden;
2.  Kiefernreinbestände und nicht standortheimische Forstkulturen sollen langfristig in naturnahe, reich strukturierte Mischwaldbestände mit standortheimischen Baumarten entwickelt werden.

### § 7  
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

### § 8  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

### § 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere §§ 31 bis 33 und 35 des Brandenburgischen Naturschutz-gesetzes, § 30 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

### § 10  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

### § 11   
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 20.
Juni 2012

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

**Anlage  
**(zu § 2 Absatz 1)

![Das Naturschutzgebiet Dürrenhofer Moor liegt im Landkreis Dahme-Spreewald im Bereich der Gemeinde Märkische Heide und umfasst die Flurstücke 73, 75/1 teilweise, 76, 77 und 118 teilweise der Flur 1 in der Gemarkung Dürrenhofe. ](/br2/sixcms/media.php/69/Nr.47-1.JPG "Das Naturschutzgebiet Dürrenhofer Moor liegt im Landkreis Dahme-Spreewald im Bereich der Gemeinde Märkische Heide und umfasst die Flurstücke 73, 75/1 teilweise, 76, 77 und 118 teilweise der Flur 1 in der Gemarkung Dürrenhofe. ")