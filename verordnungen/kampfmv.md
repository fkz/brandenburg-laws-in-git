## Ordnungsbehördliche Verordnung zur Abwehr von Gefahren durch Kampfmittel (Kampfmittelverordnung für das Land Brandenburg - KampfmV)

Auf Grund des § 25 in Verbindung mit § 30 des Ordnungsbehördengesetzes in der Fassung der Bekanntmachung vom 21.
August 1996 (GVBl.
I S. 266), von denen § 25 zuletzt durch Artikel 15 des Gesetzes vom 25.
Januar 2016 (GVBl.
I Nr. 5 S.
18) geändert worden ist, verordnet der Minister des Innern und für Kommunales nach Kenntnisnahme durch den Ausschuss für Inneres und Kommunales des Landtages:

#### § 1 Anwendungsbereich und Begriffsbestimmungen

(1) Diese Verordnung dient der Abwehr von Gefahren, die von Kampfmitteln ausgehen.

(2) Kampfmittel sind gewahrsamslos gewordene, zur Kriegsführung bestimmte Gegenstände militärischer Herkunft und Teile solcher Gegenstände, die

1.  explosionsgefährliche Stoffe oder Rückstände dieser Stoffe enthalten oder aus explosionsgefährlichen Stoffen oder deren Rückständen bestehen, beispielsweise Gewehrpatronen, Granaten, Bomben, Zünder, Minen, Spreng- und Zündmittel,
2.  laborierte Kampf-, Nebel-, Brand- und Reizstoffe oder Rückstände dieser Stoffe enthalten,
3.  Munition sind und keine explosionsgefährlichen Stoffe enthalten, insbesondere Zünder und Zündsysteme, Exerziermunition, Granaten- und Bombenkörper ohne Füllung, oder
4.  Kriegswaffen oder wesentliche Teile von Kriegswaffen sind.

(3) Diese Verordnung gilt nicht für Maßnahmen der Polizei, der Bundeswehr und der Zollverwaltung.

#### § 2 Anzeigepflicht

Jede Person, die Kampfmittel entdeckt oder in Besitz hat, ist verpflichtet, dies unverzüglich der örtlichen Ordnungsbehörde oder der Polizei anzuzeigen.
Satz 1 gilt auch bei Kenntnis von Fund- oder Lagerstätten, an denen vergrabene, verschüttete oder überflutete Kampfmittel liegen.

#### § 3 Verbote

(1) Es ist verboten, nach Kampfmitteln zu suchen, entdeckte Kampfmittel zu berühren, ihre Lage zu verändern oder sie in Besitz zu nehmen sowie sie zu beseitigen oder zu vernichten.

(2) Es ist ferner verboten, Flächen, auf denen Kampfmittel vermutet werden oder entdeckt worden sind und die als Gefahrenbereich gekennzeichnet sind, zu betreten oder Anlagen oder Vorrichtungen zur Kennzeichnung von Gefahrenbereichen zu beschädigen, unwirksam zu machen oder ohne Zustimmung der örtlichen Ordnungsbehörde zu beseitigen.

(3) Die Verbote nach den Absätzen 1 und 2 gelten nicht für die Beschäftigten

1.  des Kampfmittelbeseitigungsdienstes des Zentraldienstes der Polizei oder für die von ihm mit Aufgaben der Kampfmittelbeseitigung beauftragten Dritte sowie
2.  von Unternehmen, die Grundstückseigentümer oder andere Nutzungsberechtigte zur Durchführung der Sondierung, Freilegung und Bergung von Kampfmitteln beauftragt haben.

#### § 4 Anzeigepflicht von Unternehmen

(1) Unternehmen, die über Rechte gemäß der §§ 7 und 20 des Sprengstoffgesetzes in der Fassung der Bekanntmachung vom 10.
September 2002 (BGBl.
I S.
3518), das zuletzt durch Artikel 1 des Gesetzes vom 11.
Juni 2017 (BGBl.
I S.
1586) geändert worden ist, verfügen, sind Maßnahmen der Kampfmittelsuche gestattet.
Sie haben den Beginn der Arbeiten mindestens zwei Wochen vor Aufnahme der Tätigkeiten und das voraussichtliche Ende der Arbeiten unverzüglich der örtlichen Ordnungsbehörde schriftlich oder elektronisch anzuzeigen.
Die örtliche Ordnungsbehörde setzt den Kampfmittelbeseitigungsdienst des Zentraldienstes der Polizei hierüber unverzüglich in Kenntnis.

(2) Anzeigen und Mitteilungen nach Absatz 1 Satz 2 können über den Einheitlichen Ansprechpartner als einheitliche Stelle nach § 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg in Verbindung mit den §§ 71a bis 71e des Verwaltungsverfahrensgesetzes abgewickelt werden.

#### § 5 Ordnungswidrigkeiten

(1) Ordnungswidrig handelt, wer vorsätzlich oder fahrlässig

1.  entgegen § 2 die Entdeckung oder den Besitz von Kampfmitteln oder die Kenntnis von Fund- oder Lagerstellen nicht oder nicht unverzüglich den zuständigen Stellen anzeigt,
2.  entgegen § 3 Absatz 1 nach Kampfmitteln sucht, entdeckte Kampfmittel berührt, ihre Lage verändert oder sie in Besitz nimmt, sie beseitigt oder vernichtet,
3.  entgegen § 3 Absatz 2 Flächen, auf denen Kampfmittel vermutet werden oder entdeckt worden sind und die als Gefahrenbereich gekennzeichnet sind, betritt oder Anlagen oder Vorrichtungen zur Kennzeichnung von Gefahrenbereichen beschädigt, unwirksam macht oder diese ohne Zustimmung der örtlichen Ordnungsbehörde beseitigt,
4.  entgegen § 4 Absatz 1 Satz 2 den Beginn oder das voraussichtliche Ende der Arbeiten nicht oder nicht unverzüglich anzeigt.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße geahndet werden.

(3) Kampfmittel, die durch eine Ordnungswidrigkeit nach Absatz 1 erlangt sind, können eingezogen werden.

#### § 6 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Kampfmittelverordnung für das Land Brandenburg vom 23.
November 1998 (GVBl.
II S.
633), die durch Artikel 3 des Gesetzes vom 7.
Juli 2009 (GVBl.
I S.
262, 266) geändert worden ist, außer Kraft.

Potsdam, den 9.
November 2018

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter