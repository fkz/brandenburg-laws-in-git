## Verordnung über die Bildungsgänge der Berufsfachschule zum Erwerb beruflicher Grundbildung und von gleichgestellten Abschlüssen der Sekundarstufe I (Berufsgrundbildungsverordnung - GrBiBFSV)

Auf Grund des § 26 Absatz 4 in Verbindung mit § 13 Absatz 3, § 56 Satz 1, § 57 Absatz 4, § 58 Absatz 3, § 59 Absatz 9 und § 61 Absatz 3 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
August 2002 (GVBl.
I S. 78), von denen § 13 Absatz 3 durch Artikel 1 des Gesetzes vom 8.
Januar 2007 (GVBl.
I S. 2) und § 56 Satz 1 durch Artikel 2 des Gesetzes vom 14.
März 2014 (GVBl.
I Nr. 14) geändert worden sind, verordnet der Minister für Bildung, Jugend und Sport:

#### § 1 Geltungsbereich und Ziel

(1) Berufsschulpflichtige Schülerinnen und Schüler, die keinen Bildungsgang der Berufsschule besuchen können, erfüllen ihre Berufsschulpflicht im einjährigen Bildungsgang der Berufsfachschule zum Erwerb beruflicher Grundbildung und von gleichgestellten Abschlüssen der Sekundarstufe I (BFS-G).

(2) Berufsschulpflichtige Schülerinnen und Schüler, die keinen Bildungsgang der Berufsschule besuchen können und über keine ausreichenden Deutschkenntnisse verfügen, erfüllen ihre Berufsschulpflicht im zweijährigen Bildungsgang der Berufsfachschule zum Erwerb beruflicher Grundbildung und von gleichgestellten Abschlüssen der Sekundarstufe I (BFS-G-Plus).
Über die Voraussetzung der nicht ausreichenden Deutschkenntnisse entscheidet die Schulleiterin oder der Schulleiter.
Keine ausreichenden Deutschkenntnisse liegen grundsätzlich vor, wenn sie unter dem Niveau B2 nach dem Gemeinsamen europäischen Referenzrahmen für Sprachen liegen.
Für die Entscheidung der Schulleiterin oder des Schulleiters können fachlich gesicherte Feststellungen zu den Deutschkenntnissen von anderen Stellen oder Behörden hinzugezogen werden.

(3) Ziel der Bildungsgänge gemäß den Absätzen 1 und 2 ist es, durch eine Erweiterung der Allgemeinbildung und durch Vermittlung beruflicher Grundkenntnisse und -fertigkeiten sowie Kenntnisse über Formen der Berufsausbildung und Berufsbilder die Voraussetzungen für die Aufnahme einer beruflichen Ausbildung zu verbessern.
Die Entwicklung und Erweiterung der Sprachkompetenz in der deutschen Sprache ist integraler Bestandteil des Bildungsganges gemäß Absatz 2.
Der erfolgreiche Besuch des Bildungsgangs gemäß Absatz 1 führt zu einem der Berufsbildungsreife oder der erweiterten Berufsbildungsreife gleichgestellten Abschluss.
Der erfolgreiche Besuch des Bildungsganges gemäß Absatz 2 führt zu einem der Berufsbildungsreife gleichgestellten Abschluss.

#### § 2 Dauer und Gliederung

(1) Der Bildungsgang gemäß § 1 Absatz 1 wird zu Beginn des Schuljahres eingerichtet und dauert ein Schuljahr.
Reicht nach dem Ende der Orientierungsphase die Anzahl der Schülerinnen und Schüler für die Bildung einer Klasse nicht aus, erfolgt in Abstimmung mit dem für Schule zuständigen Ministerium eine Zuweisung in ein anderes Oberstufenzentrum.

(2) Der Bildungsgang gemäß § 1 Absatz 1 beginnt mit einer Orientierungsphase über einen Zeitraum von zwei Monaten, in der die Schülerinnen und Schüler über die verschiedensten Formen und Möglichkeiten einer Berufsausbildung orientiert, informiert und beraten werden sowie im Zusammenwirken mit den Agenturen für Arbeit, den Industrie- und Handelskammern sowie Handwerkskammern in eine berufliche Ausbildung vermittelt werden können.

(3) Nach dem Ende der Orientierungsphase wird der Unterricht in den Fächern des berufsübergreifenden und des berufsbezogenen Bereichs realisiert.
Entsprechend dem Profil des Oberstufenzentrums, der räumlichen, sächlichen und personellen Ausstattung und der Zusammensetzung der jeweiligen Klasse kann der berufsbezogene Bereich mehrere Berufsfelder umfassen.
Der Unterrichtsumfang ergibt sich aus der Stundentafel gemäß Anlage 1.

(4) Der Bildungsgang gemäß § 1 Absatz 2 wird zu Beginn des Schuljahres eingerichtet und dauert zwei Schuljahre.
Ein Schuljahr kann auf schriftlichen Antrag einmal wiederholt werden, wenn eine erfolgreiche Mitarbeit nicht gewährleistet ist oder der angestrebte Abschluss nicht erreicht wurde.
Über den Antrag entscheidet die Klassenkonferenz.
Die Höchstverweildauer im Bildungsgang darf insgesamt drei Schuljahre nicht überschreiten.
Der Unterricht wird in den Fächern des berufsübergreifenden und des berufsbezogenen Bereichs realisiert.
Der Unterrichtsumfang ergibt sich aus der Stundentafel gemäß Anlage 2.

#### § 3 Unterrichtsorganisation

(1) Der Unterricht im Bildungsgang gemäß § 1 Absatz 1 wird in der Orientierungsphase im Umfang von zwölf Wochenstunden erteilt.
Dabei sind die vom für Schule zuständigen Ministerium vorgegebenen Themenfelder zu bearbeiten.
Nach dem Ende der Orientierungsphase wird der Unterricht in den Fächern der Stundentafel gemäß Anlage 1 im Klassenverband oder in Kursen erteilt.

(2) Der Unterricht im Bildungsgang gemäß § 1 Absatz 2 erfolgt in den Fächern der Stundentafel gemäß Anlage 2 im Klassenverband oder in Kursen.

(3) Für den Unterricht im berufsübergreifenden Bereich gelten die für diese Fächer gültigen Rahmenlehrpläne für die Bildungsgänge der Berufsschule.
Im Bildungsgang gemäß § 1 Absatz 1 wird das Fach Mathematik nach den Inhalten und Anforderungen der Jahrgangsstufe 9 des Rahmenlehrplans für die Jahrgangsstufen 1 bis 10 für den Bildungsgang zum Erwerb der erweiterten Berufsbildungsreife unterrichtet.
Im Bildungsgang gemäß § 1 Absatz 2 werden die Fächer Mathematik und Deutsch nach den Inhalten und Anforderungen der Jahrgangsstufe 9 des Rahmenlehrplans für die Jahrgangsstufen 1 bis 10 für den Bildungsgang zum Erwerb der erweiterten Berufsbildungsreife unterrichtet.
Der Wahlpflichtbereich wird vor allem für die Fächer des berufsübergreifenden Bereichs zur Stützung, Vertiefung und Erweiterung genutzt.
Abhängig von den unterschiedlichen Bildungsvoraussetzungen können auch andere Fächer oder Kurse angeboten werden.

(4) Der Unterricht im berufsbezogenen Bereich soll entsprechend den räumlichen, sächlichen und personellen Bedingungen des Oberstufenzentrums als fachpraktischer Unterricht realisiert werden.
Dafür gelten die Unterrichtsvorgaben des für Schule zuständigen Ministeriums oder schulinterne Rahmenlehrpläne, die vom staatlichen Schulamt genehmigt wurden.

#### § 4 Aufnahmevoraussetzungen

(1) In den Bildungsgang gemäß § 1 Absatz 1 wird aufgenommen, wer

1.  im Land Brandenburg berufsschulpflichtig ist und
2.  zum Zeitpunkt des Beginns des Unterrichts in diesem Bildungsgang keinen vollzeitschulischen weiterführenden Bildungsgang und keinen Bildungsgang der Berufsschule besucht.

(2) In den Bildungsgang gemäß § 1 Absatz 2 wird aufgenommen, wer

1.  im Land Brandenburg berufsschulpflichtig ist,
2.  zum Zeitpunkt des Beginns des Unterrichts in diesem Bildungsgang keinen vollzeitschulischen weiterführenden Bildungsgang und keinen Bildungsgang der Berufsschule besucht und
3.  über keine ausreichenden Deutschkenntnisse verfügt.

(3) Treten die Voraussetzungen gemäß den Absätzen 1 und 2 für einzelne Schülerinnen oder Schüler im Verlauf des Schuljahres auf, erfolgt die Aufnahme zu diesem Zeitpunkt.
Für den Bildungsgang gemäß § 1 Absatz 2 betrifft diese Möglichkeit nur das erste Schuljahr.

(4) Abweichend von Absatz 1 Nummer 1 und Absatz 2 Nummer 1 ist eine Aufnahme nur nach Maßgabe freier Plätze möglich.

#### § 5 Aufnahme

(1) Die Schülerinnen und Schüler besuchen in der Regel das für ihre Wohnung oder den gewöhnlichen Aufenthalt zuständige Oberstufenzentrum.
Bei Erschöpfung der Aufnahmekapazität erfolgt durch das staatliche Schulamt eine Zuweisung gemäß § 50 Absatz 4 des Brandenburgischen Schulgesetzes.

(2) Im Ausnahmefall kann das staatliche Schulamt gemäß § 106 Absatz 4 des Brandenburgischen Schulgesetzes den Besuch eines anderen Oberstufenzentrums gestatten, insbesondere wenn

1.  das zuständige Oberstufenzentrum nur unter besonderen Schwierigkeiten erreicht werden kann,
2.  gewichtige pädagogische Gründe hierfür sprechen oder
3.  besondere soziale Gründe vorliegen und
4.  die Aufnahmekapazität des anderen Oberstufenzentrums nicht erschöpft ist.

#### § 6 Leistungsbewertung

(1) Die Leistungen der Schülerinnen und Schüler werden durch Noten bewertet.
Die Leistungsbewertung erfolgt gemäß § 57 Absatz 2 und 3 des Brandenburgischen Schulgesetzes.

(2) Die Leistungsnachweise können insbesondere durch schriftliche Arbeiten, Referate, Hausarbeiten und andere geeignete Formen, auch fächerübergreifend und projektspezifisch, erbracht werden.

(3) Pro Schulhalbjahr ist mindestens ein erforderlicher Leistungsnachweis in jedem Fach vorzusehen.
Neben schriftlichen Klassenarbeiten können auch gleichwertige Leistungsnachweise für praktische Tätigkeiten oder für eine Kombination von praktischen und schriftlichen oder mündlichen Aufgaben vorgesehen werden.

(4) Grundsätze für die Beobachtung und Bewertung der Lernentwicklung sowie für die Koordinierung der Leistungsbeurteilung beschließt die Abteilungskonferenz für die Angelegenheiten des Bildungsganges oder die Fach- oder Lernbereichskonferenz für die jeweiligen fachlichen Angelegenheiten.

(5) Wer aus nicht selbst zu vertretenden Gründen einen erforderlichen Leistungsnachweis nicht erbracht hat, kann diesen durch eine schriftliche oder mündliche Leistungsfeststellung nachholen.
Die Entscheidung trifft die im Fach unterrichtende Lehrkraft.

(6) Für Schülerinnen und Schüler mit einer erheblichen Sprachauffälligkeit, Sinnes- oder Körperbehinderung kann ein Förderausschuss gemäß der Sonderpädagogik-Verordnung eine Empfehlung zum spezifischen Umgang mit der Leistungsbewertung erarbeiten, um Nachteile auszugleichen, die sich aus Art und Umfang der jeweiligen Behinderung ergeben.
Die Leistungsanforderungen müssen den Zielsetzungen der Rahmenlehrpläne und des besuchten Bildungsganges entsprechen.

#### § 7 Verweigerung und Täuschung bei Leistungsnachweisen

(1) Versäumt eine Schülerin oder ein Schüler aus selbst zu vertretenden Gründen einen Leistungsnachweis oder wird ein Leistungsnachweis verweigert, so wird die Note „ungenügend“ erteilt.

(2) Wer sich bei einer Leistungsfeststellung unerlaubter Hilfen bedient, begeht eine Täuschung.
Art und Umfang sind von der im jeweiligen Fach unterrichtenden Lehrkraft festzustellen.
Gleiches gilt für Täuschungsversuche sowie Beihilfe zur Täuschung.

(3) Die Lehrkraft entscheidet, ob bei geringerer Schwere der Täuschung der ohne Täuschung geleistete Teil des Leistungsnachweises bewertet und der übrige Teil als nicht geleistet gewertet wird.
Bei erheblichen Täuschungen wird die gesamte Leistung mit „ungenügend“ bewertet.
Lässt sich der Umfang der Täuschung nicht feststellen, wird der Leistungsnachweis wiederholt.

#### § 8 Erteilung von Abschlüssen, Ausgleichsleistungen

(1) Der jeweilige Bildungsgang ist erfolgreich abgeschlossen, wenn in allen Fächern der Stundentafel ausreichende Leistungen erreicht wurden oder ein Ausgleich nach Absatz 2 möglich ist.
Die Note im Fach Sport bleibt für die Feststellung des Abschlusses und für einen gegebenenfalls notwendigen Notenausgleich unberücksichtigt.

(2) Mangelhafte Leistungen in bis zu zwei Fächern können durch jeweils mindestens befriedigende Leistungen in je einem anderen Fach ausgeglichen werden.
Ungenügende Leistungen können jeweils durch mindestens gute Leistungen entsprechend Satz 1 ausgeglichen werden.
Nicht ausreichende Leistungen in mehr als zwei Fächern können nicht ausgeglichen werden.
Im Bildungsgang gemäß § 1 Absatz 2 können nicht ausreichende Leistungen in den Fächern Deutsch und Mathematik nicht ausgeglichen werden.

(3) Den erfolgreichen Abschluss und die Erteilung gleichwertiger Abschlüsse stellt die Klassenkonferenz fest.
Wer in den Bildungsgang gemäß § 1 Absatz 1 nach Beginn des zweiten Schulhalbjahres eintritt, kann nur dann einen Abschluss gemäß den Absätzen 4 und 5 erwerben, wenn zuvor an einem Unterricht mit mindestens zwölf Unterrichtsstunden pro Woche mit mindestens ausreichenden Leistungen in allen Fächern teilgenommen wurde.
Im Bildungsgang gemäß § 1 Absatz 2 kann nur dann ein Abschluss gemäß Absatz 4 erworben werden, wenn die Leistungen im zweiten Schuljahr mindestens ausreichend sind.
Für die Bewertung des Abschlusses werden die Leistungen des zweiten Schuljahres herangezogen.

(4) Einen der Berufsbildungsreife gleichgestellten Abschluss erwirbt, wer den Bildungsgang erfolgreich abgeschlossen hat.

(5) Einen der erweiterten Berufsbildungsreife gleichgestellten Abschluss im Bildungsgang gemäß § 1 Absatz 1 erwirbt, wer bei Eintritt in den Bildungsgang die Berufsbildungsreife bereits erworben hatte und den Bildungsgang erfolgreich beendet oder den Bildungsgang mit mindestens guten Leistungen in allen Fächern abschließt.
Absatz 1 Satz 2 gilt entsprechend.

#### § 9 Zeugnisse

(1) Ein Abschlusszeugnis erhält, wer den jeweiligen Bildungsgang erfolgreich abschließt.
Das Zeugnis trägt das Datum des Ausgabetages.

(2) Ein Abgangszeugnis erhält, wer den jeweiligen Bildungsgang ohne Abschluss verlässt.
Das Zeugnis trägt das Datum des Ausgabetages.

#### § 10 Übergangsbestimmungen

Schülerinnen und Schüler, die mit Beginn des Schuljahres 2017/2018 in den Bildungsgang gemäß § 1 Absatz 2 aufgenommen wurden, absolvieren den Bildungsgang auf der Grundlage der Berufsgrundbildungsverordnung in der Fassung vom 1.
März 2016 (GVBl.
II Nr.
8).
Abweichend von Satz 1 findet § 3 Absatz 4 der geltenden Fassung Anwendung.

#### § 11 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Februar 2016 in Kraft.
Gleichzeitig tritt die Berufsgrundbildungsverordnung vom 16.
Juni 1998 (GVBl.
II S. 442) außer Kraft.

Potsdam, den 1.
März 2016

Der Minister für Bildung,  
Jugend und Sport

Günter Baaske

* * *

### Anlagen

1

[Anlage 1](/br2/sixcms/media.php/68/GVBl_II_08_2016-Anlage-1.pdf "Anlage 1") 576.6 KB

2

[Anlage 2](/br2/sixcms/media.php/68/GVBl_II_08_2016-Anlage-2.pdf "Anlage 2") 583.5 KB