## Verordnung zur Festsetzung von Stellenobergrenzen im Land Brandenburg (Brandenburgische Stellenobergrenzenverordnung - BbgStogV)

_Hinweis: Artikel 5 tritt gemäß Artikel 6 Absatz 4 der Verordnung vom 2.
August 2019 (GVBl. II Nr. 56 S.
3) am 1.
Januar 2023 in Kraft._

Auf Grund des § 24 Absatz 3 des Brandenburgischen Besoldungsgesetzes vom 20. November 2013 (GVBl.
I Nr. 32 S. 2, Nr. 34) verordnet die Landesregierung:

#### § 1 Geltungsbereich

(1) Diese Verordnung gilt für das Land und die der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts mit Ausnahme der Gemeinden und Gemeindeverbände sowie des Kommunalen Versorgungsverbandes Brandenburg.
Sie regelt die Stellenobergrenzen für Beamtinnen und Beamte der in Satz 1 bezeichneten Dienstherren.

(2) Diese Verordnung gilt nicht für die obersten Landesbehörden, für Lehrerinnen und Lehrer und pädagogisches Hilfspersonal an öffentlichen Schulen und Hochschulen, für Beamtinnen und Beamte des Schulaufsichtsdienstes sowie für Lehrkräfte an verwaltungsinternen Fachhochschulen.

#### § 2 Bewertungs- und Berechnungsgrundsätze

(1) Die Stellenobergrenzen dürfen nur ausgeschöpft werden, wenn dies nach sachgerechter Bewertung der Funktionen im Einzelfall gerechtfertigt ist.
Maßstab dafür sind die Grundsätze der §§ 18 und 24 Absatz 1 des Brandenburgischen Besoldungsgesetzes.

(2) Bei der Berechnung der Stellenobergrenzen sind die sich ergebenden Stellenbruchteile unter 0,5 abzurunden und Stellenbruchteile von 0,5 und mehr aufzurunden.

#### § 3 Planstellen

(1) Die Prozentsätze für die Stellenobergrenzen beziehen sich auf die Gesamtzahl aller Planstellen der Laufbahnen mit denselben Stellenobergrenzen, im höheren Dienst auf die Gesamtzahl der Planstellen in den Besoldungsgruppen A 13 bis A 16 und B 2.

(2) Planstellen, die als „künftig umzuwandeln“ (ku) bezeichnet sind, sind der Laufbahn- oder Besoldungsgruppe zuzurechnen, der sie nach der Umwandlung angehören werden.

(3) Planstellen, die als „künftig wegfallend“ (kw) bezeichnet sind, sind rechnerisch zu berücksichtigen, solange sie besetzt sind.

(4) Die für dauernd beschäftigte Tarifbeschäftigte ausgebrachten gleichwertigen Stellen sind mit der Maßgabe in die Berechnungsgrundlage einzubeziehen, dass eine entsprechende Anrechnung auf die jeweiligen Stellen für Beförderungsämter erfolgt.
Das für das Besoldungsrecht zuständige Ministerium kann Ausnahmen zulassen.

#### § 4 Stellenobergrenzen

Die Anteile der Beförderungsämter für Beamtinnen und Beamte dürfen nach Maßgabe sachgerechter Bewertung folgende Stellenobergrenzen nicht überschreiten:

1.  im mittleren Dienst  
    in der Besoldungsgruppe A 9
    
    *   im allgemeinen Vollzugsdienst,
    
    Werkdienst und Krankenpflegedienst  
    bei den Vollzugsanstalten                                                                                40 Prozent,
    *   im Gerichtsvollzieherdienst                                                                  70 Prozent,
    *   im Polizeivollzugsdienst                                                                      70 Prozent,
    *   in der Steuerverwaltung                                                                       45 Prozent,
    *   in allen übrigen Laufbahnen                                                                 20 Prozent;
2.  im gehobenen Dienst  
    in der Besoldungsgruppe A 12
    
    *   im Amtsanwaltsdienst                                                                         40 Prozent,
    *   im technischen Dienst                                                                         35 Prozent,
    *   in allen übrigen Laufbahnen                                                                  25 Prozent;
    
    in der Besoldungsgruppe A 13  
    *   im Amtsanwaltsdienst                                                                         60 Prozent,
    *   in allen übrigen Laufbahnen                                                                  15 Prozent;
3.  im höheren Dienst  
    in der Besoldungsgruppe A 15
    
    *   im technischen Dienst                                                                         35 Prozent,
    *   in allen übrigen Laufbahnen                                                                  30 Prozent;
    
    in den Besoldungsgruppen A 16 und B 2 zusammen  
    *   in der Steuerverwaltung                                                                        15 Prozent,
    *   in allen übrigen Laufbahnen                                                                  10 Prozent.

#### § 5 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Stellenobergrenzenverordnung vom 3. Dezember 2007 (GVBl.
II S.
496), die durch die Verordnung vom 25. Juni 2012 (GVBl.
II Nr.
51) geändert worden ist, außer Kraft.

Potsdam, den 14.
Juli 2015

Die Landesregierung des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister der Finanzen

Christian Görke