## Verordnung zur Übertragung von Aufgaben nach dem Prämien- und Ehrenzeichengesetz (Prämien- und Ehrenzeichenzuständigkeitsverordnung - PrämEhrZustV)

Auf Grund des § 3 Absatz 2 Satz 3, des § 9 Absatz 1 Satz 4 und Absatz 2 Satz 5 sowie des § 13 Absatz 1 Satz 5 und Absatz 2 Satz 3 des Prämien- und Ehrenzeichengesetzes vom 30. April 2019 (GVBl. I Nr. 9) verordnet der Minister des Innern und für Kommunales:

#### § 1 Aufgabenübertragung

Die Aufgaben der Bewilligungsbehörde gemäß § 3 Absatz 2, § 9 Absatz 1, 2 und 5 sowie § 13 Absatz 1, 2 und 4 des Prämien- und Ehrenzeichengesetzes werden auf die Landesschule und Technische Einrichtung für Brand- und Katastrophenschutz übertragen.

#### § 2 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 7.
Mai 2019

Der Minister der Innern und für Kommunales

Karl-Heinz Schröter