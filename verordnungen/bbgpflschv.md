## Verordnung zur Neuregelung pflanzenschutzrechtlicher Vorschriften im Land Brandenburg (Brandenburgische Pflanzenschutzverordnung - BbgPflSchV)

Auf Grund des § 9 Absatz 7 Satz 1 in Verbindung mit Absatz 6 Nummer 2, des § 10 Satz 2, des § 16 Absatz 5 Satz 1 und 2 in Verbindung mit Absatz 4 Satz 1 Nummer 3, des § 24 Absatz 1 Satz 2 des Pflanzenschutzgesetzes vom 6.
Februar 2012 (BGBl.
I S. 148, 1281), von denen § 9 Absatz 6 durch Artikel 375 Nummer 4 und § 16 Absatz 4 Satz 1 und Absatz 5 Satz 1 durch Artikel 375 Nummer 6 des Gesetzes vom 31.
August 2015 (BGBl.
I S.
1474, 1529) geändert worden sind, in Verbindung mit § 2 Satz 1 der Pflanzenschutzzuständigkeitsverordnung vom 11.
Februar 2014 (GVBl.
II Nr. 9) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Anzeige nach § 10 des Pflanzenschutzgesetzes bei Beratung und Anwendung

(1) Die Anzeige eines Unternehmens nach § 10 Satz 1 des Pflanzenschutzgesetzes muss bei der zuständigen Behörde eingereicht werden.
Sie muss folgende Angaben enthalten:

1.  Name, Anschrift und Telekommunikationsdaten des Betriebes sowie des Betriebsinhabers oder des Geschäftsführers,
2.  Name, Anschrift und den Sachkundenachweis gemäß § 9 Absatz 1 Nummer 1 bis 3 des Pflanzenschutzgesetzes der Personen,
    1.  unter deren Leitung die Anwendung der Pflanzenschutzmittel oder die Beratung zum Pflanzenschutz erfolgen soll,
    2.  die Pflanzenschutzmittel anwenden oder zum Pflanzenschutz beraten,
3.  die Angabe, ob die Anwendung auf landwirtschaftlich, forstwirtschaftlich oder gärtnerisch genutzten Freilandflächen oder auf anderen Flächen erfolgen soll,
4.  Angaben zur Art des Betriebes.

(2) Änderungen der Angaben nach Absatz 1 oder die Beendigung der Tätigkeit im Sinne des § 10 Satz 1 des Pflanzenschutzgesetzes sind der zuständigen Behörde unverzüglich mitzuteilen.

#### § 2 Anzeige nach § 24 Absatz 1 des Pflanzenschutzgesetzes bei Abgabe von Pflanzenschutzmitteln

(1) Die Anzeige eines Unternehmens nach § 24 Absatz 1 Satz 1 des Pflanzenschutzgesetzes muss bei der zuständigen Behörde eingereicht werden.
Sie muss folgende Angaben enthalten:

1.  Name, Anschrift und Telekommunikationsdaten des Betriebes sowie des Betriebsinhabers oder des Geschäftsführers,
2.  den Ort der Tätigkeit,
3.  den Sachkundenachweis gemäß § 9 Absatz 1 Nummer 4 und 5 des Pflanzenschutzgesetzes,
4.  Name, Anschrift und Telekommunikationsdaten der Personen,
    1.  die Pflanzenschutzmittel in Verkehr bringen,
    2.  einführen oder
    3.  innergemeinschaftlich verbringen und
5.  die Angabe, ob Pflanzenschutzmittel über das Internet in Verkehr gebracht werden.

(2) Änderungen der Angaben nach Absatz 1 oder die Beendigung der Tätigkeit im Sinne des § 24 Absatz 1 Satz 1 des Pflanzenschutzgesetzes des registrierten Unternehmens sind der zuständigen Behörde unverzüglich mitzuteilen.

#### § 3 Durchführung der Pflanzenschutzsachkundeprüfung

(1) Die zuständige Behörde beschließt die Prüfungsaufgaben auf der Grundlage von § 3 der Pflanzenschutz-Sachkundeverordnung vom 27.
Juni 2013 (BGBl.
I S.
1953), die zuletzt durch Artikel 376 der Verordnung vom 31.
August 2015 (BGBl.
I S.
1474, 1530) geändert worden ist, in der jeweils geltenden Fassung.

(2) Die Prüfung ist nicht öffentlich.

(3) In der schriftlichen Prüfung ist ein Fragebogen mit 60 Fragen innerhalb von 60 Minuten zu bearbeiten.

(4) Die Voraussetzung zur Zulassung der mündlichen und fachpraktischen Prüfung ist das Bestehen der schriftlichen Prüfung.

#### § 4 Anerkennung der Kontrollwerkstätten

(1) Funktionsprüfungen von in Gebrauch befindlichen Pflanzenschutzgeräten gemäß § 3 Absatz 1 Satz 2 der Pflanzenschutz-Geräteverordnung vom 27.
Juni 2013 (BGBl.
I S.
1953, 1962) in der jeweils geltenden Fassung sind durch eine amtlich anerkannte Kontrollwerkstatt oder Kontrollperson durchzuführen.
Die Anerkennung erfolgt, wenn gewährleistet ist, dass die Funktionsprüfung genau und zuverlässig gemäß § 3 Absatz 2 der Pflanzenschutz-Geräteverordnung durchgeführt wird, insbesondere

1.  in ausreichendem Maß fachlich geeignetes Personal nach Maßgabe der Anlage 1 Nummer 1,
2.  die erforderliche Kontrollausrüstung nach Maßgabe der Anlage 1 Nummer 2 und
3.  ein geeigneter Kontrollort nach Maßgabe der Anlage 1 Nummer 3 vorhanden sind.

Die Anerkennung kann mit Nebenbestimmungen erlassen werden.
Diese können Vorgaben zur Dokumentation der durchgeführten Kontrollen, Aufbewahrung der Kontrollberichte und zur Information der zuständigen Behörde über durchgeführte Kontrollen enthalten.

(2) Ist die Anerkennung in einem anderen Bundesland, Mitgliedstaat der Europäischen Union oder einem Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum erfolgt, so ist vor Aufnahme der Tätigkeit in Brandenburg diese Anerkennung bei der zuständigen Behörde anzuzeigen.

#### § 5 Prüfplaketten, Schilder

Die anerkannten Kontrollwerkstätten sind befugt,

1.  Prüfplaketten gemäß § 5 Absatz 1 in Verbindung mit Anlage 6 der Pflanzenschutzgeräte-Verordnung nach dem Muster der Anlage 2 zu vergeben,
2.  Schilder nach dem Muster der Anlage 3 zu führen.

#### § 6 Pflichten der Kontrollwerkstätten

Die Kontrollwerkstätten sind verpflichtet,

1.  den Beauftragten der zuständigen Behörde während der ortsüblichen Geschäftszeit jederzeit Zugang zu den Kontrolleinrichtungen und -unterlagen zu gestatten und diesen Auskünfte zum Prüfablauf zu erteilen,
2.  Änderungen bei Betriebsführung, Kontrollpersonal, Prüfeinrichtungen sowie das Ruhen oder die Aufgabe der Kontrolltätigkeit der zuständigen Behörde unverzüglich anzuzeigen,
3.  über die Verwendung der Prüfplaketten einen Nachweis zu führen, der fünf Jahre aufzubewahren ist,
4.  die Prüftermine- und orte, sowie die zu prüfenden Geräteklassen der zuständigen Behörde mindestens sieben Tage vor dem Prüftermin bekannt zu geben.

#### § 7 Widerruf der Anerkennung als Kontrollwerkstatt

Die zuständige Behörde kann die Zulassung als Kontrollwerkstatt widerrufen, wenn eine der Anerkennungsvoraussetzungen nicht mehr erfüllt ist oder erhebliche Verstöße gegen die Pflichten der Kontrollwerkstatt vorliegen oder festgestellte Verstöße trotz der Aufforderung der zuständigen Behörde zur Beseitigung fortbestehen.

#### § 8 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig treten die Brandenburgische Pflanzenschutzsachkundeverordnung vom 13.
Juli 1995 (GVBl.
II S.
514), die durch Verordnung vom 6. Mai 1996 (GVBl.
II S.
397) geändert worden ist, die Verordnung über die Ausbringung von Pflanzenschutzmitteln unter Verwendung von Luftfahrzeugen vom 25.
März 1994 (GVBl.
II S.
286), die durch Verordnung vom 20.
Mai 1994 (GVBl.
II S.
470) geändert worden ist, und die Verordnung über die amtliche Anerkennung von Kontrollbetrieben zur Durchführung der Funktionsprüfung von im Gebrauch befindlichen Pflanzenschutzgeräten vom 8.
Juli 1992 (GVBl.
II S.
396) außer Kraft.

Potsdam, den 4.
September 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 4 Absatz 1) - Voraussetzungen für die Anerkennung als Kontrollwerkstatt](/br2/sixcms/media.php/68/GVBl_II_60_2018-Anlage-1.pdf "Anlage 1 (zu § 4 Absatz 1) - Voraussetzungen für die Anerkennung als Kontrollwerkstatt") 150.2 KB

2

[Anlage 2 (zu § 5 Nummer 1) - Muster der Plakette](/br2/sixcms/media.php/68/GVBl_II_60_2018-Anlage-2.pdf "Anlage 2 (zu § 5 Nummer 1) - Muster der Plakette") 126.8 KB

3

[Anlage 3 (zu § 5 Nummer 2) - Anerkennungsschilder für die Kontrollwerkstätten](/br2/sixcms/media.php/68/GVBl_II_60_2018-Anlage-3.pdf "Anlage 3 (zu § 5 Nummer 2) - Anerkennungsschilder für die Kontrollwerkstätten") 133.3 KB