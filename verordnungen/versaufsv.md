## Verordnung zu den Grundsätzen der Geschäftsführung und Versicherungsaufsicht über die berufsständischen Versorgungseinrichtungen im Land Brandenburg (Versicherungsaufsichtsverordnung - VersAufsV)

Auf Grund:

*   des § 29 Absatz 9 des Heilberufsgesetzes vom 28.
    April 2003 (GVBl.
    I S. 126), der durch Artikel 2 Nummer 2 des Gesetzes vom 25.
    April 2017 (GVBl.
    I Nr. 10) eingefügt worden ist,
*   des § 19 Absatz 6 des Brandenburgischen Rechtsanwaltsversorgungsgesetzes vom 4. Dezember 1995 (GVBl.
    I S.
    266), der durch Artikel 1 Nummer 9 des Gesetzes vom 25. April 2017 (GVBl.
    I Nr.
    10) eingefügt worden ist, sowie
*   des § 22 Absatz 6 des Brandenburgischen Steuerberaterversorgungsgesetzes vom 18. Dezember 2001 (GVBl.
    I S.
    290), der zuletzt durch Artikel 3 Nummer 2 des Gesetzes vom 25. April 2017 (GVBl.
    I Nr.
    10) geändert worden ist,

verordnet die Ministerin der Finanzen und für Europa:

#### § 1 Grundlagen des Geschäftsbetriebs, Anwendung der Anlageverordnung

(1) Die berufsständischen Versorgungseinrichtungen dürfen nur solche Geschäfte betreiben, die in unmittelbarem Zusammenhang mit der Erfüllung ihres gesetzlichen und satzungsmäßigen Auftrags stehen.

(2) Die berufsständischen Versorgungseinrichtungen stellen einen Geschäftsplan auf, der insbesondere die für die Herleitung der dauerhaften Erfüllung der rechtlichen Leistungsverpflichtungen erforderlichen versicherungstechnischen Rückstellungen und finanziellen Grundlagen darstellt.

Von der Versicherungsaufsicht sind zu genehmigen:

1.  die Änderungen der Grundsätze für die Berechnung ausreichender Rückstellungen, einschließlich der verwendeten Rechnungsgrundlagen und mathematischen Formeln sowie die Grundsätze der Überschussrechnung;
2.  der erstmalige Abschluss von Verträgen, durch die die Bestandsverwaltung, die Leistungsbearbeitung, das Rechnungswesen, die Vermögensanlage oder die Vermögensverwaltung ganz oder zu einem wesentlichen Teil einem anderen Unternehmen auf Dauer übertragen werden
3.  und die Einführung einer Rückversicherung oder die Änderung einer bestehenden Rückversicherung.

Änderungen von Verträgen im Sinne der Nummer 2 sind der Versicherungsaufsicht unverzüglich anzuzeigen.

(3) Die berufsständischen Versorgungseinrichtungen haben der Versicherungsaufsicht jährlich ein versicherungsmathematisches Gutachten zur dauerhaften Erfüllbarkeit der Leistungsverpflichtungen vorzulegen.
Darüber hinaus soll das Gutachten Aussagen zur Plausibilität der in Absatz 2 Satz 3 Nummer 1 genannten Rechnungsgrundlagen enthalten.

(4) Die berufsständischen Versorgungseinrichtungen wenden die Anlageverordnung an.
Dabei sind die Vermögenswerte so anzulegen, dass möglichst große Sicherheit und Rentabilität bei jederzeitiger Liquidität der berufsständischen Versorgungseinrichtung unter Wahrung angemessener Mischung und Streuung der Vermögenswerte erreicht werden.
Abweichungen von den in der Anlageverordnung zugelassenen Kapitalanlageformen und deren jeweiligen Höchstsätzen kann die Versicherungsaufsicht allgemein oder im Einzelfall und, sofern geboten, auch befristet zulassen.

(5) Die Anlage der Vermögenswerte hat nach internen Anlagegrundsätzen und einer darauf beruhenden Anlagestrategie zu erfolgen.
Allgemein anerkannte Grundsätze der Nachhaltigkeit von Vermögensanlagen sollen beachtet werden.

#### § 2 Kapitalausstattung, Risikomanagement

(1) Die berufsständischen Versorgungseinrichtungen haben zur Sicherstellung der dauernden Erfüllbarkeit ihrer Leistungsverpflichtungen freie unbelastete Eigenmittel mindestens in Höhe einer Solvabilitätsspanne zu bilden.

(2) Als freie unbelastete Eigenmittel sind anzusehen:

1.  die Verlustrücklage,
2.  der Anteil der Rückstellung für Beitragsrückerstattungen, die noch nicht für die Überschussverteilung festgelegt sind,
3.  stille Reserven, soweit diese nicht Ausnahmecharakter tragen.

Von der Summe der sich danach ergebenden Beträge sind ein Verlustvortrag und die in der Bilanz ausgewiesenen immateriellen Werte abzusetzen.

(3) Die Höhe der Solvabilitätsspanne bemisst sich nach den Risiken des gesamten Geschäftsbetriebs und soll mindestens 2,5 Prozent der Deckungsrückstellung betragen.
Das Nähere regelt die Versicherungsaufsicht.
Die berufsständischen Versorgungseinrichtungen können eigene Modelle entwickeln, die der Zustimmung der Versicherungsaufsicht bedürfen.

(4) Die berufsständischen Versorgungseinrichtungen müssen über ein angemessenes Risikomanagement verfügen, zu dem die Identifikation und Bewertung von Risiken, die Beurteilung der Risikotragfähigkeit, eine Risikostreuung und eine Risikostrategie gehören.

Sofern die Konzentration von Anlagen in einem oder mehreren, von ein und demselben verantwortlichen Portfoliomanager verwalteten Investmentvermögen 20 Prozent des Nominalwertes des Anlagevermögens übersteigt, ist das Managerrisiko zu bewerten.

#### § 3 Rechnungslegung, Berichterstattung

(1) Die berufsständischen Versorgungseinrichtungen haben über ihre gesamten Vermögensanlagen, aufgegliedert in Neuanlagen und Bestände, zu berichten.
Für die Berichterstattung gilt jeweils der letzte Geschäftstag der Monate März, Juni, September und Dezember als Stichtag.
Die Versicherungsaufsicht legt Form und Frist der Berichterstattung im Benehmen mit dem einzelnen Versorgungswerk fest.

(2) Der Bericht über die Vermögensanlagen hat sich auch auf die Beachtung der in den §§ 3 und 4 der Anlageverordnung getroffenen Vorgaben über die Mischung und Streuung der Vermögensanlagen zu erstrecken.

(3) Zum Geschäftsjahresende ist dem Bericht über die Vermögensanlagen ein Vermögensverzeichnis gemäß § 126 Absatz 2 des Versicherungsaufsichtsgesetzes beizufügen.

#### § 4 Jahresabschlussprüfung

(1) Die berufsständischen Versorgungseinrichtungen haben den Jahresabschluss und den Lagebericht gemäß § 341 k Absatz 1 und 3 des Handelsgesetzbuchs prüfen zu lassen.

(2) Die oder der von der berufsständischen Versorgungseinrichtung mit der Prüfung des Jahresabschlusses beauftragte Abschlussprüferin oder Abschlussprüfer ist der Versicherungsaufsicht unverzüglich anzuzeigen.
Die Versicherungsaufsicht kann, wenn sie gegen die beauftragte Person begründete Bedenken hat, innerhalb eines Monats nach Eingang der Anzeige verlangen, dass innerhalb einer angemessenen Frist eine andere Abschlussprüferin oder ein anderer Abschlussprüfer bestimmt wird.
Unterbleibt das oder hat die Versicherungsaufsicht auch gegen die neu mit dem Jahresabschluss beauftragte Person Bedenken, so hat sie diese selbst zu bestimmen.
In diesem Fall gilt § 318 Absatz 1 Satz 4 des Handelsgesetzbuchs mit der Maßgabe, dass die berufsständische Versorgungseinrichtung den Prüfungsauftrag unverzüglich der von der Versicherungsaufsicht bestimmten Person zu erteilen hat.

(3) Für die inhaltliche Ausgestaltung der Prüfungsberichte zu den Jahresabschlüssen der berufsständischen Versorgungseinrichtungen gilt die Prüfungsberichtsverordnung.

(4) Auf Verlangen der Versicherungsaufsicht hat die mit der Prüfung des Jahresabschlusses beauftragte Person auch sonstige, bei der Prüfung bekannt gewordene Tatsachen mitzuteilen, die gegen eine ordnungsgemäße Durchführung der Geschäfte der berufsständischen Versorgungseinrichtung sprechen.

(5) Die berufsständischen Versorgungseinrichtungen haben der Versicherungsaufsicht den Jahresabschluss nebst Lagebericht sowie das versicherungsmathematische Gutachten unverzüglich nach der Feststellung des Jahresabschlusses vorzulegen.
Die Versicherungsaufsicht kann den Bericht mit der mit der Prüfung des Jahresabschlusses beauftragten Person erörtern und, wenn nötig, Ergänzungen der Prüfung und des Berichts auf Kosten der berufsständischen Versorgungseinrichtungen veranlassen.
Die Versicherungsaufsicht kann der berufsständischen Versorgungseinrichtung vertiefende oder zusätzliche Schwerpunkte für die Jahresabschlussprüfung vorschlagen.

#### § 5 Inkrafttreten

Die Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 19.
Januar 2022

Die Ministerin der Finanzen und für Europa

Katrin Lange