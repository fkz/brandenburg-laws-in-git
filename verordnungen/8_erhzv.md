## Achte Verordnung zur Festsetzung von Erhaltungszielen und Gebietsabgrenzungen für Gebiete von gemeinschaftlicher Bedeutung (Achte Erhaltungszielverordnung - 8. ErhZV)

Auf Grund des § 14 Absatz 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21. Januar 2013 (GVBl.
I Nr. 3) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Festsetzung

Die in Anlage 1 aufgeführten und in Anlage 2 näher beschriebenen Gebiete werden gemäß Artikel 4 Absatz 4 der Richtlinie 92/43/EWG als Gebiete von gemeinschaftlicher Bedeutung (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) in den in § 3 bestimmten Grenzen festgesetzt.
Sie sind Teil des kohärenten europäischen ökologischen Netzes „Natura 2000“ und liegen vollständig oder anteilig im Naturpark Niederlausitzer Landrücken.

#### § 2 Erhaltungsziele

(1) Die in Anlage 1 genannten Gebiete von gemeinschaftlicher Bedeutung stehen unter besonderem Schutz.
Erhaltungsziel für das jeweilige Gebiet ist die Erhaltung oder Wiederherstellung eines günstigen Erhaltungszustandes (§ 7 Absatz 1 Nummer 10 des Bundesnaturschutzgesetzes) der in Anlage 2 für das jeweilige Gebiet genannten natürlichen Lebensraumtypen oder Tier- und Pflanzenarten von gemeinschaftlichem Interesse.
In den Anlagen 3 und 4 werden für die in Anlage 1 aufgeführten Gebiete ökologische Erfordernisse für einen günstigen Erhaltungszustand der natürlichen Lebensraumtypen nach Anhang I und der Tier- und Pflanzenarten nach Anhang II der Richtlinie 92/43/EWG beschrieben.

(2) Soweit das Gebiet von gemeinschaftlicher Bedeutung „Stoßdorfer See“ flächengleich mit dem Naturschutzgebiet „Ostufer Stoßdorfer See“ ist, ergeben sich gemäß § 32 Absatz 3 des Bundesnaturschutzgesetzes die Erhaltungsziele aus der Verordnung über das Naturschutzgebiet.
Die im Gebiet anwendbare Schutzgebietsverordnung ist in Anlage 2 aufgeführt.

#### § 3 Gebietsabgrenzung

(1) Die Grenzen der Gebiete von gemeinschaftlicher Bedeutung sind in den in Anlage 2 genannten und in Anlage 5 Nummer 2 näher bezeichneten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 17 rot eingetragen.
Als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 5 Nummer 1 aufgeführte Übersichtskarte im Maßstab 1 : 50 000 dient der räumlichen Einordnung der Gebiete von gemeinschaftlicher Bedeutung.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 5 Nummer 2 aufgeführten topografischen Karten.
Zur Orientierung werden die Gebiete in Anlage 2 jeweils in einer Kartenskizze dargestellt.
Soweit Gebiete von gemeinschaftlicher Bedeutung flächengleich mit Naturschutzgebieten sind, sind diese Flächen in den in Satz 1 genannten Karten schraffiert eingetragen.

(2) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim jeweils zuständigen Landkreis, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 4 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 8.
Mai 2017

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#a) Diese Verordnung dient der Umsetzung der Richtlinie 92/43/EWG des Rates vom 21.
Mai 1992 zur Erhaltung der natürlichen Lebensräume sowie der wild lebenden Tiere und Pflanzen (ABl.
L 206 vom 22.7.1992, S.
7), die durch die Richtlinie 2013/17/EU vom 13.
Mai 2013 (ABl.
L 158 vom 10.6.2013, S. 193) geändert worden ist.

* * *

### Anlagen

1

[Anlage 1 - Liste der Gebiete von gemeinschaftlicher Bedeutung](/br2/sixcms/media.php/68/GVBl_II_27_2017-Anlage-1.pdf "Anlage 1 - Liste der Gebiete von gemeinschaftlicher Bedeutung") 600.9 KB

2

[Anlage 2 - Gebiete von gemeinschaftlicher Bedeutung](/br2/sixcms/media.php/68/GVBl_II_27_2017-Anlage-2.pdf "Anlage 2 - Gebiete von gemeinschaftlicher Bedeutung") 5.3 MB

3

[Anlage 3 - Ökologische Erfordernisse für einen günstigen Erhaltungszustand der natürlichen Lebensraumtypen nach Anhang I der Richtlinie 92/43/EWG](/br2/sixcms/media.php/68/GVBl_II_27_2017-Anlage-3.pdf "Anlage 3 - Ökologische Erfordernisse für einen günstigen Erhaltungszustand der natürlichen Lebensraumtypen nach Anhang I der Richtlinie 92/43/EWG") 656.2 KB

4

[Anlage 4 - Ökologische Erfordernisse für einen günstigen Erhaltungszustand von Tier- und Pflanzenarten nach Anhang II der Richtlinie 92/43/EWG](/br2/sixcms/media.php/68/GVBl_II_27_2017-Anlage-4.pdf "Anlage 4 - Ökologische Erfordernisse für einen günstigen Erhaltungszustand von Tier- und Pflanzenarten nach Anhang II der Richtlinie 92/43/EWG") 633.2 KB

5

[Anlage 5 - Übersichtskarte, Topografische Karten](/br2/sixcms/media.php/68/GVBl_II_27_2017-Anlage-5.pdf "Anlage 5 - Übersichtskarte, Topografische Karten") 610.0 KB