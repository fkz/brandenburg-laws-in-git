## Verordnung über Zuständigkeiten im Bereich der Sozialgerichtsbarkeit (ZuSozV)

Auf Grund des § 9 Absatz 2 und des § 30 Absatz 2 des Sozialgerichtsgesetzes in der Fassung der Bekanntmachung vom 23.
September 1975 (BGBl.
I S. 2535), die durch Artikel 1 Nummer 3 und 13 des Gesetzes vom 17.
August 2001 (BGBl.
I S.
2144) neu gefasst worden sind, in Verbindung mit § 6 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S.
186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28 S.
2) geändert worden ist, und § 1 Nummer 43 der Justiz-Zuständigkeitsübertragungsverordnung vom 9.
April 2014 (GVBl.
II Nr.
23), des § 1 des Gesetzes zur Regelung beamtenrechtlicher Zuständigkeiten im Bereich der Justiz vom 25. Januar 2016 (GVBl.
I Nr.
5), des § 1 Absatz 3 der Ernennungsverordnung vom 1. August 2004 (GVBl. II S. 742), des § 54 Absatz 3 Satz 2 des Beamtenstatusgesetzes vom 17.
Juni 2008 (BGBl. I S. 1010) in Verbindung mit § 71 des Deutschen Richtergesetzes in der Fassung der Bekanntmachung vom 19.
April 1972 (BGBl.
I S.
713), der durch § 62 Absatz 9 Nummer 1 des Gesetzes vom 17. Juni 2008 (BGBl.
I S.
1010, 1022) neu gefasst worden ist, des § 103 Absatz 1 und 2 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S.
26) in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes vom 12.
Juli 2011 (GVBl.
I Nr.
18), der durch Artikel 3 des Gesetzes vom 18.
Juni 2018 (GVBl.
I Nr.
13 S.
4) geändert worden ist, des § 17 Absatz 2 Satz 1 und 2, des § 34 Absatz 5, des § 35 Absatz 2 Satz 2 und des § 42 Absatz 2 Satz 2 des Landesdisziplinargesetzes vom 18.
Dezember 2001 (GVBl.
I S.
254) und des § 6 Absatz 1 des Landesorganisationsgesetzes in Verbindung mit

1.  § 32 Absatz 1 Satz 1, § 38 Satz 1 und 2, § 54 Absatz 1, § 57 Absatz 1 Satz 1 und 2, § 66 Absatz 4, § 69 Absatz 5 Satz 1, § 84 Satz 1 und 2, § 85 Absatz 4 Satz 4, § 86 Absatz 2 und 3, § 88 Absatz 5, § 89 Satz 2 und § 92 Absatz 2 des Landesbeamtengesetzes, von denen § 66 Absatz 4, § 85 Absatz 4 Satz 4, § 86 Absatz 2 und 3 und § 88 Absatz 5 durch das Gesetz vom 29.
    Juni 2018 (GVBl.
    I Nr.
    17) neu gefasst worden sind, in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes und § 7 Absatz 1 Satz 1 und 2 der Richternebentätigkeitsverordnung vom 10.
    Mai 1999 (GVBl.
    II S.
    330),
2.  § 13 Absatz 2 Satz 3 des Brandenburgischen Besoldungsgesetzes vom 20.
    November 2013 (GVBl.
    I Nr.
    32 S.
    2, Nr.
    34),
3.  § 9 Absatz 3 und § 47 Absatz 3 Satz 3 und § 48 Absatz 2 Satz 1 des Brandenburgischen Beamtenversorgungsgesetzes vom 20.
    November 2013 (GVBl.
    I Nr.
    32), von denen § 9 Absatz 3 durch Artikel 26 des Gesetzes vom 8.
    Mai 2018 (GVBl.
    I Nr.
    8 S.
    20) geändert worden ist,
4.  § 6 der Dienstjubiläumsverordnung vom 18.
    Dezember 2014 (BGBl.
    I S.
    2267) in Verbindung mit § 64 Absatz 1 Satz 1 des Landesbeamtengesetzes, der durch das Gesetz vom 11. März 2010 (GVBl.
    I Nr.
    13) neu gefasst worden ist,
5.  § 4 Absatz 5 und § 9 Absatz 3 der Trennungsgeldverordnung in der Fassung der Bekanntmachung vom 29.
    Juni 1999 (BGBl.
    I S.
    1533) in Verbindung mit § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes und § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
6.  § 6 Satz 1 des Brandenburgischen Sozialgerichtsgesetzes vom 3.
    März 1992 (GVBl.
    I S.
    86), der zuletzt durch Artikel 1 des Gesetzes vom 10.
    Juli 2014 (GVBl.
    I Nr.
    37) neu gefasst worden ist,

verordnet der Minister der Justiz und für Europa und Verbraucherschutz unter Beachtung von Artikel 9 Absatz 3 des Staatsvertrages über die Errichtung der gemeinsamen Fachobergerichte der Länder Berlin und Brandenburg vom 26.
April 2004 (GVBl.
I S.
281), der durch Staatsvertrag vom 7. Februar 2011 (GVBl.
I Nr.
18) geändert worden ist, im Einvernehmen mit dem Senator für Justiz, Verbraucherschutz und Antidiskriminierung des Landes Berlin:

#### § 1 Dienstaufsicht

(1) Die Dienstaufsicht üben aus:

1.  die Präsidentin oder der Präsident eines Sozialgerichts des Landes Brandenburg über die bei dem Sozialgericht beschäftigten Richterinnen und Richter, Beamtinnen und Beamten, Arbeitnehmerinnen und Arbeitnehmer,
2.  die Präsidentin oder der Präsident des Landessozialgerichts Berlin-Brandenburg über die Präsidentinnen und Präsidenten der Sozialgerichte des Landes Brandenburg und die bei dem Landessozialgericht Berlin-Brandenburg beschäftigten Richterinnen und Richter, Beamtinnen und Beamten, Arbeitnehmerinnen und Arbeitnehmer.

(2) Absatz 1 gilt nicht, soweit aufgrund eines Gesetzes oder einer Rechtsverordnung zwingend eine andere Stelle zuständig ist oder sich aus den §§ 2 bis 6 Abweichendes ergibt.

#### § 2 Ernennung und Folgezuständigkeiten, Versetzung und Abordnung, Berufung<br>ehrenamtlicher Richterinnen und Richter

(1) Die der obersten Dienstbehörde zustehende Befugnis zur Ernennung der Beamtinnen und Beamten des Landessozialgerichts Berlin-Brandenburg und der Sozialgerichte des Landes Brandenburg wird der Präsidentin oder dem Präsidenten des Landessozialgerichts Berlin-Brandenburg übertragen.

(2) Der Präsidentin oder dem Präsidenten des Landessozialgerichts Berlin-Brandenburg wird in dem Umfang nach Absatz 1 auch die Befugnis der obersten Dienstbehörde übertragen für

1.  Entscheidungen nach § 22 Absatz 1, 2 und 3 des Beamtenstatusgesetzes in Verbindung mit § 32 Absatz 1 Satz 1 des Landesbeamtengesetzes (Feststellung der Beendigung des Dienstverhältnisses), nach § 28 Absatz 1 und 2 des Beamtenstatusgesetzes in Verbindung mit § 38 Satz 1 des Landesbeamtengesetzes (Versetzung einer Beamtin oder eines Beamten auf Probe in den Ruhestand) und nach § 39 des Beamtenstatusgesetzes in Verbindung mit § 54 Absatz 1 des Landesbeamtengesetzes (Verbot der Führung der Dienstgeschäfte),
2.  Zustimmungen nach § 13 Absatz 2 Satz 3 des Brandenburgischen Besoldungsgesetzes (Absehen von der Rückforderung von Bezügen),
3.  die Ausübung der Disziplinarbefugnisse bei Ruhestandsbeamtinnen und Ruhestandsbeamten nach § 17 Absatz 2 Satz 1 des Landesdisziplinargesetzes**,**
4.  die Kürzung der Dienstbezüge bis zum Höchstmaß nach § 34 Absatz 3 Nummer 1, die Erhebung der Disziplinarklage nach § 35 und den Erlass des Widerspruchsbescheides nach § 42 Absatz 2 Satz 1 des Landesdisziplinargesetzes,
5.  die Gewährung und Versagung von Jubiläumszuwendungen nach § 6 Satz 2 der Dienstjubiläumsverordnung in Verbindung mit § 64 Absatz 1 Satz 1 des Landesbeamtengesetzes.

(3) Die Präsidentin oder der Präsident des Landessozialgerichts Berlin-Brandenburg ist zuständig für die Versetzung und die Abordnung der Beamtinnen und Beamten des Landessozialgerichts Berlin-Brandenburg und der Sozialgerichte des Landes Brandenburg innerhalb ihres oder seines Geschäftsbereiches nach § 27 Absatz 2 Satz 1, §§ 29 und 30 des Landesbeamtengesetzes und für die Erklärung des Einverständnisses zu einer Versetzung oder Abordnung von Beamtinnen und Beamten in den Landesdienst nach § 14 Absatz 4, § 15 Absatz 3 des Beamtenstatusgesetzes und in ihren oder seinen Geschäftsbereich nach § 27 Absatz 2 Satz 2 des Landesbeamtengesetzes.

(4) Die Präsidentin oder der Präsident des Landessozialgerichts Berlin-Brandenburg ist zuständig für die Abordnung von Richterinnen und Richtern auf Lebenszeit oder auf Zeit sowie für die Verwendung von Richterinnen und Richtern auf Probe und Richterinnen und Richtern kraft Auftrags bei den Sozialgerichten des Landes Brandenburg.
Artikel 3 Satz 1 des Staatsvertrages über die Errichtung gemeinsamer Fachobergerichte der Länder Berlin und Brandenburg bleibt unberührt.

(5) Die Präsidentin oder der Präsident des Landessozialgerichts Berlin-Brandenburg ist zuständig für die Berufung der ehrenamtlichen Richterinnen und Richter für die Sozialgerichte des Landes Brandenburg nach § 13 Absatz 1 Satz 1 des Sozialgerichtsgesetzes.

(6) Die Zuständigkeiten des Ministeriums des Innern und für Kommunales, des Ministeriums der Finanzen und des Landespersonalausschusses bleiben unberührt.

#### § 3 Sonstige Zuständigkeiten

(1) Der Präsidentin oder dem Präsidenten des Landessozialgerichts Berlin-Brandenburg werden für die Richterinnen und Richter, Beamtinnen und Beamten des Landessozialgerichts Berlin-Brandenburg und für die Präsidentinnen und Präsidenten der Sozialgerichte des Landes Brandenburg die folgenden, der obersten Dienstbehörde zustehenden Befugnisse übertragen:

1.  Entgegennahme von Erklärungen und Entscheidungen in Nebentätigkeitsangelegenheiten, Untersagung von nicht genehmigungspflichtigen Nebentätigkeiten, Geltendmachung von Auskunftsverlangen in Nebentätigkeitsangelegenheiten und Entscheidungen über die Inanspruchnahme von Einrichtungen, Personal oder Material des Dienstherren nach § 84, § 85 Absatz 4 Satz 4, § 86 Absatz 2 und 3, § 88 Absatz 5, § 89 des Landesbeamtengesetzes in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes und § 7 der Richternebentätigkeitsverordnung und Untersagung von Tätigkeiten nach Beendigung des Richter- und Beamtenverhältnisses nach § 41 Satz 2 des Beamtenstatusgesetzes in Verbindung mit § 92 Absatz 2 des Landesbeamtengesetzes, § 71 des Deutschen Richtergesetzes und § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
2.  Zustimmung zur Annahme von Belohnungen, Geschenken und sonstigen Vorteilen nach § 42 Absatz 1 Satz 2 des Beamtenstatusgesetzes in Verbindung mit § 57 Absatz 1 des Landesbeamtengesetzes, § 71 des Deutschen Richtergesetzes und § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
3.  Entscheidungen über den Ersatz von Sachschäden nach § 66 des Landesbeamtengesetzes in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
4.  Genehmigung der Führung der Amtsbezeichnung mit dem Zusatz „außer Dienst“ nach § 69 Absatz 5 Satz 1 des Landesbeamtengesetzes in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes.

Die Befugnisse zu Satz 1 Nummer 4 werden der Präsidentin oder dem Präsidenten des Landessozialgerichts Berlin-Brandenburg ferner für die Richterinnen und Richter, Beamtinnen und Beamten der Sozialgerichte des Landes Brandenburg übertragen.

(2) Der Präsidentin oder dem Präsidenten eines Sozialgerichts des Landes Brandenburg werden für die Richterinnen und Richter, Beamtinnen und Beamten ihres oder seines Geschäftsbereiches die Befugnisse zu Absatz 1 Satz 1 Nummer 1 bis 3 mit der Maßgabe übertragen, dass die Entscheidung der Behördenleitung obliegt.

(3) Die Präsidentin oder der Präsident des Landessozialgerichts Berlin-Brandenburg ist für die Richterinnen, Richter, Beamtinnen und Beamten ihres oder seines Geschäftsbereichs für die Zusage der Umzugskostenvergütung nach §§ 3 und 4 des Bundesumzugskostengesetzes und die Anerkennung einer Wohnung als vorläufige Wohnung nach § 11 des Bundesumzugskostengesetzes zuständig, soweit nicht für die den Umzug veranlassende Maßnahme die Zuständigkeit der obersten Dienstbehörde gegeben ist.

(4) Die Präsidentin oder der Präsident des Landessozialgerichts Berlin-Brandenburg ist zuständig für die Bearbeitung von außergerichtlich gestellten Anträgen auf Schadensersatz und Entschädigung, die die Sozialgerichtsbarkeit betreffen.
Das Nähere regelt eine Verwaltungsvorschrift des für Justiz zuständigen Mitglieds der Landesregierung.

#### § 4 Sonderzuständigkeiten

(1) Der Präsidentin oder dem Präsidenten des Brandenburgischen Oberlandesgerichts werden für die Richterinnen, Richter, Beamtinnen und Beamten des Landessozialgerichts Berlin-Brandenburg und der Sozialgerichte des Landes Brandenburg die folgenden, der obersten Dienstbehörde zustehenden Befugnisse in Unfallfürsorgeangelegenheiten übertragen:

1.  Anerkennung als Dienstunfall und Gewährung der Unfallfürsorge nach § 47 Absatz 3 Satz 3 des Brandenburgischen Beamtenversorgungsgesetzes,
2.  Versagung von Unfallfürsorgeleistungen nach § 48 Absatz 2 Satz 1 des Brandenburgischen Beamtenversorgungsgesetzes,
3.  Anordnung der ärztlichen oder psychologischen Untersuchung oder Beobachtung und der Erteilung von für die Gewährung der Unfallfürsorge erforderlichen Auskünften nach § 4 Nummer 4, § 9 Absatz 3 Satz 1 des Brandenburgischen Beamtenversorgungsgesetzes sowie Weitergabe von Erkenntnissen und Beweismitteln an die mit der Begutachtung beauftragten Person nach § 4 Nummer 4, § 9 Absatz 3 Satz 2 des Brandenburgischen Beamtenversorgungsgesetzes.

(2) Die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts ist für die Richterinnen, Richter, Beamtinnen und Beamten des Landessozialgerichts Berlin-Brandenburg und der Sozialgerichte des Landes Brandenburg für folgende Entscheidungen zuständig:

1.  Durchsetzung übergegangener Schadenersatzansprüche aus Dienstunfällen nach § 67 des Landesbeamtengesetzes in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
2.  Gewährung (Berechnung und Zahlbarmachung) von Umzugskostenvergütung nach § 2 Absatz 2 Satz 1 des Bundesumzugskostengesetzes,
3.  Bewilligung von Trennungsgeld einschließlich der Entscheidungen nach § 4 Absatz 5 der Trennungsgeldverordnung in Verbindung mit § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes und § 10 Absatz 1 des Brandenburgischen Richtergesetzes und der weiteren Nebenentscheidungen zu Umfang, Dauer und Höhe des Trennungsgeldes, soweit diese nicht der obersten Dienstbehörde vorbehalten sind.

#### § 5 Vorverfahren und Vertretung des Dienstherrn

(1) Die der obersten Dienstbehörde zustehende Befugnis zur Entscheidung über den Widerspruch einer Richterin oder eines Richters, einer Beamtin oder eines Beamten, einer Richterin oder eines Richters, einer Beamtin oder eines Beamten im Ruhestand, einer früheren Richterin oder eines früheren Richters, einer früheren Beamtin oder eines früheren Beamten und deren Hinterbliebenen gegen den Erlass oder die Ablehnung eines Verwaltungsaktes, gegen eine Maßnahme der Dienstaufsicht nach § 26 des Deutschen Richtergesetzes oder gegen die Ablehnung eines Anspruchs auf eine Leistung wird der Präsidentin oder dem Präsidenten des Landesssozialgerichts Berlin-Brandenburg übertragen, soweit sie selbst oder die Präsidentin oder der Präsident eines Sozialgerichts des Landes Brandenburg die mit dem Widerspruch angefochtene Entscheidung erlassen haben (§ 54 Absatz 3 Satz 2 des Beamtenstatusgesetzes in Verbindung mit § 71 des Deutschen Richtergesetzes).
Für Widersprüche gegen Entscheidungen im Zuständigkeitsbereich des § 4 ist die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts zuständig.

(2) Die Vertretung des Landes vor den Gerichten der allgemeinen Verwaltungsgerichtsbarkeit und den Richterdienstgerichten wird der in Absatz 1 genannten Stelle übertragen, soweit sie über den Widerspruch entschieden hat oder hätte entscheiden müssen.
Satz 1 ist in Verfahren auf einstweiligen Rechtsschutz nach der Verwaltungsgerichtsordnung entsprechend anzuwenden.

(3) In anderen als den in den Absätzen 1 und 2 genannten Fällen ist für die Entscheidung über den Widerspruch und die Vertretung des Landes das für Justiz zuständige Ministerium oder, soweit es sich um Angelegenheiten des Gemeinsamen Juristischen Prüfungsamtes der Länder Berlin und Brandenburg handelt, dessen Präsidentin oder Präsident zuständig.

#### § 6 Regelungen für Arbeitnehmerinnen und Arbeitnehmer (Tarifbeschäftigte)

(1) Der Präsidentin oder dem Präsidenten des Landessozialgerichts wird die Zuständigkeit für die Personalangelegenheiten der beim Landessozialgericht Berlin-Brandenburg beschäftigten Arbeitnehmerinnen und Arbeitnehmer übertragen.
Sie oder er ist ferner zuständig für

1.  die Versetzung und Abordnung,
2.  den Verzicht auf die Rückforderung zu viel gezahlten Entgelts,
3.  die Berechnung und Festsetzung der Beschäftigungszeit und der Jubiläumszeit

der an den Sozialgerichten des Landes Brandenburg beschäftigten Arbeitnehmerinnen und Arbeitnehmern.
Der Präsidentin oder dem Präsidenten eines Sozialgerichts des Landes Brandenburg wird die Zuständigkeit für die übrigen Personalangelegenheiten der bei dem Sozialgericht beschäftigten Arbeitnehmerinnen und Arbeitnehmer übertragen.

(2) Die Vertretung des Landes vor den Arbeitsgerichten wird der nach Absatz 1 jeweils zuständigen Stelle übertragen.

(3) Ist nach tariflichen Regelungen Beamtenrecht auf Arbeitnehmerinnen und Arbeitnehmer anzuwenden, so gelten die jeweiligen beamtenrechtlichen Zuständigkeitsbestimmungen für die Beschäftigten vergleichbarer Entgeltgruppen entsprechend, soweit in dieser Verordnung nichts anderes bestimmt ist.

#### § 7 Übergangsvorschriften

Soweit vor Inkrafttreten dieser Verordnung andere als die in den §§ 1 bis 6 bestimmten Zuständigkeiten bestanden, verbleibt es für die im Zeitpunkt des Inkrafttretens dieser Verordnung laufenden Beurteilungs- und Verwaltungsverfahren sowie Verfahren zur Erstellung von Regelbeurteilungen, deren Beurteilungszeitraum vor oder mit dem 31.
Dezember 2018 enden, bei den bisherigen Zuständigkeiten.
Gleiches gilt hinsichtlich der Zuständigkeit für die Vertretung in zu diesem Zeitpunkt bereits anhängigen Rechtsstreitigkeiten.

#### § 8 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 1.
Januar 2019 in Kraft.
Gleichzeitig tritt die Verordnung über Zuständigkeiten im Bereich der Sozialgerichtsbarkeit vom 20.
Juni 2005 (GVBl.
II S.
295), die durch die Verordnung vom 18.
Juli 2013 (GVBl.
II Nr.
57) geändert worden ist, außer Kraft.

Potsdam, den 13.
Dezember 2018

Der Minister der Justiz  
und für Europa und Verbraucherschutz

Stefan Ludwig