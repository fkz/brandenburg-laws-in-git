## Verordnung über die Übertragung zusätzlicher Aufgaben auf die Hochschulen des Landes Brandenburg im Archiv- und Bibliotheksbereich (Archiv- und Bibliotheksaufgabenübertragungsverordnung - ABÜV)

Auf Grund des § 3 Absatz 8 des Brandenburgischen Hochschulgesetzes vom 28.
April 2014 (GVBl. I Nr. 18) verordnet die Ministerin für Wissenschaft, Forschung und Kultur:

#### § 1 Geltungsbereich

Diese Verordnung regelt die Übertragung von Aufgaben auf Hochschulen im Archiv- und Bibliotheksbereich.

#### § 2 Aufgabenübertragung

(1) Der Fachhochschule Potsdam werden folgende Aufgaben übertragen:

1.  Unterstützung der Archive im Sinne von § 14 Absatz 5 Satz 2 Nummer 1 bis 3 des Brandenburgischen Archivgesetzes vom 7.
    April 1994 (GVBl.
    I S.
    94), das durch Artikel 23 des Gesetzes vom 13.
    März 2012 (GVBl.
    I Nr. 16) geändert worden ist, und der Öffentlichen Bibliotheken im Land Brandenburg insbesondere durch
    1.  konzeptionelle Beratung und Wissenstransfer sowie
    2.  Entwicklung und Weiterentwicklung von Konzepten und Methoden für die Archiv- und Bibliothekspraxis;
2.  Förderung der Kooperation zwischen den Öffentlichen Bibliotheken der Länder Brandenburg und Berlin.

(2) Die für Hochschulen zuständige oberste Landesbehörde regelt im Einvernehmen mit der für Kultur zuständigen obersten Landesbehörde und im Benehmen mit der Hochschule die Einzelheiten und Konkretisierungen der Aufgabenübertragung durch Organisationserlass.

(3) Die Hochschule gestaltet die Art und Weise der Aufgabenwahrnehmung im Einzelnen durch Satzung aus.

#### § 3 Fach- und Rechtsaufsicht

(1) Die Hochschule nimmt die Aufgaben nach § 2 in Selbstverwaltung wahr.

(2) Abweichend von Absatz 1 und nur soweit sie durch Rechtsverordnung als zuständige Stelle im Sinne von Bundes- oder Landesrecht bestimmt ist, untersteht sie der Fachaufsicht der für Kultur zuständigen obersten Landesbehörde.

#### § 4 Inkrafttreten

Diese Verordnung tritt am 1.
März 2016 in Kraft.

Potsdam, den 1.
Februar 2016

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Prof.
Dr.-Ing.
Dr.
Sabine Kunst