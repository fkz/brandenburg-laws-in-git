## Verordnung über die Ausbildungen und Prüfungen für die Laufbahnen des gehobenen und höheren technischen Gewerbeaufsichtsdienstes in der Gewerbeaufsicht des Landes Brandenburg (Ausbildungs- und Prüfungsordnung Arbeitsschutzaufsicht - APOghDASA)

Auf Grund des § 26 Absatz 1 Satz 1 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S.
26) verordnet der Minister für Arbeit, Soziales, Frauen und Familie im Einvernehmen mit dem Minister des Innern und dem Minister der Finanzen:

Inhaltsübersicht
----------------

### Abschnitt 1  
Allgemeines

[§ 1    Geltungsbereich](#1)  
[§ 2    Begriffsbestimmungen](#2)  
[§ 3    Einstellungsvoraussetzungen](#3)  
[§ 4    Einstellungsbehörde](#4)  
[§ 5    Einstellungsverfahren](#5)  
[§ 6    Dienstbezeichnung und Befähigung](#6)

### Abschnitt 2  
Vorbereitungsdienst

[§ 7    Dauer des Vorbereitungsdienstes](#7)  
[§ 8    Anrechnung von Zeiten auf den Vorbereitungsdienst](#8)  
[§ 9    Entlassung aus dem Beamtenverhältnis](#9)  
[§ 10  Urlaub](#10)

### Abschnitt 3  
Ausbildung

[§ 11  Ziel der Ausbildung](#11)  
[§ 12  Ausbildungsbehörde, Ausbildungsleitung, Ausbildungsstellen, Ausbildungsstellenleitungen](#12)  
[§ 13  Aufgaben der Ausbildungsleitung](#13)  
[§ 14  Aufgaben der Ausbildungsstelle](#14)  
[§ 15  Theoretische und praktische Ausbildung](#15)  
[§ 16  Lehrfächer, Hospitationen](#16)  
[§ 17  Belegarbeiten](#17)  
[§ 18  Probebesichtigungen](#18)  
[§ 19  Bewertung der Leistungen](#19)  
[§ 20  Ausbildungsnachweise](#20)  
[§ 21  Befähigungsberichte, abschließende Beurteilung](#21)  
[§ 22  Ausbildungsakte](#22)

### Abschnitt 4  
Prüfungen

[§ 23  Zweck der Prüfungen](#23)  
[§ 24  Prüfungsausschuss](#24)  
[§ 25  Zulassung zur schriftlichen Prüfung](#25)  
[§ 26  Schriftliche Arbeiten unter Aufsicht](#26)  
[§ 27  Häusliche Prüfungsarbeit](#27)  
[§ 28  Bewertung der schriftlichen Prüfungsarbeiten](#28)  
[§ 29  Zulassung zur mündlichen Prüfung](#29)  
[§ 30  Mündliche Prüfung](#30)  
[§ 31  Gesamtergebnis](#31)  
[§ 32  Prüfungsniederschrift, Prüfungsakte](#32)  
[§ 33  Prüfungszeugnis, Einsicht in Prüfungsakten](#33)  
[§ 34  Erkrankung, Versäumnisse](#34)  
[§ 35  Folgen bei Unregelmäßigkeiten](#35)  
[§ 36  Wiederholung der Prüfungen](#36)  
[§ 37  Rücknahme der Prüfungsentscheidung](#37)

### Abschnitt 5  
Schlussvorschriften

[§ 38  Personalvertretung, Schwerbehindertenvertretung, Gleichstellungsbeauftragte](#38)  
[§ 39  Inkrafttreten](#39)

[Anlage 1     Ausbildungsnachweis](#A1)  
[Anlage 2     Befähigungsbericht](#A2)  
[Anlage 3     Abschließende Beurteilung](#A3)  
[Anlage 4     Prüfungsniederschrift](#A4)  
[Anlage 5     Niederschrift über die Durchführung der schriftlichen Arbeiten](#A5)  
[Anlage 6     Prüfungszeugnis](#A6)

### Abschnitt 1  
Allgemeines

#### § 1  
Geltungsbereich

Diese Verordnung gilt für die Laufbahnen des gehobenen und des höheren technischen Gewerbeaufsichtsdienstes in der Gewerbeaufsicht des Landes Brandenburg.

#### § 2   
Begriffsbestimmungen

(1) Im Sinne dieser Verordnung ist die Arbeitsschutzverwaltung die Gewerbeaufsicht und der technische Aufsichtsdienst der Arbeitsschutzverwaltung der technische Gewerbeaufsichtsdienst des Landes Brandenburg.

(2) Im Sinne dieser Verordnung sind:

1.  Vorbereitungsdienste die Zeiten, während deren Beamtinnen und Beamte auf Widerruf durch eine Ausbildung die Voraussetzungen für eine Übernahme oder Einstellung in eine Beamtenlaufbahn erwerben können,
    
2.  Auszubildende die Bewerberinnen und Bewerber für die Vorbereitungsdienste der nachfolgenden Nummern 3 und 4,
    
3.  Anwärterinnen und Anwärter die Beamtinnen und Beamte auf Widerruf, die sich im Vorbereitungsdienst für die Laufbahn des gehobenen technischen Aufsichtsdienstes in der Arbeitsschutzverwaltung des Landes Brandenburg befinden,
    
4.  Referendarinnen und Referendare die Beamtinnen und Beamte auf Widerruf, die sich im Vorbereitungsdienst für die Laufbahn des höheren technischen Aufsichtsdienstes in der Arbeitsschutzverwaltung des Landes Brandenburg befinden,
    
5.  Belegarbeiten während der Ausbildung von den Auszubildenden zu fertigende schriftliche Leistungsnachweise,
    
6.  Ausbildungsstunden 45 Minuten umfassende Unterrichtseinheiten,
    
7.  Ausbildungsabschnitte Zeiträume, in denen theoretisches und praktisches Wissen der jeweiligen Lehrfächer vermittelt wird.
    

#### § 3  
Einstellungsvoraussetzungen

(1) In den Vorbereitungsdienst darf nur eingestellt werden, wer die gesetzlichen Voraussetzungen für die Berufung in das Beamtenverhältnis auf Widerruf erfüllt.

(2) Die Auszubildenden müssen für den Außendienst körperlich tauglich sein.
Von schwerbehinderten Menschen darf nur das für die jeweilige Laufbahn erforderliche Mindestmaß an körperlicher Eignung verlangt werden; sie müssen jedoch in der Lage sein, Außendienste zu leisten.

(3) In den Vorbereitungsdienst für die Laufbahn des gehobenen technischen Aufsichtsdienstes in der Arbeitsschutzverwaltung kann eingestellt werden, wer mindestens

1.  über ein mit einem Bachelorgrad abgeschlossenes Studium einer Hochschule oder einen als gleichwertig anerkannten Abschluss in einer für den Arbeitsschutz entsprechenden Fachrichtung, insbesondere der Fachbereiche Wirtschaft und Technik, verfügt und
    
2.  im Regelfall zwei Jahre fachbezogen praktisch tätig gewesen ist.
    

(4) In den Vorbereitungsdienst für die Laufbahn des höheren technischen Aufsichtsdienstes in der Arbeitsschutzverwaltung kann eingestellt werden, wer

1.  über ein mit einem Mastergrad abgeschlossenes Studium an einer Hochschule oder einen als gleichwertig anerkannten Abschluss in einer für den Arbeitsschutz entsprechenden Fachrichtung, insbesondere der Fachbereiche Wirtschaft und Technik, verfügt und
    
2.  im Regelfall zwei Jahre fachbezogen praktisch tätig gewesen ist.
    

(5) Andere Studienabschlüsse sind zu berücksichtigen, wenn die für das Anerkennungsverfahren zuständige Stelle diese als gleichwertig anerkannt hat.

#### § 4  
Einstellungsbehörde

Über die Einstellung in den Vorbereitungsdienst entscheidet die für den Arbeitsschutz zuständige oberste Landesbehörde.
Sofern die Befugnisse zur Einstellung in den Vorbereitungsdienst dem Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit übertragen worden sind, entscheidet dieses im Einvernehmen mit der für den Arbeitsschutz zuständigen obersten Landesbehörde.

#### § 5   
Einstellungsverfahren

(1) Bewerbungen zur Einstellung in den Vorbereitungsdienst sind an das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit zu richten.

(2) Einer Bewerbung sind beizufügen:

1.  ein tabellarischer Lebenslauf,
    
2.  Kopien der Schulabschlusszeugnisse und der Zeugnisse über Studien- sowie Berufsabschlüsse,
    
3.  Nachweise über berufspraktische Tätigkeiten,
    
4.  eine schriftliche Erklärung, ob die Bewerberin oder der Bewerber gerichtlich vorbestraft ist oder ein gerichtliches Strafverfahren oder ein staatsanwaltschaftliches Ermittlungsverfahren anhängig ist und
    
5.  gegebenenfalls eine Ablichtung des Bescheides, mit dem die Gleichwertigkeit eines Bildungsabschlusses festgestellt wurde.
    

(3) Personen, die zur Einstellung vorgesehen sind, haben zusätzlich

1.  eine Kopie ihrer Geburts- oder Abstammungsurkunde,
    
2.  ein amtsärztliches Gesundheitszeugnis, nicht älter als drei Monate,
    
3.  eine Erklärung über ihre wirtschaftlichen Verhältnisse und
    
4.  einen Nachweis über ihre Rechtsstellung als Deutsche oder Deutscher im Sinne des Grundgesetzes oder über ihre Staatsangehörigkeit zu einem anderen Mitgliedstaat der Europäischen Union vorzulegen.
    

Vor der Einstellung ist die Bewerberin oder der Bewerber aufzufordern, bei der zuständigen Meldebehörde die Erteilung des „Führungszeugnisses zur Vorlage bei einer Behörde“ zu beantragen.

#### § 6  
Dienstbezeichnung und Befähigung

(1) Während ihres Vorbereitungsdienstes führen die Anwärterinnen und Anwärter die Dienstbezeichnung Gewerbeoberinspektoranwärterin oder Gewerbeoberinspektoranwärter.
Auszubildende des höheren Dienstes führen die Dienstbezeichnung Gewerbereferendarin oder Gewerbereferendar.

(2) Durch die Ableistung des Vorbereitungsdienstes und das Bestehen der Laufbahnprüfungen erwerben die Auszubildenden die Befähigung für den gehobenen oder den höheren technischen Aufsichtsdienst in der Arbeitsschutzverwaltung.
Ein Anspruch auf eine Beschäftigung im öffentlichen Dienst wird dadurch nicht begründet.

### Abschnitt 2  
Vorbereitungsdienst

#### § 7   
Dauer des Vorbereitungsdienstes

(1) Der Vorbereitungsdienst für den gehobenen technischen Aufsichtsdienst der Arbeitsschutzverwaltung dauert drei Jahre und für den höheren technischen Aufsichtsdienst der Arbeitsschutzverwaltung zwei Jahre.

(2) Wird die Ausbildung durch Krankheiten oder andere zwingende Gründe länger als drei Monate unterbrochen, so kann der Vorbereitungsdienst entsprechend verlängert werden.
Über die Verlängerung des Vorbereitungsdienstes entscheidet die für den Arbeitsschutz zuständige oberste Landesbehörde.

(3) Das Beamtenverhältnis der Auszubildenden endet unbeschadet der Regelungen des § 9 mit der Zustellung des Prüfungszeugnisses oder der schriftlichen Bekanntgabe über das endgültige Nichtbestehen der Prüfung.

#### § 8  
Anrechnung von Zeiten auf den Vorbereitungsdienst

(1) Auf den Vorbereitungsdienst der Anwärterinnen und Anwärter werden grundsätzlich Zeiten, die zum Erwerb eines in § 3 Absatz 3 Nummer 1 genannten Abschlusses geführt haben, mit einem Jahr angerechnet.
Zeiten einer beruflichen Tätigkeit, die für die Ausbildung förderlich sind, können mit bis zu sechs Monaten berücksichtigt werden.

(2) Über die Anrechnung von Studienzeiten und förderlichen beruflichen Tätigkeiten auf den Vorbereitungsdienst entscheidet die für den Arbeitsschutz zuständige oberste Landesbehörde.

#### § 9  
Entlassung aus dem Beamtenverhältnis

(1) Beamtinnen und Beamte auf Widerruf im Vorbereitungsdienst sind mit dem Ablauf des Tages aus dem Beamtenverhältnis entlassen, an dem ihnen

1.  das Bestehen der Laufbahnprüfung oder
    
2.  das endgültige Nichtbestehen der Laufbahnprüfung oder vorgeschriebenen Zwischenprüfung bekannt gegeben worden ist.
    

Im Fall von Satz 1 Nummer 1 endet das Beamtenverhältnis jedoch frühestens nach Ablauf der für den Vorbereitungsdienst im Allgemeinen oder im Einzelfall festgesetzten Zeit.

(2) Das Nichtbestehen nach Absatz 1 Satz 1 Nummer 2 ist gegeben, wenn

1.  zwei Belegarbeiten nach jeweils einmaliger Wiederholung nicht mindestens mit der Note „ausreichend“ (5 Punkte) bewertet wurden,
    
2.  die Probebesichtigung nach § 18 nach einer Wiederholung nicht mindestens mit der Note „ausreichend“ (5 Punkte) bewertet wurde,
    
3.  auf Grund des Befähigungsberichtes vor Anmeldung zur Prüfung zu erkennen ist, dass das Ziel der Ausbildung nicht erreicht wird oder
    
4.  der Ausbildungspunktwert nach § 21 Absatz 3 unter 5 Punkten liegt.
    

(3) Die Bestimmungen des Landesbeamtengesetzes über die Entlassung von Beamtinnen und Beamten bleiben unberührt.

(4) Die Entscheidung über die Entlassung aus dem Vorbereitungsdienst trifft die Einstellungsbehörde.

#### § 10  
Urlaub

(1) Bei der Genehmigung von Erholungsurlaub sind die Erfordernisse der Ausbildung zu berücksichtigen.
Während der theoretischen Ausbildungslehrgänge soll kein Urlaub gewährt werden.

(2) Ausnahmen können zugelassen werden, wenn der Urlaub der Ausbildung förderlich oder mit dieser vereinbar ist.

### Abschnitt 3  
Ausbildung

#### § 11   
Ziel der Ausbildung

Ziel ist es, die Auszubildenden für die Laufbahnen des gehobenen oder höheren technischen Dienstes in der Arbeitsschutzaufsicht zu befähigen.
Die Ausbildung soll gründliche theoretische und praktische Kenntnisse auf dem Gebiet des Arbeitsschutzes einschließlich der Aufgaben der Arbeitsschutzbehörden sowie über den Aufbau und die Aufgaben der öffentlichen Verwaltung vermitteln.
Neben der Vermittlung des Fachwissens soll das Verständnis für staatspolitische, rechtliche, soziale und wirtschaftliche Fragen gefördert werden.

#### § 12  
Ausbildungsbehörde, Ausbildungsleitung, Ausbildungsstellen, Ausbildungsstellenleitungen

(1) Ausbildungsbehörde ist das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit.

(2) Die Ausbildungsbehörde bestellt die Ausbildungsleitung.

(3) Ausbildungsstellen sind die nach dem Geschäftsverteilungsplan des Landesamtes für Arbeitsschutz, Verbraucherschutz und Gesundheit mit der Ausbildung beauftragten Dienststellen.

(4) Ausbildungsstellenleitungen sind die nach dem Geschäftsverteilungsplan des Landesamtes für Arbeitsschutz, Verbraucherschutz und Gesundheit mit der Ausbildung beauftragten Dezernatsleitungen.

#### § 13  
Aufgaben der Ausbildungsleitung

Die Ausbildungsleitung koordiniert und überwacht die ordnungsgemäße theoretische und praktische Ausbildung.

#### § 14  
Aufgaben der Ausbildungsstelle

(1) Die Ausbildungsstelle sorgt für die ordnungsgemäße praktische Ausbildung.
Sie muss in Abstimmung mit der Ausbildungsleitung

1.  die Voraussetzungen für eine erfolgreiche praktische Ausbildung schaffen,
    
2.  geeignete Personen beauftragen, die die Auszubildenden anleiten und so zur Erreichung des Ausbildungsziels beitragen,
    
3.  die Teilnahme der Auszubildenden an theoretischen Ausbildungseinheiten gewährleisten,
    
4.  für Hospitationen der Auszubildenden bei den in § 16 Absatz 2 aufgeführten Stellen Sorge tragen,
    
5.  für die Auszubildenden Pläne über die Abläufe der praktischen Ausbildungen aufstellen,
    
6.  die Ausbildungsnachweise nach § 20 überprüfen und im Bedarfsfall erforderliche Maßnahmen zur Unterstützung der Auszubildenden organisieren und
    
7.  Bewertungen nach § 18 Absatz 1 und § 21 Absatz 1 und 2 erstellen.
    

(2) Den nach Absatz 1 Nummer 2 mit der Ausbildung beauftragten Personen obliegt besonders

1.  den ihnen zugewiesenen Auszubildenden Aufgaben entsprechend den Zielen des vom Länderausschuss für Arbeitsschutz und Sicherheitstechnik bestätigten Rahmenlehrplanes zu stellen und sie bei der Erarbeitung sachgerechter Lösungen zu unterstützen,
    
2.  sie praktisch auszubilden,
    
3.  die Ausbildungsnachweise der Auszubildenden zu prüfen und
    
4.  mindestens vierteljährlich eine Kurzeinschätzung zu erstellen.
    

(3) Die Ausbildungsstelle ist verpflichtet, der Ausbildungsleitung Mängel in der Ausbildung unverzüglich anzuzeigen.

#### § 15  
Theoretische und praktische Ausbildung

(1) Die Ausbildung im Rahmen des Vorbereitungsdienstes besteht aus theoretischen und berufspraktischen Teilen.

(2) Die durchzuführenden Lehrgänge sollen grundsätzlich für den gehobenen Dienst 650 Ausbildungsstunden und für den höheren Dienst 750 Ausbildungsstunden nicht unterschreiten.

(3) Die Auszubildenden sind in allen Gebieten des Arbeitsschutzes im Innen- und Außendienst entsprechend den Laufbahnen auszubilden.
Sie sollen insbesondere

1.  Betriebe und Baustellen besichtigen und dabei Arbeitsplätze hinsichtlich der Sicherheit und des Gesundheitsschutzes beurteilen,
    
2.  Unfälle und Schadensfälle untersuchen,
    
3.  Besichtigungsschreiben und Bescheide erstellen,
    
4.  Stellungnahmen zu betrieblichen Planungen einschließlich etwaiger Gestaltungsvorschläge fertigen und
    
5.  sich über die Zusammenarbeit der Arbeitsschutzverwaltung mit anderen Behörden durch Hospitationen informieren.
    

(4) Die Auszubildenden haben durch Selbststudien ihre Kenntnisse zu erweitern.

#### § 16  
Lehrfächer, Hospitationen

(1) Die Ausbildung umfasst im Wesentlichen folgende Lehrfächer:

1.  Allgemeiner und betrieblicher Arbeitsschutz,
    
2.  Arbeitsstätten, Arbeitsplätze, Ergonomie,
    
3.  Physikalische Einwirkungen am Arbeitsplatz,
    
4.  Betriebssicherheit – technische Arbeitsmittel, überwachungsbedürftige Anlagen,
    
5.  Geräte- und Produktsicherheit,
    
6.  Strahlenschutz,
    
7.  Gefahrstoffe und explosionsgefährliche Stoffe,
    
8.  Sozialer Arbeitsschutz,
    
9.  Gesundheitsschutz und Arbeitsmedizin,
    
10.  Staats-, Beamten-, Verwaltungs- und Strafrecht, einschließlich der aufgabenbezogenen Regelungen des Gefahrenabwehr- und Ordnungswidrigkeitenrechts.
    

(2) Die Hospitationen sollen vornehmlich bei

1.  einem Unfallversicherungsträger,
    
2.  einem Bauordnungsamt,
    
3.  einem Verwaltungsgericht,
    
4.  einem technischen Überwachungsverein oder
    
5.  einer Immissionsschutzbehörde
    

erfolgen.

#### § 17  
Belegarbeiten

(1) Während der Ausbildung sind von den Auszubildenden jeweils sechs Belegarbeiten anzufertigen.
Die Themen dieser Arbeiten sollen von der Ausbildungsstelle im Einvernehmen mit der Ausbildungsleitung am Ende des jeweiligen Ausbildungsabschnittes gestellt werden.

(2) Die Belegarbeiten sind innerhalb von vier Wochen bei der Ausbildungsleitung einzureichen.
Ist die Frist aus einem der in § 34 Absatz 1 genannten Gründe versäumt worden, so gilt der Leistungsnachweis als nicht abgelegt und soll in einem angemessenen Zeitraum nachgeholt werden.

(3) Die Belegarbeiten sind von der Ausbildungsleitung oder einer von dieser zu bestimmenden fachlich geeigneten Person des Landesamtes für Arbeitsschutz, Verbraucherschutz und Gesundheit nach § 19 Absatz 1 zu bewerten.

(4) Nicht bestandene Belegarbeiten können einmalig wiederholt werden.
Geben Auszubildende Belegarbeiten ohne wichtigen Grund nicht innerhalb der Frist des Absatzes 2 Satz 1 ab, so sind diese Arbeiten mit der Note „ungenügend“ (0 Punkte) zu bewerten.

#### § 18  
Probebesichtigungen

(1) Im letzten Halbjahr des Vorbereitungsdienstes haben die Auszubildenden im Beisein der Ausbildungsstellenleitung selbstständig eine den Anforderungen der jeweiligen Laufbahn entsprechende Probebesichtigung durchzuführen.
Die Inhalte der Probebesichtigungen sind von der Ausbildungsbehörde festzulegen.

(2) Das Auftreten im Betrieb und das Ergebnis der Probebesichtigung sind von der Ausbildungsstellenleitung zu beurteilen und nach § 19 Absatz 1 zu bewerten.
Die Bewertung ist der Ausbildungsleitung zu übermitteln.
Ist die Probebesichtigung nicht mindestens mit der Note „ausreichend“ (5 Punkte) bewertet worden, kann sie nach frühestens einem Monat einmal wiederholt werden.

#### § 19  
Bewertung der Leistungen

(1) Die während der Ausbildung einschließlich der Prüfungen gezeigten Leistungen der Auszubildenden sind mit folgenden Punktzahlen und den sich daraus ergebenden Noten zu bewerten:

> 1.  
> 
> 15 bis 14 Punkte  
> 
> \=  
> 
> eine Leistung, die den Anforderungen in besonderem Maße entspricht,
> 
> 2.  
> 
> 13 bis 11 Punkte  
> 
> \=  
> 
> eine Leistung, die den Anforderungen voll entspricht,
> 
> 3.  
> 
> 10 bis 8 Punkte  
> 
> \=  
> 
> eine Leistung, die im Allgemeinen den Anforderungen entspricht,
> 
> 4.  
> 
> 7 bis 5 Punkte  
> 
> \=  
> 
> eine Leistung, die zwar Mängel aufweist, aber im Ganzen den Anforderungen noch entspricht,
> 
> 5.  
> 
> 4 bis 2 Punkte  
> 
> \=  
> 
> eine Leistung, die den Anforderungen nicht entspricht, jedoch erkennen lässt, dass die notwendigen Grundkenntnisse vorhanden sind und die Mängel in absehbarer Zeit behoben werden könnten und
> 
> 6.  
> 
> 1 bis 0 Punkte  
> 
> \=  
> 
> eine Leistung, die den Anforderungen nicht entspricht und bei der selbst die Grundkenntnisse so lückenhaft sind, dass die Mängel in absehbarer Zeit nicht behoben werden könnten.

(2) Durchschnitts-, Gesamt- und Endpunktzahlen sind jeweils auf zwei Dezimalstellen zu berechnen und auszuweisen; die dritte Dezimalstelle bleibt unberücksichtigt.

(3) Die Notenwerte sind wie folgt abzugrenzen:

> 1.  
> 
> von 15 bis 14,00 Punkte  
> 
> sehr gut,
> 
> 2.  
> 
> von 13,99 bis 11 Punkte  
> 
> gut,
> 
> 3.  
> 
> von 10,99 bis 8 Punkte  
> 
> befriedigend,
> 
> 4.  
> 
> von 7,99 bis 5 Punkte  
> 
> ausreichend,
> 
> 5.  
> 
> von 4,99 bis 2 Punkte  
> 
> mangelhaft,
> 
> 6.  
> 
> von 1,99 bis 0 Punkte  
> 
> ungenügend.

#### § 20  
Ausbildungsnachweise

(1) Die Auszubildenden müssen Ausbildungsnachweise nach dem Muster der Anlage 1 führen.

(2) Die Ausbildungsnachweise sind von den nach § 14 Absatz 1 Nummer 2 mit der Ausbildung beauftragten Personen zu bestätigen und vierteljährlich der Ausbildungsstellenleitung vorzulegen.

#### § 21  
Befähigungsberichte, abschließende Beurteilung

(1) Die Ausbildungsstellenleitungen erstellen nach sechs und nach zwölf Monaten sowie vor der Anmeldung zur Prüfung unter Berücksichtigung der Kurzeinschätzungen nach § 14 Absatz 2 Nummer 4 Befähigungsberichte nach dem Muster der Anlage 2.
Sie bewerten darin die Fähigkeiten, Kenntnisse und praktischen Leistungen der Auszubildenden entsprechend § 19 Absatz 1.
Die Bewertungen müssen jeweils erkennen lassen, ob das Ziel des Ausbildungsabschnittes erreicht worden ist.

(2) Die Ausbildungsstellenleitungen erstellen im Einvernehmen mit der Ausbildungsleitung fünf Monate vor Beendigung des Vorbereitungsdienstes die abschließende Beurteilung nach dem Muster der Anlage 3.
Sie stellen darin fest, ob die Auszubildenden das Ziel der praktischen und theoretischen Ausbildung nach § 25 Absatz 2 erreicht haben und ermitteln die Ausbildungspunktwerte.

(3) Die Ausbildungspunktwerte nach den Anlagen 2 und 3 sowie die ermittelten Punktzahlen der §§ 18 und 19 werden wie folgt gewichtet:

> 1.  
> 
> die ersten beiden Befähigungsberichte  
> 
> zweifach,
> 
> 2.  
> 
> der Befähigungsbericht vor Anmeldung zur Prüfung  
> 
> dreifach,
> 
> 3.  
> 
> die sechs Belegarbeiten jeweils  
> 
> einfach,
> 
> 4.  
> 
> die Probebesichtigung  
> 
> dreifach.

(4) Die Summe der nach Absatz 3 gewichteten Produkte ist durch 16 zu teilen.
Das Ergebnis ist entsprechend § 19 Absatz 1 zu bewerten.

(5) Die Befähigungsberichte und die abschließenden Beurteilungen sind den Auszubildenden in ihrem vollen Wortlaut bekannt zu geben und mit ihnen auszuwerten.
Die Auswertung ist zu protokollieren.
Das Protokoll ist zur Ausbildungsakte zu nehmen.

#### § 22  
Ausbildungsakte

Neben der Personalakte ist für die Auszubildenden eine gesonderte Ausbildungsakte in der Ausbildungsstelle zu führen.
Zu der Ausbildungsakte gehören ein Ausbildungsplan, die Ausbildungsnachweise, Bewertungen, Befähigungsberichte und die abschließende Beurteilung.

### Abschnitt 4   
Prüfungen

#### § 23  
Zweck der Prüfungen

(1) Mit den Prüfungen soll festgestellt werden, ob die Auszubildenden die Befähigungen für die jeweiligen Laufbahnen des technischen Aufsichtsdienstes der Arbeitsschutzverwaltung erreicht haben.

(2) Die Prüfungen bestehen aus den schriftlichen Arbeiten unter Aufsicht nach § 26, einer häuslichen Prüfungs­arbeit nach § 27 und einer mündlichen Prüfung nach § 30.

(3) Die Prüfungsschwerpunkte sollen den in § 16 Absatz 1 genannten Lehrfächern entnommen werden.

#### § 24  
Prüfungsausschuss

(1) Die Laufbahnprüfungen für den gehobenen oder den höheren technischen Aufsichtsdienst der Arbeitsschutzverwaltung sind vor einem Prüfungsausschuss abzulegen.
Der Prüfungsausschuss wird bei der für den Arbeitsschutz zuständigen obersten Landesbehörde gebildet.

(2) Die für den Arbeitsschutz zuständige oberste Landesbehörde bestellt für die Dauer von drei Jahren einen aus fünf Mitgliedern bestehenden Prüfungsausschuss.
Für jedes Mitglied ist zudem eine Vertretungsperson zu bestellen.
Eine Wiederbestellung ist zulässig.
Die Mitglieder und Stellvertretungen können aus wichtigem Grund abberufen werden.

(3) In den Prüfungsausschuss sind zu berufen:

1.  eine Person des höheren technischen Verwaltungsdienstes oder eine mit dieser vergleichbare tarifbeschäftigte Person aus dem für den Arbeitsschutz zuständigen Fachbereich der obersten Landesbehörde als vorsitzführende Person,
    
2.  zwei Beschäftigte des höheren technischen Aufsichtsdienstes im Arbeitsschutz oder zwei vergleichbare Tarifbeschäftigte,
    
3.  eine Arbeitsmedizinerin oder ein Arbeitsmediziner und
    
4.  eine Person des höheren nichttechnischen Verwaltungsdienstes oder mit dieser vergleichbare tarifbeschäftigte Person.
    

(4) Der Vorsitz im Prüfungsausschuss kann auch einer im höheren nichttechnischen Verwaltungsdienst beschäftigten Person der für den Arbeitsschutz zuständigen obersten Landesbehörde übertragen werden.
In diesem Fall ist an Stelle einer nach Absatz 3 Nummer 4 zu bestimmenden Person eine dritte Person aus dem höheren technischen Aufsichtsdienst im Arbeitsschutz oder eine mit dieser vergleichbare tarifbeschäftigte Person zu bestellen.

(5) Die Mitglieder des Prüfungsausschusses sind bei ihrer Tätigkeit unabhängig und an Weisungen nicht gebunden.

(6) Der Prüfungsausschuss entscheidet mit Stimmenmehrheit.
Seine Beratungen und Abstimmungen sind nicht öffentlich.

(7) Der Prüfungsausschuss hat

1.  über das Bestehen der Laufbahnprüfung zu entscheiden,
    
2.  die Prüfungsnoten festzustellen und
    
3.  über den Ablauf und das Ergebnis der Prüfungen eine Niederschrift nach dem Muster der Anlage 4 zu fertigen.
    

(8) Die dem Prüfungsausschuss vorsitzende Person oder ihre Stellvertretung leitet die Prüfung.
Sie hat insbesondere

1.  die vorbereitenden Maßnahmen zur Durchführung der Prüfung zu treffen,
    
2.  die Prüfungsaufgaben auszuwählen,
    
3.  für jede schriftliche Arbeit unter Aufsicht und für die häusliche Prüfungsarbeit zwei prüfende Personen aus dem in Absatz 2 genannten Personenkreis zu benennen, die die Arbeiten bewerten, und
    
4.  den Ablauf der Prüfung festzusetzen.
    

(9) Mit Einverständnis der Auszubildenden darf die Ausbildungsleitung ohne Stimmrecht an den Prüfungen teilnehmen.
Das Landespersonalvertretungsgesetz ist zu beachten.

(10) Der Prüfungsausschuss führt das Dienstsiegel der für den Arbeitsschutz zuständigen obersten Landesbehörde.
Die laufenden Geschäfte des Prüfungsausschusses werden von dem zuständigen Fachbereich der für den Arbeitsschutz zuständigen obersten Landesbehörde geführt.

#### § 25  
Zulassung zur schriftlichen Prüfung

(1) Zur Prüfung ist zuzulassen, wer die vorgeschriebene berufspraktische Ausbildung durchlaufen, an den theoretischen Lehrgängen teilgenommen und das Ziel der theoretischen und praktischen Ausbildung erreicht hat.

(2) Das Ziel der theoretischen und praktischen Ausbildung hat erreicht, wer

1.  fünf der sechs Belegarbeiten mindestens mit der Note „ausreichend“ (5 Punkte) vorweisen kann,
    
2.  nach einfacher Addition der Noten der sechs Belegarbeiten mindestens 30 Punkte erreicht hat,
    
3.  für die Probebesichtigung nach § 18 mindestens die Note „ausreichend“ (5 Punkte) erlangt hat,
    
4.  für die Befähigungsberichte nach § 21 Absatz 1 eine durchschnittliche Punktzahl von mindestens 5 Punkten erreicht hat und
    
5.  einen Ausbildungspunktwert nach § 21 Absatz 2 von mindestens 5 Punkten nachweisen kann.
    

(3) Die Ausbildungsleitung stellt die Zulassung der Auszubildenden zu den Laufbahnprüfungen des gehobenen oder des höheren technischen Aufsichtsdienstes der Arbeitsschutzverwaltung fest und teilt dem Prüfungsausschuss spätestens 17 Wochen vor Beendigung des Vorbereitungsdienstes unter Übersendung der Ausbildungsakten die Zulassung zu den Prüfungen mit.

#### § 26  
Schriftliche Arbeiten unter Aufsicht

(1) Die Auszubildenden sollen durch die schriftlichen Arbeiten unter Aufsicht zeigen, ob sie Aufgaben aus dem Bereich der Arbeitsschutzaufsicht rasch und sicher erfassen, in kurzer Zeit lösen und das Ergebnis knapp und übersichtlich darstellen können.

(2) Die Anwärterinnen oder Anwärter fertigen drei schriftliche Arbeiten unter Aufsicht an.
Dafür stehen ihnen jeweils vier Stunden zur Verfügung.
Referendarinnen oder Referendare haben vier schriftliche Arbeiten unter Aufsicht in jeweils fünf Stunden anzufertigen.
Zwischen zwei schriftlichen Arbeiten unter Aufsicht muss mindestens ein prüfungsfreier Tag liegen.
Die Prüfungsthemen sind den in § 16 Absatz 1 aufgeführten Lehrfächern zu entnehmen.
Der Schwerpunkt einer dieser Arbeiten sollte auf einem oder mehreren Rechtsgebieten aus dem in § 16 Absatz 1 Nummer 10 aufgeführtem Lehrfach liegen.

(3) Die Aufgaben für die schriftlichen Arbeiten unter Aufsicht werden von den Mitgliedern des Prüfungsausschusses vorgeschlagen.
Die Festlegung der Prüfungsthemen einschließlich der zugelassenen Hilfsmittel beschließt der Prüfungsausschuss.

(4) Die schriftlichen Arbeiten unter Aufsicht dürfen keine Namensangaben der Auszubildenden enthalten.
Zur Wahrung der Anonymität sind alle Seiten der Arbeiten mit Kennziffern zu versehen.
Die Kennziffern werden vor Beginn der schriftlichen Arbeiten unter Aufsicht von einer unabhängigen Person der für den Arbeitsschutz zuständigen obersten Landesbehörde den zu prüfenden Personen zugelost und vertraulich übermittelt.
Eine Liste der zugelosten Kennziffern ist in einem verschlossenen Umschlag zu versiegeln.
Der Umschlag darf der vorsitzführenden Person des Prüfungsausschusses nicht vor Abschluss der Bewertung der schriftlichen Arbeiten übergeben werden.

(5) Die dem Prüfungsausschuss vorsitzende Person bestimmt, wer die Aufsicht führt.
Den Aufsichtführenden sind die Aufgaben jeweils in einem versiegelten Umschlag zu übergeben.
Sie öffnen den Umschlag erst zu Beginn der Prüfung in Gegenwart der zu prüfenden Personen.
Vor Beginn der schriftlichen Arbeiten unter Aufsicht weisen die Aufsichtführenden auf die Inhalte der Absätze 7 bis 9 besonders hin.

(6) Die Aufsichtführenden fertigen Niederschriften nach dem Muster der Anlage 5 an.
Sie vermerken darin jede Unregelmäßigkeit und verzeichnen auf jeder Arbeit den Zeitpunkt des Beginns und der Abgabe der Arbeiten.
Die abgegebenen Arbeiten und die Niederschriften sind in einem Umschlag zu verschließen und der dem Prüfungsausschuss vorsitzenden Person zuzuleiten.

(7) Bricht eine zu prüfende Person die schriftlichen Arbeiten unter Aufsicht aus einem der in § 34 Absatz 1 genannten wichtigen Gründe ab, gilt die schriftliche Arbeit als nicht abgelegt und muss in angemessener Zeit nachgeholt werden.

(8) Die Aufsichtführenden können zu prüfende Personen, die schuldhaft gegen diese Prüfungsordnung verstoßen, von der Fortsetzung der schriftlichen Arbeiten ausschließen, wenn die Personen ihr störendes Verhalten trotz Ermahnung durch die Aufsichtführenden nicht unterlassen.
Über das weitere Vorgehen entscheidet der Prüfungsausschuss.

(9) Beeinflussen die zu prüfenden Personen die Ergebnisse der schriftlichen Arbeiten durch Täuschungsversuche, so werden sie von der Fortsetzung der Arbeiten ausgeschlossen.
Absatz 8 Satz 2 gilt entsprechend.

#### § 27  
Häusliche Prüfungsarbeit

(1) Die Auszubildenden sollen durch die häuslichen Prüfungsarbeiten nachweisen, dass sie Aufgaben aus der Praxis richtig erfassen, methodisch bearbeiten und die Ergebnisse klar darstellen können.

(2) Die dem Prüfungsausschuss vorsitzende Person stellt den Auszubildenden die Aufgaben für die häuslichen Prüfungsarbeiten zu, wenn festgestellt wurde, dass

1.  alle schriftlichen Arbeiten unter Aufsicht mit mindestens „ausreichend“ (5 Punkte) bewertet wurden oder
    
2.  nicht mehr als eine schriftliche Arbeit unter Aufsicht mit „mangelhaft“ (2 Punkte) bewertet wurde, jedoch die durchschnittliche Punktzahl der schriftlichen Arbeiten unter Aufsicht mindestens „ausreichend“ (5 Punkte) beträgt.
    

(3) Auszubildenden, denen aus den im Absatz 2 genannten Fällen keine Aufgaben zugestellt wurden, ist jeweils mit schriftlichem Bescheid der vorsitzführenden Person des Prüfungsausschusses schriftlich mitzuteilen, dass sie die schriftlichen Arbeiten unter Aufsicht der Laufbahnprüfung nicht bestanden haben und ihnen die Aufgaben der häuslichen Prüfungsarbeiten daher nicht zugestellt werden.

(4) Die häuslichen Prüfungsarbeiten sind von den Auszubildenden für den gehobenen technischen Aufsichtsdienst der Arbeitsschutzverwaltung innerhalb von vier Wochen, von den Auszubildenden für den höheren technischen Aufsichtsdienst innerhalb von sechs Wochen jeweils in dreifacher Ausfertigung bei der vorsitzenden Person des Prüfungsausschusses einzureichen.
Die Frist beginnt mit dem Tag der Aufgabenzustellung.

(5) Werden die häuslichen Prüfungsarbeiten aus einem der in § 34 Absatz 1 genannten Gründe nicht fristgerecht eingereicht, kann die Frist um einen angemessenen Zeitraum verlängert oder eine neue Prüfungsaufgabe zugeteilt werden.

(6) Die Auszubildenden haben schriftlich auf einem Beiblatt zu versichern, dass sie die Aufgaben selbstständig und ohne fremde Hilfe bearbeitet haben.
Sie haben alle benutzten Quellen und Hilfsmittel anzugeben.

#### § 28  
Bewertung der schriftlichen Prüfungsarbeiten

(1) Die schriftlichen Arbeiten unter Aufsicht und die häuslichen Prüfungsarbeiten sind von zwei nach § 24 Absatz 3 zu bestimmenden Personen in einer von der dem Prüfungsausschuss vorsitzenden Person festzulegenden Reihenfolge zu beurteilen und nach § 19 Absatz 1 zu bewerten.
Weichen die Bewertungen der prüfenden Personen voneinander ab, entscheidet die dem Prüfungsausschuss vorsitzende Person.

(2) Alle Arbeiten einer Prüfung zu einem Thema sind von denselben prüfenden Personen zu bewerten.

(3) Die bewerteten Arbeiten sind zur Prüfungsakte zu nehmen.

#### § 29  
Zulassung zur mündlichen Prüfung

(1) Die Auszubildenden sind zur mündlichen Prüfung zugelassen, wenn ihre häuslichen Prüfungsarbeiten vom Prüfungsausschuss mindestens mit „ausreichend“ (5 Punkte) bewertet wurden.

(2) Die Zulassung ist den Auszubildenden von der dem Prüfungsausschuss vorsitzenden Person mindestens 14 Tage vor der mündlichen Prüfung schriftlich bekannt zu geben.

(3) Bei Nichtzulassung zur mündlichen Prüfung gilt die Laufbahnprüfung als nicht bestanden.
Das Nichtbestehen der Laufbahnprüfung ist den Auszubildenden durch Bescheid von der dem Prüfungsausschuss vorsitzenden Person mitzuteilen.

#### § 30  
Mündliche Prüfung

(1) Die mündliche Prüfung soll spätestens zwei Monate nach Beendigung der schriftlichen Prüfungen stattfinden.
Ort und Zeitpunkt der Prüfung bestimmt die dem Prüfungsausschuss vorsitzende Person.

(2) Die mündliche Prüfung besteht aus einer Verständnisprüfung zu den in § 16 Absatz 1 genannten Lehrfächern.
Die Prüfungsdauer soll für den gehobenen technischen Aufsichtsdienst der Arbeitsschutzverwaltung 60 Minuten, für den höheren technischen Aufsichtsdienst der Arbeitsschutzverwaltung 90 Minuten, nicht überschreiten.

(3) Zu Beginn der mündlichen Prüfung müssen die Auszubildenden einen freien Vortrag zu ihrer häuslichen Prüfungsarbeit (Anwärterinnen oder Anwärter 10 Minuten, Referendarinnen oder Referendare 15 Minuten) halten.
Zur Vorbereitung ihres Vortrages ist den Auszubildenden zwei Wochen vor der mündlichen Prüfung Einsicht in die Bewertungen ihrer häuslichen Prüfungsarbeiten zu gewähren.

(4) Der Prüfungsausschuss bewertet die Prüfungsleistungen der Auszubildenden entsprechend § 19 Absatz 1.

(5) Wird die Leistung in der mündlichen Prüfung nicht mindestens mit „ausreichend“ (5 Punkte) bewertet, so gilt die Laufbahnprüfung als nicht bestanden.

#### § 31  
Gesamtergebnis

(1) Bei der Ermittlung des Gesamtergebnisses der Laufbahnprüfung (Abschlussnote) durch den Prüfungsausschuss ist der Ausbildungspunktwert nach § 21 Absatz 2 mit einem Anteil von drei Zehnteln und der Punktwert der Prüfungsleistungen mit einem Anteil von sieben Zehnteln anzurechnen.
§ 19 gilt entsprechend.

(2) Die Punktwerte der Prüfungsleistungen werden errechnet, indem die Punktwerte

> 1.  
> 
> der häuslichen Prüfungsarbeiten  
> 
> mit dem Faktor 3,
> 
> 2.  
> 
> der schriftlichen Arbeiten unter Aufsicht  
> 
> mit dem Faktor 2,
> 
> 3.  
> 
> der mündlichen Prüfungen  
> 
> mit dem Faktor 3

multipliziert und die daraus ermittelten Summen beim gehobenen technischen Aufsichtsdienst der Arbeitsschutzverwaltung durch 12 und beim höheren technischen Aufsichtsdienst der Arbeitsschutzverwaltung durch 14 dividiert werden.

(3) Der Prüfungsausschuss kann von den nach Absatz 1 ermittelten Ergebnissen bis zu einem Punkt nach oben und unten abweichen, wenn dadurch die Leistungen der Auszubildenden zutreffender gekennzeichnet werden.
Die Abweichungen sind in den Prüfungsniederschriften zu begründen.

(4) Die Laufbahnprüfungen sind bestanden, wenn die nach Absatz 1 ermittelten Gesamtnoten mindestens „ausreichend“ (5 Punkte) betragen.

(5) Im Anschluss an die mündlichen Prüfungen werden den Auszubildenden die Gesamtergebnisse ihrer Prüfungen durch die vorsitzführende Person des Prüfungsausschusses bekannt gegeben.

#### § 32  
Prüfungsniederschrift, Prüfungsakte

(1) Durch die vorsitzführende Person des Prüfungsausschusses ist ein Mitglied des Prüfungsausschusses zu benennen, das über den Verlauf der Prüfung für die Auszubildenden eine Niederschrift nach dem Muster der Anlage 4 fertigt.

(2) Die Niederschriften sind von den an der Prüfung teilnehmenden Mitgliedern des Prüfungsausschusses zu unterzeichnen und zur Prüfungsakte zu nehmen.

(3) Die Prüfungsakte ist von der personalführenden Stelle zehn Jahre aufzubewahren.

#### § 33  
Prüfungszeugnis, Einsicht in Prüfungsakten

(1) Nach bestandener Laufbahnprüfung erhalten die Auszubildenden jeweils ein Zeugnis nach dem Muster der Anlage 6.
Die Zeugnisse werden von der vorsitzführenden Person des Prüfungsausschusses unterzeichnet.

(2) Ausfertigungen der Zeugnisse sind zu den Prüfungs- und Personalakten zu nehmen.

(3) Sind die Prüfungen nicht bestanden, erhalten die Auszubildenden mit Rechtsmittelbelehrungen versehene, von der vorsitzführenden Person des Prüfungsausschusses unterzeichnete schriftliche Bescheide.

(4) Nach Abschluss des Prüfungsverfahrens können die Auszubildenden auf Antrag Einsicht in ihre Prüfungsarbeiten einschließlich der dazu erstellten Beurteilungen nehmen.
Die Einsichten werden durch die Ausbildungsbehörde gewährt.

#### § 34  
Erkrankung, Versäumnisse

(1) Sind die Auszubildenden durch Krankheit oder sonstige von ihnen nicht zu vertretende Umstände gehindert, zu den Prüfungen zu erscheinen oder die Prüfungen vollständig abzulegen, haben sie die Hinderungsgründe in geeigneter Form unverzüglich nachzuweisen.
Krankheiten sind umgehend durch ärztliche Atteste nachzuweisen.
Beschäftigungsverbote nach dem Mutterschutzgesetz und Krankheit gelten als wichtige Gründe.

(2) Erscheinen die Auszubildenden ohne wichtigen Grund nicht zu Prüfungsterminen oder brechen die Prüfungen ab, so gelten die jeweiligen Prüfungsteile als nicht bestanden.

#### § 35  
Folgen bei Unregelmäßigkeiten

Werden Auszubildende nach § 26 Absatz 8 oder 9 von der Prüfung ausgeschlossen, kann der Prüfungsausschuss je nach Schwere der Verfehlungen die betreffenden Prüfungsleistungen mit „ungenügend“ (0 Punkte) bewerten oder die Laufbahnprüfung für nicht bestanden erklären.

#### § 36  
Wiederholung der Prüfungen

(1) Sind die Laufbahnprüfungen nicht bestanden, so können die Auszubildenden sie innerhalb von sechs Monaten einmal wiederholen.
Die Termine der Wiederholungen bestimmt die vorsitzführende Person des Prüfungsausschusses.

(2) Die Vorbereitungsdienste werden durch die Ausbildungsbehörde entsprechend verlängert.

(3) Inhalt und Gestaltung der verlängerten Vorbereitungsdienste legt die Ausbildungsbehörde fest.

(4) Bestehen die Auszubildenden auch die Wiederholungsprüfungen nicht, so kann der Prüfungsausschuss auf begründeten und von der Ausbildungsbehörde befürworteten Antrag der entsprechenden Auszubildenden eine zweite Wiederholungsprüfung zulassen.
Dies gilt nicht für Wiederholungen auf Grund einer Entscheidung nach § 35.

(5) Wer die Wiederholungsprüfung nicht besteht, erhält darüber einen schriftlichen Bescheid der vorsitzführenden Person des Prüfungsausschusses.

(6) Das Beamtenverhältnis endet mit Ablauf des Tages, an dem den Auszubildenden ein Bescheid nach Absatz 5 zugestellt wurde.

#### § 37  
Rücknahme der Prüfungsentscheidung

Wird innerhalb einer Frist von drei Jahren nach der Aushändigung des Prüfungszeugnisses eine Täuschungshandlung bekannt, so bewertet der Prüfungsausschuss die Prüfungsleistungen unter Berücksichtigung der Festlegungen des § 35 neu.
Die Ausbildungsbehörde kann das jeweilige Prüfungszeugnis für ungültig erklären und einziehen.
Diese Entscheidung ist nur innerhalb einer Frist von drei Monaten zulässig, nachdem die der Täuschungshandlung zugrunde liegenden wesentlichen Tatsachen bekannt geworden sind.
Die Entscheidung ist der oder dem Betroffenen zuzustellen.
Die Wiederholung der Prüfung ist nicht zulässig.

### Abschnitt 5  
Schlussvorschriften

#### § 38  
Personalvertretung, Schwerbehindertenvertretung, Gleichstellungsbeauftragte

Die Rechte der Personal-, der Schwerbehindertenvertretung und der Gleichstellungsbeauftragten richten sich nach den jeweiligen gesetzlichen Bestimmungen.

#### § 39   
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 28.
Juni 2010

Der Minister für Arbeit,  
Soziales, Frauen und Familie

Günter Baaske

**Anlage 1** (zu § 20 Absatz 1)

**Ausbildungsnachweis**

  
  
  
  
 

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_       \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
(Vor- und Familienname)                                                                  (Dienstbezeichnung)

  
Ausbildungsbehörde: \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  
Ausbildungsstelle: \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  
  
  
  
  
  
  
  
  
  
  
  
  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Der Nachweis über die Tätigkeiten im Innen- und Außendienst ist von der Auszubildenden oder dem Auszubildenden regelmäßig zu führen.
Er lässt die Eintragungen von der mit der Ausbildung beauftragten Person bestätigen und legt ihn mindestens vierteljährlich der Ausbildungsstellenleitung vor.
 

Nachweis über die praktische Ausbildung                   Familienname: …………………………                                                                                                                        Seite:

Ausbildungsabschnitt

 Ausbildungsdauer

 Tätigkeiten/Aufgaben

 Kurzeinschätzung

 Sichtvermerke

Ausbilderin/ Ausbilder

Leiterin/Leiter der Ausbildungsstelle

**Anlage 2  
**(zu § 21 Absatz 1)

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
 (Ausbildungsstelle)

**Befähigungsbericht** 

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
 (Vor- und Familienname)                                                           (Dienstbezeichnung)  
  
  
Vorbereitungsdienst vom \_\_\_\_\_\_\_\_\_\_\_\_\_\_ bis \_\_\_\_\_\_\_\_\_\_\_\_\_  
  
  
Bewertungszeitraum vom \_\_\_\_\_\_\_\_\_\_\_\_\_\_ bis \_\_\_\_\_\_\_\_\_\_\_\_\_  
  
  
Fehlen infolge von Krankheit: \_\_\_\_\_ Tage  
  
  
Fehlen infolge von Urlaub: \_\_\_\_\_ Tage  
  
  
Fehlen infolge von unentschuldigtem Fernbleiben: \_\_\_\_ Tage  
  
 

 

Bewertung\*)

1.

Auffassungsgabe

Fähigkeit, Sachverhalte und Zusammenhänge systematisch zu erfassen, zu analysieren und zu verarbeiten

\_\_\_\_ Pkt.

2.

Urteilsvermögen

Fähigkeit, Sachverhalte und Probleme folgerichtig zu untersuchen und zutreffend zu beurteilen

\_\_\_\_ Pkt.

3.

Organisatorische Befähigung

Fähigkeit, die verfügbaren Hilfsmittel zur Erfüllung der gestellten Aufgaben systematisch sinnvoll einzusetzen, rationell zu arbeiten und Arbeitstechniken anzuwenden

\_\_\_\_ Pkt.

4.

Verantwortungs-/Pflichtbewusstsein, Lernbereitschaft

\_\_\_\_ Pkt.

5.

Mündliche Ausdrucksfähigkeit

Fähigkeit, Gedanken und Sachverhalte mündlich darzulegen

\_\_\_\_ Pkt.

6.

Schriftliche Ausdrucksfähigkeit

Fähigkeit, Gedanken und Sachverhalte schriftlich darzulegen

\_\_\_\_ Pkt.

7.

Auftreten gegenüber Vorgesetzten und Beschäftigten

\_\_\_\_ Pkt.

8.

Auftreten und Umgangsformen in Betrieben, gegenüber Publikum

\_\_\_\_ Pkt.

9.

Fachliche Kenntnisse

\_\_\_\_ Pkt.

10.

Arbeitssorgfalt

\_\_\_\_ Pkt.

11.

Arbeitsleistung einschließlich Verwertbarkeit

\_\_\_\_ Pkt.

                                                                                                                                                                                                                                                  Summe: \_\_\_\_\_\_\_ Pkt.

Durchschnittspunktzahl (Summe/11): \_\_\_\_ Pkt.

  
Note:    \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Bemerkungen: \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Das Ziel des Ausbildungsabschnittes wurde erreicht/nicht erreicht.\*\*)

  
  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_                  \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
(Ort)                              (Datum)                                             (Ausbildungsstellenleitung)

  
  
Der vorstehende Befähigungsbericht wurde mir in vollem Wortlaut bekannt gegeben und mit mir besprochen.

  
  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_                    \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
(Ort)                              (Datum)                                             (Auszubildende/Auszubildender)  
  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\*)   Für die Bewertung sind die Punktzahlen nach § 19 Absatz 1 anzuwenden.

\*\*) Nichtzutreffendes bitte streichen.

**Anlage 3  
**(zu § 21 Absatz 2)

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
(Ausbildungsstelle)

**Abschließende Beurteilung** 

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_                            \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
(Vor- und Familienname)                                                                        (Dienstbezeichnung)

Vorbereitungsdienst vom \_\_\_\_\_\_\_\_\_\_ bis \_\_\_\_\_\_\_\_\_\_

Bewertungszeitraum vom \_\_\_\_\_\_\_\_\_\_ bis \_\_\_\_\_\_\_\_\_\_

Fehlen infolge von Krankheit: \_\_\_\_Tage

Fehlen infolge von Urlaub: \_\_\_\_Tage

Fehlen infolge von unentschuldigtem Fernbleiben: \_\_\_\_Tage

Punktwert\*)

Wertigkeit

Punktzahl

1

Befähigungsberichte

1.1

Befähigungsbericht nach sechs Monaten

\_\_

x 2

\_\_\_

1.2

Befähigungsbericht nach zwölf Monaten

\_\_

x 2

\_\_\_

1.3

Befähigungsbericht vor der Anmeldung zur Prüfung

\_\_

x 3

\_\_\_

2

Belege

2.1

Erster Beleg

Thematik:\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_

x 1

\_\_\_

2.2

Zweiter Beleg

Thematik:\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_

x 1

\_\_\_

2.3

Dritter Beleg

Thematik:\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_

x 1

\_\_\_

2.4

Vierter Beleg

Thematik:\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_

x 1

\_\_\_

2.5

Fünfter Beleg

Thematik:\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_

x 1

\_\_\_

2.6

Sechster Beleg

Thematik:\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_

x 1

\_\_\_

3

Probebesichtigung

\_\_

x 3

\_\_\_

Summe der Punktzahlen:

\_\_\_

Ausbildungspunktwert

(Summe der Punktzahlen dividiert durch 16)

\_\_

**  
  
Gesamtnote:** \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  
  
Bemerkungen: \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Abschließendes Urteil:

Herr/Frau \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

hat das Ziel der praktischen und theoretischen Ausbildung erreicht/nicht erreicht.\*\*)

  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_               \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
(Ort)                               (Datum)                                                     (Ausbildungsstellenleitung)

  
Die vorstehende abschließende Beurteilung wurde mir in vollem Wortlaut bekannt gegeben und mit mir besprochen.  
  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_               \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
(Ort)                               (Datum)                                                      (Auszubildende/Auszubildender)

  
**Zulassung zur Laufbahnprüfung**

  
Herr/Frau \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ ist zur Laufbahnprüfung zugelassen.

  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_           \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
(Ort)                               (Datum)                                                   (Ausbildungsleitung)  
  
  
 

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\*)   Für die Bewertung sind die Punktzahlen nach § 19 Absatz 1 anzuwenden.

\*\*) Nichtzutreffendes bitte streichen.

**Anlage 4  
**(zu § 24 Absatz 7 Nummer 3, § 32 Absatz 1)

**Prüfungsniederschrift**

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_                          \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
(Vor- und Familienname)                                                               (Dienstbezeichnung)

wurde am \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ gemäß Ausbildungs- und Prüfungsordnung Arbeitsschutzaufsicht geprüft.

Prüfungsausschuss:

1.
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ als vorsitzführendes Mitglied

2.
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ als Mitglied

3.
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ als Mitglied

4.
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ als Mitglied

5.
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ als Mitglied  
  
**Prüfungsleistungen  
  
Schriftliche Prüfung**

  
Punktwert

  
Wertigkeit

  
Punktzahl  
 

1

Häusliche Prüfungsarbeit  
Thema:

\_\_\_\_

x 3

\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

2

Schriftliche Arbeiten unter Aufsicht

2.1

Erste Arbeit  
Thematik:

\_\_\_\_

x 2

\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

2.2

Zweite Arbeit  
Thematik: 

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_

x 2

\_\_\_\_

2.3

Dritte Arbeit  
Thematik:

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_  
 

x 2

\_\_\_\_  
  
  
 

2.4

Vierte Arbeit\*)  
Theamtik:

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

   
  

  
\_\_\_\_

   
  

  
x 2

   
  

  
\_\_\_\_

**Mündliche Prüfung**

\_\_\_\_

x 3

\_\_\_\_

Summe der Punktzahlen

\_\_\_\_

**Punktwert der Prüfungsleistung**

(Summe der Punktzahlen dividiert durch 14 für den höheren technischen Dienst, dividiert durch 12 für den gehobenen technischen Dienst)

\_\_\_\_

**Gesamtergebnis**

   
Ausbildungspunktwert (Vornote)

\_\_\_\_

x 3

\_\_\_\_

Punktwert der Prüfungsleistung

\_\_\_\_

x 7

\_\_\_\_

Summe der Punktzahlen

\_\_\_\_

Gesamtergebnis (Summe der Punktzahlen dividiert durch 10)

\_\_\_\_

**Abweichung nach § 31 Absatz 3\*\*)**

\[   \]   ja          \[   \]   nein

Begründung der Abweichung:

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  
  
**Gesamtergebnis:          Abschlussnote \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ = \_\_\_\_\_\_ Punkte**

 

**Bemerkungen:**

1.   Bei Bestehen der Laufbahnprüfung:

      Herrn/Frau \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ ist das Gesamtergebnis im Anschluss     an die mündliche Prüfung durch das vorsitzführende Mitglied des

      Prüfungsausschusses  mitgeteilt worden.

  
2.   Bei Nichtbestehen der Prüfung:

      Herrn/Frau \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ ist durch das vorsitzführende Mitglied des Prüfungsausschusses mitgeteilt worden, dass die Laufbahnprüfung nicht

      bestanden wurde und dass diese

      innerhalb von sechs Monaten einmal wiederholt werden kann.

  
3.   Bei Nichtbestehen der Wiederholungsprüfung:

      Herrn/Frau \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ ist durch das vorsitzführende Mitglied des Prüfungsausschusses mitgeteilt worden, dass die Wiederholungsprüfung nicht

      bestanden wurde.

  
  
  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
(Ort                                          (Datum)

  
  
  
  
Der Prüfungsausschuss  
  
  
  
  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
  
  
  
 

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_      \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_      \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_      \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
  
  
  
  
 

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\*)   Nur für den höheren technischen Dienst.

\*\*) Zutreffendes ankreuzen.

**Anlage 5  
**(zu § 26 Absatz 6)

### Niederschrift  
über die Durchführung der schriftlichen Arbeiten unter Aufsicht nach der Ausbildungs- und Prüfungsordnung Arbeitsschutzaufsicht

Der versiegelte Briefumschlag mit der Aufgabenstellung wurde zu Beginn der Prüfung in Anwesenheit der zu prüfenden Person geöffnet.
Ein Abdruck der Prüfungsaufgabe wurde übergeben und die in der Aufgabe angegebenen Hilfsmittel bereitgestellt.

Die zu prüfende Person erklärt auf Befragung durch das aufsichtführende Mitglied des Prüfungsausschusses, dass sie sich gesundheitlich in der Lage fühlt, die Prüfung vollständig abzulegen.

Die zu prüfende Person wurde nach § 26 Absatz 5 Satz 4 der Ausbildungs- und Prüfungsordnung Arbeitsschutzaufsicht auf die Folgen von Ordnungsverstößen und Täuschungsversuchen hingewiesen.

  
**Kennziffer:** \_\_\_\_\_\_\_\_  
  
  
 

  
Ort, Datum  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
 

**:**              

   
  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Prüfungsfach  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

**Eintragungen des Prüfungskandidaten  
**\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Bearbeitungsbeginn  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

**:**

  
**:**

**  
:**

 **_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\__**

**_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\__**                                                                                                                                                                                   Uhr  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Anzahl der abgegebenen Blätter

(einschl.
Aufgaben, Konzeptpapier, ohne Deckblatt)  
 

**:** 

 Blatt

**Vermerke der Aufsicht**

  
Unterbrechungen

**  
:**

  
 von                                                                  bis                                                             Uhr  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
 

**:**

 von                                                                  bis                                                             Uhr  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  
 

**:** 

 von                                                                  bis                                                             Uhr

  
Abgabezeit

**  
:**

                                        
                                                                                                                                            Uhr

  
Ich versichere, dass\*) \[  \] keine Unregelmäßigkeiten        \[    \] folgende Unregelmäßigkeiten festgestellt worden sind: \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_                  \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
(Ort)                                       (Datum)                                                 (Unterschrift des Aufsichtführenden)

**Bewertung der Arbeit**

 

Punktwert

Note

Datum/Unterschrift

Erstbewerterin/ Erstbewerter

 

 

......................................................................

......................................................................

Zweitbewerterin/ Zweitbewerter

 

 

Entscheidung der Prüfungskommission (bei abweichender Bewertung)

 

 

 

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

(Ort)                                       (Datum)

  
  
  

.......................................................................................  
Mitglied

  

.......................................................................................  
Mitglied

...............................................................................................................

Vorsitzführendes Mitglied des Prüfungsausschusses

  
...................................................................................................................

Mitglied                                                                                                 

...................................................................................................................    

  Mitglied                                                                                                 

  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
\*)   Zutreffendes ankreuzen.

**Anlage 6  
**(zu § 33 Absatz 1)

### Der Prüfungsausschuss

### für die Laufbahnprüfungen in der Arbeitsschutzaufsicht

### beim Ministerium für Arbeit, Soziales, Frauen und Familie des Landes Brandenburg

####   
Prüfungszeugnis

  
  
Herr/Frau \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_          \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

                    (Vor- und Familienname)                                                                                      (Dienstbezeichnung)

geboren am \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ in \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  
hat am \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ die in der Ausbildungs- und Prüfungsordnung Arbeitsschutzaufsicht vorgeschriebene

  
  
**Laufbahnprüfung**

  
  
mit der Note \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ (\_\_ Punkte) bestanden und besitzt damit die Befähigung für die

Laufbahn des \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ technischen Aufsichtsdienstes in der Arbeitsschutzverwaltung

des Landes Brandenburg.  
  
 

\_\_\_\_\_\_\_\_\_\_\_\_\_\_ , den \_\_\_\_\_\_\_\_\_\_\_\_\_\_

_Siegel_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
Vorsitzende/Vorsitzender des Prüfungsausschusses