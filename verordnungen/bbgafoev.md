## Verordnung zur Durchführung des Brandenburgischen Ausbildungsförderungsgesetzes (Brandenburgische Ausbildungsförderungsverordnung - BbgAföV)

Auf Grund des § 5 Absatz 2 und 3 Satz 2 des Brandenburgischen Ausbildungsförderungsgesetzes vom 16.
Juni 2010 (GVBl.
I Nr. 24) verordnet die Ministerin für Wissenschaft, Forschung und Kultur im Einvernehmen mit dem Minister für Bildung, Jugend und Sport, dem Minister für Arbeit, Soziales, Frauen und Familie sowie dem Minister der Finanzen:

#### § 1   
Gegenstand

Durch diese Verordnung wird das Nähere zur Landesausbildungsförderung bestimmt und der Kostenausgleich für die Durchführung des Brandenburgischen Ausbildungsförderungsgesetzes durch die Landkreise und kreisfreien Städte gemäß Artikel 97 Absatz 3 der Verfassung des Landes Brandenburg geregelt.

#### § 2  
Antragstellung und Antragsunterlagen

(1) Über den Anspruch auf Landesausbildungsförderung wird auf schriftlichen Antrag entschieden.
Der Antrag kann auch elektronisch nach den Vorgaben des § 36a Absatz 2 Satz 4 Nummer 1 des Ersten Buches Sozialgesetzbuch gestellt werden.
Der Antrag ist an den nach § 7 Absatz 1 Nummer 2 des Brandenburgischen Ausbildungsförderungsgesetzes in Verbindung mit § 45 Absatz 1 des Bundesausbildungsförderungsgesetzes örtlich zuständigen Landkreis oder die örtlich zuständige kreisfreie Stadt zu richten.
Sofern kein Elternteil einen Wohnsitz im Land Brandenburg hat, ist der Landkreis oder die kreisfreie Stadt, in dessen oder deren Bezirk die Schülerin oder der Schüler ihren oder seinen ständigen Wohnsitz hat, örtlich zuständig.

(2) Die zur Feststellung des Anspruchs erforderlichen Tatsachen sind anzugeben und durch Nachweise zu belegen.
Die für Hochschulen zuständige oberste Landesbehörde stellt zur Unterstützung einer vollständigen Antragstellung geeignete Formblätter und Möglichkeiten für eine elektronische Antragstellung im Internet unter der Adresse [www.bafoeg-brandenburg.de](https://www.bafoeg-brandenburg.de) zur Verfügung.

(3) Die Anspruchsvoraussetzungen gemäß § 2 des Brandenburgischen Ausbildungsförderungsgesetzes sind nachzuweisen

1.  durch Vorlage einer Bescheinigung der Schule über den Besuch eines Bildungsgangs gemäß § 2 Absatz 2 des Brandenburgischen Ausbildungsförderungsgesetzes sowie
2.  durch Vorlage der zur Ermittlung der Einkommens- und Vermögensverhältnisse gemäß dem Brandenburgischen Ausbildungsförderungsgesetz erforderlichen Unterlagen.
    In den Fällen des § 2 Absatz 4 Satz 1 des Brandenburgischen Ausbildungsförderungsgesetzes ist der Bescheid vorzulegen, aus dem hervorgeht, dass entsprechende Leistungen im ersten Monat des jeweiligen Schuljahres, für den nach § 3 Landesausbildungsförderung geleistet wird, bezogen werden.

Die Unterlagen sind im Original oder in Kopie vorzulegen.
Antragstellerinnen und Antragsteller führen den Nachweis eines Wohnsitzes im Land Brandenburg auf Verlangen der zuständigen Behörde durch Vorlage einer Bestätigung der Meldebehörde (Original oder Kopie) oder durch Vorlage eines gültigen Personalausweises.

#### § 3  
Förderungsdauer

Über den Anspruch auf Landesausbildungsförderung wird in der Regel für die Dauer eines Bildungsganges gemäß § 2 Absatz 2 des Brandenburgischen Ausbildungsförderungsgesetzes unter der Bedingung entschieden, dass jeweils bis zum 31.
Oktober eines Schuljahres der Fortbestand der Anspruchsvoraussetzungen durch Vorlage der Belege nach § 2 Absatz 3 nachgewiesen wird.
Die Ämter für Ausbildungsförderung weisen die Leistungsempfangenden zwei Monate vor Ablauf eines Schuljahrs schriftlich auf diese Verpflichtung hin und unterrichten die Leistungsempfangenden, die die Nachweise nach § 2 Absatz 3 beigebracht haben, nach Ablauf der Frist in Satz 1 schriftlich über die Weiterförderung (Bewilligungszeitraum).
Werden die Belege nach § 2 Absatz 3 aus von den Leistungsempfangenden zu vertretenen Gründen nicht innerhalb der Frist nach Satz 1 vorgelegt, entfällt der Anspruch auf Landesausbildungsförderung rückwirkend zum Ende des vorangegangenen Schuljahres.

#### § 4  
Zahlweise

Der Förderungsbetrag wird unbar monatlich im Voraus ausgezahlt.

#### § 5  
Wechsel in der örtlichen Zuständigkeit

Wird ein anderer Landkreis oder eine andere kreisfreie Stadt zuständig, so tritt dieser oder diese für sämtliche Verwaltungshandlungen einschließlich des Vorverfahrens an die Stelle des bisher zuständigen Landkreises oder der bisher zuständigen kreisfreien Stadt.
§ 2 Absatz 2 des Zehnten Buches Sozialgesetzbuch bleibt unberührt.

#### § 6  
Auskunfts- und Mitwirkungspflichten

(1) Die Pflicht der Antragstellenden oder der Leistungsempfangenden zur Angabe von Tatsachen, insbesondere zur Mitteilung von Veränderungen in den leistungserheblichen Verhältnissen, richtet sich nach § 7 Absatz 2 des Brandenburgischen Ausbildungsförderungsgesetzes in Verbindung mit § 60 des Ersten Buches Sozialgesetzbuch.

(2) Die Pflicht der Schulen zur Auskunft und zur Unterrichtung über den Abbruch der Ausbildung richtet sich nach § 7 Absatz 1 Nummer 2 des Brandenburgischen Ausbildungsförderungsgesetzes in Verbindung mit § 47 Absatz 2 und 3 des Bundesausbildungsförderungsgesetzes.

(3) Die Pflicht der Eltern und der Ehe- oder eingetragenen Lebenspartnerin oder des Ehe- oder eingetragenen Lebenspartners zur Angabe von Tatsachen, insbesondere zur Mitteilung von Veränderungen in den leistungserheblichen Verhältnissen, richtet sich nach § 7 Absatz 1 Nummer 2 des Brandenburgischen Ausbildungsförderungsgesetzes in Verbindung mit § 47 Absatz 4 des Bundesausbildungsförderungsgesetzes und § 60 des Ersten Buches Sozialgesetzbuch.

(4) Die Pflicht des jeweiligen Arbeitgebers oder der jeweiligen Zusatzversorgungseinrichtung des öffentlichen Dienstes oder öffentlich-rechtlicher Zusatzversorgungseinrichtungen zur Auskunft richtet sich nach § 7 Absatz 1 Nummer 2 des Brandenburgischen Ausbildungsförderungsgesetzes in Verbindung mit § 47 Absatz 5 des Bundesausbildungsförderungsgesetzes.

#### § 7  
Kostenausgleich

(1) Den Landkreisen und kreisfreien Städten werden die für die Durchführung des Brandenburgischen Ausbildungsförderungsgesetzes erforderlichen Verwaltungskosten in Form einer Antragspauschale erstattet.
Diese beträgt je Antrag 100 Euro.
Abweichend von Satz 2 beträgt die Antragspauschale in den Fällen des § 2 Absatz 4 Satz 1 des Brandenburgischen Ausbildungsförderungsgesetzes je Antrag 60 Euro.

(2) Erstattungsberechtigt ist derjenige Landkreis oder diejenige kreisfreie Stadt, der oder die einen Antrag erstmalig bewilligt oder abgelehnt hat.
Die Erstattung erfolgt als Gesamtbetrag zum jeweils 1. November eines Kalenderjahres.

(3) Zur Berücksichtigung des Verwaltungsaufwandes für Anfragen und Beratungsgespräche, die nicht zu einer Antragstellung führen oder auf Grund derer die Antragstellenden ihre Anträge zurücknehmen, erhöht sich der nach Absatz 1 ermittelte Erstattungsbetrag um einen pauschalen Zuschlag von 5 Prozent.

#### § 8  
_(aufgehoben)_

#### § 9   
Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
August 2010 in Kraft.

Potsdam, den 10.
August 2010

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Dr.
Martina Münch