## Verordnung zur Bestimmung der sachlichen Zuständigkeit der eID-Karte-Behörden (eID-Karte-Zuständigkeitsverordnung - eIDKZustV)

Auf Grund

*   des § 6 Absatz 2 und § 12 Absatz 1 des Landesorganisationsgesetzes vom 24. Mai 2004 (GVBl.
    I S. 186), von denen durch Artikel 2 des Gesetzes vom 10.
    Juli 2014 (GVBl.
    I Nr. 28 S. 2) § 6 Absatz 2 geändert und § 12 Absatz 1 eingefügt worden ist, in Verbindung mit § 6 Absatz 1 Nummer 1 des eID-Karte-Gesetzes vom 21.
    Juni 2019 (BGBl.
    I S.
    846) und
*   des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
    Februar 1987 (BGBl.
    I S.
    602)

verordnet die Landesregierung:

#### § 1 Sachliche Zuständigkeit

Sachlich zuständige eID-Karte-Behörden im Sinne des § 6 Absatz 1 Nummer 1 des eID-Karte-Gesetzes vom 21.
Juni 2019 (BGBl.
I S.
846) sind die kreisfreien Städte, die Ämter und die amtsfreien Gemeinden, die Verbandsgemeinden, die mitverwaltenden Gemeinden und die mitverwalteten Gemeinden als örtliche Ordnungsbehörden.

#### § 2 Verfolgung von Ordnungswidrigkeiten

Die in § 1 benannten Behörden sind zuständig für die Verfolgung und Ahndung von Ordnungswidrigkeiten gemäß § 24 des eID-Karte-Gesetzes.

#### § 3 Inkrafttreten

Diese Verordnung tritt am 1.
November 2020 in Kraft.

Potsdam, den 15.
Oktober 2020

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

  
Der Minister des Innern und für Kommunales

Michael Stübgen