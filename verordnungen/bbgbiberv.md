## Verordnung über die Zulassung von Ausnahmen von den Schutzvorschriften für den Biber (Brandenburgische Biberverordnung - BbgBiberV)

Auf Grund des § 45 Absatz 7 Satz 4 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl. I S. 2542) und des § 17 der Bundesartenschutzverordnung vom 16.
Februar 2005 (BGBl. I S. 258, 896) in Verbindung mit § 30 Absatz 4 Satz 1 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 1 Absatz 2 Satz 2 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nr.
43) verordnet der Minister für Landwirtschaft, Umwelt und Klimaschutz:

#### § 1 Geltungsbereich

(1) Ausnahmen von Schutzvorschriften für den Biber (Castor fiber) nach den §§ 2 und 3 sind zulässig an

1.  Deichen einschließlich der zugehörigen wasserbaulichen Anlagen, der beidseitigen fünf Meter breiten Deichschutzstreifen und der Gräben, die der Abführung von Drängewasser zum Zweck der Standsicherheit von Deichen oder der ordnungsgemäßen Unterhaltung und Beobachtung im Hochwasserfall dienen;
2.  Stauanlagen und sonstigen Hochwasserschutzanlagen im Sinne des § 96 Absatz 1 des Brandenburgischen Wassergesetzes;
3.  Dämmen von Kläranlagen, Regen- und Hochwasserrückhaltebecken;
4.  Böschungen von öffentlich gewidmeten Verkehrsanlagen und Kanalseitendämmen;
5.  Dämmen und Staueinrichtungen in erwerbswirtschaftlich genutzten Fischteichanlagen;
6.  Ein- und Ausläufen von verrohrten Gewässerabschnitten und Durchlässen.

(2) Die unteren Naturschutzbehörden können über die in Absatz 1 genannten Bereiche hinaus

1.  Abschnitte von Gewässern innerhalb geschlossener Ortslagen von Dörfern und Städten;
2.  denkmalgeschützte Parkanlagen;
3.  Abschnitte von angelegten Be- und Entwässerungsgräben;
4.  Gewässer in unmittelbarer Nähe von Hochwasserschutzanlagen nach Absatz 1 Nummer 1;
5.  Gewässer in unmittelbarer Nähe von Böschungen nach Absatz 1 Nummer 4

durch Allgemeinverfügung festlegen, an denen Maßnahmen nach den §§ 2 und 3 zulässig sind.
Festlegungen nach Satz 1 sind nur zulässig, wenn Gefahren für die Gesundheit der Menschen oder für zwingende überwiegende Belange des Denkmalschutzes oder ernste land-, forst- oder sonstige ernste wirtschaftliche Schäden, die durch in Bereichen nach Satz 1 lebende Biber drohen, nicht durch Maßnahmen nach der Richtlinie zur Förderung von Präventionsmaßnahmen zum Schutz vor Schäden durch geschützte Tierarten (Wolf, Biber) vom 6.
Juni 2019 oder andere zumutbare Maßnahmen abgewendet werden können.
Festlegungen nach Satz 1 sind dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, zur Kenntnis zu geben.

(3) Die Absätze 1 und 2 gelten, außer an Hochwasserschutzanlagen nach Absatz 1 Nummer 1, nicht für die Einzugsbereiche der in der Anlage zu dieser Verordnung namentlich aufgeführten und in einer topografischen Karte dargestellten Fließgewässer.

(4) Die Absätze 1 und 2 gelten ferner nicht in

1.  Naturschutzgebieten und im Nationalpark Unteres Odertal sowie in Gebieten, die als Naturschutzgebiet einstweilig sichergestellt sind oder gemäß § 11 des Brandenburgischen Naturschutzausführungsgesetzes einer Veränderungssperre zwecks Ausweisung als Naturschutzgebiet unterliegen, es sei denn, dass insoweit eine nach der jeweiligen Schutzgebietsverordnung oder dem jeweiligen Gesetz erforderliche flächenschutzrechtliche Befreiung nach § 67 des Bundesnaturschutzgesetzes gewährt worden ist;
2.  Gebieten von gemeinschaftlicher Bedeutung nach § 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes und in Europäischen Vogelschutzgebieten nach § 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes.

Satz 1 Nummer 2 gilt nicht an Deichen oder sonstigen Hochwasserschutzanlagen, wenn eine erhebliche Beeinträchtigung von Erhaltungszielen des jeweiligen Gebietes bei der Durchführung von Maßnahmen nach den §§ 2 und 3 ausgeschlossen werden kann.

(5) Maßnahmen nach den §§ 2 und 3 dürfen nur in der Zeit vom 1.
September eines jeden Jahres bis zum 15. März des Folgejahres durchgeführt werden.
Die zeitliche Beschränkung nach Satz 1 gilt nicht

1.  für Einzelbiber, soweit die untere Naturschutzbehörde oder eine Ehrenamtliche Biberberaterin oder ein Ehrenamtlicher Biberberater zuvor bestätigt hat, dass diese keine Jungtiere haben;
2.  an Hochwasserschutzanlagen nach Absatz 1 Nummer 1, soweit Maßnahmen nach den §§ 2 und 3 zur Erhaltung deren jederzeitiger und vollständiger Funktionsfähigkeit erforderlich sind.

(6) Die Möglichkeit der unteren Naturschutzbehörde, in den in Absatz 3 genannten Gebieten im Einzelfall Ausnahmen nach § 45 Absatz 7 des Bundesnaturschutzgesetzes und in den in Absatz 4 Satz 1 Nummer 2 genannten Gebieten Ausnahmen nach § 45 Absatz 7 und § 34 Absatz 3 bis 5 des Bundesnaturschutzgesetzes zuzulassen, bleibt unberührt.

#### § 2 Vergrämen von Bibern

(1) Abweichend von § 44 Absatz 1 Nummer 3 des Bundesnaturschutzgesetzes wird nach § 4 berechtigten Personen gestattet, bewohnte Biberbaue und -burgen zu verfüllen oder zu beseitigen.
Zulässig sind auch gezielte optische oder akustische Störungen von Bibern sowie das wiederholte Absenken oder Beseitigen von Biberdämmen, um bewohnte Biberbaue und -burgen als Fortpflanzungs- oder Ruhestätten unbrauchbar zu machen und Biber aus den in § 1 Absatz 1 und 2 genannten Bereichen zu vertreiben.

(2) Bei der Durchführung von Maßnahmen nach Absatz 1 dürfen Biber nicht verletzt oder getötet werden.
Dies gilt nicht im Hochwasserfall ab Alarmstufe 3.

#### § 3 Entnahme von Bibern

(1) Soweit auch die wiederholte Durchführung von Maßnahmen nach § 2 über einen Zeitraum von mindestens vier Wochen ohne Erfolg geblieben ist, wird nach § 4 berechtigten Personen abweichend von § 44 Absatz 1 Nummer 1 und 2 des Bundesnaturschutzgesetzes gestattet, den betroffenen Bibern nach Maßgabe dieser Verordnung nachzustellen und sie mit einer für die Jagd zugelassenen Schusswaffe zu töten.
Dabei dürfen keine Elterntiere mit unselbstständigen Jungtieren geschossen werden, es sei denn, dass jeweils alle Tiere einer Familie getötet werden.
Die vorherige Durchführung von Maßnahmen nach § 2 ist an Hochwasserschutzanlagen nach § 1 Absatz 1 Nummer 1 und in Teichwirtschaften nach § 1 Absatz 1 Nummer 5 nicht erforderlich.

(2) Bei der Tötung von Bibern mit Schusswaffen nach Absatz 1 dürfen nur bleifreie Büchsenpatronen mit ausreichender Tötungswirkung verwendet werden; sie hat jagdrechtlichen Grundsätzen zu entsprechen.
Das Kaliber der Büchsenpatrone muss mindestens 6,5 Millimeter betragen und eine Auftreffenergie auf 100 Meter (E 100) von mindestens 2 000 Joule haben.
Beim Töten von in Fallen gefangenen Bibern sowie der Abgabe von Fangschüssen mit Kurzwaffen muss die Mündungsenergie der Geschosse mindestens 200 Joule betragen.
Die tierschutzrechtlichen Vorgaben sind zu beachten.

(3) Ist ein Abschuss nach Absatz 1 nicht möglich, wird nach § 4 berechtigten Personen abweichend von § 44 Absatz 1 Nummer 1 und 2 des Bundesnaturschutzgesetzes gestattet, den betroffenen Bibern nach Maßgabe dieser Verordnung nachzustellen und sie mit Fallen lebend zu fangen.
Absatz 1 Satz 2 gilt entsprechend.
Nach Satz 1 der Natur entnommene Biber sind durch einen Tierarzt oder eine andere nach § 4 des Tierschutzgesetzes zur Tötung von Wirbeltieren berechtigte Person tierschutzgerecht zu töten.

(4) Beim Fang der Biber nach Absatz 3 dürfen nur für den Fang von Bibern geeignete Fallen verwendet werden, die unversehrt fangen.
Die tierschutzrechtlichen Vorgaben sind zu beachten.
Die Fallen müssen so beschaffen sein und dürfen nur so verwendet werden, dass das unbeabsichtigte Fangen von sonstigen wild lebenden Tieren weitgehend ausgeschlossen ist.

#### § 4 Berechtigte Personen

(1) Zu Maßnahmen nach dieser Verordnung sind nur folgende Personen berechtigt:

1.  Mitarbeiterinnen und Mitarbeiter eines Wasser- und Bodenverbandes;
2.  Mitarbeiterinnen und Mitarbeiter des Landesamtes für Umwelt als Wasserwirtschaftsamt;
3.  Mitarbeiterinnen und Mitarbeiter eines Straßenbaulastträgers;
4.  Mitarbeiterinnen und Mitarbeiter der DB Netz;
5.  Mitarbeiterinnen und Mitarbeiter, Pächterinnen und Pächter oder Inhaberinnen und Inhaber eines Teichwirtschaftsbetriebes;
6.  Personen, die von der unteren Naturschutzbehörde hierzu bestellt sind.

Auf eine Berechtigung nach Satz 1 können sich die dort genannten Personen nur berufen, soweit sie auf Grund einer Schulung durch die Fachbehörde für Naturschutz und Landschaftspflege die zur Durchführung von Maßnahmen nach dieser Verordnung erforderlichen Fachkenntnisse und Fähigkeiten erworben haben oder aufgrund ihrer Ausbildung oder ihres bisherigen beruflichen oder sonstigen Umgangs mit Bibern gegenüber den für Naturschutz und Landschaftspflege zuständigen Behörden auf Verlangen nachweisen können, dass sie über vergleichbare Kenntnisse und Fähigkeiten verfügen.

(2) Zu Maßnahmen nach § 2 dieser Verordnung sind auch Personen berechtigt, die von einer nach Absatz 1 berechtigten Person im Einzelfall mit der Durchführung beauftragt und vor der Durchführung genau angeleitet wurden.

(3) Zur Tötung von Bibern nach § 3 Absatz 1 ist nur berechtigt, wer einen gültigen Jagdschein besitzt und entweder die Voraussetzungen nach Absatz 1 erfüllt oder von einer nach Absatz 1 berechtigten Person im Einzelfall mit der Tötung beauftragt wurde.
Soweit die Tötung nach § 3 Absatz 1 nicht durch die in dem jeweiligen Bereich jagdausübungsberechtigte Person erfolgt, ist diese über eine vorgesehene Tötung von Bibern vorab zu informieren.
Bei Gefahr im Verzug bedarf es der vorherigen Benachrichtigung des Jagdausübungsberechtigten nicht; in diesem Fall hat die Information umgehend nachträglich zu erfolgen.

#### § 5 Anzeigepflicht

Die Durchführung von Maßnahmen nach dieser Verordnung ist der unteren Naturschutzbehörde mindestens eine Woche im Voraus anzuzeigen.
Dies gilt nicht für Maßnahmen, die an Hochwasserschutzanlagen und Gewässern I.
Ordnung durch nach § 4 Absatz 1 Satz 1 Nummer 1 und 2 berechtigte Personen im Auftrag des Landesamtes für Umwelt durchgeführt werden.
Satz 1 gilt nicht in Fällen mit besonderer Eilbedürftigkeit, insbesondere bei akuten Hochwasserwarnlagen.

#### § 6 Berichts- und Beobachtungspflichten

(1) Die nach § 4 Absatz 1 und 2 berechtigten Personen haben der unteren Naturschutzbehörde nach der Durchführung von Maßnahmen nach den §§ 2 und 3 unverzüglich Bericht zu erstatten

1.  über die Anzahl der verfüllten oder beseitigten Biberbaue oder -burgen nach § 2 Absatz 1 Satz 1 unter Angabe des genauen Ortes und Datums;
2.  über die Anzahl der abgesenkten oder beseitigten Biberdämme oder die durch optische oder akustische Störungen oder andere Maßnahmen unbrauchbar gemachten Biberbaue oder -burgen nach § 2 Absatz 1 Satz 2 unter Angabe des genauen Ortes und Datums sowie der angewendeten Methode;
3.  über den genauen Ort und das genaue Datum des Fanges oder Abschusses nach § 3 sowie die Anzahl der jeweils getöteten oder gefangenen Biber sowie über den Verbleib der getöteten oder gefangenen Tiere.

Die unteren Naturschutzbehörden leiten die eingegangenen Berichte an die Fachbehörde für Naturschutz und Landschaftspflege und das für Naturschutz und Landschaftspflege zuständige Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, bis zum 1.
April eines jeden Jahres weiter.

(2) Die Fachbehörde für Naturschutz und Landschaftspflege beobachtet fortlaufend den Bestand des Bibers in Brandenburg.
Sie hat bis zum 30.
Juni eines jeden Jahres den Erhaltungszustand des Bibers in Brandenburg insgesamt und der von Maßnahmen nach dieser Verordnung betroffenen lokalen Populationen sowie die Auswirkungen der mit dieser Verordnung zugelassenen Ausnahmen auf deren jeweiligen Erhaltungszustand zu ermitteln und dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, zu berichten.
Ergeben sich aus den Berichten Anhaltspunkte, dass der günstige Erhaltungszustand der Populationen des Bibers in Brandenburg wegen der Ausnahmeregelungen dieser Verordnung nicht gewahrt bleibt, hat das für Naturschutz und Landschaftspflege zuständige Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, diese Verordnung im Bereich der betroffenen Populationen auszusetzen.

#### § 7 Unberührtheit anderer Rechtsvorschriften

(1) Unberührt von dieser Verordnung bleiben insbesondere

1.  die Vermarktungsverbote aus § 44 Absatz 2 Satz 1 Nummer 2 des Bundesnaturschutzgesetzes;
2.  die sonstigen Bestimmungen über verbotene Fangmethoden, Verfahren und Geräte nach § 4 Absatz 1 der Bundesartenschutzverordnung.

(2) Bei der Durchführung von Maßnahmen auf Grund dieser Verordnung, insbesondere beim Absenken oder Beseitigen von Biberdämmen nach § 2 Absatz 1 Satz 2, ist zu beachten, dass die Verbote des § 44 Absatz 1 Nummer 1 bis 4 des Bundesnaturschutzgesetzes im Hinblick auf andere wild lebende Tiere besonders oder streng geschützter Arten oder auf Pflanzenstandorte besonders geschützter Arten sowie die Verbote des § 30 Absatz 2 des Bundesnaturschutzgesetzes unberührt bleiben.

(3) Nicht den Verboten des § 44 Absatz 1 Nummer 3 des Bundesnaturschutzgesetzes unterliegt

1.  vorbehaltlich des Absatzes 2 das Absenken oder Beseitigen von Biberdämmen, soweit bewohnte Biberbaue oder -burgen dadurch nicht beeinträchtigt werden;
2.  das Verfüllen oder Beseitigen unbewohnter Biberbaue oder -burgen.

(4) Nach § 3 getötete Biber sind gemäß § 45 Absatz 1 Nummer 1 Buchstabe a des Bundesnaturschutzgesetzes von den Besitzverboten ausgenommen.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 69 Absatz 2 des Bundesnaturschutzgesetzes handelt, wer

1.  außerhalb des Geltungsbereichs nach § 1 Maßnahmen nach den §§ 2 oder 3 durchführt;
2.  andere als die in den §§ 2 oder 3 zugelassenen Maßnahmen durchführt;
3.  Maßnahmen nach den §§ 2 oder 3 durchführt, ohne hierzu nach § 4 berechtigt zu sein;
4.  gegen die Anzeigepflicht nach § 5 oder die Berichtspflicht nach § 6 verstößt.

(2) Ordnungswidrigkeiten nach Absatz 1 Nummer 1 bis 4 können nach § 69 Absatz 7 des Bundesnaturschutzgesetzes mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

#### § 9 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Sie tritt mit Ablauf des 15. März 2024 außer Kraft.

Potsdam, den 17.
April 2020

Der Minister für Landwirtschaft,  
Umwelt und Klimaschutz

Axel Vogel

* * *

### Anlagen

1

[Anlage (zu § 1 Absatz 3)](/br2/sixcms/media.php/68/GVBl_II_22_2020-Anlage.pdf "Anlage (zu § 1 Absatz 3)") 382.0 KB