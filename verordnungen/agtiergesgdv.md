## Verordnung zur Durchführung des Gesetzes zur Ausführung des Tiergesundheitsgesetzes (AGTierGesGDV)

Auf Grund des § 9 Absatz 1, des § 12 Absatz 2 Satz 2, des § 14 Absatz 3 und des § 16 Absatz 2 des Gesetzes zur Ausführung des Tiergesundheitsgesetzes in der Fassung der Bekanntmachung vom 17. Dezember 2001 (GVBl.
I S.
14), von denen § 9 Absatz 1 durch Artikel 1 Nummer 11, § 12 Absatz 2 Satz 2 durch Artikel 1 Nummer 13 Buchstabe b Doppelbuchstabe bb und § 14 Absatz 3 durch Artikel 1 Nummer 14 Buchstabe c des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr.
31) geändert worden sind, verordnet der Minister der Justiz und für Europa und Verbraucherschutz:

Abschnitt 1  
Beiträge, Beihilfen, Rücklagen
--------------------------------------------

### § 1  
Tierbestandsmeldung und Erhebung von Beiträgen

(1) Beiträge gemäß § 6 Absatz 2 des Gesetzes zur Ausführung des Tiergesundheitsgesetzes werden von Haltern von Pferden, Rindern, Schweinen, Schafen, Ziegen, Gehegewild und Geflügel (Gänse, Enten, Laufvögel – insbesondere Strauße, Emu, Nandu, Kasuare, Kiwi –, Legehennen, Masthähnchen, Hühner einschließlich Perl- und Truthühner) erhoben.

(2) Absatz 1 gilt nur für Tiere, die im Land Brandenburg gehalten werden.

(3) Stichtag für die Ermittlung der Höhe des Jahresbeitrages ist der 3.
Januar.
Maßstab für die Beitragsveranlagung ist der am Stichtag vorhandene Tierbestand.
Davon abweichend sind im Fall von Rinderhaltern die zum Stichtag in der zentralen Datenbank HI-Tier registrierten Rinder Maßstab für die Beitragsveranlagung.
Die Meldung des Tierbestandes ist durch die Tierhalter, ausgenommen der Rinderhalter, nach den Vorgaben der Tierseuchenkasse bis spätestens zum 15.
Januar jeden Jahres abzugeben.
Liegt der Tierseuchenkasse keine fristgerechte Tierbestandsmeldung vor, ist der Tierbestand des Vorjahres zugrunde zu legen.

(4) Geflügelhalterinnen und -halter, ausgenommen Laufvogelhalterinnen und -halter, bei denen am Stichtag

1.  kein Geflügel vorhanden ist oder
2.  die Anzahl der Tiere vom geschätzten durchschnittlichen Geflügelbestand des laufenden Jahres (Anzahl der voraussichtlich im laufenden Jahr gehaltenen Tiere geteilt durch die Anzahl der Produktionsdurchgänge) abweicht,

haben den geschätzten durchschnittlichen Geflügelbestand des laufenden Jahres zu melden.
Mit den vorgenannten Meldungen ist auch der durchschnittliche Tierbestand des Vorjahres anzugeben.
Beitragsmaßstab ist in diesen Fällen abweichend von Absatz 3 Satz 2 der geschätzte durchschnittliche Tierbestand.

(5) Können Tiere innerhalb eines Bestandes unterschiedlichen Besitzern zugeordnet werden, so hat derjenige die Meldung nach den Absätzen 3 und 4 vorzunehmen, der die Tierhaltung nach § 26 der Viehverkehrsverordnung der zuständigen Behörde angezeigt hat und dort als Tierhalter registriert ist.

(6) Beitragsjahr ist das Kalenderjahr.

(7) Die Beiträge sind innerhalb eines Monats nach Zugang des Bescheides fällig.

### § 2  
Gewährung von Beihilfen

(1) Für die Gewährung von Beihilfen sind die für Entschädigungen geltenden Vorschriften der §§ 15 bis 22 des Tiergesundheitsgesetzes, die §§ 12 bis 18 des Gesetzes zur Ausführung des Tiergesundheitsgesetzes sowie die §§ 4 und 5 dieser Verordnung sinngemäß anzuwenden.

(2) Beihilfen werden nicht gewährt, wenn und soweit das Tiergesundheitsgesetz eine Entschädigung vorsieht oder durch besondere Vorschrift ausschließt oder versagt.

(3) Beihilfen werden ferner nicht gewährt für Tiere, die sich zur Zeit des Todes, der Anordnung der Tötung, der Impfung oder der Maßnahmen diagnostischer Art nicht im Land Brandenburg befunden haben, es sei denn, die Tiere wurden nur zum Zweck der Schlachtung aus dem Land Brandenburg verbracht.

### § 3  
Bildung von Rücklagen, Verwaltungskosten

(1) Das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit – Tierseuchenkasse hat aus seinen Einnahmen im angemessenen Umfang für die der Beitragspflicht unterliegenden Tierarten Rücklagen zu bilden.

(2) Die Rücklagen sollen bei folgenden Tierarten höchstens betragen:

1.  je Pferd                                                                                              25 Euro
2.  je Rind                                                                                               24 Euro
3.  je Schwein (einschließlich Schwarzwild in Gehegen)                              15 Euro
4.  je Schaf (einschließlich Muffelwild in Gehegen)                                      11 Euro
5.  je Ziege                                                                                              11 Euro
6.  je Stück Gehegewild (außer Schwarz- und Muffelwild)                            15 Euro
7.  Geflügel je Tier                                                                                 1,10 Euro.

Die Rücklagen sollen in der Regel 50 Prozent dieser Beträge nicht unterschreiten.

(3) Die für jede Tierart erhobenen Beiträge einschließlich der hieraus angesammelten Rücklagen sind zur Be-streitung der Ausgaben gemäß § 6 Absatz 2 Satz 1 und 2 des Gesetzes zur Ausführung des Tiergesundheitsgesetzes zu verwenden.
Die Verwaltungskosten sind auf alle Tierarten angemessen zu verteilen.

Abschnitt 2  
Besondere Verfahrensregelungen
--------------------------------------------

### § 4  
Feststellung des Krankheitszustandes

(1) Zur Feststellung des Krankheitszustandes veranlasst der Amtstierarzt unmittelbar nach der Anzeige gemäß § 4 des Tiergesundheitsgesetzes die notwendigen Untersuchungen, die in den Tierseuchenverordnungen, den Falldefinitionen des Friedrich-Loeffler-Instituts und der amtlichen Methodensammlung vorgesehen sind.

(2) Abweichend von § 12 Absatz 1 Satz 2 des Gesetzes zur Ausführung des Tiergesundheitsgesetzes ist eine Untersuchung vor dem Tod des Tieres als ausreichend anzusehen, wenn die Krankheit auf der Grundlage labordiag-nostischer Untersuchungen im Landeslabor Berlin-Brandenburg, gegebenenfalls in Verbindung mit klinischen oder allergologischen Untersuchungen, festgestellt worden ist.

(3) Abweichend von § 12 Absatz 1 Satz 2 des Gesetzes zur Ausführung des Tiergesundheitsgesetzes kann der Amtstierarzt die Untersuchung auf einzelne Tiere beschränken oder auf die Untersuchung verzichten, soweit andere tierseuchenrechtliche Vorschriften dem nicht entgegenstehen.

### § 5  
Verfahren bei der Schätzung von Tieren

(1) Der Amtstierarzt kann die Schätzung allein vornehmen, wenn der beteiligte Tierhalter zustimmt und der Schätzwert für die gleichzeitig zu entschädigenden Tiere eines Halters den Betrag von 60 000 Euro nicht überschreitet.

(2) Stimmt der beteiligte Tierhalter der Schätzung durch den Amtstierarzt allein nicht zu oder überschreitet der Schätzwert der gleichzeitig zu entschädigenden Tiere eines Halters den Betrag von 60 000 Euro, so sind vom Amtstierarzt mindestens zwei von der Kreisordnungsbehörde bestellte Schätzer hinzuzuziehen.

(3) Der Amtstierarzt und die Schätzer haben den Wert unabhängig voneinander zu ermitteln und die Ergebnisse in die Niederschrift aufnehmen zu lassen.

Abschnitt 3  
Schlussvorschrift
-------------------------------

### § 6  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 1.
Januar 2015 in Kraft.
Gleichzeitig tritt die Verordnung zur Durchführung des Gesetzes zur Ausführung des Tierseuchengesetzes vom 28.
März 1996 (GVBl.
II S.
258), die zuletzt durch Verordnung vom 4.
November 2013 (GVBl.
II Nr.
79) geändert worden ist, außer Kraft.

Potsdam, den 11.
Dezember 2014

Der Minister der Justiz und für Europa und Verbraucherschutz

Dr.
Helmuth Markov