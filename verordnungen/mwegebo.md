## Verordnung über die Verwaltungsgebühren im Geschäftsbereich des Ministers für Wirtschaft, Arbeit und Energie (MWAEGebO)

Auf Grund des § 3 Absatz 1 des Gebührengesetzes für das Land Brandenburg vom 7. Juli 2009 (GVBl. I S. 246) verordnet der Minister für Wirtschaft und Europaangelegenheiten:

### § 1  
Gebührentarif

Für Amtshandlungen im Geschäftsbereich des Ministers für Wirtschaft, Arbeit und Energie werden Gebühren nach dem Gebührentarif der Anlage zu dieser Verordnung erhoben.

### § 2  
Gebührenfreiheit und Gebührenbefreiung

(1) Zur Zahlung von Gebühren bleiben verpflichtet, für Amtshandlungen

1.  des Ministeriums für Wirtschaft, Arbeit und Energie nach § 10 Absatz 1 Satz 2 des Gesetzes über die Anerkennung als Kurort und Erholungsort im Land Brandenburg vom 14.
    Februar 1994 (GVBl.
    I S.
    10) die in § 8 Absatz 1 Nummer 6 des Gebührengesetzes für das Land Brandenburg genannten Rechtsträger,
2.  des Landesamtes für Bergbau, Geologie und Rohstoffe die in § 8 Absatz 1 Nummer 1, 6 und 7 des Gebührengesetzes für das Land Brandenburg genannten Rechtsträger; dies gilt nicht, wenn das Landesamt für Bergbau, Geologie und Rohstoffe als Träger öffentlicher Belange beteiligt ist.

(2) Gebühren werden nicht erhoben für Leistungen und Benutzungen im Rahmen der geowissenschaftlichen Gemeinschaftsaufgaben auf Veranlassung einer Behörde des Landes Brandenburg, eines anderen Bundeslandes oder des Bundes, es sei denn, dass sie einem Dritten zur Last gelegt werden können.

### § 3  
Gebührenbemessung

Soweit Gebühren nach erforderlichem Zeitaufwand zu berechnen sind, sind der Gebührenrechnung als Stundensätze zugrunde zu legen:

1.  für Beamte des höheren Dienstes und vergleichbare Angestellte 80,00 Euro,
2.  für Beamte des gehobenen Dienstes und vergleichbare Angestellte 60,00 Euro,
3.  für Beamte des mittleren Dienstes und vergleichbare Angestellte 48,00 Euro,
4.  für Beamte des einfachen Dienstes und vergleichbare Angestellte 38,00 Euro.

### § 4  
Auslagen

(1) In § 9 Absatz 1 Satz 2 des Gebührengesetzes für das Land Brandenburg aufgeführte Auslagen sind zu erstatten, soweit sie nicht bereits in die Gebühr einbezogen sind.

(2) Gegenüber dem Landesamt für Bergbau, Geologie und Rohstoffe sind außerdem zu erstatten, Auslagen

1.  für die Abgabe von geowissenschaftlichem Informationsmaterial (zum Beispiel Plots),
2.  für Aufwendungen für Geräte, Material und Geländeuntersuchungen einschließlich der Bezahlung von Aushilfskräften,
3.  für Aufwendungen, die Dritten und anderen Verwaltungsdienststellen für ihre besonderen Leistungen zu zahlen sind (zum Beispiel Bohrungen, Aufgrabungen, Spezialauswertungen, Druckkosten, Hilfsarbeiten),
4.  für Aufwendungen für Verpackungsmaterial und Versandkosten mit Ausnahme von Portokosten für Standardbriefe bis 50 Gramm,
5.  für Aufwendungen bei Abgabe digitaler Datensätze für den Einsatz der Datenverarbeitungsanlage nach dem jeweils erforderlichen Zeitaufwand mit einem Stundensatz von 30,00 Euro.

(3) Erreichen Auslagen nicht die Höhe von 5,00 Euro, kann von ihrer Erhebung abgesehen werden.

### § 5  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über die Verwaltungsgebühren im Geschäftsbereich des Ministers für Wirtschaft vom 12.
Dezember 2001 (GVBl.
II S.
642), die zuletzt durch Verordnung vom 17.
Februar 2010 (GVBl.
II Nr.
11) geändert worden ist, außer Kraft.

Potsdam, den 14.
Januar 2011

Der Minister für Wirtschaft  
und Europaangelegenheiten

Ralf Christoffers

**Anlage  
**(zu § 1)

Gebührentarif

Tarifstelle

Gegenstand

Gebühr (EUR)

**1**

**Allgemein**

 

1.1

Allgemeine Verwaltungsgebühren und Auslagen

 

1.1.1

Beglaubigungen

 

1.1.1.1

von Unterschriften oder Handzeichen

1,50

1.1.1.2

von Abschriften, Ablichtungen usw.
je Seite

1,50 – 3,00

1.1.1.3

von Urkunden zum Gebrauch im Ausland

8,00 – 73,00

1.1.2

Sonstige Bescheinigungen, Ausstellen von Urkunden und Zeugnissen (auch bei Wiederholungsausstellung)

1,50 – 26,00

1.1.3

Anfertigung von Zweitschriften, Fotokopien, Computerausdrucken

 

1.1.3.1

DIN A4, schwarz-weiß

 

1.1.3.1.1

für die ersten 50 Seiten, je Seite

0,50

1.1.3.1.2

für jede weitere Seite

0,15

1.1.3.2

DIN A3, schwarz-weiß

2,00

1.1.3.3

DIN A4 bis DIN A0 farbig, je Seite

2,00 – 15,00

1.1.3.4

DIN A2, schwarz-weiß

5,00

1.1.3.5

DIN A1, schwarz-weiß

10,00

1.1.3.6

DIN A0, schwarz-weiß

13,00

1.1.4

Datenbankrecherchen

 

1.1.4.1

Datenbankrecherche auf der Grundlage von bis zu drei Datenbanktabellen, pro Stunde

45,00 – 60,00

1.1.4.2

Datenbankrecherche auf der Grundlage von vier bis sechs Datenbanktabellen, pro Stunde

45,00 – 70,00

1.1.4.3

Datenbankrecherche auf der Grundlage von mehr als sechs Tabellen, pro Stunde

45,00 – 80,00

1.2

Allgemeine Benutzungsgebühren

 

1.2.1

Nutzung von Diensträumen inkl.
Nutzung von Technik, pro angefangene Stunde

16,00

1.2.2

Gebühr für den Einsatz verwaltungseigener Kraftfahrzeuge pro Kilometer

0,41

1.3

Amtshandlungen, für die keine andere Tarifstelle vorgesehen ist und die nicht einem von der handelnden Behörde wahrzunehmenden besonderen öffentlichen Interesse dienen

0,00 – 10 000,00

1.4

Erteilung von Bescheiden über Widersprüche, wenn und soweit sie zurückgewiesen werden

 

1.4.1

Dritte, die sich durch die Sachentscheidung beschwert fühlen

3,00 – 512,00

1.4.2

gegen Kostenentscheidungen

3,00 – 103,00

**2**

**Gewerberechtliche Angelegenheiten**

 

2.1

Anzeigen, Auskünfte

 

2.1.1

Bearbeitung von Gewerbeanzeigen (§ 14 Absatz 1, § 15 Absatz 1 der Gewerbeordnung – GewO)

 

2.1.1.1

Gewerbeanmeldung (§ 14 Absatz 1 Satz 1 GewO)

 

2.1.1.1.1

natürliche Person

nach Zeitaufwand, mindestens 26,00

2.1.1.1.2

juristische Person

 

2.1.1.1.2.1

mit einem gesetzlichen Vertreter

nach Zeitaufwand, mindestens 31,00

2.1.1.1.2.2

für jeden weiteren gesetzlichen Vertreter

nach Zeitaufwand, mindestens 13,00

2.1.1.1.3

Beim Ausschank alkoholischer Getränke (§ 3 Absatz 1 des Brandenburgischen Gaststättengesetzes – BbgGastG) erhöht sich die Gebühr für jede natürliche Person und jeden gesetzlichen Vertreter um

8,00

2.1.1.2

Gewerbeummeldung (§ 14 Absatz 1 Satz 2 Nummer 1 und 2 GewO)

 

2.1.1.2.1

natürliche und juristische Person

20,00

2.1.1.2.2

Bei einer Erweiterung auf Ausschank alkoholischer Getränke (§ 3 Absatz 1 BbgGastG) erhöht sich die Gebühr für jede natürliche Person und jeden gesetzlichen Vertreter um

8,00

2.1.2

Auskünfte aus dem Gewerberegister

 

2.1.2.1

Auskünfte aus den beim Gewerbeamt vorhandenen Unterlagen

 

2.1.2.1.1

für die erste bis zehnte Person, pro Person oder Betriebsstätte

12,00

2.1.2.1.2

für jede weitere Person oder Betriebsstätte

5,00

2.1.2.2

Auskünfte, wenn Nachfragen oder Ermittlungen über die beim Gewerbeamt vorhandenen Unterlagen hinaus erforderlich sind, pro Person oder Betriebsstätte

15,00

2.2

Erlaubnisse

 

2.2.1

Schaustellen von Personen

 

2.2.1.1

Erlaubnis zur Veranstaltung von Schaustellungen von Personen (§ 33a Absatz 1 GewO)

 

2.2.1.1.1

mit unbeschränkter Geltungsdauer

128,00 – 767,00

2.2.1.1.2

mit beschränkter Geltungsdauer

51,00 – 256,00

2.2.1.2

Fristverlängerung (§ 49 Absatz 3 in Verbindung mit Absatz 2 GewO)

25 Prozent der jeweils geltenden Genehmigungsgebühr

2.2.2

Spiele mit Gewinn

 

2.2.2.1

Spielgeräte mit Gewinnmöglichkeit

 

2.2.2.1.1

Erteilung einer Erlaubnis zum Aufstellen von Spielgeräten (§ 33c Absatz 1 GewO)

102,50 – 1 023,00

2.2.2.1.2

Bestätigung über die Geeignetheit eines Aufstellungsortes für Spielgeräte (§ 33c Absatz 3 GewO)

 

2.2.2.1.2.1

für Betriebe im Sinne des § 1 Absatz 1 Nummer 1 und 3 der Verordnung über Spielgeräte und andere Spiele mit Gewinnmöglichkeiten – SpielV

57,00

2.2.2.1.2.2

für Betriebe im Sinne des § 1 Absatz 1 Nummer 2 SpielV

85,50

2.2.2.2

Erteilung einer Erlaubnis zur Veranstaltung anderer Spiele mit Gewinnmöglichkeit (§ 33d Absatz 1 GewO)

25,50 – 256,00

2.2.2.3

Spielhallen und ähnliche Unternehmen

 

2.2.2.3.1

Erteilung einer Erlaubnis zum Betrieb einer Spielhalle oder eines ähnlichen Unternehmens (§ 33i Absatz 1 GewO)

255,60 – 2 046,00

2.2.2.3.2

Erteilung einer Fristverlängerung (§ 49 Absatz 3 in Verbindung mit Absatz 2 GewO)

25 Prozent der jeweils geltenden Genehmigungsgebühr

2.2.2.3.3

Erteilung einer Stellvertretungserlaubnis (§ 47 GewO)

50 Prozent der jeweils geltenden Genehmigungsgebühr

2.2.3

Pfandleihgewerbe

 

2.2.3.1

Erteilung einer Erlaubnis zum Betrieb des Pfandleih- und -vermittlungsgeschäftes (§ 34 Absatz 1 GewO)

102,50 – 921,00

2.2.3.2

Verlängerung der Pfandverwertungs- und -abführungsfrist für die Überschüsse (§ 9 Absatz 2, § 11 der Verordnung über den Geschäftsbetrieb der gewerblichen Pfandleiher – PfandlV)

2 Prozent des betreffenden Darlehens

2.2.3.3

Erteilung einer Stellvertretungserlaubnis (§ 47 GewO)

50 Prozent der jeweils geltenden Genehmigungsgebühr

2.2.4

Bewachungsgewerbe

 

2.2.4.1

Erteilung einer Erlaubnis zur Ausübung des Bewachungsgewerbes (§ 34a Absatz 1 GewO)

128,00 – 1 279,00

2.2.4.2

Regelüberprüfung der Zuverlässigkeit eines Bewachungsgewerbetreibenden oder Betriebsleiters (§ 34a Absatz 1 Satz 10 der GewO)

33,00 – 276,00

2.2.4.3

Erteilung einer Stellvertretungserlaubnis (§ 47 GewO)

50 Prozent der jeweils geltenden Genehmigungsgebühr

2.2.4.4

Überprüfung der Qualifikation bei einer vorübergehenden und gelegentlichen Bewachungsdienstleistung und Unterrichtung über das Wahlrecht (§ 13 der Bewachungsverordnung – BewachV)

nach Zeitaufwand, mindestens 26,40

2.2.4.5

Erstmalige Überprüfung der Zuverlässigkeit von Personen nach § 16 Absatz 1 BewachV (§ 16 Absatz 2 Satz 3 BewachV)

33,00 – 276,00

2.2.4.6

Regelüberprüfung der Zuverlässigkeit von Personen nach § 16 Absatz 1 BewachV (§ 16 Absatz 4 BewachV)

33,00 – 276,00

2.2.5

Versteigerergewerbe

 

2.2.5.1

Erteilung einer Erlaubnis zur Versteigerung fremder beweglicher Sachen, fremder Grundstücke oder fremder Rechte (§ 34b Absatz 1 GewO)

200,00 – 1 500,00

2.2.5.2

Erteilung einer öffentlichen Bestellung und Vereidigung von besonders sachkundigen Versteigerern (§ 34b Absatz 5 GewO)

75,00 – 350,00

2.2.5.3

Abkürzung und Verlängerung von Fristen

 

2.2.5.3.1

Abkürzung der Frist für die Anzeige der Versteigerung (§ 3 Absatz 1 Satz 2 der Verordnung über gewerbsmäßige Versteigerungen – VerstV)

28,00

2.2.5.3.2

Abkürzung der Frist von fünf Tagen für die neue Versteigerung am Ort der vorhergehenden Versteigerung (§ 3 Absatz 3 Satz 3 VerstV)

28,00

2.2.5.3.3

Verlängerung der Versteigerung über sechs Tage (§ 3 Absatz 3 Satz 3 VerstV)

28,00

2.2.5.4

Zulassung von Ausnahmen (soweit nicht § 6 Absatz 1 Nummer 1 bis 3 VerstV selbst Ausnahmen zulässt)

 

2.2.5.4.1

von den Anforderungen des § 2 Absatz 1 VerstV bei freiwilligen Hausrat- und Nachlassversteigerungen (§ 2 Absatz 2 Satz 2 VerstV)

15,00 – 75,00

2.2.5.4.2

von dem Gebot, mindestens zwei Stunden Gelegenheit zur Besichtigung des Versteigerungsgutes zu geben (§ 4 Satz 2 VerstV)

15,00 – 75,00

2.2.5.4.3

von dem Verbot, neue Handelsware zu versteigern (§ 6 Absatz 1 Satz 2 VerstV)

30,00 – 150,00

2.2.5.4.4

von dem Verbot einer Versteigerung, die in räumlichem oder zeitlichem Zusammenhang mit einer anderen Verkaufsveranstaltung steht (§ 6 Absatz 2 Satz 2 VerstV)

20,00 – 100,00

2.2.5.4.5

von dem Verbot, das Versteigerungsgut zum Zwecke der Versteigerung in eine andere Gemeinde zu verbringen (§ 6 Absatz 2 Satz 2 VerstV)

20,00 – 100,00

2.2.5.4.6

Zulassung von Ausnahmen für die Versteigerung leicht verderblicher Waren auf Veranstaltungen nach den §§ 64 bis 67 GewO (§ 71b Absatz 2 Satz 2 GewO)

20,00 – 100,00

2.2.5.5

Erteilung einer Stellvertretungserlaubnis (§ 47 GewO)

50 Prozent der jeweils geltenden Genehmigungsgebühr

2.2.6

Makler, Bauträger, Baubetreuer, Finanzanlagenvermittler, Honorar-Finanzanlagenberater, Immobiliardarlehensvermittler, Wohnimmobilienverwalter

 

2.2.6.1

Erteilung einer Erlaubnis zur Ausübung des Maklergewerbes (§ 34c Absatz 1 Nummer 1 GewO)

450,00

2.2.6.2

Erteilung einer Erlaubnis zur Ausübung des Darlehensvermittlergewerbes mit Ausnahme von Verträgen im Sinne des § 34i Absatz 1 Satz 1 GewO (§ 34c Absatz 1 Nummer 2 GewO)

450,00

2.2.6.3

Erteilung einer Erlaubnis zur Ausübung des Bauträgergewerbes (§ 34c Absatz 1 Nummer 3 Buchstabe a GewO)

450,00

2.2.6.4

Erteilung einer Erlaubnis zur Ausübung des Baubetreuergewerbes (§ 34c Absatz 1 Nummer 3 Buchstabe b GewO)

450,00

2.2.6.5

Erteilung einer Erlaubnis zur Ausübung des Wohnimmobilienverwaltergewerbes (§ 34c Absatz 1 Nummer 4 GewO)

510,00

2.2.6.6

Erteilung einer Erlaubnis zur Ausübung des Finanzanlagenvermittlergewerbes (§ 34f Absatz 1 Satz 1 Nummer 1, 2 und 3 GewO)

612,00

2.2.6.6.1

beschränkt sich die Erlaubnis nach Tarifstelle 2.2.6.6 nur auf zwei Nummern oder nur auf eine Nummer nach § 34f Absatz 1 Satz 1 Nummer 1, 2 und 3 GewO, verringert sich die Gebühr jeweils um

51,00

2.2.6.7

Erteilung einer Erlaubnis zur Ausübung des Honorar-Finanzanlagenberaters (§ 34h Absatz 1 GewO)

510,00

2.2.6.8

Erteilung einer Erlaubnis zur Ausübung des Immobiliardarlehensvermittlers (§ 34i Absatz 1 GewO)

510,00

2.2.6.9

bei gleichzeitiger Erteilung von insgesamt zwei Erlaubnissen und für jede weitere Erlaubnis nach den Tarifstellen 2.2.6.1 bis 2.2.6.8 verringert sich die Gebühr um

225,00

2.2.6.10

Erteilung einer Stellvertretererlaubnis (§ 47 GewO)

50 Prozent der jeweils geltenden Genehmigungsgebühr

2.2.6.11

Anordnung der Vorlage und Prüfung der Weiterbildungsnachweise (§ 15b Absatz 3 der Makler- und Bauträgerverordnung – MaBV)

Zeitgebühr,  
jedoch mindestens 60,00

2.2.7

Gewerbeuntersagung, Gestattung der Fortführung, Wiedergestattung

 

2.2.7.1

Gewerbeuntersagung ganz oder teilweise gegenüber dem Gewerbetreibenden (§ 35 Absatz 1 GewO) oder gegenüber dem Vertretungsberechtigten oder der mit der Leitung des Gewerbebetriebs beauftragten Person (§ 35 Absatz 7a GewO)

378,00 – 2 724,00

2.2.7.2

Gestattung der Fortführung des Betriebes durch einen Stellvertreter (§ 35 Absatz 2 GewO)

25,50 – 256,00

2.2.7.3

Wiedergestattung der Ausübung des Gewerbes (§ 35 Absatz 6 GewO)

102,50 – 512,00

2.2.8

Erteilung einer Gestattung zum Betreiben eines Gewerbes ohne den nach § 45 GewO befähigten Stellvertreter (§ 46 Absatz 3 GewO)

25,50 – 154,00

2.2.9

Reisegewerbe

 

2.2.9.1

Erteilung einer Reisegewerbekarte (§ 55 GewO)

 

2.2.9.1.1

unbefristet

40,00 – 500,00

2.2.9.1.2

befristet, je angefangenes Jahr

20,00 – 150,00

2.2.9.2

Verlängerung der Geltungsdauer (§ 55 GewO)

50 Prozent von Tarifstelle 2.2.9.1.2

2.2.9.3

Änderung der zugelassenen Reisegewerbetätigkeit (§ 55 GewO)

30,50 – 103,00

2.2.9.4

Ausstellung einer Zweitschrift oder beglaubigten Kopie der Reisegewerbekarte (§ 60c Absatz 2 GewO)

15,50

2.2.9.5

Erteilung einer Erlaubnis zum Feilbieten von Waren gelegentlich auf Messen, Ausstellungen, öffentlichen Festen oder aus besonderem Anlass (§ 55a Absatz 1 Nummer 1 GewO)

10,00 – 41,00

2.2.9.6

Zulassung von Ausnahmen von dem Erfordernis der Reisegewerbekarte für besondere Verkaufsveranstaltungen (§ 55a Absatz 2 GewO)

10,00 – 41,00

2.2.9.7

Erteilung einer Gewerbelegitimationskarte (§ 55b Absatz 2 GewO)

20,00

2.2.9.8

Bescheinigung des Empfangs der Anzeige über den Beginn einer reisegewerbekartenfreien Tätigkeit (§ 55c GewO)

20,50

2.2.9.9

Zulassung von Ausnahmen von dem Verbot der Ausübung des Reisegewerbes an Sonn- und Feiertagen (§ 55e Absatz 2 GewO)

10,00 – 128,00

2.2.9.10

Zulassung von Ausnahmen im Einzelfall von den Verboten des § 56 Absatz 1 GewO (§ 56 Absatz 2 Satz 3 GewO)

25,50 – 103,00

2.2.9.11

Wanderlager

 

2.2.9.11.1

Entgegennahme der Anzeige zur Veranstaltung eines Wanderlagers (§ 56a Absatz 1 GewO)

11,00

2.2.9.11.2

Untersagung der Veranstaltung eines Wanderlagers (§ 56a Absatz 2 GewO)

51,00 – 383,00

2.2.9.12

Erteilung einer Erlaubnis

 

2.2.9.12.1

für die Veranstaltung eines anderen Spiels im Sinne des § 33d Absatz 1 Satz 1 GewO im Reisegewerbe (§ 60a Absatz 2 GewO)

25,50 – 128,00

2.2.9.12.2

zum Betrieb einer Spielhalle oder eines ähnlichen Unternehmens im Sinne des § 33i GewO im Reisegewerbe (§ 60a Absatz 3 GewO)

51,00 – 256,00

2.2.10

Rücknahme oder Widerruf einer Erlaubnis (§ 33d GewO in Verbindung mit den §§ 48, 49 VwVfG)

378,00 – 2 724,00

2.3

Messen, Ausstellungen, Märkte, Volksfeste

 

2.3.1

Festsetzung von Messen (§ 6e GewO), Ausstellungen (§ 65 GewO), Volksfesten (§ 60b GewO), Großmärkten (§ 66 GewO), Wochenmärkten (§ 67 GewO), Spezialmärkten (§ 68 Absatz 1 GewO) und Jahrmärkten (§ 68 Absatz 2 GewO) nach Gegenstand, Zeit, Öffnungszeit und Platz (§ 69 Absatz 1 GewO)

50,00 – 2 000,00

2.3.2

Änderung oder Aufhebung einer Festsetzung (§ 69b GewO)

25 Prozent der Gebühr nach Tarifstelle 2.3.1

2.3.3

Untersagung der Teilnahme an einer Veranstaltung (§ 70a GewO)

371,00 – 1 697,00

2.4

Gaststätten

 

2.4.1

Bescheinigung des Empfangs der Anzeige eines vorübergehenden Gaststättengewerbes (§ 2 Absatz 2 Satz 2 BbgGastG)

28,00

2.4.2

Bescheinigung der Änderung der Anzeige (§ 2 Absatz 3 in Verbindung mit Absatz 2 BbgGastG)

11,00

2.4.3

Zulassung von Ausnahmen für den Ausschank aus Automaten (§ 4 Satz 4 BbgGastG)

33,50

2.5

Rücknahme oder Widerruf einer Erlaubnis nach den Tarifstellen 2.2.1.1, 2.2.2.1.1, 2.2.2.3.1, 2.2.2.3.3, 2.2.3.1, 2.2.3.3, 2.2.4.1, 2.2.4.3, 2.2.5.1, 2.2.5.2, 2.2.5.5, 2.2.6.1 bis 2.2.6.10, 2.2.9.1, 2.2.9.5 und 2.2.9.12, sofern die oder der Betroffene dazu Anlass gegeben hat (§ 1 Absatz 1 VwVfGBbg in Verbindung mit den §§ 48, 49 VwVfG)

nach Zeitaufwand bis zur Höhe der für die Amtshandlung im Zeitpunkt der Rücknahme oder des Widerrufs festzusetzenden Gebühr

2.6 

Untersagung der Fortsetzung eines zulassungspflichtigen Handwerks als stehendes Gewerbe (§ 16 Absatz 3 der Handwerksordnung – HwO)

378,00 – 2 724,00

**3**

**Amtshandlungen des Landesamtes für Mess- und Eichwesen Berlin-Brandenburg**

 

3.1

Amtshandlungen und Leistungen des Landesamtes für Mess- und Eichwesen Berlin-Brandenburg (insbesondere Stellungnahmen, Beratungen, Auskünfte, Recherchen, Herausgabe sowie Bekanntgabe von digitalen Signaturen und Dateien, Überwachungen und Analysen)

nach Zeitaufwand

3.2

Maßnahmen zum Schutz der Gesundheit oder Sicherheit von Patienten, Anwendern oder anderen Personen oder zum Schutz der öffentlichen Gesundheit (§ 74 Medizinprodukterecht-Durchführungsgesetz - MPDG)

120,00 – 3 600,00

3.3

Überwachung der Voraussetzungen zum Inverkehrbringen, zur Inbetriebnahme, zum Bereitstellen auf dem Markt, zum Errichten, Betreiben und Anwenden (§ 77 Absatz 1 und 2 Nummer 1 MPDG)

120,00 – 3 600,00

3.4

Überwachung der ordnungsgemäßen Umsetzung der von der Bundesoberbehörde zum Schutz vor Risiken angeordneten Maßnahmen und eigenverantwortlich durchgeführten Sicherheitskorrekturmaßnahmen der Hersteller (§ 77 Absatz 3 MPDG)

120,00 – 4 200,00

3.5

Überwachung der Qualitätssicherung laboratoriumsmedizinischer Untersuchungen (§ 77 Absatz 1 MPDG in Verbindung mit § 9 Medizinprodukte-Betreiberverordnung - MPBetreibV)

120,00 – 3 600,00

3.6

Prüfung der Voraussetzungen für die Durchführung messtechnischer Kontrollen (§ 77 Absatz 1 MPDG in Verbindung mit § 14 MPBetreibV)

120,00 – 900,00

3.7

Maßnahmen zur Beseitigung festgestellter oder zur Vorbeugung künftiger Verstöße und bei sonstiger Nichtkonformität (§ 78 MPDG)

120,00 – 1 800,00

3.8

Anordnung der Untersuchung eines Produktes, soweit diese Untersuchung Maßnahmen nach sich ziehen (§ 79 Absatz 1 Nummer 3 MPDG)

120,00 – 3 600,00

**4**

**Energiewirtschaft**

 

4.1

Amtshandlungen nach dem Energiewirtschaftsgesetz (EnWG) – Energieaufsicht

 

4.1.1

Entscheidung über Anträge auf Genehmigung des Netzbetriebes (§ 4 Absatz 1 EnWG)

500,00 – 25 000,00

4.1.2

Untersagung eines Betriebes (§ 4 Absatz 2 Satz 2 EnWG)

1 000,00 – 50 000,00

4.1.3

Entscheidung über Einwände gegen die Entscheidung des Grundversorgers (§ 36 Absatz 2 Satz 4 EnWG)

160,00 – 5 000,00

4.1.4

Entscheidung zur Planfeststellung von Energieanlagen (§ 43 EnWG), deren Errichtungskosten

 

4.1.4.1

500 000,00 Euro nicht übersteigen

8 000,00

4.1.4.2

mehr als 500 000,00 Euro bis zu 2 500 000,00 Euro

8 000,00

zuzüglich 0,8 Prozent der  
500 000,00 Euro übersteigenden Errichtungskosten.

Soweit die Gebühr nach den Errichtungskosten zu berechnen ist, sind die voraussichtlichen Gesamtkosten jener Lieferung und Leistung zugrunde zu legen, die im Zeitpunkt der Genehmigung für die Errichtung bis zur Schlussabnahme erforderlich erscheinen, einschließlich Umsatzsteuer.

4.1.4.3

mehr als 2 500 000,00 Euro bis zu 7 500 000,00 Euro

24 000,00

zuzüglich 0,45 Prozent der  
2 500 000,00 Euro übersteigenden Errichtungskosten.

Soweit die Gebühr nach den Errichtungskosten zu berechnen ist, sind die voraussichtlichen Gesamtkosten jener Lieferung und Leistung zugrunde zu legen, die im Zeitpunkt der Genehmigung für die Errichtung bis zur Schlussabnahme erforderlich erscheinen, einschließlich Umsatzsteuer.

4.1.4.4

mehr als 7 500 000,00 Euro

46 500,00

zuzüglich 0,3 Prozent der  
7 500 000,00 Euro übersteigenden Errichtungskosten.

Soweit die Gebühr nach den Errichtungskosten zu berechnen ist, sind die voraussichtlichen Gesamtkosten jener Lieferung und Leistung zugrunde zu legen, die im Zeitpunkt der Genehmigung für die Errichtung bis zur Schlussabnahme erforderlich erscheinen, einschließlich Umsatzsteuer.

4.1.4.5

Entscheidungen der sonstigen Planänderungen, insbesondere notwendige Folgemaßnahmen

800,00 – 10 000,00

4.1.4.6

Entscheidungen über die Anzeige gemäß § 43f EnWG

500,00 – 10 000,00

4.1.5

Entscheidung zur Plangenehmigung von Energieanlagen (§ 43 Absatz 1 Satz 2 EnWG)

50 Prozent der Gebühr nach Tarifstelle 4.1.4

4.1.6

Anordnung der Duldung von Vorarbeiten (§ 44 Absatz 1 Satz 2 EnWG)

60,00 – 1 100,00

4.1.7

Festsetzung der Entschädigung wegen Duldung von Vorarbeiten (§ 44 Absatz 3 Satz 2 EnWG)

60,00 – 1 100,00

4.1.8

Feststellung der Zulässigkeit der Enteignung (§ 45 Absatz 2 Satz 2 EnWG)

260,00 – 8 500,00

4.1.9

Maßnahmen der technischen Aufsicht (§ 49 Absatz 5 EnWG)

160,00 – 15 000,00

4.1.10

Entscheidungen zu der Notwendigkeit von Maßnahmen zur Gewährleistung der technischen Sicherheit (§ 23 Absatz 1 Satz 2 Nummer 7 der Anreizregulierungsverordnung – ARegV)

160,00 – 15 000,00

4.1.11

Untersagung des Netzbetriebs oder vorläufige Verpflichtung des Netzbetreibers, ein Verhalten abzustellen (§ 4 Absatz 4 EnWG)

500,00 – 25 000,00

4.1.12

Maßnahmen zur Sicherstellung einer ordnungsgemäßen Durchführung des Verfahrens nach § 36 Absatz 2 Satz 1 und 2 EnWG (§ 36 Absatz 2 Satz 3 EnWG)

500,00 – 25 000,00

4.1.13

Feststellung der UVP-Pflicht gemäß §§ 5 ff.
des Gesetzes über die Umweltverträglichkeitsprüfung (UVPG).
Die Gebühr entfällt, wenn zugleich Gebühren nach der Tarifstelle 4.1.4 oder 4.1.5 erhoben werden.

100,00 – 1 500,00

4.2

Amtshandlungen nach der Verordnung über Gashochdruckleitungen (GasHDrLtgV)

 

4.2.1

Anordnung weitergehender Anforderungen, soweit sie nicht in Zusammenhang mit einer Anzeige stehen (§ 2 Absatz 2 GasHDrLtgV)

60,00 – 1 000,00

4.2.2

Zulassung von Ausnahmen von den Vorschriften der §§ 3 und 4 GasHDrLtgV und Abweichungen vom Stand der Technik (§ 2 Absatz 3 GasHDrLtgV)

60,00 – 10 000,00

4.2.3

Prüfung der Anzeige von Leitungsvorhaben oder Beanstandungen (§ 5 Absatz 1 und 2 GasHDrLtgV)

60,00 – 2 200,00

4.2.4

Prüfung der Nachweise (§ 6 Absatz 1 Nummer 2 in Verbindung mit § 5 Absatz 2 und 3 Satz 1 und Absatz 4 Satz 1 und 2 GasHDrLtgV)

120,00 – 2 200,00

4.2.5

Festsetzung von Fristen (§ 6 Absatz 2 Satz 2 GasHDrLtgV)

60,00 – 500,00

4.2.6

Untersagung des Betriebs oder Anordnung von Bedingungen und Auflagen (§ 6 Absatz 4 GasHDrLtgV)

60,00 – 2 000,00

4.2.7

Prüfung der Anzeige von Druckabsenkungen, Betriebseinstellungen und Stilllegungen (§ 7 Absatz 2 GasHDrLtgV)

60,00 – 500,00

4.2.8

Anordnungen von Überprüfungen (§ 10 Absatz 1 und 2 GasHDrLtgV)

60,00 – 500,00

4.2.9

Anerkennung von Sachverständigen (§ 11 Absatz 1 GasHDrLtgV)

nach Zeitaufwand

4.2.10

Prüfung der Anzeige vorübergehender grenzüberschreitender Tätigkeit von Sachverständigen (§ 18 GasHDrLtgV)

nach Zeitaufwand

4.3

Anordnung nach § 6 der Verordnung über Konzessionsabgaben für Strom und Gas (KAV) in Verbindung mit den §§ 65 und 69 EnWG – Energieaufsicht

160,00 – 15 000,00

4.4

Amtshandlungen nach dem EnWG und hierzu ergangenen Rechtsverordnungen – Landesregulierungsbehörde

 

4.4.1

Anordnung der Abschöpfung des wirtschaftlichen Vorteils und Auferlegung der Zahlung des entsprechenden Geldbetrages gegenüber dem Unternehmen nach § 33 Absatz 1 EnWG

2 500,00 – 75 000,00

4.4.2

Genehmigung der Entgelte für den Netz-zugang nach § 23a EnWG

1 000,00 – 50 000,00

4.4.3

Entscheidungen nach § 29 Absatz 1 EnWG über die Bedingungen und Methoden für den Netzanschluss oder den Netzzugang nach den in § 17 Absatz 3, § 21 Absatz 6 und § 24 EnWG genannten Rechtsverordnungen durch Festlegung gegenüber dem Netzbetreiber, einer Gruppe von oder gegenüber allen Netzbetreibern oder durch Genehmigung gegenüber dem Antragsteller

 

4.4.3.1

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 27 Absatz 1 der Stromnetzzugangsverordnung (StromNZV)

1 500,00 – 150 000,00

4.4.3.2

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 27 Absatz 2 StromNZV

2 500,00 – 70 000,00

4.4.3.3

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 27 Absatz 3 StromNZV

8 000,00 – 80 000,00

4.4.3.4

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 28 Absatz 1 bis 4 StromNZV

20 000,00 – 150 000,00

4.4.3.5

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 50 Absatz 1 der Gasnetzzugangsverordnung (GasNZV)

10 000,00 – 180 000,00

4.4.3.6

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 50 Absatz 2 GasNZV

10 000,00 – 175 000,00

4.4.3.7

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 50 Absatz 3 Satz 1 oder 2 GasNZV

10 000,00 – 90 000,00

4.4.3.8

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 50 Absatz 4 GasNZV

25 000,00 – 160 000,00

4.4.3.9

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 50 Absatz 5 GasNZV

8 000,00 – 80 000,00

4.4.3.10

Genehmigungen nach § 29 Absatz 1 EnWG in Verbindung mit § 19 Absatz 2 der Stromnetzentgeltverordnung (StromNEV)

500,00 – 15 000,00

4.4.3.11

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 29 StromNEV

500,00 – 5 000,00

4.4.3.12

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 30 Absatz 1 StromNEV

1 000,00 – 15 000,00

4.4.3.13

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 30 Absatz 2 StromNEV

1 000,00 – 15 000,00

4.4.3.14

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 30 Absatz 3 StromNEV

1 000,00 – 15 000,00

4.4.3.15

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 29 der Gasnetzentgeltverordnung (GasNEV)

500,00 – 5 000,00

4.4.3.16

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 30 Absatz 1 GasNEV

20 000,00 – 180 000,00

4.4.3.17

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 30 Absatz 2 GasNEV

20 000,00 – 180 000,00

4.4.3.18

_aufgehoben_

4.4.3.19

Festlegung nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 1 und § 4 Absatz 2 ARegV

1 000,00 – 180 000,00

4.4.3.20

Genehmigungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 1 und § 4 Absatz 4 ARegV

500,00 – 40 000,00

4.4.3.21

Festlegungen und Genehmigungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 10 und § 26 Absatz 2 ARegV

500,00 – 50 000,00

4.4.3.22

Sonstige Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 1 ARegV

500,00 – 100 000,00

4.4.3.23

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 2 ARegV

500,00 – 50 000,00

4.4.3.24

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 3 ARegV

500,00 – 50 000,00

4.4.3.25

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 4 ARegV

500,00 – 50 000,00

4.4.3.26

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 4a ARegV

1 000,00 – 100 000,00

4.4.3.27

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 5 ARegV

500,00 – 50 000,00

4.4.3.28

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 6 ARegV

500,00 – 100 000,00

4.4.3.29

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 7 ARegV

500,00 – 50 000,00

4.4.3.30

Genehmigungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 8 und § 23 ARegV

500,00 – 80 000,00

4.4.3.31

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 8 ARegV

500,00 – 100 000,00

4.4.3.32

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 8a ARegV

1 000,00 – 100 000,00

4.4.3.33

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 9 und § 24 Absatz 4 Satz 3 ARegV

500,00 – 10 000,00

4.4.3.34

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 9 ARegV

1 000,00 – 50 000,00

4.4.3.35

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 10 ARegV

500,00 – 100 000,00

4.4.3.36

Festlegung nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 11 ARegV

10 000,00 – 180 000,00

4.4.3.37

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 2 ARegV

500,00 – 100 000,00

4.4.3.38

Festlegungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 9a ARegV

1 000,00 – 100 000,00

4.4.3.39

Entscheidungen nach § 29 Absatz 1 EnWG in Verbindung mit § 25a ARegV

500,00 – 15 000,00

4.4.3.40

Entscheidungen nach § 29 Absatz 1 EnWG in Verbindung mit § 12g Absatz 1 EnWG

500,00 – 50 000,00

4.4.3.41

Entscheidungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 2 und § 5 Absatz 3 ARegV

500,00 – 50 000,00

4.4.3.42

Entscheidungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 2a und § 9 ARegV

500,00 – 50 000,00

4.4.3.43

Entscheidungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 3a und § 10a ARegV

500,00 – 50 000,00

4.4.3.44

Entscheidungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 10 und § 26 Absatz 3 bis 5 ARegV

500,00 – 50 000,00

4.4.3.45

Entscheidungen nach § 29 Absatz 1 EnWG in Verbindung mit § 118 Absatz 6 EnWG

500,00 – 15 000,00

4.4.3.46

Entscheidungen nach § 29 Absatz 1 EnWG in Verbindung mit § 32 Absatz 1 Nummer 10 und § 26 Absatz 6 ARegV

500,00 – 50 000,00

4.4.3.47

Festlegung nach § 29 Absatz 1 EnWG in Verbindung mit § 19a Absatz 2 Satz 2 EnWG

500,00 – 50 000,00

4.4.3.48

Feststellung nach § 19a Absatz 2 Satz 3 EnWG

500,00 – 50 000,00

4.4.4

Änderung einer Festlegung oder Genehmigung nach § 29 Absatz 2 EnWG

50,00 – 180 000,00

4.4.5

Verpflichtung, eine Zuwiderhandlung gegen § 30 Absatz 1 EnWG abzustellen nach § 30 Absatz 2 EnWG

2 500,00 – 180 000,00

4.4.6

Abstellen der Zuwiderhandlung nach Einleitung eines Verfahrens nach § 30 Absatz 2 EnWG bevor eine Verfügung der Regulierungsbehörde ergangen ist

1 250,00 – 90 000,00

4.4.7

Ablehnung eines Antrags nach § 31 Absatz 2 EnWG

50,00 – 5 000,00

4.4.8

Entscheidungen der Regulierungsbehörde nach § 31 Absatz 3 EnWG

500,00 – 180 000,00

4.4.9

Aufsichtsmaßnahmen nach § 65 EnWG

500,00 – 180 000,00

4.4.10

Entscheidungen nach § 110 WG

 

4.4.10.1

Einstufung als geschlossenes Verteilernetz nach § 110 Absatz 2 EnWG

500,00 – 30 000,00

4.4.10.2

Überprüfung der Entgelte nach § 110 Absatz 4 EnWG

1 000,00 – 50 000,00

4.4.11

Erteilung von beglaubigten Abschriften nach § 91 Absatz 1 Satz 1 Nummer 9 EnWG

15,00

4.4.12

Untersagung nach § 19 Absatz 2 Satz 8 und 9 StromNEV

800,00 – 10 000,00

4.5

Amtshandlungen nach der Verordnung über Heizkostenabrechnung (HeizkostenV)

 

4.5.1

Bestätigung einer sachverständigen Stelle für Heiz- oder Warmwasserkostenverteiler (§ 5 Absatz 1 Satz 2 und 3 HeizkostenV)

1 200,00 – 4 500,00

4.5.2

Bestätigung einer Erweiterung der messtechnischen Befugnisse oder einer sonstigen Änderung einer bestätigten sachverständigen Stelle (§ 5 Absatz 1 Satz 2 und 3 HeizkostenV)

300,00 – 1 200,00

**5**

**Staatliche Anerkennung von Erholungsorten**

 

5.1

Entscheidung über das Verleihen der Artbezeichnung „Staatlich anerkannter Erholungsort“ (§ 10 des Brandenburgischen Kurortegesetzes – BbgKOG)

1 660,00 – 2 140,00

5.2

Wiederholungsprüfung der Anerkennungsvoraussetzungen für die Verleihung der Artbezeichnung „Staatlich anerkannter Erholungsort“ (§ 13 Absatz 1 BbgKOG)

1 660,00

5.3

Rücknahme und Widerruf der staatlichen Anerkennung (§ 14 BbgKOG)

1 820,00

**6**

**Schornsteinfegerwesen**

 

6.1

Erlass einer Duldungsverfügung (§ 1 Absatz 4 Satz 1 des Schornsteinfeger-Handwerksgesetzes – SchfHwG)

97,00

6.2

Durchsetzung der Duldungsverfügung (§ 1 Absatz 4 Satz 1 SchfHwG)  
  
Hinweis: Neben der Gebühr werden Aufwendungen, die zum Zweck der Durchsetzung der Duldungsverfügung entstehen und die den Umständen nach erforderlich sind, als Auslagen erhoben.

50,00 - 500,00

6.3

Bewerbung für die Tätigkeit als bevollmächtigte Bezirksschornsteinfegerin oder bevollmächtigter Bezirksschornsteinfeger

 

6.3.1

Prüfung der Voraussetzungen und Ermittlung der Bewertungspunkte bei erstrangiger Bewerbung zu einem Vergabetermin (§ 9a Absatz 3 SchfHwG)

84,00

6.3.2

Prüfung der Bewertungspunkte bei nachrangiger Bewerbung bei jeder anderen Behörde zum selben Vergabetermin nach Tarifstelle 6.3.1 (§ 9 Absatz 1 SchfHwG)

36,00

6.3.3

Erneute Bewerbung nach Tarifstelle 6.3.1 oder Tarifstelle 6.3.2 zu jedem weiteren Vergabetermin (§ 9 Absatz 1 SchfHwG)

54,00

6.3.4

Fertigung des Ablehnungsbescheides (§ 9a Absatz 3 SchfHwG)

19,00

6.4

Bestellung als bevollmächtigte Bezirksschornsteinfegerin oder bevollmächtigter Bezirksschornsteinfeger; Vertretungsfälle

 

6.4.1

Bestellung als bevollmächtigte Bezirksschornsteinfegerin oder bevollmächtigter Bezirksschornsteinfeger nach § 8 Absatz 1 in Verbindung mit § 10 Absatz 1 SchfHwG

nach Zeitaufwand  
mindestens 341,00

6.4.2

Bestellung als bevollmächtigte Bezirksschornsteinfegerin oder bevollmächtigter Bezirksschornsteinfeger nach § 8 Absatz 1 in Verbindung mit § 10 Absatz 3 Satz 1 SchfHwG

46,00

6.4.3

Anordnung der Vertretung (§ 11 Absatz 3 Satz 2 SchfHwG)

46,00

6.4.4

Bestimmung des Vertreters (§ 11 Absatz 3 Satz 3 SchfHwG)

nach Zeitaufwand  
mindestens 46,00

6.5

Aufhebung der Bestellung

 

6.5.1

Aufhebung nach § 12 Absatz 1 Nummer 1 SchfHwG

68,00

6.5.2

Aufhebung nach § 12 Absatz 1 Nummer 2 SchfHwG

nach Zeitaufwand  
mindestens 154,00

6.5.3

Aufhebung nach § 12 Absatz 1 Nummer 3 SchfHwG

140,00

6.5.4

Aufforderung zur Vorlage eines amtsärztlichen Gutachtens (§ 12 Absatz 2 SchfHwG)

70,00

6.6

Erlass eines Bescheides zur Feststellung rückständiger Gebühren und Auslagen (§ 20 Absatz 3 SchfHwG)

75,00

6.7

Überprüfung der bevollmächtigten Bezirksschornsteinfegerin oder des bevollmächtigten Bezirksschornsteinfegers (§ 21 Absatz 1 Satz 2 SchfHwG)

6.7.1

wenn wesentliche Pflichtverletzungen festgestellt werden

nach Zeitaufwand, mindestens 72,00

6.7.2

wenn die Überprüfung durch die bevollmächtigte Bezirksschornsteinfegerin oder den bevollmächtigten Bezirksschornsteinfeger begehrt wurde

48,00 – 96,00

6.8

Erlass eines Zweitbescheides (§ 25 Absatz 2 SchfHwG)

97,00

6.9

Anwendung des Zwangsmittels Ersatzvornahme (§ 26 Absatz 2 Satz 1 SchfHwG)  
  
Hinweis: Neben der Gebühr werden Aufwendungen, die zum Zweck der Ausführung der Ersatzvornahme entstehen und die den Umständen nach erforderlich sind, als Auslagen erhoben.

50,00 – 500,00

6.10

Erhöhung der Anzahl der Kehrungen oder Überprüfungen (§ 1 Absatz 5 Kehr- und Überprüfungsordnung - KÜO)

nach Zeitaufwand, mindestens 36,00

6.11

Abweichende Regelung für nach Bundesimmissionsschutzgesetz genehmigte kehr- und überprüfungspflichtige Anlagen (§ 1 Absatz 6 KÜO)

nach Zeitaufwand, mindestens 36,00

**7**

**Anerkennung von Unternehmensbeteiligungsgesellschaften**

 

7.1

Entscheidung über die Anerkennung als Unternehmensbeteiligungsgesellschaft (§ 14 Absatz 2 des Gesetzes über Unternehmensbeteiligungsgesellschaften – UBGG)

600,00 – 3 600,00

**8**

**Amtshandlungen nach dem Grundbuchbereinigungsgesetz (GBBerG)**

 

8.1

Antrag auf Erteilung einer Bescheinigung (§ 9 Absatz 4 GBBerG)

pro Grundbuchamtsbezirk 260,00 zzgl.
2,50 pro Flurstück (Rahmenhöchstsatz  
5 000,00)

8.2

Erteilung einer Verzichtsbescheinigung auf Antrag des Energieversorgungsunternehmens (§ 9 Absatz 6 GBBerG)

pro Grundbuchamtsbezirk 260,00

8.3

Antrag auf Erteilung einer Erlöschensbescheinigung (§ 9 Absatz 7 GBBerG)

pro Grundbuchblatt 50,00

8.4

bei Änderung eines Antrages nach Tarifstelle 8.1

pro Flurstück 2,50

**9**

**Geologie und Rohstoffe**

 

9.1

Amtshandlungen und Leistungen des Landesamtes für Bergbau, Geologie und Rohstoffe (Stellungnahmen, Beratungen, Auskünfte, Recherchen, Analysen)

nach Zeitaufwand

9.2

_aufgehoben_

 

9.2.1

_aufgehoben_

 

9.2.2

_aufgehoben_

 

9.2.3

_aufgehoben_

 

9.3

_aufgehoben_

 

9.3.1

_aufgehoben_

 

9.3.1.1

_aufgehoben_

 

9.3.1.2

_aufgehoben_

 

9.3.1.3

_aufgehoben_

 

9.3.1.4

_aufgehoben_

 

9.3.1.5

_aufgehoben_

 

9.3.1.6

_aufgehoben_

 

9.3.1.7

_aufgehoben_

 

9.3.2

_aufgehoben_

 

9.3.2.1

_aufgehoben_

 

9.3.2.2

_aufgehoben_

 

9.3.2.3

_aufgehoben_

 

9.3.2.4

_aufgehoben_

 

9.3.2.5

_aufgehoben_

 

9.3.2.6

_aufgehoben_

 

9.3.2.6.1

_aufgehoben_

 

9.3.2.6.2

_aufgehoben_

 

9.3.3

_aufgehoben_

 

9.3.3.1

_aufgehoben_

 

9.3.3.2

_aufgehoben_

 

9.3.4

_aufgehoben_

 

9.3.4.1

_aufgehoben_

 

9.3.4.2

_aufgehoben_

 

9.3.5

_aufgehoben_

 

9.3.5.1

_aufgehoben_

 

9.3.5.2

_aufgehoben_

 

9.3.6

_aufgehoben_

 

9.3.7

_aufgehoben_

 

9.3.8

_aufgehoben_

 

9.3.8.1

_aufgehoben_

 

9.3.8.2

_aufgehoben_

 

9.3.9

_aufgehoben_

 

9.3.9.1

_aufgehoben_

 

9.3.9.2

_aufgehoben_

 

9.4

Abgabe von geowissenschaftlichen Daten zur befristeten Nutzung (nach Umfang und Bedeutung der Daten)

10,00 – 200,00 jährlich, je Kartenblattschnitt und geologisches Thema

9.5

Abgabe von geologischen Datenverarbeitungsprogrammen

10,00 – 2 500,00

9.6

_aufgehoben_

 

**10**

**Bergbau (Landesamt für Bergbau, Geologie und Rohstoffe)**

 

10.1

Bergbauberechtigungen

 

10.1.1

Entscheidung über die Erteilung einer Erlaubnis (§§ 6, 7, 11 des Bundesberggesetzes – BBergG)

 

10.1.1.1

zu gewerblichen Zwecken

1 000,00 – 8 000,00

10.1.1.2

zu wissenschaftlichen Zwecken

500,00 – 1 500,00

10.1.1.3

zur großräumigen Aufsuchung

250,00 – 4 000,00

10.1.2

Entscheidung über die Erteilung einer Bewilligung (§§ 6, 8, 12 BBergG)

2 000,00 – 20 000,00

10.1.3

Entscheidung über die Verleihung von Bergwerkseigentum (§§ 6, 9, 13 BBergG)

2 000,00 – 30 000,00

10.1.4

Entscheidung über die Verlängerung einer Erlaubnis (§ 16 Absatz 4 BBergG)

 

10.1.4.1

zu gewerblichen Zwecken

500,00 – 4 000,00

10.1.4.2

zu wissenschaftlichen Zwecken

250,00 – 750,00

10.1.4.3

zur großräumigen Aufsuchung

250,00 – 2 000,00

10.1.5

Entscheidung über die Verlängerung einer Bewilligung oder von Bergwerkseigentum (§ 16 Absatz 5 BBergG)

500,00 – 8 000,00

10.1.6

Nachträgliche Aufnahme, Änderung oder Ergänzung von Auflagen (§ 16 Absatz 3 BBergG)

250,00 – 2 500,00

10.1.7

Ausstellung einer Berechtsamsurkunde (§ 17 BBergG)

250,00 – 500,00

10.1.8

Entscheidung über den Widerruf einer Erlaubnis, einer Bewilligung oder von Bergwerkseigentum (§ 18 BBergG)

500,00 – 5 000,00

10.1.9

Fristverlängerung sowie Fristsetzung einer Erlaubnis (§ 18 Absatz 2 BBergG)

50,00 – 250,00

10.1.10

Entscheidung über die vollständige oder teilweise Aufhebung einer Erlaubnis oder einer Bewilligung (§ 19 BBergG)

200,00 – 1 000,00

10.1.11

Entscheidung über die Aufhebung von Bergwerkseigentum (§ 20 BBergG)

200,00 – 2 000,00

10.1.12

Stellung eines Verlangens (§ 21 Absatz 2 BBergG)

100,00 – 200,00

10.1.13

Entscheidung über die Zustimmung zur Übertragung einer Erlaubnis oder Bewilligung oder zur Beteiligung Dritter (§ 22 Absatz 1 BBergG)

200,00 – 1 500,00

10.1.14

Entscheidung über die Genehmigung der Veräußerung von Bergwerkseigentum und des schuldrechtlichen Vertrages hierüber sowie die Erteilung eines Zeugnisses (§ 23 BBergG)

500,00 – 3 000,00

10.1.15

Entscheidung über die Genehmigung der Vereinigung, Teilung oder des Austausches von Bergwerksfeldern (§§ 25, 26 28, 29 BBergG)

150,00 – 2 500,00

10.1.16

Bestellung eines Vertreters von Amts wegen (§ 36 Satz 1 Nummer 2 BBergG)

50,00 – 100,00

10.1.17

Beurkundung der Einigung über die Zulegung (§ 36 Satz 1 Nummer 3 BBergG)

150,00 – 1 500,00

10.1.18

Entscheidung über den Antrag auf Zulegung (§ 36 Satz 1 Nummer 4 BBergG)

100,00 – 1 000,00

10.1.19

Entscheidung über die Verlängerung einer Zulegung (§ 38 Absatz 1, § 16 Absatz 5 BBergG)

50,00 – 500,00

10.1.20

Entscheidung über die Gewinnung von Bodenschätzen bei der Aufsuchung (§ 41 BBergG)

50,00 – 500,00

10.1.21

Entscheidung über die Mitgewinnung von Bodenschätzen (§ 42 Absatz 1, § 43 BBergG)

50,00 – 1 000,00

10.1.22

Entscheidung über die Trennung von Bodenschätzen und die Größe der Anteile (§ 42 Absatz 4, § 43, § 45 Absatz 2 BBergG)

50,00 – 500,00

10.1.23

Entscheidung über die Mitgewinnung von Bodenschätzen bei Anlegung von Hilfsbauten (§ 45 Absatz 1 BBergG)

50,00 – 500,00

10.1.24

Entscheidung über die Benutzung fremder Grubenbaue (§ 47 Absatz 4 BBergG)

50,00 – 500,00

10.1.25

Entscheidung über die Bestätigung der Aufrechterhaltung alter Rechte oder Verträge (§ 149 BBergG)

50,00 – 500,00

10.1.26

Entscheidung über die Verlängerung aufrechterhaltender Rechte oder Verträge (§ 152 Absatz 2 Satz 2, § 153 Satz 3 BBergG)

100,00 – 2 500,00

10.1.27

Entscheidung über den Inhalt eines aufrechterhaltenden Rechtes (§ 154 Absatz 1 Satz 3 BBergG)

50,00 – 500,00

10.1.28

Ausstellung einer Ersatzurkunde (§ 154 Absatz 2 BBergG)

50,00 – 500,00

10.1.29

Entscheidung über die Genehmigung zur Abtretung, Überlassung oder Änderung aufrechterhaltender Rechte oder Verträge (§ 156 Absatz 2 BBergG)

50,00 – 500,00

10.1.30

Entscheidung über die Ausdehnung von Bergwerkseigentum (§§ 161, 162 BBergG)

250,00 – 2 000,00

10.2

Einsichtnahme, Auskunft

10.2.1

Einsichtnahme in das Berechtsamsbuch und die Berechtsamskarte (§ 76 Absatz 1 BBergG)

10.2.1.1

ohne Inanspruchnahme der Dienstkraft

gebührenfrei

10.2.1.2

mit Inanspruchnahme einer Dienstkraft

 

10.2.1.2.1

bis zu der Dauer einer halben Stunde

gebührenfrei

10.2.1.2.2

beim Überschreiten einer halben Stunde für die darüber hinausgehende Zeit

nach Zeitaufwand

10.2.2

Schriftliche Auskünfte aus dem Berechtsamsbuch und den Berechtsamsurkunden, Ablichtungen (§ 76 Absatz 2 BBergG)

nach Zeitaufwand

10.2.3

Einsichtnahme in Grubenbilder (§ 63 Absatz 4 BBergG)

 

10.2.3.1

mit Inanspruchnahme einer Dienstkraft bis zu der Dauer einer halben Stunde

gebührenfrei

10.2.3.2

beim Überschreiten einer halben Stunde für die darüber hinausgehende Zeit

nach Zeitaufwand

10.2.4

Einsichtnahme in Ergebnisse von Messungen (§ 125 Absatz 1 BBergG) und Auszüge aus den Messunterlagen

 

10.2.4.1

mit Inanspruchnahme einer Dienstkraft bis zu der Dauer einer halben Stunde

gebührenfrei

10.2.4.2

beim Überschreiten einer halben Stunde für die darüber hinausgehende Zeit

nach Zeitaufwand

10.2.5

Auszüge

 

10.2.5.1

aus den Messunterlagen

nach Zeitaufwand

10.2.5.2

aus der Berechtsamskarte und den sonstigen bergbaulichen Riss- oder Kartendarstellungen

nach Zeitaufwand

10.2.6

Prüfung und Beglaubigung von vorgelegten Kartenauszügen

nach Zeitaufwand

10.2.7

Schriftliche Auskünfte bei Bergbauanfragen bei Nichtvorhandensein haftungspflichtiger Unternehmen bzw.
Bergbauberechtigter – Baugrundbeurteilungen (§§ 115, 166 BBergG)

nach Zeitaufwand

10.3

Bergwerksbetrieb

 

10.3.1

Entscheidung über die Zulassung eines Betriebsplanes (§§ 51 bis 55 BBergG)

 

10.3.1.1

Rahmenbetriebsplan ohne Durchführung eines Planfeststellungsverfahrens

2 000,00 – 25 000,00

10.3.1.2

Rahmenbetriebsplan mit Durchführung eines Planfeststellungsverfahrens

5 000,00 – 500 000,00

10.3.1.3

Zulassung des vorzeitigen Beginns (§ 57b Absatz 1 BBergG)

1 000,00 – 50 000,00

10.3.1.4

Aufhebung eines Planfeststellungsbeschlusses (§ 5 BBergG in Verbindung mit § 77 des Verwaltungsverfahrensgesetzes, § 1 Absatz 1 Satz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg)

500,00 – 5 000,00

10.3.1.5

Entscheidung über den Untersuchungsrahmen zur Erarbeitung eines Rahmenbetriebsplanes (§ 52 Absatz 2a Satz 2 BBergG)

200,00 – 2 000,00

10.3.1.6

Entscheidung über die Zulassung eines Hauptbetriebsplanes

500,00 – 12 500,00

10.3.1.7

Entscheidung über die Zulassung eines Sonderbetriebsplanes

300,00 – 10 000,00

10.3.1.8

Entscheidung über die Zulassung eines Abschlussbetriebsplanes

500,00 – 25 000,00

10.3.1.9

Ergänzung, Änderung, Verlängerung von Betriebsplänen

– gemäß Tarifstelle 10.3.1.1 und 10.3.1.8  
– gemäß Tarifstelle 10.3.1.2  
– gemäß Tarifstellen 10.3.1.6 und 10.3.1.7 bis 10.3.1.8

   

250,00 – 10 000,00  
1 000,00 – 50 000,00  
100,00 – 5 000,00

10.3.1.10

Tätigkeiten im Zusammenhang mit einer Umweltverträglichkeitsprüfung

 

10.3.1.10.1

Prüfung der Umweltverträglichkeit in einem Trägerverfahren

Erhöhung der jeweiligen  
Gebühr um 12,5 Prozent

10.3.1.10.2

Vorprüfung zur Feststellung der UVP-Pflicht im Trägerverfahren mit negativem Ergebnis

100,00 – 1 500,00

10.3.1.10.3

Feststellung der UVP-Pflicht gemäß §§ 5 ff.
UVPG vor Beginn des Verfahrens auf Antrag des Trägers des Vorhabens

100,00 – 1 500,00

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

10.3.1.10.4

Unterrichtung über voraussichtlich beizubringende Unterlagen bei UVP-pflichtigen Vorhaben auf Ersuchen des Trägers des Vorhabens vor Beginn des Verfahrens

100,00 – 1 000,00

Wird ein Antrag auf Entscheidung bei der für das Trägerverfahren zuständigen Behörde gestellt, so entfällt die Gebührenpflicht für die Feststellung der UVP-Pflicht in diesem Verfahren, wenn diese Feststellung von derselben Behörde getroffen wurde.
Eine bereits gezahlte Gebühr ist auf die Gebühr für die Entscheidung im Trägerverfahren anzurechnen.

10.3.2

Entscheidung über die Befreiung von der Betriebsplanpflicht (§ 51 Absatz 3 Satz 1 BBergG)

50,00 – 500,00

10.3.3

Entscheidung über die nachträgliche Aufnahme, Änderung oder Ergänzung von Auflagen im Betriebsplan nach § 56 Absatz 1 Satz 2 BBergG

100,00 – 2 500,00

10.3.4

Entscheidung über die Genehmigung einer Unterbrechung des Betriebes über zwei Jahre (§ 52 Absatz 1 Satz 2 BBergG)

100,00 – 1 000,00

10.3.5

Sonstige Anordnungen und Untersagungen gemäß § 71 Absatz 1 und 2, § 72 Absatz 1 und § 73 BBergG

250,00 – 5 000,00

10.3.6

Anordnung von Maßnahmen bei der Einstellung des Betriebes gemäß § 71 Absatz 3 BBergG

250,00 – 10 000,00

10.3.7

Entscheidung über die Genehmigung, Erlaubnis, Zustimmung, Prüfung, allgemeine Zulassung auf Grund einer Bergverordnung (§§ 65, 66, 67, 68, 176 Absatz 3 BBergG)

100,00 – 5 000,00

10.3.8

Entscheidung über die Bewilligung einer Ausnahme von Vorschriften einer Bergverordnung (§§ 65, 66, 67, 68, 176 Absatz 3 BBergG)

50,00 – 2 500,00

10.3.9

Entscheidung über die Verlängerung, Ergänzung oder Änderung einer Entscheidung nach Tarifstelle 10.3.7 oder Tarifstelle 10.3.8

50,00 – 2 500,00

10.3.10

Prüfung einer Anzeige eines nicht betriebsplanpflichtigen Betriebes (§ 127 BBergG)

50,00 – 1 000,00

10.3.11

Entscheidung über die Anerkennung einer Person oder Stelle als Sachverständiger (§ 65 BBergG)

nach Zeitaufwand

10.3.12

Entscheidung über die Einstufung eines Bodenschatzes als grundeigen gemäß § 3 Absatz 4 BBergG, soweit diese Entscheidung nicht in einem Verfahren nach Tarifstelle 10.3.1 getroffen wird

200,00 – 1 000,00

10.4

Streitentscheidung, Grundabtretung

 

10.4.1

Streitentscheidung (§ 40 BBergG)

250,00 – 2 500,00

10.4.2

Entscheidung über den Antrag auf Grundabtretung (§§ 77, 78 BBergG)

1 000,00 – 12 500,00

10.4.3

Entscheidung über die Zustimmung zur Abtretung eines bebauten Grundstückes (§ 79 Absatz 3 BBergG)

150,00 – 5 000,00

10.4.4

Entscheidung über die Ergänzungsentschädigung (§ 89 Absatz 2 BBergG)

150,00 – 2 500,00

10.4.5

Entscheidung über die Neufestsetzung wiederkehrender Leistungen (§ 89 Absatz 3 BBergG)

50,00 – 500,00

10.4.6

Entscheidung über eine Sicherheit (§ 89 Absatz 4, § 92 Absatz 1 Satz 2 und Absatz 2 Satz 2 BBergG)

50,00 – 500,00

10.4.7

Anordnung der Wiederherstellung des früheren Zustandes (§ 90 Absatz 5 BBergG)

50,00 – 500,00

10.4.8

Entscheidung über den Antrag auf Vorentscheidung (§ 91 BBergG)

1 000,00 – 7 500,00

10.4.9

Beurkundung der Einigung über die Grundabtretung (§ 92 Absatz 1 Satz 3 BBergG)

50,00 – 500,00

10.4.10

Anordnung der vorzeitigen Ausführung der Grundabtretung (§ 92 Absatz 2 Satz 1 BBergG)

50,00 – 500,00

10.4.11

Entscheidung über den Antrag auf Fristverlängerung (§ 95 Absatz 2 BBergG)

50,00 – 500,00

10.4.12

Entscheidung über den Antrag auf Aufhebung der Grundabtretung (§ 96 BBergG)

50,00 – 500,00

10.4.13

Entscheidung über den Antrag auf vorzeitige Besitzeinweisung (§ 97 BBergG)

1 000,00 – 7 500,00

10.4.14

Feststellung des Zustandes des Grundstückes (§ 99 BBergG)

50,00 – 500,00

10.4.15

Aufhebung oder Änderung der Besitzeinweisung oder Fristverlängerung (§ 101 Absatz 1 und 2 BBergG)

50,00 – 500,00

10.4.16

Entscheidung über den Antrag auf Festsetzung der Entschädigung oder das Aussprechen der Verpflichtung zur Wiederherstellung (§ 102 Absatz 2 BBergG)

150,00 – 1 500,00

10.4.17

Entscheidung über die Entschädigung für eine Wertminderung des Grundstückes (§ 109 Absatz 4 BBergG)

150,00 – 1 500,00

10.4.18

Entscheidung über die Entschädigung gemäß Anlage I Kapitel V Sachgebiet D Abschnitt III Nummer 1 Buchstabe e Doppelbuchstabe aa des Einigungsvertrages vom 31. August 1990

150,00 – 1 500,00

10.5

Markscheiderische Angelegenheiten

 

10.5.1

Entscheidung über die Anerkennung als Markscheider (§ 3 Brandenburgisches Markscheidergesetz)

nach Zeitaufwand

10.5.2

Entscheidung über Widerruf der Anerkennung als Markscheider (§ 5 Absatz 1 Brandenburgisches Markscheidergesetz)

nach Zeitaufwand

10.5.3

Entscheidung über Tätigkeitsuntersagung der Anerkennung als Markscheider (§ 5 Absatz 3 Markscheidergesetz)

nach Zeitaufwand

10.5.4

Entscheidung über die Anerkennung anderer Personen (§ 13 Markscheider-Bergverordnung – MarkschBergV)

nach Zeitaufwand

10.5.5

Entscheidung über die Veränderung der Nachtragungs- und Einreichungsfristen (§ 10 Absatz 3 MarkschBergV)

100,00 – 300,00

10.5.6

Entscheidung über die Bewilligung oder den Widerruf einer Ausnahme vom Erfordernis des Grubenbildes (§ 12 MarkSchBergV)

150,00 – 600,00

10.5.7

Entscheidung über die Zustimmung zur Nichteinreichung von Unterlagen (§ 63 Absatz 3 Satz 2 BBergG)

50,00 – 250,00

10.5.8

Entscheidung über die Erteilung einer Löschungsbewilligung alter Aufsuchungs- und Gewinnberechtigungen

nach Zeitaufwand

10.5.9

Amtshandlungen im Zusammenhang mit der Prüfung, Festlegung und Bekanntgabe eines Einwirkungsbereiches (§ 2 Absatz 4, § 3 Absatz 3 und 4, § 5, § 6 Nummer 2 der Bergverordnung über Einwirkungsbereiche – EinwirkungsBergV)

nach Zeitaufwand

**11**

**Maßnahmen nach dem Geldwäschegesetz (GwG)**

 

11.1

Befreiung von der Dokumentation der Risikoanalyse nach § 5 Absatz 4 GwG, soweit diese auf Antrag erfolgt

50,00 – 1 000,00

11.2

Anordnung zur Bestellung eines Geldwäschebeauftragten nach § 7 Absatz 3 GwG, soweit diese nicht durch eine Allgemeinverfügung erfolgt

50,00 – 1 000,00

11.3

Ausnahme von der Bestellung eines Geldwäschebeauftragten nach § 7 Absatz 2 GwG, soweit diese auf Antrag erfolgt

50,00 – 1 000,00

11.4

Anordnung zur Schaffung interner Sicherungsmaßnahmen nach § 6 Absatz 8 GwG, soweit diese nicht durch eine Allgemeinverfügung erfolgt

50,00 – 1 000,00

**12**

**Maßnahmen der Staatsaufsicht gegenüber genossenschaftlichen Prüfverbänden nach dem Genossenschaftsgesetz**

 

12.1

Untersuchungen nach § 64 Absatz 2 des Genossenschaftsgesetzes

nach Zeitaufwand

**13**

**Maßnahmen der Laufbahnordnungsbehörde nach der EU-Laufbahnbefähigungsanerkennungsverordnung (EU–LBAV)**

 

13.1

Entscheidung über den Antrag gemäß § 8 Absatz 1 EU-LBAV

nach Zeitaufwand

13.2

Durchführung einer Eignungsprüfung oder eines Anpassungslehrgangs gemäß § 8 Absatz 1 in Verbindung mit den §§ 5 bis 7 EU-LBAV

nach Zeitaufwand

13.3

Ausstellung einer Bescheinigung gemäß Anlage 2 zu § 9 EU-LBAV

20,00

**14**

**Amtshandlungen nach dem Berufsbildungsgesetz (BBiG)**

14.1

Widerrufliche Zuerkennung der fachlichen Eignung für Auszubildende (§ 30 Absatz 6 BBiG)

78,00

14.2

Befristete widerrufliche Zuerkennung der fachlichen Eignung zur Ausbildung (§ 30 Absatz 6 BBiG)

65,00

**15**

**Amtshandlungen nach der Handwerksordnung (HwO)**

15.1

Widerrufliche Zuerkennung der fachlichen Eignung zur Lehrlingsausbildung (§ 22b Absatz 5 HwO)

78,00

15.2

Befristete widerrufliche Zuerkennung der fachlichen Eignung zur Lehrlingsausbildung (§ 22b Absatz 5 HwO)

65,00

**16**

**Amtshandlungen nach dem Umsatzsteuergesetz (UStG)**

16.1

Ausstellen einer Bescheinigung zur Umsatzsteuerbefreiung bei beruflichen Bildungsmaßnahmen im Bereich der gewerblichen Wirtschaft zur Vorlage beim Finanzamt (§ 4 Nummer 21 Buchstabe a Doppelbuchstabe bb UStG)  
  
a.
für eine Bildungsmaßnahme  
b.
für 2 bis 10 Bildungsmaßnahmen  
c.
für 11 bis 20 Bildungsmaßnahmen  
d.
für mehr als 20 Bildungsmaßnahmen

  
  
  
55,00  
65,00  
80,00  
95,00