## Gebührenordnung für das amtliche Vermessungswesen im Land Brandenburg (Vermessungsgebührenordnung - VermGebO)

Auf Grund des § 3 Absatz 1 und 2 in Verbindung mit § 7 Absatz 1 Nummer 1, § 9 Satz 2 und § 18 Absatz 2 Satz 2 des Gebührengesetzes für das Land Brandenburg vom 7.
Juli 2009 (GVBl.
I S. 246), von denen § 18 Absatz 2 Satz 2 durch Artikel 5 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 32 S. 27) neu gefasst worden ist, verordnet der Minister des Innern und für Kommunales:

#### § 1 Anwendungsbereich

(1) Für die in der Anlage (Gebührentarif) aufgeführten öffentlichen Leistungen der Aufgabenträger des amtlichen Vermessungswesens sind Gebühren nach den dort genannten Gebührensätzen zu erheben.

(2) Die Verordnung ist nicht anzuwenden für die Bereitstellung und Nutzung der Geobasisinformationen der Liegenschaften in digitaler Form mit Ausnahme von Auszügen im PDF\-Format oder Bild-Format aus den Nachweisen des Liegenschaftskatasters.

#### § 2 Umsatzsteuer

Soweit die Amtshandlung der Umsatzsteuer unterliegt, werden die Gebühren nach dieser Verordnung zuzüglich der gesetzlichen Umsatzsteuer erhoben.

#### § 3 Gebührenpflicht für juristische Personen

Die in § 8 Absatz 1 des Gebührengesetzes für das Land Brandenburg genannten juristischen Personen des öffentlichen Rechts und Stiftungen bürgerlichen Rechts sind zur Zahlung von Gebühren verpflichtet.

#### § 4 Gebühren- und Auslagenfreiheit

Gebühren und Auslagen werden von der Landesvermessung und Geobasisinformation Brandenburg sowie den Katasterbehörden nicht erhoben für

1.  Amtshandlungen,
    1.  die von Amts wegen durchgeführt werden, soweit nicht durch Gesetz etwas anderes bestimmt ist,
    2.  die im Zuge der Zusammenarbeit der Landesvermessung und Geobasisinformation Brandenburg und der Katasterbehörde oder der Katasterbehörden untereinander anfallen,
2.  die Mitteilung an die Grundbuchämter bei den Amtsgerichten über die laufenden Veränderungen der Flurstücke,
3.  die Fortführung des Liegenschaftskatasters zur
    1.  Änderung der Landes-, Kreis-, Gemeinde-, Gemarkungs- oder Flurgrenzen,
    2.  Änderung der Angaben aus dem Grundbuch,
    3.  Änderung der flurstücksbeschreibenden Angaben durch Mitteilung der zuständigen Behörde,
    4.  Löschung der Darstellung von baulichen Anlagen, soweit dies ohne örtliche Vermessung möglich ist,oder
    5.  Berichtigung von fehlerhaften Daten im Liegenschaftskataster gemäß § 11 Absatz 3 des Brandenburgischen Vermessungsgesetzes,
    6.  Verschmelzung von Flurstücken,
4.  die Ausfertigung von Anlagen zum Antrag auf Vereinigung oder Teilung von Grundstücken,
5.  die Bereitstellung oder Ergänzung von Auszügen aus den Nachweisen des Liegenschaftskatasters für hoheitliche Liegenschaftsvermessungen (Vermessungsunterlagen), wenn die Auszüge im automatisierten Abrufverfahren nicht verfügbar sind.

#### § 5 Wertgebühr

(1) Ist eine Gebühr nach dem Wert des Bodens zu berechnen, so ist der Bodenrichtwert zu Grunde zu legen.

(2) Ist eine Gebühr nach dem Wert einer baulichen Anlage zu berechnen, so ist der Wert der fertigen baulichen Anlage, einschließlich der für die Gebäudefunktion notwendigen technischen Anlagen, zu Grunde zu legen.

(3) Die gebührenschuldende Person hat auf Verlangen den Wert des Bodens beziehungsweise einer baulichen Anlage nachzuweisen.
Wird der Nachweis nicht oder unzureichend erbracht, so schätzt die gebührenerhebende Behörde den Wert sachgerecht.

#### § 6 Zeitgebühr

(1) Soweit Gebühren nach dem Zeitaufwand zu berechnen sind, sind für jede außen- oder innendienstlich begonnene halbe Arbeitsstunde folgende Gebühren zu Grunde zu legen:

1.  für die Präsidentin oder den Präsidenten der Landesvermessung und Geobasisinformation Brandenburg 54,30 Euro,
2.  für die Leiterin oder den Leiter der Katasterbehörde gemäß § 27 Absatz 2 des Brandenburgischen Vermessungsgesetzes 54,30 Euro,
3.  für die Öffentlich bestellte Vermessungsingenieurin oder den Öffentlich bestellten Vermessungsingenieur 54,30 Euro,
4.  für eine vermessungstechnische Fachkraft 46,60 Euro oder
5.  für eine Hilfskraft 31,10 Euro.

(2) Der Zeitaufwand bestimmt sich nach der Arbeitszeit, die zur ordnungsgemäßen Aufgabenerfüllung erforderlich ist, einschließlich der notwendigen Reisezeiten.

#### § 7 Auslagen

(1) An Auslagen sind von der gebührenschuldenden Person zu erstatten:

1.  Aufwendungen für öffentliche Bekanntgaben, ortsübliche Bekanntmachungen (Offenlegungen) oder öffentliche Zustellungen,
2.  Aufwendungen, die in Verbindung mit der Amtshandlung für Auszüge oder Auskünfte an Dritte verauslagt wurden.

(2) Alle weiteren Auslagen, die mit der Amtshandlung notwendig werden, sind mit der Gebühr abgegolten.

(3) Wenn für eine Amtshandlung Gebührenfreiheit besteht, sind neben den in Absatz 1 auch die in § 9 des Gebührengesetzes für das Land Brandenburg aufgeführten Auslagen zu erstatten, soweit nichts anderes bestimmt ist.

#### § 8 Gebühren und Auslagen in besonderen Fällen

(1) Wurde mit der sachlichen Bearbeitung begonnen und kann sie aus Gründen, welche die Behörde nicht zu vertreten hat, nicht beendet werden, ist § 17 des Gebührengesetzes für das Land Brandenburg entsprechend anzuwenden.

(2) Wird eine vorzeitig beendete Amtshandlung auf erneuten Antrag oder nach Wegfall des Hindernisses fortgesetzt, so sind bereits erhobene Gebühren und Auslagen in der Höhe anzurechnen, in der sich der Aufwand durch die bereits erbrachten Teilleistungen verringert.

(3) Eine beantragte Amtshandlung, die während der Ausführung in eine andere Amtshandlung geändert wird, ist nur nach der geänderten Amtshandlung abzurechnen, wenn Teilleistungen, die bereits ausgeführt wurden, angerechnet werden können.

(4) Für Amtshandlungen, die keiner Tarifstelle zugeordnet werden können und die nicht ausschließlich im besonderen öffentlichen Interesse liegen, kann eine Gebühr bis zu einer Höhe von höchstens 2 500 Euro erhoben werden.

#### § 9 Allgemeine Festlegungen

(1) Infrastrukturanlagen sind Einrichtungen, die dem Straßen-, Schienen- oder Schiffsverkehr sowie der Versorgung beziehungsweise Entsorgung mit Wasser, Energie, Telekommunikation oder Ähnlichem dienen und von der Natur der Anlage her als Trasse geplant werden beziehungsweise ausgebaut sind.
Hierzu gehören auch die sie begleitenden Anlagen, wie sie in den entsprechenden Fachvorschriften aufgeführt werden.
Gewässer gemäß Anlage 1 des Bundeswasserstraßengesetzes und Gewässer gemäß der Brandenburgischen Gewässereinteilungsverordnung sowie Gräben, die der Be- und Entwässerung dienen, gehören ebenfalls zu den Infrastrukturanlagen nach Satz 1.

(2) Das Baufeld im Sinne dieser Verordnung umfasst das Flurstück, mehrere zusammenhängende Flurstücke oder Teile von Flurstücken, auf dem bauliche Anlagen errichtet werden sollen.
Belegt das Baufeld nur einen Teil eines Flurstücks, so bemisst sich die Größe des Baufeldes nach den gemäß § 7 Absatz 3 der Brandenburgischen Bauvorlagenverordnung auf diesem Flurstück darzustellenden Sachverhalten.
Die Flächen der Zuwegung und die Abstandsflächen, soweit sie nicht selbst auf dem Flurstück zu liegen kommen, sind nicht Teil des Baufeldes.
Für Windenergieanlagen ist bei der Ermittlung der Baufeldgröße nicht von der Abstandsfläche, sondern von der Kreisfläche der fiktiven Außenwand auszugehen.

(3) Beziehen sich gleichzeitig beantragte Amtshandlungen in einem Antrag auf ein oder mehrere Flurstücke oder mehrere bauliche Anlagen, gelten diese gebührenrechtlich als ein Antrag, wenn die beantragten Flurstücke oder die durch eine beantragte Einmessung baulicher Anlagen betroffenen Flurstücke mit ihren Grenzen aneinander liegen (räumlicher Zusammenhang) oder als ein Grundstück im Grundbuch gebucht sind.
Dies gilt auch dann, wenn dieser Antrag im laufenden Verfahren bis zum Abschluss der örtlichen Vermessung erweitert wird.
Liegenschaftsvermessungen an Infrastrukturanlagen sind hiervon ausgenommen.

#### § 10 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 1.
November 2019 in Kraft.
Gleichzeitig tritt die Vermessungsgebührenordnung vom 16.
September 2011 (GVBl.
II Nr.
55), die zuletzt durch die Verordnung vom 10.
Mai 2017 (GVBl.
II Nr.
28) geändert worden ist, außer Kraft.

Potsdam, den 14.
Oktober 2019

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter

* * *

Anlage  
(zu § 1 Absatz 1)

Gebührentarif

Allgemeine Regelung:

Die Verweise innerhalb des Gebührentarifs auf Tarifstellen beziehen immer die hierarchisch untergliederten Tarifstellen mit ein.

Tarifstelle

Gegenstand

1

Informationen und Bescheinigungen von Tatbeständen des Liegenschaftskatasters

2

Bereitstellung von Geobasisinformationen der Liegenschaften

3

Liegenschaftsvermessung

4

Amtshandlungen in Verbindung mit anderen Rechtsvorschriften

5

Aufsichtsbehörde über die Öffentlich bestellten Vermessungsingenieurinnen und die Öffentlich bestellten Vermessungsingenieure

6

Rechtsbehelfe

Tarifstelle  
(Tst.)

Gegenstand

Gebühr in  
Euro

**1**

**Informationen und Bescheinigungen von Tatbeständen des Liegenschaftskatasters**

 

1.1

Einsichtnahme, Auskunft und Bescheinigung

 

1.1.1

Die Gewährung der Einsichtnahme von mehr als einer Arbeitshalbstunde, mündliche Auskünfte von mehr als einer Arbeitshalbstunde sowie einfache schriftliche oder einfache elektronische Auskünfte, ab der zweiten begonnenen Arbeitshalbstunde

Zeitgebühr

1.1.2

Schriftliche oder elektronische Auskünfte sowie Bescheinigungen über festgestellte oder im Liegenschaftskataster nachgewiesene Tatbestände, soweit diese nicht durch Auszüge aus den Nachweisen des amtlichen Vermessungswesens belegt werden können und auch andere Tarifstellen nicht gelten, je begonnene Arbeitshalbstunde

Zeitgebühr

1.2

Unschädlichkeitszeugnis

 

 

Für die Entscheidung über den Antrag auf Erteilung eines Unschädlichkeitszeugnisses nach § 21 des Brandenburgischen Ausführungsgesetzes zum Bürgerlichen Gesetzbuch und die Erstausfertigung des Unschädlichkeitszeugnisses für jeden Berechtigten, je begonnene Arbeitshalbstunde

Zeitgebühr

1.3

Nichtbetroffenheitsbescheinigung

 

 

Für die Entscheidung über den Antrag auf Erteilung einer Bescheinigung nach den §§ 1025 und 1026 des Bürgerlichen Gesetzbuches und die Erstausfertigung der Bescheinigung, je begonnene Arbeitshalbstunde

Zeitgebühr

1.4

Kopien

 

 

Kopien von Urkunden, Abschriften, Ablichtungen od­er Plänen, die nicht zum Nachweis des aktuellen Liegenschaftskatasters gehören

 

1.4.1

bis DIN A3 je Seite

3,10

1.4.2

bis DIN A2 je Seite

6,20

1.4.3

bis DIN A1 je Seite

12,40

1.4.4

größer DIN A1 je Seite

24,80

1.4.5

bei gleichzeitiger Beantragung für eine zweite und jede weitere Kopie des selben Dokumentes, die in einem Arbeitsgang erstellt wird, je Seite

25 Prozent der  
Gebühr nach  
Tst.
1.4.1 bis  
Tst.
1.4.4

1.5

Mehrausfertigungen von Informationen und Bescheinigungen

 

 

Bei gleichzeitiger Beantragung von Mehrausfertigungen von Bescheinigungen, Unschädlichkeitszeugnissen, Kopien oder Ausfertigungen von Dokumenten oder Plänen nach den Tarifstellen 1.1 bis 1.3 je weitere Ausfertigung

6,20

**2**

**Bereitstellung von Geobasisinformationen der Liegenschaften**

 

2.1

Ausfertigung aktueller Geobasisinformationen

 

2.1.1

Auszug als Liegenschaftskarte bis DIN A3 je Auszug

22,00

2.1.2

Auszug als Liegenschaftskarte größer als DIN A3 bis einschließlich DIN A0  
je Auszug

44,00

2.1.3

Auszug als Flurstücksnachweis je Auszug

11,00

2.1.4

Auszug als Flurstücks- und Eigentümernachweis je Auszug

11,00

2.1.5

Auszug als Grundstücksnachweis je Auszug

11,00

2.1.6

Auszug als Bestandsnachweis je Auszug

22,00

2.2

Andere Unterlagen aus dem aktuellen Nachweis des Liegenschaftskatasters

 

2.2.1

bis DIN A3 je Seite

10,00

2.2.2

bis DIN A2 je Seite

20,00

2.2.3

bis DIN A1 je Seite

30,00

2.2.4

größer DIN A1 je Seite

40,00

2.3

Mehrausfertigungen von Geobasisinformationen der Liegenschaften

 

 

Bei gleichzeitiger Beantragung von Mehrausfertigungen nach den  
Tarifstellen 2.1 und 2.2, je Mehrausfertigung

25 Prozent der  
Gebühr nach  
Tst.
2.1 bis  
Tst.
2.2

**3**

**Liegenschaftsvermessung**

 

 

Allgemeine Regelung: Mit den Gebühren nach dieser Tarifstelle sind alle mit dem Antrag in Zusammenhang stehenden Tätigkeiten, soweit sie im Einzelfall anfallen, einschließlich der Erstellung der Vermessungsschriften sowie die Fortführung des Liegenschaftskatasters und die Bekanntgabe der Veränderungen abgegolten.

 

3.1

Liegenschaftsvermessung – Grenzvermessungen, soweit nicht Tarifstelle 3.2 oder 4.1 anzuwenden ist

 

 

Allgemeine Regelung:

1.  Für die Gebührenberechnung sind der Grundaufwand, die Längen der Grenzen sowie die neu eingebrachten und gewidmeten Grenzzeichen in Ansatz zu bringen.
    Bei der Bildung neuer Flurstücke ist zusätzlich auch die Anzahl der neuen Flurstücke zu berücksichtigen.
2.  Der Grundaufwand ist einmal je Antrag in Höhe von 983,30 Euro anzurechnen.
    Das gilt auch, wenn der Antrag verschiedene Tatbestände nach dieser Tarifstelle umfasst.
3.  Die Längen der beantragten Grenzen sowie bei der Bildung neuer Grenzen, die Längen der bestehenden Grenzen, in die neue Grenzen eingebunden werden, sind zu summieren.
    Grenzen, die lediglich zur Bestätigung von Punktidentitäten angemessen werden oder Grenzen, die im Zuge der Fortführung des Liegenschaftskatasters wegfallen, bleiben außer Betracht.
    Anzurechnende Grenzen können nur einmal je Antrag für die Gebührenberechnung angerechnet werden.
    Das Ergebnis ist auf den nächsten vollen Meter aufzurunden und mit der Gebühr des zutreffenden Bodenwertes zu multiplizieren.
    Beträgt die Summe der so ermittelten Grenzlängen weniger als 15 Meter, ist für die Berechnung der Gebühr eine Mindestgrenzlänge von 15 Metern anzusetzen.
    Die Länge der Grenze zwischen zwei direkt benachbarten Grenzpunkten ist mit maximal 500 Metern anrechenbar.
4.  Bestehende Grenzen, in die eine oder mehrere neue Grenzen eingebunden werden, sind mit mindestens 15 Metern und maximal 160 Metern anrechenbar.
5.  Beginnt oder endet eine neue Grenze in einem bestehenden Grenzpunkt, so ist eine fiktive Grenzlänge von 15 Metern anzurechnen, wenn an diesem Grenzpunkt keine Grenzlänge einer bestehenden Grenze in der Gebührenberechnung zu berücksichtigen ist.
6.  Die folgende Gebühr ist bei entsprechendem Bodenwert des Antragsflurstücks je Meter der ermittelten Grenzlänge anzuhalten:

> Bodenwert (Euro/m²)
> 
> Gebühr (Euro/Meter)
> 
> unter 3
> 
> 7,00
> 
> bis 30
> 
> 11,00
> 
> bis 100
> 
> 12,00
> 
> bis 200
> 
> 13,50
> 
> bis 400
> 
> 15,00
> 
> über 400
> 
> 16,50

7.  Wenn an einer Grenze mehr als ein Bodenwert anliegt, ist der Gebührenberechnung der höchste der betreffenden Bodenwerte zugrunde zu legen.
    Berührungen in nur einem Punkt bleiben außer Betracht.

 

3.1.1

Feststellung neuer und bestehender Grenzen mit örtlicher Vermessung

 

3.1.1.1

Die Gebühr berechnet sich nach den allgemeinen Regelungen Nummer 1 bis 7 der Tarifstelle 3.1 und soweit zutreffend, zuzüglich der Gebühr nach Tarifstelle 3.1.1.2 bis 3.1.1.5 je Antrag

100 Prozent  
der Gebühr

3.1.1.2

für das dritte neue Flurstück

103,50

3.1.1.3

ab dem vierten neuen Flurstück je weiteres neues Flurstück

310,50

3.1.1.4

für jedes eingebrachte und gewidmete Grenzzeichen

31,10

3.1.1.5

Für die Prüfung von mehr als einer Arbeitshalbstunde, ob durch eine die Teilung vorbereitende Liegenschaftsvermessung Zustände geschaffen werden, die den Vorschriften der Brandenburgischen Bauordnung widersprechen ab der zweiten begonnenen Arbeitshalbstunde

Zeitgebühr

3.1.2

Feststellung neuer Grenzen ohne örtliche Vermessungen (Sonderung)

 

3.1.2.1

Die Gebühr berechnet sich nach den allgemeinen Regelungen Nummer 1 bis 7 der Tarifstelle 3.1 je Antrag

55 Prozent  
der Gebühr

3.1.2.2

Soweit zutreffend zuzüglich der Gebühr nach Tarifstelle 3.1.2.1 für die Prüfung von mehr als einer Arbeitshalbstunde, ob durch eine die Teilung vorbereitende Liegenschaftsvermessung Zustände geschaffen werden, die den Vorschriften der Brandenburgischen Bauordnung widersprechen ab der zweiten begonnenen Arbeitshalbstunde

Zeitgebühr

3.1.2.3

Soweit zutreffend zuzüglich der Gebühr nach Tarifstelle 3.1.2.1 ab dem vierten neuen Flurstück je weiteres neues Flurstück

155,30

3.1.3

Fortführung des Liegenschaftskatasters aufgrund von Grenzfeststellungen

 

3.1.3.1

ohne Bildung neuer Flurstücke je Antrag

227,70

 

mit Bildung neuer Flurstücke:

 

3.1.3.2

bei einem Bodenwert unter 3 Euro je m² je neues Flurstück

155,30

3.1.3.3

bei einem Bodenwert bis 30 Euro je m² je neues Flurstück

227,70

3.1.3.4

bei einem Bodenwert bis 100 Euro je m² je neues Flurstück

238,10

3.1.3.5

bei einem Bodenwert bis 200 Euro je m² je neues Flurstück

258,80

3.1.3.6

bei einem Bodenwert bis 400 Euro je m² je neues Flurstück

269,10

3.1.3.7

bei einem Bodenwert über 400 Euro je m² je neues Flurstück

279,50

3.1.4

Grenzwiederherstellung mit Abmarkungen der Grenzpunkte

 

 

Allgemeine Regelung:  
  
Für die Gebührenberechnung der Abmarkung eines einzelnen Grenzpunktes ist die Länge einer anliegenden, wiederherzustellenden Grenze des Antragsflurstücks, wie sie im Antrag bezeichnet ist, mit mindestens 15 Metern und maximal 75 Metern anzurechnen.

 

3.1.4.1

Die Gebühr berechnet sich nach der allgemeinen Regelung dieser Tarifstelle sowie nach den allgemeinen Regelungen Nummer 1 bis 3 und 6 bis 7 der Tarifstelle 3.1 je Antrag

85 Prozent  
der Gebühr

3.1.4.2

zuzüglich zur Gebühr nach Tarifstelle 3.1.4.1 für jedes auf Antrag eingebrachte und gewidmete Grenzzeichen

31,10

3.1.4.3

Einleitung des Amtsverfahrens nach § 15 Absatz 2 Satz 3 des Brandenburgischen Vermessungsgesetzes je Einleitung des Amtsverfahrens

10 Prozent der  
Gebühr nach  
Tst.
3.1.4.1

3.1.5

Fortführung des Liegenschaftskatasters aufgrund von Grenzwiederherstellung mit Abmarkungen der Grenzpunkte, wenn keine Gebühr nach Tarifstelle 3.1.3 anzurechnen ist je Antrag

227,70

3.1.6

Grenzwiederherstellung mit Erteilung eines Grenzzeugnisses

 

 

Allgemeine Regelung:  
  
Mit der Gebühr sind bis zu drei Ausfertigungen abgegolten.

 

 

Die Gebühr berechnet sich nach den allgemeinen Regelungen Nummer 1 bis 3 und 6 bis 7 der Tarifstelle 3.1 je Antrag

55 Prozent  
der Gebühr

3.1.7

Fortführung des Liegenschaftskatasters aufgrund von Grenzwiederherstellung mit Erteilung eines Grenzzeugnisses je Antrag

gebührenfrei

3.1.8

Mehrausfertigungen der Fortführungsmitteilung oder des Grenzzeugnisses bei gleichzeitiger Beantragung nach den Tarifstellen 3.1.3, 3.1.5 oder des Grenzzeugnisses nach Tarifstelle 3.1.7 je weitere Mitteilung oder weiteres Grenzzeugnis

12,40

3.2

Liegenschaftsvermessung – Infrastrukturanlagen

 

 

Allgemeine Regelung:

1.  Für die Gebührenberechnung bei der Liegenschaftsvermessung von Infrastrukturanlagen sind die Anzahl der neuen Flurstücke und die beantragten Grenzlängen anzurechnen.
2.  Die Liegenschaftsvermessung von Infrastrukturanlagen wird nach Tarifstelle 3.1 abgerechnet, wenn die Infrastrukturanlage im Zusammenhang mit der Liegenschaftsvermessung für Bauplatz- oder Siedlungsbefassungen oder ähnlichen Erfassungen im nachbarschaftlichen Zusammenhang steht.
3.  Bei gleichzeitiger Liegenschaftsvermessung nebeneinander verlaufender Infrastrukturanlagen, die verschiedenen Kategorien angehören, sind die gemeinsamen Grenzen der jeweils höheren Kategorie zuzuordnen.
    Gleiches gilt für angrenzende neue Flurstücke, die in keiner Kategorie direkt eingebunden sind.
4.  Für die Gebührenberechnung sind die ermittelten Grenzlängen einmal innerhalb einer Kategorie zu addieren und gemäß den nachfolgend aufgeführten Bemessungsgrundlagen einzeln oder in jeweils zutreffender Kombination anzusetzen.
    Die jeweilige Summe der ermittelten Grenzlängen ist auf den nächsten vollen Meter aufzurunden.
    Neue Flurstücke sind nach der jeweiligen Kategorie der Anlage, in der sie gebildet werden, beziehungsweise für neue angrenzende Flurstücke, nach der Kategorie der Anlage, mit der sie die längste gemeinsame Grenze haben, anzusetzen.
5.  Anzurechnen sind:
    
    *   die Anzahl der neu entstehenden Flurstücke,
    *   die Länge der neuen Grenzen,
    *   die Länge der auf Antrag festzustellenden bestehenden Grenzen,
    *   die Länge der auf Antrag wiederherzustellenden Grenzen
    
    Die Summe der anzurechnenden Längen von Grenzen beträgt bei einer sachlich zusammengehörigen Liegenschaftsvermessung mindestens 100 Meter.  
      
    Lücken von über 100 Metern unterbrechen den sachlichen Zusammenhang.

6.  Die Infrastrukturanlage kreuzende oder eine von ihr abzweigende Infrastrukturanlage, die sich jeweils mit einer eigenen Länge von weniger als 100 Metern erstreckt, werden in den Grenzlängen berücksichtigt.
    Gleiches gilt für Flächen (zum Beispiel Regenrückhaltebecken, Aufforstungsgebiete oder ähnliche Flächen), sofern diese an die Infrastrukturanlage direkt angrenzen.

 

3.2.1

Einteilung der Infrastrukturanlagen

 

 

Kategorie I:  
Bundesautobahnen, Bundes- und Landesstraßen, die zwei oder mehr Richtungsfahrspuren in beide Richtungen haben, Eisenbahnhauptstrecken, Gewässer gemäß der Anlage 1 des Bundeswasserstraßengesetzes und Gewässer gemäß der Brandenburgischen Gewässereinteilungsverordnung

 

3.2.1.1

für jedes neue Flurstück

186,30

3.2.1.2

für jeden beantragten Meter Grenzlänge

20,70

3.2.2

Kategorie II:  
Bundes- und Landesstraßen, soweit sie nicht in Kategorie I genannt sind, Eisenbahnnebenstrecken, Gräben, die der Be- und Entwässerung dienen (Gewässer II.
Ordnung, ohne Meliorationsgräben) oder Infrastrukturanlagen, die der Ver- und Entsorgung mit Wasser, Energie oder Kommunikation dienen je Antrag

75 Prozent der  
Gebühr nach  
Tst.
3.2.1

3.2.3

Kategorie III:  
Kreisstraßen, Gemeindestraßen, sonstige Gleisanlagen je Antrag

65 Prozent der  
Gebühr nach  
Tst.
3.2.1

3.2.4

Kategorie IV:  
Sonstige öffentliche Straßen, Meliorationsgräben oder sonstige Infrastrukturanlagen, die nicht der Kategorie III zuzurechnen sind je Antrag

55 Prozent der  
Gebühr nach  
Tst.
3.2.1

3.2.5

Sonderung von Infrastrukturanlagen je Antrag

55 Prozent der  
Gebühr nach  
Tst.
3.2.1 bis 3.2.4

3.2.6

Fortführung des Liegenschaftskatasters aufgrund von Liegenschaftsvermessungen an Infrastrukturanlagen

 

3.2.6.1

Feststellung oder Wiederherstellung neuer oder bestehender Grenzen je Antrag

227,70

3.2.6.2

für jedes neue Flurstück

124,20

3.2.7

Mehrausfertigungen der Fortführungsmitteilung

 

 

Bei gleichzeitiger Beantragung von Mehrausfertigungen von Mitteilungen über die Ergebnisse der Fortführung nach der Tarifstelle 3.2.6,

 

3.2.7.1

erste Ausfertigung der Mitteilung für die antragstellende Person

gebührenfrei

3.2.7.2

je weitere ausgefertigte Mitteilung

12,40

3.3

Liegenschaftsvermessung – bauliche Anlagen

 

 

Allgemeine Regelung:

1.  Die Gebühr ist je Antrag für die bauliche Anlage oder die baulichen Anlagen festzusetzen.
    Für die Festsetzung der Gebühr ist der Wert der einzumessenden baulichen Anlage oder die Summe der Werte der gleichzeitig einzumessenden baulichen Anlagen anzusetzen.
    Bauliche Anlagen, die in einer oder mehreren Geschossebenen verbunden sind (zum Beispiel mit einer Tiefgarage), gelten als eine bauliche Anlage.
2.  Werden mehrere getrennt stehende bauliche Anlagen bis zu einem Wert von jeweils 100 000 Euro oder ab einem Wert von jeweils 100 000 Euro gleichzeitig eingemessen, wird eine zusätzliche Gebühr entsprechend Tarifstelle 3.3.1.9 je weiterer baulicher Anlage erhoben.

 

3.3.1

Einmessung von baulichen Anlagen

 

 

Bei einem Gesamtwert der baulichen Anlagen:

 

3.3.1.1

bis 20 000 Euro

414,00

3.3.1.2

über 20 000 Euro bis 100 000 Euro

621,00

3.3.1.3

über 100 000 Euro bis 300 000 Euro

776,30

3.3.1.4

über 300 000 Euro bis 600 000 Euro

931,50

3.3.1.5

über 600 000 Euro bis 800 000 Euro

1 190,30

3.3.1.6

über 800 000 Euro bis 1 000 000 Euro

1 552,50

3.3.1.7

über 1 000 000 Euro

1 552,50

 

zuzüglich für je weitere angefangene 500 000 Euro

517,50

3.3.1.8

über 4 000 000 Euro

4 657,50

 

zuzüglich für je weitere angefangene 500 000 Euro

103,50

3.3.1.9

bei gleichzeitiger Einmessung mehrerer getrennt stehender baulicher Anlagen deren Wert

 

 

jeweils bis 100 000 Euro beträgt, zusätzlich ab der zweiten und jeder weiteren baulichen Anlage bis 100 000 Euro Wert

103,50

 

jeweils über 100 000 Euro beträgt, zusätzlich ab der dritten und jeder weiteren baulichen Anlage über 100 000 Euro Wert

207,00

3.3.2

Einleitung des Amtsverfahrens nach § 23 Absatz 2 Satz 3 des Brandenburgischen Vermessungsgesetzes je Einleitung des Amtsverfahrens

25 Prozent der  
Gebühr nach  
Tst.
3.3.1

3.3.3

Fortführung des Liegenschaftskatasters aufgrund von Liegenschaftsvermessungen an baulichen Anlagen je Antrag

27 Prozent der  
Gebühr nach  
Tst.
3.3.1

3.3.4

Mehrausfertigungen der Mitteilung über die Fortführung je Mehrausfertigung

12,40

3.4

Sonstige vermessungstechnische Tätigkeiten die im Auftrag der Landesvermessung und Geobasisinformation Brandenburg oder einer Katasterbehörde ausgeführt werden.

 

3.4.1

Passpunktbestimmung je Passpunkt

310,50

3.4.2

Auswertung von Geobasisdaten des Liegenschaftskatasters je begonnene Arbeitshalbstunde

Zeitgebühr

3.4.3

Vermessungen zur Berichtigung fehlerhafter Daten des Liegenschaftskatasters oder zur Bereinigung von Mängeln in den Vermessungen und Vermessungsschriften, wenn diese Vermessungen aufgrund § 9 Absatz 8 des Brandenburgischen ÖbVI-Gesetzes nicht durch die Vermessungsstellen selbst auszuführen sind,

 

3.4.3.1

je Antrag

20 Prozent bis  
100 Prozent der  
Gebühr nach  
Tst.
3.1 bis  
Tst.
3.3

3.4.3.2

Fortführung des Liegenschaftskatasters zur Berichtigung fehlerhafter Daten des Liegenschaftskatasters oder zur Bereinigung von Mängeln in den Vermessungen und Vermessungsschriften je Antrag

gebührenfrei

3.4.4

Vermessungen, die im Zusammenhang mit § 10 Absatz 2 Satz 1 des Brandenburgischen ÖbVI-Gesetzes durchzuführen sind

 

3.4.4.1

Einleitung des Amtsverfahrens nach § 10 Absatz 2 Satz 1 Brandenburgisches ÖbVI-Gesetz je Einleitung des Amtsverfahrens

20 Prozent der  
Gebühr nach  
Tst.
3.1 bis  
Tst.
3.3

3.4.4.2

je Antrag

20 Prozent bis  
100 Prozent der  
Gebühr nach  
Tst.
3.1 bis  
Tst.
3.3

3.4.4.3

Fortführung des Liegenschaftskatasters je Antrag

Gebühr nach  
Tst.
3.1 bis  
Tst.
3.3

**4**

**Amtshandlungen in Verbindung mit anderen Rechtsvorschriften**

 

4.1

Bodenordnungsverfahren

 

4.1.1

Verfahren nach dem Flurbereinigungsgesetz und nach dem Landwirtschafts-  
anpassungsgesetz

 

 

Allgemeine Regelung:

1.  Die Tarifstelle ist nur für Amtshandlungen in Verfahren nach dem Flurbereinigungsgesetz und dem Landwirtschaftsanpassungsgesetz anzuwenden.
2.  Die Vermessung zur Festlegung der Verfahrensgrenze umfasst im erforderlichen Umfang die Feststellung beziehungsweise Wiederherstellung bestehender Flurstücksgrenzen, die Bildung neuer Flurstücksgrenzen, die Errichtung fester Grenzzeichen (Abmarkung) und die Aufnahme der  
    Anerkennungserklärungen der Beteiligten.
3.  Für Liegenschaftsvermessungen auf Antrag vor Rechtskraft des Flurbereinigungsplans oder in Verfahren nach Abschnitt 8 des Landwirtschaftsanpassungsgesetzes ist die Tarifstelle Nummer 3.1 anzuwenden.
    Eine Kombination dieser Tarifstellen (Nummer 3.1.1, 3.1.2 und 3.1.4) ist möglich.

 

4.1.1.1

Vermessung der Verfahrensgrenze je angefangene 100 Meter

828,00

4.1.1.2

für jedes neue Flurstück

186,30

4.1.1.3

Passpunktbestimmung je Passpunkt

310,50

4.1.2

Umlegungsverfahren nach dem Baugesetzbuch

 

 

Allgemeine Regelung:  
  
Die Liegenschaftsvermessungen zur Festlegung der Verfahrensgrenze sowie die Fortführung des Liegenschaftskatasters sind nach der Tarifstelle Nummer 3.1 abzurechnen.
Eine Kombination der Vermessung nach den Tarifstellen 3.1.1, 3.1.2 und 3.1.4 ist innerhalb eines Antrags möglich.

 

 

Vermessungen zur Übertragung der neuen Grenzen in die Örtlichkeit und Abmarkung der neuen Grenzpunkte vor Unanfechtbarkeit des Umlegungsplans oder des Beschlusses über die vereinfachte Umlegung je begonnene Arbeitshalbstunde

Zeitgebühr

4.1.3

Berichtigung des Liegenschaftskatasters durch den Umlegungsplan oder den Umlegungsbeschluss über die vereinfachte Umlegung

 

4.1.3.1

bei einem Bodenwert bis 30 Euro je m² je neues Flurstück

227,70

4.1.3.2

bei einem Bodenwert bis 100 Euro je m² je neues Flurstück

238,10

4.1.3.3

bei einem Bodenwert bis 200 Euro je m² je neues Flurstück

258,80

4.1.3.4

bei einem Bodenwert bis 400 Euro je m² je neues Flurstück

269,10

4.1.3.5

bei einem Bodenwert über 400 Euro je m² je neues Flurstück

279,50

4.1.4

Mehrausfertigungen der Mitteilung über die Berichtigung je Ausfertigung

12,40

4.2

Feststellungen von Tatbeständen an Grund und Boden für vermessungstechnische Amtshandlungen nach dem Brandenburgischen Bauordnungsrecht

 

4.2.1

Amtlicher Lageplan

 

 

Allgemeine Regelung:

1.  Nach dieser Tarifstelle sind amtliche Lagepläne gemäß der Brandenburgischen Bauvorlagenverordnung abzurechnen.
    Die Fläche des Baufeldes ist auf den nächsten vollen Quadratmeter aufzurunden.
2.  Mit der Gebühr sind bis zu drei Ausfertigungen des amtlichen Lageplans abgegolten.
3.  Die Gebühr ist für jedes Baufeld, das zur Beurteilung des Bauvorhabens notwendig ist, einzeln festzusetzen.
    Aneinandergrenzende bauliche Anlagen stehen auf einem gemeinsamen Baufeld.

 

4.2.1.1

Gebühr für die Erstellung eines amtlichen Lageplans bis zu einer Größe des Baufeldes von 1 000 m²  

1 242,00

4.2.1.2

über 1 000 m² bis 2 000 m²

1 242,00

 

zuzüglich je weitere angefangene 100 m²

103,50

4.2.1.3

über 2 000 m² bis 4 000 m²

2 277,00

 

zuzüglich je weitere angefangene 250 m²

103,50

4.2.1.4

über 4 000 m² bis 10 000 m² 

3 105,00

 

zuzüglich je weitere angefangene 500 m²

103,50

4.2.1.5

über 10 000 m² bis 100 000 m²

4 347,00

 

zuzüglich je weitere angefangene 5 000 m²

621,00

4.2.1.6

über 100 000 m²    

15 525,00

 

zuzüglich je weitere angefangene 10 000 m²

517,50

4.2.2

Gebühr für die Erstellung eines amtlichen Lageplans in besonderen Fällen

 

4.2.2.1

bei zuverlässig nachgewiesenen Grundstücksgrenzen und baulichen Anlagen gemäß § 7 Absatz 4 der Brandenburgischen Bauvorlagenverordnung je amtlicher Lageplan

80 Prozent der  
Gebühr nach  
Tst.
4.2.1

4.2.2.2

im Außenbereich (§ 35 BauGB), wenn keine vorhandenen baulichen Anlagen darzustellen oder diese bereits im Liegenschaftskataster entsprechend der Brandenburgischen Bauvorlagenverordnung zuverlässig nachgewiesen sind je amtlicher Lageplan

80 Prozent der  
Gebühr nach  
Tst.
4.2.1

4.2.3

Gebühr für die Erstellung eines amtlichen Lageplans für untergeordnete Anbauten oder untergeordnete Nebengebäude mit einer maximalen Bruttogrundfläche von 50 m² je amtlicher Lageplan

50 Prozent der  
Gebühr nach  
Tst.
4.2.1 oder  
Tst.
4.2.2,  
mindestens 621,00

4.2.4

Aktualisierung eines amtlichen Lageplans  
  
Die Gebühr für die Erstellung eines amtlichen Lageplans auf der Grundlage eines von der Vermessungsstelle für dasselbe Erfassungsgebiet bereits erstellten amtlichen Lageplans, sofern dieser nicht älter als 6 Jahre ist je amtlicher Lageplan

55 Prozent der  
Gebühr nach  
Tst.
4.2.1 oder  
Tst.
4.2.2 oder  
Tst.
4.2.3,  
mindestens 400,00

4.2.5

Bei Beantragung von mehr als drei Ausfertigungen des amtlichen Lageplans nach den Tarifstellen 4.2.1, 4.2.2, 4.2.3 oder 4.2.4 je weitere Ausfertigung

18,60

4.3

Grundflächen- und Höhennachweis

 

 

Allgemeine Regelung:  
  
Die Tarifstelle ist nur anzuwenden, wenn die Vermessungstätigkeiten zum Grundflächen- und Höhennachweis gemäß § 72 Absatz 9 der Brandenburgischen Bauordnung zeitgleich mit der Liegenschaftsvermessung nach Tarifstelle 3.3 ausgeführt werden und die Bescheinigung aufgrund dieses Vermessungsergebnisses erteilt wird.

 

 

Je Bescheinigung des Grundflächen- und Höhennachweises

10 Prozent der Gebühr nach Tst.
3.3.1

4.4

Beglaubigung von Unterschriften nach § 84 Absatz 2 der Brandenburgischen Bauordnung

 

 

Allgemeine Regelung:  
  
Mit der Gebühr ist die Beglaubigung einer oder mehrerer verschiedener Unterschriften abgegolten, wenn diese in einem einzigen Vermerk erfolgt.

 

 

Je Beglaubigung

31,10

4.5

Bescheinigungen nach anderen Rechtsgebieten

 

 

Je begonnene Arbeitshalbstunde

Zeitgebühr

**5**

**Aufsichtsbehörde über die Öffentlich bestellten Vermessungsingenieurinnen und die Öffentlich bestellten Vermessungsingenieure**

 

5.1

Entscheidung über den Antrag auf Zulassung zur Öffentlich bestellten Vermessungsingenieurin oder zum Öffentlich bestellten Vermessungsingenieur nach § 2 Absatz 1 Satz 1 des Brandenburgischen ÖbVI-Gesetzes je Antrag

1 293,80

5.2

Erteilung einer Erlaubnis zum beruflichen Zusammenschluss nach § 6 Absatz 3 Satz 1 des Brandenburgischen ÖbVI-Gesetzes je Erlaubnis

310,50

**6**

**Rechtsbehelfe**

 

 

Zurückweisung oder Teilzurückweisung von Drittwidersprüchen je Widerspruch

10,00 bis 500,00