## Verordnung über den nachträglichen Erwerb von Lehrbefähigungen und Lehramtsbefähigungen (Befähigungserwerbsverordnung - BEV)

Auf Grund des § 8 Absatz 5 und des § 11 Absatz 3 des Brandenburgischen Lehrerbildungsgesetzes vom 18.
Dezember 2012 (GVBl.
I Nr. 45), die durch das Gesetz vom 31.
Mai 2018 (GVBl. I Nr. 10) geändert worden sind, verordnet die Ministerin für Bildung, Jugend und Sport im Einvernehmen mit dem Minister des Innern und für Kommunales, der Ministerin der Finanzen und für Europa und der Ministerin für Wissenschaft, Forschung und Kultur:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeine Bestimmungen

[§ 1 Geltungsbereich](#1)

[§ 2 Allgemeine Voraussetzungen](#2)

[§ 3 Feststellung, Anerkennung](#3)

### Abschnitt 2  
Zertifikatsstudium

[§ 4 Strukturelle Anforderungen](#4)

[§ 5 Inhaltliche Anforderungen](#5)

[§ 6 Hochschulzertifikat](#6)

### Abschnitt 3  
Erwerb einer Lehrbefähigung in einem Fach

[§ 7 Voraussetzung, Anrechnung](#7)

[§ 8 Allgemeinbildende Fächer](#8)

[§ 9 Sonderpädagogische Fachrichtungen](#9)

[§ 10 Berufliche Fächer](#10)

[§ 11 Inklusionspädagogische Schwerpunktbildung](#11)

### Abschnitt 4  
Erwerb der Befähigung für ein weiteres Lehramt oder für ein Lehreramt

[§ 12 Voraussetzungen, Anrechnung](#12)

[§ 13 Lehramt für die Primarstufe](#13)

[§ 14 Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer)](#14)

[§ 15 Lehramt für die Sekundarstufe II (berufliche Fächer)](#15)

[§ 16 Lehramt für Förderpädagogik](#16)

[§ 17 Lehreramt](#17)

### Abschnitt 5  
Besondere Staatsprüfung

[§ 18 Grundsätze und Ziel der besonderen Staatsprüfung](#18)

[§ 19 Zulassungsvoraussetzungen](#19)

[§ 20 Prüfungsleistungen](#20)

[§ 21 Zulassungsantrag](#21)

[§ 22 Entscheidung über die Zulassung](#22)

[§ 23 Prüfungsausschüsse](#23)

[§ 24 Notenbildung und Gesamtnote der besonderen Staatsprüfung](#24)

[§ 25 Wiederholung der Prüfung](#25)

[§ 26 Zeugnis und Bescheinigungen](#26)

### Abschnitt 6  
Schlussbestimmungen

[§ 27 Übergangsregelung](#27)

[§ 28 Inkrafttreten, Außerkrafttreten](#28)

### Abschnitt 1  
Allgemeine Bestimmungen

#### § 1 Geltungsbereich

Diese Verordnung gilt für den nachträglichen Erwerb

1.  der Lehrbefähigung für ein weiteres Fach oder einen weiteren Lernbereich oder einer weiteren Fachrichtung oder weiterer Fächer,
2.  der Befähigung für ein oder ein weiteres Lehramt gemäß § 2 Absatz 1 des Brandenburgischen Lehrerbildungsgesetzes (Lehramtsbefähigung) oder
3.  der Befähigung für ein Amt der Lehrerin oder des Lehrers der Besoldungsgruppen A 13 kw der Anlage 1 des Brandenburgischen Besoldungsgesetzes (Lehreramt), für das eine Ergänzungsprüfung vorgesehen ist.

Sie bestimmt das Nähere zu den Voraussetzungen, zum Umfang und der Art sowie zur Anerkennung der für den Befähigungserwerb nachzuweisenden Studien- und Prüfungsleistungen sowie zur Feststellung der Befähigung.

#### § 2 Allgemeine Voraussetzungen

(1) Der nachträgliche Erwerb einer Befähigung setzt voraus, dass

1.  eine Lehramtsbefähigung, die Zugangsvoraussetzungen für den Vorbereitungsdienst für ein Lehramt oder eine Lehrbefähigung nach dem Recht der Deutschen Demokratischen Republik und
2.  die für die angestrebte Befähigung erforderlichen Studien- und Prüfungsleistungen in mindestens einem Fach, einer Fachrichtung oder einem Lernbereich (Fach)

nach Maßgabe der Abschnitte 2 bis 4 nachgewiesen werden.

(2) An die Stelle der nachzuweisenden Studien- und Prüfungsleistungen in einem Fach können Studien- und Prüfungsleistungen im Studienbereich Inklusionspädagogik treten.

(3) Die erforderlichen Studien- und Prüfungsleistungen gemäß Absatz 1 Nummer 2 werden mit dem erfolgreichen Abschluss eines lehramtsbezogenen Zertifikatsstudiums, das die strukturellen und inhaltlichen Anforderungen gemäß den §§ 4 und 5 erfüllt, nachgewiesen.

(4) An die Stelle der für den Erwerb der Befähigung für ein Lehreramt abzulegenden Ergänzungsprüfung tritt die Feststellung gemäß § 3 Absatz 1.

#### § 3 Feststellung, Anerkennung

(1) Das nach dem Brandenburgischen Lehrerbildungsgesetz für die Anerkennung von lehrerbildenden Abschlüssen zuständige Ministerium stellt auf Antrag fest, ob die angestrebte Befähigung erworben wurde.

(2) Dem Antrag auf Feststellung gemäß Absatz 1 sind

1.  der Nachweis über die Befähigung, die für den Erwerb der angestrebten Befähigung gemäß den Bestimmungen dieser Verordnung vorausgesetzt wird,
2.  das Hochschulzertifikat gemäß § 6 und
3.  bei Erwerb einer Befähigung für ein oder ein weiteres Lehramt oder für ein Lehreramt ein Nachweis über ein Beschäftigungsverhältnis im Schul- oder Schulaufsichtsdienst des Landes Brandenburg oder an Ersatzschulen im Land Brandenburg

beizufügen.

(3) Soweit die für den nachträglichen Befähigungserwerb erforderlichen Studien- und Prüfungsleistungen nicht in einem Zertifikatsstudium gemäß Abschnitt 2 erworben wurden, sind diese anzuerkennen, wenn das jeweilige Studium den Anforderungen gemäß dieser Verordnung im Wesentlichen entspricht.

### Abschnitt 2  
Zertifikatsstudium

#### § 4 Strukturelle Anforderungen

(1) Das Zertifikatsstudium ist an einer Hochschule zu absolvieren.

(2) Die Zertifikatsstudienangebote sind zu akkreditieren und zu reakkreditieren.
Hierfür gelten die Bestimmungen für die Akkreditierung und Reakkreditierung lehramtsbezogener Masterstudiengänge gemäß § 3 der Lehramtsstudienverordnung entsprechend.
Soweit eine Akkreditierung oder Reakkreditierung der Zertifikatsstudienangebote aus hochschulrechtlichen Gründen nicht möglich ist, sind abweichend von Satz 1 die Studien- und Prüfungsordnungen für die Zertifikatsstudienangebote durch das für Schule zuständige Ministerium zu genehmigen.
In diesem Fall sind zur Sicherung und Entwicklung der Lehrqualität die Studienangebote regelmäßig zu evaluieren.
Das für Schule zuständige Ministerium kann die Vorlage der Evaluationsergebnisse verlangen.

(3) Die Zertifikatsstudienangebote sind zu modularisieren.
Jedes Modul ist mit einer Modulabschlussprüfung, die eine benotete Prüfungsleistung umfasst, abzuschließen.
In begründeten Ausnahmefällen, insbesondere wenn die inhaltlichen Anforderungen oder der Umfang eines Moduls es erfordern, kann die Modulabschlussprüfung aus benoteten Teilprüfungsleistungen bestehen.
Soweit ein Modul überwiegend praktische Studien umfasst, kann anstelle der Benotung der zu erbringenden Prüfungsleistung die Feststellung erfolgen, ob sie bestanden oder nicht bestanden wurde.

(4) Die Prüfungsanforderungen und -formen sind an den in den Modulen jeweils zu erreichenden Kompetenzen auszurichten, wobei die unterschiedlichen Prüfungsformen in einem ausgewogenen Verhältnis zueinanderstehen sollen.

(5) Den in den Modulen nachzuweisenden Studien- und Prüfungsleistungen sind Leistungspunkte nach dem European Credit Transfer System (ECTS) zuzuordnen.

(6) Ein Zertifikatsstudium gilt als erfolgreich abgeschlossen, wenn mindestens die Gesamtnote „ausreichend“ nachgewiesen wird.

(7) Im Übrigen gelten für die Durchführung eines Zertifikatsstudiums und für den Nachweis der zu erbringenden Studien- und Prüfungsleistungen die auf der Grundlage des Brandenburgischen Hochschulgesetzes erlassenen Rechtsverordnungen und Hochschulordnungen, insbesondere im Hinblick auf die Bestimmungen zur Modularisierung und Vergabe von Leistungspunkten, zur Anerkennung von Modulen und Leistungen, zur Leistungsbewertung und zu den Prüfungsverfahren entsprechend, soweit in dieser Verordnung nichts anders bestimmt wird.

(8) Abweichend von Absatz 1 kann ein Zertifikatsstudium auch an Einrichtungen der Lehrerfort- und -weiterbildung absolviert werden, wenn

1.  die in ihm gestellten wissenschaftlichen oder künstlerischen Anforderungen zu denen eines entsprechenden Hochschulstudienangebots gleichwertig sind und
2.  von der Einrichtung dargelegt wird, dass die mit der Lehre und Prüfung beauftragten Personen eine entsprechende wissenschaftliche oder künstlerische Qualifikation nachweisen.

Absatz 2 Satz 2 bis 4 gilt entsprechend.

#### § 5 Inhaltliche Anforderungen

(1) Das Zertifikatsstudium umfasst fachwissenschaftliche oder künstlerische und fachdidaktische Studien.
In den fachdidaktischen Studien sollen Aspekte der inklusiven Bildung in einem angemessenen Umfang berücksichtigt werden.

(2) Bezieht sich ein Zertifikatsstudium auf eine Fremdsprache, sollen in ihm sprachpraktische Studien vorgesehen werden.
Bezieht sich ein Zertifikatsstudium auf ein Fach aus dem musisch-ästhetischen Bereich oder das Fach Sport, sollen in ihm fachpraktische Studien vorgesehen werden.

(3) Der Gesamtumfang der in einem Zertifikatsstudium nachzuweisenden Studien- und Prüfungsleistungen richtet sich nach den Bestimmungen in den Abschnitten 3 und 4.

(4) Die für ein Zertifikatsstudium zugelassenen Fächer richten sich nach den Bestimmungen in den Abschnitten 3 und 4.
Das für Schule zuständige Ministerium kann abweichend davon jeweils andere Fächer zulassen.
Die Sätze 1 und 2 gelten auch für Fächer und Fachrichtungen, die als Voraussetzungen für den Erwerb eines Lehramtes nach dieser Verordnung gefordert sind.

(5) Für die curriculare Gestaltung eines Zertifikatsstudiums sind die von der Ständigen Konferenz der Kultusminister der Länder in der Bundesrepublik Deutschland beschlossenen ländergemeinsamen inhaltlichen Anforderungen für die Fachwissenschaften und die Fachdidaktiken für das jeweilige Fach zugrunde zu legen.
Bei der Auswahl der curricularen Schwerpunkte sind insbesondere die Anforderungen für die jeweils angestrebte Befähigung und die bereits erworbene Befähigung zu berücksichtigen.
Soweit für einzelne Fächer keine curricularen Vorgaben gemäß Satz 1 vorliegen, werden diese vom für Schule zuständigen Ministerium durch Verwaltungsvorschriften festgelegt.

#### § 6 Hochschulzertifikat

Nach Abschluss des Zertifikatsstudiums fertigt die Hochschule oder die Einrichtung gemäß § 4 Absatz 8 Satz 1 ein Zertifikat aus, in dem

1.  die Bezeichnung des Zertifikatsstudiums,
2.  die Bezeichnung der absolvierten Module und der ihnen jeweils zugeordneten Leistungspunkte,
3.  die Gesamtnote und
4.  Aussagen zur Akkreditierung beziehungsweise Reakkreditierung des Zertifikatsstudiums

auszuweisen sind.

### Abschnitt 3  
Erwerb einer Lehrbefähigung in einem Fach

#### § 7 Voraussetzung, Anrechnung

(1) Soweit nachstehend keine anderen Festlegungen getroffen werden, kann eine Lehrbefähigung in einem weiteren Fach erwerben, wer eine Lehramtsbefähigung, die Zugangsvoraussetzungen für den Vorbereitungsdienst für ein Lehramt oder eine Lehrbefähigung nach dem Recht der Deutschen Demokratischen Republik nachweist.

(2) Werden fachbezogene Studien- und Prüfungsleistungen, die im Wesentlichen den Anforderungen dieser Verordnung entsprechen, nachgewiesen, sind diese auf den für das jeweilige Fach nachzuweisenden Gesamtstudienumfang anzurechnen.

#### § 8 Allgemeinbildende Fächer

(1) Für den Erwerb der Lehrbefähigung für ein weiteres allgemeinbildendes Fach der Primarstufe sind die Fächer Deutsch, Englisch, Französisch, Kunst, Mathematik, Musik, Polnisch, Russisch, Sachunterricht mit den Bezugsfächern Gesellschaftswissenschaften oder Naturwissenschaften, Sorbisch/Wendisch, Spanisch und Sport zugelassen und Studien- und Prüfungsleistungen im Umfang von jeweils mindestens 30 Leistungspunkten nachzuweisen.
Für das Fach Sachunterricht gelten die §§ 8 Absatz 2 und 10 Absatz 2 der Lehramtsstudienverordnung entsprechend.

(2) Für den Erwerb der Lehrbefähigung für ein weiteres allgemeinbildendes Fach der Sekundarstufen I und II sind die Fächer

1.  Biologie, Chemie, Deutsch, Englisch, Französisch, Geografie, Geschichte, Gesellschaftswissenschaften, Informatik, Kunst, Latein, Lebensgestaltung-Ethik-Religionskunde, Mathematik, Musik, Naturwissenschaften, Physik, Politische Bildung, Polnisch, Russisch, Sorbisch/Wendisch, Spanisch, Sport, Technik und Wirtschaft-Arbeit-Technik sowie
2.  Astronomie, Darstellendes Spiel, Darstellen und Gestalten, Italienisch, Pädagogik, Philosophie, Psychologie, Recht und Wirtschaftswissenschaften

zugelassen und jeweils Studien- und Prüfungsleistungen im Umfang von mindestens 45 Leistungspunkten nachzuweisen.
Soweit sich die Lehrbefähigung schwerpunktmäßig auf die Sekundarstufe II beziehen soll, sind davon mindestens 15 Leistungspunkte für die fachwissenschaftliche Vertiefung im jeweiligen Fach nachzuweisen.

#### § 9 Sonderpädagogische Fachrichtungen

Für den Erwerb der Lehrbefähigung für eine oder eine weitere Fachrichtung, die einem sonderpädagogischen Förderschwerpunkt zugeordnet ist (sonderpädagogische Fachrichtung), sind die Fachrichtungen für die sonderpädagogischen Förderschwerpunkte Sehen, Hören, Sprache, geistige Entwicklung, körperliche und motorische Entwicklung, Lernen sowie emotionale und soziale Entwicklung zugelassen und Studien- und Prüfungsleistungen im Umfang von jeweils mindestens 27 Leistungspunkten nachzuweisen.

#### § 10 Berufliche Fächer

Für den Erwerb der Lehrbefähigung für ein oder ein weiteres berufliches Fach der Sekundarstufe II sind die Fächer Agrarwirtschaft, Bautechnik, Biotechnik, Druck- und Medientechnik, Elektrotechnik, Ernährung und Hauswirtschaft, Fahrzeugtechnik, Farbtechnik/Raumgestaltung/Oberflächentechnik, Gesundheit und Körperpflege, Holztechnik, Informations- und Kommunikationstechnik, Labortechnik/Prozesstechnik, Mediendesign und Designtechnik, Metalltechnik, Pflege, Sozialpädagogik, Textil- und Bekleidungstechnik, Vermessungstechnik sowie Wirtschaft und Verwaltung zugelassen und Studien- und Prüfungsleistungen im Umfang von jeweils mindestens 45 Leistungspunkten nachzuweisen.

#### § 11 Inklusionspädagogische Schwerpunktbildung

Für eine inklusionspädagogische Schwerpunktbildung in einem Lehramt gemäß § 2 Absatz 1 Nummer 1 bis 3 des Brandenburgischen Lehrerbildungsgesetzes sind Studien- und Prüfungsleistungen in

1.  der allgemeinen Inklusionspädagogik und -didaktik und
2.  den inklusionspädagogisch orientierten Studien in den sonderpädagogischen Fachrichtungen Lernen, Sprache und emotionale und soziale Entwicklung

im Umfang von mindestens insgesamt 50 Leistungspunkten nachzuweisen.
Dabei umfassen die Studien- und Prüfungsleistungen für den Teilbereich gemäß Satz 1 Nummer 1 mindestens 15 Leistungspunkte.

### Abschnitt 4  
Erwerb der Befähigung für ein weiteres Lehramt oder für ein Lehreramt

#### § 12 Voraussetzungen, Anrechnung

(1) Wer sich im Schuldienst oder Schulaufsichtsdienst des Landes Brandenburg befindet oder als Lehrkraft an einer Ersatzschule im Land Brandenburg tätig ist und die im Land Brandenburg geltenden laufbahnrechtlichen Zugangsvoraussetzungen erfüllt, kann bei Nachweis einer

1.  Befähigung für ein Lehramt eine weitere Lehramtsbefähigung oder
2.  Lehrbefähigung nach dem Recht der Deutschen Demokratischen Republik eine Befähigung für ein Lehreramt

erwerben.

(2) Werden fachbezogene Studien- und Prüfungsleistungen, die im Wesentlichen den Anforderungen dieser Verordnung entsprechen, nachgewiesen, sind diese auf den jeweils nachzuweisenden Gesamtstudienumfang anzurechnen.
Für den gemäß § 15 nachzuweisenden Gesamtstudienumfang gilt dies auch für Studien- und Prüfungsleistungen, die an Fachhochschulen erworben wurden.
Beträgt die Differenz weniger als 16 Leistungspunkte, sind keine weiteren Studien- und Prüfungsleistungen für den Erwerb der angestrebten Befähigung nachzuweisen.

#### § 13 Lehramt für die Primarstufe

(1) Die Befähigung für das Lehramt für die Primarstufe kann erwerben, wer die Befähigung für das

1.  Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer) mit zwei der Fächer Biologie, Chemie, Deutsch, Englisch, Französisch, Geografie, Geschichte, Kunst, Lebensgestaltung-Ethik-Religionskunde, Mathematik, Musik, Physik, Politische Bildung, Polnisch, Russisch, Sorbisch/Wendisch, Spanisch, Sport oder Wirtschaft-Arbeit-Technik sowie Studien- und Prüfungsleistungen in den Teilbereichen der Grundschulbildung
    1.  Grundschulpädagogik und -didaktik und
    2.  fachwissenschaftliche und -didaktische Grundlagen für den Unterricht in den Fächern Deutsch, Mathematik und Englisch (einschließlich sprachliche Kompetenzentwicklung) in der Schuleingangsphaseim Umfang von mindestens 60 Leistungspunkten

oder

2.  Lehramt für die Sekundarstufe II (berufliche Fächer) mit einem der Fächer Deutsch, Englisch, Französisch, Kunst, Lebensgestaltung-Ethik-Religionskunde, Mathematik, Musik, Polnisch, Russisch, Sorbisch/Wendisch, Spanisch, Sport oder Wirtschaft-Arbeit-Technik sowie Studien- und Prüfungsleistungen in den Teilbereichen der Grundschulbildung
    1.  Grundschulpädagogik und -didaktik und
    2.  fachwissenschaftliche und -didaktische Grundlagen für den Unterricht in den Fächern Deutsch, Mathematik und Englisch (einschließlich sprachliche Kompetenzentwicklung) in der Schuleingangsphaseim Umfang von mindestens 60 Leistungspunkten

oder

3.  Lehramt für Förderpädagogik mit einem Fach gemäß Nummer 1 sowie Studien- und Prüfungsleistungen
    1.  in einem weiteren Fach gemäß § 8 Absatz 1 Satz 1 im Umfang von mindestens 30 Leistungspunkten und
    2.  im Teilbereich der Grundschulbildung gemäß Nummer 1 Buchstabe a im Umfang von mindestens 15 Leistungspunkten

nachweist.

(2) Soll beim Erwerb der Lehramtsbefähigung für das Lehramt für die Primarstufe eine inklusionspädagogische Schwerpunktbildung erfolgen, tritt an die Stelle der Grundschulbildung die Inklusionspädagogik, in der Studien- und Prüfungsleistungen in den Teilbereichen

1.  allgemeine Inklusionspädagogik und -didaktik und
2.  inklusionspädagogisch orientierte Studien der Fachrichtungen für die Förderschwerpunkte Lernen, Sprache sowie emotionale und soziale Entwicklung

im Umfang von 60 Leistungspunkten sowie fachwissenschaftliche Studien- und Prüfungsleistungen für das Fach Deutsch und das Fach Mathematik im Umfang von jeweils 30 Leistungspunkten nachzuweisen sind, sofern sie nicht gemäß Absatz 1 enthalten sind.

#### § 14 Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer)

(1) Die Befähigung für das Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer) mit einer Schwerpunktbildung auf die Sekundarstufe I kann erwerben, wer die Befähigung für das

1.  Lehramt für die Primarstufe,
2.  Lehramt für die Sekundarstufe II (berufliche Fächer) oder
3.  Lehramt für Förderpädagogik

sowie Studien- und Prüfungsleistungen in zwei Fächern gemäß § 8 Absatz 2 im Umfang von mindestens 45 Leistungspunkten nachweist.

(2) Die Befähigung für das Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer) mit einer Schwerpunktbildung auf die Sekundarstufe II kann erwerben, wer die Befähigung für das

1.  Lehramt für die Primarstufe,
2.  Lehramt für die Bildungsgänge der Sekundarstufe I und der Primarstufe an allgemeinbildenden Schulen,
3.  Lehramt für die Sekundarstufe II (berufliche Fächer) oder
4.  Lehramt für Förderpädagogik

sowie Studien- und Prüfungsleistungen in zwei Fächern gemäß § 8 Absatz 2 im Umfang von mindestens jeweils 45 Leistungspunkten nachweist.
Dabei sind in jedem Fach mindestens 15 Leistungspunkte für vertiefte fachwissenschaftliche Studien nachzuweisen.
§ 12 Absatz 2 Satz 3 findet keine Anwendung.

(3) Die Befähigung gemäß Absatz 2 kann auch erwerben, wer die Befähigung für das Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer) mit einer Schwerpunktbildung auf die Sekundarstufe I sowie weitere vertiefende fachwissenschaftliche Studien- und Prüfungsleistungen in einem bereits studierten Fach in einem Umfang von mindestens 15 Leistungspunkten nachweist.

#### § 15 Lehramt für die Sekundarstufe II (berufliche Fächer)

Die Befähigung für das Lehramt für die Sekundarstufe II (berufliche Fächer) kann erwerben, wer die Befähigung für das

1.  Lehramt für die Bildungsgänge der Sekundarstufe I und der Primarstufe an allgemeinbildenden Schulen,
2.  Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer) oder
3.  Lehramt für Förderpädagogik

sowie Studien- und Prüfungsleistungen in einem Fach gemäß § 9 Absatz 1 und in einem Fach gemäß § 14 Absatz 1 Satz 1 Nummer 2 der Lehramtsstudienverordnung im Umfang von jeweils mindestens 45 Leistungspunkten nachweist.

#### § 16 Lehramt für Förderpädagogik

Die Befähigung für das Lehramt für Förderpädagogik kann erwerben, wer die Befähigung für das

1.  Lehramt für die Primarstufe,
2.  Lehramt für die Bildungsgänge der Sekundarstufe I und der Primarstufe an allgemeinbildenden Schulen,
3.  Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer) oder
4.  Lehramt für die Sekundarstufe II (berufliche Fächer)

sowie Studien- und Prüfungsleistungen in

1.  einem Fach gemäß § 16 Absatz 1 der Lehramtsstudienverordnung im Umfang von mindestens 45 Leistungspunkten,
2.  zwei sonderpädagogischen Fachrichtungen gemäß § 16 Absatz 2 Nummer 2 der Lehramtsstudienverordnung im Umfang von mindestens jeweils 30 Leistungspunkten und
3.  der allgemeinen Förder- und Inklusionspädagogik im Umfang von mindestens 20 Leistungspunkten

nachweist.

#### § 17 Lehreramt

Wer die Befähigung für das Amt als Lehrerin oder Lehrer für die unteren Klassen gemäß Besoldungsgruppe A 12 kw nachweist, kann die Befähigung für das Amt der Lehrerin oder des Lehrers im allgemeinbildenden Schulunterricht gemäß Fußnote 5 zur Besoldungsgruppe A 13 kw der Anlage 1 des Brandenburgischen Besoldungsgesetzes erwerben.
Voraussetzung hierfür ist das Zertifikatsstudium, das sich auf zwei sonderpädagogische Fachrichtungen erstreckt und die Studien- und Prüfungsleistungen im Umfang von 54 Leistungspunkten umfasst.

### Abschnitt 5  
Besondere Staatsprüfung

#### § 18 Grundsätze und Ziel der besonderen Staatsprüfung

(1) Für die Durchführung der besonderen Staatsprüfung gemäß § 8 Absatz 4 des Brandenburgischen Lehrerbildungsgesetzes gilt Abschnitt 3 der Ordnung für den Vorbereitungsdienst entsprechend, soweit in dieser Verordnung nichts Anderes geregelt wird.

(2) Die Prüfung wird in der Regel an der Schule durchgeführt, an der die Prüfungskandidatin oder der Prüfungskandidat überwiegend beschäftigt ist.
Sie ist in der Regel innerhalb von drei Monaten durchzuführen.
Die Frist beginnt mit der Zustellung der schriftlichen Mitteilung über die Zulassung zur Prüfung.

#### § 19 Zulassungsvoraussetzungen

(1) Zur besonderen Staatsprüfung wird zugelassen, wer ein weiteres Lehramt oder ein weiteres Lehreramt gemäß Abschnitt 4 erwerben will und die Voraussetzungen gemäß § 8 Absatz 4 des Brandenburgischen Lehrerbildungsgesetzes nachweist.

(2) Es sind Qualifizierungsmaßnahmen im Umfang von 200 Stunden zu fach- und schulstufenspezifischen Inhalten nachzuweisen,

1.  zu Anforderungen des Rahmenlehrplans und dessen Umsetzung in der entsprechenden Schulstufe und in den jeweiligen Fächern,
2.  in Fachwissenschaft, -didaktik und -methodik und
3.  zur Analyse, Planung und Reflexion von schulstufen- und fachspezifischen Lehr- und Lernprozessen.

#### § 20 Prüfungsleistungen

(1) Als Prüfungsleistungen sind

1.  eine unterrichtspraktische Prüfung und
2.  eine mündliche Prüfung

zu erbringen.

(2) Die unterrichtspraktische Prüfung besteht aus zwei Unterrichtsproben in dem Prüfungsfach.
Die beiden Unterrichtsproben sind in verschiedenen Klassen oder Kursen der dem angestrebten Lehramt oder Lehreramt entsprechenden Schulstufe oder Schulform abzulegen.
Für den Erwerb des Lehramtes für die Primarstufe muss eine Unterrichtsprobe mindestens im Fach Deutsch oder Mathematik durchgeführt werden.
Für den Erwerb des Lehramtes für Förderpädagogik sind die Unterrichtsproben im studierten Fach unter Berücksichtigung der jeweiligen studierten sonderpädagogischen Schwerpunkte durchzuführen.
Das für Schule zuständige Ministerium bestimmt auf Vorschlag des Prüflings in Abstimmung mit der Leiterin oder dem Leiter der Beschäftigungsschule die Termine für die Durchführung der Unterrichtsproben und die Klassen oder Kurse.

(3) Die mündliche Prüfung wird als Einzelprüfung durchgeführt und dauert in der Regel 40 Minuten.
Der Termin für die mündliche Prüfung wird auf Vorschlag des Prüflings durch die Vorsitzende oder den Vorsitzenden des Prüfungsausschusses festgelegt und bis spätestens zwei Wochen vor dem Prüfungstermin dem Prüfling schriftlich mitgeteilt.

(4) Die Aufgabenstellung für die mündliche Prüfung, die aus mehreren Teilaufgaben bestehen kann, und der dazugehörige Erwartungshorizont sind von den Mitgliedern des Prüfungsausschusses gemäß § 23 Absatz 1 Satz 2 Nummer 2 einvernehmlich zu erarbeiten.
Die Aufgabenstellung ist dem Prüfling vorab nicht bekannt zu geben, dem Prüfling ist jedoch eine Vorbereitungszeit von 30 Minuten zu gewähren.

#### § 21 Zulassungsantrag

(1) Der Antrag auf Zulassung zur besonderen Staatsprüfung (Zulassungsantrag) ist schriftlich bei dem für Schule zuständigen Ministerium zu stellen.

(2) In dem Zulassungsantrag sind das angestrebte Lehramt oder Lehreramt und das Prüfungsfach anzugeben.
Als Antragsunterlagen sind Nachweise, insbesondere Bescheinigungen oder Zertifikate zu Qualifizierungsmaßnahmen gemäß § 19 Absatz 2 beizufügen.

#### § 22 Entscheidung über die Zulassung

Die Entscheidung über die Zulassung trifft das für Schule zuständige Ministerium.
Sie ist auf dem Dienstweg der Bewerberin oder dem Bewerber schriftlich mitzuteilen und umfasst

1.  das angestrebte Lehramt oder Lehreramt und das Fach,
2.  die für die Durchführung der Prüfung beauftragte Stelle sowie
3.  den Tag der Prüfung und die Uhrzeit des Prüfungsbeginns (Prüfungstermin).

#### § 23 Prüfungsausschüsse

(1) Für jeden Prüfling werden Prüfungsausschüsse für die Unterrichtsproben der unterrichtspraktischen Prüfung und die mündliche Prüfung gebildet.
Es gehören dem Prüfungsausschuss

1.  für die Unterrichtsproben der unterrichtspraktischen Prüfung
    1.  die Vorsitzende oder der Vorsitzende,
    2.  eine geeignete Person mit einer fachlichen Befähigung für das Prüfungsfach, die im Rahmen der Lehrerausbildung beschäftigt ist, und
    3.  eine Lehrkraft der Schule, an der die Unterrichtsprobe stattfindet,

und

2.  für die mündliche Prüfung
    1.  die Vorsitzende oder der Vorsitzende,
    2.  die Vertreterin oder der Vertreter der Beschäftigungsschule und
    3.  eine geeignete Person mit einer fachlichen Befähigung für das Prüfungsfach, die im Rahmen der Lehrerausbildung beschäftigt ist,

als Mitglieder an.

(2) Als Vorsitzende für die Prüfungsausschüsse sind bei den Unterrichtsproben der unterrichtspraktischen Prüfung und bei der mündlichen Prüfung schulfachliche oder ausbildungsfachliche Vertreterinnen und Vertreter der Schulbehörden mit der Wahrnehmung des Vorsitzes für jede Prüfung zu beauftragen.
Sofern Personen gemäß Satz 1 nicht zur Verfügung stehen, können auch die Personen als Vorsitzende von Prüfungsausschüssen beauftragt werden, die über eine mehrjährige Erfahrung in der Lehrkräfteausbildung verfügen.
Die Beauftragung ist schriftlich bekannt zu geben.
Die übrigen, nach dieser Verordnung beauftragten Mitglieder der Prüfungsausschüsse gelten mit der Übertragung der Ausbildungsaufgaben als in den Prüfungsausschuss berufen.

#### § 24 Notenbildung und Gesamtnote der besonderen Staatsprüfung

(1) Aus den Bewertungen der beiden Unterrichtsproben wird die Note der unterrichtspraktischen Prüfung gebildet.
Der Prüfungsausschuss der zweiten Unterrichtsprobe setzt die Note der unterrichtspraktischen Prüfung fest.
Der Prüfungsausschuss der mündlichen Prüfung setzt die Note dieser Prüfung fest.

(2) Der Prüfungsausschuss der mündlichen Prüfung ermittelt nach der Prüfung aus

1.  der Note der mündlichen Prüfung und
2.  der vierfach gewichteten Note der unterrichtspraktischen Prüfung

die Gesamtnote der besonderen Staatsprüfung und setzt diese fest.

(3) Die besondere Staatsprüfung ist nicht bestanden, wenn

1.  die Gesamtnote der Staatsprüfung,
2.  die Note der unterrichtspraktischen Prüfung oder
3.  die Note der mündlichen Prüfung

nicht mindestens ausreichend (4,0) ist.
Sie ist ebenfalls nicht bestanden, wenn eine Note der Unterrichtsproben mit ungenügend (6,0) bewertet worden ist.

(4) Die oder der Vorsitzende des Prüfungsausschusses für die mündliche Prüfung teilt dem Prüfling am Ende der mündlichen Prüfung das Ergebnis der besonderen Staatsprüfung mit.

(5) Die für die Durchführung der Prüfung beauftragte Stelle gibt das Ergebnis der besonderen Staatsprüfung dem Prüfling schriftlich bekannt.
Wurde die Staatsprüfung nicht bestanden, sind dem Prüfling der früheste Zeitpunkt, zu dem ein Zulassungsantrag zur Wiederholungsprüfung gestellt werden kann, sowie die Frist, in der ein entsprechender Antrag gestellt werden kann, mitzuteilen.
Wurde die besondere Staatsprüfung endgültig nicht bestanden, erfolgt die Bekanntgabe des Ergebnisses der Staatsprüfung unverzüglich nach Abschluss des Prüfungsverfahrens.

#### § 25 Wiederholung der Prüfung

(1) Wurde die Prüfung nicht bestanden oder für nicht bestanden erklärt, kann sie innerhalb von fünf Jahren ab Bekanntgabe des Ergebnisses gemäß § 24 Absatz 5 einmal wiederholt werden.
Für die Antragstellung gilt § 21 entsprechend.

(2) Auf Antrag des Prüflings sind die Ergebnisse der bestandenen Prüfungsteile anzurechnen.

(3) Die Prüfung ist endgültig nicht bestanden, wenn die Wiederholungsprüfung nicht bestanden wurde.

#### § 26 Zeugnis und Bescheinigungen

Über die bestandene besondere Staatsprüfung ist von dem für Schule zuständigen Ministerium ein Zeugnis zu fertigen, das

1.  die Bezeichnung des Lehramtes und des Schwerpunktes einschließlich des Faches für das die Befähigung erworben wurde,
2.  die Note der unterrichtspraktischen Prüfung und die Note der mündlichen Prüfung und
3.  die Gesamtnote der besonderen Staatsprüfung

umfasst.

### Abschnitt 6  
Schlussbestimmungen

#### § 27 Übergangsregelung

Wer sich bei Inkrafttreten dieser Verordnung in einem Zertifikatsstudium gemäß Abschnitt 2 oder in einem Studium gemäß § 3 Absatz 3 der Befähigungserwerbsverordnung vom 17. Oktober 2013 (GVBl. II Nr. 74), die durch Verordnung vom 21.
März 2017 (GVBl.
II Nr.
19) geändert worden ist, befindet, beendet das Studium auf der Grundlage der Befähigungserwerbsverordnung vom 17. Oktober 2013 (GVBl. II Nr.
74), die durch Verordnung vom 21.
März 2017 (GVBl.
II Nr.
19) geändert worden ist.
Das Studium muss bis spätestens 30.
September 2023 abgeschlossen sein.

#### § 28 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Dezember 2020 in Kraft.
Gleichzeitig tritt die Befähigungserwerbsverordnung vom 17.
Oktober 2013 (GVBl.
II Nr.
74), die durch Verordnung vom 21. März 2017 (GVBl.
II Nr.
19) geändert worden ist, außer Kraft.

Potsdam, den 26.
November 2020

Die Ministerin für Bildung,  
Jugend und Sport

Britta Ernst