## Verordnung zur Festsetzung der Investitionspauschale nach dem Brandenburgischen Krankenhausentwicklungsgesetz  (Krankenhausinvestitionspauschalverordnung - BbgKHEGIPV)

Auf Grund des § 16 Absatz 8 des Brandenburgischen Krankenhausentwicklungsgesetzes vom 8. Juli 2009 (GVBl.
I S. 310), der durch Gesetz vom 18.
Dezember 2012 (GVBl.
I Nr. 44) neu gefasst worden ist, verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1  
Berechnungsgrundlagen der Investitionspauschale

(1) Die Berechnungsgrundlagen des Anteils der Investitionspauschale nach § 16 Absatz 2 Nummer 1 des Brandenburgischen Krankenhausentwicklungsgesetzes sind

1.  der Zeitpunkt und die Höhe der Finanzmittel, die dem Träger des Krankenhauses im Wege der Krankenhauseinzelförderung während des Zeitraumes vom 1.
    Januar 1991 bis 31. Dezember 2012 durch das Land bewilligt und ausgezahlt wurden,
2.  die Versorgungsstufe des Krankenhauses am 1.
    Januar 2013,
3.  die Anzahl der nach dem Feststellungsbescheid bedarfsnotwendigen vollstationären Betten am 1. Januar 2013,
4.  die Anzahl der nach dem Feststellungsbescheid bedarfsnotwendigen tagesklinischen Behandlungsplätze am 1.
    Januar 2013.

(2) Für die Berechnung des Anteils der Investitionspauschale nach § 16 Absatz 2 Nummer 2 des Brandenburgischen Krankenhausentwicklungsgesetzes sind maßgeblich

1.  für Krankenhäuser oder Teile von Krankenhäusern im Geltungsbereich des Krankenhausentgeltgesetzes
    1.  die Fallpauschalen nach § 7 Absatz 1 Satz 1 Nummer 1 des Krankenhausentgeltgesetzes vom 23.
        April 2002 (BGBl.
        I S. 1412, 1422) in der jeweils geltenden Fassung,
    2.  die Zusatzentgelte nach § 7 Absatz 1 Satz 1 Nummer 2 des Krankenhausentgeltgesetzes in der jeweils geltenden Fassung,
    3.  die Entgelte nach § 6 Absatz 1 des Krankenhausentgeltgesetzes in der jeweils geltenden Fassung,
    4.  die Vergütung neuer Untersuchungs- und Behandlungsmethoden nach § 6 Absatz 2 des Krankenhausentgeltgesetzes in der jeweils geltenden Fassung,
    5.  die Entgelte nach § 6 Absatz 2a des Krankenhausentgeltgesetzes in der jeweils geltenden Fassung,
    6.  die tagesbezogenen Pflegeentgelte nach § 7 Absatz 1 Satz 1 Nummer 6a des Krankenhausentgeltgesetzes in der jeweils geltenden Fassung;
2.  für Krankenhäuser oder Teile von Krankenhäusern im Geltungsbereich der Bundespflegesatzverordnung
    
    1.  die Entgelte nach § 7 Absatz 1 Satz 1 Nummer 1 der Bundespflegesatzverordnung in der jeweils geltenden Fassung,
    2.   die Zusatzentgelte nach § 7 Absatz 1 Satz 1 Nummer 2 der Bundespflegesatzverordnung in der jeweils geltenden Fassung,
    3.   die Entgelte nach § 6 Absatz 1 und Absatz 3 Satz 3 der Bundespflegesatzverordnung in der jeweils geltenden Fassung,
    4.  die Vergütung neuer Untersuchungs- und Behandlungsmethoden nach § 6 Absatz 4 der Bundespflegesatzverordnung in der jeweils geltenden Fassung,
    5.  die Entgelte für regionale und strukturelle Besonderheiten in der Leistungserbringung gemäß  
        § 6 Absatz 2 der Bundespflegesatzverordnung in der jeweils geltenden Fassung;
3.  die Erlöse der vor- und nachstationären Behandlungen nach § 115a des Fünften Buches Sozialgesetzbuch vom 20.
    Dezember 1988 (BGBl.
    I S. 2477, 2482) in der jeweils geltenden Fassung;
4.  die Entgelte für die Behandlung von Blutern mit Blutgerinnungsfaktoren nach Maßgabe der jeweils geltenden Fallpauschalenvereinbarung;
5.  die Entgelte für die medizinisch notwendige Aufnahme von Begleitpersonen;
6.  die Entgelte für die stationären Leistungen der integrierten Versorgung nach § 140a des Fünften Buches Sozialgesetzbuch in der jeweils geltenden Fassung;
7.  die Entgelte für die stationären Leistungen aus Modellvorhaben nach § 64b des Fünften Buches Sozialgesetzbuch (SGB V) in der jeweils geltenden Fassung.

### § 2  
Bemessung der Investitionspauschale

(1) Für die Bemessung der Investitionspauschale nach § 1 Absatz 1 sind maßgeblich

1.  der Zeitpunkt der Bewilligung der Finanzmittel sowie der Zeitfaktor, der sich nach dem Förderzeitraum bestimmt.
    Der Förderzeitraum
    1.  vom 1.
        Januar 1991 bis 31.
        Dezember 1994 erhält den Zeitfaktor 1,5
    2.  vom 1.
        Januar 1995 bis 31.
        Dezember 2001 erhält den Zeitfaktor 2,5
    3.  vom 1.
        Januar 2002 bis 31.
        Dezember 2006 erhält den Zeitfaktor 3,5
    4.  vom 1.
        Januar 2007 bis 31.
        Dezember 2012 erhält den Zeitfaktor 4,5.Die dem Krankenhaus gewährten Finanzmittel werden mit dem maßgeblichen Zeitfaktor multipliziert;
2.  die nach Nummer 1 ermittelte Berechnungsgröße im klinikspezifischen Verhältnis zu den anderen, nach dieser Verordnung anspruchsberechtigten Krankenhäusern.
    Der über einen prozentualen Wert ermittelte Finanzanteil des Krankenhauses an der Gesamtsumme der nach Satz 1 ermittelten Werte (gewichtetes Förderaufkommen) wird durch eine Äquivalenzziffer ersetzt.
    Das Krankenhaus erhält bei einem Finanzanteil
    
    1.  bis 0,49 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 17,
    2.  zwischen 0,50 Prozent und 0,99 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 16,
    3.  zwischen 1,00 Prozent und 1,49 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 15,
    4.  zwischen 1,50 Prozent und 1,99 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 14,
    5.  zwischen 2,00 Prozent und 2,49 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 13,
    6.  zwischen 2,50 Prozent und 2,99 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 12,
    7.  zwischen 3,00 Prozent und 3,49 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 11,
    8.  zwischen 3,50 Prozent und 3,99 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 10,
    9.  zwischen 4,00 Prozent und 4,49 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 9,
    10.  zwischen 4,50 Prozent und 4,99 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 8,
    11.  zwischen 5,00 Prozent und 5,49 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 7,
    12.  zwischen 5,50 Prozent und 5,99 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 6,
    13.  zwischen 6,00 Prozent und 6,49 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 5,
    14.  zwischen 6,50 Prozent und 6,99 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 4,
    15.  zwischen 7,00 Prozent und 7,49 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 3,
    16.  zwischen 7,50 Prozent und 7,99 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 2,
    17.  ab 8,00 Prozent des gewichteten Förderaufkommens die Äquivalenzziffer 1.
    
    Abweichend hiervon erhalten Krankenhäuser, denen seit dem Jahr 2009 Leistungen nach Maßgabe des § 9 Absatz 2 des Krankenhausfinanzierungsgesetzes in der Fassung der Bekanntmachung vom 10.
    April 1991 (BGBl.
    I S. 886), das zuletzt durch Artikel 16a des Gesetzes vom 21.
    Juli 2014 (BGBl.
    I S. 1133) geändert worden ist, gewährt wurden, die Äquivalenzziffer 8,5;
3.  die Versorgungsstufe des Krankenhauses und des hieraus abgeleiteten Versorgungsstufenfaktors, der bei
    1.  den Krankenhäusern der Grundversorgung 1,9
    2.  den Fachkrankenhäusern 2,2
    3.  den Krankenhäusern der Regelversorgung 2,4
    4.  den Krankenhäusern der qualifizierten Regelversorgung 2,9
    5.  den Krankenhäusern der Schwerpunktversorgung 3,4beträgt;
4.  die Anzahl der nach dem Feststellungsbescheid bedarfsnotwendigen vollstationären Betten und tagesklinischen Behandlungsplätze am 1.
    Januar 2013.
    Aus der nach Satz 1 ermittelten Anzahl wird ein Größenklassenfaktor abgeleitet, der bei
    1.  Krankenhäusern mit einer Gesamtzahl bis 100 Betten und tagesklinischen Behandlungsplätzen den Wert von 2,0
    2.  Krankenhäusern mit einer Gesamtzahl bis 150 Betten und tagesklinischen Behandlungsplätzen den Wert von 2,5
    3.  Krankenhäusern mit einer Gesamtzahl bis 200 Betten und tagesklinischen Behandlungsplätzen den Wert von 3,0
    4.  Krankenhäusern mit einer Gesamtzahl bis 250 Betten und tagesklinischen Behandlungsplätzen den Wert von 3,5
    5.  Krankenhäusern mit einer Gesamtzahl bis 300 Betten und tagesklinischen Behandlungsplätzen den Wert von 4,0
    6.  Krankenhäusern mit einer Gesamtzahl bis 350 Betten und tagesklinischen Behandlungsplätzen den Wert von 4,5
    7.  Krankenhäusern mit einer Gesamtzahl bis 400 Betten und tagesklinischen Behandlungsplätzen den Wert von 5,0
    8.  Krankenhäusern mit einer Gesamtzahl bis 450 Betten und tagesklinischen Behandlungsplätzen den Wert von 5,5
    9.  Krankenhäusern mit einer Gesamtzahl bis 500 Betten und tagesklinischen Behandlungsplätzen den Wert von 6,0
    10.  Krankenhäusern mit einer Gesamtzahl bis 550 Betten und tagesklinischen Behandlungsplätzen den Wert von 6,5
    11.  Krankenhäusern mit einer Gesamtzahl bis 600 Betten und tagesklinischen Behandlungsplätzen den Wert von 7,0
    12.  Krankenhäusern mit einer Gesamtzahl bis 650 Betten und tagesklinischen Behandlungsplätzen den Wert von 7,5
    13.  Krankenhäusern mit einer Gesamtzahl bis 700 Betten und tagesklinischen Behandlungsplätzen den Wert von 8,0
    14.  Krankenhäusern mit einer Gesamtzahl bis 750 Betten und tagesklinischen Behandlungsplätzen den Wert von 8,5
    15.  Krankenhäusern mit einer Gesamtzahl bis 800 Betten und tagesklinischen Behandlungsplätzen den Wert von 9,0
    16.  Krankenhäusern mit einer Gesamtzahl bis 850 Betten und tagesklinischen Behandlungsplätzen den Wert von 9,5
    17.  Krankenhäusern mit einer Gesamtzahl bis 900 Betten und tagesklinischen Behandlungsplätzen den Wert von 10,0
    18.  Krankenhäusern mit einer Gesamtzahl bis 950 Betten und tagesklinischen Behandlungsplätzen den Wert von 10,5
    19.  Krankenhäusern mit einer Gesamtzahl bis 1 000 Betten und tagesklinischen Behandlungsplätzen den Wert von 11,0
    20.  Krankenhäusern mit einer Gesamtzahl bis 1 050 Betten und tagesklinischen Behandlungsplätzen den Wert von 11,5
    21.  Krankenhäusern mit einer Gesamtzahl bis 1 100 Betten und tagesklinischen Behandlungsplätzen den Wert von 12,0
    22.  Krankenhäusern mit einer Gesamtzahl bis 1 150 Betten und tagesklinischen Behandlungsplätzen den Wert von 12,5
    23.  Krankenhäusern mit einer Gesamtzahl bis 1 200 Betten und tagesklinischen Behandlungsplätzen den Wert von 13,0
    24.  Krankenhäusern mit einer Gesamtzahl bis 1 250 Betten und tagesklinischen Behandlungsplätzen den Wert von 13,5
    25.  Krankenhäusern mit einer Gesamtzahl bis 1 300 Betten und tagesklinischen Behandlungsplätzen den Wert von 14,0
    26.  Krankenhäusern mit einer Gesamtzahl über 1 300 Betten und tagesklinischen Behandlungsplätzen den Wert von 14,5erhält.

(2) Die für das Krankenhaus maßgeblichen Werte

1.  der Äquivalenzziffer nach Nummer 2,
2.  des Versorgungsstufenfaktors nach Nummer 3,
3.  des Größenklassenfaktors nach Nummer 4

werden multipliziert.

Die nach Satz 1 ermittelte klinikspezifische Berechnungsgröße wird in das prozentuale Verhältnis zur Gesamtheit der klinikspezifischen Berechnungsgrößen aller nach dieser Verordnung anspruchsberechtigten Krankenhäuser, die einen zulässigen Antrag gemäß § 4 Absatz 3 gestellt haben, gesetzt.
Der auf dieser Grundlage errechnete Wert wird mit den nach § 16 Absatz 2 Nummer 1 des Brandenburgischen Krankenhausentwicklungsgesetzes zur Verfügung stehenden Finanzmitteln multipliziert und bildet die Summe der über den förderhistorischen Ansatz festzusetzenden Investitionspauschale.

(3) Für die Bemessung der Investitionspauschale nach § 1 Absatz 2 sind maßgeblich

1.  für Krankenhäuser oder Teile von Krankenhäusern im Geltungsbereich des Krankenhausentgeltgesetzes
    1.  die Erlössumme für Fallpauschalen nach Anlage 1 des jeweils geltenden Fallpauschalenkataloges für die Jahres- und Überlieger auf der Grundlage der effektiven Ist-Bewertungsrelationen zum 31.
        Dezember eines Jahres bewertet mit dem Landesbasisfallwert desselben Jahres,
    2.  die Erlössumme für Zusatzentgelte nach Anlage 2 des jeweils geltenden Fallpauschalenkataloges für die Jahres- und Überlieger zum 31.
        Dezember eines Jahres,
    3.  die Erlössumme der fallbezogenen Entgelte nach Anlage 3 des jeweils geltenden Fallpauschalenkataloges für die Jahres- und Überlieger auf der Grundlage der Ist-Leistungen zum 31.
        Dezember eines Jahres bewertet mit dem zuletzt vereinbarten Preis.
        Bei einer fehlenden Vereinbarung gilt der Preis gemäß den Regelungen der Fallpauschalenvereinbarung in der jeweils geltenden Fassung,
    4.  die Erlössumme der Zusatzentgelte nach Anlage 4 des jeweils geltenden Fallpauschalenkataloges für die Jahres- und Überlieger auf der Grundlage der Ist-Leistungen zum 31.
        Dezember eines Jahres bewertet mit dem zuletzt vereinbarten Preis.
        Bei einer fehlenden Vereinbarung gilt der Preis gemäß den Regelungen der Fallpauschalenvereinbarung in der jeweils geltenden Fassung,
    5.  die Erlössumme der Entgelte nach § 6 Absatz 2a des Krankenhausentgeltgesetzes in der jeweils geltenden Fassung auf der Grundlage der Ist-Leistungen zum 31.
        Dezember eines Jahres bewertet mit dem zuletzt vereinbarten Preis,
    6.  die Erlössumme der tagesbezogenen Entgelte nach Anlage 3 des jeweils geltenden Fallpauschalenkataloges und weitere tagesbezogene Entgelte für teilstationäre Leistungen für die Jahres- und Überlieger auf der Grundlage der Ist-Leistungen zum 31.
        Dezember eines Jahres bewertet mit dem zuletzt vereinbarten Tagessatz.
        Bei einer fehlenden Vereinbarung gilt der Preis gemäß den Regelungen der Fallpauschalenvereinbarung in der jeweils geltenden Fassung,
    7.  die Erlössumme der Entgelte für Besondere Einrichtungen auf der Grundlage der Ist-Leistungen zum 31.
        Dezember eines Jahres bewertet mit den zuletzt vereinbarten Tagessätzen ohne Ausgleiche und Berichtigungen,
    8.  die Erlössumme der Entgelte für Neue Untersuchungs- und Behandlungsmethoden auf der Grundlage der Ist-Leistungen zum 31.
        Dezember eines Jahres bewertet mit den zuletzt vereinbarten Preisen,
    9.  die Erlössumme der tagesbezogenen Pflegeentgelte nach § 7 Absatz 1 Satz 1 Nummer 6a des Krankenhausentgeltgesetzes auf der Grundlage der Ist-Leistungen zum 31. Dezember eines Jahres;
2.  für Krankenhäuser oder Teile von Krankenhäusern im Geltungsbereich der Bundespflegesatzverordnung
    
    1.  die Erlössumme für Entgelte nach Anlage 1a oder Anlage 2a des jeweils geltenden PEPP-Entgeltkataloges für die Jahres- und Überlieger auf der Grundlage der IST-Bewertungsrelationen zum 31. Dezember eines Jahres bewertet mit dem Basisentgeltwert (ohne Ausgleiche) desselben Jahres beziehungsweise dem zuletzt vereinbarten oder festgesetzten Basisentgeltwert (ohne Ausgleiche),
    2.  die Erlössumme für Entgelte nach Anlage 1b oder Anlage 2b des jeweils geltenden PEPP-Entgeltkataloges für die Jahres- und Überlieger zum 31.
        Dezember eines Jahres (Entgelte nach § 6 Absatz 1 Satz 1 der Bundespflegesatzverordnung) bewertet mit dem zuletzt vereinbarten Preis.
        Bei einer fehlenden Vereinbarung gilt der Preis gemäß den Regelungen der Verordnung pauschalierende Entgelte Psychiatrie und Psychosomatik oder der Vereinbarung über die pauschalierenden Entgelte für die Psychiatrie und Psychosomatik (PEPPV) in der jeweils geltenden Fassung,
    3.  die Erlössumme für Zusatzentgelte nach Anlage 3 des jeweils geltenden PEPP-Entgeltkataloges für die Jahres- und Überlieger zum 31.
        Dezember eines Jahres,
    4.  die Erlössumme für Zusatzentgelte nach Anlage 4 des jeweils geltenden PEPP-Entgeltkataloges für die Jahres- und Überlieger zum 31.
        Dezember eines Jahres bewertet mit dem zuletzt vereinbarten Preis.
        Bei einer fehlenden Vereinbarung gilt der Preis gemäß den Regelungen der Verordnung pauschalierende Entgelte Psychiatrie und Psychosomatik oder der Vereinbarung über die pauschalierenden Entgelte für die Psychiatrie und Psychosomatik (PEPPV) in der jeweils geltenden Fassung,
    5.  die Erlössumme für ergänzende Tagesentgelte nach Anlage 5 des jeweils geltenden PEPP-Entgeltkatalogs für die Jahres- und Überlieger zum 31.
        Dezember eines Jahres bewertet mit dem Basisentgeltwert (ohne Ausgleiche) desselben Jahres beziehungsweise dem zuletzt vereinbarten oder festgesetzten Basisentgeltwert (ohne Ausgleiche),
    6.  die Erlössumme für bewertete Entgelte bei stationsäquivalenter Behandlung nach § 115d des Fünften Buches Sozialgesetzbuch nach Anlage 6a des jeweils geltenden PEPP-Entgeltkatalogs für die Jahres- und Überlieger zum 31.
        Dezember eines Jahres bewertet mit dem Basisentgeltwert (ohne Ausgleiche) desselben Jahres beziehungsweise dem zuletzt vereinbarten oder festgesetzten Basisentgeltwert (ohne Ausgleiche),
    7.  die Erlössumme für unbewertete Entgelte bei stationsäquivalenter Behandlung nach § 115d des Fünften Buches Sozialgesetzbuch nach Anlage 6b des jeweils geltenden PEPP-Entgeltkatalogs für die Jahres- und Überlieger zum 31.
        Dezember eines Jahres bewertet mit dem zuletzt vereinbarten Preis.
        Bei einer fehlenden Vereinbarung gilt der Preis gemäß den Regelungen der Verordnung über pauschalierende Entgelte in der Psychiatrie und Psychosomatik oder der Vereinbarung über pauschalierende Entgelte für die Psychiatrie und Psychosomatik (PEPPV) in der jeweils geltenden Fassung,
    8.  die Erlössumme der Entgelte für regionale und strukturelle Besonderheiten in der Leistungserbringung nach § 6 Absatz 2 der Bundespflegesatzverordnung in der jeweils geltenden Fassung auf der Grundlage der Ist-Leistungen zum 31. Dezember eines Jahres bewertet mit den zuletzt vereinbarten Preisen,
    9.  die Erlössumme der Entgelte für Neue Untersuchungs- und Behandlungsmethoden nach § 6 Absatz 4 der Bundespflegesatzverordnung in der jeweils geltenden Fassung auf der Grundlage der Ist-Leistungen zum 31.
        Dezember eines Jahres bewertet mit den zuletzt vereinbarten Preisen;
3.  die Erlössumme für vor- und nachstationäre Behandlungen auf der Grundlage der Ist-Leistungen zum 31.
    Dezember eines Jahres;
4.  die Erlössumme der Entgelte für die Behandlung von Blutern mit Blutgerinnungsfaktoren auf der Grundlage der Ist-Leistungen zum 31.
    Dezember eines Jahres;
5.  die Erlössumme der Entgelte für die medizinisch notwendige Aufnahme von Begleitpersonen auf der Grundlage der Ist-Leistungen zum 31.
    Dezember eines Jahres;
6.  die Erlössumme der Entgelte für stationäre Leistungen der integrierten Versorgung auf der Grundlage der Ist-Leistungen zum 31.
    Dezember eines Jahres;
7.  die Erlössumme der Entgelte für stationäre Leistungen aus Modellvorhaben nach § 64b SGB V auf der Grundlage der Ist-Leistungen zum 31.
    Dezember eines Jahres.

(4) Die Summe der Erlöse nach Absatz 3 wird in das prozentuale Verhältnis zur Gesamtheit der Erlöse der anderen, nach dieser Verordnung anspruchsberechtigten Krankenhäuser, die einen zulässigen Antrag nach § 4 Absatz 3 gestellt haben, gesetzt.
Der auf dieser Grundlage errechnete Wert wird mit den nach § 16 Absatz 2 Nummer 2 des Brandenburgischen Krankenhausentwicklungsgesetzes zur Verfügung stehenden Finanzmitteln multipliziert und bildet die Summe der über den Ansatz von Leistungsparametern festzusetzenden Investitionspauschale.

(5) Soweit die Addition der Beträge nach den Absätzen 2 und 4 zu einem geringeren Anspruch führt, ist bei Krankenhäusern der Grundversorgung die Förderung auf 250 000 Euro festzusetzen.

### § 3  
Bemessung der Investitionspauschale zugunsten der Schulen für Gesundheitsberufe

Zur Förderung der notwendigen Investitionen und Reinvestitionen erhalten Träger, die nach dem Krankenhausfinanzierungsgesetz eine geförderte Schule für Gesundheitsberufe betreiben, eine jährliche Investitionspauschale von 200 Euro je besetzten pflegesatzfinanzierten Ausbildungsplatz.

### § 4  
Erhebungs- und Bemessungszeitraum

(1) Die Investitionspauschale wird jährlich festgesetzt.

(2) Die Bemessung der Investitionspauschale nach § 2 Absatz 3 stützt sich auf die Erlösdaten des vorvergangenen Jahres.

(3) Der Antrag auf Bewilligung der Investitionspauschale ist bis zum 31.
Oktober des Vorjahres zu stellen.
Als zwingender Bestandteil des Antrages sind der Bewilligungsbehörde innerhalb der Antragsfrist die Erlösdaten nach Absatz 2 mit einem entsprechenden Testat einer Wirtschaftsprüferin, eines Wirtschaftsprüfers oder einer Wirtschaftsprüfungsgesellschaft vorzulegen.
Nach Ablauf dieser Frist sind Anträge unzulässig.
Die Träger der Schulen für Gesundheitsberufe sind verpflichtet, der Bewilligungsbehörde die Daten zu den im vorvergangenen Jahr besetzten pflegesatzfinanzierten Ausbildungsplätzen als zwingend vorgegebenen Bestandteil des Antrages bis zum 31.
Oktober des Vorjahres mit Testat einer Wirtschaftsprüferin, eines Wirtschaftsprüfers oder einer Wirtschaftsprüfungsgesellschaft vorzulegen.
Nach Ablauf dieser Frist sind Anträge unzulässig.

### § 5  
Bereitstellung der Fördermittel

Die Fördermittel werden in vier Tranchen zum 15.
Februar, zum 15.
Mai, zum 15.
August und zum 15. November eines jeden Jahres ausgezahlt.

### § 6  
Nachweisführung

(1) Der Jahresabschluss des Krankenhauses ist unter Einbeziehung der Buchführung durch eine Wirtschaftsprüferin, einen Wirtschaftsprüfer oder eine Wirtschaftsprüfungsgesellschaft (Abschlussprüfer) zu prüfen.

(2) Die Prüfung des Jahresabschlusses ist nach den allgemeinen für Jahresabschlussprüfungen geltenden Grundsätzen durchzuführen.
Die Prüfung erstreckt sich insbesondere auf die zweckentsprechende, sparsame und wirtschaftliche Verwendung der Fördermittel nach § 16 des Brandenburgischen Krankenhausentwicklungsgesetzes.

(3) Werden nach dem abschließenden Ergebnis der Prüfung des Jahresabschlusses keine Einwendungen gegen die zweckentsprechende, sparsame und wirtschaftliche Verwendung der Fördermittel erhoben, hat der Abschlussprüfer dies zu bestätigen.
Soweit die Bestätigung versagt oder eingeschränkt erteilt wird, ist der Abschlussbericht der Bewilligungsbehörde vorzulegen.
Wird ein uneingeschränkter Bestätigungsvermerk erteilt, behält sich die Bewilligungsbehörde vor, den Abschlussbericht gemäß § 22 Absatz 1 des Brandenburgischen Krankenhausentwicklungsgesetzes anzufordern.

(4) Der Nachweis über die zweckentsprechende, sparsame und wirtschaftliche Verwendung der Fördermittel ist gesondert zu führen und der Bewilligungsbehörde mit gesondertem Vermerk des Abschlussprüfers bis zum 31.
Oktober des Folgejahres vorzulegen.

(5) Der Fördermittelempfänger ist verpflichtet,

1.  die Einhaltung der bei der Durchführung der Investition/Reinvestition geltenden Festlegungen des Landeskrankenhausplanes und Feststellungsbescheides sowie
2.  die Einhaltung der vergaberechtlichen Bestimmungen

bis zum 31.
Oktober des Folgejahres zu bestätigen.
Die Bestätigung ist nicht Gegenstand der Testatspflicht.

### § 7  
_(aufgehoben)_

### § 8  
Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2013 in Kraft.

Potsdam, den 10.
April 2013

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack