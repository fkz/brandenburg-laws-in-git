## Verordnung zur Regelung der zuständigen Behörde gemäß Artikel 252 Absatz 5 und Artikel 253 §§ 2 und 3 Absatz 1 und 2 des Einführungsgesetzes zum Bürgerlichen Gesetzbuche (EGBGB-Zuständigkeitsverordnung - EGBGBZV)

Auf Grund des § 12 Absatz 1 Satz 2 in Verbindung mit § 6 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), von denen durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28 S.
2) § 6 Absatz 2 geändert und § 12 Absatz 1 Satz 2 eingefügt worden ist, verordnet die Landesregierung:

#### § 1 Zuständigkeit der örtlichen Ordnungsbehörden

Zuständige Behörden im Sinne von Artikel 252 Absatz 5 sowie Artikel 253 §§ 2 und 3 Absatz 1 und 2 des Einführungsgesetzes zum Bürgerlichen Gesetzbuche in der Fassung der Bekanntmachung vom 21. September 1994 (BGBl.
I S.
2494; 1997 I S.
1061), das zuletzt durch Artikel 5 des Gesetzes vom 27. März 2020 (BGBl.
I S.
569) geändert worden ist, in der jeweils geltenden Fassung sind die örtlichen Ordnungsbehörden.

#### § 2 Kostenerstattung

Das Land erstattet den örtlichen Ordnungsbehörden die notwendigen Kosten für die Wahrnehmung der durch diese Verordnung zugewiesenen neuen Aufgabe bezüglich eingehender Auskunftsersuchen einschließlich der Personal- und Sachkosten.
Der nachgewiesene finanzielle Aufwand wird den zuständigen Behörden nach Ablauf eines Haushaltsjahres von dem für Wirtschaft zuständigen Ministerium auf Antrag erstattet.

#### § 3 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 7.
Mai 2020

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Minister für Wirtschaft, Arbeit und Energie

Prof.
Dr.-Ing.
Jörg Steinbach