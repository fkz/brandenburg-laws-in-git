## Verordnung über die Genehmigungsfreiheit von Rechtsgeschäften der Gemeinden (Genehmigungsfreistellungsverordnung - GenehmFV)

Auf Grund des § 111 Absatz 3 der Kommunalverfassung des Landes Brandenburg vom 18. Dezember 2007 (GVBl.
I S. 286), verordnet der Minister des Innern und für Kommunales im Einvernehmen mit dem Minister der Justiz und für Europa und Verbraucherschutz, dem Minister der Finanzen, dem Minister für Wirtschaft und Energie, dem Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft und der Ministerin für Infrastruktur und Landesplanung:

#### § 1 Allgemeine Regelung

(1) Die Veräußerung von Grundstücken, grundstücksgleichen Rechten, Unternehmen und Beteiligungen ist von der Genehmigungspflicht gemäß § 79 Absatz 3 der Kommunalverfassung des Landes Brandenburg freigestellt, wenn die Voraussetzungen der §§ 2, 3, 4 oder des § 5 erfüllt sind.

(2) Die Veräußerung von anderen als den in Absatz 1 genannten Vermögensgegenständen der Gemeinden unter ihrem vollen Wert ist von der Genehmigungspflicht gemäß § 79 Absatz 3 der Kommunalverfassung des Landes Brandenburg freigestellt, sofern es sich nicht um eine unentgeltliche Veräußerung handelt oder ein Preis vereinbart ist, der einer unentgeltlichen Veräußerung gleichkommt.
Die Veräußerung nach Satz 1 ist auch dann von der Genehmigungspflicht freigestellt, wenn die Voraussetzungen des § 4 oder § 5 erfüllt sind.

(3) Die Veräußerung von Vermögensgegenständen ist darüber hinaus von der Genehmigungspflicht gemäß § 79 Absatz 1 Satz 2 der Kommunalverfassung des Landes Brandenburg freigestellt, wenn die Voraussetzungen des § 4 oder § 5 erfüllt sind.

#### § 2 Grundstücke der Gemeinden

(1) Die Veräußerung von Grundstücken und grundstücksgleichen Rechten ist genehmigungsfrei, wenn der gesamte Kaufpreis spätestens sechs Monate nach Abschluss des Rechtsgeschäftes fällig wird und wenn diese

1.  zum Höchstgebot aus einer bedingungsfreien öffentlichen Ausschreibung,
2.  zum Verkehrswert nach § 194 des Baugesetzbuches, welcher durch ein Verkehrswertgutachten des zuständigen Gutachterausschusses für Grundstückswerte oder eines öffentlich bestellten und vereidigten Sachverständigen für die Bewertung von Immobilien, dessen Bewertungsstichtag bei Abschluss des Rechtsgeschäftes nicht länger als zwölf Monate zurückliegt, nachgewiesen wird,
3.  bei unbebauten Grundstücken zum geeigneten Bodenrichtwert gemäß § 196 des Baugesetzbuches in Verbindung mit § 16 Absatz 1 Satz 3 der Immobilienwertermittlungsverordnung oder
4.  zum Höchstgebot in einer durch einen öffentlich bestellten und vereidigten Versteigerer auf Grund der Versteigererverordnung durchgeführten Versteigerung, die erst durchgeführt werden darf, wenn auf eine Ausschreibung gemäß Nummer 1 kein Gebot abgegeben worden ist,

erfolgt.
Der Nachweis über die Wertermittlung ist zu den Akten zu nehmen.

(2) Veräußerungen von Grundstücken, die nicht größer als 100 Quadratmeter sind und deren aus Grundstücksgröße und Bodenrichtwert berechneter Wert weniger als 1 000 Euro beträgt, bedürfen unabhängig vom Verkaufspreis keiner Genehmigung.

(3) Die Veräußerung von Grundstücken und grundstücksgleichen Rechten, die ausschließlich der Wohnraumversorgung von Haushalten dienen, die sich nicht angemessen mit Wohnraum versorgen können und daher auf Unterstützung angewiesen sind und die einen Anspruch auf einen Wohnberechtigungsschein besitzen, ist auch genehmigungsfrei, wenn der Wert gemäß Absatz 1 Satz 1 Nummer 2 oder Nummer 3 um bis zu 40 Prozent, bei Maßnahmen des übrigen geförderten Wohnungsbaus um bis zu 20 Prozent, unterschritten wird und der gewährte Abschlag von diesem Wert durch eine Mehrerlösklausel für mindestens zehn Jahre durch ein Grundpfandrecht gesichert wird.

(4) Die Bestellung von Erbbaurechten ist genehmigungsfrei.

#### § 3 Veräußerung von kommunalen Unternehmen

Die Veräußerung von Unternehmen und Beteiligungen der Gemeinden gemäß § 92 Absatz 2 Nummer 2 bis 4 der Kommunalverfassung des Landes Brandenburg ist genehmigungsfrei, wenn

1.  in einer bedingungsfreien, transparenten und willkürfreien Ausschreibung mit nachfolgendem Bieterverfahren an den Meistbietenden veräußert wird,
2.  der Preis erreicht wird, den ein vereidigter Wirtschaftsprüfer oder eine Wirtschaftsprüfungsgesellschaft, die nicht gemäß § 319 des Handelsgesetzbuches von der Abschlussprüfung ausgeschlossen sind, in einer marktbezogenen, den aktuell anerkannten Standards entsprechenden Unternehmensbewertung ermittelt hat, oder
3.  frei handelbare Anteilscheine zum tagesaktuellen Kurs verkauft werden.

#### § 4 Gesetzlich geregelte Verfahren

Eine Genehmigung ist nicht erforderlich

1.  bei der Erfüllung gesetzlicher Veräußerungspflichten, insbesondere bei Veräußerung von Grundstücken im Rahmen von städtebaulichen Sanierungs- und Entwicklungsmaßnahmen nach dem Baugesetzbuch und bei Enteignung nach dem Baugesetzbuch oder nach dem Enteignungsgesetz des Landes Brandenburg,
2.  innerhalb eines Verfahrens der Bodenordnung (Verfahren nach dem Baugesetzbuch, dem Flurbereinigungsgesetz, dem Landwirtschaftsanpassungsgesetz oder dem Bodensonderungsgesetz),
3.  bei der Übertragung in das Treuhandvermögen eines von der Gemeinde beauftragten Sanierungs- oder Entwicklungsträgers, der die Anforderungen des Baugesetzbuches erfüllt, oder
4.  bei der Erfüllung von Pflichten nach dem Sachenrechtsbereinigungsgesetz, ausgenommen Pflichten aus Vereinbarungen nach § 3 Absatz 1 Satz 2 des Sachenrechtsbereinigungsgesetzes,

wenn dabei keine Veräußerung unter den gesetzlich vorgesehenen Werten erfolgt.

#### § 5 Veräußerung an juristische Personen des öffentlichen Rechts und an kommunale Unternehmen

(1) Rechtsgeschäfte mit dem Land, einzelnen Gemeinden, Gemeindeverbänden und kommunalen Anstalten bedürfen keiner Genehmigung.

(2) Die Veräußerung von Vermögensgegenständen an Unternehmen mit eigener Rechtspersönlichkeit in einer Form des Privatrechts, deren alleiniger Träger mittelbar oder unmittelbar allein oder zusammen mit anderen Gemeinden die veräußernde Gemeinde ist, bedarf unabhängig von der Höhe des Kaufpreises keiner Genehmigung, wenn

1.  der Verkehrswert eines Grundstückes gemäß § 2 Absatz 1 Satz 1 Nummer 2 oder Nummer 3 nachgewiesen wird,
2.  der Wert von Beteiligungen und Unternehmen gemäß § 3 Nummer 2 oder Nummer 3 ermittelt wurde oder
3.  bei sonstigem Vermögen die fortgeschriebenen Anschaffungs- und Herstellungskosten angesetzt sind.

Erfüllt das kommunale Unternehmen unter Verwendung dieser Vermögensgegenstände eine pflichtige kommunale Aufgabe, ist die Veräußerung gemäß Satz 1 nur dann genehmigungsfrei, wenn die Aufgabenerledigung durch den Erwerber vertraglich abgesichert ist und durch eine entsprechende Klausel im Einbringungs- beziehungsweise Übertragungsvertrag gewährleistet ist, dass die Gemeinde die Vermögensgegenstände zurückerhalten kann, wenn der Erwerber die Aufgabe nicht weiter erfüllt.
Bei Grundstücksübertragungen ist eine jede andere Nutzung ausschließende Dienstbarkeit und eine entsprechende Rückauflassungsvormerkung zu bestellen.

#### § 6 Belastungsvollmachten

Bestellt die Gemeinde gemäß § 75 Absatz 4 der Kommunalverfassung des Landes Brandenburg im Rahmen der Veräußerung eines Grundstückes oder eines bestehenden Erbbaurechts ein Grundpfandrecht oder bevollmächtigt sie den Käufer, Grundpfandrechte zugunsten Dritter zu bestellen, so bedarf dies unter folgenden Voraussetzungen keiner Genehmigung:

1.  Die Bestellung darf nur zugunsten eines Kreditinstitutes, das gemäß dem Kreditwesengesetz in Deutschland Bankgeschäfte betreiben oder Finanzdienstleistungen erbringen darf, erfolgen.
2.  In der Grundpfandrechtsbestellungsurkunde sind folgende Bestimmungen im Wortlaut wiederzugeben:
    1.  Der Grundpfandrechtsgläubiger darf das Grundpfandrecht nur insoweit als Sicherheit verwerten oder behalten, wie er tatsächlich Zahlungen mit Tilgungswirkung auf die Kaufpreisschuld des Käufers geleistet hat.
        Alle weiteren Zweckbestimmungserklärungen, Sicherungs- und Verwertungsvereinbarungen innerhalb oder außerhalb der Urkunde gelten erst, nachdem der Kaufpreis vollständig bezahlt ist, in jedem Fall ab Eigentumsumschreibung.
        Ab diesem Zeitpunkt gelten sie für und gegen den Käufer als neuen Sicherungsgeber.
    2.  Der Verkäufer übernimmt im Zusammenhang mit der Grundpfandrechtsbestellung keinerlei persönliche Zahlungsverpflichtungen.
        Der Käufer verpflichtet sich, den Verkäufer von allen Kosten und sonstigen Folgen der Grundpfandrechtsbestellung freizustellen.
3.  In Fällen der Belastung von Grundstücken, die nur hinsichtlich einer katastermäßig noch nicht erfassten Teilfläche von der Veräußerung betroffen sind, ist zusätzlich folgende Bestimmung in die Urkunde aufzunehmen:
    
    Der Grundpfandrechtsgläubiger verpflichtet sich unwiderruflich, die nicht veräußerte Teilfläche des Grundstückes unverzüglich nach Fortführung des Liegenschaftskatasters auflagenfrei aus der Haftung zu entlassen und bis zu diesem Zeitpunkt keine Zwangsvollstreckungsmaßnahmen vorzunehmen.
    
4.  Wird in dem Veräußerungsgeschäft durch den Verkäufer eine Vollmacht zur Grundpfandrechtsbestellung erteilt, sind darin die unter den Nummern 1 bis 3 genannten Bestimmungen im Wortlaut vorzuschreiben.

#### § 7 Erklärung der Genehmigungsfreiheit

Bei der

1.  genehmigungsfreien Veräußerung von Grundstücken und grundstücksgleichen Rechten sowie
2.  Bestellung beziehungsweise der Ermächtigung zur Bestellung eines Grundpfandrechtes gemäß § 6

ist dem jeweiligen Antrag auf Eintragung in das Grundbuch eine in der Form des § 29 Absatz 3 der Grundbuchordnung ausgestellte separate Erklärung der veräußernden Gemeinde beizufügen, dass der Abschluss des Rechtsgeschäftes genehmigungsfrei ist.
In der Erklärung ist auf die in Betracht kommende Vorschrift ausdrücklich Bezug zu nehmen.

#### § 8 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Genehmigungsfreistellungsverordnung vom 9.
März 2009 (GVBl.
II S.
118), die durch Artikel 16 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 32 S.
32) geändert worden ist, außer Kraft.

Potsdam, den 4.
Oktober 2019

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter