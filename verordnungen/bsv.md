## Verordnung über die Bildungsgänge der Berufsschule (Berufsschulverordnung - BSV)

Auf Grund des § 25 Absatz 6 in Verbindung mit § 13 Absatz 3, § 56, § 57 Absatz 4, § 58 Absatz 3, § 59 Absatz 9 und § 61 Absatz 3 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
August 2002 (GVBl.
I S. 78), von denen § 13 Absatz 3 durch Artikel 1 des Gesetzes vom 8.
Januar 2007 (GVBl.
I S. 2) und § 56 durch Artikel 2 des Gesetzes vom 14. März 2014 (GVBl.
I Nr.
14) geändert worden sind, verordnet der Minister für Bildung, Jugend und Sport:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeine Bestimmungen

[§ 1 Geltungsbereich, Bildungsgänge und Ziele](#1)

[§ 2 Zusammenarbeitsgebot](#2)

[§ 3 Zusammenarbeit von Berufsschule und Ausbildungsstätte](#3)

[§ 4 Berufsschulbesuch](#4)

### Abschnitt 2  
Bildungsgang zur Vermittlung des schulischen Teils einer Berufsausbildung gemäß Berufsbildungsgesetz oder Handwerksordnung

[§ 5 Aufnahmevoraussetzungen](#5)

[§ 6 Anmeldung](#6)

[§ 7 Dauer des Bildungsgangs](#7)

[§ 8 Klassenbildung](#8)

[§ 9 Umfang und Organisation des Unterrichts](#9)

[§ 10 Stundentafeln](#10)

[§ 11 Fächer und Lernfelder im berufsbezogenen Bereich](#11)

[§ 12 Fächer im berufsübergreifenden Bereich](#12)

[§ 13 Fremdsprachen](#13)

[§ 14 Wahlpflichtbereich](#14)

[§ 15 Noten](#15)

[§ 16 Zeugnisse](#16)

[§ 17 Abschluss des Bildungsgangs](#17)

[§ 18 Gleichstellung von Abschlüssen](#18)

### Abschnitt 3  
Leistungsbewertung

[§ 19 Leistungsbewertung](#19)

[§ 20 Verweigerung und Täuschung bei Leistungsnachweisen](#20)

### Abschnitt 4  
Bildungsgänge zur Vertiefung der Allgemeinbildung und zur Berufsorientierung, Berufsvorbereitung oder Berufsausbildungsvorbereitung

[§ 21 Organisation der Bildungsgänge](#21)

[§ 22 Klassenbildung](#22)

[§ 23 Noten, Zeugnisse, Abschluss des Bildungsgangs](#23)

[§ 24 Gleichstellung von Abschlüssen](#24)

### Abschnitt 5  
Sonderpädagogische Förderung in den Bildungsgängen der Berufsschule

[§ 25 Ziel und Dauer](#25)

[§ 26 Aufnahmevoraussetzungen](#26)

[§ 27 Klassenbildung](#27)

[§ 28 Sonderpädagogische Förderung und Begleitung](#28)

### Abschnitt 6  
Schlussbestimmungen

[§ 29 Übergangsvorschriften](#29)

[§ 30 Inkrafttreten, Außerkrafttreten](#30)

[Anlage 1 Rahmenstundentafel des Bildungsgangs zur Vermittlung des schulischen Teils einer Berufsausbildung gemäß Berufsbildungsgesetz oder Handwerksordnung](#31)

[Anlage 2 Rahmenstundentafeln für die Bildungsgänge zur Vertiefung der Allgemeinbildung und zur Berufsorientierung, Berufsvorbereitung oder Berufsausbildungsvorbereitung](#32)

### Abschnitt 1  
Allgemeine Bestimmungen

#### § 1 Geltungsbereich, Bildungsgänge und Ziele

(1) Die Bestimmungen dieser Verordnung gelten für Schülerinnen und Schüler

1.  mit einem Berufsausbildungsvertrag gemäß Berufsbildungsgesetz oder Handwerksordnung,
2.  mit einer Fördervereinbarung der Bundesagentur für Arbeit oder
3.  mit einem Arbeitsvertrag.

(2) Die Bildungsgänge werden in Oberstufenzentren eingerichtet.

(3) Die Bildungsgänge der Berufsschule sind

1.  der Bildungsgang zur Vermittlung des schulischen Teils einer Berufsausbildung gemäß Berufsbildungsgesetz oder Handwerksordnung und
2.  die Bildungsgänge zur Vertiefung der Allgemeinbildung und zur Berufsorientierung, Berufsvorbereitung oder Berufsausbildungsvorbereitung.

(4) Die Bildungsgänge der Berufsschule haben auf der Grundlage der §§ 4 und 25 des Brandenburgischen Schulgesetzes in Verbindung mit der Rahmenvereinbarung über die Berufsschule (Beschluss der Kultusministerkonferenz) insbesondere zum Ziel, eine berufliche Handlungskompetenz zu entwickeln.

#### § 2 Zusammenarbeitsgebot

Das Oberstufenzentrum arbeitet gemäß § 9 Absatz 1 Satz 1 in Verbindung mit § 7 Absatz 1 und 2 des Brandenburgischen Schulgesetzes insbesondere mit den

1.  zuständigen Stellen gemäß Berufsbildungsgesetz und Handwerksordnung sowie den Innungen und Fachverbänden,
2.  betrieblichen, überbetrieblichen und außerbetrieblichen Ausbildungsstätten,
3.  Agenturen für Arbeit,
4.  Jugend- und Sozialämtern,
5.  Fachhochschulen und Hochschulen sowie
6.  bei Minderjährigen mit den Eltern und
7.  anderen Oberstufenzentren sowie mit allgemeinbildenden Schulen

zusammen.

#### § 3 Zusammenarbeit von Berufsschule und Ausbildungsstätte

(1) Soweit das Führen von Berichtsheften oder Ausbildungsnachweisen im Ausbildungsberuf vorgeschrieben ist, hat grundsätzlich die Klassenlehrkraft einmal im Schulhalbjahr die Berichtshefte oder Ausbildungsnachweise für den Berufsschulunterricht zur Kenntnis zu nehmen, wenn zuvor die oder der Ausbildende das Berichtsheft oder den Ausbildungsnachweis abgezeichnet hat.

(2) Die oder der Ausbildende kann mit Zustimmung der Schulleitung des Oberstufenzentrums und der jeweiligen Lehrkraft am Unterricht teilnehmen, um sich über die dort vermittelten Inhalte zu informieren.
Informationen zu diesem Unterricht gegenüber Dritten dürfen nur mit Zustimmung der Lehrkraft gegeben werden.
Dabei zur Kenntnis gelangte personenbezogene Daten der Auszubildenden dürfen nur mit deren Einwilligung weitergegeben werden.

(3) Werden bei Auszubildenden Lerndefizite festgestellt, sind im Rahmen der Zusammenarbeit mit der oder dem Ausbildenden unter Einbeziehung der oder des Auszubildenden die Möglichkeiten der Fördermaßnahmen im Oberstufenzentrum oder in der Ausbildungsstätte zur Optimierung des Leistungsstandes miteinander abzustimmen.
Die hierfür erforderlichen personenbezogenen Daten werden durch das Oberstufenzentrum der oder dem Ausbildenden gemäß § 65 Absatz 6 des Brandenburgischen Schulgesetzes übermittelt.

#### § 4 Berufsschulbesuch

(1) Zum Nachweis des regelmäßigen Besuchs der Berufsschule gegenüber der Ausbildungsstätte dienen Schulbesuchskarten.
Diese werden den Auszubildenden auf Anforderung der Ausbildungsstätte ausgestellt und unter Angabe des Zeitpunktes, an dem Auszubildende den Unterricht begonnen und beendet haben, von der zuletzt unterrichtenden Lehrkraft am jeweiligen Unterrichtstag abgezeichnet.
Der Ausbildende nimmt diese Information durch seine Unterschrift zur Kenntnis.

(2) Wird diese Kenntnisnahme über einen längeren Zeitraum nicht dokumentiert, so entfällt die Verpflichtung gemäß Absatz 1.

(3) Ab dem vierten unentschuldigten Fehltag von Auszubildenden ist die Ausbildungsstätte darüber zu benachrichtigen.

### Abschnitt 2  
Bildungsgang zur Vermittlung des schulischen Teils einer Berufsausbildung gemäß Berufsbildungsgesetz oder Handwerksordnung

#### § 5 Aufnahmevoraussetzungen

(1) In den Bildungsgang gemäß § 1 Absatz 3 Nummer 1 wird aufgenommen, wer sich in einem Ausbildungsverhältnis auf Grund eines Berufsausbildungsvertrages gemäß Berufsbildungsgesetz oder Handwerksordnung befindet.
Die Aufnahme erfolgt in der Regel zu Beginn des Schuljahres.

(2) Die Aufnahme von Teilnehmerinnen und Teilnehmern in Umschulungsmaßnahmen der Bundesagentur für Arbeit oder in betriebliche Einzelumschulungsmaßnahmen erfolgt nach Maßgabe der personellen, sächlichen und schulorganisatorischen Voraussetzungen.

#### § 6 Anmeldung

(1) Ausbildungsstätten gemäß § 27 des Berufsbildungsgesetzes oder § 21 der Handwerksordnung melden die Schülerinnen und Schüler gemäß der Landesschulbezirksverordnung am jeweils zuständigen Oberstufenzentrum an.
Ausbildungsstätte und Auszubildende erhalten von der Schulleitung eine Bestätigung über die Aufnahme und Zuordnung in die jeweilige Klasse.
Wurde diese Klasse in einem anderen Oberstufenzentrum eingerichtet, leitet das Oberstufenzentrum, an dem die Anmeldung erfolgte, diese unverzüglich an das nunmehr zuständige Oberstufenzentrum weiter und teilt dies den Ausbildungsstätten und Auszubildenden mit.

(2) Teilnehmerinnen oder Teilnehmer an Umschulungsmaßnahmen gemäß § 39 Absatz 4 Satz 2 des Brandenburgischen Schulgesetzes werden vom Maßnahmenträger oder vom Arbeitgeber angemeldet.

#### § 7 Dauer des Bildungsgangs

(1) Die Dauer des Schulbesuchs richtet sich gemäß den §§ 5 und 6 des Berufsbildungsgesetzes oder gemäß den §§ 26 und 42m der Handwerksordnung nach den Ausbildungsordnungen oder Ausbildungsregelungen.

(2) Gemäß den §§ 7 und 8 des Berufsbildungsgesetzes oder den §§ 27a und 27b der Handwerksordnung sind Anrechnungen beruflicher Vorbildungen auf die Ausbildungszeit ebenso möglich wie deren Verkürzung und Verlängerung.

(3) Besteht die Schülerin oder der Schüler die Abschlussprüfung vor Ablauf der Ausbildungszeit, so endet der Bildungsgang mit Bestehen der Abschlussprüfung.

(4) Verlängert sich die Berufsausbildung, so organisiert das Oberstufenzentrum den Schulbesuch nach Maßgabe der im Einzelfall angemessenen Förderung.

#### § 8 Klassenbildung

(1) Die Klassen werden grundsätzlich aus Schülerinnen und Schülern des gleichen Ausbildungsjahres entsprechend den pädagogischen und organisatorischen Möglichkeiten des Oberstufenzentrums nach Ausbildungsberufen gebildet.
Berufsübergreifende Klassen können bei einer berufsfeldbreiten Grundbildung eingerichtet werden.
Daneben können im Organisationsrahmen des Oberstufenzentrums berufsübergreifende Kurse zur Vermittlung besonderer Inhalte eingerichtet werden.

(2) Für die Bildung von Klassen gelten die Bestimmungen der Landesschulbezirksverordnung.

#### § 9 Umfang und Organisation des Unterrichts

(1) Bei der Festlegung von Organisationsvarianten sind pädagogische und lernpsychologische Ziele zu beachten.

(2) Der Unterricht umfasst den berufsbezogenen, den berufsübergreifenden Bereich und den Wahlpflichtbereich.

(3) Der Unterricht umfasst mindestens 480 Unterrichtsstunden im Schuljahr und kann in diesem Rahmen unterschiedlich auf die beiden Schulhalbjahre verteilt werden.
Der Unterricht umfasst grundsätzlich höchstens acht Unterrichtsstunden am Schultag.
Er wird an einzelnen Unterrichtstagen oder in geblockter Form in Unterrichtswochen (Blockunterricht) erteilt.

(4) Die Organisation des Unterrichts erfolgt durch die Schulleitung im Benehmen mit den Ausbildungsstätten und zuständigen Stellen.
Wird bei einer vollständigen Lerngruppe die Ausbildungszeit verkürzt, so organisiert das Oberstufenzentrum in Abstimmung mit den Ausbildungsstätten den Unterricht.

(5) Blockunterricht soll nur zu Beginn des Schuljahres eingeführt, geändert oder aufgehoben werden.

#### § 10 Stundentafeln

(1) Der Unterricht ist auf der Grundlage der Rahmenstundentafel gemäß Anlage 1 für Berufe im berufsbezogenen und berufsübergreifenden Bereich durchzuführen.

(2) Für Berufe im Bildungsgang gemäß § 1 Absatz 3 Nummer 1, in denen besondere zusätzliche berufliche Qualifikationen nach Maßgabe entsprechender Festlegungen im Berufsausbildungsvertrag vermittelt werden, werden von den Oberstufenzentren gesonderte Stundentafeln festgelegt.

#### § 11 Fächer und Lernfelder im berufsbezogenen Bereich

(1) Die Fächer oder Lernfelder des berufsbezogenen Bereichs und ihr jeweiliger Stundenrahmen richten sich nach den VV-Rahmenlehrplan und curriculare Materialien.

(2) Die im berufsbezogenen Bereich in den Fächern oder Lernfeldern verbindlich zu erteilenden Unterrichtsstunden sind gemäß den im Rahmenlehrplan oder in anderen curricularen Materialien ausgewiesenen Zeitrichtwerten je Ausbildungsjahr im jeweiligen Schuljahr umzusetzen.
Eine schuljahresübergreifende Verteilung der Jahresstunden bedarf der Genehmigung des für Schule zuständigen Ministeriums.

#### § 12 Fächer im berufsübergreifenden Bereich

(1) Der berufsübergreifende Bereich besteht aus den Fächern Deutsch, Fremdsprache, Wirtschafts- und Sozialkunde und Sport.

(2) Die Fächer im berufsübergreifenden Bereich sind grundsätzlich im Mindestumfang von jeweils 40 Unterrichtsstunden pro Schuljahr zu unterrichten.

(3) Das Fach Wirtschafts- und Sozialkunde wird durchgehend mit mindestens jeweils 40 Jahresstunden unterrichtet.

(4) Die Fächer Deutsch und Sport müssen in mindestens zwei Ausbildungsjahren mit jeweils mindestens 40 Jahresstunden unterrichtet werden.

(5) Der Unterricht in den berufsübergreifenden Fächern kann entsprechend den personellen und organisatorischen Möglichkeiten des Oberstufenzentrums im Klassenverband oder in leistungsdifferenzierten Kursen erteilt werden.

#### § 13 Fremdsprachen

(1) Das Fach Englisch ist in allen Berufen, mit Ausnahme der gastgewerblichen Ausbildungsberufe, in denen wahlweise auch das Fach Französisch angeboten werden kann, die verbindlich zu unterrichtende Fremdsprache.
Soll abweichend von dieser Regel eine andere Fremdsprache im berufsübergreifenden Bereich unterrichtet werden, bedarf es der Genehmigung des für Schule zuständigen Ministeriums.

(2) Der Mindeststundenrahmen ergibt sich aus den Bestimmungen gemäß § 12 sowie den vorgegebenen Stunden gemäß dem Rahmenlehrplan für den einzelnen Beruf.
Sind für die einzelnen Berufe keine Festlegungen getroffen worden, so muss in mindestens zwei Ausbildungsjahren mit jeweils mindestens 40 Jahresstunden unterrichtet werden.
Für Ausbildungsberufe, in denen eine hiervon abweichende Mindestvorgabe gilt, ist diese verbindlich.

#### § 14 Wahlpflichtbereich

(1) Die Stunden des Wahlpflichtbereichs sollen zur Stützung, Vertiefung und Erweiterung oder zur besonderen Schwerpunktsetzung genutzt werden.

(2) Die Unterrichtsstunden des Wahlpflichtbereichs können für den berufsbezogenen Bereich und für den berufsübergreifenden Bereich genutzt werden.

(3) Die jeweiligen Jahresstunden des Wahlpflichtbereichs entsprechen der Differenz aus den Jahresstunden gemäß § 9 Absatz 3 Satz 1 und den Jahresstunden des berufsübergreifenden Bereichs mit der jeweiligen Mindestbindung im Fach und den vorgegebenen Jahresstunden des berufsbezogenen Bereichs.
Das Oberstufenzentrum entscheidet über die Verteilung der Stunden des Wahlpflichtbereichs auf den berufsbezogenen Lernbereich oder die Fächer des berufsübergreifenden Bereichs im Rahmen der Bestimmungen gemäß Absatz 1.

#### § 15 Noten

(1) Ist gemäß Rahmenlehrplan nach Fächern zu unterrichten, werden die Leistungen in jedem Unterrichtsfach mit einer Note bewertet.
Für diese Note sind die Leistungen und die Leistungsentwicklung über den gesamten Unterricht dieses Faches zu berücksichtigen.

(2) Ist gemäß Rahmenlehrplan nach Lernfeldern zu unterrichten, werden die Leistungen in jedem Lernfeld mit einer Note bewertet.
Können Lernfelder wegen einer verkürzten Ausbildung nicht oder nur unvollständig unterrichtet werden, erfolgt keine Bewertung.
Wird ein Lernfeld über mehr als ein Schulhalbjahr oder Schuljahr unterrichtet, wird die Note nach Abschluss des Lernfelds erteilt.
Absatz 1 Satz 2 gilt entsprechend.

(3) Die Abschlussnoten werden aus dem Mittelwert der Noten aus den Halbjahres- oder Jahreszeugnissen unter Berücksichtigung der Leistungsentwicklung gebildet.

(4) Bei Unterricht in den Lernfeldern ist die Abschlussnote für den berufsbezogenen Bereich eine Durchschnittsnote mit einer Dezimalstelle, die sich als gewichteter arithmetischer Mittelwert aus den Teilnoten für die jeweils im Schulhalbjahr oder im Schuljahr abgeschlossenen Lernfelder zusammensetzt.
Die Teilnoten werden in Abhängigkeit vom Stundenumfang des jeweiligen Lernfelds im Unterrichtszeitraum gewichtet.
Es wird nicht gerundet.

(5) Die Gesamtabschlussnote ist der Gesamtnotendurchschnitt mit einer Dezimalstelle, der sich als gewichteter arithmetischer Mittelwert aus den Abschlussnoten des berufsbezogenen und des berufsübergreifenden Bereichs zusammensetzt.
Dabei werden unter Berücksichtigung der Zuordnung des Wahlpflichtbereichs die Abschlussnoten des berufsbezogenen Bereichs dreifach und die Abschlussnoten des berufsübergreifenden Bereichs einfach gewichtet.
Das Fach Sport wird bei der Bildung der Gesamtabschlussnote nicht berücksichtigt.
Es wird nicht gerundet.

#### § 16 Zeugnisse

(1) Die Schülerinnen und Schüler erhalten grundsätzlich am Ende eines jeden Schulhalbjahres und in jedem Fall am Ende des jeweiligen Schuljahres ein Zeugnis.
Diese Zeugnisse tragen das Datum des jeweils letzten Unterrichtstages der Klasse.
Sie sind der oder dem Ausbildenden und bei Minderjährigen den Eltern zur Kenntnisnahme vorzulegen.

(2) Bei Blockunterricht mit einer Länge von mindestens drei Unterrichtswochen je Block sind ausschließlich Jahreszeugnisse zu erteilen.
Diese Zeugnisse tragen das Datum des letzten Unterrichtstages der Klasse.

(3) Ein Abschlusszeugnis zusätzlich zu den Halbjahres- oder Jahreszeugnissen erhält, wer den Bildungsgang erfolgreich abschließt.
Das Abschlusszeugnis trägt das Datum des letzten Unterrichtstages und wird in der Regel am letzten Unterrichtstag ausgegeben.

(4) Kann auf dem Abschlusszeugnis eine Gleichstellung gemäß § 18 Absatz 3 bescheinigt werden, so trägt es den Zusatz: „Die Gleichstellung gilt nur in Zusammenhang mit dem erfolgreichen Berufsabschluss gemäß Berufsbildungsgesetz oder Handwerksordnung.“

(5) Ein Abgangszeugnis erhält, wer den Bildungsgang verlässt, ohne dass das Ziel des Bildungsgangs erreicht wurde.
§ 15 Absatz 3 und 4 gilt entsprechend.
Das Zeugnis trägt das Datum der Beendigung des Schulverhältnisses.

(6) Schülerinnen und Schüler, die die Ausbildung nach der ersten Stufe einer Stufenausbildung gemäß § 5 des Berufsbildungsgesetzes oder § 26 der Handwerksordnung erfolgreich beendet haben und nicht in die letzte Stufe übergehen, erhalten ein Abschlusszeugnis.
Wer die Ausbildung nach der ersten Stufe einer zweistufigen Stufenausbildung erfolgreich beendet hat und in die letzte Stufe übergeht, erhält bei vorzeitigem Abgang oder bei nicht erfolgreicher Beendigung der letzten Stufe ein Abgangszeugnis.
Es wird folgender Vermerk im Abgangszeugnis aufgenommen: „Die Schülerin/Der Schüler hat das Ziel der Berufsschule in der … Stufe der Stufenausbildung erreicht.“

#### § 17 Abschluss des Bildungsgangs

(1) Den erfolgreichen Abschluss des Bildungsgangs stellt die Klassenkonferenz fest.

(2) Das Ziel des Bildungsgangs ist erreicht, wenn in allen Fächern mit Ausnahme des Faches Sport mindestens ausreichende Leistungen erreicht wurden.
Bei lernfeldstrukturierten Berufen ist das Ziel erreicht, wenn in den berufsübergreifenden Fächern mindestens ausreichende Leistungen erreicht wurden und in den Lernfeldern im berufsbezogenen Bereich mindestens ein Leistungsdurchschnitt von 4,4 erreicht wurde oder ein Ausgleich gemäß Absatz 3 möglich ist.

(3) Mangelhafte Leistungen in einem Fach des berufsübergreifenden Bereichs können durch mindestens befriedigende Leistungen in einem anderen Fach dieses Bereichs oder durch mindestens eine befriedigende Leistung des berufsbezogenen Bereichs ausgeglichen werden.
Bei einer Leistungsbewertung in Lernfeldern gemäß § 15 Absatz 2 kann die Note des berufsbezogenen Bereichs zum Ausgleich von mangelhaften Leistungen in einem Fach des berufsübergreifenden Bereichs hinzugezogen werden.
Dabei muss der gewichtete arithmetische Mittelwert gemäß der Vorgabe in § 15 Absatz 4 aus der dreifach gewichteten Note des berufsbezogenen Bereichs und der einfach gewichteten auszugleichenden mangelhaften Leistung des Faches aus dem berufsübergreifenden Bereich mindestens 4,4 ergeben.
Das Fach Sport sowie der Wahlpflichtbereich können nicht zum Ausgleich hinzugezogen werden.

#### § 18 Gleichstellung von Abschlüssen

(1) Den Erwerb gleichgestellter Abschlüsse stellt die Klassenkonferenz fest.

(2) Einen der erweiterten Berufsbildungsreife gleichgestellten Abschluss erwirbt, wer den Bildungsgang gemäß § 1 Absatz 3 Nummer 1 erfolgreich abschließt.

(3) Einen der Fachoberschulreife gleichgestellten Abschluss erwirbt, wer

1.  den erfolgreichen Abschluss einer Berufsausbildung gemäß Berufsbildungsgesetz oder Handwerksordnung in einem anerkannten Ausbildungsberuf mit einer Regelausbildungsdauer von mindestens zwei Jahren nachweist,
2.  im Abschlusszeugnis des Bildungsgangs gemäß § 1 Absatz 3 Nummer 1 einen Gesamtnotendurchschnitt von mindestens 3,0 erreicht hat sowie
3.  Fremdsprachenkenntnisse entsprechend einem mindestens fünfjährigen Fremdsprachenunterricht nachweist, der mit mindestens ausreichenden Leistungen abgeschlossen wurde.
    
    Das zuständige staatliche Schulamt kann im Einzelfall zulassen, dass der Nachweis der Fremdsprachenkenntnisse gemäß Nummer 3 durch eine Sprachfeststellungsprüfung gemäß den Bestimmungen der Eingliederungsverordnung erfolgt.
    An die Stelle der Sprachfeststellungsprüfung kann insbesondere das KMK-Fremdsprachenzertifikat im Kompetenzbereich Stufe I (A2 = Elementare Sprachverwendung) gemäß der „Rahmenvereinbarung über die Zertifizierung von Fremdsprachenkenntnissen in der beruflichen Bildung“ (Beschluss der Kultusministerkonferenz) treten.
    

### Abschnitt 3  
Leistungsbewertung

#### § 19 Leistungsbewertung

(1) Die Leistungsbewertung erfolgt gemäß den Grundsätzen in § 57 des Brandenburgischen Schulgesetzes.

(2) Leistungsnachweise sind alle im Zusammenhang mit dem Unterricht geforderten und erwarteten sowie selbstständig erbrachten Leistungen, insbesondere schriftliche Arbeiten, projektspezifische Leistungsnachweise sowie sonstige Leistungen, die sich vor allem auf den Erwerb von Fach-, Selbst- und Sozialkompetenzen beziehen.
Die Leistungen sind bei der Leistungsbewertung zu berücksichtigen und entsprechend ihrem Gewicht in die abschließende Leistungsbewertung einzubeziehen.
Die Einzelnoten der Leistungsnachweise sind Grundlage für die Gesamtnote der Fächer oder Lernfelder.
Nicht jede Leistung muss gesondert benotet werden.
Bei Gruppenarbeiten erfolgt die Leistungsbewertung für jedes Mitglied der Gruppe einzeln.

(3) Im fachübergreifenden Unterricht gemäß § 11 Absatz 1 Satz 2 des Brandenburgischen Schulgesetzes werden in den jeweils beteiligten Fächern Einzelnoten festgelegt.
Ist dies nicht möglich, wird die Gesamtnote des fachübergreifenden Unterrichts in die jeweils beteiligten Fächer übertragen.

(4) Die Teilkonferenz gemäß § 94 Absatz 4 des Brandenburgischen Schulgesetzes beschließt über die Verfahren der Leistungsbewertung im jeweiligen Lernfeld des berufsbezogenen Bereichs.

(5) Den Schülerinnen und Schülern mit einer erheblichen Sprachauffälligkeit, Sinnes- oder Körperbehinderung sind auf der Grundlage der Empfehlungen des Förderausschusses gemäß den Bestimmungen der Sonderpädagogik-Verordnung angemessene Erleichterungen zu gewähren, um Nachteile auszugleichen, die sich aus der Art und dem Umfang der jeweiligen Behinderung ergeben.
Die Leistungsanforderungen müssen den Zielsetzungen der Rahmenlehrpläne oder curricularen Materialien des besuchten Bildungsgangs entsprechen.

(6) Das Nähere zum Ausgleich von Nachteilen auf Grund einer Lese-Rechtschreib-Schwierigkeit oder einer Schwierigkeit im Rechnen wird durch Verwaltungsvorschriften geregelt.

#### § 20 Verweigerung und Täuschung bei Leistungsnachweisen

(1) Versäumt eine Schülerin oder ein Schüler aus selbst zu vertretenden Gründen die Teilnahme an einem Leistungsnachweis, so wird grundsätzlich die Note ungenügend erteilt.
Bei Verweigerung eines Leistungsnachweises wird die Note ungenügend erteilt.
Ist das Versäumnis nicht zu vertreten, wird keine Bewertung erteilt oder der Leistungsnachweis zu einem anderen Termin nachgeholt.

(2) Wer sich bei einer Leistungsfeststellung unerlaubter Hilfen bedient, begeht eine Täuschung.
Dies gilt auch für Täuschungsversuche sowie Beihilfe zur Täuschung.
Art und Umfang der Täuschung sind von der aufsichtführenden Lehrkraft festzustellen.

(3) Die Lehrkraft entscheidet, ob bei geringerer Schwere der Täuschung der ohne Täuschung geleistete Teil des Leistungsnachweises bewertet und der übrige Teil als nicht geleistet gewertet wird.
Bei erheblichen Täuschungen wird die gesamte Leistung mit ungenügend bewertet.
Lässt sich der Umfang der Täuschung nicht eindeutig feststellen, wird der Leistungsnachweis wiederholt.

### Abschnitt 4  
Bildungsgänge zur Vertiefung der Allgemeinbildung und zur Berufsorientierung, Berufsvorbereitung oder Berufsausbildungsvorbereitung

#### § 21 Organisation der Bildungsgänge

(1) In den Bildungsgängen zur Vertiefung der Allgemeinbildung und zur Berufsorientierung, Berufsvorbereitung oder Berufsausbildungsvorbereitung gemäß § 1 Absatz 3 Nummer 2 werden Schülerinnen und Schülern berufsorientierende oder berufsvorbereitende und grundlegende allgemein bildende Bildungsinhalte vermittelt.
Die Bildungsgänge dauern in der Regel ein Schuljahr.

(2) Der Unterricht ist auf der Grundlage der Rahmenstundentafeln gemäß Anlage 2 für die Bildungsgänge im berufsvorbereitenden und berufsübergreifenden Bereich durchzuführen.

(3) Der Unterricht wird in Teilzeitform erteilt.
Er umfasst 7 bis 16 Unterrichtsstunden pro Woche.
Die Unterrichtsorganisation erfolgt im Benehmen mit dem Träger der Maßnahme.

(4) Die Schülerinnen und Schüler werden vom Maßnahmenträger angemeldet.

#### § 22 Klassenbildung

(1) Für Schülerinnen und Schüler mit einem Arbeitsvertrag werden entsprechend der Schülerzahl eigene Klassen gebildet, wobei sich die Fächer an den jeweiligen beruflichen Tätigkeiten und Interessen orientieren.
Kann keine eigene Klasse gebildet werden, erfolgt die Aufnahme in der Regel in einer dafür geeigneten Klasse des Bildungsgangs gemäß § 1 Absatz 3 Nummer 1 unter Berücksichtigung der Art der beruflichen Tätigkeit.

(2) Für Schülerinnen und Schüler in berufsvorbereitenden Lehrgängen der Bundesagentur für Arbeit sollen entsprechend dem unterschiedlichen Bedarf an schulischer Förderung jeweils eigene Klassen oder Lerngruppen gebildet werden.
Die schulische Förderung kann bei entsprechendem Bedarf auch sozialpädagogische Förderungen gemäß § 9 des Brandenburgischen Schulgesetzes umfassen oder durch präventive Maßnahmen von Lehrkräften Sonderpädagogischer Förder- und Beratungsstellen gemäß § 29 Absatz 4 Satz 1 des Brandenburgischen Schulgesetzes ergänzt werden.

#### § 23 Noten, Zeugnisse, Abschluss des Bildungsgangs

(1) Die Abschlussnoten werden aus dem Mittelwert der Noten aus den Halbjahres- oder Jahreszeugnissen unter Berücksichtigung der Leistungsentwicklung gebildet.

(2) Die Schülerinnen und Schüler erhalten am Ende eines jeden Schuljahres ein Zeugnis.
Beim einjährigen Bildungsgang ist dieses das Abgangs- oder Abschlusszeugnis.
Ein Abschlusszeugnis erhält, wer den jeweiligen Bildungsgang erfolgreich abschließt.
Das Abschlusszeugnis trägt das Datum des letzten Unterrichtstages und wird am letzten Unterrichtstag ausgegeben.
Ein Abgangszeugnis erhält, wer den Bildungsgang verlässt, ohne dass das Ziel des Bildungsgangs erreicht wurde.
Das Zeugnis trägt das Datum der Beendigung des Schulverhältnisses.

(3) Das Ziel des Bildungsgangs ist erreicht, wenn im Durchschnitt aller Fächer der Stundentafel, mit Ausnahme des Faches Sport, mindestens ausreichende Leistungen erreicht wurden oder ein Ausgleich möglich ist.
Mangelhafte Leistungen in bis zu zwei Fächern können durch jeweils mindestens befriedigende Leistungen ausgeglichen werden, wenn die zum Ausgleich hinzugezogenen Fächer über die gleiche Jahresstundenzahl wie die auszugleichenden Fächer verfügen.
Das Fach Sport kann nicht zum Ausgleich hinzugezogen werden.

#### § 24 Gleichstellung von Abschlüssen

Der erfolgreiche Abschluss schließt einen der Berufsbildungsreife gleichgestellten Abschluss ein, wenn in den Fächern Deutsch und Mathematik des Ergänzungsunterrichts gemäß Buchstabe A Nummer 3 der Anlage 2 mindestens ausreichende Leistungen erzielt wurden.

### Abschnitt 5  
Sonderpädagogische Förderung in den Bildungsgängen der Berufsschule

#### § 25 Ziel und Dauer

Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf besuchen die Bildungsgänge gemäß § 1 Absatz 3 Nummer 1 und 2 entsprechend dem Ziel und der Dauer des Bildungsgangs.

#### § 26 Aufnahmevoraussetzungen

(1) Vor der Aufnahme von Schülerinnen und Schülern mit den Förderschwerpunkten „körperliche und motorische Entwicklung“, „Hören“ oder „Sehen“ und mit dem sonderpädagogischen Förderbedarf im autistischen Verhalten in den gemeinsamen Unterricht oder in eine Klasse für Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf (Förderklasse) an einem Oberstufenzentrum muss ein Verfahren gemäß Abschnitt 3 der Sonderpädagogik-Verordnung durchgeführt werden.

(2) Das Feststellungsverfahren gemäß Absatz 1 entfällt, wenn wegen des Besuchs einer sonstigen Rehabilitationseinrichtung die Feststellung des sonderpädagogischen Förderbedarfs bereits durch ein von der Bundesagentur für Arbeit veranlasstes Verfahren vorgenommen wurde.

#### § 27 Klassenbildung

Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf besuchen in der Regel im gemeinsamen Unterricht die Klassen in den Bildungsgängen gemäß § 1 Absatz 3 Nummer 1 und 2.
Mit Genehmigung des für Schule zuständigen Ministeriums können auch Förderklassen in den Förderschwerpunkten „körperliche und motorische Entwicklung“, „Hören“ oder „Sehen“ eingerichtet werden.

#### § 28 Sonderpädagogische Förderung und Begleitung

Lehrkräfte der Sonderpädagogischen Förder- und Beratungsstellen unterstützen den gemeinsamen Unterricht von Schülerinnen und Schülern mit den Förderschwerpunkten „körperliche und motorische Entwicklung“, „Hören“ oder „Sehen“ und mit dem sonderpädagogischen Förderbedarf im autistischen Verhalten in den Bildungsgängen gemäß § 1 Absatz 3 Nummer 1 und 2.

### Abschnitt 6  
Schlussbestimmungen

#### § 29 Übergangsvorschriften

Für Schülerinnen und Schüler, die sich vor Inkrafttreten dieser Verordnung in einem Bildungsgang gemäß § 1 Absatz 3 Nummer 1 befinden, ist § 15 Absatz 5 nicht anzuwenden.

#### § 30 Inkrafttreten, Außerkrafttreten

(1) Diese Verordnung tritt am 1.
August 2016 in Kraft.

(2) Gleichzeitig treten

1.  die [Berufsschulverordnung vom 5.
    April 2002](/verordnungen/bsv_2008 "Link zur Berufsschulverordnung - Archiv") (GVBl.
    II S. 335), die durch Verordnung vom 11. August 2008 (GVBl.
    II S. 334) geändert worden ist, und
2.  die Berufsfachschulverordnung Berufsabschluss nach BBiG oder HwO vom 3. Juli 1997 (GVBl. II S. 610), die zuletzt durch Verordnung vom 25. Juni 2004 (GVBl. II S. 502) geändert worden ist,

außer Kraft.

Potsdam, den 28.
April 2016

Der Minister für Bildung,  
Jugend und Sport

Günter Baaske

### Anlagen

1

[Anlage 1 - Rahmenstundentafel des Bildungsgangs zur Vermittlung des schulischen Teils einer Berufsausbildung gemäß Berufsbildungsgesetz oder Handwerksordnung](/br2/sixcms/media.php/68/GVBl_II_21_2016-Anlage-1.pdf "Anlage 1 - Rahmenstundentafel des Bildungsgangs zur Vermittlung des schulischen Teils einer Berufsausbildung gemäß Berufsbildungsgesetz oder Handwerksordnung") 603.8 KB

2

[Anlage 2 - Rahmenstundentafeln für die Bildungsgänge zur Vertiefung der Allgemeinbildung und zur Berufsorientierung, Berufsvorbereitung oder Berufsausbildungsvorbereitung](/br2/sixcms/media.php/68/GVBl_II_21_2016-Anlage-2.pdf "Anlage 2 - Rahmenstundentafeln für die Bildungsgänge zur Vertiefung der Allgemeinbildung und zur Berufsorientierung, Berufsvorbereitung oder Berufsausbildungsvorbereitung") 631.1 KB