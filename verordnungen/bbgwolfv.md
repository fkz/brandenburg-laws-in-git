## Verordnung über die Zulassung von Ausnahmen von den Schutzvorschriften für den Wolf (Brandenburgische Wolfsverordnung - BbgWolfV)

Auf Grund des § 45 Absatz 7 Satz 4 in Verbindung mit Satz 1 Nummer 1, 2, 4 und 5 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) und des § 17 der Bundesartenschutzverordnung vom 16.
Februar 2005 (BGBl.
I S.
258, 896) in Verbindung mit § 30 Absatz 4 Satz 1 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 1 Absatz 2 Satz 2 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nr.
43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1  
Verscheuchen von Wölfen \[Canis lupus\]

Soweit Wölfe hierbei nicht verletzt werden, unterliegt das Verscheuchen von Wölfen, die sich Menschen oder Weidetieren annähern oder in geschlossene Ortslagen von Dörfern und Städten eingedrungen sind oder sich in deren unmittelbarer Nähe aufhalten, nicht den Verboten des § 44 Absatz 1 Nummer 1 des Bundesnaturschutzgesetzes; zulässig ist auch das Werfen mit Gegenständen oder Ähnliches.
Das Nachstellen und Aufsuchen von Wölfen mit dem Ziel, sie zu verscheuchen, ist gemäß § 44 Absatz 1 Nummer 1 des Bundesnaturschutzgesetzes verboten, soweit in § 2 nichts anderes bestimmt ist.

#### § 2  
Vergrämung von Wölfen mit auffälligem Verhalten

(1) Im Interesse der Gesundheit des Menschen wird nach § 7 berechtigten Personen nach Maßgabe dieser Verordnung gestattet, Wölfen mit auffälligem Verhalten nachzustellen und sie zu vergrämen.
Zur Vergrämung zugelassen sind alle geeigneten Methoden und Geräte, einschließlich Gummigeschossen, Warn- oder Schreckschüssen, künstlichen Lichtquellen, Spiegeln oder anderen beleuchtenden oder blendenden Vorrichtungen sowie akustischen, elektrischen oder elektronischen Geräten, sofern den Wölfen hierdurch keine Verletzungen zugefügt werden, die über kleine Hautwunden oder Hämatome hinausgehen.

(2) Maßnahmen nach Absatz 1 sind nur zulässig, wenn das Landesamt für Umwelt als Fachbehörde für Naturschutz und Landschaftspflege zuvor bestätigt hat, dass ein auffälliges Verhalten vorliegt.
Ein solches Verhalten ist insbesondere dann anzunehmen, wenn sich ein Wolf

1.  wiederholt Menschen außerhalb von Fahrzeugen bis auf eine Entfernung von wenigen Metern aktiv annähert und es sich nicht um einen Welpen handelt,
2.  tagsüber wiederholt in geschlossenen Ortslagen von Dörfern und Städten oder
3.  über mehrere Tage hintereinander in der unmittelbaren Nähe von Siedlungsbereichen aufhält

und er sich nicht durch nach § 1 zulässige Maßnahmen verscheuchen lässt.

(3) Sofern die Fachbehörde für Naturschutz und Landschaftspflege dies für erforderlich hält, dürfen die in § 7 genannten Personen Wölfe mit auffälligem Verhalten auch mit Fallen fangen und betäuben oder mittels Betäubung durch Teleinjektionsgeräte der Natur entnehmen, um sie zu besendern oder anderweitig zu kennzeichnen und anschließend bei oder nach ihrer Wiederfreilassung gezielt zu vergrämen.

#### § 3  
Tötung von Wölfen mit für den Menschen problematischem oder aggressivem Verhalten

(1) Im Interesse der Gesundheit des Menschen wird nach § 7 berechtigten Personen nach Maßgabe dieser Verordnung gestattet, Wölfen mit für den Menschen problematischem Verhalten nachzustellen und mit einer geeigneten Schusswaffe tierschutzgerecht zu töten.
Ein für den Menschen problematisches Verhalten liegt vor, wenn die Vergrämung eines nach § 2 Absatz 2 Satz 2 auffälligen Wolfes nach Einschätzung der Fachbehörde für Naturschutz und Landschaftspflege nicht möglich ist oder die Vergrämung erfolglos bleibt.

(2) Ist ein Abschuss nach Absatz 1 nicht möglich, dürfen Wölfe mit für den Menschen problematischem Verhalten von nach § 7 berechtigten Personen auch mit Fallen gefangen oder mit einem Narkosegewehr oder sonstigen Teleinjektionsgeräten betäubt und der Natur entnommen werden.
Nach Satz 1 der Natur entnommene Wölfe sind durch einen Tierarzt oder eine andere zur Tötung von Wirbeltieren berechtigte Person tierschutzgerecht zu töten, sofern bei Welpen eine artgerechte Unterbringung nicht in Frage kommt.

(3) Im Interesse der Gesundheit des Menschen dürfen Wölfe, die sich aggressiv gegenüber Menschen verhalten, von nach § 7 berechtigten Personen auch ohne vorherige Vergrämung oder den Versuch der Vergrämung gemäß Absatz 1 geschossen werden.
Absatz 2 gilt entsprechend.
§ 10 Absatz 1 in Verbindung mit den §§ 1 und 2 des Brandenburgischen Polizeigesetzes bleibt unberührt.

#### § 4  
Ausnahmen zur Abwendung von Übergriffen auf Nutztiere

(1) Zur Abwendung drohender erheblicher landwirtschaftlicher Schäden wird nach § 7 berechtigten Personen nach Maßgabe dieser Verordnung gestattet, Wölfen nachzustellen und mit einer geeigneten Schusswaffe tierschutzgerecht zu töten.
§ 3 Absatz 2 gilt entsprechend.

(2) Maßnahmen nach Absatz 1 sind nur zulässig, wenn ein oder mehrere Wölfe mehrfach in Weidetierbestände eingedrungen sind, die nach den in der Anlage aufgeführten „Zumutbaren Maßnahmen zum Schutz von Weidetierbeständen vor Wolfsübergriffen“ geschützt waren, und dort Nutztiere gerissen oder verletzt haben.
Als mehrfaches Eindringen gilt das mindestens zweimalige Eindringen in denselben Weidetierbestand oder das mindestens zweimalige Eindringen in verschiedene Weidetierbestände durch mutmaßlich denselben Wolf oder mutmaßlich dieselben Wölfe.

(3) Soweit Übergriffe auf nach Absatz 2 geschützte Nutztiere anders nicht beendet werden können, ist es zulässig, auch das gesamte Rudel zu entnehmen oder zu töten.

#### § 5  
Wolfs-Hund-Hybriden

Ergibt das Monitoring der Fachbehörde für Naturschutz und Landschaftspflege das Vorkommen von Hybriden zwischen Wolf und Hund (Wolfshybriden) wird nach § 7 berechtigten Personen nach Maßgabe dieser Verordnung gestattet, den Hybriden zum Schutz der heimischen Tierwelt nachzustellen, um sie zu fangen, auf sonstige Weise lebend der Natur zu entnehmen oder durch Abschuss zu töten.
Nach Satz 1 lebend der Natur entnommene Wolfshybriden sind tierschutzgerecht zu töten, soweit eine artgerechte Unterbringung im Einzelfall nicht möglich ist.

#### § 6  
Einschränkungen

(1) Bei der Entnahme oder Tötung von Wölfen oder Wolfshybriden nach dieser Verordnung sind die tierschutzrechtlichen Vorgaben zu beachten.
Insbesondere dürfen

1.  beim Fallenfang nach § 2 Absatz 3, § 3 Absatz 2 oder § 5 nur von der Fachbehörde für Naturschutz und Landschaftspflege hierzu bereit gestellte oder empfohlene Fallen verwendet werden, die unversehrt fangen und das unbeabsichtigte Fangen von sonstigen wild lebenden Tieren nach Möglichkeit ausschließen;
2.  keine Wölfe oder Wolfshybriden mit unselbstständigen Jungtieren geschossen oder der Natur entnommen werden, es sei denn, dass das verbleibende Elterntier nach Einschätzung der Fachbehörde für Naturschutz und Landschaftspflege allein in der Lage ist, die Jungen aufzuziehen oder, wenn dies nicht der Fall ist oder beide Elterntiere entnommen werden müssen, die Jungtiere vor den Elterntieren getötet oder der Natur entnommen werden; soweit dies im Einzelfall möglich ist, sind die Jungtiere lebend der Natur zu entnehmen und artgerecht unterzubringen;
3.  bei der Tötung von Wölfen oder Wolfshybriden mit Schusswaffen nur Patronen mit ausreichender Tötungswirkung verwendet werden.

Satz 2 Nummer 2 gilt nicht für Wölfe nach § 3 Absatz 3 oder bei denen nach Einschätzung der Fachbehörde für Naturschutz und Landschaftspflege aufgrund ihres sonstigen Verhaltens eine akute Gefahr für die Gesundheit von Menschen nicht ausgeschlossen werden kann oder bei der Tötung schwer verletzter Wölfe nach § 9.

(2) Die Durchführung von Maßnahmen nach den §§ 2 bis 5 ist in Naturschutzgebieten und im Nationalpark Unteres Odertal sowie in Gebieten, die als Naturschutzgebiet einstweilig sichergestellt sind oder gemäß § 11 des Brandenburgischen Naturschutzausführungsgesetzes einer Veränderungssperre zwecks Ausweisung als Naturschutzgebiet unterliegen, nur zulässig, wenn die Maßnahme nicht nach der jeweiligen Schutzgebietsverordnung oder dem jeweiligen Gesetz verboten ist oder wenn für die Maßnahme durch den jeweiligen Landkreis beziehungsweise die jeweilige kreisfreie Stadt als zuständige Naturschutzbehörde nach § 1 Absatz 1 der Naturschutzzuständigkeitsverordnung eine flächenschutzrechtliche Befreiung nach § 67 des Bundesnaturschutzgesetzes gewährt worden ist.
§ 3 Absatz 3 Satz 3 gilt entsprechend.

(3) In Gebieten von gemeinschaftlicher Bedeutung nach § 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes und in Europäischen Vogelschutzgebieten nach § 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes ist die Durchführung von Maßnahmen nach dieser Verordnung zulässig, wenn die Fachbehörde für Naturschutz und Landschaftspflege durch eine Prüfung nach § 34 Absatz 1 des Bundesnaturschutzgesetzes festgestellt hat, dass eine erhebliche Beeinträchtigung von Erhaltungszielen des jeweiligen Gebietes bei der Durchführung der Maßnahmen ausgeschlossen werden kann.
§ 3 Absatz 3 Satz 3 gilt entsprechend.

(4) Die Fachbehörde für Naturschutz und Landschaftspflege kann in den in Absatz 3 genannten Gebieten im Einzelfall Ausnahmen nach § 34 Absatz 3 des Bundesnaturschutzgesetzes zulassen, wenn eine erhebliche Beeinträchtigung von Erhaltungszielen des jeweiligen Gebietes bei der Durchführung der Maßnahmen nicht ausgeschlossen werden kann.

#### § 7  
Berechtigte Personen

(1) Zu Maßnahmen nach den §§ 2 bis 5 sind nur dazu geeignete Personen berechtigt, die von der Fachbehörde für Naturschutz und Landschaftspflege nach Prüfung des Vorliegens der Voraussetzungen dieser Verordnung im Einzelfall zur Durchführung der jeweiligen Maßnahmen bestellt wurden.
Zusammen mit der Beauftragung legt die Fachbehörde die genauen zeitlichen und örtlichen Umstände bei der Durchführung der Maßnahmen fest.

(2) Zur Vergrämung von Wölfen nach § 2 mit Gummigeschossen, Warn- oder Schreckschüssen sowie zur Tötung von Wölfen oder Wolfshybriden mit einer Schusswaffe nach den §§ 3 bis 5 darf nur bestellt werden, wer einen gültigen Jagdschein oder eine andere waffenrechtliche Erlaubnis besitzt.
Bei der Bestellung von Personen zur Tötung von Wölfen nach § 4 sind vorrangig die in dem jeweiligen Bereich jagdausübungsberechtigten Personen zu berücksichtigen.
Soweit Maßnahmen nach Satz 1 nicht durch die in dem jeweiligen Bereich jagdausübungsberechtigte Person erfolgen, ist diese nach Möglichkeit vorab zu informieren.
Besteht keine Möglichkeit, die jagdausübungsberechtigte Person vor Durchführung der Maßnahmen zu informieren, hat die Information nachträglich zu erfolgen.

(3) Zur Entnahme von Wölfen oder Wolfshybriden mit betäubenden Mitteln nach § 2 Absatz 3, § 3 Absatz 2 oder § 5 dürfen nur Tierärzte oder Personen, die über eine Ausnahme nach § 5 Absatz 1 Satz 5 des Tierschutzgesetzes verfügen, bestellt werden.
Zur Entnahme von Wölfen mit einem Narkosegewehr darf zusätzlich nur bestellt werden, wer die waffenrechtlichen Voraussetzungen erfüllt.

#### § 8  
Informations- und Beobachtungspflichten

(1) Der Fachbehörde für Naturschutz und Landschaftspflege hat unverzüglich Bericht zu erstatten, wer von

1.  § 2 Gebrauch gemacht hat, über die Anzahl der vergrämten Wölfe unter Angabe des genauen Ortes und Datums und der angewandten Methode;
2.  den §§ 3 bis 5 Gebrauch gemacht hat, über den genauen Entnahme- oder Abschussort, das genaue Entnahme- oder Abschussdatum und die Anzahl der jeweils entnommenen oder getöteten Wölfe oder Wolfshybriden.

(2) Die Fachbehörde für Naturschutz und Landschaftspflege hat die örtlich zuständige untere Naturschutzbehörde zu informieren, wenn in ihrem Bereich ein Wolf mit für den Menschen problematischem Verhalten bestätigt wurde.
Beim Auftreten eines Wolfes, der sich ohne ersichtlichen Grund aggressiv gegenüber Menschen verhält, sind zusätzlich die örtlich zuständigen Polizeidienststellen und Kommunen zu informieren.

(3) Die Fachbehörde für Naturschutz und Landschaftspflege hat darüber zu wachen, dass es weder zu einer Verschlechterung des Erhaltungszustands der Populationen des Wolfs kommt noch die Wiederherstellung eines günstigen Erhaltungszustands des Wolfs in der kontinentalen Region Deutschlands behindert wird.
Die Verordnung ist aufzuheben, falls sich eine Verschlechterung des Erhaltungszustands der Populationen des Wolfs abzeichnen sollte oder die Wiederherstellung eines günstigen Erhaltungszustands des Wolfs in der kontinentalen Region Deutschlands behindert wird.

(4) Die Fachbehörde für Naturschutz und Landschaftspflege hat außerdem darüber zu wachen, dass die Vorschriften dieser Verordnung eingehalten werden.
Sie kann nach pflichtgemäßem Ermessen die im Einzelfall erforderlichen Maßnahmen treffen, um deren Einhaltung sicherzustellen.
Sie kann insbesondere die Befugnisse nach den §§ 1 bis 4 im Einzelfall entziehen, wenn von ihnen in missbräuchlicher Weise Gebrauch gemacht, der Berichtspflicht nach § 8 oder der Abgabepflicht nach § 10 nicht nachgekommen wird.

#### § 9  
Tötung schwer verletzter Wölfe

(1) Schwer verletzte Wölfe dürfen aus zwingenden Gründen des überwiegenden öffentlichen Interesses von einer Tierärztin oder einem Tierarzt getötet werden, wenn das Tier nach dem Urteil der Tierärztin oder des Tierarztes nicht oder nur unter nicht behebbaren erheblichen Leiden oder Schmerzen weiterleben könnte.
Die Tötung darf in Beisein der Tierärztin oder des Tierarztes mit einer geeigneten Schusswaffe auch durch Polizeibeamte oder durch Personen erfolgen, die im Besitz eines gültigen Jagdscheins sind, sofern die Tierärztin oder der Tierarzt in der konkreten Situation nicht hierzu in der Lage ist.

(2) Bei Verletzungen, die so schwerwiegend sind, dass ein Überleben bei vernünftigem menschlichen Ermessen ausgeschlossen ist, dürfen Polizeibeamte oder von der Polizei hierzu hinzugezogene Jagdscheininhaberinnen oder Jagdscheininhaber einen schwer verletzten und leidenden Wolf auch dann töten, wenn eine Tierärztin oder ein Tierarzt zeitnah nicht hinzugezogen werden kann (Nottötung von Wölfen).

(3) § 8 Absatz 1 gilt entsprechend.

#### § 10  
Verbleib getöteter Wölfe

Getötete Wölfe oder Wolfshybriden sind der Fachbehörde für Naturschutz und Landschaftspflege für wissenschaftliche Untersuchungen zu übergeben.

#### § 11  
Evaluation

Das für den Erlass dieser Verordnung zuständige Mitglied der Landesregierung hat die Verordnung drei Jahre nach ihrem Inkrafttreten zu überprüfen und bei Bedarf anzupassen.

#### § 12  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 26.
Januar 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage (zu § 4 Absatz 2) - Zumutbare Maßnahmen zum Schutz von Weidetierbeständen vor Wolfsübergriffen](/br2/sixcms/media.php/68/GVBl_II_08_2018-Anlage.pdf "Anlage (zu § 4 Absatz 2) - Zumutbare Maßnahmen zum Schutz von Weidetierbeständen vor Wolfsübergriffen") 538.0 KB