## Verordnung zur Bestimmung der Gebiete mit Mietpreisbegrenzung (Mietpreisbegrenzungsverordnung - MietbegrenzV)

Auf Grund des § 556d Absatz 2 des Bürgerlichen Gesetzbuchs in der Fassung der Bekanntmachung vom 2. Januar 2002 (BGBl.
I S. 42, 2909; 2003 I S.
738), der zuletzt durch Artikel 1 des Gesetzes vom 19. März 2020 (BGBl.
I S.
540) geändert worden ist, verordnet die Landesregierung:

#### § 1 Gebiete

Gemeinden im Sinne des § 556d Absatz 2 des Bürgerlichen Gesetzbuchs, in denen die ausreichende Versorgung der Bevölkerung mit Mietwohnungen zu angemessenen Bedingungen besonders gefährdet ist und in denen die Miete zu Beginn des Mietverhältnisses die ortsübliche Vergleichsmiete (§ 558 Absatz 2 des Bürgerlichen Gesetzbuchs) höchstens um 10 Prozent übersteigen darf, sind:

**Gemeinde**

**Kreisfreie Stadt**

Potsdam

**In den Landkreisen**

Barnim

Panketal

Dahme-Spreewald

Eichwalde  
Schulzendorf

Havelland

Falkensee

Märkisch-Oderland

Hoppegarten  
Neuenhagen bei Berlin

Oberhavel

Birkenwerder  
Glienicke/Nordbahn  
Hohen Neuendorf  
Mühlenbecker Land

Oder-Spree

Gosen-Neu Zittau  
Schöneiche bei Berlin  
Woltersdorf

Potsdam-Mittelmark

Kleinmachnow  
Stahnsdorf  
Teltow

Teltow-Fläming

Blankenfelde-Mahlow  
Großbeeren

#### § 2 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2021 in Kraft und mit Ablauf des 31. Dezember 2025 außer Kraft.

Potsdam, den 8.
Juni 2021

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Minister für Infrastruktur und Landesplanung

Guido Beermann

### Anlagen

1

[Anlage 1](/br2/sixcms/media.php/68/GVBl_II_61_2021%20Anlage%201.pdf "Anlage 1") 1.7 MB

2

[Anlage 2](/br2/sixcms/media.php/68/GVBl_II_61_2021%20Anlage%202.pdf "Anlage 2") 1.9 MB

3

[Anlage 2 - Austauschseite](/br2/sixcms/media.php/68/GVBl_II_61_2021%20Anlage%202%20-%20Austauschseite.pdf "Anlage 2 - Austauschseite") 248.6 KB

4

[Anlage 2 - Legende](/br2/sixcms/media.php/68/GVBl_II_61_2021%20Anlage%202%20-%20Legende.pdf "Anlage 2 - Legende") 140.7 KB

5

[Anlage 3](/br2/sixcms/media.php/68/GVBl_II_61_2021%20Anlage%203.pdf "Anlage 3") 489.9 KB

6

[Anlage 4](/br2/sixcms/media.php/68/GVBl_II_61_2021%20Anlage%204.pdf "Anlage 4") 3.3 MB

7

[Begründung](/br2/sixcms/media.php/68/GVBl_II_61_2021%20-%20Begr%C3%BCndung.pdf "Begründung") 474.8 KB