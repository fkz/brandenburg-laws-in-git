## Verordnung über das Naturschutzgebiet „Körbaer Teich und Niederungslandschaft am Schweinitzer Fließ“

Auf Grund des § 22 Absatz 1 und 2, der §§ 23 und 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542), von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl.
I S.
1972, 1974) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl. II Nr.
43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche in den Landkreisen Teltow-Fläming und Elbe-Elster wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Körbaer Teich und Niederungslandschaft am Schweinitzer Fließ“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 222 Hektar.
Es umfasst vier Teilflächen in folgenden Fluren:

Landkreis:

Stadt/Gemeinde:

Gemarkung:

Flur:

Teltow-Fläming

Dahme/Mark

Dahme

8;

Ihlow

Bollensdorf

1, 2;

Mehlsdorf

3, 4;

Niederer Fläming

Meinsdorf

10, 11;

Elbe-Elster

Lebusa

Körba

2;

Lebusa

1, 3;

Schönewalde

Freywalde

3;

Knippelsdorf

1, 3;

Schönewalde (S)

4.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebiets mit den Teilgebieten „Park Bärwalde - Bärwalder Busch“ (Teilfläche 1 mit rund 65 Hektar), „Mittelbusch und angrenzende Bereiche“ (Teilfläche 2 mit rund 15 Hektar), „Mehlsdorfer Busch“ (Teilfläche 3 mit rund 28 Hektar) und „Körbaer Teich mit Hundezagel und Moorwälder am Torfgraben“ (Teilfläche 4 mit rund 114 Hektar) ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte Übersichtskarte im Maßstab 1 : 30 000 und die in Anlage 2 Nummer 2 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 4 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 3 mit den Blattnummern 1 bis 7 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie bei den Landkreisen Teltow-Fläming und Elbe-Elster, untere Naturschutzbehörden, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes, das sich aus vier besonders artenreichen und naturnahen Niederungsbereichen am Schweinitzer Fließ zusammensetzt, ist

1.  die Erhaltung, Wiederherstellung und Entwicklung als Lebensraum wild lebender Pflanzengesellschaften, insbesondere von stehenden und fließenden Gewässern und deren Verlandungsbereichen mit ihren verschiedenen Sumpf- und Röhrichtgesellschaften, Feucht- und Nasswiesen mit deren Brachen, von artenreichen Säumen, Gehölzgruppen sowie von naturnahen Moor-, Laub- und Laubmischwäldern;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Wasserfeder (Hottonia palustris), Sumpf-Schwertlilie (Iris pseudacorus), Gelbe Teichrose (Nuphar lutea) und Kleines Tausendgüldenkraut (Centaurium pulchellum);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Säugetiere, Vögel, Amphibien und Reptilien darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Großer Abendsegler (Nyctalus noctula), Rauhautfledermaus (Pipistrellus nathusii), Wasserfledermaus (Myotis daubentonii), Braunes Langohr (Plecotus auritus), Kleiner Bartfledermaus (Myotis mystacinus), Großer Bartfledermaus (Myotis brandtii) und Fransenfledermaus (Myotis nattereri), Sperber (Accipiter nisus), Drosselrohrsänger (Acrocephalus arundinaceus), Grünspecht (Picus viridis), Heidelerche (Lullula arborea), Schwarzspecht (Dryocopus martius), Seeadler (Haliaeetus albicilla), Ortolan (Emberiza hortulana), Bekassine (Gallinago gallinago), Kiebitz (Vanellus vanellus), Kranich (Grus grus), Eisvogel (Alcedo atthis), Neuntöter (Lanius collurio) und Rohrschwirl (Locustella luscinioides), Knoblauchkröte (Pelobates fuscus), Laubfrosch (Hyla arborea), Moorfrosch (Rana arvalis) und Zauneidechse (Lacerta agilis);
4.  die Erhaltung von Niederungsbereichen des Schweinitzer Fließes, die durch den Körbaer Teich, die naturnahen Waldbestände sowie die ausgedehnten, feuchten Grünland- und Gehölzbestände eine besondere Vielfalt, Eigenart und hervorragende Schönheit aufweisen;
5.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Bestandteil des regionalen Biotopverbundes entlang des Schweinitzer Fließes.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Körbaer Teich und Niederungslandschaft am Schweinitzer Fließ“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), das ehemals Teile der Gebiete von gemeinschaftlicher Bedeutung „Schweinitzer Fließ“ und „Schweinitzer Fließ Ergänzung“ umfasste, mit seinen Vorkommen von

1.  Oligo- bis mesotrophen stehenden Gewässern mit Vegetation der Littorelletea uniflorae und/oder der Isoëto-Nanojuncetea, Flüssen der planaren Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Feuchten Hochstaudenfluren der planaren und montanen bis alpinen Stufe, Mageren Flachland-Mähwiesen (Alopecurus pratensis, Sanguisorba officinalis), Subatlantischem oder mitteleuropäischem Stieleichenwald oder Hainbuchenwald (Carpinion betuli) und von Alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur als natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Moorwäldern sowie von Auenwäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion, Alnion incanae, Salicion albae) als prioritären natürlichen Lebensraumtypen im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
3.  Biber (Castor fiber), Fischotter (Lutra lutra), Bechsteinfledermaus (Myotis bechsteinii), Mopsfledermaus (Barbastella barbastellus), Schlammpeitzger (Misgurnus fossilis), Bitterling (Rhodeus amarus), Bauchiger Windelschnecke (Vertigo moulinsiana) und Schmaler Windelschnecke (Vertigo angustior) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume;
4.  Eremit (Osmoderma eremita) als prioritärer Art im Sinne von § 7 Absatz 2 Nummer 11 des Bundesnaturschutzgesetzes, einschließlich seiner für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten ab dem 1. August eines jeden Jahres außerhalb von Moorwäldern zum Zweck der Erholung und zum nichtgewerblichen Sammeln von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 7;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit motorbetriebenen Fahrzeugen außerhalb der für den öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
12.  innerhalb des in der gemäß § 2 genannten topografischen Karte gekennzeichneten Bereichs zu baden;
13.  innerhalb des in der gemäß § 2 genannten topografischen Karte gekennzeichneten Bereichs Boote einzusetzen und mit Booten anzulegen;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel aller Art zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland umzubrechen, nachzusäen oder neu anzusäen;
25.  Verlandungsbereiche, Röhrichte und Schwimmblattgesellschaften zu betreten oder zu befahren.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle, Jauche, Gärreste und Sekundärrohstoffdünger einzusetzen.
          
          
        Sekundärrohstoffdünger im Sinne dieser Verordnung sind Abwasser, Fäkalien, Klärschlamm und ähnliche Stoffe aus Siedlungsabfällen und vergleichbare Stoffe aus anderen Quellen, jeweils auch weiterbehandelt und in Mischungen untereinander oder mit Düngemitteln, Wirtschaftsdünger, Bodenhilfsstoffen, Kultursubstraten und Pflanzenhilfsmitteln,
    2.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt.
        Bei Narbenschäden ist eine umbruchlose Nachsaat zulässig;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Bäume mit Horsten oder Höhlen sowie Habitate des Eremiten nicht gefällt werden,
    2.  nur Arten der potenziell natürlichen Vegetation in lebensraumtypischer Zusammensetzung eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
    3.  auf den Flächen naturnaher Wälder gemäß § 3 Absatz 1 Nummer 1 sowie den in § 3 Absatz 2 genannten Waldlebensraumtypen eine dauerwaldartige Nutzung einzelstamm- bis gruppenweise erfolgt.
        In den übrigen Wäldern und Forsten sind Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von 0,5 Hektar zulässig,
    4.  in den in § 3 Absatz 2 genannten Waldlebensraumtypen mindestens fünf lebensraumtypische Stämme je Hektar mit einem Brusthöhendurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum Zerfall aus der Nutzung genommen sein müssen.
        Die Bäume sind dauerhaft zu markieren,
    5.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 30 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden und liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimetern am stärksten Ende) dauerhaft im Bestand verbleiben.
        Sofern diese nicht vorhanden sind, sind sie zu entwickeln,
    6.  das Befahren des Waldes nur auf Waldwegen und Rückegassen erfolgt,
    7.  zum Erhalt der Bodenfunktionen hydromorphe Böden sowie Böden mit einem hohen Anteil an feinkörnigem Substrat nur bei ausreichender Tragfähigkeit auf dauerhaft festgelegten Rückegassen befahren werden,
    8.  § 4 Absatz 2 Nummer 17 und 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang mit der Maßgabe, dass
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters und des Bibers weitgehend ausgeschlossen ist,
    2.  § 4 Absatz 2 Nummer 19 gilt;
4.  die Teichbewirtschaftung, die den Anforderungen des § 5 Absatz 4 des Bundesnaturschutzgesetzes in Verbindung mit dem Fischereigesetz des Landes Brandenburg entspricht und die im Sinne der guten fachlichen Praxis gemäß den Leitlinien zur naturschutzgerechten Teichwirtschaft in Brandenburg auf den bisher rechtmäßig dafür genutzten Flächen durchgeführt wird, mit der Maßgabe, dass
    1.  § 4 Absatz 2 Nummer 11, 13, 17, 19 und 25 gilt,
    2.  der Fischbesatz im Körbaer Teich auf 200 Kilogramm pro Jahr begrenzt ist,
    3.  der Fischbesatz nur mit heimischen Arten erfolgt; § 13 der Fischereiordnung des Landes Brandenburg bleibt unberührt,
    4.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters und des Bibers weitgehend ausgeschlossen ist,
    5.  Pflegepläne einvernehmlich mit der unteren Naturschutzbehörde abzustimmen sind,
    6.  die rechtmäßige Ausübung der Angelfischerei vom Ufer und vom Boot aus nur außerhalb des in der gemäß § 2 Absatz 2 aufgeführten topografischen Karte gekennzeichneten Bereichs zulässig ist,
5.  für den Bereich der Jagd:
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        in der Zeit vom 1.
        März bis zum 31.
        August eines jeden Jahres keine Bewegungsjagd erfolgt,
        
        bb)
        
        die Fallenjagd nur mit Lebendfallen erfolgt,
        
        cc)
        
        keine Baujagd in einem Abstand von 100 Metern zu Gewässerufern vorgenommen wird.
        Ausnahmen von der Einhaltung dieses Abstands kann die untere Naturschutzbehörde erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde erfolgt.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
        Die Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        Die Entscheidung hierzu soll unverzüglich erfolgen,
    3.  der Einsatz transportabler und mobiler Ansitzeinrichtungen,
    4.  die Anlage von Kirrungen, Ablenkfütterungen und Ansaatfütterungen sowie die Anlage und Unterhaltung von Wildäckern außerhalb gesetzlich geschützter Biotope gemäß § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes und der in § 3 Absatz 2 Nummer 1 und 2 genannten Lebensraumtypen erfolgt.
        Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
6.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch außerhalb von Moorwäldern ab dem 1.
    August eines jeden Jahres;
7.  das Baden und Tauchen im Körbaer Teich außerhalb des in der gemäß § 2 Absatz 2 aufgeführten topografischen Karte gekennzeichneten Bereichs sowie das Lagern an Badestellen außerhalb des in der in § 2 Absatz 2 genannten topografischen Karte ausgewiesenen Bereichs;
8.  die Durchführung des traditionellen Dorffestes ab dem 15.
    Juli eines jeden Jahres auf der Wiese nordwestlich der Staumauer des Körbaer Teichs (Flurstücke 117/3 bis 120/3, 122/3, 124/3, 125, 532, 533, 592, Flur 2, Gemarkung Körba, Gemeinde Lebusa);
9.  die Durchführung von Natur- und Umweltbildungsveranstaltungen mit Zustimmung der unteren Naturschutzbehörde;
10.  das Betreten der Eisflächen;
11.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege.
    Die untere Naturschutzbehörde ist rechtzeitig mit dem Ziel einer einvernehmlichen Lösung zu beteiligen;
12.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
13.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
14.  die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern diese nicht unter die Nummern 11 bis 13 fallen, im Einvernehmen mit der unteren Naturschutzbehörde;
15.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
16.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
17.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
18.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen;
19.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungserfordernissen, die sich aus anderen fachrechtlichen Vorgaben ergeben, unberührt.

#### § 6 Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  zur Erhaltung der Teichbodenvegetation ist ein anteiliges Trockenfallen notwendig.
    Nach mehr als vier aufeinander folgenden Jahren mit hohem Wasserstand bei vollem Pegel soll der Körbaer Teich im Spätsommer so abgelassen werden, dass dabei mindestens ein Drittel der Teichbodenfläche trocken fällt.
    Das Wiederbespannen soll vor Einsetzen von Bodenfrösten, frühestens jedoch zum 30.
    November eines jeden Jahres erfolgen.
    Dabei sind durch das Ablassen gefährdete Fische und Muscheln in den verbleibenden Wasserkörper umzusetzen;
2.  zur Erhaltung der Teichbodenvegetation sollen Röhrichte wasserseitig im Frühsommer gemäht werden.
    Altbestände sollen nach dem 30.
    September eines jeden Jahres abschnittsweise gemäht werden.
    Im Uferbereich aufkommende Gehölze sollen entfernt werden;
3.  zum Schutz der Ufer sollen Sammelstege errichtet werden;
4.  die Insel am Südufer des Körbaer Teichs soll mit einer sie umgebenden, offenen Wasserfläche als Brutgebiet entwickelt werden;
5.  die Stauanlagen im Schweinitzer Fließ sollen saniert und in ihrer Wasserhaltung entsprechend der Erhaltungsziele gesteuert werden.
    Ziel ist die Verbesserung der Vorflutverhältnisse im oberen Einzugsgebiet des Schweinitzer Fließes;
6.  eine weitere Moorsackung des Moorwaldes soll durch Sohlschwellen im Torfgraben verhindert werden;
7.  die Randstreifen entlang des Schweinitzer Fließes sollen naturnah eingerichtet werden;
8.  bei der Bewirtschaftung der landwirtschaftlichen Nutzflächen
    1.  soll die Nutzung des Grünlands mosaikartig erfolgen; Feucht- und Nasswiesen sollen einmal pro Jahr gemäht werden.
        Der Erstnutzungstermin sollte nicht vor dem 16.
        Juni eines jeden Jahres liegen,
    2.  soll Grünland von innen nach außen gemäht werden,
    3.  sollen Flachland-Mähwiesen als zweischürige Wiesen außerhalb des Zeitraums vom 15. Juni bis zum 31.
        August eines jeden Jahres genutzt werden,
    4.  sollen die halboffenen Flächen am Hundezagel in der Gemarkung Lebusa, Flur 3, Flurstücke 194 bis 197, 201 bis 207, 615 und 616 in mehrjährigen Turnus gepflegt werden.
        Dabei soll das Mähgut entnommen werden;
9.  bei der forstlichen Bewirtschaftung der Wälder sollen
    1.  eingebürgerte, gebietsfremde Arten wie die Spätblühende Traubenkirsche entnommen oder zurückgedrängt werden,
    2.  innerhalb des Teilgebietes „Park Bärwalde - Bärwalder Busch“ potenzielle Brutbäume holzbewohnender Käferarten, wie Eremit, durch Freistellen vorzugsweise an Bestandsrändern gezielt erhalten und gefördert werden,
    3.  Nadelholzreinbestände in naturnahe Mischwälder überführt werden,
    4.  gestufte Waldränder entwickelt werden.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

§ 5 Absatz 1 Nummer 1 tritt am 1.
Januar 2019 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 13.
November 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Körbaer Teich und Niederungslandschaft am Schweinitzer Fließ“](/br2/sixcms/media.php/68/Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Körbaer Teich und Niederungslandschaft am Schweinitzer Fließ“") 396.2 KB

2

[Anlage 2 (zu § 2 Absatz 2) - Übersichtskarte, Topografische Karten, Liegenschaftskarten](/br2/sixcms/media.php/68/Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Übersichtskarte, Topografische Karten, Liegenschaftskarten") 163.7 KB

3

[Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Körbaer Teich und Niederungslandschaft am Schweinitzer Fließ“](/br2/sixcms/media.php/68/GVBl_II_79_2018-Anlage-3.pdf "Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Körbaer Teich und Niederungslandschaft am Schweinitzer Fließ“") 145.3 KB