## Verordnung über den Landesentwicklungsplan Hauptstadtregion Berlin-Brandenburg (LEP HR)

Auf Grund des Artikels 8 Absatz 4 in Verbindung mit Artikel 8 Absatz 3 des Landesplanungsvertrages in der Fassung der Bekanntmachung vom 13.
Februar 2012 (GVBl.
I Nr. 14) und Artikel 1 des Gesetzes zu dem Fünften Staatsvertrag vom 16.
Februar 2011 über die Änderung des Landesplanungsvertrages und zur Änderung weiterer planungsrechtlicher Vorschriften vom 21. September 2011 (GVBl. I Nr. 21) verordnet die Landesregierung:

#### § 1 

Der Landesentwicklungsplan Hauptstadtregion Berlin-Brandenburg (LEP HR), der als Anlage zu dieser Verordnung veröffentlicht wird, ist Bestandteil dieser Verordnung.
Er besteht aus textlichen und zeichnerischen Festlegungen (Landesentwicklungsplan Hauptstadtregion Berlin-Brandenburg (LEP HR) einschließlich Festlegungskarte im Maßstab 1 : 300 000).

#### § 2 

Der Landesentwicklungsplan Hauptstadtregion Berlin-Brandenburg (LEP HR) wird mit der Begründung, der zusammenfassenden Erklärung nach § 10 Absatz 3 des Raumordnungsgesetzes und einer Rechtsbehelfsbelehrung bei der Gemeinsamen Landesplanungsabteilung, bei den Landkreisen, den kreisfreien Städten, amtsfreien Gemeinden und Ämtern zur Einsicht für jedermann niedergelegt.
Darüber hinaus sind diese Dokumente auf der Internetseite der Gemeinsamen Landesplanungsabteilung unter der Adresse https://gl.berlin-brandenburg.de abrufbar.

#### § 3 

Diese Verordnung tritt am 1.
Juli 2019 in Kraft.

Potsdam, den 29.
April 2019

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider

Hinweis nach § 11 Absatz 5 Satz 2 des Raumordnungsgesetzes:

Folgende Mängel werden unbeachtlich, wenn sie nicht innerhalb eines Jahres ab Bekanntmachung der Verordnung über den Landesentwicklungsplan Hauptstadtregion Berlin-Brandenburg (LEP HR) gegenüber der Gemeinsamen Landesplanungsabteilung unter Darlegung des die Verletzung begründenden Sachverhalts geltend gemacht worden sind:

1.  eine nach § 11 Absatz 1 Nummer 1 und 2 des Raumordnungsgesetzes beachtliche Verletzung der dort bezeichneten Verfahrens- und Formvorschriften,
2.  nach § 11 Absatz 3 des Raumordnungsgesetzes beachtliche Mängel des Abwägungsvorgangs,
3.  eine nach § 11 Absatz 4 des Raumordnungsgesetzes beachtliche Verletzung der Vorschriften über die Umweltprüfung.

* * *

### Anlagen

1

[Anlage zur Verordnung über den Landesentwicklungsplan Hauptstadtregion Berlin-Brandenburg (LEP HR)](/br2/sixcms/media.php/68/GVBl_II_35_2019-01-Anlage-Landesentwicklungsplan.pdf "Anlage zur Verordnung über den Landesentwicklungsplan Hauptstadtregion Berlin-Brandenburg (LEP HR)") 10.5 MB

2

[Festlegungskarte](/br2/sixcms/media.php/68/GVBl_II_35_2019-02-Anlage-Festlegungskarte.pdf "Festlegungskarte") 25.2 MB