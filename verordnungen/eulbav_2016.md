## Verordnung über die Anerkennung europäischer Berufsqualifikationen als Laufbahnbefähigung im Land Brandenburg (EU-Laufbahnbefähigungsanerkennungsverordnung - EU-LBAV)

#### § 1 Anwendungsbereich

(1) Diese Verordnung regelt die Anerkennung in einem anderen Mitgliedstaat erworbener Berufsqualifikationen von Staatsangehörigen

1.  eines Mitgliedstaates der Europäischen Union,
2.  eines anderen Vertragsstaates des Abkommens über den Europäischen Wirtschaftsraum oder
3.  eines Staates, dem die Bundesrepublik Deutschland und die Europäische Gemeinschaft oder die Bundesrepublik Deutschland und die Europäische Union vertraglich einen Anspruch auf Anerkennung von Berufsqualifikationen eingeräumt haben,

auf Antrag als Laufbahnbefähigung im Sinne des Landesbeamtengesetzes entsprechend der Richtlinie 2005/36/EG des Europäischen Parlaments und des Rates vom 7.
September 2005 über die Anerkennung von Berufsqualifikationen (ABl.
L 255 vom 30.9.2005, S. 22, L 271 vom 16.10.2007, S. 18, L 93 vom 4.4.2008, S. 28, L 33 vom 3.2.2009, S. 49, L 305 vom 24.10.2014, S. 115, L 177 vom 8.7.2015, S. 60, L 268 vom 15.10.2015, S. 35), die zuletzt durch die Richtlinie 2013/55/EU (ABl.
L 354 vom 28.12.2013, S. 132) geändert worden ist.
Der Grundsatz der automatischen Anerkennung nach Titel III Kapitel III der Richtlinie 2005/36/EG und die Regelungen zur Anerkennung von Lehramtsbefähigungen bleiben unberührt.

(2) Mitgliedstaat im Sinne dieser Verordnung ist

1.  jeder Mitgliedstaat der Europäischen Union,
2.  jeder andere Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum oder
3.  jeder andere Vertragsstaat, dem die Bundesrepublik Deutschland und die Europäische Gemeinschaft oder die Bundesrepublik Deutschland und die Europäische Union vertraglich einen Rechtsanspruch auf Anerkennung von Berufsqualifikationen eingeräumt haben.

#### § 2 Anerkennungsvoraussetzungen

(1) Die Befähigungs- und Ausbildungsnachweise (Qualifikationsnachweise), die in einem anderen Mitgliedstaat erforderlich sind, um in seinem Hoheitsgebiet die Erlaubnis zur Aufnahme und Ausübung eines reglementierten Berufes (im öffentlichen Dienst) zu erhalten, sind auf Antrag als Laufbahnbefähigung, die der Fachrichtung der Qualifikationsnachweise entspricht, anzuerkennen, wenn

1.  sie in einem Mitgliedstaat von einer nach dessen Rechts- und Verwaltungsvorschriften zuständigen Behörde ausgestellt worden sind und
2.  sie im Vergleich zu den in Brandenburg für den Erwerb der Laufbahnbefähigung zu erfüllenden Voraussetzungen
    1.  kein Defizit im Sinne des § 4 Absatz 2 aufweisen,
    2.  zwar ein Defizit aufweisen, aber Ausgleichsmaßnahmen nicht zu fordern sind oder
    3.  ein Defizit aufweisen, das durch erfolgreich absolvierte Ausgleichsmaßnahmen ausgeglichen worden ist.

Reglementiert ist ein Beruf dann, wenn dessen Aufnahme und Ausübung durch staatliche Rechtsvorschriften an das Vorliegen bestimmter Berufsqualifikationen gebunden ist.

(2) Hat die antragstellende Person in einem Mitgliedstaat, der die Berufsausübung nicht reglementiert hat, den Beruf im öffentlichen Dienst innerhalb der letzten zehn Jahre ein Jahr lang vollzeitlich oder in einer entsprechenden Gesamtdauer in Teilzeit ausgeübt, so gilt Absatz 1 entsprechend, wenn die Qualifikationsnachweise bescheinigen, dass die antragstellende Person auf die Ausübung des betreffenden Berufes vorbereitet wurde.
Bestätigen die Qualifikationsnachweise den Abschluss einer reglementierten Ausbildung, so ist der Nachweis von Berufserfahrung nicht erforderlich.

(3) Abweichend von den Absätzen 1 und 2 kann die Anerkennung einer Befähigung für die Laufbahn des höheren Dienstes aufgrund eines Qualifikationsnachweises, der nicht mindestens Artikel 11 Buchstabe b der Richtlinie 2005/36/EG entspricht, verweigert werden.

(4) Einem Qualifikationsnachweis nach den Absätzen 1 und 2 sind gleichgestellt

1.  ein Ausbildungsnachweis im Sinne des Artikels 12 der Richtlinie 2005/36/EG und
2.  jeder in einem Drittland ausgestellte Ausbildungsnachweis, sofern seine Inhaberin oder sein Inhaber in dem betreffenden Beruf drei Jahre Berufserfahrung im Hoheitsgebiet des Mitgliedstaates besitzt, der diesen Ausbildungsnachweis nach Artikel 2 Absatz 2 der Richtlinie 2005/36/EG anerkannt hat, und dieser Mitgliedstaat die Berufserfahrung bescheinigt.

(5) Abweichend von den Absätzen 1 und 2 kann im Einzelfall auf Antrag eine Qualifikation als Befähigung für eine Laufbahn mit partiellem Zugang für eine bestimmte Tätigkeit anerkannt werden, wenn

1.  die antragstellende Person ohne Einschränkung qualifiziert ist, im Mitgliedstaat, in dem sie die Qualifikation erworben hat, die berufliche Tätigkeit auszuüben, für die im Land Brandenburg partieller Zugang beantragt wird,
2.  die Unterschiede zwischen der Berufstätigkeit im Herkunftsmitgliedstaat und der Tätigkeit in der Laufbahn so groß sind, dass die Anwendung von Ausgleichsmaßnahmen dem Durchlaufen einer Ausbildung für die Laufbahn gleichkäme, und
3.  die Berufstätigkeit sich objektiv von anderen im Land Brandenburg unter den reglementierten Beruf fallenden Tätigkeiten trennen lässt.
    Dafür ist zu berücksichtigen, ob die berufliche Tätigkeit im Herkunftsmitgliedstaat eigenständig ausgeübt werden kann.

Der partielle Zugang kann verweigert werden, wenn zwingende Gründe des Allgemeininteresses dies rechtfertigen.

#### § 3 Antragsverfahren

(1) Zuständig für die Entscheidung über die Anerkennung ist die für die jeweilige Laufbahn zuständige Laufbahnordnungsbehörde.
Die zuständigen Behörden nach Satz 1 sind zuständige Stellen nach Maßgabe des § 13b des Brandenburgischen Berufsqualifikationsfeststellungsgesetzes.
Der Antrag auf Anerkennung kann schriftlich oder elektronisch sowohl unmittelbar bei der zuständigen Behörde als auch beim Einheitlichen Ansprechpartner im Sinne des Gesetzes über den Einheitlichen Ansprechpartner für das Land Brandenburg gestellt werden.

(2) Im Antrag ist anzugeben, welche Tätigkeit im öffentlichen Dienst angestrebt wird.
Dem Antrag sind beizufügen:

1.  ein Lebenslauf mit tabellarischer Darstellung des beruflichen Werdegangs,
2.  ein Nachweis der Staatsangehörigkeit,
3.  Qualifikationsnachweise,
4.  Bescheinigungen oder Urkunden des Heimat- oder Herkunftsstaates darüber, dass keine Straftaten, schwerwiegenden beruflichen Verfehlungen oder sonstige, die Eignung in Frage stellenden Umstände bekannt sind; die Bescheinigungen oder Urkunden dürfen bei ihrer Vorlage nicht älter als drei Monate sein,
5.  eine Bescheinigung der zuständigen Behörde oder der zuständigen Stelle des Heimat- oder Herkunftsstaates, aus der hervorgeht, zu welcher Berufsausübung die Qualifikationsnachweise dort im öffentlichen Dienst berechtigen,
6.  Bescheinigungen über die Art und Dauer der nach Erwerb des Qualifikationsnachweises in einem Mitgliedstaat ausgeübten Tätigkeiten in der Fachrichtung des Qualifikationsnachweises,
7.  Nachweise über Inhalte und Dauer der Studien und Ausbildungen in Form von Studienordnungen, Prüfungsordnungen, Studienbüchern oder in anderer geeigneter Weise, aus denen die Anforderungen, die zur Erlangung des Abschlusses geführt haben, hervorgehen müssen,
8.  gegebenenfalls von einer dazu berechtigten Stelle ausgestellte Bescheinigungen über Kenntnisse, Fähigkeiten und Kompetenzen, die durch lebenslanges Lernen erworben wurden,
9.  eine Erklärung, ob die Anerkennung bei einer anderen Behörde in der Bundesrepublik Deutschland beantragt wurde und gegebenenfalls wie darüber entschieden worden ist.

(3) Der Antrag und die beizufügenden Unterlagen sind, soweit sie von der antragstellenden Person stammen, in deutscher Sprache einzureichen.
Handelt es sich um fremdsprachige Unterlagen, ist eine beglaubigte Übersetzung beizufügen.
Bei begründeten Zweifeln an der Übereinstimmung der in Kopie eingereichten Unterlagen mit dem Original oder der Richtigkeit von Angaben kann die Vorlage einer beglaubigten Kopie verlangt werden.

(4) Die Laufbahnordnungsbehörde bestätigt der antragstellenden Person innerhalb eines Monats den Empfang der Unterlagen und teilt ihr gegebenenfalls mit, welche Unterlagen fehlen.

(5) Bestehen berechtigte Zweifel, so kann von der zuständigen Behörde eines Mitgliedstaates eine Bescheinigung der Tatsache verlangt werden, dass der antragstellenden Person die Ausübung des Berufes nicht aufgrund eines disziplinarischen Verhaltens oder einer Verurteilung wegen strafbarer Handlung ausgesetzt oder untersagt worden ist.
Der Informationsaustausch soll über das Binnenmarkt-Informationssystem (IMI) nach der Verordnung (EU) Nr. 1024/2012 erfolgen.

#### § 4 Bewertung der Qualifikationsnachweise

(1) Die Laufbahnordnungsbehörde stellt fest, ob die Qualifikationsnachweise einer Laufbahnbefähigung des Landes Brandenburg zugeordnet werden können.
Anhand eines Vergleichs zwischen den Vorbildungs- und Ausbildungsvoraussetzungen der Laufbahnbefähigung und der Qualifikationsnachweise stellt sie fest, ob ein Defizit im Sinne des Absatzes 2 besteht.

(2) Ein Defizit liegt vor, wenn

1.  die bisherige Ausbildung und der dazugehörige Ausbildungsnachweis sich auf Fächer beziehen, die sich wesentlich von den im Land Brandenburg vorgeschriebenen unterscheiden, oder
2.  die Laufbahnbefähigung die Wahrnehmung eines umfangreicheren Aufgabenfeldes ermöglicht als der reglementierte Beruf im Mitgliedstaat der antragstellenden Person und wenn dieser Unterschied in einer besonderen Ausbildung besteht, die für den Erwerb der Laufbahnbefähigung vorgeschrieben wird und sie sich auf Fächer bezieht, die sich wesentlich von denen unterscheiden, die von den Qualifikationsnachweisen abgedeckt werden, die die antragstellende Person vorlegt.

Fächer unterscheiden sich wesentlich, wenn die durch sie vermittelten Kenntnisse, Fähigkeiten und Kompetenzen eine wesentliche Voraussetzung für die Ausübung des Berufes ist und die bisherige Ausbildung diesbezüglich bedeutende Abweichungen hinsichtlich des Inhalts gegenüber der für die Qualifikation für die Laufbahnbefähigung geforderten (fachtheoretischen) Ausbildung aufweist.

(3) Wird ein Defizit festgestellt, ist zu prüfen, ob die im Anschluss an den Erwerb der Berufsqualifikation im Rahmen der bisherigen einschlägigen Berufspraxis oder durch lebenslanges Lernen erworbenen Kenntnisse, Fähigkeiten und Kompetenzen, die von einer dazu berechtigten Stelle bescheinigt worden sind, den wesentlichen Unterschied ganz oder teilweise ausgleichen können.

#### § 5 Notwendigkeit von Ausgleichsmaßnahmen

(1) Liegt ein Defizit im Sinne des § 4 Absatz 2 vor, das nicht gemäß § 4 Absatz 3 ausgeglichen worden ist, ist die Anerkennung vom erfolgreichen Absolvieren einer Ausgleichsmaßnahme abhängig zu machen.
Bei einer Anerkennung für eine Laufbahn

1.  des einfachen und des mittleren Dienstes kann die antragstellende Person zwischen einer Eignungsprüfung und einem Anpassungslehrgang wählen,
2.  des gehobenen und des höheren Dienstes kann die antragstellende Person zwischen einer Eignungsprüfung und einem Anpassungslehrgang wählen, wenn der Befähigungsnachweis mindestens Artikel 11 Buchstabe c der Richtlinie 2005/36/EG entspricht.

In den übrigen Fällen legt die Laufbahnordnungsbehörde die Ausgleichsmaßnahmen fest.
Dabei kann sie

1.  bei einer Anerkennung für eine Laufbahn des gehobenen oder des höheren Dienstes als Ausgleichsmaßnahme einen Anpassungslehrgang und eine Eignungsprüfung festlegen, wenn der Befähigungsnachweis höchstens Artikel 11 Buchstabe a der Richtlinie 2005/36/EG entspricht,
2.  in den übrigen Fällen als Ausgleichsmaßnahme eine Eignungsprüfung oder einen Anpassungslehrgang festlegen.

(2) Abweichend von Absatz 1 ist eine Berufsqualifikation für Laufbahnen des gehobenen und höheren Dienstes, deren Aufgabenausübung eine genaue Kenntnis des deutschen Rechts erfordert und bei denen Beratung oder Beistand in Bezug auf das deutsche Recht ein wesentlicher und beständiger Teil der Berufsausübung ist, nur anzuerkennen, wenn die antragstellende Person erfolgreich eine Eignungsprüfung abgelegt hat.

#### § 6 Eignungsprüfung

(1) Die Eignungsprüfung ist eine die beruflichen Kenntnisse, Fähigkeiten und Kompetenzen betreffende, staatliche Prüfung, mit der die Fähigkeiten, die Aufgaben der angestrebten Laufbahn auszuüben, beurteilt werden.
Die Eignungsprüfung muss dem Umstand Rechnung tragen, dass im Heimat- oder Herkunftsstaat bereits eine entsprechende berufliche Qualifikation vorliegt.
Der antragstellenden Person ist die Möglichkeit zu geben, die Eignungsprüfung spätestens sechs Monate nach Ausübung ihres Wahlrechts oder der Festsetzung durch die Laufbahnordnungsbehörde abzulegen.

(2) Bei Laufbahnen mit Vorbereitungsdienst führt die Eignungsprüfung die für die Durchführung der Laufbahnprüfung zuständige Behörde durch.
Bei Laufbahnen besonderer Fachrichtungen wird die Eignungsprüfung von der zuständigen Laufbahnordnungsbehörde durchgeführt, die hierfür auch eine andere Behörde bestimmen kann.

(3) Die Laufbahnordnungsbehörde vergleicht die für die Laufbahnbefähigung als unverzichtbar angesehenen Sachgebiete mit den Qualifikationen und den Erfahrungen der antragstellenden Person, die bereits in einem anderen Mitgliedstaat erworben wurden.
Sie erstellt ein Verzeichnis der Sachgebiete, die aufgrund eines Vergleiches der für die Laufbahnbefähigung erforderlichen Qualifikation und den vorliegenden Qualifikationsnachweisen der antragstellenden Person nicht abgedeckt werden.
Anschließend legt die Laufbahnordnungsbehörde im Einzelfall, abhängig von den festgestellten Defiziten, den konkreten Inhalt und Umfang der Prüfung fest.
Sie kann weitere Bestimmungen zur Prüfung treffen, soweit diese Verordnung und die Ausbildungs- und Prüfungsordnungen keine abschließenden Regelungen treffen.

(4) Bei Laufbahnen mit Vorbereitungsdienst gelten die in den jeweiligen Ausbildungs- und Prüfungsordnungen genannten Prüfungsgebiete als für die Laufbahn notwendige Sachgebiete.
Bei Laufbahnen besonderer Fachrichtungen sind die Prüfungsgebiete aufgrund eines Vergleiches mit den der Laufbahnbefähigung zugrunde liegenden Prüfungsgebieten festzulegen.

(5) Für die Durchführung der Prüfung und die Bewertung der Prüfungsleistungen gelten die für die jeweilige Laufbahn geltenden Prüfungsbestimmungen entsprechend.
Soweit für die Laufbahn keine Prüfungsbestimmungen existieren, erfolgt die Bewertung auf der Grundlage folgender Notenskala:

Sehr gut (1)

\=

eine Leistung, die den Anforderungen in besonderem Maße entspricht;

Gut (2)

\=

eine Leistung, die den Anforderungen voll entspricht;

Befriedigend (3)

\=

eine Leistung, die im Allgemeinen den Anforderungen entspricht;

Ausreichend (4)

\=

eine Leistung, die zwar Mängel aufweist, aber im Ganzen den Anforderungen noch entspricht;

Mangelhaft (5)

\=

eine Leistung, die den Anforderungen nicht entspricht, jedoch erkennen lässt, dass die notwendigen Grundkenntnisse vorhanden sind und die Mängel in absehbarer Zeit behoben werden können;

Ungenügend (6)

\=

eine Leistung, die den Anforderungen nicht entspricht und bei der selbst die Grundkenntnisse so lückenhaft sind, dass die Mängel in absehbarer Zeit nicht behoben werden können.

Zur Bildung der Prüfungsnoten können die Einzelleistungen und die Gesamtleistung der Prüfung nach einem System von Punktzahlen bewertet werden.
Über den Prüfungshergang ist eine Niederschrift zu fertigen.
Die Eignungsprüfung kann einmal wiederholt werden.
Die antragstellende Person erhält über das Ergebnis der Prüfung einen schriftlichen Bescheid.

(6) Die Eignungsprüfung kann auf der Grundlage einer Verwaltungsvereinbarung mit dem Bund oder einem anderen Land auch von einem darin bestimmten Prüfungsamt nach dessen Prüfungsvorschriften abgenommen werden.

#### § 7 Anpassungslehrgang

(1) Ein Anpassungslehrgang vermittelt die Aufgaben der angestrebten Laufbahn unter der Verantwortung einer qualifizierten Inhaberin oder eines qualifizierten Inhabers der angestrebten Laufbahnbefähigung.
Er kann mit einer theoretischen Zusatzausbildung einhergehen.

(2) Durchführung und Organisation des Anpassungslehrgangs obliegen der Laufbahnordnungsbehörde, die hierfür auch eine andere Behörde bestimmen kann.

(3) Der Anpassungslehrgang dient dazu, die im Vergleich zwischen vorhandener und geforderter Ausbildung fehlenden Qualifikationen zu erwerben.
Die konkreten Inhalte und die konkrete Dauer werden unter Berücksichtigung des festgestellten Defizits im Hinblick auf die Erfordernisse der jeweiligen Laufbahn festgelegt.
Er darf höchstens drei Jahre dauern.
Bei Laufbahnen mit Vorbereitungsdienst darf der Anpassungslehrgang die Dauer des Vorbereitungsdienstes nicht überschreiten.

(4) Der Lehrgang ist Gegenstand einer Bewertung.
Zur Bewertung wird die Notenskala der jeweiligen Ausbildungs- und Prüfungsordnung oder, sofern eine solche nicht existiert, die des § 6 Absatz 5 herangezogen.
Werden die Leistungen nicht mindestens mit der Gesamtnote „ausreichend“ bewertet, ist der Anpassungslehrgang nicht bestanden.
Die antragstellende Person erhält über das Ergebnis einen schriftlichen Bescheid.

(5) Die Rechte und Pflichten während des Anpassungslehrgangs werden durch Vertrag zwischen dem Land Brandenburg, vertreten durch die nach Absatz 2 zuständige Behörde, und der antragstellenden Person festgelegt.
Die antragstellende Person befindet sich während des Anpassungslehrgangs in einem öffentlich-rechtlichen Berufsqualifikations-Anerkennungsverhältnis, welches durch das als Anlage 1 beigefügte Vertragsmuster näher geregelt wird.
Der Anpassungslehrgang endet vorzeitig auf Antrag oder wenn schwerwiegende Pflichtverletzungen der antragstellenden Person der Fortführung entgegenstehen.

#### § 8 Entscheidung

(1) Über den Antrag ist innerhalb von vier Monaten nach Vorlage der vollständigen Unterlagen zu entscheiden.
In den Fällen des § 1 Absatz 1 Satz 2 beträgt die Frist drei Monate.
Die schriftliche Entscheidung ist zu begründen und mit einer Rechtsbehelfsbelehrung zu versehen.
Wird die Anerkennung von einer Ausgleichsmaßnahme abhängig gemacht, muss die Begründung auch Informationen enthalten:

1.  über das Niveau der verlangten und das Niveau der vorgelegten Berufsqualifikation gemäß der Klassifizierung in Artikel 11 der Richtlinie 2005/36/EG und
2.  zu den wesentlichen Defiziten nach § 4 Absatz 2 sowie zu den Gründen, aus denen diese Unterschiede nicht durch Kenntnisse, Fähigkeiten und Kompetenzen, die durch lebenslanges Lernen erworben und hierfür von einer einschlägigen Stelle als gültig anerkannt wurden, ausgeglichen werden können.

Auf ein nach § 5 Absatz 1 Satz 2 Nummer 1 und 2 bestehendes Wahlrecht ist hinzuweisen.

(2) Im Falle einer Anerkennung oder einer Entscheidung über einen partiellen Zugang ist im Bescheid darauf hinzuweisen, dass die Anerkennung keinen Anspruch auf Einstellung begründet.

(3) Die Anerkennung ist insbesondere zu versagen, wenn

1.  die Voraussetzungen des § 2 nicht erfüllt sind,
2.  die für die Anerkennung erforderlichen Unterlagen trotz Aufforderung nicht in angemessener Frist vollständig vorgelegt wurden,
3.  die festgelegten Ausgleichsmaßnahmen nicht erfolgreich abgeschlossen worden sind oder die antragstellende Person sich ihnen aus von ihr zu vertretenden Gründen nicht unterzogen hat oder
4.  die antragstellende Person wegen schwerwiegender beruflicher Verfehlungen, Straftaten oder aus sonstigen Gründen für den Zugang zum Beamtenverhältnis nicht geeignet erscheint.

(4) Die Verfahrensfristen nach Absatz 1 und § 3 Absatz 4 laufen ab dem Zeitpunkt, in dem die antragstellende Person den Antrag oder ein fehlendes Dokument bei der zuständigen Behörde oder beim Einheitlichen Ansprechpartner einreicht.
Eine Aufforderung zur Vorlage beglaubigter Kopien im Sinne des § 3 Absatz 3 hemmt nicht den Fristlauf nach Absatz 1.

#### § 9 Erwerb der Laufbahnbefähigung

Mit dem erfolgreichen Abschluss des Anerkennungsverfahrens wird die Befähigung für die jeweilige Laufbahn erworben.
Über den Erwerb der Befähigung ist der antragstellenden Person eine Bescheinigung nach dem Muster der Anlage 2 auszuhändigen.

#### § 10 Berufsbezeichnung

Sofern mit dem Erwerb der Laufbahnbefähigung die Befugnis verbunden ist, eine Bezeichnung zu führen, wird diese als Berufsbezeichnung geführt.
Bei Gewährung eines partiellen Zugangs wird die für die berufliche Tätigkeit nach § 2 Absatz 5 Satz 1 Nummer 1 im Herkunftsstaat bestehende Berufsbezeichnung mit deutscher Übersetzung geführt.

#### § 11 Verwaltungszusammenarbeit

Die zuständigen Behörden arbeiten mit den zuständigen Behörden der Mitgliedstaaten sowie mit den einheitlichen Ansprechpartnern nach Artikel 6 der Richtlinie 2006/123/EG zusammen und leisten Amtshilfe, um die Anwendung der Richtlinie 2005/36/EG zu erleichtern.

**Anlage 1**  
(zu § 7 Absatz 5)

**Muster des Vertrags zur Regelung der Rechte und Pflichten während des Anpassungslehrgangs**

### Vertrag

Zwischen  
dem Land Brandenburg,

vertreten durch ………………………………………………………,

und

Frau/Herrn ………………………………………

geboren am …………………………………….

wohnhaft ………………………………………..

wird folgender Vertrag geschlossen:

#### § 1

Frau/Herrn …………………………………… wird für die Zeit vom ………………………… bis zum ……………………… Gelegenheit gegeben, in einem Anpassungslehrgang im Sinne des Artikels 3 Absatz 1 Buchstabe g, des Artikels 14 der Richtlinie 2005/36/EG und des § 7 der EU-Laufbahnbefähigungsanerkennungsverordnung die Kenntnisse und Fähigkeiten für die Laufbahn  
…………………………………………………………………………………………………………………  
zu erwerben, die ihr/ihm nach den festgestellten Defiziten noch fehlen.
Dadurch entsteht ein öffentlich-rechtliches Berufsqualifikations-Anerkennungsverhältnis.

#### § 2

(1) Der Anpassungslehrgang besteht aus einer berufspraktischen Ausbildung in den Aufgaben der oben genannten Laufbahn unter Anleitung und Verantwortung einer qualifizierten Inhaberin oder eines qualifizierten Inhabers der Laufbahnbefähigung (Ausbildungsleitung).

(2) Der Anpassungslehrgang umfasst eine Zusatzausbildung in Form von Fortbildungsmaßnahmen, wenn die vorhandenen Defizite nicht im Rahmen der berufspraktischen Tätigkeit vermittelt werden können.

(3) Folgende Defizite wurden bei Frau/Herrn …………………………………… festgestellt:  
…………………………………………………………………………………………………………………  
…………………………………………………………………………………………………………………  
…………………………………………………………………………………………………………………  
…………………………………………………………………………………………………………………  
…………………………………………………………………………………………………………………  
…………………………………………………………………………………………………………………  
…………………………………………………………………………………………………………………  
…………………………………………………………………………………………………………………

Das Ziel des Anpassungslehrgangs ist die Beseitigung dieser Defizite.
Die Ausbildungsleitung legt die weiteren Einzelheiten des Anpassungslehrgangs fest.
Dabei stellt sie durch geeignete Maßnahmen sicher, dass sich Frau/Herr ………………………………………………… die Kenntnisse und Fähigkeiten der in § 1 genannten Laufbahnbefähigung in sachgerechter Form aneignen kann.

(4) Sie/Er kann sich in allen Fragen der Durchführung des Anpassungslehrgangs an die Ausbildungsleitung wenden.

#### § 3

Dienstobliegenheiten werden nicht übertragen.

#### § 4

Der Anpassungslehrgang endet außer durch Ablauf der festgesetzten Zeit vorzeitig auf Antrag.
Er kann außerdem vorzeitig von Amts wegen beendet werden, wenn schwerwiegende Pflichtverletzungen von Frau/Herrn …………………………………… der Fortführung entgegenstehen.

#### § 5

Frau/Herr …………………………………… hat den Anweisungen der Ausbildungsleitung zu folgen; sie/er wird zu Beginn des Anpassungslehrgangs auf die Pflicht zur Verschwiegenheit hingewiesen.

#### § 6

Eine Vergütung oder ein sonstiges Entgelt wird nicht gewährt.

Potsdam, den …………………………………………

………………………………………………………………  
(Unterschrift der Teilnehmerin oder des Teilnehmers  
des Anpassungslehrgangs)

………………………………………………………………  
(Unterschrift des Vertreters des Landes Brandenburg)

**Anlage 2**  
(zu § 9)

**Muster der Bescheinigung über den Erwerb der Laufbahnbefähigung aufgrund des Unionsrechts**

### Bescheinigung  
über den Erwerb der Laufbahnbefähigung  
aufgrund einer Berufsqualifikation nach der Richtlinie 2005/36/EG[1)](#aa)

Aufgrund des § 9 der EU-Laufbahnbefähigungsanerkennungsverordnung[2)](#bb) wird bescheinigt, dass

Herr/Frau …………………………………………………….…

geboren am ……………………………………………….……

auf Grund folgender Qualifikationsnachweise[3)](#cc)

…………………………………………………………………………………………………………………  
…………………………………………………………………………………………………………………  
…………………………………………………………………………………………………………………  
…………………………………………………………………………………………………………………  
…………………………………………………………………………………………………………………  
…………………………………………………………………………………………………………………

ggf.[4)](#dd)  
und nach Ablegung einer Eignungsprüfung[5)](#ee)

oder[4)](#dd)  
nach Teilnahme an einem Anpassungslehrgang[6)](#ff)

die Befähigung für die Laufbahn[7)](#gg)

…………………………………………………………………………………………………………………  
…………………………………………………………………………………………………………………

erworben hat.

Potsdam, den …………………………………………

………………………………………………………………  
(Laufbahnordnungsbehörde)

………………………………………………………………  
(Unterschrift/Dienstsiegel)

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[1)](#a) Richtlinie 2005/36/EG des Europäischen Parlaments und des Rates vom 7.
September 2005 über die Anerkennung von Berufsqualifikationen (ABl.
L 255 vom 30.9.2005, S. 22, L 271 vom 16.10.2007, S. 18, L 93 vom 4.4.2008, S. 28, L 33 vom 3.2.2009, S. 49, L 305 vom 24.10.2014, S. 115, L 177 vom 8.7.2015, S. 60, L 268 vom 15.10.2015, S. 35), die zuletzt durch die Richtlinie 2013/55/EU (ABl.
L 354 vom 28.12.2013, S. 132) geändert worden ist  
[2)](#b) EU-Laufbahnbefähigungsanerkennungsverordnung vom 2.
Februar 2016 (GVBl.
II Nr.
4)

Erläuterungen (nicht Inhalt der Bescheinigung):

[3)](#c) Bezeichnungen der Qualifikationsnachweise (§ 3 Absatz 2 Satz 2 Nummer 3, 5 bis 7 der EU-Laufbahnbefähigungsanerkennungsverordnung) sind anzuführen  
[4)](#d) Nicht Zutreffendes ist zu streichen  
[5)](#e) Angabe der Art der Eignungsprüfung (§ 6 der EU-Laufbahnbefähigungsanerkennungsverordnung)  
[6)](#f) Bezeichnung des Anpassungslehrgangs (§ 7 der EU-Laufbahnbefähigungsanerkennungsverordnung)  
[7)](#g) Bezeichnung der jeweiligen Laufbahn einfügen (gegebenenfalls mit partiellem Zugang)