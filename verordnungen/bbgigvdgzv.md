## Brandenburgische Verordnung über die Zuständigkeiten nach dem IGV-Durchführungsgesetz (BbgIGVDGZV)

Auf Grund des § 2 Absatz 1 Satz 1 des IGV\-Durchführungsgesetzes vom 21. März 2013 (BGBl. I S. 566) in Verbindung mit § 6 Absatz 2 und § 12 Absatz 1 Satz 2 des Landesorganisationsgesetzes vom 24. Mai 2004 (GVBl. I S. 186), von denen durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl. I Nr. 28 S. 2) § 6 Absatz 2 geändert und § 12 Absatz 1 Satz 2 eingefügt worden sind, sowie § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19. Februar 1987 (BGBl. I S. 602) verordnet die Landesregierung:

#### § 1 Zuständigkeit an Flughäfen

(1) Zuständige Behörde nach § 2 Absatz 1 Satz 1 des IGV-Durchführungsgesetzes sowie für den Vollzug nach § 8 Absatz 6 Satz 1 des IGV-Durchführungsgesetzes ist an Flughäfen der Landkreis oder die kreisfreie Stadt, auf dessen Gebiet der Flughafen gelegen ist, soweit sich aus den nachfolgenden Vorschriften nicht etwas anderes ergibt.
Die zuständige Behörde nach Satz 1 kann Dritte mit der Durchführung ihrer Aufgaben nach § 8 Absatz 6 Satz 1 des IGV-Durchführungsgesetzes im Einvernehmen mit der Aufsichtsbehörde nach Absatz 3 Satz 3 beauftragen.

(2) Zuständiges Gesundheitsamt nach § 2 Absatz 1 Satz 1 und 2 des IGV-Durchführungsgesetzes ist an Flughäfen das Gesundheitsamt nach § 2 Absatz 2 Satz 1 Nummer 3 des Brandenburgischen Gesundheitsdienstgesetzes vom 23. April 2008 (GVBl. I S. 95), das zuletzt durch Artikel 13 des Gesetzes vom 25. Januar 2016 (GVBl. I Nr. 5 S. 17) geändert worden ist, auf dessen Gebiet der Flughafen gelegen ist.

(3) Die Aufgaben nach dem IGV-Durchführungsgesetz sind Aufgaben der Gefahrenabwehr.
Die Landkreise und kreisfreien Städte erfüllen die ihnen nach dieser Verordnung übertragenen Aufgaben als Pflichtaufgaben zur Erfüllung nach Weisung.
Das für Gesundheit zuständige Ministerium übt die Sonderaufsicht nach den kommunalrechtlichen Vorschriften aus.
Es erlässt Verwaltungsvorschriften, die mindestens die vorzuhaltenden Kapazitäten nach § 8 Absatz 4 in Verbindung mit § 8 Absatz 6 Satz 1 des IGV-Durchführungsgesetzes und Einzelheiten der Dokumentation festlegen.
Haben die Verwaltungsvorschriften nach Satz 4 finanzielle Auswirkungen auf den Landeshaushalt hat das für Gesundheit zuständige Ministerium das Einvernehmen mit dem für Finanzen zuständigen Ministerium herzustellen.

(4) Zuständige Behörde nach § 8 Absatz 7 Satz 1 des IGV-Durchführungsgesetzes ist das für Gesundheit zuständige Ministerium.
Dieses überwacht die Erfüllung der Verpflichtungen nach § 8 Absatz 9 des IGV-Durchführungsgesetzes als insoweit zuständige Behörde nach § 8 Absatz 10 des IGV-Durchführungsgesetzes.

(5) Zuständigkeitsregelungen nach anderen Rechtsvorschriften bleiben unberührt.

#### § 2 Mitteilungen nach §&nbsp;4 Absatz&nbsp;2 des IGV-Durchführungsgesetzes

Zuständige Landesbehörde nach § 4 Absatz 2 des IGV-Durchführungsgesetzes ist das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit.

#### § 3 Gelbfieber-Impfstellen, oberste Landesgesundheitsbehörde

(1) Zuständige Behörde im Sinne des § 7 Absatz 1 des IGV-Durchführungsgesetzes ist das für Gesundheit zuständige Ministerium.

(2) Zuständige oberste Landesgesundheitsbehörde im Sinne des IGV-Durchführungsgesetzes ist das für Gesundheit zuständige Ministerium.

#### § 4 Ordnungswidrigkeiten

Die sachliche Zuständigkeit für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 21 Absatz 1 Nummer 1 Alternative 1 des IGV-Durchführungsgesetzes, soweit Pflichten des Flughafenunternehmers betroffen sind, nach § 21 Absatz 1 Nummer 2 und 5 Alternative 1 sowie Nummer 9 des IGV-Durchführungsgesetzes wird den Landkreisen und kreisfreien Städten übertragen.

#### § 5 Kostentragung und Erstattungsverfahren

(1) Das Land erstattet dem Landkreis oder der kreisfreien Stadt im Wege des Mehrbelastungsausgleichs die notwendigen Kosten für die Wahrnehmung der Aufgaben nach § 1 Absatz 1 und 2.
Leistungen, für die dem Landkreis oder der kreisfreien Stadt dem Grunde nach bereits nach anderen Vorschriften oder im Rahmen der Wahrnehmung der kommunalen Selbstverwaltungsaufgaben ein Ausgleich gezahlt wird oder auf die nach anderen Vorschriften ein Ausgleichsanspruch besteht, werden nicht erstattet.
Erstattungsbehörde ist das Landesamt für Soziales und Versorgung des Landes Brandenburg.

(2) Die Erstattung nach Absatz 1 erfolgt kalenderjährlich auf Antrag in Textform bei der Erstattungsbehörde.
Der Mehrbelastungsausgleich ist jeweils bis zum 15. Februar des folgenden Kalenderjahres geltend zu machen.
Abschlagszahlungen können von der Erstattungsbehörde vierteljährlich zur Quartalsmitte in Höhe von bis zu 95 Prozent des zu erwartenden Erstattungsbetrags auf Antrag in Textform gewährt werden.
Der Kostenerstattungsanspruch nach Absatz 1 mindert sich um den Betrag, in dessen Höhe im vorangegangenen Abrechnungszeitraum aufgrund von Abschlagszahlungen insgesamt ein Überschuss erzielt worden ist.

(3) Der Landkreis oder die kreisfreie Stadt ist verpflichtet, bei einem Antrag auf Kostenerstattung den tatsächlichen Vollzugsaufwand und bei einem Antrag auf Gewährung einer Abschlagszahlung den zu erwartenden Erstattungsbetrag auf Verlangen der Erstattungsbehörde nachzuweisen.
Von der Erstattungsbehörde können zur Feststellung der Ordnungsmäßigkeit der Kostenerstattungsbeträge die sie begründenden Unterlagen vor Ort eingesehen oder angefordert werden.
Die tatsächlich vorgenommenen Einsätze am Flughafen nach § 8 Absatz 1 des IGV-Durchführungsgesetzes im Ereignisfall sind zum Nachweis der Kostenerstattungsbeträge sowie auch zu Evaluations- und Qualitätssicherungszwecken in anonymisierter Form zu dokumentieren.

(4) Das Land erstattet auf schriftlichen Antrag dem Flughafenunternehmer des nach § 8 Absatz 1 benannten Flughafens Berlin Brandenburg die Selbstkosten bei tatsächlicher Nutzungsbeeinträchtigung entsprechend § 8 Absatz 6 Satz 3 bis 5 des IGV-Durchführungsgesetzes.
Erstattungsbehörde ist das Landesamt für Soziales und Versorgung des Landes Brandenburg.
Der Flughafenunternehmer ist verpflichtet, die tatsächliche Nutzungsbeeinträchtigung im Ereignisfall sowie die für die Nutzung der Räumlichkeiten entstandenen Selbstkosten nachzuweisen.
Zur Überprüfung hat der Flughafenunternehmer der Erstattungsbehörde auf Verlangen die erforderlichen Unterlagen zur Verfügung zu stellen.

#### § 6 Inkrafttreten

Die §§ 1 und  5 Absatz 1 bis 3 treten mit Wirkung vom 1.
Januar 2020 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 15.
September 2020

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

Ursula Nonnemacher