## Verordnung über das Naturschutzgebiet „Zützener Moorwiesen“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542), von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl.
I S.
1972, 1974) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21. Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27. Mai 2013 (GVBl. II Nr. 43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Dahme-Spreewald wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Zützener Moorwiesen“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 18 Hektar.
Es umfasst Flächen in folgenden Fluren:

Gemeinde:     

Gemarkung: 

Flur: 

Flurstücke: 

Kasel-Golzig 

Jetsch 

2 

6 anteilig, 60 bis 62, 63 anteilig, 64, 65 anteilig, 402; 

Golßen 

Zützen 

2 

522/1 anteilig, 527 anteilig, 530, 531, 532/1, 532/3,  
533 bis 535, 544, 792 anteilig, 794, 795. 

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage beigefügt.

(2) Die Grenze des Naturschutzgebietes ist jeweils in einer topografischen Karte und einer Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Zützener Moorwiesen“ mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die topografische Karte im Maßstab 1 : 10 000 mit der Blattnummer 1 ermöglicht die Verortung im Gelände.
Die topografische Karte wurde vom Siegelverwahrer am 31.
Juli 2018, Siegelnummer 15 des Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft unterzeichnet.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Liegenschaftskarte im Maßstab 1 : 2 500 mit der Blattnummer 1.
Die Liegenschaftskarte wurde von der Siegelverwahrerin am 23. August 2019, Siegelnummer 13 des Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft unterzeichnet.

(3) Innerhalb des Naturschutzgebietes ist eine Zone 1 mit einer Fläche von rund 5 Hektar und eine Zone 2 mit einer Fläche von rund 5 Hektar mit zusätzlichen Beschränkungen der landwirtschaftlichen Bodennutzung festgesetzt.
Die Zone 1 umfasst folgende Flächen:

Gemeinde:     

Gemarkung: 

Flur: 

Flurstücke: 

Kasel-Golzig 

Jetsch 

2 

65 anteilig; 

Golßen 

Zützen 

2 

522/1 anteilig, 527 anteilig, 530 anteilig, 531, 532/1,  
532/3, 533 anteilig, 544 anteilig, 792 anteilig, 794, 795.

Die Zone 2 umfasst folgende Flächen:

Gemeinde:     

Gemarkung: 

Flur: 

Flurstücke: 

Golßen 

Zützen 

2 

533 anteilig, 534, 535, 544 anteilig.

Die Grenze der Zonen ist in der in Absatz 2 genannten topografischen Karte mit der Blattnummer 1 sowie in der in Absatz 2 genannten Liegenschaftskarte mit der Blattnummer 1 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Liegenschaftskarte.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Dahme-Spreewald, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes, das einen flachgründigen Moorkomplex am Rande des Baruther Urstromtals im Übergangsbereich zum Luckau-Calauer Becken umfasst, ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere von stehenden Gewässern und deren Verlandungsbereichen mit ihren verschiedenen Sumpf-, Ried- und Röhrichtgesellschaften, frischem bis nassem Grünland mit teilweise salzliebenden (halophilen) Beständen, sowie von artenreichen Säumen und Gehölzgruppen;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter
    1.  im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Steifblättriges Knabenkraut (Dactylorhiza incarnata), Sumpf-Platterbse (Lathyrus palustris), Sumpf-Schwertlilie (Iris pseudacorus) und Wasserfeder (Hottonia palustris),
    2.  besonders charakteristischer, seltener und gefährdeter Pflanzenarten der Binnensalzstellen mit landes- und bundesweiter Bedeutung, beispielsweise Erdbeerklee (Trifolium fragiferum) und Weniglappiger Löwenzahn (Taraxacum paucilobum);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum, sowie als potenzielles Wiederausbreitungszentrum wild lebender, an Grünland frischer bis nasser Standorte gebundener Tierarten, insbesondere von Vögeln, Amphibien und Insekten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Drosselrohrsänger (Acrocephalus arundinaceus), Feldlerche (Alauda arvensis), Kiebitz (Vanellus vanellus), Kranich (Grus grus), Rohrweihe (Circus aeruginosus), Schafstelze (Motacilla flava), Zwergtaucher (Tachybaptus ruficollis), Grasfrosch (Rana temporaria), Knoblauchkröte (Pelobates fuscus), Moorfrosch (Rana arvalis), Grashummel (Bombus ruderarius) und Deichhummel (Bombus distinguendus);
4.  die Entwicklung des Gebietes als potenzieller Wiederbesiedelungsbereich wild lebender Tier- und Pflanzenarten, insbesondere verschiedener Schnecken-, Libellen- und Hummelarten sowie Pflanzenarten der Wasserwechselzonen, insbesondere dem Kriechenden Scheiberich (Apium repens);
5.  die Erhaltung der Zützener Moorwiesen wegen ihrer besonderen Eigenart und Schönheit, als ein abwechslungsreich gegliederter Niederungsbereich, der in einer an Strukturelementen weitgehend verarmten Landschaft eine besondere Vielfalt aufweist.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Zützener Moorwiesen“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), das ehemals einen Teilbereich des Gebietes von gemeinschaftlicher Bedeutung „Dahmetal Ergänzung“ umfasste, mit seinen Vorkommen von

1.  Mageren Flachland-Mähwiesen (Alopecurus pratensis, Sanguisorba officinalis) als natürlichem Lebensraumtyp von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Salzwiesen im Binnenland als prioritärem natürlichen Lebensraumtyp im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
3.  Fischotter (Lutra lutra) als Art von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich seiner für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten.
    Ausgenommen ist das Betreten zum Zweck des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 9 nach dem 31.
    Juli eines jeden Jahres;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der Wege, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, zu reiten;
11.  mit Fahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
12.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
13.  Hunde frei laufen zu lassen;
14.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
15.  Düngemittel aller Art zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
16.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
17.  Tiere zu füttern oder Futter bereitzustellen;
18.  Tiere auszusetzen oder Pflanzen anzusiedeln;
19.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
20.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
21.  Pflanzenschutzmittel jeder Art anzuwenden;
22.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle, Jauche, flüssige Gärreste und Sekundärrohstoffdünger einzusetzen,
    2.  auf Grünland § 4 Absatz 2 Nummer 21 und 22 gilt.
        Bei Narbenschäden ist eine umbruchlose Nachsaat mit Zustimmung der unteren Naturschutzbehörde zulässig,
    3.  das Walzen und Schleppen von Grünland im Zeitraum vom 31.
        März bis zur ersten Nutzung eines jeden Jahres unzulässig bleibt.
        In begründeten Ausnahmefällen kann bei der unteren Naturschutzbehörde eine Terminsetzung nach dem 31.
        März beantragt werden.
        Die Zustimmung ist zu erteilen, sofern sie dem Schutzzweck nicht entgegen steht,
    4.  bei Mahd von Grünland eine eingestellte Schnitthöhe von mindestens acht Zentimetern einzuhalten ist,
    5.  bei Beweidung von Grünland eine Auszäunung der Gewässerufer, Röhrichte und Gehölze erfolgt,
    6.  in den Zonen 1 und 2 über die Maßgaben des Buchstaben a hinaus § 4 Absatz 2 Nummer 15 gilt.
        Bei der Nutzung der Zone 2 als Weide darf eine Besatzdichte von maximal 1,4 Raufutter verwertenden Großvieheinheiten (RGV) pro Hektar im Jahresmittel nicht überschritten werden,
    7.  in der Zone 1 über die Maßgaben des Buchstaben a hinaus das Grünland ausschließlich durch Mahd genutzt wird;
2.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        in der Zeit vom 1.
        Februar bis 31.
        Juli eines jeden Jahres die Ausübung der Jagd nur vom Ansitz aus erfolgt,
        
        bb)
        
        die Fallenjagd mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern vom Gewässerufer der im Gebiet liegenden Gräben verboten ist.
        Ausnahmen bedürfen einer Genehmigung der unteren Naturschutzbehörde,
        
        cc)
        
        keine Baujagd in einem Abstand von bis zu 100 Metern vom Gewässerufer der im Gebiet liegenden Gräben vorgenommen wird,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
          
          
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Errichtung anzuzeigen.
        Die Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        Die Entscheidung hierzu soll unverzüglich erfolgen,  
          
        
    3.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und der in § 3 Absatz 2 Nummer 1 und 2 genannten Lebensraumtypen.
    
    Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und die Anlage und Unterhaltung von Wildäckern sind unzulässig.
    Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
    
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung auf den rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne einer Fischbestandskontrolle, -regulierung und -förderung mit Zustimmung der unteren Naturschutzbehörde erfolgen.
    Die Zustimmung ist zu erteilen, wenn sie dem Schutzzweck nicht entgegensteht;
4.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege.
    Die untere Naturschutzbehörde ist rechtzeitig mit dem Ziel einer einvernehmlichen Lösung zu beteiligen;
5.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
6.  die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern dies nicht unter die Nummern 4 und 5 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
7.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
8.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
9.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch jeweils nach dem 31.
    Juli eines jeden Jahres;
10.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
11.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
12.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen;
13.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungserfordernissen, die sich aus anderen Vorgaben ergeben, unberührt.

#### § 6 Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  die Erhaltung und Verbesserung des Salzgehaltes der Salzstellen soll durch angepasste Wasserstände erreicht werden;
2.  das Grünland der Zone 1 (Magere Flachlandmähwiesen und Binnensalzstellen) soll als zweischürige Mähwiese mit der ersten Mahd ab Mitte Mai eines jeden Jahres und einer mindestens achtwöchigen Nutzungspause genutzt werden;
3.  auf den Grünlandflächen außerhalb der Zone 1 soll zur Schaffung geeigneter Brut- und Nahrungshabitate für Wiesen- und Röhrichtbrüter eine späte Nutzung des Grünlandes mit Belassen von Altgrasstreifen an den Gewässerrändern erfolgen;
4.  Maßnahmen zur erneuten Etablierung des Kriechenden Scheiberichs (Apium repens) im Bereich der Binnensalzstellen sollen entwickelt und umgesetzt werden;
5.  die Röhrichtbestände im Bereich der Torfstiche sollen in mehrjährigen Abständen abschnittsweise gemäht werden, um eine Gehölzentwicklung dauerhaft zu verhindern.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

§ 5 Absatz 1 Nummer 1 Buchstabe a, b und f dieser Verordnung treten am 1.
Januar 2019 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 4.
September 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage (zu § 2 Absatz 1) - Kartenskizze zur Lages des Naturschutzgebietes "Zützener Moorwiesen"](/br2/sixcms/media.php/68/GVBl_II_61_2018-Anlage.pdf "Anlage (zu § 2 Absatz 1) - Kartenskizze zur Lages des Naturschutzgebietes ") 613.7 KB