## Verordnung über regelmäßige Datenübermittlungen der Meldebehörden (MeldDÜV)

**_Hinweis: Die Verordnung vom 18.
Januar 2022 (GVBl.
II Nr.
4) tritt am 1. Mai 2022 in Kraft._**

Auf Grund des § 11 des Brandenburgischen Meldegesetzes in der Fassung der Bekanntmachung vom 17. Januar 2006 (GVBl.
I S. 6), der durch das Gesetz vom 4.
Juli 2014 (GVBl.
I Nr. 26) geändert worden ist, verordnet der Minister des Innern und für Kommunales:

#### § 1 Grundsätze der regelmäßigen Datenübermittlung

(1) Die regelmäßige Übermittlung von Daten durch die Meldebehörden und durch die Registerbehörde (§ 5 des Brandenburgischen Meldegesetzes) wird nach Maßgabe dieser Verordnung zugelassen.
Darüber hinausgehende Vorschriften des Bundes- und Landesrechts bleiben unberührt.

(2) Bei der Berichtigung oder Ergänzung (Fortschreibung) der nach dieser Verordnung zu übermittelnden Daten unterrichten die Meldebehörden und die Registerbehörde die Empfänger regelmäßiger Datenübermittlungen unverzüglich über die Fortschreibung und übermitteln die aktuellen Daten.

(3) Bei Datenübermittlungen nach dieser Verordnung ist der Datensatz für das Meldewesen (Einheitlicher Bundes- und Länderteil und Landesteil Brandenburg) zugrunde zu legen.

(4) Hat eine Einwohnerin oder ein Einwohner mehrere Wohnungen, wird die Datenübermittlung durch die für die Hauptwohnung zuständige Meldebehörde durchgeführt.
In den Fällen der §§ 8, 13, 16, 17 werden die Daten auch von der für die Nebenwohnung zuständigen Meldebehörde übermittelt.

#### § 2 Verfahren

(1) Die Übermittlung von Daten nach dieser Verordnung erfolgt durch automatisierte Datenübermittlung oder im Wege automatisierter Abrufverfahren.

(2) Für das Verfahren der Datenübermittlung nach § 7 des Brandenburgischen Meldegesetzes sind die Form und das Verfahren der §§ 2 und 3 der Ersten Bundesmeldedatenübermittlungsverordnung vom 1. Dezember 2014 (BGBl. I S. 1945) in der jeweils geltenden Fassung entsprechend anzuwenden.

(3) Die Datenübermittlungen der Meldebehörden und der Registerbehörde nach dieser Verordnung erfolgen elektronisch unter Zugrundelegung des Datenaustauschformats OSCI-XMeld und Nutzung des Übermittlungsprotokolls OSCI-Transport in der im Bundesanzeiger jeweils bekannt gemachten geltenden Fassung, wenn die datenempfangende Stelle zugestimmt hat.

(4) Daten dürfen nur in den in dieser Verordnung besonders geregelten Fällen durch Bereithalten zum Abruf übermittelt werden (automatisierter Abruf).
Die abrufenden Stellen müssen sich bei der Meldebehörde oder der Registerbehörde anmelden und registrieren lassen.
Die zum Abruf zugelassenen Stellen haben jeweils dem Stand der Technik entsprechende Maßnahmen zur Gewährleistung von Datenschutz und Datensicherheit zu treffen sowie durch organisatorische Maßnahmen sicherzustellen, dass der Abruf nur durch berechtigte Bedienstete erfolgt, die die Daten zur Erfüllung ihrer Aufgaben benötigen.

(5) Für den automatisierten Abruf von Meldedaten gilt § 38 des Bundesmeldegesetzes.
Für die Protokollierung automatisierter Abrufe gilt § 40 des Bundesmeldegesetzes.

#### § 3 Automatisierter Abruf von Meldedaten

(1) Die Meldebehörden und die Registerbehörde dürfen anderen öffentlichen Stellen, einschließlich der öffentlich-rechtlichen Religionsgesellschaften, unbeschadet der weiteren Übermittlungsbefugnisse nach dieser Verordnung die Daten gemäß § 38 Absatz 1 des Bundesmeldegesetzes und die nachfolgend aufgeführten Daten zur Erfüllung der in der Zuständigkeit der abrufenden Stelle liegenden Aufgaben aus Anlass der Feststellung der Identität von Einwohnerinnen und Einwohnern und deren Wohnungen im Wege des automatisierten Abrufs übermitteln:

1.  Familienstand,
2.  derzeitige Staatsangehörigkeiten,
3.  Einzugsdatum und Auszugsdatum,
4.  Daten zum gesetzlichen Vertreter (Familienname, Vornamen, Doktorgrad, Anschrift, Geburtsdatum, Geschlecht, Sterbedatum, bedingte Sperrvermerke nach § 52 des Bundesmeldegesetzes),
5.  Daten zum Ehegatten oder Lebenspartner (Familienname, Vornamen, Geburtsname, Doktorgrad, derzeitige und letzte frühere Anschriften, Geburtsdatum, Geschlecht, Sterbedatum, bedingte Sperrvermerke nach § 52 des Bundesmeldegesetzes).

(2) Die Meldebehörden und die Registerbehörde dürfen den in § 34 Absatz 4 Satz 1 des Bundesmeldegesetzes genannten Stellen die Daten gemäß § 38 Absatz 1 und 3 des Bundesmeldegesetzes und die nachfolgend aufgeführten Daten zur Erfüllung der in der Zuständigkeit der abrufenden Stelle liegenden Aufgaben aus Anlass der Feststellung der Identität von Einwohnerinnen und Einwohnern und deren Wohnungen im Wege des automatisierten Abrufs übermitteln:

1.  Familienstand,
2.  Daten zum gesetzlichen Vertreter (Familienname, Vornamen, Doktorgrad, Anschrift, Geburtsdatum, Geschlecht, Sterbedatum, bedingte Sperrvermerke nach § 52 des Bundesmeldegesetzes),
3.  Daten zum Ehegatten oder Lebenspartner (Familienname, Vornamen, Geburtsname, Doktorgrad, derzeitige und letzte frühere Anschriften, Geburtsdatum, Geschlecht, Sterbedatum, bedingte Sperrvermerke nach § 52 des Bundesmeldegesetzes).

#### § 4 Datenübermittlungen an die Suchdienste

(1) Die Meldebehörden dürfen den Suchdiensten zur Erfüllung ihrer öffentlich-rechtlichen Aufgaben regelmäßig folgende Daten von den Personen, die aus den in § 1 Absatz 2 Nummer 3 des Bundesvertriebenengesetzes bezeichneten Gebieten stammen, anlässlich ihrer Anmeldung übermitteln:

1.  Familienname,
2.  frühere Namen,
3.  Vornamen,
4.  Geburtsdatum und Geburtsort sowie bei Geburt im Ausland auch den Staat,
5.  derzeitige und frühere Anschriften,
6.  Anschrift am 1.
    September 1939 und
7.  Auskunftssperren nach § 51 des Bundesmeldegesetzes.

(2) Die Datenübermittlung nach § 43 Absatz 2 des Bundesmeldegesetzes darf durch automatisierten Abruf bei der Registerbehörde erfolgen.

#### § 5 Datenübermittlungen zwischen Meldebehörden und öffentlich-rechtlichen Religionsgesellschaften

(1) Die Meldebehörden dürfen den öffentlich-rechtlichen Religionsgesellschaften oder den von ihnen beauftragten Stellen im Fall der Anmeldung, der Abmeldung, im Todesfall oder der Änderung der Kirchenzugehörigkeit die in § 42 Absatz 1 des Bundesmeldegesetzes bestimmten Daten ihrer Mitglieder und die in § 42 Absatz 2 des Bundesmeldegesetzes bestimmten Daten der Familienangehörigen von Mitgliedern öffentlich-rechtlicher Religionsgesellschaften sowie zusätzlich frühere Namen und die Staatsangehörigkeit von Familienangehörigen von Mitgliedern öffentlich-rechtlicher Religionsgesellschaften übermitteln.

(2) Die Meldebehörden dürfen bei der Datenübermittlung nach Absatz 1 auch das Ordnungsmerkmal nach § 4 des Bundesmeldegesetzes übermitteln.

(3) Die Meldebehörden verarbeiten die von öffentlich-rechtlichen Religionsgesellschaften elektronisch (§ 2 Absatz 3) übermittelten Daten von Einwohnerinnen und Einwohnern, die die Zugehörigkeit zur öffentlich-rechtlichen Religionsgesellschaft begründen.

#### § 6 Datenübermittlungen an die Versorgungsverwaltung

Die Registerbehörde darf dem Landesamt für Soziales und Versorgung zur Erfüllung seiner Aufgaben nach dem Bundesversorgungsgesetz und nach den Gesetzen, die dieses Gesetz für entsprechend anwendbar erklären, sowie nach dem Neunten Buch Sozialgesetzbuch und nach dem Strafrechtlichen Rehabilitierungsgesetz aus Anlass der An- und Abmeldung sowie des Todes von Einwohnerinnen und Einwohnern folgende Daten übermitteln:

1.  Familienname,
2.  frühere Namen,
3.  Vornamen,
4.  Geburtsdatum,
5.  Geschlecht,
6.  derzeitige Staatsangehörigkeiten,
7.  derzeitige und letzte frühere Anschriften,
8.  Auszugsdatum,
9.  Auskunftssperren nach § 51 des Bundesmeldegesetzes.

#### § 7 Datenübermittlungen an die Behörden zur Wahrnehmung der Aufgaben der Grundsicherung für Arbeitsuchende

Die Registerbehörde darf den für die Wahrnehmung der Aufgaben der Grundsicherung für Arbeitsuchende nach dem Zweiten Buch Sozialgesetzbuch zuständigen Stellen zur Erfüllung der diesen Stellen obliegenden Aufgaben aus Anlass der An- und Abmeldung sowie des Todes von Einwohnerinnen und Einwohnern folgende Daten übermitteln:

1.  Familienname,
2.  frühere Namen,
3.  Vornamen,
4.  Geburtsdatum,
5.  derzeitige Anschriften,
6.  Auszugsdatum,
7.  Sterbedatum und
8.  Auskunftssperren nach § 51 des Bundesmeldegesetzes.

#### § 8 Datenübermittlungen an den Rundfunk Berlin-Brandenburg

(1) Die Meldebehörden dürfen dem Rundfunk Berlin-Brandenburg (RBB) oder der von ihm nach § 10 Absatz 7 Satz 1 des Rundfunkbeitragsstaatsvertrages mit der Durchführung der Erhebung und des Einzugs von Rundfunkbeiträgen gemäß § 2 des Rundfunkbeitragsstaatsvertrages und zur Ermittlung von Beitragsschuldnern beauftragten Stelle im Fall der Anmeldung, Abmeldung oder des Todes folgende Daten volljähriger Einwohnerinnen und Einwohner monatlich übermitteln:

1.  Familienname,
2.  frühere Namen,
3.  Vornamen,
4.  Doktorgrad,
5.  Geburtsdatum,
6.  derzeitige und letzte frühere Anschriften,
7.  Einzugsdatum, Auszugsdatum und
8.  Sterbedatum.

(2) Sofern ein Rückmeldeverfahren aus Anlass einer Anmeldung, einer Abmeldung ohne Bezug einer neuen Wohnung im Inland oder bei Änderungen des Wohnungsstatus vorgesehen ist, erfolgt die Übermittlung der Daten nach Absatz 1 erst nach Abschluss des Rückmeldeverfahrens.

(3) Ist für eine Einwohnerin oder einen Einwohner eine Auskunftssperre nach § 51 des Bundesmeldegesetzes eingetragen, erfolgt keine Datenübermittlung.

(4) Die übermittelten Daten dürfen nur verwendet werden, um Beginn und Ende der Rundfunkgebührenpflicht sowie die Landesrundfunkanstalt, der die Gebühr zusteht, zu ermitteln.
Daten, die nicht mehr zur Aufgabenerfüllung erforderlich sind, sind spätestens innerhalb eines halben Jahres zu löschen.

#### § 9 Datenübermittlungen an die Finanzämter

Die Meldebehörden dürfen den für ihren Bereich zuständigen Finanzämtern zur Sicherung des Steueraufkommens bei einer Abmeldung in das Ausland monatlich folgende Daten übermitteln:

1.  Familienname,
2.  frühere Namen,
3.  Vornamen,
4.  Geburtsdatum,
5.  derzeitige Anschriften,
6.  Auszugsdatum und
7.  Auskunftssperren nach § 51 des Bundesmeldegesetzes.

#### § 10 Datenübermittlungen an die Gesundheitsämter

Die Meldebehörden dürfen den Gesundheitsämtern zur Erfüllung der Betreuungsaufgaben des Kinder- und Jugendgesundheitsdienstes nach dem Brandenburgischen Gesundheitsdienstgesetz aus Anlass der Geburt eines Kindes folgende Daten übermitteln:

1.  Familienname,
2.  Vornamen,
3.  Geburtsdatum,
4.  Geschlecht,
5.  Daten der Kindesmutter nach § 3 Absatz 1 Nummer 9 des Bundesmeldegesetzes und
6.  Auskunftssperren nach § 51 des Bundesmeldegesetzes.

#### § 11 Datenübermittlungen an die Schulverwaltungsbehörden

(1) Die Meldebehörden dürfen zur Überwachung der allgemeinen Schulpflicht (Vollzeit- und Berufsschulpflicht) Daten an die zuständigen Schulverwaltungsämter der Landkreise und kreisfreien Städte sowie an die schulverwaltenden Stellen der Gemeinden oder Gemeindeverbände (Schulträger) übermitteln.

(2) Die Übermittlung der Daten erfolgt zum 1.
September eines Jahres von den im darauffolgenden Jahr schulpflichtig werdenden Kindern sowie monatlich anlässlich der Anmeldung von Schulpflichtigen, die das 18.
Lebensjahr noch nicht vollendet haben.
Folgende Daten dürfen übermittelt werden:

1.  Familienname,
2.  Vornamen,
3.  Geburtsdatum,
4.  Geschlecht,
5.  derzeitige Staatsangehörigkeiten,
6.  derzeitige Anschriften,
7.  Daten der gesetzlichen Vertreter nach § 3 Absatz 1 Nummer 9 Buchstabe a, b, c, d, h des Bundesmeldegesetzes und
8.  Auskunftssperren nach § 51 des Bundesmeldegesetzes.

#### § 12 Datenübermittlungen an die örtlichen Träger der Jugendhilfe

Die Meldebehörden dürfen den örtlichen Trägern der Jugendhilfe zur Erfüllung ihrer Aufgaben nach § 2 des Gesetzes zur Kooperation und Information im Kinderschutz vom 22. Dezember 2011 (BGBl. I S. 2975) aus Anlass der Geburt eines Kindes folgende Daten übermitteln:

1.  Familienname,
2.  Vornamen,
3.  Geburtsdatum,
4.  Daten der Kindesmutter nach § 3 Absatz 1 Nummer 9 des Bundesmeldegesetzes und
5.  Auskunftssperren nach § 51 des Bundesmeldegesetzes.

#### § 13 Datenübermittlungen zur Erfassung öffentlich geförderter Wohnungen

Die Meldebehörden dürfen zur Führung und Aktualisierung der Wohnraumdatei den für die Sicherung der Zweckbestimmung von öffentlich geförderten Wohnungen nach dem Wohnungsbindungsgesetz in der Fassung der Bekanntmachung vom 13.
September 2001 (BGBl. I S. 2404), das zuletzt durch Artikel 2 des Gesetzes vom 9.
November 2012 (BGBl. I S. 2291, 2292) geändert worden ist, und nach dem Wohnraumförderungsgesetz vom 13.
September 2001 (BGBl. I S. 2376), das zuletzt durch Artikel 2 des Gesetzes vom 9.
Dezember 2010 (BGBl. I S. 1885, 1893) geändert worden ist, zuständigen Stellen, in der Regel den Wohnungsämtern, aus Anlass der An- und Abmeldung sowie des Todes von Einwohnerinnen und Einwohnern, die in eine der bezeichneten Wohnungen einziehen oder aus einer solchen ausziehen, folgende Daten übermitteln:

1.  Familienname,
2.  Vornamen,
3.  Geburtsdatum,
4.  derzeitige Anschriften,
5.  Einzugsdatum, Auszugsdatum und
6.  Auskunftssperren nach § 51 des Bundesmeldegesetzes.

#### § 14 Datenübermittlungen zur Ehrung von Alters-, Ehe- und Lebenspartnerschaftsjubiläen

(1) Die Meldebehörden dürfen Daten für die Ehrung von Alters- Ehe- und Lebenspartnerschaftsjubiläen (§ 50 Absatz 2 Satz 2 des Bundesmeldegesetzes) übermitteln.
Die Übermittlung erfolgt mindestens sechs Wochen vor dem jeweiligen Ereignis

1.  bei Vollendung des 70., 75., 80., 85., 90.
    und 95.
    Lebensjahres sowie bei dem 50.
    und 60.
    Ehe- oder Lebenspartnerschaftsjubiläum an die Landrätinnen und Landräte,
2.  in den übrigen Fällen durch die Meldebehörden der Ämter und amtsfreien Gemeinden an die Landrätinnen und die Landräte zur Weiterübermittlung an die Staatskanzlei und durch die Meldebehörden der kreisfreien Städte an die Staatskanzlei.

(2) Folgende Daten dürfen übermittelt werden:

1.  Familienname,
2.  frühere Namen,
3.  Vornamen,
4.  Doktorgrad,
5.  derzeitige Anschrift,
6.  Geburtsdatum bei Altersjubilaren,
7.  Datum der Eheschließung oder der Begründung der Lebenspartnerschaft bei Ehe- oder Lebenspartnerschaftsjubilaren,
8.  derzeitige Staatsangehörigkeiten,
9.  Auskunftssperren nach § 51 des Bundesmeldegesetzes und
10.  Übermittlungssperren nach § 50 Absatz 5 in Verbindung mit § 50 Absatz 2 des Bundesmeldegesetzes.

#### § 15 Datenübermittlungen an die Bürgermeisterinnen und Bürgermeister amtsangehöriger Gemeinden

(1) Die Meldebehörden dürfen den Bürgermeisterinnen und den Bürgermeistern der amtsangehörigen Gemeinden zur Erfüllung ihrer Aufgaben folgende Daten von Einwohnerinnen und Einwohnern ihres Gemeindegebietes übermitteln:

1.  bei der Geburt eines Kindes
    1.  Familienname,
    2.  Vornamen,
    3.  Geburtsdatum,
    4.  derzeitige Anschrift der alleinigen Wohnung oder der Hauptwohnung und
    5.  Auskunftssperren nach § 51 des Bundesmeldegesetzes;
2.  bei dem Tod einer Einwohnerin oder eines Einwohners
    1.  Familienname,
    2.  Vornamen,
    3.  Doktorgrad,
    4.  Geburtsdatum,
    5.  letzte Anschrift der alleinigen Wohnung oder der Hauptwohnung,
    6.  Sterbedatum und
    7.  Auskunftssperren nach § 51 des Bundesmeldegesetzes.

(2) Zum Zweck der Ehrung von Alters-, Ehe- und Lebenspartnerschaftsjubiläen (§ 14) dürfen den ehrenamtlichen Bürgermeisterinnen und Bürgermeistern sowie den Ortsvorsteherinnen und Ortsvorstehern die in § 14 Absatz 2 genannten Daten übermittelt werden.
Die Übermittlung erfolgt mindestens sechs Wochen vor dem jeweiligen Ereignis.

#### § 16 Datenübermittlungen an die für Wasserver- und Abwasserentsorgung zuständigen Behörden

(1) Die Meldebehörden dürfen den für die Wasserver- und Abwasserentsorgung zuständigen Behörden oder den von ihnen beauftragten Stellen die nach den Satzungsvorschriften zur Bemessung, Festsetzung und Erhebung von Gebühren erforderlichen Daten zu den rechtlich maßgebenden Stichtagen übermitteln.
Anlässlich der An- und Abmeldung, der Geburt und des Todes von Einwohnerinnen und Einwohnern darf die Übermittlung der Daten vierteljährlich erfolgen.

(2) Folgende Daten dürfen übermittelt werden:

1.  Familienname,
2.  Vornamen,
3.  Geburtsdatum,
4.  derzeitige Anschriften,
5.  Einzugsdatum, Auszugsdatum,
6.  Sterbedatum und
7.  Auskunftssperren nach § 51 des Bundesmeldegesetzes.

#### § 17 Datenübermittlungen an die für die Abfallentsorgung zuständigen Behörden

(1) Die Meldebehörden dürfen den für die Abfallentsorgung zuständigen Behörden oder den von ihnen beauftragten Stellen die nach den Satzungsvorschriften zur Bemessung, Festsetzung und Erhebung von Gebühren erforderlichen Daten übermitteln.
Die Daten aller Einwohnerinnen und Einwohner dürfen zu den rechtlich maßgebenden Stichtagen übermittelt werden.
Anlässlich der An- und Abmeldung, der Geburt und des Todes darf die Übermittlung der Daten vierteljährlich erfolgen.

(2) Folgende Daten dürfen übermittelt werden:

1.  Familienname,
2.  Vornamen,
3.  Geburtsdatum,
4.  derzeitige Anschriften,
5.  Einzugsdatum, Auszugsdatum,
6.  Sterbedatum und
7.  Auskunftssperren nach § 51 des Bundesmeldegesetzes.

#### § 18 Datenübermittlung an die Vertrauensstelle

(1) Aufgrund des Artikels 3 Absatz 5 bis 7 des Staatsvertrages über das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen vom 20./24.
November 1997, der zuletzt durch den Zweiten Staatsvertrag zur Änderung des Staatsvertrages über das Gemeinsame Krebsregister vom 29. März/2. Mai 2017 geändert worden ist, übermittelt die Registerbehörde zur Aktualisierung und Berichtigung der im Gemeinsamen Krebsregister gespeicherten Daten an dessen Vertrauensstelle halbjährlich die folgenden Daten zu den Personen, die im Kalenderhalbjahr vor der Datenübermittlung verstorben sind, sich an- oder abgemeldet haben oder deren Name sich geändert hat:

1.  Familiennamen,
2.  frühere Namen,
3.  Vornamen,
4.  Geburtsdatum,
5.  Geschlecht,
6.  derzeitige und letzte frühere Anschrift der alleinigen Wohnung oder der Hauptwohnung,
7.  Einzugsdatum, Auszugsdatum,
8.  Datum der Namensänderung und
9.  Sterbedatum.

Sofern ein Rückmeldeverfahren aus Anlass einer Anmeldung oder einer Abmeldung ohne Bezug einer neuen Wohnung im Inland oder bei Änderungen des Wohnungsstatus vorgesehen ist, erfolgt die Übermittlung der Daten erst nach Abschluss des Rückmeldeverfahrens.
Von der Übermittlung von Daten ist bei Bestehen einer Auskunftssperre nach § 51 Absatz 1 oder 5 des Bundesmeldegesetzes abzusehen.

(2) Die Registerbehörde darf der Vertrauensstelle des Gemeinsamen Krebsregisters einmalig die Daten im Sinne des Absatzes 1 zu den zurückliegenden fünf Kalenderjahren übermitteln.

#### § 19 Verwaltungsvorschriften

Das Ministerium des Innern und für Kommunales kann zu den in dieser Verordnung bestimmten Datenübermittlungen weitere Verfahrenshinweise durch Verwaltungsvorschriften festlegen.

#### § 20 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über regelmäßige Datenübermittlungen der Meldebehörden vom 7.
August 1997 (GVBl.
II S. 734), die zuletzt durch die Verordnung vom 27.
September 2012 (GVBl.
II Nr. 83) geändert worden ist, außer Kraft.

Potsdam, den 2.
November 2015

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter