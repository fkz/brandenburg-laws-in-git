## Verordnung über die elektronische Rechnungsstellung bei öffentlichen Aufträgen im Land Brandenburg (Brandenburgische E-Rechnungsverordnung - BbgERechV)

Auf Grund des § 5 Absatz 2 Satz 2 und 3 des Brandenburgischen E\-Government-Gesetzes vom 23. November 2018 (GVBl.
I Nr. 28) verordnet der Minister der Finanzen im Einvernehmen mit dem Minister des Inneren und für Kommunales:

#### § 1 Geltungsbereich

(1) Diese Verordnung gilt grundsätzlich für alle Rechnungen, mit denen eine Lieferung oder eine sonstige Leistung abgerechnet wird und die nach Erfüllung von Aufträgen und öffentlichen Aufträgen sowie zu Konzessionen ausgestellt wurden, soweit diese Verordnung keine abweichenden Bestimmungen für geheimhaltungsbedürftige Rechnungsdaten enthält.

(2) Auftraggebende im Sinne des Teiles 4 des Gesetzes gegen Wettbewerbsbeschränkungen stellen den Empfang und die Verarbeitung elektronischer Rechnungen nach Erfüllung von öffentlichen Aufträgen, Aufträgen sowie zu Konzessionen sicher, soweit für sie gemäß § 159 des Gesetzes gegen Wettbewerbsbeschränkungen eine Vergabekammer des Landes Brandenburg zuständig ist und soweit diese Verordnung keine abweichenden Bestimmungen enthält.
Diese Verpflichtung gilt unabhängig davon, ob der Wert des vergebenen öffentlichen Auftrags, des vergebenen Auftrags oder der Vertragswert der vergebenen Konzession den gemäß § 106 des Gesetzes gegen Wettbewerbsbeschränkungen jeweils maßgeblichen Schwellenwert erreicht oder überschreitet.

#### § 2 Begriffsbestimmungen

(1) Eine Rechnung ist jedes Dokument, mit dem eine Lieferung oder eine sonstige Leistung abgerechnet wird, gleichgültig, wie dieses Dokument im Geschäftsverkehr bezeichnet wird.

(2) Eine elektronische Rechnung ist jedes Dokument im Sinne von Absatz 1, wenn

1.  es in einem strukturierten elektronischen Format ausgestellt, übermittelt und empfangen wird und
2.  das Format die automatische und elektronische Verarbeitung des Dokuments ermöglicht.

(3) Rechnungsstellende sind alle Unternehmer im Sinne von § 14 Absatz 1 des Bürgerlichen Gesetzbuches, die eine Rechnung an Rechnungsempfangende im Sinne von Absatz 4 ausstellen und übermitteln.

(4) Rechnungsempfangende sind alle Stellen im Sinne von § 1 Absatz 2 dieser Verordnung.

(5) Rechnungssendende sind alle Unternehmer im Sinne von § 14 Absatz 1 des Bürgerlichen Gesetzbuches, die eine elektronische Rechnung im Auftrag des Rechnungsstellenden ausstellen und übermitteln.

#### § 3 Elektronisches Empfangsverfahren

(1) Rechnungsstellende können Rechnungen gegenüber Rechnungsempfangenden in elektronischer Form ausstellen und übermitteln.
Sie können sich hierbei der Dienstleistung von Rechnungssendenden bedienen.

(2) Rechnungsempfangende, die zur juristischen Person Land Brandenburg gehören, müssen die nach Absatz 1 ausgestellten und übermittelten Rechnungen unter Nutzung des Verwaltungsportals nach § 4 Absatz 2 elektronisch empfangen.
Ausnahmen bedürfen der Zustimmung des für Finanzen zuständigen Ministeriums.

(3) Rechnungsempfangende, die nicht zur juristischen Person Land Brandenburg gehören, können die nach Absatz 1 ausgestellten und übermittelten Rechnungen unter Nutzung des Verwaltungsportals nach § 4 Absatz 2 elektronisch empfangen.
Sie können auch andere elektronische Verfahren zum Empfang nutzen.

#### § 4 Anforderungen an das Rechnungsdatenmodell und an die Übermittlung

(1) Für die Ausstellung von elektronischen Rechnungen haben Rechnungsstellende und Rechnungssendende grundsätzlich den Datenaustauschstandard XRechnung vom 29. September 2017 (BAnz AT 10.10.2017 B1) in der jeweils aktuellen Fassung zu verwenden.
Es kann auch ein anderer Datenaustauschstandard verwendet werden, wenn er den Anforderungen der europäischen Norm für die elektronische Rechnungsstellung entspricht.

(2) Für die Übermittlung von elektronischen Rechnungen an die juristische Person Land Brandenburg haben Rechnungsstellende und Rechnungssendende das vom Land zur Verfügung gestellte Verwaltungsportal im Sinne von § 2 Absatz 2 des Onlinezugangsgesetzes vom 14. August 2017 (BGBl.
I S. 3122, 3138) zu nutzen.
Voraussetzung für die Übermittlung einer elektronischen Rechnung ist, dass der Rechnungsstellende oder Rechnungssendende sich zuvor registriert hat.
Elektronische Rechnungen, die über das vom Land zur Verfügung gestellte Verwaltungsportal übermittelt werden, sind automationsunterstützt auf ihre formale Fehlerlosigkeit zu prüfen.
Sobald die ordnungsgemäße Übermittlung einer elektronischen Rechnung festgestellt ist, ist der Rechnungsstellende oder der Rechnungssendende automationsunterstützt davon zu benachrichtigen.
Eine formal fehlerhafte elektronische Rechnung ist automationsunterstützt abzulehnen.
In diesem Fall ist der Rechnungsstellende oder der Rechnungssendende über die Ablehnung zu informieren.
Das für Finanzen zuständige Ministerium kann im Einzelfall Ausnahmen von Satz 1 zulassen.

(3) Andere Rechnungsempfangende können, auch wenn sie von § 3 Absatz 3 Satz 2 Gebrauch machen, zusätzlich das vom Land zur Verfügung gestellte Verwaltungsportal nach Absatz 2 nutzen.

(4) Die Bereitstellung des Verwaltungsportals nach Absatz 2 obliegt dem für Finanzen zuständigen Ministerium, das sich zur Aufgabenerfüllung und beim Betrieb des Portals der Leistungen Dritter bedienen kann.
Für die Erfüllung der Aufgaben nach Satz 1 sind das für Finanzen zuständige Ministerium und von ihm beauftragte Dritte in dem erforderlichen Umfang zur Verarbeitung personenbezogener Daten befugt.
Soweit das für Finanzen zuständige Ministerium personenbezogene Daten für die genannten Zwecke verarbeitet, ist es Verantwortlicher im Sinne des Artikels 4 Nummer 7 der Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates vom 27.
April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr und zur Aufhebung der Richtlinie 95/46/EG (Datenschutz-Grundverordnung) (ABl. L 119 vom 4.5.2016, S. 1; L 314 vom 22.11.2016, S.
72; L 127 vom 23.5.2018, S.
2); dies gilt insbesondere für die Übermittlung der auf dem Verwaltungsportal nach Absatz 2 eingegangenen Rechnungen an Rechnungsempfangende im Land Brandenburg.
Im Übrigen sind die Rechnungsempfangenden in Bezug auf die Weiterverarbeitung personenbezogener Rechnungsdaten für eigene fachliche Aufgabenzwecke und die Gewährleistung der Rechte der Betroffenen nach Artikel 12 bis 21 der Verordnung (EU) 2016/679 Verantwortliche im Sinne des Artikels 4 Nummer 7 der Datenschutz-Grundverordnung.

(5) Erhält ein Rechnungsempfangender eine elektronische Rechnung, die keinem Rechnungsstellenden zugeordnet werden kann, hat der Rechnungsempfangende die elektronische Rechnung abzulehnen.
Einer Information der Rechnungsstellenden oder der Rechnungssendenden über die Ablehnung bedarf es dabei nicht.

(6) Die Verpflichtungen gemäß den Absätzen 1 und 2 gelten nicht für die Übermittlung von elektronischen Rechnungen, die in Verfahren der Organleihe nach § 159 Absatz 1 Nummer 5 des Gesetzes gegen Wettbewerbsbeschränkungen auszustellen sind.

#### § 5 Inhalt der elektronischen Rechnung

(1) Die elektronische Rechnung hat neben den umsatzsteuerrechtlichen Rechnungsbestandteilen mindestens folgende Angaben zu enthalten:

1.  eine Leitweg-Identifikationsnummer,
2.  die Bankverbindungsdaten,
3.  die Zahlungsbedingungen und
4.  die E-Mail\-Adresse des Rechnungsstellenden.

(2) Die elektronische Rechnung hat zusätzlich zu den Angaben nach Absatz 1 mindestens folgende Angaben zu enthalten, wenn diese dem Rechnungssteller bereits bei Beauftragung übermittelt wurden:

1.  die Lieferantennummer,
2.  eine Bestellnummer,
3.  ein Aktenzeichen, sofern vorhanden.

(3) Die Vorgaben nach den Absätzen 1 und 2 gelten nicht für Rechnungen, die in Verfahren der Organleihe nach § 159 Absatz 1 Nummer 5 des Gesetzes gegen Wettbewerbsbeschränkungen auszustellen sind.

#### § 6 Verarbeitung von elektronischen Rechnungen

(1) Rechnungsempfangende, die das Standardverfahren des Landes für das Haushalts-, Kassen- und Rechnungswesen (HKR) nutzen, haben die gemäß § 4 Absatz 2 übermittelten elektronischen Rechnungen entsprechend den Vorgaben des für Finanzen zuständigen Ministeriums zu verarbeiten.

(2) Rechnungsempfangende, die nicht das Standardverfahren des Landes für das Haushalts-, Kassen- und Rechnungswesen (HKR) nutzen, haben die gemäß § 4 Absatz 3 übermittelten elektronischen Rechnungen nach pflichtgemäßem Ermessen zu verarbeiten.

(3) Die Absätze 1 und 2 gelten nicht für Rechnungen, die in Verfahren der Organleihe nach § 159 Absatz 1 Nummer 5 des Gesetzes gegen Wettbewerbsbeschränkungen auszustellen sind.

#### § 7 Schutz personenbezogener Daten

Personenbezogene Daten, die durch elektronische Rechnungsstellung übermittelt und empfangen wurden, dürfen vom Rechnungsempfangenden nur zur Erfüllung der Aufgaben nach dieser Rechtsverordnung und zur Erfüllung der haushaltsrechtlichen Vorgaben verarbeitet werden.

#### § 8 Ausnahmen für geheimhaltungsbedürftige Rechnungsdaten

Rechnungsdaten, die gemäß § 6 Absatz 2 Nummer 1 bis 3 des Brandenburgischen Sicherheitsüberprüfungsgesetzes vom 30.
Juli 2001 (GVBl.
I S.
126), das zuletzt durch Artikel 14 des Gesetzes vom 8.
Mai 2018 (GVBl.
I Nr.
8 S.13) geändert worden ist, in der jeweils geltenden Fassung, geheimhaltungsbedürftig sind, sind vom Geltungsbereich dieser Verordnung ausgenommen.

#### § 9 Übergangsbestimmungen

Für Rechnungsempfangende, die nicht zur juristischen Person Land Brandenburg gehören, gilt die Verpflichtung aus § 1 Absatz 2 Satz 2 hinsichtlich des Wertes des vergebenen öffentlichen Auftrags, des vergebenen Auftrags oder der Vertragswert der vergebenen Konzession, soweit dieser den gemäß § 106 des Gesetzes gegen Wettbewerbsbeschränkungen jeweils maßgeblichen Schwellenwert nicht erreicht, ab dem 1.
Januar 2025.

#### § 10 Inkrafttreten

Diese Verordnung tritt am 1.
April 2020 in Kraft.

Potsdam, den 19.
September 2019

Der Minister der Finanzen

Christian Görke