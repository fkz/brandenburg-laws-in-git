## Verordnung über die Feststellung gleichwertiger Fähigkeiten für Arbeitsfelder im Berufsfeld von staatlich anerkannten Erzieherinnen und Erziehern (Erzieher-Gleichwertigkeits-Verordnung - ErzGleichV)

Auf Grund des § 7 Absatz 2 des Brandenburgischen Sozialberufsgesetzes vom 3.
Dezember 2008 (GVBl.
I S. 278) verordnet der Minister für Bildung, Jugend und Sport:

#### § 1 Gleichwertige Fähigkeiten im Berufsfeld von staatlich anerkannten Erzieherinnen und Erziehern

(1) Gleichwertige Fähigkeiten für das Arbeitsfeld der Kindertagesbetreuung oder für das Arbeitsfeld der stationären und teilstationären Hilfen zur Erziehung werden nach § 7 Absatz 1 des Brandenburgischen Sozialberufsgesetzes auf Antrag bescheinigt.
Voraussetzung für die Bescheinigung ist die Feststellung, dass im Rahmen einer Qualifizierungsmaßnahme Fähigkeiten in dem jeweiligen Arbeitsfeld erworben worden sind, die denen, die im Rahmen der Fachschulausbildung nach § 4 Absatz 1 Nummer 1 Buchstabe b des Brandenburgischen Sozialberufsgesetzes erworben werden, gleichwertig sind.

(2) Die Gleichwertigkeit nach Absatz 1 setzt eine gemäß den Rahmenvorgaben nach Anlage 2 durchzuführende zweijährige tätigkeitsbegleitende Qualifizierungsmaßnahme zum Erwerb gleichwertiger Fähigkeiten im jeweiligen Arbeitsfeld voraus, die insgesamt einen Umfang von 1 200 Seminarstunden und 2 100 Zeitstunden praktische Tätigkeit erfordert.
Die Seminarstunden sollen auf 15 Wochen und die Zeitstunden der praktischen Tätigkeit auf 30 Wochen pro Jahr verteilt werden, wobei über den gesamten Zeitraum der Qualifizierung auf eine Seminarwoche zwei Wochen praktische Tätigkeit folgen müssen.
Die praktische Tätigkeit soll in Kindertageseinrichtungen oder in Einrichtungen der stationären und teilstationären Hilfen zur Erziehung (Praxisstellen) unter Berücksichtigung der Vorgabe nach Anlage 2 Nummer 6 abgeleistet werden.

#### § 2 Genehmigung der Qualifizierungsmaßnahme

Qualifizierungsmaßnahmen nach § 1 müssen für die Bescheinigung gleichwertiger Fähigkeiten für das jeweilige Arbeitsfeld vor deren Beginn von dem für Jugend zuständigen Ministerium schriftlich genehmigt worden sein.
Für die Genehmigung zur Durchführung der Maßnahmen sind von dem jeweiligen Bildungsträger die inhaltlich-organisatorischen Voraussetzungen nach Anlage 1 nachzuweisen.

#### § 3 Aufnahme in die Qualifizierungsmaßnahme

Voraussetzungen für die Aufnahme in die tätigkeitsbegleitende Qualifizierung nach § 1 sind ein mittlerer Schulabschluss sowie eine abgeschlossene Berufsausbildung.
Die Entscheidung über die Aufnahme trifft der jeweilige Bildungsträger unter Berücksichtigung der zusätzlichen Voraussetzungen gemäß den Rahmenvorgaben nach Anlage 2 Nummer 5.

#### § 4 Erteilung der Bescheinigung gleichwertiger Fähigkeiten

(1) Die Erteilung der Bescheinigung gleichwertiger Fähigkeiten gemäß Anlage 3 oder Anlage 4 erfolgt auf Antrag von der nach § 5 zuständigen Behörde.

(2) Zum individuellen Nachweis der Voraussetzungen für die Erteilung der Bescheinigung gleichwertiger Fähigkeiten ist von der antragstellenden Person das Zertifikat über den erfolgreichen und als gleichwertig nach § 1 Absatz 1 festgestellten Abschluss der Qualifizierungsmaßnahme vorzulegen.
Das Zertifikat hat den Umfang und die Inhalte der Qualifizierung sowie die erworbenen Fähigkeiten im jeweiligen Arbeitsfeld auszuweisen.

(3) Eine Kopie der Bescheinigung nach Absatz 1 wird bei der nach § 5 zuständigen Behörde 30 Jahre aufbewahrt.

#### § 5 Zuständige Behörde

(1) Das Sozialpädagogische Fortbildungsinstitut Berlin-Brandenburg ist die zuständige Behörde für die Erteilung der Bescheinigung gleichwertiger Fähigkeiten für das Arbeitsfeld der Kindertagesbetreuung.

(2) Für das Arbeitsfeld der stationären und teilstationären Hilfen zur Erziehung ist das für Jugend zuständige Ministerium die zuständige Behörde für die Erteilung der Bescheinigung gleichwertiger Fähigkeiten.

#### § 6 Übergangsvorschriften

Für die zum Zeitpunkt des Inkrafttretens dieser Verordnung noch nicht beendeten Qualifizierungsmaßnahmen sind die zum Zeitpunkt ihrer Genehmigung geltenden Rahmenvorgaben für eine tätigkeitsbegleitende Qualifizierung zur Erzieherin/zum Erzieher für den Bereich der Kindertagesbetreuung im Land Brandenburg sowie für das Verfahren zur Erteilung der Bescheinigung über die Feststellung gleichwertiger Fähigkeiten vom 18.
August 2009 (ABl.
MBJS S.
307) zugrunde zu legen.

#### § 7 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 11.
September 2017

Der Minister für Bildung,  
Jugend und Sport

Günter Baaske

* * *

### Anlagen

1

[Anlage 1 (zu § 2) - Inhaltlich-organisatorische Voraussetzungen für die Genehmigung von Qualifizierungsmaßnahmen](/br2/sixcms/media.php/68/GVBl_II_48_2017-Anlage-1.pdf "Anlage 1 (zu § 2) - Inhaltlich-organisatorische Voraussetzungen für die Genehmigung von Qualifizierungsmaßnahmen") 614.5 KB

2

[Anlage 2 (zu § 1 Absatz 2, § 3) - Rahmenvorgaben für eine tätigkeitsbegleitende Qualifizierung zum Erwerb gleichwertiger Fähigkeiten ...](/br2/sixcms/media.php/68/GVBl_II_48_2017-Anlage-2.pdf "Anlage 2 (zu § 1 Absatz 2, § 3) - Rahmenvorgaben für eine tätigkeitsbegleitende Qualifizierung zum Erwerb gleichwertiger Fähigkeiten ...") 681.1 KB

3

[Anlage 3 (zu § 4 Absatz 1) - Bescheinigung gleichwertiger Fähigkeiten Kita](/br2/sixcms/media.php/68/GVBl_II_48_2017-Anlage-3.pdf "Anlage 3 (zu § 4 Absatz 1) - Bescheinigung gleichwertiger Fähigkeiten Kita") 597.8 KB

4

[Anlage 4 (zu § 4 Absatz 1) - Bescheinigung gleichwertiger Fähigkeiten HzE](/br2/sixcms/media.php/68/GVBl_II_48_2017-Anlage-4.pdf "Anlage 4 (zu § 4 Absatz 1) - Bescheinigung gleichwertiger Fähigkeiten HzE") 613.6 KB