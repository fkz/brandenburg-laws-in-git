## Verordnung zur Übertragung von Ermächtigungen zum Erlass von Rechtsverordnungen nach dem Brandenburgischen Vergabegesetz  (Brandenburgische Vergabegesetz-Zuständigkeitsübertragungsverordnung - BbgVergGZÜV)

Auf Grund des § 3 Absatz 2 Satz 8, des § 4 Absatz 4 und des § 10 Absatz 2 des Brandenburgischen Vergabegesetzes vom 21.
September 2011 (GVBl.
I Nr. 19) verordnet die Landesregierung:

### § 1  
Übertragung von Ermächtigungen auf das für Arbeit zuständige Mitglied der Landesregierung

Die in § 4 Absatz 1 Satz 6 und 7 und § 7 Absatz 2 und 3 des Brandenburgischen Vergabegesetzes enthaltenen Ermächtigungen zum Erlass von Rechtsverordnungen werden auf das für Arbeit zuständige Mitglied der Landesregierung übertragen.

### § 2  
Übertragung von Ermächtigungen auf das für Wirtschaft zuständige Mitglied der Landesregierung

Die in § 14 Absatz 1 Nummer 1 bis 3 des Brandenburgischen Vergabegesetzes enthaltenen Ermächtigungen zum Erlass von Rechtsverordnungen werden auf das für Wirtschaft zuständige Mitglied der Landesregierung übertragen.

### § 3  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 29.
März 2012

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident  
Matthias Platzeck

Der Minister für Wirtschaft  
und Europaangelegenheiten

Ralf Christoffers

Der Minister für Arbeit,  
Soziales, Frauen und Familie

Günter Baaske