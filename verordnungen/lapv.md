## Verordnung über die Ausbildung von Lehrkräften zur Deckung des Unterrichtsbedarfs an Schulen im Land Brandenburg und deren Staatsprüfung (Lehrkräfteausbildungs- und -prüfungsverordnung - LAPV)

Auf Grund des § 5 Absatz 8, des § 6 Absatz 4 und des § 8 Absatz 5 in Verbindung mit § 7 Absatz 1 und 2 des Brandenburgischen Lehrerbildungsgesetzes vom 18. Dezember 2012 (GVBl.
I Nr. 45), die zuletzt durch Gesetz vom 31.
Mai 2018 (GVBl.
I Nr.
10) geändert worden sind, verordnet die Ministerin für Bildung, Jugend und Sport im Einvernehmen mit dem Minister des Innern und für Kommunales und dem Minister der Finanzen:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeine Vorschriften

[§ 1 Geltungsbereich, Ziel](#1)

[§ 2 Ausbildungsdauer](#2)

[§ 3 Ausbildungs- und Prüfungsbehörde](#3)

### Abschnitt 2  
Berufsbegleitender Vorbereitungsdienst

### Unterabschnitt 1  
Ausbildungsplätze, Teilnahme

[§ 4 Ausbildungsplätze](#4)

[§ 5 Teilnahmevoraussetzungen](#5)

[§ 6 Antrag auf Teilnahme](#6)

[§ 7 Entscheidung über die Teilnahme](#7)

### Unterabschnitt 2  
Ausbildung

[§ 8 Ausbildungsorganisation](#8)

[§ 9 Beurteilungen](#9)

[§ 10 Verantwortlichkeiten](#10)

[§ 11 Vorzeitige Beendigung der Ausbildung](#11)

### Abschnitt 3  
Besonderer Zugang zum Vorbereitungsdienst

[§ 12 Ausbildungsplätze](#12)

[§ 13 Voraussetzungen für den besonderen Zugang zum Vorbereitungsdienst](#13)

[§ 14 Zulassung](#14)

[§ 15 Ausbildung und Beurteilungen](#15)

### Abschnitt 4  
Staatsprüfung

[§ 16 Staatsprüfung, Zeugnis und Bescheinigungen](#16)

### Abschnitt 5  
Besondere Staatsprüfung

[§ 17 Grundsätze und Ziel](#17)

[§ 18 Zulassungsvoraussetzungen](#18)

[§ 19 Prüfungsleistungen](#19)

[§ 20 Zulassungsantrag](#20)

[§ 21 Zulassung zur Prüfung](#21)

[§ 22 Prüfungsausschuss](#22)

[§ 23 Notenbildung und Gesamtnote der besonderen Staatsprüfung](#23)

[§ 24 Wiederholung der Prüfung](#24)

[§ 25 Zeugnis und Bescheinigungen](#25)

### Abschnitt 6  
Schlussbestimmungen

[§ 26 Übergangsregelung](#26)

[§ 27 Inkrafttreten, Außerkrafttreten](#27)

### Abschnitt 1  
Allgemeine Vorschriften

#### § 1 Geltungsbereich, Ziel

Diese Verordnung regelt das Nähere

1.  zur Ausbildung und Staatsprüfung für Personen, die auf Grund eines bestehenden Unterrichtsbedarfs, der nicht durch Lehrkräfte mit einer Lehramtsbefähigung dauerhaft gedeckt werden kann, mit dem Ziel ausgebildet werden sollen, die Befähigung für ein Lehramt gemäß § 2 Absatz 1 des Brandenburgischen Lehrerbildungsgesetzes zu erwerben und
2.  zur besonderen Staatsprüfung gemäß § 8 Absatz 3 des Brandenburgischen Lehrerbildungsgesetzes.

#### § 2 Ausbildungsdauer

Die Ausbildung im berufsbegleitenden Vorbereitungsdienst und beim besonderen Zugang zum Vorbereitungsdienst dauert 24 Monate.
Soweit in dieser Verordnung Regelungen zur vorzeitigen Beendigung der Ausbildung getroffen werden, bleiben diese unberührt.

#### § 3 Ausbildungs- und Prüfungsbehörde

Ausbildungs- und Prüfungsbehörde ist das für Schule zuständige Ministerium.
Die ihm zugehörigen Studienseminare sind für die Organisation und die Durchführung der Ausbildung und der Staatsprüfung sowie der besonderen Staatsprüfung zuständig.

### Abschnitt 2  
Berufsbegleitender Vorbereitungsdienst

### Unterabschnitt 1  
Ausbildungsplätze, Teilnahme

#### § 4 Ausbildungsplätze

(1) Die Ausbildung erfolgt im Rahmen der zur Verfügung stehenden personellen, sächlichen und organisatorischen Kapazitäten.

(2) Das für Schule zuständige Ministerium entscheidet gemäß Absatz 1 und auf der Grundlage des von den staatlichen Schulämtern festgestellten Lehrkräftebedarfs über die lehramts- und fachbezogene Anzahl der Ausbildungsplätze für den berufsbegleitenden Vorbereitungsdienst.

(3) Das für Schule zuständige Ministerium gibt in seinem Amtsblatt rechtzeitig vor Beginn der Ausbildung insbesondere die gemäß Absatz 2 zur Verfügung gestellten Ausbildungsplätze, den Ausbildungsbeginn, die für die Ausbildung zuständigen Studienseminare und die Bewerbungsfrist bekannt.

#### § 5 Teilnahmevoraussetzungen

(1) Am berufsbegleitenden Vorbereitungsdienst kann teilnehmen, wer

1.  aus Bedarfsgründen in den öffentlichen Schuldienst unbefristet oder befristet mit dem Ziel einer unbefristeten Beschäftigung als Lehrkraft im Land Brandenburg eingestellt wurde,
2.  einen nicht lehramtsbezogenen Hochschulabschluss gemäß Absatz 2 nachweist, wobei ein Bachelorabschluss nicht ausreicht und
3.  über Kenntnisse in der deutschen Sprache auf dem Niveau C 2 des Gemeinsamen Europäischen Referenzrahmens für Sprache verfügt, sofern Deutsch nicht Muttersprache ist.

Lehrkräfte an Ersatzschulen im Land Brandenburg können am berufsbegleitenden Vorbereitungsdienst teilnehmen, wenn nach der Entscheidung über die Teilnahme von Lehrkräften im öffentlichen Schuldienst freie Ausbildungsplätze zur Verfügung stehen.
Satz 1 Nummer 2 und 3 gilt entsprechend.

(2) Mit dem Hochschulabschluss gemäß Absatz 1 Satz 1 Nummer 2 sind die fachwissenschaftlichen oder künstlerischen Bildungsvoraussetzungen nachzuweisen, die den Unterrichtseinsatz in mindestens zwei Fächern, Fachrichtungen oder Lernbereichen (Fächer) ermöglichen, wobei mindestens für eines der beiden Fächer ein Bedarf gemäß § 4 Absatz 2 festgestellt sein muss.

(3) Die fachwissenschaftlichen oder künstlerischen Bildungsvoraussetzungen sind erfüllt, wenn die Inhalte und der Umfang des absolvierten Studiums

1.  in mindestens einem Fach in der Regel mindestens drei Viertel und
2.  in einem weiteren Fach in der Regel mindestens die Hälfte

der gemäß der Lehramtsstudienverordnung für das jeweilige Lehramt vorgesehenen Anforderungen nachgewiesen werden.
Soweit diese Anforderungen nicht erfüllt werden, können fehlende fachwissenschaftliche oder künstlerische Kenntnisse oder Fähigkeiten durch weitere oder ergänzende Studien vor Beginn des berufsbegleitenden Vorbereitungsdienstes erworben werden.

(4) Wird das Lehramt für die Primarstufe angestrebt, gilt Absatz 3 mit der Maßgabe, dass die fachwissenschaftlichen Bildungsvoraussetzungen in mindestens einem der Fächer Deutsch, Englisch oder Mathematik nachzuweisen sind.
Ein weiteres Fach muss den für die Primarstufe zugelassenen Fächern gemäß der Lehramtsstudienverordnung entsprechen.

(5) Die Teilnahme am berufsbegleitenden Vorbereitungsdienst ist nicht möglich, wenn eine Ausbildung im berufsbegleitenden Vorbereitungsdienst oder ein Vorbereitungsdienst gemäß Abschnitt 3 schon einmal begonnen und die sie jeweils abschließende Staatsprüfung oder eine besondere Staatsprüfung nach Abschnitt 5 nicht bestanden wurde.

#### § 6 Antrag auf Teilnahme

(1) Der Antrag auf Teilnahme am berufsbegleitenden Vorbereitungsdienst ist schriftlich auf dem Dienstweg bei dem für Schule zuständigen Ministerium in der von ihm bekannt gegebenen Bewerbungsfrist zu stellen.
Nach der Bewerbungsfrist eingegangene Bewerbungen können nur Berücksichtigung finden, wenn die Anzahl der fristgemäß eingegangenen Bewerbungen, bei denen die Teilnahmevoraussetzungen gemäß § 5 erfüllt sind, die Anzahl der zur Verfügung stehenden Ausbildungsplätze nicht übersteigt.

(2) In dem Antrag sind insbesondere

1.  das angestrebte Lehramt und die Fächer, auf die sich die Ausbildung beziehen soll, und
2.  die Anzahl der bisher erfolglos gestellten Anträge auf Teilnahme am berufsbegleitenden Vorbereitungsdienst im Land Brandenburg oder Zulassung zum Vorbereitungsdienst für ein Lehramt gemäß Abschnitt 3

anzugeben.
Als Antragsunterlagen sind

1.  die Zeugnisse über die antragsrelevanten Hochschulabschlüsse und deren Anlagen und
2.  gegebenenfalls Nachweise gemäß Satz 1 Nummer 2

beizufügen.
Soweit es für die Entscheidung über die Teilnahme am berufsbegleitenden Vorbereitungsdienst erforderlich ist, können weitere Unterlagen verlangt werden.
Sind die Antragsunterlagen nicht in deutscher Sprache verfasst, können davon Übersetzungen in deutscher Sprache verlangt werden, die von einer Dolmetscherin oder einem Dolmetscher, einer Übersetzerin oder einem Übersetzer anzufertigen sind, die oder der in einem Mitgliedsstaat der Europäischen Union, des Europäischen Wirtschaftsraumes (Norwegen, Island, Liechtenstein) oder in der Schweiz öffentlich bestellt oder beeidigt wurde.
Die Kosten für Kopien, Beglaubigungen und Übersetzungen sind von der antragstellenden Person zu tragen.

#### § 7 Entscheidung über die Teilnahme

(1) Die Entscheidung über die Teilnahme trifft das für Schule zuständige Ministerium.
Sie ist der Bewerberin oder dem Bewerber schriftlich mitzuteilen und umfasst insbesondere

1.  das angestrebte Lehramt und die Ausbildungsfächer sowie
2.  das für die Ausbildung zuständige Studienseminar.

(2) Soweit die Anzahl der Bewerbungen die Anzahl der gemäß § 4 Absatz 2 zur Verfügung stehenden Ausbildungsplätze übersteigt, erfolgt die Auswahl nach der Gesamtnote des der jeweiligen Bewerbung zu Grunde liegenden Hochschulabschlusses.
Bei der Gesamtnote ist eine Dezimalstelle ohne Rundung zu berücksichtigen.
Bei gleicher Gesamtnote hat zunächst die Bewerberin oder der Bewerber Vorrang, die oder der im Sinne des Neunten Buches Sozialgesetzbuch schwerbehindert oder entsprechend gleichgestellt ist.
Im Übrigen entscheidet die Anzahl der bisher erfolglos gebliebenen Bewerbungen für die Teilnahme am berufsbegleitenden Vorbereitungsdienst oder den Vorbereitungsdienst gemäß Abschnitt 3.
Über die Vergabe der danach verbleibenden Ausbildungsplätze entscheidet das Los.

### Unterabschnitt 2  
Ausbildung

#### § 8 Ausbildungsorganisation

(1) Die Ausbildung erfolgt in den Ausbildungsfächern, die bei der Entscheidung über die Teilnahme am berufsbegleitenden Vorbereitungsdienst festgelegt wurden, sowie in den Bildungswissenschaften auf der Grundlage eines individuellen standard- und kompetenzorientierten Ausbildungsplans.
Sie findet an dem zuständigen Studienseminar und in der Schule statt, in der die oder der Auszubildende als Lehrkraft beschäftigt ist (Ausbildungsschule).
In begründeten Ausnahmefällen kann eine weitere Schule in die schulpraktische Ausbildung einbezogen werden.
§ 10 Absatz 3 gilt entsprechend.

(2) Während der Dauer der Ausbildung ist zu gewährleisten, dass die oder der Auszubildende in jedem der Ausbildungsfächer acht Lehrerwochenstunden im Unterricht der auf das angestrebte Lehramt bezogenen Schulstufe in unterschiedlichen Jahrgangsstufen eingesetzt ist.
In begründeten Ausnahmefällen kann in einem Ausbildungsfach die Anzahl der Lehrerwochenstunden um bis zu vier Lehrerwochenstunden abgesenkt werden, wobei die Gesamtlehrerwochenstundenzahl von 16 Lehrerwochenstunden gewährleistet werden muss.

#### § 9 Beurteilungen

Vor Eintritt in die Staatsprüfung ist eine Beurteilung über die fachliche Leistung und Eignung der oder des Auszubildenden zu fertigen.
Hierfür gilt § 18 der Ordnung für den Vorbereitungsdienst entsprechend.

#### § 10 Verantwortlichkeiten

(1) Die Gesamtverantwortung für die Organisation und Durchführung des berufsbegleitenden Vorbereitungsdienstes trägt die Leiterin oder der Leiter des zuständigen Studienseminars.
Sie bestimmen in enger Abstimmung mit der Leiterin oder dem Leiter der Ausbildungsschule den individuellen Ausbildungsplan für die Auszubildende oder den Auszubildenden, organisieren die obligatorischen und fakultativen Ausbildungsangebote und gewährleisten, dass die Ausbilderinnen und Ausbilder die Unterrichtshospitationen und individuelle Entwicklungsgespräche durchführen.

(2) Die Verantwortung für die schulpraktische Ausbildung trägt die Leiterin oder der Leiter der Ausbildungsschule.
Sie oder er arbeitet mit dem zuständigen Studienseminar bei der Wahrnehmung ihrer jeweiligen Ausbildungsaufgaben gemäß § 17 Absatz 3 der Ordnung für den Vorbereitungsdienst eng zusammen.

(3) Ist die oder der Auszubildende an mehr als einer Schule als Lehrkraft tätig, nimmt die Schule, an der sie oder er überwiegend beschäftigt ist, die Aufgaben der Ausbildungsschule wahr.
Insbesondere wenn die oder der Auszubildende Unterricht in den Ausbildungsfächern an anderen Schulen erteilt, arbeiten die Leiterinnen und Leiter dieser Schulen eng mit der Leiterin oder dem Leiter der Ausbildungsschule zusammen.

#### § 11 Vorzeitige Beendigung der Ausbildung

(1) Die Ausbildung endet abweichend von § 2 Satz 1 vorzeitig, wenn das Beschäftigungsverhältnis gemäß § 5 Absatz 1 Satz 1 Nummer 1 beendet worden ist.

(2) Die Ausbildung kann im Ausnahmefall durch die Ausbildungsbehörde beendet werden, wenn

1.  das Ausbildungsziel auf Grund gravierender defizitärer fachlicher und pädagogischer Kenntnisse und Fähigkeiten, die absehbar durch die Ausbildung nicht erworben werden können, nicht erreicht werden kann oder
2.  die oder der Auszubildende ihre oder seine Ausbildungs- und Dienstpflichten grob verletzt.

(3) Im Fall von Absatz 2 Nummer 1 bedarf es einer Beurteilung zu dem Zeitpunkt, zu dem erstmals erkennbar ist, dass das Ausbildungsziel voraussichtlich nicht erreicht werden kann.
Sie ist von der Leiterin oder dem Leiter der Ausbildungsschule im Einvernehmen mit der Leiterin oder dem Leiter des Studienseminars zu fertigen.

### Abschnitt 3  
Besonderer Zugang zum Vorbereitungsdienst

#### § 12 Ausbildungsplätze

(1) Soweit für den Vorbereitungsdienst für ein Lehramt nicht genutzte Ausbildungskapazitäten der Studienseminare gemäß § 6 Absatz 1 Satz 2 des Brandenburgischen Lehrerbildungsgesetzes zur Verfügung stehen, können diese für die Einstellung von Personen in den Vorbereitungsdienst für dieses Lehramt verwendet werden, die die Voraussetzungen gemäß § 13 Absatz 1 erfüllen.

(2) Das für Schule zuständige Ministerium entscheidet bei Bedarf im Rahmen der gemäß Absatz 1 zur Verfügung stehenden Ausbildungskapazitäten und auf der Grundlage des von den staatlichen Schulämtern festgestellten unabweisbaren Lehrkräftebedarfs über die lehramts- und fachbezogene Anzahl bereitzustellender Ausbildungsplätze.

(3) Das für Schule zuständige Ministerium gibt in seinem Amtsblatt rechtzeitig vor Beginn der Ausbildung insbesondere die gemäß Absatz 2 zur Verfügung gestellten Ausbildungsplätze, den Ausbildungsbeginn, das für die Ausbildung zuständige Studienseminar und die Bewerbungsfrist bekannt.

#### § 13 Voraussetzungen für den besonderen Zugang zum Vorbereitungsdienst

(1) Der besondere Zugang zum Vorbereitungsdienst für ein Lehramt wird Personen eröffnet, die

1.  einen nicht lehramtsbezogenen Hochschulabschluss gemäß Absatz 2 nachweisen können, wobei ein Bachelorabschluss nicht ausreicht und
2.  über Kenntnisse in der deutschen Sprache auf dem Niveau C 2 des Gemeinsamen Europäischen Referenzrahmens für Sprache verfügen, sofern Deutsch nicht Muttersprache ist.

(2) Mit dem Hochschulabschluss gemäß Absatz 1 Satz 1 Nummer 1 sind die fachwissenschaftlichen oder künstlerischen Bildungsvoraussetzungen nachzuweisen, die den Unterrichtseinsatz in mindestens zwei Fächern, Fachrichtungen oder Lernbereichen (Fächer) ermöglichen, wobei mindestens für eines der beiden Fächer ein Bedarf gemäß § 12 Absatz 2 festgestellt sein muss.
Für die fachwissenschaftlichen oder künstlerischen Bildungsvoraussetzungen gilt § 5 Absatz 3 entsprechend.
Wird das Lehramt für die Primarstufe angestrebt, gilt § 5 Absatz 4 entsprechend.

(3) § 5 Absatz 5 gilt entsprechend.

#### § 14 Zulassung

(1) Für das Zulassungsverfahren gelten die in der Ordnung für den Vorbereitungsdienst zu den Terminen und Fristen sowie den Zulassungsantrag und die Auswahl erlassenen Regelungen.

(2) Soweit die Anzahl der Bewerbungen die Anzahl der zur Verfügung stehenden Ausbildungsplätze übersteigt, gilt § 7 Absatz 2 entsprechend.

(3) Soweit auf Grund der Bewerberlage die Ausbildungskapazität nicht ausgeschöpft wird, können die verbleibenden Ausbildungsplätze insbesondere für die Ausbildung von Lehrkräften für Lehrämter und Fächer verwendet werden, für die an Ersatzschulen im Land Brandenburg ein Bedarf besteht.

#### § 15 Ausbildung und Beurteilungen

(1) Für die Ausbildung gelten die in der Ordnung für den Vorbereitungsdienst bestimmten Regelungen zur Organisation des Vorbereitungsdienstes, zur Ausbildung an den Schulen mit der Maßgabe, dass als Ausbildungsschule in der Regel die Schule vorzusehen ist, an der zum Zeitpunkt des Ausbildungsbeginns der gemäß § 12 Absatz 2 vom staatlichen Schulamt angezeigte Lehrkräftebedarf besteht oder absehbar bestehen wird, entsprechend.

(2) Soweit im Lauf der Ausbildung erkennbar wird, dass das Ausbildungsziel aufgrund mangelhafter fachlicher und pädagogischer Kenntnisse und Fähigkeiten nicht mehr erreicht werden kann, gilt § 11 Absatz 2 und 3 entsprechend.

(3) Vor dem Eintritt in die Staatsprüfung ist die fachliche Leistung und Eignung der Lehramtskandidatin oder des Lehramtskandidaten schriftlich zu beurteilen.
Hierfür gelten die Regelungen gemäß § 18 der Ordnung für den Vorbereitungsdienst entsprechend.

### Abschnitt 4  
Staatsprüfung

#### § 16 Staatsprüfung, Zeugnis und Bescheinigungen

(1) Die Staatsprüfung schließt die Ausbildung gemäß Abschnitt 2 und Abschnitt 3 ab.
Für die Staatsprüfung gelten die Regelungen gemäß Abschnitt 3 der Ordnung für den Vorbereitungsdienst, soweit in dieser Verordnung nichts Anderes geregelt wird, entsprechend.

(2) Im Falle einer Ausbildung gemäß Abschnitt 2 gehört bei der Durchführung der Unterrichtsproben anstelle der Ausbildungslehrkraft für das jeweilige Ausbildungsfach dem Prüfungsausschuss eine Lehrkraft der Ausbildungsschule an.

(3) Wird die Staatsprüfung bei einer Ausbildung gemäß Abschnitt 2 nicht bestanden, kann die Staatsprüfung ohne Verlängerung des berufsbegleitenden Vorbereitungsdienstes innerhalb von fünf Jahren einmal wiederholt werden.

(4) Soweit eine vorzeitig zugelassene Staatsprüfung gemäß § 8 Absatz 2 des Brandenburgischen Lehrerbildungsgesetzes nicht bestanden wurde, wird der berufsbegleitende Vorbereitungsdienst oder der im Rahmen des besonderen Zugangs begonnene Vorbereitungsdienst fortgesetzt und die Staatsprüfung am Ende der Ausbildung erneut durchgeführt.

### Abschnitt 5  
Besondere Staatsprüfung

#### § 17 Grundsätze und Ziel

(1) Für die besondere Staatsprüfung gelten die Regelungen für die Staatsprüfung gemäß der Ordnung für den Vorbereitungsdienst entsprechend, soweit in dieser Verordnung nichts Anderes geregelt wird.

(2) Die Prüfung wird in der Regel an der Schule durchgeführt, an der die Prüfungskandidatin oder der Prüfungskandidat überwiegend beschäftigt ist.
Sie ist innerhalb von drei Monaten durchzuführen.
Die Frist beginnt mit der Zustellung der schriftlichen Zulassung gemäß § 21.
Die Prüfungsanteile sind innerhalb von einem Monat zu absolvieren.

#### § 18 Zulassungsvoraussetzungen

(1) Zur besonderen Staatsprüfung wird zugelassen, wer

1.  die Teilnahmevoraussetzungen gemäß § 5 Absatz 1 bis 4 erfüllt,
2.  einen mindestens einjährigen, dem angestrebten Lehramt entsprechenden Unterrichtseinsatz in zwei Unterrichtsfächern, die als Prüfungsfächer gemäß § 20 Absatz 2 Satz 1 angegeben wurden, in einem Umfang von in der Regel jeweils mindestens acht Lehrerwochenstunden nachweisen kann,
3.  die Teilnahme an Qualifizierungsmaßnahmen gemäß Absatz 2 nachweisen kann und
4.  bisher nicht für eine den Vorbereitungsdienst für das angestrebte Lehramt abschließende Staatsprüfung zugelassen wurde.

(2) Die nachzuweisenden Qualifizierungsmaßnahmen gemäß Absatz 1 Nummer 3 sollen mindestens drei Viertel den im Rahmen eines Vorbereitungsdienstes für das angestrebte Lehramt vermittelten Inhalten und zu erwerbenden Kompetenzen entsprechen.

#### § 19 Prüfungsleistungen

(1) Als Prüfungsleistungen sind

1.  eine unterrichtspraktische Prüfung und
2.  eine mündliche Prüfung

zu erbringen.

(2) Die unterrichtspraktische Prüfung besteht aus jeweils zwei Unterrichtsproben in den Prüfungsfächern.
Die beiden Unterrichtsproben je Fach sind in verschiedenen Klassen oder Kursen der dem angestrebten Lehramt entsprechenden Schulstufe oder Schulform abzulegen.
Das beauftragte Studienseminar bestimmt auf Vorschlag des Prüflings in Abstimmung mit der Leiterin oder dem Leiter der Beschäftigungsschule die Termine für die Durchführung der Unterrichtsproben und die Klassen oder Kurse.

(3) Die mündliche Prüfung wird als Einzelprüfung durchgeführt und dauert in der Regel 40 Minuten.
Der Termin für die mündliche Prüfung wird auf Vorschlag des Prüflings durch die Leiterin oder den Leiter des Studienseminars festgelegt und bis spätestens zwei Wochen vor dem Prüfungstermin dem Prüfling schriftlich mitgeteilt.

(4) Die Aufgabenstellung für die mündliche Prüfung, die aus mehreren Teilaufgaben bestehen kann, und der dazugehörige Erwartungshorizont sind von den Mitgliedern des Prüfungsausschusses gemäß § 22 Satz 2 Nummer 2 einvernehmlich zu erarbeiten.
Die Aufgabenstellung ist dem Prüfling vorab nicht bekannt zu geben, dem Prüfling ist jedoch eine Vorbereitungszeit von 30 Minuten zu gewähren.

#### § 20 Zulassungsantrag

(1) Der Antrag zur besonderen Staatsprüfung (Zulassungsantrag) ist schriftlich bei dem für Schule zuständigen Ministerium zu stellen.

(2) In dem Zulassungsantrag sind insbesondere das angestrebte Lehramt und die Prüfungsfächer anzugeben.
Als Antragsunterlagen sind

1.  die Zeugnisse über die antragsrelevanten Hochschulabschlüsse und dessen Anlagen in amtlicher Beglaubigung und
2.  eine schriftliche Erklärung der Schulleitung der Beschäftigungsschule zum Einsatz gemäß § 18 Absatz 1 Nummer 2,
3.  Nachweise, insbesondere Bescheinigungen oder Zertifikate zu Qualifizierungsmaßnahmen gemäß § 18 Absatz 2 sowie
4.  eine Erklärung zu § 18 Absatz 1 Nummer 4

einzureichen.
§ 6 Absatz 2 Satz 4 gilt entsprechend.

#### § 21 Zulassung zur Prüfung

Die Entscheidung über die Zulassung trifft das für Schule zuständige Ministerium.
Sie ist der Bewerberin oder dem Bewerber schriftlich mitzuteilen.

#### § 22 Prüfungsausschuss

Für jeden Prüfling ist ein Prüfungsausschuss zu berufen.
Ihm gehören

1.  die Vorsitzende oder der Vorsitzende und
2.  als weitere Mitglieder
    
    1.  in den Unterrichtsproben je Unterrichtsfach
        
        aa)
        
        eine Ausbilderin oder ein Ausbilder des beauftragten Studienseminars mit einer fachlichen Befähigung für das Prüfungsfach und
        
        bb)
        
        eine Lehrkraft der Schule, an der die Unterrichtsprobe stattfindet,
        
    
    sowie
    2.  in der mündlichen Prüfung zwei Ausbilderinnen oder Ausbilder des beauftragten Studienseminars mit einer fachlichen Befähigung für jeweils eines der beiden Prüfungsfächeran.

#### § 23 Notenbildung und Gesamtnote der besonderen Staatsprüfung

(1) Die Bewertung der beiden Unterrichtsproben in einem Fach bildet eine Fachnote.
Der Prüfungsausschuss der jeweils zweiten Unterrichtsprobe je Fach setzt die jeweilige Fachnote fest.
Beide Fachnoten bilden die Note der unterrichtspraktischen Prüfung.
Der Prüfungsausschuss der mündlichen Prüfung setzt die Note dieser Prüfung fest.
Für die Ermittlung der Fachnoten, der Note sowie der Note der mündlichen Prüfung gilt § 23 Absatz 3 der Ordnung für den Vorbereitungsdienst entsprechend.

(2) Der Prüfungsausschuss der mündlichen Prüfung ermittelt nach der Prüfung aus

1.  der Note der mündlichen Prüfung und
2.  der vierfach gewichteten Note der unterrichtspraktischen Prüfung

die Gesamtnote der besonderen Staatsprüfung und setzt diese fest.

(3) Die besondere Staatsprüfung ist nicht bestanden, wenn

1.  die Gesamtnote der Staatsprüfung,
2.  die Note der unterrichtspraktischen Prüfung oder
3.  die Note der mündlichen Prüfung

nicht mindestens ausreichend (4,0) ist.
Sie ist ebenfalls nicht bestanden, wenn eine Fachnote mit ungenügend (6,0) bewertet worden ist.

(4) Die oder der Vorsitzende des Prüfungsausschusses für die mündliche Prüfung teilt dem Prüfling am Ende der mündlichen Prüfung das Ergebnis der besonderen Staatsprüfung mit.

(5) Das für die Durchführung der Prüfung zuständige Studienseminar gibt das Ergebnis der besonderen Staatsprüfung dem Prüfling schriftlich bekannt.
Wurde die Staatsprüfung nicht bestanden, sind dem Prüfling der früheste Zeitpunkt, zu dem ein Zulassungsantrag zur Wiederholungsprüfung gestellt werden kann, sowie die Frist, in der ein entsprechender Antrag gestellt werden kann, mitzuteilen.
Wurde die besondere Staatsprüfung endgültig nicht bestanden, erfolgt die Bekanntgabe des Ergebnisses der Staatsprüfung unverzüglich nach Abschluss des Prüfungsverfahrens.

#### § 24 Wiederholung der Prüfung

(1) Wurde die Prüfung nicht bestanden oder für nicht bestanden erklärt, kann sie innerhalb von fünf Jahren ab Bekanntgabe des Ergebnisses gemäß § 23 Absatz 5 Satz 1 einmal wiederholt werden.
Für die Antragstellung gilt § 20 entsprechend.

(2) Auf Antrag des Prüflings sind die Ergebnisse der bestandenen Prüfungsteile anzurechnen.

(3) Die Prüfung ist endgültig nicht bestanden, wenn die Wiederholungsprüfung nicht bestanden wurde.

#### § 25 Zeugnis und Bescheinigungen

Über die bestandene besondere Staatsprüfung ist von dem für Schule zuständigen Ministerium ein Zeugnis zu fertigen, das insbesondere

1.  die Bezeichnung des Lehramtes und des Schwerpunktes einschließlich der Fächer für das die Befähigung erworben wurde,
2.  die Fachnoten und die Note der mündlichen Prüfung und
3.  die Gesamtnote der besonderen Staatsprüfung

umfasst.

### Abschnitt 6  
Schlussbestimmungen

#### § 26 Übergangsregelung

Wer sich zum Zeitpunkt des Inkrafttretens dieser Verordnung in einer Ausbildung auf der Grundlage der

1.  Berufsbegleitenden Vorbereitungsdienstverordnung vom 17.
    Oktober 2013 (GVBl.
    II Nr.
    75) oder
2.  der Lehrkräfteausbildungs- und -prüfungsverordnung vom 11.
    Mai 2017 (GVBl.
    II Nr.
    29)

befindet, absolviert die Ausbildung auf der Grundlage dieser Verordnungen.

#### § 27 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Dezember 2019 in Kraft.
Gleichzeitig tritt die Lehrkräfteausbildungs- und -prüfungsverordnung vom 11.
Mai 2017 (GVBl.
II Nr.
29) außer Kraft.

Potsdam, den 20.
Dezember 2019

Die Ministerin für Bildung,  
Jugend und Sport

Britta Ernst