## Verordnung über das Naturschutzgebiet „Elsteraue bei Arnsnesta“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542), von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl.
I S.
1972, 1974) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21. Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl. II Nr. 43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Elbe-Elster wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Elsteraue bei Arnsnesta“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 118 Hektar.
Es umfasst Flächen in folgenden Fluren:

Stadt:     

Gemarkung: 

Flur: 

Herzberg (Elster)

Arnsnesta

1 und 2;

 

Borken

3;

 

Frauenhorst

1 und 4.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 2 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 3 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Innerhalb des Naturschutzgebietes wird gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes eine Zone 1 mit rund 7 Hektar mit weiteren Regelungen der landwirtschaftlichen Nutzung festgesetzt.

Die Grenze der Zone ist in der in Anlage 2 Nummer 1 genannten topografischen Karte mit der Blattnummer 1 sowie in der in Anlage 2 Nummer 2 genannten Liegenschaftskarte mit der Blattnummer 1 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Elbe-Elster, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes, das aus einem Feuchtgebiet mit Altarmen der Schwarzen Elster, Niedermooren, charakteristischen Gehölzbeständen der Fließ- und Stillgewässer des Elbe-Mulde-Tieflands und Frisch- und Feuchtwiesen besteht, ist

1.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Fließ- und Stillgewässer, der Flussauen und Feuchtwiesenbereiche wie unter anderem Wasserpflanzengesellschaften, Schwimmblatt-, Röhricht- und Wasserschwebegesellschaften;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Wasserfeder (Hottonia palustris), Froschbiss (Hydrocharis morsus-ranae), Flutende Moorbinse (Isolepis fluitans), Seekanne (Nymphoides peltata) und Krebsschere (Stratiotes aloides);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere als Brut-, Nahrungs- und Rastgebiet für die in ihrem Bestand bedrohten Vogelarten der Feuchtgebiete, für an aquatische Lebensräume gebundene Säugetierarten, Amphibien, Reptilien, Fische und Insekten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten wie Weißstorch (Ciconia ciconia), Rotmilan (Milvus milvus), Rohrweihe (Circus aeruginosus), Schellente (Bucephala clangula), Ortolan (Emberiza hortulana), Kranich (Grus grus), Kiebitz (Vanellus vanellus), Bekassine (Gallinago gallinago), Wiedehopf (Upupa epops), Schwarzspecht (Dryocopus martius), Grünspecht (Picus viridis), Braunkehlchen (Saxicola rubetra), Neuntöter (Lanius collurio), Raubwürger (Lanius excubitor), Drosselrohrsänger (Acrocephalus arundinaceus), Grauammer (Emberiza calandra), Eisvogel (Alcedo atthis), Krickente (Anas crecca), Zauneidechse (Lacerta agilis), Moorfrosch (Rana arvalis) und Knoblauchkröte (Pelobates fuscus), Bachneunauge (Lampetra planeri), Rapfen (Aspius aspius), Bitterling (Rhodeus sericeus amarus), Großer Schillerfalter (Apatura iris), Braun-fleckiger Perlmutterfalter (Boloria selene), Magerrasen-Perlmutterfalter (Boloria dia) und Grüne Mosaikjungfer (Aeshna viridis);
4.  die Erhaltung des Gebietes aus wissenschaftlichen Gründen, beispielsweise zur Wiederansiedlung des Lachses und für die Beobachtung und Erforschung von Lebensgemeinschaften der Flussauen;
5.  die Erhaltung und Entwicklung des Gebietes für den überregionalen Biotopverbund zwischen den Niederungsbereichen der Schwarzen Elster und der Pulsnitz bis zum Schraden.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Elsteraue bei Arnsnesta“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), das sich aus dem ehemals bestehenden Gebiet von gemeinschaftlicher Bedeutung „Fluten von Arnsnesta“ und Teilen der Gebiete von gemeinschaftlicher Bedeutung „Mittellauf der Schwarzen Elster“ und „Mittellauf der Schwarzen Elster/Ergänzung“ zusammensetzt, mit seinen Vorkommen von

1.  Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Flüssen der planaren bis montanen Stufen mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Brenndolden-Auenwiesen (Cnidion dubii), Mageren Flachland-Mähwiesen (Alopecurus pratensis, Sanguisorba officinalis) und Alten bodensauren Eichenwäldern auf Sand​ebenen mit Quercus robur als natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Auen-Wäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion, Alnion incanae, Salicion albae) als prioritärem natürlichen Lebensraumtyp im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
3.  Fischotter (Lutra lutra), Biber (Castor fiber), Schlammpeitzger (Misgurnus fossilis) und Grüner Keiljungfer (Ophiogomphus cecilia) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume;
4.  Schwimmendem Froschkraut (Luronium natans) als Art von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich seiner Lebensräume und den für seine Reproduktion erforderlichen Standortbedingungen.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet, oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten.
    Ausgenommen ist das Betreten zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 14 nach dem 30.
    Juni eines jeden Jahres;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  außerhalb der in den in § 2 Absatz 2 genannten topografischen Karten gekennzeichneten Badestelle zu baden oder zu tauchen;
13.  Wasserfahrzeuge aller Art, einschließlich Surfbretter oder Luftmatratzen zu benutzen; ausgenommen ist das Befahren der Schwarzen Elster mit muskelkraftbetriebenen Booten außerhalb von Röhrichten und Schwimmblattgesellschaften;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel aller Art zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und Gärreste aus Biogasanlagen oder Sekundärrohstoffdünger im Sinne von § 4 Absatz 2 Nummer 17 einzusetzen,  
          
        Sekundärrohstoffdünger im Sinne dieser Verordnung sind Abwasser, Fäkalien, Klärschlamm und ähnliche Stoffe aus Siedlungsabfällen und vergleichbare Stoffe aus anderen Quellen, jeweils auch weiterbehandelt und in Mischungen untereinander oder mit Düngemitteln, Wirtschaftsdünger, Bodenhilfsstoffen, Kultursubstraten und Pflanzenhilfsmitteln,
    2.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt, wobei bei Narbenschäden eine umbruchlose Nachsaat auf Grünland zulässig bleibt,
    3.  in der Zone 1 darüber hinaus Grünland auf den in der topografischen Karte gekennzeichneten Flächen als Wiese oder Weide genutzt wird und § 4 Absatz 2 Nummer 17 gilt,
    4.  Wiesen nicht gemulcht werden, jedoch eine Nachmahd als Pflegemaßnahme zulässig ist,
    5.  bei Beweidung Gehölze in geeigneter Weise gegen Verbiss und sonstige Beschädigungen sowie Ränder von Gewässern wirksam gegen Trittschäden von weidenden Nutztieren geschützt werden;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  die Walderneuerung auf den Flächen der in § 3 Absatz 2 genannten Waldlebensraumtypen durch Naturverjüngung erfolgt.
        Sofern keine ausreichende Naturverjüngung möglich ist, dürfen ausschließlich lebensraumtypische Baumarten eingebracht werden.
        Auf den übrigen Wald- und Forstflächen dürfen nur Arten der potenziell natürlichen Vegetation eingebracht werden, wobei nur heimische Baumarten in lebensraumtypischen Zusammensetzung unter Ausschluss eingebürgerter Arten zu verwenden sind,
    2.  Bäume mit Horsten oder Höhlen nicht gefällt werden dürfen,
    3.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 30 Zentimetern Durchmesser in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden und liegendes Totholz (ganze Bäume mit über 65 Zentimetern Durchmesser am stärksten Ende) im Bestand verbleibt,
    4.  auf den Flächen des in § 3 Absatz 2 genannten Waldlebensraumtyps und sonstiger Laub-Mischwälder eine Nutzung der Bestände einzelstamm- bis truppweise durchgeführt wird.
        In den übrigen Wäldern und Forsten sind Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von 0,5 Hektar zulässig,
    5.  Holzernte- und Pflegemaßnahmen auf den Flächen der unter § 3 Absatz 2 genannten Lebensraumtypen und sonstiger Laub-Mischwälder in der Zeit vom 1.
        März bis 30.
        Juni eines jeden Jahres verboten sind,
    6.  § 4 Absatz 2 Nummer 17 und 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Fanggeräte und Fangmittel so eingesetzt werden, dass eine Gefährdung des Bibers und des Fischotters weitgehend ausgeschlossen ist,
    2.  § 4 Absatz 2 Nummer 19 gilt, wobei das Anfüttern zulässig ist;
4.  die rechtmäßige Ausübung der Angelfischerei an der Schwarzen Elster und an den ausgewiesenen zwei Angelgewässern in der Gemarkung Arnsnesta, Flur 2, Flurstück 7/1 und Gemarkung Frauenhorst, Flur 4, Flurstücke 153, 155 und 156 mit der Maßgabe, dass § 4 Absatz 2 Nummer 19 gilt, wobei das Anfüttern zulässig ist;
5.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Jagd in der Zeit vom 1.
        März bis zum 30.
        Juni eines jeden Jahres ausschließlich vom Ansitz stattfindet,
        
        bb)
        
        die Fallenjagd mit Lebendfallen erfolgt und bis zu einem Abstand von 100 Meter zum Gewässerufer verboten ist.
        Die Fallen sind mindestens einmal täglich zu kontrollieren,
        
        cc)
        
        die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
    2.  Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Errichtung anzuzeigen.
        Die untere Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        Die Entscheidung hierzu soll unverzüglich erfolgen.
    
    Im Übrigen bleiben Kirrungen innerhalb geschützter Biotope und Magerer Flachlandmähwiesen sowie im gesamten Gebiet Ablenkfütterungen, die Anlage von Ansaatwildwiesen und die Anlage und Unterhaltung von Wildäckern unzulässig.
    Jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg bleiben unberührt;
    
6.  die jährliche traditionelle Durchführung des Country-Festes, jeweils an jährlich bis zu drei Tagen an der Flut Arnsnesta im bisherigen Umfang;
7.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege.
    Die untere Naturschutzbehörde ist rechtzeitig mit dem Ziel einer einvernehmlichen Lösung zu beteiligen;
8.  die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter die Nummern 7, 9, 10 und 11 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
9.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie dem in § 3 aufgeführten Schutzzweck nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
10.  die ordnungsgemäße Unterhaltung der Hochwasserschutzanlagen einschließlich ihrer Deichschutzstreifen und des Deichseitengrabens nach Anzeige gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes bei der unteren Naturschutzbehörde.
    Die Zeitpunkte des Beginns der Frühjahrsmahd und der Durchführung von Maßnahmen mit Neuaufbau des Deichdeckmaterials über eine Länge von 100 Metern hinaus sind einvernehmlich mit der unteren Naturschutzbehörde abzustimmen.
    Dies kann durch Abstimmung eines Unterhaltungsplanes hergestellt werden;
11.  ausgenommen von den Verboten des § 4 bleiben weiterhin Maßnahmen des Hochwasserschutzes zur Beseitigung von Abflusshindernissen im Deichvorland wie zum Beispiel Stammholz, Äste, Treibgut, wenn dadurch der Schutzzweck nicht gefährdet wird.
    Die Maßnahmen sind der unteren Naturschutzbehörde gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes schriftlich anzuzeigen;
12.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
13.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
14.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch jeweils nach dem 30.
    Juni eines Jahres;
15.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung nach Anzeige gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes bei der unteren Naturschutzbehörde;
16.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
17.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen;
18.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungserfordernissen, die sich aus anderen Vorgaben ergeben, unberührt.

#### § 6 Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  die natürliche Auendynamik und die ökologische Durchgängigkeit des Gebietes soll durch die Anbindung von Altarmen an die Schwarze Elster, die Schaffung neuer Retentionsflächen und die Renaturierung von Stillgewässern wiederhergestellt werden;
2.  Unterhaltungsmaßnahmen der Gewässer sollen unter Beachtung der Lebensraumfunktion für das Froschkraut erfolgen.
    Hierzu sollen an ausgewählten Gewässern offene Rohbodenstandorte geschaffen werden;
3.  für die Grüne Mosaikjungfer sollen die Wiederansiedlungsmöglichkeiten verbessert werden; dafür sollen vorhandene Bestände der Krebsschere geschützt und deren Ausbreitung gefördert werden;
4.  das Grünland soll mosaikartig, vorzugsweise durch Mahd genutzt werden, dabei soll eine zweischürige Nutzung des Grünlandes mit einer Nutzungsruhe von mindestens zehn Wochen erfolgen.
    Die erste Nutzung soll vor dem 15.
    Juni und die zweite Nutzung nicht vor dem 31. August eines jeden Jahres erfolgen.
    Bei Beweidung soll diese als Spätweide mit Nachmahd erfolgen.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 17 und 18 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 29 Absatz 3 und § 30 des Bundesnaturschutzgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten, ihrer Lebensstätten und Biotope (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 54 Absatz 7 des Bundesnaturschutzgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

§ 5 Absatz 1 Nummer 1 Buchstabe a bis c tritt am 1.
Januar 2019 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 7.
September 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes "Elsteraue bei Arnsnesta"](/br2/sixcms/media.php/68/GVBl_II_62_2018-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes ") 865.3 KB

2

[Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten](/br2/sixcms/media.php/68/GVBl_II_62_2018-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten") 190.3 KB

3

[Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Elsteraue bei Arnsnesta“](/br2/sixcms/media.php/68/GVBl_II_62_2018-Anlage-3.pdf "Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Elsteraue bei Arnsnesta“") 176.2 KB