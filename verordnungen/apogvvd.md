## Verordnung über die Ausbildung und Prüfung für die Laufbahn des gehobenen Vollzugs- und Verwaltungsdienstes bei Justizvollzugsanstalten des Landes Brandenburg (Ausbildungs- und Prüfungsordnung gehobener Vollzugs- und Verwaltungsdienst - APOgVVD)

Auf Grund des § 26 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S. 26), der durch Artikel 1 des Gesetzes vom 5.
Dezember 2013 (GVBl.
I Nr. 36) geändert worden ist, verordnet der Minister der Justiz und für Europa und Verbraucherschutz im Einvernehmen mit dem Minister des Innern und für Kommunales und dem Minister der Finanzen:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeine Regelungen

[§ 1 Geltungsbereich, Erwerb der Laufbahnbefähigung](#1)

[§ 2 Ausbildungsziel](#2)

[§ 3 Einstellungsvoraussetzungen, Einstellungsbehörde](#3)

[§ 4 Bewerbung](#4)

[§ 5 Auswahlverfahren](#5)

[§ 6 Einstellung in den Vorbereitungsdienst](#6)

[§ 7 Rechtsstellung während des Vorbereitungsdienstes](#7)

### Abschnitt 2  
Ausbildung

[§ 8 Dauer des Vorbereitungsdienstes](#8)

[§ 9 Gliederung und Inhalt der Ausbildung, Ausbildungsstellen](#9)

[§ 10 Ablauf der fachpraktischen Studienzeiten, Ausbildungsbehörden; Leitung und Verantwortung](#10)

[§ 11 Beurteilungen](#11)

[§ 12 Bewertung der Leistungen](#12)

[§ 13 Urlaub, Krankheitszeiten, Unterbrechung und Verlängerung des Vorbereitungsdienstes](#13)

### Abschnitt 3  
Prüfungen, Laufbahnbefähigung

[§ 14 Zweck der Laufbahnprüfung](#14)

[§ 15 Prüfungen, Zuständigkeiten, Verfahren](#15)

[§ 16 Wiederholung der Laufbahnprüfung](#16)

[§ 17 Zuerkennung der Befähigung für eine Laufbahn des mittleren Dienstes derselben Fachrichtung](#17)

### Abschnitt 4  
Ergänzende Vorschriften

[§ 18 Vorzeitige Entlassung und Beendigung des Beamtenverhältnisses](#18)

[§ 19 Aufstiegsbeamtinnen und Aufstiegsbeamte](#19)

[§ 20 Regelung für Menschen mit Behinderungen](#20)

### Abschnitt 5  
Schlussbestimmung

[§ 21 Inkrafttreten](#21)

### Abschnitt 1  
Allgemeine Regelungen

#### § 1 Geltungsbereich, Erwerb der Laufbahnbefähigung

(1) Diese Verordnung regelt die Ausbildung und Prüfung für die Laufbahn des gehobenen Vollzugs- und Verwaltungsdienstes bei Justizvollzugsanstalten des Landes Brandenburg.

(2) Die Befähigung für die Laufbahn des gehobenen Vollzugs- und Verwaltungsdienstes bei Justizvollzugsanstalten des Landes Brandenburg wird durch Ableistung des Vorbereitungsdienstes und das Bestehen der Laufbahnprüfung erworben.

#### § 2 Ausbildungsziel

(1) Die Ausbildung soll in einem Fachhochschulstudiengang mit praktischem Bezug Beamtinnen und Beamte heranbilden, die nach ihrer Persönlichkeit und nach ihren allgemeinen und fachlichen Kenntnissen und Fähigkeiten in der Lage sind, selbstständig mit sozialem und wirtschaftlichem Verständnis und mit organisatorischem Geschick Aufgaben in der Vollzugsverwaltung wahrzunehmen, an der Gefangenenbehandlung mitzuwirken und die erforderlichen Entscheidungen und sonstigen Maßnahmen sachgemäß zu treffen und sie überzeugend zu begründen.

(2) Die Beamtinnen und Beamten sind so auszubilden, dass sie sich der freiheitlichen demokratischen Grundordnung unseres Staates verpflichtet fühlen und ihren künftigen Beruf als Dienst für das allgemeine Wohl auffassen.
In der Ausbildung wird darauf hingewirkt, dass diese Einstellung sich auch in der Arbeitsweise, insbesondere im Umgang mit Gefangenen und Publikum, niederschlägt.

(3) Die Beamtinnen und Beamten sind verpflichtet, ihre Kenntnisse und Fähigkeiten durch Selbststudium zu vertiefen und weiterzuentwickeln; ihr Selbststudium ist zu fördern.
Sie sollen dazu befähigt werden, sich eigenständig weiterzubilden.

#### § 3 Einstellungsvoraussetzungen, Einstellungsbehörde

(1) In den Vorbereitungsdienst kann eingestellt werden, wer

1.  die gesetzlichen Voraussetzungen für die Berufung in das Beamtenverhältnis erfüllt,
2.  in charakterlicher, geistiger und in gesundheitlicher Hinsicht für die Laufbahn geeignet ist; dabei darf von schwerbehinderten Menschen nur das für die Laufbahn erforderliche Mindestmaß körperlicher Rüstigkeit verlangt werden; und
3.  eine zu einem Hochschulstudium berechtigende Schulbildung oder einen als gleichwertig anerkannten Bildungsstand vorweisen kann.

(2) Zur Ausbildung kann auch zugelassen werden, wer die Voraussetzung nach Absatz 1 Nummer 3 erst zum Zeitpunkt der Einstellung in den Vorbereitungsdienst erfüllen wird.

(3) Über die Einstellung in den Vorbereitungsdienst entscheidet das für Justiz zuständige Ministerium (Einstellungsbehörde) nach Durchführung eines Auswahlverfahrens.

#### § 4 Bewerbung

(1) Bewerbungen sind an die Einstellungsbehörde zu richten.

(2) Der Bewerbung sind beizufügen:

1.  ein Lebenslauf,
2.  Ablichtungen des Zeugnisses über den Erwerb der Fachhochschulreife oder eines gleichwertigen Bildungsstandes sowie sonstiger Schulabschlusszeugnisse, in den Fällen des § 3 Absatz 2 das letzte Schulzeugnis,
3.  Ablichtungen von Nachweisen und Zeugnissen etwaiger beruflicher Tätigkeiten und Prüfungen seit der Beendigung des Schulverhältnisses und
4.  bei Minderjährigen die Einwilligung des gesetzlichen Vertreters.

(3) Bewerberinnen oder Bewerber, die bereits im Justizdienst stehen, reichen ihre Bewerbung auf dem Dienstweg ein.
Soweit die erforderlichen Unterlagen in den Personalakten enthalten sind, kann auf sie Bezug genommen werden.
Die Leiterin oder der Leiter der Beschäftigungsbehörde hat sich in einer dienstlichen Beurteilung über Eignung, Befähigung und fachliche Leistung der Bewerberin oder des Bewerbers zu äußern; etwaige Bedenken gegen die Zulassung zum Vorbereitungsdienst sind darzustellen.

(4) Die Einstellungsbehörde kann die Beibringung der in Absatz 2 genannten Unterlagen auf elektronischem Weg fordern.
Sie kann die Teilnahme am Auswahlverfahren nach § 5 davon abhängig machen, dass die einzureichenden Unterlagen fristgerecht und vollständig beigebracht werden.

#### § 5 Auswahlverfahren

(1) Vor der Entscheidung über die Zulassung zur Ausbildung wird in einem Auswahlverfahren festgestellt, ob die Bewerberinnen und Bewerber aufgrund ihrer Kenntnisse, Fähigkeiten und persönlichen Eigenschaften für die Einstellung in den Vorbereitungsdienst des gehobenen Vollzugs- und Verwaltungsdienstes bei Justizvollzugsanstalten im Land Brandenburg geeignet sind.

(2) Dem Auswahlverfahren nach Absatz 1 kann eine Vorauswahl vorausgehen, die sich an der Eignung der Bewerberinnen und Bewerber zu orientieren hat.
Die Einstellungsbehörde kann die Vorauswahl nach Satz 1 insbesondere auf der Grundlage der Durchschnittsnote des letzten Schul- oder Berufsschulzeugnisses oder der letzten beiden Schul- oder Berufsschulzeugnisse oder auf der Grundlage der Noten einzelner Fächer des letzten Schul- oder Berufsschulzeugnisses treffen.
Hierzu kann sie die Beibringung des letzten Schul- oder Berufsschulzeugnisses oder der letzten beiden Schul- oder Berufsschulzeugnisse verlangen.

(3) Art und Durchführung des Auswahlverfahrens werden durch die Einstellungsbehörde geregelt.
Sie hat für Bewerberinnen und Bewerber desselben Einstellungstermins das gleiche Auswahlverfahren anzuwenden.

(4) Die Bewerbungsunterlagen der Bewerberinnen und Bewerber, deren Einstellung in Aussicht genommen ist, werden vor der Einstellung der Fachhochschule für Rechtspflege Nordrhein-Westfalen in Bad Münstereifel (nachfolgend: Fachhochschule) zur Feststellung der Zugangsvoraussetzungen als ordentliche Studierende zugeleitet.

#### § 6 Einstellung in den Vorbereitungsdienst

(1) Die Einstellungsbehörde entscheidet unter Berücksichtigung des Ergebnisses des Auswahlverfahrens und der Prüfung durch die Fachhochschule nach § 5 über die Einstellung der Bewerberinnen und Bewerber in den Vorbereitungsdienst.
Die Einstellung erfolgt in der Regel zum 1. August eines Jahres.

(2) Vor der Einstellung in den Vorbereitungsdienst müssen folgende Unterlagen vorliegen:

1.  eine Geburtsurkunde, gegebenenfalls auch die Heiratsurkunde oder die Urkunde über die Begründung der Lebenspartnerschaft,
2.  ein amtsärztliches Gesundheitszeugnis,
3.  eine Erklärung über etwaige Vorstrafen oder anhängige Ermittlungs- oder Strafverfahren und ein behördliches Führungszeugnis,
4.  das Original des Schulzeugnisses oder der Bescheinigung, durch die die Voraussetzung nach § 4 Absatz 2 Nummer 2 nachgewiesen wird,
5.  eine Erklärung der Bewerberin oder des Bewerbers, dass sie oder er in geordneten wirtschaftlichen Verhältnissen lebt,
6.  ein Nachweis, dass die Bewerberin oder der Bewerber die deutsche Staatsangehörigkeit gemäß Artikel 116 des Grundgesetzes oder die Staatsangehörigkeit eines anderen Mitgliedstaates der Europäischen Union oder eines anderen Vertragsstaates des Abkommens über den Europäischen Wirtschaftsraum besitzt sowie
7.  die Originale der Zeugnisse über Tätigkeiten seit Beendigung des Schulverhältnisses, § 4 Absatz 2 Nummer 3.

#### § 7 Rechtsstellung während des Vorbereitungsdienstes

(1) Die Einstellung in den Vorbereitungsdienst erfolgt unter Berufung in das Beamtenverhältnis auf Widerruf.
Die Anwärterinnen und Anwärter führen die Dienstbezeichnung „Regierungsinspektoranwärterin“ oder „Regierungsinspektoranwärter“.

(2) Während des Vorbereitungsdienstes unterstehen die Anwärterinnen und Anwärter der Dienstaufsicht der Einstellungsbehörde.

### Abschnitt 2  
Ausbildung

#### § 8 Dauer des Vorbereitungsdienstes

(1) Der Vorbereitungsdienst dauert in der Regel drei Jahre und besteht aus fachwissenschaftlichen und fachpraktischen Studienzeiten von jeweils 18 Monaten.
Auf den Vorbereitungsdienst kann ein erfolgreich abgeschlossenes Studium an einer wissenschaftlichen Hochschule bis zur Dauer von zwölf Monaten angerechnet werden, sofern es der Ausbildung förderlich ist.
Über die Anrechnung entscheidet die Einstellungsbehörde auf Antrag der Anwärterin oder des Anwärters im Einvernehmen mit der Leiterin oder dem Leiter der Fachhochschule.

(2) Der Vorbereitungsdienst kann aus wichtigem Grund um bis zu ein Jahr verlängert werden.
Das Nähere regelt § 13.
Im Falle der Verlängerung ist das weitere Studium gesondert zu gestalten.

#### § 9 Gliederung und Inhalt der Ausbildung, Ausbildungsstellen

(1) Die Ausbildung gliedert sich in fachwissenschaftliche und fachpraktische Studienzeiten.
Die fachwissenschaftlichen Studienzeiten werden an der Fachhochschule im Studiengang Strafvollzug abgeleistet.
Die fachpraktischen Studienzeiten finden in der Regel an mindestens zwei verschiedenen Justizvollzugsanstalten des Landes Brandenburg statt.

(2) Die Ausbildung umfasst sechs Studienabschnitte.
Reihenfolge und Dauer der Studienabschnitte werden wie folgt festgelegt:

1.  Praktische Einführung

1 Monat,

2.  Fachwissenschaftliches Studium I

8 Monate,

3.  Fachpraktisches Studium I

8 Monate,

4.  Fachwissenschaftliches Studium II

7 Monate,

5.  Fachpraktisches Studium II

9 Monate,

6.  Fachwissenschaftliches Studium III

3 Monate.

(3) In der fachpraktischen Einführung soll ein Einblick in die Aufgaben der Laufbahn, in den inneren Aufbau einer Justizvollzugsanstalt und in die Aufgaben der anderen in den Justizvollzugsanstalten tätigen Berufsgruppen gewährt werden.
Die fachpraktische Einführung kann durch geeignete Lehrveranstaltungen ergänzt werden.

(4) Die Durchführung der fachwissenschaftlichen Studienzeiten richtet sich nach § 10 der Verordnung über die Ausbildung und Prüfung für die Laufbahn des gehobenen Vollzugs- und Verwaltungsdienstes bei Justizvollzugsanstalten des Landes Nordrhein-Westfalen vom 19.
April 2004 (GV.
NRW. S. 236), die zuletzt durch Artikel 2 der Verordnung vom 5.
Juni 2016 (GV.
NRW.
S.
298) geändert worden ist, in der jeweils geltenden Fassung sowie nach der Studienordnung und den Studienplänen der Fachhochschule für diesen Studiengang.

(5) Die fachpraktischen Studienzeiten richten sich nach § 11 der Verordnung über die Ausbildung und Prüfung für die Laufbahn des gehobenen Vollzugs- und Verwaltungsdienstes bei Justizvollzugsanstalten des Landes Nordrhein-Westfalen.
Während der fachpraktischen Studienzeiten sollen die Anwärterinnen und Anwärter in verschiedenen Justizvollzugsanstalten des Landes Brandenburg eingesetzt werden.

(6) Die fachpraktischen Studienzeiten werden durch begleitende Lehrveranstaltungen ergänzt, welche blockweise in der Fachhochschule stattfinden.
Sie dienen der Wiederholung und Vertiefung der in den fachwissenschaftlichen Studienabschnitten erworbenen Kenntnisse und geben ferner Gelegenheit, die im fachpraktischen Studium gewonnenen Erfahrungen kritisch zu verarbeiten.
Die Durchführung der begleitenden Lehrveranstaltungen richtet sich nach § 12 der Verordnung über die Ausbildung und Prüfung für die Laufbahn des gehobenen Vollzugs- und Verwaltungsdienstes bei Justizvollzugsanstalten des Landes Nordrhein-Westfalen.
Die Teilnahme an den begleitenden Lehrveranstaltungen geht jedem anderen Dienst vor.

(7) Die Studienpläne für die praktische Einführung und die fachpraktischen Studienzeiten werden von der Einstellungsbehörde erstellt.

(8) Die Einstellungsbehörde kann den Anwärterinnen oder Anwärtern, deren Leistungsstand dies zulässt, nach Abschluss der in Absatz 2 vorgegebenen Studienabschnitte im Rahmen des Ausbildungsziels Dienstleistungsaufträge erteilen und Projektarbeiten aufgeben.

#### § 10 Ablauf der fachpraktischen Studienzeiten, Ausbildungsbehörden; Leitung und Verantwortung

(1) Die Einstellungsbehörde leitet die Ausbildung und weist die Anwärterinnen und Anwärter für die fachwissenschaftlichen Studienabschnitte und die begleitenden Lehrveranstaltungen der Fachhochschule zu.
Sie bestimmt die Justizvollzugsanstalten (Ausbildungsbehörden), bei denen die Anwärterinnen und Anwärter ausgebildet werden und die für die Ausbildung zuständige Hauptanstalt (Stammanstalt).
Die fachhochschulrechtlichen Regelungen und die Verantwortlichkeit der Leiterin oder des Leiters der Fachhochschule für die fachwissenschaftlichen Studienzeiten bleiben unberührt.

(2) Für die praktische Einführung und für die fachpraktischen Studienzeiten ist die Leiterin oder der Leiter der Ausbildungsbehörde verantwortlich.

(3) Die Ausbildungsbehörden bestimmen im Einvernehmen mit der Einstellungsbehörde die Anstaltsbediensteten, die die Anwärterinnen und Anwärter am Arbeitsplatz ausbilden.
Mit der Ausbildung sollen nur Bedienstete betraut werden, die dafür fachlich und persönlich geeignet erscheinen.

(4) Die Ausbilderinnen und Ausbilder am Arbeitsplatz unterweisen die Anwärterinnen und Anwärter und leiten sie an.
Sie sind verpflichtet, die Anwärterinnen und Anwärter möglichst mit allen an dem Arbeitsplatz zu erfüllenden Aufgaben vertraut zu machen.

#### § 11 Beurteilungen

(1) Die Leiterin oder der Leiter der Fachhochschule beurteilt die Anwärterinnen und Anwärter jeweils am Ende der in § 9 Absatz 2 genannten fachwissenschaftlichen Studienzeiten auf der Grundlage von §§ 14 und 15 der Verordnung über die Ausbildung und Prüfung für die Laufbahn des gehobenen Vollzugs- und Verwaltungsdienstes bei Justizvollzugsanstalten des Landes Nordrhein-Westfalen und der Studienordnung.
Die Bewertungen müssen erkennen lassen, ob das Ziel der Ausbildung erreicht worden ist.

(2) Die Ausbilderinnen und Ausbilder in den fachpraktischen Studienzeiten erstellen jeweils zum Ende der einzelnen Ausbildungsabschnitte Beurteilungsbeiträge.
In den Beiträgen ist zu den fachlichen und allgemeinen Kenntnissen und Fähigkeiten, zum praktischen Geschick, zum Stand der Ausbildung und zum Gesamtbild der Persönlichkeit Stellung zu nehmen.
Die Beurteilungsbeiträge sind mit der Anwärterin oder dem Anwärter zu besprechen.
Die Leiterin oder der Leiter der Ausbildungsbehörde erstellt jeweils zum Ende der einzelnen fachpraktischen Ausbildungsabschnitte die entsprechende Beurteilung.

(3) Die Lehrkräfte der begleitenden Lehrveranstaltungen fertigen Beurteilungen auf der Grundlage von § 14 Absatz 3 der Verordnung über die Ausbildung und Prüfung für die Laufbahn des gehobenen Vollzugs- und Verwaltungsdienstes bei Justizvollzugsanstalten des Landes Nordrhein-Westfalen.

(4) Jede Beurteilung ist der Anwärterin oder dem Anwärter zur Kenntnisnahme vorzulegen.
Die Beurteilungen sind – gegebenenfalls mit einer Gegenäußerung der Anwärterin oder des Anwärters – in einem Beiheft zu den Personalakten zu nehmen.

#### § 12 Bewertung der Leistungen

(1) Die einzelnen Leistungen der Anwärterinnen und Anwärter sind mit folgenden Noten und Punkten zu bewerten:

sehr gut (16 bis 18 Punkte)

 = 

eine besonders hervorragende Leistung,

gut (13 bis 15 Punkte)

 = 

eine erheblich über den durchschnittlichen Anforderungen liegende Leistung,

vollbefriedigend (10 bis 12 Punkte)

 = 

eine über den durchschnittlichen Anforderungen liegende Leistung,

befriedigend (7 bis 9 Punkte)

 = 

eine Leistung, die in jeder Hinsicht durchschnittlichen Anforderungen entspricht,

ausreichend (4 bis 6 Punkte)

 = 

eine Leistung, die trotz ihrer Mängel durchschnittlichen Anforderungen noch entspricht,

mangelhaft (1 bis 3 Punkte)

 = 

eine an erheblichen Mängel leidende, im Ganzen nicht mehr brauchbare Leistung,

ungenügend (0 Punkte)

 = 

eine völlig unbrauchbare Leistung.

Zwischennoten und von vollen Zahlenwerten abweichende Punktzahlen dürfen nicht verwendet werden.

(2) Soweit Einzelbewertungen rechnerisch zusammengefasst werden, entsprechen den ermittelten Punkten folgende Notenbezeichnungen:

14,00 bis 18,00 Punkte:   

sehr gut,

11,50 bis 13,99 Punkte:   

gut,

9,00 bis 11,49 Punkte:   

vollbefriedigend,

6,50 bis 8,99 Punkte:   

befriedigend,

4,00 bis 6,49 Punkte:   

ausreichend,

1,50 bis 3,99 Punkte:   

mangelhaft,

0 bis 1,49 Punkte:   

ungenügend.

#### § 13 Urlaub, Krankheitszeiten, Unterbrechung und Verlängerung des Vorbereitungsdienstes

(1) Erholungsurlaub wird nach den für Beamtinnen und Beamte des Landes Brandenburg jeweils geltenden Bestimmungen gewährt.
Der Erholungsurlaub wird auf den Vorbereitungsdienst angerechnet.
Während der fachwissenschaftlichen Studienzeiten soll Erholungsurlaub nur gewährt werden, wenn dadurch keine Lehrveranstaltungen versäumt werden.
Soweit die praxisbegleitenden Lehrveranstaltungen nach § 9 Absatz 5 an der Fachhochschule stattfinden, ist der Urlaub während der fachpraktischen Studienzeiten ebenso in der veranstaltungsfreien Zeit zu gewähren.

(2) Urlaubsmonat für die fachpraktischen Studienzeiten I und II ist jeweils der Monat August.
Um den Erfolg des fachpraktischen Studiums I und II nicht zu beeinträchtigen, sind, soweit erforderlich, Urlaub und Krankheitszeiten auf die in § 11 der Verordnung über die Ausbildung und Prüfung für die Laufbahn des gehobenen Vollzugs- und Verwaltungsdienstes bei Justizvollzugsanstalten des Landes Nordrhein-Westfalen aufgeführten Studienabschnitte anteilig anzurechnen.

(3) Der Vorbereitungsdienst kann im Einzelfall verlängert werden, wenn dies im Falle des § 16 oder aufgrund einer Unterbrechung wegen längerer Krankheit oder Beurlaubung erforderlich ist oder das Ziel der Ausbildung in den einzelnen Ausbildungsabschnitten nicht erreicht wurde.
Die Verlängerung soll ein Jahr nicht überschreiten.

(4) Wenn nach Absatz 3 die Verlängerung einzelner Ausbildungsabschnitte oder die erneute Teilnahme an einem Ausbildungsabschnitt angeordnet wird oder wenn der Vorbereitungsdienst unterbrochen war, ist der weitere Ausbildungsverlauf gesondert zu regeln.

(5) Entscheidungen nach den Absätzen 3 und 4 trifft die Einstellungsbehörde.

### Abschnitt 3  
Prüfungen, Laufbahnbefähigung

#### § 14 Zweck der Laufbahnprüfung

Die Laufbahnprüfung dient der Feststellung, ob die Anwärterinnen oder Anwärter das Ziel der Ausbildung für die Laufbahn erreicht haben.

#### § 15 Prüfungen, Zuständigkeiten, Verfahren

Die Laufbahnprüfung wird nach den §§ 18 bis 37 der Verordnung über die Ausbildung und Prüfung für die Laufbahn des gehobenen Vollzugs- und Verwaltungsdienstes bei Justizvollzugsanstalten des Landes Nordrhein-Westfalen vor dem Landesjustizprüfungsamt Nordrhein-Westfalen abgelegt.
Die schriftliche Prüfung soll bereits während der vorgesehenen Dauer des Vorbereitungsdienstes abgenommen werden.
Die mündliche Prüfung wird sobald wie möglich nach der schriftlichen Prüfung abgeschlossen.

#### § 16 Wiederholung der Laufbahnprüfung

Ist die Prüfung für nicht bestanden erklärt worden, kann sie einmal wiederholt werden.
Die Prüfung ist vollständig zu wiederholen; einzelne Prüfungsleistungen können nicht erlassen werden.
Die Anwärterin oder der Anwärter hat hierzu ein ergänzendes Studium von längstens einem Jahr abzuleisten.
Auf Vorschlag des Prüfungsausschusses ordnet die Einstellungsbehörde an, welche Studienabschnitte ganz oder teilweise zu wiederholen sind.

#### § 17 Zuerkennung der Befähigung für eine Laufbahn des mittleren Dienstes derselben Fachrichtung

(1) Anwärterinnen und Anwärter, die die Prüfung nicht oder endgültig nicht bestanden haben, kann die Befähigung für die Laufbahn des mittleren Verwaltungsdienstes bei Justizvollzugsanstalten oder des allgemeinen Justizvollzugsdienstes auf Antrag zuerkannt werden, wenn die nachgewiesenen Kenntnisse ausreichen.

(2) Die Entscheidung über die Anerkennung der Befähigung trifft die Einstellungsbehörde nach Anhörung des Prüfungsausschusses.

### Abschnitt 4  
Ergänzende Vorschriften

#### § 18 Vorzeitige Entlassung und Beendigung des Beamtenverhältnisses

(1) Wer aufgrund seiner Leistung oder seines Verhaltens für den gehobenen Vollzugs- und Verwaltungsdienst bei Justizvollzugseinrichtungen nicht geeignet erscheint oder wer den geforderten geistigen und körperlichen Anforderungen nicht gerecht wird, soll nach Maßgabe des § 23 Absatz 4 Satz 1 des Beamtenstatusgesetzes entlassen werden.
Die Entscheidung trifft die Einstellungsbehörde.

(2) Das Beamtenverhältnis auf Widerruf endet gemäß § 32 Absatz 3 des Landesbeamtengesetzes mit Ablauf des Tages, an dem der Anwärterin oder dem Anwärter

1.  das Bestehen der Laufbahnprüfung oder
2.  das endgültige Nichtbestehen der Laufbahnprüfung oder einer vorgeschriebenen Zwischenprüfung

bekannt gegeben worden ist.
Im Fall von Satz 1 Nummer 1 endet das Beamtenverhältnis jedoch frühestens nach Ablauf der für den Vorbereitungsdienst im Allgemeinen oder im Einzelfall festgesetzten Zeit.

#### § 19 Aufstiegsbeamtinnen und Aufstiegsbeamte

(1) Beamtinnen und Beamte des mittleren Verwaltungsdienstes bei Justizvollzugsanstalten sowie Beamtinnen und Beamte des allgemeinen Vollzugsdienstes können nach Maßgabe der beamtenrechtlichen Bestimmungen des Landes Brandenburg zur Einführung in die Laufbahn des gehobenen Vollzugs- und Verwaltungsdienstes bei Justizvollzugsanstalten des Landes zugelassen werden.
Beamtinnen und Beamte, die die Zugangsvoraussetzungen als ordentliche Studierende an der Fachhochschule nicht erfüllen, werden der Fachhochschule als Studierende oder Studierender mit besonderer Zulassungsvoraussetzung zugewiesen.
Über die Zulassung entscheidet die Einstellungsbehörde.

(2) Die Beamtin oder der Beamte wird in die Aufgaben der Laufbahn des gehobenen Vollzugs- und Verwaltungsdienstes eingeführt.
An die Stelle des Vorbereitungsdienstes tritt eine Einführungszeit von gleicher Dauer.
Während der Einführungszeit nimmt die Beamtin oder der Beamte an der Ausbildung nach den Vorschriften dieser Ausbildungs- und Prüfungsverordnung teil.
Die Aufstiegsprüfung entspricht der Laufbahnprüfung nach § 15.

(3) Die Beamtin oder der Beamte tritt in die frühere Beschäftigung zurück, wenn sie oder er die Einführungszeit abgebrochen, die Aufstiegsprüfung auch nach Wiederholung nicht bestanden oder auf die Wiederholungsprüfung verzichtet hat oder nicht geeignet erscheint.

(4) Erholungsurlaub soll der Aufstiegsbeamtin oder dem Aufstiegsbeamten anteilig während der praktischen Einführungszeit gewährt werden.

#### § 20 Regelung für Menschen mit Behinderungen

Schwerbehinderten Menschen und ihnen gleichgestellten Menschen mit Behinderungen im Sinne von § 2 Absatz 2 und 3 des Neunten Buches Sozialgesetzbuch sind bei der Erbringung von Leistungen nach § 10 Absatz 5 und § 12 Absatz 2 der Verordnung über die Ausbildung und Prüfung für die Laufbahn des gehobenen Vollzugs- und Verwaltungsdienstes bei Justizvollzugsanstalten des Landes Nordrhein-Westfalen sowie für die Teilnahme an der Laufbahnprüfung die ihrer Behinderung angemessenen Erleichterungen zu gewähren.
Art und Umfang der Erleichterungen sind mit ihnen zu erörtern.
Die Erleichterungen dürfen nicht zu einer qualitativen Herabsetzung der Anforderungen führen.
Bei schwerbehinderten Menschen und diesen gleichgestellten Menschen mit Behinderungen im Sinne von Teil 3 des Neunten Buches Sozialgesetzbuch ist die zuständige Schwerbehindertenvertretung rechtzeitig zu informieren und anzuhören.
§ 24 Absatz 2 Satz 3 der Verordnung über die Ausbildung und Prüfung für die Laufbahn des gehobenen Vollzugs- und Verwaltungsdienstes bei Justizvollzugsanstalten des Landes Nordrhein-Westfalen bleibt unberührt.

### Abschnitt 5  
Schlussbestimmung

#### § 21 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 22.
August 2019

Der Minister der Justiz  
und für Europa und Verbraucherschutz

Stefan Ludwig