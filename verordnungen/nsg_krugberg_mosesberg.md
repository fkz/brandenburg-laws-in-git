## Verordnung über das Naturschutzgebiet „Krugberg-Mosesberg“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl. I S. 2542) in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBI.
II Nr.
 43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Märkisch-Oderland wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Krugberg-Mosesberg“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 42 Hektar.
Es umfasst vier Teilflächen in folgenden Fluren:

Gemeinde: 

Gemarkung: 

Flur: 

Seelow 

Werbig 

1; 

 

Seelow 

4. 

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.
Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 2 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den topografischen Karten im Maßstab 1 : 10 000 mit dem Titel „Topografische Karte zur Verordnung über das Naturschutzgebiet ‚Krugberg-Mosesberg‘“ und den Liegenschaftskarten im Maßstab 1 : 2 500 mit dem Titel „Liegenschaftskarte zur Verordnung über das Naturschutzgebiet ‚Krugberg-Mosesberg‘“ mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die topografischen Karten mit den Blattnummern 1 und 2 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten mit den Blattnummern 1 und 2.
Die Karten sind von der Siegelverwahrerin, Siegelnummer 13 des Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft am 12. November 2015 unterzeichnet worden.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Märkisch-Oderland, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes als Ausschnitt des steil abfallenden Geländes von der Lebuser Platte in das Oderbruch ist

1.  die Erhaltung, Entwicklung und Wiederherstellung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der basophilen Trocken- und Halbtrockenrasen (Steppenrasen), Sandtrockenrasen, Gras- und Staudenfluren, Feuchtwiesen und -weiden, Röhrichte, Quellfluren, Gräben, Laubgebüsche, Feldgehölze und naturnahen Laubwälder;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Karthäuser-Nelke (Dianthus carthusianorum), Sand-Strohblume (Helichrysum arenarium), Haar-Pfriemengras (Stipa capillata), Ähriger Blauweiderich (Veronica spicata) und Violette Schwarzwurzel (Scorzonera purpurea);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Insekten, Lurche, Kriechtiere, Vögel und Säugetiere, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Bibernell-Widderchen (Zygaena minos), Silbergrüner Bläuling (Polyommatus coridon), Veränderliches Widderchen (Zygaena ephialtes), Beilfleck-Widderchen (Zygaena loti), Rotbraunes Wiesenvögelchen (Coenonympha glycerion), Moorfrosch (Rana arvalis), Zauneidechse (Lacerta agilis), Grauammer (Emberiza calandra), Heidelerche (Lullula arborea), Neuntöter (Lanius collurio), Schwarzkehlchen (Saxicola rubicola) und Sperbergrasmücke (Sylvia nisoria);
4.  die Erhaltung der steilen, überwiegend beweideten Odertalhänge wegen ihrer besonderen Eigenart und hervorragenden Schönheit;
5.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Trockenrasen-Biotopverbundes am Rand des Odertales.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Krugberg-Mosesberg“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), das einen Teil des ehemaligen Gebietes von gemeinschaftlicher Bedeutung „Trockenrasen am Oderbruch“ umfasst, mit seinen Vorkommen von Trockenen, kalkreichen Sandrasen, Subpannonischem Steppen-Trockenrasen und Schlucht- und Hangmischwäldern (Tilio-Acerion) als prioritären natürlichen Lebensraumtypen im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten.
    Ausgenommen ist das Betreten zum Zweck des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 10;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der Wege, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, und außerhalb der Waldbrandwundstreifen zu reiten;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrich​tungen dafür bereitzuhalten;
13.  Hunde frei laufen zu lassen;
14.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
15.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle und Rückstände aus Biogasanlagen) und Sekundärrohstoffdünger (zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
16.  sonstige Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
17.  Tiere zu füttern oder Futter bereitzustellen;
18.  Tiere auszusetzen oder Pflanzen anzusiedeln;
19.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
20.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
21.  Pflanzenschutzmittel jeder Art anzuwenden;
22.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung mit der Maßgabe, dass Grünland als Wiese oder Weide mit einer Besatzdichte von maximal 1,4 Raufutter verwertenden Großvieheinheiten (RGV) pro Hektar im Jahresmittel genutzt wird und § 4 Absatz 2 Nummer 15, 21, 22 gilt.
    Umbruchlose Nachsaaten mit Genehmigung durch die untere Naturschutzbehörde bleiben zulässig;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  eine Nutzung ausschließlich einzelstammweise erfolgt,
    2.  nur Arten der potenziell natürlichen Vegetation in lebensraumtypischer Zusammensetzung eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
    3.  ein Altholzanteil von mindestens 10 Prozent am aktuellen Bestandesvorrat zu sichern ist,
    4.  mindestens fünf dauerhaft markierte Stämme je Hektar mit einem Brusthöhendurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum Absterben aus der Nutzung genommen sein müssen,
    5.  je Hektar mindestens fünf Stück dauerhaft markiertes stehendes Totholz (mehr als 30 Zentimeter Brusthöhendurchmesser in 1,30 Meter über Stammfuß) nicht gefällt werden; liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimeter am stärksten Ende) verbleibt im Bestand,
    6.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    7.  § 4 Absatz 2 Nummer 21 gilt;
3.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd,
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    3.  der Einsatz transportabler und mobiler Ansitzeinrichtungen,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und der in § 3 Absatz 2 genannten Lebensraumtypen.
    
    Ablenkfütterungen, die Anlage von Ansaatwildwiesen und die Anlage und Unterhaltung von Wildäckern sind unzulässig.
    Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Brandenburgischen Jagdgesetzes unberührt;
4.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter die Nummer 6 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
5.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
6.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
7.  die Nutzung und Unterhaltung des Bungalows auf dem Krugberg (Gemarkung Werbig, Flur 1, Flurstücke 582, 579 und 182);
8.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
9.  die Durchführung eines Osterfeuers und das Anzünden von Silvesterfeuerwerk auf der Feuerstelle nördlich des Friedenshains auf Flurstück 581 der Gemarkung Werbig, Flur 1 und das Betreten des Skulpturenparks auf der Hangschulter östlich des Friedenshains (Südabschnitt des Flurstücks 127 der Gemarkung Werbig, Flur 1);
10.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch;
11.  das Rodeln auf dem Flurstück 129 der Gemarkung Werbig, Flur 1;
12.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung nach Anzeige gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes bei der unteren Natur​schutzbehörde;
13.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der zuständigen Naturschutzbehörde zugelassen oder angeordnet worden sind;
14.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristi​sche Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl. S.
    1734), die zuletzt durch die Bekanntmachung vom 1. Oktober 2013 (ABl.
    S. 2811) geändert worden ist, an Straßen und Wegen freigestellt;
15.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungs​erfordernissen, die sich aus anderen fachrechtlichen Vorgaben ergeben, unberührt.

#### § 6 Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  Trockenrasen sollen mindestens zweimal jährlich mit Schafen und Ziegen beweidet werden.
    Alternativ können ein- bis zweischürige Mahd oder Mähweide durchgeführt werden;
2.  zur Wiederherstellung der Beweidungs- oder Mahdfähigkeit und zur Vermeidung der Verbuschung von Trockenrasen sollen Gehölze in erforderlichem Umfang entfernt werden.
    Teilflächen zum Beispiel sollen in Abstimmung mit der unteren Naturschutzbehörde im späten Winter abgeflämmt werden;
3.  bei der Bewirtschaftung der Waldflächen sollen
    1.  nicht heimische invasive Baumarten, insbesondere die Robinie, zugunsten von Baumarten der natürlichen Waldgesellschaft zurückgedrängt werden,
    2.  der Laubholzunterstand und -zwischenstand sowie die vorhandene Naturverjüngung aus Baumarten der natürlichen Waldgesellschaft begünstigt und in die nächste Bestandesgeneration übernommen werden,
4.  Streuobstbestände sollen biotopgerecht gepflegt werden;
5.  Feuchtwiesenbrachen sollen unter Beachtung der Maßgaben von § 5 Absatz 1 Nummer 1 einer regelmäßigen extensiven Grünlandnutzung zugeführt werden;
6.  Krautsäume entlang von Wegen und Gräben sollen möglichst breit erhalten oder entwickelt werden und möglichst nicht vor dem 15.
    September eines jeden Jahres abschnittsweise und nicht alljährlich gemäht werden;
7.  im Bereich der Quellfließe, Entwässerungsgräben und aufgelassenen Abgrabungen sollen Amphibienlebensräume erhalten und gepflegt werden.
    Laichgewässer sollen wiederhergestellt oder neu angelegt werden.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Branden​burgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Unberührtheit, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes), über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungs​gesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschafts​pflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

§ 5 Absatz 1 Nummer 1 tritt am 1.
Januar 2017 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 10.
Juni 2016

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes "Krugberg-Mosesberg"](/br2/sixcms/media.php/68/GVBl_II_27_2016-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes ") 867.3 KB

2

[Anlage 2 (zu § 2 Absatz 1) - Flurstücksliste zur Verordnung über das Naturschutzgebiet "Krugberg-Mosesberg"](/br2/sixcms/media.php/68/GVBl_II_27_2016-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 1) - Flurstücksliste zur Verordnung über das Naturschutzgebiet ") 582.6 KB