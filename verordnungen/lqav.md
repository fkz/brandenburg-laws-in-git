## Verordnung über die Anerkennung ausländischer Lehrerqualifikationen (Lehrerqualifikationsanerkennungsverordnung - LQAV)

Auf Grund

*   des § 13 Absatz 3 Satz 3 des Brandenburgischen Lehrerbildungsgesetzes vom 18. Dezember 2012 (GVBl.
    I Nr. 45) im Einvernehmen mit dem Minister des Innern und für Kommunales, dem Minister der Finanzen und der Ministerin für Wissenschaft, Forschung und Kultur,
*   des § 6 Absatz 4 Nummer 6 des Brandenburgischen Lehrerbildungsgesetzes im Einvernehmen mit dem Minister des Innern und für Kommunales und dem Minister der Finanzen und
*   des § 15 Absatz 1 des Landesbeamtengesetzes vom 3.
    April 2009 (GVBl.
    I S. 26), der zuletzt durch Artikel 3 des Gesetzes vom 17.
    Dezember 2015 (GVBl.
    I Nr. 38 S. 7) geändert worden ist,

verordnet der Minister für Bildung, Jugend und Sport:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeine Vorschriften

[§ 1 Geltungs- und Anwendungsbereich](#1)

[§ 2 Begriffsbestimmungen](#2)

### Abschnitt 2  
Grundsätze

[§ 3 Anerkennungsvoraussetzungen](#3)

[§ 4 Antrag](#4)

[§ 5 Feststellung](#5)

[§ 6 Verfahren](#6)

[§ 7 Mitwirkungspflichten](#7)

### Abschnitt 3  
Ausgleichsmaßnahmen

### Unterabschnitt 1  
Allgemeine Bestimmungen

[§ 8 Ausgleichsmaßnahmen](#8)

[§ 9 Bewerbung um die Teilnahme an einer Ausgleichsmaßnahme](#9)

### Unterabschnitt 2  
Anpassungslehrgang

[§ 10 Kapazitäten, Zulassung, Rechtsstellung](#10)

[§ 11 Dauer, Organisation, Durchführung](#11)

[§ 12 Leistungsbeurteilung und -bewertung, Gesamtergebnis des Anpassungslehrgangs](#12)

### Unterabschnitt 3  
Eignungsprüfung

[§ 13 Zulassung](#13)

[§ 14 Inhalt, Teile und Durchführung](#14)

[§ 15 Leistungsbewertung, Gesamtergebnis der Eignungsprüfung](#15)

### Abschnitt 4  
Schlussvorschriften

[§ 16 Statistik](#16)

[§ 17 Übergangsvorschriften](#17)

[§ 18 Inkrafttreten, Außerkrafttreten](#18)

### Abschnitt 1  
Allgemeine Vorschriften

#### § 1 Geltungs- und Anwendungsbereich

Diese Verordnung gilt für die Anerkennung von im Ausland erworbenen Qualifikationen für den Beruf der Lehrerin oder des Lehrers als Befähigung für ein Lehramt an Schulen im Land Brandenburg gemäß § 2 Absatz 1 des Brandenburgischen Lehrerbildungsgesetzes (angestrebtes Lehramt).
Sie ist auf alle Personen anzuwenden, die im Ausland eine entsprechende Qualifikation erworben haben und beabsichtigen, den Beruf der Lehrerin oder des Lehrers im Land Brandenburg aufzunehmen und auszuüben.
§ 10 des Bundesvertriebenengesetzes bleibt unberührt.

#### § 2 Begriffsbestimmungen

(1) Lehrerqualifikationen im Sinne dieser Verordnung sind Qualifikationen, die zur Aufnahme und Ausübung des Berufs der Lehrerin oder des Lehrers im Ausland berechtigen und durch Ausbildungsnachweise, Befähigungsnachweise oder einschlägige, im Inland oder Ausland erworbene und dokumentierte Berufserfahrung nachgewiesen werden.

(2) Ausbildungsnachweise sind Prüfungszeugnisse und Befähigungsnachweise, die von den zuständigen Stellen für den Abschluss einer erfolgreich absolvierten Ausbildung ausgestellt werden.

(3) Ausbildungsstaat ist der Staat, in dem der Ausbildungsnachweis für eine Lehrerqualifikation erworben oder anerkannt wurde.

(4) Mitgliedstaat ist ein Staat, der Mitglied der Europäischen Union oder Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum oder ein durch Abkommen gleichgestellter Staat ist.

(5) Ausgleichsmaßnahmen sind Maßnahmen, die den Ausgleich von Qualifikationsunterschieden, die zwischen der erworbenen und der für das angestrebte Lehramt geforderten Qualifikation bestehen, ermöglichen oder die Eignung für das angestrebte Lehramt feststellen.

(6) Zuständige Stelle für die Anerkennung einer Lehrerqualifikation sowie zur Organisation und Durchführung von Anpassungsmaßnahmen ist das für Schule zuständige Ministerium.

### Abschnitt 2  
Grundsätze

#### § 3 Anerkennungsvoraussetzungen

(1) Eine Lehrerqualifikation wird auf Antrag als Befähigung für das angestrebte Lehramt anerkannt, wenn

1.  die antragstellende Person auf Grund ihrer Lehrerqualifikation, die durch eine erfolgreich abgeschlossene Ausbildung an einer Universität oder einer anderen Hochschule oder Ausbildungseinrichtung mit gleichwertigem Ausbildungsniveau erworben wurde, den Beruf der Lehrerin oder des Lehrers im Ausbildungsstaat in mindestens einem der gemäß der Lehramtsstudienordnung für das angestrebte Lehramt zugelassenen Fächer (anzuerkennendes Fach) aufnehmen und ausüben kann oder die Befugnis zur Aufnahme oder Ausübung des Berufs der Lehrerin oder des Lehrers aus Gründen verwehrt wurde, die der Aufnahme oder Ausübung im Land Brandenburg nicht entgegenstehen, und
2.  zwischen der nachgewiesenen Lehrerqualifikation und den Qualifikationsanforderungen für das angestrebte Lehramt im Land Brandenburg keine wesentlichen Unterschiede (Qualifikationsunterschiede) bestehen oder bestehende Qualifikationsunterschiede durch eine Ausgleichsmaßnahme kompensiert werden konnten.

Abweichend von Absatz 1 Satz 1 Nummer 1 kann an die Stelle des Faches Deutsch das Fach Deutsch als Fremdsprache als anzuerkennendes Fach treten.

(2) Qualifikationsunterschiede liegen vor, wenn

1.  die Unterschiede zwischen den gemäß dem Ausbildungsnachweis erworbenen und den in der Ausbildung für das angestrebte Lehramt im Land Brandenburg zu erwerbenden Kenntnissen und Fähigkeiten für die Aufnahme und Ausübung des angestrebten Lehramtes maßgeblich sind und
2.  diese Unterschiede nicht durch sonstige Befähigungsnachweise, den Nachweis einschlägiger Berufserfahrung im Inland oder Ausland oder durch sonstige nachgewiesene Qualifikationen, die auch im Rahmen des lebenslangen Lernens erworben wurden, ausgeglichen werden können.

(3) Qualifikationsunterschiede können durch eine Ausgleichsmaßnahme gemäß § 8 Absatz 1 ausgeglichen werden.
Wird die erworbene Berufserfahrung teilweise als Ausgleich des Qualifikationsunterschieds angerechnet, so sind die Anforderungen für die Ausgleichsmaßnahme entsprechend anzupassen.
Die Möglichkeit, anstelle einer Ausgleichsmaßnahme den Vorbereitungsdienst für das angestrebte Lehramt zu absolvieren, bleibt unberührt.

#### § 4 Antrag

(1) Der Antrag auf Anerkennung einer Lehrerqualifikation als Befähigung für das angestrebte Lehramt ist schriftlich bei der zuständigen Stelle zu stellen.
Ihm sind als Antragsunterlagen

1.  ein amtlicher Identitätsnachweis der antragstellenden Person,
2.  die Ausbildungsnachweise oder Befähigungsnachweise für die anzuerkennende Lehrerqualifikation sowie Nachweise oder Informationen, aus denen Inhalte und Dauer der absolvierten Ausbildung hervorgehen,
3.  die amtlichen Bescheinigungen über Dauer und Art der bisher im Inland oder Ausland ausgeübten beruflichen Tätigkeiten als Lehrkraft oder sonstige Befähigungsnachweise, sofern diese für die Feststellung erforderlich sind,
4.  im Fall von § 3 Absatz 1 Nummer 1 zweiter Teilsatz eine Bescheinigung über die Berechtigung zur Aufnahme oder Ausübung des Berufs der Lehrerin oder des Lehrers,
5.  eine tabellarische Aufstellung in deutscher Sprache über die absolvierten Ausbildungsgänge und abgelegten Prüfungen sowie der ausgeübten berufsbezogenen Erwerbstätigkeiten,
6.  eine Erklärung in deutscher Sprache, ob und gegebenenfalls mit welchem Ergebnis in einem anderen Land in der Bundesrepublik Deutschland ein entsprechender Antrag gestellt oder eine Ausgleichsmaßnahme absolviert wurde,
7.  gegebenenfalls der Bescheid zur Feststellung der Lehrerqualifikation, der in einem anderen Land in der Bundesrepublik Deutschland erteilt wurde, und
8.  gegebenenfalls der Nachweis gemäß Absatz 5

beizufügen.
Die zuständige Stelle kann weitere Nachweise und Unterlagen verlangen, soweit dies für das Feststellungsverfahren zwingend erforderlich ist.
Die Forderung zur Beibringung weiterer Nachweise und Unterlagen ist von der zuständigen Stelle gegenüber der antragstellenden Person im Einzelnen zu begründen.
Wurde die Lehrerqualifikation ganz oder teilweise in einem Mitgliedstaat erworben, kann sich die zuständige Stelle zu Informationszwecken an die dort zuständige Stelle wenden.

(2) Die Antragsunterlagen gemäß Absatz 1 Satz 2 Nummer 1 bis 4 und Absatz 1 Satz 3 sind in Form von Originalen oder beglaubigten Kopien beizubringen.
Außerdem können von den Antragsunterlagen gemäß Satz 1 Übersetzungen in deutscher Sprache verlangt werden, die von einer in der Bundesrepublik Deutschland öffentlich bestellten oder beeidigten Dolmetscherin oder Übersetzerin oder einem in der Bundesrepublik Deutschland öffentlich bestellten oder beeidigten Dolmetscher oder Übersetzer zu fertigen sind.
Die Kosten für Kopien, Beglaubigungen und Übersetzungen sind von der antragstellenden Person zu tragen.

(3) Die zuständige Stelle kann abweichend von Absatz 2 für die Antragsunterlagen andere Formen zulassen.
Antragsunterlagen, die in einem Mitgliedstaat ausgestellt oder anerkannt wurden, können abweichend von Absatz 2 Satz 1 auch elektronisch übermittelt werden.
Bei begründeten Zweifeln an der Echtheit der elektronisch übermittelten Antragsunterlagen kann sich die zuständige Stelle an die jeweils zuständige Stelle des Mitgliedstaates, in dem sie ausgestellt oder anerkannt wurden, wenden oder die Antragstellerin oder den Antragsteller auffordern, beglaubigte Kopien vorzulegen, wobei der Lauf der Fristen gemäß § 6 Absatz 2 nicht gehemmt wird.
Absatz 2 Satz 2 und 3 gilt entsprechend.

(4) Bestehen begründete Zweifel an der Echtheit oder der inhaltlichen Richtigkeit der Antragsunterlagen, kann die zuständige Stelle die antragstellende Person auffordern, weitere geeignete Nachweise innerhalb einer angemessenen Frist beizubringen.
Absatz 1 Satz 5 gilt entsprechend.

(5) Antragstellende Personen sollen ihre Absicht, den Beruf der Lehrerin oder des Lehrers im Land Brandenburg aufzunehmen und auszuüben, insbesondere durch die Beantragung eines Einreisevisums zur Erwerbstätigkeit oder einen Nachweis über die Kontaktaufnahme mit der einstellenden Schulbehörde oder einer Ersatzschule im Land Brandenburg nachweisen.
Satz 1 gilt nicht für antragstellende Personen, die Staatsbürger eines Mitgliedstaates sind oder ihren ständigen Wohnsitz in einem Mitgliedstaat haben, sofern keine besonderen Gründe gegen eine entsprechende Absicht sprechen.

#### § 5 Feststellung

(1) Die zuständige Stelle stellt bei Erfüllung der Voraussetzungen gemäß § 3 Absatz 1 die Anerkennung der Lehrerqualifikation als Befähigung für ein Lehramt gemäß § 2 Absatz 1 des Brandenburgischen Lehrerbildungsgesetzes fest.

(2) Erfolgt keine Feststellung gemäß Absatz 1, stellt die zuständige Stelle die Qualifikationsunterschiede, die der Anerkennung der Lehrerqualifikation als Befähigung für das angestrebte Lehramt entgegenstehen, und die Möglichkeiten zu ihrem Ausgleich fest.
Der antragstellenden Person sind dazu insbesondere

1.  die Zuordnung der Lehrerqualifikation zu einem Lehramt gemäß § 2 Absatz 1 des Brandenburgischen Lehrerbildungsgesetzes,
2.  die bestehenden Qualifikationsunterschiede sowie
3.  die Möglichkeiten zum Ausgleich der Qualifikationsunterschiede

mitzuteilen.
Für eine in einem Mitgliedstaat erworbene Lehrerqualifikation sind die Qualifikationsunterschiede einschließlich des verlangten Qualifikationsniveaus gemäß Artikel 11 Buchstabe e der Richtlinie 2005/36/EG festzustellen.
Die Angaben gemäß Absatz 2 Satz 2 Nummer 3 umfassen insbesondere die Form und den Zeitpunkt oder die Dauer der Ausgleichsmaßnahme sowie die durch sie zu erwerbenden Qualifikationsmerkmale.

(3) Soweit Qualifikationsunterschiede nur durch wissenschaftliche oder künstlerische Studien (Zusatzausbildung) ausgeglichen werden können, stellt die zuständige Stelle den Studienbereich oder die Studienbereiche sowie den Umfang der jeweils zu erbringenden Studienleistungen fest.
Der Studienumfang ist in Leistungspunkten anzugeben.
Die Entscheidung über die in der Zusatzausbildung zu absolvierenden Module oder zu erbringenden sonstigen Studienleistungen trifft die Bildungseinrichtung, an der die Zusatzausbildung absolviert wird, auf der Grundlage des vorgelegten Ausbildungsnachweises, der sonstigen Befähigungsnachweise und der erworbenen Berufserfahrung.

(4) Wurde eine Lehrerqualifikation bereits in einem anderen Land in der Bundesrepublik Deutschland anerkannt und mit der Befähigung für ein Lehramt nach dem jeweiligen Landesrecht gleichgestellt, gilt § 13 Absatz 2 des Brandenburgischen Lehrerbildungsgesetzes entsprechend.

(5) Kann die antragstellende Person die Antragsunterlagen nicht oder nur teilweise aus nicht selbst zu vertretenden Gründen oder nur mit einem unangemessenen zeitlichen oder sachlichen Aufwand beibringen, sind die berufsbezogenen Kenntnisse und Fähigkeiten durch sonstige geeignete Verfahren festzustellen.
Von der antragstellenden Person sind die Gründe, die der Vorlage der Antragsunterlagen entgegenstehen, glaubhaft darzulegen.
Hierzu kann von der antragstellenden Person eine Versicherung an Eides Statt verlangt und abgenommen werden.

(6) Sonstige geeignete Verfahren sind insbesondere Unterrichtsproben, schriftliche und mündliche Prüfungen, Fachgespräche sowie Gutachten von Sachverständigen, die von der zuständigen Stelle benannt werden.

(7) Die Anerkennung einer Lehrerqualifikation ist zu versagen, wenn die Antragsunterlagen trotz Aufforderung nicht in einer angemessenen Frist vollständig beigebracht werden.
Sie ist außerdem zu versagen, wenn

1.  die Voraussetzungen gemäß § 3 Absatz 1 Nummer 1 erster Teilsatz nicht erfüllt sind oder
2.  die Qualifikationsunterschiede nicht ausgeglichen werden können.

#### § 6 Verfahren

(1) Der Eingang des Antrags ist innerhalb der Frist von einem Monat gegenüber der antragstellenden Person schriftlich zu bestätigen.
In der Eingangsbestätigung ist das Eingangsdatum des Antrags mitzuteilen sowie auf die Frist gemäß Absatz 2 und die Voraussetzungen für den Beginn des Fristlaufs hinzuweisen.
Sind die Antragsunterlagen unvollständig, teilt die zuständige Stelle innerhalb der Frist gemäß Satz 1 der antragstellenden Person schriftlich mit, welche Antragsunterlagen noch beizubringen sind.
Die Mitteilung enthält den Hinweis auf den Fristlauf gemäß Absatz 2, der erst nach Eingang aller Antragsunterlagen beginnt.

(2) Die Feststellungen gemäß § 5 sind innerhalb der Frist von drei Monaten zu treffen.
Der Fristlauf beginnt nach dem Eingang aller Antragsunterlagen bei der zuständigen Stelle.
Sie kann einmal angemessen verlängert werden, wenn dies wegen der Besonderheiten der Angelegenheit gerechtfertigt ist.
Soweit die Lehrerqualifikation in einem Mitgliedstaat erworben oder anerkannt wurde, beträgt die Fristverlängerung gemäß Satz 3 höchstens einen Monat.
Die Fristverlängerung ist der antragstellenden Person rechtzeitig schriftlich mitzuteilen und ihr gegenüber zu begründen.

(3) Im Fall von § 4 Absatz 4 ist der Fristlauf gemäß Absatz 2 Satz 1 bis zum Ablauf der festgelegten Frist gehemmt.

(4) Feststellungen gemäß § 5 sind der antragstellenden Person durch einen schriftlichen Bescheid, der mit einer Rechtsbehelfsbelehrung zu versehen ist, mitzuteilen.

(5) Für die Durchführung des Feststellungsverfahrens werden Gebühren gemäß der Gebührenordnung MBJS erhoben.

(6) Das Verfahren kann, soweit die anzuerkennende Lehrerqualifikation in einem Mitgliedstaat erworben oder anerkannt wurde, auch über den Einheitlichen Ansprechpartner im Sinne des Gesetzes über den Einheitlichen Ansprechpartner für das Land Brandenburg abgewickelt werden.
Absatz 5 bleibt unberührt.

#### § 7 Mitwirkungspflichten

(1) Die antragstellende Person ist verpflichtet, alle geforderten Antragsunterlagen vorzulegen sowie alle dazu erforderlichen Auskünfte zu erteilen.

(2) Kommt die antragstellende Person ihrer Mitwirkungspflicht nicht nach und wird dadurch das Verfahren erheblich erschwert, kann ohne weitere Ermittlungen entschieden werden.
Dies gilt entsprechend, wenn die antragstellende Person in anderer Weise das Verfahren erheblich erschwert.

(3) Der Antrag kann wegen fehlender oder nicht ausreichender Mitwirkung abgelehnt werden, wenn die antragstellende Person auf die Folgen schriftlich hingewiesen worden ist und ihrer Mitwirkungspflicht innerhalb einer angemessenen Frist nicht nachkommt.

### Abschnitt 3  
Ausgleichsmaßnahmen

### Unterabschnitt 1  
Allgemeine Bestimmungen

#### § 8 Ausgleichsmaßnahmen

(1) Als Ausgleichsmaßnahme kann wahlweise

1.  ein Anpassungslehrgang absolviert oder
2.  eine Eignungsprüfung abgelegt

werden.
Ein Anpassungslehrgang kann mit einer Zusatzausbildung gemäß § 5 Absatz 3 einhergehen, soweit festgestellte Qualifikationsunterschiede nicht im Rahmen von Angeboten gemäß § 11 Absatz 2 Satz 2 Nummer 2 ausgeglichen werden können.

(2) Das Wahlrecht gemäß Absatz 1 Satz 1 wird von der antragstellenden Person mit der Bewerbung um die Teilnahme an einer Ausgleichsmaßnahme wahrgenommen.
Nach der Zulassung zu der Ausgleichsmaßnahme ist die Änderung der Wahlentscheidung in der Regel nicht mehr möglich.
Hat sich die antragstellende Person für eine Eignungsprüfung entschieden, ist diese innerhalb von sechs Monaten abzulegen.

#### § 9 Bewerbung um die Teilnahme an einer Ausgleichsmaßnahme

(1) Bewerbungen um die Teilnahme an einem Anpassungslehrgang sind bis spätestens zu der für den Vorbereitungsdienst für das angestrebte Lehramt bekannt gegebenen Bewerbungsfrist an die zuständige Stelle zu richten.
Sie gilt ausschließlich für den Einstellungstermin, für den die Bewerbungsfrist bestimmt wurde.

(2) Bewerbungen um die Teilnahme an einer Eignungsprüfung sind zu jedem Zeitpunkt bei der zuständigen Stelle möglich.

(3) Der Bewerbung sind insbesondere

1.  der Bescheid zu den Feststellungen gemäß § 5 Absatz 2 und 3,
2.  ein erweitertes Führungszeugnis gemäß Bundeszentralregistergesetz oder ein entsprechendes Dokument, das frühestens drei Monate vor der Bewerbung ausgestellt wurde, und
3.  eine persönliche Erklärung zum individuellen Gesundheitszustand gemäß den Vorgaben der zuständigen Stelle, sofern eine Bewerbung um die Teilnahme an einem Anpassungslehrgang erfolgt,

beizufügen.
Für antragstellende Personen mit ständigem Wohnsitz in einem Mitgliedstaat sowie für Staatsangehörige von Mitgliedstaaten genügt anstelle des erweiterten Führungszeugnisses die Vorlage einer Bescheinigung oder Urkunde gemäß Anhang VII Buchstabe g der Richtlinie 2005/36/EG.
Die Vorlage weiterer Unterlagen kann insbesondere dann verlangt werden, wenn ein entsprechender Bescheid gemäß § 5 Absatz 2 und 3 in einem anderen Land in der Bundesrepublik Deutschland erteilt wurde.

(4) Die Bewerbungsmodalitäten für die Teilnahme an einer Zusatzausbildung werden von der sie durchführenden Bildungseinrichtung bestimmt.

(5) Die Teilnahme an einer Ausgleichsmaßnahme setzt voraus, dass die Bewerberin oder der Bewerber, für die oder den Deutsch nicht Muttersprache ist, über die für den Unterricht erforderlichen deutschen Sprachkenntnisse, mindestens jedoch auf der Kompetenzstufe C 2 des Gemeinsamen Europäischen Referenzrahmens, verfügt oder diese nachweist.

### Unterabschnitt 2  
Anpassungslehrgang

#### § 10 Kapazitäten, Zulassung, Rechtsstellung

(1) Für Anpassungslehrgänge sollen mindestens eine und höchstens bis zu 4 Prozent der Stellen, die im Haushaltsgesetz für den Vorbereitungsdienst des jeweiligen Lehramtes ausgebracht sind, bereitgestellt werden.
Übersteigt die Anzahl der zu berücksichtigenden Bewerbungen die Anzahl der zur Verfügung stehenden Stellen, erfolgt die Zulassung nach der zeitlichen Reihenfolge des Eingangs der Bewerbung.
Bei gleichem Eingangsdatum entscheidet die Anzahl der bisher erfolglosen Bewerbungen um die Teilnahme an einen Anpassungslehrgang im Land Brandenburg.

(2) Die zuständige Stelle entscheidet bei fristgemäß eingegangenen Bewerbungen über die Zulassung zum Anpassungslehrgang.
Sie weist der Teilnehmerin oder dem Teilnehmer das Studienseminar des für Schule zuständigen Ministeriums zu, in dessen Zuständigkeit der Anpassungslehrgang durchzuführen ist.
Die Entscheidungen gemäß den Sätzen 1 und 2 sind der Bewerberin oder dem Bewerber rechtzeitig vor Beginn des Anpassungslehrgangs schriftlich mitzuteilen.
Kann keine Zulassung zum Anpassungslehrgang erfolgen, ist die Entscheidung der Bewerberin oder dem Bewerber begründet schriftlich mitzuteilen.
Über die Zulassung zu einer Zusatzausbildung entscheidet die sie durchführende Bildungseinrichtung.

(3) Der Anpassungslehrgang wird im Rahmen eines öffentlich-rechtlichen Ausbildungsverhältnisses absolviert.
In dieser Zeit besteht für die teilnehmende Person ein Anspruch auf Unterhaltsgeld in Höhe der Bezüge für die Lehramtskandidatinnen und Lehramtskandidaten im Vorbereitungsdienst für das angestrebte Lehramt.

(4) Abweichend von Absatz 3 können Lehrkräfte im Schuldienst den Anpassungslehrgang berufsbegleitend absolvieren, wenn

1.  in dem beauftragten Studienseminar die dafür erforderlichen personellen Kapazitäten zur Verfügung stehen,
2.  die Dauer des Beschäftigungsverhältnisses die festgelegte Dauer des Anpassungslehrgangs nicht unterschreitet,
3.  der Einsatz als Lehrkraft dem angestrebten Lehramt entspricht und
4.  der Unterrichtseinsatz gemäß § 11 Absatz 2 Satz 2 Nummer 1 während der Dauer des Anpassungslehrgangs gewährleistet ist.

Abweichend von Absatz 4 Satz 1 Nummer 4 richtet sich der Umfang des Unterrichtseinsatzes nach dem vereinbarten Beschäftigungsumfang, wobei der geforderte Mindestumfang gemäß § 11 Absatz 2 Satz 2 Nummer 1 nicht unterschritten werden darf.

#### § 11 Dauer, Organisation, Durchführung

(1) Die zuständige Stelle bemisst für jede Teilnehmerin und jeden Teilnehmer die Dauer des Anpassungslehrgangs auf Grund der festgestellten Qualifikationsunterschiede.
Sie beträgt mindestens sechs Monate und höchstens drei Jahre.
Die Schulferien werden auf die Dauer in vollem Umfang angerechnet.
Soweit eine Zusatzausbildung verlangt wird, soll bei der Bemessung der Dauer des Anpassungslehrgangs der zu erbringende Studienumfang insofern berücksichtigt werden, dass die geforderten Studienleistungen während der Dauer des Anpassungslehrgangs erbracht werden können.

(2) Die zuständige Stelle beauftragt für jede Teilnehmerin und jeden Teilnehmer ein Studienseminar mit der Organisation und Durchführung des Anpassungslehrgangs (beauftragtes Studienseminar).
Der Anpassungslehrgang beginnt entsprechend den zur Verfügung stehenden Kapazitäten des beauftragten Studienseminars jeweils zum 1.
August oder 1.
Februar eines Kalenderjahres und umfasst

1.  unterrichtspraktische Tätigkeiten im Umfang von mindestens zwölf und höchstens 19 Lehrerwochenstunden in den anzuerkennenden Fächern, Hospitationen und die Teilnahme an schulischen Veranstaltungen (Schulpraxis) an einer Schule (Praxisschule) in Verantwortung ihrer Leiterin oder ihres Leiters und
2.  die Teilnahme an Fortbildungsangeboten des beauftragten Studienseminars.

(3) Das beauftragte Studienseminar bestimmt für jede Teilnehmerin und jeden Teilnehmer

1.  im Benehmen mit der unteren Schulbehörde eine Schule in öffentlicher Trägerschaft oder im Benehmen mit dem Schulträger eine Ersatzschule im Land Brandenburg als Praxisschule und
2.  eine betreuende Seminarleiterin oder einen betreuenden Seminarleiter, die oder der eng mit der Leiterin oder dem Leiter der Praxisschule kooperiert.

Die betreuende Seminarleiterin oder der betreuende Seminarleiter hat das Recht, jederzeit nach Anmeldung im Unterricht der Teilnehmerin und des Teilnehmers zu hospitieren.
Im Rahmen der Hospitationen soll im Benehmen mit der Leiterin oder dem Leiter der Praxisschule der erreichte Qualifikationsstand in einem Beratungsgespräch eingeschätzt werden.
Hospitationen und Beratungsgespräche können auch auf Wunsch der Teilnehmerin oder des Teilnehmers durchgeführt werden.

(4) Die Zusatzausbildung wird von einer Hochschule oder einer von der zuständigen Stelle dafür zugelassenen Einrichtung der Lehrkräftefort- und -weiterbildung (Bildungseinrichtung) in eigener Zuständigkeit auf der Grundlage der Ordnungen für die lehramtsbezogenen Studienangebote organisiert und durchgeführt.
Sie ist so zu organisieren, dass die geforderten Studienleistungen innerhalb der festgelegten Dauer für den Anpassungslehrgang erbracht werden können.

(5) Wird der Anpassungslehrgang aus nicht von der teilnehmenden Person zu verantwortenden Gründen um mehr als zehn Wochen unterbrochen, kann er auf Antrag auch über die Höchstgrenze hinaus angemessen verlängert werden, wenn dadurch das Erreichen des Qualifikationsziels ermöglicht wird.

(6) Wird während des Anpassungslehrgangs festgestellt, dass seine Qualifikationsziele korrekturbedürftig sind, kann die Dauer des Anpassungslehrgangs bis zu der Höchstdauer von drei Jahren auf Antrag der teilnehmenden Person verlängert werden.

(7) Der Anpassungslehrgang endet mit Ablauf der festgelegten Dauer.
Er kann vorzeitig beendet werden, wenn die teilnehmende Person

1.  die sich aus dem Ausbildungsverhältnis ergebenden Pflichten nicht oder nicht ausreichend erfüllt oder
2.  einen Antrag auf vorzeitige Beendigung des Anpassungslehrgangs stellt.

Im Fall gemäß § 11 Absatz 7 Satz 2 Nummer 2 kann sie auf Antrag bis spätestens sechs Monate nach Beendigung des Anpassungslehrgangs abweichend von § 8 Absatz 2 Satz 2 eine Eignungsprüfung ablegen.
§ 8 Absatz 2 Satz 3 gilt entsprechend.

(8) Wurde ein Anpassungslehrgang nicht bestanden oder vorzeitig beendet, kann er nicht wiederholt werden.

#### § 12 Leistungsbeurteilung und -bewertung, Gesamtergebnis des Anpassungslehrgangs

(1) Am Ende des Anpassungslehrgangs sind die von der teilnehmenden Person erbrachten Leistungen in Bezug auf die im Anpassungslehrgang zu erwerbenden Qualifikationsmerkmale gemäß Absatz 2 und 3 schriftlich zu beurteilen.
Die Beurteilungen sind jeweils vor ihrer Weiterleitung der teilnehmenden Person zur Kenntnis zu geben.
Sie hat das Recht, sich zu jeder Beurteilung innerhalb einer Woche nach Kenntnisnahme schriftlich zu äußern.
Die Termine und Fristen für die Anfertigung der Beurteilungen werden von der Leiterin oder dem Leiter des beauftragten Studienseminars bestimmt.

(2) Die Leiterin oder der Leiter der Praxisschule beurteilt die von der teilnehmenden Person in der Schulpraxis erbrachten Leistungen.
Sie oder er leitet die Beurteilung gegebenenfalls zusammen mit der schriftlichen Äußerung der teilnehmenden Person unverzüglich der betreuenden Seminarleiterin oder dem betreuenden Seminarleiter zu.

(3) Die betreuende Seminarleiterin oder der betreuende Seminarleiter beurteilt die von der teilnehmenden Person in Hospitationen gezeigten sowie in Fortbildungsveranstaltungen erbrachten Leistungen und bewertet unter Einbeziehung der Beurteilung gemäß Absatz 2 die im Anpassungslehrgang erbrachte Gesamtleistung mit dem Prädikat „bestanden“ oder „nicht bestanden“.
Sie oder er leitet die Beurteilungen und die Bewertung der Gesamtleistung gegebenenfalls zusammen mit der schriftlichen Äußerung der teilnehmenden Person unverzüglich der Leiterin oder dem Leiter des beauftragten Studienseminars zu.

(4) Wurde eine Zusatzausbildung absolviert, fertigt nach deren Abschluss die durchführende Bildungseinrichtung eine Bescheinigung, die insbesondere

1.  die Bezeichnung der Studiengebiete und der ihnen jeweils zugeordneten Module,
2.  die in den einzelnen Modulen erworbenen Leistungspunkte,
3.  die Bewertung der in den Modulen erbrachten Leistungen mit dem Prädikat „bestanden“ oder „nicht bestanden“ und
4.  die Bewertung der in Zusatzausbildung erbrachte Gesamtleistung mit dem Prädikat „bestanden“ oder „nicht bestanden“

umfasst.

(5) Das Gesamtergebnis des Anpassungslehrgangs ist mit dem Prädikat „bestanden“ zu bewerten, wenn die Gesamtleistungen gemäß Absatz 3 Satz 1 und gegebenenfalls Absatz 4 Nummer 4 jeweils mit dem Prädikat „bestanden“ bewertet wurden.

(6) Die zuständige Stelle stellt auf Grundlage der Leistungsbewertungen gemäß Absatz 3 und gegebenenfalls Absatz 4 Nummer 4 das Gesamtergebnis des Anpassungslehrgangs mit dem Prädikat „bestanden“ oder „nicht bestanden“ sowie die in ihm erworbenen Qualifikationsmerkmale fest.
Die Feststellung erfolgt durch schriftlichen Bescheid, der mit einer Rechtsbehelfsbelehrung zu versehen ist.

### Unterabschnitt 3  
Eignungsprüfung

#### § 13 Zulassung

Die zuständige Stelle entscheidet über die Zulassung zur Eignungsprüfung und teilt die Entscheidung der Bewerberin oder dem Bewerber in einem schriftlichen Bescheid mit.
Außerdem sind in dem Bescheid

1.  das mit der Durchführung der Eignungsprüfung beauftragte Studienseminar,
2.  der Termin der Eignungsprüfung,
3.  die Schule, an der die Eignungsprüfung durchgeführt wird,
4.  die Besetzung des Prüfungsausschusses und
5.  die zu erbringenden Prüfungsleistungen

mitzuteilen.

#### § 14 Inhalt, Teile und Durchführung

(1) Die Eignungsprüfung erstreckt sich ausschließlich auf zu erwerbende Qualifikationsmerkmale, die gemäß § 5 Absatz 2 Satz 4 festgestellt wurden.
Bei den Prüfungsanforderungen ist zu berücksichtigen, dass der Prüfling bereits über eine Lehrerqualifikation verfügt.

(2) Für die Durchführung der Eignungsprüfung gelten die in der Ordnung für den Vorbereitungsdienst getroffenen Regelungen zur Durchführung der Staatsprüfung entsprechend, soweit nichts anderes geregelt ist.
Sie umfasst

1.  eine Unterrichtsprobe in dem anzuerkennenden Fach oder je eine Unterrichtsprobe in den anzuerkennenden Fächern und
2.  eine mündliche Prüfung in Form eines Kolloquiums mit einer zeitlichen Dauer von 40 Minuten

und ist, abgesehen von Unterrichtsproben im fremdsprachlichen oder bilingualen Unterricht, ausschließlich in deutscher Sprache abzulegen.
Alle Prüfungsteile sind am selben Tag abzulegen.
Über den Verlauf und das Ergebnis der Prüfungsteile ist jeweils eine Niederschrift anzufertigen.

(3) Mit der Organisation und Durchführung der Eignungsprüfung wird von der zuständigen Stelle ein Studienseminar beauftragt.
Es bestimmt für jeden Prüfling

1.  im Benehmen mit der unteren Schulbehörde die Schule, an der die Unterrichtsproben durchgeführt werden,
2.  im Einvernehmen mit der Leiterin oder dem Leiter der Schule die Themen für die abzulegenden Unterrichtsproben und
3.  den Termin für das Ablegen der Eignungsprüfung.

(4) Zur Vorbereitung auf die Unterrichtsproben ist der Prüfling berechtigt, für einen Zeitraum von höchstens vier Wochen in den für die Prüfung vorgesehenen Lerngruppen und Fächern zu hospitieren und zu unterrichten.

(5) Der thematische Rahmen für die mündliche Prüfung wird auf Vorschlag des Prüflings von der zuständigen Stelle festgelegt und ihm bis spätestens zehn Tage vor dem Prüfungstermin mitgeteilt.

(6) Für jeden Prüfling sind von der zuständigen Stelle Prüfungsausschüsse zu bilden.
Dem Prüfungsausschuss für die Unterrichtsproben gehören

1.  eine Seminarleiterin oder ein Seminarleiter des beauftragten Studienseminars oder eine Person der Schulaufsicht als Vorsitzende oder Vorsitzender,
2.  eine weitere Seminarleiterin oder ein weiterer Seminarleiter des beauftragten Studienseminars und
3.  ein Schulleitungsmitglied der Schule, in der die Unterrichtsproben abgelegt werden,

als Mitglieder an.
Dem Prüfungsausschuss für die mündliche Prüfung gehören

1.  eine Seminarleiterin oder ein Seminarleiter des beauftragten Studienseminars oder eine Person der Schulaufsicht als Vorsitzende oder Vorsitzender,
2.  je anzuerkennendes Fach eine weitere Seminarleiterin oder ein weiterer Seminarleiter des beauftragten Studienseminars

als Mitglieder an.

#### § 15 Leistungsbewertung, Gesamtergebnis der Eignungsprüfung

(1) Für die Leistungsbewertung gelten die in der Ordnung für den Vorbereitungsdienst getroffenen Regelungen zur Leistungsbewertung entsprechend, soweit nichts anderes geregelt ist.

(2) Die in jeder Unterrichtsprobe und in der mündlichen Prüfung jeweils erbrachte Leistung ist von dem jeweiligen Prüfungsausschuss mit einer Note zu bewerten.
Sie wird aus dem arithmetischen Mittel der Noten, die von jedem Mitglied des Prüfungsausschusses für die erbrachte Leistung erteilt wurde, ermittelt.
Die Ergebnisse der einzelnen Prüfungsteile sind unverzüglich nach Abschluss der Eignungsprüfung der zuständigen Stelle zuzuleiten.

(3) Die Eignungsprüfung ist bestanden, wenn die in den Prüfungsteilen erbrachten Leistungen mindestens mit der Note „ausreichend“ bewertet worden sind.
Wurden Prüfungsteile nicht bestanden, können sie einmal wiederholt werden.
Die Wiederholung muss spätestens sechs Monate nach Bekanntgabe der Note im betreffenden Prüfungsteil bei der zuständigen Stelle vom Prüfling beantragt werden.

(4) Wird ein Prüfungsteil nach seiner Wiederholung nicht bestanden oder lässt der Prüfling die Frist gemäß Absatz 3 Satz 3 ohne anerkannten Grund verstreichen, so sind dieser Prüfungsteil und die Eignungsprüfung endgültig nicht bestanden.

(5) Die zuständige Stelle stellt auf Grund der erbrachten Prüfungsleistungen das Gesamtergebnis der Eignungsprüfung mit dem Prädikat „bestanden“ oder „nicht bestanden“ durch schriftlichen Bescheid, der mit einer Rechtsbehelfsbelehrung zu versehen ist, fest.

### Abschnitt 4  
Schlussvorschriften

#### § 16 Statistik

Für die Durchführung einer Statistik über die Verfahren zur Feststellung der Gleichwertigkeit gilt § 17 des Brandenburgischen Berufsqualifikationsfeststellungsgesetzes.

#### § 17 Übergangsvorschriften

(1) Über Anträge zur Anerkennung einer Lehrerqualifikation, die bis zum Inkrafttreten dieser Verordnung bei der zuständigen Stelle eingegangen sind und über die noch nicht entschieden wurde, ist auf der Grundlage dieser Verordnung zu entscheiden.

(2) Feststellungen, die auf der Grundlage der Lehrerqualifikationsanerkennungsverordnung vom 10. Juni 2013 (GVBl.
II Nr. 46) vor dem Inkrafttreten dieser Verordnung getroffen wurden, bleiben unberührt.

(3) Anpassungslehrgänge, die vor dem Inkrafttreten dieser Verordnung begonnen wurden, sind wahlweise nach der Lehrerqualifikationsanerkennungsverordnung vom 10.
Juni 2013 (GVBl. II Nr. 46) oder nach dieser Verordnung abzuschließen.

#### § 18 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1. November 2016 in Kraft.
Gleichzeitig tritt die Lehrerqualifikationsanerkennungsverordnung vom 10. Juni 2013 (GVBl. II Nr. 46) außer Kraft.

Potsdam, den 29.
November 2016

Der Minister für Bildung,  
Jugend und Sport

Günter Baaske

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#a) Diese Verordnung dient der Umsetzung der Richtlinie 2005/36/EG des Europäischen Parlaments und des Rates vom 7.
September 2005 über die Anerkennung von Berufsqualifikationen (ABl.
L 255 vom 30.9.2005, S.
22), die zuletzt durch die Richtlinie 2013/55/EU des Europäischen Parlaments und des Rates vom 20.
November 2013 (ABl.
L 354 vom 28.12.2013, S.
132) geändert worden ist.