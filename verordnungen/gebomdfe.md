## Verordnung über die Gebühren für Amtshandlungen im Geschäftsbereich des Ministeriums der Finanzen und für Europa (GebOMdFE)

Auf Grund des § 3 Absatz 1 des Gebührengesetzes für das Land Brandenburg vom 7. Juli 2009 (GVBl. I S. 246) verordnet die Ministerin der Finanzen und für Europa:

#### § 1 Gebührentarif

(1) Für Amtshandlungen im Geschäftsbereich des Ministeriums der Finanzen und für Europa werden Gebühren nach anliegendem Gebührentarif, der Bestandteil dieser Verordnung ist, erhoben.

(2) Soweit die Leistungen der Umsatzsteuer unterliegen, werden Gebühren und Auslagen nach dieser Verordnung zuzüglich der gesetzlichen Umsatzsteuer erhoben.

#### § 2 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über die Gebühren für Amtshandlungen im Geschäftsbereich des Ministeriums der Finanzen vom 2. April 2003 (GVBl. II S. 298) außer Kraft.

Potsdam, den 25.
März 2020

Die Ministerin der Finanzen und für Europa

Katrin Lange

* * *

### Anlagen

1

[Anlage - Gebührenordnung des Ministeriums der Finanzen und für Europa](/br2/sixcms/media.php/68/GVBl_II_12_2020-Anlage.pdf "Anlage - Gebührenordnung des Ministeriums der Finanzen und für Europa") 161.6 KB