## Verordnung über das Naturschutzgebiet „Kuhzer See-Klaushagen“

Auf Grund des § 22 Absatz 1 und 2, der §§ 23 und 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542), von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl.
I S.
1972, 1974) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl. I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl. II Nr. 43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Uckermark wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Kuhzer See-Klaushagen”.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 1 638 Hektar.
Es umfasst Flächen in folgenden Fluren:

Stadt/Gemeinde:

Gemarkung:

Flur:

Boitzenburger Land

Haßleben

1, 4, 5;

Jakobshagen

2 bis 4;

Klaushagen

1 bis 4;

Kuhz

1 bis 5;

Wichmannsdorf

1;

Templin

Herzfelde

1 bis 4;

Mittenwalde

Mittenwalde

1, 2.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte Übersichtskarte dient der räumlichen Einordnung des Naturschutzgebietes.
Die in Anlage 2 Nummer 2 aufgeführten sechs topografischen Karten im Maßstab 1 : 10 000 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 3 mit den Blattnummern 1 bis 25 aufgeführten Liegenschaftskarten.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam, sowie beim Landkreis Uckermark, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der naturnahen Waldtypen wie Waldmeister-Buchenwald, Moor- und Feuchtwälder sowie Laubgebüsche, Feldgehölze, Streuobstbestände, Schwimmblatt- und Tauchflurengesellschaften eutroph-mesotropher Seen, Gesellschaften der Seggen- und Röhrichtmoore, Feuchtwiesen- und Trockenrasengesellschaften;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Rotes Waldvögelein (Cephalanthera rubra), Sand-Grasnelke (Armeria elongata), Sumpfcalla (Calla palustris), Karthäusernelke (Dianthus carthusianorum), Heidenelke (Dianthus deltoides), Sand-Strohblume (Helichrysum arenarium), Wasserfeder (Hottonia palustris), Fieberklee (Menyanthes trifoliata) und Krebsschere (Stratiotes aloides);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und Nummer 14 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Braunes Langohr (Plecotus auritus), Blässgans (Anser albifrons), Drosselrohrsänger (Acrocephalus arundinaceus), Eisvogel (Alcedo atthis), Flussseeschwalbe (Sterna hirundo), Gänsesäger (Mergus merganser), Graugans (Anser anser), Große Rohrdommel (Botaurus stellaris), Heidelerche (Lullula arborea), Kiebitz (Vanellus), Kleinralle (Porzana parva), Kranich (Grus grus), Mittelspecht (Dendrocopos medius), Neuntöter (Lanius collurio), Rotmilan (Milvus milvus), Reiherente (Aythya fuligula), Saatgans (Anser fabalis), Sperbergrasmücke (Sylvia nisoria), Schwarzspecht (Dryocopus martius), Schwarzstorch (Ciconia nigra), Tüpfelralle (Porzana porzana), Uferschwalbe (Riparia riparia), Weißstorch (Ciconia ciconia), Knoblauchkröte (Pelobates fuscus), Laubfrosch (Hyla arborea), Moorfrosch (Rana arvalis), Ringelnatter (Natrix natrix), Violetter Feuerfalter (Lycaena alciphron), Lilagold-Feuerfalter (Lycaena hippothoe), Goldlaufkäfer (Carabus auratus), Große Flussmuschel (Unio tumidus), Mond-Azurjungfer (Coenagrion lunulatum) und Grüne Mosaikjungfer (Aeshna viridis);
4.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit einer reichgegliederten störungsarmen Landschaft, geprägt von einem Mosaik aus Seen, Mooren, Kleingewässern, Feucht-, Frisch- und Trockengrünländern, extensiv bewirtschafteten Äckern sowie naturnahen Wäldern;
5.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Biotopverbundes zwischen den Gebieten von gemeinschaftlicher Bedeutung „Jungfernheide“, „Mellensee-Marienfließ, „Suckowseen“ und dem Biosphärenreservat „Schorfheide-Chorin“.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Kuhzer See-Klaushagen“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), das die ehemaligen Gebiete von gemeinschaftlicher Bedeutung „Kuhzer See-Jakobshagen“ und „Klaushagen“ umfasst, mit seinen Vorkommen von

1.  Oligo- bis mesotrophen kalkhaltigen Gewässern mit benthischer Vegetation aus Armleuchteralgen, Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Mageren Flachland-Mähwiesen (Alopecurus pratensis, Sanguisorba officinalis) und Waldmeister-Buchenwald (Asperulo-Fagetum) als natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Fischotter (Lutra lutra), Kammmolch (Triturus cristatus), Rotbauchunke (Bombina bombina), Bitterling (Rhodeus amarus), Großer Moosjungfer (Leucorrhinia pectoralis) sowie Schmaler und Bauchiger Windelschnecke als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten des Gebietes außerhalb von Bruchwäldern, Röhrichten, Feuchtwiesen und Mooren zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 11 jeweils nach dem 30.
    Juni eines jeden Jahres;
10.  zu baden oder zu tauchen; ausgenommen ist das Baden im Kuhzer See, im Großen Trebowsee, im Kleinen Trebowsee und im Großen Mäuschensee;
11.  außerhalb der für den öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb von Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, und außerhalb der Waldbrandwundstreifen zu reiten;
12.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der für den öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen.
    Außerhalb von Röhrichten, Schwimmblattgesellschaften oder Verlandungsbereichen bleibt das Befahren des Kuhzer Sees, des Großen Mäuschensees und des Großen Trebowsees mit muskelkraftbetriebenen Booten, einschließlich Luftmatratzen zulässig.
    Auf dem Kuhzer See ist darüber hinaus das Befahren mit elektrisch angetriebenen Booten mit einer maximalen Leistungskraft von 600 Watt mit Genehmigung der unteren Naturschutzbehörde zulässig;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel aller Art zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und des § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertende Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle, Jauche, Gärreste und Sekundärrohstoffdünger einzusetzen.
          
          
        Sekundärrohstoffdünger im Sinne dieser Verordnung sind Abwasser, Fäkalien, Klärschlamm und ähnliche Stoffe aus Siedlungsabfällen und vergleichbare Stoffe aus anderen Quellen, jeweils auch weiterbehandelt und in Mischungen untereinander oder mit Düngemitteln, Wirtschaftsdünger, Bodenhilfsstoffen, Kultursubstraten und Pflanzenhilfsmitteln,
    2.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt, wobei bei Narbenschäden eine umbruchlose Nachsaat zulässig bleibt;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  die Nutzung der Laubwälder einzelstamm- bis truppweise erfolgt,
    2.  in Misch- und Nadelwäldern Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrates reduzieren, nur bis zu einer Größe von maximal 0,5 Hektar zulässig sind,
    3.  nur Baumarten der potenziell natürlichen Vegetation eingebracht werden, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind.
        Nebenbaumarten dürfen dabei nicht als Hauptbaumart eingesetzt werden,
    4.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    5.  das Befahren des Waldes nur auf Wegen oder Rückegassen erfolgt,
    6.  der Boden unter Verzicht auf Pflügen und Umbruch bearbeitet wird; ausgenommen ist eine streifenweise, flachgründige, nicht in den Mineralboden eingreifende Bodenverwundung zur Unterstützung von Verjüngungsmaßnahmen,
    7.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß und einer Mindesthöhe von fünf Metern nicht gefällt werden und liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimetern am stärksten Ende) im Bestand verbleibt,
    8.  ein Altholzanteil von mindestens 10 Prozent am aktuellen Bestandesvorrat zu sichern ist, wobei, sofern vorhanden, mindestens fünf Stämme je Hektar mit einem Brusthöhendurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum Absterben aus der Nutzung genommen sein müssen,
    9.  § 4 Absatz 2 Nummer 17 und 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigem Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Fischbesatz nur mit heimischen Fischarten erfolgt und der Besatz mit Karpfen im Kleinen Mäuschensee, Kleinen Trebowsee, Großen Trebowsee und Kuhzer See unzulässig ist,
    2.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass ein Einschwimmen und eine Gefährdung des Fischotters weitgehend ausgeschlossen ist,
    3.  im Übrigen § 4 Absatz 2 Nummer 19 gilt;
4.  die rechtmäßige Ausübung der Angelfischerei am Kuhzer See, am Großen Trebowsee und am Kleinen Trebowsee sowie am Großen Mäuschensee mit der Maßgabe, dass
    1.  die Angelnutzung am Großen Mäuschensee nur vom Boot aus erfolgt,
    2.  im Übrigen § 4 Absatz 2 Nummer 19 und 20 gilt;
5.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Jagd auf Wasserfederwild verboten ist,
        
        bb)
        
        die Fallenjagd mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern vom Gewässerufer verboten ist.
        Ausnahmen von der Einhaltung dieses Abstandes kann die untere Naturschutzbehörde erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
        cc)
        
        die Baujagd in einem Abstand von bis zu 100 Metern vom Ufer aller innerhalb des Schutzgebietes liegenden Gewässer unzulässig ist,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht oder nur unerheblich beeinträchtigt wird,
    3.  das Aufstellen transportabler und mobiler Ansitzeinrichtungen,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und des in § 3 Absatz 2 Nummer 1 genannten Lebensraumtyps Magere Flachland-Mähwiesen.
    
    Ablenkfütterungen, die Anlage von Ansaatwildwiesen sowie die Anlage oder Unterhaltung von Wildäckern sind unzulässig.
    Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
6.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege.
    Die untere Naturschutzbehörde ist rechtzeitig mit dem Ziel einer einvernehmlichen Lösung zu beteiligen;
7.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
8.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
9.  die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern diese nicht unter die Nummern 6 und 7 fallen, im Einvernehmen mit der unteren Naturschutzbehörde;
10.  andere bei Inkrafttreten dieser Verordnung aufgrund behördlicher Einzelfallentscheidung zugelassene Nutzungen oder rechtmäßig ausgeübte Nutzungen in der bisherigen Art und im bisherigen Umfang;
11.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch nach dem 30. Juni eines jeden Jahres;
12.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
13.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der zuständigen Naturschutzbehörde zugelassen oder angeordnet worden sind;
14.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen;
15.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungserfordernissen, die sich aus anderen fachrechtlichen Vorgaben ergeben, unberührt.

#### § 6 Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  die Durchführung von wasserrückhaltenden Maßnahmen in Mooren und Gewässern, insbesondere das Setzen von Sohlschwellen wird angestrebt;
2.  Kleingewässer sollen unter besonderer Berücksichtigung des Amphibienschutzes entwickelt oder wiederhergestellt werden;
3.  im Wassereinzugsbereich des Kuhzer Sees, des Großen Trebowsees, des Kleinen Trebowsees, des Großen und Kleinen Mäuschensees wird die Umwandlung von Ackerflächen in Grünland angestrebt;
4.  die Extensivierung von Ackerflächen sowie die Anlage von Randstreifen auf Ackerflächen an Kleingewässern wird angestrebt;
5.  faunenfremde Fischarten im Großen Trebowsee und Kuhzer See sowie erhöhter Fischbestände in Söllen und Kleingewässer, insbesondere nördlich des Kuhzer Sees, sollen abgefischt werden;
6.  die Steilwand in der ehemaligen Kiesgrube südlich des Großen Trebowsees soll als Brutstätte für Uferschwalben entwickelt werden;
7.  östlich von Jakobshagen wird die Entwicklung von Hutewäldern durch Beweidung angestrebt.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

§ 5 Absatz 1 Nummer 1 tritt am 1.
Januar 2019 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 19.
November 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Kuhzer See-Klaushagen“](/br2/sixcms/media.php/68/GVBl_II_87_2018-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Kuhzer See-Klaushagen“") 593.7 KB

2

[Anlage 2 (zu § 2 Absatz 2) - Übersichtskarte, Topografische Karten, Liegenschaftskarten](/br2/sixcms/media.php/68/GVBl_II_87_2018-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Übersichtskarte, Topografische Karten, Liegenschaftskarten") 242.8 KB