## Verordnung über das Naturschutzgebiet „Leitsakgraben“

Auf Grund des § 22 Absatz 1 und 2, der §§ 23 und 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542), von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl.
I S.
1972, 1974) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21. Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl. II Nr. 43) verordnet der Minister für Landwirtschaft, Umwelt und Klimaschutz:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Havelland wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Leitsakgraben“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 879 Hektar.
Es umfasst Flächen in folgenden Fluren:

Stadt/Gemeinde:

Gemarkung:

Flur:

Nauen

Börnicke

7;

Nauen

2 bis 5, 8, 37, 38;

Schönwalde-Glien

Grünefeld

1, 5, 6, 8;

Paaren im Glien

1, 4, 11, 13.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 3 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 13 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Innerhalb des Naturschutzgebietes ist eine Zone 1 mit rund 12 Hektar mit besonderen Beschränkungen der Grünlandnutzung festgesetzt.
Die Zone 1 liegt in der Gemeinde Schönwalde-Glien, Gemarkung Paaren im Glien, Flur 13.
Die Grenze der Zone ist in den in Anlage 2 Nummer 1 genannten topografischen Karten mit den Blattnummern 1 bis 3 sowie in den in Anlage 2 Nummer 2 genannten Liegenschaftskarten mit den Blattnummern 1 bis 13 mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Havelland, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebiets, eines laubholzreichen Waldgebiets mit eingelagerten Grünlandflächen im Ostteil des Havelländischen Luchs, ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Röhrichte, Riede und Wasserpflanzenfluren in naturnahen Kleingewässern und Gräben, der feuchten bis trockenen Staudenfluren, der Feucht- und Frischwiesen mit ihren Brachestadien, der Sandtrockenrasen, der naturnahen Wälder wie Erlenbruchwälder, Erlen-Eschen-Niederungswälder, Rotbuchen-, Eichen- und Eichen-Hainbuchenmischwälder sowie der standorttypischen Gebüsche feuchter bis frischer Standorte;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Große Händelwurz (Gymnadenia conopsea), Helm-Knabenkraut (Orchis militaris), Geflecktes Knabenkraut (Dactylorhiza maculata), Breitblättriges Knabenkraut (Dactylorhiza majalis), Aschersons Knabenkraut (Dactylorhiza x aschersoniana), Fleischfarbenes Knabenkraut (Dactylorhiza incarnata), Vogel-Nestwurz (Neottia nidus-avis), Breitblättrige Stendelwurz (Epipactis helleborine), Großes Zweiblatt (Listera ovata), Sumpf-Platterbse (Lathyrus palustris), Wasserfeder (Hottonia palustris), Pracht-Nelke (Dianthus superbus), Heide-Nelke (Dianthus deltoides), Leberblümchen (Hepatica nobilis), Echte Schlüsselblume (Primula veris), Körnchen-Steinbrech (Saxifraga granulata) und Gewöhnliche Grasnelke (Armeria maritima);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Säugetiere, Vögel, Amphibien, Reptilien und Insekten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Fransenfledermaus (Myotis nattereri), Brandtfledermaus (Myotis brandtii), Kleine Bartfledermaus (Myotis mystacinus), Wasserfledermaus (Myotis daubentonii), Braunes Langohr (Plecotus auritus), Breitflügelfledermaus (Eptesicus serotinus), Zwergfledermaus (Pipistrellus pipistrellus), Mückenfledermaus (Pipistrellus pygmaeus), Rauhautfledermaus (Pipistrellus nathusii), Großer Abendsegler (Nyctalus noctula), Kleiner Abendsegler (Nyctalus leisleri), Schwarzstorch (Ciconia nigra), Wespenbussard (Pernis apivorus), Rotmilan (Milvus milvus), Kranich (Grus grus), Mittelspecht (Dendrocopus medius), Schwarzspecht (Dendrocopus martius), Pirol (Oriolus oriolus), Neuntöter (Lanius collurio), Braunkehlchen (Saxicola rubetra), Grauammer (Emberiza calandra), Teichmolch (Triturus vulgaris), Moorfrosch (Rana arvalis), Grasfrosch (Rana temporaria), Waldeidechse (Lacerta vivipara), Ringelnatter (Natrix natrix), Großer Rosenkäfer (Protaetia aeruginosa), Scharlachroter Plattkäfer (Cucujus cinnaberinus) und Schwalbenschwanz (Papilio machaon);
4.  die Erhaltung eines eiszeitlich geprägten Niederungsgebiets aus grundwassernahen, zum Teil kalkhaltigen Niedermoor-, Anmoor- und Talsandbereichen sowie stellenweise aufgesetzten Dünen aus naturgeschichtlichen Gründen;
5.  die Erhaltung des Gebietes zur Umweltbeobachtung und wissenschaftlichen Untersuchung ökologischer Zusammenhänge in Wäldern und Feuchtwiesen des östlichen Havelländischen Luchs;
6.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit eines Ausschnitts des Havelländischen Luchs mit ausgedehnten strukturreichen laubholzgeprägten Wäldern und eingelagerten blütenreichen Wiesenflächen;
7.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Biotopverbundes im Bereich des Havellandes, insbesondere zwischen den Waldgebieten Lindholz und Paulinenauer Luch sowie Bredower Forst und Heimscher Heide.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Leitsakgraben“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinem Vorkommen von

1.  Pfeifengraswiesen auf kalkreichem Boden, torfigen und tonig-schluffigen Böden (Molinion caeruleae), Feuchten Hochstaudenfluren der planaren und montanen bis alpinen Stufe, Mageren Flachland-Mähwiesen (Alopecurus pratensis, Sanguisorba officinalis), Waldmeister-Buchenwald (Asperulo-Fagetum), Subatlantischem oder mitteleuropäischem Stieleichenwald oder Eichen-Hainbuchenwald (Carpinion betuli) und Alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur als natürlichen Lebensraumtypen von gemeinschaftlichen Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Auenwäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion, Alnion incanae, Salicion albae) als prioritärem natürlichen Lebensraumtyp im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
3.  Fischotter (Lutra lutra), Großem Mausohr (Myotis myotis), Kammmolch (Triturus cristatus), Schlammpeitzger (Misgurnus fossilis), Schmaler Windelschnecke (Vertigo angustior) als Tierarten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume;
4.  Eremit (Osmoderma eremita) als prioritärer Tierart von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 11 des Bundesnaturschutzgesetzes, einschließlich seiner für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten jeweils zwischen dem 1. Juli eines jeden Jahres und dem 28.
    Februar des Folgejahres außerhalb von Feuchtwiesen und Feuchtwäldern zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 10;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der Wege, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, und außerhalb der Waldbrandwundstreifen zu reiten;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
13.  Hunde frei laufen zu lassen;
14.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
15.  Düngemittel aller Art zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
16.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
17.  Tiere zu füttern oder Futter bereitzustellen;
18.  Tiere auszusetzen oder Pflanzen anzusiedeln;
19.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
20.  wildlebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
21.  Pflanzenschutzmittel jeder Art anzuwenden;
22.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  auf Grünland § 4 Absatz 2 Nummer 21 und 22 gilt, wobei bei Narbenschäden eine umbruchlose Nachsaat mit standortangepassten Grasarten zulässig bleibt,
    2.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertende Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle, Jauche, flüssige Gärreste und Sekundärrohstoffdünger einzusetzen.
          
        Sekundärrohstoffdünger im Sinne dieser Verordnung sind Abwasser, Fäkalien, Klärschlamm und ähnliche Stoffe aus Siedlungsabfällen und vergleichbare Stoffe aus anderen Quellen, jeweils auch weiterbehandelt und in Mischungen untereinander oder mit Düngemitteln, Wirtschaftsdünger, Bodenhilfsstoffen, Kultursubstraten und Pflanzenhilfsmitteln,
    3.  Grünland der Zone 1 als Wiese genutzt wird und § 4 Absatz 2 Nummer 15 gilt;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  nur Arten der jeweils potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Gehölzarten in gesellschaftstypischer Zusammensetzung unter Ausschluss eingebürgerter Arten zu verwenden sind; von der gesellschaftstypischen Artenzusammensetzung kann nach Kalamitätsausfällen, wie zum Beispiel durch das Eschentriebsterben, mit Zustimmung der unteren Naturschutzbehörde abgewichen werden,
    2.  in gesetzlich geschützten Biotopen und in den in § 3 Absatz 2 Nummer 1 und 2 aufgeführten Waldlebensraumtypen eine Nutzung nur einzelstamm- bis gruppenweise erfolgt sowie außerhalb der genannten Biotope und Lebensraumtypen Holzerntemaßnahmen die den Holzvorrat auf weniger als 40 von 100 des üblichen Vorrats reduzieren nur bis zu einer Größe von 0,5 Hektar zulässig sind,
    3.  hydromorphe Böden sowie Böden mit einem hohen Anteil an feinkörnigem Substrat nur bei ausreichender Tragfähigkeit auf dauerhaft festgelegten Rückegassen befahren werden,
    4.  in gesetzlich geschützten Biotopen und in den in § 3 Absatz 2 Nummer 1 und 2 aufgeführten Waldlebensraumtypen Pflügen und Umbruch des Bodens unzulässig sind; ausgenommen ist eine streifenweise, flachgründige, nicht in den Mineralboden eingreifende Bodenverwundung zur Unterstützung von Verjüngungsmaßnahmen,
    5.  Bäume mit Horsten oder Höhlen nicht gefällt werden; ausgenommen davon sind vom Eschentriebsterben befallene Bäume,
    6.  mindestens fünf dauerhafte markierte Stämme von lebensraumtypischen Arten je Hektar mit einem Brusthöhendurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum Absterben aus der Nutzung genommen sein müssen,
    7.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 30 Zentimeter Brusthöhendurchmesser in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden; liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimetern am stärkeren Ende) verbleibt im Bestand,
    8.  in den in § 3 Absatz 2 Nummer 1 und 2 aufgeführten Waldlebensräumen lebensraumtypische Gehölzarten in der Reifephase mit einer Deckung von 25 Prozent zu erhalten oder, sofern nicht vorhanden, zu entwickeln sind,
    9.  § 4 Absatz 2 Nummer 15 und 21 gilt,
    10.  bei forstwirtschaftlichen Maßnahmen auf dem Flurstück 173 in der Gemarkung Nauen, Flur 5, der Orchideenbestand von Vogel-Nestwurz nicht beeinträchtigt wird;
3.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Fallenjagd nur mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern zu Gewässerufern verboten ist.
        Von der Einhaltung dieses Abstandes kann die untere Naturschutzbehörde Ausnahmen erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
        bb)
        
        keine Baujagd in einem Abstand von bis zu 100 Metern zu Gewässerufern vorgenommen wird,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen außerhalb gesetzlich geschützter Biotope und der in § 3 Absatz 2 Nummer 1 genannten Flachlandmähwiesen nach der Anzeige bei der unteren Naturschutzbehörde.
        Die Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht,
    3.  das Aufstellen von transportablen und mobilen Ansitzeinrichtungen,
    4.  die Anlage und Unterhaltung von Ansaatwildwiesen und Wildäckern außerhalb gesetzlich geschützter Biotope und der in § 3 Absatz 2 Nummer 1 genannten Flachlandmähwiesen mit Genehmigung der unteren Naturschutzbehörde.
    
    Die Anlage von Kirrungen innerhalb gesetzlich geschützter Biotope und der in § 3 Absatz 2 Nummer 1 genannten Flachlandmähwiesen sowie Ablenkfütterungen sind unzulässig.
    Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
4.  Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne einer Fischbestandskontrolle, -regulierung und -förderung mit Zustimmung der unteren Naturschutzbehörde.
    Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird;
5.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege.
    Die untere Naturschutzbehörde ist rechtzeitig mit dem Ziel einer einvernehmlichen Lösung zu beteiligen;
6.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
7.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen (in der bisherigen Art und im bisherigen Umfang).
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
8.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
9.  die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen und privater Wege, sofern diese nicht unter die Nummern 6 und 7 fallen, im Einvernehmen mit der unteren Naturschutzbehörde;
10.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch zwischen dem 1.
    Juli eines Jahres und dem 28.
    Februar des Folgejahres;
11.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
12.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der zuständigen Naturschutzbehörde zugelassen oder angeordnet worden sind;
13.  die Durchführung von Natur- und Umweltbildungsveranstaltungen mit Zustimmung der unteren Naturschutzbehörde;
14.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen;
15.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungserfordernissen, die sich aus anderen Vorgaben ergeben, unberührt.

#### § 6 Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  zur Erhaltung und Entwicklung artenreicher Pfeifengraswiesen in der Zone 1 sollen diese nicht vor dem 1.
    Juli eines jeden Jahres als Mähwiese mit standortangepasster Technik und zeitnahem Abtransport des Mahdguts genutzt werden.
    In Abstimmung mit der unteren Naturschutzbehörde kann zum Zweck der Aushagerung auf Teilflächen eine vorgezogene Mahd erfolgen;
2.  zur Erhaltung und Entwicklung artenreicher Grünlandvegetation frischer bis mäßig feuchter Standorte soll diese zweimal jährlich, vorzugsweise als Mähwiese mit zeitnahem Abtransport des Mahdguts genutzt werden.
    Dabei soll die erste Nutzung bis Mitte Juni, die zweite nach Ende einer Nutzungsruhe von mindestens acht Wochen erfolgen.
    Bei Weidenutzung soll eine Nachmahd erfolgen.
    An charakteristischen Arten verarmtes Grünland soll zum Beispiel durch Übertragung von Mahdgut aufgewertet werden;
3.  zur Erhaltung und Entwicklung von naturnah bestockten und strukturierten Waldflächen sollen bei deren Bewirtschaftung
    1.  die für die jeweiligen Waldlebensraumtypen charakteristischen Hauptbaumarten, insbesondere Stiel- und Trauben-Eiche, durch aktive Erhaltungs- und Verjüngungsmaßnahmen gefördert werden,
    2.  nicht gebietsheimische Gehölzarten, wie zum Beispiel Späte Traubenkirsche, Rot-Eiche, Rot-Esche, Robinie, Eschen-Ahorn, Schneebeere, Douglasie, Fichten-, Tannen- und Lärchenarten im Rahmen der Nutzung durch bevorzugte Entnahme und Mischungsregulierung zurückgedrängt werden,
    3.  die Naturverjüngung der gebietsheimischen und jeweils standorttypischen Baum- und Straucharten durch Regulierung des Schalenwildbestandes gefördert werden,
    4.  Kiefernforste in naturnahe Laubmischwälder überführt werden,
    5.  Horst- und Höhlenbäume sowie potenzielle Biotopbäume vor der forstlichen Nutzung markiert werden,
    6.  an Bestandsrändern strukturreiche Waldmäntel aus standortgerechten, gebietseigenen Gehölzen sowie in geeigneten Bereichen artenreiche Krautsäume erhalten und entwickelt werden,
    7.  Brutbäume altholzbewohnender Käferarten, insbesondere des Eremiten, bei Bedarf freigestellt werden, um ein geeignetes Mikroklima in den Baumhöhlen sicherzustellen.
        Im weiteren Umfeld sollen potenzielle Brutbäume erhalten und gegebenenfalls entwickelt werden,
    8.  absterbende Hybrid-Pappeln mit feuchtem Bast als Teillebensraum für Totholz bewohnende Insekten im Bestand belassen werden.
        Bei notwendiger Verkehrssicherung sollen Rumpfstämme möglichst erhalten bleiben;
4.  zur Stabilisierung und Verbesserung des Landschaftswasserhaushaltes im Gebiet sollen vorhandene Stauanlagen instandgesetzt und gehalten werden.
    Unter Berücksichtigung der Erfordernisse angrenzender Nutzungen sollen fachlich abgesicherte, schutzzweckkonforme Stauziele festgelegt werden;
5.  zur Erhaltung und Entwicklung der Artenvielfalt in Gewässern soll deren Unterhaltung schonend nach dem 30.
    September eines jeden Jahres unter Berücksichtigung der Lebensraumansprüche des Schlammpeitzgers erfolgen.
    Die Böschungsmahd soll jeweils nur einseitig durchgeführt werden, Krautungs- und Mähgut soll beräumt werden.
    Im Bereich von landwirtschaftlichen Nutzflächen sollen Gewässerrandstreifen eingerichtet werden dabei können besonnte Gräben mit standorttypischen Gehölzen gesäumt werden;
6.  die ökologische Durchgängigkeit von Gewässern soll verbessert und Straßenunterführungen sollen ottergerecht gestaltet werden;
7.  zur Erhaltung und Verbesserung der Lebensbedingungen des Kammmolches und weiterer Amphibienarten soll die Wasserführung von Kleingewässern gegebenenfalls durch eine schonende Entschlammung gestützt und eine Beschattung der Gewässer durch partielle Entnahme sonnenseitiger Ufergehölze reduziert werden;
8.  es sollen geeignete Einrichtungen zur Besucherlenkung und -information geschaffen werden.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

§ 5 Absatz 1 Nummer 1 tritt am 1.
Januar 2021 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 11.
September 2020

Der Minister für Landwirtschaft,  
Umwelt und Klimaschutz

Axel Vogel

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Leitsakgraben“](/br2/sixcms/media.php/68/GVBl_II_83_2020-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Leitsakgraben“") 629.7 KB

2

[Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten](/br2/sixcms/media.php/68/GVBl_II_83_2020-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten") 208.2 KB

3

[Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Leitsakgraben“](/br2/sixcms/media.php/68/GVBl_II_83_2020-Anlage-3.pdf "Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Leitsakgraben“") 182.4 KB