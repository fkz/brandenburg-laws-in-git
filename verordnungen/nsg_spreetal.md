## Verordnung über das Naturschutzgebiet „Spreetal zwischen Neubrück und Fürstenwalde“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542), von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl.
I S.
1972) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 und § 42 Absatz 2 Satz 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21. Januar 2013 (GVBl.
I Nummer 3), von denen § 8 Absatz 1 durch Artikel 1 des Gesetzes vom 25. September 2020 (GVBl.
I Nr. 28) geändert worden ist, und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nummer 43), der durch Artikel 2 des Gesetzes vom 25.
September 2020 (GVBl.
I Nr.
28) geändert worden ist, verordnet der Minister für Landwirtschaft, Umwelt und Klimaschutz:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Oder-Spree wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Spreetal zwischen Neubrück und Fürstenwalde“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 1 343 Hektar.
Es umfasst Flächen in folgenden Fluren:

Stadt/Gemeinde:

Gemarkung:

Flur:

Berkenbrück

Berkenbrück

2, 5 bis 9;

Briesen (Mark)

Neubrück Forst

1, 3, 5, 7;

Fürstenwalde/Spree

Fürstenwalde/Spree

21, 45;

Langewahl

Langewahl

2, 4;

Madlitz-Wilmersdorf

Madlitz Forst

1;

Rietz-Neuendorf

Alt Golm

5 bis 7;

Drahendorf

1, 2, 4;

Neubrück

1 bis 7, 9, 14.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte Übersichtskarte im Maßstab 1 : 50 000 dient der räumlichen Einordnung des Naturschutzgebietes.
Die in Anlage 2 Nummer 2 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 7 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 3 mit den Blattnummern 1 bis 29 aufgeführten Liegenschaftskarten.
Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 3 beigefügt.

(3) Innerhalb des Naturschutzgebietes ist gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes eine Zone 1 festgesetzt, die der direkten menschlichen Einflussnahme weitgehend entzogen ist.
Die Zone 1 (Rietzer See) umfasst rund zehn Hektar und befindet sich auf dem Flurstück 7 der Flur 2, Gemarkung Neubrück, Gemeinde Rietz-Neuendorf.
Weiterhin sind innerhalb des Naturschutzgebietes eine Zone 2 und eine Zone 3 mit Beschränkungen der landwirtschaftlichen Bodennutzung festgesetzt.

Die Zone 2 umfasst rund 360 Hektar und liegt in folgenden Fluren:

Stadt/Gemeinde:

Gemarkung:

Flur:

Berkenbrück

Berkenbrück

5, 6, 8;

Briesen (Mark)

Neubrück Forst

1, 3, 7;

Fürstenwalde/Spree

Fürstenwalde/Spree

21, 45;

Langewahl

Langewahl

2;

Rietz-Neuendorf

Alt Golm

5 bis 7;

Drahendorf

1;

Neubrück

1 bis 7.

Die Zone 3 umfasst rund 68 Hektar und liegt in folgenden Fluren:

Stadt/Gemeinde:

Gemarkung:

Flur:

Briesen (Mark)

Neubrück Forst

1;

Fürstenwalde/Spree

Fürstenwalde/Spree

45;

Langewahl

Langewahl

2;

Neubrück

1, 5, 6.

Die Grenzen der Zonen sind in der in Anlage 2 Nummer 1 genannten Übersichtskarte, in den in Anlage 2 Nummer 2 genannten topografischen Karten mit den Blattnummern 1 bis 7 sowie in den in Anlage 2 Nummer 3 genannten Liegenschaftskarten mit den Blattnummern 1 bis 29 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Oder-Spree, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes, das eine vielfältig strukturierte Auenlandschaft des Spreetals mit randlichen, bewaldeten Talsandflächen innerhalb des Berlin-Fürstenwalder Urstromtals umfasst, ist

1.  die Erhaltung, naturnahe Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Still- und Fließgewässer wie Schwimmblatt- und Tauchfluren, der Röhrichte und Riede in Verlandungs- und Auenbereichen, der Moore, des Grünlands feuchter bis trockener Standorte mit deren Brachen, der Staudenfluren feuchter bis trockener Standorte, der Gebüsche mittlerer bis feuchter Standorte, der Auen-, Bruch- und Moorwälder sowie der Laubmischwälder auf Talsandstandorten;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Wasserfeder (Hottonia palustris), Krebsschere (Stratiotes aloides), Fieberklee (Menyanthes trifoliata), Sumpf-Schlangenwurz (Calla palustris), Zungen-Hahnenfuß (Ranunculus lingua), Breitblättriges Knabenkraut (Dactylorhiza majalis), Gottes-Gnadenkraut (Gratiola officinalis), Sumpf-Platterbse (Lathyrus palustris), Langblättriger Blauweiderich (Pseudolysimachium longifolia), Ähriger Blauweiderich (Pseudolysimachium spicata), Gemeine Grasnelke (Armeria elongata) sowie Heide-Nelke (Dianthus deltoides);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Säugetiere, der Vögel, Kriechtiere, Lurche, Hautflügler, Libellen und Muscheln, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Großer Abendsegler (Nyctalus noctula), Große Bartfledermaus (Myotis brandtii), Wasserfledermaus (Myotis daubentonii), Rauhautfledermaus (Pipistrellus nathusii), Braunes Langohr (Plecotus auritus), Mittelspecht (Dendrocopus medius), Wespenbussard (Pernis apivorus), Kranich (Grus grus), Eisvogel (Alcedo atthis), Schwarzstorch (Ciconia nigra), Bekassine (Gallinago gallinago), Drosselrohrsänger (Acrocephalus arundinaceus), Zaun​eidechse (Lacerta agilis), Ringelnatter (Natrix natrix), Moorfrosch (Rana arvalis), Erdkröte (Bufo bufo), Hornisse (Vespa crabro), Gemeine Heidelibelle (Sympetrum vulgatum), Asiatische Keiljungfer (Gomphus flavipes), Östliche Moosjungfer (Leucorrhinia albifrons), Veränderlicher Edelscharrkäfer (Gnorimus variabilis), Marmorierter Rosenkäfer (Protaetia lugubris) und Abgeplattete Teichmuschel (Pseudanodonta complanata);
4.  die Erhaltung der besonderen Eigenart, hervorragenden Schönheit und hohen strukturellen Vielfalt des Gebietes, das durch Fließ- und Stillgewässer mit ihren Begleitstrukturen, ausgedehntes blütenreiches Niederungsgrün​land sowie naturnahe, altbaumreiche Waldbestände auf den umgebenden Talsandbereichen charakterisiert ist;
5.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Biotopverbundes innerhalb des Spreetals.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Spreetal zwischen Neubrück und Fürstenwalde“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), das Teilbereiche der ehemaligen Gebiete von gemeinschaftlicher Bedeutung „Drahendorfer Spreeniederung“ und „Spree“ umfasst, mit seinen Vorkommen von

1.  Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Flüssen der planaren Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Pfeifengraswiesen auf kalkreichen, torfigen und tonig-schluffigen Böden (Molinion caeruleae), Feuchten Hochstaudenfluren der planaren Stufe, Brenndolden-Auenwiesen (Cnidion dubii), Mageren Flachland-Mähwiesen mit Wiesenfuchsschwanz (Alope​curus pratensis), Subatlantischem oder mitteleuropäischem Stieleichenwald oder Hainbuchenwald (Carpinion-betuli), Labkraut-Eichen-Hainbuchenwald (Galio-Carpi​ne​tum) und Alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur als natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnatur​schutzgesetzes;
2.  Auen-Wäldern mit Alnus glutinosa (Schwarz-Erle) und Fraxinus excelsior (Gewöhnlicher Esche) (Alno-Padion, Salicion albae) als prioritärer natürlicher Lebensraumtyp von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
3.  Biber (Castor fiber), Fischotter (Lutra lutra), Großes Mausohr (Myotis myotis), Kammmolch (Triturus cristatus), Rotbauchunke (Bombina bombina), Rapfen (Aspius aspius), Schlammpeitzger (Misgurnus fossilis), Steinbeißer (Cobitis taenia), Bitterling (Rhodeus amarus), Hirschkäfer (Lucanus cervus), Großer Moosjungfer (Leucorrhinia pectoralis), Großer Feuerfalter (Lycaena dispar), Kleine Flussmuschel (Unio crassus) und Bauchige Windelschnecke (Vertigo moulinsiana) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume;
4.  Eremit (Osmoderma eremita) als prioritärer Art im Sinne von § 7 Absatz 2 Nummer 11 des Bundesnaturschutzgesetzes, einschließlich seiner für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

(3) Darüber hinaus ist besonderer Schutzzweck der Zone 1 die Entwicklung natürlicher Verlandungs- und Moorwaldökosysteme.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten jeweils zwischen dem 1.
    August eines jeden Jahres und dem 1.
    März des Folgejahres außerhalb der Zone 1 und außerhalb von Nassbereichen wie Röhrichte, Nasswälder und Nasswiesen zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 14;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit nicht motorisierten Fahrzeugen und Fahrrädern mit Trethilfe außerhalb der Straßen und Wege sowie mit sonstigen Fahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
12.  abseits der Bundeswasserstraße „Spree-Oder-Wasserstraße“, des Hauptlaufs der Drahendorfer Spree, des Streitberger Altarms sowie abseits der in § 2 genannten topografischen Karten eingetragenen Badestellen am Dehmsee und am Sauener See zu baden.
    § 8.10 der Binnenschifffahrtsstraßen-Ordnung bleibt unberührt;
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen auf Gewässern außerhalb der Bundeswasserstraße „Spree-Oder-Wasserstraße“ und des Hauptlaufs der Drahendorfer Spree zu benutzen, wobei auf dem nicht schiffbaren Landesgewässer Drahendorfer Spree nur muskelbetriebene Fahrzeuge zulässig sind und das Anlanden und Einsetzen von motorisierten Wasserfahrzeugen in der Bundeswasserstraße nur an rechtmäßig bestehenden Stegen und Anlegeplätzen zulässig ist;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel aller Art zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  sonstige Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grund​sätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung mit der Maßgabe, dass
    1.  auf Grünland § 4 Absatz 2 Nummer 23 gilt, wobei Störstellen mit Vorkommen von giftigen oder invasiven Arten nach Genehmigung durch die untere Naturschutzbehörde chemisch behandelt werden können;
    2.  auf Grünland § 4 Absatz 2 Nummer 24 gilt, wobei bei Narbenschäden eine umbruchlose Nachsaat mit standortangepassten Grasarten zulässig ist;
    3.  Grünland innerhalb der Zone 2 als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertende Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle, Jauche, flüssige Gärreste und Sekundärrohstoffdünger einzusetzen.
          
        Sekundärrohstoffdünger im Sinne dieser Verordnung sind Abwasser, Fäkalien, Klärschlamm und ähnliche Stoffe aus Siedlungsabfällen und vergleichbare Stoffe aus anderen Quellen, jeweils auch weiterbehandelt und in Mischungen untereinander oder mit Düngemitteln, Wirtschaftsdünger, Bodenhilfsstoffen, Kultursubstraten und Pflanzenhilfsmitteln,
    4.  Grünland innerhalb der Zone 3 als Wiese oder Weide genutzt wird, wobei die erste Nutzung bis zum 15. Juni und eine weitere Nutzung erst wieder nach dem 31.
        August eines jeden Jahres erfolgen darf;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang außerhalb der Zone 1 auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  eine Nutzung nur einzelstammweise bis gruppenweise erfolgt,
    2.  das Befahren des Waldes nur auf Waldwegen und Rückegassen erfolgt, wobei hydromorphe Böden nur bei ausreichender Tragfähigkeit durch Frost sowie Böden mit einem hohen Anteil an feinkörnigem Substrat nur bei ausreichender Tragfähigkeit durch Frost oder in Trockenperioden auf dauerhaft festgelegten Rückegassen befahren werden,
    3.  nur Arten der potenziell natürlichen Vegetation in lebensraumtypischer Zusammensetzung eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
    4.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    5.  mindestens fünf dauerhaft markierte Stämme von lebensraumtypischen Arten je Hektar mit einem Brusthöhendurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum Absterben aus der Nutzung genommen sein müssen,
    6.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 30 Zentimetern Brusthöhendurchmesser in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden; liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimeter am stärksten Ende) verbleibt im Bestand,
    7.  in den in § 3 Absatz 2 aufgeführten Wäldern lebensraumtypische Gehölze in der Reifephase mit einer Deckung von 25 Prozent zu erhalten oder (sofern nicht vorhanden) zu entwickeln sind,
    8.  in Moorwäldern gemäß § 3 Absatz 1 Nummer 1 keine forstwirtschaftlichen Maßnahmen erfolgen,
    9.  § 4 Absatz 2 Nummer 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  innerhalb der Zone 1 (Rietzer See) nur erforderliche Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne eines Monitorings mit Genehmigung der unteren Naturschutzbehörde erfolgen; die Genehmigung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    2.  auf den Flächen außerhalb der Zone 1
        
        aa)
        
        Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters und Bibers weitgehend ausgeschlossen ist,
        
        bb)
        
        die Fischarten Bitterling, Schlammpeitzger und Steinbeißer ganzjährig geschont werden,
        
        cc)
        
        der Fischbesatz nur mit heimischen Arten erfolgt und dabei eine Gefährdung der in § 3 Absatz 2 Nummer 3 genannten Arten ausgeschlossen ist; § 13 der Fischereiordnung des Landes Brandenburg bleibt unberührt,
        
        dd)
        
        Hegepläne einvernehmlich mit der unteren Naturschutzbehörde abzustimmen sind;
        
4.  die rechtmäßige Ausübung der Angelfischerei in der Bundeswasserstraße „Spree-Oder-Wasserstraße“, im Streitberger Altarm, im Hauptlauf der Drahendorfer Spree und in ihrem Altarm nordwestlich Drahendorf sowie im Sauener See mit der Maßgabe, dass
    1.  die Fischarten Bitterling, Schlammpeitzger und Steinbeißer ganzjährig geschont sind,
    2.  das Befahren von Röhrichten und Schwimmblattgesellschaften unzulässig bleibt,
    3.  an Ufern das Betreten von Röhrichten, Rieden und feuchten Hochstaudenfluren unzulässig ist,
    4.  § 4 Absatz 2 Nummer 19 und 20 gilt;
5.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Fallenjagd mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern von den Gewässerufern verboten ist.
        Ausnahmen bedürfen einer Genehmigung der unteren Naturschutzbehörde,
        
        bb)
        
        die Baujagd in einem Abstand von 100 Metern zu Gewässerufern unterbleibt,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
    3.  das Aufstellen transportabler und mobiler Ansitzeinrichtungen;
    
    Im Übrigen bleibt die Anlage von Futterstellen, Kirrungen, Ansaatwildwiesen und Wildäckern innerhalb gesetzlich geschützter Biotope und der in § 3 Absatz 2 Nummer 1 und 2 genannten Lebensraumtypen unzulässig.
    Jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg bleiben unberührt;
6.  das Befahren des Hauptlaufs der Drahendorfer Spree mit Wasserfahrzeugen mit elektrischer Motorkraft mit einer Leistung bis zu einem Kilowatt und einer Wasserverdrängung bis zu 1500 Kilogramm, wobei das Einsetzen und Anlanden nur an rechtmäßig bestehenden Stegen und Anlegeplätzen zulässig ist; Gestattungserfordernisse nach § 43 Absatz 3 des Brandenburgischen Wassergesetzes bleiben unberührt;
7.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie der Bundesautobahn.
    Die untere Naturschutz​behörde ist rechtzeitig mit dem Ziel einer einvernehmlichen Lösung zu beteiligen;
8.  die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
9.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer und die ordnungsgemäße Unterhaltung der Bundeswasser​straßen, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
10.  die ordnungsgemäße Unterhaltung der Hochwasserschutzanlagen nach Anzeige gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes bei der unteren Naturschutzbehörde.
    Ausgenommen von den Verboten des § 4 bleiben weiterhin Maßnahmen des Hochwasserschutzes zur Beseitigung von Abflusshindernissen wie zum Beispiel Stammholz, Äste, Treibgut, wenn dadurch der Schutzzweck nicht gefährdet wird.
    Die Maßnahmen sind der unteren Naturschutzbehörde gemäß § 34 Absatz 6 des Bundes​naturschutzgesetzes schriftlich anzuzeigen;
11.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhal​tungsarbeiten kann durch langfristig gültige Vereinbarungen herstellt werden;
12.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
13.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch nach dem 31.
    Juli eines jeden Jahres, wobei § 4 Absatz 2 Nummer 9 gilt;
14.  das Betreten von Eisflächen auf dem Sauener See, dem Streitberger Altarm und dem Dehmsee;
15.  Anliegerverkehr auf dem Wegegrundstück zur Siedlung Bunter Schütz (Flurstück 63, Gemarkung Neubrück Forst, Flur 7);
16.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Boden​schutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
17.  Schutz-, Pflege- und Entwicklungsmaßnahmen und Wiederherstellungs​maßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
18.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touri​stische Informationen oder Warntafeln dienen;
19.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen;
20.  die Durchführung von Natur- und Umweltbildungsveranstaltungen mit Zustimmung der unteren Naturschutzbehörde sowie die Nutzung und Instandhaltung des Naturerlebnispfades der Stadt Fürstenwalde westlich Berkenbrück.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungs​erfordernissen, die sich aus anderen fachrechtlichen Vorgaben ergeben, unberührt.

#### § 6 Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  das Gewässerbett der Spree soll in geeigneten Bereichen zum Beispiel durch Anbindung von Altarmen renaturiert werden;
2.  zum Erhalt der Fauna und Flora von der Spree und der mit ihr verbundenen Lebensräume sollen beim Betrieb des Drahendorfer Nadelwehs erhebliche Wasserspiegelschwankungen vermieden werden;
3.  die ökologische Durchgängigkeit kleinerer natürlicher Fließgewässer soll wiederhergestellt werden;
4.  die Revitalisierung entwässerter Moore soll unter Berücksichtigung der Nutzungsfähigkeit von artenreichem Grünland angestrebt werden.
    Die Nutzung nasser Moorstandorte soll ohne Entwässerung mit standortangepasster Technik erfolgen;
5.  Mäharbeiten an Gräben sollen, soweit es mit den Anforderungen an eine ordnungsgemäße Gewässerunterhaltung vereinbar ist, in mehrjährigen Abständen und außerhalb der Zeit von Juni bis August eines jeden Jahres erfolgen;
6.  bei der Bewirtschaftung von Waldflächen sollen
    1.  die für die jeweiligen Waldlebensraumtypen charakteristischen Hauptbaumarten, insbesondere Hainbuche, Stiel- und Trauben-Eiche, durch aktive Erhaltungs- und Verjüngungsmaßnahmen gefördert werden,
    2.  Horst- und Höhlenbäume vor Nutzungsbeginn markiert werden,
    3.  Brutbäume holzbewohnender Käferarten, wie Eremit und Veränderlicher Edelscharrkäfer durch Freistellen gezielt gefördert werden sowie Brutstätten des Hirschkäfers vor Schwarzwild geschützt werden,
    4.  nicht gebietsheimische Gehölze entfernt werden,
    5.  Kiefernreinbestände in naturnahe Laubmischwälder überführt werden;
7.  landschaftsprägende Bäume an Gewässern sollen vor Fraß geschützt werden;
8.  eichengeprägte, altbaumreiche lineare Waldbestände und Baumreihen am Rand der Spreeaue sollen für den Biotopverbund sowie als kulturhistorische Landschaftselemente erhalten, entwickelt oder wiederhergestellt werden;
9.  Grünland soll extensiv bewirtschaftet werden, wobei über die Nutzung in der Zone 3 hinaus jährlich eine zehnwöchige Nutzungspause eingehalten werden soll.
    Bei Mahd soll eine Schnitthöhe von 10 Zentimetern nicht unterschritten werden und die Werbung von Mähgut soll so erfolgen, dass reife Samen von Wiesenpflanzen ausfallen können;
10.  an charakteristischen Arten verarmtes Grünland soll aufgewertet werden, zum Beispiel durch Übertragung von Mahdgut von standörtlich vergleich​baren artenreichen Spenderflächen im Spreetal;
11.  aufgelassenes Grünland mit Restvorkommen charakteristischer Pflan​zenarten der Feucht- und Nasswiesen soll wieder einer dauerhaften extensiven Nutzung zugeführt oder durch geeignete periodische Pflegemaßnahmen offen gehalten werden;
12.  über die Vorgaben der Verordnung (EU) Nummer 1143/2014 über die Prävention und das Management der Einbringung und Ausbreitung invasiver gebietsfremder Arten hinaus sollen Staudenknöterich (Fallopia spp.), Goldrute (Solidago canadensis, S.
    gigantea) und Mink (Neovison vison) zurückgedrängt werden;
13.  Ackerland im Spreetal soll extensiv bewirtschaftet oder in Grünland überführt werden.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnatur​schutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutz​ausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzaus​führungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzaus​führungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten, Außerkrafttreten

(1) § 5 Absatz 1 Nummer 1 Buchstabe c tritt am 1.
Januar 2022 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

(2) Mit dem Inkrafttreten nach Absatz 1 Satz 2 tritt die Nummer 11 des Bezirks Frankfurt (Oder) der Anlage der Anordnung Nr.
1 über Naturschutzgebiete vom 30.
März 1961 (GBl.
II Nr.
27 S.
166) außer Kraft.

Potsdam, den 20.
Dezember 2021

Der Minister für Landwirtschaft,  
Umwelt und Klimaschutz

Axel Vogel

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Spreetal zwischen Neubrück und Fürstenwalde“](/br2/sixcms/media.php/68/GVBl_II_104_2021-Anlage%201.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Spreetal zwischen Neubrück und Fürstenwalde“") 1.3 MB

2

[Anlage 2 (zu § 2 Absatz 2) - Karten](/br2/sixcms/media.php/68/GVBl_II_104_2021-Anlage%202.pdf "Anlage 2 (zu § 2 Absatz 2) - Karten") 240.2 KB

3

[Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Spreetal zwischen Neubrück und Fürstenwalde“](/br2/sixcms/media.php/68/GVBl_II_104_2021-Anlage%203.pdf "Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Spreetal zwischen Neubrück und Fürstenwalde“") 258.7 KB