## Verordnung zur Übertragung von Zuständigkeiten des Ministeriums für Wirtschaft und Energie für die Bewilligung, Berechnung und Zahlung von Trennungsgeld, die Berechnung und Zahlung von Reise- und Umzugskosten, den Ersatz von Sachschäden und die Unfallfürsorgeangelegenheiten auf die Zentrale Bezügestelle des Landes Brandenburg (ZBB-Zuständigkeitsübertragungsverordnung MWE - ZBBZÜVMWE)

Auf Grund des § 63 Absatz 3 Satz 2 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S. 26) verordnet der Minister für Wirtschaft und Europaangelegenheiten im Einvernehmen mit dem Minister der Finanzen:

### § 1  
Übertragung von Aufgaben

Die Zuständigkeit des Ministeriums für Wirtschaft und Energie für

1.  die Berechnung und Zahlung von Reisekosten gemäß § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes,
2.  die Berechnung und Zahlung von Umzugskosten gemäß § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes,
3.  die Bewilligung, Berechnung und Zahlung von Trennungsgeld gemäß § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes,
4.  den Ersatz von Sachschäden und die Geltendmachung von übergegangenen Schadensersatzansprüchen gegen Dritte gemäß § 66 Absatz 1 und 3 des Landesbeamtengesetzes,
5.  die Unfallfürsorgeangelegenheiten gemäß Abschnitt 2 Unterabschnitt 3 des Brandenburgischen Beamtenversorgungsgesetzes und
6.  die Zustimmung zu einem Verzicht auf die Rückforderung von Versorgungsbezügen aus Billigkeitsgründen gemäß § 7 Absatz 2 Satz 3 des Brandenburgischen Beamtenversorgungsgesetzes

wird für den Geschäftsbereich des Ministeriums für Wirtschaft und Energie auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.
Die Zentrale Bezügestelle des Landes Brandenburg ist hinsichtlich der Unfallfürsorgeangelegenheiten (Satz 1 Nummer 5) personalaktenführende Stelle im Sinne des § 2 der Beamtenversorgungs-Zuständigkeitsverordnung vom 28.
Januar 1997 (GVBl.
II S.
53) in der jeweils geltenden Fassung.

### § 2  
Vertretung bei Klagen

Im Rahmen der Übertragung der Zuständigkeit nach § 1 wird die Zentrale Bezügestelle des Landes Brandenburg ermächtigt, das Ministerium für Wirtschaft und Energie sowie den gesamten Geschäftsbereich in verwaltungs- und arbeitsgerichtlichen Streitigkeiten zu vertreten.
Dies gilt auch für Anträge in einstweiligen Rechtsschutzverfahren.

### § 3  
Übergangsvorschrift

Für Anträge und Verfahren im Sinne von § 1, die vor dem Inkrafttreten dieser Verordnung eingegangen sind oder eingeleitet wurden und über die noch nicht abschließend entschieden worden ist, verbleibt es bei der bisherigen Zuständigkeit, soweit die Bearbeitung nicht bereits der Zentralen Bezügestelle des Landes Brandenburg übertragen war.
Dies gilt auch für die Vertretung in zu diesem Zeitpunkt bereits anhängigen Rechtsstreitigkeiten.

### § 4  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 11.
November 2011

Der Minister für Wirtschaft  
und Europaangelegenheiten

Ralf Christoffers