## Brandenburgische Verordnung über die Schiedsstelle nach § 133 des Neunten Buches Sozialgesetzbuch (Eingliederungshilfe-Schiedsstellenverordnung - EhSchV)

#### § 1 Bildung der Schiedsstelle

(1) Für das Land Brandenburg wird beim Landesamt für Soziales und Versorgung eine Schiedsstelle nach § 133 Absatz 1 des Neunten Buches Sozialgesetzbuch gebildet.

(2) Die Schiedsstelle besteht aus elf Mitgliedern.
Sie ist mit einem unparteiischen vorsitzenden Mitglied und zehn weiteren Mitgliedern besetzt, davon fünf Vertretungen der Leistungserbringer und fünf Vertretungen der örtlichen Träger und des überörtlichen Trägers der Eingliederungshilfe.

(3) Für jedes Schiedsstellenmitglied wird eine Stellvertretung bestellt.
Die Stellvertretung übt bei Verhinderung des Mitglieds dessen Rechte und Pflichten aus.

(4) Das vorsitzende Mitglied und dessen Stellvertretung dürfen weder haupt- noch nebenberuflich bei einem Leistungserbringer oder einem Träger der Eingliederungshilfe tätig sein.

#### § 2 Bestellung der Mitglieder

(1) Die in § 7 benannte Geschäftsstelle fordert spätestens zwei Monate vor Beginn der neuen Amtsperiode der Schiedsstelle die Vereinigungen der Leistungserbringer und die Träger der Eingliederungshilfe auf, ihre Schiedsstellenmitglieder sowie deren Stellvertretungen zu bestellen.
Gleichzeitig sind die Leistungserbringer und die Träger der Eingliederungshilfe aufzufordern, Personen für das Amt des vorsitzenden Mitgliedes sowie dessen Stellvertretung zu benennen.
Als Schiedsstellenmitglied oder als Stellvertretung soll nur bestellt oder benannt werden, wer sich zur Übernahme des Amtes schriftlich bereiterklärt hat.
Im Falle der vorzeitigen Amtsniederlegung hat die Geschäftsstelle innerhalb von zwei Monaten nach Zugang der Erklärung nach § 4 Absatz 5 die Neubestellungen sicherzustellen.

(2) Als Schiedsstellenmitglieder der Leistungserbringer und deren Stellvertretung bestellen:

1.  die LIGA der Freien Wohlfahrtspflege des Landes Brandenburg vier Mitglieder sowie vier stellvertretende Mitglieder,
2.  die im Land Brandenburg vertretenen Vereinigungen der privatgewerblichen Träger ein Mitglied sowie ein stellvertretendes Mitglied.

(3) Als Schiedsstellenmitglieder der Träger der Eingliederungshilfe und deren Stellvertretungen bestellt:

1.  der Landkreistag Brandenburg zwei Mitglieder und zwei stellvertretende Mitglieder,
2.  der Städte- und Gemeindebund Brandenburg ein Mitglied und ein stellvertretendes Mitglied,
3.  der überörtliche Träger der Eingliederungshilfe im Land Brandenburg zwei Mitglieder und zwei stellvertretende Mitglieder.

(4) Die Benennung und die Bestellung sind der Geschäftsstelle schriftlich unter Beifügung der Bereitschaftserklärung nach § 2 Absatz 1 Satz 3 der Bestellten bekanntzugeben.
Die Geschäftsstelle unterrichtet schriftlich die beteiligten Vereinigungen der Leistungserbringer, die Träger der Eingliederungshilfe sowie die bestellten Schiedsstellenmitglieder.

(5) Werden innerhalb einer Frist von zwei Monaten nach Aufforderung keine Schiedsstellenmitglieder nach Absatz 1, 2 und 3 bestellt, bestimmt die zuständige Landesbehörde diese nach Maßgabe des § 133 Absatz 3 Satz 6 des Neunten Buches Sozialgesetzbuch.

#### § 3 Amtsdauer

(1) Die Amtsdauer der Schiedsstellenmitglieder beträgt vier Jahre (Amtsperiode).
Die erste Amtsperiode beginnt am 1.
Januar 2020.

(2) Das Amt der Schiedsstellenmitglieder endet mit dem Ablauf der Amtsperiode.
Sie führen die Geschäfte bis zu einer Neubestellung weiter.

(3) Scheidet ein Mitglied vor Ablauf der für ihn maßgebenden Amtsdauer aus, so wird das nachfolgende Mitglied für den Rest der Amtsperiode bestellt.
Das Gleiche gilt bei Ausscheiden eines stellvertretenden Mitgliedes.

(4) Eine erneute Bestellung ist möglich.

#### § 4 Abberufung und Amtsniederlegung

(1) Die beteiligten Vereinigungen der Leistungserbringer und die Träger der Eingliederungshilfe können gemeinsam das vorsitzende Mitglied aus wichtigem Grund abberufen.
Kommt eine Einigung nicht zustande, kann jede beteiligte Vereinigung der Leistungserbringer sowie jeder Träger der Eingliederungshilfe bei der nach § 16 zuständigen Rechtsaufsichtsbehörde die Abberufung des vorsitzenden Mitgliedes beantragen.
Die Rechtsaufsichtsbehörde entscheidet über diesen Antrag nach pflichtgemäßem Ermessen.
Wird dem Antrag entsprochen, gilt das vorsitzende Mitglied als abberufen.
Bis zur Bestimmung eines neuen vorsitzenden Mitgliedes führt dessen Stellvertretung die Geschäfte.
Für das stellvertretende vorsitzende Mitglied gelten die Sätze 1 bis 4 und Absatz 4 entsprechend.

(2) Legt das vorsitzende Mitglied sein Amt nieder, übernimmt die Stellvertretung ohne Neubestellung die Geschäfte bis zum Ende der Amtsperiode.

(3) Die übrigen Schiedsstellenmitglieder und deren Stellvertretungen können von der Organisation abberufen werden, die das Mitglied bestellt hat.
Wurde die betroffene Person nach § 133 Absatz 3 Satz 6 des Neunten Buches Sozialgesetzbuch von der zuständigen Landesbehörde bestellt, so wird die Abberufung erst wirksam, wenn für die betroffene Person eine nachfolgende Person bestellt wurde.

(4) Vor einer Entscheidung nach Absatz 1 Satz 3 hat die Rechtsaufsichtsbehörde die beteiligten Vereinigungen der Leistungserbringer sowie die Träger der Eingliederungshilfe anzuhören.
Die Abberufung bedarf der Schriftform.
Sie ist der Geschäftsstelle schriftlich mitzuteilen.

(5) Die Schiedsstellenmitglieder und die stellvertretenden Mitglieder können durch schriftliche oder elektronische Erklärung gegenüber der Geschäftsstelle ihr Amt niederlegen.

(6) Die Geschäftsstelle unterrichtet die beteiligten Vereinigungen der Leistungserbringer und die Träger der Eingliederungshilfe schriftlich oder elektronisch von der Abberufung oder der Niederlegung des Amtes.

#### § 5 Amtsführung und Befangenheit

(1) Die Mitglieder sind verpflichtet, an den Sitzungen der Schiedsstelle teilzunehmen.
Ist ein Mitglied an der Sitzungsteilnahme verhindert, hat es unverzüglich das stellvertretende Mitglied und die Geschäftsstelle zu benachrichtigen.
Dies gilt entsprechend für das stellvertretende Mitglied.
In der Ladung soll auf diese Pflicht hingewiesen werden.

(2) Die Schiedsstellenmitglieder sowie deren Stellvertretung haben vertraulich mit den dem Verfahren zugrundeliegenden Daten der Vertragsparteien umzugehen und sind zur Verschwiegenheit auch nach der Beendigung ihrer Amtsdauer verpflichtet.

(3) In einem Schiedsstellenverfahren darf als Mitglied nicht tätig werden, wer bei der streitgegenständlichen Vertragsverhandlung maßgeblich mitgewirkt hat oder wer durch die Mitwirkung im Schiedsstellenverfahren oder durch die Entscheidung einen unmittelbaren Vorteil oder Nachteil erlangen kann.
Maßgebliche Mitwirkung leistet unter anderem, wer eine Vertragspartei bei den Vertragsverhandlungen vertritt oder vertreten hat.
Satz 1 gilt nicht, wenn die maßgebliche Mitwirkung oder der Vorteil oder Nachteil nur darauf beruhen, dass jemand einer der nach § 2 zu beteiligenden Organisationen angehört und deren Interessen wahrnimmt.

#### § 6 Beteiligung der Interessenvertretungen der Menschen mit Behinderungen

(1) Die Interessenvertretungen der Menschen mit Behinderungen sind an den Verfahren der Schiedsstelle zu beteiligen.
Sie können an den Sitzungen der Schiedsstelle beratend teilnehmen.

(2) Der Landesbehindertenbeirat Brandenburg als maßgebliche Interessenvertretung der Menschen mit Behinderungen im Sinne des § 133 Absatz 5 Nummer 10 des Neunten Buches Sozialgesetzbuch bestimmt für die Amtsperiode der Schiedsstelle zwei Vertretungen sowie zwei Stellvertretungen zur Interessensvertretung und benennt diese gegenüber der Geschäftsstelle.

(3) § 5 Absatz 2 gilt entsprechend.

#### § 7 Geschäftsstelle

(1) Die Geschäfte der Schiedsstelle werden beim Landesamt für Soziales und Versorgung des Landes Brandenburg geführt.

(2) Die Beschäftigten der Geschäftsstelle unterliegen den fachlichen Weisungen des vorsitzenden Mitgliedes der Schiedsstelle.

#### § 8 Antrag

(1) Das Schiedsstellenverfahren ist einzuleiten, wenn eine der Vertragsparteien die Entscheidung der Schiedsstelle schriftlich oder elektronisch beantragt.
Der Antrag und alle weiteren Unterlagen sind mit 14 Abschriften bei der Geschäftsstelle einzureichen.
Im Falle der elektronischen Antragstellung entfallen die Abschriften, jedoch nicht die Einreichung eines schriftlichen Originals.

(2) Der Landesbehindertenbeirat Brandenburg erhält eine Abschrift des Antrages.

(3) Der Antrag muss die Vertragsparteien bezeichnen, den Sachverhalt und das Ergebnis der vorangegangenen Verhandlungen darlegen sowie die Gegenstände aufführen, über die eine Vereinbarung nicht zustande gekommen ist.
Die von der Einrichtung in den Verhandlungen vorgelegten Nachweise und sonstigen Unterlagen sind beizufügen.

(4) Der Antrag kann ohne Einwilligung der anderen Vertragspartei jederzeit zurückgenommen werden.

(5) Das vorsitzende Mitglied leitet den Vertragsparteien eine Ausfertigung des Antrages elektronisch zu.
Es fordert die Vertragsparteien auf, innerhalb einer von ihm zu bestimmenden Frist zu dem Antrag Stellung zu nehmen.
Äußern sich eine oder mehrere Vertragsparteien innerhalb der gesetzten Frist nicht, kann auch ohne die Stellungnahme über den Antrag entschieden werden.

#### § 9 Vorbereitung und Leitung der Sitzung

(1) Das vorsitzende Mitglied legt Ort, Zeit und Gegenstand der Sitzungen der Schiedsstelle fest.

(2) Von dem Termin jeder Sitzung sollen die Schiedsstellenmitglieder drei Wochen zuvor in Kenntnis gesetzt werden.
Die Ladung muss den Mitgliedern spätestens zwei Wochen vor der Sitzung zugegangen sein.
Die Ladung enthält Angaben über Ort und Zeit der Schiedsstellensitzung, die Tagesordnung und die Anträge sowie alle weiteren Unterlagen der Parteien.

(3) Der Landesbehindertenbeirat Brandenburg wird über den Termin der Sitzung in Kenntnis gesetzt.

(4) Die Sitzungen der Schiedsstelle werden von dem vorsitzenden Mitglied vorbereitet und geleitet.

#### § 10 Verhandlung

(1) Die Schiedsstelle entscheidet über den Antrag auf Grund mündlicher Verhandlung.
Dabei ist auf eine gütliche Einigung hinzuwirken.

(2) Zu der mündlichen Verhandlung sind die Vertragsparteien zu laden.
Die Ladungsfrist beträgt mindestens zwei Wochen.
Es kann in Abwesenheit von Vertragsparteien verhandelt und entschieden werden, wenn mit der Ladung darauf hingewiesen wurde.

(3) Die mündliche Verhandlung ist nicht öffentlich.
In der mündlichen Verhandlung können anwesend sein:

1.  mitarbeitende Personen des für Soziales zuständigen Ministeriums,
2.  Beschäftigte der Geschäftsstelle, wenn diese von dem vorsitzenden Mitglied zugelassen wurden,
3.  die Interessenvertretungen der Menschen mit Behinderungen nach § 6,
4.  weitere zuhörende Personen, wenn keine Vertragspartei widerspricht.

Den Anwesenden nach Satz 2 Nummer 1, 2 und 4 steht kein Rede- und Stimmrecht zu.
Die Interessenvertretungen der Menschen mit Behinderung nach § 6 haben Rederecht.

(4) Zeugen und Sachverständige können auf Beschluss der Schiedsstelle zu Verhandlungen hinzugezogen werden.

(5) Eine Aussetzung des Verfahrens ist nur mit Zustimmung der Vertragsparteien zulässig.

(6) Über die mündliche Verhandlung ist eine Niederschrift zu fertigen.
Das vorsitzende Mitglied kann Beschäftigte der Geschäftsstelle zur Fertigung der Niederschrift bestimmen.
Die Niederschrift muss Angaben enthalten über:

1.  den Ort und den Tag der Verhandlung,
2.  die Namen der Verhandlungsleitung, die Namen von den Vertretungen der erschienenen Vertragsparteien, der anwesenden Mitglieder der Schiedsstelle, der Zeugen und Sachverständigen sowie der weiteren anwesenden Personen nach Absatz 3,
3.  den behandelten Verfahrensgegenstand und die gestellten Anträge,
4.  den wesentlichen Inhalt der Aussagen der Zeugen,
5.  den wesentlichen Inhalt der Aussagen der Sachverständigen für den Fall, dass die Aussagen über ein schriftlich vorgelegtes Gutachten hinausgehen.

Die Niederschrift ist von dem vorsitzenden Mitglied der Schiedsstelle und, soweit eine schriftführende Person hinzugezogen worden ist, auch von dieser zu unterzeichnen.
Der Aufnahme in die Verhandlungsniederschrift steht die Aufnahme in ein Schriftstück gleich, das ihr als Anlage beigefügt und als solche bezeichnet ist; auf die Anlage ist in der Verhandlungsniederschrift hinzuweisen.

#### § 11 Beratung und Entscheidung

(1) Die Schiedsstelle ist beschlussfähig, wenn sämtliche Mitglieder form- und fristgerecht geladen sind und neben dem vorsitzenden Mitglied mindestens je drei Schiedsstellenmitglieder der Leistungserbringer und der Träger der Eingliederungshilfe anwesend sind.
Das vorsitzende Mitglied stellt vor Verhandlungsbeginn die Beschlussfähigkeit fest.
Bei fehlender Beschlussfähigkeit ist eine neue Sitzung innerhalb von vier Wochen durchzuführen.
Dabei ist mit der Ladung darauf hinzuweisen, dass die Schiedsstelle im neuen Termin ohne Rücksicht auf die Anzahl der anwesenden Mitglieder beschlussfähig ist.

(2) Die Schiedsstelle berät und entscheidet nicht öffentlich in Abwesenheit der Vertragsparteien.
Stimmenthaltung ist nicht zulässig.

(3) Die Entscheidung ist schriftlich zu erlassen und zu begründen sowie von dem vorsitzenden Mitglied zu unterzeichnen.
Sie ist den Vertragsparteien durch die Geschäftsstelle zuzustellen.

(4) Bei Einvernehmen der Vertragsparteien kann die Schiedsstelle auf Vorschlag des vorsitzenden Mitgliedes ohne mündliche Verhandlung entscheiden.

(5) Erkennt eine Vertragspartei die Forderung in der mündlichen Verhandlung ganz oder zum Teil an, so ist auf Antrag der anderen Vertragspartei dem Anerkenntnis gemäß zu entscheiden.

#### § 12 Entschädigung

(1) Für entstandene Reisekosten erhält das vorsitzende Mitglied oder dessen Stellvertretung eine Entschädigung entsprechend dem Bundesreisekostengesetz vom 26.
Mai 2005 (BGBl.
I S. 1418), das zuletzt durch Artikel 3 des Gesetzes vom 20.
Februar 2013 (BGBl.
I S.
285, 290) geändert worden ist, in der jeweils geltenden Fassung.
Für die baren Auslagen und den Zeitaufwand erhalten sie ferner eine Fallpauschale in Höhe von 400 Euro.
Die Fallpauschale ermäßigt sich bei Antragsrücknahme oder sonstiger Erledigung sowie in Fällen gemäß § 11 Absatz 4 auf 200 Euro.
Eine Wiederaufnahme des Schiedsverfahrens nach gerichtlicher Aufhebung gilt als neuer Antrag.

(2) Für die übrigen Mitglieder tragen die entsendenden Organisationen die Kosten.

(3) Zeugen und Sachverständige, die auf Beschluss der Schiedsstelle hinzugezogen worden sind, erhalten auf Antrag eine Entschädigung entsprechend dem Justizvergütungs- und -entschädigungsgesetz vom 5.
Mai 2004 (BGBI.
I S.
718, 776), das zuletzt durch Artikel 5 Absatz 2 des Gesetzes vom 11.
Oktober 2016 (BGBI.
I S.
2222, 2224) geändert worden ist, in der jeweils geltenden Fassung.

(4) Ansprüche auf Entschädigungen nach den Absätzen 1 und 3 sind bei der Geschäftsstelle binnen vier Wochen, nachdem sie angefallen sind, geltend zu machen.

#### § 13 Gebühren

(1) Für das Verfahren der Schiedsstelle setzt das vorsitzende Mitglied entsprechend der wirtschaftlichen Bedeutung und der Schwierigkeit des Falles eine Gebühr von mindestens 500 Euro und höchstens 5 000 Euro fest.
Bei Antragstellung ist ein Vorschuss auf die festzusetzende Gebühr in Höhe von 125 Euro zu leisten.
Wird der Antrag vor der mündlichen Verhandlung oder der Entscheidung gemäß § 11 Absatz 4 zurückgenommen, ist eine Gebühr von mindestens 125 Euro und höchstens 3 750 Euro zu entrichten.
Wurde vor der Antragsrücknahme nicht mit der sachlichen Bearbeitung durch die Schiedsstelle begonnen, ist abweichend von Satz 3 keine Gebühr zu entrichten.

(2) Die Gebühr des Verfahrens trägt die unterliegende Vertragspartei.
Bei teilweisem Unterliegen sowie im Vergleichsfall teilt das vorsitzende Mitglied die Gebühr anteilmäßig zwischen den Vertragsparteien auf.
Sofern der Vorschuss nach Absatz 1 von der obsiegenden Vertragspartei erbracht wurde, ist ihr dieser zurückzuerstatten; bei teilweisem Obsiegen ist er entsprechend zu verrechnen.

(3) Die Gebühr wird mit der Zustellung gemäß § 11 Absatz 3 fällig.

#### § 14 Kostentragung

(1) Die Kosten der Schiedsstelle nach § 12 Absatz 1 und 3 sowie die sonstigen Kosten der Schiedsstelle (einschließlich der Geschäftsstelle) sind durch die Gebühreneinnahmen nach § 13 Absatz 1 zu decken.

(2) Die nicht durch Gebühreneinnahmen gedeckten Kosten trägt das Land Brandenburg.
Bei nicht kostendeckenden Gebühren erfolgt im Folgejahr eine Anpassung des in § 13 Absatz 1 genannten Gebührenrahmens.

(3) Die Geschäftsstelle legt der Schiedsstelle und nachrichtlich der Rechtsaufsichtsbehörde bis zum 31.
März eines jeden Jahres eine Aufstellung über die Einnahmen und Ausgaben des Vorjahres sowie eine darauf basierende Kostenkalkulation für das laufende Jahr zur Genehmigung vor.

#### § 15 Zuständige Landesbehörde

Das Landesamt für Soziales und Versorgung des Landes Brandenburg ist zuständige Landesbehörde nach § 133 Absatz 3 Satz 6 des Neunten Buches Sozialgesetzbuch.

#### § 16 Rechtsaufsicht

Die Rechtsaufsicht über die Schiedsstelle führt das für Soziales zuständige Ministerium.

#### § 17 Verarbeitung personenbezogener Daten

(1) Die Geschäftsstelle darf personenbezogene Daten verarbeiten, soweit dies zur Erfüllung der in dieser Verordnung zugewiesenen Aufgaben erforderlich ist.

(2) Die personenbezogenen Daten sind zehn Jahre nach Ende des Schiedsverfahrens aufzubewahren.
Danach sind sie zu vernichten und bei elektronischer Speicherung zu löschen.