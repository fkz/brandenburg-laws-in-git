## Verordnung zur Einteilung landwirtschaftlicher Flächen nach dem Grad der Erosionsgefährdung durch Wasser und Wind (Cross-Compliance Erosionseinstufungsverordnung)

Auf Grund des § 6 Absatz 1 der Agrarzahlungen-Verpflichtungenverordnung vom 17. Dezember 2014 (BAnz.
AT 23.12.2014 V1) verordnet die Landesregierung:

#### § 1 Bezeichnung der Gebiete, die den Erosionsgefährdungsklassen zugehören

(1) Maßgeblich für die Festlegung der Erosionsgefährdungsklassen auf landwirtschaftlichen Flächen ist der Feldblock im Sinne des § 3 Absatz 1 Satz 1 Nummer 1 der InVeKoS-Verordnung vom 24. Februar 2015 (BGBl. I S. 166), die durch Artikel 3 der Verordnung vom 10.
Juli 2015 (BAnz.
AT 13.07.2015 V1) geändert worden ist, in der jeweils geltenden Fassung.

(2) Der einer Gefährdungsklasse zugeordnete Feldblock ist das Gebiet im Sinne des § 6 Absatz 1 Satz 4 der Agrarzahlungen-Verpflichtungenverordnung.
Ein Feldblock kann gleichzeitig ein durch Wasser- und Winderosion gefährdetes Gebiet sein.

(3) Ein Feldblock kann nur als Ganzes einer Gefährdungsklasse zugeordnet werden.
Die Gefährdungsklasse eines Feldblocks wird mit den Attributen CC Wasser 1, CC Wasser 2 oder CC Wind im „Feldblockkataster – GIS InVeKoS Land Brandenburg“ bezeichnet.

(4) Feldblöcke, für die eine Erosionsgefährdungsklasse festgestellt ist, sind in der Übersichtskarte der Anlage farblich dargestellt und können zusätzlich im Feldblockkataster – GIS InVeKoS Land Brandenburg unter dem Link: http://luaplims01.brandenburg.de/invekos\_internet/viewer.htm im Internet eingesehen werden.

#### § 2 Methodik der Gefährdungseinstufung

(1) Die Ermittlung und Festlegung der Erosionsgefährdungsklasse Wasser erfolgt nach Maßgabe der Anlage 2 zu § 6 Absatz 1 Satz 1 Nummer 1 der Agrarzahlungen-Verpflichtungenverordnung.

(2) Die Ermittlung und Festlegung der Erosionsgefährdungsklasse Wind erfolgt nach Maßgabe der Anlage 3 zu § 6 Absatz 1 Satz 1 Nummer 2 der Agrarzahlungen-Verpflichtungenverordnung.

#### § 3 Unterrichtung der Antragsteller

Die Unterrichtung der Begünstigten im Sinne des Artikels 92 Satz 1 der Verordnung (EU) Nr. 1306/2013 über die Erosionsgefährdungseinstufung der Feldblöcke erfolgt jährlich zusammen mit dem Sammelantrag nach § 7 der InVeKoS-Verordnung bei der Antragstellung auf Agrarförderung.
Notwendige Änderungen bei der jährlichen Erosionsgefährdungseinstufung werden jeweils mit Ablauf des in § 7 der InVeKoS-Verordnung genannten Termins wirksam.

#### § 4 Hauptwindrichtung

Erosionswirksame Hauptwindrichtung im Sinne des § 6 Absatz 4 der Agrarzahlungen-Verpflichtungenverordnung ist die West-Ost- und die Ost-West-Richtung.

#### § 5 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig treten die §§ 3 bis 5 der Brandenburgischen Verordnung zur Umsetzung der Gemeinsamen Agrarpolitik vom 30. September 2005 (GVBl. II S. 509), die durch die Verordnung vom 22. Juni 2010 (GVBl. II Nr. 34) geändert worden ist, außer Kraft.

Potsdam, den 28.
Oktober 2015

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

### Anlagen

1

[Anlage](/br2/sixcms/media.php/68/GVBl_II_53_2015-Anlage.pdf "Anlage") 811.4 KB