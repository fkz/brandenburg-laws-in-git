## Verordnung zur Bestimmung der zuständigen Behörden auf dem Gebiet des Straßenverkehrsrechts, des Güterkraftverkehrs und nach dem Berufskraftfahrer-Qualifikations-Gesetz (Straßenverkehrsrechts- und Güterkraftverkehrs-Zuständigkeits-Verordnung - StGÜZV)

Auf Grund

*   des § 6 Absatz 2 des Landesorganisationsgesetzes vom 24.
    Mai 2004 (GVBl.
    I S.186), der durch Artikel 2 des Gesetzes vom 10.
    Juli 2014 (GVBl.
    I Nr. 28 S.
    2) geändert worden ist, auch in Verbindung mit
    1.  § 9 Absatz 1 Satz 3 und § 12 Absatz 1 Satz 2 des Landesorganisationsgesetzes, die durch Artikel 2 des Gesetzes vom 10.
        Juli 2014 (GVBl.
        I Nr.
        28 S.
        2) eingefügt worden sind, und in Verbindung mit § 44 Absatz 1 Satz 1 und § 46 Absatz 2 Satz 1 der Straßenverkehrs-Ordnung vom 6.
        März 2013 (BGBl.
        I S.
        367),
    2.  § 9 Absatz 1 Satz 3 und § 12 Absatz 1 Satz 2 des Landesorganisationsgesetzes und in Verbindung mit § 4 Absatz 3 Satz 1 der Ferienreiseverordnung vom 13.
        Mai 1985 (BGBl. I S. 774),
    3.  § 12 Absatz 1 Satz 2 des Landesorganisationsgesetzes und in Verbindung mit § 46 Absatz 1 Satz 1 der Fahrzeug-Zulassungsverordnung vom 3.
        Februar 2011 (BGBl.
        I S.
        139),
    4.  § 12 Absatz 1 Satz 2 des Landesorganisationsgesetzes und in Verbindung mit § 2 Absatz 2 der EG-Fahrzeuggenehmigungsverordnung vom 3.
        Februar 2011 (BGBl.
        I S.
        126),
    5.  § 12 Absatz 1 Satz 2 des Landesorganisationsgesetzes und in Verbindung mit § 57d Absatz 4 und 9 Satz 1, § 68 Absatz 1, § 70 Absatz 1 Nummer 2, Anlage VIII Nummer 4.1 Satz 2 und Nummer 4.3 Satz 1, Anlage VIIIb Nummer 1 und 9.1 Satz 1, Anlage VIIIc Nummer 1.1 Satz 1, Nummer 7.2 Satz 1, Nummer 8.1 Satz 1 und Nummer 8.2 Satz 1, Anlage XVIIa Nummer 7.1 Buchstabe g, Nummer 7.2 Satz 1, Nummer 8.1 Satz 1 und Nummer 8.2 Satz 1, Anlage XVIIIc Nummer 1.1 sowie Anlage XVIIId Nummer 8.2, 9.1 Satz 1 und Nummer 9.2 Satz 1 der Straßenverkehrs-Zulassungs-Ordnung vom 26.
        April 2012 (BGBl.
        I S.
        679),
    6.  § 10 Absatz 2 des Kraftfahrsachverständigengesetzes vom 22.
        Dezember 1971 (BGBl. I S. 2086),
    7.  § 12 Absatz 1 Satz 2 des Landesorganisationsgesetzes und in Verbindung mit § 5 Absatz 1 Satz 3, § 36 Absatz 6 Satz 1, § 42 Absatz 2 Satz 4, § 48 Absatz 4 Nummer 7 Satz 2, § 66 Absatz 1 und 7 Satz 1, § 67 Absatz 1, 3 Satz 4 und Absatz 4 Satz 2, § 68 Absatz 1 Satz 1 und 3 und Absatz 2 Satz 6, § 70 Absatz 1 Satz 1, § 71 Absatz 5 Satz 1, § 71a Absatz 2 Satz 1 und Absatz 7, § 71b Satz 2, § 73 Absatz 1 Satz 1, § 74 Absatz 1 der Fahrerlaubnis-Verordnung vom 13.
        Dezember 2010 (BGBl.
        I S.
        1980), von denen § 66 Absatz 1 und § 70 durch Artikel 1 der Verordnung vom 16.
        April 2014 (BGBl.
        I S.
        348) neu gefasst, § 68 Absatz 1 zuletzt durch die Verordnung vom 2.
        Oktober 2015 (BGBl.
        I S.
        1674) geändert, § 66 Absatz 7 Satz 1 durch Artikel 1 der Verordnung vom 21.
        Dezember 2016 (BGBl. I S. 3083) geändert, § 74 Absatz 1 durch Artikel 1 der Verordnung vom 21.
        Dezember 2016 (BGBl.
        I S.
        3083) neu gefasst und die §§ 71a und 71b durch Artikel 1 der Verordnung vom 14.
        August 2017 (BGBl. I S. 3232) eingefügt worden sind,
    8.  § 12 Absatz 1 Satz 2 des Landesorganisationsgesetzes und in Verbindung mit § 29 Absatz 3 Nummer 2 des Straßenverkehrsgesetzes in der Fassung der Bekanntmachung vom 5. März 2003 (BGBl.
        I S.
        310, 919),
    9.  § 12 Absatz 1 Satz 2 des Landesorganisationsgesetzes und in Verbindung mit § 50 Absatz 1 des Fahrlehrergesetzes vom 30.
        Juni 2017 (BGBl.
        I S.
        2162, 3784),
    10.  § 3 Absatz 7 Satz 1, § 21 Absatz 1 Satz 1 und § 21a Absatz 1 des Güterkraftverkehrsgesetzes vom 22.
        Juni 1998 (BGBl.
        I S.
        1485), von denen § 21a Absatz 1 durch Artikel 1 des Gesetzes vom 22.
        November 2011 (BGBl.
        I S.
        2272) geändert worden ist,
    11.  § 1 Absatz 1 Satz 1 der Verordnung über den grenzüberschreitenden Güterkraftverkehr und den Kabotageverkehr vom 28.
        Dezember 2011 (BGBl.
        2012 I S.
        42),
*   des § 70 Absatz 5 Satz 1 der Straßenverkehrs-Zulassungs-Ordnung,
*   des § 15 des Kraftfahrsachverständigengesetzes,
*   des § 43 Absatz 1 Satz 1 und § 47 Absatz 1 der Fahrzeug-Zulassungsverordnung, von denen § 47 Absatz 1 durch Artikel 1 der Verordnung vom 31.
    Juli 2017 (BGBl.
    I S.
    3090) geändert worden ist,
*   des § 8 Absatz 3 Satz 1 des Berufskraftfahrer-Qualifikations-Gesetzes vom 14. August 2006 (BGBl.
    I S.
    1958) und
*   des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
    Februar 1987 (BGBl.
    I S.
    602)

verordnet die Landesregierung und

auf Grund

*   des § 6 Absatz 1 des Landesorganisationsgesetzes, der durch Artikel 2 des Gesetzes vom 10. Juli 2014 (GVBl.
    I Nr.
    28 S.
    2) geändert worden ist, in Verbindung mit § 10 Absatz 1 Satz 1 des Kraftfahrsachverständigengesetzes und
*   des § 6 Absatz 1 in Verbindung mit § 13 Absatz 3 des Landesorganisationsgesetzes und in Verbindung mit Anlage VIIIc Nummer 1.1, Anlage XVIIa Nummer 1.1 sowie Anlage XVIIId Nummer 1.1 der Straßenverkehrs-Zulassungs-Ordnung und in Verbindung mit § 67 Absatz 4 Satz 5 der Fahrerlaubnis-Verordnung

verordnet die Ministerin für Infrastruktur und Landesplanung:

#### § 1 

(1) Das für Verkehr zuständige Ministerium ist zuständig für:

1.  die Genehmigung von Ausnahmen nach § 46 Absatz 2 der Straßenverkehrs-Ordnung vom 6. März 2013 (BGBl.
    I S.
    367), die zuletzt durch Artikel 1 der Verordnung vom 6. Oktober 2017 (BGBl.
    I S.
    3549) geändert worden ist,
2.  die Anerkennung von Überwachungsorganisationen nach Anlage VIIIb Nummer 1 der Straßenverkehrs-Zulassungs-Ordnung vom 26.
    April 2012 (BGBl.
    I S.
    679), die zuletzt durch Artikel 1 der Verordnung vom 20.
    Oktober 2017 (BGBl.
    I S.
    3723) geändert worden ist, mit Ausnahme der Entscheidung über die Zulassung zur Prüfung und die Beauftragung des Prüfungsausschusses nach Anlage VIIIb Nummer 3.6 in Verbindung mit Nummer 1 der Straßenverkehrs-Zulassungs-Ordnung sowie der Zustimmung zur Betrauung nach Anlage VIIIb Nummer 3.7 und 4.1.3 der Straßenverkehrs-Zulassungs-Ordnung,
3.  die Erteilung des Auftrages zur Errichtung und Unterhaltung einer Technischen Prüfstelle nach § 10 Absatz 1 des Kraftfahrsachverständigengesetzes vom 22. Dezember 1971 (BGBI. I S. 2086), das zuletzt durch Artikel 4 des Gesetzes vom 28.
    November 2016 (BGBI. I S. 2722, 2727) geändert worden ist, sowie
4.  die Genehmigung von Satzungen der Industrie- und Handelskammern über das Prüfungsverfahren nach § 27 Absatz 2 des Berufskraftfahrerqualifikationsgesetzes.

(2) Dem für Verkehr zuständigen Ministerium obliegt die Aufsicht über die Technische Prüfstelle nach den §§ 10 bis 14 des Kraftfahrsachverständigengesetzes, über anerkannte Überwachungsorganisationen nach Anlage VIIIb Nummer 9.1 der Straßenverkehrs-Zulassungs-Ordnung sowie über die Augenoptiker- und Optometristen-Innung des Landes Brandenburg bei der Erfüllung ihrer Aufgabe nach § 6.

#### § 2 

(1) Das Landesamt für Bauen und Verkehr ist

1.  höhere Verwaltungsbehörde im Sinne des § 68 Absatz 1 der Straßenverkehrs-Zulassungs-Ordnung,
2.  Erlaubnisbehörde nach § 3 Absatz 7 Satz 1 des Güterkraftverkehrsgesetzes vom 22. Juni 1998 (BGBl.
    I S.
    1485), das zuletzt durch Artikel 1 des Gesetzes vom 16.
    Mai 2017 (BGBl. I S. 1214) geändert worden ist,
3.  Aufsichtsbehörde im Sinne des § 21a Absatz 1 des Güterkraftverkehrsgesetzes
4.  zuständige Verwaltungsbehörde im Sinne des § 36 Absatz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
    Februar 1987 (BGBl. I S. 602), das zuletzt durch Artikel 5 des Gesetzes vom 27.
    August 2017 (BGBl. I S. 3295, 3297) geändert worden ist, in Verbindung mit § 21 Absatz 1 Satz 1 des Güterkraftverkehrsgesetzes,
5.  zuständige Verwaltungsbehörde im Sinne des § 36 Absatz 1 des Gesetzes über Ordnungswidrigkeiten im Rahmen der Zuständigkeiten nach Absatz 2 Nummer 30 und 31.

(2) Das Landesamt für Bauen und Verkehr ist darüber hinaus zuständig für:

1.  die Genehmigung von Ausnahmen nach § 47 Absatz 1 der Fahrzeug-Zulassungsverordnung vom 3.
    Februar 2011 (BGBl.
    I S.
    139), die zuletzt durch Artikel 4 des Gesetzes vom 29. Juni 2020 (BGBl.
    I S.
    1528) geändert worden ist sowie nach § 70 Absatz 1 Nummer 2 der Straßenverkehrs-Zulassungs-Ordnung, soweit nicht der Zentraldienst der Polizei gemäß § 9 zuständig ist,
2.  die Anordnung von Übermittlungssperren nach § 43 Absatz 1 der Fahrzeug-Zulassungsverordnung,
3.  die Prüfung der Untersuchungsstellen zur Durchführung von Hauptuntersuchungen und Sicherheitsprüfungen sowie die Entgegennahme der Meldung und Anerkennung von Untersuchungsstellen nach Anlage VIII Nummer 4.1 und 4.3 der Straßenverkehrs-Zulassungs-Ordnung,
4.  die Anerkennung von Kraftfahrzeugwerkstätten zur Durchführung von Sicherheitsprüfungen nach Anlage VIIIc Nummer 1.1, die Entgegennahme der Meldung der Schulungsstätten nach Anlage VIIIc Nummer 7.2, die Aufsicht über die Anerkennungsstellen und das Anerkennungsverfahren für Kraftfahrzeugwerkstätten zur Durchführung von Untersuchungen der Abgase nach Anlage VIIIc Nummer 8.1 sowie die Aufsicht über die Schulungen nach Anlage VIIIc Nummer 8.2 der Straßenverkehrs-Zulassungs-Ordnung,
5.  die Anerkennung von Schulungsstätten für Gaseinbauprüfungen nach Anlage XVIIa Nummer 7.1 Buchstabe g, die Entgegennahme der Meldung der Schulungsstätten nach Anlage XVIIa Nummer 7.2, die Aufsicht über die Anerkennungsstellen und das Anerkennungsverfahren nach Anlage XVIIa Nummer 8.1 sowie die Aufsicht über die Schulungen nach Anlage XVIIa Nummer 8.2 der Straßenverkehrs-Zulassungs-Ordnung,
6.  die Anerkennung von Fahrtschreiber- oder Kontrollgeräteherstellern für die Durchführung von Prüfungen allgemein sowie von Fahrzeugherstellern oder Fahrzeugimporteuren zur Durchführung von Einbauprüfungen der Fahrtschreiber und Kontrollgeräte nach Anlage XVIIIc Nummer 1.1 der Straßenverkehrs-Zulassungs-Ordnung,
7.  die Entgegennahme der Meldung der Schulungsstätten nach Anlage XVIIId Nummer 8.2, die Aufsicht über die Anerkennungsstellen und das Anerkennungsverfahren nach Anlage XVIIId Nummer 9.1 sowie die Aufsicht über die Schulungen nach Anlage XVIIId Nummer 9.2 der Straßenverkehrs-Zulassungs-Ordnung,
8.  die Anerkennung der Fahrzeughersteller, der Hersteller von Geschwindigkeitsbegrenzern oder Beauftragten der Hersteller nach § 57d Absatz 4 sowie die Aufsicht über die Inhaber der Anerkennung nach § 57d Absatz 9 der Straßenverkehrs-Zulassungs-Ordnung,
9.  die Erteilung der Zustimmung zur Betrauung von Prüfingenieuren nach Anlage VIIIb Nummer 3.7 und 4.1.3 in Verbindung mit Anlage VIIIb Nummer 1 der Straßenverkehrs-Zulassungs-Ordnung,
10.  die Anerkennung der Sachverständigen und Prüfer für den Kraftfahrzeugverkehr nach den §§ 1 bis 9 des Kraftfahrsachverständigengesetzes,
11.  die Genehmigung von Ausnahmen nach § 17 des Kraftfahrsachverständigengesetzes,
12.  die Bildung des Prüfungsausschusses, die Bestellung seiner Mitglieder sowie die Bestimmung des Vorsitzenden nach § 2 Absatz 1 und 2 der Verordnung zur Durchführung des Kraftfahrsachverständigengesetzes vom 24.
    Mai 1972 (BGBI.
    I S.
    854), die zuletzt durch Artikel 477 der Verordnung vom 31.
    August 2015 (BGBI.
    I S.
    1474, 1544) geändert worden ist, und die Entscheidung über die Zulassung zur Prüfung und die Beauftragung des Prüfungsausschusses nach § 3 der Verordnung zur Durchführung des Kraftfahrsachverständigengesetzes,
13.  die amtliche Anerkennung von Kursleiterinnen und Kursleitern für die Durchführung von besonderen Aufbauseminaren gemäß § 36 Absatz 6 Satz 1 der Fahrerlaubnis-Verordnung vom 13. Dezember 2010 (BGBl.
    I S.
    1980), die zuletzt durch Artikel 4 des Gesetzes vom 16. April 2021 (BGBl.
    I S.
    822) geändert worden ist,
14.  die Entscheidung über die Geeignetheit alternativer Lehr- und Lernmethoden für die verkehrspädagogische Teilmaßnahme des Fahreignungsseminars nach § 42 Absatz 2 Satz 4 der Fahrerlaubnis-Verordnung,
15.  die amtliche Anerkennung von Trägern von Begutachtungsstellen für Fahreignung und ihre Begutachtungsstellen nach § 66 Absatz 1 sowie die Anordnung einer Begutachtung aus besonderem Anlass nach § 66 Absatz 7 Satz 1 der Fahrerlaubnis-Verordnung,
16.  die Anerkennung von Trägern von Kursen zur Wiederherstellung der Kraftfahreignung nach § 70 Absatz 1 Satz 1 der Fahrerlaubnis-Verordnung,
17.  die Rücknahme und der Widerruf der Anerkennung von verkehrspsychologischen Beratern nach § 71 Absatz 5 Satz 1, sowie die Aufsicht über die verkehrspsychologischen Berater nach § 71 Absatz 5 Satz 2 der Fahrerlaubnis-Verordnung,
18.  die Anerkennung und Überwachung von Trägern von unabhängigen Stellen für die Bestätigung der Eignung von eingesetzten psychologischen Testverfahren und -geräten nach § 71a Absatz 2 Satz 1 in Verbindung mit § 71a Absatz 7 der Fahrerlaubnis-Verordnung,
19.  die Anerkennung und Überwachung von Trägern von unabhängigen Stellen für die Bestätigung der Eignung von Kursen zur Wiederherstellung der Kraftfahreignung nach § 71b Satz 2 der Fahrerlaubnis-Verordnung,
20.  die Erteilung von Ausnahmen nach § 74 Absatz 1 der Fahrerlaubnis-Verordnung von den Bestimmungen des § 36 Absatz 6 sowie der §§ 66, 70, 71a und 71b der Fahrerlaubnis-Verordnung,
21.  die Errichtung des Prüfungsausschusses, die Berufung seiner Mitglieder und die Bestimmung des vorsitzenden Mitglieds nach den §§ 1 und 3 Absatz 1 der Fahrlehrer-Prüfungsverordnung vom 2. Januar 2018 (BGBl.
    I S.
    2, 42), die zuletzt durch Artikel 3 der Verordnung vom 2. Oktober 2019 (BGBl.
    I S.
    1416) geändert worden ist,
22.  die Anerkennung von Berufsverbänden der Fahrlehrerinnen und Fahrlehrer nach § 16 Absatz 1 Satz 2 Nummer 2 des Fahrlehrergesetzes, die amtliche Anerkennung von Fahrlehrerausbildungsstätten nach § 36 Absatz 1 des Fahrlehrergesetzes, von Trägern von Lehrgängen nach § 45 Absatz 3 Satz 2 und § 53 Absatz 10 des Fahrlehrergesetzes, von Bewerberinnen und Bewerbern für die Durchführung von Einweisungslehrgängen nach § 47 Absatz 1 Satz 1 des Fahrlehrergesetzes, von Trägern von Einweisungsseminaren nach § 48 Satz 1 des Fahrlehrergesetzes, deren Überwachung nach § 51 Absatz 1 Satz 1 des Fahrlehrergesetzes, die Erteilung von Ausnahmen nach § 54 Absatz 1 bis 3 des Fahrlehrergesetzes sowie die Genehmigung eines Qualitätssicherungssystems gemäß § 51 Absatz 7 Satz 1 des Fahrlehrergesetzes,
23.  die Genehmigung des Praktikumsplans für Fahrlehreranwärterinnen und Fahrlehreranwärter nach § 3 Absatz 1 der Fahrlehrer-Ausbildungsverordnung vom 2.
    Januar 2018 (BGBl. I S. 2, 15), die zuletzt durch Artikel 2 der Verordnung vom 2.
    Oktober 2019 (BGBl. I S. 1416) geändert worden ist,
24.  die Genehmigung des Ausbildungsplans für das Einweisungsseminar für Ausbildungsfahrlehrer nach § 4 der Fahrlehrer-Ausbildungsverordnung,
25.  die Genehmigung des Rahmenlehrplans über die neuntägige Basisausbildung nach § 15 Absatz 2 Satz 2 der Durchführungsverordnung zum Fahrlehrergesetz vom 2. Januar 2018 (BGBl. I S. 2), die zuletzt durch Artikel 1 der Verordnung vom 2.
    Oktober 2019 (BGBl. I S. 1416) geändert worden ist,
26.  die Genehmigung des Rahmenplans über die einschlägige Fortbildung nach § 15 Absatz 3 Satz 2 der Durchführungsverordnung zum Fahrlehrergesetz,
27.  die Erteilung der Seminarerlaubnis Verkehrspsychologie gemäß § 4a Absatz 3 Satz 2 des Straßenverkehrsgesetzes sowie deren Rücknahme und dem Absehen von der Rücknahme nach § 4a Absatz 5 Satz 1 und 2 des Straßenverkehrsgesetzes und die Überwachung nach § 4a Absatz 8 Satz 1 des Straßenverkehrsgesetzes,
28.  die Anerkennung eines Qualitätssicherungssystems für die verkehrspsychologische Teilmaßnahme des Fahreignungsseminars gemäß § 4a Absatz 8 Satz 6 zweiter Halbsatz des Straßenverkehrsgesetzes,
29.  die Erteilung von Bescheinigungen nach § 4 Nummer 21 Buchstabe a) Doppelbuchstabe bb) des Umsatzsteuergesetzes in der Fassung der Bekanntmachung vom 21.
    Februar 2005 (BGBl. I S. 386), das zuletzt durch Artikel 4 des Gesetzes vom 3.
    Juni 2021 (BGBl. I S. 1498) geändert worden ist, für den Bereich des Straßenverkehrs,
30.  die Anerkennung von Ausbildungsstätten für die beschleunigte Grundqualifikation und die Weiterbildung nach § 9 Absatz 1 des Berufskraftfahrerqualifikationsgesetzes, den Widerruf der Anerkennung nach § 10 Absatz 1 Satz 1 und Absatz 2 des Berufskraftfahrerqualifikationsgesetzes und die Untersagung nach § 10 Absatz 4 des Berufskraftfahrerqualifikationsgesetzes,
31.  die Überwachung der Tätigkeit der Ausbildungsstätten für die beschleunigte Grundqualifikation und die Weiterbildung nach § 11 Absatz 1 Satz 1 des Berufskraftfahrerqualifikationsgesetzes sowie,
32.  für die Datenübermittlung nach § 18 Absatz 3 des Berufskraftfahrerqualifikationsgesetzes.

#### § 3 

(1) Der Landesbetrieb Straßenwesen ist anzuhörende Behörde nach § 70 Absatz 2 der Straßenverkehrs-Zulassungs-Ordnung.

(2) Der Landesbetrieb Straßenwesen ist für die Genehmigung von Großraum- und Schwertransporten Straßenverkehrsbehörde im Sinne des § 44 Absatz 1 Satz 1 der Straßenverkehrs-Ordnung.
Er ist insoweit zuständig für

1.  die Erteilung von Erlaubnissen nach § 29 Absatz 3 der Straßenverkehrs-Ordnung, die Erteilung von Ausnahmen nach § 46 Absatz 1 Nummer 5 der Straßenverkehrs-Ordnung und die Erteilung von mit den vorgenannten Verfahren zusammenhängenden Ausnahmen nach § 46 Absatz 1 Nummer 2 der Straßenverkehrs-Ordnung für Kraftfahrstraßen sowie
2.  die Erteilung von Ausnahmegenehmigungen nach § 46 Absatz 1 Nummer 7 der Straßenverkehrs-Ordnung und nach § 4 der Ferienreiseverordnung für erlaubnis- oder genehmigungspflichtige Fahrten nach Nummer 1 und
3.  die Anordnung von Verkehrszeichen und Verkehrseinrichtungen nach § 45 Absatz 1 der Straßenverkehrs-Ordnung, soweit diese durch die Regelpläne vom 9.
    Oktober 2015 des Bundesministeriums für Verkehr und digitale Infrastruktur im Verkehrsblatt (VkBl.
    2015 S. 686) oder des für den Straßenverkehr zuständigen Ministeriums des Landes im Amtsblatt für Brandenburg im Zusammenhang mit der Durchführung von Großraum- und Schwertransporten zur Visualisierung der Verkehrszeichenanordnungen durch private Verwaltungshelfer festgelegt worden sind, soweit nicht das Fernstraßen-Bundesamt nach § 44a Absatz 1 Satz 1 und Satz 5 in Verbindung mit § 45 Absatz 11 Satz 1 der Straßenverkehrs-Ordnung zuständig ist.

(3) Der Landesbetrieb Straßenwesen ist für den Bereich der Bundesautobahnen Straßenverkehrsbehörde im Sinne des § 44 Absatz 1 Satz 1 Straßenverkehrs-Ordnung, soweit nicht das Fernstraßen-Bundesamt gemäß § 44a Absatz 1 Satz 1, Satz 3 und Satz 5 in Verbindung mit § 45 Absatz 11 Satz 1 der Straßenverkehrs-Ordnung zuständig ist.
Er ist insoweit zuständig für

1.  die Erteilung von Erlaubnissen nach § 29 Absatz 2 und § 30 Absatz 2 der Straßenverkehrs-Ordnung und
2.  die Anordnungen nach § 45 der Straßenverkehrs-Ordnung, soweit nicht das Fernstraßen-Bundesamt gemäß § 44a Absatz 1 Satz 1 und Satz 5 in Verbindung mit § 45 Absatz 11 Satz 1 der Straßenverkehrs-Ordnung zuständig ist, sowie die Erteilung von Ausnahmegenehmigungen nach § 46 Absatz 1 Nummer 2, 4c, 8, 9 und 11 der Straßenverkehrs-Ordnung, soweit nicht das Fernstraßen-Bundesamt nach § 46 Absatz 2a der Straßenverkehrs-Ordnung zuständig ist.

#### § 4 

(1) Die Landkreise und kreisfreien Städte sind zuständige Behörden nach § 2 Absatz 1, §§ 2a, 4 und 4a des Straßenverkehrsgesetzes, Straßenverkehrsbehörden im Sinne des § 44 Absatz 1 Satz 1 der Straßenverkehrs-Ordnung, untere Verwaltungsbehörden im Sinne des § 46 Absatz 1 Satz 1 der Fahrzeug-Zulassungsverordnung, des § 68 Absatz 1 der Straßenverkehrs-Zulassungs-Ordnung und des § 73 Absatz 1 Satz 1 der Fahrerlaubnis-Verordnung sowie Genehmigungsbehörde für Einzelgenehmigungen nach § 2 Absatz 2 der EG-Fahrzeuggenehmigungsverordnung, soweit nicht in dieser Verordnung etwas anderes bestimmt ist.

(2) Die Landkreise und kreisfreien Städte sind zuständige Verwaltungsbehörden im Sinne des § 36 Absatz 1 des Gesetzes über Ordnungswidrigkeiten im Rahmen der Zuständigkeiten für die Ausführungen des Berufskraftfahrer-Qualifikations-Gesetzes und darauf beruhender Rechtsverordnungen einschließlich der sich daraus ergebenden Aufgaben der Überwachung, soweit nicht das Landesamt für Bauen und Verkehr nach § 2 Absatz 2 Nummer 30 und 31 sowie die Industrie- und Handelskammer zuständig sind.

(3) Die Landkreise und kreisfreien Städte sind darüber hinaus zuständig für:

1.  die Erteilung von Erlaubnissen nach § 29 Absatz 2 und § 30 Absatz 2 der Straßenverkehrs-Ordnung.
    Geht die Veranstaltung über den Bezirk einer Straßenverkehrsbehörde hinaus, so ist nach § 44 Absatz 3 Satz 3 der Straßenverkehrs-Ordnung die Straßenverkehrsbehörde zuständig, in deren Bezirk die Veranstaltung beginnt.
    Bei länderübergreifenden Veranstaltungen gelten die Sätze 1 und 2 entsprechend für die zu treffende Entscheidung und die zu erstellende Stellungnahme,
2.  die Maßnahmen nach § 45 der Straßenverkehrs-Ordnung, soweit nicht gemäß § 3 der Landesbetrieb Straßenwesen zuständig ist,
3.  die Erteilung von Ausnahmegenehmigungen nach § 46 Absatz 1 Nummer 1 bis 4c und 5a bis 12 der Straßenverkehrs-Ordnung, soweit nicht gemäß § 3 der Landesbetrieb Straßenwesen zuständig ist,
4.  die Anordnung der Vorladung zum Verkehrsunterricht gemäß § 48 der Straßenverkehrs-Ordnung,
5.  die Erteilung von Ausnahmegenehmigungen nach § 4 Absatz 1 und 3 der Ferienreiseverordnung,
6.  die Genehmigung von Ausnahmen nach § 74 Absatz 1 der Fahrerlaubnis-Verordnung, soweit nicht das Landesamt für Bauen und Verkehr nach § 2 Absatz 2 Nummer 20 zuständig ist,
7.  die Anordnung der Tilgung von Eintragungen nach § 29 Absatz 3 Nummer 2 des Straßenverkehrsgesetzes,
8.  die Bestimmung der Stellen zur Durchführung der Ortskundeprüfung nach § 48 Absatz 4 Nummer 7 Satz 2 der Fahrerlaubnis-Verordnung,
9.  die Anerkennung als Sehteststelle nach § 67 Absatz 1 der Fahrerlaubnis-Verordnung und die Aufsicht über die Inhaber der Anerkennung nach § 67 Absatz 3 Satz 4 der Fahrerlaubnis-Verordnung,
10.  die Anerkennung von Stellen für die Schulung in Erster Hilfe nach § 68 Absatz 1 Satz 1 der Fahrerlaubnis-Verordnung, die Untersagung der Aus- und Fortbildungen nach § 68 Absatz 1 Satz 3 der Fahrerlaubnis-Verordnung und die Aufsicht über die Inhaber der Anerkennung nach § 68 Absatz 2 Satz 6 der Fahrerlaubnis-Verordnung sowie
11.  die Ausführungen des Fahrlehrergesetzes und der auf dem Fahrlehrergesetz beruhenden Rechtsverordnungen einschließlich der sich daraus ergebenden Aufgaben der Überwachung, soweit nicht das Landesamt für Bauen und Verkehr nach § 2 Absatz 2 Nummer 21 bis 26 zuständig ist,
12.  die Erteilung des Fahrerqualifizierungsnachweises über den Erwerb der Grundqualifikation oder der Weiterbildung nach § 8 Absatz 1 Satz 1 der Berufskraftfahrerqualifikationsverordnung vom 9. Dezember 2020 (BGBl.
    I S.
    2905),
13.  die Datenübermittlung nach § 15 sowie § 18 Absatz 1 und 2 Berufskraftfahrerqualifikations-gesetz, sowie
14.  die Anrechnung anderer abgeschlossener spezieller Aus- oder Weiterbildungsmaßnahmen gemäß § 4 Absatz 4 Satz 1 der Berufskraftfahrerqualifikationsverordnung als Teil des Unterrichts bei der Berufskraftfahrerweiterbildung.

(4) Die Großen kreisangehörigen Städte Eberswalde, Eisenhüttenstadt und Schwedt sind Straßenverkehrsbehörde im Sinne des § 44 Absatz 1 Satz 1 der Straßenverkehrs-Ordnung für die in Absatz 3 Nummer 1 bis 5 aufgeführten Aufgaben.

(5) Die Aufgaben nach den Absätzen 1 bis 4 sind Pflichtaufgaben zur Erfüllung nach Weisung.
Die Landrätin oder der Landrat als allgemeine untere Landesbehörde ist Sonderaufsichtsbehörde über die Großen kreisangehörigen Städte als untere Straßenverkehrsbehörden.
Das für Verkehr zuständige Ministerium ist Sonderaufsichtsbehörde über die Landkreise und kreisfreien Städte und oberste Sonderaufsichtsbehörde über die Großen kreisangehörigen Städte als untere Straßenverkehrsbehörden.
Der Umfang des Weisungsrechts ergibt sich aus § 121 der Kommunalverfassung des Landes Brandenburg.

(6) Das Land erstattet den zuständigen Behörden die aus der Zuweisung neuer Zuständigkeiten in Absatz 3 Nummer 12, 13 und 14 sowie Absatz 5 resultierenden notwendigen Mehrkosten einschließlich der Personal- und Sachkosten, soweit dieser finanzielle Aufwand nicht durch Gebühren oder Einnahmen aus der Aufgabenwahrnehmung gedeckt wird.
Der die Einnahmen übersteigende, nachgewiesene finanzielle Aufwand wird auf Antrag mit entsprechendem Nachweis den zuständigen Behörden von dem für Verkehr zuständigen Ministerium erstattet.

#### § 4a 

(1) Abweichend von § 4 Absatz 1 sind die Städte Guben, Prenzlau, Teltow und Werder auf ihren Antrag hin Straßenverkehrsbehörde im Sinne des § 44 Absatz 1 Satz 1 der Straßenverkehrs-Ordnung für ihr Gebiet für die nachfolgend bestimmten Aufgaben:

1.  § 44 Absatz 3 Satz 1 in Verbindung mit § 29 Absatz 2 und § 30 Absatz 2 der Straßenverkehrs-Ordnung,
2.  § 45 der Straßenverkehrs-Ordnung,
3.  § 46 Absatz 1 Satz 1 Nummer 1, 3 bis 4b, 5a und 5b, 6 sowie 8 bis 12 der Straßenverkehrs-Ordnung.

(2) Abweichend von § 4 Absatz 1 sind die Städte Wittenberge, Kyritz, Finsterwalde und Luckau sowie die Gemeinde Kleinmachnow und das Amt Schlieben auf ihren Antrag hin Straßenverkehrsbehörde im Sinne des § 44 Absatz 1 Satz 1 der Straßenverkehrs-Ordnung für ihr oder sein Gebiet nach folgenden Vorschriften der Straßenverkehrs-Ordnung:

1.  § 44 Absatz 3 Satz 1 in Verbindung mit § 29 Absatz 2 der Straßenverkehrs-Ordnung,
2.  § 45 der Straßenverkehrs-Ordnung, soweit es sich um straßenverkehrsrechtliche Anordnungen
    
    1.  über das Halten und Parken,
    2.  im Zusammenhang mit Veranstaltungen nach § 29 Absatz 2 der Straßenverkehrs-Ordnung,
    3.  im Zusammenhang mit Arbeiten im Straßenraum,
    4.  die Verhütung außerordentlicher Schäden an Gemeindestraßen
    
    handelt.
    Die Buchstaben b) und c) gelten nicht, wenn Anordnungen für das Gebiet mehrerer Gemeinden zu erteilen sind;
3.  § 46 Absatz 1 Satz 1 Nummer 1, 3 bis 4b, 5a und 5b, 6, 8 bis 10 sowie 12 der Straßenverkehrs-Ordnung;
4.  § 46 Absatz 1 Satz 1 Nummer 11 der Straßenverkehrs-Ordnung, soweit es sich um Ausnahmen von Verboten oder Beschränkungen des Haltens und Parkens sowie zum Befahren von Fußgängerbereichen und Fahrradstraßen handelt.

(3) Die Aufgaben nach den Absätzen 1 und 2 sind Pflichtaufgaben zur Erfüllung nach Weisung.
Die Landrätin oder der Landrat als allgemeine untere Landesbehörde ist Sonderaufsichtsbehörde über die in Absatz 1 und Absatz 2 genannten Städte und Gemeinden.
Das für Verkehr zuständige Ministerium ist oberste Sonderaufsichtsbehörde über die in Absatz 1 und Absatz 2 genannten Städte und Gemeinden.
Der Umfang des Weisungsrechts ergibt sich aus § 121 der Kommunalverfassung des Landes Brandenburg.

(4) Das Land erstattet den zuständigen Behörden die aus der Zuweisung neuer Zuständigkeiten in Absatz 3 resultierenden notwendigen Mehrkosten einschließlich der Personal- und Sachkosten, soweit dieser finanzielle Aufwand nicht durch Gebühren oder Einnahmen aus der Aufgabenwahrnehmung gedeckt wird.
Der die Einnahmen übersteigende, nachgewiesene finanzielle Aufwand wird auf Antrag mit entsprechendem Nachweis den zuständigen Behörden von dem für Verkehr zuständigen Ministerium erstattet.

#### § 5 

(1) Abweichend von § 4 Absatz 1 können die Landkreise die ihnen als untere Verwaltungsbehörde im Sinne des § 46 Absatz 1 Satz 1 der Fahrzeug-Zulassungsverordnung und des § 68 Absatz 1 der Straßenverkehrs-Zulassungs-Ordnung obliegenden Aufgaben auf Antrag durch eine öffentlich-rechtliche Vereinbarung teilweise oder vollständig auf amtsfreie Gemeinden oder Ämter übertragen, wenn eine effektive Aufgabenwahrnehmung und die erforderliche personelle und sachliche Ausstattung gewährleistet sind.

(2) Neben den Landkreisen und kreisfreien Städten als unteren Verwaltungsbehörden im Sinne des § 73 Absatz 1 Satz 1 der Fahrerlaubnis-Verordnung sind die Ämter und amtsfreien Gemeinden für die Entgegennahme von Anträgen gemäß § 21 Absatz 1 Satz 1 der Fahrerlaubnis-Verordnung zuständig.

(3) Die Ämter und amtsfreien Gemeinden nehmen die Aufgaben nach den Absätzen 1 und 2 als Pflichtaufgaben zur Erfüllung nach Weisung wahr.
Das für Verkehr zuständige Ministerium ist oberste Sonderaufsichtsbehörde.
Der Umfang des Weisungsrechtes ergibt sich aus § 121 der Kommunalverfassung des Landes Brandenburg.

#### § 6 

Die Augenoptiker- und Optometristen-Innung des Landes Brandenburg ist für die nachträgliche Erteilung von Auflagen, den Widerruf der Anerkennung im Einzelfall und die Aufsicht über die nach § 67 Absatz 4 Satz 1 der Fahrerlaubnis-Verordnung als amtlich anerkannte Sehteststellen geltenden Augenoptikerbetriebe nach § 67 Absatz 4 Satz 5 der Fahrerlaubnis-Verordnung zuständig.

#### § 7 

Die für das Land Brandenburg zuständige Technische Prüfstelle für den Kraftfahrzeugverkehr ist prüfende Stelle nach § 5 Absatz 1 Satz 3 der Fahrerlaubnis-Verordnung.

#### § 8 

Die örtlich und fachlich zuständigen Kraftfahrzeuginnungen sind zuständig für:

1.  die Anerkennung von Kraftfahrzeugwerkstätten zur Durchführung von Untersuchungen der Abgase oder Untersuchungen der Abgase an Krafträdern nach Anlage VIIIc Nummer 1.1 der Straßenverkehrs-Zulassungs-Ordnung,
2.  die Anerkennung von Kraftfahrzeugwerkstätten zur Durchführung von Gassystemeinbauprüfungen oder wiederkehrenden Gasanlagenprüfungen (GWP) und sonstigen Gasanlagenprüfungen im Sinne des § 41a Absatz 6 der Straßenverkehrs-Zulassungs-Ordnung nach Anlage XVIIa Nummer 1.1 der Straßenverkehrs-Zulassungs-Ordnung sowie
3.  die Anerkennung von Kraftfahrzeugwerkstätten zur Durchführung von Prüfungen der Fahrtschreiber und Kontrollgeräte nach Anlage XVIIId Nummer 1.1 der Straßenverkehrs-Zulassungs-Ordnung.

#### § 9 

Der Zentraldienst der Polizei des Landes Brandenburg ist für die Genehmigung von Ausnahmen nach § 70 Absatz 1 Nummer 2 der Straßenverkehrs-Zulassungs-Ordnung für den Dienstbereich der Polizei zuständig.

#### § 10 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig treten die Straßenverkehrsrechts-Zuständigkeits-Verordnung vom 11.
August 2009 (GVBl.
II S.
523), die zuletzt durch die Verordnung vom 9.
Mai 2016 (GVBl.
II Nr.
22) geändert worden ist, und die Güterkraftverkehrs- und Berufskraftfahrer-Qualifikations-Zuständigkeitsverordnung vom 10. Juli 2008 (GVBl.
II S.
245) außer Kraft.

Potsdam, den 9.
November 2018

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider