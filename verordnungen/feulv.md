## Verordnung über die Laufbahnen der Beamten des feuerwehrtechnischen Dienstes im Land Brandenburg  (Feuerwehrlaufbahnverordnung - FeuLV)

Auf Grund des § 117 Satz 2 und des § 111 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S. 26) und des § 49 Absatz 2 Nummer 1 des Brandenburgischen Brand- und Katastrophenschutzgesetzes vom 24.
Mai 2004 (GVBl.
I S. 197) verordnet der Minister des Innern im Einvernehmen mit dem Minister der Finanzen:

### § 1  
Geltungsbereich

(1) Diese Verordnung gilt für die Beamten des feuerwehrtechnischen Dienstes:

1.  der öffentlichen Feuerwehren,
2.  der Regionalleitstellen,
3.  der Sonderaufsichtsbehörden nach § 22 des Brandenburgischen Brand- und Katastrophenschutzgesetzes,
4.  der Landesschule und Technischen Einrichtung für Brand- und Katastrophenschutz und
5.  der Brandschutzdienststellen nach § 32 des Brandenburgischen Brand- und Katastrophenschutzgesetzes.

(2) Soweit nachfolgend nichts anderes geregelt ist, gilt die Laufbahnverordnung.

(3) Funktions-, Status- und andere Bezeichnungen werden in weiblicher und männlicher Form geführt.
Die in dieser Verordnung verwendeten Funktions-, Status- und anderen personenbezogenen Bezeichnungen gelten für Frauen und Männer.

### § 2  
Gestaltung der Laufbahnen

(1) Der feuerwehrtechnische Dienst gliedert sich in die Laufbahnen des mittleren, des gehobenen und des höheren Dienstes.

(2) Für einen höherbewerteten Dienstposten hat der Beamte seine Eignung in einer Erprobungszeit nachzuweisen.
Für die Eignung ist bei entsprechenden Ämtern insbesondere zu berücksichtigen, ob der Beamte in der Lage ist, Führungsaufgaben wahrzunehmen.
Für die Eignung von Beamten des mittleren feuerwehrtechnischen Dienstes für die Funktion eines Gruppenführers ist als Nachweis im Sinne von Satz 2 der erfolgreiche Abschluss eines an einer Landesfeuerwehrschule absolvierten Führungslehrgangs für Gruppenführer anzusehen.

(3) Ein Aufstieg für besondere Verwendungen ist ausgeschlossen.

### § 3  
Einstellung in den Vorbereitungsdienst des mittleren Dienstes

(1) In den Vorbereitungsdienst kann eingestellt werden, wer:

1.  mindestens die Berufsbildungsreife, einen Hauptschulabschluss oder einen als gleichwertig anerkannten Bildungsabschluss nachweist,
2.  eine Gesellenprüfung in einem für den feuerwehrtechnischen Dienst geeigneten Handwerk abgelegt hat oder eine entsprechende förderliche abgeschlossene Berufsausbildung nachweist,
3.  nach amtsärztlichem Gutachten für den Dienst in der Feuerwehr geeignet ist,
4.  eine Fahrerlaubnis der Klasse B besitzt,
5.  erfolgreich an einem Auswahlverfahren teilgenommen hat, das hinsichtlich der körperlichen Eignung Sportübungen zu enthalten hat, und
6.  höchstens 30 Jahre alt ist.

(2) Das Ministerium des Innern kann im Einvernehmen mit dem Ministerium der Finanzen Ausnahmen von Absatz 1 Nummer 6 zulassen.

### § 4  
Vorbereitungsdienst im mittleren Dienst

(1) Die ausgewählten Bewerber werden unter Berufung in das Beamtenverhältnis auf Widerruf in den Vorbereitungsdienst eingestellt.
Sie führen während des Vorbereitungsdienstes die Dienstbezeichnung „Brandmeisteranwärter“.

(2) Der Vorbereitungsdienst dauert ein Jahr.
Das Ministerium des Innern kann für Spitzensportler den Vorbereitungsdienst verlängern.

(3) Der Vorbereitungsdienst umfasst eine mindestens sechsmonatige Feuerwehrmann-Grundausbildung und die sich anschließende berufspraktische Ausbildung.
Der Erholungsurlaub ist nur während der berufspraktischen Ausbildung zu gewähren.
Der Vorbereitungsdienst schließt mit der Laufbahnprüfung ab.

### § 5  
Probezeit

(1) Während der Probezeit hat sich der Beamte in den Aufgaben des mittleren feuerwehrtechnischen Dienstes zu bewähren.
Ihm soll Gelegenheit gegeben werden, sich Spezialkenntnisse anzueignen.
Insbesondere hat der Beamte den erfolgreichen Abschluss der Rettungssanitäterausbildung und am Ende der Probezeit den Erwerb der Fahrerlaubnis der Klasse C nachzuweisen.
Der Nachweis derartiger Spezialkenntnisse ist bei der Beurteilung, ob der Beamte sich hinsichtlich Eignung und Befähigung bewährt hat, zu berücksichtigen.

(2) Auf die Probezeit kann eine ehrenamtliche Tätigkeit in einer Freiwilligen Feuerwehr oder eine nebenberufliche Tätigkeit in einer Werkfeuerwehr von insgesamt mindestens zwei Jahren im Umfang von bis zu drei Monaten angerechnet werden.

(3) Die Mindestprobezeit beträgt ein Jahr.

### § 6  
Einstellung in den Vorbereitungsdienst des gehobenen Dienstes

(1) In den Vorbereitungsdienst kann eingestellt werden, wer:

1.  ein mit einem Bachelorgrad abgeschlossenes Hochschulstudium oder einen gleichwertigen Abschluss in einer für den feuerwehrtechnischen Dienst geeigneten Fachrichtung erworben hat,
2.  nach amtsärztlichem Gutachten für den Dienst in der Feuerwehr geeignet ist,
3.  erfolgreich an einem Auswahlverfahren teilgenommen hat, das hinsichtlich der körperlichen Eignung Sportübungen zu enthalten hat,
4.  eine Fahrerlaubnis der Klasse B besitzt und
5.  höchstens 35 Jahre alt ist.

(2) Das Ministerium des Innern kann im Einvernehmen mit dem Ministerium der Finanzen Ausnahmen von Absatz 1 Nummer 5 zulassen.

### § 7  
Vorbereitungsdienst im gehobenen Dienst

(1) Die ausgewählten Bewerber werden unter Berufung in das Beamtenverhältnis auf Widerruf in den Vorbereitungsdienst eingestellt.
Sie führen während des Vorbereitungsdienstes die Dienstbezeichnung „Brandoberinspektoranwärter“.

(2) Der Vorbereitungsdienst dauert 18 Monate.
Er gliedert sich in fachtheoretische Ausbildungsabschnitte an einer Landesfeuerwehrschule und in fachpraktische Ausbildungszeiten.

(3) Während des Vorbereitungsdienstes hat der Beamte einen Führungslehrgang für Gruppenführer zu absolvieren.
Beamte, die diesen Lehrgang endgültig nicht bestehen, sind zu entlassen.
Das Beamtenverhältnis endet in diesem Fall am Tag der schriftlichen Bekanntgabe über das Nichtbestehen.

(4) Am Ende des Vorbereitungsdienstes hat der Beamte außerdem an einem Brandoberinspektorenlehrgang teilzunehmen.

(5) Der Vorbereitungsdienst schließt mit der Laufbahnprüfung ab.

### § 8  
Aufstieg in den gehobenen Dienst

(1) Beamte des mittleren feuerwehrtechnischen Dienstes können zum Aufstieg für die Laufbahn des gehobenen feuerwehrtechnischen Dienstes zugelassen werden, wenn sie:

1.  geeignet sind,
2.  sich in einer Dienstzeit von fünf Jahren seit der ersten Verleihung eines Amtes des mittleren Dienstes bewährt haben,
3.  einen Führungslehrgang für Gruppenführer erfolgreich absolviert haben und
4.  höchstens 45 Jahre alt sind.

Bei Beamten des mittleren feuerwehrtechnischen Dienstes, die die Laufbahnprüfung für den mittleren Dienst mindestens mit der Note „gut“ bestanden haben, kann die Dienstzeit nach Nummer 2 um ein Jahr verkürzt werden.

(2) Die Beamten werden auf Grund eines beim Dienstherrn vorzunehmenden Auswahlverfahrens zum Aufstieg zugelassen und in die Aufgaben der neuen Laufbahn eingeführt.
Die Einführungszeit dauert 18 Monate.
Die Beamten verbleiben bis zur Verleihung eines Amtes der neuen Laufbahn in ihrer bisherigen Rechtsstellung.

(3) Nach erfolgreicher Einführung ist die Aufstiegsprüfung, die der Laufbahnprüfung nach § 7 Absatz 5 entspricht, abzulegen.
Beamte, die die Prüfung endgültig nicht bestehen, treten mit der schriftlichen Bekanntgabe über das Nichtbestehen in die Aufgaben ihrer bisherigen Laufbahn zurück.

### § 9  
Einstellung in den Vorbereitungsdienst des höheren Dienstes

(1) In den Vorbereitungsdienst kann eingestellt werden, wer:

1.  ein mit einem Mastergrad abgeschlossenes Hochschulstudium oder einen gleichwertigen Abschluss in einer für den feuerwehrtechnischen Dienst geeigneten Fachrichtung erworben hat,
2.  nach amtsärztlichem Gutachten für den Dienst in der Feuerwehr geeignet ist,
3.  eine Fahrerlaubnis der Klasse B besitzt und
4.  höchstens 35 Jahre alt ist.

(2) Das Ministerium des Innern kann im Einvernehmen mit dem Ministerium der Finanzen Ausnahmen von Absatz 1 Nummer 4 zulassen.

### § 10  
Vorbereitungsdienst im höheren Dienst

(1) Die ausgewählten Bewerber werden unter Berufung in das Beamtenverhältnis auf Widerruf in den Vorbereitungsdienst eingestellt.
Sie führen während des Vorbereitungsdienstes die Dienstbezeichnung „Brandreferendar“.

(2) Der Vorbereitungsdienst dauert mindestens zwei Jahre.
Er vermittelt durch eine Ausbildung in fachbezogenen Schwerpunktbereichen der Laufbahnaufgaben die für den höheren feuerwehrtechnischen Dienst erforderlichen berufspraktischen Fähigkeiten, Kenntnisse und Fertigkeiten.
Die Ausbildung ist mit praxisbezogenen Lehrveranstaltungen zu verbinden.

(3) Zeiten einer beruflichen Tätigkeit nach Erwerb der nach § 9 Absatz 1 Nummer 1 vorgeschriebenen Bildungsvoraussetzung, die geeignet sind, die Ausbildung in einzelnen Ausbildungsabschnitten ganz oder teilweise zu ersetzen, können auf Antrag im Umfang von bis zu sechs Monaten auf den Vorbereitungsdienst angerechnet werden.

(4) Der Vorbereitungsdienst schließt mit der Laufbahnprüfung ab.
Die Laufbahnprüfung ist vor dem Prüfungsausschuss für die Laufbahn des höheren feuerwehrtechnischen Dienstes an dem Institut der Feuerwehr Nordrhein-Westfalen abzulegen.

### § 11  
Aufstieg in den höheren Dienst

(1) Beamte des gehobenen feuerwehrtechnischen Dienstes können zum Aufstieg für die Laufbahn des höheren feuerwehrtechnischen Dienstes zugelassen werden, wenn sie:

1.  geeignet sind,
2.  eine Dienstzeit von mindestens acht Jahren seit der ersten Verleihung eines Amtes des gehobenen Dienstes zurückgelegt haben,
3.  ein Beförderungsamt erreicht haben und
4.  höchstens 50 Jahre alt sind.

(2) Die Beamten werden auf Grund eines beim Dienstherrn vorzunehmenden Auswahlverfahrens zum Aufstieg zugelassen und in die Aufgaben der neuen Laufbahn eingeführt.
Die Einführungszeit dauert zwölf Monate; die Gliederung richtet sich nach der Verordnung über die Ausbildung und Prüfung für die Laufbahn des höheren feuerwehrtechnischen Dienstes im Land Nordrhein-Westfalen.
Die Beamten verbleiben bis zur Verleihung eines Amtes der neuen Laufbahn in ihrer bisherigen Rechtsstellung.

(3) Nach erfolgreicher Einführung ist die Aufstiegsprüfung für den höheren feuerwehrtechnischen Dienst an dem Institut der Feuerwehr Nordrhein-Westfalen, die der Laufbahnprüfung nach § 10 Absatz 4 entspricht, abzulegen.
Beamte, die die Prüfung endgültig nicht bestehen, treten mit der schriftlichen Bekanntgabe über das Nichtbestehen in die Aufgaben ihrer bisherigen Laufbahn zurück.

### § 12  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Feuerwehrlaufbahnverordnung vom 9.
September 1997 (GVBl.
II S. 773, 917), die durch Verordnung vom 2.
November 1999 (GVBl.
II S. 633) geändert worden ist, außer Kraft.

Potsdam, den 24.
Oktober 2011

Der Minister des Innern  
Dr.
Dietmar Woidke