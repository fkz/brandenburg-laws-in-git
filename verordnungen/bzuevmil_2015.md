## Verordnung zur Übertragung der beamtenrechtlichen Zuständigkeiten des Ministeriums für Infrastruktur und Landesplanung auf das Landesamt für Bauen und Verkehr sowie den Landesbetrieb Straßenwesen (Beamtenzuständigkeitsübertragungsverordnung MIL - BZÜVMIL)

Auf Grund des § 9 Absatz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186) in Verbindung mit

*   § 4 Absatz 1 Satz 3 des Landesbeamtengesetzes vom 3.
    April 2009 (GVBl.
    I S. 26) und § 13 Absatz 2 Satz 3 des Brandenburgischen Besoldungsgesetzes vom 20.
    November 2013 (GVBl. I Nr. 32 S. 2, Nr. 34) sowie in Verbindung mit § 1 Absatz 1 Satz 2 und Absatz 3 Satz 1 der Ernennungsverordnung vom 1.
    August 2004 (GVBl.
    II S. 742),
*   § 32 Absatz 1, § 54 Absatz 1, § 56 Absatz 1 Satz 5, § 57 Absatz 1 Satz 2, § 69 Absatz 5 Satz 1, § 84 Satz 2, § 85 Absatz 2 Satz 1, § 86 Absatz 1 Satz 3, § 87 Satz 4, § 88 Satz 5, § 89 Satz 3, § 92 Absatz 2 zweiter Halbsatz, § 103 Absatz 2 des Landesbeamtengesetzes und
*   § 54 Absatz 3 Satz 2 des Beamtenstatusgesetzes vom 17.
    Juni 2008 (BGBl. I S. 1010)

verordnet die Ministerin für Infrastruktur und Landesplanung:

#### § 1 Dienstvorgesetzter

(1) Dienstvorgesetzter im Sinne von § 2 Absatz 2 Satz 1 des Landesbeamtengesetzes für die Präsidentin oder den Präsidenten des Landesamtes für Bauen und Verkehr sowie deren oder dessen Stellvertretung ist das Mitglied der Landesregierung, zu dessen Geschäftsbereich das Landesamt für Bauen und Verkehr gehört.
Dienstvorgesetzter im Sinne von § 2 Absatz 2 Satz 1 des Landesbeamtengesetzes für die Vorsitzende oder den Vorsitzenden des Vorstandes (Präsidentin oder Präsident des Landesbetriebs Straßenwesen) sowie die weiteren Vorstandsmitglieder (Direktorinnen oder Direktoren beim Landesbetrieb Straßenwesen) ist das Mitglied der Landesregierung, zu dessen Geschäftsbereich der Landesbetrieb Straßenwesen gehört.
Ausgenommen von den Regelungen der Sätze 1 und 2 sind die Entscheidungen über Erholungsurlaub gemäß § 77 Absatz 2 des Landesbeamtengesetzes und Fortbildungen gemäß § 23 des Landesbeamtengesetzes.
Hierüber entscheidet die jeweilige Leitung beziehungsweise deren Vertretung.

(2) Für die übrigen Beamtinnen und Beamten des Landesamtes für Bauen und Verkehr und des Landesbetriebs Straßenwesen sind Dienstvorgesetzte im Sinne von § 2 Absatz 2 Satz 1 des Landesbeamtengesetzes die jeweiligen Leitungen.
Hiervon ausgenommen sind die folgenden Entscheidungen und Maßnahmen:

1.  Ernennung, Beförderung, Abordnung, Versetzung und Umsetzung von Beamtinnen und Beamten in Leitungspositionen mit einem Amt des höheren Dienstes ab der Besoldungsgruppe A 16 des Brandenburgischen Besoldungsgesetzes,
2.  Begründung von Beamtenverhältnissen auf Widerruf (Regierungsbaureferendare) sowie die Durchführung der hierfür erforderlichen Auswahlverfahren,
3.  Personalauswahlverfahren für die Leitung des Landesamtes für Bauen und Verkehr (Präsidentin oder Präsident) und die Abteilungsleitungen sowie für die Leitung des Landesbetriebs Straßenwesen (alle Vorstandsmitglieder einschließlich der oder des Vorsitzenden),
4.  Zulassungsverfahren zum Regelaufstieg gemäß § 22 des Landesbeamtengesetzes,
5.  Verfahren zur Prüfung der Dienstunfähigkeit gemäß den §§ 37 bis 43 des Landesbeamtengesetzes sowie die Versetzung in den Ruhestand gemäß § 50 des Landesbeamtengesetzes,
6.  Entscheidungen über den einstweiligen Ruhestand gemäß den §§ 47 und 48 des Landesbeamtengesetzes,
7.  Verfahren, Maßnahmen und Entscheidungen nach dem Landesdisziplinargesetz.

Für diese Entscheidungen und Maßnahmen bleibt das Mitglied der Landesregierung, zu dessen Geschäftsbereich das Landesamt für Bauen und Verkehr sowie der Landesbetrieb Straßenwesen gehören, Dienstvorgesetzter gemäß § 2 Absatz 3 des Landesbeamtengesetzes.

#### § 2 Übertragung der Ernennungsbefugnis

Dem Landesamt für Bauen und Verkehr sowie dem Landesbetrieb Straßenwesen wird die Befugnis zur Ernennung der Beamtinnen und Beamten, denen ein Amt des mittleren, des gehobenen Dienstes oder des höheren Dienstes bis einschließlich der Besoldungsgruppe A 15 verliehen wird, übertragen.
Ernennungen zur Verleihung eines Amtes des höheren Dienstes der Besoldungsgruppe A 15 bedürfen der vorherigen Zustimmung des Dienstvorgesetzten nach § 1 Absatz 1 Satz 1 und 2 dieser Verordnung.

#### § 3 Übertragung weiterer Befugnisse

Für die folgenden Entscheidungen und Maßnahmen nach dem Beamtenstatusgesetz und dem Landesbeamtengesetz wird die Befugnis der obersten Dienstbehörde auf das Landesamt für Bauen und Verkehr und den Landesbetrieb Straßenwesen jeweils für ihre Beamtinnen und Beamten übertragen:

1.  Entscheidungen über das Vorliegen der Voraussetzungen der Entlassung kraft Gesetzes gemäß § 32 Absatz 1 des Landesbeamtengesetzes,
2.  Entscheidungen über das Verbot der Führung der Dienstgeschäfte gemäß § 54 Absatz 1 des Landesbeamtengesetzes,
3.  Entscheidungen über die Aussagegenehmigung gemäß § 56 Absatz 1 Satz 3 des Landesbeamtengesetzes,
4.  Erteilung der Zustimmung zur Annahme von Belohnungen und Geschenken gemäß § 57 des Landesbeamtengesetzes mit der Maßgabe, dass die Entscheidung der jeweiligen Leitung persönlich oder der von ihr bestimmten Stelle obliegt,
5.  Erteilung der Erlaubnis zur Führung der Amtsbezeichnung mit dem Zusatz „außer Dienst“ („a. D.“) gemäß § 69 Absatz 5 des Landesbeamtengesetzes,
6.  Entscheidungen auf dem Gebiet des Nebentätigkeitsrechts gemäß den §§ 84 bis 89 des Landesbeamtengesetzes,
7.  Untersagung einer Erwerbstätigkeit oder sonstigen Beschäftigung nach Beendigung des Beamtenverhältnisses gemäß § 92 Absatz 2 des Landesbeamtengesetzes,
8.  Entscheidungen über die Feststellung der Befähigung für eine Laufbahn besonderer Fachrichtung des mittleren und des gehobenen Dienstes nach § 39 der Laufbahnverordnung,
9.  Zustimmung zum Absehen von der Rückforderung von Bezügen nach § 13 Absatz 2 Satz 3 des Brandenburgischen Besoldungsgesetzes.

Ausgenommen hiervon sind Entscheidungen und Maßnahmen gegenüber den Leitungen gemäß § 1 Absatz 1, für diese bleibt die oberste Dienstbehörde zuständig.

#### § 4 Befugnis zum Erlass von Widerspruchsbescheiden

Die Entscheidung über den Widerspruch von Beamtinnen und Beamten, Beamtinnen und Beamten im Ruhestand, früheren Beamtinnen und Beamten sowie deren Hinterbliebenen gegen den Erlass oder die Ablehnung eines das Beamtenverhältnis betreffenden Verwaltungsaktes oder gegen die Ablehnung des Anspruchs auf eine Leistung aus dem Beamtenverhältnis wird dem Dienstvorgesetzten gemäß § 1 Absatz 2 Satz 1 jeweils für die ihm zugeordneten Beamtinnen und Beamten übertragen, soweit die jeweilige Dienststelle die mit dem Widerspruch angefochtene Entscheidung erlassen hat.

#### § 5 Vertretung bei Klagen aus dem Beamtenverhältnis

Die Vertretung des Landes vor den Gerichten der Verwaltungsgerichtsbarkeit wird auf die in § 1 genannten Dienststellen für ihren jeweiligen Bereich übertragen, soweit diese selbst über den Widerspruch entschieden haben oder hätten entscheiden müssen.
Satz 1 ist in Verfahren zur Erlangung des einstweiligen Rechtsschutzes nach der Verwaltungsgerichtsordnung entsprechend anzuwenden.

#### § 6 Inkrafttreten

Diese Verordnung tritt am 1.
Dezember 2015 in Kraft.

Potsdam, den 20.
November 2015

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider