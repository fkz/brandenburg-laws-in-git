## Verordnung zur Ausführung des Gesetzes zur Ausführung des Gesetzes über die psychosoziale Prozessbegleitung im Strafverfahren (Psychosoziale Prozessbegleitungs-Ausführungsverordnung)

Auf Grund des § 10 des Gesetzes zur Ausführung des Gesetzes über die psychosoziale Prozessbegleitung im Strafverfahren vom 20.
Dezember 2016 (GVBl.
I Nr. 29) verordnet der Minister der Justiz und für Europa und Verbraucherschutz:

#### § 1 

Zu den in § 2 Absatz 2 des Gesetzes zur Ausführung des Gesetzes über die psychosoziale Prozessbegleitung im Strafverfahren vom 20.
Dezember 2016 (GVBl.
I Nr. 29) genannten Inhalten sollen in der Regel mindestens die folgenden Punkte zählen:

1.  Rechtliche Grundlagen
    1.  Rechtsgrundlagen und Grundsätze des Strafverfahrens
    2.  Rechte und Pflichten der Verletzten und der Bezugspersonen im Strafverfahren (aktive Teilnahme und Schutz vor Belastung), besondere Rechte und Pflichten von Kindern und Jugendlichen (Kinder- und Jugendhilferecht)
    3.  Das Ermittlungsverfahren – Strafanzeige
    4.  Funktion und Tätigkeit von Polizei und Staatsanwaltschaft
    5.  Die Strafverteidigung
    6.  Rechtsbeistand und Nebenklage
    7.  Aussagepsychologische Begutachtung
    8.  Das Hauptverfahren
    9.  Stellung der psychosozialen Prozessbegleitung im Strafverfahren
    10.  Möglichkeiten der Entschädigung (einschließlich Ansprüchen nach dem Opferentschädigungsgesetz), Schadensersatz und Schmerzensgeld einschließlich der möglichen Kostenfolgen für Verletzte
    11.  Täter-Opfer-Ausgleich
    12.  Grundlagen weiterer opferrelevanter Rechtsgebiete, zum Beispiel Familien- und Zivilrecht (Gewaltschutzgesetz)
2.  Viktimologie
    1.  Viktimologische Grundlagen
        
        aa)
        
        Theorien der Viktimisierung
        
        bb)
        
        Bedürfnisse von Opfern
        
        cc)
        
        Verarbeitungsprozesse und Bewältigungsstrategien von Opfern
        
        dd)
        
        Sekundäre Viktimisierung
        
        ee)
        
        Umgang mit Scham und Schuld
        
    2.  Wissen über spezielle Opfergruppen, unter anderem
        
        aa)
        
        Kinder und Jugendliche
        
        bb)
        
        Personen mit Behinderung
        
        cc)
        
        Personen mit einer psychischen Beeinträchtigung
        
        dd)
        
        Betroffene von Sexualstraftaten
        
        ee)
        
        Betroffene von Menschenhandel
        
        ff)
        
        Betroffene von Gewalttaten (mit schweren physischen, psychischen oder finanziellen Folgen oder längerem Tatzeitraum, wie zum Beispiel bei häuslicher Gewalt oder Stalking)
        
        gg)
        
        Betroffene von vorurteilsmotivierter Gewalt und sonstiger Hasskriminalität
        
    3.  Grundlagen gendersensibler und interkultureller Kommunikation
3.  Psychologie/Psychotraumatologie
    1.  Zielgruppenspezifische Belastungsfaktoren von Zeugen im Strafverfahren
    2.  Aspekte der Aussagepsychologie
    3.  Trauma und Traumabehandlung
    4.  Stabilisierungstechniken
4.  Theorie und Praxis der psychosozialen Prozessbegleitung
    1.  Ziele und Grundsätze der psychosozialen Prozessbegleitung
    2.  Leistungen und Methoden, insbesondere
        
        aa)
        
        Leistungen der psychosozialen Prozessbegleitung während der verschiedenen Phasen des Strafverfahrens
        
        bb)
        
        Methodenkompetenz (zum Beispiel adressatengerechte Kommunikation, fachgerechter Umgang mit Zeugenaussagen, Dokumentation, Aufklärung über fehlendes Zeugnisverweigerungsrecht)
        
        cc)
        
        Kooperation mit anderen Professionen, Netzwerkarbeit
        
5.  Qualitätssicherung und Eigenvorsorge
    1.  Formen der Dokumentation
    2.  Integration der psychosozialen Prozessbegleitung in das eigene Arbeitsfeld (Möglichkeiten und Grenzen)
    3.  Methoden zur Selbstreflexion (zum Beispiel kollegiale Beratung, Supervision)
    4.  interdisziplinärer Austausch
    5.  Reflexion der eigenen Motivation zur Opferhilfe
    6.  Methoden der Selbstfürsorge in der professionellen Opferarbeit (zum Beispiel Vermeidung von Überidentifikation, Burn-out-Prävention)

#### § 2 

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2017 in Kraft.

Potsdam, den 9.
Januar 2017

Der Minister der Justiz  
und für Europa und Verbraucherschutz

Stefan Ludwig