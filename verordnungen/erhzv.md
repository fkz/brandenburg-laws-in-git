## Verordnung zur Festsetzung von Erhaltungszielen und Gebietsabgrenzungen für Gebiete von gemeinschaftlicher Bedeutung (Erhaltungszielverordnung - ErhZV)

Auf Grund des § 14 Absatz 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21. Januar 2013 (GVBl. I Nr. 3) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Festsetzung

Die in Anlage 1 aufgeführten und in Anlage 2 näher beschriebenen Gebiete werden gemäß Artikel 4 Absatz 4 der Richtlinie 92/43/EWG als Gebiete von gemeinschaftlicher Bedeutung (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) in den in § 3 bestimmten Grenzen festgesetzt.
Sie sind Teil des kohärenten europäischen ökologischen Netzes „Natura 2000“.

#### § 2 Erhaltungsziele

(1) Die in Anlage 1 genannten Gebiete von gemeinschaftlicher Bedeutung stehen unter besonderem Schutz.
Erhaltungsziel für das jeweilige Gebiet ist die Erhaltung oder Wiederherstellung eines günstigen Erhaltungszustandes (§ 7 Absatz 1 Nummer 10 des Bundesnaturschutzgesetzes) der in Anlage 2 für das jeweilige Gebiet genannten natürlichen Lebensraumtypen oder Tier- und Pflanzenarten von gemeinschaftlichem Interesse.
Die dafür allgemein erforderlichen ökologischen Erfordernisse werden in den Anlagen 3 und 4 dargestellt.

(2) Soweit Gebiete von gemeinschaftlicher Bedeutung flächengleich mit Naturschutzgebieten sind, ergeben sich ihre Erhaltungsziele gemäß § 23 des Bundesnaturschutzgesetzes aus der jeweiligen Verordnung über das Naturschutzgebiet.
Die in einem Gebiet jeweils anwendbaren Schutzgebietsverordnungen sind in Anlage 2 aufgeführt.

#### § 3 Gebietsabgrenzung

(1) Die Grenzen der Gebiete von gemeinschaftlicher Bedeutung sind in den in Anlage 2 näher bezeichneten topografischen Karten (Maßstab 1 : 10 000) rot eingetragen.
Als Grenze gilt der innere Rand dieser Linie.
Zur Orientierung werden die Gebiete in Anlage 2 jeweils in einer Kartenskizze dargestellt.
Soweit Gebiete von gemeinschaftlicher Bedeutung flächengleich mit Naturschutzgebieten sind, sind diese Flächen in den in Satz 1 genannten Karten schraffiert eingetragen.

(2) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim jeweils zuständigen Landkreis, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 4 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 1.
Dezember 2015

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
\*) Diese Verordnung dient der Umsetzung der Richtlinie 92/43/EWG des Rates vom 21. Mai 1992 zur Erhaltung der natürlichen Lebensräume sowie der wild lebenden Tiere und Pflanzen (ABl. L 206 vom 22.7.1992, S. 7), die durch die Richtlinie 2013/17/EU vom 13.
Mai 2013 (ABl. L 158 vom 10.6.2013, S. 193) geändert worden ist.

* * *

### Anlagen

1

[Anlage 1 - Liste der Gebiete von gemeinschaftlicher Bedeutung](/br2/sixcms/media.php/68/GVBl_II_60_2015-Anlage-1.pdf "Anlage 1 - Liste der Gebiete von gemeinschaftlicher Bedeutung") 590.7 KB

2

[Anlage 2 (Gebiete 1 bis 15) - Gebiete von gemeinschaftlicher Bedeutung](/br2/sixcms/media.php/68/ErhZV-Anlage-2-Gebiete-1-15.pdf "Anlage 2 (Gebiete 1 bis 15) - Gebiete von gemeinschaftlicher Bedeutung") 8.9 MB

3

[Anlage 2 (Gebiete 16 bis 32) - Gebiete von gemeinschaftlicher Bedeutung](/br2/sixcms/media.php/68/ErhZV-Anlage-2-Gebiete-16-32.pdf "Anlage 2 (Gebiete 16 bis 32) - Gebiete von gemeinschaftlicher Bedeutung") 10.1 MB

4

[Anlage 3 - Ökologische Erfordernisse für einen günstigen Erhaltungszustand der natürlichen Lebensraumtypen nach Anhang I der FFH-Richtlinie](/br2/sixcms/media.php/68/GVBl_II_60_2015-Anlage-3.pdf "Anlage 3 - Ökologische Erfordernisse für einen günstigen Erhaltungszustand der natürlichen Lebensraumtypen nach Anhang I der FFH-Richtlinie") 669.4 KB

5

[Anlage 4 - Ökologische Erfordernisse für einen günstigen Erhaltungszustand von Tier- und Pflanzenarten nach Anhang II der FFH-Richtlinie](/br2/sixcms/media.php/68/GVBl_II_60_2015-Anlage-4.pdf "Anlage 4 - Ökologische Erfordernisse für einen günstigen Erhaltungszustand von Tier- und Pflanzenarten nach Anhang II der FFH-Richtlinie") 656.3 KB