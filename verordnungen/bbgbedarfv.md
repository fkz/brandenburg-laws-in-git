## Brandenburgische Verordnung über das Instrument zur Bedarfsermittlung nach § 118 des Neunten Buches Sozialgesetzbuch (BbgBedarfV)

Auf Grund des § 118 Absatz 2 des Neunten Buches Sozialgesetzbuch – Rehabilitation und Teilhabe von Menschen mit Behinderung – vom 23. Dezember 2016 (BGBl. I S. 3234) verordnet die Landesregierung:

#### § 1 Integrierter Teilhabeplan Brandenburg als Instrument zur Bedarfsermittlung für Kinder ab Schuleintritt, Jugendliche und Erwachsene

Als Instrument zur Bedarfsermittlung im Gesamtplanverfahren und im Teilhabeplanverfahren für leistungsberechtigte Kinder, Jugendliche und Erwachsene in der Eingliederungshilfe ab Schuleintritt wird für die örtlichen Träger der Eingliederungshilfe der Integrierte Teilhabeplan Brandenburg bestimmt, der aus der Anlage ersichtlich ist.

#### § 2 Evaluation

(1) Das Instrument zur Bedarfsermittlung nach § 1 wird ab dem Jahr 2021 begleitend wissenschaftlich evaluiert.

(2) Das Instrument ist in seiner Anwendung im Gesamtplanverfahren zu betrachten, wenn der örtliche Träger der Eingliederungshilfe der einzige zuständige Rehabilitationsträger ist und aus einer Leistungsgruppe Leistungen der Eingliederungshilfe bewilligt werden.

(3) Das Instrument ist in seiner Anwendung im Teilhabeplanverfahren zu betrachten, wenn

1.  neben dem örtlichen Träger der Eingliederungshilfe weitere Rehabilitationsträger Leistungen bewilligen und der örtliche Träger der Eingliederungshilfe der leistende Rehabilitationsträger ist oder
2.  der örtliche Träger der Eingliederungshilfe der einzige zuständige Rehabilitationsträger ist und aus mehreren Leistungsgruppen gemäß § 5 des Neunten Buches Sozialgesetzbuch Leistungen der Eingliederungshilfe gewährt werden.

(4) Der Abschlussbericht ist im Jahr 2023 vorzulegen.

#### § 3 Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 1. Januar 2020 in Kraft.

Potsdam, den 7.
Mai 2020

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

  
Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

Ursula Nonnemacher

* * *

### Anlagen

1

[Anlage (zu § 1) - Integrierter Teilhabeplan Brandenburg als Instrument zur Bedarfsermittlung für Kinder ab Schuleintritt, Jugendliche und Erwachsene](/br2/sixcms/media.php/68/GVBl_II_37_2020-Anlage.pdf "Anlage (zu § 1) - Integrierter Teilhabeplan Brandenburg als Instrument zur Bedarfsermittlung für Kinder ab Schuleintritt, Jugendliche und Erwachsene") 2.5 MB