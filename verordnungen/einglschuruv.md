## Verordnung über die Eingliederung von fremdsprachigen Schülerinnen und Schülern in die allgemein bildenden und beruflichen Schulen sowie zum Ruhen der Schulpflicht (Eingliederungs- und Schulpflichtruhensverordnung - EinglSchuruV)

Auf Grund des § 13 Absatz 3 Nummer 6 und des § 40 Absatz 2 in Verbindung mit § 36 Absatz 2, § 56 Satz 1, § 57 Absatz 4, § 58 Absatz 3, § 59 Absatz 9, § 60 Absatz 4 und § 61 Absatz 3 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
August 2002 (GVBl.
I S. 78), von denen § 56 Satz 1 zuletzt durch Artikel 2 Nummer 18 des Gesetzes vom 14.
März 2014 (GVBl.
I Nr. 14) geändert worden ist sowie § 40 Absatz 2 durch Artikel 1 Nummer 2 und § 57 Absatz 4 durch Artikel 1 Nummer 3 des Gesetzes vom 10.
Juli 2017 (GVBl.
I Nr. 16) neu gefasst worden sind, verordnet der Minister für Bildung, Jugend und Sport:

**Inhaltsübersicht**

[§ 1 Geltungsbereich](#1)

[§ 2 Ruhen der Schulpflicht](#2)

[§ 3 Allgemeines zur Eingliederung fremdsprachiger Schülerinnen und Schüler](#3)

[§ 4 Aufnahme](#4)

[§ 5 Unterricht in Vorbereitungsgruppen](#5)

[§ 6 Unterricht in Förderkursen](#6)

[§ 7 Muttersprachlicher Unterricht](#7)

[§ 8 Fremdsprachenregelung](#8)

[§ 9 Sprachfeststellungsprüfung](#9)

[§ 10 Leistungsbewertung, Zeugnisse](#10)

[§ 11 Berufliche Bildung](#11)

[§ 12 Inkrafttreten, Außerkrafttreten](#12)

#### § 1 Geltungsbereich

(1) Diese Verordnung gilt

1.  in Bezug auf die Regelungen für das Ruhen der Schulpflicht für junge Menschen, die sich unabhängig davon, ob sie selbst oder nur ein Elternteil die ausländerrechtlichen Voraussetzungen erfüllen, im Land Brandenburg nach einem Asylantrag oder aus anderen Fluchtgründen auf der Grundlage einer Aufenthaltsgestattung, einer Aufenthaltserlaubnis oder einer Duldung aufhalten und schulpflichtig sind sowie
2.  im Übrigen für Schülerinnen und Schüler, deren Muttersprache nicht Deutsch ist und die über keine Deutschkenntnisse verfügen oder deren Deutschkenntnisse nicht ausreichen, um am Regelunterricht mit Erfolg teilnehmen zu können (fremdsprachige Schülerinnen und Schüler).
    Bei Vorliegen ausreichender Deutschkenntnisse, spätestens jedoch nach vier Schulbesuchsjahren, endet der Status als fremdsprachige Schülerin oder fremdsprachiger Schüler.
    In begründeten Einzelfällen kann dieser genannte Zeitraum mit Zustimmung des staatlichen Schulamtes verlängert werden.

(2) Sie regelt

1.  Besonderheiten des Ruhens der Schulpflicht,
2.  Besonderheiten der Aufnahme, der Fördermaßnahmen, der Leistungsbewertung, des Fremdsprachenunterrichts, des muttersprachlichen Unterrichts und des Erwerbs von Abschlüssen für die Eingliederung der Schülerinnen und Schüler in die Schule.

Im Übrigen gelten die Regelungen der jeweiligen Bildungsgangverordnungen.

(3) Diese Verordnung gilt in den Bildungsgängen des Zweiten Bildungsweges entsprechend.
Sie gilt nicht für Schülerinnen und Schüler in den deutsch-polnischen Schulprojekten.

#### § 2 Ruhen der Schulpflicht

(1) Für junge Menschen gemäß § 1 Absatz 1 Nummer 1, die im Rahmen der Erstaufnahme in einer Aufnahmeeinrichtung des Landes untergebracht sind, ruht die Schulpflicht für den Zeitraum von drei Monaten nach der Unterbringung in einer Aufnahmeeinrichtung.

(2) Für junge Menschen gemäß § 1 Absatz 1 Nummer 1, die nicht in einer Aufnahmeeinrichtung des  
Landes untergebracht sind, ruht die Schulpflicht für den Zeitraum von sechs Wochen nach Verteilung auf oder Zuzug in die Landkreise und kreisfreien Städte.
Soweit diese jungen Menschen gemäß § 42a und § 42 des Achten Buches Sozialgesetzbuch im Rahmen einer Inobhutnahme in einer Jugendhilfeeinrichtung untergebracht sind, ruht die Schulpflicht abweichend von Satz 1 für den Zeitraum von drei Monaten nach dem Beginn der Inobhutnahme.

(3) Das Ruhen der Schulpflicht gemäß den Absätzen 1 und 2 darf insgesamt drei Monate nicht überschreiten.
Während des Ruhens der Schulpflicht besteht das Recht, eine Schule zu besuchen.

#### § 3 Allgemeines zur Eingliederung fremdsprachiger Schülerinnen und Schüler

(1) Schülerinnen und Schüler haben im Rahmen der personellen, schulorganisatorischen und sächlichen Voraussetzungen einen Anspruch auf schulische Förderung und Ausgleich von Benachteiligungen, die aus den mangelnden Deutschkenntnissen erwachsen.

(2) Die Förderung der Schülerinnen und Schüler gemäß den §§ 5 und 6 erfolgt nach Feststellung der Lernausgangslage unter Berücksichtigung der Ergebnisse der Sprachstandsfeststellung in der deutschen Sprache und auf der Grundlage individueller Lernpläne.

(3) Schülerinnen und Schüler nehmen ihre Mitwirkungsrechte in der Klasse oder dem Kurs wahr, in die oder den sie gemäß § 4 aufgenommen wurden.
Die in den Fördermaßnahmen unterrichtenden Lehrkräfte sind während der Zeitdauer einer Förder- oder Eingliederungsmaßnahme Mitglieder der jeweiligen Klassenkonferenz gemäß § 88 Absatz 1 des Brandenburgischen Schulgesetzes.

#### § 4 Aufnahme

(1) Über die Aufnahme entscheidet die Schulleiterin oder der Schulleiter auf der Grundlage der Zeugnisse oder entsprechender Unterlagen sowie eines Gesprächs mit der Schülerin oder dem Schüler und deren Eltern, zu dem sprachkundige Lehrkräfte oder andere sprachkundige in der Regel volljährige Personen bei Bedarf hinzugezogen werden sollen.

(2) Bei der Aufnahme und der Zuordnung zu der jeweiligen Jahrgangsstufe ist vom Alter, von der bisherigen Jahrgangs- und Kurseinstufung oder dem bisher im Herkunftsland besuchten Bildungsgang auszugehen.
Ist die Vorbildung für die Aufnahme in eine dem Alter entsprechende Jahrgangsstufe insbesondere auf Grund einer fehlenden Dokumentation des bisherigen Bildungsverlaufs zweifelhaft, kann die Teilnahme am Unterricht nach Anhörung der Eltern in der nächstniedrigeren Jahrgangsstufe erfolgen, soweit damit nicht ein Übergang in die Primarstufe verbunden ist.

(3) Wer am Unterricht in der nächstniedrigeren Jahrgangsstufe teilnimmt, kann auf Antrag der Eltern und durch Beschluss der Klassenkonferenz in den Unterricht der dem Alter entsprechenden Jahrgangsstufe wechseln, wenn die bisherigen Leistungen eine erfolgreiche Mitarbeit in der höheren Jahrgangsstufe erwarten lassen und wenn die Schülerin oder der Schüler dadurch in ihrer oder seiner Lernentwicklung besser gefördert werden kann.
Schülerinnen und Schüler in der flexiblen Eingangsphase rücken spätestens nach drei Schulbesuchsjahren in die Jahrgangsstufe 3 auf.

(4) Bei der Klassen- und Kursbildung ist auf eine gleichmäßige Verteilung der aufzunehmenden Schülerinnen und Schüler zu achten.
Der Gesamtanteil der Schülerinnen und Schüler gemäß § 1 Absatz 1 Nummer 2 soll in der Regel 30 Prozent der Schülerinnen und Schüler einer Klasse nicht übersteigen.

(5) Über die Teilnahme an Fördermaßnahmen gemäß den §§ 5 und 6 entscheidet die Schulleiterin oder der Schulleiter auf Vorschlag der Klassenlehrkraft, der Tutorin oder des Tutors in der Regel nach Beratung der Eltern.

#### § 5 Unterricht in Vorbereitungsgruppen

(1) Der Unterricht in Vorbereitungsgruppen dient vorwiegend dem intensiven Erlernen der deutschen Sprache, der Alphabetisierung und der Vorbereitung auf die vollständige Teilnahme am Regelunterricht sowie der durchgängigen Sprachförderung und der sozialen Integration.
Soweit Schülerinnen und Schüler über keine oder nur geringe schulische Vorkenntnisse verfügen, können für diese eigene Vorbereitungsgruppen mit dem Ziel der Alphabetisierung gebildet werden.
Der Unterricht erfolgt auf der Grundlage der jeweils geltenden Rahmenlehrpläne, verbindlicher curricularer Materialien, der individuellen Lernpläne und der jeweils geltenden Stundentafeln.
Die individuellen Lernpläne sind von den in den Vorbereitungsgruppen unterrichtenden Lehrkräften in Zusammenarbeit mit der jeweiligen Klassenlehrkraft zu erstellen.

(2) Schülerinnen und Schüler können in den Jahrgangsstufen 2 und 3 bis zu zwölf Monaten, in den Jahrgangsstufen 4 bis 10 bis zu 24 Monaten in der Vorbereitungsgruppe verbleiben.
Die Schülerinnen und Schüler an beruflichen Schulen können bis zu zwölf Monaten in der Vorbereitungsgruppe verbleiben.
In begründeten Einzelfällen kann die Verweildauer in den Jahrgangsstufen 2 bis 10 mit Zustimmung des staatlichen Schulamtes verlängert werden.
Dies gilt insbesondere, wenn auf Grund längerer Krankheit oder aus anderen nicht selbst verschuldeten Gründen der Schülerin oder des Schülers noch keine ausreichenden Deutschkenntnisse für einen erfolgreichen Schulbesuch ohne Unterricht in der Vorbereitungsgruppe vorliegen.

(3) Schülerinnen und Schüler in der Jahrgangsstufe 1, die in einer Aufnahmeeinrichtung des Landes untergebracht sind, können während ihres gemäß § 47 Absatz 1 Satz 1 und 4 und Absatz 1a Satz 2 des Asylgesetzes auf längstens sechs Monate beschränkten Aufenthaltes in der Vorbereitungsgruppe verbleiben.

(4) Während des Besuchs der Vorbereitungsgruppe soll eine Teilnahme am Regelunterricht insbesondere in den Fächern Sport, Musik, Kunst, Sachunterricht und Wirtschaft-Arbeit-Technik (W-A-T) erfolgen.
In Abhängigkeit von den individuellen Sprachfortschritten kann die Teilnahme am gemeinsamen Regelunterricht auch andere Fächer umfassen.
An beruflichen Schulen entscheidet die Klassenkonferenz über den Umfang der Teilnahme am Regelunterricht und die Fächerauswahl.
Die Sätze 1 und 2 gelten nicht für die Schülerinnen und Schüler, die sich im Bildungsgang der Berufsfachschule zum Erwerb beruflicher Grundbildung und von gleichgestellten Abschlüssen der Sekundarstufe I (BFS-G-Plus) befinden.

(5) Vorbereitungsgruppen werden im Rahmen personeller und schulorganisatorischer Voraussetzungen eingerichtet, wenn dafür ein Bedarf besteht.
Die Entscheidung über die Einrichtung einer Vorbereitungsgruppe trifft die Schulleiterin oder der Schulleiter.
Es können Schülerinnen und Schüler verschiedener Sprachzugehörigkeiten, aus verschiedenen Jahrgangsstufen und aus verschiedenen Schulen gemeinsam unterrichtet werden.
Die Entscheidung über die Einrichtung einer schulübergreifenden Vorbereitungsgruppe trifft das staatliche Schulamt.

(6) Der Unterricht in den Vorbereitungsgruppen und der Unterricht in den Fächern gemäß Absatz 4 ergeben in der Summe die Anzahl der Unterrichtsstunden, die in der jeweiligen Jahrgangsstufe nach der Kontingentstundentafel zu unterrichten sind.
Soweit es die personellen, schulorganisatorischen oder sächlichen Voraussetzungen erfordern, kann nach Antrag durch die Schule und Entscheidung des staatlichen Schulamtes zeitlich befristet von der Stundentafel abgewichen werden.
Ein Abweichen darf sechs Monate nicht überschreiten.

(7) Schülerinnen und Schüler nehmen am Regelunterricht der ihnen zugewiesenen Jahrgangsstufe und Klasse in der Jahrgangsstufe 2 mit mindestens 14 Unterrichtsstunden, in den Jahrgangsstufen 3 und 4 mit mindestens zehn Unterrichtsstunden und in den Jahrgangsstufen 5 bis 10 mit mindestens acht Unterrichtsstunden in der Woche teil.
Soweit es die personellen, schulorganisatorischen oder sächlichen Voraussetzungen erfordern, kann nach Antrag durch die Schule und Entscheidung des staatlichen Schulamtes zeitlich befristet von der Stundentafel abgewichen werden.
Ein Abweichen darf sechs Monate nicht überschreiten.

(8) Die Eltern werden über pädagogische und fachliche Ziele, Inhalte und die Organisation der Fördermaßnahmen informiert und erhalten regelmäßige Informationen über die Lernfortschritte der Schülerinnen und Schüler.

#### § 6 Unterricht in Förderkursen

(1) Der Unterricht in Förderkursen dient in der Regel der Weiterentwicklung deutscher Sprachkenntnisse.
Darüber hinaus kann dieser Unterricht nach entsprechenden Lernfortschritten in der deutschen Sprache auch genutzt werden, um fehlende Kenntnisse in den Unterrichtsfächern auszugleichen.
Der Unterricht erfolgt auf der Grundlage der jeweils geltenden Rahmenlehrpläne oder anderer geeigneter curricularer Materialien und der individuellen Lernpläne.
Die individuellen Lernpläne sind von den Lehrkräften zu erstellen, die den Unterricht in den Förderkursen erteilen.
Sofern der Unterricht auch in Teilen zur Förderung in den Unterrichtsfächern genutzt wird, ist der jeweilige individuelle Lernplan auch mit den diesen Unterricht erteilenden Lehrkräften abzustimmen.
An beruflichen Schulen können Förderkurse auch zur Alphabetisierung von Schülerinnen und Schülern eingerichtet werden, wenn aus schulorganisatorischen Gründen die Bildung einer Vorbereitungsgruppe nicht möglich ist.

(2) Schülerinnen und Schüler sollen nicht länger als 24 Monate an einem Förderkurs teilnehmen.
Die Teilnahme am Unterricht im Förderkurs muss in der Regel bis zum Ende der Einführungsphase der gymnasialen Oberstufe abgeschlossen sein.
Sollte in den ersten beiden Kurshalbjahren der Qualifikationsphase der gymnasialen Oberstufe ein Bedarf für eine Förderung zur Verbesserung vorhandener Kenntnisse in der deutschen Sprache bestehen, kann auf Antrag der Eltern am Förderkurs teilgenommen werden.

(3) Förderkurse werden im Rahmen personeller und schulorganisatorischer Voraussetzungen eingerichtet, wenn dafür ein Bedarf besteht.
Die Entscheidung über die Einrichtung von Förderkursen trifft die Schulleiterin oder der Schulleiter.
Es können Schülerinnen und Schüler verschiedener Sprachzugehörigkeiten und aus verschiedenen Jahrgangsstufen gemeinsam unterrichtet werden.

(4) Die Eltern werden über pädagogische und fachliche Ziele, Inhalte und die Organisation der Fördermaßnahmen informiert und erhalten regelmäßige Informationen über die Lernfortschritte der Schülerinnen und Schüler.

#### § 7 Muttersprachlicher Unterricht

(1) Muttersprachlicher Unterricht dient der Förderung und Pflege der in der Muttersprache oder Amtssprache des Herkunftslandes bisher erworbenen sprachlichen und der Weiterentwicklung interkultureller Kompetenzen.

(2) Die Teilnahme am muttersprachlichen Unterricht ist freiwillig und erfolgt zusätzlich zur Stundentafel.
Alle Schülerinnen und Schüler, deren Muttersprache oder Amtssprache des Herkunftslandes nicht Deutsch ist, können am muttersprachlichen Unterricht teilnehmen.
Die Leistungen werden nicht bewertet.

(3) Lerngruppen für muttersprachlichen Unterricht können jahrgangsstufen- oder schulübergreifend eingerichtet werden.
Im Rahmen der personellen Voraussetzungen und unter Berücksichtigung der übrigen Fördermaßnahmen kann der muttersprachliche Unterricht bis zu vier Wochenstunden umfassen.
Die Entscheidung über die Einrichtung von schulübergreifendem muttersprachlichen Unterricht trifft das staatliche Schulamt.

(4) Wenn aus personellen oder organisatorischen Gründen an einer Schule die Erteilung von muttersprachlichem Unterricht nicht möglich ist, können freien Trägern nach Maßgabe haushaltsrechtlicher Möglichkeiten und der Erfüllung festgelegter Anforderungen auf Antrag Zuwendungen zum Zweck der Erteilung des muttersprachlichen Unterrichts gewährt werden.
Nach Maßgabe der räumlichen und organisatorischen Möglichkeiten sollen Schulträger Schulräume für dieses Angebot kostenlos zur Verfügung stellen.

#### § 8 Fremdsprachenregelung

(1) Schülerinnen und Schüler, die in die Sekundarstufe I aufgenommen werden, können statt der Teilnahme am Fremdsprachenunterricht ihre Fähigkeiten und Kenntnisse durch das Ablegen einer Sprachfeststellungsprüfung in ihrer Herkunftssprache anerkennen lassen.
Das Ergebnis dieser Prüfung ersetzt die Note sowie die Teilnahme am Unterricht in der ersten, zweiten oder dritten Fremdsprache und geht in die Versetzungs- und Abschlussentscheidung ein.
Für Englisch als erste Fremdsprache kann das Ergebnis der Sprachfeststellungsprüfung die Note nur auf Antrag der Eltern hin ersetzen, jedoch nicht die Teilnahme am Unterricht.

(2) Schülerinnen und Schüler, die in die Einführungsphase der gymnasialen Oberstufe aufgenommen werden, können eine Sprachfeststellungsprüfung ablegen und damit die in der gymnasialen Oberstufe geforderte Belegverpflichtung in einer Fremdsprache erfüllen, wenn im Übrigen eine ausreichende Anzahl von Kursen in die Gesamtqualifikation eingebracht werden kann.
Sofern eine Sprachfeststellungsprüfung bereits in der Sekundarstufe I abgelegt wurde, ist bei einer Aufnahme der Schülerin oder des Schülers in die Einführungsphase eine neue Sprachfeststellungsprüfung auf dem jeweiligen Anforderungsniveau der gymnasialen Oberstufe abzulegen.
Das Ergebnis dieser Prüfung geht nicht in die Gesamtqualifikation ein.

#### § 9 Sprachfeststellungsprüfung

(1) Sprachfeststellungsprüfungen können innerhalb der Sekundarstufe I und zu Beginn der Einführungsphase auf Antrag der Eltern durchgeführt werden, wenn geeignete Prüferinnen und Prüfer zur Verfügung stehen.
Vor Antragstellung sind die Schülerinnen und Schüler sowie deren Eltern über Zweck, Anforderungen und Organisation der Sprachfeststellungsprüfung zu informieren.

(2) Die Festlegung der Prüferin oder des Prüfers sowie die Durchführung der Prüfung obliegen dem staatlichen Schulamt.

(3) Für die Durchführung der Prüfung setzt das staatliche Schulamt jeweils einen Prüfungsausschuss ein.
Der Prüfungsausschuss besteht aus

1.  einem Mitglied der Schulleitung als das den Vorsitz führende Mitglied,
2.  der Prüferin oder dem Prüfer mit Kenntnissen in der zu prüfenden Sprache sowie
3.  einer Lehrkraft mit der Lehrbefähigung für eine moderne Fremdsprache in der entsprechenden Schulstufe, wenn das unter Nummer 2 genannte Mitglied nicht über diese Lehrbefähigung verfügt.

Falls erforderlich, ist dem staatlichen Schulamt vorbehalten, den Vorsitz an eine Schulrätin oder einen Schulrat zu übertragen.

(4) Die Sprachfeststellungsprüfung besteht aus einer schriftlichen und einer mündlichen Prüfung.
Beide Prüfungen sollen an einem Tag stattfinden.
Die jeweiligen Prüfungsanforderungen richten sich nach den Fremdsprachenrahmenlehrplänen und berücksichtigen die am Ende der jeweiligen Schulstufe zu erreichenden Kompetenzen des angestrebten Bildungsgangs.
Bei der Festsetzung der Prüfungsanforderungen im schriftlichen und mündlichen Prüfungsteil muss eine Lehrkraft, die über die Lehrbefähigung für eine moderne Fremdsprache in der entsprechenden Schulstufe verfügt, verantwortlich mitwirken, wenn die Prüferin oder der Prüfer nicht selbst über die entsprechende Lehrbefähigung verfügt.
Themen und Inhalte berücksichtigen den bisherigen unterrichtlichen und außerschulischen Erfahrungshintergrund der Schülerinnen und Schüler.
Die Dauer der schriftlichen Prüfung beträgt in der Sekundarstufe I 60 bis 90 Minuten und in der Sekundarstufe II 120 Minuten.
Der mündliche Prüfungsteil dauert in der Regel 20 Minuten und kann auch als Gruppenprüfung durchgeführt werden.

(5) Über das Bestehen der Prüfung entscheidet der Prüfungsausschuss auf der Grundlage der Ergebnisse beider Prüfungsteile.
Die Gesamtnote der Sprachfeststellungsprüfung setzt sich aus dem rechnerischen Mittelwert der beiden Prüfungsteile zusammen.
Die Gesamtnote ist nach der rechnerischen Ermittlung durch Auf- oder Abrunden festzusetzen.
Liegt das rechnerische Ergebnis genau zwischen zwei Notenstufen oder Punktwerten (n,5), ist zugunsten der Schülerin oder des Schülers zu entscheiden.
Über den Prüfungsverlauf ist ein Protokoll in deutscher Sprache zu erstellen.
Die Prüfung ist bestanden, wenn in beiden Prüfungsteilen mindestens ausreichende Leistungen nachgewiesen werden.

(6) Eine nicht bestandene Sprachfeststellungsprüfung kann einmal wiederholt werden.
Eine Befreiung vom Unterricht in der zweiten oder dritten Fremdsprache sowie von der Belegverpflichtung in einer modernen Fremdsprache kann erst nach Bestehen der Sprachfeststellungsprüfung erfolgen.

#### § 10 Leistungsbewertung, Zeugnisse

(1) Die individuelle Lernentwicklung der Schülerinnen und Schüler ist fortlaufend zu begleiten, zu beobachten und in geeigneter Weise zu dokumentieren.
Die Leistungsbewertung erfolgt auf der Grundlage der sich aus den Rahmenlehrplänen ergebenden Anforderungen.
Eine Absenkung der Leistungsanforderungen und ein Abweichen von den Grundsätzen der Leistungsbewertung sind nicht zulässig.

(2) Ausgehend von dem Zeitpunkt der Aufnahme in eine Schule ist in den ersten vier Schulhalbjahren und auf Beschluss der Klassenkonferenz bis zu weiteren zwei Schulhalbjahren

1.  die individuelle Lernentwicklung bei der Bewertung der Leistungen besonders zu berücksichtigen, insbesondere soll der Lernentwicklung bei der Bildung der abschließenden Leistungsbewertung zum Schulhalbjahr oder zum Schuljahresende eine ausschlaggebende Bedeutung zukommen,
2.  auf sprachlich bedingte Erschwernisse des Lernens Rücksicht zu nehmen, insbesondere sollen bei der Aufgabenstellung und Aufgabenformulierung die jeweiligen sprachlichen Voraussetzungen der Schülerinnen und Schüler berücksichtigt werden und
3.  ein Nachteilsausgleich gemäß Absatz 4 zu gewähren.

(3) Ist eine abschließende Leistungsbewertung zum Schulhalbjahr oder zum Schuljahresende trotz Anwendung der Absätze 2 und 4 aufgrund von fehlenden Deutschkenntnissen in einzelnen Fächern oder insgesamt noch nicht möglich, ist dies auf Beschluss der Klassenkonferenz auf dem Zeugnis unter „Bemerkungen“ einzutragen und auch der erteilte Unterricht auf dem Zeugnis zu bestätigen.
In den Jahrgangsstufen 3 bis 10 und in den beruflichen Schulen kann die Klassenkonferenz ausgehend von den individuellen Voraussetzungen einer Schülerin oder eines Schülers auch beschließen, dass die zu erteilenden Noten durch schriftliche Informationen zur Lernentwicklung ergänzt werden.
Der Beschluss wird nach einem Jahr von der Klassenkonferenz überprüft und kann längstens für eine Übergangszeit von bis zu zwei Schuljahren gefasst werden.
Dies gilt nicht für abschlussbezogene Jahrgangsstufen.

(4) Schülerinnen und Schüler, die auf Grund noch nicht ausreichender Kompetenzen in der deutschen Sprache keinen oder einen erschwerten Zugang zu Aufgabenstellungen in den Fächern haben und deshalb nicht das tatsächliche Leistungsvermögen nachweisen können, kann auf Beschluss der Klassenkonferenz ein Nachteilsausgleich jeweils befristet für ein Schulhalbjahr gewährt werden.
Im Rahmen des Nachteilsausgleichs können die Bedingungen für mündliche oder schriftliche Leistungsfeststellungen geändert werden, insbesondere durch

1.  eine Verlängerung der Bearbeitungszeit,
2.  die Verwendung spezieller Arbeitsmittel, insbesondere eines Wörterbuches in der Herkunftssprache (auch in elektronischer Form),
3.  alternative Aufgabenstellungen und Präsentationen von Ergebnissen,
4.  die Bereitstellung von Verständnishilfen und zusätzlichen Erläuterungen durch die jeweilige Lehrkraft und
5.  die Schaffung individueller Leistungsfeststellungen in Einzelsituationen mit individuellen Aufgabenstellungen.

(5) Die durch die Sprachfeststellungsprüfung erreichte Note wird in Abhängigkeit von der Entscheidung der Schülerin oder des Schülers oder deren Eltern anstelle der Note in der ersten, zweiten oder dritten Fremdsprache auf das Zeugnis übertragen.
Unter Bemerkungen erfolgt ein entsprechender Hinweis.
In der gymnasialen Oberstufe erfolgt unter Bemerkungen der Hinweis, dass die Belegverpflichtung in einer Fremdsprache durch eine Sprachfeststellungsprüfung erfüllt wurde.

(6) Das Ergebnis einer innerhalb der Bundesrepublik Deutschland abgelegten schulischen Sprachfeststellungsprüfung wird durch das staatliche Schulamt anerkannt, wenn die Anforderungen denen dieser Verordnung gleichwertig sind.

(7) Der Unterricht in Förderkursen und die Teilnahme am muttersprachlichen Unterricht der Schule werden auf dem Zeugnis ausgewiesen.
Die Gewährung eines Nachteilsausgleichs wird in den Zeugnissen nicht vermerkt.

#### § 11 Berufliche Bildung

(1) Berufsschulpflichtige Schülerinnen und Schüler werden mit Hilfe von Maßnahmen gemäß den §§ 5 bis 8 soweit gefördert, dass sie den Anforderungen im Unterricht ihres Bildungsgangs und in der Ausbildungsstätte oder Praktikumsstätte in ausreichendem Maß zu folgen vermögen.

(2) Wer in ein Berufsausbildungsverhältnis oder in einen Bildungsgang zur Vertiefung der Allgemeinbildung und Berufsorientierung oder Berufsvorbereitung eintreten will und über keine deutschen Sprachkenntnisse verfügt, kann diese auch in vom für Schule zuständigen Ministerium anerkannten Fördermaßnahmen bei kommunalen oder freien Trägern erwerben.
Auf Antrag an das staatliche Schulamt ruht während dieser Zeit die Berufsschulpflicht.

(3) Die Teilnahme am Unterricht in einer Pflichtfremdsprache kann in Bildungsgängen, für die diese Fremdsprache ein wesentlicher berufsbezogener Bestandteil ist, nicht durch eine Sprachfeststellungsprüfung ersetzt werden.
Dies sind insbesondere

1.  der Bildungsgang zur Vermittlung des schulischen Teils einer Berufsausbildung nach dem Berufsbildungsgesetz oder der Handwerksordnung,
2.  die Bildungsgänge der Fachschule Technik und Wirtschaft,
3.  die Bildungsgänge der Berufsfachschule zum Erwerb eines Berufsabschlusses nach Landesrecht und
4.  die Bildungsgänge der Fachoberschule.

(4) In den Bildungsgängen der Berufsfachschule Soziales und der Fachschule Sozialwesen kann die Teilnahme am Unterricht in einer Fremdsprache durch eine Sprachfeststellungsprüfung ersetzt werden.
Auf dem Zeugnis erfolgt unter Bemerkungen der Hinweis, dass die Teilnahme am Unterricht in einer Fremdsprache durch eine Sprachfeststellungsprüfung ersetzt wurde.

#### § 12 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
August 2017 in Kraft.
Gleichzeitig treten die Eingliederungsverordnung vom 25.
Februar 2014 (GVBl.
II Nr. 14) und die Schulpflichtruhensverordnung vom 30.
November 1998 (GVBl.
1999 II S. 86) außer Kraft.

Potsdam, den 4.
August 2017

Der Minister für Bildung,  
Jugend und Sport

Günter Baaske