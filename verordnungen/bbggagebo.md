## Gebührenordnung für die Gutachterausschüsse für Grundstückswerte des Landes Brandenburg und deren Geschäftsstellen  (Brandenburgische Gutachterausschuss-Gebührenordnung - BbgGAGebO)

Auf Grund des § 3 Absatz 1 und 2 in Verbindung mit § 7 Absatz 1 Nummer 1, § 9 Satz 2 und § 18 Absatz 2 Satz 2 des Gebührengesetzes für das Land Brandenburg vom 7.
Juli 2009 (GVBl.
I S. 246) verordnet der Minister des Innern:

### § 1  
Anwendungsbereich

(1) Für Amtshandlungen der Gutachterausschüsse für Grundstückswerte und deren Geschäftsstellen werden Gebühren und Auslagen nach dieser Verordnung und dem Gebührentarif in der Anlage zu dieser Verordnung erhoben.
Der Gebührentarif ist Bestandteil dieser Verordnung.

(2) Diese Verordnung ist nicht anzuwenden, wenn der Gutachterausschuss oder dessen Geschäftsstelle von dem Gericht oder der Staatsanwaltschaft zu Beweiszwecken herangezogen wird.

### § 2  
Umsatzsteuer

Unterliegt die Amtshandlung der Umsatzsteuerpflicht, ist der Gebühr die gesetzliche Umsatzsteuer hinzuzurechnen.

### § 3  
Gebührenpflicht für juristische Personen

Für Amtshandlungen der Gutachterausschüsse und deren Geschäftsstellen bleiben die in § 8 Absatz 1 des Gebührengesetzes für das Land Brandenburg genannten juristischen Personen des öffentlichen Rechts und Stiftungen bürgerlichen Rechts zur Zahlung von Gebühren verpflichtet.

### § 4  
Gebühren und Auslagen

Als bereits in die Gebühr nach § 1 Absatz 1 einbezogen gelten:

1.  die Entschädigung der ehrenamtlichen Gutachter,
2.  Auslagen nach § 9 Satz 2 Nummer 6 des Gebührengesetzes für das Land Brandenburg,
3.  bei der Erstattung von Gutachten und Obergutachten jeweils die Kosten einer Ausfertigung des Gutachtens für den Antragsteller und den Eigentümer des Grundstücks.

### § 5  
Oberer Gutachterausschuss

Die §§ 1 bis 4 sind entsprechend für Amtshandlungen des Oberen Gutachterausschusses und dessen Geschäftsstelle anzuwenden.

### § 6  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Gutachterausschuss-Gebührenordnung vom 19.
November 2003 (GVBl.
II S.
678) außer Kraft.

Potsdam, den 30.
Juli 2010

Der Minister des Innern  
Rainer Speer

**Anlage  
**(zu § 1 Absatz 1)

#### Gebührentarif

Tarifstelle

Gegenstand der Gebühr

1

Erstattung von Gutachten und Obergutachten sowie Zustandsfeststellungen

2

Ermittlung von Bodenrichtwerten

3

Bereitstellung von Daten aus der Kaufpreissammlung und aus der Datensammlung über vereinbarte Nutzungsentgelte

4

Auswertungen des Oberen Gutachterausschusses

5

Auskünfte und Tätigkeiten nach Zeitaufwand

6

Bereitstellung der Bodenrichtwerte

7

Grundstücksmarktberichte

8

Sonstiges

Tarifstelle

Gegenstand

Gebühr

**1**

**Erstattung von Gutachten und Obergutachten sowie Zustandsfeststellungen**

 

 

Ist die Gebühr wertabhängig, wird der im Gutachten für das Bewertungsobjekt ermittelte Wert der Gebührenberechnung zugrunde gelegt.
Folgende Ausnahmen sind zu berücksichtigen:

1.  Sind im Gutachten für ein und dasselbe Bewertungsobjekt mehrere Werte (z.
    B.
    Anfangs- und Endwert, Werte zu mehreren Stichtagen) zu ermitteln, so ist die Summe dieser Werte der Gebühr zugrunde zu legen.
2.  Ist es zur Erstattung eines Gutachtens zwingend erforderlich, zusätzlich zu dem beantragten Wert weitere nicht ausdrücklich beantragte Werte zu ermitteln, so ist die Summe dieser Werte der Gebühr zugrunde zu legen.
    Dies gilt auch, wenn für die Ermittlung des Wertes eines Erbbaurechts zusätzlich der Wert des Grund und Bodens ermittelt werden muss.
3.  Sind in einem Gutachten auch Rechte Dritter zu bewerten, die den zu ermittelnden Wert des Grundstücks oder Rechtes mindern, so ist der Gebühr die Summe der Werte des unbelasteten Grundstücks oder Rechtes und der wertmindernden fremden Rechte zugrunde zu legen, auch wenn die Ermittlung der wertmindernden Rechte selbst nicht ausdrücklich beantragt war.
4.  Sind in einem Gutachten Liquidationsobjekte zu bewerten, ist der Gebühr die Summe des Wertes des fiktiv unbebauten Grundstücks und der Freilegungskosten zugrunde zu legen.
5.  Ist bei der Ermittlung des Wertes eines Grundstücksteils auch das Reststück einzubeziehen (Differenzmethode), so ist der Gesamtwert des Grundstücks der Gebühr zugrunde zu legen.
6.  Bei der Erstattung eines Gutachtens mit Bruchteilseigentum ist der Gesamtwert des Grundstücks der Gebühr zugrunde zu legen.
7.  Beziehen sich mehrere, von einem Antragsteller beantragte Gutachten auf verschiedene Bewertungsobjekte mit nahezu gleichen wertbestimmenden Merkmalen, so ist der Gebühr die Summe der Werte zugrunde zu legen.
8.  Ist ein Gutachten für mehrere Rechte, die ein und dasselbe Grundstück betreffen, zu erstatten, so ist die Summe ihrer Werte der Gebühr zugrunde zu legen.

 

1.1

Gutachten über

*   bebaute Grundstücke
*   Rechte an Grundstücken

 

1.  bei einem Wert bis 250 000 EUR

0,3 Prozent des Wertes zuzüglich 850 EUR

2.  bei einem Wert über 250 000 EUR bis 500 000 EUR

0,2 Prozent des Wertes zuzüglich 1 100 EUR

3.  bei einem Wert über 500 000 EUR

0,08 Prozent des Wertes zuzüglich 1 700 EUR

1.2

Gutachten über

*   unbebaute Grundstücke
*   den Bodenwertanteil eines bebauten Grundstücks

 

1.  bei einem Wert bis 250 000 EUR

0,2 Prozent des Wertes zuzüglich 750 EUR

2.  bei einem Wert über 250 000 EUR bis 500 000 EUR

0,15 Prozent des Wertes zuzüglich 875 EUR

3.  bei einem Wert über 500 000 EUR

0,06 Prozent des Wertes zuzüglich 1 325 EUR

1.3

Gutachten und Zustandsfeststellungen in Enteignungsverfahren

 

1.3.1

Gutachten im Enteignungsverfahren über

*   bebaute Grundstücke
*   Rechte an Grundstücken

Gebühr nach Tarifstelle 1.1 zuzüglich 200 EUR

1.3.2

Gutachten im Enteignungsverfahren über

*   unbebaute Grundstücke
*   den Bodenwertanteil eines bebauten Grundstücks

Gebühr nach Tarifstelle 1.2 zuzüglich 200 EUR

1.3.3

Gutachten über die Höhe anderer Vermögensvor- oder -nachteile

Gebühr nach Tarifstelle 1.2

1.3.4

Zustandsfeststellungen bei vorzeitiger Besitzeinweisung

 

1.  für ein unbebautes Grundstück

750 EUR

2.  für ein bebautes Grundstück

850 EUR

1.3.5

Anhörung des Gutachterausschusses bei Verhandlungen vor der Enteignungsbehörde einschließlich der erforderlichen Fahrt- und Wartezeiten, je angefangene Viertelstunde

19 EUR

1.4

Gutachten über

*   die ortsübliche Pacht im erwerbsmäßigen Obst- und Gemüseanbau gemäß § 5 Absatz 2 BKleingG
*   das ortsübliche Nutzungsentgelt für vergleichbar genutzte Grundstücke gemäß § 7 Absatz 1 Satz 1 NutzEV

750 EUR

1.5

Gutachten über Miet- und Pachtwerte

1 000 EUR

1.6

Ist die Gutachtenerstattung nach den Tarifstellen 1.1 bis 1.5 mit deutlich geringerem Aufwand möglich und wird dieser durch den Antragsteller veranlasst (z.
B.
bei der Ermittlung von Anfangs- oder Endwerten auf der Basis besonderer Bodenrichtwerte, bei der Erstattung von mehreren nach verschiedenen Tarifstellen abzurechnenden Gutachten für dasselbe Bewertungsobjekt, bei der Fortschreibung eines vom Gutachterausschuss erstatteten Gutachtens auf einen späteren Bewertungsstichtag bei gleichbleibenden wertbeeinflussenden Merkmalen), so ist die Gebühr unter Berücksichtigung dieses geringeren Aufwands wie folgt festzusetzen:

50 bis 90 Prozent der Gebühr nach den Tarifstellen 1.1 bis 1.5

1.7

Sind im Zusammenhang mit der Gutachtenerstattung nach den Tarifstellen 1.1 bis 1.5 deutlich über den üblichen Rahmen hinausgehende Mehrarbeiten erforderlich (z.
B.
für örtliche Bauaufnahmen wegen fehlender oder nicht verwertbarer Bauunterlagen; für die Untersuchung von gravierenden Mängeln am Wertermittlungsobjekt, bei Auseinandersetzung mit Grundsatzfragen der Wertermittlung), so ist die Gebühr unter Berücksichtigung dieses Mehraufwands wie folgt festzusetzen:

110 bis175 Prozent der Gebühr nach den Tarifstellen 1.1 bis 1.5

1.8

Erstattung von Obergutachten

150 Prozent der Gebühr nach den Tarifstellen 1.1 bis 1.7

1.9

Farbkopien und -ausdrucke für Gutachten und Obergutachten je Seite

 

1.  DIN A4

1,50 EUR

2.  DIN A3

2 EUR

**2**

**Ermittlung von Bodenrichtwerten**

 

2.1

Ermittlung von Bodenrichtwerten gemäß § 196 Absatz 1 Satz 6 und Absatz 2 BauGB

gebührenfrei

2.2

Ermittlung von besonderen Bodenrichtwerten gemäß § 196 Absatz 1 Satz 7 BauGB

 

1.  je Bodenrichtwert

400 EUR

2.  Mindestgebühr je Antrag

800 EUR

2.3

Anpassung von besonderen Bodenrichtwerten an die allgemeinen Wertverhältnisse, je Anpassung

 

1.  je Bodenrichtwert

40 EUR

2.  Mindestgebühr je Antrag

80 EUR

**3**

**Bereitstellung von Daten aus der Kaufpreissammlung und aus der Datensammlung über vereinbarte Nutzungsentgelte**

 

3.1

Erteilung von Auskünften aus der Kaufpreissammlung

 

1.  Grundgebühr je Auskunft mit bis zu zehn Vergleichsfällen für ein unbebautes Grundstück

65 EUR

2.  Grundgebühr je Auskunft mit bis zu zehn Vergleichsfällen für ein bebautes Grundstück

80 EUR

3.  je weiteren Vergleichsfall

5 EUR

3.2

abschließende Auskunft darüber, dass keine Vergleichsfälle vorhanden sind (Negativauskunft)

 

1.  Negativauskunft

30 EUR

2.  Negativauskunft für das zuständige Finanzamt für Zwecke der steuerlichen Bewertung

gebührenfrei

3.3

Auswertungen und summarische Auskünfte aus der Kaufpreissammlung

50 bis 2 000 EUR

3.4

Weitergabe von Daten aus der Kaufpreissammlung für wissenschaftliche Zwecke  
  
Anmerkung:  
  
Wissenschaftliche Zwecke liegen nicht vor, wenn die Nutzung der Ergebnisse aus den wissenschaftlichen Untersuchungen überwiegend kommerziellen Zwecken dienen soll.

 

1.  für die ersten 200 Vergleichsfälle

140 EUR

2.  für je weitere angefangene 200 Vergleichsfälle

70 EUR

3.5

Erteilung von flächendeckenden Auskünften aus der Datensammlung über vereinbarte Nutzungsentgelte gemäß § 7 Absatz 1 Satz 2 NutzEV

 

1.  für bis zu drei Gemarkungen

38 EUR

2.  für mehr als drei Gemarkungen

63 EUR

**4**

**Auswertungen des Oberen Gutachterausschusses**

 

Überregionale Auswertungen und Analysen sowie Bereitstellung von Daten von Objekten, die bei den Gutachterausschüssen nur vereinzelt vorhanden sind

 

1.  auf Antrag eines Gutachterausschusses

gebührenfrei

2.  auf Antrag Dritter

50 bis 4 000 EUR

**5**

**Auskünfte und Tätigkeiten nach Zeitaufwand**

 

5.1

Erteilung von mündlichen Auskünften über Bodenrichtwerte, aus dem Grundstücksmarktbericht oder von sonstigen Auskünften

 

1.  die erste Viertelstunde

gebührenfrei

2.  je angefangene weitere Viertelstunde

15 EUR

5.2

Erteilung von schriftlichen und elektronischen Auskünften

1.  über Bodenrichtwerte  
    (Enthält die Bodenrichtwertauskunft einen Auszug aus der Bodenrichtwertkarte, ist dieser bis zu einem Format von DIN A4 in der Gebühr enthalten.)
2.  aus dem Grundstücksmarktbericht, insbesondere über die sonstigen für die Wertermittlung erforderlichen Daten und über Miet- und Pachtwerte

je angefangene Viertelstunde

15 EUR

5.3

Mitteilung der Bodenrichtwerte an das zuständige Finanzamt zum Zwecke der steuerlichen Bewertung

gebührenfrei

5.4

fachliche Äußerungen über Grundstückswerte für Behörden zur Erfüllung ihrer Aufgaben

 

je angefangene Viertelstunde

15 EUR

5.5

Mitwirkung bei der Erstellung des Mietspiegels

 

je angefangene Viertelstunde

15 EUR

**6**

**Bereitstellung der Bodenrichtwerte**

 

6.1

historische Bodenrichtwertkarte (Druckexemplar auf der Grundlage topografischer Kartenwerke bis Jahrgang 2009)

30 EUR

6.2

Bodenrichtwertdaten, die von den Nutzerinnen und Nutzern über automatisierte Verfahren abgerufen werden

gebührenfrei

6.3

Auszug aus der Bodenrichtwertkarte (Darstellung der Bodenrichtwerte auf Kartengrundlagen) im Papierformat bis

 

1.  DIN A3

20 EUR

2.  DIN A2

30 EUR

3.  DIN A1

40 EUR

4.  DIN A0

50 EUR

6.4

Auszug aus der Bodenrichtwertkarte (Darstellung der Bodenrichtwerte auf Kartengrundlagen) als PDF-Datei im Seitenformat bis

 

1.  DIN A3

20 EUR

2.  DIN A2

24 EUR

3.  DIN A1

30 EUR

4.  DIN A0

37 EUR

6.5

automatisierte Ansicht von Bodenrichtwerten und automatisierter Abruf von Bodenrichtwertinformationen im PDF-Format aus dem Bodenrichtwert-Portal

gebührenfrei

**7**

**Grundstücksmarktberichte**

 

7.1

Abruf als PDF-Dokument über automatisierte Verfahren

gebührenfrei

7.2

individuelle Bereitstellung des Grundstücksmarktberichts für den Zuständigkeitsbereich eines Gutachterausschusses oder des Oberen Gutachterausschusses in gedruckter Form

40 EUR

7.3

Mitteilung der vollständigen Grundstücksmarktberichte für das zuständige Finanzamt zum Zwecke der steuerlichen Bewertung für

 

1.  den Bereich eines Gutachterausschusses

gebührenfrei

2.  das Land Brandenburg

gebührenfrei

**8**

**Sonstiges**

 

8.1

Amtshandlungen, für die keine andere Tarifstelle vorgesehen ist und die nicht einem vom Gutachterausschuss wahrzunehmenden besonderen öffentlichen Interesse dienen

15 bis 500 EUR

8.2

Zurückweisung und teilweise Zurückweisung von Drittwidersprüchen

15 bis 500 EUR