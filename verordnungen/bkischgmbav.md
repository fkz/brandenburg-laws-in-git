## Verordnung über den Mehrbelastungsausgleich zum Bundeskinderschutzgesetz (Bundeskinderschutzgesetz-Mehrbelastungsausgleichsverordnung - BKiSchGMBAV)

Auf Grund des § 25 Absatz 4 Satz 2 des Ersten Gesetzes zur Ausführung des Achten Buches Sozialgesetzbuch – Kinder- und Jugendhilfe in der Fassung der Bekanntmachung vom 26. Juni 1997 (GVBl.
I S. 87), der durch Artikel 1 des Gesetzes vom 5.
Dezember 2013 (GVBl.
I Nr. 43) eingefügt worden ist, verordnet die Landesregierung:

#### § 1 Anwendungsbereich

Diese Verordnung regelt den Ausgleich der Mehrbelastungen gemäß Artikel 97 Absatz 3 Satz 2 und 3 der Verfassung des Landes Brandenburg, die den Landkreisen und kreisfreien Städten aufgrund der Aufgaben aus dem Bundeskinderschutzgesetz vom 22.
Dezember 2011 (BGBl.
I S.
2975) entstehen.
Mit diesem Ausgleich wird bei den örtlichen Trägern der Jugendhilfe sichergestellt, dass die sich aus Artikel 2 des Bundeskinderschutzgesetzes ergebenden pflichtigen Aufgaben unabhängig von dem tatsächlichen Fallzahlaufwuchs und die sich aus Artikel 1 des Bundeskinderschutzgesetzes ergebenden pflichtigen Aufgaben, die nicht durch Bundesmittel gedeckt werden, umgesetzt werden können.
Die pflichtigen Aufgaben beziehen sich insbesondere auf

1.  die Ausweitung der strukturellen Vernetzung, einer Verbesserung der Handlungssicherheit durch Beratungsansprüche im Einzelfall sowie den für den Einzelfall erforderlichen Informations- und Datenaustausch,
2.  Maßnahmen zur kontinuierlichen Qualitätsentwicklung und Qualitätssicherung sowie
3.  eine Verbesserung der Qualität der Datenerhebung und Erweiterung der Datenbasis, insbesondere zu Führungszeugnissen und Statistiken zur Erfassung von Gefährdungseinschätzungen.

#### § 2 Höhe des Mehrbelastungsausgleichs

(1) Zum Ausgleich der Mehrbelastungen, die für die Durchführung der in § 1 genannten Aufgaben entstehen, erstattet das Land den Landkreisen und kreisfreien Städten ab 2021 die Kosten für den Einsatz von insgesamt 45 Fachkraftstellen zuzüglich eines Sachkostenzuschlags von 25 Prozent.
Die Bemessung der Personalkosten richtet sich nach dem für die Vereinigung der kommunalen Arbeitgeberverbände geltenden Tarifvertrag für den öffentlichen Dienst, Entgeltordnung für den Sozial- und Erziehungsdienst, Eingruppierungsmerkmal für die Entgeltgruppe S12 Stufe 5, einschließlich aller vom Arbeitgeber zu tragenden Entgeltbestandteile und Nebenkosten (Arbeitgeberbrutto).

(2) Bei der Ermittlung des Arbeitgeberbruttos nach Absatz 1 ist von der Tarifhöhe am 1. Januar des jeweiligen Ausgleichsjahres auszugehen.
Zu diesem Zeitraum bereits feststehende Tarifveränderungen sind dabei zu berücksichtigen.

#### § 3 Verteilung des Mehrbelastungsausgleichsbetrages

Maßgeblich für die Verteilung des Mehrbelastungsausgleichs gemäß § 2 auf die Landkreise und kreisfreien Städte ist je zur Hälfte

1.  die Anzahl der Kinder und Jugendlichen im Sinne von § 7 Absatz 1 Nummer 1 und 2 des Achten Buches Sozialgesetzbuch (Minderjährige) gemäß der amtlichen Statistik des Amtes für Statistik Berlin-Brandenburg zum 31.
    Dezember des jeweils vorletzten Jahres vor dem Ausgleichsjahr sowie
2.  die Anzahl der Minderjährigen, die am 31.
    Dezember des jeweils vorletzten Jahres vor dem Ausgleichsjahr nach § 19 Absatz 1 Satz 1 und 2 des Zweiten Buches Sozialgesetzbuch leistungsberechtigt waren; maßgeblich sind die Daten der amtlichen Statistik der Bundesagentur für Arbeit.

#### § 4 Fälligkeit des Mehrbelastungsausgleichs

Der Mehrbelastungsausgleich nach § 2 ist zum 1.
Mai des jeweiligen Ausgleichsjahres fällig.

#### § 5 Revisionsklausel und Härtefallregelung

(1) Auf Verlangen der kommunalen Spitzenverbände oder der obersten Landesjugendbehörde können erstmals 2024 und danach alle vier Jahre die Mehrbelastungen daraufhin überprüft werden, ob der Mehrbelastungsausgleich anzupassen ist.
Dazu teilen die örtlichen Träger der Jugendhilfe der obersten Landesjugendbehörde die Anzahl der im ablaufenden Zeitraum zur Durchführung der Aufgaben nach § 1 eingesetzten Stellen (Vollzeitäquivalente), die nach Einführung der neuen und erweiterten Aufgaben nach dem Bundeskinderschutzgesetz seit dem Jahr 2012 zusätzlich bei den Landkreisen und kreisfreien Städten entstanden sind und deren tarifliche Eingruppierung mit.
Die örtlichen Träger der öffentlichen Jugendhilfe geben Auskunft über die Mehrbelastungen sowie deren prozentuale Verteilung in Bezug auf die in § 1 genannten Aufgaben.
Die oberste Landesjugendbehörde prüft die mitgeteilten Angaben nach einem Prüfkonzept, zu dem das Benehmen mit den kommunalen Spitzenverbänden herzustellen ist.
Die örtlichen Träger der Jugendhilfe stellen die dazu erforderlichen zusätzlichen Informationen und Unterlagen zur Verfügung.
Die oberste Landesjugendbehörde stellt auf dieser Grundlage die durchschnittlichen Mehrbelastungen in dem darauffolgenden Vierjahreszeitraum fest.
Der Ausgleich erfolgt entsprechend § 3.

(2) Die Landkreise und kreisfreien Städte können beim Land für das laufende Jahr binnen eines Monats nach der Fälligkeit der Zahlung nach § 4 den Ausgleich nachgewiesener und erforderlicher Mehrbelastungen im Sinne des § 1 beantragen, soweit diese nicht bereits gemäß den §§ 2 und 3 ausgeglichen sind.

#### § 6 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über den Mehrbelastungsausgleich zum Bundeskinderschutzgesetz vom 11.
November 2015 (GVBl. II Nr. 57) außer Kraft.

Potsdam, den 18.
Dezember 2020

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Die Ministerin für Bildung, Jugend und Sport

Britta Ernst