## Verordnung über den Umfang der Lehrverpflichtung des hauptberuflich tätigen wissenschaftlichen und künstlerischen Personals an den staatlichen Hochschulen des Landes Brandenburg (Lehrverpflichtungsverordnung - LehrVV)

Auf Grund des § 50 des Brandenburgischen Hochschulgesetzes vom 28.
April 2014 (GVBl.
I Nr. 18) verordnet die Ministerin für Wissenschaft, Forschung und Kultur:

#### § 1 Anwendungsbereich

Diese Verordnung gilt für das hauptberufliche wissenschaftliche und künstlerische Personal (Lehrpersonen) nach § 39 des Brandenburgischen Hochschulgesetzes mit Lehraufgaben an den staatlichen Hochschulen des Landes Brandenburg im Sinne des § 2 Absatz 1 des Brandenburgischen Hochschulgesetzes.
Sie findet Anwendung auf Lehrveranstaltungen im grundständigen, im postgradualen und im weiterbildenden Studium.

#### § 2 Lehrverpflichtung

(1) Der Umfang der Lehrverpflichtung wird in Lehrveranstaltungsstunden (LVS) ausgedrückt.
Eine Lehrveranstaltungsstunde umfasst mindestens 45 Minuten Lehrzeit pro Woche der Vorlesungszeit des Semesters.
Bei künstlerischem Einzel- und Gruppenunterricht umfasst eine Lehrveranstaltungsstunde mindestens 60 Minuten.
Lehrveranstaltungen, die nicht in Wochenstunden je Semester ausgedrückt sind, sind entsprechend umzurechnen.

(2) Lehrveranstaltungen sind vorzugsweise von Professorinnen und Professoren durchzuführen.

(3) Die Dekanin oder der Dekan entscheidet über den Umfang der Lehrverpflichtung nach näherer Bestimmung in den §§ 3 bis 7.
Sie oder er entscheidet nach pflichtgemäßem Ermessen und im Einvernehmen mit der Präsidentin oder dem Präsidenten über eine Anrechnung von Lehrveranstaltungen auf die Lehrverpflichtung.

(4) Soweit Lehrpersonen in zentralen Einrichtungen tätig sind, entscheidet abweichend von Absatz 3 sowie § 8 Absatz 2 bis 4 die Präsidentin oder der Präsident.

#### § 3 Lehrverpflichtung der Hochschullehrerinnen und Hochschullehrer an Universitäten

(1) An den Universitäten beträgt die Regellehrverpflichtung der

1.  Professorinnen und Professoren 8 LVS,
2.  Professorinnen und Professoren mit Schwerpunkt in der Lehre 10 bis 12 LVS,
3.  Juniorprofessorinnen und Juniorprofessoren 4 bis 6 LVS,
4.  Juniorprofessorinnen und Juniorprofessoren mit Schwerpunkt in der Lehre 6 bis 8 LVS.

(2) Professorinnen und Professoren können gemäß der Funktionsbeschreibung ihrer Stellen von der Dekanin oder dem Dekan befristet überwiegend mit Lehrtätigkeit betraut werden.
Sie haben eine Lehrverpflichtung von bis zu 14 LVS.
Professorinnen und Professoren können gemäß der Ausgestaltung ihres Dienstverhältnisses und der Funktionsbeschreibung ihrer Stellen von der Dekanin oder dem Dekan befristet ausschließlich oder überwiegend mit Forschungstätigkeit betraut werden.
Die Funktionsbeschreibung der Stelle und die entsprechende Lehrverpflichtung sind spätestens nach vier Semestern zu überprüfen.

(3) Für Lehrkräfte nach Absatz 1 mit künstlerischer Lehrtätigkeit gilt § 5 Absatz 1 Nummer 2 und 3 entsprechend.

#### § 4 Lehrverpflichtung der Hochschullehrerinnen und Hochschullehrer an der Brandenburgischen Technischen Universität Cottbus-Senftenberg

(1) Für Professorinnen und Professoren an der Brandenburgischen Technischen Universität Cottbus-Senftenberg gelten die Regellehrverpflichtungen gemäß § 6, wenn sie

1.  bis zum 30.
    Juni 2013 als Professor an der Hochschule Lausitz (FH) berufen waren und die Dienstaufgaben nicht nach § 6 Absatz 2 des Gesetzes zur Weiterentwicklung der Hochschulregion Lausitz angeglichen worden sind oder
2.  als Professor mit Schwerpunkt in der Forschung nach § 6 Absatz 4 des Gesetzes zur Weiterentwicklung der Hochschulregion Lausitz eingestellt sind.

(2) Bei nach dem 30.
Juni 2013 berufenen Professorinnen und Professoren für anwendungsbezogene Studiengänge im Sinne des § 41 Absatz 3 Satz 2 des Brandenburgischen Hochschulgesetzes beträgt die Regellehrverpflichtung, je nach Anforderung der Stelle, 15 bis 18 LVS.
Bei Professorinnen und Professoren nach Satz 1, die nach § 6 Absatz 3 des Gesetzes zur Weiterentwicklung der Hochschulregion Lausitz berufen werden und die die Einstellungsvoraussetzungen nach § 41 Absatz 1 Nummer 4 Buchstabe a und b des Brandenburgischen Hochschulgesetzes erfüllen müssen, beträgt die Regellehrverpflichtung 9 bis 12 LVS.

(3) An der Brandenburgischen Technischen Universität Cottbus-Senftenberg kann die Regellehrverpflichtung im Einzelfall oder für bestimmte Gruppen von Lehrpersonen durch den Gründungspräsidenten oder die Präsidentin oder den Präsidenten im Benehmen mit der Dekanin oder dem Dekan abweichend festgelegt werden.
Der Gründungspräsident oder die Präsidentin oder der Präsident erlässt dazu im Benehmen mit dem in der Grundordnung bestimmten Organ der Brandenburgischen Technischen Universität Cottbus-Senftenberg eine Richtlinie, die die Grundsätze für die Festlegung der Lehrverpflichtung bestimmt.
Die Richtlinie bedarf der Genehmigung der für die Hochschulen zuständigen obersten Landesbehörde.

(4) Bei der Festlegung der Regellehrverpflichtung nach Absatz 3 sind die Leistungen in der Lehre, der Forschung und im Wissens- und Technologietransfer zu berücksichtigen, die an der Brandenburgischen Technischen Universität Cottbus-Senftenberg erbracht werden, sowie der besondere Einsatz in den zentralen wissenschaftlichen Einrichtungen nach § 3 Absatz 1 des Gesetzes zur Weiterentwicklung der Hochschulregion Lausitz.

(5) Die Festlegungen nach Absatz 3 können zeitlich befristet getroffen werden.
Die Regellehrverpflichtung nach § 3 Absatz 1 darf nicht unterschritten werden.
§ 9 Absatz 1 gilt entsprechend.
§ 8 bleibt unberührt.

#### § 5 Lehrverpflichtung der Hochschullehrerinnen und Hochschullehrer an der Filmuniversität Babelsberg Konrad Wolf

(1) An der Filmuniversität Babelsberg Konrad Wolf beträgt die Regellehrverpflichtung

1.  der Professorinnen und Professoren mit Lehrtätigkeit in wissenschaftlichen Fächern 8 LVS,
2.  der Professorinnen und Professoren mit Lehrtätigkeit in Fächern mit wissenschaftlichen und künstlerischen oder wissenschaftlichen und anwendungsbezogenen Anteilen 12 LVS,
3.  der Professorinnen und Professoren mit Lehrtätigkeit in künstlerischen oder anwendungsbezogenen Fächern 18 LVS,
4.  der Juniorprofessorinnen und Juniorprofessoren 4 bis 6 LVS,
5.  der Juniorprofessorinnen und Juniorprofessoren mit Schwerpunkt in der Lehre 6 bis 8 LVS.

(2) § 3 Absatz 2 gilt entsprechend, soweit die Professorin oder der Professor Lehrtätigkeit in wissenschaftlichen Fächern nach Absatz 1 Nummer 1 ausübt.

(3) Die Präsidentin oder der Präsident kann Professorinnen und Professoren mit Lehrtätigkeit in  
künstlerischen Fächern im Sinne von Absatz 1 Nummer 3 sowie Professorinnen und Professoren mit  
Lehrtätigkeit in Fächern mit wissenschaftlichen und künstlerischen Anteilen im Sinne von Absatz 1  
Nummer 2 vorübergehend überwiegend Aufgaben in der künstlerischen Forschung übertragen, wenn  
innerhalb der zuständigen Lehreinheit in angemessener Weise die Verringerung des bisherigen Lehrangebots ausgeglichen wird und die Wahrnehmung der sonstigen Verpflichtungen sichergestellt ist.
Der Anteil von Professorinnen und Professoren mit einer Lehrermäßigung nach Satz 1 darf 20 Prozent der Gesamtzahl der W 2- und W 3-Stellen der Hochschule, entsprechend der genehmigten Personalentwicklungsplanung, nicht übersteigen.
Der Umfang der Lehrverpflichtung für die in Satz 1 genannten Professorinnen und Professoren darf jeweils 9 LVS nicht unterschreiten.
Die Ermäßigung darf längstens für sechs Semester gewährt werden.
Diese Regelung tritt mit Ablauf des 30. September 2025 außer Kraft.

#### § 6 Lehrverpflichtung der Hochschullehrerinnen und Hochschullehrer an Fachhochschulen

An Fachhochschulen beträgt die Regellehrverpflichtung der Professorinnen und Professoren 18 LVS.
Die Lehrverpflichtung der Professorinnen und Professoren mit Schwerpunkt in der Forschung beträgt 9 bis 12 LVS.

#### § 7 Lehrverpflichtung der Akademischen Mitarbeiterinnen und Mitarbeiter

(1) Akademische Mitarbeiterinnen und Mitarbeiter haben eine Lehrverpflichtung von bis zu 24 LVS.

(2) Die ausschließliche Übertragung von Lehr- oder Forschungsaufgaben auf Akademische Mitarbeiterinnen und Mitarbeiter ist nur im begründeten Ausnahmefall zulässig.
Begründete Ausnahmefälle im Bereich der Lehre sind insbesondere Sprach- und Sportlehrerinnen und Sprach- und Sportlehrer.
Für Akademische Mitarbeiterinnen und Mitarbeiter an Fachhochschulen gilt dies mit der Maßgabe, dass eine ausschließliche Übertragung von Lehraufgaben auf Akademische Mitarbeiterinnen und Mitarbeiter nur als Ausgleich für eine Absenkung der Lehrverpflichtung von Professorinnen und Professoren mit Schwerpunkt in der Forschung zulässig ist.

#### § 8 Ausgleich und Ermäßigung der Lehrverpflichtung

(1) Die Präsidentin oder der Präsident kann die Lehrverpflichtung ermäßigen bei

1.  Vizepräsidentinnen und Vizepräsidenten
    
    1.  an Hochschulen mit mehr als 10 000 Studierenden um insgesamt höchstens 225 Prozent der Lehrverpflichtung einer Professorin oder eines Professors,
    2.  an Hochschulen mit mehr als 2 500 Studierenden um insgesamt höchstens 150 Prozent der Lehrverpflichtung einer Professorin oder eines Professors,
    3.  an Hochschulen mit bis zu 2 500 Studierenden um insgesamt höchstens 100 Prozent der Lehrverpflichtung einer Professorin oder eines Professors,
    4.  an der Filmuniversität Babelsberg Konrad Wolf um insgesamt höchstens 75 Prozent der Lehrverpflichtung einer Professorin oder eines Professors gemäß § 5 Absatz 1 Nummer 2;
    
    anteilige Ermäßigungen für die einzelnen Vizepräsidentinnen oder Vizepräsidenten erfolgen nach Maßgabe des Umfangs der übertragenen Aufgabe,
    
2.  Dekaninnen und Dekanen um bis zu 50 Prozent; an Fachbereichen mit mehr als 500 Studierenden kann eine zusätzliche Ermäßigung von bis zu 20 Prozent und im Fall einer Mitgliedschaft im Präsidialkollegium von zusätzlich bis zu 5 Prozent gewährt werden,
3.  Vorsitzenden des Senates um bis zu 25 Prozent der jeweiligen Lehrverpflichtung.

(2) Die Dekanin oder der Dekan kann gestatten, dass eine Lehrperson ihre Lehrverpflichtung im Durchschnitt zweier aufeinander folgender akademischer Jahre erfüllt oder mehrere Lehrpersonen einer Lehreinheit ihre Lehrverpflichtung innerhalb des jeweiligen Semesters ausgleichen.
Die Lehrtätigkeit der einzelnen Lehrperson in einem Semester darf in diesen Fällen die Hälfte der jeweiligen Lehrverpflichtung nicht unterschreiten.
Professorinnen und Professoren dürfen nur untereinander ausgleichen.

(3) Die Dekanin oder der Dekan entscheidet nach pflichtgemäßem Ermessen und im Einvernehmen mit der Präsidentin oder dem Präsidenten über eine Ermäßigung der Lehrverpflichtung.
Ermäßigungstatbestände können insbesondere sein

1.  die überdurchschnittliche Belastung durch die Betreuung von Studienabschlussarbeiten,
2.  Besonderheiten in einzelnen Fachgebieten, insbesondere ein geringer Lehrbedarf oder ein Überangebot in der Lehre,
3.  der überdurchschnittliche Aufwand für die Vor- und Nachbereitung bei der Entwicklung und beim Einsatz neuer, innovativer Lehrangebote,
4.  Lehrleistungen in der nicht durch Studien- oder Prüfungsordnungen geregelten Weiterbildung sowie im Fernstudium,
5.  die Tätigkeit als Studienfachberater, die Wahrnehmung von Aufgaben der Studienreform und der Sprecherfunktion in Sonderforschungsbereichen,
6.  das Ausmaß der Wahrnehmung von Aufgaben des Innovations- und Technologietransfers,
7.  an Fachhochschulen das Ausmaß der Wahrnehmung von Forschungs- und Entwicklungsaufgaben,
8.  die Wahrnehmung von Aufgaben im öffentlichen Interesse außerhalb der Hochschule, die die Ausübung der Lehrtätigkeit ganz oder teilweise ausschließen,
9.  die Wahrnehmung von Aufgaben, die nach Art und Umfang von der Hochschulverwaltung nicht übernommen werden können und deren Übernahme zusätzlich zur Lehrverpflichtung wegen der damit verbundenen Belastung nicht zumutbar ist.

(4) Die Dekanin oder der Dekan kann die Lehrverpflichtung schwerbehinderter Lehrpersonen auf Antrag bei einem Grad der Behinderung

1.  von mindestens 50 Prozent um bis zu 12 Prozent,
2.  von mindestens 70 Prozent um bis zu 18 Prozent,
3.  von mindestens 90 Prozent um bis zu 25 Prozent ermäßigen.

#### § 9 Verfahren bei Ausgleich und Ermäßigung der Lehrverpflichtung

(1) Entscheidungen nach § 8 dürfen nur ergehen, wenn das nach den Prüfungs- und Studienordnungen vorgesehene Gesamtlehrangebot der Hochschule in jedem Semester erfüllt wird.

(2) Für Entscheidungen nach § 8 Absatz 3 stehen bei den Universitäten maximal 2,5 Prozent, bei der Brandenburgischen Technischen Universität Cottbus-Senftenberg maximal 6 Prozent, bei der Filmuniversität Babelsberg Konrad Wolf maximal 2,5 Prozent sowie bei den Fachhochschulen maximal 7 Prozent der Gesamtzahl aller Lehrverpflichtungen der hauptberuflich tätigen Lehrpersonen zur Verfügung.

#### § 10 Lehrverpflichtung an einer weiteren Hochschule

(1) Lehrpersonen können von dem für die Hochschulen zuständigen Mitglied der Landesregierung im Benehmen mit der Präsidentin oder dem Präsidenten der abgebenden Hochschule verpflichtet werden, Lehr- und Prüfungsaufgaben an einer weiteren Hochschule zu erbringen (§ 50 Satz 2 des Brandenburgischen Hochschulgesetzes).
Die Präsidentin oder der Präsident der aufnehmenden Hochschule ist vor der Entscheidung zu hören.

(2) Lehrpersonen, die an verschiedenen Lehrorten des Landes Brandenburg eingesetzt werden, sollen auf Antrag angemessen entlastet werden.
Über den Umfang der Entlastung entscheidet die Präsidentin oder der Präsident der abgebenden Hochschule im Einvernehmen mit der Präsidentin oder dem Präsidenten der aufnehmenden Hochschule.

#### § 11 Erfüllung der Lehrverpflichtung; Berichtspflicht

(1) Die Lehrpersonen haben der Dekanin oder dem Dekan jeweils am Ende eines Semesters zur Erfüllung ihrer Lehrverpflichtung zu berichten.
Über den Umfang der Berichtspflicht entscheidet die Dekanin oder der Dekan.
Sie oder er nimmt die Angaben der Lehrpersonen in den nach § 73 Absatz 4 Satz 2 des Brandenburgischen Hochschulgesetzes zu erstellenden Lehrbericht auf.

(2) Die Präsidentin oder der Präsident berichtet dem für die Hochschulen zuständigen Mitglied der Landesregierung jährlich zum 31.
Oktober schriftlich und geordnet nach Personalkategorien und Lehreinheiten über die nach § 8 getroffenen Entscheidungen.

#### § 12 Übergangsbestimmungen

Für wissenschaftliche und künstlerische Assistentinnen und Assistenten, Oberassistentinnen und Oberassistenten, Oberingenieurinnen und Oberingenieure sowie Hochschuldozentinnen und Hochschuldozenten gelten die Bestimmungen der Lehrverpflichtungsverordnung vom 6. September 2002 (GVBl.
II S. 568), die zuletzt durch Artikel 4 des Gesetzes vom 11. Februar 2013 (GVBl.
I Nr. 4) geändert worden ist, fort.

#### § 13 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Lehrverpflichtungsverordnung vom 6.
September 2002 (GVBl.
II S. 568), die zuletzt durch Artikel 4 des Gesetzes vom 11.
Februar 2013 (GVBl.
I Nr.
4) geändert worden ist, außer Kraft.

Potsdam, den 13.
Januar 2017

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Dr.
Martina Münch