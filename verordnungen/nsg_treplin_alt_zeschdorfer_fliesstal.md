## Verordnung über das Naturschutzgebiet „Treplin - Alt Zeschdorfer Fließtal“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nr.
43) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1   
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Märkisch-Oderland wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Treplin - Alt Zeschdorfer Fließtal“.

### § 2   
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 132 Hektar.
Es umfasst Flächen in folgenden Fluren:

Gemeinde:

Gemarkung:

Flur:

Treplin

Treplin

2, 4;

Zeschdorf

Alt Zeschdorf

1, 3;

Zeschdorf

Petershagen

3;

Zeschdorf

Döbberin

2.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 und 2 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 3 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Innerhalb des Naturschutzgebietes wird gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes eine Zone 1 festgesetzt, in der Lebensräume und Lebensgemeinschaften langfristig ihrer natürlichen Entwicklung überlassen bleiben.
Die Zone 1 umfasst drei Teilflächen mit insgesamt rund 42 Hektar und liegt in folgenden Fluren:

Gemeinde:

Gemarkung:

Flur:

Treplin

Treplin

4;

Zeschdorf

Alt Zeschdorf

1, 3.

Die Grenze der Zone 1 ist in den in Anlage 2 Nummer 1 genannten topografischen Karten mit den Blattnummern 1 und 2 sowie in den in Anlage 2 Nummer 2 genannten Liegenschaftskarten mit den Blattnummern 1 bis 3 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Märkisch-Oderland, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

### § 3   
Schutzzweck

(1) Schutzzweck des im Osten der Lebuser Platte befindlichen Naturschutzgebietes, das eine subglaziale Schmelzwasserrinne mit dem Alt Zeschdorfer Mühlenfließ, begleitende Feuchtlebensräume mit alten Teichanlagen sowie angrenzende überwiegend bewaldete Talhänge umfasst, ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Quellfluren, Röhrichte und Riede, Schwimmblatt- und Tauchfluren, feuchten Staudenfluren, Feucht- und Frischwiesen mit deren Brachestadien, sowie der naturnahen Wälder und Gebüsche wie Erlenbruchwälder, mesophile Edellaubholz-, Eichen-Hainbuchen- und Rotbuchenwälder, bodensaure Eichen-Mischwälder und Weidengebüsche;
    
2.  die Erhaltung und Entwicklung der Lebensräume wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Krebsschere (Stratiotes aloides), Wasserfeder (Hottonia palustris), Gelbe Teichrose (Nuphar lutea), Wasser-Schwertlilie (Iris pseudacorus) und Schlüsselblume (Primula veris);
    
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Fledermäuse, Vögel, Amphibien, Reptilien und Weichtiere, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Großer Abendsegler (Nyctalus noctula), Rauhautfledermaus (Pipistrellus nathusii), Kranich (Grus grus), Eisvogel (Alcedo atthis), Drosselrohrsänger (Acrocephalus arundinaceus), Mittelspecht (Dendrocopus medius), Schwarzspecht (Drycopus martius), Neuntöter (Lanius colurio), Moorfrosch (Rana arvalis), Grasfrosch (Rana temporaria), Wechselkröte (Bufo viridis) und Ringelnatter (Natrix natrix);
    
4.  die Erhaltung der Überreste der alten Trepliner Wassermühle aus landeskundlichen Gründen;
    
5.  die Erhaltung des Gebietes zur Umweltbeobachtung und wissenschaftlichen Untersuchung ökologischer Zusammenhänge;
    
6.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit der Landschaft des Fließtals, die durch mäandrierende, von Feuchtwäldern begleitete Fließgewässer, vegetationsreiche Teiche, gehölzarme Sukzessionsflächen sowie altbaum- und totholzreiche bewaldete Randhänge mit stark gegliederter Geländeoberfläche gekennzeichnet ist;
    
7.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des regionalen Fließgewässer- und Auen-biotopverbundes entlang des Alt Zeschdorfer Mühlenfließes und seiner Zuläufe bis in das Odertal.
    

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Treplin - Alt Zeschdorfer Fließtal“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinen Vorkommen von

1.  Flüssen der planaren Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Feuchten Hochstaudenfluren der planaren Stufe, Subatlantischem oder mitteleuropäischem Stieleichenwald oder Hainbuchenwald (Carpinion betuli) \[Stellario-Carpinetum\] sowie Alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur als natürliche Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
    
2.  Auen-Wäldern mit Alnus glutinosa (Schwarz-Erle) und Fraxinus excelsior (Gewöhnliche Esche) (Alno-Padion) als prioritärer natürlicher Lebensraumtyp im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
    
3.  Fischotter (Lutra lutra), Elbe-Biber (Castor fiber albicus), Rotbauchunke (Bombina bombina), Bauchiger Windelschnecke (Vertigo moulinsiana) und Schmaler Windelschnecke (Vertigo angustior) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.
    

(3) Darüber hinaus ist besonderer Schutzzweck der Zone 1 die störungsarme Entwicklung naturnaher Fließgewässer mit ihren Begleitbiotopen wie Röhrichten, Rieden, feuchten Staudenfluren und Auenwäldern sowie der langfristig ungestörte Ablauf natürlicher Prozesse.

### § 4   
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
    
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
    
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
    
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
    
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
    
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
    
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
    
8.  die Ruhe der Natur durch Lärm zu stören;
    
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 11 jeweils nach dem 31.
    Juli eines jeden Jahres außerhalb der Zone 1 und außerhalb von Feucht- und Moorlebensräumen wie Erlenwäldern, Röhrichten und Rieden;
    
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
    
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
    
12.  zu baden;
    
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen;
    
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
    
15.  Hunde frei laufen zu lassen;
    
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
    
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
    
18.  sonstige Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
    
19.  Tiere zu füttern oder Futter bereitzustellen;
    
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
    
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
    
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
    
23.  Pflanzenschutzmittel jeder Art anzuwenden;
    
24.  Wiesen, Weiden oder sonstiges Grünland umzubrechen oder neu anzusäen.
    

### § 5   
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  Grünland als Wiese oder Weide mit einer Besatzdichte von maximal 1,4 Raufutter verwertenden Großvieheinheiten (RGV) pro Hektar im Jahresmittel genutzt wird und § 4 Absatz 2 Nummer 17 gilt,
        
    2.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 weiter gilt, wobei bei Narbenschäden eine umbruchlose Nachsaat zulässig ist;
        
    3.  Bäume und Feldgehölze in geeigneter Weise gegen Verbiss und sonstige Beschädigungen sowie Ränder von Gewässern wirksam gegen Trittschäden von weidenden Nutztieren geschützt werden;
        
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung außerhalb der Zone 1 auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  nur Arten der jeweils potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Gehölzarten in gesellschaftstypischen Anteilen unter Ausschluss eingebürgerter Arten zu verwenden sind,
        
    2.  auf den Flächen der in § 3 Absatz 2 Nummer 1 und 2 genannten Waldgesellschaften eine Nutzung nur einzelstamm- bis truppweise erfolgt und hydromorphe Böden nur bei Frost sowie Böden mit einem hohen Anteil an feinkörnigem Substrat nur bei Frost oder in Trockenperioden auf dauerhaft festgelegten Rückegassen befahren werden dürfen,
        
    3.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
        
    4.  mindestens fünf Stämme je Hektar mit einem Durchmesser von mehr als 40 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum natürlichen Absterben und Zerfall aus der Nutzung genommen sein müssen,
        
    5.  je Hektar mindestens fünf Stück lebensraumtypische, abgestorbene, stehende Bäume (Totholz) mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden; liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimeter am stärkeren Ende) verbleibt als ganzer Baum im Bestand,
        
    6.  § 4 Absatz 2 Nummer 23 gilt;
        
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung auf den bisher rechtmäßig dafür genutzten Flächen außerhalb der Zone 1 mit der Maßgabe, dass
    
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung von Bibern und Fischottern weitgehend ausgeschlossen ist,
        
    2.  der Fischbesatz nur mit heimischen Arten erfolgt und dabei eine Gefährdung der in § 3 Absatz 2 Nummer 3 genannten Arten ausgeschlossen ist; § 13 der Fischereiordnung des Landes Brandenburg bleibt unberührt,
        
    3.  § 4 Absatz 2 Nummer 19 gilt;
        
4.  innerhalb der Zone 1 erforderliche Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne eines Monitorings mit Genehmigung der unteren Naturschutzbehörde;
    
5.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass diese nur vom Ufer aus und ausschließlich von den in den in § 2 Absatz 2 genannten topografischen Karten gekennzeichneten Stellen am Nordufer des Kleinen Trepliner Sees und entlang des Straßendamms bei Hohenjesar aus erfolgt;
    
6.  für den Bereich der Jagd:
    
    1.   die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Fallenjagd nur mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern von Gewässerufern aus verboten ist.
        Von der Einhaltung dieses Abstandes kann die untere Naturschutzbehörde Ausnahmen erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
        bb)
        
        keine Baujagd in einem Abstand von bis zu 100 Metern zum Ufer der Fließgewässer und der Teiche vorgenommen wird,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Errichtung anzuzeigen.
        Die untere Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        Die Entscheidung hierzu soll unverzüglich erfolgen,
        
    3.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope.
        
    
    Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern sind unzulässig.
    Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
    
7.  die Durchführung biotopeinrichtender Maßnahmen in naturfernen Kiefern- und Robinienbeständen der Zone 1 zur Regeneration standortypischer Wälder bis zum 31. Dezember 2025 mit Genehmigung der Fachbehörde für Naturschutz und Landschaftspflege;
    
8.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter Nummer 9 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
    
9.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, die den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
    
10.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren  
    Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
    
11.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch außerhalb der Zone 1 und außerhalb von Feucht- und Moorlebensräumen wie Erlenwäldern, Röhrichten und Rieden jeweils nach dem 31.
    Juli eines jeden Jahres;
    
12.  die Wiederherstellung von Teilen der ehemaligen Trepliner Mühle zu Schauzwecken sowie die Einrichtung und Nutzung eines Wander-Rastplatzes im Umfeld der Mühlenruine mit Genehmigung der unteren Naturschutzbehörde.
    Die Genehmigung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird;
    
13.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
    
14.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung nach Anzeige gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes bei der unteren Naturschutzbehörde;
    
15.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der zuständigen Naturschutzbehörde zugelassen oder angeordnet worden sind;
    
16.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl. S.
    1734), die durch die Bekanntmachung vom 1.
    Oktober 2013 (ABl.
    S. 2811) geändert worden ist, an Straßen und Wegen freigestellt;
    
17.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

### § 6   
Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege- und Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  das Alt Zeschdorfer Mühlenfließ soll renaturiert und seine ökologische Durchgängigkeit verbessert werden, zum Beispiel durch die Förderung von Seitenerosion in begradigten Abschnitten, die Aufweitung von Wegedurchlässen sowie die Anlage von Umgehungsgerinnen im Bereich von alten Mühlenstauen;
    
2.  aufgelassene Grünlandflächen mit Restvorkommen von typischen Feuchtwiesenarten sollen unter Beachtung der Maßgaben von § 5 Absatz 1 Nummer 1 wieder einer regelmäßigen extensiven Nutzung zugeführt werden;
    
3.  nasse Seggenriede und andere für die Schneckenfauna besonders bedeutsame Offenlandlebensräume sollen durch gelegentliche Entbuschung gehölzarm gehalten werden;
    
4.  Auen- und Bruchwälder außerhalb der Zone 1 sollen aus der forstwirtschaftlichen Nutzung genommen werden;
    
5.  nicht heimische Gehölzarten, wie zum Beispiel Robinie (Robinia pseudoacacia), Späte Traubenkirsche (Prunus serotina), Rosskastanie (Aesculus hippocastanum), Roteiche (Quercus rubra), Hybrid- und Balsam-Pappel (Populus x canadensis, P.
    balsamifera) sowie Gemeine Fichte (Picea abies), Douglasie (Pseudotsuga menziesii) und andere Nadelhölzer sollen bei der forstwirtschaftlichen Flächennutzung aus dem Bestand entnommen werden;
    
6.  die Verjüngung von Waldflächen soll nach Möglichkeit durch Naturverjüngung erfolgen; dazu soll der Schalenwildbestand entsprechend reguliert werden;
    
7.  an Bestandesrändern von Wäldern sollen strukturreiche Waldmäntel aus standortgerechten, gebietsheimischen Bäumen und Sträuchern sowie artenreiche Krautsäume erhalten und entwickelt werden;
    
8.  es sollen geeignete Einrichtungen zur Besucherlenkung und -information geschaffen werden.
    

### § 7   
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

### § 8   
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

### § 9   
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 17 und 18 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 29 Absatz 3 und § 30 des Bundesnaturschutzgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten, ihrer Lebensstätten und Biotope (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 54 Absatz 7 des Bundesnaturschutzgesetzes) unberührt.

### § 10   
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

### § 11   
Inkrafttreten

§ 5 Absatz 1 Nummer 1 Buchstabe a und b tritt am 1.
Januar 2015 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 21.
Oktober 2014

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

* * *

### Anlagen

1

[NSGTreplin-Alt Zeschdorfer Fließtal-Anlg-1](/br2/sixcms/media.php/68/GVBl_II_80_2014-Anlage-1.pdf "NSGTreplin-Alt Zeschdorfer Fließtal-Anlg-1") 686.1 KB

2

[NSGTreplin-Alt Zeschdorfer Fließtal-Anlg-2](/br2/sixcms/media.php/68/GVBl_II_80_2014-Anlage-2.pdf "NSGTreplin-Alt Zeschdorfer Fließtal-Anlg-2") 590.7 KB

3

[NSGTreplin-Alt Zeschdorfer Fließtal-Anlg-3](/br2/sixcms/media.php/68/GVBl_II_80_2014-Anlage-3.pdf "NSGTreplin-Alt Zeschdorfer Fließtal-Anlg-3") 598.6 KB