## Verordnung über die Gebühren für öffentliche Leistungen im Geschäftsbereich des Ministeriums für Soziales, Gesundheit, Integration und Verbraucherschutz (Gebührenordnung MSGIV - GebOMSGIV)

#### § 1 Gebührentarif

Für die in der Anlage genannten Amtshandlungen werden die dort aufgeführten Verwaltungsgebühren erhoben.
Die Anlage ist Teil dieser Verordnung.

#### § 2 Einschränkungen der persönlichen Gebührenfreiheit

(1) Zur Zahlung von Gebühren für Amtshandlungen der Ärztekammer und der Zahnärztekammer nach den Tarifstellen 2.5.1.3.12, 2.5.1.3.13, 2.5.2.3.2, 2.5.2.3.3 und 2.5.2.3.4 der Anlage bleiben die in § 8 Absatz 1 des Gebührengesetzes für das Land Brandenburg genannten juristischen Personen des öffentlichen Rechts verpflichtet.

(2) Zur Zahlung von Gebühren für Amtshandlungen der Gesundheitsämter nach den Tarifstellen 7.8.4 und 7.9 der Anlage bleiben die in § 8 Absatz 1 des Gebührengesetzes für das Land Brandenburg genannten juristischen Personen des öffentlichen Rechts verpflichtet.

#### § 3 Gebührenbemessung

Soweit Gebühren nach dem erforderlichen Zeitaufwand zu berechnen sind, werden folgende Stundensätze zugrunde gelegt:

1.  für Beamtinnen oder Beamte des höheren Dienstes und vergleichbare Angestellte 80 Euro,
2.  für Beamtinnen oder Beamte des gehobenen Dienstes und vergleichbare Angestellte 60 Euro,
3.  für Beamtinnen oder Beamte des mittleren Dienstes und vergleichbare Angestellte 48 Euro,
4.  für Beamtinnen oder Beamte des einfachen Dienstes und vergleichbare Angestellte 38 Euro.

Bei der Ermittlung der Gebühren nach Zeitaufwand ist die Zeit anzusetzen, die unter regelmäßigen Verhältnissen von einer entsprechend ausgebildeten Fachkraft benötigt wird.
Die Zeit für Ortsbesichtigungen, einschließlich An- und Abreise, ist einzurechnen.

#### § 4 Gebühren im Bereich Verbraucherschutz

Für die Erhebung von Verwaltungsgebühren für Amtshandlungen im Bereich Verbraucherschutz ist die Anlage der Gebührenordnung des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz vom 22. November 2011 (GVBl. II Nr. 77), die zuletzt durch Artikel 2 der Verordnung vom 8. Oktober 2020 (GVBl. II Nr. 96) geändert worden ist, anzuwenden.

#### § 5 Außerkrafttreten von Gebühren im Bereich Medizinprodukte ohne Messfunktion

Die Tarifstellen 7.7.1 bis 7.7.9 der Gebührenübersicht der Anlage treten mit Ablauf des 25. Mai 2022 außer Kraft.

* * *

### Anlagen

1

[Anlage (zu § 1) - Gebührentarif](/br2/sixcms/media.php/68/GebO.MSGIV.Anlage.pdf "Anlage (zu § 1) - Gebührentarif") 1.0 MB