## Verordnung zur elektronischen Aktenführung bei den Gerichten (elektronische-Akten-Verordnung - eAktV)

Auf Grund des § 298a Absatz 1 Satz 2 und 4 erster Teilsatz der Zivilprozessordnung in der Fassung der Bekanntmachung vom 5.
Dezember 2005 (BGBl.
I S. 3202; 2006 I S.
431; 2007 I S.
1781) in Verbindung mit § 1 Nummer 56 der Justiz-Zuständigkeitsübertragungsverordnung vom 9. April 2014 (GVBl.
II Nr. 23), der durch Verordnung vom 27.
August 2018 (GVBl.
II Nr.
55) geändert worden ist, verordnet der Minister der Justiz und für Europa und Verbraucherschutz:

#### § 1 Anordnung der elektronischen Aktenführung

Bei den in der Anlage bezeichneten Gerichten werden die Akten in den genannten Verfahren ab dem angegebenen Zeitpunkt nach Maßgabe der Sätze 2 und 3 elektronisch geführt.
Akten, die ab dem angegebenen Zeitpunkt neu angelegt werden, werden im Ganzen elektronisch geführt.
Akten, die zum angegebenen Zeitpunkt bereits in Papierform angelegt sind, werden im Ganzen in Papierform geführt, soweit nicht in der Anlage etwas anderes bestimmt ist; dies betrifft auch von anderen Gerichten insbesondere wegen Unzuständigkeit abgegebene Verfahren, soweit die Akten dort zum angegebenen Zeitpunkt bereits in Papierform geführt wurden.

#### § 2 Bildung elektronischer Akten

(1) Elektronische Dokumente sowie in Papierform beibehaltene Schriftstücke und sonstige Unterlagen gemäß § 3 Satz 2, die dieselbe Angelegenheit betreffen, sind zu Akten zu vereinigen.

(2) Enthält eine elektronisch geführte Akte sowohl elektronische als auch in Papierform beibehaltene Bestandteile, so muss beim Zugriff auf jeden der Teile ein Hinweis auf den jeweils anderen Teil enthalten sein.

(3) Elektronisch geführte Akten sind so zu strukturieren, dass sie die gerichtsinterne Bearbeitung sowie den Aktenaustausch unterstützen.

#### § 3 Übertragung von Papierdokumenten

Schriftstücke und sonstige Unterlagen, die zu einer gemäß § 1 elektronisch zu führenden Akte in Papierform eingereicht werden, sind in die elektronische Form zu übertragen.
Ausgenommen sind Schriftstücke und sonstige Unterlagen, deren Übertragung wegen ihres Umfanges oder ihrer sonstigen Beschaffenheit unverhältnismäßig wäre, sowie in Papierform geführte Akten anderer Instanzen und Beiakten.

#### § 4 Übermittlung elektronischer Akten

Elektronische Akten sind zwischen den in der Anlage genannten Gerichten in elektronischer Form zu übermitteln.

#### § 5 Führung und Aufbewahrung elektronischer Akten

Die elektronische Akte ist mit einem elektronischen Datenverarbeitungssystem nach dem Stand der Technik zu führen und aufzubewahren, das insbesondere gewährleistet, dass

1.  die elektronische Akte benutzbar, lesbar und auffindbar ist (Verfügbarkeit),
2.  die Funktionen der elektronischen Akte nur genutzt werden können, wenn sich der Benutzer dem System gegenüber identifiziert und authentisiert (Identifikation und Authentisierung),
3.  die eingeräumten Benutzungsrechte im System verwaltet werden (Berechtigungsverwaltung),
4.  die eingeräumten Benutzungsrechte vom System geprüft werden (Berechtigungsprüfung),
5.  die Vornahme von Veränderungen und Ergänzungen der elektronischen Akte im System protokolliert wird (Beweissicherung),
6.  eingesetzte Backup-Systeme ohne Sicherheitsrisiken wiederhergestellt werden können (Wiederaufbereitung),
7.  etwaige Verfälschungen der gespeicherten Daten durch Fehlfunktionen des Systems durch geeignete technische Prüfmechanismen rechtzeitig bemerkt werden können (Unverfälschtheit),
8.  die Funktionen des Systems fehlerfrei ablaufen und auftretende Fehlfunktionen unverzüglich gemeldet werden (Verlässlichkeit),
9.  der Austausch von Daten im System und bei Einsatz öffentlicher Netze sicher erfolgen kann (Übertragungssicherheit).

#### § 6 Ersatzmaßnahmen

Im Falle anhaltender technischer Störungen beim Betrieb der elektronischen Akte kann die Gerichtsleitung anordnen, dass eine Ersatzakte in Papierform geführt wird.
Diese ist in die elektronische Form zu übertragen, sobald die Störung behoben ist.

#### § 7 Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2019 in Kraft.

Potsdam, den 3.
Januar 2019

Der Minister der Justiz  
und für Europa und Verbraucherschutz

Stefan Ludwig

* * *

**Anlage**  
(zu § 1)

**Gericht**

**Verfahren**

**Datum**

Landgericht Frankfurt (Oder)

Alle Verfahren der Zivilkammern und der Kammern für Handelssachen außer Beschwerdeverfahren  

1. Januar 2021  

Beschwerdeverfahren, soweit  die erstinstanzlichen Akten elektronisch geführt wurden

1. September 2021

Abweichend von § 1 Satz 3 Verfahren erster Instanz nach der Zivilprozessordnung in der Zuständigkeit der 2. Zivilkammer, die seit dem 1. Januar 2019 eingegangen sind oder eingehen, auch soweit die Akten bereits in Papier angelegt sind

15.
April 2019

Abweichend von § 1 Satz 3 alle Verfahren erster Instanz nach der Zivilprozessordnung in der Zuständigkeit der 3. Zivilkammer

15.
April 2019

Landgericht Neuruppin

Alle Verfahren der Zivilkammern und der Kammern für Handelssachen außer Beschwerdeverfahren, deren erstinstanzliche Akten in Papier geführt wurden

1.
September 2021

Landgericht Potsdam

Alle Verfahren der Zivilkammern und der Kammern für Handelssachen außer Beschwerdeverfahren, deren erstinstanzliche Akten in Papier geführt wurden

1.
April 2022

Amtsgericht  Brandenburg an der Havel

Alle Verfahren in Familiensachen

1.
Mai 2021

Alle Verfahren in Zivilsachen

1.
Januar 2022

Amtsgericht Strausberg

Alle Verfahren in Zivilsachen

1.
Juni 2021

Alle Verfahren in Familiensachen

1.
Januar 2022

Amtsgericht Cottbus

Unternehmensrechtliche Verfahren gemäß § 375 Nummer 1 und 3 bis 16 des Gesetzes über das Verfahren in Familiensachen und in den Angelegenheiten der freiwilligen Gerichtsbarkeit

15.
April 2019

Amtsgericht Frankfurt (Oder)

Unternehmensrechtliche Verfahren gemäß § 375 Nummer 1 und 3 bis 16 des Gesetzes über das Verfahren in Familiensachen und in den Angelegenheiten der freiwilligen Gerichtsbarkeit

15.
April 2019

Amtsgericht  Neuruppin

Unternehmensrechtliche Verfahren gemäß § 375 Nummer 1 und 3 bis 16 des Gesetzes über das Verfahren in Familiensachen und in den Angelegenheiten der freiwilligen Gerichtsbarkeit

15.
April 2019

Amtsgericht Potsdam

Unternehmensrechtliche Verfahren gemäß § 375 Nummer 1 und 3 bis 16 des Gesetzes über das Verfahren in Familiensachen und in den Angelegenheiten der freiwilligen Gerichtsbarkeit

15.
April 2019