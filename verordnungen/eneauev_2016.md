## Verordnung über die Übertragung von Aufgaben nach dem Energieeinsparungsgesetz im Land Brandenburg (Energieeinsparungs-Aufgabenübertragungsverordnung - EnEAÜV)

Auf Grund des § 7b Absatz 4 des Energieeinsparungsgesetzes in der Fassung der Bekanntmachung vom 1. September 2005 (BGBl. I S. 2684), der durch Artikel 1 des Gesetzes vom 4.
Juli 2013 (BGBl. I S. 2197) eingefügt worden ist, verordnet die Landesregierung:

#### § 1 Zuständigkeit

Die Brandenburgische Ingenieurkammer ist zuständige Kontrollstelle für die Kontrolle von Energieausweisen und Inspektionsberichten sowie für die nicht personenbezogene Auswertung der hierbei erhobenen und gespeicherten Daten nach den §§ 26d und 26e der Energieeinsparverordnung.

#### § 2 Aufsicht

Es gilt § 30 des Brandenburgischen Ingenieurgesetzes.
Für den Vollzug der Aufgaben nach § 1 findet darüber hinaus § 121 Absatz 2 Nummer 2 der Kommunalverfassung des Landes Brandenburg entsprechend Anwendung.

#### § 3 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 10.
Juni 2016

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider