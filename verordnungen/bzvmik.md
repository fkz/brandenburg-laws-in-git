## Verordnung zur Übertragung beamtenrechtlicher Zuständigkeiten im Geschäftsbereich des Ministeriums des Innern und für Kommunales (Beamtenzuständigkeitsverordnung MIK - BZVMIK)

Auf Grund des § 6 Absatz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBI.
I S. 186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28) geändert worden ist, in Verbindung mit

*   § 4 Absatz 1 Satz 2 des Landesbeamtengesetzes vom 3.
    April 2009 (GVBI.
    I S.
    26) in Verbindung mit § 1 Absatz 3 Satz 1 der Ernennungsverordnung vom 1.
    August 2004 (GVBI. II S. 742),
*   § 32 Absatz 1 Satz 1, § 34 Absatz 1 Satz 1, § 38 Satz 2, § 50 Satz 1 zweiter Halbsatz, § 54 Absatz 1, § 56 Absatz 1 Satz 5, § 57 Absatz 1 Satz 2, § 69 Absatz 5 Satz 1, § 84 Satz 2, § 85 Absatz 4 Satz 4, § 86 Absatz 2 und Absatz 3, § 88 Absatz 5, § 89 Satz 2, § 92 Absatz 2 zweiter Halbsatz und § 103 Absatz 2 des Landesbeamtengesetzes, von denen durch Artikel 1 des Gesetzes vom 5.
    Dezember 2013 (GVBl.
    I Nr.
    36) § 34 Absatz 1 Satz 1 geändert und § 50 Satz 1 zweiter Halbsatz eingefügt und § 85 Absatz 4 Satz 4, § 86 Absatz 2 und 3, § 88 Absatz 5 und § 89 Satz 2 durch Gesetz vom 29.
    Juni 2018 (GVBl.
    I Nr.
    17) geändert worden sind,
*   § 2 Absatz 2, § 4 Absatz 4, § 9 Absatz 4 Satz 3, § 21 Absatz 3 Satz 1 der Laufbahnverordnung vom 1.
    Oktober 2019 (GVBl.
    II Nr.
    82),
*   § 17 Absatz 2 Satz 2, § 34 Absatz 5, § 35 Absatz 2 Satz 2 und des § 42 Absatz 2 Satz 2 des Landesdisziplinargesetzes vom 18.
    Dezember 2001 (GVBI.
    I S.
    254),
*   § 54 Absatz 3 Satz 2 des Beamtenstatusgesetzes vom 17.
    Juni 2008 (BGBl.
    I S.
    1010),
*   § 6 Satz 2 der Dienstjubiläumsverordnung vom 18.
    Dezember 2014 (BGBI.
    I S.
    2267) in Verbindung mit § 64 Absatz 1 Satz 1 des Landesbeamtengesetzes, der durch Gesetz vom 11. März 2010 (GVBl.
    I Nr.
    13) neu gefasst worden ist,
*   § 13 Absatz 2 Satz 3, § 16 Absatz 2 Satz 2 und § 58 Absatz 1 des Brandenburgischen Besoldungsgesetzes vom 20.
    November 2013 (GVBI.
    I Nr.
    32 S.
    2, Nr.
    34)

verordnet der Minister des Innern und für Kommunales:

#### § 1 Übertragung der Befugnis zur Ernennung, Entlassung und Versetzung in den Ruhestand

(1) Die Ausübung der Befugnis zur Ernennung der Beamtinnen und Beamten in den Laufbahnen des mittleren Dienstes, des gehobenen Dienstes bis zur Besoldungsgruppe A 13 und im Eingangsamt des höheren Dienstes sowie die Ausübung der Befugnis zur Entlassung und Versetzung in den Ruhestand der Beamtinnen und Beamten aller Laufbahngruppen wird auf

1.  die Zentrale Ausländerbehörde des Landes Brandenburg (ZABH),
2.  den Brandenburgischen IT-Dienstleister (ZIT-BB),
3.  die Landesvermessung und Geobasisinformation Brandenburg (LGB)

für ihren jeweiligen Zuständigkeitsbereich übertragen.

(2) Die Ausübung der Befugnis zur Ernennung der Beamtinnen und Beamten auf Widerruf in den Laufbahnen des gehobenen vermessungstechnischen und des gehobenen kartographischen Verwaltungsdienstes und in der Laufbahn des höheren technischen Verwaltungsdienstes in der Fachrichtung Geodäsie und Geoinformation wird auf die Landesvermessung und Geobasisinformation Brandenburg übertragen.

(3) Die nach den Absätzen 1 und 2 übertragenen Befugnisse werden im Namen des Landes Brandenburg ausgeübt.

#### § 2 Übertragung weiterer Befugnisse

(1) Den in § 1 Absatz 1 genannten Stellen werden für die Beamtinnen und Beamten aller Laufbahngruppen in ihrem Zuständigkeitsbereich folgende beamtenrechtliche Zuständigkeiten nach dem Landesbeamtengesetz vom 3.
April 2009 (GVBI.
I S.
26), das zuletzt durch Artikel 2 des Gesetzes vom 5.
Juni 2019 (GVBl.
I Nr.
19) geändert worden ist, übertragen:

1.  die Entscheidungen über das Verbot der Führung der Dienstgeschäfte gemäß § 54,
2.  die Entscheidung über die Versagung der Aussagegenehmigung gemäß § 56; die Versagung der Aussagegenehmigung bedarf der vorherigen Zustimmung des Ministeriums des Innern und für Kommunales,
3.  die Entscheidung über Ausnahmen vom Verbot der Annahme von Belohnungen, Geschenken und sonstigen Vorteilen gemäß § 57,
4.  die Entscheidung über die Erteilung und die Rücknahme der Erlaubnis zur Führung der Amtsbezeichnung mit dem Zusatz „außer Dienst“ („a.
    D.“) gemäß § 69 Absatz 5,
5.  die Entscheidungen in Nebentätigkeitsangelegenheiten und Untersagung von Tätigkeiten nach Beendigung des Beamtenverhältnisses gemäß §§ 84 bis 89 und § 92, soweit nicht die Entscheidung gemäß § 87 Absatz 1 Satz 2 Nummer 2 der obersten Dienstbehörde vorbehalten ist.

(2) Des Weiteren werden den in § 1 Absatz 1 genannten Stellen folgende weitere Befugnisse der obersten Dienstbehörde für die Beamtinnen und Beamten aller Laufbahngruppen in ihrem Zuständigkeitsbereich übertragen:

1.  die Entscheidung gemäß § 2 Absatz 2 der Laufbahnverordnung vom 1.
    Oktober 2019 (GVBl. II Nr. 82) über die Einführung und Ausgestaltung der Personalführungs- und -entwicklungsmaßnahmen,
2.  die Entscheidung gemäß § 4 Absatz 4 der Laufbahnverordnung zu Art und Umfang der Ausschreibungen und ihrer Bekanntmachung,
3.  die Entscheidungen gemäß § 9 Absatz 4 der Laufbahnverordnung über die Anrechnung einer im öffentlichen oder dienstlichen Interesse liegenden Beurlaubung auf die Probezeit im Einvernehmen mit dem für das allgemeine öffentliche Dienstrecht und dem für das finanzielle Dienstrecht zuständigen Ministerium,
4.  die Entscheidung gemäß § 21 Absatz 3 Satz 1 der Laufbahnverordnung über die Zulassung zur Einführung in die nächsthöhere Laufbahn auf Grund des Vorschlags der Auswahlkommission; dies gilt entsprechend für den Aufstieg in Laufbahnen ohne Vorbereitungsdienst,
5.  die Disziplinarbefugnis bei Ruhestandsbeamtinnen und Ruhestandsbeamten gemäß § 17 des Landesdisziplinargesetzes vom 18.
    Dezember 2001 (GVBl.
    I S.
    254), das zuletzt durch Artikel 11 des Gesetzes vom 15.
    Oktober 2018 (GVBl.
    I Nr.
    22) geändert worden ist, Disziplinarbefugnisse gemäß den §§ 34, 35 und 42 des Landesdisziplinargesetzes, wobei die Erhebung der Disziplinarklage der vorherigen Zustimmung des Ministeriums des Innern und für Kommunales bedarf,
6.  die Befugnis zur Gewährung und Versagung von Dienstjubiläumszuwendungen gemäß § 6 der Dienstjubiläumsverordnung vom 18.
    Dezember 2014 (BGBl.
    I S.
    2267), die durch Artikel 3 des Gesetzes vom 3.
    Dezember 2015 (BGBl.
    I S.
    2163) geändert worden ist, in Verbindung mit § 64 Absatz 1 Satz 1 des Landesbeamtengesetzes,
7.  die Entscheidung über die Anweisung des dienstlichen Wohnsitzes gemäß § 16 des Brandenburgischen Besoldungsgesetzes vom 20.
    November 2013 (GVBl.
    I Nr.
    32 S. 2, Nr. 34), das zuletzt durch Bekanntmachung vom 25.
    Juni 2019 (GVBl.
    I Nr.
    46, Nr.
    47, 48) geändert worden ist,
8.  die Zustimmung zum Absehen von der Rückforderung von Bezügen aus Billigkeitsgründen gemäß § 13 Absatz 2 Satz 3 des Brandenburgischen Besoldungsgesetzes,
9.  die Kürzung der Anwärterbezüge gemäß § 58 des Brandenburgischen Besoldungsgesetzes, soweit die Befugnis zur Ernennung von Beamtinnen und Beamten auf Widerruf gemäß § 1 Absatz 2 übertragen worden ist.

(3) Ferner wird den in § 1 Absatz 1 genannten Stellen für die Beamtinnen und Beamten in den Laufbahngruppen des mittleren und des gehobenen Dienstes die Befugnis über die Feststellung der Befähigung bei Laufbahnen ohne Vorbereitungsdienst gemäß § 30 der Laufbahnverordnung übertragen.

#### § 3 Befugnis zum Erlass von Widerspruchsbescheiden

Für den Erlass von Widerspruchsbescheiden in beamtenrechtlichen Angelegenheiten der Beamtinnen und Beamten, Ruhestandsbeamtinnen und Ruhestandsbeamten und früheren Beamtinnen und Beamten im Geschäftsbereich des Ministeriums des Innern und für Kommunales sowie deren Hinterbliebenen gemäß § 54 des Beamtenstatusgesetzes vom 17.
Juni 2008 (BGBl.
I S.
1010), das zuletzt durch Artikel 1 des Gesetzes vom 29.
November 2018 (BGBl.
I S.
2232) geändert worden ist, sind die in § 1 Absatz 1 genannten Stellen zuständig, soweit diese die mit dem Widerspruch angegriffene Maßnahme getroffen oder unterlassen haben.

#### § 4 Vertretung bei Klagen aus dem Beamtenverhältnis

Die Vertretung des Landes vor Gerichten der Verwaltungsgerichtsbarkeit gemäß § 103 des Landesbeamtengesetzes wird auf die in § 1 Absatz 1 genannten Stellen für ihren jeweiligen Zuständigkeitsbereich übertragen, soweit diese selbst über den Widerspruch entschieden haben oder hätten entscheiden müssen.
Satz 1 ist in Verfahren zur Erlangung des einstweiligen Rechtsschutzes gemäß §§ 80 bis 80b und § 123 der Verwaltungsgerichtsordnung entsprechend anzuwenden.

#### § 5 Übergangsvorschrift

Für Anträge, die vor dem Inkrafttreten dieser Verordnung eingegangen sind und über die noch nicht abschließend entschieden worden ist, geht die Zuständigkeit auf die in § 1 Absatz 1 genannten Stellen über.
Dies gilt auch für die Vertretung in zu diesem Zeitpunkt bereits anhängigen Rechtsstreitigkeiten.

#### § 6 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Beamtenrechtszuständigkeitsverordnung LGB vom 12.
November 2007 (GVBl.
II S.
466) außer Kraft.

Potsdam, den 14.
Oktober 2019

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter