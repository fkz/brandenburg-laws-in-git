## Verordnung zur Bestimmung der Zuständigkeiten für die Festsetzung der Besoldung der Beamtinnen und Beamten sowie der Richterinnen und Richter des Landes Brandenburg (Brandenburgische Besoldungszuständigkeitsverordnung - BbgBesZV)

Auf Grund des § 70 Absatz 2 Satz 1 des Brandenburgischen Besoldungsgesetzes vom 20. November 2013 (GVBl. I Nr. 32 S. 2, Nr.
34) verordnet die Landesregierung:

#### § 1 Zuständigkeiten der Zentralen Bezügestelle des Landes Brandenburg

(1) Die Zentrale Bezügestelle des Landes Brandenburg ist zuständig für die Festsetzung der Besoldung der Beamtinnen, Beamten, Richterinnen und Richter des Landes Brandenburg, soweit in dieser Verordnung nichts anderes bestimmt ist.
Zur Besoldung im Sinne des Satzes 1 gehören die Dienstbezüge gemäß § 1 Absatz 3 des Brandenburgischen Besoldungsgesetzes, die sonstigen Bezüge gemäß § 1 Absatz 4 des Brandenburgischen Besoldungsgesetzes sowie alle anderen aufgrund des Brandenburgischen Besoldungsgesetzes gewährten Leistungen einschließlich der Aufwandsentschädigungen.
Für die Festsetzung der Besoldung übernimmt die Zentrale Bezügestelle des Landes Brandenburg die in Absatz 2 und § 2 genannten Entscheidungen der dort bezeichneten Stellen.

(2) Die in § 4 Absatz 2 Satz 5, § 9 Satz 3, § 10 Absatz 2 Satz 2, § 25 Absatz 3 Satz 4 und § 58 Absatz 1 des Brandenburgischen Besoldungsgesetzes geregelten sowie in sonstigen Rechtsvorschriften außerhalb dieser Verordnung bestimmten Zuständigkeiten für die Festsetzung von Bezügen bleiben unberührt.

#### § 2 Zuständigkeiten der personalaktenführenden Stellen

Die personalaktenführenden Stellen sind zuständig für

1.  die Feststellung der vergütungsfähigen Mehrarbeitsstunden, des Stundensatzes der Mehrarbeitsvergütung und der Anspruchsvoraussetzungen für die Gewährung von Ausgleichszahlungen zur Abgeltung von Arbeitszeitguthaben,
2.  die Feststellung der Anspruchsvoraussetzungen und der Merkmale für die Gewährung von funktionsgebundenen Stellenzulagen, Erschwerniszulagen sowie sonstigen Zulagen, Prämien und Vergütungen,
3.  die Festsetzung von Ausgleichs- und Überleitungszulagen, soweit die Anspruchsvoraussetzungen auf Merkmalen beruhen, die nur den personalaktenführenden Stellen bekannt sind,
4.  die Feststellung der nach § 57 des Brandenburgischen Besoldungsgesetzes auf die Anwärterbezüge anzurechnenden anderen Einkünfte,
5.  die Entscheidung über die Anrechnung anderer Einkünfte gemäß § 10 Absatz 1 des Brandenburgischen Besoldungsgesetzes.

#### § 3 Rückforderung von Bezügen

(1) Die Zentrale Bezügestelle des Landes Brandenburg ist zuständig für die Rückforderung zu viel gezahlter Bezüge, die sie festgesetzt und ausgezahlt hat.
Beruht die Überzahlung auf einer Entscheidung, die eine andere Stelle getroffen hat, fordert die Zentrale Bezügestelle des Landes Brandenburg den zu viel gezahlten Betrag zurück, nachdem die andere Stelle die Überzahlung festgestellt hat.

(2) Die Entscheidung, ob und in welchem Umfang wegen der Nichterfüllung von Auflagen im Sinne des § 53 Absatz 3 des Brandenburgischen Besoldungsgesetzes die Anwärterbezüge zurückzufordern sind, trifft die oder der Dienstvorgesetzte oder die oder der letzte Dienstvorgesetzte nach § 2 Absatz 2 des Landesbeamtengesetzes.
Die Zentrale Bezügestelle des Landes Brandenburg ist bei ihrem Rückforderungsbescheid an diese Entscheidung gebunden.
Satz 1 gilt für die Rückforderung von Anwärtersonderzuschlägen gemäß § 56 des Brandenburgischen Besoldungsgesetzes entsprechend.

(3) Die Zuständigkeit für die Zustimmung zu einer Entscheidung über das Absehen von der Rückforderung gemäß § 13 Absatz 2 Satz 3 des Brandenburgischen Besoldungsgesetzes bleibt unberührt.

#### § 4 Beamtinnen und Beamte der mittelbaren Landesverwaltung

Diese Verordnung findet keine Anwendung auf Beamtinnen und Beamte der Gemeinden und Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts.

#### § 5 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Bezügezuständigkeitsverordnung vom 21.
Dezember 1993 (GVBl.
1994 II S.
3), die durch Artikel 2 der Verordnung vom 10.
Mai 2004 (GVBl.
II S.
329, 330) geändert worden ist, außer Kraft.

Potsdam, den 21.
Mai 2021

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Die Ministerin der Finanzen und für Europa

Katrin Lange