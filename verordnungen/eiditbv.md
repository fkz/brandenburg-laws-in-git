## Verordnung über Einzelheiten der elektronischen Identitätsfeststellung und den Einsatz von IT-Basiskomponenten im Land Brandenburg (eID- und IT-Basiskomponentenverordnung - eIDITBV)

Aufgrund des § 3 Absatz 3 Satz 2 und 3 und des § 11 Absatz 2 Satz 1 des Brandenburgischen E-Government-Gesetzes vom 23.
November 2018 (GVBl.
I Nr. 28) sowie des § 6 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr.
28) geändert worden ist, verordnet die Landesregierung:

#### § 1 Verfahren beim Einsatz elektronischer Identitätsnachweise, Verarbeitung personenbezogener Daten

(1) Soweit nach § 3 Absatz 3 Satz 1 des Brandenburgischen E-Government-Gesetzes vom 23. November 2018 (GVBl.
I Nr.
28) in Verwaltungsverfahren gegenüber den Behörden von der Möglichkeit des elektronischen Identitätsnachweises Gebrauch gemacht wird, gelten für den Einsatz von Berechtigungszertifikaten und die Bereitstellung der dafür erforderlichen technischen Dienste (eID-Management) die nachfolgenden Bestimmungen.

(2) Als zentrale Stelle des Landes Brandenburg nach § 3 Absatz 3 Satz 2 des Brandenburgischen E-Government-Gesetzes wird der Brandenburgische IT-Dienstleister bestimmt.

(3) Der Brandenburgische IT-Dienstleister wird für die Behörden des Landes in Verwaltungsverfahren, in denen die Feststellung der Identität nach § 3 Absatz 3 Satz 1 des Brandenburgischen E-Government-Gesetzes elektronisch erfolgt, als Diensteanbieter im Sinne des § 2 Absatz 3 des Personalausweisgesetzes vom 18.
Juni 2009 (BGBl.
I S.
1346), das zuletzt durch Artikel 4 des Gesetzes vom 18.
Juli 2017 (BGBl.
I S.
2745, 2751) geändert worden ist, tätig.
Er stellt seine Leistungen als Diensteanbieter für die in Satz 1 bezeichneten Zwecke auch den Gemeinden, Ämtern und Gemeindeverbänden und sonstigen der Aufsicht des Landes unterstehenden juristischen Personen des öffentlichen Rechts zur Verfügung.

(4) Im Rahmen der Aufgabenerfüllung nach den Absätzen 2 und 3 ist der Brandenburgische IT-Dienstleister für die Datenverarbeitung Verantwortlicher im Sinne des Artikels 4 Nummer 7 der Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates vom 27.
April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr und zur Aufhebung der Richtlinie 95/46/EG (Datenschutz-Grundverordnung) (ABl. L 119 vom 4.5.2016, S. 1; L 314 vom 22.11.2016, S.
72; L 127 vom 23.5.2018, S.
2).
Er ist berechtigt, zur Identitätsfeststellung Namen, Vornamen, Geburtsnamen, Ordensnamen, Künstlernamen, Geburtsort, Geburtsdatum, Anschrift, akademischer Grad, Abkürzung der Staatsangehörigkeit, Abkürzung „D“ für Bundesrepublik Deutschland, Dokumentenart, letzter Tag der Gültigkeitsdauer, Nebenbestimmungen sowie das dienste- und kartenspezifische Kennzeichen zu verarbeiten.

#### § 2 Bereitstellung und Nutzung der IT-Basiskomponenten des Landes, Verarbeitung personenbezogener Daten

(1) Die Bereitstellung der IT-Basiskomponenten nach § 11 Absatz 1 Satz 3 und Absatz 2 Satz 2 des Brandenburgischen E-Government-Gesetzes ist durch Veröffentlichung des Brandenburgischen IT-Dienstleisters im Amtsblatt für Brandenburg bekanntzugeben.
Dies gilt nicht für vor Inkrafttreten dieser Verordnung durch den Brandenburgischen IT-Dienstleister bereits betriebene IT-Basiskomponenten.

(2) Soweit der Brandenburgische IT-Dienstleister personenbezogene Daten für die Bereitstellung (Einrichtung und Betrieb) der IT-Basiskomponenten des Landes nach § 11 Absatz 1 Satz 3 oder Absatz 2 Satz 2 des Brandenburgischen E-Government-Gesetzes verarbeitet, ist er Verantwortlicher im Sinne von Artikel 4 Nummer 7 der Verordnung (EU) 2016/679.

(3) Werden IT-Basiskomponenten nach § 11 Absatz 1 Satz 3 oder Absatz 2 Satz 2 des Brandenburgischen E-Government-Gesetzes von einer Behörde genutzt, gilt die Behörde als Verantwortlicher im Sinne von Artikel 4 Nummer 7 der Verordnung (EU) 2016/679, soweit von ihr mittels dieser IT-Basiskomponenten personenbezogene Daten zur Erfüllung eigener Aufgaben verarbeitet werden.
Stellt die Behörde im Rahmen der Nutzung einer IT-Basiskomponente Verfahrensmängel bei der Verarbeitung personenbezogener Daten fest, hat sie den Brandenburgischen IT-Dienstleister unverzüglich darüber zu informieren.

(4) Für die Wahrnehmung der Verantwortlichkeit der Behörde nach Absatz 3 gewährleistet der Brandenburgische IT-Dienstleister

1.  die Bereitstellung der für das Verzeichnis der Verarbeitungstätigkeiten und notwendige Datenschutz-Folgenabschätzungen der Behörde nach Artikel 30 Absatz 1 und Artikel 35 Absatz 1 der Verordnung (EU) 2016/679 erforderlichen Angaben sowie sonstiger für die Erfüllung der Verpflichtungen nach Absatz 5 benötigter Informationen,
2.  die Bereitstellung der für das Sicherheitskonzept der Behörde nach § 16 Absatz 1 Satz 2 des Brandenburgischen E-Government-Gesetzes erforderlichen Informationen,
3.  die unverzügliche Information der Behörde über ihm bekannt gewordene Verfahrensmängel bei der Verarbeitung personenbezogener Daten und die voraussichtliche Dauer der Mängelbeseitigung.

(5) Die in den Absätzen 2 und 3 genannten Stellen sind unter den dort genannten Voraussetzungen verantwortlich für die Einhaltung der in den Artikeln 33 und 34 der Verordnung (EU) 2016/679 geregelten Verpflichtungen.
Die Gewährleistung der Rechte der betroffenen Personen gemäß den Artikeln 12 bis 21 der Verordnung (EU) 2016/679 obliegt den in Absatz 3 genannten Stellen.

(6) Die Absätze 2 bis 5 gelten nicht für die dem Anwendungsbereich des Artikels 2 Absatz 1 der Richtlinie (EU) 2016/680 des Europäischen Parlaments und des Rates 27.
April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten durch die zuständigen Behörden zum Zwecke der Verhütung, Ermittlung, Aufdeckung oder Verfolgung von Straftaten oder der Strafvollstreckung sowie zum freien Datenverkehr und zur Aufhebung des Rahmenbeschlusses 2008/977/JI des Rates (ABl.
L 119 vom 4.5.2016, S.
89) unterliegenden Behörden.

#### § 3 Zuständige Stelle für die Einrichtung von Servicekonten

Der Brandenburgische IT-Dienstleister wird als öffentliche Stelle des Landes Brandenburg nach § 7 Absatz 1 des Onlinezugangsgesetzes vom 14.
August 2017 (BGBl.
I S.
3122, 3138) bestimmt.

#### § 4 Weitere Ausnahmen von der Verpflichtung zur Nutzung der IT-Basiskomponenten des Landes

(1) Die Festlegung weiterer Ausnahmen nach § 11 Absatz 2 Satz 1 des Brandenburgischen E-Government-Gesetzes von der Verpflichtung zur Nutzung der IT-Basiskomponenten durch die Behörden des Landes erfolgt auf Antrag des jeweils fachlich betroffenen Ressorts oder der Staatskanzlei an das für E-Government zuständige Ministerium im beiderseitigen Einvernehmen.
In dem Antrag sind die Gründe für das Vorliegen einer Ausnahme darzulegen.
Das für E-Government zuständige Ministerium kann hierfür die Verwendung eines Formblattes vorsehen.
Es dokumentiert die festgelegten Ausnahmen in einem Verzeichnis nach dem Muster der Anlage zu dieser Verordnung.

(2) Das für E-Government zuständige Ministerium und das beantragende Ressort oder die Staatskanzlei prüfen unverzüglich gemeinsam das Vorliegen einer Ausnahme nach Maßgabe der in § 11 Absatz 2 Satz 1 des Brandenburgischen E-Government-Gesetzes geregelten Voraussetzungen und legen das Ergebnis in schriftlicher oder elektronischer Form nieder.
Soweit erforderlich, kann das für E-Government zuständige Ministerium im Einvernehmen mit den Ressorts der Landesregierung und der Staatskanzlei nähere Kriterien zur Gewährleistung der Einheitlichkeit der Prüfung nach Satz 1 bestimmen.

#### § 5 Inkrafttreten

Die Verordnung tritt vorbehaltlich des Satzes 2 am Tag nach der Verkündung in Kraft.
§ 1 tritt mit Wirkung vom 1.
Juli 2019 in Kraft.

Potsdam, den 9.
Juli 2019

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter

* * *

### Anlagen

1

[Anlage (zu § 4 Absatz 1) - Musterverzeichnis](/br2/sixcms/media.php/68/GVBl_II_48_2019-Anlage.pdf "Anlage (zu § 4 Absatz 1) - Musterverzeichnis") 172.6 KB