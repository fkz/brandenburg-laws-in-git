## Verordnung über die Ausbildung und Prüfung für die Laufbahn des gehobenen technischen Verwaltungsinformatikdienstes im Land Brandenburg (Ausbildungs- und Prüfungsordnung gehobener technischer Verwaltungsinformatikdienst - APOgtVwID)

Auf Grund des § 26 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S. 26), der durch Artikel 1 des Gesetzes vom 5.
Dezember 2013 (GVBl.
I Nr. 36) geändert worden ist, verordnet der Minister des Innern und für Kommunales im Einvernehmen mit dem Minister der Finanzen:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeine Regelungen

[§ 1 Geltungsbereich](#1)

[§ 2 Einstellungsbehörden](#2)

[§ 3 Bewerbung](#3)

[§ 4 Zulassung zur Ausbildung](#4)

[§ 5 Auswahlverfahren](#5)

[§ 6 Eingliederungsberechtigte nach dem Soldatenversorgungsgesetz](#6)

[§ 7 Einstellung in den Vorbereitungsdienst](#7)

[§ 8 Dauer des Vorbereitungsdienstes](#8)

[§ 9 Rechtsstellung und Pflichten während des Vorbereitungsdienstes](#9)

[§ 10 Ende des Beamtenverhältnisses](#10)

[§ 11 Urlaub](#11)

### Abschnitt 2  
Ausbildung

[§ 12 Ziel der Ausbildung](#12)

[§ 13 Gliederung der Ausbildung, Studium](#13)

[§ 14 Module, Leistungspunkte](#14)

[§ 15 Grundsätze des fachwissenschaftlichen Studiums](#15)

[§ 16 Inhalt des fachwissenschaftlichen Studiums](#16)

[§ 17 Wahlpflichtstudium](#17)

[§ 18 Grundsätze der berufspraktischen Studienzeiten](#18)

[§ 19 Ablauf der berufspraktischen Studienzeiten, Ausbildungsstellen](#19)

[§ 20 Leistungsnachweise und Bewertungen während des fachwissenschaftlichen Studiums](#20)

[§ 21 Praktische Arbeiten und Bewertungen während der berufspraktischen Studienzeiten](#21)

### Abschnitt 3  
Prüfungen, Laufbahnbefähigung

[§ 22 Aufgaben des Prüfungsausschusses](#22)

[§ 23 Aufgaben und Zusammensetzung der Prüfungskommissionen](#23)

[§ 24 Bachelor-Arbeit](#24)

[§ 25 Kolloquium](#25)

[§ 26 Laufbahnprüfung](#26)

[§ 27 Ergebnis der Laufbahnprüfung](#27)

### Abschnitt 4  
Gemeinsame Vorschriften

[§ 28 Wiederholung von Prüfungen](#28)

[§ 29 Prüfungserleichterungen](#29)

[§ 30 Fernbleiben, Rücktritt und Prüfungsverlängerung](#30)

[§ 31 Unlauteres Verhalten im Prüfungsverfahren](#31)

[§ 32 Ausbildungs- und Prüfungsakten](#32)

### Abschnitt 5  
Schlussbestimmung

[§ 33 Inkrafttreten](#33)

### Abschnitt 1  
Allgemeine Regelungen

#### § 1 Geltungsbereich

Diese Verordnung regelt die Ausbildung und Prüfung für die Laufbahn des gehobenen technischen Verwaltungsinformatikdienstes an der Technischen Hochschule Wildau.

#### § 2 Einstellungsbehörden

Einstellungsbehörden im Sinne dieser Verordnung sind:

1.  für die Landesverwaltung das für Inneres zuständige Ministerium und
2.  für die Verwaltung der Gemeinden und Gemeindeverbände
    1.  die Landkreise,
    2.  die kreisfreien Städte,
    3.  die amtsfreien kreisangehörigen Gemeinden,
    4.  die Ämter sowie
    5.  die kommunalen Zweckverbände mit Dienstherrnfähigkeit.

#### § 3 Bewerbung

(1) Bewerbungen für die Ausbildung im Sinne von § 1 sind an die Einstellungsbehörde zu richten.

(2) Die Einstellungsbehörde kann verlangen, dass der Bewerbung insbesondere folgende Nachweise und Unterlagen beizufügen sind:

1.  ein Lebenslauf,
2.  der Nachweis einer Hochschulzugangsberechtigung im Sinne des § 9 des Brandenburgischen Hochschulgesetzes vom 28.
    April 2014 (GVBl.
    I Nr.
    18), das zuletzt durch Artikel 2 des Gesetzes vom 20.
    September 2018 (GVBI.
    I Nr.
    21 S.
    2) geändert worden ist, in der jeweils geltenden Fassung,
3.  Nachweise über etwaige berufliche Tätigkeiten und Prüfungen und
4.  eine Einverständniserklärung der gesetzlichen Vertretung, falls die sich bewerbende Person nicht volljährig ist.

(3) Die Einstellungsbehörde kann die Beibringung der in Absatz 2 genannten Unterlagen auf elektronischem Weg fordern.
Sie kann die Teilnahme am Auswahlverfahren nach § 5 davon abhängig machen, dass die einzureichenden Unterlagen fristgerecht und vollständig beigebracht werden.

#### § 4 Zulassung zur Ausbildung

(1) Zur Ausbildung kann zugelassen werden, wer

1.  im Auswahlverfahren nach § 5 ausgewählt worden ist,
2.  eine Hochschulzugangsberechtigung im Sinne des Brandenburgischen Hochschulgesetzes besitzt und
3.  die persönlichen Voraussetzungen für die Berufung in das Beamtenverhältnis erfüllt.

(2) Zur Ausbildung kann auch zugelassen werden, wer die Voraussetzung nach Absatz 1 Nummer 2 erst zum Zeitpunkt der Einstellung in den Vorbereitungsdienst erfüllen wird.

(3) Die Entscheidung über die Zulassung zur Ausbildung trifft die jeweilige Einstellungsbehörde für ihren Bereich.
Sie kann die Zulassung auf Inhaberinnen und Inhaber bestimmter Hochschulzugangsberechtigungen im Sinne des Brandenburgischen Hochschulgesetzes beschränken.

#### § 5 Auswahlverfahren

(1) Vor der Entscheidung über die Zulassung zur Ausbildung wird in einem Auswahlverfahren festgestellt, ob die Bewerberinnen und Bewerber aufgrund ihrer Kenntnisse, Fähigkeiten und persönlichen Eigenschaften für die Einstellung in den Vorbereitungsdienst des gehobenen technischen Verwaltungsinformatikdienstes im Land Brandenburg geeignet sind.

(2) Dem Auswahlverfahren nach Absatz 1 kann eine Vorauswahl vorausgehen, die sich an der Eignung der Bewerberinnen und Bewerber zu orientieren hat.
Die Einstellungsbehörde kann die Vorauswahl nach Satz 1 insbesondere auf der Grundlage der Durchschnittsnote des letzten Schul- oder Berufsschulzeugnisses oder der letzten beiden Schul- oder Berufsschulzeugnisse oder auf der Grundlage der Noten einzelner Fächer des letzten Schul- oder Berufsschulzeugnisses treffen.
Hierzu kann sie die Beibringung des letzten Schul- oder Berufsschulzeugnisses oder der letzten beiden Schul- oder Berufsschulzeugnisse verlangen.

(3) Das Auswahlverfahren bestimmt die jeweilige Einstellungsbehörde für ihren Bereich.
Sie hat für Bewerberinnen und Bewerber desselben Einstellungstermins das gleiche Auswahlverfahren anzuwenden.

(4) Wer nicht zum Auswahlverfahren zugelassen wird oder im Auswahlverfahren erfolglos bleibt, ist spätestens einen Monat vor dem Einstellungsdatum schriftlich zu benachrichtigen.

#### § 6 Eingliederungsberechtigte nach dem Soldatenversorgungsgesetz

(1) Die Vorschriften des Soldatenversorgungsgesetzes in der Fassung der Bekanntmachung vom 16. September 2009 (BGBI.
I S.
3054), das zuletzt durch Artikel 90 des Gesetzes vom 29.
März 2017 (BGBI.
I S.
626) geändert worden ist, in der jeweils geltenden Fassung und die dazu ergangenen Durchführungsvorschriften bleiben unberührt.

(2) Für Bewerberinnen und Bewerber, die Inhaber einer Eingliederungsberechtigung im Sinne von § 9 Soldatenversorgungsgesetz sind, gelten die Bestimmungen über die Zulassung entsprechend.
Im Rahmen des Auswahlverfahrens findet ein Vergleich mit Bewerberinnen und Bewerbern, die nicht Eingliederungsberechtige sind, nicht statt.

#### § 7 Einstellung in den Vorbereitungsdienst

(1) Die Einstellungsbehörde entscheidet unter Berücksichtigung des Ergebnisses des Auswahlverfahrens nach § 5 über die Einstellung der Bewerberinnen und Bewerber in den Vorbereitungsdienst.

(2) Vor der Einstellung in den Vorbereitungsdienst müssen folgende Unterlagen vorliegen:

1.  eine Geburtsurkunde,
2.  ein amtsärztliches Gesundheitszeugnis,
3.  eine Erklärung über etwaige Bestrafungen oder anhängige Ermittlungs- oder Strafverfahren und
4.  ein behördliches Führungszeugnis.

(3) Die ausgewählten Bewerberinnen und Bewerber werden in der Regel zum 1. September des jeweiligen Kalenderjahres eingestellt.

#### § 8 Dauer des Vorbereitungsdienstes

(1) Der Vorbereitungsdienst umfasst die Ausbildung und die Prüfung und dauert in der Regel drei Jahre und sechs Monate.

(2) Der Vorbereitungsdienst kann durch Anrechnung von Zeiten verkürzt werden, die die Bewerberin oder der Bewerber bereits bei einem anderen Dienstherrn in einem gleichwertigen Vorbereitungsdienst für den technischen Verwaltungsinformatikdienst abgeleistet hat.
Die Entscheidung über die Anrechnung von Zeiten nach Satz 1 trifft die Einstellungsbehörde im Einvernehmen mit der Laufbahnordnungsbehörde.
Die Anrechnung von Studien- oder Prüfungsleistungen sowie außerhalb des Hochschulwesens erworbenen Kenntnissen richtet sich nach den entsprechenden Bestimmungen des Brandenburgischen Hochschulgesetzes.
Erfolgt eine Anrechnung im Sinne des Satzes 1 oder des Satzes 3, entscheidet die Einstellungsbehörde im Einvernehmen mit der Laufbahnordnungsbehörde über die Verwendung der oder des Studierenden während der aufgrund der Anrechnung freiwerdenden Studienzeiten.
Eine Verkürzung des Vorbereitungsdienstes erfolgt um längstens sechs Monate und ist unzulässig, wenn das Erreichen des Ausbildungsziels gefährdet erscheint.

(3) Wird die Ausbildung wegen Krankheit oder aus anderen zwingenden Gründen unterbrochen, können Ausbildungsabschnitte gekürzt oder verlängert und Abweichungen vom Studien- und Ausbildungsplan zugelassen werden, um eine zielgerichtete Fortsetzung des Vorbereitungsdienstes zu ermöglichen.

(4) Der Vorbereitungsdienst ist im Einzelfall zu verlängern, wenn die Ausbildung

1.  krankheitsbedingt länger als sechs Wochen am Stück,
2.  wegen eines Beschäftigungsverbotes nach § 3 des Mutterschutzgesetzes vom 23. Mai 2017 (BGBl. I S. 1228), in der jeweils geltenden Fassung,
3.  wegen einer Elternzeit nach den §§ 15 und 16 des Bundeselterngeld- und Elternzeitgesetzes in der Fassung der Bekanntmachung vom 27.
    Januar 2015 (BGBl.
    I S.
    33), das zuletzt durch Artikel 6 Absatz 9 des Gesetzes vom 23.
    Mai 2017 (BGBl.
    I S.1228) geändert worden ist, in der jeweils geltenden Fassung oder
4.  aus anderen zwingenden Gründen

unterbrochen wurde und die zielgerichtete Fortsetzung des Vorbereitungsdienstes nicht gewährleistet ist.

(5) Der Vorbereitungsdienst kann in den Fällen des Absatzes 4 Nummer 1 und 4 höchstens zweimal und bis zu insgesamt nicht mehr als 24 Monaten verlängert werden.
Die oder der Betroffene ist vorher anzuhören.
In begründeten Einzelfällen können Ausnahmen von Satz 1 zugelassen werden, wenn die Versagung einer weiteren Verlängerung eine unbillige Härte darstellen würde.

(6) Entscheidungen nach den Absätzen 3 bis 5 trifft die Einstellungsbehörde.

(7) Bei Nichtbestehen von Prüfungsleistungen richtet sich die Verlängerung des Vorbereitungsdienstes nach § 28.

#### § 9 Rechtsstellung und Pflichten während des Vorbereitungsdienstes

(1) Die im Rahmen des Auswahlverfahrens nach § 5 ausgewählten Bewerberinnen und Bewerber werden unter Berufung in das Beamtenverhältnis auf Widerruf in den Vorbereitungsdienst eingestellt.
Sie führen die Dienstbezeichnung „Technische Oberinspektoranwärterin“ oder „Technischer Oberinspektoranwärter“ mit dem für den Dienstherrn maßgeblichen Zusatz.

(2) Die Einstellungsbehörde kann abweichend von Absatz 1 die Ausbildung im Sinne des § 1 außerhalb eines Vorbereitungsdienstes durchführen.
Sie kann hierzu mit der ausgewählten Bewerberin oder dem ausgewählten Bewerber einen Vertrag für das Studium im Beschäftigungsverhältnis abschließen.

(3) Während des Vorbereitungsdienstes unterstehen die ausgewählten Bewerberinnen und Bewerber der Dienstaufsicht der Einstellungsbehörde.

(4) Die ausgewählten Bewerberinnen und Bewerber sind zur Teilnahme an sämtlichen Studienveranstaltungen des Studiengangs „Verwaltungsinformatik Brandenburg“ an der Technischen Hochschule Wildau und an sonstigen von ihrem Dienstherrn bestimmten Veranstaltungen verpflichtet.
Sie sind ferner zur Ablegung der vorgesehenen Modulprüfungen sowie zu eigenverantwortlichem Selbststudium verpflichtet.

#### § 10 Ende des Beamtenverhältnisses

Das Beamtenverhältnis auf Widerruf endet mit dem Ablauf des Tages, an dem

1.  das Bestehen oder
2.  das endgültige Nichtbestehen der Laufbahnprüfung gemäß § 26

bekannt gegeben worden ist.
Im Fall von Satz 1 Nummer 1 endet das Beamtenverhältnis jedoch frühestens nach Ablauf der für den Vorbereitungsdienst im Allgemeinen oder im Einzelfall festgesetzten Zeit.

#### § 11 Urlaub

(1) Erholungsurlaub ist grundsätzlich in den lehrveranstaltungsfreien und vorlesungsfreien Studienzeiten in Anspruch zu nehmen und soll drei zusammenhängende Wochen nicht überschreiten.
Die vorlesungsfreien Arbeitstage im Dezember und Januar eines jeden Jahres, an denen die Technische Hochschule Wildau geschlossen ist, werden auf den Erholungsurlaub angerechnet.

(2) Prüfungszeiten stehen Vorlesungszeiten gleich.

### Abschnitt 2  
Ausbildung

#### § 12 Ziel der Ausbildung

Ziel der Ausbildung im Sinne des § 1 ist es, die Befähigung für die Laufbahn des gehobenen technischen Verwaltungsinformatikdienstes zu vermitteln.

#### § 13 Gliederung der Ausbildung, Studium

(1) Die Ausbildung gliedert sich in eine Einführungsphase in der Landes- oder Kommunalverwaltung und ein duales Bachelor-Studium „Verwaltungsinformatik Brandenburg“ an der Technischen Hochschule Wildau.

(2) Die Einführungsphase nach Absatz 1 soll den ausgewählten Bewerberinnen und Bewerbern einen Überblick über die Strukturen und Verwaltungsabläufe in der öffentlichen Verwaltung des Landes Brandenburg verschaffen.

(3) Das Bachelor-Studium nach Absatz 1 umfasst sieben Semester und wird in folgenden Abschnitten durchgeführt:

1.  fachwissenschaftliches Grundlagenstudium (1.
    bis 3.
    Semester),
2.  fachwissenschaftliches Grundlagenstudium und Praxisabschnitt I (4.
    Semester),
3.  fachwissenschaftliches Vertiefungsstudium und Praxisabschnitt II (5.
    Semester),
4.  Wahlpflichtstudium und Praxisabschnitt III (6.
    Semester),
5.  Praxisabschnitt IV, Bachelor-Arbeit und Kolloquium (7.
    Semester).

(4) Fachwissenschaftliche und berufspraktische Studienzeiten (Praxisabschnitte I bis IV) bilden eine Einheit.

(5) Mit dem erfolgreichen Abschluss des Studiums „Verwaltungsinformatik Brandenburg“ erwerben die Studierenden, unabhängig vom Status als Beamtin oder Beamter auf Widerruf, den akademischen Grad „Bachelor of Science (B.Sc.)“ und die Laufbahnbefähigung für den gehobenen technischen Verwaltungsinformatikdienst im Land Brandenburg.

#### § 14 Module, Leistungspunkte

(1) Das Studium nach § 13 Absatz 1 gliedert sich in thematisch und zeitlich abgeschlossene Studieneinheiten (Module), die sich aus verschiedenen Lehr- und Lernformen zusammensetzen können.
Module schließen mit einer Modulprüfung in Form eines Leistungsnachweises nach § 20 Absatz 2 Satz 1 ab.

(2) Für Module, deren Prüfungen bestanden wurden, werden Leistungspunkte nach dem Europäischen System zur Übertragung und Akkumulierung von Studienleistungen (ECTS) vergeben.
Die Anzahl der Leistungspunkte, die für bestandene Modulprüfungen erreicht werden können, sind von der Hochschule durch Satzung festzulegen.

(3) Während des gesamten Studiums sind insgesamt 210 Leistungspunkte nach dem ECTS zu erreichen.
Mit den Leistungspunkten ist keine qualitative Bewertung der Studienleistungen verbunden.

#### § 15 Grundsätze des fachwissenschaftlichen Studiums

(1) Die Studieninhalte der fachwissenschaftlichen Studienabschnitte sind nach wissenschaftlichen Erkenntnissen und Methoden anwendungsorientiert auf dem aktuellen Stand des Fachs zu vermitteln.
Die Studienveranstaltungen sollen in jedem Fachgebiet aktuelle Bezüge zur Verwaltungspraxis im Land Brandenburg aufweisen.

(2) Unbeschadet der Regelung des § 26 Absatz 2 des Landesbeamtengesetzes regelt die Hochschule das Nähere zum Inhalt und Ablauf des fachwissenschaftlichen Studiums unter Beachtung der Bestimmungen dieser Verordnung durch Satzung im Einvernehmen mit dem für Inneres zuständigen Ministerium.
Den kommunalen Spitzenverbänden wird die Möglichkeit zur Stellungnahme gegeben.

#### § 16 Inhalt des fachwissenschaftlichen Studiums

(1) Das fachwissenschaftliche Studium besteht zu ungefähr 70 Prozent aus naturwissenschaftlich-technischen Lehrinhalten und zu ungefähr 30 Prozent aus Themen der öffentlichen Verwaltung.
Die Studienveranstaltungen an der Hochschule umfassen Studieninhalte aus den Fachgebieten Informatik, Rechtswissenschaften, Wirtschaftswissenschaften sowie Sozial- und Verwaltungswissenschaften.

(2) Die Studieninhalte aus dem Fachgebiet Informatik umfassen insbesondere folgende Schwerpunkte:

1.  Grundlagen der Informatik,
2.  IT-Sicherheit, Datenschutz und Datensicherheit,
3.  E-Government,
4.  IT-Administration und
5.  Softwareanpassung und -entwicklung.

(3) Die Studieninhalte aus dem Fachgebiet Rechtswissenschaften umfassen insbesondere allgemeines Verwaltungsrecht und Grundlagen des Staats-, Europa- und Privatrechts.

(4) Die Studieninhalte aus dem Fachgebiet Wirtschaftswissenschaften umfassen insbesondere die Verwaltungsbetriebswirtschaft und öffentliche Finanzwirtschaft.

(5) Die Studieninhalte aus dem Fachgebiet Sozial- und Verwaltungswissenschaften umfassen insbesondere die Grundlagen der Politik-, Verwaltungs- und Sozialwissenschaften.

(6) Die Hochschule hat in den Studienveranstaltungen der in Absatz 1 genannten Fachgebiete in hinreichendem Umfang die Besonderheiten der Kommunalverwaltung zu berücksichtigen, sofern thematische Bezüge zu den in den Absätzen 2 bis 5 genannten Schwerpunkten vorhanden sind.

#### § 17 Wahlpflichtstudium

(1) Für das sechste Semester haben die Studierenden mindestens zwei Wahlpflichtmodule zu wählen.
Wahlpflichtmodule sollen der Vertiefung bereits vorhandener und der Vermittlung neuer Kenntnisse und Fähigkeiten dienen.
Die Wahlmöglichkeit kann durch Vorgaben der jeweiligen Einstellungsbehörde beschränkt werden.

(2) § 15 Absatz 2 gilt für das Wahlpflichtstudium entsprechend.

#### § 18 Grundsätze der berufspraktischen Studienzeiten

(1) Die berufspraktischen Studienzeiten sollen berufliche Kenntnisse und Erfahrungen vermitteln.
Die Studierenden sollen während der Praxisphasen die während des fachwissenschaftlichen Studiums erworbenen Kenntnisse vertiefen und insbesondere lernen, diese in der Praxis anzuwenden.
Die praktischen Unterweisungen in den Ausbildungsstellen sollen systematisch und didaktisch effektiv auf die Inhalte des fachwissenschaftlichen Studiums abgestimmt sein.

(2) Die Arbeitszeit während der berufspraktischen Studienzeit richtet sich nach der Arbeitszeitregelung der Ausbildungsstelle.
Die Praxisabschnitte werden in Vollzeit absolviert.

(3) § 15 Absatz 2 gilt für die berufspraktischen Studienzeiten entsprechend.

#### § 19 Ablauf der berufspraktischen Studienzeiten, Ausbildungsstellen

(1) Die berufspraktische Studienzeit gliedert sich in vier jeweils 13 Wochen dauernde Praxisabschnitte, die in unterschiedlichen Bereichen der öffentlichen Verwaltung des Landes Brandenburg absolviert werden können.
Nach Möglichkeit soll mindestens ein Praxisabschnitt im Bereich einer jeweilig anderen Einstellungsbehörde im Sinne des § 2 Nummer 1 oder Nummer 2 abgeleistet werden.
Die Studierenden lernen während der berufspraktischen Studienzeit die verschiedenen Einsatzgebiete der Informationstechnologie in der Verwaltung, die vorhandenen Systeme und Anwendungsprogramme sowie allgemeine Verwaltungsabläufe kennen.

(2) Die Einstellungsbehörde kann zulassen, dass ein Praxisabschnitt in einer bundesdeutschen oder ausländischen Verwaltung, in der Privatwirtschaft oder bei Verbänden absolviert wird.
Bei Ableistung eines Praxisabschnitts außerhalb der öffentlichen Verwaltung des Landes Brandenburg muss die in der jeweiligen Ausbildungsstelle stattfindende Ausbildung einen unmittelbaren Bezug zur öffentlichen Verwaltung aufweisen und die Vergleichbarkeit der Leistungsbewertung mit Ausbildungsstellen in der öffentlichen Verwaltung des Landes Brandenburg gegeben sein.
Absatz 5 gilt entsprechend.

(3) Die Abordnung zu den Ausbildungsstellen erfolgt durch die Einstellungsbehörde.
Dabei sind Wünsche der Anwärterinnen und Anwärter nach Möglichkeit zu berücksichtigen.

(4) Die praktischen Unterweisungen in den Ausbildungsstellen erfolgen durch Ausbilderinnen und Ausbilder, die mindestens über einen Bachelorabschluss oder einen gleichwertigen Abschluss sowie eine Ausbilderzertifizierung verfügen müssen.
Die Ausbilderzertifizierung nach Satz 1 wird im Rahmen eines Lehrgangs an der Technischen Hochschule Wildau erworben.

(5) Zur Ergänzung der berufspraktischen Studienzeiten können praxisbegleitende Arbeitsgemeinschaften eingerichtet werden.
Sie dienen der Ergänzung der praktischen Unterweisungen in den Ausbildungsstellen.

#### § 20 Leistungsnachweise und Bewertungen während des fachwissenschaftlichen Studiums

(1) Leistungsnachweise sollen die fachliche Qualifikation, die Eigeninitiative sowie die schriftliche und mündliche Ausdrucksfähigkeit prüfen.
Die Benotung der Leistungsnachweise soll den Leistungsstand der Studierenden zum Prüfungszeitpunkt zuverlässig und vergleichbar abbilden.

(2) Als Formen der Leistungsnachweise kommen insbesondere Klausuren, Hausarbeiten, Aktenvorträge, Referate, Fallbearbeitungen, Projektleistungen, Praxisberichte und mündliche Prüfungen in Betracht.
Aus den in § 16 Absatz 1 genannten Fachgebieten sollen jeweils mindestens drei Leistungsnachweise erbracht werden.

(3) Die Leistungsnachweise sind mit folgenden Noten und den zu ihrer Differenzierung vorgesehenen Zwischennoten zu bewerten:

sehr gut (1,0)

\=

HERVORRAGEND – ausgezeichnete Leistungen und nur wenige unbedeutende Fehler,

sehr gut (1,3)

\=

SEHR GUT – überdurchschnittliche Leistungen, aber einige Fehler,

gut (1,7 oder 2,0 oder 2,3)

\=

GUT – insgesamt gute und solide Arbeit, jedoch mit einigen grundlegenden Fehlern,

befriedigend (2,7 oder 3,0 oder 3,3)

\=

BEFRIEDIGEND – mittelmäßig, jedoch mit deutlichen Mängeln,

ausreichend (3,7 oder 4,0)

\=

AUSREICHEND – die gezeigten Leistungen entsprechen den Mindestanforderungen und

nicht ausreichend (5,0)

\=

NICHT AUSREICHEND – es sind Verbesserungen erforderlich, bevor die Leistungen anerkannt werden können.

Andere Noten oder Zwischennoten dürfen nicht vergeben werden.

(4) § 15 Absatz 2 gilt für die von den Studierenden zu erbringenden Leistungsnachweise sowie die Bewertungen während des fachwissenschaftlichen Studiums entsprechend.

#### § 21 Praktische Arbeiten und Bewertungen während der berufspraktischen Studienzeiten

(1) Die Studierenden haben während des Praxisabschnitts I in den Ausbildungsbehörden Verwaltungsvorgänge unter Anwendung der in Betracht kommenden Rechts- und Verwaltungsvorschriften sowie unter Berücksichtigung fachlicher, betriebswirtschaftlicher und politischer Gesichtspunkte bis zur Entscheidungsreife zu bearbeiten.
In den übrigen Praxisabschnitten haben die Studierenden typische, exemplarisch ausgewählte Aufgaben der Ausbildungsbehörden unter kompetenter Nutzung Informationstechnik-bezogener Vorgehensweisen, Methoden und Werkzeuge sowie unter Anwendung der in Betracht kommenden Rechts- und Verwaltungsvorschriften und unter Berücksichtigung fachlicher, betriebswirtschaftlicher und politischer Gesichtspunkte bis zur Entscheidungsreife zu bearbeiten.

(2) Über die Studierenden ist bei Beendigung eines jeden Praxisabschnitts von der jeweiligen Ausbilderin oder dem jeweiligen Ausbilder eine Beurteilung über die Praxisleistung abzugeben.
Diese muss erkennen lassen, ob das Ziel des jeweiligen Praxisabschnitts erreicht wurde.
Sie ist mit der oder dem Studierenden zu besprechen.
Für die Benotung der Praxisleistung gilt § 20 Absatz 3 entsprechend.
Ist zu erwarten, dass die Leistungen in einem Praxisabschnitt mit „nicht ausreichend“ (5,0) zu bewerten sind, soll die oder der Studierende spätestens sechs Wochen vor dem Ende dieser Zeit auf ihren oder seinen Leistungsstand und die sich daraus ergebenden Folgen hingewiesen werden.

(3) Zum Abschluss eines jeden Praxisabschnitts haben die Studierenden jeweils einen Leistungsnachweis in Form einer Konzeptarbeit und einer hierzu passenden Praxispräsentation zu erbringen.
Für die Benotung dieser Leistungsnachweise gilt § 20 Absatz 3 entsprechend.
Die Abnahme und Bewertung der Praxispräsentation und die Bewertung der Konzeptarbeit erfolgt durch die ausbildende Person.
Die Anwesenheit einer Beisitzerin oder eines Beisitzers ist zu gewährleisten.
Die Beisitzerin oder der Beisitzer müssen über einen Bachelor-Abschluss beziehungsweise einen gleichwertigen Abschluss verfügen.
Aus der Konzeptarbeit, der Praxispräsentation und der Beurteilung über die Praxisleistung ist eine Gesamtnote für jeden Praxisabschnitt zu bilden.
Die Gesamtnote errechnet sich zu jeweils gleichen Teilen aus den Noten der in Satz 1 genannten Teilleistungen.

(4) Die Zuständigkeit für die Qualitätssicherung während der berufspraktischen Studienzeiten wird von den Einstellungsbehörden und der Technischen Hochschule Wildau gemeinsam wahrgenommen.
Die jeweilige Einstellungsbehörde sichert die Qualität während der praktischen Tätigkeiten und die Technische Hochschule Wildau gewährleistet über die Zertifizierung und Re-Zertifizierung gleichmäßige Qualitätsstandards bei den Ausbilderinnen und Ausbildern in den Einstellungsbehörden.

(5) § 15 Absatz 2 gilt für die praktischen Arbeiten und Bewertungen während der berufspraktischen Studienzeiten entsprechend.

### Abschnitt 3  
Prüfungen, Laufbahnbefähigung

#### § 22 Aufgaben des Prüfungsausschusses

(1) Die Organisation und Durchführung des Prüfungsverfahrens im Studiengang „Verwaltungsinformatik Brandenburg“ obliegt dem Prüfungsausschuss des Fachbereichs der Technischen Hochschule Wildau, dem der Studiengang angegliedert ist.
Vertreterinnen oder Vertreter des für Inneres zuständigen Ministeriums haben das Recht, an Sitzungen des Prüfungsausschusses, die den Studiengang „Verwaltungsinformatik Brandenburg“ betreffen, teilzunehmen; sie haben Rede- und Antragsrecht und sind über den Studiengang betreffende Beschlüsse des Prüfungsausschusses unverzüglich zu informieren.
Erachten die Vertreterinnen oder Vertreter des für Inneres zuständigen Ministeriums die Beschlüsse des Prüfungsausschusses, die den Studiengang „Verwaltungsinformatik Brandenburg“ betreffen, für rechtswidrig, können sie diese beanstanden; die Beanstandung hat aufschiebende Wirkung.

(2) Der Prüfungsausschuss bestellt die Prüferinnen und Prüfer.
Zur Prüferin oder zum Prüfer kann bestellt werden, wer das betreffende Modul oder das Prüfungsfach hauptberuflich an der Hochschule lehrt oder mindestens die durch die Prüfung festzustellende oder eine gleichwertige Qualifikation besitzt.
Professorinnen und Professoren können für alle Prüfungen ihres Fachgebiets zu Prüfenden bestellt werden.
Akademische Mitarbeiterinnen und Mitarbeiter und Lehrbeauftragte können für den in ihren Studienveranstaltungen dargebotenen Lehrinhalt zu Prüfenden bestellt werden.
Zu Prüfenden können auch Personen anderer Fachbereiche der Technischen Hochschule Wildau oder einer anderen Hochschule sowie fachlich geeignete Bedienstete der Landesverwaltung bestellt werden, sofern sie mindestens die durch die Prüfung festzustellende oder eine gleichwertige Qualifikation besitzen.

(3) Die Mitglieder des Prüfungsausschusses sind zur Verschwiegenheit über alle Angelegenheiten des Prüfungsverfahrens verpflichtet.

#### § 23 Aufgaben und Zusammensetzung der Prüfungskommissionen

(1) Der Prüfungsausschuss setzt Prüfungskommissionen ein, die das Kolloquium im Sinne des § 26 abnehmen.

(2) Jede Prüfungskommission besteht mindestens aus zwei Prüferinnen oder Prüfern.
Eine Prüferin oder ein Prüfer soll Betreuerin oder Betreuer der Bachelor-Arbeit sein.

(3) Die Prüfer einigen sich auf eine Note für jeden Prüfungsabschnitt.
Können sich die Prüfer ausnahmsweise nicht einigen, wird das arithmetische Mittel gebildet.

(4) Die Mitglieder der Prüfungskommission sind bei der Beurteilung von Prüfungsleistungen den allgemein gültigen Bewertungsmaßstäben unterworfen; sie sind nicht an Weisungen gebunden.
Sie sind zur Verschwiegenheit über alle mit der Prüfung zusammenhängenden Vorgänge und Beratungen verpflichtet.

#### § 24 Bachelor-Arbeit

(1) Die Studierenden haben am Ende des Studiums eine schriftliche Ausarbeitung (Bachelor-Arbeit) zu erstellen, mit der sie ihre Befähigung nachweisen, in einer vorgegebenen Frist eine für die Studienziele relevante Problemstellung unter enger Verknüpfung der theoretisch und praktisch erworbenen Kenntnisse mit wissenschaftlichen Methoden selbstständig zu bearbeiten.
Das Thema der Arbeit wird von der oder dem vom Prüfungsausschuss bestimmten Erstprüfenden festgelegt.
Die oder der Studierende hat die Möglichkeit, ein Thema ihrer oder seiner Wahl vorzuschlagen.
Es soll einen unmittelbaren Bezug zu den von der oder dem Studierenden abgeleisteten berufspraktischen Studienzeiten aufweisen.

(2) Mit der Bachelor-Arbeit haben die Studierenden eine schriftliche Erklärung darüber abzugeben, dass die Bachelor-Arbeit selbstständig verfasst wurde, nur die angegebenen Quellen und Hilfsmittel benutzt und alle Stellen der Arbeit, die wörtlich oder sinngemäß aus anderen Quellen übernommen wurden, als solche kenntlich gemacht wurden und die Bachelor-Arbeit in gleicher oder ähnlicher Form noch keiner Prüfungsbehörde vorgelegt worden ist.
Für die Bachelor-Arbeit ist die Note „nicht ausreichend“ (5,0) zu erteilen, wenn die oder der Studierende eine falsche schriftliche Erklärung abgegeben hat.

(3) Die Bearbeitungszeit für die Bachelor-Arbeit beträgt neun Wochen.

#### § 25 Kolloquium

(1) Das Bachelor-Studium schließt mit einem Kolloquium ab.
Dieses besteht aus

1.  einer mündlichen Präsentation der Bachelor-Arbeit und
2.  Fragen der Prüferinnen oder Prüfer mit Bezug zur Bachelor-Arbeit.

(2) Zum Kolloquium wird zugelassen, wer die Modulprüfungen an der Hochschule, die Praxisabschnitte und die Bachelor-Arbeit jeweils mindestens mit der Note „ausreichend“ (4,0) bestanden hat.

(3) Die Dauer des Kolloquiums soll für jede Studierende und jeden Studierenden insgesamt 60 Minuten betragen und wird im Regelfall als Einzelprüfung durchgeführt.

(4) Das Kolloquium ist bestanden, wenn es mindestens mit der Note „ausreichend“ (4,0) bewertet wurde.

#### § 26 Laufbahnprüfung

(1) Die Laufbahnprüfung besteht aus sämtlichen Modulprüfungen nach § 14, der Bachelor-Arbeit nach § 24 und dem Kolloquium nach § 25.

(2) Die Laufbahnprüfung ist bestanden, wenn alle nach Absatz 1 vorgeschriebenen Prüfungsteile mit mindestens der Note „ausreichend“ (4,0) bewertet wurden.

(3) Mit dem Bestehen der Laufbahnprüfung wird kein Rechtsanspruch auf Übernahme in den öffentlichen Dienst erworben.

#### § 27 Ergebnis der Laufbahnprüfung

Im Anschluss an das Kolloquium nach § 25 ist die Gesamtnote vom Prüfungsausschuss festzustellen.
Bei der Gesamtnote werden das arithmetische Mittel der erzielten Leistungspunkte aller Modulprüfungen, der Bachelor-Arbeit und des Kolloquiums unter Berücksichtigung ihrer Art und ihres Schwierigkeitsgrades nach Maßgabe der Studien- und Prüfungsordnung und der Rahmenordnung der Technischen Hochschule Wildau bewertet.
Bei der Gesamtnote wird eine zweite Dezimalstelle nicht berücksichtigt.

### Abschnitt 4  
Gemeinsame Vorschriften

#### § 28 Wiederholung von Prüfungen

Eine mit „nicht ausreichend“ (5,0) bewertete Modulprüfung oder Teilprüfung einer Modulprüfung darf innerhalb einer vom Prüfungsausschuss festzusetzenden Frist zweimal wiederholt werden.
Die Bachelor-Arbeit, das Kolloquium und die Prüfungen der berufspraktischen Studienzeiten dürfen nur einmal wiederholt werden.
Das Nähere regelt die Hochschule.
Das Studium verlängert sich um die Dauer der Wiederholungsprüfung, längstens um ein Jahr.

#### § 29 Prüfungserleichterungen

(1) Schwerbehinderten und gleichgestellten behinderten Studierenden im Sinne von § 2 Absatz 2 und 3 des Neunten Buches Sozialgesetzbuch vom 23.
Dezember 2016 (BGBl.
I S.
3234), das zuletzt durch Artikel 23 des Gesetzes vom 17.
Juli 2017 (BGBl.
I S.
2541, 2557) geändert worden ist, in der jeweils geltenden Fassung, sind bei den Prüfungen auf Antrag die ihrer Behinderung angemessenen Erleichterungen zu gewähren.
Die fachlichen Anforderungen dürfen nicht herabgesetzt werden.

(2) Studierenden, die vorübergehend erheblich körperlich beeinträchtigt sind, können bei Modulprüfungen auf Antrag angemessene Erleichterungen gewährt werden.
Absatz 1 Satz 2 gilt entsprechend.

(3) Anträge auf Prüfungserleichterungen sind spätestens einen Monat vor Beginn der Modulprüfung bei dem Prüfungsausschuss einzureichen.
Liegen die Voraussetzungen für die Gewährung einer Prüfungserleichterung erst zu einem späteren Zeitpunkt vor, ist der Antrag unverzüglich zu stellen.
Der Nachweis der Prüfungsbehinderung ist durch ein ärztliches Zeugnis zu führen, das Angaben über Art und Form der notwendigen Prüfungserleichterungen enthalten muss.
Der Prüfungsausschuss kann bestimmen, dass ein amtsärztliches Zeugnis beizubringen ist.

#### § 30 Fernbleiben, Rücktritt und Prüfungsverlängerung

(1) Bleibt eine Studierende oder ein Studierender einer Modulprüfung oder Teilen derselben ohne Zustimmung des Prüfungsausschusses fern oder tritt sie oder er ohne Zustimmung des Prüfungsausschusses von der Prüfung oder einem Teil von ihr zurück, wird die Prüfung oder der betreffende Teil mit der Note „nicht ausreichend“ (5,0) bewertet.

(2) Stimmt der Prüfungsausschuss dem Fernbleiben oder dem Rücktritt zu, gilt die Prüfung oder der betreffende Teil als nicht durchgeführt.
Die Zustimmung darf nur erteilt werden, wenn wichtige Gründe vorliegen, insbesondere, wenn die oder der Studierende aufgrund von Krankheit an der Prüfung oder einem Prüfungsteil nicht teilnehmen kann.
Die oder der Studierende hat das Vorliegen eines wichtigen Grundes unverzüglich gegenüber der oder dem Vorsitzenden des Prüfungsausschusses geltend zu machen und nachzuweisen.
Im Krankheitsfall ist ein ärztliches Zeugnis vorzulegen, aus dem sich die Prüfungsunfähigkeit ergibt und das in der Regel nicht später als am Prüfungstag ausgestellt sein darf.
Der Prüfungsausschuss kann bestimmen, dass ein amtsärztliches Zeugnis beizubringen ist.
Der Krankheit einer oder eines Studierenden steht die Krankheit eines von ihr oder ihm zu versorgenden Kindes oder die Pflege einer oder eines nahen Angehörigen in einer kurzfristig auftretenden Pflegesituation gleich.
In offensichtlichen Fällen kann auf die Vorlage eines ärztlichen Zeugnisses verzichtet werden.

(3) Haben sich Studierende in Kenntnis einer gesundheitlichen Beeinträchtigung oder eines anderen Rücktrittsgrundes einer Modulprüfung unterzogen, kann ein nachträglicher Rücktritt von der bezeichneten Modulprüfung wegen dieses Grundes nicht mehr genehmigt werden.

(4) Für Studierende, die mit Zustimmung des Prüfungsausschusses einer Modulprüfung oder Teilen derselben ferngeblieben oder davon zurückgetreten sind, bestimmt der Prüfungsausschuss eine Nachprüfung.
Bereits abgelegte Teile der Modulprüfung werden bei der Nachprüfung angerechnet.
Eine nicht oder nicht vollständig abgelegte mündliche Prüfung ist in vollem Umfang nachzuholen.

(5) Die Bearbeitungszeit für die Bachelor-Arbeit verlängert sich auf Antrag um Zeiten, in denen die oder der Studierende aus Gründen, die sie oder er nicht zu vertreten hat, an der Bearbeitung der Bachelor-Arbeit gehindert war.
Der Nachweis über die Gründe der Verhinderung ist unverzüglich dem Prüfungsausschuss vorzulegen.
Im Krankheitsfall ist ein ärztliches Zeugnis vorzulegen, aus dem sich die Dienstunfähigkeit der oder des Studierenden und die voraussichtliche Dauer ihrer oder seiner Erkrankung ergeben.
Der Prüfungsausschuss kann bestimmen, dass ein amtsärztliches Zeugnis beizubringen ist.
Absatz 2 Satz 6 gilt entsprechend.

#### § 31 Unlauteres Verhalten im Prüfungsverfahren

(1) Unternimmt es eine Studierende oder ein Studierender, das Ergebnis einer Modulprüfung durch Täuschung, Mitführung oder Benutzung nicht zugelassener Hilfsmittel, unzulässige Hilfe Dritter oder durch Einwirkung auf den Prüfungsausschuss oder auf von diesem mit der Wahrnehmung von Prüfungsangelegenheiten beauftragte Personen zu beeinflussen, wird die betroffene Modulprüfung mit „nicht ausreichend“ (5,0) bewertet; Entsprechendes gilt, wenn sie oder er den ordnungsgemäßen Verlauf einer Modulprüfung stört.
In besonders schweren Fällen können Studierende von der weiteren Teilnahme am Studium ausgeschlossen werden.
Für die Bachelor-Arbeit und das Kolloquium gelten die Sätze 1 und 2 entsprechend.

(2) Entscheidungen nach Absatz 1 trifft der Prüfungsausschuss; die oder der Studierende ist vorher anzuhören.
Bis zur Entscheidung des Prüfungsausschusses setzt die oder der Studierende die Modulprüfung fort, es sei denn, dass nach der Entscheidung der oder des Aufsichtführenden ein vorläufiger Ausschluss zur ordnungsgemäßen Weiterführung der Modulprüfung unerlässlich ist.

(3) Wird nachträglich erkannt, dass eine der Voraussetzungen nach Absatz 1 vorlag, kann der Prüfungsausschuss eine bestandene Modulprüfung oder die Bachelor-Prüfung für nicht bestanden erklären.
Das unrichtige Bachelor-Zeugnis ist einzuziehen und gegebenenfalls neu auszustellen.
Entscheidungen des Prüfungsausschusses nach den Sätzen 1 und 2 sind ausgeschlossen, wenn seit der Aushändigung des Zeugnisses mehr als fünf Jahre vergangen sind.

#### § 32 Ausbildungs- und Prüfungsakten

Die Ausbildungsakten werden bei der Einstellungsbehörde geführt.
Die Prüfungsakten werden bei der Hochschule geführt.

### Abschnitt 5  
Schlussbestimmung

#### § 33 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 15.
November 2018

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter