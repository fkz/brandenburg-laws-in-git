## Verordnung über das Naturschutzgebiet „Kleine Röder“

Auf Grund des § 22 Absatz 1 und 2, der §§ 23 und 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2, § 21 Absatz 1 Satz 2 und § 78 Absatz 1 Satz 3 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

#### § 1  
Erklärung zum Schutzgebie**t**

Die in § 2 näher bezeichnete Fläche im Landkreis Elbe-Elster wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Kleine Röder“.

#### § 2  
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 385 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Stadt:**

**Gemarkung:**

**Flur:**

Bad Liebenwerda

Kosilenzien

Kröbeln

Oschätzchen

Prieschka

Zobersdorf

7, 8;

3 bis 7;

2, 3, 5 bis 7;

1;

2, 3.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten mit den Blattnummern 1 und 2 im Maßstab 1 : 10 000 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 aufgeführten Liegenschaftskarten mit den Blattnummer 1 bis 12.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Innerhalb des Naturschutzgebietes wird eine Zone 1 mit weitergehenden Beschränkungen der landwirtschaftlichen Nutzung festgesetzt.
Die Zone 1 umfasst rund 28 Hektar und liegt in folgenden Fluren:

**Stadt:**

**Gemarkung:**

**Flur:**

Bad Liebenwerda

Kosilenzien

Kröbeln

Oschätzchen

8;

4, 5;

3, 6, 7.

Die Grenze der Zone 1 ist in den in Anlage 2 Nummer 1 genannten topografischen Karten und in den in Anlage 2 Nummer 2 aufgeführten Liegenschaftskarten mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten und Flurstücksliste kann beim Ministerium für Umwelt, Gesundheit und Verbraucherschutz des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam, sowie beim Landkreis Elbe-Elster, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3  
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes, das ein vielfältiges Feuchtgebiet mit Feuchtwiesen, Niedermooren, charakteristischen Gehölzbeständen und Fließ- und Stillgewässern im Elbe-Mulde-Tiefland an der Grenze zu Sachsen umfasst, ist

1.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere von Hochstaudenfluren, des Feuchtgrünlandes, Röhrichten und Uferzonen, Schwimmblatt- und Tauchfluren, naturnaher Wald- und Gebüschgesellschaften wie Auwälder, Eichenmischwälder und Weidengebüsche;
    
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Wasserfeder (Hottonia palustris), Wasserschwertlilie (Iris pseudacorus) und Froschkraut (Luronium natans);
    
3.  die Erhaltung und Entwicklung der Niederungslandschaft als Lebens- beziehungsweise Rückzugsraum und potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Vögel und Amphibien, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Moorente (Aythya nyroca), Große Rohrdommel (Botaurus stellaris), Bartmeise (Panurus biarmiaes), Wachtelkönig (Crex crex), Kiebitz (Vanellus vanellus), Bekassine (Gallinago gallinago), Schwarzstorch (Ciconia ciconia), Fischadler (Pandion haliaetus), Rohrweihe (Circus aeruginosus), Grauammer (Emberiza calandra), Schafstelze (Motacilla flava), Drosselrohrsänger (Acrocephalus arundinaceus), Rohrschwirl (Locustella luscinioides), Eisvogel (Alcedo atthis), Wechselkröte (Bufo viridis), Laubfrosch (Hyla arborea), Knoblauchkröte (Pelobates fuscus) und Moorfrosch (Rana arvalis);
    
4.  die Erhaltung des Burgwalles bei Kosilenzien mit alten Laubholzbeständen aus landeskundlichen Gründen;
    
5.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit einer insbesondere durch Wald, Gehölzreihen, Grünland, Teiche und Moorbereiche reich strukturierten Niederungslandschaft;
    
6.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Biotopverbundes zwischen den Gebieten von gemeinschaftlicher Bedeutung „Röderaue und Teiche unterhalb Großenhain“ im Freistaat Sachsen und dem „Mittellauf der Schwarzen Elster“ in Brandenburg.
    

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Kleine Röder“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinen Vorkommen von

1.  Oligo- bis mesotrophen stehenden Gewässern mit Vegetation der Littorelletea uniflorae und Isoeto-Nanojuncetea, Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Flüssen der planaren bis montanen Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Feuchten Hochstaudenfluren der planaren und montanen bis alpinen Stufe, Subatlantischem oder mitteleuropäischem Stieleichenwald oder Hainbuchenwald (Carpinion betuli) als natürliche Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Auen-Wäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion, Alnion incanae, Salicion albae) als prioritärem natürlichem Lebensraumtyp im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
3.  Biber (Castor fiber), Fischotter (Lutra lutra), Rotbauchunke (Bombina bombina), Kammmolch (Triturus cristatus), Schlammpeitzger (Misgurnus fossilis) und Bitterling (Rhodeus sericeus amarus) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

#### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
    
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
    
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
    
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
    
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
    
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
    
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
    
8.  die Ruhe der Natur durch Lärm zu stören;
    
9.  das Gebiet außerhalb der Wege zu betreten.
    Ausgenommen ist das Betreten zum Zwecke der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 9 jeweils nach dem 30.
    Juni eines jeden Jahres;
    
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der Wege, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, und außerhalb der Waldbrandwundstreifen zu reiten;
    
11.  mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
    
12.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen;
    
13.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
    
14.  Hunde frei laufen zu lassen;
    
15.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
    
16.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (wie zum Beispiel Abwasser, Klärschlamm und Bioabfälle) zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
    
17.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
    
18.  Tiere zu füttern oder Futter bereitzustellen;
    
19.  Tiere auszusetzen oder Pflanzen anzusiedeln;
    
20.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
    
21.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
    
22.  Pflanzenschutzmittel jeder Art anzuwenden;
    
23.  Wiesen, Weiden oder sonstiges Grünland umzubrechen oder neu anzusäen.
    

#### § 5  
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  beim Einsatz von Düngemitteln ein Abstand von 3 Metern zu Gewässern einzuhalten ist,
        
    2.  Gehölze in geeigneter Weise gegen Verbiss und sonstige Beschädigungen sowie Ränder von Gewässern wirksam gegen Trittschäden von weidenden Nutztieren geschützt werden,
        
    3.  auf Grünland § 4 Absatz 2 Nummer 23 gilt, bei Narbenschäden ist eine umbruchlose Nachsaat mit Zustimmung der unteren Naturschutzbehörde möglich,
        
    4.  in der Zone 1 Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle oder Sekundärrohstoffdünger (wie zum Beispiel Abwasser, Klärschlamm und Bioabfälle) einzusetzen und § 4 Absatz 2 Nummer 22 gilt;
        
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  nur Arten der potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
        
    2.  eine Nutzung der in § 3 Absatz 2 genannten Waldlebensraumtypen einzelstamm- bis truppweise durchgeführt wird.
        In den übrigen Wäldern und Forsten sind Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von 0,5 Hektar zulässig,
        
    3.  je Hektar mindestens fünf Stück stehendes Totholz mit einem Mindestdurchmesser ohne Rinde von mindestens 35 Zentimetern und einer Mindesthöhe von 5 Metern im Bestand verbleiben und je Hektar mindestens zwei Stück liegendes Totholz mit einem Durchmesser von mindestens 65 Zentimetern ohne Rinde am stärkeren Ende und einer Mindestlänge von 5 Metern im Bestand verbleibt,
        
    4.  je Hektar mindestens fünf lebensraumtypische Altbäume mit einem Mindestdurchmesser von 40 Zentimetern in 1,30 Meter Höhe ohne Rinde aus der Nutzung genommen werden.
        Als Altbäume gelten über 80 Jahre alte Nadelbäume sowie über 120 Jahre alte Laubbäume,
        
    5.  hydromorphe Böden nur bei Frost befahren werden dürfen,
        
    6.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
        
    7.  § 4 Absatz 2 Nummer 22 gilt;
        
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass ein Einschwimmen und eine Gefährdung des Fischotters und des Elbebibers weitgehend ausgeschlossen sind,
        
    2.  für die Fließgewässer § 4 Absatz 2 Nummer 18 und 19 gilt;
        
4.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende Teichbewirtschaftung im Sinne der guten fachlichen Praxis auf den bisher rechtmäßig genutzten Flächen, sofern der Schutzzweck nach § 3 Absatz 2 nicht gefährdet wird, mit der Maßgabe, dass Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass ein Einschwimmen und eine Gefährdung des Fischotters und des Elbebibers weitgehend ausgeschlossen sind;
    
5.  im Bereich der fischereilich genutzten Teiche kann die zuständige Naturschutzbehörde Maßnahmen zur Vergrämung und Tötung von Kormoranen genehmigen, sofern hierfür die erforderliche artenschutzrechtliche Ausnahmegenehmigung oder Befreiung vorliegt.
    Die Genehmigung kann mit Auflagen versehen werden; sie ist zu erteilen, wenn der Schutzzweck von der Maßnahme nicht wesentlich beeinträchtigt wird;
    
6.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass § 4 Absatz 2 Nummer 18 und 19 gilt;
    
7.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Jagd in der Zeit vom 1.
        März bis 30.
        Juni eines jeden Jahres ausschließlich vom Ansitz aus erfolgt,
        
        bb)
        
        die Fallenjagd ausschließlich mit Lebendfallen erfolgt,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
        
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Aufstellung anzuzeigen.
        Die untere Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten.
        Die Entscheidung hierüber soll unverzüglich erfolgen.
        
        Im Übrigen bleibt die Anlage von Kirrungen innerhalb geschützter Biotope und die Anlage von Wildäckern unzulässig;
        
8.  die Nutzung der Pferdetränke im bisherigen Umfang an der in der in § 2 Absatz 2 genannten topografischen Karte Blatt 1 im Maßstab 1 : 10 000 gekennzeichneten Stelle;
    
9.  das Sammeln von Pilzen und Wildfrüchten von geringen Mengen für den persönlichen Gebrauch jeweils nach dem 30.
    Juni eines jeden Jahres;
    
10.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
    
11.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
    
12.  die ordnungsgemäße Unterhaltung der Hochwasserschutzanlagen einschließlich ihrer Deichschutzstreifen und des Deichseitengrabens nach Anzeige gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes bei der unteren Naturschutzbehörde.
    Eine Anzeige ist nicht erforderlich, wenn den Maßnahmen ein abgestimmter Unterhaltungsplan zugrunde liegt;
    
13.  Maßnahmen des Hochwasserschutzes zur Beseitigung von Abflusshindernissen im Deichvorland wie zum Beispiel Stammholz, Äste, Treibgut, wenn dadurch der Schutzzweck nicht gefährdet wird.
    Die Maßnahmen sind der unteren Naturschutzbehörde gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes schriftlich anzuzeigen;
    
14.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen.
    Die ordnungsgemäße Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
    
15.  die jährliche Durchführung des Burgwallfestes an den Pfingstfeiertagen.
    Die Durchführung des Festes ist der unteren Naturschutzbehörde vorher schriftlich anzuzeigen.
    Sie kann ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen;
    
16.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
    
17.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
    
18.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
    
19.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 an Straßen und Wegen freigestellt;
    
20.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutz-behörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

#### § 6  
Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  es sollen Maßnahmen zur Anhebung der Grundwasserstände im Bereich des Niedermoores durchgeführt werden;
    
2.  in dem in § 3 Absatz 2 genannten prioritären Lebensraumtyp „Auen-Wälder mit Alnus glutinosa (Schwarzerle) und Fraxinus excelsior (Gewöhnliche Esche) (Alno-Padion, Alnion incanae, Salicion albae) sollen keine forstlichen Maßnahmen erfolgen;
    
3.  für die Teiche soll ein Bewirtschaftungsplan erstellt werden, der folgende Mindestangaben enthält: Besatz nach Arten und Altersklassen; Bespannungszeiträume; Düngung, Teichpflege- und Sanierungsmaßnahmen jeweils nach Art, Umfang und Zeitpunkt;
    
4.  Teichdämme und Wege sollen in der Zeit vom 1.
    März bis zum 15.
    November eines jeden Jahres nicht mit schlagenden oder rotierenden Mähwerken (Mulchern) gemäht werden;
    
5.  die Kleine Röder soll renaturiert und Seitenarme wieder an das Fließgewässer angebunden werden.
    Dabei soll die ökologische Durchgängigkeit der Kleinen Röder verbessert und ein Gewässerrandstreifen entwickelt werden;
    
6.  die naturnahen Uferzonen und Röhrichtbestände an den Teichen sollen erhalten und entwickelt werden.
    

#### § 7  
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu 50 000 (in Worten: fünfzigtausend) Euro geahndet werden.

#### § 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere §§ 31 bis 33 und § 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

#### § 10  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11  
Inkrafttreten, Außerkrafttreten

(1) § 5 Absatz 1 Nummer 1 Buchstabe d tritt am 1.
Juli 2011 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig tritt der Beschluss Nummer 03-2/68 des Rates des Bezirkes Cottbus vom 24.
April 1968, soweit er sich auf die Landschaftsschutzgebiete „Lampfert bei Kröbeln“ und „Burgwall bei Cosilenzien“ bezieht, außer Kraft.

Potsdam, den 1.
Juni 2011

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

**Anlage 1  
**(zu § 2 Absatz 1)

![Das Naturschutzgebiet Kleine Röder im Landkreis Elbe-Elster liegt im Bereich der Stadt Bad Liebenwerda und umfasst Teile der Gemarkungen Kosilenzien, Kröbeln, Oschätzchen, Prieschka und Zobersdorf. ](/br2/sixcms/media.php/69/Nr.%2031-2.GIF "Das Naturschutzgebiet Kleine Röder im Landkreis Elbe-Elster liegt im Bereich der Stadt Bad Liebenwerda und umfasst Teile der Gemarkungen Kosilenzien, Kröbeln, Oschätzchen, Prieschka und Zobersdorf. ")

**Anlage 2** (zu § 2 Absatz 2)

**1.
Topografische Karten Maßstab 1 : 10 000**

Titel: Topografische Karte zur Verordnung über das Naturschutzgebiet „Kleine Röder“

**Blattnummer**

**Unterzeichnung**

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV), am 28.
März 2011

2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 28.
März 2011

**2.
Liegenschaftskarten im Maßstab 1 : 2 000**

Titel: Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Kleine Röder“

**Blattnummer**

**Gemarkung**

**Flur**

**Unterzeichnung**

1

Oschätzchen

Prieschka

Zobersdorf

2, 5

1

2, 3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 28.
März 2011

2

Prieschka

Zobersdorf

1

3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 28.
März 2011

3

Oschätzchen

2, 3, 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 28.
März 2011

4

Oschätzchen

2, 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 28.
März 2011

5

Kosilenzien

Oschätzchen

7, 8

6, 7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 28.
März 2011

6

Kröbeln

Oschätzchen

4

3, 6, 7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 28.
März 2011

7

Kosilenzien

Kröbeln

Oschätzchen

7, 8

3, 4, 5

6, 7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 28.
März 2011

8

Kröbeln

Oschätzchen

4, 5

3, 7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 28.
März 2011

9

Kröbeln

3, 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 28.
März 2011

10

Kröbeln

3, 6, 7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 28.
März 2011

11

Kröbeln

7

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 28.
März 2011

12

Kröbeln

6

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 28.
März 2011

**Anlage 3  
**(zu § 2 Absatz 2)

**Flurstücksliste zur Verordnung über das Naturschutzgebiet „Kleine Röder“**

**Landkreis: Elbe-Elster**

**Stadt: Bad Liebenwerda**

**Gemarkung**

**Flur**

**Flurstück**

Kosilenzien

7

70 (anteilig Graben), 98 bis 100 anteilig;

8

(10, 12, 13, 16, 17, 43, 44 anteilig Graben), 45 bis 95, (136, 137 anteilig Weg), 138 Weg, 139, 140 Weg, 141 bis 143, 151 und 152 anteilig Graben, 158, 159 Graben, 160 Röder, 205, (207, 209, 211, 213, 215, 217, 219, 221, 223, 225 anteilig Graben), 226, 227 anteilig, 228 und 229 Graben;

Kröbeln

3

234 anteilig Weg, 236 anteilig Graben, 246 anteilig, 308 anteilig Weg, 483 und 484 Röder;

4

1 bis 8, 10, 11, 12 Weg, 13 bis 19, (20, 21, 22 Graben), 23 Weg, 24 bis 32, 33 Graben, 34 bis 39, 40 Weg, 41 bis 45, 46 Graben, 47 bis 52, 53 und 54 Weg, 55 bis 61, 62 Graben, 63 bis 66, 67 Weg, 68 Damm, 69 Röder, 70 Damm, 71 Graben, 72 bis 90, 91 anteilig Graben, 92 anteilig Weg, 93 bis 104, 106 Weg, 107/1, 107/2, 108, 112, 113 Graben, 114, 115, 116 Weg, 117, 118 Graben, 119 bis 128, 129 anteilig Weg, 130 anteilig Graben, 131 bis 151, 152 bis 155 anteilig, 200 bis 206, 207 Graben, 208, 209;

5

22 anteilig Graben, 23 anteilig, 25 bis 29 anteilig, 317 anteilig;

6

86 und 87 anteilig, 107 anteilig, 108 bis 123, 124 und 125 Weg, 126 bis 136, 137 Weg, 138 bis 153, 154 Graben, 155 bis 157, 158 Graben, 159 bis 162, 165 bis 170, 174 bis 187, 190 bis 195, 197 anteilig, 203, 204, 207 bis 209, 212, 213, 216, 217/1, 217/2, 218/1, 218/2, 219 bis 228, 229 Graben, 230 Teich, 231, 233, 234 Graben, 369 Weg, (406, 407, 409, 411, 413, 415, 417, 419, 421, 423, 425, 427 anteilig), 442 bis 447, 448 Röder, 449 Graben, (450 bis 452 Weg), 453 und 454 Graben, 455, 456 anteilig, 457, 458 anteilig, 459, 460 anteilig, 461, 462 anteilig, 463, 464 anteilig, 465, 466 anteilig, 467, 468 anteilig, 469, 470;

7

42, 43, 46, 47, 48 und 49 anteilig, 51 bis 55, 56 Graben, 121 bis 126, 127 anteilig Weg, 128 bis 141, 142 Graben, 143, 144, 145/1, 147/1, 147/2, 148 bis 153, 154 Graben, 155 bis 158, 160 anteilig Weg, 162 Graben, 163, 164, 165 anteilig Graben, 166 Graben, 304 und 308 anteilig Straßenunterführung, 322 und 323 Röder, 324, 326 und 327 Röder, 328 anteilig Weg, 329, 330 anteilig Teich, 331 bis 334;

Oschätzchen

2

(11, 12/1, 20/2, 23/2, 29/2, 29/3 anteilig), 58 Röder, (59/1, 89/29, 157/32, 159/25, 160/12, 161/12, 201, 203, 205, 207, 209, 211, 218, 219, 222, 224, 226, 228, 230, 232, 234 anteilig);

3

3/1 und 6/1 anteilig, 6/2, 7/2 anteilig, 7/3, (7/4, 8/2, 9/1 anteilig), 11/2, 12/1 und 13/1 anteilig, 21 und 23 Weg, 60 anteilig Weg, 61/2 und 62/2 anteilig, 150/22 Rödergraben, (203, 204, 206, 249 anteilig), 250, 251;

5

(2/1, 2/2, 2/3 anteilig), 12/1 Weg, 12/2, (37/1, 40/1, 43/1, 46/1, 52/1, 128/2 anteilig), 148/2 anteilig Graben, 149/1;

6

38 bis 72, 80 bis 83, 84 und 85 anteilig, 86, 87, 100, 101 anteilig, 102, 103 bis 120 anteilig, 192, 193 Weg, 194 anteilig Weg, 197 anteilig Weg, 204 bis 207, 208 anteilig Graben, 209, 301 bis 314;

7

1 bis 24, 25/1, 25/2, 26 bis 29, 36, 105, 106, 107 Weg, 108, 109, 110 Weg, 123 bis 126, 127 Graben, 128, 139 Graben, 201 bis 217;

Prieschka

1

39/1 anteilig, 42/8, 75 anteilig, 77 anteilig Graben, 78/1 anteilig, 81 anteilig Graben, 82 anteilig Röder, (112, 114, 116, 118, 120, 122, 124, 126, 128, 130 anteilig);

Zobersdorf

2

77 Graben, 172/74 anteilig;

3

242/1 anteilig, 246 anteilig, (368/244, 389/234, 396/237, 397/239, 398/240, 410/240, 612/225, 613/225, 614/225, 624/241, 747, 749 anteilig).

**Flächen der Zone 1**

**Gemarkung**

**Flur**

**Flurstück**

Kosilenzien

8

143;

Kröbeln

4

1 bis 8, 10, 11, 12 Weg, 13 bis 19, 20 anteilig, 21 bis 66, 208, 209;

Oschätzchen

6

38 bis 51, 101 anteilig, 102, 103 bis 120 anteilig, 192, 204;

7

1.