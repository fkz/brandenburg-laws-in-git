## Ordnung für den Vorbereitungsdienst und die Staatsprüfung für ein Lehramt im Land Brandenburg (Ordnung für den Vorbereitungsdienst - OVP)

Auf Grund des § 3 Absatz 6, des § 5 Absatz 8, des § 6 Absatz 4 und des § 8 Absatz 5 des Brandenburgischen Lehrerbildungsgesetzes vom 18.
Dezember 2012 (GVBl.
I Nr. 45), die durch Gesetz vom 31.
Mai 2018 geändert worden sind, sowie des § 26 des Landesbeamtengesetzes vom 3. April 2009 (GVBl.
I S. 26), der durch Artikel 1 des Gesetzes vom 5.
Dezember 2013 (GVBl. I Nr. 36 S. 7) geändert worden ist, verordnet die Ministerin für Bildung, Jugend und Sport im Einvernehmen mit dem Minister des Innern und für Kommunales und dem Minister der Finanzen:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeine Vorschriften

[§ 1 Geltungsbereich](#1)

[§ 2 Zulassungsvoraussetzungen](#2)

[§ 3 Termine und Fristen](#3)

[§ 4 Zulassungsantrag](#4)

[§ 5 Ausbildungskapazitäten, Zulassungsbeschränkung](#5)

[§ 6 Auswahlkriterien](#6)

[§ 7 Auswahl nach außergewöhnlicher Härte](#7)

[§ 8 Auswahl nach Leistung](#8)

[§ 9 Auswahl nach Wartezeit](#9)

[§ 10 Zulassung](#10)

[§ 11 Dienstverhältnis und Dienstbezeichnung](#11)

[§ 12 Ausbildungs- und Prüfungsbehörde](#12)

### Abschnitt 2  
Vorbereitungsdienst

[§ 13 Ziel](#13)

[§ 14 Dauer](#14)

[§ 15 Organisation](#15)

[§ 16 Ausbildung am Studienseminar](#16)

[§ 17 Ausbildung an Schulen](#17)

[§ 18 Beurteilungen](#18)

[§ 19 Abweichende Bestimmungen für das Fach Religion](#19)

### Abschnitt 3  
Staatsprüfung

[§ 20 Zweck der Prüfung](#20)

[§ 21 Prüfungsleistungen](#21)

[§ 22 Meldung zur Prüfung und Prüfungsverfahren](#22)

[§ 23 Leistungsbewertung](#23)

[§ 24 Grundsätze für die Organisation und Durchführung der Prüfung](#24)

[§ 25 Prüfungsausschüsse](#25)

[§ 26 Unterrichtspraktische Prüfung](#26)

[§ 27 Mündliche Prüfung](#27)

[§ 28 Zuhörende](#28)

[§ 29 Gesamtnote der Staatsprüfung](#29)

[§ 30 Nichterbringen von Prüfungsleistungen](#30)

[§ 31 Rücktritt von der Staatsprüfung](#31)

[§ 32 Ordnungswidriges Verhalten](#32)

[§ 33 Wiederholung der Staatsprüfung](#33)

[§ 34 Prüfungsakten](#34)

[§ 35 Zeugnisse und Bescheinigungen](#35)

[§ 36 Abweichende Bestimmungen für das Fach Religion](#36)

### Abschnitt 4  
Besondere Bestimmungen bei eingeschränktem Schul- und Ausbildungsbetrieb

[§ 37 Maßnahmen](#37)

[§ 38 Ausbildung an Schulen und Studienseminaren](#38)

[§ 39 Prüfungsersatzleistungen innerhalb der Staatsprüfung](#39)

[§ 40 Prüfungsausschüsse](#40)

### Abschnitt 5  
Übergangs- und Schlussvorschriften

[§ 41 Übergangs- und Schlussvorschriften](#41)

[§ 42 Inkrafttreten, Außerkrafttreten](#42)

### Abschnitt 1  
Allgemeine Vorschriften

#### § 1 Geltungsbereich

Diese Verordnung regelt das Nähere für den Vorbereitungsdienst für ein Lehramt (Vorbereitungsdienst) sowie die ihn abschließende Staatsprüfung.
Sie gilt für die berufsbegleitende Teilnahme am Vorbereitungsdienst gemäß § 5 Absatz 7 des Brandenburgischen Lehrerbildungsgesetzes (berufsbegleitende Teilnahme am Vorbereitungsdienst) entsprechend, soweit in dieser Verordnung nichts anderes geregelt wird.

#### § 2 Zulassungsvoraussetzungen

(1) Für den Vorbereitungsdienst für ein Lehramt kann zugelassen werden, wer die fachlichen Voraussetzungen durch einen lehramtsbezogenen Masterabschluss gemäß § 4 Absatz 1 Satz 1 des Brandenburgischen Lehrerbildungsgesetzes oder einen ihm gleichgestellten Abschluss nachweist, der den Zugang zum Vorbereitungsdienst eröffnet.
Für die Zulassung zur berufsbegleitenden Teilnahme am Vorbereitungsdienst ist außerdem ein Beschäftigungsverhältnis im Schuldienst nachzuweisen.

(2) Bewerberinnen und Bewerber, deren Muttersprache nicht Deutsch ist, haben nachzuweisen, dass sie über die für den Unterricht erforderlichen deutschen Sprachkenntnisse, mindestens jedoch auf der Kompetenzstufe C 2 des Gemeinsamen Europäischen Referenzrahmens, verfügen.

#### § 3 Termine und Fristen

(1) Für die Zulassung zum Vorbereitungsdienst sind in der Regel jährlich zwei Termine vorzusehen.
Die Einstellungstermine und Antragsfristen werden von dem für Schule zuständigen Ministerium bekannt gegeben.

(2) Der Antrag auf Zulassung zum Vorbereitungsdienst (Zulassungsantrag) ist fristgemäß gestellt, wenn er zusammen mit den Antragsunterlagen gemäß § 4 Absatz 1 Satz 2 bis zum Ablauf der Antragsfrist für den jeweiligen Zulassungstermin bei dem für Schule zuständigen Ministerium eingegangen ist.

(3) Zulassungsanträge, denen aus Kapazitätsgründen nicht entsprochen werden kann, sind für jeden nachfolgenden Termin zu wiederholen, wenn eine zusammenhängende Wartezeit gemäß § 9 anerkannt werden soll.
In jedem weiteren Zulassungsantrag sind die Zulassungstermine anzugeben, zu denen der jeweils gestellte Zulassungsantrag aus Kapazitätsgründen erfolglos war.

#### § 4 Zulassungsantrag

(1) Der Zulassungsantrag ist in der Regel über das Onlinebewerbungsportal bei dem für Schule zuständigen Ministerium zu stellen.
Dem Zulassungsantrag sind als Antragsunterlagen

1.  das Zeugnis über die Prüfung oder den Hochschulabschluss gemäß § 2 Absatz 1 Satz 1, gegebenenfalls einschließlich des erteilten Bescheids über die Anerkennung der Prüfung oder des Hochschulabschlusses, und
2.  gegebenenfalls das Zeugnis über eine Erweiterungsprüfung oder das Hochschulzertifikat gemäß den Vorschriften der Befähigungserwerbsverordnung oder eines gleichwertigen Abschlusses sowie
3.  gegebenenfalls Nachweise über Kenntnisse in der deutschen Sprache auf dem Niveau C 2 des Gemeinsamen Europäischen Referenzrahmens für Sprache gemäß § 2 Absatz 2 sowie
4.  die weiteren geforderten Nachweise und Unterlagen

in elektronischer Form beizufügen.
Soweit abweichend von Satz 1 der Zulassungsantrag schriftlich gestellt wird, sind die Antragsunterlagen dem Zulassungsantrag als Kopie beizufügen.

(2) Die Antragsunterlagen gemäß Absatz 1 Satz 2 Nummer 1 und 2 sind von den Bewerberinnen und Bewerbern, deren Einstellung in Aussicht genommen wird, im Original oder als amtlich beglaubigte Kopie vorzulegen.
Außerdem können von diesen Bewerberinnen und Bewerbern weitere, für die Zulassung erforderliche Nachweise und Unterlagen verlangt werden.
Bestehen an der Echtheit der Antragsunterlagen gemäß Absatz 1 Satz 2 Nummer 3 begründete Zweifel, kann die Vorlage der entsprechenden Originale oder Beibringung amtlich beglaubigter Kopien innerhalb einer angemessenen Frist verlangt werden.
Die Kosten für die Beglaubigung von Kopien sind von der Antragstellerin oder dem Antragsteller zu tragen.

(3) Sind die Antragsunterlagen nicht in deutscher Sprache verfasst, kann die Vorlage einer Übersetzung in deutscher Sprache, die von einer oder einem in einem Mitgliedstaat der Europäischen Union öffentlich bestellten oder beeidigten Übersetzerin oder Übersetzer gefertigt wurde, verlangt werden, wenn das für die Prüfung der Zulassungsvoraussetzungen erforderlich ist.
Die Kosten hierfür sind von der Antragstellerin oder dem Antragsteller zu tragen.

(4) Der Eingang des Zulassungsantrags ist spätestens zum Ablauf der jeweiligen Antragsfrist schriftlich zu bestätigen.
Soweit für die Zulassung erforderliche Antragsunterlagen nachzureichen sind, sind diese sowie die Nachreichfrist in der Eingangsbestätigung aufzuführen.
Auf die Folgen der Nichteinhaltung der eingeräumten Nachreichfrist ist hinzuweisen.

(5) Antragstellerinnen und Antragsteller, die bereits Zeiten im Vorbereitungsdienst für ein Lehramt erbracht haben, können unter Anrechnung dieser Zeiten nach Maßgabe freier Ausbildungsplätze in den Vorbereitungsdienst aufgenommen werden, wenn ein geordneter Ausbildungszusammenhang gewährleistet ist.
Ein erneuter Beginn des Vorbereitungsdienstes ist nur im begründeten Einzelfall möglich.
Nach der Meldung zur Staatsprüfung ist eine Einstellung in den Vorbereitungsdienst ausgeschlossen.

(6) Abweichend von Absatz 1 Satz 1 ist der Zulassungsantrag für die berufsbegleitende Teilnahme am Vorbereitungsdienst schriftlich auf dem Dienstweg an das für Schule zuständige Ministerium zu richten.

#### § 5 Ausbildungskapazitäten, Zulassungsbeschränkung

(1) Die Ausbildungskapazität für ein Lehramt ergibt sich aus der Zahl der im Einzelplan 05 des Haushaltsplanes ausgewiesenen Stellen für Beamtinnen und Beamte auf Widerruf im Vorbereitungsdienst (Lehramtskandidatinnen und Lehramtskandidaten).
Durch sie wird die Anzahl der Ausbildungsplätze an den Studienseminaren des für Schule zuständigen Ministeriums, in denen für das jeweilige Lehramt ausgebildet wird, bestimmt.
Die Ausbildungskapazität einer Ausbildungsschule beträgt maximal 15 Prozent des in ihr insgesamt erteilten Unterrichts.

(2) Für ein Lehramt ist die Zulassung zum Vorbereitungsdienst zu beschränken, wenn die Zahl der Zulassungsanträge die Ausbildungskapazität der Studienseminare um 10 Prozent oder die Ausbildungskapazität der Ausbildungsschulen überschreitet.

(3) Wird die für ein Lehramt bestehende Ausbildungskapazität nicht ausgeschöpft, können auch Einstellungen nach dem jeweils vorgesehenen Einstellungstermin erfolgen.

(4) Abweichend von Absatz 1 Satz 1 können die für bestimmte Lehrämter jeweils festgelegten Ausbildungskapazitäten zusammengefasst, ein einheitliches Zulassungsverfahren vorgesehen und unter Berücksichtigung abweichender Ausbildungsinhalte eine einheitliche Ausbildung im Vorbereitungsdienst bestimmt werden.

(5) Im Rahmen der Ausbildungskapazität der Studienseminare gemäß Absatz 1 Satz 2 legt das für Schule zuständige Ministerium die Anzahl der lehramtsbezogenen Ausbildungsplätze für die berufsbegleitende Teilnahme am Vorbereitungsdienst für jeden Schulamtsbereich fest.

#### § 6 Auswahlkriterien

(1) Sofern die Zahl der fristgemäß gestellten Zulassungsanträge die Zahl der gemäß § 5 Absatz 1 und 5 jeweils zur Verfügung stehenden Ausbildungsplätze übersteigt, sind vorab bis zu zehn Prozent der Ausbildungsplätze jeder Laufbahn für Fälle außergewöhnlicher Härte zu vergeben.
Von den danach verbleibenden Ausbildungsplätzen sind

1.  65 Prozent nach der Rangfolge nach Leistung gemäß § 8 und
2.  35 Prozent nach der Dauer der Wartezeit gemäß § 9

zu vergeben.

(2) Über die Vergabe verbleibender Ausbildungsplätze innerhalb eines Auswahlkriteriums gemäß Absatz 1 entscheidet das Los.

(3) Soweit in einem Schulamtsbereich die Anzahl der Bewerbungen die Anzahl der gemäß § 5 Absatz 5 zur Verfügung stehenden Ausbildungsplätze für die berufsbegleitende Teilnahme am Vorbereitungsdienst übersteigt, erfolgt die Auswahl nach der Gesamtnote des der jeweiligen Bewerbung zu Grunde liegenden Hochschulabschlusses.
Bei der Gesamtnote ist eine Dezimalstelle ohne Rundung zu berücksichtigen.
Bei gleicher Gesamtnote hat zunächst die Bewerberin oder der Bewerber Vorrang, die oder der im Sinne des Neunten Buches Sozialgesetzbuch schwerbehindert oder entsprechend gleichgestellt ist.
Im Übrigen entscheidet die Anzahl der bisher erfolglos gebliebenen Bewerbungen für den Vorbereitungsdienst oder die berufsbegleitende Teilnahme am Vorbereitungsdienst.
Werden in einem Schulamtsbereich auf Grund der Bewerberlage die zur Verfügung stehenden Ausbildungsplätze nicht oder nicht vollumfänglich genutzt, können die verbleibenden Ausbildungsplätze bei Bedarf an Bewerberinnen und Bewerber aus anderen Schulamtsbereichen vergeben werden.
Über die Vergabe dieser Ausbildungsplätze entscheidet das für Schule zuständige Ministerium gemäß Satz 1.
Die §§ 7 bis 9 finden für die berufsbegleitende Teilnahme am Vorbereitungsdienst keine Anwendung.

#### § 7 Auswahl nach außergewöhnlicher Härte

(1) Die Auswahl auf Grund außergewöhnlicher Härte setzt voraus, dass eine Auswahl gemäß § 8 oder § 9 nicht erfolgen kann.

(2) Eine außergewöhnliche Härte liegt insbesondere dann vor, wenn die Antragstellerin oder der Antragsteller

1.  im Sinne des Neunten Buches Sozialgesetzbuch schwerbehindert oder entsprechend gleichgestellt ist,
2.  mindestens ein mit ihr oder ihm in häuslicher Gemeinschaft lebendes Kind erzieht oder eine pflegebedürftige Person überwiegend betreut,
3.  Sicherung des Lebensunterhaltes gemäß dem Zwölften Buch Sozialgesetzbuch oder Hilfe zum Lebensunterhalt gemäß dem Zwölften Buch Sozialgesetzbuch erhält,
4.  nach Aufnahme des Lehramtsstudiums länger als sechs Monate ununterbrochen erkrankt war,
5.  eine zusammenhängende Wartezeit gemäß § 9 von mindestens zwei Jahren nachweist,
6.  Zeitverluste bei der Aufnahme und Durchführung des Lehramtsstudiums selbst nicht zu vertreten hat,
7.  eine abgeschlossene berufliche Ausbildung in einem staatlich anerkannten Ausbildungsberuf oder eine mindestens dreijährige geregelte berufliche Tätigkeit nachweist oder
8.  eine Unterbrechung des Vorbereitungsdienstes im Land Brandenburg aus zwingenden persönlichen Gründen nachweist und die Ausbildung innerhalb einer Frist von zwei Jahren nach der Entlassung fortsetzen will oder
9.  mit einer Bescheinigung des zuständigen Bundesfachverbandes des Deutschen Olympischen Sportbundes nachweist, dass sie oder er auf Bundesebene als A-, B- oder C-Kader geführt wird und die örtlichen Voraussetzungen für eine Teilnahme am Training die Bewerbung an anderen Standorten einschränken oder ausschließen.

(3) Sofern die Zahl der Zulassungsanträge mit dem Nachweis für einen Fall oder mehrere Fälle außergewöhnlicher Härte die Zahl der Ausbildungsplätze gemäß § 5 Absatz 1 Satz 1 übersteigt, ist die Antragstellerin oder der Antragsteller, die oder der mehr als einen Fall außergewöhnlicher Härte nachweist, bei der Zulassung zu bevorzugen.
Dabei zählt jedes Kind oder jede Person gemäß § 7 Absatz 2 Nummer 2 als ein Härtegrund.
Bei gleicher Anzahl von Härtegründen entscheidet das Los.

#### § 8 Auswahl nach Leistung

(1) Die Auswahl nach Leistung erfolgt insbesondere auf Grund der Gesamtnote der Prüfung oder des Abschlusses, mit der oder dem die fachlichen Voraussetzungen für die Zulassung zum Vorbereitungsdienst nachgewiesen werden.
Die Gesamtnote wird mit einer Dezimalstelle ohne Rundung für die Rangbildung bei der Zulassung berücksichtigt.

(2) Kann aus kapazitiven Gründen von den Antragstellerinnen und Antragstellern mit gleicher Gesamtnote nur ein Teil zugelassen werden, sind die vorrangig zu berücksichtigen, die höchstens sechs Monate vor Ablauf der Antragsfrist

1.  eine Tätigkeit im Sinne des Entwicklungshelfer-Gesetzes,
2.  eine Tätigkeit im Sinne des Jugendfreiwilligendienstgesetzes,
3.  einen Wehr- oder Zivildienst gemäß Artikel 12a Absatz 1 des Grundgesetzes oder
4.  eine Tätigkeit im Sinne des Bundesfreiwilligendienstgesetzes

beendet haben.
Im Übrigen sind förderliche hauptberufliche Tätigkeiten nach einem Berufsabschluss, die in einem ununterbrochenen Zeitraum von mindestens drei Monaten absolviert wurden, als weiteres Kriterium für den Vorrang zu berücksichtigen.

#### § 9 Auswahl nach Wartezeit

(1) Die Auswahl nach Dauer der ununterbrochenen Zeit seit der ersten Antragstellung auf Zulassung zum Vorbereitungsdienst (Wartezeit) setzt voraus, dass eine Auswahl gemäß § 7 oder § 8 nicht erfolgen konnte.

(2) Die Wartezeit beginnt mit dem Tag der Antragsfrist für den Einstellungstermin im Vorbereitungsdienst, für den erstmals ein Zulassungsantrag fristgemäß gestellt wurde.
Die Zeit, die

1.  nach einer nicht bestandenen Ersten Staatsprüfung oder ihr gleichzustellenden oder anzuerkennenden Hochschulprüfung,
2.  nach Rücknahme eines Zulassungsantrags oder
3.  nach einem nicht fristgemäß wiederholt gestellten Zulassungsantrag

bis zum Tag der Antragsfrist der nächsten Antragstellung zurückgelegt wurde, gilt in der Regel nicht als Wartezeit.

(3) Bei gleicher Wartezeit ist der Antragstellerin oder dem Antragsteller mit der besseren Leistung der Vorrang zu geben.

(4) Antragstellerinnen und Antragsteller, die Spitzensport ausgeübt haben und deren Ausbildungsphasen sich infolge dessen um mindestens vier Kalenderjahre verlängerten, werden zwölf Monate auf die Wartezeit angerechnet.
Berücksichtigt werden Verzögerungszeiten während des Schulbesuchs und Hochschulstudiums, das den Zugang zum Vorbereitungsdienst eröffnet.
Als Spitzensport gilt die Zugehörigkeit als Sportlerin oder Sportler in der Bundeskaderklassifikation A, B oder C des jeweiligen Bundessportverbandes des Deutschen Olympischen Sportbundes.

#### § 10 Zulassung

Über die Zulassung zum Vorbereitungsdienst entscheidet das für Schule zuständige Ministerium.
Ein Anspruch auf Zuweisung zu einem bestimmten Studienseminar besteht nicht.

#### § 11 Dienstverhältnis und Dienstbezeichnung

(1) Die Zulassung zum Vorbereitungsdienst erfolgt in der Regel durch Berufung in das Beamtenverhältnis auf Widerruf.
Soweit die Voraussetzungen dafür nicht vorliegen, erfolgt eine Anstellung in einem öffentlich-rechtlichen Ausbildungsverhältnis.
In diesem Fall besteht Anspruch auf Unterhaltsgeld in Höhe der Anwärterbezüge für Beamtinnen und Beamte im Vorbereitungsdienst.

(2) Für die gemäß Absatz 1 Satz 2 in den Vorbereitungsdienst eingestellten Personen gelten die Vorschriften für Beamtinnen und Beamte entsprechend.

(3) Die Dienstbezeichnungen richten sich nach § 5 Absatz 3 des Brandenburgischen Lehrerbildungsgesetzes.

(4) Im Fall der berufsbegleitenden Teilnahme am Vorbereitungsdienst bleibt das bestehende Beschäftigungsverhältnis unberührt.
Die Absätze 1 bis 3 finden bei der berufsbegleitenden Teilnahme am Vorbereitungsdienst keine Anwendung.

#### § 12 Ausbildungs- und Prüfungsbehörde

Ausbildungs- und Prüfungsbehörde ist das für Schule zuständige Ministerium.
Es bildet ihm zugehörige Studienseminare, die für die Organisation und Durchführung des Vorbereitungsdienstes und der Staatsprüfung zuständig sind.

### Abschnitt 2  
Vorbereitungsdienst

#### § 13 Ziel

Der Vorbereitungsdienst hat das Ziel, Lehramtskandidatinnen und Lehramtskandidaten zu befähigen, selbstständig den Beruf der Lehrerin oder des Lehrers ausüben zu können.
Das heißt insbesondere, dass sie berufliche Handlungsfähigkeit bezogen auf die in den von der Konferenz der Kultusminister der Länder in der Bundesrepublik Deutschland beschlossenen Bildungsstandards für die Lehrerbildung ausgewiesenen Kompetenzbereiche erwerben.
Die organisatorische und inhaltliche Gestaltung des Vorbereitungsdienstes hat sich an diesen Zielen zu orientieren.
Er schließt mit der Staatsprüfung gemäß Abschnitt 3 ab.

#### § 14 Dauer

(1) Der Vorbereitungsdienst dauert zwölf Monate.
Abweichend davon dauert die Ausbildung

1.  in den Fällen gemäß § 5 Absatz 2 des Brandenburgischen Lehrerbildungsgesetzes und
2.  bei berufsbegleitender Teilnahme am Vorbereitungsdienst

18 Monate.

(2) Werden auf Grund von Krankheit, des Beschäftigungsverbots gemäß dem Mutterschutzgesetz oder sonstiger Beurlaubung mehr als 24 Ausbildungstage bei einer zwölfmonatigen Dauer des Vorbereitungsdienstes oder mehr als 35 Ausbildungstage bei einer 18-monatigen Dauer versäumt, kann auf Antrag vor dem Zeitpunkt der Bestimmung des Themas für die erste Unterrichtsprobe die Dauer des Vorbereitungsdienstes um die Anzahl der versäumten Ausbildungstage, höchstens jedoch um vier Monate bei einer zwölfmonatigen Ausbildungsdauer oder sechs Monate bei einer 18-monatigen Ausbildungsdauer verlängert werden.
In begründeten Einzelfällen kann von der Höchstdauer gemäß Satz 1 abgewichen werden.

(3) Bei vorzeitiger Meldung und Zulassung zur Staatsprüfung gemäß § 8 Absatz 2 des Brandenburgischen Lehrerbildungsgesetzes endet der Vorbereitungsdienst oder die berufsbegleitende Teilnahme am Vorbereitungsdienst mit Ablauf des Monats, in dem die Staatsprüfung bestanden wird, frühestens jedoch nach 12 Monaten.
Bei nichtbestandener Staatsprüfung wird der Vorbereitungsdienst fortgesetzt und die Staatsprüfung am Ende des Vorbereitungsdienstes erneut durchgeführt.

(4) Die berufsbegleitende Teilnahme am Vorbereitungsdienst endet vorzeitig, wenn das bestehende Beschäftigungsverhältnis beendet wird.
In diesem Fall wird keine Staatsprüfung durchgeführt.

(5) Der 12-monatige Vorbereitungsdienst kann auf Antrag auch in Teilzeit im Umfang von zwei Drittel der Vollzeitausbildung absolviert werden.
Abweichend von Absatz 1 Satz 1 dauert der Vorbereitungsdienst in diesem Fall 18 Monate.
Der 18-monatige Vorbereitungsdienst kann auf Antrag auch in Teilzeit im Umfang von drei Viertel der Vollzeitausbildung absolviert werden.
Abweichend von Absatz 1 Satz 1 dauert der Vorbereitungsdienst in diesem Fall 24 Monate.
Der Antrag muss mit der Bewerbung um Aufnahme in den Vorbereitungsdienst gestellt werden.
Im Falle einer Wiederholungsprüfung gemäß § 33 kann die verlängerte Ausbildungszeit nicht in Teilzeit absolviert werden.

#### § 15 Organisation

(1) Der Vorbereitungsdienst wird an Studienseminaren und Ausbildungsschulen durchgeführt.
Bei berufsbegleitender Teilnahme am Vorbereitungsdienst findet die Ausbildung an der Schule statt, an der die Teilnehmerin oder der Teilnehmer als Lehrkraft beschäftigt ist.
Die Ausbildung bezieht sich gemäß Lehramtsstudienverordnung auf zwei Fächer, Lernbereiche oder Fachrichtungen (Fach) des jeweiligen Lehramtsstudienabschlusses auf der Grundlage der jeweils geltenden Stundentafel.
Soweit in dem auf das Lehramt für die Primarstufe bezogenen Studium das Fach Sachunterricht mit einem gesellschaftswissenschaftlichen oder naturwissenschaftlichen Bezugsfach gemäß der Lehramtsstudienverordnung studiert wurde, ist in die Ausbildung das Fach Gesellschaftswissenschaften oder Naturwissenschaften entsprechend des studierten Bezugsfaches einzubeziehen.
Im Lehramt für Förderpädagogik bezieht sich die Ausbildung auf das studierte wissenschaftliche oder künstlerische Fach und eine sonderpädagogische Fachrichtung.
Ist in dem auf das Lehramt für die Primarstufe bezogenen Studium eine inklusionspädagogische Schwerpunktbildung erfolgt, sind in die Ausbildung inklusionspädagogische Aspekte zu integrieren.

(2) Abweichend von Absatz 1 Satz 3 kann im Fall des Nachweises einer Lehrbefähigung für ein weiteres Fach, einen weiteren Lernbereich oder eine weitere Fachrichtung wahlweise dieses eines der beiden Ausbildungsfächer sein.
Das für Schule zuständige Ministerium kann hierzu Ausnahmen zulassen.

#### § 16 Ausbildung am Studienseminar

(1) Die Ausbildung an den Studienseminaren erfolgt in individuellen sowie in überfachlichen und fachbezogenen Ausbildungsangeboten, die sich auf die Ausbildungsfächer beziehen, sowie in anderen Veranstaltungsformen.
Diese Veranstaltungen haben grundsätzlich Vorrang vor Verpflichtungen in der Ausbildungsschule.

(2) Der durchschnittliche wöchentliche zeitliche Umfang der Ausbildung beträgt

1.  für die individuellen Ausbildungsangebote drei Stunden und
2.  für die weiteren Ausbildungsangebote vier Stunden.

(3) Wird der Vorbereitungsdienst in Teilzeit absolviert, entwickeln die Studienseminare am Beginn der Ausbildung jeweils einen Ausbildungsplan in Bezug auf die Ausbildungsangebote gemäß Absatz 1 Satz 1, durch den sichergestellt wird, dass die Ausbildung in Teilzeit in Umfang und Inhalten der zwölfmonatigen Ausbildung entspricht.

#### § 17 Ausbildung an Schulen

(1) Die schulpraktische Ausbildung erfolgt an Ausbildungsschulen.
Ausbildungsschulen sind alle Schulen in öffentlicher Trägerschaft.
Anerkannte Ersatzschulen können Ausbildungsschulen sein.

(2) Das Studienseminar weist die Lehramtskandidatinnen und Lehramtskandidaten im Benehmen mit dem staatlichen Schulamt oder dem Träger der Ersatzschule den Ausbildungsschulen zu.
Sie sollen hinsichtlich des Bildungsgangs und der Schulstufe dem jeweils angestrebten Lehramt unter Berücksichtigung einer gegebenenfalls vorgenommenen Schwerpunktbildung entsprechen.
In begründeten Ausnahmefällen kann eine weitere Schule in die schulpraktische Ausbildung einbezogen werden.

(3) Die Zuweisung zu einer Schule, an der ein Schulversuch durchgeführt wird, bedarf der vorherigen Zustimmung der Lehramtskandidatin oder des Lehramtskandidaten.
Das Studienseminar und die Ausbildungsschule arbeiten zur Erfüllung ihrer Ausbildungsaufgaben eng zusammen.
Die schulpraktische Ausbildung zählt zum Aufgabenbereich der Schule.
Die Leiterin oder der Leiter der Ausbildungsschule beauftragt im Benehmen mit der Lehramtskandidatin oder dem Lehramtskandidaten geeignete Lehrkräfte mit deren oder dessen Betreuung in der schulpraktischen Ausbildung.
Sie nehmen ihre Aufgaben eigenverantwortlich wahr.

(4) Die schulpraktische Ausbildung umfasst den Ausbildungsunterricht und andere die Gestaltung des Unterrichts und des Schullebens betreffenden Tätigkeiten der Lehrkräfte.
Der Ausbildungsunterricht umfasst Hospitationen und selbständigen Unterricht im Umfang von zwölf Lehrerwochenstunden.
Er beginnt in einem Umfang von mindestens sechs Lehrerwochenstunden und soll nach Ablauf der ersten Hälfte der Ausbildungsdauer mindestens zehn Lehrerwochenstunden betragen.
Im Fall der berufsbegleitenden Teilnahme am Vorbereitungsdienst wird der Umfang des selbständigen Unterrichts durch die arbeitsvertraglichen Unterrichtsverpflichtungen ersetzt.
Er soll in beiden Fächern mindestens sechs Lehrerwochenstunden umfassen.
Im Ausnahmefall kann in den ersten zwei Ausbildungsmonaten an die Stelle des selbständigen Unterrichts auch Unterricht unter Anleitung treten.

(5) Wird der Vorbereitungsdienst in Teilzeit absolviert, beträgt der Umfang des selbständigen Unterrichts acht Lehrerwochenstunden.
Er beginnt in einem Umfang von vier Lehrerwochenstunden und soll nach Ablauf von neun Monaten acht Lehrerwochenstunden betragen.

(6) Die Ausbilderinnen oder Ausbilder des Studienseminars und der Ausbildungsschule sowie deren Schulleiterin oder Schulleiter hospitieren im selbständigen Unterricht der Lehramtskandidatinnen oder Lehramtskandidaten und führen Auswertungsgespräche durch.

(7) Kann die Ausbildung in einem Ausbildungsfach an der Ausbildungsschule nicht erfolgen, bestimmt die Leiterin oder der Leiter des Studienseminars im Benehmen mit der Leiterin oder dem Leiter der Ausbildungsschule den möglichen und fachlich naheliegenden Ausbildungsunterricht und organisiert im Ausnahmefall den entsprechenden Ausbildungsunterricht an einer anderen Ausbildungsschule.

#### § 18 Beurteilungen

(1) Am Ende des Ausbildungszeitraums ist der Stand der Kompetenzentwicklung der Lehramtskandidatinnen oder Lehramtskandidaten hinsichtlich der Ziele gemäß § 13 Satz 2 durch die Schulleiterin oder den Schulleiter der Ausbildungsschule schriftlich zu beurteilen.
Findet die Ausbildung an zwei oder mehreren Schulen statt, ist von deren Schulleiterin oder Schulleiter ein Beurteilungsbeitrag anzufertigen und der Schulleitung der letzten Ausbildungsschule zuzuleiten.
Die zu fertigende Beurteilung schließt mit einer Note ab und ist dem Studienseminar spätestens zwei Wochen vor dem festgelegten Termin für die erste Unterrichtsprobe zuzuleiten.

(2) Findet die Ausbildung an einer weiteren Schule gemäß § 17 Absatz 7 statt, ist von deren Leiterin oder Leiter ein Beurteilungsbeitrag zu fertigen und der Leiterin oder dem Leiter der Ausbildungsschule zuzuleiten.
Der Beurteilungsbeitrag ist von der Leiterin oder dem Leiter der Ausbildungsschule bei der Fertigung der Beurteilung angemessen zu würdigen.

(3) Im Falle der Verlängerung des Vorbereitungsdienstes gemäß § 33 Absatz 1 ist eine neue Beurteilung unter Berücksichtigung der Beurteilung gemäß Absatz 1 anzufertigen.
Absatz 1 Satz 2 und 3 gilt entsprechend.
Die Sätze 1 und 2 gelten auch bei Nichtbestehen der Staatsprüfung gemäß § 14 Absatz 3.

(4) Die Beurteilung ist der Lehramtskandidatin oder dem Lehramtskandidaten unverzüglich nach Eingang beim Studienseminar als Kopie auszuhändigen.
Sie oder er hat das Recht, sich zu der Beurteilung innerhalb einer Woche nach Kenntnisnahme schriftlich zu äußern.
Die schriftliche Äußerung ist zur Prüfungsakte zu nehmen.

#### § 19 Abweichende Bestimmungen für das Fach Religion

Für Lehramtskandidatinnen und Lehramtskandidaten mit dem Fach Religion gilt

1.  § 16 mit der Maßgabe, dass sich die Ausbildung nur auf ein Fach, eine Fachrichtung oder einen Lernbereich bezieht und die Ausbildung im Fach Religion in der Verantwortung der jeweiligen Kirche oder Religionsgemeinschaft erfolgt, und
2.  § 17 Absatz 4 mit der Maßgabe, dass der Ausbildungsunterricht im Ausbildungsfach Religion höchstens sechs Lehrerwochenstunden in Form von Hospitationen und selbständigem Unterricht beträgt und auf den Ausbildungsunterricht insgesamt angerechnet wird.

### Abschnitt 3  
Staatsprüfung

#### § 20 Zweck der Prüfung

Mit der Staatsprüfung wird festgestellt, ob und mit welchem Erfolg das Ziel des Vorbereitungsdienstes erreicht wurde.
Mit bestandener Staatsprüfung wird die Befähigung für

1.  das Lehramt für die Primarstufe,
2.  das Lehramt für die Sekundarstufe I und II (allgemein bildende Fächer),
3.  das Lehramt für die Sekundarstufe II (berufliche Fächer) oder
4.  das Lehramt für Förderpädagogik

erworben.

#### § 21 Prüfungsleistungen

Als Prüfungsleistungen sind

1.  die unterrichtspraktische Prüfung, die aus den Unterrichtsproben in den Ausbildungsfächern besteht, und
2.  eine mündlichen Prüfung, die in der Regel in Form eines Kolloquiums durchgeführt wird,

zu erbringen.

#### § 22 Meldung zur Prüfung und Prüfungsverfahren

Mit der Bekanntgabe des Prüfungsthemas der ersten Unterrichtsprobe der unterrichtspraktischen Prüfung gilt die Meldung zur Prüfung als erfolgt.
Rechtzeitig vor der ersten Unterrichtsprobe teilt der Prüfling im Einvernehmen mit der Schulleiterin oder dem Schulleiter der Ausbildungsschule der Leiterin oder dem Leiter des Studienseminars mit, in welchen Klassen oder Kursen die Unterrichtsproben gehalten werden sollen.
Außerdem ist von dem Prüfling zu erklären, ob er der Anwesenheit anderer Lehramtskandidatinnen oder Lehramtskandidaten im Vorbereitungsdienst oder Lehrkräften gemäß § 5 Absatz 7 und § 7 Absatz 1 des Brandenburgischen Lehrerbildungsgesetzes während der Unterrichtsproben oder der mündlichen Prüfung zustimmt.
Diese Erklärung kann bis zum Beginn der einzelnen Prüfungsteile zurückgenommen werden.
Bei einer vorzeitigen Zulassung zur Staatsprüfung gemäß § 14 Absatz 1 Satz 2 gelten die Sätze 1 bis 3 entsprechend.

#### § 23 Leistungsbewertung

(1) Eine nach dieser Verordnung zu erbringende Leistung wird mit der Note

1.  sehr gut (1) bewertet, wenn sie den Anforderungen in besonderem Maße entspricht,
2.  gut (2) bewertet, wenn sie den Anforderungen voll entspricht,
3.  befriedigend (3) bewertet, wenn sie den Anforderungen im Allgemeinen entspricht,
4.  ausreichend (4) bewertet, wenn sie zwar Mängel aufweist, aber im Ganzen den Anforderungen noch entspricht,
5.  mangelhaft (5) bewertet, wenn sie den Anforderungen nicht entspricht, jedoch erkennen lässt, dass die notwendigen Grundkenntnisse vorhanden sind und die Mängel in absehbarer Zeit behoben werden können und
6.  ungenügend (6) bewertet, wenn sie den Anforderungen nicht entspricht und selbst die Grundkenntnisse so lückenhaft sind, dass die Mängel in absehbarer Zeit nicht behoben werden können.

(2) Zur differenzierten Bewertung können im Bereich zwischen den Noten 1 bis 4 Zwischenwerte durch Vermindern oder Erhöhen der jeweiligen Note um den Zahlenwert 0,3 gebildet werden.

(3) Ist gemäß dieser Verordnung aus Noten, die für Einzelleistungen erteilt wurden, eine Gesamtnote zu ermitteln, ergibt sich diese aus dem arithmetischen Mittel der Einzelnoten unter Berücksichtigung ihrer jeweiligen Wichtung.
Die Gesamtnote wird auf eine Dezimalstelle genau ermittelt.
Weitere Dezimalstellen bleiben ohne Rundung unberücksichtigt.
Der so ermittelte Zahlenwert ist im Bereich

1.  bis 1,5 der Note „sehr gut“,
2.  von 1,6 bis 2,5 der Note „gut“,
3.  von 2,6 bis 3,5 der Note „befriedigend“,
4.  von 3,6 bis 4,0 der Note „ausreichend“,
5.  von 4,1 bis 5,0 der Note „mangelhaft“ und
6.  über 5,0 der Note „ungenügend“

zuzuordnen.

#### § 24 Grundsätze für die Organisation und Durchführung der Prüfung

(1) Die Prüfung wird vor dem für Schule zuständigen Ministerium abgelegt.
Für die Planung, Organisation und Durchführung der Prüfung und der Wiederholungsprüfungen sind die Studienseminare zuständig.
Die Leiterin oder der Leiter des Studienseminars ist für die ordnungsgemäße Durchführung der Prüfungsverfahren verantwortlich und beauftragt die Vorsitzenden der Prüfungsausschüsse.

(2) Als Vorsitzende für die Prüfungsausschüsse sind

1.  bei den Unterrichtsproben die Leiterinnen oder Leiter der Ausbildungsschulen oder ihre Vertretungen oder schulfachliche oder ausbildungsfachliche Vertreterinnen und Vertreter der Schulbehörden und
2.  bei den mündlichen Prüfungen schulfachliche oder ausbildungsfachliche Vertreterinnen und Vertreter der Schulbehörden

mit der Wahrnehmung des Vorsitzes für jede Prüfung zu beauftragen.
Sofern Personen gemäß Satz 1 nicht zur Verfügung stehen, können auch Lehrkräfte als Vorsitzende von Prüfungsausschüssen beauftragt werden, die über eine mehrjährige Erfahrung in der Lehrkräfteausbildung verfügen.
Die Beauftragung ist schriftlich bekannt zu geben.
Die übrigen, nach dieser Verordnung beauftragten Mitglieder der Prüfungsausschüsse gelten mit der Übertragung der Ausbildungsaufgaben als in den Prüfungsausschuss berufen.

(3) Über den Verlauf der Unterrichtsproben und der mündlichen Prüfung ist von einem Mitglied des Prüfungsausschusses, das von der oder dem Vorsitzenden bestimmt wird, eine Niederschrift zu fertigen.
Sie umfasst

1.  den Tag, den Beginn, das Ende und den Ort der Prüfung,
2.  die Namen des Prüflings und der Mitglieder des jeweiligen Prüfungsausschusses,
3.  die Gegenstände der Prüfung,
4.  die Bewertung der Prüfungsleistung und die sie tragende Begründung,
5.  die Namen der Zuhörenden und ihre Funktion,
6.  gegebenenfalls besondere Vorkommnisse während der Prüfung sowie
7.  die von dem jeweiligen Prüfungsausschuss zu bildenden Gesamtnoten.

Die Niederschrift ist von jedem Mitglied des Prüfungsausschusses zu unterzeichnen und zur Prüfungsakte zu geben.

(4) Die Prüferinnen und Prüfer sind in ihrer Prüfertätigkeit im Rahmen der Rechtsvorschriften unabhängig.
Das für Schule zuständige Ministerium wirkt auf die Einhaltung einheitlicher Prüfungsanforderungen hin.

#### § 25 Prüfungsausschüsse

(1) Für jeden Prüfling werden Prüfungsausschüsse für die Unterrichtsproben der unterrichtspraktischen Prüfung und die mündliche Prüfung gebildet.
Es gehören dem Prüfungsausschuss

1.  für die Unterrichtsproben der unterrichtspraktischen Prüfung
    1.  die oder der Vorsitzende,
    2.  die Ausbilderin oder der Ausbilder des Studienseminars des jeweiligen Faches und
    3.  eine Ausbildungslehrkraft für das jeweilige Ausbildungsfach

und

2.  für die mündliche Prüfung
    1.  die oder der Vorsitzende,
    2.  die Vertreterin oder der Vertreter der Ausbildungsschule,
    3.  eine Ausbilderin oder ein Ausbilder des Studienseminars

als Mitglieder an.

(2) Die oder der Vorsitzende ist für den ordnungsgemäßen Ablauf der Prüfung verantwortlich.
Die Mitglieder des Prüfungsausschusses sind verpflichtet, über die Vorgänge, die im Zusammenhang mit der Prüfung stehen, Verschwiegenheit zu wahren.
Bei der Beratung des Prüfungsausschusses dürfen nur dessen Mitglieder zugegen sein.
§ 28 Absatz 1 Satz 2 bleibt unberührt.

(3) Erscheint ein Mitglied des Prüfungsausschusses für die Unterrichtsproben nicht zur Prüfung, ist von der oder dem Vorsitzenden eine Lehrkraft der Ausbildungsschule oder Beschäftigungsschule, die das Fach der Unterrichtsprobe vertritt, als Mitglied des Prüfungsausschusses zu bestellen.
Erscheinen mehrere Mitglieder des Prüfungsausschusses nicht oder kann die Vertretung einer Prüferin oder eines Prüfers aus fachlichen Gründen nicht gewährleistet werden, so ist ein neuer Termin für die Unterrichtsprobe durch das Studienseminar zu bestimmen.
Erscheint ein Mitglied des Prüfungsausschusses für die mündliche Prüfung nicht, so entscheidet die oder der Vorsitzende im Benehmen mit dem Prüfling über die Durchführung oder die terminliche Verlagerung der mündlichen Prüfung.
Satz 2 gilt entsprechend.

(4) Der Prüfungsausschuss für die Unterrichtsprobe legt die Prüfungsnote auf Vorschlag der Ausbilderin oder des Ausbilders des jeweiligen Faches des Studienseminars, der Prüfungsausschuss für die mündliche Prüfung legt die Prüfungsnote auf Vorschlag der oder des Vorsitzenden jeweils mit der Mehrheit der Stimmen fest.
Die Mitglieder der Prüfungsausschüsse haben jeweils eine Stimme.
Stimmenthaltung ist nicht zulässig.
Bei Stimmengleichheit entscheidet die Stimme der oder des Vorsitzenden.

#### § 26 Unterrichtspraktische Prüfung

(1) Die unterrichtspraktische Prüfung umfasst zwei Unterrichtsproben.
Eine Unterrichtsprobe dauert in der Regel eine Unterrichtsstunde.
Die beiden Unterrichtsproben sollen in der Regel in verschiedenen Klassen oder Kursen der dem angestrebten Lehramt entsprechenden Schulstufe oder Schulform und spätestens bis zum Termin für die mündliche Prüfung abgelegt werden.

(2) Das Studienseminar bestimmt auf Vorschlag des Prüflings in Abstimmung mit ihren oder seinen Ausbilderinnen oder Ausbildern den Termin für die Durchführung der beiden Unterrichtsproben.

(3) Der Prüfling bestimmt für jede Unterrichtsprobe im Einvernehmen mit der Ausbildungslehrkraft oder mit der Leiterin oder dem Leiter der Ausbildungsschule das Thema der jeweiligen Unterrichtsprobe und leitet es unverzüglich schriftlich über die Ausbilderin oder den Ausbilder dem Studienseminar zur Bestätigung zu.

(4) Der Prüfling legt vor Beginn der Unterrichtsprobe jedem Mitglied des Prüfungsausschusses eine auf den notwendigen Umfang beschränkte schriftliche Planung der Unterrichtsprobe vor, von der jeweils ein Exemplar zur Prüfungsakte zu nehmen ist.

(5) Nach der Unterrichtsprobe hat der Prüfling Gelegenheit, sich zur Unterrichtsprobe zu äußern.

(6) Der Prüfungsausschuss bewertet unter Berücksichtigung der schriftlichen Planung der Unterrichtsprobe die in ihr erbrachte Prüfungsleistung hinsichtlich der Fachlichkeit des Unterrichts sowie seiner pädagogischen und didaktisch-methodischen Gestaltung mit einer Note.

(7) Die erteilte Note ist dem Prüfling von der oder dem Vorsitzenden des Prüfungsausschusses mitzuteilen und mündlich zu begründen.
Der Prüfungsausschuss der zweiten Unterrichtsprobe setzt außerdem aus den beiden Noten für die Unterrichtsproben die Gesamtnote für die Unterrichtsproben gemäß § 23 Absatz 3 fest, die von der oder dem Vorsitzenden dem Prüfling mitzuteilen und in die Niederschrift über die zweite Unterrichtsprobe aufzunehmen ist.

(8) Stellt der Prüfungsausschuss gemäß Absatz 6 fest, dass

1.  die Note für eine Unterrichtsprobe nicht mindestens mangelhaft (5) ist oder
2.  die Note für die unterrichtspraktische Prüfung nicht mindestens ausreichend (4) ist,

wird das Prüfungsverfahren abgebrochen und die Staatsprüfung für nicht bestanden erklärt.

#### § 27 Mündliche Prüfung

(1) Die mündliche Prüfung schließt das Prüfungsverfahren ab.
Sie kann als Gruppenprüfung mit bis zu drei Prüflingen, die das gleiche Lehramt mit identischer Fächerkombination anstreben, durchgeführt werden.
Die Dauer der mündlichen Prüfung soll 120 Minuten nicht überschreiten.
Wird die mündliche Prüfung mit weniger als drei Prüflingen durchgeführt, ist die Dauer der mündlichen Prüfung anteilig zu reduzieren.

(2) Der thematische Rahmen der mündlichen Prüfung wird auf Vorschlag der Prüflinge oder des Prüflings durch die Leiterin oder den Leiter des Studienseminars spätestens zwei Wochen vor dem Prüfungstermin festgelegt.
Er ist auf mehrere Kompetenzbereiche gemäß § 13 Satz 2 auszurichten.

(3) Die Vorsitzende oder der Vorsitzende gibt den Prüflingen oder dem Prüfling zu Beginn der mündlichen Prüfung Gelegenheit zu einer kurzen thematischen Einführung.

(4) Der Prüfungsausschuss bewertet die Prüfungsleistung jedes Prüflings hinsichtlich ihrer wissenschaftlichen Fundierung, der Komplexität der Problemdarstellung, des fachlichen Gehalts der Ausführungen, der Folgerichtigkeit der Gedankenführung, der Eigenständigkeit des Urteils und der Kommunikationsfähigkeit mit einer Note.
Die Note ist dem Prüfling von der oder dem Vorsitzenden mitzuteilen und mündlich zu begründen.

#### § 28 Zuhörende

(1) Die Unterrichtsproben und die mündliche Prüfung sind nicht öffentlich.
Abweichend davon können ohne Stimmrecht und Beratungsrecht schulfachliche und lehrerbildungsfachliche Vertreterinnen oder Vertreter der Schulaufsichtsbehörden an den Unterrichtsproben und der mündlichen Prüfung sowie den Beratungen und Entscheidungen der jeweiligen Prüfungsausschüsse teilnehmen.
Außerdem kann, soweit die Zustimmung der Prüflinge oder des Prüflings vorliegt, jeweils eine Vertrauensperson der Lehramtskandidatinnen und Lehramtskandidaten gemäß dem Landespersonalvertretungsgesetz an der jeweiligen Prüfung sowie der Beratung und Entscheidung des Prüfungsausschusses teilnehmen.

(2) Die oder der Vorsitzende des Prüfungsausschusses kann mit Zustimmung der Prüflinge oder des Prüflings bis zu zwei Lehramtskandidatinnen und Lehramtskandidaten, die sich im zweiten Ausbildungsabschnitt befinden, als Zuhörende zulassen.
Die Zulassung bezieht sich nicht auf die Teilnahme an Beratungen und an der Entscheidungsfindung des Prüfungsausschusses sowie auf die Bekanntgabe des Prüfungsergebnisses.

(3) Wenn die ordnungsgemäße Durchführung der Prüfung gefährdet ist, kann die oder der Vorsitzende des Prüfungsausschusses Zuhörende von der weiteren Teilnahme ausschließen.
Für Zuhörende gilt § 25 Absatz 2 Satz 2 entsprechend.

#### § 29 Gesamtnote der Staatsprüfung

(1) Der Prüfungsausschuss für die mündliche Prüfung ermittelt gemäß § 23 Absatz 3 aus

1.  der dreifach gewichteten Note der Beurteilung,
2.  der zweifach gewichteten Note der mündlichen Prüfung und
3.  der fünffach gewichteten Note der unterrichtspraktischen Prüfung

die Gesamtnote der Staatsprüfung und setzt diese fest.

(2) Die Staatsprüfung ist nicht bestanden, wenn

1.  die Gesamtnote der Staatsprüfung oder
2.  die Note der unterrichtspraktischen Prüfung oder
3.  die Note der mündlichen Prüfung

nicht mindestens ausreichend (4,0) ist.
Sie ist endgültig nicht bestanden, wenn die Wiederholungsprüfung nicht bestanden wurde.

(3) Die oder der Vorsitzende des Prüfungsausschusses für die mündliche Prüfung teilt dem Prüfling am Ende der mündlichen Prüfung das Ergebnis der Staatsprüfung mit.

(4) Das Studienseminar gibt das Ergebnis der Staatsprüfung dem Prüfling schriftlich bekannt.
Wurde die Staatsprüfung nicht bestanden, ist dem Prüfling außerdem die Entscheidung über die Verlängerung des Vorbereitungsdienstes gemäß § 33 Absatz 2 mitzuteilen.
Wurde die Staatsprüfung endgültig nicht bestanden, erfolgt die Bekanntgabe des Ergebnisses der Staatsprüfung unverzüglich nach Abschluss des Prüfungsverfahrens.

(5) Bei Entscheidungen gemäß den §§ 30 bis 32 wird das Ergebnis der Staatsprüfung durch das Studienseminar festgestellt.

#### § 30 Nichterbringen von Prüfungsleistungen

(1) Die Staatsprüfung wird für nicht bestanden erklärt, wenn aus vom Prüfling zu vertretenden Gründen

1.  die Planungen für die Unterrichtsproben gemäß § 26 Absatz 4 nicht rechtzeitig vorgelegt werden oder
2.  der Termin für eine Unterrichtsprobe der unterrichtspraktischen Prüfung oder für die mündliche Prüfung versäumt wird.

(2) Soweit vom Prüfling nicht selbst zu verantwortende Gründe für das Nichterbringen einer Prüfungsleistung vorliegen, sind diese dem Studienseminar unverzüglich nach deren Bekanntwerden schriftlich mitzuteilen.
Wird das Nichterbringen einer Prüfungsleistung mit Krankheit begründet, ist ein ärztliches Attest beizubringen.
Im begründeten Einzelfall kann die Vorlage eines amtsärztlichen Attests verlangt werden.

#### § 31 Rücktritt von der Staatsprüfung

(1) Der Prüfling kann aus schwerwiegenden Gründen den Rücktritt vom Prüfungsverfahren oder von Teilen der Staatsprüfung erklären.
§ 30 Absatz 2 gilt entsprechend.
Über die Genehmigung des Rücktritts entscheidet die Leiterin oder der Leiter des Studienseminars.
Wird der Rücktritt genehmigt, gilt die Prüfung oder der Prüfungsteil als nicht unternommen.

(2) Erfolgt der Rücktritt ohne Genehmigung, gilt die Staatsprüfung als nicht bestanden.
Dies gilt entsprechend, wenn gleichzeitig der Antrag auf Entlassung aus dem Vorbereitungsdienst gestellt wird.

(3) Bei einem genehmigten Rücktritt bestimmt das Studienseminar den Termin für die Wiederaufnahme des Prüfungsverfahrens oder den Termin für die Durchführung des betreffenden Prüfungsteils.
Soweit für noch zu erbringende Prüfungsleistungen die Prüfungsthemen vor dem Rücktritt festgelegt wurden, sind jeweils neue Themenstellungen festzulegen.
Satz 1 gilt nicht, wenn ein Prüfling aus dem Vorbereitungsdienst entlassen oder unter Wegfall der Dienstbezüge beurlaubt wird.
Das Prüfungsverfahren kann auf Antrag des Prüflings innerhalb von fünf Jahren nach der Genehmigung des Rücktritts an der Stelle wieder aufgenommen werden, an der es unterbrochen wurde.
Nach Ablauf der Frist gemäß Satz 4 wird das Prüfungsverfahren endgültig eingestellt und die Prüfung für endgültig nicht bestanden erklärt.

(4) Bei Wiederaufnahme des Vorbereitungsdienstes innerhalb von fünf Jahren nach Genehmigung des Rücktritts ist das Prüfungsverfahren an der Stelle wieder aufzunehmen, an der es unterbrochen wurde.
In diesem Fall richtet sich die Dauer des Vorbereitungsdienstes nach der Anzahl der noch zu erbringenden Prüfungsleistungen.
Sie soll mindestens die Differenz aus der Gesamtdauer des Vorbereitungsdienstes und den bereits absolvierten Zeiten im Vorbereitungsdienst, höchstens jedoch sechs Monate betragen.
Die Entscheidung trifft die Leiterin oder der Leiter des Studienseminars.

#### § 32 Ordnungswidriges Verhalten

(1) Die oder der Vorsitzende eines Prüfungsausschusses kann einen Prüfling, der im Zusammenhang mit der Unterrichtsprobe oder der mündlichen Prüfung zu täuschen versucht oder sich ein anderes erhebliches ordnungswidriges Verhalten zuschulden kommen lässt, von der Fortsetzung der jeweiligen Prüfung ausschließen.

(2) Wird ein ordnungswidriges Verhalten gemäß Absatz 1 festgestellt, entscheidet auf Grundlage der Umstände und Schwere die Leiterin oder der Leiter des Studienseminars, ob

1.  einzelne Prüfungsteile der Staatsprüfung zu wiederholen sind,
2.  die Prüfungsleistung, die sich auf das ordnungswidrige Verhalten bezieht, mit der Note ungenügend (6) bewertet und entsprechend in die Ermittlung der jeweiligen Gesamtnote einbezogen wird oder
3.  die Staatsprüfung für nicht bestanden erklärt wird.

(3) In besonders schweren Fällen ordnungswidrigen Verhaltens kann die Leiterin oder der Leiter des Studienseminars eine Wiederholungsprüfung ausschließen.

(4) Wird innerhalb einer Frist von fünf Jahren nach Ausstellung des Zeugnisses über die Staatsprüfung ein ordnungswidriges Verhalten durch entsprechende Tatsachen bekannt, kann die Staatsprüfung durch das für Schule zuständige Ministerium als für nicht bestanden erklärt werden.
In diesem Fall ist das Zeugnis einzuziehen und eine entsprechende Bescheinigung gemäß § 35 Absatz 2 auszustellen.

#### § 33 Wiederholung der Staatsprüfung

(1) Wurde die Staatsprüfung nicht bestanden oder für nicht bestanden erklärt, kann sie höchstens einmal wiederholt werden.
Für das Ablegen der Wiederholungsprüfung auf Grund einer unternommenen und gemäß § 26 Absatz 7 nicht bestandenen Staatsprüfung ist der Vorbereitungsdienst zu verlängern.
Die Dauer der Verlängerung soll mindestens vier und höchstens sechs Monate betragen.
In dieser Zeit gilt der Prüfling als in die Prüfung eingetreten.
Über die Dauer der Verlängerung des Vorbereitungsdienstes entscheidet die Leiterin oder der Leiter des Studienseminars.
Die Entscheidung ist dem Prüfling schriftlich mitzuteilen.

(2) Wurde eine Staatsprüfung gemäß § 30 Absatz 1, § 31 Absatz 2 oder § 32 Absatz 2 für nicht bestanden erklärt, ist der Vorbereitungsdienst nicht zu verlängern.
Auf Antrag des Prüflings kann die Staatsprüfung innerhalb von fünf Jahren außerhalb des Ausbildungszeitraums gemäß § 31 Absatz 1 wiederholt werden.

#### § 34 Prüfungsakten

(1) Die Prüfungsakten für die Staatsprüfung sind bei dem für Schule zuständigen Ministerium mit Ausnahme der Zeugniskopien für die Dauer von zehn Jahren aufzubewahren.
Die Aufbewahrungsfrist für die Zeugniskopien beträgt 50 Jahre.

(2) Auf Antrag ist dem Prüfling nach Bekanntgabe des Ergebnisses der Staatsprüfung persönliche Einsicht in seine Prüfungsakte bei dem für Schule zuständigen Ministerium zu gewähren.
Ort, Dauer und Zeitpunkt der Einsichtnahme werden von dem für Schule zuständigen Ministerium bestimmt.
Der Prüfling kann bei der Einsichtnahme Aufzeichnungen über den Inhalt der Akten sowie Kopien fertigen.
Die Einsichtnahme ist zu beaufsichtigen.

(3) Wenn ein Prüfungsteil nicht bestanden wurde, ist dem Prüfling auf Antrag vor der Wiederholungsprüfung die Einsicht in die Prüfungsakte zu gewähren.
Absatz 2 Satz 2 bis 4 gilt entsprechend.

#### § 35 Zeugnisse und Bescheinigungen

(1) Über die bestandene Staatsprüfung ist von dem für Schule zuständigen Ministerium ein Zeugnis zu fertigen, das insbesondere

1.  die Bezeichnung des Lehramtes und gegebenenfalls des Schwerpunktes einschließlich der Fächer, Lernbereiche oder Fachrichtungen, für das die Befähigung erworben wurde,
2.  die Noten der beiden Unterrichtsproben, die Note der unterrichtspraktischen Prüfung und der mündlichen Prüfung und
3.  die Gesamtnote der Staatsprüfung

umfasst.
Außerdem ist in dem Zeugnis die Bezeichnung des Faches oder der Fachrichtung des Hochschulabschlusses oder der Ersten Staatsprüfung oder ihr gleichgestellten ausländischen Lehrerqualifikation aufzunehmen, für das oder die keine Ausbildung im Vorbereitungsdienst erfolgte.
Das Zeugnis ist auf den Tag zu datieren, an dem das Ergebnis der Staatsprüfung dem Prüfling mitgeteilt wurde.

(2) Über eine nicht bestandene Staatsprüfung ist von dem für Schule zuständigen Ministerium eine Bescheinigung auszustellen.
Die Bescheinigung ist jeweils auf den Tag zu datieren, an dem die letzte Prüfungsleistung erbracht worden ist.

#### § 36 Abweichende Bestimmungen für das Fach Religion

(1) Für Prüflinge mit dem Fach Religion gilt

1.  § 21 mit der Maßgabe, dass nur die Unterrichtsprobe im Ausbildungsfach gemäß § 19 Nummer 1 abzulegen ist,
2.  § 26 mit der Maßgabe, dass
    1.  an die Stelle einer Unterrichtsprobe die für das Fach Religion in Verantwortung der Kirchen und Religionsgemeinschaften abzulegende schulpraktische Prüfung tritt und
    2.  sich die Gesamtnote für die Unterrichtsproben aus der Note für die Unterrichtsprobe im Ausbildungsfach gemäß Nummer 1 und der Note für die unterrichtspraktische Prüfung gemäß Buchstabe a zusammensetzt und
    3.  die Note für die unterrichtspraktische Prüfung im Fach Religion oder das Nichtbestehen dieser Prüfung in die Niederschrift der zweiten Unterrichtsprobe aufzunehmen ist,
3.  § 27 mit der Maßgabe, dass
    1.  sich die mündliche Prüfung nur auf die Ausbildungsinhalte der Fachausbildungen des jeweiligen Ausbildungsfaches bezieht,
    2.  die mündliche Prüfung als Einzelprüfung mit einer Dauer von höchstens 30 Minuten durchzuführen ist,
    3.  die gegebenenfalls verlangte mündliche Prüfung im Fach Religion in Verantwortung der jeweiligen Kirche oder Religionsgemeinschaft durchgeführt wird,
    4.  sich in diesem Fall die Note für die mündliche Prüfung aus der Gesamtnote der einfach gewichteten Note der mündlichen Prüfung im Fach Religion und der zweifach gewichteten Note der mündlichen Prüfung im Ausbildungsfach gemäß § 19 Nummer 1 ergibt,
4.  § 29 Absatz 2 mit der Maßgabe, dass die Staatsprüfung auch nicht bestanden ist, wenn die Prüfung der jeweiligen Kirche oder Religionsgemeinschaft im Fach Religion nicht bestanden wurde, und
5.  § 35 mit der Maßgabe, dass im Zeugnis über die Staatsprüfung ein Hinweis auf das Zeugnis der jeweiligen Kirche oder Religionsgemeinschaft, in dem die gemäß Nummer 3 und 4 zu berücksichtigenden Prüfungsleistungen ausgewiesen sind, aufgenommen wird.

### Abschnitt 4  
Besondere Bestimmungen bei eingeschränktem Schul- und Ausbildungsbetrieb

#### § 37 Maßnahmen

Soweit Schulschließungen oder eingeschränkter Regelbetrieb der Schulen aufgrund behördlicher Maßnahmen eine Ausbildung an Schulen gemäß § 17 nicht zulassen oder einschränken, gilt Abschnitt 4 dieser Verordnung.
Dies gilt auch für die Durchführung der Staatsprüfung gemäß Abschnitt 3 dieser Verordnung.

#### § 38 Ausbildung an Schulen und Studienseminaren

(1) Die schulpraktische Ausbildung gemäß § 17 Absatz 4 wird im Anteil selbstständiger Unterricht durch Lehr- und Lernformen des Distanzunterrichts ergänzt und den spezifischen Bedingungen an der Ausbildungsschule angepasst.
Unter asynchronen Formen werden Lehr- und Lernformen verstanden, die sich durch eine räumliche Trennung und eine zeitlich deutlich verzögerte Reaktion und Interaktion der Schülerinnen und Schüler und der Lehrkraft auszeichnen.
Dabei werden die betroffenen Lehramtskandidatinnen und Lehramtskandidaten von den Ausbildungslehrkräften und den Ausbilderinnen und Ausbildern der Studienseminare unterstützt.

(2) Die seminaristische Ausbildung einschließlich der Unterrichtshospitationen durch Ausbilderinnen und Ausbilder der Studienseminare wird bei Beeinträchtigungen des Ausbildungsbetriebs oder des Schulbetriebs in geeigneter Form kontinuierlich fortgesetzt.

(3) Kann aufgrund von Einschränkungen des Ausbildungsbetriebs an Schulen der selbstständige Unterricht nicht mindestens zu 75 Prozent in Präsenzform oder in Distanzform durch Nutzung digitaler Kommunikationsmedien in gemeinsamen Lehr- und Lernveranstaltungen durchgeführt werden oder können Rückmeldungen zum Unterricht und zum Ausbildungsstand gemäß § 17 Absatz 6 nicht ausreichend gewährleistet werden, ist auf Antrag der Lehramtskandidatin oder des Lehramtskandidaten eine Verlängerung des Vorbereitungsdienstes um bis zu vier Monate möglich.
Der Antrag ist frühestens drei Monate vor Ablauf des Vorbereitungsdienstes und spätestens eine Woche vor Meldung zur Prüfung schriftlich oder elektronisch bei der Leiterin beziehungsweise dem Leiter des Studienseminars zu stellen, die über die Verlängerung des Vorbereitungsdienstes entscheiden.

#### § 39 Prüfungsersatzleistungen innerhalb der Staatsprüfung

(1) Unterrichtsproben können im Präsenzunterricht oder im Distanzunterricht per Videokonferenz stattfinden.
Soweit die Durchführung der beiden Unterrichtsproben als Teile der unterrichtspraktischen Prüfung der Staatsprüfung nicht oder nur teilweise möglich ist, werden diese jeweils durch eine Prüfungsersatzleistung ersetzt.

(2) Die Prüfungsersatzleistung bezieht sich inhaltlich auf die Lerngruppe und das Ausbildungsfach der zu ersetzenden Unterrichtsprobe.
Sie besteht aus einer schriftlichen Unterrichtsplanung und einem Einzelprüfungsgespräch im Umfang von 45 Minuten.

(3) Die schriftliche Unterrichtsplanung bezieht sich auf eine

1.  Unterrichtsstunde oder
2.  Unterrichtssequenz im zeitlichen Umfang von mindestens drei Unterrichtsstunden.

Die schriftliche Planung der Unterrichtsstunde entspricht den Anforderungen für eine Unterrichtsprobe.
Der Prüfling bestimmt für seine jeweilige Prüfungsersatzleistung im Einvernehmen mit der Fachausbilderin oder dem Fachausbilder das Thema und leitet dieses unverzüglich dem Studienseminar zur Bestätigung zu.

(4) Der Prüfling legt in der Regel eine Woche vor dem Termin der Prüfungsersatzleistung jedem Mitglied des Prüfungsausschusses eine auf den notwendigen Umfang beschränkte schriftliche Planung vor, von der jeweils ein Exemplar zur Prüfungsakte zu nehmen ist.

(5) Zum Beginn des Prüfungsgesprächs wird dem Prüfling die Gelegenheit zu einer kurzen erläuternden Einführung zur schriftlichen Unterrichtsplanung gegeben.
Gegenstand des anschließenden Prüfungsgesprächs sind vertiefende pädagogische, (lern-)psychologische, fachliche, (fach-)didaktische und methodische Aspekte des gewählten Themas mit kontinuierlichem Bezug zur Praxis des Unterrichts.

(6) Der Prüfungsausschuss bewertet unter Berücksichtigung der schriftlichen Planung die Prüfungsersatzleistung hinsichtlich der pädagogischen, (lern-)psychologischen, fachlichen, (fach-)didaktischen und methodischen Reflexionsfähigkeit des Prüflings mit einer Note.

#### § 40 Prüfungsausschüsse

(1) Für jeden Prüfling wird jeweils ein Prüfungsausschuss für die Prüfungsersatzleistung und die mündliche Prüfung gebildet.
Dem Prüfungsausschuss gehören

1.  als Vorsitzende oder Vorsitzender eine schul- oder ausbildungsfachliche Vertreterin oder ein Vertreter der Schulbehörden sowie
2.  zwei Ausbilderinnen oder Ausbilder des Studienseminars

an.
Eine Ausbilderin oder ein Ausbilder des Studienseminars kann jeweils durch eine Vertreterin oder einen Vertreter der Ausbildungsschule ersetzt werden.

(2) Die oder der Vorsitzende ist für den ordnungsgemäßen Ablauf der Prüfung verantwortlich.
Erscheint ein Mitglied des Prüfungsausschusses nicht, entscheidet die oder der Vorsitzende im Benehmen mit dem Prüfling über die Durchführung oder terminliche Verlagerung der Prüfung.

(3) Die Prüfungsnote wird auf Vorschlag der Ausbilderin oder des Ausbilders des jeweiligen Faches mit der Mehrheit der Stimmen festgelegt.
Eine Stimmenthaltung ist nicht zulässig.
Bei Stimmengleichheit entscheidet die Stimme der oder des Vorsitzenden.
Die Note ist dem Prüfling von der oder dem Vorsitzenden des Prüfungsausschusses mitzuteilen und mündlich zu begründen.

(4) Die Teilnahme von Zuhörenden ist für alle unterrichtspraktischen Prüfungen sowie für die mündliche Prüfung ausgeschlossen.

### Abschnitt 5  
Übergangs- und Schlussvorschriften

#### § 41 Übergangs- und Schlussvorschriften

(1) Lehramtskandidatinnen und Lehramtskandidaten, die ihren Vorbereitungsdienst vor dem Inkrafttreten dieser Verordnung aufgenommen haben, absolvieren diesen auf der Grundlage der Ordnung für den Vorbereitungsdienst vom 14.
November 2016 (GVBl.
II Nr.
64) und legen die Staatsprüfung nach deren Regelungen ab.
Abschnitt 4 dieser Verordnung mit besonderen Bestimmungen bei eingeschränktem Schul- und Ausbildungsbetrieb gilt für diese Lehramtskandidatinnen und Lehramtskandidaten gleichermaßen.

(2) Lehramtskandidatinnen oder Lehramtskandidaten, die ihr lehramtsbezogenes Studium gemäß § 18 Absatz 4 des Brandenburgischen Lehrerbildungsgesetzes vom 18. Dezember 2012 (GVBl. I Nr. 45) abgeschlossen haben und nach dem 31.
Dezember 2018 in den Vorbereitungsdienst aufgenommen werden, absolvieren den Vorbereitungsdienst auf der Grundlage dieser Verordnung mit der Maßgabe, dass sie das Lehramt für die Bildungsgänge der Sekundarstufe I und der Primarstufe an allgemeinbildenden Schulen oder das Lehramt an Gymnasien erwerben.
Der Vorbereitungsdienst muss bis zum 31.
Dezember 2025 abgeschlossen werden.

#### § 42 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2019 in Kraft.
Gleichzeitig tritt die Ordnung für den Vorbereitungsdienst vom 14.
November 2016 (GVBl.
II Nr.
64) außer Kraft.

Potsdam, den 19.
März 2019

Die Ministerin für Bildung,  
Jugend und Sport

Britta Ernst