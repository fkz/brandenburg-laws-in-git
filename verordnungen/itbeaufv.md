## Verordnung über die Befugnisse und Einzelheiten der Durchführung der Aufgaben der oder des IT-Beauftragten (IT-Beauftragten-Verordnung - ITBeaufV)

Auf Grund des § 13 Absatz 3 des Brandenburgischen E-Government-Gesetzes vom 23. November 2018 (GVBl.
I Nr. 28) verordnet der Minister des Innern und für Kommunales:

#### § 1 Befugnisse der oder des IT-Beauftragten

(1) Die oder der IT-Beauftragte kann mit Genehmigung des Ministerpräsidenten an Sitzungen der Landesregierung teilnehmen, soweit der Beratungsgegenstand sich auf die in Absatz 2 genannten Aufgaben oder ihrer Umsetzung dienenden Rechts- oder Verwaltungsvorschriften bezieht.

(2) Sie oder er hat das Recht, für die Wahrnehmung der in § 13 Absatz 2 des Brandenburgischen E-Government-Gesetzes genannten Aufgaben bei den Ministerien des Landes die erforderlichen Informationen einzuholen.
Im Rahmen der Zusammenarbeit des Landes und der Kommunen nach § 14 Absatz 1 Satz 1 des Brandenburgischen E-Government-Gesetzes wird die oder der IT-Beauftragte von den Gemeinden, Ämtern und Gemeindeverbänden sowie den der Aufsicht des Landes unterstehenden juristischen Personen des öffentlichen Rechts bei der Wahrnehmung der Aufgaben nach § 13 Absatz 2 des Brandenburgischen E-Government-Gesetzes durch Bereitstellung der erforderlichen Informationen unterstützt.
Die Grundsätze der Amtshilfe gelten entsprechend.

(3) Die oder der IT-Beauftragte ist frühzeitig bei der Ausarbeitung von Rechts- und Verwaltungsvorschriften sowie in sonstigen Angelegenheiten zu beteiligen, wenn diese Fragen des E-Government oder der Informationstechnik von ressortübergreifender Bedeutung berühren.
Die Planung der Umsetzung von IT-Vorhaben in der Landesverwaltung mit ressortübergreifender Bedeutung erfolgt im Einvernehmen mit der oder dem IT-Beauftragten.
Die oder der IT-Beauftragte ist regelmäßig über den Stand dieser Vorhaben zu informieren.

#### § 2 Aufgabendurchführung

Bei der Durchführung der in § 13 Absatz 2 des Brandenburgischen E-Government-Gesetzes genannten Aufgaben stimmt sich die oder der IT-Beauftragte kontinuierlich mit den Ressorts der Landesregierung und der Staatskanzlei ab.
Sie oder er gewährleistet die Beteiligung des IT-Rates nach § 15 Absatz 2 des Brandenburgischen E-Government-Gesetzes.

#### § 3 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 27.
Juni 2019

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter