## Bekanntmachung der Geschäftsbereiche der obersten Landesbehörden

Gemäß § 5 Absatz 3 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), der durch Artikel 2 Nummer 7 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28) geändert worden ist, lege ich die Geschäftsbereiche der obersten Landesbehörden mit Wirkung vom 20. November 2019 fest und gebe sie nachfolgend bekannt.

Zu diesem Zeitpunkt sind auch die in Gesetzen und Rechtsverordnungen der bisher zuständigen obersten Landesbehörde zugewiesenen Zuständigkeiten gemäß § 5 Absatz 4 des Landesorganisationsgesetzes auf die nunmehr zuständige oberste Landesbehörde übergegangen.

### Geschäftsbereiche der obersten Landesbehörden

#### I.
Geschäftsbereich der Ministerpräsidentin oder des Ministerpräsidenten (Stk)

1.  Regierungsplanung und ressortübergreifendes Controlling
2.  Koordination strategischer Gesamtrahmen Hauptstadtregion, Regionale Wachstumskerne (RWK), Beziehungen zu angrenzenden Metropolregionen; Koordinierungsstelle des Nachhaltigkeitsbeirats
3.  Koordination Entwicklung und Steuerung der „Regionalen“; Regionalkoordinatorinnen und Regionalkoordinatoren
4.  Lausitzbeauftragte oder Lausitzbeauftragter der Ministerpräsidentin oder des Ministerpräsidenten, Lausitzkoordination
5.  Politische Koordinierung, strategische Projekte von besonderer Bedeutung
6.  Kabinett, Landtag, Fraktionen
7.  Beziehungen zum Bund und zu den Ländern, insbesondere zu Berlin, Vertretung des Landes beim Bund sowie im Bundesrat und dessen Gremien (einschließlich Koordinierung der Landesposition), Beziehungen zu Parteien, Organisationen und Verbänden
8.  Internationale Beziehungen, Koordination der Aufgaben des Koordinators der Bundesregierung für die deutsch-polnische zwischengesellschaftliche und grenznahe Zusammenarbeit
9.  Strategie- und Maßnahmenentwicklung zur Gestaltung des demografischen Wandels
10.  Presse- und Öffentlichkeitsarbeit, Koordinierung der Presse- und Öffentlichkeitsarbeit der Landesregierung und des Landesmarketings
11.  Grundsatzfragen der Medienpolitik, Rundfunkangelegenheiten
12.  Koordinierungsstelle „Ehrenamt und bürgerschaftliches Engagement“, Bürgerangelegenheiten, Bürgerdialoge, Koordinierungsstelle Bürgerbeteiligung, Ordensangelegenheiten/Ehrungen/Auszeichnungen, Schirmherrschaften des Ministerpräsidenten
13.  Protokoll
14.  Gnadensachen soweit vorbehalten
15.  Organisation der Landesregierung
16.  Koordinierungsstelle Tolerantes Brandenburg/Bündnis für Brandenburg
17.  Koordinierungsstelle Zukunftsstrategie Digitales Brandenburg

#### II.
Geschäftsbereich des Ministeriums des Innern und für Kommunales (MIK)

1.  Verfassung (Staatsorganisation), Staatsgebiet und Hoheitszeichen, Wahlen und Volksabstimmungen, Europäischer Verbund für territoriale Zusammenarbeit
2.  Verwaltungsrecht, Verwaltungskostenrecht, Verwaltungsvollstreckungsrecht, Datenschutz, Akteneinsichts- und Informationszugangsrecht
3.  Organisation der Landesverwaltung
4.  Zentrale Steuerung der Verwaltungsmodernisierung, Koordinierung des landesweiten Personalmanagements, Zentrale Servicestelle Gesundheitsmanagement
5.  Leitstelle Bürokratieabbau, Zentrale Normprüfstelle
6.  Interministerielle Zusammenarbeit, Gemeinsame Geschäftsordnung, Behördliches Vorschlagswesen
7.  Zentrale Koordinierung und strategische Planung und Steuerung des E-Government\- und IT\-Einsatzes in der Landesverwaltung (CPIO und E-Government- und IT-Leitstelle), insbesondere OZG\-Umsetzung, LeiKa, Open-Data und landesweite IT-Sicherheit
8.  Recht des öffentlichen Dienstes einschließlich der Mitwirkung an der Rechtssetzung des finanziellen Dienstrechts, Grundsatzfragen der Fürsorge und der Wahrung der Einheitlichkeit des Rechts des öffentlichen Dienstes, Ministerrecht, Personalvertretungsrecht, Landespersonalausschuss; Sonderversorgungssystem der Volkspolizei, der Feuerwehr und des Strafvollzugs der ehemaligen DDR
9.  Arbeits-, Tarif- und Sozialversicherungsrecht im Recht des öffentlichen Dienstes, Vertretung des Landes in der Tarifgemeinschaft deutscher Länder
10.  Staatsangehörigkeitsangelegenheiten, Personenstandswesen, Namensänderungsangelegenheiten
11.  Melde-, Pass- und Ausweiswesen
12.  Ausländer- und Asylrecht, Geschäftsstelle der Härtefallkommission, Zuwanderungspolitik (gemeinsam mit MSGIV)
13.  Öffentliche Sicherheit, Polizei, Allgemeines Ordnungsrecht
14.  Verfassungsschutz
15.  SED\-Unrechtsbereinigung, Stasi-Unterlagen-Gesetz
16.  Brand-, Katastrophen- und Zivile Verteidigung sowie Koordinierungszentrum Krisenmanagement der Landesregierung
17.  Kommunales Verfassungs-, Abgaben-, Haushalts-, Dienst- und Disziplinarrecht einschließlich Kommunalaufsicht, interkommunale Zusammenarbeit sowie Bewirtschaftung und Verteilung der Mittel nach § 16 des Finanzausgleichsgesetzes
18.  Militärangelegenheiten
19.  Vermessungs- und Katasterwesen, Geodateninfrastruktur, Gutachterausschüsse für Grundstückswerte, Immobilienwertermittlung
20.  Glücksspielwesen
21.  Angelegenheiten der Stiftungen bürgerlichen Rechts
22.  Angelegenheiten der Gräber der Opfer von Krieg und Gewaltherrschaft und Friedhofswesen
23.  Enteignungsrecht
24.  Koordinierung des Beschaffungswesens in der Landesverwaltung, technische Betreuung des Vergabeportales (einschließlich Vergabemarktplatz) und Aufsicht über den Vergabemarktplatz
25.  Amtliche Statistik
26.  Ressortübergreifende Aufgaben der Aus- und Fortbildung für die Landesverwaltung; Laufbahn- und Berufsausbildung im Vermessungswesen
27.  Laufbahnausbildungen des mittleren und gehobenen allgemeinen Verwaltungsdienstes und des gehobenen technischen Verwaltungsinformatikdienstes des Landes
28.  Korruptionsprävention in der Landesverwaltung Brandenburg

### III.
Geschäftsbereich des Ministeriums der Justiz (MdJ)

1.  Verfassungsrecht (soweit nicht MIK), Federführung in verfassungsgerichtlichen Verfahren
2.  Völkerrecht
3.  Mitwirkung in grundsätzlichen Rechtsfragen, Rechtsförmlichkeitsprüfung in Bezug auf Gesetz- und Verordnungsentwürfe der Landesregierung, Vertragsförmlichkeitsprüfung in Bezug auf Staatsverträge und Verwaltungsabkommen
4.  Rechts- und Justizpolitik
5.  Gerichtsverfassungsrecht, Prozessrecht, Richterrecht, Rechtspflegerrecht, Juristenausbildungsrecht
6.  Bürgerliches Recht, Zwangsvollstreckungs-, Insolvenzrecht, Grundbuchwesen
7.  Strafrecht, strafrechtliche Rehabilitierung und Entschädigung, Opferschutz und -hilfe im Bereich strafbarer Handlungen und Strafverfahren, Gnadenwesen
8.  Straf- und Justizvollzug (außer Abschiebungshaft)
9.  Sämtliche Verwaltungsangelegenheiten (außer Justizkassenangelegenheiten) im Bereich der ordentlichen Gerichtsbarkeit, der Staatsanwaltschaften, der Verwaltungsgerichtsbarkeit, der Arbeitsgerichtsbarkeit, der Sozialgerichtsbarkeit und der Finanzgerichtsbarkeit; Fachaufsicht über die Staatsanwaltschaften
10.  Bewährungshilfe und Gerichtshilfe
11.  Zwischenstaatliche Angelegenheiten der Rechtspflege
12.  Angelegenheiten der Rechtsanwältinnen und Rechtsanwälte sowie der Notarinnen und Notare
13.  Bereinigung und Dokumentation des Landesrechts; Redaktion Gesetz- und Verordnungsblatt II, Amtsblatt, Justizministerialblatt
14.  Ausbildung und Prüfung des juristischen Nachwuchses und der Anwärterinnen und Anwärter für die Laufbahnen der in Nummer 9 genannten Gerichtsbarkeiten

### IV.
Geschäftsbereich des Ministeriums der Finanzen und für Europa (MdFE)

1.  Allgemeine Finanzpolitik und öffentliche Finanzwirtschaft, insbesondere
    *   Aufstellung und Ausführung des Gesamthaushalts; Finanzplanung; Haushalts-, Kassen- und Rechnungswesen; Haushaltsrecht
    *   Finanzbeziehungen zum Bund, zu den Ländern und Gemeinden sowie zur Europäischen Union, Kommunaler Finanzausgleich, Kommunale Investitionsprogramme
    *   Geld-, Kredit-, Schuldenmanagement
2.  Bescheinigende Stelle für die Europäischen Fonds EGFL und ELER gemäß Artikel 9 der Verordnung (EU) Nr.
    1306/2013;  
    Prüfbehörde für die Europäischen Fonds EFRE, ESF, EMFF und INTERREG V A (Programm Brandenburg-Polen) in der Förderperiode 2014 bis 2020 gemäß Artikel 123, Absatz 4 in Verbindung mit Artikel 127 der Verordnung (EU) Nr.
    1303/2013 beziehungsweise Artikel 21 Absatz 1 in Verbindung mit Artikel 25 der Verordnung (EU) Nr.
    1299/2013
3.  Landesbeteiligungen, insbesondere Wahrnehmung der Anteilseignerrechte; Vergabe von Bürgschaften, Garantien und sonstigen Gewährleistungen; Vermögens- und Schuldenverwaltung
4.  Staatsaufsicht über die öffentlich-rechtlichen Kreditinstitute und über das Versorgungswerk der Mitglieder des Landtages Nordrhein-Westfalen und des Landtages Brandenburg und des Landtages Baden-Württemberg sowie über den Ostdeutschen Sparkassenverband
5.  Steuerwesen einschließlich Organisation und Informationswesen der Steuerverwaltung des Landes
6.  Personalbedarfsplanung
7.  Liegenschafts- und Gebäudemanagement; Landesbaumanagement (außer Landes- und Bundesfernstraßenbaumaßnahmen und wasserwirtschaftliche Baumaßnahmen)
8.  Grundsatzfragen des Besoldungs-, Versorgungs- und Beihilferechts einschließlich der besoldungsrechtlichen Nebengebiete des finanziellen Dienstrechts
9.  Festsetzung und Zahlbarmachung der Bezüge und Beihilfeleistungen; Berechnung und Zahlung von Reisekosten, Umzugskosten und Trennungsgeld; Berechnung und Gewährung von Schadenersatz sowie Unfall- und sonstige Fürsorgeleistungen
10.  Offene Vermögensfragen
11.  Angelegenheiten der Dienstkraftfahrzeuge
12.  Verteidigungslasten
13.  Ausbildung und Prüfung des höheren bautechnischen Verwaltungsdienstes des Landes (Fachrichtungen Hochbau, Maschinenbau und Elektrotechnik); Ausbildung und Prüfung des gehobenen und mittleren Dienstes der Steuerverwaltung des Landes, Laufbahnrecht für Steuerbeamtinnen und Steuerbeamte
14.  Europaangelegenheiten, Europapolitik, EU-Wirtschafts-und Finanzpolitik, Europarecht, Europapolitische Kommunikation, Entwicklungspolitik
15.  Beauftragte oder Beauftragter des Landes für Brandenburgisch-Polnische Beziehungen
16.  Vertretung des Landes bei der Europäischen Union
17.  Interregionale und grenzüberschreitende Zusammenarbeit, Internationales (soweit nicht Staatskanzlei), INTERREG, Internationalisierungsstrategie
18.  Koordinierungsstelle für die EU-Strukturfonds;  
    Benennung der Verwaltungsbehörden und der Bescheinigungsbehörden für EFRE, ESF, EMFF und INTERREG V A (Programm Brandenburg-Polen) gemäß Artikel 124 Absatz 1 der Verordnung (EU) Nr.
    1303/2013 beziehungsweise Artikel 21 der Verordnung (EU) Nr.
    1299/2013 und Verordnung (EU) Nr.
    508/2014 in Verbindung mit Artikel 124 der Verordnung (EU) Nr. 1303/2013 (Benennende Stelle)

### V.
Geschäftsbereich des Ministeriums für Soziales, Gesundheit, Integration und Verbraucherschutz (MSGIV)

1.  Sozialpolitik einschließlich der Bekämpfung von Kinder- und Altersarmut, Sozialhilfe, Wohlfahrtspflege, freiwilliges Engagement im sozialen Bereich, Sozialberichterstattung
2.  Sozialversicherung einschließlich Krankenversicherung
3.  Soziales Entschädigungsrecht bei Gesundheitsschäden durch Krieg, Verfolgung, Gewalt oder Impfungen
4.  Koordinierung des nachsorgenden Opferschutzes bei Katastrophen, terroristischen Anschlägen und Großschadensereignissen
5.  Seniorenpolitik, Seniorenbeauftrage oder Seniorenbeauftragter des Landes, Pflegepolitik, Heimrecht
6.  Politik für Menschen mit Behinderungen, Beauftragte oder Beauftragter der Landesregierung für die Belange von Menschen mit Behinderungen, Teilhabe und Rehabilitation
7.  Zuwanderungspolitik und humanitäre Landesaufnahmeprogramme (gemeinsam mit MIK), Aufnahme und Integration von Zuwanderern (mit Ausnahme des Erstaufnahmeverfahrens), Ausländerleistungsrecht, Angelegenheiten von Spätaussiedlern, Kriegsfolgenrecht, Integrationsbeauftragte oder Integrationsbeauftragter des Landes
8.  Arbeitsschutz, arbeitsweltbezogene Prävention, Sicherheit und Gesundheitsschutz am Arbeitsplatz einschließlich Arbeitszeit, Sozialvorschriften für Kraftfahrer, Berufskrankheiten, Mutterschutz, Kinder- und Jugendarbeitsschutz, Heimarbeit, Ladenöffnungsrecht
9.  Marktüberwachung in den Bereichen Geräte- und Produktsicherheitsrecht, explosionsgefährliche Stoffe, ortsbewegliche Druckgeräte
10.  Ausbildung und Prüfung des höheren und gehobenen Dienstes der Arbeitsschutzverwaltung des Landes, Laufbahnrecht für die Arbeitsschutzaufsicht
11.  Sicherheitstechnische und betriebsärztliche Betreuung der Beschäftigten der Landesverwaltung nach dem Arbeitssicherheitsgesetz (Kompetenzzentrum für Sicherheit und Gesundheit)
12.  Energieeffizienz und Kennzeichnung energieverbrauchsrelevanter Produkte, PKW und Reifen
13.  Gesundheits- und Krankenhauswesen, Prävention, Gesundheitsförderung, Rehabilitation, Kur- und Bäderwesen
14.  Fachkräftesicherung im Bereich Pflege und Gesundheit
15.  Öffentlicher Gesundheitsdienst, Infektionsschutz
16.  Psychiatrie, Versorgung psychisch Kranker, Maßregelvollzug
17.  Apotheken, Arzneimittel, Medizinprodukte
18.  Heil-, Pflege- und Gesundheitsberufe, soziale Berufe (Studiengang der Sozialen Arbeit, Bildungsgänge der Fachrichtung Heilerziehungspflege und Heilpädagogik)
19.  Rettungswesen, Gesundheitlicher Bevölkerungsschutz
20.  Gerichtsmedizin
21.  Familienpolitik, einschließlich präventiver Kinderschutz im Rahmen von Familienbildung, soweit nicht MBJS
22.  Frauen- und Gleichstellungspolitik, einschließlich der Politik für Lesben, Schwule, Bi-, Trans- und Intersexuelle, Gleichstellungsbeauftragte des Landes, Gender Mainstreaming, Gewaltprävention
23.  Verbraucherschutz, unter anderem wirtschaftlicher Verbraucherschutz
24.  Verbraucherpolitik, Angelegenheiten der Verbraucherzentrale Brandenburg, Verbraucherrechte, Verbraucherinformation
25.  Lebensmittel- und Futtermittelsicherheit
26.  Trink- und Badebeckenwasserhygiene sowie Badegewässerqualität
27.  Chemikaliensicherheit, stoff-, produkt- und wirkungsbezogener Umwelt- und Verbraucherschutz
28.  Kerntechnik, Strahlenschutzvorsorge, Strahlenschutz
29.  Allgemeine Angelegenheiten des Veterinärwesens und des gesundheitlichen Verbraucherschutzes
30.  Tierseuchenverhütung und -bekämpfung, Tierkörperbeseitigung
31.  Tierschutz; Landestierschutzbeauftragte oder Landestierschutzbeauftragter
32.  Überwachung des Verkehrs mit Tierarzneimitteln
33.  Gentechnik
34.  Landeslabor Berlin-Brandenburg

### VI.
Geschäftsbereich des Ministeriums für Wirtschaft, Arbeit und Energie (MWAE)

1.  Wirtschaftspolitik, Wirtschaftsordnung, Wirtschaftsrecht
2.  Wirtschafts- und Arbeitsförderung, regionale Strukturentwicklung und sektorale Strukturentwicklung, Förderprogramme der EU und des Bundes, Verwaltungs- und Bescheinigungsbehörden für die Europäischen Fonds EFRE und ESF
3.  Außenwirtschaft, Standortwerbung
4.  Technologie- und Innovationsförderung
5.  Industrie, Handwerk, Handel, Gewerbe, Aufsicht über die Industrie- und Handelskammern sowie die Handwerkskammern, Genossenschaftswesen
6.  Mess- und Eichwesen, Fachaufsicht über das Landesamt für Mess- und Eichwesen Berlin-Brandenburg
7.  Börsen- und Wertpapierwesen
8.  Gelwäscheprävention (außer Finanzsektor)
9.  Digitales, digitale Infrastruktur
10.  Arbeitsmarktpolitik, einschließlich Europäischer Beschäftigungspolitik, Arbeits- und Tarifrecht sowie Sozialpartnerschaft
11.  Grundsicherung für Arbeitsuchende
12.  Existenzgründungen
13.  Berufliche Ausbildung und berufliche Weiterbildung
14.  Fachkräftesicherung einschließlich Anwerbeoffensive und Arbeitsmigration, insbesondere die Umsetzung des Fachkräfteeinwanderungsgesetzes, die Instrumente der Arbeitsmarktintegration des SGB III und II sowie die berufsbezogene Deutschsprachenförderung nach § 45a Aufenthaltsgesetz
15.  Zuständige Stelle für die Umsetzung der EU-Richtlinie über die Verhältnismäßigkeitsprüfung vor Erlass neuer Berufsreglementierungen
16.  Energiepolitik (konventionelle und erneuerbare Energien), Energiewirtschaft, Emissionsrechtehandel, Energieaufsicht, Energieeffizienz (mit Ausnahme Gebäude und Verkehr), Landesregulierungsbehörde
17.  Bergbau, Geologie, Rohstoffwirtschaft (außer Bodenschutz), Fachaufsicht über das Landesamt für Bergbau, Geologie und Rohstoffe (im Bereich Boden- und Hydrogeologie mit MLUK)
18.  Konversion
19.  Film- und Medienwirtschaft, Kreativwirtschaft
20.  Informations- und Kommunikationstechnologien
21.  Tourismuswirtschaft, zuständige Behörde gemäß Artikel 252 und 253 EGBGB (neu)
22.  Koordinatorin oder Koordinator für den Wassertourismus einschließlich tourismusrelevanter Wasserstraßen
23.  Wettbewerb, Kartellrecht, Grundsatzfragen des öffentlichen Auftragswesens, Preisrecht
24.  Angelegenheiten nach Artikel 107 und 108 des AEUV (Beihilfenkontrolle), soweit nicht Landwirtschaft
25.  Einheitlicher Ansprechpartner für das Land Brandenburg

### VII.
Geschäftsbereich des Ministeriums für Bildung, Jugend und Sport (MBJS)

1.  Schulische Bildung und Erziehung, insbesondere
    *   allgemeinbildende Schulen
    *   berufliche Schulen
    *   Ersatz- und Ergänzungsschulen
    *   Angelegenheiten des Religionsunterrichts
    *   Lehreraus-, fort- und -weiterbildung
    *   Dienst-, Besoldungs- und Tarifrecht, soweit es sich auf den Schul- und Schulaufsichtsdienst bezieht
    *   Berufsorientierung (einschließlich Übergang Schule-Beruf)
    *   Zweiter Bildungsweg und Fernunterricht
2.  Schul- und Sportstättenentwicklungsplanung, Angelegenheiten des Schul- und Sportstättenbaus
3.  Angelegenheiten der Weiterbildung
4.  Politische Bildung
5.  Kinder- und Jugendpolitik, Kinder- und Jugendhilfe, gesetzlicher Jugendschutz, freiwilliges soziales Jahr (FSJ), Netzwerk Gesunde Kinder, Kinder- und Jugendbeauftragte oder -beauftragter des Landes
6.  Unterhaltsvorschuss, Adoption
7.  Angelegenheiten der Kindertagesbetreuung, Nachmittagsbetreuung von Kindern mit Behinderungen
8.  Soziale Berufe (staatliche Anerkennung von Sozialpädagoginnen und Sozialpädagogen, Erzieherinnen und Erziehern sowie Sonderpädagoginnen und Sonderpädagogen, Anerkennung von Weiterbildungsmaßnahmen und Fortbildung von Fachkräften der Kinder- und Jugendhilfe)
9.  Angelegenheiten des Sports

### VIII.
Geschäftsbereich des Ministeriums für Landwirtschaft, Umwelt und Klimaschutz (MLUK)

1.  Landwirtschaft, Gartenbau, insbesondere
    *   Agrarforschung, Agrarbildung
    *   Ernährungswirtschaft, Ernährungsvorsorge
    *   Maßnahmenplan Ambrosiabekampfung
2.  Ländliche Entwicklung, EU-Direktzahlungen sowie EU- und Bundesförderung, Verwaltungsbehörde für den Europäischen Fonds ELER
    *   Zustandige Behörde gemäß Artikel 1 Absatz 2 der Verordnung (EU) Nr.
        908/2014 oder einer entsprechenden Nachfolgeverordnung für die Zulassung und die Aufsicht über die Zahlstelle für EGFL und ELER gemäß Artikel 7 Absatz 1 der Verordnung (EU) Nr.
        1306/2013 oder einer entsprechenden Nachfolgeverordnung
    *   Zahlstelle gemäß Artikel 7 Absatz 1 der Verordnung (EU) Nr.
        1306/2013 oder einer entsprechenden Nachfolgeverordnung für die Verwaltung und Kontrolle der Ausgaben des EGFL und des ELER
    *   Verwaltungsbehörde für den Europäischen Landwirtschaftsfonds für die Entwicklung des ländlichen Raums (ELER) für Brandenburg und Berlin gemäß Artikel 66 der Verordnung (EU) Nr.
        1305/2013 des Europäischen Parlaments und des Rates vom 17.
        Dezember 2013 über die Förderung der ländlichen Entwicklung durch den Europäischen Landwirtschaftsfonds für die Entwicklung des ländlichen Raums (ELER)
3.  Forsten, Waldpädagogik
4.  Jagd, Fischerei
5.  Nachhaltige Entwicklung
6.  Klimapolitik
    *   Klimaschutz
    *   Klimaschutzaspekte und Landesvollzug des Treibhausgasemissionshandels
    *   Umgang mit den Folgen des Klimawandels
7.  Nachwachsende Rohstoffe und sonstige Biomasse
8.  Energie- und verkehrsbezogene Fragen der Umweltpolitik
9.  Immissionsschutz
10.  Wasserwirtschaft, Gewässerschutz, Hochwasserschutz, Hydrologie
11.  Kreislauf- und Abfallwirtschaft
12.  Bodenschutz, Altlasten/Schädliche Bodenveränderungen, Umweltgeologie
13.  Natur- und Landschaftsschutz, Artenschutz
14.  Umweltbildung, freiwilliges ökologisches Jahr (FÖJ)
15.  Umweltpartnerschaften, Umweltprüfung (SUP, UVP)
16.  Ausbildung und Prüfung in Ausbildungsberufen der Bereiche Land-, Forst- und Fischereiwirtschaft

### IX.
Geschäftsbereich des Ministeriums für Infrastruktur und Landesplanung (MIL)

1.  Raumbezogene Strukturpolitik
2.  Raumordnung und Landesplanung einschließlich Braunkohleplanung, Sanierungsplanung und -umsetzung, Aufsicht über die Regionalen Planungsgemeinschaften
3.  Stadtentwicklung, Städtebauförderung, Städtebaurecht
4.  Bauordnungsrecht, Bauaufsicht, Bauberufsrecht, einschließlich Rechtsaufsicht über die Architektenkammer und die Ingenieurkammer
5.  Wohnungswesen, Wohngeld
6.  Bauwirtschaft, Wohnungswirtschaft
7.  Mobilitätspolitik, insbesondere
    *   Straßenwesen und Straßenrecht
    *   Rad- und Fußverkehr
    *   Moderne, technologieoffene, alternative Mobilitat
    *   Eisenbahnangelegenheiten und ÖPNV
    *   Luftfahrtangelegenheiten und Wetterdienst
    *   Binnenschifffahrt
    *   Logistik und Güterverkehr
    *   Straßenverkehr und Straßenverkehrsrecht
8.  Kommunaler Straßenbau, insbesondere
    *   Straßenaufsicht
    *   Förderung kommunaler Straßen und Radwege
9.  Ausbildung des höheren technischen Verwaltungsdienstes des Landes (Fachrichtungen Städtebau und Bauingenieurwesen); Ausbildung und Prüfung in Ausbildungsberufen des Bereichs Straßenwesen

### X.
Geschäftsbereich des Ministeriums für Wissenschaft, Forschung und Kultur (MWFK)

1.  Hochschulwesen, insbesondere
    *   Universitäten
    *   Fachhochschulen
    *   Kunsthochschule
    *   Anerkennung privater Hochschulen und Berufsakademien
    *   Studentische Angelegenheiten einschließlich Ausbildungsförderung
    *   Hochschulaufsicht einschließlich Dienst- und Besoldungsrecht
    *   Wissens- und Technologietransfer/Technologiefolgenabschätzung
2.  Außerhochschulische Forschung
    *   Helmholtz-Gemeinschaft Deutscher Forschungszentren (HGF)
    *   Max-Planck-Gesellschaft (MPG)
    *   Wissenschaftsgemeinschaft Gottfried Wilhelm Leibniz (WGL)
    *   Fraunhofer Gesellschaft (FhG)
    *   Berlin Brandenburgische Akademie der Wissenschaften (BBAW)
    *   Geisteswissenschaftliche Zentren (GWZ)
    *   Institutionell geförderte Einrichtungen (Einstein Forum, Moses Mendelssohn Zentrum)
    *   Projektförderung (unter anderem Institute for Advanced Sustainability Studies - IASS, Berlin-Brandenburg Center for Regenerative Therapies - BCRT)
3.  Hochschulentwicklungsplanung sowie internationale Beziehungen und Programme im Hochschul- und Forschungsbereich
4.  Künstliche Intelligenz (KI)
5.  Innerstaatliche Kulturpolitik und internationale kulturelle Angelegenheiten
6.  Archivwesen, Denkmalschutz
7.  Pflege der Kunst und Kultur, insbesondere der darstellenden Kunst, der Musik, der Museen, der bildenden Kunst, der Literatur, der Soziokultur, des Bibliothekswesens und der Denkmalpflege sowie der Industriekultur
8.  Angelegenheiten der Kirchen und Religionsgemeinschaften
9.  Angelegenheiten der Sorben/Wenden und des Niederdeutschen
10.  Angelegenheiten der Vertriebenen

Potsdam, den 7.
Mai 2020

Der Ministerpräsident

Dr.
Dietmar Woidke