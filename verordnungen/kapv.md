## Verordnung über die Kapazitätsermittlung für die Hochschulen (Kapazitätsverordnung - KapV)

Auf Grund des § 10 Absatz 3 und des § 11 Absatz 2 des Brandenburgischen Hochschulgesetzes vom 18. Dezember 2008 (GVBl.
I S. 318) verordnet die Ministerin für Wissenschaft, Forschung und Kultur nach Anhörung der Hochschulen:

Abschnitt 1  
Allgemeine Grundsätze und Verfahren
-------------------------------------------------

### § 1 

(1) Der Festsetzung der Zulassungszahlen nach § 10 des Brandenburgischen Hochschulgesetzes in Verbindung mit § 2 geht die Überprüfung voraus, ob im Rahmen der verfügbaren Mittel die Möglichkeiten zur Nutzung der vorhandenen Ausbildungskapazität ausgeschöpft worden sind.
Hierzu wird die jährliche Aufnahmekapazität in zwei Verfahrensschritten ermittelt:

1.  Berechnung auf Grund der personellen Ausstattung nach den Vorschriften des Abschnitts 2;
2.  Überprüfung des Ergebnisses nach Nummer 1 anhand der weiteren kapazitätsbestimmenden Kriterien nach den Vorschriften des Abschnitts 3.

(2) Maßnahmen, die gemäß § 10 Absatz 4 des Brandenburgischen Hochschulgesetzes bei der Feststellung der Aufnahmekapazität unberücksichtigt bleiben, sind gesondert auszuweisen.

### § 2

(1) Die Hochschulen legen bis zum 31.
März jedes Jahres für das darauffolgende Studienjahr der für die Hochschulen zuständigen obersten Landesbehörde einen Kapazitätsbericht vor.
Der Bericht enthält insbesondere eine Darstellung der Ermittlung der Aufnahmekapazität nach § 1, der Aufteilung der Curricularnormwerte der Studiengänge auf Lehreinheiten (§ 11 Absatz 4) und einen Vorschlag für die Festsetzung von Zulassungszahlen.

(2) Legt die Hochschule keinen Bericht vor oder ist der Bericht unvollständig oder verspätet, trifft die für die Hochschulen zuständige oberste Landesbehörde die erforderlichen Maßnahmen zur Festsetzung der Zulassungszahlen.

(3) Die Berichte der Hochschulen werden zwischen der für die Hochschulen zuständigen obersten Landesbehörde und den Hochschulen gemeinsam erörtert.
Weicht die für die Hochschulen zuständige oberste Landesbehörde bei der Festsetzung der Zulassungszahlen von dem Vorschlag der Hochschule ab, wird die Hochschule hierüber unterrichtet.

### § 3

(1) Die jährliche Aufnahmekapazität wird auf der Grundlage der Daten eines Stichtages ermittelt, der nicht mehr als neun Monate vor Beginn des Zeitraums liegt, für den die Ermittlung und die Festsetzung gelten (Berechnungszeitraum).
Der Berechnungszeitraum besteht jeweils aus dem Wintersemester und dem darauffolgenden Sommersemester.

(2) Sind wesentliche Änderungen der Daten vor einem Vergabetermin erkennbar, sollen sie berücksichtigt werden.
Treten wesentliche Änderungen der Daten vor einem Vergabetermin ein, sollen eine Neuermittlung und eine Neufestsetzung durchgeführt werden.

(3) Bei Studiengängen, für die während eines Studienjahres Bewerber an mehreren Vergabeterminen aufgenommen werden, wird die jährliche Aufnahmekapazität auf die einzelnen Vergabetermine aufgeteilt.

Abschnitt 2  
Berechnung auf Grund der personellen Ausstattung
--------------------------------------------------------------

### § 4 

Die jährliche Aufnahmekapazität auf Grund der personellen Ausstattung wird nach Anlage 1 unter Anwendung von Curricularnormwerten nach Anlage 2 berechnet.

### § 5 

(1) Der Berechnung werden Lehreinheiten zugrunde gelegt, denen einzelne oder mehrere Studiengänge zuzuordnen sind.
Eine Lehreinheit ist eine für die Zwecke der Kapazitätsermittlung abgegrenzte fachlich zusammenhängende Einheit, die ein Lehrangebot bereitstellt.
Ein Studiengang ist der Lehreinheit zuzuordnen, bei der er den überwiegenden Teil der Lehrveranstaltungsstunden nachfragt, in Ausnahmefällen kann die Hochschule nach fachlichen oder organisatorischen Gesichtspunkten eine abweichende Festlegung treffen.
Einer Lehreinheit zugeordnete Teilstudiengänge (Lehramtsstudiengänge und Zwei-Fach-Bachelorstudiengänge) können bei der Berechnung zusammengefasst und mit einer gemeinsamen Zulassungszahl ausgewiesen werden.

(2) Lehreinheiten können auch hochschulübergreifend gebildet werden.

### § 6 

(1) Für die Berechnung des Lehrangebots sind alle Stellen des wissenschaftlichen und künstlerischen Lehrpersonals und die sonstigen Lehrpersonen nach Personalkategorien gegebenenfalls anteilig den Lehreinheiten zuzuordnen.

(2) Lehrpersonen, die zur Wahrnehmung von Aufgaben in der Lehre im Berechnungszeitraum an die Hochschule abgeordnet sind, werden in die Berechnung einbezogen.

(3) Stellen oder Stellenanteile, die voraussichtlich während mindestens der Hälfte des Berechnungszeitraums nicht besetzt sind, können bei der Berechnung entsprechend dem Besetzungsgrad ganz oder teilweise unberücksichtigt bleiben; sie sind gesondert auszuweisen.

(4) Einer Lehreinheit zugeordnete Stellen oder sonstige Lehrpersonen, die im Berechnungszeitraum oder in dem darauffolgenden Berechnungszeitraum entfallen, bleiben bei der Feststellung der Aufnahmekapazität unberücksichtigt.

(5) Einer Lehreinheit zugeordnete Stellen oder sonstige Lehrpersonen, die in einem späteren als dem in Absatz 4 bezeichneten Zeitraum entfallen, bleiben dann unberücksichtigt, wenn sie für die ordnungsgemäße Ausbildung einer höheren Studierendenzahl auf Grund früherer höherer Zulassungen erforderlich sind.

(6) Die Stellen oder sonstigen Lehrpersonen nach Absatz 4 und 5 sind unter Angabe des Zeitpunkts des Wegfalls aufzuführen.

### § 7

(1) Das Lehrdeputat ist die gegenüber einer Lehrperson festgesetzte individuelle Lehrverpflichtung, gemessen in Deputatstunden.

(2) Werden nicht besetzte Stellen in die Berechnung einbezogen, ist im Fall von Hochschullehrern die Regellehrverpflichtung und in den übrigen Fällen der Durchschnitt des der Lehreinheit in der jeweiligen Personalkategorie zur Verfügung stehenden Lehrdeputats zugrunde zu legen.
Sofern eine Stellenbesetzung innerhalb des Berechnungszeitraums bereits geplant oder darüber entschieden ist, wird die (beispielsweise auf Grund der Stellenausschreibung) vorgesehene individuelle Lehrverpflichtung zugrunde gelegt.

(3) Soweit die individuelle Lehrverpflichtung vermindert wird, ist dies zu berücksichtigen.

### § 8

Als Lehrauftragsstunden werden die Lehrveranstaltungsstunden in die Berechnung einbezogen, die der Lehreinheit für den Ausbildungsaufwand nach § 11 Absatz 1 im Berechnungszeitraum gemäß Planung der Hochschule zur Verfügung stehen und nicht auf einer Regellehrverpflichtung beruhen.
Ersatzweise kann der Durchschnitt der Lehraufträge im zum Stichtag laufenden Wintersemester und dem davor liegenden Sommersemester angewendet werden.
Die Sätze 1 und 2 gelten nicht, soweit die Lehrveranstaltungsstunden zur Abdeckung des Lehrdeputats von unbesetzten Stellen eingesetzt werden.
Dies gilt ferner nicht, soweit außerhochschulische Personen oder ganz oder teilweise aus Mitteln Dritter finanziertes Personal der Hochschulen freiwillig und unentgeltlich Lehrleistungen erbringen.
Die Lehrauftragsstunden sind auf der Grundlage der dienstrechtlichen Vorschriften in Deputatstunden umzurechnen.

### § 9 

(1) Dienstleistungen einer Lehreinheit sind die Lehrveranstaltungsstunden, die die Lehreinheit für ihr nicht zugeordnete Studiengänge zu erbringen hat.

(2) Zur Berechnung des Bedarfs an Dienstleistungen sind Studienanfängerzahlen für die nicht zugeordneten Studiengänge anzusetzen.
Liegen keine Studienanfängerzahlen vor, sind die voraussichtlichen Zulassungszahlen für diese Studiengänge anzusetzen.
Die bisherige Entwicklung der Studienanfängerzahlen und planerische Festlegungen der Hochschule können beim Dienstleistungsbedarf ebenfalls berücksichtigt werden.

### § 10

(1) Die Anteilquote ist das Verhältnis der jährlichen Aufnahmekapazität eines der Lehreinheit zugeordneten Studiengangs zur Summe der jährlichen Aufnahmekapazitäten aller der Lehreinheit zugeordneten Studiengänge.

(2) Die Hochschule kann ein Berechnungsverfahren zur Ermittlung der Anteilquoten in einer Lehreinheit selbst festlegen oder die Anteilquoten nach planerischen Gesichtspunkten bestimmen.

### § 11

(1) Der Curricularnormwert bestimmt den in Deputatstunden gemessenen Aufwand aller beteiligten Lehreinheiten, der für die ordnungsgemäße Ausbildung eines Studierenden in dem jeweiligen Studiengang erforderlich ist.
Bei der Berechnung der jährlichen Aufnahmekapazität sind die in Anlage 2 aufgeführten Curricularnormwerte anzuwenden.

(2) Bei Studiengangkombinationen sind die in Anlage 2 aufgeführten Curricularnormwerte unter Berücksichtigung der Ausbildungsstruktur, des Anteils des jeweiligen Studiengangs am Gesamtstudium und der Studiendauer entsprechend der Studienordnung aufzuteilen.

(3) Ist für einen Studiengang ein Curricularnormwert in Anlage 2 nicht aufgeführt, wird von der für die Hochschulen zuständigen obersten Landesbehörde im Benehmen mit der Hochschule ein Curricularnormwert festgelegt.

(4) Zur Ermittlung der Lehrnachfrage in den einzelnen Lehreinheiten wird der Curricularnormwert auf die am Lehrangebot für den Studiengang beteiligten Lehreinheiten aufgeteilt (Bildung von Curricularanteilen).
Die Angaben für die beteiligten Lehreinheiten sind aufeinander abzustimmen.
Hilfsweise gilt die bisherige Verteilung des Lehrangebots.

Abschnitt 3  
Überprüfung des Berechnungsergebnisses und Ausnahmetatbestände
----------------------------------------------------------------------------

### § 12 

(1) Das nach den Vorschriften des Abschnitts 2 berechnete Ergebnis ist zur Festsetzung der Zulassungszahlen anhand der weiteren, in den Absätzen 2 und 3 aufgeführten kapazitätsbestimmenden Kriterien zu überprüfen, wenn Anhaltspunkte gegeben sind, dass sie sich auf das Berechnungsergebnis auswirken.

(2) Eine Verminderung kommt nur in Betracht, wenn Tatbestände gegeben sind, die die Durchführung einer ordnungsgemäßen Lehre beeinträchtigen (Nummer 1 bis 3) oder wenn ein Ausgleich für eine Mehrbelastung des Personals (§ 6 Absatz 1) durch Studierende höherer Semester oder durch Studierende in auslaufenden Studiengängen erforderlich ist (Nummer 4):

1.  Fehlen von Räumen in ausreichender Zahl, Größe und Ausstattung;
2.  Fehlen einer ausreichenden Ausstattung mit sächlichen Mitteln;
3.  Fehlen einer ausreichenden Ausstattung der Lehreinheit mit akademischen und nichtwissenschaftlichen Mitarbeitern;
4.  gegenüber dem nach Absatz 3 Nummer 1 bis 3 überprüften Berechnungsergebnis des Zweiten Abschnitts höhere Aufnahme von Studierenden erster oder höherer Fachsemester in den vergangenen Jahren.

(3) Eine Erhöhung kommt nur in Betracht, wenn das Personal (§ 6 Absatz 1) eine Entlastung von Lehraufgaben durch folgende Tatbestände erfährt:

1.  besondere Ausstattung der Lehreinheit mit akademischen und nichtwissenschaftlichen Mitarbeitern;
2.  besondere Ausstattung mit sächlichen Mitteln;
3.  Studienabbruch, Fachwechsel oder Hochschulwechsel von Studierenden in höheren Semestern (Schwundquote).

### § 13

(1) Ist in einer Lehreinheit ein Engpass an Räumen in ausreichender Zahl, Größe und Ausstattung vorherzusehen, ist der Raumbedarf der Lehrveranstaltungsarten, für die der Engpass vermutet wird, festzustellen.
Diesem Raumbedarf wird das Angebot an Raumstunden nach Lehrveranstaltungsarten gegenübergestellt.

(2) Für die Ermittlung des Angebots an Raumstunden ist davon auszugehen, dass die Räume für die Lehrveranstaltungen mit begrenzter Teilnehmerzahl ganztägig und ganzjährig zur Verfügung stehen, falls keine fachspezifischen Gegebenheiten entgegenstehen.

(3) Ist das Angebot an Raumstunden geringer als der jährliche Lehrveranstaltungsbedarf, und ist eine Bereitstellung von sonstigen Räumen nicht möglich, kann das nach den Vorschriften des Abschnitts 2 ermittelte Berechnungsergebnis entsprechend dem größtmöglichen Angebot an Raumstunden vermindert werden.

### § 14

(1) Die Zulassungszahl soll erhöht werden, wenn zu erwarten ist, dass die Zahl der Abgänge an Studierenden in höheren Fachsemestern größer ist als die Zahl der Zugänge (Schwundquote).
Die Obergrenze der anzuwendenden Schwundquote beträgt 50 Prozent.

(2) Bei neuen Studiengängen oder in sonstigen begründeten Fällen kann die anzuwendende Schwundquote auf Vorschlag der Hochschule durch die für die Hochschulen zuständige oberste Landesbehörde festgesetzt werden.
Im Ausnahmefall oder im Zusammenhang mit dem Abschluss einer Zielvereinbarung kann von der Anwendung der Schwundquote vollständig abgesehen werden.

(3) Die Hochschule kann über ein einheitliches Berechnungsverfahren zur Ermittlung der Schwundquote selbst entscheiden oder eine Bestimmung nach planerischen Gesichtspunkten vornehmen.

Abschnitt 4  
Schlussbestimmungen
---------------------------------

### § 15 

Diese Verordnung gilt entsprechend für Hochschulen, an denen die jährliche Unterrichtsdauer in anderer Weise als nach Semestern aufgeteilt ist, sowie für die Festsetzung von Zulassungszahlen für höhere Fachsemester.
Sie gilt auch für Fern-, Online-, Teilzeit-, Aufbau- und duale Studiengänge.

### § 16 

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Sie gilt erstmals für die Festsetzung von Zulassungszahlen für das Wintersemester 2012/13.
Gleichzeitig tritt die Kapazitätsverordnung vom 30. Juni 1994 (GVBl.
II S. 588), die zuletzt durch Artikel 2 Absatz 7 des Gesetzes vom 18. Dezember 2008 (GVBl.
I S. 318) geändert worden ist, außer Kraft.

Potsdam, den 16.
Februar 2012

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Prof.
Dr.-Ing.
Dr.
Sabine Kunst

* * *

Anlage 1 
---------

### Verfahren zur Berechnung der personellen Aufnahmekapazität auf Grund des Abschnitts 2 der Verordnung

Die personelle Aufnahmekapazität wird unter Zugrundelegung der je Studiengang aufgestellten Curricularnormwerte (§ 11, Anlage 2) berechnet.
Die Curricularnormwerte sind als Curricularanteile auf die Lehreinheiten so aufzuteilen und darzustellen, dass die Summe der Curricularanteile eines Studiengangs den Curricularnormwert ergibt.

### I.
Berechnung des Angebots einer Lehreinheit an Deputatstunden

Das Angebot einer Lehreinheit an Deputatstunden (S) ergibt sich aus dem Lehrdeputat der verfügbaren Stellen und der sonstigen Lehrpersonen einschließlich dem Lehrdeputat an die Hochschule abgeordneter Personen und dem durch Lehraufträge zusätzlich zur Verfügung stehenden Deputat.
Abzuziehen sind Verminderungen des Lehrdeputats nach § 7 Absatz 3.

(1) S = ∑j (lj • hj - rj) + L

Das so ermittelte Angebot ist zu reduzieren um die Dienstleistungen, gemessen in Deputatstunden, die die Lehreinheit für die ihr nicht zugeordneten Studiengänge zu erbringen hat.
Dabei sind die Curricularanteile anzuwenden, die für die jeweiligen nicht zugeordneten Studiengänge auf die Lehreinheit entfallen.

(2) E = ∑q CAq • (Aq • 0,5)

Damit beträgt das bereinigte Lehrangebot

(3) Sb = S - E

### II.
Berechnung der jährlichen Aufnahmekapazität

Unter Anwendung der Anteilquoten der zugeordneten Studiengänge wird ein gewichteter Curricularanteil ermittelt:

(4) CA = ∑p CAp • zp

(5) Ap \= (2 • Sb) / (CA) • zp

### III.
Verzeichnis der benutzten Symbole

Ap

Jährliche Aufnahmekapazität des der Lehreinheit zugeordneten Studiengangs p

Aq

Die für den Dienstleistungsabzug anzusetzende jährliche Studienanfängerzahl des der Lehreinheit nicht zugeordneten Studiengangs q (§ 9 Absatz 2)

CAp

Anteil am Curricularnormwert (Curricularanteil) des zugeordneten Studiengangs p, der auf die Lehreinheit entfällt (§ 11 Absatz 4)

CAq

Anteil am Curricularnormwert (Curricularanteil) des nicht zugeordneten Studiengangs q, der von der Lehreinheit als Dienstleistung zu erbringen ist (§ 11 Absatz 4)

CA

Gewichteter Curricularanteil aller einer Lehreinheit zugeordneten Studiengänge

E

Dienstleistungen der Lehreinheit für die ihr nicht zugeordneten Studiengänge in Deputatstunden je Semester (§ 9)

hj

Lehrdeputat je Stelle bzw.
je sonstiger Lehrperson der Personalkategorie in der Lehreinheit j, gemessen in Deputatstunden je Semester (§ 7 Absatz 1)

lj

Anzahl der in der Lehreinheit j verfügbaren Stellen der Personalkategorie oder sonstiger Lehrpersonen mit gleichem Lehrdeputat

Lj

Anzahl der Lehrauftragsstunden der Lehreinheit in Deputatstunden je Semester (§ 8)

rj

Gesamtsumme der Verminderungen für die Stellen oder sonstigen Lehrpersonen in der Lehreinheit j, gemessen in Deputatstunden je Semester (§ 7 Absatz 3)

S

Lehrangebot der Lehreinheit in Deputatstunden je Semester (§ 7 Absatz 1)

Sb

Um Dienstleistungen für die nicht zugeordneten Studiengänge bereinigtes Lehrangebot der Lehreinheit in Deputatstunden je Semester

zp

Anteil der jährlichen Aufnahmekapazität eines zugeordneten Studiengangs p an der Aufnahmekapazität der Lehreinheit (§ 10, Anteilquote)

### Anlagen

1

[Anlage 2 - Verzeichnis der anzuwendenden Curricularnormwerte für Studiengänge](/br2/sixcms/media.php/68/KapV-Anlage-2.pdf "Anlage 2 - Verzeichnis der anzuwendenden Curricularnormwerte für Studiengänge") 984.0 KB