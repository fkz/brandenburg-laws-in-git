## Verordnung über die Gestaltung von Prüfungsordnungen zur Gewährleistung der Gleichwertigkeit von Studium, Prüfungen und Abschlüssen (Hochschulprüfungsverordnung - HSPV)

Auf Grund des § 22 Absatz 4 des Brandenburgischen Hochschulgesetzes vom 28.
April 2014 (GVBl.
I Nr. 18) verordnet die Ministerin für Wissenschaft, Forschung und Kultur:

#### § 1 Geltungsbereich

(1) Diese Verordnung gilt für alle Studiengänge mit einer Hochschulprüfung, auf Grund derer ein Bachelor- oder Mastergrad verliehen wird.

(2) Auf Diplom- und Magisterstudiengänge, in denen das Lehrangebot modularisiert und mit Leistungspunkten versehen ist, finden die nachfolgenden Bestimmungen entsprechende Anwendung.

#### § 2 Begriffsbestimmungen

(1) Module sind in sich abgeschlossene abprüfbare Einheiten, die die Stoffgebiete thematisch und zeitlich abgerundet zusammenfassen.
Module können sich aus verschiedenen Lehr- und Lernformen zusammensetzen.
Ein Modul umfasst im Regelfall Inhalte eines einzelnen Semesters oder eines Studienjahres.
In besonders begründeten Fällen kann sich ein Modul auch über mehrere Semester erstrecken.

(2) Bei der Einrichtung von Masterstudiengängen ist festzulegen, ob es sich um einen konsekutiven oder weiterbildenden Studiengang handelt.
Konsekutive Masterstudiengänge sind als vertiefende, verbreiternde, fachübergreifende oder fachlich andere Studiengänge auszugestalten.
Weiterbildende Masterstudiengänge setzen nach einem ersten berufsqualifizierenden Hochschulabschluss eine berufspraktische Tätigkeit von in der Regel mindestens einem Jahr voraus.
Die Inhalte des Masterstudiengangs sollen die beruflichen Erfahrungen berücksichtigen und an diese anknüpfen.

#### § 3 Regelstudienzeit

(1) Für jeden Studiengang ist die jeweilige Regelstudienzeit nach Maßgabe des Brandenburgischen Hochschulgesetzes festzusetzen.

(2) Die Regelstudienzeit umfasst die einzelnen Studienabschnitte, in den Studiengang integrierte berufspraktische Tätigkeiten und praktische Studiensemester sowie die Prüfungszeiten unter Einschluss der Abschlussarbeit.

(3) Die strukturelle und inhaltliche Gliederung des Studiengangs muss die Studierbarkeit des Lehrangebots einschließlich der praktischen Studienabschnitte sowie den Abschluss aller Module innerhalb der Regelstudienzeit gewährleisten.
Die Studiengänge sollen so gestaltet werden, dass sie Zeiträume für Studienaufenthalte an anderen Hochschulen und in der Praxis bieten (Mobilitätsfenster), ohne dass sich dadurch die erforderliche Studiendauer verlängert.
Die Module innerhalb von Studiengängen sollen nicht übermäßig verknüpft werden.

(4) Die Zahl der Lehrveranstaltungswochen pro Semester soll in der Regel 15 Wochen nicht unterschreiten.
Sie darf pro Studienjahr im Durchschnitt 30 Wochen nicht unterschreiten.
Das für die Hochschulen zuständige Mitglied der Landesregierung kann hiervon Ausnahmen zur Bewältigung von Notlagen, in denen eine reguläre Aufgabenwahrnehmung durch die Hochschulen nicht möglich ist, und ihren Folgen zulassen.

#### § 4 Modularisierung des Lehrangebots und Vergabe von Leistungspunkten

(1) Das Lehrangebot ist zu modularisieren.

(2) Die in einem Modul festgelegten Leistungen sind studienbegleitend zu erbringen.
Die Beschreibung der Module muss insbesondere die Inhalte, Lehrformen, Teilnahmevoraussetzungen, den Leistungserfassungsprozess, den Studienzeitaufwand (gemessen in Leistungspunkten) und die zu erreichende Gesamtqualifikation umfassen.

(3) Jedem Modul ist in Abhängigkeit vom Arbeitsaufwand für die Studierenden eine bestimmte Anzahl von Leistungspunkten entsprechend dem European Credit Transfer System (ECTS) zuzuordnen.
Module sollen einen Umfang von jeweils mindestens fünf Leistungspunkten aufweisen.
In begründeten Fällen können Module auch einen geringeren Umfang aufweisen, sofern die durchschnittliche Prüfungsbelastung im Semester hierdurch nicht steigt.
Die Hochschulen regeln die Voraussetzungen für die Vergabe von Leistungspunkten in ihren Ordnungen.

(4) Je Semester sind in der Regel 30 Leistungspunkte zugrunde zu legen, wobei ein Leistungspunkt einer Gesamtarbeitsleistung der Studierenden von 25 bis 30 Zeitstunden entspricht.
Die Arbeitsbelastung im Vollzeitstudium beträgt in der Vorlesungs- und vorlesungsfreien Zeit insgesamt 32 bis 39 Stunden pro Woche in 46 Wochen pro Jahr (insgesamt 750 bis 900 Stunden je Semester).

(5) Leistungspunkte werden für ein Modul nur vergeben, wenn die Modulnote mindestens „ausreichend“ oder die Bewertung „mit Erfolg“ lautet.

(6) Für praktische Studienabschnitte und Projektarbeiten sowie für Studienarbeiten und Abschlussarbeiten sind Leistungspunkte in Abhängigkeit vom zeitlichen Umfang festzulegen.
Eine Vergabe von Leistungspunkten ist nur möglich, wenn die Praxisphasen von der Hochschule inhaltlich bestimmt sind, in der Regel durch Lehrveranstaltungen begleitet und mit einem Leistungsnachweis abgeschlossen werden.

(7) Für den Bachelorabschluss sind mindestens 180 und höchstens 240 Leistungspunkte nachzuweisen.
Für den Masterabschluss sind – mit Ausnahme für das Lehramt Sekundarstufe I/Primarstufe – unter Einbeziehung des vorangegangenen Bachelorstudiums 300 Leistungspunkte zu erbringen.
Ausnahmen von den Anforderungen nach Satz 2 können in begründeten Einzelfällen für Studierende mit einem Bachelorabschluss, der zusammen mit dem Masterabschluss weniger als 300 Leistungspunkte umfasst, bei entsprechender Qualifikation der oder des Studierenden zugelassen werden.
Über die entsprechende Qualifikation der oder des Studierenden nach Satz 3 befindet der zuständige Prüfungsausschuss vor Aufnahme des Masterstudiums.
Hierfür kann eine Eingangsprüfung durchgeführt werden.
Die Eingangsprüfung ist eine Hochschulprüfung.
Die entsprechende Qualifikation nach Satz 3 können Studierende auch durch erfolgreiche Absolvierung entsprechender Module an einer Hochschule außerhalb von Bachelor- und Masterstudiengängen (Zertifikatsmodule) nachweisen.
Voraussetzung ist, dass sich die im Rahmen der Zertifikatsmodule erbrachten Studien- und Prüfungsleistungen nicht wesentlich von den Studien- und Prüfungsleistungen der Bachelorstudiengänge unterscheiden, die für die Erbringung der 300 Leistungspunkte herangezogen werden.

#### § 5 Anerkennung von Leistungen, Studiengänge in anderen Sprachen

(1) Die Anerkennung von Leistungen eines vorangegangenen Studiums bei einem Hochschul- oder Studiengangwechsel ist zu erteilen, sofern sich die Leistungen nicht wesentlich unterscheiden.
Ein wesentlicher Unterschied ist insbesondere dann gegeben, wenn bei Anerkennung der Leistung der Studienerfolg gefährdet ist, weil die Leistung, für die eine Anerkennung begehrt wird, nicht eine für den Studienerfolg erforderliche Kompetenz umfasst.
Wesentliches Kriterium für die Anerkennung sind die Erfordernisse sowie die Qualifikationsziele des nachfolgenden Studiums.
Die antragstellende Person hat die erforderlichen Informationen über die Leistung, deren Anerkennung begehrt wird, beizubringen.
Die Beweislast dafür, dass eine Leistung die Voraussetzung der Anerkennung nicht erfüllt, liegt bei der Hochschule.

(2) Außerhalb des Hochschulwesens erworbene Kenntnisse und Fähigkeiten sind bis zu 50 Prozent auf ein Hochschulstudium anzurechnen, wenn sie nach Inhalt und Niveau dem Teil des Studiums gleichwertig sind, der ersetzt werden soll.

(3) Die Anerkennung und Anrechnung nach den Absätzen 1 und 2 kann im Einzelfall im Ergebnis einer Prüfung der von der antragstellenden Person beigebrachten Unterlagen, pauschal für homogene Bewerbergruppen oder im Ergebnis einer erfolgreich bestandenen Anerkennungsprüfung erfolgen.
Die Anerkennungsprüfung ist eine Hochschulprüfung.
Die Durchführung einer Anerkennungsprüfung liegt im Ermessen der Hochschule, ein Anspruch der oder des Studierenden hierauf besteht mit Ausnahme der im Brandenburgischen Hochschulgesetz geregelten Fälle nicht.

(4) Bei Studiengängen, für die die Möglichkeit vorgesehen ist, ausschließlich in einer anderen Sprache als Deutsch zu studieren, ist den für den Studiengang relevanten Ordnungen eine Übersetzung in der Sprache, in der der Studiengang angeboten wird, beizufügen und auf der Internetseite der Hochschule einzustellen.

#### § 6 Studien- und Prüfungsleistungen

(1) Module werden in der Regel mit einer benoteten Leistung abgeschlossen, deren Ergebnis in das Abschlusszeugnis eingeht.
In begründeten Fällen können Modulnoten aus mehreren benoteten Leistungen ermittelt werden, insbesondere wenn dies wegen der Größe oder des inhaltlichen Aufbaus des Moduls oder wegen der Besonderheiten der in dem Modul vermittelten Kompetenzen geboten erscheint.
Module, die ausschließlich oder ganz überwiegend praktische Abschnitte umfassen, sowie Module, die ausschließlich oder ganz überwiegend künstlerisch-praktische Kompetenzen umfassen, können ohne Benotung bewertet werden („mit Erfolg“/„ohne Erfolg“).
Die Prüfungsinhalte eines Moduls orientieren sich an den für das Modul definierten Lernergebnissen.
Der Prüfungsumfang ist auf das dafür notwendige Maß zu beschränken.
Bei der Ermittlung der Gesamtnote werden sämtliche Modulnoten mit Ausnahme der Wahlmodule sowie der nichtbenoteten Module berücksichtigt.
Die Hochschulen regeln in ihren Ordnungen die Gewichtung der Modulnoten bei der Bildung der Gesamtnote.
Die in Wahlmodulen erreichten Noten werden auf Antrag der Studierenden im Zeugnis ausgewiesen.

(2) Dem Zeugnis ist ein Diploma Supplement beizufügen, welches Informationen insbesondere über die Struktur und die Inhalte des dem Studienabschluss zugrunde liegenden Studiums enthält.

(3) Leistungen, die benotet werden und Gegenstand der Modulnote sein können, sind insbesondere mündliche Prüfungen, Klausuren und andere schriftliche Prüfungen, Projektarbeiten, Referate, Performanzprüfungen und schriftliche Hausarbeiten.
Die Hochschulen legen in den Modulbeschreibungen die Prüfungsform für die jeweiligen Module fest.
Die Auswahl der Prüfungsformen richtet sich nach den im Modul zu erreichenden Kompetenzen.
Die Hochschulen stellen sicher, dass die gewählten Prüfungsformen nach Inhalt und Dauer hochschuladäquat sind.
Sie achten auf eine Ausgewogenheit der Prüfungsformen.
Die Mindestdauer von mündlichen Prüfungen soll je Studierenden und Fach 15 Minuten nicht unterschreiten.
Die Dauer von Klausurarbeiten soll 90 Minuten nicht unterschreiten.

(4) Modulprüfungen können bei Nichtbestehen in der Regel mindestens zweimal wiederholt werden.
Näheres zur Anzahl der Wiederholungsmöglichkeiten sowie zu den Fristen für die Wiederholungsversuche regeln die Hochschulen in ihren Ordnungen.
Gleiches gilt für die Wiederholbarkeit von Modulprüfungen zur Notenverbesserung.

(5) Die Hochschule bestimmt in ihren Ordnungen den Zeitpunkt, bis zu dem die Modulprüfungen und die Abschlussarbeit abgelegt werden sollen.
Die Fristen sind so festzulegen, dass sämtliche Modulprüfungen und die Abschlussarbeit einschließlich eines gegebenenfalls von der Hochschule vorgesehenen Kolloquiums innerhalb der für den Studiengang festgesetzten Regelstudienzeit vollständig abgelegt und bewertet werden können.
Wenn das Studienangebot aufgrund einer Notlage, in der eine reguläre Aufgabenwahrnehmung durch die Hochschulen nicht möglich ist, nicht sichergestellt werden kann, können die Hochschulen die Fristen angemessen verlängern.

(6) Schriftliche und mündliche Leistungen, deren Bestehen Voraussetzung für die Fortsetzung des Studiums ist (letzte Wiederholungsmöglichkeit), sind in der Regel von mindestens zwei Prüferinnen oder Prüfern zu bewerten.
Mündliche Leistungen sind von einem Prüfenden in der Regel in Gegenwart einer sachkundigen Beisitzerin oder eines sachkundigen Beisitzers abzunehmen.
Als Beisitzerin oder Beisitzer kann nur bestellt werden, wer mindestens den entsprechenden Abschlussgrad, der mit dem Studiengang erlangt werden soll, oder einen vergleichbaren Hochschulgrad, eine vergleichbare staatliche oder kirchliche Prüfung in einem vergleichbaren Studiengang abgelegt hat.

#### § 7 Abschlussarbeit

(1) In Bachelor- und Masterstudiengängen ist die Anfertigung einer Abschlussarbeit (Bachelorarbeit beziehungsweise Masterarbeit), die selbst kein Modul ist, obligatorisch.
Die Bachelorarbeit hat einen Bearbeitungsumfang von mindestens sechs und höchstens zwölf Leistungspunkten.
Die Masterarbeit hat einen Bearbeitungsumfang von mindestens 15 und höchstens 30 Leistungspunkten.
In begründeten Ausnahmefällen kann der Bearbeitungsumfang in künstlerischen Studiengängen an der Hochschule für Film und Fernsehen Potsdam-Babelsberg bis zu 20 Leistungspunkte für die Bachelorarbeit und bis zu 40 Leistungspunkte für die Masterarbeit betragen.

(2) Das Thema der Abschlussarbeit wird frühestens nach erfolgreichem Abschluss der deutlichen Mehrzahl der Studien- und Prüfungsleistungen, in der Regel nach erfolgreichem Abschluss von Studien- und Prüfungsleistungen im Umfang von mindestens 75 Prozent der Gesamtzahl der im Studiengang zu absolvierenden Leistungspunkte abzüglich der Leistungspunkte für die Abschlussarbeit und für das Kolloquium ausgegeben.
Nach erfolgreichem Abschluss sämtlicher Studien- und Prüfungsleistungen soll das Thema der Abschlussarbeit spätestens vier Wochen nach Anmeldung ausgegeben werden.

(3) Die Abschlussarbeiten und ein von der Prüfungsordnung vorgesehenes Kolloquium als mündliche Prüfung sind von mindestens zwei Prüferinnen oder Prüfern aus dem Fachgebiet, auf das sich die Abschlussarbeit bezieht, zu bewerten.
Eine Prüferin oder ein Prüfer, in der Regel die Erstprüferin oder der Erstprüfer, muss die Einstellungsvoraussetzungen für Professoren nach dem Brandenburgischen Hochschulgesetz erfüllen und in dem Fachgebiet, auf das sich die Abschlussarbeit bezieht, eine eigenverantwortliche, selbstständige Lehrtätigkeit an der Hochschule ausüben.
Sie oder er kann auch Juniorprofessorin oder Juniorprofessor in dem Fachgebiet sein.

(4) Die Bachelorarbeit soll innerhalb von vier Wochen, die Masterarbeit innerhalb von sechs Wochen bewertet werden.

(5) Die Bachelorarbeit, die Masterarbeit und ein nach der Prüfungsordnung vorgesehenes Kolloquium können bei Nichtbestehen jeweils einmal wiederholt werden.

#### § 8 Studiengangsprofil

(1) Masterstudiengänge können nach den Profiltypen „anwendungsorientiert“ oder „forschungsorientiert“ differenziert werden.
Künstlerische Masterstudiengänge an der Hochschule für Film und Fernsehen Potsdam-Babelsberg sollen ein besonderes künstlerisches Profil haben.
Wird eine Profilzuordnung vorgenommen, ist sie im Diploma Supplement darzustellen und wird im Akkreditierungsverfahren überprüft.

(2) Masterstudiengänge, mit denen die Voraussetzungen für ein Lehramt vermittelt werden, haben ein lehramtsbezogenes Profil auszuweisen.

#### § 9 Abschlussbezeichnungen, Grade

(1) Eine Differenzierung der Grade nach der Dauer der Regelstudienzeit, nach dem Profiltyp (Masterstudiengänge) und nach dem Hochschultyp erfolgt nicht.

(2) Bachelorgrade mit dem Zusatz „honours“ („B.A.
hon.“) dürfen nicht verliehen werden.

(3) Für Bachelor- und Mastergrade sind folgende Bezeichnungen zu verwenden:

1.  Bachelor of Arts (B.A.); Master of Arts (M.A.) in den Fächergruppen Sprach- und Kulturwissenschaften, Sport und Sportwissenschaften, Sozialwissenschaft, Kunstwissenschaft sowie in den Fächergruppen Künstlerisch-angewandte Studiengänge und Darstellende Kunst,
2.  Bachelor of Science (B.Sc.); Master of Science (M.Sc.) in den Fächergruppen Mathematik, Naturwissenschaften, Agrar-, Forst- und Ernährungswissenschaften,
3.  nach der inhaltlichen Ausrichtung des Studiengangs Bachelor of Science (B.Sc.); Master of Science (M.Sc.) oder Bachelor of Engineering (B.Eng.); Master of Engineering (M.Eng.) in den Ingenieurwissenschaften,
4.  nach der inhaltlichen Ausrichtung des Studiengangs Bachelor of Arts (B.A.); Master of Arts (M.A.) oder Bachelor of Science (B.Sc.); Master of Science (M.Sc.) in den Wirtschaftswissenschaften,
5.  Bachelor of Laws (LL.B.); Master of Laws (LL.M.) in den Rechtswissenschaften, soweit es sich nicht um staatlich geregelte Studiengänge handelt,
6.  Bachelor of Education (B.Ed.); Master of Education (M.Ed.) in Studiengängen, mit denen die Voraussetzungen für ein Lehramt vermittelt werden,
7.  Bachelor of Fine Arts (B.F.A.); Master of Fine Arts (M.F.A.) in den Fächergruppen Animation, Kamera, Montage, Regie, Sound, Szenografie an der Hochschule für Film und Fernsehen Potsdam-Babelsberg,
8.  Bachelor of Music (B.Mus.); Master of Music (M.Mus.) im Studiengang Filmmusik an der Hochschule für Film und Fernsehen Potsdam-Babelsberg.

(4) Bei interdisziplinären Studiengängen richtet sich die Abschlussbezeichnung nach dem Fachgebiet, dessen Bedeutung im Studiengang überwiegt.

(5) Fachliche Zusätze zu den Abschlussbezeichnungen und der Zusatz der verleihenden Hochschule sind ausgeschlossen.

(6) Für weiterbildende Masterstudiengänge können auch Grade verliehen werden, die von den Abschlussbezeichnungen nach Absatz 3 abweichen (zum Beispiel Master of Business Administration, MBA).
Die Abschlussbezeichnung ist in deutscher oder englischer Sprache festzulegen; gemischtsprachige Bezeichnungen sind ausgeschlossen.

#### § 10 Übergangsregelungen

Für Studiengänge, die vor dem Inkrafttreten dieser Verordnung eingerichtet und genehmigt wurden, findet die Hochschulprüfungsverordnung vom 7.
Juni 2007 (GVBl.
II S. 134), die zuletzt durch Verordnung vom 15.
Juni 2010 (GVBl.
II Nr. 33) geändert worden ist, mit der Maßgabe Anwendung, dass diese Studiengänge innerhalb einer angemessenen Frist, spätestens jedoch bis zum 31. Dezember 2016 oder vor einer erneuten Genehmigung oder Akkreditierung oder Reakkreditierung des Studiengangs den Vorschriften dieser Verordnung anzupassen sind.

#### § 11 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Hochschulprüfungsverordnung vom 7.
Juni 2007 (GVBl.
II S. 134), die zuletzt durch Verordnung vom 15.
Juni 2010 (GVBl.
II Nr. 33) geändert worden ist, außer Kraft.

Potsdam, den 4.
März 2015

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Prof.
Dr.-Ing.
Dr.
Sabine Kunst