## Verordnung über Leistungsbezüge sowie Forschungs- und Lehrzulagen für Professoren und Professorinnen und hauptamtliche Hochschulleitungen im Geltungsbereich des Brandenburgischen Hochschulgesetzes (Hochschulleistungsbezügeverordnung - HLeistBV)

Auf Grund des § 37 Absatz 2 des Brandenburgischen Besoldungsgesetzes vom 20.
November 2013 (GVBl.
I Nr. 32 S. 2, Nr.
34) verordnet die Ministerin für Wissenschaft, Forschung und Kultur im Einvernehmen mit dem Minister der Finanzen:

§ 1   
Gegenstand
-----------------

Diese Verordnung gilt für Professorinnen und Professoren und hauptamtliche Hochschulleitungen in den Ämtern der Besoldungsgruppen W 2 und W 3 der Besoldungsordnung W.
Sie regelt nach Maßgabe des § 37 Absatz 2 des Brandenburgischen Besoldungsgesetzes die Grundsätze, Zuständigkeiten und das Verfahren sowie die Voraussetzungen und die Kriterien der Vergabe von Leistungsbezügen einschließlich deren Ruhegehaltfähigkeit.
Sie regelt ferner die Grundsätze, Zuständigkeiten und das Verfahren für die Gewährung von Forschungs- und Lehrzulagen sowie für die Überschreitung des Besoldungsdurchschnitts.

§ 2   
Berufungs- und Bleibe-Leistungsbezüge
--------------------------------------------

(1) Über die Gewährung von Berufungs- und Bleibe-Leistungsbezügen entscheidet die Präsidentin oder der Präsident, in den Fällen des § 34 Absatz 2 des Brandenburgischen Besoldungsgesetzes mit Zustimmung der für die Hochschulen zuständigen obersten Landesbehörde.
Die Präsidentin oder der Präsident trifft die Entscheidung auf Vorschlag der Dekanin oder des Dekans.
Die Entscheidungsgründe sind aktenkundig zu machen.
Die Kanzlerin oder der Kanzler oder die für die Leitung der Verwaltung zuständige Vizepräsidentin oder der für die Leitung der Verwaltung zuständige Vizepräsident wirkt beratend mit und bereitet die Entscheidungen vor.
§ 9 der Landeshaushaltsordnung bleibt unberührt.

(2) Unbefristet gewährte Berufungs- und Bleibe-Leistungsbezüge nehmen an den regelmäßigen Besoldungsanpassungen teil.

(3) Wird eine Professorin oder ein Professor ohne Änderung der Besoldungsgruppe an eine andere Hochschule im Geltungsbereich des Brandenburgischen Besoldungsgesetzes versetzt, so bleiben erworbene Ansprüche auf Leistungsbezüge nach Absatz 1 unberührt.

§ 3   
Besondere Leistungsbezüge
--------------------------------

(1) Über die Gewährung besonderer Leistungsbezüge entscheidet die Präsidentin oder der Präsident, in den Fällen des § 34 Absatz 2 des Brandenburgischen Besoldungsgesetzes mit Zustimmung der für die Hochschulen zuständigen obersten Landesbehörde.
Die Präsidentin oder der Präsident trifft die Entscheidung auf Vorschlag der Dekanin oder des Dekans.
Die Entscheidungsgründe sind aktenkundig zu machen.
Die Kanzlerin oder der Kanzler oder die für die Leitung der Verwaltung zuständige Vizepräsidentin oder der für die Leitung der Verwaltung zuständige Vizepräsident wirkt beratend mit und bereitet die Entscheidungen vor.
§ 9 der Landeshaushaltsordnung bleibt unberührt.

(2) Die Hochschulen bestimmen im Einzelnen geeignete Kriterien zur Bemessung der besonderen Leistungen in den Bereichen Forschung, Lehre, Kunst, Weiterbildung und Nachwuchsförderung.
Als Kriterien können entsprechend dem jeweiligen Aufgabenprofil beispielsweise herangezogen werden:

1.  das besondere Engagement bei der Betreuung Studierender, Hochbegabter, Absolventinnen und Absolventen sowie des wissenschaftlichen und künstlerischen Nachwuchses,
2.  das besondere Engagement bei Studienreformangelegenheiten, bei der Entwicklung innovativer Studiengänge und von Weiterbildungsangeboten, beim Fernstudium und bei der Qualitätssicherung,
3.  besondere Lehrerfolge und Lehrtätigkeiten, die über die gesetzliche Lehrverpflichtung einschließlich der vom Lehrdeputat umfassten Weiterbildung hinaus geleistet werden,
4.  das herausragende internationale Engagement in Wissenschaft, Forschung und Kunst, bei der Betreuung und Integration ausländischer Studierender sowie beim internationalen Austausch,
5.  das besondere Engagement bei der Bildung von Forschungsschwerpunkten und Sonderforschungsbereichen, beim Wissenschaftstransfer einschließlich Existenzgründungen und Erfinderverwertungen sowie bei Ausstellungen, Konzerten, Aufführungen und bei künstlerischen Entwicklungsvorhaben und Projekten,
6.  das besondere Engagement bei der Kooperation mit anderen Hochschulen, mit Schulen sowie mit Einrichtungen von Wissenschaft, Kunst und Praxis,
7.  das besondere Engagement bei der Gleichstellung von Wissenschaftlerinnen und Wissenschaftlern,
8.  ein besonders hoher Anteil an Drittmitteln, Weiterbildungseinnahmen und Sponsorenmitteln.
    Die Einbringung von Drittmitteln kann nur berücksichtigt werden, soweit nicht aus demselben Anlass eine Forschungs- und Lehrzulage nach § 10 gewährt wird.

(3) Bei der Bewertung der individuellen Leistung sind auch die Ergebnisse der Lehr- und Forschungsevaluation sowie die Mitwirkung an der Erfüllung von Hochschulverträgen und anderen Ziel- und Leistungsvereinbarungen über Aufgabenwahrnehmung und Entwicklung der Hochschulen zu berücksichtigen.
Der Gewährungs- und der Bewertungszeitraum für die individuelle Leistung müssen zueinander in einem angemessenen Verhältnis stehen.
Der Bewertungszeitraum soll fünf Jahre nicht überschreiten.
Die Erwägungen und Feststellungen, die der Bewertung der individuellen Leistung zugrunde liegen, sind aktenkundig zu machen.
Im Fall von Professorinnen und Professoren, die in einem gemeinsamen Berufungsverfahren berufen worden sind, dürfen die Bewertungsergebnisse der außerhochschulischen Forschungseinrichtung übernommen werden.

(4) Unbefristet gewährte besondere Leistungsbezüge nehmen an den regelmäßigen Besoldungsanpassungen teil.

(5) Wird eine Professorin oder ein Professor ohne Änderung der Besoldungsgruppe an eine andere Hochschule im Geltungsbereich des Brandenburgischen Besoldungsgesetzes versetzt, so bleiben erworbene Ansprüche auf Leistungsbezüge nach Absatz 1 unberührt.

§ 4   
Funktions-Leistungsbezüge der hauptamtlichen Hochschulleiterinnen und Hochschulleiter sowie der Vizepräsidentinnen und Vizepräsidenten
---------------------------------------------------------------------------------------------------------------------------------------------

(1) Hauptamtliche Hochschulleiterinnen und Hochschulleiter sowie hauptamtliche und nebenamtliche Vizepräsidentinnen und Vizepräsidenten, die ein Amt der Besoldungsordnung W innehaben, erhalten für die Dauer der Wahrnehmung dieser Aufgaben als Funktions-Leistungsbezüge folgende Monatsbeträge:

1.  die Präsidentin oder der Präsident
    
    1.  der Universität Potsdam 53 Prozent,
    2.  der Brandenburgischen Technischen Universität Cottbus-Senftenberg 44 Prozent,
    3.  der Europa-Universität Viadrina Frankfurt (Oder) 36 Prozent,
    4.  der Filmuniversität Babelsberg Konrad Wolf 28 Prozent,
    5.  der Fachhochschule Brandenburg, der Fachhochschule Potsdam, der Hochschule für nachhaltige Entwicklung Eberswalde (FH) und der Technischen Hochschule Wildau (FH) 28 Prozent
    
    des Grundgehalts der Besoldung aus der Besoldungsgruppe W 3,
    
2.  hauptamtliche Vizepräsidentinnen und Vizepräsidenten
    
    1.  der Universität Potsdam 32 Prozent,
    2.  der Brandenburgischen Technischen Universität Cottbus-Senftenberg 27 Prozent,
    3.  der Europa-Universität Viadrina Frankfurt (Oder) 22 Prozent,
    4.  der Filmuniversität Babelsberg Konrad Wolf 20 Prozent,
    5.  der Fachhochschule Brandenburg, der Fachhochschule Potsdam, der Hochschule für nachhaltige Entwicklung Eberswalde (FH) und der Technischen Hochschule Wildau (FH) 20 Prozent
    
    des Grundgehalts der Besoldung aus der Besoldungsgruppe W 3 sowie
    
3.  nebenamtliche Vizepräsidentinnen und Vizepräsidenten
    
    1.  der Universität Potsdam 26 Prozent,
    2.  der Brandenburgischen Technischen Universität Cottbus-Senftenberg 23 Prozent,
    3.  der Europa-Universität Viadrina Frankfurt (Oder) 20 Prozent,
    4.  der Filmuniversität Babelsberg Konrad Wolf 18 Prozent,
    5.  der Fachhochschule Brandenburg, der Fachhochschule Potsdam, der Hochschule für nachhaltige Entwicklung Eberswalde (FH) und der Technischen Hochschule Wildau (FH) 18 Prozent
    
    des zum Zeitpunkt ihrer Wahl zur Vizepräsidentin oder zum Vizepräsidenten maßgeblichen Grundgehalts der Besoldung aus der Besoldungsgruppe W 3.
    

(2) Auf Antrag des für die Wahl der Präsidentin oder des Präsidenten zuständigen Organs der Hochschule und nach Zustimmung des für die Hochschulen zuständigen Mitglieds der Landesregierung kann abweichend von Absatz 1 Nummer 1 auch ein höherer Funktions-Leistungsbezug gewährt werden, wenn dies aus folgenden Gründen erforderlich ist:

1.  zur Gewinnung einer Persönlichkeit, deren Eignung für eine Präsidentschaft in herausragender Weise nachgewiesen ist und deren Gewinnung die Bedeutung der Hochschule hebt,
2.  zur Gewinnung einer Persönlichkeit, die in ihrem bisherigen Dienst- oder Beschäftigungsverhältnis Entgelt oder Bezüge erhält, die die ihr unter Anwendung von Absatz 1 Nummer 1 zu gewährenden Bezüge übersteigen oder absehbar künftig übersteigen werden,
3.  zur Verhinderung der Abwanderung einer amtierenden Präsidentin oder eines amtierenden Präsidenten in den Bereich außerhalb der staatlichen Hochschulen des Landes Brandenburg, sofern ein höherwertiges Einstellungsangebot eines anderen Arbeitgebers beziehungsweise Dienstherrn nachgewiesen ist.

(3) Auf Antrag des für die Wahl der hauptamtlichen Vizepräsidentin oder des hauptamtlichen Vizepräsidenten zuständigen Organs der Hochschule und nach Zustimmung des für die Hochschulen zuständigen Mitglieds der Landesregierung kann abweichend von Absatz 1 Nummer 2 auch ein höherer Funktions-Leistungsbezug gewährt werden, wenn dies zur Gewinnung einer Persönlichkeit erforderlich ist, die in ihrem bisherigen Dienst- oder Beschäftigungsverhältnis Entgelt oder Bezüge erhält, die die ihr unter Anwendung von Absatz 1 Nummer 2 zu gewährenden Bezüge übersteigen oder absehbar künftig übersteigen werden.
Betrifft der Antrag die hauptberufliche Vizepräsidentin oder den hauptberuflichen Vizepräsidenten der Brandenburgischen Technischen Universität Cottbus-Senftenberg, die oder der gemäß § 8 Absatz 3 Satz 2 des Gesetzes zur Weiterentwicklung der Hochschulregion Lausitz bestellt wird, ist die Gründungspräsidentin oder der Gründungspräsident der Brandenburgischen Technischen Universität Cottbus-Senftenberg für die Stellung des Antrags zuständig.

§ 5   
Sonstige Funktions-Leistungsbezüge
-----------------------------------------

(1) Über die Gewährung von Funktions-Leistungsbezügen für die Wahrnehmung besonderer Aufgaben im Rahmen der Hochschulselbstverwaltung (§ 33 Satz 2 des Brandenburgischen Besoldungsgesetzes) entscheidet die Präsidentin oder der Präsident, im Fall von Professorinnen und Professoren, die in einem gemeinsamen Berufungsverfahren berufen worden sind und eine Leitungsfunktion in einer außerhochschulischen Forschungseinrichtung übernehmen (§ 33 Satz 3 des Brandenburgischen Besoldungsgesetzes), im Einvernehmen mit der außerhochschulischen Forschungseinrichtung.
Die Entscheidungsgründe sind aktenkundig zu machen.
Die Kanzlerin oder der Kanzler oder die für die Leitung der Verwaltung zuständige Vizepräsidentin oder der für die Leitung der Verwaltung zuständige Vizepräsident wirkt beratend mit und bereitet die Entscheidungen vor.
§ 9 der Landeshaushaltsordnung bleibt unberührt.

(2) Besondere Aufgaben im Rahmen der Hochschulselbstverwaltung sind insbesondere die Tätigkeit als Dekanin oder Dekan oder die Wahrnehmung einer Funktion, die ebenfalls mit erheblicher zusätzlicher Belastung und Verantwortung verbunden ist.
Bei der Bemessung der Funktions-Leistungsbezüge ist eine etwaige Ermäßigung der Lehrverpflichtung zu berücksichtigen.

(3) Die Funktions-Leistungsbezüge können als Monatsbeträge oder als Einmalzahlung gewährt werden.
Sie sind so zu bemessen, dass sie in einem angemessenen Verhältnis zu den Funktions-Leistungsbezügen der Präsidentin oder des Präsidenten und der Vizepräsidentinnen oder der Vizepräsidenten stehen.

§ 6   
Berücksichtigung des Mindest-Leistungsbezugs
---------------------------------------------------

(1) Berufungs- und Bleibe-Leistungsbezüge, besondere Leistungsbezüge sowie Funktions-Leistungsbezüge werden auf den Betrag der mindestens zu gewährenden Leistungsbezüge (§ 30 Absatz 2 Satz 1 des Brandenburgischen Besoldungsgesetzes) angerechnet.
Maßgeblich für die Anrechnung ist jeweils die Summe der monatlich gewährten Leistungsbezüge.
Dabei werden Funktions-Leistungsbezüge gemäß § 5 Absatz 2 nur berücksichtigt, soweit sie 300 Euro übersteigen.

(2) Sofern Leistungsbezüge als Einmalzahlung gewährt werden, ist für die Dauer eines Jahres ab dem Monat der Auszahlung bei der Anrechnung gemäß Absatz 1 monatlich jeweils ein Zwölftel der Einmalzahlung zu berücksichtigen.

§ 7   
Beteiligung bei der Vergabe von Leistungsbezügen an gemeinsam berufene Professorinnen und Professoren
------------------------------------------------------------------------------------------------------------

Vor der Entscheidung über Leistungsbezüge nach den §§ 2 bis 5 an Professorinnen und Professoren, die nach einem gemeinsamen Berufungsverfahren überwiegend an einer Forschungseinrichtung außerhalb der Hochschule tätig sind oder werden sollen, ist das Einvernehmen mit der für die Hochschulen zuständigen obersten Landesbehörde herzustellen.

§ 8   
Einhaltung des Besoldungsdurchschnitts, Berichtspflicht
--------------------------------------------------------------

(1) Die für die Hochschulen zuständige oberste Landesbehörde entscheidet nach Bekanntgabe des für das jeweilige Jahr bekannt gemachten Besoldungsdurchschnitts (§ 30 Absatz 8 des Brandenburgischen Besoldungsgesetzes), ob und in welcher Höhe der Besoldungsdurchschnitt überschritten werden kann (§ 30 Absatz 3 Satz 2 des Brandenburgischen Besoldungsgesetzes).
Sie legt die für die jeweilige Hochschule maßgeblichen durchschnittlichen Besoldungsausgaben je Professorin beziehungsweise Professor unter Berücksichtigung des für das jeweilige Jahr bekannt gemachten Besoldungsdurchschnitts sowie der Entscheidung nach Satz 1 fest und teilt diese den Hochschulen mit.

(2) Die Hochschulen berichten der für die Hochschulen zuständigen obersten Landesbehörde jeweils zum 1.
April eines Jahres über die Verteilung der Leistungsbezüge des Vorjahres auf Berufungs- und Bleibe-Leistungsbezüge, besondere Leistungsbezüge und Funktions-Leistungsbezüge.

§ 9   
Ruhegehaltfähigkeit
--------------------------

Soweit Berufungs- und Bleibe-Leistungsbezüge sowie besondere Leistungsbezüge nach Maßgabe des § 35 Absatz 2 Satz 1 zweiter Halbsatz und Absatz 5 des Brandenburgischen Besoldungsgesetzes für ruhegehaltfähig erklärt werden können, entscheidet die Präsidentin oder der Präsident, in den Fällen des § 35 Absatz 5 des Brandenburgischen Besoldungsgesetzes im Einvernehmen mit der für die Hochschulen zuständigen obersten Landesbehörde.

§ 10   
Forschungs- und Lehrzulage
----------------------------------

Über die Gewährung einer Forschungs- oder Lehrzulage entscheidet die Präsidentin oder der Präsident nach Anhörung der Dekanin oder des Dekans auf Antrag der Professorin oder des Professors, in den Fällen des § 36 Absatz 2 Satz 2 des Brandenburgischen Besoldungsgesetzes mit Zustimmung der für die Hochschulen zuständigen obersten Landesbehörde.
Die Entscheidungsgründe sind aktenkundig zu machen.
Die Kanzlerin oder der Kanzler oder die oder der für die Leitung der Verwaltung zuständige Vizepräsidentin oder zuständige Vizepräsident wirkt beratend mit und bereitet die Entscheidungen vor.
§ 9 der Landeshaushaltsordnung bleibt unberührt.

§ 11   
Satzung
---------------

Die Hochschulen regeln, soweit die Präsidentin oder der Präsident entscheidet, durch Satzung das Verfahren zur Gewährung von Leistungsbezügen und Forschungs- und Lehrzulagen, die Kriterien zur Bemessung der besonderen Leistungen im Sinne des § 3 Absatz 2, das Nähere zu den Funktions-Leistungsbezügen nach § 5 sowie das Verfahren für Entscheidungen nach § 9.

§ 12   
Übergangsregelung für die Brandenburgische Technische Universität Cottbus und die Hochschule Lausitz (FH)
-----------------------------------------------------------------------------------------------------------------

**(außer Kraft getreten)**

§ 13  
Inkrafttreten, Außerkrafttreten
--------------------------------------

(1) § 4 Absatz 1 Nummer 1 Buchstabe b, Nummer 2 Buchstabe b, Nummer 3 Buchstabe b und Absatz 2 Satz 2 tritt mit Wirkung vom 1.
Juli 2013 in Kraft.
Im Übrigen tritt diese Verordnung mit Wirkung vom 1.
Januar 2013 in Kraft.

(2) § 12 tritt mit Wirkung vom 1.
Juli 2013 außer Kraft.

(3) Die Hochschulleistungsbezügeverordnung vom 23.
März 2005 (GVBl.
II S.
152) tritt mit Wirkung vom 1.
Januar 2013 außer Kraft.

Potsdam, den 17.
Juli 2014

Die Ministerin für Wissenschaft, Forschung und Kultur

Prof.
Dr.-Ing.
Dr.
Sabine Kunst