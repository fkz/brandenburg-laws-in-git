## Verordnung über die Besoldung und Dienstaufwandsentschädigung der hauptamtlichen Wahlbeamtinnen und Wahlbeamten auf Zeit der Gemeinden und Gemeindeverbände im Land Brandenburg (Brandenburgische Kommunalbesoldungsverordnung - BbgKomBesV)

Auf Grund des § 17 Absatz 2 und des § 21 des Brandenburgischen Besoldungsgesetzes vom 20. November 2013 (GVBl.
I Nr. 32, 34) verordnet der Minister des Innern und für Kommunales im Einvernehmen mit dem Minister der Finanzen:

#### § 1 Geltungsbereich

Diese Verordnung regelt die besoldungsrechtliche Einstufung und die Höhe der Dienstaufwandsentschädigung der hauptamtlichen Wahlbeamtinnen und Wahlbeamten auf Zeit in den Gemeinden und Gemeindeverbänden und die Höhe der Dienstaufwandsentschädigung der Beamtinnen und Beamten, die als allgemeine Stellvertreter der Hauptverwaltungsbeamtinnen und Hauptverwaltungsbeamten bestimmt worden sind.

#### § 2 Allgemeine Vorschriften

(1) Für die Einstufung der Ämter der hauptamtlichen Wahlbeamtinnen und Wahlbeamten auf Zeit sowie die Höhe der Dienstaufwandsentschädigung ist die bei der letzten Volkszählung ermittelte und vom Amt für Statistik Berlin-Brandenburg auf den 30.
Juni des Vorjahres fortgeschriebene Einwohnerzahl maßgebend.
Im Jahr, in dem eine Volkszählung stattgefunden hat, ist die Einwohnerzahl am Tag der Volkszählung maßgebend.

(2) Eine Gemeinde mit weniger als 30 000 Einwohnern, die als Heilbad, Kurort oder Erholungsort nach den Vorschriften des Brandenburgischen Kurortegesetzes ganz oder teilweise anerkannt ist, kann bestimmen, dass der Einwohnerzahl nach Absatz 1 die jahresdurchschnittliche Zahl der täglichen Fremdenübernachtungen hinzugerechnet wird, wenn die Zahl der Übernachtungen mindestens 40 Prozent der Einwohnerzahl nach Absatz 1 beträgt und die Hauptverwaltungsbeamtin oder der Hauptverwaltungsbeamte eine zur Gemeindeverwaltung gehörende Kurverwaltung unmittelbar leitet.

(3) Maßgebende Einwohnerzahl der Verbandsgemeinden und der Ämter ist die Summe der Einwohnerzahlen ihrer jeweiligen Mitgliedsgemeinden nach den Absätzen 1 und 2.
Satz 1 gilt für mitverwaltende Gemeinden im Sinne von § 16 Absatz 1 des Verbandsgemeinde- und Mitverwaltungsgesetzes vom 15.
Oktober 2018 (GVBl.
I Nr.
22) entsprechend.

(4) Werden Körperschaften umgebildet, so ist vom Inkrafttreten der Neugliederung an die Einwohnerzahl der umgebildeten oder der neuen Körperschaft nach den Absätzen 1 und 2 zu errechnen.

#### § 3 Einstufung der Wahlbeamtinnen und Wahlbeamten auf Zeit

(1) Die Ämter der hauptamtlichen Wahlbeamtinnen und Wahlbeamten auf Zeit sind den Besoldungsgruppen der Besoldungsordnungen A und B des Brandenburgischen Besoldungsgesetzes nach Maßgabe der Absätze 2 bis 5 zuzuordnen.
Dabei bleibt die Besoldungsgruppe B 1 außer Betracht.

(2) Das Amt der hauptamtlichen Bürgermeisterin (in kreisfreien Städten der Oberbürgermeisterin) oder des hauptamtlichen Bürgermeisters (in kreisfreien Städten des Oberbürgermeisters) wird wie folgt eingestuft:

1.  in kreisangehörigen Gemeinden mit einer Einwohnerzahl
    
    bis zu 10 000 in die Besoldungsgruppe A 15,
    
    bis zu 15 000 in die Besoldungsgruppe A 16,
    
    bis zu 25 000 in die Besoldungsgruppe B 2,
    
    bis zu 40 000 in die Besoldungsgruppe B 3,
    
    bis zu 60 000 in die Besoldungsgruppe B 4,
    
    über 60 000 in die Besoldungsgruppe B 5;
    
2.  in kreisfreien Städten mit einer Einwohnerzahl
    
    bis zu 100 000 in die Besoldungsgruppe B 5,
    
    bis zu 150 000 in die Besoldungsgruppe B 6,
    
    über 150 000 in die Besoldungsgruppe B 7.
    

(3) Für die Einstufung der Ämter der Verbandsgemeindebürgermeisterin oder des Verbandsgemeindebürgermeisters und der Amtsdirektorin oder des Amtsdirektors gilt Absatz 2 Nummer 1 entsprechend.

(4) Das Amt der Landrätin oder des Landrates wird in Landkreisen mit einer Einwohnerzahl

bis zu 75 000 in die Besoldungsgruppe B 4,

bis zu 150 000 in die Besoldungsgruppe B 5,

bis zu 225 000 in die Besoldungsgruppe B 6,

über 225 000 in die Besoldungsgruppe B 7

eingestuft.

(5) Die Ämter der weiteren Wahlbeamtinnen und Wahlbeamten werden wie folgt eingestuft:

1.  das Amt der oder des Ersten Beigeordneten zwei Besoldungsgruppen unter der nach den Absätzen 2, 3 oder 4 maßgebenden Besoldungsgruppe,
2.  die Ämter der Beigeordneten drei Besoldungsgruppen unter der nach den Absätzen 2, 3 oder 4 maßgebenden Besoldungsgruppe.

(6) Abweichend von den Absätzen 2 bis 5 erfolgt eine Einstufung in die nächsthöhere Besoldungsgruppe, wenn die Wahlbeamtin oder der Wahlbeamte nach Ablauf der Amtszeit bei der unmittelbar darauf folgenden Wahl in dasselbe Amt wieder berufen wird.
Dies gilt auch für Wahlbeamtinnen und Wahlbeamte, deren Ämter infolge einer Umbildung von Gebietskörperschaften entfallen sind und die in ein gleich oder niedriger bewertetes Amt bei einer anderen Gebietskörperschaft berufen wurden und in beiden Ämtern eine Amtszeit von insgesamt acht Jahren abgeleistet haben.

(7) Erhöht sich die maßgebliche Einwohnerzahl während einer Amtszeit über eine maßgebliche Größenklasse nach den Absätzen 2 bis 4 mit der Folge, dass das Wahlamt einer höheren Besoldungsgruppe zugeordnet ist, bedarf es für die Verleihung des entsprechenden statusrechtlichen Amtes einer Entscheidung der Vertretungskörperschaft.
Satz 1 gilt für Wahlbeamte oder Wahlbeamtinnen, die nach Absatz 6 Satz 1 in dasselbe Amt wiederberufen worden sind, mit der Maßgabe, dass das Amt der nächsthöheren Besoldungsgruppe zuzuordnen ist.

#### § 4 Bemessung des Grundgehalts

(1) Für das Aufsteigen in den Stufen des Grundgehalts gelten die §§ 25 bis 28 des Brandenburgischen Besoldungsgesetzes mit der Maßgabe, dass abweichend von § 25 Absatz 3 Satz 1 des Brandenburgischen Besoldungsgesetzes mit Wirkung vom Ersten des Monats, in dem die Berufung wirksam wird, mindestens eine Zuordnung zu der Stufe 10 der jeweiligen Besoldungsgruppe erfolgt.

(2) Die Wahlbeamtinnen und Wahlbeamten auf Zeit, die am 6.
Februar 2018 bereits in einem Dienstverhältnis stehen, sind der Stufe zuzuordnen, die sich ausgehend von der Stufe 10 unter Berücksichtigung von Zeiten nach § 26 Absatz 1 des Brandenburgischen Besoldungsgesetzes ergibt.

#### § 5 Rechtsstand

Verringert sich die jeweils maßgebende Einwohnerzahl und kommt die Körperschaft dadurch in eine niedrigere Größenklasse, behalten die im Amt befindlichen Wahlbeamtinnen oder Wahlbeamten für ihre Person und für die Dauer ihrer Amtszeit die Bezüge der bisherigen Besoldungsgruppe.

#### § 6 Umfang des Leistungsanspruchs auf Dienstaufwandsentschädigungen

(1) Die Wahlbeamtinnen und Wahlbeamten können für die durch das Amt bedingten Mehraufwendungen eine steuerfreie Dienstaufwandsentschädigung zur Abdeckung des mit dem übertragenen Amt verbundenen zusätzlichen persönlichen Aufwandes nach Maßgabe von § 17 Absatz 1 des Brandenburgischen Besoldungsgesetzes erhalten.
Die Höhe der Dienstaufwandsentschädigung wird zu Beginn jeder Amtszeit durch Beschluss der kommunalen Vertretungskörperschaft unter Berücksichtigung der voraussichtlichen Höhe des Aufwandes nach Maßgabe der Bestimmungen dieser Verordnung festgesetzt.
Besteht der Leistungsanspruch nicht für einen vollen Kalendermonat, wird nur der Teil der Dienstaufwandsentschädigung gezahlt, der auf den Anspruchszeitraum entfällt.

(2) Der Leistungsanspruch ruht für die Dauer des Verbots der Führung der Dienstgeschäfte, einer vorläufigen Dienstenthebung im Zusammenhang mit einem Disziplinarverfahren oder einer Beurlaubung ohne Dienstbezüge.
Absatz 1 Satz 3 gilt entsprechend.

(3) Der Leistungsanspruch ruht, wenn die Dienstgeschäfte aus anderen Gründen ununterbrochen länger als einen Monat nicht wahrgenommen werden, für die über einen Monat hinausgehende Zeit.
Satz 1 gilt nicht für Zeiten eines Erholungsurlaubs.
Absatz 1 Satz 3 gilt entsprechend.

(4) Die Dienstaufwandsentschädigung ist bei einer wesentlichen Änderung der ihr zugrunde liegenden Feststellungen, insbesondere der Einwohnerzahl, unverzüglich anzupassen.

#### § 7 Dienstaufwandsentschädigung für Hauptverwaltungsbeamtinnen und Hauptverwaltungsbeamte

(1) Die Dienstaufwandsentschädigung der hauptamtlichen Bürgermeisterinnen (in kreisfreien Städten der Oberbürgermeisterinnen) oder der hauptamtlichen Bürgermeister (in kreisfreien Städten der Oberbürgermeister) darf monatlich in Gemeinden mit einer Einwohnerzahl

bis zu 10 000 den Betrag in Höhe von 160 Euro,

bis zu 15 000 den Betrag in Höhe von 195 Euro,

bis zu 25 000 den Betrag in Höhe von 225 Euro,

bis zu 40 000 den Betrag in Höhe von 260 Euro,

bis zu 60 000 den Betrag in Höhe von 295 Euro,

bis zu 100 000 den Betrag in Höhe von 335 Euro,

bis zu 150 000 den Betrag in Höhe von 375 Euro,

über 150 000 den Betrag in Höhe von 420 Euro

nicht überschreiten.

(2) Für die Dienstaufwandsentschädigung der Verbandsgemeindebürgermeisterinnen oder Verbandsgemeindebürgermeister und der Amtsdirektorinnen oder Amtsdirektoren gilt Absatz 1 entsprechend.

(3) Die Dienstaufwandsentschädigung der Landrätinnen und Landräte darf in Landkreisen mit einer Einwohnerzahl

bis zu 150 000 den Betrag in Höhe von 375 Euro,

über 150 000 den Betrag in Höhe von 420 Euro

nicht überschreiten.

#### § 8 Dienstaufwandsentschädigung für Beigeordnete

(1) Die Dienstaufwandsentschädigung für die Ersten Beigeordneten darf 75 Prozent, die der weiteren Beigeordneten 50 Prozent der nach § 7 Absatz 1 bis 3 jeweils maßgeblichen Beträge, aufgerundet auf volle Euro, nicht überschreiten.

(2) Soweit keine Beigeordneten berufen wurden, kann den zu allgemeinen Stellvertretern der Hauptverwaltungsbeamtinnen und Hauptverwaltungsbeamten bestellten Beamtinnen und Beamten mit Dienstbezügen eine Dienstaufwandsentschädigung bis zu 60 Prozent der nach § 7 Absatz 1 bis 3 jeweils maßgeblichen Beträge, aufgerundet auf volle Euro, gewährt werden.

#### § 9 Stellvertretung

Beamtinnen und Beamten, denen vertretungsweise die Wahrnehmung der Dienstgeschäfte eines mit einer Dienstaufwandsentschädigung ausgestatteten Amtes übertragen wird, kann eine Aufwandsentschädigung insoweit gewährt werden, als sie die Amtsinhaber nach § 6 Absatz 2 und 3 nicht mehr erhalten.
Erhält die Beamtin oder der Beamte bereits aufgrund des ihr oder ihm übertragenen Amtes eine Dienstaufwandsentschädigung, darf der insgesamt gewährte Betrag die Höchstgrenze der für das Amt des Vertretenen vorgesehenen Dienstaufwandsentschädigung nicht überschreiten.

#### § 10 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig treten außer Kraft:

1.  die Einstufungsverordnung vom 3.
    Februar 1992 (GVBl.
    II S. 76), die zuletzt durch Verordnung vom 2. Februar 2010 (GVBl.
    II Nr. 7) geändert worden ist, und
2.  die Kommunaldienstaufwandsentschädigungsverordnung vom 1.
    Dezember 1994 (GVBl. II S. 991), die durch Verordnung vom 28.
    November 2001 (GVBl.
    II S. 638) geändert worden ist.

Potsdam, den 2.
Februar 2018

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter