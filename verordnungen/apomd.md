## Verordnung über die Ausbildung und Prüfung für die Laufbahn des mittleren allgemeinen Verwaltungsdienstes im Land Brandenburg (Ausbildungs- und Prüfungsordnung mittlerer Dienst - APOmD)

Auf Grund des § 26 Absatz 1 Satz 1 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl. I S. 26), der durch Artikel 1 des Gesetzes vom 5.
Dezember 2013 (GVBl.
I Nr. 36) geändert worden ist, verordnet der Minister des Innern und für Kommunales im Einvernehmen mit der Ministerin der Finanzen und für Europa:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeine Regelungen

[§ 1 Geltungsbereich](#1)

[§ 2 Einstellungsbehörden](#2)

[§ 3 Bewerbung](#3)

[§ 4 Auswahlverfahren](#4)

[§ 5 Zulassung zur Ausbildung](#5)

[§ 6 Einstellung in den Vorbereitungsdienst](#6)

[§ 7 Dauer des Vorbereitungsdienstes](#7)

[§ 8 Rechtsstellung und Pflichten während des Vorbereitungsdienstes, Urlaub](#8)

[§ 9 Beendigung des Beamtenverhältnisses auf Widerruf](#9)

### Abschnitt 2  
Ausbildung

[§ 10 Ziel der Ausbildung](#10)

[§ 11 Gliederung der Ausbildung](#11)

[§ 12 Grundsätze der fachtheoretischen Ausbildung](#12)

[§ 13 Inhalt der fachtheoretischen Ausbildung](#13)

[§ 14 Wahlpflichtfächer](#14)

[§ 15 Grundsätze der berufspraktischen Ausbildungsabschnitte](#15)

[§ 16 Ablauf der berufspraktischen Ausbildungsabschnitte, Ausbildungsstellen](#16)

[§ 17 Bewertung der berufspraktischen Ausbildung](#17)

[§ 18 Wiederholung eines berufspraktischen Ausbildungsabschnitts](#18)

[§ 19 Fachtheoretische Leistungstests](#19)

[§ 20 Bewertung der Leistungen](#20)

### Abschnitt 3  
Prüfungen, Laufbahnbefähigung

[§ 21 Prüfungsamt](#21)

[§ 22 Prüfungskommission, Prüfende](#22)

[§ 23 Laufbahnprüfung](#23)

[§ 24 Zwischenprüfung](#24)

[§ 25 Abschlussprüfung](#25)

[§ 26 Schriftlicher Prüfungsteil](#26)

[§ 27 Mündlicher Prüfungsteil](#27)

[§ 28 Gesamtergebnis der Laufbahnprüfung](#28)

[§ 29 Bestehen der Laufbahnprüfung](#29)

[§ 30 Abschlusszeugnis](#30)

[§ 31 Wiederholung der Prüfung](#31)

[§ 32 Prüfungserleichterungen](#32)

[§ 33 Fernbleiben, Rücktritt](#33)

[§ 34 Unlauteres Verhalten im Prüfungsverfahren](#34)

[§ 35 Prüfungsakten, Einsichtnahme](#35)

[§ 36 Inkrafttreten](#36)

### Abschnitt 1  
Allgemeine Regelungen

#### § 1 Geltungsbereich

Diese Verordnung regelt die Ausbildung und Prüfung für die Laufbahn des mittleren allgemeinen Verwaltungsdienstes im Land Brandenburg.

#### § 2 Einstellungsbehörden

Einstellungsbehörden im Sinne dieser Verordnung sind:

1.  für die Landesverwaltung das für Inneres zuständige Ministerium und
2.  im kommunalen Bereich
    1.  die Gemeinden und Gemeindeverbände,
    2.  der Kommunale Versorgungsverband Brandenburg sowie
    3.  die kommunalen Anstalten des öffentlichen Rechts.

#### § 3 Bewerbung

(1) Bewerbungen für die Ausbildung gemäß § 1 sind an die Einstellungsbehörde gemäß § 2 zu richten.

(2) Die Einstellungsbehörde kann verlangen, dass der Bewerbung insbesondere folgende Nachweise und Unterlagen beizufügen sind:

1.  ein Lebenslauf,
2.  eine Kopie des Abschluss-/Abgangszeugnisses der zuletzt besuchten Schule; liegt dieses noch nicht vor, zunächst eine Kopie des letzten Zeugnisses,
3.  Nachweise über etwaige berufliche Tätigkeiten und Prüfungen und
4.  eine Einverständniserklärung der gesetzlichen Vertretung zur Bewerbung, falls die Bewerberin oder der Bewerber nicht volljährig ist.

(3) Die Einstellungsbehörde kann die Beibringung der in Absatz 2 genannten Unterlagen auf elektronischem Weg fordern.
Sie kann die Teilnahme am Auswahlverfahren nach § 4 davon abhängig machen, dass die einzureichenden Unterlagen fristgerecht und vollständig beigebracht werden.

#### § 4 Auswahlverfahren

(1) Vor der Entscheidung über die Zulassung zur Ausbildung wird in einem Auswahlverfahren festgestellt, ob die Bewerberinnen und Bewerber aufgrund ihrer Kenntnisse, Fähigkeiten und persönlichen Eigenschaften für die Einstellung in den Vorbereitungsdienst des mittleren allgemeinen Verwaltungsdienstes im Land Brandenburg geeignet sind.

(2) Dem Auswahlverfahren nach Absatz 1 kann eine Vorauswahl vorausgehen, die sich an der Eignung der Bewerberinnen und Bewerber zu orientieren hat.
Die Einstellungsbehörde kann die Vorauswahl nach Satz 1 insbesondere auf der Grundlage der Durchschnittsnote des letzten Schul- oder Berufsschulzeugnisses oder der letzten beiden Schul- oder Berufsschulzeugnisse oder auf der Grundlage der Noten einzelner Fächer des letzten Schul- oder Berufsschulzeugnisses treffen.
Hierzu kann sie die Beibringung der entsprechenden Zeugnisse verlangen.

(3) Das Auswahlverfahren bestimmt die Einstellungsbehörde für ihren Bereich.
Sie hat für Bewerberinnen und Bewerber desselben Einstellungstermins das gleiche Auswahlverfahren anzuwenden.

(4) Wer nicht zum Auswahlverfahren zugelassen wird oder im Auswahlverfahren erfolglos bleibt, ist schnellstmöglich, spätestens einen Monat vor dem Einstellungsdatum, schriftlich zu benachrichtigen.

#### § 5 Zulassung zur Ausbildung

(1) Zur Ausbildung kann zugelassen werden, wer

1.  im Auswahlverfahren nach § 4 ausgewählt worden ist,
2.  die Voraussetzungen des § 3 Absatz 2 und des § 10 Absatz 2 Nummer 1 des Landesbeamtengesetzes vom 3.
    April 2009 (GVBl.
    I S.
    26), das zuletzt durch Artikel 2 des Gesetzes vom 5.
    Juni 2019 (GVBl.
    I Nr.
    19) geändert worden ist, und
3.  die persönlichen Voraussetzungen für die Berufung in das Beamtenverhältnis erfüllt.

(2) Zur Ausbildung kann auch zugelassen werden, wer die Voraussetzung nach Absatz 1 Nummer 2 erst zum Zeitpunkt der Einstellung in den Vorbereitungsdienst erfüllen wird.

(3) Die Entscheidung über die Zulassung zur Ausbildung trifft die jeweilige Einstellungsbehörde für ihren Bereich.

#### § 6 Einstellung in den Vorbereitungsdienst

(1) Die Einstellungsbehörde entscheidet unter Berücksichtigung des Ergebnisses des Auswahlverfahrens nach § 4 über die Einstellung der Bewerberinnen und Bewerber in den Vorbereitungsdienst.

(2) Vor der Einstellung in den Vorbereitungsdienst müssen folgende Unterlagen bei der Einstellungsbehörde vorliegen:

1.  eine Geburtsurkunde,
2.  ein amtsärztliches Gesundheitszeugnis,
3.  eine Erklärung über etwaige Bestrafungen oder anhängige Ermittlungs- oder Strafverfahren,
4.  ein behördliches Führungszeugnis und
5.  die Einverständniserklärung der gesetzlichen Vertretung zur Einstellung in den Vorbereitungsdienst, falls die Bewerberin oder der Bewerber nicht volljährig ist.

(3) Die ausgewählten Bewerberinnen und Bewerber werden in der Regel zum 1.
September eines Kalenderjahres eingestellt.

#### § 7 Dauer des Vorbereitungsdienstes

(1) Der Vorbereitungsdienst umfasst die Ausbildung und die Prüfung und dauert in der Regel zwei Jahre und sechs Monate.
Er besteht aus fachtheoretischen und berufspraktischen Ausbildungszeiten.

(2) Der Vorbereitungsdienst kann durch Anrechnung von Zeiten verkürzt werden, die die Bewerberin oder der Bewerber bereits bei einem anderen Dienstherrn in einem gleichwertigen Vorbereitungsdienst für den allgemeinen Verwaltungsdienst abgeleistet hat.
Die Entscheidung über die Anrechnung von Zeiten nach Satz 1 trifft die Einstellungsbehörde im Einvernehmen mit der Laufbahnordnungsbehörde (das für ressortübergreifende Aufgaben der Aus- und Fortbildung für die Landesverwaltung zuständige Ministerium).
Erfolgt eine Anrechnung gemäß Satz 1, entscheidet die Einstellungsbehörde über die Verwendung der Anwärterin oder des Anwärters während der aufgrund der Anrechnung freiwerdenden Ausbildungszeiten.

(3) Wird die Ausbildung wegen Krankheit oder aus anderen zwingenden Gründen unterbrochen, können Ausbildungsabschnitte gekürzt oder verlängert und Abweichungen vom Ausbildungsplan zugelassen werden, um eine zielgerichtete Fortsetzung des Vorbereitungsdienstes zu ermöglichen.

(4) Der Vorbereitungsdienst ist im Einzelfall zu verlängern, wenn die Ausbildung

1.  wegen einer länger als sechs Wochen andauernden Krankheit in einem fachtheoretischen Ausbildungsabschnitt,
2.  wegen einer Abwesenheit von mehr als der Hälfte der Zeit in einem berufspraktischen Ausbildungsabschnitt,
3.  wegen eines Beschäftigungsverbots nach § 3 des Mutterschutzgesetzes vom 23. Mai 2017 (BGBl. I S.
    1228), das durch Artikel 57 Absatz 8 des Gesetzes vom 12. Dezember 2019 (BGBl. I S.
    2652) geändert worden ist, in der jeweils geltenden Fassung,
4.  wegen einer Elternzeit nach den §§ 15 und 16 des Bundeselterngeld- und Elternzeitgesetzes in der Fassung der Bekanntmachung vom 27.
    Januar 2015 (BGBl.
    I S.
    33), das zuletzt durch Artikel 36 des Gesetzes vom 12.
    Dezember 2019 (BGBl.
    I S.
    2451) geändert worden, in der jeweils geltenden Fassung oder
5.  aus anderen zwingenden Gründen unterbrochen wurde

und die zielgerichtete Fortsetzung des Vorbereitungsdienstes nicht gewährleistet ist.

(5) Der Vorbereitungsdienst kann in den Fällen des Absatzes 4 Nummer 1 und 4 höchstens zweimal und bis zu insgesamt nicht mehr als 24 Monaten verlängert werden.
Die oder der Betroffene ist vorher anzuhören.
In begründeten Einzelfällen können Ausnahmen von Satz 1 zugelassen werden, wenn die Versagung einer weiteren Verlängerung eine unbillige Härte darstellen würde.

(6) Entscheidungen nach den Absätzen 3 bis 5 trifft die Einstellungsbehörde im Einvernehmen mit der Laufbahnordnungsbehörde.

(7) Bei Nichtbestehen von Prüfungsleistungen oder eines berufspraktischen Ausbildungsabschnittes richtet sich die Verlängerung des Vorbereitungsdienstes nach den §§ 18 und 31.

#### § 8 Rechtsstellung und Pflichten während des Vorbereitungsdienstes, Urlaub

(1) Die im Rahmen des Auswahlverfahrens nach § 4 ausgewählten Bewerberinnen und Bewerber werden unter Berufung in das Beamtenverhältnis auf Widerruf in den Vorbereitungsdienst eingestellt.

(2) Während des Vorbereitungsdienstes unterstehen die ausgewählten Bewerberinnen und Bewerber der Dienstaufsicht der Einstellungsbehörde.

(3) Die ausgewählten Bewerberinnen und Bewerber sind zur Teilnahme an sämtlichen Lehrveranstaltungen und Praktika der Ausbildung sowie an sonstigen von ihrem Dienstherrn bestimmten Veranstaltungen verpflichtet.
Sie sind ferner zur Ablegung der vorgesehenen Leistungstests sowie zu eigenverantwortlichem Selbstlernen verpflichtet.

(4) Erholungsurlaub ist grundsätzlich in den berufspraktischen Ausbildungsabschnitten (§ 15) in Anspruch zu nehmen sofern nicht während der fachtheoretischen Ausbildungszeiten der Erholungsurlaub festgelegt wird.
Er soll vier zusammenhängende Wochen in den jeweiligen berufspraktischen Ausbildungsabschnitten nicht überschreiten.
Prüfungszeiten stehen Lehrveranstaltungszeiten gleich.
Lehrveranstaltungsfreie Tage werden auf den Erholungsurlaub angerechnet.

#### § 9 Beendigung des Beamtenverhältnisses auf Widerruf

(1) Das Beamtenverhältnis auf Widerruf endet mit dem Ablauf des Tages, an dem

1.  das Bestehen der Laufbahnprüfung oder
2.  die Nichtzulassung zur Zwischenprüfung oder
3.  das endgültige Nichtbestehen der Zwischenprüfung oder
4.  das Nichtbestehen von zwei berufspraktischen Abschnitten oder
5.  das endgültige Nichtbestehen eines berufspraktischen Ausbildungsabschnittes oder
6.  die Nichtzulassung zur Abschlussprüfung oder
7.  das endgültige Nichtbestehen der Laufbahnprüfung gemäß § 23

bekannt gegeben worden ist oder

8.  der Vorbereitungsdienst gemäß § 7 Absatz 5 bereits zweimal oder um 24 Monate wegen Krankheit verlängert worden ist.
    

Im Fall von Satz 1 Nummer 1 endet das Beamtenverhältnis jedoch frühestens nach Ablauf der für den Vorbereitungsdienst im Allgemeinen oder im Einzelfall festgesetzten Zeit.

(2) Das Beamtenverhältnis kann jederzeit aus sonstigen wichtigen Gründen beendet werden.

### Abschnitt 2  
Ausbildung

#### § 10 Ziel der Ausbildung

Ziel der Ausbildung ist es, die Befähigung für die Laufbahn des mittleren allgemeinen Verwaltungsdienstes im Land Brandenburg zu vermitteln.

#### § 11 Gliederung der Ausbildung

(1) Die Ausbildung gliedert sich in fachtheoretische und in berufspraktische Ausbildungsabschnitte (Praktika), die in der Regel jeweils insgesamt 15 Monate umfassen.

(2) Fachtheoretische Ausbildungsabschnitte sind:

1.  der Einführungslehrgang,
2.  der Aufbaulehrgang I,
3.  der Aufbaulehrgang II und
4.  der Abschlusslehrgang.

(3) Zwischen den fachtheoretischen Ausbildungsabschnitten sind mindestens drei berufspraktische Ausbildungsabschnitte zu absolvieren.

(4) Fachtheoretische und berufspraktische Ausbildungsabschnitte sind aufeinander abzustimmen.

(5) Die berufspraktischen Ausbildungsabschnitte werden in den von den Einstellungsbehörden bestimmten Behörden und Einrichtungen (Ausbildungsbehörden) absolviert.

(6) Mit dem erfolgreichen Abschluss der Ausbildung wird die Laufbahnbefähigung für den mittleren allgemeinen Verwaltungsdienst im Land Brandenburg erworben.

#### § 12 Grundsätze der fachtheoretischen Ausbildung

(1) Die Lerninhalte der fachtheoretischen Ausbildungsabschnitte sind praxisbezogen sowie handlungs- und anwendungsorientiert auf dem aktuellen Stand des Fachs zu gestalten.
Die Lehrveranstaltungen sollen aktuelle Bezüge zur Verwaltungspraxis im Land Brandenburg aufweisen.

(2) Die fachtheoretische Ausbildung umfasst insgesamt mindestens 1 500 Lehrstunden, die anteilig auf die in § 11 Absatz 2 aufgeführten Ausbildungsabschnitte aufgeteilt werden.

(3) Die inhaltliche Ausgestaltung der fachtheoretischen Ausbildung, insbesondere die Festlegung der Lehrmodule, deren Lerninhalte und deren Stundenzahl, wird in einem Rahmenplan geregelt.

#### § 13 Inhalt der fachtheoretischen Ausbildung

(1) Die Lehrveranstaltungen der fachtheoretischen Ausbildung umfassen Lehrinhalte aus den Bausteinen:

1.  Grundsätze der Ausbildung,
2.  Verwaltungsorganisation und Arbeitsabläufe,
3.  Recht und Rechtsanwendung,
4.  Personalangelegenheiten,
5.  Wirtschaft und Verwaltungswirtschaft,
6.  Wahlpflichtfächer Recht und Sprachen.

(2) Die Bausteine beinhalten die im Rahmenplan aufgeführten Module und Mindeststunden.

#### § 14 Wahlpflichtfächer

(1) Im Einführungslehrgang haben die Anwärterinnen und Anwärter zur Erweiterung ihrer Sprachkenntnisse im Wahlpflichtmodul Sprachen aus mindestens zwei Sprachen eine Sprache zu wählen, die bis zum Abschluss der Ausbildung belegt wird.

(2) Im Abschlusslehrgang haben die Anwärterinnen und Anwärter im Wahlpflichtmodul Recht aus mindestens drei Rechtsgebieten ein Rechtsgebiet zu wählen.
Das Wahlpflichtmodul soll der Vertiefung bereits vorhandener und der Vermittlung neuer Kenntnisse dienen.

(3) Die Wahlmöglichkeit der beiden Wahlpflichtmodule kann durch Vorgaben der jeweiligen Einstellungsbehörde beschränkt werden.

#### § 15 Grundsätze der berufspraktischen Ausbildungsabschnitte

(1) Die berufspraktischen Ausbildungsabschnitte sollen berufliche Kenntnisse und Erfahrungen vermitteln.
Die Anwärterinnen und Anwärter sollen während der Praxisphasen die während der fachtheoretischen Ausbildung erworbenen Kenntnisse vertiefen und insbesondere lernen, diese in der Praxis anzuwenden.
Die praktischen Unterweisungen in den Ausbildungsstellen sollen systematisch und didaktisch effektiv auf die Inhalte der fachtheoretischen Ausbildung abgestimmt sein.

(2) Die Anwärterinnen und Anwärter sollen in den Ausbildungsstellen die wesentlichen Aufgaben ihrer Laufbahn kennenlernen und in typische Arbeitsvorgänge eingeführt werden.
Ihnen ist unter Berücksichtigung des Ausbildungsgegenstandes Gelegenheit zu geben, Vorgänge und Arbeitsaufgaben selbstständig zu bearbeiten.
Dabei sollen sie sich in der schriftlichen und mündlichen Kommunikation üben.
Die bearbeiteten Vorgänge sind zu besprechen und bei der Beurteilung zu berücksichtigen.

#### § 16 Ablauf der berufspraktischen Ausbildungsabschnitte, Ausbildungsstellen

(1) Die berufspraktische Ausbildung gliedert sich in mindestens drei Praktika.

(2) Ausbildungsschwerpunkte sind die Bereiche: Verwaltungsorganisation und Arbeitsabläufe, Recht und Rechtsanwendung, Personalangelegenheiten und Wirtschaft und Verwaltungswirtschaft.
Der Bereich Verwaltungsorganisation und Arbeitsabläufe ist in die anderen Bereiche zu integrieren.
Jeder Ausbildungsschwerpunkt muss absolviert werden.

(3) Die Praktika sollen in verschiedenen Geschäftsbereichen und auf unterschiedlichen Stufen der Landesverwaltung absolviert werden.
Wer von Einstellungsbehörden gemäß § 2 Nummer 2 zur Ausbildung zugelassen wurde, absolviert die Praktika vorrangig in der Einstellungsbehörde.

(4) Die Abordnung zu den Ausbildungsstellen erfolgt durch die Einstellungsbehörde.
Dabei sind Wünsche der Anwärterinnen und Anwärter nach Möglichkeit zu berücksichtigen.

(5) Die praktischen Unterweisungen in den Ausbildungsstellen erfolgen durch Ausbildende, die Beamtinnen oder Beamte des mittleren oder des gehobenen Dienstes oder vergleichbare Tarifbeschäftigte sind und über eine mindestens fünfjährige Berufserfahrung im öffentlichen Dienst verfügen.

(6) Zur Ergänzung der berufspraktischen Ausbildung können praxisbegleitende Arbeitsgemeinschaften eingerichtet werden.
Sie dienen der Ergänzung der praktischen Unterweisungen in den Ausbildungsstellen und der Befähigung der Anwärterinnen und Anwärter, Verwaltungsabläufe in ihrer Gesamtheit zu betrachten sowie rechtsübergreifende, wirtschaftliche und soziale Zusammenhänge zu erkennen, Wege für eine zweckmäßige Verfahrensgestaltung zu finden sowie die organisatorischen, personellen und finanziellen Konsequenzen des Verwaltungshandelns abzuschätzen.

(7) Näheres zur Ausgestaltung der berufspraktischen Ausbildung regelt das für das Recht des öffentlichen Dienstes zuständige Ministerium.

#### § 17 Bewertung der berufspraktischen Ausbildung

(1) Über die Anwärterinnen und Anwärter ist bei Beendigung eines jeden Praktikums von dem jeweiligen Ausbilder eine Beurteilung über die Praxisleistung abzugeben.
Diese muss erkennen lassen, ob das Ziel des jeweiligen Ausbildungsabschnitts erreicht wurde.
Sie ist mit der Anwärterin oder dem Anwärter zu besprechen.
Für die Benotung der Praxisleistung gilt § 20 entsprechend.
Ist zu erwarten, dass die Leistungen in einem Ausbildungsabschnitt nicht mindestens mit ausreichend (Note 4) zu bewerten sind, ist die Anwärterin oder der Anwärter spätestens in der Mitte des Ausbildungsabschnitts darauf hinzuweisen; die Einstellungsbehörde ist zu informieren.

(2) Ein berufspraktischer Ausbildungsabschnitt ist bestanden, wenn er mit mindestens ausreichend (Note 4) bewertet wurde.

(3) Die Beurteilung ist von der Ausbilderin oder dem Ausbilder der Landesakademie für öffentliche Verwaltung zuzuleiten.

(4) Von der Abgabe einer Beurteilung ist abzusehen, wenn die Abwesenheit in der Ausbildungsstelle mehr als die Hälfte der vorgesehenen Praktikumszeit betragen hat.
Die Einstellungsbehörde ist zu unterrichten.
Der berufspraktische Ausbildungsabschnitt gilt dann als nicht durchlaufen.

#### § 18 Wiederholung eines berufspraktischen Ausbildungsabschnitts

Von den berufspraktischen Ausbildungsabschnitten kann nur ein nicht bestandener Abschnitt einmal wiederholt werden.
Der Vorbereitungsdienst verlängert sich um die Dauer des berufspraktischen Ausbildungsabschnittes.

#### § 19 Fachtheoretische Leistungstests

(1) In allen fachtheoretischen Abschnitten sind Leistungstests zu erbringen.
Die Leistungstests sollen die fachliche Qualifikation sowie die schriftliche und mündliche Ausdrucksfähigkeit darstellen.
Die Benotung der Leistungsnachweise soll den Leistungsstand der Anwärterinnen und Anwärter zuverlässig und vergleichbar abbilden.

(2) Die Leistungstests können schriftlich, mündlich oder in praktischer Anwendung durchgeführt werden.

(3) Die Anzahl der in den einzelnen fachtheoretischen Abschnitten zu erbringenden Leistungstests, die Module, in denen diese vorgesehen sind, sowie deren Form und Dauer werden im Rahmenplan der fachtheoretischen Ausbildung festgelegt.

(4) Wird ein Leistungstest aufgrund einer Erkrankung oder sonstiger von den Anwärterinnen beziehungsweise Anwärtern nicht zu vertretender Umstände versäumt, ist der Leistungstest nachzuholen.
§ 33 Absatz 2 Satz 3 bis 5 gilt entsprechend.

(5) Die Bewertung der Leistungstests richtet sich nach § 20.

(6) Die Wiederholung eines Leistungstests in anderen als in Absatz 4 genannten Fällen ist nicht möglich.

#### § 20 Bewertung der Leistungen

(1) Leistungstests und Prüfungen sind mit einer der folgenden Prozentzahlen und der sich daraus ergebenden Note zu bewerten:

Beschreibung

Prozentzahlen

Note

Eine Leistung, die den Anforderungen in besonderem Maße entspricht

100 % bis 92 %

Note 1 – sehr gut

Eine Leistung, die den Anforderungen voll entspricht

unter 92 % bis 81 %

Note 2 – gut

Eine Leistung, die im Allgemeinen den Anforderungen entspricht

unter 81 % bis 67 %

Note 3 – befriedigend

Eine Leistung, die zwar Mängel aufweist, aber im Ganzen den Anforderungen noch entspricht

unter 67 % bis 50 %

Note 4 – ausreichend

Eine Leistung, die den Anforderungen nicht entspricht, jedoch erkennen lässt, dass die notwendigen Grundkenntnisse vorhanden sind und die Mängel in absehbarer Zeit behoben werden können

unter 50 % bis 30 %

Note 5 – mangelhaft

Eine Leistung, die den Anforderungen nicht entspricht und bei der selbst die Grundkenntnisse so lückenhaft sind, dass die Mängel in absehbarer Zeit nicht behoben werden können

unter 30 % bis 0 %

Note 6 – ungenügend

(2) Soweit eine Bewertung der Leistungen nach dem Prozentsystem nicht sachgerecht ist, ist die Bewertung nach Noten vorzunehmen.

(3) Ist ein Notendurchschnitt zu bilden, wird dieser mit einer Nachkommastelle angegeben.
Bei der Ermittlung des Notendurchschnitts ist ab der zweiten Nachkommastelle ab 0,05 aufzurunden und darunter abzurunden.

### Abschnitt 3  
Prüfungen, Laufbahnbefähigung

#### § 21 Prüfungsamt

Für die Organisation und Durchführung der Zwischenprüfung sowie der Abschlussprüfung ist das Staatliche Prüfungsamt für Verwaltungslaufbahnen (Prüfungsamt) zuständig.

#### § 22 Prüfungskommission, Prüfende

(1) Die Laufbahnprüfung wird vor einer Prüfungskommission abgelegt, die beim Prüfungsamt zu bilden ist; sie führt die Bezeichnung ,,Prüfungskommission für die Laufbahn des mittleren allgemeinen Verwaltungsdienstes des Landes Brandenburg“.
Das Prüfungsamt richtet bei Bedarf weitere Prüfungskommissionen ein.

(2) Jede Prüfungskommission ist zu besetzen mit

1.  einer Beamtin oder einem Beamten des höheren Dienstes oder einer oder einem Tarifbeschäftigten mit vergleichbarer Qualifikation als Vorsitzender oder Vorsitzendem sowie
2.  einer Beamtin oder einem Beamten des gehobenen allgemeinen Verwaltungsdienstes oder einer oder einem Tarifbeschäftigten mit vergleichbarer Qualifikation und
3.  einer Lehrkraft.

Es können stellvertretende Mitglieder der Prüfungskommissionen bestellt werden.
Ein Prüfungskommissionsmitglied muss Beamtin oder Beamter sein.

(3) Die Prüfungskommission ist nur beschlussfähig, wenn mindestens drei Mitglieder oder stellvertretende Mitglieder anwesend sind.

(4) Die Mitglieder der Prüfungskommissionen und deren Vertreter und Vertreterinnen werden für die Dauer von fünf Jahren bestellt; die Wiederbestellung ist zulässig.
Scheidet ein Mitglied aus, wird die Nachfolge für die restliche Zeitdauer bestellt.

(5) Die Mitglieder der Prüfungskommission und deren Vertreter und Vertreterinnen sind in dieser Funktion unabhängig und nicht weisungsgebunden.

(6) Die Prüfungskommission entscheidet mit Stimmenmehrheit.
Eine Stimmenthaltung ist nicht zulässig.

(7) Jede schriftliche Aufsichtsarbeit der Zwischen- und Abschlussprüfung wird von zwei Prüfenden bewertet.
Prüfende sind Mitglieder oder stellvertretende Mitglieder der Prüfungskommission.
Das Prüfungsamt legt fest, wer Erstprüferin oder Erstprüfer und wer Zweitprüferin oder Zweitprüfer ist.
Weichen die Bewertungen voneinander ab, ist zunächst eine Einigung zwischen den Prüfenden anzustreben; einigen sich die Prüfenden nicht, entscheidet die Prüfungskommission mit Stimmenmehrheit.

(8) Die Sitzungen der Prüfungskommissionen sind nicht öffentlich.
Beauftragte der Einstellungsbehörden sowie des Staatlichen Prüfungsamtes für Verwaltungslaufbahnen sind berechtigt, bei der mündlichen Abschlussprüfung zugegen zu sein.
Die oder der Vorsitzende der Prüfungskommission kann ferner Personen, bei denen ein dienstliches Interesse vorliegt, gestatten, bei der mündlichen Abschlussprüfung zugegen zu sein.

#### § 23 Laufbahnprüfung

Die Laufbahnprüfung dient dazu, die Eignung und Befähigung der Anwärterinnen und Anwärter für den mittleren allgemeinen Verwaltungsdienst des Landes Brandenburg festzustellen.
Die Laufbahnprüfung besteht aus den folgenden Teilen:

1.  der Zwischenprüfung,
2.  den Leistungstests der fachtheoretischen Ausbildung,
3.  den Bewertungen in der berufspraktischen Ausbildung sowie
4.  der Abschlussprüfung.

#### § 24 Zwischenprüfung

(1) Zum Abschluss des Einführungslehrgangs haben die Anwärterinnen und Anwärter in einer Zwischenprüfung nachzuweisen, dass sie einen Wissens- und Kenntnisstand erreicht haben, der eine erfolgreiche weitere Ausbildung erwarten lässt.

(2) Zur Zwischenprüfung werden nur Anwärterinnen und Anwärter zugelassen, die an allen Leistungstests des Einführungslehrgangs teilgenommen haben und einen Notendurchschnitt von mindestens der Note 4,0 erreicht haben.

(3) Die Zwischenprüfung besteht aus vier schriftlichen Aufsichtsarbeiten in den Modulen

1.  Organisation und Geschäftsverkehr,
2.  Verwaltungsrecht,
3.  Arbeits- und Tarifrecht sowie Beamtenrecht,
4.  Haushaltswesen, Buchführung und Kassenverwaltung.

Näheres wird durch eine Verwaltungsvorschrift geregelt.

(4) Hat eine Anwärterin oder ein Anwärter die schriftliche Aufsichtsarbeit nicht oder nicht rechtzeitig abgegeben, gilt diese als mit ungenügend (Note 6) bewertet.

(5) Die Zwischenprüfung ist bestanden, wenn

1.  mindestens drei schriftliche Aufsichtsarbeiten mit mindestens ausreichend (Note 4) bewertet wurden und
2.  keine der schriftlichen Aufsichtsarbeiten mit ungenügend (Note 6) bewertet wurde und
3.  in allen vier schriftlichen Aufsichtsarbeiten mindestens eine Durchschnittsnote von 4,0 erreicht wurde.

(6) Wer die Zwischenprüfung bestanden hat, erhält vom Prüfungsamt ein Zeugnis über die erbrachten Leistungen.
Wer die Zwischenprüfung endgültig nicht bestanden hat, erhält vom Prüfungsamt einen Bescheid über die nicht bestandene Zwischenprüfung und eine Bescheinigung über die erbrachten Prüfungsleistungen.

#### § 25 Abschlussprüfung

(1) Zur Abschlussprüfung werden nur Anwärterinnen und Anwärter zugelassen, die die Zwischenprüfung bestanden, die fachtheoretischen Ausbildungsabschnitte durchlaufen und in allen Leistungstests bis einschließlich Aufbaulehrgang II mindestens einen Notendurchschnitt von 4,0 erreicht sowie alle berufspraktischen Ausbildungsabschnitte bestanden haben.

(2) Die Abschlussprüfung besteht aus einem schriftlichen und einem mündlichen Prüfungsteil.

(3) Die Abschlussprüfung ist bestanden, wenn die in Absatz 2 genannten Prüfungsteile bestanden sind.

#### § 26 Schriftlicher Prüfungsteil

(1) Der schriftliche Prüfungsteil der Abschlussprüfung besteht aus

1.  einer schriftlichen Aufsichtsarbeit im Baustein Recht und Rechtsanwendung mit den Modulen Staats- und Verfassungsrecht sowie Europarecht,
2.  einer schriftlichen Aufsichtsarbeit im Baustein Recht und Rechtsanwendung mit den Modulen Verwaltungsrecht, Polizei- und Ordnungsrecht, Sozialrecht sowie Kommunalrecht,
3.  einer schriftlichen Aufsichtsarbeit im Baustein Personalangelegenheiten mit den Modulen Arbeitsrecht und Tarifrecht, Beamtenrecht sowie Personalwesen,
4.  einer schriftlichen Aufsichtsarbeit im Baustein Wirtschaft und Verwaltungswirtschaft mit allen darin enthaltenen Modulen.

Näheres wird durch eine Verwaltungsvorschrift geregelt.

(2) Hat eine Anwärterin oder ein Anwärter die schriftliche Aufsichtsarbeit nicht oder nicht rechtzeitig abgegeben, gilt diese als mit ungenügend (Note 6) bewertet.

(3) Der schriftliche Prüfungsteil ist bestanden, wenn

1.  mindestens drei schriftliche Aufsichtsarbeiten mit mindestens ausreichend (Note 4) bewertet wurden und
2.  keine der schriftlichen Aufsichtsarbeiten mit ungenügend (Note 6) bewertet wurde und
3.  in allen vier schriftlichen Aufsichtsarbeiten mindestens eine Durchschnittsnote von 4,0 erreicht wurde.

#### § 27 Mündlicher Prüfungsteil

(1) Voraussetzung für die Teilnahme am mündlichen Prüfungsteil der Abschlussprüfung ist die Teilnahme am schriftlichen Prüfungsteil.

(2) Im mündlichen Prüfungsteil haben die Anwärterinnen und Anwärter die in den Bausteinen

1.  Verwaltungsorganisation und Arbeitsabläufe,
2.  Recht und Rechtsanwendung

erworbenen Kenntnisse nachzuweisen.
Näheres wird durch eine Verwaltungsvorschrift geregelt.

(3) Die Prüfungskommission bewertet den mündlichen Prüfungsteil mit einer Note gemäß § 20.

(4) Der mündliche Prüfungsteil ist bestanden, wenn dieser mindestens mit ausreichend (Note 4) bewertet wurde.

#### § 28 Gesamtergebnis der Laufbahnprüfung

(1) Das Gesamtergebnis der Laufbahnprüfung wird von der oder dem Vorsitzenden der Prüfungskommission im Anschluss an den mündlichen Prüfungsteil ermittelt.
Es wird aus den Durchschnittsnoten der Zwischenprüfung, der fachtheoretischen Ausbildung, der berufspraktischen Ausbildung, der schriftlichen Aufsichtsarbeiten des schriftlichen Prüfungsteils sowie der Note des mündlichen Prüfungsteils der Abschlussprüfung errechnet; Näheres wird durch eine Verwaltungsvorschrift geregelt.

(2) Die oder der Vorsitzende der Prüfungskommission teilt den Anwärterinnen und Anwärtern das Gesamtergebnis mit.

(3) Über die Feststellung des Gesamtergebnisses ist eine Niederschrift zu fertigen.
Sie ist von den Mitgliedern der Prüfungskommission zu unterzeichnen und dem Prüfungsamt unverzüglich vorzulegen.

#### § 29 Bestehen der Laufbahnprüfung

Die Laufbahnprüfung ist bestanden, wenn die Abschlussprüfung bestanden ist und im Gesamtergebnis gemäß § 28 ein Notendurchschnitt von mindestens 4,0 erreicht wurde.

#### § 30 Abschlusszeugnis

(1) Wer die Laufbahnprüfung bestanden hat, erhält ein Abschlusszeugnis.

(2) Wer die Laufbahnprüfung endgültig nicht bestanden hat, erhält vom Prüfungsamt einen Bescheid über die nicht bestandene Laufbahnprüfung und eine Bescheinigung über die erbrachten Ausbildungsleistungen.

#### § 31 Wiederholung der Prüfung

(1) Jede mit schlechter als ausreichend (Note 4) bewertete schriftliche Aufsichtsarbeit der Zwischen- und Abschlussprüfung oder die mündliche Abschlussprüfung darf innerhalb einer vom Prüfungsamt festzusetzenden Frist einmal wiederholt werden.
Der Wiederholungswunsch ist innerhalb von zwei Wochen nach Bekanntgabe der Prüfungsergebnisse dem Prüfungsamt schriftlich mitzuteilen.
Das Ergebnis der wiederholten Prüfungsleistung ersetzt das Ergebnis der vorangegangenen Prüfungsleistung.

(2) Die Laufbahnausbildung verlängert sich bei Abschlussprüfungen um die Dauer der Wiederholung der Abschlussprüfung, längstens um sechs Monate.

(3) Die Laufbahnordnungsbehörde kann in begründeten Ausnahmefällen eine zweite Wiederholung der in Absatz 1 Satz 1 genannten Prüfungen zulassen, wenn ihre Versagung eine unbillige Härte darstellen würde.
Der Zulassungsantrag ist bei der Einstellungsbehörde zu stellen, die den Antrag der Laufbahnordnungsbehörde zuzuleiten hat; die Einstellungsbehörde kann zu dem Antrag der Anwärterin oder des Anwärters schriftlich Stellung nehmen.

#### § 32 Prüfungserleichterungen

(1) Schwerbehinderten und gleichgestellten behinderten Anwärterinnen und Anwärtern gemäß § 2 Absatz 2 und 3 des Neunten Buchs Sozialgesetzbuch vom 23.
Dezember 2016 (BGBl.
I S.
3234), das zuletzt durch Artikel 8 des Gesetzes vom 14.
Dezember 2019 (BGBl.
I S.
2789) geändert worden ist, in der jeweils geltenden Fassung, sind bei der Zwischen- und Abschlussprüfung auf Antrag die ihrer Behinderung angemessenen Erleichterungen zu gewähren.
Die fachlichen Anforderungen dürfen nicht herabgesetzt werden.

(2) Anwärterinnen und Anwärter, die vorübergehend erheblich körperlich beeinträchtigt sind, können bei der Zwischen- und Abschlussprüfung auf Antrag angemessene Erleichterungen gewährt werden.
Absatz 1 Satz 2 gilt entsprechend.

(3) Anträge auf Prüfungserleichterungen sind spätestens einen Monat vor Beginn der Zwischen- und Abschlussprüfung beim Prüfungsamt einzureichen.
Liegen die Voraussetzungen für die Gewährung einer Prüfungserleichterung erst zu einem späteren Zeitpunkt vor, ist der Antrag unverzüglich zu stellen.
Der Nachweis der Prüfungsbehinderung ist durch ein ärztliches Attest zu führen, das Angaben über Art und Form der notwendigen Prüfungserleichterungen enthalten muss.
Das Prüfungsamt kann bestimmen, dass ein amtsärztliches Zeugnis beizubringen ist.

#### § 33 Fernbleiben, Rücktritt

(1) Bleibt eine Anwärterin oder ein Anwärter einer Prüfung oder Teilen derselben ohne Zustimmung des Prüfungsamtes fern oder tritt sie oder er ohne Zustimmung des Prüfungsamtes von der Prüfung oder einem Teil von ihr zurück, wird die Prüfung oder der betreffende Teil mit ungenügend (Note 6) bewertet.

(2) Stimmt das Prüfungsamt dem Fernbleiben oder dem Rücktritt zu, gilt die Prüfung oder der betreffende Teil als nicht durchgeführt.
Die Zustimmung darf nur erteilt werden, wenn wichtige Gründe vorliegen, insbesondere, wenn die Anwärterin oder der Anwärter aufgrund von Krankheit an der Prüfung oder einem Prüfungsteil nicht teilnehmen kann.
Die Anwärterin oder der Anwärter hat das Vorliegen eines wichtigen Grundes unverzüglich gegenüber dem Prüfungsamt geltend zu machen und nachzuweisen.
Im Krankheitsfall ist ein ärztliches Zeugnis vorzulegen, aus dem sich die Prüfungsunfähigkeit ergibt und das in der Regel nicht später als am Prüfungstag ausgestellt sein darf.
Das Prüfungsamt kann bestimmen, dass ein amtsärztliches Zeugnis beizubringen ist.

(3) Der Krankheit einer Anwärterin oder eines Anwärters steht die Krankheit eines von ihr oder ihm zu versorgenden Kindes oder die Pflege einer oder eines nahen Angehörigen in einer kurzfristig auftretenden Pflegesituation gleich.
In offensichtlichen Fällen kann auf die Vorlage eines ärztlichen Zeugnisses verzichtet werden.

(4) Haben sich Anwärterinnen oder Anwärter in Kenntnis einer gesundheitlichen Beeinträchtigung oder eines anderen Rücktrittsgrundes einer Prüfung unterzogen, kann ein nachträglicher Rücktritt von der bezeichneten Prüfung wegen dieses Grundes nicht mehr genehmigt werden.

(5) Für Anwärterinnen und Anwärter, die mit Zustimmung des Prüfungsamtes einer Prüfung oder Teilen derselben ferngeblieben oder davon zurückgetreten sind, bestimmt das Prüfungsamt eine Nachprüfung.
Bereits abgelegte Teile der Prüfung werden bei der Nachprüfung angerechnet.
Eine nicht oder nicht vollständig abgelegte mündliche Prüfung ist in vollem Umfang nachzuholen.

#### § 34 Unlauteres Verhalten im Prüfungsverfahren

(1) Unternimmt es eine Anwärterin oder ein Anwärter, das Ergebnis einer Prüfung durch Täuschung, Mitführung oder Benutzung nicht zugelassener Hilfsmittel, unzulässige Hilfe Dritter oder durch Einwirkung auf das Prüfungsamt oder auf von diesem mit der Wahrnehmung von Prüfungsangelegenheiten beauftragte Personen zu beeinflussen, wird die betroffene Prüfung mit ungenügend (Note 6) bewertet.
Entsprechendes gilt, wenn sie oder er den ordnungsgemäßen Verlauf einer Prüfung stört.
In besonders schweren Fällen können Anwärterinnen oder Anwärter von der weiteren Teilnahme an der Laufbahnausbildung ausgeschlossen werden.
Für den mündlichen Prüfungsteil gelten die Sätze 1 und 2 entsprechend.

(2) Entscheidungen nach Absatz 1 Satz 1 und 2 trifft das Prüfungsamt; die Anwärterin oder der Anwärter ist vorher anzuhören.
Bis zur Entscheidung des Prüfungsamtes setzt die Anwärterin oder der Anwärter die Prüfung fort, es sei denn, dass nach der Entscheidung der oder des Aufsichtführenden ein vorläufiger Ausschluss zur ordnungsgemäßen Weiterführung der Prüfung unerlässlich ist.
Entscheidungen nach Absatz 1 Satz 3 trifft die Einstellungsbehörde nach vorheriger Anhörung der Anwärterin oder des Anwärters.

(3) Wird eine Täuschung erst nach Beendigung einer Prüfung oder eines Prüfungsteils festgestellt, sind die Absätze 1 und 2 entsprechend anzuwenden.
Wird eine Täuschung erst nach Abschluss der Laufbahnprüfung festgestellt, kann das Prüfungsamt die Laufbahnprüfung innerhalb von fünf Jahren nach dem Tag der mündlichen Abschlussprüfung für nicht bestanden erklären.

#### § 35 Prüfungsakten, Einsichtnahme

(1) Zu den Prüfungsakten zu nehmen sind:

1.  eine Ausfertigung des Zeugnisses über die Zwischenprüfung oder des Bescheids über die nicht bestandene Zwischenprüfung (§ 24),
2.  die schriftlichen Aufsichtsarbeiten der Zwischenprüfung und der Abschlussprüfung,
3.  die Anlage zur Niederschrift der Laufbahnprüfung und
4.  eine Ausfertigung des Abschlusszeugnisses oder des Bescheids über die nicht bestandene Laufbahnprüfung (§ 30).

Die Prüfungsakten werden nach Beendigung des Vorbereitungsdienstes mindestens fünf Jahre beim Prüfungsamt aufbewahrt.
Sie sind spätestens zehn Jahre nach Beendigung des Vorbereitungsdienstes zu vernichten.
Das Abschlusszeugnis wird nach Beendigung des Vorbereitungsdienstes mindestens 40 Jahre aufbewahrt.

(2) Nach Zustellung des Zwischenprüfungs- oder Abschlusszeugnisses können die Betroffenen auf Antrag Einsicht in die sie betreffenden Prüfungsakten nehmen.
Satz 1 gilt entsprechend, wenn die Zwischen- oder Abschlussprüfung nicht bestanden wurde.

#### § 36 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 20.
Mai 2020

Der Minister des Innern und für Kommunales

Michael Stübgen