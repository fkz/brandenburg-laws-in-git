## Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Umsatzsteuer für die Haushaltsjahre 2022 und 2023 (Umsatzsteueraufteilverordnung 2022 und 2023 - UStAV 2022 und 2023)

Auf Grund des § 5a Absatz 3 Satz 3 und des § 5d Absatz 2 und 3 in Verbindung mit § 4 Absatz 2 und des § 8 des Gemeindefinanzreformgesetzes in der Fassung der Bekanntmachung vom 10. März 2009 (BGBl. I S. 502), von denen § 5a Absatz 3 Satz 3 und § 5d Absatz 2 und 3 zuletzt durch Artikel 3 des Gesetzes vom 21.
November 2016 (BGBl. I S. 2613) geändert worden sind, in Verbindung mit § 1 der Gemeindefinanzen-Ermächtigungsübertragungsverordnung vom 9.
Juli 2019 (GVBl.
II Nr. 53) und der Bekanntmachung der Geschäftsbereiche der obersten Landesbehörden vom 7.
Mai 2020 (GVBl. II Nr. 34) verordnet die Ministerin der Finanzen und für Europa:

#### § 1 Verteilungsschlüssel für den Gemeindeanteil an der Umsatzsteuer

(1) Der auf die Gemeinden im Land Brandenburg entfallende Gemeindeanteil an der Umsatzsteuer wird für die Haushaltsjahre 2022 und 2023 nach dem in der Anlage festgesetzten Schlüssel aufgeteilt.

(2) In Fällen kommunaler Neugliederung nach dem 31.
Dezember 2019 ist nach § 5 der Umsatzsteuerschlüsselzahlenfestsetzungsverordnung vom 21.
September 2020 (BGBl. I S. 2018) zu verfahren.
Dabei sind bei der Neufestsetzung die Schlüsselzahlen der betroffenen Gemeinden den neu- oder umgebildeten Gemeinden im Verhältnis der in sie aufgenommenen Einwohner und Einwohnerinnen zuzurechnen.
Maßgebend ist die fortgeschriebene und zum Zeitpunkt der Festsetzung veröffentlichte Bevölkerungszahl der amtlichen Statistik, die durch das Amt für Statistik Berlin-Brandenburg zum 31. Dezember des vorvergangenen Jahres festgestellt wird.

#### § 2 Berichtigung von Fehlern

(1) Ausgleichsbeträge nach § 5d Absatz 3 in Verbindung mit § 4 Absatz 1 des Gemeindefinanzreformgesetzes werden nach den Anteilen der einzelnen Gemeinden an dem nach § 5a Absatz 2 des Gemeindefinanzreformgesetzes auf die Gemeinden entfallenden Steueraufkommen errechnet, um die die in der Anlage genannten Anteile zu hoch oder zu niedrig festgesetzt sind.

(2) Der Ausgleich ist mit der jeweiligen Schlussabrechnung vorzunehmen.
Ein Ausgleich unterbleibt, wenn der auszugleichende Betrag 500 Euro nicht übersteigt.

#### § 3 Berechnung, Anweisung und Auszahlung

(1) Der Gemeindeanteil an der Umsatzsteuer nach § 1 ist vom Amt für Statistik Berlin-Brandenburg zu berechnen.

(2) Das für Finanzen zuständige Ministerium stellt die anzuweisenden Beträge fest und regelt die Auszahlung an die Gemeinden.

(3) Auf den Gemeindeanteil an der Umsatzsteuer sind an die Gemeinden für die jeweiligen Haushaltsjahre vierteljährliche Abschlagszahlungen zu folgenden Terminen anzuweisen:

1.  für das 1.
    Quartal jeweils bis zum Ablauf des 16.
    April,
2.  für das 2.
    Quartal jeweils bis zum Ablauf des 16.
    Juli,
3.  für das 3.
    Quartal jeweils bis zum Ablauf des 16.
    Oktober,
4.  für das 4.
    Quartal jeweils bis zum Ablauf des 1.
    Dezember des laufenden Jahres,
5.  für die Schlussrechnung jeweils bis zum Ablauf des 4.
    Februar des Folgejahres.

Den Abschlagszahlungen ist jeweils das vierteljährliche Aufkommen des Gemeindeanteils an der Umsatzsteuer zugrunde zu legen.
Die Abschlagszahlung für das jeweils vierte Quartal ist in Höhe der Abschlagszahlung für das jeweils dritte Quartal zu leisten.

#### § 4 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2021 in Kraft.
Gleichzeitig tritt die Umsatzsteueraufteilverordnung vom 9.
Februar 2015 (GVBl.
II Nr. 8) außer Kraft.

Potsdam, den 25.
Januar 2021

Die Ministerin der Finanzen und für Europa

Katrin Lange

* * *

### Anlagen

1

[Anlage (zu § 1 Absatz 1, § 2 Absatz 1) Schlüsselzahlen für den Gemeindeanteil an der Umsatzsteuer für die Haushaltsjahre 2022 und 2023](/br2/sixcms/media.php/68/UStAV2021%20-%20Anlage.pdf "Anlage (zu § 1 Absatz 1, § 2 Absatz 1) Schlüsselzahlen für den Gemeindeanteil an der Umsatzsteuer für die Haushaltsjahre 2022 und 2023") 157.5 KB