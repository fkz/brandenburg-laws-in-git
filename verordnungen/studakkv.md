## Verordnung zur Regelung der Studienakkreditierung (Studienakkreditierungsverordnung - StudAkkV)

Auf Grund der §§ 1 und 2 des Gesetzes zu dem Staatsvertrag über die Organisation eines gemeinsamen Akkreditierungssystems zur Qualitätssicherung in Studium und Lehre an deutschen Hochschulen vom 14.
Dezember 2017 (GVBl.
I Nr. 32) in Verbindung mit Artikel 4 und Artikel 16 Absatz 2 des Studienakkreditierungsstaatsvertrages verordnet die Ministerin für Wissenschaft, Forschung und Kultur:

**Inhaltsübersicht**

### Teil 1  
Allgemeine Vorschriften

[§ 1 Anwendungsbereich](#1)

[§ 2 Formen der Akkreditierung](#2)

### Teil 2  
Formale Kriterien für Studiengänge

[§ 3 Studienstruktur und Studiendauer](#3)

[§ 4 Studiengangsprofile](#4)

[§ 5 Zugangsvoraussetzungen](#5)

[§ 6 Abschlüsse und Abschlussbezeichnungen](#6)

[§ 7 Modularisierung](#7)

[§ 8 Leistungspunktesystem](#8)

[§ 9 Besondere Kriterien für Kooperationen mit nichthochschulischen Einrichtungen](#9)

[§ 10 Sonderregelungen für Joint-Degree-Programme](#10)

### Teil 3  
Fachlich-inhaltliche Kriterien für Studiengänge und Qualitätsmanagementsysteme

[§ 11 Qualifikationsziele und Abschlussniveau](#11)

[§ 12 Schlüssiges Studiengangskonzept und adäquate Umsetzung](#12)

[§ 13 Fachlich-inhaltliche Gestaltung der Studiengänge](#13)

[§ 14 Studienerfolg](#14)

[§ 15 Geschlechtergerechtigkeit und Nachteilsausgleich](#15)

[§ 16 Sonderregelungen für Joint-Degree-Programme](#16)

[§ 17 Konzept des Qualitätsmanagementsystems (Ziele, Prozesse, Instrumente)](#17)

[§ 18 Maßnahmen zur Umsetzung des Qualitätsmanagementkonzepts](#18)

[§ 19 Kooperationen mit nichthochschulischen Einrichtungen](#19)

[§ 20 Hochschulische Kooperationen](#20)

[§ 21 Besondere Kriterien für Bachelorausbildungsgänge an Berufsakademien](#21)

### Teil 4  
Verfahrensregeln für die Programm- und Systemakkreditierung

[§ 22 Entscheidung des Akkreditierungsrates; Verleihung des Siegels](#22)

[§ 23 Vorzulegende Unterlagen](#23)

[§ 24 Beauftragung einer Agentur; Akkreditierungsgutachten; Begehung](#24)

[§ 25 Zusammensetzung des Gutachtergremiums; Anforderungen an die Gutachterinnen und Gutachter](#25)

[§ 26 Geltungszeitraum der Akkreditierung; Verlängerung](#26)

[§ 27 Auflagen](#27)

[§ 28 Anzeigepflicht bei Änderungen](#28)

[§ 29 Veröffentlichung](#29)

[§ 30 Bündelakkreditierung; Teil-Systemakkreditierung](#30)

[§ 31 Stichproben](#31)

### Teil 5  
Verfahrensregeln für besondere Studiengangsformen

[§ 32 Kombinationsstudiengänge](#32)

[§ 33 Joint-Degree-Programme](#33)

### Teil 6  
Alternative Akkreditierungsverfahren nach Artikel 3 Absatz 1 Nummer 3 Studienakkreditierungsstaatsvertrag

[§ 34 Alternative Akkreditierungsverfahren](#34)

### Teil 7  
Schlussbestimmungen

[§ 35 Verbindung mit Verfahren, die die berufszulassungsrechtliche Eignung eines Studiengangs zum Gegenstand haben](#35)

[§ 36 Evaluation](#36)

[§ 37 Inkrafttreten](#37)

### Teil 1  
Allgemeine Vorschriften

#### § 1 Anwendungsbereich

(1) Diese Verordnung regelt das Nähere zu den formalen Kriterien nach Artikel 2 Absatz 2, zu den fachlich-inhaltlichen Kriterien nach Artikel 2 Absatz 3 sowie zum Verfahren nach Artikel 3 des Studienakkreditierungsstaatsvertrages.

(2) Soweit in dieser Verordnung keine besonderen Bestimmungen getroffen werden, gelten die nachfolgenden Regelungen der Programmakkreditierung auch für Ausbildungsgänge an staatlichen und staatlich anerkannten Berufsakademien, die zu der Abschlussbezeichnung Bachelor führen.
Ein auf der Grundlage dieser Verordnung akkreditierter Bachelorabschluss steht hochschulrechtlich dem Bachelorabschluss einer Hochschule gleich.

#### § 2 Formen der Akkreditierung

Formen der Akkreditierung sind die Verfahren nach Artikel 3 Absatz 1 Nummer 1 des Studienakkreditierungsstaatsvertrages (Systemakkreditierung), nach Artikel 3 Absatz 1 Nummer 2 (Programmakkreditierung) oder alternative Akkreditierungsverfahren nach Artikel 3 Absatz 1 Nummer 3.

### Teil 2  
Formale Kriterien für Studiengänge

#### § 3 Studienstruktur und Studiendauer

(1) Im System gestufter Studiengänge ist der Bachelorabschluss der erste berufsqualifizierende Regelabschluss eines Hochschulstudiums; der Masterabschluss stellt einen weiteren berufsqualifizierenden Hochschulabschluss dar.
Grundständige Studiengänge, die unmittelbar zu einem Masterabschluss führen, sind mit Ausnahme der in Absatz 3 genannten Studiengänge ausgeschlossen.

(2) Die Regelstudienzeiten für ein Vollzeitstudium betragen sechs, sieben oder acht Semester bei den Bachelorstudiengängen und vier, drei oder zwei Semester bei den Masterstudiengängen.
Im Bachelorstudium beträgt die Regelstudienzeit im Vollzeitstudium mindestens drei Jahre.
Bei konsekutiven Studiengängen beträgt die Gesamtregelstudienzeit im Vollzeitstudium fünf Jahre (zehn Semester).
Nach § 18 Absatz 4 des Brandenburgischen Hochschulgesetzes vom 28. April 2014 (GVBl. I Nr. 18), das zuletzt durch Artikel 2 des Gesetzes vom 5.
Juni 2019 (GVBl.
I Nr.
20) geändert worden ist, sind längere Regelstudienzeiten bei entsprechender studienorganisatorischer Gestaltung ausnahmsweise möglich, um den Studierenden eine individuelle Lernbiografie, insbesondere durch Teilzeit-, Fern- oder berufsbegleitendes Studium, zu ermöglichen.
Abweichend von Satz 3 können in künstlerischen Kernfächern an der Filmuniversität Babelsberg _KONRAD WOLF_ auf Antrag der Hochschule gemäß § 18 Absatz 3 Satz 4 des Brandenburgischen Hochschulgesetzes konsekutive Bachelor- und Masterstudiengänge mit einer Gesamtregelstudienzeit von bis zu sechs Jahren eingerichtet werden.
Bei Fachhochschulstudiengängen, die zu einem Diplomgrad führen, beträgt die Regelstudienzeit gemäß § 18 Absatz 3 Satz 5 des Brandenburgischen Hochschulgesetzes höchstens vier Jahre, bei anderen Studiengängen, die zu einem Diplom- oder Magistergrad führen, höchstens vierundeinhalb Jahre.

(3) Theologische Studiengänge, die für das Pfarramt, das Priesteramt und den Beruf der Pastoralreferentin oder des Pastoralreferenten qualifizieren („Theologisches Vollstudium“), müssen nicht gestuft sein und können eine Regelstudienzeit von zehn Semestern aufweisen.

#### § 4 Studiengangsprofile

(1) Masterstudiengänge können in „anwendungsorientierte“ und „forschungsorientierte“ unterschieden werden.
Masterstudiengänge an Kunst- und Musikhochschulen können ein besonderes künstlerisches Profil haben.
Masterstudiengänge, in denen die Bildungsvoraussetzungen für ein Lehramt vermittelt werden, haben ein besonderes lehramtsbezogenes Profil.
Das jeweilige Profil ist in der Akkreditierung festzustellen.

(2) Bei der Einrichtung eines Masterstudiengangs ist festzulegen, ob er konsekutiv oder weiterbildend ist.
Weiterbildende Masterstudiengänge entsprechen in den Vorgaben zur Regelstudienzeit und zur Abschlussarbeit den konsekutiven Masterstudiengängen und führen zu dem gleichen Qualifikationsniveau und zu denselben Berechtigungen.

(3) Bachelor- und Masterstudiengänge beinhalten eine Abschlussarbeit, mit der die Fähigkeit nachgewiesen wird, innerhalb einer vorgegebenen Frist ein Problem aus dem jeweiligen Fach selbständig nach wissenschaftlichen beziehungsweise künstlerischen Methoden zu bearbeiten.

#### § 5 Zugangsvoraussetzungen

(1) Zugangsvoraussetzung für einen Masterstudiengang ist ein erster berufsqualifizierender Hochschulabschluss.
In künstlerischen und besonderen weiterbildenden Masterstudiengängen kann gemäß § 9 Absatz 5 Satz 4 des Brandenburgischen Hochschulgesetzes an die Stelle des berufsqualifizierenden Hochschulabschlusses eine Eingangsprüfung treten, bei der die Bewerberin oder der Bewerber Kenntnisse und Fähigkeiten nachweist, die einem geeigneten berufsqualifizierenden Hochschulabschluss entsprechen.
Weiterbildende Masterstudiengänge setzen qualifizierte berufspraktische Erfahrung von in der Regel nicht unter einem Jahr voraus.

(2) Für den Zugang zu Masterstudiengängen können die Hochschulen gemäß § 9 Absatz 5 Satz 2 des Brandenburgischen Hochschulgesetzes durch Satzung weitere Voraussetzungen vorsehen.

#### § 6 Abschlüsse und Abschlussbezeichnungen

(1) Nach einem erfolgreich abgeschlossenen Bachelor- oder Masterstudiengang wird jeweils nur ein Grad, der Bachelor- oder Mastergrad, verliehen, es sei denn, es handelt sich um einen Multiple-Degree-Abschluss.
Dabei findet keine Differenzierung der Abschlussgrade nach der Dauer der Regelstudienzeit statt.

(2) Für Bachelor- und konsekutive Mastergrade sind folgende Bezeichnungen zu verwenden:

1.  Bachelor of Arts (B.A.) und Master of Arts (M.A.) in den Fächergruppen Sprach- und Kulturwissenschaften, Sport, Sportwissenschaft, Sozialwissenschaften, Kunstwissenschaft, Darstellende Kunst und bei entsprechender inhaltlicher Ausrichtung in der Fächergruppe Wirtschaftswissenschaften sowie in künstlerisch angewandten Studiengängen,
2.  Bachelor of Science (B.Sc.) und Master of Science (M.Sc.) in den Fächergruppen Mathematik, Naturwissenschaften, Medizin, Agrar-, Forst- und Ernährungswissenschaften, in den Fächergruppen Ingenieurwissenschaften und Wirtschaftswissenschaften bei entsprechender inhaltlicher Ausrichtung,
3.  Bachelor of Engineering (B.Eng.) und Master of Engineering (M.Eng.) in der Fächergruppe Ingenieurwissenschaften bei entsprechender inhaltlicher Ausrichtung,
4.  Bachelor of Laws (LL.B.) und Master of Laws (LL.M.) in der Fächergruppe Rechtswissenschaften,
5.  Bachelor of Fine Arts (B.F.A.) und Master of Fine Arts (M.F.A.) in der Fächergruppe Freie Kunst,
6.  Bachelor of Music (B.Mus.) und Master of Music (M.Mus.) in der Fächergruppe Musik,
7.  Bachelor of Education (B.Ed.) und Master of Education (M.Ed.) für Studiengänge, in denen die Bildungsvoraussetzungen für ein Lehramt vermittelt werden.
    Für einen polyvalenten Studiengang kann entsprechend dem inhaltlichen Schwerpunkt des Studiengangs eine Bezeichnung nach den Nummern 1 bis 7 vorgesehen werden.

Fachliche Zusätze zu den Abschlussbezeichnungen und gemischtsprachige Abschlussbezeichnungen sind ausgeschlossen.
Bachelorgrade mit dem Zusatz „honours“ („B.A.hon.“) sind ausgeschlossen.
Bei interdisziplinären und Kombinationsstudiengängen richtet sich die Abschlussbezeichnung nach demjenigen Fachgebiet, dessen Bedeutung im Studiengang überwiegt.
Für Weiterbildungsstudiengänge dürfen auch Mastergrade verwendet werden, die von den vorgenannten Bezeichnungen abweichen.
Für theologische Studiengänge, die für das Pfarramt, das Priesteramt und den Beruf der Pastoralreferentin oder des Pastoralreferenten qualifizieren („Theologisches Vollstudium“), können auch abweichende Bezeichnungen verwendet werden.

(3) In den Abschlussdokumenten darf an geeigneter Stelle verdeutlicht werden, dass das Qualifikationsniveau des Bachelorabschlusses einem Diplomabschluss an Fachhochschulen beziehungsweise das Qualifikationsniveau eines Masterabschlusses einem Diplomabschluss an Universitäten oder gleichgestellten Hochschulen entspricht.

(4) Auskunft über das dem Abschluss zugrunde liegende Studium im Einzelnen erteilt das Diploma Supplement, das Bestandteil jedes Abschlusszeugnisses ist.

#### § 7 Modularisierung

(1) Die Studiengänge sind in Studieneinheiten (Module) zu gliedern, die durch die Zusammenfassung von Studieninhalten thematisch und zeitlich abgegrenzt sind.
Die Inhalte eines Moduls sind so zu bemessen, dass sie in der Regel innerhalb von maximal zwei aufeinander folgenden Semestern vermittelt werden können; in besonders begründeten Ausnahmefällen kann sich ein Modul auch über mehr als zwei Semester erstrecken.
Für das künstlerische Kernfach im Bachelorstudium sind mindestens zwei Module verpflichtend, die etwa zwei Drittel der Arbeitszeit in Anspruch nehmen können.

(2) Die Beschreibung eines Moduls soll mindestens enthalten:

1.  Inhalte und Qualifikationsziele des Moduls,
2.  Lehr- und Lernformen,
3.  Voraussetzungen für die Teilnahme,
4.  Verwendbarkeit des Moduls,
5.  Voraussetzungen für die Vergabe von ECTS-Leistungspunkten entsprechend dem European Credit Transfer System (ECTS-Leistungspunkte),
6.  ECTS-Leistungspunkte und Benotung,
7.  Häufigkeit des Angebots des Moduls,
8.  Arbeitsaufwand und
9.  Dauer des Moduls.

(3) Unter den Voraussetzungen für die Teilnahme sind die Kenntnisse, Fähigkeiten und Fertigkeiten für eine erfolgreiche Teilnahme und Hinweise für die geeignete Vorbereitung durch die Studierenden zu benennen.
Im Rahmen der Verwendbarkeit des Moduls ist darzustellen, welcher Zusammenhang mit anderen Modulen desselben Studiengangs besteht und inwieweit es zum Einsatz in anderen Studiengängen geeignet ist.
Bei den Voraussetzungen für die Vergabe von ECTS-Leistungspunkten ist anzugeben, wie ein Modul erfolgreich absolviert werden kann (Prüfungsart, -umfang, -dauer).

#### § 8 Leistungspunktesystem

(1) Jedem Modul ist in Abhängigkeit vom Arbeitsaufwand für die Studierenden eine bestimmte Anzahl von ECTS-Leistungspunkten zuzuordnen.
Je Semester sind in der Regel 30 ECTS-Leistungspunkte zu Grunde zu legen.
Ein Leistungspunkt entspricht einer Gesamtarbeitsleistung der Studierenden im Präsenz- und Selbststudium von 25 bis höchstens 30 Zeitstunden.
Für ein Modul werden ECTS-Leistungspunkte gewährt, wenn die in der Prüfungsordnung vorgesehenen Leistungen nachgewiesen werden.
Die Vergabe von ECTS-Leistungspunkten setzt nicht zwingend eine Prüfung, sondern den erfolgreichen Abschluss des jeweiligen Moduls voraus.

(2) Für den Bachelorabschluss sind mindestens 180 ECTS-Leistungspunkte nachzuweisen.
Für den Masterabschluss werden unter Einbeziehung des vorangehenden Studiums bis zum ersten berufsqualifizierenden Abschluss 300 ECTS-Leistungspunkte benötigt.
Davon kann bei entsprechender Qualifikation der Studierenden im Einzelfall abgewichen werden, auch wenn nach Abschluss eines Masterstudiengangs 300 ECTS-Leistungspunkte nicht erreicht werden.
Bei konsekutiven Bachelor- und Masterstudiengängen in den künstlerischen Kernfächern an Kunst- und Musikhochschulen mit einer Gesamtregelstudienzeit von sechs Jahren wird das Masterniveau mit 360 ECTS-Leistungspunkten erreicht.

(3) Der Bearbeitungsumfang beträgt für die Bachelorarbeit 6 bis 12 ECTS-Leistungspunkte und für die Masterarbeit 15 bis 30 ECTS-Leistungspunkte.
In Studiengängen der Freien Kunst kann in begründeten Ausnahmefällen der Bearbeitungsumfang für die Bachelorarbeit bis zu 20 ECTS-Leistungspunkte und für die Masterarbeit bis zu 40 ECTS-Leistungspunkte betragen.

(4) In begründeten Ausnahmefällen können für Studiengänge mit besonderen studienorganisatorischen Maßnahmen bis zu 75 ECTS-Leistungspunkte pro Studienjahr zugrunde gelegt werden.
Dabei ist die Arbeitsbelastung eines ECTS-Leistungspunktes mit 30 Stunden bemessen.
Besondere studienorganisatorische Maßnahmen können insbesondere Lernumfeld und Betreuung, Studienstruktur, Studienplanung und Maßnahmen zur Sicherung des Lebensunterhalts betreffen.

(5) Bei Lehramtsstudiengängen für Lehrämter der Grundschule oder Primarstufe, für übergreifende Lehrämter der Primarstufe und aller oder einzelner Schularten der Sekundarstufe, für Lehrämter für alle oder einzelne Schularten der Sekundarstufe I sowie für Sonderpädagogische Lehrämter I kann ein Masterabschluss vergeben werden, wenn nach mindestens 240 an der Hochschule erworbenen ECTS-Leistungspunkten unter Einbeziehung des Vorbereitungsdienstes insgesamt 300 ECTS-Leistungspunkte erreicht sind.

(6) An Berufsakademien sind bei einer dreijährigen Ausbildungsdauer für den Bachelorabschluss in der Regel 180 ECTS-Leistungspunkte nachzuweisen.
Der Umfang der theoriebasierten Ausbildungsanteile darf 120 ECTS-Leistungspunkte, der Umfang der praxisbasierten Ausbildungsanteile 30 ECTS-Leistungspunkte nicht unterschreiten.

#### § 9 Besondere Kriterien für Kooperationen mit nichthochschulischen Einrichtungen

(1) Umfang und Art bestehender Kooperationen mit Unternehmen und sonstigen Einrichtungen sind unter Einbezug nichthochschulischer Lernorte und Studienanteile sowie der Unterrichtssprachen vertraglich geregelt und auf der Internetseite der Hochschule beschrieben.
Bei der Anwendung von Anrechnungsmodellen im Rahmen von studiengangsbezogenen Kooperationen ist die inhaltliche Gleichwertigkeit anzurechnender nichthochschulischer Qualifikationen und deren Äquivalenz gemäß dem angestrebten Qualifikationsniveau nachvollziehbar dargelegt.

(2) Im Fall von studiengangsbezogenen Kooperationen mit nichthochschulischen Einrichtungen ist der Mehrwert für die künftigen Studierenden und die gradverleihende Hochschule nachvollziehbar dargelegt.

#### § 10 Sonderregelungen für Joint-Degree-Programme

(1) Ein Joint-Degree-Programm ist ein gestufter Studiengang, der von einer inländischen Hochschule gemeinsam mit einer oder mehreren Hochschulen ausländischer Staaten aus dem Europäischen Hochschulraum koordiniert und angeboten wird, zu einem gemeinsamen Abschluss führt und folgende weitere Merkmale aufweist:

1.  Integriertes Curriculum,
2.  Studienanteil an einer oder mehreren ausländischen Hochschulen von in der Regel mindestens 25 Prozent,
3.  vertraglich geregelte Zusammenarbeit,
4.  abgestimmtes Zugangs- und Prüfungswesen und
5.  eine gemeinsame Qualitätssicherung.

(2) Qualifikationen und Studienzeiten werden in Übereinstimmung mit dem Gesetz zu dem Übereinkommen vom 11.
April 1997 über die Anerkennung von Qualifikationen im Hochschulbereich in der europäischen Region vom 16.
Mai 2007 (BGBl.
2007 II S. 712) (Lissabon-Konvention) anerkannt.
Das ECTS wird entsprechend den §§ 7 und 8 Absatz 1 angewendet und die Verteilung der Leistungspunkte ist geregelt.
Für den Bachelorabschluss sind 180 bis 240 Leistungspunkte nachzuweisen und für den Masterabschluss nicht weniger als 60 Leistungspunkte.
Die wesentlichen Studieninformationen sind veröffentlicht und für die Studierenden jederzeit zugänglich.

(3) Wird ein Joint Degree-Programm von einer inländischen Hochschule gemeinsam mit einer oder mehreren Hochschulen ausländischer Staaten koordiniert und angeboten, die nicht dem Europäischen Hochschulraum angehören (außereuropäische Kooperationspartner), so finden auf Antrag der inländischen Hochschule die Absätze 1 und 2 entsprechende Anwendung, wenn sich die außereuropäischen Kooperationspartner in der Kooperationsvereinbarung mit der inländischen Hochschule zu einer Akkreditierung unter Anwendung der in den Absätzen 1 und 2 sowie in § 16 Absatz 1 und § 33 Absatz 1 geregelten Kriterien und Verfahrensregeln verpflichtet.

### Teil 3  
Fachlich-inhaltliche Kriterien für Studiengänge und Qualitätsmanagementsysteme

#### § 11 Qualifikationsziele und Abschlussniveau

(1) Die Qualifikationsziele und die angestrebten Lernergebnisse sind klar formuliert und tragen den in Artikel 2 Absatz 3 Nummer 1 des Studienakkreditierungsstaatsvertrages genannten Zielen von Hochschulbildung nachvollziehbar Rechnung.
Die Dimension Persönlichkeitsbildung umfasst auch die künftige zivilgesellschaftliche, politische und kulturelle Rolle der Absolventinnen und Absolventen.
Die Studierenden sollen nach ihrem Abschluss in der Lage sein, gesellschaftliche Prozesse kritisch, reflektiert sowie mit Verantwortungsbewusstsein und in demokratischem Gemeinsinn maßgeblich mitzugestalten.

(2) Die fachlichen und wissenschaftlichen/künstlerischen Anforderungen umfassen die Aspekte Wissen und Verstehen (Wissensverbreiterung, Wissensvertiefung und Wissensverständnis), Einsatz, Anwendung und Erzeugung von Wissen/Kunst (Nutzung und Transfer, wissenschaftliche Innovation), Kommunikation und Kooperation sowie wissenschaftliches/künstlerisches Selbstverständnis/Professionalität und sind stimmig im Hinblick auf das vermittelte Abschlussniveau.

(3) Bachelorstudiengänge dienen der Vermittlung wissenschaftlicher Grundlagen, Methodenkompetenz und berufsfeldbezogener Qualifikationen und stellen eine breite wissenschaftliche Qualifizierung sicher.
Konsekutive Masterstudiengänge sind als vertiefende, verbreiternde, fachübergreifende oder fachlich andere Studiengänge ausgestaltet.
Weiterbildende Masterstudiengänge setzen qualifizierte berufspraktische Erfahrung von in der Regel nicht unter einem Jahr voraus.
Das Studiengangskonzept weiterbildender Masterstudiengänge berücksichtigt die beruflichen Erfahrungen und knüpft zur Erreichung der Qualifikationsziele an diese an.
Bei der Konzeption legt die Hochschule den Zusammenhang von beruflicher Qualifikation und Studienangebot sowie die Gleichwertigkeit der Anforderungen zu konsekutiven Masterstudiengängen dar.
Künstlerische Studiengänge fördern die Fähigkeit zur künstlerischen Gestaltung und entwickeln diese fort.

#### § 12 Schlüssiges Studiengangskonzept und adäquate Umsetzung

(1) Das Curriculum ist unter Berücksichtigung der festgelegten Eingangsqualifikation und im Hinblick auf die Erreichbarkeit der Qualifikationsziele adäquat aufgebaut.
Die Qualifikationsziele, die Studiengangsbezeichnung, Abschlussgrad und -bezeichnung und das Modulkonzept sind stimmig aufeinander bezogen.
Das Studiengangskonzept umfasst vielfältige, an die jeweilige Fachkultur und das Studienformat angepasste Lehr- und Lernformen sowie gegebenenfalls Praxisanteile.
Es schafft geeignete Rahmenbedingungen zur Förderung der studentischen Mobilität, die den Studierenden einen Aufenthalt an anderen Hochschulen ohne Zeitverlust ermöglichen.
Es bezieht die Studierenden aktiv in die Gestaltung von Lehr- und Lernprozessen ein (studierendenzentriertes Lehren und Lernen) und eröffnet Freiräume für ein selbstgestaltetes Studium.

(2) Das Curriculum wird durch ausreichendes fachlich und methodisch-didaktisch qualifiziertes Lehrpersonal umgesetzt.
Die Verbindung von Forschung und Lehre wird entsprechend dem Profil der Hochschulart insbesondere durch hauptberuflich tätige Professorinnen und Professoren sowohl in grundständigen als auch weiterführenden Studiengängen gewährleistet.
Die Hochschule ergreift geeignete Maßnahmen der Personalauswahl und -qualifizierung.

(3) Der Studiengang verfügt darüber hinaus über eine angemessene Ressourcenausstattung (insbesondere nicht-wissenschaftliches Personal, Raum- und Sachausstattung einschließlich IT-Infrastruktur, Lehr- und Lernmittel).

(4) Prüfungen und Prüfungsarten ermöglichen eine aussagekräftige Überprüfung der erreichten Lernergebnisse.
Sie sind modulbezogen und kompetenzorientiert.

(5) Die Studierbarkeit in der Regelstudienzeit ist gewährleistet.
Dies umfasst insbesondere

1.  einen planbaren und verlässlichen Studienbetrieb,
2.  die weitgehende Überschneidungsfreiheit von Lehrveranstaltungen und Prüfungen,
3.  einen plausiblen und der Prüfungsbelastung angemessenen durchschnittlichen Arbeitsaufwand, wobei die Lernergebnisse eines Moduls so zu bemessen sind, dass sie in der Regel innerhalb eines Semesters oder eines Jahres erreicht werden können, was in regelmäßigen Erhebungen validiert wird, und
4.  eine adäquate und belastungsangemessene Prüfungsdichte und -organisation, wobei in der Regel für ein Modul nur eine Prüfung vorgesehen wird und Module mindestens einen Umfang von fünf ECTS-Leistungspunkten aufweisen sollen.

(6) Studiengänge mit besonderem Profilanspruch weisen ein in sich geschlossenes Studiengangskonzept aus, das die besonderen Charakteristika des Profils angemessen darstellt.

#### § 13 Fachlich-inhaltliche Gestaltung der Studiengänge

(1) Die Aktualität und Adäquanz der fachlichen und wissenschaftlichen Anforderungen ist gewährleistet.
Die fachlich-inhaltliche Gestaltung und die methodisch-didaktischen Ansätze des Curriculums werden kontinuierlich überprüft und an fachliche und didaktische Weiterentwicklungen angepasst.
Dazu erfolgt eine systematische Berücksichtigung des fachlichen Diskurses auf nationaler und gegebenenfalls internationaler Ebene.

(2) In Studiengängen, in denen die Bildungsvoraussetzungen für ein Lehramt vermittelt werden, sind Grundlage der Akkreditierung sowohl die Bewertung der Bildungswissenschaften und Fachwissenschaften sowie deren Didaktik nach ländergemeinsamen und länderspezifischen fachlichen Anforderungen als auch die ländergemeinsamen und länderspezifischen strukturellen Vorgaben für die Lehrerausbildung.

(3) Im Rahmen der Akkreditierung von Lehramtsstudiengängen ist insbesondere zu prüfen, ob

1.  ein integratives Studium an Universitäten oder gleichgestellten Hochschulen von mindestens zwei Fachwissenschaften und von Bildungswissenschaften in der Bachelorphase sowie in der Masterphase (Ausnahmen sind bei den Fächern Kunst und Musik zulässig),
2.  schulpraktische Studien bereits während des Bachelorstudiums und
3.  eine Differenzierung des Studiums und der Abschlüsse nach Lehrämtern erfolgt sind.

Ausnahmen beim Lehramt für die beruflichen Schulen sind zulässig.

#### § 14 Studienerfolg

Der Studiengang unterliegt unter Beteiligung von Studierenden und Absolventinnen und Absolventen einem kontinuierlichen Monitoring.
Auf dieser Grundlage werden Maßnahmen zur Sicherung des Studienerfolgs abgeleitet.
Diese werden fortlaufend überprüft und die Ergebnisse für die Weiterentwicklung des Studiengangs genutzt.
Die Beteiligten werden über die Ergebnisse und die ergriffenen Maßnahmen unter Beachtung datenschutzrechtlicher Belange informiert.

#### § 15 Geschlechtergerechtigkeit und Nachteilsausgleich

Die Hochschule verfügt über Konzepte zur Geschlechtergerechtigkeit und zur Förderung der Chancengleichheit von Studierenden in besonderen Lebenslagen, die auf der Ebene des Studiengangs umgesetzt werden.

#### § 16 Sonderregelungen für Joint-Degree-Programme

(1) Für Joint-Degree-Programme finden die Regelungen in § 11 Absatz 1 und 2, § 12 Absatz 1 Satz 1 bis 3, Absatz 2 Satz 1, Absatz 3 und 4 sowie § 14 entsprechend Anwendung.
Daneben gilt:

1.  Die Zugangsanforderungen und Auswahlverfahren sind der Niveaustufe und der Fachdisziplin, in der der Studiengang angesiedelt ist, angemessen.
2.  Es kann nachgewiesen werden, dass mit dem Studiengang die angestrebten Lernergebnisse erreicht werden.
3.  Soweit einschlägig, sind die Vorgaben der Richtlinie 2005/36/EG des Europäischen Parlaments und des Rates vom 7.
    September 2005 über die Anerkennung von Berufsqualifikationen (ABl. L 255 vom 30.9.2005, S.
    22; L 271 vom 16.10.2007, S. 18; L 93 vom 4.4.2008, S.
    28; L 33 vom 3.2.2009, S.
    49; L 305 vom 24.10.2014, S.
    115), die zuletzt durch die Richtlinie 2013/55/EU (ABl.
    L 354 vom 28.12.2013, S.
    132; L 268 vom 15.10.2015, S. 35; L 95 vom 9.4.2016, S. 20) geändert worden ist, berücksichtigt.
4.  Bei der Betreuung, der Gestaltung des Studiengangs und den angewendeten Lehr- und Lernformen werden die Vielfalt der Studierenden und ihrer Bedürfnisse respektiert und die spezifischen Anforderungen mobiler Studierender berücksichtigt.
5.  Das Qualitätsmanagementsystem der Hochschule gewährleistet die Umsetzung der vorstehenden und der in § 17 genannten Maßgaben.

(2) Wird ein Joint-Degree-Programm von einer inländischen Hochschule gemeinsam mit einer oder mehreren Hochschulen ausländischer Staaten koordiniert und angeboten, die nicht dem Europäischen Hochschulraum angehören (außereuropäische Kooperationspartner), so findet auf Antrag der inländischen Hochschule Absatz 1 entsprechende Anwendung, wenn sich die außereuropäischen Kooperationspartner in der Kooperationsvereinbarung mit der inländischen Hochschule zu einer Akkreditierung unter Anwendung der in Absatz 1 sowie der in § 10 Absatz 1 und 2 und § 33 Absatz 1 geregelten Kriterien und Verfahrensregeln verpflichtet.

#### § 17 Konzept des Qualitätsmanagementsystems (Ziele, Prozesse, Instrumente)

(1) Die Hochschule verfügt über ein Leitbild für die Lehre, das sich in den Curricula ihrer Studiengänge widerspiegelt.
Das Qualitätsmanagementsystem folgt den Werten und Normen des Leitbildes für die Lehre und zielt darauf ab, die Studienqualität kontinuierlich zu verbessern.
Es gewährleistet die systematische Umsetzung der in Teil 2 und 3 genannten Maßgaben.
Die Hochschule hat Entscheidungsprozesse, Zuständigkeiten und Verantwortlichkeiten für die Einrichtung, Überprüfung, Weiterentwicklung und Einstellung von Studiengängen und die hochschuleigenen Verfahren zur Akkreditierung von Studiengängen im Rahmen ihres Qualitätsmanagementsystems festgelegt und hochschulweit veröffentlicht.

(2) Das Qualitätsmanagementsystem wurde unter Beteiligung der Mitgliedsgruppen der Hochschule und unter Einbeziehung externen Sachverstands erstellt.
Es stellt die Unabhängigkeit von Qualitätsbewertungen sicher und enthält Verfahren zum Umgang mit hochschulinternen Konflikten sowie ein internes Beschwerdesystem.
Es beruht auf geschlossenen Regelkreisen, umfasst alle Leistungsbereiche der Hochschule, die für Studium und Lehre unmittelbar relevant sind und verfügt über eine angemessene und nachhaltige Ressourcenausstattung.
Funktionsfähigkeit und Wirksamkeit mit Bezug auf die Studienqualität werden von der Hochschule regelmäßig überprüft und kontinuierlich weiterentwickelt.

#### § 18 Maßnahmen zur Umsetzung des Qualitätsmanagementkonzepts

(1) Das Qualitätsmanagementsystem beinhaltet regelmäßige Bewertungen der Studiengänge und der für Lehre und Studium relevanten Leistungsbereiche durch interne und externe Studierende, hochschulexterne wissenschaftliche Expertinnen und Experten, Vertreterinnen und Vertreter der Berufspraxis, Absolventinnen und Absolventen.
Zeigt sich dabei Handlungsbedarf, werden die erforderlichen Maßnahmen ergriffen und umgesetzt.

(2) Sofern auf der Grundlage des Qualitätsmanagementsystems der Hochschule auch Bewertungen von Lehramtsstudiengängen, Lehramtsstudiengängen mit dem Kombinationsfach Evangelische oder Katholische Theologie/Religion, evangelisch-theologischen Studiengängen, die für das Pfarramt qualifizieren, und anderen Bachelor- und Masterstudiengängen mit dem Kombinationsfach Evangelische oder Katholische Theologie vorgenommen werden, gelten die Mitwirkungs- und Zustimmungserfordernisse gemäß § 25 Absatz 1 Satz 3 bis 5 entsprechend.

(3) Die für die Umsetzung des Qualitätsmanagementsystems erforderlichen Daten werden hochschulweit und regelmäßig erhoben.

(4) Die Hochschule dokumentiert die Bewertung der Studiengänge des hochschulinternen Qualitätsmanagementsystems unter Einschluss der Voten der externen Beteiligten und informiert Hochschulmitglieder, Öffentlichkeit, Träger und das für die Hochschulen zuständige Mitglied der Landesregierung regelmäßig über die ergriffenen Maßnahmen.
Sie informiert die Öffentlichkeit über die auf der Grundlage des hochschulinternen Verfahrens erfolgten Akkreditierungsentscheidungen und stellt dem Akkreditierungsrat die zur Veröffentlichung nach § 29 erforderlichen Informationen zur Verfügung.

#### § 19 Kooperationen mit nichthochschulischen Einrichtungen

Führt eine Hochschule einen Studiengang in Kooperation mit einer nichthochschulischen Einrichtung durch, ist die Hochschule für die Einhaltung der Maßgaben gemäß der Teile 2 und 3 verantwortlich.
Die gradverleihende Hochschule darf Entscheidungen über Inhalt und Organisation des Curriculums, über Zulassung, Anerkennung und Anrechnung, über die Aufgabenstellung und Bewertung von Prüfungsleistungen, über die Verwaltung von Prüfungs- und Studierendendaten, über die Verfahren der Qualitätssicherung sowie über Kriterien und Verfahren der Auswahl des Lehrpersonals nicht delegieren.

#### § 20 Hochschulische Kooperationen

(1) Führt eine Hochschule eine studiengangsbezogene Kooperation mit einer anderen Hochschule durch, gewährleistet die gradverleihende Hochschule beziehungsweise gewährleisten die gradverleihenden Hochschulen die Umsetzung und die Qualität des Studiengangskonzeptes.
Art und Umfang der Kooperation sind beschrieben und die der Kooperation zu Grunde liegenden Vereinbarungen dokumentiert.

(2) Führt eine systemakkreditierte Hochschule eine studiengangsbezogene Kooperation mit einer anderen Hochschule durch, kann die systemakkreditierte Hochschule dem Studiengang das Siegel des Akkreditierungsrates gemäß § 22 Absatz 4 Satz 2 verleihen, sofern sie selbst gradverleihend ist und die Umsetzung und die Qualität des Studiengangskonzeptes gewährleistet.
Absatz 1 Satz 2 gilt entsprechend.

(3) Im Fall der Kooperation von Hochschulen auf der Ebene ihrer Qualitätsmanagementsysteme ist eine Systemakkreditierung jeder der beteiligten Hochschulen erforderlich.
Auf Antrag der kooperierenden Hochschulen ist ein gemeinsames Verfahren der Systemakkreditierung zulässig.

#### § 21 Besondere Kriterien für Bachelorausbildungsgänge an Berufsakademien

(1) Die hauptberuflichen Lehrkräfte an Berufsakademien und diejenigen Lehrkräfte an Berufsakademien, die zur Vergabe von Leistungspunkten im Sinne von § 24 Absatz 3 des Brandenburgischen Hochschulgesetzes führende Lehrveranstaltungen anbieten oder als Prüfer an der Ausgabe oder Bewertung der Bachelorarbeit mitwirken, müssen gemäß § 87 Absatz 3 Nummer 5 des Brandenburgischen Hochschulgesetzes die für Professorinnen und Professoren geltenden Einstellungsvoraussetzungen an Fachhochschulen gemäß § 41 des Brandenburgischen Hochschulgesetzes erfüllen oder einen geeigneten Hochschulabschluss und eine in der Regel mindestens fünfjährige einschlägige Berufserfahrung nachweisen können.
Soweit Lehrangebote überwiegend der Vermittlung praktischer Fertigkeiten und Kenntnisse dienen, für die nicht die Einstellungsvoraussetzungen für Professorinnen oder Professoren an Fachhochschulen erforderlich sind, können diese akademischen Mitarbeiterinnen oder Mitarbeitern entsprechend den Regelungen nach § 87 Absatz 3 Nummer 5 und § 49 des Brandenburgischen Hochschulgesetzes übertragen werden.
Der Anteil der Lehre, der von hauptberuflichen Lehrkräften erbracht wird, soll 40 Prozent nicht unterschreiten.
Im Ausnahmefall gehören dazu auch Professorinnen oder Professoren an Fachhochschulen oder Universitäten, die in Nebentätigkeit an einer Berufsakademie lehren, wenn auch durch sie die Kontinuität im Lehrangebot und die Konsistenz der Gesamtausbildung sowie verpflichtend die Betreuung und Beratung der Studierenden gewährleistet sind; das Vorliegen dieser Voraussetzungen ist im Rahmen der Akkreditierung des einzelnen Studiengangs gesondert festzustellen.

(2) Absatz 1 Satz 1 gilt entsprechend für nebenberufliche Lehrkräfte, die theoriebasierte, zu ECTS-Leistungspunkten führende Lehrveranstaltungen anbieten oder die als Prüferinnen oder Prüfer an der Ausgabe und Bewertung der Bachelorarbeit mitwirken.
Soweit es der Eigenart des Faches und den Anforderungen der Stelle entspricht, können Lehrveranstaltungen nach Satz 1 ausnahmsweise auch von nebenberuflichen Lehrkräften angeboten werden, die über einen fachlich einschlägigen Hochschulabschluss oder einen gleichwertigen Abschluss sowie über eine fachwissenschaftliche und didaktische Befähigung sowie über eine mehrjährige fachlich einschlägige Berufserfahrung entsprechend den Anforderungen an die Lehrveranstaltung verfügen.

(3) Im Rahmen der Akkreditierung ist auch zu überprüfen:

1.  das Zusammenwirken der unterschiedlichen Lernorte (Studienakademie und Betrieb),
2.  die Sicherung von Qualität und Kontinuität im Lehrangebot und in der Betreuung und Beratung der Studierenden vor dem Hintergrund der besonderen Personalstruktur an Berufsakademien und
3.  das Bestehen eines nachhaltigen Qualitätsmanagementsystems, das die unterschiedlichen Lernorte umfasst.

### Teil 4  
Verfahrensregeln für die Programm- und Systemakkreditierung

#### § 22 Entscheidung des Akkreditierungsrates; Verleihung des Siegels

(1) Der Akkreditierungsrat entscheidet auf Antrag der Hochschule über die Akkreditierung durch die Feststellung der Einhaltung der formalen Kriterien und der fachlich-inhaltlichen Kriterien gemäß Artikel 3 Absatz 5 Satz 1 des Studienakkreditierungsstaatsvertrages in Verbindung mit Teil 2 und Teil 3 dieser Rechtsverordnung.
Grundlage für die Entscheidung über die formalen Kriterien ist ein Prüfbericht gemäß Artikel 4 Absatz 3 Satz 1 Nummer 2 Buchstabe b des Studienakkreditierungsstaatsvertrages.
Grundlage für die Entscheidung über die fachlich-inhaltlichen Kriterien ist ein Gutachten gemäß Artikel 3 Absatz 2 Satz 1 Nummer 4 des Studienakkreditierungsstaatsvertrages.

(2) Die Entscheidung ergeht durch schriftlichen Bescheid.
Sie ist zu begründen.

(3) Die Hochschule erhält vor der Entscheidung des Akkreditierungsrates Gelegenheit zur Stellungnahme, wenn er von der Empfehlung der Gutachterinnen und Gutachter in erheblichem Umfang abzuweichen beabsichtigt.
Die Frist zur Stellungnahme beträgt einen Monat.

(4) Mit der Akkreditierung verleiht der Akkreditierungsrat dem Studiengang oder dem Qualitätsmanagementsystem sein Siegel.
Bei einer Systemakkreditierung erhält die Hochschule das Recht, das Siegel des Akkreditierungsrates für die von ihr geprüften Studiengänge selbst zu verleihen.

(5) Die Akkreditierung von katholisch-theologischen Studiengängen, die für das Priesteramt und den Beruf der Pastoralreferentin oder des Pastoralreferenten qualifizieren („Theologisches Vollstudium“), erfolgt ausschließlich in Form der Programmakkreditierung.
Die Entscheidung des Akkreditierungsrates bedarf in volltheologischen und teiltheologischen Studiengängen der Zustimmung der zuständigen kirchlichen Stellen.

#### § 23 Vorzulegende Unterlagen

(1) Dem Antrag sind folgende Unterlagen beizufügen:

1.  Selbstbericht der Hochschule,
2.  ein Akkreditierungsbericht einer beim Akkreditierungsrat zugelassenen Agentur, der aus einem Prüfbericht und einem Gutachten besteht; im Fall der Systemakkreditierung bezieht sich der Prüfbericht auf die Nachweise gemäß Nummer 3 und 4,
3.  bei Antrag auf Systemakkreditierung zusätzlich der Nachweis, dass mindestens ein Studiengang das Qualitätsmanagementsystem durchlaufen hat,
4.  bei Antrag auf Systemreakkreditierung der Nachweis, dass grundsätzlich alle Bachelor- und Masterstudiengänge das Qualitätsmanagementsystem mindestens einmal durchlaufen haben.

(2) Von den Unterlagen nach Absatz 1 Nummer 2 sind, soweit sie nicht in deutscher Sprache verfasst sind, Übersetzungen in deutscher Sprache vorzulegen.

(3) Sobald der Akkreditierungsrat ein elektronisches Datenverarbeitungssystem zur Verfügung stellt, ist dieses zu nutzen.

#### § 24 Beauftragung einer Agentur; Akkreditierungsgutachten; Begehung

(1) Die Hochschule beauftragt eine beim Akkreditierungsrat gemäß Artikel 5 Absatz 3 Satz 1 Nummer 5 des Studienakkreditierungsstaatsvertrages zugelassene Agentur mit der Begutachtung der formalen und fachlich-inhaltlichen Kriterien und der Erstellung eines Akkreditierungsberichts.
Für katholisch-theologische Studiengänge, die für das Priesteramt und den Beruf der Pastoralreferentin oder des Pastoralreferenten qualifizieren („Theologisches Vollstudium“), erfolgt die Begutachtung durch die Agentur für Qualitätssicherung und Akkreditierung kanonischer Studiengänge in Deutschland, die durch den Akkreditierungsrat zugelassen ist.

(2) Die Hochschule stellt der Agentur einen Selbstbericht zur Verfügung, der mindestens Angaben zu den Qualitätszielen der Hochschule und zu den formalen und fachlich-inhaltlichen Kriterien nach Teil 2 und Teil 3 enthält.
Der Selbstbericht der Hochschule, an dessen Erstellung die Studierendenvertretung zu beteiligen ist, soll für die Programmakkreditierung 20 Seiten und für die System- und Bündelakkreditierung 50 Seiten nicht überschreiten.

(3) Der Prüfbericht wird von der Agentur erstellt; bei Studiengängen nach § 25 Absatz 1 Satz 3 und 4 bedarf der Prüfbericht der Zustimmung der dort jeweils benannten Personen.
Maßgebliche Standards für den Prüfbericht sind die formalen Kriterien nach Teil 2.
Er enthält einen Vorschlag zur Feststellung der Einhaltung der formalen Kriterien.
Der Prüfbericht ist in dem durch den Akkreditierungsrat vorzugebenden Raster abzufassen.
Über die Nichterfüllung eines formalen Kriteriums ist die Hochschule unverzüglich zu informieren.

(4) Das Gutachten wird vom Gutachtergremium nach § 25 abgegeben.
Das Gutachtergremium erhält den Prüfbericht nach Absatz 3.
Maßgebliche Standards für das Gutachten sind die fachlich-inhaltlichen Kriterien nach Teil 3.
Es enthält einen Vorschlag zur Feststellung der Einhaltung der fachlich-inhaltlichen Kriterien.
Das Gutachten ist in dem durch den Akkreditierungsrat vorzugebenden Raster abzufassen und soll für die Programmakkreditierung 20 Seiten und für die System- und Bündelakkreditierung 100 Seiten nicht überschreiten.

(5) Im Rahmen der Begutachtung der fachlich-inhaltlichen Kriterien findet eine Begehung durch das Gutachtergremium statt.
Bei der Akkreditierung eines Studiengangs, der zum Zeitpunkt der Beauftragung der Agentur noch nicht angeboten wird (Konzeptakkreditierung), kann das Gutachtergremium einvernehmlich auf eine Begehung verzichten.
Gleiches gilt bei der Reakkreditierung eines Studiengangs.

#### § 25 Zusammensetzung des Gutachtergremiums; Anforderungen an die Gutachterinnen und Gutachter

(1) Dem Gutachtergremium der Agenturen gehören bei einer Programmakkreditierung mindestens vier Personen an.
Es setzt sich wie folgt zusammen:

1.  mindestens zwei fachlich nahestehende Hochschullehrerinnen oder Hochschullehrer,
2.  eine fachlich nahestehende Vertreterin oder ein fachlich nahestehender Vertreter aus der beruflichen Praxis,
3.  eine fachlich nahestehende Studierende oder ein fachlich nahestehender Studierender.

Bei der Akkreditierung von Studiengängen, die die Befähigung für die Aufnahme in den Vorbereitungsdienst für ein Lehramt vermitteln, tritt eine Vertreterin oder ein Vertreter der für das Schulwesen zuständigen Obersten Landesbehörde an die Stelle der Person nach Satz 2 Nummer 2; bei Lehramtsstudiengängen mit dem Kombinationsfach Evangelische oder Katholische Theologie/Religion tritt zusätzlich eine Vertreterin oder ein Vertreter der örtlich zuständigen Diözese oder Landeskirche hinzu.
Bei der Akkreditierung von theologischen Studiengängen, die für das Pfarramt, das Priesteramt und den Beruf der Pastoralreferentin oder des Pastoralreferenten qualifizieren („Theologisches Vollstudium“) und in allen anderen Bachelor- und Masterstudiengängen mit dem Kombinationsfach Evangelische oder Katholische Theologie/Religion tritt an die Stelle der Person nach Satz 2 Nummer 2 eine Vertreterin oder ein Vertreter der zuständigen kirchlichen Stelle.
Für die in den Sätzen 3 und 4 genannten Studiengänge bedarf die Abgabe des Gutachtens gemäß § 24 Absatz 4 Satz 1 der Zustimmung der jeweils genannten Personen; ohne diese Zustimmung erfolgt keine Vorlage des Gutachtens an den Akkreditierungsrat.

(2) Dem Gutachtergremium der Agenturen gehören bei einer Systemakkreditierung mindestens fünf Personen an.
Es setzt sich wie folgt zusammen:

1.  mindestens drei Hochschullehrerinnen oder Hochschullehrer mit einschlägiger Erfahrung in der Qualitätssicherung im Bereich Lehre,
2.  eine Vertreterin oder ein Vertreter aus der beruflichen Praxis,
3.  eine Studierende oder ein Studierender.

(3) Die Hochschullehrerinnen und Hochschullehrer verfügen über die Mehrheit der Stimmen.
In dem jeweiligen Gutachtergremium muss die Mehrzahl der Gutachterinnen oder Gutachter über Erfahrungen mit Akkreditierungen verfügen.
Bei einer Systemakkreditierung muss die Mehrzahl der Gutachterinnen und Gutachter über Erfahrungen mit Systemakkreditierungen verfügen.

(4) Die Gutachterinnen und Gutachter werden von der mit der Erstellung des Akkreditierungsberichts beauftragten Agentur benannt.
Die Agentur ist bei der Bestellung an das von der Hochschulrektorenkonferenz zu entwickelnde Verfahren gemäß Artikel 3 Absatz 3 Satz 3 des Studienakkreditierungsstaatsvertrages gebunden.

(5) Als Gutachterin und Gutachter ist ausgeschlossen, wer

1.  an der Hochschule, die den Antrag auf Akkreditierung stellt, tätig oder eingeschrieben ist,
2.  bei Kooperationsstudiengängen oder Joint-Degree-Programmen an einer der an dem Studiengang beteiligten Hochschulen tätig oder eingeschrieben ist oder
3.  nach in der Wissenschaft üblichen Regeln als befangen gilt.

(6) Die Agentur teilt der Hochschule vor der Benennung der Gutachterinnen und Gutachter die personelle Zusammensetzung des Gutachtergremiums mit.
Die Hochschule hat ein Recht zur Stellungnahme innerhalb einer Frist von zwei Wochen.

#### § 26 Geltungszeitraum der Akkreditierung; Verlängerung

(1) Die erstmalige Akkreditierung ist für den Zeitraum von acht Jahren ab Beginn des Semesters oder Trimesters gültig, in dem die Akkreditierungsentscheidung bekanntgegeben wird.
Ist bei einer Programmakkreditierung der Studiengang noch nicht eröffnet, ist die Akkreditierung ab dem Beginn des Semesters oder Trimesters, in dem der Studiengang erstmalig angeboten wird, spätestens aber mit Beginn des zweiten auf die Bekanntgabe der Akkreditierungsentscheidung folgenden Semesters oder Trimesters wirksam.

(2) Vor Ablauf des Geltungszeitraums der Akkreditierung ist eine unmittelbar anschließende Akkreditierung (Reakkreditierung) einzuleiten.
Reakkreditierungen sind für den Zeitraum von acht Jahren gültig.

(3) Wird ein akkreditierter Studiengang nicht fortgeführt, kann die Akkreditierung für bei Ablauf des Geltungszeitraums der Akkreditierung noch eingeschriebene Studierende verlängert werden.
Die Akkreditierung eines Studiengangs kann für einen Zeitraum von bis zu zwei Jahren verlängert werden, wenn die Hochschule einen Antrag auf eine Bündel- oder Systemakkreditierung vorbereitet, in die der jeweilige Studiengang einbezogen ist.
Bei Antragstellung auf eine Bündel- oder Systemakkreditierung kann die Akkreditierung von Studiengängen, deren Akkreditierung während des Verfahrens endet, für die Dauer des Verfahrens zuzüglich eines Jahres vorläufig verlängert werden.

#### § 27 Auflagen

(1) Für die Erfüllung einer Auflage ist eine Frist von in der Regel zwölf Monaten zu setzen.

(2) In begründeten Ausnahmefällen kann die Frist auf Antrag der Hochschule verlängert werden.

(3) Die Erfüllung der Auflage ist gegenüber dem Akkreditierungsrat nachzuweisen.

#### § 28 Anzeigepflicht bei Änderungen

(1) Die Hochschule ist verpflichtet, dem Akkreditierungsrat unverzüglich jede wesentliche Änderung am Akkreditierungsgegenstand während des Geltungszeitraums der Akkreditierung anzuzeigen.

(2) Der Akkreditierungsrat entscheidet, ob die wesentliche Änderung von der bestehenden Akkreditierung umfasst ist.

#### § 29 Veröffentlichung

Die Entscheidung des Akkreditierungsrates und der Akkreditierungsbericht werden vom Akkreditierungsrat auf seiner Internetseite veröffentlicht.
Bei der Veröffentlichung dürfen personenbezogene Daten nicht offenbart werden, es sei denn, die betroffene Person hat eingewilligt oder die Einholung der Einwilligung der betroffenen Person ist nicht oder nur mit unverhältnismäßigem Aufwand möglich und es ist offensichtlich, dass die Offenbarung im Interesse der betroffenen Person liegt.
Die Sätze 1 und 2 gelten für interne Akkreditierungsentscheidungen systemakkreditierter Hochschulen entsprechend.

#### § 30 Bündelakkreditierung; Teil-Systemakkreditierung

(1) Das Gutachten des Gutachtergremiums nach § 24 Absatz 4 kann mehrere Studiengänge umfassen, wenn diese eine hohe fachliche Nähe aufweisen, die über die bloße Zugehörigkeit zu einer Fächerkultur wie Geistes- und Kulturwissenschaften, Sozialwissenschaften oder Naturwissenschaften hinaus geht (Bündelakkreditierung).
Die fachlich-inhaltlichen Kriterien nach Teil 3 sind für jeden Studiengang gesondert zu prüfen.
Ein Bündel soll sich aus nicht mehr als zehn Studiengängen zusammensetzen.

(2) Auf Antrag der Hochschule kann der Akkreditierungsrat die konkrete Zusammensetzung des Bündels vor Einreichung des Antrags nach § 23 genehmigen.

(3) Im Ausnahmefall kann eine studienorganisatorische Teileinheit der Hochschule Gegenstand der Systemakkreditierung sein.
Dies kann insbesondere der Fall sein, wenn

1.  die Akkreditierung des Qualitätsmanagementsystems für die gesamte Hochschule noch nicht sinnvoll oder nicht praktikabel ist,
2.  das Qualitätsmanagementsystem der Teileinheit in die Hochschule integriert ist und
3.  mindestens ein Studiengang der Teileinheit dieses System bereits durchlaufen hat.

#### § 31 Stichproben

(1) Bei der Systemakkreditierung und Teilsystemakkreditierung wird vom Gutachtergremium nach § 25 Absatz 2 eine Stichprobe durchgeführt.
In der Stichprobe wird geprüft, ob die im zu begutachtenden Qualitätsmanagementsystem angestrebten Wirkungen auf der Ebene des Studiengangs eintreten.

(2) Gegenstand der Stichprobe ist

1.  die Berücksichtigung aller Kriterien gemäß Teil 2 und Teil 3 innerhalb eines Studiengangs, der das Qualitätsmanagementsystem der Hochschule durchlaufen hat, und
2.  die Berücksichtigung formaler und fachlich-inhaltlicher Kriterien gemäß Teil 2 und Teil 3 nach Maßgabe des Gutachtergremiums.

Bei der Auswahl der Stichprobe berücksichtigt das Gutachtergremium das Fächerspektrum der Hochschule in der Lehre.

(3) Bietet die Hochschule Studiengänge an, die auch auf einen reglementierten Beruf vorbereiten, ist hiervon zusätzlich ein Studiengang unter Berücksichtigung der Kriterien nach Teil 2 und 3, die sich auf Studiengänge beziehen, in die Stichproben einzubeziehen; gleiches gilt für den Fall von Lehramtsstudiengängen für jeweils einen Studiengang von jedem angebotenen Lehramtstyp sowie für Studiengänge mit Evangelischer oder Katholischer Theologie/Religion.
An der Stichprobe wirkt jeweils eine von der für den jeweiligen reglementierten Beruf zuständigen Stelle benannte Vertreterin oder ein von der für den jeweiligen reglementierten Beruf zuständigen Stelle benannten Vertreter oder eine Vertreterin oder ein Vertreter der für das Schulwesen zuständigen Obersten Landesbehörde oder der jeweiligen kirchlichen Stelle mit.

### Teil 5  
Verfahrensregeln für besondere Studiengangsformen

#### § 32 Kombinationsstudiengänge

(1) Wählen die Studierenden aus einer größeren Zahl zulässiger Fächer für das Studium einzelne Fächer aus, ist jedes dieser Fächer ein Teilstudiengang als Teil eines Kombinationsstudiengangs.

(2) Akkreditierungsgegenstand ist der Kombinationsstudiengang.
Die Hochschulen stellen durch ihr jeweiliges Qualitätsmanagement sicher, dass die Studierbarkeit in allen möglichen Fächerkombinationen gegeben ist.

(3) Die Akkreditierung eines Kombinationsstudiengangs kann durch die Aufnahme weiterer wählbarer Teilstudiengänge oder Studienfächer ergänzt werden.
Der Geltungszeitraum der Akkreditierung für den Kombinationsstudiengang ändert sich dadurch nicht.

(4) Auf der Akkreditierungsurkunde werden alle in die Akkreditierung einbezogenen Teilstudiengänge oder Studienfächer aufgeführt.
Im Falle der Ergänzung der Akkreditierung nach Absatz 3 ist eine neue Akkreditierungsurkunde auszustellen.

(5) Die Regelungen von Teil 4 bleiben im Übrigen unberührt.

#### § 33 Joint-Degree-Programme

(1) Für Joint-Degree-Programme, an denen eine inländische Hochschule und weitere Hochschulen aus dem Europäischen Hochschulraum beteiligt sind, kann die Akkreditierungsentscheidung in Abweichung von § 22 Absatz 1 durch Anerkennung der Bewertung durch eine in dem European Quality Assurance Register for Higher Education (EQAR) gelistete Agentur getroffen werden.
Der Akkreditierungsrat erkennt diese Bewertung auf Antrag der Hochschule an und verleiht sein Siegel, wenn die Einhaltung der formalen und fachlich-inhaltlichen Kriterien für Joint-Degree-Programme gemäß Teil 2 und Teil 3 dieser Verordnung nachgewiesen ist und das Begutachtungsverfahren folgenden Anforderungen genügt hat:

1.  die Durchführung des Verfahrens wurde dem Akkreditierungsrat vor Beginn des Verfahrens angezeigt,
2.  die Akkreditierungsentscheidung beruht auf einem Selbstbericht der kooperierenden Hochschulen, der insbesondere Informationen zu den jeweiligen nationalen Rahmenbedingungen enthält und der die besonderen Merkmale des Joint-Degree-Programms hervorhebt,
3.  es hat eine Begehung an mindestens einem Standort des Studiengangs unter Mitwirkung von Vertreterinnen und Vertretern aller kooperierenden Hochschulen sowie anderen Beteiligten stattgefunden,
4.  die Bewertung beruht auf einem Gutachten, das die Maßgaben von Joint-Degree-Programmen in Teil 2 und Teil 3 beachtet,
5.  die Begutachtung ist durch eine mindestens vierköpfige Gutachtergruppe erfolgt, die sich mindestens wie folgt zusammengesetzt hat:
    1.  Mitglieder aus mindestens zwei der am Joint-Degree-Programm beteiligten Länder,
    2.  mindestens eine studentische Vertreterin oder ein studentischer Vertreter,
    3.  die Gutachtergruppe repräsentiert Expertise in den entsprechenden Fächern und Fachdisziplinen einschließlich des Arbeitsmarktes/der Arbeitswelt in den entsprechenden Bereichen und Expertise auf dem Gebiet der Qualitätssicherung im Hochschulbereich und verfügt über Kenntnisse der Hochschulsysteme der beteiligten Hochschulen sowie der verwendeten Unterrichtssprachen und
    4.  die Maßgaben gemäß § 25 Absatz 3 Satz 1, Absatz 5 und 6 wurden eingehalten,
6.  die Bewertung benennt folgende Merkmale: Begründung, Bestandskraft und gegebenenfalls nachgewiesene Erfüllung von Auflagen und
7.  die Agentur hat das Gutachten und die Bewertung auf ihrer Homepage in deutscher und englischer Sprache veröffentlicht.

§ 22 Absatz 2, 3 und 4 Satz 1, § 26 Absatz 1 Satz 1 und Absatz 2 Satz 1 sowie die §§ 28 und 29 gelten entsprechend.
Der Geltungszeitraum der Akkreditierung beträgt in Abweichung von § 26 Absatz 1 Satz 1 und Absatz 2 Satz 2 sechs Jahre.
Bei der Veröffentlichung wird die Entscheidung als Akkreditierungsentscheidung auf Basis des gesonderten Verfahrens für Joint-Degree-Programme kenntlich gemacht.
Die Hochschule hat dies in den Studienabschlussdokumenten deutlich zu machen.

(2) Wird ein Joint-Degree-Programm von einer inländischen Hochschule gemeinsam mit einer oder mehreren Hochschulen ausländischer Staaten koordiniert und angeboten, die nicht dem Europäischen Hochschulraum angehören (außereuropäische Kooperationspartner), so findet auf Antrag der inländischen Hochschule Absatz 1 entsprechende Anwendung, wenn sich die außereuropäischen Kooperationspartner in der Kooperationsvereinbarung mit der inländischen Hochschule zu einer Akkreditierung unter Anwendung der in Absatz 1 sowie der in den § 10 Absatz 1 und 2 und § 16 Absatz 1 geregelten Kriterien verpflichtet.

### Teil 6  
Alternative Akkreditierungsverfahren nach Artikel 3 Absatz 1 Nummer 3 Studienakkreditierungsstaatsvertrag

#### § 34 Alternative Akkreditierungsverfahren

(1) Neben die beiden in Teil 4 geregelten Verfahren können gemäß Artikel 3 Absatz 1 Nummer 3 des Studienakkreditierungsstaatsvertrages alternative Verfahren zur Sicherung und Entwicklung der Qualität in Studium und Lehre treten.

(2) In alternativen Verfahren sind die Kriterien nach Teil 2 und Teil 3 dieser Verordnung einzuhalten.
Die in Artikel 3 Absatz 2 Satz 1 des Studienakkreditierungsstaatsvertrages sowie die im Studienakkreditierungsstaatsvertrag und in dieser Verordnung geltenden Grundsätze für die angemessene Beteiligung der Wissenschaft gelten entsprechend; ebenso gelten die Mitwirkungs- und Zustimmungserfordernisse gemäß § 18 Absatz 2 entsprechend.

(3) Die Durchführung von alternativen Verfahren bedarf vorab der Zustimmung des Akkreditierungsrates und des für die Hochschulen zuständigen Mitglieds der Landesregierung; der Akkreditierungsrat kann eine externe Begutachtung veranlassen.
Der Antrag ist über das für die Hochschulen zuständige Mitglied der Landesregierung dem Akkreditierungsrat vorzulegen.
Der Akkreditierungsrat kann im Rahmen der Abstimmung mit dem für die Hochschulen zuständigen Mitglied der Landesregierung seine Zustimmung nur verweigern, wenn das alternative Verfahren den Maßgaben des Artikel 2 und den Bestimmungen des Artikel 3 Absatz 2 Satz 1 des Studienakkreditierungsstaatsvertrages sowie den im Studienakkreditierungsstaatsvertrag und in dieser Verordnung festgelegten Grundsätzen für die angemessene Beteiligung der Wissenschaft nicht entspricht.
Das alternative Verfahren soll geeignet sein, grundsätzliche Erkenntnisse zu alternativen Ansätzen externer Qualitätssicherung jenseits der in Artikel 3 Absatz 1 Nummern 1 und 2 des Studienakkreditierungsstaatsvertrages genannten Verfahren zu gewinnen.

(4) Der Akkreditierungsrat entwickelt eine Verfahrensordnung, die insbesondere die Antragsvoraussetzungen regelt.

(5) Das alternative Verfahren wird auf maximal acht Jahre befristet.
§ 22 Absatz 4 Satz 2 und § 26 Absatz 3 Satz 3 gelten entsprechend.
Es wird durch den Akkreditierungsrat begleitet und ist in der Regel zwei Jahre vor Ablauf der Projektzeit von einer unabhängigen, wissenschaftsnahen Einrichtung zu evaluieren.

### Teil 7  
Schlussbestimmungen

#### § 35 Verbindung mit Verfahren, die die berufszulassungsrechtliche Eignung eines Studiengangs zum Gegenstand haben

(1) Akkreditierungsverfahren gemäß Artikel 3 Absatz 1 Nummer 1 und 2 des Studienakkreditierungsstaatsvertrages können auf Antrag der Hochschule mit Verfahren, die über die berufszulassungsrechtliche Eignung eines Studiengangs entscheiden, organisatorisch verbunden werden.

(2) Die Beteiligung von zusätzlich zu den anderen Vertreterinnen oder den Vertreten der Berufspraxis zu berufenden externen Expertinnen oder Experten mit beratender Funktion in den Gutachtergremien gemäß § 25 Absatz 1 und 2 erfolgt durch Benennung der für den reglementierten Beruf jeweils zuständigen staatlichen Stelle.

#### § 36 Evaluation

(1) Nach Ablauf von drei Jahren nach Inkrafttreten dieser Verordnung werden ihre Anwendungen und Auswirkungen überprüft.

(2) Über das Ergebnis ist der Ständigen Konferenz der Kultusminister der Länder in der Bundesrepublik Deutschland zu berichten.

#### § 37 Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2018 in Kraft.

Potsdam, den 28.Oktober 2019

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Dr.
Martina Münch