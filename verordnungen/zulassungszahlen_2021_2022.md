## Verordnung über die Festsetzung von Zulassungszahlen für das Studienjahr 2021/2022

Auf Grund des § 11 Absatz 1 Satz 1 sowie des § 12 Absatz 2 in Verbindung mit § 16 Absatz 6 Satz 3 des Brandenburgischen Hochschulgesetzes vom 28.
April 2014 (GVBl.
I Nr. 18), von denen § 12 Absatz 2 durch Artikel 2 des Gesetzes vom 1.
Juli 2015 (GVBl.
I Nr.
18 S. 8) neu gefasst worden ist, verordnet die Ministerin für Wissenschaft, Forschung und Kultur im Benehmen mit den Hochschulen und nach Anhörung der Landeskonferenz der Studierendenschaften:

#### § 1 

(1) Für die in der Anlage bezeichneten Studiengänge wird an den dort genannten Hochschulen die Zahl der im Wintersemester 2021/2022 und im Sommersemester 2022 aufzunehmenden Bewerberinnen und Bewerber in das erste Fachsemester nach Maßgabe der Anlage festgesetzt.

(2) Die Studienplätze werden durch die Hochschulen vergeben.

#### § 2 

(1) Für die in der Anlage bezeichneten Studiengänge an den dort genannten Hochschulen werden auch Zulassungsbegrenzungen für Bewerberinnen und Bewerber, die nicht Studienanfänger sind, festgesetzt.

(2) Bewerberinnen und Bewerber, die nicht Studienanfänger sind, werden zum Weiterstudium in einem höheren Fachsemester nur in dem Maße neu aufgenommen, wie die Zahl der Studierenden des jeweiligen Fachsemesters unter der festgelegten Auffüllgrenze liegt und sofern ein Studienangebot für höhere Fachsemester besteht.
Die Studierendenzahlen und Auffüllgrenzen der jeweils einem früheren Studienjahr zuzuordnenden zwei Fachsemester werden zusammengefasst.

(3) Soweit nicht in der Anlage im Einzelnen anders festgelegt, entsprechen die Auffüllgrenzen den für den betreffenden Studiengang festgesetzten Zulassungszahlen für Studienanfänger.
Dabei ist im Wintersemester für Fachsemester mit ungerader Zahl die für das Wintersemester und für Fachsemester mit gerader Zahl die für das Sommersemester festgesetzte Zulassungszahl und im Sommersemester für Fachsemester mit ungerader Zahl die für das Sommersemester und für Fachsemester mit gerader Zahl die für das Wintersemester festgesetzte Zulassungszahl maßgeblich.
Dies gilt nicht für Masterstudiengänge, für die zum Sommer- und Wintersemester Zulassungszahlen festgesetzt sind.

#### § 3 

(1) Von den in der Anlage festgesetzten Zulassungszahlen im Studiengang Rechtswissenschaft (Staatsexamen) an der Universität Potsdam stehen im Rahmen der vereinbarten Zusammenarbeit zwischen der Universität Potsdam und der Universität Paris-Nanterre französischen Bewerberinnen und Bewerbern zum ersten Fachsemester höchstens 30 Studienplätze zur Verfügung.

(2) Von den in der Anlage festgesetzten Zulassungszahlen im Masterstudiengang Betriebswirtschaftslehre an der Universität Potsdam stehen im Wintersemester im Rahmen der vereinbarten Zusammenarbeit zwischen der Universität Potsdam und der University of International Business and Economics Beijing chinesischen Bewerberinnen und Bewerbern zum ersten Fachsemester höchstens zehn Studienplätze zur Verfügung.

#### § 4 

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über die Festsetzung von Zulassungszahlen für das Studienjahr 2020/2021 vom 15.
Juni 2020 (GVBl. II Nr. 52) außer Kraft.

Potsdam, den 6.
Juli 2021

Die Ministerin für Wissenschaft,  
Forschung und Kultur

In Vertretung

Tobias Dünow

* * *

### Anlagen

1

[Anlage - Zulassungsbeschränkte Studiengänge](/br2/sixcms/media.php/68/GVBl_II_67_2021%20Anlage.pdf "Anlage - Zulassungsbeschränkte Studiengänge") 381.6 KB