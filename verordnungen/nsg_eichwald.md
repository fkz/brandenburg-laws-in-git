## Verordnung über das Naturschutzgebiet „Eichwald mit Tzschetzschnower Schweiz und Steiler Wand“

Auf Grund des § 22 Absatz 1 und 2, der §§ 23 und 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542), von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl.
I S.
1972, 1974) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 und § 42 Absatz 2 Satz 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27. Mai 2013 (GVBl.
II Nr.
43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche in der kreisfreien Stadt Frankfurt (Oder) und im Landkreis Oder-Spree wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Eichwald mit Tzschetzschnower Schweiz und Steiler Wand“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 580 Hektar.
Es umfasst Flächen in folgenden Fluren:

Stadt/Gemeinde:

Gemarkung:

Flur:

Frankfurt (Oder)

Frankfurt (Oder)

40, 53, 54, 107, 109, 124, 125 bis 127;

Brieskow-Finkenheerd

Brieskow-Finkenheerd

2.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 4 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 13 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Innerhalb des Naturschutzgebietes wird gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes eine Zone 1 festgesetzt, die der direkten menschlichen Einflussnahme weitgehend entzogen ist.
Die Zone 1 umfasst rund 166 Hektar und liegt in folgenden Fluren:

Stadt:

Gemarkung:

Flur:

Frankfurt (Oder)

Frankfurt (Oder)

109, 124, 125.

Die Grenzen der Zone 1 sind in den in Anlage 2 Nummer 1 genannten topografischen Karten mit den Blattnummern 1 bis 3 sowie in den in Anlage 2 Nummer 2 genannten Liegenschaftskarten mit den laufenden Nummern 2, 4 und 8 bis 10 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie bei der kreisfreien Stadt Frankfurt (Oder) und dem Landkreis Oder-Spree, untere Naturschutzbehörden, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes, das einen Abschnitt der Mittleren Oder mit einer vielfältig strukturierten, von ausgedehnten Auenwäldern und Wiesen geprägten Überschwemmungslandschaft sowie überwiegend bewaldete Randhänge umfasst, ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Still- und Fließgewässer wie Quellfluren, Schwimmblatt- und Tauchfluren, Uferpionierfluren und Flutrasen, Röhrichte und Riede, der feuchten, frischen und trockenwarmen Staudenfluren, der Trocken- und Halbtrockenrasen, der Kryptogamenfluren offener Lehmwände, der Nass-, Feucht- und Frischwiesen mit ihren Brachestadien, der Streuobstwiesen, der naturnahen Wälder wie Hart- und Weichholzauenwälder, Erlen-Eschen-Auenwälder, Erlenbruchwälder, Eichen-Hainbuchenwälder, edellaubholzreiche Hang- und Schluchtwälder sowie der standorttypischen Gebüsche feuchter bis trocken-warmer Standorte;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzen- und Pilzarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Kantiger Lauch (Allium angulosum), Gottes-Gnadenkraut (Gratiola officinalis), Sumpf-Wolfsmilch (Euphorbia palustris), Glanz-Wolfsmilch (Euphorbia lucida), Langblättriger Blauweiderich (Veronica maritima), Sumpf-Platterbse (Lathyrus palustris), Kleines Tausengüldenkraut (Centaurium pulchellum), Weiße Seerose (Nymphaea alba), Schwimmfarn (Salvinia natans), Wassernuss (Trapa natans), Wasserfeder (Hottonia palustris), Fieberklee (Menyanthes trifoliata), Großes Zweiblatt (Listera ovata), Nestwurz (Neottia nidus-avis), Weißes Waldvögelein (Cephalanthera damasonium), Türkenbund-Lilie (Lilium martagon), Wiesen-Schlüsselblume (Primula veris), Körnchen-Steinbrech (Saxifraga granulata), Mondraute (Botrychium lunaria), Sand-Grasnelke (Armeria maritima ssp.
    elongata), Heide-Nelke (Dianthus deltoides), Kartäuser-Nelke (Dianthus carthusianorum) und Ähriger Blauweiderich (Veronica spicata);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Säugetiere, Vögel, Amphibien, Reptilien, Fische, Insekten und Weichtiere; darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Fledermäuse (Chiroptera spp.), Reiherente (Aythya fuligula), Schellente (Bucephala clangula), Knäkente (Anas querquedula), Gänsesäger (Mergus merganser), Seeadler (Haliaeetus albicilla), Rotmilan (Milvus milvus), Schwarzmilan (Milvus migrans), Kranich (Grus grus), Wachtelkönig (Crex crex), Kiebitz (Vanellus vanellus), Flussregenpfeifer (Charadrius dubius), Bekassine (Gallinago gallinago), Flussuferläufer (Actitis hypoleucos), Silbermöwe (Larus argentatus), Eisvogel (Alcedo atthis), Wendehals (Jynx torquilla), Mittelspecht (Dendrocopos medius), Schwarzspecht (Dryocopus martius), Neuntöter (Lanius collurio), Raubwürger (Lanius excubitor), Heidelerche (Lullula arborea), Sperbergrasmücke (Sylvia nisoria), Wiesenpieper (Anthus pratensis), Wechselkröte (Bufo viridis), Knoblauchkröte (Pelobates fuscus), Moorfrosch (Rana arvalis), Kleiner Wasserfrosch (Rana lessonae), Teichmolch (Lissotriton vulgaris), Zauneidechse (Lacerta agilis), Wald-Eidechse (Zootoca vivipara), Ringelnatter (Natrix natrix), Großer Goldkäfer (Protaetia aeruginos), Marmorierter Rosenkäfer (Protaetia lugubris), Gebänderte Prachtlibelle (Calopteryx splendens), Asiatische Keiljungfer (Gomphus flavipes), Gemeine Keiljungfer (Gomphus vulgatissimus), Malermuschel (Unio pictorum), Große Flussmuschel (Unio tumidus) und Große Teichmuschel (Anodonta cygnea);
4.  die Erhaltung des bis 20 Meter hohen und rund 500 Meter langen Kliffs „Steile Wand“ aus elsterzeitlichen Grundmoränensedimenten an einem Prallhang der Oder aus naturgeschichtlichen Gründen;
5.  die Erhaltung des Burgwalls Lossow und der slawischen Niederungsburg Burghübbel aus landeskundlichen Gründen;
6.  die Erhaltung des Gebietes zur Umweltbeobachtung und wissenschaftlichen Untersuchung ökologischer Zusammenhänge;
7.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit der strukturreichen Überschwemmungslandschaft mit Flussuferkomplexen, großflächigen altbaumreichen Auenwäldern sowie weitläufigen, durch Baumgruppen, Kopfbaumreihen, Flutrinnen und Kleingewässer gegliederten Wiesenbereichen, einschließlich naturnah bewaldeter Talhänge und Seitentäler;
8.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des großräumigen internationalen Gewässer- und Feuchtgebietsverbundes entlang der Oder sowie als Kernbereich im regionalen Wald- beziehungsweise Gehölzbiotopverbund.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebiets von gemeinschaftlicher Bedeutung „Eichwald mit Tzschetzschnower Schweiz und Steiler Wand“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), das ehemals die Gebiete von gemeinschaftlicher Bedeutung „Eichwald und Buschmühle“ und „Oderwiesen am Eichwald“ sowie einen Teil des Gebietes von gemeinschaftlicher Bedeutung „Oder-Neiße-Ergänzung“ umfasste, mit seinen Vorkommen von

1.  Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Flüssen der planaren bis montanen Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Flüssen mit Schlammbänken mit Vegetation des Chenopodion rubri p.p.
    und des Bidention p.p., Feuchten Hochstaudenfluren der planaren bis alpinen Stufe, Brenndolden-Auenwiesen (Cnidion dubii), Mageren Flachland-Mähwiesen (Alopecurus pratensis, Sanguisorba officinalis), Subatlantischem oder mitteleuropäischem Stieleichenwald oder Hainbuchenwald (Carpinion betuli-Stellario-Carpinetum), Labkraut-Eichen-Hainbuchenwald (Galio-Carpinetum), Hartholzauenwäldern mit Quercus robur, Ulmus laevis, Ulmus minor, Fraxinus excelsior oder Fraxinus angustifolia (Ulmenion minoris) als natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes,
2.  Kalktuffquellen (Ceratoneurion), Schlucht- und Hangmischwäldern (Tilio-Acerion), Auen-Wäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion, Alnion incanae, Salicion albae) als prioritären natürlichen Lebensraumtypen im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes,
3.  Fischotter (Lutra lutra), Biber (Castor fiber), Mopsfledermaus (Barbastella barbastellus), Großes Mausohr (Myotis myotis), Bechsteinfledermaus (Myotis bechsteinii), Kammmolch (Triturus cristatus), Rapfen (Aspius aspius), Steinbeißer (Cobitis taenia), Schlammpeitzger (Misgurnus fossilis), Bitterling (Rhodeus amarus), Stromgründling (Romanogobio belingi), Flussneunauge (Lampetra fluviatilis), Grüner Keiljungfer (Ophiogomphus cecilia) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume,
4.  Kriechendem Sellerie (Apium repens) als Art von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich seines Lebensraumes und den für seine Reproduktion erforderlichen Standortbedingungen,
5.  Eremit (Osmoderma eremita) als prioritärer Art im Sinne von § 7 Absatz 2 Nummer 11 des Bundesnaturschutzgesetzes, einschließlich seiner für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume;

(3) Darüber hinaus ist besonderer Schutzzweck der Zone 1 der Schutz der Gesamtheit ökologischer Prozesse in ihrer natürlichen Dynamik in dem größten zusammenhängenden Auenwaldgebiet Brandenburgs und in angrenzenden überregional bedeutsamen Hangwäldern sowie der Erhaltung eines der bedeutendsten Vorkommen der Käferart Eremit (Osmoderma eremita) in Deutschland.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten jeweils zwischen dem 1.
    August eines jeden Jahres und Ende Februar des Folgejahres außerhalb der Zone 1 und außerhalb von Feuchtbereichen zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 14;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  Wasserfahrzeuge aller Art außerhalb der Bundeswasserstraße Oder zu benutzen;
13.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
14.  Hunde frei laufen zu lassen;
15.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
16.  Düngemittel aller Art zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
17.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
18.  Tiere zu füttern oder Futter bereitzustellen;
19.  Tiere auszusetzen oder Pflanzen anzusiedeln;
20.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
21.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
22.  Pflanzenschutzmittel jeder Art anzuwenden;
23.  Wiesen, Weiden oder sonstiges Grünland umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung außerhalb der Zone 1 auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertende Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und Sekundärrohstoffdünger einzusetzen.
        Sekundärrohstoffdünger im Sinne dieser Verordnung sind Abwasser, Fäkalien, Klärschlamm und ähnliche Stoffe aus Siedlungsabfällen und vergleichbare Stoffe aus anderen Quellen, jeweils auch weiterbehandelt und in Mischungen untereinander oder mit Düngemitteln, Wirtschaftsdünger, Bodenhilfsstoffen, Kultursubstraten und Pflanzenhilfsmitteln,
    2.  auf Grünland § 4 Absatz 2 Nummer 22 und 23 gilt, wobei bei Narbenschäden eine umbruchlose Nachsaat mit standortangepassten Grasarten zulässig ist,
    3.  das Walzen und Schleppen von Grünland im Zeitraum vom 31.
        März bis zur ersten Nutzung eines jeden Jahres unzulässig bleibt,
    4.  auf Grünland bei Beweidung Gehölze in geeigneter Weise gegen Verbiss und sonstige Beschädigungen sowie Ränder von Gewässern wirksam gegen Trittschäden von weidenden Nutztieren geschützt werden; diese Maßgabe gilt nicht für die Gewässer Bardaune II und Hospitalmühlenfließ im Bereich der Oderwiesen;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung außerhalb der Zone 1 auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  nur Arten der jeweils potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Gehölzarten in gesellschaftstypischer Zusammensetzung unter Ausschluss eingebürgerter Arten zu verwenden sind,
    2.  eine Nutzung nur einzelstamm- bis gruppenweise, in Robinienbeständen ausschließlich einzelstammweise erfolgt,
    3.  hydromorphe Böden nur bei ausreichender Tragfähigkeit durch Frost sowie Böden mit einem hohen Anteil an feinkörnigem Substrat nur bei ausreichender Tragfähigkeit durch Frost oder in Trockenperioden auf dauerhaft festgelegten Rückegassen befahren werden,
    4.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    5.  ein Altholzanteil von mindestens 10 Prozent am aktuellen Bestandesvorrat zu sichern ist, wobei mindestens fünf Stämme je Hektar mit einem Durchmesser von mehr als 35 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum natürlichen Absterben und Zerfall aus der Nutzung genommen sein müssen,
    6.  je Hektar mindestens fünf Stück lebensraumtypische, abgestorbene, stehende Bäume (Totholz) mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden; liegendes Totholz (ganze Bäume mit einem Durchmesser von über 65 Zentimetern am stärkeren Ende) verbleibt als ganzer Baum im Bestand,
    7.  § 4 Absatz 2 Nummer 16 und 22 gilt.Die Gewinnung von Vermehrungsgut bleibt auch in der Zone 1 zulässig;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  innerhalb der Zone 1 nur erforderliche Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne eines Monitorings mit Genehmigung der unteren Naturschutzbehörde zulässig sind; die Genehmigung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    2.  auf den Flächen außerhalb der Zone 1:
        
        aa)
        
        Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Bibers und des Fischotters weitgehend ausgeschlossen ist,
        
        bb)
        
        der Fischbesatz nur mit heimischen Arten erfolgt; § 13 der Fischereiordnung des Landes Brandenburg bleibt unberührt,
        
        cc)
        
        § 4 Absatz 2 Nummer 18 gilt;
        
4.  die rechtmäßige Ausübung der Angelfischerei außerhalb der Zone 1 an der Oder und an Oderaltarmen mit der Maßgabe, dass
    1.  das Betreten und die Beseitigung von Uferröhrichten unzulässig ist,
    2.  § 4 Absatz 2 Nummer 12, 18 und 19 gilt;
5.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Jagd auf Wasserfederwild verboten ist,
        
        bb)
        
        in der Zeit vom 1.
        März bis zum 31.
        Juli eines jeden Jahres keine Bewegungsjagd erfolgt,
        
        cc)
        
        die Fallenjagd nur mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern vom Ufer der Fließgewässer und Altarme verboten ist.
        Von der Einhaltung dieses Abstandes kann die untere Naturschutzbehörde Ausnahmen erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
        dd)
        
        keine Baujagd in einem Abstand von bis zu 100 Metern vom Ufer der Fließgewässer und Altarme vorgenommen wird,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    3.  der Einsatz transportabler und mobiler Ansitzeinrichtungen,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope,
    
    Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern sind unzulässig.
    Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
6.  die Durchführung biotopeinrichtender Maßnahmen in Pappel-, Rot-Eschen- und Robinienbeständen der Zone 1 zur Regeneration standorttypischer Wälder bis zum 31. Dezember 2028 im Einvernehmen mit der unteren Naturschutzbehörde;
7.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege.
    Die untere Naturschutzbehörde ist rechtzeitig mit dem Ziel einer einvernehmlichen Lösung zu beteiligen;
8.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer und die ordnungsgemäße Unterhaltung der Bundeswasserstraße Strom-Oder, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
9.  die ordnungsgemäße Unterhaltung der Anlagen an der Bundeswasserstraße Oder sowie der Bau und die wesentliche Änderung von Anlagen, sofern sie nicht einer Planfeststellung unterliegen, im Benehmen mit der unteren Naturschutzbehörde.
    Gegenstand der Benehmensherstellung ist dabei auch die Prüfung der Verträglichkeit mit den in § 3 Absatz 2 genannten Erhaltungszielen des Gebietes.
    Soll bei der Durchführung der Maßnahme von der Stellungnahme der unteren Naturschutzbehörde abgewichen werden, entscheidet hierüber die Generaldirektion Wasserstraßen und Schifffahrt im Benehmen mit der Fachbehörde für Naturschutz und Landschaftspflege;
10.  Maßnahmen des Hochwasserschutzes zur Beseitigung von Abflusshindernissen im Überschwemmungsbereich, wie zum Beispiel Stammholz, Äste, Treibgut, wenn dadurch der Schutzzweck nicht gefährdet wird.
    Die Maßnahmen sind der unteren Naturschutzbehörde gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes schriftlich anzuzeigen;
11.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
12.  die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern diese nicht unter die Nummern 8, 9 und 11 fallen, im Einvernehmen mit der unteren Naturschutzbehörde;
13.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
14.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch außerhalb der Zone 1 und außerhalb von Feuchtbereichen jeweils zwischen dem 1. August eines jeden Jahres bis Ende Februar des Folgejahres;
15.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung nach Anzeige gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes bei der unteren Naturschutzbehörde;
16.  Maßnahmen zum Schutz, zur Erhaltung, Pflege, Erforschung und Zugänglichkeit von Bodendenkmalen im Einvernehmen mit der unteren Naturschutzbehörde.
    Das Einvernehmen ist zu erteilen, wenn der Schutzzweck nicht erheblich beeinträchtigt wird;
17.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
18.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen;
19.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungsbefugnissen, die sich aus anderen fachrechtlichen Vorgaben ergeben, unberührt.

#### § 6 Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  im Überschwemmungsbereich der Oder soll eine naturnahe Auendynamik durch geeignete Maßnahmen gefördert werden, zum Beispiel durch Rückbau von nicht mehr benötigtem Uferdeckwerk und von künstlichen Strömungshindernissen in der Aue;
2.  entlang der Oder sollen Uferstreifen von mindestens 20 Meter Breite ungenutzt bleiben; in geeigneten Bereichen soll dort die Entwicklung von Weichholzarten gefördert werden;
3.  künstlich veränderte Abschnitte von natürlichen Oderzuflüssen wie Hospitalmühlenfließ und Bardaune sollen renaturiert und ihre ökologische Durchgängigkeit verbessert werden, zum Beispiel durch Rückbau von Quell-, Ufer- und Sohlbefestigungen, Förderung von Seitenerosion, Aufweitung von Wege-, Straßen- und Bahndurchlässen;
4.  der Gebietswasserhaushalt soll renaturiert werden, indem Gräben außerhalb von Nutzungszeiten eingestaut bleiben.
    Die Unterhaltung von Gräben und ihren Vorflutern soll, wenn nötig, möglichst schonend erfolgen;
5.  Grünland soll vorzugsweise als Mähwiese oder Mähweide genutzt werden, wobei in Wuchsschwerpunkten von Stromtalpflanzen eine Nutzungspause vom 15.
    Juni bis 31.
    August eines jeden Jahres eingehalten werden soll und in Schwerpunkten der Fortpflanzungsstätten von wiesenbrütenden Vogelarten die erste Nutzung frühestens ab Mitte Juni eines jeden Jahres und die Mahd von innen nach außen beziehungsweise in Streifen erfolgen soll;
6.  zur Förderung der Eichen-Naturverjüngung sowie zur Freistellung von Brutbäumen des Eremiten sollen an geeigneten Stellen in Auen- und Hangwäldern der Zone 1 im Einvernehmen mit der unteren Naturschutzbehörde Einzelbäume entnommen werden;
7.  zur Erhaltung und Entwicklung von naturnah bestockten und strukturierten Waldflächen außerhalb der Zone 1 sollen bei deren Bewirtschaftung
    1.  andere nicht gebietsheimische Gehölzarten, wie zum Beispiel Robinie (Robinia pseudoacacia), Spätblühende Traubenkirsche (Prunus serotina), Hybrid- und Balsam-Pappel (Populus x canadensis, P.
        balsamifera), Rot-Esche (Fraxinus pennsylvanica), Eschen-Ahorn (Acer negundo), Fichte (Picea abies) und andere Nadelholzarten im Rahmen der Nutzung durch bevorzugte Entnahme und Mischungsregulierung zurückgedrängt werden,
    2.  im Unter- und Zwischenstand vorhandene Baumarten der jeweils natürlichen Waldgesellschaft durch Freistellung begünstigt und in die nächste Bestandesgeneration übernommen werden,
    3.  Baumarten der jeweils natürlichen Waldgesellschaften, insbesondere Stiel- und Trauben-Eiche, Hainbuche und Winter-Linde auch durch Saat ergänzt werden,
    4.  auf Steilhängen mit über 25 Prozent Neigung Holzerntemaßnahmen möglichst unterbleiben oder diese nur mit nicht bodengebundener Holzerntetechnik erfolgen;
8.  im Umkreis von einer Baumlänge zum Fledermauswinterquartier Bunker Lossow sollen forstwirtschaftliche Maßnahmen unterbleiben;
9.  Kopfbaum- und Streuobstbestände sollen gepflegt werden;
10.  zum Schutz der Avifauna soll der Waschbär intensiv bejagt werden;
11.  in angrenzenden, zum Naturschutzgebiet hin geneigten Ackerflächen sollen zum Schutz von Waldbiotopen vor Stoffeinträgen und Erosion Randstreifen angelegt werden;
12.  es sollen geeignete Einrichtungen zur Besucherlenkung und -information geschaffen werden wie zum Beispiel eine Wegekonzeption zur Vermeidung von Störungen während des Brutgeschehens von Bodenbrütern auf Auenwiesen und entlang der Oder sowie von Greifvögeln im Eichwald.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 1 Nummer 1 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten, Außerkrafttreten

(1) § 5 Absatz 1 Nummer 1 Buchstabe a und b tritt am 1.
Januar 2019 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig tritt der Beschluss 130, Abschnitt II.2, des Bezirkstages Frankfurt (Oder) vom 14. März 1990 über das Naturschutzgebiet „Eichwald und Buschmühle“ außer Kraft.

Potsdam, den 6.
November 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Eichwald mit Tzschetzschnower Schweiz und Steiler Wand“](/br2/sixcms/media.php/68/GVBl_II_75_2018-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Eichwald mit Tzschetzschnower Schweiz und Steiler Wand“") 434.6 KB

2

[Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten](/br2/sixcms/media.php/68/GVBl_II_75_2018-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten") 208.1 KB

3

[Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Eichwald mit Tzschetzschnower Schweiz und Steiler Wand“](/br2/sixcms/media.php/68/GVBl_II_75_2018-Anlage-3.pdf "Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Eichwald mit Tzschetzschnower Schweiz und Steiler Wand“") 205.2 KB