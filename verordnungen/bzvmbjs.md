## Verordnung über die beamtenrechtlichen Zuständigkeiten im Geschäftsbereich des Ministeriums für Bildung, Jugend und Sport (Beamtenzuständigkeitsverordnung MBJS - BZVMBJS)

Auf Grund des § 6 Absatz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28) geändert worden ist, in Verbindung mit

*   § 4 Absatz 1 Satz 2 des Landesbeamtengesetzes vom 3.
    April 2009 (GVBl. I S. 26) in Verbindung mit § 1 Absatz 3 Satz 1 der Ernennungsverordnung vom 1.
    August 2004 (GVBl. II S. 742),
*   § 32 Absatz 1 Satz 1, § 38 Satz 2, § 50 Satz 1 zweiter Halbsatz, § 54 Absatz 1 Satz 1, § 56 Absatz 1 Satz 5, § 57 Absatz 1 Satz 2, § 66 Absatz 4 zweiter Halbsatz, § 69 Absatz 5 Satz 1, § 84 Satz 2, § 85 Absatz 2 Satz 1, § 86 Absatz 1 Satz 3, § 87 Satz 4, § 88 Satz 5, § 89 Satz 3, § 92 Absatz 2 zweiter Halbsatz und § 103 Absatz 2 des Landesbeamtengesetzes, von denen § 50 Satz 1 durch Artikel 1 Nummer 18 des Gesetzes vom 5.
    Dezember 2013 (GVBl.
    I Nr. 36 S. 9) geändert worden ist,
*   § 63 Absatz 3 Satz 3 des Landesbeamtengesetzes, der durch Artikel 4 Nummer 2 des Gesetzes vom 20.
    November 2013 (GVBl.
    I Nr. 32 S. 123) geändert worden ist, und § 9 Absatz 3 der Trennungsgeldverordnung in der Fassung der Bekanntmachung vom 29. Juni 1999 (BGBl. I S. 1533) im Einvernehmen mit dem Minister der Finanzen,
*   § 6 Satz 2 der Dienstjubiläumsverordnung vom 18.
    Dezember 2014 (BGBl. I S. 2267) in Verbindung mit § 64 Satz 1 des Landesbeamtengesetzes, der durch das Gesetz vom 11. März 2010 (GVBl.
    I Nr. 13) neu gefasst worden ist,
*   § 39 Absatz 4 Satz 2 und 3, § 48 Absatz 1 Satz 4 und § 49 Satz 2 der Schullaufbahnverordnung vom 24.
    Juni 1999 (GVBl.
    II S. 378),
*   § 17 Absatz 2 Satz 2, § 34 Absatz 5, § 35 Absatz 2 Satz 2 und § 42 Absatz 2 Satz 2 des Landesdisziplinargesetzes vom 18.
    Dezember 2001 (GVBl.
    I S. 254),
*   § 13 Absatz 2 Satz 3 des Brandenburgischen Besoldungsgesetzes vom 20. November 2013 (GVBl.
    I Nr. 32),
*   § 54 Absatz 3 Satz 2 des Beamtenstatusgesetzes vom 17.
    Juni 2008 (BGBl. I S. 1010) in Verbindung mit § 103 Absatz 2 des Landesbeamtengesetzes,
*   § 47 Absatz 3 Satz 3, § 48 Absatz 2 Satz 1, § 57 Absatz 6 Satz 2 und Absatz 7 Satz 1, § 63 Absatz 1 Satz 1 in Verbindung mit § 89 Absatz 1 des Brandenburgischen Beamtenversorgungsgesetzes vom 20.
    November 2013 (GVBl.
    I Nr. 32) und
*   § 7 Absatz 2 Satz 3, § 47 Absatz 3 Satz 3, § 48 Absatz 2 Satz 1, § 57 Absatz 6 Satz 2 und Absatz 7 Satz 1, § 63 Absatz 1 Satz 1 in Verbindung mit § 89 Absatz 1 des Brandenburgischen Beamtenversorgungsgesetzes im Einvernehmen mit dem Minister der Finanzen

verordnet die Ministerin für Bildung, Jugend und Sport:

#### § 1 Übertragung der Ernennungsbefugnis

(1) Die Ausübung der Befugnis zur Ernennung der Landesbeamtinnen und Landesbeamten in den Laufbahnen des mittleren, des gehobenen und des höheren allgemeinen Verwaltungsdienstes wird den staatlichen Schulämtern für ihren jeweiligen Zuständigkeitsbereich übertragen.

(2) Die Ausübung der Befugnis zur Ernennung der Landesbeamtinnen und Landesbeamten in den Eingangsämtern der Laufbahnen des gehobenen und des höheren Schuldienstes wird den staatlichen Schulämtern für ihren jeweiligen Zuständigkeitsbereich übertragen.
Die Befugnis gemäß Satz 1 gilt auch für die Beförderungsämter in diesen Laufbahnen, sofern damit keine Funktionen in der Schulleitung gemäß § 69 des Brandenburgischen Schulgesetzes verbunden sind.

(3) Die Ausübung der Befugnis zur Ernennung der Landesbeamtinnen und Landesbeamten in den Laufbahnen des mittleren, des gehobenen und des höheren allgemeinen Verwaltungsdienstes wird dem Landesinstitut für Schule und Medien Berlin-Brandenburg für seinen Zuständigkeitsbereich übertragen.

(4) Die nach den Absätzen 1 bis 3 jeweils übertragene Befugnis wird im Namen des Landes Brandenburg ausgeübt.

#### § 2 Übertragung weiterer Befugnisse auf die staatlichen Schulämter

Den staatlichen Schulämtern werden jeweils für ihren Zuständigkeitsbereich die folgenden beamtenrechtlichen Zuständigkeiten übertragen:

1.  Entscheidung über das Vorliegen der Voraussetzungen bei Entlassung aus dem Beamtenverhältnis gemäß § 32 des Landesbeamtengesetzes in Verbindung mit § 22 des Beamtenstatusgesetzes,
2.  Entscheidung über die Versetzung in den Ruhestand von Beamtinnen und Beamten auf Probe gemäß § 38 des Landesbeamtengesetzes in Verbindung mit § 28 Absatz 1 und 2 des Beamtenstatusgesetzes, wobei die Entscheidung in Fällen des § 28 Absatz 2 des Beamtenstatusgesetzes der vorherigen Zustimmung des für Bildung zuständigen Ministeriums bedarf,
3.  Entscheidung über die Versetzung in den Ruhestand von Beamtinnen und Beamten gemäß § 50 Satz 1 des Landesbeamtengesetzes in Beförderungsämtern der Laufbahnen des gehobenen und des höheren Schuldienstes, die mit einer Funktion in der Schulleitung gemäß § 69 des Brandenburgischen Schulgesetzes verbunden sind, sowie in der Laufbahn des Schulaufsichtsdienstes,
4.  Entscheidung über das Verbot der Führung der Dienstgeschäfte gemäß § 54 des Landesbeamtengesetzes,
5.  Entscheidung über die Versagung der Aussagegenehmigung gemäß § 56 des Landesbeamtengesetzes, wobei die Versagung der Aussagegenehmigung der vorherigen Zustimmung des für Bildung zuständigen Ministeriums bedarf,
6.  Zustimmung zur Annahme von Belohnungen und Geschenken und sonstigen Vorteilen gemäß § 57 des Landesbeamtengesetzes,
7.  Genehmigung zur Führung der Amtsbezeichnung mit dem Zusatz „außer Dienst“ („a.
    D.“) gemäß § 69 des Landesbeamtengesetzes,
8.  Nebentätigkeitsangelegenheiten und Untersagung von Tätigkeiten nach Beendigung des Beamtenverhältnisses gemäß den §§ 84 bis 89 und 92 des Landesbeamtengesetzes, soweit nicht die Entscheidung gemäß § 87 Satz 1 des Landesbeamtengesetzes dem für Bildung zuständigen Ministerium vorbehalten ist,
9.  Befugnis zur Gewährung und Versagung der Dienstjubiläumszuwendungen gemäß § 6 der Dienstjubiläumsverordnung,
10.  Anerkennung von Urlaub auf die Probezeit gemäß § 39 der Schullaufbahnverordnung sowie die Anrechnung der Probezeit und sonstigen Dienstzeiten bei Übernahme von Beamtinnen und Beamten anderer Dienstherren gemäß § 48 der Schullaufbahnverordnung,
11.  Disziplinarbefugnis bei Ruhestandsbeamtinnen und Ruhestandsbeamten gemäß § 17 des Landesdisziplinargesetzes, Disziplinarbefugnisse gemäß den §§ 34, 35 und 42 des Landesdisziplinargesetzes, wobei die Erhebung der Disziplinarklage der vorherigen Zustimmung des für Bildung zuständigen Ministeriums bedarf.

#### § 3 Übertragung weiterer Befugnisse auf das Landesinstitut für Schule und Medien Berlin-Brandenburg

Dem Landesinstitut für Schule und Medien Berlin-Brandenburg werden die folgenden beamtenrechtlichen Zuständigkeiten übertragen:

1.  Entscheidung über das Vorliegen der Voraussetzungen bei Entlassung aus dem Beamtenverhältnis gemäß § 32 des Landesbeamtengesetzes in Verbindung mit § 22 des Beamtenstatusgesetzes,
2.  Entscheidung über die Versetzung in den Ruhestand von Beamtinnen und Beamten auf Probe gemäß § 38 des Landesbeamtengesetzes in Verbindung mit § 28 Absatz 1 und 2 des Beamtenstatusgesetzes, wobei die Entscheidung in Fällen des § 28 Absatz 2 des Beamtenstatusgesetzes der vorherigen Zustimmung des für Bildung zuständigen Ministeriums bedarf,
3.  Entscheidung über die Versetzung in den Ruhestand von Beamtinnen und Beamten gemäß § 50 Satz 1 des Landesbeamtengesetzes in Laufbahnen des gehobenen und des höheren Schuldienstes sowie in der Laufbahn des Schulaufsichtsdienstes,
4.  Entscheidung über das Verbot der Führung der Dienstgeschäfte gemäß § 54 des Landesbeamtengesetzes,
5.  Entscheidung über die Versagung der Aussagegenehmigung gemäß § 56 des Landesbeamtengesetzes, wobei die Versagung der Aussagegenehmigung der vorherigen Zustimmung des für Bildung zuständigen Ministeriums bedarf,
6.  Zustimmung zur Annahme von Belohnungen und Geschenken und sonstigen Vorteilen gemäß § 57 des Landesbeamtengesetzes,
7.  Genehmigung zur Führung der Amtsbezeichnung mit dem Zusatz „außer Dienst“ („a.
    D.“) gemäß § 69 des Landesbeamtengesetzes,
8.  Nebentätigkeitsangelegenheiten und Untersagung von Tätigkeiten nach Beendigung des Beamtenverhältnisses gemäß den §§ 84 bis 89 und 92 des Landesbeamtengesetzes, soweit nicht die Entscheidung gemäß § 87 Satz 1 des Landesbeamtengesetzes dem für Bildung zuständigen Ministerium vorbehalten ist,
9.  Befugnis zur Gewährung und Versagung der Dienstjubiläumszuwendungen gemäß § 6 der Dienstjubiläumsverordnung,
10.  Anerkennung von Urlaub auf die Probezeit gemäß § 39 der Schullaufbahnverordnung sowie die Anrechnung der Probezeit und sonstigen Dienstzeiten bei Übernahme von Beamtinnen und Beamten anderer Dienstherren gemäß § 48 der Schullaufbahnverordnung,
11.  Disziplinarbefugnis bei Ruhestandsbeamtinnen und Ruhestandsbeamten gemäß § 17 des Landesdisziplinargesetzes, Disziplinarbefugnisse gemäß den §§ 34, 35 und 42 des Landesdisziplinargesetzes, wobei die Erhebung der Disziplinarklage der vorherigen Zustimmung des für Bildung zuständigen Ministeriums bedarf.

#### § 4 Übertragung von Aufgaben auf die Zentrale Bezügestelle des Landes Brandenburg

(1) Die Zuständigkeit im Geschäftsbereich des Ministeriums für Bildung, Jugend und Sport für die Berechnung und Zahlung von Reisekosten gemäß § 63 Absatz 1 Satz 1 und Absatz 3 Satz 3 des Landesbeamtengesetzes wird mit Ausnahme der Landesbeamtinnen und Landesbeamten des Landesinstitutes für Schule und Medien Berlin-Brandenburg auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

(2) Die Zuständigkeit im Geschäftsbereich des Ministeriums für Bildung, Jugend und Sport für die Berechnung und Zahlung von Umzugskosten gemäß § 63 Absatz 1 Satz 1 und Absatz 3 Satz 3 des Landesbeamtengesetzes wird auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

(3) Die Zuständigkeit im Geschäftsbereich des Ministeriums für Bildung, Jugend und Sport für die Bewilligung, Berechnung und Zahlung von Trennungsgeld gemäß § 9 Absatz 3 der Trennungsgeldverordnung in Verbindung mit § 63 Absatz 3 Satz 1 des Landesbeamtengesetzes wird mit Ausnahme der Landesbeamtinnen und Landesbeamten des Landesinstitutes für Schule und Medien Berlin-Brandenburg auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

(4) Die Zuständigkeit im Geschäftsbereich des Ministeriums für Bildung, Jugend und Sport für den Ersatz von Sachschäden und die Geltendmachung von übergegangenen Schadensersatzansprüchen gegen Dritte gemäß § 66 Absatz 1, 3 und 4 des Landesbeamtengesetzes wird auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

(5) Die Zuständigkeit im Geschäftsbereich des Ministeriums für Bildung, Jugend und Sport für die Zustimmung zu einem Verzicht auf die Rückforderung von Bezügen aus Billigkeitsgründen gemäß § 13 Absatz 2 Satz 3 des Brandenburgischen Besoldungsgesetzes wird auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

(6) Die Zuständigkeit im Geschäftsbereich des Ministeriums für Bildung, Jugend und Sport für die Gewährung von Unfallfürsorge gemäß Abschnitt 2 Unterabschnitt 3 des Brandenburgischen Beamtenversorgungsgesetzes wird auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

(7) Die Zuständigkeit im Geschäftsbereich des Ministeriums für Bildung, Jugend und Sport für die Zustimmung zu einem Verzicht auf die Rückforderung von Versorgungsbezügen aus Billigkeitsgründen gemäß § 7 Absatz 2 Satz 3 des Brandenburgischen Beamtenversorgungsgesetzes wird auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

#### § 5 Befugnis zum Erlass von Widerspruchsbescheiden

(1) Die Zuständigkeit für den Erlass von Widerspruchsbescheiden in beamtenrechtlichen Angelegenheiten der Beamtinnen und Beamten, Ruhestandsbeamtinnen und Ruhestandsbeamten und früheren Beamtinnen und Beamten im Geschäftsbereich des für Bildung zuständigen Ministeriums sowie deren Hinterbliebenen wird auf die Zentrale Bezügestelle des Landes Brandenburg übertragen, soweit diese die mit dem Widerspruch angegriffene Maßnahme getroffen oder unterlassen hat.

(2) Die Zuständigkeit für den Erlass von Widerspruchsbescheiden in beamtenrechtlichen Angelegenheiten der Beamtinnen und Beamten, Ruhestandsbeamtinnen und Ruhestandsbeamten und früheren Beamtinnen und Beamten im Geschäftsbereich des für Bildung zuständigen Ministeriums sowie deren Hinterbliebenen wird auf die staatlichen Schulämter für ihren jeweiligen Zuständigkeitsbereich übertragen, soweit diese die mit dem Widerspruch angegriffene Maßnahme getroffen oder unterlassen haben.

(3) Die Zuständigkeit für den Erlass von Widerspruchsbescheiden in beamtenrechtlichen Angelegenheiten der Beamtinnen und Beamten, Ruhestandsbeamtinnen und Ruhestandsbeamten und früheren Beamtinnen und Beamten im Geschäftsbereich des für Bildung zuständigen Ministeriums sowie deren Hinterbliebenen wird auf das Landesinstitut für Schule und Medien Berlin-Brandenburg übertragen, soweit dieses die mit dem Widerspruch angegriffene Maßnahme getroffen oder unterlassen hat.

#### § 6 Vertretung bei Klagen aus dem Beamtenverhältnis

Die Vertretung des Landes vor den Gerichten der Verwaltungsgerichtsbarkeit gemäß § 103 des Landesbeamtengesetzes wird den in den §§ 1 bis 4 genannten Stellen übertragen.
Satz 1 ist in Verfahren auf einstweiligen Rechtsschutz (§§ 80 bis 80b und 123 der Verwaltungsgerichtsordnung) entsprechend anzuwenden.

#### § 7 Übergangsvorschriften

(1) Die in § 4 Absatz 1 und 3 benannten Zuständigkeiten gelten mit der Maßgabe, dass sie für die Landesbeamtinnen und Landesbeamten der staatlichen Schulämter in den Laufbahnen des gehobenen und des höheren Schuldienstes im Bereich

1.  des Staatlichen Schulamtes Cottbus bis zum 31.
    Januar 2018,
2.  des Staatlichen Schulamtes Brandenburg an der Havel bis zum 30.
    April 2018,
3.  des Staatlichen Schulamtes Frankfurt (Oder) bis zum 31.
    Juli 2018 und
4.  des Staatlichen Schulamtes Neuruppin bis zum 31.
    Oktober 2018

im Geschäftsbereich des Ministeriums für Bildung, Jugend und Sport verbleiben.
Dies gilt nicht, soweit die Berechnung und Zahlung von Reisekosten sowie die Bewilligung, Berechnung und Zahlung von Trennungsgeld für die Landesbeamtinnen und Landesbeamten in den Laufbahnen des gehobenen und höheren Schuldienstes im Rahmen der Tätigkeiten in den Studienseminaren anfallen.

(2) Die in § 4 Absatz 1 und 3 benannten Zuständigkeiten gelten mit der Maßgabe, dass sie für die Landesbeamtinnen und Landesbeamten, die im Beamtenverhältnis auf Widerruf den Vorbereitungsdienst ableisten (Lehramtskandidatinnen und Lehramtskandidaten), bis zum 30. November 2018 im Zuständigkeitsbereich des Ministeriums für Bildung, Jugend und Sport verbleiben.

(3) Für Anträge, die vor dem Inkrafttreten dieser Verordnung eingegangen sind und über die noch nicht abschließend entschieden worden ist, soweit die Bearbeitung nicht bereits der Zentralen Bezügestelle des Landes Brandenburg übertragen war, verbleibt es bei den bisherigen Zuständigkeiten.
Dies gilt auch für die Vertretung in zu diesem Zeitpunkt bereits anhängigen Rechtsstreitigkeiten.
Die Sätze 1 und 2 finden auch auf die in Absatz 1 Satz 1 Nummer 1 bis 4 und Absatz 2 genannten Zeiträume Anwendung, in denen die Zuständigkeit im Geschäftsbereich des Ministeriums für Bildung, Jugend und Sport verbleibt.

#### § 8 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2018 in Kraft.
Gleichzeitig tritt die Beamtenzuständigkeitsverordnung MBJS vom 16.
März 2016 (GVBl.
II Nr. 11) außer Kraft.

Potsdam, den 5.
Februar 2018

Die Ministerin für Bildung,  
Jugend und Sport

Britta Ernst