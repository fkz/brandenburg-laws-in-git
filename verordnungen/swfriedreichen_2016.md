## Verordnung über den Schutzwald „Friedrichsthaler Eichen“

Auf Grund des § 12 des Waldgesetzes des Landes Brandenburg vom 20. April 2004 (GVBl. I S. 137), der durch Artikel 2 des Gesetzes vom 21. Juni 2007 (GVBl. I S. 106, 108) geändert worden ist, verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzwald

Die in § 2 näher bezeichneten Waldflächen mit teilweise besonderer Schutzfunktion als Naturwald werden zum Schutzwald erklärt.
Der Schutzwald trägt die Bezeichnung „Friedrichsthaler Eichen“ und wird in das Register der geschützten Waldgebiete aufgenommen.

#### § 2 Schutzgegenstand

(1) Das geschützte Waldgebiet befindet sich im Landkreis Uckermark und hat eine Größe von rund 161 Hektar.
Es umfasst folgende Flurstücke:

**Stadt:**

**Gemarkung:**

**Flur:**

**Flurstück:**

Schwedt/Oder

Hohenfelde

3

141 (teilweise), 303, 305, 307, 309, 311, 313,  
323 (teilweise), 328 bis 333, 334 (teilweise),  
335 (teilweise), 359, 360.

Die Waldfläche mit besonderer Schutzfunktion als Naturwald „Pommersche Heide“ hat eine Größe von rund 36 Hektar.
Sie umfasst folgendes Flurstück:

**Stadt:**

**Gemarkung:**

**Flur:**

**Flurstück:**

Schwedt/Oder

Hohenfelde

3

307 (teilweise).

(2) Zur Orientierung ist dieser Verordnung eine Kartenskizze über die Lage des Schutzwaldes und des Naturwaldes als Anlage beigefügt.

(3) Die Grenze des Schutzwaldes ist in der „Topografischen Karte zur Verordnung über den Schutzwald ‚Friedrichsthaler Eichen‘“, Maßstab 1 : 10 000 und in der „Liegenschaftskarte zur Verordnung über den Schutzwald ‚Friedrichsthaler Eichen‘“, Maßstab 1 : 7 500 mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Liegenschaftskarte.
Die Karten sind mit dem Dienstsiegel des Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft des Landes Brandenburg, Siegelnummer 19, versehen und vom Siegelverwalter am 7. März 2016 unterschrieben worden.

(4) Die Verordnung mit Karten kann bei dem für Forsten zuständigen Fachministerium des Landes Brandenburg, oberste Forstbehörde, in Potsdam sowie beim Landesbetrieb Forst Brandenburg, untere Forstbehörde, in Potsdam von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Schutzwaldes ist

1.  der Erhalt und die Entwicklung eines natürlich entstandenen Kiefern-Eichenwaldes zum Zweck der wissenschaftlichen Beobachtung und Erforschung der naturnahen Entwicklung des Waldes;
2.  die Erhaltung und Wiederherstellung der Leistungsfähigkeit des Naturhaushaltes;
3.  die Erhaltung und Entwicklung des Gebietes als Lebensraum gefährdeter Pflanzen- und Tierarten;
4.  die Sicherung der genetischen Ressourcen.

(2) Der besondere Schutzzweck ist der Erhalt des Naturwaldes insbesondere zur

1.  Repräsentation eines besonders wertvollen Kiefern-Eichenwaldes;
2.  langfristigen wissenschaftlichen Erforschung der durch den Menschen nicht direkt beeinflussten Waldentwicklung;
3.  Erforschung der Waldstruktur, des Bodens, der Flora und der Fauna;
4.  Nutzung als lokale und regionale Weiserfläche zur Ableitung und exemplarischen Veranschaulichung von Erkenntnissen für die Waldbaupraxis und forstliche Lehre;
5.  Erhaltung und Regeneration forstgenetischer Ressourcen;
6.  Erhaltung der floristischen und faunistischen Artenvielfalt in sich natürlich entwickelnden Lebensgemeinschaften.

#### § 4 Verbote, Maßgaben zur forstwirtschaftlichen Bodennutzung

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind im Schutzwald gemäß § 12 Absatz 6 des Waldgesetzes des Landes Brandenburg alle Handlungen verboten, die dem in § 3 genannten Schutzzweck zuwiderlaufen, die das Gebiet oder einzelne seiner Bestandteile nachhaltig stören, verändern, beschädigen oder zerstören können.

(2) Es ist insbesondere verboten:

1.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
2.  das Gebiet außerhalb der Waldwege zu betreten;
3.  im Gebiet mit motorisierten Fahrzeugen und Gespannen zu fahren oder diese dort abzustellen;
4.  zu lagern, zu zelten, Wohnwagen aufzustellen, zu rauchen und Feuer zu entzünden;
5.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
6.  die Bodengestalt zu verändern, Böden zu verfestigen und zu versiegeln;
7.  Düngemittel einschließlich Wirtschaftsdünger und Sekundärrohstoffdünger zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
8.  Pflanzenschutzmittel jeder Art oder Holzschutzmittel anzuwenden;
9.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
10.  Vieh weiden zu lassen und dazu erforderliche Einrichtungen zu schaffen.

(3) Über die Verbote der Absätze 1 und 2 hinaus ist im Naturwald „Pommersche Heide“ die forstwirtschaftliche Nutzung verboten.

(4) Eine ordnungsgemäße forstwirtschaftliche Bodennutzung gemäß den in § 4 des Waldgesetzes des Landes Brandenburg genannten Anforderungen und Grundsätzen bleibt auf den Waldflächen außerhalb des Naturwaldes mit der Maßgabe zulässig, dass

1.  die Holznutzung einzelstammweise bis gruppenweise mit dem Ziel der Erhaltung und Entwicklung typischer Strukturen naturnaher Kiefern-Eichenwälder erfolgt;
2.  die Waldverjüngung ausschließlich mit standortheimischen Baumarten der potenziell natürlichen Waldgesellschaft erfolgt.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen;
2.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Forstbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Schutzwaldes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Forstbehörden und sonstige von den Forstbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer und Nutzer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums.

#### § 6 Ausnahmen, Befreiungen

(1) Aus Gründen des Waldschutzes und zur Nutzung nach Naturereignissen kann die untere Forstbehörde außerhalb des Naturwaldes Ausnahmen von den Verboten dieser Verordnung zulassen, sofern der Schutzzweck nicht beeinträchtigt wird.

(2) Wenn überwiegende Gründe des Gemeinwohls es erfordern, kann die untere Forstbehörde auf Antrag Befreiungen von den Verboten dieser Verordnung gewähren.

#### § 7 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 37 Absatz 1 Nummer 7 des Waldgesetzes des Landes Brandenburg handelt, wer vorsätzlich oder fahrlässig den Verboten oder den Maßgaben des § 4 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 37 Absatz 3 des Waldgesetzes des Landes Brandenburg mit einer Geldbuße bis zu zwanzigtausend Euro geahndet werden.

#### § 8 Verhältnis zu anderen rechtlichen Bestimmungen

(1) Die Regelungen naturschutzrechtlicher Schutzgebietsausweisungen im Bereich des in § 2 genannten Gebietes bleiben unberührt.

(2) Die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) bleiben unberührt.

#### § 9 Geltendmachung von Rechtsmängeln

Eine Verletzung der in § 1 der Waldschutzgebietsverfahrensverordnung genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber der obersten Forstbehörde geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 10 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 18.
Mai 2016

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

### Anlagen

1

[Anlage (zu § 2 Absatz 2) - Kartenskizze zur Verordnung über den Schutzwald "Friedrichsthaler Eichen"](/br2/sixcms/media.php/68/GVBl_II_23_2016-Anlage.pdf "Anlage (zu § 2 Absatz 2) - Kartenskizze zur Verordnung über den Schutzwald ") 855.1 KB