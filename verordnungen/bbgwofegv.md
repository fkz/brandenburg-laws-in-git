## Verordnung über die Einkommensgrenzen bei der sozialen Wohnraumförderung im Land Brandenburg (Wohnraumförderungseinkommensgrenzenverordnung - BbgWoFEGV)

Auf Grund des § 22 Absatz 5 des Brandenburgischen Wohnraumförderungsgesetzes vom 5. Juni 2019 (GVBl. I Nr. 17) verordnet die Ministerin für Infrastruktur und Landesplanung:

#### § 1 Mietwohnraum

Bei der Förderung von Mietwohnraum und der Belegung von gefördertem Mietwohnraum dürfen zur Berücksichtigung von Haushalten mit Schwierigkeiten bei der Wohnraumversorgung sowie zur Schaffung und Erhaltung sozial stabiler Bewohnerstrukturen die in § 22 Absatz 2 bis 4 des Brandenburgischen Wohnraumförderungsgesetzes festgelegten Einkommensgrenzen nach Maßgabe der Förderbestimmungen um bis zu 20 Prozent überschritten werden.

#### § 2 Selbst genutztes Wohneigentum

Bei der Förderung von selbst genutztem Wohneigentum dürfen die in § 22 Absatz 2 bis 4 des Brandenburgischen Wohnraumförderungsgesetzes festgelegten Einkommensgrenzen nach Maßgabe der Förderbestimmungen um bis zu 100 Prozent überschritten werden.

#### § 3 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 1.
Oktober 2019 in Kraft.
Gleichzeitig tritt die Wohnraumförderungseinkommensgrenzenverordnung vom 8.
Dezember 2015 (GVBl.
II Nr.
64) außer Kraft.

Potsdam, den 6.
September 2019

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider