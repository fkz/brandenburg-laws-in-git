## Verordnung über das Naturschutzgebiet „Netzowsee-Metzelthiner Feldmark“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 8 Absatz 1 und 3 und § 42 Absatz 2 Satz 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21. Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27. Mai 2013 (GVBl.
II Nr. 43) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1   
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Uckermark wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Netzowsee-Metzelthiner Feldmark“.

### § 2   
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 1 262 Hektar.
Es umfasst sechs Teilflächen in folgenden Fluren:

Stadt:

Gemarkung:

Flur:

Templin

Densow

2, 3;

Gandenitz

1 bis 5;

Klosterwalde

4;

Metzelthin

2, 3, 5, 7 bis 9;

Templin

1, 6 bis 10, 16, 20;

Netzow

1 bis 4.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte Übersichtskarte im Maßstab 1 : 30 000 dient der räumlichen Einordnung des Naturschutzgebietes.
Die in Anlage 2 Nummer 2 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 5 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 3 mit den Blattnummern 1 bis 29 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke wird eine Flurstücksliste gemäß Absatz 4 hinterlegt.

(3) Innerhalb des Naturschutzgebietes werden die Zonen 1 und 2 mit unterschiedlichen Beschränkungen der landwirtschaftlichen Nutzung festgelegt.
Die Grenzen der Zonen sind in den in Anlage 2 Nummer 2 genannten topografischen Karten sowie in den in Anlage 2 Nummer 3 genannten Liegenschaftskarten mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten und Flurstücksliste kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Uckermark, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

### § 3   
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Buchen-, Eschen-, Ulmen-Hangwälder, der Erlen-Bruch-Sumpfwaldgesellschaften, der Schwimmblatt- und Tauchflurengesellschaften nährstoffarmer Seen, der Gesellschaften der Torfmoos-, Seggen- und Röhrichtmoore, der Staudenfluren sowie des Grünlandes frischer bis feuchter Ausprägung;
    
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Breitblättriges Knabenkraut (Dactylorhiza majalis), Steifblättriges Knabenkraut (Dactylorhiza incarnata), Sumpfsitter (Epipactis palustris), Heidenelke (Dianthus deltoides), Rundblättriger Sonnentau (Drosera rotundifolia), Sumpfiris (Iris pseudacorus), Fieberklee (Menyanthes trifoliata), Zungenhahnenfuß (Ranunculus lingua), Krebsschere (Stratiotes aloides) und Sandstrohblume (Helichrysum arenarium);
    
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Fransenfledermaus (Myotis nattereri), Fischadler (Pandion haliaetus), Schwarzstorch (Ciconia nigra), Wachtelkönig (Crex crex), Kranich (Grus grus), Rohrdommel (Botaurus stellaris), Zwergschnäpper (Ficedula parva), Schellente (Bucephala clangula), Mittelspecht (Dendrocopus medius) und Heidelerche (Lullula arborea), Teichmolch (Triturus vulgaris), Laubfrosch (Hyla arborea), Gemeine Keiljungfer (Gomphus vulgatissimus), Grüne Mosaikjungfer (Aeshna viridis), Nordische Moosjungfer (Leucorrhinia rubicunda), Gefleckte Smaragdlibelle (Somatochlora flavomaculata) und Zierliche Moosjungfer (Leucorrhinia caudalis);
    
4.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit einer unzerschnittenen und störungsarmen, eiszeitlich entstandenen Landschaft geprägt von ausgedehnten Wäldern, Stand- und Fließgewässern und artenreichem Offenland, Alleen, Gebüschen, Feld- und Obstgehölzen;
    
5.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Biotopverbundes zwischen den Lychener Gewässern und dem Templiner Seenkreuz.
    

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung

1.  eines Teiles des Europäischen Vogelschutzgebietes „Uckermärkische Seenlandschaft“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) in seiner Funktion als Lebensraum von Arten nach Anhang I der Richtlinie 79/409 EWG, insbesondere Seeadler (Haliaeetus albicilla), Fischadler (Pandion haliaetus), Schwarzstorch  
    (Ciconia nigra), Kranich (Grus grus), Rohrdommel (Botaurus stellaris), Schellente (Bucephala clangula), Schwarz- (Dryocopus martius) und Mittelspecht (Dendrocopus medius) sowie Zwergschnäpper (Ficedula parva) einschließlich ihrer Brut- und Nahrungsbiotope;
    
2.  des Gebietes von gemeinschaftlicher Bedeutung „Netzowsee-Metzelthiner Feldmark“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), das ehemals einen Teil des Gebietes von gemeinschaftlicher Bedeutung „Platkowsee-Netzowsee-Metzelthin“ umfasste, mit seinen Vorkommen von
    
    1.  Oligo- bis mesotrophen kalkhaltigen Gewässern mit benthischer Vegetation aus Armleuchteralgen, Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Flüssen der planaren bis montanen Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Pfeifengraswiesen auf kalkreichem Boden (Molinion caeruleae), Mageren Flachland-Mähwiesen \[Alopecurus pratensis (Wiesenfuchsschwanzgras), Sanguisorba officinalis (Großer Wiesenknopf)\], Übergangs- und Schwingrasenmooren, Kalkreichen Niedermooren, Hainsimsen-Buchenwald (Luzulo-Fagetum) und Waldmeister-Buchenwald (Asperulo-Fagetum) als natürliche Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes,
        
    2.  Trockenen kalkreichen Sandrasen, Schlucht- und Hangmischwäldern (Tilio-Acerion), Auen-Wäldern mit Alnus glutinosa (Schwarzerle) und Fraxinus excelsior (Gewöhnliche Esche) (Alno-Padion, Alnion incanae, Salicion albae) und Moorwäldern als prioritäre natürliche Lebensraumtypen im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes,
        
    3.  Fischotter (Lutra lutra), Biber (Castor fiber), Großem Mausohr (Myotis myotis), Bitterling (Rhodeus sericeus amarus), Steinbeißer (Cobitis taenia), Europäischer Sumpfschildkröte (Emys orbicularis), Rotbauchunke (Bombina bombina), Kamm-Molch (Triturus cristatus), Großem Feuerfalter (Lycaena dispar) und Großer Moosjungfer (Leucorrhinia pectoralis) und Vierzähniger Windelschnecke (Vertigo geyeri) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume,
        
    4.  Sumpfglanzkraut (Liparis loeselii) und Firnisglänzendem Sichelmoos (Drepanocladus vernicosus) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer Lebensräume und den für ihre Reproduktion erforderlichen Standortbedingungen.
        

### § 4   
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
    
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
    
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
    
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
    
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
    
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
    
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
    
8.  die Ruhe der Natur durch Lärm zu stören;
    
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist außerhalb von Röhrichten, Bruchwäldern, Feuchtwiesen und Mooren das Betreten zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 13 jeweils nach dem 30. Juni eines jeden Jahres;
    
10.  außerhalb der für den öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb von Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, und außerhalb der Waldbrandwundstreifen zu reiten;
    
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der für den öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
    
12.  zu baden oder zu tauchen; ausgenommen ist das Baden und Tauchen am Netzowsee vom Boot, von den bestehenden Stegen und von den gekennzeichneten Badestellen aus.
    Die betroffenen Badestellen sind in der in § 2 Absatz 2 dieser Verordnung aufgeführten Übersichtskarte, in den topografischen Karten mit den Blattnummern 2, 3, 5 und in den Liegenschaftskarten mit den Blattnummern 17, 18 und 25 eingetragen;
    
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen außerhalb der Bundeswasserstraßen zu benutzen; ausgenommen ist das Befahren des Steißsees mit muskelkraftbetriebenen Booten und des Netzowsees mit elektrisch angetriebenen Booten mit einer maximalen Leistungskraft von 600 Watt mit Genehmigung der unteren Naturschutzbehörde sowie mit muskelkraftbetriebenen Booten und Luftmatratzen jeweils außerhalb von Röhrichten und Schwimmblattgesellschaften sowie des Gleuenfließes zwischen dem Netzowsee und dem Gleuensee ausschließlich mit Einer- und Zweier-Kajaks.Das Einsetzen und Anlegen der Boote ist nur an den Stegen sowie darüber hinaus am Netzowsee an den in der in § 2 Absatz 2 aufgeführten Übersichtskarte, in den topografischen Karten mit den Blattnummen 2, 3, 5 und in den Liegenschaftskarten mit den Blattnummern 18 und 25 eingezeichneten Uferabschnitten zulässig;
    
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
    
15.  Hunde frei laufen zu lassen;
    
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
    
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle, Rückstände aus Biogasanlagen) und Sekundärrohstoffdünger (wie zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
    
18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
    
19.  Tiere zu füttern oder Futter bereitzustellen;
    
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
    
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
    
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
    
23.  Pflanzenschutzmittel jeder Art anzuwenden;
    
24.  Wiesen, Weiden oder sonstiges Grünland umzubrechen, neu anzusäen oder nachzusäen.
    

### § 5   
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Boden-nutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und vergleichbare Rückstände aus Biogasanlagen oder Sekundärrohstoffdünger (wie zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) einzusetzen,
        
    2.  auf Grünland die Verbote des § 4 Absatz 2 Nummer 23 und 24 gelten, wobei bei Narbenschäden eine umbruchlose Nachsaat von Grünland zulässig bleibt,
        
    3.  innerhalb der Zone 1 auf Grünland über die Regelungen gemäß Buchstaben a und b hinaus die Bewirtschaftung erst ab dem 16.
        Juni eines jeden Jahres gestattet ist,
        
    4.  innerhalb der Zone 2 auf Grünland über die Regelungen gemäß Buchstaben a und b hinaus § 4 Absatz 2 Nummer 17 gilt;
        
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung mit der Maßgabe, dass
    
    1.  die Nutzung der in § 3 Absatz 2 Nummer 2 genannten Waldgesellschaften einzelstamm- bis truppweise erfolgt; auf sonstigen Waldflächen sind Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von maximal 0,5 Hektar zulässig,
        
    2.  nur Baumarten der potenziell natürlichen Vegetation in lebensraumtypischen Anteilen eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
        
    3.  der Boden unter Verzicht auf Umbruch bearbeitet wird; ausgenommen ist eine streifenweise, flachgründige, nicht in den Mineralboden eingreifende Bodenverwundung zur Unterstützung von Verjüngungsmaßnahmen,
        
    4.  das Befahren des Waldes nur auf Wegen oder Rückegassen erfolgt,
        
    5.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
        
    6.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß und einer Mindesthöhe von 5 Metern nicht gefällt werden und liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimeter am stärksten Ende) im Bestand verbleibt; das stehende Totholz ist mit einer dauerhaften Markierung zu versehen,
        
    7.  § 4 Absatz 2 Nummer 17 und 23 gilt;
        
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  sich die Nutzung des Metzelthiner Haussees sowie des Kessel- und Schulzensees auf erforderliche Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne einer Fischbestandskontrolle, -regulierung und -förderung mit Zustimmung der unteren Naturschutzbehörde beschränkt.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
    2.  Besatzmaßnahmen nur im Netzowsee zulässig sind, wobei nur heimische Arten eingebracht werden dürfen und der Besatz mit Karpfen unzulässig ist,
        
    3.  Fanggeräte und Fangmittel so eingesetzt oder ausgestattet werden, dass eine Gefährdung des Fischotters und des Bibers weitgehend ausgeschlossen ist,
        
    4.  die in § 3 Absatz 2 Nummer 3 genannten Fischarten ganzjährig geschont sind;
        
4.  die rechtmäßige Ausübung der Angelfischerei am Netzowsee, am Gleuensee, am Steißsee sowie am Fienensee mit der Maßgabe, dass
    
    1.  die Nutzung nur von den Stegen, vom Boot aus sowie von den Stellen aus zulässig ist, die in der in § 2 Absatz 2 zu dieser Verordnung aufgeführten Übersichtskarte, in den topografischen Karten mit den Blattnummern 2, 3, 5 und in den Liegenschaftskarten mit den Blattnummern 17,18, 25 eingezeichnet sind,
        
    2.  § 4 Absatz 2 Nummer 13, 19 und 20 gilt;
        
5.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa) die Fallenjagd mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern vom Ufer aller innerhalb des Schutzgebietes liegender Gewässer verboten ist,
        
        bb)  keine Baujagd in einem Abstand von bis zu 100 Metern vom Ufer aller innerhalb des Schutzgebietes liegenden Gewässer vorgenommen wird.
        Ausnahmen bedürfen der Zustimmung der unteren Naturschutzbehörde,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
    3.  das Aufstellen transportabler und mobiler Ansitzeinrichtungen,
        
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und des in § 3 Absatz 2 Nummer 1 genannten Lebensraumtyps „Magere Flachland-Mähwiesen“.
        
    
    Ablenkfütterungen, die Anlage von Ansaatwiesen sowie die Anlage und Unterhaltung von Wildäckern sind unzulässig.
    Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
    
6.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege hinsichtlich der Fahrbahn und des Banketts in der Zeit vom 1.
    Juli bis zum 30.
    September eines jeden Jahres, sofern jeweils eine Beschädigung des Gehölzbestandes ausgeschlossen ist.
    Die Maßnahmen sind der unteren Naturschutzbehörde vorab anzuzeigen.
    
    Unterhaltungsmaßnahmen an Straßen und Wegen außerhalb des in Satz 1 genannten Zeitraums oder Umfangs bedürfen des Einvernehmens mit der unteren Naturschutzbehörde;
    
7.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
    
8.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
    
9.  der Betrieb und die Unterhaltung des Naturerlebnispfades am Aschbergmoor;
    
10.  Unterhaltungsmaßnahmen an rechtmäßig bestehenden Anlagen, sofern diese nicht unter die Nummern 6, 7, 8 und 9 fallen, im Einvernehmen mit der unteren Naturschutzbehörde;
    
11.  die sonstigen bei Inkrafttreten dieser Verordnung aufgrund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
    
12.  die Nutzung, Unterhaltung und Pflege der bestehenden Gärten;
    
13.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch nach dem 30.
    Juni eines jeden Jahres;
    
14.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
    
15.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der zuständigen Naturschutz-behörde zugelassen oder angeordnet worden sind, beispielsweise Maßnahmen zur Bekämpfung der Spätblühenden Traubenkirsche;
    
16.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl.
    S. 1734), die durch die Bekanntmachung vom 1.
    Oktober 2013 (ABl.
    S. 2811) geändert worden ist, an Straßen und Wegen freigestellt;
    
17.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Dies gilt nicht für das Befahren des Naturschutzgebietes im Rahmen der Angelnutzung gemäß Absatz 1 Nummer 4.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

### § 6   
Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  das Ackerland südlich und südwestlich von Knehden und südlich des Steißsees soll langfristig in extensiv genutztes Grünland umgewandelt oder zu Wald entwickelt werden;
    
2.  für den Bereich des Kleinen Griebchensees soll, insbesondere durch die Verfüllung des Entwässerungsgrabens, das hydrologische Binneneinzugsgebiet wiederhergestellt werden;
    
3.  die Wiederherstellung naturnaher Standortbedingungen für einen wachsenden Moorkörper im Moosbruch und in den Feuchtwiesen nördlich und nordwestlich von Netzow wird angestrebt;
    
4.  das Knehdenmoor soll regelmäßig gemäht werden;
    
5.  aus dem Netzowsee sollen nicht heimische Fischarten entnommen werden;
    
6.  die Umwandlung von Nadelholzforsten sowie nicht standortgerechter Laubwälder in naturnahe Laubwaldbestände wird angestrebt;
    
7.  zum Schutz vor der Zurückdrängung indigener Pflanzen sollen zum Beispiel Maßnahmen zur Bekämpfung der Spätblühenden Traubenkirsche eingeleitet werden.
    

### § 7   
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

### § 8   
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

### § 9   
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 17 und 18 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 29 Absatz 3 und § 30 des Bundesnaturschutzgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten, ihrer Lebensstätten und Biotope (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 54 Absatz 7 des Bundesnaturschutzgesetzes) unberührt.

### § 10   
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

### § 11   
Inkrafttreten, Außerkrafttreten

(1) § 5 Absatz 1 Nummer 1 tritt am 1.
Januar 2015 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig tritt der Beschluss Nummer 86/89 des Bezirkstages Neubrandenburg vom 30. März 1989 über das Naturschutzgebiet „Knehden Moor“ außer Kraft.

Potsdam, den 22.
Oktober 2014

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

* * *

### Anlagen

1

[NSGNetzowsee-Metzelthiner Feldmark-Anlg-1](/br2/sixcms/media.php/68/GVBl_II_81_2014-Anlage-1.pdf "NSGNetzowsee-Metzelthiner Feldmark-Anlg-1") 697.8 KB

2

[NSGNetzowsee-Metzelthiner Feldmark-Anlg-2](/br2/sixcms/media.php/68/GVBl_II_81_2014-Anlage-2.pdf "NSGNetzowsee-Metzelthiner Feldmark-Anlg-2") 669.4 KB