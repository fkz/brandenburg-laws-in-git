## Verordnung zur Durchführung des Waffengesetzes (Waffengesetzdurchführungsverordnung - WaffGDV)

Auf Grund des § 42 Absatz 5 Satz 4 erster Halbsatz, des § 48 Absatz 1 Satz 1, Absatz 1a und des § 55 Absatz 6 Satz 1 des Waffengesetzes vom 11.
Oktober 2002 (BGBl.
I S. 3970, 4592; 2003 I S. 1957), von denen § 42 Absatz 5 durch Gesetz vom 5.
November 2007 (BGBl.
I S.
2557) und § 48 Absatz 1a durch Artikel 1 b des Gesetzes vom 25.
November 2012 (BGBl.
II S.
1381) eingefügt worden sind, des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl.
I S.
602) verordnet die Landesregierung:

#### § 1 Zuständigkeiten

(1) Zuständige Behörde für die Durchführung des Waffengesetzes vom 11.
Oktober 2002 (BGBl. I S. 3970, 4592; 2003 I S.
1957), das zuletzt durch Artikel 1 des Gesetzes vom 30.
Juni 2017 (BGBl. I S. 2133) geändert worden ist, und der aufgrund dieses Gesetzes erlassenen Rechtsverordnungen ist das Polizeipräsidium, soweit nicht durch Bundesrecht oder in dieser Verordnung etwas anderes bestimmt ist.

(2) Zuständige Behörde im Sinne des § 15 Absatz 3 des Waffengesetzes ist die für Inneres zuständige oberste Landesbehörde.

(3) Die Geschäftsführung für die Abnahme der Prüfung der Fachkunde nach § 22 Absatz 1 Satz 1 des Waffengesetzes obliegt der Industrie- und Handelskammer Potsdam.

(4) Zuständige Kontaktstelle nach Artikel 6 Absatz 5 Satz 2 der Verordnung (EU) Nr. 1214/2011 des Europäischen Parlaments und des Rates vom 16.
November 2011 über den gewerbsmäßigen grenzüberschreitenden Straßentransport von Euro-Bargeld zwischen den Mitgliedstaaten des Euroraums (ABl. L 316 vom 29.11.2011, S.
1) ist das Polizeipräsidium.

#### § 2 Ausnahmen von der Anwendung des Waffengesetzes

Das Waffengesetz ist über die in § 55 Absatz 1 Satz 1 des Waffengesetzes genannten Stellen hinaus nicht anzuwenden auf

1.  die Fachhochschule der Polizei,
2.  den Zentraldienst der Polizei,
3.  die Gerichte,
4.  die Justizvollzugsanstalten,
5.  die Forstbehörden sowie
6.  die Gemeinsame Obere Luftfahrtbehörde Berlin-Brandenburg im Landesamt für Bauen und Verkehr

sowie deren Bedienstete, wenn sie dienstlich tätig werden, soweit nicht das Waffengesetz etwas anderes bestimmt.
Die Nichtanwendbarkeit gilt für das Landesamt für Bauen und Verkehr sowie dessen Bedienstete nur, soweit sie zur Durchführung von Aufgaben nach den §§ 2 und 8 des Luftsicherheitsgesetzes vom 11.
Januar 2005 (BGBl.
I S.
78), das zuletzt durch Artikel 1 des Gesetzes vom 23.
Februar 2017 (BGBl.
I S.
298) geändert worden ist, in Verbindung mit der Verordnung (EG) Nr.
300/2008 erforderlich ist.

#### § 3 Ermächtigung zur Einrichtung von Waffenverbotszonen

Die Befugnis zum Erlass von Rechtsverordnungen nach § 42 Absatz 5 Satz 1 und 2 des Waffengesetzes wird auf das für Inneres zuständige Mitglied der Landesregierung übertragen.

#### § 4 Bußgeldbehörden

(1) Zuständige Behörde für die Verfolgung von Ordnungswidrigkeiten nach § 53 Absatz 1 des Waffengesetzes ist das Polizeipräsidium.

(2) Zuständig für die Ahndung von Ordnungswidrigkeiten nach Absatz 1 ist der Zentraldienst der Polizei mit seiner Zentralen Bußgeldstelle.

#### § 5 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung zur Durchführung des Waffengesetzes vom 17.
Dezember 1991 (GVBl.
II S.
670), die zuletzt durch die Verordnung vom 18.
April 2008 (GVBl.
II S.
136) geändert worden ist, außer Kraft.

Potsdam, den 13.
Mai 2019

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter