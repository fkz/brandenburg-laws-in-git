## Verordnung zur Erweiterung des Gemeingebrauches an nicht schiffbaren Gewässern für Elektro-Motorboote (Brandenburgische Elektro-Motorbootverordnung - BbgEMV)

Auf Grund des § 43 Absatz 1a des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20), der durch Artikel 1 Nummer 18 des Gesetzes vom 4.
Dezember 2017 (GVBl.
I Nr.
28) eingefügt worden ist, verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Zweck und Anwendungsbereich

(1) Diese Verordnung regelt den Gemeingebrauch nach § 43 Absatz 1a des Brandenburgischen Wassergesetzes an oberirdischen stehenden, nicht schiffbaren Gewässern durch Befahren mit kleinen Fahrzeugen, die mit elektrischer Motorkraft angetrieben werden.
Als kleine Fahrzeuge zählen solche bis zu 1 500 kg Wasserverdrängung und mit einer Motorleistung bis zu einem Kilowatt.

(2) Ausgenommen von dem Gemeingebrauch nach Absatz 1 sind Gewässer im Sinne des § 43 Absatz 4 des Brandenburgischen Wassergesetzes.

#### § 2 Anforderungen

(1) Das Befahren darf nur so ausgeübt werden, wie dies der vorhandene natürliche und ausgebaute Zustand erlaubt.
Auf die Herstellung oder Aufrechterhaltung der Benutzbarkeit besteht kein Anspruch.

(2) Benutzer und Benutzerinnen müssen sich so verhalten, dass keine anderen Personen gefährdet, geschädigt oder mehr als nach den Umständen vermeidbar behindert oder belästigt werden.
Sie müssen ihr Verhalten außerdem so einrichten, dass Ufer, Anlagen und Einrichtungen im und am Gewässer nicht beschädigt oder beeinträchtigt werden.

(3) Alle Fahrzeuge haben einen Mindestabstand von einem Meter vom Ufer einzuhalten.
Die ufernahen Wasserflächen dürfen zum An- und Ablegen auf dem kürzesten Weg befahren werden.

(4) Bestände von Wasserpflanzen, wie Schilf, Rohrkolben, Binsen und Seerosen, dürfen nicht befahren werden.
Zu diesen bewachsenen Bereichen ist ein Mindestabstand von einem Meter einzuhalten.
Wasserfahrzeuge dürfen hier weder zu Wasser gelassen noch aus dem Wasser gezogen werden.

(5) An Badestellen im Sinne der Brandenburgischen Badegewässerverordnung ist das Befahren mit Wasserfahrzeugen nach § 1 Absatz 1 innerhalb der Badesaison verboten.

#### § 3 Entscheidungen der Wasserbehörde

Die zuständige Wasserbehörde kann Beschränkungen des durch diese Verordnung geregelten Gemeingebrauches gemäß § 44 des Brandenburgischen Wassergesetzes vornehmen oder einzelne Gewässer von diesem Gemeingebrauch ausnehmen.

#### § 4 Weitergehende Verbote und Ausnahmen

Verbots- und Ausnahmebestimmungen nach anderen Vorschriften, insbesondere naturschutzrechtlichen Regelungen, bleiben unberührt.

#### § 5 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 14.
Januar 2019

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger