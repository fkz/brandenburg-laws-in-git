## Verordnung über die Zulassung zu Studienplätzen in zulassungsbeschränkten Studiengängen durch die Hochschulen des Landes Brandenburg (Hochschulzulassungsverordnung - HZV)

Auf Grund des § 16 des Brandenburgischen Hochschulzulassungsgesetzes vom 1.
Juli 2015 (GVBl. I Nr. 18) verordnet die Ministerin für Wissenschaft, Forschung und Kultur:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeine Bestimmungen

[§ 1 Geltungsbereich](#1)

[§ 2 Frist und Form der Anträge, Ausschluss vom Verfahren](#2)

[§ 3 Dialogorientiertes Serviceverfahren, Registrierung, Kommunikation und Koordinierung](#3)

[§ 4 Auswahl](#4)

[§ 5 Ablauf des Vergabeverfahrens](#5)

[§ 6 Bescheide](#6)

[§ 7 Abschluss des Verfahrens](#7)

[§ 8 Losverfahren](#8)

### Abschnitt 2  
Einzelne Auswahlkriterien

[§ 9 Auswahl auf Grund früheren Zulassungsanspruchs](#9)

[§ 10 Auswahl von Deutschen nicht gleichgestellten ausländischen oder staatenlosen Bewerberinnen und Bewerbern](#10)

[§ 11 Auswahl für ein Zweitstudium](#11)

[§ 12 Grad der Qualifikation (Durchschnittsnote)](#12)

[§ 13 Auswahl nach Wartezeit](#13)

[§ 14 Auswahl bei festgestelltem besonderem öffentlichen Bedarf](#14)

[§ 15 Auswahl nach Härtegesichtspunkten](#15)

[§ 16 Auswahl in künstlerischen Studiengängen](#16)

### Abschnitt 3  
Vergabeverfahren in grundständigen Studiengängen

[§ 17 Quoten in grundständigen Studiengängen](#17)

[§ 18 Quoten in besonderen grundständigen Studiengängen](#18)

### Abschnitt 4  
Vergabeverfahren in Masterstudiengängen

[§ 19 Quoten in Masterstudiengängen](#19)

[§ 20 Quoten in besonderen Masterstudiengängen](#20)

[Anlage 1  Ermittlung der Durchschnittsnote](#22)  
[Anlage 2  Ermittlung der Messzahl bei der Auswahl für ein Zweitstudium](#23)

### Abschnitt 1  
Allgemeine Bestimmungen

#### § 1 Geltungsbereich

(1) Diese Verordnung regelt die Vergabe von Studienplätzen in zulassungsbeschränkten Studiengängen an Universitäten und Fachhochschulen, soweit nicht die Vergabe im zentralen Vergabeverfahren durch die Stiftung für Hochschulzulassung in Dortmund erfolgt.

(2) Bei der Vergabe von Studienplätzen nach dieser Verordnung nehmen die Hochschulen die Stiftung für Hochschulzulassung (Stiftung) nach Artikel 4 des Staatsvertrags über die Hochschulzulassung vom 4. April 2019 (GVBl.
I Nr.
25) gegen Erstattung der entstehenden Kosten in Anspruch.
Die für die Hochschulen zuständige oberste Landesbehörde kann in begründeten Fällen Ausnahmen zulassen.

#### § 2 Frist und Form der Anträge, Ausschluss vom Verfahren

(1) Der Zulassungsantrag für den gewählten Studiengang muss

1.  für das Sommersemester bis zum 15.
    Januar,
2.  für das Wintersemester 2021/2022 bis zum 31.
    Juli 2021 und für die folgenden Wintersemester bis zum 15.
    Juli

bei der Hochschule eingegangen sein (Ausschlussfristen).
Die Hochschulen können durch Satzung zulassen, dass mehrere gleichrangige Zulassungsanträge gestellt werden können oder dass im Zulassungsantrag neben dem gewählten Studiengang (Hauptantrag) eine Reihenfolge von weiteren Studiengangswünschen (Hilfsanträge) angegeben werden kann.
In diesem Fall legt die Hochschule die Zahl der möglichen Anträge fest.
Bewerberinnen und Bewerber für ein Zweitstudium können nur einen Zulassungsantrag und keine Hilfsanträge stellen.

(2) Die Hochschulen können durch Satzung regeln, dass Bewerberinnen und Bewerber mit einer vor dem 16.
Januar beziehungsweise 16. Juli erworbenen Hochschulzugangsberechtigung bis zum 31. Mai beziehungsweise 30.
November den Zulassungsantrag stellen müssen (Ausschlussfristen).
Bei Bewerbungen für ein Zweitstudium gilt der Zeitpunkt des Abschlusses des Erststudiums als Zeitpunkt des Erwerbs der Hochschulzugangsberechtigung nach Satz 1.
Die Hochschulen können für die Zulassung zum Masterstudium und für höhere Fachsemester durch Satzung abweichende Termine und Fristen festlegen.
Ein Antrag, mit dem ein Anspruch auf Zulassung außerhalb der festgesetzten Zulassungszahl geltend gemacht wird, muss für das Sommersemester bis zum 15.
März, für das Wintersemester 2021/2022 bis zum 1.
Oktober 2021 und für die folgenden Wintersemester bis zum 15. September bei der Hochschule eingegangen sein (Ausschlussfristen).

(3) Die Hochschulen regeln die Bewerbungsfristen für ausländische und staatenlose Bewerberinnen und Bewerber durch Satzung.

(4) Stellt eine Bewerberin oder ein Bewerber mehrere Zulassungsanträge, ohne dass dies nach Absatz 1 Satz 2 und 3 zugelassen ist, wird nur über den letzten fristgerecht eingegangenen Antrag entschieden.
Anträge, die nach dem Brandenburgischen Hochschulzulassungsgesetz oder nach dieser Verordnung ergänzend zum Zulassungsantrag gestellt werden können, sind mit dem Zulassungsantrag zu stellen.

(5) Die Hochschule bestimmt in geeigneter Weise die Form des Zulassungsantrags und der Anträge nach Absatz 4 Satz 2.
Sie bestimmt auch die Unterlagen, die den Anträgen mindestens beizufügen sind, sowie deren Form.
Sie ist nicht verpflichtet, den Sachverhalt von Amts wegen zu ermitteln; § 1 Absatz 1 Satz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg in Verbindung mit § 25 des Verwaltungsverfahrensgesetzes bleibt unberührt.
Die Hochschule kann vorsehen, dass Anträge ganz oder teilweise in elektronischer Form zu übermitteln sind.
In diesem Fall hat die Hochschule unter Anwendung von Verschlüsselungsmaßnahmen dem jeweiligen Stand der Technik entsprechende Maßnahmen zu treffen, die die Vertraulichkeit und Unversehrtheit der Daten gewährleisten.
Bewerberinnen und Bewerber, die glaubhaft machen, dass ihnen die elektronische Antragstellung nicht zumutbar ist, werden durch die Hochschule unterstützt.
Setzt der Zugang zu einem Studiengang das Bestehen einer Eignungsfeststellungsprüfung voraus, ist der Nachweis über die erfolgreich abgelegte Prüfung mit dem Zulassungsantrag vorzulegen.

(6) Der Zulassungsantrag kann nur auf eine zum Zeitpunkt der Antragstellung bereits vorliegende Berechtigung für den gewählten Studiengang (Hochschulzugangsberechtigung) gestützt werden.
Setzt die Studienaufnahme neben dem Schulabschluss oder dem Hochschulabschluss eine weitere Prüfung (Deutsche Sprachprüfung für ausländische Studienbewerberinnen und Studienbewerber – DSH und vergleichbare Prüfungen) voraus, die nicht unter Absatz 5 Satz 7 fällt, ist der Zulassungsantrag gleichwohl zulässig.
§ 9 Absatz 6 des Brandenburgischen Hochschulgesetzes bleibt unberührt.

(7) Im Zulassungsantrag ist anzugeben, ob

1.  für den gewählten Studiengang im Zeitpunkt der Antragstellung an einer deutschen Hochschule eine Einschreibung als Studentin oder Student vorliegt,
2.  an einer deutschen Hochschule ein Studium abgeschlossen worden ist oder bereits früher eine Einschreibung vorgelegen hat, gegebenenfalls für welche Zeit.

(8) Wer die Bewerbungsfristen nach Absatz 1 Satz 1 oder eine von der Hochschule durch Satzung festgesetzte Bewerbungsfrist versäumt, ist vom Vergabeverfahren ausgeschlossen.
Entspricht der Zulassungsantrag nicht den rechtlichen Mindestanforderungen oder fehlen bei Ablauf der Fristen nach den Absätzen 5 bis 7 notwendige Unterlagen oder erforderliche Angaben, gilt Satz 1 entsprechend.

#### § 3 Dialogorientiertes Serviceverfahren, Registrierung, Kommunikation und Koordinierung

(1) Das dialogorientierte Serviceverfahren (DoSV) ist ein webbasiertes System zum Abgleich von Zulassungsangeboten, das der vollständigen und schnellen Studienplatzvergabe entsprechend der Nachfrage dient.
Die Stiftung betreibt das DoSV.
Die Hochschulen sollen am DoSV teilnehmen und die Stiftung damit beauftragen, im Namen der Hochschule Zulassungsanträge entgegenzunehmen und zu prüfen sowie Bescheide (Zulassungs-, Rückstellungs- und Ablehnungsbescheide sowie Ausschlussbescheide) zu erstellen und zu versenden.
Soweit die Hochschule mit der Vergabe von Studienplätzen am DoSV teilnimmt, richtet sich die Vergabe der Studienplätze nach den §§ 2 und 4 bis 21, soweit in diesem Paragraphen nichts Anderes bestimmt ist.

(2) Für die Bewerbung um einen Studienplatz in einem Studiengang, der im DoSV koordiniert wird, muss sich die Bewerberin oder der Bewerber über das Webportal der Stiftung registrieren.
Für die Registrierung hat die Bewerberin oder der Bewerber folgende Daten anzugeben: Nachname, Vorname, Geburtsname, Geschlecht, Geburtsdatum, Geburtsort, Staatsangehörigkeit, Postanschrift, Benutzername, Passwort und eine für die Dauer des Vergabeverfahrens gültige E-Mail-Adresse.
Die Bewerberin oder der Bewerber erhält ein Benutzerkonto (DoSV-Benutzerkonto) sowie Ordnungsmerkmale, insbesondere eine Identifikationsnummer und eine Authentifizierungsnummer, die zur Identifizierung im DoSV gegenüber der Stiftung und der Hochschule anzugeben sind.
Für jede Bewerberin und jeden Bewerber ist im Vergabeverfahren nur eine Registrierung zulässig.
Im Fall mehrerer Registrierungen einer Bewerberin oder eines Bewerbers gilt die zeitlich letzte Registrierung, unter der Zulassungsanträge eingegangen sind; nur über diese Zulassungsanträge wird entschieden.

(3) Statusmitteilungen, Zulassungsangebote der Hochschulen und der Stiftung sowie Erklärungen der Bewerberinnen und Bewerber erfolgen ausschließlich über das DoSV-Benutzerkonto, soweit in dieser Verordnung nichts Anderes geregelt ist.
Die Bewerberinnen und Bewerber werden von der Stiftung durch E-Mail benachrichtigt, dass in ihrem DoSV-Benutzerkonto Änderungen eingetreten sind.
Bewerberinnen und Bewerber, die glaubhaft machen, dass ihnen die Kommunikation über die Webportale der Hochschule und der Stiftung nicht möglich ist, werden durch die Hochschule und die Stiftung unterstützt.
Die Erstellung von Bescheiden kann vollständig durch automatische Einrichtungen erfolgen.
Ein zum Abruf bereitgestellter Bescheid gilt am dritten Tag nach Absendung der elektronischen Benachrichtigung über die Bereitstellung der Daten an die abrufberechtigte Person als bekannt gegeben.
Im Zweifel hat die Hochschule den Zugang der Benachrichtigung nachzuweisen.
Zulassungsbescheide ergehen unter der Bedingung, dass die im Zulassungsantrag gemachten Angaben sowie die sonstigen Zugangs- und Einschreibevoraussetzungen spätestens bei der Immatrikulation nachgewiesen werden.

(4) Die Hochschulen und die Stiftung übermitteln sich gegenseitig die für das DoSV erforderlichen, insbesondere personenbezogenen Daten der Bewerberinnen und Bewerber um einen Studienplatz an der jeweiligen Hochschule.

(5) Für die Teilnahme am DoSV können in einem Vergabeverfahren bundesweit bis zu zwölf Zulassungsanträge gestellt werden; § 2 Absatz 1 Satz 2 und 3 bleibt unberührt.
Ein Zulassungsantrag muss elektronisch nach Maßgabe dieser Verordnung bei der Stiftung oder der Hochschule fristgerecht eingegangen sein.
Die Hochschulen übermitteln der Stiftung für das Sommersemester bis zum 20. Januar, für das Wintersemester 2021/2022 bis zum 5.
August 2021 und für die folgenden Wintersemester bis zum 20. Juli alle über das Webportal der jeweiligen Hochschule fristgerecht elektronisch eingegangenen Zulassungsanträge.
Überzählige Zulassungsanträge werden im DoSV-Benutzerkonto als „inaktiv“ gekennzeichnet.
Für im DoSV-Benutzerkonto als „inaktiv“ gekennzeichnete Zulassungsanträge können weder Zulassungsangebote noch Zulassungen ergehen.
Die Bewerberin oder der Bewerber kann einen oder mehrere der bisher als „inaktiv“ gekennzeichneten Zulassungsanträge aktivieren, indem sie oder er bisher nicht als „inaktiv“ gekennzeichnete Zulassungsanträge in entsprechender Anzahl für das Sommersemester bis zum 22. Januar, für das Wintersemester 2021/2022 bis zum 7.
August 2021 und für die folgenden Wintersemester bis zum 22. Juli zurücknimmt (Ausschlussfristen).

(6) Die Bewerberin oder der Bewerber kann eine Präferenzenfolge der Zulassungsanträge festlegen.
Legt die Bewerberin oder der Bewerber keine Präferenzenfolge der Zulassungsanträge fest, ergibt sich diese aus der zeitlichen Reihenfolge des elektronischen Eingangs des Zulassungsantrags; dem zeitlich zuerst elektronisch eingegangenen Zulassungsantrag kommt dabei die höchste Präferenz zu.
Die Bewerberin oder der Bewerber kann die Präferenzenfolge der Zulassungsanträge ändern.

(7) Die Ranglisten sind, soweit nichts Anderes in dieser Verordnung geregelt ist, für das Sommersemester bis zum 15.
Februar, für das Wintersemester 2021/2022 bis zum 31. August 2021 und für die folgenden Wintersemester bis zum 15.
August im DoSV freizugeben.

(8) Wer ein Zulassungsangebot annimmt, erhält eine Zulassung und einen Zulassungsbescheid.
Mit der Annahme eines Zulassungsangebots gelten die weiteren gestellten Zulassungsanträge als zurückgenommen und die Bewerberin oder der Bewerber scheidet aus diesen Vergabeverfahren aus.
Auf diese Rechtsfolgen ist die Bewerberin oder der Bewerber von der Stiftung hinzuweisen.
Wieder verfügbare Studienplätze werden gemäß den Ranglisten aufrückenden Bewerberinnen und Bewerbern angeboten.

(9) Die Koordinierung der Zulassungsanträge erfolgt für das Sommersemester in der Zeit vom 23. Januar bis zum 21.
Februar, für das Wintersemester 2021/2022 in der Zeit vom 8. August 2021 bis zum 6.
September 2021 und für die folgenden Wintersemester in der Zeit vom 23. Juli bis zum 21. August nach den folgenden Regeln:

1.  hat die Bewerberin oder der Bewerber nur einen Zulassungsantrag gestellt und liegt für diesen ein Zulassungsangebot vor, erfolgt eine Zulassung und es wird ein Zulassungsbescheid erteilt,
2.  hat die Bewerberin oder der Bewerber mehrere Zulassungsanträge gestellt und liegt für jeden Zulassungsantrag ein Zulassungsangebot vor, erfolgt für das Zulassungsangebot mit der höchsten Präferenz die Zulassung; Absatz 8 Satz 2 bis 4 gilt entsprechend,
3.  hat die Bewerberin oder der Bewerber mehrere Zulassungsanträge gestellt und liegen für mindestens zwei, aber nicht für alle Zulassungsanträge Zulassungsangebote vor, bleibt das Zulassungsangebot mit der höchsten Präferenz erhalten; für jedes nachrangige Zulassungsangebot gilt der entsprechende Zulassungsantrag als zurückgenommen.

Über ein neues Zulassungsangebot wird die Bewerberin oder der Bewerber gemäß Absatz 3 benachrichtigt.
Für das Sommersemester am 22.
Februar, für das Wintersemester 2021/2022 am 7. September 2021 und für die folgenden Wintersemester am 22. August erfolgt für die Zulassungsmöglichkeit mit der höchsten Präferenz die Zulassung und es wird ein Zulassungsbescheid erteilt; Absatz 8 Satz 2 bis 4 gilt entsprechend; für alle Zulassungsanträge höherer Präferenz werden Ablehnungsbescheide erteilt.
Erhält eine Bewerberin oder ein Bewerber keine Zulassung, wird für jeden Zulassungsantrag ein Ablehnungsbescheid erteilt.

(10) Nach Abschluss der Koordinierungsphase für das Sommersemester vom 28. Februar bis 31. März, für das Wintersemester 2021/2022 vom 13.
September 2021 bis 30.
September 2021 und für die folgenden Wintersemester vom 28.
August bis 30.
September rücken Bewerberinnen und Bewerber, die keine Zulassung erhalten haben, innerhalb der Ranglisten fortlaufend auf im DoSV noch verfügbare Studienplätze auf, soweit sie ihre weitere Teilnahme am Verfahren gegenüber der Stiftung erklärt haben und soweit die Hochschule an diesem Verfahren teilnimmt; eine Teilzulassung gilt nicht als Zulassung nach Halbsatz 1.
Die Erklärung der Teilnahme kann für das Sommersemester in der Zeit vom 25.
Februar bis 27.
Februar, für das Wintersemester 2021/2022 vom 10.
September 2021 bis 12.
September 2021 und für die folgenden Wintersemester in der Zeit vom 25.
August bis 27. August abgegeben werden (Ausschlussfristen).
Auf die Folgen der Nichtteilnahme ist die Bewerberin oder der Bewerber hinzuweisen.
Sind die Ranglisten erschöpft, werden noch verfügbare Studienplätze auch an Bewerberinnen und Bewerber, die bisher noch nicht am DoSV teilgenommen haben, für das Sommersemester vom 25.
Februar bis 31.
März, für das Wintersemester 2021/2022 vom 10. September 2021 bis 30.
September 2021 und für die folgenden Wintersemester vom 25. August bis 30. September durch Los vergeben.
Absatz 2 bis 4 (Registrierung) und Absatz 5 Satz 1 Halbsatz 1 finden Anwendung.
Der Zulassungsantrag von Bewerberinnen oder Bewerbern nach Satz 4 muss elektronisch über das Webportal der Stiftung innerhalb des dort genannten Zeitraums eingegangen sein.
Besteht eine Zulassungsmöglichkeit, erhält die Bewerberin oder der Bewerber einen Zulassungsbescheid; Ablehnungsbescheide werden nicht erteilt.
Ist das Verfahren nach den Sätzen 1 bis 7 in einem Studiengang beendet und sind noch Studienplätze verfügbar oder werden wieder verfügbar, führt die Hochschule ein Losverfahren nach § 8 durch.

(11) Die Bewerberin oder der Bewerber kann ein Zulassungsangebot oder eine Zulassung wegen eines Dienstes im Sinne des Artikels 8 Absatz 3 des Staatsvertrags über die Hochschulzulassung und § 9 Absatz 1 zurückstellen lassen.
Es wird ein Rückstellungsbescheid erteilt.
Ein Anspruch auf Einschreibung im laufenden Vergabeverfahren besteht nicht; ein Zulassungsbescheid gilt insoweit als widerrufen.
Durch Rückstellung wieder verfügbare Studienplätze werden nach dem jeweiligen Stand der Vergabeverfahren gemäß den Absätzen 8 bis 10 vergeben.

(12) Die Fristen nach Absatz 5 Satz 6 und Absatz 10 Satz 2 und 4 sind Ausschlussfristen.
Fällt das Ende einer Ausschlussfrist auf einen Sonnabend, Sonntag oder gesetzlichen Feiertag, so endet die Frist mit dem Ablauf des entsprechenden Tags und verlängert sich nicht bis zum Ablauf des nächstfolgenden Werktags.

#### § 4 Auswahl

Übersteigt die Zahl der Bewerberinnen und Bewerber die Gesamtzahl der Studienplätze in dem betreffenden Studiengang, erfolgt die Auswahl nach den Vorschriften der §§ 5 bis 20.

#### § 5 Ablauf des Vergabeverfahrens

(1) Zunächst wird im Hauptverfahren über den im Zulassungsantrag gestellten Hauptantrag entschieden.
Die danach noch verfügbaren Studienplätze werden in Nachrückverfahren vergeben.
Hierbei wird auch über die Hilfsanträge entschieden.
An Nachrückverfahren nimmt teil, wer bis zu diesem Zeitpunkt in keinem der beantragten Studiengänge zugelassen ist.

(2) Wer in einer oder mehreren nach den §§ 17 bis 20 zu bildenden Quoten zu berücksichtigen ist, wird auf allen entsprechenden Ranglisten geführt.

(3) Bei der Auswahl werden die Ranglisten in folgender Reihenfolge berücksichtigt:

1.  Auswahl nach einem Dienst auf Grund früheren Zulassungsanspruchs sowie Auswahl auf Grund der Angehörigkeit zum Bundeskader eines Bundessportfachverbandes des Deutschen Olympischen Sportbundes,
2.  Auswahl der ausländischen und staatenlosen Bewerberinnen und Bewerber, die Deutschen nicht gleichgestellt sind,
3.  Auswahl für ein Zweitstudium,
4.  Auswahl nach dem Ergebnis des Hochschulauswahlverfahrens,
5.  Auswahl nach Wartezeit,
6.  Auswahl innerhalb der Profilquote,
7.  Auswahl nach Härtegesichtspunkten,
8.  Auswahl auf Grund weiterer Quoten in der in den §§ 18 und 20 genannten Reihenfolge.

(4) Fordert die Hochschule bisher nicht zugelassene Bewerberinnen und Bewerber zu einer Erklärung auf, ob sie im Fall der Zulassung in einem Nachrückverfahren die Einschreibung für den betreffenden Studiengang beantragen werden, ist die Erklärung bis zu dem von der Hochschule zu bestimmenden Termin abzugeben.
Wer sich innerhalb dieser Frist nicht erklärt oder erklärt, dass auf eine Teilnahme an einem Nachrückverfahren verzichtet wird, nimmt am weiteren Verfahren nicht mehr teil.

(5) Die Hochschule kann die voraussichtliche Nichtannahme von Studienplätzen durch Überbuchung der Zulassungszahlen berücksichtigen.

(6) In Nachrückverfahren gelten die Absätze 2 bis 5 entsprechend mit der Maßgabe, dass zunächst nur Bewerberinnen und Bewerber berücksichtigt werden, die den Studiengang im Hauptantrag genannt haben.
Danach noch verfügbare Plätze werden in der sich aus den Benennungen ergebenden Reihenfolge an die Bewerberinnen und Bewerber vergeben, die den Studiengang in einem Hilfsantrag genannt haben.
Die Vergabe erfolgt nach der in Absatz 3 Nummer 1 und 2 und 4 bis 8 angegebenen Reihenfolge.

#### § 6 Bescheide

(1) Die Hochschule teilt unverzüglich die Entscheidung über den Zulassungsantrag mit.
Der Bescheid ist mit einer Rechtsbehelfsbelehrung zu versehen.

(2) Im Zulassungsbescheid bestimmt die Hochschule einen Termin, bis zu dem gegenüber der Hochschule zu erklären ist, ob der Studienplatz angenommen wird.
Liegt die Erklärung der Hochschule bis zu diesem Termin nicht vor, wird der Zulassungsbescheid unwirksam.
Lehnt die Hochschule die Einschreibung ab, weil übrige Voraussetzungen für die Aufnahme als Studentin oder Student nicht vorliegen, wird der Zulassungsbescheid ebenfalls unwirksam.

(3) Beruht die Zulassung auf falschen Angaben der Bewerberin oder des Bewerbers, nimmt die Hochschule den Zulassungsbescheid zurück.
Ist die Zulassung sonst fehlerhaft, kann die Hochschule den Zulassungsbescheid zurücknehmen; nach Ablauf eines Jahres ist die Rücknahme des Zulassungsbescheides ausgeschlossen.

#### § 7 Abschluss des Verfahrens

(1) Das Vergabeverfahren ist abgeschlossen, wenn die Nachrücklisten erschöpft sind oder alle verfügbaren Studienplätze durch Einschreibung besetzt sind.

(2) Die Hochschule soll das Vergabeverfahren für abgeschlossen erklären, wenn ein weiteres Nachrückverfahren wegen der fortgeschrittenen Vorlesungszeit nicht mehr sinnvoll erscheint.

#### § 8 Losverfahren

(1) Sind nach Abschluss des Vergabeverfahrens in einem Studiengang noch Studienplätze verfügbar oder werden Studienplätze wieder verfügbar, werden diese von der Hochschule an deutsche und ausländische Bewerberinnen und Bewerber vergeben, die für das Sommersemester bis zum 1. April und für das Wintersemester bis zum 1.
Oktober die Zulassung schriftlich beantragt haben.
Ist das Vergabeverfahren in einem Studiengang vor diesem Zeitpunkt abgeschlossen, kann die Hochschule eine frühere Frist bestimmen.
Die Frist ist in geeigneter Weise bekannt zu geben.
Über die Zulassung dieser Bewerberinnen und Bewerber entscheidet das Los.

(2) Das Ergebnis der Vergabe der Studienplätze ist von den Hochschulen in geeigneter Weise bekannt zu geben.

### Abschnitt 2  
Einzelne Auswahlkriterien

#### § 9 Auswahl auf Grund früheren Zulassungsanspruchs

(1) Bewerberinnen und Bewerber, die

1.  eine Dienstpflicht nach Artikel 12a des Grundgesetzes erfüllt oder eine solche Dienstpflicht oder entsprechende Dienstleistungen auf Zeit übernommen haben bis zur Dauer von drei Jahren,
2.  einen freiwilligen Wehrdienst nach dem Wehrpflichtgesetz in der Fassung der Bekanntmachung vom 15.
    August 2011 (BGBI.
    I S. 1730) geleistet haben,
3.  einen Bundesfreiwilligendienst nach dem Bundesfreiwilligendienstgesetz vom 28. April 2011 (BGBI.
    I S. 687) geleistet haben,
4.  mindestens zwei Jahre Entwicklungsdienst nach dem Entwicklungshelfer-Gesetz vom 18. Juni 1969 (BGBl. I S. 549) geleistet haben,
5.  einen Jugendfreiwilligendienst im Sinne des Jugendfreiwilligendienstegesetzes vom 16. Mai 2008 (BGBI.
    I S. 842) oder im Rahmen eines von der Bundesregierung geförderten Modellprojekts geleistet haben; § 15 Absatz 2 des Jugendfreiwilligendienstegesetzes gilt entsprechend,
6.  ein Kind unter 18 Jahren oder eine pflegebedürftige Person aus dem Kreis der sonstigen Angehörigen bis zur Dauer von drei Jahren betreut oder gepflegt haben,

(Dienst) werden in dem im Antrag genannten Studiengang auf Grund früheren Zulassungsanspruchs ausgewählt, wenn sie zu Beginn oder während eines Dienstes für diesen Studiengang zugelassen worden sind oder wenn zu Beginn oder während eines Dienstes für diesen Studiengang keine Zulassungszahlen festgesetzt waren.
Der von einem oder einer gemäß § 9 Absatz 1 Satz 3 des Brandenburgischen Hochschulgesetzes Deutschen gleichgestellten ausländischen Staatsangehörigen oder Staatenlosen geleistete Dienst steht einem Dienst nach Satz 1 gleich, wenn er diesem gleichwertig ist.

(2) Die Auswahl nach Absatz 1 Satz 1 muss spätestens zum zweiten Vergabeverfahren beantragt werden, das nach Beendigung des Dienstes durchgeführt wird.
Ist der Dienst noch nicht beendet, ist durch Bescheinigung glaubhaft zu machen, dass der Dienst bei einer Bewerbung für das Sommersemester bis zum 30.
April oder bei einer Bewerbung für das Wintersemester bis zum 31. Oktober beendet sein wird.

(3) Wird die Festlegung einer Rangfolge zwischen den nach einem Dienst auf Grund früheren Zulassungsanspruchs Auszuwählenden erforderlich, entscheidet das Los.

(4) Wer auf Grund einer gerichtlichen Entscheidung zuzulassen ist, die sich auf ein bereits abgeschlossenes Vergabeverfahren bezieht, ist wie ein vorweg nach einem Dienst auf Grund früheren Zulassungsanspruchs Auszuwählender zu behandeln.

#### § 10 Auswahl von Deutschen nicht gleichgestellten ausländischen oder staatenlosen Bewerberinnen und Bewerbern

Besondere Umstände nach § 5 Absatz 2 Satz 2 und 3 des Brandenburgischen Hochschulzulassungsgesetzes sind in der Regel bei der Auswahl ausländischer und staatenloser Bewerberinnen und Bewerber, die Deutschen nicht gleichgestellt sind, angemessen einzubeziehen.

#### § 11 Auswahl für ein Zweitstudium

(1) Wer bereits ein Studium in einem anderen Studiengang an einer deutschen Hochschule abgeschlossen hat und sich für ein weiteres grundständiges Studium bewirbt (Bewerberinnen und Bewerber für ein Zweitstudium), kann nur im Rahmen der Quote nach § 17 Absatz 1 Nummer 3 ausgewählt werden.

(2) Die Rangfolge wird durch eine Messzahl bestimmt, die aus der Abschlussnote des ersten Hochschulabschlusses und dem Grad der Bedeutung der Gründe für das Zweitstudium ermittelt wird.
Die Einzelheiten zur Ermittlung der Messzahl ergeben sich aus der Anlage 2.

#### § 12 Grad der Qualifikation (Durchschnittsnote)<br>

Die Einzelheiten zur Ermittlung und zum Nachweis der Durchschnittsnote ergeben sich aus der Anlage 1.

#### § 13 Auswahl nach Wartezeit

(1) Die Rangfolge wird durch die Zahl der seit dem Erwerb der Hochschulzugangsberechtigung für das angestrebte grundständige Studium gemäß § 9 Absatz 2 des Brandenburgischen Hochschulgesetzes verstrichenen Halbjahre bestimmt.
Es zählen nur volle Halbjahre vom Zeitpunkt des Erwerbs der Hochschulzugangsberechtigung bis zum Beginn des Semesters, für das die Zulassung beantragt wird.
Halbjahre sind die Zeit vom 1.
April bis zum 30.
September eines Jahres (Sommersemester) und die Zeit vom 1.
Oktober eines Jahres bis zum 31.
März des folgenden Jahres (Wintersemester).

(2) Für einen angestrebten Masterstudiengang gilt für die Rangfolge Absatz 1 seit dem Erwerb der Zugangsvoraussetzungen gemäß § 9 Absatz 2 Nummer 5 und Absatz 5 des Brandenburgischen Hochschulgesetzes entsprechend.

#### § 14 Auswahl bei festgestelltem besonderem öffentlichen Bedarf

Die Vorabquote zur Auswahl für eine einen Beruf vorbereitende Studiengangswahl oder Studienfach-Kombination kommt nur zur Anwendung, wenn die für die Hochschulen zuständige oberste Landesbehörde den besonderen öffentlichen Bedarf festgestellt und im Amtsblatt für Brandenburg bekannt gemacht hat.

#### § 15 Auswahl nach Härtegesichtspunkten

Die Studienplätze der Quote nach § 17 Absatz 1 Nummer 2 und § 19 Absatz 1 Nummer 2 werden auf Antrag nach dem Grad der außergewöhnlichen Härte vergeben.
Die Hochschulen können zu Einzelheiten des Verfahrens und zur Bestimmung des Grads der außergewöhnlichen Härte eine Satzung erlassen.

#### § 16 Auswahl in künstlerischen Studiengängen

In künstlerischen Studiengängen gemäß § 9 Absatz 3 des Brandenburgischen Hochschulzulassungsgesetzes kann die Hochschule durch Satzung regeln, dass die Auswahlentscheidung überwiegend oder ausschließlich nach dem Ergebnis der Eignungsprüfung getroffen wird.
Die Satzung legt dabei die Kriterien fest, nach denen das Ergebnis der Eignungsprüfung bei der Ranglistenbildung zu berücksichtigen ist.

### Abschnitt 3  
Vergabeverfahren in grundständigen Studiengängen

#### § 17 Quoten in grundständigen Studiengängen

(1) Von der Gesamtzahl der festgesetzten Zulassungszahl je Studiengang, vermindert um die Zahl der nach einem Dienst auf Grund eines früheren Zulassungsanspruchs gemäß § 9 dieser Verordnung und § 12 des Brandenburgischen Hochschulzulassungsgesetzes Auszuwählenden und der auf Grund ihrer Angehörigkeit zum Bundeskader eines Bundessportfachverbandes des Deutschen Olympischen Sportbundes gemäß § 12a des Brandenburgischen Hochschulzulassungsgesetzes Auszuwählenden, werden folgende Vorabquoten festgesetzt:

1.  11 Prozent für die Zulassung von ausländischen und staatenlosen Bewerberinnen und Bewerbern, die Deutschen nicht gleichgestellt sind,
2.  3 Prozent für Fälle außergewöhnlicher Härte,
3.  3 Prozent für die Auswahl für ein Zweitstudium.
4.  Die Hochschulen können durch Satzung eine Quote von einem Prozent, mindestens einen Studienplatz, für Bewerberinnen und Bewerber festlegen, die einem im öffentlichen Interesse zu berücksichtigenden oder zu fördernden Personenkreis angehören und auf Grund begründeter Umstände an den Studienort gebunden sind (Profilquote).
    Die Kriterien für die Auswahl innerhalb der Profilquote regeln die Hochschulen durch Satzung.

(2) Bei der Berechnung der Quoten nach Absatz 1 wird gerundet.
Für jede Quote muss mindestens ein Studienplatz zur Verfügung stehen, wenn für die entsprechende Quote zu berücksichtigende Bewerbungen vorliegen.

(3) Die Hochschulen können durch Satzung bestimmen, dass die Anzahl der Studienplätze nach Absatz 1 Nummer 3 nur nach dem in § 4 Absatz 2 des Brandenburgischen Hochschulzulassungsgesetzes vorgesehenen Verhältnis vergeben werden.

#### § 18 Quoten in besonderen grundständigen Studiengängen

(1) Ist für eine Studiengangswahl oder Studienfach-Kombination, die auf einen Beruf vorbereitet, ein besonderer öffentlicher Bedarf gemäß § 14 festgestellt worden, werden je 2 Prozent der Studienplätze eines zulassungsbeschränkten Studiengangs Studienbewerberinnen und Studienbewerbern vorbehalten, deren Studiengangswahl oder Studienfach-Kombination dem festgestellten Bedarf entspricht.
Je Kombinationsmöglichkeit ist eine Rangliste zu erstellen.

(2) In international ausgerichteten Studiengängen kann die Hochschule durch Satzung eine von § 17 Absatz 1 Nummer 1 abweichende Quote festsetzen, wenn dieser Studiengang im Rahmen eines Programms von einer Institution gefördert wird oder Vereinbarungen mit ausländischen Hochschulen bestehen.
Die Quote darf 50 Prozent nicht übersteigen.
Soweit die Voraussetzungen nach Satz 1 zweiter Halbsatz nicht vorliegen, kann eine abweichende Quote festgesetzt werden, die 15 Prozent nicht übersteigen darf.

(3) In Studiengängen, die eine Hochschule gemeinsam mit einer anderen Hochschule betreibt, kann durch Satzung vorgesehen werden, dass bis zu 50 Prozent der Studienplätze durch die andere Hochschule vergeben werden.
Die Quoten nach § 17 Absatz 1 können entsprechend reduziert werden.

(4) Auf der Grundlage von Vereinbarungen zur Durchführung des Studiums in besonderen Studienformen innerhalb eines grundständigen Studiengangs (berufsbegleitend oder dual) können die Hochschulen durch Satzung für diesen Personenkreis besondere Quoten festlegen.

### Abschnitt 4  
Vergabeverfahren in Masterstudiengängen

#### § 19 Quoten in Masterstudiengängen

(1) Von der Gesamtzahl der festgesetzten Zulassungszahl je Studiengang, vermindert um die Zahl der nach einem Dienst auf Grund eines früheren Zulassungsanspruchs gemäß § 9 dieser Verordnung und § 12 des Brandenburgischen Hochschulzulassungsgesetzes Auszuwählenden und der auf Grund ihrer Angehörigkeit zum Bundeskader eines Bundessportfachverbandes des Deutschen Olympischen Sportbundes gemäß § 12a des Brandenburgischen Hochschulzulassungsgesetzes Auszuwählenden, werden folgende Vorabquoten festgesetzt:

1.  11 Prozent für die Zulassung von ausländischen und staatenlosen Bewerberinnen und Bewerbern, die Deutschen nicht gleichgestellt sind,
2.  3 Prozent für Fälle außergewöhnlicher Härte.
3.  Die Hochschulen können durch Satzung eine Quote von einem Prozent, mindestens einen Studienplatz, für Bewerberinnen und Bewerber festlegen, die einem im öffentlichen Interesse zu berücksichtigenden oder zu fördernden Personenkreis angehören und auf Grund begründeter Umstände an den Studienort gebunden sind (Profilquote).
    Die Kriterien für die Auswahl innerhalb der Profilquote regeln die Hochschulen durch Satzung.

(2) Bei der Berechnung der Quoten nach Absatz 1 wird gerundet.
Für jede Quote muss mindestens ein Studienplatz zur Verfügung stehen, wenn für die entsprechende Quote zu berücksichtigende Bewerbungen vorliegen.

(3) Die nach Abzug der gemäß § 9 dieser Verordnung und § 12 des Brandenburgischen Hochschulzulassungsgesetzes Auszuwählenden sowie nach Abzug der gemäß § 12a des Brandenburgischen Hochschulzulassungsgesetzes Auszuwählenden und nach Abzug der Quoten nach Absatz 1 und § 20 verbleibenden Studienplätze werden zu 90 Prozent im Ergebnis eines Hochschulauswahlverfahrens vergeben.

#### § 20 Quoten in besonderen Masterstudiengängen

(1) Ist für eine Studiengangswahl oder Studienfach-Kombination, die auf einen Beruf vorbereitet, ein besonderer öffentlicher Bedarf gemäß § 14 festgestellt worden, werden je 2 Prozent der Studienplätze eines zulassungsbeschränkten Studiengangs Studienbewerberinnen und Studienbewerbern vorbehalten, deren Studiengangswahl oder Studienfach-Kombination dem festgestellten Bedarf entspricht.
Je Kombinationsmöglichkeit ist eine Rangliste zu erstellen.

(2) In international ausgerichteten Studiengängen kann die Hochschule durch Satzung eine von § 19 Absatz 1 Nummer 1 abweichende Quote festsetzen.
Die Quote soll 50 Prozent nicht übersteigen.

(3) In Studiengängen, die eine Hochschule gemeinsam mit einer anderen Hochschule betreibt, kann durch Satzung vorgesehen werden, dass bis zu 50 Prozent der Studienplätze durch die andere Hochschule vergeben werden.
Die Quoten nach § 19 Absatz 1 können entsprechend reduziert werden.

(4) Auf der Grundlage von Vereinbarungen zur Durchführung des Studiums in besonderen Studienformen (berufsbegleitend oder dual) können die Hochschulen durch Satzung für diesen Personenkreis besondere Quoten festlegen.

Potsdam, den 17.
Februar 2016

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Prof.
Dr.-Ing.
Dr.
Sabine Kunst

* * *

**Anlage 1**  
(zu § 12)

### Ermittlung der Durchschnittsnote

(1) Bei Hochschulzugangsberechtigungen auf der Grundlage der

*   „Vereinbarung zur Gestaltung der gymnasialen Oberstufe und der Abiturprüfung“ gemäß Beschluss der Kultusministerkonferenz vom 7.
    Juli 1972 in der jeweils geltenden Fassung (Beschluss-Sammlung der Kultusministerkonferenz Nummer 176),
*   „Vereinbarung über die Abiturprüfung für Nichtschülerinnen und Nichtschüler entsprechend der Gestaltung der gymnasialen Oberstufe in der Sekundarstufe II“ gemäß Beschluss der Kultusministerkonferenz vom 13.
    September 1974 in der jeweils geltenden Fassung (Beschluss-Sammlung der Kultusministerkonferenz Nummer 192.2),
*   „Vereinbarung über die Durchführung der Abiturprüfung für Schülerinnen und Schüler an Waldorfschulen“ gemäß Beschluss der Kultusministerkonferenz vom 21. Februar 1980 in der jeweils geltenden Fassung (Beschluss-Sammlung der Kultusministerkonferenz Nummer 485.2),
*   „Vereinbarung zur Gestaltung der Abendgymnasien“ gemäß Beschluss der Kultusministerkonferenz vom 21.
    Juni 1979 in der jeweils geltenden Fassung (Beschluss-Sammlung der Kultusministerkonferenz Nummer 240.2),
*   „Vereinbarung zur Gestaltung der Kollegs“ gemäß Beschluss der Kultusministerkonferenz vom 21.
    Juni 1979 in der jeweils geltenden Fassung (Beschluss-Sammlung der Kultusministerkonferenz Nummer 248.1),

die eine auf eine Stelle nach dem Komma bestimmte Durchschnittsnote enthalten, wird diese zugrunde gelegt.
Enthält die Hochschulzugangsberechtigung keine Durchschnittsnote nach Satz 1, aber eine Punktzahl der Gesamtqualifikation, wird nach Anlage 4 der „Vereinbarung zur Gestaltung der gymnasialen Oberstufe und der Abiturprüfung“ gemäß Beschluss der Kultusministerkonferenz vom 7. Juli 1972 in der jeweils geltenden Fassung (Beschluss-Sammlung der Kultusministerkonferenz Nummer 176) die Durchschnittsnote aus der Punktzahl der Gesamtqualifikation errechnet.
Die Durchschnittsnote wird auf eine Stelle nach dem Komma errechnet; es wird nicht gerundet.

(2) Bei Hochschulzugangsberechtigungen auf der Grundlage

*   der „Vereinbarung über Abendgymnasien“ gemäß Beschluss der Kultusministerkonferenz vom 3.
    Oktober 1957 in der Fassung vom 8.
    Oktober 1970 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 240),
*   des Beschlusses der Kultusministerkonferenz vom 8.
    Juli 1965 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 248) über die „Institute zur Erlangung der Hochschulreife (‚Kollegs‘)“

wird die Durchschnittsnote aus dem arithmetischen Mittel der Noten der Hochschulzugangsberechtigung mit Ausnahme der Noten für die Fächer, die in der Hochschulzugangsberechtigung oder einer besonderen Bescheinigung als vorzeitig abgeschlossen ausgewiesen sind, gebildet.
Absatz 3 Satz 2 Nummer 1 bis 6 und 9 findet Anwendung.
Ist die Durchschnittsnote nicht von der Schule ausgewiesen, wird sie nach Satz 1 und 2 errechnet.

(3) Bei Hochschulzugangsberechtigungen auf der Grundlage der

1.  „Vereinbarung über die befristete gegenseitige Anerkennung von Zeugnissen der fachgebundenen Hochschulreife, die an zur Zeit bestehenden Schulen, Schulformen beziehungsweise -typen erworben worden sind“ gemäß Beschluss der Kultusministerkonferenz vom 25.
    November 1976 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 226.2) und vom 16.
    Februar 1978 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 226.2.1),
2.  „Sondervereinbarung über die gegenseitige Anerkennung der Zeugnisse von besonderen gymnasialen Schulformen, die zu einer allgemeinen Hochschulreife führen“ gemäß Beschluss der Kultusministerkonferenz vom 25.
    November 1976 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 226.1),
3.  „Rahmenvereinbarung über die Berufsoberschule“ gemäß Beschluss der Kultusministerkonferenz vom 25.
    November 1976 in der jeweils geltenden Fassung (Beschluss-Sammlung der Kultusministerkonferenz Nummer 470)

finden die Absätze 1 und 2 entsprechende Anwendung.
Dabei wird eine Durchschnittsnote aus dem arithmetischen Mittel wie folgt gebildet:

1.  weist die Hochschulzugangsberechtigung eine Note für das Fach Gemeinschaftskunde aus, werden die Noten für die Fächer Geschichte, Erdkunde, Sozialkunde und Philosophie sowie für sonstige Fächer, die in der Hochschulzugangsberechtigung als zu dem Fach Gemeinschaftskunde gehörig ausgewiesen sind, nicht gewertet;
2.  weist die Hochschulzugangsberechtigung keine Note für das Fach Gemeinschaftskunde aus, ist diese aus dem arithmetischen Mittel der Noten für die Fächer Geschichte, Erdkunde, Sozialkunde und Philosophie oder für die Fächer, die in der Hochschulzugangsberechtigung als zu dem Fach Gemeinschaftskunde gehörig ausgewiesen sind, zu bilden; dabei ist bei der Bildung der Note für das Fach Gemeinschaftskunde nach Halbsatz 1 eine im Zeugnis ausgewiesene Note für das Fach Wirtschaftsgeographie beziehungsweise Geographie mit Wirtschaftsgeographie einzubeziehen;
3.  ist in der Hochschulzugangsberechtigung eine Note für das Fach Geschichte mit Gemeinschaftskunde ausgewiesen, gilt diese Note als Note für das Fach Geschichte und als Note für das Fach Sozialkunde;
4.  bei der Bildung der Note für das Fach Gemeinschaftskunde wird gerundet;
5.  ist in der Hochschulzugangsberechtigung neben den Noten für die Fächer Biologie, Chemie und Physik eine Gesamtnote für den naturwissenschaftlichen Bereich ausgewiesen, bleibt diese bei der Errechnung der Durchschnittsnote außer Betracht;
6.  Noten für die Fächer Religionslehre, Ethik, Kunsterziehung, Musik und Sport bleiben außer Betracht, es sei denn, dass die Zulassung zu einem entsprechenden Studiengang beantragt wird;
7.  Noten für die Fächer Kunsterziehung, Musik und Sport werden gewertet, soweit sie Kernpflichtfächer waren;
8.  Noten für zusätzliche Unterrichtsveranstaltungen und für Arbeitsgemeinschaften bleiben unberücksichtigt;
9.  die Durchschnittsnote wird auf eine Stelle nach dem Komma errechnet; es wird nicht gerundet.

(4) Bei Hochschulzugangsberechtigungen, die auf dem Gebiet der Bundesrepublik Deutschland nach dem Stand bis zum 3.
Oktober 1990 an einer in eine Hochschule übergeleiteten Bildungseinrichtung erworben wurden, ist eine Durchschnittsnote von der Hochschule in dem Zeugnis oder einer besonderen Bescheinigung auszuweisen.
Die Durchschnittsnote wird auf eine Stelle nach dem Komma errechnet; es wird nicht gerundet.

(5) Bei sonstigen Hochschulzugangsberechtigungen, die auf dem Gebiet der Bundesrepublik Deutschland nach dem Stand bis zum 3.
Oktober 1990 erworben wurden und eine Durchschnittsnote enthalten, die auf eine Stelle nach dem Komma bestimmt ist, wird diese zugrunde gelegt.

(6) Bei sonstigen Hochschulzugangsberechtigungen, die auf dem Gebiet der Bundesrepublik Deutschland nach dem Stand bis zum 3.
Oktober 1990 erworben wurden und nur Einzelnoten im Rahmen eines sechsstufigen Notensystems enthalten, wird eine Durchschnittsnote unter entsprechender Anwendung des Absatzes 3 Satz 2 Nummer 1 bis 6 und 9 aus dem arithmetischen Mittel der Noten gebildet; Noten für gegebenenfalls im 11.
und 12.
Schuljahr abgeschlossene Fächer sowie Noten für zusätzliche Unterrichtsveranstaltungen und für Arbeitsgemeinschaften bleiben unberücksichtigt.

(7) Bei sonstigen Hochschulzugangsberechtigungen, die auf dem Gebiet der Bundesrepublik Deutschland nach dem Stand bis zum 3.
Oktober 1990 erworben wurden und weder eine Durchschnittsnote, die auf eine Stelle nach dem Komma bestimmt ist, noch Einzelnoten im Rahmen eines sechsstufigen Notensystems enthalten, ist eine Durchschnittsnote durch eine besondere Bescheinigung nachzuweisen, die von der für die Abnahme der entsprechenden Prüfung zuständigen Stelle oder von der obersten Landesbehörde auszustellen ist, unter deren Aufsicht diese Prüfung durchgeführt worden ist.
Bei der Bestimmung der Durchschnittsnote sind einzelne Prüfungsleistungen, die der Hochschulzugangsberechtigung zugrunde liegen, zur Beurteilung heranzuziehen.
Die Durchschnittsnote wird auf eine Stelle nach dem Komma bestimmt; es wird nicht gerundet.

(8) Bei Hochschulzugangsberechtigungen aus der ehemaligen Deutschen Demokratischen Republik, die nach dem Beschluss der Kultusministerkonferenz vom 10. Mai 1990 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 908) zur Aufnahme eines Studiums in der Bundesrepublik Deutschland berechtigen, wird die Durchschnittsnote nach dem Beschluss der Kultusministerkonferenz vom 8.
Juli 1987 in der Fassung vom 8.
Oktober 1990 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 289.1) errechnet.
Bei Hochschulzugangsberechtigungen aus den in Artikel 3 des Einigungsvertrages genannten Ländern, die nach dem Beschluss der Kultusministerkonferenz vom 21.
Februar 1992 in der Fassung vom 12.
März 1993 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 234) und vom 25.
Februar 1994 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 234.1) zur Aufnahme eines Studiums in der Bundesrepublik Deutschland berechtigen, wird die Durchschnittsnote nach dem Beschluss der Kultusministerkonferenz vom 21.
Februar 1992 in der Fassung vom 9.
Juni 1993 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 235) errechnet.
Die Durchschnittsnote wird jeweils von der für die Ausstellung des Zeugnisses zuständigen Stelle auf eine Stelle nach dem Komma errechnet; es wird nicht gerundet.
Es wird die auf dem Zeugnis oder in einer besonderen Bescheinigung ausgewiesene Durchschnittsnote zugrunde gelegt.

(9) Bei ausländischen Vorbildungsnachweisen wird die Gesamtnote, wenn keine Bescheinigung der Zeugnisanerkennungsstelle eines Landes über die Festsetzung einer Gesamtnote vorliegt, auf der Grundlage der „Vereinbarung über die Festsetzung der Gesamtnote bei ausländischen Hochschulzugangszeugnissen“ vom 15.
März 1991 in der jeweils geltenden Fassung (Beschluss-Sammlung der Kultusministerkonferenz Nummer 289.5) berechnet.

(10) Bei Hochschulzugangsberechtigungen, die bis einschließlich 1986 aufgrund einer Abschlussprüfung unter dem Vorsitz einer oder eines Prüfungsbeauftragten der Kultusministerkonferenz an deutschen Schulen im Ausland (ausgenommen die Schulen mit neugestalteter gymnasialer Oberstufe) erworben wurden, ist die Durchschnittsnote durch eine Bescheinigung der oder des Prüfungsbeauftragten nachzuweisen.
Dasselbe gilt weiterhin für die Zeugnisse der deutschen Reifeprüfungen, die am Lyzeum Alpinum in Zuoz und am Institut auf dem Rosenberg in St.
Gallen erworben wurden.
Die Durchschnittsnote wird auf eine Stelle nach dem Komma bestimmt; es wird nicht gerundet.
Bei Hochschulzugangsberechtigungen, die ab 1987 aufgrund einer Abschlussprüfung unter dem Vorsitz einer oder eines Prüfungsbeauftragten der Kultusministerkonferenz an deutschen Schulen im Ausland erworben wurden, wird die auf dem Zeugnis ausgewiesene, auf eine Stelle nach dem Komma bestimmte Durchschnittsnote zugrunde gelegt.
Bei Hochschulzugangsberechtigungen, die ab 1998 aufgrund einer Abschlussprüfung unter der Leitung einer oder eines Beauftragten der Kultusministerkonferenz an Deutschen Schulen im Ausland erworben wurden, werden die auf dem Zeugnis ausgewiesene, auf eine Stelle nach dem Komma bestimmte Durchschnittsnote sowie die ausgewiesene Punktzahl des Gesamtergebnisses zugrunde gelegt.

(11) Bei Hochschulzugangsberechtigungen, die an den deutsch-französischen Gymnasien ab dem Abiturtermin 1982 erworben wurden, wird der in den Zeugnissen gemäß Artikel 30 des Abkommens zwischen der Regierung der Bundesrepublik Deutschland und der Regierung der Französischen Republik vom 10.
Februar 1972 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 90) ausgewiesene „allgemeine Notendurchschnitt“ bei der Rangplatzbestimmung zugrunde gelegt.
Für die Umrechnung des „allgemeinen Notendurchschnitts“ wird der für die Europäischen Schulen geltende Umrechnungsschlüssel gemäß Beschluss der Kultusministerkonferenz vom 8. Dezember 1975 in der jeweils geltenden Fassung (Beschluss-Sammlung der Kultusministerkonferenz Nummer 289.2) angewendet.
Bei Absolventinnen und Absolventen der deutsch-französischen Gymnasien in Freiburg und Saarbrücken werden für das Abitur 1982 und 1983 die bis 1981 geltenden Richtlinien angewendet, sofern durch die Neuregelung im Einzelfall eine Verschlechterung der Durchschnittsnote eintritt.
Die nach diesem Verfahren umgerechnete allgemeine Durchschnittsnote wird zusätzlich zum „allgemeinen Notendurchschnitt“ im „Zeugnis über das Bestehen des deutsch-französischen Abiturs“ ausgewiesen und durch den Stempelzusatz „Durchschnittsnote gemäß Staatsvertrag über die Vergabe von Studienplätzen“ gekennzeichnet.
Bei Hochschulzugangsberechtigungen, die an den deutsch-französischen Gymnasien ab dem Abiturtermin 2014 erworben wurden, wird der in den Zeugnissen gemäß Artikel 30 des Abkommens zwischen der Regierung der Bundesrepublik Deutschland und der Regierung der Französischen Republik vom 10.
Februar 1972 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 90) ausgewiesene „allgemeine Notendurchschnitt“ bei der Rangplatzbestimmung zugrunde gelegt.
Für die Umrechnung des „allgemeinen Notendurchschnitts“ wird das „Berechnungsverfahren zur Ermittlung der ‚Punktzahl des Gesamtergebnisses (E)‘ und der ‚Abiturdurchschnittsnote (N)‘ für die Deutsch-Französischen Gymnasien“ gemäß Beschluss der Kultusministerkonferenz vom 5.
Juni 2014 (Beschlusssammlung der Kultusministerkonferenz Nummer 290) angewendet.
Die nach diesem Verfahren ermittelte „Punktzahl des Gesamtergebnisses“ wird als „Punktzahl der Gesamtqualifikation“ und „Abiturdurchschnittsnote“ zusätzlich zum „allgemeinen Notendurchschnitt“ im „Zeugnis über das Bestehen des deutsch-französischen Abiturs“ ausgewiesen.

(12) Bei Hochschulzugangsberechtigungen, die in Bildungsgängen in der Französischen Republik erworben wurden, die auf den gleichzeitigen Erwerb des Baccalauréat und der Allgemeinen Hochschulreife vorbereiten („Abibac“), wird die Durchschnittsnote der Bescheinigung zugrunde gelegt, die von der oder dem Prüfungsbeauftragten der Ständigen Konferenz der Kultusminister der Länder in der Bundesrepublik Deutschland gemäß der „Verwaltungsabsprache zwischen dem Bevollmächtigten der Bundesrepublik Deutschland für kulturelle Angelegenheiten im Rahmen des Vertrags über die deutsch-französische Zusammenarbeit und dem Minister für Erziehung, Hochschulwesen und Forschung der Französischen Republik über die Organisation des Bildungsgangs, die Gestaltung der Lehrpläne und die Prüfungsordnung zum gleichzeitigen Erwerb der deutschen Allgemeinen Hochschulreife und des französischen Baccalauréat“ vom 11.
Mai 2006 ausgewiesen wird.

(13) Bei Hochschulzugangsberechtigungen, die an den Deutschen Abteilungen französischer Internationaler Schulen (Lycées Internationaux) erworben wurden, bei denen das Baccalauréat mit dem deutschen Prüfungsteil „option international“ abgelegt wurde, wird die Durchschnittsnote auf der Grundlage der „Vereinbarung über die Berechnung der Durchschnittsnoten für die an den Deutschen Abteilungen französischer Schulen (Lycées internationaux) erworbenen Hochschulzugangsberechtigungen deutscher Staatsbürger“ gemäß Beschluss der Kultusministerkonferenz vom 13.
April 1988 in der jeweils geltenden Fassung (Beschluss-Sammlung der Kultusministerkonferenz Nummer 289.4) nachgewiesen.
Die nach diesen Verfahren ermittelte Durchschnittsnote wird durch eine Bescheinigung einer oder eines Prüfungsbeauftragten der Ständigen Konferenz der Kultusminister der Länder in der Bundesrepublik Deutschland nachgewiesen.

(14) Bei Hochschulzugangsberechtigungen, die an den Europäischen Schulen erworben wurden, wird die Europäische Abiturdurchschnittsnote bei der Rangplatzbestimmung zugrunde gelegt.
Für die Umrechnung der Europäischen Durchschnittsnote bis zum Abitur 2020 wird der „Umrechnungsschlüssel zur Bewertung der an Europäischen Schulen erworbenen Reifezeugnissen bei der zentralen Vergabe von Studienplätzen“ gemäß Beschluss der Kultusministerkonferenz vom 8. Dezember 1975 in der jeweils geltenden Fassung (Beschluss-Sammlung der Kultusministerkonferenz Nummer 289.2) angewendet.
Die Durchschnittsnote wird auf eine Stelle nach dem Komma ausgewiesen; die Umrechnung wird von der deutschen Inspektorin oder dem deutschen Inspektor für die Europäischen Schulen (Sekundarbereich) oder in seiner beziehungsweise ihrer Vertretung von dazu beauftragten Lehrkräften an den Europäischen Schulen bescheinigt.
Für die Umrechnung der Europäischen Abiturdurchschnittsnote in eine deutsche Abiturdurchschnittsnote ab dem Abitur 2021 werden die „Richtlinien zur Behandlung und Bewertung des Europäischen Abiturzeugnisses und von an offiziellen Europäischen Schulen und an akkreditierten Europäischen Schulen erbrachten Einzelleistungen“ gemäß Beschluss der Kultusministerkonferenz vom 14. Juni 2018 angewendet.
Die Umrechnung erfolgt in die deutsche Dezimalnote sowie die erreichte Punktzahl nach der „Vereinbarung zur Gestaltung der gymnasialen Oberstufe und der Abiturprüfung“ gemäß Beschluss der Kultusministerkonferenz vom 7.
Juli 1972 in der jeweils geltenden Fassung (Beschluss-Sammlung der Kultusministerkonferenz Nummer 176).
Die Durchschnittsnote wird nicht auf- oder abgerundet und auf eine Dezimalstelle gebildet.
Die Umrechnung wird von der deutschen Inspektorin oder dem deutschen Inspektor für die Europäischen Schulen (Sekundarbereich) oder in seiner beziehungsweise ihrer Vertretung von dazu beauftragten Lehrkräften an den Europäischen Schulen bescheinigt.

(15) Bei Hochschulzugangsberechtigungen, die nach den Bestimmungen der/des „International Baccalaureate Organisation/Office du Baccalauréat International“ erworben wurden, wird die Durchschnittsnote auf der Grundlage der Vereinbarung über die Anerkennung des „International Baccalaureate Diploma/Diplôme du Baccalauréat International“ gemäß Beschluss der Kultusministerkonferenz vom 10.
März 1986 in der jeweils geltenden Fassung (Beschluss-Sammlung der Kultusministerkonferenz Nummer 283) berechnet.

**Anlage 2**  
(zu § 11 Absatz 2)

### Ermittlung der Messzahl bei der Auswahl für ein Zweitstudium

(1) Die Messzahl ist die Summe der Punktzahlen, die für das Ergebnis der Abschlussprüfung des Erststudiums und für den Grad der Bedeutung der Gründe für das Zweitstudium vergeben werden.

(2) Für das Ergebnis der Abschlussprüfung des Erststudiums werden folgende Punktzahlen vergeben:

*   Noten „ausgezeichnet“ und „sehr gut“                    —            4 Punkte;
*   Noten „gut“ und „voll befriedigend“                         —            3 Punkte;
*   Note „befriedigend“                                              —            2 Punkte;
*   Note „ausreichend“                                              —            1 Punkt.

Ist die Note der Abschlussprüfung des Erststudiums nicht nachgewiesen, wird das Ergebnis der Abschlussprüfung mit 1 Punkt bewertet.

(3) Nach dem Grad der Bedeutung der Gründe für das Zweitstudium werden folgende Punktzahlen vergeben:

1.  „zwingende berufliche Gründe“                             —             9 Punkte;

zwingende berufliche Gründe liegen vor, wenn ein Beruf angestrebt wird, der nur aufgrund zweier abgeschlossener Studiengänge ausgeübt werden kann;

2.  „wissenschaftliche Gründe“                                 —             7 bis 11 Punkte;

wissenschaftliche Gründe liegen vor, wenn im Hinblick auf eine spätere Tätigkeit in Wissenschaft und Forschung auf der Grundlage der bisherigen wissenschaftlichen und praktischen Tätigkeit eine weitere wissenschaftliche Qualifikation in einem anderen Studiengang angestrebt wird;

3.  „besondere berufliche Gründe“                            —              7 Punkte;

besondere berufliche Gründe liegen vor, wenn die berufliche Situation dadurch erheblich verbessert wird, dass der Abschluss des Zweitstudiums das Erststudium sinnvoll ergänzt.
Dies ist der Fall, wenn die durch das Zweitstudium in Verbindung mit dem Erststudium angestrebte Tätigkeit als Kombination zweier studiengangspezifischer Tätigkeitsfelder anzusehen ist, die im Regelfall nicht bereits von Absolventinnen und Absolventen einer der beiden Studiengänge wahrgenommen werden kann, und die oder der Betroffene nachweisbar diese Tätigkeit anstrebt;

4.  „sonstige berufliche Gründe“                               —             4 Punkte;

sonstige berufliche Gründe liegen vor, wenn das Zweitstudium aufgrund der individuellen beruflichen Situation aus sonstigen Gründen, insbesondere zum Ausgleich eines unbilligen beruflichen Nachteils oder um die Einsatzmöglichkeiten der mithilfe des Erststudiums ausgeübten Tätigkeit zu erweitern, erforderlich ist;

5.  „keiner der vorgenannten Gründe“                       —              1 Punkt.

Liegen wissenschaftliche Gründe vor, ist die Punktzahl innerhalb des Rahmens von 7 bis 11 Punkten davon abhängig, welches Gewicht die Gründe haben, welche Leistungen bisher erbracht worden sind und in welchem Maß die Gründe von allgemeinem Interesse sind.
Wird das Zweitstudium nach einer Familienphase zum Zwecke der Wiedereingliederung oder des Neueinstiegs in das Berufsleben angestrebt, kann dieser Umstand unabhängig von der Bewertung des Vorhabens und seiner Zuordnung zu einer der vorgenannten Fallgruppen durch Gewährung eines Zuschlags von bis zu 2 Punkten bei der Messzahlbildung berücksichtigt werden.