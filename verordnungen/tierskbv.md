## Verordnung über die Erhebung von Tierseuchenkassenbeiträgen (Tierseuchenkassenbeitragsverordnung - TierskBV)

Auf Grund des § 9 Absatz 1 des Gesetzes zur Ausführung des Tiergesundheitsgesetzes in der Fassung der Bekanntmachung vom 17. Dezember 2001 (GVBl.
I S. 14), der durch Artikel 1 Nummer 11 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 31) geändert worden ist, verordnet die Ministerin für Soziales, Gesundheit, Integration und Verbraucherschutz:

#### § 1 Beiträge

Die von den Tierhalterinnen und -haltern jährlich zu erhebenden Beiträge werden wie folgt festgelegt:

Tierarten

Betrag in Euro

**1**

**Rinder (einschließlich Kälber)  
**  
je Tier

  
  
2,00

**2**

**Schweine (einschließlich Schwarzwild, das in Gehegen zum Zweck der Gewinnung von Fleisch für den menschlichen Verzehr gehalten wird)**

2.1

je Schwein über 30 kg Lebendmasse in Stallhaltung (Zuchtsauen und sonstige Zucht- und Mastschweine)

0,60

2.2

je Schwein über 30 kg Lebendmasse in Freilandhaltung und bei Schwarzwild in Gehegen

1,80

**3**

**Pferde (einschließlich Fohlen)  
**  
je Tier

  
  
3,00

**4**

**Schafe (einschließlich Muffelwild, das in Gehegen zum Zweck der Gewinnung von Fleisch für den menschlichen Verzehr gehalten wird)  
**  
Tiere, die älter als neun Monate sind,

4.1

in Beständen mit ein bis fünf Tieren je Bestand

10,00

4.2

in Beständen mit sechs und mehr Tieren zusätzlich ab sechstem Tier je Tier

0,85

**5**

**Ziegen  
**  
Tiere, die älter als neun Monate sind,

5.1

in Beständen mit ein bis fünf Tieren je Bestand

10,00

5.2

in Beständen mit sechs und mehr Tieren zusätzlich ab sechstem Tier je Tier

0,85

**6**

**Geflügel**

6.1

Laufvögel je Tier

2,50

6.2

Geflügel in Junghennenaufzuchtbeständen für Legehennenbetriebe zum Zweck der Konsumeierproduktion ab 250 Tiere je Tier

0,17

6.3

Gallus-gallus-Zuchttiere in Zuchtbeständen ab 250 Zuchttiere je Tier

0,18

6.4

Putenelterntiere in Zuchtbeständen ab 250 Tiere je Tier

0,21

6.5

Enten je Tier

0,08

6.6

Gänse je Tier

0,08

6.7

Truthühner, die nicht unter Nummer 6.4 fallen, je Tier

0,12

6.8

Geflügel, das nicht unter die Nummern 6.1 bis 6.7 fällt, je Tier

0,06

Mindestbeitrag pro Bestand

5,00

**7**

**Wildklauentiere (außer Schwarz- und Muffelwild), die in Gehegen zum Zweck der Gewinnung von Fleisch für den menschlichen Verzehr gehalten werden (Gehegewild)  
**  
je Tier

  
  
  
0,60

#### § 2 Aussetzen der Beitragserhebung

Abweichend von § 1 kann die Tierseuchenkasse im Einvernehmen mit der für die Tierseuchenbekämpfung zuständigen obersten Landesbehörde die Erhebung von Beiträgen für bestimmte Tierarten zeitlich begrenzt aussetzen, wenn die Rücklagenhöhe für die betreffende Tierart den in § 3 der Verordnung zur Durchführung des Gesetzes zur Ausführung des Tiergesundheitsgesetzes jeweils festgelegten Betrag überschritten hat.

#### § 3 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 1. Januar 2022 in Kraft.
Gleichzeitig tritt die Tierseuchenkassenbeitragsverordnung vom 27.
November 2017 (GVBl.
II Nr.
64), die zuletzt durch die Verordnung vom 12. November 2018 (GVBl.
II Nr.
80) geändert worden ist, außer Kraft.

Potsdam, den 30.
November 2021

Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

Ursula Nonnemacher