## Verordnung über die einheitliche Durchführung von Raumordnungsverfahren im gemeinsamen Planungsraum Berlin-Brandenburg (Gemeinsame Raumordnungsverfahrensverordnung - GROVerfV)

Auf Grund des Artikels 16 Absatz 4 des Landesplanungsvertrages in der Fassung der Bekanntmachung vom 10.
Februar 2008 (GVBl.
I S. 42) in Verbindung mit Artikel 1 des Gesetzes zu dem Staatsvertrag der Länder Berlin und Brandenburg über das Landesentwicklungsprogramm 2007 (LEPro 2007) und die Änderung des Landesplanungsvertrages vom 18.
Dezember 2007 (GVBl.
I S. 325) verordnet der Minister für Infrastruktur und Landwirtschaft im Einvernehmen mit der Senatorin für Stadtentwicklung des Landes Berlin:

#### § 1  
Gegenstand des Raumordnungsverfahrens

(1) Im Raumordnungsverfahren prüft die Gemeinsame Landesplanungsabteilung die Raumverträglichkeit raumbedeutsamer Planungen und Maßnahmen im Sinne von Artikel 16 Absatz 1 des Landesplanungsvertrages.
Dabei prüft sie die raumbedeutsamen Auswirkungen der Planung oder Maßnahme unter überörtlichen Gesichtspunkten, die Übereinstimmung mit den Erfordernissen der Raumordnung und die Abstimmung mit anderen raumbedeutsamen Planungen und Maßnahmen (Raumverträglichkeitsprüfung).

(2) Bei Planungen und Maßnahmen, für die

1.  nach den §§ 6 bis 14 des Gesetzes über die Umweltverträglichkeitsprüfung oder
2.  nach § 3 des Brandenburgischen Gesetzes über die Umweltverträglichkeitsprüfung

eine Umweltverträglichkeitsprüfung durchzuführen ist, schließt das Raumordnungsverfahren die  
Ermittlung, Beschreibung und Bewertung der raumbedeutsamen Auswirkungen der Planung oder  
Maßnahme auf die Schutzgüter einschließlich ihrer Wechselwirkungen nach § 2 Absatz 1 des  
Gesetzes über die Umweltverträglichkeitsprüfung nach dem Planungsstand ein.
Von einer raumordnerischen Umweltverträglichkeitsprüfung kann abgesehen werden, wenn auf Grund einer Einzelfallprüfung nach § 7 des Gesetzes über die Umweltverträglichkeitsprüfung festgestellt wird, dass die Planung oder Maßnahme voraussichtlich keine erheblichen nachteiligen Auswirkungen auf die Umwelt haben kann.

(3) Im Raumordnungsverfahren ist auch zu prüfen, ob die Planung oder Maßnahme geeignet ist, ein Gebiet von gemeinschaftlicher Bedeutung oder ein europäisches Vogelschutzgebiet in seinen für die Erhaltungsziele oder den Schutzzweck maßgeblichen Bestandteilen erheblich zu beeinträchtigen.
Können derartige Beeinträchtigungen nicht ausgeschlossen werden, sind die raumbedeutsamen Auswirkungen der Planung oder Maßnahme auf die Erhaltungsziele und den Schutzzweck der Gebiete nach dem Planungsstand zu ermitteln, zu beschreiben und zu bewerten (raumordnerische Prüfung nach der Fauna-Flora-Habitat-Richtlinie).

(4) Gegenstand der Prüfungen nach den Absätzen 1 bis 3 sollen auch ernsthaft in Betracht kommende Standort- oder Trassenalternativen sein.

#### § 2  
Vorbereitung

(1) Die Gemeinsame Landesplanungsabteilung entscheidet auf Antrag des Trägers der raumbedeutsamen Planung oder Maßnahme oder von Amts wegen innerhalb einer Frist von vier Wochen nach Vorlage der Antragsunterlagen über das Erfordernis, ein Raumordnungsverfahren durchzuführen.
Die Antragsunterlagen müssen eine allgemein verständliche Projektbeschreibung und eine kartografische Darstellung der raumbedeutsamen Planung oder Maßnahme enthalten.
Ein Anspruch auf die Einleitung eines Raumordnungsverfahrens besteht nicht.

(2) Sieht die Gemeinsame Landesplanungsabteilung unter den Voraussetzungen des Artikels 16 Absatz 2 des Landesplanungsvertrages von der Durchführung eines Raumordnungsverfahrens ab, unterrichtet sie den Träger der Planung oder Maßnahme und die in die Vorbereitung des Raumordnungsverfahrens einbezogenen Stellen.

(3) Ist die Durchführung eines Raumordnungsverfahrens erforderlich, führt die Gemeinsame Landesplanungsabteilung eine Antragskonferenz durch.
Sie bezieht dabei den Träger der raumbedeutsamen Planung oder Maßnahme, die Zulassungsbehörde und weitere in ihrem Aufgabenbereich berührte öffentliche Stellen sowie bei sachlicher und räumlicher Betroffenheit auch die vom Bund oder Land anerkannten Vereinigungen mit dem Ziel der Förderung des Natur- und Umweltschutzes ein.
In der Antragskonferenz sollen Gegenstand des Raumordnungsverfahrens, Methode und Untersuchungsrahmen für die Prüfungen nach § 1 sowie Inhalt und Umfang der voraussichtlich vorzulegenden Verfahrensunterlagen erörtert und festgelegt werden.

(4) Zur Vorbereitung auf die Antragskonferenz leitet die Gemeinsame Landesplanungsabteilung den Beteiligten eine vom Träger der Planung oder Maßnahme erstellte Unterlage zu, die eine Kurzbeschreibung des Vorhabens mit übersichtlicher kartografischer Darstellung sowie Vorschläge zur inhaltlichen Ausgestaltung und räumlichen Abgrenzung des voraussichtlichen Untersuchungsrahmens enthält.

(5) Das Ergebnis der Antragskonferenz, insbesondere zum Untersuchungsrahmen sowie zum Inhalt und Umfang der voraussichtlich vorzulegenden Unterlagen, teilt die Gemeinsame Landesplanungsabteilung dem Träger der raumbedeutsamen Planung oder Maßnahme und allen übrigen an der Antragskonferenz Beteiligten schriftlich mit.

#### § 3  
Verfahrensunterlagen

(1) Der Träger der raumbedeutsamen Planung oder Maßnahme legt der Gemeinsamen Landesplanungsabteilung die Verfahrensunterlagen vor, die zur Bewertung der raumbedeutsamen Auswirkungen des Vorhabens notwendig sind.
Die Unterlagen müssen mindestens folgende Angaben enthalten:

1.  Allgemeine Angaben:
    
    1.  Beschreibung der Planung oder Maßnahme nach Art und Umfang, des Zeitrahmens, der Angaben über Standort, Trasse oder Fläche und Bedarf an Grund und Boden, einschließlich der ernsthaft in Betracht kommenden Standort- oder Trassenalternativen unter Angabe der wesentlichen Auswahlgründe,
        
    2.  allgemein verständliche kartografische Darstellung in der Regel im Maßstab 1 : 10 000 und einen Übersichtsplan im Maßstab 1 : 25 000 oder 1 : 50 000;
        
2.  Angaben für die Raumverträglichkeitsprüfung:
    
    1.  Beschreibung der nach dem Planungsstand zu erwartenden raumbedeutsamen Auswirkungen der Planung oder Maßnahme auf die Erfordernisse der Raumordnung; dazu können gehören: Auswirkungen auf Zentrale Orte, die Entwicklung des Gesamtraums Berlin-Brandenburg, auf Siedlungs- und Freiraum, Verkehr, Wirtschaft, Land- und Forstwirtschaft, Ver- und Entsorgung, technische Infrastruktur, Erholung und  
        Tourismus, Rohstoffabbau und Lagerstätten, Hochwasserschutz, Altlasten und Konversion, Verteidigung sowie auf Belange des Katastrophenschutzes, insbesondere bei Planungen und Maßnahmen im Anwendungsbereich der Richtlinie 96/82/EG, jeweils untergliedert in Bestand und Auswirkungen,
        
    2.  Beschreibung von Maßnahmen, mit denen Beeinträchtigungen von Raumordnungsbelangen sowie der Verbrauch natürlicher Ressourcen so gering wie möglich gehalten werden können,
        
    3.  Anforderungen an die soziale und technische Infrastruktur (Verkehr, Energie, Wasser, Abwasser, Abfall) mit Beschreibung notwendiger Aus- und Neubaumaßnahmen;
        
3.  Angaben für die raumordnerische Umweltverträglichkeitsprüfung:
    
    1.  Beschreibung der Umwelt und ihrer Bestandteile im Einwirkungsbereich der Planung oder Maßnahme sowie Beschreibung der nach dem Planungsstand zu erwartenden erheblichen nachteiligen Auswirkungen auf die in § 2 Absatz 1 des Gesetzes über die Umweltverträglichkeitsprüfung genannten Schutzgüter einschließlich ihrer Wechselwirkungen in der Regel anhand von Bestandsdaten; dabei ist zwischen bau-, anlagen- und betriebsbedingten Umweltauswirkungen zu unterscheiden,
        
    2.  Beschreibung der Maßnahmen, mit denen nach dem Planungsstand zu erwartende, erhebliche nachteilige Umweltauswirkungen der Planung oder Maßnahme vermieden, vermindert oder soweit möglich, ausgeglichen werden können, sowie der Ersatzmaßnahmen bei nicht ausgleichbaren, aber vorrangigen Eingriffen in Natur und Landschaft,
        
    3.  Beschreibung der wichtigsten, vom Träger der Planung oder Maßnahme geprüften anderweitigen Lösungsmöglichkeiten und Angabe der wesentlichen Auswahlgründe im Hinblick auf die Umweltauswirkungen der Planung oder Maßnahme;
        
4.  Angaben für die raumordnerische Prüfung nach der Fauna-Flora-Habitat-Richtlinie:
    
    1.  Beschreibung der Erhaltungsziele und Schutzzwecke des Gebietes von gemeinschaftlicher Bedeutung oder eines europäischen Vogelschutzgebietes, das von der Planung oder Maßnahme einschließlich ernsthaft in Betracht kommender Standort- oder Trassenalternativen erheblich beeinträchtigt werden kann, in der Regel anhand vorhandener Unterlagen,
        
    2.  Beschreibung der raumbedeutsamen Auswirkungen der Planung oder Maßnahme auf die für die Erhaltungsziele oder den Schutzzweck maßgeblichen Bestandteile der unter Buchstabe a genannten Gebiete und Einschätzung der Erheblichkeit möglicher Beeinträchtigungen,
        
    3.  Beschreibung von Maßnahmen zur Vermeidung oder Verminderung erheblicher Beeinträchtigungen;
        
5.  allgemein verständliche Zusammenfassung der in den Nummern 2 bis 4 genannten Angaben.
    

(2) Bei raumbedeutsamen Planungen und Maßnahmen der Verteidigung entscheidet die nach § 15 Absatz 2 Satz 2 des Raumordnungsgesetzes zuständige Stelle über Art und Umfang der Angaben.

#### § 4  
Einleitung und Durchführung

(1) Liegen der Gemeinsamen Landesplanungsabteilung die nach § 3 erforderlichen Unterlagen vollständig vor, leitet sie das Verfahren mit der schriftlichen Unterrichtung des Trägers der raumbedeutsamen Planung oder Maßnahme ein.
Die Gemeinsame Landesplanungsabteilung übergibt den Beteiligten die Verfahrensunterlagen und fordert sie auf, dazu innerhalb einer angemessenen Frist schriftlich Stellung zu nehmen.

(2) Bei raumbedeutsamen Planungen und Maßnahmen von öffentlichen Stellen des Bundes entscheidet die Gemeinsame Landesplanungsabteilung im Benehmen mit den in § 15 Absatz 5 des Raumordnungsgesetzes genannten Stellen über die Einleitung eines Raumordnungsverfahrens.

(3) Ergibt sich insbesondere bei wesentlichen Änderungen ein Bedarf nach weitergehenden oder zusätzlichen Informationen über die raumbedeutsame Planung oder Maßnahme und ihre Auswirkungen, fordert die Gemeinsame Landesplanungsabteilung den Träger der Planung oder Maßnahme zur Vorlage entsprechend geänderter oder zusätzlicher Planungsunterlagen innerhalb angemessener Frist auf.
Innerhalb dieser Frist ruht das Verfahren.

#### § 5  
Beteiligung

(1) Im Raumordnungsverfahren sind alle von der Planung oder Maßnahme in ihrem fachlichen oder räumlichen Aufgabenbereich berührten öffentlichen Stellen im Sinne von § 3 Absatz 1 Nummer 5 des Raumordnungsgesetzes sowie die vom Bund oder Land anerkannten Vereinigungen mit dem Ziel der Förderung des Natur- und Umweltschutzes zu beteiligen.
Die Gemeinsame Landesplanungsabteilung entscheidet im Einzelfall, ob weitere Stellen beteiligt werden sollen.

(2) Ist ein den gemeinsamen Planungsraum Berlin-Brandenburg überschreitendes Beteiligungsverfahren durchzuführen, legt die Gemeinsame Landesplanungsabteilung das Vorgehen im Benehmen mit der zuständigen Landesplanungsbehörde des inländischen Nachbarlandes fest.
Bei raumbedeutsamen Planungen oder Maßnahmen, die erhebliche Auswirkungen auf das Gebiet eines ausländischen Nachbarstaates haben können, erfolgt die Beteiligung nach § 15 Absatz 3 Satz 5 des Raumordnungsgesetzes.
Kann die raumbedeutsame Planung oder Maßnahme, für die eine Umweltverträglichkeitsprüfung durchzuführen ist, erhebliche Umweltauswirkungen auf das Gebiet eines ausländischen Nachbarstaates haben, ist dieser nach den §§ 54 bis 56 des Gesetzes über die Umweltverträglichkeitsprüfung sowie die Republik Polen darüber hinaus nach dem Vertragsgesetz zur Deutsch-Polnischen UVP-Vereinbarung zu beteiligen.

(3) Die Öffentlichkeit ist bei der Durchführung des Raumordnungsverfahrens dadurch zu beteiligen, dass die nach § 3 erforderlichen Unterlagen in den Landkreisen, kreisfreien Städten, Verbandsgemeinden, Ämtern, amtsfreien und mitverwaltenden Gemeinden, in denen sich die raumbedeutsame Planung oder Maßnahme voraussichtlich auswirkt, auf Veranlassung der Gemeinsamen Landesplanungsabteilung einen Monat zur Einsicht ausgelegt werden. Ort und Dauer der Auslegung sind mindestens eine Woche vorher im Amtsblatt für Brandenburg und in regionalen Zeitungen, die in dem Bereich verbreitet sind, in dem sich die Planung oder Maßnahme voraussichtlich auswirken wird, öffentlich bekannt zu machen.
Die Öffentlichkeit hat Gelegenheit, sich zu der raumbedeutsamen Planung oder Maßnahme bis zwei Wochen nach Ablauf der Auslegungsfrist schriftlich oder zur Niederschrift bei der auslegenden Stelle zu äußern; darauf ist in der Bekanntmachung hinzuweisen.
Die auslegenden Stellen leiten die fristgemäß vorgebrachten Äußerungen der Gemeinsamen Landesplanungsabteilung zu.
Sie können dazu eine eigene Stellungnahme abgeben.

(4) Die Gemeinsame Landesplanungsabteilung kann Erörterungen und Ortsbesichtigungen durchführen.

(5) Bei der Beteiligung sollen elektronische Informationstechnologien ergänzend genutzt werden.
Die ausschließliche Verwendung elektronischer Informationstechnologien ist zulässig, wenn die Beteiligten nach Absatz 1 über die notwendigen technischen Voraussetzungen verfügen.

(6) Die nach Absatz 3 Satz 1 auszulegenden Unterlagen und der Inhalt der Bekanntmachung nach Absatz 3 Satz 2 werden über das zentrale Internetportal zu Umweltverträglichkeitsprüfungen und der Bauleitplanung im Land Brandenburg zugänglich gemacht.

#### § 6  
Einstellung

Nimmt der Träger von der raumbedeutsamen Planung oder Maßnahme Abstand, stellt die Gemeinsame Landesplanungsabteilung das Raumordnungsverfahren nach Anhörung des Trägers der Planung oder Maßnahme ein.
Wirkt der Träger der raumbedeutsamen Planung oder Maßnahme auch nach Aufforderung innerhalb angemessener Frist nicht im Raumordnungsverfahren mit, kann die Gemeinsame Landesplanungsabteilung das Verfahren nach vorheriger Anhörung von Amts wegen einstellen.
Die übrigen Beteiligten sind über die Einstellung zu unterrichten.

#### § 7   
Landesplanerische Beurteilung

(1) Das Raumordnungsverfahren ist mit einer landesplanerischen Beurteilung abzuschließen.
In der landesplanerischen Beurteilung stellt die Gemeinsame Landesplanungsabteilung fest, ob und mit welchen Maßgaben die raumbedeutsame Planung oder Maßnahme mit den Erfordernissen der Raumordnung vereinbar ist (Ergebnis des Raumordnungsverfahrens).
Darüber hinaus sind Gegenstand und Ablauf des Verfahrens, Planungsträger und Beteiligte, die Beschreibung und Bewertung der Auswirkungen der raumbedeutsamen Planung oder Maßnahme sowie die raumordnerische Gesamtabwägung darzustellen.

(2) In den Fällen des § 1 Absatz 2 enthält die landesplanerische Beurteilung zusätzlich eine zusammenfassende Darstellung der im Raumordnungsverfahren erkennbaren Umweltauswirkungen der Planung oder Maßnahme sowie Maßnahmen, mit denen erhebliche nachteilige Umweltauswirkungen vermieden, vermindert oder ausgeglichen werden können.

(3) Die landesplanerische Beurteilung ist dem Träger der Planung oder Maßnahme und den am Raumordnungsverfahren Beteiligten zuzuleiten.
Die Öffentlichkeit wird über das Ergebnis des Raumordnungsverfahrens durch öffentliche Bekanntmachung im Amtsblatt für Brandenburg und in den regionalen Zeitungen, die in dem Bereich verbreitet sind, in dem sich die Planung oder Maßnahme voraussichtlich auswirken wird sowie durch Einstellung in das Internet unter der Adresse der Gemeinsamen Landesplanungsabteilung Berlin-Brandenburg unterrichtet.
Dabei ist das Ergebnis des Raumordnungsverfahrens mitzuteilen sowie auf die Möglichkeit zur Einsicht in die vollständige landesplanerische Beurteilung hinzuweisen.

(4) Die Unterlagen nach den Absätzen 1 und 2 werden über das zentrale Internetportal zu Umweltverträglichkeitsprüfungen und der Bauleitplanung im Land Brandenburg zugänglich gemacht.

#### § 8  
Beschleunigtes Verfahren

Bei der Durchführung des beschleunigten Verfahrens nach § 16 Absatz 1 des Raumordnungsgesetzes kann die Gemeinsame Landesplanungsabteilung im Einzelfall auch von Verfahrensschritten nach dieser Verordnung absehen, soweit keine anderen Rechtsvorschriften entgegenstehen.

#### § 9   
Geltungsdauer

Die landesplanerische Beurteilung kann ihre Gültigkeit verlieren, wenn sich die zugrunde liegende raumbedeutsame Planung oder Maßnahme oder ihre Bewertungsgrundlagen wesentlich geändert haben, insbesondere weil für die Planung oder Maßnahme bedeutsame neue oder geänderte Ziele der Raumordnung verbindlich geworden sind.
Die Entscheidung hierüber trifft die Gemeinsame Landesplanungsabteilung.

#### § 10  
Gebühren und Auslagen

(1) Die Gemeinsame Landesplanungsabteilung erhebt für ihre Amtshandlungen bei der Durchführung von Raumordnungsverfahren Verwaltungsgebühren und Auslagen nach Maßgabe dieser Verordnung und dem anliegenden Gebührenverzeichnis.
Bei der Bemessung der Höhe der Verwaltungsgebühren im Einzelfall sind der Verwaltungsaufwand, die Bedeutung, der wirtschaftliche Wert oder der sonstige Nutzen für den Träger der raumbedeutsamen Planung oder Maßnahme angemessen zu berücksichtigen.
Im Zusammenhang mit der Amtshandlung stehende Auslagen nach § 9 des Gebührengesetzes für das Land Brandenburg werden gesondert erhoben, soweit sie nicht von dem anliegenden Gebührenverzeichnis erfasst sind.

(2) Von der Zahlung einer Verwaltungsgebühr sind die öffentlichen Stellen im Sinne des § 3 Absatz 1 Nummer 5 des Raumordnungsgesetzes befreit, deren raumbedeutsame Planungen und Maßnahmen Gegenstand eines Raumordnungsverfahrens sind.

(3) Wird das Raumordnungsverfahren nach Einleitung, aber vor Beendigung einschließlich der Bekanntmachung der landesplanerischen Beurteilung eingestellt, werden für bis dahin vollständig erbrachte Amtshandlungen Verwaltungsgebühren nach dieser Verordnung und dem anliegenden Gebührenverzeichnis erhoben.
Dies gilt auch für teilweise erbrachte Amtshandlungen, wobei sich der Mindestsatz auf ein Viertel und der Höchstsatz auf drei Viertel der für die Amtshandlung vorgesehenen Verwaltungsgebühr reduziert.

#### § 11  
Übergangsregelung

Raumordnungsverfahren, die vor dem 17.
Juli 2020 förmlich eingeleitet wurden, werden nach der Gemeinsamen Raumordnungsverfahrensverordnung vom 14.
Juli 2010 (GVBl.
II Nr. 47), die durch Artikel 12 des Gesetzes vom 15.
Oktober 2018 (GVBl.
I Nr.
22 S.
28) geändert worden ist, fortgesetzt.

#### § 12  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Gemeinsame Raumordnungsverfahrensverordnung vom 24.
Januar 1996 (GVBl.
II S. 82) außer Kraft.

Potsdam, den 14.
Juli 2010

Der Minister für Infrastruktur und Landwirtschaft

In Vertretung  
Rainer Bretschneider

* * *

### Anlagen

1

[Anlage (zu § 10) - Gebührenverzeichnis](/br2/sixcms/media.php/68/GROVerfV-Anlage.pdf "Anlage (zu § 10) - Gebührenverzeichnis") 339.4 KB