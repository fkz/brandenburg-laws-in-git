## Verordnung über das Verfahren für Anträge nach § 56 Absatz 5 Satz 3 und 4 des Infektionsschutzgesetzes

Auf Grund des § 56 Absatz 11 Satz 2 des Infektionsschutzgesetzes vom 20.
Juli 2000 (BGBl. I S. 1045), der zuletzt durch Artikel 1 Nummer 4 des Gesetzes vom 28.
Mai 2021 (BGBl. I S. 1174) geändert worden ist, verordnet die Landesregierung:

#### § 1 Verfahren

Arbeitgeber und Selbstständige haben Anträge nach § 56 Absatz 5 Satz 3 und 4 des Infektionsschutzgesetzes nach amtlich vorgeschriebenem Datensatz durch Datenfernübertragung zu übermitteln.
§ 56 Absatz 11 Satz 3 des Infektionsschutzgesetzes bleibt unberührt.

#### § 2 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 2.
August 2021

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

In Vertretung

Michael Ranft