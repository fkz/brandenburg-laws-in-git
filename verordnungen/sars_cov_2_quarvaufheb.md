## Verordnung zur Aufhebung der SARS-CoV-2-Quarantäneverordnung

Auf Grund des § 32 Satz 1 in Verbindung mit den §§ 28, 28a, 29, 30 Absatz 1 Satz 2 des Infektionsschutzgesetzes vom 20.
Juli 2000 (BGBl.
I S. 1045), von denen § 28 zuletzt durch Artikel 1 Nummer 16 des Gesetzes vom 18.
November 2020 (BGBl.
I S.
2397, 2400), § 28a durch Artikel 1 Nummer 2c des Gesetzes vom 29.
März 2021 (BGBl.
I S.
370, 372), § 29 zuletzt durch Artikel 41 Nummer 7 des Gesetzes vom 8.
Juli 2016 (BGBl.
I S.
1594, 1598), § 30 zuletzt durch Artikel 1 Nummer 18 des Gesetzes vom 19.
Mai 2020 (BGBl.
I S.
1018, 1023) geändert und § 32 Satz 1 durch Artikel 1 Nummer 4 des Gesetzes vom 22.
April 2021 (BGBl.
I S.
802, 806) neu gefasst worden sind, in Verbindung mit § 2 der Infektionsschutzzuständigkeitsverordnung vom 27.
November 2007 (GVBl. II S. 488), der durch die Verordnung vom 10.
Januar 2012 (GVBl.
II Nr. 2) neu gefasst worden ist, verordnet die Ministerin für Soziales, Gesundheit, Integration und Verbraucherschutz:

#### § 1 Aufhebung

Die SARS-CoV-2-Quarantäneverordnung vom 3.
Februar 2021 (GVBl.
II Nr.
14), die zuletzt durch die Verordnung vom 27.
April 2021 (GVBl.
II Nr.
42) geändert worden ist, wird aufgehoben.

#### § 2 Übergangsregelung

Auf Personen, die vor dem 13.
Mai 2021 in das Gebiet des Landes Brandenburg eingereist sind, sind die Regelungen der SARS-CoV-2-Quarantäneverordnung vom 3.
Februar 2021 (GVBl. II Nr. 14), die zuletzt durch die Verordnung vom 27.
April 2021 (GVBl.
II Nr.
42) geändert worden ist, bis zum Ablauf von 14 Tagen nach der Einreise weiter anzuwenden; Erleichterungen und Ausnahmen nach der COVID-19-Schutzmaßnahmen-Ausnahmenverordnung bleiben unberührt.

#### § 3 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 18.
Mai 2021

Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

In Vertretung

Anna Heyer-Stuffer