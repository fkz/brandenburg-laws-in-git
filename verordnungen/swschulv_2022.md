## Verordnung über die schulischen Bildungsangelegenheiten der Sorben/Wenden (Sorben/Wenden-Schulverordnung - SWSchulV)

Auf Grund des § 5 Absatz 3 und § 13 Absatz 3 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
August 2002 (GVBl.
I S. 78), von denen § 5 Absatz 3 zuletzt durch Artikel 6 des Gesetzes vom 11.
Februar 2014 (GVBl.
I Nr. 7) neu gefasst und § 13 Absatz 3 zuletzt durch Artikel 1 des Gesetzes vom 8.
Januar 2007 (GVBl.
I S. 2) geändert worden ist, in Verbindung mit § 13b Absatz 2 des Sorben/Wenden-Gesetzes vom 7.
Juli 1994 (GVBl.
I S. 294), der durch Artikel 1 des Gesetzes vom 11.
Februar 2014 (GVBl.
I Nr. 7) eingefügt worden ist, verordnet die Ministerin für Bildung, Jugend und Sport im Benehmen mit dem für Schule zuständigen Ausschuss des Landtages und dem Rat für Angelegenheiten der Sorben/Wenden beim Landtag:

#### § 1 Allgemeine Grundsätze

(1) Die Vermittlung und Förderung von Kenntnissen und das Verstehen der sorbischen/wendischen Identität, Kultur und Geschichte sind besondere Aufgaben der Schule.
In den Schulen im angestammten Siedlungsgebiet der Sorben/Wenden im Sinne des § 3 Absatz 2 des Sorben/Wenden-Gesetzes (angestammtes Siedlungsgebiet) sind insbesondere die sorbische/wendische Sprache, Geschichte und Kultur in die Bildungsarbeit einzubeziehen.

(2) Die Europäische Charta der Regional- oder Minderheitensprachen und das Rahmenübereinkommen zum Schutz nationaler Minderheiten bilden die Grundlage für die Regelung der schulischen Bildungsangelegenheiten der Sorben/Wenden im Land Brandenburg.

(3) Im angestammten Siedlungsgebiet ist allen Schülerinnen und Schülern die Möglichkeit zu geben, gemäß den Bestimmungen dieser Verordnung die niedersorbische Sprache zu erlernen und in festzulegenden Fächern, Klassen- und Jahrgangsstufen in niedersorbischer Sprache unterrichtet zu werden.
Die Schulen im angestammten Siedlungsgebiet informieren die Eltern und die Schülerinnen und Schüler rechtzeitig vor Schuljahresbeginn, zum Zeitpunkt der Anmeldung, über die Möglichkeiten, die niedersorbische Sprache zu erlernen und zu pflegen.

(4) Schulen, die besonders der Pflege, Förderung und Vermittlung der sorbischen/wendischen Sprache und Kultur dienen und niedersorbische Bildungsangebote oder solche mit Niedersorbisch als eine von mehreren Sprachen anbieten, werden durch das Land gefördert und unterstützt.

(5) Lehrkräfte müssen die Möglichkeit erhalten, niedersorbische Sprachkenntnisse zu erwerben und zu vertiefen.
Die Aus-, Fort- und Weiterbildungsplanung für die Lehrkräfte berücksichtigt die Kultur und Geschichte der Sorben/Wenden.
Das staatliche Schulamt informiert in geeigneter Weise über aktuelle Aus-, Fort- und Weiterbildungsangebote.

(6) Es gelten die Regelungen der jeweiligen Bildungsgangverordnung, soweit in dieser Verordnung nichts Abweichendes geregelt wird.

(7) Das Land unterhält in Cottbus/Chóśebuz eine Arbeitsstelle für sorbische/wendische Bildungsentwicklung (ABC).
Zu deren Aufgaben zählt die Erstellung von Lehr- und Lernmitteln, die Entwicklung der Rahmenlehrpläne aller Jahrgangsstufen und die Lehrerfortbildung für den Unterricht in Sorbisch/Wendisch einschließlich des dafür notwendigen Personals.

#### § 2 Unterricht in Sorbisch/Wendisch

(1) Sorbisch/Wendisch kann angeboten werden

1.  als Unterrichtsfach Sorbisch/Wendisch und
2.  als Fremdsprachenunterricht.

Der Unterricht in den übrigen Fächern kann auch in niedersorbischer Sprache erfolgen.

(2) Der Unterricht in Sorbisch/Wendisch kann als Wahlunterricht angeboten werden.
Die Entscheidung der Schülerin oder des Schülers sowie der Eltern über die freiwillige Teilnahme erfolgt zu Beginn eines Schuljahres und gilt jeweils für ein Schuljahr.

(3) Im Unterricht und in außerunterrichtlichen Angeboten sollen die sprachlichen und kulturellen Erfahrungen der Schülerinnen und Schüler berücksichtigt werden.

(4) Reichen die Schülerzahlen einer Schule nicht aus oder stehen an der eigenen Schule keine befähigten Lehrkräfte zur Verfügung, können Schülerinnen und Schüler am Unterricht in Sorbisch/Wendisch an einer anderen Schule teilnehmen.

(5) Das für Schule zuständige Mitglied der Landesregierung und der Rat für Angelegenheiten der Sorben/Wenden beraten auf der Basis eines Monitorings im zweiten Quartal eines Kalenderjahres über den Stand der Schuljahresorganisation für den Unterricht in Sorbisch/Wendisch gemäß Absatz 1 im kommenden Schuljahr und im vierten Quartal über das laufende Schuljahr.

#### § 3 Sorbisch/Wendisch als Unterrichtsfach

(1) Das Fach Sorbisch/Wendisch dient dazu, in der Grundschule die Sprech-, Hörverstehens-, Schreib- und Lesekompetenz zu erlernen bzw.
weiterzuentwickeln.
Die muttersprachlichen Kompetenzen sind durch geeignete Maßnahmen und Methoden besonders zu fördern.
Schülerinnen und Schüler in den Jahrgangsstufen 1 bis 6, die die niedersorbische Sprache erlernen und vertiefen wollen, sind zur Teilnahme am Unterricht im Fach Sorbisch/Wendisch berechtigt.

(2) In den Sekundarstufen I und II kann Sorbisch/Wendisch als Unterrichtsfach angeboten werden.
Schülerinnen und Schüler ab der Jahrgangsstufe 7 sind zur Teilnahme am Unterricht im Fach Sorbisch/Wendisch berechtigt, wenn aufgrund der vorhandenen Kenntnisse zu erwarten ist, dass sie erfolgreich am Unterricht teilnehmen können.
Die Entscheidung trifft die Fachkonferenz oder die das Fach Sorbisch/Wendisch unterrichtende Lehrkraft.

(3) Soweit die organisatorischen Bedingungen es zulassen, können Schülerinnen und Schüler, deren Leistungen in der niedersorbischen Sprache so ausgeprägt sind, dass sie den Anforderungen der nächsthöheren Jahrgangsstufe gewachsen sind, auf Antrag der Eltern im Fach Sorbisch/Wendisch am Unterricht der nächsthöheren Jahrgangsstufe teilnehmen.
Reichen die Kenntnisse in der niedersorbischen Sprache nach einem Wechsel auf eine andere Schule nicht aus, um am Unterricht der Jahrgangsstufe im Fach Sorbisch/Wendisch teilnehmen zu können und lassen es die organisatorischen Bedingungen zu, kann die Schülerin oder der Schüler den Unterricht in diesem Fach in einer niedrigeren Jahrgangsstufe aufnehmen oder den Fremdsprachenunterricht Sorbisch/Wendisch besuchen.

(4) Für das Bildungsangebot im Fach Sorbisch/Wendisch gelten bei Entscheidungen über das Aufrücken oder Versetzen und bei Zuerkennung schulischer Abschlüsse die Bestimmungen des jeweiligen Bildungsgangs für Fremdsprachen.
Dabei wird das Fach Sorbisch/Wendisch in den Bildungsgängen der Sekundarstufe I als ein Pflicht- oder Wahlpflichtfach berücksichtigt, nicht aber wie ein Fach der Fächergruppe I.

#### § 4 Sorbisch/Wendisch als Fremdsprachenunterricht

(1) Sorbisch/Wendisch als Fremdsprachenunterricht dient dazu, Grundlagen der Sprech-, Hörverstehens-, Schreib- und Lesekompetenzen zu erlernen, auszubauen, weiterzuentwickeln und zu vertiefen.

(2) Sorbisch/Wendisch als Fremdsprachenunterricht kann bei kleinen Lerngruppen oder der Nichtverfügbarkeit von Lehrkräften jahrgangsstufenübergreifend durch die Zusammenlegung zweier aufeinanderfolgender Jahrgangsstufen organisiert werden.
Dies gilt nicht für die gymnasiale Oberstufe.
In den Jahrgangsstufen 1 und 2 soll jahrgangsstufenübergreifender Unterricht vermieden werden.
Bei jahrgangsstufenübergreifendem Unterricht muss binnendifferenziert unterrichtet werden.

(3) Verlässt eine Schülerin oder ein Schüler nach mindestens vier Jahren Unterricht im Fach Sorbisch/Wendisch oder Sorbisch/Wendisch als Fremdsprachenunterricht eine sorbische/wendische Spezialschule, so erhält die Schülerin oder der Schüler eine Bescheinigung über das erreichte Sprachniveau nach dem „Gemeinsamen Europäischen Referenzrahmen für Sprachen (GER)“ auf dem Zeugnis.

#### § 5 Niedersorbische Sprache in den übrigen Fächern

(1) In allen Jahrgangsstufen und Bildungsgängen kann in festzulegenden Fächern in niedersorbischer Sprache unterrichtet werden.
Die Schulleiterin oder der Schulleiter entscheidet auf der Grundlage der Beschlüsse der Konferenz der Lehrkräfte, nach Anhörung der Fachkonferenz, über die nachfolgende Verwendung der niedersorbischen Sprache als Allgemein- und Fachsprache:

1.  einsprachig niedersorbisch,
2.  bilingual überwiegend in niedersorbischer Sprache und
3.  in Form bilingualer Module innerhalb des Fachs.

(2) Soweit ein Fach durchgehend einsprachig niedersorbisch unterrichtet werden soll, bedarf es der Genehmigung des staatlichen Schulamtes.

(3) Als Voraussetzung für die Teilnahme am Unterricht im Sinne von Absatz 1 sollen die Schülerinnen und Schüler über ausreichende Kenntnisse in der niedersorbischen Sprache verfügen.
Die Entscheidung hierüber trifft die Fachkonferenz oder die das Fach unterrichtende Lehrkraft.

#### § 6 Sorbische/Wendische Schulen

(1) Schulen, die gemäß § 5 Absatz 2 des Brandenburgischen Schulgesetzes die sorbische/wendische Sprache und Kultur in besonderer Weise vermitteln und fördern und das hinreichend im Schulprogramm nachweisen, können sich nach Genehmigung des staatlichen Schulamtes „Sorbische/Wendische Schule“ nennen.
In diesen Schulen sind Geschichte, Kultur, Minderheitenrechte und das aktuelle Leben der Sorben/Wenden in die Bildungsarbeit vertiefend einzubeziehen.

(2) In Sorbischen/Wendischen Schulen soll den Schülerinnen und Schülern die Möglichkeit eingeräumt werden, Unterricht in mindestens einem Unterrichtsfach in niedersorbischer Sprache gemäß § 5 Absatz 1 zu erhalten.
In allen in deutscher Sprache unterrichteten Fächern der Stundentafel und in schulischen Veranstaltungen sollen Fachbegriffe und umgangssprachliche Wendungen sowie Begriffe des täglichen Lebens in angemessenem Umfang auch in niedersorbischer Sprache vermittelt werden.
Der Wunsch zum Besuch des Unterrichts in Sorbisch/Wendisch an einer sorbischen/wendischen Schule ist ein wichtiger pädagogischer Grund gemäß § 106 Absatz 4 Satz 3 Nummer 3 des Brandenburgischen Schulgesetzes.

#### § 7 Sorbische/Wendische Schulen mit besonderer Prägung

(1) Sorbische/Wendische Schulen können als Schulen mit besonderer Prägung (Spezialschulen) gemäß § 8a des Brandenburgischen Schulgesetzes organisiert werden.
Die Genehmigung dafür erfolgt durch das für Schule zuständige Mitglied der Landesregierung.
Die Teilnahme am Unterricht im Fach Sorbisch/Wendisch oder am Unterricht in Sorbisch/Wendisch als Fremdsprache ist für Schülerinnen und Schüler der Schule Pflicht.
In festzulegenden Fächern ist Unterricht in niedersorbischer Sprache zu erteilen.

(2) In sorbischen/wendischen Spezialschulen wird die niedersorbische Sprache außerhalb des Unterrichts und mit der wachsenden sprachlichen Befähigung der Schülerinnen und Schüler zunehmend als Umgangssprache in der Schule genutzt.
Insbesondere Lehrer- und Schulkonferenzbeschlüsse und Bescheide sollen in deutscher und in niedersorbischer Sprache verfasst werden.

(3) An sorbischen/wendischen Spezialschulen sollen Lehrkräfte eingesetzt werden, die die niedersorbische Sprache beherrschen.
Soweit dies bei der Einstellung und Umsetzung nicht gewährleistet ist, sollen sie die erforderlichen Sprachkenntnisse innerhalb von drei Jahren nach Aufnahme der Tätigkeit an einer Spezialschule erwerben und nachweisen.

#### § 8 Inkrafttreten, Außerkrafttreten

(1) Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Sorben-\[Wenden-\]Schulverordnung vom 31.
Juli 2000 (GVBl.
II S. 291) außer Kraft.

(2) Diese Verordnung wird in deutscher und niedersorbischer Sprache veröffentlicht.

Potsdam, den 27.
Januar 2022

Die Ministerin für Bildung, Jugend und Sport

Britta Ernst

Póstajenje wó šulskich kubłańskich nastupnosćach Serbow  
(Serbske šulske póstajenje – Sšp)
-------------------------------------------------------------------------------------------

**Z dnja 27.
Januar 2022**

Na zakłaźe § 5 pódstawk 3 a § 13 pódstawk 3 Bramborskeje šulskeje kazni we wersiji wuzjawjenja z dnja 2.
awgusta 2002 (GVBl.
I S.
78), z kótarejuž § 5 pódstawk 3 jo se slědny raz pśez artikel 6 kazni z dnja 11.
februara 2014 (GVBl.
I Nr.
7) nowo wobzamknuł a § 13 pódstawk 3 jo se slědny raz pśez artikel 1 kazni z dnja 8.
januara 2007 (GVBl.
I S.
2) změnił, w zwisku z § 13b pódstawk 2 Serbskeje kazni z dnja 7.
julija 1994 (GVBl.
I S.
294), kótaryž jo se pśez artikel 1 kazni z dnja 11.
februara 2014 (GVBl.
I Nr.
7) zasajźił, póstajijo ministaŕka za kubłanje, młoźinu a sport we wobjadnosći ze za šulu zagronitym wuběrkom krajnego sejma a Radu za nastupnosći Serbow pśi krajnem sejmje:

####  § 1 <span lang="dsb">Powšykne zasady</span>

(1) Pósrědnjanje a spěchowanje znaśow a rozměśe serbskeje identity, kultury a stawiznow su wósebne nadawki šule.
W šulach w starodawnem sedleńskem rumje Serbow we zmysle § 3 pódstawk 2 Serbskeje kazni (starodawny sedleński rum) maju wósebnje serbska rěc, stawizny a kultura se do kubłańskego źěła zapśimjeś.

(2) Europska charta regionalnych abo mjeńšynowych rěcow a ramikowe dojadnanje za šćit narodnych mjeńšynow stej zakład rědowanja šulskich kubłańskich nastupnosćow Serbow w Bramborskej.

(3) W starodawnem sedleńskem rumje ma se wšyknym wuknicam a wuknikam zmóžniś, aby pó wustajenjach toś togo póstajenja nawuknuli dolnoserbsku rěc a w pśedmjatach, rědowniskich schójźeńkach a lětnikach, kótarež se maju póstajiś, se w dolnoserbskej rěcy rozwucowali.
Šule w starodawnem sedleńskem rumje informěruju starjejšych, wuknice a wuknikow zawcasa do zachopjeńka šulskego lěta, w pśizjawjeńskem casu, wó móžnosćach nawuknjenja a woplěwanja dolnoserbskeje rěcy.

(4) Šule, kótarež wósebnje se pśiwobrośuju woplěwanju, spěchowanju a pósrědnjanju serbskeje rěcy a kultury a pórucuju dolnoserbske kubłańske pórucenja abo take z dolnoserbšćinu ako jadneju z wjele rěcow, se pśez kraj spěchuju a pódpěruju.

(5) Ceptaŕki a ceptarje muse dostaś móžnosć, sebje znaśa dolnoserbskeje rěcy pśiswójś a pódłymiś.
Planowanje wu-, do- a dalejkubłanja za ceptaŕki a ceptarjow źiwa na kulturu a stawizny Serbow.
Statny šulski amt informěrujo na pśigódnu wašnju wó aktualnych pórucenjach wu-, do- a dalejkubłanja.

(6) Płaśe rědowanja danego póstajenja kubłańskego pśeběga, jo-lic w tom póstajenju se nic wótchylajucego njerědujo.

(7) Kraj ma w Chóśebuzu Źěłanišćo za serbske kubłańske wuwijanje (ABC).
K jogo nadawkam słuše wuźěłanje wucbnych a wuknjeńskich srědkow, wuwijanje ramikowych planow wšyknych lětnikow a dokubłanje ceptaŕkow a ceptarjow za wucbu w pśedmjaśe serbšćina inkluziwnje za to trjebny personal.

####  § 2 <span lang="dsb">Wucba w pśedmjaśe serbšćina</span>

(1) Pśedmjat serbšćina móžo se pórucyś

1.  ako wucbny pśedmjat serbšćina a
2.  ako cuzorěcna wucba.

Wucba w dalšnych pśedmjatach móžo se teke w dolnoserbskej rěcy wótměś.

(2) Wucba w serbšćinje móžo se teke ako wuzwólona wucba pórucyś.
Wuknice abo wuknik ako teke starjejšej rozsuźiju wó dobrowólnem wobźělenju na zachopjeńku šulskego lěta, rozsuźenje płaśi pśecej za jadno šulske lěto.

(3) We wucbje a w zwenkawucbnych pórucenjach dej se źiwaś na rěcne a kulturne nazgónjenja wuknicow a wuknikow.

(4) Njejo-li licba wuknjecych jadneje šule wusoka dosć abo njejsu-li na tej šuli žedne wuzamóžnjone ceptaŕki daniž ceptarje k dispoziciji, mógu se wuknice a wukniki na wucbje serbšćiny w drugej šuli wobźěliś.

(5) Za šulu zagronity cłonk krajnego kněžaŕstwa a Rada za nastupnosći Serbow wobradujotej na bazy monitoringa w drugem kwartalu kalendaŕskego lěta wó stawje organizacije šulskego lěta za wucbu serbšćiny pó pódstawku 1 w pśiducem lěśe a w stwórtem kwartalu wó běžnem šulskem lěśe.

####  § 3 <span lang="dsb">Serbšćina ako wucbny pśedmjat</span>

(1) Pśedmjat serbšćina słužy w zakładnej šuli k tomu, aby nawuknuli abo dalej wuwijali kompetence powědanja, rozmějucego šłyšanja, pisanja a cytanja.
Maminorěcne kompetence maju se pśez se góźece napšawy a metody wósebnje spěchowaś.
Wuknice a wukniki lětnikow 1 do 6, kótarež kśě dolnoserbsku rěc nawuknuś a pódłymiś, maju pšawo se wobźěliś na wucbje w pśedmjaśe serbšćina.

(2) W sekundarnyma schójźeńkoma I a II móžo serbšćina se pórucyś ako wucbny pśedmjat.
Wuknice a wukniki wót lětnika 7 su wopšawnjone se wobźěliś na wucbje w pśedmjaśe serbšćina, gaž móžoš na zakłaźe eksistěrujucych znaśow wócakowaś, až mógu se z wuspěchom na wucbje wobźěliś.
Rozsuźijo fachowa konferenca abo w pśedmjaśe serbšćina rozwucujuca ceptaŕka abo ceptaŕ.

(3) Jo-lic to organizatoriske wuměnjenja dopušćiju, mógu wuknice a wukniki, kótarychž wugbaśa w dolnoserbskej rěcy su tak wuwite, až su pominanjam direktnje slědujucego lětnika zrosćone, na pšosbu starjejšeju se wobźěliś w pśedmjaśe serbšćina na wucbje direktnje slědujucego lětnika.
Njedosegaju-li znaśa w dolnoserbskej rěcy pó změnje do drugeje šule, aby se mógali wobźěliś na wucbje togo lětnika w pśedmjaśe serbšćina a dowóliju-li to organizatoriske wuměnjenja, móžo wuknica abo wuknik zachopiś wucbu w tom pśedmjaśe w nišem lětniku abo se wobźěliś na wucbje serbšćiny ako cuza rěc.

(4) Za kubłańske pórucenje w pśedmjaśe serbšćina płaśe pśi rozsuźenjach wó póstupjenju abo pśesajźenju a pśi pśiznaśu wótzamknjonych šulskich wukubłanjow póstajenja danego kubłańskego pśeběga za cuze rěcy.
Pśi tom pśedmjat serbšćina se w kubłańskich pśeběgach sekundarnego schójźeńka I pśipóznajo ako obligatoriski abo wólny pśedmjat, nic pak ako pśedmjat kupki pśedmjatow I.

####  § 4 <span lang="dsb">Serbšćina ako cuzorěcna wucba</span>

(1) Serbšćina ako cuzorěcna wucba jo k tomu, aby zakłady kompetencow powědanja, rozmějucego słyšanja, pisanja a cytanja nawuknuli, wutwarjali, dalej wuwijali a pódłymjowali.

(2) Serbšćina ako cuzorěcna wucba móžo pśi małych kupkach a felujucych ceptaŕkach a ceptarjach se organizěrowaś lětniki pśesegajucy pśez gromadu zestajenje dweju zasobu slědujuceju lětnikowu.
To njepłaśi za gymnazialny wušy schójźeńk.
W lětnikoma 1 a 2 dejali se lětniki pśesegajuceje wucby wobijaś.
Pśi lětniki pśesegajucej wucbje musy se pó nutśikownem diferencěrowanju rozwucowaś.

(3) Spušćijo-li wuknica abo wuknik pó nanejmjenjej styrich lětach wucby w pśedmjaśe serbšćina abo serbšćina ako cuzorěcna wucba serbsku specialnu šulu, pótom wuknica abo wuknik dostanjo wobtwarźenje wó dojśpjonem rěcnem niwowje pó „Zgromadnem europskem referencowem ramiku za rěcy (GER)“ na wopismje.

####  § 5 <span lang="dsb">Dolnoserbska rěc w dalšnych pśedmjatach</span>

(1) We wšyknych lětnikach a kubłańskich pśeběgach móžo se w pśedmjatach, kótarež se maju póstajiś, w dolnoserbskej rěcy wuwucowaś.
Šulska wjednica abo wjednik rozsuźijo na zakłaźe wobzamknjenjow konference ceptaŕkow a ceptarjow, pó napšašowanju fachoweje konference, wó slědujucem nałožowanju dolnoserbskeje rěcy ako powšykna a fachowa rěc:

1.  jadnorěcnje dolnoserbski
2.  bilingualnje pśewažnje w dolnoserbskej rěcy a
3.  we formje bilingualnych modulow w pśedmjaśe.

(2) Jo-lic jaden pśedmjat dej se stawnje jadnorěcnje dolnoserbski wuwucowaś, jo za to trjeba pśizwólenje statnego šulskego amta.

(3) Ako wuměnjenje za wobźělenje na wucbje w zmysle pódstawka 1 deje wuknice a wukniki wobsejźeś dosegajuce znaśa w dolnoserbskej rěcy.
Rozsuźijo wó tom fachowa konferenca abo w pśedmjaśe wuwucujuca ceptaŕka abo ceptaŕ.

####  § 6 <span lang="dsb">Serbske šule</span>

(1) Šule, kótarež pó § 5 pódstawk 2 Bramborskeje šulskeje kazni na wósebnu wašnju pósrědnjaju a spěchuju serbsku rěc a kulturu a to dosegajucy w šulskem programje dopokazuju, mógu pó pśizwólenju šulskego amta se pomjeniś „Serbska šula“.
W tych šulach maju stawizny, kultura, mjeńšynowe pšawa a aktuelne žywjenje Serbow se dłymjej do kubłańskego źěła zapśimjeś.

(2) W serbskich šulach ma se wuknicam a wuknikam pśizwóliś móžnosć, aby dostali w nanejmjenjej jadnom pśedmjaśe wucbu w dolnoserbskej rěcy pó § 5 pódstawk 1.
We wšyknych w nimskej rěcy pódawanych pśedmjatach góźinskeje tofle a na šulskich zarědowanjach deje fachowe zapśimjeśa a wobchadne wobroty ako teke zapśimjeśa wšednego dnja se w pśiměrjonej rozměrje teke w dolnoserbskej rěcy pósrědnjaś.
Žycenje, chójźiś na wucbu w serbskej rěcy na jadnej serbskej šuli jo wažny pedagogiski zakład pó § 106 pódstawk 4 sada 3 cysło 3 Bramborskeje šulskeje kazni.

####  § 7 <span lang="dsb">Serbske šule z wósebnym charakterom</span>

(1) Serbske šule mógu se organizěrowaś ako šule z wósebnym charakterom (specialne šule) pó § 8a Bramborskeje šulskeje kazni.
Pśizwólenje za to dawa za šulu zagronity cłonk krajnego kněžaŕstwa.
Wobźělenje na wucbje w pśedmjaśe serbšćina abo na wucbje serbšćina ako cuza rěc jo za wuknice a wuknikow winowatosć.
W pśedmjatach, kótarež deje se póstajiś, ma se wuwucowaś w dolnoserbskej rěcy.

(2) W serbskich specialnych šulach se dolnoserbska rěc wužywa zwenka wucby a z rosćecym rěcnym wuzamóžnjenim wuknicow a wuknikow w pśiběrajucej měrje ako wobchadna rěc w šuli.
Pśedewšym wobzamknjenja ceptaŕskeje a šulskeje konference a wopowěsći deje se spisaś w nimskej a serbskej rěcy.

(3) Na serbskich specialnych šulach deje se zasajźiś ceptaŕki a ceptarje, kótarež serbsku rěc derje wuměju.
Jo-lic njejo to pśi pśistajenju a pśesajźenju zarucone, deje wóni trjebne rěcne znaśa w běgu tśich lět wót zachopjeńka źěła na jadnej specialnej šuli sebje pśiswójś a dopokazaś.

####  § 8 <span lang="dsb">Nabyśe płaśiwosći, zgubjenje płaśiwosći</span>

(1) Toś to póstajenje nabydnjo na dnju pó wózjawjenju płaśiwosć.
Rownocasnje zgubijo Serbske šulske póstajenje z dnja 31.
julija 2000 (GVBl.
II S.
291) płaśiwosć.

(2) Toś to póstajenje se w nimskej a dolnoserbskej rěcy wózjawijo.

Pódstupim, dnja 27.
Januar 2022

ministaŕka za kubłanje, młoźinu a sport

Britta Ernst