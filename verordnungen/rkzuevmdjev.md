## Verordnung zur Übertragung von Zuständigkeiten des Ministeriums der Justiz und für Europa und Verbraucherschutz für die Berechnung und Zahlung von Reise- und Umzugskosten, die Bewilligung, Berechnung und Zahlung von Trennungsgeld, die Unfallfürsorgeangelegenheiten sowie den Ersatz von Sachschäden auf die Zentrale Bezügestelle des Landes Brandenburg (Reisekostenzuständigkeitsübertragungsverordnung MdJEV - RkZÜVMdJEV)

Auf Grund

*   des § 63 Absatz 3 Satz 3 des Landesbeamtengesetzes vom 3.
    April 2009 (GVBl.
    I S. 26), der durch Artikel 4 des Gesetzes vom 20.
    November 2013 (GVBl.
    I Nr. 32 S. 123) geändert worden ist, in Verbindung mit § 9 Absatz 3 der Trennungsgeldverordnung in der Fassung der Bekanntmachung vom 29.
    Juni 1999 (BGBl. I S. 1533) im Einvernehmen mit dem Minister der Finanzen,
*   des § 66 Absatz 4 des Landesbeamtengesetzes in Verbindung mit § 6 Absatz 1 des Landesorganisationsgesetzes vom 24.
    Mai 2004 (GVBl.
    I S. 186), der durch Artikel 2 des Gesetzes vom 10.
    Juli 2014 (GVBl.
    I Nr. 28 S. 2) geändert worden ist,
*   des § 89 Absatz 1 des Brandenburgischen Beamtenversorgungsgesetzes vom 20. November 2013 (GVBl.
    I Nr. 32) im Einvernehmen mit dem Minister der Finanzen

verordnet der Minister der Justiz und für Europa und Verbraucherschutz:

#### § 1 Übertragung von Aufgaben

Die Zuständigkeit des Ministeriums der Justiz und für Europa und Verbraucherschutz für

1.  die Berechnung und Zahlung von Reisekosten gemäß § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes,
2.  die Berechnung und Zahlung von Umzugskosten gemäß § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes,
3.  die Bewilligung, Berechnung und Zahlung von Trennungsgeld gemäß § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes,
4.  die Unfallfürsorgeangelegenheiten gemäß Abschnitt 2 Unterabschnitt 3 des Brandenburgischen Beamtenversorgungsgesetzes und
5.  den Ersatz von Sachschäden und die Geltendmachung von übergegangenen Schadensersatzansprüchen gegen Dritte gemäß § 66 Absatz 1 und 3 des Landesbeamtengesetzes

wird betreffend die Bediensteten des Ministeriums der Justiz und für Europa und Verbraucherschutz, des Gemeinsamen Sekretariats des Kooperationsprogramms INTERREG V/A Brandenburg – Polen 2014 – 2020 in Frankfurt (Oder), die Bediensteten der Justizvollzugsanstalten des Landes Brandenburg sowie die Bediensteten des Zentralen IT-Dienstleisters der Justiz des Landes Brandenburg auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.
Die Zentrale Bezügestelle des Landes Brandenburg ist hinsichtlich der Unfallfürsorgeangelegenheiten (Satz 1 Nummer 4) Personalakten führende Stelle im Sinne des § 2 der Beamtenversorgungs-Zuständigkeitsverordnung vom 28.
Januar 1997 (GVBl.
II S. 53), die durch Artikel 3 der Verordnung vom 10. Mai 2004 (GVBl.
II S. 329, 330) geändert worden ist.

#### § 2 Vertretung bei Klagen

Im Rahmen der Übertragung der Zuständigkeit nach § 1 wird die Zentrale Bezügestelle des Landes Brandenburg ermächtigt, das Ministerium der Justiz und für Europa und Verbraucherschutz in verwaltungs- und arbeitsgerichtlichen Streitigkeiten zu vertreten.
Dies gilt auch für Anträge in einstweiligen Rechtsschutzverfahren.

#### § 3 Übergangsvorschrift

Für Anträge auf Berechnung und Zahlung von Reise- und Umzugskosten sowie auf Bewilligung, Berechnung und Zahlung von Trennungsgeld, die vor dem Inkrafttreten dieser Verordnung eingegangen sind und über die noch nicht abschließend entschieden worden ist, verbleibt es bei der Zuständigkeit der bisher zuständigen Stellen.
Unfallfürsorgeangelegenheiten sowie Anträge auf Ersatz von Sachschäden einschließlich übergegangener Schadensersatzansprüche gegen Dritte werden von der Zentralen Bezügestelle in dem Stand übernommen, in dem sie sich zum Zeitpunkt des Inkrafttretens dieser Verordnung befinden.
Dies gilt auch für die Vertretung in zu diesem Zeitpunkt bereits anhängigen Rechtsstreitigkeiten.

#### § 4 

### Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 1.
Januar 2016 in Kraft.
Gleichzeitig tritt die Reisekostenzuständigkeitsübertragungsverordnung MdJEV vom 6.
März 2015 (GVBl.
II Nr. 13) außer Kraft.

Potsdam, den 15.
Dezember 2015

Der Minister der Justiz  
und für Europa und Verbraucherschutz

Dr.
Helmuth Markov