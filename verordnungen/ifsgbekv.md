## Verordnung zur elektronischen öffentlichen Bekanntgabe von Allgemeinverfügungen nach dem Infektionsschutzgesetz (Infektionsschutzgesetz-Bekanntgabeverordnung - IfSGBekV)

Auf Grund des § 17 Absatz 1 Satz 1 Nummer 1 des Brandenburgischen E-Government-Gesetzes vom 23. November 2018 (GVBl. I Nr. 28) verordnet der Minister des Innern und für Kommunales im Benehmen mit dem IT-Beauftragten:

#### § 1 Öffentliche Bekanntgabe von Allgemeinverfügungen im Internet

(1) Die öffentliche Bekanntgabe einer Allgemeinverfügung über Schutzmaßnahmen nach dem Infektionsschutzgesetz in der jeweils geltenden Fassung kann abweichend von § 41 Absatz 4 des Verwaltungsverfahrensgesetzes in Verbindung mit § 1 Absatz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg dadurch erfolgen, dass die Allgemeinverfügung auf der Internetseite der erlassenden Behörde veröffentlicht wird (zugänglich machen).
Die Allgemeinverfügung tritt, wenn kein späterer Zeitpunkt bestimmt ist, am Tag nach der Zugänglichmachung auf der Internetseite der erlassenden Behörde in Kraft.

(2) Wird von der Möglichkeit der öffentlichen Bekanntgabe nach Absatz 1 Gebrauch gemacht, ist zusätzlich unverzüglich der verfügende Teil der Allgemeinverfügung nach den für die erlassende Behörde geltenden Vorschriften über eine Bekanntmachung zu verbreiten; dabei ist anzugeben, auf welcher Internetseite und an welchem Tag die Allgemeinverfügung veröffentlicht (zugänglich gemacht) wurde.

#### § 2 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft und mit Ablauf des 31. Dezember 2023 außer Kraft.

Potsdam, den 12.
Februar 2021

Der Minister des Innern und für Kommunales

Michael Stübgen