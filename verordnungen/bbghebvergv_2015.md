## Verordnung über die Vergütungen für die berufsmäßigen Leistungen der freiberuflichen Hebammen und Entbindungspfleger gegenüber Selbstzahlerinnen im Land Brandenburg (Brandenburgische Hebammenvergütungsverordnung - BbgHebVergV)

Auf Grund des § 2 Absatz 2 Satz 1 des Gesetzes über die Ausübung des Berufes der Hebamme und des Entbindungspflegers im Land Brandenburg vom 19.
Oktober 1993 (GVBl.
I S. 460) verordnet die Ministerin für Arbeit, Soziales, Gesundheit, Frauen und Familie:

#### § 1 Geltungsbereich

Freiberuflich tätigen Hebammen und Entbindungspflegern stehen für ihre berufsmäßigen Leistungen außerhalb der gesetzlichen Krankenkassen gegenüber Selbstzahlerinnen Gebühren, Auslagen, Zuschläge und Wegegelder zu.

#### § 2 Berechnungsgrundlage

(1) Gebühren, Auslagen, Zuschläge und Wegegelder sind nach Maßgabe des Vertrages über die Versorgung mit Hebammenhilfe nach § 134a des Fünften Buches Sozialgesetzbuch in der jeweils geltenden Fassung sowie des Ergänzungsvertrages nach § 134a des Fünften Buches Sozialgesetzbuch über Betriebskostenpauschalen bei ambulanten Geburten in von Hebammen geleiteten Einrichtungen und die Anforderungen an die Qualitätssicherung in diesen Einrichtungen in der jeweils geltenden Fassung abzurechnen.

(2) Gebühren für erbrachte Leistungen nach Maßgabe des in Absatz 1 genannten Vertrages können bis zum zweifachen Satz abgerechnet werden.
Innerhalb dieses Rahmens sind die Gebühren für erbrachte Leistungen nach den besonderen Umständen des Einzelfalles, insbesondere nach der Schwierigkeit und dem Zeitaufwand der einzelnen Leistung, zu bemessen.
Gebühren für erbrachte Leistungen sind mit dem einfachen Satz abzurechnen, wenn die Zahlung aufgrund des Zwölften Buches Sozialgesetzbuch oder aufgrund des Asylbewerberleistungsgesetzes erfolgt.

(3) Gebühren nach Maßgabe des in Absatz 1 genannten Ergänzungsvertrages sowie Wegegelder, Auslagen und Zuschläge nach Maßgabe des in Absatz 1 genannten Vertrages sind mit dem einfachen Satz abzurechnen.

#### § 3 Rechnungslegung

In der Rechnung sind die berechneten Leistungen chronologisch geordnet aufzulisten.
Beginn und Ende der Einzelleistung sind anzugeben, soweit dies für die Höhe der Vergütung von Bedeutung ist.
Sofern einzelne Positionen ärztliche Anordnungen voraussetzen, sind diese der Rechnung beizufügen.

#### § 4 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über die Vergütung der Leistungen von Hebammen und Entbindungspflegern gegenüber Selbstzahlerinnen im Land Brandenburg vom 22.
September 2009 (GVBl.
II S. 702) außer Kraft.

Potsdam, den 16.
November 2015

Die Ministerin für Arbeit, Soziales,  
Gesundheit, Frauen und Familie

Diana Golze