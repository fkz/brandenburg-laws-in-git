## Verordnung über die Übertragung der Befugnis zum Erlass von Verwaltungsvorschriften über Dienstkleidung der Bediensteten des feuerwehrtechnischen Dienstes im Land Brandenburg (Feuerwehrdienstkleidungsübertragungsverordnung - FeuDkÜV)

Auf Grund des § 59 Satz 2 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S. 26) in Verbindung mit § 6 Absatz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S.
186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28) geändert worden ist, verordnet die Landesregierung:

#### § 1 Übertragung

Die Befugnis der Landesregierung nach § 59 Satz 1 des Landesbeamtengesetzes zum Erlass von Verwaltungsvorschriften über Dienstkleidung, die bei Ausübung des Amtes üblich oder erforderlich ist, wird für die Beamtinnen und Beamten in den Laufbahnen des feuerwehrtechnischen Dienstes des Landes sowie der Gemeinden und Gemeindeverbände auf das für den Brand- und Katastrophenschutz zuständige Ministerium übertragen.

#### § 2 Einvernehmen

Die in § 1 genannten Verwaltungsvorschriften sind im Einvernehmen mit dem für Finanzen zuständigen Mitglied der Landesregierung zu erlassen.

#### § 3 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt rückwirkend zum 1.
Januar 2021 in Kraft.
Gleichzeitig tritt die Verordnung über die Übertragung der Befugnis zum Erlass von Verwaltungsvorschriften über Dienstkleidung für im Brandschutzwesen tätige Landesbedienstete vom 19.
April 1997 (GVBl.
II S.
259) außer Kraft.

Potsdam, den 19.
Januar 2022

Die Landesregierung des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister des Innern und für Kommunales

Michael Stübgen