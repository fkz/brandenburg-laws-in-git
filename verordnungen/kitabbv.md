## Kita-Beitragsbefreiungsverordnung (KitaBBV)

Auf Grund des § 17 Absatz 1a Satz 4 des Kindertagesstättengesetzes in der Fassung der Bekanntmachung vom 27.
Juni 2004 (GVBl.
I S. 384), der durch Artikel 1 des Gesetzes vom 1. April 2019 (GVBl.
I Nr. 8) eingefügt worden ist, verordnet die Ministerin für Bildung, Jugend und Sport und auf Grund des § 23 Absatz 1 Nummer 12 des Kindertagesstättengesetzes, der zuletzt durch Artikel 1 des Gesetzes vom 1.
April 2019 (GVBl.
I Nr.
8) geändert worden ist, verordnet die Ministerin für Bildung, Jugend und Sport im Einvernehmen mit dem Minister der Finanzen, dem Minister des Innern und für Kommunales und dem Ausschuss für Bildung, Jugend und Sport des Landtags:

#### § 1 Anwendungsbereich

(1) Diese Verordnung regelt das Vorliegen der Unzumutbarkeit, die Höhe des Pauschalbetrages und das Verfahren zur Erstattung der Einnahmeausfälle der Einrichtungsträger und der Landkreise und kreisfreien Städte, die dadurch entstehen, dass nach § 17 Absatz 1a des Kindertagesstättengesetzes kein Elternbeitrag von Personensorgeberechtigten erhoben werden darf, denen nach § 90 des Achten Buches Sozialgesetzbuch der Elternbeitrag nicht zuzumuten ist, sowie den Verwaltungskostenausgleich der Landkreise und kreisfreien Städte.

(2) Diese Verordnung gilt für alle Formen der Kindertagesbetreuung und Altersgruppen sowie für alle zeitlichen Umfänge der Betreuung.
Sie findet entsprechende Anwendung auf die Kindertagespflege.

(3) Zu den Elternbeiträgen gehören nicht das Essengeld und einmalige Beiträge für besondere Veranstaltungen und Leistungen.
Elternbeiträge im Sinne dieser Verordnung umfassen Zuschläge für Übernachtungen in Kindertagesstätten.

(4) Beitragsregelungen, die bis zum 31.
Juli 2019 wirksam geworden sind, insbesondere Beitragssatzungen und Beitragsordnungen, gelten bis zu ihrer Aufhebung fort, soweit nicht durch diese Rechtsverordnung ab dem 1.
August 2019 Beitragsfreiheit eintritt.

(5) § 17 Absatz 1 Satz 3 des Kindertagesstättengesetzes als Sonderregelung für Pflegekinder und Kinder in stationären Einrichtungen bleibt unberührt.

#### § 2 Unzumutbarkeit

(1) Den in § 90 Absatz 4 des Achten Buches Sozialgesetzbuch genannten Personensorgeberechtigten ist kein Elternbeitrag zuzumuten.
Dies gilt insbesondere, wenn die Personensorgeberechtigten oder deren Kind

1.  Leistungen zur Sicherung des Lebensunterhalts nach dem Zweiten Buch Sozialgesetzbuch,
2.  Leistungen nach dem dritten und vierten Kapitel des Zwölften Buches Sozialgesetzbuch,
3.  Leistungen nach den §§ 2 und 3 des Asylbewerberleistungsgesetzes,
4.  einen Kinderzuschlag gemäß § 6a des Bundeskindergeldgesetzes oder
5.  Wohngeld nach dem Wohngeldgesetz erhalten.

Ein Elternbeitrag kann den Personensorgeberechtigten auch dann nicht zugemutet werden, wenn ihr Haushaltseinkommen einen Betrag von 20 000 Euro im Kalenderjahr nicht übersteigt (Geringverdienende).
Haushaltseinkommen im Sinne des Satzes 3 ist die Gesamtsumme der laufenden Netto-Einnahmen aller im Haushalt des Kindes lebenden Eltern.

(2) Über die Unzumutbarkeit der Belastung durch die Erhebung eines Elternbeitrags aus sonstigen Gründen, die den Gründen nach Absatz 1 vergleichbar sind, entscheidet der Landkreis oder die kreisfreie Stadt nach pflichtgemäßem Ermessen.
In diesen Fällen kann der Träger der Einrichtung den Elternbeitrag festlegen und erheben, es sei denn, der Landkreis oder die kreisfreie Stadt hat die Unzumutbarkeit der Belastung durch die Erhebung eines Elternbeitrags nach Satz 1 festgestellt.

#### § 3 Einkommensbegriff

(1) Für die Feststellung des maßgeblichen Einkommens bei Geringverdienenden gemäß § 2 Absatz 1 Satz 3 gelten § 82 Absatz 1 und 2 sowie die §§ 83 und 84 des Zwölften Buches Sozialgesetzbuch entsprechend.
Zur Ermittlung kann das in der Anlage befindliche Formular verwendet werden.

(2) Im Regelfall sind zum Einkommen gemäß Absatz 1 alle Einkünfte in Geld oder Geldeswert zu rechnen, mit Ausnahme

1.  der Leistungen nach dem Zwölften Buch Sozialgesetzbuch,
2.  der Grundrente nach dem Bundesversorgungsgesetz und nach den Gesetzen, die eine entsprechende Anwendung des Bundesversorgungsgesetzes vorsehen, und
3.  der Renten oder Beihilfen nach dem Bundesentschädigungsgesetz für Schaden an Leben sowie an Körper oder Gesundheit bis zur Höhe der vergleichbaren Grundrente nach dem Bundesversorgungsgesetz,
4.  von Einkünften aus Rückerstattungen, die auf Vorauszahlungen beruhen, die Leistungsberechtigte aus dem Regelsatz gemäß dem Zwölften Buch Sozialgesetzbuch erbracht haben.

Zum regelmäßigen Einkommen zählen insbesondere auch Erwerbsminderungs-, Erwerbsunfähigkeits- und Waisenrenten, Unterhaltsbezüge sowie der Bezug von Elterngeld.
Abweichend von Absatz 1 bleiben bei der Einkommensberechnung das Kindergeld und das Baukindergeld des Bundes sowie die Eigenheimzulage nach dem Eigenheimzulagengesetz außer Betracht.

(3) Von dem Einkommen gemäß Absatz 2 sind abzusetzen

1.  auf das Einkommen entrichtete Steuern,
2.  Pflichtbeiträge zur Sozialversicherung einschließlich der Beiträge zur Arbeitsförderung,
3.  Beiträge zu öffentlichen oder privaten Versicherungen oder ähnlichen Einrichtungen, soweit diese Beiträge gesetzlich vorgeschrieben oder nach Grund und Höhe angemessen sind, sowie geförderte Altersvorsorgebeiträge nach § 82 des Einkommensteuergesetzes, soweit sie den Mindesteigenbeitrag nach § 86 des Einkommensteuergesetzes nicht überschreiten, und
4.  die mit der Erzielung des Einkommens verbundenen notwendigen Ausgaben, sogenannte Werbungskosten.

Erhält eine leistungsberechtigte Person aus einer Tätigkeit Bezüge oder Einnahmen, die nach § 3 Nummer 12, 26, 26a oder Nummer 26b des Einkommensteuergesetzes steuerfrei sind, ist abweichend von Satz 1 Nummer 2 bis 4 ein Betrag von bis zu 200 Euro monatlich nicht als Einkommen zu berücksichtigen.

(4) Maßgeblich ist das Einkommen in dem Kalenderjahr, das der Aufnahme des Kindes in die Kindertagesbetreuung vorausgegangen ist, es sei denn, es wird im laufenden Kalenderjahr ein geringeres Einkommen nachgewiesen.
Unterjährige Einkommensänderungen können berücksichtigt werden.

#### § 4 Prüfung durch den Einrichtungsträger

(1) Bei der Festsetzung der Elternbeiträge zu Beginn des jeweiligen Kita-Jahres gemäß § 17 Absatz 3 des Kindertagesstättengesetzes befragt der Träger der Kindertagesstätte die Personensorgeberechtigten unter Beachtung der datenschutzrechtlichen Bestimmungen, ob sie oder das Kind

1.  Leistungen zur Sicherung des Lebensunterhalts nach dem Zweiten Buch Sozialgesetzbuch,
2.  Leistungen nach dem dritten und vierten Kapitel des Zwölften Buches Sozialgesetzbuch,
3.  Leistungen nach den §§ 2 und 3 des Asylbewerberleistungsgesetzes,
4.  einen Kinderzuschlag gemäß § 6a des Bundeskindergeldgesetzes,
5.  Wohngeld nach dem Wohngeldgesetz erhalten oder
6.  Geringverdienende im Sinne des § 2 Absatz 1 Satz 3 sind.

(2) Die Personensorgeberechtigten legen dem Träger der Kindertagesstätte für die Prüfung nach Satz 1 entsprechende Nachweise vor, aus denen sich eine Unzumutbarkeit nach § 2 Absatz 1 ergibt.
Der Nachweis kann insbesondere durch die Vorlage folgender Dokumente erbracht werden:

1.  Leistungsbescheid über den Empfang einer der in § 90 Absatz 4 des Achten Buches Sozialgesetzbuch genannten Leistungen,
2.  Lohnsteuerbescheinigung,
3.  Verdienstbescheinigung,
4.  Steuerbescheid.

(3) Nach Vorlage eines Nachweises gemäß Absatz 2 tritt die Beitragsbefreiung nach dieser Verordnung ein.
Haben die Voraussetzungen des § 2 Absatz 1 bereits vor der Nachweiserbringung vorgelegen, so weist der Träger der Kindertagesstätte die Personensorgeberechtigten auf die Möglichkeit der Antragstellung nach § 90 Absatz 4 des Achten Buches Sozialgesetzbuch beim Landkreis oder bei der kreisfreien Stadt hin.
Eine Erstattung der Elternbeiträge durch den Träger der Kindertagesstätte findet in diesen Fällen nicht statt.

(4) Liegt kein Fall der Unzumutbarkeit nach § 2 Absatz 1 vor und hält der Träger der Kindertagesstätte die Unzumutbarkeit der Belastung der Personensorgeberechtigten mit einem Elternbeitrag aus sonstigen Gründen gemäß § 2 Absatz 2 für möglich, so weist der Träger der Kindertagesstätte die Personensorgeberechtigten auf die Möglichkeit der Antragstellung nach § 90 Absatz 4 des Achten Buches Sozialgesetzbuch beim Landkreis oder bei der kreisfreien Stadt hin.

(5) Der Träger der Kindertagesstätte ist verpflichtet, im Mindestmaß zu dokumentieren, dass er die Personensorgeberechtigten befragt hat und entsprechende Dokumente gemäß Absatz 1 von den Personensorgeberechtigen vorlagen.
Ein Rückgriff hinsichtlich der Erstattungen gemäß § 5 findet gegenüber dem Träger der Kindertagesstätte für das betroffene Betreuungsverhältnis nicht statt, wenn die Absätze 1 und 2 fahrlässig nicht beachtet wurden.
Für die Zukunft entfällt in diesem Fall jedoch der Ausgleichsanspruch gemäß § 5 für das betroffene Betreuungsverhältnis.

(6) Die Absätze 1 bis 5 gelten bei einer Betreuung des Kindes in Kindertagespflege entsprechend.

#### § 5 Ausgleich entgangener Einnahmen der Einrichtungsträger

(1) Der Landkreis oder die kreisfreie Stadt gleichen den Trägern der Kindertagesstätten die aufgrund von § 2 Absatz 1 entstehenden Einnahmeausfälle in Höhe eines Pauschalbetrags von 12,50 Euro je Kind und Monat aus.
Der Ausgleichsbetrag wird für jede Kindertagesstätte auf der Grundlage der Anzahl der betreuten Kinder, deren Eltern ein Kostenbeitrag nach § 2 Absatz 1 nicht zuzumuten ist, nach Meldung gemäß § 3 Absatz 1 der Kindertagesstätten-Betriebskosten- und Nachweisverordnung und des Pauschalbetrags bemessen.
Maßgeblich sind die Stichtage 1.
September und 1. Dezember sowie 1. März und 1.
Juni.

(2) Der Landkreis oder die kreisfreie Stadt stellt auf Antrag des Trägers einer Kindertagesstätte nach Prüfung höhere Einnahmeausfälle als die nach Absatz 1 Satz 1 fest und gleicht diese aus.
Dabei muss der Träger der Kindertagesstätte durch geeignete Unterlagen nachweisen, dass sein Elternbeitrag, der über dem Pauschalbetrag gemäß Absatz 1 Satz 1 liegt, den von § 2 Absatz 1 betroffenen Personensorgeberechtigten im Einzelfall zugemutet werden kann.
Der Landkreis oder die kreisfreie Stadt prüft als örtlicher Träger der öffentlichen Jugendhilfe die Rechtmäßigkeit der zugrunde liegenden Beitragsregelungen.
Für den Ausgleich höherer Einnahmeausfälle ist einmal jährlich bis zum 1. September für das ablaufende Kalenderjahr ein Antrag zu stellen.
Soweit abweichende Vereinbarungen zwischen dem Landkreis und kreisangehörigen Gemeinden und Ämtern über die Finanzierung gemäß § 12 Absatz 1 Satz 3 des Kindertagesstättengesetzes getroffen wurden, sind die Bestimmungen der Absätze 1 und 2 Satz 1 bis 4 sowie Absatz 3 sinngemäß anzuwenden.

(3) Der Pauschalbetrag gemäß Absatz 1 Satz 1 wird im Hinblick auf die Angemessenheit seiner Höhe erstmals für das Kita-Jahr 2021/2022 und danach alle zwei Jahre überprüft.
Das für Jugend zuständige Mitglied der Landesregierung stellt den Pauschalbetrag fest.
Der Pauschalbetrag ist im Amtsblatt der obersten Landesjugendbehörde bekannt zu machen.

(4) Der Landkreis oder die kreisfreie Stadt zahlt die Ausgleichsbeträge zur Erstattung der Einnahmeausfälle gemäß Absatz 1 den Trägern der Kindertagesstätten zweckgebunden zu den Zahlungsterminen gemäß § 3 Absatz 5 der Kindertagesstätten-Betriebskosten- und Nachweisverordnung aus.
Die höheren Erstattungsbeträge gemäß Absatz 2 werden zum 1. November ausgereicht.
Im Jahr 2019 werden die Ausgleichsbeträge zur Erstattung der Einnahmeausfälle gemäß den Absätzen 1 und 2 zum 1.
November auf Basis der Daten zum Stichtag 1.
September rückwirkend für die Zeit ab Beginn des Kita-Jahres 2019/2020 ausgereicht.

(5) Der Träger der Kindertagesstätte stellt dem Landkreis oder der kreisfreien Stadt und diese stellen der obersten Landesjugendbehörde die zur Durchführung der Elternbeitragsbefreiung erforderlichen Daten zur Verfügung.
Sozialdaten sind zu anonymisieren oder zu pseudonymisieren, soweit die Aufgabenerfüllung dies zulässt.
Personenbezogene Daten sind nicht zu übermitteln.

(6) Die Landkreise und kreisfreien Städte nehmen den Ausgleich der Einnahmeausfälle bei den Trägern der Kindertagesstätten gemäß den Absätzen 1 und 2 als Pflichtaufgabe zur Erfüllung nach Weisung wahr.

#### § 6 Kostenausgleich für die Elternbeitragsbefreiung durch das Land

(1) Das Land gewährt den Landkreisen und kreisfreien Städten die erforderlichen Mittel zum Ausgleich der nach § 5 entstehenden Kosten sowie der Einnahmeausfälle aufgrund § 17 Absatz 1a des Kindertagesstättengesetzes für die Betreuung von Kindern in Kindertagespflege.
Für die Ermittlung des Ausgleichsbetrags bei Kindertagespflege gilt § 5 entsprechend.
Der Ausgleichsbetrag jedes Landkreises und jeder kreisfreien Stadt wird auf der Grundlage des Mittels der gemäß § 6 Absatz 1 Satz 3 der Kindertagesstätten-Betriebskosten- und Nachweisverordnung gemeldeten Anzahl der betreuten Kinder, deren Eltern ein Kostenbeitrag nach § 2 Absatz 1 nicht zuzumuten ist, im Zuständigkeitsbereich eines örtlichen Trägers der öffentlichen Jugendhilfe und des Pauschalbetrags gemäß § 5 Absatz 1 Satz 1 bemessen.
Maßgeblich sind die Stichtage 1.
September und 1. Dezember des Vorjahres sowie 1.
März und 1.
Juni des Jahres der Meldung.
In den Jahren 2019 und 2020 gilt der Stichtag 1.
September 2019.

(2) Das Land gleicht den Landkreisen und kreisfreien Städten auf Antrag nachgewiesene höhere Ausgleichsbeträge aus.
Der Antrag ist innerhalb von zwei Monaten nach Ende der Antragsfrist des § 5 Absatz 2 Satz 4 für die Träger der Kindertageseinrichtungen zu stellen.
Mit dem Antrag ist der in dem Zuständigkeitsbereich des jeweiligen örtlichen Trägers der öffentlichen Jugendhilfe entstehende erhöhte Ausgleichsbetrag und seine Berechnung nachzuweisen.
Für den Nachweis erhöhter Ausgleichsbeträge kann die oberste Landesjugendbehörde durch Verwaltungsvorschrift Vorgaben machen und ein elektronisches Antrags- und Nachweisverfahren regeln.

(3) Die Auszahlung der Ausgleichsbeträge an die Landkreise oder kreisfreien Städte erfolgt zu den in § 5 Absatz 1 Satz 2 der Kindertagesstätten-Betriebskosten- und Nachweisverordnung genannten Terminen.
Im Jahr 2019 erfolgt die Auszahlung der Ausgleichsbeträge an den örtlichen Träger der öffentlichen Jugendhilfe nach Absatz 1 für fünf Monate bis zum 1.
Dezember 2019.
Die Erstattung der durch den örtlichen Träger der öffentlichen Jugendhilfe nachgewiesenen erhöhten Ausgleichsbeträge nach Absatz 2 erfolgt zum 15.
Dezember des jeweiligen Jahres.

#### § 7 Verwaltungskostenausgleich

Zum Ausgleich des Verwaltungsaufwands für den Vollzug der Aufgaben nach dieser Verordnung erhalten die Landkreise und kreisfreien Städte einen Verwaltungskostenausgleich.
Die Höhe des Ausgleichs ergibt sich aus dem Aufwand für die Ermittlung der zu erstattenden Einnahmeausfälle und die Auszahlung der Beträge sowie die Bearbeitung der Anträge gemäß § 5 Absatz 2.
Für den Aufwand werden jährlich je Kindertageseinrichtung, in der Kinder betreut werden, deren Eltern ein Kostenbeitrag nach § 2 Absatz 1 nicht zuzumuten ist, eine Stunde einer Kraft im gehobenen nichttechnischen Verwaltungsdienst der fünften Entwicklungsstufe der Entgeltgruppe 9b des Tarifvertrags für den öffentlichen Dienst (Kommunen) und ein zusätzlicher Gemeinkostenanteil von 30 Prozent der dafür aufzuwendenden Personalkosten angesetzt.
Der Landkreis oder die kreisfreie Stadt kann auf Antrag höhere Verwaltungskosten als nach Satz 3 gegenüber der obersten Landesjugendbehörde geltend machen, soweit seine oder ihre nachgewiesenen notwendigen Verwaltungskosten für den Vollzug der Aufgaben nach dieser Verordnung höher sind, als die nachzuweisenden ersparten Aufwendungen durch das Wegfallen der Bearbeitung von Anträgen nach § 90 des Achten Buches Sozialgesetzbuch aufgrund dieser Verordnung.
Der Antrag ist bis zum 1.
November für das ablaufende Kalenderjahr zu stellen.
Die Mittel werden mit den Zahlungen gemäß § 6 durch das Land ausgereicht.

#### § 8 Einvernehmensfähigkeit neuer Beitragsregelung

Bei der Herstellung des Einvernehmens über die Grundsätze der Höhe und Staffelung der Elternbeiträge gemäß § 17 Absatz 3 Satz 2 des Kindertagesstättengesetzes mit dem Landkreis oder mit der kreisfreien Stadt zu Beitragsregelungen, die nach dem 1.
August 2019 wirksam werden, sind die Vorgaben nach dieser Verordnung zu beachten.

#### § 9 Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
August 2019 in Kraft.

Potsdam, den 16.
August 2019

Die Ministerin für Bildung,  
Jugend und Sport

Britta Ernst

* * *

### Anlagen

1

[Anlage (zu § 3 Absatz 1)](/br2/sixcms/media.php/68/GVBl_II_61_2019-Anlage.pdf "Anlage (zu § 3 Absatz 1)") 145.1 KB