## Verordnung über Leitlinien zu Ausbildungsinhalten für Ingenieurinnen und Ingenieure (Ingenieurausbildungsinhaltsverordnung - IngAusInhV)

Auf Grund des § 34 Satz 1 Nummer 1 des Brandenburgischen Ingenieurgesetzes vom 25. Januar 2016 (GVBl. I Nr. 4) verordnet die Ministerin für Infrastruktur und Landesplanung:

#### § 1 Anforderungen an die Studiendauer

Der Studiengang gemäß § 1 Absatz 1 Satz 1 Nummer 1 des Brandenburgischen Ingenieurgesetzes muss eine Regelstudienzeit von mindestens sechs Semestern auf Vollzeitbasis an einer deutschen Hochschule aufweisen.
Studiengänge auf Teilzeitbasis und Ausbildungsgänge an einer Berufsakademie werden anerkannt, wenn sie hinsichtlich der Studiendauer und der Studieninhalte gleichwertig sind.

#### § 2 Anforderungen an die Studieninhalte

(1) Bei einem Studiengang nach § 1, der kein Diplomstudiengang ist, müssen mindestens 180 ECTS-Leistungspunkte (Credit Points) in Studienfächern erworben werden.
Zudem müssen die folgenden Anforderungen erfüllt sein:

1.  Für den ersten berufsqualifizierenden Studienabschluss müssen mindestens 91 ECTS-Punkte in Studienfächern erworben werden, die den Bereichen
    
    *   Mathematik,
    *   Informatik,
    *   Naturwissenschaften und
    *   Technik
    
    zugeordnet werden können.
    
2.  Die Inhalte des Studienganges müssen auf die Berufsaufgaben nach § 3 des Brandenburgischen Ingenieurgesetzes sowie auf die beruflichen Fähigkeiten und Tätigkeiten der Ingenieurinnen und Ingenieure ausgerichtet sein.
    Zu den beruflichen Fähigkeiten und Tätigkeiten gehören insbesondere folgende Fachkompetenzen und die zugehörigen Methoden und Techniken:
    1.  Beherrschen der mathematisch-naturwissenschaftlichen und ingenieurwissenschaftlich-technischen Grundlagen und Spezialdisziplinen der jeweiligen Studienrichtung,
    2.  Analysieren und Bearbeiten von ingenieurwissenschaftlichen Aufgabenstellungen unter Auswertung und Anwendung aktueller naturwissenschaftlicher Untersuchungsergebnisse,
    3.  Berücksichtigen der sozialen, ökologischen, wirtschaftlichen und rechtlichen Rahmenbedingungen und Auswirkungen sowie möglicher Gesundheits- und Sicherheitsfragen.

(2) Bei einem Diplomstudiengang nach § 1 muss der zeitliche Umfang von Studienfächern, die den Bereichen Mathematik, Informatik, Naturwissenschaften und Technik zugeordnet werden können, mindestens 51 Prozent betragen.
Absatz 1 Satz 2 Nummer 2 gilt entsprechend.

(3) Für das Führen der Bezeichnung „Wirtschaftsingenieurin“ oder „Wirtschaftsingenieur“ durch Personen, die ein grundständiges Studium des Wirtschaftsingenieurwesens absolviert haben, muss der Studiengang von den Fächern der vier genannten Bereiche Mathematik, Informatik, Naturwissenschaften und Technik zumindest geprägt sein.

#### § 3 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 1.
Juni 2017

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#*) Diese Verordnung dient der Umsetzung der Richtlinie 2013/55/EU des Europäischen Parlaments und des Rates vom 20.
November 2013 zur Änderung der Richtlinie 2005/36/EG über die Anerkennung von Berufsqualifikationen und der Verordnung (EU) Nr.
1024/2012 über die Verwaltungszusammenarbeit mit Hilfe des Binnenmarkt-Informationssystems („IMI-Verordnung“) (ABl. L 354 vom 28.12.2013, S. 132).