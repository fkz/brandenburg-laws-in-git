## Verordnung über den Sitz, die Zuständigkeiten und Aufgaben der staatlichen Schulämter (Schulämterverordnung - SchÄV)

Auf Grund

*   des § 1 Absatz 2 des Schulämtererrichtungsgesetzes vom 25.
    Januar 2016 (GVBl. I Nr. 5 S. 7),
*   des § 61 Absatz 1 und des § 131 Absatz 3 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
    August 2002 (GVBl.
    I S. 78), von denen durch Artikel 7 des Gesetzes vom 25.
    Januar 2016 (GVBl.
    I Nr. 5 S. 9) § 61 Absatz 1 geändert und § 131 Absatz 3 neu gefasst worden ist,
*   des § 8 Absatz 1 des Brandenburgischen Berufsqualifikationsfeststellungsgesetzes vom 5. Dezember 2013 (GVBl.
    I Nr. 37) und
*   des § 29 des Brandenburgischen Sozialberufsgesetzes vom 3.
    Dezember 2008 (GVBl. I S. 278)

in Verbindung mit § 8 Absatz 2 und 3 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl. I S. 186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28) neu gefasst worden ist, verordnet der Minister für Bildung, Jugend und Sport:

#### § 1 Sitz und Zuständigkeit

(1) Die zuständigen staatlichen Schulämter gemäß § 131 Absatz 2 des Brandenburgischen Schulgesetzes sind:

1.  das Staatliche Schulamt Brandenburg an der Havel; es ist zuständig für die Landkreise Teltow-Fläming und Potsdam-Mittelmark sowie für die kreisfreien Städte Brandenburg an der Havel und Potsdam und hat seinen Sitz in der Stadt Brandenburg an der Havel,
2.  das Staatliche Schulamt Cottbus; es ist zuständig für die Landkreise Dahme-Spreewald, Spree-Neiße, Oberspreewald-Lausitz und Elbe-Elster sowie die kreisfreie Stadt Cottbus und hat seinen Sitz in der Stadt Cottbus,
3.  das Staatliche Schulamt Frankfurt (Oder); es ist zuständig für die Landkreise Uckermark, Barnim, Märkisch-Oderland und Oder-Spree sowie für die kreisfreie Stadt Frankfurt (Oder) und hat seinen Sitz in der Stadt Frankfurt (Oder) sowie
4.  das Staatliche Schulamt Neuruppin; es ist zuständig für die Landkreise Prignitz, Ostprignitz-Ruppin, Havelland und Oberhavel und hat seinen Sitz in der Stadt Neuruppin.

(2) Die staatlichen Schulämter tragen im Rahmen der Ausübung der Fach- und Dienstaufsicht über die in ihrem Zuständigkeitsbereich liegenden Schulen insbesondere die Verantwortung für die Bereiche Personalentwicklung, pädagogische Schulentwicklung sowie Qualitätsentwicklung und Qualitätssicherung.

#### § 2 Übertragung von Aufgaben

Den staatlichen Schulämtern werden die in der Anlage festgelegten Zuständigkeiten für die Anerkennung sonstiger schulischer Abschlüsse und Berechtigungen, die außerhalb des Landes Brandenburg erworben wurden, die Feststellung der Gleichwertigkeit nicht reglementierter Berufe sowie die Feststellung der Gleichwertigkeit von Abschlüssen in Erzieherberufen, die außerhalb des Landes Brandenburg erworben wurden und deren staatliche Anerkennung, übertragen.

#### § 3 Übertragung überregionaler Aufgaben

Zur Ausübung der Schulaufsicht werden den staatlichen Schulämtern für den Bereich anderer staatlicher Schulämter die in der Anlage festgelegten Zuständigkeiten übertragen.

#### § 4 Inkrafttreten, Außerkrafttreten

(1) Diese Verordnung tritt mit Wirkung vom 1.
Februar 2016 in Kraft und am 31.
Januar 2023 außer Kraft.

(2) Die Aufgabenübertragungs-Verordnung MBJS vom 18.
April 2002 (GVBl.
II S. 247), die zuletzt durch die Verordnung vom 27.
September 2012 (GVBl.
II Nr. 84) geändert worden ist, tritt mit Wirkung vom 1.
Februar 2016 außer Kraft.

Potsdam, den 1.
März 2016

Der Minister für Bildung,  
Jugend und Sport

Günter Baaske

* * *

### Anlagen

1

[Anlage - Verzeichnis der überregionalen Zuständigkeiten der Schulämter](/br2/sixcms/media.php/68/Sch%C3%84V-Anlage.pdf "Anlage - Verzeichnis der überregionalen Zuständigkeiten der Schulämter") 310.3 KB