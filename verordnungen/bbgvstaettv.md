## Verordnung über den Bau und Betrieb von Versammlungsstätten im Land Brandenburg (Brandenburgische Versammlungsstättenverordnung - BbgVStättV)

Auf Grund des § 86 Absatz 1 Nummer 1 und Nummer 4 bis 6, Absatz 3 Satz 1 Nummer 2, 4, 5, 7 und 8 sowie Absatz 3 Satz 2 in Verbindung mit § 85 Absatz 1 Satz 1 Nummer 1 der Brandenburgischen Bauordnung vom 19.
Mai 2016 (GVBl.
I Nr. 14) verordnet die Ministerin für Infrastruktur und Landesplanung:

**Inhaltsübersicht**

Teil 1  
Allgemeine Vorschriften
--------------------------------

[§ 1 Anwendungsbereich, Anzahl der Besucherinnen und Besucher](#1)

[§ 2 Begriffe](#2)

Teil 2  
Allgemeine Bauvorschriften
-----------------------------------

### Abschnitt 1  
Bauteile und Baustoffe

[§ 3 Bauteile](#3)

[§ 4 Dächer](#4)

[§ 5 Dämmstoffe, Unterdecken, Bekleidungen und Bodenbeläge](#5)

### Abschnitt 2  
Rettungswege

[§ 6 Führung der Rettungswege](#6)

[§ 7 Bemessung der Rettungswege](#7)

[§ 8 Treppen](#8)

[§ 9 Türen und Tore](#9)

### Abschnitt 3  
Besucherplätze und Einrichtungen für Besucherinnen und Besucher

[§ 10 Bestuhlung, Gänge und Stufengänge](#10)

[§ 11 Abschrankungen und Schutzvorrichtungen](#11)

[§ 12 Toilettenräume](#12)

[§ 13 Barrierefreie Stellplätze](#13)

### Abschnitt 4  
Technische Anlagen und Einrichtungen, besondere Räume

[§ 14 Sicherheitsstromversorgungsanlagen, elektrische Anlagen und Blitzschutzanlagen](#14)

[§ 15 Sicherheitsbeleuchtung](#15)

[§ 16 Rauchableitung](#16)

[§ 17 Heizungsanlagen und Lüftungsanlagen](#17)

[§ 18 Stände und Arbeitsgalerien für Licht-, Ton-, Bild- und Regieanlagen](#18)

[§ 19 Feuerlöscheinrichtungen und -anlagen](#19)

[§ 20 Brandmelde- und Alarmierungsanlagen, Brandmelder- und Alarmzentrale, Brandfallsteuerung der Aufzüge](#20)

[§ 21 Werkstätten, Magazine und Lagerräume](#21)

Teil 3  
Besondere Bauvorschriften
----------------------------------

### Abschnitt 1  
Großbühnen

[§ 22 Bühnenhaus](#22)

[§ 23 Schutzvorhang](#23)

[§ 24 Feuerlösch- und Brandmeldeanlagen](#24)

[§ 25 Platz für die Brandsicherheitswache](#25)

### Abschnitt 2  
Versammlungsstätten mit mehr als 5 000 Besucherplätzen

[§ 26 Räume für Lautsprecherzentrale, Polizei, Feuerwehr, Sanitäts- und Rettungsdienst](#26)

[§ 27 Abschrankung und Blockbildung in Sportstadien mit mehr als 10 000 Besucherplätzen](#27)

[§ 28 Wellenbrecher](#28)

[§ 29 Abschrankung von Stehplätzen vor Szenenflächen](#29)

[§ 30 Einfriedungen und Eingänge](#30)

Teil 4  
Betriebsvorschriften
-----------------------------

### Abschnitt 1  
Rettungswege, Besucherplätze

[§ 31 Rettungswege, Flächen für die Feuerwehr](#31)

[§ 32 Besucherplätze nach dem Bestuhlungs- und Rettungswegeplan](#32)

### Abschnitt 2  
Brandverhütung

[§ 33 Vorhänge, Sitze, Ausstattungen, Requisiten und Ausschmückungen](#33)

[§ 34 Aufbewahrung von Ausstattungen, Requisiten, Ausschmückungen und brennbarem Material](#34)

[§ 35 Rauchen, Verwendung von offenem Feuer und pyrotechnischen Gegenständen](#35)

### Abschnitt 3  
Betrieb technischer Einrichtungen

[§ 36 Bedienung und Wartung der technischen Einrichtungen](#36)

[§ 37 Laseranlagen](#37)

### Abschnitt 4  
Verantwortliche Personen, besondere Betriebsvorschriften

[§ 38 Pflichten der Betreiberinnen und Betreiber, Veranstalterinnen und Veranstalter und beauftragten Personen](#38)

[§ 39 Verantwortliche Personen für Veranstaltungstechnik](#39)

[§ 40 Aufgaben und Pflichten der verantwortlichen Personen für Veranstaltungstechnik, technische Probe](#40)

[§ 41 Brandsicherheitswache, Sanitäts- und Rettungsdienst](#41)

[§ 42 Brandschutzordnung, Räumungskonzept, Feuerwehrpläne](#42)

[§ 43 Sicherheitskonzept, Ordnungsdienst](#43)

Teil 5  
Zusätzliche Bauvorlagen
--------------------------------

[§ 44 Zusätzliche Bauvorlagen, Bestuhlungs- und Rettungswegeplan](#44)

[§ 45 Gastspielprüfbuch](#45)

Teil 6  
Bestehende Versammlungsstätten
---------------------------------------

[§ 46 Anwendung der Vorschriften auf bestehende Versammlungsstätten](#46)

Teil 7  
Schlussvorschriften
----------------------------

[§ 47 Ordnungswidrigkeiten](#47)

[§ 48 Inkrafttreten, Außerkrafttreten](#48)

Teil 1  
Allgemeine Vorschriften
--------------------------------

#### § 1 Anwendungsbereich, Anzahl der Besucherinnen und Besucher

(1) Die Vorschriften dieser Verordnung gelten für den Bau und Betrieb von

1.  Versammlungsstätten mit Versammlungsräumen, die einzeln mehr als 200 Besucherinnen und Besucher fassen.
    Sie gelten auch für Versammlungsstätten mit mehreren Versammlungsräumen, die insgesamt mehr als 200 Besucherinnen und Besucher fassen, wenn diese Versammlungsräume gemeinsame Rettungswege haben,
2.  Versammlungsstätten im Freien mit Szenenflächen und Tribünen, die keine fliegenden Bauten sind und insgesamt mehr als 1 000 Besucherinnen und Besucher fassen,
3.  Sportstadien und Freisportanlagen mit Tribünen, die keine fliegenden Bauten sind, und die jeweils insgesamt mehr als 5 000 Besucherinnen und Besucher fassen.

(2) Soweit sich aus den Bauvorlagen nichts anderes ergibt, ist die Anzahl der Besucherinnen und Besucher im Sinne dieser Verordnung wie folgt zu ermitteln:

1.
für Sitzplätze an Tischen

ein Besucher je Quadratmeter Grundfläche des Versammlungsraumes,

2.
für Sitzplätze in Reihen

zwei Besucher je Quadratmeter Grundfläche des Versammlungsraumes,

3.
für Stehplätze auf Stufenreihen

zwei Besucher je laufender Meter Stufenreihe,

4.
bei Ausstellungsräumen

ein Besucher je Quadratmeter Grundfläche des Versammlungsraumes,

5.
für sonstige Stehplätze

mindestens zwei Besucher je Quadratmeter Grundfläche.

Für Besucherinnen und Besucher nicht zugängliche Flächen werden in die Berechnung nicht einbezogen.
Für Versammlungsstätten im Freien, für Freisportanlagen und für Sportstadien gelten Satz 1 Nummer 1 bis 3 und Nummer 5 sowie Satz 2 entsprechend.

(3) Die Vorschriften dieser Verordnung gelten nicht für

1.  Räume, die dem Gottesdienst gewidmet sind,
2.  Unterrichtsräume in allgemein- und berufsbildenden Schulen,
3.  Ausstellungsräume in Museen,
4.  Fliegende Bauten.

(4) Werden bauliche Anlagen, die für eine andere Nutzung bauaufsichtlich genehmigt sind, im Einzelfall als Versammlungsstätte genutzt, sind die Bestimmungen dieser Verordnung entsprechend anzuwenden.

(5) Soweit in dieser Verordnung nichts Abweichendes geregelt ist, sind auf tragende und aussteifende sowie auf raumabschließende Bauteile die Anforderungen der Brandenburgischen Bauordnung an diese Bauteile in Gebäuden der Gebäudeklasse 5 anzuwenden.
Die Erleichterungen des § 30 Absatz 3 Satz 2, § 31 Absatz 4 Nummer 1 und 2, § 36 Absatz 1 Satz 2 Nummer 2, § 39 Absatz 1 Nummer 4, § 40 Absatz 1 Nummer 1 und 3 sowie des § 41 Absatz 5 Nummer 1 und 3 der Brandenburgischen Bauordnung sind nicht anzuwenden.

#### § 2 Begriffe

(1) Versammlungsstätten sind bauliche Anlagen oder Teile baulicher Anlagen, die für die gleichzeitige Anwesenheit vieler Menschen bei Veranstaltungen, insbesondere erzieherischer, wirtschaftlicher, geselliger, kultureller, künstlerischer, politischer, sportlicher oder unterhaltender Art, bestimmt sind sowie Schank- und Speisewirtschaften.

(2) Erdgeschossige Versammlungsstätten sind Gebäude mit nur einem Geschoss ohne Ränge oder Emporen, dessen Fußboden an keiner Stelle mehr als 1 Meter unter der Geländeoberfläche liegt; dabei bleiben Geschosse außer Betracht, die ausschließlich der Unterbringung technischer Anlagen und Einrichtungen dienen.

(3) Versammlungsräume sind Räume für Veranstaltungen oder für den Verzehr von Speisen und Getränken.
Hierzu gehören auch Aulen und Foyers, Vortrags- und Hörsäle sowie Studios.

(4) Szenenflächen sind Flächen für künstlerische und andere Darbietungen; für Darbietungen bestimmte Flächen unter 20 Quadratmeter gelten nicht als Szenenflächen.

(5) In Versammlungsstätten mit einem Bühnenhaus ist

1.  das Zuschauerhaus der Gebäudeteil, der die Versammlungsräume und die mit ihnen in baulichem Zusammenhang stehenden Räume umfasst,
2.  das Bühnenhaus der Gebäudeteil, der die Bühnen und die mit ihnen in baulichem Zusammenhang stehenden Räume umfasst,
3.  die Bühnenöffnung die Öffnung in der Trennwand zwischen der Hauptbühne und dem Versammlungsraum,
4.  die Bühne der hinter der Bühnenöffnung liegende Raum mit Szenenflächen; zur Bühne zählen die Hauptbühne sowie die Hinter- und Seitenbühnen einschließlich der jeweils zugehörigen Ober- und Unterbühnen,
5.  eine Großbühne eine Bühne
    1.  mit einer Szenenfläche hinter der Bühnenöffnung von mehr als 200 Quadratmeter,
    2.  mit einer Oberbühne mit einer lichten Höhe von mehr als 2,5 Meter über der Bühnenöffnung oder
    3.  mit einer Unterbühne,
6.  die Unterbühne der begehbare Teil des Bühnenraumes unter dem Bühnenboden, der zur Unterbringung einer Untermaschinerie geeignet ist,
7.  die Oberbühne der Teil des Bühnenraumes über der Bühnenöffnung, der zur Unterbringung einer Obermaschinerie geeignet ist.

(6) Mehrzweckhallen sind überdachte Versammlungsstätten für verschiedene Veranstaltungsarten.

(7) Studios sind Produktionsstätten für Film, Fernsehen und Hörfunk und mit Besucherplätzen.

(8) Foyers sind Empfangs- und Pausenräume für Besucherinnen und Besucher.

(9) Ausstattungen sind Bestandteile von Bühnen- oder Szenenbildern.
Hierzu gehören insbesondere Wand-, Fußboden- und Deckenelemente, Bildwände, Treppen und sonstige Bühnenbildteile.

(10) Requisiten sind bewegliche Einrichtungsgegenstände von Bühnen- oder Szenenbildern.
Hierzu gehören insbesondere Möbel, Leuchten, Bilder und Geschirr.

(11) Ausschmückungen sind vorübergehend eingebrachte Dekorationsgegenstände.
Zu den Ausschmückungen gehören insbesondere Drapierungen, Girlanden, Fahnen und künstlicher Pflanzenschmuck.

(12) Sportstadien sind Versammlungsstätten mit Tribünen für Besucherinnen und Besucher und mit nicht überdachten Sportflächen.

(13) Tribünen sind bauliche Anlagen mit ansteigenden Steh- oder Sitzplatzreihen (Stufenreihen) für Besucherinnen und Besucher.

(14) Innenbereich ist die von Tribünen umgebene Fläche für Darbietungen.

Teil 2  
Allgemeine Bauvorschriften
-----------------------------------

### Abschnitt 1  
Bauteile und Baustoffe

#### § 3 Bauteile

(1) Tragende und aussteifende Bauteile, wie Wände, Pfeiler, Stützen und Decken, müssen feuerbeständig, in erdgeschossigen Versammlungsstätten feuerhemmend sein.
Satz 1 gilt nicht für erdgeschossige Versammlungsstätten mit automatischen Feuerlöschanlagen.

(2) Außenwände mehrgeschossiger Versammlungsstätten müssen aus nichtbrennbaren Baustoffen bestehen.

(3) Trennwände sind erforderlich zum Abschluss von Versammlungsräumen und Bühnen.
Diese Trennwände müssen feuerbeständig, in erdgeschossigen Versammlungsstätten mindestens feuerhemmend sein.
In der Trennwand zwischen der Bühne und dem Versammlungsraum ist eine Bühnenöffnung zulässig.

(4) Werkstätten, Magazine und Lagerräume sowie Räume unter Tribünen und Podien müssen feuerbeständige Trennwände und Decken haben.

(5) Der Fußboden von Szenenflächen muss fugendicht sein.
Betriebsbedingte Öffnungen sind zulässig.
Die Unterkonstruktion, mit Ausnahme der Lagerhölzer, muss aus nichtbrennbaren Baustoffen bestehen.
Räume unter dem Fußboden, die nicht zu einer Unterbühne gehören, müssen feuerbeständige Wände und Decken haben.

(6) Die Unterkonstruktion der Fußböden von Tribünen und Podien, die veränderbare Einbauten in Versammlungsräumen sind, muss aus nichtbrennbaren Baustoffen bestehen; dies gilt nicht für Podien mit insgesamt nicht mehr als 20 Quadratmeter Fläche.

(7) Veränderbare Einbauten sind so auszubilden, dass sie in ihrer Standsicherheit nicht durch dynamische Schwingungen gefährdet werden können.

#### § 4 Dächer

(1) Tragwerke von Dächern, die den oberen Abschluss von Räumen der Versammlungsstätte bilden oder die von diesen Räumen nicht durch feuerbeständige Bauteile getrennt sind, müssen feuerhemmend sein.
Tragwerke von Dächern über Tribünen und Szenenflächen im Freien müssen mindestens feuerhemmend sein oder aus nichtbrennbaren Baustoffen bestehen.
Satz 1 gilt nicht für Versammlungsstätten mit automatischen Feuerlöschanlagen.

(2) Bedachungen, ausgenommen Dachhaut und Dampfsperre, müssen bei Dächern, die den oberen Abschluss von Räumen der Versammlungsstätte bilden oder die von diesen Räumen nicht durch feuerbeständige Bauteile getrennt sind, aus nichtbrennbaren Baustoffen hergestellt werden.
Dies gilt nicht für Bedachungen über Versammlungsräumen mit nicht mehr als 1 000 Quadratmeter Grundfläche.

(3) Lichtdurchlässige Bedachungen über Versammlungsräumen müssen aus nichtbrennbaren Baustoffen bestehen.
Bei Versammlungsräumen mit automatischen Feuerlöschanlagen genügen schwerentflammbare Baustoffe, die nicht brennend abtropfen können.

#### § 5 Dämmstoffe, Unterdecken, Bekleidungen und Bodenbeläge

(1) Dämmstoffe müssen aus nichtbrennbaren Baustoffen bestehen.

(2) Bekleidungen an Wänden in Versammlungsräumen müssen aus mindestens schwerentflammbaren Baustoffen bestehen.
In Versammlungsräumen mit nicht mehr als 1 000 Quadratmeter Grundfläche genügen geschlossene nicht hinterlüftete Holzbekleidungen.

(3) Unterdecken und Bekleidungen an Decken in Versammlungsräumen müssen aus nichtbrennbaren Baustoffen bestehen.
In Versammlungsräumen mit nicht mehr als 1 000 Quadratmeter Grundfläche genügen Bekleidungen aus mindestens schwerentflammbaren Baustoffen oder geschlossene nicht hinterlüftete Holzbekleidungen.

(4) In Foyers, durch die Rettungswege aus anderen Versammlungsräumen führen, in notwendigen Treppenräumen, Räumen zwischen notwendigen Treppenräumen und Ausgängen ins Freie sowie notwendigen Fluren müssen Unterdecken und Bekleidungen aus nichtbrennbaren Baustoffen bestehen.

(5) Unterdecken und Bekleidungen, die mindestens schwerentflammbar sein müssen, dürfen nicht brennend abtropfen.

(6) Unterkonstruktionen, Halterungen und Befestigungen von Unterdecken und Bekleidungen nach den Absätzen 2 bis 4 müssen aus nichtbrennbaren Baustoffen bestehen; dies gilt nicht für Versammlungsräume mit nicht mehr als 100 Quadratmeter Grundfläche.
In den Hohlräumen hinter Unterdecken und Bekleidungen aus brennbaren Baustoffen dürfen Kabel und Leitungen nur in Installationsschächten oder Installationskanälen aus nichtbrennbaren Baustoffen verlegt werden.

(7) In notwendigen Treppenräumen, Räumen zwischen notwendigen Treppenräumen und Ausgängen ins Freie müssen Bodenbeläge nichtbrennbar sein.
In notwendigen Fluren sowie in Foyers, durch die Rettungswege aus anderen Versammlungsräumen führen, müssen Bodenbeläge mindestens schwerentflammbar sein.

### Abschnitt 2  
Rettungswege

#### § 6 Führung der Rettungswege

(1) Rettungswege müssen ins Freie zu öffentlichen Verkehrsflächen führen.
Zu den Rettungswegen von Versammlungsstätten gehören insbesondere die frei zu haltenden Gänge und Stufengänge, die Ausgänge aus Versammlungsräumen, die notwendigen Flure und notwendigen Treppen, die Ausgänge ins Freie, die als Rettungsweg dienenden Balkone, Dachterrassen und Außentreppen sowie die Rettungswege im Freien auf dem Grundstück.

(2) Versammlungsstätten müssen in jedem Geschoss mit Aufenthaltsräumen mindestens zwei voneinander unabhängige bauliche Rettungswege haben; dies gilt für Tribünen entsprechend.
Die Führung beider Rettungswege innerhalb eines Geschosses durch einen gemeinsamen notwendigen Flur ist zulässig.
Rettungswege dürfen über Balkone, Dachterrassen und Außentreppen auf das Grundstück führen, wenn sie im Brandfall sicher begehbar sind.

(3) Rettungswege dürfen über Gänge und Treppen durch Foyers oder Hallen zu Ausgängen ins Freie geführt werden, soweit mindestens ein weiterer von dem Foyer oder der Halle unabhängiger baulicher Rettungsweg vorhanden ist.
Foyers oder Hallen dürfen nicht als Raum zwischen notwendigen Treppenräumen und Ausgängen ins Freie im Sinne des § 35 Absatz 3 Satz 2 der Brandenburgischen Bauordnung dienen.

(4) Versammlungsstätten müssen für Geschosse mit jeweils mehr als 800 Besucherplätzen nur diesen Geschossen zugeordnete Rettungswege haben.

(5) Versammlungsräume und sonstige Aufenthaltsräume, die für mehr als 100 Besucherinnen und Besucher bestimmt sind oder mehr als 100 Quadratmeter Grundfläche haben, müssen jeweils mindestens zwei möglichst weit auseinander und entgegengesetzt liegende Ausgänge ins Freie oder zu Rettungswegen haben.
Die nach § 7 Absatz 4 Satz 1 ermittelte Breite ist möglichst gleichmäßig auf die Ausgänge zu verteilen; die Mindestbreiten nach § 7 Absatz 4 Satz 3 und 4 bleiben unberührt.

(6) Ausgänge und sonstige Rettungswege müssen durch Sicherheitszeichen dauerhaft und gut sichtbar gekennzeichnet sein.

#### § 7 Bemessung der Rettungswege

(1) Die Entfernung von jedem Besucherplatz bis zum nächsten Ausgang aus dem Versammlungsraum darf nicht länger als 30 Meter sein.
Bei mehr als 5 Meter lichter Höhe ist je 2,5 Meter zusätzlicher lichter Höhe über der für Besucherinnen und Besucher zugänglichen Ebene für diesen Bereich eine Verlängerung der Entfernung um 5 Meter zulässig.
Die Entfernung von 60 Meter bis zum nächsten Ausgang darf nicht überschritten werden.
Die Sätze 1 bis 3 gelten für Tribünen außerhalb von Versammlungsräumen sinngemäß.

(2) Die Entfernung von jeder Stelle einer Bühne bis zum nächsten Ausgang darf nicht länger als 30 Meter sein.
Gänge zwischen den Wänden der Bühne und dem Rundhorizont oder den Dekorationen müssen eine lichte Breite von 1,20 Meter haben; in Großbühnen müssen diese Gänge vorhanden sein.

(3) Die Entfernung von jeder Stelle eines notwendigen Flures oder eines Foyers bis zum Ausgang ins Freie oder zu einem notwendigen Treppenraum darf nicht länger als 30 Meter sein.

(4) Die Breite der Rettungswege ist nach der größtmöglichen Personenzahl zu bemessen.
Dabei muss die lichte Breite eines jeden Teils von Rettungswegen für die darauf angewiesenen Personen mindestens betragen bei

1.
Versammlungsstätten im Freien sowie Sportstadien

1,20 Meter je 600 Personen,

2.
anderen Versammlungsstätten

1,20 Meter je 200 Personen;

Zwischenwerte sind zulässig.
Die lichte Mindestbreite eines jeden Teils von Rettungswegen muss 1,20 Meter betragen.
Bei Rettungswegen von Versammlungsräumen mit nicht mehr als 200 Besucherplätzen und bei Rettungswegen im Bühnenhaus genügt eine lichte Breite von 0,90 Meter.
Für Rettungswege von Arbeitsgalerien genügt eine Breite von 0,80 Meter.

(5) Ausstellungshallen müssen durch Gänge so unterteilt sein, dass die Tiefe der zur Aufstellung von Ausstellungsständen bestimmten Grundflächen (Ausstellungsflächen) nicht mehr als 30 Meter beträgt.
Die Entfernung von jeder Stelle auf einer Ausstellungsfläche bis zu einem Gang darf nicht mehr als 20 Meter betragen; sie wird auf die nach Absatz 1 bemessene Entfernung nicht angerechnet.
Die Gänge müssen auf möglichst geradem Weg zu entgegengesetzt liegenden Ausgängen führen.
Die lichte Breite der Gänge und der zugehörigen Ausgänge muss mindestens 3 Meter betragen.

(6) Die Entfernungen werden in der Lauflinie gemessen.

#### § 8 Treppen

(1) Die Führung der jeweils anderen Geschossen zugeordneten notwendigen Treppen in einem gemeinsamen notwendigen Treppenraum (Schachteltreppen) ist zulässig.

(2) Notwendige Treppen müssen feuerbeständig sein.
Für notwendige Treppen in notwendigen Treppenräumen oder als Außentreppen genügen nichtbrennbare Baustoffe.
Für notwendige Treppen von Tribünen und Podien als veränderbare Einbauten genügen Bauteile aus nichtbrennbaren Baustoffen und Stufen aus Holz.
Die Sätze 1 bis 3 gelten nicht für notwendige Treppen von Ausstellungsständen.

(3) Die lichte Breite notwendiger Treppen darf nicht mehr als 2,40 Meter betragen.

(4) Notwendige Treppen und dem allgemeinen Besucherverkehr dienende Treppen müssen auf beiden Seiten feste und griffsichere Handläufe ohne freie Enden haben.
Die Handläufe sind über Treppenabsätze fortzuführen.

(5) Notwendige Treppen und dem allgemeinen Besucherverkehr dienende Treppen müssen geschlossene Trittstufen haben; dies gilt nicht für Außentreppen.

(6) Wendeltreppen sind als notwendige Treppen für Besucherinnen und Besucher unzulässig.

#### § 9 Türen und Tore

(1) Türen und Tore in raumabschließenden Innenwänden, die feuerbeständig sein müssen, sowie in inneren Brandwänden, müssen mindestens feuerhemmend, rauchdicht und selbstschließend sein.

(2) Türen und Tore in raumabschließenden Innenwänden, die feuerhemmend sein müssen, müssen mindestens rauchdicht und selbstschließend sein.

(3) Türen in Rettungswegen müssen in Fluchtrichtung aufschlagen und dürfen keine Schwellen haben.
Während des Aufenthaltes von Personen in der Versammlungsstätte müssen die Türen der jeweiligen Rettungswege jederzeit von innen leicht und in voller Breite geöffnet werden können.

(4) Schiebetüren sind im Zuge von Rettungswegen unzulässig, dies gilt nicht für automatische Schiebetüren, die die Rettungswege nicht beeinträchtigen.
Pendeltüren müssen in Rettungswegen Vorrichtungen haben, die ein Durchpendeln der Türen verhindern.

(5) Türen, die selbstschließend sein müssen, dürfen offengehalten werden, wenn sie Einrichtungen haben, die bei Raucheinwirkung ein selbsttätiges Schließen der Türen bewirken; sie müssen auch von Hand geschlossen werden können.

(6) Mechanische Vorrichtungen zur Vereinzelung oder Zählung von Besucherinnen und Besuchern, wie Drehtüren oder -kreuze, sind in Rettungswegen unzulässig; dies gilt nicht für mechanische Vorrichtungen, die im Gefahrenfall von innen leicht und in voller Breite geöffnet werden können.

### Abschnitt 3  
Besucherplätze und Einrichtungen für Besucherinnen und Besucher

#### § 10 Bestuhlung, Gänge und Stufengänge

(1) In Reihen angeordnete Sitzplätze müssen unverrückbar befestigt sein; werden nur vorübergehend Stühle aufgestellt, so sind sie in den einzelnen Reihen fest miteinander zu verbinden.
Satz 1 gilt nicht für Gaststätten und Kantinen sowie für abgegrenzte Bereiche von Versammlungsräumen mit nicht mehr als 20 Sitzplätzen und ohne Stufen, wie Logen.

(2) Die Sitzplatzbereiche der Tribünen von Versammlungsstätten mit mehr als 5 000 Besucherplätzen müssen unverrückbar befestigte Einzelsitze haben.

(3) Sitzplätze müssen mindestens 0,50 Meter breit sein.
Zwischen den Sitzplatzreihen muss eine lichte Durchgangsbreite von mindestens 0,40 Meter vorhanden sein.

(4) Sitzplätze müssen in Blöcken von höchstens 30 Sitzplatzreihen angeordnet sein.
Hinter und zwischen den Blöcken müssen Gänge mit einer Mindestbreite von 1,20 Meter vorhanden sein.
Die Gänge müssen auf möglichst kurzem Weg zum Ausgang führen.

(5) Seitlich eines Ganges dürfen höchstens zehn Sitzplätze, bei Versammlungsstätten im Freien und Sportstadien höchstens 20 Sitzplätze angeordnet sein.
Zwischen zwei Seitengängen dürfen 20 Sitzplätze, bei Versammlungsstätten im Freien und Sportstadien höchstens 40 Sitzplätze angeordnet sein.
In Versammlungsräumen dürfen zwischen zwei Seitengängen höchstens 50 Sitzplätze angeordnet sein, wenn auf jeder Seite des Versammlungsraumes für jeweils vier Sitzreihen eine Tür mit einer lichten Breite von 1,20 Meter angeordnet ist.

(6) Von jedem Tischplatz darf der Weg zu einem Gang nicht länger als 10 Meter sein.
Der Abstand von Tisch zu Tisch soll 1,50 Meter nicht unterschreiten.

(7) In Versammlungsräumen mit Reihenbestuhlung müssen

1.  von bis zu 5 000 vorhandenen Besucherplätzen mindestens 1 Prozent und
2.  von darüber hinaus vorhandenen Besucherplätzen mindestens 0,5 Prozent,

mindestens jedoch zwei Plätze als Flächen für Rollstuhlbenutzerinnen und Rollstuhlbenutzer freigehalten werden.
Die Plätze und die Wege zu ihnen sind durch Hinweisschilder gut sichtbar zu kennzeichnen.
Für Versammlungsstätten im Freien, Freisportanlagen und Sportstadien gelten die Sätze 1 und 2 entsprechend.

(8) Stufen in Gängen (Stufengänge) müssen eine Steigung von mindestens 0,10 Meter und höchstens 0,19 Meter und einen Auftritt von mindestens 0,26 Meter haben.
Der Fußboden des Durchganges zwischen Sitzplatzreihen und der Fußboden von Stehplatzreihen muss mit dem anschließenden Auftritt des Stufenganges auf einer Höhe liegen.
Stufengänge in Mehrzweckhallen mit mehr als 5 000 Besucherplätzen und in Sportstadien müssen sich durch farbliche Kennzeichnung von den umgebenden Flächen deutlich abheben.

#### § 11 Abschrankungen und Schutzvorrichtungen

(1) Flächen, die im Allgemeinen zum Begehen bestimmt sind und unmittelbar an tiefer liegende Flächen angrenzen, sind mit Abschrankungen zu umwehren, soweit sie nicht durch Stufengänge oder Rampen mit der tiefer liegenden Fläche verbunden sind.
Satz 1 ist nicht anzuwenden:

1.  für die den Besucherinnen und Besuchern zugewandten Seiten von Bühnen und Szenenflächen,
2.  vor Stufenreihen, wenn die Stufenreihe nicht mehr als 0,50 Meter über dem Fußboden der davor liegenden Stufenreihe oder des Versammlungsraumes liegt oder
3.  vor Stufenreihen, wenn die Rückenlehnen der Sitzplätze der davor liegenden Stufenreihe den Fußboden der hinteren Stufenreihe um mindestens 0,65 Meter überragen.

(2) Abschrankungen, wie Umwehrungen, Geländer, Wellenbrecher, Zäune, Absperrgitter oder Glaswände, müssen mindestens 1,10 Meter hoch sein.
Umwehrungen und Geländer von Flächen, auf denen mit der Anwesenheit von Kleinkindern zu rechnen ist, sind so zu gestalten, dass ein Überklettern erschwert wird; der Abstand von Umwehrungs- und Geländerteilen darf in einer Richtung nicht mehr als 0,12 Meter betragen.

(3) Vor Sitzplatzreihen genügen Umwehrungen von 0,90 Meter Höhe; bei mindestens 0,20 Meter Brüstungsbreite der Umwehrung genügen 0,80 Meter; bei mindestens 0,50 Meter Brüstungsbreite genügen 0,70 Meter.
Liegt die Stufenreihe nicht mehr als 1 Meter über dem Fußboden der davor liegenden Stufenreihe oder des Versammlungsraumes, genügen vor Sitzplatzreihen 0,65 Meter.

(4) Abschrankungen in den für Besucherinnen und Besucher zugänglichen Bereichen müssen so bemessen sein, dass sie dem Druck einer Personengruppe standhalten.

(5) Die Fußböden und Stufen von Tribünen, Podien, Bühnen oder Szenenflächen dürfen keine Öffnungen haben, durch die Personen abstürzen können.

(6) Spielfelder, Manegen, Fahrbahnen für den Rennsport und Reitbahnen müssen durch Abschrankungen, Netze oder andere Vorrichtungen so gesichert sein, dass Besucherinnen und Besucher durch die Darbietung oder den Betrieb des Spielfeldes, der Manege oder der Bahn nicht gefährdet werden.
Für Darbietungen und für den Betrieb technischer Einrichtungen im Luftraum über den Besucherplätzen gilt Satz 1 entsprechend.

(7) Werden Besucherplätze im Innenbereich von Fahrbahnen angeordnet, so muss der Innenbereich ohne Betreten der Fahrbahnen erreicht werden können.

#### § 12 Toilettenräume

(1) Versammlungsstätten müssen getrennte Toilettenräume für Damen und Herren haben.
Toiletten sollen in jedem Geschoss mit Besucherplätzen angeordnet werden.
Es sollen mindestens vorhanden sein für:

Besucherplätze

Damen

Herren

Toiletten

Toiletten

Urinalbecken

bis 100

3

1

2

über 100 je weitere 100

1,2

0,4

0,8

über 1 000 je weitere 100

0,9

0,3

0,6

über 20 000 je weitere 100

0,6

0,2

0,4.

Die ermittelten Zahlen sind auf ganze Zahlen aufzurunden.
Soweit die Aufteilung der Toilettenräume nach Satz 2 nach der Art der Veranstaltung nicht zweckmäßig ist, kann für die Dauer der Veranstaltung eine andere Aufteilung erfolgen, wenn die Toilettenräume entsprechend gekennzeichnet werden.
Auf dem Gelände der Versammlungsstätte oder in der Nähe vorhandene Toiletten können angerechnet werden, wenn sie für die Besucherinnen und Besucher der Versammlungsstätte zugänglich sind.

(2) Mindestens eine je zwölf der nach Absatz 1 erforderlichen Toiletten muss barrierefrei sein.
Absatz 1 Satz 2 gilt entsprechend.

(3) Jeder Toilettenraum muss einen Vorraum mit Waschbecken haben.

#### § 13 Barrierefreie Stellplätze

Die Zahl der notwendigen barrierefreien Stellplätze muss mindestens der Hälfte der Zahl der nach § 10 Absatz 7 erforderlichen Besucherplätze entsprechen.
Auf diese Stellplätze ist dauerhaft und leicht erkennbar hinzuweisen.

### Abschnitt 4  
Technische Anlagen und Einrichtungen, besondere Räume

#### § 14 Sicherheitsstromversorgungsanlagen, elektrische Anlagen und Blitzschutzanlagen

(1) Versammlungsstätten müssen eine Sicherheitsstromversorgungsanlage haben, die bei Ausfall der Stromversorgung den Betrieb der sicherheitstechnischen Anlagen und Einrichtungen übernimmt, insbesondere der

1.  Sicherheitsbeleuchtung,
2.  automatischen Feuerlöschanlagen und Druckerhöhungsanlagen für die Löschwasserversorgung,
3.  Rauchabzugsanlagen,
4.  Brandmeldeanlagen,
5.  Alarmierungsanlagen.

(2) In Versammlungsstätten für verschiedene Veranstaltungsarten, wie Mehrzweckhallen, Theater und Studios, sind für die vorübergehende Verlegung beweglicher Kabel und Leitungen bauliche Vorkehrungen, wie Installationsschächte und -kanäle oder Abschottungen, zu treffen, die die Ausbreitung von Feuer und Rauch verhindern und die sichere Begehbarkeit, insbesondere der Rettungswege, gewährleisten.

(3) Elektrische Schaltanlagen dürfen für Besucherinnen und Besucher nicht zugänglich sein.

(4) Versammlungsstätten müssen Blitzschutzanlagen haben, die auch die sicherheitstechnischen Einrichtungen schützen (äußerer und innerer Blitzschutz).

#### § 15 Sicherheitsbeleuchtung

(1) In Versammlungsstätten muss eine Sicherheitsbeleuchtung vorhanden sein, die so beschaffen ist, dass Arbeitsvorgänge auf Bühnen und Szenenflächen sicher abgeschlossen werden können und sich Besucherinnen und Besucher, Mitwirkende und Betriebsangehörige auch bei vollständigem Versagen der allgemeinen Beleuchtung bis zu öffentlichen Verkehrsflächen hin gut zurechtfinden können.

(2) Eine Sicherheitsbeleuchtung muss vorhanden sein

1.  in notwendigen Treppenräumen, in Räumen zwischen notwendigen Treppenräumen und Ausgängen ins Freie und in notwendigen Fluren,
2.  in Versammlungsräumen sowie in allen übrigen Räumen für Besucherinnen und Besucher (zum Beispiel Foyers, Garderoben, Toiletten),
3.  für Bühnen und Szenenflächen,
4.  in den Räumen für Mitwirkende und Beschäftigte mit mehr als 20 Quadratmeter Grundfläche, ausgenommen Büroräume,
5.  in elektrischen Betriebsräumen, in Räumen für haustechnische Anlagen sowie in Scheinwerfer- und Bildwerferräumen,
6.  in Versammlungsstätten im Freien und Sportstadien, die während der Dunkelheit benutzt werden,
7.  für Sicherheitszeichen von Ausgängen und Rettungswegen,
8.  für Stufenbeleuchtungen.

(3) In betriebsmäßig verdunkelten Versammlungsräumen, auf Bühnen und Szenenflächen muss eine Sicherheitsbeleuchtung in Bereitschaftsschaltung vorhanden sein.
Die Ausgänge, Gänge und Stufen im Versammlungsraum müssen auch bei Verdunklung unabhängig von der übrigen Sicherheitsbeleuchtung erkennbar sein.
Bei Gängen in Versammlungsräumen mit auswechselbarer Bestuhlung sowie bei Sportstadien mit Sicherheitsbeleuchtung ist eine Stufenbeleuchtung nicht erforderlich.

#### § 16 Rauchableitung

(1) Versammlungsräume und sonstige Aufenthaltsräume mit jeweils mehr als 50 Quadratmeter Grundfläche sowie Magazine, Lagerräume und Szenenflächen mit jeweils mehr als 200 Quadratmeter Grundfläche, Bühnen und notwendige Treppenräume müssen zur Unterstützung der Brandbekämpfung entraucht werden können.

(2) Die Anforderung des Absatzes 1 ist insbesondere erfüllt bei

1.  Versammlungsräumen und sonstigen Aufenthaltsräumen bis 200 Quadratmeter Grundfläche, wenn diese Räume Fenster nach § 47 Absatz 2 der Brandenburgischen Bauordnung haben,
2.  Versammlungsräumen, sonstigen Aufenthaltsräumen, Magazinen und Lagerräumen mit nicht mehr als 1 000 Quadratmeter Grundfläche, wenn diese Räume entweder an der obersten Stelle Öffnungen zur Rauchableitung mit einem freien Querschnitt von insgesamt 1 Prozent der Grundfläche oder im oberen Drittel der Außenwände angeordnete Öffnungen, Türen oder Fenster mit einem freien Querschnitt von insgesamt 2 Prozent der Grundfläche haben und Zuluftflächen in insgesamt gleicher Größe, jedoch mit nicht mehr als 12 Quadratmeter freiem Querschnitt, vorhanden sind, die im unteren Raumdrittel angeordnet werden sollen,
3.  Versammlungsräumen, sonstigen Aufenthaltsräumen, Magazinen und Lagerräumen mit mehr als 1 000 Quadratmeter Grundfläche, wenn diese Räume Rauchabzugsanlagen haben, bei denen je höchstens 400 Quadratmeter der Grundfläche mindestens ein Rauchabzugsgerät mit mindestens 1,5 Quadratmeter aerodynamisch wirksamer Fläche im oberen Raumdrittel angeordnet wird, je höchstens 1 600 Quadratmeter Grundfläche mindestens eine Auslösegruppe für die Rauchabzugsgeräte gebildet wird und Zuluftflächen im unteren Raumdrittel von insgesamt mindestens 12 Quadratmeter freiem Querschnitt vorhanden sind,
4.  Bühnen gemäß § 2 Absatz 5 sowie Szenenflächen, wenn an der obersten Stelle des Bühnenraumes oder des Raumes oberhalb der Szenenfläche Öffnungen zur Rauchableitung mit einem freien Querschnitt von insgesamt mindestens 5 Prozent, bei den Szenenflächen von insgesamt mindestens 3 Prozent ihrer Grundfläche angeordnet werden.
    Zuluftflächen müssen in insgesamt gleicher Größe im unteren Raumdrittel der Bühnen oder der Räume mit Szenenflächen vorhanden sein.
    Bei Bühnenräumen mit Schutzvorhang müssen die Zuluftflächen so angeordnet sein, dass sie auch bei geschlossenem Schutzvorhang im Bühnenbereich wirksam sind.

(3) Die Anforderung des Absatzes 1 ist insbesondere auch erfüllt, wenn in den Fällen des Absatzes 2 Nummer 1 bis 3 maschinelle Rauchabzugsanlagen vorhanden sind, bei denen je höchstens 400 Quadratmeter der Grundfläche der Räume mindestens ein Rauchabzugsgerät oder eine Absaugstelle mit einem Luftvolumenstrom von 10 000 Kubikmeter pro Stunde im oberen Raumdrittel angeordnet wird.
Bei Räumen mit mehr als 1 600 Quadratmeter Grundfläche genügt

1.  zu dem Luftvolumenstrom von 40 000 Kubikmeter pro Stunde für die Grundfläche von 1 600 Quadratmeter ein zusätzlicher Luftvolumenstrom von 5 000 Kubikmeter pro Stunde je angefangene weitere 400 Quadratmeter Grundfläche; der sich ergebende Gesamtvolumenstrom je Raum ist gleichmäßig auf die nach Satz 1 anzuordnenden Absaugstellen oder Rauchabzugsgeräte zu verteilen, oder
2.  ein Luftvolumenstrom von mindestens 40 000 Kubikmeter pro Stunde je Raum, wenn sichergestellt ist, dass dieser Luftvolumenstrom im Bereich der Brandstelle auf einer Grundfläche von höchstens 1 600 Quadratmeter von den nach Satz 1 anzuordnenden Absaugstellen oder Rauchabzugsgeräten gleichmäßig gefördert werden kann.

Die Zuluftflächen müssen im unteren Raumdrittel in solcher Größe und so angeordnet werden, dass eine maximale Strömungsgeschwindigkeit von 3 Meter pro Sekunde nicht überschritten wird.
Anstelle der Öffnungen zur Rauchableitung nach Absatz 2 Nummer 4 können maschinelle Rauchabzugsanlagen verwendet werden, wenn sie bezüglich des Schutzziels nach Absatz 1 ausreichend bemessen sind.

(4) Die Anforderung des Absatzes 1 ist auch erfüllt bei Versammlungsräumen, sonstigen Aufenthaltsräumen, Magazinen und Lagerräumen nach Absatz 2 Nummer 1 bis 3 mit Sprinkleranlagen, wenn in diesen Räumen vorhandene Lüftungsanlagen automatisch bei Auslösen der Brandmeldeanlage, soweit diese nach § 20 Absatz 1 erforderlich ist, im Übrigen bei Auslösen der Sprinkleranlage so betrieben werden, dass sie nur entlüften und die ermittelten Luftvolumenströme nach Absatz 3 Satz 1 und Satz 2 Nummer 1 einschließlich Zuluft erreicht werden, soweit es die Zweckbestimmung der Absperrvorrichtungen gegen Brandübertragung zulässt; in Leitungen zum Zweck der Entlüftung dürfen Absperrvorrichtungen nur thermische Auslöser haben.

(5) Die Anforderung des Absatzes 1 ist erfüllt bei

1.  notwendigen Treppenräumen mit Fenstern gemäß § 35 Absatz 8 Satz 2 Nummer 1 der Brandenburgischen Bauordnung, wenn diese Treppenräume an der obersten Stelle eine Öffnung zur Rauchableitung mit einem freien Querschnitt von mindestens 1 Quadratmeter haben,
2.  notwendigen Treppenräumen gemäß § 35 Absatz 8 Satz 2 Nummer 2 der Brandenburgischen Bauordnung, wenn diese Treppenräume Rauchabzugsgeräte mit insgesamt mindestens 1 Quadratmeter aerodynamisch wirksamer Fläche haben, die im oder unmittelbar unter dem oberen Treppenraumabschluss angeordnet werden.

(6) Anstelle von Öffnungen zur Rauchableitung nach Absatz 2 Nummer 2 und 4 und Absatz 5 Nummer 1 sowie Rauchabzugsgeräten nach Absatz 5 Nummer 2 ist die Rauchableitung über Schächte mit strömungstechnisch äquivalenten Querschnitten zulässig, wenn die Wände der Schächte raumabschließend und so feuerwiderstandsfähig wie die durchdrungenen Bauteile, mindestens jedoch feuerhemmend sowie aus nichtbrennbaren Baustoffen sind.

(7) Türen oder Fenster nach Absatz 2 Nummer 2, mit Abschlüssen versehene Öffnungen zur Rauchableitung nach Absatz 2 Nummer 2 und 4 und Absatz 5 Nummer 1 und Rauchabzugsgeräte nach Absatz 5 Nummer 2 müssen Vorrichtungen zum Öffnen haben, die von jederzeit zugänglichen Stellen aus leicht von Hand bedient werden können; sie können auch an einer jederzeit zugänglichen Stelle zusammengeführt werden.
In notwendigen Treppenräumen müssen die Vorrichtungen von jedem Geschoss aus bedient werden können.
Geschlossene Öffnungen, die als Zuluftflächen dienen, müssen leicht geöffnet werden können.

(8) Rauchabzugsanlagen müssen automatisch auslösen und von Hand von einer jederzeit zugänglichen Stelle ausgelöst werden können.

(9) Manuelle Bedienungs- und Auslösestellen nach den Absätzen 7 und 8 sind mit einem Hinweisschild mit der Bezeichnung „RAUCHABZUG“ und der Angabe des jeweiligen Raumes zu versehen.
An den Stellen muss die Betriebsstellung der jeweiligen Anlage sowie der Fenster, Türen, Abschlüsse und Rauchabzugsgeräte erkennbar sein.

(10) Maschinelle Rauchabzugsanlagen sind für eine Betriebszeit von 30 Minuten bei einer Rauchgastemperatur von 600 Grad Celsius auszulegen.
Die Auslegung kann mit einer Rauchgastemperatur von 300 Grad Celsius erfolgen, wenn der Luftvolumenstrom des Raumes mindestens 40 000 Kubikmeter pro Stunde beträgt.
Die Zuluftzuführung muss durch automatische Ansteuerung und spätestens gleichzeitig mit Inbetriebnahme der Anlage erfolgen.
Maschinelle Lüftungsanlagen können als maschinelle Rauchabzugsanlagen betrieben werden, wenn sie die an diese gestellten Anforderungen erfüllen.

(11) Die Abschlüsse der Öffnungen zur Rauchableitung von Bühnen mit Schutzvorhang müssen bei einem Überdruck von 350 Pascal selbsttätig öffnen; eine automatische Auslösung durch geeignete Temperaturmelder ist zulässig.

#### § 17 Heizungsanlagen und Lüftungsanlagen

(1) Heizungsanlagen in Versammlungsstätten müssen dauerhaft fest eingebaut sein.
Sie müssen so angeordnet sein, dass ausreichende Abstände zu Personen, brennbaren Bauprodukten und brennbarem Material eingehalten werden und keine Beeinträchtigungen durch Abgase entstehen.

(2) Versammlungsräume und sonstige Aufenthaltsräume mit mehr als 200 Quadratmeter Grundfläche müssen Lüftungsanlagen haben.

#### § 18 Stände und Arbeitsgalerien für Licht-, Ton-, Bild- und Regieanlagen

(1) Stände und Arbeitsgalerien für den Betrieb von Licht-, Ton-, Bild- und Regieanlagen, wie Schnürböden, Beleuchtungstürme oder Arbeitsbrücken, müssen aus nichtbrennbaren Baustoffen bestehen.
Der Abstand zwischen Arbeitsgalerien und Raumdecken muss mindestens 2 Meter betragen.

(2) Von Arbeitsgalerien müssen mindestens zwei Rettungswege erreichbar sein.
Jede Arbeitsgalerie einer Hauptbühne muss auf beiden Seiten der Hauptbühne einen Ausgang zu Rettungswegen außerhalb des Bühnenraumes haben.

(3) Öffnungen in Arbeitsgalerien müssen so gesichert sein, dass Personen oder Gegenstände nicht herabfallen können.

#### § 19 Feuerlöscheinrichtungen und -anlagen

(1) Versammlungsräume, Bühnen, Foyers, Werkstätten, Magazine, Lagerräume und notwendige Flure sind mit geeigneten Feuerlöschern in ausreichender Zahl auszustatten.
Die Feuerlöscher sind gut sichtbar und leicht zugänglich anzubringen.

(2) In Versammlungsstätten mit Versammlungsräumen von insgesamt mehr als 1 000 Quadratmeter Grundfläche müssen Wandhydranten für die Feuerwehr (Typ F) in ausreichender Zahl gut sichtbar und leicht zugänglich an geeigneten Stellen angebracht sein; im Einvernehmen mit der Brandschutzdienststelle kann auf Wandhydranten verzichtet oder können anstelle von Wandhydranten trockene Löschwasserleitungen zugelassen werden.

(3) Versammlungsstätten mit Versammlungsräumen von insgesamt mehr als 3 600 Quadratmeter Grundfläche müssen eine automatische Feuerlöschanlage haben; dies gilt nicht für Versammlungsstätten, deren Versammlungsräume jeweils nicht mehr als 400 Quadratmeter Grundfläche haben.

(4) Versammlungsräume, bei denen eine Fußbodenebene höher als 22 Meter über der Geländeoberfläche liegt, sind nur in Gebäuden mit automatischer Feuerlöschanlage zulässig.

(5) Versammlungsräume in Kellergeschossen müssen eine automatische Feuerlöschanlage haben.
Dies gilt nicht für Versammlungsräume mit nicht mehr als 200 Quadratmeter, deren Fußboden an keiner Stelle mehr als 5 Meter unter der Geländeoberfläche liegt.

(6) In Versammlungsräumen müssen offene Küchen oder ähnliche Einrichtungen mit einer Grundfläche von mehr als 30 Quadratmeter eine dafür geeignete automatische Feuerlöschanlage haben.

(7) Die Wirkung automatischer Feuerlöschanlagen darf durch überdeckte oder mehrgeschossige Ausstellungs- oder Dienstleistungsstände nicht beeinträchtigt werden.

(8) Automatische Feuerlöschanlagen müssen an eine Brandmelderzentrale angeschlossen sein.

#### § 20 Brandmelde- und Alarmierungsanlagen, Brandmelder- und Alarmzentrale, Brandfallsteuerung der Aufzüge

(1) Versammlungsstätten mit Versammlungsräumen von insgesamt mehr als 1 000 Quadratmeter Grundfläche müssen Brandmeldeanlagen mit automatischen und nichtautomatischen Brandmeldern haben.

(2) Versammlungsstätten mit Versammlungsräumen von insgesamt mehr als 1 000 Quadratmeter Grundfläche müssen Alarmierungs- und Lautsprecheranlagen haben, mit denen im Gefahrenfall Besucherinnen und Besucher, Mitwirkende und Betriebsangehörige alarmiert und Anweisungen erteilt werden können.

(3) Versammlungsstätten mit Foyers oder Hallen, durch die Rettungswege aus anderen Versammlungsräumen führen, müssen Brandmeldeanlagen nach Absatz 1 und Alarmierungs- und Lautsprecheranlagen nach Absatz 2 haben.

(4) In Versammlungsstätten mit Versammlungsräumen von insgesamt mehr als 1 000 Quadratmeter Grundfläche müssen zusätzlich zu den örtlichen Bedienungsvorrichtungen zentrale Bedienungsvorrichtungen für Rauchabzugs-, Feuerlösch-, Brandmelde-, Alarmierungs- und Lautsprecheranlagen in einem für die Feuerwehr leicht zugänglichen Raum (Brandmelder- und Alarmzentrale) zusammengefasst werden.

(5) In Versammlungsstätten mit Versammlungsräumen von insgesamt mehr als 1 000 Quadratmeter Grundfläche müssen die Aufzüge mit einer Brandfallsteuerung ausgestattet sein, die durch die automatische Brandmeldeanlage ausgelöst wird.
Die Brandfallsteuerung muss sicherstellen, dass die Aufzüge ein Geschoss mit Ausgang ins Freie oder das diesem nächstgelegene, nicht von der Brandmeldung betroffene Geschoss unmittelbar anfahren und dort mit geöffneten Türen außer Betrieb gehen.

(6) Automatische Brandmeldeanlagen müssen durch technische Maßnahmen gegen Falschalarme gesichert sein.
Brandmeldungen müssen von der Brandmelderzentrale unmittelbar und automatisch zur zuständigen Regionalleitstelle weitergeleitet werden.

#### § 21 Werkstätten, Magazine und Lagerräume

(1) Für feuergefährliche Arbeiten, wie Schweiß-, Löt- oder Klebearbeiten, müssen dafür geeignete Werkstätten vorhanden sein.

(2) Für das Aufbewahren von Dekorationen, Requisiten und anderem brennbaren Material müssen eigene Lagerräume (Magazine) vorhanden sein.

(3) Für die Sammlung von Abfällen und Wertstoffen müssen dafür geeignete Behälter im Freien oder besondere Lagerräume vorhanden sein.

(4) Werkstätten, Magazine und Lagerräume dürfen mit notwendigen Treppenräumen nicht in unmittelbarer Verbindung stehen.

Teil 3  
Besondere Bauvorschriften
----------------------------------

### Abschnitt 1  
Großbühnen

#### § 22 Bühnenhaus

(1) In Versammlungsstätten mit Großbühnen sind alle für den Bühnenbetrieb notwendigen Räume und Einrichtungen in einem eigenen, von dem Zuschauerhaus getrennten Bühnenhaus unterzubringen.

(2) Die Trennwand zwischen Bühnen- und Zuschauerhaus muss feuerbeständig und in der Bauart einer Brandwand hergestellt sein.
Türen in dieser Trennwand müssen feuerbeständig und selbstschließend sein.

#### § 23 Schutzvorhang

(1) Die Bühnenöffnung von Großbühnen muss gegen den Versammlungsraum durch einen Vorhang aus nichtbrennbarem Material dicht geschlossen werden können (Schutzvorhang).
Der Schutzvorhang muss durch sein Eigengewicht schließen können.
Die Schließzeit darf 30 Sekunden nicht überschreiten.
Der Schutzvorhang muss einem Druck von 450 Pascal nach beiden Richtungen standhalten.
Eine höchstens 1 Meter breite, zur Hauptbühne sich öffnende, selbsttätig schließende Tür im Schutzvorhang ist zulässig.

(2) Der Schutzvorhang muss so angeordnet sein, dass er im geschlossenen Zustand an allen Seiten an feuerbeständige Bauteile anschließt.
Der Bühnenboden darf unter dem Schutzvorhang durchgeführt werden.
Das untere Profil dieses Schutzvorhangs muss ausreichend steif sein oder mit Stahldornen in entsprechende stahlbewehrte Aussparungen im Bühnenboden eingreifen.

(3) Die Vorrichtung zum Schließen des Schutzvorhangs muss mindestens an zwei Stellen von Hand ausgelöst werden können.
Beim Schließen muss auf der Bühne ein Warnsignal zu hören sein.

#### § 24 Feuerlösch- und Brandmeldeanlagen

(1) Großbühnen müssen eine automatische Sprühwasserlöschanlage haben, die auch den Schutzvorhang beaufschlagt.

(2) Die Sprühwasserlöschanlage muss zusätzlich mindestens von zwei Stellen aus von Hand in Betrieb gesetzt werden können.

(3) In Großbühnen müssen neben den Ausgängen zu den Rettungswegen in Höhe der Arbeitsgalerien und des Schnürbodens Wandhydranten vorhanden sein.

(4) Großbühnen und Räume mit besonderen Brandgefahren müssen eine Brandmeldeanlage mit automatischen und nichtautomatischen Brandmeldern haben.

(5) Die Auslösung eines Alarmes muss optisch und akustisch am Platz der Brandsicherheitswache erkennbar sein.

#### § 25 Platz für die Brandsicherheitswache

(1) Auf jeder Seite der Bühnenöffnung muss für die Brandsicherheitswache ein besonderer Platz mit einer Grundfläche von mindestens 1 Meter mal 1 Meter und einer Höhe von mindestens 2,20 Meter vorhanden sein.
Die Brandsicherheitswache muss die Fläche, die bespielt wird, überblicken und betreten können.

(2) Am Platz der Brandsicherheitswache müssen die Vorrichtung zum Schließen des Schutzvorhangs und die Auslösevorrichtungen der Rauchabzugs- und Sprühwasserlöschanlagen der Bühne sowie ein nichtautomatischer Brandmelder leicht erreichbar angebracht und durch Hinweisschilder gekennzeichnet sein.
Die Auslösevorrichtungen müssen beleuchtet sein.
Diese Beleuchtung muss an die Sicherheitsstromversorgung angeschlossen sein.
Die Vorrichtungen sind gegen unbeabsichtigtes Auslösen zu sichern.

### Abschnitt 2  
Versammlungsstätten mit mehr als 5 000 Besucherplätzen

#### § 26 Räume für Lautsprecherzentrale, Polizei, Feuerwehr, Sanitäts- und Rettungsdienst

(1) Mehrzweckhallen und Sportstadien müssen einen Raum für eine Lautsprecherzentrale haben, von dem aus die Besucherbereiche und der Innenbereich überblickt und Polizei, Feuerwehr und Rettungsdienste benachrichtigt werden können.
Die Lautsprecheranlage muss eine Vorrangschaltung für die Einsatzleitung der Polizei haben.

(2) In Mehrzweckhallen und Sportstadien sind ausreichend große Räume für die Polizei und die Feuerwehr anzuordnen.
Der Raum für die Einsatzleitung der Polizei muss eine räumliche Verbindung mit der Lautsprecherzentrale haben und mit Anschlüssen für eine Videoanlage zur Überwachung der Besucherbereiche ausgestattet sein.

(3) Wird die Funkkommunikation der Einsatzkräfte von Polizei und Feuerwehr innerhalb der Versammlungsstätte durch die bauliche Anlage gestört, ist die Versammlungsstätte mit technischen Anlagen zur Unterstützung des Funkverkehrs auszustatten.

(4) In Mehrzweckhallen und Sportstadien muss mindestens ein ausreichend großer Raum für den Sanitäts- und Rettungsdienst vorhanden sein.

#### § 27 Abschrankung und Blockbildung in Sportstadien mit mehr als 10&nbsp;000 Besucherplätzen

(1) Die Besucherplätze müssen vom Innenbereich durch mindestens 2,20 Meter hohe Abschrankungen abgetrennt sein.
In diesen Abschrankungen sind den Stufengängen zugeordnete, mindestens 1,80 Meter breite Tore anzuordnen, die sich im Gefahrenfall leicht zum Innenbereich hin öffnen lassen.
Die Tore dürfen nur vom Innenbereich oder von zentralen Stellen aus zu öffnen sein und müssen in geöffnetem Zustand durch selbsteinrastende Feststeller gesichert werden.
Der Übergang in den Innenbereich muss niveaugleich sein.

(2) Stehplätze müssen in Blöcken für höchstens 2 500 Besucherinnen und Besucher angeordnet werden, die durch mindestens 2,20 Meter hohe Abschrankungen mit eigenen Zugängen abgetrennt sind.

(3) Die Anforderungen nach den Absätzen 1 oder 2 gelten nicht, soweit in dem Sicherheitskonzept nachgewiesen wird, dass abweichende Abschrankungen oder Blockbildungen unbedenklich sind.
Das Sicherheitskonzept ist mit den für öffentliche Sicherheit oder Ordnung zuständigen Behörden, insbesondere der Polizei, der Feuerwehr und den Rettungsdiensten abzustimmen.

#### § 28 Wellenbrecher

Werden mehr als fünf Stufen von Stehplatzreihen hintereinander angeordnet, so ist vor der vordersten Stufe eine durchgehende Schranke von 1,10 Meter Höhe anzuordnen.
Nach jeweils fünf weiteren Stufen sind Schranken gleicher Höhe (Wellenbrecher) anzubringen, die einzeln mindestens 3 Meter und höchstens 5,50 Meter lang sind.
Die seitlichen Abstände zwischen den Wellenbrechern dürfen nicht mehr als 5 Meter betragen.
Die Abstände sind nach höchstens fünf Stehplatzreihen durch versetzt angeordnete Wellenbrecher zu überdecken, die auf beiden Seiten mindestens 0,25 Meter länger sein müssen als die seitlichen Abstände zwischen den Wellenbrechern.
Die Wellenbrecher sind im Bereich der Stufenvorderkante anzuordnen.

#### § 29 Abschrankung von Stehplätzen vor Szenenflächen

(1) Werden vor Szenenflächen Stehplätze für Besucherinnen und Besucher angeordnet, so sind die Besucherplätze von der Szenenfläche durch eine Abschrankung so abzutrennen, dass zwischen der Szenenfläche und der Abschrankung ein Gang von mindestens 2 Meter Breite für den Ordnungsdienst und Rettungskräfte vorhanden ist.

(2) Werden vor Szenenflächen mehr als 5 000 Stehplätze für Besucherinnen und Besucher angeordnet, so sind durch mindestens zwei weitere Abschrankungen vor der Szenenfläche nur von den Seiten zugängliche Stehplatzbereiche zu bilden.
Die Abschrankungen müssen voneinander an den Seiten einen Abstand von jeweils mindestens 5 Meter und über die Breite der Szenenfläche einen Abstand von mindestens 10 Meter haben.

#### § 30 Einfriedungen und Eingänge

(1) Stadionanlagen müssen eine mindestens 2,20 Meter hohe Einfriedung haben, die das Überklettern erschwert.

(2) Vor den Eingängen sind Geländer so anzuordnen, dass Besucherinnen und Besucher nur einzeln und hintereinander Einlass finden.
Es sind Einrichtungen für Zugangskontrollen sowie für die Durchsuchung von Personen und Sachen vorzusehen.
Für die Einsatzkräfte von Polizei, Feuerwehr und Rettungsdiensten sind von den Besuchereingängen getrennte Eingänge anzuordnen.

(3) Für Einsatz- und Rettungsfahrzeuge müssen besondere Zufahrten, Aufstell- und Bewegungsflächen vorhanden sein.
Von den Zufahrten und Aufstellflächen aus müssen die Eingänge der Versammlungsstätten unmittelbar erreichbar sein.
Für Einsatz- und Rettungsfahrzeuge muss eine Zufahrt zum Innenbereich vorhanden sein.
Die Zufahrten, Aufstell- und Bewegungsflächen müssen gekennzeichnet sein.

Teil 4  
Betriebsvorschriften
-----------------------------

### Abschnitt 1  
Rettungswege, Besucherplätze

#### § 31 Rettungswege, Flächen für die Feuerwehr

(1) Rettungswege auf dem Grundstück sowie Zufahrten, Aufstell- und Bewegungsflächen für Einsatzfahrzeuge von Polizei, Feuerwehr und Rettungsdiensten müssen ständig frei gehalten werden.
Darauf ist dauerhaft und gut sichtbar hinzuweisen.

(2) Rettungswege in der Versammlungsstätte müssen ständig frei gehalten werden.

(3) Während des Betriebes müssen alle Türen von Rettungswegen unverschlossen sein.

#### § 32 Besucherplätze nach dem Bestuhlungs- und Rettungswegeplan

(1) Die Zahl der im Bestuhlungs- und Rettungswegeplan genehmigten Besucherplätze darf nicht überschritten und die genehmigte Anordnung der Besucherplätze darf nicht geändert werden.

(2) Eine Ausfertigung des für die jeweilige Nutzung genehmigten Planes ist in der Nähe des Haupteinganges eines jeden Versammlungsraumes gut sichtbar anzubringen.

(3) Ist nach der Art der Veranstaltung die Abschrankung der Stehflächen vor Szenenflächen erforderlich, sind Abschrankungen nach § 29 auch in Versammlungsstätten mit nicht mehr als 5 000 Stehplätzen einzurichten.

### Abschnitt 2  
Brandverhütung

#### § 33 Vorhänge, Sitze, Ausstattungen, Requisiten und Ausschmückungen

(1) Vorhänge von Bühnen und Szenenflächen müssen aus mindestens schwerentflammbarem Material bestehen.

(2) Sitze von Versammlungsstätten mit mehr als 5 000 Besucherplätzen müssen aus mindestens schwerentflammbarem Material bestehen.
Die Unterkonstruktion muss aus nichtbrennbarem Material bestehen.

(3) Ausstattungen müssen aus mindestens schwerentflammbarem Material bestehen.
Bei Bühnen oder Szenenflächen mit automatischen Feuerlöschanlagen genügen Ausstattungen aus normalentflammbarem Material.

(4) Requisiten müssen aus mindestens normalentflammbarem Material bestehen.

(5) Ausschmückungen müssen aus mindestens schwerentflammbarem Material bestehen.
Ausschmückungen in notwendigen Fluren und notwendigen Treppenräumen müssen aus nichtbrennbarem Material bestehen.

(6) Ausschmückungen müssen unmittelbar an Wänden, Decken oder Ausstattungen angebracht werden.
Frei im Raum hängende Ausschmückungen sind zulässig, wenn sie einen Abstand von mindestens 2,50 Meter zum Fußboden haben.
Ausschmückungen aus natürlichem Pflanzenschmuck dürfen sich nur so lange sie frisch sind in den Räumen befinden.

(7) Der Raum unter dem Schutzvorhang ist von Ausstattungen, Requisiten oder Ausschmückungen so freizuhalten, dass die Funktion des Schutzvorhangs nicht beeinträchtigt wird.

(8) Brennbares Material muss von Zündquellen, wie Scheinwerfern oder Heizstrahlern, so weit entfernt sein, dass das Material durch diese nicht entzündet werden kann.

#### § 34 Aufbewahrung von Ausstattungen, Requisiten, Ausschmückungen und brennbarem Material

(1) Ausstattungen, Requisiten und Ausschmückungen dürfen nur außerhalb der Bühnen und der Szenenflächen aufbewahrt werden; dies gilt nicht für den Tagesbedarf.

(2) Auf den Bühnenerweiterungen dürfen Szenenaufbauten der laufenden Spielzeit bereitgestellt werden, wenn die Bühnenerweiterungen durch dichtschließende Abschlüsse aus nichtbrennbaren Baustoffen gegen die Hauptbühne abgetrennt sind.

(3) An den Zügen von Bühnen oder Szenenflächen dürfen nur Ausstattungsteile für einen Tagesbedarf hängen.

(4) Pyrotechnische Gegenstände, brennbare Flüssigkeiten und anderes brennbares Material, insbesondere Packmaterial, dürfen nur in den dafür vorgesehenen Magazinen aufbewahrt werden.

#### § 35 Rauchen, Verwendung von offenem Feuer und pyrotechnischen Gegenständen

(1) Auf Bühnen und Szenenflächen, in Werkstätten und Magazinen ist das Rauchen verboten.
Das Rauchverbot gilt nicht für Darstellerinnen und Darsteller und Mitwirkende auf Bühnen- und Szenenflächen während der Proben und Veranstaltungen, soweit das Rauchen in der Art der Veranstaltungen begründet ist.

(2) In Versammlungsräumen, auf Bühnen- und Szenenflächen und in Sportstadien ist das Verwenden von offenem Feuer, brennbaren Flüssigkeiten und Gasen, pyrotechnischen Gegenständen und anderen explosionsgefährlichen Stoffen verboten.
§ 17 Absatz 1 bleibt unberührt.
Das Verwendungsverbot gilt nicht, soweit das Verwenden von offenem Feuer, brennbaren Flüssigkeiten und Gasen sowie pyrotechnischen Gegenständen in der Art der Veranstaltung begründet ist und die Veranstalterin oder der Veranstalter die erforderlichen Brandschutzmaßnahmen im Einzelfall mit der Brandschutzdienststelle abgestimmt hat.
Die Verwendung pyrotechnischer Gegenstände muss durch eine nach Sprengstoffrecht geeignete Person überwacht werden.

(3) Die Verwendung von Kerzen und ähnlichen Lichtquellen als Tischdekoration sowie die Verwendung von offenem Feuer in dafür vorgesehenen Kücheneinrichtungen zur Zubereitung von Speisen ist zulässig.

(4) Auf die Verbote der Absätze 1 und 2 ist dauerhaft und gut sichtbar hinzuweisen.

### Abschnitt 3  
Betrieb technischer Einrichtungen

#### § 36 Bedienung und Wartung der technischen Einrichtungen

(1) Der Schutzvorhang muss täglich vor der ersten Vorstellung oder Probe durch Aufziehen und Herablassen auf seine Betriebsbereitschaft geprüft werden.
Der Schutzvorhang ist nach jeder Vorstellung herabzulassen und zu allen arbeitsfreien Zeiten geschlossen zu halten.

(2) Die Automatik der Sprühwasserlöschanlage kann während der Dauer der Anwesenheit der verantwortlichen Personen für Veranstaltungstechnik abgeschaltet werden.

(3) Die automatische Brandmeldeanlage kann abgeschaltet werden, soweit dies in der Art der Veranstaltung begründet ist und die Veranstalterin oder der Veranstalter die erforderlichen Brandschutzmaßnahmen im Einzelfall mit der Brandschutzdienststelle abgestimmt hat.

(4) Während des Aufenthaltes von Personen in Räumen, für die eine Sicherheitsbeleuchtung vorgeschrieben ist, muss diese in Betrieb sein, soweit die Räume nicht ausreichend durch Tageslicht erhellt sind.

#### § 37 Laseranlagen

Auf den Betrieb von Laseranlagen in den für Besucherinnen und Besucher zugänglichen Bereichen sind die arbeitsschutzrechtlichen Vorschriften entsprechend anzuwenden.

### Abschnitt 4  
Verantwortliche Personen, besondere Betriebsvorschriften

#### § 38 Pflichten der Betreiberinnen und Betreiber, Veranstalterinnen und Veranstalter und beauftragten Personen

(1) Die Betreiberin oder der Betreiber ist für die Sicherheit der Veranstaltung und die Einhaltung der Vorschriften verantwortlich.

(2) Während des Betriebes von Versammlungsstätten muss die Betreiberin oder der Betreiber oder eine von ihr oder ihm beauftragte Veranstaltungsleitung ständig anwesend sein.

(3) Die Betreiberin oder der Betreiber muss die Zusammenarbeit von Ordnungsdienst, Brandsicherheitswache und Sanitätswache mit der Polizei, der Feuerwehr und dem Rettungsdienst gewährleisten.

(4) Die Betreiberin oder der Betreiber ist zur Einstellung des Betriebes verpflichtet, wenn für die Sicherheit der Versammlungsstätte notwendige Anlagen, Einrichtungen oder Vorrichtungen nicht betriebsfähig sind oder wenn Betriebsvorschriften nicht eingehalten werden können.

(5) Die Betreiberin oder der Betreiber kann die Verpflichtungen nach den Absätzen 1 bis 4 durch schriftliche Vereinbarung auf die Veranstalterin oder den Veranstalter übertragen, wenn diese oder dieser oder deren oder dessen beauftragte Veranstaltungsleitung mit der Versammlungsstätte und deren Einrichtungen vertraut ist.
Die Verantwortung der Betreiberin oder des Betreibers bleibt unberührt.

#### § 39 Verantwortliche Personen für Veranstaltungstechnik

(1) Verantwortliche Personen für Veranstaltungstechnik sind

1.  die Geprüften Meisterinnen oder Geprüften Meister für Veranstaltungstechnik,
2.  technische Fachkräfte mit bestandenem fachrichtungsspezifischen Teil der Prüfung nach § 3 Absatz 1 Nummer 2 in Verbindung mit den §§ 5, 6 oder 7 der Verordnung über die Prüfung zum anerkannten Abschluss „Geprüfter Meister für Veranstaltungstechnik/Geprüfte Meisterin für Veranstaltungstechnik“ in den Fachrichtungen Bühne/Studio, Beleuchtung, Halle in der jeweiligen Fachrichtung,
3.  Hochschulabsolventinnen und Hochschulabsolventen mit berufsqualifizierendem Hochschulabschluss der Fachrichtung Theater- oder Veranstaltungstechnik mit mindestens einem Jahr Berufserfahrung im technischen Betrieb von Bühnen, Studios oder Mehrzweckhallen in der jeweiligen Fachrichtung, denen die Industrie- und Handelskammer zu Berlin ein Befähigungszeugnis nach Anlage 1 ausgestellt hat,
4.  technische Bühnen- und Studiofachkräfte, die das Befähigungszeugnis nach den bis zum Inkrafttreten dieser Verordnung geltenden Vorschriften erworben haben oder die Tätigkeit als technische Bühnen- und Studiofachkraft ohne Befähigungszeugnis ausüben durften und in den letzten drei Jahren ausgeübt haben.

Auf Antrag stellt die Industrie- und Handelskammer zu Berlin auch den Personen nach Satz 1 Nummer 1 bis 4 ein Befähigungszeugnis nach Anlage 1 aus.
Die in einem anderen Land der Bundesrepublik Deutschland ausgestellten Befähigungszeugnisse werden anerkannt.

(2) Gleichwertige Ausbildungen, die in einem anderen Mitgliedstaat der Europäischen Union oder einem Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum erworben und durch einen Ausbildungsnachweis belegt werden, sind entsprechend den europäischen Richtlinien zur Anerkennung von Berufsqualifikationen den in Absatz 1 genannten Ausbildungen gleichgestellt.

#### § 40 Aufgaben und Pflichten der verantwortlichen Personen für Veranstaltungstechnik, technische Probe

(1) Die verantwortlichen Personen für Veranstaltungstechnik müssen mit den bühnen-, studio- und beleuchtungstechnischen und sonstigen technischen Einrichtungen der Versammlungsstätte vertraut sein und deren Sicherheit und Funktionsfähigkeit, insbesondere hinsichtlich des Brandschutzes, während des Betriebes gewährleisten.

(2) Auf- oder Abbau bühnen-, studio- und beleuchtungstechnischer Einrichtungen von Großbühnen oder Szenenflächen mit mehr als 200 Quadratmeter Grundfläche oder in Mehrzweckhallen mit mehr als 5 000 Besucherplätzen, wesentliche Wartungs- und Instandsetzungsarbeiten an diesen Einrichtungen und technische Proben müssen von einer verantwortlichen Person für Veranstaltungstechnik geleitet und beaufsichtigt werden.

(3) Bei Generalproben, Veranstaltungen, Sendungen oder Aufzeichnungen von Veranstaltungen auf Großbühnen oder Szenenflächen mit mehr als 200 Quadratmeter Grundfläche oder in Mehrzweckhallen mit mehr als 5 000 Besucherplätzen müssen mindestens eine für die bühnen- oder studiotechnischen Einrichtungen sowie eine für die beleuchtungstechnischen Einrichtungen verantwortliche Person für Veranstaltungstechnik anwesend sein.

(4) Bei Szenenflächen mit mehr als 50 Quadratmeter und nicht mehr als 200 Quadratmeter Grundfläche oder in Mehrzweckhallen mit nicht mehr als 5 000 Besucherplätzen müssen die Aufgaben nach den Absätzen 1 bis 3 zumindest von einer Fachkraft für Veranstaltungstechnik mit mindestens drei Jahren Berufserfahrung wahrgenommen werden.
Die Aufgaben können auch von erfahrenen Bühnenhandwerkerinnen oder Bühnenhandwerkern oder Beleuchterinnen oder Beleuchtern wahrgenommen werden, die diese Aufgaben nach den bis zum Inkrafttreten dieser Verordnung geltenden Vorschriften wahrnehmen durften und in den letzten drei Jahren ausgeübt haben.

(5) Die Anwesenheit nach Absatz 3 ist nicht erforderlich, wenn

1.  die Sicherheit und Funktionsfähigkeit der bühnen-, studio- und beleuchtungstechnischen sowie der sonstigen technischen Einrichtungen der Versammlungsstätte von der verantwortlichen Person für Veranstaltungstechnik überprüft wurden,
2.  diese Einrichtungen während der Veranstaltung nicht bewegt oder sonst verändert werden,
3.  von Art oder Ablauf der Veranstaltung keine Gefahren ausgehen können und
4.  die Aufsicht durch eine Fachkraft für Veranstaltungstechnik geführt wird, die mit den technischen Einrichtungen vertraut ist.

Im Fall des Absatzes 4 können die Aufgaben nach den Absätzen 1 bis 3 von einer aufsichtführenden Person wahrgenommen werden, wenn

1.  von Auf- und Abbau sowie dem Betrieb der bühnen-, studio- und beleuchtungstechnischen Einrichtungen keine Gefahren ausgehen können,
2.  von Art oder Ablauf der Veranstaltung keine Gefahren ausgehen können und
3.  die aufsichtführende Person mit den technischen Einrichtungen vertraut ist.

(6) Bei Großbühnen sowie bei Szenenflächen mit mehr als 200 Quadratmeter Grundfläche und bei Gastspielveranstaltungen mit eigenem Szenenaufbau in Versammlungsräumen muss vor der ersten Veranstaltung eine nichtöffentliche technische Probe mit vollem Szenenaufbau und voller Beleuchtung stattfinden.
Diese technische Probe ist der Bauaufsichtsbehörde mindestens 24 Stunden vorher anzuzeigen.
Beabsichtigte wesentliche Änderungen des Szenenaufbaues nach der technischen Probe sind der zuständigen Bauaufsichtsbehörde rechtzeitig anzuzeigen.
Die Bauaufsichtsbehörde kann auf die technische Probe verzichten, wenn dies nach der Art der Veranstaltung oder nach dem Umfang des Szenenaufbaues unbedenklich ist.

#### § 41 Brandsicherheitswache, Sanitäts- und Rettungsdienst

(1) Bei Veranstaltungen mit erhöhten Brandgefahren hat die Betreiberin oder der Betreiber eine Brandsicherheitswache einzurichten.

(2) Bei jeder Veranstaltung auf Großbühnen sowie Szenenflächen mit mehr als 200 Quadratmeter Grundfläche muss eine Brandsicherheitswache der Feuerwehr anwesend sein.
Den Anweisungen der Brandsicherheitswache ist zu folgen.
Eine Brandsicherheitswache der Feuerwehr ist nicht erforderlich, wenn die Brandschutzdienststelle der Betreiberin oder dem Betreiber bestätigt, dass er über eine ausreichende Zahl ausgebildeter Kräfte verfügt, die die Aufgaben der Brandsicherheitswache wahrnehmen.

(3) Veranstaltungen mit voraussichtlich mehr als 5 000 Besucherinnen und Besuchern sind der für den Sanitäts- und Rettungsdienst zuständigen Behörde rechtzeitig anzuzeigen.

#### § 42 Brandschutzordnung, Räumungskonzept, Feuerwehrpläne

(1) Die Betreiberin oder der Betreiber oder eine von ihr oder ihm beauftragte Person hat im Einvernehmen mit der Brandschutzdienststelle eine Brandschutzordnung und gegebenenfalls ein Räumungskonzept aufzustellen.
Darin sind

1.  die Erforderlichkeit und die Aufgaben einer oder eines Brandschutzbeauftragten und der Kräfte für den Brandschutz sowie
2.  die Maßnahmen, die im Gefahrenfall für eine schnelle und geordnete Räumung der gesamten Versammlungsstätte oder einzelner Bereiche unter besonderer Berücksichtigung von Menschen mit Behinderungen erforderlich sind,

festzulegen.
Die Maßnahmen nach Satz 2 Nummer 2 sind bei Versammlungsstätten, die für mehr als 1 000 Besucherinnen und Besucher bestimmt sind, gesondert in einem Räumungskonzept darzustellen, sofern diese Maßnahmen nicht bereits Bestandteil des Sicherheitskonzeptes nach § 43 sind.

(2) Das Betriebspersonal ist bei Beginn des Arbeitsverhältnisses und danach mindestens einmal jährlich zu unterweisen über

1.  die Lage und die Bedienung der Feuerlöscheinrichtungen und -anlagen, Rauchabzugsanlagen, Brandmelde- und Alarmierungsanlagen und der Brandmelder- und Alarmzentrale,
2.  die Brandschutzordnung, insbesondere über das Verhalten bei einem Brand oder bei einer sonstigen Gefahrenlage, gegebenenfalls in Verbindung mit dem Räumungskonzept und
3.  die Betriebsvorschriften.

Den Brandschutzdienststellen ist Gelegenheit zu geben, an der Unterweisung teilzunehmen.
Über die Unterweisung ist eine Niederschrift zu fertigen, die der Bauaufsichtsbehörde auf Verlangen vorzulegen ist.

(3) Im Einvernehmen mit der Brandschutzdienststelle sind Feuerwehrpläne anzufertigen und der örtlichen Feuerwehr zur Verfügung zu stellen.

#### § 43 Sicherheitskonzept, Ordnungsdienst

(1) Erfordert es die Art der Veranstaltung, hat die Betreiberin oder der Betreiber ein Sicherheitskonzept aufzustellen und einen Ordnungsdienst einzurichten.

(2) Für Versammlungsstätten mit mehr als 5 000 Besucherplätzen hat die Betreiberin oder der Betreiber im Einvernehmen mit den für Sicherheit oder Ordnung zuständigen Behörden, insbesondere der Polizei, der Feuerwehr und der Rettungsdienste, ein Sicherheitskonzept aufzustellen.
Im Sicherheitskonzept sind die Mindestzahl der Kräfte des Ordnungsdienstes gestaffelt nach Besucherzahlen und Gefährdungsgraden sowie die betrieblichen Sicherheitsmaßnahmen und die allgemeinen und besonderen Sicherheitsdurchsagen festzulegen.

(3) Der nach dem Sicherheitskonzept erforderliche Ordnungsdienst muss unter der Leitung einer von der Betreiberin oder dem Betreiber oder der Veranstalterin oder dem Veranstalter bestellten Ordnungsdienstleitung stehen.

(4) Die Ordnungsdienstleitung und die Ordnungsdienstkräfte sind für die betrieblichen Sicherheitsmaßnahmen verantwortlich.
Sie sind insbesondere für die Kontrolle an den Ein- und Ausgängen und den Zugängen zu den Besucherblöcken, die Beachtung der maximal zulässigen Besucherzahl und die Anordnung der Besucherplätze, die Beachtung der Verbote des § 35, die Sicherheitsdurchsagen sowie für die geordnete Evakuierung im Gefahrenfall verantwortlich.

Teil 5  
Zusätzliche Bauvorlagen
--------------------------------

#### § 44 Zusätzliche Bauvorlagen, Bestuhlungs- und Rettungswegeplan

(1) Mit den Bauvorlagen ist ein Brandschutzkonzept vorzulegen, in dem insbesondere die maximal zulässige Zahl der Besucherinnen und Besucher, die Anordnung und Bemessung der Rettungswege und die zur Erfüllung der brandschutztechnischen Anforderungen erforderlichen baulichen, technischen und betrieblichen Maßnahmen dargestellt sind.
Ist eine höhere Anzahl von Besucherinnen und Besuchern je Quadratmeter Grundfläche des Versammlungsraumes als nach § 1 Absatz 2 Satz 1 vorgesehen, sind die schnelle und sichere Erreichbarkeit der Ausgänge ins Freie und die Möglichkeit zur Durchführung wirksamer Lösch- und Rettungsarbeiten gesondert darzustellen.

(2) Für die nach dieser Verordnung erforderlichen technischen Einrichtungen sind besondere Pläne, Beschreibungen und Nachweise vorzulegen.

(3) Mit den bautechnischen Nachweisen sind Standsicherheitsnachweise für dynamische Belastungen vorzulegen.

(4) Der Verlauf der Rettungswege im Freien, die Zufahrten und die Aufstell- und Bewegungsflächen für die Einsatz- und Rettungsfahrzeuge sind in einem besonderen Außenanlagenplan darzustellen.

(5) Die Anordnung der Sitz- und Stehplätze, einschließlich der Plätze für Rollstuhlbenutzerinnen und Rollstuhlbenutzer, der Bühnen-, Szenen- oder Spielflächen sowie der Verlauf der Rettungswege sind in einem Bestuhlungs- und Rettungswegeplan im Maßstab von mindestens 1 : 200 darzustellen.
Sind verschiedene Anordnungen vorgesehen, so ist für jede ein besonderer Plan vorzulegen.

#### § 45 Gastspielprüfbuch

(1) Für den eigenen, gleichbleibenden Szenenaufbau von wiederkehrenden Gastspielveranstaltungen kann auf schriftlichen Antrag ein Gastspielprüfbuch erteilt werden.

(2) Das Gastspielprüfbuch muss dem Muster der Anlage 2 entsprechen.
Die Veranstalterin oder der Veranstalter ist durch das Gastspielprüfbuch von der Verpflichtung entbunden, an jedem Gastspielort die Sicherheit des Szenenaufbaues und der dazu gehörenden technischen Einrichtungen erneut nachzuweisen.

(3) Das Gastspielprüfbuch wird von der unteren Bauaufsichtsbehörde erteilt.
Die Geltungsdauer ist auf die Dauer der Tournee zu befristen und kann auf schriftlichen Antrag verlängert werden.
Vor der Erteilung ist eine technische Probe durchzuführen.
Die in einem anderen Land der Bundesrepublik Deutschland ausgestellten Gastspielprüfbücher werden anerkannt.

(4) Das Gastspielprüfbuch ist der für den Gastspielort zuständigen Bauaufsichtsbehörde rechtzeitig vor der ersten Veranstaltung am Gastspielort vorzulegen.
Werden für die Gastspielveranstaltung Fliegende Bauten genutzt, ist das Gastspielprüfbuch mit der Anzeige der Aufstellung der Fliegenden Bauten vorzulegen.
Die ordnungsbehördlichen Befugnisse nach der Brandenburgischen Bauordnung bleiben unberührt.

Teil 6  
Bestehende Versammlungsstätten
---------------------------------------

#### § 46 Anwendung der Vorschriften auf bestehende Versammlungsstätten

(1) Auf die zum Zeitpunkt des Inkrafttretens dieser Verordnung bestehenden Versammlungsstätten ist der betrieblich-organisatorische Brandschutz innerhalb von zwei Jahren § 42 Absatz 1 und 2 entsprechend anzupassen.

(2) Die Bauaufsichtsbehörde hat Versammlungsstätten in Zeitabständen von höchstens drei Jahren zu prüfen.
Dabei ist auch die Einhaltung der Betriebsvorschriften zu überwachen und festzustellen, ob die vorgeschriebenen wiederkehrenden Prüfungen fristgerecht durchgeführt und etwaige Mängel beseitigt worden sind.
Den Ordnungsbehörden, der Gewerbeaufsicht und der Brandschutzdienststelle ist Gelegenheit zur Teilnahme an den Prüfungen zu geben.

Teil 7  
Schlussvorschriften
----------------------------

#### § 47 Ordnungswidrigkeiten

Ordnungswidrig nach § 85 Absatz 1 Satz 1 Nummer 1 der Brandenburgischen Bauordnung handelt, wer vorsätzlich oder fahrlässig

1.  entgegen § 31 Absatz 1 die Rettungswege auf dem Grundstück, die Zufahrten, Aufstell- und Bewegungsflächen nicht frei hält,
2.  entgegen § 31 Absatz 2 die Rettungswege in der Versammlungsstätte nicht frei hält,
3.  entgegen § 31 Absatz 3 Türen in Rettungswegen verschließt oder fest stellt,
4.  entgegen § 32 Absatz 1 die Zahl der genehmigten Besucherplätze überschreitet oder die genehmigte Anordnung der Besucherplätze ändert,
5.  entgegen § 32 Absatz 3 erforderliche Abschrankungen nicht einrichtet,
6.  entgegen § 33 Absatz 1 bis 5 andere als die dort genannten Materialien verwendet oder entgegen § 33 Absatz 6 bis 8 anbringt,
7.  entgegen § 34 Absatz 1 bis 3 Ausstattungen auf der Bühne aufbewahrt oder nicht von der Bühne entfernt,
8.  entgegen § 34 Absatz 4 pyrotechnische Gegenstände, brennbare Flüssigkeiten oder anderes brennbares Material außerhalb der dafür vorgesehenen Magazine aufbewahrt,
9.  entgegen § 35 Absatz 1 und 2 raucht oder offenes Feuer, brennbare Flüssigkeiten oder Gase, explosionsgefährliche Stoffe oder pyrotechnische Gegenstände verwendet,
10.  entgegen § 36 Absatz 4 die Sicherheitsbeleuchtung nicht in Betrieb nimmt,
11.  entgegen § 37 Laseranlagen in Betrieb nimmt,
12.  als Betreiberin oder Betreiber, Veranstalterin oder Veranstalter oder beauftragte Veranstaltungsleitung entgegen § 38 Absatz 2 während des Betriebes nicht anwesend ist,
13.  als Betreiberin oder Betreiber, Veranstalterin oder Veranstalter oder beauftragte Veranstaltungsleitung entgegen § 38 Absatz 4 den Betrieb der Versammlungsstätte nicht einstellt,
14.  entgegen § 40 Absatz 2 bis 5 in Verbindung mit § 38 Absatz 1 als Betreiberin oder Betreiber, Veranstalterin oder Veranstalter oder beauftragte Veranstaltungsleitung den Betrieb von Bühnen oder Szenenflächen zulässt, ohne dass die erforderlichen verantwortlichen Personen oder Fachkräfte für Veranstaltungstechnik, die erfahrenen Bühnenhandwerkerinnen oder Bühnenhandwerker oder Beleuchterinnen oder Beleuchter oder die aufsichtführenden Personen anwesend sind,
15.  entgegen § 40 Absatz 2 bis 5 als verantwortliche Person oder Fachkraft für Veranstaltungstechnik, als erfahrene Bühnenhandwerkerin oder erfahrener Bühnenhandwerker oder Beleuchterin oder Beleuchter oder als aufsichtführende Person die Versammlungsstätte während des Betriebes verlässt,
16.  als Betreiberin oder Betreiber entgegen § 41 Absatz 1 und 2 nicht für die Durchführung der Brandsicherheitswache sorgt oder entgegen § 41 Absatz 3 die Veranstaltung nicht anzeigt,
17.  als Betreiberin oder Betreiber oder Veranstalterin oder Veranstalter die nach § 42 Absatz 2 vorgeschriebenen Unterweisungen unterlässt,
18.  als Betreiberin oder Betreiber oder Veranstalterin oder Veranstalter entgegen § 43 Absatz 1 bis 3 keinen Ordnungsdienst oder keine Ordnungsdienstleitung bestellt,
19.  als Ordnungsdienstleitung oder Ordnungsdienstkraft entgegen § 43 Absatz 3 oder 4 ihren oder seinen Aufgaben nicht nachkommt,
20.  als Betreiberin oder Betreiber einer der Anpassungspflichten nach § 46 Absatz 1 nicht oder nicht fristgerecht nachkommt.

#### § 48 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Brandenburgische Versammlungsstättenverordnung vom 29.
November 2005 (GVBl.
II S. 540), die durch die Verordnung vom 24.
August 2012 (GVBl.
II Nr. 76) geändert worden ist, außer Kraft.

Potsdam, den 28.
November 2017

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#a) Diese Verordnung dient der Umsetzung der Richtlinie (EU) 2015/1535 des Europäischen Parlaments und des Rates vom 9.
September 2015 über ein Informationsverfahren auf dem Gebiet der technischen Vorschriften und der Vorschriften für die Dienste der Informationsgesellschaft (ABl. L 241 vom 17.09.2015, S.
1).

* * *

### Anlagen

1

[Anlage 1 (zu § 39 Absatz 1) - Befähigungszeugnis](/br2/sixcms/media.php/68/GVBl_II_01_2018-Anlage-1.pdf "Anlage 1 (zu § 39 Absatz 1) - Befähigungszeugnis") 576.3 KB

2

[Anlage 2 (zu § 45 Absatz 2) - Gastspielprüfbuch](/br2/sixcms/media.php/68/GVBl_II_01_2018-Anlage-2.pdf "Anlage 2 (zu § 45 Absatz 2) - Gastspielprüfbuch") 1.0 MB