## Verordnung über befristete Basismaßnahmen zum Infektionsschutz aufgrund des SARS-CoV-2-Virus und COVID-19 im Land Brandenburg (SARS-CoV-2-Infektionsschutz-Basismaßnahmenverordnung - SARS-CoV-2-IfSBMV)

Auf Grund des § 32 Satz 1 in Verbindung mit § 28 Absatz 1 und § 28a Absatz 7 des Infektionsschutzgesetzes vom 20.
Juli 2000 (BGBl.
I S. 1045), von denen § 28 Absatz 1 zuletzt durch Artikel 1 Nummer 2 des Gesetzes vom 22. November 2021 (BGBl.
I S.
4906) geändert sowie § 28a Absatz 7 durch Artikel 1 Nummer 5 Buchstabe a des Gesetzes vom 18.
März 2022 (BGBl.
I S. 466, 469) und § 32 Satz 1 durch Artikel 1 Nummer 4 des Gesetzes vom 22. April 2021 (BGBl.
I S. 802, 806) neu gefasst worden sind, in Verbindung mit § 2 der Infektionsschutzzuständigkeitsverordnung vom 27.
November 2007 (GVBl.
II S.
488), der durch die Verordnung vom 10.
Januar 2012 (GVBl. II Nr. 2) neu gefasst worden ist, verordnet die Ministerin für Soziales, Gesundheit, Integration und Verbraucherschutz:

#### § 1 Beurteilung des Infektionsgeschehens

Beurteilungsmaßstab für die mit dieser Verordnung angeordneten Schutzmaßnahmen sind insbesondere folgende Indikatoren:

1.  Anzahl der stationär behandelten COVID-19-Patientinnen und -Patienten innerhalb der letzten sieben Tage pro 100 000 Einwohnerinnen und Einwohnern (Sieben-Tage-Hospitalisierungsinzidenz),
2.  Anzahl der tatsächlich verfügbaren intensivmedizinischen Behandlungskapazitäten,
3.  Anzahl der Neuinfektionen innerhalb der letzten sieben Tage pro 100 000 Einwohnerinnen und Einwohnern (Sieben-Tage-Inzidenz),
4.  Anzahl der gegen das SARS-CoV-2-Virus geimpften Personen,
5.  absehbare Änderungen des Infektionsgeschehens durch ansteckendere, das Gesundheitssystem stärker belastende Virusvarianten.

#### § 2 Maskenpflicht

(1) In geschlossenen Räumen von

1.  Arztpraxen,
2.  Krankenhäusern,
3.  Einrichtungen für ambulantes Operieren,
4.  Vorsorge- und Rehabilitationseinrichtungen, in denen eine den Krankenhäusern vergleichbare medizinische Versorgung erfolgt,
5.  Dialyseeinrichtungen,
6.  Tageskliniken,
7.  Rettungsdiensten,
8.  nicht unter § 23 Absatz 5 Satz 1 des Infektionsschutzgesetzes fallenden voll- und teilstationären Einrichtungen zur Betreuung und Unterbringung älterer, behinderter oder pflegebedürftiger Menschen oder vergleichbaren Einrichtungen, mit Ausnahme der nach § 45 des Achten Buches Sozialgesetzbuch betriebserlaubnispflichtigen Einrichtungen für Kinder und Jugendliche,
9.  Obdachlosenunterkünften,
10.  Einrichtungen zur gemeinschaftlichen Unterbringung von Asylbewerberinnen und Asylbewerbern, vollziehbar Ausreisepflichtigen, Flüchtlingen und Spätaussiedlern

haben alle Besucherinnen und Besucher während des gesamten Aufenthalts eine FFP2-Maske zu tragen.
Die Beschäftigten in den Einrichtungen nach Satz 1 haben bei der Ausübung körpernaher Tätigkeiten eine FFP2-Maske zu tragen; im Übrigen haben sie in geschlossenen Räumen mindestens eine OP-Maske zu tragen, soweit physische Kontakte zu anderen Personen nicht ausgeschlossen sind.
Die in den Einrichtungen nach Satz 1 behandelten, betreuten, gepflegten oder untergebrachten Personen haben beim Empfang von körpernahen Dienstleistungen mindestens eine OP-Maske zu tragen, soweit die besondere Eigenart der Dienstleistung das Tragen einer Maske zulässt; in Einrichtungen nach Satz 1 Nummer 1 bis 7, 9 und 10 haben sie auch in den allgemein zugänglichen Bereichen der Einrichtung mindestens eine OP-Maske zu tragen.
Für Beschäftigte von

1.  ambulanten Pflegediensten, die ambulante Intensivpflege in Einrichtungen, Wohngruppen oder sonstigen gemeinschaftlichen Wohnformen erbringen,
2.  nicht unter § 23 Absatz 5 Satz 1 des Infektionsschutzgesetzes fallenden ambulanten Pflegediensten und Unternehmen, die den Einrichtungen nach Satz 1 Nummer 8 vergleichbare Dienstleistungen anbieten, wobei Angebote zur Unterstützung im Alltag im Sinne von § 45a Absatz 1 Satz 2 des Elften Buches Sozialgesetzbuch nicht zu den Dienstleistungen, die mit Angeboten in Einrichtungen nach Satz 1 Nummer 8 vergleichbar sind, zählen,

gilt die Tragepflicht nach Satz 2 entsprechend.
In den Einrichtungen nach Satz 1 Nummer 1 bis 8 besteht die Verpflichtung zum Tragen einer Maske nach den Sätzen 1 bis 4 nur, soweit dies zur Abwendung einer Gefahr für Personen, die auf Grund ihres Alters oder ihres Gesundheitszustandes ein erhöhtes Risiko für einen schweren oder tödlichen Krankheitsverlauf der COVID-19-Krankheit haben, erforderlich ist.

(2) In Verkehrsmitteln des öffentlichen Personennahverkehrs haben alle Fahrgäste eine FFP2-Maske zu tragen; bei der Schülerbeförderung und für Kinder bis zum vollendeten 14.
Lebensjahr ist das Tragen einer OP-Maske ausreichend.
Das Kontroll- und Servicepersonal hat mindestens eine OP-Maske zu tragen, soweit tätigkeitsbedingt physische Kontakte zu anderen Personen bestehen.

(3) Soweit nach den Absätzen 1 oder 2 eine FFP2-Maske oder OP-Maske zu tragen ist, muss

1.  die FFP2-Maske den Anforderungen an eine CE-gekennzeichnete FFP2-Maske der europäischen Norm EN 149:2001+A1:2009 oder vergleichbaren Schutzstandards entsprechen, wobei die Maske nicht über ein Ausatemventil verfügen darf,
2.  die OP-Maske den Anforderungen an eine CE-gekennzeichnete medizinische Gesichtsmaske mit der Norm DIN EN 14683:2019-10 entsprechen.

(4) Sofern Kinder unter 14 Jahren aufgrund der Passform keine FFP2-Maske oder OP-Maske tragen können, ist ersatzweise eine Mund-Nasen-Bedeckung zu tragen.
Die Mund-Nasen-Bedeckung muss aufgrund ihrer Beschaffenheit geeignet sein, eine Ausbreitung von übertragungsfähigen Tröpfchenpartikeln beim Husten, Niesen, Sprechen oder Atmen zu verringern, unabhängig von einer Kennzeichnung oder zertifizierten Schutzkategorie.

(5) Unbeschadet des Absatzes 6 sind von der Verpflichtung zum Tragen einer FFP2-Maske, OP-Maske oder einer Mund-Nasen-Bedeckung folgende Personen befreit:

1.  Kinder bis zum vollendeten sechsten Lebensjahr,
2.  Gehörlose und schwerhörige Menschen, ihre Begleitperson und im Bedarfsfall Personen, die mit diesen kommunizieren,
3.  Personen, denen die Verwendung einer FFP2-Maske, OP-Maske oder Mund-Nasen-Bedeckung wegen einer Behinderung oder aus gesundheitlichen Gründen nicht möglich oder unzumutbar ist; dies ist vor Ort durch ein schriftliches ärztliches Zeugnis im Original nachzuweisen,
4.  das Personal, wenn die Ausbreitung übertragungsfähiger Tröpfchenpartikel durch geeignete technische Vorrichtungen mit gleicher Wirkung wie durch das Tragen einer OP-Maske verringert wird.

Das ärztliche Zeugnis nach Satz 1 Nummer 3 Halbsatz 2 muss mindestens den vollständigen Namen und das Geburtsdatum enthalten; im Falle der Vorlage bei Behörden oder Gerichten muss es zusätzlich konkrete Angaben beinhalten, warum die betroffene Person von der Tragepflicht befreit ist.
Die oder der nach dieser Verordnung zur Kontrolle befugte Verantwortliche hat Stillschweigen über die erhobenen Daten zu bewahren und sicherzustellen, dass die Kenntnisnahme der Daten durch Unbefugte ausgeschlossen ist.
Sofern im Einzelfall eine Dokumentation der Befreiung von der Tragepflicht erforderlich ist, darf die Tatsache, dass das ärztliche Zeugnis vorgelegt wurde, die ausstellende Ärztin oder der ausstellende Arzt sowie ein eventueller Gültigkeitszeitraum des ärztlichen Zeugnisses in die zu führenden Unterlagen aufgenommen werden; die Anfertigung einer Kopie des ärztlichen Zeugnisses ist nicht zulässig.
Die erhobenen Daten dürfen ausschließlich zum Zweck des Nachweises der Einhaltung bereichsspezifischer Hygieneregeln genutzt werden.
Die Aufbewahrung und Speicherung der erhobenen Daten hat unter Einhaltung der einschlägigen datenschutzrechtlichen Vorschriften zu erfolgen.
Die erhobenen Daten sind umgehend zu vernichten oder zu löschen, sobald sie für den in Satz 5 genannten Zweck nicht mehr erforderlich sind.

(6) Die Befreiungstatbestände nach Absatz 5 gelten nicht in den Fällen des Absatzes 1 Satz 1 Nummer 1 bis 8, Satz 2 und 4.

#### § 3 Testpflicht

(1) Alle Beschäftigten in

1.  Einrichtungen nach § 2 Absatz 1 Satz 1 Nummer 2, 8 und 10,
2.  ambulanten Pflegediensten und Unternehmen nach § 2 Absatz 1 Satz 4,
3.  Maßregelvollzugseinrichtungen sowie anderen Abteilungen und Einrichtungen, wenn und soweit dort dauerhaft freiheitsentziehende Unterbringungen erfolgen, insbesondere psychiatrische Krankenhäuser, Heime der Jugendhilfe und für Senioren,

haben sich an jedem Tag, an dem die oder der Beschäftigte zum Dienst eingeteilt ist, einer Testung in Bezug auf eine Infektion mit dem SARS-CoV-2-Virus zu unterziehen und das Ergebnis der Leitung der Einrichtung auf deren Verlangen vorzulegen.
Die Einrichtungen sollen die erforderlichen Testungen organisieren.
Auf der Grundlage eines von dem zuständigen Gesundheitsamt zu genehmigenden individuellen Testkonzepts können Krankenhäuser vorsehen, dass ihre Beschäftigten abweichend von Satz 1 nur mindestens zweimal pro Woche einer Testung in Bezug auf eine Infektion mit dem SARS-CoV-2-Virus zu unterziehen sind.

(2) In Schulen nach § 16 des Brandenburgischen Schulgesetzes und in Schulen in freier Trägerschaft müssen sich

1.  Schülerinnen und Schüler an mindestens drei von der jeweiligen Schule bestimmten, nicht aufeinanderfolgenden Tagen pro Woche,
2.  Lehrkräfte sowie das sonstige Schulpersonal, für das physische Kontakte zu Schülerinnen und Schülern oder zu Lehrkräften nicht ausgeschlossen werden können, täglich

in Bezug auf eine Infektion mit dem SARS-CoV-2-Virus testen lassen.
Die Testung erfolgt durch Antigen-Tests zur Eigenanwendung ohne fachliche Aufsicht; die durchgeführte Testung und deren negatives Ergebnis ist von der getesteten Person oder, sofern diese nicht volljährig ist, von einer oder einem Sorgeberechtigten zu bescheinigen.
Das für Bildung zuständige Ministerium kann im Einvernehmen mit dem für Gesundheit zuständigen Ministerium im Rahmen von Pilotprojekten für einzelne Schulen die Erprobung von in der Schule einmal pro Woche durchzuführenden PCR-Pooltestungen zur Feststellung einer Infektion mit dem SARS-CoV-2-Virus zulassen.

(3) Für Horteinrichtungen sowie während der Betreuungszeiten für Kindertagespflegestellen, die Kinder im Grundschulalter betreuen, gilt Absatz 2 Satz 1 und 2 entsprechend.
Die Testpflicht der betreuten Kinder nach Satz 1 in Verbindung mit Absatz 2 Satz 1 gilt als erfüllt, wenn diese einen auf sie ausgestellten Testnachweis bereits für die Teilnahme am schulischen Präsenzunterricht vorgelegt haben.
Für Kindertagesstätten sowie während der Betreuungszeiten für Kindertagespflegestellen, die Kinder im Vorschulalter betreuen, gilt Absatz 2 Satz 1 und 2 entsprechend mit der Maßgabe, dass sich die betreuten Kinder mindestens an zwei nicht aufeinanderfolgenden Tagen pro Woche testen lassen müssen; ausgenommen sind Kinder bis zum vollendeten ersten Lebensjahr.

(4) Die Testpflicht nach den Absätzen 1 bis 3 gilt nicht für

1.  geimpfte Personen nach § 2 Nummer 2 der COVID-19-Schutzmaßnahmen-Ausnahmenverordnung,
2.  genesene Personen nach § 2 Nummer 4 der COVID-19-Schutzmaßnahmen-Ausnahmenverordnung,

die einen auf sie ausgestellten Impf- oder Genesenennachweis nach § 22a Absatz 1 oder 2 des Infektionsschutzgesetzes vorlegen.

#### § 4 Bußgeldtatbestände

(1) Ordnungswidrig im Sinne des § 73 Absatz 1a Nummer 24 des Infektionsschutzgesetzes handelt, wer

1.  vorsätzlich entgegen § 2 Absatz 2 Satz 1 Halbsatz 1 keine FFP2-Maske trägt, ohne dass eine Ausnahme nach § 2 Absatz 2 Satz 1 Halbsatz 2 oder Absatz 5 Satz 1 vorliegt,
2.  sich vorsätzlich oder fahrlässig entgegen § 3 Absatz 1 nicht einer regelmäßigen Testung in Bezug auf eine Infektion mit dem SARS-CoV-2-Virus unterzieht, ohne dass eine Ausnahme nach § 3 Absatz 4 vorliegt.

(2) Ordnungswidrigkeiten im Sinne des Absatzes 1 können mit einer Geldbuße bis zu 25 000 Euro geahndet werden.

(3) Die Regelsätze für Geldbußen wegen einer Ordnungswidrigkeit nach Absatz 1 sind als Anlage veröffentlicht.

#### § 5 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 3.
April 2022 in Kraft und mit Ablauf des 30.
April 2022 außer Kraft.

Potsdam, den 31.
März 2022

Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

Ursula Nonnemacher

### Anlagen

1

[Allgemeine Begründung der SARS-CoV-2-Infektionsschutz-Basismaßnahmenverordnung](/br2/sixcms/media.php/68/GVBl_II_30_2022-Allgemeine%20Begr%C3%BCndung.pdf "Allgemeine Begründung der SARS-CoV-2-Infektionsschutz-Basismaßnahmenverordnung") 255.5 KB

2

[Anlage (zu § 4 Absatz 3) Bußgeldkatalog für Ordnungswidrigkeiten nach dem Infektionsschutzgesetz im Zusammenhang mit dieser Verordnung](/br2/sixcms/media.php/68/GVBl_II_30_2022-Anlage.pdf "Anlage (zu § 4 Absatz 3) Bußgeldkatalog für Ordnungswidrigkeiten nach dem Infektionsschutzgesetz im Zusammenhang mit dieser Verordnung") 208.3 KB