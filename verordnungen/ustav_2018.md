## Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Umsatzsteuer für die Haushaltsjahre 2019 und 2020 (Umsatzsteueraufteilverordnung - UStAV)

Auf Grund des § 5a Absatz 3 Satz 3 und des § 5d Absatz 2 und 3 in Verbindung mit § 4 Absatz 2 und des § 8 des Gemeindefinanzreformgesetzes in der Fassung der Bekanntmachung vom 10.
März 2009 (BGBl. I S. 502), von denen § 5a Absatz 3 Satz 3 und § 5d Absatz 2 und 3 zuletzt durch Artikel 3 des Gesetzes vom 21. November 2016 (BGBl. I S. 2613) geändert worden sind, in Verbindung mit § 1 der Verordnung zur Übertragung der Ermächtigung zum Erlass von Rechtsverordnungen nach dem Gemeindefinanzreformgesetz vom 23. September 2003 (GVBl. II S. 579) verordnet der Minister der Finanzen:

#### § 1 Verteilungsschlüssel für den Gemeindeanteil an der Umsatzsteuer

(1) Der auf die Gemeinden im Land Brandenburg entfallende Gemeindeanteil an der Umsatzsteuer wird für die Haushaltsjahre 2019 und 2020 nach dem in der Anlage 1 festgesetzten Schlüssel aufgeteilt.

(2) In Fällen kommunaler Neugliederung nach dem 31.
Dezember 2016 ist nach § 5 der Umsatzsteuerschlüsselzahlenfestsetzungsverordnung vom 2.
Januar 2018 (BGBl. I S. 50) zu verfahren.

#### § 2 Berichtigung von Fehlern

(1) Ausgleichsbeträge nach § 5d Absatz 3 in Verbindung mit § 4 Absatz 1 des Gemeindefinanzreformgesetzes werden nach den Anteilen der einzelnen Gemeinden an dem nach § 5a Absatz 2 des Gemeindefinanzreformgesetzes auf die Gemeinden entfallenden Steueraufkommen errechnet, um die die in der Anlage 1 genannten Anteile zu hoch oder zu niedrig festgesetzt sind.

(2) Der Ausgleich ist mit der jeweiligen Schlussabrechnung vorzunehmen.
Ein Ausgleich unterbleibt, wenn der auszugleichende Betrag 500 Euro nicht übersteigt.

#### § 3 Berechnung, Anweisung und Auszahlung

(1) Der Gemeindeanteil an der Umsatzsteuer nach § 1 ist vom Amt für Statistik Berlin-Brandenburg zu berechnen.

(2) Das Ministerium der Finanzen stellt die anzuweisenden Beträge fest und regelt die Auszahlung an die Gemeinden.

(3) Auf den Gemeindeanteil an der Umsatzsteuer sind an die Gemeinden für die jeweiligen Haushaltsjahre vierteljährliche Abschlagszahlungen zu den in Anlage 2 festgesetzten Terminen anzuweisen.
Den Abschlagszahlungen ist jeweils das vierteljährliche Aufkommen des Gemeindeanteils an der Umsatzsteuer zugrunde zu legen.
Die Abschlagszahlung für das jeweils vierte Quartal ist in Höhe der Abschlagszahlung für das jeweils dritte Quartal zu leisten.

#### § 4 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2018 in Kraft.
Gleichzeitig treten die

1.  Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Umsatzsteuer für die Haushaltsjahre 1998 und 1999 vom 12.
    März 1998 (GVBl.
    II S. 274),
2.  Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Umsatzsteuer für die Haushaltsjahre 2000 bis 2008 vom 30.
    Mai 2000 (GVBl. II S. 154), die zuletzt durch die Verordnung vom 10.
    Januar 2006 (GVBl.
    II S. 13) geändert worden ist,
3.  Verordnung über die Aufteilung und Auszahlung des Gemeindeanteils an der Umsatzsteuer für die Haushaltsjahre 2009, 2010 und 2011 vom 19.
    November 2008 (GVBl.
    II S. 438),
4.  Umsatzsteueraufteilverordnung vom 10.
    September 2012 (GVBl.
    II Nr. 80)

außer Kraft.

Potsdam, den 6.
März 2018

Der Minister der Finanzen

Christian Görke

* * *

### Anlagen

1

[Anlage 1 (zu § 1 Absatz 1, § 2 Absatz 1) - Schlüsselzahlen für den Gemeindeanteil an der Umsatzsteuer für die Haushaltsjahre 2019 und 2020](/br2/sixcms/media.php/68/UStAV-Anlage-1.pdf "Anlage 1 (zu § 1 Absatz 1, § 2 Absatz 1) - Schlüsselzahlen für den Gemeindeanteil an der Umsatzsteuer für die Haushaltsjahre 2019 und 2020") 210.4 KB

2

[Anlage 2 (zu § 3 Absatz 3) - Termine der vierteljährlichen Abschlagszahlungen für die jeweiligen Haushaltsjahre](/br2/sixcms/media.php/68/UStAV-Anlage-2.pdf "Anlage 2 (zu § 3 Absatz 3) - Termine der vierteljährlichen Abschlagszahlungen für die jeweiligen Haushaltsjahre") 5.4 KB