## Brandenburgische Verordnung über den Bau und Betrieb von Verkaufsstätten (Brandenburgische Verkaufsstätten-Bauverordnung - BbgVBauV)

Auf Grund des § 86 Absatz 1 Nummer 1 und Nummer 4 bis 6 und Absatz 3 Satz 1 Nummer 5 in Verbindung mit § 85 Absatz 1 Satz 1 Nummer 1 der Brandenburgischen Bauordnung vom 19. Mai 2016 (GVBl.
I Nr. 14) verordnet die Ministerin für Infrastruktur und Landesplanung:

**Inhaltsübersicht**

[§ 1 Anwendungsbereich](#1)

[§ 2 Begriffe](#2)

[§ 3 Tragende Wände, Pfeiler und Stützen](#3)

[§ 4 Außenwände](#4)

[§ 5 Trennwände](#5)

[§ 6 Brandabschnitte](#6)

[§ 7 Decken](#7)

[§ 8 Dächer](#8)

[§ 9 Bekleidungen, Dämmstoffe](#9)

[§ 10 Rettungswege in Verkaufsstätten](#10)

[§ 11 Treppen](#11)

[§ 12 Treppenräume, Treppenraumerweiterungen](#12)

[§ 13 Ladenstraßen, Flure, Hauptgänge](#13)

[§ 14 Ausgänge](#14)

[§ 15 Türen in Rettungswegen](#15)

[§ 16 Rauchableitung](#16)

[§ 17 Beheizung](#17)

[§ 18 Sicherheitsbeleuchtung](#18)

[§ 19 Blitzschutzanlagen](#19)

[§ 20 Feuerlöscheinrichtungen, Brandmeldeanlagen und Alarmierungseinrichtungen, Brandfallsteuerung der Aufzüge](#20)

[§ 21 Sicherheitsstromversorgungsanlagen](#21)

[§ 22 Lage der Verkaufsräume](#22)

[§ 23 Räume für Abfälle](#23)

[§ 24 Gefahrenverhütung](#24)

[§ 25 Rettungswege auf dem Grundstück, Flächen für die Feuerwehr](#25)

[§ 26 Verantwortliche Personen](#26)

[§ 27 Brandschutzordnung, Räumungskonzept](#27)

[§ 28 Barrierefreie Stellplätze](#28)

[§ 29 Zusätzliche Bauvorlagen](#29)

[§ 30 Weitergehende Anforderungen](#30)

[§ 31 Ordnungswidrigkeiten](#31)

[§ 32 Übergangsvorschriften](#32)

[§ 33 Inkrafttreten, Außerkrafttreten](#33)

#### § 1 Anwendungsbereich

Die Vorschriften dieser Verordnung gelten für jede Verkaufsstätte, deren Verkaufsräume und Ladenstraßen einschließlich ihrer Bauteile eine Fläche von insgesamt mehr als 2 000 Quadratmeter haben.

#### § 2 Begriffe

(1) Verkaufsstätten sind Gebäude oder Gebäudeteile, die

1.  ganz oder teilweise dem Verkauf von Waren dienen,
2.  mindestens einen Verkaufsraum haben und
3.  keine Messebauten sind.

Zu einer Verkaufsstätte gehören alle Räume, die unmittelbar oder mittelbar, insbesondere durch Aufzüge oder Ladenstraßen, miteinander in Verbindung stehen; als Verbindung gilt nicht die Verbindung durch notwendige Treppenräume sowie durch Leitungen, Schächte und Kanäle haustechnischer Anlagen.

(2) Erdgeschossige Verkaufsstätten sind Gebäude mit nicht mehr als einem Geschoss, dessen Fußboden an keiner Stelle mehr als 1 Meter unter der Geländeoberfläche liegt; dabei bleiben Treppenraumerweiterungen sowie Geschosse außer Betracht, die ausschließlich der Unterbringung haustechnischer Anlagen und Feuerungsanlagen dienen.

(3) Verkaufsräume sind Räume, in denen Waren zum Verkauf oder sonstige Leistungen angeboten werden oder die dem Kundenverkehr dienen, ausgenommen notwendige Treppenräume, Treppenraumerweiterungen sowie Garagen.
Ladenstraßen gelten nicht als Verkaufsräume.

(4) Ladenstraßen sind überdachte oder überdeckte Flächen, an denen Verkaufsräume liegen und die dem Kundenverkehr dienen.

(5) Treppenraumerweiterungen sind Räume, die Treppenräume mit Ausgängen ins Freie verbinden.

#### § 3 Tragende Wände, Pfeiler und Stützen

Tragende Wände, Pfeiler und Stützen müssen feuerbeständig, bei erdgeschossigen Verkaufsstätten ohne Sprinkleranlagen mindestens feuerhemmend sein.
Dies gilt nicht für erdgeschossige Verkaufsstätten mit Sprinkleranlagen.

#### § 4 Außenwände

Außenwände müssen bestehen aus

1.  nichtbrennbaren Baustoffen, soweit sie nicht feuerbeständig sind, bei Verkaufsstätten ohne Sprinkleranlagen,
2.  mindestens schwerentflammbaren Baustoffen, soweit sie nicht feuerbeständig sind, bei Verkaufsstätten mit Sprinkleranlagen,
3.  mindestens schwerentflammbaren Baustoffen, soweit sie nicht mindestens feuerhemmend sind, bei erdgeschossigen Verkaufsstätten.

#### § 5 Trennwände

(1) Trennwände zwischen einer Verkaufsstätte und Räumen, die nicht zur Verkaufsstätte gehören, müssen feuerbeständig sein und dürfen keine Öffnungen haben.

(2) In Verkaufsstätten ohne Sprinkleranlagen sind Lagerräume mit einer Fläche von mehr als jeweils 100 Quadratmeter sowie Werkräume mit erhöhter Brandgefahr, wie Schreinereien, Maler- oder Dekorationswerkstätten, von anderen Räumen durch feuerbeständige Wände zu trennen.
Diese Werk- und Lagerräume müssen durch feuerbeständige Trennwände so unterteilt werden, dass Abschnitte von nicht mehr als 500 Quadratmeter entstehen.
Öffnungen in den Trennwänden müssen mindestens feuerhemmende und selbstschließende Abschlüsse haben.

#### § 6 Brandabschnitte

(1) Verkaufsstätten sind durch Brandwände in Brandabschnitte zu unterteilen.
Die Fläche der Brandabschnitte darf je Geschoss betragen in

1.  erdgeschossigen Verkaufsstätten mit Sprinkleranlagen nicht mehr als 10 000 Quadratmeter,
2.  sonstigen Verkaufsstätten mit Sprinkleranlagen nicht mehr als 5 000 Quadratmeter,
3.  erdgeschossigen Verkaufsstätten ohne Sprinkleranlagen nicht mehr als 3 000 Quadratmeter,
4.  sonstigen Verkaufsstätten ohne Sprinkleranlagen nicht mehr als 1 500 Quadratmeter, wenn sich die Verkaufsstätten über nicht mehr als drei Geschosse erstrecken und die Gesamtfläche aller Geschosse innerhalb eines Brandabschnitts nicht mehr als 3 000 Quadratmeter beträgt.

(2) Abweichend von Absatz 1 können Verkaufsstätten mit Sprinkleranlagen auch durch Ladenstraßen in Brandabschnitte unterteilt werden, wenn

1.  die Ladenstraßen bis zu ihrem oberen Abschluss in voller Höhe zusammenhängend mindestens 10 Meter breit sind; in diesen Ladenstraßen sind Einbauten oder Einrichtungen innerhalb dieser Breite unzulässig, ausgenommen sind Fahrtreppen und Aufzüge sowie Einrichtungen der technischen Gebäudeausrüstung, die der Ladenstraße dienen; § 13 Absatz 5 bleibt unberührt;
2.  die Ladenstraßen Öffnungen und Flächen für den Wärmeabzug von insgesamt 5 Prozent der Grundflächen der Ladenstraßen haben, die Öffnungen und Flächen möglichst an oberster Stelle liegen und regelmäßig angeordnet sind und Öffnungen einzeln eine freie Mindestfläche von 1 Quadratmeter aufweisen;
3.  das Tragwerk der Dächer der Ladenstraßen aus nichtbrennbaren Baustoffen besteht und die Bedachung der Ladenstraße die Anforderungen nach § 8 Absatz 2 Nummer 1 und Absatz 3 Nummer 1 erfüllt.

(3) In Verkaufsstätten mit Sprinkleranlagen brauchen Brandwände abweichend von Absatz 1 im Kreuzungsbereich mit Ladenstraßen nicht hergestellt zu werden, wenn

1.  die Ladenstraßen bis zu ihrem oberen Abschluss in voller Höhe eine zusammenhängende Breite über eine zusammenhängende Länge von jeweils mindestens 10 Meter beiderseits der Brandwände haben und
2.  die Anforderungen nach Absatz 2 Nummer 1 Halbsatz 2, Nummer 2 und 3 in diesem Bereich erfüllt sind.

(4) Öffnungen in den Brandwänden nach Absatz 1 sind zulässig, wenn sie selbstschließende und feuerbeständige Abschlüsse haben.
Die Abschlüsse müssen Feststellanlagen haben, die bei Raucheinwirkung ein selbsttätiges Schließen bewirken.

(5) Brandwände sind mindestens 30 Zentimeter über Dach zu führen oder in Höhe der Dachhaut mit einer beiderseits 50 Zentimeter auskragenden feuerbeständigen Platte aus nichtbrennbaren Baustoffen abzuschließen; darüber dürfen brennbare Teile des Dachs nicht hinweggeführt werden.

(6) § 30 Absatz 2 Nummer 1 der Brandenburgischen Bauordnung bleibt unberührt.

#### § 7 Decken

(1) Decken müssen feuerbeständig sein und aus nichtbrennbaren Baustoffen bestehen.
Decken über Geschossen, deren Fußboden an keiner Stelle mehr als 1 Meter unter der Geländeoberfläche liegt, brauchen nur

1.  feuerhemmend zu sein und aus nichtbrennbaren Baustoffen zu bestehen in erdgeschossigen Verkaufsstätten ohne Sprinkleranlagen,
2.  aus nichtbrennbaren Baustoffen zu bestehen in erdgeschossigen Verkaufsstätten mit Sprinkleranlagen.

Für die Beurteilung der Feuerwiderstandsfähigkeit bleiben abgehängte Unterdecken außer Betracht.

(2) Unterdecken einschließlich ihrer Aufhängungen müssen in Verkaufsräumen, Treppenräumen, Treppenraumerweiterungen, notwendigen Fluren und in Ladenstraßen aus nichtbrennbaren Baustoffen bestehen.
In Verkaufsräumen mit Sprinkleranlagen dürfen Unterdecken aus brennbaren Baustoffen bestehen, wenn auch der Deckenhohlraum durch die Sprinkleranlagen geschützt ist.

(3) In Decken sind Öffnungen unzulässig.
Dies gilt nicht für Öffnungen zwischen Verkaufsräumen, zwischen Verkaufsräumen und Ladenstraßen sowie zwischen Ladenstraßen

1.  in Verkaufsstätten mit Sprinkleranlagen,
2.  in Verkaufsstätten ohne Sprinkleranlagen, soweit die Öffnungen für nicht notwendige Treppen erforderlich sind.

#### § 8 Dächer

(1) Das Tragwerk von Dächern, die den oberen Abschluss von Räumen der Verkaufsstätten bilden oder die von diesen Räumen nicht durch feuerbeständige Bauteile getrennt sind, muss

1.  aus nichtbrennbaren Baustoffen bestehen in Verkaufsstätten mit Sprinkleranlagen, ausgenommen in erdgeschossigen Verkaufsstätten,
2.  mindestens feuerhemmend sein in erdgeschossigen Verkaufsstätten ohne Sprinkleranlagen,
3.  feuerbeständig sein in sonstigen Verkaufsstätten ohne Sprinkleranlagen.

(2) Bedachungen müssen

1.  gegen Flugfeuer und strahlende Wärme widerstandsfähig sein und
2.  bei Dächern, die den oberen Abschluss von Räumen der Verkaufsstätten bilden oder die von diesen Räumen nicht durch feuerbeständige Bauteile getrennt sind, aus nichtbrennbaren Baustoffen bestehen mit Ausnahme der Dachhaut und der Dampfsperre.

(3) Lichtdurchlässige Bedachungen über Verkaufsräumen und Ladenstraßen dürfen abweichend von Absatz 2 Nummer 1

1.  schwerentflammbar sein bei Verkaufsstätten mit Sprinkleranlagen,
2.  nichtbrennbar sein bei Verkaufsstätten ohne Sprinkleranlagen.

Sie dürfen im Brandfall nicht brennend abtropfen.

#### § 9 Bekleidungen, Dämmstoffe

(1) Außenwandbekleidungen einschließlich der Dämmstoffe und Unterkonstruktionen müssen bestehen aus

1.  mindestens schwerentflammbaren Baustoffen bei Verkaufsstätten mit Sprinkleranlagen und bei erdgeschossigen Verkaufsstätten,
2.  nichtbrennbaren Baustoffen bei sonstigen Verkaufsstätten ohne Sprinkleranlagen.

(2) Deckenbekleidungen einschließlich der Dämmstoffe und Unterkonstruktionen müssen aus nichtbrennbaren Baustoffen bestehen.

(3) Wandbekleidungen einschließlich der Dämmstoffe und Unterkonstruktionen müssen in Treppenräumen, Treppenraumerweiterungen, notwendigen Fluren und in Ladenstraßen aus nichtbrennbaren Baustoffen bestehen.

#### § 10 Rettungswege in Verkaufsstätten

(1) Für jeden Verkaufsraum, Aufenthaltsraum und für jede Ladenstraße müssen in demselben Geschoss mindestens zwei voneinander unabhängige Rettungswege zu Ausgängen ins Freie oder zu notwendigen Treppenräumen vorhanden sein.
Anstelle eines dieser Rettungswege darf ein Rettungsweg über Außentreppen ohne Treppenräume, Rettungsbalkone, Terrassen und begehbare Dächer auf das Grundstück führen, wenn hinsichtlich des Brandschutzes keine Bedenken bestehen; dieser Rettungsweg gilt als Ausgang ins Freie.

(2) Von jeder Stelle

1.  eines Verkaufsraums in höchstens 25 Meter Entfernung,
2.  eines sonstigen Raums oder einer Ladenstraße in höchstens 35 Meter Entfernung

muss mindestens ein Ausgang ins Freie oder ein notwendiger Treppenraum erreichbar sein (erster Rettungsweg).

(3) Der erste Rettungsweg darf, soweit er über eine Ladenstraße führt, auf der Ladenstraße eine zusätzliche Länge von höchstens 35 Meter haben, wenn

1.  der nach Absatz 1 erforderliche zweite Rettungsweg für Verkaufsräume nicht über diese Ladenstraße führt oder
2.  der Verkaufsraum eine Fläche von insgesamt nicht mehr als 100 Quadratmeter und eine Raumtiefe von höchstens 10 Meter hat, großflächige Sichtbeziehungen zur Ladenstraße bestehen und die Ladenstraße in diesem Bereich über zwei entgegengesetzte Fluchtrichtungen ins Freie verfügt.

(4) In Verkaufsstätten mit Sprinkleranlagen oder in erdgeschossigen Verkaufsstätten darf der Rettungsweg nach den Absätzen 2 und 3 innerhalb von Brandabschnitten eine zusätzliche Länge von höchstens 35 Meter haben, soweit er über einen notwendigen Flur für Kundinnen und Kunden mit einem unmittelbaren Ausgang ins Freie oder in einen notwendigen Treppenraum führt.

(5) Von jeder Stelle eines Verkaufsraums muss ein Hauptgang oder eine Ladenstraße in höchstens 10 Meter Entfernung erreichbar sein.

(6) In Rettungswegen ist nur eine Folge von mindestens drei Stufen zulässig.
Die Stufen müssen eine Stufenbeleuchtung haben.

(7) An Kreuzungen der Ladenstraßen und der Hauptgänge sowie an Türen im Zuge von Rettungswegen ist deutlich und dauerhaft auf die Ausgänge durch Sicherheitszeichen hinzuweisen.
Die Sicherheitszeichen müssen beleuchtet sein.

(8) Die Entfernungen nach den Absätzen 2 bis 5 sind in der Luftlinie, jedoch nicht durch Bauteile zu messen.
Die Länge der Lauflinie darf in Verkaufsräumen 35 Meter nicht überschreiten.

#### § 11 Treppen

(1) Notwendige Treppen müssen feuerbeständig sein, aus nichtbrennbaren Baustoffen bestehen und an den Unterseiten geschlossen sein.
Dies gilt nicht für notwendige Treppen nach § 10 Absatz 1 Satz 2, wenn wegen des Brandschutzes Bedenken nicht bestehen.

(2) Notwendige Treppen für Kundinnen und Kunden müssen mindestens 2 Meter breit sein und dürfen eine Breite von 2,50 Meter nicht überschreiten.
Für notwendige Treppen für Kundinnen und Kunden genügt eine Breite von mindestens 1,25 Meter, wenn die Treppen für Verkaufsräume bestimmt sind, deren Fläche insgesamt nicht mehr als 500 Quadratmeter beträgt.

(3) Notwendige Treppen brauchen nicht in eigenen Treppenräumen im Sinne von § 35 Absatz 1 der Brandenburgischen Bauordnung zu liegen und die Anforderungen nach Absatz 1 Satz 1 nicht zu erfüllen in Verkaufsräumen, die

1.  eine Fläche von nicht mehr als 100 Quadratmeter haben oder
2.  eine Fläche von mehr als 100 Quadratmeter, aber nicht mehr als 500 Quadratmeter haben, wenn diese Treppen im Zuge nur eines der zwei erforderlichen Rettungswege liegen.

Notwendige Treppen mit gewendelten Läufen sind in Verkaufsräumen unzulässig.
Dies gilt nicht für notwendige Treppen nach Satz 1.

(4) Treppen für Kundinnen und Kunden müssen auf beiden Seiten Handläufe ohne freie Enden haben.
Die Handläufe müssen fest und griffsicher sein und sind über Treppenabsätze fortzuführen.

#### § 12 Treppenräume, Treppenraumerweiterungen

(1) Die Wände von notwendigen Treppenräumen müssen in der Bauart von Brandwänden hergestellt sein.
Bodenbeläge müssen in notwendigen Treppenräumen aus nichtbrennbaren Baustoffen bestehen.

(2) Treppenraumerweiterungen müssen

1.  die Anforderungen an Treppenräume erfüllen,
2.  feuerbeständige Decken aus nichtbrennbaren Baustoffen haben und
3.  mindestens so breit sein wie die notwendigen Treppen, mit denen sie in Verbindung stehen.

Sie dürfen nicht länger als 35 Meter sein und keine Öffnungen zu anderen Räumen haben.

#### § 13 Ladenstraßen, Flure, Hauptgänge

(1) Ladenstraßen müssen mindestens 5 Meter breit sein.

(2) Wände und Decken notwendiger Flure für Kundinnen und Kunden müssen

1.  feuerbeständig sein und aus nichtbrennbaren Baustoffen bestehen in Verkaufsstätten ohne Sprinkleranlagen,
2.  mindestens feuerhemmend sein und in den wesentlichen Teilen aus nichtbrennbaren Baustoffen bestehen in Verkaufsstätten mit Sprinkleranlagen.

Bodenbeläge in notwendigen Fluren für Kundinnen und Kunden müssen mindestens schwerentflammbar sein.

(3) Notwendige Flure für Kundinnen und Kunden müssen mindestens 2 Meter breit sein.
Für notwendige Flure für Kundinnen und Kunden genügt eine Breite von 1,50 Meter, wenn die Flure für Verkaufsräume bestimmt sind, deren Fläche insgesamt nicht mehr als 500 Quadratmeter beträgt.

(4) Hauptgänge müssen mindestens 2 Meter breit sein.
Sie müssen auf möglichst kurzem Weg zu Ausgängen ins Freie, zu notwendigen Treppenräumen, zu notwendigen Fluren für Kundinnen und Kunden oder zu Ladenstraßen führen.
Verkaufsstände an Hauptgängen müssen unverrückbar sein.

(5) Ladenstraßen, notwendige Flure für Kundinnen und Kunden und Hauptgänge dürfen innerhalb der nach den Absätzen 1, 3 und 4 erforderlichen Breiten nicht durch Einbauten oder Einrichtungen eingeengt sein.

#### § 14 Ausgänge

(1) Jeder Verkaufsraum, Aufenthaltsraum und jede Ladenstraße müssen mindestens zwei Ausgänge haben, die zum Freien oder zu notwendigen Treppenräumen führen.
Für Verkaufs- und Aufenthaltsräume, die eine Fläche von nicht mehr als 100 Quadratmeter haben, genügt ein Ausgang.

(2) Ausgänge aus Verkaufsräumen müssen mindestens 2 Meter breit sein; für Ausgänge aus Verkaufsräumen, die eine Fläche von nicht mehr als 500 Quadratmeter haben, genügt eine Breite von 1 Meter.
Ein Ausgang, der in einen Flur führt, darf nicht breiter sein als der Flur.

(3) Die Ausgänge aus einem Geschoss einer Verkaufsstätte ins Freie oder in notwendige Treppenräume müssen eine Breite von

1.  mindestens 30 Zentimeter je 100 Quadratmeter der Flächen der Verkaufsräume und
2.  der Hälfte der Flächen der Ladenstraßen, mindestens jedoch der Flächen der Ladenstraßen bezogen auf die Mindestbreite nach § 13 Absatz 1

haben.
Ausgänge aus den Geschossen einer Verkaufsstätte müssen mindestens 2 Meter breit sein.
Ein Ausgang, der in einen Treppenraum führt, darf nicht breiter sein als die notwendige Treppe.

(4) Ausgänge aus notwendigen Treppenräumen ins Freie oder in Treppenraumerweiterungen müssen mindestens so breit sein wie die notwendigen Treppen.

#### § 15 Türen in Rettungswegen

(1) In Verkaufsstätten ohne Sprinkleranlagen müssen Türen von notwendigen Treppenräumen und von notwendigen Fluren für Kundinnen und Kunden mindestens feuerhemmend, rauchdicht und selbstschließend sein, ausgenommen Türen, die ins Freie führen.

(2) In Verkaufsstätten mit Sprinkleranlagen müssen Türen von notwendigen Treppenräumen und von notwendigen Fluren für Kundinnen und Kunden rauchdicht und selbstschließend sein, ausgenommen Türen, die ins Freie führen.

(3) Türen nach den Absätzen 1 und 2 sowie Türen, die ins Freie führen, dürfen nur in Fluchtrichtung aufschlagen und keine Schwellen haben.
Sie müssen während der Betriebszeit von innen leicht in voller Breite zu öffnen sein.
Elektrische Verriegelungen von Türen in Rettungswegen sind nur zulässig, wenn die Türen im Gefahrenfall jederzeit geöffnet werden können.

(4) Türen, die selbstschließend sein müssen, dürfen offengehalten werden, wenn sie Feststellanlagen haben, die bei Raucheinwirkung ein selbsttätiges Schließen der Türen bewirken; sie müssen auch von Hand geschlossen werden können.

(5) Drehtüren und Schiebetüren sind in Rettungswegen unzulässig; dies gilt nicht für automatische Dreh- und Schiebetüren, die die Rettungswege im Gefahrenfall nicht beeinträchtigen.
Pendeltüren müssen in Rettungswegen Schließvorrichtungen haben, die ein Durchpendeln der Türen verhindern.

(6) Rollläden, Scherengitter oder ähnliche Abschlüsse von Türöffnungen, Toröffnungen oder Durchfahrten im Zug von Rettungswegen müssen so beschaffen sein, dass sie von Unbefugten nicht geschlossen werden können.

#### § 16 Rauchableitung

(1) In Verkaufsstätten müssen Verkaufsräume und sonstige Aufenthaltsräume mit jeweils mehr als 50 Quadratmeter Grundfläche, Lagerräume mit mehr als 200 Quadratmeter Grundfläche, Ladenstraßen sowie notwendige Treppenräume zur Unterstützung der Brandbekämpfung entraucht werden können.

(2) Die Anforderung des Absatzes 1 ist insbesondere erfüllt bei

1.  Verkaufsräumen und sonstigen Aufenthaltsräumen bis 200 Quadratmeter Grundfläche, wenn diese Räume Fenster nach § 47 Absatz 2 der Brandenburgischen Bauordnung haben,
2.  Verkaufsräumen, sonstigen Aufenthaltsräumen und Lagerräumen mit nicht mehr als 1 000 Quadratmeter Grundfläche, wenn diese Räume entweder an der obersten Stelle Öffnungen zur Rauchableitung mit einem freien Querschnitt von insgesamt 1 Prozent der Grundfläche oder im oberen Drittel der Außenwände angeordnete Öffnungen, Türen oder Fenster mit einem freien Querschnitt von insgesamt 2 Prozent der Grundfläche haben und Zuluftflächen in insgesamt gleicher Größe, jedoch mit nicht mehr als 12 Quadratmeter freiem Querschnitt, vorhanden sind, die im unteren Raumdrittel angeordnet werden sollen,
3.  Verkaufsräumen, sonstigen Aufenthaltsräumen und Lagerräumen mit mehr als 1 000 Quadratmeter Grundfläche, wenn diese Räume Rauchabzugsanlagen haben, bei denen je höchstens 400 Quadratmeter der Grundfläche mindestens ein Rauchabzugsgerät mit mindestens 1,5 Quadratmeter aerodynamisch wirksamer Fläche im oberen Raumdrittel angeordnet wird, je höchstens 1 600 Quadratmeter Grundfläche mindestens eine Auslösegruppe für die Rauchabzugsgeräte gebildet wird und Zuluftflächen im unteren Raumdrittel von insgesamt mindestens 12 Quadratmeter freiem Querschnitt vorhanden sind,
4.  Ladenstraßen mit nur auf einer Ebene liegenden Verkehrsflächen, wenn diese Ladenstraßen Rauchabzugsanlagen haben, bei denen je höchstens 20 Meter Länge der Ladenstraße mindestens ein Rauchabzugsgerät mit mindestens 1,5 Quadratmeter aerodynamisch wirksamer Fläche im oberen Raumdrittel angeordnet wird, je 80 Meter Länge der Ladenstraße mindestens eine Auslösegruppe für die Rauchabzugsgeräte gebildet wird und Zuluftflächen im unteren Raumdrittel von insgesamt mindestens 12 Quadratmeter freiem Querschnitt vorhanden sind; bei sonstigen Ladenstraßen, wenn die Ladenstraßen Rauchabzugsanlagen haben, bei denen die Größe und Anordnung der Rauchabzugsgeräte und der notwendigen Zuluftflächen hinsichtlich des Schutzziels des Absatzes 1 ausreichend bemessen sind.

(3) Die Anforderung des Absatzes 1 ist insbesondere auch erfüllt, wenn in den Fällen des Absatzes 2 Nummer 1 bis 3 und 4 Halbsatz 1 maschinelle Rauchabzugsanlagen vorhanden sind, bei denen je höchstens 400 Quadratmeter der Grundfläche der Räume mindestens ein Rauchabzugsgerät oder eine Absaugstelle mit einem Luftvolumenstrom von 10 000 Kubikmeter pro Stunde im oberen Raumdrittel angeordnet wird.
Bei Räumen mit mehr als 1 600 Quadratmeter Grundfläche genügt

1.  zu dem Luftvolumenstrom von 40 000 Kubikmeter pro Stunde für die Grundfläche von 1 600 Quadratmeter ein zusätzlicher Luftvolumenstrom von 5 000 Kubikmeter pro Stunde je angefangene weitere 400 Quadratmeter Grundfläche; der sich ergebende Gesamtvolumenstrom je Raum ist gleichmäßig auf die nach Satz 1 anzuordnenden Absaugstellen oder Rauchabzugsgeräte zu verteilen, oder
2.  ein Luftvolumenstrom von mindestens 40 000 Kubikmeter pro Stunde je Raum, wenn sichergestellt ist, dass dieser Luftvolumenstrom im Bereich der Brandstelle auf einer Grundfläche von höchstens 1 600 Quadratmeter von den nach Satz 1 anzuordnenden Absaugstellen oder Rauchabzugsgeräten gleichmäßig gefördert werden kann.

Die Zuluftflächen müssen im unteren Raumdrittel in solcher Größe und so angeordnet werden, dass eine maximale Strömungsgeschwindigkeit von 3 Meter pro Sekunde nicht überschritten wird.
Anstelle der Rauchabzugsanlagen für sonstige Ladenstraßen nach Absatz 2 Nummer 4 Halbsatz 2 können maschinelle Rauchabzugsanlagen verwendet werden, wenn sie bezüglich des Schutzziels nach Absatz 1 ausreichend bemessen sind.

(4) Die Anforderung des Absatzes 1 ist auch erfüllt bei Räumen nach Absatz 2 Nummer 1 bis 3 in Verkaufsstätten mit Sprinkleranlagen, wenn in diesen Räumen vorhandene Lüftungsanlagen automatisch bei Auslösen der Brandmeldeanlage oder, soweit § 20 Absatz 2 Nummer 2 Halbsatz 2 Anwendung findet, der Sprinkleranlage so betrieben werden, dass sie nur entlüften und die ermittelten Luftvolumenströme nach Absatz 3 Satz 1 und Satz 2 Nummer 1 einschließlich Zuluft erreicht werden, soweit es die Zweckbestimmung der Absperrvorrichtungen gegen Brandübertragung zulässt; in Leitungen zum Zweck der Entlüftung dürfen Absperrvorrichtungen nur thermische Auslöser haben.

(5) Die Anforderung des Absatzes 1 ist erfüllt bei

1.  notwendigen Treppenräumen mit Fenstern gemäß § 35 Absatz 8 Satz 2 Nummer 1 der Brandenburgischen Bauordnung, wenn diese Treppenräume an der obersten Stelle eine Öffnung zur Rauchableitung mit einem freien Querschnitt von mindestens 1 Quadratmeter haben, und
2.  notwendigen Treppenräumen gemäß § 35 Absatz 8 Satz 2 Nummer 2 der Brandenburgischen Bauordnung, wenn diese Treppenräume Rauchabzugsgeräte mit insgesamt mindestens 1 Quadratmeter aerodynamisch wirksamer Fläche haben, die im oder unmittelbar unter dem oberen Treppenraumabschluss angeordnet werden.

(6) Anstelle von Öffnungen zur Rauchableitung nach Absatz 2 Nummer 2 und Absatz 5 Nummer 1 sowie Rauchabzugsgeräten nach Absatz 5 Nummer 2 ist die Rauchableitung über Schächte mit strömungstechnisch äquivalenten Querschnitten zulässig, wenn die Wände der Schächte raumabschließend und so feuerwiderstandsfähig wie die durchdrungenen Bauteile, mindestens jedoch feuerhemmend sowie aus nichtbrennbaren Baustoffen sind.

(7) Türen oder Fenster nach Absatz 2 Nummer 2, mit Abschlüssen versehene Öffnungen zur Rauchableitung nach Absatz 2 Nummer 2 und Absatz 5 Nummer 1 und Rauchabzugsgeräte nach Absatz 5 Nummer 2 müssen Vorrichtungen zum Öffnen haben, die von jederzeit zugänglichen Stellen aus leicht von Hand bedient werden können; sie können auch an einer jederzeit zugänglichen Stelle zusammengeführt werden.
In notwendigen Treppenräumen müssen die Vorrichtungen von jedem Geschoss aus bedient werden können.
Geschlossene Öffnungen, die als Zuluftflächen dienen, müssen leicht geöffnet werden können.

(8) Rauchabzugsanlagen müssen automatisch auslösen und von Hand von einer jederzeit zugänglichen Stelle ausgelöst werden können.

(9) Manuelle Bedienungs- und Auslösestellen nach Absatz 7 und 8 sind mit einem Hinweisschild mit der Bezeichnung „RAUCHABZUG“ und der Angabe des jeweiligen Raums zu versehen.
An den Stellen müssen die Betriebsstellung der jeweiligen Anlage sowie der Fenster, Türen, Abschlüsse und Rauchabzugsgeräte erkennbar sein.

(10) Maschinelle Rauchabzugsanlagen sind für eine Betriebszeit von 30 Minuten bei einer Rauchgastemperatur von 600 Grad Celsius auszulegen.
Die Auslegung kann mit einer Rauchgastemperatur von 300 Grad Celsius erfolgen, wenn der Luftvolumenstrom des Raums mindestens 40 000 Kubikmeter pro Stunde beträgt.
Die Zuluftzuführung muss durch automatische Ansteuerung und spätestens gleichzeitig mit Inbetriebnahme der der Anlage erfolgen.
Maschinelle Lüftungsanlagen können als maschinelle Rauchabzugsanlagen betrieben werden, wenn sie die an diese gestellten Anforderungen erfüllen.

#### § 17 Beheizung

Feuerstätten dürfen in Verkaufsräumen, Ladenstraßen, Lagerräumen und Werkräumen zur Beheizung nicht aufgestellt werden.

#### § 18 Sicherheitsbeleuchtung

(1) In Verkaufsstätten muss eine Sicherheitsbeleuchtung vorhanden sein, die so beschaffen ist, dass sich Besucherinnen und Besucher und Betriebsangehörige auch bei vollständigem Versagen der allgemeinen Beleuchtung bis zu öffentlichen Verkehrsflächen hin gut zurechtfinden können.

(2) Eine Sicherheitsbeleuchtung muss vorhanden sein

1.  in notwendigen Treppenräumen, in Räumen zwischen notwendigen Treppenräumen und Ausgängen ins Freie und in notwendigen Fluren,
2.  in Verkaufsräumen und allen übrigen Räumen für Besucherinnen und Besucher sowie Toilettenräumen mit mehr als 50 Quadratmeter Grundfläche,
3.  in Räumen für Beschäftigte mit mehr als 20 Quadratmeter Grundfläche, ausgenommen Büroräume,
4.  in elektrischen Betriebsräumen und Räumen für haustechnische Anlagen,
5.  für Sicherheitszeichen von Ausgängen und Rettungswegen,
6.  für Stufenbeleuchtungen.

#### § 19 Blitzschutzanlagen

Gebäude mit Verkaufsstätten müssen Blitzschutzanlagen haben.

#### § 20 Feuerlöscheinrichtungen, Brandmeldeanlagen und Alarmierungseinrichtungen, Brandfallsteuerung der Aufzüge

(1) Verkaufsstätten müssen Sprinkleranlagen haben.
Dies gilt nicht für

1.  erdgeschossige Verkaufsstätten nach § 6 Absatz 1 Nummer 3,
2.  sonstige Verkaufsstätten nach § 6 Absatz 1 Nummer 4.

Geschosse einer Verkaufsstätte nach Satz 2 Nummer 2 müssen Sprinkleranlagen haben, wenn sie mit ihrem Fußboden im Mittel mehr als 3 Meter unter der Geländeoberfläche liegen und Verkaufsräume eine Fläche von mehr als 500 Quadratmeter haben.

(2) In Verkaufsstätten müssen vorhanden sein:

1.  geeignete Feuerlöscher und Wandhydranten für die Feuerwehr (Typ F) in ausreichender Zahl, gut sichtbar und leicht zugänglich; im Einvernehmen mit der Brandschutzdienststelle kann auf Wandhydranten verzichtet oder können anstelle von Wandhydranten trockene Löschwasserleitungen zugelassen werden;
2.  Brandmeldeanlagen mit automatischen und nichtautomatischen Brandmeldern; auf automatische Brandmelder kann verzichtet werden, wenn in diesen Räumen während der Betriebszeit ständig entsprechend eingewiesene Betriebsangehörige in ausreichender Anzahl anwesend sind; die Brandmeldungen müssen von der Brandmelderzentrale unmittelbar und automatisch zur zuständigen Regionalleitstelle weitergeleitet werden, automatische Brandmeldeanlagen müssen durch technische Maßnahmen gegen Falschalarme gesichert sein und
3.  Alarmierungseinrichtungen, durch die alle Betriebsangehörigen alarmiert und Anweisungen an sie und an die Kundinnen und Kunden gegeben werden können.

(3) In Verkaufsstätten müssen die Aufzüge mit einer Brandfallsteuerung ausgestattet sein, die durch die Brandmeldeanlage ausgelöst wird.
Die Brandfallsteuerung muss sicherstellen, dass die Aufzüge ein Geschoss mit Ausgängen ins Freie oder das diesem nächstgelegene, nicht von der Brandmeldung betroffene Geschoss unmittelbar anfahren und dort mit geöffneten Türen außer Betrieb gehen.

#### § 21 Sicherheitsstromversorgungsanlagen

Verkaufsstätten müssen eine Sicherheitsstromversorgungsanlage haben, die bei Ausfall der allgemeinen Stromversorgung den Betrieb der sicherheitstechnischen Anlagen und Einrichtungen übernimmt, insbesondere der

1.  Sicherheitsbeleuchtung,
2.  Beleuchtung der Stufen und Hinweise auf Ausgänge,
3.  Sprinkleranlagen,
4.  Rauchabzugsanlagen,
5.  Schließeinrichtungen für Feuerschutzabschlüsse (zum Beispiel Rolltore),
6.  Brandmeldeanlagen,
7.  Alarmierungseinrichtungen.

#### § 22 Lage der Verkaufsräume

Verkaufsräume, ausgenommen Gaststätten, dürfen mit ihrem Fußboden nicht mehr als 22 Meter über der Geländeoberfläche liegen.
Verkaufsräume dürfen mit ihrem Fußboden im Mittel nicht mehr als 5 Meter unter der Geländeoberfläche liegen.

#### § 23 Räume für Abfälle

Verkaufsstätten müssen für Abfälle besondere Räume haben, die mindestens den Abfall von zwei Tagen aufnehmen können.
Die Räume müssen feuerbeständige Wände und Decken sowie mindestens feuerhemmende und selbstschließende Türen haben.

#### § 24 Gefahrenverhütung

(1) Das Rauchen und das Verwenden von offenem Feuer ist in Verkaufsräumen und Ladenstraßen verboten.
Dies gilt nicht für Bereiche, in denen Getränke oder Speisen verabreicht oder Besprechungen abgehalten werden.
Auf das Verbot ist dauerhaft und leicht erkennbar hinzuweisen.

(2) In notwendigen Treppenräumen, in Treppenraumerweiterungen und in notwendigen Fluren dürfen keine Dekorationen vorhanden sein.
In diesen Räumen sowie auf Ladenstraßen und Hauptgängen innerhalb der nach § 13 Absatz 1 und 4 erforderlichen Breiten dürfen keine Gegenstände abgestellt sein.

#### § 25 Rettungswege auf dem Grundstück, Flächen für die Feuerwehr

(1) Kundinnen und Kunden und Betriebsangehörige müssen aus der Verkaufsstätte unmittelbar oder über Flächen auf dem Grundstück auf öffentliche Verkehrsflächen gelangen können.

(2) Die erforderlichen Zufahrten, Durchfahrten und Aufstell- und Bewegungsflächen für die Feuerwehr müssen vorhanden sein.

(3) Die als Rettungswege dienenden Flächen auf dem Grundstück sowie die Flächen für die Feuerwehr nach Absatz 2 müssen ständig freigehalten werden.
Hierauf ist dauerhaft und leicht erkennbar hinzuweisen.

#### § 26 Verantwortliche Personen

(1) Während der Betriebszeit einer Verkaufsstätte muss die Betreiberin oder der Betreiber oder eine von ihr oder ihm bestimmte Vertretung ständig anwesend sein.

(2) Die Betreiberin oder der Betreiber einer Verkaufsstätte hat

1.  eine Brandschutzbeauftragte oder einen Brandschutzbeauftragten und
2.  für Verkaufsstätten, deren Verkaufsräume eine Fläche von insgesamt mehr als 15 000 Quadratmeter haben, Selbsthilfekräfte für den Brandschutz

zu bestellen.
Die Namen dieser Personen und jeder Wechsel sind der für den Brandschutz zuständigen Dienststelle auf Verlangen mitzuteilen.
Die Betreiberin oder der Betreiber hat für die Ausbildung dieser Personen im Einvernehmen mit der für den Brandschutz zuständigen Dienststelle zu sorgen.

(3) Die oder der Brandschutzbeauftragte hat für die Einhaltung des § 13 Absatz 5, der §§ 24, 25 Absatz 3, des § 26 Absatz 5 und des § 27 zu sorgen.

(4) Die erforderliche Anzahl der Selbsthilfekräfte für den Brandschutz ist von der Bauaufsichtsbehörde im Einvernehmen mit der für den Brandschutz zuständigen Dienststelle festzulegen.

(5) Selbsthilfekräfte für den Brandschutz müssen in erforderlicher Anzahl während der Betriebszeit der Verkaufsstätte anwesend sein.

#### § 27 Brandschutzordnung, Räumungskonzept

(1) Die Betreiberin oder der Betreiber einer Verkaufsstätte hat im Einvernehmen mit der Brandschutzdienststelle eine Brandschutzordnung aufzustellen.
Darin sind

1.  die Aufgaben der oder des Brandschutzbeauftragten und der Selbsthilfekräfte für den Brandschutz sowie
2.  die erforderlichen Maßnahmen, die im Gefahrenfall für eine schnelle und geordnete zur Räumung der gesamten Verkaufsstätte oder einzelner Bereiche unter besonderer Berücksichtigung von Menschen mit Behinderung erforderlich sind,

festzulegen.
Die Maßnahmen nach Satz 2 Nummer 2 sind bei Verkaufsstätten, deren Verkaufsräume eine Fläche von mehr als 5 000 Quadratmeter haben, gesondert in einem Räumungskonzept darzustellen.

(2) Die Betriebsangehörigen sind bei Beginn des Arbeitsverhältnisses und danach mindestens einmal jährlich zu belehren über

1.  die Lage und die Bedienung der Feuerlöschgeräte, Brandmelde- und Feuerlöscheinrichtungen und
2.  die Brandschutzordnung, insbesondere über das Verhalten bei einem Brand oder einer sonstigen Gefahrenlage in Verbindung mit dem Räumungskonzept.

(3) Im Einvernehmen mit der Brandschutzdienststelle sind Feuerwehrpläne anzufertigen und der örtlichen Feuerwehr zur Verfügung zu stellen.

#### § 28 Barrierefreie Stellplätze

Mindestens 3 Prozent der notwendigen Stellplätze, mindestens jedoch zwei Stellplätze, müssen barrierefrei sein.
Auf diese Stellplätze ist dauerhaft und leicht erkennbar hinzuweisen.

#### § 29 Zusätzliche Bauvorlagen

Die Bauvorlagen müssen zusätzliche Angaben enthalten über

1.  eine Berechnung der Flächen der Verkaufsräume und der Brandabschnitte,
2.  eine Berechnung der erforderlichen Breiten der Ausgänge aus den Geschossen ins Freie oder in notwendige Treppenräume,
3.  die Sprinkleranlagen, die sonstigen Feuerlöscheinrichtungen und die Feuerlöschgeräte,
4.  die Brandmeldeanlagen,
5.  die Alarmierungseinrichtungen,
6.  die Sicherheitsbeleuchtung und die Sicherheitsstromversorgung,
7.  die Rauchabzugsvorrichtungen und Rauchabzugsanlagen,
8.  die Rettungswege auf dem Grundstück und die Flächen für die Feuerwehr.

#### § 30 Weitergehende Anforderungen

An Lagerräume, deren lichte Höhe mehr als 9 Meter beträgt, können aus Gründen des Brandschutzes weitergehende Anforderungen gestellt werden.

#### § 31 Ordnungswidrigkeiten

Ordnungswidrig im Sinne des § 85 Absatz 1 Satz 1 Nummer 1 der Brandenburgischen Bauordnung handelt, wer vorsätzlich oder fahrlässig

1.  Rettungswege entgegen § 13 Absatz 5 einengt oder einengen lässt,
2.  Türen im Zuge von Rettungswegen entgegen § 15 Absatz 3 während der Betriebszeit abschließt oder abschließen lässt,
3.  in notwendigen Treppenräumen, in Treppenraumerweiterungen oder in notwendigen Fluren entgegen § 24 Absatz 2 Dekorationen anbringt oder anbringen lässt oder Gegenstände abstellt oder abstellen lässt,
4.  auf Ladenstraßen oder Hauptgängen entgegen § 24 Absatz 2 Gegenstände abstellt oder abstellen lässt,
5.  Rettungswege auf dem Grundstück oder Flächen für die Feuerwehr entgegen § 25 Absatz 3 nicht freihält oder freihalten lässt,
6.  als Betreiberin oder Betreiber oder deren oder dessen Vertretung entgegen § 26 Absatz 1 während der Betriebszeit nicht ständig anwesend ist,
7.  als Betreiberin oder Betreiber entgegen § 26 Absatz 2 die Brandschutzbeauftragte oder den Brandschutzbeauftragten und die Selbsthilfekräfte für den Brandschutz in der erforderlichen Anzahl nicht bestellt,
8.  als Betreiberin oder Betreiber entgegen § 26 Absatz 5 nicht sicherstellt, dass Selbsthilfekräfte für den Brandschutz in der erforderlichen Anzahl während der Betriebszeit anwesend sind.

#### § 32 Übergangsvorschriften

Auf die zum Zeitpunkt des Inkrafttretens der Verordnung bestehenden Verkaufsstätten ist der betrieblich-organisatorische Brandschutz innerhalb von zwei Jahren § 27 entsprechend anzupassen.

#### § 33 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Brandenburgische Verkaufsstätten-Bauverordnung vom 21.
Juli 1998 (GVBl.
II S. 524), die zuletzt durch Artikel 1 der Verordnung vom 23.
März 2005 (GVBl.
II S. 159) geändert worden ist, außer Kraft.

Potsdam, den 8.
November 2017

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#*) Diese Verordnung dient der Umsetzung der Richtlinie (EU) 2015/1535 des Europäischen Parlaments und des Rates vom 9.
September 2015 über ein Informationsverfahren auf dem Gebiet der technischen Vorschriften und der Vorschriften für die Dienste der Informationsgesellschaft (ABl. L 241 vom 17.09.2015, S.
1).