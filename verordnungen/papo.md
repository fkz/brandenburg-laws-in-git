## Verordnung über die Ausbildung und Prüfung für die Laufbahnen des Polizeivollzugsdienstes im Land Brandenburg (Polizeiausbildungs- und Prüfungsordnung - PAPO)

Auf Grund des § 26 Absatz 1 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S. 26), der durch Artikel 1 des Gesetzes vom 5.
Dezember 2013 (GVBl.
I Nr. 36) geändert worden ist, verordnet der Minister des Innern und für Kommunales im Einvernehmen mit der Ministerin der Finanzen und für Europa:

**Inhaltsübersicht**

### Abschnitt 1  
Gemeinsamer Teil

[§ 1 Geltungsbereich, Grundsätze](#1)

[§ 2 Prüfungsamt](#2)

[§ 3 Ausbildungsinhalte des Vorbereitungsdienstes](#3)

[§ 4 Dauer des Vorbereitungsdienstes](#4)

[§ 5 Pflichten](#5)

[§ 6 Ausbildungs-, Studien- und Prüfungsakten](#6)

[§ 7 Prüfungen](#7)

[§ 8 Bewertungsgrundsätze](#8)

[§ 9 Bewertungsverfahren](#9)

[§ 10 Bestehen, Nichtbestehen von Prüfungen](#10)

[§ 11 Überprüfung von Bewertungen](#11)

[§ 12 Wiederholung von Prüfungen](#12)

[§ 13 Prüfungstermine, Fristen](#13)

[§ 14 Prüfungsversäumnis](#14)

[§ 15 Unlauteres Prüfungsverhalten](#15)

[§ 16 Prüfungsvergünstigung](#16)

[§ 17 Prüfende](#17)

[§ 18 Anerkennung, Anrechnung](#18)

### Abschnitt 2  
Ausbildungsgang für den mittleren Polizeivollzugsdienst

[§ 19 Ausbildungsgang](#19)

[§ 20 Zwischenprüfungen](#20)

[§ 21 Laufbahnprüfung](#21)

[§ 22 Abschlussprüfung](#22)

[§ 23 Prüfungskommission](#23)

[§ 24 Schriftliche Abschlussprüfung](#24)

[§ 25 Mündliche Abschlussprüfung](#25)

[§ 26 Abschlussnote](#26)

### Abschnitt 3  
Studiengang für den gehobenen Polizeivollzugsdienst

[§ 27 Bachelorstudiengang](#27)

[§ 28 Modulprüfungen](#28)

[§ 29 Laufbahnprüfung](#29)

[§ 30 Bachelorthesis](#30)

[§ 31 Verteidigung der Bachelorthesis](#31)

[§ 32 Abschlussnote](#32)

[§ 33 Prüfungsurkunden](#33)

[§ 34 Anerkennung der Befähigung für die Laufbahn des mittleren Polizeivollzugsdienstes](#34)

### Abschnitt 4  
Schlussbestimmungen

[§ 35 Übergangsregelungen](#35)

[§ 36 Inkrafttreten, Außerkrafttreten](#36)

### Abschnitt 1  
Gemeinsamer Teil

#### § 1 Geltungsbereich, Grundsätze

(1) Diese Verordnung regelt die Ausbildung und die Prüfungen im Vorbereitungsdienst für die Laufbahnen des mittleren und gehobenen Polizeivollzugsdienstes an der Hochschule der Polizei des Landes Brandenburg (Hochschule).

(2) Die Hochschule regelt auf der Grundlage ihres Satzungsrechts nähere Vorschriften

1.  über die Ausbildung und die Prüfungen im Vorbereitungsdienst
    1.  für den mittleren Polizeivollzugsdienst in einer Lehr- und Prüfungsordnung,
    2.  für den gehobenen Polizeivollzugsdienst in einer Studien- und Prüfungsordnung,
2.  für das Berufspraktikum in jeweils einer Praktikumsordnung und
3.  zum Anerkennungs- und Anrechnungsverfahren in einer Anerkennungs- und Anrechnungsordnung.

#### § 2 Prüfungsamt

Für Entscheidungen der Hochschule in Prüfungsangelegenheiten und zur Koordination des Prüfungswesens ist an der Hochschule ein Prüfungsamt eingerichtet.
Die Leiterin oder der Leiter des Prüfungsamtes muss die Befähigung zum Richteramt besitzen.

#### § 3 Ausbildungsinhalte des Vorbereitungsdienstes

Ausbildungsinhalte sind insbesondere:

1.  rechtliche und sozialwissenschaftliche Grundlagen des polizeilichen Handelns,
2.  polizeiliche Einsatzbewältigung,
3.  polizeiliche Kriminalitätskontrolle, Kriminalistik,
4.  polizeiliche Verkehrssicherheitsarbeit,
5.  ausbildungsbegleitende Trainings.

#### § 4 Dauer des Vorbereitungsdienstes

(1) Der Vorbereitungsdienst dauert

1.  für den mittleren Polizeivollzugsdienst zwei Jahre und sechs Monate und
2.  für den gehobenen Polizeivollzugsdienst drei Jahre.

(2) Der Vorbereitungsdienst endet mit Bestehen der Laufbahnprüfung, frühestens jedoch mit Ablauf der vorgeschriebenen Dauer.
Er endet ebenfalls mit Ablauf des Tages, an dem das endgültige Nichtbestehen der Laufbahnprüfung (§ 10 Absatz 1) bekanntgegeben worden ist.
Diese Bekanntgabe erfolgt durch schriftlichen Bescheid des Prüfungsamts.

(3) Wird der Vorbereitungsdienst

1.  wegen Krankheit,
2.  durch Zeiten eines Beschäftigungsverbotes im Rahmen des Mutterschutzes,
3.  wegen der Inanspruchnahme von Elternzeit oder
4.  aus anderen, von den Auszubildenden oder Studierenden nicht zu vertretenden Gründen

in einem Maße unterbrochen, dass wesentliche Teile nicht wahrgenommen und dadurch der Vorbereitungsdienst nicht erfolgreich abgeschlossen werden kann, entscheidet die Hochschule, ob und in welchem Umfang der Vorbereitungsdienst verlängert wird.
Durch Wiederholung von Prüfungen kann sich der Vorbereitungsdienst ebenfalls verlängern.
Der Verlängerungszeitraum soll insgesamt 24 Monate nicht überschreiten.

(4) Zum Zweck der Spitzensportförderung kann der Vorbereitungsdienst für die Laufbahn des gehobenen Polizeivollzugsdienstes auf fünf Jahre ausgedehnt werden.

(5) Der Vorbereitungsdienst kann verkürzt werden, soweit für die Laufbahnbefähigung erforderliche Kenntnisse oder Fähigkeiten bereits durch eine anderweitige Laufbahnausbildung oder eine soldatenrechtliche Ausbildung erworben wurden.
Beamtenrechtliche Zugangsvoraussetzungen bleiben hiervon unberührt.
In den Fällen des Satzes 1 müssen zum Zeitpunkt des Abschlusses der Ausbildung im Ressort genügend freie und besetzbare Planstellen in der jeweiligen Laufbahn vorhanden sein.

#### § 5 Pflichten

(1) Unbeschadet der Pflichten aus dem Beamtenverhältnis sind die Auszubildenden und Studierenden verpflichtet, an allen vorgeschriebenen Lehrveranstaltungen, Prüfungen und sonstigen durch die Hochschule festgelegten dienstlichen Maßnahmen und Veranstaltungen teilzunehmen (Präsenzpflicht) sowie die in diesem Zusammenhang erteilten Aufgaben zu erfüllen.
Sie haben Lehrveranstaltungen angemessen vor- und nachzubereiten und angewiesenes Selbststudium eigenverantwortlich durchzuführen.

(2) Auszubildende und Studierende, die noch keine Fahrerlaubnis der Führerscheinklasse B besitzen, haben diese innerhalb von acht Monaten nach Einstellung in den Vorbereitungsdienst außerdienstlich zu erwerben.
In begründeten Ausnahmefällen kann die Frist auf Antrag angemessen verlängert werden.
Ohne Fahrerlaubnis ist die Ausbildung zur Dienstfahrberechtigung ausgeschlossen.
Mit Fristablauf nach Satz 1 und 2 gilt die Prüfung Dienstfahrberechtigung und damit die Laufbahnprüfung als endgültig nicht bestanden.
Dies gilt nicht, wenn die Frist aus Gründen ablief, die die Auszubildenden und Studierenden entsprechend § 4 Absatz 3 nicht zu vertreten haben.

(3) Die Auszubildenden und Studierenden haben ihren Erholungsurlaub in der festgelegten vorlesungsfreien oder unterrichtsfreien Zeit zu nehmen.
Über Ausnahmen entscheidet die Hochschule.

#### § 6 Ausbildungs-, Studien- und Prüfungsakten

(1) Die Hochschule führt für Auszubildende und Studierende jeweils eine Ausbildungs- oder Studienakte sowie jeweils eine Prüfungsakte.

(2) Auf schriftlichen Antrag ist Einsicht in eigene personenbezogene Akten zu gewähren.
In eigene schriftliche Prüfungsleistungen ist ab Bekanntgabe deren Bewertung Einsicht zu gewähren.
Diese sind von der Hochschule ab dem Ende des Vorbereitungsdienstes für zehn Jahre aufzubewahren.

#### § 7 Prüfungen

(1) Prüfungen sind Ausbildungsbestandteile, bei denen in der Ausbildung erworbene Kenntnisse und Fähigkeiten (Kompetenzen) festgestellt werden.
Bei der Bemessung der laufbahnbezogenen Anforderungen des Polizeivollzugsdienstes besteht keine Verpflichtung, sich auf ein unerlässliches Mindestmaß zu beschränken, sondern ist auf eine optimale Aufgabenerfüllung der öffentlichen Verwaltung abzustellen.

(2) Prüfungen können in schriftlicher, elektronischer, mündlicher, praktischer und in kombinierter Form durchgeführt werden.
Fächerübergreifende Prüfungen sind zulässig.
Prüfungsleistungen werden in deutscher Sprache erbracht, soweit sie nicht dazu dienen, fremdsprachliche Kompetenzen zu überprüfen.

(3) Bedienstete der zuständigen obersten Dienstbehörde und des Prüfungsamtes sind berechtigt, bei Prüfungen zugegen zu sein.
Bei dienstlichem Interesse kann das Prüfungsamt anderen Personen die Anwesenheit bei Prüfungen gestatten.

#### § 8 Bewertungsgrundsätze

(1) Prüfungsleistungen werden nach Maßgabe der Studien- und Prüfungsordnung sowie der Lehr- und Prüfungsordnung unter Verwendung eines Punktwertes bewertet.
Dieser ist jeweils auf zwei Dezimalstellen hinter dem Komma anzugeben.
Die dritte Stelle bleibt unberücksichtigt.

(2) Aufgrund der vorgenommenen Bewertung ist eine Prüfungsnote wie folgt zu vergeben:

sehr gut (Note 1) bei 14,00 bis 15,00 Punkten für eine Leistung, die den Anforderungen in besonderem Maße entspricht;

gut (Note 2) bei 11,00 bis 13,99 Punkten für eine Leistung, die den Anforderungen voll entspricht;

befriedigend (Note 3) bei 8,00 bis 10,99 Punkten für eine Leistung, die im Allgemeinen den Anforderungen entspricht;

ausreichend (Note 4) bei 5,00 bis 7,99 Punkten für eine Leistung, die zwar Mängel aufweist, aber im Ganzen den Anforderungen noch entspricht;

mangelhaft (Note 5) bei 2,00 bis 4,99 Punkten für eine Leistung, die den Anforderungen nicht entspricht, jedoch erkennen lässt, dass die notwendigen Grundkenntnisse vorhanden sind und die Mängel in absehbarer Zeit behoben werden können;

ungenügend (Note 6) bei 0 bis 1,99 Punkten für eine Leistung, die den Anforderungen nicht entspricht und bei der die Grundkenntnisse so lückenhaft sind, dass die Mängel in absehbarer Zeit nicht behoben werden können.

(3) Je nach Prüfungsform werden neben den erforderlichen fachlichen Kompetenzen insbesondere die Richtigkeit der Aussagen und deren praktische Verwertbarkeit, die Art und Folgerichtigkeit der Argumentation, die Stimmigkeit des Aufbaus, die Ausdrucksweise und die Beachtung der Regeln der deutschen Rechtschreibung berücksichtigt.
In den rechtswissenschaftlich geprägten Prüfungen wird darüber hinaus die Einhaltung der Rechtsmethodik bewertet.
In besonderem Maße ist zu berücksichtigen, dass es für den Polizeivollzugsdienst unerlässliche Kompetenzen gibt, deren Vorhandensein ohne Einschränkungen nachgewiesen werden muss.

(4) Prüfungsleistungen können abweichend von Absatz 1 auch ohne Punktwert mit „bestanden“ oder „nicht bestanden“ bewertet werden.

#### § 9 Bewertungsverfahren

(1) Mündliche Prüfungsleistungen werden in der Regel von einer Prüfungskommission aus zwei bis vier prüfenden Personen bewertet, wovon eine den Vorsitz innehat.
Der Vorsitz wird durch das Prüfungsamt bestimmt.
Sofern in der Prüfungskommission kein Einvernehmen über die Bewertung hergestellt werden kann, bestimmt der oder die Vorsitzende unter Berücksichtigung der in der Prüfungskommission vorgeschlagenen Bewertung den endgültigen Punktwert.

(2) Bei der Bewertung sonstiger Prüfungsleistungen durch mehrere Prüfende wird der Punktwert durch das arithmetische Mittel entsprechend der festgelegten Gewichtung gebildet.

(3) Bei Prüfungen aus fachübergreifenden Komplexen ist eine prozentuale Gewichtung der Fachkomplexe festzulegen.
Der Punktwert wird dann aus dem arithmetischen Mittel der komplexbezogenen Einzelleistungen entsprechend der festgelegten Gewichtung gebildet.
Wird die Prüfungsleistung in einem Fachkomplex mit 3 Punkten oder weniger bewertet, ist die Prüfung insgesamt nicht bestanden und kann nur mit maximal 4 Punkten bewertet werden.

(4) Im Falle der Wiederholung einer Prüfung wird für die endgültige Bewertung das arithmetische Mittel aus den Bewertungen der Erst- und Wiederholungsprüfung sowie gegebenenfalls des einmaligen Drittversuchs gebildet.
Bei Bestehen der Wiederholungsprüfung oder des einmaligen Drittversuchs werden jedoch mindestens fünf Punkte vergeben.

(5) Wenn eine schriftliche Wiederholungsprüfung mit weniger als fünf Punkten bewertet werden soll, erfolgt eine Zweitbewertung wobei Zweitbewertenden die Erstbewertung vorliegt.
Beträgt der Unterschied zwischen Erst- und Zweitbewertung nicht mehr als zwei Punkte, wird das arithmetische Mittel gebildet.
Bei mehr als zwei Punkten Unterschied wird eine Drittbewertung von einer weiteren prüfenden Person vorgenommen, wobei dieser die Vorbewertungen vorliegen.
Diese legt die Punktzahl unter Berücksichtigung der Vorbewertungen abschließend fest.

#### § 10 Bestehen, Nichtbestehen von Prüfungen

(1) Die Laufbahnprüfung im Vorbereitungsdienst für den mittleren Polizeivollzugsdienst besteht aus Zwischen- und Abschlussprüfungen und im Vorbereitungsdienst für den gehobenen Polizeivollzugsdienst aus Modulprüfungen.
Für Zwischen-, Abschluss- und Modulprüfungen können mehrere Prüfungen vorgeschrieben werden.
Unterschiedliche prozentuale Gewichtungen sind zulässig.
Der Punktwert für eine Zwischen-, Abschluss- oder Modulprüfung wird aus dem arithmetischen Mittel der Prüfungen entsprechend der festgelegten Gewichtung gebildet.
Eine Zwischen-, Abschluss- oder Modulprüfung ist bestanden, wenn alle dafür vorgeschriebenen Prüfungen bestanden wurden.

(2) Eine Prüfung ist bestanden, wenn die Prüfungsleistung unter Verwendung eines Punktwertes mit mindestens fünf Punkten bewertet wurde.
Bei weniger Punkten ist sie nicht bestanden.
Eine Prüfung ohne Verwendung eines Punktwertes ist bestanden, wenn die Prüfungsleistung den vorgeschriebenen Anforderungen entspricht.
Anderenfalls ist sie nicht bestanden.
Bei unentschuldigtem Prüfungs- oder Fristversäumnis und unlauterem Prüfungsverhalten ist eine Prüfung ebenfalls nicht bestanden.

(3) Eine Prüfung ist endgültig nicht bestanden, wenn die Prüfungsleistung in der letzten Wiederholungsprüfung mit weniger als fünf Punkten oder als nicht bestanden bewertet wurde.
Gleiches gilt für eine besonders schwerwiegende Täuschungshandlung.

(4) Mit endgültigem Nichtbestehen einer vorgeschriebenen Prüfung ist die entsprechende Zwischen-, Abschluss- oder Modulprüfung und damit die jeweilige Laufbahnprüfung endgültig nicht bestanden.

#### § 11 Überprüfung von Bewertungen

Die Bewertung einzelner Prüfungsleistungen ist kein Verwaltungsakt.
Sie wird auf Antrag überprüft.
Dieser muss begründete Einwände oder konkrete Hinweise auf Bewertungsfehler enthalten.

#### § 12 Wiederholung von Prüfungen

(1) Nicht bestandene Prüfungen sind einmal zu wiederholen.
Bestandene Prüfungen dürfen nicht wiederholt werden.

(2) Bei einer nicht bestandenen Wiederholungsprüfung wird auf schriftlichen Antrag einmal im Vorbereitungsdienst eine zweite Wiederholung der Prüfung ermöglicht (einmaliger Drittversuch).
Der Antrag ist innerhalb einer Woche nach Bekanntgabe des Prüfungsergebnisses zu stellen.

(3) Der einmalige Drittversuch wird nicht gewährt

1.  für das Praktikum,
2.  für die Anfertigung und Verteidigung der Bachelorthesis,
3.  für den schriftlichen und mündlichen Teil der Abschlussprüfung zum Erwerb der Laufbahnbefähigung für den mittleren Polizeivollzugsdienst,
4.  für die Wiederholung einer Prüfungsleistung, die zuvor wegen einer Täuschungshandlung oder Prüfungsstörung mit ungenügend benotet wurde.

(4) Ausnahmen von der Wiederholungsbeschränkung nach Absatz 1 Satz 1 können nach der Lehr- und Prüfungsordnung oder Studien- und Prüfungsordnung für Prüfungen geregelt werden, die ohne Punktwert bewertet werden.
Die Jahresfrist nach § 13 Absatz 2 bleibt davon unberührt.

#### § 13 Prüfungstermine, Fristen

(1) Das Prüfungsamt gibt Prüfungstermine spätestens zwei Wochen vor der jeweiligen Prüfung bekannt.

(2) Eine vorgeschriebene Prüfungsleistung, auch in der Wiederholung, muss innerhalb eines Jahres nach dem ersten Prüfungstermin erbracht werden, der für die zu prüfende Person galt.
Anderenfalls wird die Prüfung als endgültig nicht bestanden gewertet.
Dies gilt nicht, wenn die Jahresfrist entsprechend § 4 Absatz 3 Satz 1 aus Gründen ablief, die die zu prüfende Person nicht zu vertreten hat.

#### § 14 Prüfungsversäumnis

(1) Wird der Prüfungstermin nicht wahrgenommen, gilt die Prüfung als versäumt.
Gleiches gilt, wenn Prüfungsleistungen, die nicht unter Aufsicht zu fertigen sind, zum Abgabetermin nicht vorgelegt wurden.

(2) Eine unentschuldigt versäumte Prüfung ist nicht bestanden und die Prüfungsleistung wird bei Punktbewertung mit null Punkten bewertet.
Ein hinreichender Entschuldigungsgrund für eine versäumte Prüfung (Prüfungsrücktritt) liegt vor bei unverzüglichem Nachweis einer Prüfungsunfähigkeit oder eines sonstigen Grundes, der nicht von der zu prüfenden Person zu vertreten ist.

(3) Der Nachweis erfolgt

1.  durch Vorlage eines ärztlichen Attests beim Prüfungsamt, das die für die Beurteilung der Prüfungsunfähigkeit notwendigen medizinischen Befundtatsachen und Angaben über die konkrete Leistungsbeeinträchtigung enthält und das in der Regel nicht später als am Prüfungstag ausgestellt sein darf,
2.  auf Anordnung durch das Prüfungsamt durch Vorlage eines amtsärztlichen Attests oder
3.  im Ausnahmefall durch Darlegung und Beleg eines sonstigen Entschuldigungsgrundes.

(4) Bei einem Prüfungsabbruch wegen Prüfungsunfähigkeit oder eines sonstigen, von der zu prüfenden Person nicht zu vertretenden Grundes, ist die Prüfung in vollem Umfang nachzuholen.
Eine abgebrochene schriftliche Prüfung wird gewertet, wenn die zu prüfende Person dies unverzüglich, spätestens vor Bekanntgabe der Prüfungsbewertung beim Prüfungsamt beantragt.

(5) Hat sich die zu prüfende Person trotz Belehrung und konkreter Anhaltspunkte für ihre Prüfungsunfähigkeit einer Prüfung unterzogen, ist die deshalb abgebrochene Prüfung zu bewerten.

#### § 15 Unlauteres Prüfungsverhalten

(1) Unlauteres Prüfungsverhalten liegt vor

1.  bei Bearbeitungszeitüberschreitungen in schriftlichen Prüfungen, die unter Aufsicht erbracht werden müssen,
2.  wenn die zu prüfende Person eine selbstständige oder reguläre Prüfungsleistung vortäuscht, obwohl sie sich in Wahrheit unerlaubter Hilfe bedient, unerlaubte Hilfsmittel mit sich führt oder fremde geistige Leistungen in nicht nur unerheblichem Umfang ohne Kennzeichnung übernommen hat (Täuschung),
3.  bei mutwilligen Störungen des ordnungsgemäßen Verlaufs einer Prüfung trotz vorheriger Verwarnung oder
4.  bei unredlicher Einflussnahme auf Personen, die vom Prüfungsamt mit der Wahrnehmung von Prüfungsangelegenheiten beauftragt sind.

(2) Zu prüfende Personen haben an verhältnismäßigen Maßnahmen zum Auffinden unerlaubter Hilfsmittel mitzuwirken.
Kommen sie dieser Mitwirkungspflicht nicht nach, indem sie eine Überprüfung der Hilfsmittel verhindern, deren Herausgabe verweigern oder diese nach Beanstandung verändern, ist von einer Täuschung auszugehen.
Die Mitwirkungspflicht endet, wenn Hilfsmittel nicht mehr als Beweismittel benötigt werden.

(3) Unternimmt es eine zu prüfende Person, das Ergebnis einer Prüfung durch unlauteres Prüfungsverhalten zu beeinflussen, kann nach Grad der Verfehlung

1.  die Prüfungsaufsicht die betroffene Person
    1.  verwarnen oder
    2.  von der weiteren Prüfung ausschließen,
2.  das Prüfungsamt nach Anhörung der zu prüfenden Person entscheiden, dass
    1.  die betroffene Prüfungsleistung nicht bewertet wird und die Prüfung nachzuholen ist,
    2.  die Prüfung nicht bestanden ist und bei Punktbewertung mit null Punkten bewertet wird oder
    3.  eine Prüfung endgültig nicht bestanden ist.

(4) Kann eine Täuschung innerhalb einer Frist von fünf Jahren nach Abschluss der Prüfungsleistung nachgewiesen werden, prüft und entscheidet das Prüfungsamt gemäß Absatz 3.
Täuschungsbedingt unrichtige Prüfungsurkunden sind einzuziehen und gegebenenfalls neu auszustellen.

#### § 16 Prüfungsvergünstigung

(1) Zum Ausgleich von Nachteilen aufgrund erheblicher vorübergehender körperlicher Beeinträchtigungen, die nicht zur Prüfungsunfähigkeit führen, gewährt das Prüfungsamt auf schriftlichen Antrag individuelle Prüfungsvergünstigungen etwa durch

1.  Verlängerung der vorgesehenen Bearbeitungszeit um höchstens die Hälfte oder
2.  ersatzweise gleichwertige Prüfungsleistungen innerhalb derselben Prüfungsform.

Fachliche Anforderungen dürfen dabei nicht herabgesetzt werden.

(2) Dem Antrag auf Prüfungsvergünstigung ist ein ärztliches Attest beizufügen, aus dem sich Art und Umfang der körperlichen Beeinträchtigung und die daraus resultierende Leistungsbeeinträchtigung für eine konkrete Prüfung ergeben.
Bei Zweifeln über die körperliche Beeinträchtigung ist auf Anordnung des Prüfungsamtes ein amtsärztliches Attest vorzulegen.

#### § 17 Prüfende

(1) Als Prüfende können tätig werden:

1.  Angehörige des Lehr- und Trainingspersonals,
2.  Lehrbeauftragte,
3.  in der beruflichen Praxis erfahrene sowie persönlich und fachlich geeignete Personen.

Prüfende werden auf Entscheidung des Prüfungsamtes tätig.

(2) Prüfungsleistungen dürfen nur von Prüfenden bewertet werden, die selbst die durch die Laufbahnprüfung festzustellende oder eine mindestens gleichwertige Qualifikation besitzen.

(3) Prüfende sind in ihrer Prüfungstätigkeit unabhängig und an Weisungen nicht gebunden.
Sie sind in Prüfungsangelegenheiten gegenüber Dritten zur Verschwiegenheit verpflichtet.

#### § 18 Anerkennung, Anrechnung

(1) Studien- oder Prüfungsleistungen eines vorangegangenen Studiums sind bei Aufnahme oder Fortsetzung des Studiums anzuerkennen, soweit keine wesentlichen Unterschiede bestehen.

(2) Außerhalb des Hochschulwesens erworbene Kenntnisse und Fähigkeiten sind bis zu 50 Prozent auf vorgeschriebene Studien- oder Prüfungsleistungen anzurechnen, wenn sie nach Inhalt und Niveau dem Teil des Studiums gleichwertig sind, der ersetzt werden soll.

(3) Die Absätze 1 und 2 gelten für den Ausbildungsgang entsprechend.

### Abschnitt 2  
Ausbildungsgang für den mittleren Polizeivollzugsdienst

#### § 19 Ausbildungsgang

Die Ausbildung im Vorbereitungsdienst für die Laufbahn des mittleren Polizeivollzugsdienstes erfolgt einheitlich im Rahmen eines Ausbildungsgangs.
Dieser gliedert sich in fünf Ausbildungssemester mit einer Dauer von jeweils sechs Monaten.

#### § 20 Zwischenprüfungen

(1) Bis zum Ende des zweiten und vierten Ausbildungssemesters sowie im fünften Ausbildungssemester vor der Abschlussprüfung ist je eine Zwischenprüfung abzulegen.
Die erste Zwischenprüfung besteht aus vier Prüfungen, die zweite aus fünf Prüfungen und die dritte Zwischenprüfung aus zwei Prüfungen, die jeweils mit einem Punktwert bewertet werden.
Prüfungen in ausschließlich schriftlicher Form dauern 90 bis 240 Minuten, andere 15 bis 90 Minuten.
Im Rahmen von Zwischenprüfungen sind zudem Prüfungsleistungen abzulegen, die ohne Punktwert bewertet werden.

(2) Näheres regelt die Lehr- und Prüfungsordnung.

(3) Als Prüfung im Rahmen der zweiten Zwischenprüfung ist ein Praktikum nach Maßgabe der Praktikumsordnung zu absolvieren.

#### § 21 Laufbahnprüfung

Mit der Laufbahnprüfung wird festgestellt, ob die Auszubildenden für die Laufbahn des mittleren Polizeivollzugsdienstes befähigt sind.
Wurde jede Zwischenprüfung und die Abschlussprüfung bestanden, ist die Laufbahnprüfung insgesamt bestanden und die Ausbildung erfolgreich abgeschlossen.

#### § 22 Abschlussprüfung

Die Abschlussprüfung findet im fünften Ausbildungssemester statt.
Sie besteht aus einem schriftlichen und einem mündlichen Teil.
Zur Abschlussprüfung ist zugelassen, wer alle Zwischenprüfungen bestanden hat.

#### § 23 Prüfungskommission

(1) Zur Durchführung der Abschlussprüfung richtet das Prüfungsamt Prüfungskommissionen ein und bestellt deren Mitglieder und deren Ersatzmitglieder.
Jede Prüfungskommission besteht aus:

1.  einer Beamtin oder einem Beamten des höheren Dienstes oder einer vergleichbar beschäftigten Person, die oder der den Vorsitz innehat, und
2.  vier Prüfenden.

Vom Vorsitz wird die Stellvertretung innerhalb der Prüfungskommission geregelt.
Die Bestellung zum Mitglied einer Prüfungskommission darf die Dauer von fünf Jahren nicht überschreiten.
Wiederbestellungen sind zulässig.

(2) Prüfungskommissionen sollen sich aus den für die Ausbildung zuständigen Lehrkräften und Angehörigen der Polizeibehörde, der Polizeieinrichtungen sowie der zuständigen obersten Dienstbehörde zusammensetzen.
Mindestens drei Mitglieder sollen Beamtinnen oder Beamte des Polizeivollzugsdienstes sein.
Mit Einverständnis des Prüfungsamtes können Mitglieder in anderen Prüfungskommissionen tätig werden.

(3) Eine Prüfungskommission ist beschlussfähig, wenn mindestens vier Mitglieder anwesend sind.
Entscheidungen erfolgen mehrheitlich, wobei Stimmenthaltungen ausgeschlossen sind.
Bei Stimmengleichheit entscheidet der Vorsitz.

(4) Sitzungen der Prüfungskommission erfolgen unter Ausschluss der Öffentlichkeit.
Das Prüfungsamt kann mit Einverständnis der Prüfungskommission teilnehmen.

#### § 24 Schriftliche Abschlussprüfung

(1) Die schriftliche Abschlussprüfung umfasst zwei fächerübergreifende Klausuren mit einer Bearbeitungsdauer von jeweils 180 Minuten.
Sie dürfen keine Namensangaben oder andere direkte Hinweise auf die zu prüfende Person enthalten (Pseudonymisierung).
Die Klausuraufgaben bestimmt das Prüfungsamt auf Vorschlag des zuständigen Lehrpersonals.

(2) Die Prüfungsleistungen werden nacheinander von zwei Prüfenden der jeweiligen Prüfungskommission bewertet, wobei Zweitprüfenden die Erstbewertung vorliegt.
Weichen Erst- und Zweitbewertung um mehr als zwei Punkte voneinander ab, erfolgt eine Drittbewertung durch ein weiteres Mitglied der jeweiligen Prüfungskommission, welches die endgültige Bewertung vornimmt.

(3) Nachdem die Prüfungskommission die Prüfungsnote festgelegt hat, ist sie der zu prüfenden Person zuzuordnen, die die Prüfungsleistung erbracht hat.
Die Prüfungskommission kann die Bewertung anschließend nicht mehr ändern und teilt sie unverzüglich dem Prüfungsamt mit.

(4) Der Punktwert für den schriftlichen Teil der Abschlussprüfung ist der zu prüfenden Person spätestens zwei Wochen vor dem Termin der mündlichen Abschlussprüfung bekanntzugeben.

#### § 25 Mündliche Abschlussprüfung

(1) Der mündliche Teil der Abschlussprüfung ist eine fächerübergreifende Prüfung, die mit praktischen Prüfungsaufgaben kombiniert werden kann.
Sie soll in Gruppenprüfungen mit höchstens fünf zu prüfenden Personen erfolgen und in der Regel pro Person 30 Minuten dauern.

(2) Die Prüfungskommission legt die zu prüfenden Themengebiete fest, bewertet die Prüfungsleistungen und stellt abschließend den Punktwert fest.

(3) Die Person, die den Vorsitz der Prüfungskommission innehat, leitet den mündlichen Teil der Abschlussprüfung und wirkt auf die Wahrung von Sachlichkeit und Chancengleichheit hin.
Sie ist berechtigt, jederzeit in die mündliche Prüfung einzugreifen.

(4) Wurde der mündliche Teil der Abschlussprüfung bestanden, gibt die Person, die den Vorsitz der Prüfungskommission innehat, der Absolventin oder dem Absolventen den Punktwert und die Abschlussnote unverzüglich bekannt.

#### § 26 Abschlussnote

(1) Die Abschlussnote ergibt sich aus dem Gesamtpunktwert.
Dieser errechnet sich:

1.  aus dem arithmetischen Mittel der Punktwerte für Prüfungsleistungen in den Zwischenprüfungen, wovon der Mittelwert
    1.  der ersten Zwischenprüfung mit 20 Prozent,
    2.  der zweiten Zwischenprüfung mit 25 Prozent und
    3.  der dritten Zwischenprüfung mit 15 Prozent anzusetzen sind,
2.  aus den Punktwerten der schriftlichen und mündlichen Abschlussprüfung, die mit jeweils 20 Prozent anzusetzen sind.

(2) Über das Bestehen der Laufbahnprüfung stellt das Prüfungsamt ein Zeugnis aus.
Je eine Ausfertigung des Zeugnisses ist zur Prüfungsakte und zur Personalakte zu nehmen.

### Abschnitt 3  
Studiengang für den gehobenen Polizeivollzugsdienst

#### § 27 Bachelorstudiengang

(1) Die Ausbildung im Vorbereitungsdienst für die Laufbahn des gehobenen Polizeivollzugsdienstes erfolgt einheitlich im Rahmen eines akademischen Bachelorstudiengangs.
Dieser gliedert sich in zwölf Studienmodule und führt zu einem ersten berufsqualifizierenden Hochschulabschluss.

(2) Die Studienmodule setzen sich aus verschiedenen Lehr- und Lernformen zusammen, in denen neben berufsbezogenen praktischen Fähigkeiten auch die wissenschaftlichen Kenntnisse und Methoden vermittelt werden, die für eine differenzierte Analyse und Bewertung komplexer Sachverhalte erforderlich sind.

(3) Die Bachelorthesis einschließlich ihrer Verteidigung bildet ein gesondertes Modul.
Gleiches gilt für das Praktikum, das im Studienverlauf nach Maßgabe der Praktikumsordnung zu absolvieren ist.

#### § 28 Modulprüfungen

(1) Die Studienmodule werden jeweils mit einer Modulprüfung abgeschlossen.
Die Studien- und Prüfungsordnung berücksichtigt, dass

1.  mindestens drei Klausuren mit einer Bearbeitungszeit von 240 Minuten vorgesehen sind, wovon eine dieser Klausuren einen rechtswissenschaftlichen Schwerpunkt und eine juristische Fallbearbeitung aufweist und
2.  mindestens eine mündliche Prüfung aus dem Bereich der Rechts-, Verwaltungs- oder Sozialwissenschaften stammt.

(2) Für absolvierte Module und bestandene Modulprüfungen werden Leistungspunkte gemäß dem European Credit Transfer and Accumulation System (ECTS) vergeben.
Ein Leistungspunkt entspricht 30 Arbeitsstunden.
Das Studium umfasst insgesamt 180 Leistungspunkte.
Je Semester sind grundsätzlich 30 Leistungspunkte zugrundezulegen.

(3) Die Lernziele, die Inhalte und der Umfang der einzelnen Module sowie die Verteilung der Leistungspunkte auf die jeweiligen Modulprüfungen sind in der Studien- und Prüfungsordnung festgelegt.

#### § 29 Laufbahnprüfung

Mit der Laufbahnprüfung wird festgestellt, ob die Studierenden für die Laufbahn des gehobenen Polizeivollzugsdienstes befähigt sind.
Wurde jede Modulprüfung bestanden, ist die Laufbahnprüfung insgesamt bestanden und das Studium erfolgreich abgeschlossen.

#### § 30 Bachelorthesis

(1) Eine Bachelorthesis ist eine in einem vorgegebenen Zeitrahmen selbstständig, nicht unter Aufsicht anzufertigende wissenschaftliche Abschlussarbeit zu einem berufsbezogenen Thema.
Sie wird in einem Erst- und Zweitgutachten bewertet.

(2) Als Begutachtende bestimmt das Prüfungsamt nur Personen, die als Prüfende tätig werden können.
Mindestens eine der begutachtenden Personen einer Bachelorthesis muss dem Lehrpersonal der Hochschule angehören.
Mindestens eine der begutachtenden Personen muss über einen den Professorinnen oder Professoren oder den Lehrkräften des höheren Dienstes vergleichbaren Hochschulabschluss verfügen.
Erstbegutachtende betreuen die Studierenden bei der Anfertigung der Bachelorthesis.

(3) Studierende müssen das Thema ihrer Bachelorthesis mit der oder dem Erstbegutachtenden abstimmen.
Die Bachelorthesis kann von zwei Studierenden als Gemeinschaftsarbeit erstellt werden, wenn der als Prüfungsleistung zu bewertende Beitrag der einzelnen Studierenden aufgrund von Abschnitten, Seitenzahlen oder anderen Kriterien, deutlich unterscheidbar ist.
Der Themenvorschlag ist durch die Studierenden fristgerecht und unter Angabe der oder des Erst- und Zweitbegutachtenden einzureichen.
Die Zuweisung des Themas und der Begutachtenden obliegt dem Prüfungsamt unter Berücksichtigung wissenschaftlicher Empfehlungen des Dekanats.

(4) Die Bearbeitung der Bachelorthesis erfolgt in einem Zeitraum, der in der Studien- und Prüfungsordnung näher bestimmt ist.
Ein Wechsel von Thema oder Begutachtenden ist innerhalb der ersten vier Wochen zulässig, ohne dass sich dabei der Bearbeitungszeitraum verlängert.
Verzögert sich die Anfertigung der Bachelorthesis aus Gründen, die auch ein Prüfungsversäumnis nachweislich entschuldigen würden, kann das Prüfungsamt je nach Verzögerungsdauer den Bearbeitungszeitraum angemessen verlängern oder das Nachholen der Bachelorthesis mit neuem Thema anordnen.

(5) Eine Bachelorthesis kann nur begutachtet werden, wenn sie die schriftliche Versicherung der jeweiligenStudierenden enthält, dass

1.  die Arbeit selbstständig erstellt wurde und
2.  keine weiteren als die angegebenen und bei Zitaten kenntlich gemachten Quellen und Hilfsmittel benutzt wurden sowie
3.  keine Übereinstimmung mit einer anderen von ihnen angefertigten Arbeit besteht.

(6) Die endgültige Bewertung der Bachelorthesis ergibt sich aus dem arithmetischen Mittel der Erst- und Zweitbegutachtung.
Weichen Erst- und Zweitbegutachtung um mehr als zwei Punkte voneinander ab, bestimmt das Prüfungsamt in Abstimmung mit dem Dekanat eine weitere begutachtende Person.
Diese nimmt die Bewertung vor und legt die Punktzahl abschließend fest.

(7) Ist eine Wiederholung der Bachelorthesis erforderlich, beträgt der Bearbeitungszeitraum sechs Wochen.
Ein nachträglicher Themenwechsel ist ausgeschlossen.

#### § 31 Verteidigung der Bachelorthesis

(1) Die Verteidigung der Bachelorthesis besteht aus der Präsentation ihrer wesentlichen Inhalte und deren fachlicher Diskussion.
Darin soll die zu prüfende Person nachweisen, dass sie das von ihr bearbeitete Thema sicher beherrscht.
Sie erfolgt als 30- bis 45-minütige Einzelprüfung und setzt voraus, dass alle vorgeschriebenen Modulprüfungen und die Bachelorthesis bestanden wurden.

(2) Die Bachelorthesis wird als Prüfung in mündlicher Form vor einer Prüfungskommission verteidigt.
Die Prüfungskommission besteht aus drei zur Prüfung berechtigten Personen, wovon eine den Vorsitz innehat.
Eine der prüfenden soll eine der beiden begutachtenden Personen sein.
Ersatzmitglieder der Prüfungskommission bestimmt das Prüfungsamt in Abstimmung mit dem Dekanat entsprechend § 30 Absatz 2.

(3) Wurde die Verteidigung der Bachelorthesis bestanden, gibt die Person, die den Vorsitz der Prüfungskommission innehat, der Absolventin oder dem Absolventen den Punktwert und die Abschlussnote bekannt.

#### § 32 Abschlussnote

Die Abschlussnote ergibt sich aus dem Gesamtpunktwert.
Dieser errechnet sich:

1.  mit 20 Prozent aus dem Punktwert für die Bachelorthesis,
2.  mit zehn Prozent aus dem Punktwert für deren Verteidigung und
3.  mit 70 Prozent aus dem Mittel der Punktwerte der übrigen Modulprüfungen entsprechend der Leistungspunkte.

#### § 33 Prüfungsurkunden

(1) Über das Ergebnis der bestandenen Laufbahnprüfung stellt das Prüfungsamt ein Prüfungszeugnis und die Bachelorurkunde aus.
Der Bachelorurkunde wird ein Diploma Supplement in deutscher und englischer Sprache beigefügt, das nähere Angaben zum Studium, zur fachlichen Ausrichtung und Spezialisierung, zum absolvierten Praktikum und zu fakultativen Studienleistungen enthält.

(2) Das Prüfungszeugnis enthält:

1.  die erworbene Laufbahnbefähigung und die Abschlussbezeichnung „Bachelor of Arts (B.A.), Polizeivollzugsdienst/Police-Service“,
2.  eine Auflistung der absolvierten Module einschließlich der erzielten Noten, Punktwerte und Leistungspunkte,
3.  das Thema, den Punktwert und die Note der Bachelorthesis,
4.  die Note der Verteidigung der Bachelorthesis und
5.  die Einstufung der Abschlussnote des Studienjahrgangs nach der ECTS-Bewertungsskala:  
    
    A für die besten
    
    zehn Prozent,
    
    B für die nächsten
    
    25 Prozent,
    
    C für die nächsten
    
    30 Prozent,
    
    D für die nächsten
    
    25 Prozent,
    
    E für die nächsten
    
    zehn Prozent.
    

(3) Je eine Ausfertigung des Prüfungszeugnisses, der Bachelorurkunde und des Diploma Supplements ist zur Prüfungsakte sowie zur Personalakte zu nehmen.

#### § 34 Anerkennung der Befähigung für die Laufbahn des mittleren Polizeivollzugsdienstes

Wurden die Bachelorthesis oder deren Verteidigung endgültig nicht bestanden, kann die Befähigung für die Laufbahn des mittleren Polizeivollzugsdienstes nach erfolgreicher Teilnahme an der mündlichen Abschlussprüfung im Ausbildungsgang für die Laufbahn des mittleren Polizeivollzugsdienstes verliehen werden.
Der Antrag auf Teilnahme an dieser Prüfung muss innerhalb einer Woche nach Bekanntgabe über das endgültige Nichtbestehen der Laufbahnprüfung für den gehobenen Polizeivollzugsdienst von der betroffenen Person beim Prüfungsamt gestellt werden.
Die Prüfung findet innerhalb von sechs bis zwölf Wochen nach Antragstellung statt.
Der Vorbereitungsdienst verlängert sich hierdurch nicht.
Eine Wiederholung dieser Prüfung ist ausgeschlossen.
Das Prüfungsamt stellt eine Bescheinigung über die Befähigung für die Laufbahn des mittleren Polizeivollzugsdienstes ohne Angaben von Punktwerten und Noten aus.
Die §§ 23 und 25 finden entsprechende Anwendung.

### Abschnitt 4  
Schlussbestimmungen

#### § 35 Übergangsregelungen

Auszubildende und Studierende, die vor Inkrafttreten dieser Verordnung ihren Vorbereitungsdienst begonnen haben, schließen diesen innerhalb der regulären Dauer des Vorbereitungsdienstes nach bisherigem Recht ab.

#### § 36 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig treten die Ausbildungs- und Prüfungsordnung mittlerer Polizeivollzugsdienst vom 25.
April 2007 (GVBl.
II S.
118), die zuletzt durch Artikel 2 der Verordnung vom 20.
März 2015 (GVBl.
II Nr.
16 S.
2) geändert worden ist, und die Ausbildungs- und Prüfungsordnung gehobener Polizeivollzugsdienst vom 24.
August 2012 (GVBl. II Nr. 78) außer Kraft.

Potsdam, den 2.
September 2020

Der Minister des Innern und für Kommunales

Michael Stübgen