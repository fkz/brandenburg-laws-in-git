## Verordnung zur Übertragung beamtenrechtlicher Zuständigkeiten im Geschäftsbereich des Ministeriums für Wissenschaft, Forschung und Kultur (Beamtenzuständigkeitsverordnung MWFK - BZVMWFK)

Auf Grund des § 4 Absatz 1 Satz 2 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S. 26) in Verbindung mit § 1 Absatz 3 Satz 1 der Ernennungsverordnung vom 1.
August 2004 (GVBl.
II S. 742) und auf Grund des § 6 Absatz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28) geändert worden ist, in Verbindung mit

*   § 32 Absatz 1 Satz 1, § 38 Satz 2, § 50 Satz 1 zweiter Halbsatz, § 54 Absatz 1, § 56 Absatz 1 Satz 5, § 57 Absatz 1 Satz 2, § 66 Absatz 4 zweiter Halbsatz, § 69 Absatz 5 Satz 1, § 84 Satz 2, § 85 Absatz 2 Satz 1, § 86 Absatz 1 Satz 3, § 87 Satz 4, § 88 Satz 5, § 89 Satz 3, § 92 Absatz 2 zweiter Halbsatz und § 103 Absatz 2 des Landesbeamtengesetzes, von denen § 50 Satz 1 durch Artikel 1 Nummer 18 des Gesetzes vom 5.
    Dezember 2013 (GVBl.
    I Nr. 36 S. 9) geändert worden ist,
*   § 63 Absatz 3 Satz 3 des Landesbeamtengesetzes, der durch Artikel 4 Nummer 2 des Gesetzes vom 20.
    November 2013 (GVBl.
    I Nr. 32 S. 123) geändert worden ist, und § 9 Absatz 3 der Trennungsgeldverordnung in der Fassung der Bekanntmachung vom 29. Juni 1999 (BGBl. I S. 1533) im Einvernehmen mit dem Minister der Finanzen,
*   § 89 Absatz 1 des Brandenburgischen Beamtenversorgungsgesetzes vom 20. November 2013 (GVBl.
    I Nr. 32), der durch Artikel 3 des Gesetzes vom 10.
    Juli 2017 (GVBl.
    I Nr. 14) geändert worden ist, im Einvernehmen mit dem Minister der Finanzen,
*   § 6 Satz 2 der Dienstjubiläumsverordnung vom 18.
    Dezember 2014 (BGBl. I S. 2267) in Verbindung mit § 64 Satz 1 des Landesbeamtengesetzes, der durch das Gesetz vom 11. März 2010 (GVBl.
    I Nr. 13) neu gefasst worden ist,
*   § 4 Absatz 4, § 9 Absatz 4 Satz 3, § 22 Absatz 4 Satz 1 und § 39 Absatz 1 zweiter Halbsatz der Laufbahnverordnung vom 16.
    September 2009 (GVBl.
    II S. 622),
*   § 13 Absatz 2 Satz 3 und § 58 Absatz 1 des Brandenburgischen Besoldungsgesetzes vom 20. November 2013 (GVBl.
    I Nr. 32),
*   § 54 Absatz 3 Satz 2 des Beamtenstatusgesetzes vom 17.
    Juni 2008 (BGBl. I S. 1010) in Verbindung mit § 103 Absatz 2 des Landesbeamtengesetzes

verordnet die Ministerin für Wissenschaft, Forschung und Kultur:

#### § 1 Anwendungsbereich

Diese Verordnung gilt für den Geschäftsbereich des Ministeriums für Wissenschaft, Forschung und Kultur, des Brandenburgischen Landeshauptarchivs und des Brandenburgischen Landesamtes für Denkmalpflege und Archäologisches Landesmuseum.
Diese Verordnung gilt nicht für die Hochschulen des Landes Brandenburg.

#### § 2 Ernennung, Entlassung und Versetzung in den Ruhestand

(1) Die Ausübung der Befugnis zur Ernennung der Beamtinnen und Beamten in den Laufbahnen des mittleren und des gehobenen Dienstes und die Ausübung der Befugnis zur Entlassung und Versetzung in den Ruhestand für die Beamtinnen und Beamten des mittleren und des gehobenen Dienstes werden auf das Brandenburgische Landeshauptarchiv und das Brandenburgische Landesamt für Denkmalpflege und Archäologisches Landesmuseum für ihren jeweiligen Geschäftsbereich übertragen.

(2) Die nach Absatz 1 übertragene Befugnis wird im Namen des Landes Brandenburg ausgeübt.

#### § 3 Weitere Zuständigkeiten

Das Brandenburgische Landeshauptarchiv und das Brandenburgische Landesamt für Denkmalpflege und Archäologisches Landesmuseum sind für ihren jeweiligen Geschäftsbereich zuständig für:

1.  die Entscheidung über das Verbot der Führung der Dienstgeschäfte gemäß § 54 des Landesbeamtengesetzes,
2.  die Entscheidung über die Versagung der Aussagegenehmigung gemäß § 56 des Landesbeamtengesetzes, wobei die Versagung der Aussagegenehmigung der vorherigen Zustimmung des Ministeriums für Wissenschaft, Forschung und Kultur bedarf,
3.  die Zustimmung zur Annahme von Belohnungen und Geschenken und sonstigen Vorteilen gemäß § 57 des Landesbeamtengesetzes,
4.  die Erlaubnis zur Führung der Amtsbezeichnung mit dem Zusatz „außer Dienst“ („a.
    D.“) gemäß § 69 des Landesbeamtengesetzes,
5.  die Nebentätigkeitsangelegenheiten und Untersagung von Tätigkeiten nach Beendigung des Beamtenverhältnisses gemäß den §§ 84 bis 89 und 92 des Landesbeamtengesetzes, soweit nicht die Entscheidung gemäß § 87 Satz 1 des Landesbeamtengesetzes der obersten Dienstbehörde vorbehalten ist,
6.  die Befugnis zur Gewährung und Versagung der Dienstjubiläumszuwendungen gemäß § 6 der Dienstjubiläumsverordnung,
7.  die Entscheidungen über den Umfang der Stellenausschreibungen und ihrer Bekanntmachung gemäß § 4 Absatz 4 der Laufbahnverordnung,
8.  die Anerkennung von Urlaub auf die Probezeit gemäß § 9 Absatz 4 Satz 3 der Laufbahnverordnung,
9.  die Zulassung zur Einführung in die nächsthöhere Laufbahn gemäß § 22 Absatz 4 Satz 1 sowie die Entscheidung über den Erwerb der Laufbahnbefähigung gemäß § 39 Absatz 1 zweiter Halbsatz der Laufbahnverordnung,
10.  die Kürzung der Anwärterbezüge gemäß § 58 Absatz 1 des Brandenburgischen Besoldungsgesetzes.

#### § 4 Übertragung von Aufgaben auf die Zentrale Bezügestelle des Landes Brandenburg

(1) Die Zuständigkeit des Ministeriums für Wissenschaft, Forschung und Kultur, des Brandenburgischen Landeshauptarchivs sowie des Brandenburgischen Landesamtes für Denkmalpflege und Archäologisches Landesmuseum für:

1.  die Berechnung und Zahlung von Umzugskosten gemäß § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes,
2.  die Unfallfürsorgeangelegenheiten gemäß Abschnitt 2 Unterabschnitt 3 des Brandenburgischen Beamtenversorgungsgesetzes,
3.  den Ersatz von Sachschäden und die Geltendmachung von übergegangenen Schadensersatzansprüchen gegen Dritte gemäß § 66 Absatz 1 und 3 des Landesbeamtengesetzes und
4.  die Zustimmung zu einem Verzicht auf die Rückforderung von Bezügen aus Billigkeitsgründen gemäß § 13 Absatz 2 Satz 3 des Brandenburgischen Besoldungsgesetzes

wird auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

(2) Die Zuständigkeit des Ministeriums für Wissenschaft, Forschung und Kultur und des Brandenburgischen Landeshauptarchivs für:

1.  die Berechnung und Zahlung von Reisekosten gemäß § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes und
2.  die Bewilligung, Berechnung und Zahlung von Trennungsgeld gemäß § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes

wird auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

#### § 5 Befugnis zum Erlass von Widerspruchsbescheiden

Die Zuständigkeit für den Erlass von Widerspruchsbescheiden in beamtenrechtlichen Angelegenheiten der Beamtinnen und Beamten, Ruhestandsbeamtinnen und Ruhestandsbeamten und früheren Beamtinnen und Beamten im Geschäftsbereich des Ministeriums für Wissenschaft, Forschung und Kultur sowie deren Hinterbliebenen wird auf die Zentrale Bezügestelle des Landes Brandenburg übertragen, soweit diese die mit dem Widerspruch angegriffene Maßnahme getroffen oder unterlassen hat.

#### § 6 Vertretung bei Klagen aus dem Beamtenverhältnis

(1) Die Vertretung des Landes vor den Gerichten der Verwaltungsgerichtsbarkeit gemäß § 103 des Landesbeamtengesetzes wird auf die in § 5 genannte Stelle übertragen, soweit diese über den Widerspruch zu entscheiden hat.

(2) Im Rahmen der Übertragung der Zuständigkeit nach § 5 wird die Zentrale Bezügestelle des Landes Brandenburg ermächtigt, das Ministerium für Wissenschaft, Forschung und Kultur, das Brandenburgische Landeshauptarchiv sowie das Brandenburgische Landesamt für Denkmalpflege und Archäologisches Landesmuseum in verwaltungs- und arbeitsgerichtlichen Streitigkeiten zu vertreten.

(3) Die Absätze 1 und 2 gelten entsprechend für Verfahren des einstweiligen Rechtsschutzes.

#### § 7 Übergangsvorschriften

Für Anträge, die vor dem Inkrafttreten dieser Verordnung eingegangen sind und über die noch nicht abschließend entschieden worden ist, soweit die Bearbeitung nicht bereits der Zentralen Bezügestelle des Landes Brandenburg übertragen war, verbleibt es bei den bisherigen Zuständigkeiten.
Dies gilt auch für die Vertretung in zu diesem Zeitpunkt bereits anhängigen Rechtsstreitigkeiten.

#### § 8 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig treten

1.  die Reisekosten-Trennungsgeldzuständigkeitsübertragungsverordnung MWFK vom 22. August 2012 (GVBl.
    II Nr. 74), die durch die Verordnung vom 8. Mai 2013 (GVBl. II Nr. 36) geändert worden ist,
2.  die Widerspruchszuständigkeitsverordnung MWFK vom 9.
    Januar 1998 (GVBl.
    II S. 82), die durch die Verordnung vom 17.
    August 2004 (GVBl.
    II S. 684) geändert worden ist, und
3.  die Beamtenzuständigkeitsverordnung MWFK vom 19.
    August 1998 (GVBl.
    II S. 567)

außer Kraft.

Potsdam, den 6.
März 2018

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Dr.
Martina Münch