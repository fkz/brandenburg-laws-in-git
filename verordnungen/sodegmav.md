## Verordnung über den Mehrbelastungsausgleich für den Vollzug des Sozialdienstleister-Einsatzgesetzes (Sozialdienstleister-Einsatzgesetz-Mehrbelastungsausgleichsverordnung - SodEGMAV)

Auf Grund des § 5 Satz 1 des Sozialdienstleister-Einsatzgesetzes vom 27.
März 2020 (BGBl. I S. 575, 578) in Verbindung mit § 4 Absatz 2 Satz 1 und 2 der Sozialdienstleister-Einsatzgesetz-Zuständigkeitsverordnung vom 24.
April 2020 (GVBl.
II Nr. 26) verordnet die Ministerin für Soziales, Gesundheit, Integration und Verbraucherschutz im Einvernehmen mit der Ministerin für Bildung, Jugend und Sport und der Ministerin der Finanzen und für Europa:

#### § 1 Mehrbelastungsausgleich

(1) Die nach § 1 Nummer 1 und 2 der Sozialdienstleister-Einsatzgesetz-Zuständigkeitsverordnung zuständigen Behörden erhalten zum Ausgleich der durch die Aufgabenwahrnehmung nach dem Sozialdienstleister-Einsatzgesetz resultierenden Mehrbelastungen eine Verwaltungskostenpauschale für jeden bearbeiteten Antrag.
Die nach § 1 Nummer 3 der Sozialdienstleister-Einsatzgesetz-Zuständigkeitsverordnung zuständigen Behörden erhalten zum Ausgleich der durch die Aufgabenwahrnehmung nach dem Sozialdienstleister-Einsatzgesetz resultierenden Mehrbelastungen eine Verwaltungskostenpauschale für jeden bearbeiteten Antrag und einen pauschalen Ersatz für die zu leistenden Beratungs-, Koordinierungs- und Verwaltungsleistungen.

(2) Die Verwaltungskostenpauschale wird für die nach § 1 Nummer 1 und 2 der Sozialdienstleister-Einsatzgesetz-Zuständigkeitsverordnung zuständigen Behörden auf einen Betrag von 650 Euro für jeden bewilligten Antrag und auf einen Betrag von 433 Euro für jeden bestandkräftig abgelehnten Antrag festgesetzt.
Der Betrag von 433 Euro gilt auch bei Antragsrücknahme oder sonstiger Erledigung, sofern mit der Bearbeitung des Antrags bereits begonnen wurde.
Wird ein zuvor zurückgenommener oder durch sonstige Erledigung abgerechneter Antrag erneut – in angepasster Form – gestellt und bewilligt, wird die Verwaltungskostenpauschale in Höhe von 433 Euro zu 50 Prozent angerechnet.

(3) Die Verwaltungskostenpauschale wird für die nach § 1 Nummer 3 der Sozialdienstleister-Einsatzgesetz-Zuständigkeitsverordnung zuständigen Behörden auf einen Betrag von 200 Euro für jeden bewilligten Antrag und einen pauschalen Ersatz von 1 500 Euro monatlich für die Zeit der tatsächlichen Anwendung des Sozialdienstleister-Einsatzgesetzes festgesetzt.

#### § 2 Zuständigkeit, Verfahren

(1) Zuständige Behörde für die Festsetzung und Auszahlung der Verwaltungskostenpauschale an die örtlichen Träger der Eingliederungshilfe und die örtlichen Träger der Sozialhilfe ist das Landesamt für Soziales und Versorgung.

(2) Zuständige Behörde für die Festsetzung und Auszahlung der Verwaltungskostenpauschale an die örtlichen Träger der Kinder- und Jugendhilfe ist das Ministerium für Bildung, Jugend und Sport.

(3) Die Verwaltungskostenpauschale ist mit zahlenmäßiger Angabe der bewilligten, abgelehnten und zurückgenommenen oder in sonstiger Weise erledigten Anträge als Gesamtbetrag jährlich bis zum 30. April des dem Abrechnungszeitraum folgenden Kalenderjahres gegenüber der zuständigen Behörde in Textform geltend zu machen.
Von der zuständigen Behörde können Unterlagen zur Berechnung des Gesamtbetrages eingesehen oder angefordert werden.

#### § 3 Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 25.
April 2020 in Kraft.

Potsdam, den 14.
Juni 2021

Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

Ursula Nonnemacher