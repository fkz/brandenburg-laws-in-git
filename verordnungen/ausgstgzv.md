## Verordnung über Zuständigkeiten nach dem Ausgangsstoffgesetz (Ausgangsstoffgesetz-Zuständigkeitsverordnung - AusgStGZV)

Auf Grund des § 6 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), der durch Artikel 2 Nummer 8 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28 S.
2) geändert worden ist, und des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl.
I S.
602) verordnet die Landesregierung:

#### § 1 Zuständigkeiten

(1) Das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit ist Inspektionsbehörde nach § 5 des Ausgangsstoffgesetzes und darüber hinaus zuständig für die Durchführung der sonstigen Aufgaben nach dem Ausgangsstoffgesetz, soweit nicht nach Absatz 2 das Polizeipräsidium zuständig ist.
Das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit ist auch zuständig für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 14 des Ausgangsstoffgesetzes.

(2) Das Polizeipräsidium ist Kontaktstelle nach § 3 des Ausgangsstoffgesetzes.
Es führt auch die Schulungsmaßnahmen nach § 11 Absatz 1 Nummer 1 des Ausgangsstoffgesetzes durch und übermittelt dem Bundeskriminalamt jährlich Informationen nach § 12 Satz 2 Nummer 1 des Ausgangsstoffgesetzes.

#### § 2 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 19.
Januar 2022

Die Landesregierung des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

Ursula Nonnemacher

Der Minister des Innern und für Kommunales

Michael Stübgen