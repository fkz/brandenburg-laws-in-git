## Sechste Verordnung über die Aufhebung von Wasserschutzgebieten

Auf Grund des § 15 Absatz 3 Satz 4 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Aufhebung von Wasserschutzgebieten

(1) Folgende, auf der Grundlage des § 28 des Landeskulturgesetzes vom 14.
Mai 1970 (GBl.
I Nr. 12 S. 67), des § 28 des Wassergesetzes vom 17.
April 1963 (GBl.
I Nr. 5 S. 77) und der Verordnung über die Festlegung von Schutzgebieten für die Wasserentnahme aus dem Grund- und Oberflächenwasser zur Trinkwassergewinnung vom 11.
Juli 1974 (GBl.
I Nr. 37 S. 349) festgesetzte und nach § 46 des Wassergesetzes vom 2.
Juli 1982 (GBl.
I Nr. 26 S. 467) aufrechterhaltene Wasserschutzgebiete werden aufgehoben:

1.  die mit Beschluss Nummer 125/72 vom 2.
    November 1972 des Kreistages Neuruppin für das Wasserwerk I Trenckmannstraße Neuruppin festgesetzten Schutzzonen I und II des Trinkwasserschutzgebietes,
2.  die mit Beschluss Nummer 77-21/73 vom 9.
    November 1973 des Kreistages Angermünde für die Wasserwerke Berkholz Gutshof, Berkholz Karlsberg (nach Pinnow), Groß Pinnow, Hohenfelde, Hohenselchow Am Ortsausgang nach Casekow, Hohenselchow (Mitte), Hohenselchow (nach Groß Pinnow), Jamikow, Leopoldsthal, Pinnow und Stützkow (Neu-Galow) festgesetzten Trinkwasserschutzgebiete,
3.  die mit Beschluss Nummer 77-21/73 vom 9.
    November 1973 und Beschluss Nummer 22-05/80 vom 17.
    März 1980 des Kreistages Angermünde für die Wasserwerke Biesendahlshof und Hohenreinkendorf festgesetzten Trinkwasserschutzgebiete,
4.  das mit Beschluss Nummer 5-20/74 vom 3.
    Januar 1974 des Kreistages Königs Wusterhausen für das Wasserwerk Oderin festgesetzte Trinkwasserschutzgebiet,
5.  die mit Beschluss Nummer 102/74 vom 21.
    Januar 1974 des Kreistages Wittstock für die Wasserwerke LPG Blesendorf und Kreisbetrieb für Landtechnik Wittstock (für Tetschendorf) festgesetzten Trinkwasserschutzgebiete,
6.  das mit Beschluss Nummer 0134 vom 25.
    September 1974 des Rates der Stadt Brandenburg und Beschluss Nummer 0030/74 vom 23.
    Oktober 1974 der Stadtverordnetenversammlung Brandenburg für das Not- und Spitzenwasserwerk Kirchmöser festgesetzte Trinkwasserschutzgebiet,
7.  das mit Beschluss Nummer 027 vom 13.
    Mai 1976 des Kreistages Kyritz für das Wasserwerk Pflegeheim Ganz festgesetzte Trinkwasserschutzgebiet,
8.  das mit Beschluss Nummer 78/77 vom 14.
    Januar 1977 des Kreistages Cottbus-Land für das Wasserwerk Leuthen festgesetzte Trinkwasserschutzgebiet,
9.  das mit Beschluss Nummer 125-26/78 vom 27.
    November 1978 des Kreistages Perleberg für das Wasserwerk Bendelin festgesetzte Trinkwasserschutzgebiet,
10.  das mit Beschluss Nummer 153-27/79 vom 11.
    Januar 1979 des Kreistages Forst (Lausitz) für das Wasserwerk Schweinemastanlage Jocksdorf festgesetzte Trinkwasserschutzgebiet,
11.  das mit Beschluss Nummer 29/1980 vom 17.
    Januar 1980 des Kreistages Spremberg für das Wasserwerk Graustein festgesetzte Trinkwasserschutzgebiet,
12.  die mit Beschluss Nummer 22-05/80 vom 17.
    März 1980 des Kreistages Angermünde für die Wasserwerke Berkholz, Bruchhagen und Zützen festgesetzten Trinkwasserschutzgebiete,
13.  das mit Beschluss Nummer 0019-07/80 vom 30.
    Juli 1980 des Kreistages Pritzwalk für das Wasserwerk Rosenwinkel festgesetzte Trinkwasserschutzgebiet,
14.  das mit Beschluss Nummer 0055 vom 7.
    Mai 1981 des Kreistages Nauen für das Wasserwerk Brunnen Autobahnmeisterei Bredow festgesetzte Trinkwasserschutzgebiet,
15.  das mit Beschluss Nummer 68-14/81 vom 23.
    September 1981 des Kreistages Belzig für das Wasserwerk Belzig festgesetzte Trinkwasserschutzgebiet.

(2) Folgende, auf der Grundlage des § 29 des Wassergesetzes vom 2.
Juli 1982 und der Dritten Durchführungsverordnung zum Wassergesetz – Schutzgebiete und Vorbehaltsgebiete – vom 2. Juli 1982 (GBl.
I Nr. 26 S. 487) festgesetzte Trinkwasserschutzgebiete werden aufgehoben:

1.  die mit Beschluss Nummer 252/82 vom 21.
    Dezember 1982 des Kreistages Neuruppin für die Wasserwerke Braunsberg und Wulkow festgesetzten Trinkwasserschutzgebiete,
2.  das mit Beschluss Nummer 157/83 vom 7.
    September 1983 des Kreistages Strausberg für das Wasserwerk Garzau, Magistrat von Berlin festgesetzte Trinkwasserschutzgebiet,
3.  das mit Beschluss Nummer 09/36/85 vom 21.
    August 1985 des Kreistages Beeskow für das Wasserwerk Pfaffendorf festgesetzte Trinkwasserschutzgebiet,
4.  das mit Beschluss Nummer 176/20/88 vom 8.
    Juni 1988 des Kreistages Fürstenwalde (Spree) für das Wasserwerk Tempelberg – Rechtsträger LPG „Eintracht“ festgesetzte Trinkwasserschutzgebiet.

#### § 2 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 24.
Mai 2016

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger