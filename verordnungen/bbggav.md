## Verordnung über die Gutachterausschüsse für Grundstückswerte des Landes Brandenburg (Brandenburgische Gutachterausschussverordnung - BbgGAV)

Auf Grund des § 199 Absatz 2 des Baugesetzbuches in der Fassung der Bekanntmachung vom 23. September 2004 (BGBl.
I S. 2414), der durch Artikel 4 des Gesetzes vom 24. Dezember 2008 (BGBl.
I S. 3018) geändert worden ist, verordnet die Landesregierung:

Inhaltsübersicht
----------------

### Teil 1  
Gutachterausschüsse

#### Abschnitt 1  
Bildung und Zusammensetzung der Gutachterausschüsse

[§ 1    Bildung der Gutachterausschüsse](#1)  
[§ 2    Bestellung der Gutachter](#2)  
[§ 3    Sachkunde, Unabhängigkeit und Pflichten der Gutachter](#3)  
[§ 4    Aufsicht](#4)  
[§ 5    Beendigung der Amtszeit](#5)

#### Abschnitt 2  
Aufgaben der Gutachterausschüsse und ihrer Geschäftsstellen

[§ 6    Aufgaben des Gutachterausschusses](#6)  
[§ 7    Aufgaben des Vorsitzenden](#7)  
[§ 8    Übertragung von Befugnissen](#8)  
[§ 9    Kaufpreissammlung](#9)  
[§ 10  Geheimhaltung](#10)  
[§ 11  Auskünfte aus der Kaufpreissammlung](#11)  
[§ 12  Bodenrichtwerte](#12)  
[§ 13  Sonstige für die Wertermittlung erforderliche Daten](#13)  
[§ 14  Grundstücksmarktbericht](#14)  
[§ 15  Zusammenarbeit der Gutachterausschüsse](#15)  
[§ 16  Geschäftsstelle des Gutachterausschusses](#16)

#### Abschnitt 3  
Verfahren der Gutachterausschüsse

[§ 17  Besetzung des Gutachterausschusses im Einzelfall](#17)  
[§ 18  Verfahrensgrundsätze](#18)  
[§ 19  Örtliche Zuständigkeit](#19)  
[§ 20  Entschädigung der Gutachter](#20)  
[§ 21  Kosten des Gutachterausschusses](#21)

### Teil 2  
Oberer Gutachterausschuss

[§ 22  Bildung des Oberen Gutachterausschusses](#22)  
[§ 23  Aufgaben des Oberen Gutachterausschusses](#23)  
[§ 24  Kosten des Oberen Gutachterausschusses](#24)  
[§ 25  Geschäftsstelle des Oberen Gutachterausschusses](#25)  
[§ 26  Anwendung der Vorschriften über die Gutachterausschüsse](#26)

### Teil 3  
Übergangs- und Schlussvorschriften

[§ 27  Gleichstellungsbestimmung](#27)  
[§ 28  Übergangsbestimmung](#28)  
[§ 29  Inkrafttreten, Außerkrafttreten](#29)

Teil 1  
Gutachterausschüsse
----------------------------

### Abschnitt 1  
Bildung und Zusammensetzung der Gutachterausschüsse

#### § 1   
Bildung der Gutachterausschüsse

(1) Für die Bereiche der Landkreise und der kreisfreien Städte wird je ein selbstständiger und unabhängiger Gutachterausschuss gebildet.
Abweichend von Satz 1 kann das für Inneres zuständige Ministerium für benachbarte Gebietskörperschaften auf deren Antrag hin einen gemeinsamen Gutachterausschuss bilden.
Dem Antrag ist die Vereinbarung nach § 16 Absatz 2 beizufügen.

(2) Die Bildung eines gemeinsamen Gutachterausschusses, seine Bezeichnung und der Sitz seiner Geschäftsstelle sind von dem für Inneres zuständigen Ministerium im Amtsblatt für Brandenburg bekannt zu machen.
Die bestehenden Gutachterausschüsse sind aufgelöst, sobald der neue Gutachterausschuss gebildet ist.

(3) Für die Auflösung eines gemeinsamen Gutachterausschusses gelten Absatz 1 Satz 2 und Absatz 2 sinngemäß.

(4) Der Gutachterausschuss führt die Bezeichnung „Gutachterausschuss für Grundstückswerte im Landkreis/in der Stadt ...“.
Bei der Bildung eines gemeinsamen Gutachterausschusses wird die Bezeichnung durch das für Inneres zuständige Ministerium nach Anhörung der beteiligten Gebietskörperschaften festgelegt.

(5) Der Gutachterausschuss besteht aus einem Vorsitzenden, seinen Stellvertretern und ehrenamtlichen weiteren Gutachtern.
Der Gutachterausschuss führt das Landessiegel.

#### § 2  
Bestellung der Gutachter

(1) Das für Inneres zuständige Ministerium bestellt nach Anhörung der Gebietskörperschaft oder der Gebietskörperschaften, für deren Bereich der Gutachterausschuss zu bilden ist, den Vorsitzenden, seine Stellvertreter und die ehrenamtlichen weiteren Gutachter zu Mitgliedern des Gutachterausschusses.
Die Bestellung kann wiederholt werden.
Die Amtszeit des Gutachterausschusses beträgt fünf Jahre.
Ist während der Amtszeit eine Neubestellung notwendig, so erfolgt sie für den Rest der Amtszeit.

(2) Zum Vorsitzenden soll ein Bediensteter der Katasterbehörde oder der Katasterbehörden bestellt werden, für deren Bereich der Gutachterausschuss gebildet ist.

(3) Auf Vorschlag des für Finanzen zuständigen Ministeriums werden je ein Bediensteter der zuständigen Finanzämter mit Zuständigkeit für die steuerliche Bewertung des Grundbesitzes als ehrenamtlicher und je ein weiterer als stellvertretender ehrenamtlicher Gutachter bestellt.
Diese Gutachter werden insbesondere für die Ermittlung der Bodenrichtwerte und der in § 193 Absatz 5 Satz 2 des Baugesetzbuches genannten sonstigen für die Wertermittlung erforderlichen Daten bestellt.
Eine Anhörung nach Absatz 1 entfällt.

(4) Der Vorsitzende und die Stellvertreter dürfen nicht der Vertretung oder einem Ausschuss der Gebietskörperschaft oder der Gebietskörperschaften angehören, für deren Bereich der Gutachterausschuss gebildet ist.
Sie dürfen hauptamtlich nicht mit der Verwaltung der Grundstücke der vorgenannten Gebietskörperschaften befasst sein.

(5) Die ehrenamtlichen Gutachter des Gutachterausschusses dürfen nicht der Verwaltung der Gebietskörperschaft oder der Gebietskörperschaften angehören, für deren Bereich der Gutachterausschuss gebildet ist.

(6) Zum Mitglied des Gutachterausschusses darf nicht bestellt werden, wer nach § 21 Absatz 1 Nummer 1 und 2 sowie Absatz 2 der Verwaltungsgerichtsordnung vom Amt eines ehrenamtlichen Verwaltungsrichters ausgeschlossen ist.

(7) Die ehrenamtlichen Gutachter sind bei ihrer Bestellung zur Einhaltung ihrer Pflichten nach § 3 Absatz 2 bis 4 und zur Geheimhaltung nach § 10 aktenkundig zu verpflichten.

#### § 3  
Sachkunde, Unabhängigkeit und Pflichten der Gutachter

(1) Die Gutachter müssen die für die Wertermittlung von Grundstücken oder entsprechende Wertermittlungen erforderliche Sachkunde besitzen und sollen in diesen Wertermittlungen erfahren sein; unter ihnen sollen sich Personen mit besonderer Sachkunde für die verschiedenen Grundstücksarten und Gebietsteile im Zuständigkeitsbereich des Gutachterausschusses befinden.

(2) Die Gutachter haben ihre Gutachten unparteiisch und nach bestem Wissen und Gewissen abzugeben.
Sie sind an Weisungen nicht gebunden.

(3) Die Gutachter sind verpflichtet, die durch ihre Tätigkeit zu ihrer Kenntnis gelangenden persönlichen und wirtschaftlichen Verhältnisse der Beteiligten, auch nach Beendigung ihrer Tätigkeit als Mitglied des Gutachterausschusses, geheim zu halten.

(4) Für den Ausschluss von Gutachtern gilt § 1 Absatz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg in Verbindung mit § 20 des Verwaltungsverfahrensgesetzes entsprechend.

#### § 4  
Aufsicht

(1) Das für Inneres zuständige Ministerium (Rechtsaufsichtsbehörde) führt die Rechtsaufsicht über die Gutachterausschüsse.

(2) Die Rechtsaufsichtsbehörde prüft die Einhaltung der Rechtsvorschriften bei der Aufgabenwahrnehmung der Gutachterausschüsse, die Einhaltung der den Gutachtern auferlegten Pflichten sowie die Geschäftsführung der Gutachterausschüsse und deren Geschäftsstellen.

#### § 5  
Beendigung der Amtszeit

(1) Das für Inneres zuständige Ministerium hat einen Gutachter abzuberufen, wenn die Voraussetzungen für die Bestellung entfallen sind.

(2) Das für Inneres zuständige Ministerium kann einen Gutachter abberufen, wenn

1.  er gegen die Vorschriften des § 3 Absatz 3 oder Absatz 4 verstoßen hat oder
    
2.  ein wichtiger Grund im Sinne von § 1 Absatz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg in Verbindung mit § 86 des Verwaltungsverfahrensgesetzes vorliegt.
    

(3) Ein nach § 2 Absatz 3 bestellter Gutachter ist auch abzuberufen, wenn er nicht mehr bei dem zuständigen  
Finanzamt für die steuerliche Bewertung des Grundbesitzes zuständig ist.

(4) Die Amtszeit eines Gutachters endet ohne Abberufung mit Vollendung des 70.
Lebensjahres oder durch Niederlegung des Amtes.
Die Niederlegung ist unter Vorlage der Bestellungsurkunde schriftlich zu erklären.

### Abschnitt 2  
Aufgaben der Gutachterausschüsse und ihrer Geschäftsstellen

#### § 6  
Aufgaben des Gutachterausschusses

(1) Neben den in anderen Rechtsvorschriften aufgeführten Aufgaben werden dem Gutachterausschuss die in den Absätzen 2 und 3 genannten weiteren Aufgaben übertragen.

(2) Der Gutachterausschuss hat Gutachten zu erstatten über die Höhe anderer Vermögensvor- und -nachteile bei städtebaulichen oder sonstigen Maßnahmen im Zusammenhang mit

1.  dem Grunderwerb oder mit Bodenordnungsmaßnahmen,
    
2.  der Aufhebung oder Beendigung von Miet- oder Pachtverhältnissen.
    

(3) Der Gutachterausschuss hat auf Antrag der Enteignungsbehörde Zustandsfeststellungen für ein Grundstück oder einen Grundstücksteil einschließlich seiner Bestandteile bei vorzeitiger Besitzeinweisung nach § 116 Absatz 5 des Baugesetzbuches und nach § 8 Absatz 4 des Enteignungsgesetzes des Landes Brandenburg durchzuführen.

(4) Der Gutachterausschuss kann

1.  Gutachten erstatten über Miet- oder Pachtwerte,
    
2.  Miet- oder Pachtwertübersichten erstellen,
    
3.  bei der Erstellung des Mietspiegels mitwirken.
    

(5) Antragsberechtigt für Gutachten nach den Absätzen 2 und 4 Nummer 1 sind die Berechtigten nach § 193  
Absatz 1 des Baugesetzbuches.
In den Fällen nach Absatz 2 Nummer 2 und Absatz 4 Nummer 1 ist außerdem der jeweilige Mieter oder Pächter antragsberechtigt.

#### § 7  
Aufgaben des Vorsitzenden

Dem Vorsitzenden des Gutachterausschusses obliegen neben den anderen ihm übertragenen Aufgaben insbesondere

1.  die Vertretung des Gutachterausschusses nach außen,
    
2.  die Erteilung fachlicher Weisungen an die Geschäftsstelle,
    
3.  die Festlegung und Leitung der Sitzungen,
    
4.  die Entscheidung über die Besetzung des Gutachterausschusses im Einzelfall,
    
5.  die Verpflichtung der ehrenamtlichen Mitglieder nach § 2 Absatz 7,
    
6.  die Erläuterung von Gutachten bei Behörden und Gerichten; er kann hierzu ein am Gutachten beteiligtes Mitglied des Gutachterausschusses als Vertreter bestimmen,
    
7.  die Entscheidung über die Erteilung von Auskünften aus der Kaufpreissammlung nach § 11.
    

#### § 8  
Übertragung von Befugnissen

Der Gutachterausschuss kann durch Beschluss mit der Mehrheit seiner Mitglieder auf den Vorsitzenden übertragen

1.  die Wahrnehmung der Befugnisse des Gutachterausschusses nach § 197 Absatz 1 des Baugesetzbuches,
    
2.  die Befugnis zur Erteilung von Weisungen
    
    1.  zur Führung und Auswertung der Kaufpreissammlung nach § 9,
        
    2.  zur Vorbereitung der Ermittlung
        
        aa) von Bodenrichtwerten nach § 12 sowie
        
        bb) sonstiger für die Wertermittlung erforderlicher Daten nach § 13,
        
3.  die Entscheidung über die Annahme von Anträgen nach § 6 Absatz 4.
    

#### § 9  
Kaufpreissammlung

(1) Die Kaufpreissammlung wird bei der Geschäftsstelle des Gutachterausschusses eingerichtet und geführt.
Für die Beschaffung, Ersatzbeschaffung und Unterhaltung der dafür notwendigen Auswerte- und Informationssysteme ist der Landesbetrieb Landesvermessung und Geobasisinformation Brandenburg zuständig.

(2) Die Verträge und sonstigen Rechtshandlungen nach § 195 Absatz 1 des Baugesetzbuches sind nach Weisung des Gutachterausschusses unverzüglich auszuwerten und in die Kaufpreissammlung aufzunehmen.
Die Verträge, Beschlüsse sowie ergänzenden Angaben und Unterlagen, die personenbezogene Daten enthalten, sind nach Eingabe der Daten in die Kaufpreissammlung zu vernichten.

(3) Die Flurbereinigungs- und Flurneuordnungsbehörden übermitteln dem Gutachterausschuss laufend die zur Führung und Auswertung der Kaufpreissammlung erforderlichen Daten.
Dazu gehören insbesondere Daten aus Landverzichtserklärungen (§ 52 des Flurbereinigungsgesetzes) und aus Planvereinbarungen.

(4) Die Kaufpreissammlung ist als georeferenzierter Nachweis so anzulegen, dass die Daten nach sachlichen und zeitlichen Gesichtspunkten eingeordnet werden können und eine Auswertung jederzeit möglich ist.
Die Kaufpreissammlung ist zeitnah zu führen.

(5) Die Kaufpreissammlung wird landeseinheitlich eingerichtet und automatisiert geführt.
Zu den in den Absätzen 2 und 3 genannten Vorgängen werden, nach Objektgruppen getrennt, Ordnungsmerkmale, Vertragsmerkmale und die verkehrswertbeeinflussenden Grundstücksmerkmale erfasst und in die Kaufpreissammlung übernommen.
Die Entgelte sind auf die für die Objektgruppen geeigneten Vergleichsmaßstäbe zu beziehen.

(6) Objektgruppen sind Gruppen von Grundstücken, für die nach den örtlichen Marktverhältnissen Teilmärkte bestehen.

(7) Ordnungsmerkmale sind insbesondere die entsprechenden Angaben des Liegenschaftskatasters und des Grundbuches, die Lagebezeichnung und die Koordinaten im amtlichen geodätischen Bezugssystem.

(8) Vertragsmerkmale sind insbesondere die Vertragsart oder der sonstige Grund des Rechtsüberganges, die Gruppen der Vertragsparteien, das Entgelt, die Zahlungsbedingungen sowie Besonderheiten der Preisvereinbarung.
Der Name und das Aktenzeichen der beurkundenden Stelle können ebenfalls in die Kaufpreissammlung übernommen werden.

(9) Bestandteil der Kaufpreissammlung sind auch weitere Daten, die Grundlage für die Wertermittlung im Einzelfall und für die Ableitung sonstiger für die Wertermittlung erforderlicher Daten sein können.
Hierzu gehören insbesondere Mieten, Pachten, Nutzungsentgelte, Fotografien der Grundstücke und Bewirtschaftungskosten.

#### § 10  
Geheimhaltung

(1) Die Kaufpreissammlung ist einschließlich der Verträge, Beschlüsse und Unterlagen geheim zu halten.
Sie darf nur von den Mitgliedern des Gutachterausschusses und den Bediensteten der Geschäftsstelle zur Erfüllung ihrer Aufgaben eingesehen werden.

(2) Absatz 1 gilt für erstattete Gutachten entsprechend.

(3) Daten aus der Kaufpreissammlung dürfen in Gutachten angegeben werden, soweit es zu deren Begründung erforderlich ist, personenbezogene Daten jedoch nur, wenn kein Grund zu der Annahme besteht, dass dadurch schutzwürdige Geheimhaltungsinteressen der Betroffenen beeinträchtigt werden.

#### § 11  
Auskünfte aus der Kaufpreissammlung

(1) Anonymisierte Auskünfte aus der Kaufpreissammlung sind zu erteilen, wenn ein berechtigtes Interesse dargelegt wird.

(2) Grundstücksbezogene Auskünfte aus der Kaufpreissammlung erhalten

1.  öffentliche Stellen nach § 2 Absatz 1 des Brandenburgischen Datenschutzgesetzes,
    
2.  öffentlich bestellte und vereidigte Sachverständige für Grundstückswertermittlung und
    
3.  Sachverständige für Grundstückswertermittlung mit einer Zertifizierung nach DIN EN ISO/IEC 17024, Aus­gabe November 2012,
    

wenn die Auskunft zur Wertermittlung erforderlich ist.

(3) Die übermittelten Daten dürfen ausschließlich für den Zweck verwendet werden, zu dessen Erfüllung sie übermittelt werden.
Sie dürfen dabei anonymisiert weitergegeben werden, sofern der Empfänger ein berechtigtes Interesse hat.

#### § 12  
Bodenrichtwerte

(1) Der Gutachterausschuss hat bis zum 15.
Februar jedes Kalenderjahres Bodenrichtwerte zum Stichtag 31.
Dezember des vorangegangenen Kalenderjahres flächendeckend zu ermitteln und zu beschließen.

(2) Die Bodenrichtwerte sind auf der Grundlage der Geobasisdaten landeseinheitlich zu erfassen und darzustellen sowie in geeigneten Formen bereitzustellen.
Zur Bereitstellung der Bodenrichtwerte in digitaler Form ist durch den Landesbetrieb Landesvermessung und Geobasisinformation Brandenburg ein Geoportal einzurichten und zu betreiben.
Am anderweitigen Vertrieb von Produkten der Gutachterausschüsse wirkt der Landesbetrieb Landesvermessung und Geobasisinformation Brandenburg nach Maßgabe des für Inneres zuständigen Ministeriums mit.

(3) Die Bodenrichtwerte sollen bis zum auf die Ermittlung folgenden 31.
März veröffentlicht werden.
Die Bodenrichtwerte nach § 196 Absatz 1 Satz 7 des Baugesetzbuches können zu einem abweichenden Zeitpunkt oder von der sie beantragenden Behörde veröffentlicht werden.
Die Art der Veröffentlichung und der Hinweis auf das Recht, von der Geschäftsstelle Auskunft über die Bodenrichtwerte zu verlangen (§ 196 Absatz 3 des Baugesetzbuches), sind ortsüblich bekannt zu machen.

#### § 13  
Sonstige für die Wertermittlung erforderliche Daten

(1) Auf der Grundlage der ausgewerteten Kaufpreise und weiterer Daten nach § 9 Absatz 9 hat der Gutachterausschuss sonstige für die Wertermittlung erforderliche Daten nach der jeweiligen Lage auf dem Grundstücksmarkt zu ermitteln.
Dabei sind die Ermittlungsmethode und Art und Umfang der zugrunde liegenden Ausgangsdaten darzustellen.

(2) Zur Ermittlung sonstiger für die Wertermittlung erforderlicher Daten können auch geeignete Daten aus den Zuständigkeitsbereichen anderer Gutachterausschüsse herangezogen werden.

#### § 14  
Grundstücksmarktbericht

Der Gutachterausschuss soll Feststellungen über den Grundstücksmarkt, insbesondere über Umsatz- und Preisentwicklung, und die sonstigen für die Wertermittlung erforderlichen Daten in einem Grundstücksmarktbericht zusammenfassen und diesen bis zum 15.
Mai jeden Jahres veröffentlichen.
Der Grundstücksmarktbericht ist dem Oberen Gutachterausschuss und der Rechtsaufsichtsbehörde vorzulegen.

#### § 15   
Zusammenarbeit der Gutachterausschüsse

(1) Die Gutachterausschüsse tauschen bei Bedarf Bodenrichtwerte, Grundstücksmarktberichte und sonstige für die Wertermittlung erforderliche Daten untereinander aus.
Die Daten der Kaufpreissammlung sind den anderen Gutachterausschüssen zugänglich zu machen, soweit es zur Erfüllung ihrer Aufgaben erforderlich ist.

(2) Die Gutachterausschüsse sind verpflichtet, dem Oberen Gutachterausschuss die Daten der Kaufpreissammlung zugänglich zu machen und für die Wahrnehmung seiner Aufgaben alle in Betracht kommenden Daten und Unterlagen aufzubereiten und vorzulegen.

#### § 16  
Geschäftsstelle des Gutachterausschusses

(1) Die Geschäftsstelle des Gutachterausschusses wird bei der Katasterbehörde eingerichtet, für deren Bereich der Gutachterausschuss gebildet ist oder bei gemeinsamen Gutachterausschüssen nach § 1 Absatz 1 Satz 2 bei einer der Katasterbehörden, für deren Bereich der Gutachterausschuss gebildet ist.
Die Gebietskörperschaft stellt für die Geschäftsstelle des Gutachterausschusses fachlich geeignetes Personal und Sachmittel in erforderlichem Umfang zur Verfügung.
Die Regelungen in § 21 bleiben unberührt.

(2) Für einen gemeinsamen Gutachterausschuss nach § 1 Absatz 1 Satz 2 ist durch die beteiligten Gebietskörperschaften die Einrichtung einer gemeinsamen Geschäftsstelle zu vereinbaren.
In dieser Vereinbarung sind insbesondere

1.  der Sitz der Geschäftsstelle,
    
2.  die Ausstattung der Geschäftsstelle mit Personal und Sachmitteln und
    
3.  die Aufteilung der Kosten auf die beteiligten Gebietskörperschaften zu regeln.
    

Die Vereinbarung soll für eine Dauer von mindestens zehn Jahren gelten.
Änderungen der Vereinbarung sind dem für Inneres zuständigen Ministerium anzuzeigen.
Änderungen des Sitzes der Geschäftsstelle bedürfen der Bekanntmachung nach § 1 Absatz 2 Satz 1.

(3) Die Geschäftsstelle des Gutachterausschusses arbeitet nach Weisung des Gutachterausschusses oder dessen Vorsitzenden.
Ihr obliegen insbesondere

1.  die Einrichtung und Führung der Kaufpreissammlung,
    
2.  die vorbereitenden Arbeiten für die Ermittlung der Bodenrichtwerte,
    
3.  die vorbereitenden Arbeiten für die Ermittlung und Fortschreibung der sonstigen für die Wertermittlung erforderlichen Daten,
    
4.  die Vorbereitung des Grundstücksmarktberichtes,
    
5.  die Vorbereitung der Gutachten,
    
6.  die Erteilung von Auskünften aus der Kaufpreissammlung,
    
7.  die Erteilung von Auskünften über vereinbarte Nutzungsentgelte,
    
8.  die Erteilung von Auskünften über Bodenrichtwerte und
    
9.  die Erledigung der Geschäfte der laufenden Verwaltung des Gutachterausschusses.
    

(4) Die Geschäftsstelle kann nach Weisung des Vorsitzenden auf Antrag von Behörden zur Erfüllung von deren Aufgaben fachliche Äußerungen über Grundstückswerte erteilen.

### Abschnitt 3  
Verfahren der Gutachterausschüsse

#### § 17  
Besetzung des Gutachterausschusses im Einzelfall

Der Gutachterausschuss wird bei der Erstattung von Gutachten und bei der Zustandsfeststellung nach § 6 Absatz 3 in der Besetzung mit dem Vorsitzenden oder einem Stellvertreter und zwei ehrenamtlichen weiteren Gutachtern tätig.
In besonderen Fällen kann der Vorsitzende weitere Gutachter sowie Sachverständige hinzuziehen.
Bei der Ermittlung von Bodenrichtwerten und der sonstigen für die Wertermittlung erforderlichen Daten, bei der Erstellung des Grundstücksmarktberichtes sowie bei der Erstellung von Miet- oder Pachtwertübersichten wird der Gutachterausschuss in der Besetzung mit dem Vorsitzenden oder einem Stellvertreter und mindestens vier ehrenamtlichen weiteren Gutachtern tätig.
Bei der Ermittlung von Bodenrichtwerten und der in § 193 Absatz 5 Satz 2 des Baugesetzbuches genannten sonstigen für die Wertermittlung erforderlichen Daten ist zusätzlich der nach § 2 Absatz 3 bestellte ehrenamtliche Gutachter hinzuzuziehen.

#### § 18  
Verfahrensgrundsätze

(1) Der Gutachterausschuss berät und beschließt in nicht öffentlicher Sitzung.
Er beschließt mit Stimmenmehrheit; abweichende Auffassungen von Mitgliedern des Gutachterausschusses sind auf Verlangen aktenkundig zu machen.
Die Beschlüsse sind von den mitwirkenden Gutachtern zu unterzeichnen.

(2) Der Erstattung von Gutachten soll eine Ortsbesichtigung durch den Gutachterausschuss vorausgehen.
Die Gutachten sind zu begründen.
Die Sachverhalte, auf denen die Wertermittlung beruht, sind darzulegen.

#### § 19  
Örtliche Zuständigkeit

Örtlich zuständig ist der Gutachterausschuss, in dessen Bereich der Gegenstand der Wertermittlung liegt.
Liegt der Gegenstand der Wertermittlung im Bereich mehrerer Gutachterausschüsse, so ist der Gutachterausschuss zuständig, in dessen Bereich der größte Teil liegt.

#### § 20  
Entschädigung der Gutachter

Die Gutachter der Gutachterausschüsse erhalten auf Antrag eine Entschädigung für ihre Leistungen (Leistungsentschädigung), Fahrtkostenersatz, Entschädigung für Aufwand sowie Ersatz für sonstige und für besondere Aufwendungen nach Maßgabe der von dem für Inneres zuständigen Ministerium erlassenen Richtlinien.
Die im öffentlichen Dienst beschäftigten Gutachter erhalten keine Leistungsentschädigung, soweit sie die Gutachtertätigkeit als dienstliche Angelegenheit wahrnehmen.
Die Entschädigung wird von der Geschäftsstelle festgesetzt.

#### § 21  
Kosten des Gutachterausschusses

(1) Die Kosten des Gutachterausschusses und seiner Geschäftsstelle trägt die Gebietskörperschaft oder tragen die Gebietskörperschaften, für deren Bereich der Gutachterausschuss gebildet ist.

(2) Für die Tätigkeit des Gutachterausschusses werden Gebühren und Auslagen nach dem Gebührengesetz für das Land Brandenburg in Verbindung mit der Gutachterausschuss-Gebührenordnung erhoben.
Die Gebühren und Auslagen stehen dem jeweiligen Kostenträger nach Absatz 1 zu.
Sie werden von der Geschäftsstelle festgesetzt.

(3) Kosten des Gutachterausschusses und seiner Geschäftsstelle, die nicht von den erhobenen Gebühren und Auslagen gedeckt werden, werden durch das Land erstattet.

Teil 2  
Oberer Gutachterausschuss
----------------------------------

#### § 22   
Bildung des Oberen Gutachterausschusses

(1) Für den Bereich des Landes Brandenburg wird ein Oberer Gutachterausschuss gebildet.
Er führt die Bezeichnung „Oberer Gutachterausschuss für Grundstückswerte im Land Brandenburg“.

(2) Die Mitglieder des Oberen Gutachterausschusses werden von dem für Inneres zuständigen Ministerium bestellt.
Sie sollen Mitglieder eines Gutachterausschusses sein.
Als Mitglieder des Oberen Gutachterausschusses können auch Bedienstete des Landes bestellt werden.

(3) Ein Gutachter des Oberen Gutachterausschusses ist von der Mitwirkung an einem Obergutachten ausgeschlossen, wenn er an dem Gutachten des örtlich zuständigen Gutachterausschusses mitgewirkt hat.

(4) Der Obere Gutachterausschuss kann durch Beschluss mit der Mehrheit seiner Mitglieder Weisungsbefugnisse und die Wahrnehmung der Befugnisse nach § 197 Absatz 1 des Baugesetzbuches auf den Vorsitzenden übertragen.

#### § 23  
Aufgaben des Oberen Gutachterausschusses

(1) Neben der Erstattung von Obergutachten auf Antrag eines Gerichtes hat der Obere Gutachterausschuss auf Antrag einer Behörde in einem gesetzlichen Verfahren ein Obergutachten zu erstatten, wenn das Gutachten eines Gutachterausschusses vorliegt.

(2) Der Obere Gutachterausschuss erarbeitet den Grundstücksmarktbericht für den Bereich des Landes Brandenburg.

(3) Der Obere Gutachterausschuss soll

1.  Daten von Objekten, die bei den Gutachterausschüssen nur vereinzelt vorhanden sind, sammeln, auswerten und bereitstellen,
    
2.  im Einvernehmen mit den Vorsitzenden der Gutachterausschüsse verbindliche Standards für die überregionale Ermittlung der sonstigen für die Wertermittlung erforderlichen Daten festlegen,
    
3.  zu besonderen Problemen der Wertermittlung Empfehlungen an die Gutachterausschüsse abgeben.
    

#### § 24  
Kosten des Oberen Gutachterausschusses

Die Kosten des Oberen Gutachterausschusses und seiner Geschäftsstelle trägt das Land.
§ 21 Absatz 2 gilt entsprechend.

#### § 25  
Geschäftsstelle des Oberen Gutachterausschusses

(1) Die Geschäftsstelle des Oberen Gutachterausschusses wird beim Landesbetrieb Landesvermessung und Geo­basisinformation Brandenburg eingerichtet.

(2) Der Geschäftsstelle des Oberen Gutachterausschusses obliegen die Geschäfte der laufenden Verwaltung des Oberen Gutachterausschusses.
Nach Weisung des Oberen Gutachterausschusses bereitet sie die Obergutachten, den Grundstücksmarktbericht und die Empfehlungen zu besonderen Problemen der Wertermittlung vor, und sammelt Daten von Objekten, die bei den Gutachterausschüssen nur vereinzelt vorhanden sind, wertet sie aus und stellt sie bereit.

#### § 26  
Anwendung der Vorschriften über die Gutachterausschüsse

Soweit sich aus den §§ 22 bis 25 nichts anderes ergibt, sind die Vorschriften des Teils 1 entsprechend anzuwenden.

Teil 3  
Übergangs- und Schlussvorschriften
-------------------------------------------

#### § 27  
Gleichstellungsbestimmung

Die in dieser Verordnung verwendeten Funktions-, Status- und anderen Bezeichnungen gelten für Frauen und Männer.

#### § 28  
Übergangsbestimmung

Auf bei Inkrafttreten dieser Verordnung bereits bestellte Gutachter findet § 5 Absatz 4 Satz 1 erste Alternative keine Anwendung.

#### § 29  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Gutachterausschussverordnung vom 29.
Februar 2000 (GVBl.
II S. 61), die zuletzt durch Artikel 39 des Gesetzes vom 23.
September 2008 (GVBl.
I S. 202, 211) geändert worden ist, außer Kraft.

Potsdam, den 12.
Mai 2010

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Matthias Platzeck

Der Minister des Innern

Rainer Speer