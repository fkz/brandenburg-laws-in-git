## Verordnung über Zulagen für Lehrkräfte mit besonderen Funktionen im Land Brandenburg (Brandenburgische Lehrkräftezulagenverordnung - BbgLZV)

Auf Grund des § 42 des Brandenburgischen Besoldungsgesetzes vom 20.
November 2013 (GVBl. l Nr. 32 S. 2, Nr. 34) verordnet die Landesregierung:

#### § 1 Geltungsbereich

(1) Diese Verordnung regelt die Gewährung von Stellenzulagen für Lehrkräfte mit besonderen Funktionen.
Lehrkräfte im Sinne dieser Verordnung sind Lehrerinnen und Lehrer, Förderschullehrerinnen und Förderschullehrer sowie Studienrätinnen und Studienräte.

(2) Ein Anspruch auf eine Stellenzulage besteht nur, wenn die Funktion ständig wahrgenommen wird und die Wahrnehmung der Funktion nicht schon bei der Einstufung des Amtes der Lehrkraft berücksichtigt ist.

(3) Werden mehrere der in den §§ 2 bis 4 genannten besonderen Funktionen ausgeübt, wird nur die Stellenzulage mit dem höchsten Zulagenbetrag gewährt.

#### § 2 Verwendung in der Lehrerausbildung und Lehrerfortbildung

Lehrkräfte erhalten für die Dauer ihrer Verwendung als

1.  Seminarleiterin oder Seminarleiter oder
2.  Lehrkraft mit Aufgaben in der Lehrerfortbildung für Lehrkräfte ohne abgeschlossene Lehramtsausbildung (Seiteneinsteigerinnen und Seiteneinsteiger)

eine Stellenzulage in Höhe von 100 Euro monatlich.

#### § 3 Verwendung in der Schulvisitation

Lehrkräfte erhalten für die Dauer ihrer Verwendung in der Schulvisitation eine Stellenzulage in Höhe von 100 Euro monatlich.

#### § 4 Verwendung als Lehrertrainerin oder Lehrertrainer

Lehrkräfte, die als Lehrertrainerinnen oder Lehrertrainer im Bereich der sportlichen Begabungsförderung an den Spezialschulen für Sport oder in den Spezialklassen für Sport verwendet werden und eine Trainer A-Lizenz des jeweiligen sportlichen Fachverbandes nachweisen, erhalten für die Dauer der Verwendung eine Stellenzulage in Höhe von 100 Euro monatlich.

#### § 5 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am ersten Tag des auf die Verkündung folgenden Kalendermonats in Kraft.
Gleichzeitig tritt die Lehrkräftezulagenverordnung vom 21.
Februar 2000 (GVBl.
II S. 61) außer Kraft.

Potsdam, den 12.
Oktober 2015

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister der Finanzen

Christian Görke