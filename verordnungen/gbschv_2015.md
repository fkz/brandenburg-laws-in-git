## Verordnung über die staatliche Anerkennung von Schulen für Gesundheitsberufe im Land Brandenburg (Gesundheitsberufeschulverordnung - GBSchV)

Auf Grund

*   des § 4 Absatz 3 Satz 3 und Absatz 4 des Krankenpflegegesetzes vom 16.
    Juli 2003 (BGBI.
    I S. 1442),
*   des § 6 Absatz 2 Satz 3 und Absatz 3 des Notfallsanitätergesetzes vom 22.
    Mai 2013 (BGBl.
    I S. 1348),
*   des § 36 Absatz 2 und 3 des Brandenburgischen Krankenhausentwicklungsgesetzes vom 8. Juli 2009 (GVBI.
    I S. 310), der durch Gesetz vom 18.
    Dezember 2012 (GVBl.
    I Nr. 44) neu gefasst worden ist, und
*   des § 8 Absatz 2 des Brandenburgischen Krankenpflegehilfegesetzes vom 26. Mai 2004 (GVBI.
    I S. 244)

verordnet die Ministerin für Arbeit, Soziales, Gesundheit, Frauen und Familie:

**Inhaltsübersicht**

[§ 1 Anwendungsbereich](#1)

[§ 2 Staatliche Anerkennung](#2)

[§ 3 Zahl der Lehrkräfte](#3)

[§ 4 Lehrkräfte](#4)

[§ 5 Schulleitung](#5)

[§ 6 Standort der Schule und Standort des Schulzentrums](#6)

[§ 7 Räumliche und sächliche Ausstattung der Schule](#7)

[§ 8 Praktische Ausbildung](#8)

[§ 8a Praktische Ausbildung nach dem Pflegeberufegesetz](#8a)

[§ 9 Ausbildungsunterlagen und Schulunterlagen](#9)

[§ 10 Aufsicht und Informationspflichten](#10)

[§ 11 Erlöschen der staatlichen Anerkennung](#11)

[§ 12 Zuständige Behörde](#12)

[§ 13 Übergangsregelung](#13)

[§ 14 Inkrafttreten](#14)

[Anlage 1 Allgemeine räumliche Ausstattung der Schule](#15)

[Anlage 2 Allgemeine sächliche Ausstattung der Schule](#16)

[Anlage 3 Geeignetheit von Einrichtungen zur Durchführung der praktischen Ausbildung nach § 7 Absatz 5 des Pflegeberufegesetzes](#16)

#### § 1 Anwendungsbereich

Diese Verordnung regelt die Voraussetzungen der staatlichen Anerkennung von Schulen für die Ausbildung in den folgenden Gesundheitsberufen sowie die Aufsicht über diese Schulen:

1.  Anästhesietechnische Assistentin und Anästhesietechnischer Assistent,
2.  Diätassistentin und Diätassistent,
3.  Ergotherapeutin und Ergotherapeut,
4.  Gesundheits- und Krankenpflegehelferin und Gesundheits- und Krankenpflegehelfer,
5.  Hebamme und Entbindungspfleger,
6.  Logopädin und Logopäde,
7.  Masseurin und medizinische Bademeisterin und Masseur und medizinischer Bademeister,
8.  Medizinisch-technische Assistentin für Funktionsdiagnostik und Medizinisch-technischer Assistent für Funktionsdiagnostik,
9.  Medizinisch-technische Laboratoriumsassistentin und Medizinisch-technischer Laboratoriumsassistent,
10.  Medizinisch-technische Radiologieassistentin und Medizinisch-technischer Radiologieassistent,
11.  Notfallsanitäterin und Notfallsanitäter,
12.  Operationstechnische Assistentin und Operationstechnischer Assistent,
13.  Orthoptistin und Orthoptist,
14.  Pflegefachfrau und Pflegefachmann,
15.  Pharmazeutisch-technische Assistentin und Pharmazeutisch-technischer Assistent,
16.  Physiotherapeutin und Physiotherapeut,
17.  Podologin und Podologe.

#### § 2 Staatliche Anerkennung

(1) Träger von Schulen, die eine Ausbildung in einem der in § 1 genannten Gesundheitsberufe durchführen wollen, bedürfen der staatlichen Anerkennung.
Hierfür muss ein Antrag bei der zuständigen Behörde gestellt werden.

(2) Voraussetzung für die Erteilung ist, dass der Träger der Schule die Gewähr für eine dauerhafte und ordnungsgemäße Ausbildung nach den Vorgaben der einschlägigen Berufsgesetze sowie der Ausbildungs- und Prüfungsverordnungen und, soweit vorhanden, des Rahmenlehrplans des Landes Brandenburg bietet.
Dabei sind der aktuelle Stand der pädagogischen und didaktischen Erkenntnisse sowie die diesem Absatz nachfolgenden Bestimmungen zu beachten.
Schulen nach § 1 Nummer 14 berücksichtigen zudem die Empfehlungen des Rahmenlehrplans nach § 53 Absatz 1 und 2 des Pflegeberufegesetzes.

(3) Die staatliche Anerkennung darf nur erteilt werden, wenn der Träger der Schule die notwendige Zuverlässigkeit und wirtschaftliche Leistungsfähigkeit für eine dauerhafte und ordnungsgemäße Ausbildung im jeweiligen Gesundheitsberuf besitzt.
Die wirtschaftliche Leistungsfähigkeit liegt insbesondere vor, wenn

1.  ein Finanzplan vorliegt, aus dem hervorgeht, dass die Finanzierung hinreichend gesichert ist, und
2.  eine Bescheinigung in Steuersachen belegt, dass keine Steuerrückstände bestehen, ein fristgerechtes Zahlungsverhalten vorliegt und die Steuererklärungspflichten erfüllt wurden.

(4) Die staatliche Anerkennung darf darüber hinaus nur erteilt werden, wenn die Voraussetzungen der §§ 3 bis 9 erfüllt sind und der Träger der Schule auf Dauer jährlich mindestens eine einzügige Ausbildung gewährleistet.

(5) Die Klassen der in § 1 genannten Gesundheitsberufe sollen nicht mehr als 25 Schülerinnen und Schüler haben.

(6) Die zuständige Behörde setzt die Höchstzahl der Ausbildungsplätze und Anzahl der Klassen je Schule in Abhängigkeit von den Ausbildungsbedingungen fest.
Die Höchstzahl der Ausbildungsplätze sowie die Anzahl der Klassen werden von der zuständigen Behörde neu festgesetzt, wenn sich die Voraussetzungen ändern.

(7) Erfüllt der Träger der Schule einzelne Voraussetzungen gemäß Absatz 4 oder gemäß den §§ 3 bis 9 nicht oder nicht in vollem Umfang, können auf Antrag in begründeten Einzelfällen, insbesondere wenn es das öffentliche Interesse erfordert, Ausnahmen zugelassen werden.

(8) Schulen nach § 1 Nummer 14 dürfen auch Ausbildungen nach § 60 des Pflegeberufegesetzes zur Gesundheits- und Kinderkrankenpflegerin oder zum Gesundheits- und Kinderkrankenpfleger sowie nach § 61 des Pflegeberufsgesetzes zur Altenpflegerin oder zum Altenpfleger durchführen.
In diesem Fall gelten die Maßgaben dieser Verordnung entsprechend.
Darüber hinaus gelten Schulen nach § 1 Nummer 14 als Schulen im Sinne von § 3 Absatz 2 des Brandenburgischen Altenpflegehilfegesetzes vom 27.
Mai 2009 (GVBl.
I S.
154), das durch Artikel 7 des Gesetzes vom 15.
Juli 2010 (GVBl. I Nr. 28 S.
8) geändert worden ist.

#### § 3 Zahl der Lehrkräfte

(1) Jede Schule muss über eine im Verhältnis zu den Ausbildungsplätzen ausreichende Anzahl fachlich und pädagogisch qualifizierter und von der zuständigen Behörde bestätigter hauptberuflicher Lehrkräfte verfügen.

(2) Als ausreichend gilt eine nach § 4 Absatz 1 bis 4 qualifizierte, hauptberufliche und vollzeitbeschäftigte Lehrkraft für Schulen nach

1.  § 1 Nummer 1 und 12 für je 15 bis 17 Ausbildungsplätze,
2.  § 1 Nummer 2, 3, 5, 7 und 16 für je zwölf bis 15 Ausbildungsplätze,
3.  § 1 Nummer 4 und 11 für je 15 Ausbildungsplätze,
4.  § 1 Nummer 6, 13 und 17 für je sechs bis acht Ausbildungsplätze,
5.  § 1 Nummer 8, 9, 10 und 15 für je zehn bis zwölf Ausbildungsplätze,
6.  § 1 Nummer 14 für je 17 Ausbildungsplätze.

Die Schulleitung ist entsprechend ihrer Unterrichtsverpflichtung nach § 5 Absatz 6 in die Zahl der Lehrkräfte einzubeziehen.

(3) Werden hauptberufliche Lehrkräfte vom Träger der Schule nicht ausschließlich nur an einer Schule eingesetzt oder sind sie teilzeitbeschäftigt, sind zur Erfüllung der Maßgabe nach Absatz 2 hauptberufliche Lehrkräfte entsprechend den Vollzeitäquivalenten einzustellen.

#### § 4 Lehrkräfte

(1) Hauptberufliche Lehrkräfte sind fachlich und pädagogisch qualifiziert, wenn folgende Voraussetzungen erfüllt sind:

1.  Nachweis über die Erlaubnis zum Führen der entsprechenden Berufsbezeichnung nach dem jeweiligen Berufsgesetz,
2.  Nachweis eines Master-Abschlusses, aufbauend auf einem entsprechenden Bachelor-Abschluss, der zur Lehre im jeweiligen Gesundheitsberuf befähigt, dabei insgesamt mindestens 180 Leistungspunkte nach dem Europäischen System zur Anrechnung von Studienleistungen (ECTS) in den Bereichen
    
    a)
    
    Fach- und Bezugswissenschaften mit 100 Leistungspunkten verteilt auf Bachelor- und Masterniveau,
    
    b)
    
    Bildungswissenschaften mit 60 Leistungspunkten verteilt auf Bachelor- und Masterniveau, davon 30 Leistungspunkte in der Berufsfelddidaktik und allgemeinen Didaktik und
    
    c)
    
    supervidierte Praktika in der Lehre mit 20 Leistungspunkten überwiegend auf Masterniveau
    
    oder
    
    Nachweis über einen gleichwertigen Hochschulabschluss für die Lehre im jeweiligen Berufsfeld,
    
3.  Nachweis über Berufserfahrung im jeweiligen Berufsfeld und
4.  Nachweis gemäß § 30a des Bundeszentralregistergesetzes, dass die Lehrkraft sich nicht eines Verhaltens schuldig gemacht hat, aus dem sich die Unzuverlässigkeit zur Ausübung der Lehre ergibt.

Hauptberufliche Lehrkräfte sind von der zuständigen Behörde zu bestätigen.

(2) Abweichend von Absatz 1 Nummer 1 sind für Schulen nach § 1 Nummer 7 auch Physiotherapeutinnen oder Physiotherapeuten als hauptberufliche Lehrkräfte geeignet.
Die übrigen Voraussetzungen nach Absatz 1 bleiben unberührt.

(3) Abweichend von Absatz 1 Nummer 1 gilt für Schulen nach § 1 Nummer 1 und 12, dass hauptberufliche Lehrkräfte nur fachlich und pädagogisch qualifiziert sind, wenn sie die Erlaubnis zum Führen folgender Berufsbezeichnung besitzen und zusätzlich eine entsprechende Weiterbildung im Operationsdienst (OTO) oder der Anästhesie (ATA) absolviert haben:

1.  Gesundheits- und Krankenpflegerin oder Gesundheits- und Krankenpfleger,
2.  Krankenschwester oder Krankenpfleger,
3.  Gesundheits- und Kinderkrankenpflegerin oder Gesundheits- und Kinderkrankenpfleger,
4.  Kinderkrankenschwester oder Kinderkrankenpfleger oder
5.  Pflegefachfrau oder Pflegefachmann.

Sie müssen die weiteren in Absatz 1 aufgeführten Voraussetzungen erfüllen.

(4) Abweichend von Absatz 1 Nummer 1 gilt für Schulen nach § 1 Nummer 4 und 14, dass hauptberufliche Lehrkräfte nur fachlich und pädagogisch qualifiziert sind, wenn sie die Erlaubnis zum Führen folgender Berufsbezeichnung besitzen:

1.  Gesundheits- und Krankenpflegerin oder Gesundheits- und Krankenpfleger,
2.  Krankenschwester oder Krankenpfleger,
3.  Altenpflegerin oder Altenpfleger,
4.  Gesundheits- und Kinderkrankenpflegerin oder Gesundheits- und Kinderkrankenpfleger,
5.  Kinderkrankenschwester oder Kinderkrankenpfleger oder
6.  Pflegefachfrau oder Pflegefachmann.

Sie müssen die weiteren in Absatz 1 aufgeführten Voraussetzungen erfüllen.

(5) Hauptberufliche Lehrkräfte bilden sich regelmäßig fort, insbesondere im fachspezifischen und pädagogischen Bereich sowie im Umgang mit Unterschiedlichkeit und der Arbeit mit Menschen mit Behinderungen.

(6) Vollzeitbeschäftigte hauptberufliche Lehrkräfte dürfen an Schulen für Gesundheitsberufe nicht mehr als durchschnittlich 24 bis 26 Unterrichtsstunden je Woche unterrichten.
Eine Unterrichtstunde dauert 45 Minuten.
Zeiten für die Begleitung der praktischen Ausbildung sind anzurechnen.
Für besondere Aufgaben, zum Beispiel die Klassenleitung oder die Betreuung von Nachwuchslehrkräften, verringern sich die Unterrichtswochenstunden in angemessenem Umfang.

(7) Als nebenberufliche Lehrkräfte wirken an der Ausbildung pädagogisch geeignete Spezialistinnen und Spezialisten mit, soweit dies für das Erreichen der in dem entsprechenden Berufsgesetz genannten Ausbildungsziele der in § 1 genannten Gesundheitsberufe erforderlich ist.
Als Spezialistinnen und Spezialisten gelten insbesondere Ärztinnen und Ärzte für die Lehrgebiete der speziellen Krankheitslehre sowie Apothekerinnen und Apotheker bei der Ausbildung nach § 1 Nummer 15.
Absatz 1 Nummer 4 gilt entsprechend.

(8) Zur Sicherung der Ausbildung in den Gesundheitsberufen und zur Gewinnung geeigneter Lehrkräfte können auf Antrag Lehrkräfte, die noch nicht über die Voraussetzungen nach Absatz 1 Nummer 1 oder 2 verfügen, als hauptberufliche Nachwuchslehrkräfte von der zuständigen Behörde bestätigt werden.
Diese Bestätigung darf nur erteilt werden, sofern die Qualität der Ausbildung durch den Einsatz von Nachwuchslehrkräften nicht gefährdet wird.
Die Bestätigung ist zu befristen und mit geeigneten Auflagen zur Sicherung der Ausbildungsqualität und zum Erwerb der unter Absatz 1 Nummer 1 oder 2 genannten Voraussetzungen zu verbinden.
Vollzeitbeschäftigte Nachwuchslehrkräfte dürfen durchschnittlich nicht mehr als 20 Unterrichtsstunden in der Woche unterrichten.
Bei Teilzeitbeschäftigung reduzieren sich die Unterrichtsstunden entsprechend.
Nachwuchslehrkräfte sind durch eine hauptberufliche Lehrkraft zu betreuen.

#### § 5 Schulleitung

(1) Eine Schule für Gesundheitsberufe wird von einer hauptberuflichen und von der zuständigen Behörde bestätigten Lehrkraft geleitet.

(2) Die Schulleitung muss die in § 4 Absatz 1 genannten Voraussetzungen erfüllen.
Darüber hinaus muss sie eine Berufserfahrung von mindestens zwei Jahren in der Lehre nachweisen.

(3) Abweichend von § 4 Absatz 1 Nummer 1 können Schulen nach

1.  § 1 Nummer 1 oder 12 auch von einer Ärztin und einem Arzt mit entsprechender Facharztqualifikation geleitet werden; für die Schulleitung einer Schule nach § 1 Nummer 1 oder 12 ist § 4 Absatz 3 entsprechend anzuwenden,
2.  § 1 Nummer 7 auch von einer Physiotherapeutin oder einem Physiotherapeuten,
3.  § 1 Nummer 11 auch von einer Notärztin oder einem Notarzt,
4.  § 1 Nummer 13 auch von einer Augenärztin oder einem Augenarzt,
5.  § 1 Nummer 15 auch von einer Apothekerin oder einem Apotheker

geleitet werden.
Die übrigen Voraussetzungen nach § 4 Absatz 1 bleiben unberührt.

(4) Für die Schulleitung einer Schule nach § 1 Nummer 4 oder 14 ist § 4 Absatz 4 anzuwenden.

(5) Die Schulleitung bildet sich regelmäßig fort, insbesondere im fachspezifischen und pädagogischen Bereich, im Bereich der Leitung von Schulen sowie im Umgang mit Unterschiedlichkeit und der Arbeit mit Menschen mit Behinderungen.

(6) Die Schulleitung muss selbst Unterricht erteilen.
Die Unterrichtsverpflichtung umfasst höchstens 50 Prozent der Unterrichtsstunden einer hauptberuflichen Lehrkraft.

(7) Eine der hauptberuflichen Lehrkräfte gemäß § 4 Absatz 1 bis 4 ist als stellvertretende Schulleitung zu benennen.
Für die stellvertretende Schulleitung gelten die Absätze 2 bis 5 sowie Absatz 6 Satz 1 entsprechend.

(8) Unterhält ein Träger an einem Standort mehrere Schulen für Gesundheitsberufe als Schulzentrum, muss jede Schule von einer Schulleitung gemäß den Absätzen 1 bis 4 geleitet werden.
Zusätzlich kann eine Person mit der Gesamtleitung des Schulzentrums beauftragt werden.
Die Absätze 1 bis 5 sind dabei zu beachten, von Absatz 6 können Ausnahmen zugelassen werden.

#### § 6 Standort der Schule und Standort des Schulzentrums

(1) Eine Schule für Gesundheitsberufe ist eine Ausbildungsstätte oder juristische Person gemäß § 35 Absatz 1 des Brandenburgischen Krankenhausentwicklungsgesetzes, der die staatliche Anerkennung für einen der in § 1 genannten Gesundheitsberufe erteilt wurde.
Sie soll nur an einem einzigen Standort betrieben werden.

(2) Ein Schulzentrum ist eine Ausbildungsstätte oder juristische Person gemäß § 35 Absatz 1 des Brandenburgischen Krankenhausentwicklungsgesetzes, der die staatliche Anerkennung für mindestens zwei Schulen der in § 1 genannten Gesundheitsberufe erteilt wurde.
Es soll nur an einem einzigen Standort betrieben werden.

#### § 7 Räumliche und sächliche Ausstattung der Schule

(1) Eine Schule für Gesundheitsberufe muss über eine für den ordnungsgemäßen Schulbetrieb angemessene räumliche und sächliche Ausstattung verfügen.

(2) Als angemessene allgemeine räumliche Ausstattung für eine einzügige Ausbildung gelten mindestens die in der Anlage 1 aufgeführten Vorgaben.
Im Fall einer mehrzügigen Ausbildung legt die zuständige Behörde die Ausstattung entsprechend fest.

(3) Als angemessene allgemeine sächliche Ausstattung gelten mindestens die in der Anlage 2 zu dieser Verordnung aufgeführten Vorgaben.

(4) Neben der in den Anlagen 1 und 2 genannten allgemeinen räumlichen und sächlichen Ausstattung sind spezifische, der Fachrichtung entsprechende Ausstattungen und Einrichtungen zur Erreichung der jeweiligen im entsprechenden Berufsgesetz festgelegten Ausbildungsziele erforderlich.
Über die spezifische, der Fachrichtung entsprechende Ausstattung und Einrichtung entscheidet die zuständige Behörde.

(5) Der Träger der Schule ist verpflichtet, die einschlägigen Anforderungen der Bau-, Brand-, Gesundheits- und Arbeitsschutzbestimmungen sowie der Medizinprodukte-Betreiberverordnung in der Fassung der Bekanntmachung vom 21.
August 2002 (BGBl.
I S. 3396), die zuletzt durch Artikel 2 der Verordnung vom 25.
Juli 2014 (BGBl.
I S. 1227) geändert worden ist, in der jeweils geltenden Fassung einzuhalten und entsprechende Nachweise vorzulegen.

#### § 8 Praktische Ausbildung

(1) Die praktische Ausbildung findet in geeigneten und soweit die jeweiligen Berufsgesetze der Berufe nach § 1 dies vorschreiben, in von der zuständigen Behörde ermächtigten Einrichtungen statt.
Die Auswahl der Einrichtungen obliegt der Schule.

(2) Zur dauerhaften Sicherstellung der praktischen Ausbildung muss der Träger der Schule, sofern er die praktische Ausbildung nicht oder nicht vollständig in eigenen Einrichtungen sicherstellen kann, eine ausreichende Anzahl von fachpraktischen Ausbildungsplätzen in anderen geeigneten Einrichtungen nachweisen.
Hierzu hat er Kooperationsvereinbarungen mit den Trägern dieser Einrichtungen abzuschließen.
Diesen Vereinbarungen müssen die Art und Dauer des Praxiseinsatzes sowie die Anzahl der für die praktische Ausbildung bereitgestellten Plätze zu entnehmen sein.
Ferner sind in diesen Vereinbarungen die verantwortlichen Fachkräfte für die Praxisanleitung und die sonstigen Aufgaben und Pflichten der Vertragspartner festzuhalten

(3) Die Schwerpunkte der praktischen Ausbildung sind in Form von Ausbildungsaufgaben durch die Schule unter angemessener Beteiligung der Einrichtungen der praktischen Ausbildung festzulegen.
Die Schule hat die mit der praktischen Ausbildung betraute Einrichtung rechtzeitig über die Ausbildungsaufgaben zu informieren.

(4) Die praktischen Ausbildungseinsätze müssen durch eine angemessene Anzahl von Besuchen durch die hauptberuflichen Lehrkräfte der Schule begleitet werden.
Als angemessen gilt in der Regel ein Besuch innerhalb von sechs bis acht Wochen.
Je Praxiseinsatz sollte in der Regel mindestens ein Besuch erfolgen.
Die Praxisbegleitung dient insbesondere

1.  der Betreuung, Unterstützung und Förderung der Schülerinnen und Schüler,
2.  der Beratung und Unterstützung der für die Praxisanleitung zuständigen Fachkräfte,
3.  der Leistungsüberprüfungen der Schülerinnen und Schüler gemeinsam mit den Fachkräften für die Praxisanleitung und
4.  der Überwachung der Ausbildungsqualität gemeinsam mit der Ausbildungseinrichtung.

(5) Neben den in den Absätzen 1 bis 4 genannten allgemeinen Anforderungen an die praktische Ausbildung sind, soweit vorhanden, die Empfehlungen des Rahmenlehrplans des Landes Brandenburg zu beachten.
Darüber hinaus können spezifische, der Fachrichtung entsprechende Anforderungen zur Erreichung der jeweiligen im entsprechenden Berufsgesetz festgelegten Ausbildungsziele erforderlich sein.
Über die spezifischen Anforderungen entscheidet die zuständige Behörde.

#### § 8a Praktische Ausbildung nach dem Pflegeberufegesetz

(1) Die praktische Ausbildung findet in geeigneten Einrichtungen statt.
Die Eignung richtet sich nach der Anlage 3.

(2) In den Kooperationsverträgen nach § 8 der Pflegeberufe-Ausbildungs- und -Prüfungsverordnung ist zudem festzuhalten:

1.  die Pflicht des Trägers der praktischen Ausbildung und der weiteren an der Ausbildung beteiligten Einrichtungen, an der Nachweispflicht nach § 9 Absatz 1 mitzuwirken,
2.  die verantwortlichen Fachkräfte für die Praxisanleitung,
3.  die Pflicht des Trägers der praktischen Ausbildung und der weiteren an der Ausbildung beteiligten Einrichtungen, der zuständigen Behörde Auskünfte zu der Durchführung der praktischen Ausbildung zu erteilen, Einsicht in Ausbildungsunterlagen sowie Zutritt zu den Ausbildungseinrichtungen für die Inspektion nach § 10 Absatz 2 zu gewähren,
4.  die Pflicht des Trägers der praktischen Ausbildung und der weiteren an der Ausbildung beteiligten Einrichtungen, die berufspädagogischen Fortbildungen nach § 4 Absatz 3 Satz 1 der Pflegeberufe-Ausbildungs- und -Prüfungsverordnung auf Verlangen der zuständigen Behörde nachzuweisen.

(3) Auf die praktische Ausbildung nach dem Pflegeberufegesetz findet § 8 mit der Ausnahme des Absatzes 4 sowie des Absatzes 5 Satz 2 und 3 keine Anwendung.

#### § 9 Ausbildungsunterlagen und Schulunterlagen

(1) Der Träger der Schule muss die ordnungsgemäße Durchführung der gesamten Ausbildung gegenüber der zuständigen Behörde durch Vorlage der in den nachfolgenden Absätzen benannten Unterlagen nachweisen.

(2) Es ist ein dem aktuellen Stand der Bildungswissenschaften entsprechendes Curriculum auf der Grundlage der jeweils einschlägigen Ausbildungs- und Prüfungsverordnung und, soweit vorhanden, des Rahmenlehrplans des Landes Brandenburg und für Schulen nach § 1 Nummer 14 auf der Grundlage der Empfehlungen des Rahmenlehrplans nach § 53 Absatz 1 und 2 des Pflegeberufegesetzes zu erstellen.
Ergänzend zum Curriculum ist in einer Rahmenstundentafel die geplante Verteilung der Stundenzahl auf Fächer, Kompetenz- oder Themenbereiche und auf die jeweiligen Ausbildungsjahre darzulegen.

(3) In einem Rahmenablaufplan sind der theoretische und praktische Unterricht sowie die praktische Ausbildung über die gesamte Ausbildungszeit wochenweise abzubilden.

(4) Für die praktische Ausbildung ist eine Übersicht über die Einrichtungen und Einsatzbereiche unter Angabe der Anzahl der zur Verfügung stehenden Ausbildungsplätze und der in den Einrichtungen mit der Anleitung beauftragten Fachkräfte (Praxisanleitung) zu erstellen.

(5) Es ist eine Schulordnung aufzustellen, welche insbesondere die Organisation des Schulbetriebes und die Grundsätze der Leistungsüberprüfung sowie Leistungsbewertung enthält.

(6) Der Einsatz der haupt- und nebenberuflichen Lehrkräfte in den Unterrichtsfächern, Kompetenz- oder Themenbereichen ist unter Angabe der jeweiligen Stundenanteile in einer Übersicht zusammenzustellen.

(7) Schulen nach § 1 Nummer 14 erstellen ein Konzept für die Zwischenprüfung nach § 7 der Pflegeberufe-Ausbildungs- und -Prüfungsordnung.

(8) Zwischen dem Träger der Ausbildung und der Schülerin oder dem Schüler ist ein schriftlicher Ausbildungsvertrag zu schließen.
Der Ausbildungsvertrag muss mindestens enthalten:

1.  die Bezeichnung der Ausbildungseinrichtung,
2.  die Bezeichnung des Berufes,
3.  den Beginn und die Dauer der Ausbildung,
4.  Angaben über das der Ausbildung zugrunde liegende Berufsgesetz und die entsprechende Ausbildungs- und Prüfungsverordnung,
5.  die Dauer der regelmäßigen täglichen oder wöchentlichen Ausbildungszeit,
6.  die Dauer der Probezeit,
7.  Angaben über Zahlung und Höhe der Ausbildungsvergütung, soweit dies im Berufsgesetz vorgesehen ist,
8.  Angaben über die Höhe der Ausbildungskosten, soweit diese der Schülerin oder dem Schüler in Rechnung gestellt werden,
9.  die Dauer des Urlaubs oder der Ferien,
10.  die Voraussetzungen, unter denen der Ausbildungsvertrag gekündigt werden kann,
11.  Regelungen zur eventuell notwendigen Verlängerung der Ausbildung und
12.  Regelungen zur eventuell notwendigen Wiederholung der staatlichen Prüfung.

Änderungen des Ausbildungsvertrages bedürfen der Schriftform.

(9) Die gesamte Ausbildung, insbesondere der theoretische und der praktische Unterricht, die praktische Ausbildung sowie die Leistungsüberprüfungen sind angemessen zu dokumentieren.

(10) Alle die Ausbildung betreffenden Daten einschließlich personenbezogener Daten dürfen in Akten und Dateien gespeichert werden und sind am Standort der Schule aufzubewahren.
Zur Sicherung der aufbewahrten Akten und gespeicherten Dateien ist ein Datenschutzkonzept vorzulegen.
Für die Speicherung von Dateien und die Aufbewahrung von Akten in der Schule gelten als Fristen für

1.  Akten der Schülerinnen und Schüler sowie Prüfungsunterlagen, soweit diese nicht bei der zuständigen Behörde aufbewahrt werden, zehn Jahre,
2.  Klassen- und Notenbücher sowie Leistungsüberprüfungen zwei Jahre,
3.  Unterlagen über die Schulleitung und die Lehrkräfte zwei Jahre.

Alle sonstigen personenbezogenen Daten von Schülerinnen und Schülern sind nach Abschluss des Zwecks, für die sie erhoben wurden, zu löschen oder zu vernichten, spätestens zu dem Zeitpunkt, zu dem die Schülerin oder der Schüler die Schule verlässt.
Die Aufbewahrungs- oder Speicherungsfrist beginnt für Nachweise gemäß den Nummern 1 und 2 am Ende der Ausbildung der betroffenen Schülerin oder des betroffenen Schülers sowie gemäß Nummer 3 am Ende des Ausbildungsjahres nach der Abberufung oder dem Ausscheiden der Schulleitung oder dem Ausscheiden der betreffenden Lehrkraft aus der Schule.

(11) Sofern die Schule schließt, die staatliche Anerkennung zurückgenommen oder widerrufen wird, hat der Träger der Schule die gesicherte Aufbewahrung der Akten und Speicherung von Dateien unter Einhaltung der in Absatz 9 Satz 3 benannten Fristen nachzuweisen.

#### § 10 Aufsicht und Informationspflichten

(1) Die Schulen für Gesundheitsberufe unterliegen der staatlichen Aufsicht.
Die Aufsicht erstreckt sich auf die Einhaltung einer den Ausbildungszielen entsprechenden qualitativ hochwertigen Ausbildung und Prüfung sowie auf die Voraussetzungen der §§ 2 bis 9.

(2) Die zuständige Behörde kann von der Schule jederzeit Informationen über maßgebliche Tatsachen zum Schulbetrieb anfordern und Einsicht in Ausbildungsunterlagen und Schulunterlagen nehmen.
Sie führt zur Überprüfung der Voraussetzungen der §§ 2 bis 9 regelmäßig Inspektionen durch.
Diese können Stichprobenüberprüfungen und Hospitationen im theoretischen und praktischen Unterricht sowie Überprüfungen der praktischen Ausbildungseinrichtungen einschließen.

(3) Der Träger der Schule ist verpflichtet, der zuständigen Behörde wesentliche Änderungen der für die staatliche Anerkennung maßgeblichen Tatsachen unverzüglich anzuzeigen und geplante wesentliche Änderungen rechtzeitig zu beantragen.
Als wesentliche Änderungen gelten insbesondere ein Standort- oder Trägerwechsel, ein Wechsel in der Schulleitung oder des hauptberuflichen Lehrpersonals, Änderungen der Höchstzahl der Ausbildungsplätze, das Vorhaben, grundlegend von den nach § 9 beigebrachten Ausbildungsunterlagen abzuweichen sowie die geplante Beendigung des Ausbildungsbetriebes.

(4) Der Träger der Schule ist verpflichtet, Auskunft insbesondere über statistische Daten gegenüber der zuständigen Behörde und autorisierten Dritten zu erteilen.

(5) Die Schule ist verpflichtet, den Schülerinnen und Schülern zu Beginn der Ausbildung alle einschlägigen Rechtsvorschriften für die Ausbildung und soweit vorhanden den Rahmenlehrplan des Landes Brandenburg, das Curriculum oder eine curriculare Übersicht, die Rahmenstundentafel, den Rahmenablaufplan sowie die Schulordnung in geeigneter Form bekannt zu geben.
Schulen nach § 1 Nummer 14 geben zusätzlich den Rahmenlehrplan nach § 53 Absatz 1 und 2 des Pflegeberufegesetzes bekannt.
Die konkrete Unterrichtsplanung ist den Schülerinnen und Schülern spätestens zwei Wochen vorab zur Kenntnis zu geben.
Die Ausbildungsaufgaben für die einzelnen praktischen Ausbildungsabschnitte sind den Schülerinnen und Schülern rechtzeitig vor Beginn des jeweiligen praktischen Ausbildungsabschnittes zur Kenntnis zu geben.

#### § 11 Erlöschen der staatlichen Anerkennung

Die staatliche Anerkennung erlischt bei Schließung der Schule.

#### § 12 Zuständige Behörde

Zuständige Behörde zur Durchführung dieser Verordnung ist das für Gesundheit zuständige Landesamt.

#### § 13 Übergangsregelung

(1) Staatliche Anerkennungen von Schulen, die vor Inkrafttreten dieser Verordnung erteilt wurden, gelten fort.
Diese Anerkennungen können widerrufen werden, wenn die Voraussetzungen des § 2 Absatz 2 bis 7 sowie der §§ 3 bis 9 nicht innerhalb von zwei Jahren nach dem Inkrafttreten dieser Verordnung erfüllt werden.

(2) Die Voraussetzungen des § 4 Absatz 1 Nummer 1 bis 3 und des § 5 Absatz 2 gelten nicht für vor Inkrafttreten dieser Verordnung bestätigte Schulleitungen und Lehrkräfte.

(3) Für Schulen gemäß § 1 Nummer 11 gelten neben den Maßgaben dieser Verordnung die Regelungen des § 31 des Notfallsanitätergesetzes.

(4) Für Schulen gemäß § 1 Nummer 1 und 12 gelten neben den Maßgaben dieser Verordnung die Regelungen des § 68 des Anästhesietechnische- und Operationstechnische-Assistenten-Gesetzes“ vom 14.
Dezember 2019 (BGBI.
I S.
2768).

(5) Vor Inkrafttreten dieser Verordnung erteilte Nebenbestimmungen bleiben unberührt.

(6) Staatliche Anerkennungen von Schulen für die Ausbildungen nach dem Krankenpflegegesetz vom 16. Juli 2003 (BGBI.
I S.
1442), das zuletzt durch Artikel 1a des Gesetzes vom 17.
Juli 2017 (BGBl. I S. 2581) geändert worden ist, die bis zum 31.
Dezember 2019 nach den bis dahin geltenden Vorschriften staatlich anerkannt sind, gelten ab dem 1.
Januar 2020 als staatliche Anerkennung nach § 2 von Schulen nach § 1 Nummer 14.
Schulen mit einer staatlichen Anerkennung nach Satz 1 haben vor Beginn des ersten Ausbildungsjahrganges nach dem Pflegeberufegesetz die Voraussetzungen nach § 6 Absatz 2 des Pflegeberufegesetzes und § 8 Absatz 1 der Pflegeberufe-Ausbildungs- und -Prüfungsverordnung gegenüber der zuständigen Behörde nachzuweisen.
Für Ausbildungen nach § 66 Absatz 1 des Pflegeberufegesetzes gelten die Vorschriften dieser Verordnung in der am 31. Dezember 2019 geltenden Fassung bis zum 31.
Dezember 2024 fort.

(7) Staatliche Anerkennungen von Altenpflegeschulen, die am 31.
Dezember 2019 nach den Vorschriften der Altenpflegeschulverordnung vom 22.
April 2009 (GVBl.
II S.
263) staatlich anerkannt sind, gelten ab dem 1.
Januar 2020 als staatliche Anerkennung nach § 2 von Schulen nach § 1 Nummer 14.
Für Schulen mit einer staatlichen Anerkennung nach Satz 1 gilt Absatz 5 Satz 2 entsprechend.
Für Ausbildungen nach § 66 Absatz 2 des Pflegeberufegesetzes gelten die Vorschriften der Altenpflegeschulverordnung in der am 31.
Dezember 2019 geltenden Fassung bis zum 31. Dezember 2024 fort.
Für Ausbildungen nach dem Brandenburgischen Altenpflegehilfegesetz gelten die Vorschriften der Altenpflegeschulverordnung in der am 31.
Dezember 2019 geltenden Fassung bis zum 31.
Dezember 2029 fort.

(8) Die staatliche Anerkennung nach Absatz 7 Satz 1 ist zu widerrufen, wenn das Vorliegen der Voraussetzungen nach § 3 Absatz 2 Nummer 5 nicht bis zum 31.
Dezember 2029 gegenüber der zuständigen Behörde nachgewiesen wird.

#### § 14 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 25.
Februar 2015

Die Ministerin für Arbeit, Soziales,  
Gesundheit, Frauen und Familie

Diana Golze

* * *

### Anlagen

1

[Anlage 1 (zu § 7 Absatz 2) Allgemeine räumliche Ausstattung der Schule](/br2/sixcms/media.php/68/GBSchV-Anlage-1.pdf "Anlage 1 (zu § 7 Absatz 2) Allgemeine räumliche Ausstattung der Schule") 194.5 KB

2

[Anlage 2 (zu § 7 Absatz 3) Allgemeine sächliche Ausstattung der Schule](/br2/sixcms/media.php/68/GBSchV-Anlage-2.pdf "Anlage 2 (zu § 7 Absatz 3) Allgemeine sächliche Ausstattung der Schule") 266.7 KB

3

[Anlage 3 (zu § 8a Absatz 1) Geeignetheit von Einrichtungen zur Durchführung der praktischen Ausbildung nach § 7 Absatz 5 des Pflegeberufegesetzes](/br2/sixcms/media.php/68/GBSchV-Anlage-3.pdf "Anlage 3 (zu § 8a Absatz 1) Geeignetheit von Einrichtungen zur Durchführung der praktischen Ausbildung nach § 7 Absatz 5 des Pflegeberufegesetzes") 278.9 KB