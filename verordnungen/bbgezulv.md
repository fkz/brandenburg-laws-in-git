## Verordnung über die Gewährung von Erschwerniszulagen im Land Brandenburg (Brandenburgische Erschwerniszulagenverordnung - BbgEZulV)

Auf Grund des § 45 des Brandenburgischen Besoldungsgesetzes vom 20.
November 2013 (GVBl. I Nr. 32 S. 2, Nr. 34) verordnet die Landesregierung:

Inhaltsübersicht
----------------

Abschnitt 1  
Allgemeine Vorschriften
-------------------------------------

[§ 1 Anwendungsbereich](#)  
[§ 2 Ausschluss einer Erschwerniszulage neben einer Ausgleichszulage](#2)  
[§ 3 Erschwerniszulage bei einer Verwendung im Dienst des Bundes oder eines anderen Landes](#3)

Abschnitt 2  
Einzeln abzugeltende Erschwernisse
------------------------------------------------

### Unterabschnitt 1  
Zulage für Dienst zu ungünstigen Zeiten

[§ 4 Allgemeine Voraussetzungen](#4)  
[§ 5 Höhe und Berechnung der Zulage](#5)  
[§ 6 Fortzahlung bei vorübergehender Dienstunfähigkeit](#6)  
[§ 7 Ausschluss der Zulage](#7)

### Unterabschnitt 2  
Zulage für Tauchertätigkeit

[§ 8 Allgemeine Voraussetzungen](#8)  
[§ 9 Höhe der Zulage](#9)  
[§ 10 Berechnung der Zulage](#10)

### Unterabschnitt 3  
Zulagen für den Umgang mit Munition und Explosivstoffen

[§ 11 Zulage für den Umgang mit Munition mit besonders hohem Gefährlichkeitsgrad](#11)  
[§ 12 Zulage für Sprengstoffentschärfung und Sprengstoffermittlung](#12)

### Unterabschnitt 4  
Zulage für Tätigkeiten an Antennen und Antennenträgern, an Geräten und Geräteträgern des Wetterdienstes, des Vermessungsdienstes, an Windmasten des lufthygienischen Überwachungsdienstes sowie an Tankanlagen zur eichtechnischen Überprüfung

[§ 13 Allgemeine Voraussetzungen](#13)  
[§ 14 Höhe der Zulage](#14)  
[§ 15 Berechnung der Zulage](#15)  
[§ 16 Zulage für Tätigkeiten an Geräten und Geräteträgern des Wetterdienstes, des Vermessungsdienstes, an Windmasten des lufthygienischen Überwachungsdienstes sowie an Tankanlagen zur eichtechnischen Überprüfung](#16)

### Unterabschnitt 5  
Zulage für Tätigkeiten als Notfallsanitäterin oder Notfallsanitäter

[§ 16a Zulage für Notfallsanitäterinnen und Notfallsanitäter](#16a)

Abschnitt 3  
Zulagen in festen Monatsbeträgen
----------------------------------------------

[§ 17 Entstehung des Anspruchs](#17)  
[§ 18 Unterbrechung der zulagenberechtigenden Tätigkeit](#18)  
[§ 19 Zulagen für Wechselschichtdienst und für Schichtdienst](#19)  
[§ 20 Zulagen für die Pflege von Kranken in Justizvollzugseinrichtungen](#20)  
[§ 21 Zulage für besondere polizeiliche Einsätze und Einsätze beim Verfassungsschutz](#21)  
[§ 22 Zulage für Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte als fliegendes Personal](#22)  
[§ 23 Zulage für die Beseitigung von Munition aus den Weltkriegen](#23)

Abschnitt 4  
Schlussvorschrift
-------------------------------

[§ 24 Inkrafttreten](#24)

Abschnitt 1  
Allgemeine Vorschriften
-------------------------------------

#### § 1  
Anwendungsbereich

Diese Verordnung regelt die Gewährung von Zulagen zur Abgeltung besonderer, bei der Bewertung des Amtes oder bei der Regelung der Anwärterbezüge nicht berücksichtigter Erschwernisse (Erschwerniszulagen) für Empfängerinnen und Empfänger von Dienstbezügen oder Anwärterbezügen.
Durch eine Erschwerniszulage wird ein mit der Erschwernis verbundener Aufwand mit abgegolten.

#### § 2  
Ausschluss einer Erschwerniszulage neben einer Ausgleichszulage

Ist die Gewährung einer Erschwerniszulage neben einer anderen Zulage ganz oder teilweise ausgeschlossen, gilt dies auch für eine nach Wegfall der anderen Zulage gewährte Ausgleichszulage, solange diese noch nicht bis zur Hälfte aufgezehrt ist.

#### § 3  
Erschwerniszulage bei einer Verwendung im Dienst des Bundes oder eines anderen Landes

Sehen die Vorschriften zu den Erschwerniszulagen des Bundes oder eines anderen Landes Zulagen vor, die in dieser Verordnung nicht geregelt sind, so erhalten Beamtinnen und Beamte während der Zeit ihrer Verwendung im Dienst des Bundes oder eines anderen Landes die Erschwerniszulage nach Maßgabe und in Höhe der Vorschriften des Bundes oder des anderen Landes.
Voraussetzung ist, dass der Dienstherr, für den sie tätig sind, eine Erstattung in vollem Umfang vornimmt.

Abschnitt 2  
Einzeln abzugeltende Erschwernisse
------------------------------------------------

### Unterabschnitt 1  
Zulage für Dienst zu ungünstigen Zeiten

#### § 4  
Allgemeine Voraussetzungen

(1) Empfängerinnen und Empfänger von Dienstbezügen in Besoldungsgruppen mit aufsteigenden Gehältern oder von Anwärterbezügen erhalten eine Zulage für Dienst zu ungünstigen Zeiten, wenn sie mit mehr als fünf Stunden im Kalendermonat zum Dienst zu ungünstigen Zeiten herangezogen werden.
Bei Teilzeitbeschäftigung werden diese Dienststunden im gleichen Umfang wie die Arbeitszeit reduziert.

(2) Dienst zu ungünstigen Zeiten ist der Dienst

1.  an Sonntagen und gesetzlichen Wochenfeiertagen,
    
2.  an Samstagen nach 13 Uhr,
    
3.  an den Samstagen vor Ostern und Pfingsten nach 12 Uhr; dies gilt auch für den 24.
    und 31. Dezember jeden Jahres, wenn diese Tage nicht auf einen Sonntag fallen,
    
4.  an den übrigen Tagen in der Zeit zwischen 20 Uhr und 6 Uhr.
    

(3) Zulagefähig sind nur Zeiten einer tatsächlichen Dienstausübung.
Bereitschaftsdienst, der zu ungünstigen Zeiten geleistet wird, ist voll zu berücksichtigen.
Wachdienst ist nur zulagefähig, wenn er mit mehr als 24 Stunden im Kalendermonat zu ungünstigen Zeiten geleistet wird.
Bei Teilzeitbeschäftigung werden diese Dienststunden im gleichen Umfang wie die Arbeitszeit reduziert.

(4) Zum Dienst zu ungünstigen Zeiten gehören nicht Reisezeiten bei Dienstreisen und die Rufbereitschaft.

(5) Rufbereitschaft im Sinne von Absatz 4 ist das Bereithalten von hierzu Verpflichteten in ihrer Wohnung (Hausrufbereitschaft) oder das Bereithalten an einem von ihnen anzuzeigenden und dienstlich genehmigten Ort ihrer Wahl (Wahlrufbereitschaft), um bei Bedarf zu Dienstleistungen sofort abgerufen werden zu können.
Beim Wohnen in einer Gemeinschaftsunterkunft gilt diese als Wohnung.

#### § 5  
Höhe und Berechnung der Zulage

(1) Die Zulage beträgt für Dienst

1.  an Sonntagen und gesetzlichen Wochenfeiertagen, an den Samstagen vor Ostern und Pfingsten nach 12 Uhr sowie am 24.
    und 31.
    Dezember jeden Jahres nach 12 Uhr, wenn diese Tage nicht auf einen Sonntag fallen, ab 1.
    Januar 2019 3,64 Euro je Stunde, ab 1. Januar 2020 3,77 Euro je Stunde und ab 1.
    Januar 2021 3,82 Euro je Stunde,
    
2.  an den übrigen Samstagen in der Zeit zwischen 13 Uhr und 20 Uhr 0,64 Euro je Stunde und ab 1.
    August 2014 0,73 Euro je Stunde sowie
    
3.  im Übrigen in der Zeit zwischen 20 Uhr und 6 Uhr 1,28 Euro je Stunde und ab 1.
    August 2014 1,47 Euro je Stunde.
    

(2) In den Fällen des Absatzes 1 Nummer 2 beträgt die Zulage

1.  für Beamtinnen und Beamte nach den Nummern 8 und 9 der Vorbemerkungen zu den Besoldungsordnungen A und B des Brandenburgischen Besoldungsgesetzes (Polizeizulage und Feuerwehrzulage) sowie
    
2.  für Beamtinnen und Beamte in Ämtern der Besoldungsordnung A des Brandenburgischen Besoldungsgesetzes bei Justizvollzugsanstalten
    

0,77 Euro je Stunde; dies gilt auch für entsprechende Beamtinnen und Beamte auf Widerruf im Vorbereitungsdienst.

(3) Für Dienst über volle Stunden hinaus wird die Zulage anteilig gewährt.

#### § 6  
Fortzahlung bei vorübergehender Dienstunfähigkeit

Bei einer vorübergehenden Dienstunfähigkeit infolge eines Unfalls im Sinne des § 56 des Brandenburgischen Beamtenversorgungsgesetzes wird Beamtinnen und Beamten des Vollzugsdienstes und des Einsatzdienstes der Feuerwehr die Zulage für Dienst zu ungünstigen Zeiten weitergewährt.
Ferner wird die Zulage weitergewährt, wenn Beamtinnen oder Beamte bei einem besonderen Einsatz im Ausland oder im dienstlichen Zusammenhang damit einen Unfall erleiden, der auf vom Inland wesentlich abweichende Verhältnisse mit gesteigerter Gefährdungslage zurückzuführen ist.
Die sonstigen Voraussetzungen des § 46 des Brandenburgischen Beamtenversorgungsgesetzes bleiben dabei außer Betracht.
Bemessungsgrundlage für die Zahlung der Erschwerniszulage ist der Durchschnitt der Zulage der letzten drei Monate vor Beginn des Monats, in dem die vorübergehende Dienstunfähigkeit eingetreten ist.

#### § 7  
Ausschluss der Zulage

(1) Die Zulage wird nicht gewährt neben

1.  einer Vergütung für Beamtinnen und Beamte im Vollstreckungsdienst (§ 47 des Brandenburgischen Besoldungsgesetzes),
    
2.  Auslandsbesoldung (§ 52 des Brandenburgischen Besoldungsgesetzes),
    
3.  einer Zulage nach Nummer 7 der Vorbemerkungen zu den Besoldungsordnungen A und B des Brandenburgischen Besoldungsgesetzes (Sicherheitszulage).
    

(2) Die Zulage entfällt oder sie verringert sich, soweit der Dienst zu ungünstigen Zeiten auf andere Weise als mit abgegolten oder ausgeglichen gilt.

### Unterabschnitt 2  
Zulage für Tauchertätigkeit

#### § 8  
Allgemeine Voraussetzungen

(1) Beamtinnen und Beamte erhalten eine Zulage für Tauchertätigkeiten.

(2) Tauchertätigkeiten sind Übungen oder Arbeiten im Wasser

1.  im Tauchanzug ohne Helm oder ohne Tauchgerät,
    
2.  mit Helm oder Tauchgerät.
    

Zu den Tauchertätigkeiten gehören auch Übungen oder Arbeiten in Pressluft (Druckkammern).

#### § 9  
Höhe der Zulage

(1) Die Zulage für Tauchertätigkeit nach § 8 Absatz 2 Satz 1 Nummer 1 beträgt je Stunde 2,76 Euro und ab 1.
August 2014 3,09 Euro.

(2) Die Zulage für Tauchertätigkeit nach § 8 Absatz 2 Satz 1 Nummer 2 beträgt je Stunde Tauchzeit bei einer Tauchtiefe

1. bis zu 5 Metern

11,45 Euro und ab 1. August 2014 12,82 Euro,

2.
von mehr als 5 Metern

13,89 Euro und ab 1.
August 2014 15,56 Euro,

3.
von mehr als 10 Metern

17,26 Euro und ab 1.
August 2014 19,33 Euro,

4.
von mehr als 15 Metern

22,23 Euro und ab 1.
August 2014 24,90 Euro.

Bei Tauchtiefen von mehr als 20 Metern erhöht sich die Zulage für je 5 Meter weiterer Tauchtiefe um 4,44 Euro je Stunde und ab 1.
August 2014 um 4,97 Euro je Stunde.

(3) Die Zulage nach Absatz 2 erhöht sich für Tauchertätigkeit

1.  in Strömung mit Stromschutz gleich welcher Art um 15 Prozent,
    
2.  in Strömung ohne Stromschutz um 30 Prozent,
    
3.  in Binnenwasserstraßen bei Lufttemperaturen von weniger als 3 Grad Celsius Wärme um 25 Prozent.
    

(4) Die Zulage für Tauchertätigkeit nach § 8 Absatz 2 Satz 2 beträgt je Stunde ein Drittel der Sätze nach Absatz 2.

#### § 10  
Berechnung der Zulage

(1) Die Zulage wird nach Stunden berechnet.
Die Zeiten sind für jeden Kalendertag zu ermitteln, und das Ergebnis ist zu runden.
Dabei bleiben Zeiten von weniger als 10 Minuten unberücksichtigt; Zeiten von 10 bis 30 Minuten werden auf eine halbe Stunde, von mehr als 30 Minuten auf eine volle Stunde aufgerundet.

(2) Als Tauchzeit gilt

1.  für Helmtaucherinnen und Helmtaucher die Zeit unter dem geschlossenen Taucherhelm,
    
2.  für Schwimmtaucherinnen und Schwimmtaucher die Zeit unter der Atemmaske,
    
3.  bei Arbeiten in Druckkammern die Zeit von Beginn des Einschleusens bis zum Ende des Ausschleusens.
    

### Unterabschnitt 3  
Zulagen für den Umgang mit Munition und Explosivstoffen

#### § 11  
Zulage für den Umgang mit Munition mit besonders hohem Gefährlichkeitsgrad

Beamtinnen und Beamte erhalten für das Laborieren, Delaborieren, Untersuchen von unbekannter, beanstandeter oder belasteter Munition mit besonders hohem Gefährlichkeitsgrad oder entsprechenden Munitionskomponenten eine Zulage in Höhe von 3,83 Euro täglich.
Die Tätigkeit muss von der Beamtin oder dem Beamten selbst ausgeübt werden.
Bei einem Einsatz von mehr als sechs Stunden täglich erhöht sich die Zulage für jede weitere Stunde um 0,77 Euro, höchstens jedoch bis zu 7,68 Euro.

#### § 12  
Zulage für Sprengstoffentschärfung und Sprengstoffermittlung

(1) Beamtinnen und Beamte mit gültigem Nachweis über eine erfolgreich abgeschlossene Ausbildung zur Sprengstoffentschärferin oder zum Sprengstoffentschärfer, deren ständige Aufgabe das Prüfen, Entschärfen und Beseitigen unkonventioneller Spreng- und Brandvorrichtungen ist, erhalten eine Zulage.
Die Zulage beträgt 25,56 Euro für jeden Einsatz im unmittelbaren Gefahrenbereich, der erforderlich wird, um verdächtige Gegenstände einer näheren Behandlung zu unterziehen.
Unmittelbarer Gefahrenbereich ist der Wirkungsbereich einer möglichen Explosion oder eines Brandes.

Die Behandlung umfasst insbesondere

1.  optische, akustische, elektronische und mechanische Prüfung auf Spreng-, Zünd- und Brandvorrichtungen,
    
2.  Überwinden von Sprengfallen, Öffnen von unkonventionellen Spreng- und Brandvorrichtungen, Trennen der Zündkette, Unterbrechen der Zündauslösevorrichtung, Neutralisieren, Phlegmatisieren,
    
3.  Vernichten, Transportvorbehandlung, Verladen, Transportieren der unkonventionellen Spreng- und Brand-vorrichtungen oder ihrer Teile.
    

Die Zulage darf den Betrag von 383,40 Euro im Monat nicht übersteigen.

(2) Besondere Schwierigkeiten bei dem Unschädlichmachen oder Delaborieren von Spreng- und Brandvorrichtungen oder ähnlichen Gegenständen, die explosionsgefährliche Stoffe enthalten, können mit einer Erhöhung der Zulage auf bis zu 255,65 Euro für jeden Einsatz abgegolten werden.

(3) Beamtinnen und Beamte mit gültigem Nachweis über eine erfolgreich abgeschlossene Ausbildung zur Sprengstoffermittlerin oder zum Sprengstoffermittler, die im Rahmen ihrer Tätigkeit als Sprengstoffermittlerin oder Sprengstoffermittler mit explosionsgefährlichen Stoffen umgehen, erhalten eine Zulage von 15,34 Euro je Einsatz.
Der Umgang umfasst insbesondere Sicherstellung, Asservierung und Transport.
Die Zulage darf den Betrag von 230,10 Euro im Monat nicht übersteigen.

(4) Die Zulagen nach den Absätzen 1 und 2 dürfen den Gesamtbetrag von 818,07 Euro im Monat nicht übersteigen.

### Unterabschnitt 4  
Zulage für Tätigkeiten an Antennen und Antennenträgern, an Geräten und Geräteträgern des Wetterdienstes, des Vermessungsdienstes, an Windmasten des lufthygienischen Überwachungsdienstes sowie an Tankanlagen zur eichtechnischen Überprüfung

#### § 13  
Allgemeine Voraussetzungen

(1) Beamtinnen und Beamte erhalten eine Zulage für Tätigkeiten an Antennen oder Antennenträgern, wenn diese Tätigkeiten zu ihren regelmäßigen Aufgaben gehören.

(2) Tätigkeiten an Antennen oder Antennenträgern sind

1.  das Besteigen von Antennenträgern über Leitern oder Sprossen,
    
2.  die Arbeiten in einer Höhe von mindestens 20 Metern über dem Erdboden an und auf über Leitern oder Sprossen zu besteigenden Antennenträgern oder an Antennen, die sich auf Dächern und Plattformen ohne Randsicherung (oder ohne seitliche Abdeckung) oder an wegen ihrer schweren Zugänglichkeit ähnlich gefährlichen Stellen befinden.
    

#### § 14  
Höhe der Zulage

(1) Die Zulage für eine Tätigkeit nach § 13 Absatz 2 Nummer 1 beträgt für jeden Tag bei Überwindung eines Höhenunterschiedes

1. von mehr als 20 Metern

1,53 Euro,

2.
von mehr als 50 Metern

2,56 Euro,

3.
von mehr als 100 Metern

4,09 Euro,

4.
von mehr als 200 Metern

6,65 Euro,

5.
von mehr als 300 Metern

9,20 Euro.

Diese Sätze erhöhen sich, wenn vom Erdboden bis zum Fußpunkt der Leitern oder Sprossen ein Höhenunterschied besteht

1. von mehr als 50 Metern

um 0,51 Euro,

2.
von mehr als 100 Metern

um 1,02 Euro,

3.
von mehr als 200 Metern

um 1,53 Euro,

4.
von mehr als 300 Metern

um 2,05 Euro.

Die Zulage nach Satz 1 erhöht sich ferner, wenn die Tätigkeit in den Monaten November bis März durchgeführt wird, um jeweils 25 Prozent.

(2) Die Zulage für Tätigkeiten nach § 13 Absatz 2 Nummer 2 beträgt für jeden Tag bei

1.  Inaugenscheinnahme aus besonderem Anlass, Prüfgängen, Erkundungen, Einweisungen oder Beaufsichtigungen 1,02 Euro,
    
2.  Instandhalten, Instandsetzen oder Abnehmen 1,53 Euro,
    
3.  Errichten oder Abbrechen 2,05 Euro.
    

Die Sätze erhöhen sich, wenn die Tätigkeiten in den Monaten November bis März durchgeführt werden, um jeweils 25 Prozent.

#### § 15  
Berechnung der Zulage

Die Zulagen nach § 13 Absatz 2 Nummer 1 und 2 werden nebeneinander gewährt; jede Zulage wird für jeden Tag nur einmal, und zwar nach dem höchsten zustehenden Satz gewährt.

#### § 16  
Zulage für Tätigkeiten an Geräten und Geräteträgern des Wetterdienstes, des Vermessungsdienstes, an Windmasten des lufthygienischen Überwachungsdienstes sowie an Tankanlagen zur eichtechnischen Überprüfung

Die §§ 13 bis 15 gelten entsprechend für Tätigkeiten an Geräten und Geräteträgern des Wetterdienstes und an trigonometrischen Beobachtungseinrichtungen des Vermessungsdienstes, an Windmasten des lufthygienischen Überwachungsdienstes sowie an Tankanlagen zur eichtechnischen Überprüfung.

### Unterabschnitt 5  
Zulage für Tätigkeiten als Notfallsanitäterin oder Notfallsanitäter

#### § 16a  
Zulage für Notfallsanitäterinnen und Notfallsanitäter

(1) Beamtinnen und Beamten des feuerwehrtechnischen Dienstes, die die Erlaubnis zur Führung der Berufsbezeichnung „Notfallsanitäterin“ oder „Notfallsanitäter“ besitzen, erhalten für die Tätigkeit als Notfallsanitäterin oder Notfallsanitäter in der Notfallrettung eine Zulage.

(2) Die Zulage beträgt 2,50 Euro je Stunde der tatsächlichen Verwendung in der Notfallrettung.
§ 10 Absatz 1 gilt entsprechend.

Abschnitt 3  
Zulagen in festen Monatsbeträgen
----------------------------------------------

#### § 17  
Entstehung des Anspruchs

(1) Der Anspruch auf die Zulage entsteht mit der tatsächlichen Aufnahme der zulageberechtigenden Tätigkeit und erlischt mit deren Beendigung, soweit in den §§ 18 bis 23 nichts anderes bestimmt ist.

(2) Besteht der Anspruch auf die Zulage nicht für einen vollen Kalendermonat und sieht die Zulagenregelung eine tageweise Abgeltung nicht vor, wird nur der Teil der Zulage gezahlt, der auf den Anspruchszeitraum entfällt.

#### § 18  
Unterbrechung der zulagenberechtigenden Tätigkeit

(1) Bei einer Unterbrechung der zulageberechtigenden Tätigkeit wird die Zulage nur weitergewährt im Fall

1.  eines Erholungsurlaubs,
    
2.  einer Dienstbefreiung oder Beurlaubung unter Fortzahlung der Dienstbezüge,
    
3.  einer Erkrankung einschließlich Rehabilitationsmaßnahme,
    
4.  einer Teilnahme an Fortbildungsveranstaltungen,
    
5.  einer Dienstreise,
    

soweit in den §§ 19 bis 23 nichts anderes bestimmt ist.
In den Fällen des Satzes 1 Nummer 2 bis 5 wird die Zulage nur weitergewährt bis zum Ende des Monats, der auf den Eintritt der Unterbrechung folgt.
Bei einer Unterbrechung der zulageberechtigenden Verwendung durch Erkrankung einschließlich Rehabilitationsmaßnahme, die auf einem Dienstunfall beruht, wird die Zulage weitergewährt bis zum Ende des sechsten Monats, der auf den Eintritt der Unterbrechung folgt.

(2) Die Befristungen nach Absatz 1 Satz 2 und 3 gelten nicht, wenn bei Beamtinnen und Beamten die Voraussetzungen des § 56 des Brandenburgischen Beamtenversorgungsgesetzes erfüllt sind.

#### § 19  
Zulagen für Wechselschichtdienst und für Schichtdienst

(1) Beamtinnen und Beamte erhalten eine Wechselschichtzulage von 102, 26 Euro monatlich und ab 1.
Januar 2015 von 115 Euro monatlich, wenn sie ständig nach einem Schichtplan (Dienstplan) eingesetzt sind.
Der Schichtplan muss einen regelmäßigen Wechsel der täglichen Arbeitszeit in Wechselschichten (wechselnde Arbeitsschichten, in denen ununterbrochen bei Tag und Nacht, werktags, sonntags und feiertags gearbeitet wird) vorsehen.
Die Beamtinnen und Beamten haben dabei in je fünf Wochen durchschnittlich mindestens 40 Dienststunden in der dienstplanmäßigen oder betriebsüblichen Nachtschicht zu leisten.
Bei Teilzeitbeschäftigung werden die in Satz 3 genannten 40 Dienststunden im gleichen Verhältnis wie die Arbeitszeit reduziert.
Zeiten eines Bereitschaftsdienstes gelten nicht als Arbeitszeit im Sinne dieser Vorschrift.

(2) Beamtinnen und Beamte erhalten, wenn sie ständig Schichtdienst zu leisten haben (Dienst nach einem Schichtplan, der einen regelmäßigen Wechsel der täglichen Arbeitszeit in Zeitabschnitten von längstens einem Monat vorsieht),

1.  eine Schichtzulage von 61,36 Euro monatlich und ab 1.
    Januar 2015 von 75 Euro monatlich, wenn sie die Voraussetzungen für eine Wechselschichtzulage nach Absatz 1 nur deshalb nicht erfüllen, weil nach dem Schichtplan eine zeitlich zusammenhängende Unterbrechung des Dienstes von höchstens 48 Stunden vorgesehen ist oder sie durchschnittlich mindestens 40 Dienststunden in der dienstplanmäßigen oder betriebsüblichen Nachtschicht nur in je sieben Wochen leisten,
    
2.  eine Schichtzulage von 46,02 Euro monatlich und ab 1.
    Januar 2015 von 55 Euro monatlich, wenn der Schichtdienst innerhalb einer Zeitspanne von mindestens 18 Stunden,
    
3.  eine Schichtzulage von 35,79 Euro monatlich und ab 1.
    Januar 2015 von 45 Euro monatlich, wenn der Schichtdienst innerhalb einer Zeitspanne von mindestens 13 Stunden geleistet wird.
    

Zeitspanne ist die Zeit zwischen dem Beginn der frühesten und dem Ende der spätesten Schicht innerhalb von 24 Stunden.
Die geforderte Stundenzahl muss im Durchschnitt an den im Schichtplan vorgesehenen Arbeitstagen erreicht werden.
Sieht der Schichtplan mehr als fünf Arbeitstage wöchentlich vor, können, falls dies günstiger ist, der Berechnung des Durchschnitts fünf Arbeitstage wöchentlich zugrunde gelegt werden.
Bei Teilzeitbeschäftigung werden die in Satz 1 Nummer 1 genannten 40 Dienststunden im gleichen Verhältnis wie die Arbeitszeit reduziert.
Absatz 1 Satz 5 gilt entsprechend.

(3) Die Absätze 1 und 2 gelten nicht, soweit der Schichtplan (Dienstplan) eine Unterscheidung zwischen Volldienst und Bereitschaftsdienst nicht vorsieht.
Sie finden keine Anwendung auf Beamtinnen und Beamte auf Widerruf im Vorbereitungsdienst; abweichend hiervon erhalten Beamtinnen und Beamte im Vorbereitungsdienst für den Krankenpflegedienst 75 Prozent der entsprechenden Beträge.
Sie finden ferner keine Anwendung auf Beamtinnen und Beamte, die im Pförtnerdienst oder Wächterdienst tätig sind oder Auslandsbesoldung (§ 52 des Brandenburgischen Besoldungsgesetzes) erhalten oder die auf Schiffen und schwimmenden Geräten tätig sind, wenn die dadurch bedingte besondere Dienstplangestaltung bereits anderweitig berücksichtigt ist.

(4) Die Erschwerniszulagen nach den Absätzen 1 und 2 werden nur zur Hälfte gewährt, wenn für denselben Zeitraum Anspruch besteht auf eine Stellenzulage nach den Nummern 7, 8, 9 und 10 der Vorbemerkungen zu den Besoldungsordnungen A und B des Brandenburgischen Besoldungsgesetzes (Sicherheitszulage, Polizeizulage, Feuerwehrzulage und Vollzugsdienstzulage).
Abweichend von Satz 1 erhalten Beamtinnen und Beamte im Krankenpflegedienst, die für den gleichen Zeitraum Anspruch auf eine Zulage nach Nummer 10 der Vorbemerkungen zu den Besoldungsordnungen A und B des Brandenburgischen Besoldungsgesetzes (Vollzugsdienstzulage) haben, die Erschwerniszulage nach Absatz 1 in Höhe von 76,69 Euro monatlich und nach Absatz 2 in voller Höhe.

#### § 20  
Zulagen für die Pflege von Kranken in Justizvollzugseinrichtungen

(1) Beamtinnen und Beamte des mittleren Dienstes im Krankenpflegedienst bei Justizvollzugseinrichtungen erhalten eine Zulage in Höhe von monatlich 61,36 Euro, wenn sie

1.  die Grund- und Behandlungspflege zeitlich überwiegend bei
    
    1.  an schweren Infektionskrankheiten erkrankten Patientinnen und Patienten (zum Beispiel Tuberkulose, Hepatitis B oder C), die wegen der Ansteckungsgefahr in besonderen Infektionsabteilungen oder Infektionsstationen untergebracht sind,
        
    2.  Kranken in geriatrischen Abteilungen oder Stationen,
        
    3.  gelähmten oder an multipler Sklerose erkrankten Patientinnen und Patienten,
        
    4.  Patientinnen und Patienten nach Transplantationen innerer Organe oder von Knochenmark,
        
    5.  an AIDS (Vollbild) erkrankten Patientinnen und Patienten,
        
    6.  Patientinnen und Patienten, bei denen Chemotherapien durchgeführt oder die mit Strahlen oder mit inkorporierten radioaktiven Stoffen behandelt werden,
        
    7.  Patientinnen und Patienten in Einheiten für Intensivmedizin,
        
    8.  ausüben oder
        
2.  ständig Kranke in psychiatrischen Abteilungen oder Stationen pflegen.
    

(2) Eine Stellenzulage nach Nummer 10 der Vorbemerkungen zu den Besoldungsordnungen A und B des Brandenburgischen Besoldungsgesetzes (Vollzugsdienstzulage) ist mit dem Betrag von 46,02 Euro anzurechnen.

#### § 21  
Zulage für besondere polizeiliche Einsätze und Einsätze beim Verfassungsschutz

(1) Eine Zulage in Höhe von 300 Euro monatlich erhalten

1.  Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte, die
    1.  in einem Spezialeinsatzkommando für besondere polizeiliche Einsätze
    2.  in einem Mobilen Einsatzkommando oder
    3.  im Personenschutz,
2.  Beamtinnen und Beamte, die
    1.  als Verdeckte Ermittlerin oder Verdeckter Ermittler unter einer verliehenen, auf Dauer angelegten veränderten Identität (Legende) oder
    2.  in der Observation beim Verfassungsschutz

verwendet werden.

(2) Eine Zulage in Höhe von 150 Euro monatlich erhalten Beamtinnen und Beamte, die in dienstlicher Funktion

1.  als Führungsperson von Verdeckten Ermittlerinnen oder Verdeckten Ermittlern nach Absatz 1 Nummer 2 Buchstabe a,
2.  zur Führung von Vertrauenspersonen im kriminalitätsverdächtigen Milieu oder
3.  als Fallführerin oder Fallführer menschlicher nachrichtendienstlicher Quellen beim Verfassungsschutz

operativ verwendet werden.

(3) Eine Zulage in Höhe von 100 Euro monatlich erhalten Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte, die

1.  in einer Mobilen Fahndungseinheit oder
2.  als Tatbeobachterin oder Tatbeobachter in einer Beweissicherungs- und Festnahmeeinheit

verwendet werden.

(4) Eine Zulage in Höhe von 60 Euro monatlich erhalten Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte, die in der Bereitschaftspolizei verwendet werden.
Die Zulage nach Satz 1 wird nicht neben einer Zulage nach den Absätzen 1 bis 3 gewährt.

(5) Die Zulage wird nicht neben einer Stellenzulage nach der Vorbemerkung Nummer 4 zu den Besoldungsordnungen A und B des Brandenburgischen Besoldungsgesetzes (Fliegerzulage) und einer Zulage nach § 22 gewährt.

#### § 22  
Zulage für Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte als fliegendes Personal

(1) Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte, die als Luftfahrzeugführerinnen oder Luftfahrzeugführer oder Flugtechnikerinnen oder Flugtechniker verwendet werden, erhalten eine Zulage.

(2) Die Zulage erhalten auch Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte, die

1.  auf Grund von Dienstvorschriften oder Dienstanweisungen als nichtständige Luftfahrzeugbesatzungsangehörige zum Mitfliegen in Luftfahrzeugen dienstlich verpflichtet sind und mindestens zehn Flüge im laufenden Kalendermonat nachweisen,
    
2.  in Erfüllung ihrer Aufgaben als Prüferin oder Prüfer von Luftfahrtgerät zum Mitfliegen verpflichtet sind
    

(Sondergruppe).
Eine Anrechnung von Flügen aus anderen Kalendermonaten und von Reiseflügen ist nicht zulässig.

(3) Die Zulage beträgt monatlich für Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte in der Verwendung als

1.  Luftfahrzeugführerin oder Luftfahrzeugführer oder Flugtechnikerin oder Flugtechniker jeweils mit Zusatzqualifikation 176,40 Euro,
    
2.  Luftfahrzeugführerin oder Luftfahrzeugführer oder Flugtechnikerin oder Flugtechniker jeweils ohne Zusatzqualifikation 132,94 Euro,
    
3.  Angehörige der Sondergruppe (Absatz 2) bei zehn oder mehr Flügen im laufenden Kalendermonat 46,02 Euro.
    

Werden im laufenden Kalendermonat weniger als zehn, jedoch mindestens fünf Flüge nachgewiesen, vermindert sich die Zulage für jeden fehlenden Flug um 4,60 Euro.
§ 18 findet keine Anwendung.
Zusatzqualifikation im Sinne des Satzes 1 Nummer 1 sind insbesondere Instrumentenflugberechtigung sowie die erworbene Ausbildung im Umgang mit Bildverstärkerbrille oder Wärmebildkamera.

### § 23  
Zulage für die Beseitigung von Munition aus den Weltkriegen

(1) Beamtinnen und Beamte erhalten, wenn sie als Räumgruppenleiterin oder Räumgruppenleiter bei besonderen Entgiftungsarbeiten eingesetzt werden, eine Zulage.
Die Zulage beträgt monatlich 586,47 Euro, wenn die Beamtinnen oder die Beamten 120 oder mehr Stunden im Kalendermonat im unmittelbaren Gefahrenbereich tätig sind.
Die Zulage verringert sich für jede Stunde, die an 120 Stunden fehlt, um ein Einhundertzwanzigstel.

(2) Beamtinnen und Beamte erhalten, wenn sie als Feuerwerkerinnen, Feuerwerker oder als Hilfskräfte in Munitionsräumgruppen zur Beseitigung von Munition und anderen Sprengkörpern eingesetzt werden, eine Zulage.
Die Zulage beträgt monatlich höchstens 398,81 Euro für die Feuerwerkerinnen und Feuerwerker, sofern sie selbst Munition und Sprengkörper entschärfen, für die Hilfskräfte höchstens 281,21 Euro.
Die Beamtinnen und Beamten müssen 135 oder mehr Arbeitsstunden im Kalendermonat im unmittelbaren Gefahrenbereich tätig sein.
Sinkt die Zahl der Arbeitsstunden im unmittelbaren Gefahrenbereich im Kalendermonat um mehr als 30, so verringert sich die Zulage für jede Stunde, die an 135 Stunden fehlt, um ein Einhundertfünfunddreißigstel.

(3) Eine Tätigkeit im unmittelbaren Gefahrenbereich nach Absatz 2 ist das Suchen, Prüfen, Entfernen, Entschärfen, Sprengen oder Zerlegen von Munition oder Munitionsteilen sowie deren Transport.

(4) Für die Entschärfung von Bomben mit Langzeitzündern oder für sonstige besonders schwierige Entschärfungen mit außergewöhnlichem Gefahrenmoment oder für den Transport nicht entschärfter Bomben mit Langzeitzündern und Ausbausperre kann die Zulage nach Absatz 2 um einen Betrag bis zu 255,65 Euro erhöht werden.

Abschnitt 4  
Schlussvorschrift
-------------------------------

#### § 24  
Inkrafttreten

Diese Verordnung tritt vorbehaltlich des Satzes 2 mit Wirkung vom 1.
Januar 2014 in Kraft.
§ 21 Absatz 2 tritt mit Wirkung vom 1.
August 2014 in Kraft.

Potsdam, den 10.
September 2014

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister der Finanzen

Christian Görke