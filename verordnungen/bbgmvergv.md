## Brandenburgische Verordnung über die Gewährung von Mehrarbeitsvergütung für Beamtinnen und Beamte (Brandenburgische Mehrarbeitsvergütungsverordnung - BbgMVergV)

Auf Grund des § 46 des Brandenburgischen Besoldungsgesetzes vom 20.
November 2013 (GVBl. I Nr. 32 S. 2, Nr. 34), der durch Artikel 1 des Gesetzes vom 20.
Dezember 2016 (GVBl. I Nr. 32) geändert worden ist, verordnet die Landesregierung:

#### § 1 Geltungsbereich

(1) Diese Verordnung gilt für die Beamtinnen und Beamten des Landes, der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts; ausgenommen sind die Ehrenbeamtinnen und Ehrenbeamten.

(2) Vergütungen für Mehrarbeit sowie Ausgleichszahlungen zur finanziellen Abgeltung von Arbeitszeitguthaben dürfen nur nach Maßgabe dieser Verordnung gezahlt werden.

#### § 2 Gewährung von Mehrarbeitsvergütung

(1) Beamtinnen und Beamten mit Dienstbezügen in Besoldungsgruppen der Besoldungsordnung A, deren Dienstverrichtung messbar ist, kann in folgenden Bereichen eine Vergütung für Mehrarbeit gezahlt werden:

1.  in der Krankenversorgung,
2.  im Polizeivollzugsdienst,
3.  im Justizvollzugsdienst,
4.  im Einsatzdienst der Feuerwehr,
5.  im Schuldienst als Lehrkraft für Unterrichtstätigkeit,
6.  im Bereich der Hochschulen als Lehrkraft für Unterrichtstätigkeit.

(2) Absatz 1 gilt entsprechend auch in anderen Bereichen, soweit Mehrarbeit geleistet wird im Rahmen eines

1.  Dienstes in Bereitschaft,
2.  Schichtdienstes,
3.  allgemein geltenden besonderen Dienstplanes, wenn ihn die Eigenart des Dienstes erfordert,
4.  Dienstes, der ausschließlich aus gleichartigen, im Wesentlichen die gleiche Arbeitszeit erfordernden Arbeitsvorgängen besteht, für die Richtwerte eingeführt sind,
5.  Dienstes zur Herbeiführung eines im öffentlichen Interesse liegenden unaufschiebbaren und termingebundenen Ergebnisses.

(3) Eine Mehrarbeitsvergütung wird nicht gewährt neben

1.  einer Auslandsbesoldung nach § 52 des Brandenburgischen Besoldungsgesetzes,
2.  einer Zulage nach Nummer 6 der Vorbemerkungen zu den Besoldungsordnungen A und B der Anlage 1 des Brandenburgischen Besoldungsgesetzes,
3.  einer Zulage nach Nummer 7 der Vorbemerkungen zu den Besoldungsordnungen A und B der Anlage 1 des Brandenburgischen Besoldungsgesetzes.

Beamtinnen und Beamten des Observations- und Ermittlungsdienstes, die überwiegend im Außendienst eingesetzt sind, wird eine Mehrarbeitsvergütung neben der in Satz 1 Nummer 2 oder Nummer 3 genannten Zulagen gewährt.
Im Übrigen erhalten Beamtinnen und Beamte der Besoldungsgruppen A 5 bis A 8 neben den in Satz 1 Nummer 2 oder Nummer 3 genannten Zulagen eine Mehrarbeitsvergütung in Höhe des die Zulage übersteigenden Betrages.

(4) Ist die Gewährung einer Mehrarbeitsvergütung neben einer Zulage ganz oder teilweise ausgeschlossen, gilt dies auch für eine nach Wegfall der Zulage gewährte Ausgleichszulage, solange diese noch nicht bis zur Hälfte aufgezehrt ist.

#### § 3 Voraussetzungen

(1) Die Mehrarbeitsvergütung wird nur gewährt, wenn die Mehrarbeit von Beamtinnen oder Beamten geleistet wurde, für die beamtenrechtliche Arbeitszeitregelungen gelten, und sie

1.  schriftlich angeordnet oder genehmigt wurde,
2.  aus zwingenden dienstlichen Gründen nicht durch Zeitausgleich innerhalb der in § 76 Absatz 2 des Landesbeamtengesetzes genannten Frist ausgeglichen werden kann und
3.  die sich aus der regelmäßigen Arbeitszeit ergebende Arbeitszeit um mehr als fünf Stunden im Kalendermonat (Mindeststundenzahl) übersteigt.

(2) Soweit nur während eines Teils eines Kalendermonats Dienst geleistet wurde, gilt die Mindeststundenzahl für die jeweils anteilige Arbeitszeit.
Sie verkürzt sich bei Teilzeitbeschäftigung entsprechend dem Umfang der Teilzeitbeschäftigung.

(3) Besteht keine feste tägliche Arbeitszeit, so dass eine Mehrarbeit nicht für den einzelnen Arbeitstag, sondern nur aufgrund der regelmäßigen wöchentlichen Arbeitszeit für eine volle Woche ermittelt werden kann, so ist Mehrarbeit innerhalb einer Kalenderwoche, wenn diese zum Teil auf den laufenden, zum Teil auf den folgenden Kalendermonat fällt, dem folgenden Kalendermonat zuzurechnen.

#### § 4 Höhe der Vergütung

(1) Die Vergütung beträgt je Stunde bei Beamtinnen und Beamten in den Besoldungsgruppen

1.

A 5 bis A 8

ab 1.
Januar 2019 15,30 Euro, ab 1.
Januar 2020 15,87 Euro und ab 1. Januar 2021 16,09 Euro,

2.

A 9 bis A 12

ab 1.
Januar 2019 20,95 Euro, ab 1.
Januar 2020 21,73 Euro und ab 1. Januar 2021 22,03 Euro,

3.

A 13 bis A 16

ab 1.
Januar 2019 28,88 Euro, ab 1.
Januar 2020 29,95 Euro und ab 1. Januar 2021 30,37 Euro.

(2) Bei Mehrarbeit im Schuldienst und im Bereich der Hochschulen beträgt die Vergütung abweichend von Absatz 1 für Lehrkräfte

1.  des gehobenen Dienstes, soweit sie nicht unter die Nummern 2 und 3 fallen, ab 1. Januar 2019 19,52 Euro, ab 1.
    Januar 2020 20,24 Euro und ab 1.
    Januar 2021 20,52 Euro,
2.  des gehobenen Dienstes, deren Eingangsämter mindestens der Besoldungsgruppe A 12 zugeordnet sind, ab 1.
    Januar 2019 24,16 Euro, ab 1.
    Januar 2020 25,05 Euro und ab 1. Januar 2021 25,40 Euro,
3.  des gehobenen Dienstes, deren Eingangsämter der Besoldungsgruppe A 13 zugeordnet sind, ab 1.
    Januar 2019 28,68 Euro, ab 1.
    Januar 2020 29,74 Euro und ab 1. Januar 2021 30,16 Euro,
4.  des höheren Dienstes an Gymnasien und an berufsbildenden Schulen und des höheren Dienstes an Hochschulen, ab 1.
    Januar 2019 33,52 Euro, ab 1.
    Januar 2020 34,76 Euro und ab 1. Januar 2021 35,25 Euro.

(3) Die in den Absätzen 1 und 2 enthaltenen Vergütungssätze gelten nur für Mehrarbeit, die nach dem Inkrafttreten dieser Verordnung geleistet wird.

(4) Teilzeitbeschäftigte Beamtinnen und Beamte erhalten bis zur Erreichung der regelmäßigen Arbeitszeit von Vollzeitbeschäftigten für jede Stunde Mehrarbeit eine Vergütung in Höhe des auf eine Stunde entfallenden Anteils der Besoldung entsprechender Vollzeitbeschäftigter.
Zur Ermittlung der auf eine Stunde entfallenden Besoldung sind die monatlichen Bezüge entsprechender Vollzeitbeschäftigter durch das 4,348fache ihrer regelmäßigen wöchentlichen Arbeitszeit zu teilen.
Bezüge, die nicht der anteiligen Kürzung nach § 6 des Brandenburgischen Besoldungsgesetzes unterliegen, bleiben unberücksichtigt.
Mehrarbeit, die über die Arbeitszeit von entsprechenden Vollzeitbeschäftigten hinausgeht, wird nach den Absätzen 1 bis 3 vergütet.

#### § 5 Berechnung

(1) Als Mehrarbeitsstunde im Sinne der §§ 3 und 4 Absatz 1 gilt die volle Zeitstunde.
Hiervon abweichend wird eine Stunde Dienst in Bereitschaft nur entsprechend dem Umfang der erfahrungsgemäß bei der betreffenden Tätigkeit durchschnittlich anfallenden Inanspruchnahme berücksichtigt; dabei ist schon die Ableistung eines Dienstes in Bereitschaft als solche in jeweils angemessenem Umfang anzurechnen.

(2) Bei Mehrarbeit im Schuldienst und im Bereich der Hochschulen gelten bei Anwendung des § 3 Absatz 1 Nummer 3 drei Unterrichtsstunden als fünf Stunden.
§ 3 Absatz 2 gilt entsprechend.

(3) Ergibt sich bei der monatlichen Berechnung der Mehrarbeitsstunden ein Bruchteil einer Stunde, so werden 30 Minuten und mehr auf eine volle Stunde aufgerundet, weniger als 30 Minuten bleiben unberücksichtigt.

#### § 6 Ausgleichszahlung zur Abgeltung von Arbeitszeitguthaben

(1) Wenn bei einer langfristigen ungleichmäßigen Verteilung der Arbeitszeit ein Ereignis eintritt, durch das der vorgesehene Zeitausgleich nicht oder nur teilweise möglich ist, ist eine Ausgleichszahlung zu gewähren:

1.  Bei Vollzeitbeschäftigung, bei der über die regelmäßige Arbeitszeit hinaus Dienst geleistet wurde, der für Beamtinnen und Beamte geltende Mehrarbeitsvergütungssatz.
    Maßgebend sind die beim Eintritt des Ereignisses geltenden Vergütungssätze.
2.  Bei Teilzeitbeschäftigung die Bezüge in Höhe des Unterschiedsbetrags zwischen den gezahlten Bezügen und der Besoldung, die nach dem Anteil der tatsächlichen Beschäftigung zugestanden hätte.

(2) In der Arbeitsphase bleiben Zeiten ohne Dienstbezüge, soweit sie insgesamt sechs Monate überschreiten, unberücksichtigt.

(3) Der Anspruch richtet sich gegen den Dienstherrn, bei dem die auszugleichende Arbeitszeit erbracht wurde.

#### § 7 Inkrafttreten

Diese Verordnung tritt am ersten Tag des auf die Verkündung folgenden Kalendermonats in Kraft.

Potsdam, den 9.
April 2019

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

  
Der Minister der Finanzen

Christian Görke