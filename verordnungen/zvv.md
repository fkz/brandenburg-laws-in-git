## Verordnung über die zentrale Vergabe von Studienplätzen durch die Stiftung für Hochschulzulassung (Zentrale Vergabeverordnung - ZVV)

Auf Grund des § 11 Absatz 2 des Brandenburgischen Hochschulgesetzes vom 18. Dezember 2008 (GVBl.
I S. 318) in Verbindung mit Artikel 12 des Staatsvertrages über die Errichtung einer gemeinsamen Einrichtung für Hochschulzulassung vom 5.
Juni 2008 (GVBl.
I S. 310) verordnet die Ministerin für Wissenschaft, Forschung und Kultur:

Inhaltsübersicht
----------------

### Abschnitt 1  
Allgemeines

[§ 1    Umfang der zentralen Studienplatzvergabe](#1)  
[§ 2    Einbezogener Personenkreis](#2)

### Abschnitt 2  
Antragstellung

[§ 3    Frist und Form der Anträge, Ausschluss vom Verfahren](#3)  
[§ 4    Beteiligung am Verfahren](#4)  
[§ 5    Besonderer öffentlicher Bedarf](#5)

### Abschnitt 3  
Quotierung und Verfahrensablau**f**

[§ 6    Quotierung](#6)  
[§ 7    Ablauf des zentralen Vergabeverfahrens](#7)  
[§ 8    Zulassungsbescheid](#8)  
[§ 9    Abschluss des zentralen Vergabeverfahrens](#9)  
[§ 10  Auswahlverfahren der Hochschulen](#10)

### Abschnitt 4  
Quoten und Auswahlkriterien des zentralen Vergabeverfahrens

[§ 11  Auswahl in der Abiturbestenquote](#11)  
[§ 12  Landesquoten](#12)  
[§ 13  Zurechnung zu den Landesquoten](#13)  
[§ 14  Auswahl nach Wartezeit](#14)  
[§ 15  Auswahl nach Härtegesichtspunkten](#15)  
[§ 16  Auswahl der Bewerberinnen und Bewerber mit besonderer Hochschulzugangsberechtigung](#16)  
[§ 17  Auswahl für ein Zweitstudium](#17)  
[§ 18  Nachrangige Auswahlkriterien](#18)

### Abschnitt 5  
Auswahl nach einem Dienst auf Grund früherer Zulassung

[§ 19  Auswahl nach einem Dienst auf Grund früheren Zulassungsanspruchs](#19)

### Abschnitt 6  
Verteilung auf die Studienorte

[§ 20  Verteilung der in der Abiturbestenquote Ausgewählten auf die Studienorte](#20)  
[§ 21  Verteilung der nach § 7 Absatz 3 Ausgewählten auf die Studienorte](#21)

### Abschnitt 7  
Vergabe von Teilstudienplätzen

[§ 22  Teilstudienplätze](#22)

### Abschnitt 8  
Schlussbestimmungen

[§ 23  Inkrafttreten, Außerkrafttreten](#23)

[Anlage 1     In das zentrale Vergabeverfahren einbezogene Studiengänge](#A1)  
[Anlage 2     Ermittlung der Durchschnittsnote](#A2)  
[Anlage 3     Ermittlung der Messzahl bei der Auswahl für ein Zweitstudium](#A3)  
[Anlage 4     Zuordnung der Kreise und kreisfreien Städte zu den Studienorten](#A4)  
[Anlage 5     Ermittlung der Punktzahl der Gesamtqualifikation](#A5)

### Abschnitt 1  
Allgemeines

#### § 1   
Umfang der zentralen Studienplatzvergabe

Die Stiftung für Hochschulzulassung (Stiftung) vergibt die Studienplätze des ersten Fachsemesters der in das zentrale Vergabeverfahren einbezogenen Studiengänge, soweit sie nicht von den Hochschulen vergeben werden.
Die in das zentrale Vergabeverfahren einbezogenen Studiengänge sind in Anlage 1 aufgeführt.

#### § 2   
Einbezogener Personenkreis

Die Studienplätze werden an Deutsche sowie an ausländische Staatsangehörige oder Staatenlose, die im Sinne dieser Verordnung Deutschen gleichgestellt sind, vergeben.
Deutschen gleichgestellt sind hiernach:

1.  Staatsangehörige eines anderen Mitgliedstaates der Europäischen Union oder eines anderen Vertragsstaates des Abkommens über den Europäischen Wirtschaftsraum,
    
2.  in der Bundesrepublik Deutschland wohnende Kinder von Staatsangehörigen eines anderen Mitgliedstaates der Europäischen Union oder von Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum, sofern diese Staatsangehörigen in der Bundesrepublik Deutschland beschäftigt sind oder gewesen sind,
    
3.  in der Bundesrepublik Deutschland wohnende andere Familienangehörige im Sinne des Artikels 2 Nummer 2 der Richtlinie 2004/38/EG des Europäischen Parlaments und des Rates vom 29. April 2004 (ABl. L 158 vom 30.4.2004, S. 77) von Staatsangehörigen eines anderen Mitgliedstaates der Europäischen Union oder von Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum, sofern diese Staatsangehörigen in der Bundesrepublik Deutschland beschäftigt sind, sowie
    
4.  sonstige ausländische Staatsangehörige oder Staatenlose, die eine in der Bundesrepublik Deutschland oder an einer deutschen Auslandsschule erworbene Hochschulzugangsberechtigung, die nicht ausschließlich nach ausländischem Recht erworben wurde (deutsche Hochschulzugangsberechtigung), besitzen.
    

Wer nach Satz 2 Deutschen gleichgestellt ist, wird nach den für Deutsche geltenden Bestimmungen am Vergabeverfahren beteiligt.

### Abschnitt 2  
Antragstellung

#### § 3   
Frist und Form der Anträge, Ausschluss vom Verfahren

(1) Zulassungsanträge richten sich zugleich auf die Teilnahme am zentralen Vergabeverfahren und auf die Teilnahme an den Auswahlverfahren der Hochschulen.

(2) Der Zulassungsantrag muss

1.  für das Sommersemester bis zum 15.
    Januar,
    
2.  für das Wintersemester, wenn die Hochschulzugangsberechtigung vor dem 16.
    Januar erworben wurde, bis zum 31.
    Mai, andernfalls bis zum 15.
    Juli
    

bei der Stiftung eingegangen sein (Ausschlussfristen).
Bei Bewerbungen für ein Zweitstudium gilt der Zeitpunkt des Abschlusses des Erststudiums als Zeitpunkt des Erwerbs der Hochschulzugangsberechtigung nach Satz 1.

(3) Im Zulassungsantrag ist ein Studiengang zu wählen.
Für die Vergabe der Studienplätze in der Abiturbestenquote können bis zu sechs Studienorte in einer Reihenfolge gewählt werden.
Für die Vergabe der Studienplätze in den weiteren durch die Stiftung vergebenen Quoten sind gewünschte Studienorte in einer Reihenfolge zu wählen.
Für das Auswahlverfahren der Hochschulen können bis zu sechs Studienorte in einer Reihenfolge gewählt werden.
Studiengangwunsch und Ortswünsche können nach Ablauf der Fristen nach Absatz 2 Satz 1 nicht mehr geändert werden.

(4) Im Zulassungsantrag hat die Bewerberin oder der Bewerber anzugeben, ob sie oder er

1.  für den gewählten Studiengang im Zeitpunkt der Antragstellung an einer deutschen Hochschule als Studentin oder Student eingeschrieben ist,
    
2.  bereits an einer deutschen Hochschule ein Studium abgeschlossen hat oder als Studentin oder Student eingeschrieben war, gegebenenfalls für welche Zeit.
    

(5) Stellt eine Bewerberin oder ein Bewerber mehrere Zulassungsanträge, wird nur über den letzten fristgerecht eingegangenen entschieden.
Anträge, die nach dieser Verordnung ergänzend zum Zulassungsantrag gestellt werden können, sind mit dem Zulassungsantrag zu stellen, es sei denn, der Antrag stützt sich auf einen zum Wintersemester vor dem 16.
Juli nach Ablauf der Bewerbungsfrist eingetretenen Sachverhalt.

(6) Die Stiftung bestimmt die Form des Zulassungsantrags und der Anträge nach Absatz 5 Satz 2.
Sie bestimmt auch die Unterlagen, die den Anträgen mindestens beizufügen sind, sowie deren Form.
Sie ist nicht verpflichtet, den Sachverhalt von Amts wegen zu ermitteln.
Der Zulassungsantrag ist der Stiftung in Form des elektronisch ausgefüllten Antragsformulars vor Ablauf der in Absatz 2 Satz 1 genannten Fristen elektronisch zu übermitteln; das ausgedruckte und unterschriebene Antragsformular muss der Stiftung samt den erforderlichen Unterlagen vor Ablauf der in Absatz 7 Satz 2 genannten Fristen zugegangen sein.
Bei der elektronischen Übermittlung hat die Stiftung unter Anwendung von Verschlüsselungsverfahren dem jeweiligen Stand der Technik entsprechende Maßnahmen zu treffen, die die Vertraulichkeit und Unversehrtheit der Daten gewährleisten.
Bewerberinnen und Bewerbern, die glaubhaft machen, dass ihnen die elektronische Antragstellung nicht zumutbar ist, wird gestattet, den Zulassungsantrag schriftlich zu stellen; Absatz 2 Satz 1 bleibt unberührt.
Die Bewerberinnen und Bewerber übersenden den nach Absatz 3 Satz 4 gewählten Hochschulen die jeweils für deren Auswahlverfahren benötigten Unterlagen; das Nähere regeln die Hochschulen durch Satzung.

(7) Wer die Bewerbungsfristen nach Absatz 2 Satz 1 versäumt, ist vom Vergabeverfahren ausgeschlossen.
Ist der Zulassungsantrag fristgerecht gestellt worden, können nachträglich eingereichte Unterlagen

1.  für das Sommersemester bis zum 31.
    Januar,
    
2.  für das Wintersemester, wenn die Hochschulzugangsberechtigung vor dem 16.
    Januar erworben wurde, bis zum 15.
    Juni, andernfalls bis zum 31.
    Juli
    

berücksichtigt werden (Ausschlussfristen).
Entspricht der Zulassungsantrag nicht den rechtlichen Mindestanforderungen oder fehlen bei Ablauf der Fristen nach Satz 2 notwendige Unterlagen oder nach Absatz 4 erforderliche Angaben, gilt Satz 1 entsprechend.

#### § 4  
Beteiligung am Verfahren

(1) Am Vergabeverfahren wird nur beteiligt, wer bei der Bewerbung für das Sommersemester bis zum 15.
Januar, bei der Bewerbung für das Wintersemester bis zum 15.
Juli die Hochschulzugangsberechtigung für den gewählten Studiengang erworben hat.
Werden mehrere einschlägige Hochschulzugangsberechtigungen vorgelegt, wird dem Zulassungsantrag die zuerst erworbene zugrunde gelegt.
Die Feststellung der Hochschulzugangsberechtigung von Bewerberinnen und Bewerbern mit ausländischen Vorbildungsnachweisen erfolgt, wenn keine Anerkennungsentscheidung der Zeugnisanerkennungsstelle eines Landes vorliegt, für den angestrebten Studiengang durch die Stiftung auf der Grundlage der Bewertungsvorschläge der Zentralstelle für ausländisches Bildungswesen.

(2) Wer bei der Bewerbung für das Sommersemester bis zum 15.
Januar, bei der Bewerbung für das Wintersemester bis zum 15.
Juli das 55.
Lebensjahr vollendet hat, wird am Vergabeverfahren nur beteiligt, wenn für das beabsichtigte Studium unter Berücksichtigung der persönlichen Situation der Bewerberin oder des Bewerbers schwerwiegende wissenschaftliche oder berufliche Gründe sprechen.

(3) Vom Vergabeverfahren ist ausgeschlossen, wer für den gewählten Studiengang im Zeitpunkt der Antragstellung an einer in der Bundesrepublik Deutschland gelegenen Hochschule (deutsche Hochschule) als Studentin oder Student eingeschrieben ist; dies gilt nicht im Fall der Einschreibung für einen Teilstudienplatz oder bei Nachweis von Gründen für einen Studienortwechsel nach § 15 Satz 2.
Wer in dem gewählten Studiengang bereits an einer deutschen Hochschule eingeschrieben war, kann seine Zulassung in diesem Studiengang sowohl im Verfahren der Stiftung für einen Studienplatz des ersten Fachsemesters als auch nach Maßgabe der Vorschriften für die Zulassung zu höheren Fachsemestern beantragen.

#### § 5  
Besonderer öffentlicher Bedarf

Das Bundesministerium der Verteidigung teilt der Stiftung für das Sommersemester bis zum 15. Januar, für das Wintersemester bis zum 15.
Juli (Ausschlussfristen) unter Angabe einer Reihenfolge mit, wen es für die Studienplätze benennt, die dem Sanitätsoffizierdienst der Bundeswehr vorbehalten sind.
Wer einen Studienplatz aus dieser Quote erhält, kann nicht nach anderen Bestimmungen dieser Verordnung zugelassen werden.

### Abschnitt 3  
Quotierung und Verfahrensablauf

#### § 6  
Quotierung

(1) Von den festgesetzten Zulassungszahlen sind je Studienort vorweg abzuziehen:

1.  für die Zulassung von ausländischen Staatsangehörigen oder Staatenlosen, die nicht nach § 2 Satz 2 Deutschen gleichgestellt sind, bis zu 5 Prozent,
    
2.  für die Zulassung im Sanitätsoffizierdienst der Bundeswehr
    
    1.  2,2 Prozent im Studiengang Medizin,
        
    2.  0,5 Prozent im Studiengang Pharmazie,
        
    3.  0,1 Prozent im Studiengang Tiermedizin,
        
    4.  1,4 Prozent im Studiengang Zahnmedizin.
        
    
    Die von der jährlichen Aufnahmekapazität auf die Quote nach Satz 1 Nummer 1 entfallenden Studienplätze können nach Maßgabe des Landesrechts zu einem Zulassungstermin (Wintersemester oder Sommersemester) vergeben werden; § 7 Absatz 1 bleibt unberührt.
    Für die Quoten nach Satz 1 Nummer 2 gelten zusammen für ein Wintersemester und das darauf folgende Sommersemester bundesweit folgende Obergrenzen:
    

> 1.  im Studiengang Medizin: 220 Studienplätze,
>     
> 2.  im Studiengang Pharmazie: 12 Studienplätze,
>     
> 3.  im Studiengang Tiermedizin: 2 Studienplätze,
>     
> 4.  im Studiengang Zahnmedizin: 30 Studienplätze.
>     

(2) Darüber hinaus sind von der Gesamtzahl der festgesetzten Zulassungszahlen vorweg abzuziehen:

1.  2 Prozent für Fälle außergewöhnlicher Härte,
    
2.  0,2 Prozent für die Auswahl der Bewerberinnen und Bewerber mit besonderer Hochschulzugangsberechtigung,
    
3.  3 Prozent für die Auswahl für ein Zweitstudium.
    

Der Anteil der für Bewerberinnen und Bewerber mit besonderer Hochschulzugangsberechtigung bei der Verfahrensdurchführung zur Verfügung stehenden Studienplätze an der Gesamtzahl der Studienplätze darf nicht größer sein als ihr Anteil an der Bewerbergesamtzahl.
Für jede Quote nach Satz 1 muss mindestens ein Studienplatz zur Verfügung gestellt werden.

(3) Die Zahl der in der Abiturbestenquote zu vergebenden Studienplätze beträgt je Studienort 20 Prozent der Zahl der nach Abzug der Quoten nach den Absätzen 1 und 2 verbleibenden Studienplätze.

(4) Die Zahl der durch das Auswahlverfahren der Hochschulen zu vergebenden Studienplätze beträgt je Studienort 60 Prozent der Zahl der nach Abzug der Quoten nach den Absätzen 1 und 2 verbleibenden Studienplätze.

(5) Die verbleibenden Studienplätze, vermindert um die Zahl der nach einem Dienst auf Grund früheren Zulassungsanspruchs Auszuwählenden, die nicht in der Abiturbestenquote oder im Auswahlverfahren der Hochschulen zugelassen worden waren, werden nach Wartezeit vergeben.

(6) In den Quoten nach Absatz 1 Satz 1 Nummer 2 und Absatz 2 verfügbar gebliebene Studienplätze werden der Quote nach Absatz 5 hinzugerechnet.
In den Quoten nach Absatz 1 Satz 1 Nummer 1, Absatz 3 und 5 verfügbar gebliebene Studienplätze werden der Quote nach Absatz 4 hinzugerechnet.

#### § 7  
Ablauf des zentralen Vergabeverfahrens

(1) Ein Vergabeverfahren umfasst jeweils die auf einen Zulassungstermin (Sommersemester oder Wintersemester) bezogene Vergabe von Studienplätzen.

(2) Nach der Zulassung der nach § 5 Satz 1 Benannten trifft die Stiftung die Auswahl in der Abiturbestenquote nach § 11 und lässt die ausgewählten Bewerberinnen und Bewerber nach § 20 zu.

(3) Danach vergibt die Stiftung die Studienplätze der Quoten nach § 6 Absatz 2 Satz 1 und Absatz 5.
An der Vergabe der Studienplätze dieser Quoten wird nicht beteiligt, wer in der Abiturbestenquote zugelassen worden ist.
Wer in einer oder mehreren dieser Quoten zu berücksichtigen ist, wird auf allen entsprechenden Ranglisten geführt.
Bei der Auswahl werden die Ranglisten in folgender Reihenfolge berücksichtigt:

1.  Auswahl nach einem Dienst auf Grund früheren Zulassungsanspruchs nach § 19, sofern die frühere Zulassung weder in der Abiturbestenquote noch im Auswahlverfahren der Hochschulen erfolgt ist,
    
2.  Auswahl der Bewerberinnen und Bewerber mit besonderer Hochschulzugangsberechtigung nach § 16 und Auswahl für ein Zweitstudium nach § 17,
    
3.  Auswahl nach Wartezeit nach § 14,
    
4.  Auswahl nach Härtegesichtspunkten nach § 15.
    

Die ausgewählten Bewerberinnen und Bewerber lässt die Stiftung nach § 21 zu.
Bei der Auswahl und Verteilung kann die Stiftung durch Überbuchung der Zulassungszahlen berücksichtigen, dass Studienplätze voraussichtlich nicht angenommen werden.

(4) Wer an der Vergabe der Studienplätze nach Absatz 2 oder Absatz 3 beteiligt, aber nicht zugelassen worden ist, erhält von der Stiftung einen Ablehnungsbescheid.

#### § 8  
Zulassungsbescheid

Im Zulassungsbescheid teilt die Stiftung mit, bis wann sich die oder der Zugelassene bei der im Zulassungsbescheid genannten Hochschule einzuschreiben hat.
Ist die Einschreibung bis zu diesem Termin nicht beantragt worden oder lehnt die Hochschule eine Einschreibung ab, weil sonstige Einschreibvoraussetzungen nicht vorliegen, wird der Zulassungsbescheid unwirksam; auf diese Rechtsfolge ist im Bescheid hinzuweisen.
Die Sätze 1 und 2 gelten entsprechend, wenn der Zulassungsbescheid von der Hochschule erlassen wird.

#### § 9   
Abschluss des zentralen Vergabeverfahrens

Mit der Vergabe der Studienplätze nach § 7 Absatz 3 ist das zentrale Vergabeverfahren abgeschlossen.
Studienplätze in den von der Stiftung vergebenen Quoten, die nach Abschluss des zentralen Vergabeverfahrens noch verfügbar sind oder wieder verfügbar werden, werden im Auswahlverfahren der Hochschulen vergeben.

#### § 10  
Auswahlverfahren der Hochschulen

(1) Das Auswahlverfahren der Hochschulen wird von den einzelnen Hochschulen durchgeführt.
Die Hochschulen sind in diesem Verfahren nicht verpflichtet, den Sachverhalt von Amts wegen zu ermitteln.
Hochschulen können die Stiftung damit beauftragen, Zulassungs- sowie Ablehnungsbescheide zu erstellen und im Namen und Auftrag der Hochschule zu versenden.
Hochschulen können bei der Durchführung ihrer Auswahlverfahren durch Überbuchung der Zulassungszahlen berücksichtigen, dass Studienplätze voraussichtlich nicht besetzt werden.

(2) Die Auswahlentscheidung der Hochschulen ist zu treffen

1.  nach dem Grad der Qualifikation (Durchschnittsnote),
    
2.  nach gewichteten Einzelnoten der Qualifikation, die über die fachspezifische Eignung Auskunft geben,
    
3.  nach dem Ergebnis eines fachspezifischen Studierfähigkeitstests,
    
4.  nach der Art der Berufsausbildung oder Berufstätigkeit,
    
5.  nach dem Ergebnis eines von der Hochschule zu führenden Gesprächs mit den Bewerberinnen und Bewerbern, das Aufschluss über die Motivation der Bewerberin oder des Bewerbers und über die Identifikation mit dem gewählten Studium und dem angestrebten Beruf geben sowie zur Vermeidung von Fehlvorstellungen über die Anforderungen des Studiums dienen soll oder
    
6.  auf Grund einer Verbindung von Maßstäben nach den Nummern 1 bis 5.
    

Bei der Auswahlentscheidung muss dem Grad der Qualifikation ein maßgeblicher Einfluss gegeben werden.

(3) Die Zahl der Teilnehmerinnen und Teilnehmer an einem Auswahlgespräch kann bis auf das Dreifache der Zahl der hiernach zu vergebenden Studienplätze begrenzt werden.
In diesem Fall entscheidet die Hochschule über die Teilnahme nach einem der in Absatz 2 Nummer 1 bis 3 genannten Maßstäbe sowie nach dem Grad der Ortspräferenz oder nach einer Verbindung dieser Maßstäbe.

(4) Die Hochschulen regeln die Einzelheiten des Auswahlverfahrens durch Satzung.
Die Satzung ist nach Genehmigung durch die Präsidentin oder den Präsidenten der für die Hochschulen zuständigen obersten Landesbehörde vor Veröffentlichung anzuzeigen.

(5) Am Auswahlverfahren der Hochschulen wird nicht beteiligt, wer

1.  unter die Quoten nach § 6 Absatz 1 Satz 1 oder Absatz 2 Satz 1 Nummer 2 und 3 fällt oder
    
2.  im Zulassungsantrag keinen gültigen Studienortwunsch für dieses Verfahren genannt hat oder
    
3.  nach § 7 Absatz 2 oder Absatz 3 Satz 5 von der Stiftung zugelassen worden ist.
    

Liegen die Voraussetzungen nach Satz 1 Nummer 2 vor, erlässt die Stiftung für das Auswahlverfahren der Hochschulen im eigenen Namen einen Ausschlussbescheid.

(6) Die Stiftung teilt den Hochschulen für das Sommersemester bis zum 10.
Februar, für das Wintersemester bis zum 10.
August mit, welche Bewerberinnen und Bewerber an ihren Auswahlverfahren zu beteiligen sind, und übermittelt dabei studiengangweise folgende Angaben:

1.  Namen und Anschrift sowie Tag und Ort der Geburt,
    
2.  die Ortspräferenz für die jeweilige Hochschule,
    
3.  die nach § 11 Absatz 3 bis 5 ermittelte Durchschnittsnote,
    
4.  die nach § 14 ermittelte Wartezeit,
    
5.  Einzelnoten der Hochschulzugangsberechtigung,
    
6.  das Ergebnis eines fachspezifischen Studierfähigkeitstests, sofern es der Stiftung vorliegt,
    
7.  die Art einer Berufsausbildung und die Dauer einer Berufstätigkeit oder eines Praktikums,
    
8.  die Erfüllung der Voraussetzungen für eine erneute Zulassung nach § 19 Absatz 2 Satz 2.
    

(7) Soweit der Stiftung Verfahrensergebnisse der Hochschulen in Form von Ranglisten für das Sommersemester bis zum 25.
Februar, für das Wintersemester bis zum 25.
August vorliegen, werden Bewerberinnen und Bewerber, die nach diesen Ranglisten eine Zulassungsmöglichkeit für die von ihnen in höchster Präferenz gewählte Hochschule haben, an deren Auswahlverfahren sie zu beteiligen sind, von dieser Hochschule zugelassen.
Die Stiftung teilt den Hochschulen für das Sommersemester bis zum 5.
März, für das Wintersemester bis zum 2.
September mit, welche Bewerberinnen und Bewerber unter Satz 1 fallen.
Die Hochschulen erteilen in diesen Fällen Zulassungsbescheide.
Die Zugelassenen nehmen am weiteren Verfahren nicht mehr teil.
Die Hochschulen teilen der Stiftung die Einschreibergebnisse für das Sommersemester bis zum 16.
März, für das Wintersemester bis zum 16.
September mit.

(8) Die Hochschulen teilen der Stiftung für das Sommersemester bis zum 18.
März, für das Wintersemester bis zum 18.
September ihre Verfahrensergebnisse in Form von Ranglisten mit, soweit die Ranglisten nicht bereits nach Absatz 7 übermittelt worden sind.
Die Stiftung gleicht sämtliche Ranglisten ab, indem in den Fällen mehrerer Zulassungsmöglichkeiten für eine Bewerberin oder einen Bewerber nur diejenige für die in höchster Präferenz genannte Hochschule bestehen bleibt, und übermittelt den Hochschulen für das Sommersemester bis zum 22.
März, für das Wintersemester bis zum 22.
September die bereinigten Ranglisten.
Die Hochschulen erteilen nach Maßgabe dieser Ranglisten Zulassungs- und Ablehnungsbescheide.
Die Zugelassenen nehmen am weiteren Verfahren nicht mehr teil.
Die Hochschulen teilen der Stiftung die Einschreibergebnisse für das Sommersemester bis zum 30.
März, für das Wintersemester bis zum 30.
September mit.

(9) Sind danach Studienplätze noch verfügbar oder werden Studienplätze wieder verfügbar, schreibt die Stiftung die Ranglisten nach Maßgabe des Absatzes 8 Satz 2 fort und übermittelt sie für das Sommersemester bis zum 2.
April, für das Wintersemester bis zum 2.
Oktober an die Hochschulen.
Die Hochschulen führen auf dieser Grundlage ein Nachrückverfahren durch; dabei werden keine Ablehnungsbescheide erteilt.
Die Zugelassenen nehmen am weiteren Verfahren nicht mehr teil.
Die Hochschulen teilen der Stiftung die Einschreibergebnisse für das Sommer­semester bis zum 8.
April, für das Wintersemester bis zum 8.
Oktober mit.

(10) Sind nach Durchführung des Nachrückverfahrens nach Absatz 9 Studienplätze noch verfügbar oder werden Studienplätze wieder verfügbar, schreibt die Stiftung die Ranglisten nach Maßgabe des Absatzes 8 Satz 2 fort und übermittelt sie für das Sommersemester bis zum 10.
April, für das Wintersemester bis zum 10.
Oktober an die Hochschulen.
Absatz 9 Satz 2 und 3 gilt entsprechend.
Die Hochschulen teilen der Stiftung die Einschreibergebnisse für das Sommersemester bis zum 17. April, für das Wintersemester bis zum 17.
Oktober mit.

(11) Nach Abschluss der Nachrückverfahren werden Studienplätze, die noch verfügbar sind oder wieder verfügbar werden, von der Hochschule durch das Los an Bewerberinnen und Bewerber vergeben, die bei der Hochschule die Zulassung beantragt haben.
Die Hochschule bestimmt Form und Frist der Antragstellung und gibt sie in geeigneter Weise bekannt.

### Abschnitt 4  
Quoten und Auswahlkriterien des zentralen Vergabeverfahrens

#### § 11  
Auswahl in der Abiturbestenquote

(1) An der Vergabe der Studienplätze in der Abiturbestenquote wird nicht beteiligt, wer

1.  im Zulassungsantrag keinen gültigen Studienortwunsch für diese Quote genannt hat oder
    
2.  unter die Quoten nach § 6 Absatz 1 oder Absatz 2 Nummer 2 oder Nummer 3 fällt.
    

(2) Für die Besetzung der Studienplätze in der Abiturbestenquote werden so viele Bewerberinnen und Bewerber ausgewählt, wie insgesamt in dieser Quote Studienplätze zu vergeben sind.
Die Auswahl erfolgt nach den Absätzen 3 bis 5; dabei werden die §§ 12 und 13 angewendet.

(3) Die Rangfolge wird durch die nach Anlage 2 ermittelte Durchschnittsnote bestimmt.
Eine Gesamtnote gilt als Durchschnittsnote nach Satz 1.

(4) Wer keine Durchschnittsnote nachweist, wird hinter die letzte Bewerberin und den letzten Bewerber mit feststellbarer Durchschnittsnote eingeordnet.

(5) Wer nachweist, aus in der eigenen Person liegenden, nicht selbst zu vertretenden Gründen daran gehindert gewesen zu sein, eine bessere Durchschnittsnote zu erreichen, wird auf Antrag mit der besseren Durchschnittsnote berücksichtigt.

#### § 12  
Landesquoten

(1) Für die Auswahl in der Abiturbestenquote bildet die Stiftung Landesquoten, sofern in dem jeweiligen Studiengang mehr als 15 Studienplätze zur Verfügung stehen.

(2) Die Quote eines Landes bemisst sich zu einem Drittel nach seinem Anteil an der Gesamtzahl der Bewerberinnen und Bewerber für den betreffenden Studiengang (Bewerberanteil) und zu zwei Dritteln nach seinem Anteil an der Gesamtzahl der Achtzehn- bis unter Einundzwanzigjährigen (Bevölkerungsanteil).
Die sich danach für die Länder Berlin, Bremen und Hamburg ergebenden Quoten werden um 30 Prozent erhöht.
Die auf die so ermittelten Landesquoten entfallenden Studienplätze werden in der Weise errechnet, dass zunächst jeder Landesquote ein Studienplatz zugeteilt wird und die verbleibenden Studienplätze nach dem d\`Hondtschen Höchstzahlverfahren ermittelt werden.

(3) Bei der Berechnung des Bewerberanteils eines Landes wird nur berücksichtigt, wer

1.  für diesen Studiengang zu dem Personenkreis gehört, der an der Auswahl in der Abiturbestenquote zu beteiligen ist, und
    
2.  eine nach den Beschlüssen der Kultusministerkonferenz bei der Berechnung des Bewerberanteils eines Landes zu berücksichtigende Hochschulzugangsberechtigung in dem betreffenden Land erworben hat.
    

(4) Für die Berechnung des Bevölkerungsanteils ist die Fortschreibung über die deutsche Wohnbevölkerung maßgeblich, die zuletzt vor dem Bewerbungsschluss des jeweiligen Vergabeverfahrens vom Statistischen Bundesamt veröffentlicht wurde.

#### § 13  
Zurechnung zu den Landesquoten

(1) Soweit Landesquoten gebildet werden, wird die Auswahl für jede Landesquote getrennt unter den Bewerberinnen und Bewerbern vorgenommen, die der jeweiligen Landesquote zuzurechnen sind.

(2) Im Falle einer im Inland erworbenen deutschen Hochschulzugangsberechtigung bestimmt der Ort des Erwerbs die Zurechnung zu den Landesquoten.
Wer keiner Landesquote zugerechnet werden kann, wird entsprechend den Bevölkerungsanteilen durch das Los einer Landesquote zugeordnet.

(3) Kann das Studienplatzkontingent einer Landesquote aus Mangel an Bewerbungen nicht ausgeschöpft werden, werden die Studienplätze in entsprechender Anwendung des § 12 Absatz 2 auf die übrigen Landesquoten verteilt.

#### § 14  
Auswahl nach Wartezeit

(1) Die Rangfolge wird durch die Zahl der seit dem Erwerb der Hochschulzugangsberechtigung verstrichenen Halbjahre bestimmt.
Es zählen nur volle Halbjahre vom Zeitpunkt des Erwerbs der Hochschulzugangsberechtigung bis zum Beginn des Semesters, für das die Zulassung beantragt wird.
Halbjahre sind die Zeit vom 1.
April bis zum 30.
September eines Jahres (Sommersemester) und die Zeit vom 1.
Oktober eines Jahres bis zum 31.
März des folgenden Jahres (Wintersemester).

(2) Wird der Zeitpunkt des Erwerbs der Hochschulzugangsberechtigung nicht nachgewiesen, wird die Zahl der Halbjahre seit dem Erwerb der Hochschulzugangsberechtigung nicht berücksichtigt.

(3) Wer nachweist, aus in der eigenen Person liegenden, nicht selbst zu vertretenden Gründen daran gehindert gewesen zu sein, die Hochschulzugangsberechtigung zu einem früheren Zeitpunkt zu erwerben, wird auf Antrag bei der Ermittlung der Wartezeit mit dem früheren Zeitpunkt des Erwerbs der Hochschulzugangsberechtigung berücksichtigt.

(4) Ist vor dem Erwerb der Hochschulzugangsberechtigung ein berufsqualifizierender Abschluss außerhalb der Hochschule erlangt und die Hochschulzugangsberechtigung vor dem 16.
Juli 2007 erworben worden, wird die Zahl der Halbjahre um eins für je sechs Monate Berufsausbildung, höchstens jedoch um zwei Halbjahre erhöht.
Ist im Falle des Satzes 1 die Hochschulzugangsberechtigung vor dem 16.
Januar 2002 erworben worden, wird die Zahl der Halbjahre um bis zu vier erhöht.
Dies gilt entsprechend, wenn die Ableistung eines Dienstes eine Bewerberin oder einen Bewerber daran gehindert hat, vor dem Erwerb der Hochschulzugangsberechtigung einen berufsqualifizierenden Abschluss außerhalb der Hochschule zu erlangen, sofern der berufsqualifizierende Abschluss zu einer Erhöhung der Zahl der Halbjahre nach Satz 1 oder Satz 2 geführt hätte.

(5) Ein berufsqualifizierender Abschluss nach Absatz 4 liegt vor bei

1.  Ausbildungsberufen, die in dem Verzeichnis der anerkannten Ausbildungsberufe nach § 90 Absatz 3 Nummer 3 des Berufsbildungsgesetzes vom 23.
    März 2005 (BGBl.
    I S. 931) enthalten sind,
    
2.  einer Berufsausbildung an einer staatlichen oder staatlich anerkannten Berufsfachschule oder Fachschule,
    
3.  einer abgeschlossenen Ausbildung im einfachen oder mittleren Dienst der öffentlichen Verwaltung,
    
4.  einer abgeschlossenen Berufsausbildung, die nach Artikel 37 Absatz 1 oder Absatz 3 des Einigungsvertrages einer Berufsausbildung nach den Nummern 1 bis 3 gleichzustellen ist.
    

Ein berufsqualifizierender Abschluss nach Absatz 4 Satz 1 mit zweijähriger Ausbildungsdauer gilt als nachgewiesen, wenn die Hochschulzugangsberechtigung an einem Abendgymnasium oder an einem Kolleg erworben worden ist.

(6) Von der Gesamtzahl der Halbjahre wird die Zahl der Halbjahre abgezogen, in denen die Bewerberin oder der Bewerber an einer deutschen Hochschule als Studentin oder Student eingeschrieben war.

#### § 15  
Auswahl nach Härtegesichtspunkten

Die Studienplätze der Härtequote werden auf Antrag an Bewerberinnen und Bewerber vergeben, für die es eine außergewöhnliche Härte bedeuten würde, wenn sie für den genannten Studiengang keine Zulassung erhielten.
Eine außergewöhnliche Härte liegt vor, wenn in der eigenen Person liegende besondere soziale oder familiäre Gründe die sofortige Aufnahme des Studiums oder einen sofortigen Studienortwechsel zwingend erfordern.
Die Rangfolge wird durch den Grad der außergewöhnlichen Härte bestimmt.

#### § 16   
Auswahl der Bewerberinnen und Bewerber mit besonderer Hochschulzugangsberechtigung

(1) Ist die Hochschulzugangsberechtigung in einem anderen noch nicht abgeschlossenen Studiengang erworben worden (besondere Hochschulzugangsberechtigung), ist eine Auswahl im Rahmen der Quoten nach § 6 Absatz 3 bis 5 ausgeschlossen.
Die Rangfolge wird durch die Durchschnittsnote der Hochschulzugangsberechtigung bestimmt.

(2) Weist die Hochschulzugangsberechtigung keine auf eine Stelle nach dem Komma bestimmte Durchschnittsnote im Rahmen eines sechsstufigen Notensystems aus, ist diese durch eine besondere Bescheinigung der Einrichtung nachzuweisen, an der die Hochschulzugangsberechtigung erworben wurde.

(3) Wer keine Durchschnittsnote nachweist, wird hinter die letzte Bewerberin und den letzten Bewerber mit feststellbarer Durchschnittsnote eingeordnet.

#### § 17  
Auswahl für ein Zweitstudium

(1) Wer bereits ein Studium in einem anderen Studiengang an einer deutschen Hochschule abgeschlossen hat (Bewerberinnen und Bewerber für ein Zweitstudium), kann nicht im Rahmen der Quoten nach § 6 Absatz 3 bis 5 ausgewählt werden.

(2) Die Rangfolge wird durch eine Messzahl bestimmt, die aus dem Ergebnis der Abschlussprüfung des Erststudiums und dem Grad der Bedeutung der Gründe für das Zweitstudium ermittelt wird.
Die Einzelheiten zur Ermittlung der Messzahl ergeben sich aus Anlage 3.

(3) Soweit ein Zweitstudium aus wissenschaftlichen Gründen angestrebt wird, erfolgt die Auswahl auf der Grundlage der Feststellungen der im Zulassungsantrag an erster Stelle genannten Hochschule.

#### § 18  
Nachrangige Auswahlkriterien

(1) Besteht bei der Auswahl in der Abiturbestenquote Ranggleichheit, bestimmt sich die Rangfolge nach den Bestimmungen über die Auswahl nach Wartezeit.
Besteht bei der Auswahl nach Wartezeit Ranggleichheit, bestimmt sich die Rangfolge durch die nach § 11 Absatz 3 bis 5 ermittelte Durchschnittsnote.

(2) Besteht danach noch Ranggleichheit oder besteht bei der Auswahl in den übrigen Quoten Ranggleichheit, wird vorrangig ausgewählt, wer zu dem Personenkreis nach § 19 Absatz 1 Satz 1 Nummer 1 bis 3 gehört und durch eine Bescheinigung glaubhaft macht, dass der Dienst in vollem Umfang abgeleistet ist oder bei einer Bewerbung für das Sommersemester bis zum 30.
April und bei einer Bewerbung für das Wintersemester bis zum 31.
Oktober in vollem Umfang abgeleistet sein wird, oder glaubhaft macht, dass bis zu den genannten Zeitpunkten mindestens sechs Monate Dienst nach § 19 Absatz 1 Satz 1 Nummer 4 ausgeübt sein werden.
Im Übrigen entscheidet bei Ranggleichheit das Los.

### Abschnitt 5  
Auswahl nach einem Dienst auf Grund früherer Zulassung

#### § 19   
Auswahl nach einem Dienst auf Grund früheren Zulassungsanspruchs

(1) Bewerberinnen und Bewerber, die

1.  eine Dienstpflicht nach Artikel 12a des Grundgesetzes erfüllt oder eine solche Dienstpflicht oder entsprechende Dienstleistungen auf Zeit übernommen haben bis zur Dauer von drei Jahren,
    
2.  mindestens ein Jahr Entwicklungsdienst nach dem Entwicklungshelfer-Gesetz vom 18. Juni 1969 (BGBl.
    I S. 549) geleistet haben,
    
3.  einen Jugendfreiwilligendienst im Sinne des Jugendfreiwilligendienstegesetzes vom 16. Mai 2008 (BGBl.
    I S. 842) oder im Rahmen eines von der Bundesregierung geförderten Modellprojekts geleistet haben; § 15 Absatz 2 des Jugendfreiwilligendienstegesetzes gilt entsprechend,
    
4.  ein Kind unter 18 Jahren oder eine pflegebedürftige Person aus dem Kreis der sonstigen Angehörigen bis zur Dauer von drei Jahren betreut oder gepflegt haben,
    

(Dienst) werden in dem genannten Studiengang auf Grund früheren Zulassungsanspruchs ausgewählt, wenn sie zu Beginn oder während eines Dienstes für diesen Studiengang zugelassen worden sind oder wenn zu Beginn oder während eines Dienstes für diesen Studiengang nicht an allen Hochschulen Zulassungszahlen festgesetzt waren.
Der von einem nach § 2 Satz 2 Deutschen gleichgestellten ausländischen Staatsangehörigen oder Staatenlosen geleistete Dienst steht einem Dienst nach Satz 1 gleich, wenn er diesem gleichwertig ist.

(2) Ist die frühere Zulassung in der Abiturbestenquote erfolgt, lässt die Stiftung vorab die Bewerberin oder den Bewerber in dieser Quote an demselben Studienort erneut zu.
Ist die frühere Zulassung im Auswahlverfahren einer Hochschule oder im Losverfahren einer Hochschule nach § 10 Absatz 11 erfolgt, lässt diese Hochschule in ihrem Auswahlverfahren die Bewerberin oder den Bewerber vorab erneut zu.
Ist die frühere Zulassung in einer sonstigen, von der Stiftung vergebenen Quote erfolgt oder beruht der Zulassungsanspruch nicht auf einer tatsächlich erfolgten Zulassung, wählt die Stiftung die Bewerberin oder den Bewerber vor der Vergabe der Studienplätze in den sonstigen Quoten aus.
Die erneute Zulassung nach den Sätzen 1 und 2 setzt voraus, dass der Studienort der früheren Zulassung für die entsprechende Quote an erster Stelle genannt worden ist.

(3) Die Auswahl nach Absatz 1 Satz 1 muss spätestens zum zweiten Vergabeverfahren beantragt werden, das nach Beendigung des Dienstes durchgeführt wird.
Ist der Dienst noch nicht beendet, ist durch Bescheinigung glaubhaft zu machen, dass der Dienst bei einer Bewerbung für das Sommersemester bis zum 30.
April oder bei einer Bewerbung für das Wintersemester bis zum 31. Oktober beendet sein wird.

(4) Wird die Festlegung einer Rangfolge zwischen den nach einem Dienst auf Grund früheren Zulassungsanspruchs Auszuwählenden erforderlich, entscheidet das Los.

(5) Beruht ein Zulassungsanspruch auf einer gegen die Stiftung gerichteten gerichtlichen Entscheidung, die sich auf ein bereits abgeschlossenes Vergabeverfahren bezieht, sind die Absätze 1 bis 4 entsprechend anzuwenden.

### Abschnitt 6   
Verteilung auf die Studienorte

#### § 20   
Verteilung der in der Abiturbestenquote Ausgewählten auf die Studienorte

Die Zulassung richtet sich vorrangig nach den im Zulassungsantrag nach § 3 Absatz 3 Satz 2 geäußerten Studienortwünschen.
Können an einem Studienort nicht alle Bewerberinnen und Bewerber zugelassen werden, die diesen Studienort an gleicher Stelle genannt haben, entscheidet über die Zulassung die nach § 11 Absatz 3 bis 5 ermittelte Durchschnittsnote.
Besteht bei der Zulassung nach Satz 2 Ranggleichheit, entscheidet die nach Anlage 5 ermittelte Punktzahl der Gesamtqualifikation der Hochschulzugangsberechtigung.
Besteht bei der Zulassung nach Satz 3 Ranggleichheit, entscheidet die Rangfolge nach § 21 Absatz 1 Satz 2.
Im Übrigen entscheidet bei Ranggleichheit das Los.
Wer an keinen für diese Quote genannten Studienort verteilt werden kann, wird nicht zugelassen.

#### § 21   
Verteilung der nach § 7 Absatz 3 Ausgewählten auf die Studienorte

(1) Die Zulassung richtet sich vorrangig nach den im Zulassungsantrag nach § 3 Absatz 3 Satz 3 geäußerten Studienortwünschen.
Können an einem Studienort nicht alle Bewerberinnen und Bewerber zugelassen werden, die diesen Studienort an gleicher Stelle genannt haben, entscheidet die nachstehende Rangfolge:

1.  amtlich festgestellte Eigenschaft als schwerbehinderter Mensch nach Teil 2 des Neunten Buches Sozialgesetzbuch (SGB IX),
    
2.  einzige Wohnung oder Hauptwohnung mit dem Ehegatten, den Kindern in den dem Studienort zugeordneten Kreisen und kreisfreien Städten,
    
3.  Anerkennung des ersten Studienortwunsches nach Absatz 3,
    
4.  einzige Wohnung oder Hauptwohnung bei den Eltern in den dem Studienort zugeordneten Kreisen und kreisfreien Städten,
    
5.  keiner der vorgenannten Gründe.
    

Die Zuordnung von Kreisen und kreisfreien Städten zu den einzelnen Studienorten ergibt sich aus Anlage 4.

(2) Besteht bei der Zulassung nach Absatz 1 Satz 2 Ranggleichheit, entscheidet die nach § 11 Absatz 3 bis 5 ermittelte Durchschnittsnote; bei der Zulassung für ein Zweitstudium gilt das Ergebnis der Abschlussprüfung des Erst­studiums als Grad der Qualifikation.
Im Übrigen entscheidet bei Ranggleichheit das Los.

(3) Für den an erster Stelle genannten Studienort kann ein Antrag auf bevorzugte Berücksichtigung gestellt werden.
Dem Antrag soll nur stattgegeben werden, wenn die Zulassung an einem anderen Studienort unter Anlegung eines strengen Maßstabs mit erheblichen Nachteilen verbunden wäre.
Hierbei kommen insbesondere eigene gesundheitliche, familiäre oder wirtschaftliche Umstände sowie wissenschaftliche Gründe in Betracht.

### Abschnitt 7   
Vergabe von Teilstudienplätzen

#### § 22   
Teilstudienplätze

(1) Studienplätze, bei denen die Zulassung auf den ersten Teil eines Studiengangs beschränkt ist, weil das Weiterstudium an einer deutschen Hochschule nicht gewährleistet ist (Teilstudienplätze), werden getrennt von den übrigen Studienplätzen von der Stiftung vergeben.

(2) Die festgesetzte Zahl an Teilstudienplätzen, vermindert um die Zahl der nach einem Dienst auf Grund früheren Zulassungsanspruchs Auszuwählenden, wird jeweils im Anschluss an das Verfahren nach § 10 Absatz 10 durch das Los an die Bewerberinnen und Bewerber vergeben, die bis dahin nicht zugelassen sind.
Die §§ 1 bis 4, 8, 19 und 21 gelten entsprechend; die Zulassung für einen Teilstudienplatz wird nicht nach § 4 Absatz 3 Satz 1 berücksichtigt.

### Abschnitt 8  
Schlussbestimmungen

#### § 23   
Inkrafttreten, Außerkrafttreten

(1) Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Sie gilt erstmals für das Vergabeverfahren zum Wintersemester 2010/2011.

(2) Mit Inkrafttreten dieser Verordnung tritt die Zentrale Vergabeverordnung vom 22.
März 2006 (GVBl. II S. 66), die zuletzt durch Verordnung vom 22.
April 2009 (GVBl.
II S. 266) geändert worden ist, außer Kraft.

Potsdam, den 18.
Mai 2010

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Dr.
Martina Münch

**Anlage 1** (zu § 1 Satz 2)

**In das zentrale Vergabeverfahren einbezogene Studiengänge**

Studiengänge ohne Fachhochschulstudiengänge mit dem Abschluss Diplom oder Staatsexamen (ohne Lehrämter):

Medizin

Pharmazie

Tiermedizin

Zahnmedizin

**Anlage 2** (zu § 11 Absatz 3 Satz 1)

**Ermittlung der Durchschnittsnote**

(1) Bei Hochschulzugangsberechtigungen auf der Grundlage der

1.  „Vereinbarung über die gegenseitige Anerkennung von Zeugnissen der allgemeinen Hochschulreife, die an Gymnasien mit neugestalteter Oberstufe erworben wurden“ gemäß Beschluss der Kultusministerkonferenz vom 7.
    Mai 1971 in der Fassung vom 8.
    November 1972 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 191.1),
    
2.  „Vereinbarung zur Gestaltung der gymnasialen Oberstufe in der Sekundarstufe II“ gemäß Beschluss der Kultusministerkonferenz vom 7.
    Juli 1972 in der Fassung vom 24.
    Oktober 2008 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 176),
    
3.  „Vereinbarung über die Abiturprüfung für Nichtschülerinnen und Nichtschüler entsprechend der Gestaltung der gymnasialen Oberstufe in der Sekundarstufe II“ gemäß Beschluss der Kultusministerkonferenz vom 13.
    September 1974 in der Fassung vom 24.
    Oktober 2008 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 192.2),
    
4.  „Vereinbarung über die Durchführung der Abiturprüfung für Schülerinnen und Schüler an Waldorfschulen“ gemäß Beschluss der Kultusministerkonferenz vom 21.
    Februar 1980 in der Fassung vom 24.
    Oktober 2008 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 485.2),
    
5.  „Vereinbarung zur Gestaltung der Abendgymnasien“ gemäß Beschluss der Kultusministerkonferenz vom  
    21.
    Juni 1979 in der Fassung vom 24.
    Oktober 2008 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 240.2),
    
6.  „Vereinbarung zur Gestaltung der Kollegs“ gemäß Beschluss der Kultusministerkonferenz vom 21.
    Juni 1979 in der Fassung vom 24.
    Oktober 2008 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 248.1),
    

die eine auf eine Stelle nach dem Komma bestimmte Durchschnittsnote enthalten, wird diese von der Stiftung bei der Rangplatzbestimmung zugrunde gelegt.
Enthält die Hochschulzugangsberechtigung keine Durchschnittsnote nach Satz 1, aber eine Punktzahl der Gesamtqualifikation, wird von der Stiftung nach Anlage 2 der „Vereinbarung über die Abiturprüfung der gymnasialen Oberstufe in der Sekundarstufe II“ gemäß Beschluss der Kultusministerkonferenz vom 13.
Dezember 1973 in der Fassung vom 24.
Oktober 2008 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 192) die Durchschnittsnote aus der Punktzahl der Gesamtqualifikation errechnet.
Die Durchschnittsnote wird auf eine Stelle nach dem Komma errechnet; es wird nicht gerundet.

(2) Bei Hochschulzugangsberechtigungen auf der Grundlage der „Vereinbarung über die gegenseitige Anerkennung der an Gymnasien erworbenen Zeugnisse der allgemeinen Hochschulreife“ gemäß Beschluss der Kultusministerkonferenz vom 20.
März 1969 – in der Fassung vom 20.
Juni 1972 – und vom 13.
Dezember 1973 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 191) wird die allgemeine Durchschnittsnote aus dem arithmetischen Mittel der Noten der Hochschulzugangsberechtigung einschließlich der Noten für die im 11.
und 12.
Schuljahr abgeschlossenen Fächer wie folgt gebildet:

1.  weist die Hochschulzugangsberechtigung eine Note für das Fach Gemeinschaftskunde aus, werden die Noten für die Fächer Geschichte, Erdkunde, Sozialkunde und Philosophie sowie für sonstige Fächer, die in der Hochschulzugangsberechtigung als zu dem Fach Gemeinschaftskunde gehörig ausgewiesen sind, nicht gewertet;
    
2.  weist die Hochschulzugangsberechtigung keine Note für das Fach Gemeinschaftskunde aus, ist diese aus dem arithmetischen Mittel der Noten für die Fächer Geschichte, Erdkunde, Sozialkunde und Philosophie oder für die Fächer, die in der Hochschulzugangsberechtigung als zu dem Fach Gemeinschaftskunde gehörig ausgewiesen sind, zu bilden;
    
3.  ist in der Hochschulzugangsberechtigung eine Note für das Fach Geschichte mit Gemeinschaftskunde ausgewiesen, gilt diese Note als Note für das Fach Geschichte und als Note für das Fach Sozialkunde;
    
4.  bei der Bildung der Note für das Fach Gemeinschaftskunde wird gerundet;
    
5.  ist in der Hochschulzugangsberechtigung neben den Noten für die Fächer Biologie, Chemie und Physik eine Gesamtnote für den naturwissenschaftlichen Bereich ausgewiesen, bleibt diese bei der Errechnung der Durchschnittsnote außer Betracht;
    
6.  Noten für die Fächer Religionslehre, Ethik, Kunsterziehung, Musik und Sport bleiben außer Betracht, es sei denn, dass die Zulassung zu einem entsprechenden Studiengang beantragt wird;
    
7.  Noten für die Fächer Kunsterziehung, Musik und Sport werden gewertet, soweit sie Kernpflichtfächer waren;
    
8.  Noten für zusätzliche Unterrichtsveranstaltungen und für Arbeitsgemeinschaften bleiben unberücksichtigt;
    
9.  die Durchschnittsnote wird auf eine Stelle nach dem Komma errechnet; es wird nicht gerundet.
    

Die allgemeine Durchschnittsnote wird von der Schule, die besonderen Durchschnittsnoten für bestimmte Studiengänge nach Satz 1 Nummer 6 werden auf Antrag von der Schule in der Hochschulzugangsberechtigung oder einer besonderen Bescheinigung ausgewiesen.
Für Hochschulzugangsberechtigungen, die vor dem 1.
April 1975 erworben wurden, ermittelt die Stiftung die Durchschnittsnoten, soweit sie nicht von der Schule ausgewiesen sind.

(3) Bei Hochschulzugangsberechtigungen auf der Grundlage

1.  der „Vereinbarung über Abendgymnasien“ gemäß Beschluss der Kultusministerkonferenz vom 3.
    Oktober 1957 in der Fassung vom 8.
    Oktober 1970 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 240),
    
2.  des Beschlusses der Kultusministerkonferenz vom 8.
    Juli 1965 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 248) über die „Institute zur Erlangung der Hochschulreife (‚Kollegs‘)“
    

wird die Durchschnittsnote aus dem arithmetischen Mittel der Noten der Hochschulzugangsberechtigung mit Ausnahme der Noten für die Fächer, die in der Hochschulzugangsberechtigung oder einer besonderen Bescheinigung als vorzeitig abgeschlossen ausgewiesen sind, gebildet.
Absatz 2 Satz 1 Nummer 1 bis 6 und 9 findet Anwendung.
Ist die Durchschnittsnote nicht von der Schule ausgewiesen, wird sie von der Stiftung nach den Sätzen 1 und 2 errechnet.

(4) Bei Hochschulzugangsberechtigungen auf der Grundlage der

1.  „Vereinbarung über die befristete gegenseitige Anerkennung von Zeugnissen der fachgebundenen Hochschulreife, die an zur Zeit bestehenden Schulen, Schulformen beziehungsweise -typen erworben worden sind“ gemäß Beschluss der Kultusministerkonferenz vom 25.
    November 1976 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 226.2) und vom 16.
    Februar 1978 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 226.2.1),
    
2.  „Sondervereinbarung über die gegenseitige Anerkennung der Zeugnisse von besonderen gymnasialen Schulformen, die zu einer allgemeinen Hochschulreife führen“ gemäß Beschluss der Kultusministerkonferenz vom 25.
    November 1976 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 226.1),
    
3.  „Rahmenvereinbarung über die Berufsoberschule“ gemäß Beschluss der Kultusministerkonferenz vom  
    25.
    November 1976 in der Fassung vom 1.
    Februar 2007 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 470)
    

finden die Absätze 1 bis 3 entsprechende Anwendung.
Dabei ist bei der Bildung der Note für das Fach Gemeinschaftskunde nach Absatz 2 Satz 1 Nummer 2 eine im Zeugnis ausgewiesene Note für das Fach Wirtschaftsgeografie beziehungsweise Geografie mit Wirtschaftsgeografie einzubeziehen.

(5) Bei Hochschulzugangsberechtigungen, die auf dem Gebiet der Bundesrepublik Deutschland nach dem Stand bis zum 3.
Oktober 1990 an einer in eine Hochschule übergeleiteten Bildungseinrichtung erworben wurden, ist eine Durchschnittsnote von der Hochschule in dem Zeugnis oder einer besonderen Bescheinigung auszuweisen.
Die Durchschnittsnote wird auf eine Stelle nach dem Komma errechnet; es wird nicht gerundet.

(6) Bei sonstigen Hochschulzugangsberechtigungen, die auf dem Gebiet der Bundesrepublik Deutschland nach dem Stand bis zum 3.
Oktober 1990 erworben wurden und eine Durchschnittsnote enthalten, die auf eine Stelle nach dem Komma bestimmt ist, wird diese von der Stiftung bei der Rangplatzbestimmung zugrunde gelegt.

(7) Bei sonstigen Hochschulzugangsberechtigungen, die auf dem Gebiet der Bundesrepublik Deutschland nach dem Stand bis zum 3.
Oktober 1990 erworben wurden und nur Einzelnoten im Rahmen eines sechsstufigen Notensystems enthalten, wird von der Stiftung eine Durchschnittsnote unter entsprechender Anwendung des Absatzes 2 Satz 1 Nummer 1 bis 6 und 9 aus dem arithmetischen Mittel der Noten gebildet; Noten für gegebenenfalls im 11.
und  
12.
Schuljahr abgeschlossene Fächer sowie Noten für zusätzliche Unterrichtsveranstaltungen und für Arbeitsgemeinschaften bleiben unberücksichtigt.

(8) Bei sonstigen Hochschulzugangsberechtigungen, die auf dem Gebiet der Bundesrepublik Deutschland nach dem Stand bis zum 3.
Oktober 1990 erworben wurden und weder eine Durchschnittsnote, die auf eine Stelle nach dem Komma bestimmt ist, noch Einzelnoten im Rahmen eines sechsstufigen Notensystems enthalten, ist eine Durchschnittsnote durch eine besondere Bescheinigung nachzuweisen, die von der für die Abnahme der entsprechenden Prüfung zuständigen Stelle oder von der obersten Landesbehörde auszustellen ist, unter deren Aufsicht diese Prüfung durchgeführt worden ist.
Bei der Bestimmung der Durchschnittsnote sind einzelne Prüfungsleistungen, die der Hochschulzugangsberechtigung zugrunde liegen, zur Beurteilung heranzuziehen.
Die Durchschnittsnote wird auf eine Stelle nach dem Komma bestimmt; es wird nicht gerundet.

(9) Bei Hochschulzugangsberechtigungen aus der ehemaligen Deutschen Demokratischen Republik, die nach dem Beschluss der Kultusministerkonferenz vom 10.
Mai 1990 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 908) zur Aufnahme eines Studiums in der Bundesrepublik Deutschland berechtigen, wird die Durchschnittsnote nach dem Beschluss der Kultusministerkonferenz vom 8.
Juli 1987 in der Fassung vom 8.
Oktober 1990 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 289.1) errechnet.
Bei Hochschulzugangsberechtigungen aus den in Artikel 3 des Einigungsvertrages genannten Ländern, die nach dem Beschluss der Kultusministerkonferenz vom 21.
Februar 1992 in der Fassung vom 12.
März 1993 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 234) und vom 25.
Februar 1994 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 234.1) zur Aufnahme eines Studiums in der Bundesrepublik Deutschland berechtigen, wird die Durchschnittsnote nach dem Beschluss der Kultusministerkonferenz vom 21.
Februar 1992 in der Fassung vom 9.
Juni 1993 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 235) errechnet.
Die Durchschnittsnote wird jeweils von der für die Ausstellung des Zeugnisses zuständigen Stelle auf eine Stelle nach dem Komma errechnet; es wird nicht gerundet.
Die Stiftung legt die auf dem Zeugnis oder in einer besonderen Bescheinigung ausgewiesene Durchschnittsnote bei der Rangplatzbestimmung zugrunde.

(10) Bei ausländischen Vorbildungsnachweisen wird die Gesamtnote, wenn keine Bescheinigung der Zeugnisanerkennungsstelle eines Landes über die Festsetzung einer Gesamtnote vorliegt, von der Stiftung auf der Grundlage der „Vereinbarung über die Festsetzung der Gesamtnote bei ausländischen Hochschulzugangszeugnissen“ vom 15.
März 1991 in der Fassung vom 18.
November 2004 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 289.5) berechnet.

(11) Bei Hochschulzugangsberechtigungen, die bis einschließlich 1986 auf Grund einer Abschlussprüfung unter dem Vorsitz einer oder eines Prüfungsbeauftragten der Kultusministerkonferenz an deutschen Schulen im Ausland (ausgenommen die Schulen mit neugestalteter gymnasialer Oberstufe) und an Privatschulen im deutschsprachigen Ausland erworben wurden, ist die Durchschnittsnote durch eine Bescheinigung der oder des Prüfungsbeauftragten nachzuweisen.
Dasselbe gilt weiterhin für die Zeugnisse der deutschen Reifeprüfungen, die am Lyzeum Alpinum in Zuoz und am Institut auf dem Rosenberg in St.
Gallen erworben wurden.
Die Durchschnittsnote wird auf eine Stelle nach dem Komma bestimmt; es wird nicht gerundet.
Bei Hochschulzugangsberechtigungen, die ab 1987 auf Grund einer Abschlussprüfung unter dem Vorsitz einer oder eines Prüfungsbeauftragten der Kultusministerkonferenz an deutschen Schulen im Ausland erworben wurden, wird die auf dem Zeugnis ausgewiesene, auf eine Stelle nach dem Komma bestimmte Durchschnittsnote von der Stiftung bei der Rangplatzbestimmung zugrunde gelegt.

(12) Bei Hochschulzugangsberechtigungen, die an den deutsch-französischen Gymnasien ab dem Abiturtermin 1982 erworben wurden, wird der in den Zeugnissen gemäß Artikel 30 des Abkommens zwischen der Regierung der Bundesrepublik Deutschland und der Regierung der Französischen Republik vom 10.
Februar 1972 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 90) ausgewiesene „allgemeine Notendurchschnitt“ bei der Rangplatzbestimmung zugrunde gelegt.
Für die Umrechnung des „allgemeinen Notendurchschnitts“ wird der für die Europäischen Schulen geltende Umrechnungsschlüssel gemäß Beschluss der Kultusministerkonferenz vom 8.
Dezember 1975 in der Fassung vom 14.
Februar 1996 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 289.2) angewendet.
Bei Absolventinnen und Absolventen der deutsch-französischen Gymnasien in Freiburg und Saarbrücken werden für das Abitur 1982 und 1983 die bis 1981 geltenden Richtlinien angewendet, sofern durch die Neuregelung im Einzelfall eine Verschlechterung der Durchschnittsnote eintritt.
Die nach diesem Verfahren umgerechnete allgemeine Durchschnittsnote wird zusätzlich zum „allgemeinen Notendurchschnitt“ im „Zeugnis über das Bestehen des deutsch-französischen Abiturs“ ausgewiesen und durch den Stempelzusatz „Durchschnittsnote gemäß Staatsvertrag über die Vergabe von Studienplätzen“ gekennzeichnet.

(13) Bei Hochschulzugangsberechtigungen, die nach den Bestimmungen der/des „International Baccalaureate Organisation/Office du Baccalauréat International“ erworben wurden, wird die Durchschnittsnote auf der Grundlage der Vereinbarung über die Anerkennung des „International Baccalaureate Diploma/Diplôme du Baccalauréat International“ gemäß Beschluss der Kultusministerkonferenz vom 10.
März 1986 in der Fassung vom 26.
Juni 2009 (Beschluss-Sammlung der Kultusministerkonferenz Nummer 283) berechnet.

**Anlage 3** (zu § 17 Absatz 2 Satz 2)

**Ermittlung der Messzahl bei der Auswahl für ein Zweitstudium**

(1) Die Messzahl ist die Summe der Punktzahlen, die für das Ergebnis der Abschlussprüfung des Erststudiums und für den Grad der Bedeutung der Gründe für das Zweitstudium vergeben werden.

(2) Für das Ergebnis der Abschlussprüfung des Erststudiums werden folgende Punktzahlen vergeben:

> 1.  
> 
> Noten „ausgezeichnet“ und „sehr gut“  
> 
> –  
> 
> 4 Punkte
> 
> 2.  
> 
> Noten „gut“ und „voll befriedigend“  
> 
> –  
> 
> 3 Punkte
> 
> 3.  
> 
> Note „befriedigend“  
> 
> –  
> 
> 2 Punkte
> 
> 4.  
> 
> Note „ausreichend“  
> 
> –  
> 
> 1 Punkt.

Ist die Note der Abschlussprüfung des Erststudiums nicht nachgewiesen, wird das Ergebnis der Abschlussprüfung mit einem Punkt bewertet.

(3) Nach dem Grad der Bedeutung der Gründe für das Zweitstudium werden folgende Punktzahlen vergeben:

> 1.  
> 
> „zwingende berufliche Gründe“  
> 
> –  
> 
> 9 Punkte;
> 
> zwingende berufliche Gründe liegen vor, wenn ein Beruf angestrebt wird, der nur auf Grund zweier abgeschlossener Studiengänge ausgeübt werden kann;
> 
> 2.  
> 
> „wissenschaftliche Gründe“  
> 
> –  
> 
> 7 bis 11 Punkte;
> 
> wissenschaftliche Gründe liegen vor, wenn im Hinblick auf eine spätere Tätigkeit in Wissenschaft und Forschung auf der Grundlage der bisherigen wissenschaftlichen und praktischen Tätigkeit eine weitere wissenschaftliche Qualifikation in einem anderen Studiengang angestrebt wird;
> 
> 3.  
> 
> „besondere berufliche Gründe“  
> 
> –  
> 
> 7 Punkte;
> 
> besondere berufliche Gründe liegen vor, wenn die berufliche Situation dadurch erheblich verbessert wird, dass der Abschluss des Zweitstudiums das Erststudium sinnvoll ergänzt;   
> 
> 4.  
> 
> „sonstige berufliche Gründe“  
> 
> –  
> 
> 4 Punkte;
> 
> sonstige berufliche Gründe liegen vor, wenn das Zweitstudium auf Grund der beruflichen Situation aus sonstigen Gründen zu befürworten ist;
> 
> 5.  
> 
> „keiner der vorgenannten Gründe“  
> 
> –  
> 
> 1 Punkt.

Liegen wissenschaftliche Gründe vor, ist die Punktzahl innerhalb des Rahmens von 7 bis 11 Punkten davon abhängig, welches Gewicht die Gründe haben, welche Leistungen bisher erbracht worden sind und in welchem Maß die Gründe von allgemeinem Interesse sind.
Wird das Zweitstudium nach einer Familienphase zum Zwecke der Wiedereingliederung oder des Neueinstiegs in das Berufsleben angestrebt, kann dieser Umstand unabhängig von der Bewertung des Vorhabens und seiner Zuordnung zu einer der vorgenannten Fallgruppen durch Gewährung eines Zuschlags von bis zu 2 Punkten bei der Messzahlbildung berücksichtigt werden.

**Anlage 4** (zu § 21 Absatz 1 Satz 3)

**Zuordnung der Kreise und kreisfreien Städte zu den Studienorten**

(1) Ein Studienort kann eine Hochschule, ein Teil einer Hochschule oder ein gemeinsames Studienangebot mehrerer Hochschulen sein.

(2) Einem Studienort eines Landes zugeordnet sind der Kreis oder die kreisfreie Stadt des Studienorts sowie die hieran angrenzenden Kreise oder kreisfreien Städte des Landes.
Sofern sich in einem Kreis oder in einer kreisfreien Stadt oder in den hieran angrenzenden Kreisen oder kreisfreien Städten kein Studienort des Landes befindet, ist dieser Kreis oder diese kreisfreie Stadt dem nächsten Studienort des Landes zugeordnet.
Dies gilt entsprechend, wenn Studiengänge nur an bestimmten Studienorten des Landes angeboten werden.
Kreise und kreisfreie Städte eines Landes sind auch dem Studienort eines anderen Landes zugeordnet, wenn sie an den Kreis oder die kreisfreie Stadt des Studienorts des anderen Landes angrenzen; dabei gelten Bremen und Bremerhaven als eine kreisfreie Stadt.

(3) Örtliche und regionale Verwaltungseinheiten eines anderen Mitgliedstaates der Europäischen Union, die an ein Land der Bundesrepublik Deutschland angrenzen, können einem Studienort dieses Landes zugeordnet werden, wenn sie an den Kreis oder die kreisfreie Stadt dieses Studienorts angrenzen.

(4) In der nachfolgenden Übersicht ist für jeden Kreis und jede kreisfreie Stadt die Entfernung zu den Studienorten des Landes als Länge der Luftlinie zwischen Kreisstadt und Studienort in Kilometern (km), jeweils auf 10 km gerundet, angegeben.

(5) Ist ein Studienort im Kreis oder in der kreisfreien Stadt oder in einem hieran angrenzenden Kreis oder einer hieran angrenzenden kreisfreien Stadt gelegen, ist als Entfernung 0 angegeben; dies gilt auch für außerhalb des Landes gelegene Studienorte.

(6) Entfernungen zwischen den Landkreisen und kreisfreien Städten und dem Studienort des Landes Brandenburg:

 **Landkreise/kreisfreie Städte**

 **Universität Potsdam**

 Brandenburg

 40

 Cottbus

 110

 Frankfurt (Oder)

 100

 Potsdam

 0

 Barnim

 70

 Dahme-Spreewald

 80

 Elbe-Elster

 80

 Havelland

 60

 Märkisch-Oderland

 90

 Oberhavel

 40

 Oberspreewald-Lausitz

 120

 Oder-Spree

 90

 Ostprignitz-Ruppin

 60

 Potsdam-Mittelmark

 40

 Prignitz

 110

 Spree-Neiße

 130

 Teltow-Fläming

 40

 Uckermark

 110

**Anlage 5  
**(zu § 20 Satz 3)

**Ermittlung der Punktzahl der Gesamtqualifikation**

(1) Bei deutschen Abiturzeugnissen, bei denen die Durchschnittsnote auf der Grundlage einer maximal erreichbaren Punktzahl von 840 errechnet worden ist, ist die auf dem Zeugnis ausgewiesene Punktzahl maßgeblich.

(2) Bei deutschen Abiturzeugnissen, bei denen die Durchschnittsnote auf der Grundlage einer maximal erreichbaren Punktzahl von 900 errechnet worden ist, wird die maßgebliche Punktzahl P nach der Formel: P = (840 x PA) : 900 errechnet; dabei ist PA die auf dem Abiturzeugnis ausgewiesene Gesamtpunktzahl; es wird auf eine ganze Zahl gerundet.

(3) Bei Hochschulzugangsberechtigungen, auf denen keine nach den Beschlüssen der Kultusministerkonferenz errechnete Gesamtpunktzahl ausgewiesen ist, gilt der Mittelwert der Punktspanne, die der jeweiligen Durchschnittsnote nach den Beschlüssen der Kultusministerkonferenz in den Fällen des Absatzes 1 zugeordnet ist, als maßgebliche Punktzahl; es wird auf eine ganze Zahl gerundet.