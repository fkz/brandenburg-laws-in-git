## Verordnung über die Zuständigkeit der Naturschutzbehörden  (Naturschutzzuständigkeitsverordnung - NatSchZustV)

Auf Grund des § 30 Absatz 4 des Brandenburgischen Naturschutzausführungsgesetzes vom 21. Januar 2013 (GVBl.
I Nr. 3) und des § 1 der Naturschutzermächtigungsübertragungsverordnung vom 21.
Mai 2013 (GVBl.
II Nr.
41) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1  
Zuständigkeit der Naturschutzbehörden

(1) Soweit im Brandenburgischen Naturschutzausführungsgesetz oder in dieser Verordnung nichts anderes bestimmt ist, ist die untere Naturschutzbehörde zuständig für die Durchführung des Bundesnaturschutzgesetzes sowie des Brandenburgischen Naturschutzausführungsgesetzes und der auf ihrer Grundlage erlassenen Rechtsvorschriften.
Die oberste Naturschutzbehörde kann bestimmen, dass anstelle einer unteren Naturschutzbehörde eine andere untere Naturschutzbehörde zuständig ist, wenn die Angelegenheit in den Zuständigkeitsbereich mehrerer unterer Naturschutzbehörden fällt.

(2) Für den Vollzug der Vorschriften des Kapitels 5 des Bundesnaturschutzgesetzes über den Schutz der wild lebenden Tier- und Pflanzenarten, ihrer Lebensstätten und Biotope, der Bundesartenschutzverordnung sowie des Artenschutzrechts der Europäischen Gemeinschaft ist, soweit nach § 7 nicht die unteren Naturschutzbehörden zuständig sind, die Fachbehörde für Naturschutz und Landschaftspflege zuständig.
Zuständig für die Zulassung allgemeiner Ausnahmen von den Verboten des § 44 Absatz 1 des Bundesnaturschutzgesetzes durch Rechtsverordnung nach § 45 Absatz 7 Satz 4 des Bundesnaturschutzgesetzes ist das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung.
Trifft die Fachbehörde für Naturschutz und Landschaftspflege eine Entscheidung nach § 45 Absatz 7 Satz 1 Nummer 3 des Bundesnaturschutzgesetzes und sind daneben weitere naturschutzrechtliche Entscheidungen erforderlich, ist für diese ebenfalls die Fachbehörde für Naturschutz und Landschaftspflege zuständig.
Ihr obliegt auch die Vornahme aller in die Zuständigkeit des Landes fallenden Handlungen und Maßnahmen, die sich aus internationalen Verträgen auf dem Gebiet des Naturschutzes ergeben.

(3) Bei Vorhaben, die einer Zulassung durch eine Bundes- oder oberste Landesbehörde oder eine Landesoberbehörde bedürfen, ist die Fachbehörde für Naturschutz und Landschaftspflege für alle naturschutz- einschließlich der artenschutzrechtlichen Entscheidungen und Maßnahmen, die in Bezug auf das Vorhaben zu treffen sind, zuständig; sie ist die zu beteiligende Behörde, soweit die Zulassung konzentrierende Wirkung entfaltet.
Wird ein Vorhaben im Sinne des Satzes 1 auf der Grundlage eines Vorhaben- und Erschließungsplans nach § 12 des Baugesetzbuchs oder eines Bebauungsplans nach § 8 des Baugesetzbuchs zugelassen, ist die Fachbehörde für Naturschutz und Landschaftspflege die zuständige Naturschutzbehörde für die im Zusammenhang mit diesen Planverfahren wahrzunehmenden naturschutzrechtlichen Aufgaben.

### § 2  
Beobachtung von Natur und Landschaft

Zuständig für die Beobachtung von Natur und Landschaft nach § 6 des Bundesnaturschutzgesetzes ist die Fachbehörde für Naturschutz und Landschaftspflege.

### § 3  
Kompensationsverzeichnis

Die Fachbehörde für Naturschutz und Landschaftspflege führt ein Kompensationsverzeichnis im Sinne des § 17 Absatz 6 des Bundesnaturschutzgesetzes.

### § 4  
Schutzausweisungen

(1) Die Rechtsverordnungen zur Unterschutzstellung von Naturschutzgebieten im Sinne des § 23 Absatz 1 des Bundesnaturschutzgesetzes oder Landschaftsschutzgebieten im Sinne des § 26 Absatz 1 des Bundesnaturschutzgesetzes erlässt das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung; die Befugnis kann im Benehmen mit dem für Wirtschaft zuständigen Mitglied der Landesregierung auf die untere Naturschutzbehörde übertragen werden, wenn sich das geplante Naturschutzgebiet oder Landschaftsschutzgebiet auf das Stadt- oder Kreisgebiet beschränkt.
Die Rechtsverordnungen zur Unterschutzstellung von Nationalen Naturmonumenten im Sinne des § 24 Absatz 4 des Bundesnaturschutzgesetzes erlässt die Landesregierung.

(2) Die Rechtsverordnungen zur Unterschutzstellung von Naturdenkmälern im Sinne des § 28 Absatz 1 des Bundesnaturschutzgesetzes erlässt die untere Naturschutzbehörde.

(3) Die Rechtsverordnungen zur Unterschutzstellung geschützter Landschaftsbestandteile im Sinne des § 29 Absatz 1 des Bundesnaturschutzgesetzes erlässt das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung, wenn es sich um den Schutz von Landschaftsbestandteilen für das ganze Land oder für Teile des Landes, die mehrere Kreise umfassen, handelt, im Übrigen die untere Naturschutzbehörde.

(4) Für den Erlass von Rechtsverordnungen nach § 8 Absatz 1 oder § 11 des Brandenburgischen Naturschutzausführungsgesetzes ist in Landkreisen der Kreistag und in kreisfreien Städten die Stadtverordnetenversammlung zuständig.
Für den Erlass einer Verfügung nach § 11 des Brandenburgischen Naturschutzausführungsgesetzes ist in Landkreisen der Landrat oder die Landrätin, in kreisfreien Städten der Oberbürgermeister oder die Oberbürgermeisterin zuständig.

(5) Gebiete, die die Voraussetzungen des § 25 des Bundesnaturschutzgesetzes erfüllen, können durch Bekanntmachung der obersten Naturschutzbehörde zu Biosphärenreservaten erklärt werden.
Durch Bekanntmachung der obersten Naturschutzbehörde können außerdem Gebiete, die die Voraussetzungen des § 27 Absatz 1 des Bundesnaturschutzgesetzes erfüllen, zu Naturparken erklärt werden.

(6) Zuständig für die Ausgliederung nach § 10 des Brandenburgischen Naturschutzausführungsgesetzes, die sonstigen Änderungen oder die Aufhebung einer Rechtsverordnung ist die Behörde, die die Rechtsverordnung erlassen hat.

(7) Die Kennzeichnung der Nationalparke, Biosphärenreservate und Naturparke sowie der innerhalb dieser Gebiete gelegenen Naturschutzgebiete und Landschaftsschutzgebiete gemäß § 22 Absatz 4 des Bundesnaturschutzgesetzes erfolgt durch die jeweilige Großschutzgebietsverwaltung.
Die Kennzeichnung der Nationalen Naturmonumente erfolgt durch die Fachbehörde für Naturschutz und Landschaftspflege.

### § 5  
Natura 2000 (zu § 32 Absatz 5 des Bundesnaturschutzgesetzes)

Für die Aufstellung von Bewirtschaftungsplänen nach § 32 Absatz 5 des Bundesnaturschutzgesetzes und die Durchführung von Pflege- und Entwicklungsmaßnahmen nach § 32 Absatz 3 Satz 3 des Bundesnaturschutzgesetzes ist die Fachbehörde für Naturschutz und Landschaftspflege zuständig.

### § 6  
Schutz bestimmter Biotope

Zuständig für das Führen und Fortschreiben des Verzeichnisses der gesetzlich geschützten Biotope gemäß § 18 Absatz 4 des Brandenburgischen Naturschutzausführungsgesetzes ist die Fachbehörde für Naturschutz und Landschaftspflege.

### § 7  
Vollzug des Artenschutzes durch die untere Naturschutzbehörde

(1) Zuständige Behörde für die Erteilung einer Befreiung nach § 67 Absatz 1 des Bundesnaturschutzgesetzes von den Verboten des § 39 Absatz 1, 5 und 6 des Bundesnaturschutzgesetzes sowie für die Entscheidung über eine Genehmigung nach § 39 Absatz 4 des Bundesnaturschutzgesetzes ist die untere Naturschutzbehörde.
Unterliegen Gehölze nach § 39 Absatz 5 Satz 1 Nummer 2 des Bundesnaturschutzgesetzes dem Schutz einer gemeindlichen Satzung nach § 8 Absatz 2 Satz 1 des Brandenburgischen Naturschutzausführungsgesetzes, entscheidet über die Befreiung nach § 67 Absatz 1 des Bundesnaturschutzgesetzes das Amt oder die amtsfreie Gemeinde.

(2) Zuständig für die Entscheidung über Ausnahmen nach § 45 Absatz 7 Satz 1 Nummer 1, 2, 4 und 5 des Bundesnaturschutzgesetzes von den Verboten des § 44 Absatz 1 des Bundesnaturschutzgesetzes ist die untere Naturschutzbehörde.
Soweit für Entscheidungen nach Satz 1 erforderlich, entscheiden die unteren Naturschutzbehörden auch über die Erteilung von

1.  Ausnahmen nach § 45 Absatz 7 Nummer 1, 2, 4 und 5 des Bundesnaturschutzgesetzes von den Verboten des § 44 Absatz 2 des Bundesnaturschutzgesetzes und
2.  Ausnahmen nach § 4 Absatz 3 Nummer 1 und 2 der Bundesartenschutzverordnung von den Verboten des § 4 Absatz 1 der Bundesartenschutzverordnung.

Soweit eine Ausnahme nach Satz 1 oder Satz 2 nicht erteilt werden kann, ist die untere Naturschutzbehörde auch zuständig für die Entscheidung über Befreiungen nach § 67 Absatz 2 des Bundesnaturschutzgesetzes.
Die Sätze 1 bis 3 gelten nur, soweit sich aus § 1 Absatz 3 nichts Abweichendes ergibt.

(3) Die untere Naturschutzbehörde ist auch zuständig für die Überwachung der Einhaltung der Rechtsvorschriften des Absatzes 1 Satz 1 und des Absatzes 2 Satz 1 bis 3 sowie dafür, die im Einzelfall erforderlichen Maßnahmen zu treffen, um die Einhaltung dieser Vorschriften sicherzustellen.

(4) Für den Vollzug des § 42 des Bundesnaturschutzgesetzes ist die untere Naturschutzbehörde zuständig.

(5) Die untere Naturschutzbehörde nimmt die Anzeigen gemäß § 43 Absatz 3 Satz 1 des Bundesnaturschutzgesetzes entgegen und trifft die erforderlichen Anordnungen nach § 43 Absatz 3 Satz 2 und 3 des Bundesnaturschutzgesetzes.

### § 8  
Ordnungswidrigkeiten

Verwaltungsbehörde im Sinne des § 36 Absatz 1 Nummer 1 des Gesetzes über Ordnungswidrigkeiten ist die Fachbehörde für Naturschutz und Landschaftspflege, soweit sie gemäß § 1 Absatz 2 und 3 zuständig ist, im Übrigen die untere Naturschutzbehörde.

### § 9  
Härte- und Ausgleichsregelung

Wenn den Landkreisen und kreisfreien Städten durch die Wahrnehmung von neuen öffentlichen Aufgaben im Sinne des Artikels 97 Absatz 3 Satz 2 der Verfassung des Landes Brandenburg auf Grund des Bundesnaturschutzgesetzes, des Brandenburgischen Naturschutzausführungsgesetzes und dieser Verordnung trotz aller zumutbarer eigener Anstrengungen Mehrbelastungen entstehen, werden die diesbezüglichen nachgewiesenen Kosten vom Land auf Antrag erstattet.
Die Kostenerstattung kann in pauschalierter Form erfolgen.

### § 10  
Übergangsregelung

Zuständige Naturschutzbehörde für die Verwaltungsverfahren, die zum Zeitpunkt des Inkrafttretens dieser Verordnung noch nicht abgeschlossen waren, ist die Naturschutzbehörde, die vor dem Inkrafttreten dieser Verordnung zuständig war.

### § 11  
Inkrafttreten

Diese Verordnung tritt am 1.
Juni 2013 in Kraft.

Potsdam, den 27.
Mai 2013

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack