## Verordnung über beamtenrechtliche Zuständigkeiten im Justizvollzug des Landes Brandenburg (JVollzZV)

Auf Grund

1.  des § 4 Absatz 1 Satz 2 des Landesbeamtengesetzes in der Fassung der Bekanntmachung vom 3.
    April 2009 (GVBl.
    I S. 26) in Verbindung mit § 1 Absatz 3 der Ernennungsverordnung vom 1.
    August 2004 (GVBl.
    II S.
    742),
    
2.  des § 32 Absatz 1, des § 38 Satz 2, des § 54 Absatz 1 Satz 1, des § 56 Absatz 1 Satz 5, des § 57 Absatz 1 Satz 2, des § 66 Absatz 4, des § 69 Absatz 5 Satz 1, des § 84 Satz 2, des § 85 Absatz 2 Satz 1, des § 86 Absatz 1 Satz 3, des § 87 Satz 1 und Satz 4, des § 88 Satz 5, des § 89 Satz 3, des § 92 Absatz 2 des Landesbeamtengesetzes in Verbindung mit § 9 Absatz 1 Satz 1 des Landesorganisationsgesetzes vom 24.
    Mai 2004 (GVBl.
    I S.
    186),
    
3.  des § 103 Absatz 2 des Landesbeamtengesetzes,
    
4.  des § 54 Absatz 3 Satz 2 des Beamtenstatusgesetzes in der Fassung der Bekanntmachung vom 17.
    Juni 2008 (BGBl.
    I S.
    1010),
    
5.  des § 25 des Landesbeamtengesetzes in Verbindung mit § 39 Absatz 1 der Laufbahnverordnung vom 16.
    September 2009 (GVBl.
    II S.
    622) und in Verbindung mit § 9 Absatz 1 des Landesorganisationsgesetzes,
    
6.  des § 12 Absatz 2 Satz 3, des § 15 Absatz 2 Satz 2 und des § 66 Absatz 1 des Bundesbesoldungsgesetzes in der Fassung der Bekanntmachung vom 19.
    Juni 2009 (BGBl.
    I S.
    1434) in Verbindung mit § 9 Absatz 1 des Landesorganisationsgesetzes,
    
7.  des § 71 des Landesbeamtengesetzes in Verbindung mit § 1 Absatz 1 Satz 3 der Verordnung über den Mutterschutz für Beamtinnen des Bundes und die Elternzeit für Beamtinnen und Beamte des Bundes in Verbindung mit § 8 Absatz 6 des Mutterschutzgesetzes,
    
8.  des § 17 Absatz 2 Satz 2, des § 34 Absatz 5 und des § 42 Absatz 2 Satz 2 des Landesdisziplinargesetzes vom 18.
    Dezember 2001 (GVBl.
    I S.
    254),
    
9.  des § 64 Satz 1 des Landesbeamtengesetzes, der zuletzt durch Artikel 1 des Gesetzes vom 11.
    März 2010 (GVBl.
    I Nr. 13) geändert worden ist, in Verbindung mit § 8 Absatz 1 der Verordnung über die Gewährung von Jubiläumszuwendungen an Beamte und Richter des Bundes in der Fassung der Bekanntmachung vom 13.
    März 1990 (BGBl.
    I S.
    487) und in Verbindung mit § 9 Absatz 1 des Landesorganisationsgesetzes,
    
10.  des § 63 Absatz 1 des Landesbeamtengesetzes in Verbindung mit § 9 Absatz 3 der Trennungsgeldverordnung in der Fassung der Bekanntmachung vom 29.
    Juni 1999 (BGBl.
    I S. 1533),
    
11.  des § 63 Absatz 1 des Landesbeamtengesetzes in Verbindung mit § 8 Satz 1 des Bundesreisekostengesetzes vom 26.
    Mai 2005 (BGBl.
    I S.
    1418) und § 9 Absatz 1 des Landesorganisationsgesetzes
    

verordnet der Minister der Justiz:

#### § 1  
Dienstvorgesetzter

(1) Das für Justiz zuständige Mitglied der Landesregierung ist Dienstvorgesetzter im Sinne von § 2 Absatz 2 Satz 1 des Landesbeamtengesetzes der Leiter der Justizvollzugsanstalten, des Leiters der Sicherungsverwahrungsvollzugseinrichtung, ihrer Stellvertreter, der Vollzugsleiter sowie der Beamten des höheren allgemeinen Verwaltungsdienstes.
Dienstvorgesetzte der übrigen Beamten bei den Justizvollzugsanstalten sind die Leiter der Justizvollzugsanstalten jeweils für ihren Zuständigkeitsbereich.

(2) Soweit die Leiter der Justizvollzugsanstalten Dienstvorgesetzte nach Absatz 1 sind, sind die Justizvollzugsanstalten Personalakten führende Stellen für die ihnen zugeordneten Beamten.

#### § 2  
Übertragung von Befugnissen nach dem Landesbeamtengesetz

(1) Die der obersten Dienstbehörde zustehende Befugnis zur Ernennung der Beamten wird den Leitern der Justizvollzugsanstalten für ihren jeweiligen Geschäftsbereich übertragen.
Von der Übertragung ausgenommen ist die Befugnis zur Ernennung der Stellvertreter der Leiter der Justizvollzugsanstalten, der Stellvertreter des Leiters der Sicherungsverwahrungsvollzugseinrichtung, der Vollzugsleiter sowie der Beamten des höheren allgemeinen Verwaltungsdienstes.

(2) Den in Absatz 1 genannten Stellen werden für ihren Geschäftsbereich folgende weitere Zuständigkeiten übertragen:

1.  Entscheidungen über das Vorliegen der Voraussetzungen der Entlassung kraft Gesetzes gemäß § 32 Absatz 1 des Landesbeamtengesetzes,
    
2.  die Entscheidungen über die Versetzung eines Beamten auf Probe in den Ruhestand gemäß § 38 des Landesbeamtengesetzes,
    
3.  die Entscheidung über das Verbot der Führung der Dienstgeschäfte nach § 54 Absatz 1 des Landesbeamtengesetzes,
    
4.  die Entscheidung über die Versagung der Aussagegenehmigung nach § 56 Absatz 1 Satz 3 und 4 des Landesbeamtengesetzes; die Versagung der Aussagegenehmigung bedarf der Zustimmung der obersten Dienstbehörde,
    
5.  die Entscheidung über Ausnahmen vom Verbot der Annahme von Belohnungen oder Geschenken nach § 57 des Landesbeamtengesetzes mit der Maßgabe, dass die Entscheidung dem jeweiligen Leiter der betreffenden Dienststelle oder dessen Stellvertreter persönlich obliegt,
    
6.  die Bewilligung von Umzugskostenvergütung nach § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes,
    
7.  die Erteilung der Erlaubnis zur Führung der Amtsbezeichnung mit dem Zusatz „außer Dienst“ („a.
    D.“) nach § 69 Absatz 5 des Landesbeamtengesetzes,
    
8.  Entscheidungen auf dem Gebiet des Nebentätigkeitsrechts nach den §§ 84, 85 Absatz 2 Satz 1, § 86 Absatz 1 Satz 1, §§ 87, 88 Satz 2 und 3 und § 89 Satz 1 des Landesbeamtengesetzes,
    
9.  die Untersagung einer Erwerbstätigkeit oder sonstigen Beschäftigung nach Beendigung des Beamtenverhältnisses nach § 92 Absatz 2 des Landesbeamtengesetzes.
    

#### § 3  
Sonstige Übertragungen

Den in § 2 Absatz 1 genannten Stellen werden für die ihnen zugeordneten Beamten, für die sie Dienstvorgesetzte nach § 1 sind, folgende weitere Befugnisse der obersten Dienstbehörde übertragen:

1.  die Feststellung der Befähigung für eine Laufbahn besonderer Fachrichtung des mittleren und gehobenen Dienstes nach § 39 der Laufbahnverordnung,
    
2.  Entscheidungen gemäß § 71 des Landesbeamtengesetzes in Verbindung mit § 1 Absatz 1 der Verordnung über den Mutterschutz für Beamtinnen des Bundes und die Elternzeit für Beamtinnen und Beamte des Bundes und in Verbindung mit § 8 Absatz 6 des Mutterschutzgesetzes,
    
3.  die Zustimmung zum Absehen von der Rückforderung von Bezügen nach § 12 Absatz 2 Satz 3 des Bundesbesoldungsgesetzes,
    
4.  die Entscheidung über die Anweisung des dienstlichen Wohnsitzes nach § 15 Absatz 2 des Bundesbesoldungsgesetzes,
    
5.  die Ausübung der Disziplinarbefugnis bei Ruhestandsbeamten nach § 17 Absatz 2 des Landesdisziplinargesetzes,
    
6.  die Kürzung der Dienstbezüge bis zum Höchstmaß nach § 34 Absatz 3 Nummer 1 des Landesdisziplinargesetzes,
    
7.  die Gewährung und Versagung von Jubiläumszuwendungen nach § 64 Satz 1 des Landesbeamtengesetzes in Verbindung mit § 8 Absatz 1 der Verordnung über die Gewährung von Jubiläumszuwendungen an Beamte und Richter des Bundes.
    

#### § 4  
_(aufgehoben)_

#### § 5  
Vorverfahren und Vertretung des Dienstherrn

(1) Die der obersten Dienstbehörde zustehende Befugnis zur Entscheidung über den Widerspruch eines Beamten, eines Ruhestandsbeamten, eines früheren Beamten oder der Hinterbliebenen gegen Entscheidungen aus dem Beamtenverhältnis und des Dienstherrn wird den in § 2 Absatz 1 und § 4 genannten Stellen übertragen, soweit sie die mit dem Widerspruch angefochtene Entscheidung erlassen haben (§ 54 Absatz 3 des Beamtenstatusgesetzes).
Dies gilt auch für die Befugnis zum Erlass des Widerspruchsbescheides nach § 42 Absatz 2 des Landesdisziplinargesetzes.

(2) Die Vertretung des Landes bei Klagen aus dem Beamtenverhältnis und des Dienstherrn nach § 54 des Beamtenstatusgesetzes wird den in Absatz 1 genannten Stellen übertragen, soweit sie über den Widerspruch entschieden haben oder hätten entscheiden müssen.
In Verfahren zur Erlangung einstweiligen Rechtsschutzes nach der Verwaltungsgerichtsordnung gilt Satz 1 entsprechend.

#### § 6  
Übergangsvorschriften

Soweit vor Inkrafttreten dieser Verordnung andere als in den §§ 2 bis 5 festgelegte Zuständigkeiten bestanden haben, verbleibt es für die im Zeitpunkt des Inkrafttretens dieser Verordnung anhängigen Verwaltungsverfahren bei den bisherigen Zuständigkeiten.
Gleiches gilt hinsichtlich der Zuständigkeit für die Vertretung in zu diesem Zeitpunkt bereits anhängigen Rechtsstreitigkeiten.

#### § 7  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über beamtenrechtliche Zuständigkeiten im Justizvollzug des Landes Brandenburg vom 26.
April 2006 (GVBl.
II S.
102) außer Kraft.

Potsdam, den 8.
September 2010

Der Minister der Justiz

Dr.
Volkmar Schöneburg