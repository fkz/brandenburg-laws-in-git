## Verordnung über das Naturschutzgebiet „Magerrasen Schönwalde“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl. I S. 2542) in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nr. 43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Dahme-Spreewald wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Magerrasen Schönwalde“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 12 Hektar.
Es umfasst Flächen in folgender Flur:

Gemeinde:

Gemarkung:

Flur:

Schönwald

Schönwalde

6.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in der topografischen Karte im Maßstab 1 : 10 000 mit dem Titel „Topografische Karte zur Verordnung über das Naturschutzgebiet ‚Magerrasen Schönwalde‘“ und in der Liegenschaftskarte im Maßstab 1 : 2 500 mit dem Titel „Liegenschaftskarte zur Verordnung über das Naturschutzgebiet ‚Magerrasen Schönwalde‘“ mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die topografische Karte mit der Blattnummer 1 ermöglicht die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Liegenschaftskarte mit der Blattnummer 1.
Die Karten sind von der Siegelverwahrerin, Siegelnummer 13 des Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft am 14.
September 2016 unterzeichnet worden.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 2 beigefügt.

(3) Innerhalb des Naturschutzgebietes wird gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes eine Zone 1 mit weiteren Regelungen der Nutzung festgesetzt.
Die Zone 1 umfasst rund 4,3 Hektar und liegt in der Gemeinde Schönwald, Gemarkung Schönwalde, Flur 6.
Die Grenzen der Zone 1 sind in der in Absatz 1 genannten Kartenskizze und in der topografischen Karte sowie in der Liegenschaftskarte, die in Absatz 2 genannt sind, mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Liegenschaftskarte.

(4) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Dahme-Spreewald, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere von Eichenwäldern, Trockenrasen, Staudenfluren und Säumen trockenwarmer Standorte sowie Vorwaldstadien;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Heide-Nelke (Dianthus deltoides), Sand-Strohblume (Helichrysum arenarium), Sumpf-Schwertlilie (Iris pseudacorus), Rautenfarn (Botrychium ssp.) und Ohrlöffel-Leimkraut (Silene otitis);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere Vögel und Reptilien, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Wiedehopf und Zauneidechse;
4.  die Erhaltung und Entwicklung des Magerrasenkomplexes in überwiegender Ausprägung als Sandrasen mit Übergängen zu frischen bis feuchten Standorten im Baruther Urstromtal als Teil des landesweiten und regionalen Biotopverbundes.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Magerrasen Schönwalde“ und von Teilen des Gebietes von gemeinschaftlicher Bedeutung „Magerrasen Schönwalde Ergänzung“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit ihren Vorkommen von

1.  Alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur als natürlichem Lebensraumtyp von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Vorblattlosem Vermeinkraut (Thesium ebracteatum) als Art von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich seiner Lebensräume und den für seine Reproduktion erforderlichen Standortbedingungen.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet, seinen Naturhaushalt oder einzelne seiner Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten.
    Ausgenommen ist das Betreten zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 6;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb von Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, und außerhalb der Waldbrandwundstreifen zu reiten;
11.  mit Fahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
13.  Hunde frei laufen zu lassen;
14.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
15.  Düngemittel aller Art zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
16.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
17.  Tiere zu füttern oder Futter bereitzustellen;
18.  Tiere auszusetzen oder Pflanzen anzusiedeln;
19.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
20.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
21.  Pflanzenschutzmittel jeder Art anzuwenden;
22.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die im Sinne des § 5 Absatz 2 des Bundesnaturschutzgesetzes ordnungsgemäße landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verzehrenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und Sekundärrohstoffdünger einzusetzen und § 4 Absatz 2 Nummer 21 und 22 gilt, bei Narbenschäden ist eine umbruchlose Nachsaat zulässig.  
          
        Sekundärrohstoffdünger im Sinne dieser Verordnung sind Abwasser, Fäkalien, Klärschlamm und ähnliche Stoffe aus Siedlungsabfällen und vergleichbare Stoffe aus anderen Quellen, jeweils auch weiterbehandelt und in Mischungen untereinander oder mit Düngemitteln, Wirtschaftsdünger, Bodenhilfsstoffen, Kultursubstraten und Pflanzenhilfsmitteln,
    2.  bei einer ackerbaulichen Nutzung der in den in § 2 Absatz 2 genannten Karten gekennzeichneten Flächen der Flurstücke 19, 20 und 226 (jeweils anteilig) der Gemarkung Schönwalde, Flur 6 keine chemisch-synthetischen Düngemittel, Gülle sowie keine Herbizide oder Insektizide eingesetzt werden,
    3.  die Flächen der Zone 1 nicht beweidet werden.
        § 4 Absatz 2 Nummer 15, 21 und 22 gilt;
2.  die im Sinne des § 5 Absatz 3 des Bundesnaturschutzgesetzes ordnungsgemäße forstwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  die Walderneuerung in der Zone 1 nur durch Naturverjüngung erfolgt,
    2.  außerhalb der Zone 1 nur Arten der potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur gebietsheimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
    3.  eine Nutzung des in § 3 Absatz 2 genannten Waldlebensraumes in der Zone 1 einzelstamm- bis truppweise durchgeführt wird und nur in der Zeit vom 1.
        Oktober eines jeden Jahres bis 28.
        Februar des Folgejahres zulässig ist; gefällte Bäume und Gehölzteile sind umgehend von der Fläche zu entfernen,
    4.  keine Horst- oder Höhlenbäume entfernt werden,
    5.  Neuaufforstungen unzulässig sind,
    6.  § 4 Absatz 2 Nummer 15 und 21 gilt;
3.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd,
    2.  die Errichtung ortsunveränderlicher sowie transportabler und mobiler Ansitzeinrichtungen außerhalb der Zone 1.
    
    Im Übrigen bleibt die Anlage von Kirrungen, Wildwiesen, Wildäckern und Ablenkfütterungen in der Zone 1 und innerhalb geschützter Biotope gemäß § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes unzulässig.
    Jagdrechtliche Regelungen nach § 41 des Brandenburgischen Jagdgesetzes bleiben unberührt;
    
4.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen;
5.  die rechtmäßige Ausübung der Angelfischerei;
6.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch;
7.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter die Nummer 8 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
8.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer.
    Unterhaltungsmaßnahmen sind ab dem 15.
    September eines jeden Jahres zulässig; sie sind auf den Schutzzweck des § 3 Absatz 2 abzustellen.
    Dabei ist zu beachten, dass das Mäh- und Räumgut nicht länger als drei Tage in Zone 1 abgelagert wird.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
9.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen.
    Die ordnungsgemäße Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen herstellt werden;
10.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
11.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung nach Anzeige gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes bei der unteren Naturschutzbehörde;
12.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
13.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Tourismus im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl.
    S. 1734), die durch die Bekanntmachung vom 1.
    Oktober 2013 (ABl.
    S. 2811) geändert worden ist, an Straßen und Wegen freigestellt;
14.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestands und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungsbefugnissen, die sich aus anderen fachrechtlichen Vorgaben ergeben, unberührt.

#### § 6 Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  die Sukzession auf Habitaten des Vorblattlosen Vermeinkrauts soll durch herbstliche Mahd und mosaikartige Bodenverwundung gesteuert werden; aufkommende Gehölze mit Tendenzen zu Waldbeständen sollen zurückgedrängt werden;
2.  die Nutzung der Zone 1 soll durch Mahd mit einem Freischneider sowie anschließende Beräumung des Mähgutes ab dem 15.
    September eines jeden Jahres erfolgen.
    In den ersten drei Jahren nach Inkrafttreten der Verordnung soll zur Aushagerung des Standortes und zur Begrenzung starkwüchsiger Ruderalarten eine jährliche Mahd und Beräumung im Zeitraum zwischen dem 16.
    August und dem 30.
    August eines jeden Jahres durchgeführt werden;
3.  das Walzen und Schleppen von Grünland soll im Zeitraum vom 15.
    März bis zur ersten Nutzung eines jeden Jahres unterbleiben;
4.  die Ackerflächen in der Zone 1 und südwestlich der Bahn (Flur 6, Flurstücke 19, 20) sollen in extensive Grünlandflächen beziehungsweise Ackerbrache umgewandelt werden.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

§ 5 Absatz 1 Nummer 1 tritt am 1.
Januar 2017 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 26.
Oktober 2016

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes "Magerrasen Schönwalde"](/br2/sixcms/media.php/68/GVBl_II_58_2016-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes ") 825.7 KB

2

[Anlage 2 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet "Magerrasen Schönwalde"](/br2/sixcms/media.php/68/GVBl_II_58_2016-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet "Magerrasen Schönwalde"") 583.4 KB