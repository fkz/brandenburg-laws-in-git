## Verordnung über das Naturschutzgebiet „Dömnitz“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl. I S. 2542), von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl. I S. 1972) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 und § 42 Absatz 2 Satz 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21. Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27. Mai 2013 (GVBl.
II Nr. 43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Prignitz wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Dömnitz“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 160 Hektar.
Es umfasst Flächen in folgenden Fluren:

Stadt/Gemeinde:

Gemarkung:

Flur:

Pritzwalk

Beveringen

1, 2 und 7;

Falkenhagen

2;

Pritzwalk

3, 4, 5, 9 und 10;

Sadenbeck

1, 2, 3 und 4;

Halenbeck-Rohlsdorf

Rohlsdorf (S)

6.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 4 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 10 aufgeführten Liegenschaftskarten.
Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 3 beigefügt, die gemäß Absatz 4 hinterlegt wird.

(3) Die Grenze des in § 3 Absatz 2 aufgeführten Gebietes von gemeinschaftlicher Bedeutung „Dömnitz“ (ehemals Teil des Gebietes von gemeinschaftlicher Bedeutung „Stepenitz“) ist in den in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 4 sowie in den in Anlage 2 Nummer 2 genannten Liegenschaftskarten mit den Blattnummern 1 bis 10 mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.

(4) Die Verordnung mit Karten und Flurstücksliste kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Prignitz, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes, das als Teil des Fließgewässersystems der Stepenitz im Prignitzer Platten- und Hügelland einen Abschnitt der Dömnitz sowie fließgewässerbegleitende naturnahe Waldflächen und Grünlandflächen umfasst, ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere von Erlenbruch- und Erlen-Eschen-Wäldern, Quellen, Schwimmblatt- und Wasserpflanzengesellschaften, Röhrichten, gewässerbegleitenden Hochstaudenfluren und Baumreihen, Quellmoorbereichen, einem nährstoffreichen Moor sowie von Großseggen- und Feuchtwiesen;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Breitblättriger Sitter (Epipactis helleborine) und Sumpf-Schwertlilie (Iris pseudacorus);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der an die Forellen- und Äschenregion gebundene Neunaugen und Fischarten sowie der Vögel, Amphibien und Insekten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Eisvogel (Alcedo atthis), Schwarzspecht (Dryocopus martius) und Moorfrosch (Rana arvalis), Knoblauchkröte (Pelobates fuscus), Grasfrosch (Rana temporaria), Kleiner Wasserfrosch (Rana lessonae), Edelkrebs (Astacus astacus) und Kleiner Eisvogel (Limenitis camilla);
4.  die Erhaltung und Entwicklung der Dömnitz als Teil des Fließgewässersystems der Stepenitz aus wissenschaftlichen Gründen zur Beobachtung und Erforschung der tierischen und pflanzlichen Lebensgemeinschaften eines naturnahen Gewässersystems sowie der Abläufe im Rahmen einer naturnahen Wiederherstellung;
5.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit des Gebietes mit seinen naturnahen Gewässerläufen und Waldbeständen.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Dömnitz“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), das ehemals einen Teil des Gebietes von gemeinschaftlicher Bedeutung „Stepenitz“ umfasste, mit seinen Vorkommen von

1.  Flüssen der planaren Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Feuchten Hochstaudenfluren der planaren Stufe, Hainsimsen-Buchenwald (Luzulo-Fagetum), Mitteleuropäischem Stieleichenwald oder Hainbuchenwald (Stellario-Carpinetum) sowie Alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur (Stiel-Eiche) als natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Auen-Wäldern mit Alnus glutinosa (Schwarz-Erle) und Fraxinus excelsior (Gewöhnliche Esche) (Alno-Padion, Alnion incanae, Salicion albae) als prioritärem natürlichen Lebensraumtyp von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
3.  Fischotter (Lutra lutra), Bachneunauge (Lampetra planeri), Westgroppe (Cottus gobio), Lachs (Salmo salar), Kleiner Flussmuschel (Unio crassus), Schmaler Windelschnecke (Vertigo angustior) und Bauchiger Windelschnecke (Vertigo moulinsiana) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten des Gebietes außerhalb von Bruchwäldern und Röhrichten zum Zweck der Erholung, im Rahmen der Bildungsangebote der Waldnaturwacht Hainholz sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 10;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  zu baden oder zu tauchen;
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel aller Art zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland innerhalb des Gebietes von gemeinschaftlicher Bedeutung „Dömnitz“ als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertende Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle, Jauche, Gärreste oder Sekundärrohstoffdünger einzusetzen.  
          
        Sekundärrohstoffdünger im Sinne dieser Verordnung sind Abwasser, Fäkalien, Klärschlamm und ähnliche Stoffe aus Siedlungsabfällen und vergleichbare Stoffe aus anderen Quellen, jeweils auch weiterbehandelt und in Mischungen untereinander oder mit Düngemitteln, Wirtschaftsdünger, Bodenhilfsstoffen, Kultursubstraten und Pflanzenhilfsmitteln.
    2.  bei Beweidung Gehölze und Gewässerufer an der Böschungsoberkante sowie Quellen und Quellbereiche in einem Abstand von 5 Metern zum äußeren Rand der quelligen Bereiche auszuzäunen sind,
    3.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt.
        Bei Narbenschäden ist mit Zustimmung der unteren Naturschutzbehörde eine umbruchlose Nachsaat zulässig,
    4.  bei der Nutzung der Ackerflächen der Einsatz von chemisch-synthetischen Düngemitteln, Gülle, Jauche, Gärresten, Herbiziden und Insektiziden unzulässig ist;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  die Walderneuerung auf den in § 3 Absatz 2 Nummer 1 und 2 genannten Waldlebensraumtypen durch Naturverjüngung erfolgt.
        Sofern keine ausreichende Naturverjüngung erfolgt, dürfen nur lebensraumtypische Arten eingebracht werden.
        In die übrigen Waldflächen sind nur Arten der potenziell natürlichen Vegetation einzubringen.
        Es sind nur heimische Baumarten in lebensraumtypischen Anteilen unter Ausschluss eingebürgerter Arten zu verwenden,
    2.  in den in § 3 Absatz 2 Nummer 1 und 2 genannten Waldlebensraumtypen eine Nutzung ausschließlich einzelstamm- bis truppweise erfolgt, wobei ein Bestockungsgrad von 0,7 nicht unterschritten werden darf.
        Auf den übrigen Waldflächen sind Holzerntemaßnahmen, die den Holzvorrat auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von 0,5 Hektar zulässig,
    3.  mindestens fünf Stämme je Hektar mit einem Durchmesser von mindestens 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum Absterben aus der Nutzung genommen sein müssen,
    4.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 30 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden; liegendes Totholz (ganze Bäume mit Durchmesser von über 65 Zentimetern am stärksten Ende) verbleibt im Bestand,
    5.  hydromorphe Böden nur bei gefrorenem Boden mit ausreichender Tragfähigkeit sowie Böden mit einem hohen Anteil an feinkörnigem Substrat nur bei Frost oder in Trockenperioden auf dauerhaft festgelegten Rückegassen befahren werden,
    6.  Horst- und Höhlenbäume nicht entfernt werden,
    7.  Kalkung auf den Flächen der Waldlebensraumtypen Hainsimsen-Buchenwald und Alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur unzulässig ist,
    8.  § 4 Absatz 2 Nummer 17 und 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  die in § 3 Absatz 2 Nummer 3 genannten Arten Westgroppe (Cottus gobio), Bachneunauge (Lampetra planeri), Kleine Flussmuschel (Unio crassus) sowie Edelkrebs (Astacus astacus) ganzjährig geschont werden,
    2.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters und Bibers weitgehend ausgeschlossen ist,
    3.  Hegepläne im Einvernehmen mit der unteren Naturschutzbehörde zu erstellen sind,
    4.  die Elektrofischerei in Bereichen mit Vorkommen des Edelkrebses nur mit Zustimmung der unteren Naturschutzbehörde erfolgt;
4.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass
    1.  die in § 3 Absatz 2 Nummer 3 genannten Arten Westgroppe (Cottus gobio), Bachneunauge (Lampetra planeri), Kleine Flussmuschel (Unio crassus) sowie der Edelkrebs (Astacus astacus) ganzjährig geschont werden,
    2.  die Angelfischerei nur vom Ufer aus erfolgt,
    3.  in der Dömnitz einschließlich der Nebengewässer der Fang von Köderfischen, auch der nicht geschützten Arten, verboten ist und das Angeln nur in der Zeit vom 16.
        April bis 30.
        November eines jeden Jahres und ausschließlich unter Verwendung künstlicher Köder erfolgt,
    4.  § 4 Absatz 2 Nummer 19 und 20 gilt;
5.  für den Bereich der Jagd:
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Fallenjagd mit Lebendfallen erfolgt und bis zu einem Abstand von 100 Metern zum Gewässerufer der Dömnitz einschließlich der Nebengewässer verboten ist,
        
        bb)
        
        keine Baujagd bis zu einem Abstand von 100 Metern zum Gewässerufer der Dömnitz einschließlich der Nebengewässer vorgenommen wird,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
        Mögliche Standorte transportabler und mobiler Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Aufstellung zu Beginn eines Jagdjahres anzuzeigen.
        Die untere Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        Die Entscheidung hierüber soll unverzüglich erfolgen,
    3.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und der in § 3 Absatz 2 genannten Lebensraumtypen,
    4.  Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und die Anlage von Wildäckern bleiben unzulässig;
6.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter die Nummer 8 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
7.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
8.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der un​teren Naturschutzbehörde zulässig; das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
9.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
10.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch;
11.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung nach Anzeige gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes bei der unteren Naturschutzbehörde;
12.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der zuständigen Naturschutzbehörde zugelassen oder angeordnet worden sind;
13.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Tourismus im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl.
    S. 1734), die durch die Bekanntmachung vom 1.
    Oktober 2013 (ABl.
    S. 2811) geändert worden ist, an Straßen und Wegen freigestellt;
14.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungserfordernissen, die sich aus anderen Vorgaben ergeben, unberührt.

#### § 6 Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  Unterhaltungsmaßnahmen der Gewässer sollen unter Beachtung der Lebensraumfunktion für die Kleine Flussmuschel und den Edelkrebs erfolgen.
    Bei der Krautung soll nicht in das Substrat eingegriffen werden;
2.  naturferne Abschnitte der Dömnitz einschließlich der Nebengewässer sollen renaturiert werden, beispielsweise durch Förderung der natürlichen Gewässerdynamik und den Rückbau oder Umbau von Querbauwerken.
    Die Durchgängigkeit der Dömnitz sowie geeigneter Zuflüsse zur Dömnitz für Fische und andere Gewässerorganismen soll wiederhergestellt werden;
3.  Altarme der Fließgewässer sollen wieder an das Fließgewässersystem angeschlossen werden;
4.  geeignete Uferabschnitte der Fließgewässer sollen zur Erhaltung und Verbesserung des Lebensraumes von Bachmuschel und Edelkrebs mit heimischen, standortgerechten Gehölzen zur partiellen Beschattung bepflanzt werden;
5.  zur Reduzierung der Belastung sollen Einleitungen von Straßenentwässerungen in die Fließgewässer über Versickerungsmulden beziehungsweise in den Ablauf eingebaute Überlaufschwellen erfolgen.
    Vorhandene Anlagen sollen entsprechend nachgerüstet werden;
6.  Nadelholzreinbestände sollen in standortgerechte und stabile Mischbestände überführt werden;
7.  nicht standortgerechte sowie nicht heimische Baum- und Straucharten sollen aus Wäldern und Baumreihen entfernt werden;
8.  gewässerbegleitende Ackerflächen sollen zur Reduzierung von Stoffeinträgen in die Fließgewässer in Dauergrünland umgewandelt werden beziehungsweise als Uferrandstreifen oder Dauerstilllegungsflächen eingerichtet werden;
9.  Grünlandbrachen sollen wieder in eine extensive Nutzung übernommen oder gemäht und innerhalb von zehn Tagen beräumt werden.
    Grünland außerhalb des Gebietes von gemeinschaftlicher Bedeutung soll extensiv genutzt werden;
10.  feuchte Hochstaudenfluren sollen alternierend in mehrjährigem Abstand gemäht und innerhalb von zehn Tagen beräumt werden.
    Dabei ist die Gehölzsukzession zu unterbinden.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten, Außerkrafttreten

(1) § 5 Absatz 1 Nummer 1 Buchstabe a und d tritt am 1.
Januar 2019 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

(2) Für die im Geltungsbereich dieser Verordnung liegenden und in den in Anlage 2 Nummer 1 aufgeführten topografischen Karten mit den Blattnummern 1, 2 und 4 gekennzeichneten Flächen tritt gleichzeitig die Verordnung über das Naturschutzgebiet „Sadenbecker Brandhorst“ vom 21.
Mai 1997 (GVBl.
II S. 419) außer Kraft.

Potsdam, den 26.
März 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes "Dömnitz"](/br2/sixcms/media.php/68/GVBl_II_24_2018-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes ") 687.4 KB

2

[Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten](/br2/sixcms/media.php/68/GVBl_II_24_2018-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten") 206.7 KB