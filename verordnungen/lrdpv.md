## Verordnung über den Landesrettungsdienstplan  (Landesrettungsdienstplanverordnung - LRDPV)

Auf Grund des § 7 Absatz 1 Satz 1 und des § 20 des Brandenburgischen Rettungsdienstgesetzes vom 14.
Juli 2008 (GVBl.
I S. 186) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz im Einvernehmen mit dem Minister des Innern:

Inhaltsübersicht
----------------

### Abschnitt 1  
Allgemeine Regelungen

[§ 1    Einsatzgrundsätze](#1)  
[§ 2    Abgrenzung von Primäreinsätzen und Sekundärverlegungen](#2)  
[§ 3    Hilfsfrist](#3)  
[§ 4    Fehlfahrten und Fehleinsätze](#4)

### Abschnitt 2  
Rettungsfahrzeuge und Qualifikation des Personals im Rettungsdienst

[§ 5    Boden- und luftgebundene Rettungsfahrzeuge](#5)  
[§ 6    Qualifikation des Personals und Besetzung der Rettungsfahrzeuge](#6)  
[§ 7    Fortbildung des Personals](#7)

### Abschnitt 3  
Ärztliche Leitung des Rettungsdienstbereiches

[§ 8    Ärztliche Leitung](#8)  
[§ 9    Aufgaben der Ärztlichen Leitung](#9)  
[§ 10  Qualitätssicherung](#10)

### Abschnitt 4  
Besondere Aufgaben und Standorte der Luftrettung

[§ 11  Rettungshubschrauber](#11)  
[§ 12  Intensivtransporthubschrauber](#12)

### Abschnitt 5  
Massenanfall von verletzten oder erkrankten Personen

[§ 13  Allgemeines](#13)  
[§ 14  Maßnahmeplan MANV](#14)  
[§ 15  Bereichsübergreifende Einsätze](#15)  
[§ 16  Durchführung des Maßnahmeplanes MANV](#16)  
[§ 17  Führungsorganisation](#17)  
[§ 18  Aufgaben der rettungsdienstlichen Leitung](#18)  
[§ 19  Qualifikation der rettungsdienstlichen Leitung](#19)

### Abschnitt 6  
Inkrafttreten, Außerkrafttreten

[§ 20  Inkrafttreten, Außerkrafttreten](#20)

### Abschnitt 1  
Allgemeine Regelungen

#### [](#01)§ 1  
Einsatzgrundsätze

(1) Durch die integrierte Regionalleitstelle ist in der Notfallrettung das jeweils nächste freie, geeignete und verfügbare öffentliche Rettungsfahrzeug einzusetzen (Nächstes-Fahrzeug-Strategie).
Zu diesem Zweck arbeiten die integrierten Regionalleitstellen zusammen und unterstützen sich gegenseitig, insbesondere bei einem Schadensereignis mit einem Massenanfall von verletzten und erkrankten Personen (MANV).
Die Träger des bodengebundenen Rettungsdienstes können für ihre Notarzteinsatzfahrzeuge und Rettungswagen spezielle Regelungen bezüglich der Vorschrift in Satz 1 vorsehen.

(2) In Abstimmung mit den Trägern des Rettungsdienstes können durch die integrierten Regionalleitstellen zur Verkürzung der Eintreffzeit in der Notfallrettung georeferenzierte Verfahren zur Bestimmung des nächsten freien, geeigneten und verfügbaren öffentlichen Rettungsfahrzeugs verwendet werden.
Auf der Durchfahrt befindliche Rettungsfahrzeuge aus fremden Rettungsdienstbereichen sollen hierbei nur eine notfallmedizinische Erstversorgung bis zum Eintreffen eines örtlich zuständigen Rettungsfahrzeugs durchführen.
Die Träger des Rettungsdienstes können für ihre Zuständigkeitsbereiche Ausnahmen zulassen.

(3) Notfallrettungen (Primäreinsätze) haben Vorrang vor allen anderen Rettungsdiensteinsätzen.
Bei einem Primäreinsatz mit Notarztindikation ist grundsätzlich die Notärztin oder der Notarzt einzusetzen, die oder der den Notfallort am schnellsten erreichen kann.

(4) Helfer vor Ort sind nicht Teil des öffentlichen Rettungsdienstes und können die zuständigen Kräfte nicht ersetzen.
Sie können den Rettungsdienst nur unterstützen, in dem sie therapiefreie Intervalle verkürzen oder qualifizierte Erste Hilfe leisten.
Die Träger des Rettungsdienstes erfüllen durch die Heranziehung von Helfern vor Ort nicht ihre Sicherstellungsaufträge.

(5) Zur Erleichterung der Einsatzentscheidungen der integrierten Regionalleitstellen sind von den zuständigen Trägern des Rettungsdienstes bereichsübergreifend Übersichten zu erstellen, aus denen für jeden Notfallort die ermittelte Eintreffzeit der nächstgelegenen boden- und luftgebundenen Rettungsfahrzeuge ablesbar ist.
Auf dieser Grundlage ist eine Notruf-Alarmierungsfolge festzulegen, die für jeden Notfallort eine Abmarschfolge für mindestens je drei Rettungswagen und notarztbesetzte Rettungsfahrzeuge vorsieht.

(6) Notrufe, die unter der Notrufnummer 112 eingehen, werden von den integrierten Regionalleitstellen regelmäßig unter Verwendung einer standardisierten Notrufabfrage beantwortet.

(7) Eine sofortige Alarmierung eines Rettungshubschraubers ist immer dann erforderlich, wenn eine Notarztindikation vorliegt und

1.  die Notärztin oder der Notarzt den Notfallort am schnellsten erreichen kann oder
2.  eine medizinische Indikation für einen Lufttransport besteht oder
3.  wenn mehrere Verletzte notärztlich zu versorgen sind oder weitere Notärztinnen oder Notärzte am Notfallort benötigt werden.

#### [](#02)§ 2  
Abgrenzung von Primäreinsätzen und Sekundärverlegungen

(1) Die Notfallrettung (Primäreinsatz) umfasst die präklinische Notfallversorgung einschließlich der notärztlichen Versorgung (Primärversorgung) und den Notfalltransport (Primärtransport) von Notfallpatientinnen und Notfallpatienten in eine geeignete medizinische Versorgungseinrichtung.
Dazu gehören auch Primärverlegungen (Notverlegungen), wenn die Beförderungen von Notfallpatientinnen oder Notfallpatienten aus einer Gesundheitseinrichtung zur Weiterversorgung in ein Krankenhaus, das über die Möglichkeiten zur jeweils indizierten besonderen notfallmedizinischen Versorgung verfügt, keinen Aufschub duldet, weil beim Absehen von der Verlegung entweder Lebensgefahr besteht oder unmittelbar schwere gesundheitliche Schäden drohen.
In Absprache mit der jeweiligen Klinik findet die ärztliche Begleitung durch die diensthabende Notärztin beziehungsweise den diensthabenden Notarzt oder durch eine Klinikärztin beziehungsweise einen Klinikarzt statt.

(2) Der ärztlich begleitete Patiententransport (Sekundärverlegung), der in der Regel zur Beförderung von stationär behandelten Patientinnen oder Patienten in eine weiterführende diagnostische oder therapeutische Gesundheitseinrichtung durchgeführt wird, umfasst als besondere Form des qualifizierten Krankentransports Fahrten, bei denen eine Person aus zwingenden medizinischen Gründen des Einsatzes geeigneter und besonders ausgerüsteter Rettungsfahrzeuge sowie einer ärztlichen Betreuung und Überwachung bedarf.
Die abgebende Gesundheitseinrichtung hat dabei die ärztliche Begleitung zu gewährleisten.
Die jeweilige Dringlichkeit einer Sekundärverlegung wird gegebenenfalls in einem Gespräch zwischen dem beabsichtigten Verlegungsarzt und einer Ärztin beziehungsweise einem Arzt der abgebenden Einrichtung geklärt.

(3) Zeitkritische Transporte von Arzneien, Blutkonserven und medizinischen Geräten sowie der für die Notfalleinsätze benötigten Personen stehen einem Primärtransport gleich, sofern diese Transporte nicht anderweitig zeitgerecht sichergestellt werden können.

#### [](#03)§ 3  
Hilfsfrist

(1) Die Hilfsfrist nach § 8 Absatz 2 des Brandenburgischen Rettungsdienstgesetzes ist der Zeitraum, der in der Notfallversorgung mit dem Eingang der Notfallmeldung in der integrierten Regionalleitstelle beginnt und mit dem Eintreffen des ersten geeigneten Rettungsfahrzeugs am Einsatzort an der öffentlichen Straße endet.
Die Notfallmeldung ist eingegangen, wenn alle erforderlichen Informationen in der integrierten Regionalleitstelle vorliegen, um diese Meldung als Notfall zu klassifizieren.
Dieser Zeitpunkt ist in einem elektronischen Einsatzleitsystem messbar zu dokumentieren.

(2) Anhand der Dokumentationen sind durch die Träger des bodengebundenen Rettungsdienstes die erreichten Hilfsfristen auszuwerten und die Planungen für die Einsätze zur Notfallrettung im Rettungsdienstbereich zu überprüfen.
Bei der Berechnung des Erfüllungsgrades der Hilfsfrist bleiben folgende Einsätze außer Betracht:

1.  Einsätze zur Notfallrettung während der Bewältigung von MANV-Lagen sowohl im betroffenen als auch in den hilfeleistenden Rettungsdienstbereichen,
2.  Einsätze mit fehlender Statusmeldung für die Eintreffzeit am Einsatzort,
3.  Einsätze in benachbarten Landkreisen, kreisfreien Städten oder Ländern der Bundesrepublik Deutschland und
4.  Verlegungsfahrten nach § 14 Absatz 8 des Brandenburgischen Rettungsdienstgesetzes einschließlich Notverlegungen aus Krankenhäusern der Grund- und Regelversorgung.

(3) Als Basis von Maßnahmen zur Qualitätssicherung sind durch die Träger der integrierten Regionalleitstellen auch die folgenden Zeitabschnitte zu dokumentieren und auszuwerten:

1.  Rufannahmezeit beziehungsweise Wartezeit,
2.  Gesprächszeit von Annahme des Anrufes bis zur Einsatzentscheidung,
3.  technische Alarmlaufzeit,
4.  Anfahrzeit bis zum Einsatzort.

#### [](#04)§ 4  
Fehlfahrten und Fehleinsätze

(1) Fehlfahrten im Sinne von § 17 Absatz 4 Satz 2 Nummer 8 des Brandenburgischen Rettungsdienstgesetzes sind Einsätze in der Notfallrettung, bei denen eine Fahrt zum Einsatzort erfolgt ist, eine Beförderung jedoch auf Grund nicht vorhandener medizinischer Indikation nicht durchgeführt wurde.
Eine Fehlfahrt liegt auch vor, wenn die Person, für die der Notruf erfolgte, beim Eintreffen des Rettungsdienstes bereits verstorben ist und der Rettungsdienst nicht mehr tätig werden konnte.

(2) Fehleinsätze im Sinne von § 17 Absatz 4 Satz 2 Nummer 8 des Brandenburgischen Rettungsdienstgesetzes sind Einsätze von Rettungsfahrzeugen, bei denen die den Einsatz verursachende Person am Notfallort nicht angetroffen wird.

### Abschnitt 2  
Rettungsfahrzeuge und Qualifikation des Personals im Rettungsdienst

#### [](#05)§ 5  
Boden- und luftgebundene Rettungsfahrzeuge

(1) Die Rettungsfahrzeuge des bodengebundenen Rettungsdienstes sind Rettungswagen, Notarztwagen, Notarzteinsatzfahrzeuge und Krankentransportwagen.
Darüber hinaus für die Bewältigung besonderer Aufgaben im Rettungsdienst benötigte Fahrzeuge können ebenfalls Fahrzeuge des bodengebundenen Rettungsdienstes sein.
Rettungsfahrzeuge müssen für ihren Einsatzzweck in geeigneter Weise ausgestattet und eingerichtet sein.
Ausstattung und Einrichtung der Rettungsfahrzeuge müssen den jeweils geltenden Normen entsprechen.
Fahrzeuge des Katastrophen- und Zivilschutzes sind keine Fahrzeuge des bodengebundenen Rettungsdienstes.

(2) Rettungswagen sind bestimmt für den bedarfsgerechten Primäreinsatz oder für Sekundärtransporte.
Rettungswagen, die zusätzlich mit einem Notarzt besetzt werden, sind Notarztwagen (Stationssystem).

(3) Das Notarzteinsatzfahrzeug dient dem schnellen Heranführen einer Notärztin oder eines Notarztes an den Notfallort und ermöglicht mit der mitgeführten medizinisch-technischen Ausrüstung die Primärversorgung von Notfallpatientinnen und Notfallpatienten.
Ein Notarzteinsatzfahrzeug ist für die notärztliche Versorgung mehrerer Rettungswachenbereiche zuständig und fährt getrennt vom Rettungswagen zum Notfallort (Rendezvoussystem).

(4) Verlegungs-Notarzteinsatzfahrzeuge (V-NEF) dienen insbesondere als Zubringer eines qualifizierten Verlegungsarztes und des medizinischen Equipments für Verlegungen, bei denen eine ärztliche Versorgung notwendig ist.

(5) Verlegungs-Rettungswagen (V-RTW) sind für Verlegungsfahrten speziell ausgerüstete Rettungswagen, die im Rendezvoussystem mit den Verlegungs-Notarzteinsatzfahrzeugen insbesondere ärztlich begleitete Verlegungen durchführen.

(6) Krankentransportwagen sind nur im Ausnahmefall, beispielsweise bei MANV-Ereignissen, für den Transport von Notfallpatienten bestimmt.

(7) Die Anzahl und die Art der vorzuhaltenden Rettungs- und Krankentransportfahrzeuge sind nach § 8 Absatz 1 Nummer 2 des Brandenburgischen Rettungsdienstgesetzes in einem Rettungsdienstbereichsplan von den Trägern des bodengebundenen Rettungsdienstes festzulegen.

(8) Im bodengebundenen Rettungsdienst sind zusätzlich zu den im Rettungsdienstbereich vorzuhaltenden Rettungsfahrzeugen bedarfsgerecht jederzeit einsatzbereite Rettungsfahrzeuge zur Überbrückung von planmäßigen und außerplanmäßigen Reparatur-, Wartungs-, Umrüstungs- und Desinfektionsarbeiten vorzuhalten.
Im Bedarfsfall sind diese Rettungsfahrzeuge einzusetzen zur

1.  Bewältigung eines temporär erhöhten Einsatzaufkommens,
2.  Verstärkung der Regelvorhaltungen im Fall von MANV-Ereignissen und
3.  Abdeckung von Transportanforderungen für Arzneimittel, Blutkonserven, Transplantaten und Medizingeräten zur Notfallrettung, soweit die Transporte nicht anderweitig sichergestellt werden können.

(9) Die Träger des Rettungsdienstes treffen die hierzu erforderlichen Festlegungen.
Die Reserven an jederzeit einsatzbereiten bodengebundenen Rettungsfahrzeugen sollen insgesamt 25 Prozent der regulär vorgehaltenen Fahrzeuge umfassen.
Sie dürfen die Anzahl von drei Fahrzeugen (Mindestreserve) nicht unterschreiten.
Die Mindestreserve muss einen Rettungswagen und ein Notarzteinsatzfahrzeug enthalten.
Als Reservefahrzeuge sind Rettungsfahrzeuge zu nutzen, bei denen die gewöhnliche Nutzungsdauer überschritten ist, die aber jederzeit einsetzbar sind.

(10) Zur Erfüllung der Aufgaben nach § 2 Absatz 1 des Brandenburgischen Rettungsdienstgesetzes können von den Trägern des Rettungsdienstes Spezialfahrzeuge, insbesondere zum Transport von Infektionspatienten, übergewichtigen Patienten, Intensivbetten, Inkubatoren oder für den Transport von Führungspersonal und Notfallausrüstungen für MANV-Ereignisse vorgehalten und eingesetzt werden.
Das Land gewährt nach Maßgabe des Haushaltsplanes den Trägern des bodengebundenen Rettungsdienstes Zuwendungen für die Beschaffung sowie Unterhaltung dieser Fahrzeuge.

(11) Luftrettungsfahrzeuge sind notarztbesetzte Rettungshubschrauber und Intensivtransporthubschrauber.
Deren Ausstattung und Einrichtung muss den jeweils geltenden Normen entsprechen.

(12) Über notwendige Ausnahmen zu den jeweiligen Einsatzmöglichkeiten der Rettungsfahrzeuge entscheiden die integrierten Regionalleitstellen im Einzelfall.

#### [](#06)§ 6  
Qualifikation des Personals und Besetzung der Rettungsfahrzeuge

(1) Im Rettungsdienst darf nur tätig werden, wer für anfallende Aufgaben neben der beruflichen Qualifikation die notwendige gesundheitliche und persönliche Eignung besitzt.

(2) Notarzteinsatzfahrzeuge müssen mit einer Notärztin oder einem Notarzt und einer Notfallsanitäterin/Rettungsassistentin oder einem Notfallsanitäter/Rettungsassistenten als Fahrerin oder Fahrer besetzt sein.
Notärztinnen und Notärzte im Sinne dieser Verordnung und des Brandenburgischen Rettungsdienstgesetzes sind solche Ärztinnen und Ärzte, die über eine Qualifikation nach § 14 Absatz 1 Satz 3 des Brandenburgischen Rettungsdienstgesetzes verfügen.

(3) Rettungswagen sind mit mindestens zwei fachlich geeigneten Personen zu besetzen.
Mindestens eine dieser Personen muss eine Ausbildung zur Notfallsanitäterin oder zum Notfallsanitäter erfolgreich abgeschlossen haben.
Die zweite Person muss mindestens die Ausbildung zur Rettungssanitäterin oder zum Rettungssanitäter erfolgreich durchlaufen haben.
Im Regelfall soll diese Person den Rettungswagen fahren.

(4) Krankentransportwagen sind mit zwei Personen zu besetzen, die mindestens als Rettungssanitäterin oder Rettungssanitäter qualifiziert sind.
Satz 1 gilt nicht für Krankentransportwagen des Katastrophen- oder Zivilschutzdienstes.

(5) Luftrettungsfahrzeuge müssen mit einer Notärztin oder einem Notarzt, einer Notfallsanitäterin oder einem Notfallsanitäter und einer Pilotin oder einem Piloten besetzt sein.
Rettungshubschrauber sind mit erfahrenen Notfallsanitäterinnen oder Notfallsanitätern zu besetzen, die mindestens drei Jahre im bodengebundenen Rettungsdienst tätig waren und über die Qualifikation als HEMS-Crew-Member nach den Luftverkehrsvorschriften verfügen.
Ärztinnen oder Ärzte auf Intensivtransporthubschraubern müssen aus einer Fachrichtung mit Bezug zur Intensivmedizin kommen, an einer Fortbildung Intensivtransport teilgenommen haben und sich regelmäßig intensivmedizinisch fortbilden.
Notfallsanitäterinnen oder Notfallsanitäter auf Intensivtransporthubschraubern müssen an einer Fortbildung Intensivtransport teilgenommen haben und sich regelmäßig intensivmedizinisch fortbilden.

(6) Die für das Rettungswesen zuständige oberste Landesbehörde kann auf begründeten Antrag des Aufgabenträgers im Einzelfall Ausnahmen von den Qualifikationsanforderungen der Absätze 2 bis 5 zulassen.

(7) Für die Qualifikationsanforderungen gilt folgende Übergangsregelung:

Bis zum 31.
Dezember 2022 können anstelle von Notfallsanitäterinnen und Notfallsanitätern auch Rettungsassistentinnen und Rettungsassistenten eingesetzt werden.

#### [](#07)§ 7  
Fortbildung des Personals

Wer die Notfallrettung und den qualifizierten Krankentransport betreibt, ist verpflichtet, für eine regelmäßige Fortbildung des einzusetzenden Personals zu sorgen.
Die Fortbildung muss den jeweils aktuellen medizinischen und technischen Anforderungen gerecht werden.
Der Mindestumfang der rettungsdienstlichen Fortbildung beträgt 32 Stunden im Kalenderjahr für jede Mitarbeiterin und jeden Mitarbeiter im Rettungsdienst.
Disponentinnen und Disponenten haben zusätzlich jährlich ein Praktikum von 80 Stunden auf Rettungsdienstfahrzeugen abzuleisten.
§ 6 Absatz 6 gilt entsprechend.

### Abschnitt 3  
Ärztliche Leitung des Rettungsdienstbereiches

#### [](#08)§ 8   
Ärztliche Leitung

(1) Der Ärztlichen Leiterin oder dem Ärztlichen Leiter des Rettungsdienstes obliegt die fachliche Anleitung und Kontrolle über das gesamte im Zuständigkeitsbereich des Trägers des bodengebundenen Rettungsdienstes eingesetzte medizinische Personal.
Die Ärztliche Leitung des Rettungsdienstes ist in medizinischen Fragen und Belangen gegenüber dem ärztlichen sowie dem nichtärztlichen Personal im Rettungsdienst weisungsbefugt.
Die mit der Ärztlichen Leitung des Rettungsdienstes beauftragte Person soll zumindest mit beratender Stimme an den Sitzungen des jeweiligen Rettungsdienstbereiches teilnehmen.
Sie nimmt regelmäßig an Notarztdiensten sowie an Bereitschaftsdiensten der Leitenden Notärztinnen und Notärzte teil und leitet die ihr dienstlich unterstellten Personen an.

(2) Ist die Ärztliche Leiterin oder der Ärztliche Leiter des Rettungsdienstes bei einem Krankenhausträger beschäftigt, so schließt der Träger des Rettungsdienstes mit dem Krankenhausträger eine schriftliche Vereinbarung über die Einzelheiten der Personalgestellung.
§ 14 Absatz 3 Satz 2 des Brandenburgischen Rettungsdienstgesetzes gilt entsprechend.

#### [](#09)§ 9  
Aufgaben der Ärztlichen Leitung

Über die Regelungen in § 15 des Brandenburgischen Rettungsdienstgesetzes hinaus nimmt die Ärztliche Leitung unter Beachtung und Einhaltung der geltenden Datenschutzbestimmungen folgende Aufgaben wahr:

1.  Beratung des Trägers des bodengebundenen Rettungsdienstes bei der Sicherstellung der erforderlichen Qualität und Leistungsfähigkeit der rettungsmedizinischen Betreuung sowie bei der Erstellung des Rettungsdienstbereichsplanes,
2.  Feststellung des Standes der Qualität im medizinischen Bereich des Rettungsdienstes, die Bewertung der eingeführten Qualitätssicherungsmaßnahmen auf ihre Wirksamkeit und die Erarbeitung von Vorgaben beziehungsweise Empfehlungen für die Qualitätssicherung im Rettungsdienst.
    Zur Erfüllung dieser Aufgaben kann die Ärztliche Leitung von den im Rettungsdienst mitwirkenden Personen und Stellen Auskünfte, Aufzeichnungen und Dokumentationen in geeigneter Form verlangen.
    Diese sollen hinsichtlich der konkreten Patientendaten anonymisiert sein, jedoch das Patientenalter und Geschlecht enthalten.
    Die Ärztliche Leitung berichtet regelmäßig über die Auswertung der Qualitätssicherungsmaßnahmen an den Träger des Rettungsdienstes nach § 15 Absatz 1 Nummer 3 des Brandenburgischen Rettungsdienstgesetzes.
3.  Beratung des Trägers des Rettungsdienstes bei Beschwerden; die Ärztliche Leitung kann hierzu alle erforderlichen medizinischen Daten einer Patientin oder eines Patienten einsehen,
4.  Festlegung der Leistungsanforderungen für die einzusetzende rettungsmedizinische Ausrüstung und deren Überwachung im Betrieb,
5.  Beratung des Trägers des Rettungsdienstes bei der Auswahl des geeigneten ärztlichen und nichtärztlichen Personals und Mitwirkung bei der Anwendung von Einsatztauglichkeitskriterien für das Personal,
6.  Überwachung der notfallmedizinischen Versorgung von Notfallpatientinnen und Notfallpatienten durch das ärztliche und nichtärztliche Personal und Erarbeitung von Empfehlungen für das ärztliche Personal und von Handlungsrichtlinien für das nichtärztliche Personal im Rettungsdienst,
7.  Festlegung von medizinischen Behandlungsstandards für bestimmte notfallmedizinische Zustandsbilder und -situationen sowie die daraus resultierende Delegation heilkundlicher Maßnahmen im Sinne von § 4 Absatz 2 Nummer 2 Buchstabe c des Notfallsanitätergesetzes vom 22.
    Mai 2013 (BGBl.
    I S.
    1348), das durch Artikel 40 des Gesetzes vom 15. August 2019 (BGBl.
    I S.
    1307, 1333) geändert worden ist, in der jeweils geltenden Fassung, an Notfallsanitäterinnen und Notfallsanitäter,
8.  Überwachung und Optimierung der Einsatzstrategien und der Bearbeitung des Einsatzgeschehens in der Regionalleitstelle und Fortschreibung von Strategien für die Bearbeitung medizinischer Hilfeersuchen,
9.  Mitglied im Regionalleitstellenbeirat,
10.  Mitwirkung bei der Auswahl geeigneter Schutzkleidung und Überwachung der Einhaltung der Hygiene- und Arbeitsschutzvorschriften,
11.  Überwachung und Festlegung der rettungsmedizinischen Aus-, Weiter- und Fortbildung der Mitarbeiterinnen und Mitarbeiter im Rettungsdienst,
12.  Beratung des Trägers des Rettungsdienstes bei der Planung, Vorbereitung und Durchführung von Maßnahmen bei Schadensereignissen mit einem Massenanfall von Verletzten sowie Erkrankten.

#### [](#010)§ 10  
Qualitätssicherung

(1) Der Träger des bodengebundenen Rettungsdienstes legt auf Empfehlung der Ärztlichen Leitung die Dokumentationsinstrumente zur elektronischen Erfassung relevanter notfallmedizinischer Daten fest.
Diese müssen mindestens den von der Deutschen Interdisziplinären Vereinigung für Intensiv- und Notfallmedizin e.
V.
empfohlenen Minimalen Notfalldatensatz in der jeweils aktuellen Version umfassen.

(2) Die Ärztliche Leitung legt Folgendes fest:

1.  Methodenauswahl für die Analyse der medizinischen Daten,
2.  medizinische Bewertung der Datenanalyse und des Berichtswesens und
3.  medizinische Behandlungsrichtlinien für nichtärztliches Personal im Rettungsdienst.

### Abschnitt 4  
Besondere Aufgaben und Standorte der Luftrettung

#### [](#011)§ 11  
Rettungshubschrauber

(1) Rettungshubschrauber sind grundsätzlich von Sonnenaufgang, frühestens 7 Uhr MEZ, bis Sonnenuntergang einsatzbereit zu halten.
Der Einsatzradius eines Rettungshubschraubers für Primäreinsätze beträgt in der Regel 50 Kilometer und im Ausnahmefall 80 Kilometer.

(2) Rettungshubschrauber dürfen Primärverlegungen nach § 2 Absatz 1 Satz 2 immer durchführen und Sekundärverlegungen nach § 2 Absatz 2 nur, wenn keine Notfalleinsätze gemeldet sind und die vorausberechnete Einsatzzeit höchstens zwei Stunden beträgt.

(3) Rettungshubschrauber sind im Land Brandenburg an folgenden Standorten stationiert:

Luftrettungsstandort

Funkrufname

Versorgungsgebiet

Angermünde

Christoph 64

Barnim, Oberhavel, Märkisch-Oderland, Ostprignitz-Ruppin, Uckermark, südöstliches Mecklenburg-Vorpommern

Bad Saarow

Christoph 49

Frankfurt (Oder), Barnim, Dahme-Spreewald, Märkisch-Oderland, Oder-Spree, Teltow-Fläming

Brandenburg an der Havel

Christoph 35

Brandenburg an der Havel, Landeshauptstadt Potsdam, Havelland, Oberhavel, Ostprignitz-Ruppin, Potsdam-Mittelmark, Teltow-Fläming

Perleberg

Christoph 39

Havelland, Ostprignitz-Ruppin, Prignitz, südwestliches Mecklenburg-Vorpommern, nördliches Sachsen-Anhalt, nordöstliches Niedersachsen

Senftenberg

Christoph 33

Cottbus, Dahme-Spreewald, Elbe-Elster, Oberspreewald-Lausitz, Spree-Neiße, Nordostsachsen

(4) Der Rettungshubschrauber Christoph 35 wird vom Bund als Zivilschutzhubschrauber bereitgestellt.
Auf Anforderung der Katastrophenschutzbehörden des Landes Brandenburg steht der Zivilschutzhubschrauber auch für Zwecke des Zivil- und Katastrophenschutzes zur Verfügung.

#### [](#012)§ 12  
Intensivtransporthubschrauber

(1) Intensivtransporthubschrauber sind für Sekundärverlegungen immer dann einzusetzen, wenn der Einsatzradius eines Rettungshubschraubers nach § 11 Absatz 1 überschritten wird, die voraussichtliche Einsatzzeit mehr als zwei Stunden beträgt oder in die Nachtstunden fällt.
Für Sekundärverlegungen, die den Einsatzradius nach § 11 Absatz 1 nicht überschreiten, sollen Intensivtransporthubschrauber nur eingesetzt werden, wenn der für den Standort vorgehaltene Rettungshubschrauber im Einsatz ist und die Verlegung keinen zeitlichen Aufschub gestattet oder der Rettungshubschrauber wegen seiner Ausstattung für die Verlegung ungeeignet ist.

(2) Zur Notfallrettung sind Intensivtransporthubschrauber nur dann einzusetzen, wenn der Rettungshubschrauber im Einsatz oder nicht einsatzbereit ist oder sonstige geeignete Rettungsfahrzeuge innerhalb der Hilfsfrist nicht zur Verfügung stehen.

(3) Im Land Brandenburg ist ein Intensivtransporthubschrauber (Funkrufname: Christoph Brandenburg) an der Luftrettungsstation Senftenberg stationiert.

### Abschnitt 5  
Massenanfall von verletzten oder erkrankten Personen

#### [](#013)§ 13  
Allgemeines

Die Versorgung von Notfallpatientinnen und Notfallpatienten bei einem Schadensereignis mit einem Massenanfall von verletzten oder erkrankten Personen zählt unabhängig vom auslösenden Ereignis zu den Aufgaben des Rettungsdienstes.
Die Träger des Rettungsdienstes haben anhand der Grundsätze dieses Abschnitts Planungen vorzunehmen und Vorbereitungen zu treffen.
Diese sind in einem Maßnahmeplan MANV zu dokumentieren.

#### [](#014)§ 14  
Maßnahmeplan MANV

(1) Der Maßnahmeplan MANV ist vom Träger des Rettungsdienstes im Rahmen des integrierten Hilfeleistungssystems an den unter Absatz 3 festgelegten Stufen und den jeweils zugeordneten Schutzzielen auszurichten.
Er ist im Rettungsdienstbereich mit den Brand- und Katastrophenschutzbehörden, den jeweiligen benachbarten Trägern des Rettungsdienstes, der Regionalleitstelle und den für die weiteren Hilfeleistungspotenziale Verantwortlichen abzustimmen sowie durch Übungen zu erproben.
Die Untersetzung definierter Versorgungsstufen mit konkreten Planungen erfolgt in jedem Rettungsdienstbereich entsprechend den örtlichen Ressourcen.
Der Maßnahmeplan MANV ist Teil des Rettungsdienstbereichsplanes und als ereignisbezogener Sonderplan Bestandteil der Katastrophenschutzpläne nach § 39 des Brandenburgischen Brand- und Katastrophenschutzgesetzes.

(2) Im Maßnahmeplan MANV ist das vereinbarte Zusammenwirken mit den Krankenhäusern und anderen beteiligten Gesundheitseinrichtungen darzustellen und zu dokumentieren.
Die Regionalleitstellen führen zur Informations- und Dispositionsunterstützung einen Nachweis der jeweils verfügbaren Aufnahme- und Versorgungskapazitäten der Krankenhäuser.

(3) Die Versorgungsstufen und Schutzziele für den MANV werden wie folgt festgelegt:

Stufe

Versorgung gemäß Gefährdungsstufe

Schutzziel MANV

1

normierter alltäglicher Schutz

Hilfeleistung für Schadensereignisse mit mehreren Verletzten, die im Rahmen des Regelrettungsdienstes bewältigt werden können

2

standardisierter, flächendeckender Grundschutz

Hilfeleistung für Schadensereignisse mit MANV, die mittels besonderer Vorgehensweise mit den Potenzialen des betroffenen Rettungsdienstbereiches einschließlich Reserven und Hinzuziehung von überörtlicher Luftrettung, Kräften und Mitteln der benachbarten Rettungsdienstbereiche und des Katastrophenschutzes in angemessener Zeit bewältigt werden können

3

erhöhter Schutz für gefährdete Regionen und Einrichtungen

Hilfeleistung für Schadensereignisse mit MANV, die nicht mit den Potenzialen des Grundschutzes abzudecken sind und die zur Bewältigung insbesondere zusätzliche standardisierte Hilfeleistungspotenziale des Rettungsdienstes und des Katastrophenschutzes erfordern

4

Sonderschutz mit Hilfe von Spezialkräften

Hilfeleistung für Schadensereignisse mit MANV, die den zusätzlichen Einsatz von Spezialkräften des Bundes, den überregionalen Einsatz von Hilfeleistungskapazitäten aus anderen Bundesländern und gegebenenfalls Potenziale aus anderen Staaten erfordert

(4) Die Träger des bodengebundenen Rettungsdienstes sind verpflichtet, eine aktuelle Übersicht über die jeweils vorgehaltenen Hilfeleistungspotenziale zu führen und diese den anderen Trägern des Rettungsdienstes und den Katastrophenschutzbehörden zur Verfügung zu stellen.

(5) Aus den vorhandenen Einsatzmitteln des Regelrettungsdienstes sollen Leistungskomponenten so definiert, zusammengestellt und im Maßnahmeplan MANV dokumentiert werden, dass damit sowohl eine effektive Bewältigung eines MANV-Ereignisses im eigenen Bereich als auch eine effektive überörtliche Hilfeleistung ermöglicht wird.

(6) Die Träger des bodengebundenen Rettungsdienstes haben das für die Versorgung von Notfallpatientinnen und Notfallpatienten bei einem MANV notwendige Personal und die erforderliche materielle Ausstattung für den Grundschutz vorzuhalten und für den bedarfsnotwendigen Transport zum Einsatzort zu sorgen.
Hierzu zählen insbesondere das rettungsdienstliche Führungspersonal, technisch-logistische Ausstattungen für die Erstversorgung an den Einsatzstellen, Kennzeichnungsmittel und geeignete persönliche Schutzausrüstungen für die Einsatzkräfte, Patientendokumentationssysteme sowie Arzneimittel und Medizinprodukte (Sanitätsmaterialien).
Die Planung über die Vorhaltungen und deren Transport zum Einsatzort ist im Maßnahmeplan MANV zu dokumentieren.

#### [](#015)§ 15  
Bereichsübergreifende Einsätze

(1) Zur Bewältigung eines Schadensereignisses mit einem Massenanfall von verletzten oder erkrankten Personen haben sich die Träger des Rettungsdienstes gegenseitig zu unterstützen.
Bereichsübergreifende Einsätze von Einsatzmitteln des Rettungsdienstes bei einem Schadensereignis mit einem Massenanfall von verletzten oder erkrankten Personen erfolgen unter Beachtung des Maßnahmeplanes MANV.

(2) Wenn zur Schadensbewältigung die bereichsübergreifende Unterstützung durch eine Vielzahl an Rettungsmitteln erforderlich ist, sollen aus der rettungsdienstlichen Regelvorhaltung durch die integrierten Regionalleitstellen vorrangig Leistungskomponenten nach § 14 Absatz 5 alarmiert werden, die ein effizientes Zusammenwirken insbesondere der Potenziale des Rettungsdienstes und des Brand- und Katastrophenschutzes gewährleisten.

(3) Die Regelversorgung in der Notfallrettung kann im hilfeleistenden Rettungsdienstbereich nach Maßgabe der Planungen eingeschränkt werden.
Die Einschränkung beinhaltet eine vorübergehende Aussetzung der Hilfsfrist nach § 8 Absatz 2 des Brandenburgischen Rettungsdienstgesetzes.
Für die Aufrechterhaltung der Notfallversorgung während der Bewältigung des MANV-Ereignisses sowohl im betroffenen als auch im hilfeleistenden Rettungsdienstbereich soll im Maßnahmeplan MANV eine bestimmte Anzahl an Rettungsmitteln vorgesehen werden.
Durch Aktivierung von rettungsdienstlichen Reserven und Inanspruchnahme von Amtshilfe aus weiteren benachbarten Rettungsdienstbereichen soll die Einschränkung der Regelversorgung gering gehalten werden.

#### [](#016)§ 16  
Durchführung des Maßnahmeplanes MANV

(1) Bei einem Schadensereignis mit Massenanfall von verletzten oder erkrankten Personen löst die integrierte Regionalleitstelle, im Bedarfsfall im Zusammenwirken mit anderen integrierten Regionalleitstellen, den MANV-Alarm lageabhängig, stufenbezogen in einzelnen oder mehreren Rettungsdienstbereichen des Landes Brandenburg aus.
Die verfügbaren Einsatz- und Behandlungskapazitäten der Rettungsdienstbereiche sind unter Beachtung von § 15 einzusetzen.

(2) Über die Verstärkungen des Rettungsdienstes wird im Einzelfall auf der Grundlage des Maßnahmeplanes MANV entschieden.
Zur Verstärkung sollen rettungsdienstliche Reserven, Leistungskomponenten benachbarter Rettungsdienstbereiche, Luftrettungsmittel, Einsatzmittel des Katastrophenschutzes und der Feuerwehren, organisationseigene Potenziale der Hilfsorganisationen sowie geeignete Hilfskräfte weiterer Stellen herangezogen werden.

(3) Die Durchführung des rettungsdienstlichen Einsatzes gemäß Maßnahmeplan MANV ist durch die rettungsdienstliche Einsatzleitung nach § 17 Absatz 1 anzuweisen und zu koordinieren.

#### [](#017)§ 17  
Führungsorganisation

(1) Bei einem MANV-Ereignis übernimmt die ersteintreffende Notärztin oder der ersteintreffende Notarzt mit Unterstützung der Rettungsassistentin oder Notfallsanitäterin beziehungsweise des Rettungsassistenten oder des Notfallsanitäters eines vor Ort befindlichen Rettungsfahrzeuges vorläufig die rettungsdienstliche Einsatzleitung kommissarisch.
Die zuständige integrierte Regionalleitstelle hat sofort das rettungsdienstliche Führungspersonal zu alarmieren.
Dies sind Leitende Notärztinnen und Notärzte (LNA) als notärztliche und Organisatorische Leiterinnen und Leiter Rettungsdienst (OrgL) als organisatorische Leitung.

(2) Die Träger des bodengebundenen Rettungsdienstes haben für ihren Zuständigkeitsbereich eine ausreichende Anzahl an LNA und OrgL zu bestellen, deren Einsatzbereitschaft zu gewährleisten und die zuständige integrierte Regionalleitstelle über die Erreichbarkeiten zu unterrichten.
Die Alarmierung des rettungsdienstlichen Führungspersonals erfolgt auf der Grundlage besonderer Einsatzstichwörter.
Die schnellstmögliche Beförderung des LNA und des OrgL zum Einsatzort ist sicherzustellen und hat unverzüglich, ohne schuldhafte Verzögerung zu erfolgen.

#### [](#018)§ 18  
Aufgaben der rettungsdienstlichen Leitung

(1) Im Fall eines MANV-Ereignisses obliegt der notärztlichen Leitung die medizinische Führung und Koordination des MANV-Einsatzes.
Diese umfasst insbesondere folgende Aufgaben:

1.  die medizinische Lagebeurteilung hinsichtlich der Schadensart, des Schadensumfangs, der möglichen Folgegefährdung sowie der erforderlichen Hilfeleistungspotenziale,
2.  die Schwerpunktbestimmung und Priorisierung des Ressourceneinsatzes auf der Basis von Sichtungen sowie die Organisation der Notfallversorgung,
3.  den Einsatz und die Anweisung des rettungs- und sanitätsdienstlichen Personals,
4.  die Koordination der Zusammenarbeit zwischen Rettungsdienst, Sanitätsdienst und weiterem für die Patientenbehandlung herangezogenen Personal,
5.  die Festlegung der notfallmedizinischen Versorgung, der Transportmittel und -ziele,
6.  die Veranlassung, Koordination und Überwachung der festgelegten Maßnahmen als Mitglied der Einsatzleitung in ständiger Abstimmung mit dieser.

(2) Der organisatorischen Leitung obliegt dabei die Unterstützung der notärztlichen Leitung insbesondere durch die Wahrnehmung folgender organisatorisch-technischer Führungs- und Koordinierungsaufgaben:

1.  die rettungsdienstliche Lagebeurteilung aus taktisch-organisatorischer Sicht und die Anforderung der erforderlichen Hilfeleistungskapazitäten in Abstimmung mit der notärztlichen Leitung,
2.  der ständige Kontakt zur Einsatzleitung beziehungsweise zur integrierten Regionalleitstelle,
3.  die Festlegung der Raumordnung in Abstimmung mit der notärztlichen Leitung und der Einsatzleitung,
4.  der Aufbau der rettungs- und sanitätsdienstlichen Infrastruktur an der Einsatzstelle,
5.  die Organisation und Koordinierung des Personal- und Materialeinsatzes im Einsatzabschnitt Patientenbehandlung,
6.  die Organisation des Patiententransports nach den Vorgaben der notärztlichen Leitung,
7.  die Durchführung oder Delegation der Patientenregistrierung und -dokumentation.

#### [](#019)§ 19  
Qualifikation der rettungsdienstlichen Leitung

(1) Für die rettungsdienstliche Einsatzleitung nach § 17 Absatz 1 sind von den Trägern des bodengebundenen Rettungsdienstes LNA sowie OrgL in ausreichender Anzahl auszubilden.
Im Rahmen der Ausbildung sowie von Schulungen und Übungen hat der Träger des bodengebundenen Rettungsdienstes sicherzustellen, dass das rettungsdienstliche Führungspersonal in die Einsatzleitung nach § 9 des Brandenburgischen Brand- und Katastrophenschutzgesetzes eingebunden werden kann.

(2) LNA und OrgL müssen über mehrjährige rettungsdienstliche Einsatzerfahrung und umfassende Kenntnisse der regionalen Organisation und Leistungsfähigkeit des Rettungsdienstes, des Katastrophenschutzes und des Gesundheitswesens verfügen.

(3) Die mit der notärztlichen Leitung zu betrauende Person muss über die Qualifikation als Leitende Notärztin oder Leitender Notarzt entsprechend den Empfehlungen der Bundesärztekammer verfügen, die Voraussetzungen des § 14 Absatz 1 Satz 3 des Brandenburgischen Rettungsdienstgesetzes aufweisen sowie im Notarztdienst tätig sein.

(4) Die mit der organisatorischen Leitung des Rettungsdienstes zu betrauende Person sollte über die Qualifikation einer Notfallsanitäterin/eines Notfallsanitäters oder einer Rettungsassistentin oder eines Rettungsassistenten verfügen, mehrjährig im Rettungsdienst tätig sein sowie einen OrgL-Lehrgang erfolgreich absolviert haben.

### Abschnitt 6  
Inkrafttreten, Außerkrafttreten

#### [](#020)§ 20  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über den Landesrettungsdienstplan des Landes Brandenburg vom 24. Februar 1997 (GVBl.
II S. 106) außer Kraft.

Potsdam, den 24.
Oktober 2011

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack