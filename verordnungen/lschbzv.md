## Verordnung zur Festlegung der Schulbezirke für Berufe nach dem Berufsbildungsgesetz und der Handwerksordnung im Land Brandenburg (Landesschulbezirksverordnung - LSchBzV)

Auf Grund des § 106 Absatz 5 Satz 1 Nummer 2 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
August 2002 (GVBl.
I S. 78) verordnet der Minister für Bildung, Jugend und Sport nach Anhörung der beteiligten Schulträger:

#### § 1 Schulbezirke und Klassenbildung

(1) Für Fachklassen in anerkannten Ausbildungsberufen gemäß § 4 des Berufsbildungsgesetzes und § 25 der Handwerksordnung an Oberstufenzentren im Land Brandenburg werden Schulbezirke festgelegt.
Grundsätzlich werden nur das 1.
und 2.
Ausbildungsjahr gemäß der Anlage bestimmt.

(2) Der Unterricht ab dem 3.
Ausbildungsjahr wird in der Regel an dem Oberstufenzentrum fortgeführt, an dem die Ausbildung im 2.
Ausbildungsjahr stattgefunden hat.
Wenn gemäß der Anlage ab dem 2. Ausbildungsjahr keine Angaben für eine weitere Beschulung des betreffenden Ausbildungsberufes aufgeführt sind, ist die „Rahmenvereinbarung über die Bildung länderübergreifender Fachklassen für Schüler in anerkannten Ausbildungsberufen mit geringer Zahl Auszubildender“ (Beschluss der Kultusministerkonferenz vom 26.
Januar 1984 in der Fassung vom 1.
Oktober 2010) unter Beachtung der Fortschreibung zur „Liste der anerkannten Ausbildungsberufe, für welche länderübergreifende Fachklassen eingerichtet werden, mit Angabe der aufnehmenden Länder (Berufsschulstandorte) und Einzugsbereiche“ oder Absatz 7 zugrunde zu legen.

(3) Für anerkannte Ausbildungsberufe gemäß § 64 des Berufsbildungsgesetzes oder gemäß § 42k der Handwerksordnung legen die staatlichen Schulämter die Schulbezirke und damit das örtlich zuständige Oberstufenzentrum fest.
Dies gilt entsprechend für Ausbildungsregelungen gemäß § 66 des Berufsbildungsgesetzes oder gemäß § 42m der Handwerksordnung.

(4) Kreisübergreifende Fachklassen werden an einem Oberstufenzentrum eines Schulträgers mit einem Einzugsbereich von mindestens einem weiteren Schulträger eingerichtet.
Die Bezirke der Industrie- und Handelskammern sowie Handwerkskammern sind bei der Bildung der Einzugsbereiche zu berücksichtigen.

(5) Landesfachklassen und länderübergreifende Fachklassen, entsprechend der in Absatz 2 genannten Rahmenvereinbarung, werden an nur einem gemäß der Anlage ausgewiesenen Oberstufenzentrum eingerichtet.

(6) Von den Absätzen 4 und 5 abweichende Fachklassenbildungen bedürfen eines schriftlichen Antrags des Schulträgers auf Genehmigung durch das für Schule zuständige Ministerium.
Der Antrag ist in der Regel ab Schuljahresbeginn vor Bildung der Fachklasse bei dem für Schule zuständigen Ministerium einzureichen und hat bei Genehmigung nur für die Fachklassenbildung in dem beantragten Schuljahr Gültigkeit.
Der Unterricht für die folgenden Ausbildungsjahre der genehmigten Fachklasse bedarf keiner erneuten Antragstellung oder Genehmigung.

(7) Sofern ein Ausbildungsvertrag in einem Ausbildungsberuf abgeschlossen wurde, für den keine Zuständigkeitsregelung besteht, entscheidet das für Schule zuständige Ministerium über die örtlich zuständige Schule.

#### § 2 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 1.
August 2016 in Kraft.
Gleichzeitig tritt die Landesschulbezirksverordnung vom 25.
Januar 2013 (GVBl.
II Nr. 13) außer Kraft.

Potsdam, den 2.
Juni 2016

Der Minister für Bildung,  
Jugend und Sport

Günter Baaske

### Anlagen

1

[Anlage - Alphabetisch geordnete Ausbildungsberufe an OSZ im Land Brandenburg ab dem Schuljahr 2016/2017](/br2/sixcms/media.php/68/GVBl_II_25_2016-Anlage.pdf "Anlage - Alphabetisch geordnete Ausbildungsberufe an OSZ im Land Brandenburg ab dem Schuljahr 2016/2017") 1.3 MB