## Verordnung über das Naturschutzgebiet „Klapperberge“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

§ 1  
Erklärung zum Schutzgebiet
--------------------------------

Die in § 2 näher bezeichnete Fläche im Landkreis Uckermark wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Klapperberge“.

§ 2  
Schutzgegenstand
----------------------

(1) Das Naturschutzgebiet hat eine Größe von rund 1 560 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Gemeinde:**  

**Gemarkung:**  

**Flur:**

Lychen  

Lychen  

1;

Lychen  

Retzow  

1, 2;

Lychen  

Rutenberg  

1, 2, 3, 4, 6, 7, 8.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte Übersichtskarte im Maßstab 1 : 25 000 dient der räumlichen Einordnung des Naturschutzgebietes.
Die in Anlage 2 Nummer 2 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 01 bis 05 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 3 mit den Blattnummern 01 bis 19 aufgeführten Liegenschaftskarten.
Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 3 beigefügt, die gemäß Absatz 4 hinterlegt wird.

(3) Innerhalb des Naturschutzgebietes werden gemäß § 22 Absatz 1 Satz 3 des Bundesnaturschutzgesetzes festgesetzt: eine Zone 1 als Naturentwicklungsgebiet, die der direkten menschlichen Einflussnahme entzogen ist und in dem Lebensräume und Lebensgemeinschaften langfristig ihrer natürlichen Entwicklung überlassen bleiben, eine Zone 2 mit besonderen Beschränkungen der landwirtschaftliche Nutzung.
Die Zone 1 umfasst rund 88 Hektar und liegt in folgenden Fluren:

**Gemeinde:**  

**Gemarkung:**  

**Flur:**

Lychen  

Lychen  

1;

Lychen  

Retzow  

1, 2;

Lychen  

Rutenberg  

1, 6, 7.

Die Grenzen der Zonen 1 und 2 sind in der in Anlage 2 Nummer 1 genannten Übersichtskarte, den in Anlage 2 Nummer 2 genannten topografischen Karten mit den Blattnummern 01 bis 05 und den in Anlage 2 Nummer 3 genannten Liegenschaftskarten mit den Blattnummern 01 bis 19 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten und Flurstücksliste kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Uckermark, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

§ 3  
Schutzzweck
-----------------

(1) Schutzzweck des Naturschutzgebietes als naturnaher Ausschnitt des Neustrelitzer Kleinseenlandes ist   

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere naturnaher Waldtypen wie Buchenwälder, Bruchwaldgesellschaften und Moorgehölze, Schwimmblatt- und Tauchflurengesellschaften nährstoffarmer Seen, Gesellschaften der Seggen-, Torfmoos- und Röhrichtmoore sowie des Grünlandes frischer bis feuchter Ausprägung;
2.  die Erhaltung und Entwicklung der Lebensräume wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders geschützter Arten, insbesondere Heidenelke (Dianthus deltoides), Rundblättriger Sonnentau (Drosera rotundifolia), Sumpfiris (Iris pseudacorus), Fieberklee (Menyanthes trifoliata), Krebsschere (Stratiotes aloides), Blasenbinse (Scheuchzeria palustris) und Sandstrohblume (Helichrysum arenarium);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- beziehungsweise Rückzugsraum und potenzielles Wiederausbreitungszentrum wild lebender Tierarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders geschützter Arten, insbesondere Braunes Langohr (Plecotus auritus), Moorfrosch (Rana arvalis), Teichmolch (Triturus vulgaris), Laubfrosch (Hyla arborea), Gemeine Keiljungfer (Gomphus vulgatissimus), Mondazurjungfer (Coenagrion lunulatum) und Keilfleckmosaikjungfer (Aeshna isosceles);
4.  die Erhaltung einer reich gegliederten Wald- und Seenlandschaft mit nährstoffarmen Klarwasserseen, natürlich eutrophen Seen, Quellen, naturnahen Fließgewässern, Söllen und einer Vielzahl kleiner Moore einschließlich ihrer Binneneinzugsgebiete;
5.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit eines unzerschnittenen, störungsarmen Gebietes als typischer Ausschnitt der Hauptendmoräne des Neustrelitzer und Feldberg-Alt-Temmener Endmoränenbogens mit ausgedehnten Sanderflächen, Dünen und Schmelzwasserrinnen;
6.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Biotopverbundes zwischen dem Lychener Seenkreuz und den Havelgewässern.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung

1.  eines Teiles des Europäischen Vogelschutzgebietes „Uckermärkische Seen“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) in seiner Funktion als Lebensraum von Arten nach Anhang I der Richtlinie 79/409 EWG, insbesondere Fischadler (Pandion haliaetus), Schwarzstorch (Ciconia nigra), Kranich (Grus grus), Große Rohrdommel (Botaurus stellaris), Zwergschnäpper (Ficedula parva), Schellente (Bucephala clangula), Schwarzspecht (Dryocopus martius), Mittelspecht (Dendrocopus medius), Heidelerche (Lullula arborea) und Ziegenmelker (Caprimulgus europeus) einschließlich ihrer Brut- und Nahrungsbiotope;
2.  des Gebietes von gemeinschaftlicher Bedeutung „Klapperberge“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinen Vorkommen von
    
    1.  Dünen mit offenen Grasflächen mit Corynephorus und Agrostis, Oligo- bis mesotrophen kalkhaltigen Gewässern mit benthischer Vegetation aus Armleuchteralgen, Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Dystrophen Seen, Flüssen der planaren bis montanen Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Trockenen europäischen Heiden, Pfeifengraswiesen auf kalkreichem Boden, torfigen und tonig-schluffigen Böden (Molinion caeruleae), Feuchten Hochstaudenfluren der planaren und montanen bis alpinen Stufe, Mageren Flachland-Mähwiesen (Alopecurus pratensis, Sanguisorba officinalis), Übergangs- und Schwingrasenmooren, Kalkreichen Niedermooren, Hainsimsen-Buchenwald (Luzulo-Fagetum) und Mitteleuropäischem Flechten-Kiefernwald als natürliche Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes,
        
    2.  Trockenen, kalkreichen Sandrasen, Moorwäldern, Birken-Moorwald und Auen-Wäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion, Alnion incanae, Salicion albae) als prioritäre natürliche Lebensraumtypen im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes,
        
    3.  Biber (Castor fiber), Fischotter (Lutra lutra), Schlammpeitzger (Misgurnus fossilis), Steinbeißer (Cobitis taenia) und Großer Moosjungfer (Leucorrhinia pectoralis) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.
        
    
    (3) Darüber hinaus ist besonderer Schutzzweck der Zone 1 (Naturentwicklungsgebiet) die weitgehend eigen-dynamische und störungsfreie Entwicklung der Wälder, Seen und Moore sowie deren wissenschaftliche Untersuchung.
    
    § 4  
    Verbote
    -------------
    
    (1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.
    
    (2) Es ist insbesondere verboten:
    
    1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
    2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
    3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
    4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
    5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
    6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
    7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
    8.  die Ruhe der Natur durch Lärm zu stören;
    9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist außerhalb der Zone 1 und außerhalb von Bruchwäldern, Röhrichten, Feuchtwiesen und Mooren das Betreten zum Zwecke der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 7 jeweils nach dem 30.
        Juni eines jeden Jahres sowie ganzjährig in der Schutzzone 2 westlich der Ortslage Eichhof;
    10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb von Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können und außerhalb der Waldbrandwundstreifen zu reiten;
    11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege, ausgenommen des Anliegergebrauchs, zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
        Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzgesetzes und des Waldgesetzes des Landes Brandenburg;
    12.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen; ausgenommen ist das Befahren des Linowsees sowie des Kleinen Kastavensees, des Krummen Sees, des Großen und des Kleinen Köllnsees sowie des Kleinen Brückentinsees mit muskelkraftbetriebenen Booten außerhalb von Röhrichten und Schwimmblattgesellschaften.
        Das Einsetzen und Anlegen der Boote ist nur an den rechtmäßig bestehenden Stegen sowie an den in Anlage 2 Nummer 2 aufgeführten topografischen Karte mit der Blattnummer 02 und den in Anlage 2 Nummer 3 aufgeführten Liegenschaftskarten mit den Blattnummern 02, 07 und 08 eingezeichneten Stellen am Linowsee zulässig;
    13.  zu baden oder zu tauchen; ausgenommen ist das Baden vom Boot und von den rechtmäßig bestehenden Stegen aus sowie das Baden und Tauchen von der in Anlage 2 Nummer 2 aufgeführten topografischen Karte, Blattnummer 02 und den in Anlage 2 Nummer 3 aufgeführten Liegenschaftskarten mit den Blattnummern 02, 07 und 08 gekennzeichneten Stellen am Linowsee.
        Am Großen Köllnsee, Kleinen Köllnsee, Kleinen Kastavensee, Krummen See und Kleinen Brückentinsee ist das Baden ausschließlich vom Boot sowie von den rechtmäßig bestehenden Stegen aus zulässig;
    14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
    15.  Hunde frei laufen zu lassen;
    16.  Be- und Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu be-einträchtigen;
    17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle, Rückstände aus Biogasanlagen) und Sekundärrohstoffdünger (zum Beispiel Abwasser, Klärschlamm und Bioabfälle, wie zum Beispiel Schlempe) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf Flächen auf- oder auszubringen oder ins Grundwasser oder ins Oberflächengewässer einzuleiten;
    18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
    19.  Tiere zu füttern oder Futter bereitzustellen;
    20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
    21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
    22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
    23.  Pflanzenschutzmittel jeder Art anzuwenden;
    24.  Wiesen, Weiden oder sonstiges Grünland umzubrechen, neu anzusäen oder nachzusäen.
    
    § 5  
    Zulässige Handlungen
    --------------------------
    
    (1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:
    
    1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen außerhalb der Zone 1 mit der Maßgabe, dass
        1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Großvieheinheiten (GVE) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und vergleichbare Rückstände aus Biogasanlagen oder Sekundärrohstoffdünger (wie zum Beispiel solche aus Abwasser, Klärschlamm oder Bioabfälle) einzusetzen;
        2.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt, wobei die Nachsaat von Grünland zulässig bleibt,
        3.  auf Grünland innerhalb der Zone 2 § 4 Absatz 2 Nummer 17, 23 und 24 gilt;
    
    2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung außerhalb der Zone 1 mit der Maßgabe, dass
        1.  der in § 3 Absatz 2 Nummer 2 Buchstabe b genannte Lebensraumtyp „Moorwälder“ auf dem Flurstück 33 (teilweise) der Flur 1, Flurstück 35 (teilweise) der Flur 2 und Flurstück 10 (teilweise) der Flur 8 jeweils der Gemarkung Rutenberg sowie dem Flurstück 141 (teilweise) der Flur 1 der Gemarkung Retzow nicht bewirtschaftet wird und im Übrigen eine Nutzung der Laubwälder sowie der sonstigen in § 3 Absatz 2 Nummer 2 Buchstabe a genannten Waldgesellschaften einzelstamm- bis truppweise erfolgt,
        2.  in Misch- und Nadelwäldern Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von maximal 0,5 Hektar zulässig sind,
        3.  nur Baumarten der potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
        4.  der Boden unter Verzicht auf Pflügen und Umbruch bearbeitet wird, ausgenommen ist eine streifenweise, flachgründige, nicht in den Mineralboden eingreifende Bodenverwundung zur Unterstützung von Verjüngungsmaßnahmen,
        5.  das Befahren des Waldes nur auf Wegen oder festgelegten Rückegassen erfolgt,
        6.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
        7.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß und einer Mindesthöhe von 5 Metern nicht gefällt werden und die zu belassenden Bäume zu markieren sind sowie liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimeter am stärksten Ende) im Bestand verbleibt,
        8.  § 4 Absatz 2 Nummer 17 und 23 gilt;
    3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
        1.  innerhalb der Zone 1 nur Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne eines Monitorings mit Zustimmung der unteren Naturschutzbehörde zulässig sind.
            Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        2.  auf den Flächen außerhalb der Zone 1:  
            aa)  Besatzmaßnahmen nur mit heimischen Fischarten erfolgen, der Besatz mit Karpfen unzulässig ist und Besatzmaßnahmen am Linowsee auf die Kleine Maräne (Coregonus albula) und Große Maräne (Coregonus lavaretus)  
                   beschränkt bleiben,  
            bb) § 4 Absatz 2 Nummer 19 gilt;
    4.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass
        1.  die Nutzung am Großen Köllnsee, Kleinen Köllnsee, Kleinen Kastavensee, Kleinen Brückentinsee und Krummen See von den rechtmäßig bestehenden Stegen sowie vom Boot, sowie darüber hinaus am Linowsee von der in Anlage 2 Nummer 2 aufgeführten topografischen Karte, Blattnummer 02 und den in Anlage 2 Nummer 3 genannten Liegenschaftskarten, Blattnummern 02, 07 und 08 eingezeichneten Stellen zulässig ist,
        2.  § 4 Absatz 2 Nummer 12, 19 und 20 gilt;
    5.  für den Bereich der Jagd innerhalb der Zone 1:
        1.  die rechtmäßige Ausübung der Jagd innerhalb der Zone 1 mit der Maßgabe, dass  
            aa)  Maßnahmen zur Bestandsregulierung von Schalenwild bis zum 31.
            Dezember 2017 zulässig sind,  
            bb)  nach dem 31.
            Dezember 2017 Maßnahmen zur Bestandsregulierung von Schalenwild durch drei eintägige Gesellschaftsjagden im Zeitraum vom 1.
            Oktober eines jeden Jahres bis zum 31.
            Januar des Folgejahres zulässig sind.  
                   Die Durchführung der Gesellschaftsjagden ist jeweils eine Woche vorher schriftlich bei der unteren Naturschutzbehörde anzuzeigen.
            Sonstige Maßnahmen der Bestandsregulierung sind nach dem 31.
            Dezember 2017  
                   nach der Zulassung durch die untere Naturschutzbehörde zulässig.
            Dazu sind vom Antragsteller Erfordernis, Ziel, Art, Umfang, Zeitpunkt und Ort der Maßnahme darzulegen.
            Die Zulassung ist zu erteilen, wenn die Maßnahme  
                  dem Schutzzweck nicht oder nur unerheblich zuwiderläuft,
        2.  das Aufstellen transportabler und mobiler Ansitzeinrichtungen,
        3.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope.Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern sind unzulässig, im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Brandenburgischen Jagdgesetzes unberührt;
    6.  für den Bereich der Jagd außerhalb der Zone 1:
        1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass  
            aa)  die Fallenjagd mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern zum Gewässerufer verboten ist; Ausnahmen von der Einhaltung dieses Abstandes kann die untere Naturschutzbehörde erteilen, wenn  
                   der Schutzzweck nicht beeinträchtigt wird,  
            bb)  die Baujagd in einem Abstand von bis zu 100 Metern vom Ufer aller innerhalb des Schutzgebietes liegenden Gewässer unzulässig ist,
        2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde erfolgt.
            Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
            Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Aufstellung anzuzeigen.
            Die untere Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten.
            Die Entscheidung hierüber soll unverzüglich erfolgen,
        3.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und des in § 3 Absatz 2 Nummer 2 genannten Lebensraumtyps „Magere Flachland-Mähwiesen“.Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern sind unzulässig, im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Brandenburgischen Jagdgesetzes unberührt;
    7.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch nach dem 30.
        Juni eines jeden Jahres;
    8.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege hinsichtlich der Fahrbahn und des Banketts in der Zeit vom 1.
        Juli bis zum 30.
        September eines jeden Jahres, sofern jeweils eine Beschädigung des Gehölzbestandes ausgeschlossen ist.
        Die Maßnahmen sind der unteren Naturschutzbehörde vorab anzuzeigen.
        Unterhaltungsmaßnahmen an Straßen und Wegen außerhalb des in Satz 1 genannten Zeitraums oder Umfangs bedürfen des Einvernehmens mit der unteren Naturschutzbehörde;
    9.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
        Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
    10.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstiger wasserwirtschaftlicher Anlagen.
        Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
        Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
    11.  die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern diese nicht unter die Nummern 8 und 10 fallen, im Einvernehmen mit der unteren Naturschutzbehörde;
    12.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
    13.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
    14.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen außerhalb der Zone 1, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind, beispielsweise Maßnahmen zur Bekämpfung der spätblühenden Traubenkirsche,
    15.  innerhalb der Zone 1 Maßnahmen zur Umwandlung von Nadelholzforsten in naturnahe Laubwaldbestände, sofern diese bis zum 31.
        Dezember 2017 abgeschlossen werden;
    16.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen. Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
        Juli 2007 in der jeweils geltenden Fassung an Straßen und Wegen freigestellt;
    17.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
        Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
        Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    
    (2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
    Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
    Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.
    
    § 6  
    Pflege- und Entwicklungsmaßnahmen
    ---------------------------------------
    
    Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:
    
    1.  die Renaturierung der Moore südlich des Linowbaches, der Köllnseekette, der Birkentalniederung sowie des Faulen Seebruchs und Faulen Sees wird angestrebt;
    2.  im Bereich von Marienhof wird die Renaturierung von entwässerten Söllen angestrebt;
    3.  durch die Umwandlung von Ackerflächen am Nord-Ostufer des Linowsees in Grünland soll ein rund 100 Meter breiter Gewässerschutzstreifen entwickelt werden;
    4.  die Hutungsflächen nordwestlich von Retzow sollen entbuscht werden;
    5.  die Umwandlung von Nadelholzforsten in naturnahe Laubwaldbestände, insbesondere in Rotbuchenwälder bodensaurer und lehmiger Standorte wird angestrebt;
    6.  zum Schutz vor der Zurückdrängung indigener Pflanzen sollen zum Beispiel Maßnahmen zur Bekämpfung der Spätblühenden Traubenkirsche eingeleitet werden;
    7.  die Entnahme von Friedfischen aus dem Schulzensee und dem Großen und Kleinen Köllnsee wird angestrebt.
    
    § 7  
    Befreiungen
    -----------------
    
    Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.
    
    § 8  
    Ordnungswidrigkeiten
    --------------------------
    
    (1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.
    
    (2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.
    
    § 9  
    Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen
    --------------------------------------------------------------------------------
    
    (1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 68 des Brandenburgischen Naturschutzgesetzes.
    
    (2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.
    
    (3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere §§ 31 bis 33 und § 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.
    
    § 10  
    Geltendmachen von Rechtsmängeln
    --------------------------------------
    
    Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
    Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
    Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.
    
    § 11  
    Inkrafttreten
    --------------------
    
    § 5 Absatz 1 Nummer 1 Buchstabe a, b und c dieser Verordnung tritt am 1.
    Juli 2013 in Kraft.
    Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.
    
    Potsdam, den 27.
    September 2012
    
    Die Ministerin für Umwelt,  
    Gesundheit und Verbraucherschutz
    
    Anita Tack
    
    **Anlage 1** (zu § 2 Absatz 1)
    
    **![Das Naturschutzgebiet hat eine Größe von rund 1560 Hektar und liegt im Landkreis Uckermark in der Stadt Lychen. Es umfasst Flächen der Gemarkungen Lychen, Retzow und Rutenberg.](/br2/sixcms/media.php/69/Nr.82-1.GIF "Das Naturschutzgebiet hat eine Größe von rund 1560 Hektar und liegt im Landkreis Uckermark in der Stadt Lychen. Es umfasst Flächen der Gemarkungen Lychen, Retzow und Rutenberg.")**
    
    **Anlage 2  
    **(zu § 2 Absatz 2)
    
    ### 1.
    Übersichtskarte Maßstab 1 : 25 000
    
    **Titel:** Übersichtskarte zur Verordnung über das Naturschutzgebiet „Klapperberge“
    
    **Lfd.  Nummer**
    
    **Unterzeichnung**
    
    1
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV), am 12.
    April 2012
    
    ### 2. Topografische Karten Maßstab 1 : 10 000
    
    **Titel:**  Topografische Karte zur Verordnung über das Naturschutzgebiet „Klapperberge“
    
    Lfd.  Nummer
    
    Kartenblatt
    
    Unterzeichnung
    
    01
    
    2745-NW
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    02
    
    2745-NO
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    03
    
    2746-NW
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    04
    
    2745-SW
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    05
    
    2745-SO
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    ### 3. Liegenschaftskarten Maßstab 1 : 1 500
    
    **Titel:** Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Klapperberge“
    
    Lfd.
    Nummer
    
    Gemarkung
    
    Flur
    
    Unterzeichnung
    
    01
    
    Rutenberg
    
    2
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    02
    
    Rutenberg
    
    2
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    03
    
    Rutenberg
    
    2
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    04
    
    Rutenberg
    
    3
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    05
    
    Rutenberg
    
    3
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    06
    
    Rutenberg
    
    8
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    07
    
    Rutenberg
    
    1, 7, 8
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    08
    
    Rutenberg
    
    1, 2, 7
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    09
    
    Rutenberg
    
    2, 3, 6, 7
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
     10
    
    Rutenberg
    
    3
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
     11
    
    Rutenberg
    
    3
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    12
    
    Retzow  
    Rutenberg
    
    1  
    8
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    13
    
    Retzow  
    Rutenberg
    
    1  
    1, 7, 8
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    14
    
    Retzow  
    Rutenberg
    
    1  
    1, 6, 7
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    15
    
    Rutenberg
    
    6, 7
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    16
    
    Retzow
    
    1
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    17
    
    Retzow
    
    1, 2
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    18
    
    Retzow  
    Rutenberg
    
    1, 2  
    6, 7
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012
    
    19
    
    Rutenberg
    
    6
    
    unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 12.
    April 2012