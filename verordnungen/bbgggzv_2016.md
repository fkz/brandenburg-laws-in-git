## Verordnung über die Zuständigkeiten nach dem Gefahrgutbeförderungsrecht (Brandenburgische Gefahrgutzuständigkeitsverordnung -  BbgGGZV)

Auf Grund des § 9 Absatz 2 in Verbindung mit § 16 Absatz 2 Satz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186) sowie des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten vom 19.
Februar 1987 (BGBl.
I S.
602) verordnet die Landesregierung:

### § 1  
Zuständigkeiten für Aufsicht, Überwachung, Zulassung und Prüfung

Für die Durchführung der in der Anlage aufgeführten Verwaltungsaufgaben nach dem Gefahrgutbeförderungsrecht sind die dort bezeichneten Behörden zuständig, soweit nicht durch Bundesrecht andere Zuständigkeiten festgelegt sind.

### § 2  
Zuständigkeit für die Verfolgung und Ahndung von Ordnungswidrigkeiten

(1) Die Zuständigkeit für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 10 Absatz 1 des Gefahrgutbeförderungsgesetzes, insbesondere in Verbindung mit § 37 der Gefahrgutverordnung Straße, Eisenbahn und Binnenschifffahrt und § 10 der Gefahrgutverordnung See wird, soweit nicht die Zuständigkeiten nach § 5 Absatz 1 und § 10 Absatz 3 in Verbindung mit § 9a Absatz 5 des Gefahrgutbeförderungsgesetzes, den §§ 6 bis 16 der Gefahrgutverordnung Straße, Eisenbahn und Binnenschifffahrt oder § 6 Absatz 1 der Gefahrgutverordnung See eingreifen,

1.  für den Bereich der Betriebe, die der Bergaufsicht unterliegen, dem Landesamt für Bergbau, Geologie und Rohstoffe,
2.  für Anlagen gemäß den §§ 6, 7 und 9 des Atomgesetzes, sofern es einen Transport von radioaktiven Stoffen betrifft, dem Ministerium der Justiz und für Europa und Verbraucherschutz bezüglich Kernbrennstoffe, dem Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit bezüglich sonstiger radioaktiver Stoffe,
3.  für den Bereich der Häfen gemäß § 1 Absatz 1 und 2 der Landeshafenverordnung und der schiffbaren Landesgewässer nach § 1 Absatz 1 der Landesschifffahrtsverordnung dem Landesamt für Bauen und Verkehr,
4.  für den Bereich des Straßenverkehrs im Anwendungsbereich der Straßenverkehrsordnung den Polizeipräsidien und dem Zentraldienst der Polizei mit seiner zentralen Bußgeldstelle,
5.  für den Bereich der nichtbundeseigenen Eisenbahnen dem Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit,
6.  für den Bereich von Fertigungen von Verpackungen, Behältern (Containern) und Fahrzeugen, die nach Baumustern hergestellt sind, welche in den Vorschriften für die Beförderung gefährlicher Güter festgelegt sind, dem Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit,
7.  für den Bereich der übrigen Betriebe dem Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit

übertragen.

(2) Die Zuständigkeit für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 7a der Gefahrgutbeauftragtenverordnung wird, soweit nicht die Zuständigkeiten nach § 5 Absatz 1 des Gefahrgutbeförderungsgesetzes eingreifen,

1.  für den Bereich der Betriebe, die der Bergaufsicht unterliegen, dem Landesamt für Bergbau, Geologie und Rohstoffe,
2.  für den Bereich der nicht bundeseigenen Eisenbahnen dem Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit,
3.  für den Bereich der übrigen Betriebe dem Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit

übertragen.

(3) Die Zuständigkeit für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 58 des Luftverkehrsgesetzes wird

1.  für den Bereich der Betriebe, die der Bergaufsicht unterliegen, dem Landesamt für Bergbau, Geologie und Rohstoffe,
2.  für den Bereich der übrigen Betriebe dem Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit

übertragen.

### § 3  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Gefahrgutzuständigkeitsverordnung vom 18.
September 2002 (GVBl.
II S.
581) außer Kraft.

Potsdam, den 7.
Dezember 2010

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Matthias Platzeck

Der Minister für Infrastruktur und Landwirtschaft

Jörg Vogelsänger

**Anlage**   
(zu § 1)

**Zuständigkeiten**

**Erläuterungen zum nachfolgenden Verzeichnis:**

1.  

In der nachstehenden Übersicht bedeuten: 

  

–

MASGF  

Ministerium für Arbeit, Soziales, Gesundheit, Frauen und Familie

–

MdJEV  

Ministerium der Justiz und für Europa und Verbraucherschutz

–

MIL  

Ministerium für Infrastruktur und Landesplanung

–

LBGR  

Landesamt für Bergbau, Geologie und Rohstoffe

–

LAVG  

Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit

–

LBV  

Landesamt für Bauen und Verkehr

–

LfU  

Landesamt für Umwelt

–

IHK  

Industrie- und Handelskammer

–

PP  

Polizeipräsidium

–

KrOrdB  

Landkreis und kreisfreie Städte als Kreisordnungsbehörde

–

ZLS  

Zentralstelle der Länder Sicherheitstechnik

–

ADR  

Europäisches Übereinkommen über die internationale Beförderung gefährlicher Güter auf der Straße

–

RID  

Ordnung für die internationale Eisenbahnbeförderung gefährlicher Güter

–

ADN  

Europäisches Übereinkommen über die internationale Beförderung von gefährlichen Gütern auf Binnenwasserstraßen

–

ADNR  

Europäisches Übereinkommen über die internationale Beförderung von gefährlichen Gütern auf dem Rhein

–

In der Spalte 4 der nachstehenden Übersicht bedeuten: 

 

 

– die Trennung der Abkürzungen durch ein Komma: zuständig sowohl als auch, 

 

 

– die Trennung der Abkürzungen durch einen Schrägstich: alternative Zuständigkeit

**Lfd. Nr.**

**Vorschrift**

**Verwaltungsaufgabe**

**Zuständige Behörde**

**1**

**Gefahrgutbeförderungsgesetz**

 

 

1.1

§ 9 Absatz 1 auch in Verbindung mit Absatz 3a

Überwachung während der Ortsveränderung einschließlich der Prüfung der Konformität der in Verkehr befindlichen Verpackungen, Beförderungsbehältnisse und Fahrzeuge

 

1.  auf der Straße

1.  PP

2.  bei nicht bundeseigenen Bahnen

2.  LBGR/LAVG

3.  auf den schiffbaren Landesgewässern nach § 1 Absatz 1 der Landesschifffahrtsverordnung und Binnenwasserstraßen des Bundes

3.  PP

4.  in den Häfen gemäß § 1 Absatz 1 und 2 der Landeshafenverordnung

4.  LBV, PP

1.2

§ 9 Absatz 1 auch in Verbindung mit Absatz 3a

Überwachung der Übernahme, Ablieferung, des Be- und Entladens, des Umschlags, der Verpackung einschließlich der Prüfung der Konformität der in Verkehr befindlichen Verpackungen, Beförderungsbehältnisse und Fahrzeuge

 

1.  bei nicht bundeseigenen Bahnen

1.  LBGR/LAVG

2.  in den Häfen gemäß § 1 Absatz 1 und 2 der Landeshafenverordnung

2.  LAVG, LBV, PP

3.  in Betrieben, die der Bergaufsicht unterliegen

3.  LBGR

4.  in Anlagen gemäß den §§ 6, 7 und 9 des Atomgesetzes, sofern es einen Transport von radioaktiven Stoffen betrifft

4.  MdJEV für Kernbrennstoffe, LAVG für sonstige radioaktive Stoffe

5.  in allen übrigen Betrieben

5.  LAVG

1.3

§ 9 Absatz 1 in Verbindung mit Absatz 3

Überwachung des Herstellens, Einführens und Inverkehrbringens von Verpackungen, Beförderungsmitteln und Fahrzeugen für die Beförderung gefährlicher Güter einschließlich der Überwachung der Fertigung von Verpackungen, Behältern (Containern) und Fahrzeugen, die nach Baumustern hergestellt werden, welche in den Vorschriften für die Beförderung gefährlicher Güter festgelegt sind

 

1.  bei nicht bundeseigenen Bahnen

1.  LBGR/LAVG

2.  in den Häfen gemäß § 1 Absatz 1 und 2 der Landeshafenverordnung

2.  LAVG, LBV

3.  in Betrieben, die der Bergaufsicht unterliegen

3.  LBGR

4.  in Anlagen gemäß den §§ 6, 7 und 9 des Atomgesetzes, sofern es einen Transport von radioaktiven Stoffen betrifft

4.  MdJEV für Kernbrennstoffe, LAVG für sonstige radioaktive Stoffe

5.  in allen übrigen Betrieben

5.  LAVG

**2**

**Gefahrgutverordnung Straße, Eisenbahn und Binnenschifffahrt**  
Zuständigkeitsbereich Straßenverkehr

 

 

2.1

§ 4 Absatz 2 Nummer 1

Entgegennahme von Meldungen bei Gefahrenlagen

PP

2.2

§ 5 Absatz 1 Nummer 1

Erteilung von Ausnahmen

LBV

2.3

§ 13

Prüfung und Zulassung von Gefäßen nach den Absätzen 6.2.1.6.1 bis 6.2.1.6.2 ADR/RID

bis zum 31.
Dezember 2009 von der ZLS anerkannte und benannte Stellen für ortsbewegliche Druckgeräte nach § 2 Nummer 2 der OrtsDruckV in Verbindung mit Artikel 8 der Richtlinie 1999/36/EG und entsprechende Stellen, die ab dem 1. Januar 2010 von der ZLS benannt und von der Deutschen Akkreditierungsstelle GmbH anerkannt sind

2.4

§ 14 Absatz 3

Anerkennung und Überwachung von Schulungen und Durchführung der Prüfungen und die Erteilung der Bescheinigung über die Fahrzeugführerschulung nach Abschnitt 8.2.2.
ADR Führen eines Verzeichnisses über alle gültigen Schulungsbescheinigungen nach Unterabschnitt 1.10.1.6 ADR, ausgenommen Schulungsbescheinigungen nach § 7 Absatz 1 Nummer 3 und Absatz 2 Nummer 3

IHK Potsdam,  
Ostbrandenburg,  
Cottbus

2.5

§ 14 Absatz 4

Erste Untersuchungen nach Unterabschnitt 9.1.2.1 Satz 2 ADR zur Übereinstimmung mit den anwendbaren Vorschriften der Kapitel 9.2 bis 9.8 ADR und Ausstellung von ADR-Zulassungsbescheinigungen nach Unterabschnitt 9.1.2.1 Satz 4 in Verbindung mit Unterabschnitt 9.1.3.1 ADR

vom MIL benannte amtlich anerkannte Sachverständige für den Kraftfahrzeugverkehr

2.6

§ 14 Absatz 5

Jährliche technische Untersuchungen und Verlängerung der Gültigkeit von ADR-Zulassungsbescheinigungen nach Unterabschnitt 9.1.2.3 ADR

vom MIL benannte für Hauptuntersuchungen gemäß § 29 der StVZO zuständige Stellen oder Personen

2.7

§ 35 Absatz 3

Schriftliche Bestimmung des Fahrweges

KrOrdB im Einvernehmen mit dem LfU

2.8

§ 35 Absatz 5 Satz 4

Erteilung der Bescheinigung nach § 35 Absatz 5 Satz 1 und 2 bei grenzüberschreitenden Beförderungen

LBV

2.9

ADR – Anlage B, Teil 8, Kapitel 8.5

Einschränkung der Be- und Entladung geschlossener Ladungen auf einer Stelle

KrOrdB

2.10

ADR – Anlage B, Teil 8, Kapitel 8.5

Erlaubnis zum Be- und Entladen; Erteilung einer Zustimmung über längeres Halten

KrOrdB

2.11

ADR – Anlage A, Absatz 6.2.1.5.1 Bemerkung und Absatz 6.2.1.6.1  
Bemerkung 1

Zustimmung zum Ersetzen einer Flüssigkeitsprüfung durch eine Prüfung mit einem Gas

LAVG

2.12

ADR – Anlage A, Absatz 6.8.2.3.1

Ausstellung der Bescheinigung über die Zulassung des Baumusters

LBV

2.13

ADR – Anlage A, Absatz 6.8.2.1.23

Anerkennung der Befähigung zur Ausführung von Schweißarbeiten an Tanks

LBV

**3**

**Gefahrgutverordnung Straße, Eisenbahn und Binnenschifffahrt**  
Zuständigkeitsbereich Eisenbahnen

 

 

3.1

§ 4 Absatz 2 Nummer 2

Entgegennahme von Meldungen bei Gefahrenlagen

PP

3.2

§ 5 Absatz 1 Nummer 2

Erteilung von Ausnahmen für den Bereich der nicht bundeseigenen Eisenbahnen

LBV

3.3

§ 13

Verfahren für die Prüfung und Zulassung der Gefäße nach den Absätzen 6.2.1.6.1 bis 6.2.1.6.2 ADR/RID

bis zum 31.
Dezember 2009 von der ZLS anerkannte und benannte Stellen für ortsbewegliche Druckgeräte nach § 2 Nummer 2 der OrtsDruckV in Verbindung mit Artikel 8 der Richtlinie 1999/36/EG und entsprechende Stellen, die ab dem 1. Januar 2010 von der ZLS benannt und von der Deutschen Akkreditierungsstelle GmbH anerkannt sind

3.4

§ 15 Absatz 3

Zuständigkeit für nicht bundeseigene Eisenbahnen, soweit vorstehend nichts anderes bestimmt ist

LAVG

**4**

**Gefahrgutverordnung Straße, Eisenbahn und Binnenschifffahrt**  
Zuständigkeitsbereich Binnenschifffahrt

 

 

4.1

§ 4 Absatz 2 Nummer 3

Entgegennahme von Meldungen bei Gefahrenlagen

LBV,PP

4.2

§ 5 Absatz 1 Nummer 3

Erteilung von Ausnahmen

LBV

4.3

§ 16 Absatz 3

Anerkennung von Sachverständigen für die Ausstellung von Gasfreiheitsbescheinigungen nach Abschnitt 8.3.5 Satz 2 ADNR/ADN  
Zulassen von sachkundigen Personen oder Firmen zum Entgasen von Ladetanks nach Absatz 7.2.3.7.1 oder zur Reinigung von Ladetanks nach Absatz 7.2.4.15.3 ADNR/ADN

LBV

4.4

§ 16 Absatz 4

Ausstellung von Bescheinigungen über von ihr nach § 5 erteilte Ausnahmen nach Absatz 1.5.1.4.1 ADNR/1.5.2.2.2 ADN

Zugelassene Gleichwertigkeiten und Abweichungen nach Abschnitt 1.5.3 ADNR/ADN

LBV

4.5

§ 16 Absatz 7

Aufgaben nach Teil 7 ADNR/ADN mit Ausnahme von Aufgaben nach Absatz 1 Nummer 1 und Absatz 3 Nummer 2

Genehmigung von Reparatur- und Wartungsarbeiten mit elektrischem Strom oder Feuer nach Abschnitt 8.3.5 ADNR/ADN

Entgegennahme der Meldungen über erhöhte Konzentrationen an Schwefelwasserstoff nach Teil 3 Tabelle C Spalte 20 Nummer 28b ADNR/ADN bei der Beförderung von UN 2448  
Kontrollen nach Unterabschnitt 1.8.1.1 ADNR/ADN

Entgegennahme der Informationen und Mitteilungen nach Unterabschnitt 1.7.6.1 Buchstabe b Gliederungseinheit iv und Buchstabe c ADNR/ADN  
Zuständigkeit nach § 16 Absatz 7 Satz 1 Nummer 4 und 5 GGVSEB sowie Unterabschnitt 7.1.5.5 ADNR/ADN

LBV

4.6

§ 16 Absatz 8

Kontrollen nach Unterabschnitt 1.8.1.4 ADNR/ADN

LBV

**5**

**Gefahrgutverordnung See**

 

 

5.1

§§ 3 bis 4, 6 bis 9

Überwachung während Übernahme, Umschlag, Verpacken, Be- und Entladen

 

1.  in Betrieben, die der Bergaufsicht unterstehen

1.  LBGR

2.  in Anlagen gemäß den §§ 6, 7 und 9 des Atomgesetzes sofern es einen Transport von radioaktiven Stoffen betrifft

2.  MdJEV für Kernbrennstoffe, LAVG für sonstige radioaktive Stoffe

3.  in allen übrigen Betrieben

3.  LAVG

5.2

§ 5

Zulassen von Ausnahmen

LBV

**6**

**Gefahrgutbeauftragtenverordnung**

Gesamte Verordnung

LAVG/LBGR

**7**

**Luftverkehrsgesetz mit den jeweils geltenden Gefahrgutvorschriften der ICAO/IATA**

Überwachung während Übernahme, Umschlag, Verpacken, Be- und Entladen

 

1.  in Betrieben, die der Bergaufsicht unterstehen

1.  LBGR

2.  in allen übrigen Betrieben

2.  LAVG

**8**

**Richtlinie für die Anordnung von verkehrsregelnden Maßnahmen für den Transport gefährlicher Güter auf Straßen (Verkehrsblatt 1987 S. 857 mit Berichtigung Verkehrsblatt 1988 S. 576)**

 

 

8.1

Abschnitt 6 Absatz 2

Zustimmung zur Anordnung verkehrsregelnder Maßnahmen

LBV