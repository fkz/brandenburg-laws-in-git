## Verordnung über das Verfahren zur Feststellung repräsentativer Entgelttarifverträge im öffentlichen Personennahverkehr im Land Brandenburg  (Brandenburgische Vergabegesetz-ÖPNV-Verfahrensverordnung - BbgVergGÖPNVVV)

Auf Grund des § 3 Absatz 2 Satz 6 bis 8 des Brandenburgischen Vergabegesetzes vom 21. September 2011 (GVBl.
I Nr. 19) in Verbindung mit § 1 der Brandenburgischen Vergabegesetz-Zuständigkeitsübertragungsverordnung vom 29.
März 2012 (GVBl.
II Nr.
22) verordnetder Minister für Arbeit, Soziales, Frauen und Familie:

#### § 1  
Beirat

(1) Die Entscheidung über die Repräsentativität von Entgelttarifverträgen für den öffentlichen Personennahverkehr im Land Brandenburg wird durch einen Beirat vorbereitet.

(2) Das für Arbeit zuständige Ministerium führt die Geschäfte und den Vorsitz des Beirats.

#### § 2  
Zusammensetzung des Beirats

(1) Dem Beirat nach § 1 gehören acht stimmberechtigte Mitglieder sowie eine vorsitzende Person ohne Stimmrecht an.
Für jedes Mitglied ist eine Stellvertretung zu benennen.
Der Beirat wird in jeweils gleicher Zahl mit Vertreterinnen und Vertretern von Gewerkschaften und von Arbeitgebervereinigungen oder einzelnen Unternehmen im Bereich des öffentlichen Personennahverkehrs im Land Brandenburg besetzt.
An der Auswahl der Mitglieder werden Tarifvertragsparteien im Bereich des öffentlichen Personennahverkehrs im Land Brandenburg beteiligt.
Die Auswahl der Mitglieder und deren Stellvertretungen sowie die Berufung aller Mitglieder des Beirats obliegen dem für Arbeit zuständigen Ministerium.

(2) Zur Gewährleistung der gleichberechtigten Teilhabe von Frauen und Männern sind von den Vorschlagsberechtigten jeweils mindestens zur Hälfte Frauen vorzuschlagen.
Wird nur ein Mitglied entsandt, ist mindestens in jeder zweiten Amtsperiode eine Frau vorzuschlagen.
Dies gilt auch für die Stellvertretungen und Nachbesetzungen während der laufenden Amtsperiode.
Die Sätze 1 bis 3 finden keine Anwendung, soweit den Vorschlagsberechtigten die Einhaltung dieser Vorgaben aus rechtlichen oder tatsächlichen Gründen nicht möglich ist; sie haben dem für Arbeit zuständigen Ministerium diese Gründe nachvollziehbar darzulegen.

#### § 3  
Mitgliedschaft im Beirat

(1) Die Mitglieder des Beirats und deren Stellvertretungen werden für die Dauer von vier Jahren berufen.

(2) Die Mitglieder sind ehrenamtlich tätig.
Sie werden für Reisekosten nach Maßgabe der §§ 4 bis 7 des Bundesreisekostengesetzes sowie für nachgewiesenen Verdienstausfall entschädigt.

#### § 4  
Sitzungen des Beirats

(1) Der Beirat ist bei Bedarf oder auf Verlangen von mindestens drei Mitgliedern von dem für Arbeit zuständigen Ministerium einzuberufen.

(2) Die Sitzungen sind nicht öffentlich.
Zu den Sitzungen können weitere sachverständige Personen beigezogen werden; sie haben jedoch kein Stimmrecht.

#### § 5  
Entscheidungen des Beirats

(1) Der Beirat ist entscheidungsfähig, wenn mehr als die Hälfte seiner stimmberechtigten Mitglieder anwesend oder vertreten sind.

(2) In Einzelfällen kann eine Entscheidung auch im schriftlichen Umlaufverfahren herbeigeführt werden.

(3) Der Beirat gibt dem für Arbeit zuständigen Ministerium Empfehlungen für die Aufnahme oder Nichtaufnahme von Entgelttarifverträgen in die Liste nach § 7 Satz 1 ab.
Er entscheidet durch die Mehrheit der abgegebenen Stimmen.
Die Empfehlung ist schriftlich zu begründen.
Kommt keine Mehrheit zustande, sind stattdessen die unterschiedlichen Positionen ausführlich schriftlich darzulegen.

#### § 6  
Feststellung der Repräsentativität

(1) Bei der Feststellung der Repräsentativität eines Entgelttarifvertrages ist vor allem auf die Zahl der von den jeweils tarifgebundenen Arbeitgebern beschäftigten unter den Geltungsbereich des Entgelttarifvertrages fallenden Arbeitnehmerinnen und Arbeitnehmer am Ort der Leistung abzustellen.
Sofern mehrere Entgelttarifverträge nach der Zahl der Arbeitnehmerinnen und Arbeitnehmer nicht wesentlich voneinander abweichen, können auch mehrere Entgelttarifverträge nebeneinander als repräsentativ angesehen werden.

(2) Ergänzend kann auch die Zahl der jeweils unter den Geltungsbereich des Entgelttarifvertrages fallenden Mitglieder der Gewerkschaft berücksichtigt werden, die den Entgelttarifvertrag geschlossen hat.

#### § 7  
Liste repräsentativer Entgelttarifverträge

Das für Arbeit zuständige Ministerium führt eine Liste der Entgelttarifverträge, die im Hinblick auf öffentliche Auftragsvergaben über eine Leistung im öffentlichen Personennahverkehr im Land Brandenburg als repräsentativ im Sinne des § 4 Absatz 1 Satz 1 des Brandenburgischen Vergabegesetzes anzusehen sind.
Die Liste wird im Amtsblatt für Brandenburg veröffentlicht.

#### § 8  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 19.
Juli 2013

Der Minister für Arbeit,  
Soziales, Frauen und Familie

Günter Baaske