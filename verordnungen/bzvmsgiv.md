## Verordnung über die beamtenrechtlichen Zuständigkeiten im Geschäftsbereich des Ministeriums für Soziales, Gesundheit, Integration und Verbraucherschutz (Beamtenzuständigkeitsverordnung MSGIV - BZVMSGIV)

Auf Grund des § 6 Absatz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), der zuletzt durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28) geändert worden ist, in Verbindung mit

*   § 4 Absatz 1 Satz 2 des Landesbeamtengesetzes vom 3.
    April 2009 (GVBl.
    I S.
    26) in Verbindung mit § 1 Absatz 1 Satz 2 und Absatz 3 Satz 1 der Ernennungsverordnung vom 1. August 2004 (GVBl.
    II S.
    742),
*   § 32 Absatz 1 Satz 1, § 38 Satz 2, § 54 Absatz 1, § 56 Absatz 1 Satz 5, § 57 Absatz 1 Satz 2, § 69 Absatz 5 Satz 1, § 84 Satz 2, § 85 Absatz 4 Satz 4, § 86 Absatz 3, § 87 Absatz 1 Satz 2 Nummer 1, § 88 Absatz 5, § 89 Satz 2, § 92 Absatz 2 zweiter Halbsatz und § 103 Absatz 2 des Landesbeamtengesetzes, von denen § 85 Absatz 4 Satz 4, § 86 Absatz 3, § 87 Absatz 1 Satz 2 Nummer 1, § 88 Absatz 5 und § 89 Satz 2 durch Gesetz vom 29. Juni 2018 (GVBl.
    I Nr.
    17) geändert worden sind,
*   § 17 Absatz 2 Satz 2, § 34 Absatz 5, § 35 Absatz 2 Satz 2 und § 42 Absatz 2 Satz 2 des Landesdisziplinargesetzes vom 18.
    Dezember 2001 (GVBl.
    I S.
    254),
*   § 64 Absatz 1 Satz 1 des Landesbeamtengesetzes, der durch Gesetz vom 11. März 2010 (GVBl.
    I Nr.
    13) geändert worden ist, in Verbindung mit § 6 Satz 2 der Dienstjubiläumsverordnung vom 18.
    Dezember 2014 (BGBl.
    I S.
    2267),
*   § 54 Absatz 3 Satz 2 des Beamtenstatusgesetzes vom 17.
    Juni 2008 (BGBl.
    I S. 1010),
*   § 25 des Landesbeamtengesetzes, der durch Gesetz vom 29.
    Juni 2018 (GVBl.
    I Nr. 17) geändert worden ist, in Verbindung mit § 2 Absatz 2, § 4 Absatz 4, § 9 Absatz 4 Satz 3, § 21 Absatz 3 Satz 1 und § 30 Absatz 1 der Laufbahnverordnung vom 1.
    Oktober 2019 (GVBl. II Nr. 82),
*   § 13 Absatz 2 Satz 3, § 16 Absatz 2 Satz 2 und § 58 Absatz 1 des Brandenburgischen Besoldungsgesetzes vom 20.
    November 2013 (GVBl.
    I Nr.
    32 S.
    2, Nr.
    34)

verordnet die Ministerin für Soziales, Gesundheit, Integration und Verbraucherschutz:

#### § 1 Dienstvorgesetzter

Dienstvorgesetzter im Sinne von § 2 Absatz 2 Satz 1 des Landesbeamtengesetzes für die Präsidentin oder den Präsidenten des Landesamtes für Arbeitsschutz, Verbraucherschutz und Gesundheit und für die Präsidentin oder den Präsidenten des Landesamtes für Soziales und Versorgung sowie deren Stellvertretungen ist das Mitglied der Landesregierung, zu dessen Geschäftsbereich das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit und das Landesamt für Soziales und Versorgung gehören.
Für die übrigen Beamtinnen und Beamten des Landesamtes für Arbeitsschutz, Verbraucherschutz und Gesundheit und des Landesamtes für Soziales und Versorgung sind die jeweiligen Leitungen Dienstvorgesetzte.

#### § 2 Übertragung von Befugnissen nach dem Landesbeamtengesetz

(1) Die Ausübung der Befugnis zur Ernennung der Beamtinnen und Beamten wird der Präsidentin oder dem Präsidenten des Landesamtes für Arbeitsschutz, Verbraucherschutz und Gesundheit und der Präsidentin oder dem Präsidenten des Landesamtes für Soziales und Versorgung jeweils für die ihnen zugeordneten Beamtinnen und Beamten übertragen, soweit sie für diese Dienstvorgesetzte nach § 1 sind.

(2) Dem Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit und dem Landesamt für Soziales und Versorgung werden für ihren Geschäftsbereich folgende weitere Zuständigkeiten übertragen:

1.  Entscheidungen über das Vorliegen der Voraussetzungen der Entlassung kraft Gesetzes gemäß § 32 Absatz 1 Satz 1 des Landesbeamtengesetzes,
2.  Entscheidungen über die Versetzung einer Beamtin auf Probe oder eines Beamten auf Probe in den Ruhestand gemäß § 38 Satz 1 des Landesbeamtengesetzes,
3.  Entscheidungen über das Verbot der Führung der Dienstgeschäfte gemäß § 54 Absatz 1 des Landesbeamtengesetzes,
4.  Entscheidungen über die Versagung der Aussagegenehmigung gemäß § 56 Absatz 1 Satz 5 des Landesbeamtengesetzes, wobei die Versagung der Aussagegenehmigung der vorherigen Zustimmung der obersten Dienstbehörde bedarf,
5.  Entscheidungen über Ausnahmen vom Verbot der Annahme von Belohnungen, Geschenken und sonstigen Vorteilen gemäß § 57 Absatz 1 des Landesbeamtengesetzes mit der Maßgabe, dass die Entscheidung der jeweiligen Leitung oder deren Stellvertretung persönlich obliegt,
6.  Befugnis zur Gewährung und Versagung von Dienstjubiläumszuwendungen gemäß § 64 Absatz 1 des Landesbeamtengesetzes in Verbindung mit § 6 der Dienstjubiläumsverordnung,
7.  Entscheidungen über die Erteilung und Rücknahme der Erlaubnis zur Führung der Amtsbezeichnung mit dem Zusatz „außer Dienst“ („a.
    D.“) gemäß § 69 Absatz 5 des Landesbeamtengesetzes,
8.  Entscheidungen in Nebentätigkeitsangelegenheiten gemäß §§ 84 bis 89 des Landesbeamtengesetzes, soweit nicht die Entscheidung gemäß § 87 Absatz 1 Satz 2 Nummer 2 des Landesbeamtengesetzes der obersten Dienstbehörde vorbehalten ist,
9.  Untersagung einer Erwerbstätigkeit oder sonstigen Beschäftigung nach Beendigung des Beamtenverhältnisses gemäß § 92 Absatz 2 des Landesbeamtengesetzes.

#### § 3 Übertragung weiterer Befugnisse

Den in § 2 Absatz 1 genannten Stellen werden für die ihnen zugeordneten Beamtinnen und Beamten, für die sie Dienstvorgesetzte sind, folgende weitere Befugnisse der obersten Dienstbehörde übertragen:

1.  Ausübung der Disziplinarbefugnisse bei Ruhestandsbeamtinnen und Ruhestandsbeamten gemäß § 17 Absatz 2 Satz 1 des Landesdisziplinargesetzes,
2.  Disziplinarbefugnisse gemäß §§ 34, 35 und 42 des Landesdisziplinargesetzes, wobei die Erhebung der Disziplinarklage der vorherigen Zustimmung der obersten Dienstbehörde bedarf,
3.  Entscheidungen über die Einführung und Ausgestaltung der Personalführungs- und -entwicklungsmaßnahmen gemäß § 2 Absatz 2 der Laufbahnverordnung,
4.  Entscheidungen über Art und Umfang von Ausschreibungen und ihre Bekanntmachung gemäß § 4 Absatz 4 der Laufbahnverordnung,
5.  Entscheidungen über die Anrechnung einer im öffentlichen oder dienstlichen Interesse liegenden Beurlaubung auf die Probezeit im Einvernehmen mit dem für das allgemeine öffentliche Dienstrecht und dem für das finanzielle öffentliche Dienstrecht zuständigen Ministerium gemäß § 9 Absatz 4 der Laufbahnverordnung,
6.  Entscheidungen über die Zulassung zur Einführung in die nächsthöhere Laufbahn aufgrund des Vorschlags der Auswahlkommission gemäß § 21 Absatz 3 der Laufbahnverordnung; dies gilt entsprechend für den Aufstieg in Laufbahnen ohne Vorbereitungsdienst,
7.  Entscheidungen über die Feststellung der Befähigung für eine Laufbahn ohne Vorbereitungsdienst des mittleren und des gehobenen Dienstes gemäß § 30 Absatz 1 der Laufbahnverordnung,
8.  Zustimmung zum Absehen von der Rückforderung von Bezügen aus Billigkeitsgründen gemäß § 13 Absatz 2 Satz 3 des Brandenburgischen Besoldungsgesetzes,
9.  Entscheidungen über die Anweisung des dienstlichen Wohnsitzes gemäß § 16 des Brandenburgischen Besoldungsgesetzes,
10.  Kürzung der Anwärterbezüge gemäß § 58 des Brandenburgischen Besoldungsgesetzes.

#### § 4 Befugnis zum Erlass von Widerspruchsbescheiden

Die Entscheidung über den Widerspruch von Beamtinnen und Beamten, Ruhestandsbeamtinnen und Ruhestandsbeamten, früheren Beamtinnen und früheren Beamten sowie deren Hinterbliebenen gegen den Erlass oder die Ablehnung eines das Beamtenverhältnis betreffenden Verwaltungsaktes oder gegen die Ablehnung eines Anspruchs auf eine Leistung wird der Präsidentin oder dem Präsidenten des Landesamtes für Arbeitsschutz, Verbraucherschutz und Gesundheit und der Präsidentin oder dem Präsidenten des Landesamtes für Soziales und Versorgung für die ihnen zugeordneten Beamtinnen und Beamten übertragen, soweit sie Dienstvorgesetzte sind und die jeweilige Dienststelle die mit dem Widerspruch angefochtene Entscheidung erlassen hat (§ 54 Absatz 3 Satz 2 des Beamtenstatusgesetzes).

#### § 5 Vertretung bei Klagen aus dem Beamtenverhältnis

Die Vertretung des Landes vor den Gerichten der Verwaltungsgerichtsbarkeit wird auf die in § 1 genannten Dienststellen für ihren jeweiligen Bereich übertragen, soweit diese selbst über den Widerspruch entschieden haben oder hätten entscheiden müssen (§ 103 Absatz 2 des Landesbeamtengesetzes).
Satz 1 ist in Verfahren zur Erlangung des einstweiligen Rechtsschutzes gemäß §§ 80 bis 80b und § 123 der Verwaltungsgerichtsordnung entsprechend anzuwenden.

#### § 6 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Beamtenzuständigkeitsverordnung MASGF vom 8.
Januar 2013 (GVBl.
II Nr.
3), die zuletzt durch Verordnung vom 1.
August 2016 (GVBl.
II Nr.
41) geändert worden ist, außer Kraft.

Potsdam, den 1.
Februar 2021

Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

Ursula Nonnemacher