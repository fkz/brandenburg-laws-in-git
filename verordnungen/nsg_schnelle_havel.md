## Verordnung über das Naturschutzgebiet „Schnelle Havel“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nr. 43) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1   
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche in den Landkreisen Oberhavel und Barnim wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Schnelle Havel“.

### § 2   
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 2 463 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Landkreis: Barnim**

Gemeinde:

Gemarkung:

Flur:

Schorfheide

Prötze

3, 4;

Schluft

1 bis 3;

**Landkreis: Oberhavel**

Gemeinde:

Gemarkung:

Flur:

Löwenberger Land

Falkenthal

11;

Liebenwalde

Freienhagen

1, 3 bis 5, 7 bis 10, 20, 101;

Kreuzbruch

3, 6, 8, 9, 32;

Liebenwalde

1, 2, 5, 6, 8, 101 bis 103;

Liebenwalde 1

101;

Liebenwalde 8

17;

Neuholland

101 bis 103, 105, 106;

Prötze 1

1, 2, 5;

Wiesen am linken Ufer des Malzer Kanals 1

7;

Wiesen am linken Ufer des Malzer Kanals 2

8;

Wiesen am linken Ufer des Malzer Kanals 3

9;

Wiesen am linken Ufer des Malzer Kanals 4

10;

Wiesen am linken Ufer des Malzer Kanals 5

11;

Wiesen am linken Ufer des Malzer Kanals 6

12;

Oranienburg

Bernöwe

1 bis 3;

Freienhagen 11

16;

Freienhagen 12

17, 23;

Freienhagen 14

19;

Friedrichsthal

1, 3, 4, 6;

Malz

1, 3 bis 14, 21, 24, 25;

Malz 1

10;

Malz 2

11;

Malz 3

12;

Malz 4

13;

Malz 6

14;

Malz 7

15;

Malz 8

16;

Malz 9

17;

Malz 10

18;

Malz 11

19;

Malz 12

20;

Sachsenhausen

1, 2, 4, 10;

Schmachtenhagen

5, 6;

Schmachtenhagen 1

7;

Wiesen am rechten Ufer des Malzer Kanals

1;

Zehdenick

Falkenthal 1

13;

Kappe

1, 2;

Krewelin

1 bis 4, 101;

Krewelin 1

6;

Kurtschlag

7, 8;

Wesendorf

7;

Zehdenick

20 bis 24, 31, 32.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte Übersichtskarte im Maßstab 1 : 50 000 dient der räumlichen Einordnung des Naturschutzgebietes.
Die in Anlage 2 Nummer 2 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 10 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 3 mit den Blattnummern 1 bis 58 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke wird eine Flurstücksliste gemäß Absatz 3 hinterlegt.

(3) Die Verordnung mit Karten und Flurstücksliste kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie bei den Landkreisen Oberhavel und Barnim, untere Naturschutzbehörden, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

### § 3   
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes als Teil der Zehdenick-Spandauer Havelniederung mit seinem naturnahen Fließgewässersystem und angrenzenden strukturreichen Biotopkomplexen ist

1.  die Erhaltung und Entwicklung als Lebensraum wild lebender Pflanzengesellschaften, insbesondere von Feuchtwiesen und Feuchtweiden, Bruch- und Moorwäldern, Röhricht- und Schwimmblattgesellschaften, Mooren und Trockenrasen;
2.  die Erhaltung und Entwicklung der Lebensräume wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Langblättriger Ehrenpreis (Veronica longifolia), Zungen-Hahnenfuß (Ranunculus lingua), Weiße Seerose (Nymphaea alba), Sumpf-Stendelwurz (Epipactis palustris) und Sand-Strohblume (Helichrysum arenarium);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, beispielsweise Große Bartfledermaus (Myotis brandti), Bekassine (Gallinago gallinago), Rothalstaucher (Podiceps grisegena), Moorfrosch (Rana arvalis), Glattnatter (Coronella austriaca) und Gebänderte Prachtlibelle (Calopteryx splendens);
4.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit der Niederungslandschaft mit der mä-andrierenden Schnellen Havel einschließlich ihren Altarmen sowie großflächigen Bruch- und Auenwäldern und Feuchtwiesenkomplexen;
5.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Biotopverbundes zwischen den Zehdenicker-Mildenberger Tonstichen und dem Döllnfließ.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung

1.  eines Teiles des Europäisches Vogelschutzgebietes „Obere Havelniederung“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) in seiner Funktion
    1.  als Lebensraum von Arten nach Anhang I der Vogelschutz-Richtlinie, insbesondere Eisvogel (Alcedo atthis), Kranich (Grus grus), Wachtelkönig (Crex crex), Flussseeschwalbe (Sterna hirundo), Trauerseeschwalbe (Chlidonias niger), Seeadler (Haliaeetus albicilla) und Schreiadler (Aquila pomarina) einschließlich ihrer Brut- und Nahrungsbiotope,
    2.  als Vermehrungs-, Rast-, Mauser- und Überwinterungsgebiet für im Gebiet regelmäßig auftretende Zugvogelarten wie Gänsesäger (Mergus merganser), Löffelente (Anas clypeata), Knäkente (Anas querquedula) und Kiebitz (Vanellus vanellus);
2.  des Gebietes von gemeinschaftlicher Bedeutung „Schnelle Havel“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinen Vorkommen von
    1.  Dünen mit offenen Grasflächen mit Corynephorus (Silbergras) und Agrostis (Straußgras), Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Flüssen der planaren Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Feuchten Hochstaudenfluren der planaren Stufe, Hainsimsen-Buchenwald (Luzulu-Fagetum) und Alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur (Stiel-Eiche) als natürliche Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes,
    2.  Auen-Wäldern mit Alnus glutinosa (Schwarz-Erle) und Fraxinus excelsior (Gewöhnliche Esche \[Alno-Padion, Alnion incanae\]), Birken-Moorwäldern als prioritäre natürliche Lebensraumtypen im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes,
    3.  Biber (Castor fiber), Fischotter (Lutra lutra), Großem Mausohr (Myotis myotis), Rapfen (Aspius aspius), Steinbeißer (Cobitis taenia), Schlammpeitzger (Misgurnus fossilis), Bitterling (Rhodeus amarus), Hellem Wiesenknopf-Ameisenbläuling (Maculinea teleius) und Großem Feuerfalter (Lycaena dispar) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

### § 4   
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet, seinen Naturhaushalt oder einzelne seiner Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist außerhalb von Bruchwäldern, Röhrichten, Feuchtwiesen und Mooren das Betreten zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 9 jeweils nach dem 30. Juni eines jeden Jahres;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb von Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wege und außerhalb der Waldbrandwundstreifen zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  zu baden oder zu tauchen; ausgenommen ist das Baden von den rechtmäßig bestehenden Stegen aus sowie von der in den Karten zu dieser Verordnung nach § 2 Absatz 2 (Topografische Karte im Maßstab 1 : 10 000, Blattnummer 6 und Liegenschaftskarte, Blattnummer 31) eingetragenen Badestelle an der Schnellen Havel östlich des Ortsteils Neu-Holland;
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen außerhalb der Bundeswasserstraßen Malzer Kanal, des Oder-Havel-Kanals, des Vosskanals sowie der Havel südlich des Malzer Kanals bei Friedrichsthal zu benutzen; ausgenommen bleibt darüber hinaus das abschnittsweise Befahren der Schnellen Havel mit muskelkraftbetriebenen Booten gemäß § 5 Absatz 1 Nummer 8;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle, Rückstände aus Biogasanlagen) und Sekundärrohstoffdünger (wie zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen, wie zum Beispiel Schlempe) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland umzubrechen oder neu anzusäen.

### § 5   
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Rauhfutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle und vergleichbare Rückstände aus Biogasanlagen oder Sekundärrohstoffdünger (wie zum Beispiel solche aus Abwasser, Klärschlamm oder Bioabfällen) einzusetzen,
    2.  bei Beweidung Gehölze in geeigneter Weise gegen Verbiss und sonstige Beschädigungen sowie Ränder von Gewässern wirksam gegen Trittschäden von weidenden Nutztieren zu schützen sind,
    3.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt, wobei bei Narbenschäden eine umbruchlose Nachsaat von Grünland zulässig bleibt;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  eine Nutzung der Laubwälder einzelstamm- bis truppweise erfolgt,
    2.  auf den übrigen Flächen Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von maximal 0,5 Hektar zulässig sind,
    3.  nur Baumarten der potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind,
    4.  das Befahren des Waldes nur auf Wegen und Rückegassen erfolgt,
    5.  Bäume mit Horsten und Höhlen nicht gefällt werden,
    6.  je Hektar mindestens sieben Stück stehendes Totholz (mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß) nicht gefällt werden und liegendes Totholz (ganze Bäume mit einem Durchmesser über 65 Zentimeter am stärksten Ende) im Bestand verbleibt,
    7.  § 4 Absatz 2 Nummer 17 und 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass ein Einschwimmen und eine Gefährdung des Fischotters weitgehend ausgeschlossen sind,
    2.  § 4 Absatz 2 Nummer 19 gilt;
4.  die Teichbewirtschaftung, die den Anforderungen des § 5 Absatz 4 des Bundesnaturschutzgesetzes in Verbindung mit dem Fischereigesetz für das Land Brandenburg entspricht und die im Sinne der guten fachlichen Praxis gemäß den Leitlinien zur naturschutzgerechten Teichwirtschaft in Brandenburg vom 16.
    März 2011 auf den bisher rechtmäßig dafür genutzten Flächen durchgeführt wird.
5.  die rechtmäßige Ausübung der Angelfischerei in der bisherigen Art und im bisherigen Umfang auf den bisher dafür genutzten Flächen mit der Maßgabe, dass § 4 Absatz 2 Nummer 19 und 20 gilt;
6.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        in der Zeit vom 1.
        März bis 30.
        Juni eines jeden Jahres die Jagd nur vom Ansitz aus erfolgt,
        
        bb)
        
        die Jagd auf Wasservögel in der Zeit vom 1.
        September eines jeden Jahres bis zum 15. Januar des folgenden Jahres im Bereich des Stausees Neuholland verboten ist,
        
        cc)
        
        die Fallenjagd mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern vom Ufer aller innerhalb des Schutzgebietes liegenden Gewässer aus verboten ist; Ausnahmen von der Einhaltung dieses Abstandes kann die untere Naturschutzbehörde erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
        dd)
        
        keine Baujagd in einem Abstand von bis zu 100 Metern vom Ufer aller innerhalb des Schutzgebietes liegenden Gewässer aus vorgenommen wird,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde; die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    3.  das Aufstellen mobiler Ansitzeinrichtungen,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope sowie in einem Mindestabstand von 20 Metern vom Gewässerrand;
    
    Ablenkfütterungen sowie die Anlage und Unterhaltung von Ansaatwildwiesen und Wildäckern sind unzulässig; im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
    
7.  die Genehmigung von Maßnahmen zur Vergrämung und Tötung von Kormoranen im Bereich der fischereilich genutzten Teiche durch die zuständige Naturschutzbehörde, sofern hierfür die erforderliche artenschutzrechtliche Ausnahmegenehmigung oder Befreiung vorliegt.
    Die Genehmigung kann mit Auflagen versehen werden; sie ist zu erteilen, wenn der Schutzzweck nicht wesentlich von der Maßnahme beeinträchtigt wird;
8.  das Befahren der Schnellen Havel mit muskelkraftbetriebenen Wasserfahrzeugen ab der in den Karten zu dieser Verordnung nach § 2 Absatz 2 (Topografische Karte im Maßstab 1 : 10 000, Blattnummer 6 und Liegenschaftskarte, Blattnummer 30) eingetragenen Stelle westlich der Ortslage Liebenwalde im südlichen Verlauf bis zur Einmündung des Malzer Kanals mit der Maßgabe, dass
    1.  das Befahren ausschließlich mit Einer- und Zweier-Kajaks erlaubt ist,
    2.  das Befahren ausschließlich vom 1.
        Juli eines jeden Jahres bis zum 31.
        März des Folgejahres zulässig ist sowie frühestens eine Stunde vor Sonnenaufgang beginnt und spätestens eine Stunde nach Sonnenuntergang endet,
    3.  nicht entgegen der Strömung gefahren werden darf und vorhandene Steuer hochzuziehen sind, sofern sie tiefer als die Kiellinie gefahren werden;
9.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch nach dem 30.
    Juni eines jeden Jahres;
10.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, die den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
11.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
12.  die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern diese nicht unter die Nummern 10 und 11 fallen, im Einvernehmen mit der unteren Naturschutzbehörde;
13.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
14.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
15.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der zuständigen Naturschutzbehörde zugelassen oder angeordnet worden sind;
16.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkie-rungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur  
    Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl. S. 1734), die durch die Bekanntmachung vom 1.
    Oktober 2013 (ABl.
    S. 2811) geändert worden ist, an Straßen und Wegen freigestellt;
17.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen;
18.  die im Folgenden benannten Maßnahmen im Rahmen der Wassertourismusinitiative Nordbrandenburg (WIN), soweit die in § 3 Absatz 2 aufgeführten Lebensraumtypen und Arten nicht beeinträchtigt werden:
    1.  der Ersatzneubau für die Straßenbrücke Friedrichsthal über den Malzer Kanal mit Verlegung der Straßenanschlüsse,
    2.  die Errichtung einer Schleuse mit Schleusenkanal, Brücke, Fußgängerbrücke, Pumpwerk, Ufersicherungen am Malzer Kanal,
    3.  die Sanierung der Schleuse Sachsenhausen,
    4.  Kanalvertiefungen im Einfahrts- und Vorhafenbereich der Schleuse Sachsenhausen und im Bereich nördlich der Schleuse Sachsenhausen mit Uferräumung sowie die Ufersanierung und Beseitigung einzelner Untiefen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

### § 6   
Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  zum Schutz der Moorböden sollen niedermoortypische Abflussverhältnisse wiederhergestellt werden.
    Hierzu soll eine nutzungsverträgliche Wassermengenbewirtschaftung mit dem vorrangigen Ziel der Wasserrückhaltung zur Verminderung der Moordegradierung erfolgen;
2.  ausgebaute Abschnitte der Fließgewässer sollen renaturiert und Altarme angeschlossen werden.
    Künstliche Migrationshindernisse für aquatische und semiaquatische Tierarten sollen beseitigt werden;
3.  die Forstreinbestände sowie Waldbestockungen mit nicht standortheimischen Baumarten sollen in naturnahe, standortgerechte Wälder entwickelt werden.

### § 7   
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

### § 8   
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

### § 9   
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege- und Entwicklungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 17 und 18 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 29 Absatz 3 und § 30 des Bundesnaturschutzgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten, ihrer Lebensstätten und Biotope (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 54 Absatz 7 des Bundesnaturschutzgesetzes) unberührt.

### § 10   
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

### § 11   
Inkrafttreten

§ 5 Absatz 1 Nummer 1 Buchstabe a tritt am 1.
Januar 2015 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 28.
Oktober 2014

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

* * *

### Anlagen

1

[NSGSchnelle Havel-Anlg-1](/br2/sixcms/media.php/68/GVBl_II_82_2014-Anlage-1.pdf "NSGSchnelle Havel-Anlg-1") 722.9 KB

2

[NSGSchnelle Havel-Anlg-2](/br2/sixcms/media.php/68/GVBl_II_82_2014-Anlage-2.pdf "NSGSchnelle Havel-Anlg-2") 845.2 KB