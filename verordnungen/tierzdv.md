## Verordnung zur Durchführung des Tierzuchtgesetzes (Tierzuchtdurchführungsverordnung - TierZDV)

Auf Grund

*   des § 9 Absatz 2 Satz 1 Nummer 1 und des § 28 Absatz 2 des Tierzuchtgesetzes vom 18. Januar 2019 (BGBl.
    I S. 18),
*   des § 6 Absatz 4 des Landesorganisationsgesetzes vom 24.
    Mai 2004 (GVBl.
    I S.
    186), der durch Artikel 2 des Gesetzes vom 10.
    Juli 2014 (GVBl.
    I Nr. 28 S.
    2) geändert worden ist,
*   des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
    Februar 1987 (BGBl.
    I S.
    602) sowie
*   des § 1 der Verordnung zur Übertragung von Ermächtigungen nach dem Tierzuchtgesetz vom 28.
    August 2019 (GVBl.
    II Nr.
    70)

verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 

Das Ministerium für Ländliche Entwicklung, Umwelt und Landwirtschaft ist zuständige Behörde für:

1.  die Anerkennung von Zuchtverbänden und Zuchtunternehmen nach § 4 des Tierzuchtgesetzes,
2.  die Genehmigung von Zuchtprogrammen nach § 5 des Tierzuchtgesetzes,
3.  die Genehmigung von Ausnahmen bezüglich der Anforderungen an Tierzuchtbescheinigungen nach Artikel 31 und Artikel 32 der Verordnung (EU) 2016/1012 des Europäischen Parlaments und des Rates vom 8.
    Juni 2016 über die Tierzucht- und Abstammungsbestimmungen für die Zucht, den Handel und die Verbringung in die Union von reinrassigen Zuchttieren und Hybridzuchtschweinen sowie deren Zuchtmaterial und zur Änderung der Verordnung (EU) Nr. 652/2014, der Richtlinien des Rates 89/608/EWG und 90/425/EWG sowie zur Aufhebung einiger Rechtsakte im Bereich der Tierzucht („Tierzuchtverordnung“) (ABl. L Nr.
    171 vom 29.6.2016, S.
    66),
4.  die Entscheidung gemäß Artikel 38 der Verordnung (EU) 2016/1012 ein Zuchtprogramm bei reinrassigen Zuchttieren durchzuführen,
5.  die Prüfung von Zuchtprogrammen aus anderen Mitgliedsstaaten der Europäischen Union nach § 6 Absatz 1 Nummer 2 des Tierzuchtgesetzes darauf, ob Gründe für eine Verweigerung der Durchführung des Zuchtprogramms vorliegen,
6.  die Befristung der Anerkennung eines Zuchtverbandes oder Zuchtunternehmens oder der Genehmigung eines Zuchtprogrammes sowie für die Festlegung besonderer Regelungen nach § 7 des Tierzuchtgesetzes,
7.  die Erteilung der Erlaubnis für den Betrieb einer Besamungsstation, einer Embryo-Entnahmeeinheit nach § 18 Absatz 1 bis 6 sowie die Zulassung von Ausnahmen nach § 18 Absatz 9 des Tierzuchtgesetzes,
8.  die Erteilung von Auskünften zwischen den Behörden, Datenübermittlungen und Außenverkehr nach § 21 Absatz 2 bis 4 und 6 des Tierzuchtgesetzes.

#### § 2 

(1) Das Landesamt für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung ist zuständige Behörde für:

1.  die Durchführung der Leistungsprüfungen und Zuchtwertschätzungen nach § 9 Absatz 2 des Tierzuchtgesetzes; es kann die Durchführung auf Dritte übertragen oder diese mit der Mitwirkung betrauen,
2.  die Information der Abnehmer von Zuchtprodukten über die Ergebnisse der Zuchtwertschätzung sowie die Gewährung des Zuganges der jeweils Berechtigten zu den Ergebnissen der Leistungsprüfungen und der Zuchtwertschätzung nach § 8 des Tierzuchtgesetzes,
3.  das Monitoring im Rahmen der Erhaltung der genetischen Vielfalt nach § 10 des Tierzuchtgesetzes,
4.  die Zulassung der Samengewinnung außerhalb einer Besamungsstation im Einzelfall nach § 14 Absatz 3 Satz 2 des Tierzuchtgesetzes,
5.  die Feststellung der Gleichwertigkeit der Fertigkeiten, Kenntnisse und Fähigkeiten nach § 15 Absatz 2 und nach § 17 Absatz 1 des Tierzuchtgesetzes,
6.  die Überwachung nach § 22 Absatz 1 bis 5 des Tierzuchtgesetzes,
7.  die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 23 des Tierzuchtgesetzes,
8.  die Wahrnehmung der Aufgaben nach § 1 Absatz 1, § 4, § 6 Absatz 3 Satz 1 und § 9 Absatz 2 Satz 1 der Verordnung über Lehrgänge nach dem Tierzuchtgesetz.

(2) Die mit der Durchführung der Leistungsprüfung und Zuchtwertschätzung nach Absatz 1 Nummer 1 beliehenen oder beauftragten Stellen sind verpflichtet, der zuständigen Behörde die zur Erfüllung ihrer Aufgaben notwendigen Daten kostenfrei zur Verfügung zu stellen.

#### § 3 

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Tierzuchtdurchführungsverordnung vom 9.
Juli 2013 (GVBl.
II Nr.
53) außer Kraft.

Potsdam, den 1.
Oktober 2019

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger