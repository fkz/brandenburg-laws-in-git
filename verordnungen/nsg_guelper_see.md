## Verordnung über das Naturschutzgebiet „Gülper See“

Auf Grund der §§ 22, 23 und 32 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2 und § 78 Absatz 1 Satz 3 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

#### § 1   
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Havelland wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Gülper See“.

#### § 2   
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 1 200 Hektar.
Es umfasst Flächen in folgenden Fluren des Landkreises Havelland:

**Gemeinde:**

**Gemarkung:**

**Flur:**

Havelaue

Gülpe

1, 2;

Havelaue

Strodehne

11, 26, 27;

Havelaue

Wolsier

4, 5;

Rhinow

Rhinow

9, 14, 15, 16, 17.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten mit den Blattnummern 1 bis 6 im Maßstab 1 : 10 000 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 26 aufgeführten Liegenschaftskarten.
Eine Kartenblattübersicht mit dem Raster der Liegenschaftskarten im Maßstab 1 : 30 000 ist in Anlage 2 Nummer 3 aufgeführt.
Zur Orientierung über die betroffenen Flurstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

Innerhalb des Naturschutzgebietes sind eine Zone 1 mit 330 Hektar und eine Zone 2 mit 80 Hektar mit unterschiedlichen Beschränkungen der landwirtschaftlichen Nutzung festgesetzt.
Die Grenzen der Zonen sind in den in Anlage 2 Nummer 1 genannten topografischen Karten mit den Blattnummern 1 bis 6 und in den in Anlage 2 Nummer 2 genannten Liegenschaftskarten mit den Blattnummern 1 bis 26 mit ununterbrochener roter Linie eingezeichnet sowie in der in Anlage 2 Nummer 3 genannten Kartenblattübersicht vermerkt.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(3) Für die außerhalb des Naturschutzgebietes liegende, in den in Absatz 2 Satz 3 genannten Karten als Einwirkungszone gekennzeichnete Fläche enthält diese Verordnung gemäß § 22 Absatz 1 Satz 3 Halbsatz 2 des Bundes­naturschutzgesetzes Verbote für Handlungen, die in das Naturschutzgebiet hineinwirken.
Die Verbote werden in § 5 benannt.
Die Einwirkungszone umfasst rund 1 550 Hektar und liegt in folgenden Fluren im Landkreis Havelland:

**Gemeinde:**

**Gemarkung:**

**Flur:**

Havelaue

Gülpe

1, 2;

Havelaue

Strodehne

24, 25, 26, 27;

Havelaue

Wolsier

2, 4, 5, 6;

Rhinow

Rhinow

3, 9, 14, 15, 16, 17.

Die Grenze der Einwirkungszone ist in der in Absatz 1 genannten Kartenskizze und den in Anlage 2 Nummer 1 genannten topografischen Karten mit den Blattnummern 1 bis 6, in den in Anlage 2 Nummer 2 genannten Liegenschaftskarten mit den Blattnummern 1 bis 26 mit ununterbrochener roter Linie eingezeichnet sowie in der in Anlage 2 Nummer 3 genannten Kartenblattübersicht vermerkt.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten und Flurstücksliste kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis  
Havelland, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3  
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der an Flachwasserseen gebundenen Wasserpflanzen- und Schwimmblattgesellschaften, der Feuchtgrünlandgesellschaften und der auf Dünenbereichen liegenden Trockenrasengesellschaften;
    
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Wasserfeder (Hottonia palustris), Zierliches Tausendgüldenkraut (Centaurium pulchellum), Sumpf-Schwertlilie (Iris pseudacorus), Feld-Mannstreu (Eryngium campestre) und Körnchen-Steinbrech (Saxifraga granulata);
    
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere von Reptilien, Amphibien, Libellen und Watvögeln, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Zauneidechse (Lacerta agilis), Knoblauchkröte (Pelobates fuscus), Kreuzkröte (Bufo calamita), Moorfrosch (Rana arvalis), Gebänderte Prachtlibelle (Calopteryx splendens), Gemeine Smaragdlibelle (Cordulia aenea), Bekassine (Gallinago gallinago), Großer Brachvogel (Numenius arquata), Kiebitz (Vanellus vanellus), Rotschenkel (Tringa totanus) und Uferschnepfe (Limosa limosa);
    
4.  die Erhaltung eines durch die Weichseleiszeit geprägten eutrophen Flachwassersees der Havelniederung mit Klarwasserphasen sowie die Erhaltung der angrenzenden Verlandungszonen, Niedermoorbereiche und der randlich gelegenen Dünengürtel;
    
5.  die Erhaltung und Entwicklung des Gebietes aus wissenschaftlichen Gründen zur Untersuchung langfristiger Entwicklungen eines europaweit bedeutsamen Vogelrastgebietes und Brutgebietes für Wiesenbrüter sowie zur Untersuchung der langfristigen Entwicklung von Feuchtgebieten;
    
6.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit des Gebietes mit seiner vielfältigen  
    Naturausstattung, die durch den Gülper See, artenreiche Feuchtwiesen und die randlichen Dünen bestimmt wird;
    
7.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Biotopverbundes zwischen dem Unteren Rhinluch und dem Havelländischen Luch.
    

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung

1.  eines Teiles des Europäischen Vogelschutzgebietes „Niederung der Unteren Havel“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) in seiner Funktion
    
    1.  als Lebensraum von Arten nach Anhang I der Richtlinie 2009/147/EG, insbesondere Fischadler (Pandion haliaetus), Seeadler (Haliaeetus albicilla), Rotmilan (Milvus milvus), Rohrweihe (Circus aeruginosus), Große Rohrdommel (Botaurus stellaris), Flussseeschwalbe (Sterna hirundo), Trauerseeschwalbe (Chlidonias niger), Wachtelkönig (Crex crex), Kampfläufer (Philomachus pugnax), Silberreiher (Ardea alba), Weißstorch (Ciconia ciconia) und Eisvogel (Alcedo atthis) einschließlich ihrer Brut- und Nahrungsbiotope,
        
    2.  als Vermehrungs-, Nahrungs-, Rast-, Mauser- und Überwinterungsgebiet für im Gebiet regelmäßig auftretende Zugvogelarten wie Kranich (Grus grus), Bläßgans (Anser albifrons) und Tundrasaatgans (Anser fabalis rossicus), Singschwan (Cygnus cygnus) und Zwergschwan (Cygnus columbianus bewickii), Kiebitz (Vanellus vanellus), Knäkente (Anas querquedula) und Reiherente (Aythya fuligula);
        
2.  des Gebietes von gemeinschaftlicher Bedeutung „Gülper See“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), das ehemals einen Teil des Gebietes von gemeinschaftlicher Bedeutung „Niederung der Unteren Havel/Gülper See“ umfasste, mit seinen Vorkommen von
    
    1.  Dünen mit offenen Grasflächen mit Corynephorus (Silbergras) und Agrostis (Straußgras), oligo- bis mesotrophen stehenden Gewässern mit Vegetation der Littorelletea uniflorae und der Isoeto-Nanojuncetea, natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions und des Hydrocharitions, Flüssen der planaren Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, feuchten Hochstaudenfluren der planaren Stufe, Brenndolden-Auenwiesen (Cnidion dubii), mageren Flachlandmähwiesen, (Alopecurus pratensis (Wiesen-Fuchsschwanz), Sanguisorba officinalis (Großer Wiesenknopf)), alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur (Stiel-Eiche) als Biotope von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG),
        
    2.  trockenen, kalkreichen Sandrasen und Auen-Wäldern mit Alnus glutinosa (Schwarz-Erle) und Fraxinus excelsior (Gewöhnliche Esche) (Alno-Padion, Alnion incanae, Salicion albae) als prioritäre Biotope („prioritäre Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG),
        
    3.  Biber (Castor fiber), Fischotter (Lutra lutra), Kammmolch (Triturus cristatus), Rotbauchunke (Bombina bombina), Rapfen (Aspius aspius), Steinbeißer (Cobitis taenia), Flußneunauge (Lampetra fluviatilis), Schlammpeitzger (Misgurnus fossilis), Bitterling (Rhodeus amarus) und Großem Feuerfalter (Lycaena dispar) als Tierarten von gemeinschaftlichem Interesse (im Sinne des Anhangs II der Richtlinie 92/43/EWG) einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.
        

#### § 4  
Verbote

(1) Vorbehaltlich der nach § 6 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
    
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
    
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
    
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
    
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
    
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
    
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
    
8.  die Ruhe der Natur durch Lärm zu stören;
    
9.  das Gebiet außerhalb der Wege zu betreten;
    
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 51 des Brandenburgischen Naturschutzgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
    
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzgesetzes und des Waldgesetzes des Landes Brandenburg;
    
12.  zu baden oder zu tauchen;
    
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen;
    
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
    
15.  Hunde frei laufen zu lassen;
    
16.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
    
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle, Jauche und Rückstände aus Biogasanlagen) und Sekundärrohstoffdünger (zum Beispiel Abwasser, Klärschlamm und Bioabfälle) zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
    
18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
    
19.  Tiere zu füttern oder Futter bereitzustellen;
    
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
    
21.  wild lebenden Tieren nachzustellen, sie zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
    
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
    
23.  Pflanzenschutzmittel jeder Art anzuwenden;
    
24.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.
    

#### § 5  
Verbote für die Einwirkungszone

In der nach § 2 Absatz 3 benannten, außerhalb des Naturschutzgebietes gelegenen, Einwirkungszone ist die Jagd auf Federwild verboten.
Zur Schadensabwehr auf gefährdeten Ackerkulturen ist die Vergrämung von Gänsen, auch durch Jagd, mit Zustimmung der unteren Naturschutzbehörde zulässig.

#### § 6   
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  das Ausbringen von Dünger und Pflanzenschutzmitteln mit Luftfahrzeugen unzulässig ist,
        
    2.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdünger, Gülle, Jauche, Rückstände aus Biogasanlagen mit Nassvergärung und Sekundärrohstoffdünger wie zum Beispiel Abwasser, Klärschlamm und Bioabfälle einzusetzen,
        
    3.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt.
        Bei Wildschäden ist mit Zustimmung der unteren  
        Naturschutzbehörde eine umbruchlose Nachsaat zulässig,
        
    4.  auf Grünland das Mähgut zu beräumen ist.
        Ausgenommen davon ist die vorübergehende Lagerung von Heu- und Silageballen (bis zu drei Monaten) an befahrbaren Wegen.
        Ferner bleibt die Lagerung von Heu- und Silageballen an Winterweideflächen bis zum Abschluss der Winterweide zulässig, sofern sie an befahrbaren Wegen und außerhalb der unter § 3 Absatz 2 Nummer 2 Buchstabe a und b aufgeführten Lebensraumtypen erfolgt,
        
    5.  bei Beweidung mit Ausnahme der Hutehaltung eine Auszäunung der Ufer von Gewässern sowie von Gehölzen erfolgt,
        
    6.  das Schleppen und Walzen von Grünland im Zeitraum vom 31.
        März bis zur ersten Nutzung eines jeden Jahres unzulässig ist,
        
    7.  in der Zone 1 über die Buchstaben a bis f hinaus die Nutzung von Grünland vor dem 16. Juni eines jeden Jahres unzulässig ist,
        
    8.  in der Zone 1 über die Buchstaben a bis g hinaus eine Schnitthöhe von mindestens zehn Zentimetern am Mähwerk eingestellt wird,
        
    9.  in der Zone 1 über die Buchstaben a bis h hinaus Flächen mit der Größe über einen Hektar in Blöcken mit einer maximalen Breite von 80 Metern in Bewirtschaftungsrichtung gemäht werden und zwischen den Blöcken ein Streifen in Breite des Mähwerkes bis zur nächsten Nutzung verbleibt,
        
    10.  in der Zone 2 über die Buchstaben a bis f hinaus das Ausbringen von Düngern aller Art unzulässig ist,
        
    11.  auf Ackerflächen über die Buchstaben a und e hinaus der Einsatz von chemisch-synthetischen Düngemitteln, Gülle, Herbiziden und Insektiziden unzulässig ist;
        
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  nur Baumarten der potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Baumarten unter Ausschluss eingebürgerter Arten zu verwenden sind; Nebenbaumarten dürfen dabei nicht als Hauptbaumart eingesetzt werden,
        
    2.  § 4 Absatz 2 Nummer 23 gilt,
        
    3.  hydromorphe Böden nur bei Frost befahren werden,
        
    4.  Erstaufforstungen unzulässig sind,
        
    5.  Bäume mit Höhlen und Horsten nicht gefällt werden,
        
    6.  für die unter § 3 Absatz 2 Nummer 2 Buchstabe a genannten bodensauren Eichenwälder sowie für die unter § 3 Absatz 2 Nummer 2 Buchstabe b genannten Auenwälder mit Alnus glutinosa (Schwarzerle) und Fraxinus excelsior (Gewöhnliche Esche) über die Regelungen gemäß den Buchstaben a bis e hinaus gilt, dass
        
        aa)  ein Altholzanteil von mindestens zehn Prozent am aktuellen Bestandsvorrat zu sichern ist,
        
        bb)  mindestens fünf Stämme je Hektar mit einem Mindestdurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum Absterben aus der Nutzung genommen werden,
        
        cc)   je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 30 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß und mindestens zwei Stück liegendes Totholz je Hektar mit mehr als 65 Zentimeter Durchmesser am stärksten Ende, sofern vorhanden, zu erhalten sind,
        
        dd)  die Nutzung nur einzelstamm- bis truppweise erfolgt;
        
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    
    1.  Fanggeräte und Fangmittel so eingesetzt oder ausgestattet werden, dass eine Gefährdung des Fischotters, des Bibers und tauchender Vogelarten wie beispielsweise Gänsesäger und Haubentaucher weitgehend ausgeschlossen ist,
        
    2.  die in § 3 Absatz 2 Nummer 2 Buchstabe c genannten Fischarten und Neunaugen ganzjährig geschont sind; ausgenommen ist der Rapfen.
        Für ihn gilt eine Schonzeit vom 1.
        April bis zum 30.
        Juni eines jeden Jahres und ein Mindestmaß bei der Entnahme von 40 Zentimetern,
        
    3.  der Fischbesatz nur mit heimischen Arten erfolgt und eine Gefährdung der unter § 3 Absatz 2 Nummer 2 Buchstabe c genannten Fischarten und Neunaugen ausgeschlossen ist; § 13 der Brandenburgischen Fischereiordnung bleibt unberührt,
        
    4.  die Elektrofischerei nur mit Zustimmung der unteren Naturschutzbehörde zulässig ist.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht gefährdet wird;
        
4.  die rechtmäßige Ausübung der Angelfischerei entlang des Rhins zwischen der Ortslage Kietz bis 100 Meter vor Beginn der Gebüschzone des Küddens.
    Der Bereich ist in den in § 2 Absatz 2 aufgeführten topografischen Karten gekennzeichnet.
    Die Angelfischerei erfolgt mit der Maßgabe, dass
    
    1.  sie nur vom Ufer aus zulässig ist,
        
    2.  § 4 Absatz 2 Nummer 19 und 20 gilt,
        
    3.  im Radius von 50 Metern um Biberburgen und Fischotterbaue die Angelfischerei unzulässig ist,
        
    4.  das Betreten von Röhrichten und Verlandungszonen unzulässig ist,
        
    5.  die in § 3 Absatz 2 Nummer 2 Buchstabe c genannten Fischarten und Neunaugen ganzjährig geschont sind; ausgenommen ist der Rapfen.
        Für ihn gilt eine Schonzeit vom 1.
        April bis zum 30.
        Juni eines jeden Jahres und ein Mindestmaß bei der Entnahme von 40 Zentimetern;
        
5.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)  die Jagd auf Federwild verboten ist,
        
        bb)  durch die oberste Jagdbehörde auf Antrag der unteren Naturschutzbehörde ein zeitlich begrenztes Jagdverbot für bestehende jagdliche Einrichtungen angeordnet werden kann, wenn dies zum Schutz des Reproduktions- oder Rastgeschehens gefährdeter Arten erforderlich ist,
        
        cc)  die Jagd in der Zeit vom 1.
        März bis zum 15.
        Juni eines jeden Jahres nur vom Ansitz aus erfolgt,
        
        dd)  die Fallenjagd mit Lebendfallen erfolgt und in einem Abstand von mindestens 300 Metern zum Gewässerufer des Gülper Sees, des Bärengrabens und des Rhins verboten ist.
        Ausnahmen bedürfen der Zustimmung der unteren Naturschutzbehörde,
        
        ee)  keine Baujagd bis zu einem Abstand von 100 Metern zu den Gewässerufern erfolgt,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd nur entlang des südlich des Küddens an der Schutzgebietsgrenze verlaufenden Gehölzsaums und an Teilen der nördlichen Schutz­gebietsgrenze zulässig ist.
        Der Bereich ist in den in § 2 Absatz 2 aufgeführten topografischen Karten gekennzeichnet.
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Errichtung anzuzeigen.
        Die untere Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        Die Entscheidung hierzu soll unverzüglich erfolgen,
        
    3.  die Anlage von Kirrungen nur außerhalb gesetzlich geschützter Biotope und außerhalb der in § 3 Absatz 2 Nummer 2 Buchstabe a aufgeführten mageren Flachlandmähwiesen zulässig ist.
        
    
    Im Übrigen bleiben Wildfütterungen, Ansaatwildwiesen und Wildäcker unzulässig;
    
6.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
    
7.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
    
8.  die ordnungsgemäße Unterhaltung der Hochwasserschutzanlagen einschließlich ihrer Deichschutzstreifen und des Deichseitengrabens nach Anzeige gemäß § 34 Absatz 6 des Bundesnaturschutzgesetzes bei der unteren Naturschutzbehörde.
    Die Zeitpunkte des Beginns der Frühjahrsmahd und der Durchführung von Maßnahmen mit Neuaufbau des Deichdeckmaterials über eine Länge von 100 Metern hinaus sind einvernehmlich mit der unteren Naturschutzbehörde abzustimmen; dies kann durch Abstimmung eines Unterhaltungsplanes hergestellt werden.
    
    Ausgenommen von den Verboten des § 4 bleiben weiterhin Maßnahmen des Hochwasserschutzes zur Beseitigung von Abflusshindernissen im Deichvorland wie zum Beispiel Stammholz, Äste, Treibgut, wenn dadurch der Schutzzweck nicht gefährdet wird.
    Die Maßnahmen sind der unteren Naturschutzbehörde gemäß § 34  
    Absatz 6 des Bundesnaturschutzgesetzes schriftlich anzuzeigen;
    
9.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
    
10.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
    
11.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
    
12.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde gebilligt oder angeordnet worden sind;
    
13.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweis­zeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl.
    S. 1734) an Straßen und Wegen freigestellt;
    
14.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.
    

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

#### § 7  
Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  Teilbereiche des Grünlandes sollen zu Refugialräumen für spät reproduzierende Arten mit einer ungestörten Vegetationsphase bis zum 1.
    Juli, 16.
    Juli oder einem späteren Zeitpunkt eines jeden Jahres entwickelt werden;
    
2.  die Stauzielfestlegung des Wehres Gahlberg soll innerhalb des Gebietes zu unterschiedlich vernässten Bereichen führen.
    Auf Teilflächen sollen oberflächennahe Wasserstände mit Blänkenbildung bis zum 30.
    April,  
    31.
    Mai oder 30.
    Juni eines jeden Jahres entstehen.
    Die zum Zeitpunkt des Inkrafttretens der Verordnung be­stehenden Regelungen zur Wasserhaltung sollen beibehalten werden;
    
3.  die Ackerflächen innerhalb des Naturschutzgebietes sollen in Dauergrünland umgewandelt werden;
    
4.  in der Einwirkungszone soll ein Gänsemanagement durchgeführt werden;
    
5.  ein verbesserter hydraulischer Anschluss der Wasserflächen im Küdden wird angestrebt.
    Damit sollen die weitere Verlandung des Küddens aufgehalten und seine Filterfunktion wiederhergestellt werden.
    

#### § 8  
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundes­naturschutzgesetzes Befreiung gewähren.

#### § 9   
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten der §§ 4 und 5 oder den Maßgaben des § 6 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu 50 000 (in Worten: fünfzigtausend) Euro geahndet werden.

#### § 10  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere §§ 31 bis 33 und § 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

#### § 11  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 12   
Inkrafttreten, Außerkrafttreten

(1) § 6 Absatz 1 Nummer 1 tritt mit Wirkung vom 1.
Juli 2010 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig treten außer Kraft:

1.  Anordnung Nummer 3 über Naturschutzgebiete des Landwirtschaftsrates der DDR vom 11. September 1967 (GBl.
    II Nr. 95 S. 967) für das in § 1 Nummer 16 der Anordnung aufgeführte Naturschutzgebiet „Gülper See“ (Bezirk Potsdam);
    
2.  die Behandlungsrichtlinie „Gülper See“ vom 28.
    September 1982.
    

Potsdam, den 1.
Juli 2010

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

* * *

**Anlage 1  
**(zu § 2 Absatz 1)

 **![Das Naturschutzgebiet "Gülper See" liegt im Amt Rhinow (Landkreis Havelland). Es umfasst Teile der Gemarkungen Gülpe, Strodehne, Wolsier und Rhinow. Das Gebit grenzt im Westen an die Gülper Havel und im Osten an die Ortslage Rhinow.](/br2/sixcms/media.php/69/Nr.41-1.GIF "Das Naturschutzgebiet "Gülper See" liegt im Amt Rhinow (Landkreis Havelland). Es umfasst Teile der Gemarkungen Gülpe, Strodehne, Wolsier und Rhinow. Das Gebit grenzt im Westen an die Gülper Havel und im Osten an die Ortslage Rhinow.")**

**Anlage 2  
**(zu § 2 Absatz 2)

### 1. Topografische Karten im Maßstab 1 : 10 000

**Titel:** Topografische Karte zur Verordnung über das Naturschutzgebiet „Gülper See“

**Lfd.
Nummer**

**Kartenblatt**

**Unterzeichnung**

1

3239-NW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit und Verbraucherschutz (MUGV), am 2.
Juni 2010

2

3239-NO

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

3

3240-NW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

4

3239-SW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

5

3239-SO

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

6

3240-SW

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

### 2. Liegenschaftskarten im Maßstab 1 : 2 500

**Titel:** Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Gülper See“

**Blattnummer**

**Gemarkung**

**Flur**

**Unterzeichnung**

1

Strodehne

25

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

2

Strodehne

25

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

3

Strodehne

24, 25, 26

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

4

Strodehne

25, 26, 27

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

5

Rhinow  
Strodehne

17  
25, 27

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

6

Rhinow  
Strodehne

16, 17  
25

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

7

Rhinow

14, 16

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

8

Strodehne

24, 26

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

9

Strodehne

11, 26, 27

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

10

Rhinow  
Strodehne

15, 16, 17  
11, 25, 27

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

11

Rhinow

14, 15, 16, 17

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

12

Rhinow

9, 14, 15, 16

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

13

Rhinow

3, 9, 14

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

14

Gülpe  
Strodehne

1, 2  
11, 26

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

15

Gülpe  
Strodehne

2  
11, 26, 27

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

16

Rhinow  
Strodehne  
Wolsier

15  
11  
4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

17

Rhinow  
Strodehne  
Wolsier

9, 15  
11  
4, 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

18

Rhinow  
Wolsier

9, 14, 15  
6

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

19

Rhinow

9

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

20

Gülpe  
Strodehne

1, 2  
11

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

21

Gülpe  
Strodehne  
Wolsier

2  
11  
4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

22

Gülpe  
Strodehne  
Wolsier

2  
11  
2, 4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

23

Rhinow  
Strodehne  
Wolsier

9  
11  
4, 5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

24

Rhinow  
Wolsier

9  
5, 6

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

25

Gülpe  
Wolsier

2  
2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

26

Gülpe  
Wolsier

2  
2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010

### 3.  Kartenblattübersicht im Maßstab 1 : 30 000

**Titel:**

Naturschutzgebiet „Gülper See“ Kartenblattübersicht Liegenschaftskarte

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 2.
Juni 2010