## Verordnung zur Durchführung des Zensus im Jahr 2022 im Land Brandenburg (Zensusverordnung 2022 - ZensV 2022)

Auf Grund des § 25 Absatz 2 bis 4 des Brandenburgischen Statistikgesetzes vom 1. April 2020 (GVBl. I Nr. 10) verordnet die Landesregierung:

#### § 1 Geltungsbereich

Diese Verordnung regelt die Durchführung der Bevölkerungs-, Gebäude- und Wohnungszählung (Zensus) im Jahr 2022 im Land Brandenburg.

#### § 2 Einrichtung, Leitung und Auflösung der Zensus-Erhebungsstellen

(1) Zur Durchführung des Zensus 2022 richten die kreisfreien Städte und Landkreise für ihr Gebiet ab dem 1. Juli 2021 bis spätestens zum 1.
Oktober 2021 je eine Erhebungsstelle gemäß § 11 des Brandenburgischen Statistikgesetzes (Zensus-Erhebungsstelle) ein.
Maßgebend ist der Gebietsstand vom 31. Dezember 2020.
Den Landkreisen steht es frei, in Einzelfällen örtliche Zweigstellen für ihre Zensus-Erhebungsstelle zu bilden. Die Regelungen für die Zensus-Erhebungsstellen gelten auch für die örtlichen Zweigstellen, soweit in dieser Verordnung keine abweichenden Regelungen getroffen werden.
Die Sonderaufsicht richtet sich nach § 11 Absatz 3 des Brandenburgischen Statistikgesetzes.

(2) Für jede Zensus-Erhebungsstelle bestellt die Hauptverwaltungsbeamtin oder der Hauptverwaltungsbeamte zum 1.
September 2021 eine Erhebungsstellenleitung und eine Stellvertretung.
Diese übernehmen auch die Leitung für die örtlichen Zweigstellen nach Absatz 1 Satz 3.

(3) Die Leitung der Zensus-Erhebungsstelle hat die vorbereitenden Maßnahmen zur Erfüllung der Aufgaben der Zensus-Erhebungsstelle zu veranlassen, die örtliche Durchführung der Erhebungen zu leiten, die Aufsicht über das Personal der Erhebungsstelle und der örtlichen Zweigstelle zu führen sowie die Erhebungsbeauftragten fachlich anzuleiten.
Ihr obliegt die Verpflichtung der Erhebungsbeauftragten nach § 12 Absatz 3 Satz 2 des Brandenburgischen Statistikgesetzes.

(4) Für die Zensus-Erhebungsstelle, nicht jedoch für die örtliche Zweigstelle, ist eine eigene Postanschrift einzurichten.
Die Hauptverwaltungsbeamtin oder der Hauptverwaltungsbeamte erstellt die Dienstanweisung gemäß § 13 Absatz 3 Satz 2 des Brandenburgischen Statistikgesetzes.
Diese muss mindestens folgende Regelungen enthalten:

1.  Bestimmung der Räumlichkeiten für die Zensus-Erhebungsstelle und der örtlichen Zweigstelle,
2.  Maßnahmen zur Sicherung dieser Räumlichkeiten gegen unbefugten Zutritt,
3.  Zugangsberechtigung zu den Räumlichkeiten der Zensus-Erhebungsstelle und der örtlichen Zweigstelle,
4.  Maßnahmen zur Kontrolle der Zugangsberechtigung,
5.  Maßnahmen zur Sicherung der Erhebungsunterlagen,
6.  Maßnahmen zur Datensicherung in Datenverarbeitungsanlagen,
7.  Geschäftsverteilung, Vertretung und Dienstaufsicht in der Zensus-Erhebungsstelle und in der örtlichen Zweigstelle.

§ 24 des Brandenburgischen Datenschutzgesetzes gilt entsprechend, soweit nicht besondere Rechtsvorschriften vorgehen.

(5) Die Zensus-Erhebungsstellen sind unverzüglich nach Erfüllung ihrer Aufgaben, spätestens aber am 28. Februar 2023, aufzulösen.

#### § 3 Aufgaben der Zensus-Erhebungsstellen

(1) Die Zensus-Erhebungsstellen sorgen für die ordnungsgemäße Durchführung der Erhebungen nach den §§ 9, 11, 14 und 22 des Zensusgesetzes 2022 vom 26.
November 2019 (BGBl.
I S. 1851), das durch Artikel 2 des Gesetzes vom 3.
Dezember 2020 (BGBl.
I.
S.
2675) geändert worden ist, in der jeweils geltenden Fassung.

(2) Bei der Erhebung nach § 9 des Zensusgesetzes 2022 übernehmen die Zensus-Erhebungsstellen insbesondere Aufgaben im Rahmen der Feststellung der Auskunftspflicht, der Überprüfung und Klärung von Zweifelsfällen und der ersatzweisen Befragung von Bewohnerinnen oder Bewohnern bei Antwortausfällen nach § 24 Absatz 4 des Zensusgesetzes 2022.

(3) Bei der Durchführung der Erhebungen nach den §§ 11, 14 und 22 des Zensusgesetzes 2022 sind insbesondere folgende Aufgaben zu erfüllen:

1.  die Erreichbarkeit für mündliche, telefonische und schriftliche Anfragen von Auskunftspflichtigen und Erhebungsbeauftragten zu sichern,
2.  die Anschriften auf Plausibilität und regionale Zugehörigkeit zu prüfen,
3.  die Vorbegehung der Großanschriften zu koordinieren und durchzuführen,
4.  die Anschriften (Erhebungsbezirke) den einzelnen Erhebungsbeauftragten zuzuordnen,
5.  die Organisationspapiere zu erstellen und die Erhebungsunterlagen bereitzustellen,
6.  die zu Befragenden über die Erhebungen zu unterrichten und zur Auskunft aufzufordern,
7.  erforderlichenfalls die Auskunftspflichtigen durch Heranziehungsbescheid zur Erfüllung ihrer Auskunftspflichten aufzufordern und die Auskunftspflichten durchzusetzen,
8.  die Entgegennahme der Erhebungsunterlagen und der mündlich, telefonisch und schriftlich eingegangenen Auskünfte sicherzustellen,
9.  alle Auskunftseingänge zu registrieren,
10.  notwendige Datenerfassungen und Plausibilitätsprüfungen durchzuführen, auftretende Unstimmigkeiten zu klären sowie unvollständig oder fehlerhaft ausgefüllte Erhebungsunterlagen durch Nachfrage bei den Befragten zu ergänzen und zu berichtigen,
11.  die vollzählige Erfassung und vollständige Befragung der Erhebungseinheiten zu bestätigen,
12.  die ermittelten Angaben in die zentralen informationstechnischen Verbundverfahren einzupflegen, die eingegangenen Fragebögen an einen vom Amt für Statistik Berlin-Brandenburg bestimmten Dienstleister (Beleglesezentrum) zu senden und die übrigen Erhebungsunterlagen an das Amt für Statistik Berlin-Brandenburg zu übermitteln,
13.  die für die Durchführung der Erhebungen nach dem Zensusgesetz 2022 benötigten Erhebungsbeauftragten anzuwerben, auszuwählen, zu bestellen und abzuberufen, auf die Wahrung des Statistikgeheimnisses und des Datengeheimnisses schriftlich zu verpflichten und über ihre Rechte und Pflichten zu belehren,
14.  die Erhebungsbeauftragten für die in Absatz 1 genannten Erhebungen nach den Vorgaben des Amtes für Statistik Berlin-Brandenburg zu schulen, die Schulung und die ordnungsgemäße Aufgabenerledigung der Erhebungsbeauftragten zu dokumentieren und an das Amt für Statistik Berlin-Brandenburg zu übermitteln,
15.  die Aufwandsentschädigung der Erhebungsbeauftragten abzurechnen und auszuzahlen.

Zur Erfüllung der Aufgaben nach Satz 1 Nummer 13 bis 15 und zur Zuverlässigkeitsprüfung nach § 12 Absatz 1 des Brandenburgischen Statistikgesetzes dürfen die Zensus-Erhebungsstellen die zur Erfüllung ihrer Aufgaben erforderlichen personenbezogenen Daten der Erhebungsbeauftragten verarbeiten.

(4) Die Zensus-Erhebungsstellen sind nicht befugt, Auswertungen der erhobenen Daten selbst vorzunehmen oder durch Dritte vornehmen zu lassen.

#### § 4 Erhebungsbeauftragte

(1) Für die Durchführung der Erhebungen nach den §§ 9, 11 14 und 22 des Zensusgesetzes 2022 können die Zensus-Erhebungsstellen gemäß § 20 Absatz 1 Satz 1 des Zensusgesetzes 2022 Erhebungsbeauftragte einsetzen.
Die Bestimmungen des § 12 Brandenburgisches Statistikgesetz bleiben unberührt.

(2) Die kreisfreien Städte und Landkreise, die die Aufgaben der Errichtung einer Zensus-Erhebungsstelle gemäß § 2 Absatz 1 wahrnehmen, stellen bei Bedarf Bedienstete für ihre Zensus-Erhebungsstelle als Erhebungsbeauftragte frei.
Gemeinden, Gemeindeverbände und unter der Aufsicht des Landes stehende juristische Personen des öffentlichen Rechts benennen den Zensus-Erhebungsstellen ihres Landkreises auf Ersuchen Bedienstete und stellen sie für die Tätigkeit als Erhebungsbeauftragte frei.
Für die Verpflichtung der Gemeindeverbände und der unter der Aufsicht des Landes stehenden juristischen Personen des öffentlichen Rechts gilt Satz 2 für Zensus-Erhebungsstellen der kreisfreien Städte entsprechend.
Lebenswichtige Tätigkeiten öffentlicher Dienste dürfen nicht unterbrochen werden.

(3) Gemeinden benennen über den Personenkreis nach Absatz 2 hinausgehend den Zensus-Erhebungsstellen auf Ersuchen Bürgerinnen und Bürger ihrer Gemeinde für die Tätigkeit als Erhebungsbeauftragte.

#### § 5 Kostenregelung

(1) Das Land gewährt den kreisfreien Städten und den Landkreisen für die mit der Durchführung des Zensus 2022 verbundenen Mehrbelastungen einen entsprechenden finanziellen Ausgleich nach § 20 Absatz 1 des Brandenburgischen Statistikgesetzes gemäß der Anlage.
Der Mehrbelastungsausgleich bemisst sich nach der Art und dem Umfang der Einbindung der kreisfreien Städte und der Landkreise in die Aufgabenerfüllung nach den §§ 2 bis 4.

(2) Bemessungsgrundlagen für den Ausgleich der Mehraufwendungen an die in § 2 Absatz 1 Satz 1 genannten kommunalen Körperschaften sind:

1.  Sachaufwendungen für die Einrichtung, den Betrieb und die Auflösung der Zensus-Erhebungsstellen und ihrer Zweigstellen entsprechend der Betriebsdauer der Zensus-Erhebungsstellen und ihrer Zweigstellen, der Anzahl des eingesetzten Erhebungsstellenpersonals und der Anzahl der Verwaltungs-PC (Arbeitsplatzpauschale),
2.  Personalaufwendungen, die in den Zensus-Erhebungsstellen und ihren Zweigstellen für die Erfüllung der Aufgaben nach § 3 entstehen, entsprechend der Anzahl des Erhebungsstellenpersonals, der Dauer des Beschäftigungsverhältnisses und der Höhe der Vergütung sowie die damit verbundenen allgemeinen Verwaltungskosten (Verwaltungsgemeinkosten) und
3.  fallzahlabhängige Sachaufwendungen
    1.  als Aufwandsentschädigung der ehrenamtlichen Erhebungsbeauftragten für die ersatzweise Befragung im Rahmen der Gebäude- und Wohnungszählung nach § 9 des Zensusgesetzes 2022, sofern erforderlich, in Höhe von 15 Euro je Anschrift,
    2.  als Aufwandsentschädigung der ehrenamtlichen Erhebungsbeauftragten im Rahmen der Erhebungen nach den §§ 11, 14 und 22 des Zensusgesetzes 2022 für die
        
        aa)
        
        Teilnahme an Schulungen und für die anfallenden Fahrtkosten in Höhe von pauschal 300 Euro, sofern 150 Auskunftspflichtige befragt werden,
        
        bb)
        
        Begehung in Höhe von 3 Euro je begangene Anschrift,
        
        cc)
        
        Befragung in Höhe von 5 Euro je befragte Person bei erfolgreich durchgeführten Befragungen beziehungsweise in Höhe von 1 Euro je Person bei Antwortausfällen,
        
        dd)
        
        Gewinnung zusätzlicher prozessrelevanter Informationen in Höhe von 1 Euro pro Person,
        
        ee)
        
        Befragung an Anschriften mit Sonderbereichen mit Gemeinschaftsunterkünften nach den §§ 14 bis 17 des Zensusgesetzes 2022 in Höhe von 15 Euro je Gemeinschaftsunterkunft sowie
        
    3.  für Portokosten.

Die Höhe des Mehrbelastungsausgleichs für die Aufwendungen nach Satz 1 Nummer 1 bis 3 ergibt sich aus der Anlage.

(3) Notwendige Mehrbelastungen, die bei kostenbewusster Aufgabenwahrnehmung anfallen und die noch keinen Ausgleich gefunden haben, sind spätestens zur Abschlussrechnung nach § 6 Absatz 2 Nummer 4 nachzuweisen und in dieser zu berücksichtigen.
Dies gilt auch für die Aufwendungen nach Absatz 2 Satz 1 Nummer 3 Buchstabe b und c sowie für Aufwendungen, die mit einer etwaigen projektbedingten Verlängerung der Betriebsdauer der Zensus-Erhebungsstellen verbunden sind.
Die Höhe der weiteren erstattungsfähigen Kosten für die Einrichtung zusätzlicher Arbeitsplätze und die Beschäftigung von zusätzlichem Personal bemisst sich dabei insbesondere an den Fallzahlen pro Erhebungsbereich entsprechend der §§ 9, 11, 14 und 22 des Zensusgesetzes 2022.

(4) Der zur Bewältigung der Aufgaben der Zensus-Erhebungsstellen erforderliche Zugang zu den zentralen Zensus-IT-Fachanwendungen wird vom Amt für Statistik Berlin-Brandenburg über den statistischen Verbund kostenlos bereitgestellt.

(5) Die Kosten der Datenübermittlungen an das Amt für Statistik Berlin-Brandenburg werden gemäß § 16 Absatz 4 Satz 3 des Brandenburgischen Statistikgesetzes nicht erstattet.

(6) Einnahmen aus Verwaltungszwangsverfahren, aus der Vollstreckung von Zwangsgeldforderungen oder sonstige damit verbundene Einnahmen verbleiben bei den kreisfreien Städten und den Landkreisen.

#### § 6 Erstattungsverfahren und Kostennachweis

(1) Zuständige Stelle für die Verwaltung und Abrechnung der Mittel, die vom Land den kreisfreien Städten und Landkreisen gemäß § 20 Absatz 1 des Brandenburgischen Statistikgesetzes in Verbindung mit § 5 Absatz 1 gewährt werden, ist das Amt für Statistik Berlin-Brandenburg.

(2) Die Zahlung der Finanzzuweisung erfolgt in vier Teilbeträgen gemäß der Anlage:

1.  Zum 30.
    Juni 2021 wird die Zuweisung nach § 5 Absatz 2 Satz 1 Nummer 1 und ein Viertelbetrag der Zuweisung nach § 5 Absatz 2 Satz 1 Nummer 2 ausgezahlt.
2.  Zum 15.
    Januar 2022 wird der hälftige Betrag der Zuweisung nach § 5 Absatz 2 Satz 1 Nummer 2 ausgezahlt.
3.  Zum 15.
    Mai 2022 wird ein Viertelbetrag der Zuweisung nach § 5 Absatz 2 Satz 1 Nummer 2 sowie ein Viertelbetrag der Zuweisung nach § 5 Absatz 2 Satz 1 Nummer 3 ausgezahlt.
4.  Die Schlusszahlung wird alsbald nach Fertigung und Prüfung der Abschlussrechnung gemäß Absatz 3 fällig, spätestens jedoch zum 30.
    November 2023.
    Diese Frist verlängert sich, wenn sie sachlich gerechtfertigt ist.

(3) Das Amt für Statistik Berlin-Brandenburg fertigt die Abschlussrechnung unter Berücksichtigung der Bemessungsgrundlagen nach § 5 Absatz 2 und 3 sowie bereits gezahlter Beträge nach Absatz 2 Nummer 1 bis 3.
Hierfür weisen die kreisfreien Städte und Landkreise die ihnen entstandenen Kosten bis spätestens drei Monate nach Auflösung der Zensus-Erhebungsstelle in geeigneter und prüfbarer Weise gegenüber dem Amt für Statistik Berlin-Brandenburg nach.
Die Abschlussrechnung wird durch das für die amtliche Statistik zuständige Ministerium geprüft.

(4) Den kreisfreien Städten und Landkreisen wird die ihre Zensus-Erhebungsstelle betreffende Abschlussrechnung schriftlich durch das Amt für Statistik Berlin-Brandenburg mitgeteilt.
Dabei sind nicht berücksichtigte Forderungen zu begründen und auf die Ausschlusswirkung der Schlusszahlung hinzuweisen.
Die Ausschlussfrist gilt nicht für ein Verlangen nach Richtigstellung der Abschlussrechnung und der Schlusszahlung wegen Rechen- und Übertragungsfehlern.

(5) Überzahlungen sind von den kreisfreien Städten und den Landkreisen an das Amt für Statistik Berlin-Brandenburg nach Eingang der Abschlussrechnung innerhalb von vier Wochen zurück zu zahlen.

#### § 7 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft und am 31. Dezember 2030 außer Kraft.

Potsdam, den 17.
März 2021

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Minister des Innern und für Kommunales

Michael Stübgen

### Anlagen

1

[Anlage (zu § 5 Absatz 1 und 2, § 6 Absatz 2)](/br2/sixcms/media.php/68/GVBl_II_29_2021%20Anlage.pdf "Anlage (zu § 5 Absatz 1 und 2, § 6 Absatz 2)") 156.0 KB