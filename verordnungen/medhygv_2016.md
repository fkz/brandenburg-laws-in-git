## Verordnung über die Hygiene und Infektionsprävention in medizinischen Einrichtungen (MedHygV)

Auf Grund des § 23 Absatz 5 Satz 3 und Absatz 8 Satz 3 des Gesetzes zur Verhütung und Bekämpfung von Infektionskrankheiten beim Menschen vom 20.
Juli 2000 (BGBl.
I S. 1045), der durch Artikel 1 des Gesetzes vom 28.
Juli 2011 (BGBl.
I S. 1622) neu gefasst worden ist, und des § 2 der Infektionsschutzzuständigkeitsverordnung vom 27.
November 2007 (GVBl.
II S. 488), der durch die Verordnung vom 10.
Januar 2012 (GVBl.
II Nr. 2) neu gefasst worden ist, verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1  
Regelungsgegenstand, Geltungsbereich

Diese Verordnung regelt die erforderlichen Maßnahmen zur Verhütung, Erkennung, Erfassung und Bekämpfung von nosokomialen Infektionen und Krankheitserregern mit Resistenzen in nachfolgenden medizinischen Einrichtungen:

1.  Krankenhäusern,
2.  Einrichtungen für ambulantes Operieren,
3.  Vorsorge- oder Rehabilitationseinrichtungen, in denen eine den Krankenhäusern entsprechend § 23 Absatz 3 Nummer 1 des Infektionsschutzgesetzes vergleichbare medizinische Versorgung erfolgt,
4.  Dialyseeinrichtungen,
5.  Tageskliniken,
6.  Arztpraxen und Zahnarztpraxen sowie
7.  sonstige Praxen, in denen Heilberufsangehörige invasive Eingriffe am Menschen vornehmen.

Hiervon unberührt bleiben die Verpflichtungen nach den medizinproduktrechtlichen Rechtsvorschriften, dem Transfusionsgesetz, den arzneimittel-, apotheken-, transplantations-, arbeitsschutz- und berufsrechtlichen Regelungen.

### § 2  
Anforderungen an Bau und Betrieb medizinischer Einrichtungen

(1) Die Empfehlungen der nach § 23 Absatz 1 Satz 1 des Infektionsschutzgesetzes eingerichteten Kommission zur Krankenhaushygiene und Infektionsprävention beim Robert-Koch-Institut sind einzuhalten.

(2) Die Träger der in § 1 Satz 1 genannten medizinischen Einrichtungen sind verpflichtet, die betrieblich-organisatorischen und baulich-funktionellen Voraussetzungen für die Einhaltung der Hygiene sicherzustellen.

(3) Baulich-funktionelle Anlagen, von denen ein infektionshygienisches Risiko ausgehen kann, sind nach den allgemein anerkannten Regeln der Technik zu betreiben und zu warten.
Sie sind zusätzlich regelmäßigen hygienischen Überprüfungen durch den Betreiber der Anlage zu unterziehen.
Die Anlagen dürfen nur von fachlich geschultem Personal betrieben und gewartet werden.

(4) Für Einrichtungen nach § 1 Satz 1 Nummer 1 bis 5 sind Bauvorhaben vor Beantragung der Baugenehmigung oder vor ihrer Durchführung hinsichtlich der hygienischen Anforderungen durch eine Krankenhaushygienikerin oder einen Krankenhaushygieniker in einem Gutachten zu bewerten.
Notwendige Bauunterlagen sowie das krankenhaushygienische Gutachten sind dem jeweils zuständigen Gesundheitsamt der Landkreise und kreisfreien Städte vor Antragstellung der Baugenehmigung und soweit keine Genehmigung einzuholen ist, vor Baubeginn, zu übermitteln.

### § 3   
Hygienekommission

(1) Für jede Einrichtung nach § 1 Satz 1 Nummer 1 und 3 ist eine Hygienekommission einzurichten.
Die Mitglieder der Hygienekommission werden für drei Jahre bestellt.
Der Hygienekommission müssen als Mitglieder angehören:

1.  die ärztliche Leitung,
2.  die Verwaltungsleitung,
3.  die Geschäftsführung,
4.  die Pflegedienstleitung,
5.  eine Krankenhaushygienikerin oder ein Krankenhaushygieniker,
6.  mindestens eine Hygienefachkraft,
7.  mindestens eine hygienebeauftragte Ärztin oder ein hygienebeauftragter Arzt.

(2) Die Hygienekommission kann weitere Fachkräfte als Mitglieder hinzuziehen.
Als solche kommen beispielsweise Mikrobiologinnen und Mikrobiologen von Laboratorien einschließlich der Krankenhauslaboratorien, eine Betriebsärztin oder ein Betriebsarzt, eine versorgende Krankenhausapothekerin oder ein versorgender Krankenhausapotheker, sowie die technische Leitung in Betracht.

(3) Die Hygienekommission berät und unterstützt die Einrichtungsleitungen in allen krankenhaushygienischen Angelegenheiten.
Sie hat insbesondere

1.  über die in den Hygieneplänen nach § 23 Absatz 5 Satz 1 des Infektionsschutzgesetzes festgelegten innerbetrieblichen Verfahrensweisen zur Infektionshygiene zu beschließen, an deren Fortschreibung mitzuwirken und auf deren Einhaltung hinzuwirken,
2.  auf der Basis des Risikoprofils der Einrichtung, das von der Krankenhaushygienikerin oder dem Krankenhaushygieniker zu ermittelt ist, den erforderlichen Bedarf an Hygienefachpersonal nach den §§ 6 bis 9 festzustellen,
3.  Empfehlungen für die Aufzeichnung von nosokomialen Infektionen, des Auftretens von Krankheitserregern mit speziellen Resistenzen und Multiresistenzen sowie des Antibiotikaverbrauchs entsprechend § 23 Absatz 4 des Infektionsschutzgesetzes zu erarbeiten,
4.  die Aufzeichnungen nach Nummer 3 zu bewerten und sachgerechte Schlussfolgerungen hinsichtlich erforderlicher Präventionsmaßnahmen und hinsichtlich des Einsatzes von Antibiotika zu ziehen,
5.  Untersuchungen, Maßnahmen und die Dokumentation nach § 11 festzulegen, wobei standortspezifische Belange zu beachten sind,
6.  bei der Planung von Baumaßnahmen, der Beschaffung von Anlagegütern und der Änderung von Organisationsplänen mitzuwirken, soweit Belange der Krankenhaushygiene berührt sind,
7.  den hausinternen Fortbildungsplan auf dem Gebiet der Hygiene und Infektionsprävention einschließlich des Antibiotikaeinsatzes jährlich zu beschließen sowie
8.  für das medizinische Personal mindestens einmal jährlich eine berufsgruppenspezifische Hygienepflichtfortbildung durchzuführen.

(4) Der Vorsitz der Hygienekommission obliegt der ärztlichen Leitung der Einrichtung oder einer von ihr benannten Vertretung.
Die oder der Vorsitzende beruft die Hygienekommission mindestens halbjährlich ein, im Übrigen nach Bedarf.
Bei gehäuftem Auftreten von Krankenhausinfektionen und bei besonderen, die Hygiene betreffenden Vorkommnissen, ist die Hygienekommission unverzüglich einzuberufen.
Gleiches gilt, wenn ein Drittel der Mitglieder aus einem der in Satz 3 genannten Gründe die Einberufung verlangt.
Die Sitzungsfrequenz der Hygienekommission kann in Einrichtungen reduziert werden, in denen bestimmungsgemäß Tätigkeiten ausgeführt werden, die mit einem geringen Risiko für die Entstehung und Weiterverbreitung nosokomialer Infektionen verbunden ist.

(5) Mehrere Einrichtungen eines Trägers im Geltungsbereich dieser Verordnung können eine gemeinsame einrichtungsübergreifende Hygienekommission bilden.
Aus welcher der beteiligten Einrichtungen jeweils die in § 3 Absatz 1 Satz 2 genannten Mitglieder stammen, bestimmt der Träger der Einrichtung.

(6) Die Hygienekommission gibt sich eine Geschäftsordnung.

(7) Die Ergebnisse der Beratungen sind schriftlich zu dokumentieren.
Die Aufzeichnungen sind zehn Jahre aufzubewahren.
Dem zuständigen Gesundheitsamt der Landkreise und kreisfreien Städte ist auf Verlangen Einsicht in die Aufzeichnungen zu gewähren.

### § 4  
Hygienepläne

Leitungen von Einrichtungen nach § 1 Satz 1 sind verpflichtet, auf der Grundlage einer Analyse und Bewertung der jeweiligen Infektionsrisiken innerbetriebliche Verfahrensanweisungen zur Infektionsprävention in Hygieneplänen festzulegen.

### § 5  
Fachpersonal

(1) In medizinischen Einrichtungen nach § 1 Satz 1 Nummer 1 und 3 sind nach Maßgabe der §§ 6 bis 9 Hygienefachkräfte, Krankenhaushygienikerinnen und Krankenhaushygieniker zu beschäftigen sowie hygienebeauftragte Ärztinnen und Ärzte und Hygienebeauftragte in der Pflege zu bestellen.

(2) Die Einrichtungen nach § 1 Satz 1 Nummer 2, 4 und 5 haben qualifizierte Ärztinnen und Ärzte hinzuzuziehen, die sie zu klinisch-mikrobiologischen und klinisch-pharmazeutischen Fragestellungen beraten.
Diese Ärztinnen und Ärzte haben die Leitung von Einrichtungen nach § 1 Satz 1 Nummer 2 bei der Erfüllung der Pflichten nach § 23 Absatz 4 des Infektionsschutzgesetzes zu unterstützen.

(3) Fachlich geeignetes Personal, das die Anforderungen an die Qualifikation nach den §§ 6 bis 8 nicht erfüllt, darf bis zum 31.
Dezember 2019 als Hygienefachkraft, als Krankenhaushygienikerin oder als Krankenhaushygieniker eingesetzt oder als hygienebeauftragte Ärztin oder als hygienebeauftragter Arzt bestellt werden.

### § 6  
Hygienefachkräfte

(1) Hygienefachkräfte sind im klinischen Alltag zentrale Ansprechpartner für alle Berufsgruppen und tragen damit zur Umsetzung infektionspräventiver Maßnahmen bei.
Sie vermitteln die Maßnahmen und Inhalte von Hygieneplänen, wirken bei deren Erstellung mit, kontrollieren die Umsetzung empfohlener Hygienemaßnahmen, führen hygienisch-mikrobiologische Umgebungsuntersuchungen durch, wirken bei der Erfassung und Bewertung nosokomialer Infektionen und von Erregern mit speziellen Resistenzen und Multiresistenzen mit und unterstützen bei der Aufklärung und dem Management von Ausbrüchen.
Darüber hinaus führen sie hygienisch-spezifische Fortbildungen nach Maßgabe von § 10 durch.
Sie unterstehen der fachlichen Weisung der Krankenhaushygienikerin oder des Krankenhaushygienikers.
In den Einrichtungen nach § 1 Satz 1 Nummer 1 und 3 ohne hauptamtliche Krankenhaushygienikerin oder hauptamtlichen Krankenhaushygieniker sind die Hygienefachkräfte der ärztlichen Leitung unterstellt.
Sie arbeiten eng mit den hygienebeauftragten Ärztinnen und Ärzten zusammen.
Die Hygienefachkräfte werden von den Mitgliedern der Hygienekommission bei der Umsetzung der festgelegten Hygienemaßnahmen unterstützt.

(2) Die Qualifikation für die Wahrnehmung der Aufgaben einer Hygienefachkraft besitzt, wer berechtigt ist, eine Berufsbezeichnung nach dem Gesetz über die Berufe in der Krankenpflege zu führen, über eine mindestens dreijährige Berufserfahrung verfügt und eine Weiterbildung zur Fachkraft für Hygiene in der Pflege an einer nach dem Gesetz über die Weiterbildung in den Fachberufen des Gesundheitswesens staatlich anerkannten Weiterbildungsstätte erfolgreich abgeschlossen hat.
Einem erfolgreichen Weiterbildungsabschluss gleichgestellt ist eine Weiterbildung nach § 4 Absatz 3 oder Absatz 4 des Gesetzes über die Weiterbildung in den Fachberufen des Gesundheitswesens.

(3) Der Personalbedarf für Hygienefachkräfte in medizinischen Einrichtungen nach § 1 Satz 1 Nummer 1 und 3 muss nach § 3 Absatz 3 Nummer 2 unter Berücksichtigung des Behandlungsspektrums der Einrichtung und des Risikoprofils der dort behandelten Patientinnen und Patienten festgestellt werden.
Die Personalbedarfsermittlung ist auf der Grundlage der Risikobewertung nach der jeweils geltenden aktuellen Empfehlung der nach § 23 Absatz 1 Satz 1 des Infektionsschutzgesetzes eingerichteten Kommission für Krankenhaushygiene und Infektionsprävention vorzunehmen und umzusetzen.

### § 7  
Krankenhaushygienikerin und Krankenhaushygieniker

(1) Die Krankenhaushygienikerin oder der Krankenhaushygieniker koordiniert die Surveillance und Maßnahmen der Prävention von nosokomialen Infektionen und des Ausbruchsmanagements.
Sie oder er berät die Leitung der Einrichtung sowie die ärztlich und pflegerisch Verantwortlichen in allen Fragen der Krankenhaushygiene, bewertet die vorhandenen Risiken und schlägt Maßnahmen zur Erkennung, Verhütung und Bekämpfung von nosokomialen Infektionen vor.
Darüber hinaus führt sie oder er hygienisch-spezifische Fortbildungen nach Maßgabe des § 10 durch.

(2) Die Qualifikation für die Wahrnehmung der nach Absatz 1 genannten Aufgaben besitzt, wer die Anerkennung als Fachärztin oder Facharzt für Hygiene und Umweltmedizin oder für Mikrobiologie, Virologie und Infektionsepidemiologie erhalten hat.
Die Qualifikation besitzt auch, wer approbierte Humanmedizinerin oder approbierter Humanmediziner ist, eine Facharztweiterbildung erfolgreich abgeschlossen hat und eine von einer Landesärztekammer anerkannte Zusatzbezeichnung auf dem Gebiet der Krankenhaushygiene erworben oder eine durch eine Landesärztekammer anerkannte strukturierte, curriculäre Fortbildung zur Krankenhaushygienikerin beziehungsweise zum Krankenhaushygieniker erfolgreich absolviert hat.

(3) In den medizinischen Einrichtungen nach § 1 Satz 1 Nummer 1 und 3 ist organisatorisch sicherzustellen, dass eine Beratung durch eine Krankenhaushygienikerin oder einen Krankenhaushygieniker gewährleistet ist.
Der Bedarf an Krankenhaushygienikern hängt vom Infektionsrisiko innerhalb der Einrichtung ab.
Verbindlicher Orientierungsmaßstab ist die Empfehlung der nach § 23 Absatz 1 Satz 1 des Infektionsschutzgesetzes eingerichteten Kommission für Krankenhaushygiene und Infektionsprävention in der jeweils geltenden Fassung.

### § 8  
Hygienebeauftragte Ärztin und hygienebeauftragter Arzt

(1) Die hygienebeauftragte Ärztin oder der hygienebeauftragte Arzt ist Ansprechperson sowie Multiplikator und unterstützt das Hygienefachpersonal im jeweiligen Verantwortungsbereich.
Sie oder er wirkt bei der Einhaltung der Regeln der Hygiene und Infektionsprävention mit und regt Verbesserungen der Hygienepläne und Funktionsabläufe an.
Sie oder er wirkt außerdem bei der hausinternen Fortbildung des Krankenhauspersonals in der Krankenhaushygiene mit.
Zur Erfüllung der Aufgaben ist eine Freistellung von den üblicherweise ausgeübten Tätigkeiten entsprechend dem Umfang erforderlich, um die Aufgaben nach Satz 1 bis 3 wahrzunehmen.

(2) Als hygienebeauftragte Ärztin oder als hygienebeauftragter Arzt darf nur bestellt werden, wer eine Anerkennung als Fachärztin oder als Facharzt erhalten hat und an einer von einer Landesärztekammer anerkannten strukturierten curriculären Fortbildung als hygienebeauftragter Arzt beziehungsweise als hygienebeauftragte Ärztin teilgenommen hat.

(3) Jede medizinische Einrichtung nach § 1 Satz 1 Nummer 1 und 3 hat mindestens eine hygienebeauftragte Ärztin oder einen hygienebeauftragten Arzt zu bestellen.
In Einrichtungen nach § 1 Satz 1 Nummer 1 mit mehreren Fachabteilungen mit besonderem Risikoprofil für nosokomiale Infektionen ist für jede Fachabteilung eine hygienebeauftragte Ärztin oder ein hygienebeauftragter Arzt zu bestellen.
Verbindlicher Orientierungsmaßstab ist die Empfehlung der nach § 23 Absatz 1 Satz 1 des Infektionsschutzgesetzes eingerichteten Kommission für Krankenhaushygiene und Infektionsprävention in der jeweils geltenden Fassung.

### § 9  
Hygienebeauftragte in der Pflege

(1) Hygienebeauftragte in der Pflege in den Einrichtungen nach § 1 Satz 1 Nummer 1 und 3 fungieren als Multiplikatoren hygienerelevanter Bereiche auf den Stationen oder in den Funktionsbereichen.
Sie unterstützen die Hygienefachkräfte bei der Umsetzung von Hygienemaßnahmen.
Sie wirken bei der Erstellung bereichsspezifischer Hygienestandards, der Umsetzung und Schulung von Hygienepraktiken sowie bei der Erkennung und organisatorischen Bewältigung von Ausbruchsgeschehen mit.

(2) Hygienebeauftragte in der Pflege sind Personen, die eine Berufsbezeichnung nach dem Gesetz über die Berufe in der Krankenpflege führen und über eine mehrjährige Berufserfahrung verfügen.

(3) Jede medizinische Einrichtung nach § 1 Satz 1 Nummer 1 und 3 soll mindestens eine Hygienebeauftragte oder einen Hygienebeauftragten in der Pflege bestellen.
In Einrichtungen nach § 1 Satz 1 Nummer 1 mit mehreren Fachabteilungen mit besonderem Risikoprofil für nosokomiale Infektionen soll für jede Fachabteilung eine gesonderte Hygienebeauftragte oder ein Hygienebeauftragter in der Pflege bestellt werden.

### § 10  
Fortbildung des Personals

(1) Krankenhaushygienikerinnen und Krankenhaushygieniker, Hygienebeauftragte in der Pflege sowie Hygienefachkräfte sind verpflichtet, sich mit dem aktuellen Stand der Krankenhaushygiene vertraut zu machen, darüber hinaus müssen sie mindestens alle zwei Jahre hygienespezifische Fortbildungsveranstaltungen besuchen.
Zum Besuch entsprechender Fortbildungen sind sie von ihren Aufgaben freizustellen.

(2) Die Fortbildung über Grundlagen und Zusammenhänge der Krankenhaushygiene und Infektionsprävention der hygienebeauftragten Ärztinnen und der hygienebeauftragten Ärzte sowie des weiteren Personals in medizinischen Einrichtungen nach § 1 Satz 1 Nummer 1 und 3 ist Aufgabe der Krankenhaushygienikerin oder des Krankenhaushygienikers und der Hygienefachkräfte.
In den Einrichtungen nach § 1 Satz 1 Nummer 2, 4 und 5 sind dafür die benannten qualifizierten  Ärztinnen und Ärzte verantwortlich.

### § 11  
Dokumentation und Bewertung von nosokomialen Infektionen, Ausbruchsmanagement und Antibiotikaresistenzen

(1) Die Leitungen der medizinischen Einrichtungen nach § 1 Satz 1 haben sicherzustellen, dass Patientinnen und Patienten, von denen ein Risiko für nosokomiale Infektionen ausgeht, frühzeitig identifiziert und geeignete Schutzmaßnahmen eingeleitet werden.
Die Untersuchungen und Maßnahmen sind in der Patientenakte zu dokumentieren.
Die Dokumentation muss so gestaltet sein, dass  das Personal umfassend informiert ist und die Umsetzung der Schutzmaßnahmen nicht verzögert wird.

(2) Die Erfassung und Bewertung von nosokomialen Infektionen hat mit geprüften Verfahren, wie dem von der nach § 23 Absatz 1 Satz 1 des Infektionsschutzgesetzes eingerichteten Kommission zur Krankenhaushygiene und Infektionsprävention empfohlenen Krankenhaus-Infektions-Surveillance-System zu erfolgen.

(3) Leitungen von Einrichtungen nach § 1 Satz 1 Nummer 1 bis 3 sind zu einer fortlaufenden systematischen Erfassung, Analyse und Bewertung von aufgetretenen Krankheitserregern mit speziellen Resistenzen und Multiresistenzen nach Festlegung und Definition des Robert-Koch-Institutes verpflichtet.

(4) In den in § 1 Satz 1 Nummer 1 und 3 genannten medizinischen Einrichtung werden die Daten zu nosokomialen Infektionen, zu Antibiotikaresistenzen und zu Art und Umfang des Antibiotikaverbrauchs unter Anleitung der Krankenhaushygienikerin oder des Krankenhaushygienikers so aufbereitet, dass Infektionsgefahren aufgezeigt und Präventionsmaßnahmen abgeleitet und in das Hygienemanagement aufgenommen werden können.
In den in § 1 Satz 1 Nummer 2 und 4 genannten Einrichtungen erfolgt die Datenerfassung durch eine entsprechend qualifizierte Ärztin oder einen entsprechend qualifizierten Arzt nach § 5 Absatz 2.

(5) Die erfolgten Änderungen der Organisations- und Funktionsabläufe müssen jährlich im Rahmen des Qualitätsmanagements geprüft  und in die entsprechenden Verfahrensanweisungen übernommen werden.

(6) Die Aufzeichnungen nach den Absätzen 2, 3 und 4 sind zehn Jahre aufzubewahren, sie erfolgen nicht personen- oder patientenbezogen.

### § 12  
Zutritts- und Einsichtsrecht

(1) Die Krankenhaushygienikerin oder der Krankenhaushygieniker, die hygienebeauftragten Ärztinnen und Ärzte sowie die Hygienefachkräfte haben im Rahmen ihres Beschäftigungs- oder Auftragsverhältnisses das Recht, alle zur medizinischen Einrichtung gehörenden Geschäfts- und Betriebsräume einschließlich aller begehbaren Anlagen zu betreten.
Darüber hinaus haben sie ein Einsichtsrecht in alle Aufzeichnungen, Bücher und Unterlagen, aus denen Informationen für eine grundsätzliche hygienerelevante Beurteilung entnommen werden kann.
Das Einsichtsrecht erstreckt sich auch auf die Patientenakten.
Das Zutritts- und Einsichtsrecht ist ausschließlich für die zur Erfüllung der nach dieser Verordnung übertragenen Aufgaben anzuwenden.

(2) Die Aufzeichnungen zur Erfassung und Bewertung nosokomialer Infektionen nach § 23 Absatz 4 des Infektionsschutzgesetzes sind der Krankenhaushygienikerin oder dem Krankenhaushygieniker, der hygienebeauftragten Ärztin oder dem hygienebeauftragten Arzt und der Hygienekommission jährlich, bei Gefahr im Verzug unverzüglich, bekannt zu geben.

(3) Die Aufzeichnungen und Bewertungen nach § 11 sowie die resultierenden Maßnahmen sind in schriftlicher Form niederzulegen und den Gesundheitsämtern der Landkreise und kreisfreien Städte auf Verlangen vorzulegen.

### § 13  
Information des Personals

Die Leitung der Einrichtung nach § 1 Satz 1 hat das in der Einrichtung tätige Personal bei Beginn des Arbeitsverhältnisses und danach in regelmäßigen Abständen, mindestens jedoch einmal jährlich, über die in den Hygieneplänen nach § 23 Absatz 5 des Infektionsschutzgesetzes sowie nach § 4 festgelegten innerbetrieblichen Verfahrensweisen zur Infektionshygiene zu informieren.
Dies ist durch Unterschrift des Personals zu bestätigen.

### § 14  
Sektorübergreifender Informationsaustausch

Bei Verlegung, Überweisung oder Entlassung von Patientinnen und Patienten sind Informationen über Maßnahmen, die zur Verhütung und Bekämpfung von nosokomialen Infektionen und von Krankheitserregern mit Resistenzen erforderlich sind, an die aufnehmende oder weiterbehandelnde Einrichtung nach § 1 Satz 1 weiterzugeben.
Die Patientin oder der Patient ist vorab über eine beabsichtigte Übermittlung in Kenntnis zu setzen.
Nach ärztlicher Risikoeinschätzung sind im Einzelfall darüber hinaus die Krankentransportdienste sowie die stationären und ambulanten Pflegeeinrichtungen zu informieren.
Bei der Weitergabe der Information an Heimleitungen, Krankentransportdienste sowie stationäre und ambulante Pflegeeinrichtungen, einschließlich Hospize und palliativmedizinische Einrichtungen, ist die Einwilligung der Patientin oder des Patienten vor Weitergabe einzuholen.
Die geltenden datenschutzrechtlichen Bestimmungen sind zu beachten.

### § 15  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 73 Absatz 1 Nummer 24 des Infektionsschutzgesetzes handelt, wer vorsätzlich oder fahrlässig

1.  das nach den §§ 5 bis 8 erforderliche Fachpersonal nicht beschäftigt,
2.  entgegen § 11 keine schriftlichen Aufzeichnungen und keine Bewertung der erfassten Daten zu nosokomialen Infektionen, Antibiotikaresistenzen und Antibiotikaverbrauch vornimmt und keine Maßnahme zur Vermeidung ergreift,
3.  entgegen § 14 infektionsschutzrelevante Informationen nicht weitergibt, obwohl die Patientin oder der Patient auf die beabsichtigte Übermittlung hingewiesen wurde und hierzu Gegenteiliges nicht bestimmt oder in die Übermittlung eingewilligt hat.

(2) Zuständig für die Ahndung von Ordnungswidrigkeiten sind die Gesundheitsämter der Landkreise und kreisfreien Städte.
Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu fünfundzwanzigtausend Euro geahndet werden.

### § 16  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 6.
Februar 2012

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack