## Verordnung zur Übertragung der Planungshoheit nach dem Baugesetzbuch für das Gewerbegebiet „Prignitz/Falkenhagen“ (Planungshoheitsübertragungsverordnung - PlanÜV)

Auf Grund des § 203 Absatz 1 des Baugesetzbuches in der Fassung der Bekanntmachung vom 23. September 2004 (BGBl. I S. 2414), das zuletzt durch Artikel 6 des Gesetzes vom 20. Oktober 2015 (BGBl. I S. 1722, 1731) geändert worden ist, verordnet die Landesregierung im Einvernehmen mit der Stadt Pritzwalk und der amtsangehörigen Gemeinde Gerdshagen:

#### § 1 Aufgabenübertragung

(1) Die Aufgabe der kommunalen Planungshoheit für die verbindliche Bauleitplanung gemäß den §§ 8 bis 10 des Baugesetzbuches sowie den Regelungen der §§ 11 bis 18 des Baugesetzbuches für das Gewerbegebiet „Prignitz/Falkenhagen“ wird von der Stadt Pritzwalk und der amtsangehörigen Gemeinde Gerdshagen auf den Landkreis Prignitz übertragen.

(2) Die von der Übertragung der Planungshoheit nach Absatz 1 betroffenen Grundstücke umfassen eine Größe von rund 369 Hektar.
Sie umfassen die in der Anlage aufgeführten Fluren mit Flurstücken.

#### § 2 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 17.
Januar 2017

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider

* * *

### Anlagen

1

[Anlage (zu § 1 Absatz 2) - Planungshoheitsübertragungsverordnung – Übersicht der zugehörigen Grundstücke](/br2/sixcms/media.php/68/GVBl_II_04_2017-Anlage.pdf "Anlage (zu § 1 Absatz 2) - Planungshoheitsübertragungsverordnung – Übersicht der zugehörigen Grundstücke") 1.0 MB