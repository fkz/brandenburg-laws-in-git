## Verordnung zur Übertragung der Zuständigkeit für die Bestimmung der Verwaltungsbeamtinnen und Verwaltungsbeamten für die Ausschüsse zur Wahl der Schöffinnen und Schöffen

Auf Grund des § 40 Absatz 2 Satz 2 des Gerichtsverfassungsgesetzes in der Fassung der Bekanntmachung vom 9.
Mai 1975 (BGBl.
I S. 1077), der durch Artikel 1 Nummer 4 Buchstabe b Doppelbuchstabe bb des Gesetzes vom 21.
Dezember 2004 (BGBl.
S.
3599) angefügt worden ist, verordnet die Landesregierung:

#### § 1 

Zuständig für die Bestimmung der Verwaltungsbeamtinnen und Verwaltungsbeamten für den Ausschuss zur Wahl der Schöffinnen und Schöffen bei den Gerichten der ordentlichen Gerichtsbarkeit (§ 40 Absatz 1 des Gerichtsverfassungsgesetzes) ist das für Justiz zuständige Mitglied der Landesregierung.

#### § 2 

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 19.
November 2021

Die Landesregierung des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Die Ministerin der Justiz

Susanne Hoffmann