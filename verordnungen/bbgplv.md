## Verordnung über die Laufbahn des Polizeivollzugsdienstes des Landes Brandenburg (Brandenburgische Polizeilaufbahnverordnung - BbgPLV)

Auf Grund des § 109 Absatz 2 und des § 111 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl. I S. 26), von denen § 111 durch Gesetz vom 29.
Juni 2018 (GVBl.
I Nr. 17) geändert worden ist, verordnet der Minister des Innern und für Kommunales im Einvernehmen mit dem Minister der Finanzen:

**Inhaltsübersicht**

[§ 1 Geltungsbereich, Laufbahnordnungsbehörde](#1)

[§ 2 Laufbahnen, Ämter](#2)

[§ 3 Zugangsvoraussetzungen zur Laufbahn](#3)

[§ 4 Ausschreibung](#4)

[§ 5 Vorbereitungsdienst](#5)

[§ 6 Dauer des Vorbereitungsdienstes](#6)

[§ 7 Anderweitige Laufbahnbefähigung](#7)

[§ 8 Laufbahnbefähigung mit juristischem Staatsexamen](#8)

[§ 9 Andere Bewerberinnen und andere Bewerber](#9)

[§ 10 Probezeit](#10)

[§ 11 Probezeitverringerung](#11)

[§ 12 Dienstliche und eigene Fortbildung](#12)

[§ 13 Förderung der Leistungsfähigkeit](#13)

[§ 14 Körperliche Leistungsfähigkeit](#14)

[§ 15 Innerdienstliche Ausschreibungen](#15)

[§ 16 Erprobung auf höherbewerteten Dienstposten](#16)

[§ 17 Beförderungen](#17)

[§ 18 Aufstieg](#18)

[§ 19 Zulassung zur Aufstiegsausbildung](#19)

[§ 20 Aufstiegsausbildung](#20)

[§ 21 Zulassung zu einer höheren Laufbahn](#21)

[§ 22 Laufbahnrechtliche Dienstzeiten](#22)

[§ 23 Nachteilsausgleich](#23)

[§ 24 Menschen mit Behinderung](#24)

[§ 25 Übergangsregelungen](#25)

[§ 26 Inkrafttreten, Außerkrafttreten](#26)

#### § 1 Geltungsbereich, Laufbahnordnungsbehörde

(1) Diese Verordnung regelt das Laufbahnrecht für die Beamtinnen und Beamten des Polizeivollzugsdienstes des Landes Brandenburg.

(2) Laufbahnordnungsbehörde für die Laufbahn des Polizeivollzugsdienstes ist das für das Dienstrecht der Polizei zuständige Ministerium.

#### § 2 Laufbahnen, Ämter

(1) Zur Fachrichtung des Polizeivollzugsdienstes gehören die Laufbahnen des mittleren, gehobenen und höheren Dienstes.

(2) Beamtinnen und Beamte des Polizeivollzugsdienstes können im schutzpolizeilichen oder kriminalpolizeilichen Dienst eingesetzt werden.
Die zu führende Amtsbezeichnung wird von den jeweiligen Dienstvorgesetzten schriftlich festgestellt.

(3) Im Polizeivollzugsdienst sind den Laufbahnen (Ämtergruppen) folgende Ämter zugeordnet:

1.  Mittlerer Polizeivollzugsdienst:
    1.  Polizeiobermeisterin, Polizeiobermeister, Kriminalobermeisterin, Kriminalobermeister (Eingangsämter, Besoldungsgruppe A 8),
    2.  Polizeihauptmeisterin, Polizeihauptmeister, Kriminalhauptmeisterin, Kriminalhauptmeister (Besoldungsgruppe A 9),
2.  Gehobener Polizeivollzugsdienst:
    1.  Polizeikommissarin, Polizeikommissar, Kriminalkommissarin, Kriminalkommissar (Eingangsämter, Besoldungsgruppe A 9),
    2.  Polizeioberkommissarin, Polizeioberkommissar, Kriminaloberkommissarin, Kriminaloberkommissar (Besoldungsgruppe A 10),
    3.  Polizeihauptkommissarin, Polizeihauptkommissar, Kriminalhauptkommissarin, Kriminalhauptkommissar (Besoldungsgruppe A 11 oder A 12),
    4.  Erste Polizeihauptkommissarin, Erster Polizeihauptkommissar, Erste Kriminalhauptkommissarin, Erster Kriminalhauptkommissar (Besoldungsgruppe A 13),
3.  Höherer Polizeivollzugsdienst:
    1.  Polizeirätin, Polizeirat, Kriminalrätin, Kriminalrat (Eingangsämter, Besoldungsgruppe A 13),
    2.  Polizeioberrätin, Polizeioberrat, Kriminaloberrätin, Kriminaloberrat (Besoldungsgruppe A 14),
    3.  Polizeidirektorin, Polizeidirektor, Kriminaldirektorin, Kriminaldirektor (Besoldungsgruppe A 15),
    4.  Leitende Polizeidirektorin, Leitender Polizeidirektor, Leitende Kriminaldirektorin, Leitender Kriminaldirektor (jeweils Besoldungsgruppe A 16 oder B 2),
    5.  Direktorin beim Polizeipräsidium, Direktor beim Polizeipräsidium (Besoldungsgruppe A 16 oder B 2),
    6.  Landespolizeidirektorin, Landespolizeidirektor, Landeskriminaldirektorin, Landeskriminaldirektor (Besoldungsgruppe A 16 oder B 2),
    7.  Polizeivizepräsidentin oder Polizeivizepräsident (Besoldungsgruppe B 3).

#### § 3 Zugangsvoraussetzungen zur Laufbahn

(1) In den Vorbereitungsdienst für eine der Laufbahnen des Polizeivollzugsdienstes kann nur eingestellt werden, wer

1.  die gesetzlichen Voraussetzungen für die Berufung in des Beamtenverhältnis erfüllt,
2.  den besonderen gesundheitlichen Anforderungen für den Polizeivollzugsdienst genügt und keine Anhaltspunkte vorliegen, die langwierige Zeiträume krankheitsbedingter Dienstunfähigkeit überwiegend wahrscheinlich machen (Polizeidiensttauglichkeit),
3.  die ausreichende körperliche und geistige Leistungsfähigkeit für den Polizeivollzugsdienst besitzt,
4.  die besonderen Zugangsvoraussetzungen der betreffenden Laufbahn nach den Absätzen 2 bis 4 erfüllt,
5.  im Ergebnis eines Eignungsauswahlverfahrens für die Ämter der betreffenden Laufbahn des Polizeivollzugsdienstes geeignet ist und
6.  in den Vorbereitungsdienst für eine der Laufbahnen des Polizeivollzugsdienstes nicht bereits zuvor eingestellt war und die Laufbahnprüfung oder eine Zwischenprüfung im Land Brandenburg oder bei einem anderen Dienstherrn endgültig nicht bestanden hat oder wer den Vorbereitungsdienst nicht auf eigenen Antrag ohne schwerwiegenden persönlichen Grund beendet hat.

(2) In den Vorbereitungsdienst für den mittleren Polizeivollzugsdienst kann eingestellt werden, wer als Vorbildung mindestens

1.  die Fachoberschulreife, den Realschulabschluss oder einen als gleichwertig anerkannten Bildungsstand oder
2.  die Berufsbildungsreife, den erfolgreichen Besuch einer Hauptschule oder einen als gleichwertig anerkannten Bildungsstand sowie eine förderliche abgeschlossene Berufsausbildung oder die erweiterte Berufsbildungsreife beziehungsweise den erweiterten Hauptschulabschluss sowie eine mindestens dreijährige abgeschlossene Ausbildung oder eine abgeschlossene Ausbildung in einem öffentlich-rechtlichen Ausbildungsverhältnis nachweist.

(3) In den Vorbereitungsdienst für den gehobenen Polizeivollzugsdienst kann eingestellt werden, wer als Vorbildung eine Hochschulzugangsberechtigung nach dem Brandenburgischen Hochschulgesetz besitzt.

(4) In den Vorbereitungsdienst für den höheren Polizeivollzugsdienst kann eingestellt werden, wer

1.  ein für den höheren Polizeivollzugsdienst geeignetes Hochschulstudium mit einem Mastergrad oder ein anderes geeignetes, mehr als dreijähriges Studium an einer Universität oder einer gleichstehenden Hochschule mit einer Staats- oder Hochschulprüfung abgeschlossen hat und
2.  über förderliche Fremdsprachenkenntnisse verfügt, vorzugsweise der englischen Sprache.

#### § 4 Ausschreibung

(1) Bewerberinnen und Bewerber für die Einstellung in den Polizeivollzugsdienst sind durch öffentliche Ausschreibung zu ermitteln.
Dies gilt nicht für Dienstpostenbesetzungen mit Personen, die aufgrund von Rechtsvorschriften einen Anspruch auf Einstellung oder Wiederverwendung haben.

(2) Die oberste Dienstbehörde oder eine von ihr bestimmte Stelle regeln Art und Umfang der Ausschreibung und ihre Bekanntmachung.

#### § 5 Vorbereitungsdienst

(1) Bewerberinnen und Bewerber, die die Zugangsvoraussetzungen erfüllen, werden in der Reihenfolge ihrer Eignung und im Umfang der haushaltsgesetzlichen Einstellungsmöglichkeiten als Beamtinnen und Beamte auf Widerruf in den Vorbereitungsdienst der betreffenden Laufbahn eingestellt.
Sie führen während des Vorbereitungsdienstes entsprechend der Laufbahn, in der sie eingestellt werden sollen, die Dienstbezeichnung Polizeiobermeisteranwärterin oder Polizeiobermeisteranwärter, Polizeikommissaranwärterin oder Polizeikommissaranwärter, Polizeiratanwärterin oder Polizeiratanwärter.

(2) Im Vorbereitungsdienst erwerben die Beamtinnen und Beamten auf Widerruf die Kenntnisse und Fähigkeiten für die erfolgreiche Bewältigung laufbahnbezogener Aufgaben.
Die Befähigung für die Laufbahn erwerben sie durch Bestehen der Laufbahnprüfung.

(3) Die Laufbahnprüfung ist bestanden, wenn jede der vorgeschriebenen Prüfungen mit mindestens „ausreichend“ und vorgeschriebene Leistungsnachweise mit „bestanden“ bewertet wurden.
Näheres regeln die ausbildungs- und prüfungsrechtlichen Vorschriften für den mittleren und gehobenen Polizeivollzugsdienst.
Bei erfolgreichem Abschluss der Laufbahnprüfung für den gehobenen Polizeivollzugsdienst, die ausschließlich aus studienbegleitenden Modulprüfungen besteht, wird der akademische Bachelor-Grad verliehen.
Andernfalls kann die für die jeweilige Laufbahnprüfung bestellte Prüfungskommission die Befähigung für die Laufbahn des mittleren Polizeivollzugsdienstes anerkennen, wenn die nachgewiesenen Kenntnisse und Fähigkeiten hierfür ausreichen.
Mit Bestehen der Laufbahnprüfung für den höheren Polizeivollzugsdienst wird der akademische Mastergrad durch die Deutsche Hochschule der Polizei verliehen.

(4) Das Beamtenverhältnis von Beamtinnen und Beamten auf Widerruf, die die Laufbahnbefähigung erworben haben und die Zugangsvoraussetzungen für die entsprechende Laufbahn erfüllen, wird in ein Beamtenverhältnis auf Probe umgewandelt.

#### § 6 Dauer des Vorbereitungsdienstes

(1) Der Vorbereitungsdienst dauert

1.  für den mittleren Polizeivollzugsdienst zwei Jahre und sechs Monate,
2.  für den gehobenen Polizeivollzugsdienst drei Jahre und
3.  für den höheren Polizeivollzugsdienst zwei Jahre und sechs Monate.

(2) Der Vorbereitungsdienst endet mit Bestehen der Laufbahnprüfung, frühestens jedoch mit Ablauf der vorgeschriebenen Dauer.
Er endet ebenfalls mit endgültigem Nichtbestehen der Laufbahnprüfung oder einer vorgeschriebenen Zwischenprüfung.

(3) Der Vorbereitungsdienst kann nach näherer Bestimmung der ausbildungs- und prüfungsrechtlichen Vorschriften verlängert werden, insbesondere zur Wiederholung von Prüfungen oder bei Unterbrechung der Ausbildung.
Zum Zweck der Spitzensportförderung kann der Vorbereitungsdienst für den gehobenen Polizeivollzugsdienst auf fünf Jahre ausgedehnt werden.

(4) Der Vorbereitungsdienst kann verkürzt werden, soweit für die Laufbahnbefähigung erforderliche Kenntnisse oder Fähigkeiten bereits durch eine anderweitige Laufbahnausbildung oder eine soldatenrechtliche Ausbildung erworben wurden.

#### § 7 Anderweitige Laufbahnbefähigung

(1) In die Laufbahnen des mittleren oder gehobenen Polizeivollzugsdienstes kann bei Anerkennung der Laufbahnbefähigung übernommen oder im Beamtenverhältnis auf Probe eingestellt werden, wer

1.  außerhalb des polizeilichen Vorbereitungsdienstes oder bei einem anderen Dienstherrn außerhalb des Geltungsbereiches des Landesbeamtengesetzes eine gleichwertige Laufbahnbefähigung erworben oder
2.  eine berufsbefähigende Ausbildung abgeschlossen hat,
    1.  die einer polizeilichen Laufbahnausbildung gleichwertig ist oder
    2.  dadurch über Kenntnisse und Fähigkeiten verfügt, die für eine spezielle Verwendung im Polizeivollzugsdienst erforderlich sind.

Für eine Einstellung in die Laufbahn des gehobenen Polizeivollzugsdienstes ist mindestens ein mit einem Bachelorgrad oder einem gleichwertigen Abschluss absolviertes Hochschulstudium erforderlich.

(2) Soweit die laufbahnbefähigende oder die berufsbefähigende Ausbildung hinsichtlich der Dauer oder der Inhalte Unterschiede gegenüber der polizeilichen Laufbahnausbildung aufweist, die nicht bereits durch die vorhandene Berufserfahrung als ausgeglichen gelten, kann die Anerkennung vom Ableisten einer Unterweisung zur Einführung in die Laufbahnaufgaben abhängig gemacht werden.

(3) Über die Anerkennung der Befähigung entscheidet die Laufbahnordnungsbehörde.

#### § 8 Laufbahnbefähigung mit juristischem Staatsexamen

(1) In die Laufbahn des höheren Polizeivollzugsdienstes kann im Beamtenverhältnis auf Probe eingestellt werden, wer

1.  die gesetzlichen Voraussetzungen für die Berufung in das Beamtenverhältnis erfüllt,
2.  den besonderen gesundheitlichen Anforderungen für den Polizeivollzugsdienst genügt und keine Anhaltspunkte vorliegen, die langwierige Zeiträume krankheitsbedingter Dienstunfähigkeit überwiegend wahrscheinlich machen (Polizeidiensttauglichkeit),
3.  ausreichende körperliche und geistige Leistungsfähigkeit für den Polizeivollzugsdienst besitzt,
4.  über förderliche Fremdsprachenkenntnisse verfügt, vorzugsweise der englischen Sprache,
5.  die zweite juristische Staatsprüfung oder eine als gleichwertig anerkannte Prüfung bestanden und dadurch die Befähigung für diese Laufbahn erworben hat und
6.  im Ergebnis eines Eignungsauswahlverfahrens für die Verwendung in dieser Laufbahn geeignet ist.

(2) Zu Beginn der Probezeit erfolgt eine Unterweisung in die Aufgaben der Laufbahn.

#### § 9 Andere Bewerberinnen und andere Bewerber

(1) Andere Bewerberinnen und andere Bewerber müssen durch ihre Lebens- und Berufserfahrung innerhalb oder außerhalb des öffentlichen Dienstes befähigt sein, die Aufgaben in der Laufbahn des Polizeivollzugsdienstes wahrzunehmen.
Die für die Laufbahn vorgeschriebene Vorbildung, Ausbildung (Vorbereitungsdienst oder hauptberufliche Tätigkeit) und Laufbahnprüfung dürfen von ihnen nicht gefordert werden.

(2) Andere Bewerberinnen und andere Bewerber dürfen nur eingestellt werden, wenn

1.  eine einschlägige Berufserfahrung von mindestens fünf Jahren nachgewiesen ist, die nach Fachrichtung, Breite und Wertigkeit dem Aufgabenspektrum der Laufbahn des Polizeivollzugsdienstes entspricht,
2.  keine geeigneten Laufbahnbewerberinnen oder Laufbahnbewerber zur Verfügung stehen oder ein besonderes dienstliches Interesse an der Gewinnung der Bewerbenden als Beamtinnen oder Beamte besteht und
3.  die Laufbahnbefähigung auf Antrag der obersten Dienstbehörde vom Landespersonalausschuss festgestellt worden ist.

(3) Das Verfahren zur Feststellung der Befähigung regelt der Landespersonalausschuss.

#### § 10 Probezeit

(1) Im Beamtenverhältnis auf Probe sollen sich die Beamtinnen und Beamten nach Erwerb der Laufbahnbefähigung für die Laufbahn bewähren.
Die Probezeit dauert einheitlich drei Jahre und soll insbesondere zeigen, ob sie nach Eignung, Befähigung und fachlicher Leistung in der Lage sind, laufbahnbezogene Aufgaben zu erfüllen.
Sie soll zugleich erste Erkenntnisse vermitteln, für welche Verwendungen sie besonders geeignet erscheinen.

(2) Eignung, Befähigung und fachliche Leistung sind unter Anlegung eines strengen Maßstabes während der Probezeit wiederholt zu bewerten.
Eine erste Bewertung soll spätestens bis zum Ablauf der Hälfte der abzuleistenden Probezeit erfolgen.
Unmittelbar vor Ablauf der Probezeit wird abschließend festgestellt, ob sich die Beamtin oder der Beamte bewährt hat.

(3) Ergeben sich infolge einer Beurlaubung ohne Dienstbezüge mit Ausnahme der Fälle des § 11 Absatz 3 oder infolge von Krankheit Fehlzeiten von insgesamt mehr als 90 Kalendertagen, verlängern weitere Fehlzeiten den maßgeblichen Probezeitraum.
Bei Teilzeitbeschäftigung verlängert sich die Probezeit nur um den Zeitraum einer Beschäftigung mit weniger als der Hälfte der regelmäßigen Arbeitszeit.

(4) Lässt sich die Bewährung bis zum Ablauf der Probezeit nicht feststellen, kann die Probezeit um höchstens zwei Jahre verlängert werden.
Beamtinnen oder Beamte, die sich nicht bewähren, sind zu entlassen.
Sie können statt der Entlassung wegen mangelnder Bewährung mit ihrer Zustimmung in die nächstniedrigere Laufbahn übernommen werden, wenn sie hierfür geeignet sind und ein dienstliches Interesse vorliegt.
Die Entscheidung obliegt der Laufbahnordnungsbehörde.

#### § 11 Probezeitverringerung

(1) Zeiten hauptberuflicher Tätigkeiten, die nicht schon als Zeiten für die Feststellung der Berufserfahrung nach § 9 zugrunde gelegt worden sind, sollen auf die Probezeit angerechnet werden, wenn die Tätigkeit nach Art, Schwierigkeit und Bedeutung mindestens der Tätigkeit in einem Amt der betreffenden Laufbahn gleichwertig ist.
Die Entscheidung über die Anrechnung von Dienstzeiten außerhalb des öffentlichen Dienstes bedarf der Zustimmung der Laufbahnordnungsbehörde.

(2) Die Probezeit kann für Beamtinnen und Beamte, die die Laufbahnprüfung mit der Note „sehr gut“ bestanden haben, bis auf die Hälfte, und für diejenigen, die die Laufbahnprüfung mit der Note „gut“ bestanden haben, bis auf zwei Drittel der regelmäßigen Probezeit gekürzt werden, wenn nach Einschätzung von Dienstvorgesetzten die dienstlichen Leistungen die Anforderungen im Beamtenverhältnis auf Probe überwiegend erkennbar übersteigen.

(3) Die Zeit einer im öffentlichen oder dienstlichen Interesse liegenden Beurlaubung gilt als Probezeit, wenn derweil eine den Laufbahnanforderungen gleichwertige hauptberufliche Tätigkeit ausgeübt wird.
Das Vorliegen der Voraussetzungen nach Satz 1 ist bei Gewährung der Beurlaubung von den Dienstvorgesetzten schriftlich festzustellen.
Der Zeit einer Beurlaubung nach Satz 1 steht die Zeit einer von der obersten Dienstbehörde oder der von ihr bestimmten Stelle angeordneten Tätigkeit bei einer öffentlichen zwischenstaatlichen oder überstaatlichen Einrichtung und den kommunalen Spitzenverbänden gleich.

(4) Bei Einstellung früherer Beamtinnen und Beamter auf Lebenszeit und Übernahme von Polizeivollzugsbeamtinnen und -beamten anderer Dienstherren in der Laufbahn des Polizeivollzugsdienstes kann eine damalige Probezeit angerechnet werden, wenn seit dem Ende des früheren Beamtenverhältnisses nicht mehr als neun Jahre vergangen sind.
War bereits ein Beförderungsamt verliehen, so brauchen die darunter liegenden Ämter nicht regelmäßig durchlaufen zu werden.

(5) Die Probezeit hat auch bei einer Verringerung nach den Absätzen 1 bis 4 in jedem Fall mindestens ein Jahr in originär laufbahnbezogenen Aufgaben zu erfolgen.
Verringerte Probezeiten können gemäß § 10 nachträglich verlängert werden.
Satz 1 gilt nicht für die Übernahme von Polizeivollzugsbeamtinnen und -beamten im Beamtenverhältnis auf Lebenszeit anderer Dienstherren.

#### § 12 Dienstliche und eigene Fortbildung

(1) Die Entwicklung neuer Arbeitsmethoden, der Einsatz technikunterstützter Informationsverarbeitung sowie der Wandel und die notwendige und vorausschauende Anpassung der Aufgaben und der Leistungsfähigkeit des öffentlichen Dienstes an sich verändernde gesellschaftliche, technologische und wirtschaftliche Bedingungen erfordern eine ständige Fortbildung.
Die dienstliche Fortbildung ist deshalb besonders zu fördern.

(2) Die Beamtinnen und Beamten sind insbesondere verpflichtet, an Maßnahmen der dienstlichen Fortbildung teilzunehmen, die

1.  der Erhaltung und Verbesserung der Befähigung für ihren Dienstposten oder für gleich bewertete Dienstposten dienen,
2.  bei Änderungen der Laufbahnausbildung eine Angleichung an den neuen Befähigungsstand zum Ziel haben.

(3) Die Beamtinnen und Beamten sind außerdem verpflichtet, sich selbst fortzubilden, damit sie über die Änderungen der Aufgaben und der Anforderungen in der Laufbahn unterrichtet und steigenden Anforderungen gewachsen sind.

(4) Nach den Erfordernissen der Personalplanung und des Personaleinsatzes sind Fortbildungsangebote vorzusehen, die zum Ziel haben, die Befähigung für höher bewertete Tätigkeiten zu vermitteln.

(5) Beamtinnen und Beamte, die durch Fortbildung ihre Fähigkeiten und fachlichen Kenntnisse nachweislich wesentlich gesteigert haben, sollen gefördert werden.
Insbesondere soll ihnen nach Möglichkeit Gelegenheit gegeben werden, ihre Fähigkeiten und fachlichen Kenntnisse auf höher bewerteten Dienstposten anzuwenden und hierbei ihre besondere Eignung nachzuweisen.

#### § 13 Förderung der Leistungsfähigkeit

Eignung, Befähigung und fachliche Leistung sollen durch geeignete Personalentwicklungsmaßnahmen erhalten und gefördert werden.
Neben den in den §§ 19 und 23 des Landesbeamtengesetzes genannten Maßnahmen gehören dazu insbesondere

1.  jährliche Mitarbeitergespräche,
2.  Personalentwicklungsvereinbarungen,
3.  Einschätzungen der Vorgesetzten durch die Mitarbeiterinnen und Mitarbeiter,
4.  ein die Fähigkeiten und Kenntnisse erweiternder Wechsel der Verwendung, wie auch die Tätigkeit bei internationalen Organisationen sowie
5.  die Führungskräftefortbildung und -entwicklung.

#### § 14 Körperliche Leistungsfähigkeit

Beamtinnen und Beamte sind verpflichtet, ihre körperliche Leistungsfähigkeit zu erhalten und nach Möglichkeit zu steigern.
Es soll jährlich überprüft werden, ob sie den physisch-sportlichen Anforderungen für den Polizeivollzugsdienst in dieser Hinsicht genügen.

#### § 15 Innerdienstliche Ausschreibungen

(1) Dienstposten, die mit Beamtinnen oder Beamten des Polizeivollzugsdienstes besetzt werden sollen, werden innerhalb des Geschäftsbereiches des für Inneres zuständigen Ministeriums ausgeschrieben.
Dienstposten, die bis zur Besoldungsgruppe A 11 bewertet sind, können durch Ausschreibung innerhalb der betreffenden Dienststelle besetzt werden, wenn die Besetzung auf der Grundlage einer Auswahlentscheidung erfolgt.

(2) Einer Ausschreibung nach Absatz 1 bedarf es nicht, wenn der Dienstposten durch Versetzung oder Umsetzung einer Beamtin oder eines Beamten des Polizeivollzugsdienstes mit gleichwertigem Statusamt besetzt werden kann.
Gleiches gilt, wenn sich die Beamtin oder der Beamte

1.  zum Zeitpunkt der Dienstpostenbesetzung auf einem gleichwertigen, durch eine Auswahlentscheidung besetzten Dienstposten bewährt hat, ohne das entsprechende Statusamt erreicht zu haben oder
2.  mit Erreichen einer durch die oberste Dienstbehörde vorgeschriebenen Altersgrenze oder gemäß Feststellung aufgrund eines polizeiärztlichen oder eines anderen ärztlichen Gutachtens den besonderen gesundheitlichen Anforderungen an die bisherige Funktion nicht mehr uneingeschränkt genügt.

#### § 16 Erprobung auf höherbewerteten Dienstposten

(1) Die Erprobungszeit auf einem höherbewerteten Dienstposten zum Nachweis der Eignung für ein höheres Amt beträgt ein Jahr.
Auf diese Erprobungszeit werden Zeiten einer vorausgegangenen erfolgreichen Bewährung auf einem Dienstposten mit mindestens gleicher Bewertung oder gleicher Art angerechnet.
Während der Erprobungszeit ist die Verleihung des Beförderungsamtes ausgeschlossen, für das die Erprobung durchgeführt wird.

(2) Die Erprobung kann, wenn die sonstigen Voraussetzungen nach dieser Verordnung erfüllt sind, im Rahmen der Probezeit stattfinden.

(3) Ergeben sich infolge einer Beurlaubung ohne Dienstbezüge mit Ausnahme der Fälle des § 11 Absatz 3 oder infolge von Krankheit Fehlzeiten von insgesamt mehr als 90 Kalendertagen, verlängern weitere Fehlzeiten den maßgeblichen Erprobungszeitraum.
Bei Teilzeitbeschäftigung verlängert sich die Erprobungszeit nur um den Zeitraum einer Beschäftigung mit weniger als der Hälfte der regelmäßigen Arbeitszeit.

(4) Kann die Eignung für den Dienstposten nicht festgestellt werden, ist dessen Übertragung zu widerrufen oder die Erprobungszeit zunächst um höchstens ein weiteres Jahr zu verlängern.
Die Feststellung der Eignung oder die Entscheidung nach Satz 1 haben Dienstvorgesetzte schriftlich zu treffen.

#### § 17 Beförderungen

(1) Befördert werden darf nur, wer gemäß dienstlicher Leistung, Persönlichkeit und Erfüllung der allgemeinen Beamtenpflichten den Anforderungen des höheren Amtes entspricht und die Eignung für dieses Amt in einer Erprobungszeit nachgewiesen hat.
Bei der Eignung ist neben der innerhalb auch die außerhalb des öffentlichen Dienstes erworbene Lebens- und Berufserfahrung zu berücksichtigen; die Form des Erwerbs der Laufbahnbefähigung ist dabei unbeachtlich.
Die längere Dauer zurückgelegter Dienstzeiten allein rechtfertigt eine Beförderung nicht.

(2) Die Ämter der Laufbahnen des Polizeivollzugsdienstes sind regelmäßig zu durchlaufen.
Dies gilt nicht

1.  für die Ämter der Besoldungsgruppe B,
2.  bei einem Aufstieg in die nächsthöhere Laufbahn für die noch nicht erreichten Ämter der bisherigen Laufbahn.

#### § 18 Aufstieg

(1) Beamtinnen und Beamte auf Lebenszeit können die Befähigung für die nächsthöhere Laufbahn durch Ableisten der vorgeschriebenen Aufstiegsausbildung und Bestehen der Aufstiegsprüfung erwerben.
Soweit es mit den Zielen des Aufstiegs vereinbar ist, soll auch Teilzeitkräften der Aufstieg ermöglicht werden.

(2) Zur Aufstiegsausbildung kann nur zugelassen werden, wer im Ergebnis eines Eignungsauswahlverfahrens für die Ämter der nächsthöheren Laufbahn persönlich und fachlich geeignet erscheint.

(3) Die erforderliche persönliche Eignung besitzt, wer den besonderen gesundheitlichen Anforderungen für den Polizeivollzugsdienst genügt.
Die erforderliche fachliche Eignung besitzt, wer nach der für die Zulassung maßgeblichen Beurteilung mindestens überwiegend die Anforderungen erkennbar übersteigende Leistungen zeigt und sich auf mindestens zwei unterschiedlichen Dienstposten bewährt hat.
§ 16 Absatz 1 Satz 1 und Absatz 2 bis 4 gilt sinngemäß.

(4) Zur Ausbildung für den Aufstieg in den höheren Polizeivollzugsdienst kann außerdem nur zugelassen werden, wer

1.  ein mit einem Bachelorgrad oder einem gleichwertigen Abschluss absolviertes wissenschaftliches Hochschulstudium abgeschlossen hat,
2.  mindestens das erste Beförderungsamt erreicht hat und
3.  über förderliche Fremdsprachenkenntnisse verfügt, vorzugsweise der englischen Sprache.

(5) Ausbildungsangebote für den Aufstieg in den gehobenen Polizeivollzugsdienst werden durch die für die Ernennung zuständigen Dienststellen im Rahmen des Personalbedarfs und der haushaltsrechtlichen Vorgaben ausgeschrieben.
Ausbildungsangebote für den Aufstieg in den höheren Polizeivollzugsdienst werden durch die oberste Dienstbehörde ausgeschrieben.
Diese regelt darüber hinaus jeweils die Einzelheiten der Aufstiegsverfahren und bestimmt einen jährlichen Stichtag für die Zulassung zur Aufstiegsausbildung.

#### § 19 Zulassung zur Aufstiegsausbildung

(1) Der Entscheidung über eine Zulassung zur Aufstiegsausbildung geht ein Eignungsauswahlverfahren voraus, in dem Eignung und Befähigung der Bewerbenden unter Berücksichtigung der künftigen Laufbahnaufgaben und der Anforderungen der vorgesehenen Aufstiegsausbildung festzustellen sind.
Bei der Eignungsfeststellung soll auch der Erfolg einer bisherigen Verwendung auf unterschiedlichen Dienstposten berücksichtigt werden.

(2) Die Eignung der Bewerbenden, die grundsätzlich für einen Aufstieg in Betracht kommen, ist mindestens in einer Vorstellung vor einer Auswahlkommission festzustellen.
Die ausschreibende Dienststelle kann auf der Grundlage der dienstlichen Beurteilungen unter Berücksichtigung des jeweiligen Statusamtes eine Vorauswahl treffen.

(3) Über die Zulassung zur Aufstiegsausbildung entscheidet die ausschreibende Dienststelle aufgrund des Vorschlags der Auswahlkommission.
Die Entscheidung kann auch Bewerbende früherer, höchstens zwei Jahre zurückliegender Auswahlverfahren berücksichtigen, wenn deren Eignungsfeststellung vergleichbar gestaltet war.

(4) Eine wiederholte Teilnahme am Auswahlverfahren ist einmalig frühestens nach drei Jahren möglich.
Über Ausnahmen entscheidet die ausschreibende Dienststelle.

(5) Die Zulassung für den Aufstieg kann für Bewerbende widerrufen werden, die sich in der Aufstiegsausbildung als ungeeignet erweisen.

#### § 20 Aufstiegsausbildung

(1) Nach Zulassung zur Aufstiegsausbildung nehmen Beamtinnen und Beamte des mittleren Polizeivollzugsdienstes als Kommissarbewerberinnen oder Kommissarbewerber an einer Aufstiegsausbildung und der Aufstiegsprüfung nach Maßgabe der Polizeiaufstiegsverordnung teil.

(2) Beamtinnen und Beamte des gehobenen Polizeivollzugsdienstes absolvieren nach ihrer Zulassung zur Aufstiegsausbildung als Ratsbewerberinnen oder Ratsbewerber ein mindestens zweijähriges Hochschulstudium.
Aufstiegsprüfung für den höheren Polizeivollzugsdienst ist ausschließlich die an der Deutschen Hochschule der Polizei im Rahmen des akkreditierten Masterstudiengangs nach den dafür geltenden Vorschriften abgelegte Prüfung.

(3) Die Beamtinnen und Beamten verbleiben bis zur Verleihung eines Amtes der neuen Laufbahn in ihrer bisherigen Rechtsstellung.
Nach Bestehen der Aufstiegsprüfung mit der Note „ausreichend“ kann die Amtsverleihung von einer sechsmonatigen Bewährung in laufbahnbezogenen Aufgaben abhängig gemacht werden.
§ 16 Absatz 3 und 4 gilt sinngemäß.

#### § 21 Zulassung zu einer höheren Laufbahn

(1) Wer die für eine höhere Laufbahn erforderliche Vorbildung besitzt, kann nach erfolgreicher Teilnahme an einem Auswahlverfahren zur höheren Laufbahn zugelassen werden.

(2) Wer zur höheren Laufbahn zugelassen wurde, verbleibt im bisherigen Amt, bis

1.  im gehobenen Polizeivollzugsdienst die in § 10 Absatz 3 Nummer 2 des Landesbeamtengesetzes geforderten sonstigen Voraussetzungen erfüllt sind,
2.  im höheren Polizeivollzugsdienst die in § 10 Absatz 4 Nummer 2 des Landesbeamtengesetzes geforderten sonstigen Voraussetzungen erfüllt sind oder die zweite juristische Staatsprüfung bestanden wurde und
3.  nach einer Erprobungszeit von sechs Monaten die Bewährung in der neuen Laufbahn festgestellt wurde.

§ 16 Absatz 3 und 4 gilt sinngemäß.

#### § 22 Laufbahnrechtliche Dienstzeiten

(1) Laufbahnrechtliche Dienstzeiten rechnen vom Zeitpunkt der Ernennung in das Beamtenverhältnis auf Lebenszeit an.
Erfolgte vor dem 9.
April 2009 die Verleihung eines Amtes vor der Ernennung in das Beamtenverhältnis auf Lebenszeit, rechnen die Dienstzeiten vom Zeitpunkt der Verleihung des Amtes an.
Dienstzeiten, die über die im Einzelfall festgelegte Probezeit hinaus geleistet worden sind, sind anzurechnen.
Zeiten einer Beurlaubung ohne Dienstbezüge sind keine laufbahnrechtlichen Dienstzeiten.

(2) Abweichend von Absatz 1 Satz 1 gelten Zeiten des Grundwehrdienstes und von Wehrübungen sowie die Zeit des Zivildienstes als laufbahnrechtliche Dienstzeiten.

(3) Abweichend von Absatz 1 Satz 4 gelten als laufbahnrechtliche Dienstzeiten:

1.  die Zeit eines Urlaubs, wenn der Urlaub für eine Tätigkeit als wissenschaftliche Assistentin oder wissenschaftlicher Assistent oder Geschäftsführerin oder Geschäftsführer bei Fraktionen des Europäischen Parlaments, des Deutschen Bundestages oder eines Landtages sowie bei kommunalen Spitzenverbänden erteilt oder unter vollständiger oder teilweiser Fortgewährung der Dienstbezüge erteilt wurde, in den übrigen Fällen einer im öffentlichen oder dienstlichen Interesse liegenden Beurlaubung nur bis zu einer Dauer von insgesamt zwei Jahren,
2.  die Zeit eines Urlaubs nach der Mutterschutz- und Elternzeitverordnung vom 12. Februar 2009 (BGBl.
    I S.
    320), die zuletzt durch Artikel 1 der Verordnung vom 9.
    Februar 2018 (BGBl. I S. 198) geändert worden ist, in der jeweils geltenden Fassung oder einer Beurlaubung nach § 80 Absatz 1 Satz 1 Nummer 2 des Landesbeamtengesetzes; dabei wird jeweils der Zeitraum der tatsächlichen Verzögerung bis zu einem Jahr zugrunde gelegt, insgesamt höchstens bis zu drei Jahren.

In den Fällen des Satzes 1 Nummer 1 ist § 11 Absatz 1 Satz 2 entsprechend anzuwenden.

(4) Zeiten, die nach dem Bestehen einer Laufbahnprüfung in einem Beschäftigungsverhältnis im öffentlichen Dienst zurückgelegt worden sind, sollen auf die Dienstzeit angerechnet werden, wenn

1.  die Übernahme in ein Beamtenverhältnis auf Probe aus nicht von der Beamtin oder dem Beamten zu vertretenden Gründen unterblieben ist,
2.  die Tätigkeit nach Art und Bedeutung mindestens der Tätigkeit in einem Amt der betreffenden Laufbahn entsprochen hat und
3.  sie nicht schon auf die Probezeit angerechnet worden sind.

(5) Zeiten in einem hauptberuflichen kommunalen Wahlbeamtenverhältnis, die nach Erwerb einer Laufbahnbefähigung geleistet worden sind, können auf die laufbahnrechtliche Dienstzeit angerechnet werden.

#### § 23 Nachteilsausgleich

(1) Der Ausgleich einer Verzögerung des beruflichen Werdegangs durch eine Beförderung während der Probezeit oder vor Ablauf eines Jahres seit Beendigung der Probezeit nach § 24 Absatz 2 des Landesbeamtengesetzes setzt voraus, dass die Beamtin oder der Beamte sich innerhalb von sechs Monaten oder im Falle fester Einstellungstermine zum nächsten Einstellungstermin nach Beendigung der Betreuung oder Pflege oder Abschluss der im Anschluss an die Betreuung oder Pflege begonnenen oder fortgesetzten vorgeschriebenen Ausbildung beworben hat und diese Bewerbung zur Einstellung geführt hat.
Als Ausgleich können je Kind die tatsächliche Verzögerung bis zu einem Zeitraum von einem Jahr, bei mehreren Kindern höchstens drei Jahre angerechnet werden.
Werden in einem Haushalt mehrere Kinder gleichzeitig betreut, wird für denselben Zeitraum der Ausgleich nur einmal gewährt.
Bei einer gleichzeitigen Kinderbetreuung durch mehrere Personen erhält nur eine Person den Ausgleich.
Für die Pflege pflegebedürftiger naher Angehöriger kann die tatsächliche Verzögerung bis zu einem Zeitraum von einem Jahr angerechnet werden.

(2) Für den Ausgleich einer Verzögerung des beruflichen Werdegangs durch Zeiten des Wehr- oder Zivildienstes sowie gleichgestellte Zeiten, soweit

1.  das Arbeitsplatzschutzgesetz in der Fassung der Bekanntmachung vom 16.
    Juli 2009 (BGBl.
    I S. 2055), das zuletzt durch Artikel 3 Absatz 1 des Gesetzes vom 29.
    Juni 2015 (BGBl.
    I S. 1061, 1071) geändert worden ist, in der jeweils geltenden Fassung,
2.  das Zivildienstgesetz in der Fassung der Bekanntmachung vom 17.
    Mai 2005 (BGBl.
    I S. 1346), das zuletzt durch Artikel 3 Absatz 5 des Gesetzes vom 29.
    Juni 2015 (BGBl.
    I S. 1061, 1071) geändert worden ist, in der jeweils geltenden Fassung,
3.  das Entwicklungshelfer-Gesetz vom 18.
    Juni 1969 (BGBl.
    I S.
    549), das zuletzt durch Artikel 6 Absatz 13 des Gesetzes vom 23.
    Mai 2017 (BGBl.
    I S.
    1228, 1242) geändert worden ist, in der jeweils geltenden Fassung oder
4.  das Soldatenversorgungsgesetz in der Fassung der Bekanntmachung vom 16. September 2009 (BGBl.
    I S.
    3054), das zuletzt durch Artikel 90 des Gesetzes vom 29. März 2017 (BGBl.
    I S.
    626, 641) geändert worden ist, in der jeweils geltenden Fassung

die Vornahme eines Ausgleichs beruflicher Verzögerungen, die durch die im jeweiligen Dienstverhältnis verbrachten Zeiten eintreten würden, anordnen, gilt Absatz 1 Satz 1 entsprechend.

#### § 24 Menschen mit Behinderung

(1) Von Menschen mit Behinderung darf bei der Übertragung höherbewerteter Dienstposten und einer Beförderung nur die hierfür notwendige körperliche Eignung verlangt werden.
Die fachlichen Anforderungen dürfen nicht geringer bemessen werden.

(2) Bei der Beurteilung der Leistung von Menschen mit Behinderung ist eine etwaige Minderung der Arbeits- und Verwendungsfähigkeit aufgrund der Behinderung zu berücksichtigen.

#### § 25 Übergangsregelungen

(1) Ausschreibungen nach § 12 der Laufbahnverordnung der Polizei vom 30.
Januar 2006, die zum Zeitpunkt des Inkrafttretens dieser Verordnung bereits bekannt gegeben wurden, bleiben von der Neuregelung in § 15 unberührt.

(2) Festgelegte und bereits laufende Erprobungszeiten nach § 10 der Laufbahnverordnung der Polizei vom 30.
Januar 2006 bleiben von der Neuregelung in § 16 unberührt.

#### § 26 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tage nach der Verkündung in Kraft.
Gleichzeitig tritt die Laufbahnverordnung der Polizei vom 30.
Januar 2006 (GVBl.
II S.
18), die zuletzt durch Artikel 1 der Verordnung vom 20. März 2015 (GVBl.
II Nr.
16) geändert worden ist, außer Kraft.

Potsdam, den 28.
August 2018

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter