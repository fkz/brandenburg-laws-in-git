## Verordnung zur Verteilung der Mittel aus der Weitergabe von Einsparungen an Wohngeldleistungen gemäß § 24a des Brandenburgischen Finanzausgleichsgesetzes (Wohngeldeinsparungsverteilverordnung - WoEinsparVertV)

Auf Grund des § 24a Satz 3 des Brandenburgischen Finanzausgleichsgesetzes vom 29. Juni 2004 (GVBl. I S. 262), der durch Artikel 1 des Gesetzes vom 18.
Dezember 2020 (GVBl.
I Nr. 36) eingefügt worden ist, verordnet die Ministerin der Finanzen und für Europa im Einvernehmen mit dem Minister für Wirtschaft, Arbeit und Energie:

#### § 1 Verteilung

(1) Die Mittel nach § 24a Satz 1 und 2 des Brandenburgischen Finanzausgleichsgesetzes werden jeweils hälftig nach den Kosten der Unterkunft und Heizung gemäß § 22 des Zweiten Buches Sozialgesetzbuch und nach der Anzahl der Bedarfsgemeinschaften nach dem Zweiten Buch Sozialgesetzbuch auf die Landkreise und kreisfreien Städte aufgeteilt.

(2) Als Bemessungsgrundlage für die Anzahl der Bedarfsgemeinschaften gelten die von der Bundesagentur für Arbeit nach § 53 des Zweiten Buches Sozialgesetzbuch zum Zeitpunkt der Festsetzung veröffentlichten Statistiken.
Dabei wird für das Ausgleichsjahr das arithmetische Mittel aus den Monatswerten des jeweiligen Ausgleichsjahres gebildet.
Bei den Kosten der Unterkunft und Heizung wird auf die Daten vom 1.
Januar bis zum 31.
Dezember nach den Ergebnissen der Vierteljahresstatistik der Gemeindefinanzen für das jeweilige Jahr abgestellt.

#### § 2 Festsetzung und Auszahlung

(1) Das für Finanzen zuständige Ministerium setzt die Zuweisungen nach § 1 für die kommunalen Aufgabenträger unverzüglich nach Vorliegen der für die Bemessung nach § 1 Absatz 2 erforderlichen Daten fest.

(2) Auf die Zuweisungen nach § 1 Absatz 1 erhalten die kommunalen Aufgabenträger bis zum 15. Kalendertag des zweiten Monats eines Quartals Abschlagszahlungen.
Die geleisteten Abschlagszahlungen werden mit der endgültigen Festsetzung verrechnet.
Zuviel erhaltene Abschläge werden zurückgefordert oder mit entsprechenden Zahlungen nachfolgender Zeiträume verrechnet.

#### § 3 Inkrafttreten

Diese Verordnung tritt mit Wirkung vom 20.
Dezember 2020 in Kraft.

Potsdam, den 15.
März 2021

Die Ministerin der Finanzen und für Europa

Katrin Lange