## Verordnung über den elektronischen Rechtsverkehr im Land Brandenburg (Brandenburgische Elektronischer-Rechtsverkehr-Verordnung - BbgERVV)

Auf Grund

*   des § 8a Absatz 2 Satz 1 und 2 des Handelsgesetzbuches in der im Bundesgesetzblatt Teil III, Gliederungsnummer 4100-1, veröffentlichten bereinigten Fassung, der zuletzt durch Artikel 190 Nummer 1 der Verordnung vom 31.
    August 2015 (BGBl.
    I S. 1474, 1502) geändert worden ist, in Verbindung mit § 1 Nummer 19 der Justiz-Zuständigkeitsübertragungsverordnung vom 9. April 2014 (GVBl.
    II Nr. 23),
*   des § 5 Absatz 2 erster Halbsatz des Partnerschaftsgesellschaftsgesetzes vom 25.
    Juli 1994 (BGBl.
    I S.
    1744), der zuletzt durch Artikel 22 des Gesetzes vom 23.
    Oktober 2008 (BGBl. I S. 2026, 2043) geändert worden ist, in Verbindung mit § 8a Absatz 2 Satz 1 und 2 des Handelsgesetzbuches und in Verbindung mit § 1 Nummer 34 der Justiz-Zuständigkeitsübertragungsverordnung,
*   des § 156 Absatz 1 Satz 1 des Genossenschaftsgesetzes in der Fassung der Bekanntmachung vom 16.
    Oktober 2006 (BGBl.
    I S.
    2230), der zuletzt durch Artikel 8 Nummer 3 Buchstabe b des Gesetzes vom 17.
    Juli 2017 (BGBl.
    I S.
    2541, 2543) geändert worden ist, in Verbindung mit § 8a Absatz 2 Satz 1 und 2 des Handelsgesetzbuches und in Verbindung mit § 1 Nummer 12 der Justiz-Zuständigkeitsübertragungsverordnung,
*   des § 20a Absatz 1 des Verfassungsgerichtsgesetzes Brandenburg in der Fassung der Bekanntmachung vom 22.
    November 1996 (GVBl.
    I S.
    343), der durch Artikel 1 des Gesetzes vom 18.
    Juni 2018 (GVBl.
    I Nr.
    13) eingefügt worden ist,

verordnet die Ministerin der Justiz:

#### § 1 Zulassung der elektronischen Kommunikation

Bei den in der Anlage bezeichneten Gerichten ist in den dort jeweils für sie näher bezeichneten Verfahrensarten und ab dem dort für sie angegebenen Datum die Einreichung elektronischer Dokumente eröffnet.

#### § 2 Form der Einreichung

(1) Zur Entgegennahme elektronischer Dokumente ist ausschließlich die elektronische Poststelle der Gerichte im Land Brandenburg bestimmt.
Die elektronische Poststelle ist über die auf der Internetseite [www.erv.brandenburg.de](http://www.erv.brandenburg.de "Bekanntgabe des Einreichungsverfahrens über den elektronischen Rechtsverkehr") bezeichneten Kommunikationswege erreichbar.

(2) Die Einreichung erfolgt durch die Übertragung des elektronischen Dokuments in die elektronische Poststelle.

(3) Sofern für Einreichungen die Schriftform oder die elektronische Form vorgeschrieben ist, sind, soweit kein Fall des § 12 Absatz 2 Satz 2 erster Halbsatz des Handelsgesetzbuches vorliegt, die elektronischen Dokumente mit einer qualifizierten elektronischen Signatur zu versehen.
Die qualifizierte elektronische Signatur und das ihr zugrunde liegende Zertifikat müssen durch die adressierte oder durch eine andere von der Landesjustizverwaltung mit der automatisierten Überprüfung beauftragte Stelle prüfbar sein.
Die Eignungsvoraussetzungen für eine Prüfung werden gemäß § 3 Nummer 2 bekannt gegeben.

(4) Das elektronische Dokument muss eines der folgenden Formate in einer für das adressierte Gericht bearbeitbaren Version aufweisen:

1.  Adobe PDF (Portable Document Format),
2.  XML (Extensible Markup Language),
3.  TIFF (Tag Image File Format),
4.  Microsoft Word, soweit keine aktiven Komponenten, beispielsweise Makros, verwendet werden.

Nähere Informationen, insbesondere zu den bearbeitbaren Versionen der zulässigen Dateiformate, werden gemäß § 3 Nummer 3 bekannt gegeben.

(5) Elektronische Dokumente, die einem der in Absatz 4 genannten Dateiformate in der nach § 3 Nummer 3 bekannt gegebenen Version entsprechen, können auch in komprimierter Form als ZIP-Datei eingereicht werden.
Die ZIP-Datei darf keine anderen ZIP-Dateien und keine Verzeichnisstrukturen enthalten.
Beim Einsatz von Dokumentensignaturen muss sich die Signatur auf das Dokument und nicht auf die ZIP-Datei beziehen.
Die ZIP-Datei darf zusätzlich signiert werden.

(6) Sofern strukturierte Daten übermittelt werden, sollen sie im UNICODE-Zeichensatz UTF 8 codiert sein.

#### § 3 Bekanntgabe der Bearbeitungsvoraussetzungen

Im Auftrag der Landesjustizverwaltung gibt der Betreiber der elektronischen Poststelle der Gerichte nach § 2 Absatz 1 Satz 1 auf der Internetseite [www.erv.brandenburg.de](http://www.erv.brandenburg.de "Bekanntgabe des Einreichungsverfahrens über den elektronischen Rechtsverkehr") bekannt:

1.  die Einzelheiten des Verfahrens, das bei einer vorherigen Anmeldung zur Teilnahme am elektronischen Rechtsverkehr sowie für die Authentifizierung bei der jeweiligen Nutzung der elektronischen Poststelle einzuhalten ist, einschließlich der für die datenschutzgerechte Administration elektronischer Postfächer zu speichernden Arten personenbezogener Daten,
2.  die Zertifikate, Anbieter und Versionen elektronischer Signaturen, die nach seiner Prüfung für die Bearbeitung durch die Justiz oder durch eine andere mit der automatisierten Prüfung beauftragte Stelle geeignet sind; dabei ist mindestens die Prüfbarkeit qualifizierter elektronischer Signaturen sicherzustellen, die dem Profil ISIS-MTT entsprechen,
3.  die nach seiner Prüfung den in § 2 Absatz 3 und 4 festgelegten Formatstandards entsprechenden und für die Bearbeitung durch angeschlossene Gerichte geeigneten Versionen der genannten Formate sowie die bei dem in § 2 Absatz 4 Satz 1 Nummer 5 bezeichneten XML-Format zugrunde zu legenden Definitions- oder Schemadateien,
4.  die zusätzlichen Angaben, die bei der Übermittlung oder bei der Bezeichnung des einzureichenden elektronischen Dokuments gemacht werden sollen, um die Zuordnung innerhalb des adressierten Gerichts oder der Staatsanwaltschaft und die Weiterverarbeitung durch sie zu gewährleisten,
5.  Angaben zu geeigneten Datenträgern im Fall des § 4 Absatz 1 sowie
6.  Angaben zu Dokumentenzahl und Volumengrenzen.

#### § 4 Ersatzeinreichung

(1) Ist eine Übermittlung an die elektronische Poststelle gemäß § 2 nicht möglich, so kann die Einreichung abweichend von § 2 Absatz 1 und 2 auf einem Datenträger nach § 3 Nummer 5 bei dem Gericht erfolgen.
Die Unmöglichkeit der Übermittlung nach § 2 ist darzulegen.

(2) Soweit Einreichungen die nach § 3 Nummer 6 bekannt zu gebende Dokumentenzahl oder Volumengrenzen überschreiten, können diese gemäß der Einreichung nach Absatz 1 Satz 1 übermittelt werden.

(3) Die nach § 3 bekanntgegebenen Bearbeitungsvoraussetzungen sind auch in den Fällen der Absätze 1 und 2 einzuhalten, soweit sie nicht den elektronischen Übermittlungsvorgang betreffen.

(4) Ist die Entgegennahme elektronischer Dokumente über die elektronische Poststelle gemäß § 2 und gemäß Absatz 1 Satz 1 nicht möglich, trifft der Vorstand des Gerichts im Einzelfall Anordnungen zur Einreichung von Dokumenten.

#### § 5 Übermittlung elektronischer Dokumente in Verfahren vor dem Verfassungsgericht

(1) In Verfahren vor dem Verfassungsgericht des Landes Brandenburg kann ein elektronisches Dokument abweichend von § 2 Absatz 3 Satz 1 ohne qualifizierte elektronische Signatur von der verantwortenden Person signiert und mit einem anderen sicheren Verfahren eingereicht werden.

(2) Andere sichere Verfahren sind

1.  der Postfach- und Versanddienst eines De-Mail-Kontos, wenn der Absender bei Versand der Nachricht sicher im Sinne des § 4 Absatz 1 Satz 2 des De-Mail-Gesetzes angemeldet ist und er sich die sichere Anmeldung gemäß § 5 Absatz 5 des De-Mail-Gesetzes bestätigen lässt,
2.  der Übermittlungsweg zwischen dem besonderen elektronischen Anwaltspostfach nach § 31a der Bundesrechtsanwaltsordnung oder einem entsprechenden, auf gesetzlicher Grundlage errichteten elektronischen Postfach und der elektronischen Poststelle des Gerichts,
3.  der Übermittlungsweg zwischen einem nach Durchführung eines Identifizierungsverfahrens eingerichteten Postfach einer Behörde oder einer juristischen Person des öffentlichen Rechts und der elektronischen Poststelle des Gerichts, das Nähere regelt die Elektronischer-Rechtsverkehr-Verordnung,
4.  sonstige bundeseinheitliche Übermittlungswege, die durch Rechtsverordnung der Bundesregierung mit Zustimmung des Bundesrates festgelegt werden, bei denen die Authentizität und die Integrität der Daten sowie die Barrierefreiheit gewährleistet sind.

#### § 6 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über den elektronischen Rechtsverkehr im Land Brandenburg vom 14.
Dezember 2006 (GVBl.
II S.
558), die zuletzt durch die Verordnung vom 21.
Oktober 2019 (GVBl.
II Nr.
86) geändert worden ist, außer Kraft.

Potsdam, den 28.
Januar 2022

Die Ministerin der Justiz

Susanne Hoffmann

### Anlagen

1

[Anlage (zu § 1)](/br2/sixcms/media.php/68/GVBl_II_18_2022-Anlage.pdf "Anlage (zu § 1)") 129.3 KB