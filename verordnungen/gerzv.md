## Verordnung über gerichtliche Zuständigkeiten und Zuständigkeitskonzentrationen (Gerichtszuständigkeitsverordnung - GerZV)

Auf Grund

1.  des § 219 Absatz 2 Satz 1 des Baugesetzbuches in der Fassung der Bekanntmachung vom 23.
    September 2004 (BGBl.
    I S. 2414), auch in Verbindung mit § 50 Absatz 1 Satz 2 des Enteignungsgesetzes des Landes Brandenburg vom 19.
    Oktober 1992 (GVBl.
    I S.
    430), in Verbindung mit § 1 Nummer 4 der Justiz-Zuständigkeitsübertragungsverordnung vom 9. April 2014 (GVBl.
    II Nr. 23);
2.  des § 4 Absatz 1 Satz 1 des Gesetzes über das gerichtliche Verfahren in Binnenschifffahrtssachen in der im Bundesgesetzblatt Teil III, Gliederungsnummer 310-5, veröffentlichten bereinigten Fassung, der durch Artikel 99 Nummer 4 Buchstabe a des Gesetzes vom 2.
    März 1974 (BGBl.
    I S.
    469, 561) geändert worden ist, in Verbindung mit § 1 Nummer 6 der Justiz-Zuständigkeitsübertragungsverordnung;
3.  des § 22c Absatz 1 Satz 1 und 3, § 23d Satz 1, § 58 Absatz 1 Satz 1, § 72 Absatz 2 Satz 2, § 93 Absatz 1, § 157 Absatz 2 Satz 1 des Gerichtsverfassungsgesetzes in der Fassung der Bekanntmachung vom 9.
    Mai 1975 (BGBl.
    I S.
    1077), von denen § 22c Absatz 1 Satz 1 und 3 durch Artikel 20 Nummer 1 des Gesetzes vom 23.
    Juli 2002 (BGBl.
    I S.
    2850, 2855) und § 93 Absatz 1 durch Artikel 17 Nummer 3 des Gesetzes vom 19.
    April 2006 (BGBl.
    I S.
    866, 868) neu gefasst worden sind, § 23d Satz 1 durch Artikel 22 Nummer 10 des Gesetzes vom 17. Dezember 2008 (BGBl.
    I S.
    2586, 2694) und § 72 Absatz 2 Satz 2 durch Artikel 22 Nummer 12 Buchstabe b des Gesetzes vom 17.
    Dezember 2008 (BGBl.
    I S.
    2586, 2694) geändert worden sind und § 157 Absatz 2 Satz 1 durch Artikel 2 Nummer 4 des Gesetzes vom 3.
    Dezember 1976 (BGBl.
    I S.
    3281, 3297) eingefügt worden ist, § 58 Absatz 1 Satz 1 des Gerichtsverfassungsgesetzes auch in Verbindung mit § 46 Absatz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
    Februar 1987 (BGBl.
    I S. 602), in Verbindung mit § 1 Nummer 14 der Justiz-Zuständigkeitsübertragungsverordnung;
4.  des § 8 Satz 1 des Gesetzes über das gerichtliche Verfahren in Landwirtschaftssachen in der im Bundesgesetzblatt Teil III, Gliederungsnummer 317-1, veröffentlichten bereinigten Fassung in Verbindung mit § 1 Nummer 26 der Justiz-Zuständigkeitsübertragungsverordnung;
5.  des § 68 Absatz 3 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
    Februar 1987 (BGBl.
    I S.
    602) in Verbindung mit § 1 Nummer 32 der Justiz-Zuständigkeitsübertragungsverordnung;
6.  des § 1 Absatz 2 Satz 1 und des § 65 Absatz 1 Satz 1 in Verbindung mit § 1 Absatz 2 Satz 1 der Schiffsregisterordnung in der Fassung der Bekanntmachung vom 26.
    Mai 1994 (BGBl.
    I S. 1133) in Verbindung mit § 1 Nummer 41 der Justiz-Zuständigkeitsübertragungsverordnung;
7.  des § 38 Absatz 2 Satz 1 und Absatz 4 des Sortenschutzgesetzes in der Fassung der Bekanntmachung vom 19.
    Dezember 1997 (BGBl.
    I S.
    3164), von denen § 38 Absatz 4 durch Artikel 9 Nummer 3 des Gesetzes vom 23.
    Juli 2002 (BGBl.
    I S.
    2850, 2852) geändert worden ist, in Verbindung mit § 1 Nummer 42 der Justiz-Zuständigkeitsübertragungsverordnung;
8.  des § 2 Absatz 1 Satz 3 und 4 des Transsexuellengesetzes vom 10.
    September 1980 (BGBl.
    I S.
    1654) in Verbindung mit § 1 Nummer 46 der Justiz-Zuständigkeitsübertragungsverordnung,
9.  des § 6 Absatz 2 Satz 1 des Unterlassungsklagengesetzes in der Fassung der Bekanntmachung vom 27.
    August 2002 (BGBl.
    I S.
    3422, 4346) in Verbindung mit § 1 Nummer 48 der Justiz-Zuständigkeits-übertragungsverordnung;
10.  des § 105 Absatz 1 und 2 des Urheberrechtsgesetzes vom 9.
    September 1965 (BGBl.
    I S. 1273) in Verbindung mit § 1 Nummer 49 der Justiz-Zuständigkeitsübertragungsverordnung;
11.  des § 89 Absatz 1 Satz 1 des Gesetzes gegen Wettbewerbsbeschränkungen in der Fassung der Bekanntmachung vom 26.
    Juni 2013 (BGBl.
    I S.
    1750) in Verbindung mit § 1 Nummer 53 der Justiz-Zuständigkeitsübertragungsverordnung;
12.  des § 802k Absatz 3 Satz 1 und § 882h Absatz 2 Satz 1 der Zivilprozessordnung, die durch Artikel 1 Nummer 7 und 17 des Gesetzes vom 29.
    Juli 2009 (BGBl.
    I S.
    2258) eingefügt worden sind, in Verbindung mit § 1 Nummer 56 der Justiz-Zuständigkeitsübertragungsverordnung;
13.  des § 1 Absatz 2 Satz 1 des Gesetzes über die Zwangsversteigerung und die Zwangsverwaltung in der im Bundesgesetzblatt Teil III, Gliederungsnummer 310-14, veröffentlichten bereinigten Fassung in Verbindung mit § 1 Nummer 57 der Justiz-Zuständigkeitsübertragungsverordnung

verordnet der Minister der Justiz:

#### § 1  
Kammern für Handelssachen

Bei allen Landgerichten werden für deren Bezirke Kammern für Handelssachen gebildet.

#### § 2  
Zuständigkeitskonzentrationen in Zivilsachen

(1) Das Landgericht Potsdam ist für alle Gerichtsbezirke des Landes Brandenburg zuständig für

1.  bürgerliche Rechtsstreitigkeiten nach § 87 des Gesetzes gegen Wettbewerbsbeschränkungen sowie bürgerliche Rechtsstreitigkeiten, die Artikel 101 oder 102 des Vertrages über die Arbeitsweise der Europäischen Union oder Artikel 53 oder 54 des Abkommens über den Europäischen Wirtschaftsraum betreffen,
2.  Rechtsstreitigkeiten nach dem Urheberrechtsgesetz, nach dem Gesetz betreffend das Urheberrecht an Werken der bildenden Künste und der Photographie, nach dem Gesetz über das Verlagsrecht und nach dem Urheberrechtswahrnehmungsgesetz,
3.  Rechtsstreitigkeiten nach dem Unterlassungsklagengesetz.

(2) Das Amtsgericht Potsdam ist für alle Gerichtsbezirke des Landes Brandenburg zuständig für Rechtsstreitigkeiten nach Absatz 1 Nummer 2, die in die Zuständigkeit der Amtsgerichte fallen.

(3) Das Amtsgericht Königs Wusterhausen ist für alle Gerichtsbezirke des Landes Brandenburg zuständig für die Angelegenheiten in europäischen Verfahren für geringfügige Forderungen nach der Verordnung (EG) Nr.
861/2007 des Europäischen Parlaments und des Rates vom 11. Juli 2007 zur Einführung eines europäischen Verfahrens für geringfügige Forderungen (ABl. L 199 vom 31.7.2007, S. 1), die in die Zuständigkeit der Amtsgerichte fallen.

#### § 3  
Zuständigkeitskonzentration in Streitigkeiten nach dem Wohnungseigentumsgesetz

Das Landgericht Frankfurt (Oder) ist in Streitigkeiten nach § 43 Absatz 2 des Wohnungseigentumsgesetzes gemeinsames Berufungs- und Beschwerdegericht für den Bezirk des Brandenburgischen Oberlandesgerichts.

#### § 4  
Partnerschafts- und Vereinsregister

Die Amtsgerichte am Sitz der Landgerichte führen das Partnerschafts- und das Vereinsregister für den gesamten Landgerichtsbezirk.

#### § 5  
Zuständigkeitskonzentration in Sortenschutzstreitsachen

Das Landgericht Cottbus ist in Sortenschutzstreitsachen im Sinne des § 38 Absatz 1 des Sortenschutzgesetzes für alle Gerichtsbezirke des Landes Brandenburg zuständig.

#### § 6  
Zuständigkeitskonzentrationen in Straf- und Bußgeldsachen

(1) Die Amtsgerichte am Sitz der Landgerichte sind für den gesamten Landgerichtsbezirk zuständig für die

1.  Strafsachen wegen gemeingefährlicher Straftaten und Umweltstrafsachen nach
    1.  §§ 307 bis 311, 324 bis 330a des Strafgesetzbuches,
    2.  §§ 38 und 38a des Bundesjagdgesetzes,
    3.  §§ 71 und 71a des Bundesnaturschutzgesetzes,
    4.  §§ 27 bis 27c des Chemikaliengesetzes,
    5.  § 11 des Gefahrgutbeförderungsgesetzes,
    6.  § 69 des Pflanzenschutzgesetzes,soweit sie zur Zuständigkeit der Amtsgerichte (§§ 24, 28 des Gerichtsverfassungsgesetzes) gehören,
2.  Bußgeldsachen wegen Umweltordnungswidrigkeiten nach
    1.  § 18 des Abfallverbringungsgesetzes,
    2.  § 15 des Abwasserabgabengesetzes,
    3.  § 46 des Atomgesetzes,
    4.  § 7 des Benzinbleigesetzes,
    5.  § 39 des Bundesjagdgesetzes,
    6.  § 69 des Bundesnaturschutzgesetzes,
    7.  § 62 des Bundes-Immissionsschutzgesetzes,
    8.  § 26 des Chemikaliengesetzes,
    9.  § 14 des Düngegesetzes,
    10.  § 10 des Gefahrgutbeförderungsgesetzes,
    11.  § 69 des Kreislaufwirtschaftsgesetzes,
    12.  § 68 des Pflanzenschutzgesetzes,
    13.  § 15 des Wasch- und Reinigungsmittelgesetzes,
    14.  § 103 des Wasserhaushaltsgesetzes,
    15.  § 29 des Wassersicherstellungsgesetzes,
    16.  § 48 des Brandenburgischen Abfall- und Bodenschutzgesetzes,
    17.  § 39 des Brandenburgischen Naturschutzausführungsgesetzes,
    18.  § 145 des Brandenburgischen Wassergesetzes,
    19.  § 60 des Jagdgesetzes für das Land Brandenburg,
    20.  § 23 des Landesimmissionsschutzgesetzes,
3.  Wirtschaftsstrafsachen wegen der in § 74c Absatz 1 des Gerichtsverfassungsgesetzes genannten Straftaten, soweit sie zur Zuständigkeit der Amtsgerichte (§§ 24, 28 des Gerichtsverfassungsgesetzes) gehören,
4.  Bußgeldsachen wegen Ordnungswidrigkeiten aus den in § 74c Absatz 1 Nummer 1 bis 4 und 6 Buchstabe b des Gerichtsverfassungsgesetzes genannten Rechtsgebieten.

Die Konzentration nach Satz 1 umfasst auch die im Ermittlungsverfahren zu treffenden richterlichen Entscheidungen.

(2) Dem Amtsgericht Frankfurt (Oder) werden die Strafsachen für die Bezirke der Amtsgerichte Frankfurt (Oder) und Eisenhüttenstadt zugewiesen.
Die Zuweisung nach Satz 1 umfasst auch die im Ermittlungsverfahren zu treffenden richterlichen Entscheidungen.
Der Jugendrichter des Amtsgerichts Frankfurt (Oder) wird zum Bezirksjugendrichter für die Bezirke der Amtsgerichte Frankfurt (Oder) und Eisenhüttenstadt bestellt.
Das bei dem Amtsgericht Frankfurt (Oder) eingerichtete Schöffengericht wird gemeinsames Schöffengericht für die Bezirke der Amtsgerichte Frankfurt (Oder) und Eisenhüttenstadt.
Das bei dem Amtsgericht Frankfurt (Oder) eingerichtete Jugendschöffengericht wird gemeinsames Jugendschöffengericht für die Bezirke der Amtsgerichte Frankfurt (Oder) und Eisenhüttenstadt.

(3) In Bußgeldverfahren wegen Verkehrsordnungswidrigkeiten nach den §§ 24 und 24a des Straßenverkehrsgesetzes obliegt die Entscheidung bei Einsprüchen gegen Bußgeldbescheide dem Amtsgericht, in dessen Bezirk die Ordnungswidrigkeit oder eine der Ordnungswidrigkeiten begangen worden ist.
Lässt sich die Zuständigkeit nicht nach Satz 1 bestimmen, so obliegt die Entscheidung dem nach § 68 Absatz 1 Satz 1 des Gesetzes über Ordnungswidrigkeiten zuständigen Gericht.

#### § 7  
Rechtshilfeersuchen in Verschlusssachen

Das Amtsgericht Potsdam ist zuständig für Rechtshilfeersuchen in Verschlusssachen.

#### § 8  
Zuständigkeitskonzentrationen in Landwirtschaftssachen

Die Amtsgerichte

1.  Cottbus für den Landgerichtsbezirk Cottbus,
2.  Frankfurt (Oder) für den Landgerichtsbezirk Frankfurt (Oder),
3.  Neuruppin für den Landgerichtsbezirk Neuruppin und
4.  Rathenow für den Landgerichtsbezirk Potsdam

sind zuständig für Verfahren nach § 1 des Gesetzes über gerichtliche Verfahren in Landwirtschaftssachen und nach § 65 des Landwirtschaftsanpassungsgesetzes.

#### § 9  
Binnenschifffahrtssachen und Binnenschiffsregister

Das Amtsgericht Brandenburg an der Havel ist für alle Gerichtsbezirke des Landes Brandenburg zuständig für die Verhandlung und Entscheidung von Binnenschifffahrtssachen.

#### § 10  
Baulandsachen und verwandte Verfahren

Die Landgerichte Frankfurt (Oder) für die Landgerichtsbezirke Cottbus und Frankfurt (Oder) und Neuruppin für die Landgerichtsbezirke Neuruppin und Potsdam sind zuständig

1.  in den in § 217 Absatz 1 des Baugesetzbuches genannten Fällen und
2.  für die gerichtliche Entscheidung über die Anfechtung von Entscheidungen der Enteignungsbehörde gemäß § 50 des Enteignungsgesetzes des Landes Brandenburg.

#### § 11  
Zentrales Vollstreckungsgericht

Die Aufgaben des zentralen Vollstreckungsgerichts nimmt das Amtsgericht Nauen wahr.

#### § 12  
Zuständigkeitskonzentration in Zwangsversteigerungs- und Zwangsverwaltungssachen

Die Amtsgerichte

1.  Frankfurt (Oder) für die Amtsgerichtsbezirke Eisenhüttenstadt, Frankfurt (Oder) und Fürstenwalde/Spree,
2.  Luckenwalde für die Amtsgerichtsbezirke Luckenwalde und Zossen,
3.  Neuruppin für den Landgerichtsbezirk Neuruppin,
4.  Potsdam für die Amtsgerichtsbezirke Brandenburg an der Havel, Nauen, Potsdam und Rathenow und
5.  Strausberg für die Amtsgerichtsbezirke Bad Freienwalde, Bernau bei Berlin, Eberswalde und Strausberg

sind zuständig in Verfahren nach dem Gesetz über die Zwangsversteigerung und die Zwangsverwaltung.

#### § 13  
Zuständigkeitskonzentration in Transsexuellensachen

Das Amtsgericht Potsdam ist für alle Gerichtsbezirke des Landes Brandenburg zuständig für Verfahren nach dem Transsexuellengesetz.

#### § 14  
Bereitschaftsdienstkonzentrationen

(1) Für die folgenden Amtsgerichte des Landgerichtsbezirks Frankfurt (Oder) wird gemäß § 22c Absatz 1 Satz 1 Alternative 1 des Gerichtsverfassungsgesetzes ein gemeinsamer Bereitschaftsdienstplan aufgestellt:

1.  für die Amtsgerichte Eberswalde, Bad Freienwalde (Oder), Bernau bei Berlin und Strausberg,
2.  für die Amtsgerichte Frankfurt (Oder), Eisenhüttenstadt und Fürstenwalde/Spree.

(2) Die Geschäfte des Bereitschaftsdienstes nimmt gemäß § 22c Absatz 1 Satz 1 Alternative 2 des Gerichtsverfassungsgesetzes folgendes Amtsgericht wahr:

1.  im Landgerichtsbezirk Neuruppin das Amtsgericht Neuruppin für die Amtsgerichte Oranienburg, Perleberg, Prenzlau, Schwedt/Oder und Zehdenick,
2.  im Landgerichtsbezirk Potsdam das Amtsgericht Potsdam für die Amtsgerichte Brandenburg an der Havel, Luckenwalde, Nauen, Rathenow und Zossen,
3.  im Landgerichtsbezirk Cottbus das Amtsgericht Königs Wusterhausen für das Amtsgericht Lübben (Spreewald).

(3) In den Fällen des Absatzes 2 Nummer 1 und 2 sind zu dem Bereitschaftsdienst auch die Richterinnen und Richter des Landgerichts heranzuziehen.

(4) Die Ausgestaltung des Bereitschaftsdienstplanes, insbesondere die Bereitschaftsdienstzeiten, die Abgrenzung der Zuständigkeit nach dem Bereitschaftsdienstplan von der Zuständigkeit nach dem allgemeinen Geschäftsverteilungsplan und die Angabe des zuständigen Bereitschaftsdienstgerichtes regeln die nach § 22c Absatz 1 Satz 4 und 5 des Gerichtsverfassungsgesetzes zuständigen Präsidien nach Maßgabe des § 21e des Gerichtsverfassungsgesetzes.

#### § 15  
Zuständigkeit für Streitigkeiten nach dem Asylgesetz

(1) Die Zuständigkeit des Verwaltungsgerichts für Streitigkeiten im Sinne von § 1 Absatz 1 des Asylgesetzes, einschließlich der Streitigkeiten im Sinne des § 29 Absatz 1 des Asylgesetzes, richtet sich nach allgemeinen Vorschriften, soweit sich die betroffene Person auf eine Verfolgung oder auf eine sonstige schädigende Maßnahme in einem der folgenden Herkunftsstaaten beruft:

Afghanistan, Albanien, Äthiopien, Eritrea, Kamerun, Kenia, Kosovo, Pakistan, Russische Föderation, Serbien, Somalia, Sudan, Südsudan, Syrien, Tschad.

(2) Im Übrigen ist zuständiges Verwaltungsgericht für Streitigkeiten nach dem Asylgesetz folgendes Verwaltungsgericht für betroffene Personen, soweit sie sich auf eine Verfolgung oder auf eine sonstige schädigende Maßnahme in folgenden Herkunftsstaaten berufen:

Verwaltungsgericht

Herkunftsstaat

Cottbus

Ägypten, Algerien, Angola, Äquatorialguinea, Benin, Botsuana, Burkina Faso, Burundi, Demokratische Republik Kongo, Dschibuti, Elfenbeinküste, Gabun, Gambia, Ghana, Guinea, Guinea-Bissau, Kap Verde, Komoren, Republik Kongo, Lesotho, Liberia, Libyen, Madagaskar, Malawi, Mali, Marokko, Mauretanien, Mauritius, Mosambik, Namibia, Niger, Nigeria, Ruanda, Sambia, São Tomé und Príncipe, Senegal, Seychellen, Sierra Leone, Simbabwe, Südafrika, Swasiland, Tansania, Togo, Tunesien, Uganda, Zentralafrikanische Republik

Frankfurt (Oder)

Bangladesch, Bhutan, Bosnien-Herzegowina, Brunei Darussalam, China, Demokratische Volksrepublik Korea, Demokratische Volksrepublik Laos, Indien, Indonesien, Japan, Kambodscha, Malaysia, Malediven, Republik Nordmazedonien, Moldawien, Mongolei, Montenegro, Myanmar, Nepal, Papua-Neuguinea, Philippinen, Republik Korea, Singapur, Sri Lanka, Taiwan, Thailand, Ukraine, Vietnam

Potsdam

Armenien, Aserbaidschan, Bahrain, Georgien, Irak, Iran, Israel, Jemen, Jordanien, Kasachstan, Katar, Kirgisistan, Kuwait, Libanon, Oman, Saudi-Arabien, Tadschikistan, Türkei, Turkmenistan, Usbekistan, Vereinigte Arabische Emirate und sonstige Herkunftsstaaten

#### § 16  
Begründung eines Gruppen-Gerichtsstandes

Ein Gruppen-Gerichtsstand kann auf Antrag des Schuldners gemäß § 3a der Insolvenzordnung für den Bezirk des Brandenburgischen Oberlandesgerichts bei dem Amtsgericht Potsdam als Insolvenzgericht begründet werden.

#### § 17  
Übergangsregelungen

Für die zum Zeitpunkt des Inkrafttretens dieser Verordnung anhängigen Verfahren verbleibt es bei der bisherigen Zuständigkeit.
Hiervon ausgenommen sind die nach § 6 Absatz 2 zugewiesenen Verfahren.

#### § 18  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 1.
Oktober 2014 in Kraft.
Gleichzeitig tritt die Zweite Gerichtszuständigkeits-Verordnung vom 8.
Mai 2007 (GVBl.
II S.
113), die zuletzt durch Verordnung vom 18.
Juli 2012 (GVBl.
II Nr.
60) geändert worden ist, außer Kraft.

Potsdam, den 2.
September 2014

Der Minister der Justiz

Dr.
Helmuth Markov