## Verordnung über die Anerkennung als Hochschulklinik (Hochschulklinikanerkennungsverordnung - HSKV)

Auf Grund des § 86a Absatz 2 des Brandenburgischen Hochschulgesetzes vom 28.
April 2014 (GVBl. I Nr. 18), der durch Artikel 2 des Gesetzes vom 20.
September 2018 (GVBl.
I Nr. 21 S. 2) eingefügt worden ist, verordnet die Ministerin für Wissenschaft, Forschung und Kultur im Einvernehmen mit der Ministerin für Arbeit, Soziales, Gesundheit, Frauen und Familie:

#### § 1 Anerkennungsvoraussetzungen

(1) Krankenhäuser, die gemeinsam mit einer als Hochschule staatlich anerkannten Einrichtung des Bildungswesens (staatlich anerkannte Hochschule) Lehre und Forschung im Studium der Humanmedizin gemäß der Approbationsordnung für Ärzte vom 27.
Juni 2002 (BGBl.
I S.
2405), die zuletzt durch Artikel 5 des Gesetzes vom 15.
August 2019 (BGBl.
I S.
1307, 1329) geändert worden ist, in der jeweils geltenden Fassung sicherstellen, können als Hochschulklinik staatlich anerkannt werden, wenn sie die Voraussetzungen nach den Absätzen 2 und 3 erfüllen.
Dies gilt auch, wenn mehrere Krankenhäuser im Verbund (Klinikverbund) gemeinsam die Aufgaben in Lehre und Forschung nach Satz 1 sicherstellen.

(2) Voraussetzungen der Anerkennung als Hochschulklinik sind, dass

1.  die staatlich anerkannte Hochschule dem Antrag des Krankenhauses oder im Falle des Absatzes 1 Satz 2 dem Antrag des Klinikverbundes auf Anerkennung als Hochschulklinik zustimmt,
2.  die Zusammenarbeit des Krankenhauses mit der staatlich anerkannten Hochschule in Lehre und Forschung nach Art und Umfang deutlich über die für ein akademisches Lehrkrankenhaus übliche Zusammenarbeit hinausgeht und aufgrund eines Kooperationsvertrages erfolgt, der Regelungen zum Beitrag des Krankenhauses in Lehre und Forschung insbesondere durch Bereitstellung personeller, räumlicher, sächlicher und finanzieller Ressourcen sowie Regelungen zur Gewährleistung der Freiheit von Wissenschaft, Forschung und Lehre durch das Krankenhaus enthält,
3.  im Falle des Absatzes 1 Satz 2 der Kooperationsvertrag zwischen der staatlich anerkannten Hochschule und den Krankenhäusern des Klinikverbundes zudem Regelungen zu folgenden Themen enthält:
    1.  zum Verhältnis der Krankenhäuser des Klinikverbundes untereinander sowie zur staatlich anerkannten Hochschule,
    2.  zu den jeweiligen Beiträgen der Krankenhäuser des Klinikverbundes in der Lehre und insbesondere zur Art und Weise der Sicherstellung der Lehre bei einem dezentralen Studienmodell,
    3.  zu den jeweiligen Beiträgen der Krankenhäuser des Klinikverbundes in der Forschung einschließlich einer gemeinsamen Abstimmung bei der Bildung von Forschungsschwerpunkten an den einzelnen Krankenhäusern,
    4.  zur Erfüllung der Anerkennungsvoraussetzungen nach den Nummern 4 bis 9 sowie
    5.  zur gemeinsamen Entscheidungsfindung sowie zur Lösung von Konflikten,
4.  das Krankenhaus in hinreichendem Umfang in der Krankenversorgung tätige Ärztinnen oder Ärzte in Leitungspositionen beschäftigt, die die Einstellungsvoraussetzungen für Professorinnen und Professoren nach § 41 Absatz 1 Nummer 1, 2, 3 und 4 Buchstabe a und c des Brandenburgischen Hochschulgesetzes erfüllen, und diese zur Erfüllung von Aufgaben an der staatlich anerkannten Hochschule von Aufgaben der Krankenversorgung im Umfang von mindestens der Hälfte der Dienstaufgaben einer vollbeschäftigten, fest angestellten Professorin oder eines vollbeschäftigten, fest angestellten Professors freistellt,
5.  zur Gewährleistung wissenschaftsadäquater Berufungsverfahren für klinische Professorinnen und Professoren an der staatlich anerkannten Hochschule das Krankenhaus das Personal nach Nummer 4 im Einvernehmen mit der staatlich anerkannten Hochschule einstellt und im Benehmen mit dieser entlässt,
6.  das Krankenhaus oder im Falle des Absatzes 1 Satz 2 der Klinikverbund die überwiegende Mehrzahl der klinischen Fächer, die nach Anlage 2b der Approbationsordnung für Ärzte vorgeschrieben sind, abdeckt,
7.  das Krankenhaus auf der Grundlage eines von der staatlich anerkannten Hochschule entwickelten Forschungskonzepts Aufgaben in der Forschung in dem für ein Hochschulklinikum erforderlichen Umfang erfüllt,
8.  das Krankenhaus ein Krankenhaus der höchsten Versorgungsstufe oder in Fällen des Absatzes 1 Satz 2 im besonderen Ausnahmefall ein Fachkrankenhaus mit hoch spezialisierten medizinischen Leistungen und überregionalem Versorgungsauftrag ist, wenn mindestens ein weiteres Krankenhaus der höchsten Versorgungsstufe Teil des Klinikverbundes ist und die Mitwirkung des Fachkrankenhauses im Klinikverbund ein notwendiger Bestandteil für die Lehre und Forschung der staatlich anerkannten Hochschule ist,
9.  das Krankenhaus eine nach anerkannten Maßstäben hinreichende klinische Leistungsfähigkeit in der Krankenversorgung sowie Elemente der Hochleistungsmedizin aufweist.

Die klinische Leistungsfähigkeit nach Satz 1 Nummer 9 bemisst sich insbesondere nach der Anzahl der Fachabteilungen, den jährlichen Fallzahlen, der Fallschwere und nach der Anzahl der Betten.
Das Krankenhaus oder der Klinikverbund nach Absatz 1 Satz 2 verfügt in der Regel über mindestens 1 000 Betten.

(3) Höchstens ein Krankenhaus eines Klinikverbundes nach Absatz 1 Satz 2 kann im Ausnahmefall seinen Sitz in einem anderen Land der Bundesrepublik Deutschland haben.
Voraussetzung der staatlichen Anerkennung dieses Krankenhauses ist neben der Erfüllung der Voraussetzungen nach Absatz 2 die Zustimmung des Landes, in dem das Krankenhaus seinen Sitz hat.

#### § 2 Anerkennungsverfahren

(1) Die für die Hochschulen zuständige oberste Landesbehörde spricht auf Antrag des Krankenhauses oder des Klinikverbundes nach § 1 Absatz 1 Satz 2 mit Zustimmung der für Gesundheit zuständigen obersten Landesbehörde die staatliche Anerkennung aus.
Die für Gesundheit zuständige oberste Landesbehörde kann die Zustimmung verweigern, wenn durch die Anerkennung die Krankenversorgung gefährdet würde.
Ein Anspruch auf staatliche Anerkennung besteht nicht.

(2) Die für die Hochschulen zuständige oberste Landesbehörde kann eine gutachterliche Stellungnahme einer von ihr bestimmten sachverständigen Stelle einholen.
Diese Stellungnahme nimmt die Entscheidung der Behörde weder ganz noch teilweise vorweg.
In der gutachterlichen Stellungnahme wird die Erfüllung der Anerkennungsvoraussetzungen nach § 1 fachlich bewertet.
Die Kosten für die gutachterliche Stellungnahme sind von dem antragstellenden Krankenhaus oder dem antragstellenden Klinikverbund nach § 1 Absatz 1 Satz 2 zu tragen.

(3) Die Anerkennung kann mit Nebenbestimmungen, insbesondere befristet ausgesprochen und mit Auflagen versehen werden, die der Erfüllung der Anerkennungsvoraussetzungen nach § 1 dienen.

#### § 3 Folgen der Anerkennung

(1) Die als Hochschulkliniken staatlich anerkannten Krankenhäuser haben nach Maßgabe der Anerkennung das Recht, die Bezeichnung Hochschulklinik, Universitätsklinik oder eine entsprechende fremdsprachliche Bezeichnung im Namen zu führen oder in vergleichbarer Weise zu verwenden.
Sie unterstehen der Rechtsaufsicht der für die Hochschulen zuständigen obersten Landesbehörde, soweit sie Aufgaben in Lehre und Forschung im Studium der Humanmedizin wahrnehmen.
Aufsichtszuständigkeiten nach anderen Rechtsvorschriften bleiben unberührt.

(2) Die Anerkennung als Hochschulklinik begründet keinen Anspruch auf staatliche Zuschüsse.
Dies gilt insbesondere für den Hochschulbau.
Die Investitionsfinanzierung nach dem Krankenhausfinanzierungsgesetz in der Fassung der Bekanntmachung vom 10.
April 1991 (BGBl. I S. 886), das zuletzt durch Artikel 14 des Gesetzes vom 6.
Mai 2019 (BGBl.
I S.
646, 689) geändert worden ist, in der jeweils geltenden Fassung in Verbindung mit dem Brandenburgischen Krankenhausentwicklungsgesetz vom 8. Juli 2009 (GVBl.
I S.
310), das zuletzt durch Artikel 1 des Gesetzes vom 30.
April 2019 (GVBl. I Nr. 13) geändert worden ist, in der jeweils geltenden Fassung bleibt unberührt.

#### § 4 Verlust der Anerkennung

(1) Die Anerkennung erlischt, wenn die staatliche Anerkennung der Einrichtung des Bildungswesens als Hochschule erlischt.

(2) Die Anerkennung ist durch das für die Hochschulen zuständige Mitglied der Landesregierung aufzuheben, wenn die Voraussetzungen nach § 1 nicht gegeben waren, später weggefallen sind oder Auflagen gemäß § 2 Absatz 3 nicht erfüllt wurden und diesem Mangel trotz Beanstandung innerhalb einer bestimmten Frist nicht abgeholfen wurde.

#### § 5 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 9.
September 2019

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Dr.
Martina Münch