## Verordnung über die Übertragung des Rechts zur Berufung der Hochschullehrerinnen und Hochschullehrer an der Brandenburgischen Technischen Universität Cottbus-Senftenberg

Auf Grund des § 40 Absatz 5 Satz 2 des Brandenburgischen Hochschulgesetzes vom 28.
April 2014 (GVBl.
I Nr.
18) verordnet die Ministerin für Wissenschaft, Forschung und Kultur:

#### § 1 Übertragung des Berufungsrechts

Der Brandenburgischen Technischen Universität Cottbus-Senftenberg wird das Recht zur Berufung der Hochschullehrerinnen und Hochschullehrer übertragen.

#### § 2 Übergangsvorschrift

(1) Für laufende Berufungsverfahren wird das Berufungsrecht übertragen, soweit vor Inkrafttreten dieser Verordnung bei dem für die Hochschulen zuständigen Mitglied der Landesregierung kein Berufungsvorschlag eingereicht worden ist.

(2) Soweit ein Berufungsvorschlag vor Inkrafttreten dieser Verordnung bei dem für die Hochschulen zuständigen Mitglied der Landesregierung eingereicht worden ist, kann das für die Hochschulen zuständige Mitglied der Landesregierung das Berufungsrecht im Einzelfall übertragen.

#### § 3 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 10.
November 2016

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Dr.
Martina Münch