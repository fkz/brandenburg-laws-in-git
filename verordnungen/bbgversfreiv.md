## Verordnung über die erlaubnisfreie Einleitung von Niederschlagswasser in das Grundwasser durch schadlose Versickerung (Versickerungsfreistellungsverordnung - BbgVersFreiV)

Auf Grund des § 2 Absatz 1 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20), der durch Artikel 1 Nummer 2 des Gesetzes vom 4. Dezember 2017 (GVBl.
I Nr.
28) neu gefasst worden ist, in Verbindung mit § 23 Absatz 1 und 3 sowie § 46 Absatz 2 und 3 des Wasserhaushaltsgesetzes vom 31.
Juli 2009 (BGBl.
I S. 2585), von denen § 23 zuletzt durch Artikel 2 des Gesetzes vom 4.
Dezember 2018 (BGBl.
I S.
2254) geändert worden ist, verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Geltungsbereich

Diese Verordnung gilt für Flächen im Geltungsbereich der §§ 30, 34 und 35 des Baugesetzbuches.
Sie gilt nicht für

1.  Flächen innerhalb der Schutzzonen I und II von Wasserschutzgebieten,
2.  Gebiete, deren Böden erheblich mit umweltgefährdenden Stoffen belastet sind und die im Bebauungsplan auf Grund § 9 Absatz 5 Nummer 3 des Baugesetzbuches dahingehend ausgewiesen sind,
3.  außerörtliche öffentliche Straßen gemäß § 3 Absatz 1 des Brandenburgischen Straßengesetzes sowie § 1 des Bundesfernstraßengesetzes.

#### § 2 Erlaubnisfreie Einleitung von Niederschlagswasser

(1) Für das Einleiten von Niederschlagswasser gemäß § 54 Absatz 1 Satz 1 Nummer 2 des Wasserhaushaltsgesetzes in das Grundwasser durch Versickerung ist eine Erlaubnis vorbehaltlich der §§ 3 und 5 nicht erforderlich, wenn die Anforderungen nach § 4 erfüllt sind und Anordnungen nach § 6 nicht bestehen.
Die Versickerung von Niederschlagswasser, welches von genehmigungsfreien Bauvorhaben gemäß § 61 der Brandenburgischen Bauordnung stammt, ist unabhängig von Satz 1 erlaubnisfrei.

(2) Anforderungen, die sich aus Schutzvorschriften zu Wasserschutzgebieten und aus anderen öffentlich-rechtlichen Vorschriften ergeben, bleiben unberührt.

#### § 3 Ausschluss bestimmter Herkunftsflächen

Von der Erlaubnisfreistellung bleibt das Versickern von gesammelt abfließendem Niederschlagswasser folgender Herkunftsflächen ausgenommen:

1.  Gewerbe- und Industriegebiete nach den §§ 8 und 9 der Baunutzungsverordnung, Sondergebiete mit vergleichbarer Nutzung, sowie gewerblich oder industriell genutzte Flächen in Misch- und Kerngebieten im Sinne der §§ 6 und 7 der Baunutzungsverordnung, soweit von ihnen eine maßgebliche Staubbelastung ausgeht,
2.  Flächen, auf denen mit wassergefährdenden Stoffen nach § 62 Absatz 3 des Wasserhaushaltsgesetzes einschließlich Jauche, Gülle und Silagesickersäften umgegangen wird,
3.  Dachflächen mit Anteilen unbeschichteter metallischer Flächen aus Blei, Kupfer oder Zink von mehr als 50 Quadratmeter,
4.  Parkplätze mit mehr als 100 Stellplätzen, soweit das Niederschlagswasser nicht über wasserdurchlässige Flächenbeläge mit bauaufsichtlicher Zulassung des Deutschen Institutes für Bautechnik versickert wird,
5.  abflusswirksam versiegelte Flächen größer als 800 Quadratmeter sowie Gebäude mit einer Grundfläche größer als 400 Quadratmeter.

#### § 4 Anforderungen an das erlaubnisfreie Versickern

(1) Bei der Bemessung und Herstellung sowie dem Betrieb und der Wartung von Versickerungsanlagen zur erlaubnisfreien Einleitung sind die allgemein anerkannten Regeln der Technik zu beachten.

(2) Bei der Bemessung der Anlagen sind insbesondere die Anforderungen zum Überflutungsschutz zu beachten.
Hiervon ausgenommen sind Flächen, die gezielt als temporäre Überflutungsfläche für Starkregenereignisse bestimmt sind.

(3) Erlaubnisfrei zu versickerndes Niederschlagswasser ist vorrangig flächenhaft oder in Mulden über eine geeignete Oberbodenschicht zu versickern.
Die Bodenschicht ist geeignet, wenn der Ober- und Unterboden eine ausreichende Durchlässigkeit aufweisen und die belebte Bodenzone aus einer mindestens 20 Zentimeter mächtigen bewachsenen Oberschicht besteht.
Der Abstand zwischen Geländeoberkante und mittlerem höchsten Grundwasserstand (mHGW) muss mindestens 1 Meter betragen.

(4) Die Versickerung über andere Versickerungsanlagen (zum Beispiel Rigolen, Mulden-Rigolen-Systeme, Sickerrohre) ist erlaubnisfrei, wenn eine flächenhafte Versickerung nach Absatz 3 nicht möglich ist und das zu versickernde Niederschlagswasser von geringbelasteten Herkunftsflächen stammt.
Als gering belastete Herkunftsflächen gelten unter anderem Gründächer, Dachflächen ohne oder mit nur geringfügiger Metalleindeckung, Terrassen, Fuß-, Rad- und Gehwege sowie wenig befahrene Verkehrsflächen in Wohngebieten (bis zu 300 Kraftfahrzeuge in 24 Stunden beziehungsweise bis zu 50 Wohneinheiten).
Der Abstand zwischen der Sohle der Versickerungsanlage und dem mittleren höchsten Grundwasserstand (mHGW) muss mindestens 1 Meter betragen.
Grundwasserschützende Schichten dürfen nicht durchstoßen werden.

(5) In der Schutzzone III von Wasserschutzgebieten ist nur das breitflächige Versickern von gering belasteten Niederschlagswasserabflüssen im Sinne der Begriffsbestimmung der jeweiligen Verordnung zur Festsetzung des Wasserschutzgebietes über die belebte Bodenzone (Flächen- oder Muldenversickerung) erlaubnisfrei.

#### § 5 Verfahren

(1) Im Baugenehmigungsverfahren oder Bauanzeigeverfahren ist durch die bauvorlagenberechtigte Entwurfsverfasserin oder den bauvorlagenberechtigten Entwurfsverfasser zu erklären, dass die Voraussetzungen für die Erlaubnisfreiheit gegeben sind, andernfalls ist mit dem Bauantrag zugleich auch ein Antrag auf wasserrechtliche Erlaubnis zu stellen.

(2) Entfallen die Voraussetzungen der Erlaubnisfreiheit aufgrund einer nachträglichen wesentlichen Änderung der bestehenden Niederschlagswasserbeseitigung, ist ein Antrag auf wasserrechtliche Erlaubnis durch den Gewässerbenutzer zu stellen.

#### § 6 Wiederherstellung der Erlaubnispflicht

Die Wasserbehörde kann, um eine Beeinträchtigung des Wohls der Allgemeinheit zu vermeiden, für zu bezeichnende Gebiete die Erlaubnispflicht für Niederschlagswassereinleitungen nach § 2 anordnen.
In der Anordnung sind die betroffenen Flurstücke zu benennen.
Die Anordnung ist öffentlich bekannt zu machen.

#### § 7 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 25.
April 2019

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger