## Verordnung zur Ausgestaltung der Übergangsregelung für Verbundspielhallen (Spielhallenverbundverordnung - SpielhVerbV)

Auf Grund des § 11 Absatz 4 des Brandenburgischen Spielhallengesetzes vom 23.
Juni 2021 (GVBl. I Nr. 22 S. 8) verordnet die Ministerin für Soziales, Gesundheit, Integration und Verbraucherschutz mit Zustimmung des Ministers des Innern und für Kommunales und des Ministers für Wirtschaft, Arbeit und Energie:

#### § 1 Zertifizierung von Verbundspielhallen

(1) Die Zertifizierung von Verbundspielhallen hat ausschließlich durch nach § 11 Absatz 3 Satz 5 des Brandenburgischen Spielhallengesetzes akkreditierte Prüforganisationen zu erfolgen.

(2) Die Betreiberin oder der Betreiber der Verbundspielhalle hat mit der Prüforganisation eine Vereinbarung zu schließen, die vorsieht, dass während der Laufzeit der Zertifizierung jährlich mindestens zwei stichprobenartige Überprüfungen zum Vorliegen der Voraussetzungen der Zertifizierung durchzuführen sind.
Mindestens eine dieser Überprüfungen muss unangekündigt erfolgen und darf nicht als Überprüfung erkennbar sein.

(3) Wird bei der Überprüfung nach Absatz 2 festgestellt, dass die Voraussetzungen einer Zertifizierung nicht vorliegen, ist die Zertifizierung durch die Prüforganisation zu entziehen, es sei denn, es handelt sich um einen erstmalig festgestellten und unverzüglich behobenen nicht schwerwiegenden Mangel.
Die Mangelbehebung ist gegenüber der Zertifizierungsstelle nachzuweisen.
Über festgestellte Mängel sowie einen Entzug der Zertifizierung hat die Betreiberin oder der Betreiber der Spielhalle die zuständige Erlaubnisbehörde nach dem Brandenburgischen Spielhallengesetz unverzüglich zu informieren.

(4) Alle zur Führung einer zertifizierten Verbundspielhalle notwendigen Unterlagen müssen durch die Betreiberin oder den Betreiber der Verbundspielhalle zusammengefasst und zur Kontrolle durch die Erlaubnisbehörde nach dem Brandenburgischen Spielhallengesetz während der üblichen Geschäftszeit vorgehalten werden.

(5) Die Zuständigkeiten und Befugnisse der Erlaubnisbehörde nach dem Brandenburgischen Spielhallengesetz bleiben unberührt.
Sie ist berechtigt, Erkenntnisse, die gegen eine Zertifizierung einer Spielhalle sprechen könnten, der Zertifizierungsstelle mitzuteilen.
Dazu hat die Betreiberin oder der Betreiber der Verbundspielhalle der Erlaubnisbehörde einen Wechsel der Zertifizierungsstelle anzuzeigen.

#### § 2 Inhalt und Umfang der besonderen Schulungen für das Personal von Verbundspielhallen

(1) Die besonderen Schulungen des Personals der Verbundspielhallen nach § 11 Absatz 3 Satz 1 Nummer 3 des Brandenburgischen Spielhallengesetzes müssen mindestens folgende Inhalte und Kompetenzen vermitteln:

1.  vertiefendes Wissen zum Jugend- und Spielerschutz und dessen Umsetzung,
2.  vertiefende Kenntnisse zur Glücksspielsucht einschließlich regionaler, landesweiter und bundesweiter anbieterunabhängiger Hilfeangebote einschließlich der Telefonberatungen,
3.  Besonderheiten von Verbundspielhallen,
4.  Inhalt, Umsetzung und Dokumentation des Sozialkonzeptes und daraus resultierende Aufgaben,
5.  Intensivierung der Gesprächsführung mit problematischen und pathologischen Spielerinnen und Spielern.

(2) Die Schulungsdauer muss mindestens vier Zeitstunden betragen.
Die Zahl der Teilnehmenden einer Schulung soll 15 Personen nicht überschreiten.

(3) Die Teilnahme gilt als erfolgreich, wenn die Schulungsanbieterin oder der Schulungsanbieter bescheinigt, dass die oder der Teilnehmende ohne Fehlzeiten teilgenommen hat und die Schulungsanbieterin oder der Schulungsanbieter sich in geeigneter Weise davon überzeugt hat, dass die oder der Teilnehmende mit den Inhalten nach Absatz 1 vertraut ist.
Der Nachweis über eine erfolgreiche Teilnahme an der Schulung ist der Schulungsteilnehmerin oder dem Schulungsteilnehmer von der Schulungsanbieterin oder dem Schulungsanbieter in schriftlicher Form auszuhändigen.
Der Nachweis ist der Betreiberin oder dem Betreiber der Verbundspielhalle, in welcher die Schulungsteilnehmerin oder der Schulungsteilnehmer angestellt ist, vorzulegen.

(4) Die Schulungen sind jährlich vollumfänglich zu wiederholen.

(5) Die Betreiberin oder der Betreiber von Verbundspielhallen stellt sicher, dass Mitarbeiterinnen und Mitarbeiter, die nach Erteilung der Erlaubnis nach § 11 Absatz 3 des Brandenburgischen Spielhallengesetzes eine Tätigkeit in einem Verbundspielhallenbetrieb aufnehmen, innerhalb von bis zu drei Monaten nach Aufnahme dieser Tätigkeit mit erfolgreicher Teilnahme besonders geschult wurden.

#### § 3 Sachkundenachweis für den Betrieb von Verbundspielhallen

(1) Betreiberinnen und Betreiber von Verbundspielhallen haben neben den nach § 5 Absatz 1 Satz 2 Nummer 3 des Brandenburgischen Spielhallengesetzes vorgegebenen regelmäßigen Schulungen auch an besonderen Schulungen nach § 2 teilzunehmen.

(2) Der Nachweis über eine erfolgreiche Teilnahme an der besonderen Schulung ist von der Betreiberin oder dem Betreiber in Form einer erfolgreich abgeschlossenen Prüfung zu erbringen.

(3) Der Nachweis über eine erfolgreich bestandene Prüfung ist der Betreiberin oder dem Betreiber von der Schulungsanbieterin oder dem Schulungsanbieter in Form eines schriftlichen Sachkundenachweises auszuhändigen.

(4) Der Sachkundenachweis ist von jeder Betreiberin und jedem Betreiber mindestens einmal pro Kalenderjahr zu erneuern.

#### § 4 Anerkennungsverfahren für besondere Schulungsangebote für das Personal von Verbundspielhallen

(1) Besondere Schulungsangebote für das Personal von Verbundspielhallen werden auf schriftlichen Antrag unter Beifügung entsprechender Nachweise und Erklärungen durch das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit anerkannt.
Schulungsangebote sind anzuerkennen, wenn

1.  die Voraussetzungen zu Inhalt und Umfang nach § 2 vorliegen und
2.  die Schulungsanbieterin oder der Schulungsanbieter auf Grund entsprechender Erklärungen und Nachweise die Gewähr dafür bietet, dass
    1.  sie oder er Erfahrungen mit der Durchführung von Schulungen im Rahmen von Aus- und Fortbildungen besitzt,
    2.  die Schulung durch fachlich qualifizierte Dozentinnen und Dozenten durchgeführt wird, welche auf Grund mindestens zweijähriger praktischer Erfahrungen und Aktivitäten in der Suchtprävention oder Suchtberatung in der Lage sind, die nach § 2 Absatz 1 erforderlichen Inhalte erfolgreich an die zu schulenden Personen zu vermitteln,
    3.  sie oder er keine wirtschaftliche Unterstützung durch Betreiberinnen und Betreiber von Spielhallen,Automatenaufstellerinnen und -aufsteller oder andere Veranstalterinnen und Veranstalter von Glücksspielen erhält und nicht in finanzieller Abhängigkeit zu ihnen steht.

(2) Wird bekannt, dass die in Absatz 1 Satz 2 beschriebenen Anforderungen nicht mehr vorliegen, ist die Anerkennung zu widerrufen.

(3) Anerkannte Schulungsangebote werden in eine Liste aufgenommen, die auf der Internetseite des Landesamts für Arbeitsschutz, Verbraucherschutz und Gesundheit veröffentlicht wird.

#### § 5 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt vier Wochen nach der Verkündung in Kraft.
Sie tritt mit Ablauf des 31. Dezember 2025 außer Kraft.

Potsdam, den 18.
Januar 2022

Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

Ursula Nonnemacher