## Verordnung zur Ergänzung schulrechtlicher Vorschriften zur Sicherstellung des Bildungs- und Erziehungsauftrags in den schulischen Bildungsgängen bei besonderen Einschränkungen (Bildungsgänge-Ergänzungsverordnung - BiGEV)

Auf Grund des § 19 Absatz 5, §§ 23, 24 Absatz 4 in Verbindung mit § 11 Absatz 4 Satz 1, § 13 Absatz 3, § 37 Absatz 2, § 56 Satz 1, § 57 Absatz 4, § 58 Absatz 3, § 59 Absatz 9, § 60 Absatz 4 Satz 1 und § 61 Absatz 3 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
August 2002 (GVBl.
I S. 78), von denen § 13 Absatz 3 durch Artikel 1 des Gesetzes vom 8.
Januar 2007 (GVBl.
I S. 2), § 19 Absatz 5, §§ 23, 24 Absatz 4 und § 56 Satz 1 zuletzt durch Artikel 2 des Gesetzes vom 14.
März 2014 (GVBl.
I Nr. 14 S. 2) und § 37 Absatz 2 durch Artikel 7 des Gesetzes vom 25.
Januar 2016 (GVBl.
I Nr.
5 S. 10) geändert und § 57 Absatz 4 durch Gesetz vom 10.
Juli 2017 (GVBl.
I Nr.
16, 22) neu gefasst worden sind, verordnet die Ministerin für Bildung, Jugend und Sport:

**Inhaltsübersicht**

Abschnitt 1  
Allgemeine Vorschriften
-------------------------------------

[§ 1 Geltungsbereich](#1)

[§ 2 Unterrichtsformen](#2)

[§ 3 Organisation des Distanzunterrichts](#3)

Abschnitt 2  
Bildungsgangübergreifende Vorschriften
----------------------------------------------------

[§ 4 Rahmenlehrplan und Stundentafel](#4)

[§ 5 Grundsätze der Leistungsbewertung](#5)

[§ 6 Bewertung des Arbeits- und Sozialverhaltens](#6)

[§ 7 Prüfungen](#7)

[§ 8 Lehrkräfte](#8)

[§ 9 Probezeit](#9)

[§ 10 Versetzungen](#10)

[§ 11 Wiederholung](#11)

[§ 12 Verweildauer](#12)

[§ 13 Mitwirkungsrechte in der Schule](#13)

[§ 14 Ausschüsse](#14)

[§ 15 Fristen und Termine](#15)

[§ 16 Zuhörende](#16)

[§ 17 Beschlüsse](#17)

Abschnitt 3  
Bildungsgangbezogene Vorschriften der Grundschule und der weiterführenden allgemeinbildenden Schulen sowie der Schulen des Zweiten Bildungsweges
--------------------------------------------------------------------------------------------------------------------------------------------------------------

### Unterabschnitt 1  
Bildungsgang der Grundschule und Bildungsgänge der Sekundarstufe I

[§ 18 Schulärztliche Untersuchungen](#18)

[§ 18a Aufnahme in eine Leistungs- und Begabungsklasse](#18a)

[§ 18b Unterrichtsorganisation in der Sekundarstufe I](#18b)

[§ 19 Prüfungen in der Jahrgangsstufe 10](#19)

### Unterabschnitt 2  
Bildungsgang der gymnasialen Oberstufe

[§ 20 Unterrichtsorganisation](#20)

[§ 21 Klausuren und andere Bewertungsbereiche](#21)

[§ 22 Mündliche Abiturprüfung](#22)

### Unterabschnitt 3  
Bildungsgänge im Zweiten Bildungsweg

[§ 23 Klausuren](#23)

Abschnitt 4  
Berufliche Bildungsgänge
--------------------------------------

### Unterabschnitt 1  
Bildungsgang der Berufsfachschule Soziales

[§ 24 Leistungsbewertung](#24)

[§ 25 Versetzungen](#25)

[§ 26 Zulassung zur Prüfung](#26)

[§ 27 Prüfung](#27)

[§ 28 Praktische Ausbildung](#28)

### Unterabschnitt 2  
Bildungsgang der Berufsfachschule zur Erlangung eines Berufsabschlusses nach Landesrecht

[§ 29 Leistungsbewertung](#29)

[§ 30 Wiederholung](#30)

[§ 30a Praktikum und Zulassung zur Prüfung](#30a)

### Unterabschnitt 3  
Bildungsgänge der Fachschule für Technik und Wirtschaft

[§ 31 Leistungsbewertung](#31)

[§ 32 Wiederholung](#32)

[§ 32a Mündliche Prüfung](#32a)

### Unterabschnitt 4  
Bildungsgänge für Sozialwesen in der Fachschule

[§ 33 Leistungsbewertung](#33)

[§ 34 Versetzungen](#34)

[§ 35 Wiederholung](#35)

[§ 36 Zulassung zur Prüfung](#36)

[§ 37 Prüfungen](#37)

[§ 38 Praktische Ausbildung](#38)

### Unterabschnitt 5  
Bildungsgänge der Fachoberschule und den Erwerb der Fachhochschulreife

[§ 39 Leistungsbewertung](#39)

[§ 40 Fachpraktische Ausbildung](#40)

[§ 41 Nachprüfung](#41)

[§ 42 Wiederholung](#42)

[§ 43 Beschlussfassung](#43)

[§ 44 Durchführung der schriftlichen Prüfungen](#44)

[§ 44a Mündliche Prüfung](#44a)

Abschnitt 5  
Schlussvorschriften
---------------------------------

[§ 45 Inkrafttreten, Außerkrafttreten](#45)

Abschnitt 1  
Allgemeine Vorschriften
-------------------------------------

#### § 1 Geltungsbereich

(1) Diese Verordnung gilt, wenn der Präsenzunterricht in der Schule regional oder für einzelne Klassen wegen schwerwiegender Gründe, die nicht nur vorübergehend gegeben sind, nicht stattfinden kann.
Auf der Grundlage dieser Verordnung wird das Recht aller Schülerinnen und Schüler auf schulische Bildung und individuelle Förderung gemäß § 3 Absatz 1 des Brandenburgischen Schulgesetzes weiterhin gewährleistet.

(2) Ein schwerwiegender Grund liegt vor, wenn

1.  die Schule auf Anordnung der Gesundheitsbehörden geschlossen wird,
2.  die Schülerinnen und Schüler den Präsenzunterricht auf Grund des Infektionsschutzgesetzes oder anderer Rechtsvorschriften nicht besuchen dürfen oder
3.  auf Grund anderer erheblicher Notfälle Präsenzunterricht im Schulgebäude nicht stattfinden kann.

(3) Die Feststellung, ob der Präsenzunterricht nicht nur vorübergehend wegen schwerwiegender Gründe ausgesetzt ist, trifft das für Schule zuständige Ministerium.
Soweit diese Feststellung nicht erfolgt und der Präsenzunterricht nur vorübergehend eingeschränkt ist, gilt § 2 Absatz 2 und 3 entsprechend.

(4) Soweit durch diese Verordnung keine besonderen Regelungen getroffen werden, finden die für den jeweiligen besuchten Bildungsgang geltenden Rechts- und Verwaltungsvorschriften Anwendung.

#### § 2 Unterrichtsformen

(1) Zur Sicherung des Bildungs- und Erziehungsauftrages des Staates haben die Schulen Unterricht als Lehr- und Lernveranstaltung einer Gruppe von Schülerinnen und Schülern mit mindestens einer Lehrkraft an einem gemeinsamen Ort zu organisieren (Präsenzunterricht).

(2) Sofern der Präsenzunterricht nicht an einem gemeinsamen Ort oder nur eingeschränkt an einem gemeinsamen Ort stattfinden kann, wird der Unterricht so erteilt, dass die gemeinsame Lehr- und Lernveranstaltung durch die Nutzung von digitalen Medien oder Telefonkonferenzen an unterschiedlichen Orten, insbesondere im häuslichen Bereich stattfindet (Distanzunterricht).

(3) Die in Absatz 1 und 2 beschriebenen Unterrichtsformen sind, soweit die Einschränkungen des Schulbetriebs dies zulassen, so miteinander zu verbinden, dass Lernprozesse gemäß den geltenden Rahmenlehrplänen und Stundentafeln gesichert und die Pflichten aus dem Schulverhältnis erfüllt werden können.

#### § 3 Organisation des Distanzunterrichts

(1) Die Schulen erarbeiten ein Konzept für den Distanzunterricht, gemäß den Vorgaben des für Schule zuständigen Ministeriums.
Die Verbindung zum Präsenzunterricht ist besonders zu berücksichtigen.

(2) Die Schulen können eigene und vom Landesinstitut für Schule und Medien Berlin-Brandenburg bereitgestellte didaktische Unterrichts- und Übungsmaterialien für den Distanzunterricht, die eine Einführung neuer Lerngegenstände, die Erarbeitung neuer Themen und die Weiterentwicklung von Kompetenzen der Schülerinnen und Schüler gewährleisten, verwenden.
Ein planmäßiger gesteuerter Lernprozess zwischen der Lehrkraft und den Schülerinnen und Schülern ist sicherzustellen.

(3) Die Eltern haben dafür Sorge zu tragen, dass ihr Kind der Pflicht zur Teilnahme am Distanzunterricht nachkommt.

Abschnitt 2  
Bildungsgangübergreifende Vorschriften
----------------------------------------------------

#### § 4 Rahmenlehrplan und Stundentafel

(1) Das für Schule zuständige Ministerium kann die sich aus den Rahmenlehrplänen ergebenden Maßgaben zum Kompetenzerwerb und zu den zu vermittelnden Themen und Inhalten, insbesondere Abweichungen vom Kerncurriculum, näher bestimmen.

(2) Die Stundentafel wird durch den Präsenz- und Distanzunterricht abgedeckt.

(3) In den beruflichen Bildungsgängen kann der Präsenz- oder Distanzunterricht für das Fach Sport durch alternative Distanzlernangebote, wie Anleitungen zu praktischen Sportübungen ersetzt werden.

(4) Das für Schule zuständige Ministerium kann für die jeweiligen Bildungsgänge Abweichungen von der geltenden Stundentafel nur festlegen, wenn alle anderen Maßnahmen zur Abdeckung des Unterrichts und zur Sicherstellung der schulischen Abschlüsse ausgeschöpft sind.

#### § 5 Grundsätze der Leistungsbewertung

(1) Die Leistungsbewertung bezieht sich auf die im Präsenz- und Distanzunterricht vermittelten Kenntnisse, Fähigkeiten und Fertigkeiten, die durch das Lernen im häuslichen Bereich vertieft wurden.
Eine abschließende Leistungsbewertung ergibt sich aus den erbrachten Leistungen im Präsenz- und Distanzunterricht.
Wenn die Grundsätze der Leistungsbewertung nicht gewährleistet werden können, erfolgt keine abschließende Leistungsbewertung.

(2) Die Leistungsfeststellung und -bewertung für Schülerinnen und Schüler im Distanzunterricht kann

1.  mittels Telefon- oder Videokonferenzen,
2.  an einem anderen Ort außerhalb der Schule oder
3.  im häuslichen Bereich

stattfinden.

(3) Leistungen im Distanzunterricht gehen in die abschließende Leistungsbewertung ein, wenn gewährleistet ist, dass die Leistung ohne Unterstützung durch Dritte erbracht wurde.
Soweit dies nicht sichergestellt werden kann, wird die Leistung im Rahmen der Gewichtung der erreichten Noten gegenüber allen sonstigen Noten berücksichtigt.
Soweit eine technische Störung die Leistungsbewertung nicht zulässt, ist sie zu einem späteren Zeitpunkt zu wiederholen.

(4) Die abschließende Leistungsbewertung zum Ende des Schuljahres berücksichtigt die Leistungen und die Leistungsentwicklung der Schülerin oder des Schülers im gesamten Schuljahr.

(5) Das Nähere zur Leistungsbewertung regeln Verwaltungsvorschriften.

(6) Das für Schule zuständige Ministerium kann entscheiden, ob die zentralen Orientierungsarbeiten durchgeführt werden.

#### § 6 Bewertung des Arbeits- und Sozialverhaltens

Das Sozialverhalten wird nicht bewertet, wenn der Unterricht der Schülerin oder des Schülers im Schulhalbjahr überwiegend als Distanzunterricht durchgeführt wurde.
Davon unberührt bleibt die Bewertung des Arbeitsverhaltens.

#### § 7 Prüfungen

(1) Die Durchführung von Prüfungen und Klausuren finden als Präsenzveranstaltung statt, soweit nach den Abschnitten 3 und 4 keine anderen Bestimmungen gelten.
Schülerinnen und Schüler sind verpflichtet, an den Prüfungen und Klausuren auch außerhalb des Ortes der Schule teilzunehmen.

(2) Für die schriftlichen Prüfungen kann die Prüfungsdauer verlängert und der Prüfungstermin verschoben werden.
Die Entscheidung trifft das für Schule zuständige Ministerium.

(3) Soweit das regionale oder lokale Infektionsgeschehen dazu geführt hat, dass die Schülerinnen und Schüler in dem Schuljahr überwiegend im Distanzunterricht unterrichtet wurden und dadurch der Kompetenzerwerb entsprechend des Rahmenlehrplans nicht erreicht werden konnte, entscheidet das für Schule zuständige Ministerium, ob die Prüfungen durchgeführt oder zu einem späteren Zeitpunkt nachgeholt werden.
Dies gilt auch für Schülerinnen und Schüler, die aus gesundheitlichen Gründen überwiegend im Distanzunterricht waren.

(4) Können Prüfungen nicht nachgeholt werden, wird die Abschlussnote aus dem Durchschnitt der im Schuljahr erbrachten Leistungsbewertungen im jeweiligen Prüfungsfach gebildet.
Dies gilt auch für Komplexprüfungen sowie für praktisch zu erbringende Prüfungsleistungen, die nicht durchgeführt werden können.

(5) Soweit das landesweite Infektionsgeschehen die Durchführung der Prüfungen in den beruflichen Bildungsgängen nicht zulässt, gelten die Abschnitt 3 und 4 entsprechend.

#### § 8 Lehrkräfte

Können für die Abnahme von Prüfungen oder für Aufgabenstellungen nicht die Lehrkräfte eingesetzt werden, die zuvor die Klasse oder die zu prüfende Schülerin oder den zu prüfenden Schüler unterrichtet haben, kann dafür eine andere geeignete Lehrkraft eingesetzt werden.

#### § 9 Probezeit

Eine Probezeit gilt auch dann als bestanden, wenn die erforderliche Leistungsbewertung zur Feststellung des Bestehens der Probezeit nicht erfolgen konnte.

#### § 10 Versetzungen

Ist insbesondere auf Grund von eingeschränkten technischen Möglichkeiten und langfristigem Distanzunterricht eine Leistungsbewertung im Schuljahr nur eingeschränkt möglich oder besteht eine ausgeprägte Differenz zu dem bisherigen Leistungsstand der Schülerin oder des Schülers wird nach pflichtgemäßen Ermessen über eine Versetzung entschieden.

#### § 11 Wiederholung

Schülerinnen und Schüler können das Schuljahr freiwillig wiederholen, um ihre Noten für das Übergangsverfahren oder für den Schulabschluss in diesem Schuljahr zu verbessern.
Über den Antrag der Schülerin oder des Schülers oder der Eltern auf freiwillige Wiederholung entscheidet das zuständige staatliche Schulamt.
Das staatliche Schulamt kann dem Antrag grundsätzlich nur zustimmen, soweit die Kapazitäten gegeben sind.
Dabei ist auch zu prüfen, ob an einer anderen Schule die Wiederholung ermöglicht werden kann.

#### § 12 Verweildauer

Eine Verlängerung der Verweildauer wird nicht auf die Höchstverweildauer angerechnet.

#### § 13 Mitwirkungsrechte in der Schule

Die Mitwirkungsrechte gemäß Teil 7 und 12 des Brandenburgischen Schulgesetzes sind zu sichern.
Soweit notwendig sind die Beratungen zeitlich und örtlich zu verschieben oder digital oder fernmündlich durchzuführen.

#### § 14 Ausschüsse

Mitglieder von Prüfungs- und Fachausschüssen gelten auch als anwesend, wenn sie mittels Videokonferenz an den Sitzungen teilnehmen.

#### § 15 Fristen und Termine

Können Fristen und Termine zu pädagogischen Festlegungen und Entscheidungen oder Informationen an Dritte gemäß der jeweiligen Bildungsgangverordnung nicht eingehalten werden, legt die Schule im Einvernehmen mit der zuständigen Schulaufsicht eine andere Frist oder einen anderen Termin fest.

#### § 16 Zuhörende

Zuhörende sind bei Prüfungen ausgeschlossen.

#### § 17 Beschlüsse

Gefasste Beschlüsse der schulischen Gremien sind aufzuheben und neu zu fassen, soweit auf Grund des Distanzunterrichts eine andere, insbesondere pädagogische Bewertung getroffen werden muss.

Abschnitt 3  
Bildungsgangbezogene Vorschriften der Grundschule und der weiterführenden allgemeinbildenden Schulen sowie der Schulen des Zweiten Bildungsweges
--------------------------------------------------------------------------------------------------------------------------------------------------------------

### Unterabschnitt 1  
Bildungsgang der Grundschule und Bildungsgänge der Sekundarstufe I

#### § 18 Schulärztliche Untersuchungen

Liegen die Ergebnisse der schulärztlichen Untersuchungen jeweils bis zum 31. Juli nicht vor, werden die Schülerinnen und Schüler, soweit die übrigen Voraussetzungen vorliegen, auch ohne schulärztliche Untersuchung an ihrer Grundschule aufgenommen.

#### § 18a Aufnahme in eine Leistungs- und Begabungsklasse

(1) Kann auf Grund des Infektionsgeschehens der prognostische Test nicht als Präsenzveranstaltung oder durch eine Videokonferenz durchgeführt werden, bestimmt sich das Auswahlverfahren für die Aufnahme in eine Leistungs- und Begabungsklasse nach der Empfehlung der Grundschule, den Schulhalbjahresnoten der Jahrgangsstufe 4 sowie nach der Bewertung des Eignungsgesprächs.
Dabei umfasst die Bewertung des Eignungsgesprächs auch die prognostische Einschätzung der Eignung.
Bei einer Übernachfrage werden die fachübergreifenden Kompetenzen, die Neigungen und Begabungen besonders gewichtet.
Soweit Schülerinnen und Schüler gleich geeignet sind, entscheidet das Los.

(2) Das Eignungsgespräch kann auch als Videokonferenz stattfinden.

#### § 18b Unterrichtsorganisation in der Sekundarstufe&nbsp;I

Werden Schülerinnen und Schüler außerhalb des Klassen- oder Kursverbandes unterrichtet, können diese auch in jahrgangsstufenübergreifenden Lerngruppen zusammengefasst werden.

#### § 19 Prüfungen in der Jahrgangsstufe 10

(1) Das für Schule zuständige Ministerium entscheidet bei einem landesweiten erhöhten Infektionsgeschehen, ob die schriftlichen Prüfungen oder die mündliche Fremdsprachenprüfung am Ende der Jahrgangsstufe 10 entfallen.
In diesem Fall sind die Jahresnoten die Abschlussnoten.

(2) Entfällt im Fach Englisch die schriftliche oder die mündliche Fremdsprachenprüfung wird die Abschlussnote im Verhältnis von drei zu zwei aus der Jahresnote und dem Ergebnis der schriftlichen oder mündlichen Prüfung ermittelt.

(3) Die Schule entscheidet in Anbetracht des Infektionsgeschehens, ob die mündliche Fremdsprachenprüfung als Einzelprüfung stattfindet.

(4) Soweit ein Fach in der Jahrgangsstufe 10 nicht unterrichtet wurde, werden mangelhafte Leistungen (Note 5) und ungenügende Leistungen (Note 6) aus dem Jahreszeugnis der Jahrgangsstufe 9 abweichend von § 14 Absatz 1 Satz 3 und 4 der Sekundarstufe I-Verordnung nicht auf das Zeugnis der Jahrgangsstufe 10 übertragen und fließen nicht in die Berechnung der am Ende der Jahrgangsstufe 10 erworbenen Abschlüsse ein.

### Unterabschnitt 2  
Bildungsgang der gymnasialen Oberstufe

#### § 20 Unterrichtsorganisation

In der gymnasialen Oberstufe kann die Wochenstundenzahl in jedem Leistungskurs um eine Wochenstunde durch die Reduzierung einer Wochenstunde in den nicht prüfungsrelevanten Grundkursen erhöht werden.
Davon ausgenommen sind die Fächer Deutsch, Mathematik und Fremdsprache auf Grundkursniveau.

#### § 21 Klausuren und andere Bewertungsbereiche

(1) Klausuren, die in den Zeitraum des Distanzunterrichts fallen, können jeweils durch eine schriftliche Arbeit im häuslichen Bereich, die fachlich und zeitlich mit einer Klausur in der jeweiligen Jahrgangsstufe vergleichbar ist, ersetzt werden, soweit das Unterrichtsfach dazu geeignet ist.
In den Fächern, in denen aus praktischen oder technischen Gründen keine schriftliche Arbeit im häuslichen Bereich angefertigt werden kann, entfällt die Verpflichtung zur Anfertigung der schriftlichen Arbeit im häuslichen Bereich und kann durch eine mündliche Leistungsüberprüfung ersetzt werden.
Hiervon ausgenommen sind Klausuren gemäß § 12 Absatz 4 der Gymnasiale-Oberstufe-Verordnung und Klausuren der Abiturprüfung.

(2) Die mündliche Leistungsfeststellung in mindestens einer fortgeführten Fremdsprache gemäß § 12 Absatz 3 der Gymnasiale-Oberstufe-Verordnung kann als Einzelprüfung oder im Rahmen einer Videokonferenz als Einzel- oder Gruppenprüfung durchgeführt werden.
Hierüber entscheidet die Jahrgangskonferenz.

(3) Die Anzahl und die Dauer der Klausuren im jeweiligen Schuljahr werden durch Verwaltungsvorschriften geregelt.

#### § 22 Mündliche Abiturprüfung

(1) Bei einem erhöhten Infektionsgeschehen kann der Fachausschuss entscheiden, ob die mündliche Abiturprüfung als Videokonferenz durchgeführt wird.
Sollte wegen einer technischen Störung die Videokonferenz nicht durchgeführt werden können, ist die Prüfung zu wiederholen.

(2) Soweit die oder der zu Prüfende physisch anwesend ist, muss mindestens auch ein Mitglied des Fachausschusses physisch anwesend sein.

### Unterabschnitt 3  
Bildungsgänge im Zweiten Bildungsweg

#### § 23 Klausuren

Klausuren, die in den Zeitraum des Distanzunterrichts fallen, können jeweils durch eine schriftliche Arbeit im häuslichen Bereich, die fachlich und zeitlich mit einer Klausur in der jeweiligen Jahrgangsstufe vergleichbar sind, ersetzt werden, soweit das Unterrichtsfach dazu geeignet ist.
In den Fächern, in denen aus praktischen oder technischen Gründen keine schriftliche Arbeit im häuslichen Bereich angefertigt werden kann, entfällt die Verpflichtung zur Anfertigung der schriftlichen Arbeit im häuslichen Bereich und kann durch eine mündliche Leistungsüberprüfung ersetzt werden.
Hiervon ausgenommen sind Klausuren gemäß § 24 Absatz 7 Satz 2 der ZBW-Verordnung und Klausuren der Abiturprüfung.

Abschnitt 4  
Berufliche Bildungsgänge
--------------------------------------

### Unterabschnitt 1  
Bildungsgang der Berufsfachschule Soziales

#### § 24 Leistungsbewertung

(1) Die schriftliche Klassenarbeit in einem Schulhalbjahr kann durch eine schriftliche Arbeit im häuslichen Bereich ersetzt werden.

(2) Können Leistungsnachweise für praktische Tätigkeiten wegen einer vorübergehenden Schließung der Einrichtung nicht erbracht werden, können diese bis eine Woche vor Beginn des nächsten Schuljahres nachgeholt werden.

(3) Die Halbjahres- oder Jahresnote in einem Fach oder einem Lernfeld kann auch durch andere geeignete Lehrkräfte festgesetzt werden.
Die Beauftragung erfolgt durch die Schulleiterin oder den Schulleiter.

(4) Beschlüsse zur Gewichtung der einzelnen Noten sind durch die Fach- oder Lernbereichskonferenzen zu überprüfen und anzupassen.

#### § 25 Versetzungen

Schülerinnen und Schüler werden auch versetzt, wenn ein erfolgreicher Abschluss des jeweiligen praktischen Ausbildungsabschnitts nicht nachgewiesen werden kann und dies nicht durch die Schülerin oder den Schüler zu vertreten ist.

#### § 26 Zulassung zur Prüfung

(1) Die Zulassung zur Prüfung kann auch erfolgen, wenn die praktischen Ausbildungsabschnitte wegen Schließung der Einrichtung nicht vollständig absolviert werden konnten und auch keine weitere Einrichtung für die praktische Ausbildung zur Verfügung stand.

(2) Absatz 1 gilt auch, wenn das geeignete Verfahren gemäß § 39 Absatz 5 der Berufsfachschulverordnung Soziales nicht durchgeführt werden kann.

#### § 27 Prüfung

(1) Der Fachprüfungsausschuss kann auch aus einer Fachprüferin oder einem Fachprüfer, die oder der gleichzeitig den Vorsitz führt, sowie einer Lehrkraft zur Protokollführung gebildet werden.

(2) Die Abschlussprüfung kann in den ersten drei Monaten nach Unterrichtsbeginn des neuen Schuljahres wiederholt werden.

(3) Bei einem erhöhten Infektionsgeschehen kann der Prüfungsausschuss entscheiden, ob die mündliche Prüfung als Videokonferenz durchgeführt wird.
Sollte wegen einer technischen Störung die Videokonferenz nicht durchgeführt werden können, ist die Prüfung zu wiederholen.

(4) Soweit die oder der zu Prüfende physisch anwesend ist, muss mindestens auch ein Mitglied des Prüfungsausschusses physisch anwesend sein.

#### § 28 Praktische Ausbildung

(1) Soweit die für die Durchführung einer praktischen Ausbildung bestimmten Zeiten aufgrund der Schließung der Einrichtungen nicht erbracht werden konnten, werden diese auf die zu erbringenden Zeitstunden gemäß § 35 der Berufsfachschulverordnung Soziales angerechnet.

(2) Können geeignete Verfahren gemäß § 39 Absatz 5 der Berufsfachschulverordnung Soziales nicht durchgeführt werden, sind diese auf der Grundlage der bereits erbrachten Leistungen zu beenden.

(3) Abweichend von § 38 Absatz 6 der Berufsfachschulverordnung Soziales kann die Begleitung der praktischen Ausbildung durch die Lehrkraft mittels Telefon- oder Videokonferenz erfolgen.

### Unterabschnitt 2  
Bildungsgang der Berufsfachschule zur Erlangung eines Berufsabschlusses nach Landesrecht

#### § 29 Leistungsbewertung

(1) Die schriftliche Klassenarbeit in einem Schulhalbjahr kann durch eine schriftliche Arbeit im häuslichen Bereich ersetzt werden.

(2) Können Leistungsnachweise für praktische Tätigkeiten wegen einer vorübergehenden Schließung der Einrichtung nicht erbracht werden, können diese bis eine Woche vor Beginn des nächsten Schuljahres nachgeholt werden.

(3) Die Halbjahres- oder Jahresnote in einem Fach oder einem Lernfeld kann auch durch andere geeignete Lehrkräfte festgesetzt werden.
Die Beauftragung erfolgt durch die Schulleiterin oder den Schulleiter.

(4) Beschlüsse zur Gewichtung der einzelnen Noten sind durch die Fach- oder Lernbereichskonferenzen zu überprüfen und anzupassen.

#### § 30 Wiederholung

In begründeten Fällen kann die Jahrgangsstufe ein zweites Mal wiederholt werden.
Dies gilt auch, wenn nach einer Wiederholung des zweiten Schuljahres erneut keine Zulassung zur Abschlussprüfung erfolgt.

#### § 30a Praktikum und Zulassung zur Prüfung

Soweit die praktische Ausbildung gemäß § 17 der Berufsfachschulverordnung auf Grund der Schließung der Einrichtung oder Nichtaufnahme von Praktikantinnen und Praktikanten nicht oder nicht vollständig erbracht werden konnte, werden diese Zeiten auf die zu erbringenden Praktikumszeiten angerechnet.
Die Zulassung zur Prüfung und der Erwerb des Abschlusszeugnisses stehen dem nicht entgegen.

### Unterabschnitt 3  
Bildungsgänge der Fachschule für Technik und Wirtschaft

#### § 31 Leistungsbewertung

(1) Die schriftliche Klassenarbeit in einem Schulhalbjahr kann durch eine schriftliche Arbeit im häuslichen Bereich ersetzt werden.

(2) Die Halbjahres- oder Jahresnote in einem Fach oder einem Lernfeld kann auch durch andere geeignete Lehrkräfte festgesetzt werden.
Die Beauftragung erfolgt durch die Schulleiterin oder den Schulleiter.

(3) Beschlüsse zur Gewichtung der einzelnen Noten sind durch die Fach- oder Lernbereichskonferenzen zu überprüfen und anzupassen.

#### § 32 Wiederholung

In begründeten Fällen kann eine Jahrgangsstufe ein zweites Mal wiederholt werden.
Dies gilt auch, wenn nach einer Wiederholung des letzten Schuljahres erneut keine Zulassung zur Abschlussprüfung erfolgt.

#### § 32a Mündliche Prüfung

(1) Bei einem erhöhten Infektionsgeschehen kann der Prüfungsausschuss entscheiden, ob die mündliche Prüfung als Videokonferenz durchgeführt wird.
Sollte wegen einer technischen Störung die Videokonferenz nicht durchgeführt werden können, ist die Prüfung zu wiederholen.

(2) Soweit die oder der zu Prüfende physisch anwesend ist, muss mindestens auch ein Mitglied des Prüfungsausschusses anwesend sein.

### Unterabschnitt 4  
Bildungsgänge für Sozialwesen in der Fachschule

#### § 33 Leistungsbewertung

(1) Die schriftliche Klassenarbeit in einem Schulhalbjahr kann durch eine schriftliche Arbeit im häuslichen Bereich ersetzt werden.

(2) Können Leistungsnachweise für praktische Tätigkeiten wegen einer vorübergehenden Schließung der Einrichtung nicht erbracht werden, können diese bis eine Woche vor Beginn des nächsten Schuljahres nachgeholt werden.

(3) Die Halbjahres- oder Jahresnote in einem Fach oder einem Lernfeld kann auch durch andere geeignete Lehrkräfte festgesetzt werden.
Die Beauftragung erfolgt durch die Schulleiterin oder den Schulleiter.

(4) Beschlüsse zur Gewichtung der einzelnen Noten sind durch die Fach- oder Lernbereichskonferenzen zu überprüfen und anzupassen.

#### § 34 Versetzungen

Schülerinnen und Schüler werden auch versetzt, wenn ein erfolgreicher Abschluss des jeweiligen praktischen Ausbildungsabschnitts nicht nachgewiesen werden kann und dies nicht durch die Schülerin oder den Schüler zu vertreten ist.

#### § 35 Wiederholung

In begründeten Fällen kann die Jahrgangsstufe ein zweites Mal wiederholt werden.
Dies gilt auch, wenn nach einer Wiederholung des zweiten Schuljahres erneut keine Zulassung zur Abschlussprüfung erfolgt.

#### § 36 Zulassung zur Prüfung

(1) Die Zulassung zur Prüfung kann auch erfolgen, wenn die praktischen Ausbildungsabschnitte wegen Schließung der Einrichtung nicht vollständig absolviert werden konnten und auch keine weitere Einrichtung für die praktische Ausbildung zur Verfügung stand.

(2) Absatz 1 gilt auch, wenn das geeignete Verfahren gemäß § 44 Absatz 5 der Fachschulverordnung Sozialwesen nicht durchgeführt werden kann.

#### § 37 Prüfungen

(1) Der Fachprüfungsausschuss kann auch aus einer Fachprüferin oder einem Fachprüfer, die oder der gleichzeitig den Vorsitz führt, sowie einer Lehrkraft zur Protokollführung gebildet werden.

(2) Abweichend von § 20 der Fachschulverordnung Sozialwesen kann die Frist zur Vorlage des Prüfungsablaufplans auf eine Woche verkürzt werden.

(3) Die Abschlussprüfung kann in den ersten drei Monaten nach Unterrichtsbeginn des neuen Schuljahres wiederholt werden.

(4) Bei einem erhöhten Infektionsgeschehen kann der Prüfungsausschuss entscheiden, ob die mündliche Prüfung als Videokonferenz durchgeführt wird.
Sollte wegen einer technischen Störung die Videokonferenz nicht durchgeführt werden können, ist die Prüfung zu wiederholen.

(5) Soweit die oder der zu Prüfende physisch anwesend ist, muss mindestens auch ein Mitglied des Prüfungsausschusses anwesend sein.

#### § 38 Praktische Ausbildung

(1) Soweit die für die Durchführung einer praktischen Ausbildung bestimmten Zeiten aufgrund der Schließung der Einrichtungen nicht erbracht werden konnten, werden diese auf die zu erbringenden Zeitstunden gemäß § 38 der Fachschulverordnung Sozialwesen angerechnet.

(2) Können geeignete Verfahren gemäß § 44 Absatz 5 der Fachschulverordnung Sozialwesen nicht durchgeführt werden, sind diese auf der Grundlage der bereits erbrachten Leistungen zu beenden.

(3) Abweichend von § 36 Absatz 6 der Fachschulverordnung Sozialwesen kann die Begleitung der praktischen Ausbildung durch die Lehrkräfte mittels Telefon- oder Videokonferenz erfolgen.

### Unterabschnitt 5  
Bildungsgänge der Fachoberschule und den Erwerb der Fachhochschulreife

#### § 39 Leistungsbewertung

(1) Die schriftliche Arbeit im jeweiligen Schulhalbjahr kann durch einen anderen Bewertungsbereich ersetzt werden.
Dieser ist durch die Fachkonferenz für alle Schülerinnen und Schülern der Schule gleichermaßen festzulegen und durch die Schulleitung zu bestätigen.
Satz 1 gilt nicht für die Fächer, in denen die schriftliche Fachhochschulreifeprüfung abgelegt wird.

(2) Kann die im letzten Schulhalbjahr in den Fächern der schriftlichen Prüfung geforderte schriftliche Arbeit, die den Bedingungen der Fachhochschulreifeprüfung entspricht, aus zeitlichen Gründen nicht durchgeführt werden, entfällt diese.
Es ist stattdessen eine schriftliche Arbeit ohne Prüfungsbedingungen anzufertigen.
Die Entscheidung trifft die Schulleitung.

#### § 40 Fachpraktische Ausbildung

(1) Abweichend von § 17 Absatz 1 der Fachoberschul- und Fachhochschulreifeverordnung kann bei Verlust eines Praktikumsplatzes ein neuer Praktikumsplatz innerhalb von vier Wochen nachgewiesen werden.
Ist dieser Zeitraum nicht ausreichend, kann alternativ eine nicht fachrichtungsbezogene Praktikumsstelle nachgewiesen werden, soweit die sonstigen Bedingungen erfüllt sind.

(2) Schülerinnen und Schüler im zweijährigen Bildungsgang werden auch versetzt, wenn ein erfolgreicher Abschluss des jeweiligen praktischen Ausbildungsabschnitts nicht nachgewiesen werden kann und dies nicht durch die Schülerin oder den Schüler zu vertreten ist.

(3) Abweichend von § 38 Absatz 6 der Fachoberschul- und Fachhochschulreifeverordnung kann die Begleitung der praktischen Ausbildung durch die Lehrkräfte mittels Telefon- oder Videokonferenz erfolgen.

#### § 41 Nachprüfung

Die Schule kann abweichend von § 12 Absatz 4 und 5 der Fachoberschul- und Fachhochschulreifeverordnung entscheiden, dass die Nachprüfung nur aus einer schriftlichen oder nur aus einer mündlichen Prüfung besteht.
Wenn keine mündliche Prüfung erfolgt, erhält das Zeugnis das Datum des Ausgabetages.

#### § 42 Wiederholung

In begründeten Fällen kann eine Jahrgangsstufe ein zweites Mal wiederholt werden.
Dies gilt auch, wenn nach einer Wiederholung des letzten Schuljahres erneut keine Zulassung zur Fachhochschulreifeprüfung erfolgt.

#### § 43 Beschlussfassung

(1) Der Prüfungsausschuss ist beschlussfähig, wenn mindestens die Hälfte seiner Mitglieder anwesend sind.
Die Fachausschüsse sind beschlussfähig, wenn mindestens drei Viertel aller Mitglieder anwesend sind.

(2) Der Fachausschuss kann aus einer Fachprüferin oder einem Fachprüfer, die oder der gleichzeitig den Vorsitz führt, sowie einer Lehrkraft zur Protokollführung gebildet werden.

#### § 44 Durchführung der schriftlichen Prüfungen

Die zuständige Schulrätin oder der zuständige Schulrat kann entscheiden, dass für die dezentralen schriftlichen Prüfungsfächer nur ein Aufgabenvorschlag eingereicht wird.

#### § 44a Mündliche Prüfung

(1) Bei einem erhöhten Infektionsgeschehen kann der Prüfungsausschuss entscheiden, ob die mündliche Prüfung als Videokonferenz durchgeführt wird.
Sollte wegen einer technischen Störung die Videokonferenz nicht durchgeführt werden können, ist die Prüfung zu wiederholen.

(2) Soweit die oder der zu Prüfende physisch anwesend ist, muss auch mindestens ein Mitglied des Prüfungsausschusses physisch anwesend sein.

Abschnitt 5  
Schlussvorschriften
---------------------------------

#### § 45 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
August 2020 in Kraft und mit Ablauf des 31. Juli 2022 außer Kraft.

Potsdam, den 17.
November 2020

Die Ministerin für Bildung,  
Jugend und Sport

Britta Ernst