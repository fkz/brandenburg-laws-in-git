## Verordnung zur Übertragung von Befugnissen zum Erlass von Kosten nach § 8 des Brandenburgischen Justizkostengesetzes (Kostenerlassübertragungsverordnung - KostErlÜV)

Auf Grund des § 8 Absatz 3 Satz 2 des Brandenburgischen Justizkostengesetzes vom 3.
Juni 1994 (GVBl.
I S.
172), der durch Artikel 1 Nummer 8 des Gesetzes vom 26.
Oktober 2010 (GVBl.
I Nr.
33) geändert worden ist, in Verbindung mit § 6 Absatz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S.
186), der durch Artikel 2 Nummer 8 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr.
28) geändert worden ist, und auf Grund des § 15 Absatz 2 Satz 1 und des § 34 Absatz 2 Satz 1 des Arbeitsgerichtsgesetzes in der Fassung der Bekanntmachung vom 2.
Juli 1979 (BGBl.
I S.
853, 1036), die zuletzt durch Artikel 1 des Gesetzes vom 30.
März 2000 (BGBl.
I S.
333) geändert worden sind, in Verbindung mit § 1 Nummer 2 der Justiz-Zuständigkeitsübertragungsverordnung vom 9.
April 2014 (GVBl.
II Nr.
23) verordnet der Minister der Justiz unter Beachtung von Artikel 9 Absatz 3 des Staatsvertrages über die Errichtung gemeinsamer Fachobergerichte der Länder Berlin und Brandenburg vom 26.
April 2004 (GVBl.
I S.
281):

### § 1  
Stundung, Erlass und Einwendungen

(1) Zuständig für die Stundung und den Erlass der in § 8 Absatz 1 Nummer 1 und 3 des Brandenburgischen Justizkostengesetzes genannten Gerichtskosten und Ansprüche ist bis zu einem Betrag von 6 000 Euro aus Verfahren, die in erster Instanz bei

1.  dem Amtsgericht Potsdam anhängig sind oder waren, die Präsidentin oder der Präsident des Amtsgerichts Potsdam,
    
2.  den übrigen Amtsgerichten anhängig sind oder waren, die oder der für das jeweilige Amtsgericht örtlich zuständige Präsidentin oder Präsident des Landgerichts,
    
3.  den Landgerichten anhängig sind oder waren, die jeweilige Präsidentin oder der jeweilige Präsident des Landgerichts,
    
4.  dem Brandenburgischen Oberlandesgericht anhängig sind oder waren, die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts.
    

(2) Zuständig für die Stundung und den Erlass der in § 8 Absatz 1 Nummer 2 und 4 des Brandenburgischen Justizkostengesetzes genannten Kosten und Ansprüche bis zu einem Betrag von 6 000 Euro ist für die bei

1.  dem Amtsgericht Potsdam entstandenen Ansprüche die Präsidentin oder der Präsident des Amtsgerichts Potsdam,
    
2.  den übrigen Amtsgerichten entstandenen Ansprüche die Präsidentin oder der Präsident des jeweils örtlich zuständigen Landgerichts,
    
3.  den Landgerichten entstandenen Ansprüche die jeweilige Präsidentin oder der jeweilige Präsident des Landgerichts,
    
4.  den Staatsanwaltschaften entstandenen Ansprüche die Präsidentin oder der Präsident des Landgerichts, in dessen Bezirk die Staatsanwaltschaft liegt,
    
5.  dem Brandenburgischen Oberlandesgericht sowie der Generalstaatsanwaltschaft des Landes Brandenburg entstandenen Ansprüche die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts.
    

(3) Zuständig für die Stundung und den Erlass der in § 8 Absatz 1 des Brandenburgischen Justizkostengesetzes genannten Ansprüche bei Beträgen über 6 000 Euro bis zu 12 000 Euro ist die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts.

(4) Über Einwendungen gegen die Entscheidung der Präsidentin oder des Präsidenten des Amtsgerichts Potsdam und der Präsidentinnen oder Präsidenten der Landgerichte entscheidet die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts.
Über Einwendungen gegen eine Entscheidung nach Absatz 1 Nummer 4, Absatz 2 Nummer 5 und Absatz 3 entscheidet das für Justiz zuständige Ministerium.

(5) Der Rechtsweg bleibt unberührt.

### § 2  
Verwaltungsgerichte, Finanzgericht, Arbeits- und Sozialgerichte

(1) Für den Bereich der Verwaltungsgerichtsbarkeit, der Finanzgerichtsbarkeit sowie der Arbeits- und Sozialgerichtsbarkeit gelten die Bestimmungen des § 1 Absatz 1 bis 3 entsprechend.
An die Stelle der Präsidentin oder des Präsidenten des Brandenburgischen Oberlandesgerichts treten entsprechend der Gerichtsbarkeit die Präsidentin oder der Präsident des Oberverwaltungsgerichts Berlin-Brandenburg, des Finanzgerichts Berlin-Brandenburg, des Landesarbeitsgerichts Berlin-Brandenburg und des Landessozialgerichts Berlin-Brandenburg.
An die Stelle der Präsidentinnen oder der Präsidenten der Landgerichte und des Amtsgerichts Potsdam treten die Präsidentinnen oder Präsidenten der Verwaltungsgerichte und die Direktorinnen oder Direktoren der Arbeits- und Sozialgerichte.

(2) Gegen eine Entscheidung nach Absatz 1 ist der Verwaltungsrechtsweg gegeben.
Vor Klageerhebung ist ein Vorverfahren gemäß § 68 der Verwaltungsgerichtsordnung (Widerspruchsverfahren) durchzuführen.
Die Zuständigkeit zum Erlass eines Widerspruchsbescheids richtet sich nach § 73 Absatz 1 Satz 2 Nummer 1 und 2 der Verwaltungsgerichtsordnung.
Über einen Widerspruch gegen eine Entscheidung der Präsidentin oder des Präsidenten des Oberverwaltungsgerichts Berlin-Brandenburg, des Landesarbeitsgerichts Berlin-Brandenburg, des Finanzgerichts Berlin-Brandenburg oder des Landessozialgerichts Berlin-Brandenburg entscheidet die Präsidentin oder der Präsident dieses Gerichts.

### § 3  
Zusammentreffen von Zuständigkeiten

(1) Treffen innerhalb derselben Gerichtsbarkeit unterschiedliche Zuständigkeiten für den Antrag auf Stundung oder Erlass der in § 8 Absatz 1 des Brandenburgischen Justizkostengesetzes genannten Ansprüche einer Kostenschuldnerin oder eines Kostenschuldners zu, so entscheidet diejenige Präsidentin oder Direktorin oder derjenige Präsident oder Direktor über den Antrag, die oder der für die Stundung oder den Erlass des höchsten Betrags zuständig wäre.
Eine Addition der Ansprüche findet nicht statt.

(2) Sofern ein Antrag auf Stundung oder Erlass Verfahren unterschiedlicher Gerichtsbarkeiten betrifft, entscheidet die Präsidentin oder Direktorin oder der Präsident oder Direktor der jeweiligen Gerichtsbarkeit über die dort entstandenen Ansprüche selbst.

### § 4  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Kostenerlassübertragungsverordnung vom 21.
Februar 1996 (GVBl.
II S.
230), die zuletzt durch die Verordnung vom 15.
September 2009 (GVBl.
II S.
655) geändert worden ist, außer Kraft.

Potsdam, den 24.
Juli 2014

Der Minister der Justiz

Dr.
Helmuth Markov