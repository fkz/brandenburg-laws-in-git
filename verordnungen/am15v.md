## Verordnung zur Herabsetzung des Mindestalters für die Fahrerlaubnisklasse AM (AM15-Verordnung - AM15V)

Auf Grund des § 6 Absatz 5a Satz 1 des Straßenverkehrsgesetzes in der Fassung der Bekanntmachung vom 5.
März 2003 (BGBl.
I S. 310, 919), der durch Artikel 1 des Gesetzes vom 5. Dezember 2019 (BGBl.
I S.
2008) eingefügt worden ist, verordnet die Landesregierung:

#### § 1 Herabsetzung des Mindestalters

Das Mindestalter für die Fahrerlaubnisklasse AM wird auf 15 Jahre herabgesetzt.

#### § 2 Inkrafttreten

Diese Verordnung tritt am 1.
Mai 2020 in Kraft.

Potsdam, den 26.
März 2020

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Minister für Infrastruktur und Landesplanung

Guido Beermann