## Verordnung zur Erhebung von Verwaltungsgebühren in den Bereichen Land- und Forstwirtschaft sowie Jagd (GebOLandw)

Auf Grund des § 3 Absatz 1 des Gebührengesetzes für das Land Brandenburg vom 7. Juli 2009 (GVBl. I S. 246) verordnet der Minister für Infrastruktur und Landwirtschaft:

#### § 1   
Gebührentarif

Für die in den Anlagen 1 und 2 genannten Amtshandlungen werden die dort genannten Verwaltungsgebühren erhoben.

#### § 2   
Mehrfache Amtshandlungen

Zur Abgeltung mehrfacher Amtshandlungen, die denselben Schuldner und dieselbe Tarifstelle betreffen, können die Gebühren für einen im Voraus zu bestimmenden Zeitraum von höchstens einem Jahr auf Antrag pauschal festgesetzt werden.

#### § 3   
Gebührenbemessung

 (1) Soweit Gebühren nach dem erforderlichen Zeitaufwand zu berechnen sind, sind der Gebührenberechnung als Stundensätze zugrunde zu legen:

1.  für Beamtinnen oder Beamte des höheren Dienstes und vergleichbare Beschäftigte                                                                                          80,00 EUR
    
2.  für Beamtinnen oder Beamte des gehobenen Dienstes und vergleichbare Beschäftigte                                                                                          60,00 EUR
    
3.  für Beamtinnen oder Beamte des mittleren Dienstes und vergleichbare Beschäftigte                                                                                          48,00 EUR
    
4.  für Beamtinnen oder Beamte des einfachen Dienstes und vergleichbare Beschäftigte                                                                                          38,00 EUR.
    

Bei der Ermittlung der Zeitgebühren ist die Zeit anzusetzen, die unter regelmäßigen Verhältnissen von einer entsprechend ausgebildeten Fachkraft benötigt wird.
Die Zeit für Ortsbesichtigungen, einschließlich der Anreise und Abreise, ist einzurechnen.

(2) Soweit die Leistungen nach dieser Gebührenordnung umsatzsteuerpflichtig sind, wird zu der Gebühr die Umsatzsteuer in der jeweils gesetzlichen Höhe hinzugerechnet.

#### § 4   
Gebühren in besonderen Fällen

Für Amtshandlungen der Flurbereinigungsbehörde bei der Führung des amtlichen Verzeichnisses der Grundstücke und bei der Wahrnehmung von Aufgaben der Landesvermessung findet die Gebührenordnung für das amtliche Vermessungswesen im Land Brandenburg in der jeweils geltenden Fassung Anwendung.

#### § 5   
Inkrafttreten, Außerkrafttreten

(1) Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig tritt die Verordnung zur Erhebung von Verwaltungsgebühren in den Bereichen Land- und Forstwirtschaft sowie Jagd vom 17. Juli 2007 (GVBl. II S. 314), die zuletzt durch Verordnung vom 15. Mai 2012 (GVBl. II Nr. 38) geändert worden ist, außer Kraft.

Potsdam, den 11.
Juli 2014

Der Minister für Infrastruktur und Landwirtschaft

Jörg Vogelsänger

* * *

**Anlage 1**  
(zu § 1)

### Allgemeine Gebühren

Inhaltsübersicht (Überschriften in Kurzform)

1

Allgemeine Gebühren

1.1

Beglaubigungen, Bescheinigungen und Urkunden, Zeugnisse

1.2

Anfertigung von Zweitschriften, Kopien, Computerausdrucken

1.3

Vervielfältigung von Karten

1.4

Nutzung von Diensträumen

1.5

Akteneinsicht

1.6

Sonstiges

Gebührentarif

Tarifstelle

Gegenstand

Gebühr (EUR)

**1**

**Allgemeine Gebühren**

**1.1**

**Beglaubigungen, Bescheinigungen und Urkunden, Zeugnisse**

1.1.1

Beglaubigungen von Unterschriften oder Handzeichen

2,5 bis 25

1.1.2

Beglaubigungen von Abschriften, Ablichtungen usw., je Seite

1 bis 2,5

1.1.3

sonstige Bescheinigungen, Urkunden

1 bis 51

1.1.4

Zeugnisse (z. B.
Ursprungszeugnisse)

1 bis 25

**1.2**

**Anfertigung von Zweitschriften, Kopien, Computerausdrucken – soweit nicht § 9 Nummer 2 GebGBbg Anwendung findet**

1.2.1

für die ersten 50 Seiten DIN-A4, schwarz-weiß, je Seite

0,5

1.2.2

jede weitere Seite

0,15

1.2.3

für Seiten im Format DIN-A3, je Seite

1

1.2.4

Farbkopien, je Seite

1 bis 5

1.2.5

Erstellung von Fotodokumentationen oder Lichtbildmappen

nach Aufwand,  
mindestens aber 10

1.2.6

Überlassung von elektronischen Daten

je Datei 2,5,  
je Vorgang  
höchstens 41

**1.3**

**Vervielfältigung von Karten**

1.3.1

als Schwarz-Weiß-Kopie

DIN-A4

DIN-A3

DIN-A2

DIN-A1

DIN-A0

0,5

1

2

4

8

1.3.2

als Farbkopie oder Computerausdruck

bis DIN-A3

größer als DIN-A3

5

8

**1.4**

**Nutzung von Diensträumen, je angefangene Stunde**

Anfallende Reinigungskosten sind gesondert in Rechnung zu stellen.
Für die Nutzung von technischen Geräten sind je nach Ausstattung und Grad der Beanspruchung weitere Zuschläge zu erheben.

15,5

**1.5**

**Akteneinsicht (nach § 29 VwVfG in Verbindung mit § 1 Absatz 1 VwVfGBbg, Übermittlung von Informationen)**

1.5.1

Übersendung einer in Papierform geführten Akte im Original auf Antrag

12

1.5.2

Übersendung einer elektronisch geführten Akte in Papierform auf Antrag

12, zuzüglich der Gebühr nach Tarifstelle 1.2

1.5.3

Übersendung einer elektronisch geführten Akte als Datei auf Antrag

entsprechend Tarifstelle 1.2.6

**1.6**

**Sonstiges**

1.6.1

Einsatz von Dienstkraftfahrzeugen für Dritte

Gebühr richtet sich hinsichtlich der Fahrkosten je Kilometer nach den Sätzen gemäß Anlage 5 zu Nummer 10.3 der Dienstkraftfahrzeug-Richtlinie

1.6.2

Rechtsbehelfe

1.6.2.1

bei Drittwidersprüchen

26 bis 1 023

1.6.2.2

bei Widersprüchen gegen Kostenentscheidungen

Gebühr richtet sich nach § 18 Absatz 3 GebGBbg

1.6.2.3

bei Widerspruch durch den Adressaten der Sachentscheidung

Gebühr richtet sich nach § 18 Absatz 1 GebGBbg

1.6.3

Entscheidung über die aufschiebende Wirkung von Rechtsbehelfen, soweit nicht mit der Hauptentscheidung verbunden

26 bis 2 556

1.6.4

sonstige Amtshandlungen

1.6.4.1

Amtshandlungen, für die keine andere Tarifstelle vorgesehen ist und die nicht einem von der handelnden Behörde wahrzunehmenden besonderen Interesse dienen

0 bis 1 000

**Anlage 2**  
(zu § 1)

#### Gebühren für die Bereiche Land- und Forstwirtschaft sowie Jagd

Inhaltsübersicht (Überschrift in Kurzform)

**1**

**Ökologischer Landbau**

1.1

Amtshandlungen nach der Verordnung (EG) Nr. 834/2007 des Rates vom 28. Juni 2007 über die ökologische/biologische Produktion und die Kennzeichnung von ökologischen/biologischen Erzeugnissen und zur Aufhebung der Verordnung (EWG) Nr. 2092/91

1.2

Verordnung (EG) Nr. 889/2008 der Kommission vom 5. September 2008 mit Durchführungsvorschriften zur Verordnung (EG) Nr. 834/2007

**2**

**Tierzucht und -haltung**

2.1

Anerkennung von Zuchtverbänden und Zuchtunternehmen und Zustimmung zu Änderungen

2.2

Genehmigung von Zuchtprogrammen und Zustimmung zu wesentlichen Änderungen

2.3

Erlaubniserteilung für den Betrieb einer Besamungsstation

2.4

Erlaubniserteilung für den Betrieb einer Embryo-Entnahmeeinheit

2.5

Anerkennung von Ausbildungsstätten an denen Lehrgänge oder Kurzlehrgänge über künstliche Besamung oder Lehrgänge über Embryotransfer durchgeführt werden

2.6

Ausstellung einer Bescheinigung über die erfolgreiche Teilnahme an einem Lehrgang für künstliche Besamung oder Embryotransfer

2.7

Feststellung der Gleichwertigkeit von Qualifikationen für reglementierte Berufe nach dem Berufsqualifikationsfeststellungsgesetz (BQFG)

2.8

Amtshandlungen nach dem Rennwett- und Lotteriegesetz (RennwLottG)

**3**

**Pflanzenschutz**

3.1

Prüfung von Pflanzenschutzmitteln

3.2

phytosanitäre Bearbeitung von pflanzlichen Sendungen im innergemeinschaftlichen Handel sowie bei der Ausfuhr und Einfuhr in bzw.
aus Drittländern

3.3

Warndienst

3.4

Sachkunde im Pflanzenschutz

3.5

Pflanzenschutztechnik

3.6

Erteilung von Genehmigungen zur Ausbringung und Anwendung von Pflanzenschutzmitteln

3.7

Einfuhrkontrolle von Sendungen mit Pflanzenschutzmitteln

3.8

Anzeige von Anwendung, Inverkehrbringen und Einfuhr von Pflanzenschutzmitteln zu gewerblichen Zwecken

3.9

Inspektion einer Prüfeinrichtung Pflanzenschutz im Anerkennungsverfahren Gute Experimentelle Praxis (GEP)

**4**

**Saatenanerkennung, phytopathologische Diagnostik**

4.1

nach der Saatgutverordnung (SaatgutV)

4.2

nach der Pflanzkartoffelverordnung (PflKartV)

4.3

phytopathologische Prüfungen von Pflanzen, Pflanzenteilen und Pflanzenerzeugnissen sowie von Erden, Substraten und Wasser

**5**

**Waldrechtliche Angelegenheiten**

5.1

Bereitstellung von Walddaten und Forstkarten

5.2

Verwaltungsentscheidungen nach dem Waldgesetz des Landes Brandenburg (LWaldG)

5.3

Entscheidung über die Genehmigung einer Satzungsänderung eines anerkannten forstwirtschaftlichen Zusammenschlusses nach § 33 Absatz 2 BGB und § 32 Absatz 4 LWaldG

5.4

Entscheidung über die Genehmigung eines Antrages des forstwirtschaftlichen Zusammenschlusses zur Auflösung nach § 41 BGB und § 32 Absatz 4 LWaldG

5.5

Gebühren für Amtshandlungen nach dem Forstvermehrungsgutgesetz (FoVG)

5.6

Durchführung einer Waldbewertung von Amts wegen

**6**

**Jagdrechtliche Angelegenheiten**

6.1

Jägerprüfung, Falknerprüfung, Jagdaufseherprüfung

6.2

Jagdscheine

6.3

Jagdbezirke

6.4

Jagdausübung

6.5

sonstige jagdliche Amtshandlungen, Genehmigungen, Bestätigungen

**7**

**Amtshandlungen nach dem Verbraucherinformationsgesetz (VIG)**

7.1

Erteilung mündlicher und einfacher schriftlicher Auskünfte

7.2

Erteilung schriftlicher Auskünfte

7.3

Herausgabe von Unterlagen und Duplikaten

7.4

Gewährung von Akteneinsicht

7.5

Auslagen

**8**

**Weinrechtliche Angelegenheiten**

8.1

Erteilung einer amtlichen Prüfungsnummer

8.2

Amtshandlungen nach der Verordnung (EG) Nr.
884/2001 der Kommission vom 24.
April 2001 mit Durchführungsbestimmungen zu den Begleitdokumenten für die Beförderung von Weinbauerzeugnissen und zu den Ein- und Ausgangsbüchern im Weinsektor

**9**

**Sachverständigenwesen**

9.1

Antragsgebühr

9.2

Bestellungsgebühr

9.3

Wiederbestellung früherer Sachverständiger

9.4

Gebühr für die Durchführung der Sachkundenachweise bei der erstmaligen Bestellung für ein Fachgebiet

9.5

Gebühr für die öffentliche Bestellung als Probenehmer

9.6

Gebühr für die Verlängerung der öffentlichen Bestellung als Probenehmer

**10**

**Gebühren für die Abnahme von Prüfungen und sonstige Angelegenheiten nach dem Berufsbildungsgesetz (BBiG)**

10.1

Berufsabschlussprüfungen (außer Regelerstausbildung)

10.2

Prüfungen gemäß Ausbilder-Eignungsverordnung (AEVO)

10.3

Fortbildungsprüfungen

10.4

Wiederholung von Prüfungen

10.5

Anerkennung der Gleichwertigkeit von Berufsabschlüssen nach § 4 BBiG und von Fortbildungsprüfungen nach den §§ 53 und 54 BBiG

10.6

Feststellung und Anerkennung der Gleichwertigkeit für im Ausland erworbener Berufsqualifikation nach dem Berufsqualifikationsgesetz (BQFG)

**11**

**Zulassung oder Anerkennung als private Kontrollstelle nach § 134 des Markengesetzes (MarkenG) sowie Zulassung als private Kontrollstelle nach § 5 Satz 2 des Lebensmittelspezialitätengesetzes (LSpG)**

11.1

Erstzulassung oder Erstanerkennung von privaten Kontrollstellen zum Schutz von geografischen Angaben und Ursprungsbezeichnungen für Agrarerzeugnisse und Lebensmittel nach dem Markengesetz mit Sitz in Brandenburg

11.2

Erstzulassung oder Erstanerkennung von privaten Kontrollstellen über Bescheinigung besonderer Merkmale von Agrarerzeugnissen und Lebensmitteln nach dem Lebensmittelspezialitätengesetz mit Sitz in Brandenburg

11.3

Kontrollen beim erfassten Marktteilnehmer gemäß Verordnung (EG) Nr. 510/2006 des Rates vom 20. März 2006 zum Schutz von geografischen Angaben und Ursprungsbezeichnungen bei Agrarerzeugnissen und Lebensmitteln in Verbindung mit dem Markengesetz sowie Kontrollen beim erfassten Marktteilnehmer gemäß Verordnung (EG) Nr.
509/2006 des Rates vom 20. März 2006 über die garantiert traditionellen Spezialitäten bei Agrarerzeugnissen und Lebensmitteln in Verbindung mit dem Lebensmittelspezialitätengesetz

11.4

Amtshandlungen nach der Verordnung (EU) Nr.
1151/2012 des Europäischen Parlaments und des Rates vom 21.
November 2012 über Qualitätsregelungen für Agrarerzeugnisse und Lebensmittel (ABI.
L 343 vom 14.12.2012, S.
19)

**12**

**Amtshandlungen nach dem Agrarmarktstrukturgesetz (AgrarMSG)**

12.1

Prüfung eines Antrages auf Anerkennung einer Erzeugerorganisation oder ihrer Vereinigung sowie von Branchenverbänden nach AgrarMSG

12.2

Prüfung eines Antrages auf Verleihung der Rechtsfähigkeit nach § 22 BGB an eine Erzeugerorganisation oder ihre Vereinigung nach AgrarMSG

**13**

**Fischerei**

13.1

Amtshandlungen nach dem Fischereigesetz für das Land Brandenburg (BbgFischG)

13.2

Amtshandlungen nach der Fischereiordnung des Landes Brandenburg (BbgFischO)

13.3

Amtshandlungen nach der Verordnung über die Anglerprüfung

**14**

**Amtshandlungen nach dem Einkommensteuergesetz (EStG)**

14.1

Erstellung einer Bescheinigung nach § 14a Absatz 3 Nummer 2 EStG

14.2

Anerkennung von Betriebsgutachten im Sinne von § 68 Absatz 2 der Einkommensteuer-Durchführungsverordnung (EStDV)

**15**

**Amtshandlungen nach dem Düngegesetz (DüngG)**

15.1

Erteilung einer Anordnung nach § 13 DüngG

15.2

Anerkennung eines Trägers der Qualitätssicherung im Bereich von Wirtschaftsdüngern nach § 13a DüngG

15.3

Zulassung anderer Methoden und Verfahren zur Ermittlung des Düngebedarfs nach § 4 Absatz 1 der Düngeverordnung (DüV)

15.4

Erteilung einer Genehmigung zur Nutzung anderer Verfahren für die Aufbringung von flüssigen organischen oder organisch-mineralischen Düngemitteln nach § 6 Absatz 3 DüV

15.5

Genehmigung einer Ausnahme von den Vorgaben zur Ausbringung von flüssigen organischen oder organisch-mineralischen Düngemitteln nach § 6 Absatz 3 DüV

15.6

Genehmigung einer Ausnahme für das Aufbringen höherer Mengen von Wirtschaftsdüngern tierischer Herkunft nach § 6 Absatz 5 DüV

15.7

Genehmigung einer Ausnahme für das Aufbringen höherer Mengen von flüssigen organischen oder organisch-mineralischen Düngemitteln, bei denen es sich um Gärrückstände aus dem Betrieb einer Biogasanlage handelt, auf Ackerland mit mehrjährigem Futterbau, Grünland oder Dauergrünland nach § 6 Absatz 6 DüV

15.8

Erteilung einer Genehmigung zur Verschiebung des Verbotszeitraums nach § 6 Absatz 10 DüV

15.9

Genehmigung einer Ausnahme von den Verbotszeiträumen bei der Aufbringung von Düngemitteln mit einem festgestellten Gehalt an Trockenmasse von weniger als 2 Prozent nach § 6 Absatz 10 DüV

15.10

Anerkennung von Düngeberatern nach § 9 Absatz 4 DüV und § 6 Absatz 5 Stoffstrombilanzverordnung

15.11

Anordnung zur Teilnahme an einer anerkannten Düngeberatung nach § 9 Absatz 4 DüV

**16**

**Amtshandlungen nach den Verordnungen der gemeinsamen Marktorganisation (GMO)**

16.1

Amtshandlungen Obst/Gemüse gemäß Delegierte Verordnung (EU) 2017/891

16.2

Qualitätskontrolle Sektor Obst und Gemüse gemäß Durchführungsverordnung (EU) Nr.
543/2011

16.3

Amtshandlungen nach der gemeinsamen Marktorganisation Sektor Eier gemäß der Verordnung (EU) Nr.
1308/2013

16.4

gemeinsame Marktorganisation Sektor Käse

16.5

gemeinsame Marktorganisation Sektor Butter

16.6

gemeinsame Marktorganisation Sektor Fleisch

16.7

Kontrollen nach dem Fleischgesetz (FlG) für die Einreihung von Fleisch in Handelsklassen und Gewichtsfeststellung auf Anforderung

16.8

Erteilung einer Erlaubnis zum Betrieb eines milchwirtschaftlichen Unternehmens gemäß § 4 des Milch- und Margarinegesetzes (MilchMargG)

16.9

Amtshandlungen nach der gemeinsamen Marktorganisation Sektor Geflügelfleisch gemäß der Verordnung (EU) Nr. 1308/2013

16.10

Amtshandlungen nach dem Legehennenbetriebsregistergesetz (LegRegG)

**17**

**Amtshandlungen nach dem Holzhandels-Sicherungs-Gesetz (HolzSiG)**

**18**

**Vollzug allgemeiner umweltrechtlicher Vorschriften**

Gebührentarif

Tarifstelle

Gegenstand

Gebühr (EUR)

**1**

**Ökologischer Landbau**

**1.1**

**Amtshandlungen nach der Verordnung (EG) Nr. 834/2007 des Rates vom 28. Juni 2007 über die ökologische/biologische Produktion und die Kennzeichnung von ökologischen/biologischen Erzeugnissen und zur Aufhebung der Verordnung (EWG) Nr. 2092/91 in der jeweils geltenden Fassung**

1.1.1

Feststellung einer Unregelmäßigkeit und daraus folgende Anordnung nach Artikel 30 Absatz 1 Satz 1

50 bis 1 000

1.1.2

Untersagung der Vermarktung von Erzeugnissen mit einem Bezug auf die ökologische/biologische Produktion nach Artikel 30 Absatz 1 Satz 2

100 bis 2 000

**1.2**

**Verordnung (EG) Nr.
889/2008 der Kommission vom 5. September 2008 mit Durchführungsvorschriften zur Verordnung (EG) Nr. 834/2007 in der jeweils geltenden Fassung**

1.2.1

Erhöhung der Prozentsätze nichtökologischer/nichtbiologischer Tiere nach Artikel 9 Absatz 4

20 bis 250

1.2.2

Genehmigung von Eingriffen bei Tieren nach Artikel 18 Absatz 1

20 bis 500

1.2.3

Gestattung der Parallelproduktion in Aquakultur nach Artikel 25c Absatz 1 oder 2

20 bis 300

1.2.4

Zulassung der Verwendung von natürlichen Farben und natürlichen Überzugstoffen für saisonales dekoratives Färben von Eiern nach Artikel 27 Absatz 4

20 bis 300

1.2.5

Anerkennung von Umstellungszeiträumen nach Artikel 36 Absatz 2 Buchstabe a oder Buchstabe b

je Einzelfall  
20 bis 300

1.2.6

Verlängerung des Umstellungszeitraums nach Artikel 36 Absatz 3

20 bis 300

1.2.7

Verkürzung des Umstellungszeitraums nach Artikel 36 Absatz 4

20 bis 300

1.2.8

Verkürzung des Umstellungszeitraums nach Artikel 37 Absatz 2

20 bis 500

1.2.9

rückwirkende Anerkennung von Umstellungszeiträumen in der Aquakultur nach Artikel 38a Absatz 2

20 bis 300

1.2.10

Genehmigung der Anbindung von Rindern in Kleinbetrieben nach Artikel 39

20 bis 500

1.2.11

Genehmigung der Parallelerzeugung nach Artikel 40 Absatz 2

20 bis 300

1.2.12

Genehmigung der Einstellung von Geflügel nach Artikel 42

20 bis 1 000

1.2.13

Genehmigung Verwendung von Saatgut nach Artikel 45 Absatz 1 Buchstabe b

20 bis 500

1.2.14

Genehmigung einzelner Maßnahmen in Katastrophenfällen nach Artikel 47 Buchstabe a, b, c oder Buchstabe d

je Einzelfall  
20 bis 300

1.2.15

befristete Aufrechterhaltung ökologischer Aquakulturen nach Artikel 95 Absatz 11 Satz 1

50

1.2.16

Verwendung naturidentischer Vitamine A, D und E nach Anhang VI Ziffer 3 Buchstabe a 3. Spiegelstrich

50

1.2.17

Verwendung von Natriumnitrit nach Anhang VIII Abschnitt A Fußnote 1

50

Anmerkung zu den Tarifstellen 1.2.1 bis 1.2.17  
  
Die Gebührenpflicht umfasst auch die Ablehnung der beantragten Amtshandlung sowie die Aufhebung von Anerkennungen, Genehmigungen, Prüfungsergebnissen, Zulassungen oder Zustimmungen.

**2**

**Tierzucht und -haltung**  
  
Amtshandlungen nach der Verordnung (EU) 2016/1012 des Rates vom 8. Juni 2016 über die Tierzucht- und Abstammungsbestimmungen für die Zucht, den Handel und die Verbringung in die Union von reinrassigen Zuchttieren und Hybridzuchtschweinen sowie deren Zuchtmaterial in Verbindung mit dem Tierzuchtgesetz (TierZG)

**2.1**

**Anerkennung von Zuchtverbänden und Zuchtunternehmen und Zustimmung zu Änderungen**

2.1.1

Anerkennung eines Zuchtverbandes  nach § 4 TierZG in Verbindung mit Artikel 4 der Verordnung (EU) 2016/1012

100 bis 1 600

2.1.2

Anerkennung eines Zuchtunternehmens nach  § 4 TierZG in Verbindung mit Artikel 4 der Verordnung (EU) 2016/1012

100 bis 2 600

2.1.3

Entfristung oder Verlängerung einer befristeten Anerkennung eines Zuchtverbandes oder eines Zuchtunternehmens nach § 7 TierZG in Verbindung mit Artikel 4 der Verordnung (EU) 2016/1012

100 bis 1 400

2.1.4

Zustimmung zur Änderung der Angaben und Anforderungen nach § 4 Absatz 4 TierZG in Verbindung mit Anhang I Teil I der Verordnung (EU) 2016/1012

100 bis 1 100

**2.2**

**Genehmigung von Zuchtprogrammen und Zustimmung zu wesentlichen Änderungen nach § 5 TierZG in Verbindung mit Artikel 8 und 9 der Verordnung (EU) 2016/1012**

2.2.1

Genehmigung von Zuchtprogrammen nach § 5 TierZG in Verbindung mit Artikel 8 der Verordnung (EU) 2016/1012

100 bis 1 600

2.2.2

Zustimmung zu wesentlichen Änderungen nach § 5 TierZG in Verbindung mit Artikel 9 der Verordnung (EU) 2016/1012

100 bis 1 100

2.2.3

Entfristung der Genehmigung eines Zuchtprogrammes nach § 7 TierZG in Verbindung mit Artikel 8 der Verordnung (EU) 2016/1012

100 bis 1 400

**2.3**

**Erlaubniserteilung für den Betrieb einer Besamungsstation**

2.3.1

Erteilung, Neuerteilung oder Verlängerung einer Erlaubnis für den Betrieb einer Besamungsstation nach § 18 TierZG

2.3.1.1

für Rinder, Schweine und Pferde

500 bis 1 600

2.3.1.2

für Schafe und Ziegen

100 bis 300

2.3.2

Erteilung der Erlaubnis zur Gewinnung von Samen außerhalb einer Besamungsstation nach § 14 Absatz 3 Satz 3 TierZG

2.3.2.1

für Rinder, Schweine und Pferde

150 bis 450

2.3.2.2

für Schafe und Ziegen

50

**2.4**

**Erlaubniserteilung für den Betrieb einer Embryo-Entnahmeeinheit**

2.4.1

Erteilung, Neuerteilung oder Verlängerung einer Erlaubnis für den Betrieb einer Embryo-Entnahmeeinheit nach § 18 TierZG

150 bis 550

**2.5**

**Anerkennung von Ausbildungsstätten an denen Lehrgänge oder Kurzlehrgänge über künstliche Besamung oder Lehrgänge über Embryotransfer durchgeführt werden**

2.5.1

Anerkennung einer Ausbildungsstätte nach § 19 Absatz 1 Nummer 2 TierZG

100 bis 300

2.5.2

Erlaubniserteilung zur Durchführung von Lehrgängen nach § 19 Absatz 1 Nummer 2 TierZG

50 bis 100

**2.6**

**Ausstellung einer Bescheinigung über die erfolgreiche Teilnahme an einem Lehrgang für künstliche Besamung nach § 15 Absatz 2 und für Embryotransfer nach § 17 Absatz 1 TierZG**

2.6.1

für Besamungsbeauftragte

10

2.6.2

für Eigenbestandsbesamer oder Embryonentransferberechtigung

5

**2.7**

**Feststellung der Gleichwertigkeit von Qualifikationen für reglementierte Berufe nach dem Berufsqualifikationsfeststellungsgesetz (BQFG)**

50

**2.8**

**Amtshandlungen nach dem Rennwett- und Lotteriegesetz (RennwLottG)**

2.8.1

Erteilung einer Erlaubnis als Buchmacher nach § 2 Absatz 1 RennwLottG

100 bis 600, für ein Jahr  
mindestens 100

2.8.2

Erteilung einer Erlaubnis für einen Buchmachergehilfen

30 bis 300, für ein Jahr  
mindestens 30

2.8.3

Ausfertigung einer Zulassungsurkunde innerhalb des Zeitraums, auf den sich die Erlaubnis erstreckt

30

2.8.4

Erteilung der Erlaubnis zum Betrieb eines Totalisators, je Renntag

20

2.8.5

Erteilung der Erlaubnis zur Annahme von Wetten für Rennen außerhalb der Rennbahn durch den Rennverein

25 bis 200

2.8.6

Änderung oder Ergänzung der Erlaubnis zum Betrieb eines Totalisators

20 bis 100

**3**

**Pflanzenschutz**

**3.1**

**Prüfung von Pflanzenschutzmitteln**

3.1.1

Mittel für den Ackerbau

3.1.1.1

Fungizide

3.1.1.1.1

Saatgutbehandlungsmittel

3.1.1.1.1.1

im Freiland gegen

3.1.1.1.1.1.1

Brandkrankheiten an Getreide

880

3.1.1.1.1.1.2

Streifenkrankheit an Getreide

880

3.1.1.1.1.1.3

Typhula-Fäule an Getreide

880

3.1.1.1.1.1.4

Schwarzbeinigkeit an Getreide

1 200

3.1.1.1.1.1.5

sonstige Pilzkrankheiten an Getreide (Frühbefall)

960

3.1.1.1.1.1.6

Auflaufkrankheiten, insbesondere Rhizoctonia solani an Kartoffeln

1 480

3.1.1.1.1.2

im Gewächshaus gegen

3.1.1.1.1.2.1

Fusarium nivale, Fusarium culmorum, Septoria nodorum an Getreide

640

3.1.1.1.1.2.2

Auflaufkrankheiten bei Rüben, Raps, Mais, Leguminosen

640

3.1.1.1.1.3

Prüfung von Saatgutbehandlungsmitteln

3.1.1.1.1.3.1

Prüfung des Einflusses von Saatgutbehandlungsmitteln auf die Triebkraft ohne Lagerung

360

3.1.1.1.1.3.2

Prüfung des Einflusses von Saatgutbehandlungsmitteln auf die Triebkraft mit Lagerung

720

3.1.1.1.1.3.3

künstliche Infektion von Krankheiten an Getreide

320

3.1.1.1.2

Spritzmittel gegen

3.1.1.1.2.1

Halmbasiserkrankungen an Getreide

1 120

3.1.1.1.2.2

Blattkrankheiten und Ährenkrankheiten an Getreide

960

3.1.1.1.2.3

Echten Mehltau, Rostkrankheiten, Cercospora, Ramularia an Rüben

960

3.1.1.1.2.4

Phytophtora, Alternaria an Kartoffeln (bis sechs Behandlungen)

1 480

3.1.1.1.2.5

Phytophtora, Alternaria an Kartoffeln (bis sechs Behandlungen), gesonderte Pflanzung

2 120

3.1.1.1.2.6

Krankheiten an Raps (Phoma, Alternaria, Sclerotinia)

1 120

3.1.1.1.2.7

Botrytis Ascochyta an Leguminosen

880

3.1.1.1.2.8

Krankheiten an Sonnenblumen

880

3.1.1.1.2.9

Krankheiten an Mais

1 040

3.1.1.2

Insektizide

3.1.1.2.1

Saatgutbehandlungsmittel

880

3.1.1.2.2

Spritzmittel

3.1.1.2.2.1

in Hackfrüchten gegen

3.1.1.2.2.1.1

beißende Insekten

960

3.1.1.2.2.1.2

saugende Insekten

1 600

3.1.1.2.2.1.3

Blattläuse zur Verhinderung von Virusinfektionen an Kartoffeln einschließlich Gesundheitsprüfung auf zwei Virusarten

3 400

3.1.1.2.2.1.4

Blattläuse zur Verhinderung von Virusinfektionen an Rüben

1 800

3.1.1.2.2.1.5

Moosknopfkäfer

1 600

3.1.1.2.2.1.6

Rübenfliege

1 000

3.1.1.2.2.1.7

Rübenblattwanze

1 080

3.1.1.2.2.1.8

Collembolen

1 080

3.1.1.2.2.1.9

Drahtwurm

2 000

3.1.1.2.2.2

im Mais und Getreide gegen

3.1.1.2.2.2.1

beißende Insekten

1 200

3.1.1.2.2.2.2

saugende Insekten

1 600

3.1.1.2.2.2.3

Blattläuse zur Verhinderung von Virusinfektionen

1 800

3.1.1.2.2.2.4

Weizengallmücke

2 160

3.1.1.2.2.2.5

Brachfliege

2 000

3.1.1.2.2.2.6

Drahtwurm in Getreide und Mais

2 000

3.1.1.2.2.2.7

Maiszünsler

1 560

3.1.1.2.2.2.8

Fritfliege

1 360

3.1.1.2.2.2.9

Tipula-Larven an Mais

1 360

3.1.1.2.2.3

in Ölpflanzen und Faserpflanzen gegen

3.1.1.2.2.3.1

Rapserdfloh

2 600

3.1.1.2.2.3.2

andere Erdfloharten

960

3.1.1.2.2.3.3

Rapsglanzkäfer

1 640

3.1.1.2.2.3.4

Stängelschädlinge am Raps

1 640

3.1.1.2.2.3.5

Schotenschädlinge am Raps

1 800

3.1.1.2.2.3.6

Rübsenblattwespe, Kohlmotte und Gammaeule

1 360

3.1.1.2.2.3.7

Kleine Kohlfliege an Raps

1 360

3.1.1.2.2.3.8

Blattläuse

1 600

3.1.1.2.2.4

in Leguminosen gegen

3.1.1.2.2.4.1

saugende Insekten

1 600

3.1.1.2.2.4.2

Blattrandkäfer

1 200

3.1.1.2.2.4.3

Erbsenwickler an Erbsen

1 600

3.1.1.3

Herbizide in

3.1.1.3.1

Getreide und Mais

840

3.1.1.3.2

Kartoffeln

840

3.1.1.3.3

Futter- und Zuckerrüben

1 040

3.1.1.3.4

Ölpflanzen und Faserpflanzen

840

3.1.1.3.5

Körnerleguminosen

840

3.1.1.3.6

Feldfutterpflanzen einschließlich Gräsern zur Futternutzung bzw.
zum Samenbau

920

3.1.1.3.7

Unkrautbekämpfung zwischen Anbauperioden von Kulturen

1 200

3.1.1.3.8

Herbizide gegen spezielle Schadpflanzen

920

3.1.1.4

Wachstumsregler zur

3.1.1.4.1

Ertragsbeeinflussung bei

3.1.1.4.1.1

Getreide und Ölfrüchten

880

3.1.1.4.1.2

Mais

1 320

3.1.1.4.1.3

Rüben und anderen Blattfrüchten

1 320

3.1.1.4.2

Halmfestigung im

3.1.1.4.2.1

Getreide

880

3.1.1.4.2.2

Mais

880

3.1.1.4.2.3

Sonnenblumen

880

3.1.1.4.2.4

Raps, einschließlich der Beeinflussung des Pflanzenaufbaus

880

3.1.1.4.3

Wuchshemmung von Gräsern

880

3.1.1.4.4

Krautabtötung von Kartoffeln zur Verhinderung der Virusabwanderung einschließlich Gesundheitsprüfung

2 000

3.1.1.4.5

Krautabtötung bei Kartoffeln ohne Knollenbonitur

840

3.1.1.4.6

Ernteerleichterung einschließlich Unkrautbekämpfung

1 680

3.1.1.4.7

Keimhemmung bei Kartoffeln

880

3.1.1.4.8

Brechung der Keimruhe bei Kartoffeln

840

3.1.1.4.9

Entblätterung im Samenbau

800

3.1.1.4.10

Abreifebeschleunigung in Ölfrüchten und Leguminosen

840

3.1.2

Mittel für das Grünland

3.1.2.1

Insektizide gegen Tipula-Larven und andere Bodeninsekten

2 800

3.1.2.2

Herbizide

1 080

3.1.2.3

Herbizide zur Grünlanderneuerung

1 200

3.1.3

Mittel für den Gemüsebau

3.1.3.1

Fungizide gegen

3.1.3.1.1

Auflaufkrankheiten bei Gemüsesaatgut

760

3.1.3.1.2

sonstige Schadpilze, max.
fünf Behandlungen bzw.
Bonituren

1 400

3.1.3.2

Insektizide gegen

3.1.3.2.1

beißende und saugende Insekten

1 600

3.1.3.2.2

bodenbürtige Insekten

1 240

3.1.3.2.3

Thripse an Freilandkulturen

2 000

3.1.3.3

Akarizide

1 920

3.1.3.4

Herbizide

1 040

3.1.3.5

Mittel in Champignonkulturen

1 800

3.1.4

Mittel für den Obstbau

3.1.4.1

Fungizide gegen

3.1.4.1.1

Kragenfäule an Apfel, Obstbaumkrebs und andere Rindenerkrankungen (zweijährige Prüfung)

1 760

3.1.4.1.2

Schorf an Kernobst

2 120

3.1.4.1.3

Echten Mehltau an Kernobst

1 760

3.1.4.1.4

Echten Mehltau an Beerenobst

1 240

3.1.4.1.5

Rostpilze an Kernobst

1 480

3.1.4.1.6

Rostpilze an Steinobst

1 240

3.1.4.1.7

Kräuselkrankheit am Pfirsich

1 240

3.1.4.1.8

Monilinia laxa, Monilinia fructigena und Sprühfleckenkrankheit an Kirsche

1 240

3.1.4.1.9

Botrytis an Erdbeeren

1 480

3.1.4.1.10

Lederfäule und Rhizomfäule an Erdbeeren

1 520

3.1.4.1.11

Lagerfäule und Lagerschorf

1 760

3.1.4.1.12

sonstige Pilzkrankheiten an Kernobst

1 480

3.1.4.1.13

sonstige Pilzkrankheiten an Steinobst und Beerenobst

1 240

3.1.4.2

Insektizide gegen

3.1.4.2.1

beißende und saugende Insekten

1 040

3.1.4.2.1.1

beißende und saugende Insekten in einer Prüfung

1 320

3.1.4.2.2

Blutläuse

1 080

3.1.4.2.3

Schildläuse

3.1.4.2.3.1

San-Jose-Schildlaus (Sommerspritzung, Winterspritzung oder Austriebspritzung)

1 240

3.1.4.2.3.2

andere Schildlausarten

1 080

3.1.4.2.4

Birnenblattsauger

1 120

3.1.4.2.5

Apfelwickler und Pflaumenwickler

1 080

3.1.4.2.6

Schalenwickler

1 040

3.1.4.2.7

biotechnische Verfahren gegen Wickler

2 000

3.1.4.2.8

Sägewespen

1 040

3.1.4.2.9

Kirschfruchtfliege

1 240

3.1.4.2.10

überwinternde Stadien (Winterspritzung oder Austriebsspritzung)

1 120

3.1.4.3

Akarizide

3.1.4.3.1

während der Vegetationszeit

1 320

3.1.4.3.2

gegen überwinternde Stadien

1 160

3.1.4.3.3

gegen Spinnmilben in Beerenobst

1 320

3.1.4.4

Herbizide

3.1.4.4.1

unter Obstbäumen, Obststräuchern und in Baumschulen

880

3.1.4.4.2

in Erdbeeren

1 000

3.1.5

Mittel für den Zierpflanzenbau

3.1.5.1

Fungizide gegen Pilzkrankheiten an Zierpflanzen und Zierrasen einschließlich Auflaufkrankheiten

3.1.5.1.1

bis zu drei Behandlungen

1 000

3.1.5.1.2

je weitere Behandlung

240

3.1.5.2

Insektizide gegen

3.1.5.2.1

beißende und saugende Insekten (max.
drei Behandlungen)

1 200

3.1.5.2.2

bodenbürtige Insekten

2 000

3.1.5.3

Akarizide gegen

3.1.5.3.1

Spinnmilben

1 200

3.1.5.3.2

Weichhautmilben

1 400

3.1.5.4

Herbizide

3.1.5.4.1

in Ziergehölzanlagen und Ziergehölzbaumschulen

1 040

3.1.5.4.2

in sonstigen Zierpflanzen und Zierrasen

960

3.1.5.4.3

gegen Moos und Algen

760

3.1.5.5

Wachstumsregler

3.1.5.5.1

zum Stauchen

1 840

3.1.5.5.2

zum Stutzen

1 640

3.1.5.5.3

zur Bewurzelung

960

3.1.5.5.4

zur Beeinflussung der Blüte

1 040

3.1.5.5.5

zur Wuchshemmung von Intensivrasen

1 600

3.1.5.5.6

zur Entblätterung in der Baumschule

880

3.1.6

Mittel für Sonderkulturen

3.1.6.1

in Tabak

3.1.6.1.1

Fungizide gegen

3.1.6.1.1.1

Blauschimmel im Saatbeet

920

3.1.6.1.1.2

Blauschimmel im Freiland

1 520

3.1.6.1.1.3

Sclerotina spp.

680

3.1.6.1.2

Herbizide

840

3.1.6.1.3

Hemmung von Geiztrieben

1 760

3.1.7

Mittel für den Vorratsschutz

3.1.7.1

Fungizide gegen

3.1.7.1.1

Lagerschäden bei Dauerkohl

1 040

3.1.7.1.2

Lagerfäule bei Kartoffeln

1 480

3.1.7.2

Insektizide

3.1.7.2.1

Spritzmittel

3.1.7.2.1.1

Laborprüfung

2 640

3.1.7.2.1.2

in leeren Räumen

1 000

3.1.7.2.1.3

in belegten Räumen (in Vorratsgütern mit Feststellung der Dauerwirkung Zuschlag von 50 Prozent)

1 320

3.1.7.2.2

Begasungsmittel

3.1.7.2.2.1

in leeren Räumen

1 680

3.1.7.2.2.2

in belegten Räumen

2 000

3.1.7.2.2.3

in Vorratsgütern

2 000

3.1.7.3

Rodentizide (Versuche im Biotop)

1 560

3.1.8

Mittel für den Forstbereich

3.1.8.1

Fungizide gegen

3.1.8.1.1

Kiefernschütte

1 200

3.1.8.1.2

Eichenmehltau

680

3.1.8.1.3

Bläuepilze

1 200

3.1.8.1.4

Buchenstocken

1 200

3.1.8.2

Insektizide gegen

3.1.8.2.1

beißende Insekten

3.1.8.2.1.1

blatt- und nadelfressende Käfer

1 400

3.1.8.2.1.2

Rüsselkäfer (zur vorbeugenden Behandlung)

1 400

3.1.8.2.1.3

rindenbrütende Insekten und Nutzholzborkenkäfer

3.1.8.2.1.3.1

vorbeugend

1 680

3.1.8.2.1.3.2

kurativ

1 920

3.1.8.2.1.4

Schmetterlingsraupen

2 240

3.1.8.2.2

saugende Insekten

2 240

3.1.8.3

Akarizide

2 240

3.1.8.4

Rodentizide gegen

3.1.8.4.1

Erdmaus

2 080

3.1.8.4.2

Rötelmaus

1 760

3.1.8.4.3

Schermaus

3 200

3.1.8.5

Repellents gegen Winterwildverbiss, Sommerwildverbiss, Schälschäden, Fegeschäden, Hasenschäden und Kaninchenschäden

1 240 bis 4 480

3.1.8.6

Herbizide gegen

3.1.8.6.1

Gräser

1 040

3.1.8.6.2

Gräser und Unkräuter

1 320

3.1.8.6.3

Unkräuter und Holzgewächse

1 720

3.1.8.6.4

Adlerfarn in Saatbeeten und Verschulbeeten, Kulturen, je Baumart

1 240 bis 1 520

3.1.8.7

Mittel zum Wundverschluss

3.1.8.7.1

je Baumart ein Behandlungstermin

2 080

3.1.8.7.2

bei zwei Behandlungsterminen

3 120

3.1.9

Mittel für allgemeine Einsätze

3.1.9.1

Bakterizide gegen Feuerbrand

2 960

3.1.9.2

Insektizide gegen

3.1.9.2.1

Engerlinge und Drahtwürmer

2 000

3.1.9.2.2

Larven des Dickmaulrüßlers

2 000

3.1.9.2.3

Erdraupen

960

3.1.9.2.4

Maulwurfsgrillen

800

3.1.9.2.5

Ameisen

600

3.1.9.3

Nematizide gegen

3.1.9.3.1

gallenbildene Wurzelnematoden im Feldversuch

6 200

3.1.9.3.2

zystenbildende Wurzelnematoden im Feldversuch

2 280

3.1.9.3.3

zystenbildende und gallenbildende Wurzelnematoden im Gefäßversuch

2 280

3.1.9.3.4

wandernde Wurzelnematoden (bei zusätzlich erforderlichen Untersuchungen in größeren Bodentiefen Zuschlag von 50 Prozent)

2 040

3.1.9.3.5

Blattälchen

1 240

3.1.9.3.6

Stängelälchen/Rübenkopfälchen

2 040

3.1.9.4

Molluskizide

3.1.9.4.1

in Gemüse, Erdbeeren und Zierpflanzen (im Kasten)

2 680

3.1.9.4.2

in Ackerbaukulturen (im Kasten)

2 680

3.1.9.4.3

in Gemüse, Erdbeeren und Zierpflanzen (natürlicher Befall)

1 360

3.1.9.4.4

in Ackerbaukulturen (natürlicher Befall)

1 360

3.1.9.5

Rodentizide gegen Feldmaus (Versuch im Biotop)

1 560

3.1.9.6

Repellents

3.1.9.6.1

zur Wildabwehr

800

3.1.9.6.2

zur Vogelabwehr

1 000

3.1.9.6.3

Saatgutbehandlungsmittel

1 040

3.1.9.7

Herbizide

3.1.9.7.1

auf Wegen und Plätzen mit Baumbewuchs

960

3.1.9.7.2

auf Nichtkulturland

760

3.1.9.7.3

in Windschutzanlagen

1 040

3.1.9.7.4

gegen Holzgewächse

1 040

3.1.9.7.5

auf Gleisanlagen

3.1.9.7.5.1

Großparzellen, Ausbringung mit schienengebundenen Geräten

1 480

3.1.9.7.5.2

Kleinparzellen

680

3.1.9.8

Wachstumsregler

3.1.9.8.1

zur Bewurzelung von Pflanzenstecklingen

640

3.1.9.8.2

zur Wuchshemmung auf landwirtschaftlich nicht genutzten Grasflächen (z. B.
Straßenränder, Böschungen, Spielwiesen)

1 080

3.1.9.9

Mittel zur Veredlung und zum Wundverschluss

3.1.9.9.1

Mittel zur Veredlung

880

3.1.9.9.2

Mittel zur Wundbehandlung

560

3.1.9.9.3

Mittel zur Wundbehandlung mit fungizider Wirkung

1 640

3.1.10

Verträglichkeitsprüfungen (Pflanzgutkosten werden gesondert berechnet)

3.1.10.1

im Ackerbau

100 Prozent der entsprechenden Wirksamkeitsprüfung

3.1.10.2

im Gemüsebau

75 Prozent der entsprechenden Wirksamkeitsprüfung

3.1.10.3

im Obstbau

1 160

3.1.10.4

im Zierpflanzenbau

3.1.10.4.1

eine Behandlung

3.1.10.4.1.1

eine Art bis zehn Arten bzw.
Sorten

440

3.1.10.4.1.2

elf bis zwanzig Arten bzw.
Sorten

560

3.1.10.4.1.3

je weitere Behandlung

240

3.1.10.5

an Tabak

440

3.1.11

Ertragsfeststellungen in Verbindung mit der Prüfung der biologischen Wirkung

3.1.11.1

Getreide

240

3.1.11.2

Raps

280

3.1.11.3

Sonnenblumen

360

3.1.11.4

Mais

3.1.11.4.1

Körnermais

280

3.1.11.4.2

Silomais

360

3.1.11.5

Rüben

560

3.1.11.6

Kartoffeln

400

3.1.11.7

Feldfutter

480

3.1.11.8

Kleesamenbau

440

3.1.11.9

großkörnige Leguminosen

280

3.1.11.10

Wiesen und Weiden

600

3.1.11.11

Gemüse

3.1.11.11.1

einmalige Beerntung (Blatt- und Fruchtgemüse)

360

3.1.11.11.2

einmalige Beerntung (Wurzelgemüse)

800

3.1.11.11.3

weitere Beerntungsdurchgänge nach Aufwand

3.1.11.12

Kern- und Steinobst

600

3.1.11.13

Beerenobst

720

3.1.11.14

zusätzliche Feststellung bei Ernte, je Qualitätsmerkmal

120

3.1.12

Lieferung von Unterlagen und Materialien für Rückstandsuntersuchungen

3.1.12.1

aus einer laufenden Prüfung

280

3.1.12.2

aus speziell angelegtem Versuch nach GEP (ohne Rückstandsanalytik)

1 000

3.1.13

Zuschläge zu den vorgenannten Gebühren

3.1.13.1

Versuche unter Glas

320

3.1.13.2

je zusätzlich beantragtes Versuchsglied in einer Prüfung

ein Drittel der entsprechenden Gebühr

3.1.13.3

Ertragsfeststellung je zusätzlich beantragtes Versuchsglied in einer Prüfung

ein Drittel der entsprechenden Gebühr

3.1.14

Gebührenhöhe für teilweise oder vollständig nicht auswertbare Versuche

3.1.14.1

Versuch nicht auswertbar, da Anlage und Durchführung unvollständig

keine Gebühr

3.1.14.2

Versuch angelegt, Prüfantrag vom Antragsteller zurückgezogen

50 Prozent der Gebühr

3.1.14.3

durch Witterungs- oder durch andere nicht vorhersehbare Ereignisse bedingter vorzeitiger Abbruch des Versuches ohne verwertbare Ergebnisse

50 Prozent der Gebühr

3.1.14.4

durch Witterungs- oder durch andere nicht vorhersehbare Ereignisse bedingter vorzeitiger Abbruch des Versuches mit verwertbarem Teilergebnis

75 Prozent der Gebühr

3.1.14.5

zu Ende geführter Versuch nicht vollständig auswertbar, wenn wegen besonderer Witterungsbedingungen oder bei vorbeugend anzuwendenden Präparaten Schadorganismen nicht aufgetreten sind

75 Prozent der Gebühr

3.1.15

sonstige Gebührenerhebung

3.1.15.1

für die Prüfung von Zusatzstoffen werden diejenigen Gebühren erhoben, die jeweils für die einzelnen Indikatoren vorgesehen sind

3.1.15.2

für nicht genannte Anwendungsgebiete bzw.
Feststellungen werden Gebühren je nach Aufwand wie für ein vergleichbares Anwendungsgebiet erhoben

**3.2**

**phytosanitäre Bearbeitung von pflanzlichen Sendungen im innergemeinschaftlichen Handel sowie bei der Ausfuhr und Einfuhr in bzw.
aus Drittländern**

Vorbemerkungen:  
  
Die Gebühren werden für eigenständige mit Frachtpapieren versehene Transporteinheiten (Waggon, Ganzschiff, LKW-Zug) erhoben.
Die Gebühren werden je Sendung eines Absenders und eines Empfängers berechnet.  
  
Soweit eine Sendung nicht ausschließlich aus Erzeugnissen besteht, die der Berechnung der jeweiligen Tarifstelle entsprechen, werden die Teile der Sendung, die der Beschreibung entsprechen (wobei es sich um eine oder mehrere Partien handeln kann), als separate  
Sendung behandelt.  
  
Wird die Häufigkeit der Nämlichkeitskontrollen und Pflanzengesundheitsuntersuchungen bei der Einfuhr gemäß Artikel 13d Absatz 2 der Richtlinie 2000/29/EG vermindert, so sind für alle Sendungen und Partien dieser Gruppe, unabhängig davon, ob sie kontrolliert werden oder nicht, eine anteilmäßig verminderte Gebühr zu erheben.  
  
Durch den Antragsteller geforderte zusätzliche phytopathologische Prüfungen werden nach Tarifstelle 4 dieser Anlage erhoben.
Für pflanzliche Sendungen, für die eine phytosanitäre Kontrolle beantragt wurde und deren Ausfuhr bzw.
Verbringen aus phytosanitären oder anderen Gründen nicht erfolgt ist, sind gegenüber dem Antragsteller die Gebühren für bereits durchgeführte Amtshandlungen nach den betreffenden Tarifstellen zu erheben.  
  
Für Kontrolltätigkeiten an Warenarten, die in den Tarifstellen nicht aufgeführt sind oder für Kontrollen, bei denen das Transportmittel nicht die Bezugseinheit ist, werden Gebühren nach den anfallenden personellen und sächlichen Aufwendungen erhoben.

3.2.1

allgemeine Tarifstellen

3.2.1.1

Abgabe von Plomben

Selbstkostenpreis

3.2.1.2

Aufschlag für Amtshandlungen außerhalb der Dienststunden auf Veranlassung des Antragstellers

3.2.1.2.1

an Werktagen von 16 bis 20 Uhr

25 Prozent Aufschlag

3.2.1.2.2

an Werktagen von 20 bis 6 Uhr

50 Prozent Aufschlag

3.2.1.2.3

an Sonn- und Feiertagen

50 Prozent Aufschlag

3.2.2

innergemeinschaftliches Verbringen (Registrierung, Pflanzenpass)

3.2.2.1

Registrierung inklusive Datenaufnahme und Vergabe einer Registriernummer, je Antragsteller und Betrieb

60

3.2.2.2

Registrierung und Vergabe einer Registriernummer für Betriebe mit Handel von Speisekartoffeln und Veredlungskartoffeln sowie Zitrusfrüchten

30

3.2.2.3

Erteilung eines Änderungsbescheides (zu Tarifstelle 3.3.2.1 bzw. 3.3.2.2)

10

3.2.2.4

Abgabe von Pflanzenpassetiketten

3.2.2.4.1

großer Pflanzenpass, je 1 000 Stück

26

3.2.2.4.2

kleiner Pflanzenpass, je 1 000 Stück

5

3.2.2.5

Ausfertigung eines Pflanzenpasses durch den Pflanzenschutzdienst

mit maximal 10 Etiketten (kleiner Pflanzenpass)

7

je weitere 20 Etiketten (kleiner Pflanzenpass)

3

3.2.2.6

Mindestkontrollen in registrierten Betrieben gemäß Pflanzenbeschauverordnung (PflBeschauV)

nach Zeitaufwand,  
höchstens 200

3.2.3

Inverkehrbringen von Anbaumaterialien von Obstpflanzen, Gemüsepflanzen und Zierpflanzenarten

3.2.3.1

Eintragung oder Registrierung inklusive Datenaufnahme und Vergabe einer Eintragungsnummer oder Registriernummer, je Antragsteller und Betrieb

60

3.2.3.2

Eintragung inklusive Datenaufnahme und Vergabe einer Eintragungsnummer eines Betriebes, der bereits für den innergemeinschaftlichen Handel registriert ist (Tarifstelle 3.3.2.1)

30

3.2.3.3

Erteilung eines Änderungsbescheides (zu Tarifstelle 3.3.3.1 bzw. 3.3.3.2)

10

3.2.3.4

Mindestkontrollen in registrierten Betrieben gemäß Anbaumaterialverordnung (AGOZV)

nach Zeitaufwand,  
höchstens 200

3.2.3.5

Bescheinigung für anerkanntes Anbaumaterial

10

3.2.4

Handel außerhalb der EU

3.2.4.1

Ausfertigung von Zeugnissen, Pflanzenpässen und Bescheinigungen (Ausfertigung mit einer Kopie)

3.2.4.1.1

Pflanzengesundheitszeugnis

10

3.2.4.1.2

jedes Duplikat eines Pflanzengesundheitszeugnisses

5

3.2.4.1.3

Weiterversendungszeugnis

10

3.2.4.1.4

Intra-EC

10

3.2.4.1.5

sonstige amtliche Bescheinigung oder Bestätigung

10

3.2.4.2

Entscheidung über Antrag zur Genehmigung der Einfuhrkontrolle am Bestimmungsort

30

3.2.4.3

Entscheidung über Ausnahmegenehmigung bei Einfuhr von Drittlandwaren

60

3.2.4.4

Entscheidung über Ausnahmegenehmigung für Versuchs- und Züchtungszwecke

60

3.2.4.5

Entscheidung über die Ermächtigung zur Einfuhr bzw.
innergemeinschaftlichen Verbringung für Versuchs- und Züchtungszwecke

30

3.2.5

phytosanitäre Kontrollen von pflanzlichen Sendungen

3.2.5.1

Einfuhrkontrollen an Einlassstellen bzw.
am Bestimmungsort (Dokumentenprüfung, Identitätskontrolle und phytosanitäre Kontrolle)

3.2.5.1.1

für Dokumentenkontrolle, je Sendung

10

3.2.5.1.2

für Nämlichkeitskontrolle, je Sendung

bis zu einer LKW-Ladung, einer Güterwagenladung oder einer Containerladung vergleichbarer Größe

10

größer

20

3.2.5.1.3

für Pflanzengesundheitsuntersuchungen von

3.2.5.1.3.1

Stecklingen, Sämlingen (ausgenommen forstliches Vermehrungsgut), Jungpflanzen von Erdbeeren und Gemüse, je Sendung

bis zu 10 000 Stück

22

je weitere 1 000 Stück

0,84

Höchstbetrag

200

3.2.5.1.3.2

Sträuchern, Blumen (ausgenommen gefällte Weihnachtsbäume), anderen holzigen Baumschulerzeugnissen einschließlich forstlichem Vermehrungsgut (ausgenommen Saatgut), je Sendung

bis zu 1 000 Stück

22

je weitere 100 Stück

0,53

Höchstbetrag

200

3.2.5.1.3.3

Zwiebeln, Wurzelknollen, Wurzelstöcken, Knollen zum Anpflanzen (ausgenommen Kartoffelknollen), je Sendung

bis zu 20 kg Gewicht

22

je weitere 10 kg

0,19

Höchstbetrag

200

3.2.5.1.3.4

Samen, Gewebekulturen, je Sendung

bis zu 100 kg Gewicht

22

je weitere 10 kg

0,22

Höchstbetrag

200

3.2.5.1.3.5

anderen Pflanzen zum Anpflanzen, die nicht aufgeführt sind, je Sendung

bis zu 5 000 Stück

22

je weitere 100 Stück

0,22

Höchstbetrag

200

3.2.5.1.3.6

Schnittblumen, je Sendung

bis zu 20 000 Stück

22

je weitere 1 000 Stück

0,17

Höchstbetrag

200

3.2.5.1.3.7

Ästen mit Blattwerk, Teilen von Nadelbäumen (ausgenommen gefällte Weihnachtsbäume), je Sendung

bis zu 100 kg Gewicht

22

je weitere 10 kg

2,10

Höchstbetrag

200

3.2.5.1.3.8

gefällten Weihnachtsbäumen, je Sendung

bis zu 1 000 Stück

22

je weitere 100 Stück

2,10

Höchstbetrag

200

3.2.5.1.3.9

Blättern von Pflanzen (z. B.
Kräuter, Gewürze und Blattgemüse), je Sendung

bis zu 100 kg Gewicht

22

je weitere 10 kg

2,10

Höchstbetrag

200

3.2.5.1.3.10

Obst, Gemüse (ausgenommen Blattgemüse), je Sendung

bis zu 25 000 kg Gewicht

22

je weitere 1 000 kg

0,84

3.2.5.1.3.11

Kartoffelknollen, je Partie

bis zu 25 000 kg Gewicht

64

je weitere 25 000 kg

64

3.2.5.1.3.12

Holz (ausgenommen Rinde), je Sendung

bis zu 100 m³ Volumen

22

je weiteren m³

0,22

3.2.5.1.3.13

Erde und Nährsubstraten, Rinde, je Sendung

bis zu 25 000 kg Gewicht

22

je weitere 1 000 kg

0,1

Höchstbetrag

200

3.2.5.1.3.14

Getreidekörnern, je Sendung

bis zu 25 000 kg Gewicht

20

je weitere 1 000 kg

0,8

Höchstbetrag

700

3.2.5.1.3.15

anderen Pflanzen und Pflanzenerzeugnissen, die nicht aufgeführt sind, je Sendung

22

3.2.5.1.3.16

Holz als Verpackungsmaterial, je Sendung

20

3.2.5.1.4

Amtshandlungen anlässlich der Rücksendung oder unschädlichen Beseitigung von Sendungen

3.2.5.1.4.1

Amtshandlungen anlässlich der Rücksendung oder unschädlichen Beseitigung von Sendungen sowie deren Lagerung bis zur Rücksendung oder unschädlichen Beseitigung, wenn die Erzeugnisse nicht den Einfuhrbedingungen entsprechen oder Unregelmäßigkeiten vorliegen (einschließlich der Kosten für Transport, Beladen, Entladen, jedoch ohne Untersuchungskosten)

nach Aufwand,  
mindestens 20

3.2.5.1.5

Amtshandlungen mit erhöhtem Aufwand bei der phytosanitären Abfertigung

3.2.5.1.5.1

Zusätzliche Amtshandlungen z. B.
bei fehlender oder mangelnder Kennzeichnung der Verpackungseinheiten, bei schwer zugänglicher Ware, bei mehr als 5 Warenarten je Sendung u. a.

nach Aufwand,  
mindestens 20

3.2.5.1.5.2

Zusätzliche Amtshandlungen bei festgestelltem Befall oder Befallsverdacht mit Schadorganismen

nach Aufwand,  
mindestens 20

3.2.5.1.5.3

Einziehung und Vernichtung beschlagnahmter Sendungen im privaten Reisegepäck

25

3.2.5.1.5.4

Einziehung und Vernichtung beschlagnahmter nichtkommerzieller Postsendungen

25

3.2.5.1.5.5

Importabfertigungen mit Überwachung der Vernichtung oder Umladen

100

3.2.5.1.6

Kleinsendungen (Warenproben und Warenmuster, Samen und Pflanzenproben) sowie Sendungen zum nichtgewerblichen Gebrauch

10

3.2.5.2

Untersuchung von Sendungen für die Ausfuhr und das innergemeinschaftliche Verbringen

3.2.5.2.1

Jungpflanzen und Fertigpflanzen des Gartenbaues und der Baumschulen (außer Gemüsejungpflanzen), die nach Stückzahl handelsüblich sind

bis 1 000 Stück

15

je weitere angefangene 1 000 Stück

1,3

je Sendung

20 bis 60

3.2.5.2.2

Gemüsejungpflanzen

bis zu 1 000 Stück

8

je weitere angefangene 1 000 Stück

0,7

je Sendung

20 bis 30

3.2.5.2.3

alle anderen Pflanzen und Pflanzenteile (Sämlinge, Stecklinge, Blumenzwiebeln, Veredlungsreiser, Blumenknollen, sonstiges Vermehrungsmaterial und Schnittblumen)

bis zu 1 000 Stück

12

je weitere angefangene 1 000 Stück

1

je Sendung

20 bis 110

3.2.5.2.4

Pflanzen und sonstige Pflanzenerzeugnisse, die nach Gewicht handelsüblich sind

bis 1 000 kg

6

je weitere angefangene 1 000 kg

0,5

je Sendung

20 bis 60

3.2.5.2.5

Saatgut und Pflanzgut der Landwirtschaft des Gartenbaues und der Forstwirtschaft, das nach Gewicht handelsüblich ist

bis zu 1 Tonne

6

je weitere angefangene Tonne

0,6

je Sendung

20 bis 50

3.2.5.2.6

Konsumprodukte, Futtermittel und Produkte zur industriellen Verarbeitung

3.2.5.2.6.1

Getreide, Ölfrüchte

bis zu 1 Tonne

4

je weitere angefangene Tonne

0,25

je Sendung (außer Schiff)

10 bis 20

Schiff

15 bis 70

3.2.5.2.6.2

Mehl, Haferflocken

bis zu 1 Tonne

5

je weitere angefangene Tonne

0,2

je Sendung

10 bis 20

3.2.5.2.6.3

Zucker, je Transporteinheit

5

3.2.5.2.6.4

Kartoffelstärke, je Transporteinheit

5

3.2.5.2.7

Kartoffeln (außer Pflanzkartoffeln)

bis zu 1 Tonne

4

je weitere angefangene Tonne

0,4

je Sendung

15 bis 30

3.2.5.2.8

Pflanzenerzeugnisse wie Gemüse, Obst, Südfrüchte, Trockenfrüchte, Gewürze, Genussmittel, Nüsse, Drogen, Baumwolle u. a.

bis zu 1 Tonne

5

je weitere angefangene Tonne

0,5

je Sendung

10 bis 30

3.2.5.2.9

übrige pflanzliche Produkte und andere Materialien organischen Ursprungs, die potentiell Träger von gefährlichen Schaderregern der Pflanzen sein können

3.2.5.2.9.1

Holz

bis zu 1 ccm

3

je weitere angefangene ccm

0,2

je Sendung

10 bis 30

3.2.5.2.9.2

Holz als Stückware

je Stück oder Verpackungseinheit (lose Bretter für Kisten, Paletten o. Ä.)

10

jedes weitere Stück bzw.
Verpackungseinheit

1,2

je Sendung maximal

30

3.2.5.2.9.3

Erde, Pflanzensubstrat und Torf

bis zu 1 Tonne

3

je weitere angefangene Tonne

0,6

je Sendung

10 bis 30

3.2.5.2.10

Kleinsendungen (Warenproben, Warenmuster, Samenproben und Pflanzenproben bis 10 kg) sowie Sendungen zum nichtgewerblichen Gebrauch

10

3.2.6

phytosanitäre Überwachung von Pflanzen und Pflanzenerzeugnissen während der Vegetation bzw.
Lagerung sowie sonstige behördliche Leistungen

3.2.6.1

Kontrollen in Betrieben einschließlich Lagerhäusern, Speichern, Mühlen sowie Händlern bei Anforderung

nach Zeitaufwand

3.2.6.2

sonstige Leistungen bei der phytosanitären Ausfuhrkontrolle und Einfuhrkontrolle auf Anforderung

nach Zeitaufwand

3.2.7

Kontrolle von Holzverpackungen zum Inverkehrbringen

3.2.7.1

Registrierung inklusive Datenaufnahme und Vergabe einer Registriernummer, je Antragsteller und Betrieb

60

3.2.7.2

Erteilung einer Genehmigung zur Kennzeichnung von Holzverpackungen

30

3.2.7.3

Mindestkontrollen gemäß Pflanzenbeschauverordnung (PflBeschauV)

nach Zeitaufwand,  
höchstens 200

**3.3**

**Warndienst**  
  
**Hinweise, Prognosen, Warnungen je Fachgebiet und Jahr (inklusive ISIP-Zugang und maximal eine Broschüre je Fachgebiet)**

3.3.1

Ackerbau und Grünland

3.3.1.1

Ackerbau und Grünland für Landwirte

25

3.3.1.2

Ackerbau und Grünland für Handel, Industrie, Beratung

50

3.3.2

Gemüsebau und Zierpflanzenbau

3.3.2.1

Gemüsebau und Zierpflanzenbau für Gärtner

25

3.3.2.2

Gemüsebau und Zierpflanzenbau für Handel, Industrie, Beratung

50

3.3.3

Obstbau

3.3.3.1

Obstbau für Gärtner

25

3.3.3.2

Obstbau für Handel, Industrie, Beratung

50

3.3.4

Baumschulen und Landschaftsgärtnerei

3.3.4.1

Baumschulen und Landschaftsgärtnerei für Gärtner

25

3.3.4.2

Baumschulen und Landschaftsgärtnerei für Handel, Industrie, Beratung

50

3.3.5

Schutzgebühr bei Bestellung von zusätzlichen Broschüren, je Stück

12,50

**3.4**

**Sachkunde im Pflanzenschutz**

3.4.1

Abnahme von Prüfungen zum Sachkundenachweis (PflSchG und PflSchSachkV) für Anwender und Berater

60

3.4.2

Abnahme von Prüfungen zum Sachkundenachweis (PflSchG und PflSchSachkV) für Händler

50

3.4.3

Wiederholung nicht bestandener Prüfung

50

3.4.4

Ausstellen einer Bescheinigung über den Nachweis der Sachkunde Pflanzenschutz

30

3.4.5

Ausstellen einer Zweitbescheinigung über den Nachweis der Sachkunde im Pflanzenschutz bei Verlust

15

3.4.6

Ausstellen einer Bescheinigung über den Nachweis der Sachkunde Pflanzenschutz für Bürger eines anderen EU-Mitgliedsstaates, einschließlich Prüfung deutscher Sprachkenntnisse

30

3.4.7

amtliche Anerkennung einer Fortbildungsveranstaltung (PflSchG, PflSchSachkV)

100 bis 175

3.4.8

amtliche Bescheinigung über die Teilnahme an einer Fortbildungsveranstaltung (PflSchSachkV)

20

**3.5**

**Pflanzenschutztechnik**

3.5.1

amtliche Anerkennung von Kontrollwerkstätten oder Kontrollpersonen als Kontrollstelle zur Funktionsprüfung von im Gebrauch befindlichen Pflanzenschutzgeräten

250

3.5.2

Überprüfung der Kontrolltechnik von amtlich anerkannten Kontrollstellen

200

3.5.3

Praxiseinsatz Pflanzenschutzgeräte

10 bis 500

**3.6**

**Erteilung von Genehmigungen zur Ausbringung und Anwendung von Pflanzenschutzmitteln**

3.6.1

Genehmigung zur Ausbringung von Pflanzenschutzmitteln mit Luftfahrzeugen

3.6.1.1

bis 100 ha Behandlungsfläche

60

3.6.1.2

mehr als 100 bis 500 ha Behandlungsfläche

90

3.6.1.3

mehr als 500 bis 1 000 ha Behandlungsfläche

120

3.6.1.4

über 1 000 ha Behandlungsfläche

150

3.6.2

Genehmigung zur Anwendung von Pflanzenschutzmitteln auf nicht landwirtschaftlich, gärtnerisch oder forstwirtschaftlich genutzten Flächen (Erstantrag) auf

3.6.2.1

einer Fläche bis 100 m²

20

3.6.2.2

einem Standort oder einer Fläche bis 2 000 m²

40

3.6.2.3

zwei bis fünf Standorten oder einer Fläche über 2 000 bis 5 000 m²

90

3.6.2.4

sechs bis 20 Standorten oder einer Fläche über 5 000 bis 20 000 m²

120

3.6.2.5

21 bis 50 Standorten oder einer Fläche über 20 000 bis 50 000 m²

150

3.6.2.6

über 50 Standorten oder einer Fläche über 50 000 m²

200

3.6.3

Genehmigung zur Anwendung von Pflanzenschutzmitteln auf nicht landwirtschaftlich, gärtnerisch oder forstwirtschaftlich genutzten Flächen (Wiederholungsantrag) auf

3.6.3.1

einer Fläche bis 100 m²

15

3.6.3.2

einem Standort oder einer Fläche bis 2 000 m²

30

3.6.3.3

zwei bis fünf Standorten oder einer Fläche über 2 000 bis 5 000 m²

60

3.6.3.4

sechs bis 20 Standorten oder einer Fläche über 5 000 bis 20 000 m²

80

3.6.3.5

21 bis 50 Standorten oder einer Fläche über 20 000 bis 50 000 m²

100

3.6.3.6

über 50 Standorten oder einer Fläche über 50 000 m²

150

3.6.4

Genehmigung zur Anwendung von Pflanzenschutzmitteln im Einzelfall (§ 22 Absatz 2 PflSchG)

3.6.4.1

Einzelantrag, je Anwendungsgebiet

20

3.6.4.2

Sammelantrag, je Anwendungsgebiet

3.6.4.2.1

Grundgebühr

20

3.6.4.2.2

Gebühr, je zusätzlicher Nutzer

10

**3.7**

**Einfuhrkontrolle von Sendungen mit Pflanzenschutzmitteln**

3.7.1

Pflanzenschutzmittel bis 5 kg oder 5 l, je Sendung

10

3.7.2

bis drei Pflanzenschutzmittel oder bis 50 kg bzw.
50 l, je Sendung

20

3.7.3

bis fünf Pflanzenschutzmittel oder bis 500 kg bzw.
500 l, je Sendung

40

3.7.4

mehr als fünf Pflanzenschutzmittel oder mehr als 500 kg bzw.
500 l, je Sendung

60

Anmerkung:  
  
Bei den Einzelgebühren können weitere Gebühren nach der Tarifstelle 1 der Anlage 1 festgesetzt werden.

Wegstreckenpauschale in Abweichung von Tarifstelle 1.5.1 der Anlage 1, soweit eine Amtshandlung vor Ort stattfinden muss

10

**3.8**

**Anzeige von Anwendung, Inverkehrbringen und Einfuhr von Pflanzenschutzmitteln zu gewerblichen Zwecken**

3.8.1

Anwendung von Pflanzenschutzmitteln bzw.
Beratung zum Pflanzenschutz (PflSchG)

3.8.1.1

Registrierung und Vergabe einer Registriernummer an Betriebe, welche die Anwendung von Pflanzenschutzmitteln für andere (außer gelegentlicher Nachbarschaftshilfe) anzeigen

30

3.8.1.2

Registrierung und Vergabe einer Registriernummer an Betriebe oder Personen, welche die Beratung zum Pflanzenschutz zu gewerblichen Zwecken anzeigen

30

3.8.2

Registrierung und Vergabe einer Registriernummer an Betriebe, welche das Inverkehrbringen bzw.
Einführen von Pflanzenschutzmitteln zu gewerblichen Zwecken nach § 24 PflSchG anzeigen

30

**3.9**

**Inspektion einer Prüfeinrichtung Pflanzenschutz im Anerkennungsverfahren Gute Experimentelle Praxis (GEP)**

300

**4**

**Saatenanerkennung, phytopathologische Diagnostik**

**4.1**

**nach der Saatgutverordnung (SaatgutV)**

4.1.1

Anerkennungsverfahren Saatgut von landwirtschaftlichen Arten

4.1.1.1

Antragsannahme Saatgut (§§ 3, 4 und 5 SaatgutV)

4.1.1.1.1

Anmeldung per Datenträger, je Vermehrungsvorhaben

60

4.1.1.1.2

Anmeldung per Papier, je Vermehrungsvorhaben

70

4.1.1.1.3

Rücknahme des Antrages auf Anerkennung vor Beginn der Feldbesichtigung, je Vermehrungsvorhaben

30

4.1.1.1.4

Nachmeldegebühr bei wesentlicher Überschreitung der Termine nach § 4 Absatz 1 SaatgutV, je Vermehrungsvorhaben

20

4.1.1.2

Prüfung des Feldbestandes und Mitteilung über dessen Ergebnis (§§ 7, 8 und 9 SaatgutV)

4.1.1.2.1

Getreide und großkörnige Leguminosen, je angefangenen Hektar und je Besichtigung

11

4.1.1.2.2

alle anderen Arten, je angefangenen Hektar und je Besichtigung

14

4.1.1.2.3

Nachbesichtigung (§§ 8 und 9 SaatgutV) 50 Prozent der Gebühren nach den Tarifstellen 4.1.1.2.1 und 4.1.1.2.2

5,5 bis 7

4.1.1.2.4

Nachkontrolle der Beschilderung, Schlagtrennung und Randbemähung je Vermehrungsvorhaben

38

4.1.1.2.5

Wiederholungsbesichtigung (§ 10 SaatgutV), je angefangenen Hektar falls erstes Ergebnis bestätigt wird

11 bis 14

4.1.1.2.6

Feldbestandsprüfung durch externe Feldbesichtiger, mindestens 60 Prozent der Gebühren nach den Tarifstellen 4.1.1.2.1 bis 4.1.1.2.4

3,3 bis 25

4.1.2

verwaltungstechnische Maßnahmen

4.1.2.1

Festsetzung einer Betriebsnummer (§ 40 Absatz 5 SaatgutV)

20

4.1.2.2

Prüfung des Mischungsantrages (§ 27 SaatgutV)

7,5

4.1.2.3

Prüfung des Antrages auf Zuteilung einer Kennnummer (§ 40 Absatz 6 SaatgutV)

7,5

4.1.2.4

Genehmigung des Antrages auf Zulassung von Handelssaatgut (§§ 22 bis 25 SaatgutV), je Partie

15

4.1.2.5

Genehmigung des Antrages auf Wiederverschließung (§§ 37 und 48 SaatgutV), je Partie

7,5

4.1.2.6

Anerkennungsentscheidung einschließlich Bescheiderstellung (§ 14 SaatgutV), je Partie

8

4.1.2.7

Berichterstellung ohne Anerkennungsentscheidung, je Bericht

4

4.1.3

Probenahme

4.1.3.1

Probenahme, Kennzeichnung und Verschließung

je angefangene Stunde für die Tätigkeit eines amtlichen Probenehmers 51

4.1.4

Prüfung der Beschaffenheit

4.1.4.1

Prüfung der Beschaffenheit des Saatgutes, je Probe

4.1.4.1.1

Getreide, Sorghum und Mais

4.1.4.1.1.1

Reinheit

8

4.1.4.1.1.2

Besatz

5

4.1.4.1.1.3

Keimfähigkeit

8

4.1.4.1.1.4

biochemische Prüfung der Lebensfähigkeit (TTC-Schnelltest)

15

4.1.4.1.2

großkörnige Leguminosen

4.1.4.1.2.1

Reinheit

5

4.1.4.1.2.2

Besatz

5

4.1.4.1.2.3

Keimfähigkeit

10

4.1.4.1.3

kleinkörnige Leguminosen

4.1.4.1.3.1

Reinheit

12

4.1.4.1.3.2

Besatz

10

4.1.4.1.3.3

Keimfähigkeit

10

4.1.4.1.4

Gräser (außer Weidelgräser, Wiesenschwingel, Rohrschwingel und Festulolium)

4.1.4.1.4.1

Reinheit

12

4.1.4.1.4.2

Besatz

15

4.1.4.1.4.3

Keimfähigkeit

10

4.1.4.1.5

Weidelgräser, Wiesenschwingel, Rohrschwingel und Festulolium

4.1.4.1.5.1

Reinheit

10

4.1.4.1.5.2

Besatz

12

4.1.4.1.5.3

Keimfähigkeit

10

4.1.4.1.6

Futterrüben und Zuckerrüben (Normalsaat)

4.1.4.1.6.1

Reinheit

10

4.1.4.1.6.2

Besatz

8

4.1.4.1.6.3

Keimfähigkeit

12

4.1.4.1.7

Futterrüben und Zuckerrüben (Präzisionssaatgut und Monogermsaatgut)

4.1.4.1.7.1

Reinheit

10

4.1.4.1.7.2

Besatz

8

4.1.4.1.7.3

Keimfähigkeit

18

4.1.4.1.8

Ölpflanzen und Faserpflanzen sowie sonstige Futterpflanzen

4.1.4.1.8.1

Reinheit

12

4.1.4.1.8.2

Besatz

8

4.1.4.1.8.3

Keimfähigkeit

10

4.1.4.1.9

Gemüse (großsamige Arten wie Hülsenfrüchte, Schwarzwurzeln, Gurken und Kürbis)

4.1.4.1.9.1

Reinheit

6

4.1.4.1.9.2

Besatz

6

4.1.4.1.9.3

Keimfähigkeit

12

4.1.4.1.10

sonstige Gemüsearten, Arznei-, Gewürz- und Zierpflanzen

4.1.4.1.10.1

Reinheit

12

4.1.4.1.10.2

Besatz

9

4.1.4.1.10.3

Keimfähigkeit

9

4.1.4.1.11

Mischungen

4.1.4.1.11.1

Reinheit (einschließlich Bestimmung der Artenanteile)

4.1.2.1.11.1.1

großkörnige Arten

10 + 5 je Art

4.1.4.1.11.1.2

kleinkörnige Arten

20 + 7 je Art

4.1.4.1.11.2

Keimfähigkeit, je Art

12

4.1.4.1.11.3

Reinheit (ohne Bestimmung der Artenanteile)

4.1.4.1.11.3.1

großkörnige Arten

10

4.1.4.1.11.3.2

kleinkörnige Arten

20

4.1.4.1.11.4

Keimfähigkeit (Mischprobe)

12

4.1.5

sonstige Beschaffenheits- und Nachprüfungen an Samen

4.1.5.1

Tausendkornmasse

6

4.1.5.2

Feuchtigkeitsgehalt

10

4.1.5.3

Sortierung

8

4.1.5.4

Hektolitergewicht

8

4.1.5.5

Schwarzbesatz und Auswuchs

8

4.1.5.6

Bestimmung des Besatzes mit Flughafer nach § 12 Absatz 1, Anlage 3, Nummer 1.2 SaatgutV

in einer 1-kg-Probe

7

in einer 3-kg-Probe

20

4.1.5.7

Bestimmung des Bitterstoffgehaltes bei Süßlupinen

10

4.1.5.8

Laborbeizung

8

4.1.5.9

Echtheitsbestimmung

4.1.5.9.1

mikroskopische Art- und Sortendiagnose

28

4.1.5.9.2

fluoreszenzanalytische Prüfung

20

4.1.5.10

Kalttest und Triebkraftprüfung

4.1.5.10.1

Kalttest bei Getreide außer Mais

15

4.1.5.10.2

Kalttest bei Mais

25

4.1.5.10.3

Triebkraftprüfung bei großkörnigen Leguminosen

25

4.1.5.11

andere Methoden und Untersuchungen mit besonderem Aufwand zusätzlich, je Probe oder Partie

10 bis 150

**4.2**

**nach der Pflanzkartoffelverordnung (PflKartV)**

4.2.1

Anerkennungsverfahren von Pflanzkartoffeln

4.2.1.1

Antragsannahme Pflanzkartoffeln (§ 5 PflKartV)

4.2.1.1.1

Anmeldung per Datenträger, je Vermehrungsvorhaben

50

4.2.1.1.2

Anmeldung per Papier, je Vermehrungsvorhaben

60

4.2.1.1.3

Rücknahme des Antrages auf Anerkennung vor Beginn der Feldbesichtigung, je Vermehrungsvorhaben

25

4.2.1.2

Prüfung des Feldbestandes und Mitteilung über dessen Ergebnis (§§ 8, 9 und 10 PflKartV)

4.2.1.2.1

Feldbestandsprüfung, je angefangenen Hektar und je Besichtigung

11

4.2.1.2.2

Nachkontrolle der Beschilderung, der Abtrennung, je Schlag

38

4.2.1.2.3

Nachbesichtigung (§ 10 PflKartV) 50 Prozent der Gebühren nach Tarifstelle 4.2.1.2.1

5,5

4.2.1.2.4

Wiederholungsbesichtigung (§ 12 PflKartV), je angefangenen Hektar, falls erstes Ergebnis bestätigt wird

11

4.2.1.2.5

Feldbestandsprüfung durch externe Feldbesichtiger mindestens 60 Prozent der Gebühren nach den Tarifstellen 4.2.1.2.1 bis 4.2.1.2.3

3,3 bis 25

4.2.1.3

Beschaffenheitsprüfung (§§ 13, 14 und 15 PflKartV)

4.2.1.3.1

Tätigkeit eines amtlichen Probenehmers, je angefangene Stunde

51

4.2.1.3.2

Prüfung auf Viruskrankheiten (§§ 13 und 15 PflKartV), je Probe

4.2.1.3.2.1

Vorstufen- und Basispartien

105

4.2.1.3.2.2

zertifiziertes Pflanzgut

55

4.2.1.3.3

Prüfung auf Quarantänekrankheiten nach § 15 Absatz 3 PflKartV, zwei bakterielle Erreger je Probe

69

4.2.1.3.4

besondere Untersuchungen bei der Prüfung auf Knollenkrankheiten und äußere Mängel nach Maßgabe des Aufwandes, je Probe oder Partie

15 bis 77

4.2.1.3.5

weitere Probenahmen und Prüfung auf Viruskrankheiten (§ 15 Absatz 1 PflKartV), je Probe

102 bis 152

4.2.2

sonstige Gebühren

4.2.2.1

Nachkontrolle der getrennten Lagerung (§ 6 Absatz 3 Satz 2 PflKartV), je Betrieb

30,5

4.2.2.2

Festsetzung einer Betriebsnummer (§ 30 Absatz 4 PflKartV)

20

4.2.2.3

andere Prüfungen und Untersuchungen mit besonderem Aufwand, zusätzlich je Probe oder Partie

10 bis 150

**4.3**

**phytopathologische Prüfungen von Pflanzen, Pflanzenteilen und Pflanzenerzeugnissen sowie von Erden, Substraten und Wasser**

4.3.1

allgemeine Untersuchungen

4.3.1.1

Untersuchungen mit speziellen Anforderungen (nach Aufwand)

25 bis 100

4.3.2

Untersuchung auf Befall mit Viren

4.3.2.1

Bioassay auf Indikatorpflanzen

35

4.3.2.2

serologischer Virusnachweis im ELISA-Verfahren

45

4.3.2.3

molekularer Nachweis (PCR, RT-PCR, DNA-Sequenzierung)

25 bis 50

4.3.2.4

Augenstecklingsprüfung mit visueller Beurteilung

55

4.3.2.5

Augenstecklingsprüfung mit zusätzlichem serologischen Virusnachweis im ELISA-Verfahren

105

4.3.3

Untersuchung auf Befall mit Bakterien

4.3.3.1

Identifizierung pflanzenpathogener Bakterien (diff.
Nährmedien, IFT, Biolog, PCR, DNA-Sequenzierung, Pathogenitätsnachweis)

20 bis 70

4.3.3.2

Untersuchung von Kartoffeln auf Bakterienringfäule (Cms) und Schleimkrankheit (Rs), je Probe

69

4.3.3.3

Untersuchung von Erden, Substraten, Wasser auf pflanzenpathogene Bakterien

50

4.3.4

Untersuchung auf Befall mit Pilzen

4.3.4.1

Identifizierung pflanzenpathogener Pilze und Oomyceten (Morphologie, PCR, DNA-Sequenzierung)

30 bis 60

4.3.4.2

Saatgutuntersuchungen (Nährmedium, morphologisch)

15 bis 50

4.3.4.3

Untersuchung von Erden, Substraten und Wasser

59

4.3.4.4

Bioassay mittels Indikatorpflanzen

20

4.3.5

Untersuchung auf Befall mit Nematoden

4.3.5.1

allgemeine Befallsfeststellung

10

4.3.5.2

Beaufsichtigung der Probenahme auf Pflanzkartoffelflächen, je Probe

0,3

4.3.5.3

Entnahme von Boden im vorgeschriebenen Raster, je Probe

2,5

4.3.5.4

Untersuchung auf zystenbildende Nematoden im Spülverfahren im Rahmen des Pflanzenkartoffelanerkennungsverfahrens, je angefangener Hektar

20

4.3.5.5

Inhaltsbestimmung und Vitalitätsbestimmung, je Probe oder je Gefäß

20

4.3.5.6

Biotest mit Indikatorpflanzen

20

4.3.5.7

Untersuchung von wandernden Nematoden im Trichterverfahren, je Probe

30

4.3.5.8

Molekularbiologische Verfahren zur Artdifferenzierung (PCR, Sequenzierung)

25 bis 50

4.3.5.9

Bestimmung der Gattung/Art (morphologisch), je Probe

50

4.3.5.10

Untersuchung auf zystenbildende Nematoden nach Aufwand

10 bis 150

4.3.6

Untersuchung auf Befall mit Insekten

4.3.6.1

allgemeine Befallsfeststellung

10

4.3.6.2

Fruchtholzproben, allgemeine Befallsfeststellung, je Probe

3

4.3.6.3

Identifizierung von Schadinsekten und Milben (morphologisch), nach Zeitaufwand

20 bis 60

4.3.6.4

molekularbiologische Verfahren zur Artdifferenzierung (PCR, Sequenzierung)

40 bis 70

**5**

**Waldrechtliche Angelegenheiten**

**5.1**

**Bereitstellung von Walddaten und Forstkarten**

5.1.1

Bereitstellung von Auszügen aus dem Waldverzeichnis nach § 30 Absatz 2 LWaldG, soweit es sich nicht um eine mündliche oder einfache schriftliche Auskunft handelt

nach Zeitaufwand

5.1.2

Bereitstellung von Ergebnissen der forstlichen Rahmenplanung und anderer Fachplanungen für den Wald nach § 7 LWaldG einschließlich der Waldfunktionenkartierung

nach Zeitaufwand,  
mindestens 50

**5.2**

**Verwaltungsentscheidungen nach dem Waldgesetz des Landes Brandenburg (LWaldG)**

5.2.1

Prüfung der Waldeigenschaft nach § 2 in Verbindung mit § 32 Absatz 1 Nummer 6 LWaldG

5.2.1.1

Entscheidung über die Waldeigenschaft nach § 2 LWaldG, auch soweit sie in Zulassungen aufgrund anderer Gesetze eingeschlossen oder ersetzt werden

nach Zeitaufwand,  
mindestens 100,  
höchstens 10 000

5.2.2

Waldumwandlung nach § 8 LWaldG, auch soweit sie in Zulassungen auf Grund anderer Gesetze eingeschlossen oder ersetzt werden

5.2.2.1

Entscheidung über die Genehmigung einer Umwandlung von Wald nach § 8 Absatz 1 und 6 LWaldG

350 zuzüglich 1 je m²

5.2.2.2

Verfahren Anlagen erneuerbare Energien betreffend

je Anlage bis 3 MW Nennleistung

8 000

je Anlage über 3 MW Nennleistung

zuzüglich 2 000 für jedes weitere angefangene MW

5.2.3

Entscheidung über die Genehmigung einer Erstaufforstung nach § 9 Absatz 1 LWaldG, auch soweit sie in Zulassungen auf Grund anderer Gesetze eingeschlossen oder ersetzt werden

nach Zeitaufwand,  
mindestens 100,  
höchstens 1 000

5.2.4

Entscheidung über die Genehmigung einer Fristverlängerung zur

5.2.4.1

Durchführung der Waldumwandlung nach § 8 Absatz 1 LWaldG

100

5.2.4.2

Durchführung einer Ersatzaufforstung oder sonstigen Schutz- und Gestaltungsmaßnahme zum Ausgleich nachteiliger Wirkungen einer Waldumwandlung nach § 8 Absatz 3 Satz 2 LWaldG

100

5.2.4.3

Erstaufforstung nach § 9 Absatz 1 LWaldG

100

5.2.4.4

Wiederaufforstung nach § 11 Absatz 3 LWaldG

100

5.2.5

Entscheidung eines Antrages über die Erklärung von Wald zu Schutz- oder Erholungswald nach § 12 Absatz 1 LWaldG

200 bis 10 000

5.2.6

Untersagung oder Einschränkung der Markierung von Wander-, Reit- oder Radwegen und Sport- und Lehrpfaden nach § 15 Absatz 6 LWaldG

50

5.2.7

Untersagung oder Einschränkung der Gestattung des Befahrens des Waldes nach § 16 Absatz 3 LWaldG

50

5.2.8

Entscheidung über die Genehmigung von Sperren von Wald nach § 18 Absatz 2 LWaldG

100 bis 1 000

5.2.9

Maßnahmen der Forstaufsicht nach § 34 LWaldG

100 bis 10 000

5.2.10

auf Antrag erteilte Änderungsbescheide

nach Zeitaufwand, höchstens 50 Prozent der Gebühr des Ausgangsbescheides

**5.3**

**Entscheidung über die Genehmigung einer Satzungsänderung eines anerkannten forstwirtschaftlichen Zusammenschlusses nach § 33 Absatz 2 BGB und § 32 Absatz 4 LWaldG**

50 bis 100

**5.4**

**Entscheidung über die Genehmigung eines Antrages des forstwirtschaftlichen Zusammenschlusses zur Auflösung nach § 41 BGB und § 32 Absatz 4 LWaldG**

50

**5.5**

**Gebühren für Amtshandlungen nach dem Forstvermehrungsgutgesetz (FoVG)**

5.5.1

Zulassung von Ausgangsmaterial der Kategorien „Ausgewählt“, „Qualifiziert“ oder „Geprüft“ nach § 4 Absatz 1 FoVG

5.5.1.1

auf Antrag, je Zulassungseinheit und Antragsteller

50

5.5.1.2

von Amts wegen

keine Gebühr

5.5.2

Ausstellung von Stammzertifikaten

5.5.2.1

nach § 8 Absatz 2 FoVG, soweit nicht nach Tarifstelle 5.5.2.2 gebührenfrei

30

5.5.2.2

Folgestammzertifikate zu Tarifstelle 5.5.2.1 von Ernten aus einem Bestand (einer Registernummer oder Zulassungseinheit) innerhalb eines Baumschuljahres, für die auf Grund tagweiser Abfuhren mehrere Stammzertifikate ausgestellt werden, sofern die Angaben zu Lieferant, Ernteunternehmen und Empfänger identisch sind

keine Gebühr

5.5.2.3

für Mischungen nach § 9 Absatz 2 FoVG, soweit nicht nach Tarifstelle 5.5.2.4 gebührenfrei

50

5.5.2.4

bei Mischungen von Ernten aus einem Bestand (einer Registernummer oder Zulassungseinheit) innerhalb eines Baumschuljahres, für die auf Grund tagweiser Abfuhren mehrere Stammzertifikate ausgestellt werden

keine Gebühr

5.5.3

Ausstellung von Stammzertifikaten nach § 16 Absatz 2 FoVG auf Antrag, welche für die Ausfuhr bestimmt sind

50

5.5.4

vollständige oder teilweise Untersagung der Fortführung eines Forstsamenbetriebes oder Forstpflanzenbetriebes nach § 17 Absatz 4 FoVG

250 bis 1 000

5.5.5

Aufhebung der Untersagung der Fortführung eines Forstsamenbetriebes oder Forstpflanzenbetriebes nach § 17 Absatz 4 FoVG

150 bis 300

5.5.6

Bereitstellung von Registerauszügen

nach Zeitaufwand

5.5.7

Durchführung von amtlichen Kontrollen weiterer Baumarten und künstlicher Hybriden nach § 18 Absatz 7 FoVG

nach Aufwand

5.5.8

Gestattung der Ernte von Zierzapfen außerhalb der Zeiten nach § 2 Absatz 4 BbgFoVGDV

nach Zeitaufwand,  
höchstens 50

**5.6**

**Durchführung einer Waldbewertung von Amts wegen**

100 bis 5 000

**6**

**Jagdrechtliche Angelegenheiten**

**6.1**

**Jägerprüfung, Falknerprüfung, Jagdaufseherprüfung**

6.1.1

Zulassung zur und Durchführung der Jägerprüfung nach § 24 Absatz 1 des Jagdgesetzes für das Land Brandenburg (BbgJagdG) in Verbindung mit den §§ 2 und 16 der Jägerprüfungsordnung (JPO)

280

6.1.1.1

Zulassung zur und Durchführung der Jägerprüfung nur zum Nachweis der Voraussetzungen zum Erwerb eines Falknerjagdscheines nach § 24 Absatz 1 BbgJagdG in Verbindung mit den §§ 2 und 16 JPO

60

6.1.2

Zulassung zur und Durchführung der Falknerprüfung nach § 24 Absatz 1 BbgJagdG in Verbindung mit § 2 JPO

150

6.1.3

Zulassung zur und Durchführung der Jagdaufseherprüfung nach § 39 Absatz 3 BbgJagdG in Verbindung mit § 3 Absatz 3 der Verordnung über die Prüfung von Jagdaufsehern des Landes Brandenburg (PO-Jagdaufseher)

80

**6.2**

**Jagdscheine**

6.2.1

Ausstellung eines Ein-Jahresjagdscheines/Ein-Jahresjagdscheines für Ausländer nach § 15 des Bundesjagdgesetzes (BJagdG)

35

6.2.2

Ausstellung eines Zwei-Jahresjagdscheines/Zwei-Jahresjagdscheines für Ausländer nach § 15 BJagdG

50

6.2.3

Ausstellung eines Drei-Jahresjagdscheines/Drei-Jahresjagdscheines für Ausländer nach § 15 BJagdG

80

6.2.4

Ausstellung eines Ein-Jahresjagdscheines für Jugendliche nach § 15 BJagdG

15

6.2.5

Ausstellung eines Zwei-Jahresjagdscheines für Jugendliche nach § 15 BJagdG

20

6.2.6

Ausstellung eines Tagesjagdscheines/Tagesjagdscheines für Jugendliche/Tagesjagdscheines für Ausländer nach § 15 BJagdG

20

6.2.7

Ausstellung eines Ein-Jahresfalknerjagdscheines nach § 15 BJagdG

15

6.2.8

Ausstellung eines Zwei-Jahresfalknerjagdscheines nach § 15 BJagdG

20

6.2.9

Ausstellung eines Drei-Jahresfalknerjagdscheines nach § 15 BJagdG

25

6.2.10

Ausstellung eines Ein-Jahresfalknerjagdscheines für Jugendliche nach § 15 BJagdG

10

6.2.11

Ausstellung eines Zwei-Jahresfalknerjagdscheines für Jugendliche nach § 15 BJagdG

15

6.2.12

Ausstellung eines Tagesfalknerjagdscheines/Tagesfalknerjagdscheines für Jugendliche nach § 15 BJagdG

10

6.2.13

Zweitausstellung (Ersatz) eines Jagdscheines oder eines Prüfungszeugnisses nach § 15 BJagdG

30

6.2.14

Eintragung von Flächen in den Jagdschein bei nicht gleichzeitiger Beantragung eines Jagdscheines nach § 15 Absatz 2 BbgJagdG

10

6.2.15

Prüfung von entgeltlichen Jagderlaubnissen bei nicht gleichzeitiger Beantragung eines Jagdscheines nach § 15 Absatz 2 BbgJagdG

10

6.2.16

Erteilung einer Freistellungsgenehmigung zur Teilnahme an der Jäger- oder Falknerprüfung in einem anderen Landkreis oder einem anderen Bundesland nach § 2 Absatz 2 JPO

25

**6.3**

**Jagdbezirke**

6.3.1

Abrundung von Jagdbezirken nach § 5 BJagdG in Verbindung mit § 2 BbgJagdG ohne Ortsbegehung

80

6.3.2

Abrundung von Jagdbezirken nach § 5 BJagdG in Verbindung mit § 2 BbgJagdG mit Ortsbegehung

120 bis 300

6.3.3

Ausnahmegenehmigung für Eigenjagdbezirke unter 150 Hektar nach § 7 Absatz 1 BbgJagdG

100

6.3.4

Herabsetzung der Mindestgröße für gemeinschaftliche Jagdbezirke nach § 9 Absatz 1 und 2 BbgJagdG

60

6.3.5

Angliederung und Zusammenlegung von Grundflächen zu Jagdbezirken nach § 9 Absatz 3 und 4 BbgJagdG

80

6.3.6

Erklärung von Grundflächen zu befriedeten Bezirken nach § 5 Absatz 2 BbgJagdG ohne Ortsbegehung

40

6.3.7

Erklärung von Grundflächen zu befriedeten Bezirken nach § 5 Absatz 2 BbgJagdG mit Ortsbegehung

120

6.3.8

Angliederung von Eigenjagdbezirken nach Verzicht auf die Nutzung als Eigenjagdbezirk nach § 7 Absatz 3 BbgJagdG

30 bis 80

6.3.9

Zulassung der Teilung von gemeinschaftlichen Jagdbezirken nach § 9 Absatz 5 BbgJagdG

50

6.3.10

Zulassung der Verpachtung eines Teiles von geringerer Größe als der gesetzlichen Mindestgröße nach § 13 Absatz 1 BbgJagdG

30

6.3.11

Prüfung von Jagdpachtverträgen und von Änderungen der Jagdpachtverträge nach § 12 Absatz 1 BJagdG

30

6.3.12

Genehmigung von Satzungen der Jagdgenossenschaften nach § 10 Absatz 2 BbgJagdG

40

6.3.13

Genehmigung von Änderungen der Satzungen der Jagdgenossenschaften nach § 10 Absatz 2 BbgJagdG

20

6.3.14

Festsetzung einer Satzung für Jagdgenossenschaften, die nicht binnen eines Jahres nach ihrer Entstehung eine Satzung beschlossen haben, nach § 10 Absatz 4 BbgJagdG

100

6.3.15

Festlegung eines Jägernotweges nach § 32 Absatz 1 BbgJagdG ohne Ortsbegehung

50

6.3.16

Festlegung eines Jägernotweges nach § 32 Absatz 1 BbgJagdG mit Ortsbegehung

100 bis 150

6.3.17

Festsetzung einer angemessenen Entschädigung für den Jägernotweg nach § 32 Absatz 1 BbgJagdG

60

**6.4**

**Jagdausübung**

6.4.1

Festsetzung von Abschussplänen nach § 29 Absatz 3 BbgJagdG

80

6.4.2

Zulassung der Nachtjagd in Einzelfällen nach § 26 Absatz 3 BbgJagdG

30

6.4.3

Gestattung von Jagdhandlungen in befriedeten Bezirken nach § 5 Absatz 3 BbgJagdG ohne Ortsbegehung

30

6.4.4

Gestattung von Jagdhandlungen in befriedeten Bezirken nach § 5 Absatz 3 BbgJagdG mit Ortsbegehung

90 bis 120

6.4.5

Zustimmung zum Ruhenlassen der Jagd nach § 5 Absatz 4 BbgJagdG ohne Ortsbegehung

40

6.4.6

Zustimmung zum Ruhenlassen der Jagd nach § 5 Absatz 4 BbgJagdG mit Ortsbegehung

90 bis 120

6.4.7

Beschränkung der Befugnis oder Verpflichtung zur Erteilung einer Jagderlaubnis im Einzelfall nach § 16 Absatz 5 BbgJagdG

0 bis 25

6.4.8

Erlass eines Abschussverbotes nach § 30 BbgJagdG

30

**6.5**

**sonstige jagdliche Amtshandlungen, Genehmigungen, Bestätigungen**

6.5.1

Genehmigung der Satzung einer Hegegemeinschaft nach § 12 Absatz 2 BbgJagdG

20

6.5.2

Erteilung einer Ausnahmegenehmigung zum Aushorsten von Ästlingen und Nestlingen für Beizzwecke nach § 31 Absatz 4 BbgJagdG

40

6.5.3

Ausnahmegenehmigung von den Verboten des § 19 BJagdG nach § 26 Absatz 2 BbgJagdG

30

6.5.4

Genehmigung zur Aufhebung der Schonzeit nach § 31 Absatz 3 Nummer 1 BbgJagdG

30

6.5.5

Genehmigung nach § 31 Absatz 4 Nummer 1, 2 und 4 BbgJagdG

30

6.5.6

Erteilung einer Ausnahmegenehmigung nach § 2 Absatz 5 und § 3 Absatz 4 der Bundeswildschutzverordnung (BWildSchV)

35

6.5.7

Erteilung der Genehmigung von Gattern nach § 21 BbgJagdG ohne Ortsbegehung

80

6.5.8

Erteilung der Genehmigung von Gattern nach § 21 BbgJagdG mit Ortsbegehung

100 bis 200

6.5.9

Bestätigung von Schweißhundeführern nach § 35 Absatz 4 BbgJagdG

30

6.5.10

Bestätigung von Jagdaufsehern inklusive Ausstellung des Dienstausweises nach § 39 Absatz 3 BbgJagdG

30

6.5.11

Genehmigung von Fütterungen nach § 41 Absatz 2 BbgJagdG

30

6.5.12

Genehmigung des Aussetzens von Wild nach § 42 Absatz 1 BbgJagdG

100

6.5.13

Zulassung einer Ausnahme vom Erfordernis der Jagdpachtfähigkeit nach § 11 Absatz 5 BJagdG

50

**7**

**Amtshandlungen nach dem Verbraucherinformationsgesetz (VIG)**

**7.1**

**Erteilung mündlicher und einfacher schriftlicher Auskünfte**

keine Gebühr

**7.2**

**Erteilung schriftlicher Auskünfte**

nach Zeitaufwand

**7.3**

**Herausgabe von Unterlagen und Duplikaten**

nach Zeitaufwand

**7.4**

**Gewährung von Akteneinsicht**

nach Zeitaufwand

**7.5**

**Auslagen**  
  
Auslagen werden zusätzlich zu den Gebühren nach den Tarifstellen 7.2 bis 7.4 erhoben.
Sie werden auch im Fall der Gebührenfreiheit nach Tarifstelle 7.1 erhoben.
Bei der Herstellung von Zweitschriften, Kopien und Computerausdrucken in geringem Umfang kann auf die Erhebung der Auslagen verzichtet werden.

gemäß Anlage 1,  
im Übrigen in voller Höhe

**8**

**Weinrechtliche Angelegenheiten**

**8.1**

**Erteilung einer amtlichen Prüfungsnummer**

15 bis 160

**8.2**

**Amtshandlungen nach der Verordnung (EG) Nr.
884/2001 der Kommission vom 24. April 2001 mit Durchführungsbestimmungen zu den Begleitdokumenten für die Beförderung von Weinbauerzeugnissen und zu den Ein- und Ausgangsbüchern im Weinsektor**

8.2.1

Erteilung einer Bezugsnummer und des Sichtvermerks im Begleitpapier nach Artikel 3 Absatz 4 der Verordnung (EG) Nr.
884/2001

10 bis 30

8.2.2

Genehmigung eines Buchführungsverfahrens nach Artikel 12 Absatz 1 Unterabsatz in Verbindung mit § 12 der Weinüberwachungsverordnung (WeinÜV)

50 bis 150

**9**

**Sachverständigenwesen**

**9.1**

**Antragsgebühr**

80

**9.2**

**Bestellungsgebühr**

230

**9.3**

**Wiederbestellung früherer Sachverständiger**

230

**9.4**

**Gebühr für die Durchführung der Sachkundenachweise bei der erstmaligen Bestellung für ein Fachgebiet**

260

9.4.1

für jedes weitere Fachgebiet bei der erstmaligen Bestellung erhöht sich die Gebühr je Fachgebiet um

100

9.4.2

Gebühr für die Erweiterung der öffentlichen Bestellung, je Fachgebiet bei bereits bestellten Sachverständigen

200

**9.5**

**Gebühr für die öffentliche Bestellung als Probenehmer**

125

**9.6**

**Gebühr für die Verlängerung der öffentlichen Bestellung als Probenehmer**

30

**10**

**Gebühren für die Abnahme von Prüfungen und sonstige Angelegenheiten nach dem Berufsbildungsgesetz (BBiG)**  
  
Anmerkung:  
  
Mit Beginn der Prüfung ist unabhängig von deren weiterem Verlauf die Gesamtgebühr für die Prüfung zu begleichen.  
  
Tritt der Prüfling nach erfolgter Zulassung vor Beginn der Prüfung zurück, so werden für anfallende Verwaltungsarbeiten 20 Prozent der Gebühr erhoben.
Erscheint der Prüfling nicht zum 1. Prüfungstermin oder tritt der Prüfling nach Beginn der Prüfung zurück, so ist die Prüfungsgebühr voll zu entrichten.  
  
Muss der Gebührenbescheid aus Gründen, die der Prüfling zu vertreten hat, geändert werden, so werden für anfallende Verwaltungskosten 20 Prozent der Gebühr erhoben.

**10.1**

**Berufsabschlussprüfungen (außer Regelerstausbildung)**

250

**10.2**

**Prüfungen gemäß Ausbilder-Eignungsverordnung (AEVO)**

150

**10.3**

**Fortbildungsprüfungen**

10.3.1

Meisterprüfung gemäß § 53 BBiG mit Befreiung vom Prüfungsbereich Berufsausbildung (AEVO)

300

10.3.2

Ergänzung Meister Teil 3  
  
Prüfungsbereich Mitarbeiterführung

  
  
500

10.3.3

Fortbildungsprüfungen gemäß § 53 BBiG

500

**10.4**

**Wiederholung von Prüfungen**

10.4.1

Berufsabschlussprüfungen

10.4.1.1

Je Bereich: praktische und betriebliche Prüfung

130

10.4.1.2

Je Bereich: fachtheoretische (schriftliche und mündliche) Prüfung

70

10.4.2

Prüfung gemäß Ausbilder-Eignungsverordnung (AEVO) oder Prüfungsbereich Berufsausbildung als Teil der Meisterprüfungen

10.4.2.1

praktischer Teil

115

10.4.2.2

fachtheoretischer Teil

45

10.4.3

Fortbildungsprüfungen

10.4.3.1

bei Meisterprüfungen mit insgesamt drei Prüfungsteilen

Teil 1 und Teil 2 je

150

Teil 3  
  
davon nur Bereich Mitarbeiterführung

200  
  
50

10.4.3.2

bei anderen Fortbildungsprüfungen je Prüfungsteil

170

**10.5**

**Anerkennung der Gleichwertigkeit von Berufsabschlüssen nach § 4 BBiG und von Fortbildungsprüfungen nach den §§ 53 und 54 BBiG**  
  
Anmerkung:  
  
Die Anerkennung der Bildungsnachweise von Berechtigten nach dem Bundesvertriebenengesetz sowie asylberechtigten Personen und anerkannten Flüchtlingen mit dauerndem Bleiberecht ist gebührenfrei.

30

**10.6**

**Feststellung und Anerkennung der Gleichwertigkeit für im Ausland erworbener Berufsqualifikation nach dem Berufsqualifikationsgesetz (BQFG)**

400

**11**

**Zulassung oder Anerkennung als private Kontrollstelle nach § 134 des Markengesetzes (MarkenG) sowie Zulassung als private Kontrollstelle nach § 5 Satz 2 des Lebensmittelspezialitätengesetzes (LSpG)**

**11.1**

**Erstzulassung oder Erstanerkennung von privaten Kontrollstellen zum Schutz von geografischen Angaben und Ursprungsbezeichnungen für Agrarerzeugnisse und Lebensmittel nach dem Markengesetz mit Sitz in Brandenburg**

11.1.1

Prüfung der Zulassungs- oder Anerkennungsunterlagen einschließlich Bescheid

230 bis 340

11.1.2

Inspektion der Kontrollstelle durch die Kontrollbehörde zwecks Überprüfung der Zulassungs- oder Anerkennungsvoraussetzungen nach DIN EN ISO/ICE 17065

80 bis 130

**11.2**

**Erstzulassung oder Erstanerkennung von privaten Kontrollstellen über Bescheinigung besonderer Merkmale von Agrarerzeugnissen und Lebensmitteln nach dem Lebensmittelspezialitätengesetz mit Sitz in Brandenburg**

11.2.1

Prüfung der Zulassungs- oder Anerkennungsunterlagen einschließlich Bescheid

230 bis 340

11.2.2

Inspektion der Kontrollstelle durch die Kontrollbehörde zwecks Überprüfung der Zulassungs- oder Anerkennungsvoraussetzungen nach DIN EN ISO/ICE 17065

80 bis 130

**11.3**

**Kontrollen beim erfassten Marktteilnehmer gemäß Verordnung (EG) Nr.
510/2006 des Rates vom 20. März 2006 zum Schutz von geografischen Angaben und Ursprungsbezeichnungen bei Agrarerzeugnissen und Lebensmitteln in Verbindung mit dem Markengesetz sowie Kontrollen beim erfassten Marktteilnehmer gemäß Verordnung (EG) Nr. 509/2006 des Rates vom 20. März 2006 über die garantiert traditionellen Spezialitäten bei Agrarerzeugnissen und Lebensmitteln in Verbindung mit dem Lebensmittelspezialitätengesetz**

11.3.1

Kontrollen beim erfassten Marktteilnehmer gemäß Artikel 37 der Verordnung (EG) Nr. 1151/2012 in Verbindung mit dem Markengesetz

nach Zeitaufwand

11.3.2

Kontrollen beim erfassten Marktteilnehmer gemäß Artikel 37 der Verordnung (EG) Nr. 1151/2012 in Verbindung mit dem Lebensmittelspezialitätengesetz

nach Zeitaufwand

**11.4**

**Amtshandlungen nach der Verordnung (EU) Nr. 1151/2012 des Europäischen Parlaments und des Rates vom 21. November 2012 über Qualitätsregelungen für Agrarerzeugnisse und Lebensmittel (ABI. L 343 vom 14.12.2012, S. 1)**

11.4.1

Erhebung von Gebühren gemäß Artikel 28 der Verordnung (EG) Nr. 882/2004 des Europäischen Parlaments und des Rates vom 29. April 2004 über amtliche Kontrollen zur Überprüfung der Einhaltung des Lebensmittel- und Futtermittelrechts sowie der Bestimmungen über Tiergesundheit und Tierschutz (ABI. L 165 vom 30.4.2004, S. 1) für Kosten aufgrund zusätzlicher amtlicher Kontrollen

in Verteilerzentren des LEH 90  
  
in sonstigen Fällen nach Aufwand

**12**

**Amtshandlungen nach dem Agrarmarktstrukturgesetz (AgrarMSG)**

**12.1**

**Prüfung eines Antrages auf Anerkennung einer Erzeugerorganisation oder ihrer Vereinigung sowie von Branchenverbänden nach AgrarMSG**

90

**12.2**

**Prüfung eines Antrages auf Verleihung der Rechtsfähigkeit nach § 22 BGB an eine Erzeugerorganisation oder ihre Vereinigung nach AgrarMSG**

51 bis 153

**13**

**Fischerei**

**13.1**

**Amtshandlungen nach dem Fischereigesetz für das Land Brandenburg (BbgFischG)**

13.1.1

Entscheidung über die Eintragung oder Berichtigungen im Fischereibuch nach § 4 Absatz 4 BbgFischG in Verbindung mit § 4 der Verordnung über die Einrichtung und Führung des Fischereibuches (FischBuV)

30 bis 210

13.1.2

Entscheidung über die räumliche Ausdehnung von Fischereirechten nach § 5 Absatz 2 Satz 2 BbgFischG

30 bis 1 030

13.1.3

Entscheidung zur Genehmigung von Übertragungsverträgen nach § 6 Absatz 1 Satz 3 BbgFischG

30 bis 110

13.1.4

Entscheidung zur Aufhebung eines beschränkten selbstständigen Fischereirechtes nach § 8 Absatz 2 Nummer 2 BbgFischG

30 bis 210

13.1.5

Entscheidung über Ausnahmen zur Mindestpachtzeit nach § 11 Absatz 1 Satz 3 BbgFischG

5 bis 30

13.1.6

Entscheidung über die Genehmigung des Hegeplanes nach § 24 Absatz 2 BbgFischG

25 bis 80

13.1.7

Entscheidung zur Einräumung des Rechts zum Betreten von Grundstücken und der Höhe der Entschädigung des Grundstückseigentümers nach § 16 Absatz 3 BbgFischG

30 bis 260

13.1.8

Erteilung von Fischereischeinen

13.1.8.1

Fischereischein nach § 17 Absatz 1 Nummer 1 und 2 BbgFischG

25

13.1.8.2

Zweitschrift eines Fischereischeines

10

13.1.8.3

Erklärung der Ungültigkeit und Einziehung eines Fischereischeines nach § 21 BbgFischG

25 bis 210

13.1.8.4

Jugendfischereischein nach § 17 Absatz 6 BbgFischG

2,5

13.1.9

Entscheidung zur Bildung eines gemeinschaftlichen Fischereibezirkes auf Antrag eines Fischereiberechtigten nach § 23 Absatz 2 BbgFischG

30 bis 210

13.1.10

Entscheidung über die Zulassung der Verwendung von künstlichem Licht und Elektrizität nach § 26 Absatz 2 BbgFischG

30 bis 360

13.1.11

Zweitschrift zur Zulassung der Verwendung von künstlichem Licht und Elektrizität nach § 26 Absatz 2 BbgFischG

15

13.1.12

Entscheidung zur Zulassung von Ausnahmen für den Aalfang nach § 29 Absatz 2 BbgFischG

10 bis 260

13.1.13

Entscheidung zur Zulassung von Ausnahmen zum Beseitigen und Abstellen von Fischereivorrichtungen während der Schonzeit nach § 29 Absatz 3 BbgFischG

10 bis 110

13.1.14

Entscheidung zur Zulassung von Ausnahmen über die Anlegung und Unterhaltung von Fischwegen nach § 30 Absatz 3 BbgFischG

60 bis 160

13.1.15

Entscheidung zur Zulassung von Ausnahmen zum Fischfangverbot in und an Fischwegen zu wissenschaftlichen und fischereiwirtschaftlichen Zwecken nach § 30 Absatz 8 BbgFischG

30 bis 60

13.1.16

vorläufige Regelung der Fischereiausübung nach § 11 Absatz 5 BbgFischG

25 bis 160

13.1.17

Genehmigung der Satzung von Fischereigenossenschaften nach § 25 Absatz 2 BbgFischG

25 bis 80

13.1.18

Entscheidung von Entschädigungsansprüchen nach § 35 BbgFischG

60 bis 1 600

**13.2**

**Amtshandlungen nach der Fischereiordnung des Landes Brandenburg (BbgFischO)**

13.2.1

Entscheidung zur Zulassung von Ausnahmen zu den Bestimmungen über Mindestmaße und Schonzeiten nach § 2 Absatz 2 BbgFischO

30 bis 160

13.2.2

Entscheidung zur Zulassung des Fischfangs mit lebendem Köderfisch nach § 6 Absatz 1 BbgFischO

30 bis 60

13.2.3

Entscheidung zur Zulassung von Ausnahmen zu dem Zeitraum der Ausübung des Fischfangs mit der Handangel bei Vorliegen von Koppelfischerei nach § 7 Absatz 4 BbgFischO

10 bis 110

13.2.4

schriftliche Genehmigung einer Angelveranstaltung nach § 8 Absatz 1 BbgFischO

5

13.2.5

Entscheidung zur Zulassung von Ausnahmen zum Besatz in Gewässern mit Vorkommen von sich selbst reproduzierenden Beständen nach § 12 Absatz 3 BbgFischO

10 bis 30

13.2.6

Entscheidung zur Erteilung der Genehmigung zum Aussetzen nicht heimischer Fische nach § 13 Absatz 1 BbgFischO

60 bis 260

13.2.7

Entscheidung zum Einsatz ortsfester Elektroanlagen zum Scheuchen und Abweichen von Fischen nach § 24 Absatz 2 BbgFischO

60 bis 520

13.2.8

Entscheidung über die Genehmigung gemäß Artikel 6 der Verordnung (EG) Nr. 708/2007 des Rates vom 11. Juni 2007 über die Verwendung nicht heimischer und gebietsfremder Arten in der Aquakultur in der jeweils geltenden Fassung in Verbindung mit § 13 Absatz 4 BbgFischO

300 bis 1 000

13.2.9

Entscheidung über Ausnahmen zum Betreiben von Fischteichen und bewirtschafteten Anlagen der Fischzucht und -haltung gemäß § 26a Absatz 2 BbgFischO

30 bis 60

**13.3**

**Amtshandlungen nach der Verordnung über die Anglerprüfung**

13.3.1

Entscheidung zur Anerkennung von natürlichen oder juristischen Personen des Privatrechts für die Organisation und Durchführung der Anglerprüfung nach § 1 der Verordnung über die Anglerprüfung

50 bis 500

**14**

**Amtshandlungen nach dem Einkommensteuergesetzes (EStG)**

**14.1**

**Erstellung einer Bescheinigung nach § 14a Absatz 3 Nummer 2 EStG**

31 bis 92

**14.2**

**Anerkennung von Betriebsgutachten im Sinne von § 68 Absatz 2 der Einkommensteuer-Durchführungsverordnung (EStDV)**

50 bis 1 500

**15**

**Amtshandlungen nach dem Düngegesetz (DüngG)**

15.1

Erteilung einer Anordnung nach § 13 DüngG

15.2

Anerkennung eines Trägers der Qualitätssicherung im Bereich von Wirtschaftsdüngern nach § 13a DüngG

130 bis 2 500

15.3

Zulassung anderer Methoden und Verfahren zur Ermittlung des Düngebedarfs nach § 4 Absatz 1 der Düngeverordnung (DüV)

40  bis 210

15.4

Erteilung einer Genehmigung zur Nutzung anderer Verfahren für die Aufbringung von flüssigen organischen oder organisch-mineralischen Düngemitteln nach § 6 Absatz 3 DüV

40 bis 210

15.5

Genehmigung einer Ausnahme von den Vorgaben zur Ausbringung von flüssigen organischen oder organisch-mineralischen Düngemitteln nach § 6 Absatz 3 DüV

40 bis 210

15.6

Genehmigung einer Ausnahme für das Aufbringen höherer Mengen von Wirtschaftsdüngern tierischer Herkunft nach § 6 Absatz 5 DüV

40 bis 210

15.7

Genehmigung einer Ausnahme für das Aufbringen höherer Mengen von flüssigen organischen oder organisch-mineralischen Düngemitteln, bei denen es sich um Gärrückstände aus dem Betrieb einer Biogasanlage handelt, auf Ackerland mit mehrjährigem Futterbau, Grünland oder Dauergrünland nach § 6 Absatz 6 DüV

40 bis 210

15.8

Erteilung einer Genehmigung zur Verschiebung des Verbotszeitraums nach § 6 Absatz 10 DüV

40 bis 210

15.9

Genehmigung einer Ausnahme von den Verbotszeiträumen bei der Aufbringung von Düngemitteln mit einem festgestellten Gehalt an Trockenmasse von weniger als 2 Prozent nach § 6 Absatz 10 DüV

40 bis 210

15.10

Anerkennung von Düngeberatern nach § 9 Absatz 4 DüV und § 6 Absatz 5 Stoffstrombilanzverordnung

40 bis 210

15.11

Anordnung zur Teilnahme an einer anerkannten Düngeberatung nach § 9 Absatz 4 DüV

40 bis 210

**16**

**Amtshandlungen nach den Verordnungen der gemeinsamen Marktorganisation (GMO)**

**16.1**

**Amtshandlungen Obst/Gemüse gemäß Delegierte Verordnung (EU) 2017/891**

16.1.1

Zusätzliche Kontrollen bei Unregelmäßigkeiten

nach Zeitaufwand

16.1.1.1

bei operationellen Programmen

nach Zeitaufwand

16.1.1.2

Sanktionsmaßnahmen im Rahmen von operationellen Programmen

41

**16.2**

**Qualitätskontrolle Sektor Obst und Gemüse gemäß Durchführungsverordnung (EU) Nr. 543/2011**

16.2.1

Exportkontrollen und Ausstellung von Bescheinigungen gemäß Artikel 14

nach Zeitaufwand

16.2.2

Nachkontrollen (auch im Einzelhandel) gemäß Artikel 11

nach Zeitaufwand

16.2.3

Durchführung einer zusätzlichen Gesamtprobe, Ausstellung eines Kontrollberichts einschließlich Anlage und Bescheid gemäß Artikel 17 Absatz 1 (bei mindestens fünf zu entnehmenden Packstücken \[verpackte Erzeugnisse\] oder mindestens 10 kg Masse der Einzelprobe bzw.
zehn zu entnehmenden Einheiten \[lose Erzeugnisse\])

20 bis 41

**16.3**

**Amtshandlungen nach der gemeinsamen Marktorganisation Sektor Eier gemäß der Verordnung (EU) Nr. 1308/2013**

16.3.1

Entscheidung über die Erlaubnis zum Sortieren und Verpacken von Eiern gemäß Artikel 5 Absatz 2 der Verordnung (EG) Nr. 589/2008 der Kommission vom 23. Juni 2008

30 bis 250

16.3.2

Nachkontrollen von Zulassungsbedingungen als Packstelle gemäß Artikel 5 Absatz 3 der Verordnung (EG) Nr. 589/2008

20 bis 100

16.3.3

Nachkontrollen zur Einhaltung der Anforderungen gemäß Verordnung (EG) Nr.
589/2008 bei Packstellen

bis 3 000 Eier je Woche

45

mehr als 3 000 bis 35 000 Eier je Woche

90

mehr als 35 000 bis 70 000 Eier je Woche

135

mehr als 70 000 Eier je Woche

180

16.3.4

Entscheidung über die Zulassung von Brütereien

30 bis 50

**16.4**

**gemeinsame Marktorganisation Sektor Käse  
  
Erteilung der Berechtigung zur Führung der Bezeichnung „Markenkäse“ nach der Käseverordnung (KäseV)**

210

**16.5**

**gemeinsame Marktorganisation Sektor Butter  
  
Erteilung der Berechtigung zur Führung der Bezeichnung „Deutsche Markenbutter“ nach der Butterverordnung (ButtV)**

210

**16.6**

**gemeinsame Marktorganisation Sektor Fleisch  
  
Schulungsmaßnahmen für Sachverständige zur Einreihung von Fleisch in Handelsklassen und Gewichtsfeststellung, je Teilnehmer und Lehrgang**

80

**16.7**

**Kontrollen nach dem Fleischgesetz (FlG) für die Einreihung von Fleisch in Handelsklassen und Gewichtsfeststellung auf Anforderung**

16.7.1

RinderGrundgebühr und zusätzlich entsprechend Anzahl

50

16.7.1.1

ab 10 bis 20 Rinder

80

16.7.1.2

ab 21 bis 40 Rinder

110

16.7.1.3

mehr als 40 Rinder

200

16.7.2

Schweine oder Schafe  
  
Grundgebühr und zusätzlich entsprechend Anzahl

50

16.7.2.1

ab 30 bis 50 Schweine oder Schafe

80

16.7.2.2

ab 51 bis 200 Schweine oder Schafe

110

16.7.2.3

mehr als 200 Schweine oder Schafe

200

**16.8**

**Erteilung einer Erlaubnis zum Betrieb eines milchwirtschaftlichen Unternehmens gemäß § 4 des Milch- und Margarinegesetzes (MilchMargG)**

130 bis 180

**16.9**

**Amtshandlungen nach der gemeinsamen Marktorganisation Sektor Geflügelfleisch gemäß der Verordnung (EU) Nr. 1308/2013**

16.9.1

Nachkontrollen gemäß der Verordnung (EG) Nr. 543/2008 der Kommission vom 16. Juni 2008

90 bis 180

**16.10**

**Amtshandlungen nach dem Legehennenbetriebsregistergesetz (LegRegG)**

16.10.1

Nachkontrollen gemäß § 7 LegRegG nach Beanstandungen

ab 350 bis 1 000 Legehennenplätze

50

mehr als 1 000 bis 10 000 Legehennenplätze

80

mehr als 10 000 bis 50 000 Legehennenplätze

110

mehr als 50 000 Legehennenplätze

200

**17**

**Amtshandlungen nach dem Holzhandels-Sicherungs-Gesetz (HolzSiG)**

nach Aufwand

**18**

**Vollzug allgemeiner umweltrechtlicher Vorschriften  
  
Amtshandlungen gegenüber dem Verantwortlichen nach dem Umweltschadensgesetz (USchadG), soweit diese nicht von anderen fachspezifischen Tarifstellen erfasst werden**

nach Zeitaufwand