## Verordnung zur Regelung der Zuständigkeiten nach dem Pflegeberufegesetz (Pflegeberufezuständigkeitsverordnung - PflBGZV)

#### § 1 Zuständigkeit des Landesamtes für Arbeitsschutz, Verbraucherschutz und Gesundheit

(1) Das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit ist zuständig

1.  für die Durchführung des Pflegeberufegesetzes, soweit sich aus den nachfolgenden Vorschriften nichts anderes ergibt,
2.  für die Durchführung der nach § 56 Absatz 1 des Pflegeberufegesetzes erlassenen Ausbildungs- und Prüfungsverordnung.

(2) Das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit ist zuständig für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 57 des Pflegeberufegesetzes.

#### § 2 Zuständigkeit des Landesamtes für Soziales und Versorgung

Das Landesamt für Soziales und Versorgung ist

1.  zuständige Stelle nach § 26 Absatz 4 des Pflegeberufegesetzes,
2.  zuständige Behörde nach § 30 Absatz 1 des Pflegeberufegesetzes,
3.  zuständige Behörde nach § 31 Absatz 1 Satz 1 Nummer 2 des Pflegeberufegesetzes,
4.  zuständig für die Durchführung der nach § 56 Absatz 3 des Pflegeberufegesetzes erlassenen Verordnung,
5.  zuständig für die Durchführung der Ausbildungen, die nach § 66 Absatz 2 des Pflegeberufegesetzes auf der Grundlage der am 31.
    Dezember 2019 geltenden Vorschriften des Altenpflegegesetzes abgeschlossen werden.

#### § 3 Zuständigkeit des für Soziales und Gesundheit zuständigen Ministeriums

Das für Soziales und Gesundheit zuständige Ministerium ist

1.  die weitere zuständige Behörde nach § 26 Absatz 6 Satz 2 des Pflegeberufegesetzes,
2.  zuständiges Landesministerium nach § 26 Absatz 6 Satz 3 des Pflegeberufegesetzes.