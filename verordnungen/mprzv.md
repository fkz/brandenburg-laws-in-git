## Verordnung über die Zuständigkeiten im Medizinprodukterecht (Medizinprodukterechtzuständigkeitsverordnung - MPRZV)

Auf Grund des § 6 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl. I S. 186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28 S.
2) geändert worden ist, und § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19. Februar 1987 (BGBl.
I S.
602) verordnet die Landesregierung:

#### § 1 Zuständigkeiten

(1) Das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit ist die zuständige Behörde für die Durchführung

1.  der Verordnung (EU) 2017/745 des Europäischen Parlaments und des Rates vom 5. April 2017 über Medizinprodukte, zur Änderung der Richtlinie 2001/83/EG, der Verordnung (EG) Nr. 178/2002 und der Verordnung (EG) Nr. 1223/2009 und zur Aufhebung der Richtlinien 90/385/EWG und 93/42/EWG des Rates (ABl. L 117 vom 5.5.2017, S.
    1), sowie der auf Grund dieser Verordnung erlassenen delegierten Rechtsakte und Durchführungsrechtsakte,
2.  der Verordnung (EU) 2017/746 des Europäischen Parlaments und des Rates vom 5. April 2017 über In-Vitro-Diagnostika und zur Aufhebung der Richtlinie 98/79/EG und des Beschlusses 2010/227/EU der Kommission (ABl.
    L 117 vom 5.5.2017, S.
    176), sowie der auf Grund dieser Verordnung erlassenen delegierten Rechtsakte und Durchführungsrechtsakte und
3.  des Medizinproduktegesetzes und des Medizinprodukterecht-Durchführungsgesetzes sowie der auf Grund dieser Gesetze erlassenen Rechtsverordnungen,

soweit sich aus den nachfolgenden Regelungen nicht etwas anderes ergibt.

(2) Das Landesamt für Mess- und Eichwesen Berlin-Brandenburg ist unbeschadet des Absatzes 3 die zuständige Behörde für die Durchführung der in Absatz 1 genannten Rechtsvorschriften für Medizinprodukte mit Messfunktion nach Anlage 2 der Medizinprodukte-Betreiberverordnung.
Dieses ist darüber hinaus die zuständige Behörde für

1.  die Überwachung von medizinischen Laboratorien nach § 9 Absatz 1 der Medizinprodukte-Betreiberverordnung in denen Kontrolluntersuchungen und Vergleichsmessungen durchgeführt werden,
2.  die Entgegennahme von Anzeigen von Personen, die beabsichtigen, künftig messtechnische Kontrollen durchzuführen sowie die Überprüfung des Vorliegens der Voraussetzungen nach § 14 Absatz 6 in Verbindung mit § 5 der Medizinprodukte-Betreiberverordnung und
3.  die Überwachung der Betreiber hinsichtlich der Einhaltung der festgelegten Fristen für Medizinprodukte mit Messfunktion, die nach § 14 Absatz 4 in Verbindung mit Anlage 2 der Medizinprodukte-Betreiberverordnung messtechnischen Kontrollen unterliegen.

(3) Das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit ist abweichend von Absatz 2 Satz 1 die zuständige Behörde auch für Medizinprodukte mit Messfunktion in den folgenden Fällen:

1.  Ausstellung einer Bescheinigung über die Verkehrsfähigkeit des Medizinproduktes nach § 34 Absatz 1 des Medizinproduktegesetzes,
2.  Ausstellung eines Freiverkaufszertifikates für Exportzwecke nach Artikel 60 Absatz 1 der Verordnung (EU) 2017/745 und Artikel 55 Absatz 1 der Verordnung (EU) 2017/746, jeweils in Verbindung mit § 10 des Medizinprodukterecht-Durchführungsgesetzes,
3.  Bearbeitung von Anzeigen nach § 25 Absatz 1 bis 5 des Medizinproduktegesetzes sowie nach Artikel 31 Absatz 2 der Verordnung (EU) 2017/745 und Artikel 28 Absatz 2 der Verordnung (EU) 2017/746.

(4) Das für Gesundheit zuständige Ministerium ist die zuständige Behörde für

1.  die Entgegennahme der Meldungen von Vorkommnissen und schwerwiegenden unerwünschten Ereignissen nach § 20 Absatz 1 der Medizinprodukte-Sicherheitsplanverordnung sowie
2.  die Entgegennahme der Meldungen von schwerwiegenden Vorkommnissen, mutmaßlichen schwerwiegenden Vorkommnissen, schwerwiegenden Gefahren und Sicherheitskorrekturmaßnahmen im Feld sowie über den Abschluss und das Ergebnis der durchgeführten Risikobewertung, einschließlich angeordneter Maßnahmen nach § 8 der Medizinprodukte-Anwendermelde- und Informationsverordnung.

#### § 2 Ordnungswidrigkeiten

Die nach § 1 zuständigen Behörden sind zuständig für die Verfolgung und Ahndung von Ordnungswidrigkeiten.

#### § 3 Subsidiarität

Zuständigkeitsregelungen anderer Rechtsvorschriften gehen vor.

#### § 4 Inkrafttreten, Außerkrafttreten

(1) Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

(2) Gleichzeitig tritt die Verordnung über Zuständigkeiten nach dem Medizinproduktegesetz, der Medizinprodukte-Betreiberverordnung und der Medizinprodukte-Sicherheitsplanverordnung vom 9. Februar 2005 (GVBl.
II S. 138), die zuletzt durch Artikel 23 des Gesetzes vom 25.Januar 2016 (GVBl. I Nr. 5 S. 21) geändert worden ist, außer Kraft.

Potsdam, den 8.
Juni 2021

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Die Ministerin für Soziales, Gesundheit,  
Integration und Verbraucherschutz

In Vertretung

Anna Heyer-Stuffer  
  

Der Minister für Wirtschaft,  
Arbeit und Energie

Prof.
Dr.-Ing.
Jörg Steinbach