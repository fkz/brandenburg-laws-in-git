## Brandenburgische Verordnung zur Schaffung barrierefreier Informationstechnik nach dem Brandenburgischen Behindertengleichstellungsgesetz (Brandenburgische Barrierefreie Informationstechnik-Verordnung - BbgBITV)

Auf Grund des § 9 Absatz 2 des Brandenburgischen Behindertengleichstellungsgesetzes vom 11. Februar 2013 (GVBl. I Nr. 5), der durch Artikel 4 des Gesetzes vom 20. September 2018 (GVBl. I Nr. 21 S. 5) geändert worden ist, verordnet die Ministerin für Arbeit, Soziales, Gesundheit, Frauen und Familie im Einvernehmen mit dem Minister des Innern und für Kommunales:

#### § 1 Barrierefreie Angebote der Informationstechnik

(1) Träger der öffentlichen Verwaltung gemäß § 2 Absatz 1 des Brandenburgischen Behindertengleichstellungsgesetzes vom 11. Februar 2013 (GVBl. I Nr. 5), das zuletzt durch Artikel 8 des Gesetzes vom 18. Dezember 2018 (GVBl. I Nr. 38 S. 16) geändert worden ist, gestalten die in § 9 Absatz 1 des Brandenburgischen Behindertengleichstellungsgesetzes beschriebenen Angebote der Informationstechnik im Sinne des Artikels 1 der Richtlinie (EU) 2016/2102 des Europäischen Parlaments und des Rates vom 26. Oktober 2016 über den barrierefreien Zugang zu den Websites und mobilen Anwendungen öffentlicher Stellen (ABl. L 327 vom 2.12.2016, S. 1) barrierefrei.
Für Websites und mobile Anwendungen im Sinne des Artikels 1 der Richtlinie (EU) 2016/2102 gilt für öffentliche Stellen im Sinne des Artikels 3 Nummer 1 der Richtlinie (EU) 2016/2102 Satz 1 entsprechend.

(2) Von einem barrierefreien Angebot nach Absatz 1 kann im Einzelfall abgesehen werden, wenn die Einhaltung der Barrierefreiheitsanforderungen eine unverhältnismäßige Belastung nach den Kriterien des Artikels 5 der Richtlinie (EU) 2016/2102 darstellt.
Eine unverhältnismäßige Belastung kann in begründeten Einzelfällen vorliegen, wenn es einer öffentlichen Stelle vernünftigerweise nicht möglich ist, spezifische Inhalte uneingeschränkt barrierefrei zugänglich zu machen.
Nimmt eine öffentliche Stelle für eine bestimmte Website oder mobile Anwendung nach der Durchführung der Bewertung nach Artikel 5 Absatz 2 und 3 der Richtlinie (EU) 2016/2102 diese Ausnahmeregelung in Anspruch, so erläutert sie in der Erklärung nach § 3, welche Teile der Barrierefreiheitsanforderungen nicht erfüllt werden konnten und schlägt gegebenenfalls barrierefrei zugängliche Alternativen vor.

(3) Die Regelungen in den Absätzen 1 und 2 gelten nicht für Websites und mobile Anwendungen von Schulen und Kindertageseinrichtungen mit Ausnahme der Inhalte, die sich auf wesentliche Online-Verwaltungsfunktionen beziehen.

#### § 2 Anzuwendende Standards

(1) Angebote der Informationstechnik gelten als barrierefrei, wenn sie wahrnehmbar, bedienbar, verständlich und robust sind.
Die Erfüllung der Anforderungen nach Satz 1 wird vermutet, wenn die Angebote der Informationstechnik

1.  harmonisierten Normen oder Teilen dieser Normen entsprechen, und
2.  die harmonisierten Normen oder Teile dieser Normen im Amtsblatt der Europäischen Union genannt worden sind.

(2) Soweit Teile von Angeboten der Informationstechnik nicht von harmonisierten Normen abgedeckt sind, sind sie nach dem Stand der Technik barrierefrei zu gestalten.

(3) Die regelmäßig auf der Website der Bundesfachstelle für Barrierefreiheit veröffentlichten Informationen in deutscher Sprache zur Umsetzung der Richtlinie (EU) 2016/2102, wie

1.  aktuelle Informationen zu den zu beachtenden Standards, aus denen die Barrierefreiheitsanforderungen detailliert hervorgehen,
2.  Konformitätstabellen, die einen Überblick zu den wichtigsten Barrierefreiheitsanforderungen geben,
3.  Empfehlungen und weiterführende Erläuterungen,

sind zu beachten.

#### § 3 Erklärung zur Barrierefreiheit

Die Verpflichteten nach § 1 Absatz 1 veröffentlichen nach Maßgabe von Artikel 5 Absatz 4 und Artikel 7 Absatz 1 der Richtlinie (EU) 2016/2102 sowie der nach Artikel 7 Absatz 2 Satz 1 dieser Richtlinie erlassenen Durchführungsrechtsakte eine Erklärung zur Barrierefreiheit.
Sie stellen über die jeweilige Website oder mobile Anwendung eine Kontaktmöglichkeit bereit, über die nutzende Personen der betreffenden öffentlichen Stelle Mängel bei der Einhaltung der Anforderungen an die Barrierefreiheit mitteilen oder Informationen, die nicht barrierefrei dargestellt werden müssen, anfordern können.

#### § 4 Überwachungs- und Durchsetzungsverfahren, Berichterstattung

(1) Das Landesamt für Soziales und Versorgung überwacht nach Maßgabe der nach Artikel 8 Absatz 1 bis 3 der Richtlinie (EU) 2016/2102 erlassenen Durchführungsrechtsakte die Einhaltung der Verpflichtungen nach den §§ 1 bis 3.
Für den Bereich der Justiz übernimmt das für Justiz zuständige Mitglied der Landesregierung die Überwachung im Sinne des Satzes 1.
Die Verpflichteten nach § 1 Absatz 1 sind verpflichtet, die jeweilige Überwachungsstelle bei der Erfüllung ihrer Aufgabe zu unterstützen.

(2) Bleibt eine Anfrage über die Kontaktmöglichkeit nach § 3 Satz 2 innerhalb von drei Wochen ganz oder teilweise unbeantwortet, prüft die Überwachungsstelle auf Antrag der nutzenden Person, ob im Rahmen der Überwachung nach Absatz 1 gegenüber der öffentlichen Stelle Maßnahmen erforderlich sind.
Satz 1 gilt entsprechend, wenn die nutzende Person geltend macht, dass sich die öffentliche Stelle zu Unrecht auf eine Ausnahme nach § 1 Absatz 2 beruft.

(3) Bei der beauftragten Person der Landesregierung für die Belange der Menschen mit Behinderungen wird eine Durchsetzungsstelle eingerichtet, die für das Durchsetzungsverfahren im Sinne des Artikels 9 der Richtlinie (EU) 2016/2102 zuständig ist.
Die Durchsetzungsstelle kann im Einzelfall die Überprüfung einer Website oder mobilen Anwendung einer öffentlichen Stelle verlangen.
Die Verpflichteten nach § 1 Absatz 1 sind verpflichtet, die Durchsetzungsstelle bei der Erfüllung ihrer Aufgaben zu unterstützen.

(4) Das Landesamt für Soziales und Versorgung berichtet zum 30. März 2021 und danach alle drei Jahre jeweils zum 30. März an die zuständige Stelle des Bundes über den Stand der Barrierefreiheit.
Zu berichten ist insbesondere über die Ergebnisse der Überwachung nach Artikel 8 Absatz 1 bis 3 der Richtlinie (EU) 2016/2102.
Art und Form des Berichts richten sich nach den Anforderungen, die auf der Grundlage des Artikels 8 Absatz 6 der Richtlinie (EU) 2016/2102 festgelegt werden.
Die Verpflichteten nach § 1 Absatz 1 sind verpflichtet, die für die Berichterstellung erforderlichen Daten zur Verfügung zu stellen.

#### § 5 Übergangsvorschriften

Die Vorschriften dieser Verordnung sind anzuwenden

1.  auf Websites öffentlicher Stellen im Sinne des Artikels 3 Nummer 1 der Richtlinie (EU) 2016/2102
    1.  die nicht vor dem 23. September 2018 veröffentlicht wurden, ab dem 23. September 2019,
    2.  im Übrigen ab dem 23. September 2020,
2.  auf mobile Anwendungen öffentlicher Stellen im Sinne des Artikels 3 Nummer 1 der Richtlinie (EU) 2016/2102 ab dem 23. Juni 2021.

#### § 6 Überprüfungsregelung

Die Verordnung ist unter Berücksichtigung der technischen Entwicklung regelmäßig zu überprüfen.
Das für Soziales zuständige Mitglied der Landesregierung hat dem Landtag erstmals im Jahr 2021 und danach im Abstand von drei Jahren über den Stand der Barrierefreiheit zu berichten.
Die Berichte sollen Vorschläge für gegebenenfalls notwendige Anpassungen der Regelungen enthalten.

#### § 7 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Brandenburgische Barrierefreie Informationstechnik-Verordnung vom 24. Mai 2004 (GVBl. II S. 482) außer Kraft.

Potsdam, den 17.
September 2019

Die Ministerin für Arbeit, Soziales,  
Gesundheit, Frauen und Familie

Susanna Karawanskij