## Verordnung über die Aufwandsentschädigungen für ehrenamtliche Mitglieder kommunaler Vertretungen und Ausschüsse, für sachkundige Einwohnerinnen und Einwohner sowie über den Ersatz des Verdienstausfalls (Kommunalaufwandsentschädigungsverordnung - KomAEV)

Auf Grund des § 30 Absatz 4 Satz 5 in Verbindung mit § 43 Absatz 4 Satz 4, des § 131 Absatz 2 und des § 140 Absatz 2 der Kommunalverfassung des Landes Brandenburg vom 18. Dezember 2007 (GVBl. I S. 286) sowie des § 15 Absatz 3 und des § 25 Absatz 3 des Verbandsgemeinde- und Mitverwaltungsgesetzes vom 15.
Oktober 2018 (GVBl.
I Nr. 22 S.
2) verordnet der Minister des Innern und für Kommunales:

#### § 1 Geltungsbereich

Diese Verordnung gilt für die ehrenamtlichen Mitglieder der Vertretungen und Ausschüsse von Gemeinden, Ämtern, Verbandsgemeinden, Landkreisen und für sachkundige Einwohnerinnen und Einwohner.

#### § 2 Grundsätze

Den Mitgliedern kommunaler Vertretungen und den sachkundigen Einwohnerinnen und Einwohnern kann zur Abdeckung des mit dem Mandat verbundenen Aufwandes eine Aufwandsentschädigung gewährt werden.
Aufwandsentschädigungen, die als Pauschalen gewährt werden, sollen so bemessen sein, dass der mit dem Amt verbundene Aufwand, einschließlich der sonstigen persönlichen Aufwendungen, abgegolten wird.
Zu den persönlichen Aufwendungen zählen insbesondere zusätzlicher Bekleidungsaufwand, Kosten für Verzehr, Fachliteratur und Nutzung der Telekommunikation sowie in einem in der Entschädigungssatzung festzulegenden Rahmen Fahrtkosten.
Es kann eine zusätzliche pauschale Aufwandsentschädigung für Vorsitzende gemäß § 7 gewährt werden.
Weitere besondere Aufwendungen können gemäß den §§ 9, 10, 12, 13 und 14 entschädigt werden.
Es besteht zudem ein Anspruch auf Ersatz des Verdienstausfalls nach Maßgabe des § 11.

#### § 3 Einwohnerzahlen

(1) Soweit in dieser Verordnung auf die Einwohnerzahl abgestellt wird, ist die durch das Amt für Statistik Berlin-Brandenburg fortgeschriebene Einwohnerzahl am 30.
Juni des Vorjahres maßgebend.
Im Jahr einer Kommunalwahl ist der 30.
Juni des Wahljahres maßgebend, wenn die neue Wahlperiode nach diesem Tag beginnt und die fortgeschriebene Einwohnerzahl bereits bekannt gegeben ist.

(2) Bei Unterschreiten eines Einwohnergrenzwertes infolge einer Verminderung der Einwohnerzahl ist spätestens mit der nächsten Kommunalwahlperiode die Aufwandsentschädigung neu festzusetzen.
Bei Überschreiten eines höheren Grenzwertes um mehr als zehn Prozent des höheren Grenzwertes kann die Anpassung während der laufenden Kommunalwahlperiode vorgenommen werden.

#### § 4 Form der Regelung

Der Personenkreis, die Höhe der Aufwandsentschädigungen, einschließlich des Sitzungsgeldes, und des Verdienstausfallersatzes sowie die Höhe der zu gewährenden Reisekostenvergütung und Zahlungsbestimmungen, insbesondere zur Entstehung und zum Ende von Zahlungsansprüchen, sind in einer Entschädigungssatzung zu regeln.

#### § 5 Ergänzende Zahlungsbestimmungen

(1) Die pauschale Aufwandsentschädigung wird monatlich gezahlt.
Sie kann nachträglich gezahlt werden.
Die Entschädigungssatzung kann Regelungen treffen, die bei einer Nichtausübung des Mandats oder bei unentschuldigter Nichtteilnahme an Sitzungen zu einer Verminderung der Aufwandsentschädigung beziehungsweise zu einer Rückforderung führt.

(2) Das den ehrenamtlichen Mitgliedern kommunaler Vertretungen gewährte Sitzungsgeld für Sitzungen der Vertretungen und der Ausschüsse ist spätestens nach drei Monaten auszuzahlen.
Die Entschädigungssatzung kann vorsehen, dass für mehrere Sitzungen an einem Tag in der Eigenschaft eines Vertreters einer Gebietskörperschaft nur ein Sitzungsgeld gewährt werden darf.

(3) Hinsichtlich der in dieser Verordnung festgelegten Höchstbeträge gilt der Nachweis der Angemessenheit der Höhe von Aufwandsentschädigungen für entsprechende Regelungen in den Entschädigungssatzungen als erbracht.

#### § 6 Aufwandsentschädigung für Mitglieder kommunaler Vertretungen

(1) Die monatliche pauschale Aufwandsentschädigung für Gemeindevertreterinnen und Gemeindevertreter und für Verbandsgemeindevertreterinnen und Verbandsgemeindevertreter darf die folgenden Höchstsätze nicht überschreiten:

in Gemeinden und Verbandsgemeinden mit einer Einwohnerzahl

bis

5 000

70 Euro,

von

5 001

bis

10 000

90 Euro,

von

10 001

bis

20 000

110 Euro,

von

20 001

bis

30 000

150 Euro,

von

30 001

bis

50 000

180 Euro,

von

50 001

bis

80 000

220 Euro,

von

80 001

bis

150 000

250 Euro,

über

150 000

320 Euro.

(2) Mitglieder des Amtsausschusses können eine monatliche Aufwandsentschädigung entsprechend den Höchstsätzen des Absatzes 1 erhalten.
Als Einwohnerzahl ist die Einwohnerzahl des Amtes zugrunde zu legen.

(3) Die monatliche Aufwandsentschädigung darf bei Kreistagsabgeordneten folgende Höchstsätze nicht überschreiten:

in Landkreisen bis 150 000 Einwohner

250 Euro,

in Landkreisen über 150 000 Einwohner

320 Euro.

#### § 7 Zusätzliche Aufwandsentschädigung

(1) Vorsitzenden kann neben der Aufwandsentschädigung nach § 6 eine zusätzliche monatliche pauschale Aufwandsentschädigung gewährt werden, die folgende Höchstsätze nicht überschreiten darf:

1.  für die Vorsitzenden der Gemeindevertretungen und die Vorsitzenden der Verbandsgemeindevertretungen, soweit sie nicht gleichzeitig ehrenamtliche Bürgermeisterinnen und ehrenamtliche Bürgermeister sind, von Gemeinden und Verbandsgemeinden mit einer Einwohnerzahl
    
    bis
    
    5 000
    
    260 Euro,
    
    von
    
    5 001
    
    bis
    
    10 000
    
    340 Euro,
    
    von
    
    10 001
    
    bis
    
    20 000
    
    450 Euro,
    
    von
    
    20 001
    
    bis
    
    50 000
    
    710 Euro,
    
    von
    
    50 001
    
    bis
    
    150 000
    
    980 Euro,
    
    über
    
    150 000
    
    1 260 Euro;
    
2.  für die Vorsitzenden der Kreistage von Landkreisen mit einer Einwohnerzahl
    
    bis
    
    150 000
    
    980 Euro,
    
    über
    
    150 000
    
    1 260 Euro;
    
3.  für die Fraktionsvorsitzenden in Gemeindevertretungen und Verbandsgemeindevertretungen von Gemeinden mit einer Einwohnerzahl
    
    ab
    
    1 000
    
    bis
    
    5 000
    
    70 Euro,
    
    von
    
    5 001
    
    bis
    
    10 000
    
    90 Euro,
    
    von
    
    10 001
    
    bis
    
    20 000
    
    110 Euro,
    
    von
    
    20 001
    
    bis
    
    50 000
    
    180 Euro,
    
    von
    
    50 001
    
    bis
    
    150 000
    
    250 Euro,
    
    über
    
    150 000
    
    320 Euro,
    
    wobei es zulässig ist, in der Entschädigungssatzung innerhalb des durch den Höchstsatz gesetzten Rahmens nach der Fraktionsgröße zu differenzieren;
4.  für die Fraktionsvorsitzenden in Kreistagen von Landkreisen mit einer Einwohnerzahl
    
    bis
    
    150 000
    
    250 Euro,
    
    über
    
    150 000
    
    320 Euro,
    
    wobei es zulässig ist, in der Entschädigungssatzung innerhalb des durch den Höchstsatz gesetzten Rahmens nach der Fraktionsgröße zu differenzieren;
5.  für die Vorsitzenden der Amtsausschüsse von Ämtern mit einer Einwohnerzahl
    
    bis
    
    5 000
    
    250 Euro,
    
    von
    
    5 001
    
    bis
    
    10 000
    
    340 Euro,
    
    von
    
    10 001
    
    bis
    
    20 000
    
    430 Euro,
    
    über
    
    20 000
    
    710 Euro;
    
6.  für die Vorsitzenden der Hauptausschüsse, soweit sie nicht hauptamtliche Bürgermeisterinnen oder hauptamtliche Bürgermeister sind, von amtsfreien Gemeinden und Verbandsgemeinden mit einer Einwohnerzahl
    
    bis
    
    5 000
    
    220 Euro,
    
    von
    
    5 001
    
    bis
    
    10 000
    
    290 Euro,
    
    von
    
    10 001
    
    bis
    
    20 000
    
    360 Euro,
    
    von
    
    20 001
    
    bis
    
    50 000
    
    630 Euro,
    
    von
    
    50 001
    
    bis
    
    100 000
    
    770 Euro,
    
    von
    
    100 001
    
    bis
    
    150 000
    
    850 Euro,
    
    über
    
    150 000
    
    1 060 Euro;
    
7.  für die Vorsitzenden der Kreisausschüsse, soweit sie nicht Landrätinnen oder Landräte sind, von Landkreisen mit einer Einwohnerzahl
    
    bis
    
    150 000
    
    850 Euro,
    
    über
    
    150 000
    
    1 060 Euro;
    
8.  für die Vorsitzenden von Mitverwaltungsausschüssen abhängig von der Einwohnerzahl
    
    bis
    
    5 000
    
    250 Euro,
    
    von
    
    5 001
    
    bis
    
    10 000
    
    340 Euro,
    
    von
    
    10 001
    
    bis
    
    20 000
    
    430 Euro,
    
    über
    
    20 000
    
    710 Euro,
    
    wobei die addierten Einwohnerzahlen von der mitverwaltenden Gemeinde und den mitverwalteten Gemeinden zu Grunde zu legen sind.

Nach näherer Maßgabe von Entschädigungssatzungen können auch Vorsitzende anderer Ausschüsse eine zusätzliche pauschale monatliche Aufwandsentschädigung erhalten.
Diese darf allerdings nicht mehr als 25 Prozent der entsprechenden Aufwandsentschädigung des jeweiligen Vorsitzenden der Vertretungskörperschaft betragen.
Stehen zusätzliche Aufwandsentschädigungen nach den Nummern 1 und 3 oder den Nummern 2 und 4 nebeneinander zu, so kann nur die höhere Aufwandsentschädigung gewährt werden.
Stehen zusätzliche Aufwandsentschädigungen nach den Nummern 1 und 6 oder den Nummern 2 und 7 nebeneinander zu, so ist die Aufwandsentschädigung nach den Nummern 6 und 7 um 50 Prozent zu vermindern.

(2) Stellvertretungen kann für die Dauer der Wahrnehmung besonderer Funktionen nach Absatz 1 bis zu 50 Prozent der Aufwandsentschädigung der Vertretenen gewährt werden.
Die Aufwandsentschädigung der Vertretungen ist entsprechend zu kürzen.
Ist eine Funktion nach Absatz 1 nicht besetzt und wird daher von einer Stellvertreterin oder einem Stellvertreter in vollem Umfang wahrgenommen, so kann diese oder dieser für die Dauer der Wahrnehmung der Aufgaben bis zu 100 Prozent der nach Absatz 1 zugelassenen Beträge erhalten.

#### § 8 Aufwandsentschädigung für ehrenamtliche Bürgermeisterinnen und Bürgermeister

(1) Ehrenamtlichen Bürgermeisterinnen und Bürgermeistern kann eine monatliche pauschale Aufwandsentschädigung gewährt werden, die nachstehende Höchstsätze nicht überschreiten darf:

in Gemeinden mit einer Einwohnerzahl

bis

500

320 Euro,

von

501

bis

750

440 Euro,

von

751

bis

1 000

570 Euro,

von

1 001

bis

1 500

770 Euro,

von

1 501

bis

2 000

980 Euro,

von

2 001

bis

2 500

1 130 Euro,

von

2 501

bis

3 000

1 240 Euro,

von

3 001

bis

3 500

1 340 Euro,

von

3 501

bis

4 000

1 430 Euro,

von

4 001

bis

5 000

1 530 Euro,

von

5 001

bis

6 000

1 620 Euro,

von

6 001

bis

7 000

1 720 Euro,

von

7 001

bis

8 500

1 820 Euro,

von

8 501

bis

10 000

1 930 Euro,

über

10 000

2 120 Euro,

zuzüglich des Betrages nach § 6 Absatz 1.

(2) Hinsichtlich der Stellvertretung der ehrenamtlichen Bürgermeisterinnen und Bürgermeister gilt § 7 Absatz 2 entsprechend.

#### § 9 Sitzungsgeld für Mitglieder kommunaler Vertretungen

(1) Ehrenamtliche Mitglieder der Gemeindevertretungen, der Verbandsgemeindevertretungen, der Amtsausschüsse sowie der Kreistage und ihrer jeweiligen Ausschüsse einschließlich des Mitverwaltungsausschusses können für jede Sitzung ein Sitzungsgeld in Höhe von höchstens 30 Euro erhalten.
Ehrenamtlichen Bürgermeisterinnen und Bürgermeistern von verbandsangehörigen Gemeinden oder deren Stellvertretungen kann für die Teilnahme an Sitzungen der Verbandsgemeindevertretung ein Sitzungsgeld gewährt werden, wenn die Teilnahme im Rahmen ihrer Zuständigkeit erfolgt.

(2) Sitzungsgelder dürfen den Mitgliedern der Fraktionen nur für die Teilnahme an den Sitzungen gewährt werden, die der Vorbereitung einer Sitzung der Vertretung oder eines Ausschusses dienen.

(3) Vorsitzenden von Ausschüssen, die keine zusätzliche Aufwandsentschädigung nach § 7 Absatz 1, ausgenommen nach den Nummern 3 und 4 oder § 8 erhalten, können für jede von ihnen geleitete Ausschusssitzung bis zu zwei zusätzliche Sitzungsgelder gewährt werden.

(4) Einem Mitglied eines Gremiums nach § 7 Absatz 1, ausgenommen nach den Nummern 3 und 4, kann für die Leitung der Sitzung dieses Gremiums ein doppeltes Sitzungsgeld gewährt werden, wenn die Vorsitzende oder der Vorsitzende des Gremiums an der Sitzungsteilnahme gehindert ist und eine Entschädigung nach § 7 Absatz 2 nicht gewährt wird.

#### § 10 Sitzungsgeld für sachkundige Einwohnerinnen und Einwohner

Sachkundige Einwohnerinnen und Einwohner in Gemeinden und Landkreisen können Sitzungsgeld in Höhe von höchstens 30 Euro erhalten.

#### § 11 Ersatz des Verdienstausfalls

(1) Ein Verdienstausfall wird nicht mit der pauschalen Aufwandsentschädigung oder dem Sitzungsgeld abgegolten.
Der Verdienstausfall wird auf Antrag und nur gegen Bescheinigung des Arbeitgebers erstattet; Selbständige und freiberuflich Tätige müssen den Verdienstausfall glaubhaft machen.

(2) Der Ersatz des Verdienstausfalls ist monatlich auf 35 Stunden zu begrenzen.

(3) Der Anspruch auf Ersatz des Verdienstausfalls ist nach Erreichen der Regelaltersgrenze ausgeschlossen, wenn keine auf Erwerb ausgerichtete Tätigkeit wahrgenommen wird.

#### § 12 Ersatz von Aufwendungen für Betreuung

(1) Zur Betreuung von Kindern bis zum vollendeten vierzehnten Lebensjahr oder zur Pflege von Angehörigen kann, sofern eine ausreichende Beaufsichtigung oder Betreuung anderweitig nicht sichergestellt werden kann, für die Dauer der mandatsbedingten notwendigen Abwesenheit eine Entschädigung gegen Nachweis gewährt werden, wenn die Übernahme der Betreuung durch Personensorgeberechtigte während dieser Zeit nicht möglich ist.

(2) Der zu erstattende Höchstbetrag kann in der Entschädigungssatzung bestimmt werden.

#### § 13 Reisekostenvergütung; Erstattung zusätzlicher Fahrtkosten

(1) Für Dienstreisen ist eine Reisekostenvergütung nach den Bestimmungen des Bundesreisekostengesetzes zu gewähren.
Bei der Benutzung öffentlicher Verkehrsmittel sind die für die hauptamtliche Bürgermeisterin oder den hauptamtlichen Bürgermeister, die Landrätin oder den Landrat oder die Amtsdirektorin oder den Amtsdirektor geltenden Regelungen maßgebend.
Eine Reisekostenvergütung kann nur für Dienstreisen gewährt werden, die von dem nach der Entschädigungssatzung zuständigen Organ angeordnet oder genehmigt wurde.

(2) Fahrten zu Sitzungen von Gremien der Gebietskörperschaft sind keine Dienstreisen im Sinne des Absatzes 1.
Eine Erstattung der Kosten für diese Fahrten ist zusätzlich zur pauschalen monatlichen Aufwandsentschädigung möglich, wenn die Grenzen des Wohnortes überschritten werden.
Bei der Berechnung der Fahrtkosten für die Nutzung privater Kraftfahrzeuge ist § 5 Absatz 2 des Bundesreisekostengesetzes in der jeweils geltenden Fassung entsprechend anzuwenden.
In allen anderen Fällen ist der jeweilige Normalpreis für den öffentlichen Personennahverkehr, für einen Fahrschein zweiter Klasse beziehungsweise eine Fahrt mit dem Taxi zugrunde zu legen.
Es ist jeweils das zumutbare wirtschaftlichste Beförderungsmittel zu wählen.
Ersatzweise können auch Fahrscheine für den öffentlichen Personennahverkehr zur Verfügung gestellt werden.
Unter Berücksichtigung der flächenmäßigen Ausdehnung der jeweiligen Körperschaft kann durch Entschädigungssatzung geregelt werden, dass als Wohnort auch näher zu bestimmende Ortsteile der Körperschaft gelten.
Jede Fahrtkostenerstattung setzt voraus, dass mit dem Eintrag in die Anwesenheitsliste auch die Anzahl der gefahrenen Kilometer vom Wohnort zum Ort der Sitzung angegeben wird.

(3) Die Voraussetzungen für die Erstattung des behinderungsbedingten Mehraufwandes bezüglich der in den Absätzen 1 und 2 genannten Kosten können in geeigneter Weise in einer Entschädigungssatzung geregelt werden.

#### § 14 Entschädigungen für Aufwendungen zur Anschaffung von Informationstechnik und für weitere besondere Aufwendungen

(1) Erhalten Mitglieder kommunaler Vertretungskörperschaften nicht bereits Informationstechnik als Sachausstattung, kann ihnen nach näherer Maßgabe einer Entschädigungssatzung einmalig pro Wahlperiode eine Aufwandsentschädigung für die Anschaffung eines Tablets, Notebooks oder vergleichbarer Geräte gewährt werden.

(2) In der Entschädigungssatzung können Regelungen getroffen werden zur Erstattung weiterer besonderer Aufwendungen, insbesondere für Kommunikationshilfen zum Ausgleich behinderungsbedingter Einschränkungen, die bei der Wahrnehmung der ehrenamtlichen Aufgabe erforderlich sind.

#### § 15 Anpassung von Entschädigungssatzungen

(1) Soweit Satzungsregelungen dieser Verordnung widersprechen, treten sie am 1.
Juli 2020 außer Kraft.

(2) Entschädigungssatzungen können vorsehen, dass erstmalige und höhere Aufwandsentschädigungen rückwirkend ab dem Ersten des auf das Inkrafttreten dieser Verordnung folgenden Monats gewährt werden.

#### § 16 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 31.
Mai 2019

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter