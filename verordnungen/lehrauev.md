## Verordnung zur Übertragung von Lehraufgaben an die Hochschule der Polizei des Landes Brandenburg (Lehraufgabenübertragungsverordnung - LehrAÜV)

Auf Grund des § 3 Absatz 6 des Brandenburgischen Polizeihochschulgesetzes vom 19. Juni 2019 (GVBl. I Nr. 35) verordnet der Minister des Innern und für Kommunales:

#### § 1 Geltungsbereich, Aufgabenübertragung

(1) Aufgrund des Bedarfs zur Ausbildung von Bediensteten mit Spezialkenntnissen in der Kriminalpolizei wird der Hochschule der Polizei die Aufgabe übertragen, einen Masterstudiengang Kriminalistik auszurichten.
Qualifikationsziel dieses Studienganges ist es, Ermittlerinnen und Ermittler im Polizeidienst zu befähigen, in komplexen Verfahren bestimmter Phänomenbereiche Ermittlungsmaßnahmen zu planen und durchzuführen.

(2) Das Verfahren über die Zulassung zum Masterstudium Kriminalistik regelt die oberste Dienstbehörde.
Die Erfüllung der formalen Zulassungsvoraussetzungen prüft die Hochschule der Polizei.

(3) Die Hochschule der Polizei regelt nähere Vorschriften über die inhaltliche Ausgestaltung, die Anforderungen und das Verfahren von Prüfungen in einer Studien- und Prüfungsordnung sowie über die Anerkennung von Leistungen, die Anrechnung von Kenntnissen und Fähigkeiten in einer Anerkennungs- und Anrechnungsordnung.

#### § 2 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 10.
Juni 2020

Der Minister des Innern und für Kommunales

Michael Stübgen