## Verordnung zur Verteilung und Verwendung der Mittel für die Theater- und Orchesterförderung gemäß § 5 des Brandenburgischen Finanzausgleichsgesetzes (Brandenburgische FAG-Förderungsverordnung - BbgFAGFV)

Auf Grund des § 5 Absatz 1 des Brandenburgischen Finanzausgleichgesetzes vom 29. Juni 2004 (GVBl. I S. 262), der zuletzt durch Gesetz vom 18.
Dezember 2018 (GVBl.
I Nr. 34) geändert worden ist, verordnet die Ministerin für Wissenschaft, Forschung und Kultur im Einvernehmen mit dem Minister der Finanzen:

#### § 1 Zuweisungen zur Erhaltung und Sicherung von Theatern, Orchestern und vergleichbaren Einrichtungen mit Theater- und Konzertangeboten

(1) Nachfolgend genannten Kommunen erteilt die für Kultur zuständige oberste Landesbehörde auf Anforderung Zuweisungen in Höhe von insgesamt 21 275 000 Euro zur Erhaltung und Sicherung der Spielbetriebe von Theatern, Orchestern und vergleichbaren Einrichtungen mit Theater- und Konzertangeboten.
Die Zuweisungen verteilen sich dabei wie folgt:

**Kommune/Einrichtungen**

**Zuweisungssumme in Euro**

Stadt Brandenburg an der Havel

für das Brandenburger Theater

2 228 300

2 228 300

Stadt Frankfurt (Oder)

davon für das Brandenburgische Staatsorchester Frankfurt (Oder)

davon für das Kleist Forum Frankfurt

3 314 300

2 477 600

836 700

Stadt Cottbus/Chóśebuz

davon für die Brandenburgische Kulturstiftung Cottbus-Frankfurt (Oder)

davon für das Piccolo Theater 

7 539 000

7 274 700

264 300

Landeshauptstadt Potsdam

davon für das Hans Otto Theater

davon für die Kammerakademie Potsdam

davon für die Musikfestspiele Sanssouci und Nikolaisaal

davon für die Chor- und Orchestersinfonik in Potsdam

3 920 000

3 000 000

400 000

450 000

70 000

Stadt Schwedt/Oder

für die Uckermärkischen Bühnen Schwedt

1 853 500

1 853 500

Landkreis Oberspreewald-Lausitz

für die Neue Bühne Senftenberg

1 614 200

1 614 200

Landkreis Ostprignitz-Ruppin

für die Musikkultur Rheinsberg

355 700

355 700

Landkreis Barnim

für das Brandenburgische Konzertorchester Eberswalde

220 000

220 000

Landkreis Uckermark

davon für das Preußische Kammerorchester Prenzlau

davon für die Kammerphilharmonie Uckermark (Ensemble Quillo)

230 000

170 000

60 000

(2) Die Ausgaben sind übertragbar.
Die Mittelbewirtschaftung obliegt der für Kultur zuständigen obersten Landesbehörde.
Die Zuweisungen gemäß Absatz 1 werden nach Prüfung der Wirtschafts- und Finanzpläne der einzelnen Einrichtungen in Form eines Verwaltungsaktes ausgereicht.

#### § 2 Zuweisungen zur Erhaltung und zur Sicherung eines lokalen und regionalen Theater- und Musiklebens in den Landkreisen an Orten ohne Spielstätten mit festem Theater- oder Musikensemble

(1) Für die Aufrechterhaltung und Entwicklung eines lokalen und regionalen Theater- und Musiklebens an Orten, die über keine Spielstätten mit festen Theater- oder Musikensembles verfügen, weist die für Kultur zuständige oberste Landesbehörde für Theater- und Musikveranstaltungen, bei denen vornehmlich brandenburgische Ensembles sowie Künstlerinnen und Künstler auftreten, den Landkreisen Mittel in Höhe von insgesamt bis zu 725 000 Euro zu.
Abweichend von Satz 1 können die Mittel im Zuweisungsjahr 2020 auch für die Sicherung des laufenden Geschäftsbetriebes von Veranstaltungsstätten zugewiesen werden, sofern durch diese gemäß Absatz 4 Satz 1  
benannte Theater- und Musikveranstaltungen wegen behördlicher Anordnungen aufgrund des Infektionsschutzgesetzes nicht durchgeführt werden dürfen.Die Zuweisungssumme je Landkreis wird wie folgt aufgeteilt:

**Landkreis**

**Zuweisungssumme in Euro bis zu**

Dahme-Spreewald

25 000

Havelland

25 000

Oberhavel

25 000

Oberspreewald-Lausitz    

25 000

Teltow-Fläming

25 000

Barnim

50 000

Prignitz

50 000

Elbe-Elster

50 000

Spree-Neiße

50 000

Märkisch-Oderland

75 000

Oder-Spree

75 000

Ostprignitz-Ruppin

75 000

Potsdam-Mittelmark

75 000

Uckermark

100 000

(2) Sofern infolge von Minderbedarfen der dem jeweiligen Landkreis zustehende Zuweisungsbetrag im Haushaltsjahr unterschritten wird, steht dieser Betrag für Mehrbedarfe dieses Landkreises im ersten Folgejahr zur Verfügung.
Werden von diesem Landkreis im ersten Folgejahr keine Mehrbedarfe geltend gemacht, stehen diese Mittel für Mehrbedarfe der weiteren Landkreise im zweiten Folgejahr zusätzlich zur Verfügung und werden bedarfsgerecht zugewiesen.
Werden durch die weiteren Landkreise im zweiten Folgejahr keine Mehrbedarfe geltend gemacht oder die zur Verfügung stehenden zusätzlichen Mittel nicht vollständig ausgeschöpft, so werden diese Mittel gemäß § 2 Absatz 2 des Brandenburgischen Finanzausgleichsgesetzes dem Ausgleichsfonds gemäß § 16 Absatz 1 zugeführt.

(3) Die Landkreise sind berechtigt, Mittel aus der Zuweisungssumme gemäß Absatz 1 als Veranstalter von Theater- und Musikangeboten ganz oder teilweise in Anspruch zu nehmen oder im Rahmen von Bewilligungsverfahren an sonstige Veranstalter von Theater- und Musikangeboten weiterzuleiten.
Darüber hinaus sind die Landkreise berechtigt, Mittel aus der Zuweisungssumme gemäß Absatz 1 an kreisangehörige Gemeinden oder Ämter mit der Ermächtigung zu bewilligen, diese an Veranstalter von Theater- und Musikangeboten weiterzuleiten.
Veranstalter von Theater- und Musikangeboten, die von einem Landkreis, einer Gemeinde oder einem Amt Mittel aus der Zuweisungssumme gemäß Absatz 1 erhalten, müssen sich in Trägerschaft einer juristischen Person des öffentlichen Rechts oder einer juristischen Person des privaten Rechts befinden, die ausschließlich und unmittelbar gemeinnützigen Zwecken im Sinne der Abgabenordnung dient.

(4) Die Landkreise sind verpflichtet, die Zuweisung gemäß Absatz 1 schriftlich bei der für Kultur zuständigen obersten Landesbehörde bis spätestens zum 30.
November des dem Zuweisungsjahr vorausgehenden Kalenderjahres unter Benennung der im Zuweisungsjahr geplanten Theater- und Musikveranstaltungen und deren Gesamtausgaben anzufordern.
Abweichend von Satz 1 sind die Anforderungen der Landkreise für das Zuweisungsjahr 2019 spätestens vier Wochen nach Verkündung dieser Verordnung bei der für Kultur zuständigen obersten Landesbehörde einzureichen.
Soll die Zuweisung gemäß Absatz 1 Satz 2 verwendet werden, teilen die Landkreise gegenüber der für Kultur zuständigen obersten Landesbehörde schriftlich die Veranstaltungsstätten mit einem laufenden Geschäftsbetrieb und den gemäß Satz 1 benannten Veranstaltungen mit, die wegen behördlicher Anordnungen nicht durchgeführt werden können.
Bereits gegenüber den Landkreisen durch einen Verwaltungsakt erteilte Zuweisungen gelten als um den Verwendungszweck gemäß Absatz 1 Satz 2 ergänzt.

(5) Die Ausgaben sind übertragbar.
Die Mittelbewirtschaftung obliegt der für Kultur zuständigen obersten Landesbehörde.
Die Zuweisungen werden nach Prüfung der Anforderung gemäß Absatz 4 in Form eines Verwaltungsaktes ausgereicht.

(6) Die Landkreise sind verpflichtet, zur Kontrolle einer zweckentsprechenden Verwendung der Zuweisungssumme gemäß Absatz 1 eine Verwendungsbestätigung und einen Sachbericht zu den durchgeführten Theater- und Musikveranstaltungen innerhalb eines halben Jahres nach Durchführung der für Kultur zuständigen obersten Landesbehörde vorzulegen.
Im Sachbericht sind die geförderten Maßnahmen hinsichtlich der mit der Förderung verbundenen kommunalen und regionalen kulturentwicklungsbezogenen Zielstellungen darzustellen.
Wird eine Zuweisung oder ein Teil einer Zuweisung gemäß Absatz 1 Satz 2 verwendet, sind in der Verwendungsbestätigung die entstandenen Ausgaben für den laufenden Geschäftsbetrieb der Veranstaltungsstätte in einer zahlenmäßigen Übersicht darzustellen.

#### § 3 Auszahlung der Zuweisungsmittel

Die Auszahlung der Zuweisungsbeträge gemäß § 1 Absatz 1 erfolgt in vier gleichen Teilbeträgen.
Der erste Teilbetrag wird mit Eintritt der Bestandskraft des Verwaltungsaktes gemäß § 1 Absatz 2 Satz 3 ausgezahlt.
Die weiteren Teilbeträge werden jeweils zum ersten Kalendertag des zweiten Monats des zweiten bis vierten Quartals ausgezahlt.
Die Auszahlung der Zuweisungsbeträge gemäß § 2 Absatz 1 erfolgt mit Eintritt der Bestandskraft des Verwaltungsaktes gemäß § 2 Absatz 5 Satz 3.
§ 19 Absatz 3 des Brandenburgischen Finanzausgleichsgesetzes gilt entsprechend.

#### § 4 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2019 in Kraft.
Gleichzeitig tritt die FAG-Förderungsverordnung vom 11.
Mai 2012 (GVBl.
II Nr.
37) außer Kraft.

Potsdam, den 23.
Januar 2019

Die Ministerin für Wissenschaft,  
Forschung und Kultur

Dr.
Martina Münch