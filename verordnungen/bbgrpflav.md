## Verordnung über die Ausbildung und Prüfung von Rechtspflegerinnen und Rechtspflegern des Landes Brandenburg (Rechtspflegerausbildungsverordnung - BbgRPflAV)

Auf Grund des § 26 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S. 26), der durch Artikel 1 des Gesetzes vom 5.
Dezember 2013 (GVBl.
I Nr.
36) geändert worden ist, verordnet die Ministerin der Justiz im Einvernehmen mit dem Minister des Innern und für Kommunales sowie der Ministerin der Finanzen und für Europa:

**Inhaltsübersicht**

### Abschnitt 1  
Ausbildung

[§ 1 Ziel der Ausbildung](#1)

[§ 2 Zulassungsvoraussetzungen, Aufstiegsbeamtinnen und Aufstiegsbeamte](#2)

[§ 3 Gestaltung der Ausbildung, Verarbeitung personenbezogener Daten](#3)

[§ 4 Anrechnung einer juristischen Ausbildung](#4)

[§ 5 Dauer und Gliederung des Studiums](#5)

[§ 6 Anwesenheitspflicht, Urlaub](#6)

[§ 7 Lehrgebiete des Studiums](#7)

[§ 8 Erster Studienabschnitt (1.
und 2.
Semester)](#8)

[§ 9 Zweiter Studienabschnitt (3.
Semester)](#9)

[§ 10 Dritter Studienabschnitt (4.
Semester)](#10)

[§ 11 Vierter Studienabschnitt (5.
Semester)](#11)

[§ 12 Fünfter Studienabschnitt (6.
Semester)](#12)

[§ 13 Verlängerung des Studiums](#13)

[§ 14 Leistungsbewertung in den fachtheoretischen Studienabschnitten](#14)

[§ 15 Leistungsbewertung in den berufspraktischen Studienabschnitten](#15)

[§ 16 Punkte- und Notenskala, Abschnittspunktzahl, Gesamtausbildungspunktzahl](#16)

### Abschnitt 2  
Prüfung

[§ 17 Zweck der Rechtspflegerprüfung, Dauer des Prüfungsverfahrens](#17)

[§ 18 Laufbahnprüfung](#18)

[§ 19 Tätigkeit im Justizdienst](#19)

[§ 20 Beendigung des Beamtenverhältnisses auf Widerruf, Entlassung](#20)

### Abschnitt 3  
Regelungen für Menschen mit Behinderung, Nachteilsausgleich, Teilzeitbeschäftigung

[§ 21 Regelung für Menschen mit Behinderung](#21)

[§ 22 Nachteilsausgleich, Prüfungserleichterungen](#22)

[§ 23 Teilzeitbeschäftigung, Urlaub ohne Besoldung](#23)

### Abschnitt 4  
Übergangs- und Schlussvorschriften

[§ 24 Übergangsvorschrift](#24)

[§ 25 Inkrafttreten, Außerkrafttreten](#25)

### Abschnitt 1  
Ausbildung

#### § 1 Ziel der Ausbildung

Die Ausbildung soll in einem wissenschaftlichen Studiengang mit praktischem Bezug Rechtspflegerinnen und Rechtspfleger heranbilden, die nach ihrer Persönlichkeit und nach ihren allgemeinen fachlichen Kenntnissen befähigt sind, selbstständig in den ihnen gesetzlich zugewiesenen Aufgabengebieten der Rechtspflege und bei der Ausübung von Tätigkeiten des gehobenen Dienstes in der Justizverwaltung sachgerechte und verständlich begründete Entscheidungen zu treffen.
Sie sollen in die Lage versetzt werden, wirtschaftliche, soziale und rechtspolitische Zusammenhänge zu verstehen.
Ergänzend sollen sie dazu befähigt werden, sich eigenständig fortzubilden.

#### § 2 Zulassungsvoraussetzungen, Aufstiegsbeamtinnen und Aufstiegsbeamte

(1) Die Ausbildung dient als Vorbereitungsdienst für die Laufbahn gehobener Justizdienst.
Zu ihr kann zugelassen werden, wer

1.  die allgemeinen Voraussetzungen für die Ernennung zur Beamtin oder zum Beamten erfüllt, insbesondere charakterlich, fachlich und in gesundheitlicher Hinsicht für die Laufbahn geeignet ist,
2.  die allgemeine Hochschulreife, die Fachhochschulreife oder einen als gleichwertig anerkannten Bildungsstand besitzt sowie
3.  hinsichtlich des Lebensalters die Einstellungsvoraussetzungen des § 3 Absatz 3 des Landesbeamtengesetzes vom 3.
    April 2009 (GVBl.
    I S.
    26), das zuletzt durch Artikel 2 des Gesetzes vom 5.
    Juni 2019 (GVBl.
    I Nr.
    19 S.
    3) geändert worden ist, in der jeweils geltenden Fassung erfüllt.

(2) Die Entscheidung über die Einstellung der Bewerberinnen und Bewerber trifft die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts als Einstellungsbehörde.
Diese legt zuvor nach Maßgabe der an der Hochschule für Wirtschaft und Recht Berlin für das Land Brandenburg zur Verfügung stehenden Studienplätze die jeweilige Ausbildungskapazität fest.
Die Einstellungsbehörde nimmt die ausgewählten Bewerberinnen und Bewerber unter Berufung in das Beamtenverhältnis auf Widerruf in den Vorbereitungsdienst auf, ernennt sie zu „Rechtspflegeranwärterinnen“ und „Rechtspflegeranwärtern“ und vereidigt sie.

(3) Die Einstellungsbehörde trifft die für den Vorbereitungsdienst erforderlichen Entscheidungen; § 5 Absatz 2 und 3 bleibt unberührt.

(4) Die Einstellungsbehörde entscheidet unter Beachtung von § 21 der Laufbahnverordnung vom 1. Oktober 2019 (GVBl.
II Nr.
82) in der jeweils geltenden Fassung auch über die Zulassung von Beamtinnen und Beamten des mittleren Justizdienstes zur Ausbildung als Rechtspflegerin oder Rechtspfleger, soweit diese aufgrund ihrer Persönlichkeit und den in einer mindestens dreijährigen Dienstzeit gezeigten Leistungen für den gehobenen Justizdienst als geeignet erscheinen.
Die Vorschriften dieser Verordnung sind für die Aufstiegsbeamtinnen und Aufstiegsbeamten entsprechend anzuwenden; Aufstiegsprüfung ist die Rechtspflegerprüfung.
Die Zeit der Tätigkeit im mittleren Justizdienst gemäß Satz 1 kann bis zu einer Dauer von sechs Monaten auf die berufspraktischen Studienzeiten angerechnet werden.
Über die Anrechnung entscheidet die Einstellungsbehörde im Einvernehmen mit dem Fachbereich Rechtspflege der Hochschule für Wirtschaft und Recht Berlin.
§ 22 Absatz 3 Satz 2 der Laufbahnverordnung ist anzuwenden.

#### § 3 Gestaltung der Ausbildung, Verarbeitung personenbezogener Daten

(1) Mit der Berufung in das Beamtenverhältnis auf Widerruf oder der Zulassung der Beamtinnen und Beamten des mittleren Justizdienstes zur Ausbildung werden die Studierenden dem Fachbereich Rechtspflege der Hochschule für Wirtschaft und Recht Berlin zum Studium zugewiesen.
Das Studium umfasst fachtheoretische und berufspraktische Studienabschnitte und ist Bestandteil des Vorbereitungsdienstes im Sinne von § 2 Absatz 1 des Rechtspflegergesetzes in der Fassung der Bekanntmachung vom 14.
April 2013 (BGBl.
I S.
778; 2014 I S.
46), das zuletzt durch Artikel 3 des Gesetzes vom 19.
März 2020 (BGBl.
I S.
541) geändert worden ist, in der jeweils geltenden Fassung.

(2) Der Fachbereich Rechtspflege der Hochschule für Wirtschaft und Recht Berlin gestaltet die fachtheoretischen Studienzeiten durch eine Studienordnung und Studienpläne.
Die Regelung der berufspraktischen Studienzeiten erfolgt durch die Einstellungsbehörde.

(3) Die mit der Organisation und Durchführung des Studiums sowie der Prüfung befassten Stellen dürfen personenbezogene Daten verarbeiten, soweit dies zur Erfüllung der Aufgaben nach dieser Verordnung erforderlich ist.

(4) Die Studierenden haben ihre Kenntnisse und Fähigkeiten durch Selbststudium zu vervollständigen.
Ihr Selbststudium ist zu fördern.

#### § 4 Anrechnung einer juristischen Ausbildung

Über eine Anrechnung einer juristischen Ausbildung nach § 2 Absatz 4 des Rechtspflegergesetzes auf den Vorbereitungsdienst entscheidet die Einstellungsbehörde im Einvernehmen mit dem Fachbereich Rechtspflege der Hochschule für Wirtschaft und Recht Berlin.

#### § 5 Dauer und Gliederung des Studiums

(1) Das Studium umfasst 36 Monate und gliedert sich in sechs Semester zu je sechs Monaten.
Das erste, zweite, vierte und sechste Semester sind fachtheoretische und das dritte und fünfte Semester sind berufspraktische Studienabschnitte.

(2) Die fachtheoretischen Studienabschnitte leitet der Fachbereich Rechtspflege der Hochschule für Wirtschaft und Recht Berlin.
Die berufspraktischen Studienabschnitte leitet die Einstellungsbehörde; diese weist die Studierenden einem Gericht oder einer Staatsanwaltschaft zu.
Über den Zeitpunkt und den Ort des Einsatzes der Studierenden bei den Staatsanwaltschaften stellt die Einstellungsbehörde zuvor das Einvernehmen mit der Generalstaatsanwältin oder dem Generalstaatsanwalt des Landes Brandenburg her.
Mit der Ausbildung der Studierenden sind fachlich und persönlich geeignete Praxisausbilderinnen und -ausbilder, insbesondere Rechtspflegerinnen und Rechtspfleger, zu betrauen; sie sind für die Dauer der Ausbildung von ihren übrigen Dienstgeschäften angemessen zu entlasten.

(3) Der Fachbereich Rechtspflege der Hochschule für Wirtschaft und Recht Berlin kann im Einvernehmen mit der Einstellungsbehörde und der für Justiz zuständigen obersten Landesbehörde gestatten, dass Teile der Ausbildung im Ausland absolviert werden, soweit dies der jeweilige Ausbildungsstand zulässt und dienstliche Belange nicht entgegenstehen.
Der Auslandsaufenthalt darf sechs Wochen nicht überschreiten und insgesamt nicht zu einer Verlängerung der Gesamtausbildung führen.

#### § 6 Anwesenheitspflicht, Urlaub

(1) Für die Teilnahme an den Vorlesungen am Fachbereich Rechtspflege der Hochschule für Wirtschaft und Recht Berlin, den berufspraktischen Studienabschnitten und den sonstigen Lehrveranstaltungen besteht eine Anwesenheitspflicht.

(2) Während der fachtheoretischen Studienabschnitte werden die von dem Fachbereich Rechtspflege der Hochschule für Wirtschaft und Recht Berlin bestimmten vorlesungsfreien Zeiten auf den Erholungsurlaub angerechnet.
Darüber hinaus darf Erholungs- oder Sonderurlaub in der Regel nicht an Tagen mit Lehrveranstaltungen oder Leistungsnachweisen gewährt werden.

(3) Zuständig für die Gewährung von Erholungs- und Sonderurlaub ist die Einstellungsbehörde.

#### § 7 Lehrgebiete des Studiums

(1) Das Studium erstreckt sich unter Berücksichtigung des Europarechts und des Internationalen Privatrechts sowie des Internationalen Prozessrechts auf folgende Lehrgebiete:

1.  Gerichtsverfassungs- und Rechtspflegerrecht,
2.  Einführung in das Zivilrecht,
3.  Grundzüge des Verfassungs- und Verwaltungsrechts,
4.  Familienrecht einschließlich des Verfahrensrechts,
5.  Grundstücksrecht einschließlich Grundbuchverfahren,
6.  Erbrecht einschließlich des Verfahrensrechts,
7.  Handels- und Gesellschaftsrecht einschließlich Registerverfahren,
8.  Mobiliarvollstreckungsrecht,
9.  Immobiliarvollstreckungsrecht,
10.  Insolvenzrecht,
11.  Zivilprozessrecht, Kostenrecht in Zivilverfahren sowie in Familienverfahren,
12.  Organisations- und Verwaltungskunde,
13.  Grundzüge des Strafrechts, des Strafprozessrechts, des Strafvollstreckungsrechts und
14.  Internationales Privatrecht.

(2) Die Lehrgebiete sollen inhaltlich den wesentlichen Tätigkeitsgebieten der Rechtspflegerinnen und Rechtspfleger entsprechen.
Die Bildung von Schwerpunkten ist grundsätzlich dem Streben nach Vollständigkeit vorzuziehen.
Die gesellschaftlichen, wirtschaftlichen und ordnungspolitischen Funktionen der Rechtsvorschriften sind zu erläutern.

(3) Nach Maßgabe der Studienordnung und der Studienpläne sind schriftliche Leistungsnachweise anzufertigen.
Die Aufgaben sollen vorwiegend in Form von Aktenauszügen gestellt werden.

#### § 8 Erster Studienabschnitt (1<span class="internal-dot">.</span>und 2<span class="internal-dot">.</span>Semester)

Der erste Studienabschnitt umfasst das erste und zweite Semester und erstreckt sich auf die in § 7 Absatz 1 Nummer 1 bis 8 und Nummer 11 bis 14 genannten Lehrgebiete.

#### § 9 Zweiter Studienabschnitt (3<span class="internal-dot">.</span>Semester)

(1) Der zweite Studienabschnitt findet im dritten Semester in Form von Praxisstationen statt.
Die Studierenden werden am Arbeitsplatz der Rechtspflegerinnen und Rechtspfleger beim Familiengericht, Grundbuchamt und beim Nachlassgericht sowie in der Abteilung für Zivilsachen ausgebildet.
Die Studierenden sollen ihre Kenntnisse in der Praxis anwenden und erweitern.
Ihnen ist ein Überblick über die in diesen Tätigkeitsbereichen eingeführten Informationstechniken zu geben.

(2) Den Studierenden dürfen nur solche Aufgaben übertragen werden, die dem Ausbildungszweck dienen.

(3) Die Studierenden können zu Gruppen zusammengefasst werden; einer Gruppe sollen nicht mehr als sechs Studierende angehören.

#### § 10 Dritter Studienabschnitt (4<span class="internal-dot">.</span>Semester)

Der dritte Studienabschnitt im vierten Semester erstreckt sich auf die in § 7 Absatz 1 Nummer 4, 8 bis 10 und 13 bis 14 genannten Lehrgebiete.

#### § 11 Vierter Studienabschnitt (5<span class="internal-dot">.</span>Semester)

Der vierte Studienabschnitt findet im fünften Semester in Form von Praxisstationen statt.
Die Studierenden werden am Arbeitsplatz der Rechtspflegerinnen und Rechtspfleger bei den Strafvollstreckungsbehörden, beim Betreuungsgericht, Zwangsvollstreckungsgericht und beim Zwangsversteigerungsgericht ausgebildet.
Zudem findet eine Praxisstation entweder am Handelsregistergericht oder am Insolvenzgericht nach Maßgabe der Einstellungsbehörde statt.
Den Studierenden ist ein Überblick über die in diesen Tätigkeitsbereichen eingeführten Informationstechniken zu geben.

#### § 12 Fünfter Studienabschnitt (6<span class="internal-dot">.</span>Semester)

Der fünfte Studienabschnitt im sechsten Semester erstreckt sich auf die in § 7 Absatz 1 Nummer 1, 4 bis 11 und 13bis 14 genannten Lehrgebiete.

#### § 13 Verlängerung des Studiums

(1) Für Studierende, die sich aus Krankheits- oder anderen wichtigen Gründen dem Studium nicht in dem notwendigen Maße widmen konnten und deren Studium deshalb nicht hinreichend fortschreitet, kann die Verlängerung oder Wiederholung einzelner Studienabschnitte angeordnet werden.
Liegt ein anderer wichtiger Grund im Sinne von Satz 1 vor, kommt eine Verlängerung oder Wiederholung nur in Betracht, wenn zu erwarten ist, dass dadurch das Studienziel erreicht wird.

(2) Die Entscheidungen nach Absatz 1 trifft die Einstellungsbehörde im Einvernehmen mit dem Fachbereich Rechtspflege der Hochschule für Wirtschaft und Recht Berlin.

#### § 14 Leistungsbewertung in den fachtheoretischen Studienabschnitten

(1) Im ersten, dritten und fünften Studienabschnitt erbringen die Studierenden schriftliche Leistungsnachweise.
Die Leistungen sind nach der in § 16 Absatz 1 geregelten Punkte- und Notenskala zu bewerten.

(2) Mängel in den fachtheoretischen Leistungen sind seitens des Fachbereichs Rechtspflege der Hochschule für Wirtschaft und Recht Berlin mit den Studierenden zeitnah zu besprechen, um ihnen Gelegenheit zu geben, diese zu beseitigen.

#### § 15 Leistungsbewertung in den berufspraktischen Studienabschnitten

(1) Im zweiten und vierten Studienabschnitt beurteilen die Praxisausbilderinnen und Praxisausbilder die Studierenden für jede Praxisstation in einem gesonderten Zeugnis.
Die Leistungen sind nach der in § 16 Absatz 1 geregelten Punkte- und Notenskala zu bewerten.
In der Beurteilung ist auf Art und Dauer der Tätigkeiten, Eignung, Leistung und Befähigung einzugehen.
Die Einstellungsbehörde kann hierfür ein Zeugnisformular vorgeben.

(2) Mängel in den berufspraktischen Leistungen sind seitens der für die berufspraktische Ausbildung zuständigen Stellen mit den Studierenden zeitnah zu besprechen, um ihnen Gelegenheit zu geben, diese zu beseitigen.

(3) Das Zeugnis ist zum Abschluss der jeweiligen Praxisstation der oder dem Studierenden zu eröffnen.
Die Studierenden können sich zu ihren Leistungsbewertungen schriftlich äußern.
Diese Äußerungen sind dem betreffenden Zeugnis beizufügen.

#### § 16 Punkte- und Notenskala, Abschnittspunktzahl, Gesamtausbildungspunktzahl

(1) Die Leistungen der Studierenden werden für die fachtheoretischen und die berufspraktischen Studienabschnitte nach folgender Punkte- und Notenskala bewertet:

1.  15, 14 und 13 Punkte für eine Leistung, die den Anforderungen in besonderem Maße entspricht („sehr gut“, Note 1),
2.  12, 11 und 10 Punkte für eine Leistung, die den Anforderungen voll entspricht („gut“, Note 2),
3.  9, 8 und 7 Punkte für eine Leistung, die im Allgemeinen den Anforderungen entspricht („befriedigend“, Note 3),
4.  6, 5 und 4 Punkte für eine Leistung, die zwar Mängel aufweist, aber im Ganzen den Anforderungen noch entspricht („ausreichend“, Note 4),
5.  3, 2 und 1 Punkte für eine Leistung, die den Anforderungen nicht entspricht, jedoch erkennen lässt, dass die notwendigen Grundkenntnisse vorhanden sind und die Mängel in absehbarer Zeit behoben werden könnten („mangelhaft“, Note 5) und
6.  0 Punkte für eine Leistung, die den Anforderungen nicht entspricht und bei der selbst die Grundkenntnisse so lückenhaft sind, dass die Mängel in absehbarer Zeit nicht behoben werden könnten („ungenügend“, Note 6).

(2) Für jeden Studienabschnitt wird eine Abschnittspunktzahl ermittelt.
Diese errechnet sich aus der Summe der erzielten Punkte der im jeweiligen Studienabschnitt bewerteten Leistungen geteilt durch deren Anzahl.
Werden Hausarbeiten angefertigt, gelten diese als dreifache Leistung.
Die Abschnittspunktzahl ist bis zur zweiten Stelle hinter dem Komma ohne Rundung auszuweisen.
Sie ist der oder dem Studierenden schriftlich mitzuteilen.
Die Abschnittspunktzahlen für die fachtheoretischen Studienabschnitte ermittelt der Fachbereich Rechtspflege der Hochschule für Wirtschaft und Recht Berlin, diejenigen für die berufspraktischen Studienabschnitte die Einstellungsbehörde.

(3) Aus den Abschnittspunktzahlen aller Studienabschnitte errechnet die Einstellungsbehörde die Gesamtausbildungspunktzahl.
Zu diesem Zweck ist jede Abschnittspunktzahl mit der Zahl der Ausbildungsmonate des Studienabschnitts zu multiplizieren, auf die sie sich bezieht.
Die sich so ergebenden Zahlen sind zu addieren und bis zur zweiten Stelle hinter dem Komma ohne Rundung durch die Zahl der insgesamt bewerteten Ausbildungsmonate zu teilen.
Der Quotient, welcher bis zur zweiten Stelle hinter dem Komma ohne Rundung auszuweisen ist, ist die Gesamtausbildungspunktzahl.
Die Gesamtausbildungspunktzahl ist der oder dem Studierenden spätestens bis zum Beginn der mündlichen Prüfung schriftlich mitzuteilen.

### Abschnitt 2  
Prüfung

#### § 17 Zweck der Rechtspflegerprüfung, Dauer des Prüfungsverfahrens

(1) Die Rechtspflegerprüfung dient dazu, die Befähigung des Prüflings zur Wahrnehmung der Aufgaben einer Rechtspflegerin oder eines Rechtspflegers festzustellen.
Sie besteht aus einem schriftlichen und einem mündlichen Teil.

(2) Das Prüfungsverfahren schließt sich unmittelbar an das Studium an und soll nach Ablauf von drei Monaten beendet sein.

#### § 18 Laufbahnprüfung

(1) Die inhaltliche und organisatorische Vorbereitung der Laufbahnprüfung sowie deren Abnahme erfolgt im Land Berlin und richtet sich nach der Ausbildungs- und Prüfungsordnung für Rechtspflegerinnen und Rechtspfleger des Landes Berlin vom 11.
Februar 2020 (GVBl.
für Berlin S. 39) in der jeweils geltenden Fassung.
Soweit Landesgesetze abweichende Regelungen enthalten, sind diese vorrangig anzuwenden.

(2) An den Sitzungen und Beratungen des Prüfungsausschusses, soweit diese nicht die Bestimmung der Aufgaben für die Aufsichtsarbeiten zum Gegenstand haben, sowie an den Sitzungen und Beratungen der Prüfungskommissionen kann ein vom Bezirkspersonalrat des Brandenburgischen Oberlandesgerichts zu benennendes Personalratsmitglied mit beratender Stimme teilnehmen, soweit Angelegenheiten von Rechtspflegeranwärterinnen und Rechtspflegeranwärtern der Brandenburger Justiz betroffen sind.
Bei Personaleinzelangelegenheiten bedarf dies grundsätzlich zunächst eines Ersuchens der betroffenen Person gegenüber der zuständigen Personalvertretung.
Soweit Schwerbehinderte von den Sitzungen und Beratungen des Prüfungsausschusses sowie der Prüfungskommissionen betroffen sind, kann entsprechend auch ein Mitglied der zuständigen Schwerbehindertenvertretung teilnehmen.

(3) Die Prüflinge sind an den letzten drei Arbeitstagen vor Beginn der schriftlichen Prüfung und in der letzten Woche vor ihrer mündlichen Prüfung von jedem anderen Dienst befreit.

#### § 19 Tätigkeit im Justizdienst

Die Einstellungsbehörde kann Prüflingen im Zeitraum nach Abschluss der schriftlichen und vor ihrer mündlichen Prüfung im Wege eines Dienstleistungsauftrags eine Tätigkeit auf Dienstposten im gehobenen Justizdienst zuweisen.

#### § 20 Beendigung des Beamtenverhältnisses auf Widerruf, Entlassung

(1) Das Beamtenverhältnis auf Widerruf endet bei Rechtspflegeranwärterinnen und Rechtspflegeranwärtern nach Maßgabe von § 32 Absatz 3 des Landesbeamtengesetzes.

(2) Für die Entlassung aus dem Beamtenverhältnis auf Widerruf durch Verwaltungsakt gilt § 23 des Beamtenstatusgesetzes vom 17.
Juni 2008 (BGBl.
I S.
1010), das zuletzt durch Artikel 10 des Gesetzes vom 20.
November 2019 (BGBl.
I S.
1626, 1632) geändert worden ist.
Die Rechtspflegeranwärterin oder der Rechtspflegeranwärter soll aus dem Vorbereitungsdienst insbesondere entlassen werden, wenn sie oder er sich aus persönlichen oder gesundheitlichen Gründen als ungeeignet für die Ausbildung oder das angestrebte Amt erweist.

(3) Darüber hinaus kann die Rechtspflegeranwärterin oder der Rechtspflegeranwärter entlassen werden, wenn sie oder er in mindestens zwei Studienabschnitten weniger als 4,00 Abschnittspunkte gemäß § 16 Absatz 2 erzielt hat.

### Abschnitt 3  
Regelungen für Menschen mit Behinderung, Nachteilsausgleich, Teilzeitbeschäftigung

#### § 21 Regelung für Menschen mit Behinderung

Bei der Entscheidung über die Einstellung sind die Anforderungen des § 155 Absatz 2 des Neunten Buches Sozialgesetzbuch - Rehabilitation und Teilhabe von Menschen mit Behinderungen - vom 23. Dezember 2016 (BGBl.
I S.
3234), das zuletzt durch Artikel 3 Absatz 6 des Gesetzes vom 9. Oktober 2020 (BGBl.
I S.
2075, 2076) geändert worden ist, in der jeweils geltenden Fassung zu beachten.

#### § 22 Nachteilsausgleich, Prüfungserleichterungen

(1) Auf schriftlichen, begründeten Antrag räumt die Dekanin oder der Dekan des Fachbereichs Rechtspflege der Hochschule für Wirtschaft und Recht Berlin den Studierenden, die infolge einer nachgewiesenen Behinderung oder einer chronischen Krankheit anderen gegenüber benachteiligt sind, angemessene Erleichterungen während der fachtheoretischen Studienabschnitte und bei den Hochschulprüfungen ein.
Die Erleichterungen sollen die mit der Behinderung oder chronischen Krankheit verbundenen Nachteile möglichst ausgleichen, ohne dass hierbei eine Minderung der Leistungsanforderung eintritt.
Die Dekanin oder der Dekan kann die betroffenen Lehrkräfte in die Entscheidungsfindung einbeziehen.

(2) Für die Rechtspflegerprüfung gelten die in Absatz 1 genannten Nachteilsausgleichsregelungen entsprechend.
Der Antrag auf Prüfungserleichterungen ist spätestens zwei Monate vor Beginn des Prüfungsverfahrens an die Vorsitzende oder den Vorsitzenden des Prüfungsausschusses zu richten.
Nach Fristablauf auftretende Beeinträchtigungen sind unverzüglich geltend zu machen.
Dem Antrag auf Nachteilsausgleich ist ein aussagekräftiger fachärztlicher Nachweis über die behinderungs- oder erkrankungsbedingten Einschränkungen mit einer Ausgleichsempfehlung beizufügen.
Die oder der Vorsitzende des Prüfungsausschusses kann darüber hinaus eine amtsärztliche Stellungnahme veranlassen.

(3) Soweit die berufspraktischen Studienabschnitte betroffen sind, sind entsprechende Anträge an die Präsidentin oder den Präsidenten des Brandenburgischen Oberlandesgerichts zu richten.

(4) Die Absätze 1 und 2 gelten entsprechend für krankheitsbedingte und sonstige vorübergehende Beeinträchtigungen.
Nachteilsausgleiche können auch bei persönlichen, akuten, zeitlich begrenzten Beeinträchtigungen beantragt werden.

#### § 23 Teilzeitbeschäftigung, Urlaub ohne Besoldung

(1) Für eine Teilzeitbeschäftigung oder eine Beurlaubung unter Wegfall der Besoldung aus familiären Gründen oder als Familienpflegezeit gelten § 80 Absatz 5 und § 80a Absatz 6 des Landesbeamtengesetzes.

(2) Über die Bewilligung von Teilzeitbeschäftigung oder eine Beurlaubung unter Wegfall der Besoldung aus familiären Gründen oder als Familienpflegezeit nach Maßgabe von Absatz 1 entscheidet die Einstellungsbehörde; diese soll hierzu das Einvernehmen mit der Hochschule für Wirtschaft und Recht Berlin herstellen.

### Abschnitt 4  
Übergangs- und Schlussvorschriften

#### § 24 Übergangsvorschrift

(1) Für die Ausbildung und Prüfung der Studierenden, die ihre Ausbildung vor dem 1. Oktober 2020 begonnen haben, ist die Brandenburgische Rechtspflegerausbildungsordnung vom 3. Februar 1994 (GVBl. II S. 74), die zuletzt durch die Verordnung vom 7.
August 2006 (GVBl.
II S.
306) geändert worden ist, weiter anzuwenden.

(2) Studierende gemäß Absatz 1, die nach ihrem tatsächlichen Studienverlauf im Jahr 2023 oder später zur Rechtspflegerprüfung antreten, sind unter Berücksichtigung des Einzelfalles und ihrem regelmäßig durch bereits erbrachte Leistungsnachweise nachgewiesenen Kenntnisstand entsprechend in die Ausbildung nach dieser Verordnung zu integrieren.
Die Entscheidung hierüber trifft die Einstellungsbehörde.

#### § 25 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Oktober 2020 in Kraft.
Gleichzeitig tritt die Brandenburgische Rechtspflegerausbildungsordnung vom 3.
Februar 1994 (GVBl.
II S.
74), die zuletzt durch die Verordnung vom 7.
August 2006 (GVBl.
II S.
306) geändert worden ist, außer Kraft.

Potsdam, den 8.
Dezember 2020

Die Ministerin der Justiz

Susanne Hoffmann