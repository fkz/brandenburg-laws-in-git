## Verordnung zur Übertragung von Zuständigkeiten des Ministeriums des Innern und für Kommunales für die Berechnung und Zahlung von Reise- und Umzugskosten sowie die Bewilligung, Berechnung und Zahlung von Trennungsgeld auf die Zentrale Bezügestelle des Landes Brandenburg (Reisekosten-Umzugskosten-Trennungsgeldzuständigkeitsübertragungsverordnung MIK - RkUkTrZÜVMIK)

Auf Grund des § 63 Absatz 3 Satz 3 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl. I S. 26), der durch Artikel 4 des Gesetzes vom 20.
November 2013 (GVBl.
I Nr. 32 S.
123) geändert worden ist, in Verbindung mit § 9 Absatz 3 der Trennungsgeldverordnung in der Fassung der Bekanntmachung vom 29.
Juni 1999 (BGBl.
I S.
1533), verordnet der Minister des Innern und für Kommunales im Einvernehmen mit der Ministerin der Finanzen und für Europa:

#### § 1 Übertragung von Aufgaben

(1) Die Zuständigkeit des Ministeriums des Innern und für Kommunales für die Berechnung und Zahlung von Reise- und Umzugskosten im Sinne des § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes wird für den gesamten Geschäftsbereich auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

(2) Die Zuständigkeit des Ministeriums des Innern und für Kommunales für die Bewilligung, Berechnung und Zahlung von Trennungsgeld nach § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes wird für den gesamten Geschäftsbereich auf die Zentrale Bezügestelle des Landes Brandenburg übertragen.

#### § 2 Vertretung bei Klagen

Im Rahmen der Übertragung der Zuständigkeit nach § 1 wird die Zentrale Bezügestelle des Landes Brandenburg ermächtigt, das Ministerium des Innern und für Kommunales mit seinem gesamten Geschäftsbereich in verwaltungs- und arbeitsgerichtlichen Streitigkeiten zu vertreten.
Dies gilt auch für Anträge in einstweiligen Rechtsschutzverfahren.

#### § 3 Übergangsvorschrift

Für Anträge auf Berechnung und Zahlung von Reise- und Umzugskosten sowie auf Bewilligung, Berechnung und Zahlung von Trennungsgeld, die vor dem Inkrafttreten dieser Verordnung eingegangen sind und über die noch nicht abschließend entschieden worden ist, verbleibt es bei der Zuständigkeit der bisher zuständigen Stellen.
Dies gilt auch für die Vertretung in zu diesem Zeitpunkt bereits anhängigen Rechtsstreitigkeiten.

#### § 4 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Reisekosten-Trennungsgeldzuständigkeitsübertragungsverordnung MI vom 16.
März 2012 (GVBl.
II Nr.
19), die zuletzt durch die Verordnung vom 21.
Mai 2013 (GVBl.
II Nr.
40) geändert wurde, außer Kraft.

Potsdam, den 14.
Januar 2022

Der Minister des Innern und für Kommunales

Michael Stübgen