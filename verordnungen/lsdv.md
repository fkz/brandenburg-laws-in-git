## Verordnung über die Qualifikationsanforderungen an Disponentinnen und Disponenten der integrierten Regionalleitstellen  (Leitstellendisponentenverordnung - LSDV)

**Inhaltsübersicht**

### Abschnitt 1  
Geltungsbereich, Allgemeine Regelungen

[§ 1 Geltungsbereich](#1)

[§ 2 Grundsatz](#2)

[§ 3 Anpassungslehrgänge](#3)

### Abschnitt 2  
Anpassungslehrgang zum Erwerb der rettungsdienstlichen Qualifikation

[§ 4 Ausbildungsziel und Ablauf](#4)

[§ 5 Prüfung](#5)

[§ 6 Prüfungsausschuss](#6)

[§ 7 Zulassung zur Prüfung](#7)

[§ 8 Erkrankung, Rücktritt, Versäumnis](#8)

[§ 9 Täuschungsversuch und ordnungswidriges Verhalten](#9)

[§ 10 Schriftlicher Teil der Prüfung](#10)

[§ 11 Mündlicher Teil der Prüfung](#11)

[§ 12 Praktischer Teil der Prüfung](#12)

[§ 13 Niederschrift](#13)

[§ 14 Bewertung der einzelnen Prüfungsleistungen](#14)

[§ 15 Bestehen und Nichtbestehen der Prüfung](#15)

[§ 16 Wiederholen der Prüfung](#16)

[§ 17 Prüfungsunterlagen](#17)

### Abschnitt 3  
Anpassungslehrgänge für den Erwerb der feuerwehrtechnischen Qualifikation

[§ 18 Ausbildungsziel und Ablauf](#18)

[§ 19 Prüfung](#19)

[§ 20 Prüfungsausschuss](#20)

[§ 21 Zulassung zur Prüfung](#21)

[§ 22 Schriftlicher Teil der Prüfung](#22)

[§ 23 Mündlicher Teil der Prüfung](#23)

[§ 24 Praktischer Teil der Prüfung](#24)

[§ 25 Durchführung der Prüfung](#25)

### Abschnitt 4  
Disponentenlehrgang

[§ 26 Ausbildungsziel und Ablauf](#26)

[§ 27 Prüfung](#27)

[§ 28 Prüfungsausschuss](#28)

[§ 29 Zulassung zur Prüfung](#29)

[§ 30 Schriftlicher Teil der Prüfung](#30)

[§ 31 Praktischer Teil der Prüfung](#31)

[§ 32 Durchführung der Prüfung](#32)

### Abschnitt 5  
Fortbildung, Ausnahmen

[§ 33 Fortbildung](#33)

[§ 34 Ausnahmen](#34)

[Anlage 1 Rahmenlehrplan für den Erwerb der rettungsdienstlichen Qualifikation  
Anlage 2 Rahmenlehrplan für den Erwerb der feuerwehrtechnischen Grundqualifikation  
Anlage 3 Rahmenlehrplan für den Erwerb der feuerwehrtechnischen Führungsqualifikation  
Anlage 4 Disponentenlehrgang  
Anlage 5 Bescheinigung über den erfolgreichen Abschluss der praktischen Ausbildung zum Leitstellendisponenten gemäß Leitstellendisponentenverordnung  
Anlage 6 Zeugnis über den Anpassungslehrgang zum Erwerb der rettungsdienstlichen Qualifikation für Disponentinnen und Disponenten integrierter Regionalleitstellen  
Anlage 7 Zeugnis über den Anpassungslehrgang zum Erwerb der feuerwehrtechnischen Grundqualifikation  
Anlage 8 Zeugnis über den Anpassungslehrgang zum Erwerb der feuerwehrtechnischen Führungsqualifikation für Disponentinnen und Disponenten integrierter Regionalleitstellen  
Anlage 9 Zeugnis über den Disponentenlehrgang für Disponentinnen und Disponenten integrierter Regionalleitstellen](#Anlage)

### Abschnitt 1  
Geltungsbereich, Allgemeine Regelungen

#### § 1 Geltungsbereich

Diese Verordnung regelt die Qualifikationsanforderungen an Disponentinnen und Disponenten in den integrierten Regionalleitstellen.

#### § 2 Grundsatz

(1) Die Tätigkeit als Disponentin oder Disponent in einer integrierten Regionalleitstelle erfordert die fachspezifische Qualifikation als Notfallsanitäterin, Notfallsanitäter, Rettungsassistentin oder Rettungsassistent.
Zusätzlich müssen die Disponentinnen und Disponenten die erfolgreiche Teilnahme an einem Disponentenlehrgang nach Anlage 4 oder einem vergleichbaren Lehrgang an einer vergleichbaren Einrichtung nachweisen können.

(2) Umfasst die Tätigkeit der Disponentin oder des Disponenten auch die Bearbeitung von Einsätzen der öffentlichen Feuerwehren, sind neben der rettungsdienstlichen Qualifikation auch die Befähigung für den mittleren feuerwehrtechnischen Dienst und der erfolgreiche Abschluss einer Ausbildung zur Gruppenführerin oder zum Gruppenführer (Führungslehrgang mittlerer feuerwehrtechnischer Dienst) erforderlich.

#### § 3 Anpassungslehrgänge

(1) Rettungssanitäterinnen oder Rettungssanitäter, die die fachspezifische Qualifikation als Notfallsanitäterin, Notfallsanitäter, Rettungsassistentin oder Rettungsassistent nicht besitzen, erwerben die für die Tätigkeit als Leitstellendisponentin oder Leitstellendisponent erforderliche rettungsdienstliche Qualifikation durch erfolgreiche Teilnahme an einem Anpassungslehrgang nach Anlage 1 an einer staatlich anerkannten Schule für Notfallsanitäterinnen und Notfallsanitäter.

(2) Notfallsanitäterinnen, Notfallsanitäter, Rettungsassistentinnen, Rettungsassistenten, Rettungssanitäterinnen oder Rettungssanitäter erwerben die für die Tätigkeit als Leitstellendisponentin oder Leitstellendisponent erforderliche feuerwehrtechnische Grundqualifikation durch erfolgreiche Teilnahme an einem Anpassungslehrgang nach Anlage 2 und einem Anpassungslehrgang nach Anlage 3 an der Landesschule und Technischen Einrichtung für Brand- und Katastrophenschutz oder einem vergleichbaren Lehrgang an einer anderen Einrichtung.

(3) Beamtinnen oder Beamte des feuerwehrtechnischen Dienstes oder vergleichbare Beschäftigte, die die Qualifikation Gruppenführerin oder Gruppenführer (Führungslehrgang mittlerer feuerwehrtechnischer Dienst) nicht besitzen sowie Notfallsanitäterinnen, Notfallsanitäter, Rettungsassistentinnen, Rettungsassistenten, Rettungssanitäterinnen oder Rettungssanitäter, die einen Anpassungslehrgang nach Absatz 2 erfolgreich abgeschlossen haben, erwerben die für die Tätigkeit als Leitstellendisponentin oder Leitstellendisponent erforderliche Führungsqualifikation mittlerer feuerwehrtechnischer Dienst durch erfolgreiche Teilnahme an einem Anpassungslehrgang nach Anlage 3 an der Landesschule und Technischen Einrichtung für Brand- und Katastrophenschutz oder einem vergleichbaren Lehrgang an einer anderen Einrichtung.

### Abschnitt 2  
Anpassungslehrgang zum Erwerb der rettungsdienstlichen Qualifikation

#### § 4 Ausbildungsziel und Ablauf

(1) Der Anpassungslehrgang dient dem Erwerb der erforderlichen rettungsdienstlichen Qualifikation für die Tätigkeit als Leitstellendisponentin oder Leitstellendisponent.

(2) Der Lehrgang gliedert sich in die Abschnitte:

1.  schulische Ausbildung und
2.  praktische Ausbildung.

Der Lehrgang schließt mit einer Prüfung ab.

(3) Die schulische Ausbildung sowie die Prüfung sind an einer staatlich anerkannten Schule für Notfallsanitäterinnen und Notfallsanitäter durchzuführen.

(4) Die praktische Ausbildung findet im Bereich Notaufnahme, Anästhesie/Intensivmedizin, Amtsarzt, RegioMed Bereitschaftspraxis und in einer anerkannten Lehrrettungswache statt.

#### § 5 Prüfung

(1) Die Prüfung dient der Feststellung, ob die Lehrgangsteilnehmerin oder der Lehrgangsteilnehmer über die erforderlichen rettungsdienstlichen Kenntnisse für die Tätigkeit in einer integrierten Regionalleitstelle verfügt.

(2) Die Prüfung wird vor einem Prüfungsausschuss abgelegt.
Sie setzt sich zusammen aus einem schriftlichen, einem mündlichen und einem praktischen Teil.

(3) Die Prüfung ist nicht öffentlich.
Die oder der Vorsitzende des Prüfungsausschusses kann Personen, bei denen ein dienstliches Interesse vorliegt, gestatten, als Beobachter bei der praktischen und der mündlichen Prüfung anwesend zu sein.
Beauftragte der für das Gesundheitswesen und für das Rettungswesen zuständigen obersten Landesbehörden und der für die Aufsicht über die staatlich anerkannten Schulen für Notfallsanitäterinnen und Notfallsanitäter zuständigen Behörde sind berechtigt, an der Prüfung als Beobachter teilzunehmen.

(4) Über die Umstände und Inhalte der Prüfung haben die Anwesenden Stillschweigen zu bewahren.
Bei unberechtigter Weitergabe von Informationen können dienst- beziehungsweise arbeitsrechtliche Konsequenzen angeordnet werden.

#### § 6 Prüfungsausschuss

(1) Der Prüfungsausschuss besteht aus einer oder einem Vorsitzenden und zwei Fachprüferinnen oder Fachprüfern.
Die oder der Vorsitzende ist Leiterin oder Leiter der staatlich anerkannten Schule für Notfallsanitäterinnen und Notfallsanitäter oder eine von ihr oder ihm beauftragten Lehrkraft, die über die Qualifikation als Notärztin oder Notarzt verfügt oder eine von ihr oder ihm beauftragen Lehrkraft, die über die Qualifikation Notfallsanitäterin oder Notfallsanitäter mit einem berufspädagogischen Masterabschluss verfügt.
Eine Fachprüferin oder ein Fachprüfer muss eine Praxisanleiterin oder ein Praxisanleiter für Notfallsanitäterinnen und Notfallsanitäter sein.
Die zweite Fachprüferin oder der zweite Fachprüfer muss in einer integrierten Regionalleitstelle beschäftigt sein, die Ausbildung zur Leitstellendisponentin oder zum Leitstellendisponenten erfolgreich abgeschlossen haben und eine Führungsposition in der integrierten Regionalleitstelle bekleiden.
Es sind stellvertretende Mitglieder zu bestimmen.
Die Ausschussmitglieder und deren Stellvertretungen werden von der Leiterin oder dem Leiter der Schule für mindestens zwei Jahre bestellt.
Die Wiederbestellung ist zulässig.

(2) Die oder der Vorsitzende des Prüfungsausschusses legt den Zeitpunkt der Prüfung fest.
Sie oder er benachrichtigt die Lehrgangsteilnehmerinnen und Lehrgangsteilnehmer und die Mitglieder des Prüfungsausschusses.

(3) Der Prüfungsausschuss beschließt mit der Mehrheit der abgegebenen Stimmen.
Stimmenthaltungen sind nicht zulässig.

#### § 7 Zulassung zur Prüfung

(1) Die oder der Vorsitzende des Prüfungsausschusses entscheidet auf Antrag des Prüflings über die Zulassung zur Prüfung und setzt die Prüfungstermine fest.

(2) Die Zulassung zur Prüfung wird erteilt, wenn folgende Nachweise vorliegen:

1.  ein Identitätsnachweis des Prüflings und
2.  Bescheinigungen über die Teilnahme an der schulischen und praktischen Ausbildung.

(3) Die Zulassung zur Prüfung sowie die Prüfungstermine sollen dem Prüfling spätestens zwei Wochen vor Prüfungsbeginn schriftlich mitgeteilt werden.

#### § 8 Erkrankung, Rücktritt, Versäumnis

(1) Bricht eine Lehrgangsteilnehmerin oder ein Lehrgangsteilnehmer aufgrund einer Krankheit oder eines sonstigen von ihr oder ihm nicht zu vertretenden Umstandes die Prüfung ab oder tritt sie oder er mit Genehmigung der oder des Vorsitzenden des Prüfungsausschusses von der Prüfung zurück, kann die Prüfung zu einem späteren Zeitpunkt fortgesetzt werden, wenn die Lehrgangsteilnehmerin oder der Lehrgangsteilnehmer diesen Grund durch das Vorlegen einer Prüfungsunfähigkeitsbescheinigung oder in anderer Form unverzüglich glaubhaft macht.
Der Termin, zu dem die Prüfung fortgesetzt wird, ist durch die Vorsitzende oder den Vorsitzenden des Prüfungsausschusses festzulegen.

(2) Erscheint eine Lehrgangsteilnehmerin oder ein Lehrgangsteilnehmer ohne Nachweis eines triftigen Grundes an einem Prüfungstag nicht oder tritt sie oder er ohne Genehmigung des oder der Vorsitzenden von der Prüfung zurück, so gilt die Prüfung als nicht bestanden.

#### § 9 Täuschungsversuch und ordnungswidriges Verhalten

(1) Begeht oder versucht eine Lehrgangsteilnehmerin oder ein Lehrgangsteilnehmer während einer Prüfung eine Täuschung oder verstößt sie oder er erheblich gegen die Prüfungsordnung, kann die oder der Vorsitzende des Prüfungsausschusses beziehungsweise die oder der Aufsichtsführende sie oder ihn von der Fortsetzung der Prüfung ausschließen.
Über ihre oder seine Teilnahme an weiteren Prüfungen entscheidet die oder der Vorsitzende des Prüfungsausschusses.
Sie oder er kann je nach Schwere der Verfehlung die Wiederholung einzelner oder mehrerer Prüfungsleistungen anordnen oder Lehrgangsteilnehmende endgültig vom Lehrgang ausschließen.

(2) Wird erst nach Aushändigung des Zeugnisses bekannt, dass eine Lehrgangsteilnehmerin oder ein Lehrgangsteilnehmer bei der Prüfung getäuscht hat, kann der Prüfungsausschuss nachträglich die Prüfung für nicht bestanden erklären.
Dies gilt nicht, wenn seit Beendigung der Prüfung mehr als drei Jahre vergangen sind.

#### § 10 Schriftlicher Teil der Prüfung

(1) Der schriftliche Teil der Prüfung erstreckt sich auf die folgenden Themenbereiche der Anlage 1:

1.  Rechtsgrundlagen,
2.  medizinische Grundlagen,
3.  bei Notfalleinsätzen assistieren und erweiterte notfallmedizinische Maßnahmen durchführen.

(2) Der Prüfling hat zu jedem dieser Themenbereiche in jeweils einer Aufsichtsarbeit schriftlich gestellte Aufgaben zu bearbeiten.
Die Aufsichtsarbeiten dauern jeweils 120 Minuten.
Der schriftliche Teil der Prüfung ist an zwei Tagen durchzuführen.
Die Aufsichtsführenden werden von der Schulleitung bestellt.

(3) Die Aufgaben für die Aufsichtsarbeiten werden von der oder dem Vorsitzenden des Prüfungsausschusses auf Vorschlag der Schule ausgewählt.
Jede Aufsichtsarbeit ist von mindestens zwei Fachprüferinnen oder Fachprüfern zu benoten.
Aus den Noten der Fachprüferinnen oder Fachprüfer bildet die oder der Vorsitzende des Prüfungsausschusses im Benehmen mit den Fachprüferinnen oder Fachprüfern die Note für die einzelnen Aufsichtsarbeiten.
Der schriftliche Teil der Prüfung ist bestanden, wenn jede der drei Aufsichtsarbeiten mindestens mit „ausreichend“ benotet wird.

#### § 11 Mündlicher Teil der Prüfung

(1) In der mündlichen Prüfung hat der Prüfling seine berufliche Handlungskompetenz, die sich in den Dimensionen Fach-, Sozial- und Selbstkompetenz entfaltet, nachzuweisen.

(2) Der mündliche Teil der Prüfung erstreckt sich auf die folgenden Themenbereiche der Anlage 1:

1.  Lebensbedrohliche Zustände erkennen und bewerten sowie einfache lebenserhaltende Maßnahmen durchführen (BLS),
2.  bei Notfalleinsätzen assistieren und erweiterte notfallmedizinische Maßnahmen durchführen,
3.  Patientinnen und Patienten, Angehörige, Kolleginnen und Kollegen sowie Dritte unterstützen und beraten,
4.  einen Notfalleinsatz selbstständig planen, durchführen und bewerten,
5.  Einsätze mit erweiterten Anforderungen selbstständig planen, durchführen und bewerten.

(3) Die Prüflinge werden einzeln oder zu zweit geprüft.
Die Prüfung soll für jeden Prüfling mindestens 30 und nicht länger als 45 Minuten dauern.

(4) Die Prüfung zu jedem Themenbereich wird von mindestens zwei Fachprüferinnen oder Fachprüfern abgenommen und benotet.
Die oder der Vorsitzende des Prüfungsausschusses ist berechtigt, sich an der mündlichen Prüfung zu beteiligen und dabei selbst Prüfungsfragen zu stellen.
Aus den Noten der Fachprüferinnen oder Fachprüfer bildet die oder der Vorsitzende des Prüfungsausschusses im Benehmen mit den Fachprüferinnen oder Fachprüfern die Note für den jeweiligen Themenbereich.
Der mündliche Teil der Prüfung ist bestanden, wenn jeder Themenbereich mindestens mit „ausreichend“ benotet wird.

#### § 12 Praktischer Teil der Prüfung

(1) Im praktischen Teil der Prüfung hat der Prüfling nachzuweisen, dass sie oder er in der Lage ist, die im Anpassungslehrgang erworbenen Kenntnisse und Fertigkeiten anzuwenden.

(2) Der praktische Teil der Prüfung erstreckt sich auf die rettungsdienstlichen Fähigkeiten und Fertigkeiten als Leitstellendisponentin oder Leitstellendisponent.
Der Prüfling übernimmt bei vier vorgegebenen Fallbeispielen die anfallenden Aufgaben einer Leitstellendisponentin oder eines Leitstellendisponenten.

(3) Eines der Fallbeispiele muss aus dem Bereich der internistischen Notfälle, eines aus dem Bereich der traumatologischen Notfälle und eines aus dem Bereich Herzkreislaufstillstand mit Anleitung zur Reanimation (Telefonreanimation) stammen.

(4) Jedes Fallbeispiel wird durch ein Fachgespräch ergänzt.
In diesem hat der Prüfling sein Handeln zu erläutern und zu begründen sowie die Prüfungssituation zu reflektieren.

(5) Die Auswahl der Fallbeispiele erfolgt durch die Vorsitzende oder den Vorsitzenden des Prüfungsausschusses auf Vorschlag der Schule.
Dabei sind der aktuelle Standard und die Besonderheiten und Erfordernisse der Notfallmedizin und einer zeitgemäßen Notfallversorgung angemessen zu berücksichtigen.

(6) Die Prüflinge werden einzeln oder zu zweit geprüft.
Die Prüfung soll einschließlich des Fachgesprächs für jedes Fallbeispiel mindestens 20 und nicht länger als 40 Minuten dauern.
Die Prüfung kann auf zwei aufeinanderfolgende Tage verteilt werden.

(7) Jedes Fallbeispiel wird von mindestens zwei Fachprüferinnen oder Fachprüfern abgenommen und benotet.
Aus den Noten der Fachprüferinnen oder Fachprüfer bildet die oder der Vorsitzende des Prüfungsausschusses im Benehmen mit den Fachprüferinnen oder Fachprüfern die Prüfungsnote für jedes Fallbeispiel.
Der praktische Teil der Prüfung ist bestanden, wenn jedes Fallbeispiel mindestens mit „ausreichend“ benotet wird.

#### § 13 Niederschrift

Über die Prüfung ist eine Niederschrift anzufertigen, aus der das Datum der Prüfung, die Zusammensetzung des Prüfungsausschusses, der Name des jeweiligen Prüflings, die Prüfungsgebiete, die Bewertung der einzelnen Prüfungsleistungen und das Prüfungsergebnis hervorgehen.
Die Niederschrift ist von der oder dem Vorsitzenden und den Fachprüferinnen oder Fachprüfern zu unterzeichnen.

#### § 14 Bewertung der einzelnen Prüfungsleistungen

(1) Die schriftlichen, praktischen und mündlichen Prüfungsleistungen werden wie folgt bewertet:

1.

sehr gut (1)

\=

eine Leistung, die den Anforderungen in besonderem Maße entspricht,

2.

gut (2)

\=

eine Leistung, die den Anforderungen voll entspricht,

3.

befriedigend (3)

\=

eine Leistung, die im Allgemeinen den Anforderungen entspricht,

4.

ausreichend (4)

\=

eine Leistung, die zwar Mängel aufweist, aber im Ganzen den Anforderungen  
noch entspricht,

5.

mangelhaft (5)

\=

eine Leistung, die den Anforderungen nicht entspricht, die jedoch erkennen lässt,  
dass die notwendigen Grundkenntnisse vorhanden sind und die Mängel in  
absehbarer Zeit behoben werden können,

6.

ungenügend (6)

\=

eine Leistung, die den Anforderungen nicht entspricht und bei der selbst die  
Grundkenntnisse so lückenhaft sind, dass die Mängel in absehbarer Zeit nicht  
behoben werden können.

(2) Eine ohne ausreichende Begründung nicht abgelieferte Prüfungsarbeit wird mit ungenügend bewertet.

(3) Der Prüfungsausschuss beschließt die Bewertung der einzelnen Prüfungsleistungen.
Die oder der Vorsitzende teilt den Lehrgangsteilnehmenden das Prüfungsergebnis im Anschluss an die Prüfung mit.

(4) Eine Gesamtbewertung erfolgt nicht.

#### § 15 Bestehen und Nichtbestehen der Prüfung

Die Prüfung ist nicht bestanden, wenn ein Prüfungsteil nach § 5 Absatz 2 mit der Note mangelhaft oder ungenügend bewertet wurde.
Wer die Prüfung bestanden hat, erhält ein Zeugnis nach Anlage 6.

#### § 16 Wiederholen der Prüfung

Hat der Prüfling eine schriftliche Aufsichtsarbeit nach § 10 Absatz 1 oder ein Fallbeispiel nach § 12 Absatz 2 nicht bestanden, so darf er zur Wiederholungsprüfung nur zugelassen werden, wenn er an einer zusätzlichen Ausbildung teilgenommen hat.
Dauer und Inhalt der zusätzlichen Ausbildung bestimmt die oder der Vorsitzende des Prüfungsausschusses.
Die zusätzliche Ausbildung darf einschließlich der für die Prüfung erforderlichen Zeit zwölf Monate nicht überschreiten.
Der Prüfling hat bei seinem Antrag auf Zulassung zur Wiederholungsprüfung einen Nachweis über die zusätzliche Ausbildung beizufügen.

#### § 17 Prüfungsunterlagen

Auf Antrag ist dem Prüfling nach Abschluss der Prüfung Einsicht in seine Prüfungsunterlagen zu gewähren.
Schriftliche Aufsichtsarbeiten sind drei Jahre, Anträge auf Zulassung zur Prüfung und Prüfungsniederschriften zehn Jahre aufzubewahren.
Die Aufbewahrungs- oder Speicherungsfrist beginnt mit dem jeweiligen Prüfungstag der betroffenen Teilnehmerin oder des betroffenen Teilnehmers.

### Abschnitt 3  
Anpassungslehrgänge für den Erwerb der feuerwehrtechnischen Qualifikation

#### § 18 Ausbildungsziel und Ablauf

(1) Die Anpassungslehrgänge dienen dem Erwerb der für die Tätigkeit als Leitstellendisponentin oder Leitstellendisponent erforderlichen feuerwehrtechnischen Grund- oder Führungsqualifikation.
Die Anpassungslehrgänge schließen jeweils mit einer Prüfung ab.

(2) Die Ausbildung erfolgt entsprechend der Ausbildungsrahmenpläne nach Anlage 2 oder Anlage 3.

(3) Die Anpassungslehrgänge für die feuerwehrtechnische Grundqualifikation werden an der Landesschule und Technischen Einrichtung für Brand- und Katastrophenschutz oder an einer Berufsfeuerwehr durchgeführt.

(4) Die Anpassungslehrgänge für die feuerwehrtechnische Führungsqualifikation werden an der Landesschule und Technischen Einrichtung für Brand- und Katastrophenschutz durchgeführt.

#### § 19 Prüfung

(1) Die Prüfung dient der Feststellung, ob die Teilnehmenden über die erforderlichen feuerwehrtechnischen Grund- oder Führungskenntnisse für die Tätigkeit in einer integrierten Regionalleitstelle verfügen.

(2) Die Prüfung wird vor einem Prüfungsausschuss abgelegt.
Sie setzt sich zusammen aus einem schriftlichen, einem mündlichen und einem praktischen Teil.

(3) Die Prüfung ist nicht öffentlich.
Die oder der Vorsitzende des Prüfungsausschusses kann Personen, bei denen ein dienstliches Interesse vorliegt, gestatten, als Beobachter bei der praktischen und der mündlichen Prüfung anwesend zu sein.
Beauftragte der für das Gesundheitswesen und für den Brand- und Katastrophenschutz zuständigen obersten Landesbehörden und des Landesamtes für Arbeitsschutz, Verbraucherschutz und Gesundheit sind berechtigt, an der Prüfung als Beobachter teilzunehmen.

(4) Über die Umstände und Inhalte der Prüfung haben die Anwesenden Stillschweigen zu bewahren.
Bei unberechtigter Weitergabe von Informationen können dienst- oder arbeitsrechtliche Konsequenzen angeordnet werden.

#### § 20 Prüfungsausschuss

(1) Der Prüfungsausschuss besteht aus der Leiterin oder dem Leiter der Landesschule und Technischen Einrichtung für Brand- und Katastrophenschutz oder einer von ihr oder ihm beauftragten Lehrkraft als Vorsitzende oder Vorsitzender und zwei Fachprüferinnen beziehungsweise Fachprüfern.
Eine der Fachprüferinnen beziehungsweise einer der Fachprüfer muss eine Beamtin oder ein Beamter des gehobenen feuerwehrtechnischen Dienstes einer Berufsfeuerwehr oder eine vergleichbare Beschäftigte oder vergleichbarer Beschäftigter sein.
Die zweite Fachprüferin beziehungsweise der zweite Fachprüfer muss in einer integrierten Regionalleitstelle beschäftigt sein, die Ausbildung zur Leitstellendisponentin oder zum Leitstellendisponenten erfolgreich abgeschlossen haben und eine Führungsposition in der integrierten Regionalleitstelle bekleiden.
Es sind stellvertretende Mitglieder zu bestimmen.
Die Ausschussmitglieder und deren Stellvertretungen werden von der Leiterin oder dem Leiter der Landesschule und Technischen Einrichtung für Brand- und Katastrophenschutz für mindestens zwei Jahre bestellt.
Die Wiederbestellung ist zulässig.

(2) Die oder der Vorsitzende des Prüfungsausschusses legt den Zeitpunkt der Prüfung fest und benachrichtigt die Mitglieder des Prüfungsausschusses.

#### § 21 Zulassung zur Prüfung

(1) Die oder der Vorsitzende des Prüfungsausschusses entscheidet auf Antrag des Prüflings über die Zulassung zur Prüfung und setzt die Prüfungstermine fest.

(2) Die Zulassung zur Prüfung wird erteilt, wenn folgende Nachweise vorliegen:

1.  ein Identitätsnachweis des Prüflings
2.  Bescheinigungen über die Teilnahme an der Ausbildung entsprechend der Ausbildungsrahmenpläne nach Anlage 2 und Anlage 3.

(3) Die Zulassung zur Prüfung sowie die Prüfungstermine sollen dem Prüfling spätestens zwei Wochen vor Prüfungsbeginn schriftlich mitgeteilt werden.

#### § 22 Schriftlicher Teil der Prüfung

(1) Der schriftliche Teil der Prüfung zum Erwerb der feuerwehrtechnischen Grundqualifikation erstreckt sich auf die folgenden Themenbereiche der Anlage 2:

1.  Rechtsgrundlagen, fachbezogene Grundlagen, Fahrzeug- und Gerätekunde, vorbeugender Brandschutz und
2.  Einsatzlehre.

(2) Der schriftliche Teil der Prüfung zum Erwerb der feuerwehrtechnischen Führungsqualifikation erstreckt sich auf folgende Themenbereiche der Anlage 3:

1.  Rechtsgrundlagen, Fahrzeug- und Gerätekunde, vorbeugender Brandschutz und
2.  Einsatzlehre, Melde- und Berichtswesen, Pressearbeit.

(3) Der Prüfling hat für den Erwerb der feuerwehrtechnischen Grundqualifikation und der feuerwehrtechnischen Führungsqualifikation in jeweils zwei Aufsichtsarbeiten schriftlich gestellte Aufgaben zu bearbeiten.
Die Aufsichtsarbeiten dauern jeweils 120 Minuten.
Der schriftliche Teil der Prüfung ist an zwei Tagen durchzuführen.
Die Aufsichtsführenden werden von der Schulleitung bestellt.

(4) Die Aufgaben für die Aufsichtsarbeiten werden von der oder dem Vorsitzenden des Prüfungsausschusses auf Vorschlag der Schule ausgewählt.
Jede Aufsichtsarbeit ist von mindestens zwei Fachprüferinnen oder Fachprüfern zu benoten.
Aus den Noten der Fachprüferinnen oder Fachprüfer bildet die oder der Vorsitzende des Prüfungsausschusses im Benehmen mit den Fachprüferinnen oder Fachprüfern die Note für die einzelne Aufsichtsarbeit.
Aus den Noten der zwei Aufsichtsarbeiten bildet die oder der Vorsitzende des Prüfungsausschusses die Prüfungsnote für den schriftlichen Teil der Prüfung.
Der schriftliche Teil der Prüfung ist bestanden, wenn jede der zwei Aufsichtsarbeiten mindestens mit „ausreichend“ benotet wird.

#### § 23 Mündlicher Teil der Prüfung

(1) In der mündlichen Prüfung hat der Prüfling seine berufliche Handlungskompetenz, die sich in den Dimensionen Fach-, Sozial- und Selbstkompetenz entfaltet, nachzuweisen.

(2) Der mündliche Teil der Prüfung erstreckt sich auf alle vermittelten Themenbereiche der Anlage 2 beziehungsweise Anlage 3.

(3) Die Prüflinge werden einzeln oder zu zweit geprüft.
Die Prüfung soll für jeden Prüfling mindestens 30 und nicht länger als 45 Minuten dauern.

(4) Die Prüfung zu jedem Themenbereich wird von mindestens zwei Fachprüferinnen oder Fachprüfern abgenommen und benotet.
Die oder der Vorsitzende des Prüfungsausschusses ist berechtigt, sich an der mündlichen Prüfung zu beteiligen und dabei selbst Prüfungsfragen zu stellen.
Aus den Noten der Fachprüferinnen oder Fachprüfer bildet die oder der Vorsitzende des Prüfungsausschusses im Benehmen mit den Fachprüferinnen oder Fachprüfern die Note für den jeweiligen Themenbereich.
Die oder der Vorsitzende des Prüfungsausschusses bildet die Note für den mündlichen Teil der Prüfung aus dem arithmetischen Mittel der für jeden Themenbereich erteilten Einzelnote.
Der mündliche Teil der Prüfung ist bestanden, wenn jeder Themenbereich mindestens mit „ausreichend“ benotet wird.

#### § 24 Praktischer Teil der Prüfung

(1) Im praktischen Teil der Prüfung hat der Prüfling nachzuweisen, dass sie oder er in der Lage ist, die im Anpassungslehrgang erworbenen Kenntnisse und Fertigkeiten anzuwenden.

(2) Der praktische Teil der Prüfung erstreckt sich auf die feuerwehrtechnischen Fähigkeiten und Fertigkeiten als Leitstellendisponentin oder Leitstellendisponent.

(3) Der praktische Teil der Prüfung besteht aus vier Fallbeispielen.
Eines der Fallbeispiele muss aus dem Bereich technische Hilfeleistung, eines aus dem Bereich der Brände, eines aus dem Bereich Brandmeldeanlagen und eines aus dem Bereich der Amtshilfe stammen.

(4) Jedes Fallbeispiel wird durch ein Fachgespräch ergänzt.
In diesem hat der Prüfling sein Handeln zu erläutern und zu begründen sowie die Prüfungssituation zu reflektieren.

(5) Die Auswahl der Fallbeispiele erfolgt durch die Vorsitzende oder den Vorsitzenden des Prüfungsausschusses auf Vorschlag der Schule.
Dabei sind der aktuelle Standard und die Besonderheiten und Erfordernisse der Notfallmedizin und einer zeitgemäßen Notfallversorgung angemessen zu berücksichtigen.

(6) Die Prüflinge werden einzeln oder zu zweit geprüft.
Die Prüfung soll einschließlich des Fachgesprächs für jedes Fallbeispiel mindestens 20 und nicht länger als 40 Minuten dauern.
Die Prüfung kann auf zwei aufeinanderfolgende Tage verteilt werden.

(7) Jedes Fallbeispiel wird von mindestens zwei Fachprüferinnen oder Fachprüfern abgenommen und benotet.
Aus den Noten der Fachprüferinnen oder Fachprüfer bildet die oder der Vorsitzende des Prüfungsausschusses im Benehmen mit den Fachprüferinnen oder Fachprüfern die Prüfungsnote für jedes Fallbeispiel.
Aus diesen Noten bildet die oder der Vorsitzende des Prüfungsausschusses die Gesamtnote für den praktischen Teil der Prüfung.
Der praktische Teil der Prüfung ist bestanden, wenn jedes Fallbeispiel mindestens mit „ausreichend“ benotet wird.

#### § 25 Durchführung der Prüfung

(1) Für Erkrankung, Rücktritt, Versäumnis, Täuschungsversuch und ordnungswidriges Verhalten, Niederschrift, Bewertung der Prüfungsleistung, Wiederholen der Prüfung sowie Prüfungsunterlagen gelten die §§ 8, 9, 13, 14, 16 und 17 entsprechend.

(2) Die Prüfung ist nicht bestanden, wenn ein Prüfungsteil nach § 19 Absatz 2 mit der Note mangelhaft oder ungenügend bewertet wurde.
Wer die Prüfung bestanden hat, erhält ein Zeugnis nach Anlage 7 beziehungsweise Anlage 8.

### Abschnitt 4  
Disponentenlehrgang

#### § 26 Ausbildungsziel und Ablauf

(1) Der Disponentenlehrgang dient der Vermittlung von Gesprächs- und Sozialkompetenz, dem Kennenlernen und Anwenden der Strukturen und Handlungsabläufe in einer integrierten Regionalleitstelle und der Anwendung der Leitstellensysteme in der Praxis.

(2) Der Disponentenlehrgang gliedert sich in die Abschnitte:

1.  schulische Ausbildung und
2.  praktische Ausbildung.

Der Disponentenlehrgang schließt mit einer Prüfung ab.

(3) Die Ausbildungsinhalte der schulischen und praktischen Ausbildung ergeben sich aus dem Ausbildungsrahmenplan (Anlage 4).

(4) Die schulische Ausbildung erfolgt an der Landesschule und Technischen Einrichtung für Brand- und Katastrophenschutz oder durch einen Aufgabenträger, der eine integrierte Regionalleitstelle unterhält.

(5) Die schulische Ausbildung schließt mit einer schriftlichen Aufsichtsarbeit ab.
Dabei haben die Lehrgangsteilnehmenden unter Aufsicht einen Fragebogen mit fachbezogenen Fragen zu beantworten.
Es können auch Multiple Choice Fragen genutzt werden.
Der Fragebogen für die Aufsichtsarbeit wird von der Vorsitzenden oder dem Vorsitzenden des Prüfungsausschusses (§ 20) bestimmt.
Der Zeitraum für die Beantwortung des Fragebogens beträgt 45 Minuten.
Die Aufsichtsarbeit gilt als erfolgreich abgeschlossen, wenn mindestens 50 % der maximal möglichen Punkte erreicht wird.
Hat eine Lehrgangsteilnehmerin oder ein Lehrgangsteilnehmer die Aufsichtsarbeit nicht bestanden, kann sie oder er diese auf Antrag einmal wiederholen.

(6) Nach erfolgreichem Abschluss der schulischen Ausbildung erfolgt die praktische Ausbildung in einer integrierten Regionalleitstelle.
Während der praktischen Ausbildung nimmt die Lehrgangsteilnehmerin oder der Lehrgangsteilnehmer die Tätigkeiten in der integrierten Regionalleitstelle nur unter Anleitung wahr.
Durch den Träger der integrierten Regionalleitstelle wird hierzu eine Ausbildungsverantwortliche oder ein Ausbildungsverantwortlicher benannt, die oder der die Begleitung und die inhaltliche Dokumentation der erreichten Ausbildungsziele der Lehrgangsteilnehmerin oder des Lehrgangsteilnehmers zu übernehmen hat.
Als Ausbildungsverantwortliche können die Beschäftigten der integrierten Regionalleitstelle eingesetzt werden, die mindestens die abgeschlossene Disponentenausbildung besitzen.
Über die praktische Ausbildung ist eine Bescheinigung nach Anlage 5 zu erstellen.

(7) Angehörige einer Werkfeuerwehr können die praktische Ausbildung auch in einer Leitstelle einer Werkfeuerwehr absolvieren.
Absatz 6 gilt entsprechend.

#### § 27 Prüfung

(1) An der Prüfung kann teilnehmen, wer

1.   
    1.  Notfallsanitäterin, Notfallsanitäter, Rettungsassistentin oder Rettungsassistent ist, oder
    2.  an einem Anpassungslehrgang zum Erwerb der rettungsdienstlichen Qualifikation erfolgreich teilgenommen hat und
2.   
    1.  als Beamtin oder Beamter oder vergleichbare Beschäftigte oder vergleichbarer Beschäftigter für den mittleren feuerwehrtechnischen Dienst befähigt ist, oder
    2.  an erforderlichen Anpassungslehrgängen zum Erwerb der feuerwehrtechnischen Grundqualifikation erfolgreich teilgenommen hat und
3.   
    1.  die Ausbildung zur Gruppenführerin oder zum Gruppenführer (Führungslehrgang mittlerer feuerwehrtechnischer Dienst) erfolgreich abgeschlossen hat oder
    2.  am erforderlichen Anpassungslehrgang zum Erwerb der feuerwehrtechnischen Führungsqualifikation erfolgreich teilgenommen hat.

(2) Die Prüfung dient der Feststellung, ob die Lehrgangsteilnehmerin oder der Lehrgangsteilnehmer über die erforderlichen Kenntnisse und Fähigkeiten für die Tätigkeit in einer integrierten Regionalleitstelle verfügt.

(3) Die Prüfung wird vor einem Prüfungsausschuss abgelegt.
Sie setzt sich zusammen aus einem schriftlichen und einem praktischen Teil.

(4) Die Prüfung ist nicht öffentlich.
Die oder der Vorsitzende des Prüfungsausschusses kann Personen, bei denen ein dienstliches Interesse vorliegt, gestatten, als Beobachter bei der praktischen Prüfung anwesend zu sein.
Beauftragte der für das Gesundheitswesen, Rettungswesen und Brand- und Katastrophenschutz zuständigen obersten Landesbehörden und des Landesamtes für Arbeitsschutz, Verbraucherschutz und Gesundheit sind berechtigt, an der Prüfung als Beobachter teilzunehmen.

(5) Über die Umstände und Inhalte der Prüfung haben die Anwesenden Stillschweigen zu bewahren.
Bei unberechtigter Weitergabe von Informationen können dienst- oder arbeitsrechtliche Konsequenzen angeordnet werden.

#### § 28 Prüfungsausschuss

(1) Der Prüfungsausschuss besteht aus der Leiterin oder dem Leiter der Landesschule und Technischen Einrichtung für Brand- und Katastrophenschutz oder einer von ihr oder ihm beauftragten Lehrkraft als Vorsitzende oder Vorsitzender und zwei Fachprüferinnen oder Fachprüfern.
Es sind stellvertretende Mitglieder zu bestimmen.
Die Fachprüferinnen beziehungsweise Fachprüfer und deren Stellvertretungen müssen in einer integrierten Regionalleitstelle beschäftigt sein und die Ausbildung zur Leitstellendisponentin oder zum Leitstellendisponenten erfolgreich abgeschlossen haben, über eine mindestens fünfjährige Berufserfahrung in einer integrierten Regionalleitstelle verfügen und eine Führungsfunktion in der integrierten Regionalleitstelle bekleiden.
Die Ausschussmitglieder und deren Stellvertretungen werden von der Leiterin oder dem Leiter der Landesschule und Technischen Einrichtung für Brand- und Katastrophenschutz für mindestens zwei Jahre bestellt.
Die Wiederbestellung ist zulässig.

(2) Die oder der Vorsitzende des Prüfungsausschusses legt den Zeitpunkt der Prüfung fest und benachrichtigt die Mitglieder des Prüfungsausschusses.

#### § 29 Zulassung zur Prüfung

(1) Die oder der Vorsitzende des Prüfungsausschusses entscheidet auf Antrag des Prüflings über die Zulassung zur Prüfung und setzt die Prüfungstermine fest.

(2) Zur Prüfung wird zugelassen, wer

1.   
    1.  Notfallsanitäterin, Notfallsanitäter, Rettungsassistentin oder Rettungsassistent ist oder
    2.  an einem Anpassungslehrgang zum Erwerb der rettungsdienstlichen Qualifikation erfolgreich teilgenommen hat, und
2.   
    1.  als Beamtin oder Beamter oder vergleichbare Beschäftigte oder vergleichbarer Beschäftigter zum mittleren feuerwehrtechnischen Dienst befähigt ist, oder
    2.  an erforderlichen Anpassungslehrgängen zum Erwerb der feuerwehrtechnischen Grundqualifikation erfolgreich teilgenommen hat und
3.   
    1.  die Ausbildung zur Gruppenführerin oder zum Gruppenführer (Führungslehrgang mittlerer feuerwehrtechnischer Dienst) erfolgreich abgeschlossen hat oder
    2.  am erforderlichen Anpassungslehrgang zum Erwerb der feuerwehrtechnischen Führungsqualifikation erfolgreich teilgenommen hat und
4.  einen Identitätsnachweis vorlegt.

(3) Die Zulassung zur Prüfung wird erteilt, wenn die nach Absatz 2 geforderten Ausbildungen oder Anpassungslehrgänge nachgewiesen werden und darüber hinaus Bescheinigungen über die erfolgreiche und regelmäßige Teilnahme an der schulischen und praktischen Ausbildung nach Anlage 4 vorliegen.

(4) Die Zulassung zur Prüfung sowie die Prüfungstermine sollen dem Prüfling spätestens zwei Wochen vor Prüfungsbeginn schriftlich mitgeteilt werden.

#### § 30 Schriftlicher Teil der Prüfung

(1) Der schriftliche Teil der Prüfung erstreckt sich auf alle vermittelten Themenbereiche der Anlage 4.

(2) Der Prüfling hat in einer Aufsichtsarbeit schriftlich gestellte Aufgaben zu bearbeiten.
Die Aufsichtsarbeit dauert 120 Minuten.
Die Aufsichtsführenden werden von der Schulleitung bestellt.

(3) Die Aufgaben für die Aufsichtsarbeit werden von der oder dem Vorsitzenden des Prüfungsausschusses ausgewählt.
Die Aufsichtsarbeit ist von mindestens zwei Fachprüferinnen oder Fachprüfern zu benoten.
Aus den Noten der Fachprüferinnen oder Fachprüfer bildet die oder der Vorsitzende des Prüfungsausschusses im Benehmen mit den Fachprüferinnen oder Fachprüfern die Note für die Aufsichtsarbeit.
Der schriftliche Teil der Prüfung ist bestanden, wenn die Aufsichtsarbeit mindestens mit „ausreichend“ benotet wird.

#### § 31 Praktischer Teil der Prüfung

(1) Die praktische Prüfung findet in der Lehrleitstelle der Landesschule und Technischen Einrichtung für Brand- und Katastrophenschutz statt.

(2) Der praktische Teil der Prüfung erstreckt sich auf alle vermittelten Themenbereiche der Anlage 4.
Der Prüfling übernimmt bei vier vorgegebenen Fallbeispielen die anfallenden Aufgaben einer Leitstellendisponentin oder eines Leitstellendisponenten.

(3) Eines der Fallbeispiele muss den Bereich Herzkreislaufstillstand und Anleitung zur Reanimation (Telefonreanimation) enthalten.

(4) Jedes Fallbeispiel wird durch ein Fachgespräch ergänzt.
In diesem hat der Prüfling sein Handeln zu erläutern und zu begründen sowie die Prüfungssituation zu reflektieren.

(5) Die Auswahl der Fallbeispiele erfolgt durch die Vorsitzende oder den Vorsitzenden des Prüfungsausschusses auf Vorschlag der Schule.
Dabei sind der aktuelle Standard und die Besonderheiten und Erfordernisse der Notfallmedizin und einer zeitgemäßen Notfallversorgung angemessen zu berücksichtigen.

(6) Die Prüflinge werden einzeln oder zu zweit geprüft.
Die Prüfung soll einschließlich des Fachgesprächs für jedes Fallbeispiel mindestens 20 und nicht länger als 40 Minuten dauern.
Die Prüfung kann auf zwei aufeinanderfolgende Tage verteilt werden.

(7) Jedes Fallbeispiel wird von mindestens zwei Fachprüferinnen oder Fachprüfern abgenommen und benotet.
Aus den Noten der Fachprüferinnen oder Fachprüfer bildet die oder der Vorsitzende des Prüfungsausschusses im Benehmen mit den Fachprüferinnen oder Fachprüfern die Prüfungsnote für jedes Fallbeispiel.
Aus diesen Noten bildet die oder der Vorsitzende des Prüfungsausschusses die Gesamtnote für den praktischen Teil der Prüfung.
Der praktische Teil der Prüfung ist bestanden, wenn jedes Fallbeispiel mindestens mit „ausreichend“ benotet wird.

#### § 32 Durchführung der Prüfung

(1) Für Erkrankung, Rücktritt, Versäumnis, Täuschungsversuch und ordnungswidriges Verhalten, Niederschrift, Bewertung der Prüfungsleistung, Wiederholen der Prüfung sowie Prüfungsunterlagen gelten die §§ 8, 9, 13, 14, 16 und 17 entsprechend.

(2) Die Prüfung ist nicht bestanden, wenn ein Prüfungsteil nach § 27 Absatz 3 mit der Note mangelhaft oder ungenügend bewertet wurde.
Wer die Prüfung bestanden hat, erhält ein Zeugnis nach Anlage 9.

### Abschnitt 5  
Fortbildung, Ausnahmen

#### § 33 Fortbildung

(1) Der Mindestumfang der rettungsdienstlichen Fortbildung beträgt 24 Stunden im Kalenderjahr für jede Mitarbeiterin und jeden Mitarbeiter einer integrierten Regionalleitstelle.

(2) Disponentinnen und Disponenten der integrierten Regionalleitstellen haben jährlich eine vierwöchige praktische Tätigkeit im Einsatzdienst der Feuerwehr und im Rettungsdienst, davon mindestens 80 Stunden auf Rettungsdienstfahrzeugen, abzuleisten.
Zusätzlich haben sie alle drei Jahre eine 40-stündige Fortbildung nachzuweisen.

#### § 34 Ausnahmen

Die für das Rettungswesen zuständige oberste Landesbehörde kann auf begründeten Antrag des Aufgabenträgers im Einzelfall Ausnahmen von den Qualifikationsanforderungen zulassen.

 

* * *

### Anlagen

1

[Anlage 1 - Rahmenlehrplan für den Erwerb der rettungsdienstlichen Qualifikation](/br2/sixcms/media.php/68/GVBl_II_53_2018-Anlage-1.pdf "Anlage 1 - Rahmenlehrplan für den Erwerb der rettungsdienstlichen Qualifikation") 218.1 KB

2

[Anlage 2 - Rahmenlehrplan für den Erwerb der feuerwehrtechnischen Grundqualifikation](/br2/sixcms/media.php/68/GVBl_II_53_2018-Anlage-2.pdf "Anlage 2 - Rahmenlehrplan für den Erwerb der feuerwehrtechnischen Grundqualifikation") 157.3 KB

3

[Anlage 3 - Rahmenlehrplan für den Erwerb der feuerwehrtechnischen Führungsqualifikation](/br2/sixcms/media.php/68/GVBl_II_53_2018-Anlage-3.pdf "Anlage 3 - Rahmenlehrplan für den Erwerb der feuerwehrtechnischen Führungsqualifikation") 153.5 KB

4

[Anlage 4 - Disponentenlehrgang](/br2/sixcms/media.php/68/GVBl_II_53_2018-Anlage-4.pdf "Anlage 4 - Disponentenlehrgang") 192.2 KB

5

[Anlage 5 - Bescheinigung über den erfolgreichen Abschluss der praktischen Ausbildung zum Leitstellendisponenten gemäß Leitstellendisponentenverordnung](/br2/sixcms/media.php/68/GVBl_II_53_2018-Anlage-5.pdf "Anlage 5 - Bescheinigung über den erfolgreichen Abschluss der praktischen Ausbildung zum Leitstellendisponenten gemäß Leitstellendisponentenverordnung") 196.1 KB

6

[Anlage 6 - Zeugnis über den Anpassungslehrgang zum Erwerb der rettungsdienstlichen Qualifikation für Disponentinnen und Disponenten integrierter Regionalleitstellen](/br2/sixcms/media.php/68/GVBl_II_53_2018-Anlage-6.pdf "Anlage 6 - Zeugnis über den Anpassungslehrgang zum Erwerb der rettungsdienstlichen Qualifikation für Disponentinnen und Disponenten integrierter Regionalleitstellen") 168.6 KB

7

[Anlage 7 - Zeugnis über den Anpassungslehrgang zum Erwerb der feuerwehrtechnischen Grundqualifikation](/br2/sixcms/media.php/68/GVBl_II_53_2018-Anlage-7.pdf "Anlage 7 - Zeugnis über den Anpassungslehrgang zum Erwerb der feuerwehrtechnischen Grundqualifikation") 175.4 KB

8

[Anlage 8 - Zeugnis über den Anpassungslehrgang zum Erwerb der feuerwehrtechnischen Führungsqualifikation für Disponentinnen und Disponenten integrierter Regionalleitstellen](/br2/sixcms/media.php/68/GVBl_II_53_2018-Anlage-8.pdf "Anlage 8 - Zeugnis über den Anpassungslehrgang zum Erwerb der feuerwehrtechnischen Führungsqualifikation für Disponentinnen und Disponenten integrierter Regionalleitstellen") 188.7 KB

9

[Anlage 9 - Zeugnis über den Disponentenlehrgang für Disponentinnen und Disponenten integrierter Regionalleitstellen](/br2/sixcms/media.php/68/GVBl_II_53_2018-Anlage-9.pdf "Anlage 9 - Zeugnis über den Disponentenlehrgang für Disponentinnen und Disponenten integrierter Regionalleitstellen") 170.6 KB