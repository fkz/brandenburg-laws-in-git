## Verordnung über die Zusammensetzung des Forstausschusses, die Berufung der Mitglieder sowie deren Aufwandsentschädigungen (Forstausschussverordnung - FoAV)

Auf Grund des § 33 Absatz 2 des Waldgesetzes des Landes Brandenburg vom 20. April 2004 (GVBl. I S. 137), der durch Artikel 2 des Gesetzes vom 19. Dezember 2008 (GVBl. I S. 367) geändert worden ist, verordnet der Minister für Landwirtschaft, Umwelt und Klimaschutz:

#### § 1 Aufgabe

Der Forstausschuss berät die oberste Forstbehörde bei der Durchführung des Waldgesetzes des Landes Brandenburg und bei Fragen von grundsätzlicher Bedeutung.
In diesem Rahmen nimmt er zu Themen, welche die Forstwirtschaft mittelbar oder unmittelbar berühren, Stellung.

#### § 2 Zusammensetzung

Der Forstausschuss besteht aus bis zu 23 Mitgliedern, die jeweils durch eine Stellvertretung vertreten werden können.
Die Mitglieder werden vom für Forsten zuständigen Mitglied der Landesregierung für die Dauer einer Legislaturperiode berufen und, sofern sie vor Ablauf der Amtszeit ausscheiden, abberufen.
Der Minister beruft als Mitglieder

1.  sieben Vertreter des Privat- und Körperschaftswaldes; davon drei Vertreter auf Vorschlag des Waldbesitzerverbandes Brandenburg e. V., zwei Vertreter auf Vorschlag des Waldbauernverbandes Brandenburg e. V., einen Vertreter auf Vorschlag der Familienbetriebe Land und Forst Brandenburg e. V.
    und einen Vertreter auf Vorschlag des Städte- und Gemeindebundes Brandenburg,
2.  einen Vertreter des Landeswaldes auf Vorschlag des Landesbetriebes Forst Brandenburg,
3.  zwei Vertreter der Forstbediensteten und ständig beschäftigten Arbeitnehmer; davon einen Vertreter auf Vorschlag des Bundes Deutscher Forstleute Brandenburg-Berlin und einen Vertreter auf Vorschlag der Industriegewerkschaft Bauen-Agrar-Umwelt,
4.  zwei Vertreter der Forstwirtschaft; davon einen Vertreter auf Vorschlag der Arbeitsgemeinschaft Naturgemäße Waldwirtschaft Brandenburg e. V.
    und einen Vertreter auf Vorschlag des Brandenburgischen Forstvereins e. V.,
5.  zwei Vertreter der Wissenschaft; davon einen Vertreter auf Vorschlag der Hochschule für nachhaltige Entwicklung Eberswalde und einen Vertreter auf Vorschlag des Potsdam-Instituts für Klimafolgenforschung,
6.  einen Vertreter der Holzwirtschaft auf Vorschlag des Deutsche Holz- und Sägeindustrie Bundesverbandes e. V.,
7.  einen Vertreter der Lohnunternehmen auf Vorschlag des Forstunternehmerverbandes Brandenburg e. V.,
8.  einen Vertreter der Forstbaumschulen auf Vorschlag des Verbandes Deutscher Forstbaumschulen e. V.,
9.  einen Vertreter der Forstsachverständigen auf Vorschlag des Bundesverbandes Freiberuflicher Forstsachverständiger e. V.,
10.  einen Vertreter des Naturschutzes aus dem Kreis der anerkannten Naturschutzverbände auf Vorschlag des Büros der anerkannten Naturschutzverbände GbR,
11.  zwei Vertreter der Jägerschaft; davon einen Vertreter auf Vorschlag des Ökologischen Jagdvereines Brandenburg-Berlin e. V.
    und einen Vertreter auf Vorschlag des Landesjagdverbandes Brandenburg e. V.
    sowie
12.  bis zu zwei zusätzliche Personen im Einzelfall auf Vorschlag der obersten Forstbehörde.

#### § 3 Vertretung

Der Forstausschuss wählt aus seiner Mitte eine vorsitzende Person sowie eine Stellvertretung.
Der Forstausschuss wird durch die vorsitzende Person, bei deren Verhinderung durch die Stellvertretung, vertreten.

#### § 4 Sitzung

(1) Der Forstausschuss tritt nach Bedarf, mindestens einmal im Jahr, zusammen.
Der Ausschuss ist einzuberufen, wenn das für Forsten zuständige Mitglied der Landesregierung oder die Mehrheit der Mitglieder des Forstausschusses dies verlangen.

(2) Die Sitzungen werden durch die vorsitzende Person, bei deren Verhinderung durch die Stellvertretung, unter Angabe der Tagesordnung einberufen.

(3) Zu den Sitzungen können andere Personen hinzugezogen werden, wenn dies sachdienlich ist.

(4) Die Sitzungen sind nicht öffentlich.

#### § 5 Beschlussfähigkeit, Beschlussfassung

(1) Der Forstausschuss ist beschlussfähig, wenn mindestens die einfache Mehrheit der Mitglieder anwesend ist.

(2) Beschlüsse des Forstausschusses bedürfen der einfachen Mehrheit der abgegebenen Stimmen.
Bei Stimmengleichheit gibt die Stimme der vorsitzenden Person, bei deren Verhinderung die Stimme der Stellvertretung, den Ausschlag.

#### § 6 Arten der Entschädigung

(1) Die Tätigkeit im Forstausschuss ist ehrenamtlich.

(2) Die Mitglieder des Forstausschusses sowie die stellvertretenden Mitglieder in Wahrnehmung ihrer Stellvertreterfunktion erhalten auf Antrag

1.  eine Entschädigung für den Zeitaufwand (Sitzungstagegeld) nach § 7,
2.  eine Fahrtkostenentschädigung nach § 8 und
3.  eine Entschädigung für Verdienstausfall nach § 9.

#### § 7 Sitzungstagegeld

Zur Abgeltung des durch die Teilnahme an den Sitzungen des Forstausschusses entstandenen Aufwands wird ein Sitzungstagegeld bis zur Höhe des Satzes gewährt, der Landesbeamten nach den Vorschriften über die Reisekostenvergütung als Tagegeld zusteht.
Die Vorschriften, nach denen bei Reisen, die an demselben Kalendertag angetreten oder beendet werden, sich das Tagegeld vermindert oder ein Tagegeld nicht gewährt wird, gelten entsprechend.

#### § 8 Fahrtkostenentschädigung

Die Fahrkosten für die zur Sitzung notwendigen Reisen vom Wohnort oder Dienstort zum Ort der Sitzung und zurück werden gemäß den Bestimmungen der §§ 5 und 6 des Bundesreisekostengesetzes erstattet.

#### § 9 Entschädigung für Verdienstausfall

(1) Die Ausschussmitglieder können für ihren Verdienstausfall entschädigt werden.

(2) Die Höhe der Entschädigung richtet sich nach dem regelmäßigen Bruttoverdienst.
Dabei ist höchstens der Satz anzusetzen, der einem Zeugen nach § 22 Satz 1 des Justizvergütungs- und -entschädigungsgesetzes als Höchstbetrag zusteht.

(3) Der Verdienstausfall wird für die versäumte Zeit in der regelmäßigen Arbeitszeit berechnet.

(4) Der Verdienstausfall wird nur gegen Bescheinigung des Arbeitgebers erstattet.
Selbständige und freiberuflich Tätige müssen den Verdienstausfall glaubhaft machen.

(5) Angehörige des öffentlichen Dienstes erhalten keine Entschädigung für Verdienstausfall.

#### § 10 Geltendmachung

(1) Anträge auf Aufwandsentschädigung sind schriftlich an das für Forsten zuständige Ministerium zu richten.

(2) Auf Antrag wird zu Jahresbeginn eine Bescheinigung für Einkommensteuerzwecke über die im vorhergehenden Jahr gezahlten Entschädigungen ausgestellt.

#### § 11 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Forstausschussverordnung vom 13. Januar 2010 (GVBl. II Nr. 3) außer Kraft.

Potsdam, den 4.
Juni 2020

Der Minister für Landwirtschaft,  
Umwelt und Klimaschutz

Axel Vogel