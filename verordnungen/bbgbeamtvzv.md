## Verordnung zur Übertragung von Zuständigkeiten auf dem Gebiet der Versorgung der Beamtinnen und Beamten sowie der Richterinnen und Richter des Landes Brandenburg (Brandenburgische Beamtenversorgungszuständigkeitsverordnung - BbgBeamtVZV)

Auf Grund des § 3 Absatz 1 Satz 2 und 3 und des § 89 Absatz 1 Satz 3 des Brandenburgischen Beamtenversorgungsgesetzes vom 20.
November 2013 (GVBl.
I Nr. 32 S. 77), von denen § 89 Absatz 1 Satz 3 durch Artikel 3 Nummer 3 des Gesetzes vom 10.
Juli 2017 (GVBl. I Nr. 14 S. 6) eingefügt worden ist, verordnet die Landesregierung:

#### § 1 Pensionsbehörde

(1) Pensionsbehörde für die Versorgungsberechtigten des Landes gemäß § 3 Absatz 1 Satz 1 des Brandenburgischen Beamtenversorgungsgesetzes ist die Zentrale Bezügestelle des Landes Brandenburg.

(2) Die Pensionsbehörde trifft auch die folgenden Entscheidungen für die Versorgungsberechtigten:

1.  Anordnung der amtsärztlichen Untersuchung gemäß § 9 Absatz 3 Satz 1 und § 57 Absatz 6 Satz 2 des Brandenburgischen Beamtenversorgungsgesetzes,
2.  Versagung von Unfallfürsorgeleistungen gemäß § 48 Absatz 2 Satz 1 des Brandenburgischen Beamtenversorgungsgesetzes,
3.  Feststellung gemäß § 68 Absatz 1 des Brandenburgischen Beamtenversorgungsgesetzes, dass das Ableben einer oder eines Verschollenen mit Wahrscheinlichkeit anzunehmen ist.

(3) Die Absätze 1 und 2 gelten nur, soweit § 2 nichts Abweichendes bestimmt.

(4) Die in § 11 Absatz 4 Satz 2 und Absatz 5 Satz 1 und § 13 Absatz 3 Satz 2 des Brandenburgischen Beamtenversorgungsgesetzes geregelten Zuständigkeiten bleiben unberührt.

#### § 2 Aufgaben der obersten Dienstbehörden auf dem Gebiet der Unfallfürsorge

(1) Die oberste Dienstbehörde oder die von ihr bestimmte Stelle (personalaktenführende Stelle) nimmt folgende Aufgaben auf dem Gebiet der Unfallfürsorge nach Abschnitt 2 Unterabschnitt 3 des Brandenburgischen Beamtenversorgungsgesetzes für aktive Beamtinnen, Beamte, Richterinnen und Richter wahr:

1.  Entscheidung gemäß § 47 Absatz 3 Satz 3 des Brandenburgischen Beamtenversorgungsgesetzes, ob ein Dienstunfall vorliegt und ob Unfallfürsorge gewährt wird,
2.  Versagung von Unfallfürsorgeleistungen gemäß § 48 Absatz 2 Satz 1 des Brandenburgischen Beamtenversorgungsgesetzes,
3.  Anordnung der amtsärztlichen Untersuchung gemäß § 9 Absatz 3 Satz 1 und § 54 Absatz 3 Satz 2 in Verbindung mit § 9 Absatz 3 Satz 1 des Brandenburgischen Beamtenversorgungsgesetzes.

(2) § 89 Absatz 1 Satz 1 und 2 des Brandenburgischen Beamtenversorgungsgesetzes bleibt unberührt.

#### § 3 Rückforderung von Versorgungsbezügen

(1) Die Pensionsbehörde ist zuständig für die Rückforderung zu viel gezahlter Versorgungsbezüge gemäß § 7 Absatz 2 des Brandenburgischen Beamtenversorgungsgesetzes.

(2) Zuständige Stelle für die Zustimmung zu einer Entscheidung über das Absehen von der Rückforderung gemäß § 7 Absatz 2 Satz 3 des Brandenburgischen Beamtenversorgungsgesetzes ist

1.  die Pensionsbehörde für die Versorgungsberechtigten und
2.  die personalaktenführende Stelle für die aktiven Beamtinnen, Beamten, Richterinnen und Richter.

#### § 4 Weitere Aufgaben der Pensionsbehörde

Der Pensionsbehörde werden im Übrigen folgende Aufgaben übertragen:

1.  Berechnung, Festsetzung, Erhebung und Zahlbarmachung von Versorgungszuschlägen bei Abordnungen oder Beurlaubungen,
2.  Festsetzung des Kapitalbetrages gemäß § 82 des Brandenburgischen Beamtenversorgungsgesetzes,
3.  Erstattung und Anforderung von Abfindungen oder Anteilen an den Versorgungsbezügen nach dem Versorgungslastenteilungs-Staatsvertrag vom 26.
    Januar 2010 (GVBl. I Nr. 27; 2011 I Nr. 5) oder auf der Grundlage vergleichbarer Regelungen; dies gilt gleichermaßen für landesinterne Dienstherrenwechsel gemäß § 83 Absatz 1 des Brandenburgischen Beamtenversorgungsgesetzes,
4.  Anforderung von Anteilen an den Versorgungsbezügen nach § 107c des Beamtenversorgungsgesetzes in der am 31. August 2006 geltenden Fassung,
5.  Wahrnehmung der Befugnisse des Trägers der Versorgungslast und Erteilung von Auskünften an die Familiengerichte im Zusammenhang mit der verfahrensrechtlichen Auskunftspflicht gemäß § 220 des Gesetzes über das Verfahren in Familiensachen und in den Angelegenheiten der freiwilligen Gerichtsbarkeit,
6.  Durchführung der Nachversicherung in der gesetzlichen Rentenversicherung gemäß § 8 des Sechsten Buches Sozialgesetzbuch,
7.  Beantwortung von Anfragen der Rentenversicherungsträger nach § 71 Absatz 4 des Sechsten Buches Sozialgesetzbuch,
8.  Erstattung von Aufwendungen des Trägers der Rentenversicherung durch den Träger der Versorgungslast gemäß § 225 Absatz 1 des Sechsten Buches Sozialgesetzbuch sowie gemäß der Versorgungsausgleichs-Erstattungsverordnung,
9.  Berechnung, Zahlbarmachung, Auszahlung und Abrechnung der in dieser Verordnung genannten Leistungen, auch wenn die Festsetzung von anderen Stellen vorgenommen wurde.

#### § 5 Beamtinnen und Beamte der mittelbaren Landesverwaltung

Diese Verordnung findet keine Anwendung auf Beamtinnen und Beamte der Gemeinden und Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts.

#### § 6 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Beamtenversorgungs-Zuständigkeitsverordnung vom 28.
Januar 1997 (GVBl. II S. 53), die durch Artikel 3 der Verordnung vom 10.
Mai 2004 (GVBl.
II S.
329, 330) geändert worden ist, außer Kraft.

Potsdam, den 21.
Mai 2021

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Die Ministerin der Finanzen und für Europa

Katrin Lange