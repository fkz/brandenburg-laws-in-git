## Verordnung zur Übertragung von Ermächtigungen auf dem Gebiet des Düngerechts

Auf Grund

*   des § 13a Absatz 10 Satz 1 und 2 und des § 15 Absatz 6 Satz 1 und 2 Nummer 1 des Düngegesetzes vom 9.
    Januar 2009 (BGBl.
    I S. 54, 136), von denen § 13a Absatz 10 durch Artikel 1 Nummer 8a des Gesetzes vom 5.
    Mai 2017 (BGBl.
    I S.
    1068) eingefügt und § 15 Absatz 6 Satz 2 Nummer 1 durch Artikel 1 Nummer 10 Buchstabe a Doppelbuchstabe aa des Gesetzes vom 5.
    Mai 2017 (BGBl.
    I S.
    1068) geändert worden ist, in Verbindung mit § 6 der Verordnung über das Inverkehrbringen und Befördern von Wirtschaftsdünger vom 21. Juli 2010 (BGBl.
    I S.
    1062), in Verbindung mit § 13 Absatz 2 und § 13a Absatz 1 Satz 1, Absatz 3 und 7 der Düngeverordnung vom 26.
    Mai 2017 (BGBl.
    I S.
    1305), von denen durch Artikel 1 der Verordnung vom 28.
    April 2020 (BGBl.
    I S.
    846) § 13 Absatz 2 geändert und § 13a Absatz 1 Satz 1 und Absatz 3 und 7 eingefügt worden sind, in Verbindung mit § 7 Absatz 3 der Stoffstrombilanzverordnung vom 14.
    Dezember 2017 (BGBl.
    I S.
    3942; 2018 I S.
    360),
*   des § 6 Absatz 2 und 4 des Landesorganisationsgesetzes vom 24.
    Mai 2004 (GVBl.
    I S.
    186), der durch Artikel 2 des Gesetzes vom 10.
    Juli 2014 (GVBl.
    I Nr. 28 S.
    2) geändert worden ist, und
*   des § 36 Absatz 2 Satz 1 und 2 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
    Februar 1987 (BGBl.
    I S.
    602)

verordnet die Landesregierung:

#### § 1 

Die Ermächtigungen der Landesregierung zum Erlass von Rechtsverordnungen nach

1.  § 6 der Verordnung über das Inverkehrbringen und Befördern von Wirtschaftsdünger,
2.  § 13 Absatz 2 und § 13a Absatz 1 Satz 1, Absatz 3 und 7 der Düngeverordnung,
3.  § 7 Absatz 3 der Stoffstrombilanzverordnung,
4.  § 6 Absatz 2 des Landesorganisationsgesetzes in Verbindung mit dem Düngegesetz und der aufgrund dieses Gesetzes erlassenen Rechtsverordnungen und
5.  § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach dem Düngegesetz sowie der aufgrund dieses Gesetzes erlassenen Rechtsverordnungen

werden auf das für Landwirtschaft zuständige Mitglied der Landesregierung übertragen.

#### § 2 

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 31.
August 2020

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Minister für Landwirtschaft,  
Umwelt und Klimaschutz

Axel Vogel