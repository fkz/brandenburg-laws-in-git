## Verordnung über die Organisation und die Durchführung von Brandverhütungsschauen (Brandverhütungsschauverordnung - BrVSchV)

Auf Grund des § 49 Absatz 1 des Brandenburgischen Brand- und Katastrophenschutzgesetzes vom 24. Mai 2004 (GVBl. I S. 197) verordnet die Landesregierung:

### § 1   
Brandverhütungsschau

(1) Die Brandverhütungsschau dient der Feststellung von Gefahrenquellen und Mängeln im Brandschutz.

(2) Ziel einer Brandverhütungsschau ist es, brandschutztechnische Mängel festzustellen, um der Entstehung und Ausbreitung von Schadenfeuern vorzubeugen, damit Menschen, Tiere, Sachwerte und unwiederbringliches Kulturgut sowie die Umwelt vor Brand- und Explosionsgefahren geschützt werden.

### § 2   
Zuständigkeiten

Die Brandverhütungsschau wird von der Brandschutzdienststelle gemäß § 32 des Brandenburgischen Brand- und Katastrophenschutzgesetzes durchgeführt.

### § 3   
Beauftragung

(1) Die Brandschutzdienststelle kann in Betrieben und Einrichtungen mit einer Werkfeuerwehr die Leitung der Werkfeuerwehr nach § 33 Absatz 4 des Brandenburgischen Brand- und Katastrophenschutzgesetzes mit der Durchführung der Brandverhütungsschau beauftragen, sofern die Unternehmensleitung zustimmt.

(2) Die Werkfeuerwehr hat der Brandschutzdienststelle die Anzahl der durchgeführten Brandverhütungsschauen, die dabei festgestellten Mängel, den Stand ihrer Beseitigung und die Nachschauen in einem jährlichen Ergebnisprotokoll nachzuweisen.

(3) Die Brandschutzdienststelle kann geeignete Dritte, die auch Werkfeuerwehren sein können, mit der Durchführung der Brandverhütungsschau beauftragen.
Die Beauftragungen von Werkfeuerwehren als geeignete Dritte können nur mit Zustimmung der Unternehmensleitungen erfolgen.

### § 4   
Fachliche Qualifikation, Fortbildung

(1) Zur Durchführung der Brandverhütungsschau sind nur Personen geeignet, die

1.  mindestens die Befähigung zum gehobenen feuerwehrtechnischen Dienst haben, eine der Laufbahnausbildung zum gehobenen feuerwehrtechnischen Dienst entsprechende Weiterbildung im Angestelltenverhältnis absolviert haben oder über eine dem Inhalt und dem Umfang nach der Laufbahnausbildung zum gehobenen feuerwehrtechnischen Dienst vergleichbare Weiterbildung verfügen oder
2.  als Prüfingenieurin oder Prüfingenieur für Brandschutz von der Obersten Bauaufsichtsbehörde des Landes Brandenburg oder einer von ihr bestimmten Stelle anerkannt sind.

(2) Personen, die die Brandverhütungsschau durchführen, haben sich regelmäßig durch Teilnahme an Lehrgängen, Seminaren und Tagungen zum vorbeugenden Brandschutz fortzubilden.
Die Fortbildung soll mindestens 40 Stunden innerhalb von zwei Jahren umfassen.

### § 5   
Durchführung

(1) Die Brandverhütungsschau soll der Eigentümerin, dem Eigentümer, der Besitzerin, dem Besitzer oder der oder dem sonstigen Nutzungsberechtigten mindestens zwei Wochen vor ihrer Durchführung schriftlich angezeigt werden.
Dabei ist auf die Pflichten nach § 33 Absatz 1 Satz 3 und 4 des Brandenburgischen Brand- und Katastrophenschutzgesetzes hinzuweisen.

(2) Soweit im Einzelfall besondere Dringlichkeit besteht, insbesondere bei Gefahr im Verzug, kann von der in Absatz 1 genannten Frist abgewichen werden.

(3) An der Brandverhütungsschau sollen die Eigentümerin, der Eigentümer, die Besitzerin, der Besitzer oder die oder der sonstige Nutzungsberechtigte oder eine von dieser oder diesem beauftragte Person teilnehmen.

(4) Die zur Durchführung der Brandverhütungsschau erforderlichen Unterlagen, insbesondere Brandschutzkonzepte und Nachweise zu Überprüfungen sicherheitstechnischer Anlagen, sind im Vorfeld der Brandverhütungsschau auf Verlangen schriftlich zu übermitteln.

### § 6   
Beteiligung Anderer

(1) Dem Träger für den örtlichen Brandschutz und die örtliche Hilfeleistung nach § 2 Absatz 1 Nummer 1 des Brandenburgischen Brand- und Katastrophenschutzgesetzes ist Gelegenheit zu geben, an der Brandverhütungsschau teilzunehmen, sofern er nicht selbst Brandschutzdienststelle ist.

(2) Soweit erforderlich, sind die für die Bauaufsicht sowie den Arbeits- und Immissionsschutz zuständigen Behörden vor der Durchführung der Brandverhütungsschau zu benachrichtigen und auf ihr Verlangen an der Brandverhütungsschau zu beteiligen.
Unterliegen die baulichen Anlagen der Aufsicht weiterer Behörden, sollen diese beteiligt werden, sofern dies für die Brandverhütungsschau zweckdienlich ist.

(3) Bei brandverhütungsschaupflichtigen baulichen Anlagen, die im Eigentum des Landes Brandenburg sind, ist der Brandenburgische Landesbetrieb für Liegenschaften und Bauen zu beteiligen.

### § 7  
Objekte der Brandverhütungsschau

(1) Die in der Anlage aufgeführten baulichen Anlagen unterliegen nach den dort genannten zeitlichen Abständen grundsätzlich der Brandverhütungsschau gemäß § 33 Absatz 1 des Brandenburgischen Brand- und Katastrophenschutzgesetzes.

(2) Die Brandschutzdienststellen sind verpflichtet, die baulichen Anlagen nach Absatz 1 zu erfassen.

### § 8   
Umfang der Brandverhütungsschau

(1) Bei der Brandverhütungsschau sind zu prüfen:

1.  die Organisation der Maßnahmen zur Gewährleistung des vorbeugenden Brandschutzes und
2.  alle Maßnahmen, die für den abwehrenden Brandschutz zur schnellen und sicheren Evakuierung und Brandbekämpfung erforderlich sind.

(2) Bei der Brandverhütungsschau ist festzustellen, ob insbesondere:

1.  aufgrund baulicher, technischer, betrieblicher oder sonstiger Mängel oder aufgrund einer von dem Baugenehmigungsbescheid oder einer nach der Genehmigung nach dem Bundes-Immissionsschutzgesetz abweichenden Nutzung der baulichen Anlage oder der Lagerung brennbarer Stoffe die Gefahr der Entstehung von Bränden oder Explosionen besteht oder die Gefahr der Brandausbreitung erhöht wird,
2.  aufgrund der Brandenburgischen Bauordnung erlassenen Rechtsvorschriften oder aufgrund anderer Rechtsvorschriften angeordnete brandschutztechnische, betriebliche und organisatorische Maßnahmen durchgeführt worden sind,
3.  die vorgeschriebenen Löschmittel oder Sonderlöschmittel vorgehalten werden sowie die sicherheitstechnischen Anlagen für den Brandschutz entsprechend gekennzeichnet, geprüft und betriebsbereit sind,
4.  die Flucht- und Rettungswege benutzbar, frei von brennbaren Stoffen und entsprechend gekennzeichnet sind,
5.  die vorgeschriebenen Brandschutzordnungen bekannt sind, eingehalten werden und an den Zugängen von Lager- und Bearbeitungsstätten für Stoffe mit besonderer Brand- und Explosionsgefahr entsprechende Hinweise angebracht sind und
6.  die baulichen Anlagen für die Feuerwehr erreichbar und frei zugänglich sind, im Brandfall die Möglichkeit zur Rettung von Menschen und Tieren besteht, eine wirksame Brandbekämpfung gewährleistet werden kann, Flucht- und Rettungswegpläne gut sichtbar sind und die Löschwasserversorgung und, wenn erforderlich, die Löschwasserrückhaltung gesichert ist.

### § 9   
Niederschrift, Anordnung der Mängelbeseitigung

(1) Die Brandschutzdienststelle erstellt über die durchgeführte Brandverhütungsschau eine Niederschrift, die der Eigentümerin, dem Eigentümer, der Besitzerin, dem Besitzer oder der oder dem sonstigen Nutzungsberechtigten und den beteiligten Behörden auszuhändigen ist.
Ist eine geeignete Dritte oder ein geeigneter Dritter mit der Durchführung der Brandverhütungsschau beauftragt worden, erstellt diese oder dieser die Niederschrift und händigt diese der Eigentümerin, dem Eigentümer, der Besitzerin, dem Besitzer oder der oder dem sonstigen Nutzungsberechtigten sowie der Brandschutzdienststelle aus.

(2) Sind bei der Brandverhütungsschau Mängel festgestellt worden, ordnet die Brandschutzdienststelle deren unverzügliche Beseitigung an.
Der Eigentümerin, dem Eigentümer, der Besitzerin, dem Besitzer oder der oder dem sonstigen Nutzungsberechtigten ist dabei eine Frist zu setzen, innerhalb derer die Brandschutzdienststelle über die durchgeführten Maßnahmen der Mängelbeseitigung schriftlich zu unterrichten ist.

(3) Werden Mängel festgestellt, die dem Aufsichtsbereich anderer Behörden unterliegen, sind diese unverzüglich schriftlich zu unterrichten, sofern sie nicht an der Brandverhütungsschau teilgenommen haben.

### § 10   
Nachschau

(1) Nach Ablauf der Frist gemäß § 9 Absatz 2 Satz 2 führt die Brandschutzdienststelle auf der Grundlage der Unterrichtung der Eigentümerin, des Eigentümers, der Besitzerin, des Besitzers oder der oder des sonstigen Nutzungsberechtigten über die Mängelbeseitigung eine Nachschau durch.
Wird dabei festgestellt, dass die Mängel nicht oder nicht ausreichend beseitigt worden sind, hat die Brandschutzdienststelle die notwendigen Maßnahmen einzuleiten.

(2) Die Brandschutzdienststelle kann auf die Nachschau verzichten, wenn die fristgemäße Beseitigung der festgestellten Mängel durch die Eigentümerin, den Eigentümer, die Besitzerin, den Besitzer oder die oder den sonstigen Nutzungsberechtigten nachgewiesen wird.

### § 11  
Verzicht auf eine Brandverhütungsschau

(1) Auf Antrag der Eigentümerin, des Eigentümers, der Besitzerin, des Besitzers oder der oder des sonstigen Nutzungsberechtigten kann die Brandschutzdienststelle auf die Durchführung einer Brandverhütungsschau der baulichen Anlage verzichten, wenn die Antragstellerin oder der Antragsteller durch Gutachten einer anerkannten Prüfingenieurin oder eines anerkannten Prüfingenieurs für Brandschutz den Nachweis erbringt, dass ihr oder sein Objekt keine Gefahrenquellen oder Mängel im Brandschutz aufweist.
Dies gilt zum Beispiel für die baulichen Anlagen, die in den Nummern 2.5, 4.3, 6 bis 11 sowie 12.3 und 12.6 der Anlage aufgeführt sind.

(2) Das Gutachten ist spätestens zwei Monate vor Ablauf der in der Anlage festgelegten Frist durch die Eigentümerin, den Eigentümer, die Besitzerin, den Besitzer oder die sonstige Nutzungsberechtigte oder den sonstigen Nutzungsberechtigten bei der Brandschutzdienststelle einzureichen und darf nicht älter als ein Jahr sein.

(3) Stellt die Brandschutzdienststelle fest, dass sie aufgrund des vorgelegten Gutachtens auf die Durchführung einer Brandverhütungsschau verzichtet, teilt sie dies der Eigentümerin, dem Eigentümer, der Besitzerin, dem Besitzer oder der oder dem sonstigen Nutzungsberechtigten schriftlich mit.

### § 12  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 1.
Januar 2014 in Kraft.
Gleichzeitig tritt die Brandschauverordnung vom 3. Juni 1994 (GVBl.
II S. 478), die durch die Verordnung vom 13.
August 2001 (GVBl. II S. 546) geändert worden ist, außer Kraft.

Potsdam, den 13.
Dezember 2013

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Der Minister des Innern

Ralf Holzschuher

* * *

**Anlage**   
(zu § 7 Absatz 1)

### Bauliche Anlagen, die gemäß § 7 Absatz 1 der Brandverhütungsschau unterliegen

Lfd.
Nr.

Brandverhütungsschaupflichtige bauliche Anlagen gemäß § 7 Absatz 1

Zeitabstand  
in Jahren (Höchstfrist)

**1**

**Bauliche Anlagen und Lager im Geltungsbereich des Störfallrechtes nach der Störfall-Verordnung – 12.
BImSchV – in der Fassung der Bekanntmachung vom 8. Juni 2005 (BGBl.
I S. 1598), die zuletzt durch die Verordnung vom 14.
August 2013 (BGBl.
I S. 3230) geändert worden ist, in der jeweils geltenden Fassung oder nach der Verordnung über die Anwendung der Störfall-Verordnung auf nicht wirtschaftlich genutzte Betriebsbereiche vom 9.
Mai 2000 (GVBl.
II S. 130) in der jeweils geltenden Fassung**

5

**2**

**Gewerbeobjekte**

2.1

Bauliche Anlagen und Einrichtungen zur Lagerung, Herstellung, Be- und Verarbeitung oder zum Umgang mit Gasen, explosionsgefährlichen, brennbaren sowie anderen Gefahrstoffen, sofern diese besondere Brandschutzmaßnahmen erfordern

5

2.2

Bauliche Anlagen und Einrichtungen zur Lagerung, Be- und Verarbeitung von sonstigen brennbaren Stoffen, wie Kunststoffe, gemischte Bau- und Gewerbeabfälle und heizwertreiche Sortierrestabfälle

5

2.3

Müllverbrennungsanlagen

5

2.4

Bauliche Anlagen und Einrichtungen, die der Industriebaurichtlinie unterliegen ab einer Brandabschnittsfläche von 1 600 m²

5

2.5

Hochregallager

5

2.6

Bauliche Anlagen und Einrichtungen zur landwirtschaftlichen und forstwirtschaftlichen Produktion und Lagerung mit mehr als 2 000 m² Grundfläche

5

**3**

**Pflege- und Betreuungsobjekte**

3.1

Krankenhäuser und Heime

3

3.2

Sonstige Einrichtungen zur Unterbringung oder Pflege von Personen

3

3.3

Tageseinrichtungen für Kinder, Menschen mit Behinderungen und Senioren (mehr als zwölf Personen)

3

**4**

**Einrichtungen zur Betreuung oder Ausbildung**

4.1

Schulen

3

4.2

Internate und Wohnheime

3

4.3

Gebäude mit Unterrichtsräumen, die von insgesamt mehr als 100 Personen genutzt werden können

3

**5**

**Beherbergungsstätten nach der Beherbergungsstättenbau-Verordnung vom 15. Juni 2001 (GVBl.
II S. 216), die durch Artikel 4 der Verordnung vom 23.
März 2005 (GVBl.
II S. 159) geändert worden ist, in der jeweils geltenden Fassung, mit mehr als zwölf Gästebetten**

5.1

Hotels, Pensionen und Herbergen

3

5.2

Sonstige Unterbringungseinrichtungen wie Obdachlosenasyle, Notunterkünfte, Aufnahmeeinrichtungen nach dem Landesaufnahmegesetz vom 17.
Dezember 1996 (GVBl.
I S. 358, 360), das zuletzt durch Artikel 15 des Gesetzes vom 13.
März 2012 (GVBl.
I Nr. 16 S. 7) geändert worden ist, in der jeweils geltenden Fassung

3

**6**

**Camping- und Wochenendhausplätze nach der Brandenburgischen Camping- und Wochenendhausplatz-Verordnung vom 18.
Mai 2005 (GVBl.
II S. 254) in der jeweils geltenden Fassung**

6.1

Campingplätze

5

6.2

Wochenendhausplätze

5

**7**

**Versammlungsstätten**

7.1

Versammlungsstätten im Sinne des § 1 Absatz 1 der Brandenburgischen Versammlungsstättenverordnung vom 29.
November 2005 (GVBl.
II S. 540), die durch die Verordnung vom 24.
August 2012 (GVBl.
II Nr. 76) geändert worden ist, in der jeweils geltenden Fassung

3

7.2

Versammlungsstätten, die nicht der Brandenburgischen Versammlungsstättenverordnung unterliegen, soweit es sich um nicht ebenerdige Veranstaltungs- und Gasträume (ab 100 Personen) handelt

3

**8**

**Verkaufsstätten**

8.1

Verkaufsstätten nach der Brandenburgischen Verkaufsstätten-Bauverordnung vom 21. Juli 1998 (GVBl.
II S. 524), die zuletzt durch Artikel 1 der Verordnung vom 23.
März 2005 (GVBl.
II S. 159) geändert worden ist, in der jeweils geltenden Fassung

3

8.2

Verkaufsstätten, die nicht der Brandenburgischen Verkaufsstätten-Bauverordnung unterliegen, soweit sie über eine Verkaufsfläche von mehr als 800 m² und einer direkten Verbindung zu anders genutzten Gebäuden oder Gebäudeteilen verfügen oder es sich um nicht ebenerdige Verkaufsflächen mit einer Verkaufsfläche von mehr als 500 m² handelt

3

**9**

**Mittel- und Großgaragen nach der Brandenburgischen Garagen- und Stellplatzverordnung vom 12. Oktober 1994 (GVBl.
II S. 948), die zuletzt durch die Verordnung vom 23.
März 2005 (GVBl.
II S. 159) geändert worden ist, in der jeweils geltenden Fassung**

9.1

Großgaragen

5

9.2

Unterirdische oder geschlossene Mittelgaragen

5

**10**

**Hochhäuser im Sinne des § 2 Absatz 3 Satz 3 der Brandenburgischen Bauordnung in der Fassung der Bekanntmachung vom 17. September 2008 (GVBl.
I S. 226), die zuletzt durch Artikel 2 des Gesetzes vom 29.
November 2010 (GVBl.
I Nr. 39 S. 2) geändert worden ist, in der jeweils geltenden Fassung**

5

**11**

**Verwaltungsobjekte**

11.1

Mehrgeschossige Verwaltungsgebäude ab mittlerer Höhe mit einer Kapazität von mindestens 200 Arbeitsplätzen

5

11.2

Verwaltungsbereiche in einem Gebäudekomplex mit einer Kapazität von mindestens 200 Arbeitsplätzen

5

**12**

**Andere brandverhütungsschaupflichtige Objekte**

12.1

Museen (ab 10 000 Besucherinnen und Besucher pro Jahr), Bibliotheken (ab 5 000 Besucherinnen und Besucher pro Jahr) und bedeutsame Archive (ab 200 lfm Schriftgut)

5

12.2

Religiöse Einrichtungen mit Gebetsräumen für mehr als 200 Personen

5

12.3

Hotel- und Gaststättenschiffe, die fest verankert sind

3

12.4

Terminals von Großflughäfen

2

12.5

Justizvollzugs- und Jugendarrestanstalten

5

12.6

Gebäude nach Anlage 1.2/1 Nummer 5 der Liste der Technischen Baubestimmungen, die gemäß § 3 Absatz 3 der Brandenburgischen Bauordnung als Technische Baubestimmungen eingeführt worden ist

5

12.7

Objekte nach örtlicher Festlegung (z.
B. besonders gefährdete Baudenkmäler)

5