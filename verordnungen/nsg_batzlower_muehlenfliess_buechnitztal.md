## Verordnung über das Naturschutzgebiet „Batzlower Mühlenfließ-Büchnitztal“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl. I S. 2542) in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nr. 43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Märkisch-Oderland wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Batzlower Mühlenfließ-Büchnitztal“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 245 Hektar.
Es umfasst zwei Teilflächen in folgenden Fluren:

Gemeinde: 

Gemarkung: 

Flur: 

Neuhardenberg 

Altfriedland 

1; 

Märkische Höhe 

Batzlow 

1, 2, 3, 4, 5; 

Reichenow-Möglin 

Reichenow 

3; 

 

Möglin 

1; 

Bliesdorf 

Kunersdorf 

3. 

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes dient der räumlichen Einordnung und ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 und 2 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 2 mit den Blattnummern 1 bis 8 aufgeführten Liegenschaftskarten.
Zur Orientierung über die betroffenen Grundstücke ist eine Flurstücksliste als Anlage 3 beigefügt.

(3) Die Verordnung mit Karten kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Märkisch-Oderland, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes, das die Täler des Batzlower Mühlenfließes und der Büchnitz sowie Teile eines zwischen den beiden Fließgewässern gelegenen Waldes umfasst, ist

1.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Eichen-Mischwälder trockenwarmer Standorte, der Trockenrasen und der Stillgewässer mit teilweise ausgedehnten Verlandungszonen;
2.  die Erhaltung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 10 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Großes Windröschen (Anemone sylvestris), Gemeine Grasnelke (Armeria maritima subsp.
    elongata), Gemeines Tausendgüldenkraut (Centaurium erythraea), Graue Skabiose (Scabiosa canescens), Kartäuser-Nelke (Dianthus carthusianorum), Schlüsselblume (Primula veris), Sand-Strohblume (Helichrysum arenarium), Pfriemengras (Stipa capillata), Ähriger Blauweiderich (Veronica spicata) und Breitblättriges Knabenkraut (Dactylorhiza majalis);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere der Säugetiere, Vögel, Amphibien, Reptilien, Fische und Insekten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Wasserfledermaus (Myotis daubentoni), Seeadler (Haliaeetus albicilla), Schwarzstorch (Ciconia nigra), Fischadler (Pandion haliaetus), Kranich (Grus grus), Eisvogel (Alcedo atthis), Drosselrohrsänger (Acrocephalus arundinaceus), Zauneidechse (Lacerta agilis), Ringelnatter (Natrix natrix), Moorfrosch (Rana arvalis), Knoblauchkröte (Pelobates fuscus), Schmerle (Barbatula barbatula) sowie Silbergrüner Bläuling (Polyommatus coridon), Esparsettenwidderchen (Zygaena carniolica), Bibernellwidderchen (Zygaena minos), Veränderliches Widderchen (Zygaena ephialtis) und Beilfleckwidderchen (Zygaena loti);
4.  die Erhaltung des Gebietes zur Umweltbeobachtung und wissenschaftlichen Untersuchung ökologischer Zusammenhänge;
5.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit des durch naturnahe Fließgewässer mit begleitenden Erlen-Eschenwäldern und Trockenhängen und -säumen gekennzeichneten Gebietes;
6.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil im System der Trockenrasenvorkommen Nordost- und Ostbrandenburgs entlang des Odertales zwischen Frankfurt (Oder) und Mescherin.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung des Gebietes von gemeinschaftlicher Bedeutung „Batzlower Mühlenfließ-Büchnitztal“ (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit seinen Vorkommen von

1.  Natürlichen eutrophen Seen mit Vegetation des Magnopotamions oder Hydrocharitions, Flüssen der planaren Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Feuchten Hochstaudenfluren, Labkraut-Eichen-Hainbuchenwald, Schlucht- und Hangmischwäldern (Tilio-Acerion) sowie Kiefernwäldern der sarmatischen Steppe als natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Subpannonischen Steppen-Trockenrasen, Auen-Wäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion) sowie Pannonischen Wäldern mit Quercus petraea und Carpinus betulus als prioritären natürlichen Lebensraumtypen im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
3.  Fischotter (Lutra lutra), Rotbauchunke (Bombina bombina), Kammmolch (Triturus cristatus), Steinbeißer (Cobitis taenia) sowie Bauchiger Windelschnecke (Vertigo moulinsiana) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet oder seine Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist das Betreten von Waldbereichen zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 10 jeweils nach dem 31.
    Juli eines jeden Jahres;
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb der nach öffentlichem Straßenrecht oder gemäß § 22 Absatz 5 des Brandenburgischen Naturschutzausführungsgesetzes als Reitwege markierten Wege zu reiten; § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg bleibt unberührt;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen;
13.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
14.  Hunde frei laufen zu lassen;
15.  Be- oder Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
16.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle) und Sekundärrohstoffdünger (zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
17.  Abfälle im Sinne des Kreislaufwirtschaftsgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
18.  Tiere zu füttern oder Futter bereitzustellen;
19.  Tiere auszusetzen oder Pflanzen anzusiedeln;
20.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
21.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
22.  Pflanzenschutzmittel jeder Art anzuwenden;
23.  Wiesen, Weiden oder sonstiges Grünland umzubrechen oder neu anzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und in § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide mit einer Besatzdichte von maximal 1,4 Raufutter verwertenden Großvieheinheiten (RGV) pro Hektar im Jahresmittel genutzt wird,
    2.  auf Grünland § 4 Absatz 2 Nummer 16, 22 und 23 gilt, wobei bei Narbenschäden eine umbruchlose Neueinsaat zulässig ist;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  nur Arten der jeweils potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Gehölzarten in gesellschaftstypischer Zusammensetzung unter Ausschluss eingebürgerter Arten zu verwenden sind,
    2.  auf den Flächen der in § 3 Absatz 2 Nummer 1 und 2 genannten Waldgesellschaften eine Nutzung nur einzelstamm- bis truppweise erfolgt und hydromorphe Böden nur bei Frost sowie Böden mit einem hohen Anteil an feinkörnigem Substrat nur bei Frost oder in Trockenperioden auf dauerhaft festgelegten Rückegassen befahren und zur Förderung des Lebensraumtyps Labkraut-Eichen-Hainbuchenwald kleinflächige Nutzungen bis 0,5 Hektar zur Verjüngung der Baumart Eiche zugelassen werden,
    3.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    4.  mindestens fünf Stämme je Hektar mit einem Durchmesser von mehr als 40 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum natürlichen Absterben und Zerfall aus der Nutzung genommen sein müssen,
    5.  je Hektar mindestens fünf Stück lebensraumtypische, abgestorbene, stehende Bäume (Totholz) mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß nicht gefällt werden; liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimeter am stärksten Ende) verbleibt als ganzer Baum im Bestand,
    6.  § 4 Absatz 2 Nummer 22 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende Bewirtschaftung der Teiche oberhalb der Batzlower Mühle im Sinne der guten fachlichen Praxis in Brandenburg auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters weitgehend ausgeschlossen ist;
4.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Wasservogeljagd erst ab dem 1.Oktober eines Jahres zulässig ist,
        
        bb)
        
        die Fallenjagd nur mit Lebendfallen erfolgt und bis zu einem Abstand von 100 Metern zu Gewässerufern verboten ist; Ausnahmen bedürfen einer Genehmigung der unteren Naturschutzbehörde,
        
        cc)
        
        keine Baujagd in einem Abstand von 100 Metern zum Gewässerufer des Batzlower Mühlenfließes oder der Büchnitz vorgenommen wird,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.
        Transportable und mobile Ansitzeinrichtungen sind der unteren Naturschutzbehörde vor der Errichtung anzuzeigen.
        Die untere Naturschutzbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht; die Entscheidung hierzu soll unverzüglich erfolgen,
    3.  die Anlage von Kirrungen außerhalb der in § 3 Absatz 2 Nummer 1 und 2 aufgeführten Biotope.
    
    Im Übrigen bleiben Ablenkfütterungen sowie die Neuanlage von Ansaatwildwiesen und von Wildäckern unzulässig, wobei § 41 des Jagdgesetzes für das Land Brandenburg unberührt bleibt;
    
5.  die Genehmigung von Maßnahmen zur Vergrämung und Tötung von Kormoranen im Bereich der fischereiwirtschaftlich genutzten Teiche durch die zuständige Naturschutzbehörde, sofern hierfür die erforderliche artenschutzrechtliche Ausnahmegenehmigung oder Befreiung vorliegt.
    Die Genehmigung kann mit Auflagen versehen werden.
    Sie ist zu erteilen, wenn der Schutzzweck von der Maßnahme nicht wesentlich beeinträchtigt wird;
6.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiliche Flächennutzung zur Durchführung von Hegemaßnahmen;
7.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter Nummer 9 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
8.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
9.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen in der bisherigen Art und im bisherigen Umfang.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
10.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch jeweils nach dem 31.
    Juli eines jeden Jahres;
11.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
12.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz im Einvernehmen sowie Maßnahmen der Munitionsräumung im Benehmen mit der unteren Naturschutzbehörde;
13.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der zuständigen Naturschutzbehörde zugelassen oder angeordnet worden sind;
14.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Tourismus im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hin​weiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl.
    S. 1734), die durch die Bekanntmachung vom 1.
    Oktober 2013 (ABl.
    S. 2811) geändert worden ist, an Straßen und Wegen freigestellt;
15.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungsbefugnissen, die sich aus anderen fachrechtlichen Vorgaben ergeben, unberührt.

#### § 6 Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  die Oberläufe der Büchnitz und des Batzlower Mühlenfließes sollen renaturiert und die ökologische Durchgängigkeit wiederhergestellt werden;
2.  Trocken- und Halbtrockenrasen sollen ab Mai als Weide mit Schafen und Ziegen genutzt werden; die Nutzung erfolgt möglichst in Form der Hutung oder der kurzzeitigen Umtriebsweide.
    Die konkrete Beweidung soll entsprechend eines mit der zuständigen Naturschutzbehörde abgestimmten und regelmäßig fortzuschreibenden Weideplanes durchgeführt werden;
3.  Kiefernwälder der sarmatischen Steppe sollen zur Erhaltung und Entwicklung der Bodenflora entbuscht und vorzugsweise mit Schafen und Ziegen beweidet werden.
    Für den Lebensraum typische Jungbäume sollen zuvor vor Verbiss geschützt werden;
4.  nicht mehr genutzte Grünlandflächen mit Restvorkommen typischer Arten sollen unter Beachtung der Maßgaben von § 5 Absatz 1 Nummer 1 wieder einer regelmäßigen extensiven Nutzung zugeführt werden; dabei können auch lokale Entbuschungsmaßnahmen notwendig werden;
5.  in den Weichholzauenwäldern entlang der Büchnitz und des Batzlower Mühlenfließes soll möglichst keine Holzentnahme erfolgen;
6.  nicht heimische Gehölzarten, wie zum Beispiel Robinie (Robinia pseudoacacia), Amerikanische Esche (Fraxinus pennsylvanica), Eschenahorn (Acer negundo), Hybrid- und Balsam-Pappel (Populus x canadensis, P.
    balsamifera), Späte Traubenkirsche (Prunus serotina), Gemeine Fichte (Picea abies) und Douglasie (Pseudotsuga menziesii) sollen bei der forstwirtschaftlichen Flächennutzung aus dem Bestand entnommen werden.
    Die Verjüngung von Waldflächen soll nach Möglichkeit durch Naturverjüngung der Arten der potenziellen natürlichen Waldgesellschaften erfolgen; dazu ist der Schalenwildbestand entsprechend zu regulieren; auflaufender Jungwuchs von nicht gebietsheimischen Arten soll entfernt werden;
7.  der Teich im Oberlauf des Batzlower Mühlenfließes westlich der Landesstraße 341 bei Möglin soll extensiv bewirtschaftet werden;
8.  es sollen geeignete Einrichtungen zur Besucherlenkung und -information geschaffen werden.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichenSchutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weitergehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

§ 5 Absatz 1 Nummer 1 tritt am 1.
Januar 2017 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 28.
November 2016

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Batzlower Mühlenfließ-Büchnitztal“](/br2/sixcms/media.php/68/GVBl_II_68_2016-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Batzlower Mühlenfließ-Büchnitztal“") 1.1 MB

2

[Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten](/br2/sixcms/media.php/68/GVBl_II_68_2016-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Topografische Karten, Liegenschaftskarten") 624.3 KB

3

[Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Batzlower Mühlenfließ-Büchnitztal“](/br2/sixcms/media.php/68/GVBl_II_68_2016-Anlage-3.pdf "Anlage 3 (zu § 2 Absatz 2) - Flurstücksliste zur Verordnung über das Naturschutzgebiet „Batzlower Mühlenfließ-Büchnitztal“") 602.5 KB