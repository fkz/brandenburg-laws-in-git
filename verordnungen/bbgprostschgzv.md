## Brandenburgische Verordnung über die Zuständigkeiten nach dem Prostituiertenschutzgesetz (BbgProstSchGZV)

Auf Grund des § 12 Absatz 1 Satz 2 in Verbindung mit § 6 Absatz 2 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), von denen durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28 S. 2) § 12 eingefügt und § 6 geändert worden ist, sowie des § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl. I S. 602) verordnet die Landesregierung:

#### § 1 Ausübung der Prostitution, gesundheitliche Beratung, Betrieb eines Prostitutionsgewerbes

(1) Die Landkreise und kreisfreien Städte sind zuständig für die Wahrnehmung der Aufgaben der zuständigen Behörde nach Abschnitt 2 des Prostituiertenschutzgesetzes vom 21.
Oktober 2016 (BGBl. I S. 2372) einschließlich der diesbezüglichen Aufgaben nach § 34 Absatz 6 und 8 und § 35 Absatz 1 Nummer 1 bis 3 und Absatz 2 bis 4 des Prostituiertenschutzgesetzes.
Die Landkreise und kreisfreien Städte nehmen die ihnen nach Satz 1 obliegenden Aufgaben als pflichtige Selbstverwaltungsaufgabe wahr.

(2) Die Ämter, amtsfreien Gemeinden und kreisfreien Städte sind zuständig für die Wahrnehmung der Aufgaben der zuständigen Behörde nach den Abschnitten 3 bis 5 einschließlich der diesbezüglichen Aufgaben nach § 34 Absatz 8 und § 35 Absatz 1 Nummer 4 bis 10 und Absatz 2 bis 4 des Prostituiertenschutzgesetzes und zur Überwachung der Einhaltung der in § 32 des Prostituiertenschutzgesetzes geregelten Pflichten.
Die Ämter, amtsfreien Gemeinden und kreisfreien Städte nehmen die ihnen nach Satz 1 obliegenden Aufgaben als pflichtige Selbstverwaltungsaufgabe wahr.

#### § 2 Ordnungswidrigkeiten

Die sachliche Zuständigkeit im Sinne des § 36 Absatz 1 Nummer 2 Buchstabe a des Gesetzes über Ordnungswidrigkeiten wird gemäß § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten

1.  bezüglich der Tatbestände des § 33 Absatz 1 Nummer 1 und 2 des Prostituiertenschutzgesetzes den nach § 1 Absatz 1 zuständigen Behörden und
2.  bezüglich der Tatbestände des § 33 Absatz 1 Nummer 3 und Absatz 2 des Prostituiertenschutzgesetzes den nach § 1 Absatz 2 zuständigen Behörden

übertragen.

#### § 3 Mehrbelastungsausgleich

(1) Das Land gewährt jedem Landkreis und jeder kreisfreien Stadt eine einmalige Erstattung in Höhe von 6 926,40 Euro, um den Sonderaufwand auszugleichen, der für die Schaffung der Voraussetzungen zur Durchführung des Anmeldeverfahrens nach den §§ 3 bis 9 des Prostituiertenschutzgesetzes erforderlich ist.
Die Sonderaufwandserstattung erfolgt durch das Landesamt für Soziales und Versorgung des Landes Brandenburg auf Antrag in Textform.

(2) Die Landkreise und kreisfreien Städte erhalten einen Mehrbelastungsausgleich für die fallbezogenen Kosten der Aufgabenwahrnehmung nach § 1 Absatz 1 für die nachfolgend genannten Amtshandlungen in folgender Höhe:

1.  Bearbeitung der Anmeldung (§ 5 Absatz 1 des Prostituiertenschutzgesetzes) einschließlich
    
    1.  des Informations- und Beratungsgesprächs (§§ 7 bis 9 des Prostituiertenschutzgesetzes),
    2.  der Prüfung der erforderlichen Angaben und Nachweise (§ 4 Absatz 1 bis 3 des Prostituiertenschutzgesetzes) sowie eventueller Versagungsgründe (§ 5 Absatz 2 des Prostituiertenschutzgesetzes) und Schutzmaßnahmen (§ 9 Absatz 2 des Prostituiertenschutzgesetzes),
    3.  der Ausstellung der Anmeldebescheinigung (§ 5 Absatz 1 des Prostituiertenschutzgesetzes),
    4.  der Ausstellung der Aliasbescheinigung (§ 5 Absatz 6 des Prostituiertenschutzgesetzes,
    
    je Fall 33,79 Euro.
    
    Wurde beim Informations- und Beratungsgespräch von der zuständigen Behörde eine dritte Person kostenpflichtig zur Sprachmittlung gemäß § 8 Absatz 2 Satz 3 des Prostituiertenschutzgesetzes hinzugezogen, so beträgt die Pauschale für die Bearbeitung der Anmeldung
    
    je Fall 80,45 Euro.
    
2.  Bearbeitung von Änderungsanzeigen einschließlich der Ausstellung der geänderten Anmelde- und Aliasbescheinigung (§ 4 Absatz 5 des Prostituiertenschutzgesetzes)
    
    je Fall 10,98 Euro.
    
3.  Bearbeitung der Verlängerung der Anmeldebescheinigung (§ 5 Absatz 5 des Prostituiertenschutzgesetzes)
    
    je Fall 16,89 Euro.
    
    Wurde beim Informations- und Beratungsgespräch von der zuständigen Behörde eine dritte Person kostenpflichtig zur Sprachmittlung gemäß § 5 Absatz 5 Satz 4 in Verbindung mit § 8 Absatz 2 Satz 3 des Prostituiertenschutzgesetzes hinzugezogen, so beträgt die Pauschale für die Bearbeitung der Verlängerung der Anmeldebescheinigung
    
    je Fall 40,23 Euro.
    
4.  Bearbeitung der Anordnungen nach § 11 des Prostituiertenschutzgesetzes
    
    je Fall 8,45 Euro.
    
5.  Durchführung der gesundheitlichen Beratung nach § 10 des Prostituiertenschutzgesetzes
    
    je Fall 54,40 Euro.
    
    Wurde bei der Durchführung der gesundheitlichen Beratung von der zuständigen Behörde eine dritte Person kostenpflichtig zur Sprachmittlung gemäß § 10 Absatz 2 Satz 3 des Prostituiertenschutzgesetzes hinzugezogen, so beträgt die Pauschale für die Durchführung der gesundheitlichen Beratung
    
    je Fall 124,40 Euro.
    

Die Beträge werden erstmals im vierten Quartal 2019 und danach alle drei Jahre überprüft und bei Bedarf angepasst.
Die angepassten Beträge sind im Amtsblatt für Brandenburg bekannt zu geben.

(3) Der Mehrbelastungsausgleich für die fallbezogenen Kosten nach Absatz 2 ist jährlich in Textform unter Angabe der Art und Anzahl der vorgenommenen Amtshandlungen beim Landesamt für Soziales und Versorgung des Landes Brandenburg geltend zu machen.
In den Fällen des Absatzes 2 Satz 1 Nummer 1, 3 und 5 ist zusätzlich anzugeben, ob zu den vorgenommenen Amtshandlungen eine dritte Person kostenpflichtig zur Sprachmittlung hinzugezogen wurde.
Für Amtshandlungen, die bis zum 31. März 2018 vorgenommen werden, sind Ausgleichszahlungen im zweiten Quartal 2018 geltend zu machen.
Für Amtshandlungen, die ab dem 1.
April 2018 vorgenommen werden, sind Ausgleichszahlungen im ersten Quartal des jeweils folgenden Kalenderjahres geltend zu machen.

(4) Die Ämter, amtsfreien Gemeinden und kreisfreien Städte erhalten für die Kosten der Aufgabenwahrnehmung nach § 1 Absatz 2 in den ersten drei Monaten nach Inkrafttreten nach § 4 Satz 1 einen fallbezogenen Mehrbelastungsausgleich, sofern eine Gebührenerhebung mangels einer Gebührensatzung als Rechtsgrundlage nicht möglich ist.
Als Mehrbelastungsausgleich nach Satz 1 werden für die in der Anlage zu dieser Verordnung genannten Amtshandlungen Fallpauschalen in der dort genannten Höhe gewährt.
Die Fallpauschalen sind im ersten Quartal 2019 beim Landesamt für Soziales und Versorgung des Landes Brandenburg in Textform unter Angabe der tatsächlich vorgenommenen Amtshandlungen entsprechend der Anlage zu dieser Verordnung und des Datums der Vornahme geltend zu machen.
Dabei ist zu erklären, dass zum Zeitpunkt der Vornahme der Amtshandlung eine Gebührensatzung als Rechtsgrundlage zur Gebührenerhebung nicht bestand.

(5) Die Landkreise und kreisfreien Städte sind verpflichtet, dem Landesamt für Soziales und Versorgung des Landes Brandenburg im Einzelfall auf Verlangen die tatsächliche Vornahme der in Absatz 2 genannten Amtshandlungen und den tatsächlichen Vollzugsaufwand nachzuweisen.
Zur Feststellung der Ordnungsmäßigkeit der Kostenerstattungsansprüche können vom Landesamt für Soziales und Versorgung des Landes Brandenburg begründete Unterlagen vor Ort eingesehen oder angefordert werden.

(6) Das Landesamt für Soziales und Versorgung des Landes Brandenburg erstattet den Landkreisen und kreisfreien Städten auf Antrag die tatsächlich notwendigen Kosten der Aufgabenwahrnehmung nach § 1 Absatz 1 und § 2 Nummer 1, soweit diese den Mehrbelastungsausgleich nach Absatz 2 und die bei der Aufgabenwahrnehmung erzielten sonstigen eigenen Einnahmen übersteigen.
Satz 1 gilt nicht, soweit die Landkreise und kreisfreien Städte im vorangegangenen Abrechnungszeitraum insgesamt Überschüsse erzielt haben, die den Erstattungsbetrag gemäß Satz 1 übersteigen.
Für das Verfahren der Kostenerstattung nach Satz 1 gilt Absatz 3 entsprechend mit der Maßgabe, dass für alle Amtshandlungen der Termin gemäß Absatz 3 Satz 4 gilt.

(7) Das Landesamt für Soziales und Versorgung des Landes Brandenburg erstattet den Ämtern, amtsfreien Gemeinden und kreisfreien Städten auf Antrag die tatsächlich notwendigen Kosten der Aufgabenwahrnehmung nach § 1 Absatz 2 und § 2 Nummer 2, soweit diese nicht durch Gebühren ausgeglichen werden können und durch den Mehrbelastungsausgleich nach Absatz 4 nicht ausgeglichen werden.
Satz 1 gilt nicht, soweit die Ämter, amtsfreien Gemeinden und kreisfreien Städte im vorangegangenen Abrechnungszeitraum insgesamt Überschüsse erzielt haben, die den Erstattungsbetrag gemäß Satz 1 übersteigen.
Für das Verfahren der Kostenerstattung nach Satz 1 gilt Absatz 3 Satz 1, 3 und 4 entsprechend mit der Maßgabe, dass für alle Amtshandlungen der Termin gemäß Absatz 3 Satz 4 gilt.

#### § 4 Inkrafttreten

Diese Verordnung tritt vorbehaltlich des Satzes 2 am Tag nach der Verkündung in Kraft.
§ 1 Absatz 2 und § 2 Nummer 2 treten am 1.
März 2018 in Kraft.

Potsdam, den 8.
Februar 2018

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke

Die Ministerin für Arbeit, Soziales,  
Gesundheit, Frauen und Familie

Diana Golze

* * *

### Anlagen

1

[Anlage (zu § 3 Absatz 4) - Verzeichnis der Fallpauschalen](/br2/sixcms/media.php/68/GVBl_II_13_2018-Anlage.pdf "Anlage (zu § 3 Absatz 4) - Verzeichnis der Fallpauschalen") 151.2 KB