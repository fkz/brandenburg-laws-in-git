## Verordnung über die Zuständigkeiten nach dem Geldwäschegesetz (Geldwäschezuständigkeitsverordnung - GeldwZV)

Auf Grund des §16 Absatz 2 Nummer 9 des Geldwäschegesetzes vom 13.
August 2008 (BGBl.
I S. 1690) in Verbindung mit § 9 Absatz 2 und 4 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S.
186) und § 2 Nummer 9 der Verordnung über wirtschaftsrechtliche Zuständigkeiten und Zuständigkeiten zur Zulassung von Rohrfernleitungen vom 7.
September 2009 (GVBl.
II S.
604), der durch Artikel 1 Nummer 2 der Verordnung vom 7.
Mai 2012 (GVBl.
II Nr. 33) angefügt worden ist, verordnet der Minister für Wirtschaft und Europaangelegenheiten:

### § 1

Zuständige Stelle nach § 50 Nummer 9 des Geldwäschegesetzes ist für die Verpflichteten nach § 2 Nummer 6 (Finanzunternehmen), Nummer 8 (Versicherungsvermittler), Nummer 13 (Dienstleister für Gesellschaften und für Treuhandvermögen oder Treuhänder), Nummer 14 (Immobilienmakler) und Nummer 16 (Güterhändler) des Geldwäschegesetzes das für Wirtschaft zuständige Ministerium.

### § 2

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 13.
August 2012

Der Minister für Wirtschaft  
und Europaangelegenheiten

Ralf Christoffers