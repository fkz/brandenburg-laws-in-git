## Verordnung über die Bildungsgänge der Fachoberschule und den Erwerb der Fachhochschulreife in beruflichen Bildungsgängen (Fachoberschul- und Fachhochschulreifeverordnung - FOSFHRV)

Auf Grund des § 15 Absatz 4, des § 25 Absatz 6, des § 26 Absatz 4 und des § 27 Absatz 5 in Verbindung mit § 13 Absatz 3, § 56, § 57 Absatz 4, § 58 Absatz 3, § 59 Absatz 9, § 60 Absatz 4, § 61 Absatz 3 und § 64 Absatz 6 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2.
August 2002 (GVBl.
I S. 78), von denen § 13 Absatz 3, § 25 Absatz 6 und § 26 Absatz 4 durch Artikel 1 des Gesetzes vom 8.
Januar 2007 (GVBl.
I S. 2) und § 56 zuletzt durch Artikel 2 des Gesetzes vom 14.
März 2014 (GVBl.
I Nr. 14) geändert und § 57 Absatz 4 durch Artikel 1 des Gesetzes vom 10.
Juli 2017 (GVBl.
II Nr.16) neu gefasst worden sind, verordnet die Ministerin für Bildung, Jugend und Sport:

**Inhaltsübersicht**

Kapitel 1  
Ausbildung
----------------------

### Abschnitt 1  
Allgemeine Bestimmungen

[§ 1 Ziel der Bildungsgänge](#1)

[§ 2 Organisationsformen und Dauer](#2)

[§ 3 Gliederung](#3)

### Abschnitt 2  
Aufnahme

[§ 4 Aufnahmevoraussetzungen](#4)

[§ 5 Aufnahmeverfahren](#5)

[§ 6 Aufnahme bei Übernachfrage](#6)

[§ 7 Nichtinanspruchnahme von Plätzen](#7)

### Abschnitt 3  
Bestimmungen für den Unterricht

[§ 8 Unterrichtsorganisation](#8)

[§ 9 Stundentafeln und Fächer](#9)

[§ 10 Leistungsbewertung](#10)

[§ 11 Versetzen, Aufrücken](#11)

[§ 12 Nachprüfung](#12)

[§ 13 Wiederholung](#13)

[§ 14 Zeugnisse](#14)

### Abschnitt 4  
Fachpraktische Ausbildung im zweijährigen Bildungsgang

[§ 15 Ziel und Dauer](#15)

[§ 16 Fachpraktische Ausbildungsstätten](#16)

[§ 17 Rahmenbedingungen der fachpraktischen Ausbildung](#17)

[§ 18 Durchführung der fachpraktischen Ausbildung](#18)

[§ 19 Abschluss der fachpraktischen Ausbildung, Wiederholung](#19)

Kapitel 2  
Fachhochschulreifeprüfung
-------------------------------------

### Abschnitt 1  
Allgemeine Prüfungsbestimmungen

[§ 20 Grundsätze der Fachhochschulreifeprüfung](#20)

[§ 21 Prüfungsfächer](#21)

[§ 22 Nachteilsausgleich](#22)

[§ 23 Zuhörerinnen und Zuhörer](#23)

### Abschnitt 2  
Prüfungsorgane

[§ 24 Prüfungsausschuss](#24)

[§ 25 Fachausschüsse](#25)

[§ 26 Teilnahmepflicht](#26)

[§ 27 Beschlussfassung](#27)

### Abschnitt 3  
Zulassung und Teilnahme

[§ 28 Zulassung, Vornoten, Rücktritt](#28)

[§ 29 Erkrankung, Versäumnis, Verweigerung](#29)

[§ 30 Täuschungen und Unregelmäßigkeiten](#30)

### Abschnitt 4  
Schriftliche Prüfung

[§ 31 Durchführung der schriftlichen Prüfungen](#31)

[§ 32 Bewertung der schriftlichen Prüfungsarbeiten](#32)

### Abschnitt 5  
Mündliche Prüfung

[§ 33 Vorkonferenz, Zulassung zur mündlichen Prüfung](#33)

[§ 34 Antrag des Prüflings auf mündliche Prüfung](#34)

[§ 35 Durchführung der mündlichen Prüfung](#35)

[§ 36 Bewertung der mündlichen Prüfung](#36)

### Abschnitt 6  
Abschluss der Prüfung, Prüfungswiederholung

[§ 37 Abschlusskonferenz, Endnoten](#37)

[§ 38 Prüfungsergebnis](#38)

[§ 39 Zeugnis der Fachhochschulreife](#39)

[§ 40 Wiederholung bei Nichtbestehen](#40)

Kapitel 3  
Besondere Bestimmungen
----------------------------------

### Abschnitt 1  
Allgemeines

[§ 41 Erwerb der Fachhochschulreife](#41)

### Abschnitt 2  
Ergänzende Bestimmungen für den doppelqualifizierenden Bildungsgang

[§ 42 Wiederholung bei Nichtbestehen der Fachhochschulreifeprüfung](#42)

### Abschnitt 3  
Ergänzende Bestimmungen für das Zusatzangebot zum Erwerb der Fachhochschulreife für Schülerinnen und Schüler in einer Berufsausbildung

[§ 43 Voraussetzungen](#43)

[§ 44 Aufnahme](#44)

[§ 45 Unterrichtsorganisation](#45)

[§ 46 Bescheinigung, Zeugnisse](#46)

[§ 47 Verlassen des Zusatzangebots](#47)

[§ 48 Prüfung zum Erwerb der Fachhochschulreife](#48)

### Abschnitt 4  
Nichtschülerprüfung zum Erwerb der Fachhochschulreife

[§ 49 Zulassungsvoraussetzungen](#49)

[§ 50 Antragstellung, Zulassung](#50)

[§ 51 Prüfungsbestimmungen](#51)

### Abschnitt 5  
Erwerb der Fachhochschulreife an Gymnasien, Gesamtschulen, beruflichen Gymnasien und im Zweiten Bildungsweg

[§ 52 Antrag](#52)

[§ 53 Erwerb der beruflichen Bildung](#53)

Kapitel 4  
Verwaltungsweg, Ausnahme-, Übergangs- und Schlussbestimmungen
-------------------------------------------------------------------------

[§ 54 Widerspruch und Akteneinsicht](#54)

[§ 55 Übergangsbestimmungen](#55)

[§ 56 Inkrafttreten, Außerkrafttreten](#56)

[Anlage Stundentafel](#57)

Kapitel 1  
Ausbildung
----------------------

### Abschnitt 1  
Allgemeine Bestimmungen

#### § 1 Ziel der Bildungsgänge

(1) Die Fachoberschule vermittelt berufsbezogene fachtheoretische Kenntnisse und Fähigkeiten sowie fachpraktische Fertigkeiten und erweitert und vertieft die allgemeine Bildung.
Sie führt in den verschiedenen Bildungsgängen oder über das Zusatzangebot zum Erwerb der Fachhochschulreife, die zum Studium an Fachhochschulen berechtigt.

(2) Die Bestimmungen für den Erwerb der Fachhochschulreife in Bildungsgängen der Fachschule bleiben unberührt.

#### § 2 Organisationsformen und Dauer

(1) Bildungsgänge sind

1.  der einjährige Bildungsgang der Fachoberschule für Bewerberinnen und Bewerber mit mittlerem Schulabschluss und erfolgreich abgeschlossener Berufsausbildung nach dem Berufsbildungsgesetz oder der Handwerksordnung oder nach Landesrecht oder entsprechender beruflicher Vorbildung (einjähriger Bildungsgang),
2.  der zweijährige Bildungsgang der Fachoberschule mit integrierter fachpraktischer Ausbildung für Bewerberinnen und Bewerber mit mittlerem Schulabschluss (zweijähriger Bildungsgang) und
3.  der doppelqualifizierende Bildungsgang für Bewerberinnen und Bewerber mit mittlerem Schulabschluss, die sich gleichzeitig in einer Berufsausbildung nach dem Berufsbildungsgesetz oder der Handwerksordnung mit einer Regelausbildungsdauer von mindestens drei Jahren befinden (doppelqualifizierender Bildungsgang).

(2) Der einjährige und zweijährige Bildungsgang werden in Vollzeitform, der doppelqualifizierende Bildungsgang wird in Teilzeitform angeboten.

(3) Für den doppelqualifizierenden Bildungsgang findet die Berufsschulverordnung in der jeweils gültigen Fassung insoweit Anwendung, als dass der Bildungsgang zur Vermittlung des schulischen Teils einer Berufsausbildung gemäß Berufsbildungsgesetz oder Handwerksordnung berührt ist.
Das gilt insbesondere für die Zusammenarbeit mit der Ausbildungsstätte, den Unterricht und die Noten im berufsbezogenen Bereich.

#### § 3 Gliederung

(1) Die Bildungsgänge der Fachoberschule sind nach folgenden Fachrichtungen gegliedert:

1.  Wirtschaft und Verwaltung,
2.  Technik,
3.  Gesundheit und Soziales,
4.  Gestaltung,
5.  Ernährung und Hauswirtschaft sowie
6.  Agrarwirtschaft, Bio- und Umwelttechnologie.

Innerhalb der Fachrichtungen können Schwerpunkte gebildet werden.

(2) Der einjährige Bildungsgang kann in allen in Absatz 1 genannten Fachrichtungen eingerichtet werden.
Der zweijährige Bildungsgang kann nur in den Fachrichtungen gemäß Absatz 1 Nummer 1 bis 3 eingerichtet werden.

(3) Der doppelqualifizierende Bildungsgang wird nach Ausbildungsberufen gegliedert.

(4) Im Zusatzangebot gemäß den §§ 43 bis 48 wird nicht nach Fachrichtungen oder Ausbildungsberufen gegliedert, sondern nach Bedarf angeboten.

### Abschnitt 2  
Aufnahme

#### § 4 Aufnahmevoraussetzungen

(1) In den einjährigen Bildungsgang kann aufgenommen werden, wer

1.  den mittleren Schulabschluss oder einen gleichwertigen Abschluss nachweist oder die Berechtigung zum Besuch der gymnasialen Oberstufe erworben hat und
2.  eine auf die Fachrichtung bezogene einschlägige Berufsausbildung nach dem Berufsbildungsgesetz oder der Handwerksordnung oder nach Landesrecht erfolgreich abgeschlossen hat oder
3.  eine gleichwertige, vom für Schule zuständigen Ministerium anerkannte berufliche Vorbildung oder eine einschlägige mindestens dreijährige Berufstätigkeit nachweist.

(2) In den zweijährigen Bildungsgang kann aufgenommen werden, wer

1.  den mittleren Schulabschluss oder einen gleichwertigen Abschluss nachweist oder die Berechtigung zum Besuch der gymnasialen Oberstufe erworben hat und
2.  eine schriftliche Zusage der Praxisstelle für die Durchführung der fachpraktischen Ausbildung vorlegt.

(3) In den doppelqualifizierenden Bildungsgang kann aufgenommen werden, wer

1.  mindestens den mittleren Schulabschluss oder einen gleichwertigen Abschluss nachweist und
2.  einen Ausbildungsvertrag in einem anerkannten Ausbildungsberuf nach dem Berufsbildungsgesetz oder der Handwerksordnung mit einer Regelausbildungsdauer von mindestens drei Jahren abgeschlossen hat, der durch eine ergänzende Zusatzvereinbarung die Doppelqualifizierung vorsieht.

(4) Das für Schule zuständige Ministerium trifft Festlegungen über die Zuordnung von Berufen und Berufstätigkeiten gemäß Absatz 1 Nummer 2 und 3 zu den Fachrichtungen der Fachoberschule.
Auf dieser Grundlage entscheidet die Schulleiterin oder der Schulleiter, in welcher Fachrichtung der einjährige Bildungsgang besucht wird.

(5) In die Bildungsgänge gemäß Absatz 1 bis 3 kann auch aufgenommen werden, wer

1.  im Ausland eine vergleichbare Qualifikation erworben hat und
2.  aufgrund der bisherigen im Ausland absolvierten Schullaufbahn einen erfolgreichen Abschluss des Bildungsgangs erwarten lässt und
3.  hinreichende deutsche Sprachkenntnisse mindestens auf dem Niveau B1 gemäß dem Gemeinsamen europäischen Referenzrahmen für Sprachen nachweisen kann, soweit Deutsch nicht die Muttersprache ist und
4.  hinreichende englische Sprachkenntnisse mindestens auf dem Niveau B1 gemäß dem Gemeinsamen europäischen Referenzrahmen für Sprachen nachweisen kann.

Die Bestimmungen gemäß Absatz 3 Nummer 2 bleiben unberührt.

#### § 5 Aufnahmeverfahren

(1) Die Aufnahme ist bei dem Oberstufenzentrum, bei dem der gewählte Bildungsgang eingerichtet ist, unter Angabe des Bildungsganges und der Fachrichtung schriftlich zu beantragen.
Dem Aufnahmeantrag sind die gemäß § 4 geforderten Nachweise beizufügen.

(2) Die Antragsfrist wird vom für Schule zuständigen Ministerium festgelegt.
Nach Ablauf der Frist erhält die oder der Antragstellende von der aufnehmenden Schule die Bestätigung der fristgerechten und ordnungsgemäßen Anmeldung.

(3) Liegt das Zeugnis, mit dem der Erwerb des mittleren Schulabschlusses nachgewiesen wird, zum Zeitpunkt der Antragstellung noch nicht vor, wird zunächst das letzte Schulhalbjahreszeugnis vorgelegt.
Das Oberstufenzentrum legt den Zeitpunkt fest, bis zu dem das Abschlusszeugnis eingereicht werden muss.
Maßgeblich für die Aufnahme ist das Abschlusszeugnis.

(4) Die Aufnahme erfolgt jeweils zu Beginn eines Schuljahres.
Über die Aufnahme entscheidet die Schulleiterin oder der Schulleiter.

#### § 6 Aufnahme bei Übernachfrage

(1) Übersteigt die Zahl der Anmeldungen die Aufnahmekapazität, wird ein Auswahlverfahren durchgeführt.
Schulpflichtige Schülerinnen und Schüler sind vorrangig aufzunehmen.

(2) Die Auswahl der verfügbaren Plätze erfolgt nach Berücksichtigung

1.  besonderer Härtefälle und
2.  des Vorrangs der Eignung.

(3) Im Umfang von bis zu zehn Prozent der Gesamtplätze werden Schülerinnen und Schüler vorrangig berücksichtigt, wenn besondere Härtefälle vorliegen.
Dies trifft insbesondere zu, wenn

1.  aufgrund einer Behinderung lediglich eine bestimmte Schule erreichbar ist oder notwendige bauliche Ausstattungen oder räumliche Voraussetzungen nur an der gewählten Schule vorhanden sind,
2.  durch besondere familiäre oder soziale Situationen Belastungen entstehen, die das üblicherweise Vorkommende bei weitem überschreiten,
3.  aufgrund der Verkehrsverhältnisse eine ansonsten in Betracht kommende Schule nur unter unzumutbaren Schwierigkeiten erreicht werden kann,
4.  im vergangenen Schuljahr eine Niederkunft oder eine mindestens einjährige Betreuung eines Kindes oder mindestens einjährige Betreuung einer pflegebedürftigen Person im Sinne des Elften Buches Sozialgesetzbuch erfolgte oder
5.  eine Behinderung vorliegt, bei der die ausgewählte Ausbildung die Rehabilitationschance wesentlich verbessern würde.

(4) Übersteigt die Zahl der Bewerberinnen und Bewerber, die berechtigt einen Härtefall geltend machen, die Quote gemäß Absatz 3, so wird die Rangfolge nach der Eignung ermittelt.
Für die Festlegung der Eignung gilt Absatz 5 Satz 2 entsprechend.

(5) Die verbleibenden Plätze werden nach Eignung vergeben.
Bei gleicher Eignung werden die Plätze an diejenigen vergeben, die in einem vorhergehenden Schuljahr wegen fehlender Kapazitäten nicht aufgenommen werden konnten.
Die Dauer der Wartezeit entscheidet über die Rangfolge.
Nach einer Wartezeit von drei Jahren erfolgt die Aufnahme vor Eignung.

(6) Sind auch nach Anwendung des Absatzes 5 Bewerberinnen und Bewerber gleich geeignet, so werden die noch vorhandenen Plätze durch das Los vergeben.

#### § 7 Nichtinanspruchnahme von Plätzen

(1) Plätze, die zum Schuljahresbeginn von Bewerberinnen und Bewerbern, die nach dem Auswahlverfahren eine Aufnahmebestätigung erhalten haben, nicht in Anspruch genommen worden sind, werden nach der Rangfolge einer zu bildenden Nachrückerliste vergeben.

(2) Für Bewerberinnen und Bewerber, die vor Schuljahresbeginn dem Oberstufenzentrum nicht mitgeteilt haben, dass sie ihren Platz nicht in Anspruch nehmen, wird bei der nächsten Bewerbung keine Wartezeit berücksichtigt.

### Abschnitt 3  
Bestimmungen für den Unterricht

#### § 8 Unterrichtsorganisation

(1) Der Unterricht wird im Klassenverband oder in Kursen erteilt.
Er umfasst allgemeinbildende und fachrichtungs- oder berufsbezogene Fächer.
Fachübergreifender Unterricht und Projektarbeit sind Teil des Unterrichts.

(2) In den Bildungsgängen der Fachoberschule ist die gewählte Fachrichtung entscheidend für die Klassenbildung.
Davon kann abgewichen werden, wenn eine Klassenbildung wegen geringer Schülerzahlen nicht möglich ist.
In diesem Fall werden für die fachrichtungsbezogenen Fächer Kurse eingerichtet.

(3) Für den einjährigen Bildungsgang werden eigene Klassen gebildet.
Soweit die Maßgaben für die Klassenbildung gemäß VV-Unterrichtsorganisation im einjährigen Bildungsgang nicht erfüllt werden, werden diese Klassen mit dem zweiten Schulbesuchsjahr des zweijährigen Bildungsgangs gemeinsam beschult.

(4) Im doppelqualifizierenden Bildungsgang werden die Klassen in der Regel aus Schülerinnen und Schülern des gleichen Ausbildungsberufes und Ausbildungsjahrganges gebildet.
Schülerinnen und Schüler aus unterschiedlichen Ausbildungsberufen können gemeinsam in einer Klasse unterrichtet werden, wenn der Unterricht im berufsbezogenen Fach in getrennten Kursen organisiert wird.

#### § 9 Stundentafeln und Fächer

Die Unterrichtsfächer und die Anzahl der Unterrichtsstunden für das jeweilige Schuljahr ergeben sich aus den Stundentafeln gemäß der Anlage.
Der Unterricht wird auf der Grundlage der durch das für Schule zuständige Ministerium erlassenen Rahmenlehrpläne oder anderen curricularen Vorgaben erteilt.

#### § 10 Leistungsbewertung

(1) Die Leistungsbewertung erfolgt gemäß § 57 Absatz 2 und 3 des Brandenburgischen Schulgesetzes mit Noten.

(2) Je Schulhalbjahr ist in allen Fächern mit Ausnahme des Faches Sport eine schriftliche Arbeit anzufertigen.
Diese muss in den Fächern der schriftlichen Prüfung im letzten Schulhalbjahr den Bedingungen der Fachhochschulreifeprüfung entsprechen.

(3) Schülerinnen und Schülern mit einer nachgewiesenen physischen oder psychischen Beeinträchtigung oder mit sonderpädagogischem Förderbedarf sind angemessene Erleichterungen zu gewähren, um Nachteile auszugleichen, die sich aus der Art und dem Umfang der jeweiligen Beeinträchtigung oder des sonderpädagogischen Förderbedarfs ergeben.
Als solche Erleichterungen kommen insbesondere eine angemessene Verlängerung der Arbeitszeit sowie die Zulassung besonderer Hilfsmittel in Betracht.
Die Bereitstellung von Hilfsmitteln soll von der Schule unterstützt werden.
Die fachlichen Anforderungen bleiben unberührt.
Die Entscheidung trifft die Schulleitung.

(4) Das Nähere zum Ausgleich von Nachteilen auf Grund einer Lese-Rechtschreib-Schwierigkeit wird in der Lesen-Rechtschreiben-Rechnen Verordnung geregelt.

(5) Das Nähere zu den Grundsätzen der Leistungsbewertung wird durch Verwaltungsvorschriften geregelt.

#### § 11 Versetzen, Aufrücken

(1) Im zweijährigen Bildungsgang erfolgt eine Versetzung in die nächsthöhere Jahrgangsstufe, wenn

1.  in allen Fächern mindestens ausreichende Leistungen erzielt wurden oder
2.  eine mangelhafte Leistung in höchstens einem Fach durch mindestens gute Leistungen in einem anderen Fach oder befriedigende Leistungen in zwei anderen Fächern ausgeglichen werden kann und
3.  die fachpraktische Ausbildung erfolgreich abgeschlossen wurde.

Eine ungenügende Leistung kann nicht ausgeglichen werden.
Die Leistungen im Fach Sport sind nicht zu berücksichtigen.

(2) Die Entscheidung über die Versetzung trifft die Klassenkonferenz.

(3) Von den Bestimmungen gemäß Absatz 1 Nummer 2 kann abgewichen werden, wenn eine Schülerin oder ein Schüler aus wichtigen, nicht selbst zu vertretenden Gründen, insbesondere wegen längerer Abwesenheit oder wegen Krankheit, die Leistungen nicht erbracht hat, aber die begründete Annahme besteht, dass durch den weiteren Schulbesuch das Ziel des Bildungsganges erreicht werden kann.

(4) Im doppelqualifizierenden Bildungsgang rücken die Schülerinnen und Schüler in die nächsthöhere Jahrgangsstufe auf.

#### § 12 Nachprüfung

(1) Wer nicht versetzt wird, kann zu Beginn des folgenden Schuljahres eine Nachprüfung im Fach mit einer mangelhaften Leistung ablegen, um die Versetzungsbedingungen gemäß § 11 zu erfüllen.
In Fächern mit ungenügenden Leistungen kann keine Nachprüfung erfolgen.
Die betroffenen Schülerinnen und Schüler, und bei Nichtvolljährigen deren Eltern, sind unverzüglich nach der Entscheidung über die Nichtversetzung über die Möglichkeit der Nachprüfung zu informieren.

(2) Wer die Nachprüfung ablegen möchte, stellt einen schriftlichen Antrag.
Die Schulleitung legt die Frist für die Antragstellung und den Termin der Nachprüfung fest.

(3) Die Schulleitung bildet für die Nachprüfung einen Prüfungsausschuss, in dem ein Mitglied der Schulleitung den Vorsitz führt.
Dem Prüfungsausschuss gehören die bisher im Fach unterrichtende Lehrkraft als prüfendes Mitglied und eine fachkundige Lehrkraft zur Protokollführung an.

(4) Die Nachprüfung besteht aus einer schriftlichen und einer mündlichen Prüfung.

(5) Wer auf Grund des Ergebnisses der Nachprüfung die Versetzungsbedingungen erfüllt, ist versetzt und erhält ein neues Zeugnis mit dem Datum des Tages der mündlichen Prüfung.
Die Endnote im Fach der Nachprüfung wird ohne Dezimalstelle aus dem arithmetischen Mittel der zweifach gewichteten Jahresnote, der Note der schriftlichen Nachprüfung, der Note der mündlichen Nachprüfung und unter Berücksichtigung der Leistungsentwicklung gebildet.
Der Prüfungsausschuss legt die Endnote der Nachprüfung fest.

(6) Kann ein Prüfling aus wichtigen Gründen an der gesamten Nachprüfung oder an Teilen nicht teilnehmen, so muss dies unverzüglich dem Oberstufenzentrum nachgewiesen werden.
Bei Krankheit muss ein ärztliches Attest vorliegen.
Werden die Nachweise anerkannt, ist ein neuer Termin in den ersten sechs Wochen nach Beginn des Schuljahres festzulegen und dem Prüfling, und bei Nichtvolljährigen dessen Eltern, schriftlich mitzuteilen.
Wird die Nachprüfung oder ein Teil derselben ohne entschuldigten Grund versäumt, gilt die Nachprüfung als nicht bestanden.

#### § 13 Wiederholung

In den Bildungsgängen gemäß § 2 Absatz 1 kann eine Jahrgangsstufe nur einmal wiederholt werden.
Wer zum zweiten Mal das Ziel der Jahrgangsstufe nicht erreicht, ist aus dem Bildungsgang zu entlassen.

#### § 14 Zeugnisse

(1) Im zweijährigen Bildungsgang ist auf dem Zeugnis am Ende des ersten Schuljahres ein Vermerk über den Erfolg der fachpraktischen Ausbildung aufzunehmen.

(2) Wer den gewählten Bildungsgang oder das Zusatzangebot erfolgreich abgeschlossen hat, erhält gemäß § 39 das Zeugnis der Fachhochschulreife.

(3) Ein Abgangszeugnis erhält, wer den Bildungsgang verlässt, ohne die Fachhochschulreife erworben zu haben.

(4) Wer gemäß § 13 oder § 40 entlassen wird, erhält ein Abgangszeugnis mit dem Vermerk, dass eine erneute Aufnahme in diesen Bildungsgang nicht mehr möglich ist.

### Abschnitt 4  
Fachpraktische Ausbildung im zweijährigen Bildungsgang

#### § 15 Ziel und Dauer

(1) Die fachpraktische Ausbildung gibt den Schülerinnen und Schülern Gelegenheit, die Aufgaben und Arbeitsweise der in ihrer Fachrichtung tätigen Betriebe, Behörden oder anderen gleichwertigen Einrichtungen kennenzulernen und die im Unterricht erworbenen Fähigkeiten und Fertigkeiten anzuwenden, zu vertiefen und zu erweitern.

(2) Die fachpraktische Ausbildung umfasst mindestens 800 Zeitstunden und findet während des gesamten ersten Schuljahres unterrichtsbegleitend und grundsätzlich nicht während der Schulferien statt.

(3) Die erfolgreiche Teilnahme an der fachpraktischen Ausbildung ist Voraussetzung für das Erreichen des Ausbildungsziels.

(4) Die Schulleitung bestimmt für jede einzurichtende Klasse die Unterrichtstage und die Zeiten, in denen die fachpraktische Ausbildung erfolgen soll.
Dieser Zeitplan ist den Bewerberinnen und Bewerbern vor Aufnahme in den Bildungsgang rechtzeitig zur Vorlage bei der Praxisstelle auszuhändigen.

#### § 16 Fachpraktische Ausbildungsstätten

(1) Die Bewerberinnen und Bewerber wählen ihre fachpraktische Ausbildungsstätte (Praxisstelle) mit Zustimmung der Schule.
Die Schule informiert über infrage kommende Einrichtungen und berät die Bewerberinnen und Bewerber bei der Auswahl der Praxisstelle.

(2) Die Praxisstelle muss die fachpraktische Ausbildung durchführen und nachweisen, dass sie ausbildungsgeeignet ist.
Sie benennt für die Anleitung und laufende Beratung der Schülerinnen und Schüler in der Praxisstelle eine geeignete Fachkraft als praxisanleitende Person.

(3) In der Fachrichtung Gesundheit und Soziales muss die Praxisstelle gegenüber der Schule nachweisen, dass sie über geeignetes Personal für eine qualifizierte Praxisanleitung verfügt.

#### § 17 Rahmenbedingungen der fachpraktischen Ausbildung

(1) Die fachpraktische Ausbildung ist Bestandteil des Bildungsganges.
Wer seinen Praktikumsplatz verliert, soll innerhalb von zwei Wochen einen neuen Praktikumsplatz nachweisen.
Wird keine neue fachpraktische Ausbildung aufgenommen, ist die Schülerin oder der Schüler aus dem Bildungsgang zu entlassen.
Die betroffenen Schülerinnen und Schüler, und bei Nichtvolljährigen deren Eltern, sind unverzüglich von der Entscheidung zu unterrichten.

(2) Die Schülerinnen und Schüler werden in der fachpraktischen Ausbildung nicht im Rahmen eines den arbeitsrechtlichen Grundsätzen unterliegenden Ausbildungs- oder Beschäftigungsverhältnisses ausgebildet und tätig.
Es handelt sich um kein Praktikum im Sinne des Berufsbildungsgesetzes, kein Dienstverhältnis im Sinne des Personalvertretungsgesetzes und kein Arbeitnehmerverhältnis im Sinne des Betriebsverfassungsgesetzes.

(3) Die Schülerinnen und Schüler sind zur regelmäßigen Teilnahme an der fachpraktischen Ausbildung verpflichtet.
Bei der täglichen Beschäftigungszeit sind bei Nichtvolljährigen die Bestimmungen des Jugendarbeitsschutzgesetzes zu beachten.

(4) Die Praxisstelle und die Schule sind unverzüglich zu unterrichten, wenn ein Hinderungsgrund besteht, an der fachpraktischen Ausbildung teilzunehmen.

#### § 18 Durchführung der fachpraktischen Ausbildung

(1) Die Schule arbeitet mit den Praxisstellen eng zusammen.
Die Schulleitung benennt für jede Klasse eine geeignete Lehrkraft zur Begleitung der Schülerinnen und Schüler während der fachpraktischen Ausbildung.
Diese Lehrkraft hält engen Kontakt zur Praxisstelle und zu der praxisanleitenden Person und besucht die Schülerinnen und Schüler mindestens einmal im Schuljahr in der Praxisstelle.

(2) Am Ende eines jeden Schulhalbjahres gibt die Praxisstelle eine schriftliche Beurteilung über die Schülerin oder den Schüler ab.

(3) Die Beurteilung ist rechtzeitig zum Ablauf des Beurteilungszeitraums bei der Schule einzureichen.
Die Schule setzt den Abgabetermin fest.

#### § 19 Abschluss der fachpraktischen Ausbildung, Wiederholung

(1) Die fachpraktische Ausbildung ist erfolgreich abgeschlossen, wenn die für den Bildungsgang erforderlichen praxisbezogenen Kenntnisse und Fähigkeiten gemäß den Vorgaben für die fachpraktische Ausbildung erworben wurden.
Die Entscheidung über den erfolgreichen Abschluss der fachpraktischen Ausbildung trifft die Klassenkonferenz.

(2) Die Beurteilungen der Praxisstelle und die Auswertung der Berichtsbogen und der Praxisbesuche sind Grundlage der Entscheidung der Klassenkonferenz über den erfolgreichen Abschluss der fachpraktischen Ausbildung.
Die Entscheidung lautet „bestanden“ oder „nicht bestanden“.
Es werden keine Noten erteilt.
Bei nicht erfolgreicher Teilnahme ist die Jahrgangsstufe zu wiederholen.
Die Entscheidungsgründe sind im Protokoll festzuhalten.

(3) Ausfallzeiten in der fachpraktischen Ausbildung infolge von Krankheit oder sonstigen nicht selbst zu vertretenden Gründen werden je Schulhalbjahr bis zu höchstens zehn Prozent angerechnet, wenn dadurch das Ziel der fachpraktischen Ausbildung nicht beeinträchtigt wird.
Die Entscheidung trifft die Schulleitung.

(4) Auf schriftlichen Antrag der Schülerin oder des Schülers können bei Ausfallzeiten von mehr als zehn Prozent nach erfolgter Zustimmung der Praxisstelle die fachpraktische Ausbildung auch in den Ferien nachgeholt werden.
Die Entscheidung trifft die Schulleitung im Einvernehmen mit der Praxisstelle.

(5) Abweichend von Absatz 3 können darüber hinausgehende Fehlzeiten nur durch das zuständige staatliche Schulamt auf begründeten Antrag des Oberstufenzentrums erfolgen.
Soweit eine Anrechnung nicht erfolgt, ist die Jahrgangsstufe zu wiederholen.

(6) Bei Nichtversetzung auf Grund mangelnder schulischer Leistungen muss auch eine erfolgreich abgeschlossene fachpraktische Ausbildung wiederholt werden.

Kapitel 2  
Fachhochschulreifeprüfung
-------------------------------------

### Abschnitt 1  
Allgemeine Prüfungsbestimmungen

#### § 20 Grundsätze der Fachhochschulreifeprüfung

(1) In der Fachhochschulreifeprüfung soll der Prüfling nachweisen, dass er das Ziel des Bildungsganges oder des Zusatzangebots erreicht hat und die für das Studium an einer Fachhochschule geforderten Kenntnisse und Fähigkeiten besitzt.

(2) Die Prüfung findet zum Ende des Schuljahres statt.
Das für Schule zuständige Ministerium legt vor Beginn des Schuljahres einen Zeitrahmen für die Durchführung der Fachhochschulreifeprüfung fest.

(3) Die Prüfung besteht aus einem schriftlichen und einem mündlichen Teil.
In den Bildungsgängen der Fachoberschule Fachrichtung Gestaltung kann anstelle der fachrichtungsbezogenen schriftlichen Prüfung eine praktische Prüfung erfolgen.

(4) Die Prüfungsanforderungen richten sich nach den vom für Schule zuständigen Ministerium genehmigten curricularen Vorgaben und ergänzenden Vorschriften.

#### § 21 Prüfungsfächer

(1) Die schriftliche Prüfung findet zentral in den Fächern Deutsch, Englisch und Mathematik statt.
Im einjährigen und zweijährigen Bildungsgang findet die schriftliche Prüfung auch dezentral in einem fachrichtungsbezogenen Fach statt.
Das fachrichtungsbezogene Fach wird in den Stundentafeln gemäß der Anlage ausgewiesen.

(2) Die mündliche Prüfung findet im Fach Englisch statt.
Zusätzliche mündliche Prüfungen können in allen Fächern des letzten Schuljahres mit Ausnahme des Faches Sport gemäß Abschnitt 5 durchgeführt werden.

#### § 22 Nachteilsausgleich

Für die Gewährung eines Nachteilsausgleichs gilt § 10 Absatz 3 und 4 entsprechend.

#### § 23 Zuhörerinnen und Zuhörer

Als Zuhörerinnen und Zuhörer dürfen bei der mündlichen Prüfung anwesend sein

1.  die an der Schule unterrichtenden Lehrkräfte und
2.  die Lehramtsstudierenden und Lehramtskandidatinnen und Lehramtskandidaten, die der Schule zur Ausbildung zugewiesen sind.

In besonders begründeten Fällen kann die oder der Vorsitzende des Prüfungsausschusses weiteren Personen die Anwesenheit bei der mündlichen Prüfung gestatten.
Die Personen gemäß Nummer 2 dürfen mit Zustimmung der oder des Vorsitzenden des Prüfungsausschusses auch bei den Beratungen des Prüfungsausschusses und der Fachausschüsse anwesend sein.

### Abschnitt 2  
Prüfungsorgane

#### § 24 Prüfungsausschuss

(1) Für die Durchführung der Fachhochschulreifeprüfung wird für jede Klasse oder jeden Kurs ein Prüfungsausschuss gebildet.
Er entscheidet über alle Vorgänge des Prüfungsverfahrens.

(2) Die Mitglieder des Prüfungsausschusses müssen in der Regel über das Lehramt Sekundarstufe II (berufliche Fächer) oder über das Lehramt Sekundarstufe I und Sekundarstufe II (allgemeinbildende Fächer) oder über eine entsprechende Lehrbefähigung verfügen.

(3) Den Vorsitz des Prüfungsausschusses nimmt ein vom zuständigen staatlichen Schulamt benanntes Mitglied der Schulleitung als Vorsitzende oder Vorsitzender wahr.

(4) Dem Prüfungsausschuss gehören neben der den Vorsitz führenden Person die Lehrkräfte an, die die Prüflinge zuletzt in den Prüfungsfächern unterrichtet haben.
Die Mitglieder des Prüfungsausschusses sind zur Verschwiegenheit verpflichtet.

(5) Schulaufsichtsausübende Personen können an allen Prüfungen teilnehmen.
In diesem Fall ist das den Vorsitz des Prüfungsausschusses führende Mitglied vorher zu informieren.

#### § 25 Fachausschüsse

(1) Für jedes Prüfungsfach wird zur Durchführung der mündlichen Prüfung ein Fachausschuss gebildet.
Der Fachausschuss besteht aus einer oder einem Vorsitzenden sowie einer Fachprüferin oder einem Fachprüfer und einer weiteren sachkundigen Lehrkraft als Protokollantin oder Protokollant.
Fachprüferin oder Fachprüfer ist in der Regel die Lehrkraft, die den Prüfling zuletzt im Prüfungsfach unterrichtet hat.

(2) Das den Vorsitz führende Mitglied des Prüfungsausschusses beruft die Mitglieder des Fachausschusses in der Regel aus der Mitte der Mitglieder des Prüfungsausschusses.
Sie oder er ist berechtigt, den Vorsitz des Fachausschusses selbst zu übernehmen.
§ 24 Absatz 5 gilt entsprechend.

#### § 26 Teilnahmepflicht

(1) Die Mitglieder eines Ausschusses sind zur Teilnahme an dessen Sitzungen verpflichtet.

(2) Kann ein Mitglied eines Ausschusses seine Aufgaben wegen Krankheit oder aus einem anderen zwingenden Grund nicht wahrnehmen, so bestimmt die oder der Vorsitzende des Prüfungsausschusses eine Vertretung.

#### § 27 Beschlussfassung

Der Prüfungsausschuss ist beschlussfähig, wenn drei Viertel seiner Mitglieder anwesend sind.
Die Fachausschüsse sind nur beschlussfähig, wenn alle Mitglieder anwesend sind.
Die Ausschüsse beschließen mit der Mehrheit der abgegebenen Stimmen.
Bei Stimmengleichheit gibt die Stimme der oder des Vorsitzenden den Ausschlag.
Stimmenthaltung ist nicht zulässig.

### Abschnitt 3  
Zulassung und Teilnahme

#### § 28 Zulassung, Vornoten, Rücktritt

(1) Der oder die Vorsitzende des Prüfungsausschusses entscheidet über die Zulassung zur Fachhochschulreifeprüfung.
Die Zulassung oder Nichtzulassung ist der Schülerin oder dem Schüler, und bei Nichtvolljährigen deren Eltern, unverzüglich schriftlich mitzuteilen.
Die Zulassung zur Fachhochschulreifeprüfung verpflichtet zur Teilnahme.

(2) Bei Nichtzulassung zur Fachhochschulreifeprüfung kann auf schriftlichen Antrag das letzte Schuljahr des Bildungsganges wiederholt werden.
Dies gilt nicht, wenn das Schuljahr bereits gemäß § 13 wiederholt wurde.
Der Antrag ist spätestens zwei Schultage nach Bekanntgabe der Vornoten in den Fächern der schriftlichen Prüfung zu stellen.
Dem Prüfling, und bei Nichtvolljährigen dessen Eltern, ist die Entscheidung unverzüglich schriftlich mitzuteilen.

(3) Die Zulassung zur Fachhochschulreifeprüfung wird erteilt, wenn die Vornoten in den Fächern der schriftlichen Prüfung mindestens ausreichend sind.
Eine mangelhafte Leistung kann in nur einem Fach der schriftlichen Prüfung durch mindestens gute Leistungen in einem anderen Fach oder befriedigende Leistungen in zwei anderen Fächern der schriftlichen Prüfung ausgeglichen werden.
Eine ungenügende Leistung kann nicht ausgeglichen werden.

(4) Die Vornoten in allen Fächern werden von der im jeweiligen Fach unterrichtenden Lehrkraft auf der Grundlage der Leistungen im letzten Schuljahr des Bildungsganges festgelegt und in eine Prüfungsliste eingetragen.
Die Vornote wird ohne Dezimalstelle aus dem arithmetischen Mittel aller erbrachten Leistungen und unter Berücksichtigung der Leistungsentwicklung gebildet.
Auf Verlangen eines Mitgliedes des Prüfungsausschusses ist die erteilte Note zu begründen.

(5) Die Festlegung der Vornoten in den Fächern der schriftlichen Prüfung erfolgt in der Regel zwei Wochen vor Beginn der schriftlichen Prüfung.
Die Festlegung der Vornoten in den Fächern, die nicht Gegenstand der schriftlichen Prüfung sind, erfolgt in der Regel eine Woche vor Beginn des Prüfungszeitraums der zusätzlichen mündlichen Prüfungen.
Die Vornoten sind den Prüflingen, und bei Nichtvolljährigen deren Eltern, unverzüglich schriftlich mitzuteilen.

(6) Abweichend von Absatz 3 kann, wenn eine Schülerin oder ein Schüler aus wichtigen, nicht selbst zu vertretenden Gründen, wie zum Beispiel längere Abwesenheit wegen Krankheit, die Leistungen nicht erbracht hat, aber die begründete Annahme besteht, dass eine Teilnahme an der Fachhochschulreifeprüfung Aussicht auf Erfolg hat, die Zulassung zur Fachhochschulreifeprüfung erteilt werden.

(7) Abweichend von Absatz 1 kann bei längeren Unterrichtsversäumnissen auf Grund von Krankheit oder anderen nicht vom Prüfling zu vertretenden Gründen das den Vorsitz führende Mitglied des Prüfungsausschusses auf Antrag des Prüflings den Rücktritt von der Fachhochschulreifeprüfung gestatten.
In diesem Fall wiederholt der Prüfling das letzte Schuljahr des Bildungsganges.
Der Antrag ist spätestens zwei Schultage nach Bekanntgabe der Vornoten in den Fächern der schriftlichen Prüfung zu stellen.
Die Entscheidung ist dem Prüfling, und bei Nichtvolljährigen dessen Eltern, unverzüglich schriftlich mitzuteilen.

#### § 29 Erkrankung, Versäumnis, Verweigerung

(1) Wer aus einem nicht selbst zu vertretenden Grund an der Fachhochschulreifeprüfung oder an einzelnen Prüfungsteilen nicht teilnehmen kann, muss dieses unverzüglich anzeigen und den Grund nachweisen.
Krankheit muss durch eine ärztliche Bescheinigung belegt werden.

(2) Das den Vorsitz des Prüfungsausschusses führende Mitglied prüft die Unterlagen und entscheidet, ob die Nichtteilnahme vom Prüfling zu vertreten oder nicht zu vertreten ist.
Es bestimmt, zu welchem Zeitpunkt die Fachhochschulreifeprüfung gegebenenfalls neu angesetzt oder fortgeführt wird.

(3) Prüfungsleistungen, die bereits erbracht worden sind, werden angerechnet.
Für nachzuholende schriftliche Prüfungen ist gemäß § 31 zu verfahren.

(4) Versäumt der Prüfling aus von ihm zu vertretenden Gründen einzelne Prüfungsteile oder verweigert er Prüfungsleistungen, werden diese als ungenügende Leistung gewertet.

#### § 30 Täuschungen und Unregelmäßigkeiten

(1) Bedient sich ein Prüfling zur Erbringung einer Leistung in der Fachhochschulreifeprüfung unerlaubter Hilfe, so ist dies eine Täuschung.

(2) Wird jemand beim Begehen einer Täuschung bemerkt, ist unverzüglich darüber zu entscheiden, ob die Prüfung fortgesetzt werden darf.

(3) Ist die Täuschung von geringem Umfang und eindeutig zu begrenzen, so wird der unter Täuschung entstandene Teil der Leistung als nicht erbracht bewertet oder der von der Täuschung betroffene Teil kann wiederholt werden.
Ist die Täuschung von großem Umfang, so wird die gesamte Leistung als ungenügend bewertet.

(4) Bei besonders schweren Fällen von Täuschung kann der Prüfling von der weiteren Prüfung ausgeschlossen werden.
Die Fachhochschulreifeprüfung gilt dann als nicht bestanden.

(5) Wer durch eigenes Verhalten die Prüfung so schwerwiegend behindert, dass die ordnungsgemäße Durchführung der eigenen Prüfung oder die anderer gefährdet ist, kann von der weiteren Prüfung ausgeschlossen werden.
Die Fachhochschulreifeprüfung gilt damit als nicht bestanden.

(6) Die Entscheidungen gemäß den Absätzen 2 bis 5 trifft das den Vorsitz führende Mitglied des Prüfungsausschusses.

(7) Werden Aufgabenstellungen vor Beginn der schriftlichen oder mündlichen Prüfung Unberechtigten bekannt, dürfen sie nicht verwendet werden.
Über das weitere Verfahren entscheidet das für Schule zuständige Ministerium.

(8) Stellt sich nach der schriftlichen oder mündlichen Prüfung, aber noch vor dem Abschluss der Fachhochschulreifeprüfung heraus, dass die Aufgabenstellung für die schriftliche oder mündliche Prüfung Unberechtigten bekannt gewesen ist, muss die jeweilige Prüfung wiederholt werden.
Die Entscheidung darüber trifft das für Schule zuständige Ministerium.

(9) Wird erst nach Abschluss der Fachhochschulreifeprüfung eine Täuschung festgestellt, so entscheidet das staatliche Schulamt im Benehmen mit dem für Schule zuständigen Ministerium, ob die Prüfung als nicht bestanden und das Zeugnis für ungültig erklärt wird.

### Abschnitt 4  
Schriftliche Prüfung

#### § 31 Durchführung der schriftlichen Prüfungen

(1) Die Aufgaben für die zentralen schriftlichen Prüfungsfächer und weitere Hinweise werden jährlich durch das für Schule zuständige Ministerium festgelegt und den Schulen zur Verfügung gestellt.
Für einen zentralen Nachprüfungstermin wird eine Ersatzaufgabe zur Verfügung gestellt.

(2) Für die dezentralen schriftlichen Prüfungsfächer sind zwei gleichwertige Aufgabenvorschläge einzureichen.
Die Aufgabenvorschläge werden in der Regel von der Lehrkraft erarbeitet, die in dem Schuljahr, in dem die Prüfung stattfindet, in dem Prüfungsfach den regelmäßigen Unterricht erteilt hat (aufgabenstellende Lehrkraft).
Die Schulleiterin oder der Schulleiter prüft die Aufgabenvorschläge auf Übereinstimmung mit den Rechtsvorschriften.
Die Genehmigung und Auswahl erfolgt durch die zuständige Schulrätin oder den zuständigen Schulrat.

(3) Im Falle eines zweiten, dezentralen Nachprüfungstermins ist sowohl für zentrale als auch für dezentrale schriftliche Prüfungsfächer von der aufgabenstellenden Lehrkraft ein Aufgabenvorschlag zu erarbeiten.

#### § 32 Bewertung der schriftlichen Prüfungsarbeiten

Die schriftlichen Arbeiten werden von der im Fach unterrichtenden Lehrkraft korrigiert und bewertet.
Die Bewertung ist zu begründen.

### Abschnitt 5  
Mündliche Prüfung

#### § 33 Vorkonferenz, Zulassung zur mündlichen Prüfung

(1) In der Vorkonferenz entscheidet der Prüfungsausschuss über die Zulassung zur mündlichen Prüfung und legt die Fächer fest, in denen zusätzlich zur mündlichen Prüfung im Fach Englisch eine mündliche Prüfung erfolgen soll.

(2) Die Ergebnisse der schriftlichen Prüfung werden vom Prüfungsausschuss in der Vorkonferenz festgestellt.

(3) Zur mündlichen Prüfung wird nicht zugelassen, wer in zwei oder mehr Fächern der schriftlichen Prüfung schlechter als ausreichend bewertet wurde.
Die Fachhochschulreifeprüfung gilt dann als nicht bestanden.
Der Prüfungsausschuss setzt in diesem Fall die Endnoten für alle Fächer fest.

(4) Eine zusätzliche mündliche Prüfung soll nur in den Fächern stattfinden, in denen zur abschließenden Beurteilung eine Prüfung erforderlich ist.

(5) Der Prüfungsausschuss entscheidet auf Antrag des den Vorsitz führenden Mitgliedes oder der im jeweiligen Fach unterrichtenden Lehrkraft über zusätzliche mündliche Prüfungen.
In der Regel soll hiernach kein Prüfling in mehr als zwei Fächern zusätzlich geprüft werden.

(6) Den Prüflingen ist nach Abschluss der Vorkonferenz und mindestens eine Woche vor der mündlichen Prüfung bekannt zu geben, ob und in welchen Fächern sie zusätzlich mündlich geprüft werden.

(7) Jedem Prüfling werden die in den schriftlichen Prüfungen erzielten Noten unverzüglich nach Abschluss der Vorkonferenz mitgeteilt.

(8) Den Prüflingen, die gemäß Absatz 3 die Fachhochschulreifeprüfung nicht bestanden haben, wird dieses umgehend nach der Vorkonferenz in einem Einzelgespräch mitgeteilt.
Bei Nichtvolljährigen sind deren Eltern unverzüglich von der Entscheidung zu unterrichten.

(9) Nach Bekanntgabe der Ergebnisse der Vorkonferenz sind die Prüflinge vom Unterricht befreit.

#### § 34 Antrag des Prüflings auf mündliche Prüfung

(1) Jeder Prüfling kann zusätzlich zu den Fächern der mündlichen Prüfung gemäß § 33 Absatz 5 höchstens zwei Fächer benennen, in denen er mündlich geprüft werden möchte.
Der Prüfling muss diesen Antrag spätestens zwei Schultage nach Bekanntgabe der Fächer der mündlichen Prüfung schriftlich an das den Vorsitz des Prüfungsausschusses führende Mitglied richten.
Dem Wunsch des Prüflings ist zu entsprechen.

(2) Der Prüfling kann von seinem Antrag nur dann zurücktreten, wenn er nicht selbst zu vertretende Gründe einer Verhinderung vorbringen kann, die der Prüfungsausschuss anerkennt.

#### § 35 Durchführung der mündlichen Prüfung

(1) Die mündliche Prüfung findet vor dem zuständigen Fachausschuss statt und wird von der Fachprüferin oder dem Fachprüfer durchgeführt.

(2) Die mündliche Prüfung wird im Fach Englisch als Gruppenprüfung mit in der Regel zwei Prüflingen und in allen anderen Fächern als Einzelprüfung durchgeführt.

#### § 36 Bewertung der mündlichen Prüfung

Die prüfende Lehrkraft schlägt die Note für die mündliche Prüfung vor.
Der Fachausschuss legt die Note fest und teilt sie dem Prüfling mit.

### Abschnitt 6  
Abschluss der Prüfung, Prüfungswiederholung

#### § 37 Abschlusskonferenz, Endnoten

(1) Nach Abschluss der mündlichen Prüfungen setzt der Prüfungsausschuss in der Abschlusskonferenz für jedes Fach die Endnote fest, die in das Zeugnis der Fachhochschulreife Eingang findet.

(2) Grundlage für die Festlegung der Endnoten sind die Vornoten und die Ergebnisse der schriftlichen und der mündlichen Prüfung.
Die Endnoten werden in den Fächern der schriftlichen Prüfung aus dem arithmetischen Mittel der zweifach gewichteten Vornote, der Note der schriftlichen Prüfung und der Note der mündlichen Prüfung, sofern mündlich geprüft wurde, gebildet.
Die Endnoten in den übrigen Fächern werden aus dem arithmetischen Mittel der zweifach gewichteten Vornote und der Note der mündlichen Prüfung gebildet.
Das Ergebnis ist nach der rechnerischen Ermittlung durch Auf- und Abrunden festzusetzen.
Liegt das rechnerische Ergebnis genau zwischen zwei Notenstufen (n,5) ist zugunsten der Schülerin oder des Schülers zu entscheiden.

(3) Findet in einem Fach weder eine schriftliche noch eine mündliche Prüfung statt, so ist die Vornote die Endnote.

#### § 38 Prüfungsergebnis

(1) Der Prüfungsausschuss stellt das Ergebnis der Prüfung fest, das „bestanden“ oder „nicht bestanden“ lautet.
Bei bestandener Abschlussprüfung legt der Prüfungsausschuss außerdem die Durchschnittsnote gemäß § 39 Absatz 2 fest.

(2) Die Prüfung ist bestanden, wenn die Endnote in allen Fächern mit Ausnahme des Faches Sport mindestens „ausreichend“ lautet.
Eine Endnote „mangelhaft“ in höchstens einem Fach kann durch mindestens gute Leistungen in einem anderen Fach oder befriedigende Leistungen in zwei anderen Fächern ausgeglichen werden.
Ein Leistungsausgleich in einem Fach der schriftlichen Prüfung ist nur durch Endnoten in anderen schriftlichen Prüfungsfächern möglich.
Eine ungenügende Leistung kann nicht ausgeglichen werden.

#### § 39 Zeugnis der Fachhochschulreife

(1) Wer die Fachhochschulreifeprüfung bestanden hat, erhält das Zeugnis der Fachhochschulreife.
Dieses Zeugnis trägt das Datum des Tages der Aushändigung.

(2) Auf dem Zeugnis ist eine Durchschnittsnote auszuweisen, die sich aus dem arithmetischen Mittel der Zeugnisnoten ergibt.
Die Note im Fach Sport wird nicht berücksichtigt.
Die Durchschnittsnote wird auf eine Stelle hinter dem Komma errechnet, dabei wird nicht gerundet.
Die Durchschnittsnote wird in Ziffern ausgewiesen.

(3) Ort, Datum und Zeit der Aushändigung der Zeugnisse der Fachhochschulreife werden von der Schulleitung festgelegt.
Prüflingen in einem doppelqualifizierenden Bildungsgang darf das Zeugnis erst ausgehändigt werden, wenn der Nachweis der abgeschlossenen Berufsausbildung vorliegt.

#### § 40 Wiederholung bei Nichtbestehen

(1) Eine erstmalig nicht bestandene Fachhochschulreifeprüfung kann nach dem nochmaligen Besuch des letzten Schuljahres einmal wiederholt werden.
Bei Vorliegen besonderer Gründe, die die Schülerin oder der Schüler nicht selbst zu vertreten hat, kann das zuständige staatliche Schulamt eine zweite Wiederholung gestatten.

(2) Eine Wiederholung setzt voraus, dass auch im folgenden Schuljahr der zu wiederholende Bildungsgang eingerichtet ist.

(3) Wer die Wiederholung der Fachhochschulreifeprüfung nicht besteht, ist zu entlassen.
Die betroffenen Schülerinnen und Schüler, und bei Nichtvolljährigen deren Eltern, sind unverzüglich von der Entscheidung zu unterrichten.

Kapitel 3  
Besondere Bestimmungen
----------------------------------

### Abschnitt 1  
Allgemeines

#### § 41 Erwerb der Fachhochschulreife

Für den Erwerb der Fachhochschulreife gilt Kapitel 2 entsprechend, soweit nachstehend nichts anderes geregelt ist.

### Abschnitt 2  
Ergänzende Bestimmungen für den doppelqualifizierenden Bildungsgang

#### § 42 Wiederholung bei Nichtbestehen der Fachhochschulreifeprüfung

Der Anspruch auf Wiederholung der Fachhochschulreifeprüfung besteht auch dann, wenn die Ausbildung erfolgreich abgeschlossen und dadurch das Ausbildungsverhältnis beendet wurde.

### Abschnitt 3  
Ergänzende Bestimmungen für das Zusatzangebot zum Erwerb der Fachhochschulreife für Schülerinnen und Schüler in einer Berufsausbildung

#### § 43 Voraussetzungen

(1) Wer sich in einer Berufsausbildung nach Berufsbildungsgesetz oder Handwerksordnung von mindestens dreijähriger Regelausbildungsdauer befindet und vor Eintritt in die Ausbildung den mittleren Schulabschluss oder einen gleichwertigen Abschluss erworben hat, kann an einem Zusatzangebot in Form von Zusatzkursen in den Fächern Deutsch, Englisch und Mathematik teilnehmen, um die Fachhochschulreife zu erwerben.
Der Erwerb der Fachhochschulreife setzt den erfolgreichen Abschluss der Berufsausbildung voraus.

(2) An dem Zusatzangebot kann auch teilnehmen, wer sich in einer zweijährigen Berufsausbildung nach Landesrecht befindet, die den mittleren Schulabschluss voraussetzt.
Der Erwerb der Fachhochschulreife setzt den erfolgreichen Abschluss dieser Berufsausbildung sowie den Nachweis eines im Anschluss an diese Berufsausbildung absolvierten halbjährigen einschlägigen Praktikums im Umfang von mindestens 800 Stunden oder einer anschließenden zweijährigen Berufstätigkeit oder einer weiteren abgeschlossenen Berufsausbildung voraus.

#### § 44 Aufnahme

(1) Für die Aufnahme in das Zusatzangebot ist ein schriftlicher Antrag zu stellen.
Dem Antrag ist der Nachweis über den geforderten Schulabschluss beizufügen.

(2) Die Aufnahme erfolgt jeweils zu Beginn eines Schuljahres.
Über die Aufnahme entscheidet die Schulleiterin oder der Schulleiter.
Wer in das Zusatzangebot aufgenommen wurde, hat die Pflicht zur Teilnahme.

(3) Bei Übernachfrage gelten die Bestimmungen entsprechend § 6.

#### § 45 Unterrichtsorganisation

(1) Das zusätzliche Unterrichtsangebot wird in Form von Zusatzkursen in den Fächern Deutsch, Englisch und Mathematik organisiert.
Es kann bildungsgang- und schulübergreifend angeboten werden.
Im Fall eines schulübergreifenden Angebots beauftragt das staatliche Schulamt eine Schule mit der Durchführung.

(2) Der Umfang der Zusatzkurse richtet sich nach den zeitlichen Vorgaben der Anlage.
Der Unterricht wird entsprechend den curricularen Vorgaben des für Schule zuständigen Ministeriums erteilt.

#### § 46 Bescheinigung, Zeugnisse

(1) Am Ende eines jeden Schulhalbjahres wird eine Bescheinigung über die Leistungen in den Zusatzkursen erteilt.

(2) Die Bescheinigungen gemäß Absatz 1 und das Zeugnis über den Erwerb der Fachhochschulreife werden von der Schule ausgestellt, die die Kurse durchführt.

#### § 47 Verlassen des Zusatzangebots

(1) Wer aus selbst zu vertretenden Gründen nicht am Unterricht teilnimmt, kann auf der Grundlage von § 64 Absatz 4 des Brandenburgischen Schulgesetzes aus dem Zusatzangebot entlassen werden.

(2) Wer am Ende des zweiten Schulhalbjahres in einem Zusatzkurs ungenügende oder in zwei der drei Fächer mangelhafte Leistungen erbringt, wird aus dem Zusatzangebot entlassen.
Eine erneute Aufnahme zu einem späteren Zeitpunkt ist nicht möglich.

#### § 48 Prüfung zum Erwerb der Fachhochschulreife

Die Prüfung erfolgt schriftlich in den Fächern Deutsch, Englisch und Mathematik.
Im Fach Englisch findet auch eine mündliche Prüfung statt.
Jeder Prüfling kann zusätzlich auf Antrag in den Fächern Deutsch und Mathematik mündlich geprüft werden.

### Abschnitt 4  
Nichtschülerprüfung zum Erwerb der Fachhochschulreife

#### § 49 Zulassungsvoraussetzungen

(1) Wer die Fachhochschulreife erwerben will, ohne einen Bildungsgang der Fachoberschule an einer Schule in öffentlicher Trägerschaft oder an einer anerkannten Ersatzschule zu besuchen, kann eine Nichtschülerprüfung ablegen, wenn er nachweist, dass er sich auf die Prüfung angemessen vorbereitet hat.

(2) Zur Nichtschülerprüfung kann sich anmelden, wer seine Wohnung oder seinen gewöhnlichen Aufenthalt im Land Brandenburg hat und die Voraussetzungen gemäß § 4 Absatz 2 erfüllt oder als Schülerin oder Schüler einer genehmigten Ersatzschule einen Bildungsgang der Fachoberschule in Vollzeitform besucht und im Fall des zweijährigen Bildungsganges die fachpraktische Ausbildung erfolgreich abgeschlossen hat.

(3) Zur Nichtschülerprüfung wird nicht zugelassen, wer die Fachhochschulreifeprüfung bereits einmal wiederholt hat.

#### § 50 Antragstellung, Zulassung

(1) Der Antrag auf Zulassung zur Nichtschülerprüfung ist spätestens bis zum 1.
November des Schuljahres, in dem diese abgelegt werden soll, an das zuständige staatliche Schulamt zu richten.
Dem Antrag sind beizufügen:

1.  Zeugnisse und sonstige Nachweise über die Erfüllung der gemäß § 49 geforderten Zulassungsvoraussetzungen,
2.  ein tabellarischer Lebenslauf,
3.  ein Nachweis zum Wohnort,
4.  eine Erklärung über bereits unternommene Prüfungsversuche zum Erwerb der Fachhochschulreife,
5.  eine Erklärung darüber, in welcher Fachrichtung die Prüfung erfolgen soll und
6.  eine Darstellung über Art und Umfang der Vorbereitung auf die Prüfung.

(2) Das zuständige staatliche Schulamt entscheidet nach Prüfung der Zulassungsvoraussetzungen über die Zulassung zur Prüfung und legt fest, in welchem Oberstufenzentrum die Prüfung durchgeführt werden soll.
Die Zulassungsentscheidung und der Prüfungsort werden den Bewerberinnen und Bewerbern spätestens bis zum 31.
Januar des Prüfungsjahres mitgeteilt.

(3) Die mit der Prüfung beauftragte Schule berät die Bewerberinnen und Bewerber in Fragen der fachlichen Vorbereitung und des Prüfungsverfahrens.
Sie teilt ihnen den Prüfungsort, die Termine der schriftlichen Prüfungen und die Prüfungsfächer spätestens vier Wochen vor dem ersten Prüfungstermin mit.

#### § 51 Prüfungsbestimmungen

(1) Die Prüflinge der Nichtschülerprüfung nehmen an der Fachhochschulreifeprüfung der Schule, der sie zugewiesen sind, teil.
Sie haben sich vor jeder Prüfung auszuweisen.

(2) Die Prüfungsfächer und Prüfungsaufgaben der schriftlichen Prüfung sind dieselben wie für die Schülerinnen und Schüler der mit der Prüfung beauftragten Schule.

(3) Eine mündliche Prüfung erfolgt

1.  in den vier Fächern der schriftlichen Prüfung,
2.  im Fach Politische Bildung und
3.  in einem naturwissenschaftlichen Fach, das von der prüfenden Schule festgelegt wird.

Von einer mündlichen Prüfung in höchstens einem der vier Fächer der schriftlichen Prüfung wird befreit, wer in der jeweiligen schriftlichen Prüfung gute oder sehr gute Leistungen erreicht hat.
Über die Befreiung entscheidet das den Vorsitz führende Mitglied des Prüfungsausschusses.

(4) Die mündlichen Prüfungen der einzelnen Prüflinge sind auf mindestens zwei Tage zu verteilen.
Die Mitteilung über die Fächer, Ort und Zeitpunkt der mündlichen Prüfungen erfolgt mindestens eine Woche vor Prüfungsbeginn in schriftlicher Form.

(5) Die Endnoten werden aus den in der Prüfung erbrachten Leistungen ermittelt.
In Fächern, die sowohl schriftlich als auch mündlich geprüft werden, ergibt sich die Endnote aus dem arithmetischen Mittel der Leistungen in beiden Prüfungsteilen, wobei die Note der schriftlichen Prüfung doppelt gewichtet wird.
Das Ergebnis ist nach der rechnerischen Ermittlung durch Auf- und Abrunden festzusetzen.
Liegt das rechnerische Ergebnis genau zwischen zwei Notenstufen (n,5) ist zugunsten der Schülerin oder des Schülers zu entscheiden.

(6) Wer die Nichtschülerprüfung bestanden hat, erhält ein Zeugnis über den Erwerb der Fachhochschulreife.
Das Zeugnis stellt die prüfende Schule aus.
Bei nicht bestandener Prüfung wird eine Bescheinigung über die Teilnahme und das Nichtbestehen der Fachhochschulreifeprüfung ausgestellt.

(7) Eine einmalige Wiederholung der Fachhochschulreifeprüfung kann frühestens nach einem Jahr auf Antrag erfolgen.

### Abschnitt 5  
Erwerb der Fachhochschulreife an Gymnasien, Gesamtschulen, beruflichen Gymnasien und im Zweiten Bildungsweg

#### § 52 Antrag

(1) Wer nach dem Erwerb der Fachhochschulreife (schulischer Teil) den Nachweis einer in Umfang und Ausgestaltung der fachpraktischen Ausbildung der Fachoberschule entsprechenden Ausbildung oder einer abgeschlossenen Berufsausbildung erbringt, erhält auf Antrag den Erwerb der Fachhochschulreife bescheinigt.

(2) Der Antrag ist bei dem staatlichen Schulamt zu stellen, das zum Zeitpunkt des Erwerbs der Fachhochschulreife (schulischer Teil) für diese Schule zuständig war.

(3) Dem Antrag sind das Zeugnis, auf dem der Erwerb des schulischen Teiles der Fachhochschulreife vermerkt ist, und entsprechende Nachweise über die ausreichende berufliche Bildung beizufügen.

#### § 53 Erwerb der beruflichen Bildung

(1) Eine ausreichende berufliche Bildung umfasst mindestens zwölf Monate und 800 Zeitstunden und kann nachgewiesen werden durch

1.  ein gelenktes Praktikum,
2.  eine praktische Tätigkeit im Rahmen eines Ausbildungsverhältnisses nach Bundes- oder Landesrecht,
3.  ein freiwilliges soziales oder ökologisches Jahr,
4.  einen Wehrdienst,
5.  einen Bundesfreiwilligendienst oder
6.  einen anderen anerkannten Freiwilligendienst.

(2) Die Voraussetzungen für die berufliche Bildung sind auch dann erfüllt, wenn der geforderte Zeitraum und der geforderte Stundenumfang sich insgesamt aus mehreren praktischen Tätigkeiten gemäß Absatz 1 Nummer 1 bis 6 ergeben.

(3) Die berufliche Bildung sollte in Betrieben, Behörden oder anderen gleichwertigen Einrichtungen stattfinden, die ausbildungsgeeignet sind.

Kapitel 4  
Verwaltungsweg, Ausnahme-, Übergangs- und Schlussbestimmungen
-------------------------------------------------------------------------

#### § 54 Widerspruch und Akteneinsicht

(1) Gegen schulische Entscheidungen, die Verwaltungsakte sind, kann bei der Schule Widerspruch eingelegt werden.
Hierüber sind die Schülerinnen und Schüler, und bei Nichtvolljährigen deren Eltern, schriftlich zu belehren.
Die Durchführung des Widerspruchsverfahrens richtet sich nach den geltenden Rechts- und Verwaltungsvorschriften.

(2) Wird dem Widerspruch nicht stattgegeben, entscheidet das staatliche Schulamt.

(3) Den Schülerinnen und Schülern, und bei Nichtvolljährigen deren Eltern, ist auf Antrag Einsicht in die sie betreffenden Prüfungsakten zu geben, soweit deren Kenntnis zur Geltendmachung oder Verteidigung ihrer rechtlichen Interessen erforderlich ist.

#### § 55 Übergangsbestimmungen

(1) Schülerinnen und Schüler, die vor Inkrafttreten dieser Verordnung einen Bildungsgang auf der Grundlage der Fachoberschul- und Fachhochschulreifeverordnung vom 8.
August 2008 (GVBl. II S. 346) begonnen haben, beenden diesen nach der bisher geltenden Verordnung.

(2) Schülerinnen und Schüler, die mit Inkrafttreten dieser Verordnung in den einjährigen Bildungsgang eintreten und gemäß § 4 Absatz 2 Satz 2 in der Klasse am Unterricht des zweijährigen Bildungsganges im zweiten Schulbesuchsjahr teilnehmen, absolvieren diesen nach der geltenden Verordnung für diese Klasse.

(3) Schülerinnen und Schüler, die bis zum 31.
Juli 2020 den schulischen Teil der Fachhochschulreife erworben haben, können bis zum 31.
Juli 2022 den berufspraktischen Teil auf der Grundlage von § 32 Absatz 4 der Gymnasiale-Oberstufe-Verordnung vom 21.
August 2009 (GVBl.
II S.
578), die zuletzt durch Verordnung vom 30.
Januar 2018 (GVBl.
II Nr.
9) geändert worden ist, in Verbindung mit Nummer 12 der VV-Zeugnisse vom 24.
November 2011 (Abl.
MBJS S.
294), die zuletzt durch die Verwaltungsvorschriften vom 17.
Januar 2019 (Abl.
MBJS S.
8) geändert worden sind, absolvieren.

#### § 56 Inkrafttreten, Außerkrafttreten

(1) Diese Verordnung tritt mit Wirkung vom 1.
August 2020 in Kraft.

(2) Mit Inkrafttreten dieser Verordnung tritt die Fachoberschul- und Fachhochschulreifeverordnung vom 8. August 2008 (GVBl.
II S.
346) außer Kraft.

Potsdam, den 16.
September 2020

Die Ministerin für Bildung,  
Jugend und Sport

Britta Ernst

* * *

### Anlagen

1

[Anlage Stundentafel](/br2/sixcms/media.php/68/FOSFHRV_Anlage.pdf "Anlage Stundentafel") 347.5 KB