## Verordnung über den Schutzwald „Naturentwicklungsgebiet Elsbruch“

Auf Grund des § 12 des Waldgesetzes des Landes Brandenburg vom 20. April 2004 (GVBl. I S. 137) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzwald

Die in § 2 näher bezeichneten Waldflächen werden zum Schutzwald erklärt.
Der Schutzwald trägt die Bezeichnung „Naturentwicklungsgebiet Elsbruch“ und wird in das Register der geschützten Waldgebiete aufgenommen.

#### § 2 Schutzgegenstand

(1) Das geschützte Waldgebiet befindet sich im Landkreis Prignitz und hat eine Größe von rund 207 Hektar.
Es umfasst folgende Flurstücke:

**Gemeinde:**

**Gemarkung:**

**Flur:**

**Flurstück:**

Perleberg

Karstädt

Kuhwinkel

Laaslich

1

7

1, 2/1, 5, 7/1, 11;

2/1.

(2) Zur Orientierung sind dieser Verordnung eine Kartenskizze über die Lage des Schutzwaldes als Anlage 1 sowie eine Kartenskizze, auf der die in § 5 Absatz 1 Nummer 1 genannten Flächen für biotopeinrichtende Maßnahmen dargestellt sind, als Anlage 2 beigefügt.

(3) Die Grenze des Schutzwaldes ist in der „Topografischen Karte zur Verordnung über den Schutzwald ‚Naturentwicklungsgebiet Elsbruch‘“, Maßstab 1 : 10 000 und in den in Anlage 3 dieser Verordnung mit den Blattnummern 1 bis 3 aufgeführten Liegenschaftskarten mit dem Titel „Liegenschaftskarte zur Verordnung über den Schutzwald ‚Naturentwicklungsgebiet Elsbruch‘“, Maßstab 1 : 2 500 mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die Fläche des Schutzwaldes ist rot schraffiert.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in der Liegenschaftskarte.
Die Karten sind mit dem Dienstsiegel des Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft, Siegelnummer 19, versehen und vom Siegelverwalter am 28. November 2014 unterschrieben worden.

(4) Die Verordnung mit Karten kann bei dem für Forsten zuständigen Fachministerium des Landes Brandenburg, oberste Forstbehörde, in Potsdam sowie beim Landesbetrieb Forst Brandenburg in Potsdam, untere Forstbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Schutzwaldes, der als Kernzone des Biosphärenreservates Flusslandschaft Elbe-Brandenburg der direkten menschlichen Einflussnahme entzogen ist und in dem die Lebensräume und Lebensgemeinschaften langfristig ihrer natürlichen Entwicklung überlassen bleiben, ist

1.  die Sicherung und Entwicklung eines Komplexes standorttypischer, reich strukturierter und in ihrer Entwicklung von der Dynamik der lokalen und regionalen Grundwasserstände (großräumige Beeinflussung durch die Elbe) abhängiger Waldgesellschaften (insbesondere Erlen-Eschen-Wälder, Erlenbruchwälder, Eichen-Hainbuchen-Wälder);
2.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungsgebiet wild lebender Tier- und Pflanzenarten;
3.  die Beobachtung und wissenschaftliche Dokumentation vom Menschen unbeeinflusster Waldentwicklungsprozesse;
4.  die Sicherung und Regeneration forstgenetischer Ressourcen.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teils des Gebietes von gemeinschaftlicher Bedeutung „Silge“ mit der Gebietsnummer DE 2936-302 im Sinne des § 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes mit seinem Vorkommen von

1.  Hainsimsen-Buchenwald, Eichen-Hainbuchenwald und alten bodensauren Eichenwäldern auf Sandebenen mit Quercus robur (Stieleiche) als Biotope von gemeinschaftlichem Interesse und Auenwäldern mit Alnus glutinosa und Fraxinus excelsior (Alno-Padion, Alnion incanae, Salicion albae) als prioritärer Biotop von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG);
2.  Mopsfledermaus (Barbastellus barbastellus) und Großem Mausohr (Myotis myotis) als Tierarten von gemeinschaftlichem Interesse (im Sinne des Anhangs II der Richtlinie 92/43/EWG) einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

(3) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teils des Europäischen Vogelschutzgebietes „Unteres Elbtal“ mit der Gebietsnummer DE 3036-401 im Sinne des § 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes in seiner Funktion als Lebensraum von Arten nach Anhang I der Vogelschutz-Richtlinie (2009/147/EG), beispielsweise Seeadler (Haliaeetus albicilla), Kranich (Grus grus), Schwarzstorch (Ciconia nigra), Mittelspecht (Dendrocopos medius), Schwarzspecht (Dryocopus martius), Eisvogel (Alcedo atthis) und Rotmilan (Milvus milvus) einschließlich ihrer Brut- und Nahrungsbiotope.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind im Schutzwald gemäß § 12 Absatz 6 des Waldgesetzes des Landes Brandenburg alle Handlungen verboten, die dem in § 3 genannten Schutzzweck zuwiderlaufen und das Gebiet oder einzelne seiner Bestandteile nachhaltig stören, verändern, beschädigen oder zerstören können.

(2) Es ist insbesondere verboten:

1.  das Gebiet forstwirtschaftlich zu nutzen;
2.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
3.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
4.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
5.  das Gebiet außerhalb der Wege zu betreten;
6.  im Gebiet zu reiten;
7.  im Gebiet mit motorisierten Fahrzeugen und Gespannen zu fahren oder diese dort abzustellen;
8.  zu zelten, zu lagern, Wohnwagen aufzustellen, zu rauchen und Feuer zu entzünden;
9.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
10.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
11.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
12.  die Ruhe der Natur durch Lärm zu stören;
13.  Düngemittel einschließlich Wirtschaftsdünger und Sekundärrohstoffdünger zum Zwecke der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
14.  Pflanzenschutzmittel jeder Art oder Holzschutzmittel anzuwenden;
15.  Abfälle oder sonstige Gegenstände zu lagern, abzulagern oder sich ihrer in sonstiger Weise zu entledigen;
16.  Tiere zu füttern oder Futter bereitzustellen;
17.  Tiere auszusetzen oder Pflanzen anzusiedeln;
18.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
19.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
20.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  biotopeinrichtende Maßnahmen zur Entwicklung von Waldgesellschaften der potenziell natürlichen Vegetation auf Waldstandorten mit nicht standortgerechten oder gebietsfremden Baumarten.
    Hierzu sind Waldumbaumaßnahmen in noch vorhandenen Nadelholzbeständen auf den Flächen in den Abteilungen 1172 a3, a4, a5, 1173 a1, a2, 1175 b2, 1177 a5, 1178 b, 1179 a1, a3, 1181 a1, 1182 b2, 1183 a2, 1185 a2, 1188 a1, a4 und 1191 a3 mit anschließender Anwuchspflege bis zum 31. Dezember 2024 zulässig.
    Sofern Maßnahmen über diesen Zeitraum hinaus erforderlich werden, kann gemäß § 5 Absatz 1 Nummer 4 eine Verlängerung angeordnet werden;
2.  die Beerntung des Saatgutes der Stieleiche (Quercus robur) im zugelassenen Saatgutbestand mit der Registernummer 123817030052 (Abteilung 1190 a4 und 1191 a2) in Abhängigkeit vom Behang mit der Maßgabe, dass die Früchte ausschließlich manuell durch Auflesen geerntet werden und der Schutzwald nicht maschinell befahren wird;
3.  die Anlage von Weisergattern zu wissenschaftlichen Untersuchungen;
4.  Schutz-, Pflege- und Entwicklungsmaßnahmen, die von der unteren Forstbehörde im Einvernehmen mit der Fachbehörde für Naturschutz und Landschaftspflege angeordnet werden;
5.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen;
6.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die Forstbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen;
7.  das Betreten des Schutzwaldes außerhalb der Wege zur Durchführung organisierter Führungen zur Umweltbildung mit Zustimmung der unteren Forstbehörde und im Einvernehmen mit der Fachbehörde für Naturschutz und Landschaftspflege;
8.  erforderliche Maßnahmen der Verkehrssicherungspflicht, wobei das Holz als liegendes Totholz randlich im Schutzwald zu belassen ist.

(2) Die in § 4 für das Betreten und Befahren des Schutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Forst- und Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer und Nutzer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums.

#### § 6 Pflege- und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  Entwicklung eines hohen Grundwasserstandes, der den naturnahen Verhältnissen entspricht und in den Erlenbruchwäldern einen flurnahen Grundwasserstand und zeitweise Überstauungen ermöglicht;
2.  Wege werden aus der Nutzung und Unterhaltung genommen und punktuell rückgebaut, wenn sie nicht mehr für die Durchführung biotopeinrichtender Maßnahmen nach § 5 Absatz 1 Nummer 1 benötigt werden.

#### § 7 Ausnahmen, Befreiungen

(1) Aus Gründen des Waldschutzes kann die untere Forstbehörde im Einvernehmen mit der Fachbehörde für Naturschutz und Landschaftspflege Ausnahmen von den Verboten dieser Verordnung zulassen, sofern der Schutzzweck nicht beeinträchtigt wird.

(2) Wenn überwiegende Gründe des Gemeinwohls es erfordern, kann die untere Forstbehörde im Einvernehmen mit der Fachbehörde für Naturschutz und Landschaftspflege auf Antrag Befreiungen von den Verboten dieser Verordnung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 37 Absatz 1 Nummer 7 des Waldgesetzes des Landes Brandenburg handelt, wer vorsätzlich oder fahrlässig den Regelungen der §§ 4 und 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 37 Absatz 3 des Waldgesetzes des Landes Brandenburg mit einer Geldbuße bis zu zwanzigtausend Euro geahndet werden.

#### § 9 Verhältnis zu anderen rechtlichen Bestimmungen

(1) Die Regelungen naturschutzrechtlicher Schutzgebietsausweisungen im Bereich des in § 2 genannten Gebietes bleiben unberührt.

(2) Die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§ 17 des Brandenburgischen Naturschutzausführungsgesetzes, § 30 des Bundesnaturschutzgesetzes in Verbindung mit § 18 des Brandenburgischen Naturschutzausführungsgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes) bleiben unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 1 der Waldschutzgebietsverfahrensverordnung genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber der obersten Forstbehörde geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 9.
Februar 2015

Der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft

Jörg Vogelsänger

### Anlagen

1

[Schutzwald-NEG-Elsbruch-Anlage 1.pdf](/br2/sixcms/media.php/68/Schutzw-NEGElsbruch__on__anlg__001Anlage-1.pdf "Schutzwald-NEG-Elsbruch-Anlage 1.pdf") 833.5 KB

2

[Schutzwald-NEG-Elsbruch-Anlage 2.pdf](/br2/sixcms/media.php/68/Schutzw-NEGElsbruch__on__anlg__002Anlage-2.pdf "Schutzwald-NEG-Elsbruch-Anlage 2.pdf") 671.2 KB

3

[Schutzwald-NEG-Elsbruch-Anlage 3.pdf](/br2/sixcms/media.php/68/Schutzw-NEGElsbruch__on__anlg__003Anlage-3.pdf "Schutzwald-NEG-Elsbruch-Anlage 3.pdf") 1.1 MB