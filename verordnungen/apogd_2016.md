## Verordnung über die Ausbildung und Prüfung für die Laufbahn des gehobenen allgemeinen Verwaltungsdienstes im Land Brandenburg (Ausbildungs- und Prüfungsordnung gehobener Dienst - APOgD)

Auf Grund des § 26 Absatz 1 Satz 1 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S. 26), der durch Artikel 1 des Gesetzes vom 5.
Dezember 2013 (GVBl.
I Nr. 36) geändert worden ist, verordnet der Minister des Innern und für Kommunales im Einvernehmen mit dem Minister der Finanzen:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeine Regelungen

[§ 1 Geltungsbereich](#1)

[§ 2 Einstellungsbehörden](#2)

[§ 3 Bewerbung](#3)

[§ 4 Zulassung zur Ausbildung](#4)

[§ 5 Auswahlverfahren](#5)

[§ 6 Einstellung in den Vorbereitungsdienst](#6)

[§ 7 Dauer des Vorbereitungsdienstes](#7)

[§ 8 Rechtsstellung und Pflichten während des Vorbereitungsdienstes, Urlaub](#8)

### Abschnitt 2  
Ausbildung

[§ 9 Ziel der Ausbildung](#9)

[§ 10 Gliederung der Ausbildung, Studium](#10)

[§ 11 Module, Leistungspunkte](#11)

[§ 12 Grundsätze des fachwissenschaftlichen Studiums](#12)

[§ 13 Inhalt des fachwissenschaftlichen Studiums](#13)

[§ 14 Wahlpflichtstudium](#14)

[§ 15 Grundsätze der berufspraktischen Studienzeiten](#15)

[§ 16 Ablauf der berufspraktischen Studienzeiten, Ausbildungsstellen](#16)

[§ 17 Leistungsnachweise und Bewertungen während des fachwissenschaftlichen Studiums](#17)

[§ 18 Praktische Arbeiten und Bewertungen während der berufspraktischen Studienzeiten](#18)

### Abschnitt 3  
Aufstieg

[§ 19 Aufstiegsstudium](#19)

[§ 20 Kürzung von Studienzeiten](#20)

### Abschnitt 4  
Prüfungen, Laufbahnbefähigung

[§ 21 Aufgaben des Prüfungsausschusses](#21)

[§ 22 Aufgaben und Zusammensetzung der Prüfungskommissionen](#22)

[§ 23 Bachelor-Thesis](#23)

[§ 24 Mündliche Abschlussprüfung](#24)

[§ 25 Laufbahnprüfung](#25)

[§ 26 Ergebnis der Laufbahnprüfung](#26)

### Abschnitt 5  
Gemeinsame Vorschriften

[§ 27 Wiederholung von Prüfungen](#27)

[§ 28 Prüfungserleichterungen](#28)

[§ 29 Fernbleiben, Rücktritt und Prüfungsverlängerung](#29)

[§ 30 Unlauteres Verhalten im Prüfungsverfahren](#30)

[§ 31 Ausbildungs- und Prüfungsakten](#31)

### Abschnitt 6  
Schlussbestimmung

[§ 32 Inkrafttreten, Außerkrafttreten](#32)

### Abschnitt 1  
Allgemeine Regelungen

#### § 1 Geltungsbereich

Diese Verordnung regelt die Ausbildung und Prüfung für die Laufbahn des gehobenen allgemeinen Verwaltungsdienstes im Land Brandenburg an der Technischen Hochschule Wildau.

#### § 2 Einstellungsbehörden

Einstellungsbehörden im Sinne dieser Verordnung sind:

1.  für die Landesverwaltung das für Inneres zuständige Ministerium und
2.  für die Verwaltung der Gemeinden und Gemeindeverbände
    1.  die Landkreise,
    2.  die kreisfreien Städte,
    3.  die amtsfreien kreisangehörigen Gemeinden,
    4.  die Ämter sowie
    5.  die kommunalen Zweckverbände mit Dienstherrnfähigkeit.

#### § 3 Bewerbung

(1) Bewerbungen für die Ausbildung im Sinne von § 1 sind an die Einstellungsbehörde zu richten.

(2) Die Einstellungsbehörde kann verlangen, dass der Bewerbung insbesondere folgende Nachweise und Unterlagen beizufügen sind:

1.  ein Lebenslauf,
2.  der Nachweis einer Hochschulzugangsberechtigung im Sinne des Brandenburgischen Hochschulgesetzes vom 28.
    April 2014 (GVBl.
    I Nr. 18), das durch Artikel 2 des Gesetzes vom 1.
    Juli 2015 (GVBl.
    I Nr. 18) geändert worden ist, in der jeweils geltenden Fassung,
3.  Nachweise über etwaige berufliche Tätigkeiten und Prüfungen und
4.  eine Einverständniserklärung der gesetzlichen Vertretung, falls die sich bewerbende Person nicht volljährig ist.

(3) Die Einstellungsbehörde kann die Beibringung der in Absatz 2 genannten Unterlagen auf elektronischem Weg fordern.
Sie kann die Teilnahme am Auswahlverfahren nach § 5 davon abhängig machen, dass die einzureichenden Unterlagen fristgerecht und vollständig beigebracht werden.

#### § 4 Zulassung zur Ausbildung

(1) Zur Ausbildung kann zugelassen werden, wer

1.  im Auswahlverfahren nach § 5 ausgewählt worden ist,
2.  eine Hochschulzugangsberechtigung im Sinne des Brandenburgischen Hochschulgesetzes besitzt und
3.  die persönlichen Voraussetzungen für die Berufung in das Beamtenverhältnis erfüllt.

(2) Zur Ausbildung kann auch zugelassen werden, wer die Voraussetzung nach Absatz 1 Nummer 2 erst zum Zeitpunkt der Einstellung in den Vorbereitungsdienst erfüllen wird.

(3) Die Entscheidung über die Zulassung zur Ausbildung trifft die jeweilige Einstellungsbehörde für ihren Bereich.
Sie kann die Zulassung auf Inhaberinnen und Inhaber bestimmter Hochschulzugangsberechtigungen im Sinne des Brandenburgischen Hochschulgesetzes beschränken.

#### § 5 Auswahlverfahren

(1) Vor der Entscheidung über die Zulassung zur Ausbildung wird in einem Auswahlverfahren festgestellt, ob die Bewerberinnen und Bewerber aufgrund ihrer Kenntnisse, Fähigkeiten und persönlichen Eigenschaften für die Einstellung in den Vorbereitungsdienst des gehobenen allgemeinen Verwaltungsdienstes im Land Brandenburg geeignet sind.

(2) Dem Auswahlverfahren nach Absatz 1 kann eine Vorauswahl vorausgehen, die sich an der Eignung der Bewerberinnen und Bewerber zu orientieren hat.
Die Einstellungsbehörde kann die Vorauswahl nach Satz 1 insbesondere auf der Grundlage der Durchschnittsnote des letzten Schul- oder Berufsschulzeugnisses oder der letzten beiden Schul- oder Berufsschulzeugnisse oder auf der Grundlage der Noten einzelner Fächer des letzten Schul- oder Berufsschulzeugnisses treffen.
Hierzu kann sie die Beibringung des letzten Schul- oder Berufsschulzeugnisses oder der letzten beiden Schul- oder Berufsschulzeugnisse verlangen.

(3) Das Auswahlverfahren bestimmt die jeweilige Einstellungsbehörde für ihren Bereich.
Sie hat für Bewerberinnen und Bewerber desselben Einstellungstermins das gleiche Auswahlverfahren anzuwenden.

(4) Wer nicht zum Auswahlverfahren zugelassen wird oder im Auswahlverfahren erfolglos bleibt, ist spätestens einen Monat vor dem Einstellungsdatum schriftlich zu benachrichtigen.

#### § 6 Einstellung in den Vorbereitungsdienst

(1) Die jeweilige Einstellungsbehörde entscheidet unter Berücksichtigung des Ergebnisses des Auswahlverfahrens nach § 5 über die Einstellung der Bewerberinnen und Bewerber in den Vorbereitungsdienst.

(2) Vor der Einstellung in den Vorbereitungsdienst müssen folgende Unterlagen vorliegen:

1.  eine Geburtsurkunde,
2.  ein amtsärztliches Gesundheitszeugnis,
3.  eine Erklärung über etwaige Bestrafungen oder anhängige Ermittlungs- oder Strafverfahren und
4.  ein behördliches Führungszeugnis.

(3) Die ausgewählten Bewerberinnen und Bewerber werden in der Regel zum 1.
September des jeweiligen Kalenderjahres eingestellt.

#### § 7 Dauer des Vorbereitungsdienstes

(1) Der Vorbereitungsdienst umfasst die Ausbildung und die Prüfung und dauert in der Regel drei Jahre und sechs Monate.

(2) Der Vorbereitungsdienst kann durch Anrechnung von Zeiten verkürzt werden, die die Bewerberin oder der Bewerber bereits bei einem anderen Dienstherrn in einem gleichwertigen Vorbereitungsdienst für den allgemeinen Verwaltungsdienst abgeleistet hat.
Die Entscheidung über die Anrechnung von Zeiten nach Satz 1 trifft die Laufbahnordnungsbehörde.
Die Anrechnung von Studien- oder Prüfungsleistungen sowie außerhalb des Hochschulwesens erworbenen Kenntnissen richtet sich nach den entsprechenden Bestimmungen des Brandenburgischen Hochschulgesetzes.
Erfolgt eine Anrechnung im Sinne des Satzes 1 oder des Satzes 3, entscheidet die Laufbahnordnungsbehörde über die Verwendung der oder des Studierenden während der aufgrund der Anrechnung frei werdenden Studienzeiten.
Eine Verkürzung des Vorbereitungsdienstes erfolgt um längstens sechs Monate und ist unzulässig, wenn das Erreichen des Ausbildungsziels gefährdet erscheint.
Die Kürzung von Studienzeiten im Fall eines Aufstiegs richtet sich nach § 20.

(3) Wird die Ausbildung wegen Krankheit oder aus anderen zwingenden Gründen unterbrochen, können Ausbildungsabschnitte gekürzt oder verlängert und Abweichungen vom Studien- und Ausbildungsplan zugelassen werden, um eine zielgerichtete Fortsetzung des Vorbereitungsdienstes zu ermöglichen.

(4) Der Vorbereitungsdienst ist im Einzelfall zu verlängern, wenn die Ausbildung

1.  wegen einer länger als sechs Wochen andauernden Krankheit,
2.  wegen eines Beschäftigungsverbotes nach § 3 des Mutterschutzgesetzes vom 23. Mai 2017 (BGBl. I S. 1228), in der jeweils geltenden Fassung,
3.  wegen einer Elternzeit nach den §§ 15 und 16 des Bundeselterngeld- und Elternzeitgesetzes vom 5.
    Dezember 2006 (BGBl.
    I S.
    2748), das zuletzt durch Artikel 6 Absatz 9 des Gesetzes vom 23.
    Mai 2017 (BGBl.
    I S.
    1228, 1241) geändert worden ist, in der jeweils geltenden Fassung oder
4.  aus anderen zwingenden Gründen

unterbrochen wurde und die zielgerichtete Fortsetzung des Vorbereitungsdienstes nicht gewährleistet ist.

(5) Der Vorbereitungsdienst kann in den Fällen des Absatzes 4 Nummer 1 und 4 höchstens zweimal und bis zu insgesamt nicht mehr als 24 Monaten verlängert werden.
Die oder der Betroffene ist vorher anzuhören.
In begründeten Einzelfällen können Ausnahmen von Satz 1 zugelassen werden, wenn die Versagung einer weiteren Verlängerung eine unbillige Härte darstellen würde.

(6) Entscheidungen nach den Absätzen 3 bis 5 trifft die Laufbahnordnungsbehörde.

(7) Bei Nichtbestehen von Prüfungsleistungen richtet sich die Verlängerung des Vorbereitungsdienstes nach § 27.

#### § 8 Rechtsstellung und Pflichten während des Vorbereitungsdienstes, Urlaub

(1) Die im Rahmen des Auswahlverfahrens nach § 5 ausgewählten Bewerberinnen und Bewerber werden unter Berufung in das Beamtenverhältnis auf Widerruf in den Vorbereitungsdienst eingestellt.
Sie führen die Dienstbezeichnung „Inspektoranwärterin“ oder „Inspektoranwärter“ mit dem für den Dienstherrn maßgeblichen Zusatz.

(2) Die Einstellungsbehörde kann abweichend von Absatz 1 die Ausbildung im Sinne des § 1 außerhalb eines Vorbereitungsdienstes durchführen.
Sie kann hierzu mit der ausgewählten Bewerberin oder dem ausgewählten Bewerber einen Vertrag für das Studium im Beschäftigungsverhältnis abschließen.

(3) Während des Vorbereitungsdienstes unterstehen die ausgewählten Bewerberinnen und Bewerber der Dienstaufsicht der Einstellungsbehörde.

(4) Die ausgewählten Bewerberinnen und Bewerber sind zur Teilnahme an sämtlichen Lehrveranstaltungen des Studiengangs „Öffentliche Verwaltung Brandenburg“ an der Technischen Hochschule Wildau und an sonstigen von ihrem Dienstherrn bestimmten Veranstaltungen verpflichtet.
Sie sind ferner zur Ablegung der vorgesehenen Modulprüfungen sowie zu eigenverantwortlichem Selbststudium verpflichtet.
Für die ausgewählten Bewerberinnen und Bewerber der Einstellungsbehörde nach § 2 Nummer 1 gilt die Arbeitszeitverordnung vom 16. September 2009 (GVBl. II S.
614), die zuletzt durch Verordnung vom 13.
Oktober 2015 (GVBl.
II Nr.
51) geändert worden ist, in der jeweils geltenden Fassung.
Erholungsurlaub ist grundsätzlich in der lehrveranstaltungsfreien Studienzeit in Anspruch zu nehmen und soll drei zusammenhängende Wochen nicht überschreiten.
Prüfungszeiten stehen Vorlesungszeiten gleich.

(5) Das Beamtenverhältnis auf Widerruf endet mit dem Ablauf des Tages, an dem

1.  das Bestehen oder
2.  das endgültige Nichtbestehen der Laufbahnprüfung gemäß § 25

bekannt gegeben worden ist.
Im Fall von Satz 1 Nummer 1 endet das Beamtenverhältnis jedoch frühestens nach Ablauf der für den Vorbereitungsdienst im Allgemeinen oder im Einzelfall festgesetzten Zeit.

### Abschnitt 2  
Ausbildung

#### § 9 Ziel der Ausbildung

Ziel der Ausbildung im Sinne des § 1 ist es, die Befähigung für die Laufbahn des gehobenen allgemeinen Verwaltungsdienstes im Land Brandenburg zu vermitteln.

#### § 10 Gliederung der Ausbildung, Studium

(1) Die Ausbildung gliedert sich in eine Einführungsphase in der Landes- oder Kommunalverwaltung und ein duales Bachelor-Studium „Öffentliche Verwaltung Brandenburg“ an der Technischen Hochschule Wildau.

(2) Die Einführungsphase nach Absatz 1 soll den ausgewählten Bewerberinnen und Bewerbern einen Überblick über die Strukturen und Verwaltungsabläufe in der öffentlichen Verwaltung des Landes Brandenburg verschaffen.

(3) Das Bachelor-Studium nach Absatz 1 umfasst sieben Studienhalbjahre und wird in folgenden Abschnitten durchgeführt:

1.  fachwissenschaftliches Grundlagenstudium (1.
    bis 3.
    Studienhalbjahr),
2.  Praktikum I und II (4.
    Studienhalbjahr),
3.  fachwissenschaftliches Vertiefungsstudium und Wahlpflichtstudium (5.
    und erste Hälfte 6. Studienhalbjahr) und
4.  Praktikum III und IV, Bachelor-Thesis, mündliche Abschlussprüfung (zweite Hälfte 6. Studienhalbjahr und 7.
    Studienhalbjahr).

(4) Fachwissenschaftliche und berufspraktische Studienzeiten (Praktikum I bis IV) bilden eine Einheit.

(5) Mit dem erfolgreichen Abschluss des Studiums „Öffentliche Verwaltung Brandenburg“ erwerben die Studierenden, unabhängig vom Status als Beamtin oder Beamter auf Widerruf, den akademischen Grad „Bachelor of Laws (LL.B.)“ und die Laufbahnbefähigung für den gehobenen allgemeinen Verwaltungsdienst im Land Brandenburg.

#### § 11 Module, Leistungspunkte

(1) Das Studium nach § 10 Absatz 1 gliedert sich in thematisch und zeitlich abgeschlossene Studieneinheiten (Module), die sich aus verschiedenen Lehr- und Lernformen zusammensetzen können.
Module schließen mit einer studienbegleitenden Modulprüfung in Form eines Leistungsnachweises nach § 17 Absatz 2 Satz 1 ab.

(2) Für Module, deren Prüfungen bestanden wurden, werden Leistungspunkte nach dem Europäischen System zur Übertragung und Akkumulierung von Studienleistungen (ECTS) vergeben.
Die Anzahl der Leistungspunkte, die für bestandene Modulprüfungen erreicht werden können, sind von der Hochschule durch Satzung festzulegen.

(3) Während des gesamten Studiums sind insgesamt 210 Leistungspunkte nach dem ECTS zu erreichen.
Mit den Leistungspunkten ist keine qualitative Bewertung der Studienleistungen verbunden.

#### § 12 Grundsätze des fachwissenschaftlichen Studiums

(1) Die Lerninhalte der fachwissenschaftlichen Studienabschnitte sind nach wissenschaftlichen Erkenntnissen und Methoden anwendungsorientiert auf dem aktuellen Stand des Fachs zu vermitteln.
Die Lehrveranstaltungen sollen in jedem Fachgebiet aktuelle Bezüge zur Verwaltungspraxis im Land Brandenburg aufweisen.

(2) Unbeschadet der Regelung des § 26 Absatz 2 des Landesbeamtengesetzes regelt die Hochschule das Nähere zum Inhalt und Ablauf des fachwissenschaftlichen Studiums unter Beachtung der Bestimmungen dieser Verordnung durch Satzung im Einvernehmen mit dem für Inneres zuständigen Ministerium.
Den kommunalen Spitzenverbänden wird die Möglichkeit zur Stellungnahme gegeben.

#### § 13 Inhalt des fachwissenschaftlichen Studiums

(1) Die Lehrveranstaltungen an der Hochschule umfassen Studieninhalte aus den Fachgebieten Rechtswissenschaften, Wirtschaftswissenschaften sowie Sozial- und Verwaltungswissenschaften.

(2) Die Studieninhalte aus dem Fachgebiet Rechtswissenschaften umfassen insbesondere folgende Schwerpunkte:

1.  allgemeines und besonderes Verwaltungsrecht,
2.  Verfassungsrecht,
3.  Grundlagen des Privatrechts und
4.  Grundlagen des Europarechts.

(3) Die Studieninhalte aus dem Fachgebiet Wirtschaftswissenschaften umfassen insbesondere folgende Schwerpunkte:

1.  Verwaltungsbetriebswirtschaft und
2.  öffentliche Finanzwirtschaft.

(4) Die Studieninhalte aus dem Fachgebiet Sozial- und Verwaltungswissenschaften umfassen insbesondere folgende Schwerpunkte:

1.  Verwaltungslehre,
2.  Soziologie und
3.  Politologie.

(5) Die Hochschule hat in den Lehrveranstaltungen der in Absatz 1 genannten Fachgebiete in hinreichendem Umfang die Besonderheiten der Kommunalverwaltung zu berücksichtigen, sofern thematische Bezüge zu den in den Absätzen 2 bis 4 genannten Schwerpunkten vorhanden sind.

#### § 14 Wahlpflichtstudium

(1) Für das sechste Studienhalbjahr haben die Studierenden fünf Wahlpflichtmodule zu wählen, wobei jeweils mindestens ein Wahlpflichtmodul aus den Fachgebieten Rechtswissenschaften, Wirtschaftswissenschaften sowie Sozial- und Verwaltungswissenschaften auszuwählen ist.
Wahlpflichtmodule sollen der Vertiefung bereits vorhandener und der Vermittlung neuer Kenntnisse und Fähigkeiten dienen.
Die Wahlmöglichkeit kann durch Vorgaben der jeweiligen Einstellungsbehörde beschränkt werden.

(2) § 12 Absatz 2 gilt für das Wahlpflichtstudium entsprechend.

#### § 15 Grundsätze der berufspraktischen Studienzeiten

(1) Die berufspraktischen Studienzeiten sollen berufliche Kenntnisse und Erfahrungen vermitteln.
Die Studierenden sollen während der Praxisphasen die während des fachwissenschaftlichen Studiums erworbenen Kenntnisse vertiefen und insbesondere lernen, diese in der Praxis anzuwenden.
Die praktischen Unterweisungen in den Ausbildungsstellen sollen systematisch und didaktisch effektiv auf die Inhalte des fachwissenschaftlichen Studiums abgestimmt sein.

(2) § 12 Absatz 2 gilt für die berufspraktischen Studienzeiten entsprechend.

#### § 16 Ablauf der berufspraktischen Studienzeiten, Ausbildungsstellen

(1) Die berufspraktischen Studienzeiten gliedern sich in vier jeweils drei Monate dauernde Praxisabschnitte, die wahlweise in folgenden Bereichen der öffentlichen Verwaltung des Landes Brandenburg absolviert werden können:

1.  Eingriffsverwaltung,
2.  Leistungsverwaltung,
3.  Querschnittsverwaltung,
4.  Sonstige Fachverwaltung.

Jeweils ein Praxisabschnitt soll in der Eingriffs- oder Leistungsverwaltung und ein weiterer in der Querschnittsverwaltung abgeleistet werden.
Studierende, die von der Einstellungsbehörde nach § 2 Nummer 1 zur Ausbildung zugelassen worden sind, sollen die Praxisabschnitte im Sinne des Satzes 1 in verschiedenen Geschäftsbereichen und auf unterschiedlichen Stufen der Landesverwaltung ableisten; ein Praxisabschnitt soll in der Kommunalverwaltung abgeleistet werden.

(2) Studierende, die von einer der Einstellungsbehörden im Sinne des § 2 Nummer 2 zur Ausbildung zugelassen worden sind, sollen einen Praxisabschnitt im Bereich der Landesverwaltung absolvieren.

(3) Die Einstellungsbehörde kann zulassen, dass ein Praxisabschnitt in einer bundesdeutschen oder ausländischen Verwaltung, in der Privatwirtschaft oder bei Verbänden absolviert wird.
Bei Ableistung eines Praxisabschnitts außerhalb der öffentlichen Verwaltung des Landes Brandenburg muss die in der jeweiligen Ausbildungsstelle stattfindende Ausbildung einen unmittelbaren Bezug zur öffentlichen Verwaltung aufweisen und die Vergleichbarkeit der Leistungsbewertung mit Ausbildungsstellen in der öffentlichen Verwaltung des Landes Brandenburg gegeben sein.
Absatz 5 gilt entsprechend.

(4) Die Abordnung zu den Ausbildungsstellen erfolgt durch die Einstellungsbehörde.
Dabei sind Wünsche der Anwärterinnen und Anwärter nach Möglichkeit zu berücksichtigen.

(5) Die praktischen Unterweisungen in den Ausbildungsstellen erfolgen durch Ausbilderinnen und Ausbilder, die mindestens über einen Bachelor- oder einen gleichwertigen Abschluss sowie eine Ausbildungszertifizierung verfügen müssen.
Die Ausbildungszertifizierung nach Satz 1 wird im Rahmen eines Lehrgangs an der Technischen Hochschule Wildau erworben.

(6) Zur Ergänzung der berufspraktischen Studienzeiten können praxisbegleitende Arbeitsgemeinschaften eingerichtet werden.
Sie dienen der Ergänzung der praktischen Unterweisungen in den Ausbildungsstellen.

#### § 17 Leistungsnachweise und Bewertungen während des fachwissenschaftlichen Studiums

(1) Leistungsnachweise sollen die fachliche Qualifikation, die Eigeninitiative sowie die schriftliche und mündliche Ausdrucksfähigkeit darstellen.
Die Benotung der Leistungsnachweise soll den Leistungsstand der Studierenden zum Prüfungszeitpunkt zuverlässig und vergleichbar abbilden.

(2) Als Formen der Leistungsnachweise kommen insbesondere Klausuren, Hausarbeiten, Aktenvorträge, Referate, Fallbearbeitungen, Projektleistungen, Praxisberichte und mündliche Prüfungen in Betracht.
Aus den in § 13 Absatz 1 genannten Fachgebieten sollen drei Klausuren mit einer Bearbeitungszeit von jeweils mindestens vier Stunden gestellt werden.
Mindestens eine dieser Klausuren muss einen rechtswissenschaftlichen Schwerpunkt und die Form einer juristischen Fallbearbeitung aufweisen.

(3) Die Leistungsnachweise sind mit folgenden Noten und den zu ihrer Differenzierung vorgesehenen Zwischennoten zu bewerten:

sehr gut (1,0)

\=

HERVORRAGEND – ausgezeichnete Leistungen und nur wenige unbedeutende Fehler,

sehr gut (1,3)

\=

SEHR GUT – überdurchschnittliche Leistungen, aber einige Fehler,

gut (1,7 oder 2,0 oder 2,3)

\=

GUT – insgesamt gute und solide Arbeit, jedoch mit einigen grundlegenden Fehlern,

befriedigend (2,7 oder 3,0 oder 3,3)

\=

BEFRIEDIGEND – mittelmäßig, jedoch mit deutlichen Mängeln,

ausreichend (3,7 oder 4,0)

\=

AUSREICHEND – die gezeigten Leistungen entsprechen den Mindestanforderungen und

nicht ausreichend (5,0)

\=

NICHT AUSREICHEND – es sind Verbesserungen erforderlich, bevor die Leistungen anerkannt werden können.

Andere Noten oder Zwischennoten dürfen nicht vergeben werden.

(4) § 12 Absatz 2 gilt für die von den Studierenden zu erbringenden Leistungsnachweise sowie die Bewertungen während des fachwissenschaftlichen Studiums entsprechend.

#### § 18 Praktische Arbeiten und Bewertungen während der berufspraktischen Studienzeiten

(1) Die Studierenden haben während der berufspraktischen Studienzeiten in den Ausbildungsbehörden vielschichtige Verwaltungsvorgänge unter Anwendung der in Betracht kommenden Rechts- und Verwaltungsvorschriften sowie unter Berücksichtigung fachlicher, betriebswirtschaftlicher und politischer Gesichtspunkte bis zur Entscheidungsreife zu bearbeiten.

(2) Über die Studierenden ist bei Beendigung eines jeden Praxisabschnitts von der jeweiligen Ausbilderin oder dem jeweiligen Ausbilder eine Beurteilung über die Praxisleistung abzugeben.
Diese muss erkennen lassen, ob das Ziel des jeweiligen Praxisabschnitts erreicht wurde.
Sie ist mit der oder dem Studierenden zu besprechen.
Für die Benotung der Praxisleistung gilt § 17 Absatz 3 entsprechend.
Ist zu erwarten, dass die Leistungen in einem Praxisabschnitt mit „nicht ausreichend“ (5,0) zu bewerten sind, soll die oder der Studierende spätestens sechs Wochen vor dem Ende dieser Zeit auf ihren oder seinen Leistungsstand und die sich daraus ergebenden Folgen hingewiesen werden.

(3) Zum Abschluss der Praxisabschnitte I, II und III haben die Studierenden jeweils einen Leistungsnachweis in Form eines Praxisberichts und einer hierzu passenden Praxispräsentation zu erbringen.
Für die Benotung dieser Leistungsnachweise gilt § 17 Absatz 3 entsprechend.
Die Abnahme und Bewertung des Praxisberichts und der Praxispräsentation erfolgt durch die ausbildende Person.
Die Anwesenheit einer Beisitzerin oder eines Beisitzers ist zu gewährleisten.
Die Beisitzerin oder der Beisitzer muss über einen Bachelor-Abschluss beziehungsweise einen gleichwertigen Abschluss verfügen.
Aus dem Praxisbericht, der Praxispräsentation und der Beurteilung über die Praxisleistung ist eine Gesamtnote zu bilden.
Diese errechnet sich zu jeweils gleichen Teilen aus den Noten der in Satz 6 genannten Teilleistungen.

(4) Für den Praxisabschnitt IV entfällt der Praxisbericht.
Eine Praxispräsentation gemäß Absatz 3 ist zu erbringen.
Aus der Praxispräsentation und der Beurteilung über die Praxisleistung ist eine Gesamtnote zu bilden.
Sie errechnet sich zu jeweils gleichen Teilen aus den Noten der Praxispräsentation und der Beurteilung über die Praxisleistung.
Absatz 3 Satz 2 bis 5 gilt für die Praxispräsentation und die Beurteilung über die Praxisleistung entsprechend.

(5) Die Zuständigkeit für die Qualitätssicherung während der berufspraktischen Studienzeiten wird von den Einstellungsbehörden und der Technischen Hochschule Wildau gemeinsam wahrgenommen.
Die jeweilige Einstellungsbehörde sichert die Qualität während der praktischen Tätigkeiten und die Technische Hochschule Wildau gewährleistet über die Zertifizierung und Re-Zertifizierung gleichmäßige Qualitätsstandards bei den Ausbilderinnen und Ausbildern in den Einstellungsbehörden.

(6) § 12 Absatz 2 gilt für die praktischen Arbeiten und Bewertungen während der berufspraktischen Studienzeiten entsprechend.

### Abschnitt 3  
Aufstieg

#### § 19 Aufstiegsstudium

(1) Beamtinnen und Beamte des mittleren allgemeinen Verwaltungsdienstes, die zum Regelaufstieg in die Laufbahn des gehobenen allgemeinen Verwaltungsdienstes zugelassen worden sind, nehmen gemeinsam mit den Studierenden an der Ausbildung im Sinne des § 1 teil.

(2) Sind studienbegleitende Modulprüfungen, die Bachelor-Thesis oder die mündliche Abschlussprüfung endgültig nicht bestanden, endet die Aufstiegsausbildung mit dem Tag der Bekanntgabe des Prüfungsergebnisses.

(3) Nach bestandenem Aufstiegsstudium bleiben die Beamtinnen und Beamten bis zur Verleihung eines Amtes des gehobenen Dienstes in ihrer bisherigen Rechtsstellung.

#### § 20 Kürzung von Studienzeiten

(1) Fachwissenschaftliche und berufspraktische Studienzeiten können um insgesamt sechs Monate gekürzt werden, sofern die Beamtinnen und Beamten während ihrer bisherigen Tätigkeit schon einen Teil der Kenntnisse erworben haben, die für die neue Laufbahn gefordert werden.
Kürzungen sind nur zulässig, wenn das Erreichen des Ausbildungsziels nicht gefährdet erscheint.
Die Entscheidung über die Kürzung von Studienzeiten trifft die Laufbahnordnungsbehörde.

(2) Bei Kürzungen nach Absatz 1 können Abweichungen vom Studien- oder Ausbildungsplan zugelassen werden.
Zusammenhängende Teile des fachwissenschaftlichen Studiums und der berufspraktischen Studienzeiten sollen grundsätzlich ungekürzt durchlaufen werden.

### Abschnitt 4  
Prüfungen, Laufbahnbefähigung

#### § 21 Aufgaben des Prüfungsausschusses

(1) Die Organisation und Durchführung des Prüfungsverfahrens im Studiengang „Öffentliche Verwaltung Brandenburg“ obliegt dem Prüfungsausschuss des Fachbereichs der Technischen Hochschule Wildau, dem der Studiengang angegliedert ist, in Abstimmung mit der Laufbahnordnungsbehörde.
Vertreterinnen oder Vertreter der Laufbahnordnungsbehörde haben das Recht, an Sitzungen des Prüfungsausschusses, die den Studiengang „Öffentliche Verwaltung Brandenburg“ betreffen, teilzunehmen; sie haben Rede- und Antragsrecht und sind über den Studiengang betreffende Beschlüsse des Prüfungsausschusses unverzüglich zu informieren.
Erachten die Vertreterinnen oder Vertreter der Laufbahnordnungsbehörde Beschlüsse des Prüfungsausschusses, die den Studiengang „Öffentliche Verwaltung Brandenburg“ betreffen, für rechtswidrig, können sie diese beanstanden; die Beanstandung hat aufschiebende Wirkung.

(2) Der Prüfungsausschuss bestellt die Prüferinnen und Prüfer.
Zur Prüferin oder zum Prüfer kann bestellt werden, wer das betreffende Modul oder das Prüfungsfach hauptberuflich an der Hochschule lehrt oder mindestens die durch die Prüfung festzustellende oder eine gleichwertige Qualifikation besitzt.
Professorinnen und Professoren können für alle Prüfungen ihres Fachgebiets zu Prüfenden bestellt werden.
Akademische Mitarbeiterinnen und Mitarbeiter und Lehrbeauftragte können für den in ihren Lehrveranstaltungen dargebotenen Prüfungsstoff zu Prüfenden bestellt werden.
Zu Prüfenden können auch Personen anderer Fachbereiche der Technischen Hochschule Wildau oder einer anderen Hochschule sowie fachlich geeignete Bedienstete der Landesverwaltung bestellt werden, sofern sie mindestens die durch die Prüfung festzustellende oder eine gleichwertige Qualifikation besitzen.

(3) Die Mitglieder des Prüfungsausschusses sind zur Verschwiegenheit über alle Angelegenheiten des Prüfungsverfahrens verpflichtet.

#### § 22 Aufgaben und Zusammensetzung der Prüfungskommissionen

(1) Der Prüfungsausschuss setzt Prüfungskommissionen ein, die die mündliche Abschlussprüfung im Sinne des § 24 abnehmen.

(2) Den Vorsitz der Kommission führt jeweils ein vom Prüfungsausschuss bestelltes Mitglied aus der Gruppe der Professorinnen und Professoren des Fachbereichs der Technischen Hochschule Wildau, dem der Studiengang „Öffentliche Verwaltung Brandenburg“ angegliedert ist.

(3) Jede Prüfungskommission besteht aus:

1.  der oder dem Vorsitzenden und
2.  einer weiteren Prüferin oder einem weiteren Prüfer.

Der oder die Vorsitzende ist die Betreuerin oder der Betreuer der Bachelor-Thesis.
Über Ausnahmen entscheidet der Prüfungsausschuss von Amts wegen oder auf Antrag der oder des Studierenden.
Ausnahmen sind nur aus zwingendem Grund zuzulassen.

(4) Die Einstellungsbehörden haben jederzeit das Recht, an der Prüfung teilzunehmen.

(5) Die Prüfer einigen sich auf eine Note für jeden Prüfungsabschnitt.
Können sich die Prüfer ausnahmsweise nicht einigen, wird das arithmetische Mittel gebildet.

(6) Die Mitglieder der Prüfungskommission sind bei der Beurteilung von Prüfungsleistungen den allgemein gültigen Bewertungsmaßstäben unterworfen; sie sind nicht an Weisungen gebunden.
Sie sind zur Verschwiegenheit über alle mit der Prüfung zusammenhängenden Vorgänge und Beratungen verpflichtet.

#### § 23 Bachelor-Thesis

(1) Die Studierenden haben am Ende des Studiums eine schriftliche Prüfungsarbeit (Bachelor-Thesis) zu erstellen, mit der sie ihre Befähigung nachweisen, in einer vorgegebenen Frist eine für die Studienziele relevante Problemstellung unter enger Verknüpfung der theoretisch und praktisch erworbenen Kenntnisse mit wissenschaftlichen Methoden selbstständig zu bearbeiten.
Das Thema der Arbeit wird von der oder dem vom Prüfungsausschuss bestimmten Erstprüfenden festgelegt.
Die oder der Studierende hat die Möglichkeit, ein Thema ihrer oder seiner Wahl vorzuschlagen.
Es soll einen unmittelbaren Bezug zu den von der oder dem Studierenden abgeleisteten berufspraktischen Studienzeiten aufweisen.

(2) Mit der Bachelor-Thesis haben die Studierenden eine schriftliche Erklärung darüber abzugeben, dass die Bachelor-Thesis selbstständig verfasst wurde, nur die angegebenen Quellen und Hilfsmittel benutzt und alle Stellen der Arbeit, die wörtlich oder sinngemäß aus anderen Quellen übernommen wurden, als solche kenntlich gemacht wurden und die Bachelor-Thesis in gleicher oder ähnlicher Form noch keiner Prüfungsbehörde vorgelegt worden ist.
Für die Bachelor-Thesis ist die Note „nicht ausreichend“ (5,0) zu erteilen, wenn die oder der Studierende eine falsche schriftliche Erklärung abgegeben hat.

(3) Die Bearbeitungszeit für die Bachelor-Thesis beträgt neun Wochen.

#### § 24 Mündliche Abschlussprüfung

(1) Das Bachelor-Studium schließt mit einer mündlichen Abschlussprüfung ab.
Sie besteht aus einem Kolloquium, das eine mündliche Präsentation der wesentlichen Thesen und Inhalte der Bachelor-Thesis enthält.
An die Präsentation schließt sich die Verteidigung der Thesen und Inhalte an.

(2) Zur mündlichen Abschlussprüfung wird zugelassen, wer die Modulprüfungen an der Hochschule, die berufspraktischen Studienzeiten und die Bachelor-Thesis mindestens mit der Note „ausreichend“ (4,0) bestanden hat.

(3) Die Dauer der Prüfung soll für jede Studierende und jeden Studierenden insgesamt 60 Minuten betragen.

(4) Die Prüfung wird im Regelfall als Einzelprüfung durchgeführt.

(5) Die mündliche Abschlussprüfung ist bestanden, wenn mindestens die Note „ausreichend“ (4,0) erreicht wurde.

#### § 25 Laufbahnprüfung

(1) Die Laufbahnprüfung besteht aus sämtlichen Modulprüfungen, den berufspraktischen Studienzeiten, der Bachelor-Thesis und der mündlichen Abschlussprüfung nach § 24.

(2) Die Laufbahnprüfung ist bestanden, wenn alle nach Absatz 1 vorgeschriebenen Prüfungsteile mit mindestens der Note „ausreichend“ (4,0) bewertet wurden.

(3) Mit dem Bestehen der Laufbahnprüfung wird kein Rechtsanspruch auf Übernahme in den öffentlichen Dienst erworben.

#### § 26 Ergebnis der Laufbahnprüfung

Im Anschluss an die mündliche Abschlussprüfung nach § 24 ist die Gesamtnote vom Prüfungsausschuss festzustellen.
Sie wird gebildet

1.  zu 50 Prozent aus der Teilgesamtnote der fachwissenschaftlichen Module,
2.  zu 25 Prozent aus der Teilgesamtnote der berufspraktischen Module,
3.  zu 15 Prozent aus der Note des schriftlichen Teils der Bachelor-Thesis und
4.  zu 10 Prozent aus der Note der mündlichen Abschlussprüfung.

Die Teilgesamtnoten der fachwissenschaftlichen und der berufspraktischen Module errechnen sich als gewogenes arithmetisches Mittel der Noten für die zugehörigen Module, wobei die zugeordneten Leistungspunkte die Gewichte darstellen.
Bei den Teilgesamtnoten und bei der Gesamtnote wird eine zweite Dezimalstelle nicht berücksichtigt.

### Abschnitt 5  
Gemeinsame Vorschriften

#### § 27 Wiederholung von Prüfungen

Eine mit „nicht ausreichend“ (5,0) bewertete Modulprüfung oder Teilprüfung einer Modulprüfung darf innerhalb einer vom Prüfungsausschuss festzusetzenden Frist zweimal wiederholt werden.
Die Bachelor-Thesis, die mündliche Abschlussprüfung und die Prüfungen der berufspraktischen Studienzeiten dürfen nur einmal wiederholt werden.
Das Nähere regelt die Hochschule.
Das Studium verlängert sich um die Dauer der jeweiligen Wiederholungsprüfung, längstens um ein Jahr.

#### § 28 Prüfungserleichterungen

(1) Schwerbehinderten und gleichgestellten behinderten Studierenden im Sinne von § 2 Absatz 2 und 3 des Neunten Buches Sozialgesetzbuch – Rehabilitation und Teilhabe behinderter Menschen – (Artikel 1 des Gesetzes vom 19.
Juni 2001, BGBl. I S. 1046, 1047), das zuletzt durch Artikel 452 der Verordnung vom 31.
August 2015 (BGBl. I S. 1474) geändert worden ist, in der jeweils geltenden Fassung, sind bei den Prüfungen auf Antrag die ihrer Behinderung angemessenen Erleichterungen zu gewähren.
Die fachlichen Anforderungen dürfen nicht herabgesetzt werden.

(2) Studierenden, die vorübergehend erheblich körperlich beeinträchtigt sind, können bei Modulprüfungen auf Antrag angemessene Erleichterungen gewährt werden.
Absatz 1 Satz 2 gilt entsprechend.

(3) Anträge auf Prüfungserleichterungen sind spätestens einen Monat vor Beginn der Modulprüfung bei dem Prüfungsausschuss einzureichen.
Liegen die Voraussetzungen für die Gewährung einer Prüfungserleichterung erst zu einem späteren Zeitpunkt vor, ist der Antrag unverzüglich zu stellen.
Der Nachweis der Prüfungsbehinderung ist durch ein ärztliches Zeugnis zu führen, das Angaben über Art und Form der notwendigen Prüfungserleichterungen enthalten muss.
Der Prüfungsausschuss kann bestimmen, dass ein amtsärztliches Zeugnis beizubringen ist.

#### § 29 Fernbleiben, Rücktritt und Prüfungsverlängerung

(1) Bleibt eine Studierende oder ein Studierender einer Modulprüfung oder Teilen derselben ohne Zustimmung des Prüfungsausschusses fern oder tritt sie oder er ohne Zustimmung des Prüfungsausschusses von der Prüfung oder einem Teil von ihr zurück, wird die Prüfung oder der betreffende Teil mit der Note „nicht ausreichend“ (5,0) bewertet.

(2) Stimmt der Prüfungsausschuss dem Fernbleiben oder dem Rücktritt zu, gilt die Prüfung oder der betreffende Teil als nicht durchgeführt.
Die Zustimmung darf nur erteilt werden, wenn wichtige Gründe vorliegen, insbesondere, wenn die oder der Studierende aufgrund von Krankheit an der Prüfung oder einem Prüfungsteil nicht teilnehmen kann.
Die oder der Studierende hat das Vorliegen eines wichtigen Grundes unverzüglich gegenüber der oder dem Vorsitzenden des Prüfungsausschusses geltend zu machen und nachzuweisen.
Im Krankheitsfall ist ein ärztliches Zeugnis vorzulegen, aus dem sich die Prüfungsunfähigkeit ergibt und das in der Regel nicht später als am Prüfungstag ausgestellt sein darf.
Der Prüfungsausschuss kann bestimmen, dass ein amtsärztliches Zeugnis beizubringen ist.
Der Krankheit einer oder eines Studierenden steht die Krankheit eines von ihr oder ihm zu versorgenden Kindes oder die Pflege einer oder eines nahen Angehörigen in einer kurzfristig auftretenden Pflegesituation gleich.
In offensichtlichen Fällen kann auf die Vorlage eines ärztlichen Zeugnisses verzichtet werden.

(3) Haben sich Studierende in Kenntnis einer gesundheitlichen Beeinträchtigung oder eines anderen Rücktrittsgrundes einer Modulprüfung unterzogen, kann ein nachträglicher Rücktritt von der bezeichneten Modulprüfung wegen dieses Grundes nicht mehr genehmigt werden.

(4) Für Studierende, die mit Zustimmung des Prüfungsausschusses einer Modulprüfung oder Teilen derselben ferngeblieben oder davon zurückgetreten sind, bestimmt der Prüfungsausschuss eine Nachprüfung.
Bereits abgelegte Teile der Modulprüfung werden bei der Nachprüfung angerechnet.
Eine nicht oder nicht vollständig abgelegte mündliche Prüfung ist in vollem Umfang nachzuholen.

(5) Die Bearbeitungszeit für die Bachelor-Thesis verlängert sich auf Antrag um Zeiten, in denen die oder der Studierende aus Gründen, die sie oder er nicht zu vertreten hat, an der Bearbeitung der Bachelor-Thesis gehindert war.
Der Nachweis über die Gründe der Verhinderung ist unverzüglich dem Prüfungsausschuss vorzulegen.
Im Krankheitsfall ist ein ärztliches Zeugnis vorzulegen, aus dem sich die Dienstunfähigkeit der oder des Studierenden und die voraussichtliche Dauer ihrer oder seiner Erkrankung ergeben.
Der Prüfungsausschuss kann bestimmen, dass ein amtsärztliches Zeugnis beizubringen ist.
Absatz 2 Satz 6 gilt entsprechend.

#### § 30 Unlauteres Verhalten im Prüfungsverfahren

(1) Unternimmt es eine Studierende oder ein Studierender, das Ergebnis einer Modulprüfung durch Täuschung, Mitführung oder Benutzung nicht zugelassener Hilfsmittel, unzulässige Hilfe Dritter oder durch Einwirkung auf den Prüfungsausschuss oder auf von diesem mit der Wahrnehmung von Prüfungsangelegenheiten beauftragte Personen zu beeinflussen, wird die betroffene Modulprüfung mit „nicht ausreichend“ (5,0) bewertet; Entsprechendes gilt, wenn sie oder er den ordnungsgemäßen Verlauf einer Modulprüfung stört.
In besonders schweren Fällen können Studierende von der weiteren Teilnahme am Studium ausgeschlossen werden.
Für die Bachelor-Thesis und die mündliche Abschlussprüfung gelten die Sätze 1 und 2 entsprechend.

(2) Entscheidungen nach Absatz 1 trifft der Prüfungsausschuss; die oder der Studierende ist vorher anzuhören.
Bis zur Entscheidung des Prüfungsausschusses setzt die oder der Studierende die Modulprüfung fort, es sei denn, dass nach der Entscheidung der oder des Aufsichtführenden ein vorläufiger Ausschluss zur ordnungsgemäßen Weiterführung der Modulprüfung unerlässlich ist.

(3) Wird nachträglich erkannt, dass eine der Voraussetzungen nach Absatz 1 vorlag, kann der Prüfungsausschuss eine bestandene Modulprüfung oder die Bachelor-Thesis oder die mündliche Abschlussprüfung für nicht bestanden erklären.
Das unrichtige Bachelor-Zeugnis ist einzuziehen und gegebenenfalls neu auszustellen.
Entscheidungen des Prüfungsausschusses nach den Sätzen 1 und 2 sind ausgeschlossen, wenn seit der Aushändigung des Zeugnisses mehr als fünf Jahre vergangen sind.

#### § 31 Ausbildungs- und Prüfungsakten

Die Ausbildungsakten werden bei der Einstellungsbehörde geführt.
Die Prüfungsakten werden bei der Hochschule geführt.

### Abschnitt 6  
Schlussbestimmung

#### § 32 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über die Ausbildung und Prüfung für Laufbahnen des gehobenen nichttechnischen Verwaltungsdienstes im Land Brandenburg vom 2.
Januar 1996 (GVBl.
II S. 22), die zuletzt durch Artikel 34 des Gesetzes vom 23.
September 2008 (GVBl.
I S. 202, 210) geändert worden ist, außer Kraft.

Potsdam, den 20.
Juli 2016

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter