## Verordnung zur Übertragung von Ermächtigungen zur Durchführung des Tiererzeugnisse-Handels-Verbotsgesetzes (Tiererzeugnisse-Handels-Verbotsgesetz-Ermächtigungsübertragungsverordnung - TierErzHaVerbGErmÜV)

Auf Grund des § 9 Absatz 2 und 4 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl. I S. 186) und des § 36 Absatz 2 Satz 1 und 2 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl.
I S.
602) verordnet die Landesregierung:

### § 1  
_(aufgehoben)_

### § 2  
Ermächtigung

Die Ermächtigung der Landesregierung zum Erlass von Rechtsverordnungen nach § 9 Absatz 2 des Landesorganisationsgesetzes und nach § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten wird für die Regelung der Zuständigkeiten nach dem Tiererzeugnisse-Handels-Verbotsgesetz auf das für Tierschutz zuständige Mitglied der Landesregierung übertragen.

### § 3  
Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 14.
Februar 2012

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Matthias Platzeck

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack