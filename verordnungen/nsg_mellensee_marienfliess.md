## Verordnung über das Naturschutzgebiet „Mellensee-Marienfließ“

Auf Grund des § 22 Absatz 1 und 2, der §§ 23 und 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542), von denen § 23 durch Artikel 2 des Gesetzes vom 4.
August 2016 (BGBl.
I S.
1972, 1974) geändert worden ist, in Verbindung mit § 8 Absatz 1 und 3 des Brandenburgischen Naturschutzausführungsgesetzes vom 21.
Januar 2013 (GVBl.
I Nr. 3) und § 4 Absatz 1 der Naturschutzzuständigkeitsverordnung vom 27.
Mai 2013 (GVBl.
II Nr. 43) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche im Landkreis Uckermark wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Mellensee-Marienfließ“.

#### § 2 Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 723 Hektar.
Es umfasst zwei Teilflächen in folgenden Fluren:

Gemeinde:

Gemarkung:

Flur:

Boitzenburger Land

Thomsdorf

1, 4;

Funkenhagen

2, 4 bis 7, 10, 11;

Buchenhain

5, 6, 9 bis 13;

Hardenbeck

1, 3, 4;

Boitzenburg

11 bis 14.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführte Übersichtskarte im Maßstab 1 : 30 000 und die in Anlage 2 Nummer 2 aufgeführten fünf topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 5 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 3 mit den Blattnummern 1 bis 16 aufgeführten Liegenschaftskarten.
Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 3 beigefügt, die gemäß Absatz 4 hinterlegt wird.

(3) Innerhalb des Naturschutzgebietes wird eine als Naturentwicklungsgebiet bezeichnete Zone 1 festgesetzt, die der direkten menschlichen Einflussnahme entzogen ist und in der Lebensräume und Lebensgemeinschaften langfristig ihrer natürlichen Entwicklung überlassen bleiben.
Die Zone 1 umfasst eine Fläche von rund 36 Hektar und liegt in folgenden Fluren:

Gemeinde:

Gemarkung:

Flur:

Boitzenburger Land

Hardenbeck

3;

Boitzenburg

11, 12.

Die Grenze der Zone 1 ist in den in Anlage 2 Nummer 2 genannten topografischen Karten mit den Blattnummern 4 und 5 sowie in den in Anlage 2 Nummer 3 genannten Liegenschaftskarten mit den Blattnummern 15 und 16 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Verordnung mit Karten und Flurstücksliste kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie beim Landkreis Uckermark, untere Naturschutzbehörde, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

#### § 3 Schutzzweck

(1) Schutzzweck des Naturschutzgebietes ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Bruch- und Laubmischwälder, der Röhrichte und Großseggenriede, der nährstoffreichen Moore, Tauchflurengesellschaften nährstoffarmer Seen, der Trockenrasen sowie des Grünlandes frischer bis feuchter Ausprägung;
2.  die Erhaltung und Entwicklung der Lebensräume wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Wasser-Schwertlilie (Iris pseudacorus), Weiße Seerose (Nymphaea alba), Wasserfeder (Hottonia palustris), Fieberklee (Menyanthes trifoliata), Wiesenprimel (Primula veris) und Krebsschere (Stratiotes aloides);
3.  die Erhaltung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere Großer Abendsegler(Nyctalus noctula), Bekassine (Gallinago gallinago), Braunkehlchen (Saxicola rubetra), Eisvogel (Alcedo atthis), Fischadler (Pandion haliaetus), Kleinspecht (Dendrocopus minor), Kranich (Grus grus), Mittelspecht (Dendrocopus medius), Neuntöter (Lanius collurio), Schellente (Bucephala clangula), Schwarzstorch (Ciconia nigra), Seeadler (Haliaeetus albicilla), Waldwasserläufer (Tringa ochropus), Weißstorch (Ciconia ciconia), Zwergschnäpper (Ficedula parva), Zwergtaucher (Tachybaptus ruficollis), Laubfrosch (Hyla arborea), Erdkröte (Bufo bufo), Ringelnatter (Natrix natrix), Zauneidechse (Lacerta agilis), Quappe (Lota lota), Hornisse (Vespa crabro), Kleines Wiesenvögelchen (Coenonympha pamphilus), Gemeiner Bläuling (Polyommatus icarus), Gemeine Flussjungfer (Gomphus vulgatissimus) und Blauflügelprachtlibelle (Calopteryx virgo);
4.  die Erhaltung der besonderen Eigenart und hervorragenden Schönheit einer kleinräumigen, reich strukturierten, durch naturnahe Wälder, Offenlandflächen, Seen, Kleingewässer sowie überwiegend naturnahe Fließgewässer geprägten Moränenlandschaft;
5.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des regionalen Biotopverbundes zwischen den Gebieten von gemeinschaftlicher Bedeutung „Jungfernheide“, „Brüsenwalde“, „Boitzenburger Tiergarten und Strom“ im Land Brandenburg sowie „Schmaler Luzin, Zansen und Carwitzer See“ in Mecklenburg-Vorpommern.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung eines Teils des Gebietes von gemeinschaftlicher Bedeutung „Mellensee-Marienfließ“ (ehemals ein Teil des Gebietes von gemeinschaftlicher Bedeutung „Stromgewässer“) (§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes), mit seinen Vorkommen von

1.  Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions oder Hydrocharitions, Flüssen der planaren bis montanen Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Feuchten Hochstaudenfluren der planaren und montanen bis alpinen Stufe, Hainsimsen-Buchenwald (Luzulo-Fagetum) und Waldmeister-Buchenwald (Asperulo-Fagetum) als natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 4 des Bundesnaturschutzgesetzes;
2.  Auen-Wäldern mit Alnus glutinosa (Schwarz-Erle) und Fraxinus excelsior (Gewöhnliche Esche) (Alno-Padion, Alnion incanae, Salicion albae) und Schlucht- und Hangmischwäldern (Tilio-Acerion) als prioritären natürlichen Lebensraumtypen von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 1 Nummer 5 des Bundesnaturschutzgesetzes;
3.  Großem Mausohr (Myotis myotis), Fischotter (Lutra lutra), Kammmolch (Triturus cristatus), Rotbauchunke (Bombina bombina) und Bachneunauge (Lampetra planeri) als Arten von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 10 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume;
4.  Eremit (Osmoderma eremita) als prioritärer Art von gemeinschaftlichem Interesse im Sinne von § 7 Absatz 2 Nummer 11 des Bundesnaturschutzgesetzes, einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

#### § 4 Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet, seinen Naturhaushalt oder einzelne seiner Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet außerhalb der Wege zu betreten; ausgenommen ist außerhalb der Zone 1 und außerhalb von Bruchwäldern, Röhrichten, Feuchtwiesen und Mooren das Betreten zum Zweck der Erholung sowie des Sammelns von Pilzen und Wildfrüchten gemäß § 5 Absatz 1 Nummer 13 jeweils nach dem 30.
    Juni eines jeden Jahres;
10.  außerhalb der für den öffentlichen Verkehr gewidmeten Straßen und Wege sowie außerhalb von Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, und außerhalb der Waldbrandwundstreifen zu reiten;
11.  mit nicht motorisierten Fahrzeugen außerhalb der Wege sowie mit Kraftfahrzeugen außerhalb der für den öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen.
    Hinsichtlich des Fahrens mit bespannten Fahrzeugen gelten darüber hinaus die Regelungen des Brandenburgischen Naturschutzausführungsgesetzes und des Waldgesetzes des Landes Brandenburg;
12.  zu baden oder zu tauchen; ausgenommen ist das Baden im Mellensee und im Krewitzsee;
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen.
    Außerhalb von Röhrichten, Schwimmblattgesellschaften und Verlandungsbereichen ist das Befahren des Mellensees und des Krewitzsees mit elektrisch angetriebenen Booten mit einer maximalen Leistungskraft von 600 Watt mit Genehmigung der unteren Naturschutzbehörde sowie mit muskelkraftbetriebenen Booten und Luftmatratzen zulässig.
    Das Einsetzen und Anlegen dieser Wasserfahrzeuge ist ausschließlich an den Stegen gestattet;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel einschließlich Wirtschaftsdünger (zum Beispiel Gülle, Rückstände aus Biogasanlagen) und Sekundärrohstoffdünger (zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfälle, wie zum Beispiel Schlempe) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie mutwillig zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland umzubrechen, neu anzusäen oder nachzusäen.

#### § 5 Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes und § 2 des Brandenburgischen Naturschutzausführungsgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigem Umfang auf den bisher rechtmäßig dafür genutzten Flächen außerhalb der Zone 1 mit der Maßgabe, dass
    1.  Grünland als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertende Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel, Gülle, Jauche, Gärreste und Sekundärrohstoffdünger einzusetzen.
          
          
        Sekundärrohstoffdünger im Sinne dieser Verordnung sind Abwasser, Fäkalien, Klärschlamm und ähnliche Stoffe aus Siedlungsabfällen und vergleichbare Stoffe aus anderen Quellen, jeweils auch weiterbehandelt und in Mischungen untereinander oder mit Düngemitteln, Wirtschaftsdünger, Bodenhilfsstoffen, Kultursubstraten und Pflanzenhilfsmitteln,
    2.  auf Grünland § 4 Absatz 2 Nummer 23 und 24 gilt.
        Bei Narbenschäden ist eine umbruchlose Nachsaat zulässig;
2.  die den in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung in der bisherigen Art und im bisherigem Umfang auf den bisher rechtmäßig dafür genutzten Flächen außerhalb der Zone 1 mit der Maßgabe, dass
    1.  eine Nutzung der Laubwälder einzelstamm- bis gruppenweise erfolgt,
    2.  in Misch- und Nadelwäldern Holzerntemaßnahmen, die den Holzvorrat auf einer zusammenhängenden Fläche auf weniger als 40 Prozent des üblichen Vorrats reduzieren, nur bis zu einer Größe von maximal 0,5 Hektar zulässig sind,
    3.  nur Baumarten der potenziell natürlichen Vegetation eingebracht werden dürfen, wobei nur heimische Baumarten in gesellschaftstypischer Zusammensetzung unter Ausschluss eingebürgerter Arten zu verwenden sind,
    4.  der Boden unter Verzicht auf Pflügen bearbeitet wird; ausgenommen ist eine streifenweise, flachgründige, nicht in den Mineralboden eingreifende Bodenverwundung zur Unterstützung von Verjüngungsmaßnahmen,
    5.  das Befahren des Waldes nur auf Wegen oder Rückegassen erfolgt,
    6.  Bäume mit Horsten oder Höhlen nicht gefällt werden,
    7.  je Hektar mindestens fünf Stück stehendes Totholz mit mehr als 35 Zentimeter Durchmesser in 1,30 Meter Höhe über dem Stammfuß und einer Mindesthöhe von fünf Metern nicht gefällt werden und liegendes Totholz (ganze Bäume mit Durchmesser über 65 Zentimetern am stärksten Ende) im Bestand verbleibt,
    8.  ein Altholzanteil von mindestens 10 Prozent am aktuellen Bestandesvorrat zu sichern ist, wobei, sofern vorhanden, mindestens fünf Stämme je Hektar mit einem Brusthöhendurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum Absterben aus der Nutzung genommen sein müssen,
    9.  § 4 Absatz 2 Nummer 17 und 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung in der bisherigen Art und im bisherigen Umfang auf den bisher rechtmäßig dafür genutzten Flächen außerhalb der Zone 1 mit der Maßgabe, dass
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass ein Einschwimmen und eine Gefährdung des Fischotters weitgehend ausgeschlossen sind,
    2.  der Fischbesatz im Krewitzsee und im Mellensee nur mit heimischen Arten erfolgt und der Besatz mit Karpfen unzulässig ist,
    3.  § 4 Absatz 2 Nummer 19 gilt;
4.  innerhalb der Zone 1 die erforderlichen Hegemaßnahmen gemäß § 1 der Fischereiordnung des Landes Brandenburg im Sinne eines Monitorings mit Zustimmung der unteren Naturschutzbehörde.
    Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird;
5.  die rechtmäßige Ausübung der Angelfischerei am Mellensee und am Krewitzsee mit der Maßgabe, dass § 4 Absatz 2 Nummer 13, 19 und 20 gilt;
6.  für den Bereich der Jagd in der Zone 1:
    1.  Maßnahmen der Bestandsregulierung von Schalenwild mit der Maßgabe, dass die Bestandsregulierung durch maximal drei eintägige Gesellschaftsjagden im Zeitraum vom 1. Oktober eines jeden Jahres bis zum 31.
        Januar des Folgejahres erfolgt.
        Die Durchführung der Gesellschaftsjagden ist jeweils eine Woche vorher schriftlich bei der unteren Naturschutzbehörde anzuzeigen.
        Sonstige Maßnahmen der Bestandsregulierung sind nach Zustimmung durch die untere Naturschutzbehörde zulässig.
        Dazu sind vom Antragsteller Erfordernis, Ziel, Art, Umfang, Zeitpunkt und Ort der Maßnahme darzulegen.
        Die Zustimmung ist zu erteilen, wenn die Maßnahme dem Schutzzweck nicht oder nur unerheblich zuwiderläuft,
    2.  das Aufstellen transportabler und mobiler Ansitzeinrichtungen.Im Übrigen ist die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd sowie die Anlage von Kirrungen, Fütterungen, Ansaatwildwiesen und Wildäckern verboten;
7.  für den Bereich der Jagd außerhalb der Zone 1:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        in der Zeit vom 1.
        März bis zum 30.
        Juni eines jeden Jahres die Jagd nur vom Ansitz aus erfolgt,
        
        bb)
        
        die Jagd auf Wasserfederwild verboten ist,
        
        cc)
        
        die Fallenjagd mit Lebendfallen erfolgt und in einem Abstand von bis zu 100 Metern vom Ufer aller innerhalb des Schutzgebietes liegender Gewässer verboten ist.
        Ausnahmen von der Einhaltung dieses Abstands kann die untere Naturschutzbehörde erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
        dd)
        
        keine Baujagd in einem Abstand von bis zu 100 Metern vom Ufer aller innerhalb des Schutzgebietes liegender Gewässer vorgenommen wird.
        Ausnahmen von der Einhaltung dieses Abstands kann die untere Naturschutzbehörde erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    3.  das Aufstellen transportabler und mobiler Ansitzeinrichtungen,
    4.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und der in § 3 Absatz 2 Nummer 1 und 2 genannten Lebensraumtypen.
    
    Ablenkfütterungen, die Anlage und die Unterhaltung von Ansaatwildwiesen sowie die Anlage oder Unterhaltung von Wildäckern sind unzulässig.
    Im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Brandenburgischen Jagdgesetzes unberührt;
    
8.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege.
    Die untere Naturschutzbehörde ist rechtzeitig mit dem Ziel einer einvernehmlichen Lösung zu beteiligen;
9.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht.
    Die Maßnahmen können durch einen abgestimmten Unterhaltungsplan dokumentiert werden;
10.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, Messanlagen (Pegel-, Abfluss- und andere Messstellen) und sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
11.  die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern diese nicht unter die Nummern 7 und 8 fallen, im Einvernehmen mit der unteren Naturschutzbehörde;
12.  die sonstigen bei Inkrafttreten dieser Verordnung aufgrund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
13.  das Sammeln von Pilzen und Wildfrüchten in geringen Mengen für den persönlichen Gebrauch nach dem 30.
    Juni eines jeden Jahres;
14.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
15.  Schutz-, Pflege- und Entwicklungsmaßnahmen, wie zum Beispiel Maßnahmen zur Bekämpfung der Spätblühenden Traubenkirsche, die von der zuständigen Naturschutzbehörde zugelassen oder angeordnet worden sind;
16.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen, touristische Informationen oder Warntafeln dienen;
17.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist; das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

(3) Die in Absatz 1 genannten zulässigen Handlungen bleiben von Zulassungserfordernissen, die sich aus anderen fachrechtlichen Vorgaben ergeben, unberührt.

#### § 6 Pflege-und Entwicklungsmaßnahmen

Folgende Pflege- und Entwicklungsmaßnahmen werden als Zielvorgabe benannt:

1.  Kleingewässer und ihre Umgebung sollen als Reproduktions- und Lebensräume für Amphibien gepflegt und entwickelt werden, indem
    1.  unbeschattete Wasserflächen freigehalten werden,
    2.  in Kleingewässern der Fischbestand den Reproduktionsbedingungen der Rotbauchunke angepasst wird,
    3.  die Lebensräume an den Gewässerrändern in einem Streifen von 20 Metern amphibiengerecht genutzt werden, insbesondere im Zeitraum Juli und August eines jeden Jahres keine Mahd durchgeführt wird,
    4.  an den Gewässern Winterlebensräume für Laubfrosch, Erdkröte, Zauneidechse und Ringelnatter, insbesondere Gehölzflächen mit Totholz, Laub-, Reisig- und Lesesteinhaufen entwickelt und erhalten werden;
2.  Ackerflächen sollen in Dauergrünland umgewandelt werden;
3.  Maßnahmen zur Verminderung der Nährstoffbelastung in den Zuflüssen des Mellensees und des Krewitzsees.

#### § 7 Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

#### § 8 Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 39 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzausführungsgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 40 des Brandenburgischen Naturschutzausführungsgesetzes mit einer Geldbuße bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 9 Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 25 des Brandenburgischen Naturschutzausführungsgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (§§ 17 und 18 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 29 Absatz 3 und § 30 des Bundesnaturschutzgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten, ihrer Lebensstätten und Biotope (§§ 37 bis 47 des Bundesnaturschutzgesetzes) sowie über Horststandorte (§ 19 des Brandenburgischen Naturschutzausführungsgesetzes in Verbindung mit § 54 Absatz 7 des Bundesnaturschutzgesetzes) unberührt.

#### § 10 Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 9 des Brandenburgischen Naturschutzausführungsgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

#### § 11 Inkrafttreten

§ 5 Absatz 1 Nummer 1 dieser Verordnung tritt am 1.
Januar 2019 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 14.
November 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Mellensee-Marienfließ“](/br2/sixcms/media.php/68/GVBl_II_83_2018-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 1) - Kartenskizze zur Lage des Naturschutzgebietes „Mellensee-Marienfließ“") 527.1 KB

2

[Anlage 2 (zu § 2 Absatz 2) - Übersichtskarte, Topografische Karten, Liegenschaftskarten](/br2/sixcms/media.php/68/GVBl_II_83_2018-Anlage-2.pdf "Anlage 2 (zu § 2 Absatz 2) - Übersichtskarte, Topografische Karten, Liegenschaftskarten") 228.8 KB