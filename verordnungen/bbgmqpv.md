## Verordnung über die Gewährung von Mobilitätsprämien und Qualifizierungsprämien im Land Brandenburg (BbgMQPV)

Auf Grund des § 49 Absatz 2 des Brandenburgischen Besoldungsgesetzes vom 20. November 2013 (GVBl.
I Nr. 32 S. 2, Nr.
34) verordnet der Minister der Finanzen im Einvernehmen mit dem Minister des Innern:

### § 1   
Geltungsbereich

Diese Verordnung gilt für Beamtinnen, Beamte, Richterinnen und Richter des Landes Brandenburg.

### § 2   
Mobilitätsprämie

(1) Eine Mobilitätsprämie wird Beamtinnen, Beamten, Richterinnen und Richtern gewährt, die aufgrund einer Maßnahme des Verwaltungsumbaus bei einer Dienststelle oder einem Dienststellenteil außerhalb ihres bisherigen Dienstortes und ihres Wohnortes verwendet werden.

(2) Maßnahmen des Verwaltungsumbaus im Sinne des Absatzes 1 sind

1.  die Auflösung oder Verlegung von Dienststellen oder wesentlichen Dienststellenteilen und der Zusammenschluss oder die Teilung von Dienststellen sowie grundlegende Änderungen der Dienststellenorganisation einschließlich der Bündelung und Verlagerung von Aufgaben, die Einführung grundlegend neuer Arbeitsmethoden und
2.  Umsetzungen, Abordnungen oder Versetzungen zur Unterstützung des Personalabbaus bei Dienststellen, in denen die fristgerechte Realisierung haushaltsrechtlich bestimmter Personalabbauziele durch Altersabgänge nicht möglich ist.

Bei Lehrkräften an allgemeinbildenden und berufsbildenden Schulen gelten Umsetzungen, Abordnungen und Ver-setzungen als Maßnahmen des Verwaltungsumbaus, wenn sie erfolgen, weil eine weitere Verwendung von Lehr-kräften am bisherigen Dienstort oder in der bisherigen Schulstufe aufgrund von Veränderungen der Schülerzahlen oder wegen mangelnden Fachbedarfs nicht möglich ist.
Satz 1 gilt nicht, wenn das Land nach Artikel 97 Absatz 3 der Verfassung des Landes Brandenburg den Gemeinden und Gemeindeverbänden Aufgaben des Landes zur Wahrnehmung überträgt.

(3) Die Mobilitätsprämie wird für eine Verwendungsdauer von mindestens 18 Monaten gewährt und beträgt bei einer einfachen zusätzlichen Entfernung zwischen der Wohnung und der neuen Dienststelle von

1.  10 bis 20 Kilometern                 450 Euro,
2.  21 bis 30 Kilometern                 900 Euro,
3.  31 bis 50 Kilometern              1 500 Euro,
4.  51 bis 70 Kilometern              1 725 Euro,
5.  mehr als 70 Kilometern           2 250 Euro.

(4) Bei befristeten Personalmaßnahmen mit einer Verwendungsdauer von weniger als 18 Monaten steht die Mobilitätsprämie in Höhe eines Achtzehntels des Betrages nach Absatz 3 für jeden vollen Monat der Verwendung am neuen Dienstort zu.
Bei einer Verlängerung der Verwendungsdauer ist die Mobilitätsprämie neu festzusetzen; die sich aus der Neufestsetzung der Mobilitätsprämie ergebenden Erhöhungsbeträge sind nachzuzahlen.

(5) Der Anspruch auf die Mobilitätsprämie entsteht mit dem Tag des Wirksamwerdens der Personalmaßnahme, frühestens jedoch mit dem Tag des Dienstantritts am neuen Dienstort.
Die Mobilitätsprämie wird als Einmalzahlung mit den Dienstbezügen für den dritten auf die Entstehung des Anspruchs folgenden Monat gezahlt.

(6) Bei einer Beendigung der Personalmaßnahme vor Ablauf von 18 Monaten seit dem Tag des Dienstantritts am neuen Dienstort aus Gründen, die von der oder dem Berechtigten zu vertreten sind, ist die Mobilitätsprämie in Höhe von einem Achtzehntel des Betrages nach Absatz 3 für jeden vollen Monat der entfallenen Verwendung am neuen Dienstort zurückzuzahlen.
Bei befristeten Maßnahmen nach Absatz 4 ist die Mobilitätsprämie in Höhe des sich aus der Dauer der Befristung ergebenden Bruchteiles des Betrages nach Absatz 3 für jeden vollen Monat der entfallenen Verwendung am neuen Dienstort zurückzuzahlen.

### § 3   
Qualifizierungsprämien

(1) Qualifizierungsprämien werden Beamtinnen und Beamten gewährt, die mit Zustimmung der obersten Dienstbehörde und des Personalservice eine berufliche Qualifizierungsmaßnahme mit einer Gesamtdauer von mindestens einem Jahr und längstens fünf Jahren absolviert haben.
Als Qualifizierungsprämien werden Basisprämien und leistungsabhängige Anerkennungsprämien gewährt.

(2) Die Gewährung einer Qualifizierungsprämie setzt voraus, dass

1.  ein dienstliches Bedürfnis für die Durchführung der Qualifizierungsmaßnahme besteht,
2.  eine dauerhafte Verwendung der Beamtin oder des Beamten in dem Aufgabenbereich, der Gegenstand der Qualifizierung ist, möglich ist,
3.  die Beamtin oder der Beamte die persönlichen und fachlichen Voraussetzungen für die Teilnahme an der Qualifizierungsmaßnahme sowie die dienstrechtlichen, insbesondere die laufbahnrechtlichen Voraussetzungen für die zukünftige Verwendung erfüllt und
4.  die Qualifizierungsmaßnahme zu einem berufsqualifizierenden Abschluss geführt hat und mit einer leistungsbewertenden Prüfung abgeschlossen wurde.

(3) Die Basisprämie beträgt bei einer Dauer der Qualifizierungsmaßnahme von

1.  einem Jahr bis zu zwei Jahren                  400 Euro,
2.  mehr als zwei bis zu drei Jahren               800 Euro,
3.  mehr als drei Jahren                             1 200 Euro.

(4) Die Anerkennungsprämie wird gewährt, wenn die Qualifizierungsmaßnahme mit einer Abschlussnote oder Abschlussbewertung absolviert wurde, die im oberen Viertel der jeweils geltenden Noten- oder Bewertungsskala liegt.
Die Anerkennungsprämie beträgt bei einer Dauer der Qualifizierungsmaßnahme von

1.  einem Jahr bis zu zwei Jahren                500 Euro,
2.  mehr als zwei bis zu drei Jahren          1 250 Euro,
3.  mehr als drei Jahren                            1 750 Euro.

(5) Die Qualifizierungsprämien sind zurückzuzahlen, wenn die Beamtin oder der Beamte durch die Qualifizierungsmaßnahme eine besonders hohe berufliche Qualifikation erlangt hat, die mit überdurchschnittlichen Vorteilen auf dem Arbeitsmarkt verbunden ist, und wenn sie oder er vor Ablauf der Bindungsdauer aus von ihr oder ihm zu vertretenden Gründen aus dem Landesdienst ausscheidet.
Die Bindungsdauer entspricht der Dauer der Qualifizierungsmaßnahme; sie beträgt längstens fünf Jahre.
Der Rückzahlungsbetrag vermindert sich um ein Zwölftel bis ein Sechzigstel für jeden vollen Monat, den das Beamtenverhältnis zum Land Brandenburg nach dem Abschluss der Qualifizierungsmaßnahme fortbestanden hat.

### § 4   
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt mit Wirkung vom 1.
Januar 2014 in Kraft.
Sie tritt mit Ablauf des 31. Dezember 2022 außer Kraft.

Potsdam, den 15.
September 2014

Der Minister der Finanzen

Christian Görke