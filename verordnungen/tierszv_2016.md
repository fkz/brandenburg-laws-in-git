## Verordnung über abweichende Zuständigkeiten auf den Gebieten der Tierseuchenbekämpfung und der Beseitigung tierischer Nebenprodukte  (Tierseuchenzuständigkeits-Verordnung - TierSZV)

Auf Grund des § 9 Absatz 2 und 4 des Landesorganisationsgesetzes vom 24.
Mai 2005 (GVBl. I S. 186) in Verbindung mit § 16 Absatz 2 des Landesorganisationsgesetzes und auf Grund des § 36 Absatz 2 Satz 1 und 2 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl.
I S.
602) verordnet die Landesregierung:

#### § 1  
Sachliche Zuständigkeiten

Für die Wahrnehmung der in der Anlage aufgeführten Verwaltungsaufgaben sind abweichend vom Grundsatz des § 1 Absatz 4 des Gesetzes zur Ausführung des Tiergesundheitsgesetzes und des § 2 Absatz 2 des Gesetzes zur Ausführung des Tierische Nebenprodukte-Beseitigungsgesetzes die in der Anlage bezeichneten Behörden zuständig.

#### § 2  
Ordnungswidrigkeiten

Zuständige Behörden für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 32 des Tiergesundheitsgesetzes sind die Landkreise und kreisfreien Städte als Kreisordnungsbehörden, soweit in dieser Verordnung keine abweichenden Regelungen getroffen sind.

#### § 3  
Ermächtigung

Die Ermächtigung der Landesregierung zum Erlass von Rechtsverordnungen nach § 6 Absatz 1 des Landesorganisationsgesetzes und nach § 36 Absatz 2 Satz 1 des Gesetzes über Ordnungswidrigkeiten wird für Angelegenheiten der Tierseuchenbekämpfung und der Beseitigung tierischer Nebenprodukte auf das für das Veterinärwesen zuständige Mitglied der Landesregierung übertragen.

#### § 4  
Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Tierseuchenzuständigkeits-Verordnung vom 28.
August 1995 (GVBl.
II S.
554), die durch Artikel 15 des Gesetzes vom 15.
Juli 2010 (GVBl.
I Nr.
28 S.
13) geändert worden ist, außer Kraft.

Potsdam, den 10.
April 2013

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Matthias Platzeck

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack 

**Anlage  
**(zu § 1)

I.
Rechtsgrundlagen zum nachfolgenden Verzeichnis
-------------------------------------------------

Tiergesundheitsgesetz

Binnenmarkt-Tierseuchenschutzverordnung

Verordnung (EG) Nr.
1069/2009 des Europäischen Parlaments und des Rates vom 21.
Oktober 2009 mit Hygienevorschriften für nicht für den menschlichen Verzehr bestimmte tierische Nebenprodukte und zur Aufhebung der Verordnung (EG) Nr.
1774/2002 (Verordnung (EG) Nr.
1069/2009)

Viehverkehrsverordnung

Tierimpfstoff-Verordnung

MKS-Verordnung

Schweinepest-Verordnung

Geflügelpest-Verordnung

Tollwut-Verordnung

Einhufer-Blutarmut-Verordnung

Geflügelpest-Verordnung in der Fassung der Bekanntmachung vom 20.
Dezember 2005 (BGBl. I S. 3538) hinsichtlich der Newcastle-Krankheit

Verordnung zum Schutz gegen die Vesikuläre Schweinekrankheit

Tuberkulose-Verordnung

Brucellose-Verordnung

Rinder-Leukose-Verordnung

Verordnung zum Schutz gegen die Aujeszkysche Krankheit

Fischseuchenverordnung

Geflügel-Salmonellen-Verordnung

Verordnung (EU) Nr.
206/2010 der Kommission zur Erstellung von Listen von Drittländern, Gebiete und Teile davon, aus denen das Verbringen bestimmter Tiere und bestimmten frischen Fleisches in die Europäische Union zulässig ist, und zur Feststellung der diesbezüglichen Veterinärbescheinigungen (Verordnung (EU) Nr.
206/2010)

Verordnung zum Schutz gegen den Milzbrand und den Rauschbrand

Tierseuchenerreger-Verordnung

Verordnung (EU) Nr.
142/2011 der Kommission zur Durchführung der Verordnung (EG) Nr.
1069/2009 des Europäischen Parlaments und des Rates mit Hygienevorschriften für nicht für den menschlichen Verzehr bestimmte tierische Nebenprodukte sowie zur Durchführung der Richtlinie 97/78/EG des Rates hinsichtlich bestimmter gemäß der genannten Richtlinie von Veterinärkontrollen an der Grenze befreiter Proben und Waren (Verordnung (EU) Nr.
142/2011)

Tierische Nebenprodukte-Beseitigungsverordnung

Verordnung (EU) Nr.
576/2013 des Europäischen Parlaments und des Rates vom 12.
Juni 2013 über die Verbringung von Heimtieren zu anderen als Handelszwecken und zur Aufhebung der Verordnung (EG) Nr.
998/2003 (Verordnung (EU) Nr.
576/2013)

II.
Erläuterungen zu Abkürzungen im nachfolgenden Verzeichnis
-------------------------------------------------------------

MdJEV - Ministerium der Justiz und für Europa und Verbraucherschutz

LAVG - Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit

AGTierGesG - Gesetz zur Ausführung des Tiergesundheitsgesetzes

OrdB - örtliche Ordnungsbehörde

**laufende  
Nummer**

**Vorschrift**

**Verwaltungsaufgabe**

**zuständige Behörde**

**1**

**Tiergesundheitsgesetz**

1.1

§ 12 Absatz 1 Satz 1

Erteilung einer Herstellungserlaubnis

LAVG

1.2

§ 12 Absatz 2 Satz 1

Entgegennahme einer Mitteilung

LAVG

1.3

§ 12 Absatz 2 Satz 3

Mitteilungspflicht

LAVG

1.4

§ 12 Absatz 3

Herstellen des Benehmens mit anderen Stellen

LAVG

1.5

§ 12 Absatz 4 Satz 2

Entgegennahme einer Anzeige

LAVG

**2**

**Binnenmarkt-Tierseuchenschutzverordnung**

2.1

§ 7

Erteilung von Genehmigungen

LAVG

2.2

§ 41

Verfolgung und Ahndung von Ordnungswidrigkeiten im Zusammenhang mit Aufgaben nach § 1 Absatz 5 AGTierGesG

LAVG

**3**

**Verordnung (EG) Nr. 1069/2009**

3.1

Artikel 20 Absatz 2 Satz 1 und 2

Entscheidung über Anträge

MdJEV

3.2

Artikel 48 Absatz 1 Satz 1 und 2

Entscheidung über Anträge

LAVG

**4**

**Viehverkehrsverordnung**

4.1

§ 27 Absatz 3 Satz 3 und Absatz 4

Erteilung von Ausnahmen von der Mindestgröße von Rinderohrmarken

MdJEV

4.2

§ 34 Absatz 3c und Absatz 4

Erteilung von Ausnahmen von der Mindestgröße von Schaf- und Ziegenohrmarken

MdJEV

4.3

§ 44a Absatz 3

Erteilung von Ausnahmen von der Ausstellung von Equidenpässen

MdJEV

**5**

**Tierimpfstoff-Verordnung**

5.1

§ 6

Entgegennahme einer Anzeige zur Aufnahme der jeweiligen Tätigkeit

LAVG

5.2

§ 7

Anordnung des Ruhens der Herstellungserlaubnis

LAVG

5.3

§ 18

Erteilung einer Bescheinigung über die Einhaltung der Guten Herstellungspraxis

LAVG

5.4

§ 19

Durchführung von Betriebsprüfungen

LAVG

5.5

§ 38

Erteilung einer Einfuhrerlaubnis

LAVG

5.6

§ 39 Absatz 3

Erteilung einer Ausnahme für die Ausstellung und Vorlage einer Bescheinigung

LAVG

5.7

§ 47 Absatz 2 Nummer 1, 19 i. V. m.
§ 6 Absatz 1 und § 39

Verfolgung und Ahndung von Ordnungswidrigkeiten

LAVG

**6**

**MKS-Verordnung**

6.1

§ 2 Absatz 2

Genehmigungen vom Impfungen

MdJEV

6.2

§ 9 Absatz 2 Nummer 1

Beschilderung des Sperrbezirks

OrdB

6.3

§ 11 Absatz 2 Nummer 1

Beschilderung des Beobachtungsgebietes

OrdB

6.4

§ 24 Absatz 3

Beschilderung des gefährdeten Bezirks

OrdB

6.5

§ 26

Vorlage eines Tilgungsplanes

MdJEV

**7**

**Schweinepest-Verordnung**

7.1

§ 2 Absatz 2

Genehmigung von Impfungen

MdJEV

7.2

§ 11 Absatz 2 Nummer 1

Beschilderung des Sperrbezirks

OrdB

7.3

§ 11a Absatz 2 Nummer 1

Beschilderung des Beobachtungsgebietes

OrdB

7.4

§ 14a Absatz 3

Beschilderung des gefährdeten Bezirks

OrdB

7.5

§ 14d

Vorlage eines Tilgungsplanes

MdJEV

**8**

**Geflügelpest-Verordnung**

8.1

§ 8 Absatz 2 Nummer 1

Genehmigung von Impfungen

MdJEV

8.2

§ 8 Absatz 4

Vorlage eines Impfplanes

MdJEV

8.3

§ 21 Absatz 4 Nummer 1

Beschilderung des Sperrbezirks

OrdB

8.4

§ 27 Absatz 2

Beschilderung des Beobachtungsgebietes

OrdB

8.5

§ 30 Absatz 2 Nummer 1

Beschilderung der Kontrollzone

OrdB

8.6

§ 36 Absatz 2

Vorlage eines Notimpfplanes

MdJEV

8.7

§ 56 Absatz 5

Beschilderung des Sperr- und Beobachtungsgebietes

OrdB

**9**

**Tollwut-Verordnung**

9.1

§ 3 Nummer 2

Zulassung von Ausnahmen vom Impfverbot für wissenschaftliche Versuche

MdJEV

9.2

§ 8 Absatz 2

Beschilderung des gefährdeten Bezirks

OrdB

9.3

§ 12 Absatz 2

Festlegung von Impfmaßnahmen

MdJEV

**10**

**Einhufer-Blutarmut-Verordnung**

10.1

§ 2 Satz 2

Genehmigung von Ausnahmen vom Impfverbot

MdJEV

10.2

§ 10 Absatz 2

Beschilderung des Sperrbezirks

OrdB

**11**

**Geflügelpest-Verordnung in der Fassung der Bekanntmachung vom 20.
Dezember 2005 (BGBl. I S. 3538) hinsichtlich der Newcastle-Krankheit**

11.1

§ 5 Absatz 3

Genehmigung von Ausnahmen vom Impfverbot

MdJEV

11.2

§ 15 Absatz 2 Nummer 1

Beschilderung des Sperrbezirks

OrdB

11.3

§ 16 Absatz 2 Nummer 1

Beschilderung des Beobachtungsgebietes

OrdB

**12**

**Verordnung zum Schutz gegen die Vesikuläre Schweinekrankheit**

12.1

§ 2 Absatz 2

Genehmigung von Ausnahmen vom Impfverbot

MdJEV

12.2

§ 9 Absatz 2 Nummer 1

Beschilderung des Sperrbezirks

OrdB

12.3

§ 10 Absatz 2 Nummer 1

Beschilderung des Beobachtungsgebietes

OrdB

**13**

**Tuberkulose-Verordnung**

13.1

§ 2 Satz 2

Genehmigungen von Ausnahmen vom Impfverbot

MdJEV

**14**

**Brucellose-Verordnung**

14.1

§ 2 Satz 2

Zulassung von Ausnahmen vom Impfverbot

MdJEV

**15**

**Rinder-Leukose-Verordnung**

15.1

§ 3 Satz 2

Zulassung von Ausnahmen vom Impfverbot sowie für Heilversuche

MdJEV

**16**

**Verordnung zum Schutz gegen die Aujeszkysche Krankheit**

16.1

§ 3 Absatz 2 Satz 1 Nummer 1

Genehmigung von Ausnahmen vom Impfverbot

MdJEV

**17**

**Fischseuchenverordnung**

17.1

§ 10 Absatz 1

Erklärung zum Schutzgebiet

LAVG

17.2

§ 11 Absatz 3

Genehmigung von Ausnahmen vom Impfverbot

MdJEV

**18**

**Geflügel-Salmonellen-Verordnung**

18.1

§ 35 Absatz 2 Satz 2

Zulassung von Ausnahmen zum Impfverbot

MdJEV

**19**

**Verordnung (EU) Nr. 206/2010**

19.1

Artikel 3a

Erteilung von Einfuhrgenehmigungen und Bewertung der Tiergesundheitsrisiken

LAVG

19.2

Artikel 3b

Erteilung von Durchfuhrgenehmigungen und Risikobewertung

LAVG

**20**

**Verordnung zum Schutz gegen den Milzbrand und den Rauschbrand**

20.1

§ 2 Absatz 2 Nummer 1

Zulassung von Ausnahmen vom Impfverbot

MdJEV

**21**

**Tierseuchenerreger-Verordnung**

21.1

§ 2 Absatz 1

Erteilung einer Erlaubnis

LAVG

21.2

§§ 5, 6

Entgegennahme einer Anzeige

LAVG

21.3

§ 7

Untersagen und Beschränkung von Tätigkeiten

LAVG

21.4

§ 9 i.
V.
m.
§ 24 des Tiergesundheitsgesetzes

Kontrolle von Aufzeichnungen

LAVG

21.5

§ 10

Verfolgung und Ahndung von Ordnungswidrigkeiten

LAVG

**22**

**Verordnung (EU) Nr. 142/2011**

22.1

Artikel 27 Nummer 1

Gestattung der Ein- und Ausfuhr

LAVG

22.2

Artikel 28 Nummer 1 und 3

Gestattung der Ein- und Durchfuhr

LAVG

22.3

Anhang XIV  
Kapitel IV Abschnitt 2 Nummer 1

Erteilung von Genehmigungen

LAVG

**23**

**Tierische Nebenprodukte-Beseitigungsverordnung**

23.1

§ 26 Absatz 1

Erteilung von Zulassungs- und Registriernummern

MdJEV

**24**

**Verordnung (EU) Nr. 576/2013**

24.1

Artikel 32

Erteilung von Genehmigungen

LAVG