## Siebente Verordnung über die Aufhebung von Wasserschutzgebieten

Auf Grund des § 15 Absatz 4 Satz 4 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20), der durch Artikel 1 des Gesetzes vom 4. Dezember 2017 (GVBl.
I Nr. 28) neu gefasst worden ist, verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Aufhebung von Wasserschutzgebieten

(1) Folgende, auf der Grundlage des § 28 des Landeskulturgesetzes vom 14.
Mai 1970 (GBl.
I Nr. 12 S. 67), des § 28 des Wassergesetzes vom 17.
April 1963 (GBl.
I Nr. 5 S. 77) und der Verordnung über die Festlegung von Schutzgebieten für die Wasserentnahme aus dem Grund- und Oberflächenwasser zur Trinkwassergewinnung vom 11.
Juli 1974 (GBl.
I Nr. 37 S. 349) festgesetzte und nach § 46 des Wassergesetzes vom 2.
Juli 1982 (GBl.
I Nr. 26 S. 467) aufrechterhaltene Wasserschutzgebiete werden aufgehoben:

1.  das mit Beschluss Nummer 0017 vom 19.
    November 1975 des Kreistages Jüterbog für das Wasserwerk Lobbese OT Zeuden festgesetzte Trinkwasserschutzgebiet,
2.  die mit Beschluss Nummer 63-20/77 vom 8.
    September 1977 des Kreistages Strasburg für die Wasserwerke Hansfelde, Ottenhagen und Wismar LPG Strasburg festgesetzten Trinkwasserschutzgebiete,
3.  die mit Beschluss Nummer 125-26/78 vom 27.
    November 1978 des Kreistages Perleberg für die Wasserwerke Burow und Zapel festgesetzten Trinkwasserschutzgebiete,
4.  das mit Beschluss Nummer 7-36/80 vom 30.
    Juli 1980 des Kreistages Brandenburg für das Wasserwerk Rietz festgesetzte Trinkwasserschutzgebiet,
5.  das mit Beschluss Nummer 84-13/81 vom 6.
    Mai 1981 des Kreistages Potsdam für das Wasserwerk Bochow festgesetzte Trinkwasserschutzgebiet,
6.  das mit Beschluss Nummer 53/81 vom 13.
    Mai 1981 des Kreistages Bad Freienwalde für das Wasserwerk Bad Freienwalde OT Sonnenburg festgesetzte Trinkwasserschutzgebiet,
7.  das mit Beschluss Nummer 85 vom 21.
    Dezember 1981 des Kreistages Templin für das Wasserwerk Böckenberg festgesetzte Trinkwasserschutzgebiet.

(2) Folgende, auf der Grundlage des § 29 des Wassergesetzes vom 2.
Juli 1982 und der Dritten Durchführungsverordnung zum Wassergesetz – Schutzgebiete und Vorbehaltsgebiete – vom 2. Juli 1982 (GBl.
I Nr. 26 S. 487) festgesetzte Trinkwasserschutzgebiete werden aufgehoben:

1.  das mit Beschluss Nummer 123/23/83 vom 2.
    März 1983 des Kreistages Fürstenwalde (Spree) für das Wasserwerk Steinhöfel (lfd.
    Nr. 28) festgesetzte Trinkwasserschutzgebiet,
2.  das mit Beschluss Nummer 87-24/83 vom 13.
    Juli 1983 des Kreistages Angermünde für das Wasserwerk Criewen – Vorwerk festgesetzte Trinkwasserschutzgebiet,
3.  das mit Beschluss Nummer 18-24/83 vom 12.
    September 1983 des Kreistages Seelow für das Wasserwerk Libbenichen festgesetzte Trinkwasserschutzgebiet,
4.  das mit Beschluss Nummer 09/36/85 vom 21.
    August 1985 des Kreistages Beeskow für das Wasserwerk Weichensdorf festgesetzte Trinkwasserschutzgebiet.

#### § 2 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 13.
April 2018

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger