## Mehrbelastungsausgleichsverordnung für die Gemeinden infolge des Gesetzes zur Abschaffung der Beiträge für den Ausbau kommunaler Straßen (Straßenausbau-Mehrbelastungsausgleich-Verordnung - StraMaV)

Auf Grund des § 3 Nummer 1 bis 3 und Nummer 5 des Gesetzes über den Mehrbelastungsausgleich für kommunale Straßenausbaumaßnahmen vom 19.
Juni 2019 (GVBl.
I Nr. 36) verordnet die Ministerin für Infrastruktur und Landesplanung im Einvernehmen mit dem Minister des Innern und für Kommunales und dem Minister der Finanzen:

#### § 1 Zuständigkeit

Zuständige Stelle für die Prüfung und Gewährung des Mehrbelastungsausgleichs nach § 1 des Gesetzes über den Mehrbelastungsausgleich für kommunale Straßenausbaumaßnahmen ist das Landesamt für Bauen und Verkehr (Erstattungsbehörde).

#### § 2 Pauschaler Mehrbelastungsausgleich

(1) Der zweckgebundene Mehrbelastungsausgleich gemäß § 1 Absatz 1 des Gesetzes über den Mehrbelastungsausgleich für kommunale Straßenausbaumaßnahmen wird pauschal auf der Grundlage eines Grundbetrages je Kilometer Gemeindestraße vervielfältigt mit der jeweiligen Gesamtlänge der gewidmeten Gemeindestraßen einer Gemeinde verteilt.

(2) Der Grundbetrag wird für das Ausgleichsjahr 2019 auf 1 416,77 Euro je Kilometer festgeschrieben und dann jährlich gemäß Absatz 5 fortgeschrieben.

(3) Für die Berechnung des Pauschalbetrages für das Ausgleichsjahr 2019 wird die Länge der gewidmeten Gemeindestraßen je Gemeinde (Gemeindestraßenlänge) gemäß den amtlichen Geobasisdaten des Amtlichen Topographischen-Kartographischen Informationssystems (ATKIS) des Landesbetriebs Landesvermessung und Geobasisinformation Brandenburg zum 31. Dezember 2018 angesetzt.
Die Gemeindestraßenlängen werden auf den nächsten vollen Kilometerwert aufgerundet und jährlich fortgeschrieben.
Maßgeblich für die auf das Jahr 2019 folgenden Auszahlungsjahre sind die amtlichen Geobasisdaten in ATKIS zum 31.
Dezember des jeweiligen Vorjahres.
Auf Verlangen haben die Gemeinden dem Landesbetrieb Landesvermessung und Geobasisinformation Brandenburg Auskunft über die Gemeindestraßenlängen zu geben.

(4) Der pauschale Mehrbelastungsausgleich für das Ausgleichsjahr 2019 wird unverzüglich nach Inkrafttreten der Rechtsverordnung zugewiesen.
Der pauschale Mehrbelastungsausgleich für die Folgejahre wird einmal jährlich spätestens zum 31.
Juli an die Gemeinden als Zuweisung ausgezahlt.
Für nicht verwendete Mehrbelastungsausgleichszahlungen ist eine Sonderrücklage zu bilden.

(5) Ab dem Jahre 2020 erfolgt eine Dynamisierung im Rahmen des pauschalen Mehrbelastungsausgleiches.
Der für den pauschalen Mehrbelastungsausgleich maßgebliche Grundbetrag von 1 416,77 Euro je Kilometer steigt jährlich um 1,5 Prozent.

#### § 3 Erstattung von Rückzahlungen

(1) Zusätzlich zum pauschalen Mehrbelastungsausgleich nach § 2 erhalten Gemeinden auf Antrag die Rückzahlungen von Straßenbaubeiträgen und Vorausleistungen erstattet, die sie aufgrund von § 20 Absatz 4 und 5 des Kommunalabgabengesetzes für das Land Brandenburg geleistet haben.
Dies gilt auch für Rückzahlungen der Gemeinden von Beträgen, die aufgrund von Vereinbarungen zur Ablösung von Beiträgen im Sinne des § 20 Absatz 4 des Kommunalabgabengesetzes für das Land Brandenburg geleistet worden sind.
Für die Erstattung hat die Gemeinde die Rückzahlungen im Einzelfall nachzuweisen.
Den Antragsunterlagen sind die zugrundeliegenden Bescheide und Satzungen der Gemeinde sowie der Nachweis des Entstehens der sachlichen Beitragspflicht ab dem 1. Januar 2019 beizufügen.
Auf Verlangen der Erstattungsbehörde haben die Gemeinden weitere Angaben oder Unterlagen zu ergänzen.

(2) Die Erstattung erfolgt zuzüglich einer Verwaltungskostenpauschale in Höhe von 10 Prozent des Erstattungsbetrages nach Absatz 1.

#### § 4 Fehlbetragsausgleich auf Antrag

(1) Der Antrag auf Ausgleich des Fehlbetrages (Fehlbetragsausgleich) nach § 1 Absatz 3 des Gesetzes über den Mehrbelastungsausgleich für kommunale Straßenausbaumaßnahmen ist schriftlich unter Beachtung des § 5 Absatz 3 bei der Erstattungsbehörde zu stellen.

(2) Erstattet werden die weggefallenen Beiträge für Straßenausbaumaßnahmen, für die die sachliche Beitragspflicht nach dem 31.
Dezember 2018 entstanden ist, soweit die Gemeinde die Beiträge aufgrund der am 31.
Dezember 2018 in der jeweiligen Gemeinde geltenden beitragsrechtlichen Regelungen hätte erheben können.
Dies gilt auch für Teileinrichtungen von Straßen.
Ebenfalls unberührt bleibt das Recht der Gemeinden auf Abschnittsbildung und Kostenspaltung, wie es am 31. Dezember 2018 nach dem in der Gemeinde geltenden Beitragsrecht zulässig war.

(3) Im Antrag auf Fehlbetragsausgleich ist die Höhe des rechnerisch auf die Beitragspflichtigen entfallenden umlagefähigen Aufwands der Straßenausbaumaßnahme darzulegen.
Von dieser Summe werden die bis zum Zeitpunkt der Antragstellung nach dieser Verordnung erhaltenen Zahlungen abgezogen.
Hierbei werden Zahlungen nach § 2 nicht berücksichtigt, die bereits für Straßenausbaumaßnahmen als Ersatz für nicht erhobene Straßenausbaubeiträge eingesetzt wurden sowie die in § 3 Absatz 2 geregelte Verwaltungskostenpauschale.
Die Berechnung hat die Gemeinde dem Antrag beizufügen.
Der Antrag auf Fehlbetragsausgleich kann sowohl mehrere Straßenbaumaßnahmen als auch verschiedene Kalenderjahre umfassen.

(4) Der umlagefähige Aufwand ist auf Grundlage der am 31.
Dezember 2018 geltenden kommunalabgabenrechtlichen Vorschriften der jeweiligen Gemeinde zu ermitteln.
Vorausleistungen werden höchstens in der Höhe gewährt, in der diese nach den am 31.
Dezember 2018 in der Gemeinde geltenden kommunalabgabenrechtlichen Regelungen erhoben werden konnten.
§ 8 Absatz 8 des Kommunalabgabengesetzes für das Land Brandenburg ist in der am 31. Dezember 2018 geltenden Fassung mit der Maßgabe, dass die Rückzahlung im Falle des Satzes 2 ohne Verlangen von Amts wegen zu erfolgen hat, entsprechend anwendbar.

#### § 5 Antrags- und Nachweisverfahren

(1) Dem Antrag auf Fehlbetragsausgleich sind bei der erstmaligen Antragstellung die entsprechende Satzung für Straßenbaubeiträge der jeweiligen Gemeinde in der am 31.
Dezember 2018 geltenden Fassung, im Übrigen folgende Angaben und Unterlagen beizufügen:

1.  die Entscheidung des zuständigen Organs der Gemeinde über die beabsichtigte Straßenausbaumaßnahme,
2.  Belege dafür, dass es sich um eine nach § 8 des Kommunalabgabengesetzes für das Land Brandenburg in der am 31.
    Dezember 2018 geltenden Fassung beitragsfähige Straßenbaumaßnahme handelt,
3.  die Höhe der Beitragsausfälle aufgrund der nicht erhobenen Beiträge infolge des Verbots der Beitragserhebung nach § 8 Absatz 1 Satz 2 des Kommunalabgabengesetzes für das Land Brandenburg und deren Berechnung nach § 4 Absatz 3 seit dem 1.
    Januar 2019,
4.  im Falle der Beantragung von Vorausleistungen nach § 4 Absatz 4 Satz 2 die Anzeige des Beginns, im Übrigen der Nachweis der Entstehung der sachlichen Beitragspflicht für die Straßenausbaumaßnahmen.

(2) Die Unterlagen müssen vollständig und prüffähig bei der Erstattungsbehörde eingereicht werden.
Die Erstattungsbehörde kann weitere Angaben und Unterlagen von der antragstellenden Gemeinde anfordern.

(3) Wird ein Antragsformular oder ein elektronisches Antragsverfahren zur Verfügung gestellt, ist dieses von den Gemeinden zu verwenden.
Soweit in einer Gemeinde dann die Voraussetzungen für eine elektronische Antragstellung noch nicht vorliegen, besteht ausnahmsweise die Möglichkeit der schriftlichen Antragstellung.

#### § 6 Berücksichtigung von pauschalem Mehrbelastungsausgleich künftiger Jahre

(1) Der an die Gemeinde erstattete Betrag nach § 4 wird mit den künftigen pauschalen Mehrbelastungsausgleichszahlungen nach § 2 verrechnet.

(2) Unbeschadet hiervon ist die Gemeinde berechtigt, weitere Anträge auf Fehlbetragsausgleich zu stellen.

#### § 7 Evaluation

Die Regelungen zum Mehrbelastungsausgleich gemäß § 1 Absatz 1 und 3 des Gesetzes über den Mehrbelastungsausgleich für kommunale Straßenausbaumaßnahmen werden gemäß § 1 Absatz 4 des Gesetzes über den Mehrbelastungsausgleich für kommunale Straßenausbaumaßnahmen evaluiert.
Dabei soll überprüft werden, ob die für den Ausgleich der Mehrbelastungen geschaffenen Regelungen einer pauschalen Zuweisung geeignet und angemessen sind.
Außerdem soll untersucht werden, ob ein anderer Verteilungsschlüssel besser geeignet wäre oder eine ausschließliche Spitzabrechnung gegenüber einer pauschalen Zuwendung vorzugswürdig wäre.
Die Evaluation besteht aus Datenerhebung, Auswertung und Evaluationsbericht.
Die Gemeinden haben die Pflicht, das Land oder von ihm Beauftragte mit den für die Evaluation erforderlichen Daten und Unterlagen zu unterstützen.

#### § 8 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 6.
September 2019

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider