## Gebührenordnung nach dem Gesetz zur Durchführung des Erneuerbare-Energien-Wärmegesetzes im Land Brandenburg (BbgEEWärmeGDGGebO)

Auf Grund des § 3 Absatz 1 des Gebührengesetzes für das Land Brandenburg vom 7. Juli 2009 (GVBl.
I S. 246) und des § 4 Absatz 1 des Gesetzes zur Durchführung des Erneuerbare-Energien-Wärmegesetzes im Land Brandenburg vom 4.
Dezember 2015 (GVBl.
I Nr. 34) verordnet die Ministerin für Infrastruktur und Landesplanung:

#### § 1 Kosten für Amtshandlungen

Die Landkreise und kreisfreien Städte erheben für ihre Amtshandlungen nach § 1 Absatz 1 Satz 2 Nummer 1 und 6 des Gesetzes zur Durchführung des Erneuerbare-Energien-Wärmegesetzes im Land Brandenburg Gebühren und Auslagen nach dieser Verordnung.

#### § 2 Gebührenbemessung

(1) Für die Erteilung von Befreiungen nach § 1 Absatz 1 Satz 2 Nummer 1 des Gesetzes zur Durchführung des Erneuerbare-Energien-Wärmegesetzes im Land Brandenburg wird eine Rahmengebühr in Höhe von 100 bis 2 000 Euro bestimmt.

(2) Für das Erheben von Gebühren und Auslagen für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 1 Absatz 1 Satz 2 Nummer 6 des Gesetzes zur Durchführung des Erneuerbare-Energien-Wärmegesetzes im Land Brandenburg gilt § 107 Absatz 1 und 3 des Gesetzes über Ordnungswidrigkeiten.

#### § 3 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 29.
Juni 2016

Die Ministerin für Infrastruktur und Landesplanung

Kathrin Schneider