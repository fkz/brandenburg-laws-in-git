## Verordnung über die Laufbahnen der Beamtinnen und Beamten des Landes Brandenburg (Laufbahnverordnung - LVO)

Auf Grund des § 25 des Landesbeamtengesetzes vom 3. April 2009 (GVBl. I S. 26), der zuletzt durch das Gesetz vom 29.
Juni 2018 (GVBl. I Nr. 17) geändert worden ist, verordnet die Landesregierung:

**Inhaltsübersicht**

Kapitel 1  
Allgemeines
-----------------------

[§ 1 Geltungsbereich](#1)

[§ 2 Förderung der Leistungsfähigkeit](#2)

[§ 3 Laufbahnen, Laufbahnordnungsbehörden](#3)

[§ 4 Ausschreibung](#4)

[§ 5 Erwerb der Laufbahnbefähigung](#5)

[§ 6 Zulassung zu einer höheren Laufbahn bei Besitz der erforderlichen Hochschulausbildung](#6)

[§ 7 Laufbahnwechsel](#7)

[§ 8 Einstellung in einem höheren Amt als dem Eingangsamt](#8)

[§ 9 Probezeit](#9)

[§ 10 Voraussetzungen einer Beförderung](#10)

[§ 11 Übertragung höher bewerteter Dienstposten](#11)

[§ 12 Laufbahnrechtliche Dienstzeiten](#12)

[§ 13 Nachteilsausgleich](#13)

[§ 14 Schwerbehinderte Menschen](#14)

[§ 15 Teilzeitbeschäftigung](#15)

[§ 16 Dienstliche und eigene Fortbildung](#16)

[§ 17 Anerkennung der bei Dienstherren außerhalb des Geltungsbereiches des Landesbeamtengesetzes erworbenen Laufbahnbefähigung](#17)

[§ 18 Übernahme von Richterinnen und Richtern](#18)

Kapitel 2  
Laufbahnen mit Vorbereitungsdienst
----------------------------------------------

### Abschnitt 1  
Gemeinsame Vorschriften

[§ 19 Einstellung der Laufbahnbewerberinnen und Laufbahnbewerber in den Vorbereitungsdienst](#19)

[§ 20 Vorbereitungsdienst, Laufbahnprüfung](#20)

[§ 21 Zulassung zum Aufstieg, Auswahlverfahren](#21)

[§ 22 Allgemeine Regelungen für den Aufstieg](#22)

[§ 23 Anerkennung externer Ausbildung, Befähigung für den gehobenen allgemeinen Verwaltungsdienst](#23)

### Abschnitt 2  
Gehobener Dienst

[§ 24 Besondere Einstellungsvoraussetzungen der technischen Laufbahnen mit Vorbereitungsdienst](#24)

[§ 25 Aufstieg in Laufbahnen mit Vorbereitungsdienst des gehobenen Dienstes](#25)

### Abschnitt 3  
Höherer Dienst

[§ 26 Aufstieg in Laufbahnen mit Vorbereitungsdienst des höheren Dienstes](#26)

Kapitel 3  
Laufbahnen ohne Vorbereitungsdienst
-----------------------------------------------

[§ 27 Allgemeines](#27)

[§ 28 Bildungsvoraussetzungen](#28)

[§ 29 Hauptberufliche Tätigkeit](#29)

[§ 30 Feststellung der Befähigung](#30)

[§ 31 Aufstieg in Laufbahnen ohne Vorbereitungsdienst](#31)

Kapitel 4  
Andere Bewerberinnen, andere Bewerber
-------------------------------------------------

[§ 32 Andere Bewerberinnen, andere Bewerber](#32)

Kapitel 5  
Ausnahmeentscheidungen des Landespersonalausschusses
----------------------------------------------------------------

[§ 33 Ausnahmeentscheidungen des Landespersonalausschusses](#33)

Kapitel 6  
Übergangs- und Schlussvorschriften
----------------------------------------------

[§ 34 Übergangsregelungen](#34)

[§ 35 Inkrafttreten, Außerkrafttreten](#35)

[Anlage 1 Laufbahnen mit geregelter Ausbildung (Vorbereitungsdienst) und Laufbahnordnungsbehörden](#36)  
[Anlage 2 Laufbahnen ohne Vorbereitungsdienst und Laufbahnordnungsbehörden](#36)  
[Anlage 3 Festlegung der Bildungsvoraussetzungen und von Besonderheiten der hauptberuflichen Tätigkeit der Laufbahnen ohne Vorbereitungsdienst](#36)

Kapitel 1  
Allgemeines
-----------------------

#### § 1 Geltungsbereich

(1) Diese Verordnung gilt für die Beamtinnen und Beamten des Landes, der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts, soweit sich aus ihr nichts anderes ergibt.
Sie gilt für die Beamtinnen und Beamten des Landesrechnungshofes, sofern das Landesrechnungshofgesetz vom 27. Juni 1991 (GVBl. I S. 256), das zuletzt durch Artikel 5 des Gesetzes vom 19. Juni 2019 (GVBl. I Nr. 40 S. 18) geändert worden ist, in der jeweils geltenden Fassung nichts anderes bestimmt.

(2) Diese Verordnung ist nicht anzuwenden auf

1.  hauptberufliches wissenschaftliches und künstlerisches Personal an Hochschulen mit Ausnahme der akademischen Mitarbeiterinnen und Mitarbeiter,
2.  Beamtinnen und Beamte des Polizeivollzugsdienstes,
3.  Beamtinnen und Beamte des feuerwehrtechnischen Dienstes,
4.  Beamtinnen und Beamte auf Zeit,
5.  Beamtinnen und Beamte des Schul- und Schulaufsichtsdienstes,
6.  Ehrenbeamtinnen und Ehrenbeamte.

#### § 2 Förderung der Leistungsfähigkeit

(1) Eignung, Befähigung und fachliche Leistung sollen durch geeignete Personalführungs- und -entwicklungsmaßnahmen erhalten und gefördert werden.
Neben den in den §§ 19 und 23 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl. I S. 26), das zuletzt durch das Gesetz vom 5. Juni 2019 (GVBl. I Nr. 19) geändert worden ist, genannten Maßnahmen gehören dazu insbesondere

1.  Mitarbeiter-Vorgesetzten-Gespräche,
2.  Zielvereinbarungen,
3.  die Möglichkeit der Einschätzung der Vorgesetzten durch die Mitarbeiterinnen und Mitarbeiter,
4.  ein die Fähigkeiten und Kenntnisse erweiternder Wechsel der Verwendung, wie auch die Tätigkeit bei internationalen Organisationen,
5.  die Führungskräftefortbildung und -entwicklung sowie
6.  Maßnahmen des Gesundheitsmanagements.

(2) Über die Einführung und Ausgestaltung der Personalführungs- und -entwicklungsmaßnahmen entscheidet die oberste Dienstbehörde; sie kann diese Befugnis auf nachgeordnete Stellen übertragen.

#### § 3 Laufbahnen, Laufbahnordnungsbehörden

Die im Land Brandenburg eingerichteten Laufbahnen mit Vorbereitungsdienst ergeben sich aus Anlage 1, die Laufbahnen ohne Vorbereitungsdienst aus Anlage 2.
Laufbahnordnungsbehörden sind die in den Anlagen 1 und 2 genannten obersten Dienstbehörden.

#### § 4 Ausschreibung

(1) Für Einstellungen sind die Bewerberinnen und Bewerber durch öffentliche Stellenausschreibung zu ermitteln.
Stellen, die für die Übernahme von bereits vorhandenen Arbeitnehmerinnen und Arbeitnehmern in ein Beamtenverhältnis vorgesehen sind, sind innerhalb der Behörde oder Einrichtung auszuschreiben.

(2) Freie Beförderungsdienstposten sind mindestens innerhalb der Behörde oder Einrichtung auszuschreiben.
Ein Beförderungsdienstposten gilt dann nicht als frei, wenn das Amt, das der Beamtin oder dem Beamten verliehen ist, der Wertigkeit des Beförderungsdienstpostens, der ihr oder ihm übertragen ist, noch nicht entspricht; dies gilt auch für die Fälle einer Anhebung des Dienstpostens innerhalb der Laufbahngruppe.
Ämter mit leitender Funktion im Beamtenverhältnis auf Probe gemäß § 120 des Landesbeamtengesetzes sind mindestens innerhalb der Landesverwaltung auszuschreiben.

(3) Die Absätze 1 und 2 gelten nicht für

1.  Stellen, die mit Beamtinnen und Beamten auf Probe besetzt werden, die aufgrund eines Auswahlverfahrens im Beamtenverhältnis auf Widerruf im Land Brandenburg ausgebildet wurden und deren Einstellung im Anschluss an die Ausbildung erfolgen soll,
2.  Stellen der in § 105 des Landesbeamtengesetzes genannten Beamtinnen und Beamten,
3.  Stellen, die durch Umsetzung oder durch Versetzung, mit denen keine Beförderung verbunden ist oder vorbereitet wird, besetzt werden,
4.  Stellen, die mit Personen besetzt werden, die aufgrund von Rechtsvorschriften einen Anspruch auf Einstellung oder Wiederverwendung haben,
5.  Stellen, die mit Arbeitnehmerinnen und Arbeitnehmern besetzt sind, die im Ergebnis einer öffentlichen Ausschreibung eingestellt wurden, bei der die Möglichkeit einer Berufung in das Beamtenverhältnis in Aussicht gestellt worden war.

Über Ausnahmen von der Ausschreibungspflicht nach den Absätzen 1 und 2 für Stellen des Verfassungsschutzes entscheidet das hierfür zuständige Ministerium.

(4) Die obersten Dienstbehörden oder die von ihnen bestimmten Stellen regeln im Übrigen Art und Umfang der Ausschreibungen und ihre Bekanntmachung.

(5) § 7 des Landesgleichstellungsgesetzes vom 4.
Juli 1994 (GVBl. I S. 254), das zuletzt durch Artikel 21 des Gesetzes vom 8. Mai 2018 (GVBl. I Nr. 8 S. 18) geändert worden ist, in der jeweils geltenden Fassung bleibt unberührt.

#### § 5 Erwerb der Laufbahnbefähigung

(1) Laufbahnbewerberinnen und Laufbahnbewerber erwerben die Laufbahnbefähigung durch

1.  Ableisten des Vorbereitungsdienstes und Bestehen der Laufbahnprüfung,
2.  Feststellung der Laufbahnbefähigung für eine Laufbahn ohne Vorbereitungsdienst nach Kapitel 3,
3.  Einführung und Bestehen der vorgeschriebenen Aufstiegsprüfung oder des für den Aufstieg vorgeschriebenen Befähigungsfeststellungsverfahrens,
4.  Zulassung zu einer höheren Laufbahn und Bestehen der Laufbahnprüfung oder Feststellung der Laufbahnbefähigung für eine Laufbahn ohne Vorbereitungsdienst gemäß § 6,
5.  Laufbahnwechsel gemäß § 7 Absatz 1,
6.  Anerkennung einer im Bereich anderer Dienstherren erworbenen Befähigung gemäß § 17,
7.  Anerkennung einer außerhalb des Vorbereitungsdienstes absolvierten gleichwertigen Ausbildung gemäß § 23,
8.  Anerkennung der Befähigung für die nächstniedrige Laufbahn durch die Prüfungsbehörde gemäß § 20 Absatz 2,
9.  Anerkennung der Laufbahnbefähigung als Befähigung für die nächstniedrige Laufbahn gemäß § 7 Absatz 2 oder § 9 Absatz 3 Satz 6.

(2) Darüber hinaus erwerben Laufbahnbewerberinnen und Laufbahnbewerber die Laufbahnbefähigung aufgrund des Gemeinschaftsrechts durch Anerkennung nach der EU‑Laufbahnbefähigungsanerkennungsverordnung vom 2. Februar 2016 (GVBl. II Nr. 4).

(3) Andere Bewerberinnen und Bewerber erwerben die Befähigung nach § 32.

#### § 6 Zulassung zu einer höheren Laufbahn bei Besitz der erforderlichen Hochschulausbildung

(1) Beamtinnen und Beamte auf Lebenszeit, die die für eine höhere Laufbahn erforderliche Hochschulausbildung besitzen, können nach der erfolgreichen Teilnahme an einem Auswahlverfahren zur höheren Laufbahn zugelassen werden.

(2) Die ausgewählten Beamtinnen und Beamten verbleiben in ihrem bisherigen beamtenrechtlichen Status, bis sie

1.  für den gehobenen Dienst die in § 10 Absatz 3 Nummer 2 des Landesbeamtengesetzes geforderten sonstigen Voraussetzungen oder
2.  für den höheren Dienst die in § 10 Absatz 4 Nummer 2 des Landesbeamtengesetzes geforderten sonstigen Voraussetzungen

erfüllen und sich nach Erlangung der Befähigung sechs Monate in der neuen Laufbahn bewährt haben.
Ist für die höhere Laufbahn ein Vorbereitungsdienst eingerichtet, nehmen die Beamtinnen und Beamten an dem entsprechenden Ausbildungsgang teil und legen die vorgeschriebene Prüfung ab.
Für höhere Laufbahnen ohne Vorbereitungsdienst gelten die §§ 29 und 30 entsprechend.

(3) § 10 Absatz 2 Satz 2 gilt entsprechend.

#### § 7 Laufbahnwechsel

(1) Der Wechsel in eine andere Laufbahn derselben Laufbahngruppe ist auch aus einer Laufbahn außerhalb des Geltungsbereichs dieser Verordnung zulässig, wenn die Beamtin oder der Beamte die Befähigung für die neue Laufbahn besitzt oder nach § 21 Absatz 2 bis 4 des Landesbeamtengesetzes erworben hat.

(2) Die Laufbahnbefähigung kann als Befähigung für die nächstniedrigere Laufbahn anerkannt werden, wenn hieran ein dienstliches Interesse besteht.
Über die Anerkennung der Befähigung entscheidet die oberste Dienstbehörde im Einvernehmen mit der für die neue Laufbahn zuständigen Laufbahnordnungsbehörde.

#### § 8 Einstellung in einem höheren Amt als dem Eingangsamt

(1) Einstellung ist eine Ernennung unter Begründung eines Beamtenverhältnisses.

(2) Eine Einstellung im ersten oder zweiten Beförderungsamt einer Laufbahn ist zulässig, wenn

1.  die Bewerberin oder der Bewerber nachweisbar berufliche Erfahrungen besitzt, die zusätzlich zu den in § 10 des Landesbeamtengesetzes geregelten Zugangsvoraussetzungen erworben wurden und die nach ihrer Art, Schwierigkeit, Bedeutung und Dauer den Eignungsvoraussetzungen für das angestrebte Beförderungsamt mindestens gleichwertig sind, oder
2.  die Bewerberin oder der Bewerber eine für die Laufbahn förderliche, über die Einstellungsvoraussetzungen erheblich hinausgehende besondere fachliche Qualifikation nachweist.

In den Fällen des Satzes 1 Nummer 1 muss das Beförderungsamt nach dem individuellen fiktiven Werdegang erreichbar sein.
Berufliche Bildungsgänge, Qualifikationen und Zeiten, die nach den Laufbahnvorschriften auf eine Ausbildung angerechnet wurden oder Voraussetzung für den Erwerb der Befähigung sind, dürfen nicht berücksichtigt werden.

(3) Die Entscheidung nach Absatz 2 trifft die oberste Dienstbehörde im Einvernehmen mit der Laufbahnordnungsbehörde.

#### § 9 Probezeit

(1) Die Probezeit ist die Zeit im Beamtenverhältnis auf Probe, während der sich die Beamtinnen und Beamten nach Erwerb der Laufbahnbefähigung für ihre Laufbahn bewähren sollen.
Die Probezeit soll insbesondere zeigen, ob die Beamtinnen und Beamten nach Eignung, Befähigung und fachlicher Leistung in der Lage sind, die Aufgaben der Laufbahn zu erfüllen.
Sie soll zugleich erste Erkenntnisse vermitteln, für welche Verwendungen die Beamtinnen und Beamten besonders geeignet erscheinen.
Während der Probezeit sollen die Beamtinnen und Beamten auf mindestens zwei Dienstposten in unterschiedlichen Aufgabenbereichen eingesetzt werden; die Aufgabenübertragung soll jeweils die Dauer von sechs Monaten nicht unterschreiten.
Beamtinnen und Beamte oberster Landesbehörden des gehobenen und des höheren Dienstes sollen zudem für mindestens sechs Monate außerhalb einer obersten Landesbehörde eingesetzt werden (Außenprobezeit).

(2) Wenn die besonderen Verhältnisse der Laufbahn es erfordern, kann die Laufbahnordnungsbehörde durch Verwaltungsvorschriften bestimmen, dass die Beamtinnen und Beamten in ausgewählten Tätigkeitsbereichen in die Aufgaben der Laufbahn eingeführt werden.
Die Einführung kann praxisbezogene Lehrveranstaltungen umfassen.
Die Einführungszeit soll ein Jahr nicht überschreiten.

(3) Eignung, Befähigung und fachliche Leistung sind unter Anlegung eines strengen Maßstabes während der Probezeit wiederholt zu bewerten.
Eine erste Bewertung soll spätestens bis zum Ablauf der Hälfte der abzuleistenden Probezeit erfolgen.
Bei Ablauf der Probezeit wird abschließend festgestellt, ob sich die Beamtin oder der Beamte bewährt hat.
Wenn die Bewährung bis zum Ablauf der Probezeit nicht festgestellt werden kann, kann die Probezeit um höchstens zwei Jahre verlängert werden.
Ergeben sich infolge einer Beurlaubung ohne Dienstbezüge mit Ausnahme der Fälle des Absatzes 4 oder infolge von Krankheit Fehlzeiten von insgesamt mehr als drei Monaten, so verlängert sich der maßgebliche Zeitraum der Probezeit entsprechend.
Die Beamtinnen und Beamten können statt der Entlassung wegen mangelnder Bewährung mit ihrer Zustimmung in die nächstniedrigere Laufbahn derselben Fachrichtung übernommen werden, wenn sie hierfür geeignet sind und ein dienstliches Interesse vorliegt; § 7 Absatz 2 Satz 2 gilt entsprechend.

(4) Als Probezeit gilt die Zeit einer im öffentlichen oder dienstlichen Interesse liegenden Beurlaubung, wenn eine den Laufbahnanforderungen gleichwertige hauptberufliche Tätigkeit ausgeübt wird.
Das Ableisten der Mindestprobezeit von einem Jahr bleibt unberührt.
Das Vorliegen der Voraussetzungen nach Satz 1 ist bei Gewährung des Urlaubs von der obersten Dienstbehörde oder der von ihr bestimmten Stelle, bei Beamtinnen und Beamten des Landes im Einvernehmen mit dem für das allgemeine öffentliche Dienstrecht und dem für das finanzielle öffentliche Dienstrecht zuständigen Ministerium, schriftlich festzustellen.
Der Zeit eines Urlaubs nach Satz 1 steht die Zeit einer von der obersten Dienstbehörde oder der von ihr bestimmten Stelle angeordneten Tätigkeit bei einer öffentlichen zwischenstaatlichen oder überstaatlichen Einrichtung und den kommunalen Spitzenverbänden gleich.

(5) Zeiten hauptberuflicher Tätigkeiten innerhalb (§ 28 des Brandenburgischen Besoldungsgesetzes) oder außerhalb des öffentlichen Dienstes, die nicht schon auf den Vorbereitungsdienst angerechnet oder als berufliche Erfahrung nach § 8 Absatz 2 Satz 1 Nummer 1 oder als hauptberufliche Tätigkeit nach § 29 berücksichtigt oder als Zeiten für die Feststellung der Berufserfahrung nach § 32 zugrunde gelegt worden sind, sollen auf die Probezeit angerechnet werden, wenn die Tätigkeit nach Art, Schwierigkeit und Bedeutung mindestens der Tätigkeit in einem Amt der betreffenden Laufbahn gleichwertig ist.
Die Entscheidung über die Anrechnung von Dienstzeiten außerhalb des öffentlichen Dienstes bedarf der Zustimmung der obersten Dienstbehörde.
Auf die Probezeit wird auch die Zeit einer hauptberuflichen Tätigkeit als kommunale Wahlbeamtin oder kommunaler Wahlbeamter, die nach Erwerb einer Laufbahnbefähigung nach § 5 Absatz 1 Nummer 1 und 2 geleistet worden ist, angerechnet.
Das Ableisten der Mindestprobezeit von einem Jahr bleibt unberührt.

(6) Die Probezeit kann um höchstens ein Jahr gekürzt werden, wenn sich die Beamtin oder der Beamte in der Probezeit besonders bewährt und die Laufbahnprüfung besser als mit der Note „befriedigend“ bestanden hat.
Das Ableisten der Mindestprobezeit von einem Jahr bleibt unberührt.

(7) Die Anwendung der Absätze 4 und 5 steht einer Verlängerung der abzuleistenden Probezeit bei mangelnder Bewährung bis zu der nach § 10 des Beamtenstatusgesetzes vom 17. Juni 2008 (BGBl. I S. 1010), das zuletzt durch Artikel 1 des Gesetzes vom 29. November 2018 (BGBl. I S. 2232) geändert worden ist, in Verbindung mit § 18 Absatz 2 des Landesbeamtengesetzes vorgesehenen Höchstgrenze nicht entgegen.

#### § 10 Voraussetzungen einer Beförderung

(1) Beförderung ist die Verleihung eines anderen Amtes mit höherem Endgrundgehalt; Amtszulagen gelten als Bestandteil des Grundgehaltes.
Eine Beamtin oder ein Beamter kann befördert werden, wenn

1.  sie oder er nach Eignung, Befähigung und fachlicher Leistung ausgewählt worden ist,
2.  die Eignung für die höherwertige Funktion in einer Erprobungszeit nachgewiesen wurde und
3.  kein Beförderungsverbot vorliegt.

(2) Die Ämter der Bundesbesoldungsordnung A sind regelmäßig zu durchlaufen.
Bei einem Aufstieg in die nächsthöhere Laufbahngruppe brauchen die noch nicht erreichten Ämter der bisherigen Laufbahngruppe nicht durchlaufen zu werden.

#### § 11 Übertragung höher bewerteter Dienstposten

(1) Für einen Dienstposten mit höher bewerteter Funktion ist die Eignung in einer Erprobungszeit nachzuweisen.
Die Erprobungszeit beträgt für Ämter der Besoldungsgruppen:

1.  A 5 und A 6 mindestens drei Monate,
2.  A 7 bis A 9 mindestens sechs Monate,
3.  A 10 bis A 13 mindestens neun Monate,
4.  A 14 bis A 16 und der Besoldungsordnung B mindestens ein Jahr.

(2) Die Erprobungszeit gilt als geleistet, soweit sich die Beamtin oder der Beamte in Tätigkeiten eines Dienstpostens mindestens gleicher Bewertung bewährt hat.
Sie gilt auch als geleistet, wenn sich die Beamtin oder der Beamte während einer in öffentlichem oder dienstlichem Interesse liegenden Beurlaubung in Tätigkeiten bei einer öffentlichen zwischenstaatlichen oder überstaatlichen Einrichtung, bei einem kommunalen Spitzenverband oder als wissenschaftliche Assistentin oder wissenschaftlicher Assistent oder Geschäftsführerin oder Geschäftsführer bei Fraktionen des Europäischen Parlaments, des Deutschen Bundestages oder eines Landtages bewährt hat und die ausgeübten Tätigkeiten nach Art und Schwierigkeit mindestens den Anforderungen des Dienstpostens mit höher bewerteter Funktion entsprochen haben.

(3) Die Erprobung kann, wenn die sonstigen Voraussetzungen nach dieser Verordnung erfüllt sind, im Rahmen der Probezeit stattfinden.

(4) Wenn die Eignung nicht festgestellt werden kann, ist von der dauerhaften Übertragung des Dienstpostens abzusehen oder die probeweise Übertragung zu widerrufen.
Die Entscheidung soll spätestens nach Ablauf der doppelten Dauer der in Absatz 1 festgelegten Mindestzeiten getroffen werden.

#### § 12 Laufbahnrechtliche Dienstzeiten

(1) Laufbahnrechtliche Dienstzeiten rechnen vom Zeitpunkt der Ernennung zur Beamtin oder zum Beamten auf Lebenszeit an.
Erfolgte vor dem 9. April 2009 die Verleihung eines Amtes vor der Ernennung zur Beamtin oder zum Beamten auf Lebenszeit, rechnen die Dienstzeiten vom Zeitpunkt der Verleihung des Amtes an.
Dienstzeiten, die über die im Einzelfall festgelegte Probezeit hinaus geleistet worden sind, sind anzurechnen.
Zeiten einer Beurlaubung ohne Dienstbezüge sind keine laufbahnrechtlichen Dienstzeiten.

(2) Abweichend von Absatz 1 Satz 1 gelten Zeiten des Grundwehrdienstes und von Wehrübungen sowie die Zeit des Zivildienstes als laufbahnrechtliche Dienstzeiten.

(3) Abweichend von Absatz 1 Satz 4 gelten als laufbahnrechtliche Dienstzeiten:

1.  die Zeit eines Urlaubs, wenn der Urlaub für eine Tätigkeit als wissenschaftliche Assistentin oder wissenschaftlicher Assistent oder Geschäftsführerin oder Geschäftsführer bei Fraktionen des Europäischen Parlaments, des Deutschen Bundestages oder eines Landtages sowie bei kommunalen Spitzenverbänden erteilt oder unter vollständiger oder teilweiser Fortgewährung der Dienstbezüge erteilt wurde, in den übrigen Fällen einer im öffentlichen oder dienstlichen Interesse liegenden Beurlaubung nur bis zu einer Dauer von insgesamt zwei Jahren,
2.  die Zeit einer Freistellung nach der Mutterschutz- und Elternzeitverordnung vom 12. Februar 2009 (BGBl. I S. 320), die zuletzt durch Artikel 1 der Verordnung vom 9. Februar 2018 (BGBl. I S. 198) geändert worden ist, in der jeweils geltenden Fassung oder einer Beurlaubung nach § 80 Absatz 1 Satz 1 Nummer 2 des Landesbeamtengesetzes; dabei wird jeweils der Zeitraum der tatsächlichen Verzögerung bis zu einem Jahr zugrunde gelegt, insgesamt höchstens bis zu drei Jahren.

In den Fällen des Satzes 1 Nummer 1 ist § 9 Absatz 4 Satz 3 entsprechend anzuwenden.

(4) Zeiten, die nach dem Bestehen einer Laufbahnprüfung oder nach dem Zeitpunkt des Befähigungserwerbs nach § 30 Absatz 2 als Arbeitnehmerin oder Arbeitnehmer im öffentlichen Dienst zurückgelegt worden sind, sollen auf die Dienstzeit angerechnet werden, wenn

1.  die Übernahme in ein Beamtenverhältnis auf Probe aus nicht von der Beamtin oder dem Beamten zu vertretenden Gründen unterblieben ist,
2.  die Tätigkeit nach Art und Bedeutung mindestens der Tätigkeit in einem Amt der betreffenden Laufbahn entsprochen hat und
3.  sie nicht schon auf die Probezeit angerechnet wurden.

(5) Zeiten einer hauptberuflichen Tätigkeit als kommunale Wahlbeamtin oder kommunaler Wahlbeamter, die nach Erwerb einer Laufbahnbefähigung geleistet worden sind, können auf die laufbahnrechtliche Dienstzeit angerechnet werden, sofern sie nicht bereits nach § 9 Absatz 5 Satz 3 auf die Probezeit angerechnet worden sind.

#### § 13 Nachteilsausgleich

(1) Der Ausgleich einer Verzögerung des beruflichen Werdegangs durch eine Beförderung während der Probezeit oder vor Ablauf eines Jahres seit Beendigung der Probezeit nach § 24 Absatz 2 des Landesbeamtengesetzes setzt voraus, dass die Beamtin oder der Beamte sich innerhalb von sechs Monaten oder im Falle fester Einstellungstermine zum nächsten Einstellungstermin nach Beendigung der Betreuung oder Pflege oder Abschluss der im Anschluss an die Betreuung oder Pflege begonnenen oder fortgesetzten vorgeschriebenen Ausbildung beworben hat und diese Bewerbung zur Einstellung geführt hat.
Als Ausgleich können je Kind die tatsächliche Verzögerung bis zu einem Zeitraum von einem Jahr, bei mehreren Kindern höchstens drei Jahre angerechnet werden.
Werden in einem Haushalt mehrere Kinder gleichzeitig betreut, wird für denselben Zeitraum der Ausgleich nur einmal gewährt.
Bei einer gleichzeitigen Kinderbetreuung durch mehrere Personen erhält nur eine Person den Ausgleich.
Für die Pflege eines pflegebedürftigen nahen Angehörigen kann die tatsächliche Verzögerung bis zu einem Zeitraum von einem Jahr angerechnet werden.

(2) Für den Ausgleich einer Verzögerung des beruflichen Werdegangs durch Zeiten des Wehr- oder Zivildienstes sowie gleichgestellte Zeiten, soweit

1.  das Arbeitsplatzschutzgesetz in der Fassung der Bekanntmachung vom 16. Juli 2009 (BGBl. I S. 2055), das zuletzt durch Artikel 17 des Gesetzes vom 4.
    August 2019 (BGBl. I S. 1147) geändert worden ist, in der jeweils geltenden Fassung,
2.  das Zivildienstgesetz in der Fassung der Bekanntmachung vom 17. Mai 2005 (BGBl. I S. 1346), das zuletzt durch Artikel 23 des Gesetzes vom 4. August 2019 (BGBl. I S. 1147) geändert worden ist, in der jeweils geltenden Fassung,
3.  das Entwicklungshelfer-Gesetz vom 18. Juni 1969 (BGBl. I S. 549), das zuletzt durch Artikel 6 Absatz 13 des Gesetzes vom 23. Mai 2017 (BGBl. I S. 1228, 1241) geändert worden ist, in der jeweils geltenden Fassung oder
4.  das Soldatenversorgungsgesetz in der Fassung der Bekanntmachung vom 16. September 2009 (BGBl. I S. 3054), das zuletzt durch Artikel 19 des Gesetzes vom 4. August 2019 (BGBl. I S. 1147) geändert worden ist, in der jeweils geltenden Fassung

die Vornahme eines Ausgleichs beruflicher Verzögerungen, die durch die im jeweiligen Dienstverhältnis verbrachten Zeiten eintreten würden, anordnen, gilt Absatz 1 Satz 1 entsprechend.

#### § 14 Schwerbehinderte Menschen

(1) Von schwerbehinderten Menschen darf bei der Einstellung, Übertragung höherbewerteter Dienstposten, Beförderung und bei der Zulassung zum Aufstieg nur das Mindestmaß körperlicher Eignung verlangt werden.

(2) Im Prüfungsverfahren und bei der Erstellung von Leistungsnachweisen sind für schwerbehinderte Menschen die ihrer Behinderung angemessenen Erleichterungen vorzusehen.
Die fachlichen Anforderungen dürfen nicht geringer bemessen werden.

(3) Bei der Beurteilung der Leistung schwerbehinderter Menschen ist eine etwaige Minderung der Arbeits- und Verwendungsfähigkeit aufgrund der Behinderung zu berücksichtigen.

#### § 15 Teilzeitbeschäftigung

Bei der Anwendung dieser Verordnung sind Zeiten einer Teilzeitbeschäftigung mit mindestens der Hälfte der regelmäßigen Arbeitszeit der Beamtinnen und Beamten des Landes und regelmäßige Arbeitszeiten gleich zu behandeln.
Zeiten einer Teilzeitbeschäftigung mit weniger als der Hälfte der regelmäßigen Arbeitszeit werden entsprechend ihrem Verhältnis zur hälftigen Beschäftigung berücksichtigt.
Die Regelungen über den Vorbereitungsdienst und den Aufstieg bleiben hiervon unberührt.

#### § 16 Dienstliche und eigene Fortbildung

(1) Die Entwicklung neuer Arbeitsmethoden, der Einsatz technikunterstützter Informationsverarbeitung sowie der Wandel und die notwendige und vorausschauende Anpassung der Aufgaben und der Leistungsfähigkeit des öffentlichen Dienstes an sich verändernde gesellschaftliche, technologische und wirtschaftliche Bedingungen erfordern eine ständige Fortbildung der Beamtinnen und Beamten.
Die dienstliche Fortbildung ist deshalb von allen Dienstherren besonders zu fördern.

(2) Die Beamtinnen und Beamten sind insbesondere verpflichtet, an Maßnahmen der dienstlichen Fortbildung teilzunehmen, die

1.  der Erhaltung und Verbesserung der Befähigung für ihre Dienstposten oder für gleich bewertete Dienstposten dienen,
2.  bei Änderungen der Laufbahnausbildung eine Angleichung an den neuen Befähigungsstand zum Ziel haben,
3.  der Erhaltung und Weiterentwicklung der Kompetenzen dienen, die zum Beispiel zur Förderung der Vielfalt in der öffentlichen Verwaltung und zur Gestaltung digitaler Veränderungsprozesse erforderlich sind.

(3) Die Beamtinnen und Beamten sind außerdem verpflichtet, sich selbst fortzubilden, damit sie über die Änderungen der Aufgaben und der Anforderungen in der Laufbahn unterrichtet und steigenden Anforderungen gewachsen sind.

(4) Nach den Erfordernissen der Personalplanung und des Personaleinsatzes sind Fortbildungsangebote vorzusehen, die zum Ziel haben, die Befähigung für höher bewertete Dienstgeschäfte zu vermitteln.

(5) Beamtinnen und Beamte, die durch Fortbildung ihre Fähigkeiten und fachlichen Kenntnisse nachweislich wesentlich gesteigert haben, sollen gefördert werden.
Insbesondere soll ihnen nach Möglichkeit Gelegenheit gegeben werden, ihre Fähigkeiten und fachlichen Kenntnisse in höher bewerteten Dienstposten anzuwenden und hierbei ihre besondere Eignung nachzuweisen.

(6) Als Nachweis besonderer fachlicher Kenntnisse im Sinne des Absatzes 5 sind beispielsweise

1.  das Diplom einer Verwaltungs- und Wirtschaftsakademie im Land Brandenburg, das nach einer von dem für das allgemeine öffentliche Dienstrecht zuständigen Ministerium anerkannten Prüfungsordnung erworben worden ist,
2.  das anerkannte Diplom einer Verwaltungs- und Wirtschaftsakademie eines anderen Landes der Bundesrepublik Deutschland oder
3.  Abschlüsse von anderen vergleichbaren Institutionen anzusehen.

#### § 17 Anerkennung der bei Dienstherren außerhalb des Geltungsbereiches des Landesbeamtengesetzes erworbenen Laufbahnbefähigung

(1) Wer eine Laufbahnbefähigung nach den Vorschriften eines anderen Landes oder des Bundes erworben hat, besitzt außer in den Fällen des Absatzes 2 die Befähigung für die entsprechende Laufbahn im Land Brandenburg.
Dies gilt für Bewerberinnen und Bewerber,

1.  die eine Befähigung nach § 14 Absatz 2 des Landesbeamtengesetzes besitzen und
2.  die eine Befähigung erworben haben, deren Voraussetzungen aufgrund der verwandten Vor- und Ausbildung denen nach § 5 Absatz 1 Nummer 1, 2, 4, 7 und 8 vergleichbar sind.

In Zweifelsfällen stellt die oberste Dienstbehörde im Einvernehmen mit der Laufbahnordnungsbehörde fest, ob die Voraussetzungen vorliegen.

(2) Liegen die Voraussetzungen nach Absatz 1 nicht vor, kann die Laufbahnbefähigung als Befähigung für eine vergleichbare Laufbahn anerkannt werden, wenn aufgrund der Vor- und Ausbildung sowie der vorhandenen Berufserfahrung ein Befähigungsstand nachgewiesen ist, der einer Laufbahnbefähigung nach dieser Verordnung gleichwertig ist.
Liegt die Gleichwertigkeit nach Satz 1 nicht vor, ist eine Anerkennung der Laufbahnbefähigung nach Absolvieren von Ausgleichsmaßnahmen nur zulässig, wenn zu erwarten ist, dass die Defizite durch Unterweisung und Anpassungsfortbildung innerhalb von sechs Monaten ausgeglichen werden können.
In Zweifelsfällen stellt die oberste Dienstbehörde im Einvernehmen mit der Laufbahnordnungsbehörde fest, ob die Voraussetzungen für die Anerkennung der Befähigung vorliegen.

(3) Von der Ableistung einer Probezeit ist abzusehen, soweit sich die Beamtinnen und Beamten bei anderen Dienstherren nach Erwerb der Befähigung in einer vergleichbaren Laufbahn bewährt haben.
Eine nicht beendete frühere oder vorhergehende Probezeit ist unter den gleichen Voraussetzungen anzurechnen.

(4) Für die Übernahme von kommunalen Wahlbeamtinnen und Wahlbeamten sind die §§ 8, 9 Absatz 5 Satz 3 und § 12 Absatz 5 entsprechend anzuwenden.

(5) Bei anderen Bewerberinnen und anderen Bewerbern, deren Befähigungsfeststellung außerhalb des Geltungsbereiches dieser Verordnung vorgenommen wurde, erfolgt die Feststellung der Befähigung für eine Laufbahn im Geltungsbereich dieser Verordnung durch den Landespersonalausschuss.

#### § 18 Übernahme von Richterinnen und Richtern

Treten Richterinnen und Richter, die ein Amt der Besoldungsgruppe R 1 innehaben, in die Laufbahn des höheren allgemeinen Verwaltungsdienstes ein, kann ihnen ein Amt der Besoldungsgruppe A 14 frühestens fünf Jahre, ein Amt der Besoldungsgruppe A 15 frühestens sechs Jahre nach der Ernennung zur Richterin oder zum Richter auf Probe übertragen werden.
Richterinnen und Richtern der Besoldungsgruppe R 2 kann ein Amt der Besoldungsgruppe A 16 übertragen werden.
Die Sätze 1 und 2 gelten für Staatsanwältinnen und Staatsanwälte entsprechend.

Kapitel 2  
Laufbahnen mit Vorbereitungsdienst
----------------------------------------------

### Abschnitt 1  
Gemeinsame Vorschriften

#### § 19 Einstellung der Laufbahnbewerberinnen und Laufbahnbewerber in den Vorbereitungsdienst

Die ausgewählten Bewerberinnen und Bewerber werden als Beamtinnen und Beamte auf Widerruf in den Vorbereitungsdienst der betreffenden Laufbahn eingestellt.
Sie führen während des Vorbereitungsdienstes die Dienstbezeichnung „Anwärterin“ oder „Anwärter“, in Laufbahnen des höheren Dienstes die Dienstbezeichnung „Referendarin“ oder „Referendar“, je mit einem die Fachrichtung oder die Laufbahn bezeichnenden Zusatz.
Die Laufbahnordnungsbehörde kann im Einvernehmen mit dem für das allgemeine öffentliche Dienstrecht und dem für das finanzielle öffentliche Dienstrecht zuständigen Ministerium andere Dienstbezeichnungen festsetzen.

#### § 20 Vorbereitungsdienst, Laufbahnprüfung

(1) Der Vorbereitungsdienst schließt mit der Laufbahnprüfung ab.
Die Laufbahnprüfung kann in Form von Modulprüfungen durchgeführt werden.

(2) In Laufbahnen des gehobenen Dienstes kann die Prüfungsbehörde bei Nichtbestehen der Laufbahnprüfung die Befähigung für die Laufbahn gleicher Fachrichtung des mittleren Dienstes anerkennen, wenn die nachgewiesenen Kenntnisse ausreichen.

(3) Das Bestehen der Laufbahnprüfung begründet keinen Anspruch auf Ernennung zur Beamtin oder zum Beamten auf Probe.

(4) Das Nähere zur Ausgestaltung der Ausbildung und Prüfung der Beamtinnen und Beamten im Vorbereitungsdienst regeln die Ausbildung- und Prüfungsordnungen.

#### § 21 Zulassung zum Aufstieg, Auswahlverfahren

(1) Beamtinnen und Beamte können zum Aufstieg in die nächsthöhere Laufbahn derselben Fachrichtung vorgeschlagen werden oder sich bewerben.

(2) Der Entscheidung über eine Zulassung zur Einführung in die nächsthöhere Laufbahn geht ein Auswahlverfahren voraus, in dem die Eignung der Beamtinnen und Beamten unter Berücksichtigung der künftigen Laufbahnaufgaben und der Anforderungen der vorgesehenen Einführung festzustellen ist.
Die für die Zulassungsentscheidung zuständige Stelle kann auf der Grundlage der dienstlichen Beurteilungen und sonstiger Anforderungen eine Vorauswahl treffen.
Die Eignung der Beamtinnen und Beamten, in den Fällen des Satzes 2 der Beamtinnen und Beamten, die nach der Vorauswahl grundsätzlich für einen Aufstieg in Betracht kommen, ist mindestens in einer Vorstellung vor einer Auswahlkommission nachzuweisen.

(3) Über die Zulassung zur Einführung in die nächsthöhere Laufbahn entscheidet die oberste Dienstbehörde oder die von ihr bestimmte Stelle aufgrund des Vorschlags der Auswahlkommission.
Die Entscheidung kann auch Bewerberinnen und Bewerber eines früheren Auswahlverfahrens berücksichtigen, wenn deren Eignungsfeststellung vergleichbar gestaltet war.
Soweit es mit den Zielen des Aufstiegs vereinbar ist, soll auch Teilzeitbeschäftigten der Aufstieg ermöglicht werden.
Näheres zur Ermöglichung von Teilzeitbeschäftigung und berufsbegleitender Aufstiegsausbildung regeln die Ausbildungs- und Prüfungsordnungen.

(4) Beamtinnen und Beamte können nach Maßgabe der Ausbildungs- und Prüfungsordnungen mehrmals an einem Auswahlverfahren teilnehmen.
Ist ein Aufstieg durch Ausbildungs- und Prüfungsordnung nicht geregelt, ist mindestens eine einmalige Wiederholung zuzulassen.

#### § 22 Allgemeine Regelungen für den Aufstieg

(1) Bei der Auswahl und Gestaltung der Aufstiegsverfahren sind die Benachteiligungsverbote des § 24 Absatz 1 des Landesbeamtengesetzes zu beachten.
Berufsbegleitende und modularisierte Aufstiegsverfahren sollen angeboten werden.

(2) Nach der Zulassung zum Aufstieg werden die Beamtinnen und Beamten nach Maßgabe der Ausbildungs- und Prüfungsordnungen in die Aufgaben der neuen Laufbahn eingeführt.
Die Einführung schließt mit der Aufstiegsprüfung ab.

(3) Die Beamtinnen und Beamten verbleiben bis zur Verleihung eines Amtes der neuen Laufbahn in ihrer bisherigen Rechtsstellung.
Beamtinnen und Beamte, die die Aufstiegsprüfung oder eine Zwischenprüfung, deren Bestehen Voraussetzung für die Fortsetzung der Einführung ist, endgültig nicht bestehen, treten mit der Bekanntgabe der Entscheidung in die Aufgaben ihrer bisherigen Laufbahn zurück.

(4) Ein Aufstieg ist ausgeschlossen, wenn für die höhere Laufbahn eine bestimmte Vorbildung, Ausbildung oder Prüfung außerhalb des Landesbeamtengesetzes, dieser Verordnung oder einer Rechtsverordnung nach § 26 des Landesbeamtengesetzes vorgeschrieben oder nach ihrer Eigenart zwingend erforderlich ist.

(5) Ein Amt der neuen Laufbahn darf den Beamtinnen und Beamten erst verliehen werden, wenn sie sich im Anschluss an die Einführungszeit mindestens sechs Monate in Aufgaben der höheren Laufbahn bewährt haben.

(6) An der Aufstiegsausbildung können auch Arbeitnehmerinnen und Arbeitnehmer der in § 1 Absatz 1 genannten Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts teilnehmen und nach erfolgreicher Einführung in die Aufgaben der neuen Laufbahn die Laufbahn- oder Aufstiegsprüfung ablegen, wenn die oberste Dienstbehörde sie für eine spätere Übernahme in das Beamtenverhältnis vorgesehen hat.
Die Absätze 1 bis 5 und § 21 sind entsprechend anzuwenden.
§ 10 des Landesbeamtengesetzes bleibt unberührt.

(7) Nehmen Beamtinnen und Beamte oder Arbeitnehmerinnen und Arbeitnehmer nach erfolgreichem Auswahlverfahren für den Aufstieg an einem Vorbereitungsdienst teil, sind die für die Referendarinnen, Referendare, Anwärterinnen und Anwärter im Vorbereitungsdienst geltenden Bestimmungen zur Ausbildung und Prüfung entsprechend anzuwenden.

#### § 23 Anerkennung externer Ausbildung, Befähigung für den gehobenen allgemeinen Verwaltungsdienst

(1) Die Befähigung für eine Laufbahn des mittleren oder gehobenen Dienstes besitzt auch, wer außerhalb des Vorbereitungsdienstes eine berufsbefähigende Ausbildung abgeschlossen hat, die der Laufbahnausbildung fachlich gleichwertig ist.
Über die Anerkennung der Gleichwertigkeit der außerhalb des Beamtenrechts geregelten Berufs- und Hochschulausbildung entscheidet die Laufbahnordnungsbehörde.

(2) Für Laufbahnen des gehobenen Dienstes ist eine aus Fachstudien und berufspraktischen Studienzeiten bestehende Ausbildung in einem Studiengang einer Hochschule im Sinne des § 10 Absatz 3 Nummer 2 Buchstabe b des Landesbeamtengesetzes zu fordern.
Bei Defiziten bezüglich der berufspraktischen Studienzeiten kann als Voraussetzung für die Anerkennung der Prüfung als Laufbahnprüfung der erfolgreiche Abschluss einer auf mindestens sechs Monate zu bemessenden Einführung in die Laufbahnaufgaben gefordert werden.

(3) Der an der Technischen Hochschule Wildau (FH) im Studiengang Verwaltung und Recht erworbene Diplom- oder Bachelor-Abschluss gilt als der Laufbahnprüfung für den gehobenen allgemeinen Verwaltungsdienst gleichwertig.

### Abschnitt 2  
Gehobener Dienst

#### § 24 Besondere Einstellungsvoraussetzungen der technischen Laufbahnen mit Vorbereitungsdienst

In Laufbahnen des technischen Dienstes nach § 22 Nummer 2 des Brandenburgischen Besoldungsgesetzes kann neben der Vorbildung nach § 10 Absatz 3 Nummer 1 des Landesbeamtengesetzes ein mit einem Bachelorgrad abgeschlossenes Studium an einer Hochschule oder ein als gleichwertig anerkannter Abschluss in der entsprechenden Fachrichtung nach Maßgabe der Ausbildungs- und Prüfungsordnungen verlangt werden.

#### § 25 Aufstieg in Laufbahnen mit Vorbereitungsdienst des gehobenen Dienstes

(1) Beamtinnen und Beamte des mittleren Dienstes können zum Aufstieg in eine Laufbahn des gehobenen Dienstes derselben Fachrichtung zugelassen werden, wenn sie

1.  geeignet sind,
2.  sich in einer laufbahnrechtlichen Dienstzeit von mindestens fünf Jahren seit der Ernennung zur Beamtin auf Lebenszeit oder zum Beamten auf Lebenszeit im mittleren Dienst bewährt haben und
3.  ein Beförderungsamt innehaben.

Abweichend von Satz 1 gilt für Beamtinnen und Beamte auf Lebenszeit des allgemeinen Vollzugsdienstes § 18 Absatz 2 und 3 der Brandenburgischen Polizeilaufbahnverordnung vom 28. August 2018 (GVBl. II Nr. 56), die durch Verordnung vom 3. Juni 2019 (GVBl. II Nr. 42) geändert worden ist, in der jeweils geltenden Fassung entsprechend.
Die zum Aufstieg zugelassenen Beamtinnen und Beamten werden in die Aufgaben der neuen Laufbahn durch die nach Maßgabe des § 26 des Landesbeamtengesetzes für die Laufbahn eingerichtete Aufstiegsausbildung oder unmittelbar geltender Regelungen des Bundes eingeführt.

(2) Die Aufstiegsausbildung kann gemäß § 22 Absatz 4 des Landesbeamtengesetzes auch in einem Studiengang an einer Hochschule erfolgen, wenn hieran ein dienstliches Interesse besteht.
Die berufspraktische Einführung in die höhere Laufbahn kann auch in der vorlesungsfreien Zeit erfolgen.
Das Aufstiegsverfahren kann auf die berufspraktische Einführung von einem Jahr beschränkt werden, wenn die Beamtin oder der Beamte die in der Ausschreibung geforderte Hochschulausbildung bereits absolviert hat.
Das Auswahlverfahren nach § 21 ist zu durchlaufen.

(3) Sind Arbeitnehmerinnen und Arbeitnehmer nach § 22 Absatz 6 für den Aufstieg vorgesehen, gelten die Absätze 1 und 2 entsprechend mit der Maßgabe, dass sich die zu fordernde Dienstzeit um die Dauer der regelmäßigen Probezeit von drei Jahren verlängert und dass die Regelung über das zu fordernde Beförderungsamt nicht anzuwenden ist.
Erfolgt die Aufstiegsausbildung nicht im Rahmen des für die Laufbahn eingerichteten Vorbereitungsdienstes, ist nach § 10 Absatz 3 des Landesbeamtengesetzes für den Erwerb der Befähigung mindestens ein mit einem Bachelorgrad abgeschlossenes Hochschulstudium oder ein gleichwertiger Hochschulabschluss nachzuweisen.

### Abschnitt 3  
Höherer Dienst

#### § 26 Aufstieg in Laufbahnen mit Vorbereitungsdienst des höheren Dienstes

(1) Beamtinnen und Beamte des gehobenen Dienstes können zum Aufstieg in eine Laufbahn des höheren Dienstes derselben Fachrichtung zugelassen werden, wenn sie

1.  geeignet sind,
2.  sich in einer laufbahnrechtlichen Dienstzeit von mindestens zehn Jahren seit der Ernennung zur Beamtin auf Lebenszeit oder zum Beamten auf Lebenszeit im gehobenen Dienst bewährt haben und
3.  mindestens ein Jahr ein Amt der Besoldungsgruppe A 12, Beamtinnen und Beamte der Gemeinden und Gemeindeverbände mindestens das erste Beförderungsamt, innehaben.

Die zum Aufstieg zugelassenen Beamtinnen und Beamten werden in die Aufgaben der neuen Laufbahn durch die nach Maßgabe des § 26 des Landesbeamtengesetzes für die Laufbahn eingerichtete Aufstiegsausbildung eingeführt.

(2) Die Aufstiegsausbildung kann gemäß § 22 Absatz 4 des Landesbeamtengesetzes auch in einem Studiengang an einer Hochschule erfolgen, wenn hieran ein dienstliches Interesse besteht.
Die berufspraktische Einführung in die höhere Laufbahn kann auch in der vorlesungsfreien Zeit erfolgen.
Das Aufstiegsverfahren kann auf die berufspraktische Einführung von einem Jahr beschränkt werden, wenn die Beamtin oder der Beamte die in der Ausschreibung geforderte Hochschulausbildung bereits absolviert hat.
Das Auswahlverfahren nach § 21 ist zu durchlaufen.

(3) Ist der Aufstieg nicht durch Verordnung nach § 26 des Landesbeamtengesetzes geregelt, dauert die Einführung in die Aufgaben der neuen Laufbahn abweichend von Absatz 2 mindestens ein Jahr und sechs Monate.
Die Einführung umfasst eine praktische Unterweisung in Aufgaben des höheren Dienstes und einen wissenschaftlich ausgerichteten Bildungsgang von mindestens sechs Monaten (Aufstiegsstudium), der an geeigneten Bildungseinrichtungen innerhalb oder außerhalb des öffentlichen Dienstes praxisbegleitend durchgeführt wird.
Die erfolgreiche Teilnahme ist durch Leistungsnachweise festzustellen.
Die Laufbahnordnungsbehörde erlässt für den Bildungsgang einen Rahmenplan.

(4) Die praktische Unterweisung in die Aufgaben des höheren Dienstes nach Absatz 3 erfolgt auf zwei Dienstposten in unterschiedlichen Aufgabenbereichen.
Die praktische Unterweisung auf den unterschiedlichen Dienstposten soll unter Berücksichtigung der Abwesenheitszeiten für die fachtheoretische Aufstiegsausbildung zu gleichen Teilen erfolgen.
Bei Gemeinden und Gemeindeverbänden bis zu 30 000 Einwohnerinnen und Einwohnern kann von den Voraussetzungen des Satzes 1 abgesehen werden.

(5) Hält die oberste Dienstbehörde die Einführung nach Absatz 3 für erfolgreich abgeschlossen, stellt der Landespersonalausschuss auf Antrag der obersten Dienstbehörde unter Berücksichtigung der Ergebnisse der praktischen und theoretischen Aufstiegsausbildung in einem Prüfungsgespräch fest, ob die Einführungszeit erfolgreich abgeschlossen ist.
Mit der Feststellung der erfolgreichen Einführung wird die Befähigung für die Laufbahn anerkannt.
Die Einzelheiten des Feststellungsverfahrens regelt der Landespersonalausschuss.
Für die Durchführung des Befähigungsfeststellungsverfahrens kann der Landespersonalausschuss zur Vorbereitung seiner Entscheidung Unterausschüsse bestellen.

(6) Die Prüfung vor dem Landespersonalausschuss kann einmal wiederholt werden.

(7) Sind Arbeitnehmerinnen und Arbeitnehmer nach § 22 Absatz 6 für den Aufstieg vorgesehen, gelten die Absätze 1 und 2 entsprechend mit der Maßgabe, dass sich die zu fordernde Dienstzeit um die Dauer der regelmäßigen Probezeit von drei Jahren verlängert und dass die Regelung über das zu fordernde Beförderungsamt nicht anzuwenden ist.
Für den Erwerb der Befähigung ist nach § 10 Absatz 4 des Landesbeamtengesetzes mindestens ein mit einem Mastergrad abgeschlossenes Hochschulstudium oder ein gleichwertiger Hochschulabschluss nachzuweisen.

Kapitel 3  
Laufbahnen ohne Vorbereitungsdienst
-----------------------------------------------

#### § 27 Allgemeines

(1) Laufbahnen ohne Vorbereitungsdienst sind die in der Anlage 2 genannten Laufbahnen.
Konkrete Festlegungen zu den Zugangsvoraussetzungen der Laufbahnen ergeben sich aus Anlage 3.

(2) An die Stelle des Vorbereitungsdienstes und der Laufbahnprüfung tritt eine für die Laufbahnbefähigung gleichwertige, innerhalb oder außerhalb des öffentlichen Dienstes geleistete, auf den geforderten Bildungsvoraussetzungen aufbauende hauptberufliche Tätigkeit.
Die hauptberufliche Tätigkeit muss gemeinsam mit den als Bildungsvoraussetzung nachzuweisenden Berufs- oder Hochschulabschlüssen geeignet sein, die Befähigung für die entsprechende Laufbahn zu vermitteln.
Wenn die besonderen Anforderungen der Laufbahn es erfordern, ist die hauptberufliche Tätigkeit nach Maßgabe der Festlegungen in der Anlage 3 ganz oder teilweise im öffentlichen Dienst zu leisten.
Erforderlichenfalls ist eine Zusatzqualifikation nachzuweisen.

(3) In eine Laufbahn ohne Vorbereitungsdienst kann eingestellt werden, wer

1.  die Bildungsvoraussetzungen nach § 28 erfüllt und
2.  eine hauptberufliche Tätigkeit nach § 29 nachweist.

#### § 28 Bildungsvoraussetzungen

(1) Die Bildungsvoraussetzungen müssen eine Ausbildung umfassen, die zu einem allgemein berufsbefähigenden Abschluss geführt hat.
Für Laufbahnen des mittleren und des gehobenen Dienstes muss die Ausbildung auf der nach § 10 des Landesbeamtengesetzes geforderten Mindestvorbildung aufbauen.
Für Laufbahnen des gehobenen Dienstes muss ein abgeschlossenes Hochschulstudium im Sinne von § 10 Absatz 3 Nummer 2 Buchstabe b des Landesbeamtengesetzes vorliegen.
Für Laufbahnen des höheren Dienstes ist ein den Voraussetzungen des § 10 Absatz 4 Nummer 1 des Landesbeamtengesetzes entsprechendes Hochschulstudium zu fordern.
Wenn die Anforderungen der Laufbahn es erfordern, kann bei Laufbahnen des höheren Dienstes der Abschluss eines konsekutiven Masterstudiengangs gefordert werden.
Die Bildungsvoraussetzungen ergeben sich aus Anlage 3.

(2) Die oberste Dienstbehörde kann im Einvernehmen mit der Laufbahnordnungsbehörde aufgrund einer Prüfung des Einzelfalls weitere geeignete fachverwandte Berufsabschlüsse anerkennen, sofern an der Gewinnung der Bewerberin oder des Bewerbers ein dringendes dienstliches Bedürfnis besteht.

(3) Abweichend von Absatz 2 kann die oberste Dienstbehörde für die Laufbahn des höheren allgemeinen Verwaltungsdienstes weitere als in der Anlage 3 aufgeführte Masterabschlüsse oder gleichwertige Hochschulabschlüsse nur anerkennen, wenn der Studien‑ und Prüfungsschwerpunkt in einer Studienfachrichtung der Rechtswissenschaften, der Verwaltungswissenschaften oder der Wirtschaftswissenschaften liegt.
Die oberste Dienstbehörde trifft die Entscheidung nach Satz 1 aufgrund entsprechender Nachweise im Einvernehmen mit dem Landespersonalausschuss.

#### § 29 Hauptberufliche Tätigkeit

(1) Hauptberuflich ist eine Tätigkeit, wenn sie entgeltlich ist, gewolltermaßen den Schwerpunkt der beruflichen Tätigkeit darstellt, in der Regel den überwiegenden Teil der Arbeitskraft beansprucht und dem durch Ausbildung und Berufswahl geprägten Berufsbild entspricht oder nahe kommt.
Die hauptberufliche Tätigkeit muss nach dem Erwerb der Bildungsvoraussetzungen geleistet worden sein.

(2) Die erforderliche Dauer der hauptberuflichen Tätigkeit beträgt in Laufbahnen

1.  des mittleren und gehobenen Dienstes ein Jahr und sechs Monate und
2.  des höheren Dienstes zwei Jahre und sechs Monate.

(3) Die hauptberufliche Tätigkeit ist für die Laufbahnbefähigung geeignet, wenn sie

1.  den für die Fachrichtung erforderlichen fachlichen Anforderungen entspricht,
2.  nach ihrer Schwierigkeit der Tätigkeit einer Beamtin oder eines Beamten derselben oder einer gleichwertigen Laufbahn entspricht,
3.  im Hinblick auf die Aufgaben der künftigen Laufbahn die Fähigkeit der Bewerberin oder des Bewerbers zu fachlich selbstständiger Berufsausübung erwiesen hat.

(4) Abweichend von Absatz 3 Nummer 1 können auf die nach Absatz 2 geforderte Dauer der hauptberuflichen Tätigkeit Zeiten einer fachverwandten gleichwertigen hauptberuflichen Tätigkeit angerechnet werden.

(5) Anteile einer hauptberuflichen Tätigkeit, die auf eine Teilzeitbeschäftigung entfallen, werden entsprechend ihrem Verhältnis zur regelmäßigen Arbeitszeit berücksichtigt, wenn sie mindestens die Hälfte der regelmäßigen wöchentlichen Arbeitszeit der Landesbeamtinnen und Landesbeamten betragen haben.

#### § 30 Feststellung der Befähigung

(1) Die oberste Dienstbehörde entscheidet aufgrund der nach den §§ 28 und 29 zu fordernden Nachweise über den Erwerb der Laufbahnbefähigung; sie kann diese Befugnis bei Laufbahnen des mittleren und des gehobenen Dienstes auf andere Stellen übertragen.

(2) Die Feststellung nach Absatz 1 muss

1.  den Zeitpunkt des Befähigungserwerbs,
2.  die Bezeichnung der Laufbahn und
3.  die Laufbahngruppe, sofern die Laufbahn mehreren Laufbahngruppen zugeordnet ist,

enthalten.

#### § 31 Aufstieg in Laufbahnen ohne Vorbereitungsdienst

(1) Beamtinnen und Beamte in Laufbahnen ohne Vorbereitungsdienst können zum Aufstieg in die nächsthöhere Laufbahn derselben Fachrichtung vorgeschlagen werden oder sich bewerben.
Die §§ 21, 22 Absatz 4 und 6, § 25 Absatz 1 und § 26 Absatz 1 gelten entsprechend.

(2) Der Aufstieg in den gehobenen Dienst setzt ein mit einem Bachelorgrad abgeschlossenes Hochschulstudium oder einen gleichwertigen Hochschulabschluss sowie eine erfolgreiche praktische Einführung in die Aufgaben der neuen Laufbahn von mindestens einem Jahr voraus.

(3) Der Aufstieg in den höheren Dienst setzt ein mit einem Mastergrad abgeschlossenes Hochschulstudium oder einen gleichwertigen Hochschulabschluss sowie eine erfolgreiche praktische Einführung in die Aufgaben der neuen Laufbahn von mindestens einem Jahr voraus.

(4) Die praktische Einführung schließt mit der Befähigungsfeststellung ab.
§ 22 Absatz 3 und 5, § 26 Absatz 5 Satz 2 und § 30 Absatz 1 gelten entsprechend.

Kapitel 4  
Andere Bewerberinnen, andere Bewerber
-------------------------------------------------

#### § 32 Andere Bewerberinnen, andere Bewerber

(1) Andere Bewerberinnen und andere Bewerber müssen durch ihre Lebens- und Berufserfahrung innerhalb oder außerhalb des öffentlichen Dienstes befähigt sein, die Aufgaben ihrer künftigen Laufbahn wahrzunehmen.
Die für die Laufbahn vorgeschriebene Vorbildung, Ausbildung oder Vorbereitungsdienst und Laufbahnprüfung dürfen von ihnen nicht gefordert werden.

(2) In eine Laufbahn, für die eine bestimmte Vorbildung, Ausbildung oder Prüfung außerhalb des Landesbeamtengesetzes, dieser Verordnung oder einer Rechtsverordnung nach § 26 des Landesbeamtengesetzes vorgeschrieben oder nach ihrer Eigenart zwingend erforderlich ist, können andere Bewerberinnen und andere Bewerber nicht eingestellt werden.

(3) Andere Bewerberinnen und andere Bewerber dürfen nur eingestellt werden, wenn

1.  sie oder er eine einschlägige Berufserfahrung von mindestens fünf Jahren nachweist, die nach Fachrichtung, Breite und Wertigkeit dem Aufgabenspektrum der künftigen Laufbahn entspricht,
2.  ein besonderes dienstliches Interesse an der Gewinnung der Bewerberin oder des Bewerbers als Beamtin oder Beamter besteht und
3.  die Laufbahnbefähigung auf Antrag der obersten Dienstbehörde vom Landespersonalausschuss festgestellt worden ist.

(4) Das Verfahren zur Feststellung der Befähigung regelt der Landespersonalausschuss durch eine Verfahrensordnung, die im Amtsblatt für Brandenburg veröffentlicht wird.

Kapitel 5  
Ausnahmeentscheidungen des Landespersonalausschusses
----------------------------------------------------------------

#### § 33 Ausnahmeentscheidungen des Landespersonalausschusses

Der Landespersonalausschuss kann auf Antrag der obersten Dienstbehörde Ausnahmen von folgenden Vorschriften dieser Verordnung zulassen:

1.  Ausschreibung (§ 4),
2.  Probezeit, Mindestprobezeit (§ 9),
3.  Beförderungsverbot während der Probezeit, vor Ablauf eines Jahres seit der Beendigung der Probezeit und der ersten Übertragung eines Amtes der nächsthöheren Laufbahngruppe nach einem Aufstieg oder der letzten Beförderung (§ 10 Absatz 1 Satz 2 Nummer 3),
4.  Verbot des Überspringens von Ämtern bei Beförderungen (§ 10 Absatz 2) oder
5.  Erprobungszeit für die Übertragung höher bewerteter Dienstposten (§ 11).

Kapitel 6  
Übergangs- und Schlussvorschriften
----------------------------------------------

#### § 34 Übergangsregelungen

Beamtinnen und Beamte, die sich bei Inkrafttreten dieser Verordnung in Laufbahnen befinden, die in den Anlagen 1 und 2 nicht genannt sind (geschlossene Laufbahnen), verbleiben in ihrer bisherigen Rechtsstellung; auf sie sind die Vorschriften dieser Verordnung entsprechend anzuwenden.

#### § 35 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Laufbahnverordnung vom 16.
September 2009 (GVBl.
II S. 622), die durch Artikel 2 der Verordnung vom 2. Februar 2016 (GVBl. II Nr. 4 S. 10) geändert worden ist, außer Kraft.

Potsdam, den 1.
Oktober 2019

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dr.
Dietmar Woidke  
  

Der Minister des Innern und für Kommunales

Karl-Heinz Schröter

* * *

### Anlagen

1

[Anlage 1 (zu § 3) - Laufbahnen mit geregelter Ausbildung (Vorbereitungsdienst) und Laufbahnordnungsbehörden](/br2/sixcms/media.php/68/GVBl_II_82_2019-Anlage-1.pdf "Anlage 1 (zu § 3) - Laufbahnen mit geregelter Ausbildung (Vorbereitungsdienst) und Laufbahnordnungsbehörden") 166.2 KB

2

[Anlage 2 (zu den §§ 3 und 27) - Laufbahnen ohne Vorbereitungsdienst und Laufbahnordnungsbehörden](/br2/sixcms/media.php/68/GVBl_II_82_2019-Anlage-2.pdf "Anlage 2 (zu den §§ 3 und 27) - Laufbahnen ohne Vorbereitungsdienst und Laufbahnordnungsbehörden") 185.8 KB

3

[Anlage 3 (zu den §§ 27 und 28) - Festlegung der Bildungsvoraussetzungen und von Besonderheiten der hauptberuflichen Tätigkeit der Laufbahnen ohne Vorbereitungsdienst](/br2/sixcms/media.php/68/GVBl_II_82_2019-Anlage-3.pdf "Anlage 3 (zu den §§ 27 und 28) - Festlegung der Bildungsvoraussetzungen und von Besonderheiten der hauptberuflichen Tätigkeit der Laufbahnen ohne Vorbereitungsdienst") 276.7 KB