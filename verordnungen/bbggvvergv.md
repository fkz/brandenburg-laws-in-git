## Verordnung über die Vergütung der Gerichtsvollzieherinnen und Gerichtsvollzieher im Land Brandenburg (Brandenburgische Gerichtsvollziehervergütungsverordnung - BbgGVVergV)

Auf Grund

*   des § 47 Absatz 1 und 2 des Brandenburgischen Besoldungsgesetzes vom 20. November 2013 (GVBl.
    I Nr. 32 S. 2, Nr. 34) verordnet die Landesregierung und
*   des § 47 Absatz 3 des Brandenburgischen Besoldungsgesetzes verordnet der Minister der Justiz und für Europa und Verbraucherschutz im Einvernehmen mit dem Minister der Finanzen:

#### § 1 Vergütung der Gerichtsvollzieherinnen und Gerichtsvollzieher

(1) Planmäßige und hilfsweise beschäftigte Gerichtsvollzieherinnen und Gerichtsvollzieher erhalten als Vergütung einen in vollem Umfang steuerpflichtigen Anteil an den durch sie für die Erledigung der Aufträge im Kalenderjahr vereinnahmten Gebühren und an den von ihnen erhobenen Dokumentenpauschalen (Gebührenanteil).

(2) Der Gebührenanteil wird festgesetzt bei Einnahmen an Gebühren und Dokumentenpauschalen im Kalenderjahr (Bemessungsgrenze)

*   bis zu 10 000 Euro einschließlich                                    auf 64 Prozent,
*   von dem Mehrbetrag bis zu 30 000 Euro einschließlich      auf 67 Prozent,
*   von dem Mehrbetrag bis zu 50 000 Euro einschließlich      auf 70 Prozent,
*   von dem Mehrbetrag über 50 000 Euro                              auf 50 Prozent.

(3) Aus dieser Vergütung sind alle für die Gerichtsvollziehertätigkeit typischen und besonderen Aufwendungen zu bestreiten, insbesondere für die Einrichtung und den Betrieb eines Geschäftsbetriebs einschließlich Personalkosten sowie bei Nachtdienst.
Im Übrigen verbleibt die Vergütung der Gerichtsvollzieherin oder dem Gerichtsvollzieher als Ansporn für ihre oder seine Vollstreckungstätigkeit im Außendienst.
Eine zusätzliche Aufwandsentschädigung wird nicht gewährt.
Besondere Bestimmungen, nach denen den Gerichtsvollzieherinnen und Gerichtsvollziehern die von ihnen bei der Erledigung der Aufträge vereinnahmten Auslagen nach dem Gerichtsvollzieherkostengesetz vom 19.
April 2001 (BGBl. I S. 623), das zuletzt durch Artikel 6 des Gesetzes vom 23.
Juli 2013 (BGBl. I S. 2586, 2677) geändert worden ist, in der jeweils geltenden Fassung, ganz oder teilweise überlassen werden, bleiben unberührt.

#### § 2 Vergütung bei Versetzung oder Teilzeitbeschäftigung

(1) Bei der Versetzung während des Kalenderjahres oder bei der Erteilung mehrerer Beschäftigungsaufträge innerhalb eines Kalenderjahres werden die Einnahmen an Gebühren und Dokumentenpauschalen für die einzelnen Beschäftigungszeiträume zu einer einheitlichen Bemessungsgrenze zusammengerechnet.

(2) Die für den Prozentsatz des Gebührenanteils nach § 1 Absatz 2 maßgebenden Bemessungsgrenzen vermindern sich bei Teilzeitbeschäftigung oder bei ermäßigter Arbeitszeit entsprechend dem Beschäftigungsumfang; bis einschließlich der zweiten Bemessungsgrenze erfolgt eine weitere Verminderung um 20 Prozent.

#### § 3 Vorläufige Errechnung, Entnahme und Festsetzung der Vergütung

(1) Die Gerichtsvollzieherinnen und Gerichtsvollzieher sind berechtigt, die ihnen nach den §§ 1 und 2 zustehende Vergütung jeweils zum Monatsende vorläufig zu errechnen, von den vereinnahmten Gebühren und Dokumentenpauschalen einzubehalten und darüber zu verfügen.
Die der Landeskasse verbleibenden Gebühren und Dokumentenpauschalen sind zeitgleich abzuliefern.

(2) Nach Ablauf des Kalenderjahres wird die insgesamt zustehende Vergütung durch die zuständige Dienstbehörde nach den darüber erlassenen besonderen Bestimmungen endgültig festgesetzt und angewiesen.
Dabei sind besondere Vergütungen nach § 4 Absatz 1 und 3 sowie nach § 5 zu verrechnen.

#### § 4 Vergütung bei Verhinderung oder Erkrankung

(1) Sind Gerichtsvollzieherinnen oder Gerichtsvollzieher an der Ausübung ihrer Tätigkeit länger als zwei Wochen gehindert, kann für die Dauer der Verhinderung auf Antrag eine Vergütung für die laufenden notwendigen Kosten des Geschäftsbetriebs insoweit gewährt werden, als diese Aufwendungen aus der Vergütung der letzten drei vollen Monate vor der Verhinderung oder Erkrankung nicht bestritten werden können.

(2) Erholungsurlaub ist keine Verhinderung im Sinne von Absatz 1.

(3) Bei der Erkrankung einer Bürokraft kann auf Antrag eine Vergütung für die notwendigen und angemessenen Mehrausgaben insoweit gewährt werden, als diese Aufwendungen aus der Vergütung der letzten drei vollen Monate vor der Erkrankung nicht bestritten werden können.

#### § 5 Besondere Vergütung

(1) Abweichend von den §§ 1 und 2 kann auf Antrag eine besondere Vergütung festgesetzt werden, wenn die nach den §§ 1 und 2 zustehenden Vergütungsbeträge aus Gründen, die die Gerichtsvollzieherin oder der Gerichtsvollzieher nicht zu vertreten hat, nicht ausreichen, um die besonderen, für die Gerichtsvollziehertätigkeit typischen Aufwendungen, insbesondere für die Einrichtung und den Betrieb des Büros, zu bestreiten.

(2) Die Gerichtsvollzieherin oder der Gerichtsvollzieher hat den Anfall der entstandenen höheren typischen Aufwendungen nachzuweisen und die Gründe für die Notwendigkeit der Mehrkosten schlüssig darzulegen.

#### § 6 Zuständigkeit

Über Anträge nach § 4 Absatz 1 und 3 sowie nach § 5 entscheidet die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts.

#### § 7 Ruhegehaltfähigkeit

(1) Die Vergütung gehört in Höhe von 8 Prozent des Endgrundgehalts der Besoldungsgruppe, die der Bemessung der Versorgungsbezüge der Gerichtsvollzieherin oder des Gerichtsvollziehers zugrunde liegt, zu den ruhegehaltfähigen Dienstbezügen, wenn die Beamtin oder der Beamte mindestens zehn Jahre ausschließlich im Gerichtsvollzieheraußendienst tätig gewesen ist und bis zum Eintritt des Versorgungsfalls eine Vergütung nach dieser Verordnung oder nach der Vollstreckungsvergütungsverordnung in der Fassung der Bekanntmachung vom 6.
Januar 2003 (BGBl. I S. 8) bezogen hat oder ohne Berücksichtigung einer vorangegangenen Dienstunfähigkeit bezogen hätte.
Die Frist nach Satz 1 gilt bei einer Beamtin oder einem Beamten, deren oder dessen Beamtenverhältnis durch Eintritt in den Ruhestand wegen Dienstunfähigkeit oder durch Tod geendet hat, als erfüllt, wenn sie oder er bis zum Eintritt in den Ruhestand wegen Erreichens der Altersgrenze zehn Jahre ausschließlich im Gerichtsvollzieheraußendienst hätte tätig sein können.

(2) Die Vergütung gehört in dem in Absatz 1 Satz 1 bestimmten Umfang auch dann zu den ruhegehaltfähigen Dienstbezügen, wenn die Beamtin oder der Beamte mindestens zehn Jahre im Gerichtsvollzieheraußendienst tätig gewesen ist und vor Eintritt in den Ruhestand wegen Dienstunfähigkeit für den Gerichtsvollzieheraußendienst in eine andere Verwendung übernommen worden ist.
Die Frist nach Satz 1 gilt als erfüllt, wenn die andere Verwendung infolge Krankheit oder Beschädigung, den sich die Beamtin oder der Beamte ohne grobes Verschulden bei Ausübung oder aus Veranlassung seines Dienstes als Gerichtsvollzieherin oder Gerichtsvollzieher zugezogen hat, notwendig wird und die Frist ohne diese Krankheit oder Beschädigung hätte erfüllt werden können.
In den Fällen der Sätze 1 und 2 ist bei der Bemessung des ruhegehaltfähigen Teils der Vergütung höchstens das Endgrundgehalt des Spitzenamtes des Gerichtsvollzieherdienstes zugrunde zu legen.

(3) In den Fällen einer Teilzeit mit Freistellungsphase vor Eintritt in den Ruhestand gilt Absatz 1 Satz 1 entsprechend, wenn die Beamtin oder der Beamte unmittelbar vor Beginn der Freistellungsphase mindestens zehn Jahre ausschließlich im Gerichtsvollzieheraußendienst tätig gewesen ist.

#### § 8 Übergangsvorschrift

Für die Abrechnung der Bürokostenentschädigung und der Vollstreckungsvergütung, die bis zum Inkrafttreten dieser Verordnung entstanden sind, sind die Verordnung zur Abgeltung der Bürokosten der Gerichtsvollzieher vom 27.
Dezember 1999 (GVBl.
II S. 44), die zuletzt durch die Verordnung vom 21. Juli 2015 (GVBl.
II Nr. 34) geändert worden ist, und die Vollstreckungsvergütungsverordnung in der Fassung der Bekanntmachung vom 6.
Januar 2003 (BGBl. I S. 8) weiter anzuwenden.
Der Gebührenanteil gemäß § 2 Absatz 1 Satz 2 Nummer 3 der Verordnung zur Abgeltung der Bürokosten der Gerichtsvollzieher und der Jahreshöchstbetrag gemäß § 3 Absatz 2 Satz 1 Nummer 3 der Verordnung zur Abgeltung der Bürokosten der Gerichtsvollzieher werden für die Zeit vom 1.
Januar bis zum 31. Dezember 2016 endgültig festgesetzt.

#### § 9 Prüfung

Die Vergütungsregelung wird bei wesentlichen Änderungen der für ihre Festsetzung maßgeblichen Umstände, spätestens nach einem Zeitraum von jeweils fünf Jahren, überprüft.

#### § 10 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am 1.
Januar 2017 in Kraft.
Gleichzeitig tritt die Verordnung zur Abgeltung der Bürokosten der Gerichtsvollzieher vom 27.
Dezember 1999 (GVBl.
II S. 44), die zuletzt durch die Verordnung vom 21.
Juli 2015 (GVBl.
II Nr. 34) geändert worden ist, außer Kraft.

Potsdam, den 29.
Dezember 2016

Die Landesregierung  
des Landes Brandenburg

Der Ministerpräsident

Dietmar Woidke

Der Minister der Justiz  
und für Europa und Verbraucherschutz

Stefan Ludwig