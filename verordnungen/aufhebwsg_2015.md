## Fünfte Verordnung über die Aufhebung von Wasserschutzgebieten

Auf Grund des § 15 Absatz 3 Satz 4 des Brandenburgischen Wassergesetzes in der Fassung der Bekanntmachung vom 2.
März 2012 (GVBl.
I Nr. 20) verordnet der Minister für Ländliche Entwicklung, Umwelt und Landwirtschaft:

#### § 1 Aufhebung von Wasserschutzgebieten

(1) Folgende, auf der Grundlage des § 28 des Landeskulturgesetzes vom 14.
Mai 1970 (GBl.
I Nr. 12 S. 67), des § 28 des Wassergesetzes vom 17.
April 1963 (GBl.
I Nr. 5 S. 77) und der Verordnung über die Festlegung von Schutzgebieten für die Wasserentnahme aus dem Grund- und Oberflächenwasser zur Trinkwassergewinnung vom 11.
Juli 1974 (GBl.
I Nr. 37 S. 349) festgesetzte und nach § 46 des Wassergesetzes vom 2.
Juli 1982 (GBl.
I Nr. 26 S. 467) aufrechterhaltene Wasserschutzgebiete werden aufgehoben:

1.  das mit Beschluss Nummer 118-2/71 vom 28.
    Januar 1971 des Kreistages Pritzwalk für das Wasserwerk Warnsdorf festgesetzte Trinkwasserschutzgebiet,
2.  das mit Beschluss Nummer 61-10/71 vom 3.
    November 1971 des Kreistages Kyritz für das Wasserwerk Wusterhausen festgesetzte Trinkwasserschutzgebiet,
3.  das mit Beschluss Nummer 51-15/72 vom 6.
    Juli 1972 des Kreistages Strasburg für das Wasserwerk Amalienhof festgesetzte Trinkwasserschutzgebiet,
4.  das mit Beschluss Nummer 74-11/73 vom 29.
    November 1973 des Kreistages Brandenburg für das Wasserwerk Krahne festgesetzte Trinkwasserschutzgebiet,
5.  das mit Beschluss Nummer 102/74 vom 21.
    Januar 1974 des Kreistages Wittstock für die „Trinkwasserversorgungsanlage Diakonissenhaus ‚Friedenshort‘ Heiligengrabe“ festgesetzte Trinkwasserschutzgebiet,
6.  das mit Beschluss Nummer 28-8/75 vom 10.
    September 1975 und Beschluss Nummer 87-24/83 vom 13.
    Juli 1983 des Kreistages Angermünde für das Wasserwerk Herzsprung festgesetzte Trinkwasserschutzgebiet,
7.  das mit Beschluss Nummer 0035 vom 20.
    November 1975 des Kreistages Wittstock für die „Trinkwasserversorgungsanlage des KfL Wittstock in Heiligengrabe“ festgesetzte Trinkwasserschutzgebiet,
8.  die mit Beschluss Nummer 027 vom 13.
    Mai 1976 des Kreistages Kyritz festgesetzten Trinkwasserschutzgebiete für das Wasserwerk Brauerei Dessow, Wasserwerk Heinrichsfelde Interflug und Wasserwerk Molkerei Neustadt,
9.  die mit Beschluss Nummer 63-20/77 vom 8.
    September 1977 des Kreistages Strasburg für die Wasserwerke Kleisthöh und Neuhof festgesetzten Trinkwasserschutzgebiete,
10.  das mit Beschluss Nummer 22-05/80 vom 17.
    März 1980 des Kreistages Angermünde für das Wasserwerk Günterberg festgesetzte Trinkwasserschutzgebiet,
11.  die mit Beschluss Nummer 0061 vom 8.
    Juni 1980 des Kreistages Kyritz für die Wasserwerke ACZ Breddin, Brückwitz OT Metzelthin, Drewen, Stärkefabrik Kyritz und Rehfeld festgesetzten Trinkwasserschutzgebiete,
12.  das mit Beschluss Nummer 87-14/1981 vom 1.
    Juli 1981 des Kreistages Eberswalde für das Wasserwerk Neugrimnitz festgesetzte Trinkwasserschutzgebiet,
13.  die mit Beschluss Nummer 68-14/81 vom 23.
    September 1981 des Kreistages Belzig für das Wasserwerk VEG (T) Görzke OT Arensnest und für die Wassergewinnungsanlage Schwanebeck ZGE festgesetzten Trinkwasserschutzgebiete,
14.  die mit Beschluss Nummer 70-17/81 vom 18.
    Dezember 1981 des Kreistages Prenzlau für die Wasserwerke Gramzow bei Prenzlau, Klinkow, Marienhof, Mönchehof, Neuenfeld, Parmen, Schenkenberg (Nummer 38 Schwaneberg) und Schönfeld bei Prenzlau sowie für die Wasserfassung Falkenhagen (Reserve) festgesetzten Trinkwasserschutzgebiete,
15.  die mit Beschluss Nummer 85 vom 21.
    Dezember 1981 des Kreistages Templin für die Wasserwerke Beenz bei Lychen, Dargersdorf, Flieth, Gollin, Götschendorf (Beton Nord) und Krohnhorst festgesetzten Trinkwasserschutzgebiete.

(2) Folgende, auf der Grundlage des § 29 des Wassergesetzes vom 2.
Juli 1982 und der Dritten Durchführungsverordnung zum Wassergesetz – Schutzgebiete und Vorbehaltsgebiete – vom 2.
Juli 1982 (GBl.
I Nr. 26 S. 487) festgesetzte Trinkwasserschutzgebiete werden aufgehoben:

1.  die mit Beschluss vom 15.
    Dezember 1982 des Kreistages Kyritz für die Wasserwerke Barsikow und Plänitz-Leddin festgesetzten Trinkwasserschutzgebiete,
2.  das mit Beschluss Nummer 311/83 vom 21.
    Dezember 1983 des Kreistages Neuruppin für das Wasserwerk Gnewikow festgesetzte Trinkwasserschutzgebiet,
3.  das mit Beschluss Nummer 09/36/85 vom 21.
    August 1985 des Kreistages Beeskow für das Wasserwerk Lindenberg festgesetzte Trinkwasserschutzgebiet,
4.  das mit Beschluss Nummer 51-10/85 vom 14.
    November 1985 des Kreistages Belzig für die Wasserversorgungsanlage VEG (T) Görzke OT Börnecke festgesetzte Trinkwasserschutzgebiet,
5.  die mit Beschluss Nummer 51-11/1986 vom 2.
    Juli 1986 des Kreistages Eberswalde in Ergänzung des Kreistagsbeschlusses Nummer 87-14/1981 für die Wasserwerke „BA Jugendtouristenhotel Joachimsthal“ und „WW Pionierrepublik“ festgesetzten Trinkwasserschutzgebiete,
6.  das mit Beschluss Nummer 0188/86 vom 17.
    Dezember 1986 des Kreistages Neuruppin für die Wasserversorgung Nietwerder festgesetzte Trinkwasserschutzgebiet,
7.  das mit Beschluss Nummer 061/87 vom 17.
    September 1987 des Kreistages Kyritz für das Wasserwerk Heinrichsfelde festgesetzte Trinkwasserschutzgebiet.

#### § 2 Inkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 30.
April 2015

Der Minister für Ländliche Entwicklung,  
Umwelt und Landwirtschaft

Jörg Vogelsänger