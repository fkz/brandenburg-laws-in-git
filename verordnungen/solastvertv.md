## Verordnung zur Verteilung des Soziallastenausgleichs gemäß § 15 Absatz 1 des Brandenburgischen Finanzausgleichsgesetzes (Soziallastenausgleichsverteilverordnung - SoLastVertV)

Auf Grund des § 15 Absatz 1 Satz 4 des Brandenburgischen Finanzausgleichsgesetzes vom 29. Juni 2004 (GVBl.
I S. 262), der durch Artikel 1 Nummer 10 des Gesetzes vom 18. Dezember 2018 (GVBl. I Nr. 34) geändert worden ist, verordnet der Minister der Finanzen im Einvernehmen mit der Ministerin für Arbeit, Soziales, Gesundheit, Frauen und Familie:

#### § 1 Verteilung

(1) Die Mittel nach § 15 Absatz 1 Satz 1 bis 3 des Brandenburgischen Finanzausgleichsgesetzes werden jeweils hälftig nach den Kosten der Unterkunft und Heizung gemäß § 22 des Zweiten Buches Sozialgesetzbuch und nach der Anzahl der Bedarfsgemeinschaften nach dem Zweiten Buch Sozialgesetzbuch auf die Landkreise und kreisfreien Städte aufgeteilt.

(2) Als Bemessungsgrundlage für die Anzahl der Bedarfsgemeinschaften gelten die von der Bundesagentur für Arbeit nach § 53 des Zweiten Buches Sozialgesetzbuch zum Zeitpunkt der Festsetzung veröffentlichten Statistiken.
Dabei wird für das Ausgleichsjahr das arithmetische Mittel aus den Monatswerten des jeweiligen Ausgleichsjahres gebildet.
Bei den Kosten der Unterkunft und Heizung wird auf die Daten vom 1.
Januar bis zum 31.
Dezember nach den Ergebnissen der Vierteljahresstatistik der Gemeindefinanzen für das jeweilige Jahr abgestellt.

#### § 2 Festsetzung und Auszahlung

(1) Das Ministerium der Finanzen setzt die Zuweisungen nach § 1 für die kommunalen Aufgabenträger unverzüglich nach Vorliegen der für die Bemessung nach § 1 Absatz 2 erforderlichen Daten fest.

(2) Auf die Zuweisungen nach § 1 Absatz 1 erhalten die kommunalen Aufgabenträger bis zum 15. Kalendertag des zweiten Monats eines Quartals Abschlagszahlungen.
Die geleisteten Abschlagszahlungen werden mit der endgültigen Festsetzung verrechnet.
Zuviel erhaltene Abschläge werden zurückgefordert oder mit entsprechenden Zahlungen nachfolgender Zeiträume verrechnet.

#### § 3 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig treten die

1.  Verordnung zur Verteilung von Sonderbedarfs-Bundesergänzungszuweisungen vom 30. Mai 2005 (GVBl.
    II S.
    302), die zuletzt durch Artikel 1 der Verordnung vom 18. Mai 2007 (GVBl. II S.
    127) geändert worden ist, und
2.  Verordnung zur Verteilung der Sonderbedarfs-Bundesergänzungszuweisungen 2005 vom 5. März 2013 (GVBl.
    II Nr.
    22, 33)

außer Kraft.

Potsdam, den 11.
November 2019

Der Minister der Finanzen

Christian Görke