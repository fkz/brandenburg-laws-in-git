## Verordnung über richter- und beamtenrechtliche Zuständigkeiten in der ordentlichen Gerichtsbarkeit, der Verwaltungsgerichtsbarkeit, der Finanzgerichtsbarkeit und den Staatsanwaltschaften im Land Brandenburg (RuBZV)

Auf Grund des § 1 des Gesetzes zur Regelung beamtenrechtlicher Zuständigkeiten im Bereich der Justiz vom 25.
Januar 2016 (GVBl.
I Nr. 5 S. 9), der durch das Gesetz vom 5.
Juni 2019 (GVBl.
I Nr. 23) geändert worden ist, und des § 6 Absatz 1 des Landesorganisationsgesetzes vom 24. Mai 2004 (GVBl.
I S.
186), der durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr.
28 S. 2) geändert worden ist, in Verbindung mit

*   § 54 Absatz 3 Satz 2 des Beamtenstatusgesetzes vom 17.
    Juni 2008 (BGBl. I S. 1010) in Verbindung mit § 71 des Deutschen Richtergesetzes in der Fassung der Bekanntmachung vom 19.
    April 1972 (BGBl. I S. 713), der durch § 62 Absatz 9 Nummer 1 des Gesetzes vom 17.
    Juni 2008 (BGBl. I S.
    1010) neu gefasst worden ist,
*   § 17 Absatz 2 Satz 1 und 2, § 34 Absatz 5, § 35 Absatz 2 Satz 2 sowie § 42 Absatz 2 Satz 2 des Landesdisziplinargesetzes vom 18.
    Dezember 2001 (GVBl.
    I S. 254),
*   § 32 Absatz 1 Satz 1, § 38 Satz 1 und 2, § 45 Absatz 3 Satz 2, § 54 Absatz 1, § 57 Absatz 1, § 66 Absatz 4, § 67a Absatz 3 Satz 2, § 69 Absatz 5 Satz 1, § 84, § 85 Absatz 4 Satz 4, § 86 Absatz 2 und 3, § 88 Absatz 5 Satz 1, § 89 Satz 2, § 92 Absatz 2 sowie § 103 des Landesbeamtengesetzes vom 3.
    April 2009 (GVBl.
    I S.
    26), von denen durch das Gesetz vom 29.
    Juni 2018 (GVBl.
    I Nr.
    17) § 45 Absatz 3 Satz 2 zuletzt geändert worden ist sowie § 67a Absatz 3 Satz 2, § 85 Absatz 4 Satz 4 und § 88 Absatz 5 Satz 1 eingefügt und § 66 Absatz 4, § 86 Absatz 2 und 3 und § 89 Satz 2 neu gefasst worden sind, in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes vom 12.
    Juli 2011 (GVBl.
    I Nr.
    18), der durch Artikel 3 des Gesetzes vom 18.
    Juni 2018 (GVBl.
    I Nr.
    13 S.
    4) neu gefasst worden ist, und § 7 Absatz 1 Satz 1 und 2 der Richternebentätigkeitsverordnung vom 10. Mai 1999 (GVBl.
    II S. 330),
*   § 13 Absatz 2 Satz 3 und § 58 Absatz 1 des Brandenburgischen Besoldungsgesetzes vom 20. November 2013 (GVBl.
    I Nr. 32 S. 2, Nr. 34),
*   § 9 Absatz 3, § 47 Absatz 3 Satz 3 und § 48 Absatz 2 Satz 1 des Brandenburgischen Beamtenversorgungsgesetzes vom 20.
    November 2013 (GVBl.
    I Nr. 32 S.
    77), von denen § 9 Absatz 3 Satz 2 durch Artikel 26 des Gesetzes vom 8.
    Mai 2018 (GVBl.
    I Nr.
    8 S.
    20) neu gefasst worden ist,
*   § 6 Satz 1 und 2 der Dienstjubiläumsverordnung vom 18.
    Dezember 2014 (BGBl.
    I S.
    2267) in Verbindung mit § 64 Absatz 1 Satz 1 des Landesbeamtengesetzes, der durch das Gesetz vom 11.
    März 2010 (GVBl.
    I Nr.
    13) neu gefasst worden ist,
*   § 4 Absatz 5 und § 9 Absatz 3 der Trennungsgeldverordnung in der Fassung der Bekanntmachung vom 29.
    Juni 1999 (BGBl.
    I S.
    1533) in Verbindung mit § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes und § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
*   § 1 Absatz 3 der Ernennungsverordnung vom 1.
    August 2004 (GVBl.
    II S.
    742) in Verbindung mit § 4 Absatz 1 Satz 3 des Landesbeamtengesetzes,
*   § 30 Absatz 1 der Laufbahnverordnung vom 1.
    Oktober 2019 (GVBl.
    II Nr.
    82) in Verbindung mit § 25 des Landesbeamtengesetzes, der zuletzt durch das Gesetz vom 29.
    Juni 2018 (GVBl.
    I Nr.
    17) geändert worden ist,

verordnet die Ministerin der Justiz unter Beachtung von Artikel 9 Absatz 3 des Staatsvertrages über die Errichtung der gemeinsamen Fachobergerichte der Länder Berlin und Brandenburg vom 26. April 2004 (GVBl.
I S.
281, 283):

### § 1 Ernennung der Beamtinnen und Beamten, Folgezuständigkeiten

(1) Die der obersten Dienstbehörde zustehende Befugnis zur Ernennung der Beamtinnen und Beamten mit Ausnahme der Staatsanwältinnen und Staatsanwälte wird der Präsidentin oder dem Präsidenten des Brandenburgischen Oberlandesgerichts, der Generalstaatsanwältin oder dem Generalstaatsanwalt des Landes Brandenburg und der Präsidentin oder dem Präsidenten des Finanzgerichts Berlin-Brandenburg für ihren jeweiligen Geschäftsbereich übertragen.
Der Präsidentin oder dem Präsidenten des Oberverwaltungsgerichts Berlin-Brandenburg wird diese Befugnis für die Beamtinnen und Beamten der Verwaltungsgerichte des Landes Brandenburg übertragen.

(2) Den in Absatz 1 genannten Dienstbehörden wird in demselben Umfang auch die Befugnis der obersten Dienstbehörde übertragen für

1.  Entscheidungen nach § 22 Absatz 1 bis 3 des Beamtenstatusgesetzes in Verbindung mit § 32 Absatz 1 Satz 1 des Landesbeamtengesetzes über die Feststellung der Beendigung des Dienstverhältnisses, nach § 28 Absatz 1 und 2 des Beamtenstatusgesetzes über die Versetzung einer Beamtin oder eines Beamten auf Probe in den Ruhestand, nach § 39 Satz 1 des Beamtenstatusgesetzes über das Verbot der Führung der Dienstgeschäfte und nach § 45 Absatz 3 Satz 1 des Landesbeamtengesetzes über das Hinausschieben des Eintritts in den Ruhestand über die Regelaltersgrenze,
2.  Zustimmungen nach § 13 Absatz 2 Satz 3 des Brandenburgischen Besoldungsgesetzes zum Absehen von der Rückforderung von Bezügen,
3.  die Feststellung der Laufbahnbefähigung für Laufbahnen besonderer Fachrichtungen des mittleren und gehobenen Dienstes nach § 30 Absatz 1 der Laufbahnverordnung,
4.  die Ausübung der Disziplinarbefugnisse bei Ruhestandsbeamtinnen und Ruhestandsbeamten nach § 17 Absatz 2 Satz 1 des Landesdisziplinargesetzes,
5.  die Kürzung der Dienstbezüge bis zum Höchstmaß nach § 34 Absatz 3 Nummer 1 des Landesdisziplinargesetzes, die Erhebung der Disziplinarklage nach § 35 des Landesdisziplinargesetzes und den Erlass des Widerspruchsbescheids nach § 42 Absatz 2 Satz 1 des Landesdisziplinargesetzes,
6.  die Entscheidung über die Gewährung von Jubiläumszuwendungen nach § 64 Absatz 1 Satz 1 des Landesbeamtengesetzes.

(3) Die Zuständigkeiten des Ministeriums des Innern und für Kommunales, des für Finanzen zuständigen Ministeriums und des Landespersonalausschusses bleiben unberührt.

### § 2 Abordnung und Versetzung

(1) Die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts und die Generalstaatsanwältin oder der Generalstaatsanwalt des Landes Brandenburg sind zuständig für die Abordnung und Versetzung der Beamtinnen und Beamten ihres Geschäftsbereichs nach den §§ 14 und 15 des Beamtenstatusgesetzes, § 27 Absatz 2 Satz 1, §§ 29 und 30 des Landesbeamtengesetzes und für die Erklärung des Einverständnisses zu einer Abordnung oder Versetzung von Beamtinnen und Beamten in den Landesdienst nach § 14 Absatz 4 Satz 1 und § 15 Absatz 3 Satz 1 des Beamtenstatusgesetzes und in ihren Geschäftsbereich nach § 27 Absatz 2 Satz 2 des Landesbeamtengesetzes.
Die Befugnis zur Abordnung kann für Beamtinnen und Beamte des mittleren Dienstes auf unmittelbar nachgeordnete Dienstbehörden übertragen werden.

(2) Die in § 1 Absatz 1 Satz 1 genannten Dienstbehörden sind für die Abordnung von Richterinnen und Richtern auf Lebenszeit oder auf Zeit sowie für die Verwendung von Richterinnen und Richtern auf Probe und Richterinnen und Richtern kraft Auftrags ihres Geschäftsbereichs zuständig.

(3) Die Präsidentin oder der Präsident des Oberverwaltungsgerichts Berlin-Brandenburg ist zuständig für die Abordnung von Richterinnen und Richtern auf Lebenszeit oder auf Zeit sowie für die Verwendung von Richterinnen und Richtern auf Probe und Richterinnen und Richtern kraft Auftrags bei den Verwaltungsgerichten des Landes Brandenburg.
Artikel 3 Satz 1 des Staatsvertrages über die Errichtung gemeinsamer Fachobergerichte der Länder Berlin und Brandenburg bleibt unberührt.

### § 3 Sonstige Zuständigkeiten

(1) Der Präsidentin oder dem Präsidenten des Brandenburgischen Oberlandesgerichts, der Generalstaatsanwältin oder dem Generalstaatsanwalt des Landes Brandenburg und der Präsidentin oder dem Präsidenten des Finanzgerichts Berlin-Brandenburg werden für die Richterinnen, Richter, Beamtinnen und Beamten ihres Geschäftsbereichs die folgenden, der obersten Dienstbehörde zustehenden Befugnisse übertragen:

1.  Entgegennahme von Erklärungen und Entscheidungen in Nebentätigkeitsangelegenheiten nach den §§ 84 bis 86, 88 und 89 des Landesbeamtengesetzes in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes und § 7 der Richternebentätigkeitsverordnung und Untersagung von Tätigkeiten nach Beendigung des Richter- und Beamtenverhältnisses nach § 41 Satz 2 des Beamtenstatusgesetzes in Verbindung mit § 92 Absatz 2 des Landesbeamtengesetzes, § 71 des Deutschen Richtergesetzes und § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
2.  Entscheidungen über die Annahme von Belohnungen, Geschenken und sonstigen Vorteilen nach § 42 Absatz 1 Satz 2 des Beamtenstatusgesetzes in Verbindung mit § 57 Absatz 1 des Landesbeamtengesetzes, § 71 des Deutschen Richtergesetzes und § 10 Satz 1 des Brandenburgischen Richtergesetzes,
3.  Entscheidungen über den Ersatz von Sachschäden nach § 66 des Landesbeamtengesetzes in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
4.  Entscheidungen über die Erfüllungsübernahme durch den Dienstherrn bei Schmerzensgeldansprüchen nach § 67a des Landesbeamtengesetzes in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
5.  Genehmigung der Führung der Amtsbezeichnung mit dem Zusatz „außer Dienst“ nach § 69 Absatz 5 Satz 1 des Landesbeamtengesetzes in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
6.  Kürzung der Anwärterbezüge nach § 58 des Brandenburgischen Besoldungsgesetzes.

Der Präsidentin oder dem Präsidenten des Oberverwaltungsgerichts Berlin-Brandenburg werden für die Präsidentinnen oder Präsidenten der Verwaltungsgerichte des Landes Brandenburg die Befugnisse nach Satz 1 Nummer 1 bis 5 übertragen.
Den Präsidentinnen oder Präsidenten der Verwaltungsgerichte des Landes Brandenburg werden für die Richterinnen, Richter, Beamtinnen und Beamten ihres Geschäftsbereichs die Befugnisse zu Satz 1 Nummer 1 bis 5 mit der Maßgabe übertragen, dass die Entscheidung der Behördenleitung obliegt.

(2) Die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts und die Generalstaatsanwältin oder der Generalstaatsanwalt des Landes Brandenburg sind für die Richterinnen, Richter, Beamtinnen und Beamten ihres Geschäftsbereichs zuständig für die folgenden Befugnisse des Dienstvorgesetzten:

1.  Entscheidungen über Anträge auf Teilzeitbeschäftigung nach § 78 des Landesbeamtengesetzes und § 5 des Brandenburgischen Richtergesetzes sowie über den Widerruf,
2.  Entscheidungen über Anträge auf Beurlaubung nach § 79 Absatz 1 Satz 1 des Landesbeamtengesetzes in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes, sofern die Dauer des Urlaubs einen Monat überschreitet,
3.  Entscheidungen über Anträge auf Teilzeitbeschäftigung und Beurlaubung aus familiären Gründen nach § 80 des Landesbeamtengesetzes und § 4 Absatz 1 bis 6 des Brandenburgischen Richtergesetzes,
4.  Entscheidungen über Anträge auf Bewilligung von Familienpflegezeit sowie über den Widerruf nach § 80a des Landesbeamtengesetzes und § 4 Absatz 7 des Brandenburgischen Richtergesetzes,
5.  Geltendmachung von Schadenersatzansprüchen des Landes bei Amtspflichtverletzungen nach § 48 des Beamtenstatusgesetzes in Verbindung mit § 60 des Landesbeamtengesetzes, § 71 des Deutschen Richtergesetzes und § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
6.  Angelegenheiten des Mutterschutzes und der Elternzeit nach § 71 des Landesbeamtengesetzes in Verbindung mit der Mutterschutz- und Elternzeitverordnung und § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
7.  Durchsetzung übergegangener Schadenersatzansprüche nach § 67 des Landesbeamtengesetzes in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes außer bei Dienstunfällen,
8.  Entscheidungen über das Belassen von Leistungen nach § 79 Absatz 3 Satz 1 des Landesbeamtengesetzes in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
9.  Weisungen zur ärztlichen Untersuchung und Beobachtung nach § 37 Absatz 1 Satz 1 und § 43 des Landesbeamtengesetzes in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
10.  Feststellung der Dienstunfähigkeit auf Antrag der Richterin, des Richters, der Beamtin und des Beamten nach § 40 Absatz 1 des Landesbeamtengesetzes und § 83 Absatz 1 des Brandenburgischen Richtergesetzes,
11.  Mitteilungen über die Feststellung der Dienstunfähigkeit von Amts wegen nach § 41 Absatz 1 des Landesbeamtengesetzes,
12.  Entscheidungen nach der Verwaltungsvorschrift des Ministeriums des Innern und für Kommunales über die Gewährung von Rechtsschutz für Bedienstete des Landes Brandenburg in Straf- und anderen Verfahren.

(3) Die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts, die Generalstaatsanwältin oder der Generalsstaatsanwalt des Landes Brandenburg und die Präsidentin oder der Präsident des Finanzgerichts Berlin-Brandenburg sind für die Richterinnen, Richter, Beamtinnen und Beamten ihres Geschäftsbereichs, die Präsidentin oder der Präsident des Oberverwaltungsgerichts Berlin-Brandenburg ist für die Richterinnen und Richter der Verwaltungsgerichte des Landes Brandenburg für die Zusage der Umzugskostenvergütung nach den §§ 3 und 4 des Bundesumzugskostengesetzes und die Anerkennung einer Wohnung als vorläufige Wohnung nach § 11 Absatz 1 des Bundesumzugskostengesetzes, jeweils in Verbindung mit § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes und § 10 Absatz 1 des Brandenburgischen Richtergesetzes, zuständig, soweit nicht für die den Umzug veranlassende Maßnahme die Zuständigkeit der obersten Dienstbehörde gegeben ist.

### § 4 Sonderzuständigkeiten

(1) Der Präsidentin oder dem Präsidenten des Brandenburgischen Oberlandesgerichts werden für die Richterinnen, Richter, Beamtinnen und Beamten ihres oder seines Geschäftsbereichs, der Geschäftsbereiche der Generalstaatsanwältin oder des Generalstaatsanwalts des Landes Brandenburg und der Präsidentin oder des Präsidenten des Finanzgerichts Berlin-Brandenburg sowie für die Richterinnen, Richter, Beamtinnen und Beamten der Verwaltungsgerichte des Landes Brandenburg die folgenden, der obersten Dienstbehörde zustehenden Befugnisse in Unfallfürsorgeangelegenheiten übertragen:

1.  Anerkennung als Dienstunfall und Gewährung der Unfallfürsorge nach § 47 Absatz 3 Satz 3 des Brandenburgischen Beamtenversorgungsgesetzes,
2.  Versagung von Unfallfürsorgeleistungen nach § 48 Absatz 2 Satz 1 des Brandenburgischen Beamtenversorgungsgesetzes,
3.  Anordnung der ärztlichen oder psychologischen Untersuchung oder Beobachtung und der Erteilung von für die Gewährung der Unfallfürsorge erforderlichen Auskünften nach § 4 Nummer 4 und § 9 Absatz 3 Satz 1 des Brandenburgischen Beamtenversorgungsgesetzes sowie Weitergabe von Erkenntnissen und Beweismitteln an die mit der Begutachtung beauftragte Person nach § 4 Nummer 4 und § 9 Absatz 3 Satz 2 des Brandenburgischen Beamtenversorgungsgesetzes.

(2) Die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts ist für die Richterinnen, Richter, Beamtinnen und Beamten ihres oder seines Geschäftsbereichs, der Geschäftsbereiche der Generalstaatsanwältin oder des Generalstaatsanwalts des Landes Brandenburg und der Präsidentin oder des Präsidenten des Finanzgerichts Berlin-Brandenburg sowie für die Richterinnen, Richter, Beamtinnen und Beamten der Verwaltungsgerichte des Landes Brandenburg für folgende Entscheidungen zuständig:

1.  Durchsetzung übergegangener Schadenersatzansprüche aus Dienstunfällen nach § 67 des Landesbeamtengesetzes in Verbindung mit § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
2.  Entscheidungen über die Gewährung (Berechnung und Zahlbarmachung) von Umzugskostenvergütung nach § 2 Absatz 2 Satz 1 des Bundesumzugskostengesetzes in Verbindung mit § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes und § 10 Absatz 1 des Brandenburgischen Richtergesetzes,
3.  Entscheidungen in Trennungsgeldangelegenheiten einschließlich der Entscheidungen nach § 4 Absatz 5 der Trennungsgeldverordnung in Verbindung mit § 63 Absatz 1 Satz 1 des Landesbeamtengesetzes und § 10 Absatz 1 des Brandenburgischen Richtergesetzes und der weiteren Nebenentscheidungen zu Umfang, Dauer und Höhe des Trennungsgeldes, soweit diese nicht der obersten Dienstbehörde vorbehalten sind.

### § 5 Vorverfahren und Vertretung des Dienstherrn

(1) Die der obersten Dienstbehörde zustehende Befugnis zur Entscheidung über den Widerspruch einer Richterin, eines Richters, einer Beamtin oder eines Beamten, einer Richterin, eines Richters, einer Beamtin oder eines Beamten im Ruhestand, einer früheren Richterin, eines früheren Richters, einer früheren Beamtin oder eines früheren Beamten und der Hinterbliebenen gegen den Erlass oder die Ablehnung eines Verwaltungsaktes, gegen eine Maßnahme der Dienstaufsicht im Sinne des § 26 des Deutschen Richtergesetzes oder gegen die Ablehnung eines Anspruchs auf eine Leistung wird den in § 1 Absatz 1 Satz 1 genannten Dienstbehörden sowie der Präsidentin oder dem Präsidenten des Oberverwaltungsgerichts Berlin-Brandenburg übertragen, soweit sie selbst oder ihnen nachgeordnete Behörden die mit dem Widerspruch angefochtene Entscheidung erlassen haben.
Für Widersprüche gegen Entscheidungen im Zuständigkeitsbereich des § 4 ist die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts zuständig.

(2) Die Vertretung des Landes vor den Gerichten der allgemeinen Verwaltungsgerichtsbarkeit und den Richterdienstgerichten wird den in Absatz 1 genannten Stellen übertragen, soweit sie über den Widerspruch entschieden haben oder hätten entscheiden müssen.
Vor den Gerichten der allgemeinen Verwaltungsgerichtsbarkeit vertritt jedoch statt der Präsidentin oder des Präsidenten des Oberverwaltungsgerichts Berlin-Brandenburg die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts das Land.
Die Sätze 1 und 2 sind in Verfahren auf einstweiligen Rechtsschutz nach der Verwaltungsgerichtsordnung entsprechend anzuwenden.

(3) In anderen als den in den Absätzen 1 und 2 genannten Fällen ist für die Entscheidung über den Widerspruch und die Vertretung des Landes das für Justiz zuständige Ministerium oder, soweit es sich um Angelegenheiten des Gemeinsamen Juristischen Prüfungsamtes der Länder Berlin und Brandenburg handelt, dessen Präsidentin oder Präsident zuständig.

### § 6 Übergangsvorschriften

Soweit vor Inkrafttreten dieser Verordnung andere als die in den §§ 1 bis 5 bestimmten Zuständigkeiten bestanden, verbleibt es für die im Zeitpunkt des Inkrafttretens dieser Verordnung anhängigen Verwaltungsverfahren bei den bisherigen Zuständigkeiten.
Gleiches gilt hinsichtlich der Zuständigkeit für die Vertretung in zu diesem Zeitpunkt bereits anhängigen Rechtsstreitigkeiten.

### § 7 Inkrafttreten, Außerkrafttreten

Diese Verordnung tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt die Verordnung über richter- und beamtenrechtliche Zuständigkeiten in der ordentlichen Gerichtsbarkeit, der Verwaltungsgerichtsbarkeit, der Finanzgerichtsbarkeit und den Staatsanwaltschaften im Land Brandenburg vom 9.
Mai 2018 (GVBl.
II Nr.
35, 38) außer Kraft.

Potsdam, den 18.
Februar 2022

Die Ministerin der Justiz

Susanne Hoffmann