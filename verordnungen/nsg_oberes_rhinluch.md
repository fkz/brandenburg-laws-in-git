## Verordnung über das Naturschutzgebiet „Oberes Rhinluch“

Auf Grund des § 22 Absatz 1 und 2, des § 23 und des § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes vom 29.
Juli 2009 (BGBl.
I S. 2542) in Verbindung mit § 19 Absatz 1 und 2 und § 21 Absatz 1 Satz 2 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S. 350) verordnet die Ministerin für Umwelt, Gesundheit und Verbraucherschutz:

### § 1  
Erklärung zum Schutzgebiet

Die in § 2 näher bezeichnete Fläche in den Landkreisen Ostprignitz-Ruppin und Oberhavel wird als Naturschutzgebiet festgesetzt.
Das Naturschutzgebiet trägt die Bezeichnung „Oberes Rhinluch“.

### § 2  
Schutzgegenstand

(1) Das Naturschutzgebiet hat eine Größe von rund 2 764 Hektar.
Es umfasst Flächen in folgenden Fluren:

**Landkreis:**

**Gemeinde:**

**Gemarkung:**

**Flur:**

Ostprignitz-Ruppin

Neuruppin

Karwe

1, 4, 8;

Ostprignitz-Ruppin

Neuruppin

Redernluch

1, 2;

Ostprignitz-Ruppin

Fehrbellin

Altfriesack

1 bis 3;

Ostprignitz-Ruppin

Fehrbellin

Hakenberg

1, 2, 6;

Ostprignitz-Ruppin

Fehrbellin

Langen

4 bis 6, 107;

Ostprignitz-Ruppin

Fehrbellin

Linum

2 bis 5, 13 bis 15;

Ostprignitz-Ruppin

Fehrbellin

Tarmow

103;

Ostprignitz-Ruppin

Fehrbellin

Wall

6;

Ostprignitz-Ruppin

Fehrbellin

Wustrau

5, 10, 11, 13 bis 18, 36;

Oberhavel

Kremmen

Beetz

1;

Oberhavel

Kremmen

Flatow

4, 8, 12;

Oberhavel

Kremmen

Linumhorst

1;

Oberhavel

Kremmen

Staffelde 1

20;

Oberhavel

Kremmen

Staffelde

1, 17, 18.

Eine Kartenskizze zur Orientierung über die Lage des Naturschutzgebietes ist dieser Verordnung als Anlage 1 beigefügt.

(2) Die Grenze des Naturschutzgebietes ist in den in Anlage 2 dieser Verordnung aufgeführten Karten mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.
Die in Anlage 2 Nummer 1 aufgeführten Übersichtskarten im Maßstab 1 : 25 000 mit den Bezeichnungen Karte 1 und 2 dienen der räumlichen Einordnung des Naturschutzgebietes.
Die in Anlage 2 Nummer 2 aufgeführten topografischen Karten im Maßstab 1 : 10 000 mit den Blattnummern 1 bis 5 ermöglichen die Verortung im Gelände.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den in Anlage 2 Nummer 3 mit den Blattnummern 1 bis 33 aufgeführten Liegenschaftskarten.
Darüber hinaus ist dieser Verordnung zur Orientierung über die betroffenen Grundstücke eine Flurstücksliste als Anlage 3 beigefügt, die gemäß Absatz 4 hinterlegt wird.

(3) Innerhalb des Naturschutzgebietes sind eine Zone 1 mit weiter gehenden Regelungen zum Betreten, zum Reiten und zur Jagd sowie die Zonen 2a und 2b mit weiter gehenden Regelungen zur Grünlandnutzung festgesetzt.
Die Zone 1 umfasst rund 1 870 Hektar, die Zone 2a umfasst rund 1 295 Hektar, die Zone 2b umfasst rund 18 Hektar.
Die Zone 1 überlagert sich mit den Zonen 2a und 2b auf Teilflächen.

Die Flächen der Zonen sind in den in Anlage 2 Nummer 1 genannten zwei Übersichtskarten farbig dargestellt.
Die Grenzen der Zonen sind in den in Anlage 2 Nummer 2 genannten topografischen Karten mit den Blattnummern 1 bis 5 sowie in den in Anlage 2 Nummer 3 genannten Liegenschaftskarten mit den Blattnummern 1 bis 33 mit ununterbrochener roter Linie eingezeichnet.
Als Grenze gilt der innere Rand dieser Linie.
Maßgeblich für den Grenzverlauf ist die Einzeichnung in den Liegenschaftskarten.

(4) Die Grenze des in § 3 Absatz 2 aufgeführten Gebietes von gemeinschaftlicher Bedeutung „Oberes Rhinluch“ ist in der Übersichtskarte mit der Bezeichnung Karte 2 gemäß Absatz 2 im Maßstab 1 : 25 000 mit ununterbrochener roter Linie eingezeichnet; als Grenze gilt der innere Rand dieser Linie.

(5) Die Verordnung mit Karten und Flurstücksliste kann bei dem für Naturschutz und Landschaftspflege zuständigen Fachministerium des Landes Brandenburg, oberste Naturschutzbehörde, in Potsdam sowie bei den Landkreisen Ostprignitz-Ruppin und Oberhavel, untere Naturschutzbehörden, von jedermann während der Dienstzeiten kostenlos eingesehen werden.

### § 3  
Schutzzweck

(1) Schutzzweck des Naturschutzgebietes als Teil des Luchlandes und Kernbereich eines der größten brandenburgischen Niedermoorgebiete ist

1.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzengesellschaften, insbesondere der Schwimmblatt- und Unterwasserpflanzengesellschaften, der Röhrichte der Verlandungszonen und Gewässerufer, der Großseggen- und Röhrichtmoore, der Feuchtgrünländer einschließlich ihrer Auflassungsstadien, der Staudenfluren, der Weidengebüsche sowie der Moor-, Bruch- und Auwälder;
2.  die Erhaltung, Wiederherstellung und Entwicklung der Lebensstätten wild lebender Pflanzenarten, darunter im Sinne von § 7 Absatz 2 Nummer 13 des Bundesnaturschutzgesetzes besonders geschützte Arten, insbesondere Pracht-Nelke (Dianthus superbus), Sumpf-Sitter (Epipactis palustris), Wasser-Schwertlilie (Iris pseudacorus), Fieberklee (Menyanthes trifoliata), Gelbe Teichrose (Nuphar lutea), Weiße Seerose (Nymphaea alba) und Krebsschere (Stratiotes aloides);
3.  die Erhaltung, Wiederherstellung und Entwicklung des Gebietes als Lebens- und Rückzugsraum sowie potenzielles Wiederausbreitungszentrum wild lebender Tierarten, insbesondere
    1.  der Säugetiere, Vögel, Amphibien und Fische sowie einer artenreichen, insbesondere an Moore, Sümpfe, Gewässer und Feuchtwiesen gebundene Libellen- und Heuschreckenfauna, darunter im Sinne von § 7 Absatz 2 Nummer 13 und 14 des Bundesnaturschutzgesetzes besonders und streng geschützte Arten, insbesondere verschiedene Fledermausarten (Microchiroptera spp.), Knäkente (Anas querquedula), Bekassine (Gallinago gallinago), Großer Brachvogel (Numenius arquata), Rothalstaucher (Podiceps grisegena), Kiebitz (Vanellus vanellus), Knoblauchkröte (Pelobates fuscus), Moorfrosch (Rana arvalis) und Seefrosch (Rana ridibunda),
    2.  als Brut-, Schlaf- und Nahrungshabitat für Sumpf-, Wasser- und Greifvögel sowie als zentraler Bestandteil des bedeutendsten binnenländischen Kranichrastplatzes in Mitteleuropa auf dem westeuropäischen Zugweg,
    3.  als vitales Reproduktionszentrum und vernetzende Verbindung für die Stabilisierung und Ausbreitung der Populationen von Fischotter und Biber;
4.  die Erhaltung und Entwicklung des Gebietes aus wissenschaftlichen Gründen, beispielsweise für die Untersuchung tierökologischer Fragestellungen und die Niedermoorforschung;
5.  die Erhaltung des Gebietes wegen seiner besonderen Eigenart und hervorragenden Schönheit als störungsarme und unzerschnittene Niederungslandschaft, die durch das Flusssystem des Rhins und den Bützsee als Gewässer einer glazialen Schmelzwasserrinne geprägt wird;
6.  die Erhaltung der Linumer Teiche wegen ihrer besonderen Eigenart als extensiv genutzte Teichlandschaft mit herausragendem Naturschutzwert und ihrer landeskundlichen Bedeutung als ehemaliges Zentrum des Torfabbaus im Rhinluch;
7.  die Erhaltung und Entwicklung des Gebietes als wesentlicher Teil des überregionalen Biotopverbundes zwischen den Gewässersystemen von Elbe, Brandenburger/Mecklenburger Seenplatte, Oberer Havel und Oder-Havel-Kanal bis zur Oder.

(2) Die Unterschutzstellung dient der Erhaltung und Entwicklung

1.  eines Teils des Europäischen Vogelschutzgebietes „Rhin-Havelluch“ (§ 7 Absatz 1 Nummer 7 des Bundesnaturschutzgesetzes) in seiner Funktion
    1.  als Lebensraum von Arten nach Anhang I der Richtlinie 2009/147/EG, insbesondere Eisvogel (Alcedo atthis), Große Rohrdommel (Botaurus stellaris), Weißstorch (Ciconia ciconia), Rohrweihe (Circus aeruginosus), Wachtelkönig (Crex crex), Schwarzspecht (Dryocopus martius), Kranich (Grus grus), Seeadler (Haliaeetus albicilla), Zwergdommel (Ixobrychus minutus), Neuntöter (Lanius collurio), Blaukehlchen (Luscinia svecica), Rotmilan (Milvus milvus), Schwarzmilan (Milvus migrans), Fischadler (Pandionhaliaetus) Wespenbussard (Pernis apivorus), Kampfläufer (Philomachus pugnax), Kleines Sumpfhuhn (Porzana parva), Tüpfelsumpfhuhn (Porzana porzana), Flussseeschwalbe (Sterna hirundo) und Sperbergrasmücke (Sylvia nisoria) einschließlich ihrer Brut- und Nahrungsbiotope,
    2.  als Vermehrungs-, Rast-, Mauser- und Überwinterungsgebiet für im Gebiet regelmäßig auftretende Zugvogelarten wie Kranich (Grus grus), verschiedene Gänse- und Entenarten sowie Limikolen, insbesondere Goldregenpfeifer (Pluvialis apricaria);
2.  des Gebietes von gemeinschaftlicher Bedeutung „Oberes Rhinluch“ und eines Teils des Gebietes von gemeinschaftlicher Bedeutung „Oberes Rhinluch Ergänzung“(§ 7 Absatz 1 Nummer 6 des Bundesnaturschutzgesetzes) mit ihren Vorkommen von
    1.  Natürlichen eutrophen Seen mit einer Vegetation des Magnopotamions und Hydrocharitions, Flüssen der planaren Stufe mit Vegetation des Ranunculion fluitantis und des Callitricho-Batrachion, Pfeifengraswiesen auf kalkreichen und torfigen Böden (Molinion caeruleae) und Feuchten Hochstaudenfluren der planaren Stufe als Biotope von gemeinschaftlichem Interesse („natürliche Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG),
    2.  Kalkreichen Sümpfen mit Cladium mariscus (Binsen-Schneide) und Arten des Caricion davallianae, Birken-Moorwald und Auen-Wäldern mit Alnus glutinosa (Schwarz-Erle) und Fraxinus excelsior (Gewöhnliche Esche) (Alno-Padion, Alnion incanae, Salicion albae) als prioritäre Biotope („prioritäre Lebensraumtypen“ im Sinne des Anhangs I der Richtlinie 92/43/EWG),
    3.  Biber (Castor fiber), Fischotter (Lutra lutra), Teichfledermaus (Myotis dasycneme), Rotbauchunke (Bombina bombina), Kamm-Molch (Triturus cristatus), Steinbeißer (Cobitis taenia), Schlammpeitzger (Misgurnus fossilis), Schmaler Windelschnecke (Vertigo angustior) und Bauchiger Windelschnecke (Vertigo moulinsiana) als Tierarten von gemeinschaftlichem Interesse (im Sinne des Anhangs II der Richtlinie 92/43/EWG) einschließlich ihrer für Fortpflanzung, Ernährung, Wanderung und Überwinterung wichtigen Lebensräume.

### § 4  
Verbote

(1) Vorbehaltlich der nach § 5 zulässigen Handlungen sind in dem Naturschutzgebiet gemäß § 23 Absatz 2 des Bundesnaturschutzgesetzes alle Handlungen verboten, die das Gebiet, seinen Naturhaushalt oder einzelne seiner Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können.

(2) Es ist insbesondere verboten:

1.  bauliche Anlagen zu errichten oder wesentlich zu verändern, auch wenn dies keiner öffentlich-rechtlichen Zulassung bedarf;
2.  Straßen, Wege, Plätze oder sonstige Verkehrseinrichtungen sowie Leitungen anzulegen, zu verlegen oder zu verändern;
3.  Plakate, Werbeanlagen, Bild- oder Schrifttafeln aufzustellen oder anzubringen;
4.  Buden, Verkaufsstände, Verkaufswagen oder Warenautomaten aufzustellen;
5.  die Bodengestalt zu verändern, Böden zu verfestigen, zu versiegeln oder zu verunreinigen;
6.  die Art oder den Umfang der bisherigen Grundstücksnutzung zu ändern;
7.  zu lagern, zu zelten, Wohnwagen aufzustellen, Feuer zu verursachen oder eine Brandgefahr herbeizuführen;
8.  die Ruhe der Natur durch Lärm zu stören;
9.  das Gebiet
    1.  außerhalb der Wege zu betreten, zulässig bleibt das Betreten der in § 4 Absatz 2 Nummer 12 genannten Badestelle,
    2.  darüber hinaus in der Zone 1 auf den Wegen zu betreten.
        Zulässig bleibt das Betreten
        
        aa)
        
        ganzjährig auf den dem öffentlichen Verkehr gewidmeten Straßen und Wegen und den in den topografischen Karten gemäß § 2 Absatz 2 mit „WA“ gekennzeichneten Wegen,
        
        bb)
        
        auf den in den topografischen Karten gemäß § 2 Absatz 2 mit „WB“ gekennzeichneten Wegen innerhalb der Zeiträume vom 16.
        April bis zum 15.
        September eines  jeden Jahres und vom 1.
        Dezember eines jeden Jahres bis zum 28.
        Februar des Folgejahres;
        
10.  außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu reiten, zulässig bleibt
    1.  außerhalb der Zone 1 das Reiten auf den Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, sowie auf den nach öffentlichem Straßenrecht oder gemäß § 51 des Brandenburgischen Naturschutzgesetzes als Reitwege markierten Wegen,
    2.  in der Zone 1 das Reiten
        
        aa)
        
        ganzjährig auf den in den topografischen Karten gemäß § 2 Absatz 2 mit „WA“ gekennzeichneten Wegen und
        
        bb)
        
        innerhalb der Zeiträume vom 16.
        April bis zum 15.
        September eines jeden Jahres und vom 1.
        Dezember eines jeden Jahres bis zum 28.
        Februar des Folgejahres auf den in den topografischen Karten gemäß § 2 Absatz 2 mit „WB“ gekennzeichneten Wegen,
        
    3.  § 44 Absatz 2 des Brandenburgischen Naturschutzgesetzes und § 15 Absatz 6 des Waldgesetzes des Landes Brandenburg;
11.  mit Fahrzeugen außerhalb der dem öffentlichen Verkehr gewidmeten Straßen und Wege zu fahren oder Fahrzeuge dort abzustellen, zu warten oder zu pflegen;
12.  von anderen Zugangsstellen als von dem in den topografischen Karten gemäß § 2 Absatz 2 gekennzeichneten Uferbereich des Bützsees aus zu baden oder zu tauchen;
13.  Wasserfahrzeuge aller Art einschließlich Surfbretter oder Luftmatratzen zu benutzen;
14.  Modellsport oder ferngesteuerte Modelle zu betreiben oder feste Einrichtungen dafür bereitzuhalten;
15.  Hunde frei laufen zu lassen;
16.  Entwässerungsmaßnahmen über den bisherigen Umfang hinaus durchzuführen, Gewässer jeder Art entgegen dem Schutzzweck zu verändern oder in anderer Weise den Wasserhaushalt des Gebietes zu beeinträchtigen;
17.  Düngemittel einschließlich Wirtschaftsdünger, zum Beispiel Gülle und Gärreste aus Biogasanlagen, und Sekundärrohstoffdünger (zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) zum Zweck der Düngung sowie Abwasser zu sonstigen Zwecken zu lagern, auf- oder auszubringen oder einzuleiten;
18.  sonstige Abfälle im Sinne des Kreislaufwirtschafts- und Abfallgesetzes oder sonstige Materialien zu lagern oder sie zu entsorgen;
19.  Tiere zu füttern oder Futter bereitzustellen;
20.  Tiere auszusetzen oder Pflanzen anzusiedeln;
21.  wild lebenden Tieren nachzustellen, sie zu beunruhigen, zu fangen, zu verletzen, zu töten oder ihre Entwicklungsformen, Nist-, Brut-, Wohn- oder Zufluchtsstätten der Natur zu entnehmen, zu beschädigen oder zu zerstören;
22.  wild lebende Pflanzen oder ihre Teile oder Entwicklungsformen abzuschneiden, abzupflücken, aus- oder abzureißen, auszugraben, zu beschädigen oder zu vernichten;
23.  Pflanzenschutzmittel jeder Art anzuwenden;
24.  Wiesen, Weiden oder sonstiges Grünland nachzusäen, umzubrechen oder neu anzusäen.

### § 5  
Zulässige Handlungen

(1) Ausgenommen von den Verboten des § 4 bleiben folgende Handlungen:

1.  die den in § 5 Absatz 2 des Bundesnaturschutzgesetzes genannten Grundsätzen der guten fachlichen Praxis entsprechende landwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  auf Grünland § 4 Absatz 2 Nummer 24 gilt.
        Zur Grünlanderhaltung ist außerhalb der Zone 2b (Pfeifengraswiesen) eine umbruchlose Nachsaat (mit Schlitzgerät) zulässig,
    2.  Gewässerufer bei Beweidung auszuzäunen sind,
    3.  Grünland innerhalb der Zonen 2a und 2b als Wiese oder Weide genutzt wird und die jährliche Zufuhr an Pflanzennährstoffen über Düngemittel inklusive der Exkremente von Weidetieren je Hektar Grünland die Menge nicht überschreitet, die dem Nährstoffäquivalent des Dunganfalls von 1,4 Raufutter verwertenden Großvieheinheiten (RGV) entspricht, ohne chemisch-synthetische Stickstoffdüngemittel oder Sekundärrohstoffdünger (wie zum Beispiel solche aus Abwasser, Klärschlamm und Bioabfällen) einzusetzen oder Pflanzenschutzmittel jeder Art anzuwenden.  
        Zur Absicherung eines effizienten Wiesenbrüter- und Amphibienschutzes können zum Einsatz von Gülle und von Gärresten aus Biogasanlagen im Einzelfall Abreden mit den bewirtschaftenden Betrieben durch die untere Naturschutzbehörde geboten sein,
    4.  auf den Flächen der Zone 2b (Pfeifengraswiesen) über die Maßgaben nach Buchstabe c hinaus der Einsatz von Düngern aller Art unzulässig ist;
2.  die dem in § 5 Absatz 3 des Bundesnaturschutzgesetzes genannten Ziel entsprechende forstwirtschaftliche Bodennutzung auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  in die unter § 3 Absatz 2 Nummer 2 genannten Birken-Moorwälder und Auen-Wälder mit Alnus glutinosa (Schwarz-Erle) und Fraxinus excelsior (Gewöhnliche Esche) (Alno-Padion, Alnion incanae, Salicion albae) nur gesellschaftstypische Arten und in alle anderen Waldgesellschaften nur Arten der potenziell natürlichen Vegetation eingebracht werden dürfen.
        Es sind nur heimische Baumarten in gesellschaftstypischen Anteilen unter Ausschluss eingebürgerter Arten zu verwenden,
    2.  eine Nutzung einzelstammweise erfolgt; bei ausbleibender Naturverjüngung sind Femelschläge beimaximal gruppenweiser Auflichtung zulässig, wobei ein Bestockungsgrad von 0,6 nicht unterschritten werden darf.
        In naturfernen Beständen (zum Beispiel Pappelforste) sind Holzerntemaßnahmen, die den Holzvorrat auf weniger als 40 Prozent des üblichen Vorrats reduzieren, bis zu einer Größe von 0,5 Hektar zulässig,
    3.  ein Altholzanteil von mindestens 10 Prozent am aktuellen Bestandesvorrat zu sichern ist.
        Dabei sind, sofern vorhanden, mindestens fünf Stämme Altholz je Hektar mit einem Mindestdurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß bis zum Absterben aus der Nutzung zu nehmen und dauerhaft zu markieren,
    4.  eine naturnahe Waldentwicklung mit einem Totholzanteil von mindestens 10 Prozent des aktuellen Bestandesvorrates zu sichern ist.
        Dabei sind, sofern vorhanden, mindestens fünf Stück stehendes Totholz je Hektar mit einem Mindestdurchmesser von 30 Zentimetern in 1,30 Meter Höhe über dem Stammfuß zu erhalten und dauerhaft zu markieren,
    5.  Horst- und Höhlenbäume nicht gefällt werden,
    6.  hydromorphe Böden nur bei Frost befahren werden,
    7.  § 4 Absatz 2 Nummer 23 gilt;
3.  die den in § 5 Absatz 4 des Bundesnaturschutzgesetzes genannten Anforderungen in Verbindung mit dem Fischereigesetz für das Land Brandenburg entsprechende fischereiwirtschaftliche Flächennutzung außerhalb der Teichanlagen auf den bisher rechtmäßig dafür genutzten Flächen mit der Maßgabe, dass
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters, des Bibers und tauchender Vogelarten wie Gänsesäger und Rothalstaucher weitgehend ausgeschlossen ist,
    2.  die in § 3 Absatz 2 Nummer 2 genannten Fischarten ganzjährig geschont sind,
    3.  der Fischbesatz nur mit heimischen Arten erfolgt und dabei eine Gefährdung der in § 3 Absatz 2 Nummer 2 genannten Arten ausgeschlossen ist; § 13 der Fischereiordnung des Landes Brandenburg bleibt unberührt,
    4.  Hegepläne einvernehmlich mit der unteren Naturschutzbehörde abzustimmen sind;
4.  die Teichbewirtschaftung, die den Anforderungen des § 5 Absatz 4 des Bundesnaturschutzgesetzes in Verbindung mit dem Fischereigesetz des Landes Brandenburg entspricht und die im Sinne der guten fachlichen Praxis gemäß den Leitlinien zur naturschutzgerechten Teichwirtschaft in Brandenburg vom 16.
    März 2011 auf den bisher rechtmäßig dafür genutzten Flächen durchgeführt wird, mit der Maßgabe, dass
    1.  Fanggeräte und Fangmittel so einzusetzen oder auszustatten sind, dass eine Gefährdung des Fischotters, des Bibers und tauchender Vogelarten wie Gänsesäger und Rothalstaucher weitgehend usgeschlossen ist,
    2.  die in § 3 Absatz 2 Nummer 2 genannten Fischarten ganzjährig geschont sind,
    3.  der Fischbesatz nur mit heimischen Arten erfolgt.
        Davon ausgenommen ist in den Teichen 29 bis 35 der Besatz mit Sibirischem Stör (Acipenser baeri) sowie, jeweils mit Genehmigung der unteren Naturschutzbehörde, das Einsetzen weiterer nicht heimischer Fischarten im gesamten Linumer Teichgebiet.
        Die Genehmigung ist zu erteilen, wenn dadurch die in § 3 genannten Schutzgüter nicht gefährdet werden.
        Die Nummerierung der Teiche ist den topografischen Karten gemäß § 2 Absatz 2 zu entnehmen,
    4.  die Bewirtschaftungsmaßnahmen so auszurichten sind, dass das bisherige gebietstypische mittlere Ertragsniveau des Linumer Teichgebietes nicht überschritten wird.
        Ausnahmen bedürfen der Zustimmung der unteren Naturschutzbehörde.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    5.  Teichflächen mit einer Größe von insgesamt mindestens 48 Hektar in der Zeit vom 1. Februar bis zum 15.
        April eines jeden Jahres und vom 16.
        September bis zum 30.
        November eines jeden Jahres auf einer mittleren Wasserstandshöhe von 20 bis 30 Zentimetern über Teichgrund als Kranich- und Gänserastplatz zu bespannen sind.
        Darüber hinaus ist eine Teichfläche von mindestens 33 Hektar über den Winter (mindestens vom 1.
        November eines Jahres bis zum 31.
        März des Folgejahres) bespannt zu halten (Gänseschlafplatz).
        Die Hektarzahlen können jeweils mit Zustimmung der Fachbehörde für Naturschutz und Landschaftspflege unterschritten werden unter der Voraussetzung, dass auf den angrenzenden Grünlandflächen ausreichend geeignete Rastflächen vorhanden sind,
    6.  Teichflächen mit einer Größe von insgesamt mindestens 7,5 Hektar in der Zeit vom 10. August bis zum 30.
        September eines jeden Jahres zur Errichtung eines Limikolenrastplatzes vorzeitig abzulassen sind, wobei die Schlammflächen durch oberflächennahe Grundwasserstände vor dem Austrocknen zu bewahren sind.
        Die Hektarzahl kann mit Zustimmung der Fachbehörde für Naturschutz und Landschaftspflege unterschritten werden unter der Voraussetzung, dass auf den angrenzenden Grünlandflächen ausreichend geeignete Rastflächen vorhanden sind,
    7.  die Elektrofischerei nur mit Zustimmung der unteren Naturschutzbehörde zulässig ist.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird,
    8.  der Schilfschnitt im Rahmen der Teichpflege nach dem 30.
        November eines Jahres bis zum 1. März des Folgejahres erfolgt; Maßnahmen zum Schilfschnitt außerhalb dieser Zeitspanne bedürfen der Zustimmung der unteren Naturschutzbehörde;
5.  die rechtmäßige Ausübung der Angelfischerei mit der Maßgabe, dass
    1.  die Angelfischerei vom Ufer aus auf die in den topografischen Karten gemäß § 2 Absatz 2 gekennzeichneten Stellen und Bereiche außerhalb des Linumer Teichgebietes und im Bereich des Linumer Teichgebietes auf die bisher genutzten Angelstellen auf den Außendämmen der Teiche 29 bis 35 (zu den  
        genannten Teichen sowie zum Amtmannkanal hin) sowie den Zwischendämmen zwischen den Teichen 29 bis 32 (jeweils beide Seiten des Dammes) beschränkt ist.
        Die genannten Teiche sind in den topografischen Karten gemäß § 2 Absatz 2 gekennzeichnet,
    2.  innerhalb des Zeitraums vom 1.
        Februar bis zum 31.
        August eines jeden Jahres in dem in den topografischen Karten nach § 2 Absatz 2 gekennzeichneten Bereich des Alten Rhins und des B-Grabens das Angeln verboten bleibt,
    3.  innerhalb der Zeiträume vom 1.
        März bis zum 15.
        April und vom 16.
        September bis zum 30. November eines jeden Jahres
        
        aa)
        
        in dem in den topografischen Karten gemäß § 2 Absatz 2 gekennzeichneten südlichen Bereich des Bützsees das Angeln eine Stunde vor Sonnenuntergang bis eine Stunde nach Sonnenaufgang verboten bleibt,
        
        bb)
        
        die Zone 1 nur auf den in den topografischen Karten gemäß § 2 Absatz 2 mit „WA“ gekennzeichneten Wegen und im Bereich der gemäß § 5 Absatz 1 Nummer 5  Buchstabe a gekennzeichneten Angelstellen und -bereiche betreten werden darf,
        
    4.  das Betreten sämtlicher Rhindämme (Alter Rhin, Kremmener Rhin, Bützrhin) nur im Bereich der gemäß § 5 Absatz 1 Nummer 5 Buchstabe a gekennzeichneten Angelstellen und -bereiche zulässig ist,
    5.  die Angelfischerei von den rechtmäßig bestehenden Stegen am Bützsee aus zulässig ist,
    6.  der Fischbesatz nur mit heimischen Arten erfolgt und dabei eine Gefährdung der in § 3 Absatz 2 Nummer 2 genannten Arten ausgeschlossen ist; § 13 der Fischereiordnung des Landes Brandenburg bleibt unberührt,
    7.  im Radius von 50 Metern um Biberburgen und Fischotterbaue die Angelfischerei unzulässig ist,
    8.  § 4 Absatz 2 Nummer 11 und § 5 Absatz 1 Nummer 7 gelten;
6.  für den Bereich der Jagd:
    
    1.  die rechtmäßige Ausübung der Jagd mit der Maßgabe, dass
        
        aa)
        
        die Jagd auf Federwild verboten ist,
        
        bb)
        
        die Jagd in der Zeit von 1.
        März bis zum 30.
        November eines jeden Jahres nur vom Ansitz aus erfolgt,
        
        cc)
        
        die Jagd innerhalb der Zone 1 in der Zeit vom 1.
        März bis zum 15. April und vom 16.
        September bis zum 30.
        November (Herbstrast) eines jeden Jahres nicht zulässig ist.  
          
        In der Zeit vom 1.
        März bis zum 15.
        April eines jeden Jahres kann eine Bejagung zur Vermeidung von Wildschäden durch Schwarzwild oder zur Reduzierung von Prädatoren mit Zustimmung der Fachbehörde für Naturschutz und Landschaftspflege erfolgen.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht gefährdet ist.  
          
        Sofern die Fachbehörde für Naturschutz und Landschaftspflege feststellt und bekannt gibt, dass die Herbstrast erst nach dem 16. September eines Jahres einsetzt oder bereits vor dem 30. November eines Jahres abgeschlossen ist, ist die Jagd auch innerhalb dieses bekannt gegebenen Zeitraums zulässig.
        Während der Herbstrast können örtlich und zeitlich begrenzt Maßnahmen zur Verhütung von Schäden durch Schwarzwild mit Zustimmung der Fachbehörde für Naturschutz und Landschaftspflege erfolgen, soweit das Rastgeschehen dies zulässt,
        
        dd)
        
        die Fallenjagd nur mit Lebendfallen und nur mit Genehmigung der Fachbehörde für Naturschutz und Landschaftspflege zulässig ist.
        Die Genehmigung kann mit Nebenbestimmungen versehen werden und ist zu erteilen, wenn dadurch die in § 3 genannten Schutzgüter nicht beeinträchtigt werden,
        
        ee)
        
        die Baujagd in einem Abstand von bis zu 100 Metern von Gewässerufern verboten ist,
        
    2.  die Errichtung ortsunveränderlicher jagdlicher Einrichtungen zur Ansitzjagd mit Zustimmung der Fachbehörde für Naturschutz und Landschaftspflege.
        Die Zustimmung ist zu erteilen, wenn der Schutzzweck nicht beeinträchtigt wird.  
          
        Transportable und mobile Ansitzeinrichtungen sind der Fachbehörde für Naturschutz und Landschaftspflege vor der Errichtung anzuzeigen.
        Die Fachbehörde kann in begründeten Einzelfällen das Aufstellen verbieten, wenn es dem Schutzzweck entgegensteht.
        Die Entscheidung hierzu soll unverzüglich erfolgen,
    3.  die Anlage von Kirrungen außerhalb gesetzlich geschützter Biotope und der in § 3 Absatz 2 Nummer 2 genannten Lebensraumtypen.
    
      
    Ablenkfütterungen sowie die Anlage von Ansaatwildwiesen und Wildäckern sind unzulässig; im Übrigen bleiben jagdrechtliche Regelungen nach § 41 des Jagdgesetzes für das Land Brandenburg unberührt;
7.  für den Bereich des Schiffs- und Bootsverkehrs das Befahren des Umfluters Schleuse Altfriesack im Rahmen des Gemeingebrauchs nach § 43 Absatz 1 des Brandenburgischen Wassergesetzes sowie die widmungsgemäße Nutzung der schiffbaren Landesgewässer mit der Maßgabe, dass
    1.  das Anlegen und das Stillliegen in den in den topografischen Karten gemäß § 2 Absatz 2 gekennzeichneten Bereichen unzulässig ist,
    2.  das Befahren der Wasserflächen mit einer Geschwindigkeit von mehr als 8 Kilometer pro Stunde verboten bleibt; davon ausgenommen ist die betonnte Fahrrinne auf dem Bützsee,
    3.  das Befahren des Bützsees mit Wasserfahrzeugen mit Maschinenantrieb nur auf der betonnten Fahrrinne sowie von dort auf kürzestem Wege von und zu genehmigten Liegeplätzen zulässig ist,
    4.  das Surfen und Segeln auf dem Bützsee unzulässig ist,
    5.  Bestände von Wasserpflanzen im Uferbereich nicht befahren werden dürfen.
        Zu diesen bewachsenen Uferzonen ist, soweit die Gewässergröße es zulässt, ein Mindestabstand von einem Meter einzuhalten;
8.  die im Sinne des § 10 des Brandenburgischen Straßengesetzes ordnungsgemäße Unterhaltung der dem öffentlichen Verkehr gewidmeten Straßen und Wege sowie die ordnungsgemäße Unterhaltung sonstiger rechtmäßig bestehender Anlagen, sofern sie nicht unter die Nummer 10 fallen, jeweils im Einvernehmen mit der unteren Naturschutzbehörde;
9.  die im Sinne des § 39 des Wasserhaushaltsgesetzes und des § 78 des Brandenburgischen Wassergesetzes ordnungsgemäße Unterhaltung der Gewässer, soweit sie den in § 3 aufgeführten Schutzgütern nicht entgegensteht;
10.  der Betrieb von Anlagen für die öffentliche Wasserversorgung, von Abwasseranlagen, von Messanlagen (Pegel-, Abfluss- und andere Messstellen) und von sonstigen wasserwirtschaftlichen Anlagen.
    Die Unterhaltung dieser Anlagen bleibt im Einvernehmen mit der unteren Naturschutzbehörde zulässig.
    Das Einvernehmen über regelmäßig wiederkehrende Unterhaltungsarbeiten kann durch langfristig gültige Vereinbarungen hergestellt werden;
11.  die sonstigen bei Inkrafttreten dieser Verordnung auf Grund behördlicher Einzelfallentscheidung rechtmäßig ausgeübten Nutzungen und Befugnisse in der bisherigen Art und im bisherigen Umfang;
12.  Maßnahmen zur Untersuchung von altlastverdächtigen Flächen und Verdachtsflächen sowie Maßnahmen der Altlastensanierung und der Sanierung schädlicher Bodenveränderungen gemäß Bundes-Bodenschutzgesetz sowie Maßnahmen der Munitionsräumung im Einvernehmen mit der unteren Naturschutzbehörde;
13.  Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen, die von der unteren Naturschutzbehörde zugelassen oder angeordnet worden sind;
14.  behördliche sowie behördlich angeordnete oder zugelassene Beschilderungen, soweit sie auf den Schutzzweck des Gebietes hinweisen oder als hoheitliche Kennzeichnungen, Orts- oder Verkehrshinweise, Wegemarkierungen oder Warntafeln dienen.
    Darüber hinaus sind nichtamtliche Hinweisschilder zum Fremdenverkehr im Sinne der Richtlinie des Ministeriums für Infrastruktur und Raumordnung zur Aufstellung nichtamtlicher Hinweiszeichen an Bundes-, Landes- und Kreisstraßen im Land Brandenburg (Hinweis-Z.Ri) vom 24.
    Juli 2007 (ABl. S. 1734) an Straßen und Wegen freigestellt;
15.  Maßnahmen, die der Abwehr einer unmittelbar drohenden Gefahr für die öffentliche Sicherheit und Ordnung dienen.
    Die untere Naturschutzbehörde ist über die getroffenen Maßnahmen unverzüglich zu unterrichten.
    Sie kann nachträglich ergänzende Anordnungen zur Vereinbarkeit mit dem Schutzzweck treffen.

(2) Die in § 4 für das Betreten und Befahren des Naturschutzgebietes enthaltenen Einschränkungen gelten nicht für die Dienstkräfte der Naturschutzbehörden, die zuständigen Naturschutzhelfer und sonstige von den Naturschutzbehörden beauftragte Personen sowie für Dienstkräfte und beauftragte Personen anderer zuständiger Behörden und Einrichtungen, soweit diese in Wahrnehmung ihrer gesetzlichen Aufgaben handeln.
Sie gelten unbeschadet anderer Regelungen weiterhin nicht für Eigentümer zur Durchführung von Maßnahmen zur Sicherung des Bestandes und der zulässigen Nutzung des Eigentums sowie für das Betreten und Befahren, soweit dies zur Ausübung der nach Absatz 1 zulässigen Handlungen erforderlich ist.
Das Gestattungserfordernis nach § 16 Absatz 2 des Waldgesetzes des Landes Brandenburg bleibt unberührt.

### § 6  
Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen

Folgende Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen werden als Zielvorgabe benannt:

1.  der Gebietswasserhaushalt soll durch geeignete Maßnahmen verbessert werden.
    Dabei sollen im Bereich geeigneter Grünlandflächen zeitweise über Flur liegende und oberflächennahe Grundwasserstände mit Blänkenbildung erreicht werden.
    Dies ist auch bei der Speicherbewirtschaftung der Rhinseen zu beachten;
2.  zur Erhaltung und Wiederherstellung der Artenvielfalt der Feucht- und Nasswiesen sowie für den Schutz der Wiesenbrüter sollen geeignete Bewirtschaftungstermine für die Grünlandnutzung angestrebt werden;
3.  die Bedingungen für rastende Vogelarten, insbesondere den Kranich, sollen durch geeignete Maßnahmen, beispielsweise Schaffung kurzrasiger Grünlandflächen im Umfeld der Vorsammelflächen im Herbst und Management der Nahrungsflächen in der Umgebung des Naturschutzgebietes optimiert werden;
4.  aufgelassene Feuchtwiesen, insbesondere die Pfeifengraswiesen, sollen durch angepasste regelmäßige Nutzung gepflegt und offen gehalten werden;
5.  die Ackerfläche im Gebiet soll dauerhaft in Grünland umgewandelt werden;
6.  für die Teiche soll ein Bewirtschaftungsplan erstellt werden, der folgende Mindestangaben enthält: Besatz nach Arten und Altersklassen, Bespannungszeiträume, Düngung, Maßnahmen zur Verhinderung der Teichverlandung nach Umfang und Zeitpunkt, Teichpflege- und Sanierungsmaßnahmen jeweils nach Art, Umfang und Zeitpunkt, erforderlicher Wasserbedarf für eine Wasserentnahme aus dem Amtmannkanal.
    Der Bewirtschaftungsplan soll mit der unteren Naturschutzbehörde abgestimmt werden;
7.  für das Gebiet soll, insbesondere für die Zeit der Kranichrast, ein abgestimmtes Besucherkonzept erarbeitet werden;
8.  überalternde Hybridpappelreihen sollen zu artenreichen und standortgerechten Baumreihen mit heckenartigem Unterwuchs entwickelt werden.

### § 7  
Befreiungen

Von den Verboten dieser Verordnung kann die zuständige Naturschutzbehörde auf Antrag gemäß § 67 des Bundesnaturschutzgesetzes Befreiung gewähren.

### § 8  
Ordnungswidrigkeiten

(1) Ordnungswidrig im Sinne des § 73 Absatz 2 Nummer 2 des Brandenburgischen Naturschutzgesetzes handelt, wer vorsätzlich oder fahrlässig den Verboten des § 4 oder den Maßgaben des § 5 zuwiderhandelt.

(2) Ordnungswidrigkeiten nach Absatz 1 können gemäß § 74 des Brandenburgischen Naturschutzgesetzes mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

### § 9  
Duldungspflicht, Verhältnis zu anderen naturschutzrechtlichen Bestimmungen

(1) Die Duldung von Maßnahmen des Naturschutzes und der Landschaftspflege, die zur Ausführung der in dieser Verordnung festgelegten Schutz-, Pflege-, Entwicklungs- und Wiederherstellungsmaßnahmen und zur Verwirklichung des Schutzzwecks erforderlich sind, richtet sich nach § 65 des Bundesnaturschutzgesetzes in Verbindung mit § 68 des Brandenburgischen Naturschutzgesetzes.

(2) Die Vorschriften dieser Verordnung gehen anderen naturschutzrechtlichen Schutzausweisungen im Bereich des in § 2 genannten Gebietes vor.

(3) Soweit diese Verordnung keine weiter gehenden Vorschriften enthält, bleiben die Regelungen über gesetzlich geschützte Teile von Natur und Landschaft (insbesondere die §§ 31 bis 33 und 35 des Brandenburgischen Naturschutzgesetzes, § 30 des Bundesnaturschutzgesetzes), über das Netz „Natura 2000“ (§§ 33 und 34 des Bundesnaturschutzgesetzes) und über den Schutz und die Pflege wild lebender Tier- und Pflanzenarten (§§ 37 bis 47 des Bundesnaturschutzgesetzes) unberührt.

### § 10  
Geltendmachen von Rechtsmängeln

Eine Verletzung der in § 28 des Brandenburgischen Naturschutzgesetzes genannten Verfahrens- und Formvorschriften kann gegen diese Verordnung nur innerhalb eines Jahres nach ihrem Inkrafttreten schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, gegenüber dem für Naturschutz und Landschaftspflege zuständigen Fachministerium geltend gemacht werden.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten dieser Verordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.

### § 11  
Inkrafttreten

§ 5 Absatz 1 Nummer 1 Buchstabe c und d tritt am 1.
Juli 2013 in Kraft.
Im Übrigen tritt diese Verordnung am Tag nach der Verkündung in Kraft.

Potsdam, den 20.
März 2013

Die Ministerin für Umwelt,  
Gesundheit und Verbraucherschutz

Anita Tack

* * *

**Anlage 1  
**(zu § 2 Absatz 1)

![Das Naturschutzgebiet Oberes Rhinluch hat eine Größe von rund 2764 Hektar. Es umfasst folgende Flächen: Im Landkreis Ostprignitz-Ruppin im Bereich der Stadt Neuruppin in den Gemarkungen Karwe und Redernluch, im Bereich der Gemeinde Fehrbellin in den Gemarkungen Altfriesack, Hakenberg, Langen, Linum, Tarmow, Wall und Wustrau. Im Landkreis Oberhavel im Bereich der Stadt Kremmen in den Gemarkungen Beetz, Flatow, Linumhorst, Staffelde 1 und Staffelde.](/br2/sixcms/media.php/69/Nr.%20252.GIF "Das Naturschutzgebiet Oberes Rhinluch hat eine Größe von rund 2764 Hektar. Es umfasst folgende Flächen: Im Landkreis Ostprignitz-Ruppin im Bereich der Stadt Neuruppin in den Gemarkungen Karwe und Redernluch, im Bereich der Gemeinde Fehrbellin in den Gemarkungen Altfriesack, Hakenberg, Langen, Linum, Tarmow, Wall und Wustrau. Im Landkreis Oberhavel im Bereich der Stadt Kremmen in den Gemarkungen Beetz, Flatow, Linumhorst, Staffelde 1 und Staffelde.")

 **Anlage 2  
**(zu § 2 Absatz 2)

### 1.
Übersichtskarten im Maßstab 1 : 25 000

**Titel:** Übersichtskarte zur Verordnung über das Naturschutzgebiet „Oberes Rhinluch“

Karte

Unterzeichnung

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des Ministeriums für Umwelt, Gesundheit  
und Verbraucherschutz (MUGV), am 5.
Februar 2013

2

unterzeichnet am 13.
September 2019 von der Siegelverwahrerin, Siegelnummer 13 des  
Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft (MLUL)

### 2.
Topografische Karten im Maßstab 1 : 10 000

**Titel:** Topografische Karte zur Verordnung über das Naturschutzgebiet „Oberes Rhinluch“

Blattnummer

Unterzeichnung

1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5. Februar 2013

2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

3

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

5

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

### 3.
Liegenschaftskarten im Maßstab 1 : 2 500

**Titel:** Liegenschaftskarte zur Verordnung über das Naturschutzgebiet „Oberes Rhinluch“

Blattnummer

Gemarkung

Flur

Unterzeichnung

1

Altfriesack  
  
Karwe

1, 2, 3  
  
1, 8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

2

Altfriesack  
  
Wustrau

2  
  
5, 10

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

3

Altfriesack  
  
Karwe  
  
Wustrau

2, 3  
  
8  
  
5, 10

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

4

Altfriesack  
  
Karwe

2  
  
8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

5

Altfriesack  
  
Wustrau

2  
  
10, 11

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

6

Altfriesack  
  
Karwe  
  
Redernluch  
  
Wustrau

2  
  
4, 8  
  
1  
  
10, 11

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

7

Karwe  
  
Redernluch

4, 8  
  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

8

Langen  
  
Tarmow

4, 5, 107  
  
103

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

9

Langen  
  
Tarmow

5  
  
103

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

10

Karwe  
  
Redernluch  
  
Wustrau

4  
  
1  
  
11, 18

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

11

Beetz  
  
Redernluch  
  
Wall  
  
Wustrau

1  
  
1, 2  
  
6  
  
18, 36

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

12

Langen  
  
Tarmow

5, 6  
  
103

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

13

Langen  
  
Tarmow  
  
Wustrau

6  
  
103  
  
14, 15

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

14

Wustrau

13, 14, 15, 16

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

15

Linum  
  
Wustrau

14  
  
13, 16, 17

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

16

Linum  
  
Linumhorst  
  
Wustrau

2, 14  
  
1  
  
17, 36

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

17

Beetz  
  
Redernluch  
  
Staffelde  
  
Staffelde 1  
  
Wall  
  
Wustrau

1  
  
2  
  
1  
  
20  
  
6  
  
36

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

18

Beetz  
  
Redernluch  
  
Staffelde  
  
Staffelde 1  
  
Wustrau

1  
  
2  
  
17  
  
20  
  
36

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

19

Beetz  
  
Staffelde

1  
  
17, 18

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

20

Hakenberg  
  
Langen  
  
Tarmow  
  
Wustrau

2, 6  
  
6  
  
103  
  
15

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

21

Hakenberg  
  
Wustrau

1, 2, 6  
  
15, 16

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

22

Hakenberg  
  
Linum  
  
Wustrau

1  
  
2, 14  
  
16

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

23

Linum  
  
Linumhorst  
  
Wustrau

2, 14  
  
1  
  
36

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

24

Flatow  
  
Linumhorst  
  
Redernluch  
  
Staffelde  
  
Wustrau

8, 12  
  
1  
  
2  
  
1  
  
36

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

25

Hakenberg

2

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

26

Hakenberg  
  
Linum

1, 2  
  
3, 13

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

27

Hakenberg  
  
Linum

1, 2  
  
2, 3, 13, 14

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

28

Flatow  
  
Linum  
  
Linumhorst

8  
  
2, 3  
  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

29

Flatow  
  
Linumhorst

8, 12  
  
1

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

30

Linum

3, 13

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

31

Linum

3, 4, 5, 13, 15

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

32

Flatow  
  
Linum

4, 8  
  
3, 4

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013

33

Flatow

4, 8

unterzeichnet von der Siegelverwahrerin, Siegelnummer 21 des MUGV, am 5.
Februar 2013