## Gesetz zu dem Versorgungslastenteilungs-Staatsvertrag

Der Landtag hat das folgende Gesetz beschlossen:

### § 1 

Dem vom Land Brandenburg am 16.
Dezember 2009 unterzeichneten Staatsvertrag über die Verteilung von Versorgungslasten bei bund- und länderübergreifenden Dienstherrenwechseln (Versorgungslastenteilungs-Staatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Vertrag nach seinem § 17 Absatz 1 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 15.
Juli 2010

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Staatsvertrag](/vertraege/versorgungslastenteilungsstv_2011)