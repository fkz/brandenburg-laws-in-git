## Gesetz zu dem Abkommen zur dritten Änderung des Abkommens über das Deutsche Institut für Bautechnik

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem am 15.
August 2014 vom Land Brandenburg unterzeichneten Abkommen zur dritten Änderung des Abkommens über das Deutsche Institut für Bautechnik wird zugestimmt.
Das Abkommen wird nachstehend veröffentlicht.

#### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem dieses Abkommen nach seiner Nummer 2 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 10.
Juli 2017

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Abkommen](/vertraege/dibt_abkommen)

* * *

### Anlagen

1

[Abkommen zur dritten Änderung des Abkommens über das Deutsche Institut für Bautechnik (3. DIBt-Änderungsabkommen)](/br2/sixcms/media.php/68/GVBl_I_18_2017_001Anlage-zum-Hauptdokument.pdf "Abkommen zur dritten Änderung des Abkommens über das Deutsche Institut für Bautechnik (3. DIBt-Änderungsabkommen)") 746.9 KB