## Gesetz über die Öffentlich bestellten Vermessungsingenieurinnen und Öffentlich bestellten Vermessungsingenieure im Land Brandenburg (Brandenburgisches ÖbVI-Gesetz - BbgÖbVIG)

Der Landtag hat das folgende Gesetz beschlossen:

**Inhaltsübersicht**

### Abschnitt 1  
Rechtsstellung und Zulassung

[§ 1 Rechtsstellung und Befugnisse](#1)

[§ 2 Zulassungsvoraussetzungen](#2)

[§ 3 Versagungsgründe](#3)

[§ 4 Zulassung](#4)

### Abschnitt 2  
Berufsausübung

[§ 5 Niederlassung](#5)

[§ 6 Berufliche Zusammenschlüsse](#6)

[§ 7 Vertretung](#7)

[§ 8 Allgemeine Berufspflichten](#8)

[§ 9 Ausführung der hoheitlichen Tätigkeiten](#9)

### Abschnitt 3  
Aufsicht

[§ 10 Aufsicht; Einschränkung eines Grundrechts](#10)

[§ 11 Ahndung von Pflichtverletzungen](#11)

[§ 12 Personenbezogene Daten](#12)

### Abschnitt 4  
Erlöschen der Zulassung

[§ 13 Erlöschen der Zulassung](#13)

[§ 14 Rücknahme der Zulassung](#14)

[§ 15 Widerruf der Zulassung](#15)

[§ 16 Verzicht auf die Zulassung](#16)

[§ 17 Geschäftsabwicklung](#17)

### Abschnitt 5  
Ordnungswidrigkeiten, Übergangs- und Schlussbestimmungen

[§ 18 Ordnungswidrigkeiten](#18)

[§ 19 Einschränkung eines weiteren Grundrechts](#19)

[§ 20 Anhörung der Berufsvertretung](#20)

[§ 21 Rechtsverordnungen](#21)

[§ 22 Übergangsbestimmungen](#22)

[§ 23 Inkrafttreten, Außerkrafttreten](#23)

### Abschnitt 1  
Rechtsstellung und Zulassung

#### § 1 Rechtsstellung und Befugnisse

(1) Öffentlich bestellte Vermessungsingenieurinnen und Öffentlich bestellte Vermessungsingenieure werden zur Wahrnehmung von Hoheitsaufgaben des amtlichen Vermessungswesens zugelassen und üben einen freien Beruf aus.
Personen, die nach diesem Gesetz zugelassen sind, führen die Berufsbezeichnung „Öffentlich bestellte Vermessungsingenieurin“ oder „Öffentlich bestellter Vermessungsingenieur“.

(2) Die Zulassung berechtigt,

1.  die Aufgaben nach den §§ 20 und 26 Absatz 3 des Brandenburgischen Vermessungsgesetzes und andere durch Gesetz des Landes zugewiesene Aufgaben wahrzunehmen und
2.  Bescheinigungen im Zusammenhang mit Angaben des amtlichen Vermessungswesens abzugeben.

(3) Neben den hoheitlichen Tätigkeiten nach Absatz 2 können Öffentlich bestellte Vermessungsingenieurinnen und Öffentlich bestellte Vermessungsingenieure auf anderen Gebieten des Vermessungswesens tätig werden und unter Berufung auf den Berufseid auf dem Gebiet des Vermessungs- und Katasterwesens als Sachverständige auftreten, soweit ihre hoheitlichen Tätigkeiten hierdurch nicht beeinträchtigt werden.
Hierbei unterliegen sie mit Ausnahme des § 8 Absatz 1 Satz 2 nicht diesem Gesetz.

#### § 2 Zulassungsvoraussetzungen

(1) Die Aufsichtsbehörde erteilt die Zulassung auf schriftlichen Antrag.
Die Zulassung darf nur erteilt werden, wenn die beantragende Person

1.  die Laufbahnbefähigung für den höheren vermessungstechnischen Verwaltungsdienst besitzt oder bereits als Öffentlich bestellte Vermessungsingenieurin oder Öffentlich bestellter Vermessungsingenieur im Land Brandenburg zugelassen war,
2.  mindestens 18 Monate vorwiegend bei der Ausführung von Aufgaben nach den §§ 20 und 26 Absatz 3 Nummer 1 des Brandenburgischen Vermessungsgesetzes, insbesondere bei der Erfassung der Geobasisdaten der Liegenschaften mitgewirkt hat und diese Beschäftigung nicht länger als ein Jahr zurückliegt,
3.  die für die Berufsausübung erforderliche persönliche Eignung besitzt,
4.  die Verpflichtungen aus § 5 Absatz 2 und § 8 Absatz 3 erfüllen kann.

Mindestens die Hälfte der praktischen Zeit nach Satz 2 Nummer 2 soll bei einer Öffentlich bestellten Vermessungsingenieurin oder einem Öffentlich bestellten Vermessungsingenieur im Land Brandenburg geleistet werden.

(2) Die Beschäftigung gemäß Absatz 1 Satz 2 Nummer 2 ist durch die jeweilige Aufgabenträgerin oder den jeweiligen Aufgabenträger nach § 26 Absatz 2, 3 oder Absatz 5 des Brandenburgischen Vermessungsgesetzes zu bescheinigen.
Dabei sind für die letzten sechs Monate der Beschäftigung die vermessungstechnischen Ermittlungen zur Feststellung und Beurkundung von Tatbeständen an Grund und Boden sowie Vermessungen zur Feststellung, Wiederherstellung und Abmarkung von Grenzen und zur Erfassung von baulichen Anlagen, die im Liegenschaftskataster nachzuweisen sind, anzugeben, mit deren Durchführung die antragstellende Person betraut war.

#### § 3 Versagungsgründe

Nicht zugelassen werden darf,

1.  wer nach der Entscheidung des Bundesverfassungsgerichts ein Grundrecht verwirkt hat oder die freiheitliche demokratische Grundordnung in strafbarer Weise bekämpft;
2.  wer im ordentlichen Strafverfahren zu einer Strafe verurteilt worden ist, auf deren Grund Beamtinnen oder Beamte ihre Beamtenrechte verlieren;
3.  wer in einem Disziplinarverfahren durch rechtskräftiges Urteil aus dem Beamtenverhältnis entfernt worden ist oder durch Kündigung aus wichtigem Grunde, der auch zur Entfernung aus dem Beamtenverhältnis führen würde, aus dem Arbeitsverhältnis ausgeschieden ist oder wem eine frühere Berechtigung zur Wahrnehmung öffentlicher Aufgaben entzogen worden ist und die Gründe für diesen Entzug weiter fortbestehen;
4.  wer eine Tätigkeit ausübt, die mit der Wahrnehmung der hoheitlichen Tätigkeiten unvereinbar ist;
5.  wer in einem anderen Land bereits als Öffentlich bestellte Vermessungsingenieurin oder Öffentlich bestellter Vermessungsingenieur zugelassen ist;
6.  wer Beamtin oder Beamter ist, es sei denn, es handelt sich um einen Status als Ehrenbeamtin oder Ehrenbeamter;
7.  wer sich weigert, den vorgeschriebenen Eid zu leisten (§ 4 Absatz 2);
8.  wer in Vermögensverfall geraten ist; ein Vermögensverfall wird vermutet, wenn ein Insolvenzverfahren über das Vermögen der zuzulassenden Person eröffnet oder die zuzulassende Person in das Schuldnerverzeichnis nach § 882b der Zivilprozessordnung eingetragen ist;
9.  wer nicht über die persönliche Zuverlässigkeit oder erforderliche geistige und körperliche Leistungsfähigkeit verfügt.

#### § 4 Zulassung

(1) Die Zulassung wird durch Aushändigung einer Zulassungsurkunde erteilt.
Sie wird mit dem in der Urkunde angegebenen Datum der Aushändigung wirksam, wenn nicht in der Urkunde ein späteres Datum bestimmt ist.

(2) Wer nach § 2 zugelassen werden soll, ist von der Aufsichtsbehörde auf die gewissenhafte und unparteiische Pflichterfüllung zu vereidigen.
Die Vorschriften des Landesbeamtengesetzes über den Diensteid gelten entsprechend.

### Abschnitt 2  
Berufsausübung

#### § 5 Niederlassung

(1) Öffentlich bestellte Vermessungsingenieurinnen oder Öffentlich bestellte Vermessungsingenieure dürfen ihren Beruf nur von ihrem Niederlassungsort aus ausüben.
Der Niederlassungsort muss sich im Land Brandenburg befinden.
Die Errichtung und Unterhaltung von Zweigstellen ist nicht zulässig.

(2) Am Niederlassungsort ist eine Geschäftsstelle einzurichten und diese so auszustatten, wie es zur ordnungsgemäßen Berufsausübung notwendig ist.
Die Verlegung der Geschäftsstelle und Änderungen der Anschrift sind der Aufsichtsbehörde unverzüglich anzuzeigen.

#### § 6 Berufliche Zusammenschlüsse

(1) Im Land Brandenburg zugelassene Öffentlich bestellte Vermessungsingenieurinnen und Öffentlich bestellte Vermessungsingenieure dürfen sich untereinander zur gemeinschaftlichen Berufsausübung durch schriftlichen Vertrag zusammenschließen, soweit die Erfüllung ihrer Berufspflichten nicht beeinträchtigt wird.
Sie dürfen

1.  zur Berufsausübung nach § 1 Absatz 2 und 3 eine gemeinsame Geschäftsstelle einrichten (Kooperation),
2.  sich bei der Wahrnehmung ihrer hoheitlichen Tätigkeiten unterstützen, indem bei anderen Öffentlich bestellten Vermessungsingenieurinnen oder Öffentlich bestellten Vermessungsingenieuren Beschäftigte nach § 9 Absatz 4 zur Abarbeitung von eigenen Antragsüberhängen oder zum Zweck der Einführung neuer Verfahren und Techniken gelegentlich eingesetzt werden.

§ 5 bleibt unberührt.

(2) Ein Zusammenschluss nach Absatz 1 ist der Aufsichtsbehörde unter Vorlage des Vertrags unverzüglich anzuzeigen.
Satz 1 gilt für die Auflösung des Vertrags entsprechend.

(3) Die Aufsichtsbehörde kann einen beruflichen Zusammenschluss nach Absatz 1 Satz 2 Nummer 1, der der Vorbereitung des Verzichts auf die Zulassung durch eine der Vertragsparteien dient, einmalig für längstens zwei Jahre unter Beibehaltung der jeweiligen Niederlassungsorte erlauben.
Dem Antrag auf Erteilung der Erlaubnis ist der Vertrag beizufügen.

#### § 7 Vertretung

(1) Sind Öffentlich bestellte Vermessungsingenieurinnen oder Öffentlich bestellte Vermessungsingenieure länger als zwei Wochen abwesend oder aus anderen Gründen gehindert, ihren Beruf auszuüben, so ist eine Vertretung sicherzustellen.

(2) Eine Öffentlich bestellte Vermessungsingenieurin oder ein Öffentlich bestellter Vermessungsingenieur kann einer anderen Öffentlich bestellten Vermessungsingenieurin oder einem anderen Öffentlich bestellten Vermessungsingenieur oder einer Mitarbeiterin oder einem Mitarbeiter, welche die Zulassungsvoraussetzungen nach § 2 Absatz 1 Satz 2 Nummer 1 bis 3 erfüllen, die Vertretung übertragen.
Eine Öffentlich bestellte Vermessungsingenieurin oder ein Öffentlich bestellter Vermessungsingenieur darf die Vertretung nur aus einem wichtigen Grund ablehnen.
Die Vertretung ist der Aufsichtsbehörde rechtzeitig mitzuteilen.
Die Dauer der Vertretung soll ein Jahr nicht überschreiten.

(3) Die Mitteilungspflicht nach Absatz 2 Satz 3 entfällt, wenn die Vertretung immer von derselben Person nach Absatz 2 Satz 1 wahrgenommen wird, die zuvor der Aufsichtsbehörde unter Vorlage einer Einverständniserklärung dieser Person dauerhaft benannt worden ist.

(4) Für die Person, der die Vertretung übertragen wurde, gilt für die Zeit der Vertretung dieses Gesetz entsprechend, auch wenn sie nicht Öffentlich bestellte Vermessungsingenieurin oder Öffentlich bestellter Vermessungsingenieur ist.

(5) Die Person, der die Vertretung übertragen wurde, zeichnet mit dem Zusatz „In Vertretung“.
Für eine Amtspflichtverletzung dieser Person haftet die vertretene Person.

#### § 8 Allgemeine Berufspflichten

(1) Öffentlich bestellte Vermessungsingenieurinnen und Öffentlich bestellte Vermessungsingenieure haben ihren Beruf unparteiisch, unabhängig, gewissenhaft, selbständig, eigenverantwortlich und sachgerecht auszuüben.
Ihr Verhalten hat der Achtung und dem Vertrauen zu entsprechen, die ihrem Beruf entgegengebracht werden.

(2) Sie sind verpflichtet, über Angelegenheiten, die ihnen in Ausübung ihres Berufs anvertraut oder sonst bekannt werden, Schweigen zu bewahren, es sei denn, dass sie von der Schweigepflicht entbunden werden.
Die für sie tätigen Personen sind in gleicher Weise zu verpflichten.
Die Pflicht zur Verschwiegenheit bleibt auch bestehen, wenn die Zulassung erlischt.

(3) Sie sind verpflichtet, Haftpflichtansprüche, die sich aus ihrer Berufstätigkeit ergeben, entsprechend ihres Geschäftsumfangs und der Art der überwiegend zu erledigenden Anträge, angemessen zu versichern.
Die Aufsichtsbehörde ist zuständige Stelle im Sinne des § 117 Absatz 2 des Versicherungsvertragsgesetzes.
Eine Haftung des Staates anstelle der Öffentlich bestellten Vermessungsingenieurin oder des Öffentlich bestellten Vermessungsingenieurs besteht nicht.

(4) Werbung ist nur erlaubt, soweit sie einen unbestimmten Personenkreis über die berufliche Tätigkeit in Form und Inhalt sachlich unterrichtet und nicht auf die Stellung eines Antrags im Einzelfall gerichtet ist.

(5) Öffentlich bestellte Vermessungsingenieurinnen und Öffentlich bestellte Vermessungsingenieure sind verpflichtet, sich regelmäßig beruflich fortzubilden und über die für ihre Berufsausübung geltenden Bestimmungen zu unterrichten.

#### § 9 Ausführung der hoheitlichen Tätigkeiten

(1) Bei den hoheitlichen Tätigkeiten wird die Öffentlich bestellte Vermessungsingenieurin oder der Öffentlich bestellte Vermessungsingenieur auf Antrag tätig, soweit durch Gesetz nichts anderes bestimmt ist.
Die Ausführung eines Antrags darf nur aus wichtigem Grund abgelehnt werden.
Die Annahme eines Antrags ist abzulehnen, wenn durch die Bearbeitung die Berufspflichten verletzt würden.
Für den Ausschluss von Personen und die Besorgnis der Befangenheit gelten die §§ 20 und 21 des Verwaltungsverfahrensgesetzes in Verbindung mit § 1 Absatz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg entsprechend.

(2) Soweit ein Antrag aufgrund der Einschränkungen des Absatzes 1 nicht angenommen oder aus sonstigen Gründen nicht in einer angemessenen Zeit ausgeführt werden kann, muss dies der Antragstellerin oder dem Antragsteller unverzüglich unter Angabe der Gründe schriftlich mitgeteilt werden.

(3) Hoheitliche Tätigkeiten sind unter Beachtung der für ihre Durchführung erlassenen Rechts- und Verwaltungsvorschriften in einer der Sachlage, Zweckbestimmung und dem Stand der Technik entsprechenden wirtschaftlichen Weise sorgfältig und gewissenhaft auszuführen.
Die Antragstellerinnen und Antragsteller sind sachgemäß zu beraten.

(4) Öffentlich bestellte Vermessungsingenieurinnen und Öffentlich bestellte Vermessungsingenieure dürfen sich bei ihren hoheitlichen Tätigkeiten der Mitwirkung von Beschäftigten bedienen, wenn eine wirksame persönliche Aufsicht gewährleistet ist.
Mit vermessungstechnischen Ermittlungen zur Feststellung und Beurkundung von Tatbeständen an Grund und Boden sowie der Durchführung von Vermessungen zur Feststellung, Wiederherstellung und Abmarkung von Grenzen und zur Erfassung von baulichen Anlagen, die im Liegenschaftskataster nachzuweisen sind, dürfen nur geeignete Fachkräfte betraut werden, die sich in einem ständigen Arbeitsverhältnis mit der Öffentlich bestellten Vermessungsingenieurin oder dem Öffentlich bestellten Vermessungsingenieur oder einem beruflichen Zusammenschluss nach § 6 Absatz 1 befinden, das die Durchsetzung des uneingeschränkten Weisungsrechts sicherstellt.

(5) Bei hoheitlichen Tätigkeiten ist das kleine Landessiegel als Dienstsiegel zu führen.

(6) Öffentlich bestellte Vermessungsingenieurinnen und Öffentlich bestellte Vermessungsingenieure erheben für ihre Amtshandlungen Gebühren und Auslagen nach der Gebührenordnung für das amtliche Vermessungswesen im Land Brandenburg.
Für diese Amtshandlungen gelten § 1 Absatz 2 Nummer 4 und § 20 des Gebührengesetzes für das Land Brandenburg nicht.

(7) Öffentlich bestellte Vermessungsingenieurinnen und Öffentlich bestellte Vermessungsingenieure haben Vermessungsschriften und sonstige Ergebnisse den Katasterbehörden unmittelbar nach ihrer Erstellung zur Fortführung des Liegenschaftskatasters einzureichen.
Sie sind verpflichtet, Beanstandungen der Katasterbehörden unverzüglich zu beheben.

(8) Öffentlich bestellte Vermessungsingenieurinnen und Öffentlich bestellte Vermessungsingenieure haben von ihnen schuldhaft verursachte Mängel ihrer örtlichen Vermessungen und Vermessungsschriften sowie der sonstigen Ergebnisse auf ihre Kosten zu bereinigen.
Dies gilt auch dann, wenn ihre Vermessungsergebnisse bereits in das Liegenschaftskataster übernommen wurden.

(9) Hoheitliche Tätigkeiten sind so auszuführen, dass sie geeignet sind, dem Geobasisinformationssystem zu dienen.

### Abschnitt 3  
Aufsicht

#### § 10 Aufsicht; Einschränkung eines Grundrechts

(1) Die Aufsicht über die Öffentlich bestellten Vermessungsingenieurinnen und Öffentlich bestellten Vermessungsingenieure führt der Landesbetrieb Landesvermessung und Geobasisinformation Brandenburg (Aufsichtsbehörde).
Die Aufsichtsbehörde ist berechtigt, alle Maßnahmen zu treffen, die geeignet und erforderlich sind, die recht- und zweckmäßige Berufsausübung der ihrer Aufsicht unterstehenden Personen durchzusetzen und zu sichern.

(2) Nehmen Öffentlich bestellte Vermessungsingenieurinnen oder Öffentlich bestellte Vermessungsingenieure ihre Pflichten nicht oder nicht rechtzeitig wahr, kann die Aufsichtsbehörde die erforderliche Handlung auf deren Kosten veranlassen.
Beinhaltet diese Handlung den Abschluss einer Amtshandlung, ist die Aufsichtsbehörde Gläubigerin der Gebühren und Auslagen im Sinne des Gebührengesetzes für das Land Brandenburg.

(3) Öffentlich bestellte Vermessungsingenieurinnen und Öffentlich bestellte Vermessungsingenieure sind verpflichtet, der Aufsichtsbehörde jederzeit umfassende und sachgemäße Auskünfte über ihre Berufsausübung zu geben und den Beauftragten der Aufsichtsbehörde nach vorheriger Anmeldung während der üblichen Geschäftsstunden Zutritt zur Geschäftsstelle und Akteneinsicht zu gewähren.
Das Grundrecht der Unverletzlichkeit der Wohnung (Artikel 15 der Verfassung des Landes Brandenburg und Artikel 13 des Grundgesetzes) wird eingeschränkt, soweit die Geschäftsstelle zugleich Wohnzwecken dient.

#### § 11 Ahndung von Pflichtverletzungen

(1) Die Aufsichtsbehörde kann bei schuldhafter Verletzung der Berufspflichten nach Anhörung durch schriftlich begründeten Bescheid einen Verweis aussprechen oder eine Geldbuße bis zu fünfundzwanzigtausend Euro festsetzen oder die Zulassung widerrufen.
Die Möglichkeit, Weisungen zur ordnungsgemäßen Berufsausübung zu erteilen, bleibt unberührt.

(2) Nach Ablauf von fünf Jahren können Pflichtverletzungen nicht mehr geahndet werden.
Die Frist beginnt mit dem Tag, an dem die Pflichtverletzung begangen ist und wird durch die Einleitung eines Ahndungsverfahrens unterbrochen und für die Dauer eines auf eine Maßnahme nach Absatz 1 Satz 1 bezogenen Widerspruchsverfahrens oder verwaltungsgerichtlichen Verfahrens gehemmt.

(3) Sind Öffentlich bestellte Vermessungsingenieurinnen oder Öffentlich bestellte Vermessungsingenieure dringend verdächtig, Verfehlungen gegen ihre Berufspflichten begangen zu haben, die den Widerruf der Zulassung zur Folge haben können, kann die Aufsichtsbehörde die Berufsausübung bis zur endgültigen Entscheidung vorläufig untersagen.
§ 7 findet entsprechend Anwendung.

#### § 12 Personenbezogene Daten

(1) Behörden, Einrichtungen und sonstige öffentliche Stellen des Landes und der Kommunen übermitteln der Aufsichtsbehörde auf Ersuchen auch personenbezogene Daten, die für die Versagung, Rücknahme oder den Widerruf der Zulassung oder die Einleitung eines Verfahrens wegen Verletzung der Berufspflichten oder ordnungswidrigen Verhaltens von Bedeutung sein können.

(2) Die Aufsichtsbehörde führt zu Informationszwecken ein Verzeichnis der Öffentlich bestellten Vermessungsingenieurinnen und Öffentlich bestellten Vermessungsingenieure.
Es enthält deren Namen, Hinweise auf berufliche Zusammenschlüsse nach § 6 und die Anschrift der Geschäftsstelle sowie Telefonnummern und E-Mail-Adressen.
Das Verzeichnis ist öffentlich zugänglich zu machen und darf weitergegeben werden.

### Abschnitt 4  
Erlöschen der Zulassung

#### § 13 Erlöschen der Zulassung

(1) Die Zulassung erlischt,

1.  wenn die Rücknahme oder der Widerruf der Zulassung bestandskräftig geworden ist,
2.  mit dem Wirksamwerden des Verzichts auf die Zulassung oder
3.  mit dem Tod.

(2) Öffentlich bestellte Vermessungsingenieurinnen oder Öffentlich bestellte Vermessungsingenieure, die wegen hohen Alters oder wegen körperlicher Leiden auf die Zulassung verzichtet haben oder deren Zulassung wegen Erreichen der Altersgrenze aufgrund § 15 Absatz 1 Nummer 1 widerrufen wurde, dürfen ihre Berufsbezeichnung mit dem Zusatz „in Ruhe“ oder „i.R.“ führen.

#### § 14 Rücknahme der Zulassung

Die Aufsichtsbehörde hat die Zulassung mit Wirkung für die Zukunft zurückzunehmen, wenn Tatsachen nachträglich bekannt werden, bei deren Kenntnis die Zulassung hätte versagt werden müssen.
Von der Rücknahme der Zulassung kann abgesehen werden, wenn die Gründe, aus denen die Zulassung hätte versagt werden müssen, nicht mehr bestehen.
Im Übrigen bleibt § 48 des Verwaltungsverfahrensgesetzes in Verbindung mit § 1 Absatz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg unberührt.

#### § 15 Widerruf der Zulassung

(1) Die Aufsichtsbehörde hat die Zulassung mit Wirkung für die Zukunft zu widerrufen,

1.  wenn nachträglich Tatsachen eintreten, aufgrund derer die Aufsichtsbehörde nach § 3 berechtigt wäre, die Zulassung zu versagen; die fehlende erforderliche geistige und körperliche Leistungsfähigkeit wird vermutet, wenn die Öffentlich bestellte Vermessungsingenieurin oder der Öffentlich bestellte Vermessungsingenieur das 70.
    Lebensjahr vollendet hat;
2.  aufgrund der Anzeige eines Versicherers nach § 117 Absatz 2 des Versicherungsvertragsgesetzes über den Umstand, der das Nichtbestehen oder die Beendigung der Haftpflichtversicherung nach § 8 Absatz 3 zur Folge hat.

(2) Die Aufsichtsbehörde kann die Zulassung widerrufen, wenn die Öffentlich bestellte Vermessungsingenieurin oder der Öffentlich bestellte Vermessungsingenieur

1.  länger als ein Jahr keine hoheitlichen Tätigkeiten ausgeübt hat oder
2.  überwiegend Erwerbstätigkeiten ausübt, die nicht den Tätigkeiten nach § 1 Absatz 2 und 3 zuzurechnen sind.

(3) Im Übrigen bleibt § 49 des Verwaltungsverfahrensgesetzes in Verbindung mit § 1 Absatz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg unberührt.

#### § 16 Verzicht auf die Zulassung

Will eine Öffentlich bestellte Vermessungsingenieurin oder ein Öffentlich bestellter Vermessungsingenieur auf die Zulassung verzichten, so ist dies der Aufsichtsbehörde schriftlich mitzuteilen.
Der Verzicht auf die Zulassung wird durch Bestätigung der Aufsichtsbehörde zu dem Zeitpunkt wirksam, zu dem alle anhängigen Anträge durch die Öffentlich bestellte Vermessungsingenieurin oder den Öffentlich bestellten Vermessungsingenieur abgeschlossen sind.
Ein Antrag kann auch dadurch abgeschlossen werden, dass dessen Ausführung auf eine andere Öffentlich bestellte Vermessungsingenieurin oder einen anderen Öffentlich bestellten Vermessungsingenieur einvernehmlich übertragen wird und die Antragstellerin oder der Antragsteller der Übertragung zugestimmt hat.
Bis zum Wirksamwerden des Verzichts dürfen neue Anträge nicht angenommen werden.

#### § 17 Geschäftsabwicklung

(1) Erlischt die Zulassung einer Öffentlich bestellten Vermessungsingenieurin oder eines Öffentlich bestellten Vermessungsingenieurs nach § 13 Absatz 1 Nummer 1 oder Nummer 3, regelt die Aufsichtsbehörde den Abschluss der hoheitlichen Tätigkeiten (Geschäftsabwicklung).
Beginn und Abschluss der Geschäftsabwicklung sind den Katasterbehörden von der Aufsichtsbehörde bekannt zu geben.

(2) Die Aufsichtsbehörde erstellt eine Übersicht aller noch nicht abgeschlossenen hoheitlichen Tätigkeiten und informiert die Antragstellerinnen und Antragsteller und die betroffenen Katasterbehörden über die Geschäftsabwicklung.
Die Kosten für die Erstellung dieser Übersicht, der Beauftragung nach Absatz 3 und der Abschlussrechnung nach Absatz 6 trägt die ausgeschiedene Öffentlich bestellte Vermessungsingenieurin oder der ausgeschiedene Öffentlich bestellte Vermessungsingenieur.

(3) Die Aufsichtsbehörde beauftragt eine oder mehrere Öffentlich bestellte Vermessungsingenieurinnen oder Öffentlich bestellte Vermessungsingenieure oder die jeweils zuständige Katasterbehörde, die begonnenen hoheitlichen Tätigkeiten zum Abschluss zu bringen.
Dieser Auftrag darf von Öffentlich bestellten Vermessungsingenieurinnen und Öffentlich bestellten Vermessungsingenieuren nur aus einem wichtigen Grund abgelehnt werden; über die Ablehnung entscheidet die Aufsichtsbehörde.
Die Aufsichtsbehörde kann die Beauftragung jederzeit widerrufen.

(4) Die nach Absatz 3 Beauftragten haben die Kosten für die gesamte Amtshandlung im eigenen Namen geltend zu machen.
Einen bereits an die ausgeschiedene Öffentlich bestellte Vermessungsingenieurin oder den ausgeschiedenen Öffentlich bestellten Vermessungsingenieur gezahlten Vorschuss müssen sie sich dabei anrechnen lassen; dieser Vorschuss wird ihnen von der Aufsichtsbehörde erstattet.
Sind Leistungen der ausgeschiedenen Öffentlich bestellten Vermessungsingenieurin oder des ausgeschiedenen Öffentlich bestellten Vermessungsingenieurs bei der abschließenden Bearbeitung der Amtshandlung verwendet worden, so haben die nach Absatz 3 Beauftragten diese Leistungsanteile zu beschreiben und der Aufsichtsbehörde die von ihnen hierfür festgesetzten Gebührenanteile zu erstatten.
Bedienen sich die Beauftragten nach Absatz 3 des Personals oder der Sachmittel der Geschäftsstelle der ausgeschiedenen Öffentlich bestellten Vermessungsingenieurin oder des ausgeschiedenen Öffentlich bestellten Vermessungsingenieurs, so haben sie dies eigenverantwortlich abzugelten.

(5) Die mit der Abwicklung befassten Personen sind berechtigt, die Räume der Geschäftsstelle der ausgeschiedenen Öffentlich bestellten Vermessungsingenieurin oder des ausgeschiedenen Öffentlich bestellten Vermessungsingenieurs zu betreten; § 10 Absatz 3 gilt entsprechend.
Sie sind berechtigt, alle zur Abwicklung erforderlichen analogen Unterlagen und digitalen Daten zu sichten und sicherzustellen.

(6) Abschließend stellt die Aufsichtsbehörde alle Kostenansprüche nach Absatz 2 Satz 2 und Absatz 4 Satz 2 und 3 zusammen und verrechnet sie gegeneinander.
Der sich aus dieser Verrechnung ergebende Kostenanspruch ist der ausgeschiedenen Öffentlich bestellten Vermessungsingenieurin oder dem ausgeschiedenen Öffentlich bestellten Vermessungsingenieur zu erstatten beziehungsweise wird von der Aufsichtsbehörde geltend gemacht.

(7) Im Falle eines Insolvenzverfahrens hat die Aufsichtsbehörde in Abstimmung mit der Insolvenzverwalterin oder dem Insolvenzverwalter die Abwicklung der noch nicht abgeschlossenen hoheitlichen Tätigkeiten zu betreiben und abschließend das Ergebnis nach Absatz 6 der Insolvenzverwalterin oder dem Insolvenzverwalter mitzuteilen.

### Abschnitt 5  
Ordnungswidrigkeiten, Übergangs- und Schlussbestimmungen

#### § 18 Ordnungswidrigkeiten

(1) Ordnungswidrig handelt, wer

1.  die Berufsbezeichnung nach § 1 Absatz 1 führt, ohne die Zulassung zu besitzen oder obwohl die Zulassung erloschen ist und keine Berechtigung nach § 13 Absatz 2 besteht,
2.  die Ausführung von hoheitlichen Tätigkeiten nach § 1 Absatz 2 in eigenem Namen anbietet oder abrechnet, ohne hierzu berechtigt zu sein,
3.  die Öffentlich bestellte Vermessungsingenieurin oder den Öffentlich bestellten Vermessungsingenieur zu einer Unterschreitung der Gebühren auffordert.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

(3) Verwaltungsbehörde gemäß § 36 Absatz 1 Nummer 1 des Gesetzes über Ordnungswidrigkeiten ist die Aufsichtsbehörde.

(4) Ist zum Tatbestand einer Ordnungswidrigkeit auch ein strafrechtliches Verfahren eingeleitet worden, kann die Aufsichtsbehörde das Verfahren zurückstellen und über die Ordnungswidrigkeit unter Berücksichtigung des Ergebnisses des strafrechtlichen Verfahrens entscheiden.
§ 21 des Gesetzes über Ordnungswidrigkeiten bleibt unberührt.

#### § 19 Einschränkung eines weiteren Grundrechts

Durch dieses Gesetz wird das Grundrecht auf Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 20 Anhörung der Berufsvertretung

Die Berufsvertretung der Öffentlich bestellten Vermessungsingenieurinnen und Öffentlich bestellten Vermessungsingenieure soll bei der Vorbereitung allgemeiner Regelungen ihrer Rechtsverhältnisse angehört werden.

#### § 21 Rechtsverordnungen

Das für das Vermessungs- und Katasterwesen zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnungen zu regeln:

1.  Einzelheiten der Aufsicht, insbesondere bezüglich des Selbsteintrittsrechts (§ 10 Absatz 2), der Ahndung von Pflichtverletzungen (§ 11) und der Geschäftsabwicklung (§ 17),
2.  Einzelheiten der Berufsausübung, insbesondere bezüglich der Ausstattung der Geschäftsstelle (§ 5 Absatz 2), des Umfangs und der Höhe der Haftpflichtversicherung (§ 8 Absatz 3), des Umfangs und des Nachweises der Fortbildungspflicht (§ 8 Absatz 5) und der Mitwirkung von Fachkräften (§ 9 Absatz 4).

#### § 22 Übergangsbestimmungen

(1) Die auf der Grundlage des vor Inkrafttreten dieses Gesetzes geltenden Rechts im Land Brandenburg zugelassenen Öffentlich bestellten Vermessungsingenieurinnen und Öffentlich bestellten Vermessungsingenieure gelten als zugelassen im Sinne dieses Gesetzes.

(2) Die nach § 6 Absatz 1 Satz 4 der ÖbVI-Berufsordnung vom 18.
Oktober 2000 (GVBl.
I S. 142), die zuletzt durch Artikel 3 des Gesetzes vom 13.
April 2010 (GVBl.
I Nr. 17 S. 12) geändert worden ist, erlaubten Kooperationen dürfen bis zum Ablauf des 31.
Dezember des dritten Jahres nach dem Inkrafttreten dieses Gesetzes nach dem vor Inkrafttreten dieses Gesetzes geltenden Recht weitergeführt werden.

(3) Die auf der Grundlage des vor Inkrafttreten dieses Gesetzes geltenden Rechts übertragenden Verfahren zum Abschluss der Geschäfte sind nach dem bis dahin geltenden Recht fortzuführen.

#### § 23 Inkrafttreten, Außerkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig treten außer Kraft:

1.  ÖbVI-Berufsordnung vom 18.
    Oktober 2000 (GVBl.
    I S. 142), die zuletzt durch Artikel 3 des Gesetzes vom 13. April 2010 (GVBl.
    I Nr. 17 S. 12) geändert worden ist,
2.  ÖbVI-Geschäftsabschlussverordnung vom 29.
    September 2001 (GVBl.
    II S. 622), die zuletzt durch Artikel 4 des Gesetzes vom 13.
    April 2010 (GVBl.
    I Nr. 17 S. 12) geändert worden ist,
3.  Zulassungsprüfungsverordnung vom 6.
    November 2000 (GVBl.
    II S. 414), die zuletzt durch Artikel 5 des Gesetzes vom 13.
    April 2010 (GVBl.
    I Nr. 17 S. 12) geändert worden ist.

Potsdam, den 28.
November 2016

Die Präsidentin  
des Landtages Brandenburg

Britta Stark