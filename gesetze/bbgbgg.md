## Gesetz des Landes Brandenburg zur Gleichstellung von Menschen mit Behinderungen (Brandenburgisches Behindertengleichstellungsgesetz - BbgBGG)

Der Landtag hat das folgende Gesetz beschlossen:

### Abschnitt 1  
Allgemeine Bestimmungen

#### § 1  
Gesetzesziel

(1) Ziel dieses Gesetzes ist es, in Umsetzung des Übereinkommens der Vereinten Nationen vom 13. Dezember 2006 über die Rechte von Menschen mit Behinderungen (BGBl.
2008 II S. 1420) Diskriminierungen von Menschen mit Behinderungen im Land Brandenburg zu verhindern und zu beseitigen, gleichwertige Lebensbedingungen und Chancengleichheit sowie die gleichberechtigte Teilhabe am Leben in der Gesellschaft zu gewährleisten und eine selbstbestimmte Lebensführung zu ermöglichen.
Dabei wird spezifischen Bedürfnissen von Menschen mit Behinderungen Rechnung getragen.

(2) Die Schaffung einer inklusiven Gesellschaft ist eine gesamtgesellschaftliche Aufgabe.

#### § 2  
Geltungsbereich

(1) Dieses Gesetz gilt für das Land, die Gemeinden, die Gemeindeverbände sowie für die sonstigen der Aufsicht des Landes unterstehenden juristischen Personen des öffentlichen Rechts des Landes Brandenburg.

(2) Die in Absatz 1 genannten Träger der öffentlichen Verwaltung sollen darauf hinwirken, dass auch Einrichtungen, Vereinigungen und Unternehmen, deren Anteile sich unmittelbar oder mittelbar ganz oder überwiegend in ihrer Hand befinden, die Ziele dieses Gesetzes berücksichtigen.

(3) Die in Absatz 1 genannten Träger der öffentlichen Verwaltung berücksichtigen bei der Gewährung von Zuwendungen und sonstigen Leistungen die Ziele dieses Gesetzes.

#### § 3  
Begriffsbestimmungen

(1) Menschen mit Behinderungen im Sinne dieses Gesetzes sind Menschen, die langfristige körperliche, seelische, geistige oder Sinnesbeeinträchtigungen haben, welche sie in Wechselwirkung mit einstellungs- und umweltbedingten Barrieren an der gleichberechtigten Teilhabe an der Gesellschaft hindern können.

(2) Eine Diskriminierung liegt vor, wenn Menschen mit und ohne Behinderungen ohne Grund unterschiedlich behandelt werden und dadurch Menschen mit Behinderungen in der gleichberechtigten Teilhabe am Leben in der Gesellschaft unmittelbar beeinträchtigt werden.

(3) Barrierefreiheit liegt vor, wenn bauliche und sonstige Anlagen, Verkehrsmittel, technische Gebrauchsgegenstände, Systeme der Informationsverarbeitung, akustische und visuelle Informationsquellen und Kommunikationseinrichtungen sowie andere gestaltete Lebensbereiche für Menschen mit Behinderungen in der allgemein üblichen Weise ohne besondere Erschwernis und grundsätzlich ohne fremde Hilfe zugänglich und nutzbar sind.
Eine besondere Erschwernis liegt auch dann vor, wenn Menschen mit Behinderungen die Mitnahme oder der Einsatz benötigter Hilfsmittel verweigert oder erschwert wird.

### Abschnitt 2  
Verpflichtung zur Gleichstellung und Barrierefreiheit

#### § 4  
Diskriminierungsverbot, Beweislasterleichterung

(1) Die in § 2 Absatz 1 benannten Träger der öffentlichen Verwaltung dürfen bei der Erfüllung ihrer gesetzlichen oder satzungsmäßigen Aufgaben Menschen mit Behinderungen nicht diskriminieren.

(2) Wenn ein Mensch mit Behinderung Sachverhalte oder Tatsachen glaubhaft macht, die eine Diskriminierung aufgrund seiner Behinderung vermuten lassen, ist diese Vermutung im Streitfalle von der Gegenseite zu widerlegen.

#### § 5  
Gleichstellungsgebot

(1) Zur Durchsetzung der Gleichstellung von Menschen mit und ohne Behinderungen sind die spezifischen Belange von Menschen mit Behinderungen zu berücksichtigen und bestehende Diskriminierungen zu beseitigen.
Dabei sind besondere Maßnahmen zulässig, die die gleichberechtigte Teilhabe von Menschen mit Behinderungen fördern und bestehende Diskriminierungen beseitigen.

(2) Zur Durchsetzung der Gleichstellung von Frauen und Männern sind die spezifischen Belange von Frauen mit Behinderungen zu berücksichtigen und bestehende Diskriminierungen zu beseitigen.
Dabei sind auch besondere Maßnahmen zur Förderung der tatsächlichen Durchsetzung der Gleichstellung von Frauen mit Behinderungen und zur Beseitigung bestehender Diskriminierungen zulässig.

(3) Zur Verwirklichung einer selbstbestimmten Elternschaft sind die spezifischen Bedürfnisse von Eltern mit Behinderungen zu berücksichtigen.

#### § 6  
Gebärdensprache und andere Kommunikationshilfen

(1) Die Deutsche Gebärdensprache ist als eigenständige Sprache anerkannt.

(2) Lautsprachbegleitende Gebärden sind als Kommunikationsform der deutschen Sprache anerkannt.

(3) Menschen mit einer Hörbehinderung (Gehörlose, Ertaubte und Schwerhörige) und Menschen mit einer Sprachbehinderung haben nach Maßgabe der einschlägigen Gesetze das Recht, die Deutsche Gebärdensprache oder die als Kommunikationsform anerkannten lautsprachbegleitenden Gebärden zu verwenden.
Soweit sie sich nicht in Deutscher Gebärdensprache oder mit lautsprachbegleitenden Gebärden verständigen, haben sie nach Maßgabe der einschlägigen Gesetze das Recht, andere geeignete Kommunikationshilfen zu verwenden.

#### § 7  
Recht auf Verwendung von Gebärdensprache und anderen Kommunikationshilfen

(1) Menschen mit einer Hör- und Sprachbehinderung haben das Recht, mit den in § 2 Absatz 1 genannten Trägern der öffentlichen Verwaltung in Deutscher Gebärdensprache, mit lautsprachbegleitenden Gebärden oder mit anderen geeigneten Kommunikationshilfen zu kommunizieren, insbesondere soweit dies zur Wahrnehmung eigener Rechte im Verwaltungsverfahren erforderlich ist.
Hierfür haben die in Satz 1 genannten Träger der öffentlichen Verwaltung auf Wunsch der Berechtigten im notwendigen Umfang die Übersetzung durch Gebärdensprachdolmetscherinnen und ‑dolmetscher oder die Verständigung mit anderen geeigneten Kommunikationshilfen ohne zusätzliche Kosten sicherzustellen.

(2) _**(aufgehoben)**_

(3) Die hierfür entstehenden Kosten werden durch das Land getragen.

(4) Das für Soziales zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit dem für Schule und Jugend und dem für Finanzen zuständigen Mitglied der Landesregierung Anlass und Umfang der Bereitstellung einer Gebärdensprachdolmetscherin oder eines Gebärdensprachdolmetschers oder anderer geeigneter Kommunikationshilfen durch Rechtsverordnung zu regeln.

#### § 8  
Gestaltung von Bescheiden und Vordrucken

(1) Die in § 2 Absatz 1 genannten Träger der öffentlichen Verwaltung haben bei der Gestaltung von Bescheiden, Allgemeinverfügungen, öffentlich-rechtlichen Verträgen und Vordrucken eine Behinderung von Menschen zu berücksichtigen.
Soweit Schwierigkeiten mit dem Textverständnis bestehen, sind Bescheide, Allgemeinverfügungen, öffentlich-rechtliche Verträge und Vordrucke in leicht verständlicher Sprache zu erläutern.

(2) Blinde Menschen und Menschen mit einer Sehbehinderung können insbesondere verlangen, dass ihnen Bescheide, öffentlich-rechtliche Verträge und Vordrucke ohne zusätzliche Kosten auch in einer für sie wahrnehmbaren Form zugänglich gemacht werden, soweit dies zur Wahrnehmung eigener Rechte im Verwaltungsverfahren erforderlich ist.
Die Verordnung über barrierefreie Dokumente in der Bundesverwaltung vom 17.
Juli 2002 (BGBI.
I S.
2652) findet entsprechende Anwendung.

#### § 9  
Barrierefreie Informationstechnik

(1) Die in § 2 Absatz 1 genannten Träger der öffentlichen Verwaltung gestalten ihre Internet- und Intranetauftritte und -angebote sowie die von ihnen zur Verfügung gestellten graphischen Programmoberflächen, die mit Mitteln der Informationstechnik dargestellt werden, unter Berücksichtigung der nach Absatz 2 zu erlassenden Verordnung schrittweise technisch so, dass sie von Menschen mit Behinderungen grundsätzlich uneingeschränkt genutzt werden können.

(2) Das für Soziales zuständige Mitglied der Landesregierung wird ermächtigt, für das Land und für die der Aufsicht des Landes unterstehenden juristischen Personen des öffentlichen Rechts im Einvernehmen mit dem für Inneres zuständigen Mitglied der Landesregierung durch Rechtsverordnung

1.  die in den Geltungsbereich der Verordnung einzubeziehenden Gruppen von Menschen mit Behinderungen,
2.  die anzuwendenden technischen Standards sowie den Zeitpunkt ihrer verbindlichen Anwendung,
3.  die zu gestaltenden Bereiche und Arten amtlicher Informationen,
4.  die Informationspflichten bei Internetauftritten und -angeboten, die zur Barrierefreiheit veröffentlicht werden sollen,
5.  das Verfahren zur Überwachung nach den Vorgaben des Artikels 8 Absatz 1 bis 3 der Richtlinie (EU) 2016/2102 des Europäischen Parlaments und des Rates vom 26. Oktober 2016 über den barrierefreien Zugang zu den Websites und mobilen Anwendungen öffentlicher Stellen (ABl. L 327 vom 2.12.2016, S.
    1) sowie das Verfahren zur Berichterstattung, um die Vorgaben des Artikels 8 Absatz 4 bis 6 der Richtlinie (EU) 2016/2102 zu erfüllen,
6.  das Verfahren, um die Einhaltung der Anforderungen der Artikel 4, 5 und 7 Absatz 1 der Richtlinie (EU) 2016/2102 zu gewährleisten,

zu bestimmen.

(3) Für Websites und mobile Anwendungen im Sinne des Artikels 1 der Richtlinie (EU) 2016/2102 gilt für öffentliche Stellen im Sinne des Artikels 3 Nummer 1 der Richtlinie (EU) 2016/2102 Absatz 1 entsprechend.

(4) Die Gemeinden und Gemeindeverbände treffen für ihren Zuständigkeitsbereich entsprechende Regelungen.

#### § 10  
Zielvereinbarungen

(1) Zur Herstellung von Barrierefreiheit sollen Zielvereinbarungen zwischen den Landesverbänden von Menschen mit Behinderungen und den Unternehmen und Unternehmensverbänden für ihren jeweiligen sachlichen und räumlichen Organisations- oder Tätigkeitsbereich geschlossen werden, soweit keine Zielvereinbarung nach § 5 Absatz 1 des Behindertengleichstellungsgesetzes besteht.

(2) Zur Herstellung von Barrierefreiheit können Zielvereinbarungen zwischen den Landesverbänden von Menschen mit Behinderungen und den in § 2 Absatz 1 genannten Trägern der öffentlichen Verwaltung geschlossen werden.

(3) Für das Verfahren zu Zielvereinbarungen ist § 5 Absatz 1, 2 Satz 1 und Absatz 4 des Behindertengleichstellungsgesetzes entsprechend anzuwenden.
Der die Zielvereinbarung abschließende Verband von Menschen mit Behinderungen teilt dem für Soziales zuständigen Mitglied der Landesregierung den Abschluss einer Zielvereinbarung mit.

### Abschnitt 3  
Rechtsbehelfe

#### § 11  
Vertretungsbefugnis in verwaltungsrechtlichen und sozialrechtlichen Verfahren

Werden Menschen mit Behinderungen in ihren Rechten aus § 7 Absatz 1 oder § 8 Absatz 2 verletzt oder machen sie geltend, dass in einem anderen Verfahren gegen § 4 Absatz 1 verstoßen worden ist, können an ihrer Stelle und mit ihrem Einverständnis die nach § 12 Absatz 3 anerkannten Verbände, die nicht selbst am Verfahren beteiligt sind, Rechtsschutz beantragen.
Gleiches gilt bei Verstößen gegen die Vorschriften des Landesrechts, die einen Anspruch auf Herstellung von Barrierefreiheit im Sinne des § 3 Absatz 3 oder auf Verwendung von Gebärden oder anderen Kommunikationshilfen im Sinne des § 6 Absatz 3 vorsehen.
In diesem Fall müssen alle Verfahrensvoraussetzungen wie bei einem Rechtsschutzersuchen durch den Menschen mit Behinderung selbst vorliegen.

#### § 12  
Verbandsklage

(1) Ein nach Absatz 3 anerkannter Verband kann, ohne in seinen Rechten verletzt zu sein, Klage nach Maßgabe der Verwaltungsgerichtsordnung oder des Sozialgerichtsgesetzes erheben auf Feststellung eines Verstoßes gegen das Diskriminierungsverbot nach § 4 Absatz 1, gegen die Verpflichtung zur Herstellung der Barrierefreiheit gemäß § 7 Absatz 1, § 8 Absatz 2 und § 9 oder gegen die Vorschriften des Landesrechts, die einen Anspruch auf Herstellung von Barrierefreiheit im Sinne des § 3 Absatz 3 vorsehen.
Satz 1 gilt nicht, wenn eine Maßnahme aufgrund einer Entscheidung in einem verwaltungs- oder sozialgerichtlichen Streitverfahren erlassen worden ist.

(2) Eine Klage oder ein Antrag ist nur zulässig, wenn der Verband durch die Maßnahmen in seinem satzungsgemäßen Aufgabenbereich berührt wird.
Soweit ein Mensch mit Behinderung selbst seine Rechte durch eine Gestaltungs- oder Leistungsklage verfolgen kann oder hätte verfolgen können, kann die Klage oder der Antrag nach Absatz 1 nur erhoben werden, wenn der Verband geltend macht, dass es sich bei der Maßnahme um einen Fall von allgemeiner Bedeutung handelt.
Dies ist insbesondere der Fall, wenn eine Vielzahl gleichgelagerter Fälle vorliegt.

(3) Die Anerkennung eines Verbandes kann das für Soziales zuständige Mitglied der Landesregierung nach Anhörung der beauftragten Person erteilen, wenn der Verband

1.  nach seiner Satzung nicht nur vorübergehend die Belange von Menschen mit Behinderungen fördert,
2.  nach der Zusammensetzung seiner Mitglieder der Mitgliedsverbände dazu berufen ist, Interessen von Menschen mit Behinderungen auf Landesebene zu vertreten,
3.  zum Zeitpunkt der Anerkennung mindestens drei Jahre besteht und in diesem Zeitraum im Sinne der Nummer 1 tätig gewesen ist,
4.  die Gewähr für eine sachgerechte Aufgabenerfüllung bietet; dabei sind Art und Umfang seiner bisherigen Tätigkeit, der Mitgliederkreis sowie die Leistungsfähigkeit des Vereins zu berücksichtigen und
5.  wegen Verfolgung gemeinnütziger Zwecke nach § 5 Absatz 1 Nummer 9 des Körperschaftsteuergesetzes von der Körperschaftsteuer befreit ist.

### Abschnitt 4  
Interessensvertretung der Menschen mit Behinderungen

#### § 13  
Beauftragte oder Beauftragter der Landesregierung für die Belange der Menschen mit Behinderungen

Auf Vorschlag des für Soziales zuständigen Mitgliedes der Landesregierung wird durch die Landesregierung für die Dauer der Wahlperiode eine Beauftragte oder ein Beauftragter der Landesregierung für die Belange der Menschen mit Behinderungen (beauftragte Person) berufen.
Der Landesbehindertenbeirat ist hierbei zu beteiligen.
Erneute Berufungen sind zulässig.
Die mit der Wahrnehmung der Aufgabe beauftragte Person hat ein direktes Vortragsrecht bei dem für Soziales zuständigen Mitglied der Landesregierung.
Sie übt ihre Tätigkeit nach diesem Gesetz weisungsfrei und ressortübergreifend aus.

#### § 14   
Aufgaben und Befugnisse

(1) Aufgabe der beauftragten Person ist es, insbesondere

1.  darauf hinzuwirken, dass die Verantwortung des Landes für gleichwertige Lebensbedingungen für Menschen mit und ohne Behinderungen in allen Bereichen des gesellschaftlichen Lebens erfüllt wird,
2.  sich dafür einzusetzen, dass unterschiedliche Lebensbedingungen von Frauen mit Behinderungen und Männern mit Behinderungen berücksichtigt und geschlechtsspezifische Diskriminierungen beseitigt werden,
3.  Maßnahmen in Umsetzung des Übereinkommens der Vereinten Nationen vom 13. Dezember 2006 über die Rechte von Menschen mit Behinderungen anzuregen und dabei die zivilgesellschaftlichen Beteiligten einzubinden.

(2) Zur Wahrnehmung dieser Aufgaben ist die beauftragte Person, soweit die Belange der Menschen mit Behinderungen betroffen sind, bei Gesetzgebungs- und Verordnungsvorhaben frühzeitig zu beteiligen.

(3) Jede Person hat das Recht, sich mit Bitten, Beschwerden und Anregungen unmittelbar an die beauftragte Person zu wenden, wenn sie der Auffassung ist, dass Verstöße gegen die Rechte und Belange von Menschen mit Behinderungen erfolgt sind oder drohen.

(4) Die in § 2 Absatz 1 genannten Träger der öffentlichen Verwaltung unterstützen die beauftragte Person bei der Erfüllung ihrer Aufgaben, insbesondere erteilen sie die erforderlichen Auskünfte und gewähren Akteneinsicht, soweit dies zur sachgerechten Aufgabenwahrnehmung erforderlich ist.
§ 29 Absatz 2 des Verwaltungsverfahrensgesetzes in Verbindung mit § 1 Absatz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg gilt hinsichtlich der Erteilung von Auskünften und der Gewährung von Akteneinsicht entsprechend.
Die Bestimmungen zum Schutz personenbezogener Daten bleiben unberührt.

(5) Zur Erfüllung der Aufgaben fördert die beauftragte Person die Zusammenarbeit und den Erfahrungsaustausch zwischen den kommunalen Behindertenbeauftragten der Landkreise, dem Landesbehindertenbeirat, den Behindertengruppen, -vereinen und -verbänden, Gewerkschaften und sonstigen Organisationen, die sich mit den besonderen Interessen von Menschen mit Behinderungen befassen, und unterstützt deren Tätigkeit.

(6) Die beauftragte Person legt dem Landtag alle fünf Jahre einen Bericht zur Umsetzung dieses Gesetzes vor.
Bei der Erfüllung der Berichtspflicht wird eine zusammenfassende, nach Geschlecht differenzierte Darstellung und Bewertung abgegeben.

#### § 15  
Landesbehindertenbeirat

(1) Das für Soziales zuständige Mitglied der Landesregierung beruft gemäß § 16 Absatz 1 einen ehrenamtlich tätigen Landesbehindertenbeirat.
Das Land leistet Zuwendungen zu den angemessenen Personal- und Sachkosten der Geschäftsstelle des Landesbehindertenbeirates.

(2) Der Landesbehindertenbeirat nimmt die Interessen der Menschen mit Behinderungen nach Teil 2 des Neunten Buches Sozialgesetzbuch wahr.

(3) Der Landesbehindertenbeirat unterstützt die Landesregierung bei der Aufgabe, gleichwertige Lebensbedingungen für Menschen mit und ohne Behinderungen zu schaffen.
Er berät die Landesregierung und die beauftragte Person in allen Angelegenheiten und ist berechtigt, ihr und der Landesregierung Empfehlungen zu geben.

(4) Der Landesbehindertenbeirat soll von der Landesregierung vor dem Einbringen von Gesetzentwürfen und dem Erlass von Rechtsverordnungen, die die Belange von Menschen mit Behinderungen betreffen, angehört werden.

#### § 16  
Mitglieder

(1) Das für Soziales zuständige Mitglied der Landesregierung beruft auf Vorschlag je eine Vertreterin oder einen Vertreter der landesweit tätigen rechtsfähigen Behindertenverbände sowie der Landesarbeitsgemeinschaft der Werkstatträte Brandenburg e.
V. als stimmberechtigte Mitglieder in den Landesbehindertenbeirat.

(2) Als nicht stimmberechtigte Mitglieder gehören dem Landesbehindertenbeirat je eine Vertreterin oder ein Vertreter

1.  des Landkreistages Brandenburg e. V.,
2.  des Städte- und Gemeindebundes Brandenburg,
3.  der Verbände der Freien Wohlfahrtspflege,
4.  der Regionaldirektion Berlin-Brandenburg der Bundesagentur für Arbeit,
5.  der Arbeitgeberverbände des Landes Brandenburg,
6.  der Gewerkschaften des Landes Brandenburg,
7.  des Behindertensportverbandes Brandenburg e. V.
    und
8.  des Landesamtes für Soziales und Versorgung

an.

(3) Auf Vorschlag des Landesbehindertenbeirates kann das für Soziales zuständige Mitglied der Landesregierung weitere Mitglieder berufen.

(4) Bei den Vorschlägen und bei der Berufung sollen Frauen und Männer in gleicher Zahl berücksichtigt werden.

(5) Die Mitglieder des Landesbehindertenbeirates wählen aus dem Kreis der entsandten Vertreterinnen und Vertreter der landesweit tätigen rechtsfähigen Behindertenverbände eine den Vorsitz führende Person und die stellvertretenden Personen.

(6) Der Landesbehindertenbeirat arbeitet eng mit den behindertenpolitisch sachverständigen Personen, Institutionen und Verbänden zusammen und lädt diese bei Bedarf zu seinen Sitzungen ein.

(7) Der Landesbehindertenbeirat gibt sich eine Geschäftsordnung, die Näheres über die Arbeitsweise und über die Zusammenarbeit mit den behindertenpolitischen Beteiligten festlegt.

#### § 17  
Überprüfung des Gesetzes

Drei Jahre nach Inkrafttreten ist dieses Gesetz hinsichtlich seiner Wirkungen zu überprüfen.
Der Landesregierung ist Bericht über die Prüfung durch das für Soziales zuständige Mitglied der Landesregierung zu erstatten.

#### § 18  
Inkrafttreten, Außerkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt das Brandenburgische Behindertengleichstellungsgesetz vom 20.
März 2003 (GVBl.
I S.
42), die Brandenburgische Verordnung über barrierefreie Dokumente in der Landesverwaltung vom 24.
Mai 2004 (GVBI.
II S.
489) sowie die Brandenburgische Kommunikationshilfenverordnung vom 24.
Mai 2004 (GVBI.
II S.
490) außer Kraft.
§ 7 Absatz 2 tritt am 31. Dezember 2014 außer Kraft.

Potsdam, den 11.
Februar 2013

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch