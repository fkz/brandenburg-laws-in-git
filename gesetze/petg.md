## Gesetz über die Behandlung von Petitionen an den Landtag Brandenburg (Petitionsgesetz -  PetG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1   
Petitionsberechtigung

(1) Petitionsberechtigt ist jede Person unabhängig von ihrer Geschäftsfähigkeit oder Staatsangehörigkeit.
Petitionen können von Einzelpersonen oder mehreren Personen gemeinsam bei dem Landtag Brandenburg eingereicht werden.

(2) Mitarbeiter des öffentlichen Dienstes können Petitionen auch in dienstlichen Angelegenheiten ohne Einhaltung des Dienstweges einreichen.

(3) Juristische Personen des Privatrechts sind petitionsberechtigt.

(4) Juristischen Personen des öffentlichen Rechts steht das Petitionsrecht insoweit zu, als die Petition einen Gegenstand ihres sachlichen Zuständigkeitsbereiches betrifft.

(5) Petitionen können auch durch gesetzliche oder rechtsgeschäftliche Vertreter eingereicht werden.

#### § 2  
Form und Inhalt der Petition

(1) Die Petition ist schriftlich oder elektronisch einzureichen und muss den Petenten erkennen lassen.

(2) Sammelpetitionen sind Petitionen, bei denen sich mindestens 30 Personen mit einem wortgleichen oder im Wesentlichen wortgleichen Anliegen an den Landtag wenden und eine Person oder Personengemeinschaft als Initiator der Petitionen in Erscheinung tritt.
Sie werden als eine Petition geführt.

(3) Massenpetitionen sind Petitionen, bei denen sich mindestens 30 Personen mit einem wortgleichen oder im Wesentlichen wortgleichen Anliegen an den Landtag wenden, ohne dass eine bestimmte Person oder Personengemeinschaft als Initiator der Petitionen in Erscheinung tritt.
Sie werden als eine Petition geführt.

#### § 3  
Petitionen inhaftierter oder in psychiatrischen Einrichtungen untergebrachter Personen

Petitionen inhaftierter oder in psychiatrischen Einrichtungen untergebrachter Personen sind verschlossen und ohne Kontrolle dem Landtag zuzuleiten.
Gleiches gilt für Schreiben des Landtages an diese Petenten.

#### § 4   
Verfahren im Landtag

(1) Über die dem Landtag zugeleiteten Petitionen entscheidet ein aus Mitgliedern des Landtages bestehender, für diesen besonderen Zweck eingesetzter Petitionsausschuss.
Von einer sachlichen Prüfung kann abgesehen werden, wenn die Petition Verstöße gegen Strafgesetze beinhaltet oder zum Ziel hat oder lediglich den Inhalt einer früheren Petition desselben Petenten aus derselben Wahlperiode ohne wesentlich neue Gesichtspunkte wiederholt.

(2) Der Petitionsausschuss kann die Petition zur endgültigen Beschlussfassung dem Plenum des Landtages vorlegen.
Eine Fraktion des Landtages oder zehn seiner Mitglieder können verlangen, dass eine Petition im Plenum des Landtages entschieden wird.

(3) Petitionen werden nur in öffentlicher Sitzung beraten, wenn die Mehrheit der Ausschussmitglieder dies verlangt und der Petent zugestimmt hat.
Sammel- und Massenpetitionen werden in öffentlicher Sitzung beraten, wenn ein Drittel der Ausschussmitglieder dies verlangt.

(4) Für den Petitionsausschuss gilt die Geschäftsordnung des Landtages, sofern nicht durch dieses Gesetz Abweichendes bestimmt ist.

(5) Der Ausschuss bestellt grundsätzlich für jede Petition einen Berichterstatter.
Jedes Ausschussmitglied kann verlangen, dass auch ein Mitberichterstatter bestellt und tätig wird.

(6) Der Petitionsausschuss kann vor seiner Entscheidung die Stellungnahme eines Fachausschusses des Landtages oder eines besonders fachkundigen, dem Ausschuss nicht angehörenden Mitgliedes des Landtages einholen.

(7) Für Petitionen in Verfassungsschutzangelegenheiten findet § 25 Absatz 4 des Brandenburgischen Verfassungsschutzgesetzes Anwendung.

#### § 5  
Aufklärung des Sachverhalts

(1) Zur Aufklärung des Sachverhalts und zur Vorbereitung seiner Entscheidung kann der Petitionsausschuss oder ein von ihm beauftragtes Mitglied des Landtages den Petenten, andere Beteiligte und Sachverständige anhören.
Auf Antrag eines Ausschussmitgliedes hat der Petitionsausschuss die Pflicht, den Petenten oder einen Vertreter im Ausschuss zu hören.

(2) Ferner kann der Petitionsausschuss oder ein von ihm beauftragtes Mitglied des Landtages

1.  vom Ministerpräsidenten,
2.  von der Landesregierung

     und unmittelbar

3.  von allen Mitgliedern der Landesregierung,
4.  von allen der Landesregierung oder einem ihrer Mitglieder unterstellten, ihrer Aufsicht oder ihren Weisungen unterliegenden Behörden, Verwaltungsstellen und Landesbetrieben,
5.  von allen Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts des Landes Brandenburg in dem Umfang, wie diese gegenüber einem dem Landtag Verantwortlichen der Aufsicht unterworfen sind,

      verlangen:

> 1.  mündliche oder schriftliche Auskünfte und Berichte,
> 2.  Vorlage von Akten und amtlichen Unterlagen und
> 3.  Gestattung der Ortsbesichtigung.

Dies gilt nicht, soweit die Mitglieder der Landesregierung durch Gesetze, Bestimmungen oder Weisungen anderer Institutionen gebunden sind.

(3) Der Ausschuss oder einzelne von ihm beauftragte Mitglieder können Haftanstalten sowie psychiatrische Einrichtungen im Land Brandenburg jederzeit und ohne vorherige Anmeldung besuchen.
Dabei muss Gelegenheit sein, mit jedem darin inhaftierten oder untergebrachten Menschen jederzeit und ohne Gegenwart anderer sprechen und alle Räumlichkeiten besichtigen zu können.

(4) Einem Ersuchen nach Absatz 2 oder Absatz 3 muss nur dann nicht entsprochen werden, wenn überwiegende öffentliche oder private Interessen an der Geheimhaltung dies zwingend erfordern.

(5) Besteht der Ausschuss trotz Einwendungen des Adressaten auf seinem Ersuchen, so ist ein Beschluss der Landesregierung herbeizuführen.

#### § 6  
Behandlung personenbezogener Daten, Akteneinsicht und Erteilung von Auskünften

(1) Daten zur Person des Petenten und zum Gegenstand der Petition dürfen verarbeitet werden.
Der Petent ist hierüber zu unterrichten.

(2) Der Petitionsausschuss ist im Rahmen der Wahrnehmung seiner Rechte befugt, personenbezogene Daten an die Landesregierung und andere öffentliche Stellen zu übermitteln.

(3) Es besteht kein Anspruch auf Einsicht in oder Auskunft aus Petitionsakten.
Dies gilt nicht für Ansprüche auf datenschutzrechtlicher Grundlage.

(4) Im Übrigen gilt die Datenschutzordnung des Landtages.

#### § 7  
Entscheidungen

(1) Über Petitionen kann in folgender Weise entschieden werden:

1.  Die Petition wird der Landesregierung
    1.  zur Kenntnisnahme,
    2.  zur Überprüfung oder
    3.  mit der Empfehlung, bestimmte näher bezeichnete Maßnahmen zu veranlassen,

           überwiesen.

2.  Dem Petenten wird anheimgegeben, zunächst den Rechtsweg auszuschöpfen.
3.  Die Petition wird für erledigt erklärt.
4.  Eine Petition wird, ohne auf die Sache einzugehen, zurückgewiesen oder an eine andere zuständige Stelle weitergegeben.
5.  Die Petition wird nach Beratung im Ausschuss für ungeeignet zur weiteren Behandlung erklärt.

(2) Der Petent wird in der Regel über die Art der Erledigung unterrichtet, und zwar mit Ausnahme der Fälle des § 4 Absatz 2 durch einen Bescheid des Petitionsausschusses.
Solche Bescheide bedürfen keiner Begründung.
Sie sollen jedoch den Petenten über den Sinn einer Entscheidung aufklären.
In geeigneten Fällen kann auch die Landesregierung aufgefordert werden, dem Petenten über die Sach- und Rechtslage erschöpfende Auskunft zu erteilen.

(3) Über die Art der Behandlung einer Massenpetition kann auf Beschluss des Ausschusses durch Veröffentlichungen in der Presse, auf der Internetseite des Landtages oder in anderer geeigneter Weise unterrichtet werden.
Über die Art der Behandlung einer Sammelpetition wird der Initiator unterrichtet.

(4) Wird der Landesregierung eine Petition zur Überprüfung oder mit einer Empfehlung überwiesen, so ist diese verpflichtet, darüber zu berichten, was sie aufgrund der überwiesenen Petition veranlasst hat.
Der Bericht ist innerhalb einer Frist von drei Wochen zu erstatten, sofern nicht Fristverlängerung bewilligt wurde.

(5) Der Ausschuss kann seine Vorgänge der für die Einleitung eines Straf- oder Disziplinarverfahrens zuständigen Stelle zuleiten.

#### § 8  
Entscheidungen in Gesetzgebungsangelegenheiten

Petitionen in Gesetzgebungsangelegenheiten werden grundsätzlich in derselben Weise behandelt wie andere Petitionen.
Falls ein Fachausschuss bereits mit der betreffenden Gesetzgebungsmaterie befasst ist, kann ihm die Petition zu dem Zweck zugeleitet werden, sie bei seiner Arbeit mitzuberaten.

#### § 9   
Entscheidungen bei bestandskräftigen Verwaltungsentscheidungen

Eine Behandlung der Petition ist grundsätzlich auch dann möglich, wenn bereits eine bestandskräftige Verwaltungsentscheidung vorliegt.
Handelt es sich um eine Entscheidung der Verwaltung, bei der eine nochmalige Überprüfung oder Abänderung zugunsten des Betroffenen möglich ist, so ist der Petitionsausschuss berechtigt, der Landesregierung eine erneute Prüfung oder Abänderung der Verwaltungsentscheidung zu empfehlen.

#### § 10   
Verhältnis zu den Gerichten

(1) Der Petitionsausschuss kann von den Gerichten mündliche und schriftliche Auskünfte und die Vorlage von Akten verlangen.
In Angelegenheiten der Rechtsprechung besteht nur Anspruch auf Auskunftshilfe.
Der Ausschuss hat ferner die Befugnis, Art und Umfang der Dienstaufsicht über die Gerichte zu kontrollieren.

(2) Es ist dem Petitionsausschuss versagt, in schwebende Gerichtsverfahren einzugreifen.
Unberührt hiervon bleibt das Recht des Petitionsausschusses, in einem Verfahren, in dem das Land Brandenburg oder eine Behörde, Körperschaft, Anstalt oder Stiftung des Landes Brandenburg Partei ist, der Landesregierung oder über die Landesregierung dieser Behörde beziehungsweise juristischen Person im Rahmen des Aufsichtsrechts der Landesregierung zu empfehlen, sich als Partei in dem Verfahren in bestimmter Weise zu verhalten.

(3) Nach Abschluss eines Verfahrens durch rechtskräftiges Urteil, das eine Maßnahme der Verwaltung für rechtmäßig erklärt hat, bleibt es dem Petitionsausschuss unbenommen, in besonders gelagerten Fällen die Zweckmäßigkeit der Maßnahme zu überprüfen und der Landesregierung eine Abänderung der Verwaltungsentscheidung zu empfehlen.
Eine Grenze findet dieses Recht des Petitionsausschusses dort, wo Rechtsvorschriften der Verwaltung das in der Petition angegriffene Verfahren zwingend vorschreiben oder ihr eine nachträgliche Änderung ihrer Entscheidung verbieten.

#### § 11  
Überweisung von Petitionen

Der Petitionsausschuss kann solche Petitionen, die in den Zuständigkeitsbereich des Europaparlaments, des Deutschen Bundestages oder eines anderen Landesparlaments fallen, an diese verweisen, wenn das Einverständnis des Petenten vorausgesetzt werden kann.
Der Petent ist hierüber zu unterrichten.

#### § 12   
Bericht über die Arbeit des Petitionsausschusses

(1) Der Landtag nimmt in der Regel jährlich einen Bericht über die Arbeit des Petitionsausschusses entgegen.
Die Berichte des Ausschusses können mündlich oder in Schriftform erstattet werden.

(2) In der Regel vierteljährlich sind die Beschlüsse des Ausschusses in einer Übersicht dem Landtag zur Kenntnis zu geben.

(3) Der Petitionsausschuss kann beschließen, auf der Internetseite des Petitionsausschusses Petitionen von allgemeiner oder beispielhafter Bedeutung zu veröffentlichen sowie über den Bearbeitungsstand und das Ergebnis zu informieren.
Personenbezogene Daten sind hierbei zu anonymisieren.

#### § 13  
Nicht erledigte Petitionen

Petitionen, die am Ende einer Legislaturperiode noch nicht abschließend behandelt worden sind, gelten auch innerhalb der darauffolgenden Wahlperiode als eingegangen, ohne dass es einer erneuten Eingabe des Petenten bedarf.

#### § 14   
Inkrafttreten, Außerkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt das Petitionsgesetz vom 13. Dezember 1991 (GVBl. S. 643) außer Kraft.

Potsdam, den 20.
Dezember 2010

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch