## Gesetz zur Ausführung des Zwölften Buches Sozialgesetzbuch  (AG-SGB XII)

Abschnitt 1  
Ziel des Gesetzes, Träger der Sozialhilfe und ihre Aufgaben
-------------------------------------------------------------------------

#### § 1  
Ziel des Gesetzes

Ziel dieses Gesetzes ist

1.  die Gewährleistung einer wirtschaftlichen personenzentrierten Hilfe unabhängig von bestehenden Leistungsformen,
    
2.  die Sicherstellung einer einheitlichen Rechtsanwendung,
    
3.  die Förderung der Selbstbestimmung der Leistungsberechtigten bei der Auswahl geeigneter Leistungsangebote.
    

#### § 2  
Träger der Sozialhilfe

(1) Örtliche Träger der Sozialhilfe sind die Landkreise und kreisfreien Städte.

(2) Überörtlicher Träger der Sozialhilfe ist das Land.
Die Aufgaben des überörtlichen Trägers der Sozialhilfe werden vom Landesamt für Soziales und Versorgung wahrgenommen.

#### § 3  
Gemeinsame Verantwortung und Zusammenarbeit der Träger der Sozialhilfe

(1) Die nach diesem Gesetz zuständigen Träger der Sozialhilfe tragen die gemeinsame Verantwortung für die Leistungsgewährung nach § 97 Absatz 3 des Zwölften Buches Sozialgesetzbuch sowie die damit einhergehende Ausgabenentwicklung.
Hierzu arbeiten die Träger der Sozialhilfe bei der Wahrnehmung der Aufgaben nach diesem Gesetz eng und vertrauensvoll zusammen und unterstützen sich gegenseitig.
Die Zusammenarbeit beinhaltet insbesondere eine Abstimmung, Koordinierung und Vernetzung der jeweils in eigener Zuständigkeit wahrzunehmenden Aufgaben.
Die örtlichen Träger der Sozialhilfe und der überörtliche Träger der Sozialhilfe können zu diesem Zweck eine einheitliche Vereinbarung abschließen.

(2) Die zuständigen Träger der Sozialhilfe wirken im Rahmen ihrer Aufgaben nach diesem Gesetz auf eine sozialräumliche Entwicklung hin.
Dies geschieht

1.  unter Berücksichtigung der Stärkung des Ehrenamtes, der sozialen Aufmerksamkeit und der Transparenz der vorhandenen Hilfsangebote sowie durch die Einbindung von Einrichtungen in die Gemeinde und
    
2.  durch ein abgestimmtes und vernetztes Versorgungssystem einschließlich einer unabhängigen wohnortnahen Beratung und Betreuung, insbesondere zu Maßnahmen und Hilfen, die einen Verbleib in der eigenen Häuslichkeit sichern sowie der Förderung individueller Wohn- und Betreuungsformen.
    

Sie arbeiten dabei eng mit den Ämtern und amtsfreien Gemeinden zusammen, deren Zuständigkeit unberührt bleibt.

#### § 4  
Sachliche Zuständigkeit der örtlichen Träger der Sozialhilfe

(1) Die örtlichen Träger der Sozialhilfe sind sachlich zuständig für die in § 97 Absatz 3 des Zwölften Buches Sozialgesetzbuch genannten Leistungen der

1.  Hilfe zur Pflege (§§ 61 bis 66 des Zwölften Buches Sozialgesetzbuch),
    
2.  Hilfe zur Überwindung besonderer sozialer Schwierigkeiten (§§ 67 bis 69 des Zwölften Buches Sozialgesetzbuch) und
    
3.  Blindenhilfe (§ 72 des Zwölften Buches Sozialgesetzbuch),
    

soweit sich nicht aus § 5 etwas anderes ergibt.
Sie nehmen diese Aufgaben als pflichtige Selbstverwaltungsangelegenheiten wahr.

(1a) Die örtlichen Träger der Sozialhilfe sind sachlich zuständig für die im Dritten Abschnitt des Dritten Kapitels des Zwölften Buches Sozialgesetzbuch genannten Bedarfe für Bildung und Teilhabe.
Sie nehmen diese Aufgabe als pflichtige Selbstverwaltungsangelegenheit wahr.

(2) Die örtlichen Träger der Sozialhilfe sind ferner zuständig als Träger für die Ausführung der Aufgaben der Grundsicherung im Alter und bei Erwerbsminderung nach dem Vierten Kapitel des Zwölften Buches Sozialgesetzbuch.
Sie nehmen diese Aufgaben als pflichtige Selbstverwaltungsangelegenheit wahr, soweit sie nicht der Bundesauftragsverwaltung aufgrund § 46a Absatz 1 des Zwölften Buches Sozialgesetzbuch unterliegen.
Im Bereich der Bundesauftragsverwaltung nehmen sie die Aufgaben als Auftragsangelegenheit wahr.
§ 6 des Zwölften Buches Sozialgesetzbuch gilt entsprechend.

(3) Die Rechtsaufsicht über die örtlichen Träger der Sozialhilfe übt das für Soziales zuständige Ministerium aus.
Hinsichtlich der nach Absatz 2 in Bundesauftragsverwaltung wahrgenommenen Aufgaben übt das für Soziales zuständige Ministerium die Fachaufsicht aus.
Soweit die Träger die Aufgaben nach dem Vierten Kapitel des Zwölften Buches Sozialgesetzbuch in Bundesauftragsverwaltung durchführen, kann die aufsichtführende Behörde den Trägern Weisungen erteilen, um die gesetzmäßige und zweckmäßige Erfüllung der Aufgaben zu sichern.
Das Weisungsrecht ist unbeschränkt.

(4) Weitere Zuständigkeiten aufgrund des Zwölften Buches Sozialgesetzbuch und anderer Rechtsvorschriften bleiben unberührt.

#### § 4a  
Örtliche Zuständigkeit für die Grundsicherung im Alter und bei Erwerbsminderung

Soweit § 46b des Zwölften Buches Sozialgesetzbuch nichts Abweichendes regelt, ist für die Leistungen des Vierten Kapitels des Zwölften Buches Sozialgesetzbuch der Träger der Sozialhilfe örtlich zuständig, in dessen Bereich der gewöhnliche Aufenthaltsort der leistungsberechtigten Person liegt.

#### § 5  
Sachliche Zuständigkeit des überörtlichen Trägers der Sozialhilfe

(1) Der überörtliche Träger der Sozialhilfe ist zuständig für

1.  die Unterstützung der örtlichen Träger der Sozialhilfe bei den in § 4 Absatz 1 und Absatz 1a benannten Aufgaben,
    
2.  die Förderung der zielgerichteten Erbringung und Überprüfung der in § 4 Absatz 1 und Absatz 1a benannten Leistungen und deren Qualitätssicherung,
    
3.  den Erlass von Rahmenrichtlinien und Empfehlungen zur Ausführung des Leistungsrechts,
    
4.  die Erfassung und Auswertung der Ausgaben in den Bereichen des § 97 Absatz 3 des Zwölften Buches Sozialgesetzbuch aufgrund der nach den §§ 11 und 14 vorliegenden Daten,
    
5.  die Gewährung von Sozialhilfe für Deutsche im Ausland nach den §§ 24, 132 und 133 des Zwölften Buches Sozialgesetzbuch und
    
6.  die Kostenerstattung nach § 106 Absatz 1 Satz 2 und § 108 des Zwölften Buches Sozialgesetzbuch.
    

Weitere Zuständigkeiten aufgrund anderer Rechtsvorschriften bleiben unberührt.

(2) Zur Sicherung landeseinheitlicher Regelungen und Versorgungsstrukturen ist der überörtliche Träger der Sozialhilfe ferner zuständig für

1.  den Abschluss von Leistungs-, Prüfungs- und Vergütungsvereinbarungen nach dem Zehnten Kapitel des Zwölften Buches Sozialgesetzbuch,
    
2.  die Mitwirkung bei Abschluss und Kündigung von Versorgungsverträgen nach § 72 Absatz 2 Satz 1 des Elften Buches Sozialgesetzbuch,
    
3.  den Abschluss von Pflegesatzvereinbarungen nach § 85 Absatz 2 Satz 1 Nummer 2 oder § 86 Absatz 1 Satz 1 des Elften Buches Sozialgesetzbuch und
    
4.  den Abschluss von Vereinbarungen zur Übernahme gesondert berechneter Investitionskosten nach § 75 Absatz 5 Satz 3 des Zwölften Buches Sozialgesetzbuch.
    

Der überörtliche Träger der Sozialhilfe stellt nach Satz 1 Nummer 1 bis 4 Einvernehmen mit dem jeweils für den Sitz der Einrichtung zuständigen örtlichen Träger der Sozialhilfe her.

(3) Das für Soziales zuständige Mitglied der Landesregierung kann die Zuständigkeiten nach Absatz 2 durch Rechtsverordnung im Einvernehmen mit dem für die Kommunalaufsicht zuständigen Mitglied der Landesregierung auf die örtlichen Träger der Sozialhilfe übertragen, wenn alle örtlichen Träger der Sozialhilfe sicherstellen, dass

1.  die Vorbereitung des Abschlusses der in Absatz 2 genannten Vereinbarungen und Versorgungsverträge und
    
2.  die Vorbereitung der Durchführung von Qualitäts- und Wirtschaftlichkeitsprüfungen nach dem Zehnten Kapitel des Zwölften Buches Sozialgesetzbuch
    

für die in § 4 Absatz 1 benannten Leistungen gemeinsam und zentral wahrgenommen werden.
Zu diesem Zweck schließen die örtlichen Träger der Sozialhilfe eine öffentlich-rechtliche Vereinbarung in entsprechender Anwendung des Gesetzes über kommunale Gemeinschaftsarbeit im Land Brandenburg ab, die der Genehmigung der in § 4 Absatz 3 Satz 1 bestimmten Aufsichtsbehörde bedarf.
Die örtlichen Träger der Sozialhilfe nehmen diese Aufgaben als Pflichtaufgaben zur Erfüllung nach Weisung wahr.
Die zuständige Sonderaufsichtsbehörde ist das für Soziales zuständige Ministerium.
In der Rechtsverordnung nach Satz 1 kann auch bestimmt werden, dass die Mitgliedschaft in der Schiedsstelle nach § 76 Absatz 2 Satz 2 des Elften Buches Sozialgesetzbuch durch einen örtlichen Träger der Sozialhilfe wahrgenommen wird.

(4) Die örtlichen Träger der Sozialhilfe nehmen die Aufgaben nach Absatz 3 unter Mitwirkung des überörtlichen Trägers der Sozialhilfe als Pflichtaufgaben zur Erfüllung nach Weisung wahr.
Der überörtliche Träger der Sozialhilfe ist bei der Vorbereitung der Vertragsverhandlungen und bei den Qualitäts- und Wirtschaftlichkeitsprüfungen zu beteiligen.
Bei Verträgen und Vereinbarungen, die überregionale oder grundsätzliche Bedeutung haben, steht dem überörtlichen Träger der Sozialhilfe ein Widerspruchsrecht zu.
Die zuständige Sonderaufsichtsbehörde gemäß § 121 der Kommunalverfassung des Landes Brandenburg ist das für Soziales zuständige Ministerium.
In der Rechtsverordnung nach Absatz 4 Satz 1 kann auch bestimmt werden, dass die Mitgliedschaft in der Schiedsstelle nach § 76 Absatz 2 Satz 2 des Elften Buches Sozialgesetzbuch durch einen örtlichen Träger der Sozialhilfe wahrgenommen wird.

(5) Die Schiedsstelle nach § 81 des Zwölften Buches Sozialgesetzbuch wird beim Landesamt für Soziales und Versorgung gebildet.

#### § 6  
Heranziehung von Ämtern und amtsfreien Gemeinden durch die Landkreise

(1) Die Landkreise können durch Satzung bestimmen, dass Ämter und amtsfreie Gemeinden Aufgaben durchführen, die den Landkreisen als örtliche Träger der Sozialhilfe obliegen, wenn die ordnungsgemäße Erfüllung der Aufgaben gewährleistet ist.
Dabei können die Ämter und amtsfreien Gemeinden im eigenen Namen entscheiden.

(2) Die Landkreise können Ämter und amtsfreie Gemeinden für Einzelfälle beauftragen, Aufgaben, die den Landkreisen als örtliche Träger der Sozialhilfe obliegen, durchzuführen und dabei im Namen des Landkreises zu entscheiden.

(3) Werden nach Absatz 1 oder Absatz 2 Aufgaben von Ämtern und amtsfreien Gemeinden durchgeführt, hat der Landkreis die aufgewendeten Kosten zu erstatten.
Die Erstattung von Personal- und Sachkosten erfolgt durch pauschale Abgeltung und ist in der nach Absatz 1 zu erlassenden Satzung beziehungsweise auf Grundlage der nach Absatz 2 zu schließenden öffentlich-rechtlichen Vereinbarung zu regeln.

#### § 7  
Entgegennahme und Weiterleitung von Anträgen, vorläufige Hilfeleistungen

(1) Wird ein Antrag auf Sozialhilfe bei einer kreisangehörigen Gemeinde gestellt, in welcher sich der Hilfesuchende tatsächlich aufhält, so hat die Gemeinde, soweit sie nicht selbst nach § 6 die Aufgaben durchführt, den zuständigen örtlichen Träger der Sozialhilfe unverzüglich über die Geltendmachung zu unterrichten und die Unterlagen an diesen weiterzuleiten.
Wird ein Antrag bei einem Amt gestellt, das nicht selbst die Aufgaben durchführt, findet Satz 1 entsprechende Anwendung.

(2) Die Ämter und amtsfreien Gemeinden haben vorläufig die unerlässlich notwendigen Maßnahmen zu treffen, wenn die Gewährung der Hilfe nicht bis zur Entscheidung des Trägers der Sozialhilfe aufgeschoben werden kann.
Der zuständige Träger der Sozialhilfe ist unverzüglich über die getroffenen Maßnahmen zu unterrichten.
Für die Kostenerstattung durch den zuständigen Träger gilt § 6 Absatz 3 entsprechend.

Abschnitt 2  
Gemeinsame Steuerung
----------------------------------

#### § 8  
Brandenburger Kommission

(1) Die nach § 12 des Gesetzes zur Ausführung des Neunten Buches Sozialgesetzbuch eingerichtete Brandenburger Kommission ist zuständig für die Vorbereitung der Änderung, Ergänzung und Fortentwicklung der Rahmenverträge nach § 80 des Zwölften Buches Sozialgesetzbuch in Verbindung mit § 75 Absatz 1 Satz 2 des Zwölften Buches Sozialgesetzbuch.

(2) Sie ist außerdem zuständig für landesweite Rahmenvereinbarungen für Einrichtungen und Dienste von

1.  Hilfen nach § 97 Absatz 3 Nummer 3 des Zwölften Buches Sozialgesetzbuch, Leistungsarten und den dazugehörigen Rahmenleistungsvereinbarungen, differenziert nach Zielgruppe, Leistungsinhalten und Wirkungskontrolle,
2.  Kalkulationsgrundlagen zur Ermittlung der Vergütungen, insbesondere zur Personalbemessung nach Leistungstypen gemäß Nummer 1,
3.  Pauschalen für einzelne Vergütungsbestandteile nach § 76 Absatz 3 Satz 1 Nummer 2 des Zwölften Buches Sozialgesetzbuch,
4.  pauschalen Fortschreibungsraten auf Personal- und Sachkosten einzelner Vergütungsbestandteile nach § 76 Absatz 3 Satz 1 Nummer 2 des Zwölften Buches Sozialgesetzbuch und
5.  Grundsätzen zur Weiterentwicklung der Leistungen und zur Berücksichtigung von Qualitätsstandards bei der Leistungserbringung nach dem Zwölften Buch Sozialgesetzbuch.

(3) § 12 Absatz 3 bis 6 des Gesetzes zur Ausführung des Neunten Buches Sozialgesetzbuch ist sinngemäß anzuwenden.

#### § 9  
Brandenburger Steuerungskreis

(1) Der nach § 11 des Gesetzes zur Ausführung des Neunten Buches Sozialgesetzbuch eingerichtete Brandenburger Steuerungskreis ist für die Abstimmung und Koordinierung der nach diesem Gesetz wahrzunehmenden Aufgaben zuständig.

(2) Nach diesem Gesetz hat der Brandenburger Steuerungskreis insbesondere folgende Aufgaben:

1.  Informationsaustausch und Erarbeitung gemeinsamer Positionen zu Themen der Einzelfallbearbeitung und des Vertragswesens,
2.  Erarbeitung gemeinsamer Grundlagen für die Vorhaltung von bedarfsdeckenden Angeboten zur Hilfeleistung und zur Angebotssteuerung,
3.  Positionierung der Leistungsträger zu Themen der Brandenburger Kommission und deren Arbeitsgruppen,
4.  Erarbeitung von Empfehlungen zur Ausgestaltung von bedarfsorientierten, insbesondere ambulanten Angeboten,
5.  Definition und Bewertung von Kenn- und Zielzahlen für ein landesweites Berichtswesen und einen landesweiten Kennzahlenvergleich,
6.  Vereinbarung von Steuerungszielen und -maßnahmen auf Landesebene sowie
7.  Erarbeitung eines Systems der Wirkungskontrolle der Leistungen nach § 97 Absatz 3 des Zwölften Buches Sozialgesetzbuch.

(3) § 11 Absatz 3 bis 5 des Gesetzes zur Ausführung des Neunten Buches Sozialgesetzbuch ist sinngemäß anzuwenden.

Abschnitt 3  
Deckung des Finanzbedarfs der örtlichen Träger der Sozialhilfe
----------------------------------------------------------------------------

#### § 10  
Kostenträger und Kostenerstattung

(1) Die Träger der Sozialhilfe tragen die Kosten für die Aufgaben, die ihnen nach dem Zwölften Buch Sozialgesetzbuch oder nach diesem Gesetz obliegen.

(2) Zum Ausgleich der Kosten, die den örtlichen Trägern der Sozialhilfe für die Übertragung der sachlichen Zuständigkeit nach § 4 Absatz 1 Satz 1 Nummer 1 bis 3 entstehen, erstattet das Land die notwendigen Gesamtnettoaufwendungen nach Maßgabe der Absätze 3 bis 5.

(2a) Zum Ausgleich der Kosten, die den örtlichen Trägern der Sozialhilfe für die Übertragung der sachlichen Zuständigkeit nach § 4 Absatz 1a entstehen, erstattet das Land die notwendigen Gesamtnettoaufwendungen für die zu erbringenden Leistungen für Bildung und Teilhabe nach § 34 Absatz 1, Absatz 2 Satz 1 Nummer 1 und Satz 2 sowie Absatz 4 bis 7 des Zwölften Buches Sozialgesetzbuch nach Maßgabe des § 34 Absatz 4 des Zwölften Buches Sozialgesetzbuch.

(2b) Das Land erstattet die notwendigen Gesamtnettoaufwendungen für die Sachleistungen für Bildung und Teilhabe nach § 42 Nummer 3 des Zwölften Buches Sozialgesetzbuch in Verbindung mit § 34 Absatz 2, 5 und 6 des Zwölften Buches Sozialgesetzbuch nach Maßgabe des § 34 Absatz 4 des Zwölften Buches Sozialgesetzbuch.

(3) Zu den berücksichtigungsfähigen Aufwendungen nach Absatz 2 Satz 1 können auch Aufwendungen gehören, die eine Leistungsgewährung nach § 4 Absatz 1 ergänzen oder ersetzen sowie Aufwendungen für Modellvorhaben zur Weiterentwicklung von Leistungen der Sozialhilfe nach § 97 Absatz 5 des Zwölften Buches Sozialgesetzbuch, sofern die Leistungen geeignet sind, die Sozialhilfeausgaben zu senken.
Die den Aufwendungen nach Satz 1 zugrunde liegenden Maßnahmen bedürfen der vorherigen Zustimmung durch den überörtlichen Träger der Sozialhilfe.

(4) Die Nettoaufwendungen werden durch Abzug der Einnahmen von den Ausgaben ermittelt.

(5) Die Finanzierungsquote des Landes beträgt 85 Prozent und die Finanzierungsquote der örtlichen Träger der Sozialhilfe 15 Prozent.

(6) Für die Durchführung der Kostenerstattung ist das Landesamt für Soziales und Versorgung zuständig.

#### § 11  
Abrechnungsverfahren und Abschläge

(1) Die Kosten werden auf Antrag erstattet.
Die örtlichen Träger der Sozialhilfe haben die für die Kostenerstattung nach § 10 Absatz 2 und 3 maßgeblichen Aufwendungen durch einen nach Einnahme- und Ausgabearten gegliederten Nachweis entsprechend dem vom Landesamt für Soziales und Versorgung vorgegebenen Muster nachzuweisen.
Der Nachweis für das erste Halbjahr des laufenden Jahres ist spätestens bis zum 30.
September des laufenden Jahres und der Nachweis für das gesamte Jahr spätestens bis zum 30.
April des Folgejahres vorzulegen.
Das Landesamt für Soziales und Versorgung kann zur Feststellung der Höhe der Kostenerstattungsansprüche ergänzend anspruchsbegründende Unterlagen anfordern, Prüfungen bei den örtlichen Trägern der Sozialhilfe durchführen und die Unterlagen vor Ort einsehen.

(2) Das Land gewährt jedem örtlichen Träger der Sozialhilfe monatliche Kostenerstattungsabschläge.
Die Höhe der monatlichen Abschläge beträgt ein Zwölftel der anerkannten erstattungsfähigen Aufwendungen des Vorjahres des jeweiligen örtlichen Trägers der Sozialhilfe zuzüglich eines angemessenen einheitlichen Steigerungssatzes, der sich an der Veränderung der Verbraucherpreise im Land Brandenburg gegenüber dem Vorjahr oder der vereinbarten pauschalen Entgeltfortschreibung für das laufende Jahr im Bereich der Sozialhilfe orientiert.
Bis zur Feststellung der anerkannten erstattungsfähigen Aufwendungen des Vorjahres werden die bisher gezahlten Abschläge weiter gewährt.

(3) Nach Abschluss des Verfahrens nach Absatz 1 erfolgt ein Ausgleich von Über- und Unterzahlungen mit dem Folgeabschlag.

#### § 12  
Personal- und Sachkosten

(1) Die örtlichen Träger der Sozialhilfe erhalten zum Ausgleich der aufzuwendenden Personal- und Sachkosten eine Pauschale in Höhe von 3,6 Prozent der nach § 10 ermittelten Gesamtnettoaufwendungen für die Sozialhilfe abzüglich des kommunalen Eigenanteils.
Für die Erledigung der Aufgaben nach § 4 Absatz 1a und 2, die gemäß § 10 Absatz 2a und 2b berücksichtigungsfähig sind, erhalten die örtlichen Träger der Sozialhilfe zusätzlich eine Fallpauschale von 46,43 Euro.

(2) Die Personal- und Sachkostenpauschale nach Absatz 1 Satz 1 sowie die Fallpauschale nach Absatz 1 Satz 2 werden bei der Gewährung der Abschläge nach § 11 Absatz 2 berücksichtigt.

(3) Die Auskömmlichkeit der Personal- und Sachkostenpauschale nach Absatz 1 Satz 1 wird im Rahmen der Evaluierung nach § 15 überprüft und die Pauschale rückwirkend angepasst.

(4) Soweit die Personal- und Sachkostenpauschale nach Absatz 1 Satz 1 nicht auskömmlich war, haben die örtlichen Träger der Eingliederungshilfe Anspruch auf rückwirkenden Ausgleich durch das Land.
Soweit die Personal- und Sachkostenpauschale nach Absatz 1 Satz 1 zu hoch war, werden die jeweiligen Überzahlungen des Landes im Rahmen der nächsten Abschlagszahlung verrechnet.

#### § 13  
Verteilung der Bundeserstattung für die Leistungen der Grundsicherung im Alter und bei Erwerbsminderung; Erstattungs- und Nachweisverfahren

(1) Die Erstattungsbeträge des Bundes nach § 46a Absatz 1 des Zwölften Buches Sozialgesetzbuch werden nach Maßgabe des § 46a Absatz 2 bis 5 des Zwölften Buches Sozialgesetzbuch unverzüglich an die Landkreise und kreisfreien Städte weitergeleitet.
Die Höhe des Erstattungsbetrages des jeweiligen örtlichen Sozialhilfeträgers ergibt sich aus den nachgewiesenen tatsächlichen Nettoausgaben gemäß § 46a Absatz 2 des Zwölften Buches Sozialgesetzbuch.
Eine Verteilung und Weiterleitung ist auf die Höhe der Bundeserstattung beschränkt.

(2) Die örtlichen Träger der Sozialhilfe haben der nach Absatz 5 zuständigen Landesbehörde

1.  die auf Grundlage von Leistungsbescheiden entstandenen Ausgaben und Einnahmen für die Grundsicherung im Alter und bei Erwerbsminderung nach Maßgabe des § 46a Absatz 2 des Zwölften Buches Sozialgesetzbuch jeweils bis zum 30.
    April, 31.
    Juli, 31.
    Oktober und 31. Januar für das jeweils abgelaufene Quartal mitzuteilen,
2.  die nach § 46a Absatz 4 des Zwölften Buches Sozialgesetzbuch erforderlichen Nachweise in tabellarischer Form jeweils bis zum 30.
    April, 31.
    Juli, 31.
    Oktober und 31.
    Januar, für das jeweils abgeschlossene Quartal mitzuteilen und
3.  die Nettoausgaben eines Jahres im Sinne des § 46a Absatz 5 des Zwölften Buches Sozialgesetzbuch in tabellarischer Form jeweils bis zum 20.
    März des Folgejahres zu belegen.

Werden Leistungen für Leistungszeiträume im folgenden Haushaltsjahr bereits im laufenden Haushaltsjahr zur fristgerechten Auszahlung erbracht, sind die entsprechenden Nettoausgaben in die Mitteilung nach Satz 1 Nummer 1 zum 30.
April aufzunehmen.
Nettoausgaben aus Vorjahren, für die bereits ein Jahresnachweis vorliegt, sind in die Mitteilung nach Satz 1 Nummer 1 zum 31. Juli aufzunehmen.

(3) Die örtlichen Träger der Sozialhilfe haben zu gewährleisten, dass die Nettoausgaben für Geldleistungen begründet und belegt sind sowie den Grundsätzen der Wirtschaftlichkeit und Sparsamkeit entsprechen.
Sie bestätigen dieses zusammen mit dem Nachweis ihrer Ausgaben nach Absatz 2 Satz 1 Nummer 2 und 3.

(4) Die Träger haften im Verhältnis zum Land für eine ordnungsmäßige Verwaltung im Sinne des Artikels 104a Absatz 5 Satz 1 zweiter Halbsatz des Grundgesetzes.
Weitergehende öffentlich-rechtliche Erstattungsansprüche des Landes gegenüber den Trägern bleiben unberührt.

(5) Zuständige Landesbehörde für die Durchführung des Erstattungs- und Nachweisverfahrens gemäß § 46a des Zwölften Buches Sozialgesetzbuch im Verhältnis zu den örtlichen Sozialhilfeträgern ist das Landesamt für Soziales und Versorgung.

#### § 14  
Berichts- und Auskunftspflichten

Die örtlichen Träger der Sozialhilfe haben dem für Soziales zuständigen Ministerium auf Nachfrage Daten zu den nach dem Zwölften Buch Sozialgesetzbuch erbrachten Leistungen zur Verfügung zu stellen.
Das für Soziales zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung das Nähere zur Übermittlung der Daten festzulegen.

#### § 15  
Evaluierung

Das für Soziales zuständige Ministerium gibt im Benehmen mit dem Brandenburger Steuerungskreis im Kalenderjahr 2022 ein Gutachten in Auftrag, welches die tatsächliche Leistungsentwicklung einschließlich der sich aus dieser ergebenden Auswirkungen auf die Ausgaben der Träger der Sozialhilfe wissenschaftlich evaluiert.
Zu untersuchen sind die Ausgabenentwicklung für die Aufgabenwahrnehmung nach § 97 Absatz 3 des Zwölften Buches Sozialgesetzbuch und die Kostenbestandteile sowie die Wirkungen der sozialhilfeergänzenden und -ersetzenden Leistungen.
Gegenstand der Evaluierung ist auch die Auskömmlichkeit der Kostenausgleichsregelungen.

Abschnitt 4  
Sonstige Zuständigkeiten und Verfahren
----------------------------------------------------

#### § 16  
Sonstige Zuständigkeiten

(1) Die Verordnungsermächtigung der Landesregierung zur Festsetzung der Regelsätze nach § 29 Absatz 2 des Zwölften Buches Sozialgesetzbuch wird auf das für Soziales zuständige Mitglied der Landesregierung übertragen.

(2) Das für Soziales zuständige Mitglied der Landesregierung kann im Einvernehmen mit dem für Inneres und dem für Finanzen zuständigen Mitglied der Landesregierung durch Rechtsverordnung bestimmen, dass für bestimmte Arten der Hilfen nach dem Fünften bis Neunten Kapitel des Zwölften Buches Sozialgesetzbuch bei der Einkommensgrenze ein höherer Grundbetrag nach § 86 des Zwölften Buches Sozialgesetzbuch zugrunde gelegt wird.

(3) Das für Soziales zuständige Ministerium ist zuständige Landesbehörde im Sinne des § 5 Absatz 1 der Verordnung zur Durchführung des § 82 des Zwölften Buches Sozialgesetzbuch.

(4) Zuständige Verwaltungsbehörde für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 117 Absatz 6 des Zwölften Buches Sozialgesetzbuch sind die Behörden, denen gegenüber die Pflicht zur Auskunft besteht.

#### § 17  
Beteiligung sozial erfahrener Dritter im Widerspruchsverfahren

Die Träger der Sozialhilfe können jeweils allgemein für ihren sachlichen Zuständigkeitsbereich bestimmen, dass vor dem Erlass eines Verwaltungsaktes über den Widerspruch gegen die Ablehnung der Sozialhilfe oder gegen die Festsetzung ihrer Art und Höhe sozial erfahrene Dritte gemäß § 116 Absatz 2 des Zwölften Buches Sozialgesetzbuch beratend beteiligt werden sowie das Nähere über die Beteiligung festlegen.

#### § 18  
_(aufgehoben)_

#### § 19  
_(aufgehoben)_