## Gesetz zu dem Staatsvertrag über die Errichtung eines Gemeinsamen Kompetenz- und Dienstleistungszentrums der Polizeien der Länder Berlin, Brandenburg, Sachsen, Sachsen-Anhalt und Thüringen auf dem Gebiet der polizeilichen Telekommunikationsüberwachung als rechtsfähige Anstalt öffentlichen Rechts

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem in Leipzig am 19.
Juli 2017 vom Land Brandenburg unterzeichneten Staatsvertrag über die Errichtung eines Gemeinsamen Kompetenz- und Dienstleistungszentrums der Polizeien der Länder Berlin, Brandenburg, Sachsen, Sachsen-Anhalt und Thüringen auf dem Gebiet der polizeilichen Telekommunikationsüberwachung als rechtsfähige Anstalt öffentlichen Rechts wird zugestimmt.
Der Vertrag wird nachstehend veröffentlicht.

#### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem § 20 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 4.
Dezember 2017

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag](/vertraege/gkdz_stv)

* * *

### Anlagen

1

[Staatsvertrag über die Errichtung eines Gemeinsamen Kompetenz- und Dienstleistungszentrums ...
(GKDZ-StV)](/br2/sixcms/media.php/68/GVBl_I_29_2017-Anlage.pdf "Staatsvertrag über die Errichtung eines Gemeinsamen Kompetenz- und Dienstleistungszentrums ... (GKDZ-StV)") 806.3 KB