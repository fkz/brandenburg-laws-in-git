## Gesetz zum Staatsvertrag zur Modernisierung der Medienordnung in Deutschland

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem am 28.
April 2020 vom Land Brandenburg unterzeichneten Staatsvertrag zur Modernisierung der Medienordnung in Deutschland wird zugestimmt.
Der Vertrag wird nachstehend veröffentlicht.

#### § 2 

Durch dieses Gesetz werden das Grundrecht auf Datenschutz (Artikel 11 Absatz 1 der Verfassung des Landes Brandenburg) und das Grundrecht der Berufsfreiheit (Artikel 49 Absatz 1 Satz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 3 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem Artikel 9 Absatz 2 Satz 1 am Tag nach der Hinterlegung der letzten Ratifikationsurkunde in Kraft.
Sollte der Staatsvertrag nach seinem Artikel 9 Absatz 2 Satz 2 gegenstandslos werden, ist dies unverzüglich im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 25.
Juni 2020

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[1](#a) Artikel 2 des Staatsvertrages dient der Umsetzung der Richtlinie (EU) 2018/1808 des Europäischen Parlaments und des Rates vom 14.
November 2018 zur Änderung der Richtlinie 2010/13/EU des Rates zur Koordinierung bestimmter Rechts- und Verwaltungsvorschriften der Mitgliedstaaten über die Bereitstellung audiovisueller Mediendienste (Richtlinie über audiovisuelle Mediendienste) im Hinblick auf sich verändernde Marktgegebenheiten (ABl. L 303 vom 28.11.2018, S. 69).

* * *

[zum Staatsvertrag](/vertraege/mstv) - Medienstaatsvertrag

[zum Staatsvertrag](/vertraege/jmstv) - Jugendmedienschutz-Staatsvertrag

[zum Staatsvertrag](/vertraege/ard_stv) - ARD-Staatsvertrag

[zum Staatsvertrag](/vertraege/zdf_stv) - ZDF-Staatsvertrag

[zum Staatsvertrag](/vertraege/dlr_stv) - Deutschlandradio-Staatsvertrag

[zum Staatsvertrag](/vertraege/rfinstv) - Rundfunkfinanzierungsstaatsvertrag

[zum Staatsvertrag](/vertraege/rbstv) - Rundfunkbeitragsstaatsvertrag

* * *

### Anlagen

1

[Staatsvertrag zur Modernisierung der Medienordnung in Deutschland](/br2/sixcms/media.php/68/GVBl_I_19_2020-Anlage.pdf "Staatsvertrag zur Modernisierung der Medienordnung in Deutschland") 1.0 MB