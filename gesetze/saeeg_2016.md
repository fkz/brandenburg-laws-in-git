## Gesetz zur Errichtung von staatlichen Schulämtern (Schulämtererrichtungsgesetz - SÄEG)

#### § 1 Errichtung

(1) Als sonstige untere Landesbehörden nach § 8 Absatz 2 Satz 1 des Landesorganisationsgesetzes werden das

1.  Staatliche Schulamt Neuruppin mit der Zuständigkeit für die Schulen in den Landkreisen Prignitz, Ostprignitz-Ruppin, Oberhavel und Havelland,
2.  Staatliche Schulamt Brandenburg an der Havel mit der Zuständigkeit für die Schulen in den Landkreisen Potsdam-Mittelmark, Teltow-Fläming sowie in den kreisfreien Städten Potsdam und Brandenburg an der Havel,
3.  Staatliche Schulamt Frankfurt (Oder) mit der Zuständigkeit für die Schulen in den Landkreisen Uckermark, Barnim, Märkisch-Oderland, Oder-Spree und in der kreisfreien Stadt Frankfurt (Oder) und
4.  Staatliche Schulamt Cottbus mit der Zuständigkeit für die Schulen in den Landkreisen Dahme-Spreewald, Elbe-Elster, Spree-Neiße, Oberspreewald-Lausitz und in der kreisfreien Stadt Cottbus

errichtet.

(2) Das für Schule zuständige Mitglied der Landesregierung wird ermächtigt, die Anzahl, den Sitz und die Zuständigkeitsbezirke der staatlichen Schulämter durch Rechtsverordnung zu bestimmen.

#### § 2 Auflösung

Das Landesamt für Schule und Lehrerbildung (Landesschulamt) wird aufgelöst.

#### § 3 Aufgaben

(1) Die Aufgaben und Befugnisse des Landesschulamtes im Bereich Lehrerausbildung und Lehrerweiterbildung sowie im Bereich Reisekostenabrechnung für Lehrkräfte und im Bereich IT-Ressortfachverfahren und eGovernment gehen auf das für Schule zuständige Ministerium über.
Die sonstigen Aufgaben und Befugnisse des Landesschulamtes gehen auf die staatlichen Schulämter über.

(2) Eines Vorverfahrens nach den Vorschriften des 8.
Abschnitts der Verwaltungsgerichtsordnung bedarf es, wenn ein Verwaltungsakt angefochten wird, den das für Schule zuständige Ministerium im Bereich Reisekostenabrechnung für Lehrkräfte erlassen hat.

#### § 4 Personal

Die Beamtinnen und Beamten sowie die Beschäftigten des Landesschulamtes werden,

1.  soweit sie Aufgaben in der Regionalstelle Neuruppin wahrnehmen, dem Staatlichen Schulamt Neuruppin zugeordnet,
2.  soweit sie Aufgaben in der Regionalstelle Brandenburg an der Havel wahrnehmen, dem Staatlichen Schulamt Brandenburg an der Havel zugeordnet,
3.  soweit sie Aufgaben in der Regionalstelle Frankfurt (Oder) wahrnehmen, dem Staatlichen Schulamt Frankfurt (Oder) zugeordnet,
4.  soweit sie Aufgaben in der Regionalstelle Cottbus wahrnehmen, dem Staatlichen Schulamt Cottbus zugeordnet und
5.  soweit sie am Hauptsitz in Potsdam oder in der Außenstelle Bernau
    1.  überwiegend Aufgaben im Bereich Lehrerausbildung und Lehrerweiterbildung, im Bereich Reisekostenabrechnung für Lehrkräfte oder im Bereich IT-Ressortfachverfahren und eGovernment wahrnehmen, dem für Schule zuständigen Ministerium zugeordnet, und
    2.  überwiegend andere Aufgaben wahrnehmen, die nicht von Buchstabe a erfasst werden, dem Staatlichen Schulamt Brandenburg an der Havel zugeordnet.