## Brandenburgisches Ausführungsgesetz zum Bundesnaturschutzgesetz (Brandenburgisches Naturschutzausführungsgesetz - BbgNatSchAG)

Inhaltsübersicht
----------------

### Abschnitt 1  
Allgemeine Vorschriften

[§ 1 Regelungsgegenstand  
](#1)[§ 2 Grundsätze der guten fachlichen Praxis der Landwirtschaft (zu § 5 Absatz 2 BNatSchG)  
](#2)[§ 3 Beobachtung von Natur und Landschaft (zu § 6 BNatSchG)](#3)

### Abschnitt 2  
Landschaftsplanung

[§ 4 Landschaftsprogramm, Landschaftsrahmenpläne (zu § 10 BNatSchG)  
](#4)[§ 5 Landschaftspläne, Grünordnungspläne (zu § 11 BNatSchG)](#5)

### Abschnitt 3  
Eingriffe in Natur und Landschaft

[§ 6 Ersatzzahlung (zu § 15 Absatz 6 BNatSchG)  
](#6)[§ 7 Zuständigkeit und Verfahren bei Eingriffen (zu §§ 16, 17 BNatSchG)](#7)

### Abschnitt 4  
Schutzausweisungen

#### Unterabschnitt 1  
Schutzgebiete

[§ 8 Allgemeine Vorschriften (zu § 22 Absatz 1 BNatSchG)  
](#8)[§ 9 Verfahren zur Aufstellung von Unterschutzstellungsverordnungen (zu § 22 Absatz 2 BNatSchG)  
](#9)[§ 10 Verfahren zur Ausgliederung von Flächen (zu § 22 Absatz 2 BNatSchG)  
](#10)[§ 11 Einstweilige Sicherstellung (zu § 22 Absatz 3 BNatSchG)  
](#11)[§ 12 Unbeachtlichkeit von Mängeln, Behebung von Fehlern (zu § 22 Absatz 2 BNatSchG)  
](#12)[§ 13 Bezeichnung, Registrierung (zu § 22 Absatz 4 BNatSchG)](#13)

#### Unterabschnitt 2  
Netz „Natura 2000“

[§ 14 Gebietsbekanntmachung, Erhaltungsziele, Berichte (zu § 32 Absatz 1 und 4 BNatSchG)  
](#14)[§ 15 Schutz Europäischer Vogelschutzgebiete  
](#15)[§ 16 Verfahren bei der Zulassung von Projekten und Plänen (zu § 34 BNatSchG)  
](#16)[§ 16a Gentechnisch veränderte Organismen](#16a)

### Abschnitt 5  
Gesetzlich geschützte Teile von Natur und Landschaft

[§ 17 Alleen (zu § 29 Absatz 3 BNatSchG)  
](#17)[§ 18 Schutz bestimmter Biotope (zu § 30 BNatSchG)](#18)

### Abschnitt 6  
Schutz und Pflege wild lebender Tier- und Pflanzenarten

[§ 19 Horststandorte (zu § 54 Absatz 7 BNatSchG)  
](#19)[§ 20 Zoos (zu § 42 BNatSchG)  
](#20)[§ 21 Tiergehege (zu § 43 BNatSchG)](#21)

### Abschnitt 7  
Erholung in Natur und Landschaft

[§ 22 Betreten der freien Landschaft (zu § 59 BNatSchG)  
](#22)[§ 23 Zulässigkeit von Sperren  
](#23)[§ 24 Satzungsermächtigung zur Umsetzung von Erholungskonzepten, Durchgänge](#24)

### Abschnitt 8  
Eigentumsbindung, Befreiungen

[§ 25 Duldungspflicht (zu § 65 BNatSchG)  
](#25)[§ 26 Vorkaufsrecht  
](#26)[§ 27 Enteignung (zu § 68 Absatz 3 BNatSchG)  
](#27)[§ 28 Entschädigung für Nutzungsbeschränkungen (zu § 68 BNatSchG)  
](#28)[§ 29 Ausnahmen, Befreiungen (zu § 67 BNatSchG)](#29)

### Abschnitt 9  
Behördlicher und ehrenamtlicher Naturschutz, Zuständigkeiten

#### Unterabschnitt 1  
Behörden und Naturschutzfonds

[§ 30 Naturschutzbehörden, Aufgaben, Befugnisse, Zuständigkeiten (zu § 3 Absatz 1 BNatSchG)  
](#30)[§ 31 Unterrichtungs- und Weisungsrecht  
](#31)[§ 32 Verwaltung der Großschutzgebiete  
](#32)[§ 33 Naturschutzfonds](#33)

#### Unterabschnitt 2  
Ehrenamtlicher Naturschutz

[§ 34 Naturschutzhelfer und -helferinnen  
](#34)[§ 35 Naturschutzbeiräte  
](#35)[§ 36 Mitwirkung von anerkannten Naturschutzvereinigungen (zu § 63 BNatSchG)  
](#36)[§ 37 Klagebefugnis von Naturschutzvereinigungen (zu § 64 BNatSchG)  
](#37)[§ 38 Datenverarbeitung](#38)

### Abschnitt 10  
Ordnungswidrigkeiten

[§ 39 Verstöße gegen Bestimmungen der Naturschutzgesetze (zu § 69 BNatSchG)  
](#39)[§ 40 Geldbuße  
](#40)[§ 41 Einziehung](#41)

### Abschnitt 11  
Übergangs- und Schlussbestimmungen

[§ 42 Fortgeltung von Rechtsverordnungen](#42)

### Abschnitt 1  
Allgemeine Vorschriften

#### § 1  
Regelungsgegenstand

Die Vorschriften dieses Gesetzes regeln die Ausführung des Bundesnaturschutzgesetzes vom 29. Juli 2009 (BGBl.
I S. 2542), das zuletzt durch Artikel 5 des Gesetzes vom 6.
Februar 2012 (BGBl. I S. 148) geändert worden ist, in der jeweils geltenden Fassung im Land Brandenburg und ergänzen es.
§ 2, § 4 Absatz 4, § 6 Absatz 1, § 7 Absatz 2, § 8 Absatz 3, § 16a, § 18 Absatz 2 und § 29 Absatz 4 weichen gemäß Artikel 72 Absatz 3 Satz 1 Nummer 2 des Grundgesetzes von den Bestimmungen des Bundesnaturschutzgesetzes ab.

#### § 2  
Grundsätze der guten fachlichen Praxis der Landwirtschaft (zu § 5 Absatz 2 BNatSchG)

Ergänzend zu § 5 Absatz 2 des Bundesnaturschutzgesetzes gehört auch zu den Grundsätzen der guten fachlichen Praxis, dass bei der landwirtschaftlichen Bodennutzung verwendetes Bindematerial nach seinem Einsatz aus der freien Landschaft entfernt werden soll.

#### § 3  
Beobachtung von Natur und Landschaft (zu § 6 BNatSchG)

Die bei den Landesbehörden zu § 6 Absatz 2 und 3 des Bundesnaturschutzgesetzes verfügbaren Daten sind auf Anforderung der zuständigen Behörde zur Verfügung zu stellen.
Die Fachbehörde für Naturschutz und Landschaftspflege gibt in geeigneten Zeitabständen den wissenschaftlichen Stand der Erkenntnisse über ausgestorbene und bedrohte heimische Tier- und Pflanzenarten (Rote Liste) für das Land Brandenburg bekannt.

### Abschnitt 2  
Landschaftsplanung

#### § 4  
Landschaftsprogramm, Landschaftsrahmenpläne (zu § 10 BNatSchG)

(1) Die oberste Naturschutzbehörde stellt ein Landschaftsprogramm im Sinne des § 10 Absatz 1 des Bundesnaturschutzgesetzes auf.

(2) Für die Bereiche der Biosphärenreservate stellt die oberste Naturschutzbehörde Landschaftsrahmenpläne auf.
Im Bereich des Nationalparks übernimmt der Nationalparkplan nach § 7 Absatz 2 des Nationalparkgesetzes Unteres Odertal die Funktion des Landschaftsrahmenplans.
Im Übrigen stellen die unteren Naturschutzbehörden für ihr Gebiet Landschaftsrahmenpläne auf und schreiben sie fort; diese bedürfen der Genehmigung der obersten Naturschutzbehörde.

(3) Die unteren Naturschutzbehörden kreisfreier Städte sollen gemeinsame Landschaftsrahmenpläne mit benachbarten Landkreisen aufstellen und fortschreiben, wenn ihre räumliche Entwicklung wesentlich durch gemeinsame Voraussetzungen und Bedürfnisse bestimmt wird.

(4) Für das Gebiet kreisfreier Städte kann abweichend von § 10 Absatz 2 Satz 2 des Bundesnaturschutzgesetzes von der Aufstellung oder Fortschreibung von Landschaftsrahmenplänen abgesehen werden, wenn für das gesamte Gebiet ein flächendeckender Landschaftsplan nach § 11 Absatz 1 des Bundesnaturschutzgesetzes aufgestellt wird und dieser auch die Funktion des Landschaftsrahmenplans übernimmt.
Der Landschaftsplan gilt in diesem Fall als Landschaftsrahmenplan.

(5) Bei der Festlegung des Untersuchungsrahmens für die Aufstellung des Landschaftsprogramms und der Landschaftsrahmenpläne sind diejenigen Behörden zu beteiligen, deren Aufgabenbereich berührt ist.
Der Entwurf des Landschaftsprogramms oder Landschaftsrahmenplans einschließlich der Angaben zur Überprüfung der Verwirklichung der Planziele sowie weitere Unterlagen, deren Einbeziehung die zuständige Naturschutzbehörde für zweckmäßig hält, werden frühzeitig den Behörden, deren Aufgabenbereich berührt ist, übermittelt und für eine angemessene Dauer von mindestens einem Monat ortsüblich öffentlich ausgelegt.
Die beteiligten Behörden und die betroffene Öffentlichkeit können sich zu dem Entwurf des Landschaftsrahmenplans oder Landschaftsprogramms im Rahmen einer von der zuständigen Naturschutzbehörde zu bestimmenden Frist von mindestens einem Monat äußern.
Wenn der Plan oder das Programm erhebliche Umweltauswirkungen in der Republik Polen haben kann, dann sind die Vorgaben des Rechts der Umweltverträglichkeitsprüfung für die grenzüberschreitende Behörden- und Öffentlichkeitsbeteiligung entsprechend anwendbar.

#### § 5  
Landschaftspläne, Grünordnungspläne (zu § 11 BNatSchG)

(1) Die Landschaftspläne im Sinne des § 11 Absatz 1 des Bundesnaturschutzgesetzes stellen die Gemeinden für ihr Gebiet auf.
Bei der Aufstellung von Landschaftsplänen sind die betroffenen Behörden und die Öffentlichkeit entsprechend § 4 Absatz 5 zu beteiligen.
Die Gemeinden können für Teile des Gemeindegebietes Grünordnungspläne aufstellen.
Bei der Aufstellung von Grünordnungsplänen kann auf die in § 9 Absatz 3 Satz 1 Nummer 1 bis 3 des Bundesnaturschutzgesetzes aufgeführten Darstellungen verzichtet werden.
Für das Verhältnis von Grünordnungsplänen zu Bauleitplänen ist § 11 Absatz 3 des Bundesnaturschutzgesetzes entsprechend anzuwenden.

(2) In Landschafts- und Grünordnungsplänen nach Absatz 1 sind für den besiedelten wie für den unbesiedelten Bereich unter besonderer Berücksichtigung der Pflichten nach § 15 Absatz 2 des Bundesnaturschutzgesetzes die Zweckbestimmung von Flächen sowie Schutz-, Pflege- und Entwicklungsmaßnahmen darzustellen und zwar insbesondere

1.  für den Arten- und Biotopschutz unter Berücksichtigung der Ausbreitungslinien von Tieren und Pflanzen wild lebender Arten, insbesondere der besonders geschützten Arten,
2.  für Freiflächen, die zur Erhaltung oder Verbesserung des örtlichen Klimas von Bedeutung sind; dabei kommt dem Aufbau einer nachhaltigen Energieversorgung insbesondere durch zunehmende Nutzung erneuerbarer Energien nach § 1 Absatz 3 Nummer 4 des Bundesnaturschutzgesetzes eine besondere Bedeutung zu,
3.  zur Vermeidung von Bodenerosionen, zur Regeneration von Böden sowie zur Erhaltung und Förderung eines günstigen Bodenzustandes,
4.  zur Erhaltung oder Verbesserung des Grundwasserdargebots, Wasserrückhaltung und Renaturierung von Gewässern,
5.  zur Erhaltung der für Brandenburg typischen Landschafts- und Ortsbilder sowie zur Beseitigung von Anlagen, die das Landschaftsbild beeinträchtigen und auf Dauer nicht mehr genutzt werden,
6.  zur Errichtung von Erholungs- und Grünanlagen, Kleingärten, Wander-, Rad- und Reitwegen sowie landschaftsgebundenen Sportanlagen,
7.  zur Anlage oder Anpflanzung von Flurgehölzen, Hecken, Büschen, Schutzpflanzungen, Alleen, Baumgruppen oder Einzelbäumen,
8.  zur Erhaltung und Pflege von Baumbeständen und Grünflächen.

Die naturschutzrechtlichen Darstellungen des Grünordnungsplans zum Schutz, zur Pflege und zur Entwicklung von Boden, Natur und Landschaft, die nicht die Voraussetzungen des § 9 Absatz 1 des Baugesetzbuches erfüllen, können auf der Grundlage des § 9 Absatz 4 des Baugesetzbuches als Festsetzungen in den Bebauungsplan aufgenommen werden.
Für diese Festsetzungen sind die Verfahrensvorschriften des Baugesetzbuches entsprechend anzuwenden.
Die Landschafts- und Grünordnungspläne sollen die Darstellungen der Bewirtschaftungspläne im Sinne des § 32 Absatz 5 des Bundesnaturschutzgesetzes berücksichtigen, soweit sie hierfür geeignet sind.

(3) Soweit kein Bebauungsplan aufgestellt wird, kann die Gemeinde einen Grünordnungsplan als Satzung beschließen.
In diesem sind die Zweckbestimmung für Flächen und Schutz-, Pflege- und Entwicklungsmaßnahmen im Sinne von Absatz 2 sowie die zur Erreichung der Ziele des Naturschutzes und der Landschaftspflege erforderlichen Ge- und Verbote festzusetzen.
Für das Verfahren zur Aufstellung von Grünordnungsplänen nach Satz 1 gelten die Vorschriften des Baugesetzbuches für Bebauungspläne mit Ausnahme des § 10 Absatz 2 des Baugesetzbuches sowie die Vorschriften über die Veränderungssperre entsprechend.
Eine Veränderungssperre kann ausgesprochen werden, wenn zu befürchten ist, dass durch Veränderungen der Zweck beabsichtigter Schutz-, Pflege- und Entwicklungsmaßnahmen gefährdet würde.

(4) Bei der Aufstellung von Landschafts- und Grünordnungsplänen durch kreisangehörige Gemeinden ist die untere Naturschutzbehörde und bei der Aufstellung durch kreisfreie Städte die Fachbehörde für Naturschutz und Landschaftspflege zu beteiligen.

(5) Das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung

1.  Vorschriften über das Verfahren, insbesondere Näheres zur Beteiligung der Öffentlichkeit und betroffener Behörden, bei der Aufstellung des Landschaftsprogramms, von Landschaftsrahmenplänen, Landschafts- und Grünordnungsplänen sowie
2.  Vorschriften über die Ausarbeitung von Landschafts- und Grünordnungsplänen im Sinne des § 11 Absatz 1 des Bundesnaturschutzgesetzes einschließlich der dazugehörenden Unterlagen, den jeweiligen Maßstab, die Systematik der Pläne und die Darstellung der Inhalte zu erlassen.

### Abschnitt 3  
Eingriffe in Natur und Landschaft

#### § 6  
Ersatzzahlung (zu § 15 Absatz 6 BNatSchG)

(1) Abweichend von § 15 Absatz 6 Satz 1 des Bundesnaturschutzgesetzes soll eine Ersatzzahlung auch geleistet werden, wenn durch die Verwendung der Ersatzzahlung nach Satz 2 und 3 eine Aufwertung des Naturhaushalts oder des Landschaftsbildes mit gleichen Aufwendungen besser verwirklicht werden kann als durch Ausgleich oder Ersatz der Beeinträchtigung nach § 15 Absatz 2 des Bundesnaturschutzgesetzes.
Die Ersatzzahlung soll nach Möglichkeit im Gebiet des betroffenen Landkreises oder der kreisfreien Stadt, ansonsten im betroffenen Naturraum verwendet werden.

(2) Die Ersatzzahlung ist als zweckgebundene Abgabe an das Land zu entrichten, das sie an die nach § 33 zuständige Stiftung weiterleitet.

#### § 7  
Zuständigkeit und Verfahren bei Eingriffen (zu §§ 16, 17 BNatSchG)

(1) Die zur Durchführung des § 15 des Bundesnaturschutzgesetzes erforderlichen Entscheidungen ergehen im Einvernehmen mit der gleichgeordneten Naturschutzbehörde; wird der Eingriff durch Landkreise oder kreisfreie Städte vorgenommen oder ist für die Zulassung des Eingriffs eine Bundesbehörde, eine oberste Landesbehörde oder eine Landesoberbehörde zuständig, ergeht die Entscheidung im Einvernehmen mit der Fachbehörde für Naturschutz und Landschaftspflege.
Das Einvernehmen gilt als erteilt, wenn es nicht binnen eines Monats nach Eingang des Ersuchens der Zulassungs- oder Anzeigebehörde unter Darlegung der Gründe verweigert wird.
Entscheidungen ergehen, soweit für sie die Konzentrationswirkung nach § 1 Absatz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg in Verbindung mit § 75 des Verwaltungsverfahrensgesetzes gilt, im Benehmen mit der Naturschutzbehörde.

(2) Abweichend von § 17 Absatz 3 des Bundesnaturschutzgesetzes ist eine Genehmigung auch dann erforderlich, wenn für einen Eingriff auf Basis anderer fachrechtlicher Prüfungen auf die Durchführung eines vorgeschriebenen Zulassungs- oder Anzeigeverfahrens verzichtet wird.

(3) Das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung das Nähere zur Bevorratung von Kompensationsmaßnahmen nach § 16 des Bundesnaturschutzgesetzes zu regeln; insbesondere können Bestimmungen getroffen werden über

1.  die Anrechnung und Bewertung vorgezogener Maßnahmen;
2.  die Zertifizierung von Maßnahmen- oder Flächenpools;
3.  die Anerkennung von Agenturen zur Bevorratung und zum Vertrieb vorlaufender Kompensationsmaßnahmen oder hierfür geeigneter Flächen, auch im Auftrag Dritter, die die Kompensationsverpflichtung mit befreiender Wirkung von den Verpflichteten gegen Entgelt übernehmen können und einer fachlichen Beaufsichtigung durch das Land unterstellt werden.

(4) Die Ermächtigung zum Erlass von Rechtsverordnungen nach § 17 Absatz 11 des Bundesnaturschutzgesetzes wird auf das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung übertragen.

### Abschnitt 4  
Schutzausweisungen

#### Unterabschnitt 1  
Schutzgebiete

#### § 8  
Allgemeine Vorschriften (zu § 22 Absatz 1 BNatSchG)

(1) Teile von Natur und Landschaft können durch Gesetz zum Nationalpark, durch Rechtsverordnung der Landesregierung zum Nationalen Naturmonument, durch Rechtsverordnung der zuständigen Naturschutzbehörde zum Naturschutzgebiet, Landschaftsschutzgebiet, Naturdenkmal oder geschützten Landschaftsbestandteil und durch Bekanntmachung im Amtsblatt für Brandenburg zum Biosphärenreservat oder Naturpark erklärt werden.
Auf Rechtsverordnungen nach Satz 1 und auf Verfügungen nach § 11 Satz 1 finden die Vorschriften des Ordnungsbehördengesetzes keine Anwendung.

(2) Die Gemeinden können innerhalb der im Zusammenhang bebauten Ortsteile und des Geltungsbereichs von Bebauungsplänen geschützte Landschaftsbestandteile im Sinne des § 29 des Bundesnaturschutzgesetzes auch durch Satzung unter Schutz stellen.
Die Festsetzungen in den Satzungen gehen entsprechenden Rechtsverordnungen zur Unterschutzstellung geschützter Landschaftsbestandteile vor.
Die Gemeinden nehmen die Aufgabe nach Satz 1 als freiwillige Selbstverwaltungsaufgabe wahr.

(3) Die Rechtsverordnungen nach Absatz 1 können abweichend von § 22 Absatz 1 des Bundesnaturschutzgesetzes bestimmte Handlungen von einer Genehmigung abhängig machen.
Die Genehmigung darf nur erteilt werden, wenn die beabsichtigte Handlung dem besonderen Schutzzweck nicht oder nur unerheblich zuwiderläuft.

(4) Schutz, Pflege und Entwicklung der Biosphärenreservate und Naturparke sind durch eine einheitliche Verwaltung zu gewährleisten.

#### § 9  
Verfahren zur Aufstellung von Unterschutzstellungsverordnungen (zu § 22 Absatz 2 BNatSchG)

(1) Vor dem Erlass von Rechtsverordnungen nach § 8 Absatz 1 ist den Gemeinden, deren Gebiet betroffen ist, und den betroffenen Trägern öffentlicher Belange Gelegenheit zur Stellungnahme zu geben.

(2) Die Entwürfe der Rechtsverordnungen und die dazugehörenden Karten sind einen Monat bei den unteren Naturschutzbehörden, den Ämtern und den amtsfreien Gemeinden, deren Gebiet betroffen ist, öffentlich auszulegen.
Zeit und Ort der Auslegung sind mindestens zwei Wochen vorher im Amtsblatt für Brandenburg oder, in den Fällen einer Unterschutzstellung durch die untere Naturschutzbehörde, im amtlichen Bekanntmachungsblatt des Landkreises oder der kreisfreien Stadt sowie in den betroffenen amtsfreien Gemeinden und Ämtern ortsüblich mit dem Hinweis bekannt zu machen, dass Bedenken und Anregungen während der Auslegungsfrist von den Betroffenen vorgebracht werden können.
Mit der öffentlichen Bekanntmachung der Auslegung bis zum Inkrafttreten der Rechtsverordnung, jedoch längstens drei Jahre mit der Möglichkeit der Verlängerung um ein weiteres Jahr, gilt § 22 Absatz 3 Satz 3 des Bundesnaturschutzgesetzes für das betroffene Gebiet entsprechend.
Sieht der Entwurf der Rechtsverordnung vor, dass für bestimmte Flächen eine bestehende Schutzverordnung aufgehoben werden soll und unterliegen diese Flächen keiner Veränderungssperre nach Satz 3, sind die Regelungen der bestehenden Schutzverordnung bis zum Inkrafttreten der neuen Schutzverordnung nicht mehr anzuwenden.

(3) Die zum Zeitpunkt der Bekanntmachung der öffentlichen Auslegung ausgeübte rechtmäßige Bodennutzung und rechtmäßige Ausübung der Jagd und Fischerei bleibt von der Veränderungssperre nach Absatz 2 Satz 3 unberührt.
Hierauf ist in der Bekanntmachung nach Absatz 2 Satz 2 hinzuweisen.
Die für die Unterschutzstellung zuständige Behörde kann abweichend von Satz 1 die rechtmäßig ausgeübte Bodennutzung und die rechtmäßige Ausübung der Jagd und Fischerei in Übereinstimmung mit dem Entwurf der Rechtsverordnung für die Dauer der Veränderungssperre im Einzelfall einschränken oder untersagen, wenn dies zum Erhalt des zu schützenden Teils von Natur und Landschaft erforderlich ist.
Die Anordnung nach Satz 3 soll zusammen mit der Bekanntmachung nach Absatz 2 Satz 2 bekannt gemacht werden.

(4) Von der Auslegung kann abgesehen werden, wenn die Personen, deren Belange von der vorgesehenen Rechtsverordnung berührt werden, bekannt sind, ihnen Gelegenheit gegeben wird, den Entwurf der Rechtsverordnung innerhalb eines Monats einzusehen und Bedenken und Anregungen vorzubringen.

(5) Die für den Erlass der Rechtsverordnung zuständige Naturschutzbehörde prüft im Rahmen einer Abwägung die fristgemäß vorgebrachten Bedenken und Anregungen und teilt das Ergebnis den Betroffenen schriftlich mit.

(6) Die Absätze 1 bis 5 sind nicht anzuwenden,

1.  für Rechtsverordnungen des für Naturschutz und Landschaftspflege zuständigen Mitgliedes der Landesregierung zur Festsetzung geschützter Landschaftsbestandteile;
2.  wenn eine Rechtsverordnung nur unwesentlich geändert oder nur dem geltenden Recht angepasst werden soll;
3.  wenn eine Rechtsverordnung erlassen werden soll, die sich ausschließlich auf Flächen erstreckt, die zu Zwecken des Naturschutzes und der Landschaftspflege erworben oder bereitgestellt worden sind; vor Erlass der Rechtsverordnung sind die betroffenen Nutzungsberechtigten zu hören;
4.  wenn eine Rechtsverordnung über ein Landschaftsschutzgebiet geändert werden soll, um eine Regelung zu ergänzen, die die Nichtgeltung einzelner Bestimmungen der Schutzgebietsverordnung für Flächen im Geltungsbereich eines Bauleitplans regelt, für die eine bauliche oder sonstige Nutzung dargestellt oder festgesetzt werden soll, sofern der Verordnungsgeber diesen Darstellungen oder Festsetzungen zugestimmt hat;
5.  wenn eine Rechtsverordnung über ein Naturdenkmal oder einzelne geschützte Landschaftsbestandteile erlassen oder eine Rechtsverordnung nur auf Grundstücke erstreckt werden soll, die im Eigentum weniger Personen stehen und diese bekannt sind; vor Erlass der Rechtsverordnung sind die Betroffenen und die Gemeinden zu hören;
6.  wenn geltende Unterschutzstellungsverordnungen geändert werden sollen, um den Schutzzweck an die Anforderungen zum Schutz des Europäischen ökologischen Netzes „Natura 2000“ anzupassen.

In dem Fall von Satz 1 Nummer 6 sind die Änderungen der Rechtsverordnung auch ortsüblich bekannt zu machen.

(6a) Die Absätze 2 bis 5 sind nicht anzuwenden für Rechtsverordnungen der Landesregierung zur Festsetzung eines Nationalen Naturmonumentes, dessen Gebiet wie ein Naturschutzgebiet geschützt ist.

(7) Rechtsverordnungen zur Unterschutzstellung von Teilen von Natur und Landschaft müssen mit hinreichender Klarheit erkennen lassen, welche Teile von Natur und Landschaft geschützt sind und welche Grundstücke zu einem Schutzgebiet gehören.
Im Zweifelsfall gelten Grundstücke als nicht betroffen.
Die Abgrenzung eines Schutzgebietes ist in der Rechtsverordnung

1.  zu beschreiben, wenn es sich mit Worten zweifelsfrei erfassen lässt, oder
2.  grob zu beschreiben oder zu bezeichnen und in Karten darzustellen, die Bestandteil der Rechtsverordnung sind oder bei der erlassenden Naturschutzbehörde und bei einer oder mehreren unteren Naturschutzbehörden eingesehen werden können; die Darstellung der Abgrenzung in Karten kann für einzelne Flächen durch eine Beschreibung ergänzt werden.

#### § 10  
Verfahren zur Ausgliederung von Flächen (zu § 22 Absatz 2 BNatSchG)

Bei der Änderung einer Rechtsverordnung über ein Landschaftsschutzgebiet durch Ausgliederung von Flächen aus dem geschützten Gebiet (Ausgliederungsverfahren) entfallen die Beteiligung und die öffentliche Auslegung nach § 9 Absatz 1 und 2, soweit diese durch die Gemeinde im Rahmen der Aufstellung eines Flächennutzungsplans oder von städtebaulichen Satzungen zur Festsetzung einer baulichen Nutzung (Satzungen nach den §§ 8, 9, 10, 12, 34 Absatz 4, § 35 Absatz 6 des Baugesetzbuches) erfolgt ist.
Die der Gemeinde dabei zugegangenen Stellungnahmen sind an die zuständige Naturschutzbehörde zu übergeben.
Die Gemeinde hat vor Einleitung des Beteiligungsverfahrens nach § 3 Absatz 1 des Baugesetzbuches bei der zuständigen Naturschutzbehörde einen Ausgliederungsantrag zu stellen und diesen gleichzeitig durch Vorlage insbesondere des Aufstellungsbeschlusses des Flächennutzungsplans oder der Satzung sowie weiterer beurteilungsfähiger Unterlagen zu begründen.
Die Festsetzungen der städtebaulichen Satzung haben mit dem Eintritt der Rechtsverbindlichkeit Vorrang vor den entgegenstehenden Regelungen der Rechtsverordnung, wenn die zuständige Naturschutzbehörde den Antrag auf Ausgliederung zuvor genehmigt hat.
Die Genehmigung kann mit Nebenbestimmungen versehen werden.

### § 11  
Einstweilige Sicherstellung (zu § 22 Absatz 3 BNatSchG)

Die einstweilige Sicherstellung im Sinne des § 22 Absatz 3 des Bundesnaturschutzgesetzes ergeht als Rechtsverordnung, Satzung oder als Verfügung durch die für die Unterschutzstellung zuständige Stelle.
Betroffene Gemeinden und betroffene Behörden sind zu hören.
Die zuständige Stelle hat den betroffenen Gemeinden innerhalb eines Jahres nach Erlass der einstweiligen Sicherstellung mitzuteilen, ob und inwieweit die nähere Prüfung die Schutzbedürftigkeit der sichergestellten Fläche oder des sichergestellten Objektes ergeben hat.

#### § 12  
Unbeachtlichkeit von Mängeln, Behebung von Fehlern (zu § 22 Absatz 2 BNatSchG)

(1) Eine ein Naturdenkmal ausweisende Rechtsverordnung ist nicht deshalb nichtig, weil ein geschützter Landschaftsbestandteil hätte ausgewiesen werden müssen, soweit eine Rechtsverordnung zur Unterschutzstellung eines geschützten Landschaftsbestandteils unter Berücksichtigung des Schutzzwecks zu dem gleichen Schutz hätte führen müssen.
Das Gleiche gilt, wenn eine Rechtsverordnung eine Einzelschöpfung der Natur nicht als Naturdenkmal, sondern als geschützten Landschaftsbestandteil ausgewiesen hat.

(2) Eine Verletzung der in den §§ 9 und 10 genannten Verfahrens- und Formvorschriften ist unbeachtlich, wenn sie nicht schriftlich unter Angabe der verletzten Rechtsvorschrift und des Sachverhalts, der die Verletzung begründen soll, innerhalb eines Jahres nach Inkrafttreten der Rechtsverordnung gegenüber der Naturschutzbehörde geltend gemacht worden ist, die die Rechtsverordnung erlassen hat.
Das Gleiche gilt für Mängel bei der Beschreibung des Schutzzwecks sowie für Mängel bei der Prüfung der Erforderlichkeit der Unterschutzstellung einzelner Flächen.
Mängel im Abwägungsvorgang sind nur dann beachtlich, wenn sie offensichtlich und auf das Abwägungsergebnis von Einfluss gewesen sind und die Mängel in der Abwägung innerhalb von vier Jahren nach Inkrafttreten der Rechtsverordnung unter den in Satz 1 genannten Voraussetzungen geltend gemacht worden sind.
In der Rechtsverordnung ist auf die Rechtsfolgen nach den Sätzen 1 bis 3 hinzuweisen.

(3) Eine Rechtsverordnung kann mit rückwirkender Kraft erneut erlassen werden, wenn sie eine Regelung, die auf einem Form- oder Verfahrensfehler beruht, ersetzt.

#### § 13  
Bezeichnung, Registrierung (zu § 22 Absatz 4 BNatSchG)

(1) Die Bezeichnungen „Naturschutzgebiet“, „Nationales Naturmonument“, „Nationalpark“, „Landschaftsschutzgebiet“, „Naturdenkmal“, „geschützter Landschaftsbestandteil“, „Naturpark“ und „Biosphärenreservat“ dürfen nur für die nach diesem Abschnitt und den §§ 23 bis 29 des Bundesnaturschutzgesetzes geschützten Gebiete und Gegenstände verwendet werden.
Die nach § 18 dieses Gesetzes und § 30 des Bundesnaturschutzgesetzes gesetzlich geschützten Biotope und die Natura 2000-Gebiete im Sinne des § 7 Absatz 1 Nummer 8 des Bundesnaturschutzgesetzes können gekennzeichnet werden.

(2) Die für die Unterschutzstellung zuständigen Stellen führen Verzeichnisse der von ihnen geschützten Gebiete und Gegenstände.

(3) Die Kennzeichnung der nach diesem Abschnitt geschützten Gebiete und Gegenstände ist zu dulden.

#### Unterabschnitt 2  
Netz „Natura 2000“

#### § 14  
Gebietsbekanntmachung, Erhaltungsziele, Berichte (zu § 32 Absatz 1 und 4 BNatSchG)

(1) Für die Auswahl und Benennung der Gebiete im Sinne des § 32 Absatz 1 Satz 1 des Bundesnaturschutzgesetzes sowie für die Herstellung des Benehmens nach § 32 Absatz 1 Satz 2 des Bundesnaturschutzgesetzes ist die Landesregierung zuständig.

(2) Die nach § 32 Absatz 1 des Bundesnaturschutzgesetzes benannten Schutzgebiete sowie Änderungen der Gebietsbenennungen werden im Amtsblatt für Brandenburg bekannt gemacht.

(3) Soweit nach § 32 Absatz 4 des Bundesnaturschutzgesetzes eine Unterschutzstellung nach § 32 Absatz 2 und 3 des Bundesnaturschutzgesetzes unterbleiben kann, wird das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung ermächtigt, durch Rechtsverordnung die hierfür festzulegenden gebietsspezifischen Erhaltungsziele sowie die Gebietsabgrenzung festzusetzen.

(4) Die oberste Naturschutzbehörde erstellt die Berichte gemäß Artikel 17 der Richtlinie 92/43/EWG und Artikel 12 der Richtlinie 2009/147/EG auf der Grundlage periodischer Erhebungen der Fachbehörde für Naturschutz und Landschaftspflege über den Erhaltungszustand der Gebiete von gemeinschaftlicher Bedeutung (Artikel 11 der Richtlinie 92/43/EWG).

#### § 15  
Schutz Europäischer Vogelschutzgebiete

(1) Die in der Anlage 1 aufgeführten Europäischen Vogelschutzgebiete werden nach Maßgabe des § 33 Absatz 1 Satz 1 des Bundesnaturschutzgesetzes besonders geschützt.
Sie sind in der als Anlage 2 beigefügten Übersichtskarte im Maßstab 1 : 300 000 mit dunkelgrüner Farbe dargestellt.
Zweck des Schutzes der in Anlage 1 genannten Gebiete ist die Erhaltung oder Wiederherstellung eines günstigen Erhaltungszustandes der für die jeweiligen Gebiete aufgeführten europäischen Vogelarten.
Für die Gebiete gelten die in der Anlage 1 genannten Erhaltungsziele.
Die Gebietsabgrenzungen sind in den in Anlage 3 aufgeführten topografischen Karten im Maßstab 1 : 50 000 eingezeichnet und mit dunkelgrüner Farbe dargestellt.
Eine Blattschnittübersicht ist als Anlage 4 beigefügt.
Anlage 5 führt die bereits gemäß § 32 Absatz 2 des Bundesnaturschutzgesetzes unter Schutz gestellten Europäischen Vogelschutzgebiete auf.

(2) Das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnungtspflege zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung

1.  die in der Anlage 1 dargestellten Erhaltungsziele zu ändern,
2.  die Gebietsabgrenzungen in den in der Anlage 3 aufgeführten topografischen Karten zu ändern, wenn und soweit Gebietsänderungen nach Artikel 4 der Richtlinie 2009/147/EG notwendig werden,
3.  die Gebietsabgrenzungen in den in der Anlage 3 aufgeführten topografischen Karten auf Karten im Maßstab von mindestens 1 : 10 000 umzutragen.

(3) Soweit bei Inkrafttreten dieses Gesetzes bereits erlassene Schutzerklärungen Europäische Vogelschutzgebiete umfassen, gelten als deren Schutzzweck jeweils auch die in der Anlage 1 zu den jeweiligen Europäischen Vogelschutzgebieten aufgeführten Erhaltungsziele.

#### § 16  
Verfahren bei der Zulassung von Projekten und Plänen (zu § 34 BNatSchG)

(1) Für die Entscheidungen und Maßnahmen nach § 34 Absatz 1 und 3 bis 5 des Bundesnaturschutzgesetzes und, soweit eine Ausnahme nicht erteilt werden kann, die Entscheidung über eine Befreiung nach § 67 Absatz 2 des Bundesnaturschutzgesetzes ist die nach dem jeweiligen Fachgesetz zuständige Zulassungs- oder Anzeigebehörde zuständig.
Die Entscheidungen ergehen, soweit Rechtsvorschriften nichts anderes bestimmen, im Einvernehmen mit der gleichgeordneten Naturschutzbehörde; wird das Projekt von einem Landkreis oder einer kreisfreien Stadt durchgeführt oder ist für die Zulassung oder Anzeige eine Bundesbehörde, eine oberste Landesbehörde oder Landesoberbehörde zuständig, ergeht die Entscheidung im Einvernehmen mit der Fachbehörde für Naturschutz und Landschaftspflege.
Das Einvernehmen gilt als erteilt, wenn es nicht binnen eines Monats nach Eingang des Ersuchens der Zulassungs- oder Anzeigebehörde unter Darlegung der Gründe verweigert wird.
Entscheidungen ergehen, soweit für sie die Konzentrationswirkung nach § 1 Absatz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg in Verbindung mit § 75 des Verwaltungsverfahrensgesetzes gilt, im Benehmen mit der Naturschutzbehörde.

(2) Bei der Aufstellung von Plänen im Sinne des § 36 des Bundesnaturschutzgesetzes ist der Planungsträger für die Entscheidungen und Maßnahmen nach § 34 Absatz 1 bis 5 des Bundesnaturschutzgesetzes zuständig.
Absatz 1 Satz 2 bis 4 gilt entsprechend.

(3) Die nach Absatz 1 zuständige Behörde holt die Stellungnahme der Kommission nach § 34 Absatz 4 Satz 2 des Bundesnaturschutzgesetzes über die oberste Naturschutzbehörde ein.

(4) Die Unterrichtung der Kommission über das Bundesministerium für Umwelt, Naturschutz und Reaktorsicherheit nach § 34 Absatz 5 Satz 2 des Bundesnaturschutzgesetzes erfolgt durch die nach Absatz 1 zuständige Behörde über die oberste Naturschutzbehörde.

#### § 16a  
Gentechnisch veränderte Organismen

Abweichend von § 35 des Bundesnaturschutzgesetzes sind auf

1.  Freisetzungen gentechnisch veränderter Organismen im Sinne des § 3 Nummer 5 des Gentechnikgesetzes in der Fassung der Bekanntmachung vom 16.
    Dezember 1993 (BGBl. I S. 2066), das zuletzt durch Artikel 1 des Gesetzes vom 9.
    Dezember 2010 (BGBl.
    I S.
    1934) geändert worden ist und
2.  die land-, forst- und fischereiwirtschaftliche Nutzung von rechtmäßig in Verkehr gebrachten Produkten, die gentechnisch veränderte Organismen enthalten oder aus solchen bestehen, sowie den sonstigen, insbesondere auch nicht erwerbswirtschaftlichen, Umgang mit solchen Produkten, der in seinen Auswirkungen den vorgenannten Handlungen vergleichbar ist, innerhalb eines Natura 2000-Gebietes und eines Umgriffs von 1 000 m um das Gebiet

§ 34 Absatz 1 und 2 des Bundesnaturschutzgesetzes entsprechend anzuwenden; im Fall der Nummer 2 gilt § 34 Absatz 6 des Bundesnaturschutzgesetzes entsprechend mit der Maßgabe, dass § 34 Absatz 3 bis 5 des Bundesnaturschutzgesetzes nicht anzuwenden sind.

### Abschnitt 5  
Gesetzlich geschützte Teile von Natur und Landschaft

#### § 17  
Alleen (zu § 29 Absatz 3 BNatSchG)

(1) Alleen dürfen nicht beseitigt, zerstört, beschädigt oder sonst erheblich oder nachhaltig beeinträchtigt werden.

(2) Von den Verboten des Absatzes 1 kann eine Ausnahme zugelassen werden, wenn sie aus zwingenden Gründen der Verkehrssicherheit erforderlich ist und keine anderen Maßnahmen zur Erhöhung der Verkehrssicherheit erfolgreich durchgeführt werden konnten.
Kommt es aufgrund der durchgeführten Maßnahmen zu einer Bestandsminderung, sind die jeweiligen Eigentümer oder Eigentümerinnen zu verpflichten, in angemessenem und zumutbarem Umfang Ersatzpflanzungen vorzunehmen.
Die Pflichten aus den Sätzen 1 und 2 gelten auch für Maßnahmen der Straßenbaulastträger im Rahmen der Straßenunterhaltung.

(3) Um den Alleenbestand nachhaltig zu sichern, soll die jeweils zuständige Behörde, insbesondere im Rahmen von Ausgleichs- und Ersatzmaßnahmen, rechtzeitig und in ausreichendem Umfang Alleenneupflanzungen festsetzen oder für deren Durchführung sorgen.

#### § 18  
Schutz bestimmter Biotope (zu § 30 BNatSchG)

(1) Die Verbote des § 30 Absatz 2 des Bundesnaturschutzgesetzes gelten auch für Feuchtwiesen, Lesesteinhaufen, Streuobstbestände, Moorwälder, Hangwälder und Restbestockungen anderer natürlicher Waldgesellschaften.

(2) Ergänzend zu § 30 Absatz 2 des Bundesnaturschutzgesetzes gelten als Handlungen, die zu einer erheblichen Beeinträchtigung führen können, insbesondere die Intensivierung oder Änderung der Nutzung der geschützten Biotope und der Eintrag von Stoffen, die geeignet sind, das Biotop nachteilig zu beeinflussen.

(3) Das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung die in Absatz 1 und § 30 Absatz 2 des Bundesnaturschutzgesetzes aufgeführten Biotope näher zu umschreiben und festzulegen, in welcher Ausprägung sie geschützt sind.

(4) Die zuständige Naturschutzbehörde führt ein Verzeichnis der gesetzlich geschützten Biotope und schreibt es fort.
Das Verzeichnis soll auf geeignete Weise, insbesondere über elektronische Medien, für jedermann einsehbar gemacht werden.

### Abschnitt 6  
Schutz und Pflege wild lebender Tier- und Pflanzenarten

#### § 19  
Horststandorte (zu § 54 Absatz 7 BNatSchG)

(1) Zum Schutz der Horststandorte der Adler, Wanderfalken, Korn- und Wiesenweihen, Schwarzstörche, Kraniche, Sumpfohreulen und Uhus ist es verboten,

1.  im Umkreis von 100 Metern um den Horststandort Bestockungen abzutreiben oder den Charakter des Gebietes sonst zu verändern,
2.  im Umkreis von 300 Metern um den Horststandort in der Zeit vom 1.
    Februar bis zum 31. August

> 1.  land- und forstwirtschaftliche Maßnahmen unter Maschineneinsatz durchzuführen oder
> 2.  die Jagd auszuüben, mit Ausnahme der Nachsuche,

3.  im Umkreis von 300 Metern um den Horststandort jagdliche Einrichtungen zu bauen.

Satz 1 gilt, mit Ausnahme des Verbots in Nummer 2 Buchstabe b, nicht für Fischadler, deren Horste sich auf Masten in der bewirtschafteten Feldflur befinden, sowie für Kraniche, die in der bewirtschafteten Feldflur nisten.
Die Schutzfrist in Satz 1 Nummer 2 beginnt um die Horststandorte der Seeadler und Uhus bereits am 1.
Januar; sie endet um den Nistplatz der Kraniche bereits am 30. Juni.

(2) Auf Antrag einer durch Absatz 1 in ihren Rechten betroffenen Person überprüft die zuständige Naturschutzbehörde im Einzelfall die Schutzbestimmungen nach Absatz 1.
Nach Beratung durch die Fachbehörde für Naturschutz und Landschaftspflege kann sie die Schutzzonen nach Absatz 1 Satz 1 Nummer 1 bis 3 oder die Schutzfristen nach Absatz 1 Satz 1 Nummer 2 und Satz 3 verändern, sofern der Schutzzweck dadurch nicht beeinträchtigt wird; sie kann zum Schutz der Schreiadler und Schwarzstörche im Einzelfall die Schutzzonen erweitern oder die Schutzfristen verlängern.

#### § 20  
Zoos (zu § 42 BNatSchG)

(1) Zusammen mit der Genehmigung nach § 42 Absatz 2 des Bundesnaturschutzgesetzes soll auf Antrag über das Vorliegen der Voraussetzungen nach § 4 Nummer 20 Buchstabe a Satz 1 des Umsatzsteuergesetzes entschieden werden.

(2) Die Genehmigung nach § 42 Absatz 2 Satz 1 des Bundesnaturschutzgesetzes schließt die Erlaubnis nach § 11 Absatz 1 Satz 1 Nummer 2a und 3 Buchstabe d des Tierschutzgesetzes ein und ergeht im Einvernehmen mit den nach den Vorschriften des Tierschutz- und Veterinärrechts zuständigen Behörden.

#### § 21  
Tiergehege (zu § 43 BNatSchG)

Die Anzeigepflicht des § 43 Absatz 3 des Bundesnaturschutzgesetzes gilt nicht für

1.  Gehege, die unter staatlicher Aufsicht stehen, insbesondere in denen Tiere wild lebender Arten zu Zwecken der Wiederansiedlung im Rahmen eines Artenschutzprogramms der Fachbehörde für Naturschutz und Landschaftspflege gehalten werden,
2.  Gehege, die nur für kurze Zeit aufgestellt werden, insbesondere Volieren zur Auswilderung von nicht gebietsfremdem, heimischem Federwild,
    
3.  Anlagen der Teichwirtschaft und Fischzucht,
    
4.  Gehege zur Haltung von heimischem Schalenwild nach § 2 des Bundesjagdgesetzes und
    
5.  sonstige Gehege, wenn eine Größe von insgesamt 1 000 m2 nicht überschritten wird.
    

Das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung weitere Gehege von der Anzeigepflicht nach § 43 Absatz 3 des Bundesnaturschutzgesetzes unter den Voraussetzungen des § 43 Absatz 4 des Bundesnaturschutzgesetzes auszunehmen.

### Abschnitt 7  
Erholung in Natur und Landschaft

#### § 22  
Betreten der freien Landschaft (zu § 59 BNatSchG)

(1) In der freien Landschaft darf jede Person private Wege und Pfade, Feldraine, Heide-, Öd- und Brachflächen sowie landwirtschaftliche Nutzflächen außerhalb der Nutzzeit zum Zwecke der Erholung auf eigene Gefahr betreten oder mit Krankenfahrstühlen befahren, auf Wegen Rad fahren und Fahrräder mit Trethilfe und einer durch die Bauart bestimmten Höchstgeschwindigkeit von nicht mehr als 25 km/h benutzen sowie auf Wegen, die von zwei- oder mehrspurigen Fahrzeugen befahren werden können, reiten oder mit bespannten Fahrzeugen fahren.
Fuß-, Rad-, Reit- und Wasserwanderer sowie -wanderinnen dürfen in der freien Landschaft für eine Nacht Zelte aufstellen.
Abweichungen von den Betretungsrechten aus den Sätzen 1 und 2, die sich aus den Bestimmungen dieses Gesetzes oder aus anderen Rechtsvorschriften ergeben, oder andere gesetzliche Betretungsrechte bleiben unberührt.
Als Nutzzeit gilt die Zeit zwischen der Saat oder Bestellung und der Ernte, bei Grünland die Zeit des Aufwuchses.
Ausgenommen von den Betretungsrechten nach den Sätzen 1 und 2 sind Gärten, Hofräume und sonstige zum privaten Wohnbereich, der sich nach den berechtigten Wohnbedürfnissen und den örtlichen Gegebenheiten bestimmt, gehörende oder einem gewerblichen oder öffentlichen Betrieb dienende Flächen.

(2) Das Betretungsrecht darf nur so ausgeübt werden, dass die Belange der anderen Erholungssuchenden und die Eigentums- und Nutzungsrechte nicht unzumutbar beeinträchtigt werden, insbesondere sind abgelegte Gegenstände und Abfälle aus der freien Landschaft zu entfernen.
Die Erholungssuchenden haben im Übrigen besondere Rücksicht auf Natur, Landschaft, Vegetation und wild lebende Tiere sowie die Waldbrandgefahr zu nehmen.

(3) Es ist verboten, auf Sport- oder Lehrpfaden und auf Wegen, die nicht mit zwei- oder mehrspurigen Fahrzeugen befahren werden können, zu reiten oder mit bespannten Fahrzeugen zu fahren.
Es ist ferner verboten, auf Sport- und Lehrpfaden und auf Wegen und Pfaden sowie auf Flächen außerhalb von Wegen mit motorisierten Fahrzeugen zu fahren.
Von dem Verbot nach Satz 2 ist der land-, forst-, fischerei- und wasserwirtschaftliche Verkehr sowie der auf die berechtigte Jagdausübung bezogene Verkehr insbesondere zur Wildbergung ausgenommen.
Von den Verboten nach Satz 2 sind weiterhin Fahrräder mit Trethilfe und einer durch die Bauart bestimmten Höchstgeschwindigkeit von nicht mehr als 25 km/h ausgenommen.

(4) Zelte oder sonstige bewegliche Unterkünfte (Wohnwagen) dürfen in der freien Landschaft unbeschadet weitergehender Vorschriften außerhalb von öffentlichen Straßen und Plätzen nur auf einem Zelt- oder Campingplatz aufgestellt und benutzt werden.
Zelte von Fuß-, Rad-, Reit- und Wasserwanderern und -wanderinnen dürfen auch auf Biwakplätzen, die eine Gemeinde im Rahmen der sonstigen naturschutzrechtlichen Vorschriften und mit Gestattung des Grundstückseigentümers oder der Grundstückseigentümerin in der freien Landschaft ausgewiesen hat, aufgestellt und benutzt werden.

(5) Die Landkreise oder kreisfreien Städte oder von ihnen beauftragte Organisationen oder Personen können Wanderwege, Radwanderwege, Reitwege sowie Sport- und Lehrpfade markieren.

(6) Soweit Gärten, Hofräume und sonstige zum privaten Wohnbereich gehörende Flächen im Sinne von Absatz 1 Satz 5 über das nach den berechtigten Wohnbedürfnissen und den örtlichen Gegebenheiten sich ergebende Maß in den Bereich der freien Landschaft hinein ausgedehnt werden und nach dieser Ausdehnung nicht mehr der freien Landschaft zuzurechnen sind, bleibt das Betretungsrecht nach Absatz 1 hiervon unberührt.
Die Absätze 1 bis 5 gelten entsprechend.

#### § 23  
Zulässigkeit von Sperren

(1) Die Ausübung der Betretungsbefugnis gemäß § 22 kann durch den Grundstückseigentümer oder die Grundstückseigentümerin oder die jeweiligen Nutzungsberechtigten untersagt oder tatsächlich ausgeschlossen werden (Sperrung).
Die Sperrung bedarf der vorherigen Genehmigung.
Die Genehmigung ist nicht erforderlich für die Errichtung und Unterhaltung ortsüblicher Weidezäune oder solcher Zäune, die zum Schutz von landwirtschaftlichen Nutztieren vor Wölfen errichtet und unterhalten werden.

(2) Die Genehmigung ist zu erteilen, wenn andernfalls die zulässige Nutzung der Fläche oder des Weges unzumutbar behindert oder eingeschränkt würde oder erhebliche Schäden entstehen würden.
Im Übrigen darf die Genehmigung nur erteilt werden, wenn hierfür ein wichtiger Grund vorliegt und die Sperrung unter Berücksichtigung des Interesses der Allgemeinheit vertretbar ist.
Die Genehmigung soll widerruflich oder befristet erteilt werden.

(3) Zur Wahrung überwiegender Interessen der Allgemeinheit, insbesondere aus wichtigen Gründen des Naturschutzes, kann die zuständige Naturschutzbehörde eine Fläche oder einen Weg von Amts wegen sperren.

#### § 24  
Satzungsermächtigung zur Umsetzung von Erholungskonzepten, Durchgänge

(1) Zum Zwecke der Erholung der Allgemeinheit in Natur und Landschaft können die Gemeinden ein Freiraum- und Erholungskonzept als Satzung beschließen (Erholungssatzung).
In der Satzung können zu dem genannten Zweck festgesetzt werden:

1.  Flächen zur Errichtung und Nutzung von öffentlichen oder privaten Erholungs- und Grünanlagen, Fuß-, Wander-, Rad- und Reitwegen sowie landschaftsgebundenen Sportanlagen,
2.  Betretungsrechte auf Flächen, die nicht dem allgemeinen Betretungsrecht nach § 22 unterliegen,
    
3.  Badestellen, Liegewiesen, Rastplätze,
    
4.  Benutzungsbeschränkungen hinsichtlich der zu berücksichtigenden öffentlichen und privaten Belange,
    
5.  Kennzeichnungen von Wanderwegen, Radwanderwegen, Reitwegen durch Anbringung von Markierungen und Wegweisern.
    

Bei der Aufstellung der Erholungssatzung sind die öffentlichen und privaten Belange gegeneinander und untereinander abzuwägen.

(2) Die Erholungssatzung wird als einfacher Grünordnungsplan aufgestellt.
§ 5 Absatz 1, 3 und 4 gilt entsprechend.
Abweichend von § 5 Absatz 3 kann das Verfahren auch nach den Bestimmungen über das vereinfachte Verfahren in § 13 Absatz 2 des Baugesetzbuches durchgeführt werden.

(3) In der Erholungssatzung kann vorgesehen werden, dass den Gemeinden zur Sicherung und Umsetzung von Zielen und Zwecken der Satzung ein Vorkaufsrecht an bestimmten Grundstücken zusteht.

(4) Der Grundstückseigentümer, die Grundstückseigentümerin oder sonstige Berechtigte müssen auf einem Grundstück, das nach § 22 nicht frei betreten werden kann, für die Allgemeinheit einen Durchgang offen halten, wenn andere Teile der freien Landschaft, insbesondere Erholungsflächen, Wald oder Gewässer, in anderer zumutbarer Weise nicht zu erreichen sind, und wenn sie dadurch in sinngemäßer Anwendung der Grundsätze des § 23 Absatz 2 nicht übermäßig in ihren Rechten beeinträchtigt werden.

### Abschnitt 8  
Eigentumsbindung, Befreiungen

#### § 25  
Duldungspflicht (zu § 65 BNatSchG)

(1) Die Naturschutzbehörde hat auf Antrag den Eigentümern und Eigentümerinnen oder den Nutzungsberechtigten zu gestatten, selbst für die Durchführung der Maßnahmen im Sinne des § 65 Absatz 1 des Bundesnaturschutzgesetzes zu sorgen.

(2) Absatz 1 gilt auch für Maßnahmen, die zum Schutz, zur Pflege und zur Entwicklung der nach § 18 Absatz 1 dieses Gesetzes und § 30 des Bundesnaturschutzgesetzes gesetzlich geschützten Biotope oder anderer Teile von Natur und Landschaft besonders angeordnet worden sind.

(3) Bedienstete und Beauftragte der Naturschutzbehörden dürfen zur Wahrnehmung ihrer Aufgaben Grundstücke mit Ausnahme von Haus- und Gartengrundstücken betreten und dort nach rechtzeitiger Ankündigung auch Vermessungen, Bodenuntersuchungen und ähnliche Arbeiten durchführen.
Einer Ankündigung bedarf es nicht bei der Wahrnehmung der Aufgabe aus § 6 des Bundesnaturschutzgesetzes.

#### § 26  
Vorkaufsrecht

(1) Dem Land steht ein Vorkaufsrecht beim Kauf von Grundstücken zu, die ganz oder teilweise in Nationalparks, Naturschutzgebieten oder Gebieten liegen, die als Naturschutzgebiet einstweilig sichergestellt sind.
Satz 1 gilt auch für Grundstücke, die als künftiges Naturschutzgebiet einer Veränderungssperre nach § 9 Absatz 2 Satz 3 in Verbindung mit § 22 Absatz 3 Satz 3 des Bundesnaturschutzgesetzes unterliegen.
Das Vorkaufsrecht ist ausgeschlossen, wenn der Eigentümer oder die Eigentümerin das Grundstück an den Ehepartner oder die Ehepartnerin, eingetragenen Lebenspartner oder eingetragene Lebenspartnerin oder einen Verwandten oder eine Verwandte ersten Grades veräußert.
Das Vorkaufsrecht steht dem Land nicht zu bei einem Kauf von Rechten nach dem Wohnungseigentumsgesetz oder von Erbbaurechten.

(2) Das Vorkaufsrecht darf nur ausgeübt werden, wenn das Grundstück für den Naturschutz, die Landschaftspflege oder die naturnahe Erholung verwendet werden soll.
Die vorgesehene Verwendung ist bei der Ausübung des Vorkaufsrechts anzugeben.

(3) Wird die Ausübung des Vorkaufsrechtes auf Teilflächen beschränkt, kann der Eigentümer oder die Eigentümerin verlangen, dass sich der Vorkauf auf das gesamte Grundstück erstreckt, wenn die Restfläche wirtschaftlich nicht mehr in zumutbarer Weise verwertbar ist.

(4) Das Vorkaufsrecht wird durch die Fachbehörde für Naturschutz und Landschaftspflege geltend gemacht, der gegenüber auch die Mitteilung gemäß § 469 des Bürgerlichen Gesetzbuches abzugeben ist.
Das Vorkaufsrecht des Landes geht rechtsgeschäftlichen Vorkaufsrechten im Range vor und tritt hinter Vorkaufsrechten aufgrund öffentlichen Bundesrechts zurück; es bedarf nicht der Eintragung in das Grundbuch.
§ 66 des Bundesnaturschutzgesetzes findet keine Anwendung.
Die §§ 463 bis 469, 471, 1098 Absatz 2 und die §§ 1099 bis 1102 des Bürgerlichen Gesetzbuches gelten entsprechend.
Die Fachbehörde für Naturschutz und Landschaftspflege kann bereits vor dem Verkauf eines Grundstücks oder eines Teils davon erklären, dass sie das Vorkaufsrecht nicht ausüben wird; eine solche Erklärung gilt nur innerhalb von zwei Jahren nach ihrem Zugang.

(5) Das Vorkaufsrecht kann vom Land auf Antrag zugunsten von Körperschaften des öffentlichen Rechts und von anerkannten Naturschutzvereinigungen ausgeübt werden.
Dazu muss die Zahlung des Kaufpreises sichergestellt sein; insbesondere kann das Land vom Begünstigten die Leistung einer Sicherheit verlangen.
Die Naturschutzziele sind durch Eintragung einer beschränkten persönlichen Dienstbarkeit zugunsten des Landes Brandenburg im Grundbuch dauerhaft zu sichern; der Begünstigte ist verpflichtet, deren Eintragung zuzustimmen.
Liegen mehrere Anträge vor, so haben Anträge von Gemeinden Vorrang.
Mit der Ausübung des Vorkaufsrechtes kommt der Kauf zwischen dem Begünstigten und dem Verpflichteten zustande.

(6) Das Vorkaufsrecht kann vom Land auch zugunsten von Vereinen oder Stiftungen, die sich nach ihrer Satzung überwiegend dem Naturschutz und der Landschaftspflege im Land oder Teilen des Landes Brandenburg widmen und aufgrund ihrer bisherigen Tätigkeit Gewähr für eine sachgerechte Förderung der Ziele des Naturschutzes und der Landschaftspflege bieten, ausgeübt werden, wenn der Begünstigte dem schriftlich zugestimmt hat.
Der Eigentumserwerb muss im Zusammenhang mit einem Naturschutzprojekt des Vereins oder der Stiftung stehen.
Absatz 5 Satz 2 bis 5 gilt entsprechend.

#### § 27  
Enteignung (zu § 68 Absatz 3 BNatSchG)

(1) Nach diesem Gesetz können Grundstücke enteignet werden,

1.  die in Nationalparks oder Naturschutzgebieten liegen,
2.  auf denen sich ein Naturdenkmal befindet,
    
3.  um besonders geeignete Grundstücke, insbesondere die Ufer von Seen und Flüssen, für die naturverträgliche Erholung der Allgemeinheit in Natur und Landschaft nutzbar zu machen oder
    
4.  um Maßnahmen des Naturschutzes und der Landschaftspflege aufgrund dieses Gesetzes und des Bundesnaturschutzgesetzes durchzuführen.
    

Die Enteignung ist nur zulässig, wenn sie aus Gründen des Naturschutzes, der Landschaftspflege oder der Erholungsvorsorge erforderlich ist, die entsprechende Nutzung durch den Eigentümer nicht gewährleistet und der Enteignungszweck auf andere zumutbare Weise nicht erreicht werden kann.

(2) Die Enteignung ist zugunsten des Landes oder einer anderen Körperschaft des öffentlichen Rechts zulässig.

(3) Im Übrigen gilt das Enteignungsgesetz des Landes Brandenburg.

#### § 28  
Entschädigung für Nutzungsbeschränkungen (zu § 68 BNatSchG)

(1) Der Anspruch auf eine angemessene Entschädigung im Sinne des § 68 Absatz 1 des Bundesnaturschutzgesetzes richtet sich gegen das Land oder gegen die für die Maßnahme verantwortliche Körperschaft des öffentlichen Rechts.

(2) Das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung festzulegen, dass Eigentümern und Eigentümerinnen oder Nutzungsberechtigten, denen durch dieses Gesetz oder das Bundesnaturschutzgesetz oder Maßnahmen aufgrund dieser Gesetze die bestehende land-, forst- oder fischereiwirtschaftliche Bewirtschaftung einer Fläche wesentlich erschwert wird, ohne dass eine Entschädigung nach § 68 des Bundesnaturschutzgesetzes zu gewähren ist, auf Antrag ein angemessener Geldausgleich nach Maßgabe des Haushalts gezahlt werden kann.

#### § 29  
Ausnahmen, Befreiungen (zu § 67 BNatSchG)

(1) Befreiungen nach § 67 des Bundesnaturschutzgesetzes von den Vorschriften eines Nationalparkgesetzes sowie einer Rechtsverordnung zur Festsetzung eines Naturschutzgebietes oder Landschaftsschutzgebietes in Biosphärenreservaten und in Naturparken ergehen im Benehmen mit der Großschutzgebietsverwaltung nach § 32 Absatz 1, soweit die untere Naturschutzbehörde für die Entscheidung über die Befreiung zuständig ist.
Das Benehmen ist innerhalb eines Monats herzustellen.

(2) Soweit die untere Naturschutzbehörde für die Entscheidung über Ausnahmen und Befreiungen nach diesem Gesetz oder dem Bundesnaturschutzgesetz für das Gebiet von Nationalparken oder Biosphärenreservaten zuständig ist, trifft diese Entscheidung der jeweils örtlich zuständige Landrat oder die zuständige Landrätin als allgemeine untere Landesbehörde.

(3) Soweit die zuständige Naturschutzbehörde eine Befreiung nach § 67 des Bundesnaturschutzgesetzes ablehnt, hat sie zugleich darüber zu entscheiden, ob dem Antragsteller dem Grunde nach eine Entschädigung gemäß § 68 Absatz 1 des Bundesnaturschutzgesetzes zusteht.

(4) Einer Befreiung bedarf es abweichend von § 28 Absatz 2 des Bundesnaturschutzgesetzes nicht für Maßnahmen, die der Feststellung oder Beseitigung einer von einem Naturdenkmal ausgehenden Gefahr dienen.
Die Maßnahmen sind der zuständigen Naturschutzbehörde spätestens drei Werktage vor der Durchführung, bei gegenwärtiger, erheblicher Gefahr unverzüglich, anzuzeigen.

### Abschnitt 9  
Behördlicher und ehrenamtlicher Naturschutz, Zuständigkeiten

#### Unterabschnitt 1  
Behörden und Naturschutzfonds

#### § 30  
Naturschutzbehörden, Aufgaben, Befugnisse, Zuständigkeiten (zu § 3 Absatz 1 BNatSchG)

(1) Naturschutzbehörden im Sinne dieses Gesetzes und § 3 Absatz 1 des Bundesnaturschutzgesetzes sind das für Naturschutz und Landschaftspflege zuständige Ministerium als oberste Naturschutzbehörde, das Landesamt für Umwelt als Fachbehörde für Naturschutz und Landschaftspflege und die Landkreise und kreisfreien Städte als untere Naturschutzbehörden.
Die Naturschutzbehörden sind Sonderordnungsbehörden.

(2) Den Naturschutzbehörden obliegt auch die Durchführung dieses Gesetzes und der auf seiner Grundlage erlassenen Rechtsvorschriften.
Sie haben darüber zu wachen, dass die Rechtsvorschriften dieses Gesetzes und der auf seiner Grundlage erlassenen Rechtsvorschriften eingehalten werden und können nach pflichtgemäßem Ermessen die im Einzelfall erforderlichen Maßnahmen treffen, um deren Einhaltung sicherzustellen.
Der Fachbehörde für Naturschutz und Landschaftspflege obliegt darüber hinaus die Unterstützung der obersten Naturschutzbehörde, insbesondere bei ihren Aufgaben nach den Abschnitten 2 und 4 sowie die fachliche Beratung und Unterstützung der unteren Naturschutzbehörden.
Die Naturschutzbehörden können nach pflichtgemäßem Ermessen Maßnahmen zum Schutz von Natur und Landschaft im Sinne des § 65 Absatz 1 des Bundesnaturschutzgesetzes durchführen oder anordnen.

(3) Soweit die Gemeinden auf der Grundlage dieses Gesetzes Satzungen erlassen, obliegt ihnen die Durchführung der Satzungen.
Absatz 2 Satz 2 und § 25 Absatz 1 und 3 dieses Gesetzes sowie § 65 Absatz 1 des Bundesnaturschutzgesetzes gelten entsprechend.
Soweit die Gemeinden nach Satz 1 zuständig sind, sind sie auch die Verwaltungsbehörde im Sinne des § 36 Absatz 1 Nummer 1 des Gesetzes über Ordnungswidrigkeiten.

(4) Das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung die Zuständigkeit für die Durchführung dieses Gesetzes, des Bundesnaturschutzgesetzes und der auf ihrer Grundlage erlassenen Rechtsvorschriften sowie für die in die Zuständigkeit des Landes fallenden Handlungen und Maßnahmen, die sich aus internationalen Verträgen auf dem Gebiet des Naturschutzes ergeben, zu regeln, soweit in diesem Gesetz nicht bereits etwas anderes bestimmt ist.
Die Rechtsverordnung kann auch eine Bestimmung zur Deckung der Kosten im Sinne des Artikels 97 Absatz 3 der Verfassung des Landes Brandenburg treffen, soweit sie erforderlich ist.

(5) Rechtsverordnungen aufgrund dieses Gesetzes oder des Bundesnaturschutzgesetzes, die in die Rechte der Gemeinden eingreifen, ergehen im Einvernehmen mit dem für Inneres zuständigen Mitglied der Landesregierung.

#### § 31  
Unterrichtungs- und Weisungsrecht

Die Landkreise und kreisfreien Städte nehmen die Aufgaben der unteren Naturschutzbehörden als Pflichtaufgaben zur Erfüllung nach Weisung wahr.
Das für Naturschutz und Landschaftspflege zuständige Ministerium ist Sonderaufsichtsbehörde über die Landkreise und kreisfreien Städte.
Für die Sonderaufsichtsbehörde gilt § 121 Absatz 2 bis 4 der Kommunalverfassung des Landes Brandenburg entsprechend.
Das Recht, besondere Weisungen zu erteilen, ist nicht auf den Bereich der Gefahrenabwehr beschränkt.

#### § 32  
Verwaltung der Großschutzgebiete

(1) Das Landesamt für Umwelt als Fachbehörde für Naturschutz und Landschaftspflege verwaltet die Nationalparke, Naturparke und Biosphärenreservate.
Es hat die Aufgabe, Maßnahmen für deren Entwicklung und Pflege zu koordinieren und durchzuführen sowie diese Gebiete zu betreuen und die Einhaltung der jeweils geltenden Schutzbestimmungen zu überwachen.
Die Fachbehörde für Naturschutz und Landschaftspflege stellt für die pflege- und entwicklungsbedürftigen Bereiche dieser Gebiete Pflege- und Entwicklungspläne auf und schreibt sie fort.
Die Pflege- und Entwicklungspläne können in Natura 2000-Gebieten die Funktion von Bewirtschaftungsplänen im Sinne von § 32 Absatz 5 des Bundesnaturschutzgesetzes übernehmen.

(2) Zur Abstimmung der naturschutzfachlichen Aufgaben nach Absatz 1 Satz 2 und 3 mit den Belangen der Gemeinden und den anderen örtlich oder sachlich beteiligten Behörden und Verbänden wird für die Naturparke und Biosphärenreservate jeweils ein Kuratorium gebildet.
Die Einzelheiten seiner Zusammensetzung regelt das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung im Benehmen mit dem für Naturschutz und Landschaftspflege zuständigen Ausschuss des Landtages.

(3) Das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung die Zuständigkeit für die Verwaltung des Nationalparks Unteres Odertal im Sinne von Absatz 1 Satz 1 und 2 und für die Wahrnehmung der Befugnisse und Aufgaben der Nationalparkverwaltung Unteres Odertal auf eine Einrichtung nach § 13 des Landesorganisationsgesetzes zu übertragen.
Die Bestimmungen des § 13 Absatz 2 Satz 1 des Landesorganisationsgesetzes über die Errichtung einer Einrichtung bleiben unberührt.

#### § 33  
Naturschutzfonds

(1) Unter dem Namen „Naturschutzfonds Brandenburg“ besteht eine rechtsfähige Stiftung des öffentlichen Rechts.

(2) Die Stiftung hat den Zweck,

1.  Maßnahmen zum Schutz, zur Pflege und zur Entwicklung von Natur und Landschaft durchzuführen, zu fördern oder entsprechende vertragliche Vereinbarungen nach § 3 Absatz 3 des Bundesnaturschutzgesetzes abzuschließen,
2.  den Aufbau von Flächen- und Maßnahmenpools für die Eingriffsregelung vorzunehmen oder zu unterstützen,
    
3.  Grundstücke, die für den Naturschutz, die Landschaftspflege oder die Erholung besonders geeignet sind, zu erwerben, langfristig zu pachten oder den Erwerb oder die Anpachtung solcher Grundstücke durch andere geeignete Träger zu fördern,
    
4.  die Forschung und modellhafte Untersuchungen auf dem Gebiet des Naturschutzes und der Landschaftspflege einschließlich der modellhaften Neuanpflanzung von Alleen zu fördern,
    
5.  richtungsweisende Leistungen auf dem Gebiet des Naturschutzes und der Landschaftspflege auszuzeichnen und
    
6.  Öffentlichkeitsarbeit für den Naturschutz durchzuführen.
    

Die Stiftung verfolgt ausschließlich und unmittelbar steuerbegünstigte Zwecke im Sinne der §§ 51 bis 68 der Abgabenordnung.

(3) Die Stiftung erfüllt ihre Aufgaben gemäß Absatz 2 aus

1.  dem Ertrag des Stiftungsvermögens,
2.  zweckgebundenen Zuweisungen aus dem Landeshaushalt, insbesondere der Ersatzzahlung nach § 6 dieses Gesetzes und § 15 Absatz 6 des Bundesnaturschutzgesetzes, soweit der Zweck der Zuweisungen der Erfüllung der Aufgaben nach Absatz 2 nicht entgegensteht,
    
3.  Zuwendungen Dritter, insbesondere Erträgnissen von Lotterien, Ausspielungen, Veranstaltungen, Sammlungen sowie Spenden.
    

(4) Das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit dem für Finanzen zuständigen Mitglied der Landesregierung, unbebaute landeseigene Grundstücke, die für die Zwecke des Naturschutzes und der Landschaftspflege erworben oder sonst übertragen wurden und durch das für Naturschutz und Landschaftspflege zuständige Ministerium verwaltet werden, unentgeltlich auf die Stiftung Naturschutzfonds Brandenburg zu übertragen.
Die Stiftung ist sachlich von der Zahlung der Kosten (Gebühren und Auslagen) befreit, die die Gerichte für die Eintragung als Eigentümerin in das Grundbuch erheben.
Die übertragenen Grundflächen sind ihrer Zweckbestimmung gemäß zu erhalten.

(5) Der Naturschutzfonds Brandenburg wird durch den Stiftungsrat verwaltet.
Der Stiftungsrat besteht aus dem für Naturschutz und Landschaftspflege zuständigen Mitglied der Landesregierung oder seiner Vertretung und je einer Person des für Haushalt und Finanzen zuständigen Ministeriums, des für Wirtschaft zuständigen Ministeriums, des für Infrastruktur zuständigen Ministeriums, des für Landwirtschaft zuständigen Ministeriums, des für Wasserwirtschaft zuständigen Ministeriums sowie einer Person aus dem für Naturschutz und Landschaftspflege zuständigen Ausschuss des Landtages,  
zwei Personen des Beirats bei der obersten Naturschutzbehörde, einer von den vom Land anerkannten, landesweit tätigen Naturschutzvereinigungen gemeinsam vorgeschlagenen Person, einer von den landesweit tätigen allgemeinen Verbänden der Land-, Forst- und Fischereiwirtschaft gemeinsam vorgeschlagenen Person und einer Person der kommunalen Spitzenverbände Brandenburgs.
Das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung oder seine Vertretung übernimmt den Vorsitz.
Das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung beruft die Mitglieder des Stiftungsrates auf fünf Jahre und bestimmt einen Geschäftsführer oder eine Geschäftsführerin.
Die Nichtbesetzung von Plätzen im Stiftungsrat hat keine Auswirkung auf dessen Beschlussfähigkeit.
Bei Abstimmungen im Stiftungsrat gibt im Falle einer Stimmengleichheit die Stimme der oder des Stiftungsratsvorsitzenden den Ausschlag.

(6) Der Stiftungsrat beschließt eine Satzung, die der Genehmigung durch die Aufsichtsbehörde im Einvernehmen mit dem für Haushalt und Finanzen zuständigen Ministerium sowie dem Benehmen der für Naturschutz und Landschaftspflege sowie für Haushalt und Finanzen zuständigen Ausschüsse des Landtages bedarf.

(7) Die Stiftung steht unter der Aufsicht des Landes.
Die Aufsicht beschränkt sich darauf, die Rechtmäßigkeit der Verwaltung sicherzustellen.
Rechtsaufsichtsbehörde ist das für Naturschutz und Landschaftspflege zuständige Ministerium.

(8) Die Mitarbeiter und Mitarbeiterinnen der Naturwacht beim Naturschutzfonds Brandenburg haben bei der Wahrnehmung ihrer Aufgaben im Rahmen des Absatzes 2 Nummer 1 und 6 die Befugnisse von Naturschutzhelfern und -helferinnen nach § 34 Absatz 2 Satz 2.

#### Unterabschnitt 2  
Ehrenamtlicher Naturschutz

#### § 34  
Naturschutzhelfer und -helferinnen

(1) Zur Unterstützung bei der Durchführung dieses Gesetzes und des Bundesnaturschutzgesetzes können die unteren Naturschutzbehörden geeignete sachkundige Personen zu ehrenamtlichen Naturschutzhelfern und -helferinnen bestellen.

(2) Die Naturschutzhelfer und -helferinnen sollen die zuständigen Behörden über nachteilige Veränderungen in der Landschaft benachrichtigen und darauf hinwirken, dass Schäden von Natur und Landschaft abgewendet werden.
Zur Erfüllung dieser Aufgaben sind die Naturschutzhelfer und -helferinnen berechtigt,

1.  Grundstücke mit Ausnahme von Haus- und Gartengrundstücken zu betreten und Auskünfte einzuholen,
2.  Personen zur Feststellung ihrer Identität anzuhalten, bei denen ein begründeter Verdacht der Zuwiderhandlung gegen Rechtsvorschriften besteht, die den Schutz der Natur, die Pflege der Landschaft und die Erholung in der freien Natur regeln und deren Übertretung mit Strafe oder Geldbuße bedroht ist,
    
3.  eine Person vorübergehend vom Ort zu verweisen und ihr vorübergehend das Betreten des Ortes zu verbieten und
    
4.  unberechtigt entnommene Gegenstände, gehaltene oder erworbene Pflanzen und Tiere sowie solche Gegenstände sicherzustellen, die bei Zuwiderhandlungen gegen die Vorschriften dieses Gesetzes, des Bundesnaturschutzgesetzes oder der aufgrund dieser Gesetze erlassenen Rechtsverordnungen verwendet wurden oder verwendet werden sollten.
    

Sie müssen bei der Ausübung ihrer Tätigkeit einen Dienstausweis bei sich führen, der bei Vornahme einer Amtshandlung auf Verlangen vorzuzeigen ist.

#### § 35  
Naturschutzbeiräte

(1) Zur Vertretung der Belange von Naturschutz und Landschaftspflege und zur wissenschaftlichen und fachlichen Beratung werden bei der obersten Naturschutzbehörde und den unteren Naturschutzbehörden Naturschutzbeiräte gebildet.
Die Naturschutzbeiräte sollen

1.  die Naturschutzbehörden durch Vorschläge und Anregungen fachlich unterstützen,
2.  Fehlentwicklungen in Natur und Landschaft entgegenwirken und
    
3.  der Öffentlichkeit die Absichten und Ziele des Naturschutzes und der Landschaftspflege vermitteln.
    

Die Beiräte sind in die Vorbereitung aller wichtigen Entscheidungen und Maßnahmen der Naturschutzbehörde, insbesondere von Ausnahmegenehmigungen und Befreiungen, einzubeziehen, bei der sie eingerichtet sind.
Dies gilt auch bei einer diese Entscheidungen einschließenden oder ersetzenden und auf Landesrecht beruhenden Zulassung durch einen Landkreis oder eine kreisfreie Stadt.

(2) In die Beiräte sind Bürgerinnen und Bürger zu berufen, die im Naturschutz und der Landschaftspflege besonders fachkundig und erfahren sind.
Dabei ist auf die gleiche Teilhabe von Frauen und Männern zu achten, soweit nicht rechtliche oder tatsächliche Gründe entgegenstehen.
Die Mitglieder der Beiräte sind ehrenamtlich tätig.
Ihre Anzahl beträgt bei der obersten Naturschutzbehörde neun, bei den unteren Naturschutzbehörden sieben.
Die Beiräte wählen eine vorsitzende Person und geben sich eine Geschäftsordnung, bei Bedarf können sie zu ihren Beratungen Sachverständige hinzuziehen.
Die Beiräte bei den unteren Naturschutzbehörden werden in den Landkreisen durch den Landrat oder die Landrätin auf der Grundlage eines Beschlusses des Kreisausschusses, in den kreisfreien Städten durch den Oberbürgermeister oder die Oberbürgermeisterin auf der Grundlage eines Beschlusses des Hauptausschusses berufen.

(3) Das für Naturschutz und Landschaftspflege zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung das Nähere über die Berufung, Amtsdauer und Entschädigung der Beiratsmitglieder zu regeln.

#### § 36  
Mitwirkung von anerkannten Naturschutzvereinigungen (zu § 63 BNatSchG)

Einer vom Land anerkannten Naturschutzvereinigung, die nach ihrer Satzung landesweit tätig ist, ist über § 63 Absatz 2 des Bundesnaturschutzgesetzes hinaus auch Gelegenheit zur Stellungnahme und zur Einsicht in die einschlägigen Sachverständigengutachten zu geben

1.  vor der Entscheidung nach § 9 Absatz 6 Nummer 4 über die Zustimmung zu den Darstellungen oder Festsetzungen einer baulichen Nutzung in einem Bauleitplan im Bereich eines Landschaftsschutzgebietes,
2.  vor der Zulassung von Ausnahmen nach § 30 Absatz 3 des Bundesnaturschutzgesetzes und § 17 Absatz 2 dieses Gesetzes sowie nach § 45 Absatz 7 des Bundesnaturschutzgesetzes,
    
3.  vor der Erteilung von Befreiungen nach § 67 des Bundesnaturschutzgesetzes mit Ausnahme des § 39 Absatz 5 des Bundesnaturschutzgesetzes und § 19 dieses Gesetzes,
    
4.  vor der Erteilung von Zulassungen aufgrund anderer Landesgesetze, wenn diese Entscheidungen nach den Nummern 1 bis 3 sowie § 63 Absatz 2 Nummer 5 des Bundesnaturschutzgesetzes einschließen oder ersetzen, mit Ausnahme der in § 63 Absatz 2 Nummer 6 und 7 des Bundesnaturschutzgesetzes genannten Verfahren,
    

soweit sie durch das Vorhaben in ihrem satzungsgemäßen Aufgabenbereich betroffen sind.

#### § 37  
Klagebefugnis von Naturschutzvereinigungen (zu § 64 BNatSchG)

(1) Rechtsbehelfe im Sinne des § 64 des Bundesnaturschutzgesetzes können auch gegen die in § 36 genannten Entscheidungen eingelegt werden.

(2) Absatz 1 gilt entsprechend, wenn anstelle der dort und in § 63 Absatz 2 des Bundesnaturschutzgesetzes genannten Verwaltungsakte andere Verwaltungsakte erlassen worden sind, für die dieses Gesetz oder das Bundesnaturschutzgesetz eine Mitwirkung der anerkannten Naturschutzvereinigungen nicht vorsehen.

#### § 38  
Datenverarbeitung

Die nach diesem Gesetz zuständigen Behörden und Einrichtungen dürfen im Rahmen

1.  der Biotoperfassung,
2.  der Unterschutzstellung von Landschafts- und Naturschutzgebieten,
    
3.  der Beobachtung von Natur und Landschaft im Sinne des § 6 des Bundesnaturschutzgesetzes
    

personen- und betriebsbezogene Daten erheben und übermitteln.
Die betroffene Person ist verpflichtet, den in Satz 1 genannten Stellen auf Verlangen die erforderlichen Auskünfte zu erteilen; hierauf ist sie hinzuweisen.
Eine Erhebung, Speicherung oder Übermittlung ist auch ohne Kenntnis der betroffenen Person zulässig, wenn andernfalls die Erfüllung der Aufgaben nach diesem Gesetz und dem Bundesnaturschutzgesetz gefährdet wäre.
Für die Verarbeitung personenbezogener Daten gilt im Übrigen das Brandenburgische Datenschutzgesetz.

### Abschnitt 10  
Ordnungswidrigkeiten

#### § 39  
Verstöße gegen Bestimmungen der Naturschutzgesetze (zu § 69 BNatSchG)

(1) Über die Bestimmung in § 69 des Bundesnaturschutzgesetzes hinaus handelt auch ordnungswidrig, wer vorsätzlich oder fahrlässig

1.  entgegen § 23 Absatz 2 des Bundesnaturschutzgesetzes in einem Naturschutzgebiet Handlungen vornimmt, die das Gebiet oder einzelne seiner Bestandteile zerstören, beschädigen, verändern oder nachhaltig stören können,
2.  entgegen § 26 Absatz 2 des Bundesnaturschutzgesetzes in einem Landschaftsschutzgebiet Handlungen vornimmt, die den Charakter des Gebietes verändern oder dem besonderen Schutzzweck zuwiderlaufen,
    
3.  entgegen § 28 Absatz 2 des Bundesnaturschutzgesetzes ein Naturdenkmal beseitigt oder Handlungen vornimmt, die zu einer Zerstörung, Beschädigung oder Veränderung eines Naturdenkmals führen können,
    
4.  entgegen § 29 Absatz 2 des Bundesnaturschutzgesetzes einen geschützten Landschaftsbestandteil beseitigt oder Handlungen vornimmt, die zu einer Zerstörung, Beschädigung oder Veränderung eines geschützten Landschaftsbestandteils führen können,
    
5.  entgegen § 13 eine der dort genannten Bezeichnungen führt oder Kennzeichnungen im Sinne des § 13 verändert oder entfernt,
    
6.  entgegen § 17 Absatz 1 eine Allee beseitigt, zerstört oder sonst erheblich oder nachhaltig beeinträchtigt,
    
7.  entgegen § 30 Absatz 2 des Bundesnaturschutzgesetzes ein in § 18 Absatz 1 genanntes gesetzlich geschütztes Biotop zerstört oder sonst erheblich beeinträchtigt,
    
8.  einem der Verbote zum Schutz der Horststandorte nach § 19 Absatz 1 zuwiderhandelt,
    
9.  entgegen den §§ 22, 23 anderen den Zutritt zu einem Grundstück verwehrt oder ein Grundstück ohne die erforderliche Genehmigung sperrt,
    
10.  entgegen § 22 Absatz 3 auf Sport- und Lehrpfaden oder auf Wegen, die nicht mit zwei- oder mehrspurigen Fahrzeugen befahren werden können, reitet oder mit bespannten Fahrzeugen fährt oder auf Sport- und Lehrpfaden, auf Wegen und Pfaden oder auf Flächen außerhalb von Wegen mit motorisierten Fahrzeugen fährt,
    
11.  entgegen § 22 Absatz 4 in der freien Landschaft ein Zelt oder eine sonstige bewegliche Unterkunft (Wohnwagen) aufstellt.
    

(2) Ordnungswidrig handelt ferner, wer vorsätzlich oder fahrlässig

1.  einer aufgrund dieses Gesetzes oder des Bundesnaturschutzgesetzes erlassenen vollziehbaren schriftlichen Anordnung, die auf diese Bußgeldvorschrift verweist, zuwiderhandelt oder
2.  einer aufgrund dieses Gesetzes erlassenen Rechtsverordnung oder Satzung zuwiderhandelt, soweit sie für bestimmte Tatbestände auf diese Bußgeldvorschrift verweist.

#### § 40  
Geldbuße

Ordnungswidrigkeiten nach § 69 des Bundesnaturschutzgesetzes und § 39 können mit einer Geldbuße bis zu dreizehntausend Euro, in den Fällen des § 39 Absatz 1 Nummer 1, 3, 4 und 6 sowie Absatz 2 Nummer 2 und § 69 Absatz 3 Nummer 1, 2, 3, 5 und 26 des Bundesnaturschutzgesetzes bis zu fünfundsechzigtausend Euro geahndet werden.

#### § 41  
Einziehung

Ist eine Ordnungswidrigkeit nach diesem Gesetz begangen worden, so können Gegenstände, auf die sich die Ordnungswidrigkeit bezieht oder die zu ihrer Begehung oder Vorbereitung gebraucht worden oder bestimmt gewesen sind, eingezogen werden.
§ 23 des Gesetzes über Ordnungswidrigkeiten ist anzuwenden.

### Abschnitt 11  
Übergangs- und Schlussbestimmungen

#### § 42  
Fortgeltung von Rechtsverordnungen

(1) Die aufgrund des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S.
350), das zuletzt durch Artikel 2 Absatz 9 des Gesetzes vom 15. Juli 2010 (GVBl.
I Nr. 28 S.
3) geändert worden ist, erlassenen Rechtsverordnungen bleiben in Kraft, sofern sie nicht ausdrücklich aufgehoben werden oder ihre Geltungsdauer abläuft.
Für ihre Aufhebung und Änderung gelten die Zuständigkeits- und Verfahrensvorschriften dieses Gesetzes entsprechend.
Soweit in ihnen auf § 73 Absatz 2 des Brandenburgischen Naturschutzgesetzes in der Fassung der Bekanntmachung vom 26.
Mai 2004 (GVBl.
I S.
350), das zuletzt durch Artikel 2 Absatz 9 des Gesetzes vom 15.
Juli 2010 (GVBl.
I Nr.
28 S.
3) geändert worden ist, Bezug genommen wird, gilt § 39 Absatz 2 entsprechend.

(2) Die nach Artikel 6 § 8 des Umweltrahmengesetzes übergeleiteten und die nach Artikel 6 § 3 des Umweltrahmengesetzes in Verbindung mit den §§ 12 bis 18 des Bundesnaturschutzgesetzes in der bei Inkrafttreten des Umweltrahmengesetzes geltenden Fassung erlassenen Vorschriften bleiben, sofern sie nicht befristet sind, bis zu einer anderweitigen Regelung in Kraft.
Für die Durchführung der weitergeltenden Vorschriften gelten die §§ 25 und 28 dieses Gesetzes sowie die §§ 65 und 68 des Bundesnaturschutzgesetzes entsprechend.
Für ihre Aufhebung und Änderung gelten die Zuständigkeitsvorschriften für den Erlass von Rechtsverordnungen gemäß § 8 Absatz 1 nach der aufgrund des § 30 Absatz 4 erlassenen Verordnung über die Zuständigkeit der Naturschutzbehörden; § 39 Absatz 2 Nummer 2 findet entsprechende Anwendung.

(3) Behandlungsrichtlinien und Landschaftspflegepläne, die nach § 19 der Naturschutzverordnung vom 18.
Mai 1989 (GBl.
I Nr.
12 S.
159) erlassen oder nach Artikel 8 des Umweltrahmengesetzes übergeleitet worden sind, bleiben bis zu einer anderweitigen Regelung in Kraft, soweit sie nicht den Vorschriften dieses Gesetzes oder des Bundesnaturschutzgesetzes widersprechen.
Für ihre Durchführung gelten § 65 Absatz 1 des Bundesnaturschutzgesetzes und § 25 Absatz 1.

(4) Soweit in den nach den Absätzen 1 bis 3 fortgeltenden Rechtsverordnungen auf außer Kraft getretene oder tretende Rechtsvorschriften verwiesen wird, treten die entsprechenden Vorschriften des Bundesnaturschutzgesetzes und dieses Gesetzes oder die entsprechenden aufgrund dieser Gesetze erlassenen Vorschriften an deren Stelle.

### Anlagen

1

[NatSchAG-Anlg-1](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%201.pdf "NatSchAG-Anlg-1") 1.0 MB

2

[NatSchAG-Anlg-2](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%202.pdf "NatSchAG-Anlg-2") 8.8 MB

3

[NatSchAG-Anlg-3-Blatt-2546](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2546.pdf "NatSchAG-Anlg-3-Blatt-2546") 3.7 MB

4

[NatSchAG-Anlg-3-Blatt-2548](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2548.pdf "NatSchAG-Anlg-3-Blatt-2548") 3.9 MB

5

[NatSchAG-Anlg-3-Blatt-2550](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2550.pdf "NatSchAG-Anlg-3-Blatt-2550") 3.6 MB

6

[NatSchAG-Anlg-3-Blatt-2734](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2734.pdf "NatSchAG-Anlg-3-Blatt-2734") 3.7 MB

7

[NatSchAG-Anlg-3-Blatt-2736](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2736.pdf "NatSchAG-Anlg-3-Blatt-2736") 3.4 MB

8

[NatSchAG-Anlg-3-Blatt-2738](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2738.pdf "NatSchAG-Anlg-3-Blatt-2738") 3.4 MB

9

[NatSchAG-Anlg-3-Blatt-2744](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2744.pdf "NatSchAG-Anlg-3-Blatt-2744") 4.5 MB

10

[NatSchAG-Anlg-3-Blatt-2746](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2746.pdf "NatSchAG-Anlg-3-Blatt-2746") 4.6 MB

11

[NatSchAG-Anlg-3-Blatt-2748](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2748.pdf "NatSchAG-Anlg-3-Blatt-2748") 4.2 MB

12

[NatSchAG-Anlg-3-Blatt-2750](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2750.pdf "NatSchAG-Anlg-3-Blatt-2750") 3.9 MB

13

[NatSchAG-Anlg-3-Blatt-2752](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2752.pdf "NatSchAG-Anlg-3-Blatt-2752") 4.9 MB

14

[NatSchAG-Anlg-3-Blatt-2932](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2932.pdf "NatSchAG-Anlg-3-Blatt-2932") 4.1 MB

15

[NatSchAG-Anlg-3-Blatt-2934](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2934.pdf "NatSchAG-Anlg-3-Blatt-2934") 4.1 MB

16

[NatSchAG-Anlg-3-Blatt-2938](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2938.pdf "NatSchAG-Anlg-3-Blatt-2938") 3.7 MB

17

[NatSchAG-Anlg-3-Blatt-2942](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2942.pdf "NatSchAG-Anlg-3-Blatt-2942") 4.0 MB

18

[NatSchAG-Anlg-3-Blatt-2944](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2944.pdf "NatSchAG-Anlg-3-Blatt-2944") 4.6 MB

19

[NatSchAG-Anlg-3-Blatt-2946](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2946.pdf "NatSchAG-Anlg-3-Blatt-2946") 4.4 MB

20

[NatSchAG-Anlg-3-Blatt-2948](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2948.pdf "NatSchAG-Anlg-3-Blatt-2948") 4.7 MB

21

[NatSchAG-Anlg-3-Blatt-2950](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2950.pdf "NatSchAG-Anlg-3-Blatt-2950") 4.4 MB

22

[NatSchAG-Anlg-3-Blatt-2952](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-2952.pdf "NatSchAG-Anlg-3-Blatt-2952") 1.9 MB

23

[NatSchAG-Anlg-3-Blatt-3136](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3136.pdf "NatSchAG-Anlg-3-Blatt-3136") 3.6 MB

24

[NatSchAG-Anlg-3-Blatt-3138](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3138.pdf "NatSchAG-Anlg-3-Blatt-3138") 3.8 MB

25

[NatSchAG-Anlg-3-Blatt-3140](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3140.pdf "NatSchAG-Anlg-3-Blatt-3140") 3.5 MB

26

[NatSchAG-Anlg-3-Blatt-3142](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3142.pdf "NatSchAG-Anlg-3-Blatt-3142") 3.7 MB

27

[NatSchAG-Anlg-3-Blatt-3144](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3144.pdf "NatSchAG-Anlg-3-Blatt-3144") 3.9 MB

28

[NatSchAG-Anlg-3-Blatt-3146](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3146.pdf "NatSchAG-Anlg-3-Blatt-3146") 4.3 MB

29

[NatSchAG-Anlg-3-Blatt-3148](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3148.pdf "NatSchAG-Anlg-3-Blatt-3148") 4.9 MB

30

[NatSchAG-Anlg-3-Blatt-3150](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3150.pdf "NatSchAG-Anlg-3-Blatt-3150") 5.0 MB

31

[NatSchAG-Anlg-3-Blatt-3338](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3338.pdf "NatSchAG-Anlg-3-Blatt-3338") 3.9 MB

32

[NatSchAG-Anlg-3-Blatt-3340](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3340.pdf "NatSchAG-Anlg-3-Blatt-3340") 3.9 MB

33

[NatSchAG-Anlg-3-Blatt-3342](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3342.pdf "NatSchAG-Anlg-3-Blatt-3342") 3.8 MB

34

[NatSchAG-Anlg-3-Blatt-3344](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3344.pdf "NatSchAG-Anlg-3-Blatt-3344") 4.5 MB

35

[NatSchAG-Anlg-3-Blatt-3346](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3346.pdf "NatSchAG-Anlg-3-Blatt-3346") 4.6 MB

36

[NatSchAG-Anlg-3-Blatt-3348](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3348.pdf "NatSchAG-Anlg-3-Blatt-3348") 3.9 MB

37

[NatSchAG-Anlg-3-Blatt-3350](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3350.pdf "NatSchAG-Anlg-3-Blatt-3350") 4.0 MB

38

[NatSchAG-Anlg-3-Blatt-3352](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3352.pdf "NatSchAG-Anlg-3-Blatt-3352") 4.0 MB

39

[NatSchAG-Anlg-3-Blatt-3538](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3538.pdf "NatSchAG-Anlg-3-Blatt-3538") 3.8 MB

40

[NatSchAG-Anlg-3-Blatt-3540](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3540.pdf "NatSchAG-Anlg-3-Blatt-3540") 4.4 MB

41

[NatSchAG-Anlg-3-Blatt-3542](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3542.pdf "NatSchAG-Anlg-3-Blatt-3542") 4.3 MB

42

[NatSchAG-Anlg-3-Blatt-3544](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3544.pdf "NatSchAG-Anlg-3-Blatt-3544") 5.1 MB

43

[NatSchAG-Anlg-3-Blatt-3548](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3548.pdf "NatSchAG-Anlg-3-Blatt-3548") 4.3 MB

44

[NatSchAG-Anlg-3-Blatt-3550](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3550.pdf "NatSchAG-Anlg-3-Blatt-3550") 4.1 MB

45

[NatSchAG-Anlg-3-Blatt-3552](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3552.pdf "NatSchAG-Anlg-3-Blatt-3552") 3.9 MB

46

[NatSchAG-Anlg-3-Blatt-3738](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3738.pdf "NatSchAG-Anlg-3-Blatt-3738") 3.5 MB

47

[NatSchAG-Anlg-3-Blatt-3740](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3740.pdf "NatSchAG-Anlg-3-Blatt-3740") 3.8 MB

48

[NatSchAG-Anlg-3-Blatt-3742](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3742.pdf "NatSchAG-Anlg-3-Blatt-3742") 4.2 MB

49

[NatSchAG-Anlg-3-Blatt-3744](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3744.pdf "NatSchAG-Anlg-3-Blatt-3744") 5.0 MB

50

[NatSchAG-Anlg-3-Blatt-3746](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3746.pdf "NatSchAG-Anlg-3-Blatt-3746") 4.3 MB

51

[NatSchAG-Anlg-3-Blatt-3748](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3748.pdf "NatSchAG-Anlg-3-Blatt-3748") 4.0 MB

52

[NatSchAG-Anlg-3-Blatt-3752](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3752.pdf "NatSchAG-Anlg-3-Blatt-3752") 4.2 MB

53

[NatSchAG-Anlg-3-Blatt-3754](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3754.pdf "NatSchAG-Anlg-3-Blatt-3754") 1.9 MB

54

[NatSchAG-Anlg-3-Blatt-3938](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3938.pdf "NatSchAG-Anlg-3-Blatt-3938") 3.1 MB

55

[NatSchAG-Anlg-3-Blatt-3940](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3940.pdf "NatSchAG-Anlg-3-Blatt-3940") 3.5 MB

56

[NatSchAG-Anlg-3-Blatt-3942](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3942.pdf "NatSchAG-Anlg-3-Blatt-3942") 3.5 MB

57

[NatSchAG-Anlg-3-Blatt-3944](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3944.pdf "NatSchAG-Anlg-3-Blatt-3944") 3.9 MB

58

[NatSchAG-Anlg-3-Blatt-3946](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3946.pdf "NatSchAG-Anlg-3-Blatt-3946") 4.1 MB

59

[NatSchAG-Anlg-3-Blatt-3948](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3948.pdf "NatSchAG-Anlg-3-Blatt-3948") 4.2 MB

60

[NatSchAG-Anlg-3-Blatt-3952](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3952.pdf "NatSchAG-Anlg-3-Blatt-3952") 4.3 MB

61

[NatSchAG-Anlg-3-Blatt-3954](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-3954.pdf "NatSchAG-Anlg-3-Blatt-3954") 4.4 MB

62

[NatSchAG-Anlg-3-Blatt-4144](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4144.pdf "NatSchAG-Anlg-3-Blatt-4144") 3.1 MB

63

[NatSchAG-Anlg-3-Blatt-4146](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4146.pdf "NatSchAG-Anlg-3-Blatt-4146") 3.5 MB

64

[NatSchAG-Anlg-3-Blatt-4148](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4148.pdf "NatSchAG-Anlg-3-Blatt-4148") 4.0 MB

65

[NatSchAG-Anlg-3-Blatt-4150](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4150.pdf "NatSchAG-Anlg-3-Blatt-4150") 4.1 MB

66

[NatSchAG-Anlg-3-Blatt-4152](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4152.pdf "NatSchAG-Anlg-3-Blatt-4152") 4.2 MB

67

[NatSchAG-Anlg-3-Blatt-4154](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4154.pdf "NatSchAG-Anlg-3-Blatt-4154") 4.5 MB

68

[NatSchAG-Anlg-3-Blatt-4346](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4346.pdf "NatSchAG-Anlg-3-Blatt-4346") 3.5 MB

69

[NatSchAG-Anlg-3-Blatt-4348](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4348.pdf "NatSchAG-Anlg-3-Blatt-4348") 3.8 MB

70

[NatSchAG-Anlg-3-Blatt-4350](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4350.pdf "NatSchAG-Anlg-3-Blatt-4350") 3.9 MB

71

[NatSchAG-Anlg-3-Blatt-4352](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4352.pdf "NatSchAG-Anlg-3-Blatt-4352") 4.1 MB

72

[NatSchAG-Anlg-3-Blatt-4354](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4354.pdf "NatSchAG-Anlg-3-Blatt-4354") 4.5 MB

73

[NatSchAG-Anlg-3-Blatt-4546](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4546.pdf "NatSchAG-Anlg-3-Blatt-4546") 3.9 MB

74

[NatSchAG-Anlg-3-Blatt-4548](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4548.pdf "NatSchAG-Anlg-3-Blatt-4548") 4.0 MB

75

[NatSchAG-Anlg-3-Blatt-4550](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4550.pdf "NatSchAG-Anlg-3-Blatt-4550") 4.0 MB

76

[NatSchAG-Anlg-3-Blatt-4552](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4552.pdf "NatSchAG-Anlg-3-Blatt-4552") 4.1 MB

77

[NatSchAG-Anlg-3-Blatt-4554](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%203-Blatt-4554.pdf "NatSchAG-Anlg-3-Blatt-4554") 4.3 MB

78

[NatSchAG-Anlg-4](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%204.pdf "NatSchAG-Anlg-4") 1.6 MB

79

[NatSchAG-Anlg-5](/br2/sixcms/media.php/68/GVBl_I_03_2013-Anlage%205.pdf "NatSchAG-Anlg-5") 140.2 KB