## Gesetz zur Beschleunigung der Aufstellung und Prüfung kommunaler Jahresabschlüsse (Jahresabschlussbeschleunigungsgesetz - JABG)

### § 1  
Jahresabschluss

(1) Die Gemeinden und Gemeindeverbände können bei der Aufstellung der Jahresabschlüsse für die auf die Umstellung der Haushaltswirtschaft nach § 63 Absatz 3 der Kommunalverfassung des Landes Brandenburg folgenden Haushaltsjahre bis einschließlich für das Haushaltsjahr 2019 auf die Erstellung folgender Bestandteile und Anlagen verzichten:

1.  die Teilrechnungen nach § 82 Absatz 2 Satz 1 Nummer 3 der Kommunalverfassung des Landes Brandenburg,
2.  den Rechenschaftsbericht nach § 82 Absatz 2 Satz 1 Nummer 5 der Kommunalverfassung des Landes Brandenburg,
3.  die Anlagen-, Forderungs- und Verbindlichkeitenübersicht nach § 82 Absatz 2 Satz 2 Nummer 2 bis 4 der Kommunalverfassung des Landes Brandenburg und
4.  die Angaben nach § 58 Absatz 2 Nummer 3 bis 10 der Kommunalen Haushalts- und Kassenverordnung.

Vor der Aufstellung der Jahresabschlüsse nach Satz 1 ist ein Beschluss der Gemeindevertretung erforderlich.

(2) Die Jahresabschlüsse nach Absatz 1 können zeitlich gemeinsam mit dem Jahresabschluss für das Haushaltsjahr 2020 aufgestellt werden.

### § 2  
Prüfungswesen

Das Rechnungsprüfungsamt kann auf die Prüfung der Jahresabschlüsse nach § 1 Absatz 1 verzichten.

### § 3  
Außerkrafttreten

§ 1 tritt mit Ablauf des 31.
Dezember 2022 und § 2 tritt mit Ablauf des 31.
Dezember 2023 außer Kraft.