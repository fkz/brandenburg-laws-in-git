## Gesetz zu dem Staatsvertrag über die Übertragung von Aufgaben nach §§ 802k Absatz 1 Satz 2, 882h Absatz 1 Satz 2 und 3 der Zivilprozessordnung und § 6 Absatz 1 Schuldnerverzeichnisführungsverordnung und § 7 Absatz 1 Satz 1 der Vermögensverzeichnisverordnung zur Errichtung und zum Betrieb eines gemeinsamen Vollstreckungsportals der Länder

Der Landtag hat das folgende Gesetz beschlossen:

### § 1

(1) Dem am 21.
November 2012 vom Land Brandenburg unterzeichneten Staatsvertrag über die Übertragung von Aufgaben nach §§ 802k Absatz 1 Satz 2, 882h Absatz 1 Satz 2 und 3 der Zivilprozessordnung und § 6 Absatz 1 Schuldnerverzeichnisführungsverordnung und § 7 Absatz 1 Satz 1 der Vermögensverzeichnisverordnung zur Errichtung und zum Betrieb eines gemeinsamen Vollstreckungsportals der Länder wird zugestimmt.

(2) Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem § 8 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 4.
April 2013

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Staatsvertrag](/vertraege/stvvollstreckportallaender_2013)