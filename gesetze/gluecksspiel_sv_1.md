## Gesetz zu dem Ersten Glücksspieländerungsstaatsvertrag

### § 1 

Dem am 15.
Dezember 2011 vom Land Brandenburg unterzeichneten Ersten Staatsvertrag zur Änderung des Staatsvertrages zum Glücksspielwesen in Deutschland (Erster Glücksspieländerungsstaatsvertrag - Erster GIüÄndStV) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2 

(1) Tritt der Glücksspielstaatsvertrag nach seinem § 35 Absatz 2 Satz 1 mit Ablauf des 30.
Juni 2021 außer Kraft, gilt sein Inhalt im Land Brandenburg als brandenburgisches Landesrecht im Range eines Gesetzes fort.
Dies ist durch das Ministerium des Innern im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

(2) Wird die Fortgeltung des Glücksspielstaatsvertrages nach seinem § 35 Absatz 2 Satz 1 durch die Ministerpräsidentenkonferenz mit der Stimme des Landes Brandenburg beschlossen, ist dies durch das Ministerium des Innern im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

[zum Staatsvertrag](/vertraege/ersterglueaendstv_2012/2)