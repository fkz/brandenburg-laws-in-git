## Gesetz zu dem Abkommen vom 15. Dezember 2011 zur Änderung des Abkommens über die Zentralstelle der Länder für Sicherheitstechnik und über die Akkreditierungsstelle der Länder für Mess- und Prüfstellen zum Vollzug des Gefahrstoffrechts

Der Landtag hat das folgende Gesetz beschlossen:

### § 1 

Dem am 15.
Dezember 2011 unterzeichneten Abkommen zur Änderung des Abkommens über die Zentralstelle der Länder für Sicherheitstechnik und über die Akkreditierungsstelle der Länder für Mess- und Prüfstellen zum Vollzug des Gefahrstoffrechts wird zugestimmt.
Das Abkommen wird nachstehend veröffentlicht.

###  § 2 

(1)   Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2)   Der Tag, an dem das Abkommen nach seinem § 2 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 16.
April 2012

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Abkommen](/vertraege/abksicherheitstechnik_2013/5)