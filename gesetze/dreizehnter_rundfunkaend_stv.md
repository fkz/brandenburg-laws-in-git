## Gesetz zu dem Dreizehnten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Dreizehnter Rundfunkänderungsstaatsvertrag)

Der Landtag hat das folgende Gesetz beschlossen:

### § 1

Dem am 4.
November 2009 vom Land Brandenburg unterzeichneten Dreizehnten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Dreizehnter Rundfunkänderungsstaatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Dreizehnte Rundfunkänderungsstaatsvertrag nach seinem Artikel 3 Absatz 2 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 16.
Februar 2010

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

* * *

[zum Staatsvertrag](/de/vertraege-214296) - Rundfunkstaatsvertrag

[zum Staatsvertrag](/de/vertraege-241547) - Jugendmedienschutz-Staatsvertrag