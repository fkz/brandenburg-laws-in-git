## Gesetz zu dem Zweiten Glücksspieländerungsstaatsvertrag

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem in Berlin am 16.
März 2017 vom Land Brandenburg unterzeichneten Zweiten Staatsvertrag zur Änderung des Glücksspielstaatsvertrages (Zweiter Glücksspieländerungsstaatsvertrag) wird zugestimmt.
Der Vertrag wird nachstehend veröffentlicht.

#### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel 2 Absatz 1 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 29.
September 2017

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

### Anlagen

1

[Zweiter Staatsvertrag zur Änderung des Glücksspielstaatsvertrages (Zweiter Glücksspieländerungsstaatsvertrag)](/br2/sixcms/media.php/68/GVBl_I_23_2017-Anlage.pdf "Zweiter Staatsvertrag zur Änderung des Glücksspielstaatsvertrages (Zweiter Glücksspieländerungsstaatsvertrag)") 705.7 KB