## Gesetz zur Ausführung des Neunten Buches Sozialgesetzbuch (AG-SGB IX)

#### § 1 Ziel des Gesetzes

(1) Ziel dieses Gesetzes ist es, in Umsetzung des Bundesteilhabegesetzes vom 23. Dezember 2016 (BGBl. I S. 3234), das zuletzt durch Artikel 27 des Gesetzes vom 17. Juli 2017 (BGBl. I S. 2541, 2571) geändert worden ist, im Land Brandenburg

1.  die Eingliederungshilfe für Menschen mit Behinderungen zu einer personenzentrierten Teilhabeleistung weiter zu entwickeln,
2.  flächendeckende, bedarfsdeckende, am Sozialraum orientierte und inklusiv ausgerichtete Angebote zur Teilhabe von Menschen mit Behinderungen sicherzustellen sowie
3.  eine einheitliche Rechtsanwendung zu gewährleisten.

(2) Die Leistungen für Kinder mit Behinderung oder für von Behinderung bedrohte Kinder sind so zu gestalten, dass die spezifischen Besonderheiten berücksichtigt werden.
Dies gilt insbesondere bei der Umsetzung der Ansprüche auf Leistungen der Frühförderung nach den §§ 46, 79 des Neunten Buches Sozialgesetzbuch in Verbindung mit den Verfahrensregelungen nach der Frühförderungsverordnung vom 24.
Juni 2003 (BGBl. I S. 998), die zuletzt durch Artikel 23 des Gesetzes vom 23. Dezember 2016 (BGBl. I S. 3234) geändert worden ist.

#### § 2 Träger der Eingliederungshilfe

(1) Örtliche Träger der Eingliederungshilfe sind die Landkreise und kreisfreien Städte.

(2) Überörtlicher Träger der Eingliederungshilfe ist das Land.
Die Aufgaben des überörtlichen Trägers der Eingliederungshilfe werden vom Landesamt für Soziales und Versorgung wahrgenommen.

#### § 3 Sachliche Zuständigkeit der örtlichen Träger der Eingliederungshilfe

Die örtlichen Träger der Eingliederungshilfe sind sachlich zuständig für die Leistungen zur selbstbestimmten Lebensführung für Menschen mit Behinderungen nach Teil 2 des Neunten Buches Sozialgesetzbuch.
Sie nehmen diese Aufgaben als pflichtige Selbstverwaltungsangelegenheiten wahr.
Die Rechtsaufsicht über die örtlichen Träger der Eingliederungshilfe übt das für Soziales zuständige Ministerium aus.

#### § 4 Sachliche Zuständigkeit des Landes als überörtlicher Träger der Eingliederungshilfe

(1) Der überörtliche Träger der Eingliederungshilfe ist zuständig für

1.  die fachlich-konzeptionelle Beratung und die Unterstützung der örtlichen Träger der Eingliederungshilfe mit dem Ziel des Erfahrungsaustausches sowie der Entwicklung von Instrumenten zur zielgerichteten Erbringung und Überprüfung der in § 3 benannten Leistungen und deren Qualitätssicherung einschließlich der Wirksamkeit der Leistungen,
2.  den Erlass von Rahmenrichtlinien zur einheitlichen Rechtsanwendung des Leistungsrechts nach Teil 2 des Neunten Buches Sozialgesetzbuch,
3.  die Feststellung der Leistungsminderung der antragstellenden Person und ihres Bedarfes an Anleitung und Begleitung im Rahmen des Budgets für Arbeit nach § 61 des Neunten Buches Sozialgesetzbuch,
4.  die Erteilung des Einvernehmens gegenüber der Bundesagentur für Arbeit im Rahmen des Anerkennungsverfahrens nach § 225 Satz 2 des Neunten Buches Sozialgesetzbuch,
5.  die Prüfung der fachlichen Anforderungen an andere Leistungsanbieter nach § 60 des Neunten Buches Sozialgesetzbuch,
6.  die Erfassung und Auswertung der Leistungsentwicklung und der Ausgaben in der Eingliederungshilfe.

(2) Im Rahmen der Aufgaben nach Absatz 1 Nummer 1 und 2 nimmt der überörtliche Träger der Eingliederungshilfe durch einen Fachdienst insbesondere folgende Aufgaben wahr:

1.  Weiterentwicklung des Bedarfsermittlungsinstrumentes und des Verfahrens zur Erstellung von Gesamtplänen nach § 141 des Zwölften Buches Sozialgesetzbuch bis zum 31. Dezember 2019 und nach § 121 des Neunten Buches Sozialgesetzbuch ab dem 1. Januar 2020 sowie an der Entwicklung von Verfahren zur Messung von Ergebnisqualität und Wirksamkeitskontrolle,
2.  Organisation und Durchführung von Fortbildungen,
3.  fachliche Einschätzung von Einzelfällen im Rahmen der Ermittlung des Hilfebedarfs und der bedarfsdeckenden Hilfen, insbesondere bei Menschen mit komplexen Unterstützungsbedarfen.

(3) Zur Sicherung landeseinheitlicher Regelungen und Versorgungsstrukturen ist das Land als überörtlicher Träger der Eingliederungshilfe ferner zuständig für

1.  den Abschluss von Leistungs- und Vergütungsvereinbarungen nach § 125 des Neunten Buches Sozialgesetzbuch unter Mitwirkung des jeweils für den Ort der Leistungserbringung zuständigen örtlichen Trägers der Eingliederungshilfe und
2.  den Abschluss von Rahmenverträgen gemeinsam mit den örtlichen Trägern der Eingliederungshilfe und den Vereinigungen der Leistungserbringer nach § 46 des Neunten Buches Sozialgesetzbuch in Verbindung mit §§ 79 und 131 des Neunten Buches Sozialgesetzbuch.

Der überörtliche Träger der Eingliederungshilfe stellt beim Abschluss von Vereinbarungen nach Satz 1 Nummer 1 Einvernehmen mit dem jeweils für den Sitz der Einrichtung zuständigen örtlichen Träger der Eingliederungshilfe her.

(4) Das für Soziales zuständige Mitglied der Landesregierung kann die Zuständigkeiten nach Absatz 3 Satz 1 Nummer 1 durch Rechtsverordnung im Einvernehmen mit dem für die Kommunalaufsicht zuständigen Mitglied der Landesregierung auf die örtlichen Träger der Eingliederungshilfe übertragen, wenn alle örtlichen Träger der Eingliederungshilfe sicherstellen, dass

1.  die Vorbereitung des Abschlusses der in Absatz 3 Satz 1 Nummer 1 genannten Vereinbarungen und Versorgungsverträge und
2.  die Vorbereitung der Durchführung von Qualitäts- und Wirtschaftlichkeitsprüfungen nach § 128 des Neunten Buches Sozialgesetzbuch

für die in § 3 benannten Leistungen gemeinsam und zentral wahrgenommen werden.
Zu diesem Zweck schließen die örtlichen Träger der Eingliederungshilfe eine öffentlich-rechtliche Vereinbarung in entsprechender Anwendung des Gesetzes über kommunale Gemeinschaftsarbeit im Land Brandenburg ab, die der Genehmigung der in § 3 Satz 3 bestimmten Aufsichtsbehörde bedarf.
In der zu schließenden öffentlich-rechtlichen Vereinbarung können die örtlichen Träger der Eingliederungshilfe auch festlegen, dass die Bedarfsermittlung nach § 118 des Neunten Buches Sozialgesetzbuch einheitlich und zentral durch ein Team von qualifizierten Fachkräften wahrgenommen wird.

(5) Die örtlichen Träger der Eingliederungshilfe nehmen die Aufgaben nach Absatz 4 unter Mitwirkung des überörtlichen Trägers der Eingliederungshilfe als Pflichtaufgaben zur Erfüllung nach Weisung wahr.
Der überörtliche Träger der Eingliederungshilfe ist bei der Vorbereitung der Vertragsverhandlungen und bei den Qualitäts- und Wirtschaftlichkeitsprüfungen zu beteiligen.
Bei Verträgen und Vereinbarungen, die überregionale oder grundsätzliche Bedeutung haben, steht dem überörtlichen Träger der Eingliederungshilfe ein Widerspruchsrecht zu.
Die zuständige Sonderaufsichtsbehörde gemäß § 121 der Kommunalverfassung des Landes Brandenburg ist das für Soziales zuständige Ministerium.
In der Rechtsverordnung nach Absatz 4 Satz 1 kann auch bestimmt werden, dass die Mitgliedschaft in der Schiedsstelle nach § 76 Absatz 2 Satz 2 des Elften Buches Sozialgesetzbuch durch einen örtlichen Träger der Eingliederungshilfe wahrgenommen wird.

(6) Die Schiedsstelle nach § 133 des Neunten Buches Sozialgesetzbuch wird beim Landesamt für Soziales und Versorgung gebildet.

#### § 5 Interessenvertretung der Menschen mit Behinderungen

(1) Maßgebliche Interessenvertretung der Menschen mit Behinderungen im Sinne des Teils 2 des Neunten Buches Sozialgesetzbuch ist der Landesbehindertenbeirat Brandenburg.

(2) Für seine Mitwirkung bei der Erarbeitung und Beschlussfassung der Rahmenverträge nach § 131 Absatz 2 des Neunten Buches Sozialgesetzbuch benennt der Landesbehindertenbeirat Brandenburg bis zu drei Vertreterinnen und Vertreter zur Interessenvertretung.

#### § 6 Clearingstelle, Qualitätssicherung

(1) Bei dem oder der Beauftragten der Landesregierung für die Belange der Menschen mit Behinderungen wird eine Clearingstelle eingerichtet.
Diese hat die Aufgabe, zwischen dem Leistungsberechtigten nach § 99 des Neunten Buches Sozialgesetzbuch und dem zuständigen örtlichen Träger der Eingliederungshilfe bei Streitigkeiten im Einzelfall zu vermitteln und auf eine gütliche Einigung über Art und Umfang der Leistung sowie Verfahrensfragen hinzuwirken.
Der jeweils zuständige örtliche Träger der Eingliederungshilfe ist bei der Klärung des Einzelfalles hinzuzuziehen.
Der zuständige Leistungserbringer kann bei Bedarf hinzugezogen werden.
Die Clearingstelle kann den Verfahrensparteien Fristen setzen.
Das Recht, einen förmlichen Rechtsbehelf einzulegen, bleibt unberührt.

(2) Die Wahrnehmung der Aufgaben der Clearingstelle erfolgt nach Maßgabe der Verfahrensordnung, die sich die Clearingstelle selbst gibt.
Erlass und Änderungen der Verfahrensordnung bedürfen der vorherigen Zustimmung des für Soziales zuständigen Mitglieds der Landesregierung.
Die Clearingstelle muss dem für Soziales zuständigen Mitglied der Landesregierung jährlich einen Tätigkeitsbericht über die Wahrnehmung der Aufgaben nach Absatz 1 in nicht personenbezogener Form vorlegen.

(3) Zur nachhaltigen Sicherung der Qualität in der Eingliederungshilfe legt das für Soziales zuständige Ministerium landesweit einheitliche Kriterien für die Qualifizierung der Fachkräfte in der Eingliederungshilfe fest.

(4) Zur Sicherstellung von qualitativen Standards und Verfahren im Bereich der Eingliederungshilfe führt das für Soziales zuständige Ministerium auf der Grundlage einer Machbarkeitsstudie ein Qualitätsmonitoring ein.

#### § 7 Gemeinsame Verantwortung und Zusammenarbeit

(1) Die nach diesem Gesetz zuständigen Träger der Eingliederungshilfe tragen die gemeinsame Verantwortung für die Leistungsgewährung nach Teil 2 des Neunten Buches Sozialgesetzbuch sowie die damit einhergehende Ausgabenentwicklung.
Hierzu arbeiten die Träger der Eingliederungshilfe bei der Wahrnehmung der Aufgaben nach diesem Gesetz eng und vertrauensvoll zusammen und unterstützen sich gegenseitig.
Die Zusammenarbeit beinhaltet insbesondere eine Abstimmung, Koordinierung und Vernetzung der jeweils in eigener Zuständigkeit wahrzunehmenden Aufgaben.

(2) Die örtlichen Träger der Eingliederungshilfe wirken im Rahmen ihrer Aufgaben nach diesem Gesetz auf eine sozialräumliche Entwicklung hin.
Dies geschieht

1.  unter Berücksichtigung der Stärkung des Ehrenamtes und der Transparenz der vorhandenen Leistungsangebote sowie durch die Nutzung vorhandener Strukturen und die Einbindung von unterstützenden Wohnformen in die Gemeinde und
2.  durch ein abgestimmtes und vernetztes Versorgungssystem einschließlich einer unabhängigen wohnortnahen Beratung und Betreuung, insbesondere zu Maßnahmen und Hilfen, die einen Verbleib in der eigenen Häuslichkeit unterstützen sowie durch die Förderung individueller Wohn- und Betreuungsformen.

Die örtlichen Träger der Eingliederungshilfe arbeiten dabei eng mit den weiteren Rehabilitationsträgern nach § 6 des Neunten Buches Sozialgesetzbuch, den Gemeindeverbänden sowie den örtlichen Interessenvertretungen der Menschen mit Behinderungen zusammen.

#### § 8 Heranziehung von Ämtern

(1) Die Landkreise können durch Satzung bestimmen, dass Ämter und amtsfreie Gemeinden Aufgaben durchführen, die den Landkreisen als örtliche Träger der Eingliederungshilfe obliegen, wenn die ordnungsgemäße Erfüllung der Aufgaben gewährleistet ist.
Dabei können die Ämter und amtsfreien Gemeinden im eigenen Namen entscheiden.

(2) Die Landkreise können Ämter und amtsfreie Gemeinden für Einzelfälle durch öffentlich-rechtliche Vereinbarung beauftragen, Aufgaben, die den Landkreisen als örtliche Träger der Eingliederungshilfe obliegen, durchzuführen und dabei im Namen des Landkreises zu entscheiden.

(3) Werden nach Absatz 1 oder Absatz 2 Aufgaben von Ämtern und amtsfreien Gemeinden durchgeführt, hat der Landkreis die aufgewendeten Kosten zu erstatten.
Die Erstattung von Personal- und Sachkosten erfolgt durch pauschale Abgeltung und ist in der nach Absatz 1 zu erlassenden Satzung oder auf Grundlage der nach Absatz 2 zu schließenden öffentlich-rechtlichen Vereinbarung zu regeln.

#### § 9 Entgegennahme und Weiterleitung von Anträgen, vorläufige Hilfeleistungen

(1) Wird ein Antrag auf Eingliederungshilfe bei einer kreisangehörigen Gemeinde gestellt, in welcher sich die hilfesuchende Person tatsächlich aufhält, so hat die Gemeinde, soweit sie nicht selbst nach § 3 die Aufgaben durchführt, den zuständigen örtlichen Träger der Eingliederungshilfe unverzüglich über die Geltendmachung von Eingliederungshilfe zu unterrichten und die Unterlagen an diesen weiterzuleiten.
Wird ein Antrag bei einem Amt gestellt, das nicht selbst die Aufgaben durchführt, findet Satz 1 entsprechende Anwendung.

(2) Die Ämter und amtsfreien Gemeinden haben vorläufig die unerlässlich notwendigen Maßnahmen zu treffen, wenn die Gewährung der Hilfe nicht bis zur Entscheidung des Trägers der Eingliederungshilfe aufgeschoben werden kann.
Der örtliche Träger der Eingliederungshilfe ist unverzüglich über die getroffenen Maßnahmen zu unterrichten.
Für die Kostenerstattung durch den zuständigen Träger gilt § 8 Absatz 3 entsprechend.

#### § 10 Arbeitsgemeinschaft zur Weiterentwicklung und Qualitätssicherung der Eingliederungshilfe

(1) Zur Förderung und Weiterentwicklung der Strukturen der Eingliederungshilfe und zur Sicherung und Weiterentwicklung der Teilhabe von Menschen mit Behinderungen wird beim für Soziales zuständigen Ministerium eine Arbeitsgemeinschaft im Sinne des § 94 Absatz 4 des Neunten Buches Sozialgesetzbuch gebildet.
Diese besteht aus Vertreterinnen und Vertretern

1.  des für Soziales zuständigen Ministeriums,
2.  der örtlichen Träger der Eingliederungshilfe nach § 2 Absatz 1,
3.  der Vereinigungen der privaten und öffentlichen Leistungserbringer,
4.  der landesweit tätigen rechtsfähigen Verbände für Menschen mit Behinderungen im Sinne des § 16 Absatz 1 des Brandenburgischen Behindertengleichstellungsgesetzes.

Jede der in Satz 2 genannten Gruppen kann bis zu vier Vertreterinnen und Vertreter in die Arbeitsgemeinschaft entsenden.
Das für Jugend zuständige Ministerium kann bis zu zwei Personen als ständig anwesende sachverständige Gäste entsenden.
Die Einladung von weiteren Gästen ist möglich.

(2) Zu den Aufgaben der Arbeitsgemeinschaft gehören insbesondere

1.  die Förderung und Weiterentwicklung der Strukturen der Eingliederungshilfe,
2.  die Sicherstellung eines Informations- und Erfahrungsaustausches,
3.  die Förderung der Entwicklung und Durchführung von Instrumenten zur zielgerichteten Erbringung und Überprüfung von Leistungen und der Qualitätssicherung einschließlich der Wirksamkeit der Leistungen,
4.  die Erarbeitung von Grundsätzen zur Weiterentwicklung der personenzentrierten Fachleistungen und zur Berücksichtigung von Qualitätsstandards bei der Leistungserbringung nach Teil 2 des Neunten Buches Sozialgesetzbuch.

Die Arbeitsgemeinschaft kann zu Angelegenheiten nach Absatz 2 Beschlüsse fassen.
Jede der in Absatz 1 Satz 2 benannte Gruppe hat jeweils vier Stimmen.
Die Beschlüsse bedürfen einer einfachen Mehrheit der bei der Beschlussfassung anwesenden Mitglieder.
Das für Soziales zuständige Ministerium leitet den Beschluss der Arbeitsgemeinschaft der Brandenburger Kommission nach § 12 zur weiteren Befassung zu.

(3) Die Benennung der Mitglieder erfolgt gegenüber dem für Soziales zuständigen Ministerium.
Das für Soziales zuständige Ministerium erlässt für die Arbeitsgemeinschaft eine Geschäftsordnung.

#### § 11 Brandenburger Steuerungskreis

(1) Die örtlichen Träger der Eingliederungshilfe und das Land als überörtlicher Träger der Eingliederungshilfe bilden zum Zwecke der Abstimmung und Koordinierung der nach diesem Gesetz wahrzunehmenden Aufgaben einen Brandenburger Steuerungskreis.

(2) Der Brandenburger Steuerungskreis hat insbesondere folgende Aufgaben:

1.  Informationsaustausch und Erarbeitung gemeinsamer Positionen zu Themen des Gesamtplanverfahrens und des Fallmanagements,
2.  Erarbeitung gemeinsamer Grundlagen für die Vorhaltung von personenzentrierten und bedarfsdeckenden Angeboten zur Leistungsgewährung und zur Angebotssteuerung,
3.  Positionierung der Leistungsträger zu Themen der Brandenburger Kommission nach § 12 und deren Arbeitsgruppen,
4.  Erarbeitung von Empfehlungen zur Ausgestaltung von personenzentrierten und bedarfsdeckenden Angeboten,
5.  Erstellung eines landesweiten Berichtswesens und eines landesweiten Kennzahlenvergleichs.

(3) Der Brandenburger Steuerungskreis setzt sich zusammen aus jeweils einem Mitglied jedes örtlichen Trägers der Eingliederungshilfe sowie sechs Mitgliedern, die von dem für Soziales zuständigen Ministerium benannt werden.
Die kommunalen Spitzenverbände entsenden jeweils eine Person als sachverständigen Gast.
Die Einladung weiterer Gäste ist möglich.

(4) Der Brandenburger Steuerungskreis kann zu Angelegenheiten nach Absatz 2 Beschlüsse fassen.
Das Land als überörtlicher Träger der Eingliederungshilfe und die örtlichen Träger der Eingliederungshilfe haben jeweils 18 Stimmen.
Die Beschlüsse bedürfen einer Zweidrittelmehrheit der bei der Beschlussfassung anwesenden Mitglieder.

(5) Beim Landesamt für Soziales und Versorgung wird eine Geschäftsstelle gebildet.
Die Geschäftsstelle bereitet die Sitzungen des Brandenburger Steuerungskreises vor.
Der Brandenburger Steuerungskreis gibt sich eine Geschäftsordnung.

#### § 12 Brandenburger Kommission

(1) Für die Vorbereitung der Änderung, Ergänzung und Fortentwicklung der Rahmenverträge nach § 131 des Neunten Buches Sozialgesetzbuch in Verbindung mit § 125 des Neunten Buches Sozialgesetzbuch wird eine Brandenburger Kommission gebildet.

(2) Die Brandenburger Kommission ist darüber hinaus zuständig für die Erarbeitung landesweiter Rahmenvereinbarungen für

1.  Leistungen nach § 102 des Neunten Buches Sozialgesetzbuch für die jeweiligen Zielgruppen und die dazugehörigen Rahmenleistungsvereinbarungen, differenziert nach Struktur-, Prozess- und Ergebnisqualität,
2.  Kalkulationsgrundlagen zur Ermittlung der Leistungspauschale, insbesondere zur Personalbemessung nach Leistungskategorien gemäß Nummer 1,
3.  Pauschalen für einzelne Vergütungsbestandteile nach § 125 Absatz 3 des Neunten Buches Sozialgesetzbuch sowie
4.  pauschale Fortschreibungsraten auf Personal- und Sachkosten einzelner Vergütungsbestandteile nach § 125 Absatz 1 Nummer 2 des Neunten Buches Sozialgesetzbuch.

Die Brandenburger Kommission berücksichtigt die zugeleiteten Beschlüsse der Arbeitsgemeinschaft zur Weiterentwicklung der Eingliederungshilfe nach § 10 bei der Erörterung der Angelegenheiten nach den Absätzen 1 und 2.

(3) Die Brandenburger Kommission setzt sich paritätisch aus folgenden Mitgliedern zusammen:

1.  sechs Mitglieder der Vereinigungen der Leistungserbringer auf Landesebene,
2.  sechs Mitglieder der Träger der Eingliederungshilfe, die sich aus jeweils drei Vertreterinnen oder Vertretern der örtlichen Träger der Eingliederungshilfe sowie des Landes als überörtlicher Träger der Eingliederungshilfe zusammensetzen.

Die Benennung der Mitglieder erfolgt gegenüber der Geschäftsstelle der Brandenburger Kommission für die Mitglieder nach Satz 1 Nummer 1 durch die Vereinigungen der Träger der Einrichtungen auf Landesebene, für die Mitglieder nach Satz 1 Nummer 2 für die örtlichen Träger der Eingliederungshilfe durch die kommunalen Spitzenverbände und für den überörtlichen Träger der Eingliederungshilfe durch das für Soziales zuständige Ministerium.
Jedes Mitglied hat eine Stimme.
Die Einladung von Gästen ist möglich.

(4) Der Landesbehindertenbeirat Brandenburg wirkt bei der Erarbeitung und Beschlussfassung der Rahmenverträge gemäß § 131 Absatz 2 des Neunten Buches Sozialgesetzbuch in Verbindung mit § 5 Absatz 2 mit.

(5) Die Brandenburger Kommission fasst zu den Angelegenheiten nach den Absätzen 1 und 2 Beschlüsse.
Diese bedürfen für ihre Rechtswirksamkeit der Zustimmung durch die Vertragsparteien nach § 131 des Neunten Buches Sozialgesetzbuch.

(6) Beim Landesamt für Soziales und Versorgung wird eine Geschäftsstelle gebildet.
Die Geschäftsstelle bereitet die Sitzungen der Brandenburger Kommission vor.
Die Brandenburger Kommission gibt sich eine Geschäftsordnung, in der Näheres zu ihrer Arbeitsweise, insbesondere zu Vorsitz, Beschlussfähigkeit und Beschlussfassungen geregelt wird.

#### § 13 Qualitäts- und Wirtschaftlichkeitsprüfung

Abweichend von § 128 Absatz 1 Satz 1 des Neunten Buches Sozialgesetzbuch kann das Land als überörtlicher Träger der Eingliederungshilfe oder ein von diesem beauftragter Dritter auch ohne tatsächliche Anhaltspunkte für eine Verletzung vertraglicher oder gesetzlicher Pflichten des Leistungserbringers die Qualität und Wirtschaftlichkeit einschließlich der Wirksamkeit der vereinbarten Leistungen des Leistungserbringers prüfen.

#### § 14 Zielvereinbarungen und Modellvorhaben

(1) Zur Erprobung neuer und zur Weiterentwicklung der bestehenden Leistungs- und Finanzierungsstrukturen schließt das Land als überörtlicher Träger der Eingliederungshilfe mit dem zuständigen örtlichen Träger der Eingliederungshilfe und den Leistungserbringern Zielvereinbarungen im Sinne des § 132 des Neunten Buches Sozialgesetzbuch.
Das für Soziales zuständige Ministerium kann das Nähere zur Ausgestaltung des Verfahrens durch Verwaltungsvorschriften regeln.

(2) Zur Weiterentwicklung von Leistungen in der Eingliederungshilfe, insbesondere zur Verbesserung von inklusiv sozialräumlichen Angeboten, können in Modellvorhaben neue Formen der Leistungserbringung erprobt werden.
Die Modellvorhaben bedürfen der vorherigen Zustimmung des Landes als überörtlicher Träger der Eingliederungshilfe.

(3) Nach den Absätzen 1 und 2 entstehende notwendige Aufwendungen sind kostenerstattungsfähig nach den Regelungen dieses Gesetzes.

#### § 15 Leistungsträger und Finanzierung

(1) Die Träger der Eingliederungshilfe tragen in Fortführung der bisherigen gemeinsamen Fach- und Finanzverantwortung die Kosten für die Aufgaben, die ihnen nach dem Neunten Buch Sozialgesetzbuch oder nach diesem Gesetz obliegen.
Dabei beträgt die Finanzierungsquote des Landes als überörtlicher Träger der Eingliederungshilfe 85 Prozent und die Finanzierungsquote der örtlichen Träger der Eingliederungshilfe 15 Prozent (kommunaler Eigenanteil).

(2) Der kommunale Eigenanteil wird in Form eines stadt- und landkreisindividuellen Festbetrages erbracht.
Die individuellen kommunalen Festbeträge werden jährlich im Amtsblatt für Brandenburg veröffentlicht.

(3) Die Mehraufwendungen für die ab 2020 mit dem Neunten Buch Sozialgesetzbuch zusätzlich eingeführten Leistungstatbestände trägt das Land als überörtlicher Träger der Eingliederungshilfe in voller Höhe.
Dabei werden entsprechende Entlastungen der örtlichen Träger der Eingliederungshilfe ausgabenmindernd berücksichtigt.

#### § 16 Kostenerstattung

(1) Zum Ausgleich der Kosten, die den örtlichen Trägern der Eingliederungshilfe durch die Übertragung der sachlichen Zuständigkeit nach § 3 entstehen, erstattet das Land die notwendigen Gesamtnettoaufwendungen nach Maßgabe des § 15 sowie der Absätze 2 bis 8.
Des Weiteren können Aufwendungen für Leistungen erstattungsfähig sein, die eine Leistungsgewährung nach § 3 ergänzen oder ersetzen, sofern die Leistungen geeignet sind, die Eingliederungshilfeausgaben zu senken.

(2) Die Gesamtnettoaufwendungen werden durch Abzug der Einnahmen von den Ausgaben ermittelt.

(3) Die individuellen kommunalen Festbeträge für das Kostenerstattungsjahr 2020 betragen 15 Prozent der jeweils pro kreisfreier Stadt und Landkreis anerkannten Gesamtnettoaufwendungen der Eingliederungshilfe des Jahres 2018 abzüglich des weitergeleiteten Erstattungsbetrages des Bundes für das Jahr 2018 gemäß § 18 des Gesetzes zur Ausführung des Zwölften Buches Sozialgesetzbuch sowie zuzüglich eines prognostizierten individuellen Steigerungsbetrages für die Jahre 2019 und 2020.
Die Prognose berücksichtigt die durchschnittlichen individuellen Ausgabensteigerungen in den Jahren 2016, 2017 und 2018 gegenüber dem jeweiligen Vorjahr.

(4) Die individuellen kommunalen Festbeträge für das Kostenerstattungsjahr 2021 betragen 15 Prozent der jeweils pro kreisfreier Stadt und Landkreis anerkannten Gesamtnettoaufwendungen der Eingliederungshilfe des Jahres 2019 abzüglich des weitergeleiteten Erstattungsbetrages des Bundes für das Jahr 2019 gemäß § 18 des Gesetzes zur Ausführung des Zwölften Buches Sozialgesetzbuch sowie zuzüglich eines prognostizierten individuellen Steigerungsbetrages für die Jahre 2020 und 2021.
Die Prognose berücksichtigt die durchschnittlichen individuellen Ausgabensteigerungen in den Jahren 2017, 2018 und 2019 gegenüber dem jeweiligen Vorjahr.

(5) Den individuellen kommunalen Festbeträgen für die Kostenerstattungsjahre 2022 und 2023 werden die jeweiligen prognostizierten individuellen Steigerungsquoten des Jahres 2021 zugrunde gelegt.

(6) Die Bestimmungen zur Kostenerstattung nach den Absätzen 3 bis 5 werden im Rahmen der Evaluierung nach § 20 Absatz 3 Nummer 1 überprüft.
Im Ergebnis der Überprüfung werden die individuellen kommunalen Festbeträge spätestens im Kostenerstattungsjahr 2023 rückwirkend angepasst.

(7) Soweit die den individuellen kommunalen Festbeträgen zugrunde liegenden prognostizierten Steigerungsraten im Vergleich zu den tatsächlichen Steigerungsraten zu hoch angesetzt waren, haben die örtlichen Träger der Eingliederungshilfe Anspruch auf rückwirkenden Ausgleich des jeweiligen Differenzbetrages durch das Land.
Soweit die prognostizierten Steigerungsraten zu niedrig waren, werden die jeweiligen Überzahlungen des Landes im Rahmen der nächsten Abschlagszahlung verrechnet.

(8) Die individuellen kommunalen Festbeträge sind auf 15 Prozent der jeweils pro kreisfreier Stadt und Landkreis anerkannten Gesamtnettoaufwendungen der Eingliederungshilfe begrenzt.

(9) Für die Durchführung der Kostenerstattung ist das Landesamt für Soziales und Versorgung zuständig.

#### § 17 Abrechnungsverfahren und Abschläge

(1) Die Kosten werden auf Antrag erstattet.
Die örtlichen Träger der Eingliederungshilfe haben die für die Kostenerstattung nach § 16 Absatz 1 maßgeblichen Aufwendungen durch einen nach Einnahme- und Ausgabearten gegliederten Nachweis entsprechend dem vom Landesamt für Soziales und Versorgung vorgegebenen Muster nachzuweisen.
Der Nachweis für das erste Halbjahr des laufenden Jahres ist spätestens bis zum 30.
September des laufenden Jahres und der Nachweis für das gesamte Jahr spätestens bis zum 30.
April des Folgejahres vorzulegen.
Das Landesamt für Soziales und Versorgung kann zur Feststellung der Höhe der Kostenerstattungsansprüche ergänzend anspruchsbegründende Unterlagen anfordern, Prüfungen bei den örtlichen Trägern der Eingliederungshilfe durchführen und die Unterlagen vor Ort einsehen.

(2) Das Land gewährt jedem örtlichen Träger der Eingliederungshilfe monatliche Kostenerstattungsabschläge.
Die Höhe der monatlichen Abschläge beträgt ein Zwölftel der anerkannten erstattungsfähigen Aufwendungen des Vorjahres des jeweiligen örtlichen Trägers der Eingliederungshilfe zuzüglich eines angemessenen einheitlichen Steigerungssatzes, der sich an der Veränderung der Verbraucherpreise im Land Brandenburg gegenüber dem Vorjahr oder der vereinbarten pauschalen Entgeltfortschreibung für das laufende Jahr im Bereich der Eingliederungshilfe orientiert.
Bis zur Feststellung der anerkannten erstattungsfähigen Aufwendungen des Vorjahres werden die bisher gezahlten Abschläge weiter gewährt.

(3) Nach Abschluss des Verfahrens nach Absatz 1 erfolgt ein Ausgleich von Über- und Unterzahlungen mit dem Folgeabschlag.

#### § 18 Personal- und Sachkosten

(1) Die örtlichen Träger der Eingliederungshilfe erhalten zum Ausgleich der aufzuwendenden Personal- und Sachkosten eine Pauschale in Höhe von 4,15 Prozent der nach § 16 ermittelten Gesamtnettoaufwendungen für die Eingliederungshilfe abzüglich des individuellen kommunalen Festbetrages.

(2) Die Personal- und Sachkostenpauschale nach Absatz 1 wird bei der Gewährung der Abschläge nach § 17 Absatz 2 berücksichtigt.

(3) Die Auskömmlichkeit der Personal- und Sachkostenpauschale wird im Rahmen der Evaluierung nach § 20 Absatz 3 Nummer 1 überprüft und die Pauschale rückwirkend angepasst.

(4) Soweit die Personal- und Sachkostenpauschale nach Absatz 1 nicht auskömmlich war, haben die örtlichen Träger der Eingliederungshilfe Anspruch auf rückwirkenden Ausgleich durch das Land.
Soweit die Personal- und Sachkostenpauschale nach Absatz 1 zu hoch war, werden die jeweiligen Überzahlungen des Landes im Rahmen der nächsten Abschlagszahlung verrechnet.

#### § 19 Berichts- und Auskunftspflichten

Die örtlichen Träger der Eingliederungshilfe melden quartalsweise in elektronischer Form an das Landesamt für Soziales und Versorgung Daten zu den nach dem Neunten Buch Sozialgesetzbuch erbrachten Leistungen, insbesondere Angaben zum Personenkreis, zum Leistungsort und zur Höhe der Ausgaben und Einnahmen.

#### § 20 Evaluierung

(1) Das für Soziales zuständige Ministerium gibt im Benehmen mit der Arbeitsgemeinschaft nach § 10 im Kalenderjahr 2020 ein Gutachten in Auftrag, welches die tatsächliche Leistungsentwicklung einschließlich der sich aus dieser ergebenden Auswirkungen auf die Ausgaben der örtlichen Träger der Eingliederungshilfe wissenschaftlich evaluiert.
Zu untersuchen ist die Ausgabenentwicklung für die Aufgabenwahrnehmung nach § 97 Absatz 3 Nummer 1 des Zwölften Buches Sozialgesetzbuch in der am 31. Dezember 2019 geltenden Fassung bis zum 31.
Dezember 2019 sowie nach § 102 des Neunten Buches Sozialgesetzbuch ab dem 1.
Januar 2020.

(2) Die finanziellen Auswirkungen der

1.  verbesserten Einkommens- und Vermögensanrechnung,
2.  Einführung des Budgets für Arbeit und der anderen Leistungsanbieter,
3.  Leistungskataloge für die Soziale Teilhabe und die Teilhabe an Bildung,
4.  Trennung der Fachleistungen der Eingliederungshilfe von den Leistungen zum Lebensunterhalt,
5.  Teilhabeplanverfahren und Gesamtplanverfahren,
6.  Einführung von Frauenbeauftragten in Werkstätten für Menschen mit Behinderungen

sind in dem Gutachten gemäß Absatz 1 gesondert zu untersuchen.
Zusätzlich sind die Ausgaben und Einnahmen bei den Leistungen der Eingliederungshilfe zu untersuchen.
Mehrbelastungen sind dabei getrennt von den Kostensteigerungen der Eingliederungshilfe zu ermitteln, die auch ohne die Neuregelung des Eingliederungshilferechts durch das Bundesteilhabegesetz eingetreten wären.
Vergleichsgrundlagen sind die den Trägern der Sozialhilfe entstandenen Ausgaben und Einnahmen nach dem Gesetz zur Ausführung des Zwölften Buches Sozialgesetzbuch und den entstandenen Ausgaben für die Leistungen der Eingliederungshilfe nach diesem Gesetz.

(3) Gegenstand des Gutachtens ist darüber hinaus die Untersuchung folgender Bereiche:

1.  die Auskömmlichkeit der individuellen kommunalen Festbeträge und der prognostizierten jährlichen Steigerung nach § 16 Absatz 3 bis 5 sowie der Personal- und Sachkostenpauschale nach § 18 Absatz 1,
2.  die Wirksamkeit der Gremien nach den §§ 11 und 12,
3.  die Umsetzung des Vertragswesens unter Berücksichtigung der notwendigen Weiterentwicklung der Strukturen und Angebote in der Eingliederungshilfe gemäß § 94 Absatz 3 des Neunten Buches Sozialgesetzbuch,
4.  die Wirksamkeit der Modellvorhaben nach § 14 Absatz 2,
5.  die Erreichung der Ziele nach § 1.