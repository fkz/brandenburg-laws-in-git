## Brandenburgisches Architektengesetz (BbgArchG)

Der Landtag hat das folgende Gesetz beschlossen:

Inhaltsübersicht
----------------

### Abschnitt 1  
Schutz der Berufsbezeichnungen

[§ 1 Berufsbezeichnungen](#1)

[§ 2 Führung der geschützten Berufsbezeichnungen durch auswärtige Dienstleister](#2)

[§ 3 Berufsaufgaben](#3)

[§ 4 Voraussetzungen für die Eintragung in die Architektenlisten](#4)

[§ 5 Versagung der Eintragung](#5)

[§ 6 Löschung der Eintragung](#6)

### Abschnitt 2  
Besondere Bestimmungen für Gesellschaften, Berufshaftpflicht

[§ 7 Gesellschaften](#7)

[§ 8 Auswärtige Gesellschaften](#8)

[§ 9 Partnerschaftsgesellschaften](#9)

[§ 10 Berufshaftpflichtversicherung](#10)

### Abschnitt 3  
Architektenkammer

[§ 11 Brandenburgische Architektenkammer](#11)

[§ 12 Aufgaben der Architektenkammer](#12)

[§ 13 Versorgungswerk](#13)

[§ 14 Organe der Architektenkammer](#14)

[§ 15 Vertreterversammlung der Architektenkammer](#15)

[§ 16 Aufgaben der Vertreterversammlung](#16)

[§ 17 Vorstand der Architektenkammer](#17)

[§ 18 Satzungen](#18)

[§ 19 Hauptsatzung](#19)

[§ 20 Finanzwesen](#20)

[§ 21 Datenverarbeitung, Pflicht zur Verschwiegenheit, Auskünfte](#21)

[§ 22 Einrichtung, Zusammensetzung und Wahl des Eintragungsausschusses](#22)

[§ 23 Tätigkeit des Eintragungsausschusses](#23)

[§ 24 Schlichtungsausschuss](#24)

### Abschnitt 4  
Berufspflichten, Ehrenverfahren

[§ 25 Berufspflichten](#25)

[§ 26 Rügerecht des Vorstandes](#26)

[§ 27 Ehrenausschuss](#27)

[§ 28 Ehrenverfahren](#28)

[§ 29 Maßnahmen im Ehrenverfahren](#29)

### Abschnitt 5  
Aufsicht über die Architektenkammer

[§ 30 Aufsichtsbehörde](#30)

[§ 31 Durchführung der Aufsicht](#31)

### Abschnitt 6  
Ordnungswidrigkeiten

[§ 32 Ordnungswidrigkeiten](#32)

### Abschnitt 7  
Übergangs- und Schlussvorschriften

[§ 33 Rechtsverordnungen](#33)

[§ 34 Einschränkung eines Grundrechts](#34)

[§ 35 Übergangsvorschriften](#35)

[§ 36 Inkrafttreten, Außerkrafttreten](#36)

[Anlage 1  Leitlinien zu Ausbildungsinhalten](#37)

[Anlage 2  Prüfraster für die Verhältnismäßigkeitsprüfung](#37)

### Abschnitt 1  
Schutz der Berufsbezeichnungen

#### § 1 Berufsbezeichnungen

(1) Die Berufsbezeichnungen „Architektin“ oder „Architekt“, „Innenarchitektin“ oder „Innenarchitekt“, „Landschaftsarchitektin“ oder „Landschaftsarchitekt“ und „Stadtplanerin“ oder „Stadtplaner“ dürfen nur Personen führen, die in die von der Architektenkammer eines Landes der Bundesrepublik Deutschland geführte Liste der jeweiligen Fachrichtung eingetragen oder die als auswärtige Dienstleister zur Führung der Berufsbezeichnung nach § 2 berechtigt sind.

(2) Soweit dieses Gesetz den Begriff „Architektin“ oder „Architekt“ ohne Benennung der jeweiligen Fachrichtung verwendet, sind die Personen gemeint, die Berufsbezeichnungen nach Absatz 1 führen.

(3) Architektinnen und Architekten können ihre Berufsaufgaben in der Tätigkeitsart freischaffend, gewerblich, angestellt oder im öffentlichen Dienst tätig wahrnehmen.
Dabei bedeutet

1.  freischaffend tätig sein, den Beruf eigenverantwortlich und unabhängig auszuüben,
2.  gewerblich tätig sein, den Beruf nicht ausschließlich freischaffend auszuüben und einen Baubetrieb oder ein gewerbliches Unternehmen zu führen oder an einem solchen beteiligt zu sein,
3.  angestellt tätig sein, den Beruf ausschließlich oder überwiegend in einem sozialversicherungspflichtigen Beschäftigungsverhältnis auszuüben, wobei ein zwischenzeitliches Ausscheiden von höchstens zwei Jahren unschädlich ist,
4.  im öffentlichen Dienst tätig sein, den Beruf ausschließlich oder überwiegend im öffentlichen Dienst auszuüben.

(4) Die Berufsbezeichnung darf auch führen, wer Renten-, Pensions- oder Versorgungszahlungen erhält.

(5) Personen, die die Berufsbezeichnung mit dem Zusatz „frei“ oder „freischaffend“ führen, müssen mit diesem Zusatz in die Liste ihrer Fachrichtung eingetragen sein.
Eigenverantwortlich tätig sind Personen, die ihre berufliche Tätigkeit als Inhaberin oder Inhaber eines Büros oder innerhalb einer Personengesellschaft unmittelbar selbstständig ausüben.
Unabhängig tätig sind Personen, die bei Ausübung ihrer Berufstätigkeit weder eigene Produktions-, Handels- oder Lieferinteressen haben noch fremde Interessen dieser Art vertreten, die unmittelbar oder mittelbar im Zusammenhang mit der beruflichen Tätigkeit stehen.

(6) Wortverbindungen mit den Bezeichnungen nach den Absätzen 1 und 5 oder ähnliche Bezeichnungen, auch in fremdsprachlicher Übersetzung, dürfen Personen nur verwenden, wenn sie die entsprechende Bezeichnung zu führen berechtigt sind.
Dies gilt entsprechend für Bezeichnungen, in denen das Wort „Architektur“ verwendet wird, wie beispielsweise die Bezeichnung „Architekturbüro“.

(7) Das Recht zum Führen akademischer Grade wird durch diese Regelung nicht berührt.

#### § 2 Führung der geschützten Berufsbezeichnungen durch auswärtige Dienstleister

(1) Personen, die in einem anderen Staat niedergelassen sind oder ihren Beruf dort überwiegend ausüben und sich zu einer vorübergehenden und gelegentlichen Erbringung von Dienstleistungen im Sinne des § 3 in das Land Brandenburg begeben (auswärtige Dienstleister), dürfen die Berufsbezeichnung nach § 1 Absatz 1 oder eine Wortverbindung nach § 1 Absatz 6 ohne Eintragung in die Liste ihrer Fachrichtung führen, wenn sie die Eintragungsvoraussetzungen gemäß § 4 Absatz 1 bis 3 erfüllen; § 4 Absatz 4 und 5 findet keine Anwendung.
Sie dürfen den Zusatz „frei“ oder „freischaffend“ führen, wenn sie die Voraussetzungen nach § 1 Absatz 5 erfüllen.

(2) Auswärtige Dienstleister müssen das erstmalige Tätigwerden nach Absatz 1 Satz 1 bei der Architektenkammer vorher schriftlich anzeigen.
Sie haben die Anzeige einmal jährlich zu erneuern, wenn sie beabsichtigen, während des betreffenden Jahres im Land Brandenburg Dienstleistungen nach Absatz 1 Satz 1 zu erbringen.
Auswärtige Dienstleister, die nicht die Voraussetzungen des § 4 Absatz 2 erfüllen, dürfen die Berufsbezeichnung nach § 1 Absatz 1 oder eine Wortverbindung nach § 1 Absatz 6 erst führen, wenn ihnen die Architektenkammer bestätigt hat, dass sie die Eintragungsvoraussetzungen nach § 4 Absatz 1 oder Absatz 3 erfüllen.
Für das Verfahren gilt § 4 Absatz 7 und 8 entsprechend.

(3) Auswärtige Dienstleister haben die Berufspflichten zu beachten.
Sie sind hierfür wie Mitglieder der Architektenkammer zu behandeln und in ein entsprechendes Verzeichnis einzutragen.
Die Architektenkammer stellt über die Eintragung in das Verzeichnis nach Satz 2 eine auf höchstens fünf Jahre befristete Bescheinigung aus, die auf Antrag verlängert werden kann.
Anzeigen nach Absatz 2 Satz 2 und Bescheinigungen nach Satz 3 sind nicht erforderlich, wenn bereits in einem anderen Land eine Anzeige erfolgt ist oder eine Bescheinigung erteilt wurde; eine Eintragung in das Verzeichnis nach Satz 2 erfolgt in diesem Fall nicht.

(4) Das Recht zur Führung der Berufsbezeichnung des Niederlassungsstaates nach Artikel 7 Absatz 3 der Richtlinie 2005/36/EG des Europäischen Parlaments und des Rates vom 7. September 2005 über die Anerkennung von Berufsqualifikationen (ABl. L 255 vom 30.9.2005, S. 22), die zuletzt durch die Richtlinie 2013/55/EU (ABl.
L 354 vom 28.12.2013, S. 132) geändert worden ist, bleibt unberührt.
Die Berufsbezeichnung ist so zu führen, dass keine Verwechslung mit der Berufsbezeichnung nach § 1 Absatz 1 möglich ist.

#### § 3 Berufsaufgaben

(1) Berufsaufgabe der Architektinnen und Architekten in der Fachrichtung Architektur ist insbesondere die gestaltende, technische, wirtschaftliche, umweltgerechte und soziale Planung von Bauwerken unter Beachtung der die Sicherheit der Nutzer und der Öffentlichkeit betreffenden Gesichtspunkte.

(2) Berufsaufgabe der Innenarchitektinnen und Innenarchitekten ist insbesondere die gestaltende, technische, wirtschaftliche, umweltgerechte und soziale Planung von Innenräumen und damit verbundene Änderungen von baulichen Anlagen.

(3) Berufsaufgabe der Landschaftsarchitektinnen und Landschaftsarchitekten ist insbesondere die gestaltende, technische, wirtschaftliche, umweltgerechte und soziale Planung von Landschaften, Parks, Freianlagen und Gärten.

(4) Berufsaufgabe der Stadtplanerinnen und Stadtplaner ist insbesondere die gestaltende, technische, wirtschaftliche, umweltgerechte und soziale Stadt- und Raumplanung, die Erstellung von städtebaulichen Gutachten und die Erarbeitung von Entwicklungs- und Regionalplänen.

(5) Zu den Berufsaufgaben der in den Absätzen 1 bis 4 genannten Personen gehört die Beratung, Betreuung und Vertretung des oder der Auftraggebenden in allen die Planung und Ausführung eines Vorhabens betreffenden Angelegenheiten.
Zu den Berufsaufgaben können auch Sachverständigen-, Forschungs- und Entwicklungstätigkeiten sowie sonstige Dienstleistungen bei der Vorbereitung und Steuerung von Planungs- und Baumaßnahmen, bei der Nutzung von Bauwerken sowie die Wahrnehmung der damit verbundenen sicherheits- und gesundheitstechnischen Belange gehören.

(6) Kennzeichen der beruflichen Tätigkeit ist in allen Fachrichtungen die geistig-schöpferische Bewältigung der Berufsaufgaben unter Berücksichtigung ihrer vollen Komplexität insbesondere auch im Hinblick auf technisch-funktionale, sozioökonomische, baukulturelle, rechtliche und ökologische Belange.
Die Tätigkeit berücksichtigt die Bedürfnisse des Gemeinwesens und achtet dabei das architektonische Erbe sowie die natürlichen Lebensgrundlagen.

#### § 4 Voraussetzungen für die Eintragung in die Architektenlisten

(1) In die Architektenlisten bei der Brandenburgischen Architektenkammer ist auf Antrag einzutragen, wer

1.  im Land Brandenburg die Hauptwohnung oder Niederlassung der beruflichen Tätigkeit hat oder die überwiegende berufliche Beschäftigung ausübt,
2.  ein der jeweiligen Fachrichtung entsprechendes Studium mit einer mindestens vierjährigen Regelstudienzeit an einer deutschen Universität, Hochschule, Fachhochschule oder gleichrangigen Lehranstalt gemäß den in der Anlage 1 zu dieser Vorschrift geregelten Leitlinien zu Ausbildungsinhalten erfolgreich abgeschlossen hat,
3.  danach eine mindestens zweijährige praktische Tätigkeit in der jeweiligen Fachrichtung ausgeübt hat und
4.  im Falle einer freischaffenden oder gewerblichen Tätigkeit für Dritte eine Berufshaftpflichtversicherung nach § 10 hat.

Während der praktischen Tätigkeit sind die für die spätere Berufsausübung erforderlichen Fortbildungsmaßnahmen wahrzunehmen.
In der Fachrichtung Architektur muss die praktische Tätigkeit auf den während des Studiums erworbenen Kenntnissen, Fähigkeiten und Kompetenzen aufbauen und unter Aufsicht einer berufsangehörigen Person oder der Architektenkammer absolviert werden (Berufspraktikum).
In einem anderen Mitgliedstaat oder einem nach dem Recht der Europäischen Union gleichgestellten Staat absolvierte Berufspraktika werden von der Architektenkammer anerkannt, soweit sie den von ihr veröffentlichten Leitlinien nach § 18 Absatz 1 Satz 2 Nummer 10 entsprechen; in einem Drittland absolvierte Berufspraktika werden berücksichtigt.
Die Architektenkammer hat das Berufspraktikum nach Abschluss zu bewerten.
Die praktische Tätigkeit gilt als erbracht, wenn die Bewerberin oder der Bewerber die Befähigung zum höheren technischen Verwaltungsdienst besitzt.

(2) In der Fachrichtung Architektur gelten als mit den Anforderungen des Absatzes 1 Satz 1 gleichwertig die nach den Artikeln 21, 46 und 47 der Richtlinie 2005/36/EG in Verbindung mit deren Anhang V Nummer 5.7.1 in der jeweils geltenden Fassung bekannt gemachten oder als entsprechend anerkannten Ausbildungsnachweise sowie die Nachweise nach den Artikeln 23, 48 und 49 in Verbindung mit dem Anhang VI der Richtlinie 2005/36/EG.

(3) Die Voraussetzung nach Absatz 1 Satz 1 erfüllt unbeschadet des Artikels 10 Buchstabe b, c, d und g der Richtlinie 2005/36/EG auch,

1.  in Bezug auf die Studienanforderungen, wer einen gleichwertigen Studienabschluss an einer ausländischen Hochschule oder an einer sonstigen ausländischen Einrichtung nachweisen kann oder
2.  in Bezug auf die Studienanforderung und die praktische Tätigkeit, wer vorbehaltlich der Absätze 4 und 5
    1.  über einen Berufsqualifikationsnachweis verfügt, der in einem anderen Mitgliedstaat der Europäischen Union erforderlich ist, um dort die Erlaubnis zum Führen der Berufsbezeichnung zu erhalten, oder
    2.  denselben Beruf vollzeitlich ein Jahr lang oder in einer entsprechenden Zeitdauer in Teilzeit in den vorhergehenden zehn Jahren in einem anderen Mitgliedstaat der Europäischen Union, der diesen Beruf nicht reglementiert, ausgeübt hat, sofern die antragstellende Person im Besitz eines oder mehrerer Befähigungs- oder Ausbildungsnachweise ist, die den Anforderungen nach Artikel 13 Absatz 2 der Richtlinie 2005/36/EG entsprechen; die Jahresfrist gilt nur, falls die Reglementierungen des Herkunftsmitgliedstaates nichts anderes bestimmen.

Für die Anerkennung nach Satz 1 Nummer 2 müssen die übrigen Anforderungen an die Befähigungs- oder Ausbildungsnachweise nach Artikel 13 der Richtlinie 2005/36/EG erfüllt sein; dabei sind Ausbildungsgänge oder -nachweise im Sinne des Artikels 3 Absatz 3 und des Artikels 12 der Richtlinie 2005/36/EG gleichgestellt.
Die Sätze 1 bis 2 gelten entsprechend für nach dem Recht der Europäischen Union gleichgestellte Staaten.

(4) Wenn sich die Berufsqualifikation der antragstellenden Person im Sinne von Artikel 14 Absatz 1 der Richtlinie 2005/36/EG wesentlich von den Eintragungsvoraussetzungen nach Absatz 1 unterscheidet, kann die antragstellende Person zu Ausgleichsmaßnahmen in Form eines höchstens dreijährigen Anpassungslehrgangs oder einer Eignungsprüfung verpflichtet werden, um wesentliche Abweichungen in den Ausbildungsinhalten nach Absatz 1 Satz 1 auszugleichen.
Entspricht der Ausbildungsnachweis dem Berufsqualifikationsniveau des Artikels 11 Buchstabe a der Richtlinie 2005/36/EG, kann die Architektenkammer die Eintragung versagen.
In den Fällen von Artikel 11 Buchstabe b der Richtlinie 2005/36/EG sowie in der Fachrichtung Architektur erfolgt die Überprüfung der Fähigkeiten des Antragstellers durch Eignungsprüfung.
Im Übrigen hat die antragstellende Person die Wahl zwischen der Teilnahme an einem Anpassungslehrgang oder einer Eignungsprüfung.

(5) Die Architektenkammer prüft vor der Entscheidung über die Ausgleichsmaßnahme, ob die von der antragstellenden Person durch Berufspraxis oder lebenslanges Lernen erworbenen Kenntnisse, Fähigkeiten und Kompetenzen, die hierfür von einer einschlägigen Stelle formell als gültig anerkannt wurden, wesentliche Unterschiede in den Ausbildungsinhalten nach Absatz 1 Satz 1 ausgleichen.
Art und Umfang einer Ausgleichsmaßnahme sind gegenüber der antragstellenden Person hinreichend zu begründen.
Die antragstellende Person ist insbesondere im Hinblick auf das Niveau der verlangten und der vorgelegten Berufsqualifikation nach Artikel 11 der Richtlinie 2005/36/EG und die wesentlichen Unterschiede in den Ausbildungsinhalten, die nicht durch Kenntnisse, Fähigkeiten und Kompetenzen nach Satz 1 ausgeglichen werden können, zu informieren.
Ist eine Eignungsprüfung erforderlich, ist sicherzustellen, dass diese spätestens sechs Monate nach Zugang der Entscheidung über die Verpflichtung abgelegt werden kann.
Die Architektenkammer erstellt ein Verzeichnis der Sachgebiete, die aufgrund eines Vergleichs der Ausbildungsinhalte nach der in der Anlage 1 geregelten Leitlinien zu Ausbildungsinhalten mit der bisherigen Ausbildung und den als gültig anerkannten Kenntnissen, Fähigkeiten und Kompetenzen nach Satz 1 nicht abgedeckt werden.
Die Prüfung erstreckt sich auf ausgewählte Sachgebiete, deren Kenntnis eine wesentliche Voraussetzung für die Führung der Berufsbezeichnung darstellt.
Die Architektenkammer bewertet abschließend das Ergebnis der Ausgleichsmaßnahme im Hinblick auf die Anerkennung der Berufsqualifikation.
Sie wird ermächtigt, durch Satzung Näheres zu regeln.

(6) Die Eintragung in die Architektenliste der jeweiligen Fachrichtung erfolgt ohne Prüfung der Befähigungsnachweise, wenn eine Löschung aus einer deutschen Architektenliste nicht länger als zwei Jahre zurückliegt und keine Versagungsgründe nach § 5 vorliegen.

(7) Dem Antrag sind die zur Beurteilung der Eintragungsvoraussetzungen erforderlichen Unterlagen beizufügen.
Soweit es um die Beurteilung der in den Absätzen 2 bis 4 genannten Voraussetzungen geht, dürfen nur die in Anhang VII der Richtlinie 2005/36/EG genannten Unterlagen und Bescheinigungen verlangt werden; die in Anhang VII Nummer 1 Buchstabe d, e und f aufgeführten Unterlagen dürfen nicht älter als drei Monate sein.
Die Architektenkammer bestätigt dem Antragsteller binnen eines Monats den Eingang der Unterlagen und Bescheinigungen und teilt ihm gegebenenfalls mit, welche Unterlagen und Bescheinigungen fehlen.
Das Verfahren kann elektronisch geführt werden.
Im Falle begründeter Zweifel und soweit unbedingt geboten können später beglaubigte Kopien verlangt werden.

(8) Die Verfahren nach Absatz 1 und nach § 2 Absatz 1, § 7 Absatz 2 und § 8 Absatz 1 können auch über den Einheitlichen Ansprechpartner im Sinne des Gesetzes über den Einheitlichen Ansprechpartner für das Land Brandenburg vom 7. Juli 2009 (GVBl.
I S. 262), das zuletzt durch Artikel 2 des Gesetzes vom 17.
Dezember 2015 (GVBl.
I Nr. 38 S.
4) geändert worden ist, als Einheitliche Stelle im Sinne der §§ 71a bis 71e des Verwaltungsverfahrensgesetzes abgewickelt werden.

(9) Der Europäische Berufsausweis ist eine elektronische Bescheinigung entweder zum Nachweis, dass die oder der Berufsangehörige sämtliche notwendigen Voraussetzungen für die vorübergehende und gelegentliche Erbringung von Dienstleistungen in einem Aufnahmemitgliedstaat erfüllt oder zum Nachweis der Anerkennung von Berufsqualifikationen für die Niederlassung in einem Aufnahmemitgliedstaat.
Die Architektenkammer ist zuständige Behörde im Sinne der Artikel 4a bis 4e der Richtlinie 2005/36/EG.
Das Verfahren richtet sich nach den Artikeln 4a bis 4e der Richtlinie 2005/36/EG und den hierzu erlassenen Durchführungsrechtsakten.
Der Europäische Berufsausweis stellt die Meldung nach § 2 Absatz 1 dar.
Für die Zwecke der Niederlassung begründet die Ausstellung eines Europäischen Berufsausweises weder ein automatisches Recht zur Ausübung der in § 1 Absatz 1 genannten Berufe noch zur Führung der entsprechenden Berufsbezeichnungen.

(10) Die Architektenkammer ist zuständige Stelle für ein- und ausgehende Meldungen im Sinne des Artikels 56a Absatz 3 der Richtlinie 2005/36/EG; dies gilt nicht, soweit durch Gesetz oder aufgrund eines Gesetzes abweichende Zuständigkeiten bestehen.
Die Architektenkammer unterrichtet unter Berücksichtigung eines von der Kommission nach Artikel 56a Absatz 8 der Richtlinie 2005/36/EG erlassenen Durchführungsrechtsakts die zuständigen Behörden der übrigen Mitgliedstaaten der Europäischen Union sowie der nach dem Recht der Europäischen Union gleichgestellten Staaten, die an das Binnenmarkt-Informationssystem (IMI) angeschlossen sind, spätestens drei Tage nach Wirksamkeit einer Gerichtsentscheidung mittels einer Warnung über das IMI über die Identität von Berufsangehörigen, die die Anerkennung einer Qualifikation gemäß § 2 oder § 4 beantragt haben und bei denen später gerichtlich festgestellt wurde, dass sie dabei gefälschte Berufsqualifikationsnachweise verwendet haben.
Das Verfahren richtet sich nach Artikel 56a der Richtlinie 2005/36/EG sowie den dazu erlassenen Durchführungsrechtsakten.
Die Verarbeitung personenbezogener Daten für die Zwecke des Informationsaustauschs erfolgt im Einklang mit den Richtlinien 95/46/EG und 2002/58/EG.
Gleichzeitig mit der Übermittlung einer Warnung hat die Architektenkammer die hiervon betroffene Person darüber zu unterrichten,

1.  dass eine Warnung erfolgt ist und welchen Inhalt sie hat,
2.  welchen Rechtsbehelf sie gegen die Entscheidung über die Warnung einlegen kann,
3.  dass sie die Berichtigung der Warnung verlangen kann und
4.  dass ihr im Falle einer unrichtigen Übermittlung ein Schadensersatzanspruch zustehen kann.

Wird gegen eine Warnung ein Rechtsbehelf eingelegt, ist über das IMI ein entsprechender Hinweis aufzunehmen.
Werden die in Satz 2 genannten Gerichtsentscheidungen geändert, sind die Warnungen binnen drei Tagen nach Wirksamkeit der Änderung zu löschen.
Die zuständigen Stellen der Länder sind von Meldungen zu unterrichten.

(11) Das Brandenburgische Berufsqualifikationsfeststellungsgesetz vom 5. Dezember 2013 (GVBl. I Nr. 37), das durch Artikel 1 des Gesetzes vom 17.
Dezember 2015 (GVBl.
I Nr. 38) geändert worden ist, findet mit Ausnahme der §§ 14 und 17 keine Anwendung.

#### § 5 Versagung der Eintragung

(1) Die Eintragung in die Listen nach § 1 und die Verzeichnisse nach § 2 Absatz 3, § 7 Absatz 1 und § 8 Absatz 2 ist zu versagen, wenn Tatsachen vorliegen, aus denen sich ergibt, dass die Bewerberin oder der Bewerber nicht die für den Beruf der Architektin oder des Architekten erforderliche Zuverlässigkeit besitzt.
Das ist insbesondere der Fall, wenn

1.  nach § 70 des Strafgesetzbuches oder nach § 132a der Strafprozessordnung die Ausübung des Berufes im Sinne von § 1 dieses Gesetzes untersagt wurde,
2.  nach § 35 der Gewerbeordnung die Berufsausübung untersagt ist oder
3.  im Ehrenverfahren rechtskräftig auf Löschung der Eintragung in der Architektenliste erkannt worden ist.

(2) Die Eintragung der Antragstellerin oder des Antragstellers kann auch versagt werden, wenn

1.  eine Vermögensauskunft nach § 802c der Zivilprozessordnung erteilt wurde,
2.  das Insolvenzverfahren über ihr oder sein Vermögen eröffnet wurde,
3.  die Eröffnung mangels Masse abgewiesen worden ist,
4.  sie oder er besonders schwerwiegend gegen Berufspflichten verstoßen hat oder
5.  einer der Löschungsgründe gemäß § 6 Absatz 2 vorlag

und der jeweilige Versagungsgrund nicht mehr als fünf Jahre zurückliegt.

(3) Die Eintragung ist auch für den vom Ehrenausschuss gemäß § 29 Absatz 1 Satz 2 festgesetzten Zeitraum zu versagen.

#### § 6 Löschung der Eintragung

(1) Die Eintragung in die Liste einer Fachrichtung nach § 1 ist zu löschen, wenn

1.  die eingetragene Person dies beantragt,
2.  die eingetragene Person verstorben ist,
3.  die eingetragene Person ihre Wohnung und ihre Niederlassung sowie ihre überwiegende berufliche Beschäftigung im Land Brandenburg aufgegeben hat,
4.  nach der Eintragung Tatsachen eintreten oder bekannt werden, die im Eintragungsverfahren gemäß § 5 Absatz 1 zu einer Versagung der Eintragung führen müssten oder
5.  in einem Ehrenverfahren rechtskräftig auf Löschung der Eintragung aus der Liste nach § 4 erkannt worden ist.

Satz 1 Nummer 1, 2 und 4 findet auf die Verzeichnisse nach § 2 Absatz 3, § 7 Absatz 1 und § 8 Absatz 2 entsprechende Anwendung.

(2) Die Eintragung in die Liste kann gelöscht werden, wenn

1.  einer der Versagungsgründe des § 5 Absatz 2 vorliegt,
2.  die eingetragene Person über einen Zeitraum von mindestens zwölf Monaten durchgehend keinen Versicherungsschutz im Sinne des § 10 Absatz 1 hatte, obwohl sie dazu verpflichtet war, oder
3.  die eingetragene Person ihren Beitragspflichten als Mitglied der Architektenkammer nicht nachkommt und hierdurch Beiträge offen sind, die in der Summe dem Beitrag von mindestens zwei Jahren entsprechen.

(3) Widerspruch und Anfechtungsklage gegen die Löschung der Eintragung haben keine aufschiebende Wirkung, wenn die Löschungsgründe des Absatzes 1 Satz 1 Nummer 4 oder Nummer 5 oder des Absatzes 2 Nummer 2 oder Nummer 3 vorliegen.
Im Falle von Absatz 2 Nummer 2 oder Nummer 3 gilt dies nur, wenn die Kammer vor dem Beschluss über die Löschung mit einer Frist von mindestens drei Monaten der eingetragenen Person erfolglos Gelegenheit gegeben hat, den jeweiligen Löschungsgrund zu beseitigen.

### Abschnitt 2  
Besondere Bestimmungen für Gesellschaften, Berufshaftpflicht

#### § 7 Gesellschaften

(1) Berufsbezeichnungen, Zusätze oder Wortverbindungen nach § 1 dürfen im Namen einer Partnerschaftsgesellschaft, Partnerschaftsgesellschaft mit beschränkter Berufshaftung oder in der Firma einer Kapitalgesellschaft geführt werden, wenn die Gesellschaft in ein besonderes Verzeichnis bei der Architektenkammer (Gesellschaftsverzeichnis) eingetragen oder als auswärtige Gesellschaft nach § 8 hierzu berechtigt ist.
Mit der Eintragung wird die Gesellschaft nicht Mitglied der Architektenkammer.

(2) Die Gesellschaft ist auf Antrag in das Gesellschaftsverzeichnis einzutragen, wenn sie ihren Sitz im Land Brandenburg hat, das Bestehen einer Haftpflichtversicherung nachweist und der Gesellschaftsvertrag oder die Satzung regelt, dass

1.  Gegenstand des Unternehmens die Wahrnehmung der Berufsaufgaben nach § 3 ist,
2.  die Berufsangehörigen nach § 1 mindestens die Hälfte des Kapitals und der Stimmanteile innehaben und die weiteren Anteile von natürlichen Personen gehalten werden, die aufgrund ihrer Berufsausbildung zum Erreichen des Unternehmenszwecks beitragen können und einen freien Beruf ausüben,
3.  die zur Geschäftsführung befugten Personen mehrheitlich Berufsangehörige nach § 1 sind und gewährleistet ist, dass die Gesellschaft verantwortlich von Berufsangehörigen geführt wird,
4.  Kapitalanteile nicht für Rechnung Dritter gehalten und Stimmrechte nicht für Dritte oder von Dritten ausgeübt werden dürfen,
5.  bei Aktiengesellschaften und Kommanditgesellschaften auf Aktien die Aktien auf Namen lauten,
6.  die Übertragung von Kapital- und Geschäftsanteilen an die Zustimmung aller Gesellschafter gebunden ist und
7.  die für die Berufsangehörigen nach § 25 geltenden Berufspflichten von der Gesellschaft beachtet werden.

Die Berufszugehörigkeit der Gesellschafter, die mindestens ein Viertel des Kapitals und der Stimmanteile an der Gesellschaft innehaben, ist in geeigneter Weise kenntlich zu machen.
Berufsbezeichnungen, Zusätze oder Wortverbindungen nach § 1 und Berufsbezeichnungen nach dem Brandenburgischen Ingenieurgesetz können zusammen geführt werden, wenn abweichend von Satz 1 Nummer 3 die zur Geschäftsführung befugten Personen zusammen mehrheitlich Berufsangehörige nach § 1 und Berufsangehörige nach dem Brandenburgischen Ingenieurgesetz sind und gewährleistet ist, dass die Gesellschaft verantwortlich von diesen Berufsangehörigen geführt wird und die Anforderungen des Satzes 1 im Übrigen erfüllt werden.
Die Eintragung der Gesellschaft kann nur bei einer Kammer erfolgen.

(3) Mit dem Antrag auf Eintragung ist eine öffentlich beglaubigte Ausfertigung des Gesellschaftsvertrages oder der Satzung vorzulegen und die Anmeldung zum Handelsregister oder Partnerschaftsregister nachzuweisen.
Die Architektenkammer hat gegenüber dem Registergericht zu bescheinigen, dass die im Handelsregister oder Partnerschaftsregister einzutragende Gesellschaft die Voraussetzungen zur Eintragung in das Verzeichnis nach Absatz 1 erfüllt.
Änderungen der Eintragung im Handelsregister oder Partnerschaftsregister sind der Architektenkammer von der Gesellschaft unverzüglich anzuzeigen.

(4) Die Eintragung einer Gesellschaft wird gelöscht, wenn

1.  die Gesellschaft nicht mehr besteht,
2.  die geschützte Berufsbezeichnung im Namen oder in der Firma nicht mehr geführt wird,
3.  die Eintragungsvoraussetzungen nicht mehr vorliegen,
4.  die Gesellschaft in Vermögensverfall geraten ist oder
5.  in einem Ehrenverfahren rechtskräftig auf Löschung der Eintragung aus dem Verzeichnis nach Absatz 1 erkannt wurde.

(5) In den Fällen des Absatzes 4 Nummer 3 setzt die Architektenkammer der Gesellschaft eine Frist von höchstens einem Jahr, innerhalb derer die Eintragungsvoraussetzungen wieder erfüllt werden können.
Im Falle des Todes einer Geschäftsführerin oder eines Geschäftsführers oder einer Gesellschafterin oder eines Gesellschafters soll die Frist mindestens ein Jahr und höchstens zwei Jahre betragen.

#### § 8 Auswärtige Gesellschaften

(1) Gesellschaften, die in der Bundesrepublik Deutschland nicht in einem Gesellschaftsverzeichnis eingetragen sind (auswärtige Gesellschaften), dürfen in ihrer Firma oder ihrem Namen die in § 1 genannten Berufsbezeichnungen, Wortverbindungen oder ähnliche Berufsbezeichnungen führen, wenn sie nach dem Recht ihres Herkunftsstaates befugt sind, diese oder vergleichbare Berufsbezeichnungen in ihrer Firma oder ihrem Namen zu führen.
Die Gesellschaften haben das erstmalige Erbringen von Leistungen vorher der Architektenkammer anzuzeigen.
Die Architektenkammer untersagt diesen Gesellschaften das Führen der Berufsbezeichnung, wenn sie auf Verlangen nicht nachweisen, dass

1.  sie oder ihre Gesellschafterinnen oder Gesellschafter und gesetzlichen Vertreterinnen oder Vertreter die betreffende Tätigkeit nach dem Recht des Herkunftsstaates der Gesellschaft rechtmäßig ausüben und
2.  der Gesellschaftsvertrag oder die Satzung die Voraussetzungen gemäß § 7 Absatz 2 Satz 1 Nummer 1 bis 7 erfüllt und eine Berufshaftpflichtversicherung gemäß § 10 Absatz 1 besteht.

(2) Die auswärtigen Gesellschaften werden in das entsprechende Verzeichnis bei der Architektenkammer eingetragen.
§ 2 Absatz 3 gilt entsprechend.
Sie haben die Berufspflichten gemäß § 25 zu beachten.
Für die Verfolgung von Verstößen gilt § 29 entsprechend.

#### § 9 Partnerschaftsgesellschaften

Auf Partnerschaftsgesellschaften gemäß § 8 Absatz 3 des Partnerschaftsgesellschaftsgesetzes vom 25. Juli 1994 (BGBl. I S. 1744), das zuletzt durch Artikel 1 des Gesetzes vom 15. Juli 2013 (BGBl. I S. 2386) geändert worden ist, und auf Partnerschaftsgesellschaften mit beschränkter Berufshaftung gemäß § 8 Absatz 4 des Partnerschaftsgesellschaftsgesetzes findet § 7 Absatz 2 Satz 1 Nummer 1 bis 6 keine Anwendung.

#### § 10 Berufshaftpflichtversicherung

(1) Freischaffend oder gewerblich für Dritte tätige Architektinnen und Architekten im Sinne von § 1 Absatz 3 sowie Gesellschaften im Sinne der §§ 7 und 8 haben zur Deckung der sich aus ihrer Tätigkeit ergebenden Haftpflichtgefahren für die Dauer ihrer Eintragung in die Liste einer Fachrichtung oder in das Gesellschaftsverzeichnis eine Berufshaftpflichtversicherung in angemessener Höhe abzuschließen, diese für die Dauer ihrer jeweiligen Eintragung aufrechtzuerhalten und eine Nachhaftung der Berufshaftpflichtversicherung für mindestens fünf Jahre nach Beendigung des Versicherungsvertrages zu vereinbaren.
Die Mindestversicherungssumme je Versicherungsfall beträgt 300 000 Euro für Sach- und Vermögensschäden und 1,5 Millionen Euro für Personenschäden.
Die Leistungen des Versicherers für alle innerhalb eines Versicherungsjahres verursachten Schäden können auf den zweifachen Betrag der Mindestversicherungssumme begrenzt werden.

(2) Partnerschaftsgesellschaften können ihre Haftung gegenüber Auftraggebern für Ansprüche aus Schäden wegen fehlerhafter Berufsausübung auch durch vorformulierte Vertragsbedingungen auf den vierfachen Betrag der Mindestversicherungssumme für Sach- und Vermögensschäden und den einfachen Betrag der Mindestversicherungssumme für Personenschäden begrenzen.

(3) Partnerschaftsgesellschaften mit beschränkter Berufshaftung gemäß § 8 Absatz 4 des Partnerschaftsgesellschaftsgesetzes haften für Verbindlichkeiten aus Schäden wegen fehlerhafter Berufsausübung nur in Höhe ihres Gesellschaftsvermögens, wenn sie zu diesem Zweck eine Berufshaftpflichtversicherung entsprechend Absatz 1 unterhalten und den Namenszusatz „mit beschränkter Berufshaftung“ oder die Abkürzung „mbB“ oder eine andere allgemein verständliche Abkürzung dieser Bezeichnung führen.

(4) Die Architektenkammer überwacht das Bestehen eines ausreichenden Versicherungsschutzes.
Sie ist für die in die Liste der jeweiligen Fachrichtung und in die Verzeichnisse Eingetragenen zuständige Stelle im Sinne des § 117 Absatz 2 des Versicherungsvertragsgesetzes vom 23. November 2007 (BGBl.
I S. 2631), das zuletzt durch Artikel 8 Absatz 21 des Gesetzes vom 17. Juli 2015 (BGBl.
I S. 1245) geändert worden ist.
Bescheinigungen von in anderen Mitgliedstaaten niedergelassenen Kreditinstituten und Versicherern, dass ein Versicherungsschutz gemäß Absatz 1 für Tätigkeiten im Geltungsbereich der Eintragung besteht, werden in der bescheinigten Höhe dann anerkannt, wenn die Deckung im selben Rang wie weitere zur Erreichung der erforderlichen Gesamtdeckung vorgesehene Versicherungen zugesagt ist.

### Abschnitt 3  
Architektenkammer

#### § 11 Brandenburgische Architektenkammer

(1) Die in die jeweilige Liste eingetragenen Architektinnen und Architekten, Innenarchitektinnen und Innenarchitekten, Landschaftsarchitektinnen und Landschaftsarchitekten sowie Stadtplanerinnen und Stadtplaner bilden als Mitglieder die Brandenburgische Architektenkammer (Architektenkammer).

(2) Als Anwärterin oder Anwärter werden die Personen in ein besonderes Verzeichnis eingetragen, die im Sinne des § 4 Absatz 1 Satz 1 Nummer 2

1.  einen berufsqualifizierenden Hochschulabschluss anstreben und eine entsprechende Studiendauer von mindestens drei Jahren nachweisen oder
2.  einen berufsqualifizierenden Hochschulabschluss nachweisen und die zweijährige praktische Tätigkeit ausüben.

(3) Die Architektenkammer ist eine Körperschaft des öffentlichen Rechts.
Sie führt ein Dienstsiegel.

(4) Dienstsitz der Architektenkammer ist Potsdam.
Die Architektenkammer kann durch Satzung Fachgruppen und örtliche Untergliederungen bilden.

#### § 12 Aufgaben der Architektenkammer

(1) Aufgabe der Architektenkammer ist es,

1.  die Baukultur, die Baukunst, das Bauwesen, den Städtebau und die Landschaftspflege unter Beachtung des Schutzes der natürlichen Lebensgrundlagen zu fördern,
2.  die beruflichen Belange der Gesamtheit der Mitglieder sowie das Ansehen des Berufsstandes zu wahren,
3.  die Listen der Fachrichtungen zu führen und die für die Berufsausübung und das Führen der Berufsbezeichnung notwendigen Urkunden und Bescheinigungen auszustellen,
4.  die Verzeichnisse nach § 2 Absatz 3, nach § 7 Absatz 1 und nach § 8 Absatz 2 sowie nach § 11 Absatz 2, die Verzeichnisse zur Berufsausübung für Fachaufgaben und nach anderen gesetzlichen Vorschriften zu führen und die dazu notwendigen Urkunden und Bescheinigungen auszustellen,
5.  die Berufsqualifikationen zu überprüfen und anzuerkennen, Ausgleichsmaßnahmen anzuordnen und zu bewerten,
6.  die während der zweijährigen praktischen Tätigkeit sowie der begleitenden Fort- und Weiterbildungsmaßnahmen mindestens zu bearbeitenden Aufgaben und Inhalte festzulegen, Berufspraktika zu beaufsichtigen und zu bewerten,
7.  die Erfüllung der beruflichen Pflichten zu überwachen und Verletzungen zu ahnden,
8.  die berufliche Aus-, Fort- und Weiterbildung sowie entsprechende Einrichtungen für die Aus-, Fort- und Weiterbildung zu fördern,
9.  in allen die Berufsaufgaben betreffenden Fragen gegenüber Behörden und Gerichten Stellung zu nehmen, Vorschläge zu machen und Gutachten zu erstatten,
10.  auf die Beilegung von Streitigkeiten, die sich aus der Berufsausübung zwischen Berufsangehörigen oder zwischen diesen und Dritten ergeben, hinzuwirken,
11.  das Sachverständigenwesen zu fördern, Sachverständige öffentlich zu bestellen, zu vereidigen, in einem Verzeichnis zu führen und auf Anforderung von Behörden und Gerichten sowie Dritter öffentlich bestellte und vereidigte Sachverständige zu benennen,
12.  die Berufsangehörigen in Fragen der Berufsausübung zu beraten,
13.  Planungswettbewerbe zu fördern und bei der Regelung des Wettbewerbswesens mitzuwirken und
14.  die Zusammenarbeit mit anderen Architektenkammern, Berufskammern, Hochschulen, Verbänden und Vereinen zu pflegen und zu fördern.

(2) Die Architektenkammer kann aufgrund einer Satzung zur Durchführung der Aufgaben nach Absatz 1 Nummer 1, 2 und 6 besondere Einrichtungen schaffen oder sich an anderen Einrichtungen beteiligen.

(3) Die Architektenkammer nimmt beim Vollzug dieses Gesetzes die in Artikel 56 Absatz 1 bis 3 der Richtlinie 2005/36/EG genannten Aufgaben als zuständige Behörde wahr.

#### § 13 Versorgungswerk

(1) Die Architektenkammer kann durch Satzung für ihre Mitglieder und deren Kinder, Ehegatten oder eingetragene Lebenspartner ein Versorgungswerk errichten, sich einer anderen Versorgungs- oder Versicherungseinrichtung in der Bundesrepublik Deutschland anschließen, zusammen mit einer oder mehreren Versorgungseinrichtungen eine gemeinsame Versorgungseinrichtung schaffen oder andere Versorgungs- oder Versicherungseinrichtungen aufnehmen.
Dem Versorgungswerk gehören auch die Personen an, die die Voraussetzungen zur Eintragung mit Ausnahme der zweijährigen praktischen Tätigkeit erfüllen.
Mitglieder, deren Versorgung nach beamtenrechtlichen Vorschriften geregelt ist, dürfen nicht zur Teilnahme verpflichtet werden.

(2) Das Versorgungswerk wird gerichtlich und außergerichtlich durch die Vorsitzende oder den Vorsitzenden seines Aufsichtsorgans vertreten.

(3) Die Architektenkammer kann die Mitglieder anderer Architektenkammern oder Ingenieurkammern in Versorgungseinrichtungen aufnehmen.
Sie kann sich dem Versorgungswerk einer anderen Architektenkammer oder Ingenieurkammer in der Bundesrepublik Deutschland anschließen oder zusammen mit einem anderen oder mehreren Versorgungswerken ein gemeinsames Versorgungswerk schaffen.

(4) Die Satzung muss bestimmen, dass Vermögen und Verwaltung des Versorgungswerkes unabhängig und getrennt von Vermögen, Verwaltung, Haushalt und Organen der Architektenkammer sind.
Die §§ 54 und 54d des Versicherungsaufsichtsgesetzes in der Fassung der Bekanntmachung vom 17. Dezember 1992 (BGBl. 1993 I S. 2), das zuletzt durch Artikel 353 der Verordnung vom 31. August 2015 (BGBl.
I S. 1474) geändert worden ist, gelten entsprechend.
Die Anlageverordnung vom 20.
Dezember 2001 (BGBl.
I S. 3913), die zuletzt durch Artikel 1 der Verordnung vom 3. März 2015 (BGBl.
I S. 188) geändert worden ist, ist entsprechend anzuwenden.

(5) Die Satzung muss ferner Bestimmungen enthalten über

1.  die versicherungspflichtigen Mitglieder,
2.  die Höhe und Art der Versicherungsleistungen,
3.  die Ermittlung und die Höhe der Beiträge,
4.  Beginn und Ende der Teilnahme,
5.  die Befreiung von der Teilnahme,
6.  die freiwillige Teilnahme und
7.  die Bildung, Zusammensetzung, Wahl, Amtsdauer und Aufgabe besonderer Organe für das Versorgungswerk.

(6) Die Satzung bedarf der Genehmigung der Aufsichtsbehörde nach § 30 sowie der für das Versicherungswesen zuständigen obersten Landesbehörde.
Sie ist gemäß § 18 Absatz 3 bekannt zu machen.

#### § 14 Organe der Architektenkammer

(1) Die Organe der Architektenkammer sind

1.  die Vertreterversammlung und
2.  der Vorstand.

(2) Den Organen der Architektenkammer dürfen nur Kammermitglieder angehören.
Die in die Organe der Architektenkammer gewählten Mitglieder sind zur Annahme und Ausübung ihres Amtes verpflichtet, soweit nicht ein wichtiger Grund entgegensteht.
Die Pflicht zur Ausübung des Amtes besteht über die Amtsdauer hinaus bis zum Amtsantritt des neuen Mitglieds.
Angehörige der Aufsichtsbehörde nach § 30, die mit der Aufsicht über die Architektenkammer nach § 31 befasst sind, dürfen nicht Mitglieder der Organe sein.

(3) Die Mitglieder der Organe sind ehrenamtlich tätig.
Sie haben Anspruch auf Entschädigung für Auslagen und Zeitversäumnis.

#### § 15 Vertreterversammlung der Architektenkammer

(1) Die Mitglieder der Vertreterversammlung werden von den Mitgliedern der Architektenkammer für die Dauer von fünf Jahren in allgemeiner, gleicher, unmittelbarer, geheimer und freier Wahl gewählt.
In das Verzeichnis nach § 11 Absatz 2 eingetragene Anwärterinnen und Anwärter haben kein Wahlrecht.

(2) Die Architektenkammer erlässt die Wahlordnung.
Sie regelt das Nähere über die Ausübung des Wahlrechts, die Durchführung der Wahl und die vorzeitige Beendigung der Mitgliedschaft in der Vertreterversammlung.
Es sind 31 Vertreterinnen und Vertreter in die Vertreterversammlung zu wählen.

(3) Die Vertreterversammlung ist mindestens einmal jährlich einzuberufen.
Außerordentliche Vertreterversammlungen sind binnen einer Frist von zwei Monaten einzuberufen, wenn es der Vorstand beschließt, wenn mindestens ein Drittel der Mitglieder der Vertreterversammlung unter Angabe des Verhandlungsgegenstands dies schriftlich beantragt oder auf Verlangen der Aufsichtsbehörde.

#### § 16 Aufgaben der Vertreterversammlung

(1) Die Vertreterversammlung beschließt insbesondere über

1.  die Satzungen (§ 18),
2.  die Wahl und Abberufung der Mitglieder des Vorstandes (§ 17),
3.  das Ergebnis der Prüfung der Jahresrechnung, die Entlastung der Mitglieder des Vorstandes und die Wahl der Rechnungsprüferinnen und Rechnungsprüfer,
4.  den Erwerb, die Belastung und die Veräußerung von Grundstücken und grundstücksgleichen Rechten sowie die Beteiligung an Unternehmen und die Mitgliedschaft in Vereinigungen und Verbänden,
5.  die Wahl der Mitglieder des Eintragungsausschusses (§ 22),
6.  die Wahl der Mitglieder des Ehrenausschusses (§ 27),
7.  die Bildung weiterer Ausschüsse sowie die Wahl und Abwahl der Mitglieder dieser Ausschüsse,
8.  die Höhe der Entschädigung für die Mitglieder der Organe (§ 14 Absatz 3), des Eintragungsausschusses, des Ehrenausschusses und der weiteren Ausschüsse (Nummer 7) und
9.  die Bildung eines Versorgungswerks (§ 13).

(2) Für Beschlüsse im Sinne des Absatzes 1 ist die Vertreterversammlung beschlussfähig, wenn mehr als die Hälfte der Mitglieder anwesend ist.
Ist eine Angelegenheit wegen Beschlussunfähigkeit der Versammlung zurückgestellt worden und tritt die Vertreterversammlung zur Verhandlung über denselben Gegenstand zum zweiten Male zusammen, so ist sie ohne Rücksicht auf die Zahl der Erschienenen beschlussfähig.
In der Ladung zur zweiten Sitzung muss auf diese Vorschrift ausdrücklich hingewiesen werden.

(3) Die Beschlüsse werden mit der Mehrheit der abgegebenen Stimmen gefasst.
Bei Stimmengleichheit ist ein Antrag abgelehnt.

(4) Beschlüsse zu Satzungen und deren Änderungen sowie zur vorzeitigen Abwahl von Mitgliedern des Vorstandes bedürfen der Mehrheit der Stimmen der Mitglieder der Vertreterversammlung.

#### § 17 Vorstand der Architektenkammer

(1) Der Vorstand wird von der Vertreterversammlung für die Dauer von fünf Jahren gewählt.
Er besteht aus der Präsidentin oder dem Präsidenten, zwei Vizepräsidentinnen oder Vizepräsidenten und vier Beisitzerinnen oder Beisitzern.

(2) Der Vorstand führt die Geschäfte der Architektenkammer.
Er bedient sich hierzu einer Geschäftsführerin oder eines Geschäftsführers.
Für die Geschäfte der laufenden Verwaltung ist die Geschäftsführerin oder der Geschäftsführer zuständig.

(3) Die Präsidentin oder der Präsident vertritt die Architektenkammer gerichtlich und außergerichtlich.

(4) Erklärungen, durch welche die Architektenkammer vermögensrechtlich verpflichtet werden soll, bedürfen der Schriftform.
Sie sind von der Präsidentin oder dem Präsidenten zusammen mit einem Mitglied des Vorstandes oder der Präsidentin oder dem Präsidenten zusammen mit der Geschäftsführerin oder dem Geschäftsführer zu unterzeichnen.
Satz 2 gilt nicht für Geschäfte der laufenden Verwaltung.
Näheres regelt die Hauptsatzung der Architektenkammer.

#### § 18 Satzungen

(1) Die Architektenkammer kann zur Regelung ihrer Angelegenheiten und im Rahmen der Bestimmungen dieses Gesetzes Satzungen erlassen.
Sie hat in der Form der Satzung Bestimmungen zu treffen über

1.  die innere Verfassung der Architektenkammer (Hauptsatzung),
2.  die Wahlordnung zur Vertreterversammlung,
3.  die Beitragsordnung,
4.  die Gebührenordnung,
5.  die Haushalts- und Kassenordnung,
6.  die Sachverständigenordnung,
7.  die Schlichtungsordnung,
8.  den Beschluss über den Haushaltsplan,
9.  die Ehrenordnung,
10.  die Fortbildungs- und Praktikumsordnung, die insbesondere Regelungen für die zweijährige praktische Tätigkeit einschließlich erforderlicher Fortbildungsmaßnahmen, deren Bewertung sowie die Organisation, Anerkennung und Überwachung von im Ausland erbrachten Teilen der praktischen Tätigkeit trifft, und
11.  die Berufsanerkennungsordnung, die insbesondere die Anordnung, Durchführung und Bewertung von Ausgleichsmaßnahmen nach § 4 Absatz 4 und 5 regelt.

(2) Bei Vorschriften, die dem Geltungsbereich der Richtlinie (EU) 2018/958 des Europäischen Parlaments und des Rates vom 28.
Juni 2018 über eine Verhältnismäßigkeitsprüfung vor Erlass neuer Berufsreglementierungen (ABI.
L 173 vom 9.7.2018, S.
25) in der jeweils geltenden Fassung unterfallen, sind die Vorgaben dieser Richtlinie einzuhalten.
Eine Vorschrift im Sinne des Satzes 1 ist anhand der in der Anlage 2 festgelegten Kriterien auf ihre Verhältnismäßigkeit zu überprüfen.

(3) Vor der Beschlussfassung der Vertreterversammlung über eine Vorschrift im Sinne des Absatzes 2 Satz 1 ist auf der Internetseite der Kammer ein Entwurf für einen Zeitraum von mindestens zwei Wochen mit der Gelegenheit zur Stellungnahme zu veröffentlichen.
Das Nähere wird durch die Hauptsatzung bestimmt; insbesondere ist sicherzustellen, dass eingehende Stellungnahmen in den Entscheidungsprozess der Vertreterversammlung einfließen können.

(4) Die Satzungen nach Absatz 1 Satz 2 Nummer 1, 2, 10 und 11, deren Änderungen sowie der Beschluss nach Absatz 1 Satz 2 Nummer 8 bedürfen der Genehmigung der Aufsichtsbehörde.
Alle sonstigen Vorschriften, die die Voraussetzungen des Absatzes 2 Satz 1 erfüllen, sind der Aufsichtsbehörde spätestens vier Wochen vor Beschlussfassung anzuzeigen.
Im Rahmen der Genehmigung und Prüfung von Satzungen im Sinne des Absatzes 2 Satz 1 hat die Aufsichtsbehörde auch zu prüfen, ob die Vorgaben der Richtlinie (EU) 2018/958 eingehalten werden.
Zu diesem Zweck hat die Kammer der Aufsichtsbehörde Unterlagen zuzuleiten, aus denen sich die Einhaltung der Vorgaben nach Absatz 2 Satz 2 und Absatz 3 ergibt.
Insbesondere hat die Kammer die Gründe zu übermitteln, aufgrund derer sie die Vorschrift als gerechtfertigt, notwendig und verhältnismäßig im Sinne der Richtlinie (EU) 2018/958 beurteilt hat.

(5) Die Satzungen und der Beschluss nach Absatz 1 Satz 2 Nummer 8 sind in ausgefertigter und gegebenenfalls genehmigter Fassung im Mitteilungsblatt der Architektenkammer zu veröffentlichen.

(6) Die Kammer hat nach dem Erlass einer Vorschrift gemäß Absatz 2 Satz 1 ihre Übereinstimmung mit dem Verhältnismäßigkeitsgrundsatz zu überwachen und bei einer Änderung der Umstände zu prüfen, ob die Vorschrift anzupassen ist.
Die Aufsichtsbehörde stellt sicher, dass die Gründe, nach denen die Vorschriften als gerechtfertigt, notwendig und verhältnismäßig beurteilt wurden und die der Europäischen Kommission nach Artikel 59 Absatz 5 der Richtlinie 2005/36/EG mitzuteilen sind, in die in Artikel 59 Absatz 1 Satz 3 der Richtlinie 2005/36/EG genannte Datenbank für reglementierte Berufe eingegeben werden und nimmt die zu den Eintragungen vorgebrachten Stellungnahmen anderer Mitgliedstaaten der Europäischen Union und diesen gleichgestellten Staaten sowie interessierter Kreise entgegen.

(7) Die Absätze 2 bis 6 gelten sowohl für den Erlass als auch für die Änderung und die Aufhebung von Satzungen.

#### § 19 Hauptsatzung

(1) Die Hauptsatzung muss Bestimmungen enthalten über

1.  die Rechte der Kammermitglieder und die Pflichten, die sich aus der Mitgliedschaft in der Architektenkammer ergeben,
2.  die Einberufung und die Geschäftsordnung der Vertreterversammlung der Architektenkammer,
3.  die Zusammensetzung des Vorstandes der Architektenkammer sowie die Wahl und die Abwahl von dessen Mitgliedern,
4.  die Geschäftsführung und die Verwaltungseinrichtungen der Architektenkammer,
5.  die Zusammensetzung der Ausschüsse der Architektenkammer, falls solche gebildet werden, sowie die Wahl und die Abwahl von deren Mitgliedern,
6.  die Bildung örtlicher und fachlicher Untergliederungen,
7.  die Führung der Listen der Fachrichtungen und der Verzeichnisse und
8.  die Form und die Art der Bekanntmachungen.

(2) Die Hauptsatzung ist so auszugestalten, dass die Wahrung der Belange aller Fachrichtungen und Tätigkeitsarten gesichert ist.

#### § 20 Finanzwesen

(1) Der Finanzbedarf der Architektenkammer wird, soweit er nicht anderweitig gedeckt werden kann, durch Beiträge der Kammermitglieder aufgebracht.
Die Beiträge können nach der Höhe der Einnahmen der Mitglieder aus ihrer Berufstätigkeit als Architektinnen oder Architekten gestaffelt werden.
Das Nähere bestimmt die Beitragsordnung (§ 18 Absatz 1 Satz 2 Nummer 3).

(2) Für die Inanspruchnahme von Einrichtungen und Gegenständen, Amtshandlungen und besonderen Leistungen hat die Architektenkammer Gebühren zu erheben.
Das Nähere bestimmt die Gebührenordnung (§ 18 Absatz 1 Satz 2 Nummer 4).

(3) Die Architektenkammer ist hinsichtlich ihrer Geldforderungen Vollstreckungsbehörde im Sinne des Verwaltungsvollstreckungsgesetzes für das Land Brandenburg vom 16.
Mai 2013 (GVBl.
I Nr. 18), das durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 32 S. 23) geändert worden ist.

(4) Näheres zum Finanzwesen der Kammer bestimmt die Haushalts- und Kassenordnung (§ 18 Absatz 1 Satz 2 Nummer 5).

#### § 21 Datenverarbeitung, Pflicht zur Verschwiegenheit, Auskünfte

(1) Die Mitglieder der Organe, der Ausschüsse und der Einrichtungen der Architektenkammer, deren Hilfskräfte sowie die hinzugezogenen Sachverständigen sind zur Verschwiegenheit über alle Angelegenheiten verpflichtet, die ihnen im Zusammenhang mit ihrer Tätigkeit bekannt geworden sind.
Dies gilt nicht für Mitteilungen im amtlichen Verkehr und über Tatsachen, die offenkundig sind oder ihrer Bedeutung nach keiner Geheimhaltung bedürfen.
Sie dürfen die Kenntnis der nach Satz 1 geheim zu haltenden Angelegenheiten nicht unbefugt verwerten.
Die Pflichten nach den Sätzen 1 und 3 bestehen nach Beendigung ihrer Tätigkeit fort.

(2) Die Architektenkammer darf personenbezogene Daten verarbeiten, soweit dies zur rechtmäßigen Erfüllung der Kammeraufgaben nach diesem Gesetz erforderlich ist.
Zu diesem Zweck dürfen über Kammerangehörige, Gesellschaften, Geschäftsführerinnen oder Geschäftsführer und Abwicklerinnen oder Abwickler von Gesellschaften nach § 7 und über Personen, die einen Eintragungsantrag gestellt oder Dienstleistungen nach § 2 Absatz 2 und § 8 Absatz 1 angezeigt haben, insbesondere folgende Daten verarbeitet werden:

1.  Familien-, Vor- und Geburtsnamen, Geschlecht, akademische Grade,
2.  Geburtsdaten,
3.  Anschriften der Wohnung sowie der beruflichen Niederlassung und des Dienst- oder Beschäftigungsortes,
4.  Fachrichtung und Tätigkeitsart,
5.  Angaben zur Berufsausbildung und bisherigen praktischen Tätigkeit,
6.  Angaben zur Eintragung in eine Architektenliste oder in ein Verzeichnis gemäß § 2 Absatz 3, § 7 Absatz 1, § 8 Absatz 2 oder § 11 Absatz 2 und
7.  Eintragungsversagungen, Berufspflichtverletzungen, Maßnahmen in einem Ehrenverfahren, Sperrungen und Löschungen in den in Nummer 6 genannten Listen und Verzeichnissen sowie personenbezogene Daten im Zusammenhang mit der Richtlinie 2005/36/EG.

Die in Satz 2 Nummer 1, 3 und 4 genannten Daten sind in die Architektenlisten und die Verzeichnisse nach § 2 Absatz 3, §§ 4, 7 Absatz 1 oder § 8 Absatz 2 einzutragen.

(3) Jede Person hat bei Darlegung eines berechtigten Interesses das Recht auf Auskunft aus den Listen der Fachrichtungen und den Verzeichnissen nach § 2 Absatz 3, § 7 Absatz 1 und § 8 Absatz 2.
Die in den genannten Listen und Verzeichnissen enthaltenen Angaben dürfen von der Architektenkammer veröffentlicht oder an Dritte zum Zwecke der Veröffentlichung übermittelt werden, sofern die betroffene Person nicht widerspricht.
Die Übermittlung von Daten an nicht-öffentliche Stellen ist nur zulässig, wenn sich die nicht-öffentliche Stelle verpflichtet hat, die Daten nur für den Zweck zu verarbeiten, zu dessen Erfüllung sie ihr übermittelt werden sollen.

(4) Die Architektenkammer ist berechtigt, Daten aus den Architektenlisten und den Verzeichnissen nach § 2 Absatz 3, § 7 Absatz 1 und § 8 Absatz 2, insbesondere zu Eintragungsanträgen und Anzeigen nach § 2 Absatz 1, Versagungen und Löschungen sowie über Maßnahmen in einem Ehrenverfahren an Behörden in der Bundesrepublik Deutschland und auswärtiger Staaten zu übermitteln und von diesen einzuholen.
Bei Staatsangehörigen eines Mitgliedstaates der Europäischen Union oder eines anderen Vertragsstaates des Abkommens über den Europäischen Wirtschaftsraum hat die Architektenkammer auf Anfrage der zuständigen Behörde eines Mitgliedstaates oder Vertragsstaates die entsprechenden Daten zu übermitteln.
Die Architektenkammer erteilt die nach der Richtlinie 2005/36/EG erforderlichen Auskünfte und stellt die notwendigen Bescheinigungen aus; sie ist insoweit zuständige Behörde.

(5) In den Fällen der Löschung nach § 6 ist die Verarbeitung aller bei der Architektenkammer über die betroffene Person gespeicherter Daten gemäß Artikel 18 der Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates vom 27.
April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr und zur Aufhebung der Richtlinie 95/46/EG (Datenschutz-Grundverordnung) (ABl.
L 119 vom 4.5.2016, S.
1; L 314 vom 22.11.2016, S. 72) einzuschränken.
Die Verarbeitung von Angaben über Maßnahmen in einem Ehrenverfahren ist in jedem Fall nach fünf Jahren ab deren Verhängung einzuschränken.

(6) Rügen nach § 26 und Verweise nach § 29 Absatz 1 Nummer 1 und Absatz 2 Nummer 1 sind nach Ablauf von fünf Jahren zu löschen, wenn die betroffene Person sich innerhalb dieses Zeitraums keiner weiteren Berufspflichtverletzung schuldig gemacht hat.

(7) Durch Maßnahmen aufgrund der Absätze 2 bis 5 wird das Grundrecht auf informationelle Selbstbestimmung (Artikel 11 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 22 Einrichtung, Zusammensetzung und Wahl des Eintragungsausschusses

(1) Die Architektenkammer bildet einen Eintragungsausschuss mit den Tätigkeiten nach § 23.

(2) Der Eintragungsausschuss besteht aus der Ausschussvorsitzenden oder dem Ausschussvorsitzenden und der erforderlichen Zahl von Beisitzerinnen oder Beisitzern.
Für den Vorsitz sind Stellvertreterinnen oder Stellvertreter zu bestellen.
Der Eintragungsausschuss entscheidet in der Besetzung mit der Vorsitzenden oder dem Vorsitzenden und vier Beisitzerinnen oder Beisitzern.

(3) Die Ausschussvorsitzende oder der Ausschussvorsitzende und die Stellvertreterinnen und die Stellvertreter sollen die Befähigung zum Richteramt nach dem Deutschen Richtergesetz haben oder Diplomjuristin oder Diplomjurist sein.
Die Beisitzerinnen und die Beisitzer müssen in die Listen der Fachrichtungen eingetragen sein.
Die Mitglieder des Eintragungsausschusses dürfen weder dem Vorstand der Architektenkammer angehören noch Dienstkräfte der Architektenkammer oder Mitglieder der Aufsichtsbehörde sein, die mit der Aufsicht über die Architektenkammer nach § 31 befasst sind.

(4) Die Mitglieder des Eintragungsausschusses und ihre Stellvertreterinnen und Stellvertreter werden für die Dauer von fünf Jahren von der Vertreterversammlung gewählt.

#### § 23 Tätigkeit des Eintragungsausschusses

(1) Der Eintragungsausschuss trifft die Entscheidungen, die sich auf die Listen der Fachrichtungen und das Verzeichnis nach § 7 Absatz 1 beziehen.
Die Entscheidung ist unverzüglich, spätestens jedoch binnen drei Monaten nach Einreichung der vollständigen Unterlagen zu treffen.
In den Fällen des § 4 Absatz 2 und 3 kann die Frist um einen Monat verlängert werden.
Satz 1 gilt entsprechend für die Verzeichnisse nach § 2 Absatz 3 und § 8 Absatz 2.
Die Verfahrensfrist läuft ab dem Tag, in dem der Antrag oder ein fehlendes Dokument beim Einheitlichen Ansprechpartner oder unmittelbar bei der Architektenkammer eingereicht wird.
Eine Aufforderung zur Vorlage von beglaubigten Kopien gilt nicht als Aufforderung zur Vorlage fehlender Dokumente.

(2) Der Eintragungsausschuss ist bei seiner Entscheidung unabhängig und nicht an Weisungen gebunden.
Er entscheidet nach seiner freien, aus dem Gang des gesamten Verfahrens gewonnenen Überzeugung.
Die Ausschussvorsitzende oder der Ausschussvorsitzende teilt der antragstellenden Person die Entscheidung mit.
Eine ablehnende Entscheidung ist zu begründen.

(3) Die Mitglieder des Eintragungsausschusses sind ehrenamtlich tätig.

(4) Die Sitzungen des Eintragungsausschusses sind nicht öffentlich.
Bei der Entscheidung des Eintragungsausschusses sollen mindestens zwei Beisitzerinnen oder Beisitzer der Fachrichtung der betroffenen Person angehören.
Eine oder einer von ihnen soll die gleiche Ausbildung wie die betroffene Person abgeschlossen haben.

(5) In gerichtlichen Verfahren, die Entscheidungen des Eintragungsausschusses betreffen, wird die Architektenkammer durch die Vorsitzende oder den Vorsitzenden des Eintragungsausschusses vertreten.

#### § 24 Schlichtungsausschuss

(1) Zur gütlichen Beilegung von Streitigkeiten, die sich aus der Berufsausübung zwischen Kammermitgliedern oder zwischen diesen und Dritten ergeben, ist ein Schlichtungsausschuss zu bilden.
Der Schlichtungsausschuss wird in der Besetzung mit einer Vorsitzenden oder einem Vorsitzenden und zwei Beisitzerinnen oder Beisitzern tätig.
Die oder der Vorsitzende soll die Befähigung zum Richteramt haben oder Diplomjuristin oder Diplomjurist sein.
Die Beisitzerinnen oder Beisitzer müssen Mitglieder der Architektenkammer sein.
Das Verfahren regelt die Schlichtungsordnung.

(2) Bei Streitigkeiten zwischen Kammermitgliedern hat der Schlichtungsausschuss auf Anruf durch einen der Beteiligten oder auf Anordnung des Vorstandes einen Schlichtungsversuch zu unternehmen.
Sind Dritte beteiligt, so kann der Schlichtungsausschuss nur mit deren Einverständnis tätig werden.

### Abschnitt 4  
Berufspflichten, Ehrenverfahren

#### § 25 Berufspflichten

(1) Die Kammermitglieder und die auswärtigen Dienstleister nach § 2 sind verpflichtet, ihren Beruf gewissenhaft und unter Beachtung des Rechts auszuüben, dem ihnen im Zusammenhang mit dem Beruf entgegengebrachten Vertrauen zu entsprechen und alles zu unterlassen, was dem Ansehen des Berufsstandes schaden könnte.
Sie sind insbesondere verpflichtet,

1.  sich beruflich fort- und weiterzubilden,
2.  sich unter Beachtung von § 10 ausreichend gegen Haftpflichtansprüche zu versichern,
3.  sich an Wettbewerben nur zu beteiligen, wenn durch die Verfahrensbedingungen ein lauterer Leistungsvergleich sichergestellt ist und in ausgewogener Weise den Belangen von Auslobenden sowie Teilnehmenden Rechnung getragen wird,
4.  nur solche Pläne und Bauvorlagen mit ihrer Unterschrift zu versehen, die von ihnen selbst oder unter ihrer Leitung oder Verantwortung gefertigt wurden,
5.  das geltende Lohn- und Preisrecht zu beachten und
6.  die sich aus anderen Rechtsvorschriften ergebenden Berufspflichten zu beachten.

(2) Ein außerhalb des Berufs liegendes Verhalten ist eine Berufspflichtverletzung, wenn es nach den Umständen des Einzelfalles in besonderem Maße geeignet ist, Achtung und Vertrauen in einer für die Ausübung der Berufstätigkeit oder für das Ansehen des Berufsstandes bedeutsamen Weise zu beeinträchtigen.

(3) Absatz 1 Satz 2 Nummer 3 bis 6 und Absatz 2 gelten für Gesellschaften nach § 7 entsprechend.

#### § 26 Rügerecht des Vorstandes

(1) Der Vorstand kann das Verhalten eines Kammermitglieds, durch das die ihm obliegenden Berufspflichten verletzt wurden, rügen, wenn die Schuld gering ist und ein Antrag auf Einleitung eines Ehrenverfahrens nicht erforderlich erscheint.
Architektinnen oder Architekten im öffentlichen Dienst unterliegen hinsichtlich ihrer dienstlichen Tätigkeit nicht dem Rügerecht.

(2) Das Rügerecht erlischt, sobald das Ehrenverfahren gegen das Mitglied eingeleitet ist.
§ 29 Absatz 3 gilt entsprechend.

(3) Bevor die Rüge erteilt wird, ist das Mitglied zu hören.

(4) Der Bescheid, durch den das Verhalten des Mitglieds gerügt wird, ist zu begründen.
Er ist dem Mitglied mit Rechtsbehelfsbelehrung zuzustellen.
Eine Zweitschrift des Bescheids ist der Aufsichtsbehörde zu übersenden.

(5) Gegen den Bescheid kann das Mitglied innerhalb eines Monats nach der Zustellung bei dem Vorstand Einspruch erheben.
Über den Einspruch entscheidet der Vorstand.
Absatz 4 ist entsprechend anzuwenden.
Wird der Einspruch zurückgewiesen, so kann das Mitglied binnen eines Monats nach der Zustellung beim Ehrenausschuss beantragen, dass ein Ehrenverfahren eingeleitet wird.

(6) Ein Ehrenverfahren kann auch dann eingeleitet werden, wenn wegen desselben Verhaltens bereits eine Rüge erteilt wurde.
Jedoch kann der Vorstand der Architektenkammer die Einleitung des Ehrenverfahrens nur noch beantragen, wenn nach Erteilung der Rüge neue Tatsachen oder Beweismittel bekannt geworden sind, die die Berufspflichtverletzung als durch eine Rüge nicht genügend geahndet erscheinen lassen.
Der Antrag kann nur innerhalb eines Jahres nach Erteilung der Rüge gestellt werden.
Die Rüge wird mit Rechtskraft der Entscheidung des Ehrenausschusses gegenstandslos.
Hält der Ehrenausschuss die Durchführung eines Ehrenverfahrens nur wegen Geringfügigkeit der erhobenen Beschuldigung nicht für erforderlich oder stellt er wegen der Geringfügigkeit der Berufspflichtverletzung das Verfahren ein, so hat er in seinem Beschluss die Rüge aufrechtzuerhalten, wenn die Nachprüfung ergibt, dass sie zu Recht erteilt wurde.

#### § 27 Ehrenausschuss

(1) Die Architektenkammer bildet einen Ehrenausschuss.
Der Ehrenausschuss besteht aus der oder dem Vorsitzenden und einer ausreichenden Anzahl von Beisitzerinnen und Beisitzern.
Für die Vorsitzende oder den Vorsitzenden können Vertreterinnen und Vertreter bestellt werden.
Die Mitglieder des Ehrenausschusses dürfen nicht Dienstkräfte der Architektenkammer oder Mitglieder der Aufsichtsbehörde sein, die gemäß § 31 mit der Aufsicht über die Architektenkammer befasst sind.

(2) Die Mitglieder des Ehrenausschusses werden von der Vertreterversammlung auf die Dauer von fünf Jahren gewählt.

(3) Der Ehrenausschuss entscheidet in der Besetzung mit der oder dem Vorsitzenden und zwei Beisitzerinnen oder Beisitzern.
Die oder der Vorsitzende bestimmt vor Beginn eines jeden Geschäftsjahres für dessen Dauer die Reihenfolge, in der seine Vertreterinnen und Vertreter und die Beisitzerinnen und Beisitzer unter Berücksichtigung ihrer Fachrichtung zu den Sitzungen zugezogen werden.

(4) Bei Entscheidungen im Ehrenverfahren muss mindestens eine Beisitzerin oder ein Beisitzer der Fachrichtung der betroffenen Person angehören.

(5) Die oder der Vorsitzende und die Vertreterinnen und Vertreter sollen die Befähigung zum Richteramt haben oder Diplomjuristin oder Diplomjurist sein.

(6) In gerichtlichen Verfahren, die Entscheidungen des Ehrenausschusses betreffen, wird die Architektenkammer durch die Vorsitzende oder den Vorsitzenden des Ehrenausschusses vertreten.

#### § 28 Ehrenverfahren

(1) Die schuldhafte Verletzung von Berufspflichten wird in einem förmlichen Ehrenverfahren vor dem Ehrenausschuss geahndet.
Politische, wissenschaftliche und künstlerische oder religiöse Ansichten und Handlungen können nicht Gegenstand eines Ehrenverfahrens sein.
Dem Ehrenverfahren unterliegen nicht Personen, die dem öffentlichen Dienst angehören hinsichtlich ihrer dienstlichen Tätigkeit, und Personen, soweit sie als Beliehene öffentliche Aufgaben wahrnehmen.

(2) Einen Antrag auf Einleitung eines Ehrenverfahrens kann stellen:

1.  eine betroffene Person gegen sich selbst oder
2.  der Vorstand der Architektenkammer.

(3) Ist wegen desselben Sachverhalts die öffentliche Klage im strafgerichtlichen Verfahren erhoben worden, kann ein Ehrenverfahren zwar eingeleitet werden, es muss aber bis zur Beendigung des Strafverfahrens ausgesetzt werden.
Das Gleiche gilt, wenn während des Ehrenverfahrens die öffentliche Klage erhoben wird.
Die tatsächlichen Feststellungen des Urteils im strafgerichtlichen Verfahren sind für das Ehrenverfahren bindend.

(4) Ist ein Mitglied in einem strafgerichtlichen Verfahren freigesprochen worden oder wurde das strafgerichtliche Verfahren eingestellt, kann wegen des Sachverhalts, der Gegenstand der gerichtlichen Entscheidung war, ein Ehrenverfahren nur eingeleitet oder fortgesetzt werden, wenn dieser Sachverhalt, ohne den Tatbestand eines Strafgesetzes zu erfüllen, eine Verletzung von Berufspflichten darstellt.

(5) Die Absätze 3 und 4 gelten entsprechend, wenn gegen das Mitglied ein Disziplinarverfahren wegen desselben Sachverhaltes eingeleitet wurde.

#### § 29 Maßnahmen im Ehrenverfahren

(1) Im Ehrenverfahren kann erkannt werden auf

1.  einen Verweis,
2.  eine Geldbuße bis zu 30 000 Euro,
3.  den Verlust der Fähigkeit, Ämter in der Architektenkammer zu bekleiden,
4.  die Aberkennung des Wahlrechts und der Wählbarkeit zu den Organen der Architektenkammer, ihrer Ausschüsse und Einrichtungen für eine Dauer von bis zu fünf Jahren oder
5.  die Löschung der Eintragung in den Listen nach § 1 Absatz 1 oder aus dem Verzeichnis nach § 2 Absatz 3.

In den Fällen des Satzes 1 Nummer 5 bestimmt der Ehrenausschuss einen Zeitraum von mindestens drei und höchstens sieben Jahren, innerhalb dessen eine erneute Eintragung zu versagen ist.
Auf eine Maßnahme nach Satz 1 Nummer 1, 3 oder 4 kann neben einer Maßnahme nach Satz 1 Nummer 2 erkannt werden.
Eine Maßnahme nach Satz 1 Nummer 4 schließt die Folgen einer Maßnahme nach Satz 1 Nummer 3 in sich ein.

(2) Gegenüber einer Gesellschaft nach den §§ 7 und 8 kann der Ehrenausschuss erkennen auf

1.  einen Verweis,
2.  eine Geldbuße bis zu 60 000 Euro,
3.  die Löschung der Eintragung aus dem Verzeichnis nach § 7 Absatz 1 oder § 8 Absatz 2.

(3) Sind seit einer Berufspflichtverletzung mehr als fünf Jahre verstrichen, so sind Maßnahmen im Ehrenverfahren nicht mehr zulässig.
Verstößt die Tat auch gegen ein Strafgesetz, so endet die Frist nicht vor der Verjährung der Strafverfolgung.
Ist vor Ablauf der Frist ein Ehrenverfahren oder wegen desselben Sachverhalts ein Strafverfahren eingeleitet worden, so ist die Frist für die Dauer des Verfahrens gehemmt.
Für den Beginn, das Ruhen und die Unterbrechung der Verjährung gelten die §§ 78a bis 78c des Strafgesetzbuches entsprechend.

(4) Geldbußen fließen der Architektenkammer zu.

(5) Gegen eine Maßnahme im Ehrenverfahren kann Klage beim Verwaltungsgericht erhoben werden.
Ein Vorverfahren nach den §§ 68 bis 73 der Verwaltungsgerichtsordnung findet nicht statt.

### Abschnitt 5  
Aufsicht über die Architektenkammer

#### § 30 Aufsichtsbehörde

Die Rechtsaufsicht über die Architektenkammer führt das für das Architektenrecht zuständige Ministerium (Aufsichtsbehörde).
Für die Durchführung rechtsaufsichtlicher Maßnahmen gelten die §§ 111 bis 119 der Kommunalverfassung des Landes Brandenburg entsprechend.

#### § 31 Durchführung der Aufsicht

Die Aufsichtsbehörde ist zu den Sitzungen der Vertreterversammlung der Architektenkammer einzuladen.
Der Vertreterin oder dem Vertreter der Aufsichtsbehörde ist in der Vertreterversammlung auf Verlangen das Wort zu erteilen.
Die Aufsichtsbehörde kann verlangen, dass eine Vertreterversammlung unverzüglich einberufen wird.

### Abschnitt 6  
Ordnungswidrigkeiten

#### § 32 Ordnungswidrigkeiten

(1) Ordnungswidrig handeln Personen, die

1.  unbefugt eine der in § 1 Absatz 1 oder Absatz 5 sowie § 7 Absatz 1 genannten Berufsbezeichnungen führen oder führen lassen oder eine Wortverbindung oder ähnliche Bezeichnung im Sinne des § 1 Absatz 6 verwenden,
2.  als nicht zur Führung einer der in § 1 genannten Berufsbezeichnungen Berechtigte zulassen, dass Dritte in Publikationen oder gewerblichen Verzeichnissen mit Bezug auf sie eine in § 1 genannte Berufsbezeichnung verwenden und trotz Aufforderung der Architektenkammer nicht innerhalb von zwei Monaten tätig werden, um dies zu unterbinden, oder
3.  keine Berufshaftpflichtversicherung im Sinne des § 10 unterhalten, obwohl sie dazu verpflichtet sind.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu 30 000 Euro geahndet werden.

(3) Zuständige Verwaltungsbehörde im Sinne des § 36 Absatz 1 Satz 1 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19.
Februar 1987 (BGBl. I S. 602), das zuletzt durch Artikel 4 des Gesetzes vom 13. Mai 2015 (BGBl.
I S. 706) geändert worden ist, ist die Architektenkammer.
Dies gilt auch für Ordnungswidrigkeiten nach § 6 der Dienstleistungs-Informationspflichten-Verordnung vom 12.
März 2010 (BGBl.
I S. 267), die durch Architekten begangen werden.

(4) Die festgesetzten Geldbußen und Verwarnungsgelder fließen der Architektenkammer zu.
Sie hat die notwendigen Auslagen zu tragen, die einer oder einem Betroffenen nach § 105 Absatz 2 des Gesetzes über Ordnungswidrigkeiten zu erstatten sind.
Die Vollstreckung der Bußgeldentscheidung bestimmt sich unbeschadet der besonderen Vorschriften des Gesetzes über Ordnungswidrigkeiten nach § 20 Absatz 3.

### Abschnitt 7  
Übergangs- und Schlussvorschriften

#### § 33 Rechtsverordnungen

Das für das Architektenrecht zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung die zur Durchführung dieses Gesetzes notwendigen Vorschriften zu erlassen über

1.  die Verfahren vor dem Eintragungsausschuss sowie die für die Eintragung in die in diesem Gesetz genannten Listen und Verzeichnisse vorzulegenden oder anzuerkennenden Nachweise,
2.  den Inhalt und das Verfahren der Ausstellung Europäischer Berufsausweise einschließlich der Erstellung von und des Umgangs mit Dateien des Binnenmarkt-Informationssystems (IMI-Dateien) im Sinne des Artikels 4a Absatz 5 der Richtlinie 2005/36/EG,
3.  die inhaltliche Gestaltung und Umsetzung des gemeinsamen Ausbildungsrahmens nach Artikel 49a der Richtlinie 2005/36/EG,
4.  die Gestaltung und Durchführung von gemeinsamen Ausbildungsprüfungen nach Artikel 49b der Richtlinie 2005/36/EG und
5.  von der Architektenkammer zur zweckentsprechenden Durchführung dieses Gesetzes oder nach dem Recht der Europäischen Union wahrzunehmende weitere Aufgaben.

#### § 34 Einschränkung eines Grundrechts

Durch dieses Gesetz wird das Grundrecht auf Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 35 Übergangsvorschriften

(1) Die nach den bisher geltenden Regelungen gebildeten Organe der Architektenkammer bleiben bis zu einer Neuwahl oder Neubestellung entsprechend diesem Gesetz im Amt.

(2) Bei Inkrafttreten dieses Gesetzes laufende Verfahren vor dem Eintragungs- oder Ehrenausschuss sind nach den bisher gültigen Rechtsvorschriften abzuschließen.

(3) Personen, die bei Inkrafttreten dieses Gesetzes in die Architektenliste eingetragen sind, dürfen ihre bisherige Berufsbezeichnung weiterführen.
Die Regelungen über die Löschung aus der Architektenliste bleiben unberührt.

(4) Die in § 4 Absatz 1 Satz 1 Nummer 2 genannten Anforderungen an ein Studium gelten nur für Personen, die ihr Studium nach dem 1.
Juli 2016 begonnen haben.
Die in § 4 Absatz 1 Satz 3 bis 5 genannten Anforderungen an das Berufspraktikum gelten nur für Personen, die ihr Studium in der Fachrichtung Architektur erfolgreich abgeschlossen und ihre zweijährige praktische Tätigkeit nach dem 1.
Juli 2016 begonnen haben.
Für Personen, die nicht unter Satz 1 oder Satz 2 fallen, gelten die Regelungen zu Studiendauer und praktischer Tätigkeit gemäß § 5 Absatz 1 Satz 1 des Brandenburgischen Architektengesetzes vom 8.
März 2006 (GVBl.
I S.
26), das zuletzt durch das Gesetz vom 10.
Juli 2014 (GVBl.
I Nr.
39) geändert worden ist.

#### § 36 Inkrafttreten, Außerkrafttreten

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt das Brandenburgische Architektengesetz vom 8.
März 2006 (GVBl.
I S. 26), das zuletzt durch das Gesetz vom 10. Juli 2014 (GVBl. I Nr. 39) geändert worden ist, außer Kraft.

(2) Dieses Gesetz tritt mit Ablauf des 31.
Juli 2025 außer Kraft.

Potsdam, den 11.
Januar 2016

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[1)](#1) Dieses Gesetz dient der Umsetzung der Richtlinie 2013/55/EU des Europäischen Parlaments und des Rates vom 20.
November 2013 zur Änderung der Richtlinie 2005/36/EG über die Anerkennung von Berufsqualifikationen und der Verordnung (EU) Nr.
1024/2012 über die Verwaltungszusammenarbeit mit Hilfe des Binnenmarkt-Informationssystems („IMI-Verordnung“) (ABl.
L 354 vom 28.12.2013, S. 132).

* * *

### Anlagen

1

[Anlage 1 - Leitlinien zu Ausbildungsinhalten](/br2/sixcms/media.php/68/BbgArchG-Anlage-1.pdf "Anlage 1 - Leitlinien zu Ausbildungsinhalten") 282.2 KB

2

[Anlage 2 - Prüfraster für die Verhältnismäßigkeitsprüfung](/br2/sixcms/media.php/68/BbgArchG-Anlage-2.pdf "Anlage 2 - Prüfraster für die Verhältnismäßigkeitsprüfung") 287.7 KB