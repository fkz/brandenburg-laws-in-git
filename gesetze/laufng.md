## Gesetz über die Aufnahme von Flüchtlingen, spätausgesiedelten und weiteren aus dem Ausland zugewanderten Personen im Land Brandenburg sowie zur Durchführung des Asylbewerberleistungsgesetzes (Landesaufnahmegesetz - LAufnG)

Der Landtag hat das folgende Gesetz beschlossen:

### Teil 1

### Allgemeiner Teil

#### § 1 Gegenstand und Anwendungsbereich

Dieses Gesetz regelt die Aufnahme, vorläufige Unterbringung und migrationsspezifische soziale Unterstützung von Flüchtlingen, spätausgesiedelten und weiteren aus dem Ausland zugewanderten Personen in den Landkreisen und kreisfreien Städten, soweit das Land zur Aufnahme dieser Personen gesetzlich verpflichtet ist oder sich hierzu verpflichtet hat, sowie die Durchführung des Asylbewerberleistungsgesetzes.

#### § 2 Aufgaben

(1) Die Aufnahme, vorläufige Unterbringung und die migrationsspezifische soziale Unterstützung der in § 4 genannten Personen sowie die Durchführung des Asylbewerberleistungsgesetzes sind öffentliche Aufgaben, die den Landkreisen und kreisfreien Städten als Pflichtaufgaben zur Erfüllung nach Weisung übertragen werden.
Werden nach Maßgabe dieses Gesetzes Dritte an der Durchführung der Aufgaben beteiligt oder werden ihnen Aufgaben zur Ausführung ganz oder teilweise übertragen, bleiben die Landkreise und kreisfreien Städte für die Erfüllung der Aufgaben verantwortlich.

(2) Den Ämtern und amtsfreien Gemeinden wird als Pflichtaufgabe zur Erfüllung nach Weisung die Bereitstellung der für die vorläufige Unterbringung der Personen nach § 4 notwendigen und geeigneten Liegenschaften übertragen.

(3) Bei der Ausführung dieses Gesetzes sind die besonderen Belange schutzbedürftiger Personen im Sinne des Artikels 21 der Richtlinie 2013/33/EU des Europäischen Parlaments und des Rates vom 26. Juni 2013 zur Festlegung von Normen für die Aufnahme von Personen, die internationalen Schutz beantragen (ABl. L 180 vom 29.6.2013, S. 96), zu berücksichtigen.

#### § 3 Durchführung des Asylbewerberleistungsgesetzes

(1) Zuständige Behörden und Kostenträger für die Durchführung des Asylbewerberleistungsgesetzes sind die Landkreise und kreisfreien Städte.

(2) Abweichend von Absatz 1 ist die Zentrale Ausländerbehörde zuständige Behörde und Kostenträger für Leistungen nach dem Asylbewerberleistungsgesetz, die in einer Aufnahmeeinrichtung im Sinne des § 44 des Asylgesetzes (Erstaufnahmeeinrichtung) oder in einer Abschiebungshafteinrichtung des Landes erbracht werden.

#### § 4 Aufzunehmender Personenkreis

Die Aufnahmeverpflichtung erstreckt sich auf

1.  spätausgesiedelte Personen und die mit ihnen durch Ehe oder eingetragene Lebenspartnerschaft Verbundenen und Abkömmlinge, soweit sie die Voraussetzungen des § 7 Absatz 2 des Bundesvertriebenengesetzes erfüllen, sowie Familienangehörige von spätausgesiedelten Personen, die, ohne die Voraussetzungen des § 7 Absatz 2 des Bundesvertriebenengesetzes zu erfüllen, gemeinsam mit spätausgesiedelten Personen eintreffen und nach § 8 Absatz 2 des Bundesvertriebenengesetzes im Einzelfall in das Verteilungsverfahren einbezogen werden;
2.  Ausländerinnen und Ausländer, denen nach § 23 Absatz 2 oder Absatz 4 des Aufenthaltsgesetzes eine Aufenthaltserlaubnis oder Niederlassungserlaubnis erteilt wird;
3.  Ausländerinnen und Ausländer, denen aus völkerrechtlichen, humanitären oder politischen Gründen eine Aufenthaltserlaubnis erteilt wird
    1.  zur Aufnahme aus dem Ausland nach § 22 des Aufenthaltsgesetzes,
    2.  durch die oberste Landesbehörde nach § 23 Absatz 1 des Aufenthaltsgesetzes,
    3.  zum vorübergehenden Schutz nach § 24 des Aufenthaltsgesetzes;
4.  Ausländerinnen und Ausländer im Sinne von § 1 Absatz 1 des Asylgesetzes;
5.  Personen, deren Asylantrag bestands- oder rechtskräftig abgelehnt oder zurückgenommen worden ist;
6.  unerlaubt eingereiste Ausländerinnen und Ausländer, die nach § 15a Absatz 1 des Aufenthaltsgesetzes verteilt worden sind;
7.  Personen, die einen Folgeantrag nach § 71 des Asylgesetzes oder einen Zweitantrag nach § 71a des Asylgesetzes gestellt haben;
8.  Ausländerinnen und Ausländer,
    1.  denen aus humanitären Gründen nach § 25 Absatz 3 in Verbindung mit § 60 Absatz 7 des Aufenthaltsgesetzes eine Aufenthaltserlaubnis erteilt wird,
    2.  denen nach § 25 Absatz 4 oder Absatz 5 des Aufenthaltsgesetzes eine Aufenthaltserlaubnis erteilt wird oder
    3.  bei denen die Abschiebung nach § 60a des Aufenthaltsgesetzes ausgesetzt wird.

### Teil 2

### Erstaufnahme und Verteilungsverfahren

#### § 5 Erstaufnahme

Das Landesamt für Soziales und Versorgung ist für die Durchführung des Erstaufnahmeverfahrens für die in § 4 Nummer 1, 2 und 3 Buchstabe b genannten Personen zuständig.
Soweit ein Erstaufnahmeverfahren durchzuführen ist, ist die Zentrale Ausländerbehörde für alle weiteren in § 4 genannten Personen zuständig.

#### § 6 Verteilungsverfahren

(1) Die in § 4 Nummer 1, 2, 3, 4 und 6 genannten Personen werden durch die jeweils zuständige Behörde verteilt und zugewiesen (Verteilungsverfahren).
Personen, die aufgrund dieses oder eines anderen Gesetzes bereits auf einen Landkreis oder eine kreisfreie Stadt verteilt wurden, werden nicht erneut zugewiesen.
Die in § 4 Nummer 5, 7 und 8 genannten Personen können in das Verteilungsverfahren einbezogen werden.

(2) Bei der Zuweisung sind die Haushaltsgemeinschaften von durch Ehe oder eingetragene Lebenspartnerschaft Verbundenen sowie von personensorgeberechtigten Erwachsenen minderjähriger lediger Kinder mit diesen Kindern oder sonstige humanitäre Gründe von vergleichbarem Gewicht zu berücksichtigen.
Andere wichtige Gründe, insbesondere persönliche Belange der Zuzuweisenden und die wirtschaftliche Nutzung der Einrichtungen der vorläufigen Unterbringung, können berücksichtigt werden.

(3) Gegen die Verteilung und gegen die Zuweisung findet kein Widerspruch statt.
Die Klage hat keine aufschiebende Wirkung.

(4) Die Verteilung der Personen auf die Landkreise und kreisfreien Städte erfolgt auf der Grundlage von vorrangig die Einwohnerzahl berücksichtigenden Aufnahmequoten (Verteilerschlüssel).
Bei Landkreisen und kreisfreien Städten, auf deren Gebiet eine Erstaufnahmeeinrichtung des Landes oder deren Außenstelle oder eine Einrichtung der Jugendhilfe zur Durchführung des Clearingverfahrens für unbegleitete minderjährige Flüchtlinge betrieben wird, soll die Aufnahmequote oder das Aufnahmesoll anteilig verringert werden.

(5) Das für Soziales zuständige Ministerium oder eine von ihm bestimmte Behörde teilt den Landkreisen und kreisfreien Städten die Zahl der monatlichen Zugänge von Personen nach § 4, die voraussichtliche Entwicklung der Anzahl der nach dem Asylgesetz aufzunehmenden Personen und den voraussichtlichen jährlichen sowie monatlichen Bedarf an Unterbringungsplätzen entsprechend der jeweiligen Aufnahmequote (Aufnahmesoll) mit.
Die Landkreise und kreisfreien Städte sind zur kontinuierlichen Erfüllung ihres Aufnahmesolls verpflichtet.
Hierzu haben sie insbesondere die notwendige Zahl von Unterbringungsplätzen zur vorläufigen Unterbringung rechtzeitig bereitzustellen.
Personen, die aufgrund dieses oder eines anderen Gesetzes bereits verteilt wurden, werden auf die Erfüllung des Aufnahmesolls angerechnet.

(6) Im Falle eines trotz Erfüllung der Pflichten aus § 44 Absatz 1 des Asylgesetzes gegenwärtigen, auf andere Weise nicht oder nicht rechtzeitig abwendbaren, durch das für Inneres zuständige Ministerium im Einvernehmen mit dem für Soziales zuständigen Ministerium festgestellten Unterbringungsnotstands in den Erstaufnahmeeinrichtungen des Landes nach dem Asylgesetz kann das für Soziales zuständige Ministerium im Einvernehmen mit dem für Inneres zuständigen Ministerium anordnen, dass über das gesamte jeweilige monatliche Aufnahmesoll aller Landkreise und kreisfreien Städte hinaus weitere Personen von den Landkreisen und kreisfreien Städten kurzfristig aufgenommen und vorübergehend untergebracht werden.
Das für Soziales zuständige Ministerium benennt der Zentralen Ausländerbehörde die zur Aufnahme verpflichteten Landkreise oder kreisfreien Städte.
Vorrangig aufnahmepflichtig sind Landkreise und kreisfreie Städte, die ihr anteiliges Aufnahmesoll bis zur Entscheidung über die Verteilung noch nicht erfüllt haben.

(7) Das für Soziales zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit den für Inneres und für Finanzen zuständigen Mitgliedern der Landesregierung durch Rechtsverordnung das Nähere zum Verteilungsverfahren nach den Absätzen 1 bis 6 zu bestimmen.

#### § 7 Landesinterne Umverteilung

(1) Aus Gründen des öffentlichen Interesses, insbesondere der öffentlichen Sicherheit und Ordnung oder humanitären Gründen von vergleichbarem Gewicht oder auf Antrag der aufgenommenen Person kann landesintern eine Umverteilung in einen anderen Landkreis oder eine andere kreisfreie Stadt erfolgen (landesinterne Umverteilung).
Über die Umverteilung entscheidet die für die aufgenommene Person zuständige Ausländerbehörde im Einvernehmen mit der Ausländerbehörde, zu der die Umverteilung erfolgen soll.
Das Einvernehmen ist insbesondere zu erteilen:

1.  zur Herstellung der familiären Lebensgemeinschaft zwischen Ehe- oder Lebenspartnern oder zwischen Eltern und ihren minderjährigen Kindern,
2.  zur benötigten Pflege von Eltern und nahen Angehörigen,
3.  zur Berufsausbildung oder zur Ausübung einer Erwerbstätigkeit ohne Inanspruchnahme von Leistungen nach dem Zweiten oder Zwölften Buch Sozialgesetzbuch oder dem Asylbewerberleistungsgesetz,
4.  zur Beseitigung einer Gefahrenlage, die insbesondere von Familienangehörigen oder anderen Personen aus dem persönlichen Umfeld ausgeht und die einen Umzug in den Zuständigkeitsbereich einer anderen Ausländerbehörde erfordert.

(2) Landesinterne Umverteilungen werden auf die Erfüllung des Aufnahmesolls angerechnet.

(3) Das für Soziales zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit den für Inneres und für Finanzen zuständigen Mitgliedern der Landesregierung durch Rechtsverordnung das Nähere zu den Voraussetzungen und dem Verfahren der landesinternen Umverteilung nach den Absätzen 1 und 2 zu bestimmen.

#### § 8 Landkreisinterne Verteilung

Die Landkreise können durch Satzung eine eigene Quote (landkreisinterner Verteilerschlüssel) zur gleichmäßigen und die örtlichen Verhältnisse berücksichtigenden vorläufigen oder dauerhaften Unterbringung der Personen nach § 4 in den amtsfreien Gemeinden und Ämtern festlegen.

### Teil 3

### Vorläufige Unterbringung und soziale Unterstützung

#### § 9 Aufnahme und vorläufige Unterbringung

(1) Die Landkreise und kreisfreien Städte nehmen die ihnen im Rahmen des Verteilungsverfahrens zugeteilten Personen auf und bringen sie in Einrichtungen der vorläufigen Unterbringung (Gemeinschaftsunterkünfte, Wohnungsverbünde oder Übergangswohnungen) unter.
§ 53 des Asylgesetzes bleibt unberührt.

(2) Ist trotz rechtzeitiger Bereitstellung der notwendigen Zahl von Unterbringungsplätzen gemäß § 6 Absatz 5 Satz 3, insbesondere aufgrund eines Unterbringungsnotstandes nach § 6 Absatz 6, im Zeitpunkt der Aufnahme kein Unterbringungsplatz in Einrichtungen der vorläufigen Unterbringung verfügbar, kann die Unterbringung vorübergehend auch außerhalb von Einrichtungen der vorläufigen Unterbringung erfolgen.

(3) Abweichend von Absatz 1 Satz 1 werden Personen nach § 4 Nummer 3 und 8 nur vorläufig untergebracht, soweit dies erforderlich ist.

(4) Bei der vorläufigen Unterbringung von Personen nach § 4 Nummer 4 und 7 sind die besonderen Anforderungen im Sinne des Artikels 18 der Richtlinie 2013/33/EU zu berücksichtigen.
Sofern den besonderen Belangen schutzbedürftiger Personen im Sinne von § 2 Absatz 3 nicht in einer Gemeinschaftsunterkunft entsprochen werden kann, hat ihre Unterbringung in geeigneten Wohnungen oder, sofern erforderlich, geeigneten Einrichtungen zu erfolgen.

(5) Ist für Personen nach § 4 Nummer 1 und 2 eine Versorgung mit Wohnraum im Zeitpunkt der Wohnsitznahme nicht möglich, sind diese vorübergehend in Einrichtungen der vorläufigen Unterbringung unterzubringen.
Im Falle der Unterbringung in einer Gemeinschaftsunterkunft soll diese einen Zeitraum von sechs Monaten nicht überschreiten.

(6) Das für Soziales zuständige Mitglied der Landesregierung wird ermächtigt, das Nähere zur Aufnahme und vorläufigen Unterbringung nach den Absätzen 1 bis 5 im Einvernehmen mit den für Inneres und für Finanzen zuständigen Mitgliedern der Landesregierung durch Rechtsverordnung zu regeln.

#### § 10 Einrichtungen der vorläufigen Unterbringung

(1) Die Landkreise und kreisfreien Städte sind verpflichtet, die erforderlichen Einrichtungen der vorläufigen Unterbringung zu errichten und zu unterhalten.
Die für die vorläufige Unterbringung genutzten Liegenschaften sollen in ihrer Beschaffenheit den Bewohnerinnen und Bewohnern die Teilhabe am gesellschaftlichen Leben ermöglichen.

(2) Die Landkreise und kreisfreien Städte können geeignete Dritte beauftragen, Einrichtungen der vorläufigen Unterbringung zu betreiben.

(3) Zur Gewährleistung einheitlicher Mindestbedingungen der vorläufigen Unterbringung bestimmt das für Soziales zuständige Mitglied der Landesregierung im Einvernehmen mit den für Inneres und für Finanzen zuständigen Mitgliedern der Landesregierung durch Rechtsverordnung die näheren Anforderungen an die Einrichtungen der vorläufigen Unterbringung.

(4) Das Landesamt für Soziales und Versorgung ist zuständig für die regelmäßige Überprüfung der Einhaltung der Mindestbedingungen der vorläufigen Unterbringung.
In besonderen Zugangssituationen, insbesondere zur Abwendung eines Unterbringungsnotstandes nach § 6 Absatz 6, kann das Landesamt für Soziales und Versorgung befristet Ausnahmen von der Einhaltung der Mindestbedingungen der vorläufigen Unterbringung zulassen.
Das für Soziales zuständige Mitglied der Landesregierung wird ermächtigt, das Nähere zu den Ausnahmen und dem Verfahren nach Satz 2 im Einvernehmen mit den für Inneres und für Finanzen zuständigen Mitgliedern der Landesregierung durch Rechtsverordnung zu regeln.

#### § 11 Nutzungsverhältnisse und Entgelt

(1) Das Nutzungsverhältnis in Einrichtungen der vorläufigen Unterbringung ist öffentlich-rechtlich.
Die Landkreise und kreisfreien Städte werden ermächtigt, das Nähere durch Satzung im Rahmen einer Benutzungsordnung zu bestimmen.
Insbesondere kann eine den örtlichen Gegebenheiten Rechnung tragende Hausordnung erlassen werden.

(2) Für die Inanspruchnahme der Einrichtungen der vorläufigen Unterbringung werden Nutzungsentgelte von Personen erhoben, deren anrechenbares Einkommen im Sinne des § 82 des Zwölften Buches Sozialgesetzbuch den nach § 29 des Zwölften Buches Sozialgesetzbuch jeweils geltenden Regelsatz übersteigt.
Ist die Differenz zwischen anrechenbarem Einkommen und Regelsatz niedriger als das zu erhebende Nutzungsentgelt, ist dieses entsprechend zu verringern.
Die Landkreise und kreisfreien Städte werden ermächtigt, die Höhe der Nutzungsentgelte durch Satzung festzusetzen.
Dabei ist eine nach Aufenthaltsdauer gestaffelte Erhöhung der Nutzungsentgelte vorzusehen.
Die Staffelung gilt nicht für den Personenkreis nach § 4 Nummer 4.
Die Satzung bedarf der Genehmigung durch das für Soziales zuständige Ministerium.

#### § 12 Soziale Unterstützung durch Migrationssozialarbeit

(1) Die Landkreise und kreisfreien Städte sind verpflichtet, die nach diesem Gesetz aufgenommenen Personen bei der Bewältigung der insbesondere aus ihrer Aufnahme- und Aufenthaltssituation begründeten besonderen Lebenslagen, angepasst an die jeweilige Wohn- und Unterbringungssituation, durch soziale Beratung und Betreuung (Migrationssozialarbeit) zu unterstützen.
Zur Aufgabenwahrnehmung ist ein bedarfsgerechtes und zielgruppenspezifisches fachliches Angebot kontinuierlich zu gewährleisten.

(1a) Zur Unterstützung eines kontinuierlichen Angebots an zielgruppenspezifischer Migrationssozialarbeit wird den Landkreisen und kreisfreien Städten für den Zeitraum bis zum 31. Dezember 2024 eine freiwillige Erstattungsleistung des Landes für das Angebot der Migrationssozialarbeit für Personen, die Regelleistungsberechtigte nach dem Zweiten Buch Sozialgesetzbuch aus den nichteuropäischen Asylherkunftsländern sind, gewährt.
Eine zielgruppenspezifische Migrationssozialarbeit für den Personenkreis nach Satz 1 kann für bis zu drei Jahre nach dem Wechsel in den Regelleistungsbezug vorgehalten werden.

(2) Die Aufgabenwahrnehmung nach den Absätzen 1 und 1a kann auf geeignete Dritte, in der Regel nichtstaatliche Träger der Sozialen Arbeit, übertragen werden.
Den mit der Aufgabenwahrnehmung betrauten Personen ist im Rahmen ihrer Betreuungs- und Beratungsarbeit Zugang zu den Einrichtungen der vorläufigen Unterbringung zu gewähren.
Das Hausrecht der Betreiber bleibt unberührt.

(3) Das für Soziales zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit den für Inneres und für Finanzen zuständigen Mitgliedern der Landesregierung nähere Einzelheiten der Unterstützung durch Migrationssozialarbeit, insbesondere Aufgaben, strukturelle und konzeptionelle Anforderungen, Vorgaben bezüglich der Trägerauswahl und fachlicher und personeller Standards und die erforderliche Datenübermittlung nach § 19 Absatz 2 durch Rechtsverordnung zu regeln.
Ferner soll zur bedarfsgerechten Weiterentwicklung der Aufgabenwahrnehmung durch Rechtsverordnung nach Satz 1 die Evaluierung der Auswirkungen dieser Vorschrift bestimmt werden.

### Teil 4

### Kosten

#### § 13 Kostentragung und Kostenerstattung

(1) Die Landkreise und kreisfreien Städte tragen die Kosten der Wahrnehmung der ihnen nach diesem Gesetz obliegenden Aufgaben.

(2) Das Land erstattet den Landkreisen und kreisfreien Städten auf Antrag die notwendigen Kosten der Aufgabenwahrnehmung nach diesem Gesetz nach Maßgabe der folgenden Bestimmungen.
Leistungen, für die den Landkreisen und kreisfreien Städten dem Grunde nach bereits nach anderen Vorschriften oder im Rahmen der Wahrnehmung der kommunalen Selbstverwaltungsaufgaben ein Ausgleich gezahlt wird oder auf die nach anderen Vorschriften ein Ausgleichsanspruch besteht, werden nicht erstattet.
Erstattungsbehörde ist das Landesamt für Soziales und Versorgung.

(3) Für die den Ämtern und amtsfreien Gemeinden bei der Erfüllung ihrer Aufgaben nach § 2 Absatz 2 entstehenden Kosten sind die Landkreise zum Ausgleich verpflichtet.

#### § 14 Erstattungspauschalen

(1) Die Landkreise und kreisfreien Städte erhalten für die Aufnahme des Personenkreises nach § 4 Nummer 1 und 2 pro aufgenommene Person eine einmalige Jahrespauschale.

(2) Die Landkreise und kreisfreien Städte erhalten für Personen, denen sie Leistungen nach dem Asylbewerberleistungsgesetz gewähren, mit Ausnahme der gemäß § 15 nach Kostennachweis zu erstattenden Leistungen, pro Person eine jährliche Pauschale.
Soweit Leistungsbeziehende keine Unterkunftsleistungen in Anspruch nehmen, wird die Pauschale anteilig gekürzt.
Einnahmen aus Nutzungsentgelten nach § 11 Absatz 2, Erstattungsleistungen von Leistungsberechtigten sowie aufgrund deren einzusetzendem Einkommen oder Vermögen nach § 7 Absatz 1 des Asylbewerberleistungsgesetzes verringerter Leistungen sowie sonstige Erstattungsansprüche für nach diesem Gesetz erbrachte Leistungen gegenüber Leistungsbeziehenden oder Dritten werden mit der Erstattungsleistung verrechnet.

(3) Zum Ausgleich der Aufgabenwahrnehmung nach § 12 Absatz 1 erhalten die Landkreise und kreisfreien Städte pro Erstattungsfall nach Absatz 1 und 2 eine jährliche Pauschale, die an den der wahrzunehmenden Tätigkeiten entsprechenden Personaldurchschnittskosten zuzüglich angemessener Sachkosten zu bemessen ist.
Darüber hinaus werden zur Gewährleistung der Kontinuität der Aufgabenwahrnehmung und zur Sicherstellung einer zielgruppenspezifischen Migrationssozialarbeit gesonderte pauschale Erstattungsleistungen durch Rechtsverordnung nach § 16 bestimmt.

(3a) Zum Ausgleich der Aufgabenwahrnehmung nach § 12 Absatz 1a erhalten die Landkreise und  
kreisfreien Städte eine Pauschale für die jeweiligen Neuzugänge an Regelleistungsberechtigten nach  
dem Zweiten Buch Sozialgesetzbuch aus den nichteuropäischen Asylherkunftsländern der drei dem jeweiligen Erstattungsjahr vorangegangenen Jahre, die durch die Erstattungsbehörde auf Grundlage der statistischen Daten der Bundesagentur für Arbeit ermittelt werden.

(4) Den Landkreisen und kreisfreien Städten werden die für die Aufgabenwahrnehmung nach diesem Gesetz notwendigen personellen und sächlichen Verwaltungskosten gesondert pauschal erstattet, soweit diese nicht nach § 15 Absatz 1 erstattet werden.

(5) Sofern Sicherheitsmaßnahmen für eine als Gemeinschaftsunterkunft oder Wohnungsverbund genutzte Liegenschaft erforderlich sind, erhalten die Landkreise und kreisfreien Städte auf Antrag eine monatliche Pauschale (Sicherheitspauschale).

(6) Für die erstmalige Bereitstellung von Unterbringungsplätzen in Einrichtungen der vorläufigen Unterbringung erhalten die Landkreise und kreisfreien Städte eine Investitionspauschale, deren Höhe durch Rechtsverordnung nach § 16 bestimmt wird.
Durch Rechtsverordnung nach Satz 1 können ferner weitere Leistungen für die Schaffung besonderer Unterbringungsplätze zur Erfüllung zielgruppenspezifischer oder individueller Bedarfe bestimmt werden.

(7) Zur Unterstützung kommunaler Integrationsangebote erhalten die Landkreise und kreisfreien Städte in den Haushaltsjahren 2019 und 2020 eine jährliche Integrationspauschale in Höhe von 300 Euro pro Person

1.  für jede bis einschließlich 31.
    Dezember des jeweiligen Vorjahres aufgenommene Person nach § 4 Nummer 1 und 2,
2.  für jede am 1.
    Januar des jeweiligen Jahres leistungsberechtigte Person nach dem Asylbewerberleistungsgesetz und
3.  für jede zum 1.
    Januar des jeweiligen Jahres von der Erstattungsbehörde auf der Grundlage der statistischen Daten der Bundesagentur für Arbeit ermittelte regelleistungsberechtigte Person nach dem Zweiten Buch Sozialgesetzbuch aus den nichteuropäischen Asylherkunftsländern; für die Jahre 2019 und 2020 wird die Anzahl der Regelleistungsberechtigten nach dem Zweiten Buch Sozialgesetzbuch aus den nichteuropäischen Asylherkunftsländern zum 1. Januar 2016 von der Anzahl der Regelleistungsberechtigten nach dem Zweiten Buch Sozialgesetzbuch aus den nichteuropäischen Asylherkunftsländern zum 1.
    Januar des jeweiligen Jahres abgezogen.

Die Auszahlung der Integrationspauschale erfolgt durch die Erstattungsbehörde nach § 13 Absatz 2 Satz 3.
Die Landkreise und kreisfreien Städte sollen in angemessenem Umfang Mittel der Integrationspauschale an die kreisangehörigen Städte und Gemeinden zur Unterstützung ihrer Integrationsarbeit weiterleiten.
Bei Inanspruchnahme der Integrationspauschale durch die Landkreise und kreisfreien Städte berichten diese jährlich über die Verwendung der Mittel an den jeweiligen Kreistag beziehungsweise die jeweilige Stadtverordnetenversammlung.

#### § 15 Erstattung nach Kostennachweis

(1) Die notwendigen tatsächlichen Aufwendungen der Landkreise und kreisfreien Städte für Gesundheitsleistungen nach den §§ 4 und 6 des Asylbewerberleistungsgesetzes und die den Krankenkassen nach § 264 Absatz 7 des Fünften Buches Sozialgesetzbuch zu erstattenden Aufwendungen für die Übernahme der Krankenbehandlung von Empfängern laufender Leistungen nach § 2 des Asylbewerberleistungsgesetzes werden nach Kostennachweis gesondert erstattet.
Im Falle der Übernahme der Krankenbehandlung der Leistungsbeziehenden nach § 3 des Asylbewerberleistungsgesetzes durch eine Krankenkasse auf Grundlage des § 264 Absatz 1 des Fünften Buches Sozialgesetzbuch werden darüber hinaus die angemessenen personellen und sächlichen Verwaltungskosten erstattet.
Im Falle einer Vereinbarung nach § 264 Absatz 1 Satz 1 des Fünften Buches Sozialgesetzbuch bedarf diese der Genehmigung des für Soziales zuständigen Mitglieds der Landesregierung.

(2) Die notwendigen tatsächlichen Aufwendungen der Landkreise und kreisfreien Städte für sonstige Leistungen nach § 6 des Asylbewerberleistungsgesetzes mit Ausnahme der nach Absatz 1 zu erstattenden Gesundheitsleistungen werden nach Kostennachweis gesondert erstattet.

(3) Den Leistungsberechtigten nach den §§ 2 und 3 des Asylbewerberleistungsgesetzes gewährte Leistungen für Bildung und Teilhabe werden jeweils nach Kostennachweis gesondert erstattet.

(4) Für Leistungsbeziehende nach § 2 des Asylbewerberleistungsgesetzes werden weitergehende Leistungen auf Antrag erstattet, wenn die im Einzelfall notwendigen Kosten der Leistungsgewährung aufgrund besonderer Bedarfslagen, insbesondere Pflegebedürftigkeit oder Behinderung, die Pauschale nach § 14 Absatz 2 übersteigen.

(5) Vorhaltekosten, die infolge einer rechtzeitigen erstmaligen Bereitstellung der notwendigen Zahl von Unterbringungsplätzen nach § 6 Absatz 5 Satz 3 entstanden sind, werden den Landkreisen und kreisfreien Städten auf Antrag erstattet.

#### § 16 Verordnungsermächtigung

Gemäß Artikel 97 Absatz 3 Satz 2 und 3 der Verfassung des Landes Brandenburg werden die Bestimmungen über die Deckung der Kosten und der gebotene Ausgleich der Mehrbelastungen der Landkreise und kreisfreien Städte infolge der Aufgabenübertragungen nach diesem Gesetz in einer Rechtsverordnung näher geregelt.
Hierzu wird das für Soziales zuständige Mitglied der Landesregierung ermächtigt, im Einvernehmen mit den für Inneres und für Finanzen zuständigen Mitgliedern der Landesregierung durch Rechtsverordnung das Nähere zu bestimmen über

1.  die Höhe der Pauschale nach § 14 Absatz 1 und ihre jeweilige Fortschreibung,
2.  die Höhe der Pauschale und ihrer Bestandteile nach § 14 Absatz 2 und ihre jeweilige Fortschreibung unter Berücksichtigung der Form der vorläufigen Unterbringung und regionaler Unterschiede,
3.  die Höhe der Pauschalen nach § 14 Absatz 3 bis 5 und ihre jeweilige Anpassung an die Kostenentwicklung,
4.  Voraussetzungen für gesonderte pauschale Erstattungsleistungen nach § 14 Absatz 3 Satz 2,
5.  die Höhe der Investitionspauschale sowie weiterer Leistungen für die Schaffung besonderer Unterbringungsplätze und deren Voraussetzungen nach § 14 Absatz 6 und
6.  weitere Einzelheiten zu den Voraussetzungen und zu dem Verfahren der Kostenerstattung nach den §§ 13 bis 15.

### Teil 5

### Sonderaufsicht

#### § 17 Sonderaufsichtsbehörden

(1) Sonderaufsichtsbehörde für die Ämter und amtsfreien Gemeinden nach diesem Gesetz ist der Landrat als allgemeine untere Landesbehörde.

(2) Sonderaufsichtsbehörde für die Landkreise und kreisfreien Städte nach diesem Gesetz ist das für Soziales zuständige Ministerium.

#### § 18 Sonderaufsichtsrechtliche Befugnisse

(1) Die sonderaufsichtsrechtlichen Befugnisse richten sich nach den Vorschriften der Kommunalverfassung des Landes Brandenburg in der jeweils geltenden Fassung, soweit in nachfolgenden Absätzen nichts Abweichendes bestimmt ist.

(2) Zur Überprüfung der ordnungsgemäßen Aufgabenerfüllung, insbesondere der Unterbringung und Versorgung der Personen nach § 4, ist die Sonderaufsichtsbehörde oder eine von ihr bestimmte Behörde berechtigt, jederzeit Einrichtungen der vorläufigen Unterbringung zu betreten.

(3) Die Befugnis der Sonderaufsichtsbehörde nach § 121 Absatz 2 Satz 2 Nummer 3 der Kommunalverfassung des Landes Brandenburg, besondere Weisungen zu erteilen, ist nicht auf den Bereich der Gefahrenabwehr beschränkt.

(4) Im Falle eines sonderaufsichtsrechtlichen Tätigwerdens nach § 121 Absatz 3 der Kommunalverfassung des Landes Brandenburg sind die dadurch entstehenden Kosten mit den für die Aufgabenwahrnehmung bestimmten Erstattungsleistungen im Wege der Aufrechnung zu verrechnen.

(5) Klagen gegen sonderaufsichtsrechtliche Weisungen und die Ausübung des Selbsteintrittsrechts nach § 121 Absatz 3 der Kommunalverfassung des Landes Brandenburg haben keine aufschiebende Wirkung.
Ein Vorverfahren findet nicht statt.

### Teil 6

### Sonstige Bestimmungen

#### § 19 Datenverarbeitung

(1) Abweichend von Artikel 9 Absatz 1 der Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates vom 27.
April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr und zur Aufhebung der Richtlinie 95/46/EG (Datenschutz-Grundverordnung) (ABl.
L 119 vom 4.5.2016, S.
1; L 314 vom 22.11.2016, S.
72) ist die Verarbeitung besonderer Kategorien personenbezogener Daten zulässig, wenn ihre Kenntnis zur Erfüllung einer Aufgabe nach diesem Gesetz erforderlich ist.
Die Verarbeitung von biometrischen und genetischen Daten ist nur zulässig, soweit dies gesetzlich bestimmt ist.
§ 24 des Brandenburgischen Datenschutzgesetzes gilt entsprechend.

(2) Die nach diesem Gesetz zuständigen Behörden dürfen den mit der vorläufigen Unterbringung und Migrationssozialarbeit befassten Dritten personenbezogene Daten einschließlich der nach Absatz 1 erhobenen besonderen Kategorien personenbezogener Daten mit Ausnahme von biometrischen und genetischen Daten übermitteln, soweit dies zu deren Aufgabenerfüllung erforderlich ist und sich diese verpflichtet haben, die Daten nur für den Zweck zu verarbeiten, zu dessen Erfüllung sie ihnen übermittelt werden sollen.
Die Dritten haben die ihnen übermittelten Daten in demselben Umfang geheim zu halten wie die nach diesem Gesetz zuständigen Behörden.
§ 24 des Brandenburgischen Datenschutzgesetzes gilt entsprechend.
Die übermittelten Daten sind mit Beendigung der vorläufigen Unterbringung oder der Tätigkeit im Rahmen der Migrationssozialarbeit nach § 12 unverzüglich zu löschen.

(3) Die Landkreise und kreisfreien Städte dürfen dem Suchdienst des Deutschen Roten Kreuzes zum Zwecke der Familienzusammenführung Namen, Geburtsdatum, Herkunftsland und gegenwärtige Anschrift der von ihnen aufgenommenen Personen übermitteln.

#### § 20 Überprüfung und Anpassung der Kostenerstattung

(1) Die Bestimmungen zur Kostenerstattung nach den §§ 14 und 15 sind auf Grundlage der tatsächlichen notwendigen Aufwendungen spätestens im IV.
Quartal des Erstattungsjahres 2017 auf ihre Angemessenheit zu überprüfen und erforderlichenfalls anzupassen.

(2) Sofern die Pauschalen nach § 14 Absatz 1 bis 5 im Ergebnis der Überprüfung nach Absatz 1 die notwendigen Kosten der Aufgabenwahrnehmung nicht decken, haben die Landkreise und kreisfreien Städte Anspruch auf rückwirkende Erstattung des gegenüber der Anpassung der Pauschalen bestehenden Differenzbetrages.
Sofern die Pauschalen im Ergebnis der Überprüfung nach Absatz 1 die tatsächlichen notwendigen Kosten übersteigen, können die überzahlten Beträge mit der Kostenerstattung im Erstattungsjahr der Anpassung der Pauschalen verrechnet werden.

(3) Das für Soziales zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit den für Inneres und für Finanzen zuständigen Mitgliedern der Landesregierung das Nähere zum Verfahren der Überprüfung und Anpassung der Bestimmungen zur Kostenerstattung nach Absatz 1 sowie zum Kostenausgleich nach Absatz 2 durch Rechtsverordnung zu regeln.

#### § 21 Einschränkung von Grundrechten

Durch § 10 Absatz 4 Satz 1, § 12 Absatz 2 Satz 2, § 18 Absatz 2 sowie die §§ 19 und 20 Absatz 1 werden das Grundrecht auf Unverletzlichkeit der Wohnung (Artikel 13 Absatz 1 des Grundgesetzes, Artikel 15 Absatz 1 der Verfassung des Landes Brandenburg) und das Grundrecht auf Datenschutz (Artikel 11 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 22 Übergangsvorschrift

Für die Kostenerstattung bis einschließlich 31.
März 2016 gilt § 6 des Landesaufnahmegesetzes vom 17. Dezember 1996 (GVBl.
I S. 358, 360), das zuletzt durch Artikel 15 des Gesetzes von 13. März 2012 (GVBl.
I Nr. 16 S. 7) geändert worden ist, fort.

#### § 23 Inkrafttreten, Außerkrafttreten

Dieses Gesetz tritt am 1.
April 2016 in Kraft.
Gleichzeitig tritt das Landesaufnahmegesetz vom 17. Dezember 1996 (GVBl.
I S. 358, 360), das zuletzt durch Artikel 15 des Gesetzes vom 13. März 2012 (GVBl.
I Nr. 16 S. 7) geändert worden ist, außer Kraft.

Potsdam, den 15.
März 2016

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[1)](#*) Dieses Gesetz dient der Umsetzung der Richtlinie 2013/33/EU des Europäischen Parlaments und des Rates vom 26.6.2013 zur Festlegung von Normen für die Aufnahme von Personen, die internationalen Schutz beantragen (ABl.
L 180 vom 29.6.2013, S.
96).