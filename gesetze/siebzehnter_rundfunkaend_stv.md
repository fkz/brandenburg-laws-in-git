## Gesetz zum Siebzehnten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Siebzehnter Rundfunkänderungsstaatsvertrag)

Der Landtag hat das folgende Gesetz beschlossen:

### § 1

Dem am 18.
Juni 2015 unterzeichneten Siebzehnten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Siebzehnter Rundfunkänderungsstaatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2

(1) Das Mitglied des Fernsehrates des Zweiten Deutschen Fernsehens (ZDF) nach § 21 Absatz 1 Satz 1 Buchstabe q Doppelbuchstabe dd des durch den Siebzehnten Rundfunkänderungsstaatsvertrag geänderten ZDF-Staatsvertrages aus dem Bereich Senioren, Familie, Frauen und Jugend aus dem Land Brandenburg wird gemeinsam entsandt vom

1.  Seniorenrat des Landes Brandenburg e.
    V.,
2.  Deutschen Familienverband Landesverband Brandenburg e.
    V.,
3.  Frauenpolitischen Rat Land Brandenburg e.
    V.
    und
4.  Landesjugendring Brandenburg e.
    V.

(2) Kommt bis vier Wochen vor dem Ablauf der Amtsperiode des ZDF-Fernsehrates eine Einigung über die Entsendung nach Absatz 1 nicht zustande, so entscheidet in gemeinsamer Sitzung das Los zwischen den Vorschlägen der Organisationen.

### § 3

(1) § 1 tritt am Tag nach der Verkündung in Kraft.
§ 2 tritt an dem Tag in Kraft, an dem der Siebzehnte Rundfunkänderungsstaatsvertrag nach seinem Artikel 3 Absatz 2 Satz 1 in Kraft tritt.

(2) Der Staatsvertrag tritt nach seinem Artikel 3 Absatz 2 Satz 1 am 1.
Januar 2016 in Kraft.
Sollte der Staatsvertrag nach seinem Artikel 3 Absatz 2 Satz 2 gegenstandslos werden, ist dies unverzüglich im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 4.
Dezember 2015

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag](/de/vertraege-241530) - ZDF-Staatsvertrag

[zum Staatsvertrag](/de/vertraege-240375) - Rundfunkstaatsvertrag

* * *

### Anlagen

1

[Siebzehnter Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Siebzehnter Rundfunkänderungsstaatsvertrag)](/br2/sixcms/media.php/68/GVBl_I_35_2015-001Anlage-zum-Hauptdokument.pdf "Siebzehnter Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Siebzehnter Rundfunkänderungsstaatsvertrag)") 846.0 KB