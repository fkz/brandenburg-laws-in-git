## Gesetz über die Brandenburgische Kulturstiftung Cottbus-Frankfurt (Oder) (Brandenburgisches Kulturstiftungsgesetz - KultStG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Erweiterung, Name und Sitz

(1) Die durch das Gesetz über die Errichtung einer Brandenburgischen Kulturstiftung Cottbus vom 29. Juni 2004 (GVBl.
I S.
337) errichtete Stiftung „Brandenburgische Kulturstiftung Cottbus“ bleibt unter dem in Absatz 2 bestimmten Namen bestehen.
Sie wird um das Museum Junge Kunst Frankfurt (Oder) erweitert, das mit dem Kunstmuseum Dieselkraftwerk Cottbus zum Brandenburgischen Landesmuseum für moderne Kunst zusammengelegt wird.

(2) Die Stiftung trägt den Namen „Brandenburgische Kulturstiftung Cottbus-Frankfurt (Oder)“.

(3) Die Stiftung hat ihren Sitz in Cottbus.

#### § 2 Stiftungszweck

(1) Zweck der Stiftung ist die Pflege der Kunst und Kultur durch den Betrieb des Staatstheaters Cottbus und des Brandenburgischen Landesmuseums für moderne Kunst mit den Sammlungen an den Standorten Cottbus und Frankfurt (Oder).

(2) Die Stiftung führt das Staatstheater Cottbus als Mehrspartentheater.

(3) Die Stiftung führt das Brandenburgische Landesmuseum für moderne Kunst unter der Maßgabe, Kunstwerke und Materialien zur Kunst- und Kulturgeschichte im Wesentlichen von 1945 bis zur Gegenwart zu sammeln, zu bewahren, zu pflegen, zu erforschen und der Wissenschaft zugänglich zu machen sowie den Sammlungsbestand in Ausstellungen der Öffentlichkeit zu vermitteln.
Die Präsentation von Sonderausstellungen sowie Aktivitäten im Bereich der kulturellen Bildung gehören zum Profil des Museums.

(4) Die Stiftung verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke im Sinne des Abschnitts „Steuerbegünstigte Zwecke“ der Abgabenordnung.

#### § 3 Stiftungsvermögen

(1) Zum Stiftungsvermögen gehören:

1.  die in Anlage 1 aufgeführten Liegenschaften,
2.  die für das Staatstheater Cottbus und das Brandenburgische Landesmuseum für moderne Kunst erworbenen beweglichen Vermögensbestände,
3.  die für den Betrieb des Staatstheaters Cottbus und des Brandenburgischen Landesmuseums für moderne Kunst erworbenen oder durch ihn entstandenen Rechte,
4.  die in Anlage 2 aufgeführte Kunstsammlung des Kunstmuseums Dieselkraftwerk Cottbus; ferner die Besitzrechte an Kunstgegenständen, die nicht Gegenstand des Stiftungsvermögens sind und durch die jeweiligen Leihgeber der Stiftung zur unentgeltlichen Nutzung überlassen werden,
5.  die in Anlage 3 aufgeführte Kunstsammlung des Museums Junge Kunst Frankfurt (Oder); ferner die Besitzrechte an Kunstgegenständen, die nicht Gegenstand des Stiftungsvermögens sind und durch die jeweiligen Leihgeber der Stiftung zur unentgeltlichen Nutzung überlassen werden.

(2) Die Nutzung der Liegenschaften, die für das Staatstheater Cottbus und das Brandenburgische Landesmuseum für moderne Kunst genutzt werden und die im Eigentum der Stadt Cottbus und im Eigentum der Stadt Frankfurt (Oder) verbleiben, regelt das zwischen dem Land Brandenburg, der Stadt Cottbus und der Stadt Frankfurt (Oder) geschlossene Finanzierungsabkommen.

(3) Das Land Brandenburg, die Stadt Cottbus und die Stadt Frankfurt (Oder) können der Stiftung weitere Liegenschaften übertragen.

(4) Das Stiftungsvermögen ist in seinem Bestand ungeschmälert zu erhalten.

#### § 4 Finanzierung

(1) Zur Erfüllung des Stiftungszwecks erhält die Stiftung jährliche Zuwendungen im Sinne der öffentlichen Haushaltsvorschriften nach Maßgabe des Landeshaushalts sowie der kommunalen Haushalte sowie eines zwischen dem Land Brandenburg, der Stadt Cottbus und der Stadt Frankfurt (Oder) geschlossenen Finanzierungsabkommens.

(2) Die Wirtschaftsführung und das Rechnungswesen der Stiftung richten sich nach kaufmännischen Grundsätzen.

(3) Der bis zum Ende des Geschäftsjahres nicht verbrauchte Teil der jährlichen Zuwendungen nach Absatz 1 steht der Stiftung zur Finanzierung ihrer Aufgaben zusätzlich zur Verfügung und kann dem Stiftungsvermögen zugeführt werden.
Es können Rücklagen gebildet werden.

(4) Die Stiftung ist berechtigt, Schenkungen, Erbschaften, Zustiftungen und Zuwendungen von dritter Seite zur Erfüllung des Stiftungszwecks anzunehmen.

(5) Die Erträge des Stiftungsvermögens und sonstige Einnahmen sind nur im Sinne des Stiftungszwecks zu verwenden.
Die Stiftung ist selbstlos tätig; sie verfolgt nicht in erster Linie eigenwirtschaftliche Zwecke.
Es darf keine Person durch Ausgaben, die dem Zweck der Stiftung fremd sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.

#### § 5 Rechtsaufsicht

Die Rechtsaufsicht über die Stiftung führt die für Kultur zuständige oberste Landesbehörde.

#### § 6 Organe der Stiftung

(1) Organe der Stiftung sind:

1.  der Stiftungsrat und
2.  der Vorstand.

(2) Zur Beratung der Organe in Belangen des Brandenburgischen Landesmuseums für moderne Kunst wird ein Fachbeirat gebildet.

#### § 7 Zusammensetzung des Stiftungsrates

(1) Dem Stiftungsrat gehören an:

1.  zwei Vertreterinnen oder Vertreter der für Kultur zuständigen obersten Landesbehörde, die nicht zugleich mit der Rechtsaufsicht über die Stiftung befasst sind, eine oder einer davon als Vorsitzende oder als Vorsitzender des Stiftungsrates,
2.  eine Vertreterin oder ein Vertreter der für Finanzen zuständigen obersten Landesbehörde,
3.  zwei Vertreterinnen oder Vertreter der Stadt Cottbus, eine oder einer davon als erste Stellvertreterin oder als erster Stellvertreter der oder des Vorsitzenden,
4.  eine Vertreterin oder ein Vertreter der Stadt Frankfurt (Oder), zugleich zweite Stellvertreterin oder zweiter Stellvertreter der oder des Vorsitzenden,
5.  ein Mitglied des Landtages Brandenburg, ein Mitglied der Stadtverordnetenversammlung Cottbus sowie ein Mitglied der Stadtverordnetenversammlung Frankfurt (Oder).

(2) Für jedes Mitglied ist eine Vertreterin oder ein Vertreter zu bestellen.
Sind das Mitglied und seine Vertreterin oder sein Vertreter verhindert, kann das Mitglied eine Bevollmächtigte oder einen Bevollmächtigten entsenden.

(3) An den Sitzungen des Stiftungsrates nehmen die Mitglieder des Vorstandes teil.
Weitere Vertreterinnen oder Vertreter der für Kultur und Finanzen zuständigen obersten Landesbehörden und der Städte Cottbus und Frankfurt (Oder) sowie auf Vorschlag der oder des Stiftungsratsvorsitzenden die oder der Vorsitzende des Fachbeirates können beratend teilnehmen.
Weitere Personen können zu den Sitzungen beratend hinzugezogen werden.
Das Nähere dazu regelt die Satzung.

(4) Die Mitglieder nach Absatz 1 sind unentgeltlich tätig.
Sie haben Anspruch auf Ersatz ihrer notwendigen Aufwendungen nach Maßgabe der allgemein für die Verwaltung des Landes Brandenburg geltenden Bestimmungen.

#### § 8 Aufgaben des Stiftungsrates

(1) Der Stiftungsrat erlässt und ändert eine Satzung nach Maßgabe dieses Gesetzes.
Die Satzung bedarf der Zustimmung der für Kultur zuständigen obersten Landesbehörde, die im Einvernehmen mit der für Finanzen zuständigen obersten Landesbehörde entscheidet.
Entsprechendes gilt bei Änderungen der Satzung.

(2) Der Stiftungsrat beschließt über die grundsätzlichen Angelegenheiten der Stiftung und legt die wesentlichen Aufgaben und Tätigkeiten innerhalb der Stiftung fest.
Der Stiftungsrat kann dem Vorstand unter Einhaltung der Kunstfreiheit nach Artikel 5 Absatz 3 des Grundgesetzes und Artikel 34 Absatz 1 der Verfassung des Landes Brandenburg Weisungen erteilen sowie Auskunft und Bericht verlangen.
Der Stiftungsrat entscheidet insbesondere über:

1.  die Feststellung des Wirtschaftsplans und der Finanzplanung,
2.  die Feststellung des geprüften Jahresabschlusses,
3.  die Einstellung und Entlassung der Mitglieder des Vorstandes,
4.  die Entlastung des Vorstandes,
5.  alle nicht nach § 11 dem Vorstand obliegenden Geschäfte und
6.  die Vorlagen des Vorstandes im Falle eines Dissenses innerhalb des Vorstandes.

Der Stiftungsrat berät die vom Vorstand vorzuschlagenden jährlichen und mehrjährigen Arbeits- und Veranstaltungsprogramme des Staatstheaters Cottbus und des Brandenburgischen Landesmuseums für moderne Kunst.
Er kann den Fachbeirat in fachlichen Angelegenheiten anrufen.
Er beauftragt ein Wirtschaftsprüfungsunternehmen mit der Prüfung des Jahresabschlusses.

(3) Der vorherigen Zustimmung durch den Stiftungsrat bedürfen:

1.  der Erwerb, die Veräußerung und die Belastung von Grundstücken sowie langfristige Anmietungen,
2.  der Abschluss, die Änderung und Kündigung von Anstellungsverträgen mit der Verwaltungsleiterin oder dem Verwaltungsleiter, der Schauspieldirektorin oder dem Schauspieldirektor, der Operndirektorin oder dem Operndirektor und der Generalmusikdirektorin oder dem Generalmusikdirektor, der ersten Kapellmeisterin oder dem ersten Kapellmeister, der stellvertretenden Leiterin oder dem stellvertretenden Leiter des Brandenburgischen Landesmuseums für moderne Kunst sowie mit Beschäftigten ab einer Vergütung der Vergütungsgruppe E 13 des Tarifvertrages für den öffentlichen Dienst der Länder oder E 13 des Tarifvertrages für den öffentlichen Dienst des Kommunalen Arbeitgeberverbandes Brandenburg, ferner die Gewährung sonstiger über- oder außertariflicher Leistungen,
3.  Entscheidungen über die Stiftungsstruktur,
4.  die Übernahme von Bürgschaften,
5.  die Annahme von Erbschaften, Schenkungen und Zuwendungen und
6.  die Veräußerung von Kunstwerken der Kunstsammlungen.
    Eine Veräußerung ist nur zulässig, wenn mit dem Veräußerungserlös der Sammlungsbestand ergänzt wird.
    Eine Veräußerung bedarf eines Beschlusses gemäß § 9 Absatz 3 Satz 1 und 2.

(4) Der Stiftungsrat kann die Stiftungsratsvorsitzende oder den Stiftungsratsvorsitzenden beauftragen, über die Zustimmung in einzelnen, von ihm festgelegten Bereichen allein zu entscheiden.
Dies gilt nicht für den Erwerb, die Veräußerung und die Belastung von Grundstücken und die Veräußerung von Kunstwerken.
Das Nähere regelt die Satzung.

(5) Der Stiftungsrat kann Stiftungskommissionen als Ausschüsse des Stiftungsrates einsetzen.
Das Nähere regelt die Satzung.

#### § 9 Verfahren im Stiftungsrat

(1) Der Stiftungsrat tritt mindestens zweimal jährlich zu einer ordentlichen Sitzung zusammen.
Auf Antrag von mindestens einem Drittel der Mitglieder muss der Stiftungsrat zu weiteren Sitzungen zusammentreten.
Der Stiftungsrat entscheidet in der Regel in seinen Sitzungen.

(2) Der Stiftungsrat ist beschlussfähig, wenn zwei Drittel der Mitglieder und hiervon jeweils eine Vertreterin oder ein Vertreter des Landes und der Städte anwesend oder gemäß § 7 Absatz 2 vertreten sind.
Die Beschlüsse werden mit der Stimmenmehrheit der Anwesenden gefasst.

(3) Beschlüsse über den Inhalt der Satzung, über den Wirtschaftsplan und dessen Änderung, über grundsätzliche Angelegenheiten des Staatstheaters Cottbus oder des Brandenburgischen Landesmuseums für moderne Kunst sowie Beschlüsse, die über bestehende Wirtschaftspläne hinaus Auswirkungen auf den Haushalt der Stiftung haben, können nur mit Zustimmung von zwei Dritteln der Mitglieder des Stiftungsrates getroffen werden.
Eine Beschlussfassung gegen die Stimme eines Stiftungsratsmitglieds gemäß § 7 Absatz 1 Nummer 1 bis 4 ist nicht möglich. In Angelegenheiten, die ausschließlich das Staatstheater Cottbus betreffen, kann ein Beschluss gegen die Vertreterin oder den Vertreter der Stadt Frankfurt (Oder) gemäß § 7 Absatz 1 Nummer 4 getroffen werden.

(4) Das Nähere regelt die Satzung.

#### § 10 Zusammensetzung des Vorstandes

Der Stiftungsvorstand besteht aus der Intendantin oder dem Intendanten des Staatstheaters Cottbus, der Museumsdirektorin oder dem Museumsdirektor und der Verwaltungsdirektorin oder dem Verwaltungsdirektor der Stiftung.
Der Stiftungsrat beruft auf Vorschlag des Vorstandes aus der Mitte des Vorstandes eine Vorsitzende oder einen Vorsitzenden.
Die oder der Vorsitzende kann vom Stiftungsrat jederzeit abberufen werden.

#### § 11 Aufgaben des Vorstandes

(1) Der Vorstand sorgt für eine positive Entwicklung aller Bereiche der Stiftung.

(2) Der Vorstand vertritt die Stiftung gerichtlich und außergerichtlich.
Die Satzung kann vorsehen, dass einzelne Mitglieder des Vorstandes die Stiftung allein vertreten.
Gegenüber dem Vorstand wird die Stiftung durch die Vorsitzende oder den Vorsitzenden des Stiftungsrates vertreten.

(3) Der Vorstand ist für die Erledigung der laufenden Verwaltung zuständig.
Er ist weiterhin zuständig für:

1.  die Aufstellung des Wirtschaftsplans und der Finanzplanung sowie Vermögensangelegenheiten,
2.  die Aufstellung des Jahresabschlusses einschließlich einer Vermögensübersicht und die Vorbereitung, ein Wirtschaftsprüfungsunternehmen zu beauftragen,
3.  die Vorbereitung der Sitzungen und Entscheidungen des Stiftungsrates und seiner Ausschüsse,
4.  die jährliche Aufstellung eines Berichts über die Erfüllung des Stiftungszwecks und
5.  Organisations- und Personalangelegenheiten, die die übergreifende Verwaltung des Staatstheaters Cottbus und des Brandenburgischen Landesmuseums für moderne Kunst betreffen.

(4) Das Personal der Stiftung wird vom Vorstand angestellt und entlassen.
§ 8 Absatz 3 Nummer 2 bleibt unberührt.

#### § 12 Verfahren im Vorstand

(1) Der Vorstand entscheidet, soweit nachfolgend nichts anderes geregelt ist, mit der Mehrheit seiner Mitglieder.
In allen Angelegenheiten der Stiftung gemäß § 11 Absatz 3 Satz 2 Nummer 1 bis 5 entscheidet der Vorstand einstimmig.
Für den Fall, dass kein einstimmiger Beschluss zustande kommt, legt der Vorstand die Angelegenheit dem Stiftungsrat vor.
Das Nähere regelt die Satzung.

(2) Unaufschiebbare, finanzwirksame Angelegenheiten der laufenden Verwaltung können nicht ohne Zustimmung der Verwaltungsdirektorin oder des Verwaltungsdirektors entschieden werden.

(3) Personalangelegenheiten, die das Staatstheater Cottbus betreffen, können nicht ohne die Zustimmung der Intendantin oder des Intendanten, solche, die das Brandenburgische Landesmuseum für moderne Kunst betreffen, nicht ohne Zustimmung der Museumsdirektorin oder des Museumsdirektors entschieden werden.

(4) Der Vorstand gibt sich mit Zustimmung des Stiftungsrates eine Geschäftsordnung.
Im Übrigen gelten die Regelungen in der Satzung.

#### § 13 Fachbeirat

(1) Der Fachbeirat besteht aus bis zu sieben Personen aus unterschiedlichen Bereichen der Kunst.
Mindestens zwei davon sollen über internationale Erfahrungen verfügen.
Sie werden auf Vorschlag der Museumsdirektorin oder des Museumsdirektors vom Stiftungsrat bestellt.
Die Mitglieder des Fachbeirates werden für die Dauer von drei Jahren bestellt.
Eine Wiederbestellung ist zulässig.

(2) Der Fachbeirat wählt für die Dauer der Amtszeit aus seiner Mitte jeweils eine Beiratsvorsitzende oder einen Beiratsvorsitzenden sowie eine stellvertretende Beiratsvorsitzende oder einen stellvertretenden Beiratsvorsitzenden.
Eine Wiederwahl ist zulässig.

(3) Die Mitglieder des Beirates sind ehrenamtlich und unentgeltlich tätig.
Das Nähere regelt die Satzung.

#### § 14 Aufgaben des Fachbeirates

(1) Der Fachbeirat berät die Organe der Stiftung und dabei insbesondere die Museumsdirektorin oder den Museumsdirektor in fachlichen Angelegenheiten des Brandenburgischen Landesmuseums für moderne Kunst.
Er gibt hierzu Empfehlungen ab.

(2) Der Fachbeirat soll mindestens einmal im Jahr zu einer ordentlichen Sitzung zusammentreffen.
Eine außerordentliche Sitzung ist einzuberufen, wenn mindestens ein Drittel der Mitglieder oder der Stiftungsrat dies verlangen.
Die Museumsdirektorin oder der Museumsdirektor bereitet die Sitzungen in Abstimmung mit der oder dem Beiratsvorsitzenden vor und nimmt an den Sitzungen teil.

#### § 15 Geschäftsjahr, Wirtschaftsplan und Rechnungsprüfung

(1) Der Wirtschaftsplan der Stiftung ist jährlich rechtzeitig vor Beginn des Haushaltsjahres vom Vorstand im Entwurf aufzustellen.
Er wird vom Stiftungsrat festgestellt.

(2) Das Geschäftsjahr der Stiftung ist das Kalenderjahr.

(3) Die Wirtschaftsführung und das Rechnungswesen der Stiftung richten sich nach kaufmännischen Grundsätzen.

#### § 16 Personal

Für das Personal finden der Anwendungstarifvertrag zur Übernahme des TV-L und TVÜ-L für die Brandenburgische Kulturstiftung Cottbus, der Normalvertrag Bühne und der Tarifvertrag für Musiker in Kulturorchestern in der jeweils geltenden Fassung sowie die diese ergänzenden, ändernden oder ersetzenden Tarifverträge Anwendung.

#### § 17 Überleitung

(1) Mit Inkrafttreten dieses Gesetzes gehen die Arbeitsverhältnisse mit den derzeitigen Stelleninhaberinnen und Stelleninhabern des Eigenbetriebes Kulturbetriebe Frankfurt (Oder) bezogen auf das Museum Junge Kunst auf die Brandenburgische Kulturstiftung Cottbus-Frankfurt (Oder) über.
Die Stiftung tritt vorbehaltlich der Regelung des Absatzes 2 in die Rechte und Pflichten aus diesen Arbeitsverhältnissen ein.

(2) Für die nach Absatz 1 übernommenen Beschäftigten finden der Tarifvertrag des öffentlichen Dienstes des Kommunalen Arbeitgeberverbandes Brandenburg in der jeweils geltenden Fassung sowie die diesen ergänzenden, ändernden oder ersetzenden Tarifverträge Anwendung mit Ausnahme des Altersvorsorge-Tarifvertrages-Kommunal.
Die betriebliche Altersversorgung richtet sich nach dem Tarifvertrag Altersversorgung.

(3) Die bestehenden Arbeits- und Ausbildungsverhältnisse mit der Brandenburgischen Kulturstiftung Cottbus gelten unverändert fort.

#### § 18 Stiftungsaufhebung

(1) Die Stiftung kann durch Gesetz aufgehoben werden.

(2) Bei Aufhebung der Stiftung fallen sämtliche Vermögensgegenstände dem Land Brandenburg zu, soweit sie von diesem eingebracht worden sind oder per Gesetz auf die Stiftung übertragen worden sind.
Vermögensgegenstände, die die Städte Cottbus und Frankfurt (Oder) eingebracht haben oder die von diesen per Gesetz auf die Stiftung übertragen worden sind, fallen bei Stiftungsaufhebung der jeweiligen Stadt zu.
Im Übrigen fällt das Vermögen dem Land Brandenburg zu.
Das Land Brandenburg und die Städte Cottbus und Frankfurt (Oder) treffen im Falle der Stiftungsaufhebung eine Vereinbarung, nach der das Stiftungsvermögen, das während des Bestehens des Brandenburgischen Landesmuseums für moderne Kunst hinzugewonnen worden ist, anteilig und unter Berücksichtigung der finanziellen Förderung des Brandenburgischen Landesmuseums für moderne Kunst durch die Stadt Cottbus und die Stadt Frankfurt (Oder) der jeweiligen Stadt zufällt.

(3) Soweit Vermögen dem Land Brandenburg und den Städten Cottbus und Frankfurt (Oder) zufällt, ist dieses unmittelbar und ausschließlich für gemeinnützige Zwecke im Sinne des Abschnitts „Steuerbegünstigte Zwecke“ der Abgabenordnung und in einer dem Stiftungszweck möglichst nahekommenden Weise zu verwenden.

#### § 19 Inkrafttreten, Außerkrafttreten

Dieses Gesetz tritt am 1.
Juli 2017 in Kraft.
Gleichzeitig tritt das Gesetz über die Errichtung einer Brandenburgischen Kulturstiftung Cottbus vom 29.
Juni 2004 (GVBl.
I S.
337) außer Kraft.

Potsdam, den 30.
Juni 2017

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

### Anlagen

1

[Anlage 1 (zu § 3 Absatz 1 Nummer 1) - Liegenschaften der Brandenburgischen Kulturstiftung Cottbus-Frankfurt (Oder)](/br2/sixcms/media.php/68/GVBl_I_13_2017_001Anlage-1.pdf "Anlage 1 (zu § 3 Absatz 1 Nummer 1) - Liegenschaften der Brandenburgischen Kulturstiftung Cottbus-Frankfurt (Oder)") 573.7 KB

2

[Anlage 2 (zu § 3 Absatz 1 Nummer 4) - Sammlungsbestand Kunstmuseum Dieselkraftwerk Cottbus](/br2/sixcms/media.php/68/GVBl_I_13_2017_002Anlage-2.pdf "Anlage 2 (zu § 3 Absatz 1 Nummer 4) - Sammlungsbestand Kunstmuseum Dieselkraftwerk Cottbus") 650.7 KB

3

[Anlage 3 (zu § 3 Absatz 1 Nummer 5) - Sammlungsbestand Museum Junge Kunst Frankfurt (Oder)](/br2/sixcms/media.php/68/GVBl_I_13_2017_003Anlage-3.pdf "Anlage 3 (zu § 3 Absatz 1 Nummer 5) - Sammlungsbestand Museum Junge Kunst Frankfurt (Oder)") 616.1 KB