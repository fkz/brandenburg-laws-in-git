## Brandenburgische Bauordnung (BbgBO)

**Inhaltsübersicht**

Teil 1  
Allgemeine Vorschriften
--------------------------------

[§ 1 Anwendungsbereich](#1)

[§ 2 Begriffe](#2)

[§ 3 Allgemeine Anforderungen](#3)

Teil 2  
Das Grundstück und seine Bebauung
------------------------------------------

[§ 4 Bebauung der Grundstücke mit Gebäuden](#4)

[§ 5 Zugänge und Zufahrten auf den Grundstücken](#5)

[§ 6 Abstandsflächen, Abstände](#6)

[§ 7 Teilung von Grundstücken](#7)

[§ 8 Nicht überbaute Flächen der bebauten Grundstücke, Kinderspielplätze](#8)

Teil 3  
Bauliche Anlagen
-------------------------

### Abschnitt 1  
Gestaltung

[§ 9 Gestaltung](#9)

[§ 10 Anlagen der Außenwerbung](#10)

### Abschnitt 2  
Allgemeine Anforderungen an die Bauausführung

[§ 11 Baustelle](#11)

[§ 12 Standsicherheit](#12)

[§ 13 Schutz gegen schädliche Einflüsse](#13)

[§ 14 Brandschutz](#14)

[§ 15 Wärme-, Schall-, Erschütterungsschutz](#15)

[§ 16 Verkehrssicherheit](#16)

[§ 16a Bauarten](#17)

### Abschnitt 3  
Bauprodukte

[§ 16b Allgemeine Anforderungen für die Verwendung von Bauprodukten](#18)

[§ 16c Anforderungen für die Verwendung von CE-gekennzeichneten Bauprodukten](#19)

[§ 17 Verwendbarkeitsnachweise](#20)

[§ 18 Allgemeine bauaufsichtliche Zulassung](#21)

[§ 19 Allgemeines bauaufsichtliches Prüfzeugnis](#22)

[§ 20 Nachweis der Verwendbarkeit von Bauprodukten im Einzelfall](#23)

[§ 21 Übereinstimmungsbestätigung](#24)

[§ 22 Übereinstimmungserklärung der Herstellerin oder des Herstellers](#25)

[§ 23 Zertifizierung](#26)

[§ 24 Prüf-, Zertifizierungs-, Überwachungsstellen](#27)

[§ 25 Besondere Sachkunde- und Sorgfaltsanforderungen](#28)

### Abschnitt 4  
Brandverhalten von Baustoffen und Bauteilen; Wände, Decken, Dächer

[§ 26 Allgemeine Anforderungen an das Brandverhalten von Baustoffen und Bauteilen](#29)

[§ 27 Tragende Wände, Stützen](#30)

[§ 28 Außenwände](#31)

[§ 29 Trennwände](#32)

[§ 30 Brandwände](#33)

[§ 31 Decken](#34)

[§ 32 Dächer](#35)

### Abschnitt 5  
Rettungswege, Öffnungen, Umwehrungen

[§ 33 Erster und zweiter Rettungsweg](#36)

[§ 34 Treppen](#37)

[§ 35 Notwendige Treppenräume, Ausgänge](#38)

[§ 36 Notwendige Flure, offene Gänge](#39)

[§ 37 Fenster, Türen, sonstige Öffnungen](#40)

[§ 38 Umwehrungen](#41)

### Abschnitt 6  
Technische Gebäudeausrüstung

[§ 39 Aufzüge](#42)

[§ 40 Leitungsanlagen, Installationsschächte und Installationskanäle](#43)

[§ 41 Lüftungsanlagen](#44)

[§ 42 Feuerungsanlagen, sonstige Anlagen zur Wärmeerzeugung, Brennstoffversorgung](#45)

[§ 43 Sanitäre Anlagen](#46)

[§ 44 Kleinkläranlagen, Gruben](#47)

[§ 45 Aufbewahrung fester Abfallstoffe](#48)

[§ 46 Blitzschutzanlagen](#49)

### Abschnitt 7  
Nutzungsbedingte Anforderungen

[§ 47 Aufenthaltsräume](#50)

[§ 48 Wohnungen](#51)

[§ 49 Notwendige Stellplätze und notwendige Abstellplätze für Fahrräder](#52)

[§ 50 Barrierefreies Bauen](#53)

[§ 51 Sonderbauten](#54)

Teil 4  
Die am Bau Beteiligten
-------------------------------

[§ 52 Grundpflichten](#55)

[§ 53 Bauherrin und Bauherr](#56)

[§ 54 Entwurfsverfasserin und Entwurfsverfasser](#57)

[§ 55 Unternehmerin und Unternehmer](#58)

[§ 56 Bauleiterin und Bauleiter](#59)

Teil 5  
Bauaufsichtsbehörden, Verfahren
----------------------------------------

### Abschnitt 1  
Bauaufsichtsbehörden

[§ 57 Aufbau und Zuständigkeit der Bauaufsichtsbehörden](#60)

[§ 58 Aufgaben und Befugnisse der Bauaufsichtsbehörden sowie der amtsfreien Gemeinden, der Ämter, der Verbandsgemeinden, mitverwalteten Gemeinden und mitverwaltenden Gemeinden als Sonderordnungsbehörden](#61)

### Abschnitt 2  
Genehmigungspflicht, Genehmigungsfreiheit

[§ 59 Grundsatz](#62)

[§ 60 Vorrang anderer Gestattungsverfahren](#63)

[§ 61 Genehmigungsfreie Vorhaben](#64)

[§ 62 Bauanzeigeverfahren](#65)

### Abschnitt 3  
Genehmigungsverfahren

[§ 63 Vereinfachtes Baugenehmigungsverfahren](#66)

[§ 64 Baugenehmigungsverfahren](#67)

[§ 65 Bauvorlageberechtigung](#68)

[§ 66 Bautechnische Nachweise](#69)

[§ 67 Abweichungen](#70)

[§ 68 Bauantrag, Bauvorlagen](#71)

[§ 69 Behandlung des Bauantrags](#72)

[§ 70 Beteiligung der Nachbarn und der Öffentlichkeit](#73)

[§ 71 Ersetzung des gemeindlichen Einvernehmens](#74)

[§ 72 Baugenehmigung, Baubeginn](#75)

[§ 72a Typengenehmigung](#75a)

[§ 73 Geltungsdauer der Baugenehmigung und des Vorbescheides](#76)

[§ 74 Teilbaugenehmigung](#77)

[§ 75 Vorbescheid](#78)

[§ 76 Genehmigung Fliegender Bauten](#79)

[§ 77 Bauaufsichtliche Zustimmung](#80)

### Abschnitt 4  
Bauaufsichtliche Maßnahmen

[§ 78 Verbot unrechtmäßig gekennzeichneter Bauprodukte](#81)

[§ 79 Einstellung von Arbeiten](#82)

[§ 80 Beseitigung von Anlagen, Nutzungsuntersagung](#83)

[§ 81 Anpassung bestehender baulicher Anlagen](#84)

### Abschnitt 5  
Bauüberwachung

[§ 82 Bauüberwachung](#85)

[§ 83 Bauzustandsanzeigen, Aufnahme der Nutzung](#86)

### Abschnitt 6  
Baulasten

[§ 84 Baulasten, Baulastenverzeichnis](#87)

Teil 6  
Ordnungswidrigkeiten, Rechtsvorschriften, Übergangsvorschriften
------------------------------------------------------------------------

[§ 85 Ordnungswidrigkeiten](#88)

[§ 86 Rechtsvorschriften](#89)

[§ 86 a Technische Baubestimmungen](#90)

[§ 87 Örtliche Bauvorschriften](#91)

[§ 88 Übergangsvorschriften](#92)

Teil 1  
Allgemeine Vorschriften
--------------------------------

#### § 1 Anwendungsbereich

(1) Dieses Gesetz gilt für bauliche Anlagen und Bauprodukte.
Es gilt auch für Grundstücke sowie für andere Anlagen und Einrichtungen, an die in diesem Gesetz oder in Vorschriften aufgrund dieses Gesetzes Anforderungen gestellt werden.

(2) Dieses Gesetz gilt nicht für

1.  Anlagen des öffentlichen Verkehrs, einschließlich Zubehör, Nebenanlagen und Nebenbetrieben, ausgenommen Gebäude,
2.  Anlagen, die der Bergaufsicht unterliegen, ausgenommen Gebäude,
3.  Leitungen, die der öffentlichen Versorgung mit Wasser, Gas, Elektrizität, Wärme, der öffentlichen Abwasserentsorgung oder der Telekommunikation dienen, mit Ausnahme von Masten und Unterstützungen,
4.  Rohrleitungen, die dem Ferntransport von Stoffen dienen, mit Ausnahme von Masten und Unterstützungen,
5.  Kräne, mit Ausnahme von Kranbahnen und Unterstützungen,
6.  Messestände in Messe- und Ausstellungsgebäuden,
7.  Anschläge und Lichtwerbung an dafür genehmigten Säulen, Tafeln und Flächen,
8.  Werbemittel an Zeitungs- und Zeitschriftenverkaufsstellen,
9.  Auslagen und Dekorationen in Fenstern und Schaukästen,
10.  Wahlwerbung für die Dauer eines Wahlkampfes,
11.  Parkanlagen und andere Grünflächen, die öffentliche Einrichtungen sind, sowie Friedhöfe, mit Ausnahme von Gebäuden,
12.  Sport- und Charterboote, die zweckentsprechend als Wasserfahrzeuge genutzt werden können und sollen.

#### § 2 Begriffe

(1) Bauliche Anlagen sind mit dem Erdboden verbundene, aus Bauprodukten hergestellte Anlagen; eine Verbindung mit dem Boden besteht auch dann, wenn die Anlage durch eigene Schwere auf dem Boden ruht oder auf ortsfesten Bahnen begrenzt beweglich ist oder wenn die Anlage nach ihrem Verwendungszweck dazu bestimmt ist, überwiegend ortsfest benutzt zu werden.
Bauliche Anlagen sind auch

1.  Aufschüttungen und Abgrabungen,
2.  Lagerplätze, Abstellplätze und Ausstellungsplätze,
3.  Sport- und Spielflächen,
4.  Campingplätze, Wochenendplätze und Zeltplätze,
5.  Freizeit- und Vergnügungsparks,
6.  Stellplätze für Kraftfahrzeuge und Abstellplätze für Fahrräder,
7.  Gerüste,
8.  Hilfseinrichtungen zur statischen Sicherung von Bauzuständen.

Anlagen sind bauliche Anlagen und sonstige Anlagen und Einrichtungen im Sinne des § 1 Absatz 1 Satz 2.

(2) Gebäude sind selbstständig benutzbare, überdeckte bauliche Anlagen, die von Menschen betreten werden können und geeignet oder bestimmt sind, dem Schutz von Menschen, Tieren oder Sachen zu dienen.

(3) Gebäude werden in folgende Gebäudeklassen eingeteilt:

1.  Gebäudeklasse 1:
    1.  freistehende Gebäude mit einer Höhe bis zu 7 Meter und nicht mehr als zwei Nutzungseinheiten von insgesamt nicht mehr als 400 Quadratmeter Grundfläche und
    2.  freistehende land- oder forstwirtschaftlich genutzte Gebäude,
2.  Gebäudeklasse 2:
    
    Gebäude mit einer Höhe bis zu 7 Meter und nicht mehr als zwei Nutzungseinheiten von insgesamt nicht mehr als 400 Quadratmeter Grundfläche,
    
3.  Gebäudeklasse 3:
    
    sonstige Gebäude mit einer Höhe bis zu 7 Meter,
    
4.  Gebäudeklasse 4:
    
    Gebäude mit einer Höhe bis zu 13 Meter und Nutzungseinheiten mit jeweils nicht mehr als 400 Quadratmeter Grundfläche,
    
5.  Gebäudeklasse 5:
    
    sonstige Gebäude einschließlich unterirdischer Gebäude.
    

Höhe im Sinne des Satzes 1 ist das Maß der Fußbodenoberkante des höchstgelegenen Geschosses, in dem ein Aufenthaltsraum möglich ist, über der Geländeoberfläche im Mittel.
Die Grundflächen der Nutzungseinheiten im Sinne dieses Gesetzes sind die Brutto-Grundflächen; bei der Berechnung der Brutto-Grundflächen nach Satz 1 bleiben Flächen in Kellergeschossen außer Betracht.
Wird ein Nebengebäude an Gebäude der Gebäudeklasse 1 angebaut, verändert sich die Gebäudeklasse nicht, wenn das Nebengebäude nach § 61 Absatz 1 Nummer 1 Buchstabe c oder Buchstabe d genehmigungsfrei ist.

(4) Sonderbauten sind Anlagen und Räume besonderer Art oder Nutzung, die einen der nachfolgenden Tatbestände erfüllen:

1.  Hochhäuser (Gebäude mit einer Höhe nach Absatz 3 Satz 2 von mehr als 22 Meter),
2.  bauliche Anlagen mit einer Höhe von mehr als 30 Meter,
3.  Gebäude mit mehr als 1 600 Quadratmeter Grundfläche des Geschosses mit der größten Ausdehnung, ausgenommen Wohngebäude und Garagen,
4.  Verkaufsstätten, deren Verkaufsräume und Ladenstraßen eine Grundfläche von insgesamt mehr als 800 Quadratmeter haben,
5.  Gebäude mit Räumen, die einer Büro- oder Verwaltungsnutzung dienen und einzeln eine Grundfläche von mehr als 400 Quadratmeter haben,
6.  Gebäude mit Räumen, die einzeln für die Nutzung durch mehr als 100 Personen bestimmt sind,
7.  Versammlungsstätten
    1.  mit Versammlungsräumen, die insgesamt mehr als 200 Besucher fassen, wenn diese Versammlungsräume gemeinsame Rettungswege haben,
    2.  im Freien mit Szenenflächen sowie Freisportanlagen jeweils mit Tribünen, die keine Fliegenden Bauten sind, und insgesamt mehr als 1 000 Besucher fassen,
8.  Schank- und Speisegaststätten mit mehr als 40 Gastplätzen in Gebäuden oder mehr als 1 000 Gastplätzen im Freien, Beherbergungsstätten mit mehr als zwölf Betten und Spielhallen mit mehr als 150 Quadratmeter Grundfläche,
9.  Gebäude mit Nutzungseinheiten zum Zwecke der Pflege oder Betreuung von Personen mit Pflegebedürftigkeit oder Behinderung, deren Selbstrettungsfähigkeit eingeschränkt ist, wenn die Nutzungseinheiten
    1.  einzeln für mehr als sechs Personen oder
    2.  für Personen mit Intensivpflegebedarf bestimmt sind, oder
    3.  einen gemeinsamen Rettungsweg haben und für insgesamt mehr als zwölf Personen bestimmt sind,
10.  Krankenhäuser,
11.  sonstige Einrichtungen zur Unterbringung von Personen sowie Wohnheime,
12.  Tageseinrichtungen für Kinder, Menschen mit Behinderung und alte Menschen, ausgenommen Tageseinrichtungen einschließlich Tagespflege für nicht mehr als zehn Kinder,
13.  Schulen, Hochschulen und ähnliche Einrichtungen,
14.  Justizvollzugsanstalten und bauliche Anlagen für den Maßregelvollzug,
15.  Camping- und Wochenendplätze,
16.  Freizeit- und Vergnügungsparks,
17.  Fliegende Bauten, soweit sie einer Ausführungsgenehmigung bedürfen,
18.  Regallager mit einer Oberkante Lagerguthöhe von mehr als 7,50 Meter,
19.  bauliche Anlagen, deren Nutzung durch Umgang oder Lagerung von Stoffen mit Explosions- oder erhöhter Brandgefahr verbunden ist,
20.  Anlagen und Räume, die in den Nummern 1 bis 19 nicht aufgeführt und deren Art oder Nutzung mit vergleichbaren Gefahren verbunden sind.

(5) Aufenthaltsräume sind Räume, die zum nicht nur vorübergehenden Aufenthalt von Menschen bestimmt oder geeignet sind.

(6) Geschosse sind oberirdische Geschosse, wenn ihre Deckenoberkanten im Mittel mehr als 1,40 Meter über die Geländeoberfläche hinausragen; im Übrigen sind sie Kellergeschosse.
Hohlräume zwischen der obersten Decke und der Bedachung, in denen Aufenthaltsräume nicht möglich sind, sind keine Geschosse.

(7) Stellplätze sind Flächen, die dem Abstellen von Kraftfahrzeugen außerhalb der öffentlichen Verkehrsflächen dienen.
Garagen sind Gebäude oder Gebäudeteile zum Abstellen von Kraftfahrzeugen.
Ausstellungs-, Verkaufs-, Werk- und Lagerräume für Kraftfahrzeuge sind keine Stellplätze oder Garagen.

(8) Feuerstätten sind in oder an Gebäuden ortsfest benutzte Anlagen oder Einrichtungen, die dazu bestimmt sind, durch Verbrennung Wärme zu erzeugen.

(9) Barrierefrei sind bauliche Anlagen, soweit sie für Menschen mit Behinderung in der allgemein üblichen Weise, ohne besondere Erschwernis und grundsätzlich ohne fremde Hilfe zugänglich und nutzbar sind.

(10) Bauprodukte sind

1.  Produkte, Baustoffe, Bauteile und Anlagen sowie Bausätze gemäß Artikel 2 Nummer 2 der Verordnung (EU) Nr. 305/2011 des Europäischen Parlaments und des Rates vom 9.
    März 2011 zur Festlegung harmonisierter Bedingungen für die Vermarktung von Bauprodukten und zur Aufhebung der Richtlinie 89/106/EWG des Rates (ABl. L 88 vom 4.4.2011, S. 5, L 103 vom 12.4.2013, S. 10), die zuletzt durch die Verordnung (EU) 574/2014 (ABl.
    L 159 vom 28.5.2014, S. 41) geändert worden ist, die hergestellt werden, um dauerhaft in bauliche Anlagen eingebaut zu werden,
2.  aus Produkten, Baustoffen, Bauteilen sowie Bausätzen gemäß Artikel 2 Nummer 2 der Verordnung (EU) Nr. 305/2011 vorgefertigte Anlagen, die hergestellt werden, um mit dem Erdboden verbunden zu werden

und deren Verwendung sich auf die Anforderungen nach § 3 Satz 1 auswirken kann.

(11) Bauart ist das Zusammenfügen von Bauprodukten zu baulichen Anlagen oder Teilen von baulichen Anlagen.

(12) Geländeoberfläche ist die natürliche Geländeoberfläche, soweit nicht gemäß § 9 Absatz 3 des Baugesetzbuchs oder in der Baugenehmigung eine andere Geländeoberfläche festgesetzt ist.

#### § 3 Allgemeine Anforderungen

Anlagen sind so anzuordnen, zu errichten, zu ändern und instand zu halten, dass die öffentliche Sicherheit und Ordnung, insbesondere Leben, Gesundheit und die natürlichen Lebensgrundlagen, nicht gefährdet werden; dabei sind die Grundanforderungen an Bauwerke gemäß Anhang I der Verordnung (EU) Nr. 305/2011 zu berücksichtigen.
Dies gilt auch für die Beseitigung von Anlagen und bei der Änderung ihrer Nutzung.

Teil 2  
Das Grundstück und seine Bebauung
------------------------------------------

#### § 4 Bebauung der Grundstücke mit Gebäuden

(1) Gebäude dürfen nur errichtet werden, wenn das Grundstück in angemessener Breite an einer befahrbaren öffentlichen Verkehrsfläche liegt oder wenn das Grundstück eine befahrbare, öffentlich-rechtlich gesicherte Zufahrt zu einer befahrbaren öffentlichen Verkehrsfläche hat.

(2) Ein Gebäude auf mehreren Grundstücken ist nur zulässig, wenn öffentlich-rechtlich gesichert ist, dass dadurch keine Verhältnisse eintreten können, die Vorschriften dieses Gesetzes oder aufgrund dieses Gesetzes widersprechen.
Satz 1 gilt bei bestehenden Gebäuden nicht für eine Außenwand- und Dachdämmung, die über die Bauteilanforderungen der Energieeinsparverordnung vom 24.
Juli 2007 (BGBl. I S. 1519), die zuletzt durch Artikel 3 der Verordnung vom 24. Oktober 2015 (BGBl. I S. 1789, 1790) geändert worden ist, in der jeweils geltenden Fassung für bestehende Gebäude nicht hinausgeht.
Satz 2 gilt entsprechend für die mit der Wärmedämmung zusammenhängenden notwendigen Änderungen von Bauteilen.

#### § 5 Zugänge und Zufahrten auf den Grundstücken

(1) Von öffentlichen Verkehrsflächen ist insbesondere für die Feuerwehr ein geradliniger Zu- oder Durchgang zu rückwärtigen Gebäuden zu schaffen; zu anderen Gebäuden ist er zu schaffen, wenn der zweite Rettungsweg dieser Gebäude über Rettungsgeräte der Feuerwehr führt.
Zu Gebäuden, bei denen die Oberkante der Brüstung von zum Anleitern bestimmten Fenstern oder Stellen mehr als 8 Meter über Gelände liegt, ist in den Fällen des Satzes 1 anstelle eines Zu- oder Durchgangs eine Zu- oder Durchfahrt zu schaffen.
Ist für die Personenrettung der Einsatz von Hubrettungsfahrzeugen erforderlich, sind die dafür erforderlichen Aufstell- und Bewegungsflächen vorzusehen.
Bei Gebäuden, die ganz oder mit Teilen mehr als 50 Meter von einer öffentlichen Verkehrsfläche entfernt sind, sind Zufahrten oder Durchfahrten nach Satz 2 zu den vor und hinter den Gebäuden gelegenen Grundstücksteilen und Bewegungsflächen herzustellen, wenn sie aus Gründen des Feuerwehreinsatzes erforderlich sind.

(2) Zu- und Durchfahrten, Aufstellflächen und Bewegungsflächen müssen für Feuerwehrfahrzeuge ausreichend befestigt und tragfähig sein; sie sind als solche zu kennzeichnen und ständig freizuhalten; die Kennzeichnung von Zufahrten muss von der öffentlichen Verkehrsfläche aus sichtbar sein.
Fahrzeuge dürfen auf den Flächen nach Satz 1 nicht abgestellt werden.

#### § 6 Abstandsflächen, Abstände

(1) Vor den Außenwänden von Gebäuden sind Abstandsflächen von oberirdischen Gebäuden freizuhalten.
Satz 1 gilt entsprechend für andere Anlagen, von denen Wirkungen wie von Gebäuden ausgehen, gegenüber Gebäuden und Grundstücksgrenzen.
Eine Abstandsfläche ist nicht erforderlich vor Außenwänden, die an Grundstücksgrenzen errichtet werden, wenn nach planungsrechtlichen Vorschriften an die Grenze gebaut werden muss oder gebaut werden darf.

(2) Abstandsflächen sowie Abstände nach § 30 Absatz 2 Nummer 1 und § 32 Absatz 2 müssen auf dem Grundstück selbst liegen.
Sie dürfen auch auf öffentlichen Verkehrs-, Grün- und Wasserflächen liegen, jedoch nur bis zu deren Mitte.
Abstandsflächen sowie Abstände im Sinne des Satzes 1 dürfen sich ganz oder teilweise auf andere Grundstücke erstrecken, wenn öffentlich-rechtlich gesichert ist, dass sie nicht überbaut werden; Abstandsflächen dürfen auf die auf diesen Grundstücken erforderlichen Abstandsflächen nicht angerechnet werden.

(3) Die Abstandsflächen dürfen sich nicht überdecken; dies gilt nicht für

1.  Außenwände, die in einem Winkel von mehr als 75 Grad zueinander stehen,
2.  Außenwände zu einem fremder Sicht entzogenen Gartenhof bei Wohngebäuden der Gebäudeklassen 1 und 2,
3.  Gebäude und andere bauliche Anlagen, die in den Abstandsflächen zulässig sind.

(4) Die Tiefe der Abstandsfläche bemisst sich nach der Wandhöhe; sie wird senkrecht zur Wand gemessen.
Wandhöhe ist das Maß von der Geländeoberfläche bis zum Schnittpunkt der Wand mit der Dachhaut oder bis zum oberen Abschluss der Wand.
Bei gestaffelten Wänden, bei Dächern oder Dachaufbauten sowie bei gegenüber der Außenwand vor- oder zurücktretenden Bauteilen oder Vorbauten ist die Wandhöhe für den jeweiligen Wandabschnitt, Dachaufbau, Vorbau oder das jeweilige Bauteil gesondert zu ermitteln.
Das sich ergebende Maß ist H.

(5) Die Tiefe der Abstandsflächen beträgt 0,4 H, mindestens 3 Meter.
In Gewerbe- und Industriegebieten genügt eine Tiefe von 0,2 H, mindestens 3 Meter.
Vor den Außenwänden von Gebäuden der Gebäudeklassen 1 und 2 mit nicht mehr als drei oberirdischen Geschossen genügt als Tiefe der Abstandsfläche 3 Meter.
Werden von einer städtebaulichen Satzung oder einer örtlichen Bauvorschrift nach § 87 Außenwände zugelassen oder vorgeschrieben, vor denen Abstandsflächen größerer oder geringerer Tiefe als nach den Sätzen 1 bis 3 liegen müssten, finden die Sätze 1 bis 3 keine Anwendung, es sei denn, die Satzung ordnet die Geltung dieser Vorschriften an.

(6) Bei der Bemessung der Abstandsflächen bleiben außer Betracht

1.  vor die Außenwand vortretende Bauteile wie Gesimse und Dachüberstände, wenn sie
    1.  nicht mehr als 1 Meter vortreten und
    2.  mindestens 2 Meter von der gegenüberliegenden Nachbargrenze entfernt bleiben,
2.  Wintergärten mit nicht mehr als 5 Meter Breite, wenn sie
    1.  insgesamt nicht mehr als ein Drittel der Breite der jeweiligen Außenwand in Anspruch nehmen,
    2.  über nicht mehr als zwei Geschosse reichen und nicht mehr als 3 Meter vortreten und
    3.  mindestens 2 Meter von der gegenüberliegenden Nachbargrenze entfernt bleiben,
3.  Balkone mit nicht mehr als 5 Meter Breite, wenn sie
    1.  insgesamt nicht mehr als ein Drittel der Breite der jeweiligen Außenwand in Anspruch nehmen,
    2.  nicht mehr als 2 Meter vortreten und
    3.  mindestens 2 Meter von der gegenüberliegenden Nachbargrenze entfernt bleiben,
4.  sonstige Vorbauten, wenn sie
    1.  insgesamt nicht mehr als ein Drittel der Breite der jeweiligen Außenwand in Anspruch nehmen,
    2.  nicht mehr als 1,50 Meter vor diese Außenwand vortreten und
    3.  mindestens 2 Meter von der gegenüberliegenden Nachbargrenze entfernt bleiben,
5.  bei Gebäuden an der Grundstücksgrenze die Seitenwände von Vorbauten und Dachaufbauten, auch wenn sie nicht an der Grundstücksgrenze errichtet werden.

(7) Bei der Bemessung der Abstandsflächen bleiben Maßnahmen zum Zwecke der Energieeinsparung und Solaranlagen an bestehenden Gebäuden unabhängig davon, ob diese den Anforderungen der Absätze 2 bis 6 entsprechen, außer Betracht, wenn sie

1.  eine Stärke von nicht mehr als 0,30 Meter aufweisen und
2.  mindestens 2,50 Meter von der Nachbargrenze zurückbleiben.

(8) In den Abstandsflächen eines Gebäudes sowie ohne eigene Abstandsflächen sind, auch wenn sie nicht an die Grundstücksgrenze oder an das Gebäude angebaut werden, zulässig

1.  Garagen und Gebäude ohne Aufenthaltsräume und Feuerstätten mit einer mittleren Wandhöhe der grenznahen Wand bis zu 3 Meter und einer Gesamtgebäudelänge je Grundstücksgrenze von 9 Meter; die Dachneigung darf 45 Grad nicht überschreiten,
2.  gebäudeunabhängige Solaranlagen mit einer Höhe bis zu 3 Meter und einer Gesamtlänge je Grundstücksgrenze von 9 Meter,
3.  Stützmauern und geschlossene Einfriedungen in Gewerbe- und Industriegebieten, außerhalb dieser Baugebiete mit einer Höhe bis zu 2 Meter.

Die Länge der die Abstandsflächentiefe gegenüber den Grundstücksgrenzen nicht einhaltenden Anlagen nach Satz 1 Nummer 1 und 2 darf auf einem Grundstück insgesamt 15 Meter nicht überschreiten.

(9) An bestehenden Gebäuden sind bei der nachträglichen Errichtung vor die Außenwand vortretender Aufzüge, Treppen und Treppenräume geringere Tiefen von Abstandsflächen zulässig, wenn wesentliche Beeinträchtigungen angrenzender oder gegenüberliegender Räume nicht zu befürchten sind und zu Nachbargrenzen ein Abstand von mindestens 3 Meter eingehalten wird.

(10) Bei rechtmäßig errichteten Gebäuden sind die sich ergebenden Abstandsflächen in folgenden Fällen unbeachtlich:

1.  Änderungen innerhalb des Gebäudes,
2.  Nutzungsänderungen, wenn der Abstand des Gebäudes zu den Nachbargrenzen mindestens 2,50 Meter beträgt oder die Außenwand als Gebäudeabschlusswand ausgebildet ist,
3.  die Neuerrichtung von Dachräumen oder Dachgeschossen innerhalb der Abmessungen bestehender Dachräume oder Dachgeschosse,
4.  die Errichtung und Änderung von Vor- und Anbauten, die für sich genommen die Tiefe der Abstandsflächen nach Absatz 5 einhalten,
5.  die nachträgliche Errichtung von Dach- und Staffelgeschossen, wenn deren Abstandsflächen innerhalb der Abstandsflächen des bestehenden Gebäudes liegen.

Satz 1 gilt nicht für Gebäude nach Absatz 8.

(11) Eine Abweichung von den Abstandsflächen und Abständen kann nach § 67 zugelassen werden, wenn deren Schutzziele berücksichtigt werden.
Eine atypische Grundstückssituation ist nicht erforderlich.

#### § 7 Teilung von Grundstücken

Durch die Teilung eines Grundstücks, das bebaut oder dessen Bebauung genehmigt ist, dürfen keine Verhältnisse geschaffen werden, die Vorschriften dieses Gesetzes oder aufgrund dieses Gesetzes widersprechen.
Entspricht die Teilung eines Grundstücks, das bebaut oder dessen Bebauung genehmigt ist, nicht den Anforderungen des Satzes 1 oder des § 19 Absatz 2 des Baugesetzbuchs, so darf eine die Teilung vorbereitende Liegenschaftsvermessung nur vorgenommen werden, wenn die erforderliche Abweichung nach § 67 zugelassen oder die erforderliche Befreiung erteilt ist.

#### § 8 Nicht überbaute Flächen der bebauten Grundstücke, Kinderspielplätze

(1) Die nicht mit Gebäuden oder vergleichbaren baulichen Anlagen überbauten Flächen der bebauten Grundstücke sind

1.  wasseraufnahmefähig zu belassen oder herzustellen und
2.  zu begrünen oder zu bepflanzen,

soweit dem nicht die Erfordernisse einer anderen zulässigen Verwendung der Flächen entgegenstehen.
Satz 1 findet keine Anwendung, soweit Bebauungspläne oder andere Satzungen Festsetzungen zu den nicht überbauten Flächen treffen.

(2) Bei der Errichtung von Gebäuden mit mehr als drei Wohnungen müssen die durch die Gemeinde in einer örtlichen Bauvorschrift nach § 87 festgesetzten Kinderspielplätze auf dem Baugrundstück oder in unmittelbarer Nähe auf einem anderen geeigneten Grundstück, dessen dauerhafte Nutzung für diesen Zweck öffentlich-rechtlich gesichert sein muss, hergestellt werden.

(3) Soweit die Bauherrin oder der Bauherr durch örtliche Bauvorschrift zur Herstellung von Kinderspielplätzen verpflichtet ist, kann die Gemeinde durch öffentlich-rechtlichen Vertrag mit der Bauherrin oder dem Bauherrn vereinbaren, dass die Bauherrin oder der Bauherr ihre oder seine Verpflichtung ganz oder teilweise durch Zahlung eines Geldbetrages an die Gemeinde ablöst (Kinderspielplatzablösevertrag).
Der Anspruch der Gemeinde auf Zahlung des im Kinderspielplatzablösevertrages vereinbarten Geldbetrages entsteht mit Baubeginn.

(4) Die Gemeinde hat den Geldbetrag für die Ablösung von Kinderspielplätzen für die Herstellung zusätzlicher oder die Instandhaltung, die Instandsetzung oder die Modernisierung bestehender Kinderspielplätze zu verwenden.

Teil 3  
Bauliche Anlagen
-------------------------

### Abschnitt 1  
Gestaltung

#### § 9 Gestaltung

Bauliche Anlagen müssen nach Form, Maßstab, Verhältnis der Baumassen und Bauteile zueinander, Werkstoff und Farbe so gestaltet sein, dass sie nicht verunstaltet wirken.
Bauliche Anlagen dürfen das Straßen-, Orts- und Landschaftsbild nicht verunstalten.

#### § 10 Anlagen der Außenwerbung

(1) Anlagen der Außenwerbung (Werbeanlagen) sind alle ortsfesten Einrichtungen, die der Ankündigung oder Anpreisung oder als Hinweis auf Gewerbe oder Beruf dienen und vom öffentlichen Verkehrsraum aus sichtbar sind.
Hierzu zählen insbesondere Schilder, Beschriftungen, Bemalungen, Lichtwerbungen, Schaukästen sowie für Zettelanschläge und Bogenanschläge oder Lichtwerbung bestimmte Säulen, Tafeln und Flächen.

(2) Für Werbeanlagen, die bauliche Anlagen sind, gelten die in diesem Gesetz an bauliche Anlagen gestellten Anforderungen.
Werbeanlagen, die keine baulichen Anlagen sind, dürfen weder bauliche Anlagen noch das Straßen-, Orts- und Landschaftsbild verunstalten oder die Sicherheit und Leichtigkeit des Verkehrs gefährden.
Die störende Häufung von Werbeanlagen ist unzulässig.
Die besonderen Belange von Menschen mit Behinderung sind angemessen zu berücksichtigen.

### Abschnitt 2  
Allgemeine Anforderungen an die Bauausführung

#### § 11 Baustelle

(1) Baustellen sind so einzurichten, dass bauliche Anlagen ordnungsgemäß errichtet, geändert, instand gehalten oder beseitigt werden können und Gefahren oder vermeidbare Belästigungen nicht entstehen.

(2) Bei Bauarbeiten, durch die unbeteiligte Personen gefährdet werden können, ist die Gefahrenzone abzugrenzen oder durch Warnzeichen zu kennzeichnen.
Soweit erforderlich, sind Baustellen mit einem Bauzaun abzugrenzen, mit Schutzvorrichtungen gegen herabfallende Gegenstände zu versehen und zu beleuchten.

(3) Bei der Ausführung nicht genehmigungsfreier Bauvorhaben hat die Bauherrin oder der Bauherr an der Baustelle ein Schild, das die Bezeichnung des Bauvorhabens sowie die Namen und Anschriften der am Bau Beteiligten (§§ 53 bis 56) enthalten muss, dauerhaft und von der öffentlichen Verkehrsfläche aus sichtbar anzubringen.

(4) Bäume, Hecken und sonstige Bepflanzungen, die aufgrund anderer Rechtsvorschriften zu erhalten sind, müssen während der Bauausführung geschützt werden.

#### § 12 Standsicherheit

(1) Jede bauliche Anlage muss im Ganzen und in ihren einzelnen Teilen für sich allein standsicher sein.
Die Standsicherheit anderer baulicher Anlagen und die Tragfähigkeit des Baugrundes der Nachbargrundstücke dürfen nicht gefährdet werden.

(2) Die Verwendung gemeinsamer Bauteile für mehrere bauliche Anlagen ist zulässig, wenn öffentlich-rechtlich gesichert ist, dass die gemeinsamen Bauteile bei der Beseitigung einer der baulichen Anlagen bestehen bleiben können.

#### § 13 Schutz gegen schädliche Einflüsse

Bauliche Anlagen müssen so angeordnet, beschaffen und gebrauchstauglich sein, dass durch Wasser, Feuchtigkeit, pflanzliche und tierische Schädlinge sowie andere chemische, physikalische oder biologische Einflüsse Gefahren oder unzumutbare Belästigungen nicht entstehen.
Baugrundstücke müssen für bauliche Anlagen geeignet sein.

#### § 14 Brandschutz

Bauliche Anlagen sind so anzuordnen, zu errichten, zu ändern und instand zu halten, dass der Entstehung eines Brandes und der Ausbreitung von Feuer und Rauch (Brandausbreitung) vorgebeugt wird und bei einem Brand die Rettung von Menschen und Tieren sowie eine Entrauchung von Räumen und wirksame Löscharbeiten möglich sind.

#### § 15 Wärme-, Schall-, Erschütterungsschutz

(1) Gebäude müssen einen ihrer Nutzung und den klimatischen Verhältnissen entsprechenden Wärmeschutz haben.

(2) Gebäude müssen einen ihrer Nutzung entsprechenden Schallschutz haben.
Geräusche, die von ortsfesten Einrichtungen in baulichen Anlagen oder auf Baugrundstücken ausgehen, sind so zu dämmen, dass Gefahren oder unzumutbare Belästigungen nicht entstehen.

(3) Erschütterungen oder Schwingungen, die von ortsfesten Einrichtungen in baulichen Anlagen oder auf Baugrundstücken ausgehen, sind so zu dämmen, dass Gefahren oder unzumutbare Belästigungen nicht entstehen.

(4) Die Einhaltung der Anforderungen an Gebäude nach den Vorschriften der Energieeinsparung sowie zur Nutzung erneuerbarer Energien ist nach näherer Maßgabe der Verordnung aufgrund § 86 Absatz 3 Satz 1 Nummer 6 nachzuweisen.

#### § 16 Verkehrssicherheit

(1) Bauliche Anlagen und die dem Verkehr dienenden nicht überbauten Flächen von bebauten Grundstücken müssen verkehrssicher sein.

(2) Die Sicherheit und Leichtigkeit des öffentlichen Verkehrs darf durch bauliche Anlagen oder deren Nutzung nicht gefährdet werden.

#### § 16a Bauarten

(1) Bauarten dürfen nur angewendet werden, wenn bei ihrer Anwendung die baulichen Anlagen bei ordnungsgemäßer Instandhaltung während einer dem Zweck entsprechenden angemessenen Zeitdauer die Anforderungen dieses Gesetzes oder aufgrund dieses Gesetzes erfüllen und für ihren Anwendungszweck tauglich sind.

(2) Bauarten, die von Technischen Baubestimmungen nach § 86a Absatz 2 Nummer 2 oder Nummer 3 Buchstabe a wesentlich abweichen oder für die es allgemein anerkannte Regeln der Technik nicht gibt, dürfen bei der Errichtung, Änderung und Instandhaltung baulicher Anlagen nur angewendet werden, wenn für sie

1.  eine allgemeine Bauartgenehmigung durch das Deutsche Institut für Bautechnik oder
2.  eine vorhabenbezogene Bauartgenehmigung durch die oberste Bauaufsichtsbehörde

erteilt worden ist.
§ 18 Absatz 2 bis 7 gilt entsprechend.

(3) Anstelle einer allgemeinen Bauartgenehmigung genügt ein allgemeines bauaufsichtliches Prüfzeugnis für Bauarten, wenn die Bauart nach allgemein anerkannten Prüfverfahren beurteilt werden kann.
In der Verwaltungsvorschrift nach § 86a werden diese Bauarten mit der Angabe der maßgebenden technischen Regeln bekannt gemacht.
§ 19 Absatz 2 gilt entsprechend.

(4) Wenn Gefahren im Sinne des § 3 Satz 1 nicht zu erwarten sind, kann die oberste Bauaufsichtsbehörde im Einzelfall oder für genau begrenzte Fälle allgemein festlegen, dass eine Bauartgenehmigung nicht erforderlich ist.

(5) Bauarten bedürfen einer Bestätigung ihrer Übereinstimmung mit den Technischen Baubestimmungen nach § 86a Absatz 2, den allgemeinen Bauartgenehmigungen, den allgemeinen bauaufsichtlichen Prüfzeugnissen für Bauarten oder den vorhabenbezogenen Bauartgenehmigungen; als Übereinstimmung gilt auch eine Abweichung, die nicht wesentlich ist.
§ 21 Absatz 2 gilt für die Anwenderin oder den Anwender der Bauart entsprechend.

(6) Bei Bauarten, deren Anwendung in außergewöhnlichem Maß von der Sachkunde und Erfahrung der damit betrauten Personen oder von einer Ausstattung mit besonderen Vorrichtungen abhängt, kann in der Bauartgenehmigung oder durch Rechtsverordnung der obersten Bauaufsichtsbehörde vorgeschrieben werden, dass die Anwenderin oder der Anwender über solche Fachkräfte und Vorrichtungen verfügt und den Nachweis hierüber gegenüber einer Prüfstelle nach § 24 Satz 1 Nummer 6 zu erbringen hat.
In der Rechtsverordnung können Mindestanforderungen an die Ausbildung, die durch Prüfung nachzuweisende Befähigung und die Ausbildungsstätten einschließlich der Anerkennungsvoraussetzungen gestellt werden.

(7) Für Bauarten, die einer außergewöhnlichen Sorgfalt bei Ausführung oder Instandhaltung bedürfen, kann in der Bauartgenehmigung oder durch Rechtsverordnung der obersten Bauaufsichtsbehörde die Überwachung dieser Tätigkeiten durch eine Überwachungsstelle nach § 24 Satz 1 Nummer 5 vorgeschrieben werden.

### Abschnitt 3  
Bauprodukte

#### § 16b Allgemeine Anforderungen für die Verwendung von Bauprodukten

(1) Bauprodukte dürfen nur verwendet werden, wenn bei ihrer Verwendung die baulichen Anlagen bei ordnungsgemäßer Instandhaltung während einer dem Zweck entsprechenden angemessenen Zeitdauer die Anforderungen dieses Gesetzes oder aufgrund dieses Gesetzes erfüllen und gebrauchstauglich sind.

(2) Bauprodukte, die in Vorschriften der Mitgliedstaaten der Europäischen Union und anderer Vertragsstaaten des Abkommens über den europäischen Wirtschaftsraum genannten technischen Anforderungen entsprechen, dürfen verwendet werden, wenn das geforderte Schutzniveau gemäß § 3 Satz 1 gleichermaßen dauerhaft erreicht wird.

#### § 16c Anforderungen für die Verwendung von CE-gekennzeichneten Bauprodukten

Ein Bauprodukt, das die CE-Kennzeichnung trägt, darf verwendet werden, wenn die erklärten Leistungen den in diesem Gesetz oder aufgrund dieses Gesetzes festgelegten Anforderungen für diese Verwendung entsprechen.
Die §§ 17 bis 25 Absatz 1 gelten nicht für Bauprodukte, die die CE-Kennzeichnung aufgrund der Verordnung (EU) Nr. 305/2011 tragen.

#### § 17 Verwendbarkeitsnachweise

(1) Ein Verwendbarkeitsnachweis nach den §§ 18 bis 20 ist für ein Bauprodukt erforderlich, wenn

1.  es für dieses keine Technische Baubestimmung und keine allgemein anerkannte Regel der Technik gibt,
2.  das Bauprodukt von einer Technischen Baubestimmung nach § 86a Absatz 2 Nummer 3 wesentlich abweicht oder
3.  eine Verordnung nach § 86 Absatz 4a es vorsieht.

(2) Ein Verwendbarkeitsnachweis ist nicht erforderlich für ein Bauprodukt,

1.  das von einer allgemein anerkannten Regel der Technik abweicht oder
2.  das für die Erfüllung der Anforderungen dieses Gesetzes oder aufgrund dieses Gesetzes nur eine untergeordnete Bedeutung hat.

(3) Die Technischen Baubestimmungen nach § 86a enthalten eine nicht abschließende Liste von Bauprodukten, die keines Verwendbarkeitsnachweises nach Absatz 1 bedürfen.

#### § 18 Allgemeine bauaufsichtliche Zulassung

(1) Das Deutsche Institut für Bautechnik erteilt in den Fällen des § 17 Absatz 1 eine allgemeine bauaufsichtliche Zulassung für Bauprodukte, wenn deren Verwendbarkeit im Sinne des § 16b Absatz 1 nachgewiesen ist.

(2) Die zur Begründung des Antrags erforderlichen Unterlagen sind beizufügen.
Soweit erforderlich, sind Probestücke vom Antragsteller zur Verfügung zu stellen oder durch Sachverständige, die das Deutsche Institut für Bautechnik bestimmen kann, zu entnehmen oder Probeausführungen unter Aufsicht der Sachverständigen herzustellen.
§ 69 Absatz 2 gilt entsprechend.

(3) Das Deutsche Institut für Bautechnik kann für die Durchführung der Prüfung die sachverständige Stelle und für Probeausführungen die Ausführungsstelle und die Ausführungszeit vorschreiben.

(4) Die allgemeine bauaufsichtliche Zulassung wird widerruflich und für eine bestimmte Frist erteilt, die in der Regel fünf Jahre beträgt.
Die Zulassung kann mit Nebenbestimmungen erteilt werden.
Sie kann auf Antrag in Textform in der Regel um fünf Jahre verlängert werden.
Sie kann auch rückwirkend verlängert werden, wenn der Antrag vor Fristablauf beim Deutschen Institut für Bautechnik eingegangen ist.

(5) Die Zulassung wird unbeschadet der privaten Rechte Dritter erteilt.

(6) Das Deutsche Institut für Bautechnik macht die von ihm erteilten allgemeinen bauaufsichtlichen Zulassungen nach Gegenstand und wesentlichem Inhalt öffentlich bekannt.

(7) Allgemeine bauaufsichtliche Zulassungen nach dem Recht anderer Länder der Bundesrepublik Deutschland gelten auch im Land Brandenburg.

#### § 19 Allgemeines bauaufsichtliches Prüfzeugnis

(1) Bauprodukte, die nach allgemein anerkannten Prüfverfahren beurteilt werden, bedürfen anstelle einer allgemeinen bauaufsichtlichen Zulassung nur eines allgemeinen bauaufsichtlichen Prüfzeugnisses.
Dies wird mit der Angabe der maßgebenden technischen Regeln in den Technischen Baubestimmungen nach § 86a bekannt gemacht.

(2) Ein allgemeines bauaufsichtliches Prüfzeugnis wird von einer Prüfstelle nach § 24 Satz 1 Nummer 1 für Bauprodukte nach Absatz 1 erteilt, wenn deren Verwendbarkeit im Sinne des § 16b Absatz 1 nachgewiesen ist.
§ 18 Absatz 2, 4 bis 7 gilt entsprechend.
Die Anerkennungsbehörde für Stellen nach § 24 Satz 1 Nummer 1, § 86 Absatz 4 Nummer 2 kann allgemeine bauaufsichtliche Prüfzeugnisse zurücknehmen oder widerrufen; die §§ 48 und 49 des Verwaltungsverfahrensgesetzes in Verbindung mit § 1 Absatz 1 Satz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg finden Anwendung.

#### § 20 Nachweis der Verwendbarkeit von Bauprodukten im Einzelfall

Mit Zustimmung der obersten Bauaufsichtsbehörde dürfen in den Fällen des § 17 Absatz 1 im Einzelfall Bauprodukte verwendet werden, wenn ihre Verwendbarkeit im Sinne des § 16b Absatz 1 nachgewiesen ist.
Wenn Gefahren im Sinne des § 3 Satz 1 nicht zu erwarten sind, kann die oberste Bauaufsichtsbehörde im Einzelfall erklären, dass ihre Zustimmung nicht erforderlich ist.

#### § 21 Übereinstimmungsbestätigung

(1) Bauprodukte bedürfen einer Bestätigung ihrer Übereinstimmung mit den Technischen Baubestimmungen nach § 86a Absatz 2, den allgemeinen bauaufsichtlichen Zulassungen, den allgemeinen bauaufsichtlichen Prüfzeugnissen oder den Zustimmungen im Einzelfall; als Übereinstimmung gilt auch eine Abweichung, die nicht wesentlich ist.

(2) Die Bestätigung der Übereinstimmung erfolgt durch Übereinstimmungserklärung der Herstellerin oder des Herstellers (§ 22).

(3) Die Übereinstimmungserklärung hat die Herstellerin oder der Hersteller durch Kennzeichnung der Bauprodukte mit dem Übereinstimmungszeichen (Ü-Zeichen) unter Hinweis auf den Verwendungszweck abzugeben.

(4) Das Ü-Zeichen ist auf dem Bauprodukt, auf einem Beipackzettel oder auf seiner Verpackung oder, wenn dies Schwierigkeiten bereitet, auf dem Lieferschein oder auf einer Anlage zum Lieferschein anzubringen.

(5) Ü-Zeichen aus anderen Ländern der Bundesrepublik Deutschland, aus anderen Mitgliedstaaten der Europäischen Union und anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum gelten auch im Land Brandenburg.

#### § 22 Übereinstimmungserklärung der Herstellerin oder des Herstellers

(1) Die Herstellerin oder der Hersteller darf eine Übereinstimmungserklärung nur abgeben, wenn sie oder er durch werkseigene Produktionskontrolle sichergestellt hat, dass das von ihr oder ihm hergestellte Bauprodukt den maßgebenden technischen Regeln, der allgemeinen bauaufsichtlichen Zulassung, dem allgemeinen bauaufsichtlichen Prüfzeugnis oder der Zustimmung im Einzelfall entspricht.

(2) In den Technischen Baubestimmungen nach § 86a, in den allgemeinen bauaufsichtlichen Zulassungen, in den allgemeinen bauaufsichtlichen Prüfzeugnissen oder in den Zustimmungen im Einzelfall kann eine Prüfung der Bauprodukte durch eine Prüfstelle nach § 24 Satz 1 Nummer 2 vor Abgabe der Übereinstimmungserklärung vorgeschrieben werden, wenn dies zur Sicherung einer ordnungsgemäßen Herstellung erforderlich ist.
In diesen Fällen hat die Prüfstelle das Bauprodukt daraufhin zu überprüfen, ob es den Technischen Baubestimmungen nach § 86a Absatz 2, der allgemeinen bauaufsichtlichen Zulassung, dem allgemeinen bauaufsichtlichen Prüfzeugnis oder der Zustimmung im Einzelfall entspricht.

(3) In den Technischen Baubestimmungen nach § 86a, in den allgemeinen bauaufsichtlichen Zulassungen oder in den Zustimmungen im Einzelfall kann eine Zertifizierung vor Abgabe der Übereinstimmungserklärung vorgeschrieben werden, wenn dies zum Nachweis einer ordnungsgemäßen Herstellung eines Bauproduktes erforderlich ist.
Die oberste Bauaufsichtsbehörde kann im Einzelfall die Verwendung von Bauprodukten ohne Zertifizierung gestatten, wenn nachgewiesen ist, dass diese Bauprodukte den technischen Regeln, Zulassungen, Prüfzeugnissen oder Zustimmungen im Einzelfall entsprechen.

(4) Bauprodukte, die nicht in Serie hergestellt werden, bedürfen nur einer Übereinstimmungserklärung nach Absatz 1, sofern nichts anderes bestimmt ist.

#### § 23 Zertifizierung

(1) Der Herstellerin oder dem Hersteller ist ein Übereinstimmungszertifikat von einer Zertifizierungsstelle nach § 24 Satz 1 Nummer 3 zu erteilen, wenn das Bauprodukt

1.  den Technischen Baubestimmungen nach § 86a Absatz 2, der allgemeinen bauaufsichtlichen Zulassung, dem allgemeinen bauaufsichtlichen Prüfzeugnis oder der Zustimmung im Einzelfall entspricht und
2.  einer werkseigenen Produktionskontrolle sowie einer Fremdüberwachung nach Maßgabe des Absatzes 2 unterliegt.

(2) Die Fremdüberwachung ist von Überwachungsstellen nach § 24 Satz 1 Nummer 4 durchzuführen.
Die Fremdüberwachung hat regelmäßig zu überprüfen, ob das Bauprodukt den Technischen Baubestimmungen nach § 86a Absatz 2, der allgemeinen bauaufsichtlichen Zulassung, dem allgemeinen bauaufsichtlichen Prüfzeugnis oder Zustimmung im Einzelfall entspricht.

#### § 24 Prüf-, Zertifizierungs-, Überwachungsstellen

Das Deutsche Institut für Bautechnik kann im Einvernehmen mit der obersten Bauaufsichtsbehörde eine natürliche oder juristische Person als

1.  Prüfstelle für die Erteilung allgemeiner bauaufsichtlicher Prüfzeugnisse (§ 19),
2.  Prüfstelle für die Überprüfung von Bauprodukten vor Bestätigung der Übereinstimmung (§ 22 Absatz 2),
3.  Zertifizierungsstelle (§ 23 Absatz 1),
4.  Überwachungsstelle für die Fremdüberwachung (§ 23 Absatz 2),
5.  Überwachungsstelle für die Überwachung nach § 16a Absatz 7 und § 25 Absatz 2 oder
6.  Prüfstelle für die Überprüfung nach § 16a Absatz 6 Satz 1 und § 25 Absatz 1 Satz 1

anerkennen, wenn sie oder die bei ihr Beschäftigten nach ihrer Ausbildung, Fachkenntnis, persönlichen Zuverlässigkeit, ihrer Unparteilichkeit und ihren Leistungen die Gewähr dafür bieten, dass diese Aufgaben den öffentlich-rechtlichen Vorschriften entsprechend wahrgenommen werden, und wenn sie über die erforderlichen Vorrichtungen verfügen.
Satz 1 ist entsprechend auf Behörden anzuwenden, wenn sie ausreichend mit geeigneten Fachkräften besetzt und mit den erforderlichen Vorrichtungen ausgestattet sind.
Die Anerkennung von Prüf-, Zertifizierungs- und Überwachungsstellen anderer Länder der Bundesrepublik Deutschland gilt auch im Land Brandenburg.

#### § 25 Besondere Sachkunde- und Sorgfaltsanforderungen

(1) Bei Bauprodukten, deren Herstellung in außergewöhnlichem Maß von der Sachkunde und Erfahrung der damit betrauten Personen oder von einer Ausstattung mit besonderen Vorrichtungen abhängt, kann in der allgemeinen bauaufsichtlichen Zulassung, in der Zustimmung im Einzelfall oder durch Rechtsverordnung der obersten Bauaufsichtsbehörde vorgeschrieben werden, dass die Herstellerin oder der Hersteller über solche Fachkräfte und Vorrichtungen verfügt und den Nachweis hierüber gegenüber einer Prüfstelle nach § 24 Satz 1 Nummer 6 zu erbringen hat.
In der Rechtsverordnung können Mindestanforderungen an die Ausbildung, die durch Prüfung nachzuweisende Befähigung und die Ausbildungsstätten einschließlich der Anerkennungsvoraussetzungen gestellt werden.

(2) Für Bauprodukte, die wegen ihrer besonderen Eigenschaften oder ihres besonderen Verwendungszwecks einer außergewöhnlichen Sorgfalt bei Einbau, Transport, Instandhaltung oder Reinigung bedürfen, kann in der allgemeinen bauaufsichtlichen Zulassung, in der Zustimmung im Einzelfall oder durch Rechtsverordnung der obersten Bauaufsichtsbehörde die Überwachung dieser Tätigkeiten durch eine Überwachungsstelle nach § 24 Satz 1 Nummer 5 vorgeschrieben werden, soweit diese Tätigkeiten nicht bereits durch die Verordnung (EU) Nr. 305/2011 erfasst sind.

### Abschnitt 4  
Brandverhalten von Baustoffen und Bauteilen; Wände, Decken, Dächer

#### § 26 Allgemeine Anforderungen an das Brandverhalten von Baustoffen und Bauteilen

(1) Baustoffe werden nach den Anforderungen an ihr Brandverhalten unterschieden in

1.  nichtbrennbare,
2.  schwerentflammbare,
3.  normalentflammbare.

Baustoffe, die nicht mindestens normalentflammbar sind (leichtentflammbare Baustoffe), dürfen nicht verwendet werden; dies gilt nicht, wenn sie in Verbindung mit anderen Baustoffen nicht leichtentflammbar sind.

(2) Bauteile werden nach den Anforderungen an ihre Feuerwiderstandsfähigkeit unterschieden in

1.  feuerbeständige,
2.  hochfeuerhemmende,
3.  feuerhemmende;

die Feuerwiderstandsfähigkeit bezieht sich bei tragenden und aussteifenden Bauteilen auf deren Standsicherheit im Brandfall, bei raumabschließenden Bauteilen auf deren Widerstand gegen die Brandausbreitung.
Bauteile werden zusätzlich nach dem Brandverhalten ihrer Baustoffe unterschieden in

1.  Bauteile aus nichtbrennbaren Baustoffen,
2.  Bauteile, deren tragende und aussteifende Teile aus nichtbrennbaren Baustoffen bestehen und die bei raumabschließenden Bauteilen zusätzlich eine in Bauteilebene durchgehende Schicht aus nichtbrennbaren Baustoffen haben,
3.  Bauteile, deren tragende und aussteifende Teile aus brennbaren Baustoffen bestehen und die allseitig eine brandschutztechnisch wirksame Bekleidung aus nichtbrennbaren Baustoffen (Brandschutzbekleidung) und Dämmstoffe aus nichtbrennbaren Baustoffen haben,
4.  Bauteile aus brennbaren Baustoffen.

Soweit in diesem Gesetz oder in Vorschriften aufgrund dieses Gesetzes nichts anderes bestimmt ist, müssen

1.  Bauteile, die feuerbeständig sein müssen, mindestens den Anforderungen des Satzes 2 Nummer 2,
2.  Bauteile, die hochfeuerhemmend sein müssen, mindestens den Anforderungen des Satzes 2 Nummer 3

entsprechen.

Abweichend von Satz 3 sind andere Bauteile, die feuerbeständig oder hochfeuerhemmend sein müssen, aus brennbaren Baustoffen zulässig, sofern sie den Technischen Baubestimmungen nach § 86a entsprechen.
Satz 4 gilt nicht für Wände nach § 30 Absatz 3 Satz 1 und Wände nach § 35 Absatz 4 Satz 1 Nummer 1.

#### § 27 Tragende Wände, Stützen

(1) Tragende und aussteifende Wände und Stützen müssen im Brandfall ausreichend lang standsicher sein.
Sie müssen

1.  in Gebäuden der Gebäudeklasse 5 feuerbeständig,
2.  in Gebäuden der Gebäudeklasse 4 hochfeuerhemmend,
3.  in Gebäuden der Gebäudeklassen 2 und 3 feuerhemmend

sein.
Satz 2 gilt

1.  für Geschosse im Dachraum nur, wenn darüber noch Aufenthaltsräume möglich sind; § 29 Absatz 4 bleibt unberührt,
2.  für Balkone nur, wenn sie Teil des Rettungsweges sind,
3.  für offene Gänge nur, wenn sie als notwendige Flure dienen.

(2) Im Kellergeschoss müssen tragende und aussteifende Wände und Stützen

1.  in Gebäuden der Gebäudeklassen 3 bis 5 feuerbeständig,
2.  in Gebäuden der Gebäudeklassen 1 und 2 feuerhemmend

sein.

#### § 28 Außenwände

(1) Außenwände und Außenwandteile wie Brüstungen und Schürzen sind so auszubilden, dass eine Brandausbreitung auf und in diesen Bauteilen ausreichend lang begrenzt ist.

(2) Nichttragende Außenwände und nichttragende Teile tragender Außenwände müssen aus nichtbrennbaren Baustoffen bestehen; sie sind aus brennbaren Baustoffen zulässig, wenn sie als raumabschließende Bauteile feuerhemmend sind.
Satz 1 gilt nicht für

1.  Türen und Fenster,
2.  Fugendichtungen und
3.  brennbare Dämmstoffe in nichtbrennbaren geschlossenen Profilen der Außenwandkonstruktionen.

(3) Oberflächen von Außenwänden sowie Außenwandbekleidungen müssen einschließlich der Dämmstoffe und Unterkonstruktionen schwerentflammbar sein; Unterkonstruktionen aus normalentflammbaren Baustoffen sind zulässig, wenn die Anforderungen nach Absatz 1 erfüllt sind.
Balkonbekleidungen, die über die erforderliche Umwehrungshöhe hinaus hochgeführt werden, und mehr als zwei Geschosse überbrückende Solaranlagen an Außenwänden müssen schwerentflammbar sein.
Baustoffe, die schwerentflammbar sein müssen, in Bauteilen nach Satz 1 Halbsatz 1 und Satz 2 dürfen nicht brennend abfallen oder abtropfen.

(4) Bei Außenwandkonstruktionen mit geschossübergreifenden Hohl- oder Lufträumen wie hinterlüfteten Außenwandbekleidungen sind gegen die Brandausbreitung besondere Vorkehrungen zu treffen.
Satz 1 gilt für Doppelfassaden entsprechend.

(5) Die Absätze 2, 3 und 4 Satz 1 gelten nicht für Gebäude der Gebäudeklassen 1 bis 3; Absatz 4 Satz 2 gilt nicht für Gebäude der Gebäudeklassen 1 und 2.
Abweichend von Absatz 3 sind hinterlüftete Außenwandbekleidungen, die den Technischen Baubestimmungen nach § 86a entsprechen, mit Ausnahme der Dämmstoffe, aus normalentflammbaren Baustoffen zulässig.

#### § 29 Trennwände

(1) Trennwände nach Absatz 2 müssen als raumabschließende Bauteile von Räumen oder Nutzungseinheiten innerhalb von Geschossen ausreichend lang widerstandsfähig gegen die Brandausbreitung sein.

(2) Trennwände sind erforderlich

1.  zwischen Nutzungseinheiten sowie zwischen Nutzungseinheiten und anders genutzten Räumen, ausgenommen notwendigen Fluren,
2.  zum Abschluss von Räumen mit Explosions- oder erhöhter Brandgefahr,
3.  zwischen Aufenthaltsräumen und anders genutzten Räumen im Kellergeschoss.

(3) Trennwände nach Absatz 2 Nummer 1 und 3 müssen die Feuerwiderstandsfähigkeit der tragenden und aussteifenden Bauteile des Geschosses haben, jedoch mindestens feuerhemmend sein.
Trennwände nach Absatz 2 Nummer 2 müssen feuerbeständig sein.

(4) Die Trennwände nach Absatz 2 sind bis zur Rohdecke, im Dachraum bis unter die Dachhaut zu führen; werden in Dachräumen Trennwände nur bis zur Rohdecke geführt, ist diese Decke als raumabschließendes Bauteil einschließlich der sie tragenden und aussteifenden Bauteile feuerhemmend herzustellen.

(5) Öffnungen in Trennwänden nach Absatz 2 sind nur zulässig, wenn sie auf die für die Nutzung erforderliche Zahl und Größe beschränkt sind; sie müssen feuerhemmende, dicht- und selbstschließende Abschlüsse haben.

(6) Die Absätze 1 bis 5 gelten nicht für Wohngebäude der Gebäudeklassen 1 und 2.

#### § 30 Brandwände

(1) Brandwände müssen als raumabschließende Bauteile zum Abschluss von Gebäuden (Gebäudeabschlusswand) oder zur Unterteilung von Gebäuden in Brandabschnitte (innere Brandwand) ausreichend lang die Brandausbreitung auf andere Gebäude oder Brandabschnitte verhindern.

(2) Brandwände sind erforderlich

1.  als Gebäudeabschlusswand, ausgenommen von Gebäuden ohne Aufenthaltsräume und ohne Feuerstätten mit nicht mehr als 50 Kubikmeter Brutto-Rauminhalt, wenn diese Abschlusswände an oder mit einem Abstand von weniger als 2,50 Meter gegenüber der Grundstücksgrenze errichtet werden, es sei denn, dass ein Abstand von mindestens 5 Meter zu bestehenden oder nach den baurechtlichen Vorschriften zulässigen künftigen Gebäuden gesichert ist,
2.  als innere Brandwand zur Unterteilung ausgedehnter Gebäude in Abständen von nicht mehr als 40 Meter,
3.  als innere Brandwand zur Unterteilung landwirtschaftlich genutzter Gebäude sowie sonstiger Gebäude zur Tierhaltung, ausgenommen überdachte Tierhaltungsflächen, in Brandabschnitte von nicht mehr als 10 000 Kubikmeter Brutto-Rauminhalt,
4.  als Gebäudeabschlusswand zwischen Wohngebäuden und angebauten landwirtschaftlich genutzten Gebäuden sowie als innere Brandwand zwischen dem Wohnteil und dem landwirtschaftlich genutzten Teil eines Gebäudes.

(3) Brandwände müssen auch unter zusätzlicher mechanischer Beanspruchung feuerbeständig sein und aus nichtbrennbaren Baustoffen bestehen.
Anstelle von Brandwänden sind in den Fällen des Absatzes 2 Nummer 1 bis 3 zulässig

1.  für Gebäude der Gebäudeklasse 4 Wände, die auch unter zusätzlicher mechanischer Beanspruchung hochfeuerhemmend sind,
2.  für Gebäude der Gebäudeklassen 1 bis 3 hochfeuerhemmende Wände,
3.  für Gebäude der Gebäudeklassen 1 bis 3 Gebäudeabschlusswände, die jeweils von innen nach außen die Feuerwiderstandsfähigkeit der tragenden und aussteifenden Teile des Gebäudes, mindestens jedoch feuerhemmende Bauteile, und von außen nach innen die Feuerwiderstandsfähigkeit feuerbeständiger Bauteile haben.

In den Fällen des Absatzes 2 Nummer 4 sind anstelle von Brandwänden feuerbeständige Wände zulässig, wenn der Brutto-Rauminhalt des landwirtschaftlich genutzten Gebäudes oder Gebäudeteils nicht größer als 2 000 Kubikmeter ist.

(4) Brandwände müssen bis zur Bedachung durchgehen und in allen Geschossen übereinander angeordnet sein.
Abweichend davon dürfen anstelle innerer Brandwände Wände geschossweise versetzt angeordnet werden, wenn

1.  die Wände im Übrigen Absatz 3 Satz 1 entsprechen,
2.  die Decken, soweit sie in Verbindung mit diesen Wänden stehen, feuerbeständig sind, aus nichtbrennbaren Baustoffen bestehen und keine Öffnungen haben,
3.  die Bauteile, die diese Wände und Decken unterstützen, feuerbeständig sind und aus nichtbrennbaren Baustoffen bestehen,
4.  die Außenwände in der Breite des Versatzes in dem Geschoss oberhalb oder unterhalb des Versatzes feuerbeständig sind und
5.  Öffnungen in den Außenwänden im Bereich des Versatzes so angeordnet oder andere Vorkehrungen so getroffen sind, dass eine Brandausbreitung in andere Brandabschnitte nicht zu befürchten ist.

(5) Brandwände sind 0,30 Meter über die Bedachung zu führen oder in Höhe der Dachhaut mit einer beiderseits 0,50 Meter auskragenden feuerbeständigen Platte aus nichtbrennbaren Baustoffen abzuschließen; darüber dürfen brennbare Teile des Daches nicht hinweggeführt werden.
Bei Gebäuden der Gebäudeklassen 1 bis 3 sind Brandwände mindestens bis unter die Dachhaut zu führen.
Verbleibende Hohlräume sind vollständig mit nichtbrennbaren Baustoffen auszufüllen.

(6) Müssen Gebäude oder Gebäudeteile, die über Eck zusammenstoßen, durch eine Brandwand getrennt werden, so muss der Abstand dieser Wand von der inneren Ecke mindestens 5 Meter betragen; das gilt nicht, wenn der Winkel der inneren Ecke mehr als 120 Grad beträgt oder mindestens eine Außenwand auf 5 Meter Länge als öffnungslose feuerbeständige Wand aus nichtbrennbaren Baustoffen, bei Gebäuden der Gebäudeklassen 1 bis 4 als öffnungslose hochfeuerhemmende Wand ausgebildet ist.

(7) Bauteile mit brennbaren Baustoffen dürfen über Brandwände nicht hinweggeführt werden.
Bei Außenwandkonstruktionen, die eine seitliche Brandausbreitung begünstigen können, wie hinterlüftete Außenwandbekleidungen oder Doppelfassaden, sind gegen die Brandausbreitung im Bereich der Brandwände besondere Vorkehrungen zu treffen.
Außenwandbekleidungen von Gebäudeabschluss-wänden müssen einschließlich der Dämmstoffe und Unterkonstruktionen nichtbrennbar sein.
Bauteile dürfen in Brandwände nur soweit eingreifen, dass deren Feuerwiderstandsfähigkeit nicht beeinträchtigt wird; für Leitungen, Leitungsschlitze und Schornsteine gilt dies entsprechend.

(8) Öffnungen in Brandwänden sind unzulässig.
Sie sind in inneren Brandwänden nur zulässig, wenn sie auf die für die Nutzung erforderliche Zahl und Größe beschränkt sind; die Öffnungen müssen feuerbeständige, dicht- und selbstschließende Abschlüsse haben.

(9) In inneren Brandwänden sind feuerbeständige Verglasungen nur zulässig, wenn sie auf die für die Nutzung erforderliche Zahl und Größe beschränkt sind.

(10) Absatz 2 Nummer 1 gilt nicht für seitliche Wände von Vorbauten im Sinne des § 6 Absatz 6, wenn sie von dem Nachbargebäude oder der Nachbargrenze einen Abstand einhalten, der ihrer eigenen Ausladung entspricht, mindestens jedoch 1 Meter beträgt.

(11) Die Absätze 4 bis 10 gelten entsprechend auch für Wände, die nach Absatz 3 Satz 2 und 3 anstelle von Brandwänden zulässig sind.

#### § 31 Decken

(1) Decken müssen als tragende und raumabschließende Bauteile zwischen Geschossen im Brandfall ausreichend lang standsicher und widerstandsfähig gegen die Brandausbreitung sein.
Sie müssen

1.  in Gebäuden der Gebäudeklasse 5 feuerbeständig,
2.  in Gebäuden der Gebäudeklasse 4 hochfeuerhemmend,
3.  in Gebäuden der Gebäudeklassen 2 und 3 feuerhemmend

sein.
Satz 2 gilt

1.  für Geschosse im Dachraum nur, wenn darüber Aufenthaltsräume möglich sind; § 29 Absatz 4 bleibt unberührt,
2.  für Balkone nur, wenn sie Teil des Rettungsweges sind,
3.  für offene Gänge nur, wenn sie als notwendige Flure dienen.

(2) Im Kellergeschoss müssen Decken

1.  in Gebäuden der Gebäudeklassen 3 bis 5 feuerbeständig,
2.  in Gebäuden der Gebäudeklassen 1 und 2 feuerhemmend

sein.
Decken müssen feuerbeständig sein

1.  unter und über Räumen mit Explosions- oder erhöhter Brandgefahr, ausgenommen in Wohngebäuden der Gebäudeklassen 1 und 2,
2.  zwischen dem landwirtschaftlich genutzten Teil und dem Wohnteil eines Gebäudes.

(3) Der Anschluss der Decken an die Außenwand ist so herzustellen, dass er den Anforderungen aus Absatz 1 Satz 1 genügt.

(4) Öffnungen in Decken, für die eine Feuerwiderstandsfähigkeit vorgeschrieben ist, sind nur zulässig

1.  in Gebäuden der Gebäudeklassen 1 und 2,
2.  innerhalb derselben Nutzungseinheit mit nicht mehr als insgesamt 400 Quadratmeter Grundfläche in nicht mehr als zwei Geschossen,
3.  im Übrigen, wenn sie auf die für die Nutzung erforderliche Zahl und Größe beschränkt sind und Abschlüsse mit der Feuerwiderstandsfähigkeit der Decke haben.

#### § 32 Dächer

(1) Bedachungen müssen gegen eine Brandbeanspruchung von außen durch Flugfeuer und strahlende Wärme ausreichend lang widerstandsfähig sein (harte Bedachung).

(2) Bedachungen, die die Anforderungen nach Absatz 1 nicht erfüllen, sind zulässig bei Gebäuden der Gebäudeklassen 1 bis 3, wenn die Gebäude

1.  einen Abstand von der Grundstücksgrenze von mindestens 12 Meter,
2.  von Gebäuden auf demselben Grundstück mit harter Bedachung einen Abstand von mindestens 15 Meter,
3.  von Gebäuden auf demselben Grundstück mit Bedachungen, die die Anforderungen nach Absatz 1 nicht erfüllen, einen Abstand von mindestens 24 Meter,
4.  von Gebäuden auf demselben Grundstück ohne Aufenthaltsräume und ohne Feuerstätten mit nicht mehr als 50 Kubikmeter Brutto-Rauminhalt einen Abstand von mindestens 5 Meter

einhalten.
Soweit Gebäude nach Satz 1 Abstand halten müssen, genügt bei Wohngebäuden der Gebäudeklassen 1 und 2 in den Fällen

1.  der Nummer 1 ein Abstand von mindestens 6 Meter,
2.  der Nummer 2 ein Abstand von mindestens 9 Meter,
3.  der Nummer 3 ein Abstand von mindestens 12 Meter.

(3) Die Absätze 1 und 2 gelten nicht für

1.  Gebäude ohne Aufenthaltsräume und ohne Feuerstätten mit nicht mehr als 50 Kubikmeter Brutto-Rauminhalt,
2.  lichtdurchlässige Bedachungen aus nichtbrennbaren Baustoffen; brennbare Fugendichtungen und brennbare Dämmstoffe in nichtbrennbaren Profilen sind zulässig,
3.  Dachflächenfenster, Oberlichte und Lichtkuppeln von Wohngebäuden,
4.  Eingangsüberdachungen und Vordächer aus nichtbrennbaren Baustoffen,
5.  Eingangsüberdachungen aus brennbaren Baustoffen, wenn die Eingänge nur zu Wohnungen führen.

(4) Abweichend von den Absätzen 1 und 2 sind

1.  lichtdurchlässige Teilflächen aus brennbaren Baustoffen in Bedachungen nach Absatz 1 und
2.  begrünte Bedachungen

zulässig, wenn eine Brandentstehung bei einer Brandbeanspruchung von außen durch Flugfeuer und strahlende Wärme nicht zu befürchten ist oder Vorkehrungen hiergegen getroffen werden.

(5) Dachüberstände, Dachgesimse und Dachaufbauten, lichtdurchlässige Bedachungen, Dachflächenfenster, Lichtkuppeln, Oberlichte und Solaranlagen sind so anzuordnen und herzustellen, dass Feuer nicht auf andere Gebäudeteile und Nachbargrundstücke übertragen werden kann.
Von Brandwänden und von Wänden, die anstelle von Brandwänden zulässig sind, müssen mindestens 1,25 Meter entfernt sein

1.  Dachflächenfenster, Oberlichte, Lichtkuppeln und Öffnungen in der Bedachung, wenn diese Wände nicht mindestens 30 Zentimeter über die Bedachung geführt sind,
2.  Solaranlagen, Dachgauben und ähnliche Dachaufbauten aus brennbaren Baustoffen, wenn sie nicht durch diese Wände gegen Brandübertragung geschützt sind.

(6) Dächer von traufseitig aneinandergebauten Gebäuden müssen als raumabschließende Bauteile für eine Brandbeanspruchung von innen nach außen einschließlich der sie tragenden und aussteifenden Bauteile feuerhemmend sein.
Öffnungen in diesen Dachflächen müssen waagerecht gemessen mindestens 2 Meter von der Brandwand oder der Wand, die anstelle der Brandwand zulässig ist, entfernt sein.

(7) Dächer von Anbauten, die an Außenwände mit Öffnungen oder ohne Feuerwiderstandsfähigkeit anschließen, müssen innerhalb eines Abstands von 5 Meter von diesen Wänden als raumabschließende Bauteile für eine Brandbeanspruchung von innen nach außen einschließlich der sie tragenden und aussteifenden Bauteile die Feuerwiderstandsfähigkeit der Decken des Gebäudeteils haben, an den sie angebaut werden.
Dies gilt nicht für Anbauten an Wohngebäude der Gebäudeklassen 1 bis 3.

(8) Für vom Dach aus vorzunehmende Arbeiten sind sicher benutzbare Vorrichtungen anzubringen.

(9) Soweit geneigte Dächer an Verkehrsflächen angrenzen, müssen sie Vorrichtungen zum Schutz gegen das Herabfallen von Schnee und Eis haben.

### Abschnitt 5  
Rettungswege, Öffnungen, Umwehrungen

#### § 33 Erster und zweiter Rettungsweg

(1) Für Nutzungseinheiten mit mindestens einem Aufenthaltsraum, wie Wohnungen, Praxen, selbstständige Betriebsstätten, müssen in jedem Geschoss mindestens zwei voneinander unabhängige Rettungswege ins Freie vorhanden sein; beide Rettungswege dürfen jedoch innerhalb des Geschosses über denselben notwendigen Flur führen.

(2) Für Nutzungseinheiten nach Absatz 1, die nicht zu ebener Erde liegen, muss der erste Rettungsweg über eine notwendige Treppe führen.
Der zweite Rettungsweg kann eine weitere notwendige Treppe oder eine mit Rettungsgeräten der Feuerwehr erreichbare Stelle der Nutzungseinheit sein.
Ein zweiter Rettungsweg ist nicht erforderlich, wenn die Rettung über einen sicher erreichbaren Treppenraum möglich ist, in den Feuer und Rauch nicht eindringen können (Sicherheitstreppenraum).

(3) Gebäude, deren zweiter Rettungsweg über Rettungsgeräte der Feuerwehr führt und bei denen die Oberkante der Brüstung von zum Anleitern bestimmten Fenstern oder Stellen mehr als 8 Meter über der Geländeoberfläche liegt, dürfen nur errichtet werden, wenn die Feuerwehr über die erforderlichen Rettungsgeräte wie Hubrettungsfahrzeuge verfügt.
Bei Sonderbauten ist der zweite Rettungsweg über Rettungsgeräte der Feuerwehr nur zulässig, wenn keine Bedenken wegen der Personenrettung bestehen.

#### § 34 Treppen

(1) Jedes nicht zu ebener Erde liegende Geschoss und der benutzbare Dachraum eines Gebäudes müssen über mindestens eine Treppe zugänglich sein (notwendige Treppe).
Statt notwendiger Treppen sind Rampen mit flacher Neigung zulässig.

(2) Einschiebbare Treppen und Rolltreppen sind als notwendige Treppen unzulässig.
In Gebäuden der Gebäudeklassen 1 und 2 sind einschiebbare Treppen und Leitern als Zugang zu einem Dachraum ohne Aufenthaltsraum zulässig.

(3) Notwendige Treppen sind in einem Zuge zu allen angeschlossenen Geschossen zu führen; sie müssen mit den Treppen zum Dachraum unmittelbar verbunden sein.
Dies gilt nicht für Treppen

1.  in Gebäuden der Gebäudeklassen 1 bis 3,
2.  nach § 35 Absatz 1 Satz 3 Nummer 2.

(4) Die tragenden Teile notwendiger Treppen müssen

1.  in Gebäuden der Gebäudeklasse 5 feuerhemmend und aus nichtbrennbaren Baustoffen,
2.  in Gebäuden der Gebäudeklasse 4 aus nichtbrennbaren Baustoffen,
3.  in Gebäuden der Gebäudeklasse 3 aus nichtbrennbaren Baustoffen oder feuerhemmend

sein.
Tragende Teile von Außentreppen nach § 35 Absatz 1 Satz 3 Nummer 3 für Gebäude der Gebäudeklassen 3 bis 5 müssen aus nichtbrennbaren Baustoffen bestehen.

(5) Die nutzbare Breite der Treppenläufe und Treppenabsätze notwendiger Treppen muss für den größten zu erwartenden Verkehr ausreichen.

(6) Treppen müssen einen festen und griffsicheren Handlauf haben.
Für Treppen sind Handläufe auf beiden Seiten und Zwischenhandläufe vorzusehen, soweit die Verkehrssicherheit dies erfordert.

(7) Eine Treppe darf nicht unmittelbar hinter einer Tür beginnen, die in Richtung der Treppe aufschlägt; zwischen Treppe und Tür ist ein ausreichender Treppenabsatz anzuordnen.

#### § 35 Notwendige Treppenräume, Ausgänge

(1) Jede notwendige Treppe muss zur Sicherstellung der Rettungswege aus den Geschossen ins Freie in einem eigenen, durchgehenden Treppenraum liegen (notwendiger Treppenraum).
Notwendige Treppenräume müssen so angeordnet und ausgebildet sein, dass die Nutzung der notwendigen Treppen im Brandfall ausreichend lang möglich ist.
Notwendige Treppen sind ohne eigenen Treppenraum zulässig

1.  in Gebäuden der Gebäudeklassen 1 und 2,
2.  für die Verbindung von höchstens zwei Geschossen innerhalb derselben Nutzungseinheit von insgesamt nicht mehr als 200 Quadratmeter Grundfläche, wenn in jedem Geschoss ein anderer Rettungsweg erreicht werden kann,
3.  als Außentreppe, wenn ihre Nutzung ausreichend sicher ist und im Brandfall nicht gefährdet werden kann.

(2) Von jeder Stelle eines Aufenthaltsraumes sowie eines Kellergeschosses muss mindestens ein Ausgang in einen notwendigen Treppenraum oder ins Freie in höchstens 35 Meter Entfernung erreichbar sein.
Übereinanderliegende Kellergeschosse müssen jeweils mindestens zwei Ausgänge in notwendige Treppenräume oder ins Freie haben.
Sind mehrere notwendige Treppenräume erforderlich, müssen sie so verteilt sein, dass sie möglichst entgegengesetzt liegen und dass die Rettungswege möglichst kurz sind.

(3) Jeder notwendige Treppenraum muss einen unmittelbaren Ausgang ins Freie haben.
Sofern der Ausgang eines notwendigen Treppenraumes nicht unmittelbar ins Freie führt, muss der Raum zwischen dem notwendigen Treppenraum und dem Ausgang ins Freie

1.  mindestens so breit sein wie die dazugehörigen Treppenläufe,
2.  Wände haben, die die Anforderungen an die Wände des Treppenraumes erfüllen,
3.  rauchdichte und selbstschließende Abschlüsse zu notwendigen Fluren haben und
4.  ohne Öffnungen zu anderen Räumen, ausgenommen zu notwendigen Fluren, sein.

(4) Die Wände notwendiger Treppenräume müssen als raumabschließende Bauteile

1.  in Gebäuden der Gebäudeklasse 5 die Bauart von Brandwänden haben,
2.  in Gebäuden der Gebäudeklasse 4 auch unter zusätzlicher mechanischer Beanspruchung hochfeuerhemmend und
3.  in Gebäuden der Gebäudeklasse 3 feuerhemmend sein.

Dies ist nicht erforderlich für Außenwände von Treppenräumen, die aus nichtbrennbaren Baustoffen bestehen und durch andere an diese Außenwände anschließende Gebäudeteile im Brandfall nicht gefährdet werden können.
Der obere Abschluss notwendiger Treppenräume muss als raumabschließendes Bauteil die Feuerwiderstandsfähigkeit der Decken des Gebäudes haben; dies gilt nicht, wenn der obere Abschluss das Dach ist und die Treppenraumwände bis unter die Dachhaut reichen.

(5) In notwendigen Treppenräumen und in Räumen nach Absatz 3 Satz 2 müssen

1.  Bekleidungen, Putze, Dämmstoffe, Unterdecken und Einbauten aus nichtbrennbaren Baustoffen bestehen,
2.  Wände und Decken aus brennbaren Baustoffen eine Bekleidung aus nichtbrennbaren Baustoffen in ausreichender Dicke haben,
3.  Bodenbeläge, ausgenommen Gleitschutzprofile, aus mindestens schwerentflammbaren Baustoffen bestehen.

(6) In notwendigen Treppenräumen müssen Öffnungen

1.  zu Kellergeschossen, zu nicht ausgebauten Dachräumen, Werkstätten, Läden, Lager- und ähnlichen Räumen sowie zu sonstigen Räumen und Nutzungseinheiten mit einer Fläche von mehr als 200 Quadratmeter Grundfläche, ausgenommen Wohnungen, mindestens feuerhemmende, rauchdichte und selbstschließende Abschlüsse,
2.  zu notwendigen Fluren rauchdichte und selbstschließende Abschlüsse,
3.  zu sonstigen Räumen und Nutzungseinheiten mindestens dicht- und selbstschließende Abschlüsse

haben.
Die Feuerschutz- und Rauchschutzabschlüsse dürfen lichtdurchlässige Seitenteile und Oberlichte enthalten, wenn der Abschluss insgesamt nicht breiter als 2,50 Meter ist.

(7) Notwendige Treppenräume müssen zu beleuchten sein.
Notwendige Treppenräume ohne Fenster müssen in Gebäuden mit einer Höhe nach § 2 Absatz 3 Satz 2 von mehr als 13 Meter eine Sicherheitsbeleuchtung haben.

(8) Notwendige Treppenräume müssen belüftet und zur Unterstützung wirksamer Löscharbeiten entraucht werden können.
Sie müssen

1.  in jedem oberirdischen Geschoss unmittelbar ins Freie führende Fenster mit einem freien Querschnitt von mindestens 0,50 Quadratmeter haben, die geöffnet werden können, oder
2.  an der obersten Stelle eine Öffnung zur Rauchableitung haben.

In den Fällen des Satzes 2 Nummer 1 ist in Gebäuden der Gebäudeklasse 5 an der obersten Stelle eine Öffnung zur Rauchableitung erforderlich; in den Fällen des Satzes 2 Nummer 2 sind in Gebäuden der Gebäudeklassen 4 und 5, soweit dies zur Erfüllung der Anforderungen nach Satz 1 erforderlich ist, besondere Vorkehrungen zu treffen.
Öffnungen zur Rauchableitung nach den Sätzen 2 und 3 müssen in jedem Treppenraum einen freien Querschnitt von mindestens 1 Quadratmeter und Vorrichtungen zum Öffnen ihrer Abschlüsse haben, die vom Erdgeschoss sowie vom obersten Treppenabsatz aus bedient werden können.

#### § 36 Notwendige Flure, offene Gänge

(1) Flure, über die Rettungswege aus Aufenthaltsräumen oder aus Nutzungseinheiten mit Aufenthaltsräumen zu Ausgängen in notwendige Treppenräume oder ins Freie führen (notwendige Flure), müssen so angeordnet und ausgebildet sein, dass die Nutzung im Brandfall ausreichend lang möglich ist.
Notwendige Flure sind nicht erforderlich

1.  in Wohngebäuden der Gebäudeklassen 1 und 2,
2.  in sonstigen Gebäuden der Gebäudeklassen 1 und 2, ausgenommen in Kellergeschossen,
3.  innerhalb von Nutzungseinheiten mit nicht mehr als 200 Quadratmeter Grundfläche und innerhalb von Wohnungen,
4.  innerhalb von Nutzungseinheiten, die einer Büro- oder Verwaltungsnutzung dienen, mit nicht mehr als 400 Quadratmeter Grundfläche; das gilt auch für Teile größerer Nutzungseinheiten, wenn diese Teile nicht größer als 400 Quadratmeter Grundfläche sind, Trennwände nach § 29 Absatz 2 Nummer 1 haben und jeder Teil unabhängig von anderen Teilen Rettungswege nach § 33 Absatz 1 hat.

(2) Notwendige Flure müssen so breit sein, dass sie für den größten zu erwartenden Verkehr ausreichen.
In den Fluren ist eine Folge von weniger als drei Stufen unzulässig.

(3) Notwendige Flure sind durch nichtabschließbare, rauchdichte und selbstschließende Abschlüsse in Rauchabschnitte zu unterteilen.
Die Rauchabschnitte sollen nicht länger als 30 Meter sein.
Die Abschlüsse sind bis an die Rohdecke zu führen; sie dürfen bis an die Unterdecke der Flure geführt werden, wenn die Unterdecke feuerhemmend ist.
Notwendige Flure mit nur einer Fluchtrichtung, die zu einem Sicherheitstreppenraum führen, dürfen nicht länger als 15 Meter sein.
Die Sätze 1 bis 4 gelten nicht für offene Gänge nach Absatz 5.

(4) Die Wände notwendiger Flure müssen als raumabschließende Bauteile feuerhemmend sein.
In Kellergeschossen, deren tragende und aussteifende Bauteile feuerbeständig sein müssen, müssen die Wände notwendiger Flure als raumabschließende Bauteile feuerbeständig sein.
Die Wände sind bis an die Rohdecke zu führen.
Sie dürfen bis an die Unterdecke der Flure geführt werden, wenn die Unterdecke feuerhemmend und ein demjenigen nach Satz 1 vergleichbarer Raumabschluss sichergestellt ist.
Türen in diesen Wänden müssen dicht schließen; Öffnungen zu Lagerbereichen im Kellergeschoss müssen feuerhemmende, dicht- und selbstschließende Abschlüsse haben.

(5) Für Wände und Brüstungen notwendiger Flure mit nur einer Fluchtrichtung, die als offene Gänge vor den Außenwänden angeordnet sind, gilt Absatz 4 entsprechend.
Fenster sind in diesen Außenwänden ab einer Brüstungshöhe von 0,90 Meter zulässig.

(6) In notwendigen Fluren sowie in offenen Gängen nach Absatz 5 müssen

1.  Bekleidungen, Putze, Unterdecken und Dämmstoffe aus nichtbrennbaren Baustoffen bestehen,
2.  Wände und Decken aus brennbaren Baustoffen eine Bekleidung aus nichtbrennbaren Baustoffen in ausreichender Dicke haben.

#### § 37 Fenster, Türen, sonstige Öffnungen

(1) Können die Fensterflächen nicht gefahrlos vom Erdboden, vom Innern des Gebäudes, von Loggien oder Balkonen aus gereinigt werden, so sind Vorrichtungen wie Aufzüge, Halterungen oder Stangen anzubringen, die eine Reinigung von außen ermöglichen.

(2) Glastüren und andere Glasflächen, die bis zum Fußboden allgemein zugänglicher Verkehrsflächen herabreichen, sind so zu kennzeichnen, dass sie leicht erkannt werden können.
Weitere Schutzmaßnahmen sind für größere Glasflächen vorzusehen, wenn dies die Verkehrssicherheit erfordert.

(3) Eingangstüren von Wohnungen, die über Aufzüge erreichbar sein müssen, müssen eine lichte Durchgangsbreite von mindestens 0,90 Meter haben.

(4) Jedes Kellergeschoss ohne Fenster muss mindestens eine Öffnung ins Freie haben, um eine Rauchableitung zu ermöglichen.
Gemeinsame Kellerlichtschächte für übereinanderliegende Kellergeschosse sind unzulässig.

(5) Fenster, die als Rettungswege nach § 33 Absatz 2 Satz 2 dienen, müssen im Lichten mindestens 0,90 Meter x 1,20 Meter groß und nicht höher als 1,20 Meter über der Fußbodenoberkante angeordnet sein.
Liegen diese Fenster in Dachschrägen oder Dachaufbauten, so darf ihre Unterkante oder ein davor liegender Austritt von der Traufkante horizontal gemessen nicht mehr als 1 Meter entfernt sein.

#### § 38 Umwehrungen

(1) In, an und auf baulichen Anlagen sind zu umwehren oder mit Brüstungen zu versehen:

1.  Flächen, die im Allgemeinen zum Begehen bestimmt sind und unmittelbar an mehr als 1 Meter tiefer liegende Flächen angrenzen; dies gilt nicht, wenn die Umwehrung dem Zweck der Flächen widerspricht,
2.  nicht begehbare Oberlichte und Glasabdeckungen in Flächen, die im Allgemeinen zum Begehen bestimmt sind, wenn sie weniger als 0,50 Meter aus diesen Flächen herausragen,
3.  Dächer oder Dachteile, die zum auch nur zeitweiligen Aufenthalt von Menschen bestimmt sind,
4.  Öffnungen in begehbaren Decken sowie in Dächern oder Dachteilen nach Nummer 3, wenn sie nicht sicher abgedeckt sind,
5.  nicht begehbare Glasflächen in Decken sowie in Dächern oder Dachteilen nach Nummer 3,
6.  die freien Seiten von Treppenläufen, Treppenabsätzen und Treppenöffnungen (Treppenaugen),
7.  Kellerlichtschächte und Betriebsschächte, die an Verkehrsflächen liegen, wenn sie nicht verkehrssicher abgedeckt sind.

(2) In Verkehrsflächen liegende Kellerlichtschächte und Betriebsschächte sind in Höhe der Verkehrsfläche verkehrssicher abzudecken.
An und in Verkehrsflächen liegende Abdeckungen müssen gegen unbefugtes Abheben gesichert sein.
Fenster, die unmittelbar an Treppen liegen und deren Brüstungen unter der notwendigen Umwehrungshöhe liegen, sind zu sichern.

(3) Fensterbrüstungen von Flächen mit einer Absturzhöhe bis zu 12 Meter müssen mindestens 0,80 Meter, von Flächen mit mehr als 12 Meter Absturzhöhe mindestens 0,90 Meter hoch sein.
Geringere Brüstungshöhen sind zulässig, wenn durch andere Vorrichtungen wie Geländer die nach Absatz 4 vorgeschriebenen Mindesthöhen eingehalten werden.

(4) Andere notwendige Umwehrungen müssen folgende Mindesthöhen haben:

1.  Umwehrungen zur Sicherung von Öffnungen in begehbaren Decken und Dächern sowie Umwehrungen von Flächen mit einer Absturzhöhe von 1 Meter bis zu 12 Meter 0,90 Meter,
2.  Umwehrungen von Flächen mit mehr als 12 Meter Absturzhöhe 1,10 Meter.

### Abschnitt 6  
Technische Gebäudeausrüstung

#### § 39 Aufzüge

(1) Aufzüge im Innern von Gebäuden müssen eigene Fahrschächte haben, um eine Brandausbreitung in andere Geschosse ausreichend lang zu verhindern.
In einem Fahrschacht dürfen bis zu drei Aufzüge liegen.
Aufzüge ohne eigene Fahrschächte sind zulässig

1.  innerhalb eines notwendigen Treppenraumes, ausgenommen in Hochhäusern,
2.  innerhalb von Räumen, die Geschosse überbrücken,
3.  zur Verbindung von Geschossen, die offen miteinander in Verbindung stehen dürfen,
4.  in Gebäuden der Gebäudeklassen 1 und 2;

sie müssen sicher umkleidet sein.

(2) Die Fahrschachtwände müssen als raumabschließende Bauteile

1.  in Gebäuden der Gebäudeklasse 5 feuerbeständig und aus nichtbrennbaren Baustoffen,
2.  in Gebäuden der Gebäudeklasse 4 hochfeuerhemmend,
3.  in Gebäuden der Gebäudeklasse 3 feuerhemmend

sein; Fahrschachtwände aus brennbaren Baustoffen müssen schachtseitig eine Bekleidung aus nichtbrennbaren Baustoffen in ausreichender Dicke haben.
Fahrschachttüren und andere Öffnungen in Fahrschachtwänden mit erforderlicher Feuerwiderstandsfähigkeit sind so herzustellen, dass die Anforderungen nach Absatz 1 Satz 1 nicht beeinträchtigt werden.

(3) Fahrschächte müssen zu lüften sein und eine Öffnung zur Rauchableitung mit einem freien Querschnitt von mindestens 2,5 Prozent der Fahrschachtgrundfläche, mindestens jedoch 0,10 Quadratmeter haben.
Diese Öffnung darf einen Abschluss haben, der im Brandfall selbsttätig öffnet und von mindestens einer geeigneten Stelle aus bedient werden kann.
Die Lage der Rauchaustrittsöffnungen muss so gewählt werden, dass der Rauchaustritt durch Windeinfluss nicht beeinträchtigt wird.

(4) Gebäude mit einer Höhe nach § 2 Absatz 3 Satz 2 von mehr als 13 Meter müssen Aufzüge in ausreichender Zahl haben; dabei sind Aufenthaltsräume im obersten Geschoss nicht zu berücksichtigen, die eine Nutzungseinheit mit Aufenthaltsräumen im darunter liegenden Geschoss bilden.
Von diesen Aufzügen muss mindestens ein Aufzug Kinderwagen, Rollstühle, Krankentragen und Lasten aufnehmen können und Haltestellen in allen Geschossen haben.
Dieser Aufzug muss von der öffentlichen Verkehrsfläche und von allen Wohnungen in dem Gebäude aus stufenlos erreichbar sein.
Haltestellen im obersten Geschoss, im Erdgeschoss und in den Kellergeschossen sind nicht erforderlich, wenn sie nur unter besonderen Schwierigkeiten hergestellt werden können.
Satz 1 Halbsatz 1 gilt nicht, wenn das Dach bestehender Gebäude nachträglich ausgebaut wird.

(5) Fahrkörbe zur Aufnahme einer Krankentrage müssen eine nutzbare Grundfläche von mindestens 1,10 Meter x 2,10 Meter, zur Aufnahme eines Rollstuhls von mindestens 1,10 Meter x 1,40 Meter haben; Türen müssen eine lichte Durchgangsbreite von mindestens 0,90 Meter haben.
In einem Aufzug für Rollstühle und Krankentragen darf der für Rollstühle nicht erforderliche Teil der Fahrkorbgrundfläche durch eine verschließbare Tür abgesperrt werden.
Vor den Aufzügen muss eine ausreichende Bewegungsfläche vorhanden sein.

#### § 40 Leitungsanlagen, Installationsschächte und Installationskanäle

(1) Leitungen dürfen durch raumabschließende Bauteile, für die eine Feuerwiderstandsfähigkeit vorgeschrieben ist, nur hindurchgeführt werden, wenn eine Brandausbreitung ausreichend lang nicht zu befürchten ist oder Vorkehrungen hiergegen getroffen sind; dies gilt nicht

1.  für Gebäude der Gebäudeklassen 1 und 2,
2.  innerhalb von Wohnungen,
3.  innerhalb derselben Nutzungseinheit mit nicht mehr als insgesamt 400 Quadratmeter in nicht mehr als zwei Geschossen.

(2) In notwendigen Treppenräumen, in Räumen nach § 35 Absatz 3 Satz 2 und in notwendigen Fluren sind Leitungsanlagen nur zulässig, wenn eine Nutzung als Rettungsweg im Brandfall ausreichend lang möglich ist.

(3) Für Installationsschächte und -kanäle gelten Absatz 1 sowie § 41 Absatz 2 Satz 1 und Absatz 3 entsprechend.

#### § 41 Lüftungsanlagen

(1) Lüftungsanlagen müssen betriebssicher und brandsicher sein; sie dürfen den ordnungsgemäßen Betrieb von Feuerungsanlagen nicht beeinträchtigen.

(2) Lüftungsleitungen sowie deren Bekleidungen und Dämmstoffe müssen aus nichtbrennbaren Baustoffen bestehen; brennbare Baustoffe sind zulässig, wenn ein Beitrag der Lüftungsleitung zur Brandentstehung und Brandweiterleitung nicht zu befürchten ist.
Lüftungsleitungen dürfen raumabschließende Bauteile, für die eine Feuerwiderstandsfähigkeit vorgeschrieben ist, nur überbrücken, wenn eine Brandausbreitung ausreichend lang nicht zu befürchten ist oder wenn Vorkehrungen hiergegen getroffen sind.

(3) Lüftungsanlagen sind so herzustellen, dass sie Gerüche und Staub nicht in andere Räume übertragen.

(4) Lüftungsanlagen dürfen nicht in Abgasanlagen eingeführt werden; die gemeinsame Nutzung von Lüftungsleitungen zur Lüftung und zur Ableitung der Abgase von Feuerstätten ist zulässig, wenn keine Bedenken wegen der Betriebssicherheit und des Brandschutzes bestehen.
Die Abluft ist ins Freie zu führen.
Nicht zur Lüftungsanlage gehörende Einrichtungen sind in Lüftungsleitungen unzulässig.

(5) Die Absätze 2 und 3 gelten nicht

1.  für Gebäude der Gebäudeklassen 1 und 2,
2.  innerhalb von Wohnungen,
3.  innerhalb derselben Nutzungseinheit mit nicht mehr als 400 Quadratmeter in nicht mehr als zwei Geschossen.

(6) Für raumlufttechnische Anlagen und Warmluftheizungen gelten die Absätze 1 bis 5 entsprechend.

#### § 42 Feuerungsanlagen, sonstige Anlagen zur Wärmeerzeugung, Brennstoffversorgung

(1) Feuerstätten und Abgasanlagen (Feuerungsanlagen) müssen betriebssicher und brandsicher sein.

(2) Feuerstätten dürfen in Räumen nur aufgestellt werden, wenn nach der Art der Feuerstätte und nach Lage, Größe, baulicher Beschaffenheit und Nutzung der Räume Gefahren nicht entstehen.

(3) Abgase von Feuerstätten sind durch Abgasleitungen, Schornsteine und Verbindungsstücke (Abgasanlagen) so abzuführen, dass keine Gefahren oder unzumutbaren Belästigungen entstehen.
Abgasanlagen sind in solcher Zahl und Lage und so herzustellen, dass die Feuerstätten des Gebäudes ordnungsgemäß angeschlossen werden können.
Sie müssen leicht gereinigt werden können.

(4) Behälter und Rohrleitungen für brennbare Gase und Flüssigkeiten müssen betriebssicher und brandsicher sein.
Diese Behälter sowie feste Brennstoffe sind so aufzustellen oder zu lagern, dass keine Gefahren oder unzumutbaren Belästigungen entstehen.

(5) Für die Aufstellung von ortsfesten Verbrennungsmotoren, Blockheizkraftwerken, Brennstoffzellen und Verdichtern sowie die Ableitung ihrer Verbrennungsgase gelten die Absätze 1 bis 3 entsprechend.

#### § 43 Sanitäre Anlagen

Fensterlose Bäder und Toiletten sind nur zulässig, wenn eine wirksame Lüftung gewährleistet ist.

#### § 44 Kleinkläranlagen, Gruben

Kleinkläranlagen und Gruben müssen wasserdicht und ausreichend groß sein.
Sie müssen eine dichte und sichere Abdeckung sowie Reinigungs- und Entleerungsöffnungen haben.
Diese Öffnungen dürfen nur vom Freien aus zugänglich sein.
Die Anlagen sind so zu entlüften, dass Gesundheitsschäden oder unzumutbare Belästigungen nicht entstehen.
Die Zuleitungen zu Abwasserentsorgungsanlagen müssen geschlossen, dicht, und, soweit erforderlich, zum Reinigen eingerichtet sein.

#### § 45 Aufbewahrung fester Abfallstoffe

Feste Abfallstoffe dürfen innerhalb von Gebäuden vorübergehend aufbewahrt werden, in Gebäuden der Gebäudeklassen 3 bis 5 jedoch nur, wenn die dafür bestimmten Räume

1.  Trennwände und Decken als raumabschließende Bauteile mit der Feuerwiderstandsfähigkeit der tragenden Wände und
2.  Öffnungen vom Gebäudeinnern zum Aufstellraum mit feuerhemmenden, dicht und selbstschließenden Abschlüssen haben,
3.  unmittelbar vom Freien entleert werden können und
4.  eine ständig wirksame Lüftung haben.

#### § 46 Blitzschutzanlagen

Bauliche Anlagen, bei denen nach Lage, Bauart oder Nutzung Blitzschlag leicht eintreten oder zu schweren Folgen führen kann, sind mit dauernd wirksamen Blitzschutzanlagen zu versehen.

### Abschnitt 7  
Nutzungsbedingte Anforderungen

#### § 47 Aufenthaltsräume

(1) Aufenthaltsräume müssen eine lichte Raumhöhe von mindestens 2,40 Meter haben.
Dies gilt nicht für Aufenthaltsräume in Wohngebäuden der Gebäudeklassen 1 und 2 sowie für Aufenthaltsräume im Dachraum.

(2) Aufenthaltsräume müssen ausreichend belüftet und mit Tageslicht beleuchtet werden können.
Sie müssen Fenster mit einem Rohbaumaß der Fensteröffnungen von mindestens ein Achtel der Netto-Grundfläche des Raumes einschließlich der Netto-Grundfläche verglaster Vorbauten und Loggien haben.

(3) Aufenthaltsräume, deren Nutzung eine Beleuchtung mit Tageslicht verbietet, sind ohne Belichtungsöffnungen zulässig.
Aufenthaltsräume ohne Belichtungsöffnungen müssen durch technische Einrichtungen ausreichend beleuchtet und belüftet werden können.

#### § 48 Wohnungen

(1) Innerhalb jeder Wohnung müssen die technischen Voraussetzungen für den Einbau einer Küche vorhanden sein.
§ 43 findet entsprechende Anwendung.

(2) In Wohngebäuden der Gebäudeklassen 3 bis 5 sind leicht erreichbare und gut zugängliche Abstellräume für Rollstühle, Kinderwagen und Fahrräder sowie für jede Wohnung ein ausreichend großer Abstellraum herzustellen.

(3) Jede Wohnung muss ein Bad mit Badewanne oder Dusche und eine Toilette haben.

(4) In Wohnungen müssen

1.  Aufenthaltsräume, ausgenommen Küchen, und
2.  Flure, über die Rettungswege von Aufenthaltsräumen führen,

jeweils mindestens einen Rauchwarnmelder haben.
Die Rauchwarnmelder müssen so eingebaut oder angebracht und betrieben werden, dass Brandrauch frühzeitig erkannt und gemeldet wird.
Bestehende Wohnungen sind bis zum 31. Dezember 2020 entsprechend auszustatten.

#### § 49 Notwendige Stellplätze und notwendige Abstellplätze für Fahrräder

(1) Bei der Errichtung, Änderung oder Nutzungsänderung von baulichen Anlagen sowie anderen Anlagen, bei denen ein Zu- oder Abgangsverkehr mittels Kraftfahrzeugen oder mittels Fahrrädern zu erwarten ist, müssen die durch die Gemeinde in einer örtlichen Bauvorschrift nach § 87 festgesetzten notwendigen Stellplätze oder notwendige Abstellplätze für Fahrräder hergestellt werden.

(2) Die notwendigen Stellplätze sowie die notwendigen Abstellplätze für Fahrräder sind auf dem Baugrundstück oder in zumutbarer Entfernung davon auf einem geeigneten Grundstück herzustellen, dessen Benutzung für diesen Zweck öffentlich-rechtlich gesichert wird.

(3) Soweit die Bauherrin oder der Bauherr durch örtliche Bauvorschrift zur Herstellung von notwendigen Stellplätzen oder notwendigen Abstellplätzen für Fahrräder verpflichtet ist, kann die Gemeinde durch öffentlich-rechtlichen Vertrag mit der Bauherrin oder dem Bauherrn vereinbaren, dass die Bauherrin oder der Bauherr ihre oder seine Verpflichtung ganz oder teilweise durch Zahlung eines Geldbetrages an die Gemeinde ablöst (Stellplatzablösevertrag).
Der Anspruch der Gemeinde auf Zahlung des im Stellplatzablösevertrag vereinbarten Geldbetrages entsteht mit Baubeginn.

(4) Die Gemeinde hat den Geldbetrag für die Ablösung von notwendigen Stellplätzen oder von notwendigen Abstellplätzen für Fahrräder zu verwenden für

1.  die Herstellung zusätzlicher oder die Instandhaltung, die Instandsetzung oder die Modernisierung bestehender Parkeinrichtungen oder die Herstellung von Ladeinfrastruktur für Elektrofahrzeuge,
2.  sonstige Maßnahmen zur Entlastung der Straßen vom ruhenden Verkehr einschließlich investiver Maßnahmen des öffentlichen Personennahverkehrs und Maßnahmen zur Verbesserung des Fahrradverkehrs,
3.  die Herstellung von Parkeinrichtungen für die gemeinschaftliche Nutzung von Kraftfahrzeugen (Carsharing).

(5) Die Bauaufsichtsbehörde kann zulassen, dass notwendige Stellplätze oder notwendige Abstellplätze für Fahrräder erst innerhalb eines angemessenen Zeitraums nach Fertigstellung der Anlage hergestellt werden.
Sie hat die Herstellung auszusetzen, solange und soweit nachweislich ein Bedarf an notwendigen Stellplätzen oder notwendigen Abstellplätzen für Fahrräder nicht besteht und die für die Herstellung erforderlichen Flächen für diesen Zweck durch Baulast gesichert sind.

#### § 50 Barrierefreies Bauen

(1) In Gebäuden mit mehr als zwei Wohnungen müssen die Wohnungen eines Geschosses barrierefrei erreichbar sein; diese Verpflichtung kann auch durch barrierefrei erreichbare Wohnungen in mehreren Geschossen erfüllt werden.
In diesen Wohnungen müssen die Wohn- und Schlafräume, eine Toilette, ein Bad, der Raum mit den technischen Voraussetzungen für den Einbau einer Küche und, soweit vorhanden, ein Freisitz, wie eine Terrasse, eine Loggia oder ein Balkon, barrierefrei sein.
§ 39 Absatz 4 bleibt unberührt.

(2) Bauliche Anlagen, die überwiegend oder ausschließlich von Menschen mit Behinderungen oder aufgrund von Alter oder Krankheit beeinträchtigten Menschen genutzt werden oder ihrer Betreuung dienen, müssen barrierefrei sein.

(3) Bauliche Anlagen, die öffentlich zugänglich sind, müssen in den dem allgemeinen Besucher- und Benutzerverkehr dienenden Teilen barrierefrei sein.
Dies gilt insbesondere für

1.  Einrichtungen der Kultur und des Bildungswesens,
2.  Sport- und Freizeitstätten,
3.  Einrichtungen des Gesundheitswesens,
4.  Büro-, Verwaltungs- und Gerichtsgebäude,
5.  Verkaufs-, Gast- und Beherbergungsstätten,
6.  Stellplätze, Garagen und Toilettenanlagen.

Für die der zweckentsprechenden Nutzung dienenden Räume und Anlagen genügt es, wenn sie in dem erforderlichen Umfang barrierefrei sind.
Toilettenräume für Besucher und Benutzer müssen in der erforderlichen Anzahl barrierefrei sein.

(4) Bauliche Anlagen nach den Absätzen 2 und 3 müssen eine ausreichende Zahl von Stellplätzen für die Kraftfahrzeuge behinderter Menschen haben.

#### § 51 Sonderbauten

(1) An Sonderbauten können im Einzelfall zur Verwirklichung der allgemeinen Anforderungen nach § 3 Satz 1 besondere Anforderungen gestellt werden.
Erleichterungen können gestattet werden, soweit es der Einhaltung von Vorschriften wegen der besonderen Art oder Nutzung baulicher Anlagen oder Räume oder wegen besonderer Anforderungen nicht bedarf.
Die Anforderungen und Erleichterungen nach den Sätzen 1 und 2 können sich insbesondere erstrecken auf

1.  die Anordnung der baulichen Anlagen auf dem Grundstück,
2.  die Abstände von Nachbargrenzen, von anderen baulichen Anlagen auf dem Grundstück und von öffentlichen Verkehrsflächen sowie auf die Größe der freizuhaltenden Flächen der Grundstücke,
3.  die Öffnungen nach öffentlichen Verkehrsflächen und nach angrenzenden Grundstücken,
4.  die Anlage von Zu- und Abfahrten,
5.  die Anlage von Grünstreifen, Baumpflanzungen und anderen Pflanzungen sowie die Begrünung oder Beseitigung von Halden und Gruben,
6.  die Bauart und Anordnung aller für die Stand- und Verkehrssicherheit, den Brand-, Wärme-, Schall- oder Gesundheitsschutz wesentlichen Bauteile und die Verwendung von Baustoffen,
7.  Brandschutzanlagen, -einrichtungen und –vorkehrungen,
8.  die Löschwasserrückhaltung,
9.  die Anordnung und Herstellung von Aufzügen, Treppen, Treppenräumen, Fluren, Ausgängen und sonstigen Rettungswegen,
10.  die Beleuchtung und Energieversorgung,
11.  die Lüftung und Rauchableitung,
12.  die Feuerungsanlagen und Heizräume,
13.  die Wasserversorgung,
14.  die Aufbewahrung und Entsorgung von Abwasser und festen Abfallstoffen,
15.  die notwendigen Stellplätze und notwendigen Abstellplätze für Fahrräder,
16.  die barrierefreie Nutzbarkeit,
17.  die zulässige Zahl der Benutzer, Anordnung und Zahl der zulässigen Sitz- und Stehplätze bei Versammlungsstätten, Tribünen und Fliegenden Bauten,
18.  die Zahl der Toiletten für Besucher,
19.  Umfang, Inhalt und Zahl besonderer Bauvorlagen, insbesondere eines Brandschutzkonzepts,
20.  weitere zu erbringende Bescheinigungen,
21.  die Bestellung und Qualifikation der Bauleiterin oder des Bauleiters und der Fachbauleiterinnen oder der Fachbauleiter,
22.  den Betrieb und die Nutzung einschließlich der Bestellung und der Qualifikation einer oder eines Brandschutzbeauftragten,
23.  Erst-, Wiederholungs- und Nachprüfungen und die Bescheinigungen, die hierüber zu erbringen sind.

(2) Bei Sonderbauten ist die Richtigkeit der Nachweise der Energieeinsparung sowie zur Nutzung erneuerbarer Energien durch eine Prüfsachverständige oder einen Prüfsachverständigen für energetische Gebäudeplanung zu bescheinigen.

Teil 4  
Die am Bau Beteiligten
-------------------------------

#### § 52 Grundpflichten

Bei der Errichtung, Änderung, Nutzungsänderung und der Beseitigung von Anlagen sind die Bauherrin oder der Bauherr und im Rahmen ihres Wirkungskreises die anderen am Bau Beteiligten dafür verantwortlich, dass die öffentlich-rechtlichen Vorschriften eingehalten werden.

#### § 53 Bauherrin und Bauherr

(1) Die Bauherrin oder der Bauherr hat zur Vorbereitung, Überwachung und Ausführung eines nicht genehmigungsfreien Bauvorhabens sowie der Beseitigung von Anlagen geeignete Beteiligte nach Maßgabe der §§ 54 bis 56 zu bestellen, soweit sie oder er nicht selbst zur Erfüllung der Verpflichtungen nach diesen Vorschriften geeignet ist.
Der Bauherrin oder dem Bauherrn obliegen außerdem die nach den öffentlich-rechtlichen Vorschriften erforderlichen Anträge, Anzeigen und Nachweise, soweit sie nicht von der Konzentrationswirkung der Baugenehmigung erfasst werden.
Sie oder er hat die zur Erfüllung der Anforderungen dieses Gesetzes oder aufgrund dieses Gesetzes erforderlichen Nachweise und Unterlagen zu den verwendeten Bauprodukten und den angewandten Bauarten bereitzuhalten.
Werden Bauprodukte verwendet, die die CE-Kennzeichnung nach der Verordnung (EU) Nr. 305/2011 tragen, ist die Leistungserklärung nach der Verordnung (EU) Nr. 305/2011 bereitzuhalten.
Sie oder er hat vor Baubeginn den Namen der Bauleiterin oder des Bauleiters und während der Bauausführung einen Wechsel dieser Person unverzüglich der Bauaufsichtsbehörde in Textform mitzuteilen.
Wechselt die Bauherrin oder der Bauherr, hat die neue Bauherrin oder der neue Bauherr dies der Bauaufsichtsbehörde unverzüglich in Textform mitzuteilen.

(2) Treten bei einem Bauvorhaben mehrere Personen als Bauherrin oder Bauherr auf, so kann die Bauaufsichtsbehörde verlangen, dass ihr gegenüber eine Vertreterin oder ein Vertreter bestellt wird, die oder der die der Bauherrin oder dem Bauherrn nach den öffentlich-rechtlichen Vorschriften obliegenden Verpflichtungen zu erfüllen hat.
Im Übrigen findet § 18 Absatz 1 Satz 2 und 3 sowie Absatz 2 des Verwaltungsverfahrensgesetzes in Verbindung mit § 1 Absatz 1 Satz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg entsprechende Anwendung mit der Maßgabe, dass eine Erklärung in Textform ausreichend ist.

#### § 54 Entwurfsverfasserin und Entwurfsverfasser

(1) Die Entwurfsverfasserin oder der Entwurfsverfasser muss nach Sachkunde und Erfahrung zur Vorbereitung des jeweiligen Bauvorhabens geeignet sein.
Sie oder er ist für die Vollständigkeit und Brauchbarkeit ihres oder seines Entwurfs verantwortlich.
Die Entwurfsverfasserin oder der Entwurfsverfasser hat dafür zu sorgen, dass die für die Ausführung notwendigen Einzelzeichnungen, Einzelberechnungen und Anweisungen den öffentlich-rechtlichen Vorschriften entsprechen.

(2) Hat die Entwurfsverfasserin oder der Entwurfsverfasser auf einzelnen Fachgebieten nicht die erforderliche Sachkunde und Erfahrung, so sind geeignete Fachplanerinnen oder Fachplaner heranzuziehen.
Diese sind für die von ihnen gefertigten Unterlagen verantwortlich.
Für das ordnungsgemäße Ineinandergreifen aller Fachplanungen bleibt die Entwurfsverfasserin oder der Entwurfsverfasser verantwortlich.

#### § 55 Unternehmerin und Unternehmer

(1) Jede Unternehmerin und jeder Unternehmer ist für die mit den öffentlich-rechtlichen Anforderungen übereinstimmende Ausführung der von ihr oder ihm übernommenen Arbeiten und insoweit für die ordnungsgemäße Einrichtung und den sicheren Betrieb der Baustelle verantwortlich.
Sie oder er hat die zur Erfüllung der Anforderungen dieses Gesetzes oder aufgrund dieses Gesetzes erforderlichen Nachweise und Unterlagen zu den verwendeten Bauprodukten und den angewandten Bauarten zu erbringen und auf der Baustelle bereitzuhalten.
Bei Bauprodukten, die die CE-Kennzeichnung nach der Verordnung (EU) Nr. 305/2011 tragen, ist die Leistungserklärung nach der Verordnung (EU) Nr. 305/2011 bereitzuhalten.

(2) Jede Unternehmerin oder jeder Unternehmer hat auf Verlangen der Bauaufsichtsbehörde für Arbeiten, bei denen die Sicherheit der Anlage in außergewöhnlichem Maße von der besonderen Sachkenntnis und Erfahrung der Unternehmerin oder des Unternehmers oder von einer Ausstattung des Unternehmens mit besonderen Vorrichtungen abhängt, nachzuweisen, dass sie oder er für diese Arbeiten geeignet ist und über die erforderlichen Vorrichtungen verfügt.

#### § 56 Bauleiterin und Bauleiter

(1) Die Bauleiterin oder der Bauleiter hat darüber zu wachen, dass die Baumaßnahme entsprechend den öffentlich-rechtlichen Anforderungen durchgeführt wird und die dafür erforderlichen Weisungen zu erteilen.
Sie oder er hat im Rahmen dieser Aufgabe auf den sicheren bautechnischen Betrieb der Baustelle, insbesondere auf das gefahrlose Ineinandergreifen der Arbeiten der Unternehmer zu achten.
Die Verantwortlichkeit der Unternehmerin oder des Unternehmers bleibt unberührt.

(2) Die Bauleiterin oder der Bauleiter muss über die für ihre oder seine Aufgabe erforderliche Sachkunde und Erfahrung verfügen.
Verfügt sie oder er auf einzelnen Teilgebieten nicht über die erforderliche Sachkunde, so sind geeignete Fachbauleiterinnen oder Fachbauleiter heranzuziehen.
Diese treten insoweit an die Stelle der Bauleiterin oder des Bauleiters.
Die Bauleiterin oder der Bauleiter hat die Tätigkeit der Fachbauleiterin oder des Fachbauleiters und ihre oder seine Tätigkeit aufeinander abzustimmen.

Teil 5  
Bauaufsichtsbehörden, Verfahren
----------------------------------------

### Abschnitt 1  
Bauaufsichtsbehörden

#### § 57 Aufbau und Zuständigkeit der Bauaufsichtsbehörden

(1) Die unteren Bauaufsichtsbehörden sind Sonderordnungsbehörden.
Die Landkreise, die kreisfreien Städte sowie die Großen kreisangehörigen Städte, denen diese Aufgabe übertragen ist, nehmen die Aufgaben der unteren Bauaufsichtsbehörde wahr.
Die Übertragung der Aufgaben der unteren Bauaufsichtsbehörde auf eine Große kreisangehörige Stadt sowie der Widerruf der Übertragung richten sich nach den kommunalrechtlichen Vorschriften.

(2) Die Landrätin oder der Landrat als allgemeine untere Landesbehörde ist Sonderaufsichtsbehörde über

1.  die Großen kreisangehörigen Städte als untere Bauaufsichtsbehörden,
2.  die amtsfreien Gemeinden, die Ämter, die Verbandsgemeinden, die mitverwalteten Gemeinden und mitverwaltenden Gemeinden als Sonderordnungsbehörden, soweit diese nach § 58 Absatz 6 und 7 zuständig sind.

Für die nach § 132 Absatz 5 Satz 1 der Kommunalverfassung des Landes Brandenburg von den Landkreisen zur Verfügung zu stellenden Dienstkräfte gilt Absatz 4 entsprechend.

(3) Oberste Bauaufsichtsbehörde ist das für die Bauaufsicht zuständige Mitglied der Landesregierung.
Es ist Sonderaufsichtsbehörde über die Landkreise und kreisfreien Städte als untere Bauaufsichtsbehörden sowie oberste Sonderaufsichtsbehörde über die Großen kreisangehörigen Städte als untere Bauaufsichtsbehörden und über die amtsfreien Gemeinden, Ämter, Verbandsgemein-den, mitverwalteten Gemeinden und mitverwaltenden Gemeinden als Sonderordnungsbehörden, soweit diese nach § 58 zuständig sind.

(4) Die Bauaufsichtsbehörden sind zur Durchführung ihrer Aufgaben ausreichend mit geeigneten Fachkräften zu besetzen und mit den erforderlichen Vorrichtungen auszustatten.
Den unteren Bauaufsichtsbehörden sollen Bedienstete mit der Befähigung für den höheren technischen Verwaltungsdienst, die die erforderlichen Kenntnisse der Bautechnik, der Baugestaltung und des öffentlichen Baurechts haben, sowie Bedienstete mit der Befähigung zum Richteramt oder zum höheren nicht-technischen Verwaltungsdienst angehören.
Die oberste Bauaufsichtsbehörde kann Ausnahmen zulassen.

(5) Die Befugnis der Sonderaufsichtsbehörde, besondere Weisungen zu erteilen, ist nicht auf den Bereich der Gefahrenabwehr beschränkt.
Für die Sonderaufsichtsbehörde gilt § 121 der Kommunalverfassung des Landes Brandenburg entsprechend.

(6) Die Bestimmungen des Staatshaftungsgesetzes und des § 38 Absatz 1 Buchstabe b des Ordnungsbehördengesetzes finden keine Anwendung.

#### § 58 Aufgaben und Befugnisse der Bauaufsichtsbehörden sowie der amtsfreien Gemeinden, der Ämter, der Verbandsgemeinden, mitverwalteten Gemeinden und mitverwaltenden Gemeinden als Sonderordnungsbehörden

(1) Die Landkreise, die kreisfreien Städte und die Großen kreisangehörigen Städte nehmen die Aufgaben nach diesem Gesetz als Pflichtaufgaben zur Erfüllung nach Weisung wahr.
Für den Vollzug dieses Gesetzes sowie anderer öffentlich-rechtlicher Vorschriften für die Errichtung, die Änderung, die Instandhaltung, die Nutzung oder die Beseitigung baulicher Anlagen sowie anderer Anlagen und Einrichtungen sind die unteren Bauaufsichtsbehörden zuständig, soweit nichts anderes bestimmt ist.

(2) Die Bauaufsichtsbehörde hat bei der Errichtung, der Änderung, der Beseitigung, der Instandhaltung und der Nutzung baulicher Anlagen sowie anderer Anlagen und Einrichtungen darüber zu wachen, dass die öffentlich-rechtlichen Vorschriften und die aufgrund dieser Vorschriften erlassenen Anordnungen eingehalten werden.
Sie haben in Wahrnehmung dieser Aufgaben die erforderlichen Maßnahmen zu treffen und die am Bau Beteiligten zu beraten.
Die Bauaufsichtsbehörde hat nicht die Befugnisse zum Erlass ordnungsbehördlicher Verordnungen nach den §§ 24 bis 37 des Ordnungsbehördengesetzes.

(3) Die Bauaufsichtsbehörde kann im Einvernehmen mit der Bauherrin oder dem Bauherrn zur Erfüllung ihrer Aufgaben Sachverständige und sachverständige Stellen heranziehen.
Sind natürlichen und juristischen Personen des Privatrechts hoheitliche Aufgaben nach diesem Gesetz zur Erledigung in den Handlungsformen des öffentlichen Rechts durch Beleihung nach § 16 des Landesorganisationsgesetzes übertragen, so besteht keine Haftung des Landes oder des Rechtsträgers der Bauaufsichtsbehörde anstelle der natürlichen und juristischen Personen des Privatrechts.

(4) Die mit dem Vollzug dieses Gesetzes beauftragten Personen sind berechtigt, in Ausübung ihres Amtes Grundstücke und bauliche Anlagen einschließlich der Wohnungen zu betreten.
Das Betreten einer Wohnung ist nur zulässig, wenn dies zur Verhütung dringender Gefahren für die öffentliche Sicherheit oder Ordnung erforderlich ist.
Das Grundrecht der Unverletzlichkeit der Wohnung aus Artikel 13 des Grundgesetzes und aus Artikel 15 der Verfassung des Landes Brandenburg wird insoweit eingeschränkt.

(5) Anordnungen der Bauaufsichtsbehörden sind auch gegenüber den Rechtsnachfolgern wirksam.

(6) Die amtsfreien Gemeinden, die Ämter, die Verbandsgemeinden, die mitverwalteten Gemeinden und mitverwaltenden Gemeinden sind als Sonderordnungsbehörden für den Vollzug der örtlichen Bauvorschriften und der planungsrechtlichen Festsetzungen bei genehmigungsfreien Vorhaben zuständig.
Dies gilt insbesondere für die Zulassung von Abweichungen von örtlichen Bauvorschriften sowie die Zulassung von Ausnahmen und Befreiungen nach § 31 des Baugesetzbuchs, die sonderordnungsbehördliche Erlaubnis von Werbeanlagen, die Einstellung von Bauarbeiten, die Nutzungsuntersagung sowie die Beseitigung rechtswidrig errichteter baulicher Anlagen unter entsprechender Anwendung der §§ 78 bis 80, die vorläufige Untersagung nach § 15 Absatz 1 Satz 2 des Baugesetzbuchs binnen einer Frist von einem Monat ab Kenntnis des Vorhabens, und Ordnungswidrigkeitenverfahren.
Bei Vorhaben, die nach § 61 keiner Genehmigung bedürfen, entscheidet die amtsfreie Gemeinde, das Amt, die Verbandsgemeinde, die mitverwaltete Gemeinde oder die mitverwaltende Gemeinde als Sonderordnungsbehörde in einem Erlaubnisverfahren.
Die Erlaubnis ist bei der amtsfreien Gemeinde, dem Amt, der Verbandsgemeinde, der mitverwalteten Gemeinde oder der mitverwaltenden Gemeinde zu beantragen.
Im Übrigen gelten § 67 Absatz 1 Satz 1 und die Verfahrensvorschriften dieses Gesetzes entsprechend.

(7) Die Absätze 2, 4 und 5 gelten für die von den amtsfreien Gemeinden, den Ämtern, den Verbandsgemeinden, den mitverwalteten Gemeinden und den mitverwaltenden Gemeinden als Sonderordnungsbehörden nach diesem Gesetz wahrgenommenen Aufgaben entsprechend.

### Abschnitt 2  
Genehmigungspflicht, Genehmigungsfreiheit

#### § 59 Grundsatz

(1) Die Errichtung, die Änderung und die Nutzungsänderung baulicher Anlagen sowie anderer Anlagen und Einrichtungen, an die in diesem Gesetz oder in Vorschriften aufgrund dieses Gesetzes Anforderungen gestellt sind, bedürfen der Baugenehmigung, soweit in den §§ 60 bis 62, 76 und 77 nichts anderes bestimmt ist.

(2) Die Genehmigungsfreiheit nach Absatz 1, den §§ 60 bis 62, 76 und 77 Absatz 1 Satz 3 sowie die Beschränkung der bauaufsichtlichen Prüfung nach den §§ 63, 64, 66 Absatz 4 und § 77 Absatz 3 entbinden nicht von der Verpflichtung zur Einhaltung der Anforderungen, die durch öffentlich-rechtliche Vorschriften an Anlagen gestellt werden, und lassen die bauaufsichtlichen Eingriffsbefugnisse unberührt.

#### § 60 Vorrang anderer Gestattungsverfahren

Keiner Baugenehmigung, Bauanzeige, Zulassungen von Abweichungen, Zustimmung, Bauzustandsanzeige und Bauüberwachung nach diesem Gesetz bedürfen

1.  Anlagen, die einer Genehmigung nach dem Atomgesetz bedürfen,
2.  Werbeanlagen, soweit sie einer Ausnahmegenehmigung nach Straßenverkehrsrecht oder einer Zulassung nach Straßenrecht bedürfen.

Für Anlagen, die nach Satz 1 keiner Baugenehmigung, Abweichung oder Zustimmung bedürfen, nimmt die für den Vollzug der entsprechenden Rechtsvorschriften zuständige Behörde die Aufgaben und Befugnisse der Bauaufsichtsbehörde wahr.

#### § 61 Genehmigungsfreie Vorhaben

(1) Baugenehmigungsfrei sind:

1.  folgende Gebäude oder bauliche Anlagen:
    1.  Gebäude ohne Aufenthaltsräume, Toiletten oder Feuerstätten mit nicht mehr als 75 Kubikmeter Brutto-Rauminhalt, die nicht im Außenbereich liegen; dies gilt nicht für Garagen, Ställe sowie Gebäude, die Verkaufs- oder Ausstellungszwecken dienen,
    2.  Gebäude ohne Feuerstätten im Außenbereich, die einem land- oder forstwirtschaftlichen Betrieb dienen, nur zum vorübergehenden Schutz von Tieren oder zur Unterbringung von Ernteerzeugnissen oder land- und forstwirtschaftlichen Geräten bestimmt sind, nicht unterkellert sind und nicht mehr als 150 Quadratmeter Grundfläche und nicht mehr als 5 Meter Höhe haben,
    3.  oberirdische Garagen und überdachte Abstellplätze für Fahrräder jeweils mit nicht mehr als einem Geschoss und nicht mehr als 100 Quadratmeter Grundfläche, im Geltungsbereich eines Bebauungsplans nach § 30 Absatz 1 oder Absatz 2 des Baugesetzbuchs,
    4.  Garagen einschließlich überdachte Stellplätze und überdachte Abstellplätze für Fahrräder mit einer mittleren Wandhöhe bis zu 3 Meter und einer Brutto-Grundfläche bis zu 50 Quadratmeter, außer im Außenbereich,
    5.  Gewächshäuser ohne Ausstellungs- und Verkaufsflächen und Folientunnel im Außenbereich mit einer Grundfläche von bis zu 1 600 Quadratmeter, in Landschaftsschutzgebieten bis zu 150 Quadratmeter, und einer Firsthöhe von bis zu 5 Meter, die einem land- oder forstwirtschaftlichen Betrieb oder einem Betrieb der gartenbaulichen Erzeugung im Sinne des § 35 Absatz 1 Nummer 1 und 2 sowie § 201 des Baugesetzbuchs dienen; bei einer Grundfläche von mehr als 150 Quadratmeter sind sie der Gemeinde in Textform zur Kenntnis zu geben; die Gemeinde kann innerhalb einer Frist von vier Wochen erklären, dass ein Baugenehmigungsverfahren durchgeführt werden soll oder eine vorläufige Untersagung gemäß § 15 Absatz 1 Satz 2 des Baugesetzbuchs beantragen; mit der Ausführung des Vorhabens darf bereits vor Ablauf der Frist begonnen werden, wenn die Gemeinde in Textform mitteilt, dass sie von dieser Möglichkeit keinen Gebrauch machen wird,
    6.  Gewächshäuser mit nicht mehr als 50 Kubikmeter Brutto-Rauminhalt, ausgenommen im Außenbereich,
    7.  Wochenendhäuser mit nicht mehr als 50 Quadratmeter Grundfläche und 4 Meter Höhe in durch Bebauungsplan nach § 30 Absatz 1 oder Absatz 2 des Baugesetzbuchs festgesetzten Wochenendhausgebieten oder auf bauaufsichtlich genehmigten Wochenendplätzen,
    8.  Gartenlauben einschließlich überdachten Freisitz mit nicht mehr als 24 Quadratmeter Grundfläche in Dauerkleingartenanlagen nach dem Bundeskleingartengesetz oder bauaufsichtlich genehmigten Kleingartenanlagen,
    9.  einzelne Aufenthaltsräume zu Wohnzwecken im Dachgeschoss von Wohngebäuden der Gebäudeklassen 1 bis 2 mit nicht mehr als zwei Wohnungen, wenn die Konstruktion und die äußere Gestalt des Dachgeschosses nicht verändert werden,
    10.  Terrassenüberdachungen mit einer Fläche bis zu 30 Quadratmeter und einer Tiefe bis zu 4 Meter, außer im Außenbereich,
    11.  vor der Außenwand eines Wohngebäudes aus lichtdurchlässigen Baustoffen errichtete unbeheizte Wintergärten mit nicht mehr als 20 Quadratmeter Grundfläche und 75 Kubikmeter Brutto-Rauminhalt,
    12.  Fahrgastunterstände, die dem öffentlichen Personennahverkehr oder der Schülerbeförderung dienen,
    13.  Schutzhütten, wenn die Hütten jedermann jederzeit zugänglich sind und keine Aufenthaltsräume haben,
2.  Anlagen der technischen Gebäudeausrüstung, ausgenommen freistehende Abgasanlagen mit einer Höhe von mehr als 10 Meter, sowie Anlagen der sicherheitstechnischen Gebäudeausrüstung, sofern eine Rechtsverordnung nach § 86 Absatz 1 Nummer 5 eine Prüfpflicht für diese Anlagen vorschreibt,
3.  folgende Anlagen zur Nutzung erneuerbarer Energien
    1.  Solaranlagen in, an und auf Dach- und Außenwandflächen ausgenommen bei Hochhäusern sowie die damit verbundene Änderung der Nutzung oder der äußeren Gestalt des Gebäudes,
    2.  gebäudeunabhängige Solaranlagen mit einer Höhe bis zu 3 Meter und einer Gesamtlänge bis zu 9 Meter,
    3.  Windenergieanlagen bis zu 10 Meter Höhe gemessen von der Geländeoberfläche bis zum höchsten Punkt der vom Rotor bestrichenen Fläche und einem Rotordurchmesser bis zu 3 Meter außer in reinen Wohngebieten,
4.  folgende Anlagen der Ver- und Entsorgung:
    1.  Brunnen,
    2.  Anlagen, die der Telekommunikation, der öffentlichen Versorgung mit Elektrizität, Gas, Öl oder Wärme dienen, mit einer Höhe bis zu 5 Meter und einer Brutto-Grundfläche bis zu 10 Quadratmeter,
5.  folgende Masten, Antennen und ähnliche Anlagen:
    1.  unbeschadet der Nummer 4 Buchstabe b Antennen einschließlich der Masten mit einer Höhe bis zu 15 Meter auf Gebäuden gemessen ab dem Schnittpunkt der Anlage mit der Dachhaut, im Außenbereich freistehend mit einer Höhe bis zu 20 Meter und zugehöriger Versorgungseinheiten mit einem Brutto-Rauminhalt bis zu 10 Kubikmeter sowie, soweit sie in, auf oder an einer bestehenden baulichen Anlage errichtet werden, die damit verbundene Änderung der Nutzung oder der äußeren Gestalt der Anlage,
    2.  mobile Antennen einschließlich Masten, die zur Verbesserung der Mobilfunknetzabdeckung, im Innenbereich mit einer Höhe bis zu 15 Meter, im Außenbereich bis zu 20 Meter, mit einer maximalen Standzeit von 18 Monaten errichtet werden,
    3.  Masten und Unterstützungen für Fernsprechleitungen, für Leitungen zur Versorgung mit Elektrizität, für Leitungen sonstiger Verkehrsmittel, für Sirenen und für Fahnen,
    4.  Masten, die aus Gründen des Brauchtums errichtet werden,
    5.  Signalhochbauten für die Landesvermessung,
    6.  Flutlichtmasten mit einer Höhe bis zu 10 Meter,
6.  folgende Anlagen, Behälter und Becken:
    1.  Behälter für verflüssigte und nicht verflüssigte Gase mit nicht mehr als 10 Kubikmeter Behälterinhalt,
    2.  Gärfutterbehälter mit nicht mehr als 10 Kubikmeter Behälterinhalt,
    3.  Behälter zur Lagerung von Abwasser, Jauche und Gülle sowie wassergefährdender Stoffe im Sinne von § 62 des Wasserhaushaltsgesetzes mit nicht mehr als 10 Kubikmeter Behälterinhalt,
    4.  Kleinkläranlagen mit einem Abwasseranfall von nicht mehr als 8 Kubikmeter täglich,
    5.  Klärteiche bis zu 100 Quadratmeter Grundfläche und bewachsene Bodenfilter,
    6.  sonstige drucklose Behälter mit nicht mehr als 30 Kubikmeter Behälterinhalt,
    7.  Wasserbecken mit nicht mehr als 100 Kubikmeter Beckeninhalt als Nebenanlage zu einem Wohngebäude,
    8.  Wasserbecken mit nicht mehr als 100 Kubikmeter Beckeninhalt auf bauaufsichtlich genehmigten Camping-und Wochenendplätzen und in festgesetzten Wochenendhausgebieten,
7.  folgende Mauern und Einfriedungen:
    1.  Mauern, einschließlich Stützmauern, und Einfriedungen mit einer Höhe bis zu 2 Meter, außer im Außenbereich,
    2.  offene sockellose Einfriedungen für Grundstücke, die einem land- oder forstwirtschaftlichen Betrieb dienen,
    3.  Wildzäune und andere zum Schutz von Gehölzen und landwirtschaftlichen Kulturen oder Nutztieren vor Schäden durch wildlebende Tiere errichtete Zäune,
    4.  offene sockellose Einfriedungen für Grundstücke mit nicht mehr als 5 000 Quadratmeter Grundfläche, die der Schaf- oder Ziegenhaltung dienen,
8.  private Verkehrsanlagen einschließlich Brücken und Durchlässen mit einer lichten Weite (Fahrbahnbreite) bis zu 5 Meter und Untertunnelungen mit einem Durchmesser bis zu 3 Meter, ausgenommen Wege und Straßen, die nach Anlage 1 des Brandenburgischen Gesetzes über die Umweltverträglichkeitsprüfung einer Vorprüfung oder Umweltverträglichkeitsprüfung bedürfen,
9.  Aufschüttungen und Abgrabungen mit einer Höhe oder Tiefe bis zu 2 Meter und einer Grundfläche bis zu 30 Quadratmeter, im Außenbereich bis zu 300 Quadratmeter,
10.  folgende Anlagen auf Camping- oder Wochenendplätzen, in Gärten und zur Freizeitgestaltung:
    1.  Wohnwagen und Zelte auf bauaufsichtlich genehmigten Campingplätzen,
    2.  bauliche Anlagen, die keine Gebäude sind, auf bauaufsichtlich genehmigten Wochenendplätzen,
    3.  bauliche Anlagen, die der Gartennutzung, der Gartengestaltung oder der zweckentsprechenden Einrichtung von Gärten dienen, wie Bänke, Sitzgruppen, Pergolen oder nicht überdachte Terrassen, ausgenommen Gebäude,
    4.  bauliche Anlagen, die der zweckentsprechenden Einrichtung von Sport- und Spielplätzen dienen, wie Tore für Ballspiele, Schaukeln und Klettergerüste, ausgenommen Gebäude und Tribünen,
    5.  bauliche Anlagen ohne Aufenthaltsräume auf Abenteuerspielplätzen,
    6.  Sprungtürme und Rutschbahnen mit nicht mehr als 10 Meter Höhe in genehmigten Schwimmbädern,
    7.  Schwimmbeckenabdeckungen mit nicht mehr als 100 Quadratmeter Grundfläche,
    8.  Stege in Gewässern, wie Boots- oder Badestege,
11.  folgende tragende und nichttragende Bauteile:
    1.  nichttragende und nichtaussteifende Bauteile in baulichen Anlagen,
    2.  die Änderung tragender oder aussteifender Bauteile innerhalb von Wohngebäuden der Gebäudeklassen 1 und 2,
    3.  Fenster und Türen sowie die dafür bestimmten Öffnungen,
    4.  Außenwandbekleidungen einschließlich Maßnahmen der Wärmedämmung, ausgenommen bei Hochhäusern, Verblendungen und Verputz baulicher Anlagen,
    5.  Bedachung einschließlich Maßnahmen der Wärmedämmung ausgenommen bei Hochhäusern,
12.  folgende Werbeanlagen:
    1.  Werbeanlagen mit einer Ansichtsfläche bis zu 2,5 Quadratmeter,
    2.  Werbeanlagen, die nach ihrem erkennbaren Zweck nur vorübergehend für höchstens zwei Monate angebracht werden, außer im Außenbereich,
    3.  Schilder, die Inhaber und Art gewerblicher Betriebe kennzeichnen (Hinweisschilder), wenn sie vor Ortsdurchfahrten auf einer einzigen Tafel zusammengefasst sind,
    4.  Werbeanlagen in durch Bebauungsplan festgesetzten Gewerbe-, Industrie- und vergleichbaren Sondergebieten an der Stätte der Leistung mit einer Höhe bis zu 10 Metersowie, soweit sie in, auf oder an einer bestehenden baulichen Anlage errichtet werden, die damit verbundene Änderung der Nutzung oder der äußeren Gestalt der Anlage,
13.  folgende vorübergehend aufgestellte oder benutzbare Anlagen:
    1.  Baustelleneinrichtungen einschließlich der Lagerhallen, Schutzhallen und Unterkünfte,
    2.  Gerüste,
    3.  Toilettenwagen,
    4.  Behelfsbauten, die der Landesverteidigung, dem Katastrophenschutz oder der Unfallhilfe dienen,
    5.  bauliche Anlagen, die für höchstens drei Monate auf genehmigtem Messe- und Ausstellungsgelände errichtet werden, ausgenommen Fliegende Bauten,
    6.  Verkaufsstände und andere bauliche Anlagen auf Straßenfesten, Volksfesten und Märkten, ausgenommen Fliegende Bauten,
    7.  ortsveränderlich genutzte und fahrbereit aufgestellte Geflügelställe zum Zweck der Freilandhaltung oder der ökologisch-biologischen Geflügelhaltung, wenn diese einem landwirtschaftlichen Betrieb dienen und jeweils nicht mehr als 500 Kubikmeter Brutto-Rauminhalt sowie eine Auslauffläche haben, die mindestens 7 Quadratmeter je Kubikmeter Brutto-Rauminhalt beträgt,
    8.  Mobile Anlagen, die zur Bewässerung von landwirtschaftlich und gärtnerisch genutzten Flächen dienen, mit allen dazugehörigen ober- und unterirdischen Infrastrukturelementen, einschließlich Pumpen- oder Brunneneinhausungen, Maschinen, nicht auf Dauer angelegten Fundamenten, Leitungen zur Wasserentnahme, Wasserverteilung und Wasserausbringung,
14.  folgende Plätze:
    1.  unbefestigte Lager- und Abstellplätze, die einem land- oder forstwirtschaftlichen Betrieb im Sinne des § 35 Absatz 1 Nummer 1 und 2 sowie § 201 des Baugesetzbuchs dienen,
    2.  nicht überdachte Stellplätze und Abstellplätze für Fahrräder mit einer Fläche bis zu 100 Quadratmeter und deren Zufahrten,
    3.  Kinderspielplätze,
    4.  Ausstellungsplätze und Lagerplätze mit nicht mehr als 200 Quadratmeter Grundfläche, ausgenommen im Außenbereich,
15.  folgende sonstige Anlagen:  
    1.  Zapfsäulen und Tankautomaten genehmigter Tankstellen sowie Ladestationen für Elektromobilität und die damit verbundene Änderung der Nutzung,
    2.  Regale mit einer Höhe bis zu 7,50 Meter Oberkante Lagergut,
    3.  Denkmäler und sonstige Kunstwerke jeweils mit einer Höhe bis zu 4 Meter,
    4.  andere unbedeutende Anlagen oder unbedeutende Teile von Anlagen wie Hauseingangsüberdachungen, Markisen, Rollläden, Terrassen, Maschinenfundamente, Straßenfahrzeugwaagen, Pergolen, Jägerstände, Wildfütterungen, Bienenfreistände, Taubenhäuser, Hofeinfahrten und Teppichstangen.

(2) Genehmigungsfrei ist die Änderung der Nutzung von Anlagen, wenn

1.  für die neue Nutzung keine anderen öffentlich-rechtlichen Anforderungen nach § 64 in Verbindung mit § 66 als für die bisherige Nutzung in Betracht kommen,
2.  die Errichtung oder Änderung nach Absatz 1 genehmigungsfrei wäre.

(3) Keiner Baugenehmigung bedürfen Instandhaltungsarbeiten.

#### § 62 Bauanzeigeverfahren

(1) Für die Errichtung und Änderung von Wohngebäuden der Gebäudeklassen 1 und 2, einschließlich der zugehörigen notwendigen Stellplätze, notwendigen Abstellplätze für Fahrräder, Garagen, Nebengebäude und Nebenanlagen im Geltungsbereich eines rechtswirksamen Bebauungsplans nach § 30 Absatz 1 oder Absatz 2 des Baugesetzbuchs wird abweichend von den §§ 63 und 64 auf Wunsch der Bauherrin oder des Bauherrn ein Bauanzeigeverfahren durchgeführt, wenn das Vorhaben den Festsetzungen des Bebauungsplans nicht widerspricht und die Erschließung gesichert ist.

(2) Die Bauaufsichtsbehörde hat der Bauherrin oder dem Bauherrn binnen einer Woche den Tag des Eingangs der Bauanzeige bei der Bauaufsichtsbehörde zu bestätigen.

(3) Mit der Bauausführung darf nach Ablauf eines Monats nach Eingang der Bauanzeige bei der Bauaufsichtsbehörde begonnen werden, sofern die Bauaufsichtsbehörde die Bauausführung nicht untersagt oder vorher freigegeben hat.
Die Berechtigung zur Bauausführung erlischt nach vier Jahren.
Die Berechtigung zur Bauausführung erlischt nicht, wenn das Vorhaben innerhalb der Frist nach Satz 2 begonnen worden und spätestens ein Jahr nach Ablauf der Frist fertig gestellt ist.

(4) Die Bauausführung ist zu untersagen, wenn

1.  die Voraussetzungen des Absatzes 1 nicht vorliegen,
2.  die Bauanzeige, die Bauvorlagen oder Nachweise nicht vollständig oder unrichtig sind,
3.  die Voraussetzungen des § 14 oder § 15 des Baugesetzbuchs vorliegen.

Die Untersagungsgründe sind im Einzelnen zu benennen und der Bauherrin oder dem Bauherrn innerhalb der Frist nach Absatz 3 Satz 1 bekannt zu geben.
Widerspruch und Anfechtungsklage gegen die Untersagung haben keine aufschiebende Wirkung.

(5) Im Übrigen gelten § 63 Absatz 2 und die Verfahrensvorschriften dieses Gesetzes entsprechend.

### Abschnitt 3  
Genehmigungsverfahren

#### § 63 Vereinfachtes Baugenehmigungsverfahren

(1) Für die Errichtung und Änderung von Wohngebäuden der Gebäudeklasse 1 bis 3 einschließlich der zugehörigen notwendigen Stellplätze, notwendigen Abstellplätze für Fahrräder, ihrer Garagen, Nebengebäude und Nebenanlagen, im Geltungsbereich eines rechtswirksamen Bebauungsplans nach § 30 Absatz 1 oder Absatz 2 des Baugesetzbuchs wird abweichend von § 64 auf Antrag der Bauherrin oder des Bauherrn ein vereinfachtes Baugenehmigungsverfahren durchgeführt, wenn das Vorhaben den Festsetzungen des Bebauungsplans nicht widerspricht und die Erschließung gesichert ist.

(2) Die Bauherrin oder der Bauherr hat mit dem vollständigen Bauantrag die Erklärung der Entwurfsverfasserin oder des Entwurfsverfassers vorzulegen, dass für das Vorhaben die Zulassung von Ausnahmen oder Befreiungen nach § 31 des Baugesetzbuchs sowie von Abweichungen nach § 67 nicht erforderlich ist und das Vorhaben im Übrigen den öffentlich-rechtlichen Vorschriften entspricht.

(3) Die Bauaufsichtsbehörde prüft die Beachtung

1.  der Festsetzungen des Bebauungsplans,
2.  anderer öffentlich-rechtlicher Vorschriften, soweit diese für das Vorhaben beachtlich sind.

(4) Über den Bauantrag ist innerhalb von drei Monaten nach Vorlage der vollständigen Antragsunterlagen zu entscheiden; die Bauaufsichtsbehörde kann diese Frist gegenüber der Bauherrin oder dem Bauherrn aus wichtigem Grund um bis zu zwei Monate verlängern.
Der Antrag gilt als genehmigt, wenn über ihn nicht innerhalb der nach Satz 1 maßgeblichen Frist entschieden worden ist.

#### § 64 Baugenehmigungsverfahren

Bei genehmigungspflichtigen Anlagen prüft die Bauaufsichtsbehörde die Zulässigkeit nach

1.  den Vorschriften des Baugesetzbuchs,
2.  den Vorschriften dieses Gesetzes und aufgrund dieses Gesetzes,
3.  anderen öffentlich-rechtlichen Vorschriften, soweit diese für das Vorhaben beachtlich sind.

#### § 65 Bauvorlageberechtigung

(1) Bauvorlagen für die nicht genehmigungsfreie Errichtung und Änderung von Gebäuden müssen von einer Entwurfsverfasserin oder einem Entwurfsverfasser erstellt sein, die oder der bauvorlageberechtigt ist.
Dies gilt nicht für Bauvorlagen für geringfügige oder technisch einfache Bauvorhaben, die üblicherweise von Fachkräften mit anderer Ausbildung als nach Absatz 2, insbesondere Handwerksmeisterinnen und Handwerksmeister des Maurer-, Betonbauer- und Zimmererfachs sowie staatlich geprüften Technikerinnen und Technikern der Fachrichtung Bautechnik mit dem Schwerpunkt Hochbau, verfasst werden.
Als geringfügig oder technisch einfache Bauvorhaben gelten:

1.  freistehende Gebäude bis 100 Quadratmeter Grundfläche und mit nicht mehr als zwei Geschossen,
2.  Gebäude ohne Aufenthaltsräume bis 150 Quadratmeter Grundfläche und mit nicht mehr als zwei Geschossen, wie zum Beispiel Nebengebäude, Garagen und Carports,
3.  land- und forstwirtschaftlich genutzte Gebäude mit bis zu zwei oberirdischen Geschossen und bis 250 Quadratmeter Grundfläche,
4.  einfache Änderungen an sonstigen Gebäuden, wie zum Beispiel der Anbau von Wintergärten sowie Terrassen- und Balkonüberdachungen bis 50 Quadratmeter Grundfläche,
5.  bei Gebäuden der Gebäudeklassen 1 und 2 die Errichtung von Dachgauben, Änderungen an der Dachkonstruktion im Rahmen von Sanierungs- und Instandhaltungsmaßnahmen.

(2) Bauvorlageberechtigt ist, wer

1.  die Berufsbezeichnung „Architektin“ oder „Architekt“ führen darf,
2.  in die von der Brandenburgischen Ingenieurkammer geführte Liste mit dem Zusatz Bauvorlageberechtigung eingetragen ist; Eintragungen anderer Länder der Bundesrepublik Deutschland gelten auch im Land Brandenburg;
3.  die Berufsbezeichnung „Innenarchitektin“ oder „Innenarchitekt“ führen darf, für die mit der Berufsaufgabe der Innenarchitektin oder des Innenarchitekten verbundenen baulichen Änderungen von Gebäuden oder
4.  einen berufsqualifizierenden Hochschulabschluss eines Studiums der Fachrichtung Architektur, Hochbau oder des Bauingenieurwesens nachweist, danach mindestens zwei Jahre auf dem Gebiet der Entwurfsplanung von Gebäuden praktisch tätig gewesen ist und Bedienstete oder Bediensteter einer juristischen Person des öffentlichen Rechts ist, für die dienstliche Tätigkeit.

(3) In die Liste nach Absatz 2 Nummer 2 ist auf Antrag von der Brandenburgischen Ingenieurkammer einzutragen, wer

1.  einen berufsqualifizierenden Hochschulabschluss eines Studiums der Fachrichtung Hochbau oder des Bauingenieurwesens nachweist und
2.  danach mindestens zwei Jahre auf dem Gebiet der Entwurfsplanung von Gebäuden praktisch tätig gewesen ist.

Dem Antrag sind die zur Beurteilung erforderlichen Unterlagen beizufügen.
Die Brandenburgische Ingenieurkammer bestätigt unverzüglich den Eingang der Unterlagen und teilt gegebenenfalls mit, welche Unterlagen fehlen.
Die Eingangsbestätigung muss folgende Angaben enthalten:

1.  die in Satz 5 genannte Frist,
2.  die verfügbaren Rechtsbehelfe,
3.  die Erklärung, dass der Antrag als genehmigt gilt, wenn über ihn nicht rechtzeitig entschieden wird und
4.  im Fall der Nachforderung von Unterlagen die Mitteilung, dass die Frist nach Satz 5 erst beginnt, wenn die Unterlagen vollständig sind.

Über den Antrag ist innerhalb von drei Monaten nach Vorlage der vollständigen Unterlagen zu entscheiden; die Brandenburgische Ingenieurkammer kann die Frist gegenüber der Antragstellerin oder dem Antragsteller einmal um bis zu zwei Monate verlängern.
Die Fristverlängerung und deren Ende sind ausreichend zu begründen und der Antragstellerin oder dem Antragsteller vor Ablauf der ursprünglichen Frist mitzuteilen.
Der Antrag gilt als genehmigt, wenn über ihn nicht innerhalb der nach Satz 5 maßgeblichen Frist entschieden worden ist.

(4) Personen, die in einem anderen Mitgliedstaat der Europäischen Union oder einem nach dem Recht der Europäischen Gemeinschaften gleichgestellten Staat als Bauvorlageberechtigte niedergelassen sind, sind ohne Eintragung in die Liste nach Absatz 2 Nummer 2 bauvorlageberechtigt, wenn sie

1.  eine vergleichbare Berechtigung besitzen und
2.  dafür dem Absatz 3 Satz 1 vergleichbare Anforderungen erfüllen mussten.

Sie haben das erstmalige Tätigwerden als Bauvorlageberechtigte oder Bauvorlageberechtigter vorher der Brandenburgischen Ingenieurkammer anzuzeigen und dabei

1.  eine Bescheinigung darüber, dass sie in einem Mitgliedstaat der Europäischen Union oder einem nach dem Recht der Europäischen Gemeinschaften gleichgestellten Staat rechtmäßig als Bauvorlageberechtigte niedergelassen sind und ihnen die Ausübung dieser Tätigkeiten zum Zeitpunkt der Vorlage der Bescheinigung nicht, auch nicht vorübergehend, untersagt ist, und
2.  einen Nachweis darüber, dass sie im Staat ihrer Niederlassung für die Tätigkeit als Bauvorlageberechtigte oder Bauvorlageberechtigter mindestens die Voraussetzungen des Absatzes 3 Satz 1 erfüllen mussten,

vorzulegen; sie sind in einem Verzeichnis zu führen.
Die Brandenburgische Ingenieurkammer hat auf Antrag zu bestätigen, dass die Anzeige nach Satz 2 erfolgt ist; sie kann das Tätigwerden als Bauvorlageberechtigte oder Bauvorlageberechtigter untersagen und die Eintragung in dem Verzeichnis nach Satz 2 löschen, wenn die Voraussetzungen des Satzes 1 nicht erfüllt sind.

(5) Personen, die in einem anderen Mitgliedstaat der Europäischen Union oder einem nach dem Recht der Europäischen Gemeinschaften gleichgestellten Staat als Bauvorlageberechtigte niedergelassen sind, ohne im Sinne des Absatzes 4 Satz 1 Nummer 2 vergleichbar zu sein, sind bauvorlageberechtigt, wenn ihnen die Brandenburgische Ingenieurkammer bescheinigt hat, dass sie die Anforderungen des Absatzes 3 Satz 1 erfüllen; sie sind in einem Verzeichnis zu führen.
Die Bescheinigung wird auf Antrag erteilt.
Absatz 3 Satz 2 bis 7 ist entsprechend anzuwenden.

(6) Anzeigen und Bescheinigungen nach den Absätzen 4 und 5 sind nicht erforderlich, wenn bereits in einem anderen Land der Bundesrepublik Deutschland eine Anzeige erfolgt ist oder eine Bescheinigung erteilt wurde; eine weitere Eintragung in die von der Brandenburgischen Ingenieurkammer geführten Verzeichnisse erfolgt nicht.
Verfahren nach den Absätzen 3 bis 5 können auch über den einheitlichen Ansprechpartner im Sinne des Gesetzes über den Einheitlichen Ansprechpartner für das Land Brandenburg vom 7.
Juli 2009 (GVBl.
I S. 262), das zuletzt durch Artikel 2 des Gesetzes vom 17. Dezember 2015 (GVBl.
I Nr. 38 S. 4) geändert worden ist, als einheitliche Stelle im Sinne der §§ 71a bis 71e des Verwaltungsverfahrensgesetzes in Verbindung mit § 1 Absatz 1 Satz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg abgewickelt werden.

(7) Das Grundrecht auf Berufsfreiheit (Artikel 49 der Verfassung des Landes Brandenburg) wird insoweit eingeschränkt.

#### § 66 Bautechnische Nachweise

(1) Die Einhaltung der Anforderungen an die Standsicherheit, den Brand-, Schall- und Erschütterungsschutz ist nach näherer Maßgabe der Verordnung aufgrund § 86 Absatz 3 nachzuweisen (bautechnische Nachweise); dies gilt nicht für genehmigungsfreie Bauvorhaben, einschließlich der Beseitigung von Anlagen, soweit nicht in diesem Gesetz oder in der Rechtsverordnung aufgrund § 86 Absatz 3 anderes bestimmt ist.
Die Bauvorlageberechtigung nach § 65 Absatz 2 Nummer 1, 2 und 4 schließt die Berechtigung zur Erstellung der bautechnischen Nachweise ein, soweit nicht nachfolgend Abweichendes bestimmt ist.

(2) Bei

1.  Gebäuden der Gebäudeklassen 1 bis 3,
2.  sonstigen baulichen Anlagen, die keine Gebäude sind,

muss der Standsicherheitsnachweis von einer Person mit einem berufsqualifizierenden Hochschulabschluss eines Studiums der Fachrichtung Architektur, Hochbau oder des Bauingenieurwesens mit einer mindestens dreijährigen Berufserfahrung in der Tragwerksplanung, die oder der unter Beachtung des § 65 Absatz 3 Satz 2 bis 7 in eine Liste mit dem Zusatz Tragwerksplanung eingetragen ist; oder einer Prüfingenieurin oder einem Prüfingenieur für Standsicherheit erstellt sein; Eintragungen anderer Länder der Bundesrepublik Deutschland gelten auch im Land Brandenburg.
Auch bei anderen Bauvorhaben darf der Standsicherheitsnachweis von einer Tragwerksplanerin oder einem Tragwerksplaner nach Satz 1 erstellt werden.
Bei Bauvorhaben der Gebäudeklasse 4, ausgenommen Sonderbauten sowie Mittel- und Großgaragen im Sinne der Verordnung nach § 86 Absatz 1 Nummer 3, muss der Brandschutznachweis erstellt sein von

1.  einer oder einem für das Bauvorhaben Bauvorlageberechtigten, die oder der die erforderlichen Kenntnisse des Brandschutzes nachgewiesen hat,
2.  einer Angehörigen oder einem Angehörigen der Fachrichtung Architektur, Hochbau, Bauingenieurwesen oder eines Studiengangs mit Schwerpunkt Brandschutz, die oder der ein Studium an einer deutschen Hochschule oder ein gleichwertiges Studium an einer ausländischen Hochschule abgeschlossen hat und nach Abschluss der Ausbildung mindestens zwei Jahre auf dem Gebiet der brandschutztechnischen Planung und Ausführung von Gebäuden oder deren Prüfung praktisch tätig gewesen ist und die erforderlichen Kenntnisse des Brandschutzes nachgewiesen hat,
3.  einer Absolventin oder einem Absolventen einer Ausbildung für mindestens den gehobenen feuerwehrtechnischen Dienst, die oder der nach Abschluss der Ausbildung mindestens zwei Jahre auf dem Gebiet der brandschutztechnischen Planung und Ausführung von Gebäuden oder deren Prüfung praktisch tätig gewesen ist und die erforderlichen Kenntnisse des Brandschutzes nachgewiesen hat, oder
4.  einer Prüfingenieurin oder einem Prüfingenieur für Brandschutz.

Die oder der Nachweisberechtigte nach Satz 3 Nummer 1 bis 3 muss unter Beachtung des § 65 Absatz 3 Satz 2 bis 7 in eine Liste mit dem Zusatz Brandschutzplanung eingetragen sein; Eintragungen anderer Länder der Bundesrepublik Deutschland gelten auch im Land Brandenburg.
Auch bei anderen Bauvorhaben darf der Brandschutznachweis von einer Brandschutzplanerin oder einem Brandschutzplaner nach Satz 3 erstellt werden.
Für Personen, die in einem anderen Mitgliedstaat der Europäischen Union oder einem nach dem Recht der Europäischen Gemeinschaften gleichgestellten Staat zur Erstellung von Standsicherheits- oder Brandschutznachweisen niedergelassen sind, gilt § 65 Absatz 4 bis 6 mit der Maßgabe entsprechend, dass die Anzeige oder der Antrag auf Erteilung einer Bescheinigung bei der nach Absatz 5 zuständigen Stelle einzureichen ist.
Bei Bediensteten einer juristischen Person des öffentlichen Rechts ist für die dienstliche Tätigkeit eine Eintragung in die Listen abweichend von den Sätzen 1 und 3 nicht erforderlich, wenn sie ansonsten die Voraussetzungen nach diesem Absatz erfüllen.

(3) Bei

1.  Gebäuden der Gebäudeklassen 4 und 5,
2.  wenn dies nach Maßgabe eines in der Rechtsverordnung nach § 86 Absatz 3 geregelten Kriterienkatalogs erforderlich ist, bei
    1.  Gebäuden der Gebäudeklassen 1 bis 3,
    2.  Behältern, Brücken, Stützmauern, Tribünen,
    3.  sonstigen baulichen Anlagen, die keine Gebäude sind, mit einer Höhe von mehr als 10 Meter

muss der Standsicherheitsnachweis bauaufsichtlich geprüft sein; das gilt nicht für Wohngebäude der Gebäudeklassen 1 und 2.
Bei

1.  Sonderbauten,
2.  Mittel- und Großgaragen im Sinne der Verordnung nach § 86 Absatz 1 Nummer 3,
3.  Gebäuden der Gebäudeklasse 5

muss der Brandschutznachweis bauaufsichtlich geprüft sein.
Soweit abweichend von Absatz 2 Satz 1 und 3 die Erstellerin oder der Ersteller eines Standsicherheits- oder Brandschutznachweises nicht in die Liste nach Absatz 5 eingetragen ist, müssen diese bautechnischen Nachweise bauaufsichtlich geprüft sein.
Die Vollständigkeit und Richtigkeit der bautechnischen Nachweise ist durch einen Prüfbericht zu bestätigen.
Die Prüfberichte über die Prüfung der Brandschutznachweise müssen der Bauaufsichtsbehörde vor Erteilung der Baugenehmigung vorliegen.
Die übrigen Prüfberichte müssen der Bauaufsichtsbehörde vor Baubeginn vorliegen.

(4) Außer in den Fällen des Absatzes 3 werden bautechnische Nachweise nicht geprüft; § 67 bleibt unberührt.
Werden bautechnische Nachweise durch eine Prüfingenieurin oder einen Prüfingenieur geprüft, werden die entsprechenden Anforderungen auch in den Fällen des § 67 bauaufsichtlich nicht mehr geprüft.
Einer bauaufsichtlichen Prüfung oder einer Prüfung durch eine Prüfingenieurin oder einen Prüfingenieur bedarf es ferner nicht, soweit für das Bauvorhaben Standsicherheitsnachweise vorliegen, die von einem Prüfamt für Standsicherheit allgemein geprüft sind (Typenprüfung); Typenprüfungen anderer Länder der Bundesrepublik Deutschland gelten auch im Land Brandenburg.

(5) Die in Absatz 2 genannten Listen mit den entsprechenden Zusätzen werden von der Brandenburgischen Architektenkammer und der Brandenburgischen Ingenieurkammer gemeinsam geführt.
Die §§ 5 und 6 des Brandenburgischen Architektengesetzes und die §§ 5 und 6 des Brandenburgischen Ingenieurgesetzes gelten entsprechend.

(6) Das Grundrecht auf Berufsfreiheit (Artikel 49 der Verfassung des Landes Brandenburg) wird insoweit eingeschränkt.

#### § 67 Abweichungen

(1) Die Bauaufsichtsbehörde kann Abweichungen von Anforderungen dieses Gesetzes und aufgrund dieses Gesetzes erlassener Vorschriften zulassen, wenn sie unter Berücksichtigung des Zwecks der jeweiligen Anforderung und unter Würdigung der öffentlich-rechtlich geschützten nachbarlichen Belange mit den öffentlichen Belangen, insbesondere den Anforderungen des § 3 Satz 1, vereinbar sind.
§ 86a Absatz 1 Satz 3 bleibt unberührt; der Zulassung einer Abweichung bedarf es auch nicht, wenn bautechnische Nachweise durch eine Prüfingenieurin oder einen Prüfingenieur geprüft werden, es sei denn, öffentlich-rechtlich geschützte nachbarliche Belange werden berührt.

(2) Die Zulassung von Abweichungen nach Absatz 1, von Ausnahmen und Befreiungen von den Festsetzungen eines Bebauungsplans oder einer sonstigen städtebaulichen Satzung oder von Regelungen der Baunutzungsverordnung ist gesondert in Textform zu beantragen; der Antrag ist zu begründen.
Für Anlagen, die keiner Genehmigung bedürfen, sowie für Abweichungen von Vorschriften, die im Genehmigungsverfahren nicht geprüft werden, gilt Satz 1 entsprechend.

(3) Zu Abweichungen von örtlichen Bauvorschriften nach § 87 ist das Einvernehmen der Gemeinde erforderlich.
§ 36 Absatz 2 Satz 2 des Baugesetzbuchs gilt entsprechend.

(4) Über Abweichungen nach Absatz 1 Satz 1 von örtlichen Bauvorschriften sowie über Ausnahmen und Befreiungen nach Absatz 2 Satz 1 entscheidet bei genehmigungsfreien Bauvorhaben die amtsfreie Gemeinde oder das Amt als Sonderordnungsbehörde nach Maßgabe der Absätze 1 und 2.

#### § 68 Bauantrag, Bauvorlagen

(1) Die Bauaufsichtsbehörde entscheidet in allen bauaufsichtlichen Genehmigungsverfahren nur auf Antrag der Bauherrin oder des Bauherrn (Bauantrag).
Der Bauantrag ist bei der Bauaufsichtsbehörde einzureichen.

(2) Mit dem Bauantrag sind alle für die Beurteilung des Bauvorhabens und die Bearbeitung des Bauantrags erforderlichen Unterlagen (Bauvorlagen) einzureichen.
Die Bauaufsichtsbehörde kann gestatten, dass einzelne Bauvorlagen nachgereicht werden.

(3) In besonderen Fällen kann zur Beurteilung der Einwirkung der baulichen Anlagen auf die Umgebung verlangt werden, dass die bauliche Anlage in geeigneter Weise auf dem Grundstück dargestellt wird.

(4) Ist die Bauherrin oder der Bauherr nicht Grundstückseigentümerin oder Grundstückseigentümer, so kann die Zustimmung der Grundstückseigentümerin oder des Grundstückseigentümers zu dem Bauvorhaben gefordert werden.

#### § 69 Behandlung des Bauantrags

(1) Die Bauaufsichtsbehörde hat binnen zwei Wochen nach Eingang des Bauantrags zu prüfen, ob die Bauvorlagen vollständig sind und den Eingang des Bauantrags zu bestätigen.

(2) Ist der Bauantrag unvollständig oder weist er sonstige erhebliche Mängel auf, fordert die Bauaufsichtsbehörde die Bauherrin oder den Bauherrn mit der Eingangsbestätigung zur Behebung der Mängel innerhalb einer angemessenen Frist auf.
Werden die Mängel nicht innerhalb der Frist behoben, gilt der Antrag als zurückgenommen.

(3) Sind die Bauvorlagen vollständig, holt die Bauaufsichtsbehörde unverzüglich die Stellungnahmen der Behörden und Stellen ein, deren Zustimmung, Einvernehmen oder Benehmen zur Baugenehmigung erforderlich ist oder deren Aufgabenbereich durch das Vorhaben berührt wird.
Soweit die Baugenehmigung die Entscheidung einer anderen Behörde einschließt, ist, vorbehaltlich einer anderen gesetzlichen Regelung, deren Benehmen zur Erteilung der Baugenehmigung erforderlich.

(4) Soweit bundesrechtliche Vorschriften keine längeren Fristen vorsehen, sind die Stellungnahmen der beteiligten Behörden und Stellen innerhalb eines Monats, in den Fällen der §§ 62 und 63 innerhalb von zwei Wochen, nach Zugang des Ersuchens abzugeben.
Geht die Stellungnahme nicht innerhalb dieser Frist ein, so soll die Bauaufsichtsbehörde davon ausgehen, dass die von den Behörden und Stellen wahrzunehmenden öffentlichen Belange der Erteilung der Baugenehmigung nicht entgegenstehen.
Dies gilt entsprechend, wenn die nach bundesrechtlichen Vorschriften zu beachtende Frist nicht eingehalten wird.
Die Frist nach Satz 1 geht anderen landesrechtlich geregelten Fristen vor.

(5) Eine gemeinsame Besprechung mit den am Verfahren zu beteiligenden Behörden und Stellen soll durchgeführt werden, wenn dies der beschleunigten Abwicklung des Verfahrens dienlich ist.

(6) Die Bauaufsichtsbehörde entscheidet über den Bauantrag innerhalb einer Frist von einem Monat nach Eingang aller Stellungnahmen.

#### § 70 Beteiligung der Nachbarn und der Öffentlichkeit

(1) Nachbarn sind die Eigentümer oder die Erbbauberechtigten der an das Baugrundstück angrenzenden Grundstücke.

(2) Vor der Zulassung von Abweichungen nach § 67 und vor der Erteilung von Befreiungen nach § 31 Absatz 2 des Baugesetzbuchs, die öffentlich-rechtlich geschützte nachbarliche Belange berühren können, hat die Bauaufsichtsbehörde die betroffenen Nachbarn von dem Vorhaben zu benachrichtigen und ihnen Gelegenheit zur Stellungnahme innerhalb von zwei Wochen zu geben.
Die Bauherrin oder der Bauherr hat auf Verlangen der Bauaufsichtsbehörde Unterlagen zu deren Beteiligung zur Verfügung zu stellen.

(3) Die Benachrichtigung entfällt, wenn die Nachbarin oder der Nachbar dem Vorhaben, der Zulassung der Abweichung oder der Erteilung der Befreiung in Textform zugestimmt oder die Zustimmung bereits in Textform gegenüber der Bauaufsichtsbehörde verweigert hat.

(4) Die Nachbarin oder der Nachbar hat das Recht, die von der Bauherrin oder vom Bauherrn eingereichten Bauvorlagen bei der Bauaufsichtsbehörde einzusehen.

(5) Hat eine Nachbarin oder ein Nachbar oder eine von der Bauaufsichtsbehörde hinzugezogene Verfahrensbeteiligte oder ein von der Bauaufsichtsbehörde hinzugezogener Verfahrensbeteiligter nicht Stellung genommen oder wird deren Einwendungen nicht entsprochen, so ist ihnen eine Ausfertigung der Baugenehmigung oder der Entscheidung über die Abweichung oder Befreiung zuzustellen.
Bei mehr als 20 Nachbarinnen oder Nachbarn, denen die Baugenehmigung zuzustellen ist, kann die Zustellung nach Satz 1 durch öffentliche Bekanntmachung ersetzt werden; die Bekanntmachung hat den verfügenden Teil der Baugenehmigung, die Rechtsbehelfsbelehrung sowie einen Hinweis darauf zu enthalten, wo die Akten des Baugenehmigungsverfahrens eingesehen werden können.
Sie ist in ortsüblicher Weise bekannt zu machen.
Die Zustellung gilt mit dem Tag der Bekanntmachung als bewirkt.

(6) Bei baulichen Anlagen, die aufgrund ihrer Beschaffenheit oder ihres Betriebs geeignet sind, die Allgemeinheit oder die Nachbarschaft zu gefährden, zu benachteiligen oder zu belästigen, kann die Bauaufsichtsbehörde auf Antrag der Bauherrin oder des Bauherrn das Bauvorhaben ortsüblich bekannt machen; Absatz 2 findet insoweit keine Anwendung.
Öffentlich-rechtliche Einwendungen gegen das Bauvorhaben bleiben im Verwaltungsverfahren unberücksichtigt, wenn sie nicht innerhalb eines Monats nach der Bekanntmachung bei der Bauaufsichtsbehörde eingehen.
Die Zustellung der Baugenehmigung nach Absatz 5 Satz 1 kann durch öffentliche Bekanntmachung ersetzt werden; Absatz 5 Satz 4 sowie Satz 1 gelten entsprechend.
In der Bekanntmachung nach Satz 1 ist darauf hinzuweisen,

1.  wo und wann die Akten des Verfahrens eingesehen werden können,
2.  wo und wann Einwendungen gegen das Bauvorhaben vorgebracht werden können,
3.  welche Rechtsfolgen mit Ablauf der Frist des Satzes 2 eintreten und
4.  dass die Zustellung der Baugenehmigung durch öffentliche Bekanntmachung ersetzt werden kann.

(7) Bei der Errichtung, Änderung oder Nutzungsänderung

1.  eines oder mehrerer Gebäude, wenn dadurch dem Wohnen dienende Nutzungseinheiten mit einer Größe von insgesamt mehr als 5 000 Quadratmeter Brutto-Grundfläche geschaffen werden,
2.  baulicher Anlagen, die öffentlich zugänglich sind, wenn dadurch die gleichzeitige Nutzung durch mehr als 100 zusätzliche Besucher ermöglicht wird, und
3.  baulicher Anlagen, die nach Durchführung des Bauvorhabens Sonderbauten nach § 2 Absatz 4 Nummer 9 Buchstabe c, Nummer 10 bis 13, 15 und 16 sind,

ist eine Öffentlichkeitsbeteiligung nach den hierfür geltenden immissionsschutzrechtlichen Bestimmungen durchzuführen, wenn sie innerhalb eines Sicherheitsabstands eines Betriebsbereichs im Sinne des § 3 Absatz 5a des Bundes-Immissionsschutzgesetzes liegen, es sei denn, die Immissionsschutzbehörde hat bestätigt, dass sich das Vorhaben außerhalb des angemessenen Sicherheitsabstands des Betriebsbereichs befindet.

(8) Im Übrigen gelten für die Beteiligung im bauaufsichtlichen Verfahren die Vorschriften des Verwaltungsverfahrensgesetzes in Verbindung mit den Vorschriften des Verwaltungsverfahrensgesetzes für das Land Brandenburg.

#### § 71 Ersetzung des gemeindlichen Einvernehmens

(1) Hat eine Gemeinde ihr nach den Vorschriften dieses Gesetzes oder des Baugesetzbuchs erforderliches Einvernehmen rechtswidrig versagt, soll die Bauaufsichtsbehörde das fehlende Einvernehmen der Gemeinde ersetzen; in den Fällen des § 36 Absatz 1 Satz 1 und 2 des Baugesetzbuchs ist das fehlende Einvernehmen zu ersetzen.
Wird in einem anderen Genehmigungsverfahren über die Zulässigkeit des Vorhabens entschieden, so tritt die für dieses Verfahren zuständige Behörde an die Stelle der Bauaufsichtsbehörde.

(2) Die Gemeinde ist vor Ersetzung des Einvernehmens anzuhören.
Dabei ist ihr Gelegenheit zu geben, binnen einer Frist von einem Monat erneut über das gemeindliche Einvernehmen zu entscheiden.

(3) Die Genehmigung, mit der die Zulässigkeit des Vorhabens festgestellt wird, gilt zugleich als Ersatzvornahme im Sinne des § 116 der Kommunalverfassung des Landes Brandenburg.
Sie ist insoweit zu begründen.
Widerspruch und Anfechtungsklage der Gemeinde haben auch insoweit keine aufschiebende Wirkung, als die Genehmigung als Ersatzvornahme gilt.

(4) Abweichend von § 119 der Kommunalverfassung des Landes Brandenburg kann die Gemeinde die Ersetzung des Einvernehmens durch Widerspruch gegen die Genehmigung anfechten.

(5) Die Absätze 1 bis 3 gelten entsprechend für das Widerspruchsverfahren.

#### § 72 Baugenehmigung, Baubeginn

(1) Die Baugenehmigung ist zu erteilen, wenn dem Vorhaben keine öffentlich-rechtlichen Vorschriften entgegenstehen.
Die Baugenehmigung schließt die für das Vorhaben erforderlichen weiteren behördlichen Entscheidungen ein.
Die Erlaubnis nach § 18 der Betriebssicherheitsverordnung und die Genehmigung der oberen Wasserbehörde zum Bau und Betrieb von Abwasserbehandlungsanlagen schließen eine Baugenehmigung mit ein.
Satz 2 gilt nicht für wasserrechtliche Entscheidungen über betriebsbedingte Gewässerbenutzungen, für Entscheidungen in Selbstverwaltungsangelegenheiten der Gemeinden und Gemeindeverbände sowie für Entscheidungen in Planfeststellungs- oder Plangenehmigungsverfahren.
Die durch eine Umweltverträglichkeitsprüfung ermittelten, beschriebenen und bewerteten Umweltauswirkungen sind nach Maßgabe der hierfür geltenden Vorschriften zu berücksichtigen.

(2) Wird die Baugenehmigung unter Auflagen, Bedingungen oder befristet erteilt, kann eine Sicherheitsleistung verlangt werden.
Befristet genehmigte Vorhaben müssen spätestens sechs Monate nach Fristablauf beseitigt sein.
In den Fällen des § 35 Absatz 1 Nummer 2 bis 6 des Baugesetzbuchs wird die Baugenehmigung erst erteilt, wenn der Bauaufsichtsbehörde die Verpflichtungserklärung nach § 35 Absatz 5 Satz 2 des Baugesetzbuchs vorliegt und ihr für die Einhaltung der Rückbauverpflichtung Sicherheit in Höhe der Kosten der Beseitigung der baulichen Anlage oder gleichwertige Sicherheit geleistet ist.
Dies gilt auch, soweit andere behördliche Gestattungen die Baugenehmigung einschließen oder ersetzen.

(3) In der Baugenehmigung ist anzugeben, welche weiteren behördlichen Entscheidungen sie einschließt.
Der Bauherrin oder dem Bauherrn ist die Baugenehmigung mit den genehmigten Bauvorlagen zu übermitteln.

(4) Die Baugenehmigung gilt auch für und gegen die Rechtsnachfolgerin oder den Rechtsnachfolger der Bauherrin oder des Bauherrn.

(5) Die Baugenehmigung wird unbeschadet der privaten Rechte Dritter erteilt.

(6) Der amtsfreien Gemeinde, dem Amt, der Verbandsgemeinde, der mitverwalteten Gemeinde oder mitverwaltenden Gemeinde und der Entwurfsverfasserin oder dem Entwurfsverfasser ist die Entscheidung der Bauaufsichtsbehörde im bauaufsichtlichen Genehmigungsverfahren durch eine Übermittlung des Bescheides zur Kenntnis zu geben.

(7) Mit der Bauausführung darf erst begonnen werden, wenn

1.  eine erforderliche Baugenehmigung vorliegt oder die Voraussetzung des § 62 Absatz 3 Satz 1 erfüllt ist,
2.  nach anderen Rechtsvorschriften erforderliche Genehmigungen vorliegen,
3.  die erforderlichen Prüfberichte oder Bescheinigungen über die Prüfung der bautechnischen Nachweise vorliegen.

Die Bauaufsichtsbehörde kann sich die Freigabe der Bauarbeiten für die Baugrube, für einzelne Bauabschnitte oder für das gesamte Bauvorhaben vorbehalten.

(8) Die Bauherrin oder der Bauherr hat den Zeitpunkt des Baubeginns genehmigungs- oder anzeigepflichtiger Vorhaben spätestens eine Woche vor Baubeginn der Bauaufsichtsbehörde unter Vorlage der nach Absatz 7 Satz 1 Nummer 2 und 3 erforderlichen Nachweise mitzuteilen (Baubeginnsanzeige).

(9) Vor Baubeginn muss die Grundfläche der baulichen Anlage abgesteckt und ihre Höhenlage festgelegt sein.
Die Einhaltung der festgelegten Grundfläche und Höhenlage ist der Bauaufsichtsbehörde binnen zwei Wochen nach Baubeginn durch Vorlage einer Einmessungsbescheinigung einer Vermessungsingenieurin oder eines Vermessungsingenieurs nachzuweisen.
Der Nachweis nach Satz 2 kann auch durch eine Einmessungsbescheinigung erfolgen, die auf einer nach § 23 des Brandenburgischen Vermessungsgesetzes durchgeführten Einmessung beruht.

(10) Baugenehmigung, Bauvorlagen, Ausführungszeichnungen und Baufreigabeschein müssen an der Baustelle von Baubeginn an vorliegen.

#### § 72a Typengenehmigung

(1) Für bauliche Anlagen, die in derselben Ausführung an mehreren Stellen errichtet werden sollen, wird auf Antrag durch die oberste Bauaufsichtsbehörde eine Typengenehmigung erteilt, wenn die baulichen Anlagen oder Teile von baulichen Anlagen den Anforderungen nach diesem Gesetz oder aufgrund dieses Gesetzes erlassenen Vorschriften entsprechen.
Eine Typengenehmigung kann auch für bauliche Anlagen erteilt werden, die in unterschiedlicher Ausführung, aber nach einem bestimmten System und aus bestimmten Bauteilen an mehreren Stellen errichtet werden sollen; in der Typengenehmigung ist die zulässige Veränderbarkeit festzulegen.
Für Fliegende Bauten wird eine Typengenehmigung nicht erteilt.

(2) Die Typengenehmigung gilt fünf Jahre.
Die Frist kann auf Antrag jeweils um bis zu fünf Jahren verlängert werden; § 68 Absatz 2 und § 69 gelten entsprechend.

(3) Typengenehmigungen anderer Länder gelten auch im Land Brandenburg, soweit die nach Absatz 1 zuständige Behörde die Anwendbarkeit bestätigt hat.
Die Bestätigung kann mit Auflagen und Nebenbestimmungen versehen werden.

(4) Eine Typengenehmigung entbindet nicht von der Verpflichtung, ein bauaufsichtliches Verfahren durchzuführen.
Die in der Typengenehmigung entschiedenen Fragen sind von der Bauaufsichtsbehörde nicht mehr zu prüfen.

(5) Die Prüfberichte der Prüfingenieurin oder des Prüfingenieurs über die Prüfung der Standsicherheits- und Brandschutznachweise müssen der obersten Bauaufsichtsbehörde abweichend von § 66 Absatz 3 Satz 5 und 6 vor Erteilung der Typengenehmigung vorliegen.
Abweichend von § 66 Absatz 4 und § 67 Absatz 1 Satz 2 werden bautechnische Nachweise hinsichtlich der örtlichen Gegebenheiten und der Leistungsfähigkeit der örtlichen Feuerwehr, insbesondere nach den §§ 5, 33 Absatz 2 und 3 sowie § 37 Absatz 5, im bauaufsichtlichen Verfahren abschließend geprüft.

#### § 73 Geltungsdauer der Baugenehmigung und des Vorbescheides

(1) Die Geltungsdauer der Baugenehmigung, der Teilbaugenehmigung und des Vorbescheides beträgt sechs Jahre.
Die Baugenehmigung und die Teilbaugenehmigung erlöschen nicht, wenn das Vorhaben innerhalb der Frist nach Satz 1 begonnen worden und spätestens ein Jahr nach Ablauf der Frist die Aufnahme der Nutzung angezeigt worden ist.

(2) Abweichend von Absatz 1 richtet sich die Geltungsdauer der Baugenehmigung, der Teilbaugenehmigung und des Vorbescheides für bauliche Anlagen, deren Zulässigkeit sich auch nach einem Planfeststellungsbeschluss oder einer Plangenehmigung richtet, nach der Geltungsdauer des Planfeststellungsbeschlusses oder der Plangenehmigung.
§ 81 gilt entsprechend.

#### § 74 Teilbaugenehmigung

Ist ein Bauantrag eingereicht, kann der Beginn der Bauarbeiten für die Baugrube und für einzelne Bauteile oder Bauabschnitte auf Antrag schon vor Erteilung der Baugenehmigung gestattet werden (Teilbaugenehmigung).
§ 72 gilt entsprechend.

#### § 75 Vorbescheid

(1) Vor Einreichung des Bauantrags sind auf Antrag der Bauherrin oder des Bauherrn einzelne der selbstständigen Beurteilung zugängliche Fragen zu einem Bauvorhaben durch Vorbescheid von der Bauaufsichtsbehörde zu beantworten.
Soweit sich die Fragen auf behördliche Entscheidungen beziehen, die nach § 72 Absatz 1 Satz 2 in eine Baugenehmigung eingeschlossen sind, hat die Bauaufsichtsbehörde diese Fragen im Benehmen mit den betroffenen Behörden mit Bindungswirkung auch für diese Behörden zu beantworten.

(2) Die zur Beurteilung der Fragen erforderlichen Zeichnungen oder Pläne müssen den Anforderungen an Bauvorlagen entsprechen.
Im Fall des Absatzes 1 Satz 2 sind dem Antrag ferner die Unterlagen beizufügen, die für die Beurteilung nach den für die weiteren behördlichen Entscheidungen geltenden Vorschriften erforderlich sind.

(3) Im Fall des Absatzes 1 Satz 2 beträgt die Geltungsdauer des Vorbescheides abweichend von § 73 Absatz 1 drei Jahre.

#### § 76 Genehmigung Fliegender Bauten

(1) Fliegende Bauten sind bauliche Anlagen, die geeignet und bestimmt sind, an verschiedenen Orten wiederholt aufgestellt und zerlegt zu werden.
Baustelleneinrichtungen und Baugerüste sind keine Fliegenden Bauten.

(2) Fliegende Bauten bedürfen, bevor sie erstmals aufgestellt und in Gebrauch genommen werden, einer Ausführungsgenehmigung.
Dies gilt nicht für

1.  Fliegende Bauten mit einer Höhe bis zu 5 Meter, die nicht dazu bestimmt sind, von Besuchern betreten zu werden,
2.  Fliegende Bauten mit einer Höhe bis zu 5 Meter, die für Kinder betrieben werden und eine Geschwindigkeit von höchstens 1 Meter pro Sekunde haben,
3.  Bühnen, die Fliegende Bauten sind, einschließlich Überdachungen und sonstige Aufbauten mit einer Höhe bis zu 5 Meter, einer Grundfläche bis zu 100 Quadratmeter und einer Fußbodenhöhe bis zu 1,50 Meter,
4.  erdgeschossige Zelte und erdgeschossige betretbare Verkaufsstände, die Fliegende Bauten sind, jeweils mit einer Grundfläche bis zu 75 Quadratmeter,
5.  aufblasbare Spielgeräte mit einer Höhe des betretbaren Bereichs von bis zu 5 Meter oder mit überdachten Bereichen, bei denen die Entfernung zum Ausgang nicht mehr als 3 Meter, sofern ein Absinken der Überdachung konstruktiv verhindert wird, nicht mehr als 10 Meter, beträgt.

(3) Die Ausführungsgenehmigung wird von der obersten Bauaufsichtsbehörde erteilt.
Hat die Antragstellerin oder der Antragsteller im Land Brandenburg keine Hauptwohnung oder keine gewerbliche Niederlassung, so ist die oberste Bauaufsichtsbehörde nur zuständig, wenn der Fliegende Bau im Land Brandenburg erstmals aufgestellt und in Gebrauch genommen werden soll.

(4) Die Genehmigung wird für eine bestimmte Frist erteilt, die höchstens fünf Jahre betragen soll; sie kann auf Antrag in Textform von der für die Erteilung der Ausführungsgenehmigung zuständigen Behörde jeweils bis zu fünf Jahre verlängert werden, wenn der Antrag vor Ablauf der Geltungsdauer bei der Behörde eingegangen ist.
Die Genehmigungen werden in ein Prüfbuch eingetragen, dem eine Ausfertigung der mit einem Genehmigungsvermerk zu versehenden Bauvorlagen beizufügen ist.
Ausführungsgenehmigungen anderer Länder der Bundesrepublik Deutschland gelten auch im Land Brandenburg.

(5) Die Inhaberin oder der Inhaber der Ausführungsgenehmigung hat den Wechsel ihres oder seines Wohnsitzes oder ihrer oder seiner gewerblichen Niederlassung oder die Übertragung eines Fliegenden Baus an Dritte der obersten Bauaufsichtsbehörde anzuzeigen.
Die oberste Bauaufsichtsbehörde hat die Änderungen in das Prüfbuch einzutragen und sie, wenn mit den Änderungen ein Wechsel der Zuständigkeit verbunden ist, der nunmehr zuständigen Behörde mitzuteilen.

(6) Fliegende Bauten, die nach Absatz 2 Satz 1 einer Ausführungsgenehmigung bedürfen, dürfen unbeschadet anderer Vorschriften nur in Gebrauch genommen werden, wenn ihre Aufstellung der unteren Bauaufsichtsbehörde des Aufstellungsortes unter Vorlage des Prüfbuchs angezeigt ist.
Die Bauaufsichtsbehörde kann die Inbetriebnahme dieser Fliegenden Bauten von einer Gebrauchsabnahme abhängig machen.
Das Ergebnis der Abnahme ist in das Prüfbuch einzutragen.
In der Ausführungsgenehmigung kann bestimmt werden, dass Anzeigen nach Satz 1 nicht erforderlich sind, wenn eine Gefährdung im Sinne des § 3 Satz 1 nicht zu erwarten ist.

(7) Die für die Erteilung der Gebrauchsabnahme zuständige untere Bauaufsichtsbehörde kann Auflagen machen oder die Aufstellung oder den Gebrauch Fliegender Bauten untersagen, soweit dies nach den örtlichen Verhältnissen oder zur Abwehr von Gefahren erforderlich ist, insbesondere weil die Betriebssicherheit oder Standsicherheit nicht oder nicht mehr gewährleistet ist oder weil von der Ausführungsgenehmigung abgewichen wird.
Wird die Aufstellung oder der Gebrauch aufgrund von Mängeln am Fliegenden Bau untersagt, ist dies in das Prüfbuch einzutragen.
Die ausstellende Behörde ist zu benachrichtigen, das Prüfbuch ist einzuziehen und der ausstellenden Behörde zuzuleiten, wenn die Herstellung ordnungsgemäßer Zustände innerhalb angemessener Frist nicht zu erwarten ist.

(8) Bei Fliegenden Bauten, die von Besuchern betreten und längere Zeit an einem Aufstellungsort betrieben werden, kann die für die Gebrauchsabnahme zuständige untere Bauaufsichtsbehörde aus Gründen der Sicherheit Nachabnahmen durchführen.
Das Ergebnis der Nachabnahme ist in das Prüfbuch einzutragen.

(9) § 68 Absatz 1, 2 und 4 und § 82 Absatz 1 und 4 gelten entsprechend.

#### § 77 Bauaufsichtliche Zustimmung

(1) Nicht genehmigungsfreie Bauvorhaben bedürfen keiner Genehmigung und Bauüberwachung, wenn

1.  die Leitung der Entwurfsarbeiten und die Bauüberwachung einer Baudienststelle des Bundes oder eines Landes übertragen ist und
2.  die Baudienststelle mindestens mit einer Bediensteten oder einem Bediensteten mit der Befähigung zum höheren bautechnischen Verwaltungsdienst und mit sonstigen geeigneten Fachkräften ausreichend besetzt ist.

Solche baulichen Anlagen bedürfen jedoch der Zustimmung der obersten Bauaufsichtsbehörde.
Die Zustimmung entfällt, wenn die Gemeinde nicht widerspricht und, soweit ihre öffentlich-rechtlich geschützten Belange von Abweichungen, Ausnahmen und Befreiungen berührt sein können, die Nachbarn dem Bauvorhaben zustimmen.
Keiner Genehmigung oder Zustimmung bedürfen unter den Voraussetzungen des Satzes 1 Baumaßnahmen in oder an bestehenden Gebäuden, soweit sie nicht zu einer Erweiterung des Bauvolumens oder zu einer nicht genehmigungsfreien Nutzungsänderung führen, sowie die Beseitigung baulicher Anlagen.
Satz 3 gilt nicht für bauliche Anlagen, für die nach § 70 Absatz 7 eine Öffentlichkeitsbeteiligung durchzuführen ist.
Der öffentliche Bauherr trägt die Verantwortung dafür, dass Entwurf, Ausführung und Zustand der baulichen Anlagen sowie anderer Anlagen und Einrichtungen den öffentlich-rechtlichen Vorschriften entsprechen.
Die Baudienststelle nimmt insoweit die Aufgaben und Befugnisse einer unteren Bauaufsichtsbehörde nach § 58 Absatz 2 und 3 wahr.

(2) Der Antrag auf Zustimmung ist bei der obersten Bauaufsichtsbehörde einzureichen.

(3) Die oberste Bauaufsichtsbehörde prüft

1.  die Übereinstimmung mit den Vorschriften über die Zulässigkeit der baulichen Anlagen nach den §§ 29 bis 38 des Baugesetzbuchs und
2.  andere öffentlich-rechtliche Anforderungen, soweit wegen der Zustimmung eine Entscheidung nach anderen öffentlich-rechtlichen Vorschriften entfällt oder ersetzt wird.

Sie führt bei den in Absatz 1 Satz 5 genannten Anlagen die Öffentlichkeitsbeteiligung nach § 70 Absatz 6 durch.
Die oberste Bauaufsichtsbehörde entscheidet über Ausnahmen, Befreiungen und Abweichungen von den nach Satz 1 zu prüfenden sowie von anderen Vorschriften, soweit sie nachbarschützend sind und die Nachbarn nicht zugestimmt haben.
Im Übrigen bedarf die Zulässigkeit von Ausnahmen, Befreiungen und Abweichungen keiner bauaufsichtlichen Entscheidung.

(4) Die Gemeinde ist vor Erteilung der Zustimmung zu hören.
§ 36 Absatz 2 Satz 2 Halbsatz 1 des Baugesetzbuchs gilt entsprechend.
Für die Entscheidung nach § 37 Absatz 1 des Baugesetzbuchs ist die oberste Bauaufsichtsbehörde zuständig; vor der Entscheidung ist die Gemeinde zu hören.
Im Übrigen sind die Vorschriften über das Baugenehmigungsverfahren entsprechend anzuwenden.

(5) Anlagen, die der Landesverteidigung, dienstlichen Zwecken der Bundespolizei oder dem zivilen Bevölkerungsschutz dienen, sind abweichend von den Absätzen 1 bis 4 der obersten Bauaufsichtsbehörde vor Baubeginn in geeigneter Weise zur Kenntnis zu bringen; Absatz 1 Satz 3 Halbsatz 1 gilt entsprechend.
Im Übrigen wirken die Bauaufsichtsbehörden nicht mit.
§ 76 Absatz 2 bis 9 findet auf Fliegende Bauten, die der Landesverteidigung, dienstlichen Zwecken der Bundespolizei oder dem zivilen Bevölkerungsschutz dienen, keine Anwendung.

### Abschnitt 4  
Bauaufsichtliche Maßnahmen

#### § 78 Verbot unrechtmäßig gekennzeichneter Bauprodukte

Sind Bauprodukte entgegen § 21 Absatz 3 bis 5 mit dem Ü-Zeichen gekennzeichnet, kann die Bauaufsichtsbehörde die Verwendung dieser Bauprodukte untersagen und deren Kennzeichnung entwerten oder beseitigen lassen.

#### § 79 Einstellung von Arbeiten

(1) Werden Anlagen im Widerspruch zu öffentlich-rechtlichen Vorschriften errichtet, geändert oder beseitigt, kann die Bauaufsichtsbehörde die Einstellung der Arbeiten anordnen.
Dies gilt auch dann, wenn

1.  die Ausführung eines Vorhabens entgegen der Vorschrift des § 72 Absatz 7 bis 9 begonnen wurde, oder
2.  bei der Ausführung eines genehmigungsbedürftigen Bauvorhabens von den genehmigten oder angezeigten Bauvorlagen abgewichen wird,
3.  Bauprodukte verwendet werden, die entgegen der Verordnung (EU) Nr. 305/2011 keine CE-Kennzeichnung oder entgegen § 21 Absatz 3 bis 5 kein Ü-Zeichen tragen,
4.  Bauprodukte verwendet werden, die unberechtigt mit der CE-Kennzeichnung oder dem Ü-Zeichen (§ 21 Absatz 3 bis 5) gekennzeichnet sind.

(2) Werden unzulässige Arbeiten trotz einer verfügten Einstellung fortgesetzt, kann die Bauaufsichtsbehörde die Baustelle versiegeln oder die an der Baustelle vorhandenen Bauprodukte, Geräte, Maschinen und Bauhilfsmittel in amtlichen Gewahrsam bringen.

#### § 80 Beseitigung von Anlagen, Nutzungsuntersagung

(1) Werden Anlagen im Widerspruch zu öffentlich-rechtlichen Vorschriften errichtet oder geändert, kann die Bauaufsichtsbehörde die teilweise oder vollständige Beseitigung der Anlagen anordnen, wenn nicht auf andere Weise rechtmäßige Zustände hergestellt werden können.
Werden Anlagen im Widerspruch zu öffentlich-rechtlichen Vorschriften genutzt, kann diese Nutzung untersagt werden.
Wird diese Nutzung trotz bestandskräftiger oder sofort vollziehbarer Nutzungsuntersagung fortgesetzt, kann die Bauaufsichtsbehörde die bauliche Anlage versiegeln.

(2) Die Bauaufsichtsbehörde kann die Beseitigung einer baulichen Anlage auch dann anordnen, wenn diese nicht genutzt wird und zu verfallen droht und ein öffentliches oder schutzwürdiges privates Interesse an ihrer Erhaltung nicht besteht.

#### § 81 Anpassung bestehender baulicher Anlagen

(1) Wenn es zur Abwehr von erheblichen Gefahren für Leben oder Gesundheit erforderlich ist, können die Bauaufsichtsbehörden die Vorschriften dieses Gesetzes oder die aufgrund dieses Gesetzes erlassenen Vorschriften auch auf bestehende bauliche Anlagen und andere Anlagen und Einrichtungen anwenden.

(2) Sollen bauliche Anlagen wesentlich geändert werden, so kann gefordert werden, dass auch die nicht unmittelbar berührten Teile der baulichen Anlage mit diesem Gesetz oder den aufgrund dieses Gesetzes erlassenen Vorschriften in Einklang gebracht werden, wenn

1.  die Bauteile, die diesen Vorschriften nicht mehr entsprechen, mit den beabsichtigten Arbeiten in einem konstruktiven Zusammenhang stehen und
2.  die Durchführung dieser Vorschriften bei den von den Arbeiten nicht berührten Teilen der baulichen Anlage keine unzumutbaren Mehrkosten verursacht.

### Abschnitt 5  
Bauüberwachung

#### § 82 Bauüberwachung

(1) Die Bauaufsichtsbehörde kann die Einhaltung der öffentlich-rechtlichen Vorschriften und Anforderungen und die ordnungsgemäße Erfüllung der Pflichten der am Bau Beteiligten überprüfen.
Soweit die Baugenehmigung die Entscheidung einer anderen Behörde einschließt, bleibt deren Zuständigkeit unberührt.

(2) Die Bauaufsichtsbehörde oder die Prüfingenieurin oder der Prüfingenieur überwacht nach näherer Maßgabe der Rechtsverordnung nach § 86 Absatz 2 die Bauausführung bei baulichen Anlagen

1.  nach § 66 Absatz 3 Satz 1 und 3 hinsichtlich des bauaufsichtlich geprüften Standsicherheitsnachweises,
2.  nach § 66 Absatz 3 Satz 2 und 3 hinsichtlich des bauaufsichtlich geprüften Brandschutznachweises.

Bei Gebäuden der Gebäudeklasse 4, ausgenommen Sonderbauten sowie Mittel- und Großgaragen im Sinne der Verordnung nach § 86 Absatz 1 Nummer 3, ist die mit dem Brandschutznachweis übereinstimmende Bauausführung von der Nachweiserstellern oder dem Nachweisersteller oder einer anderen Nachweisberechtigten oder einem anderen Nachweisberechtigten im Sinne des § 66 Absatz 2 Satz 3 zu bestätigen.
Wird die Bauausführung durch eine Prüfingenieurin oder einen Prüfingenieur geprüft oder nach Satz 2 bestätigt, findet insoweit eine bauaufsichtsbehördliche Überwachung nicht statt.

(3) Im Rahmen der Bauüberwachung können Proben von Bauprodukten, soweit erforderlich, auch aus fertigen Bauteilen zu Prüfzwecken entnommen werden.

(4) Im Rahmen der Bauüberwachung ist jederzeit Einblick in die Genehmigungen, Zulassungen, Prüfzeugnisse, Übereinstimmungszertifikate, Zeugnisse und Aufzeichnungen über die Prüfungen von Bauprodukten, in die CE-Kennzeichnungen und Leistungserklärungen nach der Verordnung (EU) Nr. 305/2011, in die Bautagebücher und andere vorgeschriebene Aufzeichnungen zu gewähren.

(5) Die Bauaufsichtsbehörde oder die Prüfingenieurin oder der Prüfingenieur soll, soweit sie oder er im Rahmen der Bauüberwachung Erkenntnisse über systematische Rechtsverstöße gegen die Verordnung (EU) 305/2011 erlangen, diese der für die Marktüberwachung zuständigen Stelle mitteilen.

#### § 83 Bauzustandsanzeigen, Aufnahme der Nutzung

(1) Die Bauaufsichtsbehörde oder die Prüfingenieurin oder der Prüfingenieur können verlangen, dass ihr oder ihm Beginn und Beendigung bestimmter Bauarbeiten angezeigt werden.
Die Bauarbeiten dürfen erst fortgesetzt werden, wenn die Bauaufsichtsbehörde oder die Prüfingenieurin oder der Prüfingenieur der Fortführung der Bauarbeiten zugestimmt haben.

(2) Die Bauherrin oder der Bauherr hat die beabsichtigte Aufnahme der Nutzung einer nicht genehmigungsfreien baulichen Anlage mindestens zwei Wochen vorher der Bauaufsichtsbehörde anzuzeigen.
Mit der Anzeige nach Satz 1 sind vorzulegen

1.  bei Bauvorhaben nach § 66 Absatz 3 Satz 1 eine Bescheinigung der Prüfingenieurin oder des Prüfingenieurs über die ordnungsgemäße Bauausführung hinsichtlich der Standsicherheit,
2.  bei Bauvorhaben nach § 66 Absatz 3 Satz 2 eine Bescheinigung der Prüfingenieurin oder des Prüfingenieurs über die ordnungsgemäße Bauausführung hinsichtlich des Brandschutzes (§ 82 Absatz 2 Satz 1),
3.  in den Fällen des § 82 Absatz 2 Satz 2 die jeweilige Bestätigung,
4.  bei Sonderbauten die nach § 51 Absatz 2 erforderliche Bescheinigung der Prüfsachverständigen oder des Prüfsachverständigen für energetische Gebäudeplanung.

Eine bauliche Anlage darf erst benutzt werden, wenn sie selbst, Zufahrtswege, Wasserversorgungs- und Abwasserentsorgungs- sowie Gemeinschaftsanlagen in dem erforderlichen Umfang sicher benutzbar sind, nicht jedoch vor dem in Satz 1 bezeichneten Zeitpunkt.
Feuerstätten dürfen erst in Betrieb genommen werden, wenn die bevollmächtigte Bezirksschornsteinfegerin oder der bevollmächtigte Bezirksschornsteinfeger den sicheren Anschluss der Feuerstätte sowie die Tauglichkeit und die sichere Benutzbarkeit der Abgasanlagen bescheinigt hat; Verbrennungsmotoren und Blockheizkraftwerke dürfen erst dann in Betrieb genommen werden, wenn sie oder er den sicheren Anschluss sowie die Tauglichkeit und die sichere Benutzbarkeit der Leitungen zur Abführung von Verbrennungsgasen bescheinigt hat.
Die Anforderungen des § 42 Absatz 1 bis 3 und 5 müssen erfüllt sein.

### Abschnitt 6  
Baulasten

#### § 84 Baulasten, Baulastenverzeichnis

(1) Durch Erklärung gegenüber der Bauaufsichtsbehörde können Grundstückseigentümerinnen und Grundstückseigentümer öffentlich-rechtliche Verpflichtungen zu einem ihre Grundstücke betreffenden Tun, Dulden oder Unterlassen übernehmen, die sich nicht schon aus öffentlich-rechtlichen Vorschriften ergeben.
Erbbauberechtigte können ihr Erbbaurecht in entsprechender Weise belasten.
Baulasten werden unbeschadet der Rechte Dritter mit der Eintragung in das Baulastenverzeichnis wirksam und wirken auch gegenüber Rechtsnachfolgerinnen und Rechtsnachfolgern.

(2) Die Erklärung nach Absatz 1 bedarf der Schriftform; die Unterschrift muss öffentlich beglaubigt oder von einer Öffentlich bestellten Vermessungsingenieurin oder einem Öffentlich bestellten Vermessungsingenieur oder von der Katasterbehörde beglaubigt sein, wenn sie nicht vor der Bauaufsichtsbehörde geleistet oder vor ihr anerkannt wird.

(3) Die Baulast geht durch Verzicht der Bauaufsichtsbehörde unter.
Der Verzicht ist zu erklären, wenn ein öffentliches Interesse an der Baulast nicht mehr besteht.
Vor dem Verzicht sollen die oder der Verpflichtete und die durch die Baulast Begünstigten angehört werden.
Der Verzicht wird mit der Löschung der Baulast im Baulastenverzeichnis wirksam.

(4) Das Baulastenverzeichnis wird von der Bauaufsichtsbehörde geführt.
In das Baulastenverzeichnis können auch eingetragen werden

1.  andere baurechtliche Verpflichtungen des Grundstückseigentümers zu einem sein Grundstück betreffendes Tun, Dulden oder Unterlassen,
2.  Auflagen, Bedingungen, Befristungen und Widerrufsvorbehalte.

(5) Wer ein berechtigtes Interesse darlegt, kann in das Baulastenverzeichnis Einsicht nehmen oder sich einen Auszug erstellen lassen.
Öffentlich bestellte Vermessungsingenieurinnen und Öffentlich bestellte Vermessungsingenieure sowie Notarinnen und Notare sind befugt, das Baulastenverzeichnis einzusehen, ohne dass es der Darlegung eines berechtigten Interesses bedarf.

(6) Die bestehenden rechtlichen Sicherungen durch beschränkte persönliche Dienstbarkeiten behalten ihre Gültigkeit, soweit sie nicht durch Baulasten nach Absatz 1 ersetzt und die Eintragungen im Grundbuch gelöscht wurden.

Teil 6  
Ordnungswidrigkeiten, Rechtsvorschriften, Übergangsvorschriften
------------------------------------------------------------------------

#### § 85 Ordnungswidrigkeiten

(1) Ordnungswidrig handelt, wer vorsätzlich oder fahrlässig

1.  einer nach § 86 Absatz 1 bis 3 erlassenen Rechtsverordnung oder einer nach § 87 Absatz 1 und 2 erlassenen Satzung zuwiderhandelt, sofern die Rechtsverordnung oder die örtliche Bauvorschrift für einen bestimmten Tatbestand auf diese Bußgeldvorschrift verweist,
2.  einer vollziehbaren Anordnung der Bauaufsichtsbehörde zuwiderhandelt, die aufgrund dieses Gesetzes oder aufgrund einer nach diesem Gesetz zulässigen Rechtsverordnung oder Satzung erlassen worden ist, sofern die Anordnung auf die Bußgeldvorschrift verweist,
3.  ohne die erforderliche Baugenehmigung (§ 59 Absatz 1), Teilbaugenehmigung (§ 74) oder Abweichung (§ 67) oder abweichend davon bauliche Anlagen errichtet, ändert oder benutzt,
4.  entgegen der Vorschrift des § 62 Absatz 3 Satz 1 mit der Ausführung eines Bauvorhabens beginnt,
5.  Fliegende Bauten ohne Ausführungsgenehmigung (§ 76 Absatz 2) in Gebrauch nimmt oder ohne Anzeige und Abnahme (§ 76 Absatz 6) in Gebrauch nimmt,
6.  entgegen der Vorschrift des § 72 Absatz 7 mit den Bauarbeiten beginnt, entgegen der Vorschrift des § 83 Absatz 1 Bauarbeiten fortsetzt oder entgegen der Vorschrift des § 83 Absatz 2 Satz 1 und 2 bauliche Anlagen nutzt,
7.  die Baubeginnsanzeige (§ 72 Absatz 8) nicht oder nicht fristgerecht erstattet,
8.  Bauprodukte mit dem Ü-Zeichen kennzeichnet, ohne dass dafür die Voraussetzungen nach § 21 Absatz 3 bis 5 vorliegen,
9.  Bauprodukte entgegen § 21 Absatz 3 bis 5 ohne das Ü-Zeichen verwendet,
10.  Bauarten entgegen § 16a ohne Bauartgenehmigung oder allgemeines bauaufsichtliches Prüfzeugnis für Bauarten anwendet,
11.  als Bauherrin oder Bauherr, Entwurfsverfasserin oder Entwurfsverfasser, Unternehmerin oder Unternehmer, Bauleiterin oder Bauleiter oder als deren Vertreter den Vorschriften der § 53 Absatz 1 Satz 1 bis 3, 5 und 6, § 54 Absatz 1 Satz 3, § 55 Absatz 1 Satz 1 und 2 oder § 56 Absatz 1 zuwiderhandelt,
12.  entgegen § 83 Absatz 2 Satz 4 eine Feuerstätte, einen ortsfesten Verbrennungsmotor oder ein Blockheizkraftwerk in Betrieb nimmt.

Ist eine Ordnungswidrigkeit nach Satz 1 Nummer 8 bis 10 begangen worden, können Gegenstände, auf die sich die Ordnungswidrigkeit bezieht, eingezogen werden; § 22 des Gesetzes über Ordnungswidrigkeiten ist anzuwenden.

(2) Ordnungswidrig handelt auch, wer wider besseres Wissen

1.  unrichtige Angaben macht oder unrichtige Pläne oder Unterlagen vorlegt, um einen nach diesem Gesetz vorgesehenen Verwaltungsakt zu erwirken oder zu verhindern,
2.  als Prüfingenieurin oder Prüfingenieur unrichtige Prüfberichte erstellt,
3.  unrichtige Angaben aufgrund des Kriterienkatalogs nach § 66 Absatz 3 Satz 1 Nummer 2 macht.

(3) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu 500 000 Euro geahndet werden.

(4) Verwaltungsbehörde im Sinne des § 36 Absatz 1 Nummer 1 des Gesetzes über Ordnungswidrigkeiten ist die untere Bauaufsichtsbehörde.
Ist die amtsfreie Gemeinde, das Amt, die Verbandsgemeinde, die mitverwaltete Gemeinde oder die mitverwaltende Gemeinde nach § 58 Absatz 6 und 7 als Sonderordnungsbehörde zuständig, so ist diese Verwaltungsbehörde im Sinne des § 36 Absatz 1 Nummer 1 des Gesetzes über Ordnungswidrigkeiten.

#### § 86 Rechtsvorschriften

(1) Zur Verwirklichung der in § 3 Satz 1, § 16a Absatz 1 und § 16b Absatz 1 bezeichneten Anordnungen wird das für die Bauaufsicht zuständige Mitglied der Landesregierung ermächtigt, durch Rechtsverordnung Vorschriften zu erlassen über

1.  die nähere Bestimmung allgemeiner Anforderungen der §§ 4 bis 48,
2.  Anforderungen an Feuerungsanlagen (§ 42),
3.  Anforderungen an Garagen (§ 49),
4.  besondere Anforderungen oder Erleichterungen, die sich aus der besonderen Art oder Nutzung der baulichen Anlagen für Errichtung, Änderung, Unterhaltung, Betrieb und Nutzung ergeben (§ 51), sowie über die Anwendung solcher Anforderungen auf bestehende bauliche Anlagen dieser Art,
5.  Erst-, Wiederholungs- und Nachprüfung von Anlagen, die zur Verhütung erheblicher Gefahren oder Nachteile ständig ordnungsgemäß unterhalten werden müssen, und die Erstreckung dieser Nachprüfungspflicht auf bestehende Anlagen,
6.  die Anwesenheit fachkundiger Personen beim Betrieb technisch schwieriger baulicher Anlagen und Einrichtungen wie Bühnenbetriebe und technisch schwierige Fliegende Bauten einschließlich des Nachweises der Befähigung dieser Personen.

(2) Das für die Bauaufsicht zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung Vorschriften zu erlassen über

1.  Prüfingenieurinnen und Prüfingenieure, denen bauaufsichtliche Prüfaufgaben einschließlich der Bauüberwachung und der Bauzustandsbesichtigung durch die Bauherrin oder den Bauherrn übertragen werden, sowie
2.  Prüfsachverständige, die im Auftrag der Bauherrin oder des Bauherrn oder der sonstigen nach Bauordnungsrecht Verantwortlichen die Einhaltung bauordnungsrechtlicher Anforderungen prüfen und bescheinigen.

Die Rechtsverordnungen nach Satz 1 regeln, soweit erforderlich,

1.  die Fachbereiche und die Fachrichtungen, in denen Prüfingenieurinnen und Prüfingenieure und Prüfsachverständige tätig werden,
2.  die Anerkennungsvoraussetzungen und das Anerkennungsverfahren,
3.  Erlöschen, Rücknahme und Widerruf der Anerkennung einschließlich der Festlegung einer Altersgrenze,
4.  die Aufgabenerledigung,
5.  die Vergütung,
6.  das Erfordernis einer ausreichenden Haftpflichtversicherung,
7.  die Anforderungen an die Prüfingenieurinnen und Prüfingenieure und Prüfsachverständigen, insbesondere in Bezug auf deren Ausbildung, Fachkenntnisse, Berufserfahrung, persönliche Zuverlässigkeit sowie Fort- und Weiterbildung,
8.  die Einrichtung von Stellen zur gemeinsamen und einheitlichen Bewertung, Berechnung und Erhebung der Kosten der Prüfingenieurinnen und Prüfingenieure und die Aufsicht über diese Stelle,
9.  die Übertragung der Aufgaben einer Widerspruchsbehörde für Entscheidungen über Widersprüche gegen Kostenentscheidungen auf eine nach Nummer 8 eingerichtete Stelle oder einen bei dieser Stelle gebildeten Widerspruchsausschuss,
10.  die Übertragung der Zuständigkeit für die Beitreibung der Gebühren im Verwaltungszwangsverfahren auf die nach Nummer 8 eingerichtete Stelle,
11.  die Übertragung der Befugnis zur Anerkennung und zur Überwachung oder Aufsicht auf eine der obersten Bauaufsichtsbehörde nachgeordnete Behörde oder auf Dritte,
12.  die Überwachung der Prüfsachverständigen und die Aufsicht über die Prüfingenieurinnen und Prüfingenieure.

Die Zuständigkeiten für die Erledigung der Aufgaben nach Satz 2 Nummer 8 bis 11 können durch Beleihung nach § 16 des Landesorganisationsgesetzes übertragen werden.
Das für die Bauaufsicht zuständige Mitglied der Landesregierung kann durch Rechtsverordnung ferner

1.  den Leiterinnen und Leitern und den stellvertretenden Leiterinnen und Leitern von Brandschutzdienststellen die Stellung einer Prüfsachverständigen oder eines Prüfsachverständigen nach Satz 1 Nummer 2 zuweisen,
2.  soweit für bestimmte Fachbereiche und Fachrichtungen Prüfsachverständige nach Satz 1 Nummer 2 nicht in ausreichendem Umfang anerkannt sind, anordnen, dass die von solchen Prüfsachverständigen zu prüfenden und zu bescheinigenden bauordnungsrechtlichen Anforderungen bauaufsichtlich geprüft werden können,
3.  soweit Tragwerksplanerinnen und Tragwerksplaner nach § 66 Absatz 2 Satz 1 oder Brandschutzplanerinnen und Brandschutzplaner nach § 66 Absatz 2 Satz 3 noch nicht in ausreichendem Umfang eingetragen sind, anordnen, dass die Standsicherheit- oder Brandschutznachweise bauaufsichtlich geprüft werden und die Bauausführung bauaufsichtlich überwacht wird.

(3) Das für die Bauaufsicht zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung Vorschriften zu erlassen über

1.  Umfang, Inhalt, Zahl, Form sowie Art der Übermittlung der erforderlichen Unterlagen einschließlich der Vorlagen bei der Anzeige der beabsichtigten Beseitigung von Anlagen und beim Bauanzeigeverfahren nach § 62,
2.  Umfang, Inhalt und Zahl der Bauvorlagen und deren Prüfung, die für die in die Baugenehmigung nach § 72 Absatz 1 Satz 2 eingeschlossenen Entscheidungen erforderlich sind,
3.  die erforderlichen Anträge, Anzeigen, Nachweise, Bescheinigungen und Bestätigungen, auch bei genehmigungsfreien Bauvorhaben,
4.  das Verfahren im Einzelnen,
5.  die von den am Bau Beteiligten, insbesondere zum Nachweis einer ordnungsgemäßen Bauausführung vorzulegenden Anzeigen, Bescheinigungen oder Nachweise, sowie Prüfzeugnisse oder Bescheinigungen von Sachverständigen, sachverständigen Stellen oder Behörden,
6.  die erforderlichen Unterlagen und Nachweise zur Einhaltung der Anforderungen an die Energieeinsparung sowie zur Nutzung erneuerbarer Energien (§ 15 Absatz 4),
7.  die zu erhebenden personenbezogenen Daten der am Verfahren Beteiligten, der Nachbarn und des Eigentümers des Baugrundstücks,
8.  eine Anzeige- und Vorerkundungspflicht für Vorhaben zur Beseitigung baulicher Anlagen.

Es kann dabei für verschiedene Arten von Bauvorhaben unterschiedliche Anforderungen und Verfahren festlegen und den Gebrauch der von der obersten Bauaufsichtsbehörde veröffentlichten Vordrucke vorschreiben.

(4) Das für die Bauaufsicht zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung

1.  die Zuständigkeit für die vorhabenbezogene Bauartgenehmigung nach § 16a Absatz 2 Satz 1 Nummer 2 und den Verzicht darauf im Einzelfall nach § 16a Absatz 4 sowie die Zustimmung und den Verzicht auf Zustimmung im Einzelfall (§ 20) zu übertragen,
2.  die Zuständigkeit für die Anerkennung von Prüf-, Zertifizierungs- und Überwachungsstellen (§ 24) auf andere Behörden zu übertragen; die Zuständigkeit kann auch auf eine Behörde eines anderen Landes der Bundesrepublik Deutschland übertragen werden, die der Aufsicht einer obersten Bauaufsichtsbehörde untersteht oder an deren Willensbildung die oberste Bauaufsichtsbehörde mitwirkt,
3.  das Ü-Zeichen festzulegen und zu diesem Zeichen zusätzliche Angaben zu verlangen,
4.  das Anerkennungsverfahren nach § 24, die Voraussetzungen für die Anerkennung, ihre Rücknahme, ihren Widerruf und ihr Erlöschen zu regeln, insbesondere auch Altersgrenzen festzulegen, sowie eine ausreichende Haftpflichtversicherung zu fordern,
5.  die Erteilung von Typenprüfungen (§ 66 Absatz 4 Satz 3),
6.  die Genehmigung Fliegender Bauten (§ 76),
7.  die Prüfung bautechnischer Nachweise besonderen Schwierigkeitsgrades, einschließlich der Überprüfung der Bauausführung,
8.  die Zustimmung zu Vorhaben öffentlicher Bauherrn (§ 77),
9.  die Beratung der unteren Bauaufsichtsbehörden in bauaufsichtlichen Angelegenheiten,
10.  die Erteilung und Bestätigung der Anwendung von Typengenehmigungen (§ 72a),

zur landesweit einheitlichen Wahrnehmung auf eine der obersten Bauaufsichtsbehörde nachgeordnete Behörde zu übertragen.
Die Zuständigkeiten für die Erledigung der Aufgaben nach Satz 1 Nummer 5 bis 7 und der Aufgaben nach § 76 sowie für die Erledigung dieser Aufgaben erforderlichen Befugnisse können durch Beleihung nach § 16 des Landesorganisationsgesetzes übertragen werden.

(4a) Die oberste Bauaufsichtsbehörde kann durch Rechtsverordnung regeln, dass für bestimmte Bauprodukte und Bauarten, auch soweit sie Anforderungen nach anderen Rechtsvorschriften unterliegen, hinsichtlich dieser Anforderungen § 16a Absatz 2, die §§ 17 bis 25 ganz oder teilweise anwendbar sind, wenn die anderen Rechtsvorschriften dies verlangen oder zulassen.

(5) Das für die Bauaufsicht zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit dem für den Vollzug des Produktsicherheitsgesetzes oder des Energiewirtschaftsgesetzes zuständigen Mitglied der Landesregierung durch Rechtsverordnung zu bestimmen, dass die Anforderungen der aufgrund des § 34 des Produktsicherheitsgesetzes und des § 49 Absatz 4 des Energiewirtschaftsgesetzes erlassenen Rechtsverordnungen entsprechend für Anlagen gelten, die weder gewerblichen noch wirtschaftlichen Zwecken dienen und in deren Gefahrenbereich auch keine Arbeitnehmerinnen oder Arbeitnehmer beschäftigt werden.
Sie kann auch die Verfahrensvorschriften dieser Verordnungen für anwendbar erklären oder selbst das Verfahren bestimmen sowie Zuständigkeiten und Gebühren regeln.
Dabei kann sie auch vorschreiben, dass danach zu erteilende Erlaubnisse die Baugenehmigung oder die Zustimmung nach § 77 einschließlich der zugehörigen Abweichungen einschließen sowie dass § 35 Absatz 2 des Produktsicherheitsgesetzes insoweit Anwendung findet.

(6) Das für die Bauaufsicht zuständige Mitglied der Landesregierung wird ermächtigt, im Benehmen mit dem für Umwelt und Naturschutz zuständigen Mitglied der Landesregierung durch Rechtsverordnung

1.  über Absatz 3 hinaus Vorschriften über Umfang, Inhalt und Zahl der Bauvorlagen für Aufschüttungen oder Abgrabungen zu erlassen; dabei kann insbesondere ein Aufschüttungs- oder Abgrabungsplan mit Zeichnungen, Zeitplan und Erläuterungen vorgeschrieben werden, aus dem die Einzelheiten des Vorhabens, sein Anlass, die vom Vorhaben betroffenen Grundstücke und Anlagen, seine Auswirkungen und die Maßnahmen der Rekultivierung oder Renaturierung hervorgehen,
2.  die Verpflichtung der Unternehmerin oder des Unternehmers oder der Eigentümerin oder des Eigentümers zur Rekultivierung oder Renaturierung und zu einer Sicherheitsleistung zu bestimmen und die Höhe der Sicherheitsleistung zu regeln.

(7) Das für die Bauaufsicht zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung Vorschriften über Umfang, Inhalt und Form des Baulastenverzeichnisses zu regeln.

#### § 86a Technische Baubestimmungen

(1) Die Anforderungen nach § 3 können durch Technische Baubestimmungen konkretisiert werden.
Die Technischen Baubestimmungen sind zu beachten.
Von den in den Technischen Baubestimmungen enthaltenen Planungs-, Bemessungs- und Ausführungsregelungen kann abgewichen werden, wenn mit einer anderen Lösung in gleichem Maße die Anforderungen erfüllt werden und in der Technischen Baubestimmung eine Abweichung nicht ausgeschlossen ist; § 16a Absatz 2, § 17 Absatz 1 und § 67 Absatz 1 bleiben unberührt.

(2) Die Konkretisierungen können durch Bezugnahmen auf technische Regeln und deren Fundstellen oder auf andere Weise erfolgen, insbesondere in Bezug auf:

1.  bestimmte bauliche Anlagen oder ihre Teile,
2.  die Planung, Bemessung und Ausführung baulicher Anlagen und ihrer Teile,
3.  die Leistung von Bauprodukten in bestimmten baulichen Anlagen oder ihren Teilen, insbesondere
    1.  Planung, Bemessung und Ausführung baulicher Anlagen bei Einbau eines Bauprodukts,
    2.  Merkmale von Bauprodukten, die sich für einen Verwendungszweck auf die Erfüllung der Anforderungen nach § 3 Satz 1 auswirken,
    3.  Verfahren für die Feststellung der Leistung eines Bauproduktes im Hinblick auf Merkmale, die sich für einen Verwendungszweck auf die Erfüllung der Anforderungen nach § 3 Satz 1 auswirken,
    4.  zulässige oder unzulässige besondere Verwendungszwecke,
    5.  die Festlegung von Klassen und Stufen in Bezug auf bestimmte Verwendungszwecke,
    6.  die für einen bestimmten Verwendungszweck anzugebende oder erforderliche und anzugebende Leistung in Bezug auf ein Merkmal, das sich für einen Verwendungszweck auf die Erfüllung der Anforderungen nach § 3 Satz 1 auswirkt, soweit vorgesehen in Klassen und Stufen,
4.  die Bauarten und die Bauprodukte, die nur eines allgemeinen bauaufsichtlichen Prüfzeugnisses nach § 16a Absatz 3 oder nach § 19 Absatz 1 bedürfen,
5.  Voraussetzungen zur Abgabe der Übereinstimmungserklärung für ein Bauprodukt nach § 22,
6.  die Art, den Inhalt und die Form technischer Dokumentation.

(3) Die Technischen Baubestimmungen sollen nach den Grundanforderungen gemäß Anhang I der Verordnung (EU) Nr. 305/2011 gegliedert sein.

(4) Die Technischen Baubestimmungen enthalten die in § 17 Absatz 3 genannte Liste.

(5) Das Deutsche Institut für Bautechnik macht nach Anhörung der beteiligten Kreise im Einvernehmen mit der obersten Bauaufsichtsbehörde zur Durchführung dieses Gesetzes und der aufgrund dieses Gesetzes erlassenen Rechtsverordnungen die Technischen Baubestimmungen nach Absatz 1 als Verwaltungsvorschrift bekannt.
Die nach Satz 1 bekannt gemachte Verwaltungsvorschrift gilt als Verwaltungsvorschrift des Landes Brandenburg, soweit die oberste Bauaufsichtsbehörde keine abweichende Verwaltungsvorschrift im Amtsblatt für Brandenburg bekannt macht.
Die Fundstelle der Bekanntmachung der Verwaltungsvorschrift nach Satz 1 wird im Amtsblatt für Brandenburg bekannt gemacht.

#### § 87 Örtliche Bauvorschriften

(1) Die Gemeinden können örtliche Bauvorschriften erlassen über

1.  besondere Anforderungen an die äußere Gestaltung baulicher Anlagen und anderer Anlagen und Einrichtungen sowie die Notwendigkeit oder das Verbot von Einfriedungen und das Verbot von Schottergärten,
2.  besondere Anforderungen an die Art, die Größe, die Gestaltung, die Farbe und den Anbringungsort von Werbeanlagen,
3.  den Ausschluss von Werbeanlagen an bestimmten baulichen Anlagen,
4.  eine besondere Erlaubnispflicht für Werbeanlagen, die ohne Baugenehmigung errichtet werden dürfen, soweit für diese Werbeanlagen besondere Anforderungen nach Nummer 2 bestehen,
5.  die Begrünung baulicher Anlagen.

Die Gemeinde kann die örtlichen Bauvorschriften nach Satz 1 Nummer 1 und 2 erlassen, soweit dies zur Verwirklichung baugestalterischer und städtebaulicher Absichten oder zum Schutz bestimmter Bauten, Straßen, Plätze oder Ortsteile von geschichtlicher, künstlerischer oder städtebaulicher Bedeutung sowie von Baudenkmälern und Naturdenkmälern erforderlich ist.

(2) Die Gemeinde kann durch örtliche Bauvorschriften andere als die nach § 6 Absatz 5 vorgeschriebenen Abstandsflächen festsetzen.
Die Festsetzungen über die überbaubaren Grundstücksflächen und die Höhe der baulichen Anlagen müssen so bestimmt sein, dass die nach § 6 zu berücksichtigenden nachbarlichen Belange abgewogen werden können.
Eine geringere Tiefe der Abstandsflächen darf insbesondere zur Wahrung der erhaltenswerten Eigenart und zur städtebaulichen Gestaltung eines bestimmten Ortsteiles festgesetzt werden.

(3) Die Gemeinde kann örtliche Bauvorschriften über Kinderspielplätze erlassen.
Sie kann dabei

1.  die Größe, Art und Ausstattung der Kinderspielplätze nach Art und Maß der Nutzung festsetzen,
2.  die Anforderungen für den Zugang und die sichere Benutzbarkeit der Kinderspielplätze festsetzen,
3.  die nachträgliche Anlage eines Kinderspielplatzes festsetzen, wenn dies die Gesundheit und der Schutz der Kinder erfordern
4.  die Geldbeträge für die Ablösung der Kinderspielplätze bestimmen.

(4) Die Gemeinde kann örtliche Bauvorschriften über notwendige Stellplätze erlassen.
Sie kann dabei

1.  die Zahl der erforderlichen notwendigen Stellplätze nach Art und Maß der Nutzung unter Berücksichtigung der verkehrlichen, wirtschaftspolitischen oder städtebaulichen Gründe unterschiedlich festsetzen und insoweit besondere Maßnahmen, wie Mobilitätskonzepte und Maßnahmen des Mobilitätsmanagements, die die Pflicht zur Herstellung der notwendigen Stellplätze verringern, bestimmen,
2.  die Herstellung von Stellplätzen und Garagen für Kraftfahrzeuge untersagen oder einschränken, wenn verkehrliche, wirtschaftspolitische oder städtebauliche Gründe dies rechtfertigen; insbesondere bei guter Erschließung durch Einrichtungen des öffentlichen Personennahverkehrs,
3.  die Geldbeträge für die Ablösung notwendiger Stellplätze bestimmen.

Bis zu einem Fünftel der notwendigen Stellplätze können durch Abstellplätze für Fahrräder ersetzt werden.
Dabei sind für einen notwendigen Stellplatz vier Abstellplätze für Fahrräder herzustellen; diese werden zur Hälfte auf die Verpflichtung nach Absatz 5 angerechnet.
Die Ermächtigung des Satzes 2 erstreckt sich nicht auf die nach § 50 Absatz 4 notwendigen Stellplätze.

(5) Die Gemeinde kann örtliche Bauvorschriften über notwendige Abstellplätze für Fahrräder erlassen.
Sie kann dabei

1.  die Zahl der erforderlichen Fahrradabstellplätze nach Art und Maß der Nutzung festsetzen,
2.  die Größe, die Lage und die Ausstattung dieser Abstellplätze festlegen,
3.  die Geldbeträge für die Ablösung der notwendigen Abstellplätze für Fahrräder bestimmen.

(6) Die Gemeinde kann durch örtliche Bauvorschriften die Art, die Gestaltung und die Bauausführung der für die Errichtung und den Betrieb baulicher Anlagen erforderlichen Erschließungsanlagen bestimmen sowie nach anderen landesrechtlichen Vorschriften zulässige Festsetzungen über die Errichtung und den Betrieb baulicher Anlagen in gemeindlichen Satzungen auch in örtlichen Bauvorschriften festsetzen.

(7) Die Gemeinde kann, soweit die Voraussetzungen des § 15 Absatz 1 Satz 2 des Baugesetzbuchs vorliegen, durch örtliche Bauvorschrift bestimmen, welche der nach § 61 genehmigungsfreien Vorhaben spätestens einen Monat vor Durchführung des Vorhabens der Gemeinde anzuzeigen sind.

(8) Die Gemeinde erlässt die örtlichen Bauvorschriften als Satzung für das Gemeindegebiet oder Teile des Gemeindegebietes.
Für den Außenbereich dürfen örtliche Bauvorschriften nach Absatz 1 Satz 1 Nummer 2 nicht erlassen werden.
Vor dem Erlass der Satzung ist den betroffenen Bürgern und den berührten Trägern öffentlicher Belange Gelegenheit zur Stellungnahme innerhalb einer Frist von einem Monat zu geben.

(9) Örtliche Bauvorschriften nach Absatz 1 bis 6 können auch in

1.  einen Bebauungsplan nach § 30 Absatz 1 bis 3 des Baugesetzbuchs oder
2.  eine Satzung nach § 34 Absatz 4 Satz 1 Nummer 2 und 3 des Baugesetzbuchs

als Festsetzungen aufgenommen werden.
Für diese Festsetzungen sind die Verfahrensvorschriften des Baugesetzbuchs entsprechend anzuwenden.

(10) Festsetzungen in örtlichen Bauvorschriften können auch in Form zeichnerischer Darstellungen erfolgen.
Ihre Bekanntgabe kann dadurch ersetzt werden, dass dieser Teil der örtlichen Bauvorschriften bei der Gemeinde zur Einsicht ausgelegt wird; hierauf ist in den örtlichen Bauvorschriften hinzuweisen.

#### § 88 Übergangsvorschriften

(1) § 73 Absatz 2 ist auf alle Baugenehmigungen und Vorbescheide anwendbar, deren Geltungsdauer bei Inkrafttreten dieses Gesetzes noch nicht abgelaufen war.

(2) Solange § 20 Absatz 1 der Baunutzungsverordnung zur Begriffsbestimmung des Vollgeschosses auf Landesrecht verweist, gilt insoweit § 2 Absatz 4 der Brandenburgischen Bauordnung in der Fassung der Bekanntmachung vom 17. September 2008 (GVBl.
I S. 226) fort.
Auf Satzungen nach dem Baugesetzbuch, die bis zum Inkrafttreten dieses Gesetzes Rechtswirksamkeit erlangt haben, ist der zum Zeitpunkt des jeweiligen Satzungsbeschlusses geltende Begriff des Vollgeschosses weiter anzuwenden.

(3) Die bisher erfolgten Eintragungen der Brandenburgischen Ingenieurkammer in die Liste der Bauvorlageberechtigten behalten ihre Gültigkeit, soweit sie nicht durch Eintragungen in die Liste mit dem Zusatz Bauvorlageberechtigung nach § 65 Absatz 2 Nummer 2 ersetzt wurden.

(4) Die vor dem Inkrafttreten dieses Gesetzes eingeleiteten Verfahren sind nach den bis zum Inkrafttreten geltenden Vorschriften fortzuführen; die materiellen Vorschriften dieses Gesetzes sind jedoch anzuwenden, soweit diese für die Bauherrin oder den Bauherrn günstiger sind.

(5) Die Verwendung des Ü-Zeichens auf Bauprodukten, die die CE-Kennzeichnung aufgrund der Verordnung (EU) Nr. 305/2011 tragen, ist nicht mehr zulässig.
Ab dem 16.
Oktober 2018 verliert das Ü-Zeichen seine Gültigkeit bei bereits in Verkehr gebrachten Bauprodukten, die die CE-Kennzeichnung aufgrund der Verordnung (EU) Nr. 305/2011 tragen und mit dem Ü-Zeichen gekennzeichnet sind.

(6) Bis zum Ablauf des 15.
Oktober 2018 für Bauarten erteilte allgemeine bauaufsichtliche Zulassungen oder Zustimmungen im Einzelfall gelten als Bauartgenehmigung fort.

(7) Die bis zum Ablauf des 15.
Oktober 2018 erteilten Anerkennungen als Prüf-, Überwachungs- und Zertifizierungsstellen bleiben wirksam.
Für Anträge auf Anerkennung von Prüf-, Überwachungs- und Zertifizierungsstellen, die bis zum Ablauf des 15.
Oktober 2018 gestellt worden sind, gilt dieses Gesetz in der ab dem 16. Oktober 2018 geltenden Fassung.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[1)](#1a) Die Verpflichtungen aus der Richtlinie (EU) 2015/1535 des Europäischen Parlaments und des Rates vom 9.
September 2015 über ein Informationsverfahren auf dem Gebiet der technischen Vorschriften und der Vorschriften für die Dienste der Informationsgesellschaft (ABl.
L 241 vom 17.9.2015, S.
1) sowie aus der Richtlinie 2006/123/EG des Europäischen Parlaments und des Rates vom 12. Dezember 2006 über Dienstleistungen im Binnenmarkt (ABl.
L 376 vom 27.12.2006, S. 36) sind beachtet worden.