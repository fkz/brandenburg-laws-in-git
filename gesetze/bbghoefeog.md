## Gesetz über die Höfeordnung für das Land Brandenburg (BbgHöfeOG)

Der Landtag hat das folgende Gesetz beschlossen:

### Kapitel 1  
Allgemeine höferechtliche Bestimmungen

#### § 1 Zweck des Gesetzes, Begriff des Hofes

(1) Das Gesetz dient der Erhaltung und zukunftsfähigen Weiterentwicklung bäuerlicher Betriebe im Land Brandenburg, indem es Voraussetzungen für eine wirtschaftlich stabile Hofübergabe an die nachfolgende Generation schafft.
Es dient damit der Stärkung ortsansässiger Landwirte, einer breiten Streuung des Eigentums und einer ausgewogenen Agrarstruktur im Land Brandenburg.

(2) Hof im Sinne dieses Gesetzes ist eine land- oder forstwirtschaftliche Besitzung mit einer zu ihrer Bewirtschaftung geeigneten Hofstelle, die im Alleineigentum einer natürlichen Person oder im gemeinschaftlichen Eigentum von Ehegatten (Ehegattenhof) steht oder zum Gesamtgut einer fortgesetzten Gütergemeinschaft gehört, sofern sie eine land- oder forstwirtschaftliche Fläche von mindestens 20 Hektar umfasst.
Eine Besitzung ist auch dann Hof, wenn die land- oder forstwirtschaftliche Fläche mindestens 10 Hektar und weniger als 20 Hektar umfasst und der Eigentümer oder die Eigentümerin erklärt, dass diese Besitzung Hof sein soll, und wenn der Hofvermerk im Grundbuch eingetragen wird.
Gehört die Besitzung Ehegatten, ohne nach Satz 1 Ehegattenhof zu sein, so wird sie Ehegattenhof, wenn beide Ehegatten erklären, dass sie Ehegattenhof sein soll, und wenn diese Eigenschaft im Grundbuch eingetragen wird.

(3) Eine Besitzung verliert die Eigenschaft als Hof, wenn keine der in Absatz 2 aufgezählten Eigentumsformen mehr besteht oder eine der übrigen Voraussetzungen auf Dauer wegfällt.

(4) Eine Besitzung ist kein Hof, wenn der Eigentümer oder die Eigentümerin erklärt, dass sie kein Hof sein soll und dies im Grundbuch eingetragen wird.
Die Besitzung wird, wenn sie die Voraussetzungen des Absatzes 2 erfüllt, Hof, wenn der Eigentümer oder die Eigentümerin erklärt, dass sie Hof sein soll, und wenn der Hofvermerk im Grundbuch eingetragen wird.

(5) Ein Ehegattenhof verliert diese Eigenschaft mit der Rechtskraft der Scheidung, der Aufhebung oder Nichtigerklärung der Ehe.
Bei bestehender Ehe verliert er die Eigenschaft als Ehegattenhof, wenn beide Ehegatten erklären, dass die Besitzung kein Ehegattenhof mehr sein soll und dies im Grundbuch eingetragen wird.

(6) Erklärungen nach den vorstehenden Absätzen können, wenn der Eigentümer oder die Eigentümerin nicht testierfähig ist, von einer gesetzlich vertretungsberechtigten Person abgegeben werden.
Die Erklärung bedarf hierzu der Genehmigung des Gerichts.
Das Gericht soll den Eigentümer oder die Eigentümerin vor der Entscheidung über die Genehmigung hören.
Zuständig ist in Kindschaftssachen nach § 151 Nummer 4 oder Nummer 5 des Gesetzes über das Verfahren in Familiensachen und in den Angelegenheiten der freiwilligen Gerichtsbarkeit vom 17.
Dezember 2008 (BGBl. I S. 2586, 2587), das zuletzt durch Artikel 13 des Gesetzes vom 18.
Dezember 2018 (BGBl. I S. 2639) geändert worden ist, das Familiengericht, in allen anderen Fällen das Betreuungsgericht.

(7) Wird ein Hofvermerk aufgrund einer Erklärung des Eigentümers oder der Eigentümerin oder von Ehegatten eingetragen oder gelöscht, so tritt die dadurch bewirkte Rechtsfolge rückwirkend mit dem Eingang der Erklärung beim Landwirtschaftsgericht ein.

(8) Die für Ehegatten geltenden Vorschriften dieses Gesetzes gelten für Lebenspartner entsprechend.
An die Stelle des Ehegattenhofes tritt der Lebenspartnerhof.

#### § 2 Bestandteile

Zum Hof gehören:

1.  alle Grundstücke des Hofeigentümers oder der Hofeigentümerin, die regelmäßig von der Hofstelle aus bewirtschaftet werden; eine zeitweilige Verpachtung oder ähnliche vorübergehende Benutzung durch andere schließt die Zugehörigkeit zum Hof nicht aus, ebenso wenig die vorläufige Besitzeinweisung anderer in einem Flurbereinigungsverfahren oder einem ähnlichen Verfahren;
2.  Mitgliedschaftsrechte, Nutzungsrechte und ähnliche Rechte, die dem Hof dienen, gleichviel ob sie mit dem Eigentum am Hof verbunden sind oder dem Eigentümer oder der Eigentümerin persönlich zustehen, ferner dem Hof dienende Miteigentumsanteile an einem Grundstück, falls diese Anteile im Verhältnis zu dem sonstigen, den Hof bildenden Grundbesitz von untergeordneter Bedeutung sind.

#### § 3 Hofeszubehör

Zum Hof gehört auch das Hofeszubehör.
Es umfasst insbesondere das auf dem Hof für die Bewirtschaftung vorhandene Vieh, Wirtschafts- und Hausgerät, den vorhandenen Dünger und die für die Bewirtschaftung bis zur nächsten Ernte dienenden Vorräte an landwirtschaftlichen Erzeugnissen und Betriebsmittel.

#### § 4 Erbfolge in einen Hof

Der Hof fällt als Teil der Erbschaft kraft Gesetzes nur einem der Erben (dem Hoferben oder der Hoferbin) zu.
Anstelle des Hofes tritt im Verhältnis der Miterben untereinander der Hofeswert.

#### § 5 Gesetzliche Hoferbenordnung

Wenn der Erblasser oder die Erblasserin keine Bestimmung trifft, sind als Hoferben kraft Gesetzes in folgender Ordnung berufen:

1.  die Kinder des Erblassers oder der Erblasserin und deren Abkömmlinge,
2.  die Ehegattin des Erblassers oder der Erblasserin oder der Ehegatte der Erblasserin oder des Erblassers,
3.  die Eltern des Erblassers oder der Erblasserin, wenn der Hof von ihnen oder aus ihren Familien stammt oder mit ihren Mitteln erworben worden ist,
4.  die Geschwister des Erblassers oder der Erblasserin und deren Abkömmlinge.

#### § 6 Einzelheiten zur Hoferbenordnung

(1) In der ersten Hoferbenordnung ist als Hoferbe oder Hoferbin berufen:

1.  in erster Linie die miterbende Person, der vom Erblasser oder von der Erblasserin die Bewirtschaftung des Hofes im Zeitpunkt des Erbfalles auf Dauer übertragen ist, es sei denn, dass sich der Erblasser oder die Erblasserin dabei ihr gegenüber die Bestimmung des Hoferben oder der Hoferbin ausdrücklich vorbehalten hat;
2.  in zweiter Linie die miterbende Person, hinsichtlich der der Erblasser oder die Erblasserin durch die Ausbildung oder durch Art und Umfang der Beschäftigung auf dem Hof hat erkennen lassen, dass sie den Hof übernehmen soll;
3.  in dritter Linie die älteste Person der Miterben.

Liegen die Voraussetzungen der Nummer 2 bei mehreren Miterben vor, ohne dass erkennbar ist, wer von ihnen den Hof übernehmen sollte, so ist unter diesen Miterben die älteste Person als Hoferbe oder Hoferbin berufen.

(2) In der zweiten Hoferbenordnung scheidet der Ehegatte oder die Ehegattin als Hoferbe oder Hoferbin aus,

1.  wenn Verwandte der dritten und vierten Hoferbenordnung leben und ihr Ausschluss von der Hoferbfolge, insbesondere wegen der von ihnen erbrachten Leistungen, grob unbillig wäre; oder
2.  wenn das Erbrecht nach § 1933 des Bürgerlichen Gesetzbuchs ausgeschlossen ist.

(3) In der dritten Hoferbenordnung ist nur derjenige Elternteil hoferbenberechtigt, von dem oder aus dessen Familie der Hof stammt oder mit dessen Mitteln der Hof erworben worden ist.

(4) Stammt der Hof von beiden Eltern oder aus beiden Familien oder ist er mit den Mitteln beider Eltern erworben und ist wenigstens einer der Eltern wirtschaftsfähig, so fällt der Hof den Eltern gemeinschaftlich als Ehegattenhof an.
Erbt einer von ihnen nicht mehr, so fällt er dem anderen an.
Ist die Ehe der Eltern vor dem Erbfall auf andere Weise als durch den Tod eines von ihnen aufgelöst worden, so scheiden sie als Hoferben aus.

(5) In der vierten Hoferbenordnung gilt Absatz 1 entsprechend.
Im Falle des Absatzes 1 Satz 1 Nummer 3 gehen die Geschwister vor, die mit dem Erblasser oder der Erblasserin den Elternteil gemeinsam haben, von dem oder aus dessen Familie der Hof stammt.

(6) Wer nicht wirtschaftsfähig ist, scheidet als hoferbenberechtigte Person aus, auch wenn sie hierzu nach Absatz 1 Satz 1 Nummer 1 oder 2 berufen ist.
Dies gilt jedoch nicht, wenn allein mangelnde Altersreife der Grund der Wirtschaftsunfähigkeit ist oder wenn es sich um die Vererbung an den überlebenden Ehegatten oder die überlebende Ehegattin handelt.
Scheidet die zunächst berufene hoferbenberechtigte Person aus, so fällt der Hof derjenigen Person an, die berufen wäre, wenn der Ausscheidende oder die Ausscheidende zur Zeit des Erbfalls nicht gelebt hätte.

(7) Wirtschaftsfähig ist eine Person, die nach ihren körperlichen und geistigen Fähigkeiten, nach ihren Kenntnissen und ihrer Persönlichkeit in der Lage ist, den von ihr zu übernehmenden Hof selbstständig ordnungsmäßig zu bewirtschaften.

#### § 7 Bestimmung des Hoferben oder der Hoferbin

(1) Der Eigentümer oder die Eigentümerin kann den Hoferben oder die Hoferbin durch Verfügung von Todes wegen frei bestimmen oder dem Erben oder der Erbin den Hof im Wege der vorweggenommenen Erbfolge (Übergabevertrag) übergeben.
Zum Hoferben oder zur Hoferbin kann nicht bestimmt werden, wer wegen Wirtschaftsunfähigkeit nach § 6 Absatz 6 Satz 1 und 2 als Hoferbe oder Hoferbin ausscheidet; die Wirtschaftsunfähigkeit eines Abkömmlings steht jedoch seiner Bestimmung zum Hoferben oder ihrer Bestimmung zur Hoferbin nicht entgegen, wenn sämtliche Abkömmlinge wegen Wirtschaftsunfähigkeit ausscheiden und ein wirtschaftsfähiger Ehegatte oder eine wirtschaftsfähige Ehegattin nicht vorhanden ist.

(2) Hat der Eigentümer oder die Eigentümerin die Bewirtschaftung des Hofes unter den Voraussetzungen des § 6 Absatz 1 Satz 1 Nummer 1 einem hoferbenberechtigten Abkömmling übertragen, so ist, solange dieser den Hof bewirtschaftet, eine vom Eigentümer oder von der Eigentümerin nach Übertragung der Bewirtschaftung vorgenommene Bestimmung eines anderen zum Hoferben oder zur Hoferbin insoweit unwirksam, als durch sie die hoferbenberechtigte Person von der Hoferbfolge ausgeschlossen würde.
Das gleiche gilt, wenn der Eigentümer oder die Eigentümerin durch Art und Umfang der Beschäftigung (§ 6 Absatz 1 Satz 1 Nummer 2) eines hoferbenberechtigten Abkömmlings auf dem Hof hat erkennen lassen, dass er den Hof übernehmen soll.
Das Recht des Eigentümers oder der Eigentümerin, über das der Hoferbfolge unterliegende Vermögen durch Rechtsgeschäft unter Lebenden zu verfügen, wird durch die Sätze 1 und 2 nicht beschränkt.

#### § 8 Vererbung beim Ehegattenhof

(1) Bei einem Ehegattenhof fällt der Anteil des Erblassers dem überlebenden Ehegatten als Hoferben oder der überlebenden Ehegattin als Hoferbin oder der Anteil der Erblasserin dem überlebenden Ehegatten als Hoferben oder der überlebenden Ehegattin als Hoferbin zu.

(2) Die Ehegatten können eine andere Person als Hoferben oder Hoferbin nur gemeinsam bestimmen und eine von ihnen getroffene Bestimmung nur gemeinsam wiederaufheben.
Haben die Ehegatten eine solche Bestimmung nicht getroffen oder wiederaufgehoben, so kann der überlebende Ehegatte oder die überlebende Ehegattin den Hoferben oder die Hoferbin allein bestimmen.

(3) Gehört der Hof zum Gesamtgut einer Gütergemeinschaft, so kann der überlebende Ehegatte oder die überlebende Ehegattin die Gütergemeinschaft bezüglich des Hofes nach den Vorschriften des allgemeinen Rechts mit den Abkömmlingen fortsetzen.
Wird die fortgesetzte Gütergemeinschaft anders als durch den Tod des überlebenden Ehegatten oder der überlebenden Ehegattin beendet, so wachsen ihm oder ihr die Anteile der Abkömmlinge an.
Im Übrigen steht die Beendigung der fortgesetzten Gütergemeinschaft dem Erbfall gleich.
Die Fortsetzung der Gütergemeinschaft lässt eine nach Absatz 2 getroffene Bestimmung sowie das Recht, eine solche Bestimmung zu treffen, unberührt.

#### § 9 Vererbung mehrerer Höfe

(1) Hinterlässt der Erblasser oder die Erblasserin mehrere Höfe, so können die als Hoferben berufenen Abkömmlinge in der Reihenfolge ihrer Berufung je einen Hof wählen; dabei kann jedoch nicht ein Hof gewählt werden, für den ein anderer Abkömmling, der noch nicht gewählt hat, nach § 6 Absatz 1 Satz 1 Nummer 1 oder 2 vorrangig als Hoferbe oder Hoferbin berufen ist.
Sind mehr Höfe vorhanden als berechtigte Abkömmlinge, so wird die Wahl nach denselben Grundsätzen wiederholt.
Hinterlässt der Eigentümer oder die Eigentümerin keine Abkömmlinge, so können die als Hoferben in derselben Ordnung Berufenen in der gleichen Weise wählen.
Diese Vorschriften gelten auch dann, wenn ein Hoferbe oder eine Hoferbin nach § 6 Absatz 1 Satz 1 Nummer 1 oder 2 hinsichtlich mehrerer Höfe als berufen anzusehen wäre.

(2) Die Wahl ist gegenüber dem Gericht in öffentlich beglaubigter Form oder zu seiner Niederschrift zu erklären; die Niederschrift wird nach den Vorschriften des Beurkundungsgesetzes errichtet.
Das Gericht kann der wahlberechtigten Person auf Antrag der nachstehenden wahlberechtigten Person eine angemessene Frist zur Erklärung über die Wahl bestimmen.
Nach fruchtlosem Ablauf der Frist tritt die wahlberechtigte Person hinter die übrigen Wahlberechtigten zurück.

(3) Jede hoferbenberechtigte Person erwirbt das Eigentum an dem ihr zufallenden Hof rückwirkend vom Tod des Erblassers oder der Erblasserin an.

#### § 10 Vererbung nach allgemeinem Recht

Der Hof vererbt sich nach den Vorschriften des allgemeinen Rechts, wenn nach den Vorschriften dieses Gesetzes kein Hoferbe oder keine Hoferbin vorhanden oder wirksam bestimmt ist.

#### § 11 Ausschlagung

Der Hoferbe oder die Hoferbin kann den Anfall des Hofes durch Erklärung gegenüber dem Gericht ausschlagen, ohne die Erbschaft in das übrige Vermögen auszuschlagen.
Auf diese Ausschlagung finden die Vorschriften des Bürgerlichen Gesetzbuchs über die Ausschlagung der Erbschaft entsprechende Anwendung.

#### § 12 Abfindung der Miterben nach dem Erbfall

(1) Den Miterben, die nicht Hoferben geworden sind, steht vorbehaltlich anderweitiger Regelung durch Übergabevertrag oder Verfügung von Todeswegen anstelle eines Anteils am Hof ein Anspruch gegen den Hoferben oder die Hoferbin auf Zahlung einer Abfindung in Geld zu.

(2) Der Anspruch bemisst sich nach dem Hofeswert im Zeitpunkt des Erbfalls.
Als Hofeswert gilt die Summe

1.  des Eineinhalbfachen der zuletzt festgestellten Ersatzwirtschaftswerte im Sinne der §§ 125 bis 128 des Bewertungsgesetzes vom 1.
    Februar 1991 (BGBl. I S. 230), das zuletzt durch Artikel 2 des Gesetzes vom 4.
    November 2016 (BGBl. I S. 2464, 2472) geändert worden ist, wobei nur der Anteil an den Ersatzwirtschaftswerten zu berücksichtigen ist, der auf die nach § 2 Nummer 1 zum Hof gehörenden Grundstücke entfällt, und
2.  des Verkehrswertes der zum Hof gehörenden Wohngebäude einschließlich des dazugehörigen Grund und Bodens.

Kommen besondere Umstände des Einzelfalles, die für den Wert des Hofes von erheblicher Bedeutung sind, in dem Hofeswert nicht oder ungenügend zum Ausdruck, so können auf Verlangen Zuschläge oder Abschläge nach billigem Ermessen gemacht werden.

(3) Von dem Hofeswert werden die Nachlassverbindlichkeiten abgezogen, die im Verhältnis der Erben zueinander den Hof treffen und die der Hoferbe oder die Hoferbin allein zu tragen hat.
Der danach verbleibende Betrag, jedoch mindestens ein Drittel des Hofeswertes (Absatz 2 Satz 2), gebührt den Erben des Erblassers oder der Erblasserin einschließlich der hoferbenberechtigten Person, falls sie zu ihnen gehört, zu dem Teil, der ihrem Anteil am Nachlass nach dem allgemeinen Recht entspricht.

(4) Auf die Abfindung nach Absatz 1 muss sich die miterbende Person dasjenige anrechnen lassen, was sie oder ihr vor dem Erbfall weggefallener Eltern- oder Großelternteil vom Erblasser oder von der Erblasserin als Abfindung aus dem Hof erhalten hat.

(5) Das Gericht kann die Zahlung der einem Miterben oder einer Miterbin zustehenden Abfindung, auch wenn diese durch Verfügung von Todes wegen oder vertraglich festgesetzt ist, auf Antrag stunden, soweit der Hoferbe oder die Hoferbin bei sofortiger Zahlung den Hof nicht ordnungsmäßig bewirtschaften könnte und dem einzelnen Miterben oder der einzelnen Miterbin bei gerechter Abwägung der Lage der Beteiligten eine Stundung zugemutet werden kann.
Das Gericht entscheidet nach billigem Ermessen, ob und in welcher Höhe eine gestundete Forderung zu verzinsen und ob, in welcher Art und in welchem Umfang für sie Sicherheit zu leisten ist.
Es kann die rechtskräftige Entscheidung über die Stundung, Verzinsung und Sicherheitsleistung auf Antrag aufheben oder ändern, wenn sich die Verhältnisse nach dem Erlass der Entscheidung wesentlich geändert haben.
Im Verfahren über Stundung, Verzinsung und Sicherung eines Abfindungsanspruchs ist § 264 des Gesetzes über das Verfahren in Familiensachen und in den Angelegenheiten der freiwilligen Gerichtsbarkeit entsprechend anzuwenden.

(6) Sind Miterben minderjährig, so gilt deren Abfindung bis zum Eintritt der Volljährigkeit als gestundet.
Der Hoferbe oder die Hoferbin hat jedoch die Kosten des angemessenen Lebensbedarfs und einer angemessenen Berufsausbildung zu zahlen und den Miterben eine zur Erlangung einer selbstständigen Lebensstellung oder bei Eingehung einer Ehe eine angemessene Ausstattung zu gewähren.
Leistungen nach Satz 2 sind bis zur Höhe der Abfindung einschließlich Zinsen und in Anrechnung darauf zu erbringen.

(7) Auf einen nach Absatz 6 Satz 1 als gestundet geltenden Anspruch sind die Vorschriften des Absatzes 5 Satz 2 bis 4 sinngemäß anzuwenden; Absatz 6 Satz 2 ist zu berücksichtigen.

(8) Ist ein Dritter der miterbenden Person zum Unterhalt verpflichtet, so beschränkt sich die Verpflichtung des Hoferben oder der Hoferbin nach Absatz 6 Satz 2 auf die Zahlung der Kosten, die durch den der miterbenden Person gewährten Unterhalt nicht gedeckt sind.

(9) Hat der Hoferbe oder die Hoferbin durch eine Zuwendung, die nach § 2050 des Bürgerlichen Gesetzbuchs zur Ausgleichung zu bringen ist, mehr als die Hälfte des nach Abzug der Nachlassverbindlichkeiten verbleibenden Wertes (Absatz 3 Satz 1) erhalten, so ist der Hoferbe oder die Hoferbin entgegen der Vorschrift des § 2056 des Bürgerlichen Gesetzbuchs zur Herausgabe des Mehrbetrages verpflichtet.

(10) Die Vorschriften der Absätze 2 bis 5 gelten sinngemäß für die Ansprüche von Pflichtteilsberechtigten, Vermächtnisnehmern sowie des überlebenden Ehegatten oder der überlebenden Ehegattin, wenn ein Ausgleich des Zugewinns (§ 1371 Absatz 2 und 3 des Bürgerlichen Gesetzbuchs) verlangt wird.

(11) Führt die Bemessung eines Anspruchs auf der Grundlage dieser Vorschrift nach den Umständen des Falls zu einem grob unbilligen Ergebnis, kann der Gläubiger oder die Gläubigerin einen angemessenen Zuschlag verlangen.

#### § 13 Ergänzung der Abfindung wegen Wegfalls des höferechtlichen Zwecks

(1) Veräußert der Hoferbe oder die Hoferbin innerhalb von zwanzig Jahren nach dem Erbfall den Hof, so können die nach § 12 Berechtigten unter Anrechnung einer bereits empfangenen Abfindung die Herausgabe des erzielten Erlöses zu dem Teil verlangen, der ihrem nach dem allgemeinen Recht bemessenen Anteil am Nachlass oder an dessen Wert entspricht.
Dies gilt auch, wenn zum Hof gehörende Grundstücke einzeln oder nacheinander veräußert werden und die dadurch erzielten Erlöse insgesamt ein Zehntel des Hofeswertes (§ 12 Absatz 2) übersteigen, es sei denn, dass die Veräußerung zur Erhaltung des Hofes erforderlich war.
Eine Übergabe des Hofes im Wege der vorweggenommenen Erbfolge gilt nicht als Veräußerung im Sinne des Satzes 1.
Wird der Hof in eine Gesellschaft eingebracht, so gilt der Verkehrswert des Hofes im Zeitpunkt der Einbringung als Veräußerungserlös.

(2) Hat die nach Absatz 1 verpflichtete Person innerhalb von zwei Jahren vor oder nach der Entstehung der Verpflichtung einen land- oder forstwirtschaftlichen Ersatzbetrieb oder im Falle des Absatzes 1 Satz 2 Ersatzgrundstücke erworben, so kann sie die hierfür gemachten Aufwendungen bis zur Höhe der für einen gleichwertigen Ersatzerwerb angemessenen Aufwendungen von dem Veräußerungserlös absetzen; als gleichwertig ist dabei eine Besitzung anzusehen, die als Ersatzbetrieb oder als um die Ersatzgrundstücke vervollständigter Restbesitz dem Hofeswert (§ 12 Absatz 2) des ganz oder teilweise veräußerten Hofes entspricht.
Dies gilt auch, wenn der Ersatzbetrieb oder ein Ersatzgrundstück im Gebiet eines anderen Bundeslandes belegen ist.

(3) Macht die verpflichtete Person glaubhaft, dass sie sich um einen Ersatzerwerb bemüht, so kann das Gericht den Anspruch bis zum Ablauf der in Absatz 2 Satz 1 bestimmten Frist stunden; § 12 Absatz 5 Satz 2 bis 4 gilt entsprechend.
Hat die verpflichtete Person einen notariellen Vertrag über den Erwerb eines Ersatzbetriebes oder im Falle des Absatzes 1 Satz 2 über den Erwerb von Ersatzgrundstücken abgeschlossen, so ist die Frist nach Absatz 2 Satz 1 auch gewahrt, wenn der Antrag auf Eintragung des Eigentumsübergangs oder einer den Anspruch auf Übereignung sichernden Vormerkung bis zum Ablauf der Frist beim Grundbuchamt eingegangen ist.

(4) Absatz 1 Satz 1 gilt entsprechend, wenn der Hoferbe oder die Hoferbin innerhalb von zwanzig Jahren nach dem Erbfall

1.  wesentliche Teile des Hofeszubehörs veräußert oder verwertet, es sei denn, dass dies im Rahmen einer ordnungsgemäßen Bewirtschaftung liegt, oder
2.  den Hof oder Teile davon auf andere Weise als land- oder forstwirtschaftlich nutzt

und dadurch erhebliche Gewinne erzielt.

(5) Von dem Erlös sind die durch die Veräußerung oder Verwertung entstehenden öffentlichen Abgaben, die vom Hoferben oder von der Hoferbin zu tragen sind, abzusetzen.
Erlösminderungen, die auf einer vom Hoferben oder von der Hoferbin aufgenommenen dinglichen Belastung des Hofes beruhen, sind dem erzielten Erlös hinzuzurechnen, es sei denn, dass die Aufnahme der Belastung im Rahmen einer ordnungsmäßigen Bewirtschaftung lag.
Ein Erlös, den zu erzielen der Hoferbe oder die Hoferbin wider Treu und Glauben unterlassen hat, wird hinzugerechnet.
Von dem Erlös ist der Teil abzusetzen, der bei wirtschaftlicher Betrachtungsweise auf eigenen Leistungen des Hoferben oder der Hoferbin beruht oder dessen Herausgabe aus anderen Gründen nicht der Billigkeit entsprechen würde.
Von dem Erlös ist abzusetzen ein Viertel des Erlöses, wenn die Veräußerung oder Verwertung später als zehn Jahre, die Hälfte des Erlöses, wenn sie später als fünfzehn Jahre nach dem Erbfall erfolgt.

(6) Veräußert oder verwertet der Hoferbe oder die Hoferbin innerhalb von zwanzig Jahren nach dem Erbfall einen Ersatzbetrieb, Ersatzgrundstücke oder Hofeszubehör, so sind die Vorschriften der Absätze 1 bis 5 sinngemäß anzuwenden.
Dies gilt auch, wenn der Ersatzbetrieb oder ein Ersatzgrundstück die Voraussetzungen des Absatzes 2 Satz 2 erfüllt.

(7) Veräußert oder verwertet eine andere Person, auf die der Hof im Wege der Erbfolge übergegangen oder der er im Wege der vorweggenommenen Erbfolge übereignet worden ist, innerhalb von zwanzig Jahren nach dem Erbfall (Absatz 1 Satz 1) den Hof, Teile des Hofes oder Hofeszubehör, so sind die Vorschriften der Absätze 1 bis 6 sinngemäß anzuwenden.

(8) Der Veräußerung stehen die Zwangsversteigerung und die Enteignung gleich.

(9) Die Ansprüche sind vererblich und übertragbar.
Sie verjähren mit Ablauf des dritten Jahres nach dem Zeitpunkt, in dem die berechtigte Person von dem Eintritt der Voraussetzungen des Anspruchs Kenntnis erlangt, spätestens in dreißig Jahren vom Erbfall an.
Sie entstehen auch, wenn die Besitzung im Grundbuch nicht als Hof eingetragen ist, und sie bestehen fort, wenn die Besitzung die Hofeigenschaft verliert.

(10) Die verpflichtete Person hat den berechtigten Personen über eine Veräußerung oder Verwertung unverzüglich Mitteilung zu machen sowie über alle für die Berechnung des Anspruchs erheblichen Umstände auf Verlangen Auskunft zu erteilen.

#### § 14 Stellung von überlebenden Ehegatten

(1) Dem überlebenden Ehegatten oder der überlebenden Ehegattin der Erblasserin oder der überlebenden Ehegattin oder dem überlebenden Ehegatten des Erblassers steht, wenn der Hoferbe oder die Hoferbin ein Abkömmling des Erblassers oder der Erblasserin ist, bis zur Vollendung des fünfundzwanzigsten Lebensjahres des Hoferben oder der Hoferbin die Verwaltung und Nutznießung am Hof zu.
Dieses Recht kann

1.  der Eigentümer oder die Eigentümerin durch Ehevertrag oder Verfügung von Todes wegen,
2.  das Gericht auf Antrag einer beteiligten Person aus wichtigem Grunde

verlängern, beschränken oder aufheben.

(2) Steht dem überlebenden Ehegatten oder der überlebenden Ehegattin die Verwaltung und Nutznießung nicht zu oder endet sie, so kann er oder sie, wenn eine Miterben- oder Pflichtteilsberechtigung gegeben ist, und wenn auf nach § 12 zustehende Ansprüche sowie auf alle Ansprüche aus der Verwendung eigenen Vermögens für den Hof verzichtet wird, vom Hoferben oder von der Hoferbin auf Lebenszeit den in solchen Verhältnissen üblichen Altenteil verlangen.
Der Altenteilsanspruch erlischt, wenn der überlebende Ehegatte oder die überlebende Ehegattin eine neue Ehe eingeht.
In diesem Fall kann vom Hoferben oder von der Hoferbin die Zahlung eines Kapitals verlangt werden, das dem Wert des Altenteils entspricht, jedoch nicht mehr als den Betrag, der dem Ehegatten oder der Ehegattin ohne Verzicht bei der Erbauseinandersetzung zugekommen sein würde.

(3) Der überlebende Ehegatte oder die überlebende Ehegattin kann, wenn ihm oder ihr der Eigentümer oder die Eigentümerin durch Verfügung von Todes wegen eine dahingehende Befugnis erteilt hat, unter den Abkömmlingen des Eigentümers oder der Eigentümerin den Hoferben oder die Hoferbin bestimmen.
Die Befugnis erlischt bei einer Wiederverheiratung oder wenn der gesetzliche Hoferbe oder die gesetzliche Hoferbin das fünfundzwanzigste Lebensjahr vollendet.
Die Bestimmung erfolgt durch mündliche Erklärung zur Niederschrift des Gerichts oder durch Einreichung einer öffentlich beglaubigten schriftlichen Erklärung; die Niederschrift wird nach den Vorschriften des Beurkundungsgesetzes errichtet.
Mit Abgabe der Erklärung tritt der neu bestimmte Hoferbe oder die neu bestimmte Hoferbin hinsichtlich des Hofes in die Rechtsstellung des bisherigen gesetzlichen Hoferben oder der bisherigen gesetzlichen Hoferbin ein.
Auf Antrag einer beteiligten Person regelt das Gericht, und zwar auch mit Wirkung gegenüber Dritten, die mit dem Übergang des Hofes zusammenhängenden Fragen.

#### § 15 Nachlassverbindlichkeiten

(1) Die hoferbenberechtigte Person haftet, auch wenn sie an dem übrigen Nachlass nicht als Miterbe oder Miterbin beteiligt ist, für die Nachlassverbindlichkeiten als Gesamtschuldner.

(2) Die Nachlassverbindlichkeiten einschließlich der auf dem Hof ruhenden Hypotheken, Grund- und Rentenschulden, aber ohne die auf dem Hof ruhenden sonstigen Lasten (zum Beispiel: Altenteil, Nießbrauch) sind, soweit das außer dem Hof vorhandene Vermögen dazu ausreicht, aus diesem zu berichtigen.

(3) Soweit die Nachlassverbindlichkeiten nicht nach Absatz 2 berichtigt werden können, ist der Hoferbe oder die Hoferbin den Miterben gegenüber verpflichtet, sie allein zu tragen und die Miterben von ihnen zu befreien.

(4) Verbleibt nach Berichtigung der Nachlassverbindlichkeiten ein Überschuss, so ist dieser auf die Miterben nach den Vorschriften des allgemeinen Rechts zu verteilen.
Der Hoferbe oder die Hoferbin kann eine Beteiligung an dem Überschuss nur dann und nur insoweit verlangen, als der auf ihn entfallende Anteil größer ist als der Hofeswert (§ 12 Absatz 2).

(5) Gehören zum Nachlass mehrere Höfe, so werden die Pflicht zur Abfindung der Miterben einschließlich der Leistungen nach § 12 Absatz 6 Satz 2 ebenso wie die Nachlassverbindlichkeiten von allen Hoferben gemeinschaftlich, und zwar im Verhältnis zueinander entsprechend den Hofeswerten getragen.

#### § 16 Verfügung von Todes wegen

(1) Der Eigentümer oder die Eigentümerin kann die Erbfolge kraft Höferechts (§ 4) durch Verfügung von Todes wegen nicht ausschließen.
Er oder sie kann die Erbfolge jedoch beschränken; soweit nach den Vorschriften des Grundstücksverkehrsgesetzes in der im Bundesgesetzblatt Teil III, Gliederungsnummer 7810-1, veröffentlichten bereinigten Fassung, das zuletzt durch Artikel 108 des Gesetzes vom 17.
Dezember 2008 (BGBl. I S. 2586, 2742) geändert worden ist, für ein Rechtsgeschäft unter Lebenden gleichen Inhalts eine Genehmigung erforderlich wäre, ist die Zustimmung des Gerichts zu der Verfügung von Todes wegen erforderlich.

(2) Für die Berechnung des Pflichtteils des Hoferben oder der Hoferbin ist der nach dem allgemeinen Recht, für die Berechnung des Pflichtteils der übrigen Erben der nach diesem Gesetz zu ermittelnde gesetzliche Erbteil maßgebend.
Dabei ist der Hof in jedem Falle nach dem in § 12 Absatz 2 bestimmten Wert anzusetzen.

#### § 17 Übergabevertrag

(1) Bei der Übergabe des Hofes an den Hoferben oder die Hoferbin im Wege der vorweggenommenen Hoferbfolge finden die Vorschriften des § 16 entsprechende Anwendung.

(2) Übergibt der Eigentümer oder die Eigentümerin den Hof an einen hoferbenberechtigten Abkömmling, so gilt zugunsten der anderen Abkömmlinge der Erbfall hinsichtlich des Hofes mit dem Zeitpunkt der Übertragung als eingetreten.

(3) Soweit nach den Vorschriften des Grundstücksverkehrsgesetzes eine Genehmigung erforderlich ist, wird sie durch das Gericht erteilt.

(4) Für die Genehmigung eines Übergabevertrages gelten die Vorschriften der §§ 29 bis 31 sinngemäß.

### Kapitel 2  
Verfahrensregelungen

#### § 18 Zuständigkeit der Gerichte und allgemeines Verfahrensrecht

(1) Für die Entscheidung über alle Anträge und Streitigkeiten, die sich bei Anwendung dieses Gesetzes ergeben, sowie aus Abmachungen der Beteiligten hierüber sind die im Gesetz über das gerichtliche Verfahren in Landwirtschaftssachen in der im Bundesgesetzblatt Teil III, Gliederungsnummer 317-1, veröffentlichten bereinigten Fassung, das zuletzt durch Artikel 8 des Gesetzes vom 27.
August 2017 (BGBl. I S. 3295, 3297) geändert worden ist, genannten Gerichte ausschließlich zuständig.

(2) Diese Gerichte sind auch zuständig für die Entscheidung der Frage, wer kraft Gesetzes oder kraft Verfügung von Todes wegen Hoferbe oder Hoferbin eines Hofes geworden ist, und für die Ausstellung eines Erbscheins oder eines Europäischen Nachlasszeugnisses.
In dem Erbschein oder dem Europäischen Nachlasszeugnis ist der Hoferbe oder die Hoferbin aufzuführen.
Auf Antrag einer beteiligten Person ist in dem Erbschein lediglich die Hoferbfolge zu bescheinigen.

(3) Die Entscheidung über die Erteilung, die Entziehung und die Kraftloserklärung eines Erbscheins kann ohne Zuziehung ehrenamtlicher Richter erfolgen.

(4) Auf das Verfahren sind die Vorschriften des Gesetzes über das gerichtliche Verfahren in Landwirtschaftssachen anzuwenden, soweit dieses Gesetz nichts anderes bestimmt.

(5) In den Fällen des § 13 ist das für den ursprünglichen Hof zuständige Landwirtschaftsgericht auch dann örtlich zuständig, wenn Ansprüche wegen der Veräußerung oder Verwertung eines Ersatzbetriebes oder von Ersatzgrundstücken geltend gemacht werden.

#### § 19 Eintragung und Ersuchen

(1) Wenn die Hofeigenschaft einer Besitzung aufgrund einer Erklärung des Eigentümers oder der Eigentümerin entsteht, ersucht das Landwirtschaftsgericht das Grundbuchamt um Eintragung eines Hofvermerks.
Ist ein negativer Hofvermerk eingetragen, wird dieser mit Eintragung des Hofvermerks gelöscht.

(2) Wenn die Hofeigenschaft einer Besitzung aufgrund einer Erklärung des Eigentümers oder der Eigentümerin entfällt, ersucht das Landwirtschaftsgericht das Grundbuchamt um Eintragung eines negativen Hofvermerks.
Ist ein Hofvermerk eingetragen, wird dieser mit Eintragung des negativen Hofvermerks gelöscht.

(3) Wenn eine Besitzung Hof ist und ein Hofvermerk nicht eingetragen ist, ersucht das Landwirtschaftsgericht aufgrund einer Erklärung des Eigentümers oder der Eigentümerin das Grundbuchamt um Eintragung eines Hofvermerks.

(4) Wenn eine Besitzung kein Hof ist und ein negativer Hofvermerk nicht eingetragen ist, ersucht das Landwirtschaftsgericht aufgrund einer Erklärung des Eigentümers oder der Eigentümerin das Grundbuchamt um Eintragung eines negativen Hofvermerks.
Ist ein Hofvermerk eingetragen, wird dieser mit Eintragung des negativen Hofvermerks gelöscht.

(5) Liegen Erkenntnisse vor, dass die Hofeigenschaft nicht mehr besteht, ersucht das Landwirtschaftsgericht von Amts wegen das Grundbuchamt um Löschung des Hofvermerks.

(6) Die Absätze 1 bis 5 gelten für die Ehegattenhöfe entsprechend.

(7) Über ein von ihm zu stellendes Ersuchen befindet das Landwirtschaftsgericht ohne Zuziehung ehrenamtlicher Richterinnen und Richter.

#### § 20 Erklärungen nach diesem Gesetz

(1) Die in diesem Gesetz vorgesehenen Erklärungen, dass eine Besitzung Hof oder Ehegattenhof sein soll oder nicht sein soll oder, dass ein Hofvermerk eingetragen werden soll, sind gegenüber dem Landwirtschaftsgericht abzugeben.

(2) Die Erklärung bedarf der öffentlichen Beglaubigung.

(3) Die Erklärung kann, solange die erforderliche Eintragung oder Löschung nicht bewirkt ist, bis zum Tode des oder der Erklärenden widerrufen werden; § 1 Absatz 6 Satz 1 gilt entsprechend.

#### § 21 Vermutung

Die Eintragung des Hofvermerks begründet die Vermutung, dass die Besitzung die durch den Vermerk ausgewiesene Eigenschaft hat.

#### § 22 Hofvermerk

(1) Der Hofvermerk wird in der Aufschrift des Grundbuchs des Hofes eingetragen und lautet:

„Hof gemäß dem Gesetz über die Höfeordnung für das Land Brandenburg.
Eingetragen am ...“.

(2) Beim Ehegattenhof lautet der Hofvermerk:

„Ehegattenhof gemäß dem Gesetz über die Höfeordnung für das Land Brandenburg.
Eingetragen am ...“.

(3) Der negative Hofvermerk lautet:

„Kein Hof gemäß dem Gesetz über die Höfeordnung für das Land Brandenburg.
Eingetragen am …“.

(4) Der negative Hofvermerk zum Ehegattenhof lautet:

„Kein Ehegattenhof gemäß dem Gesetz über die Höfeordnung für das Land Brandenburg.
Eingetragen am …“.

(5) Ist bei einem Ehegattenhof der Grundbesitz der Ehegatten nicht auf demselben Grundbuchblatt eingetragen, so ist im Hofvermerk wechselseitig auf den Grundbesitz des anderen Ehegatten hinzuweisen.
Der Hofvermerk lautet dementsprechend:

„Dieser Grundbesitz bildet mit dem im Grundbuch von ...
Bd.
...
Bl.
...
eingetragenen Grundbesitz einen Ehegattenhof gemäß dem Gesetz über die Höfeordnung für das Land Brandenburg.
Eingetragen am ...“.

(6) Gehört zum Hof ein Miteigentumsanteil, der auf einem anderen Grundbuchblatt eingetragen ist, so ist im Grundbuch des Hofes folgender Vermerk:

„Zum Hof gehört der im Grundbuch von ...
Bd.
...
Bl.
...
eingetragene Miteigentumsanteil.
Eingetragen am ...“

und im Grundbuch des Miteigentumsanteils folgender Vermerk:

„Der Miteigentumsanteil des ...
gehört zu dem im Grundbuch von ...
Bd.
...
Bl.
...
eingetragenen Hof.
Eingetragen am ...“

einzutragen.

#### § 23 Besonderes Grundbuchblatt

(1) Wird ein Hofvermerk in das Grundbuch eingetragen, so sind die zum Hof gehörenden Grundstücke desselben Eigentümers oder derselben Eigentümerin auf Ersuchen des Landwirtschaftsgerichts auf einem besonderen Grundbuchblatt einzutragen; das Ersuchen ist von Amts wegen zu stellen.

(2) Grundstücke, die nicht zum Hof gehören, sind nicht auf dem Grundbuchblatt des Hofes einzutragen.

(3) Werden einzelne Grundstücke vom Hof abgetrennt, so ist der Hofvermerk nicht mit zu übertragen.

#### § 24 Löschungsersuchen von Amts wegen

(1) Will das Landwirtschaftsgericht von Amts wegen um die Löschung eines Hofvermerks ersuchen, so hat es den Eigentümer oder die Eigentümerin von seiner Absicht sowie über die wesentlichen sich aus der Löschung ergebenden Folgen zu unterrichten und ihm oder ihr anheimzugeben, innerhalb einer bestimmten Frist die Feststellung der Hofeigenschaft (§ 27 Absatz 1 Nummer 1) zu beantragen.
Die Frist darf nicht weniger als sechs Wochen betragen.

(2) Das Ersuchen darf erst gestellt werden, wenn der Eigentümer oder die Eigentümerin einen Antrag auf Feststellung nicht gestellt oder zurückgenommen hat oder wenn rechtskräftig festgestellt worden ist, dass ein Hof im Sinne der höferechtlichen Vorschriften nicht vorliegt.

#### § 25 Benachrichtigung

Von der Eintragung und Löschung eines Hofvermerks sowie von der Abtrennung eines einzelnen Grundstücks (§ 23 Absatz 3) benachrichtigt das Grundbuchamt den Eigentümer oder die Eigentümerin, das Gericht und die Genehmigungsbehörde nach dem Grundstücksverkehrsgesetz.

#### § 26 Höfeakten

Das Ersuchen des Landwirtschaftsgerichts um Eintragung oder Löschung des Hofvermerks und sonstige höferechtlich erhebliche Vorgänge sind zu einer besonderen Höfeakte zu nehmen, die bei den Grundakten der Hofstelle aufzubewahren ist.

#### § 27 Feststellungsverfahren

(1) Auf Antrag eines oder einer Beteiligten, der oder die ein rechtliches Interesse an der Entscheidung glaubhaft macht, entscheidet das Landwirtschaftsgericht im Wege eines besonderen Feststellungsverfahrens,

1.  ob ein Hof im Sinne der höferechtlichen Vorschriften vorliegt oder vorgelegen hat,
2.  ob ein Hof ein Ehegattenhof im Sinne der höferechtlichen Vorschriften ist oder war,
3.  ob ein Gegenstand Bestandteil oder Zubehör eines Hofes ist,
4.  ob ein Hoferbe oder eine Hoferbin wirtschaftsfähig ist,
5.  ob für die Erbfolge in einen Hof Ältesten- oder Jüngstenrecht gilt,
6.  von wem der Hof stammt,
7.  wer nach dem Tode des Eigentümers oder der Eigentümerin eines Hofes Hoferbe oder Hoferbin geworden ist,
8.  über sonstige nach den höferechtlichen Vorschriften bestehende Rechtsverhältnisse.

(2) Das Gericht soll alle Personen, deren Rechte durch die Entscheidung betroffen werden können, von der Einleitung des Feststellungsverfahrens unter Hinweis auf die in § 28 Absatz 1 genannten Folgen benachrichtigen.
Entscheidungen in der Hauptsache sind auch diesen Personen zuzustellen.

(3) Jede der in Absatz 2 genannten Personen kann sich einem anhängigen Verfahren in jeder Instanz anschließen.
Die Anschließung kann mit der Einlegung der Beschwerde verbunden werden.

#### § 28 Abänderung der Entscheidung

(1) Ist im Feststellungsverfahren rechtskräftig entschieden worden, so können diejenigen, die sich am Verfahren beteiligt haben oder von dem Verfahren benachrichtigt worden sind (§ 27 Absatz 2 und 3), einen neuen Antrag nicht auf Tatsachen gründen, die in dem früheren Verfahren geltend gemacht worden sind oder von ihnen dort hätten geltend gemacht werden können.

(2) Im Übrigen kann ein neuer Antrag nur gestellt werden, wenn ein berechtigter Grund für die nochmalige Nachprüfung vorliegt.
In diesem Fall sind die an dem früheren Verfahren Beteiligten zuzuziehen und die in § 27 Absatz 2 genannten Personen zu benachrichtigen.
Führt die Nachprüfung zu einer abweichenden Entscheidung, so ist in der ergehenden Entscheidung gleichzeitig der frühere Beschluss aufzuheben.

(3) Nach Ablauf von fünf Jahren, vom Tag der Rechtskraft der Entscheidung an gerechnet, ist ein neuer Antrag auf Feststellung nur noch statthaft, wenn die bei der Entscheidung vorhanden gewesenen Voraussetzungen nachträglich weggefallen sind.

#### § 29 Zustimmungsverfahren

(1) Den Antrag auf Zustimmung zu einer Verfügung von Todes wegen kann der Erblasser oder die Erblasserin, zu einem Erbvertrag auch der oder die andere Vertragsschließende, stellen.

(2) Hat ein Notar oder eine Notarin die Verfügung beurkundet, so gilt er oder sie als ermächtigt, im Namen eines oder einer Antragsberechtigten die Genehmigung zu beantragen.

(3) Nach dem Tode des Erblassers oder der Erblasserin kann den Antrag jeder oder jede stellen, der oder die ein berechtigtes Interesse an der Entscheidung glaubhaft macht.

#### § 30 Beschwerdeberechtigung

Genehmigt das Landwirtschaftsgericht eine Verfügung von Todes wegen, durch die so viele Grundstücke vom Hof abgetrennt werden, dass er nach den höferechtlichen Vorschriften seine Eigenschaft als Hof verliert, so ist von den Hoferbenberechtigten nur der nächstberufene hoferbenberechtigte Abkömmling beschwerdeberechtigt.
Diesem steht derjenige Abkömmling gleich, der zulässigerweise durch Erbvertrag oder gemeinschaftliches Testament als Hoferbe oder Hoferbin bestimmt ist.

#### § 31 Entscheidung im Zustimmungsverfahren

(1) Entscheidet das Landwirtschaftsgericht rechtskräftig, dass eine Zustimmung nicht erforderlich ist, so steht diese Entscheidung der Zustimmung gleich.

(2) Die Zustimmung kann unter einer Auflage oder Bedingung erteilt werden.
Sie wird erst mit der Rechtskraft der Entscheidung wirksam.

#### § 32 Anpassungsverfahren

(1) Rechte, die aufgrund früherer anerbenrechtlicher Vorschriften entstanden sind, können, falls in diesem Gesetz gleiche oder ähnliche Rechte nicht vorgesehen sind, auf Antrag eines oder einer Beteiligten abgeändert oder umgewandelt werden, wenn dies zur Vermeidung grober Unbilligkeiten offenbar erforderlich erscheint; dabei kann das Landwirtschaftsgericht die Rechtsverhältnisse unter den Beteiligten auch mit Wirkung gegen Dritte regeln.

(2) Unbeschadet des Absatzes 1 können die Beteiligten vom Hofeigentümer oder von der Hofeigentümerin verlangen, dass Versorgungsrechte, die aufgrund früherer anerbenrechtlicher Vorschriften entstanden oder durch Übergabevertrag oder durch sonstige Vereinbarungen begründet worden sind, in das Grundbuch eingetragen werden.

### Kapitel 3  
Übergangs- und Schlussbestimmungen

#### § 33 Übergangsvorschriften

(1) Bis einschließlich 31.
Dezember 2023 kann eine Besitzung nur Hof werden, wenn der Eigentümer oder die Eigentümerin erklärt, dass die Besitzung Hof im Sinne dieses Gesetzes sein soll und der Hofvermerk im Grundbuch eingetragen wird.

(2) Die Landesregierung erstattet dem Landtag zum 30.
Juni 2022 Bericht über die Erfahrungen bei der Umsetzung dieses Gesetzes.

#### § 34 Inkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 19.
Juni 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark