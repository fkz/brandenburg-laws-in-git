## Geschäftsordnung des Landtages Brandenburg

Der Landtag Brandenburg gibt sich gemäß Artikel 68 der Verfassung des Landes Brandenburg folgende Geschäftsordnung:

**Inhaltsübersicht**

### Abschnitt 1  
Konstituierung des Landtages

[§ 1 Einberufung nach der Neuwahl](#1)

[§ 2 Konstituierende Sitzung](#2)

### Abschnitt 2  
Die Mitglieder des Landtages

[§ 3 Teilnahme an Sitzungen](#3)

[§ 4 Ausweis der Mitglieder des Landtages und Angaben für die amtliche Veröffentlichung](#4)

[§ 5 Plätze der Mitglieder des Landtages im Plenarsaal](#5)

[§ 6 Akteneinsicht](#6)

[§ 7 Geheim- und Datenschutz](#7)

### Abschnitt 3  
Fraktionen und Gruppen

[§ 8 Fraktionen und Gruppen](#8)

[§ 9 Reihenfolge der Fraktionen](#9)

[§ 10 Gremienbesetzung durch die Fraktionen und Gruppen](#10)

### Abschnitt 4  
Die Präsidentin oder der Präsident und das Präsidium

[§ 11 Wahl und Zusammensetzung des Präsidiums](#11)

[§ 12 Aufgaben der Präsidentin oder des Präsidenten](#12)

[§ 13 Vertretung der Präsidentin oder des Präsidenten](#13)

[§ 14 Einberufung des Präsidiums, Beratungen, Protokolle](#14)

[§ 15 Aufgaben des Präsidiums](#15)

[§ 16 Sitzungspräsidium](#16)

### Abschnitt 5  
Sitzungen des Landtages

[§ 17 Einberufung](#17)

[§ 18 Tagesordnung](#18)

[§ 19 Öffentlichkeit der Sitzungen](#19)

[§ 20 Sitzungsleitung und Erledigung vor Eintritt in die Tagesordnung](#20)

[§ 21 Eröffnung der Aussprache](#21)

[§ 22 Schluss der Aussprache](#22)

[§ 23 Unterbrechung und vorzeitige Beendigung der Sitzung](#23)

[§ 24 Neue Sitzung am selben Tag](#24)

### Abschnitt 6  
Redeordnung

[§ 25 Rederecht, Wortmeldung und Worterteilung](#25)

[§ 26 Reihenfolge der Redebeiträge](#26)

[§ 27 Zur Geschäftsordnung](#27)

[§ 28 Rededauer](#28)

[§ 29 Fragen zu einem Redebeitrag, Kurzintervention](#29)

[§ 30 Zutrittsrecht und Anwesenheitspflicht der Mitglieder der Landesregierung](#30)

[§ 31 Rederecht der Mitglieder der Landesregierung](#31)

[§ 32 Zutrittsrecht und Worterteilung an Dritte](#32)

### Abschnitt 7  
Ordnungsbestimmungen

[§ 33 Leitung der Sitzung](#33)

[§ 34 Ordnungsmaßnahmen der Präsidentin oder des Präsidenten gegenüber Mitgliedern des Landtages](#34)

[§ 35 Gröbliche Verletzung der parlamentarischen Ordnung oder Würde](#35)

[§ 36 Einspruch gegen Sachruf, Ordnungsruf oder Ausschließung](#36)

[§ 37 Weitere Ordnungsgewalt der Präsidentin oder des Präsidenten](#37)

[§ 38 Ordnungsmaßnahmen gegenüber der Zuhörerschaft](#38)

[§ 39 Unterbrechung und Schließung der Sitzung](#39)

### Abschnitt 8  
Gesetzgebungsverfahren, Behandlung der Beratungsgegenstände

[§ 40 Einbringung von Beratungsmaterialien](#40)

[§ 41 Zurückweisung von Beratungsmaterialien](#41)

[§ 42 Beratungsbeginn und Beratungsverfahren](#42)

[§ 43 Dringlichkeitsanträge](#43)

[§ 44 Erste Lesung](#44)

[§ 45 Zweite Lesung](#45)

[§ 46 Dritte Lesung](#46)

[§ 47 Weitere Lesung](#47)

[§ 48 Änderungsanträge](#48)

[§ 49 Rücknahme von Beratungsmaterialien](#49)

[§ 50 Zustimmungsgesetze zu Staatsverträgen](#50)

[§ 51 Berichte über akustische Wohnraumüberwachung](#51)

[§ 52 Vorlagen nach der Landeshaushaltsordnung, dem Haushaltsgesetz und sonstige Vorlagen](#52)

[§ 53 Berichte der Landesbeauftragten im Sinne des Artikels 74 der Verfassung des Landes Brandenburg](#53)

[§ 54 Immunitätsangelegenheiten und Genehmigungen zur Zeugenvernehmung nach § 50 Absatz 3 StPO und § 382 Absatz 3 ZPO](#54)

[§ 55 Angelegenheiten der Verfassungsgerichtsbarkeit](#55)

[§ 55a Verfahren nach dem Infektionsschutzbeteiligungsgesetz](#55a)

### Abschnitt 9  
Große und Kleine Anfragen, Fragestunde und Aktuelle Stunde

[§ 56 Einbringung von Großen Anfragen](#56)

[§ 57 Behandlung von Großen Anfragen](#57)

[§ 58 Kleine Anfragen](#58)

[§ 59 Ablehnung der schriftlichen Beantwortung](#59)

[§ 60 Fragestunde und Aktuelle Stunde](#60)

### Abschnitt 10  
Beschlussfähigkeit und Abstimmungen

[§ 61 Beschlussfähigkeit des Landtages](#61)

[§ 62 Anzweiflung der Beschlussfähigkeit](#62)

[§ 63 Schließung der Sitzung bei Beschlussunfähigkeit](#63)

[§ 64 Abstimmung]("#64)

[§ 65 Reihenfolge der Abstimmung](#65)

[§ 66 Abstimmungsregeln](#66)

[§ 67 Namentliche Abstimmung](#67)

[§ 68 Unzulässigkeit der namentlichen Abstimmung](#68)

[§ 69 Feststellung des Abstimmungsergebnisses](#69)

[§ 70 Persönliche Bemerkungen und Erklärungen zur Abstimmung](#70)

[§ 71 Abstimmungen über Anträge mit Wahl- oder Abwahlvorschlag](#71)

[§ 72 Mitgliedschaft in Gremien](#72)

### Abschnitt 11  
Die Ausschüsse und Kommissionen

[§ 73 Bestellung der Ausschüsse](#73)

[§ 74 Besetzung der Ausschüsse](#74)

[§ 75 Aufgaben der Ausschüsse](#75)

[§ 76 Überweisung an mehrere Ausschüsse](#76)

[§ 77 Einberufung und Durchführung der Ausschusssitzungen, Pressekonferenzen der Ausschüsse](#77)

[§ 77a Durchführung von Ausschusssitzungen unter Zuschaltung von Mitgliedern](#77a)

[§ 78 Berichterstattung der Ausschüsse an den Landtag](#78)

[§ 79 Teilnahme der Mitglieder des Landtages an Ausschusssitzungen](#79)

[§ 80 Öffentliche Sitzungen](#80)

[§ 80a Nichtöffentliche Sitzungen](#80a)

[§ 80b Beratung über geheim zu haltende Beratungsgegenstände](#80b)

[§ 80c Ausschluss eines Mitgliedes des Landtages wegen unerlaubter Preisgabe schutzwürdiger Daten und Informationen](#80c)

[§ 81 Anhörungen und Fachgespräche](#81)

[§ 82 Anwesenheitspflicht und Zutrittsrecht](#82)

[§ 83 Ausschussprotokoll](#83)

[§ 84 Wahlprüfungsausschuss](#84)

[§ 85 Petitionsausschuss](#85)

[§ 86 Untersuchungsausschüsse](#86)

[§ 87 Kommissionen](#87)

### Abschnitt 12  
Rat für Angelegenheiten der Sorben/Wenden beim Landtag

[§ 88 Berufung der Mitglieder und Konstituierung](#88)

[§ 89 Aufgaben und Rechte](#89)

[§ 90 Unterstützung durch die Landtagsverwaltung](#90)

### Abschnitt 13  
Sonderregelungen nach der Verfassung des Landes Brandenburg

[§ 91 Verfahren bei der Wahl der Verfassungsrichterinnen und Verfassungsrichter](#91)

[§ 92 Verfahren bei der Wahl der Mitglieder des Landesrechnungshofes](#92)

[§ 93 Verfahren bei der Wahl der oder des Landesbeauftragten für den Datenschutz und für das Recht auf Akteneinsicht](#93)

[§ 94 Verfahren nach Artikel 94 der Verfassung des Landes Brandenburg, sonstige Informationen über Vorhaben der Europäischen Union](#94)

### Abschnitt 14  
Niederschrift der Beratungen und Beurkundung ihrer Ergebnisse

[§ 95 Plenarprotokoll](#95)

[§ 96 Beschlussprotokoll](#96)

[§ 97 Ausfertigung und Zustellung](#97)

### Abschnitt 15  
Sonstige Bestimmungen

[§ 98 Verfahren zur Behandlung von Gegenständen gemäß Volksabstimmungsgesetz](#98)

[§ 99 Informationen der Präsidentin oder des Präsidenten sowie Zuschriften](#99)

[§ 100 Abweichungen von der Geschäftsordnung](#100)

[§ 101 Auslegung der Geschäftsordnung](#101)

[§ 102 Fristenberechnung](#102)

[§ 103 Eilverfahren im Präsidium und in den Ausschüssen](#103)

[§ 104 Behandlung unerledigter Beratungsmaterialien am Ende der Wahlperiode](#104)

[Anlage 1 Festlegung der Rededauer während der Plenarsitzung des Landtages Brandenburg](#110)

[Anlage 2 Richtlinie für die Fragestunde](#110)

[Anlage 3 Richtlinie für die Aktuelle Stunde](#110)

[Anlage 4 Datenschutzordnung des Landtages Brandenburg](#110)

[Anlage 5 Verschlusssachenordnung des Landtages Brandenburg](#110)

[Anlage 6 Immunitätsrichtlinie des Landtages Brandenburg zu Artikel 58 der Verfassung des Landes Brandenburg](#110)

[Anlage 7 Wahlordnung](#110)

[Anlage 8 Ordnung über Geheimhaltungspflichten und das Verfahren im Zusammenhang mit der Überprüfung von Mitgliedern des Landtages nach § 27 des Abgeordnetengesetzes](#110)

[Anlage 9 Verfahren bei der Einbringung, Verteilung und Veröffentlichung von Beratungsmaterialien und anderen Parlamentsdokumenten](#110)

[Anlage 10 Führung eines Lobbyregisters](#110)

[Anlage 11 Einsichtnahme in Protokolle, Veröffentlichung](#110)

### Abschnitt 1  
Konstituierung des Landtages

#### § 1 Einberufung nach der Neuwahl

Die Präsidentin oder der Präsident des bisherigen Landtages beruft die Mitglieder des Landtages zur konstituierenden Sitzung des Landtages ein.

#### § 2 Konstituierende Sitzung

(1) Die konstituierende Sitzung des Landtages wird bis zur Wahl der Präsidentin oder des Präsidenten von dem ältesten anwesenden Mitglied des Landtages geleitet, das bereit ist, diese Aufgabe zu übernehmen; es überträgt zwei Mitgliedern des Landtages vorläufig die Aufgabe der Schriftführung.

(2) Die konstituierende Sitzung beginnt mit dem Namensaufruf der Mitglieder des Landtages.

### Abschnitt 2  
Die Mitglieder des Landtages

#### § 3 Teilnahme an Sitzungen

(1) Die Mitglieder des Landtages sind grundsätzlich verpflichtet, an den Sitzungen des Landtages teilzunehmen.
Wer nicht oder nicht rechtzeitig an der Sitzung teilnehmen kann, hat dies der Präsidentin oder dem Präsidenten vor der Sitzung unter Angabe des Grundes anzuzeigen.

(2) Für die Sitzungen des Landtages, des Präsidiums, der Ausschüsse und der sonstigen parlamentarischen Gremien werden Anwesenheitslisten ausgelegt, in die sich jedes Mitglied persönlich einzutragen hat.
Zum Zwecke des Mutterschutzes und der Kinderbetreuung oder der Pflege Angehöriger sind die Mitglieder des Landtages für längstens sechs Monate befreit, wenn sie dies der Präsidentin oder dem Präsidenten anzeigen.
Längere Befreiungen sind dem Präsidium anzuzeigen.
Eine Befreiung auf unbestimmte Zeit kann nicht angezeigt werden.

(3) Die Anwesenheitslisten zu Landtagssitzungen werden den Plenarprotokollen als Anlagen beigefügt.

#### § 4 Ausweis der Mitglieder des Landtages und Angaben für die amtliche Veröffentlichung

(1) Die Mitglieder des Landtages erhalten für die Dauer ihrer Zugehörigkeit zum Landtag einen Ausweis über ihre Mitgliedschaft.

(2) Notwendige Angaben für die amtliche Veröffentlichung des Landtages sind gemäß § 26 des Abgeordnetengesetzes zu machen.

#### § 5 Plätze der Mitglieder des Landtages im Plenarsaal

Die Plätze der Mitglieder des Landtages im Plenarsaal bestimmt das Präsidium.

#### § 6 Akteneinsicht

(1) Die Mitglieder des Landtages sind berechtigt, alle Akten und Unterlagen einzusehen, die sich in der Verwahrung des Landtages befinden, und sodann im Einzelfall Kopien davon anzufordern.
Satz 1 gilt nur, soweit nicht gesetzliche Vorschriften oder Geheimhaltungs- oder sonstige schutzwürdige Interessen dem entgegenstehen.
Die Vorschriften der Richtlinie über die Rechtsstellung, Aufgaben und Arbeitsweise des Parlamentarischen Beratungsdienstes des Landtages Brandenburg bleiben unberührt.

(2) Die Einsichtnahme in Verwaltungsvorgänge, die das einzelne Mitglied des Landtages persönlich betreffen, ist nur diesem gestattet.

#### § 7 Geheim- und Datenschutz

Die Verschlusssachenordnung (Anlage 5) des Landtages regelt die Behandlung aller Angelegenheiten, die durch besondere Sicherungsmaßnahmen gegen die Kenntnisnahme durch Unbefugte geschützt werden müssen.
Der Landtag erlässt unter Berücksichtigung seiner verfassungsrechtlichen Stellung und der Grundsätze der Datenschutz-Grundverordnung und des Brandenburgischen Datenschutzgesetzes eine Datenschutzordnung (Anlage 4).
Die Einsichtnahme in Protokolle von Ausschüssen, Enquete-Kommissionen sowie des Rates für Angelegenheiten der Sorben/Wenden richtet sich nach Anlage 11.

### Abschnitt 3  
Fraktionen und Gruppen

#### § 8 Fraktionen und Gruppen

(1) Die Bildung und die Rechtsstellung der Fraktionen und Gruppen richtet sich nach dem Fraktionsgesetz.

(2) Die Namen der Vorsitzenden der Fraktionen, ihrer Stellvertreterinnen und Stellvertreter, der Parlamentarischen Geschäftsführerinnen und Geschäftsführer und der weiteren Mitglieder des jeweiligen Fraktionsvorstandes sowie der Personen, die eine Gruppe rechtsgeschäftlich vertreten, sind der Präsidentin oder dem Präsidenten schriftlich mitzuteilen.

#### § 9 Reihenfolge der Fraktionen

Die Reihenfolge der Fraktionen richtet sich nach ihrer Stärke.
Bei gleicher Zahl entscheidet das Los, das von der Präsidentin oder dem Präsidenten in einer Sitzung des Präsidiums gezogen wird.
Unbesetzte Mandate werden bis zur Neubesetzung bei der Fraktion mitgezählt, der die ausgeschiedenen Mitglieder des Landtages angehörten.

#### § 10 Gremienbesetzung durch die Fraktionen und Gruppen

(1) Die Besetzung des Präsidiums und der Ausschüsse durch die Fraktionen und Gruppen erfolgt nach dem Verfahren Hare/Niemeyer (Proporzverfahren); jedoch hat jede Fraktion das Recht, mit mindestens einem Mitglied in jedem Ausschuss, und jede Gruppe das Recht, mindestens in der ihrer Mitgliederzahl entsprechenden Anzahl von Ausschüssen vertreten zu sein.
Veränderungen des Stärkeverhältnisses sollen bei der Zusammensetzung der Ausschüsse berücksichtigt werden.

(2) Bei der Besetzung des Präsidiums werden die Präsidentin oder der Präsident und die Vizepräsidentinnen oder Vizepräsidenten der jeweiligen Fraktion, der sie angehören, angerechnet.

### Abschnitt 4  
Die Präsidentin oder der Präsident und das Präsidium

#### § 11 Wahl und Zusammensetzung des Präsidiums

(1) Der Landtag wählt in der konstituierenden Sitzung aus seiner Mitte in getrennten Wahlgängen die Präsidentin oder den Präsidenten, die Vizepräsidentinnen oder Vizepräsidenten und die weiteren Mitglieder des Präsidiums.
Die Zahl der weiteren Mitglieder wird durch Beschluss des Landtages bestimmt.
Jede Fraktion ist berechtigt, im Präsidium vertreten zu sein.

(2) Präsidentin oder Präsident, Vizepräsidentinnen oder Vizepräsidenten sowie die anderen Mitglieder des Präsidiums können durch Beschluss des Landtages abgewählt werden.
Die Abwahl ist gültig, wenn zwei Drittel der Mitglieder des Landtages zugestimmt haben.
Ein Antrag auf Abwahl kann von mindestens einem Fünftel der Mitglieder des Landtages schriftlich eingebracht werden.
Die Behandlung des Antrages auf Abwahl im Landtag darf frühestens 48 Stunden nach Eingang des Antrages erfolgen.

#### § 12 Aufgaben der Präsidentin oder des Präsidenten

(1) Die Präsidentin oder der Präsident vertritt den Landtag nach außen.
Sie oder er ernennt und entlässt die Beschäftigten des Landtages.
Sie oder er übt das Hausrecht und die Polizeigewalt im Landtagsgebäude aus.
Eine Durchsuchung oder Beschlagnahme in den Räumen des Landtages darf nur mit Einwilligung der Präsidentin oder des Präsidenten vorgenommen werden.

(2) Die Präsidentin oder der Präsident wahrt die Würde und Rechte des Landtages, fördert seine Arbeiten, leitet die Verhandlungen gerecht und unparteiisch und wahrt die Ordnung des Hauses.
Sie oder er hat beratende Stimme in den Ausschüssen, Enquete-Kommissionen und dem Rat für Angelegenheiten der Sorben/Wenden.

(3) Die Präsidentin oder der Präsident verfügt über die Einnahmen und Ausgaben des Landtages nach Maßgabe des Haushaltsplanes.

(4) Die Präsidentin oder der Präsident wirkt darauf hin, dass der Landtag ein familienfreundliches Parlament ist.
Sie oder er spricht Empfehlungen für familienfreundliche Sitzungszeiten aus und prüft weitere Verfahren für die Umsetzung eines familienfreundlichen Parlaments.

#### § 13 Vertretung der Präsidentin oder des Präsidenten

(1) Die Präsidentin oder der Präsident wird im Falle ihrer oder seiner Verhinderung durch die Vizepräsidentinnen oder Vizepräsidenten vertreten.
Die Präsidentin oder der Präsident bestimmt im Benehmen mit den Vizepräsidentinnen oder Vizepräsidenten die Vertretung.
Im Falle der Vakanz der Präsidentschaft erfolgt die Vertretung durch die Vizepräsidentinnen oder Vizepräsidenten in der Reihenfolge der Stärke der Fraktionen.
Mit der Vertretung nach Satz 3 sind die Rechte und Pflichten aus Artikel 69 Absatz 4 Satz 1 und 4 und Artikel 81 Absatz 1 der Verfassung des Landes Brandenburg sowie alle weiteren Rechte und Pflichten, die der Präsidentin oder dem Präsidenten aus der Verfassung des Landes Brandenburg, Gesetz und Geschäftsordnung des Landtages zugewiesen sind, verbunden, mit Ausnahme der Rechte und Pflichten, die der Direktorin oder dem Direktor des Landtages in ständiger Vertretung nach Absatz 3 aus Artikel 69 Absatz 4 Satz 2, 3 und 5 der Verfassung des Landes Brandenburg obliegen.
Sind die Präsidentin oder der Präsident und die Vizepräsidentinnen oder Vizepräsidenten verhindert, geht das Vertretungsrecht auf die anderen Mitglieder des Präsidiums in der Reihenfolge der Stärke der Fraktionen über; ausgenommen hiervon sind die Fraktionen, die die Präsidentin oder den Präsidenten und die Vizepräsidentinnen oder Vizepräsidenten stellen.
Bei repräsentativen Anlässen kann sich die Präsidentin oder der Präsident ausnahmsweise auch durch eine Ausschussvorsitzende oder einen Ausschussvorsitzenden vertreten lassen, soweit ein inhaltlicher Bezug zur Ausschusstätigkeit besteht und die Vizepräsidentinnen oder Vizepräsidenten verhindert sind.

(2) In den Fällen der Anlage 2 Nummer 2 Satz 2 und Anlage 3 Nummer 2 Satz 4 werden die Präsidentin oder der Präsident und die Vizepräsidentinnen oder Vizepräsidenten im Falle ihrer Verhinderung jeweils durch ein Mitglied des Präsidiums vertreten, welches der gleichen Fraktion angehört.

(3) Die ständige Vertretung der Präsidentin oder des Präsidenten in der Landtagsverwaltung ist die Direktorin oder der Direktor des Landtages.
Sie oder er hat Zutritt zu den Sitzungen des Präsidiums, der Ausschüsse, der Enquete-Kommissionen und des Rates für Angelegenheiten der Sorben/Wenden.

#### § 14 Einberufung des Präsidiums, Beratungen, Protokolle

(1) Die Präsidentin oder der Präsident beruft das Präsidium ein und leitet seine Beratungen.

(2) Das Präsidium ist unverzüglich einzuberufen, wenn ein Fünftel seiner Mitglieder es unter Angabe des Beratungsgegenstandes gemäß Anlage 9 § 5 beantragt.

(3) Die Beratungen des Präsidiums sind nichtöffentlich.
Bei seinen Beratungen muss mehr als die Hälfte der Mitglieder anwesend sein.
Das Präsidium beschließt über die Verteilung der Einladungen sowie den Inhalt, die Einsichtnahme in die und die Verteilung der Protokolle der Präsidiumssitzungen.

#### § 15 Aufgaben des Präsidiums

(1) Das Präsidium hat die Aufgabe, die Präsidentin oder den Präsidenten bei der Führung der Geschäfte zu unterstützen und die Verständigung zwischen den Fraktionen herbeizuführen.
Es beschließt den Sitzungsplan, den Terminplan für das jeweilige Kalenderjahr sowie den Entwurf der Tagesordnung für die jeweilige Plenarsitzung.

(2) Das Präsidium beschließt über die allgemeinen Angelegenheiten der Mitglieder des Landtages und der Landtagsverwaltung, soweit sie nicht der Präsidentin oder dem Präsidenten vorbehalten oder anderweitig geregelt sind.
Insbesondere stellt es den Voranschlag des Haushaltsplanes für den Landtag fest.

(3) Das Präsidium entscheidet über Ermächtigungen im Sinne des § 90b Absatz 2 und des § 194 Absatz 4 StGB.

#### § 16 Sitzungspräsidium

(1) Das Sitzungspräsidium des Landtages besteht aus der amtierenden Präsidentin oder dem amtierenden Präsidenten sowie den amtierenden Schriftführerinnen und Schriftführern.

(2) Die Schriftführerinnen und Schriftführer, deren Zahl durch das Präsidium bestimmt wird, werden von den Fraktionen benannt.

(3) Die Schriftführerinnen und Schriftführer unterstützen die Präsidentin oder den Präsidenten.
Sie beurkunden die Beratungen, führen die Redeliste und sind der Präsidentin oder dem Präsidenten bei der Feststellung der Abstimmungsergebnisse behilflich.
Die Präsidentin oder der Präsident bestimmt ihren Einsatz und kann sie mit weiteren Aufgaben betrauen.

### Abschnitt 5  
Sitzungen des Landtages

#### § 17 Einberufung

(1) Der Landtag wird durch die Präsidentin oder den Präsidenten einberufen.

(2) Der Landtag ist unverzüglich einzuberufen, wenn mindestens ein Fünftel der Mitglieder des Landtages oder die Landesregierung dies unter Angabe des Beratungsgegenstandes entsprechend Anlage 9 §§ 1 bis 3 beantragt.

#### § 18 Tagesordnung

(1) Das Präsidium soll spätestens am siebenten Tag vor der Plenarsitzung den Entwurf der Tagesordnung für die Sitzung des Landtages beschließen.
Das Präsidium, die Ausschüsse, die Enquete-Kommissionen und der Rat für Angelegenheiten der Sorben/Wenden sind aufgefordert, auf familienfreundliche Sitzungszeiten zu achten und dies bei der Erstellung der Tagesordnungen zu berücksichtigen.
Der Entwurf der Tagesordnung gemäß Satz 1 wird gemäß Anlage 9 § 4 an die Mitglieder des Landtages, die Fraktionen, die Gruppen, die Mitglieder der Landesregierung, die Präsidentin oder den Präsidenten des Landesrechnungshofes, die Landesbeauftragten im Sinne von Artikel 74 der Verfassung des Landes Brandenburg sowie den Rat für Angelegenheiten der Sorben/Wenden unverzüglich nach der Beschlussfassung verteilt.

(2) Für die Unterteilung der Tagesordnung gilt grundsätzlich folgende Reihenfolge:

1.  Aktuelle Stunde,
2.  Fragestunde,
3.  Lesung von Gesetzentwürfen,
4.  Große Anfragen,
5.  Berichte der Landesregierung aufgrund eines Landtagsbeschlusses oder gesetzlicher Vorschriften,
6.  Anträge und selbstständige Entschließungsanträge,
7.  sonstige Beratungsgegenstände.

Regierungserklärungen sowie Haushalts- und Nachtragshaushaltsgesetzentwürfe werden in der Regel anstelle der Aktuellen Stunde behandelt.

(3) Abweichend von Absatz 2 kann jede Fraktion und Gruppe einen Beratungsgegenstand, der in einer der Sitzungen der nächsten regulären Plenarsitzungswoche behandelt werden soll, als Priorität anmelden.
Die Priorität soll bis zum Dienstag der der Plenarsitzungswoche vorausgehenden Woche, spätestens jedoch bis zum Beginn der Sitzung des Präsidiums, in der die Beschlussfassung gemäß Absatz 1 Satz 1 erfolgt, angemeldet werden.
Die angemeldeten Prioritäten werden nach Aktueller Stunde und Fragestunde in einem Prioritätenblock behandelt.
Werden Prioritäten für den Folgetag einer mehrtägigen Sitzung angemeldet, werden sie zu Beginn dieses Sitzungstages behandelt.
Die Reihenfolge der für einen Sitzungstag angemeldeten Prioritäten richtet sich nach der Stärke der Fraktionen und Gruppen.

(4) Zu Beginn einer jeden Sitzung beschließt der Landtag die Tagesordnung.

#### § 19 Öffentlichkeit der Sitzungen

(1) Der Landtag verhandelt öffentlich; die Sitzungen des Landtages werden live im Internet übertragen.
Die Öffentlichkeit kann mit den Stimmen von zwei Dritteln der anwesenden Mitglieder des Landtages ausgeschlossen werden.
Über den Antrag ist in nichtöffentlicher Sitzung zu entscheiden.
Bei Ausschluss der Öffentlichkeit ist eine öffentliche Begründung zu geben.

(2) Tagesordnungspunkte, die Regierungserklärungen, Aktuelle Stunden, Fragestunden und Prioritäten enthalten, sowie Beratungen, welche die Belange von Menschen mit Behinderungen berühren, werden durch eine Gebärdensprachdolmetscherin oder einen Gebärdensprachdolmetscher begleitet.

#### § 20 Sitzungsleitung und Erledigung vor Eintritt in die Tagesordnung

(1) Die Präsidentin oder der Präsident eröffnet, leitet und schließt die Sitzung.

(2) Vor Eintritt in die Tagesordnung werden die Anzeigen der Präsidentin oder des Präsidenten behandelt:

1.  das Eintreten und Ausscheiden von Mitgliedern des Landtages,
2.  die Namen der Fraktionsvorsitzenden, ihrer Stellvertreterinnen und Stellvertreter, der Parlamentarischen Geschäftsführerinnen und Geschäftsführer, der Name der Person, die eine Gruppe rechtsgeschäftlich vertritt, sowie Veränderungen in der Mitgliedschaft in den Fraktionen oder Gruppen,
3.  die Mitteilungen der Präsidentin oder des Präsidenten über die in den Ausschüssen erfolgten Wahlen,
4.  Abwesenheit von Mitgliedern der Landesregierung und deren Vertretung.

#### § 21 Eröffnung der Aussprache

(1) Die Präsidentin oder der Präsident ruft jeden Beratungsgegenstand auf und eröffnet die Aussprache.

(2) Der Landtag kann beschließen, die Beratung eines einzelnen Gegenstandes bis zur nächsten Sitzung zu unterbrechen.
Eine erneute Unterbrechung der Beratung ist nur mit Zustimmung der Antragstellerinnen und Antragsteller möglich.

#### § 22 Schluss der Aussprache

Ist die Redeliste erschöpft oder meldet sich niemand zu Wort, erklärt die Präsidentin oder der Präsident die Aussprache für geschlossen.

#### § 23 Unterbrechung und vorzeitige Beendigung der Sitzung

(1) Die Präsidentin oder der Präsident bestimmt, ob die Sitzung unterbrochen wird und wann sie wiederbeginnt.

(2) Vor Erledigung der Tagesordnung kann die Sitzung nur geschlossen werden, wenn es der Landtag auf Vorschlag der Präsidentin oder des Präsidenten oder auf Antrag beschließt.

#### § 24 Neue Sitzung am selben Tag

Wird für denselben Tag eine neue Sitzung mit derselben Tagesordnung anberaumt, genügt hierfür die mündliche Mitteilung durch die Präsidentin oder den Präsidenten.
Die Präsidentin oder der Präsident kann in diesem Falle einen Gegenstand, über den nicht abgestimmt werden konnte, an eine andere Stelle der Tagesordnung setzen oder von ihr absetzen.

### Abschnitt 6  
Redeordnung

#### § 25 Rederecht, Wortmeldung und Worterteilung

(1) Die Mitglieder des Landtages haben Rederecht.
Es darf nur nach Maßgabe dieser Geschäftsordnung begrenzt werden.

(2) Ein Mitglied des Landtages darf nur sprechen, wenn es sich zu Wort gemeldet hat und ihm das Wort erteilt wurde.

(3) Will sich die Präsidentin oder der Präsident an der Aussprache beteiligen, gibt sie oder er für diese Zeit die Sitzungsleitung ab.

#### § 26 Reihenfolge der Redebeiträge

(1) Die Präsidentin oder der Präsident legt die Reihenfolge der Redebeiträge fest.
Dabei soll sie oder er die Sorge für eine sachgemäße Erledigung und zweckmäßige Gestaltung der Beratung, die Rücksicht auf die verschiedenen Fraktionen, auf Rede und Gegenrede und auf die Stärke der Fraktionen leiten.

(2) Wer einen Antrag stellt, kann sowohl zu Beginn als auch zum Schluss der Aussprache das Wort erhalten.
Die Aussprache soll in der Regel durch eine Vertreterin oder einen Vertreter entgegengesetzter Auffassung fortgeführt werden.

(3) Nach der Rede der Ministerpräsidentin oder des Ministerpräsidenten soll die oder der Vorsitzende der zahlenmäßig stärksten Oppositionsfraktion das Wort erhalten.
Hiernach erteilt die Präsidentin oder der Präsident den Vorsitzenden der anderen Fraktionen das Wort.
Hat eine Fraktion zwei Vorsitzende, hat nur eine oder einer von ihnen Rederecht.

#### § 27 Zur Geschäftsordnung

(1) Zur Geschäftsordnung erteilt die Präsidentin oder der Präsident vorrangig das Wort.

(2) Bemerkungen zur Geschäftsordnung dürfen sich nur auf die geschäftsordnungsgemäße Behandlung der Beratungsgegenstände beziehen und nicht länger als drei Minuten dauern.

#### § 28 Rededauer

(1) Die Zeitdauer für die Aussprache über einen Beratungsgegenstand kann auf Beschluss des Präsidiums oder auf Vorschlag der Präsidentin oder des Präsidenten durch den Landtag begrenzt werden.
Dabei sind die empfohlenen Redezeiten in der Anlage 1 zugrunde zu legen.
Fraktionslose Mitglieder des Landtages und Gruppen sind angemessen zu berücksichtigen, soweit diese den Wunsch, zu einem Beratungsgegenstand zu sprechen, der Präsidentin oder dem Präsidenten rechtzeitig anzeigen.
Überschreitet ein Mitglied der Landesregierung die empfohlene Redezeit, kann jede Fraktion die gleiche zusätzliche Redezeit beanspruchen.

(2) Spricht ein Mitglied des Landtages über die festgesetzte Redezeit hinaus, kann ihm die Präsidentin oder der Präsident nach einmaliger Mahnung das Wort entziehen.
Seine Ausführungen nach einer Wortentziehung werden in das Plenarprotokoll nicht aufgenommen.

(3) Wurde das Wort entzogen, darf es derselben Person zu demselben Beratungsgegenstand in derselben Sitzung nicht wieder erteilt werden.

(4) An der Sitzung des Landtages teilnehmende Mitglieder können ihre Redebeiträge im Umfang, der der ihnen zugeteilten Redezeit entspricht, bis zum Ende der Aussprache zu Protokoll geben, wenn sie der Präsidentin oder dem Präsidenten als Rednerin oder Redner gemeldet worden sind und kein anderes Mitglied ihrer Fraktion zu diesem Beratungsgegenstand das Wort ergreift oder bereits einen Redebeitrag abgegeben hat.
Der Beitrag ist im Protokoll entsprechend kenntlich zu machen.
Satz 1 gilt nicht für Aussprachen zu Regierungserklärungen und zu Aktuellen Stunden.
Die Präsidentin oder der Präsident kann die Aufnahme des eingereichten Redebeitrages ins Plenarprotokoll ablehnen, wenn gegen Bestimmungen der Geschäftsordnung verstoßen wird oder Ordnungsmaßnahmen ergriffen werden könnten oder der in Satz 1 vorgegebene Umfang überschritten wird.
§ 36 findet entsprechend Anwendung.

#### § 29 Fragen zu einem Redebeitrag, Kurzintervention

(1) Wenn Mitglieder des Landtages in der Aussprache über einen Beratungsgegenstand beabsichtigen, Fragen zu einem Redebeitrag zu stellen, melden sie sich hierfür während des Redebeitrages über die Saalmikrofone zu Wort.

(2) Auf Befragen durch die Präsidentin oder den Präsidenten kann sich die Rednerin oder der Redner mit der Beantwortung von Fragen einverstanden erklären oder dies ablehnen.
Die Fragen sind präzise und kurz zu formulieren.
Zulässig sind bis zu zwei Fragen des gleichen Mitgliedes des Landtages.

(3) Die Beantwortung der Fragen wird nicht auf die Rededauer des jeweiligen Redebeitrages angerechnet.
Nach Beendigung des Redebeitrages ist die Anmeldung von Fragen nicht mehr zulässig.

(4) Im Anschluss an einen Redebeitrag kann die Präsidentin oder der Präsident das Wort zu einer Kurzintervention von höchstens zwei Minuten erteilen, Kurzinterventionen zu einem Redebeitrag der eigenen Fraktion sind unzulässig.
Je Tagesordnungspunkt ist die Zahl der Kurzinterventionen pro Fraktion auf zwei, pro Gruppe und fraktionslosem Mitglied des Landtages auf jeweils eine Kurzintervention begrenzt.
Die Rednerin oder der Redner darf mit einem Beitrag von höchstens zwei Minuten reagieren.
Wortmeldungen sind der Präsidentin oder dem Präsidenten bis zum Ende des Redebeitrages durch das Aufheben einer Karte anzuzeigen.

(5) Falls mehrere Mitglieder des Landtages eine Kurzintervention anmelden, werden sie nacheinander aufgerufen.
Wird zusammengefasst erwidert, kann die Präsidentin oder der Präsident die Redezeit für die Erwiderung verlängern.

(6) Die Präsidentin oder der Präsident kann die Zulassung einer Kurzintervention oder von weiteren Kurzinterventionen ablehnen, wenn sie oder er den Beratungsgegenstand für erschöpft hält oder der weitere parlamentarische Ablauf eine Nichtzulassung nahelegt.

(7) Während der Fragestunde sowie einer Regierungserklärung und der dazugehörigen Debatte sind Fragen zu einem Redebeitrag sowie Kurzinterventionen nicht zulässig.
Anlage 2 Nummer 7 bleibt unberührt.

#### § 30 Zutrittsrecht und Anwesenheitspflicht der Mitglieder der Landesregierung

(1) Die Mitglieder der Landesregierung und ihre Beauftragten haben zu den Sitzungen des Landtages Zutritt.

(2) Jedes Mitglied des Landtages kann die Anwesenheit von Mitgliedern der Landesregierung zu einem bestimmten Beratungsgegenstand beantragen.

(3) Auf Verlangen mindestens eines Fünftels der anwesenden Mitglieder des Landtages ist ein jedes Mitglied der Landesregierung zur Anwesenheit verpflichtet.

#### § 31 Rederecht der Mitglieder der Landesregierung

(1) Die Mitglieder der Landesregierung haben jederzeit Rederecht.
Ihnen ist im Falle der Wortmeldung jeweils als Nächstem das Wort, auch außerhalb der Tagesordnung, zu erteilen.

(2) Ergreift nach Schluss der Aussprache ein Mitglied der Landesregierung zum Gegenstand der Aussprache das Wort, wird die Aussprache wiedereröffnet.

(3) Ergreift ein Mitglied der Landesregierung außerhalb der Tagesordnung das Wort, wird die Beratung über seine Erklärung eröffnet.
In diesem Falle kann jede Fraktion die gleiche Redezeit wie die Landesregierung verlangen.
Anträge zur Sache dürfen hierbei nicht gestellt werden.

#### § 32 Zutrittsrecht und Worterteilung an Dritte

(1) Zutrittsberechtigt zu den Sitzungen des Landtages sind die Präsidentin oder der Präsident des Landesverfassungsgerichtes, die Präsidentin oder der Präsident des Landesrechnungshofes, die Landesbeauftragten im Sinne des Artikels 74 der Verfassung des Landes Brandenburg und die oder der Vorsitzende oder in deren oder dessen Vertretung ein Mitglied des Rates für Angelegenheiten der Sorben/Wenden.

(2) Die Präsidentin oder der Präsident kann der Präsidentin oder dem Präsidenten des Landesrechnungshofes, den Landesbeauftragten im Sinne von Artikel 74 der Verfassung des Landes Brandenburg, in Abwesenheit eines zuständigen Mitgliedes der Landesregierung dessen Staatssekretärin oder Staatssekretär sowie einem Mitglied des Rates für Angelegenheiten der Sorben/Wenden das Wort erteilen.
Die Wortmeldung ist der Präsidentin oder dem Präsidenten vorher anzuzeigen.

(3) In der Fragestunde hat jedes Mitglied des Landtages das Recht zu verlangen, dass seine Anfrage von einem Mitglied der Landesregierung beantwortet wird.
Dieses Begehren ist der Präsidentin oder dem Präsidenten vorher schriftlich anzuzeigen.

### Abschnitt 7  
Ordnungsbestimmungen

#### § 33 Leitung der Sitzung

Die Präsidentin oder der Präsident leitet die Plenarsitzung im Sinne einer die Würde und Ordnung des Parlaments wahrenden Aussprache, während der die Rednerinnen und Redner sowie ihre Zuhörerinnen und Zuhörer einander mit Achtung begegnen.

#### § 34 Ordnungsmaßnahmen der Präsidentin oder des Präsidenten gegenüber Mitgliedern des Landtages

(1) Weichen Rednerinnen oder Redner vom Beratungsgegenstand ab, können sie von der Präsidentin oder dem Präsidenten zur Sache gerufen werden.

(2) Stellt die Präsidentin oder der Präsident Ordnungsverletzungen fest, die als Verhalten oder als Rede geeignet sind, die parlamentarische Würde oder Ordnung zu verletzen, dann ruft sie oder er das betreffende Mitglied des Landtages unter Nennung des Namens zur Ordnung.
Der Ordnungsruf und der Anlass hierzu dürfen in nachfolgenden Redebeiträgen nicht zum Gegenstand von Erörterungen gemacht werden.
Die Folgen einer Überschreitung der festgesetzten Redezeit richten sich nach § 28 Absatz 2 und 3.

(3) Ist das Mitglied des Landtages in derselben Rede dreimal zur Sache oder zur Ordnung gerufen und beim zweiten Mal auf die Folgen eines dritten Sach- oder Ordnungsrufs hingewiesen worden, kann die Präsidentin oder der Präsident, soweit das Mitglied des Landtages das Wort hat, das Wort entziehen, und ansonsten das Mitglied des Landtages von der Sitzung ausschließen.

(4) Ein ausgeschlossenes Mitglied des Landtages hat den Sitzungssaal sofort zu verlassen.
Ein solcher Ausschluss schließt das Verbot des Aufenthaltes im gesamten Sitzungssaal, einschließlich des Zuschauerraumes und der Pressetribüne, ein.
Wird die Aufforderung der Präsidentin oder des Präsidenten nicht befolgt, wird die Sitzung unterbrochen oder geschlossen.
Die Präsidentin oder der Präsident kann in diesem Falle das betreffende Mitglied des Landtages für bis zu drei weitere Sitzungstage ausschließen.

(5) Weigert sich ein ausgeschlossenes Mitglied des Landtages wiederholt, den Anordnungen der Präsidentin oder des Präsidenten zu folgen, kann die Präsidentin oder der Präsident den Ausschluss für bis zu zehn Sitzungstage in Folge festlegen.
Die Präsidentin oder der Präsident stellt dies bei Wiedereröffnung oder bei Beginn der nächsten Sitzung fest.

(6) Ausgeschlossene Mitglieder des Landtages dürfen auch an Sitzungen von Ausschüssen und sonstigen parlamentarischen Gremien nicht teilnehmen.

(7) Versucht ein ausgeschlossenes Mitglied des Landtages, widerrechtlich an den Sitzungen des Landtages oder seiner Ausschüsse teilzunehmen, finden die Absätze 4 und 5 entsprechende Anwendung.

#### § 35 Gröbliche Verletzung der parlamentarischen Ordnung oder Würde

(1) Wegen gröblicher Verletzung der parlamentarischen Würde oder Ordnung in der Rede oder im Verhalten kann die Präsidentin oder der Präsident, auch ohne dass zuvor ein Ordnungsruf ergangen ist, ein Mitglied des Landtages von der Sitzung ausschließen.

(2) Ein Sitzungsausschluss nach Absatz 1 kann auch nachträglich zu einem erteilten Ordnungsruf, spätestens in der auf die gröbliche Verletzung der parlamentarischen Würde oder Ordnung folgenden Sitzung, ausgesprochen werden, wenn die Präsidentin oder der Präsident während der Sitzung eine Verletzung der parlamentarischen Würde oder Ordnung ausdrücklich feststellt und sich einen nachträglichen Sitzungsausschluss vorbehalten hat.

#### § 36 Einspruch gegen Sachruf, Ordnungsruf oder Ausschließung

Gegen den Sachruf, den Ordnungsruf oder die Ausschließung von der Sitzung kann das betroffene Mitglied des Landtages bis zum Beginn der nächsten Sitzung schriftlich Einspruch bei der Präsidentin oder dem Präsidenten einlegen.
Über den Einspruch entscheidet das Präsidium.
Der Einspruch hat keine aufschiebende Wirkung.

#### § 37 Weitere Ordnungsgewalt der Präsidentin oder des Präsidenten

Alle in der Sitzung des Landtages Anwesenden unterstehen der Ordnungsgewalt der Präsidentin oder des Präsidenten.

#### § 38 Ordnungsmaßnahmen gegenüber der Zuhörerschaft

Wer im Zuhörerraum Beifall, Missbilligung oder sonstige politische Meinungsäußerung bekundet oder Ordnung und Anstand verletzt, kann auf Anordnung der Präsidentin oder des Präsidenten aus dem Sitzungssaal gewiesen werden.
Die Präsidentin oder der Präsident kann den Zuhörerraum wegen störender Unruhe räumen lassen.

#### § 39 Unterbrechung und Schließung der Sitzung

Entsteht im Landtag Unruhe, kann die Präsidentin oder der Präsident die Sitzung unterbrechen oder schließen.

### Abschnitt 8  
Gesetzgebungsverfahren, Behandlung der Beratungsgegenstände

#### § 40 Einbringung von Beratungsmaterialien

(1) Gesetzentwürfe, Anträge und Entschließungsanträge können von Mitgliedern des Landtages, Fraktionen, Gruppen, der Präsidentin oder dem Präsidenten, dem Präsidium oder Ausschüssen eingebracht werden; Artikel 75 und 95 Satz 3 der Verfassung des Landes Brandenburg bleiben unberührt.
Gesetzentwürfe und Anträge müssen mit einer den Inhalt kennzeichnenden Überschrift versehen sein und dürfen nicht an eine Bedingung geknüpft werden.
Zeichnungsberechtigt sind:

1.  für die Fraktion jede oder jeder Fraktionsvorsitzende, die Parlamentarische Geschäftsführerin oder der Parlamentarische Geschäftsführer sowie jede oder jeder stellvertretende Fraktionsvorsitzende,
2.  für die Gruppe alle Mitglieder gemeinsam, sofern nicht gemäß § 21 Absatz 3 Satz 2 des Fraktionsgesetzes eine Sprecherin oder ein Sprecher benannt wurde,
3.  für das Präsidium die amtierende Präsidentin oder der amtierende Präsident,
4.  für den Ausschuss die oder der amtierende Ausschussvorsitzende.

(2) Entschließungsanträge zu einem Beratungsgegenstand können bis zum Ende der Aussprache eingereicht werden.
Die Abstimmung bei Anträgen auf Entschließungen zu Gesetzentwürfen erfolgt nach deren Schlussabstimmung, in den übrigen Fällen nach der Abstimmung oder, falls eine Abstimmung nicht erfolgt, nach Schluss der Aussprache.
Entschließungsanträge können nicht an einen Ausschuss überwiesen werden.

(3) Gesetzentwürfe, Anfragen, Anträge, Entschließungsanträge, Beschlussempfehlungen und Berichte der Ausschüsse und sonstige Beratungsmaterialien sind bei der Parlamentarischen Geschäftsstelle gemäß Anlage 9 §§ 1 bis 3 einzubringen.
Die Beratungsmaterialien werden als Drucksachen an die Mitglieder des Landtages, die Fraktionen, die Gruppen, die Mitglieder der Landesregierung, die Präsidentin oder den Präsidenten des Landesrechnungshofes, die Landesbeauftragten im Sinne von Artikel 74 der Verfassung des Landes Brandenburg sowie den Rat für Angelegenheiten der Sorben/Wenden gemäß Anlage 9 § 4 verteilt und veröffentlicht.
Anträge zum geschäftsordnungsgemäßen Ablauf der Sitzungen sind von dieser Regelung ausgenommen.

#### § 41 Zurückweisung von Beratungsmaterialien

(1) Beratungsgegenstände der in § 40 bezeichneten Art soll die Präsidentin oder der Präsident zurückweisen, wenn

1.  sie gegen die parlamentarische Ordnung verstoßen,
2.  durch ihren Inhalt offenkundig der Tatbestand einer strafbaren Handlung erfüllt wird,
3.  deren Behandlung einen Eingriff in die richterliche Unabhängigkeit bedeuten könnte.

(2) Gegen die Entscheidung der Präsidentin oder des Präsidenten ist die schriftliche Beschwerde zulässig.
Über die Beschwerde entscheidet das Präsidium.

#### § 42 Beratungsbeginn und Beratungsverfahren

(1) Die Beratung von Gesetzentwürfen in erster Lesung soll frühestens am 13. Tag und von allen anderen Beratungsmaterialien (§ 40) frühestens am neunten Tag nach der Verteilung der Drucksachen beginnen.
Bei regulären Plenarsitzungswochen sollen Anträge an dem einer Plenarsitzungswoche vorangehenden Dienstag bis 13 Uhr eingebracht werden.
Die Beratung von Beschlussempfehlungen und Berichten der Ausschüsse sowie Anträgen mit Wahlvorschlag kann abweichend von Satz 1 am zweiten Tag nach ihrer Verteilung beginnen; § 46 Absatz 2 Satz 2 bleibt unberührt.
Für die Wahl des Präsidiums des Landtages gemäß § 11 Absatz 1 kann die Beratung eines Antrages mit Wahlvorschlag abweichend von Satz 2 unmittelbar nach seiner Verteilung beginnen.
Kommt in Angelegenheiten des Landtages eine Wahl nicht zustande, kann die Beratung eines Antrages mit einem weiteren Wahlvorschlag abweichend von Satz 1 unmittelbar nach seiner Verteilung beginnen.
Das Nähere regelt Anlage 9 dieser Geschäftsordnung.
Wird vor Eintritt in die Tagesordnung von mindestens einer Fraktion oder einem Fünftel der Mitglieder des Landtages Einspruch erhoben, weil die Frist nach Satz 1 und Satz 3 nicht eingehalten wurde, wird der Beratungsgegenstand zurückgestellt.

(2) Gesetzentwürfe werden grundsätzlich in zwei Lesungen beraten, alle sonstigen Beratungsmaterialien können in einer Lesung erledigt werden.

(3) Gesetzentwürfe zur Änderung oder Ergänzung des Wortlauts der Verfassung werden in drei Lesungen beraten, ebenso der Entwurf des Haushaltsgesetzes sowie Nachträge dazu.

(4) Die Abstimmung über Beschlussempfehlungen und Anträge, die Ausgaben mit sich bringen, die im Haushalt nicht gedeckt sind, ist erst zulässig, wenn ihre Beratung im Ausschuss für Haushalt und Finanzen abgeschlossen ist.

(5) Anträge mit Wahlvorschlag sollen spätestens am zweiten Tag vor ihrer Beratung bis 16 Uhr eingebracht werden.
Anträge mit Wahlvorschlag, die nach 16 Uhr eingebracht werden, werden nicht mehr am selben Tag verteilt.
Eine spätere Änderung des Wahlvorschlages setzt die Frist in Absatz 1 Satz 3 erneut in Gang.

#### § 43 Dringlichkeitsanträge

(1) Dringlichkeitsanträge sind:

1.  ein Antrag auf Beschlussfassung über ein konstruktives Misstrauensvotum,
2.  ein Antrag der Ministerpräsidentin oder des Ministerpräsidenten, ihr oder ihm das Vertrauen auszusprechen,
3.  ein Antrag auf Abwahl eines Mitgliedes des Präsidiums,
4.  Anträge auf Einsetzung von Untersuchungsausschüssen und Abwahl der oder des Vorsitzenden eines Untersuchungsausschusses,
5.  Anträge auf Herstellung der Immunität.

(2) Dringlichkeitsanträge sind bevorzugt auf die nächste Tagesordnung zu setzen.
Die in Verfassung, Gesetz oder in dieser Geschäftsordnung geregelten Fristen für Anträge gemäß Absatz 1 Nummer 1, 3 und 4 bleiben unberührt.

#### § 44 Erste Lesung

(1) Gesetzentwürfe werden in der ersten Lesung begründet und in ihren Grundsätzen beraten.

(2) Am Schluss der ersten Lesung kann die Überweisung eines Gesetzentwurfes an einen oder mehrere Ausschüsse beschlossen werden.

(3) Ein Gesetzentwurf hat sich erledigt, wenn sowohl die Überweisung an einen Ausschuss als auch der Gesetzentwurf selbst abgelehnt werden.

#### § 45 Zweite Lesung

(1) In der zweiten Lesung wird der Gesetzentwurf im Einzelnen beraten.
Sind in den Ausschussberatungen vor der zweiten Lesung Änderungsempfehlungen beschlossen worden, sollen diese dem Gesetzentwurf gegenübergestellt werden.

(2) Zwischen der ersten und der zweiten Lesung muss mindestens ein Tag liegen, an dem keine Lesung des Gesetzentwurfes stattfindet.

(3) Auf Beschluss des Landtages kann ein Gesetzentwurf vor der Schlussabstimmung an einen Ausschuss überwiesen werden.
Die Überweisung eines Gesetzentwurfes kann auch an einen anderen Ausschuss als den, dem er zuerst vorlag, erfolgen.

(4) Nach Schluss der Aussprache wird über Annahme oder Ablehnung des Gesetzentwurfes, bei Vorliegen von Änderungsanträgen zunächst über diese abgestimmt.
Die Präsidentin oder der Präsident kann die Schlussabstimmung bis zur Zusammenstellung und Verteilung der in zweiter Lesung gefassten Beschlüsse aussetzen.

(5) Werden nach der Schlussabstimmung über einen Gesetzentwurf Druckfehler oder andere offensichtliche Unrichtigkeiten festgestellt, so nimmt die Präsidentin oder der Präsident vor der Verkündung im Gesetz- und Verordnungsblatt eine Berichtigung des Wortlautes vor.
Erfolgt die Feststellung erst nach der Verkündung, so erhält die Landesregierung Gelegenheit zur Stellungnahme.

#### § 46 Dritte Lesung

(1) Eine dritte Lesung findet in den Fällen des § 42 Absatz 3 sowie auf Antrag einer Fraktion oder eines Fünftels der Mitglieder des Landtages statt.
Dieser Antrag muss vor Schluss der Aussprache in der zweiten Lesung schriftlich bei der Präsidentin oder dem Präsidenten eingereicht werden.

(2) Zur Vorbereitung der dritten Lesung kann der Landtag die Überweisung des Gesetzentwurfes an einen oder mehrere Ausschüsse beschließen.
Die dritte Lesung kann auch unmittelbar nach Schluss der zweiten Lesung oder nach Verteilung des Ausschussberichtes erfolgen.
Auf Verlangen der Antragstellerinnen und Antragsteller ist der Gesetzentwurf an mindestens einen Ausschuss zu überweisen.
Absatz 1 Satz 2 findet Anwendung.

(3) Am Schluss der dritten Lesung wird über die Annahme oder Ablehnung des Gesetzentwurfes abgestimmt.
Bei Vorliegen von Änderungsanträgen findet § 45 Absatz 4 entsprechende Anwendung.

#### § 47 Weitere Lesung

(1) Eine weitere Lesung ist erforderlich, wenn die Landesregierung oder die Präsidentin oder der Präsident des Landtages dies beantragen.

(2) Der Landtag kann eine zusätzliche Ausschussberatung beschließen.
Die Überweisung an den zuständigen Ausschuss kann auch ohne Beschluss des Landtages durch die Präsidentin oder den Präsidenten erfolgen.

#### § 48 Änderungsanträge

(1) Änderungsanträge können von jedem Mitglied des Landtages, der Präsidentin oder dem Präsidenten, dem Präsidium des Landtages, einer Fraktion, einer Gruppe oder einem Ausschuss eingebracht werden, solange die Aussprache zu dem Beratungsgegenstand, auf den sie sich beziehen, noch nicht geschlossen ist.
Änderungsanträge werden gemäß Anlage 9 §§ 1 bis 4 eingebracht, verteilt und veröffentlicht.
Änderungsanträge zu Änderungsanträgen sind unzulässig.

(2) Wird ein Beratungsgegenstand an einen oder mehrere Ausschüsse überwiesen, gelten zuvor gestellte Änderungsanträge als mitüberwiesen.
§ 45 Absatz 4 bleibt unberührt.

#### § 49 Rücknahme von Beratungsmaterialien

Wer Beratungsmaterialien einbringt, kann diese bis zur Schlussabstimmung zurückziehen.

#### § 50 Zustimmungsgesetze zu Staatsverträgen

Bei der Beratung von Gesetzentwürfen, mit denen die Zustimmung des Landtages zu einem Staatsvertrag erteilt werden soll, sind Beschlussempfehlungen von Ausschüssen und Änderungsanträge nur zum Entwurf des Zustimmungsgesetzes zulässig.

#### § 51 Berichte über akustische Wohnraumüberwachung

Berichte über Maßnahmen der akustischen Wohnraumüberwachung, die zum Zwecke der Strafverfolgung durchgeführt werden, nimmt die Parlamentarische Kontrollkommission entgegen.
Auf das Verfahren finden die Bestimmungen des Fünften Abschnittes des Brandenburgischen Verfassungsschutzgesetzes Anwendung.

#### § 52 Vorlagen nach der Landeshaushaltsordnung, dem Haushaltsgesetz und sonstige Vorlagen

(1) Vorlagen im Rahmen des jährlichen Entlastungsverfahrens (Haushaltsrechnung und Jahresbericht des Landesrechnungshofes), Sonderberichte des Landesrechnungshofes über Angelegenheiten von besonderer Bedeutung sowie Beratungsberichte des Landesrechnungshofes werden von der Präsidentin oder dem Präsidenten allen Mitgliedern des Landtages und gleichzeitig zur Beratung und Berichterstattung dem Ausschuss für Haushaltskontrolle zugeleitet.
Richtet sich ein Bericht an einen anderen Ausschuss als den Ausschuss für Haushaltskontrolle, leitet die Präsidentin oder der Präsident den Bericht an diesen Ausschuss zur federführenden Beratung weiter.
Bei Bedarf kann der Ausschuss, dem die Vorlage zugeleitet wurde, die Stellungnahmen weiterer Ausschüsse einholen.

(2) Vorlagen im Rahmen des haushaltsrechtlichen Einwilligungsverfahrens überweist die Präsidentin oder der Präsident zur Beratung und Entscheidung an den Ausschuss für Haushalt und Finanzen, der bei Bedarf Stellungnahmen weiterer Ausschüsse einholen kann.

(3) Die Entwürfe der Anmeldungen für die gemeinsame Rahmenplanung nach Artikel 91a und Artikel 91b des Grundgesetzes werden von der Präsidentin oder dem Präsidenten zur federführenden Beratung und Berichterstattung an den Ausschuss für Haushalt und Finanzen und zur Mitberatung an die zuständigen Ausschüsse überwiesen.

(4) Soweit der Landtag ganz oder teilweise über die Besetzung nichtparlamentarischer Gremien entscheidet, kann die Präsidentin oder der Präsident dem zuständigen Ausschuss entsprechende Vorlagen direkt zuleiten und ihn beauftragen, dem Landtag einen Wahlvorschlag vorzulegen.
Das Recht der Mitglieder des Landtages, eigene Anträge mit Wahlvorschlag zu stellen, bleibt davon unberührt.

(5) Sonstige Vorlagen, die dem Landtag und seinen Ausschüssen zur Unterrichtung vorgelegt werden, sind von der Präsidentin oder dem Präsidenten bestimmungsgemäß zu verteilen.

#### § 53 Berichte der Landesbeauftragten im Sinne des Artikels&nbsp;74 der Verfassung des Landes Brandenburg

(1) Die Berichte der oder des Landesbeauftragten für den Datenschutz und für das Recht auf Akteneinsicht und die Stellungnahmen der Landesregierung dazu werden von der Präsidentin oder dem Präsidenten allen Mitgliedern des Landtages zugeleitet und gleichzeitig zur Beratung und Berichterstattung an den für Inneres zuständigen Ausschuss überwiesen, der bei Bedarf Stellungnahmen weiterer Ausschüsse einholen kann.

(2) Berichte der oder des Landesbeauftragten für die Aufarbeitung der Folgen der kommunistischen Diktatur nach § 4 des Brandenburgischen Aufarbeitungsbeauftragtengesetzes werden von der Präsidentin oder dem Präsidenten allen Mitgliedern des Landtages zugeleitet und gleichzeitig zur Beratung und Berichterstattung an den Hauptausschuss überwiesen, der bei Bedarf Stellungnahmen weiterer Ausschüsse einholen kann.

#### § 54 Immunitätsangelegenheiten und Genehmigungen zur Zeugenvernehmung nach §&nbsp;50 Absatz&nbsp;3 StPO und §&nbsp;382 Absatz&nbsp;3 ZPO

(1) Das Verfahren in Immunitätsangelegenheiten richtet sich nach der Immunitätsrichtlinie in Anlage 6 dieser Geschäftsordnung.

(2) Anträge zur Erteilung einer Genehmigung gemäß § 50 Absatz 3 StPO und § 382 Absatz 3 ZPO leitet die Präsidentin oder der Präsident an den Hauptausschuss weiter.
Der Hauptausschuss gibt dem betroffenen Mitglied des Landtages Gelegenheit zur Stellungnahme.
Stimmt der Hauptausschuss der Abweichung von § 50 Absatz 1 StPO und § 382 Absatz 2 ZPO zu, teilt die Präsidentin oder der Präsident diese Entscheidung den Mitgliedern des Landtages unverzüglich schriftlich mit.
Sie gilt als Entscheidung des Landtages, wenn nicht innerhalb von sieben Tagen nach der Mitteilung von einem oder mehreren Mitgliedern des Landtages schriftlich bei der Präsidentin oder dem Präsidenten Widerspruch erhoben wird.
Im Falle des Widerspruchs setzt die Präsidentin oder der Präsident die Entscheidung des Hauptausschusses als dessen Beschlussempfehlung auf die Tagesordnung der nächsten Sitzung des Landtages.
Einer Genehmigung bedarf es nicht, wenn der Termin außerhalb der Sitzungswochen des Landtages liegt.

#### § 55 Angelegenheiten der Verfassungsgerichtsbarkeit

(1) Gibt das Bundesverfassungsgericht oder das Verfassungsgericht des Landes Brandenburg dem Landtag in verfassungsgerichtlichen Verfahren Gelegenheit zur Äußerung oder zum Verfahrensbeitritt, überweist die Präsidentin oder der Präsident derartige Vorlagen unmittelbar an den Hauptausschuss.
Soweit sich der Hauptausschuss noch nicht konstituiert hat, wird die Vorlage an das Präsidium überwiesen, das anstelle des Hauptausschusses entscheidet.

(2) Der Hauptausschuss entscheidet, ob er einen Verfahrensbeitritt oder eine Äußerung des Landtages für geboten hält.
Er kann darüber hinaus beschließen, bis auf Weiteres von einer gesonderten Beschlussfassung in weiteren gleichgelagerten, verbundenen oder im Sachzusammenhang stehenden Verfahren abzusehen.
Hält der Hauptausschuss einen Verfahrensbeitritt oder eine Äußerung für geboten, enthält der Beschluss auch die Stellungnahme.
Beschlüsse nach Satz 1 bis 3 werden der Präsidentin oder dem Präsidenten übermittelt, welche oder welcher die Mitglieder des Landtages darüber informiert.
Legt ein Mitglied des Landtages binnen sieben Tagen nach der Unterrichtung schriftlich Widerspruch bei der Präsidentin oder dem Präsidenten ein, wird die Entscheidung des Hauptausschusses als Beschlussempfehlung auf die Tagesordnung der nächsten Sitzung des Landtages gesetzt.
Nach Abschluss des Verfahrens informiert die Präsidentin oder der Präsident das erkennende Gericht über die Entscheidung des Landtages.

(3) Tritt im Falle des Absatzes 2 Satz 5 der Landtag nach seinem Terminplan bis zu dem vom Verfassungsgericht gesetzten Termin nicht mehr zusammen und kommt eine Fristverlängerung nicht in Betracht, entscheidet der Hauptausschuss abschließend.
Diese Entscheidung wird der Präsidentin oder dem Präsidenten übermittelt, welche oder welcher die Mitglieder des Landtages darüber informiert.

#### § 55a Verfahren nach dem Infektionsschutzbeteiligungsgesetz

(1) Neben der Pflicht zur Unterrichtung des Landtages informiert die Landesregierung den für Gesundheit zuständigen Ausschuss unverzüglich und vor dem Erlass von Rechtsverordnungen zur Verhinderung der Verbreitung des Coronavirus SARS-CoV-2 gemäß § 2 des Infektionsschutzbeteiligungsgesetzes sowie deren Verlängerung, Änderung oder Aufhebung.

(2) Nach Verkündung einer Rechtsverordnung zur Verhinderung der Verbreitung des Coronavirus SARSCoV-2 gemäß § 2 des Infektionsschutzbeteiligungsgesetzes tritt unverzüglich der für Gesundheit zuständige Ausschuss zusammen.
Widerspricht die Mehrheit der Mitglieder des Ausschusses der Rechtsverordnung, tritt unverzüglich der Landtag zusammen und beschließt über die Beschlussempfehlung des Ausschusses.

(3) Abweichend von Absatz 2 kann der Landtag gemäß § 17 Absatz 2 zusammentreten und über Anträge auf Widerspruch gegen Rechtsverordnungen zur Verhinderung der Verbreitung des Coronavirus SARS-CoV-2 gemäß § 2 des Infektionsschutzbeteiligungsgesetzes beschließen.

(4) Für Beschlussempfehlung und Anträge gemäß Absatz 2 oder 3 finden die Fristen gemäß § 42 keine Anwendung.

(5) Hat der Landtag gemäß Absatz 2 oder 3 über den Widerspruch gegen Rechtsverordnungen zur Verhinderung der Verbreitung des Coronavirus SARS-CoV-2 gemäß § 2 des Infektionsschutzbeteiligungsgesetzes entschieden, ist eine neuerliche Sondersitzung und Beschlussfassung des Landtages in derselben Sache unzulässig.

(6) § 94 bleibt unberührt.

(7) § 55a tritt mit Ablauf des 31.
Dezember 2022 außer Kraft.

### Abschnitt 9  
Große und Kleine Anfragen, Fragestunde und Aktuelle Stunde

#### § 56 Einbringung von Großen Anfragen

Große Anfragen an die Landesregierung werden gemäß Anlage 9 §§ 1 bis 4 eingebracht, verteilt und veröffentlicht.
Große Anfragen müssen von einer Fraktion oder mindestens einem Fünftel der Mitglieder des Landtages eingebracht werden.

#### § 57 Behandlung von Großen Anfragen

(1) Große Anfragen sind innerhalb von drei Monaten zu beantworten.
Eine Fristverlängerung ist mit Einverständnis der Einbringerin oder des Einbringers zulässig.
Die Antworten der Landesregierung werden gemäß Anlage 9 §§ 1 bis 4 eingebracht, verteilt und veröffentlicht.
Große Anfragen sind unverzüglich im Landtag zu behandeln, es sei denn, die Einbringerin oder der Einbringer widerspricht.

(2) Lehnt die Landesregierung die Beantwortung der Großen Anfrage ab, ist sie zur Beratung auf die Tagesordnung der nächsten Sitzung des Landtages zu setzen, wenn mindestens eine Fraktion oder ein Fünftel der Mitglieder des Landtages es verlangt.

#### § 58 Kleine Anfragen

(1) Jedes Mitglied des Landtages kann von der Landesregierung durch Kleine Anfragen Auskünfte verlangen.
Kleine Anfragen werden gemäß Anlage 9 §§ 1 bis 4 eingebracht, verteilt und veröffentlicht.

(2) Die Kleine Anfrage darf sich nur auf einen bestimmten Sachverhalt beziehen.
Die zur Erlangung der gewünschten Auskunft angegebenen Tatsachen und gestellten Fragen müssen in kurzer, gedrängter Form formuliert sein.

(3) Die Präsidentin oder der Präsident übermittelt die Anfragen der Landesregierung zur Beantwortung innerhalb von vier Wochen.

(4) Die Antworten werden zusammen mit den Anfragen gemäß Anlage 9 §§ 1 bis 4 eingebracht, verteilt und veröffentlicht.
Eine Beratung findet nicht statt.

#### § 59 Ablehnung der schriftlichen Beantwortung

(1) Antwortet die Landesregierung nicht innerhalb der vorgesehenen Frist, wird die Kleine Anfrage auf die Tagesordnung der nächsten Sitzung gesetzt und die Landesregierung um mündliche Beantwortung ersucht, es sei denn, das Einverständnis der Fragestellerin oder des Fragestellers zu einer Fristverlängerung liegt der Präsidentin oder dem Präsidenten vor.
Lehnt die Landesregierung auch die mündliche Beantwortung ab, so hat sie dies nach Aufruf des entsprechenden Tagesordnungspunktes durch die Präsidentin oder den Präsidenten vor dem Landtag zu begründen.

(2) Gibt die Landesregierung eine mündliche Antwort, kann das Mitglied des Landtages, welches die Frage gestellt hat, das Wort zur Berichtigung oder Ergänzung verlangen; eine allgemeine Aussprache über die Antwort und Anträge zur Sache sind unzulässig.

#### § 60 Fragestunde und Aktuelle Stunde

(1) Jedes Mitglied des Landtages ist berechtigt, kurze mündliche Anfragen an die Landesregierung zu richten.
Die Einzelheiten des Verfahrens der Fragestunde werden durch die dieser Geschäftsordnung als Anlage 2 beigefügte Richtlinie geregelt.

(2) Eine Fraktion oder eine Gruppe kann zu einer bestimmt bezeichneten aktuellen Frage der Landespolitik eine Aussprache beantragen.
Die Einzelheiten des Verfahrens der Aktuellen Stunde werden durch Anlage 3 dieser Geschäftsordnung geregelt.

### Abschnitt 10  
Beschlussfähigkeit und Abstimmungen

#### § 61 Beschlussfähigkeit des Landtages

Der Landtag ist beschlussfähig, wenn mehr als die Hälfte der Mitglieder anwesend ist.

#### § 62 Anzweiflung der Beschlussfähigkeit

(1) Die Beschlussfähigkeit kann nur unmittelbar nach Beendigung der Aussprache bis zur Eröffnung der Abstimmung angezweifelt werden.
In diesem Falle ist bis zur Feststellung der Beschlussfähigkeit eine Geschäftsordnungsdebatte unzulässig.

(2) Wird die Beschlussfähigkeit angezweifelt, ist diese durch Namensaufruf oder Zählung der anwesenden Mitglieder des Landtages festzustellen.

(3) Bei festgestellter Beschlussunfähigkeit kann die Präsidentin oder der Präsident die Sitzung für kurze Zeit unterbrechen.

#### § 63 Schließung der Sitzung bei Beschlussunfähigkeit

Kann die Beschlussfähigkeit in angemessener Zeit nicht wiederhergestellt werden, hat die Präsidentin oder der Präsident die Sitzung zu schließen sowie Zeit und Tagesordnung der nächsten Sitzung zu verkünden.
Die Abstimmung wird in der nächsten Sitzung durchgeführt.
Ein Antrag auf namentliche Abstimmung bleibt dabei in Kraft.

#### § 64 Abstimmung

(1) Nach Schluss der Aussprache teilt die Präsidentin oder der Präsident den Abstimmungsgegenstand zusätzlich unter Angabe der Drucksachennummer mit.
Sie oder er kann den Abstimmungsvorgang gliedern.
Bei Widerspruch gegen den von der Präsidentin oder dem Präsidenten vorgeschlagenen Wortlaut des Abstimmungsgegenstandes entscheidet der Landtag.
Anschließend eröffnet die Präsidentin oder der Präsident die Abstimmung.

(2) Jedes Mitglied des Landtages kann die Teilung des Abstimmungsgegenstandes beantragen.
Werden gegen die Teilung Bedenken erhoben, entscheidet der Landtag.
Unmittelbar vor der Abstimmung über diesen Antrag ist der Abstimmungsgegenstand vorzulesen.

(3) Von der Eröffnung der Abstimmung bis zur Bekanntgabe des Ergebnisses werden Anträge nicht mehr zugelassen und das Wort nicht mehr erteilt.

(4) Die Präsidentin oder der Präsident stellt durch Befragen des Landtages fest, wer für einen Abstimmungsgegenstand ist, wer dagegen ist und wer sich der Stimme enthält.

#### § 65 Reihenfolge der Abstimmung

(1) Über Anträge wird in folgender Reihenfolge abgestimmt:

1.  Anträge auf Unterbrechung der Beratung,
2.  Anträge, die ohne die Sache selbst zu berühren, lediglich Vorfragen betreffen, insbesondere Überweisung an einen Ausschuss, Einholung einer Auskunft und dergleichen,
3.  Abstimmungen in der Sache selbst,
4.  Entschließungsanträge.

(2) Im Übrigen wird über die Anträge in der Reihenfolge ihres zeitlichen Eingangs abgestimmt.
Über Änderungsanträge zu Gesetzentwürfen kann entsprechend der Gesetzessystematik abgestimmt werden.

(3) Bei Geldsummen ist über die kleinere Einnahmesumme und die größere Ausgabesumme, bei Zeitbestimmungen über die längere Zeit zuerst zu entscheiden.

(4) Verpflichtungsermächtigungen werden wie Ausgabesummen behandelt.
Sind einzelne Anträge zu einer Haushaltsstelle in der Gesamtsumme von Anschlag und Verpflichtungsermächtigung gleich, wird über den Antrag zuerst abgestimmt, bei dem der Anschlag höher ist.

(5) Liegen zur gleichen Haushaltsstelle Anträge vor, von denen einer eine Erhöhung und einer eine Kürzung des Anschlags bezwecken, wird zuerst über die höhere Haushaltsbelastung abgestimmt.

(6) Änderungsanträge sind vor dem Hauptantrag zur Abstimmung zu bringen.

#### § 66 Abstimmungsregeln

(1) Soweit keine anderen gesetzlichen Vorschriften entgegenstehen, wird durch Handheben oder namentlich abgestimmt.
Die Mehrheit der abgegebenen Stimmen entscheidet, soweit durch die Verfassung des Landes Brandenburg oder Gesetze nichts anderes bestimmt ist.
Abweichende Mehrheiten sind nur in der Schlussabstimmung erforderlich.
Stimmenthaltungen und ungültige Stimmen zählen nicht bei der Berechnung der Mehrheit mit, soweit nicht gesetzliche Vorschriften anderes vorsehen.
Ist für eine Wahl die Zahl der anwesenden Mitglieder des Landtages maßgeblich, gilt für die Feststellung des Ergebnisses als anwesend, wer seine Stimme abgegeben hat.

(2) Ist das Sitzungspräsidium über das Ergebnis der Abstimmung nicht einig, werden die Stimmen gezählt.

#### § 67 Namentliche Abstimmung

(1) Namentliche Abstimmung kann bis zur Eröffnung der Abstimmung schriftlich beantragt werden.
Sie findet statt, wenn eine Fraktion oder ein Fünftel der Mitglieder des Landtages es beantragt.

(2) Die namentliche Abstimmung erfolgt durch Aufruf der Namen der Mitglieder des Landtages.
Die Abstimmenden haben bei Namensaufruf mit „Ja“, „Nein“ oder „Enthaltung“ zu antworten.
Entstehen Zweifel darüber, ob und wie ein Mitglied des Landtages abgestimmt oder ob es sich der Stimme enthalten hat, befragt die Präsidentin oder der Präsident hierüber das Mitglied des Landtages.
Erklärt sich ein Mitglied des Landtages nicht, gilt dies als Nichtbeteiligung an der Abstimmung.

(3) Nach Beendigung des Namensaufrufes fragt die Präsidentin oder der Präsident nach, ob ein anwesendes stimmberechtigtes Mitglied des Landtages seine Stimme noch nicht abgegeben hat.
Ist dies der Fall, wird das betreffende Mitglied unter Aufruf seines Namens nach seiner Stimmabgabe befragt.
Alsdann erklärt die Präsidentin oder der Präsident die Abstimmung für geschlossen.

#### § 68 Unzulässigkeit der namentlichen Abstimmung

Eine namentliche Abstimmung ist unzulässig bei Beschlussfassung über

1.  Überweisung an einen Ausschuss,
2.  Abkürzung der Fristen,
3.  Tagesordnung,
4.  Unterbrechung der Sitzung,
5.  Unterbrechung oder Schluss der Beratung,
6.  Teilung des Abstimmungsgegenstandes.

#### § 69 Feststellung des Abstimmungsergebnisses

(1) Das Ergebnis jeder Abstimmung stellt das Sitzungspräsidium fest.
Die Präsidentin oder der Präsident verkündet es.
Hierbei erklärt sie oder er, ob die Abstimmungsfrage bejaht oder verneint ist.
Sie teilt mit, ob Gegenstimmen abgegeben wurden oder ob Stimmenthaltungen zu verzeichnen sind.

(2) Bei Abstimmungen des Landtages, die einer qualifizierten Stimmenmehrheit bedürfen, hat die Präsidentin oder der Präsident durch ausdrückliche Erklärung festzustellen, ob diese Mehrheit zugestimmt hat.

#### § 70 Persönliche Bemerkungen und Erklärungen zur Abstimmung

(1) Zu persönlichen Bemerkungen wird das Wort erst nach Schluss der Aussprache, jedoch im Falle einer Abstimmung vor Beginn der Abstimmung erteilt.
Das Mitglied des Landtages darf nicht zur Sache sprechen, sondern nur Äußerungen, die in der Aussprache gegen dieses Mitglied gerichtet wurden, zurückweisen oder deutlich gewordene Missverständnisse seiner früheren Ausführungen richtigstellen.
Die Redezeit ist auf drei Minuten beschränkt.

(2) Nach Schluss der abschließenden Abstimmung über einen Beratungsgegenstand kann jedes Mitglied des Landtages eine mündliche Erklärung zu seinem Stimmverhalten, die nicht länger als drei Minuten dauern darf, abgeben.
Ausgenommen sind Wahlen, die nach Verfassung oder Gesetz ohne Aussprache vorzunehmen sind, sowie Abstimmungen zu Geschäftsordnungsanträgen.

(3) Werden die in den Absätzen 1 und 2 genannten Bestimmungen von einem Mitglied des Landtages nicht eingehalten, kann die Präsidentin oder der Präsident ihm das Wort entziehen.

#### § 71 Abstimmungen über Anträge mit Wahl- oder Abwahlvorschlag

(1) Abstimmungen über Anträge mit Wahl- oder Abwahlvorschlag erfolgen offen, es sei denn, ein Mitglied des Landtages widerspricht.
In den Fällen, in denen mehrere konkurrierende Bewerbungen vorhanden sind, ist ebenfalls geheim abzustimmen.

(2) Die Einzelheiten des Verfahrens bei der Durchführung der geheimen Abstimmung über einen Antrag mit Wahl- oder Abwahlvorschlag werden durch die dieser Geschäftsordnung als Anlage 7 beigefügte Wahlordnung geregelt.

#### § 72 Mitgliedschaft in Gremien

Die Mitgliedschaft von Mitgliedern des Landtages in Gremien, in die sie durch Beschluss oder Wahlen im Landtag entsandt wurden, endet, soweit gesetzliche Bestimmungen nicht entgegenstehen, mit der Wahl eines Ersatzmitgliedes, spätestens jedoch 60 Tage nach dem Ende der Wahlperiode.
Satz 1 gilt entsprechend für andere Personen, die durch den Landtag in Gremien entsandt wurden.

### Abschnitt 11  
Die Ausschüsse und Kommissionen

#### § 73 Bestellung der Ausschüsse

(1) Der Landtag bestellt aus seiner Mitte einen Hauptausschuss und weitere Ausschüsse für die Dauer der Wahlperiode.

(2) Für bestimmte Aufgaben kann der Landtag Sonderausschüsse einsetzen.

(3) Die Ausschüsse können mit Zustimmung des Präsidiums aus ihrer Mitte zur Vorbereitung ihrer Beschlüsse Unterausschüsse einsetzen.
Satz 1 gilt entsprechend für die Einsetzung von Berichterstattungsgruppen durch Enquete-Kommissionen.

#### § 74 Besetzung der Ausschüsse

(1) Die Zahl der Mitglieder eines Ausschusses wird auf Vorschlag des Präsidiums vom Landtag beschlossen.

(2) Die Ausschussmitglieder und stellvertretenden Ausschussmitglieder werden von den Fraktionen und Gruppen bestimmt.
Die Fraktionen und Gruppen haben der Präsidentin oder dem Präsidenten jede Änderung in der Besetzung mitzuteilen.

(3) Das Präsidium führt eine Einigung über die Ausschussvorsitze und deren Stellvertretung herbei.
Kommt keine Einigung zustande, erfolgt die Verteilung durch Zugriff nach dem Verfahren d’Hondt.[\*\*)](#bb) Der Hauptausschuss, die weiteren Fachausschüsse, der Wahlprüfungsausschuss, die Sonderausschüsse und der Petitionsausschuss bilden eine Folge.
Die Ausschüsse wählen ihre Vorsitzenden und deren stellvertretende Vorsitzende aus den vom Präsidium vorgeschlagenen Fraktionen.

(4) Bei der Einsetzung von Unterausschüssen führt der betreffende Fachausschuss eine Einigung über den Ausschussvorsitz und dessen Stellvertretung herbei.
Die oder der Vorsitzende und die oder der stellvertretende Vorsitzende müssen unterschiedlichen Fraktionen angehören.
Kommt keine Einigung zustande, erfolgt die Vergabe des Vorsitzes und der Stellvertretung unter den Fraktionen in der Reihenfolge ihrer Stärke.

(5) Sind die oder der Vorsitzende sowie die oder der stellvertretende Vorsitzende verhindert, geht der Vorsitz in der Reihenfolge der Stärke der Fraktionen nacheinander auf die anderen ordentlichen Mitglieder des Ausschusses über; ausgenommen hiervon sind zunächst die Fraktionen, deren Mitglieder den Vorsitz und dessen Stellvertretung innehaben.

(6) Die Gruppen benennen ihre Ausschussmitglieder durch Erklärung gemäß § 21 Absatz 3 Satz 1 oder 2 des Fraktionsgesetzes gegenüber dem Präsidium.
Das Präsidium weist die Mitglieder den von der Gruppe benannten Ausschüssen zu.
Ist eine Zuweisung unter Wahrung der Mehrheitsverhältnisse im Ausschuss nicht möglich, wird die Gruppe angehört, bevor das Präsidium eine andere Zuweisung vornimmt.

(7) Ausschussvorsitzende können mit der Mehrheit von zwei Dritteln der Mitglieder des Ausschusses von dieser Funktion abberufen werden.
Sie gelten als abberufen, wenn sie von ihrer Fraktion aus dem Ausschuss zurückgezogen werden.
Der Ausschussvorsitz ist nach der gemäß Absatz 3 herbeigeführten Einigung beziehungsweise nach der Stärke der Fraktion neu zu besetzen.
Die Sätze 1 bis 3 gelten für Unterausschüsse entsprechend.

(8) Ein fraktionsloses Mitglied des Landtages hat das Recht, in einem Ausschuss mit Stimmrecht mitzuarbeiten.
Das Präsidium weist dem fraktionslosen Mitglied des Landtages unter Wahrung der Mehrheitsverhältnisse einen Ausschuss zu.
Das fraktionslose Mitglied des Landtages ist vor der Entscheidung anzuhören.

#### § 75 Aufgaben der Ausschüsse

(1) Die Ausschüsse werden im Rahmen der ihnen vom Landtag erteilten Aufträge tätig.
Innerhalb ihres Aufgabenbereiches können sie sich auch aus eigener Initiative mit einer Sache befassen und dem Landtag Empfehlungen unterbreiten.
Empfehlungen im Sinne von Satz 2 sind Gesetzentwürfe, Anträge und Entschließungsanträge.

(2) Der Hauptausschuss ist federführend zuständig für Verfassungsfragen, Bundesangelegenheiten, die Gestaltung der Beziehungen zwischen Brandenburg und Berlin sowie die Medienpolitik; er behandelt darüber hinaus andere politisch grundsätzliche Angelegenheiten, ihm durch Gesetz übertragene Aufgaben sowie Geschäftsordnungsangelegenheiten grundsätzlicher Art.

(3) Die Ausschüsse sind zu baldiger Erledigung der ihnen überwiesenen Beratungsgegenstände verpflichtet.
Auf Antrag einer Fraktion oder eines Fünftels der Mitglieder des Landtages muss bei Gesetzentwürfen und Anträgen spätestens sechs Monate nach der Überweisung Bericht erstattet oder ein schriftlicher Zwischenbericht gegeben werden.
Kann ein Auftrag von einem Ausschuss nicht erledigt werden, gibt er ihn an den Landtag zurück.

(4) Für das Verfahren der Ausschüsse gelten die Bestimmungen zum Verfahren für die Plenarsitzungen sinngemäß, soweit nichts anderes bestimmt ist.

(5) Den Vorsitzenden obliegen die Vorbereitung und Leitung der Beratungen der Ausschüsse sowie die Durchführung der Beschlüsse.

#### § 76 Überweisung an mehrere Ausschüsse

(1) Wird ein Beratungsgegenstand ganz oder teilweise zugleich an mehrere Ausschüsse überwiesen, ist ein Ausschuss als federführend zu bestimmen.
Der federführende Ausschuss kann den mitberatenden Ausschüssen Fristen für die Abgabe ihrer Stellungnahmen setzen.
Die mitberatenden Ausschüsse teilen das Ergebnis ihrer Beratungen dem federführenden Ausschuss mit.
Dieser kann auch gemeinsame Beratungen anberaumen.
Die Abstimmung erfolgt getrennt.
Ein Ausschuss kann auch die Stellungnahme eines Ausschusses des Landtages einholen, an den ein Beratungsgegenstand nicht überwiesen wurde.

(2) Die Berichterstattung obliegt dem federführenden Ausschuss.

#### § 77 Einberufung und Durchführung der Ausschusssitzungen, Pressekonferenzen der Ausschüsse

(1) Die oder der Vorsitzende des Ausschusses lädt unter Angabe von Ort und Zeit nach Maßgabe des vom Präsidium festgelegten Sitzungsplanes zur Ausschusssitzung ein.
Sie oder er erstellt den Entwurf der Tagesordnung im Benehmen mit den Mitgliedern des Ausschusses.
Dies gilt auch bei der konstituierenden Ausschusssitzung.
In diesem Fall stellt die Präsidentin oder der Präsident das Benehmen zur Tagesordnung her.
Sie oder er verteilt die Einladung mit dem Entwurf der Tagesordnung an die Mitglieder des Ausschusses, die Präsidentin oder den Präsidenten, die Fraktionen, die Gruppen, die Mitglieder der Landesregierung, den Landesrechnungshof, die Landesbeauftragten im Sinne des Artikels 74 der Verfassung des Landes Brandenburg und den Rat für Angelegenheiten der Sorben/Wenden.
Der Ausschuss beschließt zu Beginn der Sitzung über die Tagesordnung.
Der Ausschuss kann die Tagesordnung jederzeit ändern; nicht bereits im Entwurf enthaltene Beratungsgegenstände können jedoch nur auf die Tagesordnung genommen werden, wenn kein Mitglied des Ausschusses widerspricht.

(2) Die Einladung mit Angabe der Tagesordnungspunkte soll den Beteiligten nach Absatz 1 Satz 5 in der Regel mindestens drei Tage vor der Sitzung übersandt werden.

(3) Gesetzentwürfe, Sachanträge und Änderungsanträge werden gemäß Anlage 9 § 5 eingebracht und verteilt.
Anträge auf Empfehlungen im Sinne von § 75 Absatz 1 Satz 2 müssen mindestens drei Tage vor der Sitzung eingebracht werden.

(4) Ein Ausschuss ist unverzüglich einzuberufen, wenn ein Fünftel seiner Mitglieder dies gemäß Anlage 9 § 5 unter Angabe des Beratungsgegenstandes beantragt.

(5) In dringenden Fällen kann ein Ausschuss ausnahmsweise während sitzungsfreier Zeiten auf Antrag eines Fünftels seiner Mitglieder einberufen werden, wenn das Präsidium zustimmt.

(6) Sitzungen der Ausschüsse sind Pflichtsitzungen.
Sie finden grundsätzlich am Sitz des Landtages statt.
Ausnahmen kann die Präsidentin oder der Präsident auf schriftlichen Antrag zulassen.

(7) Die oder der Vorsitzende des Ausschusses veranlasst die Veröffentlichung der Einladung auf der Internetseite des Landtages.

(8) Der Ausschuss kann die Öffentlichkeit und die Medien über seine Arbeit unterrichten.
An Pressekonferenzen, die auf Beschluss eines Ausschusses abgehalten werden, ist jede Fraktion mit mindestens einem Mitglied zu beteiligen.
Entsprechendes gilt für Mitglieder von Gruppen und fraktionslose Mitglieder des Landtages, soweit sie Mitglied in diesem Ausschuss sind.
Über den Inhalt seiner Presseerklärungen beschließt der Ausschuss.
Die Presseerklärung muss auch die Ansichten der Minderheiten wiedergeben.

#### § 77a Durchführung von Ausschusssitzungen unter Zuschaltung von Mitgliedern

(1) Sitzungen der Ausschüsse können ausnahmsweise abweichend von § 77 Absatz 6 unter Zuschaltung aller oder einzelner Mitglieder per von der Landtagsverwaltung zur Verfügung gestellter Videokonferenztechnik stattfinden, sofern nicht im Rahmen der Benehmensherstellung über die Tagesordnung ein Drittel der Mitglieder widerspricht.
Satz 1 gilt nicht für Sitzungen im Sinne der §§ 80a und 80b, Sitzungen des Petitionsausschusses und von Untersuchungsausschüssen.

(2) Zugeschaltete Mitglieder gelten als anwesend im Sinne der Geschäftsordnung.

(3) Sind alle oder einzelne Mitglieder zugeschaltet, wird namentlich abgestimmt, soweit nicht gesetzliche Regelungen entgegenstehen; geheime Wahlen werden entsprechend § 103 Absatz 2, 3 und 5 durchgeführt.

#### § 78 Berichterstattung der Ausschüsse an den Landtag

(1) Die Ausschüsse können für bestimmte Beratungsgegenstände ein Mitglied oder mehrere Mitglieder mit der Berichterstattung beauftragen.
Die Berichterstattung erfolgt, wenn der Ausschuss nichts anderes beschließt, schriftlich.

(2) Über Gesetzentwürfe und Anträge, zu denen in den Ausschussberatungen Änderungsempfehlungen beschlossen worden sind, ist schriftlich zu berichten.
In besonderen Fällen kann der schriftliche Bericht mündlich ergänzt werden.

(3) Der Bericht soll eine Beschlussempfehlung an den Landtag enthalten und die Ansichten und Anträge des federführenden Ausschusses darstellen.
Er hat die Stellungnahme der Minderheit und der beteiligten Ausschüsse wiederzugeben.

(4) Den Berichten und Beschlussempfehlungen zum Haushalt sind die Gegenüberstellungen mit den zahlenmäßigen Veränderungen der Einnahmen und Ausgaben (Veränderungsnachweise) beizufügen.

#### § 79 Teilnahme der Mitglieder des Landtages an Ausschusssitzungen

(1) In den Ausschüssen hat das ordentliche Mitglied und nur bei dessen Verhinderung ein stellvertretendes Mitglied Rede- und Stimmrecht.
Sind auch die stellvertretenden Mitglieder verhindert, kann im Einzelfall die Stellvertretung durch jedes Mitglied derselben Fraktion ausgeübt werden, wenn dies der oder dem Vorsitzenden des Ausschusses angezeigt wird; dies gilt nur für die Ausschüsse, deren Mitglieder gemäß § 74 Absatz 2 bestimmt wurden.

(2) Ein stellvertretendes Mitglied kann Rederecht beantragen, wenn das ordentliche Mitglied anwesend ist.
Mitglieder des Landtages, die dem Ausschuss nicht angehören, können ebenfalls Rederecht beantragen.
Werden von ihnen gestellte Anträge behandelt, haben sie beratende Stimme.

(3) Zur Wahrung der Arbeits- und Funktionsfähigkeit des Ausschusses kann der Ausschuss im Einzelfall Redezeiten einschränken.
Ein Antrag auf Schluss der Aussprache darf frühestens zur Abstimmung gestellt werden, nachdem jedem Ausschussmitglied die Gelegenheit gegeben wurde, zur Sache zu sprechen.

#### § 80 Öffentliche Sitzungen

(1) Die Sitzungen der Ausschüsse sind öffentlich, soweit nicht durch Gesetz oder durch diese Geschäftsordnung etwas anderes geregelt ist.

(2) Zu den öffentlichen Sitzungen sind Zuhörerinnen und Zuhörer sowie die Vertreterinnen und Vertreter der Medien zugelassen.
Die Präsidentin oder der Präsident kann in der Hausordnung des Landtages hierzu ein Einlassverfahren vorsehen.

(3) Öffentliche Sitzungen von Ausschüssen werden grundsätzlich per Livestream übertragen.
Ausnahmen sind zulässig, wenn ein Livestream zum Zeitpunkt der Ausschusssitzung aus rechtlichen oder organisatorischen Gründen nicht möglich ist.
In der Einladung der Ausschusssitzung ist auf die Livestream-Übertragung hinzuweisen.

#### § 80a Nichtöffentliche Sitzungen

(1) Der Ausschuss schließt die Öffentlichkeit aus, soweit überwiegende öffentliche oder schutzwürdige private Interessen dies zwingend erfordern.
Vom Ausschluss der Öffentlichkeit kann abgesehen werden, wenn der Ausschuss einvernehmlich geeignete Schutzmaßnahmen, wie Anonymisieren oder Pseudonymisieren, beschließt, die die Interessen gemäß Satz 1 hinreichend schützen.
Im Übrigen kann der Ausschuss auf Antrag eines Mitgliedes des Ausschusses die Öffentlichkeit mit einer Mehrheit von zwei Dritteln der anwesenden Mitglieder des Ausschusses ausschließen.

(2) Äußert ein Mitglied des Ausschusses Zweifel am Vorliegen der Voraussetzungen nach Absatz 1 Satz 1 oder 2 oder wird ein Antrag auf Ausschluss der Öffentlichkeit gemäß Absatz 1 Satz 3 gestellt, berät der Ausschuss hierüber in nichtöffentlicher Sitzung.

(3) Beschäftigte der Fraktionen, der Gruppen und der Mitglieder des Landtages sowie sonstige Personen haben nach Maßgabe von § 5a Absatz 2 und 4 der Verschlusssachenordnung Zutritt zu nichtöffentlichen Sitzungen gemäß Absatz 1 Satz 1; im Falle eines Ausschlusses der Öffentlichkeit gemäß Absatz 1 Satz 3 haben die Beschäftigten der Fraktionen, der Gruppen und der Mitglieder des Landtages Zutritt, sofern der Ausschuss nichts Abweichendes beschließt.
Der Zutritt der Bediensteten der Landtagsverwaltung richtet sich nach einer Richtlinie des Präsidiums.

(4) Hat der Ausschuss die Öffentlichkeit gemäß Absatz 1 Satz 1 ausgeschlossen oder Maßnahmen gemäß Absatz 1 Satz 2 beschlossen, darf über den Inhalt der Beratungen nichts mitgeteilt werden, was zur Preisgabe schutzwürdiger Daten und Informationen führen würde.
Wurde die Öffentlichkeit gemäß Absatz 1 Satz 3 ausgeschlossen, dürfen Beratungsgegenstand und -ergebnis, nicht jedoch die Äußerungen der Sitzungsteilnehmerinnen und Sitzungsteilnehmer und deren Abstimmungsverhalten mitgeteilt werden.
Über Presseerklärungen gemäß § 77 Absatz 8 Satz 4 und 5 beschließt der Ausschuss nach Maßgabe der Sätze 1 und 2.

#### § 80b Beratung über geheim zu haltende Beratungsgegenstände

(1) Der Ausschuss beschließt die Geheimhaltung durch Einstufung eines Beratungsgegenstandes in einen Geheimhaltungsgrad nach Maßgabe der Verschlusssachenordnung (Anlage 5), soweit überwiegende öffentliche oder schutzwürdige private Interessen oder gesetzliche Bestimmungen dies erfordern.

(2) Äußert ein Mitglied des Ausschusses Zweifel am Vorliegen der Voraussetzungen nach Absatz 1, berät der Ausschuss hierüber unter Beachtung derjenigen Geheimhaltungsvorkehrungen, die denen der in Zweifel gezogenen Einstufung des Beratungsgegenstandes entsprechen.

(3) Wird über einen in einen Geheimhaltungsgrad eingestuften Beratungsgegenstand beraten, dürfen nur die Ausschussmitglieder oder stellvertretenden Ausschussmitglieder teilnehmen.
Sonstige Personen können zur Beratung zugelassen werden, sofern sie nach Maßgabe der Verschlusssachenordnung zum Umgang mit Verschlusssachen dieser Einstufung berechtigt sind; § 82 Absatz 2 Satz 1 bleibt unberührt.

(4) Über Presseerklärungen gemäß § 77 Absatz 8 Satz 4 und 5 beschließt der Ausschuss unter Beachtung der Belange des Geheimschutzes.

#### § 80c Ausschluss eines Mitgliedes des Landtages wegen unerlaubter Preisgabe schutzwürdiger Daten und Informationen

Gibt ein Mitglied des Landtages in gröblicher Verletzung von § 80a Absatz 4 Satz 1, § 91 Absatz 5 Satz 2, § 92 Absatz 3 Satz 1, § 93 Absatz 2 Satz 1 oder von § 9 Absatz 1 der Datenschutzordnung schutzwürdige Daten oder Informationen preis, kann es für bis zu drei Monate von der Teilnahme an nichtöffentlichen Beratungen des Ausschusses ausgeschlossen werden.
Über die Verhängung sowie Dauer und Umfang eines Sitzungsausschlusses entscheidet das Präsidium auf Antrag des Ausschusses.
Dem betroffenen Mitglied des Landtages ist zuvor Gelegenheit zur Stellungnahme zu geben.

#### § 81 Anhörungen und Fachgespräche

(1) Ein Ausschuss kann beschließen, Sachverständige oder andere Personen, insbesondere soweit sie betroffene Interessen vertreten, mündlich oder schriftlich anzuhören; mitberatende Ausschüsse sind zu beteiligen.
Bei überwiesenen Vorlagen ist der federführende Ausschuss auf Verlangen eines Fünftels seiner Mitglieder verpflichtet, dem Verlangen auf Durchführung einer Anhörung zu entsprechen.
Die Beschlussfassung ist nur zulässig, wenn ein entsprechender Antrag auf der Tagesordnung des Ausschusses steht; dies gilt nicht für überwiesene Vorlagen.
Der Ausschuss soll auch die Anzahl der Anzuhörenden und der Fragen beschließen.
Der mitberatende Ausschuss kann beschließen, im Benehmen mit dem federführenden Ausschuss eine Anhörung durchzuführen, soweit dieser von der Möglichkeit einer Anhörung keinen Gebrauch macht und der mitberatende Ausschuss die Anhörung auf Teilfragen der Vorlage, die nur seinen Aufgabenbereich betreffen, beschränkt.
Dem federführenden Ausschuss sind Ort und Termin sowie der zu hörende Personenkreis rechtzeitig mitzuteilen.

(2) Sollen durch Gesetz allgemeine Fragen geregelt werden, die Gemeinden oder Gemeindeverbände unmittelbar berühren, sind die kommunalen Spitzenverbände anzuhören.
Im Falle einer grundlegenden Veränderung des Gesetzentwurfes während der parlamentarischen Beratung müssen die kommunalen Spitzenverbände vor der Beschlussfassung erneut die Gelegenheit zur Anhörung erhalten; über das Verfahren entscheidet die oder der Vorsitzende im Einvernehmen mit den Mitgliedern des Ausschusses.
Berühren Beratungsgegenstände die Rechte der Sorben und Sorbinnen/Wenden und Wendinnen nach Artikel 25 der Verfassung des Landes Brandenburg, hat der Rat für Angelegenheiten der Sorben/Wenden das Recht, angehört zu werden.
Die Sitzungsprotokolle der Anhörungen nach den Sätzen 1 bis 3 oder deren Entwürfe werden allen Mitgliedern des Landtages spätestens 48 Stunden vor der Abstimmung des Landtages über den Gesetzesentwurf oder den Beratungsgegenstand zugänglich gemacht.

(3) Eine Anhörung nach Absatz 1 erfolgt grundsätzlich öffentlich.
Bei Beratungsgegenständen, mit denen mehrere Ausschüsse befasst sind, kann nur der federführende Ausschuss den Ausschluss der Öffentlichkeit beschließen.

(4) Den Anzuhörenden sind die Fragen vorher schriftlich mitzuteilen.
Der Ausschuss fordert im Regelfall die Anzuhörenden auf, dem Landtag rechtzeitig eine schriftliche Stellungnahme zuzuleiten, sodass die Ausschussmitglieder vor der Sitzung von dieser Stellungnahme Kenntnis nehmen können.

(5) Eine weitere Anhörung in derselben Sache ist nur zulässig, wenn der Ausschuss es beschließt.
Satz 1 gilt auch, wenn der Landtag zur Vorbereitung einer dritten Lesung einen Gesetzentwurf an einen oder mehrere Ausschüsse überweist.

(6) Sachverständigen werden die notwendigen Reisekosten entsprechend den Bestimmungen des Bundesreisekostengesetzes erstattet.
Anträge auf Reisekostenerstattung sind unter Angabe der Bankverbindung innerhalb eines Monats nach Ende der Sitzung an die Verwaltung des Landtages zu richten.

(7) Die Absätze 1 bis 6 gelten, mit Ausnahme des Absatzes 1 Satz 3 und 4 sowie des Absatzes 3 Satz 1, für Anhörungen in Form eines Fachgespräches sinngemäß.
Wird bei überwiesenen Vorlagen eine Anhörung nach Absatz 1 Satz 2 verlangt, kann der Ausschuss die Anhörung in Form eines Fachgespräches nur im Einvernehmen mit den Antragstellerinnen und Antragstellern beschließen.
Antragstellerinnen und Antragsteller nach Absatz 1 Satz 2 haben keinen Anspruch darauf, dass die Anhörung in Form eines Fachgespräches durchgeführt wird.
Verfassungsrechtlich oder gesetzlich vorgeschriebene Anhörungen werden nicht in der Form eines Fachgespräches durchgeführt.
Absatz 1 Satz 5 ist auf Fachgespräche auch dann entsprechend anzuwenden, wenn ein Ausschuss eine Anhörung und der andere Ausschuss ein Fachgespräch durchführen möchte.
Im Rahmen von Fachgesprächen ist die Zuleitung einer schriftlichen Stellungnahme nach Absatz 4 entbehrlich.

#### § 82 Anwesenheitspflicht und Zutrittsrecht

(1) Ein Drittel der Mitglieder eines Ausschusses kann die Anwesenheit eines jeden Mitgliedes der Landesregierung verlangen.

(2) Die Mitglieder der Landesregierung und ihre Beauftragten haben zu den Sitzungen der Ausschüsse Zutritt.
Mitglieder der Landesregierung haben Rederecht.
Die oder der Vorsitzende des Ausschusses kann Beauftragten der Landesregierung das Wort erteilen.

(3) Die Mitglieder des Landesrechnungshofes und die Landesbeauftragten im Sinne von Artikel 74 der Verfassung des Landes Brandenburg haben zu den Sitzungen der Ausschüsse Zutritt.

#### § 83 Ausschussprotokoll

(1) Über jede Sitzung eines Ausschusses wird ein Protokoll angefertigt.
Es soll, soweit gesetzliche Bestimmungen oder die Verschlusssachenordnung nichts anderes regeln, mindestens enthalten:

1.  die Tagesordnung,
2.  Beginn und Ende der Sitzung,
3.  die anwesenden Mitglieder des Ausschusses,
4.  eine inhaltliche Wiedergabe der Beratungen, die Abstimmungsergebnisse sowie den vollen Wortlaut der Sachanträge und -beschlüsse als Anlage.

Bei nichtöffentlichen Sitzungen gemäß § 80a Absatz 1 Satz 3 kann der Ausschuss beschließen, dass, abweichend von Satz 2 Nummer 4, nur die Beschlüsse festgehalten werden.

(2) Der Entwurf des Protokolls der Sitzung soll, soweit gesetzliche Bestimmungen oder die Verschlusssachenordnung nichts anderes regeln, spätestens zwei Tage vor der folgenden planmäßigen Ausschusssitzung an die Mitglieder und stellvertretenden Mitglieder des Ausschusses sowie an das zuständige Mitglied der Landesregierung verteilt werden; für den Umgang mit Protokollentwürfen über nichtöffentliche Sitzungen gemäß § 80a Absatz 1 Satz 1 oder 3 gilt § 80a Absatz 4 Satz 1 oder 2 entsprechend.
Der Ausschuss beschließt über die Richtigkeit des Protokolls.
In seiner letzten Sitzung vor Ende der Wahlperiode beschließt der Ausschuss über das Verfahren bezüglich der noch nicht bestätigten Protokolle.

(3) Einsichtnahme und Veröffentlichung der Protokolle bestimmen sich nach Anlage 11.

#### § 84 Wahlprüfungsausschuss

(1) Für den Wahlprüfungsausschuss gilt das Wahlprüfungsgesetz.

(2) Den Vorsitz erhält die stärkste Oppositionsfraktion; die Stellvertretung erhält die stärkste die Regierung tragende Fraktion.

#### § 85 Petitionsausschuss

(1) Für den Petitionsausschuss gilt das Petitionsgesetz.

(2) Der Petitionsausschuss berät in nichtöffentlicher Sitzung, soweit nicht das Petitionsgesetz die Beratung einer Petition in öffentlicher Sitzung gestattet.
Über die schutzwürdigen privaten Daten und Informationen aus Petitionen haben die Mitglieder und stellvertretenden Mitglieder des Ausschusses Stillschweigen zu wahren; dies gilt auch für die Zeit nach dem Ausscheiden aus dem Landtag.
Geheimhaltungsbeschlüsse gemäß § 80b werden nicht gefasst.
Eine Einstufung von Petitionen und der dazugehörigen Akten auf der Grundlage der Verschlusssachenordnung erfolgt nicht.

(3) Beschäftigte der Fraktionen, der Gruppen und der Mitglieder des Landtages sowie sonstige Personen haben keinen Zutritt zu nichtöffentlichen Sitzungen des Petitionsausschusses, soweit der Ausschuss nichts anderes beschließt.
Der Zutritt der Bediensteten der Landtagsverwaltung richtet sich nach einer Richtlinie des Präsidiums.

(4) Beschließt der Petitionsausschuss, dem Landtag eine Petition zur endgültigen Beschlussfassung vorzulegen, oder wird dies von einer Fraktion oder zehn Mitgliedern des Landtages gemäß § 4 Absatz 2 Satz 2 des Petitionsgesetzes beantragt, legt der Petitionsausschuss dem Landtag die Petition verbunden mit einer Beschlussempfehlung vor.

(5) Abweichend von § 83 Absatz 1 Satz 2 werden im Sitzungsprotokoll des Petitionsausschusses regelmäßig nur die Beschlüsse festgehalten.

(6) Für Presseerklärungen des Petitionsausschusses gilt § 80b Absatz 4 entsprechend.

#### § 86 Untersuchungsausschüsse

Für Untersuchungsausschüsse gilt das Gesetz über die Einsetzung und das Verfahren von Untersuchungsausschüssen des Landtages Brandenburg.

#### § 87 Kommissionen

(1) Die Einsetzung, die Zusammensetzung und das Verfahren von Enquete-Kommissionen richten sich nach dem Gesetz über die Enquete-Kommissionen des Landtages Brandenburg.

(2) Die Einsetzung, die Zusammensetzung und das Verfahren der Parlamentarischen Kontrollkommission richten sich nach dem Brandenburgischen Verfassungsschutzgesetz.

(3) Die Einsetzung, die Zusammensetzung und das Verfahren der G 10-Kommission richten sich nach dem Gesetz zur Ausführung des Artikel 10-Gesetzes.

### Abschnitt 12  
Rat für Angelegenheiten der Sorben/Wenden beim Landtag

#### § 88 Berufung der Mitglieder und Konstituierung

(1) Die Präsidentin oder der Präsident beruft die nach § 5 Absatz 2 des Sorben/Wenden-Gesetzes gewählten Mitglieder des Rates für Angelegenheiten der Sorben/Wenden spätestens sechs Wochen nach Bekanntmachung des endgültigen Wahlergebnisses in ihr Amt und lädt sie zur konstituierenden Sitzung ein.
Sie oder er übernimmt die Sitzungsleitung bis zur Wahl der oder des Vorsitzenden.

(2) Bis zur Berufung der Mitglieder des Rates für Angelegenheiten der Sorben/Wenden übernimmt dessen Aufgaben der bisherige Rat.

#### § 89 Aufgaben und Rechte

(1) Der Rat für Angelegenheiten der Sorben/Wenden wirkt an Entscheidungen, die den Schutz, die Erhaltung und die Pflege der nationalen Identität und das Siedlungsgebiet der Sorbinnen und Sorben/Wendinnen und Wenden betreffen, mit.

(2) Den Mitgliedern des Rates für Angelegenheiten der Sorben/Wenden sind die Beratungsmaterialien nach § 40 dieser Geschäftsordnung zur Verfügung zu stellen.

(3) Soweit Angelegenheiten nach Absatz 1 in den Ausschüssen beraten werden, sind die Mitglieder des Rates für Angelegenheiten der Sorben/Wenden berechtigt, an den Sitzungen der Ausschüsse mit beratender Stimme teilzunehmen.

(4) Stellungnahmen des Rates für Angelegenheiten der Sorben/Wenden zu Gesetzentwürfen, Anträgen oder Entschließungsanträgen sind auf die Tagesordnung des jeweiligen Ausschusses zu setzen.

#### § 90 Unterstützung durch die Landtagsverwaltung

Die Landtagsverwaltung unterstützt den Rat für Angelegenheiten der Sorben/Wenden.

### Abschnitt 13  
Sonderregelungen nach der Verfassung des Landes Brandenburg

#### § 91 Verfahren bei der Wahl der Verfassungsrichterinnen und Verfassungsrichter

(1) Die Aufgaben des Ausschusses gemäß Artikel 112 Absatz 4 Satz 4 der Verfassung des Landes Brandenburg und gemäß § 4 des Verfassungsgerichtsgesetzes Brandenburg werden vom Hauptausschuss wahrgenommen.

(2) Wird es nach dem Verfassungsgerichtsgesetz Brandenburg erforderlich, die Präsidentin oder den Präsidenten, die Vizepräsidentin oder den Vizepräsidenten oder weitere Verfassungsrichterinnen und Verfassungsrichter zu wählen, hat der Hauptausschuss Vorschläge zur Wahl zu beraten.

(3) Die Kandidatinnen und Kandidaten für die Wahl in das Verfassungsgericht werden von den Mitgliedern des Hauptausschusses in einer vom Hauptausschuss zu bestimmenden Frist benannt.
Andere Wahlvorschläge von Mitgliedern des Landtages, Gruppen oder Fraktionen können dem Hauptausschuss innerhalb dieser Frist zugeleitet werden.
Die Präsidentin oder der Präsident informiert die Mitglieder des Landtages über die Frist.

(4) Der Hauptausschuss prüft, ob die vorgeschlagenen Personen die Voraussetzungen der §§ 2 und 3 des Verfassungsgerichtsgesetzes Brandenburg erfüllen.
Er fordert von ihnen die Erklärung nach § 3 Absatz 1 dieses Gesetzes an.

(5) Der Hauptausschuss berät über Angelegenheiten gemäß Absatz 1 nach Maßgabe des § 80a Absatz 1 Satz 1 und Absatz 3 in nichtöffentlicher Sitzung.
Die Ausschussmitglieder sind zum Stillschweigen über die ihnen bekannt gewordenen persönlichen Verhältnisse der Vorgeschlagenen sowie über die Erörterungen hierüber verpflichtet.

(6) Der Hauptausschuss führt eine Einigung über einen gemeinsamen Wahlvorschlag herbei und strebt dabei eine angemessene Vertretung der politischen Kräfte des Landes an.
Kommt eine Einigung zustande, unterbreitet der Hauptausschuss dem Landtag einen gemeinsamen Antrag mit Wahlvorschlag.
Berücksichtigt der gemeinsame Antrag Vorschläge nach Absatz 3 Satz 2 nicht, können diese Kandidatinnen oder Kandidaten dem Landtag ebenfalls zur Wahl vorgeschlagen werden.

(7) Kommt eine Einigung nicht zustande, wählt der Landtag auf Vorschlag der Fraktionen, Gruppen oder Mitglieder des Landtages aus dem Kreis der Kandidatinnen oder Kandidaten nach Absatz 3.
Bei der Wahl ist anzustreben, dass die politischen Kräfte des Landes angemessen mit Vorschlägen vertreten sind.

#### § 92 Verfahren bei der Wahl der Mitglieder des Landesrechnungshofes

(1) Die Aufgaben des Ausschusses gemäß Artikel 107 Absatz 2 Satz 2 der Verfassung des Landes Brandenburg sowie §§ 3 und 4 des Landesrechnungshofgesetzes werden vom Ausschuss für Haushaltskontrolle wahrgenommen.

(2) Unterbreitet die Präsidentin oder der Präsident des Landesrechnungshofes Wahlvorschläge, leitet die Präsidentin oder der Präsident des Landtages diese Vorschläge an den Ausschuss für Haushaltskontrolle weiter.

(3) Für die Beratungen des Ausschusses für Haushaltskontrolle über Angelegenheiten gemäß Absatz 1 gilt § 91 Absatz 5 entsprechend.
Der Ausschuss für Haushaltskontrolle kann unter den Bewerbungen eine Vorauswahl treffen und unterbreitet nach Anhörung der ausgewählten Personen dem Landtag einen Antrag mit Wahlvorschlag.
Kommt eine Einigung nicht zustande, wählt der Landtag aus dem Kreise der angehörten Personen auf Vorschlag der Fraktionen, Gruppen oder Mitglieder des Landtages.

#### § 93 Verfahren bei der Wahl der oder des Landesbeauftragten für den Datenschutz und für das Recht auf Akteneinsicht

(1) Die Aufgaben des Ausschusses gemäß Artikel 74 Absatz 1 Satz 2 der Verfassung des Landes Brandenburg und § 22 des Brandenburgischen Datenschutzgesetzes werden von dem für Inneres zuständigen Ausschuss wahrgenommen.

(2) Für die Beratungen des für Inneres zuständigen Ausschusses über Angelegenheiten gemäß Absatz 1 gilt § 91 Absatz 5 entsprechend.
Der für Inneres zuständige Ausschuss kann unter den Bewerbungen eine Vorauswahl treffen und unterbreitet nach Anhörung der ausgewählten Personen dem Landtag einen Antrag mit Wahlvorschlag.
Kommt eine Einigung nicht zustande, wählt der Landtag aus dem Kreise der angehörten Personen auf Vorschlag der Fraktionen, Gruppen oder Mitglieder des Landtages.

#### § 94 Verfahren nach Artikel&nbsp;94 der Verfassung des Landes Brandenburg, sonstige Informationen über Vorhaben der Europäischen Union

(1) Unterrichtungen der Landesregierung gemäß Artikel 94 der Verfassung des Landes Brandenburg an den Landtag und Frühwarndokumente werden nach deren Übermittlung an die Mitglieder des Landtages verteilt.

(2) Beantragt ein Mitglied des Landtages innerhalb einer Woche nach Verteilung der Unterrichtung oder des Frühwarndokuments schriftlich eine Befassung des Landtages, so übermittelt die Präsidentin oder der Präsident die Angelegenheit an den fachlich zuständigen Ausschuss zur Unterbreitung einer Beschlussempfehlung an den Landtag; für Angelegenheiten der Europäischen Union, insbesondere im Rahmen des Subsidiaritätsfrühwarnsystems, ist dies in der Regel der für Europaangelegenheiten zuständige Ausschuss.
Der Ausschuss kann im Rahmen seiner Beratung Stellungnahmen anderer Ausschüsse einholen.

(3) In eilbedürftigen Angelegenheiten entscheidet der fachlich zuständige Ausschuss anstelle des Landtages über dessen Stellungnahme.
Eilbedürftig sind Angelegenheiten, über die nach dem vom Präsidium festgelegten Terminplan der Landtag nicht mehr rechtzeitig beschließen kann.
Die Präsidentin oder der Präsident informiert die Mitglieder des Landtages über den Beschluss des Ausschusses.

(4) Die vom Ausschuss gemäß Absatz 3 getroffene Entscheidung ist abschließend, wenn nicht eine Fraktion oder ein Fünftel der Mitglieder des Landtages innerhalb einer Woche nach der Information im Sinne des Absatzes 3 Satz 3 schriftlich beantragen, die Angelegenheit dem Landtag zur Entscheidung vorzulegen.

(5) Unterrichtungen der Landesregierung gemäß Artikel 94 der Verfassung des Landes Brandenburg an den für Europaangelegenheiten zuständigen Ausschuss werden nach deren Übermittlung an die Mitglieder des Ausschusses verteilt.
Der Ausschuss erarbeitet eine Beschlussempfehlung an den Landtag, sofern ein Mitglied des Ausschusses dies innerhalb einer Woche nach Verteilung der Unterrichtung schriftlich bei der oder dem Vorsitzenden beantragt.
Absatz 2 Satz 2 gilt entsprechend.
In eilbedürftigen Angelegenheiten findet das Verfahren gemäß Absatz 3 und 4 Anwendung.

(6) Zum Zwecke der frühzeitigen Befassung mit Vorhaben der Europäischen Union informiert die Kontakt- und Informationsstelle des Landtages in Brüssel die Mitglieder des Landtages, den für Europaangelegenheiten zuständigen Ausschuss und die übrigen Ausschüsse im Rahmen der fachlichen Zuständigkeiten.
Einordnungen der Kontakt- und Informationsstelle des Landtages zu Frühwarndokumenten werden daher entsprechend der berührten Politikbereiche auch den fachlich zuständigen Ausschüssen des Landtages übermittelt.

### Abschnitt 14  
Niederschrift der Beratungen und Beurkundung ihrer Ergebnisse

#### § 95 Plenarprotokoll

(1) Über jede Sitzung des Landtages wird ein Wortprotokoll angefertigt.
Die Plenarprotokolle werden an die Mitglieder des Landtages, die Fraktionen, die Gruppen, die Mitglieder der Landesregierung, die Präsidentin oder den Präsidenten des Landesrechnungshofes, die Landesbeauftragten im Sinne von Artikel 74 der Verfassung des Landes Brandenburg sowie den Rat für Angelegenheiten der Sorben/Wenden verteilt.
Das Plenarprotokoll enthält:

1.  Inhaltsübersicht,
2.  Wiedergabe alles Gesprochenen,
3.  die Namen der Rednerinnen und Redner,
4.  die zu den einzelnen Gegenständen gefassten Beschlüsse in ihrem Wortlaut mit dem Abstimmungsergebnis,
5.  alle ausdrücklich zur Niederschrift abgegebenen Erklärungen,
6.  die Abstimmungslisten namentlicher Abstimmungen,
7.  die Anwesenheitslisten gemäß § 3 Absatz 3.

(2) Jede Rednerin und jeder Redner erhält vor der Verteilung des Protokolls die Stenografisch aufgenommene Niederschrift ihrer oder seiner Rede zur Durchsicht und etwa erforderlichen Berichtigung zugestellt.

(3) Die im Protokoll enthaltene Rede soll eine getreue Wiedergabe des gesprochenen Wortes sein.
Die Rednerin oder der Redner ist daher nur berechtigt, Unrichtigkeiten und sprachliche Fehler zu beseitigen.
Berichtigungen dürfen den Sinn der Rede oder ihrer einzelnen Teile nicht ändern.

(4) Wird die Stenografisch aufgenommene und nach Absatz 2 zugestellte Niederschrift nicht innerhalb eines Tages zurückgesandt, wird sie mit dem Vermerk „Von der Rednerin/dem Redner nicht überprüft“ unverändert in Druck gegeben.

(5) Wird die Berichtigung nach Absatz 2 beanstandet und keine Verständigung mit der Rednerin oder dem Redner erzielt, ist die Entscheidung der Präsidentin oder des Präsidenten einzuholen.

(6) Die Fraktionen, die Gruppen und die Landesregierung erhalten vor der Prüfung der Reden durch die Rednerinnen und Redner eine vorläufige Stenografisch aufgenommene Niederschrift zur internen Unterrichtung.
Aus der vorläufigen Stenografisch aufgenommenen Niederschrift darf nur von der Rednerin oder dem Redner selbst wörtlich zitiert werden.

#### § 96 Beschlussprotokoll

(1) Über jede Sitzung des Landtages wird ein Beschlussprotokoll gefertigt.
Es wird an die Mitglieder des Landtages verteilt und gilt als genehmigt, wenn bis Ablauf einer Woche seit der Verteilung kein Einspruch erhoben wird.

(2) Wird die Fassung des Beschlussprotokolls beanstandet und der Einspruch nicht durch eine Erklärung der in der Sitzung amtierenden Präsidentin oder des in der Sitzung amtierenden Präsidenten behoben, befragt die Präsidentin oder der Präsident den Landtag.
Wird der Einspruch für begründet erachtet, ist die neue Fassung der beanstandeten Stelle noch während der Sitzung vorzulegen.

#### § 97 Ausfertigung und Zustellung

Die vom Landtag gefassten Beschlüsse werden durch die Präsidentin oder den Präsidenten ausgefertigt und, soweit sie Angelegenheiten aus dem Zuständigkeitsbereich der Landesregierung betreffen, der Ministerpräsidentin oder dem Ministerpräsidenten übermittelt.

### Abschnitt 15  
Sonstige Bestimmungen

#### § 98 Verfahren zur Behandlung von Gegenständen gemäß Volksabstimmungsgesetz

Das Verfahren zur Behandlung von Volksinitiativen und Volksbegehren richtet sich nach dem Volksabstimmungsgesetz.

#### § 99 Informationen der Präsidentin oder des Präsidenten sowie Zuschriften

(1) Zur Unterrichtung der Mitglieder des Landtages gibt die Präsidentin oder der Präsident offizielle Informationen heraus.

(2) Eingaben zu allgemeinen Belangen, die der Präsidentin oder dem Präsidenten schriftlich zugegangen sind, werden, soweit sie keine Petitionen und nach Inhalt und Form dazu geeignet sind, an die Mitglieder des Landtages von der Präsidentin oder dem Präsidenten als Zuschriften verteilt.

#### § 100 Abweichungen von der Geschäftsordnung

Abweichungen von den Vorschriften der Geschäftsordnung sind unzulässig, wenn mindestens fünf Mitglieder des Landtages widersprechen.
In den Ausschüssen, in den Enquete-Kommissionen und dem Rat für Angelegenheiten der Sorben/Wenden sind Abweichungen im Einzelfall nur zulässig, soweit sie sich innerhalb des Aufgabenkreises des jeweiligen Gremiums bewegen und kein Mitglied des Gremiums widerspricht.

#### § 101 Auslegung der Geschäftsordnung

Über die Auslegung der Geschäftsordnung entscheidet in Einzelfällen das Präsidium.
Eine grundsätzliche, über den Einzelfall hinausgehende Auslegung entscheidet der Hauptausschuss.

#### § 102 Fristenberechnung

Bei Fristen, die nach Tagen berechnet werden, wird der Tag der Verteilung der Drucksachen nicht mitgerechnet.
Fällt der letzte Tag einer Frist auf einen Sonnabend, Sonntag oder einen Feiertag, tritt an seine Stelle der folgende Werktag.

#### § 103 Eilverfahren im Präsidium und in den Ausschüssen

(1) Bei besonderer Eilbedürftigkeit können Angelegenheiten im Ausnahmefall im Eilverfahren behandelt werden.

(2) Jedem Mitglied des Präsidiums oder eines Ausschusses ist dazu einzeln die entsprechende Vorlage gemäß Anlage 9 § 5 zu übermitteln, einschließlich einer Fristsetzung für Rückäußerungen.
Die Frist soll mindestens 48 Stunden betragen.

(3) Rückäußerungen haben gemäß Anlage 9 § 5 zu erfolgen.
Im Falle einer nicht fristgemäßen Rückäußerung gilt dies als Ablehnung.

(4) Beantragt ein Mitglied des Präsidiums oder eines Ausschusses Änderungen zu einer Vorlage, gilt die Zustimmung als nicht erteilt.

(5) Die Präsidentin oder der Präsident oder die oder der Vorsitzende des Ausschusses informiert über das Ergebnis des Eilverfahrens in der nächsten Sitzung des Präsidiums oder des Ausschusses.

#### § 104 Behandlung unerledigter Beratungsmaterialien am Ende der Wahlperiode

Am Ende der Wahlperiode gelten alle Beratungsmaterialien (§ 40 dieser Geschäftsordnung) als erledigt.
Dies gilt nicht für plebiszitäre Verfahren nach den Artikeln 76 bis 78 der Verfassung des Landes Brandenburg und für Petitionen.

Potsdam, den 25.
Juni 2020

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#a)  Diese Geschäftsordnung ist mit Wirkung vom 17.
Juni 2020 (Tag der Beschlussfassung) in Kraft getreten.
  
Die Änderung der Geschäftsordnung ist mit Wirkung vom 11.
November 2020 (Tag der Beschlussfassung) in Kraft getreten.  
Die Zweite Änderung der Geschäftsordnung ist am 15.
Dezember 2020 (Tag der Beschlussfassung) in Kraft getreten.
  
Die Dritte Änderung der Geschäftsordnung ist am 18.
Juni 2021 (Tag der Beschlussfassung) in Kraft getreten.
  
Die Vierte Änderung der Geschäftsordnung ist am 16.
Dezember 2021 (Tag der Beschlussfassung) in Kraft getreten.
  

[\*\*)](#b)  Im Falle eines Zugriffsverfahrens soll der Zugriff auf den Vorsitz des Hauptausschusses der stärksten regierungstragenden Fraktion und der Zugriff auf den Vorsitz des Ausschusses für Haushalt und Finanzen der stärksten Oppositionsfraktion vorbehalten bleiben; der Zugriff auf den Vorsitz des Wahlprüfungsausschusses muss der stärksten Oppositionsfraktion vorbehalten bleiben.

* * *

### Anlagen

1

[Anlage 1 - Festlegung der Rededauer während der Plenarsitzung des Landtages Brandenburg](/br2/sixcms/media.php/68/GVBl_I_20_2020-Anlage%201.pdf "Anlage 1 - Festlegung der Rededauer während der Plenarsitzung des Landtages Brandenburg") 175.0 KB

2

[Anlage 2 - Richtlinie für die Fragestunde](/br2/sixcms/media.php/68/GVBl_I_20_2020-Anlage%202.pdf "Anlage 2 - Richtlinie für die Fragestunde") 148.9 KB

3

[Anlage 3 - Richtlinie für die Aktuelle Stunde](/br2/sixcms/media.php/68/GVBl_I_20_2020-Anlage%203.pdf "Anlage 3 - Richtlinie für die Aktuelle Stunde") 143.5 KB

4

[Anlage 4 - Datenschutzordnung des Landtages Brandenburg](/br2/sixcms/media.php/68/GVBl_I_20_2020-Anlage%204.pdf "Anlage 4 - Datenschutzordnung des Landtages Brandenburg") 193.3 KB

5

[Anlage 5 - Verschlusssachenordnung des Landtages Brandenburg](/br2/sixcms/media.php/68/GVBl_I_20_2020-Anlage%205.pdf "Anlage 5 - Verschlusssachenordnung des Landtages Brandenburg") 208.7 KB

6

[Anlage 6 - Immunitätsrichtlinie des Landtages Brandenburg zu Artikel 58 der Verfassung des Landes Brandenburg](/br2/sixcms/media.php/68/GVBl_I_20_2020-Anlage%206.pdf "Anlage 6 - Immunitätsrichtlinie des Landtages Brandenburg zu Artikel 58 der Verfassung des Landes Brandenburg") 147.6 KB

7

[Anlage 7 - Wahlordnung](/br2/sixcms/media.php/68/GVBl_I_20_2020-Anlage%207.pdf "Anlage 7 - Wahlordnung") 216.6 KB

8

[Anlage 8 - Ordnung über Geheimhaltungspflichten und das Verfahren im Zusammenhang mit der Überprüfung von Mitgliedern des Landtages nach § 27 des Abgeordnetengesetzes](/br2/sixcms/media.php/68/GVBl_I_20_2020-Anlage%208.pdf "Anlage 8 - Ordnung über Geheimhaltungspflichten und das Verfahren im Zusammenhang mit der Überprüfung von Mitgliedern des Landtages nach § 27 des Abgeordnetengesetzes") 190.3 KB

9

[Anlage 9 - Verfahren bei der Einbringung, Verteilung und Veröffentlichung von Beratungsmaterialien und anderen Parlamentsdokumenten](/br2/sixcms/media.php/68/GVBl_I_20_2020-Anlage%209.pdf "Anlage 9 - Verfahren bei der Einbringung, Verteilung und Veröffentlichung von Beratungsmaterialien und anderen Parlamentsdokumenten") 218.3 KB

10

[Anlage 10 - Führung eines Lobbyregisters](/br2/sixcms/media.php/68/GVBl_I_20_2020-Anlage%2010.pdf "Anlage 10 - Führung eines Lobbyregisters") 149.7 KB

11

[Anlage 11 - Einsichtnahme in Protokolle, Veröffentlichung](/br2/sixcms/media.php/68/GVBl_I_20_2020-Anlage%2011.pdf "Anlage 11 - Einsichtnahme in Protokolle, Veröffentlichung") 170.0 KB