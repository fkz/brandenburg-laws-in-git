## Gesetz über das Versorgungswerk der Mitglieder der Landtage von Nordrhein-Westfalen, Brandenburg und Baden-Württemberg (Versorgungswerkgesetz Brandenburg - BbgVLTG)

### § 1  
Mitgliedschaft im Versorgungswerk

Die Mitgliedschaft der Mitglieder des Landtags Brandenburg im Versorgungswerk der Mitglieder der Landtage von Nordrhein-Westfalen, Brandenburg und Baden-Württemberg (Versorgungswerk – VLT) wird durch dieses Gesetz, das Abgeordnetengesetz, den zwischen den Landtagen Nordrhein-Westfalen, Brandenburg und Baden-Württemberg abzuschließenden Vertrag zwischen dem Landtag Nordrhein-Westfalen, dem Landtag Brandenburg und dem Landtag von Baden-Württemberg über das Versorgungswerk der Mitglieder der Landtage von Nordrhein-Westfalen, Brandenburg und Baden-Württemberg (Vertrag) sowie im Übrigen durch die Satzung des Versorgungswerks der Mitglieder des Landtags Nordrhein-Westfalen, des Landtags Brandenburg und des Landtags von Baden-Württemberg (Satzung) geregelt.
Das Versorgungswerk ist eine Körperschaft des öffentlichen Rechts mit Sitz in Düsseldorf.
Es erbringt seine Leistungen ausschließlich aus eigenen Mitteln.

### § 2  
Rechtsaufsicht, Verfahren und Datenübermittlung

(1) Das Versorgungswerk unterliegt den versicherungsaufsichtsrechtlichen Vorschriften des Landes Nordrhein-Westfalen.
Die Versicherungsaufsicht und die Körperschaftsaufsicht über das Versorgungswerk führt das Finanzministerium Nordrhein-Westfalen im Benehmen mit den für die Versicherungsaufsicht zuständigen Ministerien der Länder Brandenburg und Baden-Württemberg.

(2) Das Verwaltungsverfahren des Versorgungswerks richtet sich nach dem Verwaltungsverfahrensgesetz des Landes Nordrhein-Westfalen.
Auf die Vollstreckung von Verwaltungsakten des Versorgungswerks in den Ländern Brandenburg und Baden-Württemberg finden die in den jeweiligen Ländern geltenden Verwaltungsvollstreckungsgesetze Anwendung.

(3) Die Präsidentin oder der Präsident des Landtags Brandenburg ist befugt, dem Versorgungswerk Auskünfte über die brandenburgischen Mitglieder des Versorgungswerks und die sonstigen Leistungsberechtigten zu erteilen, soweit die Auskünfte für die Feststellung der Mitgliedschaft, der Beitragspflicht und der Versorgungsleistung erforderlich sind.
Das Versorgungswerk ist befugt, den Präsidentinnen oder den Präsidenten der Landtage von Nordrhein-Westfalen, Brandenburg und Baden-Württemberg Auskünfte über seine Mitglieder und sonstigen Leistungsberechtigten zu erteilen, soweit diese für die Gewährung von Leistungen nach den jeweiligen Abgeordnetengesetzen erforderlich sind.

### § 3  
Verwaltungskosten

(1) Die Verwaltungskosten des Versorgungswerks werden anteilig vom Landtag Brandenburg und vom Landtag von Baden-Württemberg getragen und dem Landtag Nordrhein-Westfalen erstattet.
Vorbehaltlich der Übergangsregelung in Artikel 8 Absatz 5 des Vertrages ist für den zu leistenden Anteil an den Gesamtkosten das Verhältnis der Zahlen der gesetzlichen Mitglieder des Landtags Nordrhein-Westfalen, des Landtags Brandenburg und des Landtags von Baden-Württemberg maßgeblich.
Solange Anwartschaften auf Leistungen bestehen oder Renten aus dem Versorgungswerk gezahlt werden, ist im Falle einer Kündigung oder Beendigung des Vertrages nach dessen Artikel 10 bei der Umlegung der Verwaltungskosten für den kündigenden Landtag die Zahl der Mitglieder des Versorgungswerks aus dem entsprechenden Land maßgeblich, sobald diese Zahl niedriger ist als die Zahl der gesetzlichen Mitglieder des Landtags.
Die anteilige Kostentragungspflicht gilt nicht für Aufwandsentschädigungen und Reisekosten der Mitglieder des Versorgungswerks.

(2) Das von den Mitgliedern des Versorgungswerks eingebrachte Vermögen wird gemeinsam verwaltet.
Die bis zum 1.
Dezember 2019 erworbenen Ansprüche der Mitglieder des Versorgungswerks der Mitglieder des Landtags Nordrhein-Westfalen und des Landtags Brandenburg bleiben unberührt.

(3) Abweichend von § 1 Satz 3 kann das Land zur Sicherstellung der versicherungsaufsichtsrechtlichen Mindestquote für die Verlustrücklage

1.  sich nach Maßgabe des Landeshaushalts an einem Zuschuss beteiligen sowie
2.  nach Maßgabe des jeweiligen Haushaltsgesetzes Garantien und sonstige Gewährleistungen zur Risikoentlastung übernehmen.

Die Höhe der Beteiligung an einem Zuschuss gemäß Nummer 1 oder einer Garantie oder einer Gewährleistung gemäß Nummer 2 bestimmt sich vorbehaltlich gesonderter Vereinbarungen nach dem Verhältnis der gesetzlichen Mitgliederzahlen der Landtage von Nordrhein-Westfalen, Brandenburg und Baden-Württemberg zueinander.

### § 4  
Organe und Dienstverhältnisse

(1) Organe des Versorgungswerks sind

1.  die Vertreterversammlung,
2.  der Vorstand und
3.  der oder die Vorstandsvorsitzende.

Der oder die Vorstandsvorsitzende vertritt das Versorgungswerk gerichtlich und außergerichtlich.

(2) In der Vertreterversammlung müssen die nordrhein-westfälischen, die brandenburgischen und die baden-württembergischen Mitglieder des Versorgungswerks angemessen vertreten sein.
Maßgeblich ist jeweils das Verhältnis der Mitgliederzahlen der Landtage von Nordrhein-Westfalen, Brandenburg und Baden-Württemberg.
Die brandenburgischen Mitglieder des Versorgungswerks wählen jeweils zu Beginn der Wahlperiode des Landtags Brandenburg die auf sie entfallenden Vertreterinnen und Vertreter und deren Stellvertreterinnen und Stellvertreter in die Vertreterversammlung.
Die Vertreterversammlung wählt die Mitglieder des Vorstands.
Dabei steht den brandenburgischen Mitgliedern das Vorschlagsrecht für die auf sie entfallenden Mitglieder des Vorstands zu.
Die Ämter der brandenburgischen Mitglieder der Vertreterversammlung und des Vorstands enden jeweils mit dem Ablauf der Wahlperiode des Landtags Brandenburg.
Die Amtsinhaberinnen und die Amtsinhaber führen ihre Ämter bis zur Wahl ihrer Nachfolgerinnen oder Nachfolger fort.

(3) Näheres zu den Organen des Versorgungswerks wird durch den Vertrag sowie durch die Satzung geregelt.
Für eine Übergangszeit bis zur Neuwahl der Vertreterinnen und Vertreter aus Brandenburg nach dem Ende der sechsten Wahlperiode des Landtags Brandenburg können der Vertrag und die Satzung abweichende Regelungen vorsehen, soweit diese wegen der bis zu diesem Zeitpunkt noch laufenden Amtsperiode der brandenburgischen Organmitglieder erforderlich sind.

### § 5  
Kündigung

Eine Kündigung des Vertrages bedarf der Zustimmung des Landtages.

### § 6  
Beitritt anderer Landtage

Die Satzung kann vorsehen, dass andere Landesparlamente der Bundesrepublik Deutschland dem Versorgungswerk beitreten können.
Der Beitritt bedarf der Zustimmung des Landtags Nordrhein-Westfalen, des Landtags Brandenburg und des Landtags von Baden-Württemberg.

### § 7  
Zustimmung zum Vertrag

(1) Dem Abschluss des Vertrages wird zugestimmt.
Die Präsidentin des Landtags Brandenburg wird ermächtigt, den Vertrag im Namen des Landtags Brandenburg zu unterzeichnen.

(2) Der Vertrag wird als Anlage zu diesem Gesetz bekannt gegeben.

(3) Der Tag, an dem der Vertrag in Kraft sowie der Vertrag zwischen dem Landtag Nordrhein-Westfalen und dem Landtag Brandenburg über das Versorgungswerk der Mitglieder des Landtags Nordrhein-Westfalen und des Landtags Brandenburg vom 14.
Januar 2014 (GVBl.
2013 I Nr. 23 S. 19) nach Artikel 11 Absatz 2 und 3 des Vertrags außer Kraft tritt, wird von der Präsidentin des Landtages im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt gegeben.

* * *

**Anlage**

[Vertrag zwischen dem Landtag Nordrhein-Westfalen, dem Landtag Brandenburg und dem Landtag von Baden-Württemberg über das Versorgungswerk der Mitglieder der Landtage von Nordrhein-Westfalen, Brandenburg und Baden-Württemberg](/de/vertraege-248897)