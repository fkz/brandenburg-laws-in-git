## Brandenburgisches Gesetz über Mindestanforderungen für die Vergabe von öffentlichen Aufträgen (Brandenburgisches Vergabegesetz - BbgVergG)

Der Landtag hat das folgende Gesetz beschlossen:

### Teil 1  
Allgemeine Vorschriften

#### § 1 Zweck des Gesetzes

Zweck des Gesetzes ist es, einen fairen Wettbewerb um das wirtschaftlichste Angebot bei der Vergabe öffentlicher Aufträge unter gleichzeitiger Berücksichtigung sozialer Aspekte zu fördern.

#### § 2 Anwendungsbereich und Begriffsbestimmungen

(1) Dieses Gesetz gilt für die Vergabe von öffentlichen Aufträgen im Sinne des Absatzes 2 durch öffentliche Auftraggeber im Sinne des Absatzes 3.
Teil 3 dieses Gesetzes gilt nur dann, wenn der geschätzte Auftragswert für Liefer- und Dienstleistungen 5 000 Euro (ohne Umsatzsteuer) und Bauleistungen 10 000 Euro (ohne Umsatzsteuer) erreicht oder überschreitet.
Für die Schätzung des Auftragswerts gilt § 3 der Vergabeverordnung in der Fassung der Bekanntmachung vom 11. Februar 2003 (BGBl.
I S. 169), die zuletzt durch Artikel 1 der Verordnung vom 12. April 2016 (BGBl. I S. 624) geändert worden ist, in der jeweils geltenden Fassung, entsprechend.

(2) Öffentliche Aufträge im Sinne dieses Gesetzes sind Öffentliche Aufträge und Konzessionen im Sinne der §§ 103 bis 105 des Gesetzes gegen Wettbewerbsbeschränkungen in der Fassung der Bekanntmachung vom 26.
Juni 2013 (BGBl.
I S. 1750, 3245), das zuletzt durch Artikel 1 des Gesetzes vom 17.
Februar 2016 (BGBl.
I S. 203) geändert worden ist, in der jeweils geltenden Fassung.
Für die Anwendbarkeit dieses Gesetzes gelten ferner die §§ 107 bis 109, 116, 120 Absatz 4 und § 132 des Gesetzes gegen Wettbewerbsbeschränkungen entsprechend.

(3) Öffentliche Auftraggeber im Sinne dieses Gesetzes sind öffentliche Auftraggeber im Land Brandenburg im Sinne der §§ 99, 100 Absatz 1 Nummer 1 und 2 Buchstabe b, § 101 Absatz 1 Nummer 1, 2 sowie § 101 Absatz 1 Nummer 3 in Verbindung mit § 100 Absatz 1 Nummer 2 Buchstabe b des Gesetzes gegen Wettbewerbsbeschränkungen.
Satz 1 gilt nicht, wenn die Auftraggeber Vergabeverfahren im Namen oder im Auftrag des Bundes oder eines anderen Landes der Bundesrepublik Deutschland durchführen.

(4) Dieses Gesetz ist entsprechend bei der Auftragsvergabe durch Empfänger von Zuwendungen anzuwenden, wenn

1.  die Zuwendungen ausschließlich aus Mitteln des Landes stammen und
2.  dies in den Zuwendungsbescheiden ausdrücklich angeordnet ist.

(5) Dieses Gesetz gilt entsprechend für Direktvergaben im Sinne von Artikel 5 Absatz 2, 4 und 6 der Verordnung (EG) Nr. 1370/2007 des Europäischen Parlaments und des Rates vom 23.
Oktober 2007 über öffentliche Personenverkehrsdienste auf Schienen und Straße und zur Aufhebung der Verordnung (EWG) Nr. 1191/69 und (EWG) Nr. 1107/70 des Rates (ABl. L 315 vom 3.12.2007, S. 1).

(6) Andere gesetzliche Bestimmungen über Mindestentgelte bleiben unberührt.
Diese sind insbesondere, in der jeweils geltenden Fassung:

1.  das Mindestlohngesetz vom 11.
    August 2014 (BGBl.
    I S. 1348), das durch Artikel 2 Absatz 10 des Gesetzes vom 17.
    Februar 2016 (BGBl.
    I S. 203, 230) geändert worden ist,
2.  das Arbeitnehmer-Entsendegesetz vom 20.
    April 2009 (BGBl.
    I S. 799), das zuletzt durch Artikel 2 Absatz 11 des Gesetzes vom 17.
    Februar 2016 (BGBl.
    I S. 203, 230) geändert worden ist,
3.  das Tarifvertragsgesetz in der Fassung der Bekanntmachung vom 25.
    August 1969 (BGBl. I S. 1323), das zuletzt durch Artikel 1 des Gesetzes vom 3.
    Juli 2015 (BGBl.
    I S. 1130) geändert worden ist, und
4.  das Arbeitnehmerüberlassungsgesetz in der Fassung der Bekanntmachung vom 3. Februar 1995 (BGBl.
    I S. 158), das zuletzt durch Artikel 7 des Gesetzes vom 11. August 2014 (BGBl.
    I S. 1348) geändert worden ist.

Andere gesetzliche Bestimmungen über Mindestentgelte sind auch die auf Grundlage der in Satz 2 genannten Gesetze erlassenen Rechtsverordnungen oder für allgemeinverbindlich erklärten Tarifverträge.

### Teil 2  
Regelungen über das Vergabeverfahren

#### § 3 Grundsätze der Vergabe

(1) Öffentliche Aufträge und Konzessionen werden nach Maßgabe der nachfolgenden Grundsätze sowie der weiteren Vorschriften dieses Gesetzes und der haushaltsrechtlichen Bestimmungen vergeben.
Die Bestimmungen des Gesetzes gegen Wettbewerbsbeschränkungen bleiben unberührt.

(2) Öffentliche Aufträge und Konzessionen werden im Wettbewerb und im Wege transparenter Verfahren an fachkundige und leistungsfähige Unternehmen sowie an Unternehmen vergeben, die nicht vom Vergabeverfahren ausgeschlossen worden sind.

(3) Die Teilnehmer an einem Verfahren zur Vergabe öffentlicher Aufträge und Konzessionen sind gleich zu behandeln, es sei denn, eine Benachteiligung ist aufgrund des Gesetzes gegen Wettbewerbsbeschränkungen, außerhalb dessen Anwendungsbereichs durch oder aufgrund eines Gesetzes geboten oder gestattet.

(4) Bei der Vergabe öffentlicher Aufträge und Konzessionen können Aspekte der Qualität und der Innovation sowie soziale und umweltbezogene Aspekte berücksichtigt werden, wenn sie im sachlichen Zusammenhang mit dem Auftragsgegenstand stehen und sich aus der Bekanntmachung, dem Aufruf zum Teilnahmewettbewerb, zur Interessenbekundung oder den Vergabeunterlagen ergeben.
Eine Verbindung zum Auftragsgegenstand kann auch in den in § 127 Absatz 3 Satz 2 des Gesetzes gegen Wettbewerbsbeschränkungen genannten Fällen angenommen werden.
Auftraggeber, die an § 55 der Landeshaushaltsordnung in der Fassung der Bekanntmachung vom 21. April 1999 (GVBl. I S. 106) in der jeweils geltenden Fassung gebunden sind, sollen nach den Sätzen 1 und 2 verfahren.

(5) Öffentliche Aufträge und Konzessionen werden unter Wahrung der Grundsätze der Wirtschaftlichkeit und der Verhältnismäßigkeit vergeben.

(6) Unberührt bleiben, in der jeweils geltenden Fassung:

1.  § 5 des Brandenburgischen Mittelstandsförderungsgesetzes vom 8.
    Mai 1992 (GVBl.
    I S.
    166), das zuletzt durch Artikel 6 des Gesetzes vom 24.
    Mai 2004 (GVBl.
    I S. 186, 194) geändert worden ist, und
2.  § 14 des Landesgleichstellungsgesetzes vom 4.
    Juli 1994 (GVBl.
    I S. 254), das zuletzt durch Artikel 1 des Gesetzes vom 5.
    Dezember 2013 (GVBl.
    I Nr. 35) geändert worden ist.

#### § 4 Regelungen zum öffentlichen Personennahverkehr

(1) Ein Auftrag über eine Leistung des öffentlichen Personennahverkehrs wird nur an einen Bieter vergeben, der sich gegenüber dem Auftraggeber verpflichtet, seine bei der Ausführung der Leistung eingesetzten Beschäftigten mindestens nach dem hierfür jeweils geltenden einschlägigen und repräsentativen Entgelttarifvertrag zu entlohnen und auch seinen auf das Entgelt bezogenen eigenen, gegebenenfalls weitergehenden tariflichen Pflichten in der gesamten Laufzeit des zu vergebenden Verkehrsvertrages ordnungsgemäß nachzukommen.
Dies muss Bestandteil des Angebots sein.
Der Auftraggeber bestimmt in der Bekanntmachung der Ausschreibung und in den Vergabeunterlagen den oder die Tarifverträge nach Satz 1 nach billigem Ermessen.
Die Regelungen der Verordnung (EG) Nr. 1370/2007 sind zu beachten.
Die Sätze 1 bis 4 finden keine Anwendung auf ein Unternehmen, das in einem anderen Mitgliedsstaat der Europäischen Union ansässig ist, soweit es als Unternehmen im Sinne des Artikels 1 Absatz 3 Buchstabe b der Richtlinie 96/71/EG des Europäischen Parlaments und des Rates vom 16.
Dezember 1996 über die Entsendung von Arbeitnehmern im Rahmen der Erbringung von Dienstleistungen (ABl.
L 18 vom 21.1.1997, S. 1) eine Arbeitnehmerin oder einen Arbeitnehmer in eine Niederlassung oder ein der Unternehmensgruppe angehörendes Unternehmen in der Bundesrepublik Deutschland entsendet, sofern für die Dauer der Entsendung ein Arbeitsverhältnis zwischen dem entsendenden Unternehmen und der Arbeitnehmerin oder dem Arbeitnehmer besteht.
Die Landesregierung wird ermächtigt, durch Rechtsverordnung festzulegen, in welchem Verfahren festgestellt wird, welche Tarifverträge als repräsentativ im Sinne des Satzes 1 anzusehen sind.
Die Rechtsverordnung kann auch die Vorbereitung der Entscheidung durch einen Beirat vorsehen; sie regelt in diesem Fall auch die Zusammensetzung des Beirats.
Die Landesregierung kann diese Ermächtigung ganz oder teilweise durch Rechtsverordnung auf ein Mitglied der Landesregierung übertragen.

(2) Aufgabenträger des Schienenpersonennahverkehrs sollen im Rahmen der Vergabe eines öffentlichen Dienstleistungsauftrags im Sinne der Verordnung (EG) Nr. 1370/2007, eines Auftrags im Sinne des § 2 Absatz 2 oder im Rahmen einer Direktvergabe im Sinne des § 2 Absatz 5 Auftragnehmer auf der Grundlage von Artikel 4 Absatz 5 der Verordnung (EG) Nr. 1370/2007 dazu verpflichten, den Arbeitnehmerinnen und Arbeitnehmern, die zuvor zur Erbringung der Verkehrsleistungen eingestellt wurden, ein Angebot zur Übernahme zu den bisherigen Arbeitsbedingungen zu unterbreiten.
Der bisherige Betreiber ist nach Aufforderung des Aufgabenträgers binnen sechs Wochen dazu verpflichtet, dem Aufgabenträger alle hierzu erforderlichen Informationen zur Verfügung zu stellen.

(3) Bei der Vergabe von länderübergreifenden Leistungen ist von der Vergabestelle vor Beginn des Verfahrens eine Einigung mit den beteiligten weiteren Vergabestellen anderer Länder über die Anforderungen nach Absatz 1 oder 2 anzustreben.
Kommt eine Einigung nicht zustande, so kann von Absatz 1 oder 2 zugunsten einer weniger weitgehenden Regelung, die für einen der beteiligten Auftraggeber gilt, abgewichen werden.

#### § 5 Nachweise

(1) Der Auftraggeber hat eine gültige Bescheinigung über die Eintragung in ein Verzeichnis gemäß § 48 Absatz 8 der Vergabeverordnung über geeignete Unternehmen oder Sammlungen von Eignungsnachweisen auch ohne besonderen Hinweis in der Bekanntmachung oder den Vergabeunterlagen an Stelle individueller Einzelnachweise anzuerkennen.
Die Pflicht zur Anerkennung kann nicht dadurch umgangen werden, dass an Inhalt oder Aktualität der Nachweise strengere Anforderungen gestellt werden, als sie für die Eintragung des Unternehmens in das Verzeichnis nach Satz 1 vorgesehen sind.
Unterhalb der gemäß § 106 des Gesetzes gegen Wettbewerbsbeschränkungen einschlägigen Schwellenwerte kann der Auftraggeber nach seiner Wahl § 50 der Vergabeverordnung zur Einheitlichen Europäischen Eigenerklärung entsprechend anwenden.

(2) Bei der Vergabe von Bauleistungen fordert der Auftraggeber von dem für den Zuschlag vorgesehenen Bieter, für den Fall, dass kein Nachweis nach Absatz 1 vorliegt, die Bescheinigung der Sozialkasse, der der Bieter kraft allgemeiner Tarifbindung angehört, über die Bruttolohnsumme und die geleisteten Arbeitsstunden sowie die Zahl der gewerblichen Beschäftigten.
Die Nachweise dürfen nicht älter als sechs Monate sein, sofern sie nicht Bestandteil eines Nachweises nach Absatz 1 sind.
War der Bieter in den vergangenen sechs Monaten nicht im Inland ansässig, so genügt eine Eigenerklärung, in diesem Zeitraum nicht gegen Verpflichtungen über die Entrichtung der Beiträge zur sozialen Sicherheit nach den Rechtsvorschriften des betreffenden Sitzstaates verstoßen zu haben.

(3) Hat ein Bieter in den letzten sechs Monaten vor Ablauf der Angebotsfrist einem Auftraggeber bereits Nachweise nach den Absätzen 1 und 2 oder andere Eignungsnachweise nach den Vergabe- und Vertragsordnungen vorgelegt, so fordert derselbe Auftraggeber von dem Bieter diese Eignungsnachweise nur noch an, wenn begründete Zweifel an der Eignung des Bieters bestehen.
Der Bieter weist den Auftraggeber darauf hin, dass er bereits in den letzten sechs Monaten Eignungsnachweise nach den Vergabe- und Vertragsordnungen zur Prüfung vorgelegt hat und benennt das dazugehörige Vergabeverfahren.

(4) Auf Nachunternehmer lautende Nachweise und Erklärungen sind vom Auftragnehmer vor Beginn der Nachunternehmerleistung vorzulegen.

### Teil 3  
Mindestentgelt

#### § 6 Mindestentgelt

(1) Die Regelungen dieses Teils finden keine Anwendung, wenn für die zu beschaffenden Leistungen bereits durch das Mindestlohngesetz, aufgrund des Arbeitnehmer-Entsendegesetzes oder durch andere gesetzliche Bestimmungen über Mindestentgelte im Sinne des § 2 Absatz 6 ein Mindestentgelt definiert ist, welches das Mindestarbeitsentgelt gemäß Absatz 2 erreicht oder übersteigt.

(2) Ein Auftrag wird nur an Bieter vergeben, die sich gegenüber dem Auftraggeber verpflichten, den bei der Erbringung von Leistungen eingesetzten Beschäftigten das zum Zeitpunkt der Angebotsabgabe geltende Mindestentgelt je Zeitstunde zu zahlen.
Das Mindestentgelt beträgt ab dem 1. Mai 2021 13 Euro je Zeitstunde.

(3) Das Mindestentgelt muss dem regelmäßig gezahlten Grundentgelt für eine Zeitstunde ohne Sonderzahlungen, Zulagen oder Zuschläge entsprechen.
Die Verpflichtung nach Satz 1 und Absatz 2 Satz 1 muss Bestandteil des Angebots sein.
Bei einer Lieferung gilt dies nur für die mit der Anlieferung zusammenhängenden Leistungen, insbesondere Transport, Aufstellung, Montage und Einweisung zur Benutzung.

(4) Wenn die Entlohnung der Arbeitnehmer nicht nach Zeitstunden, sondern anhand einer anderen Größe erfolgt, muss der Bieter ergänzend zu der Verpflichtung gemäß Absatz 3 Satz 2 spätestens im Rahmen der Kontrolle gemäß § 9 anhand einer transparenten und nachvollziehbaren Kalkulation glaubhaft machen, dass jeder Arbeitnehmer im Durchschnitt mindestens den in Absatz 2 Satz 1 definierten Mindestlohn erhält.
Wenn die Entlohnung der Arbeitnehmer sich aus einem Grundlohn und Leistungszuschlägen zusammensetzt, muss der Bieter glaubhaft machen, dass der Grundlohn jedes Arbeitnehmers mindestens dem in Absatz 2 Satz 1 definierten Mindestlohn entspricht.

(5) Wenn Arbeitnehmer in ihrer Arbeitszeit gleichzeitig für verschiedene Auftraggeber tätig sind, von denen nicht alle diesem Gesetz unterliegen, wie es beispielsweise bei Post- oder Wäschereidienstleistungen der Fall sein kann, ist der Mindestlohn gemäß Absatz 2 Satz 1 anteilig für die Arbeitszeit zu zahlen, die auf die Erfüllung der diesem Gesetz unterliegenden Aufträge entfällt.
Der Bieter muss ergänzend zu der Verpflichtung gemäß Absatz 3 Satz 2 spätestens im Rahmen der Kontrolle gemäß § 9 anhand einer transparenten und nachvollziehbaren Kalkulation glaubhaft machen, dass jeder Arbeitnehmer anteilig mindestens den in Absatz 2 Satz 1 definierten Mindestlohn erhält.

(6) Die Absätze 2 und 3 gelten nicht für:

1.  das Arbeitsentgelt nach § 43 des Strafvollzugsgesetzes,
2.  das Arbeitsentgelt behinderter Menschen in anerkannten Werkstätten nach § 138 des Neunten Buches Sozialgesetzbuch,
3.  die Auszubildendenvergütung nach § 17 des Berufsbildungsgesetzes vom 23. März 2005 (BGBl.
    I S. 931), das zuletzt durch Artikel 436 der Verordnung vom 31.
    August 2015 (BGBl.
    I S. 1474, 1538) geändert worden ist, in der jeweils geltenden Fassung,
4.  das Taschengeld nach § 2 Nummer 4 des Bundesfreiwilligendienstgesetzes vom 28. April 2011 (BGBl.
    I S. 687), das zuletzt durch Artikel 15 Absatz 5 des Gesetzes vom 20. Oktober 2015 (BGBl.
    I S. 1722, 1735) geändert worden ist, in der jeweils geltenden Fassung und
5.  das Taschengeld nach § 2 Nummer 3 des Jugendfreiwilligendienstegesetzes vom 16. Mai 2008 (BGBl.
    I S. 842), das durch Artikel 30 des Gesetzes vom 20. Dezember 2011 (BGBl.
    I S. 2854, 2923) geändert worden ist, in der jeweils geltenden Fassung.

(7) Bei der Vergabe von länderübergreifenden Leistungen ist von der Vergabestelle vor Beginn des Vergabeverfahrens eine Einigung mit den beteiligten weiteren Vergabestellen anderer Länder über die Anforderungen nach den Absätzen 2 und 3 anzustreben.
Kommt eine Einigung nicht zustande, so kann von den Absätzen 2 und 3 zugunsten einer weniger weitgehenden Regelung, die für einen der beteiligten Auftraggeber gilt, abgewichen werden.

#### § 7 Anpassung des Entgeltsatzes

(1) Die Landesregierung überprüft den in § 6 Absatz 2 genannten Entgeltsatz regelmäßig, mindestens aber alle zwei Jahre, und legt dem Landtag einen Entwurf zur Anpassung an eine Änderung der sozialen und wirtschaftlichen Verhältnisse vor, soweit diese erforderlich ist.
Bei der Überprüfung und Anpassung des Entgeltsatzes berücksichtigt die Landesregierung den Vorschlag der Kommission nach Absatz 2.
Die Landesregierung ist an den Vorschlag der Kommission nicht gebunden.

(2) Die Landesregierung wird ermächtigt, durch Rechtsverordnung eine Kommission unabhängiger Mitglieder zur Anpassung des Entgeltsatzes nach § 6 Absatz 2 einzurichten.
Die Kommission besteht aus insgesamt neun Mitgliedern, davon je zwei Mitglieder aus den Gruppen der abhängig Beschäftigten, der Arbeitgeber und der Wissenschaft sowie je einer Vertreterin oder einem Vertreter der für Wirtschaft und für Arbeit zuständigen Ministerien sowie einer vorsitzenden Person.
Die Landesregierung wirkt darauf hin, dass die gleichberechtigte Teilhabe von Frauen und von Männern gewährleistet ist.

(3) Das für Arbeit zuständige Mitglied der Landesregierung beruft die Mitglieder der Kommission, die Hälfte der einfachen Mitglieder und deren Vertreterinnen und Vertreter auf Vorschlag des für Wirtschaft zuständigen Mitglieds der Landesregierung.
Weitere Einzelheiten zur Zusammensetzung und Berufung der Kommission sowie zum Verfahren kann die Landesregierung durch Rechtsverordnung regeln.

(4) Die Landesregierung kann die Ermächtigungen nach den Absätzen 2 und 3 ganz oder teilweise durch Rechtsverordnung auf ein Mitglied der Landesregierung übertragen.

#### § 8 Nachunternehmer und Verleiher

Der Auftraggeber vereinbart mit dem Auftragnehmer, dass der Auftragnehmer die Nachunternehmer und Verleiher von Arbeitskräften vertraglich verpflichtet, dass diese ihren Beschäftigten im Rahmen ihrer vertraglichen Leistung mindestens die Arbeitsentgeltbedingungen gewähren, die für die vom Nachunternehmer oder dem Vertragspartner des Verleihers zu erbringenden Leistungen nach § 6 Absatz 2 und Absatz 3 Satz 1 maßgeblich sind.
Diese Verpflichtung erstreckt sich auf alle an der Auftragserfüllung beteiligten Unternehmen.
Der Auftraggeber hat darauf zu achten, dass der jeweils einen Auftrag weiter Vergebende die rechtsverbindliche Übertragung der Verpflichtung und ihre Einhaltung durch die von ihm beauftragten Nachunternehmer oder Verleiher sicherstellt und seinem unmittelbaren Auftraggeber auf Verlangen nachweist.
Die Kontrollrechte sind dabei auch zugunsten des Auftraggebers zu vereinbaren.

#### § 9 Kontrollen

(1) Der Auftraggeber ist verpflichtet, die Einhaltung der gemäß § 6 Absatz 2 und Absatz 3 Satz 1 und § 8 vereinbarten Vertragsbestimmungen zu überprüfen.
Die Überprüfung erfolgt als Bestandteil der Prüfung der Richtigkeit einer vom Auftragnehmer gestellten Rechnung und durch eine ausreichende Zahl von Stichproben.
Zu diesem Zweck sind Nachweispflichten des Auftragnehmers und für den Auftraggeber Betretungsrechte für betriebliche Grundstücke und Räume des Auftragnehmers sowie das Recht zur Befragung von Beschäftigten des Auftragnehmers zu vereinbaren, soweit sie für die Durchführung von Kontrollen erforderlich sind.
Bei der Überprüfung der Einhaltung der Vertragsbestimmungen im Sinne des Satzes 1 sind im Regelfall Bescheinigungen eines Steuerberaters oder Wirtschaftsprüfers über die Lohnhöhe oder darüber, dass alle Beschäftigten mindestens den jeweils einschlägigen Mindestlohn erhalten, ausreichend.
Von der Überprüfung gemäß Satz 1 kann abgesehen werden, wenn der Auftraggeber annehmen kann, dass die Vertragsbestimmungen im Sinne des Satzes 1 eingehalten werden, insbesondere weil

1.  Leistungen in Branchen beschafft werden, die regelmäßig deutlich übertariflich zahlen, oder
2.  Leistungen durch einen Auftragnehmer erbracht werden, der dem Auftraggeber bereits aus einer dauerhaften Geschäftsbeziehung bekannt ist.

(2) Erhält der Auftraggeber Kenntnis davon, dass der Auftragnehmer oder ein Nachunternehmer einer bei der Erfüllung der Leistungspflichten eingesetzten Arbeitnehmerin oder einem bei der Erfüllung der Leistungspflichten eingesetzten Arbeitnehmer nicht mindestens die nach dem Arbeitnehmer-Entsendegesetz oder dem Mindestlohngesetz geltenden Mindestarbeitsbedingungen gewährt, so hat er dies der für die Kontrolle der Einhaltung der genannten Gesetze zuständigen Stelle mitzuteilen.

#### § 10 Vertragsstrafe, Kündigung, Auftragssperre

(1) Um die Einhaltung der Verpflichtungen, die nach § 6 Absatz 2 und Absatz 3 Satz 1 und den §§ 8 und 9 Absatz 1 vereinbart sind, zu sichern, hat der Auftraggeber mit dem Auftragnehmer für jede vom Auftragnehmer zu vertretende Verletzung dieser Pflichten durch den Auftragnehmer, seine Nachunternehmer oder Verleiher eine Vertragsstrafe wegen nicht gehöriger Erfüllung zu vereinbaren.
Die Vertragsstrafe beträgt 1 Prozent des Auftragswertes.
Ist die Vertragsstrafe im Einzelfall unverhältnismäßig hoch, so ist sie vom Auftraggeber auf Antrag auf einen angemessenen Betrag herabzusetzen.
Die Summe der Vertragsstrafen nach diesem Gesetz darf insgesamt 5 Prozent des Auftragswertes nicht überschreiten.
Es ist vorzusehen, dass die Vertragsstrafe in den Fällen der §§ 6 und 8 je beschäftigter Person je Monat, in allen anderen Fällen nur insgesamt einmal berechnet werden kann.

(2) Der Auftraggeber vereinbart mit dem Auftragnehmer, dass die von diesem zu vertretende Verletzung der nach § 6 Absatz 2 und Absatz 3 Satz 1 und den §§ 8 sowie 9 Absatz 1 vereinbarten Pflichten durch den Auftragnehmer oder seine Nachauftragnehmer oder Verleiher den Auftraggeber nach Abmahnung zur Kündigung des Vertrages mit dem Auftragnehmer berechtigen.

(3) Hat ein Auftragnehmer schuldhaft seine nach § 6 Absatz 2 und Absatz 3 Satz 1 und die §§ 8 sowie 9 Absatz 1 vereinbarten Pflichten verletzt, so soll er für die Dauer von bis zu drei Jahren von der Teilnahme am Wettbewerb um Aufträge der in § 2 genannten Auftraggeber wegen mangelnder Eignung ausgeschlossen werden.
Die Auftragssperre ist von Auftraggebern, die nicht selbst privatrechtliche Unternehmen sind, der zentralen Informationsstelle zur Aufnahme in die Sperrliste gemäß § 11 zu melden.

#### § 11 Listung von Auftragssperren

(1) Das für Wirtschaft zuständige Ministerium der Landesregierung richtet eine zentrale Stelle ein, die Informationen über Auftragssperren nach § 10 Absatz 3 bereitstellt (zentrale Informationsstelle).

(2) Auftraggeber geben die von ihnen ausgeschlossenen Unternehmen der zentralen Informationsstelle unverzüglich bekannt.
Diese Mitteilung hat folgende Daten zu enthalten:

1.  den meldenden Auftraggeber,
2.  Datum und Aktenzeichen oder Vergabenummer,
3.  das betroffene Unternehmen und die betroffene Niederlassung mit Firma, Rechtsform, Sitz und Anschrift des Unternehmens, Registergericht und Handelsregisternummer,
4.  Gewerbezweig oder Branche mit CPV-Code der betroffenen Tätigkeiten,
5.  Beginn und Dauer des Vergabeausschlusses, Rechtsgrundlage des Ausschlusses.

Betrifft die Auftragssperre oder Sperre als Bezugsquelle ausschließlich eine selbstständige Niederlassung eines Unternehmens, so sind nur die Daten dieser Niederlassung zu melden.

(3) Die zentrale Informationsstelle nimmt die Meldung in eine Sperrliste ohne eigene Prüfung auf.
Unrichtige Daten werden unverzüglich berichtigt.
Die Sperrliste kann in Form einer automatisierten Datei geführt werden.
Die Datenübermittlung kann im Wege eines automatisierten Abrufverfahrens erfolgen.

(4) Der Auftraggeber unterrichtet das von ihm ausgeschlossene Unternehmen über den Ausschluss und über die zur Sperrliste gemeldeten Daten.

(5) Der Auftraggeber, der über den Ausschluss eines Unternehmens entschieden hat, verkürzt die Dauer des Ausschlusses oder hebt den Ausschluss auf, wenn der Nachweis der wiederhergestellten Zuverlässigkeit erbracht wird.
Die Zuverlässigkeit ist in der Regel wiederhergestellt, wenn die natürliche oder juristische Person durch geeignete organisatorische und personelle Maßnahmen Vorsorge gegen eine Wiederholung des vorgeworfenen Verhaltens getroffen, eine für die Eintragung maßgebliche unterlassene Handlung nachgeholt und einen darauf beruhenden Schaden ersetzt hat.
Eine Entscheidung nach Satz 1 ist der Informationsstelle unverzüglich zu melden und die Änderung oder Löschung der Eintragung zu veranlassen.

(6) Ist der Ausschluss eines Unternehmens aufgehoben oder ist die Ausschlussfrist abgelaufen, werden die Daten unverzüglich gelöscht.

#### § 12 Abfrage

(1) Die Auftraggeber sind verpflichtet, vor Entscheidungen über die Vergabe von öffentlichen Aufträgen bei der zentralen Informationsstelle abzufragen, inwieweit Eintragungen in der Sperrliste zu Bietern mit einem für den Zuschlag in Betracht kommenden Angebot vorliegen und eine Eintragung bei der Beurteilung der Zuverlässigkeit des Bewerbers oder Bieters zu berücksichtigen.
Die Auftraggeber sollen die Abfragen auch auf bereits benannte Nachauftragnehmer erstrecken.
Satz 1 gilt entsprechend vor Entscheidungen über die Beschränkung des Bieterkreises hinsichtlich der aussichtsreichen Bewerber, wenn der Bieterkreis beim Wegfall eines Bieters beschränkt würde.

(2) Bei Vergabeverfahren, auf die das Gesetz wegen des geschätzten Auftragswertes nach § 2 Absatz 1 nicht anwendbar ist, kann der Auftraggeber bei der Informationsstelle nachfragen, ob Eintragungen zu Bewerbern oder Bietern vorliegen.

(3) Erhält der Auftraggeber binnen drei Arbeitstagen von der Informationsstelle keine Auskunft, so kann er davon ausgehen, dass keine die Abfrage betreffenden Eintragungen vorliegen.

(4) Einer Abfrage bedarf es nicht, wenn die zentrale Informationsstelle in einem elektronischen Medium, das eine tägliche Erneuerung der Information zulässt, für die Leistung, die mit der Auftragsvergabe nachgefragt werden soll, allgemein bekannt macht, dass zurzeit keine Eintragungen vorliegen.

(5) Die Informationsstelle hat jederzeit auf Antrag einem Unternehmen und einer natürlichen Person Auskunft über die sie betreffenden Eintragungen in der Sperrliste zu erteilen.

#### § 13 Kostenerstattung

(1) Das Land gewährt den Verbandsgemeinden, mitverwalteten Gemeinden, mitverwaltenden Gemeinden, Ämtern, amtsfreien Gemeinden, kreisfreien Städten und Landkreisen (Kommunen) für den mit der Anwendung dieses Teils verbundenen Verwaltungsaufwand einen finanziellen Ausgleich.
Für die Verteilung an die Kommunen ist ein Betrag in Höhe von insgesamt 1 000 000 Euro für jedes Kalenderjahr vorgesehen.
Die Verteilung der Mittel erfolgt pauschal jeweils zu drei Vierteln nach der Einwohnerzahl und zu einem Viertel nach der Fläche der Kommunen; hierbei wird der Umfang der Aufgaben der kreisfreien Städte gemäß § 1 Absatz 2 der Kommunalverfassung des Landes Brandenburg berücksichtigt.
Die Auszahlung der Mittel erfolgt jährlich für das zurückliegende Kalenderjahr.

(2) Absatz 1 ist nicht anwendbar, wenn das im Mindestlohngesetz bestimmte Mindestentgelt die Höhe des Mindestentgelts gemäß § 6 Absatz 2 erreicht oder übersteigt.

(3) Sollte die Anwendbarkeit von Absatz 1 während eines laufenden Kalenderjahres enden, so endet der Ausgleichsanspruch am gleichen Kalendertag.
Die Höhe des Ausgleichs ist in diesem Fall nach dem Verhältnis der Kalendertage, an denen Absatz 1 anwendbar war, im Vergleich zu den Tagen, an denen dieser nicht anwendbar war, zu bemessen.

### Teil 4  
Übergangs- und Schlussbestimmungen

#### § 14 Verordnungsermächtigung

(1) Die Landesregierung wird ermächtigt, durch Rechtsverordnung Bestimmungen zu treffen über:

1.  die Bearbeitungsschritte der Kontrollen nach § 9 und die zur Wahrung des Datenschutzes zu treffenden Vorkehrungen,
2.  die Voraussetzungen und das Verfahren für die Zulassung von Verzeichnissen über geeignete Unternehmen oder Sammlungen von Eignungsnachweisen von nicht der Landesverwaltung angehörenden Stellen und
3.  die Voraussetzungen und das Verfahren für die Verhängung einer Auftragssperre nach § 10 Absatz 3 sowie Aufhebung oder Verkürzung einer Auftragssperre nach § 11 Absatz 5.

(2) Die Landesregierung kann die Ermächtigung nach Absatz 1 ganz oder teilweise durch Rechtsverordnung auf ein Mitglied der Landesregierung übertragen.

#### § 15 Einschränkung von Grundrechten

Durch die §§ 9, 10 Absatz 3 sowie die §§ 11 und 12 wird das Grundrecht auf Datenschutz (Artikel 11 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 16 Übergangsvorschriften

(1) Zum Zeitpunkt des Inkrafttretens dieses Gesetzes bereits begonnene Vergabeverfahren werden nach dem bisherigen Recht fortgesetzt und abgeschlossen.
Enthalten Verträge oder in laufenden Vergabeverfahren eingereichte Angebote Lohngleitklauseln für den Fall von Tarifänderungen, können diese mit Zustimmung des Auftragnehmers oder aller Bieter auf den laufenden Vertrag oder das laufende Vergabeverfahren angewendet werden.

(2) Für die Kostenerstattung des den Kommunen bis 31.
Dezember 2016 entstehenden Verwaltungsaufwands ist § 14 des Brandenburgischen Vergabegesetzes vom 21. September 2011 (GVBl.
I Nr. 19), das durch das Gesetz vom 11.
Februar 2014 (GVBl.
I Nr. 6) geändert worden ist, weiter anzuwenden.
Kostenerstattungsanträge nach § 14 des Brandenburgischen Vergabegesetzes vom 21.
September 2011 (GVBl.
I Nr. 19), das durch das Gesetz vom 11. Februar 2014 (GVBl.
I Nr. 6) geändert worden ist, können noch bis zum 31.
Dezember 2017 gestellt werden.

#### § 17 Inkrafttreten, Außerkrafttreten

(1) Dieses Gesetz tritt, vorbehaltlich des Absatzes 2, am 1.
Oktober 2016 in Kraft.
Gleichzeitig tritt das Brandenburgische Vergabegesetz vom 21.
September 2011 (GVBl.
I Nr. 19), das durch das Gesetz vom 11.
Februar 2014 (GVBl.
I Nr. 6) geändert worden ist, außer Kraft.

(2) § 13 tritt am 1.
Januar 2017 in Kraft.

(3) Das für Wirtschaft zuständige Mitglied der Landesregierung wird ermächtigt, die Vergabegesetz-Erstattungsverordnung vom 14.
Januar 2013 (GVBl.
II Nr. 6) aufzuheben.

Potsdam, den 29.
September 2016

Die Präsidentin  
des Landtages Brandenburg

Britta Stark