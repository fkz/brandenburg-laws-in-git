## Gesetz zu dem Fünften Staatsvertrag zur Änderung des Staatsvertrages über die Zusammenarbeit zwischen Berlin und Brandenburg im Bereich des Rundfunks

#### § 1

Dem am 30.
August 2013 vom Land Brandenburg unterzeichneten Fünften Staatsvertrag zur Änderung des Staatsvertrages über die Zusammenarbeit zwischen Berlin und Brandenburg im Bereich des Rundfunks (Fünfter Änderungsstaatsvertrag zum Medienstaatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

#### § 2

Der Tag, an dem der Fünfte Änderungsstaatsvertrag zum Medienstaatsvertrag nach seinem Artikel 2 Absatz 1 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

[zum Staatsvertrag](/vertraege/medien_stv_2014)