## Geschäftsordnung des Verfassungsgerichts des Landes Brandenburg

Auf Grund des § 10 des Verfassungsgerichtsgesetzes Brandenburg in der Fassung der Bekanntmachung vom 22. November 1996 (GVBl.
I S. 343), das zuletzt durch Artikel 1 des Gesetzes vom 18.
Juni 2018 (GVBl.
I Nr. 13) geändert worden ist, hat das Verfassungsgericht des Landes Brandenburg folgende Geschäftsordnung beschlossen:

### Erster Teil  
Allgemeines

#### § 1 Pflichten der Mitglieder des Verfassungsgerichts

(1) Die Rechte und Pflichten der Mitglieder des Verfassungsgerichts bestimmen sich nach der Verfassung des Landes Brandenburg und dem Gesetz über das Verfassungsgericht des Landes Brandenburg.

(2) Die Mitglieder des Verfassungsgerichts geben der Präsidentin oder dem Präsidenten Nachricht, falls sie durch Ortsabwesenheit oder Krankheit für längere Zeit an einer richterlichen Entscheidungstätigkeit gehindert sind.

(3) Die Mitglieder des Verfassungsgerichts sind zur Amtsverschwiegenheit verpflichtet.

(4) Bei der Beendigung seines Amtes hat jedes Mitglied des Verfassungsgerichts die ihm zugegangenen der Geheimhaltung bedürftigen Schriftstücke zur Vernichtung an die Geschäftsstelle zurückzugeben oder aktenkundig zu versichern, dass sie vernichtet worden sind.
Sind Schriftstücke als elektronische Dokumente zur Verfügung gestellt worden, sind sie und sämtliche Kopien spätestens bei der Beendigung des Amtes vollständig zu löschen und sämtliche Ausdrucke zu vernichten.
Dass dies geschehen ist, ist aktenkundig zu versichern.

#### § 2 Pflichten der Präsidentin oder des Präsidenten

(1) Die Präsidentin oder der Präsident vertritt das Verfassungsgericht nach außen und führt die allgemeine Verwaltung.
Sind die Präsidentin oder der Präsident und die Vizepräsidentin oder der Vizepräsident verhindert, so nimmt die dienstälteste Berufsrichterin oder der dienstälteste Berufsrichter die Befugnisse der Präsidentin oder des Präsidenten wahr.
Das Dienstalter bestimmt sich nach der Dauer der Mitgliedschaft im Verfassungsgericht.
Bei gleichem Dienstalter entscheidet das Lebensalter.

(2) Die Präsidentin oder der Präsident unterrichtet die übrigen Mitglieder des Verfassungsgerichts über alle wichtigen das Verfassungsgericht und dessen Mitglieder berührenden Vorgänge.
Fragen von grundsätzlicher Bedeutung werden mit den Mitgliedern des Verfassungsgerichts erörtert.

(3) Die Präsidentin oder der Präsident kann Verwaltungsgeschäfte an die Geschäftsleiterin oder den Geschäftsleiter des Verfassungsgerichts zur selbständigen Erledigung übertragen.

#### § 3 Amtssiegel

Das Verfassungsgericht führt ein Landessiegel mit der Umschrift „Verfassungsgericht des Landes Brandenburg“.

#### § 4 Amtstracht

(1) Die Mitglieder des Verfassungsgerichts tragen in der mündlichen Verhandlung die Amtstracht der Brandenburger Richterinnen und Richter.

(2) Die vor dem Verfassungsgericht auftretenden Rechtsanwältinnen und Rechtsanwälte tragen ihre Amtstracht.

#### § 5 Wissenschaftliche Mitarbeiter

Die Mitglieder des Verfassungsgerichts werden bei der Erfüllung ihrer Aufgaben durch eine bzw.
einen oder mehrere wissenschaftliche Mitarbeiterinnen oder Mitarbeiter unterstützt, die die Präsidentin oder der Präsident im Benehmen mit den übrigen Mitgliedern des Verfassungsgerichts bestimmt.
Sie sollen die Befähigung zum Richteramt besitzen.

#### § 6 Öffentlichkeitsarbeit

Presseerklärungen und sonstige Verlautbarungen des Verfassungsgerichts veranlasst die Präsidentin oder der Präsident.

### Zweiter Teil  
Ergänzende Verfahrensvorschriften

#### § 7 Zustellungen

Zustellungen werden von der Präsidentin oder von dem Präsidenten oder auf ihre oder seine Anordnung verfügt und von der Geschäftsstelle nach den Vorschriften des Landeszustellungsgesetzes bewirkt.

#### § 8 Abschriften

Wird ein Schriftsatz in Papierform eingereicht, genügt zunächst eine Abschrift.
Weitere Abschriften werden bei Bedarf nachgefordert.

#### § 9 Akteneinsicht

(1) Über Art und Zeit der Gewährung von Akteneinsicht (§ 16 VerfGGBbg) entscheidet die Präsidentin oder der Präsident; gegen diese Entscheidung kann das Verfassungsgericht angerufen werden.

(2) Nach Abschluss des Verfahrens wird auch den Beteiligten und ihren Bevollmächtigten Akteneinsicht nur gewährt, wenn ein berechtigtes Interesse glaubhaft gemacht wird und Belange der übrigen Beteiligten nicht entgegenstehen; die Akteneinsicht bezieht sich dabei nur auf Akten des Verfassungsgerichts.

(3) Verfahrensakten des Verfassungsgerichts werden Gerichten und Behörden grundsätzlich nicht überlassen.
Über Ausnahmen entscheidet das Verfassungsgericht.

#### § 10 Berichterstattung

(1) Entscheidungen des Verfassungsgerichts werden durch eine Berichterstatterin oder einen Berichterstatter vorbereitet.
Die eingehenden Verfahren werden getrennt nach Verfassungsbeschwerden und nach allen übrigen Verfahrensarten jeweils fortlaufend zur Berichterstattung auf die Mitglieder des Verfassungsgerichts - mit Ausnahme der Präsidentin oder des Präsidenten - verteilt.
Die Verteilung erfolgt für die Gruppe der Verfassungsbeschwerden und entsprechender Eilverfahren und für die Gruppe der übrigen Verfahrensarten jeweils in der alphabetischen Reihenfolge der Nachnamen der Mitglieder des Verfassungsgerichts.
Bei Bedarf kann die Präsidentin oder der Präsident eine Mitberichterstatterin oder einen Mitberichterstatter bestellen.
Mitberichterstatterin oder Mitberichterstatter kann auch die Präsidentin oder der Präsident sein.
Auf eine gleichmäßige Belastung der Mitglieder des Verfassungsgerichts ist Rücksicht zu nehmen.

(2) Beim Ausscheiden eines Mitgliedes des Verfassungsgerichts übernimmt dessen Nachfolgerin oder Nachfolger dessen Verfahren als Berichterstatterin oder Berichterstatter.
Ist eine Zuordnung der Nachfolgerin oder des Nachfolgers nicht möglich, werden die Verfahren der ausgeschiedenen Mitglieder - beginnend mit dem Aktenzeichen nach ältesten Verfahren - in alphabetischer Reihenfolge der Nachnamen der neu eingetretenen Mitglieder des Verfassungsgerichts auf diese übertragen.

(3) Scheidet ein Mitglied des Verfassungsgerichts aus, bevor ein Nachfolger oder eine Nachfolgerin gewählt ist, werden dessen Verfahren entsprechend Absatz 1 Sätze 2 und 3 wie Neueingänge auf die Mitglieder des Verfassungsgerichts als Berichterstatterinnen oder Berichterstatter verteilt.

(4) Die Berichterstatterin oder der Berichterstatter legt ein schriftliches Votum, gegebenenfalls in der Form eines begründeten Entscheidungsentwurfs, vor.
Jedes andere Mitglied des Verfassungsgerichts kann ein zusätzliches Votum einreichen.

#### § 11 Unterrichtung der Mitglieder des Verfassungsgerichts

(1) Vor den Verhandlungs- oder Beratungsterminen hat die Präsidentin oder der Präsident den Mitgliedern des Verfassungsgerichts eine Abschrift der Voten und der weiteren für die Verhandlung oder Beratung erforderlichen Schriftstücke zuzuleiten oder die Voten und Schriftstücke auf Wunsch als elektronische Dokumente auf einer sicheren Austauschplattform zur Verfügung zu stellen.

(2) Zwischen der Übersendung oder elektronischen Zurverfügungstellung der Voten und der Beratung soll in der Regel mindestens eine Woche liegen.

#### § 12 Verfahren nach §&nbsp;21 VerfGGBbg Umlaufverfahren

(1) Die Präsidentin oder der Präsident und die Berichterstatterin oder der Berichterstatter können auch schon vor der Beratung im Plenum Hinweise nach § 21 VerfGGBbg auf Bedenken gegen die Zulässigkeit oder Begründetheit von Anträgen geben.

(2) Hält die Präsidentin oder der Präsident im Falle des § 21 VerfGGBbg eine Entscheidung im Wege des Umlaufs für angezeigt, so übersendet sie oder er jedem Mitglied des Verfassungsgerichts einen von ihr oder ihm unterzeichneten Entscheidungsentwurf.
Jedes Mitglied des Verfassungsgerichts sendet den ihm übersandten Entwurf mit seiner Unterschrift versehen zurück, wenn es nicht eine Beratung verlangt.
Der Beschluss kommt mit Eingang der Zustimmung aller Mitglieder des Verfassungsgerichts zustande.

(3) Bei Wahl der elektronischen Form muss das Mitglied des Verfassungsgerichts der übermittelten Entscheidung seinen Namen hinzufügen und mit einer qualifizierten elektronischen Signatur versehen.

#### § 13 Ladung der Mitglieder des Verfassungsgerichts

(1) Zu den Beratungen und den mündlichen Verhandlungen werden die Mitglieder des Verfassungsgerichts von der Präsidentin oder von dem Präsidenten unter Angabe der zur Verhandlung oder Beratung anstehenden Verfahren mit einer Frist von mindestens einer Woche auf elektronischem Weg geladen.
In Eilfällen kann die Frist abgekürzt und von der Schriftform abgesehen werden.

(2) Die Mitglieder des Verfassungsgerichts unterrichten die Präsidentin oder den Präsidenten oder die Geschäftsstelle unverzüglich, wenn sie an einer Teilnahme verhindert sind.
Die Gründe der Verhinderung sind mitzuteilen.

#### § 14 Mündliche Verhandlung, Niederschrift, Tonaufnahme

(1) Über die mündliche Verhandlung vor dem Verfassungsgericht ist eine Niederschrift aufzunehmen.
Hierfür ist eine Urkundsbeamtin oder ein Urkundsbeamter der Geschäftsstelle zuzuziehen.
Die Niederschrift ist von der Präsidentin oder dem Präsidenten und der Urkundsbeamtin oder dem Urkundsbeamten zu unterschreiben.

(2) Darüber hinaus wird die mündliche Verhandlung in einer Tonaufnahme festgehalten.
Die Aufnahme steht den Mitgliedern des Verfassungsgerichts, der Urkundsbeamtin oder dem Urkundsbeamten der Geschäftsstelle und den Verfahrensbeteiligten zur Abhörung im Gericht zur Verfügung.
Überspielungen und private Übertragungen sind unzulässig.
Die Aufnahme ist nach Zustellung der Entscheidung zu löschen, sofern das Verfassungsgericht nicht die Archivierung beschließt.

#### § 15 Beratung

(1) Über den Gang der Beratung entscheiden die Mitglieder des Verfassungsgerichts.
Wirft die Sache mehrere Rechtsfragen auf, so wird über sie in der Regel nacheinander abgestimmt, bevor über den Tenor entschieden wird.

(2) Jedes Mitglied des Verfassungsgerichts, das an der Entscheidung mitgewirkt hat, kann bis zu deren Bekanntgabe die Fortsetzung der Beratung verlangen, weil es bisher nicht erörterte Gesichtspunkte vortragen möchte oder weil ihm ein Sondervotum dazu Anlass gibt.

(3) An der Beratung und Abstimmung dürfen die wissenschaftlichen Mitarbeiterinnen und Mitarbeiter zugegen sein, wenn sich nicht ein Mitglied des Verfassungsgerichts gegen die Anwesenheit ausspricht.

#### § 16 Form der Entscheidung

(1) Die Mitglieder des Verfassungsgerichts, die an der Entscheidung mitgewirkt haben, sind im Rubrum mit ihrem Namen in der Reihenfolge des Alphabetes nach der Präsidentin oder dem Präsidenten aufzuführen.
Amts- und Berufsbezeichnungen werden nicht angegeben.

(2) Entscheidungen, die nicht auf Grund einer mündlichen Verhandlung ergehen, erhalten das Datum des Tages, an dem die letzte Unterschrift vorliegt.

(3) Ist eine Entscheidung nicht einstimmig ergangen, wird das Stimmenverhältnis am Ende der Entscheidung mitgeteilt, wenn dies das überstimmte Mitglied des Verfassungsgerichts verlangt oder das Verfassungsgericht so beschließt.

(4) Der Entscheidung können amtliche Leitsätze beigefügt werden.
Sie werden vom Verfassungsgericht beschlossen.

#### § 17 Veröffentlichung

(1) Entscheidungen, die gemäß § 29 Abs. 2 Satz 1 VerfGGBbg im Gesetz- und Verordnungsblatt zu veröffentlichen sind, übersendet die Präsidentin oder der Präsident des Verfassungsgerichts der Präsidentin oder dem Präsidenten des Landtages.

(2) Die mit amtlichen Leitsätzen versehenen Entscheidungen des Verfassungsgerichts werden in einer vom Gericht autorisierten Sammlung (LVerfGE) veröffentlicht.
Über die Aufnahme anderer Entscheidungen in die Sammlung entscheidet die Präsidentin oder der Präsident.
Eine Veröffentlichung unterbleibt, wenn das Verfassungsgericht so beschließt.

#### § 18 Sondervotum

(1) Beabsichtigt ein Mitglied des Verfassungsgerichts, ein Sondervotum abzugeben, so hat es dies in der Beratung mitzuteilen, sobald der Stand der Beratung dies ermöglicht.

(2) Das Sondervotum wird den Beteiligten und allen sonstigen Stellen in der gleichen Weise bekannt gegeben und in der autorisierten Sammlung veröffentlicht wie die Entscheidung.

(3) Das Sondervotum, in dem ein Mitglied des Verfassungsgerichts seine in der Beratung vertretene abweichende Meinung zu der Entscheidung oder deren Begründung niederlegt, muss binnen drei Wochen nach Abfassung der Entscheidung der Präsidentin oder dem Präsidenten vorliegen.
Die Präsidentin oder der Präsident kann diese Frist einmalig verlängern.
Das Sondervotum entfällt, wenn es nicht innerhalb der Frist übergeben wird.

(4) Wird das Sondervotum innerhalb der Frist nach Absatz 3 zu einem Urteil abgegeben, so gibt die oder der Vorsitzende dies bei der Verkündung unter Nennung des Namens des dissentierenden Mitgliedes des Verfassungsgerichts bekannt.
Auf dessen Verlangen teilt die oder der Vorsitzende den wesentlichen Inhalt des Sondervotums mit; sie oder er stimmt sich in diesem Fall insoweit mit dem dissentierenden Mitglied des Verfassungsgerichts ab.

(5) Die Verkündung oder Zustellung der Entscheidung erfolgt grundsätzlich erst nach Vorliegen des Sondervotums.
In dringenden Fällen kann die Verkündung oder Zustellung erfolgen, bevor das Sondervotum zu den Akten gegeben ist.
In diesem Fall ist darauf hinzuweisen, dass ein Sondervotum beabsichtigt ist.

#### § 19 Verfahrensregister

Die Geschäftsstelle des Verfassungsgerichts führt ein Verfahrensregister (VfGBbg), in das die Sachen in der Reihenfolge ihres Eingangs jahrgangsweise eingetragen werden.
Verfahren des einstweiligen Rechtsschutzes werden separat gezählt und mit dem Zusatz EA versehen.

#### § 20 Allgemeines Register

(1) Anträge und Eingaben an das Verfassungsgericht, die nicht auf eine Rechtsprechungstätigkeit des Verfassungsgerichts gerichtet sind, werden in einem allgemeinen Register erfasst.
Sie werden von der Präsidentin oder von dem Präsidenten als Verwaltungsangelegenheit bearbeitet.

(2) Die Entscheidung darüber, ob ein Vorgang in das allgemeine Register einzutragen ist, trifft die Präsidentin oder der Präsident.

(3) Ein im allgemeinen Register eingetragener Vorgang ist in das Verfahrensregister zu übertragen, wenn der Einsender nach Rückfrage eine richterliche Entscheidung begehrt.

### Dritter Teil  
Schlussvorschriften

#### § 21 Änderung der Geschäftsordnung

(1) Jedes Mitglied des Verfassungsgerichts kann die Änderung der Geschäftsordnung beantragen.
Der Antrag soll schriftlich gestellt werden, einen Formulierungsvorschlag und eine Begründung enthalten.

(2) Über eine Änderung der Geschäftsordnung beschließt das Verfassungsgericht mit einfacher Mehrheit.

#### § 22 Inkrafttreten

Diese Geschäftsordnung tritt am 15.
Juli 2020 in Kraft.
Gleichzeitig tritt die Geschäftsordnung vom 15. Januar 2009 (GVBl.
2009 I S. 14) außer Kraft.

Potsdam, den 14.
Juli 2020

Verfassungsgericht des Landes Brandenburg  
Der Präsident

Möller