## Gesetz zu dem Staatsvertrag über die Bestimmung einer innerstaatlichen Institution nach dem Gesetz zu dem Übereinkommen vom 9. September 1996 über die Sammlung, Abgabe und Annahme von Abfällen in der Rhein- und Binnenschifffahrt (Bilgenentwässerungsverband-Staatsvertrag)

Der Landtag hat das folgende Gesetz beschlossen:

**§ 1**

Dem in Dresden am 11.
Mai 2010 unterzeichneten Staatsvertrag über die Bestimmung einer innerstaatlichen Institution nach dem Gesetz zu dem Übereinkommen vom 9.
September 1996 über die Sammlung, Abgabe und Annahme von Abfällen in der Rhein- und Binnenschifffahrt (Bilgenentwässerungsverband-Staatsvertrag) wird zugestimmt.
Der Vertrag wird nachstehend veröffentlicht.

**§ 2**

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Vertrag nach seinem Artikel 4 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 22.
September 2010

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Staatsvertrag](/vertraege/bilgenentwaesserungsverbandstv_2010)