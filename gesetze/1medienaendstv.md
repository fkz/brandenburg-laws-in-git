## Gesetz zum Ersten Staatsvertrag zur Änderung medienrechtlicher Staatsverträge (Erster Medienänderungsstaatsvertrag)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem am 10.
Juni 2020 vom Land Brandenburg unterzeichneten Ersten Staatsvertrag zur Änderung medienrechtlicher Staatsverträge (Erster Medienänderungsstaatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

#### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem Artikel 2 Absatz 2 Satz 1 am 1. Januar 2021 in Kraft.
Sollte der Staatsvertrag nach seinem Artikel 2 Absatz 2 Satz 2 gegenstandslos werden, ist dies unverzüglich im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 18.
November 2020

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke

* * *

Erster Staatsvertrag zur Änderung medienrechtlicher Staatsverträge (Erster Medienänderungsstaatsvertrag) ist gegenstandslos geworden ([GVBl.I/21 Nr.
2](https://bravors.brandenburg.de/br2/sixcms/media.php/76/GVBl_I_02_2021.pdf))

* * *

### Anlagen

1

[Erster Staatsvertrag zur Änderung medienrechtlicher Staatsverträge (Erster Medienänderungsstaatsvertrag)](/br2/sixcms/media.php/68/GVBl_I_31_2020-Anlage.pdf "Erster Staatsvertrag zur Änderung medienrechtlicher Staatsverträge (Erster Medienänderungsstaatsvertrag)") 295.4 KB