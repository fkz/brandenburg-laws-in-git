## Gesetz zu dem Abkommen zur zweiten Änderung des Abkommens über das Deutsche Institut für Bautechnik

Der Landtag hat das folgende Gesetz beschlossen:

### § 1 

Dem am 5.
Mai 2011 vom Land Brandenburg unterzeichneten Abkommen zur zweiten Änderung des Abkommens über das Deutsche Institut für Bautechnik wird zugestimmt.
Das Abkommen wird nachstehend veröffentlicht.

### § 2

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem das Abkommen nach seiner Nummer 2 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 29.
November 2012

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Abkommen](/vertraege/dibtabkommen_2014/4)