## Gesetz über die Rechtsstellung und die Finanzierung von Fraktionen und Gruppen im Landtag Brandenburg (Fraktionsgesetz - FraktG)

**Inhaltsübersicht**

### Abschnitt 1  
Status und Organisation

[§ 1 Bildung](#1)

[§ 2 Rechtsstellung](#2)

[§ 3 Aufgaben](#3)

[§ 4 Geschäftsordnung der Fraktion](#4)

[§ 5 Fraktionsmitarbeiterinnen und Fraktionsmitarbeiter](#5)

[§ 5a Geheimhaltungspflicht der Fraktionsmitarbeiterinnen und -mitarbeiter](#5a)

### Abschnitt 2  
Finanzen

[§ 6 Mittel zur Deckung des allgemeinen Bedarfs](#6)

[§ 7 Zweckbindung](#7)

[§ 8 Rücklagen und Kredite](#8)

[§ 9 Rückgewähr](#9)

[§ 10 Beginn und Ende der Ansprüche](#10)

[§ 11 Zahlungsvorschriften](#11)

[§ 12 Buchführung](#12)

[§ 13 Rechnungslegung](#13)

[§ 14 Veröffentlichung](#14)

[§ 15 Prüfung der Haushalts- und Finanzwirtschaft](#15)

### Abschnitt 3  
Auflösung der Fraktionen

[§ 16 Wegfall der Rechtsstellung](#16)

[§ 17 Liquidation](#17)

[§ 18 Verwendung des Fraktionsvermögens](#18)

[§ 19 Schriftgut](#19)

[§ 20 Haftung](#20)

### Abschnitt 4  
Gruppen

[§ 21 Bildung und Rechtsstellung](#21)

[§ 22 Geld- und Sachleistungen](#22)

[§ 23 Auflösung](#23)

### Abschnitt 1  
Status und Organisation

#### § 1 Bildung

(1) Fraktionen sind Vereinigungen von mindestens fünf Mitgliedern des Landtages, die derselben Partei, politischen Vereinigung oder Listenvereinigung angehören oder von derselben Partei, politischen Vereinigung oder Listenvereinigung als Wahlbewerberinnen oder Wahlbewerber aufgestellt worden sind.
Erhält eine Partei, politische Vereinigung oder Listenvereinigung bei der Landtagswahl mindestens 5 Prozent der im Wahlgebiet abgegebenen gültigen Zweitstimmen, ohne die für fünf Mitglieder notwendige Zweitstimmenanzahl zu erreichen, kann eine solche Fraktion abweichend von Satz 1 auch aus vier Mitgliedern bestehen.
Die Bildung einer Fraktion kann abweichend von Satz 1 oder nach Ablauf eines Monats seit der Konstituierung des Landtages erfolgen, wenn der Landtag zustimmt.
Die Bildung einer Fraktion kann bereits vor der Konstituierung des Landtages stattfinden; in diesem Fall ist sie bis zur Konstituierung des Landtages schwebend unwirksam.

(2) Die Bildung einer Fraktion ist der Präsidentin oder dem Präsidenten anzuzeigen; dies gilt ebenso für den Eintritt eines Mitgliedes des Landtages in die Fraktion sowie für den Austritt, das Ausscheiden oder den Ausschluss aus der Fraktion.

(3) Ein Mitglied des Landtages kann nur einer Fraktion angehören.

(4) Mitglieder des Landtages, die derselben Partei, politischen Vereinigung oder Listenvereinigung angehören oder aufgrund von Wahlvorschlägen derselben Partei, politischen Vereinigung oder Listenvereinigung in den Landtag gewählt wurden, dürfen jeweils nur eine Fraktion bilden.

#### § 2 Rechtsstellung

(1) Die Fraktionen wirken mit eigenen Rechten und Pflichten als selbstständige und unabhängige Gliederungen an der Arbeit des Landtages mit, sie sind Adressat der politischen Willensäußerung der Bürgerinnen und Bürger und unterstützen den parlamentarisch-politischen Willensbildungsprozess.

(2) Fraktionen sind, soweit sie am allgemeinen Rechtsverkehr teilnehmen, juristische Personen des Parlamentsrechts mit originärem Rechtscharakter und können unter ihrem Namen klagen oder verklagt werden.
Sie sind nicht Teil der öffentlichen Verwaltung und üben keine öffentliche Gewalt aus.

#### § 3 Aufgaben

(1) Fraktionen unterstützen ihre Mitglieder dabei, ihre parlamentarische Arbeit auszuüben und zur Verfolgung gemeinsamer Ziele aufeinander abzustimmen.
Sie wirken unmittelbar auf den parlamentarisch-politischen Willensbildungsprozess ein, indem sie eigene Standpunkte formulieren sowie Initiativen und Konzepte entwickeln und einbringen.

(2) Zu den Aufgaben von Fraktionen gehört eine eigenständige Öffentlichkeitsarbeit.
Sie dient der Unterrichtung der Öffentlichkeit über die parlamentarischen Vorgänge, Initiativen und Konzepte der Fraktionen, der Vermittlung ihrer politischen Standpunkte und dem Dialog mit den Bürgerinnen und Bürgern über parlamentarisch-politische Fragen.
Die Fraktionen sind im Rahmen ihrer zulässigen Aufgabenwahrnehmung in der Entscheidung über geeignete Mittel, Formen und Örtlichkeiten ihrer Öffentlichkeitsarbeit frei.
Zu den Mitteln und Formen gehört insbesondere auch die digitale Kommunikation.
Die Öffentlichkeitsarbeit der Fraktionen unterliegt nicht dem Gebot der politischen Neutralität.
Die Urheberschaft der Fraktion muss erkennbar sein.

(3) Die Fraktionen haben das Recht, im Rahmen ihrer zulässigen Aufgabenwahrnehmung mit anderen Fraktionen des Landtages, mit Fraktionen anderer Parlamente und mit Fraktionen in Kommunalvertretungen zusammenzuarbeiten sowie regionale, überregionale und internationale Kontakte zu pflegen.

(4) Das Nähere über die parlamentarischen Rechte und Pflichten einer Fraktion bestimmt die Geschäftsordnung des Landtages.

#### § 4 Geschäftsordnung der Fraktion

(1) Jede Fraktion gibt sich eine schriftliche Geschäftsordnung, die demokratischen Grundsätzen entsprechen und die als notwendige Fraktionsorgane die Fraktionsversammlung und einen Fraktionsvorstand oder eine Fraktionsvorsitzende oder einen Fraktionsvorsitzenden vorsehen muss.
Statt einer Fraktionsvorsitzenden oder eines Fraktionsvorsitzenden kann die Geschäftsordnung auch zwei gleichberechtigte Fraktionsvorsitzende vorsehen.
Ist in der Geschäftsordnung ein Fraktionsvorstand nicht vorgesehen, nimmt die Fraktionsvorsitzende oder der Fraktionsvorsitzende die in diesem Gesetz geregelten Rechte und Pflichten des Vorstandes wahr.
Zwei gleichberechtigte Fraktionsvorsitzende nehmen diese Rechte und Pflichten gemeinsam wahr.

(2) Die Geschäftsordnung der Fraktion muss Bestimmungen enthalten über

1.  den Namen der Fraktion und gegebenenfalls die gewählte Abkürzung,
2.  die Wahl des Fraktionsvorstandes oder, soweit ein solcher nicht vorgesehen ist, der Fraktionsvorsitzenden oder des Fraktionsvorsitzenden,
3.  die rechtsgeschäftliche Vertretung der Fraktion; hat die Fraktion zwei Fraktionsvorsitzende, so ist in der Geschäftsordnung auch zu bestimmen, ob sie, soweit durch Gesetz oder durch die Geschäftsordnung des Landtages nichts anderes geregelt ist, die Vertretung der Fraktion gemeinschaftlich oder allein wahrnehmen; zur Abgabe von Willenserklärungen gegenüber der Fraktion genügt die Erklärung gegenüber einer oder einem Fraktionsvorsitzenden,
4.  die Aufstellung und Verabschiedung des Fraktionshaushaltes, die Aufstellung und Prüfung der Jahresrechnung, die Person oder das Organ, die oder das für die ordnungsgemäße Verwendung der Mittel der Fraktion verantwortlich ist,
5.  Beitritt, Austritt und Ausschluss von Abgeordneten.

#### § 5 Fraktionsmitarbeiterinnen und Fraktionsmitarbeiter

(1) Für eine Vertretung von Fraktionsmitarbeiterinnen und Fraktionsmitarbeitern der Fraktionen gelten die Bestimmungen des Betriebsverfassungsgesetzes.

(2) Werden Fraktionsmitarbeiterinnen und Fraktionsmitarbeiter durch das Land Brandenburg im Anschluss an ein Arbeitsverhältnis mit einer Fraktion eingestellt, so gelten die tarifvertraglichen Bestimmungen über die Einstellung von Beschäftigten im unmittelbaren Anschluss an ein Arbeitsverhältnis im öffentlichen Dienst entsprechend.

#### § 5a Geheimhaltungspflicht der Fraktionsmitarbeiterinnen und -mitarbeiter

(1) Mitarbeiterinnen und Mitarbeiter der Fraktionen sind, auch nach Beendigung ihres Beschäftigungsverhältnisses, verpflichtet, über die ihnen bei ihrer Tätigkeit bekannt gewordenen Angelegenheiten Verschwiegenheit zu bewahren.
Dies gilt nicht für Tatsachen, die offenkundig sind oder ihrer Bedeutung nach keiner Geheimhaltung bedürfen.

(2) Mitarbeiterinnen und Mitarbeiter der Fraktionen dürfen, auch nach Beendigung ihres Beschäftigungsverhältnisses, ohne Genehmigung über solche Angelegenheiten weder vor Gericht noch außergerichtlich aussagen oder Erklärungen abgeben.
Die Genehmigung erteilt die oder der jeweilige Fraktionsvorsitzende.

(3) Für Honorarkräfte bei den Fraktionen, die für die Fraktion den Aufgaben der Fraktionsmitarbeiterinnen und Fraktionsmitarbeiter vergleichbare Aufgaben wahrnehmen, gelten die Absätze 1 und 2 entsprechend.

### Abschnitt 2  
Finanzen

#### § 6 Mittel zur Deckung des allgemeinen Bedarfs

(1) Die Fraktionen haben zur Erfüllung ihrer Aufgaben Anspruch auf finanzielle Mittel aus dem Landeshaushalt.
Die Mittel setzen sich aus einem Grundbetrag für jede Fraktion, aus einem Betrag für jedes Mitglied und einem weiteren Zuschlag für jede Fraktion, die nicht die Landesregierung trägt (Oppositionszuschlag), zusammen.
Zur Eröffnung der Möglichkeit der Mitwirkung an der parlamentarischen Kontrolle der Verfassungsschutzbehörde erhöht sich der Grundbetrag je Fraktion pauschal um 13,33 Prozent.
Die Mittel sind im Landeshaushalt nach Fraktionen getrennt auszuweisen.

(2) Der Landtag kann für einen gemäß seiner Geschäftsordnung zu bildenden Sonderausschuss beschließen, dass die Fraktionen für die Einstellung von Fraktionsbeschäftigten und den Vertragsschluss mit Honorarkräften sowie die Finanzierung ihrer Tätigkeit zur Unterstützung der Arbeit der Mitglieder des Sonderausschusses zusätzliche Mittel erhalten; diese Mittel sollen die Personaldurchschnittskosten einer Vollzeitstelle der Entgeltgruppe 14 des Tarifvertrages für den öffentlichen Dienst der Länder nicht übersteigen.
Für die Gruppen gilt Satz 1 entsprechend, wenn sie ein Ausschussmitglied stellen.

(3) Fraktionen müssen gegenüber der Präsidentin oder dem Präsidenten des Landtages die Erfüllung der Voraussetzungen des § 4 nachweisen.
Geschieht dies nicht in angemessener Frist, bestimmt die Präsidentin oder der Präsident die Aussetzung der Zahlung.

(4) Die Präsidentin oder der Präsident des Landtages legt dem Landtag im Zusammenhang mit den Haushaltsberatungen nach Anhörung der Fraktionen und unter Berücksichtigung der Preisentwicklung und der Tarifabschlüsse im öffentlichen Dienst einen Vorschlag zur Anpassung der Mittel vor.

(5) Die Präsidentin oder der Präsident des Landtages überlässt den Fraktionen im Benehmen mit dem Präsidium des Landtages zur Wahrnehmung ihrer Aufgaben unentgeltlich Räume zur Nutzung.
Ihnen ist dabei die organisatorische Verantwortung für die Nutzung von Büroräumen und Arbeitsplätzen im Landtag übertragen.
Das Hausrecht und die Polizeigewalt der Präsidentin oder des Präsidenten des Landtages nach Artikel 69 Absatz 4 Satz 3 der Verfassung des Landes Brandenburg bleiben unberührt.

(6) Der Landeshaushalt kann vorsehen, dass die Fraktionen neben den Mitteln nach den Absätzen 1 und 4 zur Wahrnehmung ihrer Aufgaben Sach- und Dienstleistungen, einschließlich der durch den Landtag zur Verfügung gestellten Informations- und Kommunikationseinrichtungen erhalten.
Bei Schäden durch ein unabwendbares Ereignis an durch die Fraktionen beschafften Sachen in den zur Nutzung überlassenen Räumen stehen den Fraktionen auf Antrag an die Präsidentin oder den Präsidenten des Landtages zusätzliche Mittel aus dem Landeshaushalt zur Verfügung.

(7) Die Zuführung weiterer Mittel aus anderen Haushaltstiteln darf nur durch Gesetz oder aufgrund eines Gesetzes erfolgen.

(8) Die Mittel nach Absatz 1 erhalten die Fraktionen zur Eigenbewirtschaftung.
Sie sind sparsam und wirtschaftlich zu verwenden.

(9) Die Präsidentin oder der Präsident des Landtages kann Ansprüche des Landtages gegen die Ansprüche der Fraktionen nach Absatz 1 aufrechnen.

#### § 7 Zweckbindung

(1) Leistungen nach diesem Gesetz dürfen die Fraktionen nur für Aufgaben verwenden, die ihnen nach der Verfassung des Landes Brandenburg, den Gesetzen und der Geschäftsordnung des Landtages obliegen.
Eine Verwendung für Zwecke der Parteien ist unzulässig.

(2) Aufwendungen von Abgeordneten dürfen aus Mitteln der Fraktionen nur erstattet werden, sofern diese durch die Fraktionen veranlasst werden und nicht bereits durch die gesetzliche Aufwandsentschädigung für Abgeordnete abgegolten sind.

#### § 8 Rücklagen und Kredite

(1) Die Fraktionen sind berechtigt, aus den ihnen zugewiesenen Mitteln eine allgemeine Rücklage zu bilden.
Die Rücklage darf 35 Prozent der jährlich gemäß § 6 Absatz 1 gezahlten Mittel nicht überschreiten.
Die Mittel können auch über die Wahlperiode hinaus übertragen werden.

(2) Die Aufnahme von Krediten durch Fraktionen ist unzulässig.

#### § 9 Rückgewähr

(1) Mittel, die nicht für die in den §§ 7 und 8 bestimmten Zwecke verwendet wurden, sind bis zum Ablauf der Frist nach § 13 Absatz 1 Satz 1 zurückzugeben.
Nach Ablauf der Frist kann die Präsidentin oder der Präsident des Landtages den Anspruch durch Verwaltungsakt geltend machen.
Der Anspruch verjährt zehn Jahre nach Ablauf der Frist.

(2) Verliert eine Vereinigung von Abgeordneten die Rechtsstellung als Fraktion, so sind Sachleistungen, die das Land Brandenburg zur Verfügung gestellt hat, zurückzugeben.

(3) Verringert sich die Zahl der Mitglieder einer Fraktion, sind Sachleistungen, die das Land Brandenburg zur Verfügung gestellt hat, insoweit zurückzuerstatten, als die Ausstattung über das im Landtag übliche Maß hinausgeht.

#### § 10 Beginn und Ende der Ansprüche

(1) Der Anspruch gemäß § 6 Absatz 1 entsteht mit dem Tag der Konstituierung der Fraktion, frühestens jedoch mit Beginn der Wahlperiode.
Im Falle des § 1 Absatz 1 Satz 3 besteht für den Zeitraum von der Konstituierung der Fraktion, jedoch frühestens ab der Konstituierung des neu gewählten Landtages, bis zur Entscheidung durch den Landtag ein Anspruch nach § 6 Absatz 1.

(2) Der Anspruch endet mit Ablauf des Monats, in dem die Vereinigung von Abgeordneten die Rechtsstellung als Fraktion verliert.

(3) Tritt eine Fraktion die Rechtsnachfolge für eine in der vorausgegangenen Wahlperiode bestehende Fraktion an, werden von der vorhergehenden Fraktion gemäß § 6 Absatz 1 für den Monat des Entstehens des Anspruches der nachfolgenden Fraktion bereits bezogene Mittel auf deren Anspruch angerechnet.

(4) Ändert sich im Laufe eines Monats die Zahl der Mitglieder der Fraktion, so wird der Zuschuss für diesen Monat nach der höheren Zahl berechnet.

#### § 11 Zahlungsvorschriften

(1) Die Auszahlung der Mittel erfolgt jeweils am ersten Werktag eines jeden Monats.
Über Ausnahmen entscheidet die Präsidentin oder der Präsident im Einvernehmen mit dem Präsidium des Landtages.

(2) Ist nur ein Teil zu leisten, so wird auf jeden Kalendertag ein Dreißigstel der vollen monatlichen Mittel gezahlt.

(3) Leistungen nach diesem Gesetz werden auf volle Euro nach oben gerundet.

#### § 12 Buchführung

(1) Die Fraktionen haben über ihre Einnahmen und Ausgaben nach Maßgabe des § 13 Absatz 3 Buch zu führen.
Die Vorschriften über das öffentliche Haushalts-, Rechnungs- und Kassenwesen finden auf die Fraktionen keine Anwendung.

(2) Aus den Mitteln der Fraktion beschaffte bewegliche Sachen sind, soweit sie den in § 6 Absatz 2 Satz 1 des Einkommensteuergesetzes in der jeweils geltenden Fassung festgesetzten Wert übersteigen, zu kennzeichnen und in einem besonderen Nachweis (Gegenstandsverzeichnis) aufzuführen.
Für vom Landtag beschaffte bewegliche Sachen, die die Fraktionen nach § 6 Absatz 6 zur Nutzung erhalten, erfolgen Kennzeichnung und Nachweis durch die Landtagsverwaltung.

(3) Die Rechnungsunterlagen sind fünf Jahre aufzubewahren.

#### § 13 Rechnungslegung

(1) Die Fraktionen haben bis zum 30.
Juni des auf das Haushaltsjahr folgenden Jahres gegenüber der Präsidentin oder dem Präsidenten des Landtages über die ordnungsgemäße Verwendung ihrer Mittel Rechnung zu legen.
Die Rechnung ist von der Fraktionsvorsitzenden oder dem Fraktionsvorsitzenden und der Person, die nach der Geschäftsordnung für die ordnungsgemäße Verwendung der Mittel verantwortlich ist, zu unterzeichnen.
Die Rechnung muss bei der Vorlage an die Präsidentin oder den Präsidenten des Landtages den mit einem Siegel versehenen Prüfungsvermerk einer Wirtschaftsprüferin oder eines Wirtschaftsprüfers oder einer Wirtschaftsprüfergesellschaft enthalten, in dem bestätigt wird, dass die Rechnung den Vorschriften der Abschnitte 1 und 2 entspricht.

(2) Verliert eine Vereinigung von Mitgliedern des Landtages die Rechtsstellung als Fraktion, so ist die Rechnung für den abgelaufenen Teil des Kalenderjahres innerhalb einer Frist von sechs Monaten zu legen.

(3) Die Rechnung ist wie folgt zu gliedern:

1.  Einnahmen:
    1.  Mittel nach § 6 Absatz 1 Satz 2,
    2.  Mittel nach § 6 Absatz 1 Satz 3,
    3.  Mittel nach § 6 Absatz 2,
    4.  Mittel, die den Fraktionen für die Arbeit in einem Untersuchungsausschuss vom Landtag zur Verfügung gestellt werden,
    5.  Mittel, die den Fraktionen für die Arbeit in einer Enquete-Kommission vom Landtag zur Verfügung gestellt werden,
    6.  Spenden, dabei ist bei Beträgen von mehr als 500 Euro die Höhe und die Spenderin oder der Spender auszuweisen,
    7.  Zinseinnahmen, die aus der Rücklage erzielt werden, soweit hierdurch der in § 8 Absatz 1 Satz 2 genannte Prozentsatz nicht überschritten wird,
    8.  sonstige Einnahmen;
2.  Ausgaben:
    1.  Ausgaben für Personal (Fraktionsbeschäftigte und Honorarkräfte), gegliedert nach der Finanzierung aus Mitteln,
        
        aa)
        
        die den Fraktionen nach § 6 Absatz 1 Satz 2 und 3 zur Verfügung gestellt werden,
        
        bb)
        
        die den Fraktionen für die Arbeit in einem Untersuchungsausschuss vom Landtag zur Verfügung gestellt werden,
        
        cc)
        
        die den Fraktionen für die Arbeit in einer Enquete-Kommission vom Landtag zur Verfügung gestellt werden und
        
        dd)
        
        die den Fraktionen nach § 6 Absatz 2 zur Verfügung gestellt werden.
        
    2.  Ausgaben zur Vergütung von Fraktionsmitgliedern, die in der Fraktion herausgehobene Funktionen wahrnehmen,
    3.  Ausgaben des laufenden Geschäftsbetriebes,
    4.  Ausgaben für Veranstaltungen,
    5.  Ausgaben für die Zusammenarbeit und Kontaktpflege nach § 3 Absatz 3,
    6.  Ausgaben für Öffentlichkeitsarbeit,
    7.  Ausgaben für Dienstleistungen Dritter,
    8.  Ausgaben für Investitionen,
    9.  Rückzahlungen an den Landtag nach diesem Gesetz,
    10.  sonstige Ausgaben.

(4) Die Rechnung muss außerdem die Forderungen und Verbindlichkeiten zu Beginn und Ende des Haushaltsjahres sowie die Höhe der Rücklage ausweisen.
Der Rechnung sind eine Stellenübersicht, aus der die Zahl der Fraktionsmitarbeiterinnen und Fraktionsmitarbeiter in den einzelnen Vergütungsgruppen ersichtlich ist, sowie das Gegenstandsverzeichnis nach § 12 Absatz 2 Satz 1 beizufügen.

(5) Solange die Fraktionen mit der Rechnungslegung in Verzug sind, kann die Zahlung der Mittel nach § 6 Absatz 1 ausgesetzt werden.

#### § 14 Veröffentlichung

Die Präsidentin oder der Präsident des Landtages veröffentlicht jährlich die Rechnungen der Fraktionen als Drucksache.

#### § 15 Prüfung der Haushalts- und Finanzwirtschaft

(1) Der Landesrechnungshof ist berechtigt, die Fraktionen zu prüfen.
Bei der Prüfung hat er der Rechtsstellung und den Aufgaben der Fraktionen im Sinne der §§ 2 und 3 Rechnung zu tragen.
Die Prüfung erstreckt sich auf die bestimmungsgemäße und wirtschaftliche Verwendung und Verwaltung der nach § 6 gewährten Mittel.
Der Landesrechnungshof prüft nicht die politische Erforderlichkeit und Zweckmäßigkeit von Maßnahmen der Fraktionen im Rahmen ihrer Aufgaben.

(2) Der Landesrechnungshof übermittelt der Fraktion nach den örtlichen Erhebungen unverzüglich den Entwurf einer Prüfungsmitteilung und gibt ihr Gelegenheit zur schriftlichen Stellungnahme innerhalb einer von ihm zu bestimmenden Frist.
Der Entwurf wird mit der Fraktion in einem Schlussgespräch bilateral erörtert.
Bei der Abfassung der abschließenden Prüfungsmitteilung sind die wesentlichen Argumente der Fraktionen zu würdigen.
Die abschließende Prüfungsmitteilung wird vom Großen Kollegium beschlossenen und anschließend der Fraktion übermittelt.
Zu diesem festgestellten Prüfergebnis kann die Fraktion innerhalb einer vom Landesrechnungshof zu bestimmenden Frist schriftlich Stellung nehmen.

(3) Der Landesrechnungshof würdigt die Stellungnahme und fasst die wesentlichen Prüfungsergebnisse zu der Fraktion in einem Bericht zusammen, den er der Präsidentin oder dem Präsidenten des Landtages und der geprüften Fraktion übermittelt.
Die Fraktion kann innerhalb einer Frist von zwei Monaten gegenüber der Präsidentin oder dem Präsidenten des Landtages zu diesem Bericht in der Sache Stellung nehmen.

(4) Soweit der Landesrechnungshof die Verwendung der Mittel beanstandet hat, entscheidet die Präsidentin oder der Präsident des Landtages nach Anhörung der betreffenden Fraktion über die erforderlichen Maßnahmen, insbesondere über die Geltendmachung eines Rückforderungsanspruchs nach § 9 Absatz 1 oder ein Absehen von einer Rückforderung.

(5) Die Präsidentin oder der Präsident des Landtages teilt ihre oder seine Entscheidung nach Absatz 4 dem Landesrechnungshof mit.
Entscheidungen nach Absatz 4 von grundsätzlicher oder erheblicher finanzieller Bedeutung werden mit ihren wesentlichen Gründen als Landtagsdrucksache veröffentlicht.
Die Präsidentin oder der Präsident des Landtages gibt den Fraktionen Gelegenheit, in der Drucksache zu den sie betreffenden Entscheidungen Stellung zu nehmen und ihre wesentlichen Argumente darzulegen.

### Abschnitt 3  
Auflösung der Fraktionen

#### § 16 Wegfall der Rechtsstellung

(1) Die Rechtsstellung nach § 2 entfällt

1.  bei Erlöschen des Fraktionsstatus,
2.  bei Auflösung der Fraktion,
3.  mit dem Ende der Wahlperiode.

(2) Die Rechte und Pflichten einer Fraktion gehen am Ende einer Wahlperiode auf eine in der folgenden Wahlperiode neu gebildete Fraktion über, wenn deren Mitglieder derselben Partei, politischen Vereinigung oder Listenvereinigung angehören und die neue Fraktion sich innerhalb eines Monats nach Beginn der neuen Wahlperiode konstituiert und die Rechtsnachfolge der Präsidentin oder dem Präsidenten angezeigt wurde.

(3) Nimmt eine Partei oder politische Vereinigung, die im Landtag mit einer Fraktion vertreten ist, nicht mehr an den Wahlen zum folgenden Landtag teil, so kann die Fraktion bis zum zwanzigsten Tag vor der Wahl gegenüber der Präsidentin oder dem Präsidenten des Landtages erklären, dass eine neu gebildete Fraktion, die bei einem Wahlerfolg einer anderen, der bestehenden Fraktion politisch nahe stehenden Partei, politischen Vereinigung oder Listenvereinigung aus dieser hervorgeht, die Rechtsnachfolge erklären kann.
Die Rechtsnachfolge ist innerhalb eines Monats nach Konstituierung des Landtages gegenüber der Präsidentin oder dem Präsidenten des Landtages zu erklären.

(4) Das gleiche gilt für eine Listenvereinigung, die im Landtag mit einer Fraktion vertreten ist und die nicht oder nicht in derselben Zusammensetzung an den Wahlen zum folgenden Landtag teilnimmt.

(5) Liegt kein Fall der Absätze 2 bis 5 vor, findet eine Liquidation statt.

#### § 17 Liquidation

(1) Die Fraktion gilt bis zur Beendigung der Liquidation als fortbestehend, soweit der Zweck der Liquidation es erfordert.
Für die Zeit der Liquidation führt die Fraktion den Zusatz „i. L.“ (in Liquidation).

(2) Die Liquidation erfolgt durch den Vorstand, soweit die Geschäftsordnung der Fraktion nichts Abweichendes bestimmt.
Der Vorstand kann auch andere Personen als Liquidatorinnen oder Liquidatoren mit der Durchführung der Liquidation beauftragen.
Werden innerhalb von vier Wochen nach dem Wegfall der Rechtsstellung gemäß § 16 Absatz 1 keine Personen mit der Liquidation beauftragt, so erfolgt die Beauftragung durch die Präsidentin oder den Präsidenten des Landtages.

(3) Die Liquidatorinnen oder Liquidatoren haben die laufenden Geschäfte zu beenden.
Sie können zu diesem Zweck neue Geschäfte eingehen.
Die Zweckbindung gemäß § 7 ist zu beachten.
Den Liquidatorinnen oder Liquidatoren obliegt es auch, gegenüber der Präsidentin oder dem Präsidenten des Landtages den Nachweis der ordnungsgemäßen Verwendung der Mittel der Fraktion nach § 13 Absatz 1 Satz 1 zu erbringen.

(4) Die Liquidatorinnen oder Liquidatoren unterrichten die Präsidentin oder den Präsidenten des Landtages fortlaufend über den Stand der Liquidation.
Nach dem Abschluss der Liquidation legen sie der Präsidentin oder dem Präsidenten des Landtages einen Bericht über ihre Tätigkeit vor.

#### § 18 Verwendung des Fraktionsvermögens

(1) Vermögenswerte, die aus Mitteln nach § 6 Absatz 1 angeschafft wurden, können zu marktangemessenen Preisen verkauft werden.

(2) Aus dem Fraktionsvermögen sowie aus den Mitteln, die der Fraktion gemäß Absatz 1 zufließen, ist eine Befriedigung der Ansprüche von Fraktionsmitarbeiterinnen und Fraktionsmitarbeitern vor weiteren Gläubigerinnen oder Gläubigern vorzunehmen.

(3) Soweit nach Beendigung der Liquidation Gelder verbleiben, sind diese dem Landeshaushalt zuzuführen.

(4) Sachwerte, die den Fraktionen gemäß § 6 Absatz 5 zur Nutzung überlassen wurden, sind an die Landtagsverwaltung zurückzugeben.

(5) Reicht das Fraktionsvermögen nicht aus, um Ansprüche der Fraktionsmitarbeiterinnen und Fraktionsmitarbeiter aus ihren Arbeitsverträgen zu befriedigen, können der Fraktion i. L.
auf Antrag einmalig Mittel in Höhe des Betrages gewährt werden, den die Fraktion, beginnend mit dem Monat, ab dem die Zahlung von Mitteln nach § 6 Absatz 1 wegfällt, aufwenden müsste, um bei einer Kündigung am Tage nach dem Wegfall der Rechtsstellung als Fraktion die Gehälter der Fraktionsmitarbeiterinnen und Fraktionsmitarbeiter bis zum Ablauf von Kündigungsfristen in den betreffenden Arbeitsverträgen, jedoch höchstens bis zu drei Monaten fortzuzahlen.
Die Entscheidung über den Antrag trifft die Präsidentin oder der Präsident im Einvernehmen mit dem Präsidium des Landtages.

#### § 19 Schriftgut

(1) Die Finanzakten und Personalakten der Fraktionen sind im Falle der Liquidation der Landtagsverwaltung zur Aufbewahrung entsprechend den gesetzlichen Bestimmungen zu übergeben.
Nach Ablauf der gesetzlichen Fristen sind die Akten zu vernichten.
Auf Antrag kann einer Fraktionsmitarbeiterin oder einem Fraktionsmitarbeiter die sie beziehungsweise ihn betreffende Personalakte auch ausgehändigt werden.

(2) Über das allgemeine Schriftgut der Fraktion wird auf Beschluss der Fraktionsversammlung verfügt.
Kommt ein solcher Beschluss nicht zustande, trifft der Vorstand einen entsprechenden Beschluss.
Kommt ein solcher Beschluss nicht zustande, entscheiden die Liquidatorinnen oder Liquidatoren.

#### § 20 Haftung

Fällt den Liquidatorinnen oder Liquidatoren bei der Durchführung der Liquidation ein Verschulden zur Last, so haften diese für den daraus entstandenen Schaden gegenüber den Gläubigerinnen oder Gläubigern als Gesamtschuldner.

### Abschnitt 4  
Gruppen

#### § 21 Bildung und Rechtsstellung

(1) Mindestens drei Mitglieder des Landtages, die keiner Fraktion angehören, können sich zu Gruppen zusammenschließen, wenn sie die gemäß § 1 Absatz 1 Satz 1 erforderliche Mindestanzahl von Mitgliedern nicht erreichen, jedoch die sonstigen Voraussetzungen des § 1 Absatz 1 Satz 1 erfüllen oder entsprechend § 1 Absatz 1 Satz 3 als Gruppe durch Zustimmung des Landtages anerkannt worden sind.
Die Bildung und Auflösung einer Gruppe sind der Präsidentin oder dem Präsidenten des Landtages schriftlich anzuzeigen; die Anzeige ist von allen Mitgliedern der Gruppe zu unterzeichnen.
Der Beitritt eines Mitgliedes zu einer Gruppe ist der Präsidentin oder dem Präsidenten des Landtages durch die rechtsgeschäftliche Vertretung nach Absatz 3 schriftlich anzuzeigen.
Bei einem Austritt genügt die Anzeige des ausgetretenen Mitgliedes.

(2) § 1 Absatz 2 bis 4 sowie §§ 2 und 3 gelten entsprechend.

(3) Die Gruppe gibt sich eine Geschäftsordnung, die demokratischen Grundsätzen entspricht und die mindestens Bestimmungen über die rechtsgeschäftliche Vertretung der Gruppe durch eines der Gruppenmitglieder, die Aufstellung und Verabschiedung des Gruppenhaushalts, die Aufstellung und die Prüfung der Jahresrechnung sowie die Person, die für die ordnungsgemäße Verwendung der Mittel der Gruppe verantwortlich ist, enthält.

#### § 22 Geld- und Sachleistungen

Gruppen im Sinne des § 21 Absatz 1 erhalten zur Unterstützung der Zusammenarbeit ihrer Mitglieder Leistungen in entsprechender Anwendung von § 6 mit folgenden Maßgaben:

1.  der Grundbetrag beträgt bei Gruppen 65 Prozent des einer Fraktion zustehenden entsprechenden Betrages;
2.  der Betrag pro Mitglied und der Oppositionszuschlag gemäß § 6 Absatz 1 Satz 2 beträgt bei Gruppen 100 Prozent des einer Fraktion zustehenden entsprechenden Betrages;
3.  die §§ 7, 8, 9, 10 Absatz 1, 2 und 4 sowie die §§ 11 bis 15 gelten entsprechend;
4.  der gemäß § 10 Absatz 1 maßgebliche Zeitpunkt ist der Eingang der Anzeige über die Bildung einer Gruppe im Sinne des § 21 Absatz 1 Satz 2 bei der Präsidentin oder dem Präsidenten des Landtages.

#### § 23 Auflösung

Für die Auflösung einer Gruppe durch Anzeige gemäß § 21 Absatz 1 Satz 2 oder zum Ende der Wahlperiode gelten die §§ 17 bis 20 entsprechend mit der Maßgabe, dass die für eine Fraktion durch den Vorstand vorzunehmenden Handlungen in der gemäß § 21 Absatz 3 festgelegten Weise erfolgen.