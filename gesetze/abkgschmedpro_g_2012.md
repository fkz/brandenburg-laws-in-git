## Gesetz zu dem Zweiten Abkommen zur Änderung des Abkommens über die Zentralstelle der Länder für Gesundheitsschutz bei Arzneimitteln und Medizinprodukten 

Der Landtag hat das folgende Gesetz beschlossen:

### § 1 

Dem Zweiten Abkommen vom 15.
Dezember 2011 zur Änderung des Abkommens über die Zentralstelle der Länder für Gesundheitsschutz bei Arzneimitteln und Medizinprodukten vom 30. Juni 1994, zuletzt geändert durch das Abkommen vom 9. Juli 1998, wird zugestimmt.
Das Abkommen wird nachstehend veröffentlicht.

### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem das Abkommen nach seinem Artikel II in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 16.
April 2012

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Abkommen](/vertraege/abkgschmedpro_2013)