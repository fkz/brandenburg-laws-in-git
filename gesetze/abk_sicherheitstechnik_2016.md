## Gesetz zu dem Abkommen zur Änderung des Abkommens über die Zentralstelle der Länder für Sicherheitstechnik

Der Landtag hat das folgende Gesetz beschlossen:

### § 1  
Zustimmung

Dem am 23.
Juli 2015 vom Land Brandenburg unterzeichneten Abkommen zur Änderung des Abkommens über die Zentralstelle der Länder für Sicherheitstechnik wird zugestimmt.
Das Abkommen wird nachstehend veröffentlicht.

### § 2  
Inkrafttreten

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem das Abkommen nach seinem § 2 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 25.
Januar 2016

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Abkommen](/vertraege/abk_sicherheitstechnik)

* * *

### Anlagen

1

[Abkommen zur Änderung des Abkommens über die Zentralstelle der Länder für Sicherheitstechnik](/br2/sixcms/media.php/68/GVBl_I_06_2016_001Anlage-zum-Hauptdokument.pdf "Abkommen zur Änderung des Abkommens über die Zentralstelle der Länder für Sicherheitstechnik") 642.0 KB