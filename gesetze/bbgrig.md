## Richtergesetz des Landes Brandenburg (Brandenburgisches Richtergesetz - BbgRiG)

Inhaltsübersicht
----------------

Kapitel 1  
Allgemeine Vorschriften
-----------------------------------

[§ 1       Geltungsbereich](#1)  
[§ 2       Richtereid](#2)  
[§ 3       Altersgrenzen](#3)  
[§ 4       Teilzeitbeschäftigung und Beurlaubung aus familiären Gründen](#4)  
[§ 5       Teilzeitbeschäftigung](#5)  
[§ 6       Freistellungen und berufliches Fortkommen](#6)  
[§ 7       Beteiligung der Spitzenorganisationen](#7)  
[§ 8       Verschwiegenheitspflicht](#8)  
[§ 9       Dienstliche Beurteilungen, Stellenausschreibungen](#9)  
[§ 9a      Richterliche Gleichstellungsbeauftragte](#9a)  
[§ 10     Geltung des Beamtenrechts und Freistellung von Verfassungsrichterinnen und Verfassungsrichtern](#10)

Kapitel 2  
Richterwahlausschuss
--------------------------------

[§ 11     Zuständigkeit des Richterwahlausschusses](#11)  
[§ 12     Wahl des Richterwahlausschusses](#12)  
[§ 13    Zusammensetzung](#13)  
[§ 14     Neuwahl](#14)  
[§ 15     Vorschlagslisten](#15)  
[§ 16     Erlöschen der Mitgliedschaft](#16)  
[§ 17     Ruhen der Mitgliedschaft](#17)  
[§ 18     Feststellung des Erlöschens oder Ruhens](#18)  
[§ 19     Ausschließungsgründe](#19)  
[§ 20     Einberufung](#20)  
[§ 21     Sitzung](#21)  
[§ 22     Beschlussfassung, Stellvertretung](#22)  
[§ 22a    Wahl zur Besetzung von Spitzenpositionen](#22a)  
[§ 23     Übernahme und Entlassung von Richterinnen und Richtern auf Probe und Richterinnen und Richtern kraft Auftrags](#23)  
[§ 24     Geschäftsordnung](#24)  
[§ 25     Entschädigung](#25)

Kapitel 3  
Richtervertretungen, Kontrollgremium IT und Vertretungen  
ehrenamtlicher Richterinnen und Richter
--------------------------------------------------------------------------------------------------------------

Abschnitt 1  
Gemeinsame Vorschriften für die Richtervertretungen
-----------------------------------------------------------------

[§ 26     Richterrat und Präsidialrat](#26)  
[§ 27     Rechtsstellung der Mitglieder, Geschäftsordnung](#27)  
[§ 28     Amtszeit](#28)  
[§ 29     Ruhen der Mitgliedschaft](#29)  
[§ 30     Erlöschen der Mitgliedschaft](#30)  
[§ 31     Kosten](#31)  
[§ 32     Rechtsweg](#32)

Abschnitt 2  
Richterräte
-------------------------

[§ 33    Bildung des Richterrats und der Stufenvertretungen](#33)  
[§ 34    Zusammensetzung](#34)  
[§ 35    Wahl und Bestimmung der Mitglieder](#35)  
[§ 36    Neuwahl](#36)  
[§ 37    Eintritt der Ersatzmitglieder](#37)  
[§ 38    Ausschluss, Auflösung](#38)  
[§ 39    Zuständigkeit der Richterräte](#39)  
[§ 40    Gemeinsame Angelegenheiten](#40)  
[§ 41    Mitbestimmung](#41)  
[§ 42    _(weggefallen)_](#42)  
[§ 43    Initiativrecht](#43)  
[§ 44    Arbeitsschutz und Unfallverhütung](#44)  
[§ 45    Beteiligungsgrundsätze](#45)  
[§ 46    Verfahren bei der Mitbestimmung](#46)  
[§ 47    Verfahren bei Nichtzustimmung](#47)  
[§ 48    Verfahren vor der Einigungsstelle](#48)  
[§ 49    Beschlussfassung der Einigungsstelle](#49)  
[§ 50    Aufhebung bindungsgeeigneter Beschlüsse der Einigungsstelle](#50)  
[§ 51    _(weggefallen)_](#51)  
[§ 52    Vorläufige Regelungen](#52)  
[§ 53    Dienstvereinbarungen](#53)  
[§ 54    Gesamtrichterräte, Hauptrichter- und Hauptstaatsanwaltsrat](#45)  
[§ 55    Beteiligung an gemeinsamen Angelegenheiten](#55)  
[§ 56    Gemeinsame Personalversammlung](#56)

Abschnitt 3  
Präsidialräte
---------------------------

[§ 57    Bildung von Präsidialräten](#57)  
[§ 58    Ausschluss von gewählten Mitgliedern](#58)  
[§ 59    Stellvertretung](#59)  
[§ 60    Aufgaben](#60)  
[§ 61    Stellungnahme des Präsidialrats](#61)  
[§ 62    Neuwahl](#62)

Abschnitt 4  
Kontrollgremium IT
--------------------------------

[§ 62a   Kontrollgremium IT](#62a)  
[§ 62b   Aufgaben](#62b)

Abschnitt 5  
Vertretung ehrenamtlicher Richterinnen und Richter
----------------------------------------------------------------

[§ 63    Bildung der Vertretung ehrenamtlicher Richterinnen und Richter](#63)

Kapitel 4  
Richterdienstgerichte
---------------------------------

Abschnitt 1  
Errichtung und Zuständigkeit
------------------------------------------

[§ 64    Errichtung und Zuständigkeit](#64)  
[§ 65    Zuständigkeit des Dienstgerichts](#65)  
[§ 66    Zuständigkeit des Dienstgerichtshofs](#66)

Abschnitt 2  
Besetzung
-----------------------

[§ 67    Mitglieder der Richterdienstgerichte](#67)  
[§ 68    Bestimmung der Mitglieder der Richterdienstgerichte](#68)  
[§ 69    Besetzung des Dienstgerichts](#69)  
[§ 70    Besetzung des Dienstgerichtshofs](#70)  
[§ 71    Verbot der Amtsausübung](#71)  
[§ 72    Erlöschen des Amtes, Ruhen](#72)

Abschnitt 3  
Disziplinarverfahren
----------------------------------

[§ 73    Geltung des Disziplinargesetzes](#73)  
[§ 74    Disziplinarmaßnahmen](#74)  
[§ 75    Entscheidungen des Dienstgerichts anstelle der obersten Dienstbehörde](#75)  
[§ 76    Verfahren](#76)  
[§ 77    Zulässigkeit der Revision](#77)  
[§ 78    Bekleidung mehrerer Ämter](#78)  
[§ 79    Richterinnen und Richter kraft Auftrags](#79)

Abschnitt 4  
Versetzungs- und Prüfungsverfahren
------------------------------------------------

[§ 80    Allgemeine Verfahrensvorschriften](#80)  
[§ 81    Versetzungsverfahren](#81)  
[§ 82    Einleitung des Prüfungsverfahrens](#82)  
[§ 83    Versetzung in den Ruhestand wegen Dienstunfähigkeit mit Zustimmung](#83)  
[§ 84    Versetzung in den Ruhestand wegen Dienstunfähigkeit ohne Zustimmung](#84)  
[§ 85    Urteilsformel](#85)  
[§ 86    Aussetzung von Verfahren](#86)  
[§ 87    Kostenentscheidung in besonderen Fällen](#87)

Kapitel 5  
Wahlen
------------------

[§ 88    Grundsatz](#88)  
[§ 89    Wahlrecht, Wählbarkeit](#89)  
[§ 90    Wahlordnung](#90)  
[§ 91    Anfechtung der Wahl](#91)

Kapitel 6  
Staatsanwältinnen und Staatsanwälte
-----------------------------------------------

[§ 92    Aufgaben und Bildung der Staatsanwaltsräte](#92)  
[§ 93    Beteiligungsverfahren](#93)  
[§ 94    Nichtständiges Mitglied des Richterwahlausschusses](#94)  
[§ 95    Zuständigkeit der Richterdienstgerichte](#95)  
[§ 96    Bestellung der nichtständigen beisitzenden Mitglieder der Richterdienstgerichte](#96)  
[§ 97    Disziplinarmaßnahmen](#97)  
[§ 98    Verfahren](#98)

### Kapitel 7  
Gemeinsame Gerichte

[§ 99    Errichtung gemeinsamer Fachobergerichte der Länder Berlin und Brandenburg](#99)

Kapitel 8  
Übergangs- und Schlussvorschriften
----------------------------------------------

[§ 100  Laufende Verfahren vor dem Dienstgericht und laufende Disziplinarverfahren](#100)  
[§ 101  Evaluation](#101)  
[§ 102  Ausführung des Richterwahlgesetzes](#102)

Kapitel 1   
Allgemeine Vorschriften
------------------------------------

### § 1  
Geltungsbereich

(1) Dieses Gesetz gilt für die Berufsrichterinnen und -richter im Dienst des Landes.
Es gilt für ehrenamtliche Richterinnen und Richter sowie Staatsanwältinnen und Staatsanwälte, soweit dies besonders bestimmt ist.

(2) Die Rechtsstellung der Mitglieder des Verfassungsgerichts des Landes Brandenburg bleibt unberührt.

### § 2  
Richtereid

(1) Die Richterin oder der Richter hat in öffentlicher Sitzung eines Gerichts folgenden Eid zu leisten:

„Ich schwöre, das Richteramt getreu dem Grundgesetz für die Bundesrepublik Deutschland, der Verfassung des Landes Brandenburg und getreu dem Gesetz auszuüben, nach bestem Wissen und Gewissen ohne Ansehen der Person zu urteilen und nur der Wahrheit und Gerechtigkeit zu dienen.“

(2) Die ehrenamtliche Richterin oder der ehrenamtliche Richter leistet den Eid oder das Gelöbnis (§ 45 Absatz 3 bis 5 und 7 des Deutschen Richtergesetzes) dahin, ihre oder seine Pflichten getreu dem Grundgesetz für die Bundesrepublik Deutschland, getreu der Verfassung des Landes Brandenburg und getreu dem Gesetz zu erfüllen, nach bestem Wissen und Gewissen ohne Ansehen der Person zu urteilen und nur der Wahrheit und Gerechtigkeit zu dienen.

(3) Die ehrenamtliche Richterin oder der ehrenamtliche Richter in der Finanzgerichtsbarkeit leistet den Eid oder das Gelöbnis dahin, ihre oder seine Pflichten getreu dem Grundgesetz für die Bundesrepublik Deutschland, getreu der Verfassung des Landes Brandenburg und getreu dem Gesetz zu erfüllen, das Steuergeheimnis zu wahren, nach bestem Wissen und Gewissen ohne Ansehen der Person zu urteilen und nur der Wahrheit und Gerechtigkeit zu dienen.

(4) Der Eid kann auch mit einer religiösen Beteuerung geleistet werden.

### § 3  
Altersgrenzen

(1) Die Richterin oder der Richter auf Lebenszeit tritt mit dem Ende des Monats in den Ruhestand, in dem sie oder er das 67.
Lebensjahr vollendet (Regelaltersgrenze).
Abweichend von Satz 1 ist für die Richterinnen und Richter auf Lebenszeit, die vor dem 1.
Januar 1949 geboren sind, das vollendete 65. Lebensjahr die Regelaltersgrenze.
Für die Richterinnen und Richter auf Lebenszeit, die nach dem 31. Dezember 1948 und vor dem 1. Januar 1964 geboren sind, wird die Regelaltersgrenze wie folgt angehoben:

Geburtsjahr

Anhebung

um Monate

Altersgrenze

Jahr

Monate

1949

3

65

3

1950

4

65

4

1951

5

65

5

1952

6

65

6

1953

7

65

7

1954

8

65

8

1955

9

65

9

1956

10

65

10

1957

11

65

11

1958

12

66

0

1959

14

66

2

1960

16

66

4

1961

18

66

6

1962

20

66

8

1963

22

66

10.

Abweichend von Satz 3 ist für Richterinnen und Richter auf Lebenszeit, denen

1.  Teilzeit gemäß § 6 Absatz 1 Nummer 2 des Brandenburgischen Richtergesetzes in der Fassung der Bekanntmachung vom 22.
    November 1996 (GVBl.
    I S. 322), das zuletzt durch Artikel 13 des Gesetzes vom 3.
    April 2009 (GVBl.
    I S. 26, 59) geändert worden ist, oder
2.  Altersteilzeit gemäß § 6c des in Nummer 1 genannten Gesetzes

bewilligt worden ist, das vollendete 65.
Lebensjahr die Regelaltersgrenze.

(2) Abweichend von Absatz 1 ist auf Antrag einer Richterin oder eines Richters auf Lebenszeit der Eintritt in den Ruhestand um einen oder mehrere Monate, höchstens jedoch bis zur Vollendung des 68.
Lebensjahres, hinauszuschieben, wenn dies im dienstlichen Interesse liegt und der Antrag spätestens ein Jahr vor dem Erreichen der Altersgrenze nach Absatz 1 gestellt wird.
Über den Antrag entscheidet das für Justiz zuständige Mitglied der Landesregierung.

(3) Eine Richterin oder ein Richter ist auf Antrag in den Ruhestand zu versetzen (Antragsaltersgrenze)

1.  frühestens mit Vollendung des 63.
    Lebensjahres oder
2.  als schwerbehinderter Mensch im Sinne von § 2 Absatz 2 des Neunten Buches Sozialgesetzbuch frühestens mit Vollendung des 60.
    Lebensjahres.

### § 4  
Teilzeitbeschäftigung und Beurlaubung aus familiären Gründen

(1) Einer Richterin oder einem Richter ist auf Antrag

1.  Teilzeitbeschäftigung mit mindestens 35 Prozent des regelmäßigen Dienstes,
    
2.  Urlaub ohne Dienstbezüge bis zur Dauer von drei Jahren mit der Möglichkeit der Verlängerung zu bewilligen,  
    wenn sie oder er
    
    1.  mindestens ein Kind unter 18 Jahren oder
        
    2.  eine sonstige Angehörige oder einen sonstigen Angehörigen, die oder der nach ärztlicher Bescheinigung oder einer Bescheinigung der Pflegekasse oder des Medizinischen Dienstes der Krankenversicherung oder einer entsprechenden Bescheinigung einer privaten Pflegeversicherung pflegebedürftig ist,
        

tatsächlich betreut oder pflegt.

(2) Die Dauer des Urlaubs im Sinne des Absatzes 1 darf zwölf Jahre nicht überschreiten.
Der Antrag auf Verlängerung einer Teilzeitbeschäftigung oder eines Urlaubs ist spätestens sechs Monate vor Ablauf der genehmigten Freistellung zu stellen.

(3) In entsprechender Anwendung von § 3 Absatz 6 und § 7 Absatz 3 und 4 des Pflegezeitgesetzes vom 28.
Mai 2008 (BGBl.
I S.
874, 896), das zuletzt durch Artikel 7 des Gesetzes vom 21.
Dezember 2015 (BGBl.
I S.
2424, 2463) geändert worden ist, in der jeweils geltenden Fassung ist einer Richterin oder einem Richter Teilzeitbeschäftigung oder Urlaub unter Wegfall der Besoldung bis zur Dauer von drei Monaten zu bewilligen.

(4) Während einer Freistellung vom Dienst nach den Absätzen 1 und 3 dürfen nur solche Nebentätigkeiten ausgeübt werden, die dem Zweck der Freistellung nicht zuwiderlaufen.

(5) Über eine Änderung des Umfangs der Teilzeitbeschäftigung oder den Übergang zur Vollzeitbeschäftigung während der Dauer des Bewilligungszeitraums entscheidet auf Antrag die zuständige Dienstbehörde.
Sie soll in besonderen Härtefällen eine Änderung des Umfangs der Teilzeitbeschäftigung oder den Übergang zur Vollzeitbeschäftigung zulassen, wenn der Richterin oder dem Richter die Teilzeitbeschäftigung im bisherigen Umfang nicht zugemutet werden kann.
Die zuständige Dienstbehörde kann in besonderen Härtefällen eine Rückkehr aus dem Urlaub zulassen, wenn der Richterin oder dem Richter eine Fortsetzung des Urlaubs nicht zugemutet werden kann.
Absatz 2 Satz 2 gilt entsprechend.

(6) Während der Dauer des Urlaubs nach Absatz 1 Nummer 2 in Verbindung mit Absatz 2 Satz 1 besteht ein Anspruch auf Leistungen der Krankheitsfürsorge in entsprechender Anwendung der Beihilferegelungen für Richterinnen und Richter mit Dienstbezügen.
Dies gilt nicht, wenn die Richterin oder der Richter als Angehörige oder Angehöriger einer beihilfeberechtigten Person berücksichtigt wird oder Anspruch auf Familienhilfe nach § 10 des Fünften Buches Sozialgesetzbuch hat.

(7) Einer Richterin oder einem Richter ist auf Antrag Teilzeitbeschäftigung als Familienpflegezeit in entsprechender Anwendung des § 80a des Landesbeamtengesetzes zu gewähren.

### § 5  
Teilzeitbeschäftigung

(1) Einer Richterin oder einem Richter ist auf Antrag Teilzeitbeschäftigung bis zur Hälfte des regelmäßigen Dienstes und bis zur jeweils beantragten Dauer zu bewilligen.
Teilzeitbeschäftigung kann auch so geregelt werden, dass nach einer im Voraus festgelegten Abfolge Phasen einer vollen oder erhöhten dienstlichen Inanspruchnahme mit Phasen einer vollständigen oder teilweisen Freistellung vom regelmäßigen Dienst wechseln.

(2) Einem Antrag nach Absatz 1 darf nur entsprochen werden, wenn

1.  das Aufgabengebiet des richterlichen Amtes Teilzeitbeschäftigung zulässt,
    
2.  dienstliche Gründe nicht entgegenstehen,
    
3.  die Richterin oder der Richter sich verpflichtet, während der Dauer des Bewilligungszeitraums außerhalb des Richterverhältnisses berufliche Verpflichtungen nur in dem Umfang einzugehen, in dem nach § 10 Absatz 1 dieses Gesetzes in Verbindung mit §§ 85 und 86 des Landesbeamtengesetzes Richterinnen und Richtern die Ausübung von Nebentätigkeiten gestattet ist.
    

Ausnahmen von der Verpflichtung nach Satz 1 Nummer 3 sind nur zulässig, soweit dies mit dem Richterverhältnis vereinbar ist.
§ 10 Absatz 1 dieses Gesetzes in Verbindung mit § 85 Absatz 3 Satz 1 des Landesbeamtengesetzes gilt mit der Maßgabe, dass von der regelmäßigen wöchentlichen Arbeitszeit ohne Rücksicht auf die Bewilligung von Teilzeitbeschäftigung auszugehen ist.
Wird die Verpflichtung nach Satz 1 Nummer 3 schuldhaft verletzt, ist die Bewilligung zu widerrufen.

(3) Über eine Änderung des Umfangs der Teilzeitbeschäftigung oder den Übergang zur Vollzeitbeschäftigung während der Dauer des Bewilligungszeitraums entscheidet auf Antrag die zuständige Dienstbehörde.
Sie soll in besonderen Härtefällen eine Änderung des Umfangs der Teilzeitbeschäftigung oder den Übergang zur Vollzeitbeschäftigung zulassen, wenn der Richterin oder dem Richter die Teilzeitbeschäftigung im bisherigen Umfang nicht mehr zugemutet werden kann.

### § 6   
Freistellungen und berufliches Fortkommen

Teilzeitbeschäftigung und Beurlaubung nach § 4 oder § 5, Schwangerschaft, Mutterschutz und Elternzeit dürfen das berufliche Fortkommen nicht beeinträchtigen; eine unterschiedliche Behandlung von Richterinnen und Richtern mit Teilzeitbeschäftigung gegenüber Richterinnen und Richtern mit Vollzeitbeschäftigung ist nur zulässig, wenn zwingende sachliche Gründe sie rechtfertigen.

### § 7  
Beteiligung der Spitzenorganisationen

Bei der Vorbereitung allgemeiner die Richter- oder Staatsanwaltschaft betreffender Regelungen sind die Spitzenorganisationen der Berufsverbände der Richter- oder Staatsanwaltschaft (Spitzenorganisationen) zu beteiligen.

### § 8  
Verschwiegenheitspflicht

(1) Personen, die Aufgaben oder Befugnisse im Richterwahlausschuss, in Richtervertretungen oder als Wahlvorstand wahrnehmen oder wahrgenommen haben oder die zu den Sitzungen hinzugezogen worden sind, haben über die ihnen dabei bekannt gewordenen Angelegenheiten und Tatsachen Stillschweigen zu bewahren.
Dies gilt nicht für Angelegenheiten und Tatsachen, die offenkundig sind oder ihrer Bedeutung nach keiner Geheimhaltung bedürfen.

(2) Die Schweigepflicht besteht nicht für

1.  die Mitglieder eines Gremiums untereinander,
    
2.  die Mitglieder des Richterrats gegenüber der Stufenvertretung, dem Präsidialrat sowie gegenüber der Dienststelle, soweit diese im Rahmen ihrer Zuständigkeit beteiligt wird.
    

Gleiches gilt gegenüber den zuständigen Schwerbehindertenvertretungen.

### § 9  
Dienstliche Beurteilungen, Stellenausschreibungen

(1) Richterinnen und Richter sowie Staatsanwältinnen und Staatsanwälte sind regelmäßig zu beurteilen (Regelbeurteilung).
Sie sind zudem zu beurteilen, wenn es die dienstlichen oder persönlichen Verhältnisse erfordern (Anlassbeurteilung).
Die oberste Dienstbehörde bestimmt die Fälle für eine Anlassbeurteilung.
Sie kann bestimmen, welche Richterinnen und Richter sowie welche Staatsanwältinnen und Staatsanwälte nicht mehr regelmäßig beurteilt werden.

(2) Beurteilt werden Eignung, Befähigung und fachliche Leistung.
Bei der Beurteilung richterlicher Amtsgeschäfte sind die sich aus § 26 Absatz 1 und 2 des Deutschen Richtergesetzes ergebenden Beschränkungen zu beachten.
Auf Verlangen der Richterin oder des Richters ist der Richterrat, auf Verlangen der Staatsanwältin oder des Staatsanwalts ist der Staatsanwaltsrat an der Besprechung der Beurteilung zu beteiligen.
Die Schwerbehindertenvertretung ist an der Besprechung der Beurteilung zu beteiligen, wenn eine schwerbehinderte Person dies verlangt.
Über das Recht, eine Beteiligung nach den Sätzen 3 und 4 zu verlangen, ist die betroffene Person vor der Besprechung zu unterrichten.

(3) Die oberste Dienstbehörde kann in Beurteilungsrichtlinien nähere Bestimmungen treffen.

(4) Freie Planstellen für Richterinnen und Richter, Staatsanwältinnen und Staatsanwälte sind auszuschreiben.

### § 9a  
Richterliche Gleichstellungsbeauftragte

(1) Bei den oberen Landesgerichten werden für den jeweiligen Gerichtszweig eine richterliche Gleichstellungsbeauftragte und eine Stellvertreterin bestellt.
Die Bestellung erfolgt durch die Präsidentin oder den Präsidenten des oberen Landesgerichts auf mehrheitlichen Vorschlag der Richterinnen des jeweiligen Gerichtszweigs.
Es sollen jeweils mindestens zwei Kandidatinnen vorgeschlagen werden.
§ 20 Absatz 4 bis 7 sowie § 21 des Landesgleichstellungsgesetzes gelten entsprechend.

(2) Der richterlichen Gleichstellungsbeauftragten ist anstelle der örtlichen Gleichstellungsbeauftragten nach den für diese geltenden Vorschriften Gelegenheit zur aktiven Teilnahme bei allen personellen Angelegenheiten zu geben, die sich auf die Gleichstellung von Richterinnen und Richtern auswirken und bei denen der Gesamtrichterrat oder der Präsidialrat zu beteiligen ist.
Sie hat das Recht, in den Sitzungen des Richterwahlausschusses eine Stellungnahme abzugeben.

### § 10  
Geltung des Beamtenrechts und Freistellung  
von Verfassungsrichterinnen und Verfassungsrichtern

(1) Soweit das Deutsche Richtergesetz und dieses Gesetz nichts anderes bestimmen, gelten für die Rechtsverhältnisse der Richterinnen und Richter die beamtenrechtlichen Vorschriften des Landes entsprechend.

(2) Ernennungen von Richterinnen und Richtern, die vom Richterwahlausschuss beschlossen wurden, sind in der Zeit zwischen dem Wahltag zum Landtag und dem Tag der Ernennung der Mitglieder der Landesregierung zulässig.

(3) Ist eine Richterin Präsidentin oder ein Richter Präsident des Verfassungsgerichts des Landes Brandenburg, ist sie oder er für die Wahrnehmung der mit diesem Amt verbundenen Aufgaben in dem von ihr oder ihm für erforderlich gehaltenen Umfang, höchstens bis zu 30 Prozent, von den Aufgaben im richterlichen Hauptamt unter Fortzahlung der Besoldung freigestellt.
Nehmen Richterinnen oder Richter, die Richterinnen oder Richter des Verfassungsgerichts sind, Verwaltungsaufgaben für das Verfassungsgericht wahr, sind sie hierfür in dem von dem Verfassungsgericht des Landes Brandenburg für erforderlich gehaltenen Umfang von den Aufgaben im richterlichen Hauptamt unter Fortzahlung der Besoldung freigestellt.
Die Gesamtheit der Freistellungen nach Satz 2 darf 30 Prozent einer Arbeitskraft nicht überschreiten.

Kapitel 2  
Richterwahlausschuss
--------------------------------

### § 11  
Zuständigkeit des Richterwahlausschusses

(1) Über die Einstellung, die erstmalige Berufung in ein Richterverhältnis auf Lebenszeit, die Versetzung und über die Ernennung, durch die ein Richteramt mit höherem Endgrundgehalt als dem eines Eingangsamtes verliehen wird, entscheidet das zuständige Mitglied der Landesregierung gemeinsam mit dem Richterwahlausschuss.

(2) Der Präsident oder die Präsidentin eines oberen Landesgerichts wird auf Vorschlag der Landesregierung vom Richterwahlausschuss gewählt.

(3) Das zuständige Mitglied der Landesregierung unterrichtet den Richterwahlausschuss regelmäßig über die allgemeine Bewerbungs- und Stellensituation im Land unter Berücksichtigung von Stand und Entwicklung des Anteils von Frauen in den Besoldungs- oder Funktionsgruppen.

### § 12  
Wahl des Richterwahlausschusses

(1) Der Landtag wählt zu ständigen Mitgliedern des Richterwahlausschusses

1.  acht Abgeordnete aufgrund von Vorschlägen aus der Mitte des Parlaments,
    
2.  zwei Personen aus der Richterschaft aus der Vorschlagsliste nach § 15 Absatz 1 Satz 1 erster Halbsatz,
    
3.  eine Person aus der Rechtsanwaltschaft aus der Vorschlagsliste nach § 15 Absatz 2 Satz 1.
    

Für jedes ständige Mitglied ist eine Stellvertretung zu wählen.
Darüber hinaus wählt er zu nichtständigen Mitgliedern des Richterwahlausschusses eine Person aus der Staatsanwaltschaft und ihre Stellvertretung aus der Vorschlagsliste nach § 15 Absatz 3 Satz 1 und je eine Richterin oder einen Richter der ordentlichen Gerichtsbarkeit, der Verwaltungs-, der Finanz-, der Arbeits- und der Sozialgerichtsbarkeit sowie deren Stellvertretung aus den Vorschlagslisten nach § 15 Absatz 1 Satz 1 zweiter Halbsatz.
Im Richterwahlausschuss müssen alle Fraktionen vertreten sein.

(2) Die Wahl jedes Mitglieds bedarf der Mehrheit der anwesenden Abgeordneten.

### § 13  
Zusammensetzung

(1) Über Einstellungen entscheiden die ständigen Mitglieder des Richterwahlausschusses unter Mitwirkung einer Staatsanwältin oder eines Staatsanwalts als nichtständiges Mitglied.
Ist über die Einstellung einer Richterin oder eines Richters für die ordentliche Gerichtsbarkeit, die Verwaltungs-, Finanz-, Arbeits- oder Sozialgerichtsbarkeit zu entscheiden, so wirkt anstelle der Staatsanwältin oder des Staatsanwalts das nichtständige Mitglied dieser Gerichtsbarkeit mit.

(2) Bei sonstigen Entscheidungen im Sinne von § 11 Absatz 1 über Richterinnen und Richter der ordentlichen Gerichtsbarkeit, der Verwaltungs-, der Finanz-, der Arbeits- oder der Sozialgerichtsbarkeit wirkt an Stelle der Staatsanwältin oder des Staatsanwalts das nichtständige richterliche Mitglied aus der jeweiligen Gerichtsbarkeit mit.

(3) Den Vorsitz im Richterwahlausschuss führt das für Justiz zuständige Mitglied der Landesregierung.
Es hat kein Stimmrecht.

(4) Die Mitglieder des Richterwahlausschusses sind zur unparteiischen und gewissenhaften Ausübung ihrer Aufgaben verpflichtet.
Sie sind an Weisungen nicht gebunden.

### § 14  
Neuwahl

Nach dem Zusammentritt eines neugewählten Landtages ist innerhalb von zwei Monaten ein neuer Richterwahlausschuss zu wählen.
Mit der Neuwahl endet die Amtszeit des bisherigen Richterwahlausschusses.

### § 15  
Vorschlagslisten

(1) Die in die Vorschlagslisten nach § 12 Absatz 1 Satz 1 Nummer 2 aufzunehmenden Richterinnen und Richter werden von den auf Lebenszeit ernannten Richterinnen und Richtern, im Fall des § 12 Absatz 1 Satz 3 zweite Alternative von den auf Lebenszeit ernannten Richterinnen und Richtern des jeweiligen Gerichtszweigs gewählt.
Für jedes zu wählende Mitglied müssen mindestens vier Personen vorgeschlagen werden.

(2) Die in die Vorschlagsliste nach § 12 Absatz 1 Satz 1 Nummer 3 aufzunehmenden Rechtsanwältinnen und Rechtsanwälte werden nach näherer Regelung der Rechtsanwaltskammer in einer Kammerversammlung von den Rechtsanwältinnen und Rechtsanwälten gewählt, die im Land zugelassen sind.
Absatz 1 Satz 2 gilt entsprechend.

(3) Die in die Vorschlagsliste nach § 12 Absatz 1 Satz 3 erste Alternative aufzunehmenden Staatsanwältinnen und Staatsanwälte werden von den auf Lebenszeit ernannten Staatsanwältinnen und Staatsanwälten gewählt.
Absatz 1 Satz 2 gilt entsprechend.

### § 16  
Erlöschen der Mitgliedschaft

(1) Die Mitgliedschaft im Richterwahlausschuss erlischt mit der Wahl eines neuen Richterwahlausschusses, vorbehaltlich des Satzes 2 mit Verlust der Mitgliedschaft im Landtag oder wenn

1.  ein Mitglied schriftlich auf seine Mitgliedschaft gegenüber dem für Justiz zuständigen Mitglied der Landesregierung verzichtet,
    
2.  ein Mitglied durch rechtskräftiges Urteil wegen einer vorsätzlichen Straftat zu einer Freiheitsstrafe von mindestens drei Monaten Dauer verurteilt wird,
    
3.  das Richterverhältnis eines richterlichen Mitglieds im Geltungsbereich dieses Gesetzes endet,
    
4.  das richterliche Mitglied ein Richteramt in einem Gerichtszweig übernimmt, für den es nicht gewählt worden ist,
    
5.  ein als Rechtsanwältin oder Rechtsanwalt gewähltes Mitglied im Geltungsbereich dieses Gesetzes keine Kanzlei mehr unterhält oder die Zulassung des Mitglieds zur Rechtsanwaltschaft erloschen ist,
    
6.  das Beamtenverhältnis einer Staatsanwältin oder eines Staatsanwalts im Geltungsbereich dieses Gesetzes endet.
    

Die Mitgliedschaft der als Abgeordnete des Landtages gewählten Mitglieder erlischt nicht bereits mit dem Verlust ihrer Mitgliedschaft im Landtag infolge der Beendigung der Wahlperiode.

(2) In den Fällen des Absatzes 1 nimmt der Landtag unverzüglich eine Ersatzwahl vor.
Die Ersatzwahl erfolgt für Mitglieder nach § 12 Absatz 1 Satz 1 Nummer 1 aufgrund neuer Vorschläge aus der Mitte des Parlaments, für die übrigen Mitglieder aus den für die letzte Wahl eingereichten Vorschlagslisten.
Ist die bestehende Vorschlagsliste erschöpft, so sind unverzüglich neue Vorschläge entsprechend § 15 Absatz 1 Satz 2 vorzulegen.

### § 17  
Ruhen der Mitgliedschaft

(1) Die Mitgliedschaft im Richterwahlausschuss ruht, solange ein richterlich eröffnetes Strafverfahren wegen einer vorsätzlichen Straftat, die nicht nur auf Antrag verfolgbar ist, schwebt.

(2) Die Mitgliedschaft nach § 12 Absatz 1 Satz 1 Nummer 2 und 3 und Satz 3 ruht, solange das Mitglied sein Amt oder seinen Beruf nicht ausübt.
Die Mitgliedschaft eines richterlichen oder staatsanwaltschaftlichen Mitglieds ruht darüber hinaus, solange es vorläufig seines Dienstes enthoben oder ihm die Führung seiner Amtsgeschäfte untersagt ist.
Die Mitgliedschaft eines der Rechtsanwaltskammer angehörenden Mitglieds ruht ferner, solange ein Berufs- oder Vertretungsverbot verhängt worden ist.

### § 18  
Feststellung des Erlöschens oder Ruhens

Das Erlöschen oder Ruhen der Mitgliedschaft im Richterwahlausschuss stellt das für Justiz zuständige Mitglied der Landesregierung fest.

### § 19   
Ausschließungsgründe

(1) Ein Mitglied des Richterwahlausschusses ist von der Mitwirkung ausgeschlossen, wenn die Voraussetzungen des § 41 Nummer 1 bis 3 der Zivilprozessordnung vorliegen.

(2) Ein Mitglied kann von dem für Justiz zuständigen Mitglied der Landesregierung, einem anderen Mitglied des Richterwahlausschusses oder von einer Bewerberin oder einem Bewerber wegen Besorgnis der Befangenheit abgelehnt werden.
Das Mitglied hat einen Ablehnungsgrund auch selbst anzuzeigen.

(3) Über den Ausschluss eines Mitglieds nach Absatz 1 oder die Ablehnung nach Absatz 2 entscheiden die übrigen Mitglieder des Richterwahlausschusses ohne die Vertreterin oder den Vertreter des betroffenen Mitglieds.

### § 20  
Einberufung

(1) Das für Justiz zuständige Mitglied der Landesregierung beruft den Richterwahlausschuss ein und leitet die Sitzungen.

(2) Das für Justiz zuständige Mitglied der Landesregierung legt dem Richterwahlausschuss mit der Einladung eine Liste mit den Namen der Bewerberinnen und Bewerber sowie die Stellungnahme des Präsidialrats vor.
Ferner stehen ihm die für die Entscheidung erheblichen Personalunterlagen sämtlicher Bewerberinnen und Bewerber zur Einsichtnahme zur Verfügung.

### § 21  
Sitzung

(1) Die Sitzungen des Richterwahlausschusses sind nicht öffentlich.
Die nichtständigen Mitglieder dürfen auch dann an den Sitzungen teilnehmen, wenn sie an der Beschlussfassung nicht mitwirken.
Die vom Ministerium der Justiz beauftragten Bediensteten dürfen an der Sitzung teilnehmen.

(2) Über den Verlauf der Sitzung und das Ergebnis der Abstimmung wird eine Niederschrift angefertigt.

### § 22  
Beschlussfassung, Stellvertretung

(1) Der Richterwahlausschuss wählt in geheimer Abstimmung die Bewerberin oder den Bewerber, die oder der für das Richteramt persönlich und fachlich am besten geeignet ist.
Gewählt ist, wer zwei Drittel der abgegebenen Stimmen erhält.
Erhält ein Personalvorschlag diese Mehrheit nicht, so kann das zuständige Mitglied der Landesregierung diesen Personalvorschlag in einer weiteren Sitzung des Richterwahlausschusses zur Abstimmung stellen.

(2) Sonstige Beschlüsse fasst der Richterwahlausschuss mit der Mehrheit der offen abgegebenen Stimmen.

(3) Der Richterwahlausschuss ist beschlussfähig, wenn von seinen Mitgliedern oder deren Vertreterinnen und Vertretern mindestens die Hälfte anwesend ist.

(4) Ist ein Mitglied des Richterwahlausschusses an der Ausübung des Amtes verhindert oder von der Mitwirkung ausgeschlossen oder ruht seine Mitgliedschaft, so tritt die Vertreterin oder der Vertreter für die Dauer der Verhinderung, des Ausschlusses oder des Ruhens an seine Stelle.
Dasselbe gilt im Fall des Erlöschens der Mitgliedschaft bis zur Ersatzwahl.

(5) Die Entscheidung des Richterwahlausschusses bedarf keiner Begründung.
§ 22a Absatz 2 Satz 4 bleibt unberührt.

### § 22a  
Wahl zur Besetzung von Spitzenpositionen

(1) Ist außer in den Fällen des § 11 Absatz 2 ein Richteramt zu besetzen, mit dem Dienstaufsichtsbefugnisse über Richterinnen und Richter verbunden sind, legt das für Justiz zuständige Mitglied der Landesregierung dem Richterwahlausschuss anstelle eines Personalvorschlags einen Bericht über die hierfür geeigneten Bewerberinnen und Bewerber sowie die eine Auswahlentscheidung tragenden Gesichtspunkte vor.
Das Ziel der Gleichstellung von Frauen ist zu berücksichtigen.
§ 22 Absatz 1 Satz 3 ist nicht anzuwenden.

(2) Der Richterwahlausschuss bestimmt zur Vorbereitung seiner Entscheidung ein Mitglied oder mehrere Mitglieder für die Berichterstattung.
Er berücksichtigt die vorgelegten Personalunterlagen, den Bericht nach Absatz 1 Satz 1 sowie die Stellungnahme des Präsidialrats nach § 61 Absatz 3 Satz 2.
Das Wahlverfahren ist zu dokumentieren.
Der Richterwahlausschuss kann seiner Entscheidung abweichend von § 22 Absatz 5 eine Begründung beifügen.

(3) Die Wahlentscheidung bedarf der Zustimmung des für Justiz zuständigen Mitglieds der Landesregierung.
Die Zustimmung ist zu versagen, wenn die Wahl auf einem wesentlichen Verfahrensfehler beruht oder unter Berücksichtigung des Grundsatzes der Bestenauswahl nicht mehr nachvollziehbar erscheint.
Die Versagung der Zustimmung ist zu begründen.
Versagt das für Justiz zuständige Mitglied der Landesregierung seine Zustimmung, weil es den Grundsatz der Bestenauswahl als verletzt ansieht, stimmt der Richterwahlausschuss in seiner nächsten Sitzung erneut über die Besetzung ab; bei der Wahlentscheidung bleibt die durch die Versagung der Zustimmung ausgeschlossene Bewerbung unberücksichtigt.

### § 23  
Übernahme und Entlassung von Richterinnen und Richtern auf Probe und Richterinnen und Richtern kraft Auftrags

(1) Spätestens dreieinhalb Jahre nach der Ernennung zur Richterin oder zum Richter auf Probe und spätestens zwei Jahre nach der Ernennung zur Richterin oder zum Richter kraft Auftrags legt das zuständige Mitglied der Landesregierung die Personalunterlagen der Richterin oder des Richters dem Richterwahlausschuss zur Entscheidung vor, ob auch dieser der Übernahme in das Richterverhältnis auf Lebenszeit zustimmt.

(2) Lehnt der Richterwahlausschuss die Übernahme in das Richterverhältnis auf Lebenszeit nach Absatz 1 ab, so kann die Richterin oder der Richter entlassen werden (§ 22 Absatz 2 Nummer 2, § 23 des Deutschen Richtergesetzes).

### § 24  
Geschäftsordnung

Der Richterwahlausschuss gibt sich eine Geschäftsordnung, die der Zustimmung des für Justiz zuständigen Mitglieds der Landesregierung bedarf.
Sie enthält insbesondere Festlegungen über eine Berichterstattung im Richterwahlausschuss und kann bestimmen, inwieweit Bewerberinnen und Bewerber sowie andere Personen anzuhören sind.

### § 25   
Entschädigung

(1) Die Mitglieder des Richterwahlausschusses werden für ihre Tätigkeit nach den beamtenrechtlichen Bestimmungen des Reisekostenrechts entschädigt.

(2) Die Sachkosten des Richterwahlausschusses trägt das Land.

Kapitel 3   
Richtervertretungen, Kontrollgremium IT und Vertretungen  
ehrenamtlicher Richterinnen und Richter
---------------------------------------------------------------------------------------------------------------

Abschnitt 1  
Gemeinsame Vorschriften für die Richtervertretungen
-----------------------------------------------------------------

### § 26  
Richterrat und Präsidialrat

Als Richtervertretungen werden gebildet

1.  Richterräte als Personal- und Stufenvertretungen der Richterinnen und Richter in den Gerichten,
    
2.  Präsidialräte
    

für die nach diesem Gesetz zugewiesenen Angelegenheiten.

### § 27  
Rechtsstellung der Mitglieder, Geschäftsordnung

(1) Die Mitgliedschaft in der Richtervertretung ist ein Ehrenamt.

(2) Die Mitglieder der Richtervertretungen dürfen in der Ausübung ihrer Befugnisse nicht behindert und wegen ihrer Tätigkeit nicht benachteiligt oder begünstigt werden.
Sie sind von ihrer dienstlichen Tätigkeit freizustellen, soweit dies zur ordnungsgemäßen Durchführung der Aufgaben in der Richtervertretung erforderlich ist.

(3) Die Richtervertretungen regeln ihre Beschlussfassung und Geschäftsführung in einer Geschäftsordnung.
§ 109 des Bundespersonalvertretungsgesetzes gilt entsprechend.

### § 28  
Amtszeit

(1) Die regelmäßige Amtszeit der Richtervertretung beträgt vier Jahre.
Sie beginnt mit dem Tag der Konstituierung der neu gewählten oder gebildeten Richtervertretung oder, wenn zu diesem Zeitpunkt noch eine Richtervertretung besteht, mit Ablauf von deren Amtszeit.
Sie endet spätestens am 15. Dezember des Jahres, in dem gemäß Absatz 2 die regelmäßigen Wahlen stattfinden.

(2) Die regelmäßigen Wahlen der Richtervertretungen finden alle vier Jahre in der Zeit vom 1. Oktober bis 15.
Dezember statt.
Die Richtervertretungen sollen gleichzeitig gewählt werden.

(3) Die Richtervertretung führt die Geschäfte weiter, bis sich eine neue Vertretung konstituiert hat, längstens jedoch drei Monate.

### § 29  
Ruhen der Mitgliedschaft

Die Mitgliedschaft in der Richtervertretung ruht, solange die Führung der Amtsgeschäfte vorläufig untersagt ist oder die Richterin oder der Richter

1.  vorläufig des Dienstes enthoben ist,
    
2.  an eine Behörde abgeordnet ist,
    
3.  als Mitglied des Richterrats an ein anderes Gericht abgeordnet oder vorübergehend mit der Wahrnehmung der Dienstgeschäfte als Gerichtsvorstand oder als dessen ständige Vertreterin oder ständiger Vertreter beauftragt ist oder
    
4.  als Mitglied des Präsidialrats an ein Gericht eines anderen Gerichtszweigs abgeordnet ist.
    

### § 30   
Erlöschen der Mitgliedschaft

Die Mitgliedschaft in der Richtervertretung erlischt durch

1.  Niederlegung des Amtes,
    
2.  Beendigung des Richterverhältnisses,
    
3.  Ausscheiden aus dem Bereich, für den die Richtervertretung gebildet ist,
    
4.  Verlust der Wählbarkeit, soweit kein Fall des § 29 vorliegt,
    
5.  gerichtliche Feststellung, dass die oder der Gewählte nicht wählbar war,
    
6.  Ausschluss aufgrund gerichtlicher Entscheidung nach § 38 oder § 58.
    

### § 31  
Kosten

Die durch die Wahl und die Tätigkeit der Richtervertretungen entstehenden notwendigen Kosten trägt die Dienststelle, bei der die Richtervertretung gebildet ist; sie stellt erforderlichenfalls Räume und Geschäftsbedarf zur Verfügung.

### § 32  
Rechtsweg

Für Rechtsstreitigkeiten aus der Bildung oder Tätigkeit der Richtervertretungen steht der Rechtsweg zu den Verwaltungsgerichten offen.
Das Verwaltungsgericht entscheidet bei Rechtsstreitigkeiten aus der gemeinsamen Beteiligung von Richterrat und Personalvertretung (§§ 40 und 55) nach den Vorschriften des § 95 Absatz 2 und des § 96 des Landespersonalvertretungsgesetzes über das Verfahren und die Besetzung.

Abschnitt 2  
Richterräte
-------------------------

### § 33  
Bildung des Richterrats und der Stufenvertretungen

Es werden folgende Richterräte gebildet:

1.  ein Richterrat für jedes Gericht,
    
2.  ein Gesamtrichterrat für jeden Gerichtszweig bei den oberen Landesgerichten mit Ausnahme der Finanzgerichtsbarkeit,
    
3.  ein Hauptrichter- und Hauptstaatsanwaltsrat für alle Gerichtszweige und Staatsanwaltschaften bei der obersten Dienstbehörde; § 47 Absatz 6 Satz 2 gilt entsprechend.
    

Die Aufgaben des Gesamtrichterrats für die Finanzgerichtsbarkeit nimmt der Richterrat bei dem Finanzgericht Berlin-Brandenburg wahr.

### § 34   
Zusammensetzung

(1) Der Richterrat besteht bei den Gerichten mit bis zu zehn Planstellen für Richterinnen und Richter aus einer Richterin oder einem Richter, bei den Gerichten mit elf bis zu 40 Planstellen aus drei, bei den Gerichten mit 41 bis zu 150 Planstellen aus fünf und darüber hinaus aus sieben Richterinnen und Richtern.

(2) Der Gesamtrichterrat besteht jeweils aus sieben Richterinnen und Richtern.

(3) Der Hauptrichter- und Hauptstaatsanwaltsrat besteht aus neun Richterinnen und Richtern und drei Staatsanwältinnen und Staatsanwälten.

### § 35   
Wahl und Bestimmung der Mitglieder

(1) Die Mitglieder des Richterrats werden von den Richterinnen und Richtern des jeweiligen Gerichts aus ihrer Mitte gewählt.

(2) Die Mitglieder des Gesamtrichterrats werden von den Richterinnen und Richtern des jeweiligen Gerichtszweigs aus ihrer Mitte gewählt.

(3) Von den richterlichen Mitgliedern des Hauptrichter- und Hauptstaatsanwaltsrats werden fünf Mitglieder vom Gesamtrichterrat der ordentlichen Gerichtsbarkeit, jeweils ein Mitglied von den Gesamtrichterräten der Verwaltungs-, Arbeits- und Sozialgerichtsbarkeit sowie ein Mitglied vom Richterrat bei dem Finanzgericht Berlin-Brandenburg aus dem Kreis ihrer Mitglieder bestimmt.
Für die staatsanwaltlichen Mitglieder gilt § 92 Absatz 5.

### § 36   
Neuwahl

(1) Abweichend vom regelmäßigen Wahltermin ist der Richterrat neu zu wählen, wenn

1.  nach einem Jahr seit der Wahl die Zahl der Planstellen für Richterinnen und Richter um die Hälfte gesunken oder gestiegen ist,
    
2.  die Zahl seiner Mitglieder auch nach dem Eintritt der Ersatzmitglieder um mehr als ein Viertel der vorgeschriebenen Zahl gesunken ist,
    
3.  er mit der Mehrheit seiner Mitglieder seinen Rücktritt beschlossen hat oder
    
4.  er durch gerichtliche Entscheidung aufgelöst wird.
    

(2) In den Fällen des Absatzes 1 Nummer 1 bis 3 führt der Richterrat die Geschäfte bis zur Wahl des neuen Richterrats weiter.

(3) Hat außerhalb des für die regelmäßige Richterratswahl festgelegten Zeitraums eine Richterratswahl stattgefunden, so ist der Richterrat in dem auf die Wahl folgenden nächsten Zeitraum der regelmäßigen Richterratswahl neu zu wählen.
Hat die Amtszeit des Richterrats zu Beginn dieses Zeitraums noch nicht ein Jahr betragen, so ist der Richterrat in dem übernächsten Zeitraum der regelmäßigen Richterratswahlen neu zu wählen.

### § 37  
Eintritt der Ersatzmitglieder

(1) Scheidet ein Mitglied aus dem Richterrat oder dem Gesamtrichterrat aus, so tritt ein Ersatzmitglied ein.
Das Gleiche gilt, wenn ein Mitglied verhindert ist, für die Zeit der Verhinderung sowie für den Fall des Ruhens der Mitgliedschaft.

(2) Als Ersatzmitglieder treten der Reihe nach die nicht gewählten Richterinnen und Richter derjenigen Vorschlagslisten ein, denen die zu ersetzenden Mitglieder angehören.
Ist das ausgeschiedene Mitglied mit einfacher Stimmenmehrheit nach § 88 Absatz 1 Satz 3 gewählt worden, so tritt die nicht gewählte Person mit der nächsthöheren Stimmenzahl als Erzsatzmitglied ein; bei gleicher Stimmenzahl entscheidet das Los.

### § 38  
Ausschluss, Auflösung

Ein Viertel der Wahlberechtigten kann auf Ausschluss eines Mitglieds aus dem Richterrat oder auf Auflösung des Richterrats wegen grober Verletzung gesetzlicher Pflichten klagen.
Der Richterrat kann aus den gleichen Gründen auf Ausschluss eines Mitglieds klagen.

### § 39   
Zuständigkeit der Richterräte

(1) Es ist zu beteiligen

1.  der Richterrat in Angelegenheiten, welche die Richterinnen und Richter des Gerichts betreffen, für das der Richterrat gebildet ist,
    
2.  der Gesamtrichterrat in Angelegenheiten, die über den Aufgabenbereich eines Richterrats hinausgehen und die ihm durch dieses Gesetz zugewiesen werden,
    
3.  der Hauptrichter- und Hauptstaatsanwaltsrat in Angelegenheiten, die mehrere Gerichtszweige oder mehrere Gerichte und mindestens eine Staatsanwaltschaft betreffen und die ihm durch dieses Gesetz zugewiesen werden.
    

(2) Betrifft eine Maßnahme ein Gericht und eine oder mehrere Staatsanwaltschaften, so gelten die §§ 40 und 55 Absatz 1 und 2 entsprechend mit der Maßgabe, dass die betroffenen Vertretungen gemeinsam entscheiden.

### § 40  
Gemeinsame Angelegenheiten

In allgemeinen und sozialen Angelegenheiten, die sowohl Richterinnen und Richter als auch andere Beschäftigte des Gerichts betreffen, erfolgt eine gemeinsame Beteiligung mit der Personalvertretung.
Die Beteiligungsrechte und das Einigungsverfahren richten sich nach dem Recht der Personalvertretung, soweit dieses Gesetz nichts anderes bestimmt.

### § 41  
Mitbestimmung

Der Richterrat bestimmt bei allen personellen, sozialen, organisatorischen und sonstigen innerdienstlichen Maßnahmen mit, die die Richterinnen und Richter insgesamt oder im Einzelfall betreffen oder sich auf sie auswirken.
Soweit Mitbestimmungsfälle über die beabsichtigten Maßnahmen hinaus schutzwürdige persönliche Interessen der Richterin oder des Richters berühren, ist die Mitbestimmung von der Zustimmung der betroffenen Person abhängig.
In jedem Fall ist das den Vorsitz des Richterrats führende Mitglied von der beabsichtigten Maßnahme zu unterrichten.
Die Mitbestimmung entfällt bei Organisationsentscheidungen der obersten Dienstbehörde, die auf deren verfassungsmäßigen Rechten beruhen.
Die Zuständigkeit des Richterrats besteht nicht in Angelegenheiten, die in die Zuständigkeit des Präsidialrats fallen.

### § 42  
_(aufgehoben)_

### § 43   
Initiativrecht

Zu den Aufgaben des Richterrats gehört es,

1.  Maßnahmen zu beantragen, die dem Gericht und den Richterinnen und Richtern unter Berücksichtigung der Belange der anderen Beschäftigten dienen,
    
2.  darauf hinzuwirken, dass die zugunsten der Richterinnen und Richter geltenden Rechtsvorschriften, Dienstvereinbarungen und Verwaltungsanordnungen durchgeführt werden,
    
3.  Beschwerden und Anregungen von Richterinnen und Richtern entgegenzunehmen und, falls sie begründet erscheinen, durch Verhandlung mit dem Gerichtsvorstand auf die Erledigung der Beschwerden und die Berücksichtigung der Anregungen hinzuwirken,
    
4.  Maßnahmen zur Eingliederung und zur beruflichen Entwicklung von Richterinnen und Richtern, die schwerbehindert sind, zu beantragen,
    
5.  die Zusammenarbeit von richterlichem und nichtrichterlichem Personal zu fördern.
    

### § 44  
Arbeitsschutz und Unfallverhütung

Für die Aufgaben und Befugnisse des Richterrats bei Maßnahmen im Zusammenhang mit dem Arbeitsschutz oder der Unfallverhütung gelten die personalvertretungsrechtlichen Bestimmungen entsprechend.
Krankenstatistiken sind ihm zur Kenntnisnahme vorzulegen.

### § 45  
Beteiligungsgrundsätze

(1) Richterrat und Gerichtsvorstand arbeiten im Rahmen der Rechtsvorschriften zur Erfüllung der dienstlichen Aufgaben und zum Wohl der Richterinnen und Richter unter Berücksichtigung der Belange der anderen Beschäftigten vertrauensvoll zusammen.

(2) Der Richterrat ist zur Wahrnehmung seiner Aufgaben rechtzeitig und umfassend zu unterrichten.
Ihm sind sämtliche zur Durchführung seiner Aufgaben erforderlichen Unterlagen vorzulegen.
Gerichtsvorstand und Richterrat sollen regelmäßige Besprechungen durchführen.
Personalakten dürfen nur mit Zustimmung der betroffenen Richterin oder des betroffenen Richters und durch ein von ihm bestimmtes Mitglied des Richterrats eingesehen werden.

### § 46  
Verfahren bei der Mitbestimmung

(1) Eine der Mitbestimmung des Richterrats unterliegende Maßnahme kann nur mit seiner Zustimmung getroffen werden, soweit in diesem Gesetz nichts anderes bestimmt ist.

(2) Der Gerichtsvorstand unterrichtet den Richterrat von der beabsichtigten Maßnahme und beantragt seine Zustimmung.
Der Richterrat kann verlangen, dass der Gerichtsvorstand die beabsichtigte Maßnahme begründet.
Der Beschluss des Richterrats ist dem Gerichtsvorstand innerhalb von zwei Wochen nach dem Eingang des Antrags schriftlich mitzuteilen und im Fall der Ablehnung zu begründen.
In dringenden Fällen kann die Frist auf eine Woche verkürzt werden.
Die Maßnahme gilt als gebilligt, wenn nicht der Richterrat innerhalb der genannten Frist die Zustimmung schriftlich verweigert; dies gilt nicht, wenn der Richterrat schriftlich Fristverlängerung beantragt hat.
Eine Fristverlängerung über zwei Wochen hinaus bedarf der Festsetzung durch den Gerichtsvorstand.
Ist der Gerichtsvorstand nach allgemeinen Vorschriften an eine Frist gebunden, so kommt eine Fristverlängerung höchstens bis zu einer Woche vor Ablauf dieser Frist in Betracht; hat der Richterrat bis zum Ablauf der Fristverlängerung die Zustimmung nicht schriftlich verweigert, so gilt die Maßnahme als gebilligt.

(3) Beantragt der Richterrat eine der Mitbestimmung unterliegende Maßnahme, hat er sie schriftlich vorzuschlagen und zu begründen.
Der Gerichtsvorstand gibt dem Richterrat innerhalb von zwei Wochen nach dem Eingang des Antrags seine Entscheidung bekannt.
Er erteilt einen Zwischenbescheid, falls eine Entscheidung innerhalb der Frist nicht möglich ist.
Bei Erteilung eines Zwischenbescheids ist die Entscheidung unverzüglich, spätestens aber innerhalb von drei Monaten nach dem Ablauf der Frist des Satzes 2 zu treffen.
Ablehnung der beantragten Maßnahme und Zwischenbescheid sind zu begründen.

(4) Soweit Mitglieder des Richterrats Beschwerden oder Behauptungen vortragen, die für eine Richterin oder einen Richter ungünstig sind oder nachteilig werden können, ist ihr oder ihm Gelegenheit zur Äußerung zu geben; die Äußerung ist aktenkundig zu machen.

### § 47  
Verfahren bei Nichtzustimmung

(1) Kommt es über eine der Mitbestimmung unterliegende Maßnahme zwischen dem Gerichtsvorstand und dem Richterrat nicht zu einer Einigung, so kann innerhalb von zwei Wochen nach der Feststellung der Nichteinigung die Sache schriftlich der übergeordneten Dienststelle, bei der eine Stufenvertretung gebildet ist, oder dem Gesamtrichterrat vorgelegt werden.
Gesamtrichterrat und übergeordnete Dienststelle verhandeln innerhalb von zwei Wochen.
Die Sätze 1 und 2 gelten entsprechend, wenn die Präsidentin oder der Präsident des oberen Landesgerichts den örtlich zuständigen Richterrat zu beteiligen hat.
In der Finanzgerichtsbarkeit beteiligt die übergeordnete Dienststelle den Richterrat.
In dringenden Fällen kann die Verhandlung binnen einer Woche beantragt werden.

(2) Einigen sich die Präsidentin oder der Präsident des oberen Landesgerichts als übergeordnete Dienststelle und die zuständige Richtervertretung nicht, so kann innerhalb von zwei Wochen nach der Feststellung der Nichteinigung die Sache schriftlich auf dem Dienstweg der obersten Dienstbehörde vorgelegt werden.
Absatz 1 Satz 2, 4 und 5 gilt entsprechend.

(3) In den Fällen des Absatzes 1 kann die Sache einvernehmlich auch unmittelbar der obersten Dienstbehörde zur weiteren Verhandlung mit dem Hauptrichter- und Hauptstaatsanwaltsrat vorgelegt werden.

(4) Kommt es zwischen der obersten Dienstbehörde und dem Hauptrichter- und Hauptstaatsanwaltsrat zu keiner Einigung, so kann innerhalb von zwei Wochen nach Feststellung der Nichteinigung die Einigungsstelle angerufen werden.
Eine Anrufung durch den Hauptrichter- und Hauptstaatsanwaltsrat bedarf eines Antrags der zuständigen Richtervertretung.
Sieht er von einer Anrufung ab, so hat er dies der zuständigen Richtervertretung unverzüglich mitzuteilen.

(5) Der Feststellung der Nichteinigung in den Fällen der Absätze 1 bis 4 steht es gleich, wenn seit dem Antrag auf Zustimmung oder Vorlage der Maßnahme sechs Wochen vergangen sind.

(6) Die Einigungsstelle wird bei der zuständigen obersten Dienstbehörde für die regelmäßige Dauer der Wahlperiode gebildet.
Bestehen mehrere oberste Dienstbehörden, wird die Einigungsstelle bei der Behörde gebildet, welche die Dienstaufsicht über die ordentliche Gerichtsbarkeit ausübt.
Sie besteht aus je drei von der obersten Dienstbehörde und dem Hauptrichter- und Hauptstaatsanwaltsrat bestellten beisitzenden sowie einem unparteiischen vorsitzenden Mitglied, auf dessen Person sich beide Seiten einigen.
Bestehen mehrere oberste Dienstbehörden, sind die beisitzenden Mitglieder einvernehmlich zu bestellen.
Kommt eine Einigung über die Person des vorsitzenden Mitglieds nicht zustande, bestellt ihn die Präsidentin oder der Präsident des Landtages.

(7) In gemeinsamen Angelegenheiten nach § 55 gehören der Einigungsstelle statt drei vom Richterrat benannter Mitglieder je ein vom Richterrat und vom Personalrat benanntes Mitglied sowie ein weiteres Mitglied an, das von dem Gremium benannt wird, das die höhere Anzahl von Wahlberechtigten hat.
Bei gleicher Anzahl von Wahlberechtigten entscheidet das Los.

(8) Kommt es bei mitbestimmungspflichtigen Maßnahmen der obersten Dienstbehörde nicht zu einer Einigung, so entscheidet die oberste Dienstbehörde endgültig; Absätze 1 bis 7 sowie §§ 48 bis 50 finden in diesem Fall keine Anwendung.

### § 48  
Verfahren vor der Einigungsstelle

Die Verhandlung der Einigungsstelle ist nicht öffentlich.
Den Beteiligten ist Gelegenheit zur mündlichen Äußerung zu geben, wenn sie sich nicht auf eine schriftliche Äußerung verständigen.

### § 49  
Beschlussfassung der Einigungsstelle

(1) Die Einigungsstelle entscheidet nach mündlicher Beratung durch Beschluss.
Der Beschluss wird von den Mitgliedern der Einigungsstelle mit Stimmenmehrheit gefasst.
Der Beschluss soll innerhalb von 30 Arbeitstagen nach Anrufung der Einigungsstelle ergehen.

(2) Der Beschluss ist auf Antrag unverzüglich schriftlich abzufassen, zu begründen, von dem unparteiischen Mitglied zu unterzeichnen und den Beteiligten zuzustellen.

(3) Der Beschluss ist für die Beteiligten bindend, soweit er nicht nach § 50 ganz oder teilweise aufgehoben wird.

### § 50  
Aufhebung bindungsgeeigneter Beschlüsse der Einigungsstelle

(1) Die oberste Dienstbehörde kann Beschlüsse der Einigungsstelle ganz oder teilweise aufheben und endgültig entscheiden, wenn durch den Beschluss der Amtsauftrag, für eine geordnete Rechtspflege zu sorgen, nicht nur unerheblich berührt wird.

(2) Die oberste Dienstbehörde hebt Beschlüsse der Einigungsstelle ganz oder teilweise auf und entscheidet endgültig, sofern der Beschluss gegen geltendes Recht verstößt.

### § 51  
_(aufgehoben)_

### § 52  
Vorläufige Regelungen

Der Gerichtsvorstand kann bei Maßnahmen, die der Natur der Sache nach keinen Aufschub dulden, bis zur endgültigen Entscheidung vorläufige Regelungen treffen.
Der Richterrat ist in diesen Fällen unverzüglich zu unterrichten.

### § 53  
Dienstvereinbarungen

(1) Dienstvereinbarungen sind zulässig, soweit Rechtsvorschriften nicht entgegenstehen.
Sie werden vom Gerichtsvorstand und dem Richterrat geschlossen.
Sie werden schriftlich niedergelegt, von beiden Seiten unterzeichnet und in dem Bereich bekannt gemacht, für den sie gelten sollen.

(2) Dienstvereinbarungen, die für einen größeren Bereich gelten, gehen den Dienstvereinbarungen für einen kleineren Bereich vor.

(3) Dienstvereinbarungen, die keine Kündigungsfristen enthalten, können von beiden Seiten mit einer Frist von drei Monaten gekündigt werden.

### § 54  
Gesamtrichterräte, Hauptrichter- und Hauptstaatsanwaltsrat

(1) Für die Mitgliedschaft in den Gesamtrichterräten und dem Hauptrichter- und Hauptstaatsanwaltsrat sowie deren Auflösung und Neuwahl gelten die §§ 36 und 38 entsprechend.

(2) In Angelegenheiten, in denen der Gesamtrichterrat oder der Hauptrichter- und Hauptstaatsanwaltsrat nach § 39 Absatz 1 Nummer 2 oder 3 zu beteiligen ist, gelten die §§ 41 bis 53 entsprechend.

### § 55  
Beteiligung an gemeinsamen Angelegenheiten

(1) Bestehen Anhaltspunkte, dass an einer Angelegenheit Richterrat und Personalrat gemeinsam beteiligt sind, unterrichten sie sich gegenseitig.
Dem Gerichtsvorstand ist Gelegenheit zur Stellungnahme zu geben.

(2) Sind an einer Angelegenheit Richterrat und Personalrat gemeinsam beteiligt, so beraten und beschließen beide in einer gemeinsamen Sitzung, an der die Mitglieder des Personalrats und eine nach Maßgabe des Absatzes 3 bestimmte Zahl von entsandten Mitgliedern des Richterrats teilnehmen.

(3) Die Zahl der entsandten Mitglieder des Richterrats verhält sich zu der Zahl der zum Richterrat Wahlberechtigten wie die Zahl der Mitglieder des Personalrats zu der Zahl der zum Personalrat Wahlberechtigten.
Jedoch entsendet der Richterrat mindestens die einem Fünftel der Mitglieder des Personalrats entsprechende Zahl.

(4) Besteht der Personalrat nur aus einer Person, so tritt ein Mitglied des Richterrats zur Beschlussfassung zum Personalrat hinzu.

(5) Ist die Zahl der Mitglieder des Richterrats und des Personalrats gleich groß, so treten beide Vertretungen zusammen; den Vorsitz führt die Vorsitzende oder der Vorsitzende des Richterrats.

(6) Für den Gesamtrichterrat und den Hauptrichter- und Hauptstaatsanwaltsrat gelten die Absätze 2 bis 5 entsprechend.

### § 56  
Gemeinsame Personalversammlung

An Personalversammlungen nehmen, soweit gemeinsame Angelegenheiten behandelt werden, die Richterinnen und Richter mit den gleichen Rechten wie die anderen Beschäftigten teil.

Abschnitt 3   
Präsidialräte
----------------------------

### § 57  
Bildung von Präsidialräten

(1) Bei jedem der für die Gerichtszweige gebildeten oberen Landesgerichte wird ein Präsidialrat gebildet.
Er besteht aus der Präsidentin oder dem Präsidenten des oberen Landesgerichts als der oder dem Vorsitzenden und weiteren Mitgliedern, die von den Richterinnen und Richtern der Gerichtszweige gewählt werden.

(2) Die Richterinnen und Richter der ordentlichen Gerichtsbarkeit wählen sechs Mitglieder, die Richterinnen und Richter der Verwaltungs-, der Sozial- und der Arbeitsgerichtsbarkeit je vier und die Richterinnen und Richter der Finanzgerichtsbarkeit zwei Mitglieder.
In den Präsidialrat können nur Richterinnen und Richter gewählt werden, die am Wahltag seit mindestens fünf Jahren im Richterverhältnis auf Lebenszeit stehen.

(3) Die Mitglieder des Richterwahlausschusses und ihre Stellvertretung können dem Präsidialrat nicht angehören.
Wird ein Mitglied des Präsidialrats zum Mitglied des Richterwahlausschusses gewählt, scheidet es mit der Wahl aus dem Präsidialrat aus.

### § 58  
Ausschluss von gewählten Mitgliedern

Auf Antrag der obersten Dienstbehörde oder mindestens der Hälfte der Mitglieder des Präsidialrats kann ein gewähltes Mitglied wegen grober Verletzung seiner Pflichten durch gerichtliche Entscheidung aus dem Präsidialrat ausgeschlossen werden.

### § 59  
Stellvertretung

Die oder der Vorsitzende des Präsidialrats wird durch die ständige Vertreterin oder den ständigen Vertreter der Präsidentin oder des Präsidenten des jeweiligen oberen Landesgerichts vertreten.
Ist keine Vertreterin oder kein Vertreter ernannt, so wirkt stattdessen die oder der dienstälteste, bei gleichem Dienstalter die oder der lebensälteste Vorsitzende des jeweiligen oberen Landesgerichts mit.
Für die Stellvertretung der übrigen Mitglieder gilt § 37 entsprechend.

### § 60   
Aufgaben

(1) Der Präsidialrat ist zu beteiligen bei

1.  jeder Einstellung von Richterinnen und Richtern sowie der Ernennung zur Richterin oder zum Richter auf Lebenszeit,
    
2.  jeder Übertragung eines Richteramtes mit höherem Endgrundgehalt als dem eines Eingangsamtes,
    
3.  jeder Versetzung,
    
4.  der Rücknahme der Ernennung und der Entlassung einer Richterin oder eines Richters auf Probe und einer Richterin oder eines Richters kraft Auftrags; das gilt nicht, wenn die Richterin oder der Richter die Entlassung schriftlich beantragt oder der Rücknahme oder der Entlassung schriftlich zustimmt,
    
5.  Abordnungen ab einer Dauer von sechs Monaten,
    
6.  vorzeitiger Versetzung in den Ruhestand,
    
7.  der Übertragung eines anderen Richteramtes mit geringerem Endgrundgehalt und der Amtsenthebung infolge einer Veränderung der Gerichtsorganisation (§ 32 des Deutschen Richtergesetzes).
    

(2) In den Fällen des Absatzes 1 Nummer 4, 6 und 7 wird der Präsidialrat nur auf Antrag der betroffenen Richterin oder des betroffenen Richters beteiligt.
Der Gerichtsvorstand weist die Richterin oder den Richter rechtzeitig auf das Antragsrecht hin.

(3) Zuständig ist der Präsidialrat für den Gerichtszweig, dem die Richterin oder der Richter angehört; im Fall des Absatzes 1 Nummer 1 bis 3 ist der Präsidialrat für den Gerichtszweig zuständig, bei dem die Richterin oder der Richter verwendet werden soll.

### § 61  
Stellungnahme des Präsidialrats

(1) Eine Entscheidung über Maßnahmen nach § 60 Absatz 1 kann erst getroffen werden, wenn die Stellungnahme des Präsidialrats vorliegt oder die Frist zur Stellungnahme verstrichen ist.

(2) In den Fällen des § 60 Absatz 1 Nummer 1, 3 und 5 beantragt die Präsidentin oder der Präsident des jeweiligen oberen Landesgerichts die Stellungnahme des Präsidialrats zu ihrem oder seinem Personalvorschlag.
Die Frist zur Stellungnahme beträgt drei Wochen.
Äußert sich der Präsidialrat nicht innerhalb dieser Frist, so gilt der Personalvorschlag als gebilligt.
Weicht die oberste Dienstbehörde mit ihrem Personalvorschlag von dem Vorschlag der Präsidentin oder des Präsidenten ab, so beteiligt sie den Präsidialrat erneut; die Sätze 2 und 3 gelten entsprechend.

(3) Absatz 2 gilt entsprechend bei einer Beteiligung nach § 60 Absatz 1 Nummer 2 mit der Maßgabe, dass in den Fällen des § 22a die Präsidentin oder der Präsident des oberen Landesgerichts statt eines Personalvorschlags einen vorbereitenden Bericht vorlegt.
Der Präsidialrat nimmt zu der persönlichen und fachlichen Eignung der Bewerberinnen und Bewerber für das angestrebte Amt Stellung.
Er kann sich dabei auch zum Grad der Eignung äußern.

(4) In den Fällen des § 60 Absatz 1 Nummer 4, 6 und 7 beantragt die oberste Dienstbehörde die Stellungnahme des Präsidialrats.
Absatz 2 Satz 2 und 3 gilt entsprechend.

(5) Mit dem Antrag sind im Fall des § 60 Absatz 1 Nummer 1 die Bewerbungsunterlagen, im Fall des § 60 Absatz 1 Nummer 2 die Personalakten sämtlicher Bewerberinnen und Bewerber vorzulegen.
Im Fall des § 60 Absatz 1 Nummer 6 sind dem Antrag die medizinischen Stellungnahmen beizufügen.

(6) Der Vorstand des Gerichts oder die Dienstbehörde kann bei Maßnahmen, die der Natur der Sache nach keinen Aufschub dulden, bis zur endgültigen Entscheidung vorläufige Regelungen treffen.
Der Präsidialrat ist hiervon unverzüglich zu unterrichten.

### § 62  
Neuwahl

Für die Neuwahl der Präsidialräte gilt § 36 Absatz 1 Nummer 2 und 3 und Absatz 2 entsprechend.

Abschnitt 4   
Kontrollgremium IT
---------------------------------

### § 62a  
Kontrollgremium IT

Das Kontrollgremium IT setzt sich aus folgenden Mitgliedern zusammen:

1.  je ein von jedem Gesamtrichterrat sowie von dem Richterrat bei dem Finanzgericht Berlin-Brandenburg benanntes Mitglied aus der Richterschaft,
2.  ein vom Gesamtstaatsanwaltsrat (§ 92 Absatz 1 Satz 2) benanntes Mitglied aus der Staatsanwaltschaft,
3.  ein vom Hauptpersonalrat der Justiz benanntes Mitglied aus dem Kreis der Rechtspflegerinnen und Rechtspfleger.

Die Benennung erfolgt jeweils innerhalb von sechs Monaten nach Beginn der Amtsperiode der in Satz 1 genannten Personalvertretung.
Bis zur Benennung bleiben die bisher benannten Mitglieder im Amt.
Für die Mitglieder des Kontrollgremiums IT gelten § 27 Absatz 1 und 2 sowie § 31 entsprechend.

### § 62b  
Aufgaben

(1) Das Kontrollgremium IT und das für Justiz zuständige Ministerium vereinbaren verbindliche Regeln für den Umgang mit elektronischen gerichtlichen und staatsanwaltlichen Dokumenten durch den Zentralen IT-Dienstleister der Justiz des Landes Brandenburg.

(2) Das Kontrollgremium IT überprüft die Einhaltung der in Absatz 1 genannten Regeln.
Es hat das Recht, Berichte zur Datenverarbeitung, zum Datenschutz und zur Datensicherheit anzufordern.
Es soll das für Justiz zuständige Ministerium auf von ihm festgestellte Rechtsverletzungen aufmerksam machen und Abhilfe verlangen.

Abschnitt 5   
Vertretung ehrenamtlicher Richterinnen und Richter
-----------------------------------------------------------------

### § 63   
Bildung der Vertretung ehrenamtlicher Richterinnen und Richter

Schöffinnen und Schöffen, Handelsrichterinnen und Handelsrichter, ehrenamtliche Richterinnen und Richter in der Verwaltungs- und Finanzgerichtsbarkeit sowie in Landwirtschaftssachen können an den Gerichten, an denen sie tätig sind, Vertretungen wählen, die aus jeweils drei Mitgliedern bestehen.
Die Vertretungen werden in Angelegenheiten beteiligt, die die ehrenamtlichen Richterinnen und Richter betreffen, und nehmen deren Interessen wahr.
Das Nähere über die Aufgaben der Vertretungen in den Gerichtszweigen sowie die Wahl der Vertretungen kann das für Justiz zuständige Mitglied der Landesregierung durch Rechtsverordnung bestimmen.
Der Gerichtsvorstand beruft spätestens vier Wochen nach Beginn der Amtszeit eine Versammlung der jeweiligen Gruppe der ehrenamtlichen Richterinnen und Richter ein.
Die Versammlung entscheidet zunächst darüber, ob sie gewillt ist, eine Vertretung zu wählen.
Im Fall der Entscheidung für die Wahl einer Vertretung beschließt die Versammlung das Wahlverfahren, sofern es an einer Rechtsverordnung nach Satz 3 fehlt.
Die Vertretung der ehrenamtlichen Richterinnen und Richter in der Arbeits- und Sozialgerichtsbarkeit richtet sich nach § 29 des Arbeitsgerichtsgesetzes und § 23 des Sozialgerichtsgesetzes.

Kapitel 4  
Richterdienstgerichte
---------------------------------

Abschnitt 1  
Errichtung und Zuständigkeit
------------------------------------------

### § 64  
Errichtung und Zuständigkeit

(1) Richterdienstgerichte sind das Dienstgericht des Landes Brandenburg (Dienstgericht) und der Dienstgerichtshof des Landes Brandenburg (Dienstgerichtshof).

(2) Das Dienstgericht wird bei dem Landgericht Cottbus, der Dienstgerichtshof bei dem Brandenburgischen Oberlandesgericht errichtet.

(3) Die unmittelbare Dienstaufsicht führen die Präsidentin oder der Präsident des Landgerichts Cottbus über das Dienstgericht und die Präsidentin oder der Präsident des Brandenburgischen Oberlandesgerichts über den Dienstgerichtshof.
Die weitere Dienstaufsicht führt das für Justiz zuständige Ministerium.

(4) Die Geschäftsstelle des Gerichts, bei dem das Richterdienstgericht errichtet ist, nimmt auch die Aufgaben der Geschäftsstelle des Richterdienstgerichts wahr.

### § 65  
Zuständigkeit des Dienstgerichts

Das Dienstgericht entscheidet

1.  in Disziplinarverfahren, auch der Richterinnen und Richter im Ruhestand,
    
2.  über die Versetzung im Interesse der Rechtspflege,
    
3.  bei Richterinnen und Richtern auf Lebenszeit oder auf Zeit über die
    
    1.  Nichtigkeit einer Ernennung,
        
    2.  Rücknahme einer Ernennung,
        
    3.  Entlassung,
        
    4.  Versetzung in den Ruhestand wegen Dienstunfähigkeit,
        
    5.  eingeschränkte Verwendung wegen begrenzter Dienstfähigkeit,
        
4.  bei Anfechtung
    1.  einer Maßnahme wegen Veränderung der Gerichtsorganisation
    2.  der Abordnung einer Richterin oder eines Richters gemäß § 37 Absatz 3 des Deutschen Richtergesetzes,
    3.  einer Verfügung, durch die eine Richterin oder ein Richter auf Probe oder kraft Auftrags entlassen, durch die die Ernennung zurückgenommen oder die Nichtigkeit der Ernennung festgestellt oder durch die sie oder er wegen Dienstunfähigkeit in den Ruhestand versetzt wird,
    4.  der Übertragung eines weiteren Richteramtes bei einem anderen Gericht, soweit ein Gesetz dies zulässt,
    5.  der Heranziehung zu einer Nebentätigkeit,
    6.  einer Maßnahme der Dienstaufsicht aus den Gründen des § 26 Absatz 3 des Deutschen Richtergesetzes,
    7.  einer Verfügung über die Teilzeitbeschäftigung oder Beurlaubung (§§ 4 und 5).

### § 66   
Zuständigkeit des Dienstgerichtshofs

Der Dienstgerichtshof entscheidet

1.  über Berufungen gegen Urteile und über Beschwerden gegen Beschlüsse des Dienstgerichts,
    
2.  in den sonstigen Fällen, in denen nach den Vorschriften dieses Gesetzes und den danach anzuwendenden Verfahrensgesetzen das Gericht des zweiten Rechtszugs zuständig ist.
    

Abschnitt 2  
Besetzung
-----------------------

### § 67   
Mitglieder der Richterdienstgerichte

(1) Die richterlichen Mitglieder der Richterdienstgerichte müssen auf Lebenszeit ernannte Richterinnen und Richter sein.
Richterinnen und Richter, denen die Dienstaufsicht über Richterinnen und Richter zusteht, und ihre ständigen Vertreterinnen und Vertreter können nicht Mitglieder eines Richterdienstgerichts sein.

(2) Bei dem Dienstgerichtshof wirken ehrenamtliche Richterinnen und Richter aus der Rechtsanwaltschaft als ständige beisitzende Mitglieder an allen Entscheidungen mit.
Zum Mitglied des Dienstgerichtshofs kann nur ernannt werden, wer in den Vorstand der Rechtsanwaltskammer gewählt werden kann.
Die Mitglieder des Dienstgerichtshofs dürfen nicht gleichzeitig dem Vorstand der Rechtsanwaltskammer oder der Satzungsversammlung angehören oder bei der Rechtsanwaltskammer oder der Satzungsversammlung im Haupt- oder Nebenberuf tätig sein.
Das Amt des anwaltlichen Mitglieds können nur Deutsche ausüben.

(3) Die richterlichen Mitglieder der Richterdienstgerichte werden für fünf Geschäftsjahre von dem Präsidium des Gerichts bestellt, bei dem das Richterdienstgericht errichtet ist.
Die anwaltlichen Mitglieder des Dienstgerichtshofs werden für fünf Geschäftsjahre vom Präsidium des Brandenburgischen Oberlandesgerichts bestellt.

(4) Scheidet ein Mitglied vorher aus, so ist für den Rest der Amtszeit eine Nachfolge zu bestimmen.

(5) Die anwaltlichen Mitglieder, die weder ihren Wohnsitz noch ihren Kanzleisitz am Sitz des Richterdienstgerichts haben, erhalten Tage- und Übernachtungsgeld in entsprechender Anwendung der für Richterinnen und Richter im Eingangsamt geltenden Vorschriften.
Fahrtkosten werden ihnen in entsprechender Anwendung von § 5 des Justizvergütungs- und -entschädigungsgesetzes ersetzt.

### § 68  
Bestimmung der Mitglieder der Richterdienstgerichte

(1) Die anwaltlichen Mitglieder werden aus einer Vorschlagsliste bestimmt, welche der Vorstand der Rechtsanwaltskammer aufstellt.
Das Präsidium des Brandenburgischen Oberlandesgerichts ist bei der Berufung der anwaltlichen Mitglieder an die Vorschlagsliste gebunden.
Das Präsidium bestimmt die erforderliche Zahl von anwaltlichen Mitgliedern.
Die Vorschlagsliste muss mindestens das Eineinhalbfache der erforderlichen Anzahl von Rechtsanwältinnen und Rechtsanwälten enthalten.

(2) Die ständigen und nichtständigen richterlichen Mitglieder der Richterdienstgerichte werden aus Vorschlagslisten in der erforderlichen Anzahl bestimmt, welche die Präsidien des Brandenburgischen Oberlandesgerichts, des Oberverwaltungsgerichts Berlin-Brandenburg, des Landessozialgerichts Berlin-Brandenburg, des Landesarbeitsgerichts Berlin-Brandenburg und des Finanzgerichts Berlin-Brandenburg aufstellen.
Das Präsidium des Gerichts, bei dem das Richterdienstgericht errichtet ist, ist an die Vorschlagslisten gebunden.

(3) Das Präsidium des Gerichts, bei dem das Richterdienstgericht errichtet ist, bestimmt aus der Vorschlagsliste des Brandenburgischen Oberlandesgerichts die Vorsitzende oder den Vorsitzenden des Richterdienstgerichts.
Vor Beginn des Geschäftsjahres legt es die Regel fest, nach der die beisitzenden Mitglieder zu den Verfahren heranzuziehen sind.

### § 69  
Besetzung des Dienstgerichts

(1) Das Dienstgericht verhandelt und entscheidet in der Besetzung mit einer oder einem Vorsitzenden, einem ständigen und einem nichtständigen richterlichen beisitzenden Mitglied.

(2) Die oder der Vorsitzende des Dienstgerichts muss der ordentlichen Gerichtsbarkeit, das ständige richterliche beisitzende Mitglied der Verwaltungsgerichtsbarkeit angehören.
Die Vertreterinnen und Vertreter sind jeweils derselben Gerichtsbarkeit zu entnehmen.

(3) Das nichtständige richterliche beisitzende Mitglied des Dienstgerichts muss dem Gerichtszweig der betroffenen Richterin oder des betroffenen Richters angehören.

### § 70  
Besetzung des Dienstgerichtshofs

(1) Der Dienstgerichtshof verhandelt und entscheidet in der Besetzung mit einer oder einem Vorsitzenden, einem ständigen richterlichen und einem ständigen anwaltlichen beisitzenden Mitglied sowie zwei nichtständigen richterlichen beisitzenden Mitgliedern.

(2) Bei dem Dienstgerichtshof müssen die oder der Vorsitzende der ordentlichen Gerichtsbarkeit, das ständige richterliche beisitzende Mitglied der Verwaltungsgerichtsbarkeit angehören.
Die Vertreterinnen und Vertreter sind jeweils derselben Gerichtsbarkeit zu entnehmen.
Für den Dienstgerichtshof werden die Mitglieder aus den Richterinnen und Richtern der oberen Landesgerichte bestimmt.

(3) Die nichtständigen richterlichen beisitzenden Mitglieder müssen dem Gerichtszweig der betroffenen Richterin oder des betroffenen Richters angehören.

### § 71  
Verbot der Amtsausübung

(1) Ein richterliches Mitglied eines Richterdienstgerichts, gegen das ein gerichtliches Disziplinarverfahren oder wegen einer vorsätzlichen Straftat ein Strafverfahren eingeleitet ist oder dem die Führung seiner Amtsgeschäfte vorläufig untersagt ist, kann während dieses Verfahrens oder der Dauer des Verbots sein Amt nicht ausüben.

(2) Ein anwaltliches Mitglied des Dienstgerichtshofs, gegen das ein anwaltsgerichtliches Verfahren oder wegen einer vorsätzlichen Straftat ein Strafverfahren eingeleitet oder ein Berufs- oder Vertretungsverbot (§§ 150, 161a der Bundesrechtsanwaltsordnung) verhängt worden ist, kann während dieses Verfahrens und der Dauer des Verbots sein Amt nicht ausüben.
Werden dem Vorstand der Rechtsanwaltskammer solche Tatbestände bekannt, so unterrichtet er unverzüglich das Präsidium des Gerichts, bei dem das Dienstgericht eingerichtet ist.

### § 72  
Erlöschen des Amtes, Ruhen

(1) Das Amt des richterlichen Mitglieds des Richterdienstgerichts erlischt, wenn

1.  eine Voraussetzung für die Berufung der Richterin oder des Richters in das Amt wegfällt,
    
2.  gegen die Richterin oder den Richter im Strafverfahren eine Freiheitsstrafe oder im gerichtlichen Disziplinarverfahren mindestens eine Geldbuße rechtskräftig verhängt wird.
    

(2) Die Rechte und Pflichten als Mitglied ruhen, solange die Richterin oder der Richter an eine Behörde oder an eine andere Stelle als an ein Gericht des Landes abgeordnet ist.
Das Gleiche gilt, wenn die Richterin oder der Richter vorübergehend Aufgaben im Sinne des § 67 Absatz 1 Satz 2 wahrnimmt.

(3) Das Amt des anwaltlichen Mitglieds erlischt, wenn

1.  eine Voraussetzung für die Berufung der Rechtsanwältin oder des Rechtsanwalts in das Amt wegfällt,
    
2.  die Rechtsanwältin oder der Rechtsanwalt aus der Rechtsanwaltskammer ausscheidet, für die sie oder er als Mitglied benannt ist,
    
3.  die Rechtsanwältin oder der Rechtsanwalt im Strafverfahren zu einer Freiheitsstrafe rechtskräftig verurteilt oder gegen sie oder ihn im anwaltsgerichtlichen Verfahren mindestens eine Geldbuße rechtskräftig verhängt worden ist.
    

§ 71 Absatz 2 Satz 2 gilt entsprechend.

Abschnitt 3  
Disziplinarverfahren
----------------------------------

### § 73  
Geltung des Disziplinargesetzes

(1) Für das Verfahren in Disziplinarsachen gelten die Vorschriften des Landesdisziplinargesetzes sinngemäß, soweit dieses Gesetz nichts anderes bestimmt.

(2) Die Disziplinarklage wird von der obersten Dienstbehörde erhoben.

(3) Durch Disziplinarverfügung kann nur ein Verweis verhängt werden.

### § 74  
Disziplinarmaßnahmen

Disziplinarmaßnahmen sind:

1.  Verweis,
    
2.  Geldbuße,
    
3.  Gehaltskürzung,
    
4.  Versetzung in ein anderes Richteramt mit geringerem Endgrundgehalt,
    
5.  Versetzung in ein anderes Richteramt mit gleichem Endgrundgehalt, verbunden mit Gehaltskürzung,
    
6.  Entfernung aus dem Dienst,
    
7.  Kürzung des Ruhegehalts,
    
8.  Aberkennung des Ruhegehalts.
    

### § 75   
Entscheidungen des Dienstgerichts anstelle der obersten Dienstbehörde

(1) Über die vorläufige Dienstenthebung und die Einbehaltung von Bezügen sowie über die Aufhebung dieser Maßnahmen entscheidet auf Antrag der obersten Dienstbehörde das Dienstgericht durch Beschluss.

(2) Gegen die Entscheidung des Dienstgerichts ist die Beschwerde zulässig.

(3) Anstelle des Dienstgerichts entscheidet der Dienstgerichtshof, wenn bei ihm in derselben Sache ein Disziplinarverfahren anhängig ist.

### § 76  
Verfahren

Mit den Ermittlungen zur Durchführung des Disziplinarverfahrens kann nur eine Richterin oder ein Richter beauftragt werden.

### § 77   
Zulässigkeit der Revision

Gegen Urteile des Dienstgerichtshofs ist die Revision an das Dienstgericht des Bundes nach den Vorschriften der §§ 81 und 82 des Deutschen Richtergesetzes statthaft, wenn auf Versetzung in ein Richteramt mit geringerem Endgrundgehalt, Entfernung aus dem Dienst, Kürzung oder Aberkennung des Ruhegehalts erkannt ist oder das Gericht entgegen dem Antrag der obersten Dienstbehörde eine dieser Disziplinarmaßnahmen nicht verhängt hat.

### § 78   
Bekleidung mehrerer Ämter

(1) Ist eine Richterin oder ein Richter zugleich Beamtin oder Beamter, so sind die Vorschriften über das Disziplinarverfahren gegen Richterinnen und Richter anzuwenden.

(2) Das Richterdienstgericht kann im Urteil die Wirkung der Entfernung aus dem Dienst auf das Richterverhältnis und auf die in Verbindung mit diesem bekleideten Nebenämter beschränken.

### § 79  
Richterinnen und Richter kraft Auftrags

Ist eine Richterin oder ein Richter kraft Auftrags nach § 23 in Verbindung mit § 22 Absatz 3 des Deutschen Richtergesetzes aus ihrem oder seinem Richteramt entlassen worden, so steht dies der Durchführung eines gerichtlichen Disziplinarverfahrens gegen sie oder ihn nach den beamtenrechtlichen Vorschriften nicht entgegen.

Abschnitt 4   
Versetzungs- und Prüfungsverfahren
-------------------------------------------------

### § 80  
Allgemeine Verfahrensvorschriften

Für das Verfahren nach § 65 Nummer 2 bis 4 gelten die Vorschriften der Verwaltungsgerichtsordnung entsprechend, soweit dieses Gesetz nichts anderes bestimmt.
Ein Vorverfahren findet nur in den Fällen des § 65 Nummer 4 statt.

### § 81   
Versetzungsverfahren

(1) Das Versetzungsverfahren (§ 65 Nummer 2) wird durch einen Antrag der obersten Dienstbehörde eingeleitet.

(2) Das Gericht erklärt eine der in § 31 des Deutschen Richtergesetzes vorgesehenen Maßnahmen für zulässig oder weist den Antrag zurück.

### § 82  
Einleitung des Prüfungsverfahrens

Das Prüfungsverfahren wird in den Fällen des § 65 Nummer 3 durch einen Antrag der obersten Dienstbehörde, in den Fällen des § 65 Nummer 2 und 4 durch einen Antrag der Richterin oder des Richters eingeleitet.

### § 83   
Versetzung in den Ruhestand wegen Dienstunfähigkeit mit Zustimmung

(1) Beantragt eine Richterin oder ein Richter auf Lebenszeit oder eine Richterin oder ein Richter auf Zeit schriftlich, sie oder ihn wegen Dienstunfähigkeit in den Ruhestand zu versetzen, oder stimmt sie oder er der Versetzung in den Ruhestand schriftlich zu, wird die Dienstunfähigkeit dadurch festgestellt, dass die oder der Dienstvorgesetzte aufgrund eines ärztlichen Gutachtens über den Gesundheitszustand erklärt, sie oder er halte die Richterin oder den Richter für dauernd unfähig, die Amtspflichten zu erfüllen.

(2) Die Behörde, die über die Versetzung in den Ruhestand entscheidet, ist an die Erklärung der oder des Dienstvorgesetzten nicht gebunden; sie kann auch andere Beweise erheben.

### § 84  
Versetzung in den Ruhestand wegen Dienstunfähigkeit ohne Zustimmung

(1) Hält die oberste Dienstbehörde eine Richterin oder einen Richter auf Lebenszeit oder eine Richterin oder einen Richter auf Zeit für dienstunfähig und stellt die Richterin oder der Richter keinen Antrag nach § 83 Absatz 1, so teilt die oberste Dienstbehörde der Richterin oder dem Richter mit, dass die Versetzung in den Ruhestand beabsichtigt sei; dabei sind die Gründe für die Versetzung in den Ruhestand anzugeben.

(2) Stimmt die Richterin oder der Richter der Versetzung in den Ruhestand nicht innerhalb eines Monats schriftlich zu, beantragt die oberste Dienstbehörde beim Dienstgericht, die Zulässigkeit der Versetzung in den Ruhestand festzustellen, oder ordnet die Einstellung des Verfahrens an.
Die Einstellungsverfügung ist der Richterin oder dem Richter zuzustellen.

(3) Gibt das Dienstgericht dem Antrag nach Absatz 2 Satz 1 statt, so ist die Richterin oder der Richter nach Rechtskraft der gerichtlichen Entscheidung in den Ruhestand zu versetzen.
Der Ruhestand beginnt mit dem Ablauf des Monats, in welchem der Richterin oder dem Richter die Entscheidung über die Versetzung in den Ruhestand zugestellt worden ist.
Wird der Antrag rechtskräftig zurückgewiesen, wird das Verfahren eingestellt.
Die Einstellungsverfügung ist der Richterin oder dem Richter zuzustellen.

(4) Mit Ablauf des Monats, in dem der Antrag nach Absatz 2 Satz 1 gestellt worden ist, wird der Teil der Dienstbezüge einbehalten, der das Ruhegehalt übersteigt.
Wird die Richterin oder der Richter zur Ruhe gesetzt, werden die einbehaltenen Bezüge nicht nachgezahlt.
Wird das Verfahren nach Absatz 3 Satz 3 eingestellt, sind die einbehaltenen Bezüge nachzuzahlen.

### § 85  
Urteilsformel

(1) In dem Fall des § 65 Nummer 3 Buchstabe a stellt das Gericht die Nichtigkeit fest oder weist den Antrag zurück.
In den Fällen des § 65 Nummer 3 Buchstabe b bis d stellt das Gericht die Zulässigkeit der Maßnahme oder die Entlassung fest oder weist den Antrag zurück.

(2) In den Fällen des § 65 Nummer 4 hebt das Gericht die angefochtene Maßnahme auf oder weist den Antrag zurück.

### § 86  
Aussetzung von Verfahren

(1) Ist eine Maßnahme der Dienstaufsicht aus den Gründen des § 26 Absatz 3 des Deutschen Richtergesetzes angefochten worden und hängt die Entscheidung hierüber von dem Bestehen oder Nichtbestehen eines Rechtsverhältnisses ab, das den Gegenstand eines anderen Verfahrens bildet oder bilden kann, so hat das Richterdienstgericht die Verhandlung bis zur Erledigung des anderen Verfahrens auszusetzen.
Der Aussetzungsbeschluss ist zu begründen.

(2) Ist das Verfahren bei dem anderen Gericht noch nicht anhängig, so setzt das Richterdienstgericht in dem Aussetzungsbeschluss eine angemessene Frist zur Einleitung des Verfahrens.
Nach fruchtlosem Ablauf der Frist weist es den Antrag ohne weitere Sachprüfung zurück.

(3) Hängt die Entscheidung eines anderen Gerichts als eines Richterdienstgerichts davon ab, ob eine Maßnahme der Dienstaufsicht aus den Gründen des § 26 Absatz 3 des Deutschen Richtergesetzes unzulässig ist, so hat das Gericht die Verhandlung bis zur Erledigung des Verfahrens vor dem Richterdienstgericht auszusetzen.
Der Aussetzungsbeschluss ist zu begründen.
Absatz 2 gilt entsprechend.

### § 87  
Kostenentscheidung in besonderen Fällen

In Verfahren nach § 65 Nummer 2 und Nummer 3 Buchstabe a und c kann das Gericht die Kosten nach billigem Ermessen der Landeskasse auch insoweit auferlegen, als es nach dem Antrag der obersten Dienstbehörde erkannt hat, sofern die Richterin oder der Richter diesem Antrag nicht widersprochen hat.

Kapitel 5   
Wahlen
-------------------

### § 88  
Grundsatz

(1) Die dem Landtag zur Wahl für den Richterwahlausschuss vorzuschlagenden Richterinnen und Richter werden von der Richterschaft nach den Grundsätzen der Personen- und Mehrheitswahl gewählt.
Die Mitglieder der Richterräte sowie der Gesamtrichterräte und die zu wählenden Mitglieder der Präsidialräte werden nach den Grundsätzen der Verhältniswahl gewählt.
Wird nur ein Wahlvorschlag eingereicht oder besteht der Richterrat nur aus einer Richterin oder einem Richter, so findet Personen- und Mehrheitswahl statt.
Die Wahlen nach den Sätzen 1 bis 3 werden geheim und unmittelbar durchgeführt.

(2) Frauen und Männer sollen bei der Besetzung der in diesem Gesetz geregelten Gremien angemessen berücksichtigt werden.
Zu diesem Zweck sollen ebenso viele Frauen wie Männer vorgeschlagen werden, sofern die Wahl eines Gremiums auf der Grundlage von Vorschlagslisten erfolgt.

### § 89  
Wahlrecht, Wählbarkeit

(1) Wahlberechtigt sind alle Richterinnen und Richter, die am Wahltag einem Gericht angehören.
§ 15 Absatz 1 Satz 1 und Absatz 3 bleibt unberührt.
Die Wahlberechtigung erlischt bei einer Abordnung an eine Verwaltungsbehörde, sobald bei dieser eine Wahlberechtigung nach personalvertretungsrechtlichen Vorschriften besteht.

(2) Wählbar sind alle Richterinnen und Richter auf Lebenszeit, die am Wahltag einem Gericht im Anwendungsbereich dieses Gesetzes angehören.

(3) Nicht wählbar sind

1.  zum Richterrat die Vorstände der Gerichte, an denen der Richterrat gebildet wird, ihre ständigen Vertreterinnen und Vertreter, Aufsicht führende Richterinnen und Richter und an eine Verwaltungsbehörde abgeordnete Richterinnen und Richter,
    
2.  zum Präsidialrat die Mitglieder des Richterwahlausschusses und ihre Stellvertreterinnen und Stellvertreter.
    

### § 90  
Wahlordnung

(1) Die Landesregierung erlässt durch Rechtsverordnung eine Wahlordnung, die das Nähere über die Ausgestaltung des jeweiligen Wahlsystems und das Wahlverfahren regelt, insbesondere Bestimmungen über die Bestellung und die Aufgaben des Wahlvorstandes, die Aufstellung der Wählerliste, die Erstellung der Vorschlagslisten, die Einreichung von Wahlvorschlägen sowie die dafür erforderlichen Unterschriften, die Wahlhandlung, die Ausübung des Wahlrechts und die Feststellung des Wahlergebnisses und der Gewählten trifft.
Die Wahlordnung kann vorsehen, dass die in dem Gericht vertretenen Berufsverbände der Richterschaft Wahlvorschläge machen.

(2) Soweit dieses Gesetz oder die Wahlordnung gemäß Absatz 1 keine abschließende Regelung treffen, finden für die Wahl der Mitglieder der Richtervertretungen die Wahlvorschriften des Landespersonalvertretungsgesetzes entsprechend Anwendung.

### § 91  
Anfechtung der Wahl

(1) Sind bei der Wahl wesentliche Vorschriften über das Wahlrecht, die Wählbarkeit oder das Wahlverfahren verletzt worden, so kann die Wahl von mindestens drei Wahlberechtigten binnen zwei Wochen vom Tage der Bekanntgabe des Wahlergebnisses an bei dem Verwaltungsgericht angefochten werden, wenn der Verstoß das Wahlergebnis ändern oder beeinflussen konnte.

(2) Stellt das Gericht fest, dass die Wahl ungültig gewesen ist, so wird dadurch die Wirksamkeit der bis zur Rechtskraft der Entscheidung gefassten Beschlüsse der Gewählten nicht berührt.

(3) Bis zur Neuwahl nehmen die Richterinnen und Richter, die bis zur angefochtenen Wahl im Amt waren, die Geschäfte wahr.

Kapitel 6  
Staatsanwältinnen und Staatsanwälte
-----------------------------------------------

### § 92   
Aufgaben und Bildung der Staatsanwaltsräte

(1) Bei jeder Staatsanwaltschaft wird ein Staatsanwaltsrat gebildet.
Bei der Generalstaatsanwaltschaft wird ferner ein Gesamtstaatsanwaltsrat errichtet.

(2) Der Staatsanwaltsrat hat in Angelegenheiten der Staatsanwältinnen und Staatsanwälte die Aufgaben des Richterrats.
Der Gesamtstaatsanwaltsrat hat in Angelegenheiten der Staatsanwältinnen und Staatsanwälte die Aufgaben des Gesamtrichterrats und des Präsidialrats.
Er bestimmt aus seiner Mitte eine Vorsitzende oder einen Vorsitzenden sowie die Stellvertretung.

(3) Für die Zusammensetzung des Staatsanwaltsrats gilt § 34 Absatz 1 entsprechend.
Der Gesamtstaatsanwaltsrat besteht aus fünf Staatsanwältinnen und Staatsanwälten.

(4) Die §§ 26 bis 56 sowie die §§ 88 bis 91 gelten entsprechend.

(5) Die staatsanwaltlichen Mitglieder des Hauptrichter- und Hauptstaatsanwaltsrats werden von dem Gesamtstaatsanwaltsrat aus dem Kreis seiner Mitglieder bestimmt.

(6) Zu den Staatsanwältinnen und Staatsanwälten im Sinne dieses Kapitels gehören auch die bei der Staatsanwaltschaft beschäftigten Richterinnen und Richter auf Probe und Richterinnen und Richter kraft Auftrags.
§ 89 Absatz 2 bleibt unberührt.

### § 93  
Beteiligungsverfahren

(1) Im Verfahren vor der Einigungsstelle sollen in Angelegenheiten der Staatsanwältinnen und Staatsanwälte zwei der vom Hauptrichter- und Hauptstaatsanwaltsrat bestellten beisitzenden Mitglieder (§ 47 Absatz 6 Satz 3) Staatsanwältinnen und Staatsanwälte sein.

(2) Soweit der Gesamtstaatsanwaltsrat Aufgaben des Präsidialrats wahrnimmt, gilt § 61 entsprechend.

### § 94  
Nichtständiges Mitglied des Richterwahlausschusses

Für die Wahl des dem Landtag vorzuschlagenden staatsanwaltlichen Mitglieds des Richterwahlausschusses (§ 12 Absatz 1 Satz 3) gelten die Vorschriften über Wahlen (Kapitel 5) entsprechend.

### § 95   
Zuständigkeit der Richterdienstgerichte

In Disziplinarverfahren gegen Staatsanwältinnen und Staatsanwälte, auch soweit sie sich im Ruhestand befinden, entscheiden die Dienstgerichte für Richterinnen und Richter (§ 65).
Die Vorschriften für Richterinnen und Richter gelten mit Ausnahme von § 75 entsprechend, soweit in den folgenden Vorschriften nichts anderes bestimmt ist.
Die Befugnis gemäß § 73 Absatz 2 kann von der obersten Dienstbehörde auf die Generalstaatsanwältin oder den Generalstaatsanwalt übertragen werden.

### § 96  
Bestellung der nichtständigen beisitzenden Mitglieder der Richterdienstgerichte

(1) Die nichtständigen beisitzenden Mitglieder der Richterdienstgerichte müssen auf Lebenszeit berufene Staatsanwältinnen und Staatsanwälte sein.
Sie werden für fünf Geschäftsjahre von dem für Justiz zuständigen Mitglied der Landesregierung bestellt.
Die Berufsorganisationen der Staatsanwältinnen und Staatsanwälte und die Staatsanwaltsräte können Vorschläge für die Bestellung machen.

(2) Die Generalstaatsanwältin oder der Generalstaatsanwalt und die Leitende Oberstaatsanwältin oder der Leitende Oberstaatsanwalt bei dem Landgericht sowie ihre Vertreterinnen und Vertreter können nicht Mitglieder eines Richterdienstgerichts sein.

(3) Die oder der Dienstvorgesetzte darf in einem Verfahren gegen eine Staatsanwältin oder einen Staatsanwalt, die oder der ihrer oder seiner Dienstaufsicht untersteht, nicht als beisitzendes Mitglied mitwirken.

(4) § 67 Absatz 4, § 68 Absatz 3, § 71 Absatz 1 und § 72 gelten entsprechend.

### § 97  
Disziplinarmaßnahmen

Durch Disziplinarverfügung kann nur ein Verweis ausgesprochen werden.

### § 98   
Verfahren

Mit den Ermittlungen zur Durchführung des behördlichen Disziplinarverfahrens kann nur eine Staatsanwältin, ein Staatsanwalt, eine Richterin oder ein Richter beauftragt werden.

Kapitel 7  
Gemeinsame Gerichte
-------------------------------

### § 99   
Errichtung gemeinsamer Fachobergerichte der Länder Berlin und Brandenburg

Die Regelungen des Staatsvertrags über die Errichtung gemeinsamer Fachobergerichte der Länder Berlin und Brandenburg gehen den Bestimmungen dieses Gesetzes vor.

Kapitel 8  
Übergangs- und Schlussvorschriften
----------------------------------------------

### § 100  
Laufende Verfahren vor dem Dienstgericht und laufende Disziplinarverfahren

Die organisatorische Verbindung der Richterdienstgerichte mit dem Landgericht Cottbus und dem Brandenburgischen Oberlandesgericht erfolgt zum 1. September 2019.
Bis zu diesem Zeitpunkt bleibt die bisherige organisatorische Verbindung mit dem Verwaltungsgericht Frankfurt (Oder) und dem Oberverwaltungsgericht Berlin-Brandenburg bestehen; das Verfahren und die Besetzung der Richterdienstgerichte richten sich bis dahin nach den vor Inkrafttreten des Ersten Gesetzes zur Änderung des Brandenburgischen Richtergesetzes geltenden Rechtsvorschriften.
Die Vorschriften des Kapitels 4 Abschnitt 1 und 2 gelten ab dem 1.
September 2019 für die bei den Richterdienstgerichten anhängigen Verfahren unabhängig von dem Stand, in dem diese sich befinden.
Im Übrigen werden die am Tag des Inkrafttretens des Ersten Gesetzes zur Änderung des Brandenburgischen Richtergesetzes anhängigen gerichtlichen Verfahren nach den Bestimmungen des bis zum Inkrafttreten des genannten Gesetzes geltenden Rechts fortgeführt; dies gilt auch für die Wiederaufnahme von Disziplinarverfahren, die vor dem Inkrafttreten des genannten Gesetzes rechtskräftig abgeschlossen worden sind.
Eine mündliche Verhandlung, die in einem anhängigen Gerichtsverfahren vor Ablauf des Tages vor dem 1.
September 2019 geschlossen wurde, muss wieder eröffnet werden.

### § 101  
Evaluation

Die Landesregierung legt dem Landtag bis zum 31.
Oktober 2023 einen Bericht über die Auswirkungen des Ersten Gesetzes zur Änderung des Brandenburgischen Richtergesetzes und einen weiteren Reformbedarf vor.

### § 102  
Ausführung des Richterwahlgesetzes

Mitglied kraft Amtes im Richterwahlausschuss im Sinne des § 3 Absatz 3 des Richterwahlgesetzes ist das für Justiz zuständige Mitglied der Landesregierung.