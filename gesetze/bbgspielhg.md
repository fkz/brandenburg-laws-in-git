## Brandenburgisches Spielhallengesetz (BbgSpielhG)

#### § 1 Anwendungsbereich

(1) Dieses Gesetz regelt die sich aus dem Glücksspielstaatsvertrag 2021 ergebenden Vorgaben an die Zulassung und den Betrieb von Spielhallen.
Ziel ist es, den Bestand von Spielhallen zu begrenzen und ihr Erscheinungsbild so zu regeln, dass keine zusätzlichen Anreize von ihnen ausgehen, Spielerinnen und Spieler zu verantwortungsbewusstem Spiel angehalten werden und der Entstehung von Glücksspielsucht vorgebeugt wird.
Ergänzend gelten die nach § 2 Absatz 3 des Glücksspielstaatsvertrages 2021 anwendbaren Vorschriften.

(2) Eine Spielhalle im Sinne dieses Gesetzes ist ein Unternehmen oder Teil eines Unternehmens, das ausschließlich oder überwiegend der Aufstellung von Spielgeräten im Sinne des § 33c Absatz 1 Satz 1 der Gewerbeordnung oder der Veranstaltung anderer Spiele im Sinne des § 33d Absatz 1 Satz 1 der Gewerbeordnung dient.

(3) Für Gaststätten (Schank- und Speisewirtschaften und Beherbergungsbetriebe) und Wettannahmestellen der Buchmacher gelten, soweit sie Geld- oder Warenspielgeräte mit Gewinnmöglichkeit bereithalten, die nach § 2 Absatz 4 des Glücksspielstaatsvertrages 2021 anwendbaren Vorschriften sowie § 4 Absatz 3 und 4 des Glücksspielstaatsvertrages 2021 hinsichtlich der Geld- und Warenspielgeräte entsprechend.

#### § 2 Erlaubnis

(1) Die Betreiberin oder der Betreiber einer Spielhalle bedarf unbeschadet sonstiger Genehmigungserfordernisse für die Errichtung und den Betrieb einer Spielhalle einer Erlaubnis nach diesem Gesetz.
Insbesondere finden die Gewerbeordnung einschließlich der sich hieraus ergebenden gesonderten Genehmigungserfordernisse und die Spielverordnung sowie die auf diesen Rechtsgrundlagen erlassenen Vorschriften in der jeweils geltenden Fassung weiterhin Anwendung, soweit nicht in diesem Gesetz abweichende Bestimmungen enthalten sind.

(2) Die Erlaubnis nach Absatz 1 ist zu versagen, wenn

1.  ein Sozialkonzept gemäß § 5 nicht vorgelegt wird,
2.  die Errichtung der Spielhalle den Beschränkungen des § 3 widerspricht oder
3.  die Errichtung und der Betrieb der Spielhalle den Anforderungen des § 4 zuwiderlaufen würde.

(3) Die Erlaubnis ist auf maximal 15 Jahre zu befristen und unter dem Vorbehalt des Widerrufs zu erteilen.
Für die Erlaubnis ist eine Gebühr in Höhe von 2 500 Euro zu entrichten.
Mit der Gebühr sind alle Amtshandlungen im Zusammenhang mit der Erteilung der Erlaubnis und der Überwachung abgegolten.
Im Übrigen gelten die Vorschriften des Gebührengesetzes für das Land Brandenburg.
Die Erlaubnis kann auch nachträglich mit Nebenbestimmungen versehen werden.

(4) Die Erlaubnis kann unbeschadet der Widerrufsgründe nach § 49 des Verwaltungsverfahrensgesetzes in Verbindung mit § 1 Absatz 1 Satz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg auch widerrufen werden, wenn

1.  nachträglich Tatsachen eintreten, die eine Versagung der Erlaubnis nach Absatz 2 rechtfertigen würden, oder
2.  die Betreiberin oder der Betreiber einer Spielhalle in schwerwiegender Weise gegen Verpflichtungen verstößt, die ihm nach diesem Gesetz sowie der erteilten Erlaubnis obliegen.

(5) Die Betreiberin oder der Betreiber einer Spielhalle ist verpflichtet, jede Änderung der für die Erlaubniserteilung maßgeblichen Tatsachen der zuständigen Erlaubnisbehörde unverzüglich anzuzeigen.

(6) Die Betreiberin oder der Betreiber einer Spielhalle hat den Beauftragten der zuständigen Erlaubnisbehörde auf Verlangen die für die Überwachung des Geschäftsbetriebs erforderlichen mündlichen und schriftlichen Auskünfte unentgeltlich zu erteilen.
Die Beauftragten sind befugt, zum Zwecke der Überwachung Grundstücke und Geschäftsräume der Betreiberin oder des Betreibers einer Spielhalle während der üblichen Geschäftszeit zu betreten, dort Prüfungen und Besichtigungen vorzunehmen, sich Unterlagen vorlegen zu lassen, soweit diese zur Ausführung dieses Gesetzes erforderlich sind, und in diese Einsicht zu nehmen.

#### § 3 Beschränkungen von Spielhallen

(1) Zwischen Spielhallen ist ein Mindestabstand von 500 Metern Luftlinie einzuhalten.

(2) Die Erteilung einer Erlaubnis für eine Spielhalle, die in einem baulichen Verbund mit einer oder weiteren Spielhallen, insbesondere in einem gemeinsamen Gebäude oder Gebäudekomplex, untergebracht ist, ist ausgeschlossen.

(3) Der Betrieb einer Spielhalle in unmittelbarer Nähe zu einer Lottoannahmestelle oder einer Wettvermittlungsstelle läuft den Zielen des § 1 des Glücksspielstaatsvertrages 2021 zuwider und ist unzulässig.

#### § 4 Anforderungen an die Ausgestaltung und den Betrieb von Spielhallen

(1) Als Bezeichnung des Unternehmens ist lediglich das Wort „Spielhalle“ zulässig.

(2) Eine Spielhalle darf von außen nicht einsehbar sein.

(3) Von der äußeren Gestaltung der Spielhalle und in ihrer unmittelbaren Nähe darf keine Werbung für den Spielbetrieb oder die in der Spielhalle angebotenen Spiele ausgehen oder durch eine besonders auffällige Gestaltung ein zusätzlicher Anreiz für den Spielbetrieb geschaffen werden.

(4) Die Sperrzeit für Spielhallen beginnt um 3 Uhr und endet um 9 Uhr.
Außerdem ist am Karfreitag von 0 Uhr bis Karsamstag 9 Uhr, am Volkstrauertag und am Totensonntag von 3 Uhr bis zum nächsten Tag 9 Uhr und am Vortag des 1. Weihnachtsfeiertages (Heiliger Abend) von 13 Uhr bis zum 1. Weihnachtsfeiertag 9 Uhr das Spielen verboten.

(5) Die unentgeltliche Abgabe von Speisen und Getränken ist in Spielhallen verboten.

(6) Die Betreiberin oder der Betreiber der Spielhalle ist verpflichtet, die Spielerinnen und Spieler zu verantwortungsbewusstem Spiel anzuhalten, den Jugend- und Spielerschutz sicherzustellen und der Entstehung von Spielsucht vorzubeugen.
Zu diesem Zweck hat sie oder er insbesondere

1.  sicherzustellen, dass Minderjährige keinen Zutritt zur Spielhalle haben,
2.  ein Sozialkonzept gemäß § 5 zu entwickeln und umzusetzen,
3.  Spielerinnen und Spieler sowohl vor Spielbeginn als auch während des Aufenthaltes in der Spielhalle gemäß § 6 aufzuklären,
4.  sicherzustellen, dass in der Spielhalle stets eine Aufsichtsperson anwesend ist, die die Einhaltung der Vorgaben dieses Gesetzes überwacht,
5.  sicherzustellen, dass das Personal der Spielhalle vom Spiel ausgeschlossen ist und
6.  sicherzustellen, dass die Vergütung des Personals nicht in Abhängigkeit vom Umsatz berechnet wird.

#### § 5 Sozialkonzept

(1) In dem Sozialkonzept ist darzulegen, mit welchen Maßnahmen den sozialschädlichen Auswirkungen des Glücksspiels vorgebeugt werden soll und wie diese behoben werden sollen.
Das Sozialkonzept muss mindestens folgenden Inhalt haben:

1.  Benennung der oder des Beauftragten für das Sozialkonzept bei der Erlaubnisinhaberin oder dem Erlaubnisinhaber sowie zusätzlich die Benennung einer verantwortlichen Person vor Ort;
2.  Berücksichtigung der Anliegen nach § 4 Absatz 6 Satz 1 in der internen Unternehmenskommunikation, bei der Werbung sowie beim Sponsoring;
3.  regelmäßige Personalschulungen für das Aufsichtspersonal der Spielhalle, für die Erlaubnisinhaberin oder den Erlaubnisinhaber sowie für die Beauftragten nach Nummer 1 unter Einbindung suchtfachlich sowie pädagogisch qualifizierter Dritter mit folgenden Mindestinhalten:
    1.  Rechtsgrundlagen zum Jugend- und Spielerschutz,
    2.  Kenntnisse zur Glücksspielsucht einschließlich anbieterunabhängiger Hilfeangebote und
    3.  Vermittlung von Handlungskompetenzen insbesondere in der Früherkennung auffälligen Spielverhaltens und Kommunikation mit Spielern;
4.  Umsetzung des Jugendschutzes und der Identitätskontrolle einschließlich des Abgleichs mit der Sperrdatei;
5.  Aufklärung nach § 6 einschließlich des Verweises auf die Telefonberatung mit bundesweit einheitlicher Telefonnummer und der Bereitstellung von Informationen mit folgenden Mindestinhalten:
    1.  Suchtrisiko und mögliche negative Folgen,
    2.  Teilnahmeverbot Minderjähriger,
    3.  Hinweise zu verantwortungsbewusstem Spielverhalten,
    4.  Möglichkeit der Einschätzung des eigenen Spielverhaltens und der persönlichen Gefährdung,
    5.  Hinweise zu anbieterunabhängigen Hilfeangeboten und
    6.  Sperrverfahren;
6.  Früherkennung unter Einbeziehung suchtwissenschaftlicher Erkenntnisse;
7.  Frühintervention und Information über regionale Suchtberatungsstellen sowie andere anbieterunabhängige Hilfeangebote;
8.  Umsetzung der Sperrverfahren mit Selbst- und Fremdsperren;
9.  kontinuierliche Dokumentation der durchgeführten Maßnahmen zum Zweck von Rückschlüssen auf die Auswirkungen des angebotenen Glücksspiels, auf das Spielverhalten und auf die Entstehung von Glücksspielsucht sowie zur Beurteilung des Erfolgs der durchgeführten Maßnahmen zum Jugend- und Spielerschutz;
10.  Berichterstattung unter Zugrundelegung der Dokumentation nach Nummer 9 alle zwei Jahre gegenüber der zuständigen Behörde.

(2) Das für Gesundheit zuständige Mitglied der Landesregierung bestimmt mit Zustimmung der für Inneres sowie für Wirtschaft zuständigen Mitglieder der Landesregierung durch Rechtsverordnung das Nähere über Inhalt und Form des Sozialkonzepts nach Absatz 1, die Häufigkeit von Schulungen nach Absatz 1 Satz 2 Nummer 3, über die Anerkennung der Schulungsangebote nach Absatz 1 Satz 2 Nummer 3 sowie die zuständige Behörde nach Absatz 1 Satz 2 Nummer 10.

#### § 6 Aufklärung

(1) Die Erlaubnisinhaberin oder der Erlaubnisinhaber hat den Spielerinnen und Spielern vor der Spielteilnahme spielrelevante Informationen zur Verfügung zu stellen sowie über die Suchtrisiken der angebotenen Glücksspiele, das Verbot der Teilnahme Minderjähriger und Möglichkeiten der Beratung und Therapie aufzuklären.
Als spielrelevante Informationen kommen insbesondere in Betracht:

1.  alle Kosten, die mit der Teilnahme veranlasst sind,
2.  die Höhe aller Gewinne,
3.  wann und wo alle Gewinne veröffentlicht werden,
4.  der Prozentsatz der Auszahlung für Gewinne vom Einsatz (Auszahlungsquote),
5.  Informationen zu den Gewinn- und Verlustwahrscheinlichkeiten,
6.  Annahmeschluss der Teilnahme,
7.  das Verfahren, nach dem der Gewinner ermittelt wird, insbesondere die Information über den Zufallsmechanismus, der der Generierung der zufallsabhängigen Spielergebnisse zugrunde liegt,
8.  wie die Gewinne zwischen den Gewinnern aufgeteilt werden,
9.  die Ausschlussfrist, bis wann Gewinner Anspruch auf ihren Gewinn erheben müssen,
10.  der Name der Erlaubnisinhaberin oder des Erlaubnisinhabers sowie seine Kontaktdaten (Anschrift, E-Mail, Telefon),
11.  soweit vorhanden, die Handelsregisternummer,
12.  wie der Spieler Beschwerden vorbringen kann und
13.  das Datum der ausgestellten Erlaubnis.

Informationen über Höchstgewinne sind mit der Aufklärung über die Wahrscheinlichkeit von Gewinn und Verlust zu verbinden.
Spielerinnen oder Spieler und Behörden müssen leichten Zugang zu diesen Informationen haben.

(2) Spielscheine, Spielquittungen und vergleichbare Bescheinigungen müssen Hinweise auf die von dem jeweiligen Glücksspiel ausgehende Suchtgefahr und Hilfsmöglichkeiten enthalten.

#### § 7 Spielersperre

(1) Die Betreiberin oder der Betreiber der Spielhalle ist verpflichtet, spielwillige Personen durch Kontrolle eines amtlichen Ausweises oder eine vergleichbare Identitätskontrolle zu identifizieren und einen Abgleich mit der Sperrdatei nach § 23 des Glücksspielstaatsvertrages 2021 durchzuführen.
Sie oder er hat sicherzustellen, dass gesperrte Spielerinnen und Spieler nicht an Glücksspielen in der Spielhalle teilnehmen.
Der Abgleich ist bei jedem Betreten der Spielhalle und im Übrigen vor dem ersten Spiel während eines Aufenthaltes in der Spielhalle vorzunehmen.

(2) Die Betreiberin oder der Betreiber sowie das Personal der Spielhalle dürfen nicht auf gesperrte Spielerinnen und Spieler einwirken, einen Antrag auf Entsperrung zu stellen.
Spielerinnen und Spielern, deren Spielersperre aufgehoben worden ist, dürfen keine Vorteile wie Boni oder Rabatte gewährt werden.

(3) Der Anschluss an das Sperrsystem und die Nutzung des Sperrsystems ist für die Betreiberin oder den Betreiber der Spielhalle kostenpflichtig.
Das Stellen eines Sperrantrages oder eines Antrages auf Beendigung der Sperre ist kostenfrei.

#### § 8 Eintragung, Dauer und Beendigung der Sperre

(1) Die Betreiberin oder der Betreiber der Spielhalle sperrt Personen, die dies beantragen (Selbstsperre) oder von denen sie aufgrund der Wahrnehmung ihres Personals oder aufgrund von Meldungen Dritter wissen oder aufgrund sonstiger tatsächlicher Anhaltspunkte annehmen müssen, dass sie spielsuchtgefährdet oder überschuldet sind, ihren finanziellen Verpflichtungen nicht nachkommen oder Spieleinsätze riskieren, die in keinem Verhältnis zu ihrem Einkommen oder Vermögen stehen (Fremdsperre).

(2) Vor Eintragung einer Fremdsperre ist der betroffenen Person Gelegenheit zur Stellungnahme zu geben.
Die Gelegenheit sowie eine etwaige Stellungnahme sind zu dokumentieren.

(3) Die Betreiberin oder der Betreiber einer Spielhalle hat folgende Daten in die Sperrdatei einzutragen:

1.  Familiennamen, Vornamen, Geburtsnamen,
2.  Aliasnamen, verwendete Falschnamen,
3.  Geburtsdatum,
4.  Geburtsort,
5.  Anschrift,
6.  Lichtbilder,
7.  Grund der Sperre,
8.  Dauer der Sperre und
9.  meldende Stelle.

Ein Eintrag ist auch vorzunehmen, wenn nicht alle Daten erhoben werden können.

(4) Die Betreiberin oder der Betreiber der Spielhalle teilt der betroffenen Person unverzüglich in Textform mit, dass für sie eine Sperre eingetragen ist und informiert sie über das Verfahren zur Beendigung der Sperre nach Absatz 7.

(5) Die Sperre beträgt mindestens ein Jahr, es sei denn, die eine Selbstsperre beantragende Person beantragt einen abweichenden Zeitraum, der jedoch drei Monate nicht unterschreiten darf.
Wird eine kürzere Dauer als drei Monate angegeben, gilt dies als Angabe von drei Monaten.

(6) Die Betreiberin oder der Betreiber der Spielhalle hat die Sperranträge bei Selbstsperren und die bei Fremdsperren anfallenden Unterlagen aufzubewahren.
Bei Geschäftsaufgabe, Fusionen, Insolvenz oder dem Vorliegen sonstiger Gründe, die die weitere Aufbewahrung dieser Unterlagen durch die Betreiberin oder den Betreiber der Spielhalle unmöglich machen, hat dieser sämtliche die Sperre betreffenden Unterlagen der für die Führung der Sperrdatei zuständigen Behörde auszuhändigen.

(7) Eine Aufhebung der Sperre ist nur auf schriftlichen Antrag der gesperrten Person möglich.
Dies gilt auch dann, wenn bei Beantragung der Sperre für die Laufzeit der Sperre eine bestimmte Frist genannt wurde.
Der Antrag kann frühestens nach Ablauf der Mindestdauer nach Absatz 5 gestellt werden.
Wird kein Antrag nach Satz 1 gestellt, endet die Sperre nicht.
Die Aufhebung der Sperre wird nach ihrer Eintragung in der Sperrdatei, jedoch im Fall einer Selbstsperre nicht vor Ablauf einer Woche und im Fall einer Fremdsperre nicht vor Ablauf eines Monats nach Eingang des Antrages auf Aufhebung der Sperre bei der für die Führung der Sperrdatei zuständigen Behörde wirksam.
Der Antragstellerin oder dem Antragsteller ist die Entsperrung mitzuteilen.

(8) Die Betreiberin oder der Betreiber der Spielhalle ist verpflichtet, Anträge auf Aufhebung der Sperre an die für die Führung der Sperrdatei zuständige Behörde weiterzuleiten.

#### § 9 Zuständige Behörden

(1) Zuständige Erlaubnisbehörden nach § 2 dieses Gesetzes sind die örtlichen Ordnungsbehörden.

(2) Das Land erstattet den nach Absatz 1 zuständigen Behörden die mit der Anwendung dieses Gesetzes verbundenen notwendigen Kosten einschließlich der Personal- und Sachkosten, soweit dieser finanzielle Aufwand nicht durch Gebühren nach § 2 Absatz 3 Satz 2 ausgeglichen werden kann.
Der eine Gebührenerhebung übersteigende, nachgewiesene finanzielle Aufwand wird den zuständigen Behörden nach Ablauf eines Haushaltsjahres vom Land durch das für Wirtschaft zuständige Mitglied der Landesregierung auf Antrag erstattet.

#### § 10 Ordnungswidrigkeiten

(1) Ordnungswidrig handelt, wer vorsätzlich oder fahrlässig entgegen

1.  § 2 Absatz 1 Satz 1 eine Spielhalle ohne Erlaubnis errichtet und betreibt,
2.  § 2 Absatz 3 Satz 5 Nebenbestimmungen nicht beachtet,
3.  § 2 Absatz 5 Änderungen der für die Erlaubniserteilung maßgeblichen Tatsachen nicht unverzüglich anzeigt,
4.  § 4 Absatz 1 ein anderes Wort als „Spielhalle“ für das Unternehmen wählt,
5.  § 4 Absatz 2 den Einblick von außen ermöglicht,
6.  § 4 Absatz 3 in unmittelbarer Nähe der Spielhalle Werbung für den Spielbetrieb oder die in der Spielhalle angebotenen Spiele betreibt oder eine besonders auffällige Gestaltung der Spielhalle vornimmt,
7.  § 4 Absatz 4 die Sperrzeit oder die spielfreien Tage nicht beachtet,
8.  § 4 Absatz 5 unentgeltlich Speisen oder Getränke abgibt oder zulässt, dass unentgeltlich Speisen oder Getränke abgegeben werden,
9.  § 4 Absatz 6 Satz 2 Nummer 1 nicht sicherstellt, dass Minderjährige keinen Zutritt zur Spielhalle haben,
10.  § 4 Absatz 6 Satz 2 Nummer 2 kein Sozialkonzept entwickelt oder umsetzt, in dem dargelegt ist, mit welchen Maßnahmen den sozialschädlichen Auswirkungen des Glücksspiels vorgebeugt werden soll und wie diese behoben werden,
11.  § 4 Absatz 6 Satz 2 Nummer 3 den Spielerinnen und Spielern vor der Spielteilnahme spielrelevante Informationen nicht zur Verfügung stellt oder über die Suchtrisiken der angebotenen Glücksspiele, das Verbot der Teilnahme Minderjähriger und Möglichkeiten der Beratung und Therapie nicht aufklärt,
12.  § 4 Absatz 6 Satz 2 Nummer 4 nicht sicherstellt, dass in der Spielhalle stets eine Aufsichtsperson anwesend ist, die die Einhaltung der Vorgaben dieses Gesetzes überwacht,
13.  § 4 Absatz 6 Satz 2 Nummer 5 nicht sicherstellt, dass das Personal der Spielhalle vom Spiel ausgeschlossen ist,
14.  § 4 Absatz 6 Satz 2 Nummer 6 nicht sicherstellt, dass die Vergütung des Personals nicht in Abhängigkeit vom Umsatz berechnet wird,
15.  § 6 Absatz 1 Satz 3 und 4 Informationen über Höchstgewinne nicht mit der Aufklärung über die Wahrscheinlichkeit von Gewinn und Verlust verbindet oder keinen leichten Zugang hierzu ermöglicht,
16.  § 6 Absatz 2 Spielscheine, Spielquittungen und vergleichbare Bescheinigungen nicht mit Hinweisen auf die von dem jeweiligen Glücksspiel ausgehenden Suchtgefahren und Hilfsmöglichkeiten versieht,
17.  § 7 Absatz 1 Satz 1 spielwillige Personen nicht identifiziert oder keinen Abgleich mit der Sperrdatei vornimmt,
18.  § 7 Absatz 1 Satz 2 nicht sicherstellt, dass gesperrte Spieler nicht an Glücksspielen in der Spielhalle teilnehmen,
19.  § 7 Absatz 2 Satz 1 auf gesperrte Spieler einwirkt,
20.  § 7 Absatz 2 Satz 2 Vorteile gewährt,
21.  § 8 Absatz 1 eine Sperre nicht vornimmt,
22.  § 8 Absatz 2 Satz 1 der betroffenen Person keine Gelegenheit zur Stellungnahme gibt,
23.  § 8 Absatz 2 Satz 2 keine Dokumentation vornimmt,
24.  § 8 Absatz 3 die genannten Daten nicht in die Sperrdatei einträgt,
25.  § 8 Absatz 4 die Eintragung einer Sperre nicht mitteilt oder nicht über das Verfahren zur Beendigung der Sperre informiert,
26.  § 8 Absatz 6 Satz 1 Unterlagen nicht aufbewahrt,
27.  § 8 Absatz 6 Satz 2 Unterlagen nicht aushändigt,
28.  § 8 Absatz 8 Anträge nicht weiterleitet.

(2) Sachlich zuständige Verwaltungsbehörde im Sinne des § 36 Absatz 1 Nummer 1 des Gesetzes über Ordnungswidrigkeiten ist die für die Erteilung der Erlaubnis zuständige Behörde.

(3) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu 50 000 Euro geahndet werden.

#### § 11 Übergangs- und Härtefallregelung

(1) Im Fall des § 3 erhält nach Ablauf von fünf Jahren nach Inkrafttreten des Glücksspielstaatsvertrages vom 15. Dezember 2011 grundsätzlich diejenige Betreiberin oder derjenige Betreiber einer Spielhalle die Erlaubnis nach § 2 Absatz 1 unter Berücksichtigung der Ziele des § 1 des Glücksspielstaatsvertrages vom 15.
Dezember 2011 in Verbindung mit § 2 Absatz 2 dieses Gesetzes, die oder der über die älteste Erlaubnis nach § 33i der Gewerbeordnung verfügt.
Bei zeitgleich erteilten Erlaubnissen ist eine Auswahlentscheidung unter Abwägung der Gesamtumstände zu treffen.

(2) Stellt in den Fällen des Absatzes 1 die Nichterteilung einer Erlaubnis nach § 2 Absatz 1 insbesondere unter Abwägung der konkreten persönlichen Umstände eine unbillige Härte dar, kann eine Befreiung von der Erfüllung einzelner Anforderungen des § 24 Absatz 2 des Glücksspielstaatsvertrages vom 15.
Dezember 2011 sowie des § 3 dieses Gesetzes für einen angemessenen Zeitraum zugelassen werden.

(3) Abweichend von § 25 Absatz 2 des Glücksspielstaatsvertrages 2021 kann für am 1. Januar 2020 bestehende Spielhallen, die in einem baulichen Verbund mit weiteren Spielhallen stehen, für bis zu drei Spielhallen je Gebäude oder Gebäudekomplex auf gemeinsamen Antrag der Betreiberinnen oder Betreiber eine Erlaubnis erteilt werden, wenn

1.  mindestens alle Spielhallen von einer akkreditierten Prüforganisation zertifiziert worden sind und die Zertifizierung in regelmäßigen Abständen, mindestens alle zwei Jahre, wiederholt wird,
2.  die Betreiber über einen aufgrund einer Unterrichtung mit Prüfung erworbenen Sachkundenachweis verfügen und
3.  das Personal der Spielhallen besonders geschult wird.

Die Erlaubnis ist zu befristen.
Sie kann bis zum Ablauf des 31. Dezember 2025 erteilt werden.
Gegenstand der Zertifizierung nach Satz 1 Nummer 1 sind die Einhaltung der gesetzlichen Anforderungen, die Durchführung der Maßnahmen des Sozialkonzepts nach § 6 des Glücksspielstaatsvertrages 2021 und die besondere Schulung des Personals nach Satz 1 Nummer 3.
Prüforganisationen sind zur Zertifizierung der Spielhallen berechtigt, wenn sie hinsichtlich der zur Beurteilung der in Satz 4 genannten Sachverhalte erforderlichen Sachkunde und ihrer organisatorischen, personellen und finanziellen Unabhängigkeit von Spielhallenbetreibern, Automatenaufstellern und deren Interessensverbänden bei der nationalen Akkreditierungsstelle gemäß ISO/IEC 17065 akkreditiert sind.
Die Erlaubnis nach Satz 1 erlischt im Fall des Wechsels einer Betreiberin oder eines Betreibers für die betroffene Spielhalle unwiderruflich.

(4) Das für Gesundheit zuständige Mitglied der Landesregierung bestimmt mit Zustimmung der für Inneres sowie für Wirtschaft zuständigen Mitglieder der Landesregierung durch Rechtsverordnung das nähere Verfahren nach Absatz 3.

#### § 12 Einschränkung von Grundrechten

Durch dieses Gesetz wird das Grundrecht der Unverletzlichkeit der Wohnung (Artikel 13 des Grundgesetzes und Artikel 15 der Verfassung des Landes Brandenburg), das Grundrecht der Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) und das Grundrecht auf informationelle Selbstbestimmung (Artikel 11 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.