## Besoldungsgesetz für das Land Brandenburg (Brandenburgisches Besoldungsgesetz - BbgBesG)

Inhaltsübersicht
----------------

### Abschnitt 1  
Allgemeine Vorschriften

[§ 1 Geltungsbereich](#1)  
[§ 2  Regelung durch Gesetz](#2)  
[§ 3  Anspruch auf Besoldung](#3)  
[§ 4  Weitergewährung der Besoldung bei Versetzung in den einstweiligen Ruhestand oder bei Abwahl von Wahlbeamtinnen und Wahlbeamten auf Zeit](#4)  
[§ 5  Besoldung bei mehreren Hauptämtern](#5)  
[§ 6  Besoldung bei Teilzeitbeschäftigung](#6)  
[§ 7  Besoldung bei begrenzter Dienstfähigkeit](#7)  
[§ 8  Kürzung der Besoldung bei Gewährung einer Versorgung durch eine zwischenstaatliche oder überstaatliche Einrichtung](#8)  
[§ 9  Verlust der Besoldung bei schuldhaftem Fernbleiben vom Dienst](#9)  
[§ 10 Anrechnung anderer Einkünfte auf die Besoldung](#10)  
[§ 11 Anrechnung von Sachbezügen auf die Besoldung](#11)  
[§ 12 Abtretung von Bezügen, Verpfändung, Aufrechnungs- und Zurückbehaltungsrecht, Verjährung](#12)  
[§ 13 Rückforderung von Bezügen](#13)  
[§ 14 Anpassung der Besoldung](#14)  
[§ 15 Versorgungsrücklage](#15)  
[§ 16 Dienstlicher Wohnsitz](#16)  
[§ 17 Aufwandsentschädigungen](#17)

### Abschnitt 2  
Grundgehalt

#### Unterabschnitt 1  
Allgemeine Grundsätze

[§ 18 Grundsatz der funktionsgerechten Besoldung](#18)  
[§ 19 Bestimmung des Grundgehalts nach dem Amt](#19)

#### Unterabschnitt 2  
Vorschriften für Beamtinnen und Beamte der  Besoldungsordnungen A und B

[§ 20 Besoldungsordnungen A und B](#20)  
[§ 21 Hauptamtliche kommunale Wahlbeamtinnen und Wahlbeamte auf Zeit](#21)  
[§ 22 Eingangsämter für Beamtinnen und Beamte](#22)  
[§ 23 Eingangsämter für Beamtinnen und Beamte in Sonderlaufbahnen](#23)  
[§ 24 Obergrenzen für Beförderungsämter](#24)  
[§ 25 Bemessung des Grundgehalts](#25)  
[§ 26 Berücksichtigungsfähige Zeiten](#26)  
[§ 27 Nicht zu berücksichtigende Zeiten](#27)  
[§ 28 Öffentlich-rechtliche Dienstherren](#28)

#### Unterabschnitt 3  
Vorschriften für Professorinnen und Professoren sowie für hauptberufliche Leiterinnen und Leiter und Mitglieder von Leitungsgremien an Hochschulen

[§ 29 Besoldungsordnung W](#29)  
[§ 30 Leistungsbezüge](#30)  
[§ 31 Berufungs- und Bleibe-Leistungsbezüge](#31)  
[§ 32 Besondere Leistungsbezüge](#32)  
[§ 33 Funktions-Leistungsbezüge](#33)  
[§ 34 Höhe der Leistungsbezüge](#34)  
[§ 35 Ruhegehaltfähigkeit von Leistungsbezügen](#35)  
[§ 36 Forschungs- und Lehrzulage](#36)  
[§ 37 Evaluation und Verordnungsermächtigungen](#37)

#### Unterabschnitt 4  
Vorschriften für Richterinnen, Richter, Staatsanwältinnen und Staatsanwälte

[§ 38 Besoldungsordnung R](#38)  
[§ 39 Bemessung des Grundgehalts](#39)

### Abschnitt 3  
Familienzuschlag

[§ 40 Grundlage und Höhe des Familienzuschlags ab 1.
Januar 2015](#40)

### Abschnitt 4  
Sonstige Besoldungsbestandteile, Ausgleich bei Verringerung der Dienstbezüge

#### Unterabschnitt 1  
Sonstige Besoldungsbestandteile

[§ 41 Amtszulagen und Stellenzulagen](#41)  
[§ 42 Zulage für Lehrkräfte mit besonderen Funktionen](#42)  
[§ 43 Leistungsprämien und Leistungszulagen](#43)  
[§ 44 Zulage für die Wahrnehmung eines höherwertigen Amtes](#44)  
[§ 45 Erschwerniszulagen](#45)  
[§ 46 Mehrarbeitsvergütung](#46)  
[§ 47 Vergütung für Beamtinnen und Beamte im Vollstreckungsdienst](#47)  
[§ 48 Sonderzuschläge zur Sicherung der Funktions- und Wettbewerbsfähigkeit](#48)  
[§ 48a Zuschlag bei Hinausschieben des Eintritts in den Ruhestand](#48a)  
[§ 48b Attraktivitäts-Zuschlag für die Jahre 2017 bis 2020](#48b)  
[§ 48c Zuschlag zur Gewinnung von IT-Fachkräften](#48c)  
[§ 49 Andere Zulagen, Prämien und Vergütungen](#49)

#### Unterabschnitt 2  
Ausgleich bei Verringerungen der Dienstbezüge

[§ 50 Besoldungsanspruch bei Verleihung eines Amtes mit geringerem Grundgehalt](#50)  
[§ 51 Ausgleichszulagen bei Wegfall von Stellenzulagen](#51)

### Abschnitt 5  
Auslandsbesoldung

[§ 52 Auslandsbesoldung](#52)

### Abschnitt 6  
Anwärterbezüge

[§ 53 Anwärterbezüge](#53)  
[§ 54 Anwärterbezüge nach Ablegung der Laufbahnprüfung](#54)  
[§ 55 Anwärtergrundbetrag](#55)  
[§ 56 Anwärtersonderzuschläge](#56)  
[§ 57 Anrechnung anderer Einkünfte](#57)  
[§ 58 Kürzung der Anwärterbezüge](#58)

### Abschnitt 7  
Vermögenswirksame Leistungen

[§ 59 Vermögenswirksame Leistungen](#59)  
[§ 60 Höhe, Fälligkeit und Anlage der vermögenswirksamen Leistungen](#60)

### Abschnitt 8  
Übergangs- und Schlussbestimmungen

#### Unterabschnitt 1  
Übergangsbestimmungen zu diesem Gesetz

[§ 61 Anwendung dieses Gesetzes auf vorhandene Beamtinnen, Beamte, Richterinnen und Richter sowie Versorgungsempfängerinnen und Versorgungsempfänger](#61)  
[§ 62 Überführung in die Ämter der Besoldungsordnungen A, B, W und R](#62)  
[§ 63 Zuordnung der vorhandenen Beamtinnen, Beamten, Richterinnen und Richter der Besoldungsordnungen A und R zu den Stufen der Grundgehälter](#63)  
[§ 63a Überführung vorhandener Beamtinnen und Beamter des Justizwachtmeisterdienstes, des mittleren Steuerverwaltungsdienstes, des mittleren Polizeivollzugsdienstes, des mittleren allgemeinen Vollzugsdienstes, des Werkdienstes und des Krankenpflegedienstes bei den Justizvollzugsanstalten und bestimmter Lehrkräfte](#63a)  
[§ 64 Übergangsregelungen zur Altersteilzeit](#64)  
[§ 65 Übergangsregelungen zur Professorenbesoldung](#65)  
[§ 66 Übergangsregelungen zum Familienzuschlag](#66)  
[§ 67 Sonstige Übergangsvorschriften](#67)

#### Unterabschnitt 2  
Schlussbestimmungen

[§ 68 Künftig wegfallende Ämter](#68)  
[§ 69 Besondere Vorschriften für den Bereich der Sozialversicherung](#69)  
[§ 70 Weitergeltung von Rechtsvorschriften und Zuständigkeitsregelungen](#70)

[Anlage 1 Besoldungsordnungen A und B](#71)  
[Anlage 2 Besoldungsordnung W](#72)  
[Anlage 3 Besoldungsordnung R](#73)  
[Anlage 4 Grundgehaltssätze für die Besoldungsgruppen der Besoldungsordnungen A, B, W und R](#74)  
[Anlage 5 Grundgehaltssätze, Amtszulagen, Stellenzulagen, Zulagen, Vergütungen für Besoldungsgruppen der Besoldungsordnung C](#74)  
[Anlage 6 Familienzuschlag](#74)  
[Anlage 7 Anwärtergrundbetrag](#74)  
[Anlage 8 Amtszulagen, Stellenzulagen, Zulagen, Vergütungen](#74)

### Abschnitt 1   
Allgemeine Vorschriften

##### § 1   
Geltungsbereich

(1) Dieses Gesetz regelt die Besoldung der Beamtinnen, Beamten, Richterinnen und Richter des Landes, der Beamtinnen und Beamten der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts; ausgenommen sind Ehrenbeamtinnen, Ehrenbeamte sowie ehrenamtliche Richterinnen und Richter.

(2) Dieses Gesetz gilt nicht für die öffentlich-rechtlichen Religionsgemeinschaften und ihre Verbände.

(3) Zur Besoldung gehören folgende Dienstbezüge:

1.  Grundgehalt,
2.  Leistungsbezüge für Professorinnen und Professoren sowie für hauptberufliche Leiterinnen und Leiter und Mitglieder von Leitungsgremien an Hochschulen,
3.  Familienzuschlag,
4.  Zulagen,
5.  Vergütungen,
6.  Auslandsbesoldung.

(4) Zur Besoldung gehören ferner folgende sonstige Bezüge:

1.  Leistungsprämien und Leistungszulagen,
2.  Anwärterbezüge,
3.  vermögenswirksame Leistungen.

##### § 2   
Regelung durch Gesetz

(1) Die Besoldung der Beamtinnen, Beamten, Richterinnen und Richter wird durch Gesetz geregelt.

(2) Zusicherungen, Vereinbarungen und Vergleiche, die einer Beamtin, einem Beamten, einer Richterin oder einem Richter eine höhere als die gesetzlich zustehende Besoldung verschaffen sollen, sind unwirksam.
Das Gleiche gilt für Versicherungsverträge, die zu diesem Zweck abgeschlossen werden.

(3) Beamtinnen, Beamte, Richterinnen und Richter können auf die gesetzlich zustehende Besoldung weder ganz noch teilweise verzichten; ausgenommen sind die vermögenswirksamen Leistungen.

##### § 3   
Anspruch auf Besoldung

(1) Beamtinnen, Beamte, Richterinnen und Richter haben Anspruch auf Besoldung.
Der Anspruch entsteht mit dem Tag, an dem ihre Ernennung, Versetzung, Übernahme oder ihr Übertritt in den Dienst eines der in § 1 Absatz 1 genannten Dienstherren wirksam wird.
Der Anspruch der direkt gewählten Beamtinnen und Beamten auf Zeit entsteht mit dem Tag nach Annahme der Wahl, jedoch nicht vor Ablauf der Amtszeit der bisherigen Amtsinhaberin oder des bisherigen Amtsinhabers.
Erfolgt die rückwirkende Einweisung in eine Planstelle, so entsteht der Anspruch mit dem Tag, der in der Einweisungsverfügung bestimmt ist.
Wird ein Amt aufgrund einer Regelung nach § 21 eingestuft, so entsteht der Anspruch mit der Maßnahme, die der Einweisungsverfügung entspricht.

(2) Der Anspruch auf Besoldung endet mit Ablauf des Tages, an dem eine Beamtin, ein Beamter, eine Richterin oder ein Richter aus dem Dienstverhältnis ausscheidet, soweit gesetzlich nichts anderes bestimmt ist.

(3) Besteht der Anspruch auf Besoldung nicht für einen vollen Kalendermonat, so wird nur der Teil der Bezüge gezahlt, der auf den Anspruchszeitraum entfällt, soweit gesetzlich nichts anderes bestimmt ist.

(4) Die Besoldung wird monatlich im Voraus gezahlt, soweit nichts anderes bestimmt ist.

(5) Für Zahlungen nach diesem Gesetz haben die Empfängerinnen und Empfänger auf Verlangen der zuständigen Behörde ein Konto innerhalb eines Mitgliedstaats der Europäischen Union anzugeben oder einzurichten, auf das die Überweisung erfolgen kann.
Übermittlungskosten sind von den Empfängerinnen und Empfängern zu tragen.
Eine Auszahlung auf andere Weise kann nur zugestanden werden, wenn die Einrichtung oder Benutzung eines Kontos aus wichtigem Grund nicht zugemutet werden kann.

(6) Werden Bezüge nach dem Tag der Fälligkeit gezahlt, so besteht kein Anspruch auf Verzugszinsen.

(7) Bei der Berechnung von Bezügen nach § 1 sind die sich ergebenden Bruchteile eines Cents unter 0,5 abzurunden und Bruchteile von 0,5 und mehr aufzurunden.
Zwischenrechnungen werden jeweils auf zwei Dezimalstellen durchgeführt.
Jeder Bezügebestandteil ist einzeln zu runden.

##### § 4   
Weitergewährung der Besoldung bei Versetzung in den einstweiligen Ruhestand oder bei Abwahl von Wahlbeamtinnen und Wahlbeamten auf Zeit

(1) Beamtinnen, Beamte, Richterinnen und Richter, die in den einstweiligen Ruhestand versetzt worden sind, erhalten für den Monat, in dem die Versetzung in den einstweiligen Ruhestand mitgeteilt worden ist, und für die folgenden drei Monate die Bezüge weiter, die ihnen am Tag vor der Versetzung zustanden; Änderungen beim Familienzuschlag sind zu berücksichtigen.
Aufwandsentschädigungen werden nur bis zum Beginn des einstweiligen Ruhestands gezahlt.

(2) Beziehen in den einstweiligen Ruhestand versetzte Beamtinnen, Beamte, Richterinnen oder Richter Einkünfte aus einer selbstständigen oder nicht selbstständigen Tätigkeit, so erfolgt eine hälftige Anrechnung dieser Einkünfte.
Dabei wird mindestens ein Betrag von 20 Prozent des nach Absatz 1 zustehenden Betrags belassen.
Bei einer Verwendung im Dienst eines öffentlich-rechtlichen Dienstherrn (§ 28) oder eines Verbandes, dessen Mitglieder öffentlich-rechtliche Dienstherren sind, werden die Bezüge um den Betrag der daraus erzielten Einkünfte verringert.
Dem Dienst bei einem öffentlich-rechtlichen Dienstherrn steht die Tätigkeit im Dienst einer zwischenstaatlichen oder überstaatlichen Einrichtung gleich, an der ein öffentlich-rechtlicher Dienstherr oder ein Verband, dessen Mitglieder öffentlich-rechtliche Dienstherren sind, durch Zahlung von Beiträgen oder Zuschüssen oder in anderer Weise beteiligt ist.
Die Entscheidung, ob die Voraussetzungen erfüllt sind, trifft das für das Besoldungsrecht zuständige Ministerium oder die von ihm bestimmte Stelle.

(3) Wird eine Wahlbeamtin auf Zeit oder ein Wahlbeamter auf Zeit abgewählt, so gelten die Absätze 1 und 2 entsprechend; an die Stelle der Mitteilung über die Versetzung in den einstweiligen Ruhestand tritt die Mitteilung über die Abwahl oder der sonst bestimmte Beendigungszeitpunkt für das Beamtenverhältnis auf Zeit.
Satz 1 gilt entsprechend für die Fälle des Eintritts in den einstweiligen Ruhestand kraft Gesetzes.

##### § 5   
Besoldung bei mehreren Hauptämtern

Bekleiden Beamtinnen, Beamte, Richterinnen oder Richter mit Genehmigung der obersten Dienstbehörde gleichzeitig mehrere besoldete Hauptämter, so wird Besoldung nach diesem Gesetz nur aus dem Amt mit den höheren Dienstbezügen gewährt, soweit gesetzlich nichts anderes bestimmt ist.
Sind für die Ämter Dienstbezüge in gleicher Höhe vorgesehen, so werden die Dienstbezüge nach diesem Gesetz nur aus dem zuerst übertragenen Amt gezahlt, soweit gesetzlich nichts anderes bestimmt ist.

##### § 6   
Besoldung bei Teilzeitbeschäftigung

(1) Bei Teilzeitbeschäftigung wird die Besoldung im gleichen Verhältnis wie die Arbeitszeit gekürzt, soweit gesetzlich nichts anderes bestimmt ist.

(2) Bei Teilzeitbeschäftigung nach § 110 Absatz 10, § 117 Satz 2 in Verbindung mit § 110 Absatz 10 und § 118 Satz 1 in Verbindung mit § 110 Absatz 10 des Landesbeamtengesetzes wird zusätzlich zur Besoldung bei Teilzeitbeschäftigung ein nicht ruhegehaltfähiger Teilzeitzuschlag gewährt.
Der Zuschlag wird gewährt in Höhe des Unterschiedsbetrags zwischen der Nettobesoldung, die sich aus dem Umfang der Teilzeitbeschäftigung ergibt, und 85 Prozent der Nettobesoldung, die ohne die Teilzeitbeschäftigung zustehen würde.

(3) Besoldung im Sinne des Absatzes 2 Satz 2 sind das Grundgehalt, der Familienzuschlag, Amtszulagen, Stellenzulagen sowie Überleitungszulagen und Ausgleichszulagen, die wegen des Wegfalls oder der Verminderung dieser Bezüge zustehen.
Zur Ermittlung der Nettobesoldung im Sinne des Absatzes 2 Satz 2 2. Halbsatz ist die Bruttobesoldung um die Lohnsteuer entsprechend der individuellen Steuerklasse und den Solidaritätszuschlag zu vermindern; Freibeträge oder sonstige individuelle Merkmale bleiben unberücksichtigt.
Steuerfreie Bezüge, Erschwerniszulagen und Vergütungen werden entsprechend dem Umfang der tatsächlich geleisteten Tätigkeit gewährt.

##### § 7   
Besoldung bei begrenzter Dienstfähigkeit

(1) Beamtinnen, Beamte, Richterinnen und Richter, deren regelmäßige Arbeitszeit wegen begrenzter Dienstfähigkeit nach § 27 des Beamtenstatusgesetzes oder nach entsprechenden Bestimmungen für Richterinnen und Richter herabgesetzt wird, erhalten Besoldung entsprechend § 6 Absatz 1.
Diese wird um einen nicht ruhegehaltfähigen Zuschlag ergänzt.
Der Zuschlag beträgt 50 Prozent des Unterschiedsbetrags zwischen der nach Satz 1 gekürzten Besoldung und der Besoldung, die nach der regelmäßigen Arbeitszeit zu zahlen wäre.

(2) Ein Zuschlag nach Absatz 1 wird nicht gewährt, wenn ein Zuschlag nach § 64 Absatz 1 zusteht.

##### § 8   
Kürzung der Besoldung bei Gewährung einer Versorgung durch eine zwischenstaatliche oder überstaatliche Einrichtung

(1) Erhalten Beamtinnen, Beamte, Richterinnen oder Richter Versorgung aus einer Verwendung im öffentlichen Dienst einer zwischenstaatlichen oder überstaatlichen Einrichtung, wird die Besoldung gekürzt.
Die Kürzung beträgt 75 Prozent der von der zwischenstaatlichen oder überstaatlichen Einrichtung gewährten Versorgung.
Es verbleiben jedoch mindestens 40 Prozent der Besoldung.

(2) Beziehen Beamtinnen, Beamte, Richterinnen oder Richter Versorgungsbezüge nach Artikel 14 bis 17 des Beschlusses 2005/684/EG, Euratom des Europäischen Parlaments vom 28.
September 2005 zur Annahme des Abgeordnetenstatuts des Europäischen Parlaments (ABl. L 262 vom 7.10.2005, S. 1), so wird die Besoldung um 50 Prozent des Betrags gekürzt, um den die Besoldung und die Versorgungsbezüge nach dem Beschluss 2005/684/EG, Euratom die Entschädigung nach Artikel 10 des Beschlusses 2005/684/EG, Euratom übersteigen.
Das Übergangsgeld nach Artikel 13 des Beschlusses 2005/684/EG, Euratom zählt zu den Versorgungsbezügen.

##### § 9   
Verlust der Besoldung bei schuldhaftem Fernbleiben vom Dienst

Bleiben Beamtinnen, Beamte, Richterinnen oder Richter ohne Genehmigung schuldhaft dem Dienst fern, so verlieren sie für die Zeit des Fernbleibens ihre Besoldung.
Dies gilt auch bei einem Fernbleiben vom Dienst für Teile eines Tages.
Der Verlust der Besoldung ist durch den Dienstvorgesetzten festzustellen.

##### § 10   
Anrechnung anderer Einkünfte auf die Besoldung

(1) Haben Beamtinnen, Beamte, Richterinnen oder Richter Anspruch auf Besoldung für eine Zeit, in der sie nicht zur Dienstleistung verpflichtet waren, kann ein infolge der unterbliebenen Dienstleistung für diesen Zeitraum erzieltes anderes Einkommen auf die Besoldung angerechnet werden.
Die Beamtinnen, Beamten, Richterinnen und Richter sind zur Auskunft verpflichtet.
In den Fällen einer vorläufigen Dienstenthebung aufgrund eines Disziplinarverfahrens gelten die Vorschriften des Disziplinarrechts.

(2) Erhalten Beamtinnen, Beamte, Richterinnen oder Richter aus einer Verwendung nach § 20 des Beamtenstatusgesetzes oder nach entsprechenden Bestimmungen für Richterinnen und Richter anderweitig Bezüge, werden diese auf die Besoldung angerechnet.
In besonderen Fällen kann die oberste Dienstbehörde im Einvernehmen mit dem für das Besoldungsrecht zuständigen Ministerium von der Anrechnung ganz oder teilweise absehen.

##### § 11  
Anrechnung von Sachbezügen auf die Besoldung

(1) Erhalten Beamtinnen, Beamte, Richterinnen oder Richter Sachbezüge, so werden diese unter Berücksichtigung ihres wirtschaftlichen Wertes mit einem angemessenen Betrag auf die Besoldung angerechnet, soweit nicht etwas anderes bestimmt ist.

(2) Das für das Besoldungsrecht zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung Näheres im Einvernehmen mit dem für das allgemeine öffentliche Dienstrecht zuständigen Mitglied der Landesregierung zu regeln.
Das jeweils fachlich zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung im Einvernehmen mit dem für das Besoldungsrecht zuständigen Mitglied der Landesregierung für die Beamtinnen und Beamten der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts Näheres zu regeln.

##### § 12   
Abtretung von Bezügen, Verpfändung, Aufrechnungs- und Zurückbehaltungsrecht, Verjährung

(1) Beamtinnen, Beamte, Richterinnen und Richter können, wenn gesetzlich nichts anderes bestimmt ist, Ansprüche auf Bezüge nur abtreten oder verpfänden, soweit diese Bezüge der Pfändung unterliegen.

(2) Gegenüber Ansprüchen auf Bezüge kann der Dienstherr ein Aufrechnungs- und Zurückbehaltungsrecht nur in Höhe des pfändbaren Teils der Bezüge geltend machen.
Dies gilt nicht, soweit gegen eine Beamtin, einen Beamten, eine Richterin oder einen Richter ein Anspruch des Dienstherrn auf Schadensersatz wegen vorsätzlicher unerlaubter Handlung besteht.

(3) Ansprüche nach diesem Gesetz und nach Verordnungen, die auf der Grundlage dieses Gesetzes ergangen sind, und Ansprüche auf Rückzahlung von zu viel gezahlten Bezügen verjähren in drei Jahren; Ansprüche auf Rückzahlung von Bezügen verjähren in zehn Jahren, wenn durch vorsätzlich oder leichtfertig unrichtige oder unvollständige Angaben oder das vorsätzliche oder leichtfertige pflichtwidrige Unterlassen von Angaben die Gewährung oder Belassung von Bezügen bewirkt wurde.
Die Verjährung beginnt mit dem Schluss des Jahres, in dem der Anspruch entstanden ist.
Im Übrigen sind die §§ 194 bis 218 des Bürgerlichen Gesetzbuchs entsprechend anzuwenden.

##### § 13   
Rückforderung von Bezügen

(1) Werden Beamtinnen, Beamte, Richterinnen oder Richter durch eine gesetzliche Änderung der Bezüge einschließlich der Einreihung des Amtes in die Besoldungsgruppen der Besoldungsordnungen mit rückwirkender Kraft schlechter gestellt, so sind die Unterschiedsbeträge nicht zu erstatten.

(2) Im Übrigen regelt sich die Rückforderung zu viel gezahlter Bezüge nach den Vorschriften des Bürgerlichen Gesetzbuchs über die Herausgabe einer ungerechtfertigten Bereicherung, soweit gesetzlich nichts anderes bestimmt ist.
Der Kenntnis des Mangels des rechtlichen Grundes der Zahlung steht es gleich, wenn der Mangel so offensichtlich war, dass die Empfängerin oder der Empfänger ihn hätte erkennen müssen.
Von der Rückforderung kann aus Billigkeitsgründen mit Zustimmung der obersten Dienstbehörde oder der von ihr bestimmten Stelle ganz oder teilweise abgesehen werden.

(3) Geldleistungen, die für die Zeit nach dem Tod von Beamtinnen, Beamten, Richterinnen oder Richtern auf ein Konto bei einem Geldinstitut überwiesen wurden, gelten als unter Vorbehalt erbracht.
Das Geldinstitut hat sie der überweisenden Stelle zurückzuüberweisen, wenn diese sie als zu Unrecht erbracht zurückfordert.
Eine Verpflichtung zur Rücküberweisung besteht nicht, soweit über den entsprechenden Betrag bei Eingang der Rückforderung bereits anderweitig verfügt wurde, es sei denn, dass die Rücküberweisung aus einem Guthaben erfolgen kann.
Das Geldinstitut darf den überwiesenen Betrag nicht zur Befriedigung eigener Forderungen verwenden.

(4) Soweit Geldleistungen für die Zeit nach dem Tod von Beamtinnen, Beamten, Richterinnen und Richtern zu Unrecht erbracht worden sind, haben die Personen, die die Geldleistung in Empfang genommen oder über den entsprechenden Betrag verfügt haben, diesen Betrag der überweisenden Stelle zu erstatten, sofern er nicht nach Absatz 3 von dem Geldinstitut zurücküberwiesen wird.
Ein Geldinstitut, das eine Rücküberweisung mit dem Hinweis abgelehnt hat, dass über den entsprechenden Betrag bereits anderweitig verfügt wurde, hat der überweisenden Stelle auf Verlangen Namen und Anschrift der Personen, die über den Betrag verfügt haben, sowie etwaiger neuer Kontoinhaberinnen oder Kontoinhaber zu benennen.
Ein Anspruch gegen die Erbinnen und Erben bleibt unberührt.

##### § 14  
Anpassung der Besoldung

Die Besoldung wird entsprechend der Entwicklung der allgemeinen wirtschaftlichen und finanziellen Verhältnisse und unter Berücksichtigung der mit den Dienstaufgaben verbundenen Verantwortung durch Gesetz regelmäßig angepasst.

##### § 15   
Versorgungsrücklage

(1) Um die Versorgungsleistungen angesichts der demografischen Veränderungen und des Anstiegs der Zahl der Versorgungsempfängerinnen und Versorgungsempfänger sicherzustellen, wird beim Land und bei den Kommunen jeweils eine Versorgungsrücklage als Sondervermögen aus der Verminderung der Besoldungs- und Versorgungsanpassungen nach Absatz 2 gebildet.
Damit soll zugleich das Besoldungs- und Versorgungsniveau in gleichmäßigen Schritten von durchschnittlich 0,2 Prozentpunkten abgesenkt werden.

(2) Bis zum 31.
Dezember 2017 werden die Anpassungen der Besoldung (§ 14) gemäß Absatz 1 Satz 2 vermindert.
Der Unterschiedsbetrag gegenüber der nicht nach Satz 1 verminderten Anpassung wird den Sondervermögen zugeführt.
Die Mittel der Sondervermögen dürfen nur zur Finanzierung künftiger Versorgungsausgaben verwendet werden.

(3) Den Versorgungsrücklagen werden im Zeitraum nach Absatz 2 Satz 1 zusätzlich 50 Prozent der Verminderung der Versorgungsausgaben durch das Versorgungsänderungsgesetz 2001 vom 20. Dezember 2001 (BGBl.
I S. 3926) zugeführt.

(4) Das Nähere wird durch ein besonderes Gesetz geregelt.

##### § 16   
Dienstlicher Wohnsitz

(1) Dienstlicher Wohnsitz von Beamtinnen, Beamten, Richterinnen oder Richtern ist der Ort, an dem ihre Behörde oder ständige Dienststelle ihren Sitz hat.

(2) Die oberste Dienstbehörde kann als dienstlichen Wohnsitz anweisen:

1.  den Ort, der Mittelpunkt der dienstlichen Tätigkeit für Beamtinnen, Beamte, Richterinnen oder Richter ist,
2.  den Ort, in dem Beamtinnen, Beamte, Richterinnen oder Richter mit Zustimmung der vorgesetzten Dienststelle wohnen,
3.  einen Ort im Inland, wenn Beamtinnen oder Beamte im Ausland an der deutschen Grenze beschäftigt sind.

Sie kann diese Befugnis durch Rechtsverordnung auf nachgeordnete Stellen übertragen.

##### § 17  
Aufwandsentschädigungen

(1) Aufwandsentschädigungen dürfen nur gewährt werden, wenn und soweit aus dienstlicher Veranlassung finanzielle Aufwendungen entstehen, deren Übernahme Beamtinnen, Beamten, Richterinnen oder Richtern nicht zugemutet werden kann, und der Haushaltsplan Mittel dafür zur Verfügung stellt.
Aufwandsentschädigungen in festen Beträgen sind nur zulässig, wenn aufgrund tatsächlicher Anhaltspunkte oder tatsächlicher Erhebungen nachvollziehbar ist, dass und in welcher Höhe dienstbezogene finanzielle Aufwendungen typischerweise entstehen.
Sie werden im Einvernehmen mit dem für das Besoldungsrecht zuständigen Ministerium festgesetzt.

(2) Das fachlich zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung im Einvernehmen mit dem für das Besoldungsrecht zuständigen Mitglied der Landesregierung Richtlinien für die Gewährung von Dienstaufwandsentschädigungen an die Beamtinnen und Beamten der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts zu erlassen und dabei Höchstgrenzen festzulegen.
Die Richtlinien dürfen von den für die Beamtinnen und Beamten des Landes geltenden Bestimmungen nur abweichen, wenn dies wegen der Verschiedenheit der Verhältnisse sachlich notwendig ist.

(3) Soweit Vorschriften nach Absatz 2 nicht erlassen worden sind, bedarf die Ausbringung von Mitteln für Aufwandsentschädigungen im Haushaltsplan oder einem entsprechenden Plan der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts der Zustimmung der obersten Aufsichtsbehörde und des für das Besoldungsrecht zuständigen Mitglieds der Landesregierung oder der von ihnen bestimmten Stelle.

(4) Neben der Besoldung einschließlich Aufwandsentschädigungen dürfen sonstige Geldzuwendungen an Beamtinnen und Beamte der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts nur insoweit gewährt werden, als sie die Geldzuwendungen nach den für die Beamtinnen und Beamten des Landes geltenden Regelungen nicht übersteigen.
Sonstige Geldzuwendungen sind Geldleistungen und geldwerte Leistungen, die die Beamtinnen und Beamten unmittelbar oder mittelbar von ihrem Dienstherrn erhalten.

(5) Absatz 4 gilt nicht für im Wettbewerb stehende Unternehmen in öffentlich-rechtlicher Rechtsform und deren Verbände sowie im Wettbewerb stehende Eigenbetriebe.

(6) Vorschriften der Kommunalverfassung des Landes Brandenburg zur Gewährung von Aufwandsentschädigungen an Beamtinnen und Beamte, die eine Gemeinde oder einen Gemeindeverband in rechtlich selbstständigen Unternehmen vertreten, bleiben unberührt.

### Abschnitt 2   
Grundgehalt

#### Unterabschnitt 1   
Allgemeine Grundsätze

##### § 18   
Grundsatz der funktionsgerechten Besoldung

Die Funktionen der Beamtinnen, Beamten, Richterinnen und Richter sind nach den mit ihnen verbundenen Anforderungen sachgerecht zu bewerten und Ämtern zuzuordnen.
Eine Funktion kann bis zu drei Ämtern einer Laufbahngruppe, in obersten Landesbehörden allen Ämtern einer Laufbahngruppe zugeordnet werden.
Abweichend von Satz 2 können die Funktionen der Beamtinnen und Beamten in der Laufbahn des gehobenen Justizdienstes allen Ämtern ihrer Laufbahngruppe zugeordnet werden, soweit Aufgaben nach dem Rechtspflegergesetz in sachlicher Unabhängigkeit wahrgenommen werden.
Die Ämter sind nach ihrer Wertigkeit unter Berücksichtigung der gemeinsamen Belange aller Dienstherren den Besoldungsgruppen zuzuordnen.

##### § 19   
Bestimmung des Grundgehalts nach dem Amt

(1) Das Grundgehalt der Beamtinnen, Beamten, Richterinnen und Richter bestimmt sich nach der Besoldungsgruppe des verliehenen Amtes.
Ist ein Amt noch nicht in einer Besoldungsordnung enthalten oder ist eine Amtsbezeichnung mehreren Besoldungsgruppen zugeordnet, bestimmt sich das Grundgehalt nach der Besoldungsgruppe, die in der Einweisungsverfügung bestimmt ist, soweit sich die Besoldungsgruppe nicht bereits aus der Ernennungsurkunde ergibt.
Die Einweisung bedarf bei Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts in den Fällen, in denen das Amt in einer Besoldungsordnung noch nicht enthalten ist, der Zustimmung der obersten Rechtsaufsichtsbehörde im Einvernehmen mit dem für das Besoldungsrecht zuständigen Ministerium.
Ist einer Richterin oder einem Richter noch kein Amt verliehen worden, so bestimmt sich das Grundgehalt nach der Besoldungsgruppe R 1.

(2) Ist einem Amt gesetzlich eine Funktion zugeordnet oder richtet sich die Zuordnung eines Amtes zu einer Besoldungsgruppe einschließlich der Gewährung von Amtszulagen nach einem gesetzlich festgelegten Bewertungsmaßstab, insbesondere nach der Zahl der Planstellen, nach der Einwohnerzahl einer Gemeinde oder eines Gemeindeverbandes oder nach der Schülerzahl einer Schule, so richtet sich die Höhe der Besoldung ausschließlich nach dem verliehenen Amt.
Für die Bestimmung der Einwohnerzahl ist die vom Amt für Statistik Berlin-Brandenburg ermittelte Wohnbevölkerung jeweils vom Beginn des folgenden Kalenderjahres an maßgebend.
Für die Bestimmung der Schülerzahl einer Schule sind für das jeweilige Schuljahr die Ergebnisse der amtlichen Schulstatistik maßgebend.

(3) Wird einer Beamtin, einem Beamten, einer Richterin oder einem Richter ein Amt mit höherem Endgrundgehalt verliehen, so kann die Einweisung in die höhere Planstelle mit Rückwirkung von dem ersten oder einem sonstigen Tag des Kalendermonats, in dem die Verleihung wirksam wird, vorgenommen werden, soweit die höhere Planstelle besetzbar war.

(4) Die §§ 17 und 49 der Landeshaushaltsordnung gelten für die Gemeinden, die Gemeindeverbände und die sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts entsprechend.

(5) Richtet sich die Zuordnung von Ämtern zu den Besoldungsgruppen einschließlich der Gewährung von Amtszulagen nach der Schülerzahl einer Schule, so sind bei einer dadurch eintretenden Änderung der Zuordnung Ernennungen und Einweisungen in Planstellen nicht vorzunehmen und Amtszulagen nicht zu gewähren, wenn abzusehen ist, dass die Änderung nicht länger als für die Dauer eines Schuljahres Bestand haben wird.

#### Unterabschnitt 2   
Vorschriften für Beamtinnen und Beamte der Besoldungsordnungen A und B

##### § 20   
Besoldungsordnungen A und B

(1) Die Ämter der Beamtinnen und Beamten und ihre Besoldungsgruppen sind in den Besoldungsordnungen geregelt.
§ 21 bleibt unberührt.

(2) Die Besoldungsordnung A (aufsteigende Gehälter) und die Besoldungsordnung B (feste Gehälter) sind in Anlage 1, die Grundgehaltssätze der Besoldungsgruppen sind in Anlage 4 ausgewiesen.

##### § 21   
Hauptamtliche kommunale Wahlbeamtinnen und Wahlbeamte auf Zeit

Das für Inneres zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung im Einvernehmen mit dem für das Besoldungsrecht zuständigen Mitglied der Landesregierung die Ämter der hauptamtlichen Wahlbeamtinnen und Wahlbeamten auf Zeit der Gemeinden und der Gemeindeverbände unter Berücksichtigung der Einwohnerzahl den Besoldungsgruppen der Besoldungsordnungen A und B zuzuordnen.
Dabei können bei Körperschaften einer Größenklasse höchstens zwei Besoldungsgruppen für ein Amt vorgesehen werden.
Für diese Beamtinnen und Beamten kann der Beginn des Aufsteigens in den Stufen abweichend von § 25 geregelt werden.

##### § 22   
Eingangsämter für Beamtinnen und Beamte

Die Eingangsämter für Beamtinnen und Beamte sind folgenden Besoldungsgruppen zuzuweisen:

1.  in der Laufbahn des Justizwachtmeisterdienstes der Besoldungsgruppe A 5, in der Laufbahn des mittleren Steuerverwaltungsdienstes der Besoldungsgruppe A 7, in den übrigen Laufbahnen des mittleren nichttechnischen Dienstes der Besoldungsgruppe A 6, in Laufbahnen des mittleren technischen Dienstes der Besoldungsgruppe A 6 oder A 7,
2.  in Laufbahnen des gehobenen nichttechnischen Dienstes der Besoldungsgruppe A 9, in Laufbahnen des gehobenen technischen Dienstes der Besoldungsgruppe A 10,
3.  in Laufbahnen des höheren Dienstes der Besoldungsgruppe A 13.

##### § 23  
Eingangsämter für Beamtinnen und Beamte in Sonderlaufbahnen

Die Eingangsämter in Sonderlaufbahnen, bei denen

1.  die Ausbildung mit einer gegenüber dem nichttechnischen oder technischen Verwaltungsdienst besonders gestalteten Prüfung abgeschlossen wird oder die Ablegung einer zusätzlichen Prüfung vorgeschrieben ist und
2.  im Eingangsamt Anforderungen gestellt werden, die bei sachgerechter Bewertung zwingend die Zuweisung des Eingangsamtes zu einer anderen Besoldungsgruppe als nach § 22 erfordern,

können einer höheren Besoldungsgruppe zugewiesen werden, in die gleichwertige Ämter eingereiht sind.
Die Festlegung als Eingangsamt ist in den Besoldungsordnungen zu kennzeichnen.

##### § 24   
Obergrenzen für Beförderungsämter

(1) Beförderungsämter dürfen, soweit gesetzlich nichts anderes bestimmt ist, nur eingerichtet werden, wenn sie sich von den Ämtern der niedrigeren Besoldungsgruppe nach der Wertigkeit der zugeordneten Funktionen wesentlich abheben.

(2) Die Anteile der Beförderungsämter dürfen nach Maßgabe sachgerechter Bewertung Obergrenzen nicht überschreiten.
Dies gilt nicht für die obersten Landesbehörden, für die Gemeinden und Gemeindeverbände, für den Kommunalen Versorgungsverband Brandenburg, für Lehrerinnen, Lehrer und pädagogisches Hilfspersonal an öffentlichen Schulen und Hochschulen, für Beamtinnen und Beamte des Schulaufsichtsdienstes sowie für Lehrkräfte an verwaltungsinternen Fachhochschulen.

(3) Die Landesregierung wird ermächtigt, durch Rechtsverordnung zur sachgerechten Bewertung der Funktionen Obergrenzen für die Zahl der Beförderungsämter festzulegen.
Dabei sind insbesondere die Besonderheiten der einzelnen Laufbahnen und Funktionen zu berücksichtigen sowie Bestimmungen zur befristeten Überschreitung von Stellenobergrenzen bei organisatorischen Veränderungen zu treffen.

##### § 25  
Bemessung des Grundgehalts

(1) Die Höhe des Grundgehalts in den Besoldungsgruppen der Besoldungsordnung A wird nach Stufen bemessen.
Der Aufstieg in eine nächsthöhere Stufe erfolgt nach bestimmten Zeiten mit dienstlicher Erfahrung, in denen anforderungsgerechte Leistungen erbracht wurden (Erfahrungszeiten).

(2) Das Grundgehalt steigt in den Stufen eins bis vier im Abstand von zwei Jahren, in den Stufen fünf bis acht im Abstand von drei Jahren und ab der Stufe neun im Abstand von vier Jahren bis zum Erreichen der letzten Stufe.
Zeiten ohne Anspruch auf Besoldung verzögern den Stufenaufstieg um diese Zeiten, soweit in § 26 Absatz 2 nichts anderes bestimmt ist.
Die Zeiten werden auf volle Monate abgerundet.

(3) Das Aufsteigen in den Stufen beginnt mit dem Anfangsgrundgehalt der jeweiligen Besoldungsgruppe mit Wirkung vom Ersten des Monats, in dem die erste Ernennung mit Anspruch auf Dienstbezüge bei einem öffentlich-rechtlichen Dienstherrn im Geltungsbereich des Grundgesetzes wirksam wird.
Liegen zu diesem Zeitpunkt berücksichtigungsfähige Zeiten nach § 26 Absatz 1 vor, wird der Zeitpunkt des Beginns um diese Zeiten vorverlegt.
Ausgehend von dem Zeitpunkt des Beginns werden die Stufenlaufzeiten nach Absatz 2 berechnet.
Die Berechnung und die Festsetzung des Zeitpunkts des Beginns des Aufsteigens in den Stufen stellt die personalaktenführende Stelle fest und teilt diese Festsetzung der Beamtin oder dem Beamten schriftlich mit.

(4) Wird festgestellt, dass die Leistungen einer Beamtin oder eines Beamten nicht den mit dem Amt verbundenen Anforderungen entsprechen, verbleibt die Beamtin oder der Beamte in der bisherigen Stufe des Grundgehalts, bis die Leistungen ein Aufsteigen in die nächsthöhere Stufe rechtfertigen.
Eine darüberliegende Stufe, in der sich die Beamtin oder der Beamte ohne die Hemmung des Aufstiegs inzwischen befinden würde, darf frühestens nach Ablauf eines Jahres als Grundgehalt festgesetzt werden, wenn in diesem Zeitraum anforderungsgerechte Leistungen erbracht worden sind.
Die Landesregierung wird ermächtigt, durch Rechtsverordnung nähere Regelungen zu treffen.
Widerspruch und Anfechtungsklage haben keine aufschiebende Wirkung.

(5) Bei dauerhaft herausragenden Leistungen kann für den Zeitraum bis zum Erreichen der nächsten Stufe das Grundgehalt der nächsthöheren Stufe gezahlt werden (Leistungsstufe).
Dies gilt nicht für Beamtinnen und Beamte in einer Probezeit nach § 18 oder § 120 des Landesbeamtengesetzes.
Die Landesregierung wird ermächtigt, durch Rechtsverordnung nähere Regelungen zu treffen.

(6) Beamtinnen und Beamte verbleiben in der bisherigen Stufe, solange sie des Dienstes enthoben sind.
Führt ein Disziplinarverfahren zur Entfernung aus dem Dienst oder endet das Dienstverhältnis durch Entlassung auf Antrag einer Beamtin oder eines Beamten oder infolge strafgerichtlicher Verurteilung, so erlischt der Anspruch auch für den Zeitraum des Verbleibens in der Stufe.
Führt ein Disziplinarverfahren nicht zur Entfernung aus dem Dienst oder endet das Dienstverhältnis nicht durch Entlassung auf Antrag einer Beamtin oder eines Beamten oder infolge strafgerichtlicher Verurteilung, so regelt sich das Aufsteigen im Zeitraum der vorläufigen Dienstenthebung nach Absatz 2.

##### § 26   
Berücksichtigungsfähige Zeiten

(1) Bei der ersten Stufenzuordnung werden folgende Zeiten im Sinne des § 25 Absatz 3 anerkannt:

1.  Zeiten einer hauptberuflichen Tätigkeit bei einem öffentlich-rechtlichen Dienstherrn (§ 28), im Dienst der Fraktionen des Bundestages, der Landesparlamente, kommunaler Vertretungskörperschaften oder des Europäischen Parlaments oder im Dienst von öffentlich-rechtlichen Religionsgemeinschaften und ihren Verbänden, die nicht Voraussetzung für den Erwerb der Laufbahnbefähigung sind,
2.  Zeiten einer Kinderbetreuung bis zu drei Jahren für jedes Kind,
3.  Zeiten der tatsächlichen Pflege von nach ärztlichem Gutachten pflegebedürftigen nahen Angehörigen (Eltern, Schwiegereltern, Ehegattinnen, Ehegatten, eingetragene Lebenspartnerinnen und Lebenspartner, Geschwister, Kinder) bis zu drei Jahren für jeden dieser Angehörigen,
4.  Zeiten, in denen Wehrdienst, Zivildienst, Bundesfreiwilligendienst nach dem Bundesfreiwilligendienstgesetz, Entwicklungsdienst oder ein freiwilliges soziales oder ökologisches Jahr geleistet wurde,
5.  Verfolgungszeiten nach dem Beruflichen Rehabilitierungsgesetz, soweit eine Erwerbstätigkeit, die einem Dienst bei einem öffentlich-rechtlichen Dienstherrn entspricht, nicht ausgeübt werden konnte.

Weitere hauptberufliche Zeiten, die nicht Voraussetzung für den Erwerb der Laufbahnbefähigung sind, können ganz oder teilweise anerkannt werden, soweit sie für die Verwendung förderlich sind.
Zeiten nach den Sätzen 1 und 2 werden durch Unterbrechungszeiten nach Absatz 2 nicht vermindert.
Die Zeiten nach den Sätzen 1 und 2 werden auf volle Monate aufgerundet.

(2) Der Aufstieg in den Stufen wird durch folgende Zeiten ohne Anspruch auf Besoldung nicht verzögert:

1.  berücksichtigungsfähige Zeiten nach Absatz 1 Satz 1 Nummer 1 und Satz 2 nach der ersten Ernennung mit Anspruch auf Dienstbezüge bei einem öffentlich-rechtlichen Dienstherrn,
2.  Zeiten einer Kinderbetreuung bis zu drei Jahren für jedes Kind,
3.  Zeiten der tatsächlichen Pflege von nach ärztlichem Gutachten pflegebedürftigen nahen Angehörigen (Eltern, Schwiegereltern, Ehegattinnen, Ehegatten, eingetragene Lebenspartnerinnen und Lebenspartner, Geschwister, Kinder) bis zu drei Jahren für jeden dieser Angehörigen,
4.  Zeiten einer Beurlaubung unter Wegfall der Besoldung, wenn die oberste Dienstbehörde oder die von ihr bestimmte Stelle schriftlich anerkannt hat, dass der Urlaub dienstlichen Interessen oder öffentlichen Belangen dient,
5.  Zeiten einer sonstigen Beurlaubung unter Wegfall der Besoldung, wenn innerhalb eines Kalenderjahres ein Zeitraum von insgesamt vier Wochen nicht überschritten wird,
6.  Zeiten eines Wehr- oder Zivildienstes und
7.  Zeiten einer Eignungsübung nach dem Eignungsübungsgesetz.

##### § 27  
Nicht zu berücksichtigende Zeiten

Zeiten im Sinne des § 64 Absatz 1 des Landesbeamtengesetzes werden als Erfahrungszeiten im Sinne des § 25 Absatz 3 nicht berücksichtigt.

##### § 28   
Öffentlich-rechtliche Dienstherren

(1) Öffentlich-rechtliche Dienstherren im Sinne der §§ 25 und 26 sind der Bund, die Länder, die Gemeinden, die Gemeindeverbände und sonstige Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts mit Ausnahme der öffentlich-rechtlichen Religionsgemeinschaften und ihrer Verbände.
Einrichtungen in der Deutschen Demokratischen Republik gelten als öffentlich-rechtliche Dienstherren, wenn sie auch im Geltungsbereich des Grundgesetzes juristischen Personen des öffentlichen Rechts zugeordnet gewesen wären.

(2) Der Tätigkeit im Dienst eines öffentlich-rechtlichen Dienstherrn stehen gleich:

1.  für Staatsangehörige eines Mitgliedstaates der Europäischen Union die ausgeübte gleichartige Tätigkeit im öffentlichen Dienst einer Einrichtung der Europäischen Union oder im öffentlichen Dienst eines Mitgliedstaates der Europäischen Union,
2.  die von Spätaussiedlerinnen und Spätaussiedlern ausgeübte Tätigkeit im Dienst eines öffentlich-rechtlichen Dienstherrn ihres Herkunftslandes.

#### Unterabschnitt 3   
Vorschriften für Professorinnen und Professoren sowie für hauptberufliche Leiterinnen und Leiter und Mitglieder von Leitungsgremien an Hochschulen

##### § 29   
Besoldungsordnung W

Die Ämter der Hochschullehrerinnen und Hochschullehrer (Professorinnen und Professoren sowie Juniorprofessorinnen und Juniorprofessoren) und ihre Besoldungsgruppen sind in der Besoldungsordnung W (Anlage 2) geregelt; die Grundgehaltssätze sind in der Anlage 4 ausgewiesen.
Dies gilt auch für hauptberufliche Leiterinnen und Leiter sowie für Mitglieder von Leitungsgremien an Hochschulen, die nicht Professorinnen oder Professoren sind, soweit ihre Ämter nicht Besoldungsgruppen der Besoldungsordnungen A und B zugewiesen sind.
Der Anteil der W 3-Planstellen an Fachhochschulen beträgt höchstens 25 Prozent der Gesamtzahl der Planstellen für Professorinnen und Professoren an Fachhochschulen.

##### § 30   
Leistungsbezüge

(1) In der Besoldungsordnung W werden nach Maßgabe der nachfolgenden Vorschriften neben dem Grundgehalt variable Leistungsbezüge vergeben:

1.  aus Anlass von Berufungs- und Bleibeverhandlungen (§ 31),
2.  für besondere Leistungen in Forschung, Lehre, Kunst, Weiterbildung und Nachwuchsförderung (§ 32) sowie
3.  in den Besoldungsgruppen W 2 und W 3 für die Wahrnehmung von Funktionen oder besonderen Aufgaben im Rahmen der Hochschulselbstverwaltung oder der Hochschulleitung (§ 33).

Die Leistungsbezüge nehmen an den regelmäßigen Besoldungsanpassungen teil, sofern dies im Einzelnen gesetzlich bestimmt ist.

(2) In den Besoldungsgruppen W 2 und W 3 werden mindestens Leistungsbezüge ab 1.
Januar 2019 in Höhe von 767,56 Euro, ab 1.
Januar 2020 in Höhe von 795,96 Euro und ab 1.
Januar 2021 in Höhe von 807,10 Euro gewährt.
Leistungsbezüge nach Absatz 1 Satz 1 Nummer 3, die für die Wahrnehmung besonderer Aufgaben im Rahmen der Hochschulselbstverwaltung gewährt werden, bleiben bis zu einer Höhe von 300 Euro unberücksichtigt.

##### § 31  
Berufungs- und Bleibe-Leistungsbezüge

(1) Leistungsbezüge nach § 30 Absatz 1 Satz 1 Nummer 1 können gewährt werden, soweit dies erforderlich ist, um eine Hochschullehrerin oder einen Hochschullehrer für eine Hochschule zu gewinnen (Berufungs-Leistungsbezüge) oder zum Verbleiben an der Hochschule zu bewegen (Bleibe-Leistungsbezüge).
Hierbei sind insbesondere die individuelle Qualifikation, vorliegende Evaluationsergebnisse, die Bewerberlage und die Arbeitsmarktsituation in dem jeweiligen Fach zu berücksichtigen.

(2) Die Berufungs- und Bleibe-Leistungsbezüge können befristet oder unbefristet sowie als Einmalzahlung vergeben werden.
Seit der letzten Gewährung sollen mindestens drei Jahre vergangen sein.
Es kann bestimmt werden, dass unbefristet gewährte Berufungs- oder Bleibe-Leistungsbezüge an den regelmäßigen Besoldungsanpassungen teilnehmen.

(3) Die Gewährung von Bleibe-Leistungsbezügen setzt voraus, dass die Hochschullehrerin oder der Hochschullehrer das Einstellungsinteresse eines anderen Dienstherrn oder Arbeitgebers nachweist.

##### § 32   
Besondere Leistungsbezüge

Leistungsbezüge nach § 30 Absatz 1 Satz 1 Nummer 2 können für besondere Leistungen in den Bereichen Forschung, Lehre, Kunst, Weiterbildung oder Nachwuchsförderung, die erheblich über dem Durchschnitt liegen und in der Regel über mehrere Jahre erbracht werden müssen, gewährt werden.
Sie können als Einmalzahlung oder als monatliche Zahlungen für einen Zeitraum von bis zu fünf Jahren befristet vergeben werden.
Eine erneute Vergabe ist zulässig.
Bei einer erneuten Vergabe können laufende besondere Leistungsbezüge unbefristet vergeben werden.
Es kann bestimmt werden, dass unbefristet gewährte besondere Leistungsbezüge an den regelmäßigen Besoldungsanpassungen teilnehmen.
Unbefristet gewährte besondere Leistungsbezüge nach Satz 4 sollen bei einem erheblichen Leistungsabfall ganz oder teilweise für die Zukunft widerrufen werden.

##### § 33   
Funktions-Leistungsbezüge

Hauptberuflichen Hochschulleiterinnen und Hochschulleitern (Präsidentinnen und Präsidenten oder Rektorinnen und Rektoren) und hauptberuflichen Vizepräsidentinnen und Vizepräsidenten werden für die Dauer der Wahrnehmung dieser Aufgaben Leistungsbezüge nach § 30 Absatz 1 Satz 1 Nummer 3 gewährt; dies gilt auch in Fällen der geschäftsführenden Wahrnehmung gemäß § 63 Absatz 7 Satz 1 des Brandenburgischen Hochschulgesetzes.
Sie können auch für die Wahrnehmung sonstiger besonderer Aufgaben im Rahmen der Hochschulselbstverwaltung oder Hochschulleitung gewährt werden.
Bei Professorinnen und Professoren, die in einem gemeinsamen Berufungsverfahren nach § 38 Absatz 9 des Brandenburgischen Hochschulgesetzes berufen wurden, können die Funktions-Leistungsbezüge auch für die Übernahme von Leitungsfunktionen in einer außerhochschulischen Forschungseinrichtung gewährt werden, sofern hierfür Mittel Dritter bereitgestellt werden.
Bei der Bemessung des Funktions-Leistungsbezugs sind insbesondere die im Einzelfall mit der Aufgabe verbundene Verantwortung und Belastung sowie die Größe und Bedeutung der Hochschule oder Forschungseinrichtung zu berücksichtigen.
Der Grundsatz der funktionsgerechten Besoldung gemäß § 18 ist zu wahren.
Funktions-Leistungsbezüge nach Satz 1 nehmen an den regelmäßigen Besoldungsanpassungen teil.

##### § 34   
Höhe der Leistungsbezüge

(1) Leistungsbezüge dürfen den Unterschiedsbetrag zwischen den Grundgehältern der Besoldungsgruppe W 3 und der Besoldungsgruppe B 10 nicht übersteigen.
Hiervon ausgenommen sind die in Absatz 2 geregelten Sachverhalte.

(2) Leistungsbezüge dürfen den Unterschiedsbetrag zwischen den Grundgehältern der Besoldungsgruppe W 3 und der Besoldungsgruppe B 10 übersteigen,

1.  wenn dies erforderlich ist, um die Professorin oder den Professor aus dem Bereich außerhalb der deutschen Hochschulen zu gewinnen,
2.  um die Abwanderung der Professorin oder des Professors in den Bereich außerhalb der deutschen Hochschulen abzuwenden,
3.  wenn die Professorin oder der Professor bereits an der bisherigen Hochschule Leistungsbezüge erhält, die den Unterschiedsbetrag zwischen den Grundgehältern der Besoldungsgruppe W 3 und der Besoldungsgruppe B 10 übersteigen und dies erforderlich ist zur Gewinnung für eine Hochschule im Land Brandenburg oder zur Verhinderung der Abwanderung an eine andere deutsche Hochschule.

(3) Absatz 2 gilt entsprechend für hauptberufliche Leiterinnen und Leiter sowie für Mitglieder von Leitungsgremien an Hochschulen, die nicht Professorinnen oder Professoren sind.

##### § 35   
Ruhegehaltfähigkeit von Leistungsbezügen

(1) Leistungsbezüge sind mindestens in Höhe des in § 30 Absatz 2 genannten Betrags ruhegehaltfähig, wenn sie mindestens zwei Jahre bezogen worden sind.

(2) Berufungs- und Bleibe-Leistungsbezüge sowie besondere Leistungsbezüge sind ruhegehaltfähig, soweit sie unbefristet gewährt und jeweils mindestens zwei Jahre bezogen worden sind.
Befristet gewährte und jeweils mindestens für die Dauer von zehn Jahren bezogene Berufungs- und Bleibe-Leistungsbezüge sowie besondere Leistungsbezüge können bei wiederholter Vergabe für ruhegehaltfähig erklärt werden.
Bei mehreren befristeten Leistungsbezügen, die für ruhegehaltfähig erklärt worden sind, wird der für die Beamtin oder den Beamten günstigste Betrag als ruhegehaltfähiger Dienstbezug berücksichtigt.

(3) Unbefristet und befristet gewährte Berufungs- und Bleibe-Leistungsbezüge sowie besondere Leistungsbezüge sind bis zur Höhe von zusammen 40 Prozent des jeweiligen Grundgehalts nach Maßgabe des Absatzes 2 ruhegehaltfähig.
Abweichend davon können Berufungs- und Bleibe-Leistungsbezüge sowie besondere Leistungsbezüge über den Prozentsatz nach Satz 1 hinaus zusammen höchstens für

1.  2,5 Prozent der Inhaberinnen und Inhaber von W 2- oder W 3-Planstellen bis zur Höhe von 50 Prozent des Grundgehalts,
2.  2,5 Prozent der Inhaberinnen und Inhaber von W 2- oder W 3-Planstellen bis zur Höhe von 60 Prozent des Grundgehalts,
3.  1,6 Prozent der Inhaberinnen und Inhaber von W 2- oder W 3-Planstellen bis zur Höhe von 80 Prozent des Grundgehalts

für ruhegehaltfähig erklärt werden.

(4) Funktions-Leistungsbezüge sind in Höhe von 25 Prozent ruhegehaltfähig, soweit sie mindestens fünf Jahre zugestanden haben; sie sind in Höhe von 50 Prozent ruhegehaltfähig, soweit sie mindestens zehn Jahre zugestanden haben.
Tritt die Inhaberin oder der Inhaber des Funktions-Leistungsbezugs während oder mit Ablauf der Amtszeit aus dem Beamtenverhältnis auf Zeit in den Ruhestand, ist der Leistungsbezug in voller Höhe ruhegehaltfähig, wenn der Beamtin oder dem Beamten das Amt mindestens zwei Jahre übertragen war.
Treffen ruhegehaltfähige Berufungs- und Bleibe-Leistungsbezüge oder besondere Leistungsbezüge mit Funktions-Leistungsbezügen zusammen, wird nur der bei der Berechnung des Ruhegehalts für die Beamtin oder den Beamten günstigere Betrag als ruhegehaltfähiger Dienstbezug berücksichtigt.

(5) In den Fällen einer Beurlaubung unter Wegfall der Besoldung, die öffentlichen Belangen oder dienstlichen Interessen dient, können von der Hochschule gewährte oder in der Berufungsvereinbarung zugesagte Leistungsbezüge nach den Absätzen 2 und 4 auch dann als ruhegehaltfähig berücksichtigt werden, wenn sie infolge der Beurlaubung nicht bezogen wurden und ein Versorgungszuschlag gezahlt worden ist.

##### § 36   
Forschungs- und Lehrzulage

(1) Hochschullehrerinnen und Hochschullehrern, die Mittel privater Dritter für Forschungs- oder Lehrvorhaben der Hochschule oder einer außerhochschulischen Forschungseinrichtung einwerben und diese Vorhaben durchführen, kann für die Dauer des Drittmittelflusses aus diesen Mitteln eine nicht ruhegehaltfähige Zulage gewährt werden, soweit der Drittmittelgeber bestimmte Mittel ausdrücklich zu diesem Zweck vorgesehen hat (Forschungs- und Lehrzulage).
Eine Zulage darf nur gewährt werden, soweit neben den übrigen Kosten des Forschungs- oder Lehrvorhabens auch die Zulagenbeträge durch die Drittmittel gedeckt sind.
Die entsprechende Lehrtätigkeit ist auf die Lehrverpflichtung nicht anzurechnen.

(2) In einem Kalenderjahr dürfen an eine Hochschullehrerin oder einen Hochschullehrer Forschungs- und Lehrzulagen - insgesamt höchstens bis zu 100 Prozent des Jahresgrundgehalts - bewilligt werden; bei Wechsel der Besoldungsgruppe in der Besoldungsordnung W während eines Kalenderjahres ist die höhere Besoldungsgruppe maßgebend.
In Ausnahmefällen, insbesondere wenn für die Bindung eines Forschungsvorhabens an eine Hochschule des Landes ein besonderes Landesinteresse besteht, kann der in Satz 1 festgelegte Höchstbetrag überschritten werden.

(3) Die Gewährung einer Forschungs- und Lehrzulage schließt die Gewährung von besonderen Leistungsbezügen für die Einwerbung von Drittmitteln für Forschungs- oder Lehrvorhaben aus.

##### § 37   
Evaluation und Verordnungsermächtigungen

(1) Die Landesregierung prüft die Wirkungen der Vorschriften dieses Unterabschnitts unter Berücksichtigung der Ziele des Professorenbesoldungsreformgesetzes vom 16.
Februar 2002 (BGBl.
I S. 686).
Sie berichtet dem Landtag bis zum 1.
Januar 2018 über die Ergebnisse ihrer Prüfung und ihre Überlegungen zu einer weiteren Reform des Besoldungsrechts.

(2) Das für Hochschulen zuständige Mitglied der Landesregierung sowie die Mitglieder der Landesregierung, die für die Angelegenheiten der Fachhochschulen, deren Ausbildungsgänge ausschließlich auf den öffentlichen Dienst ausgerichtet sind, zuständig sind, regeln durch Rechtsverordnung im Einvernehmen mit dem für das Besoldungsrecht zuständigen Mitglied der Landesregierung jeweils für ihren Bereich die Grundsätze, das Verfahren und die Zuständigkeit sowie die Voraussetzungen und die Kriterien für die Vergabe von Leistungsbezügen und deren Ruhegehaltfähigkeit.
Dies gilt auch für die Vergabe von Forschungs- und Lehrzulagen.

#### Unterabschnitt 4   
Vorschriften für Richterinnen, Richter, Staatsanwältinnen und Staatsanwälte

##### § 38   
Besoldungsordnung R

Die Ämter der Richterinnen, Richter, Staatsanwältinnen und Staatsanwälte und ihre Besoldungsgruppen sind in der Besoldungsordnung R (Anlage 3) geregelt.
Die Grundgehaltssätze der Besoldungsgruppen sind in der Anlage 4 ausgewiesen.
§ 24 Absatz 1 gilt entsprechend.

##### § 39   
Bemessung des Grundgehalts

Das Grundgehalt wird, soweit gesetzlich nichts anderes bestimmt ist, nach Stufen bemessen.
Die §§ 25 bis 28 gelten entsprechend mit folgenden Maßgaben:

1.  Das Grundgehalt steigt im Abstand von zwei Jahren bis zum Erreichen der letzten Stufe.
2.  § 25 Absatz 4 und 5 findet keine Anwendung.

### Abschnitt 3  
Familienzuschlag

##### § 40   
Grundlage und Höhe des Familienzuschlags ab 1.
Januar 2015

(1) Der Familienzuschlag wird nach der Anlage 6 gewährt.
Seine Höhe richtet sich nach der Anzahl und nach der kindergeldrechtlich maßgebenden Reihenfolge der zu berücksichtigenden Kinder einer Beamtin, eines Beamten, einer Richterin oder eines Richters.
Zu berücksichtigen sind Kinder, für die nach dem Einkommensteuergesetz oder nach dem Bundeskindergeldgesetz Kindergeld zusteht oder ohne Berücksichtigung des § 64 oder des § 65 des Einkommensteuergesetzes oder des § 3 oder des § 4 des Bundeskindergeldgesetzes zustehen würde.
In den Haushalt aufgenommene Kinder von eingetragenen Lebenspartnerinnen oder eingetragenen Lebenspartnern stehen den in den Haushalt aufgenommenen Kindern von Ehegattinnen und Ehegatten gleich; § 32 Absatz 3 bis 5 des Einkommensteuergesetzes gilt entsprechend.
Die Entscheidung der Familienkasse ist bindend.

(2) Stünde der Familienzuschlag auch einer anderen Person zu, die im öffentlichen Dienst tätig ist oder aufgrund einer Tätigkeit im öffentlichen Dienst nach beamtenrechtlichen Grundsätzen versorgungsberechtigt ist, so wird der Familienzuschlag gewährt, wenn und soweit der Beamtin, dem Beamten, der Richterin oder dem Richter das Kindergeld nach dem Einkommensteuergesetz oder nach dem Bundeskindergeldgesetz gewährt wird oder ohne Berücksichtigung des § 65 des Einkommensteuergesetzes oder des § 4 des Bundeskindergeldgesetzes vorrangig zu gewähren wäre.
Dem Familienzuschlag stehen sonstige entsprechende Leistungen oder das Mutterschaftsgeld gleich.
§ 6 Absatz 1 findet auf den Betrag keine Anwendung, wenn einer der Anspruchsberechtigten im Sinne des Satzes 1 vollbeschäftigt oder nach beamtenrechtlichen Grundsätzen versorgungsberechtigt ist oder beide Anspruchsberechtigte in Teilzeit beschäftigt sind und dabei zusammen die regelmäßige Arbeitszeit bei Vollbeschäftigung erreichen.

(3) Tätigkeit im öffentlichen Dienst im Sinne des Absatzes 2 ist eine Tätigkeit im Dienst des Bundes, eines Landes, einer Gemeinde, eines  Gemeindeverbandes oder sonstiger Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts oder deren Verbänden.
Ausgenommen ist eine Tätigkeit bei öffentlich-rechtlichen Religionsgemeinschaften oder ihren Verbänden, sofern nicht bei organisatorisch selbstständigen Einrichtungen die Voraussetzungen des Satzes 3 erfüllt sind.
Einer Tätigkeit im öffentlichen Dienst steht eine Tätigkeit im Dienst einer zwischenstaatlichen oder überstaatlichen Einrichtung gleich, an der das Land oder eine andere der in Satz 1 bezeichneten Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts oder einer der dort bezeichneten Verbände durch Zahlung von Beiträgen oder Zuschüssen oder in anderer Weise beteiligt ist.
Einer Tätigkeit im öffentlichen Dienst steht ferner gleich eine Tätigkeit im Dienst eines sonstigen Arbeitgebers, der die für den öffentlichen Dienst geltenden Tarifverträge oder Tarifverträge wesentlich gleichen Inhalts oder die darin oder in Besoldungsgesetzen über Familienzuschläge oder Sozialzuschläge getroffenen Regelungen oder vergleichbare Regelungen anwendet, wenn das Land oder eine andere der in Satz 1 bezeichneten Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts oder Verbände durch Zahlung von Beiträgen oder Zuschüssen oder in anderer Weise beteiligt ist.

(4) Der Familienzuschlag wird vom Ersten des Monats an gezahlt, in den das hierfür maßgebende Ereignis fällt.
Er wird nicht mehr gezahlt für den Monat, in dem die Anspruchsvoraussetzungen an keinem Tag erfüllt waren.

(5) Die Bezügestellen des öffentlichen Dienstes und die Familienkassen dürfen die zur Durchführung dieser Vorschrift erforderlichen personenbezogenen Daten erheben und untereinander austauschen.
Dies gilt entsprechend für den Austausch von Daten anderer Personen.
Soweit zur Durchführung dieser Vorschrift die Erhebung personenbezogener Daten der Kinder oder anderer Personen nach Absatz 2 erforderlich ist, dürfen diese bei den berechtigten Beamtinnen, Beamten, Richterinnen und Richtern erhoben werden.

### Abschnitt 4   
Sonstige Besoldungsbestandteile, Ausgleich bei Verringerung der Dienstbezüge

#### Unterabschnitt 1   
Sonstige Besoldungsbestandteile

##### § 41   
Amtszulagen und Stellenzulagen

(1) Für herausgehobene Funktionen können Amtszulagen und Stellenzulagen vorgesehen werden.
Sie dürfen 75 Prozent des Unterschiedsbetrags zwischen dem Endgrundgehalt der Besoldungsgruppe der Beamtin, des Beamten, der Richterin oder des Richters und dem Endgrundgehalt der nächsthöheren Besoldungsgruppe nicht übersteigen, soweit gesetzlich nichts anderes bestimmt ist.

(2) Die Amtszulagen sind unwiderruflich und ruhegehaltfähig.
Sie gelten als Bestandteil des Grundgehalts.

(3) Die Stellenzulagen dürfen nur für die Dauer der Wahrnehmung der herausgehobenen Funktion gewährt werden.
Sie sind widerruflich und nur ruhegehaltfähig, wenn dies gesetzlich bestimmt ist.

##### § 42   
Zulage für Lehrkräfte mit besonderen Funktionen

Die Landesregierung wird ermächtigt, durch Rechtsverordnung zu regeln, dass Lehrkräfte mit ständigen Aufgaben im Rahmen der Lehrerausbildung, Lehrerfortbildung und der Schulvisitation sowie Lehrertrainerinnen und Lehrertrainer, deren Tätigkeit sich aus den ihrer Lehrbefähigung entsprechenden Aufgaben heraushebt, eine Stellenzulage nach Anlage 8 erhalten.
Eine Stellenzulage darf nur vorgesehen werden, wenn die Wahrnehmung der Funktion nicht schon bei der Einstufung des Amtes berücksichtigt ist.

##### § 43   
Leistungsprämien und Leistungszulagen

(1) Die Landesregierung wird ermächtigt, zur Abgeltung von herausragenden besonderen Leistungen die Gewährung von Leistungsprämien als Einmalzahlungen und Leistungszulagen an Beamtinnen und Beamte in Besoldungsgruppen der Besoldungsordnungen A und B durch Rechtsverordnung zu regeln.

(2) Die Gesamtzahl der in einem Kalenderjahr bei einem Dienstherrn vergebenen Leistungsprämien und Leistungszulagen darf 15 Prozent der Zahl der bei dem Dienstherrn vorhandenen Beamtinnen und Beamten der Besoldungsordnungen A und B nicht übersteigen.
Die Überschreitung des Prozentsatzes nach Satz 1 ist in dem Umfang zulässig, in dem von der Möglichkeit der Vergabe von Leistungsstufen nach § 25 Absatz 5 kein Gebrauch gemacht wird.
In der Rechtsverordnung nach Absatz 1 kann zugelassen werden, dass bei Dienstherren mit weniger als sieben Beamtinnen und Beamten in jedem Kalenderjahr einer Beamtin oder einem Beamten eine Leistungsprämie oder eine Leistungszulage gewährt werden kann.
Leistungsprämien und Leistungszulagen sind nicht ruhegehaltfähig; erneute Bewilligungen sind möglich.
Die Zahlung von Leistungszulagen ist zu befristen; bei Leistungsabfall sind sie zu widerrufen.
Leistungsprämien dürfen das Anfangsgrundgehalt der Besoldungsgruppe der Beamtin oder des Beamten, Leistungszulagen dürfen monatlich 7 Prozent des Anfangsgrundgehalts nicht übersteigen.

(3) Leistungsprämien und Leistungszulagen können nur im Rahmen besonderer haushaltsrechtlicher Regelungen gewährt werden.
In der Rechtsverordnung nach Absatz 1 sind Anrechnungs- oder Ausschlussvorschriften zu Zahlungen, die aus demselben Anlass geleistet werden, vorzusehen.
In der Rechtsverordnung nach Absatz 1 kann vorgesehen werden, dass Leistungsprämien und Leistungszulagen, die an mehrere Beamtinnen und Beamte wegen ihrer wesentlichen Beteiligung an einer durch enges arbeitsteiliges Zusammenwirken erbrachten Leistung vergeben werden, zusammen nur als eine Leistungsprämie oder Leistungszulage im Sinne des Absatzes 2 Satz 1 gelten.
Leistungsprämien und Leistungszulagen nach Satz 3 dürfen zusammen 250 Prozent des in Absatz 2 Satz 6 geregelten Umfangs nicht übersteigen; maßgebend ist die höchste Besoldungsgruppe der an der Leistung wesentlich beteiligten Beamtinnen und Beamten.
Für Teilprämien und Teilzulagen, die sich nach den Sätzen 3 und 4 für die einzelnen Beamtinnen und Beamten ergeben, gilt Absatz 2 Satz 6 entsprechend.
Bei Übertragung eines anderen Amtes mit höherem Endgrundgehalt (Grundgehalt) oder bei Gewährung einer Amtszulage können in der Rechtsverordnung nach Absatz 1 Anrechnungs- oder Ausschlussvorschriften zu Leistungszulagen vorgesehen werden.

##### § 44   
Zulage für die Wahrnehmung eines höherwertigen Amtes

(1) Werden einer Beamtin oder einem Beamten die Aufgaben eines höherwertigen Amtes vorübergehend vertretungsweise übertragen, wird nach 18 Monaten der ununterbrochenen Wahrnehmung dieser Aufgaben eine Zulage gewährt, wenn in diesem Zeitpunkt die haushaltsrechtlichen und laufbahnrechtlichen Voraussetzungen für die Übertragung des höherwertigen Amtes vorliegen.
Die Zulage wird nur gewährt, wenn eine dauerhafte Übertragung der Aufgaben des höherwertigen Amtes aufgrund einer gerichtlichen Entscheidung nicht erfolgen kann oder die dauerhafte Übertragung eines Schulleitungsamtes wegen absehbarer Veränderungen der Schülerzahlen oder der Schulstrukturen nicht erfolgen soll.
Satz 2 gilt entsprechend, wenn ein sonstiges Amt wegen wesentlicher Änderungen des Aufbaus oder der Aufgaben einer Behörde oder der Verschmelzung von Behörden nicht auf Dauer übertragen werden soll.

(2) Die Zulage wird bis zu einer Dauer von drei Jahren gewährt.

(3) Die Zulage wird gewährt in Höhe des Unterschiedsbetrags zwischen dem Grundgehalt der Besoldungsgruppe des übertragenen Amtes und dem Grundgehalt der Besoldungsgruppe des höherwertigen Amtes.
Auf die Zulage ist eine Stellenzulage nach Nummer 13 der Vorbemerkungen der Besoldungsordnungen A und B der Anlage 1 anzurechnen, wenn sie in dem höherwertigen Amt nicht zustünde.

##### § 45   
Erschwerniszulagen

Die Landesregierung wird ermächtigt, durch Rechtsverordnung die Gewährung von Zulagen zur Abgeltung besonderer, bei der Bewertung des Amtes oder bei der Regelung der Anwärterbezüge nicht berücksichtigter Erschwernisse (Erschwerniszulagen) zu regeln.
Die Erschwerniszulagen sind widerruflich und nicht ruhegehaltfähig.
Es kann bestimmt werden, in welchem Umfang durch die Gewährung von Erschwerniszulagen ein besonderer Aufwand mit abgegolten ist.

##### § 46   
Mehrarbeitsvergütung

(1) Die Landesregierung wird ermächtigt, durch Rechtsverordnung die Gewährung einer Vergütung für Mehrarbeit für Beamtinnen und Beamte zu regeln, soweit die Mehrarbeit nicht durch Dienstbefreiung ausgeglichen wird (§ 76 Absatz 2 des Landesbeamtengesetzes).
Die Vergütung darf nur für Bereiche vorgesehen werden, in denen nach Art der Dienstverrichtung eine Mehrarbeit messbar ist.
Die Höhe der Vergütung ist nach dem Umfang der tatsächlich geleisteten Mehrarbeit festzusetzen.
Sie ist unter Zusammenfassung von Besoldungsgruppen zu staffeln; für Teilzeitbeschäftigte können abweichende Regelungen getroffen werden.

(2) Die Landesregierung wird ermächtigt, durch Rechtsverordnung die Gewährung einer Ausgleichszahlung in Höhe der zum Zeitpunkt des Ausgleichsanspruchs geltenden Sätze der Mehrarbeitsvergütung für Beamtinnen und Beamte zu regeln, bei denen ein Arbeitszeitausgleich aus einer langfristigen ungleichmäßigen Verteilung der Arbeitszeit, während der eine von der für sie jeweils geltenden regelmäßigen Arbeitszeit abweichende Arbeitszeit festgelegt wurde, nicht oder nur teilweise möglich ist.
Abweichend von Satz 1 ist Beamtinnen und Beamten mit ermäßigter Arbeitszeit für die bis zum Umfang der regelmäßigen Arbeitszeit zusätzlich geleistete Arbeit anstelle einer Ausgleichszahlung in Höhe der Sätze der Mehrarbeitsvergütung eine Ausgleichszahlung in Höhe der Besoldung zu gewähren, auf die eine Beamtin oder ein Beamter mit entsprechend anteilig erhöhter Arbeitszeit im Zeitraum der zusätzlich geleisteten Arbeit Anspruch gehabt hätte.

##### § 47   
Vergütung für Beamtinnen und Beamte im Vollstreckungsdienst

(1) Die Landesregierung wird ermächtigt, durch Rechtsverordnung die Gewährung einer Vergütung für Gerichtsvollzieherinnen und Gerichtsvollzieher sowie für andere im Vollstreckungsdienst tätige Beamtinnen und Beamte zu regeln.
Maßstab für die Festsetzung der Vergütung sind die vereinnahmten Gebühren oder Beträge.

(2) Für die Vergütung können Höchstsätze für die einzelnen Vollstreckungsaufträge sowie für das Kalenderjahr festgesetzt werden.
Ein Teil der Vergütung kann für ruhegehaltfähig erklärt werden.
Es kann bestimmt werden, inwieweit mit der Vergütung ein besonderer Aufwand mit abgegolten ist.

(3) Das für Justiz zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit dem für das Besoldungsrecht zuständigen Mitglied der Landesregierung durch Rechtsverordnung die Abgeltung der den Gerichtsvollzieherinnen und Gerichtsvollziehern für die Verpflichtung zur Einrichtung und Unterhaltung eines Büros entstehenden Kosten zu regeln.

##### § 48   
Sonderzuschläge zur Sicherung der Funktions- und Wettbewerbsfähigkeit

(1) Zur Sicherung der Funktions- und Wettbewerbsfähigkeit dürfen nicht ruhegehaltfähige Sonderzuschläge gewährt werden, wenn ein bestimmter Dienstposten andernfalls insbesondere im Hinblick auf die fachliche Qualifikation sowie die Bedarfs- und Bewerberlage nicht anforderungsgerecht besetzt werden kann und die Deckung des Personalbedarfs dies im konkreten Fall erfordert.
Dies gilt auch zur Verhinderung der Abwanderung von Beamtinnen, Beamten, Richterinnen und Richtern aus dem Bereich eines Dienstherrn im Geltungsbereich dieses Gesetzes.
Die Gewährung der Sonderzuschläge nach Satz 2 setzt voraus, dass das Einstellungsinteresse eines anderen Dienstherrn oder Arbeitgebers glaubhaft gemacht wird.

(2) Der Sonderzuschlag darf monatlich 10 Prozent des Anfangsgrundgehalts der entsprechenden Besoldungsgruppe nicht übersteigen.
Grundgehalt und Sonderzuschlag dürfen zusammen das Endgrundgehalt nicht übersteigen.
Bei Beamtinnen und Beamten der Besoldungsgruppe W 1 darf der Sonderzuschlag monatlich 500 Euro nicht übersteigen.
Der Sonderzuschlag wird, wenn nichts anderes bestimmt ist, in fünf Schritten um jeweils 20 Prozent seines Ausgangsbetrags jährlich verringert, erstmals ein Jahr nach dem Entstehen des Anspruchs.
Abweichend von Satz 4 kann der Sonderzuschlag auch befristet bis zu drei Jahren als Festbetrag gewährt werden; ergänzend kann dann festgelegt werden, dass er aufgrund einer Beförderung auch vor Ablauf der Befristung wegfällt.
Der Sonderzuschlag kann rückwirkend höchstens für drei Monate gewährt werden.
Er kann nach vollständigem Wegfall erneut gewährt werden, wenn die Voraussetzungen des Absatzes 1 wieder oder noch vorliegen.
§ 6 Absatz 1 gilt entsprechend.

(3) Die Entscheidung über die Gewährung von Sonderzuschlägen trifft die oberste Dienstbehörde im Einvernehmen mit dem für das Besoldungsrecht zuständigen Ministerium.

##### § 48a   
Zuschlag bei Hinausschieben des Eintritts in den Ruhestand

(1) Bei einem Hinausschieben des Eintritts in den Ruhestand nach § 110 Absatz 7 in Verbindung mit § 45 Absatz 3 des Landesbeamtengesetzes wird längstens bis zum 31. Dezember 2025 ein Zuschlag gewährt, soweit nicht bei einer Teilzeitbeschäftigung mit ungleichmäßig verteilter Arbeitszeit eine Freistellungsphase vorliegt.
Der Zuschlag beträgt 400 Euro monatlich und ist nicht ruhegehaltfähig.
Er wird gewährt ab Beginn des Kalendermonats, der auf den Zeitpunkt des Erreichens der Altersgrenze folgt.
Bei einer Teilzeitbeschäftigung während des Hinausschiebens des Eintritts in den Ruhestand ist auf den Zuschlag § 6 Absatz 1 anzuwenden.

(2) Bei einem Hinausschieben des Eintritts in den Ruhestand in den Laufbahnen des Schuldienstes nach § 45 Absatz 3 des Landesbeamtengesetzes wird längstens bis zum 31. Dezember 2025 ein Zuschlag gewährt, soweit nicht bei einer Teilzeitbeschäftigung mit ungleichmäßig verteilter Arbeitszeit eine Freistellungsphase vorliegt.
Der Zuschlag beträgt 400 Euro monatlich und ist nicht ruhegehaltfähig.
Er wird gewährt ab Beginn des Kalendermonats, der auf das Ende des Schulhalbjahres, in dem die Regelaltersgrenze erreicht wird, folgt.
Bei einer Teilzeitbeschäftigung während des Hinausschiebens des Eintritts in den Ruhestand ist auf den Zuschlag § 6 Absatz 1 anzuwenden.

(3) Bei einem Hinausschieben des Eintritts in den Ruhestand in den Laufbahnen des Steuerverwaltungsdienstes in den Finanzämtern nach § 45 Absatz 3 des Landesbeamtengesetzes gelten die Regelungen des Absatzes 1 entsprechend.

(4) Berechtigt nach den Absätzen 1 bis 3 sind Beamtinnen und Beamte in Besoldungsgruppen der Besoldungsordnung A.

##### § 48b   
Attraktivitäts-Zuschlag für die Jahre 2017 bis 2020

(1) Zur Steigerung der Attraktivität der Tätigkeit in der öffentlichen Verwaltung im Land Brandenburg wird für die Jahre 2017 bis 2020 ein Sonderzuschlag gewährt.
Dieser ist mit den laufenden Bezügen für den Monat November zu zahlen.
Er ist befristet bis zum 31.
Dezember 2020.

(2) Voraussetzung für den Anspruch von Beamtinnen, Beamten, Richterinnen und Richtern ist, dass die Berechtigten am 1.
November in einem der in § 1 Absatz 1 bezeichneten Rechtsverhältnisse stehen und Anspruch auf Besoldung haben.

(3) Die Absätze 1 und 2 gelten für Versorgungsempfängerinnen und Versorgungsempfänger entsprechend.
Voraussetzung für den Anspruch von Versorgungsempfängerinnen und Versorgungsempfängern ist, dass ihnen für den ganzen Monat November laufende Versorgungsbezüge zustehen.
Versorgungsbezüge im Sinne des Satzes 2 sind Ruhegehalt, Witwengeld, Witwergeld, Waisengeld und Unterhaltsbeitrag.

(4) Der Sonderzuschlag beträgt für Beamtinnen, Beamte, Richterinnen und Richter 800 Euro im Jahr 2017, 600 Euro im Jahr 2018, 400 Euro im Jahr 2019 und 200 Euro im Jahr 2020.
Für Versorgungsempfängerinnen und Versorgungsempfänger beträgt der Sonderzuschlag 50 Prozent der in Satz 1 genannten Beträge; für Bezieherinnen und Bezieher von Witwengeld, Witwergeld, Waisengeld oder Unterhaltsbeiträgen finden die maßgebenden Anteilssätze vom Ruhegehalt Anwendung.
Beamtinnen und Beamte im Vorbereitungsdienst erhalten einen Sonderzuschlag in Höhe von 200 Euro im Jahr 2017, 150 Euro im Jahr 2018, 125 Euro im Jahr 2019 und 100 Euro im Jahr 2020.
§ 6 Absatz 1 ist nicht zu berücksichtigen.

(5) Haben Berechtigte nicht während des gesamten Kalenderjahres aufgrund einer Tätigkeit im Dienst eines öffentlich-rechtlichen Dienstherrn (§ 28) Dienst- oder Anwärterbezüge oder aus einem öffentlich-rechtlichen Dienstverhältnis Versorgungsbezüge (Absatz 3 Satz 3) erhalten, so vermindert sich der Sonderzuschlag für die Zeiten, für die keine Bezüge zugestanden haben.
Die Minderung beträgt für jeden vollen Kalendermonat ein Zwölftel.
Die Minderung unterbleibt für die Monate des Entlassungsjahres, in dem Wehrdienst oder Dienst in einem vergleichbaren Dienstverhältnis im Sinne von § 16 des Brandenburgischen Beamtenversorgungsgesetzes geleistet wird, wenn Berechtigte vor dem 1.
November entlassen worden sind und unverzüglich in den öffentlichen Dienst zurückkehren.
Für die Dauer einer Elternzeit unterbleibt die Minderung des Sonderzuschlags bis zur Vollendung des zwölften Lebensmonats des Kindes, wenn am Tag vor Antritt der Elternzeit Anspruch auf Dienstbezüge aus einem Rechtsverhältnis nach Satz 1 bestanden hat.

##### § 48c   
Zuschlag zur Gewinnung von IT-Fachkräften

(1) Zur anforderungsgerechten Besetzung eines Dienstpostens in der Informationstechnologie kann Beamtinnen und Beamten der Besoldungsordnung A im gehobenen Dienst ein Zuschlag gewährt werden (IT-Fachkräftegewinnungszuschlag).
Die Informationstechnologie nach Satz 1 umfasst elektronische Systeme, insbesondere zur Gewinnung, Speicherung und Verarbeitung von Informationen, sowie die IT-Sicherheit, Netzwerk- und Datenbankanwendungen und das Software Engineering.
Die reine Anwendung der Informationstechnologie stellt keine anspruchsbegründende Tätigkeit im Sinn von Satz 1 dar.
Im Übrigen gelten die Voraussetzungen des § 48 Absatz 1 Satz 1.

(2) Der Zuschlag beträgt monatlich bis zu 400 Euro.
Er vermindert sich nach fünf Jahren der tatsächlichen Zahlung um 40 Prozent, nach weiteren drei Jahren um 30 Prozent des Ausgangsbetrags und entfällt nach einer Gesamtbezugsdauer von insgesamt zehn Jahren.
§ 6 Absatz 1 gilt entsprechend.
Der Zuschlag entfällt bei einem Wechsel des Dienstpostens, wenn für den neuen Dienstposten die Voraussetzungen nach Absatz 1 nicht vorliegen.

(3) Der IT-Fachkräftegewinnungszuschlag wird nicht neben einem Zuschlag nach § 48 gewährt.

(4) Die Entscheidung über die Gewährung von Zuschlägen trifft die oberste Dienstbehörde.

##### § 49   
Andere Zulagen, Prämien und Vergütungen

(1) Andere als die in diesem Abschnitt geregelten Zulagen und Vergütungen dürfen nur gewährt werden, soweit dies gesetzlich bestimmt ist.
Vergütungen für Nebentätigkeiten im öffentlichen Dienst bleiben unberührt.

(2) Beamtinnen, Beamten, Richterinnen und Richtern des Landes können im Rahmen des Verwaltungsumbaus zur Förderung der Mobilitäts- und Qualifizierungsbereitschaft neben der Besoldung Prämien als Einmalzahlungen gewährt werden.
Das für das Besoldungsrecht zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit dem für das allgemeine öffentliche Dienstrecht zuständigen Mitglied der Landesregierung das Nähere unter Berücksichtigung der für die Tarifbeschäftigten des Landes geltenden Bestimmungen durch Rechtsverordnung zu regeln.
Die Rechtsverordnung ist zu befristen.

#### Unterabschnitt 2   
Ausgleich bei Verringerungen der Dienstbezüge

##### § 50   
Besoldungsanspruch bei Verleihung eines Amtes mit geringerem Grundgehalt

Verringert sich während eines Dienstverhältnisses nach § 1 Absatz 1 das Grundgehalt einer Beamtin, eines Beamten, einer Richterin oder eines Richters durch Verleihung eines anderen Amtes aus dienstlichen Gründen, die nicht von diesen zu vertreten sind, ist abweichend von § 19 das Grundgehalt zu zahlen, das bei einem Verbleiben in dem bisherigen Amt zugestanden hätte.
Veränderungen in der Bewertung des bisherigen Amtes bleiben unberücksichtigt.
Satz 1 gilt entsprechend für Amtszulagen, auch bei Übertragung einer anderen Funktion.
Die Sätze 1 und 2 gelten nicht, wenn ein Amt mit leitender Funktion im Beamtenverhältnis auf Probe übertragen wurde.

##### § 51   
Ausgleichszulagen bei Wegfall von Stellenzulagen

(1) Der Wegfall einer nach diesem Gesetz zustehenden Stellenzulage aus dienstlichen Gründen, die nicht von der Beamtin, dem Beamten, der Richterin oder dem Richter zu vertreten sind, wird ausgeglichen, wenn die Stellenzulage zuvor in einem Zeitraum von bis zu sieben Jahren insgesamt mindestens fünf Jahre zugestanden hat.
Die Ausgleichszulage wird auf den Betrag festgesetzt, der am Tag vor dem Wegfall zugestanden hat.
Jeweils nach Ablauf eines Jahres vermindert sich die Ausgleichszulage ab Beginn des Folgemonats um 20 Prozent des festgesetzten Betrags.
Erhöhen sich die Dienstbezüge wegen des Anspruchs auf eine Stellenzulage, wird diese auf die Ausgleichszulage angerechnet.
Bezugszeiten von Stellenzulagen, die bereits zu einem Anspruch auf eine Ausgleichszulage geführt haben, bleiben für weitere Ausgleichsansprüche unberücksichtigt.

(2) Bestand innerhalb des Zeitraumes nach Absatz 1 Satz 1 ein Anspruch auf mehrere Stellenzulagen für einen Gesamtzeitraum von mindestens fünf Jahren, ohne dass eine der Stellenzulagen allein für fünf Jahre zugestanden hat, gilt Absatz 1 mit der Maßgabe, dass der Wegfall der Stellenzulage mit dem jeweils niedrigsten Betrag ausgeglichen wird.

(3) Erfolgte der Wegfall einer Stellenzulage infolge einer Versetzung nach § 30 Absatz 3 des Landesbeamtengesetzes, gilt Absatz 1 mit der Maßgabe, dass der Mindestzeitraum nach Absatz 1 Satz 1 und Absatz 2 zwei Jahre beträgt.

(4) Die Absätze 1 bis 3 gelten entsprechend, wenn eine Ruhegehaltsempfängerin oder ein Ruhegehaltsempfänger erneut in ein Beamtenverhältnis oder Richterverhältnis berufen wird oder wenn im unmittelbaren Zusammenhang mit einem Verwendungswechsel eine zuvor gewährte Stellenzulage nur noch mit einem geringeren Betrag zusteht und die jeweilige Zulagenvorschrift keinen anderweitigen Ausgleich vorsieht.

### Abschnitt 5   
Auslandsbesoldung

##### § 52   
Auslandsbesoldung

(1) Beamtinnen, Beamte, Richterinnen und Richter mit dienstlichem und tatsächlichem Wohnsitz im Ausland, der nicht einer Tätigkeit im Grenzverkehr dient, erhalten bei einer Verwendung im Ausland neben der Besoldung, die ihnen bei einer Verwendung im Inland zusteht, Auslandsbesoldung in entsprechender Anwendung der für Bundesbeamtinnen und Bundesbeamte geltenden Bestimmungen.

(2) Während der Verwendung im Ausland eintretende Änderungen der für Bundesbeamtinnen und Bundesbeamte geltenden Bestimmungen führen nicht zu einer Verminderung der Auslandsbesoldung.

(3) Soweit in den für Bundesbeamtinnen und Bundesbeamte geltenden Bestimmungen nach Absatz 1 bei Einzelfallentscheidungen die Zuständigkeit des Bundesministeriums des Innern vorgesehen ist, tritt an dessen Stelle das für das Besoldungsrecht zuständige Ministerium.

### Abschnitt 6   
Anwärterbezüge

##### § 53   
Anwärterbezüge

(1) Beamtinnen und Beamte auf Widerruf im Vorbereitungsdienst (Anwärterinnen und Anwärter) erhalten Anwärterbezüge.

(2) Zu den Anwärterbezügen gehören der Anwärtergrundbetrag (§ 55) und die Anwärtersonderzuschläge (§ 56).
Daneben werden der Familienzuschlag und die vermögenswirksamen Leistungen gewährt.
Zulagen und Vergütungen werden nur gewährt, wenn dies gesetzlich besonders bestimmt ist.

(3) Für Anwärterinnen oder Anwärter, die im Rahmen ihres Vorbereitungsdienstes ein Studium ableisten, kann die Gewährung der Anwärterbezüge von der Erfüllung von Auflagen abhängig gemacht werden.

##### § 54   
Anwärterbezüge nach Ablegung der Laufbahnprüfung

Endet das Beamtenverhältnis einer Anwärterin oder eines Anwärters kraft Rechtsvorschrift mit dem Bestehen oder endgültigen Nichtbestehen der Laufbahnprüfung, werden die Anwärterbezüge und der Familienzuschlag für die Zeit nach Ablegung der Prüfung bis zum Ende des laufenden Monats weitergewährt.
Wird bereits vor diesem Zeitpunkt ein Anspruch auf Bezüge aus einer hauptberuflichen Tätigkeit bei einem öffentlich-rechtlichen Dienstherrn (§ 28) oder bei einer Ersatzschule erworben, so werden die Anwärterbezüge und der Familienzuschlag nur bis zum Tag vor Beginn dieses Anspruchs belassen.

##### § 55   
Anwärtergrundbetrag

Der Anwärtergrundbetrag nach Anlage 7 richtet sich nach der Besoldungsgruppe des Eingangsamtes, in das die Anwärterin oder der Anwärter nach Abschluss des Vorbereitungsdienstes voraussichtlich eintritt.
Unterschiedliche Eingangsämter können betragsmäßig zusammengefasst werden.

##### § 56   
Anwärtersonderzuschläge

(1) Besteht ein erheblicher Mangel an qualifizierten Bewerberinnen oder Bewerbern, kann das für das Besoldungsrecht zuständige Ministerium Anwärtersonderzuschläge gewähren.
Sie dürfen 70 Prozent des Anwärtergrundbetrags nicht übersteigen.

(2) Der Anspruch auf Anwärtersonderzuschläge besteht nur, wenn die Anwärterin oder der Anwärter

1.  nicht vor dem Abschluss des Vorbereitungsdienstes oder wegen schuldhaften Nichtbestehens der Laufbahnprüfung ausscheidet und
2.  nach Bestehen der Laufbahnprüfung mindestens fünf Jahre in einem Beamtenverhältnis bei einem öffentlich-rechtlichen Dienstherrn (§ 28) in der Laufbahn verbleibt, für welche die Befähigung erworben wurde, oder, wenn das Beamtenverhältnis nach Bestehen der Laufbahnprüfung endet, in derselben Laufbahn in ein neues Beamtenverhältnis bei einem öffentlich-rechtlichen Dienstherrn (§ 28) für mindestens die gleiche Zeit eintritt.

(3) Werden die in Absatz 2 genannten Voraussetzungen aus Gründen, die von der Anspruchsberechtigten oder dem Anspruchsberechtigten zu vertreten sind, nicht erfüllt, ist der Anwärtersonderzuschlag in voller Höhe zurückzuzahlen.
Der Rückzahlungsbetrag vermindert sich für jedes nach Bestehen der Laufbahnprüfung abgeleistete Dienstjahr um jeweils ein Fünftel.
§ 13 bleibt unberührt.

##### § 57   
Anrechnung anderer Einkünfte

Erhält eine Anwärterin oder ein Anwärter ein Entgelt für eine andere Tätigkeit innerhalb oder außerhalb des öffentlichen Dienstes, wird das Entgelt auf die Anwärterbezüge angerechnet, soweit es diese übersteigt.

##### § 58   
Kürzung der Anwärterbezüge

(1) Die oberste Dienstbehörde oder die von ihr bestimmte Stelle kann den Anwärtergrundbetrag bis auf 30 Prozent des Grundgehalts, das einer Beamtin oder einem Beamten der entsprechenden Laufbahn in der ersten Stufe zusteht, herabsetzen, wenn die Anwärterin oder der Anwärter die vorgeschriebene Laufbahnprüfung nicht bestanden hat oder sich die Ausbildung aus einem von der Anwärterin oder dem Anwärter zu vertretenden Grund verzögert.

(2) Von der Kürzung ist abzusehen

1.  bei Verlängerung des Vorbereitungsdienstes infolge genehmigten Fernbleibens oder Rücktritts von der Prüfung,
2.  in besonderen Härtefällen.

(3) Wird eine Zwischenprüfung nicht bestanden oder ein sonstiger Leistungsnachweis nicht erbracht, so ist die Kürzung auf den sich daraus ergebenden Zeitraum der Verlängerung des Vorbereitungsdienstes zu beschränken.

### Abschnitt 7  
Vermögenswirksame Leistungen

##### § 59   
Vermögenswirksame Leistungen

(1) Beamtinnen, Beamte, Richterinnen und Richter erhalten vermögenswirksame Leistungen nach dem Fünften Vermögensbildungsgesetz.

(2) Vermögenswirksame Leistungen werden für die Kalendermonate gewährt, in denen den Berechtigten Dienstbezüge oder Anwärterbezüge zustehen und gezahlt werden.
Sie werden im Kalendermonat jeweils nur einmal gewährt.

(3) Der Anspruch auf vermögenswirksame Leistungen entsteht frühestens für den Kalendermonat, in dem die nach § 60 Absatz 3 erforderlichen Angaben mitgeteilt werden, und für die beiden vorangegangenen Monate desselben Kalenderjahres.

##### § 60   
Höhe, Fälligkeit und Anlage der vermögenswirksamen Leistungen

(1) Die vermögenswirksamen Leistungen betragen 6,65 Euro monatlich.
Teilzeitbeschäftigte erhalten den Betrag, der dem Verhältnis der ermäßigten zur regelmäßigen Arbeitszeit entspricht; dies gilt auch bei begrenzter Dienstfähigkeit nach § 27 des Beamtenstatusgesetzes.

(2) Für die Höhe der vermögenswirksamen Leistungen sind die Verhältnisse am Ersten des Kalendermonats maßgebend.
Wird das Dienstverhältnis nach dem Ersten des Kalendermonats begründet, ist für diesen Monat der Tag des Beginns des Dienstverhältnisses maßgebend.

(3) Die Berechtigten teilen der für die Festsetzung der Besoldung zuständigen Stelle schriftlich die Art der gewählten Anlage mit und geben hierbei, soweit dies nach der Art der Anlage erforderlich ist, das Unternehmen oder Institut sowie das Konto an, auf das die Leistungen eingezahlt werden sollen.

(4) Die vermögenswirksamen Leistungen sind bis zum Ablauf der auf den Monat der Mitteilung nach Absatz 3 folgenden drei Kalendermonate, danach monatlich im Voraus zu zahlen.

(5) Die nach § 11 Absatz 3 Satz 2 des Fünften Vermögensbildungsgesetzes erforderliche Zustimmung zum Wechsel der Anlage gilt als erteilt.

### Abschnitt 8   
Übergangs- und Schlussbestimmungen

#### Unterabschnitt 1   
Übergangsbestimmungen zu diesem Gesetz

##### § 61   
Anwendung dieses Gesetzes auf vorhandene Beamtinnen, Beamte, Richterinnen und Richter sowie Versorgungsempfängerinnen und Versorgungsempfänger

Dieses Gesetz gilt auch für die am Tag des Inkrafttretens und am Vortag vorhandenen Beamtinnen, Beamten, Richterinnen und Richter sowie nach Maßgabe der §§ 62, 63 und 66 für die Versorgungsempfängerinnen und Versorgungsempfänger der in § 1 Absatz 1 genannten Dienstherren.

##### § 62   
Überführung in die Ämter der Besoldungsordnungen A, B, W und R

(1) Die Beamtinnen, Beamten, Richterinnen und Richter werden aus den am Tag vor dem Inkrafttreten dieses Gesetzes in den Bundesbesoldungsordnungen in der am 31.
August 2006 geltenden Fassung oder in den Brandenburgischen Besoldungsordnungen ausgebrachten Ämtern in die entsprechenden Ämter der Besoldungsordnungen A, B, W und R dieses Gesetzes (Anlagen 1 bis 3) überführt.

(2) Die Beamtinnen und Beamten in der Besoldungsgruppe A 3 werden am Tag des Inkrafttretens dieses Gesetzes in die Ämter ihrer Laufbahn in der Besoldungsgruppe A 4 überführt.

(3) Die Absätze 1 und 2 gelten für Versorgungsempfängerinnen und Versorgungsempfänger entsprechend.

##### § 63   
Zuordnung der vorhandenen Beamtinnen, Beamten, Richterinnen und Richter der Besoldungsordnungen A und R zu den Stufen der Grundgehälter

(1) Die Beamtinnen und Beamten in den Besoldungsgruppen der Besoldungsordnung A werden in den Besoldungsgruppen, in die sie nach § 62 überführt werden, den Stufen des Grundgehalts nach der Anlage 4 zugeordnet.
Die Zuordnung erfolgt zu der Stufe der Besoldungsgruppe, deren Betrag dem Betrag des ihnen am Tag vor dem Inkrafttreten dieses Gesetzes zustehenden Grundgehalts entspricht.
Leistungsstufen nach § 27 Absatz 3 Satz 1 des Bundesbesoldungsgesetzes in der am 31. August 2006 geltenden Fassung bleiben bei der Zuordnung unberücksichtigt.
Bei Teilzeitbeschäftigten ist für die Zuordnung zu den Stufen das Grundgehalt maßgebend, das ihnen bei Vollzeitbeschäftigung zustehen würde.
Bei beurlaubten Beamtinnen und Beamten ohne Anspruch auf Besoldung ist das Grundgehalt maßgebend, das bei einer Beendigung der Beurlaubung am Tag vor dem Inkrafttreten dieses Gesetzes nach bisherigem Recht zustehen würde.

(2) Weist die Grundgehaltstabelle keinen entsprechenden Betrag im Sinne des Absatzes 1 Satz 2 aus, erfolgt die Zuordnung der in § 62 Absatz 2 genannten Beamtinnen und Beamten des einfachen Dienstes zu der Stufe der Besoldungsgruppe A 4 mit dem nächsthöheren Betrag.
Weist die Grundgehaltstabelle in anderen Fällen keinen entsprechenden Betrag aus, erfolgt die Zuordnung zu der Stufe der Besoldungsgruppe mit dem nächsthöheren Betrag.

(3) Mit der Zuordnung zu einer Stufe des Grundgehalts nach Absatz 1 beginnt das Aufsteigen in den Stufen nach § 25 Absatz 3.
Bereits in einer Stufe mit dem entsprechenden Grundgehaltsbetrag verbrachte Zeiten mit Anspruch auf Dienstbezüge werden angerechnet.
§ 26 Absatz 2 gilt entsprechend, soweit Zeiten nach § 26 Absatz 2 Nummer 2 bis 4 nicht schon nach § 28 Absatz 3 Nummer 1 bis 3 des Bundesbesoldungsgesetzes in der bis zum 31.
August 2006 geltenden Fassung berücksichtigt wurden.
Satz 2 gilt nicht für Zeiten einer Hemmung nach § 27 Absatz 3 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung.
In den Fällen des Absatzes 2 Satz 1 verkürzt sich die reguläre Laufzeit der Stufe der Besoldungsgruppe A 4, zu der die Zuordnung erfolgt, um die Monate, welche die Beamtinnen und Beamten in ihrer bisherigen Stufe nach dem am Tag vor dem Inkrafttreten dieses Gesetzes geltendem Recht bereits verbracht haben, längstens jedoch um die Laufzeit der jeweiligen Stufe in der Besoldungsgruppe A 4.
In den Fällen des Absatzes 2 Satz 2 verlängert sich die reguläre Laufzeit der Stufe, zu der die Zuordnung erfolgt, um die Monate, welche die Beamtinnen und Beamten nach dem am Tag vor dem Inkrafttreten dieses Gesetzes geltenden Recht noch benötigt hätten, um den Betrag dieser Stufe zu erreichen.

(4) Richterinnen, Richter, Staatsanwältinnen und Staatsanwälte in Ämtern der Besoldungsgruppen R 1 und R 2 der Besoldungsordnung R werden in den Besoldungsgruppen, in die sie nach § 62 überführt werden, den Stufen des Grundgehalts nach der Anlage 4 zugeordnet.
Absatz 1 Satz 1, 2, 4 und 5, Absatz 2 Satz 2 sowie Absatz 3 Satz 1, 2, 3 und 6 gelten entsprechend.
Absatz 3 Satz 1 gilt mit der Maßgabe, dass an die Stelle von § 25 Absatz 3 § 39 Satz 2 tritt.

(5) Die Absätze 1 bis 4 gelten für Versorgungsempfängerinnen und Versorgungsempfänger entsprechend.

##### § 63a   
Überführung vorhandener Beamtinnen und Beamter des Justizwachtmeisterdienstes,  
des mittleren Steuerverwaltungsdienstes, des mittleren Polizeivollzugsdienstes,  
des mittleren allgemeinen Vollzugsdienstes, des Werkdienstes und des Krankenpflegedienstes bei den Justizvollzugsanstalten und bestimmter Lehrkräfte

(1) Die am 31.
Dezember 2018 vorhandenen Beamtinnen und Beamten des Justizwachtmeisterdienstes in der Besoldungsgruppe A 4 werden am 1.
Januar 2019 in die Ämter ihrer Laufbahn in der Besoldungsgruppe A 5, die Beamtinnen und Beamten des Justizwachtmeisterdienstes in der Besoldungsgruppe A 5 in die Ämter ihrer Laufbahn in der Besoldungsgruppe A 6 und die Beamtinnen und Beamten des Justizwachtmeisterdienstes in der Besoldungsgruppe A 6 in die Ämter ihrer Laufbahn in der Besoldungsgruppe A 7 überführt.

(2) Die am 31.
Dezember 2018 vorhandenen Beamtinnen und Beamten des mittleren Steuerverwaltungsdienstes in der Besoldungsgruppe A 6 werden am 1.
Januar 2019 in die Ämter ihrer Laufbahn in der Besoldungsgruppe A 7 überführt.

(3) Die am 31.
Dezember 2018 vorhandenen Beamtinnen und Beamten des mittleren Polizeivollzugsdienstes und des mittleren allgemeinen Vollzugsdienstes, des Werkdienstes und des Krankenpflegedienstes bei den Justizvollzugsanstalten in der Besoldungsgruppe A 7 werden am 1. Januar 2019 in die Ämter ihrer Laufbahn in der Besoldungsgruppe A 8 überführt.

(4) Die folgenden am 31.
Dezember 2018 vorhandenen Lehrkräfte werden am 1. Januar 2019 in die Ämter ihrer jeweiligen Laufbahn wie folgt überführt:

1.  Lehrerinnen und Lehrer mit der Befähigung für das Lehramt für die Primarstufe in der Besoldungsgruppe A 12 in die Besoldungsgruppe A 13,
2.  Fachlehrerinnen und Fachlehrer
    1.  im Unterricht an Förderschulen,
    2.  im berufsbezogenen Unterricht an Schulen mit berufsbildenden Bildungsgängen und
    3.  im berufstheoretischen Unterricht an Schulen mit berufsbildenden Bildungsgängenin der Besoldungsgruppe A 11 kw in die Besoldungsgruppe A 12 kw,
3.  Lehrerinnen und Lehrer mit der Befähigung als Lehrer für die unteren Klassen in der Besoldungsgruppe A 11 kw in die Besoldungsgruppe A 12 kw,
4.  Fachlehrerinnen und Fachlehrer
    1.  im Unterricht an Förderschulen oder im berufstheoretischen Unterricht an Schulen mit berufsbegleitenden Bildungsgängen und
    2.  mit abgeschlossener Ingenieur- oder Fachhochschulausbildung, wenn sie vorgeschrieben ist oder, beim Fehlen laufbahnrechtlicher Vorschriften, gefordert wird,in der Besoldungsgruppe A 12 kw in die Besoldungsgruppe A 13 kw sowie
5.  Lehrerinnen und Lehrer
    1.  als Lehrerinnen oder Lehrer im allgemeinbildenden Schulunterricht, soweit nicht anderweitig eingereiht und soweit sie
        
        aa)
        
        Lehrerinnen oder Lehrer für die unteren Klassen der allgemeinbildenden polytechnischen Oberschule (Klassen 1 bis 4) mit zusätzlichem Diplomabschluss als Diplomlehrer für ein Fach der allgemeinbildenden polytechnischen Oberschule,
        
        bb)
        
        Diplomlehrerinnen oder Diplomlehrer mit einer Lehrbefähigung für ein Fach der allgemeinbildenden polytechnischen Oberschule (Klassen 5 bis 10),
        
        cc)
        
        entsprechende Fachlehrerinnen oder Fachlehrer mit Staatsexamen für ein Fach (Abschluss der Ausbildung vor 1970),
        
        dd)
        
        Diplomsportlehrerinnen oder Diplomsportlehrer (Deutsche Hochschule für Körperkultur in Leipzig), die mit der grundständigen Ausbildung oder über eine postgraduale Zusatzausbildung auch die Ausbildung und Prüfung in Methodik des Schulsportunterrichts nachgewiesen haben,
        
        ee)
        
        Lehrerinnen oder Lehrer, Fachlehrerinnen oder Fachlehrer sowie Diplomlehrerinnen oder Diplomlehrer mit einer Lehrbefähigung für ein Fach für die Oberstufe der allgemeinbildenden Schulen, für die Erweiterte Oberschule, mit postgradualer Qualifizierung für die Abiturstufe sowie gleichgestellte Lehrkräfte oder
        
        ff)
        
        Diplomlehrerinnen oder Diplomlehrer mit einer Lehrbefähigung für zwei Fächer, deren Examen nach dem Wegfall eines nicht mehr relevanten Fachs, zum Beispiel Staatsbürgerkunde, nicht mehr als ausreichend zu betrachten sind,
        
        sind und
    2.  mit der Befähigung für das Lehramt der Primarstufe bei entsprechender Verwendungin der Besoldungsgruppe A 12 kw in die Besoldungsgruppe A 13 kw.

(5) Die folgenden am 31.
Juli 2017 vorhandenen Lehrkräfte werden mit Wirkung vom 1. August 2017 in die Ämter ihrer jeweiligen Laufbahn wie folgt überführt:

1.  Lehrerinnen und Lehrer
    1.  mit der Befähigung für das Lehramt für die Bildungsgänge der Sekundarstufe I und der Primarstufe an allgemeinbildenden Schulen,
    2.  mit der Befähigung für das Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer) bei einer Schwerpunktbildung auf die Sekundarstufe I und
    3.  mit der Befähigung für das Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer) bei einer Schwerpunktbildung auf die Sekundarstufe II bei überwiegender Verwendung in der Sekundarstufe Iin der Besoldungsgruppe A 12 in die Besoldungsgruppe A 13,
2.  Zweite Konrektorinnen und Zweite Konrektoren einer Grundschule mit mehr als 540 Schülerinnen und Schülern in der Besoldungsgruppe A 12 mit Amtszulage in die Besoldungsgruppe A 14,
3.  Konrektorinnen und Konrektoren als die ständigen Vertreterinnen oder ständigen Vertreter der Leiterin oder des Leiters einer Grundschule mit mehr als 180 bis zu 360 Schülerinnen und Schülern in der Besoldungsgruppe A 12 mit Amtszulage in die Besoldungsgruppe A 14,
4.  Rektorinnen und Rektoren als Leiterinnen oder Leiter einer Grundschule mit bis zu 80 Schülerinnen und Schülern in der Besoldungsgruppe A 12 mit Amtszulage in die Besoldungsgruppe A 14,
5.  Rektorinnen und Rektoren als Leiterinnen oder Leiter einer Grundschule mit mehr als 80 bis zu 180 Schülerinnen und Schülern in der Besoldungsgruppe A 13 in die Besoldungsgruppe A 14,
6.  Konrektorinnen und Konrektoren als die ständigen Vertreterinnen oder ständigen Vertreter der Leiterin oder des Leiters einer Grundschule mit mehr als 360 Schülerinnen und Schülern in der Besoldungsgruppe A 13 mit Amtszulage in die Besoldungsgruppe A 14 mit Amtszulage,
7.  Rektorinnen und Rektoren als Leiterinnen oder Leiter einer Grundschule mit mehr als 180 bis zu 360 Schülerinnen und Schülern in der Besoldungsgruppe A 13 mit Amtszulage in die Besoldungsgruppe A 14 mit Amtszulage,
8.  Rektorinnen und Rektoren als Leiterinnen oder Leiter einer Grundschule mit mehr als 360 Schülerinnen und Schülern in der Besoldungsgruppe A 14 in die Besoldungsgruppe A 15,
9.  Rektorinnen und Rektoren an einer Gesamtschule oder an einer Oberschule als Leiterinnen oder Leiter des Primarstufenbereichs einer Gesamtschule oder einer Oberschule mit mehr als 180 bis zu 360 Schülerinnen und Schülern in der Primarstufe in der Besoldungsgruppe A 13 in die Besoldungsgruppe A 14,
10.  Rektorinnen und Rektoren an einer Gesamtschule oder an einer Oberschule als Leiterinnen oder Leiter des Primarstufenbereichs einer Gesamtschule oder einer Oberschule mit mehr als 360 Schülerinnen und Schülern in der Primarstufe in der Besoldungsgruppe A 13 mit Amtszulage in die Besoldungsgruppe A 14 mit Amtszulage sowie
11.  Lehrerinnen und Lehrer
    1.  als Lehrerinnen oder Lehrer im allgemeinbildenden Schulunterricht, soweit nicht anderweitig eingereiht und soweit sie
        
        aa)
        
        Diplomlehrer mit einer Lehrbefähigung für zwei Fächer der allgemeinbildenden polytechnischen Oberschule (Klassen 5 bis 10) und entsprechende Fachlehrer mit Staatsexamen für zwei Fächer (Abschluss der Ausbildung vor 1970) oder
        
        bb)
        
        Lehrer, Fachlehrer und Diplomlehrer mit einer Lehrbefähigung für zwei Fächer für die Oberstufe der allgemeinbildenden Schulen, für die Erweiterte Oberschule, mit postgradualer Qualifizierung für die Abiturstufe und bei Verwendung in der Primarstufe oder Sekundarstufe I,
        
        sind,
    2.  mit der Befähigung für das Lehramt der Sekundarstufe I bei entsprechender Verwendung und
    3.  mit der Befähigung für das Lehramt für die Primarstufe und die Sekundarstufe I bei entsprechender Verwendungin der Besoldungsgruppe A 12 kw in die Besoldungsgruppe A 13 kw.

##### § 64   
Übergangsregelungen zur Altersteilzeit

(1) Bei Altersteilzeit nach § 133 des Landesbeamtengesetzes sowie nach § 6c des Brandenburgischen Richtergesetzes in der am 13.
Juli 2011 geltenden Fassung wird zusätzlich zur Besoldung bei Teilzeitbeschäftigung ein nicht ruhegehaltfähiger Altersteilzeitzuschlag gewährt, soweit die Altersteilzeit mit mindestens der Hälfte der bisherigen Arbeitszeit durchgeführt wird, die für die Bemessung der ermäßigten Arbeitszeit während der Altersteilzeit zugrunde gelegt worden ist.

(2) Der Zuschlag wird gewährt in Höhe des Unterschiedsbetrags zwischen der Nettobesoldung, die sich aus dem Umfang der Teilzeitbeschäftigung ergibt, und 83 Prozent der Nettobesoldung, die nach der bisherigen Arbeitszeit, die für die Bemessung der ermäßigten Arbeitszeit während der Altersteilzeit zugrunde gelegt worden ist, bei Beamtinnen und Beamten mit begrenzter Dienstfähigkeit nach § 27 des Beamtenstatusgesetzes unter Berücksichtigung des § 7, zustehen würde.
Für die Bemessung der bisherigen Arbeitszeit gilt § 133 Absatz 1 Satz 1 des Landesbeamtengesetzes.

(3) Brutto- und Nettobesoldung im Sinne des Absatzes 2 sind das Grundgehalt, der Familienzuschlag, Amtszulagen, Stellenzulagen, Zuschüsse zum Grundgehalt für Professorinnen und Professoren an Hochschulen, Überleitungszulagen und Ausgleichszulagen, die wegen des Wegfalls oder der Verminderung dieser Bezüge zustehen.
Zur Ermittlung der letztgenannten Nettobesoldung ist die Bruttobesoldung um die Lohnsteuer entsprechend der individuellen Steuerklasse und den Solidaritätszuschlag zu vermindern; Freibeträge oder sonstige individuelle Merkmale bleiben unberücksichtigt.

(4) Steuerfreie Bezüge, Erschwerniszulagen und Vergütungen werden entsprechend dem Umfang der tatsächlich geleisteten Tätigkeit gewährt.

(5) Wenn die Altersteilzeit mit ungleichmäßiger Verteilung der Arbeitszeit (Blockmodell) vorzeitig endet und die insgesamt gezahlten Altersteilzeitbezüge geringer sind als die Besoldung, die nach der tatsächlichen Beschäftigung ohne Altersteilzeit zugestanden hätte, ist ein Ausgleich in Höhe des Unterschiedsbetrags zu gewähren.
Dabei werden auch Zeiten ohne Dienstleistung in der Ansparphase berücksichtigt, soweit sie insgesamt sechs Monate nicht überschreiten.

##### § 65   
Übergangsregelungen zur Professorenbesoldung

(1) Für Beamtinnen und Beamte der Bundesbesoldungsordnung C, die sich am 1. Januar 2005 in einem Amt der Besoldungsordnung C befunden haben, findet § 77 Absatz 2 und 3 des Bundesbesoldungsgesetzes in der am 31. August 2006 geltenden Fassung weiter Anwendung.
Die sich aus Satz 1 unter Berücksichtigung der bisherigen Anpassungen und Änderungen des Besoldungsrechts ergebenden Beträge der Dienstbezüge werden in der Anlage 5 ausgewiesen.

(2) Professorinnen und Professoren, denen gemäß § 77 Absatz 2 Satz 2 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung auf ihren Antrag ein Amt der Besoldungsordnung W übertragen wird, können aus diesem Anlass Leistungsbezüge in entsprechender Anwendung des § 31 erhalten.

(3) Professorinnen und Professoren der Besoldungsgruppen C 2 und C 3 wird gemäß § 77 Absatz 2 Satz 2 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung auf Antrag ein Amt der Besoldungsgruppe W 2 übertragen.
Hiervon abweichend kann Professorinnen und Professoren der Besoldungsgruppen C 2 und C 3 gemäß § 77 Absatz 2 Satz 2 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung auf Antrag ein Amt der Besoldungsgruppe W 3 übertragen werden, wenn ihre Professur nach Maßgabe des Entwicklungsplans der Hochschule nach der Besoldungsgruppe W 3 bewertet ist und sie den Ruf einer anderen Hochschule auf eine Professur der Besoldungsgruppe W 3 vorlegen.
Die Übertragung erfolgt auf Vorschlag des Senats durch die für die Angelegenheiten der Fachhochschulen, deren Ausbildungsgänge ausschließlich auf den öffentlichen Dienst ausgerichtet sind, zuständigen Mitglieder der Landesregierung und das für Hochschulen zuständige Mitglied der Landesregierung jeweils für ihren Bereich.
Die nach Satz 3 zuständigen Mitglieder der Landesregierung lehnen den Vorschlag ab, wenn die gesetzlichen Voraussetzungen für die Übertragung des Amtes nicht vorliegen oder haushaltswirtschaftliche Belange der Übertragung entgegenstehen.

##### § 66   
Übergangsregelungen zum Familienzuschlag

(1) Die §§ 39 bis 41 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung sind bis zum 31. Dezember 2014 weiter anzuwenden mit der Maßgabe, dass sich die Höhe des Familienzuschlags aus den in dem Zeitraum vom 1.
Januar 2014 bis 30. Juni 2014 und aus den in dem Zeitraum vom 1. Juli 2014 bis 31.
Dezember 2014 jeweils gültigen Beträgen der Anlage 6 zu diesem Gesetz ergibt.
Bestimmungen dieser Vorschriften, die sich auf das Bestehen oder frühere Bestehen einer Ehe beziehen, sind auf das Bestehen oder frühere Bestehen einer eingetragenen Lebenspartnerschaft sinngemäß anzuwenden.
Bestimmungen, die sich auf Eheleute und deren Angehörige beziehen, sind auf durch eingetragene Lebenspartnerschaft verbundene Personen und deren Angehörige sinngemäß anzuwenden.
Bei der Gewährung kinderbezogener Leistungen stehen in den Haushalt aufgenommene Kinder von durch eingetragene Lebenspartnerschaft verbundenen Personen den in den Haushalt aufgenommenen Kindern von Eheleuten gleich.

(2) Beamtinnen, Beamte, Richterinnen und Richter, denen am 31.
Dezember 2014 ein Familienzuschlag der Stufe 1 nach § 40 Absatz 1 des Bundesbesoldungsgesetzes in der am 31. August 2006 geltenden Fassung gewährt wurde und denen bei Fortgeltung des bisherigen Rechts am 1.
Januar 2015 weiterhin ein Familienzuschlag der Stufe 1 zustehen würde, erhalten eine Ausgleichszulage.

(3) Die Ausgleichszulage wird in Höhe des Unterschiedsbetrags zwischen dem am 31. Dezember 2014 zustehenden Grundgehalt oder dem Anwärtergrundbetrag zuzüglich des Familienzuschlags der Stufe 1 und dem am 1.
Januar 2015 zustehenden Grundgehalt oder dem Anwärtergrundbetrag gewährt.
Maßgebend für die Bemessung der Ausgleichszulage sind die persönlichen Verhältnisse am 31.
Dezember 2014.
Bei Teilzeitbeschäftigten sind die Dienstbezüge maßgebend, die ihnen bei einer Vollzeitbeschäftigung zustehen würden.
Bei beurlaubten Beamtinnen, Beamten, Richterinnen und Richtern ohne Anspruch auf Besoldung sind die Dienstbezüge maßgebend, die bei einer Beendigung der Beurlaubung am 31.
Dezember 2014 zustehen würden.

(4) Die Absätze 1 bis 3 gelten sinngemäß für Versorgungsempfängerinnen und Versorgungsempfänger.

##### § 67   
Sonstige Übergangsvorschriften

(1) Soweit am Tag vor dem Inkrafttreten dieses Gesetzes Ausgleichs- oder Überleitungszulagen nach früherem Recht gewährt werden, sind diese, solange die bisherigen Anspruchsvoraussetzungen erfüllt sind, in Höhe des am Tag vor dem Inkrafttreten dieses Gesetzes zustehenden Betrags fortzuzahlen, jedoch ab dem Zeitpunkt des Inkrafttretens dieses Gesetzes nach Maßgabe des § 51 Absatz 1 Satz 3 und 4 zu vermindern.
Soweit Ausgleichszulagen nach Satz 1 wegen der Verleihung eines Amtes mit geringerem Endgrundgehalt oder der Verringerung einer Amtszulage zustehen, findet ab dem Tag des Inkrafttretens dieses Gesetzes § 50 Anwendung.

(2) Beamtinnen und Beamte, die am Tag vor dem Inkrafttreten dieses Gesetzes eine Leistungsstufe nach § 27 Absatz 3 Satz 1 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung erhalten, wird die nächsthöhere Stufe des Grundgehalts für den Zeitraum, um den nach bisherigem Recht die Erhöhung des Grundgehalts vorgezogen wurde, weiter gewährt.

(3) Auslandsdienstbezüge, die Beamtinnen, Beamten, Richterinnen oder Richtern am Tag vor dem Inkrafttreten dieses Gesetzes nach dem Fünften Abschnitt des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung zustehen, werden bis zur Dauer von einem Jahr nach dem Inkrafttreten dieses Gesetzes in der bisherigen Höhe weitergewährt, soweit sie die Auslandsbesoldung nach § 52 übersteigen und solange die bisherigen Anspruchsvoraussetzungen weiter erfüllt sind.

(4) Bei Beamtinnen und Beamten auf Widerruf im Vorbereitungsdienst, denen am Tag vor dem Inkrafttreten dieses Gesetzes vermögenswirksame Leistungen nach § 2 Absatz 2 des Gesetzes über vermögenswirksame Leistungen für Beamte, Richter, Berufssoldaten und Soldaten auf Zeit in der am 31.
August 2006 geltenden Fassung zustanden, werden die vermögenswirksamen Leistungen fortgezahlt, solange die bisherigen Anspruchsvoraussetzungen erfüllt sind.

(5) Ansprüche auf Besoldung, die vor dem Inkrafttreten dieses Gesetzes entstanden sind, verjähren nach den bisherigen Vorschriften.

(6) Zulagen nach § 45 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung, die am Tag vor dem Inkrafttreten dieses Gesetzes gewährt werden, sind in Höhe des am Tag vor dem Inkrafttreten dieses Gesetzes zustehenden Betrags fortzuzahlen, solange die bisherigen Anspruchsvoraussetzungen erfüllt sind.
Zulagen nach § 46 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung, die am Tag vor dem Inkrafttreten dieses Gesetzes zustanden, sind fortzuzahlen, solange die bisherigen Anspruchsvoraussetzungen erfüllt sind, längstens bis zum 31.
Dezember 2016.

#### Unterabschnitt 2   
Schlussbestimmungen

##### § 68   
Künftig wegfallende Ämter

Die künftig wegfallenden Ämter sind in Anlage 1 aufgeführt.
Diese Ämter dürfen Beamtinnen, Beamten, Richterinnen und Richtern nicht mehr verliehen werden, es sei denn, der Inhaberin oder dem Inhaber eines solchen Amtes wird im Wege der Ernennung ein künftig wegfallendes Amt verliehen, weil eine Ernennung in ein in den Besoldungsordnungen ausgebrachtes anderes Amt nicht möglich ist.

##### § 69   
Besondere Vorschriften für den Bereich der Sozialversicherung

(1) Die unter der Aufsicht des Landes stehenden Körperschaften des öffentlichen Rechts im Bereich der Sozialversicherung haben bei Aufstellung ihrer Dienstordnungen nach den §§ 351 bis 357, § 413 Absatz 2, § 414b der Reichsversicherungsordnung, §§ 144 bis 147 des Siebten Buches Sozialgesetzbuch für die dienstordnungsmäßig Angestellten

1.  den Rahmen dieses Gesetzes, insbesondere das für die Beamtinnen und Beamten des Landes geltende Besoldungs- und Stellengefüge, einzuhalten und
2.  alle weiteren Geld- und geldwerten Leistungen sowie die Versorgung im Rahmen und nach den Grundsätzen der für die Beamtinnen und Beamten des Landes geltenden Bestimmungen zu regeln.

(2) Die besoldungsrechtliche Einstufung der Dienstposten in der Geschäftsführung der landesunmittelbaren Träger der gesetzlichen Unfallversicherung darf die Besoldungsgruppe A 16 nicht überschreiten.
Die stellvertretende Geschäftsführerin oder der stellvertretende Geschäftsführer ist jeweils mindestens eine Besoldungsgruppe niedriger einzustufen als die Geschäftsführerin oder der Geschäftsführer.

##### § 70   
Weitergeltung von Rechtsvorschriften und Zuständigkeitsregelungen

(1) Das für das Besoldungsrecht zuständige Mitglied der Landesregierung erlässt im Einvernehmen mit dem für Inneres zuständigen Mitglied der Landesregierung die allgemeinen Verwaltungsvorschriften zu diesem Gesetz.
Bis zum Erlass von Verwaltungsvorschriften nach Satz 1 sind die Verwaltungsvorschriften zum Bundesbesoldungsgesetz und zum Brandenburgischen Besoldungsgesetz entsprechend anzuwenden.

(2) Die Landesregierung bestimmt durch Rechtsverordnung die Behörden, die die Besoldung der Beamtinnen, Beamten, Richterinnen und Richter des Landes festsetzen.
Für die Beamtinnen und Beamten der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts setzt die von der obersten Dienstbehörde bestimmte Stelle die Besoldung fest.

(3) Soweit die Landesregierung oder eine oberste Landesbehörde nach diesem Gesetz ermächtigt ist, durch Rechtsverordnung bestimmte Bereiche des Besoldungsrechts zu regeln, gelten die bisherigen Vorschriften für diese Bereiche bis zum Inkrafttreten der jeweiligen Rechtsverordnung weiter.
Für die Beträge der Mehrarbeitsvergütung und der Erschwerniszulagen gelten bis zum Inkrafttreten der jeweiligen Rechtsverordnung § 3 des Brandenburgischen Besoldungs- und Versorgungsanpassungsgesetzes 2013/2014 vom 15.
Oktober 2013 (GVBl.
I Nr. 28), das durch Artikel 10 des Gesetzes vom 20.
November 2013 (GVBl.
I Nr.
32) geändert worden ist, sowie die Anlagen 15 und 16 des Brandenburgischen Besoldungs- und Versorgungsanpassungsgesetzes 2008 in der Fassung der Bekanntmachung vom 3. November 2011 (GVBl.
I Nr. 27) weiter.

**Anlage 1**  
(zu § 20 Absatz 2, § 44 Absatz 3, § 62 Absatz 1, § 68 Satz 1)

### Besoldungsordnungen A und B (BbgBesO A und B)

### Vorbemerkungen

#### I.
Allgemeine Vorbemerkungen

##### 1 Amtsbezeichnungen

1.1 Die in der Besoldungsordnung A gesperrt gedruckten Amtsbezeichnungen sind Grundamtsbezeichnungen.
Den Grundamtsbezeichnungen können Zusätze, die

1.    auf den Dienstherrn oder den Verwaltungsbereich,
2.    auf die Laufbahn,
3.    auf die Fachrichtung

hinweisen, beigefügt werden.
Die Grundamtsbezeichnungen „Rätin“ oder „Rat“, „Oberrätin“ oder „Oberrat“, „Direktorin“ oder „Direktor“ und „Leitende Direktorin“ oder „Leitender Direktor“ dürfen nur in Verbindung mit einem Zusatz nach Satz 2 verliehen werden.

1.2 Über die Beifügung von Zusätzen zu den Grundamtsbezeichnungen entscheidet für die Beamtinnen und Beamten des Landes das für das Besoldungsrecht zuständige Mitglied der Landesregierung im Einvernehmen mit dem für das allgemeine öffentliche Dienstrecht zuständigen Mitglied der Landesregierung, für die Beamtinnen und Beamten der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts das für das allgemeine öffentliche Dienstrecht zuständige Mitglied der Landesregierung im Einvernehmen mit dem für das Besoldungsrecht zuständigen Mitglied der Landesregierung.

##### 2 Lehrkräfte

2.1 Lehrkräfte mit einer Lehrbefähigung nach dem Recht der Deutschen Demokratischen Republik werden nach Maßgabe dieses Gesetzes in die Besoldungsordnung A eingestuft.

2.2 Die besoldungsrechtliche Einstufung für Lehrkräfte nach Nummer 2.1 richtet sich nach der Lehrbefähigung sowie gegebenenfalls nach einer Ergänzungsprüfung nach Maßgabe der Fußnoten zu den Besoldungsgruppen und nach der Verwendung.

2.3 Lehrkräfte führen bei Verwendung im Geschäftsbereich der für Bildung zuständigen obersten Dienstbehörde nach Maßgabe der Schullaufbahnverordnung die für den Schuldienst oder Schulaufsichtsdienst vorgesehenen Amtsbezeichnungen.

2.4 Lehrkräfte dürfen an Grundschulen, weiterführenden allgemeinbildenden Schulen oder Oberstufenzentren in die jeweiligen Ämter des Förderschulbereichs eingestuft werden, wenn sie hierfür die entsprechende Lehrbefähigung besitzen und in den genannten Schulformen Schülerinnen und Schüler mit sonderpädagogischem Förderbedarf unterrichten.

2.5 An Schulen, an denen Schülerinnen und Schüler mit und ohne sonderpädagogischem Förderbedarf unterrichtet werden (Schulen in Mischform), kann die Schulleitung aus Lehrkräften mit einer Laufbahnbefähigung an allgemeinbildenden Schulen und aus Lehrkräften mit einer Laufbahnbefähigung an Förderschulen gebildet werden.
Ein Laufbahnwechsel ist damit nicht verbunden.
Dabei wird für die Einstufung der Funktionsämter eine lernbehinderte Schülerin oder ein lernbehinderter Schüler wie zwei Schülerinnen oder Schüler ohne sonderpädagogischen Förderbedarf berücksichtigt.
Eine Schülerin oder ein Schüler mit sonstigem sonderpädagogischen Förderbedarf wird wie zwei lernbehinderte Schülerinnen oder lernbehinderte Schüler oder wie vier Schülerinnen oder Schüler ohne sonderpädagogischen Förderbedarf berücksichtigt.

#### II.
Zulagen

##### 3 Zulage für Beamtinnen und Beamte als flugzeugtechnisches Personal

3.1 Beamtinnen und Beamte in einer Verwendung als flugzeugtechnisches Personal erhalten als erste Spezialistinnen oder erste Spezialisten oder in höherwertiger Funktion eine Stellenzulage nach Anlage 8 (Flugzeugtechnikerzulage).

3.2 Die Stellenzulage wird neben einer Stellenzulage nach Nummer 4 oder Nummer 5 nur gewährt, soweit sie diese übersteigt.

##### 4 Zulage für Beamtinnen und Beamte als fliegendes Personal

4.1 Beamtinnen und Beamte in einer Verwendung

1.  als Luftfahrzeugführerin oder Luftfahrzeugführer mit der Erlaubnis zum Führen von Luftfahrzeugen,
2.  als sonstige ständige Luftfahrzeugbesatzungsangehörige

erhalten eine Stellenzulage nach Anlage 8 (Fliegerzulage).

4.2 Die zuletzt gewährte Stellenzulage wird nach Beendigung der Verwendung für fünf Jahre weitergewährt, wenn die Beamtin oder der Beamte

1.  mindestens fünf Jahre in einer Tätigkeit nach Nummer 4.1 verwendet worden ist oder
2.  bei der Verwendung nach Nummer 4.1 einen Dienstunfall im Flugdienst oder eine durch die Besonderheiten dieser Verwendung bedingte gesundheitliche Schädigung erlitten hat, die die weitere Verwendung nach Nummer 4.1 ausschließen.

4.3 Besteht ein Anspruch auf eine Stellenzulage nach Nummer 4.2 und wechselt die Beamtin oder der Beamte in eine weitere Verwendung über, mit der ein Anspruch auf eine geringere Stellenzulage nach Nummer 4.1 verbunden ist, so wird zusätzlich zu der geringeren Stellenzulage der Unterschiedsbetrag zu der Stellenzulage nach Nummer 4.2 gezahlt.
Nach Beendigung der weiteren Verwendung wird die Stellenzulage nach Nummer 4.2 nur weitergewährt, soweit sie noch nicht vor der weiteren Verwendung bezogen und auch nicht während der weiteren Verwendung durch den Unterschiedsbetrag zwischen der geringeren Stellenzulage und der Stellenzulage nach Nummer 4.2 abgegolten worden ist.

4.4 Die Stellenzulage ist für Beamtinnen und Beamte nach Nummer 4.1

1.  Buchstabe a in Höhe von 184,07 Euro,
2.  Buchstabe b in Höhe von 147,25 Euro

ruhegehaltfähig, wenn sie mindestens fünf Jahre bezogen worden ist oder das Dienstverhältnis durch Tod oder Dienstunfähigkeit infolge eines durch die Verwendung erlittenen Dienstunfalls oder einer durch die Besonderheiten dieser Verwendung bedingten gesundheitlichen Schädigung beendet worden ist.

##### 5 Zulage für Beamtinnen und Beamte als Nachprüferin oder Nachprüfer von Luftfahrtgerät

Beamtinnen und Beamte erhalten eine Stellenzulage nach Anlage 8, wenn sie die Nachprüferlaubnis besitzen und als Nachprüferin oder Nachprüfer  von Luftfahrtgerät verwendet werden (Nachprüferzulage).
Die Zulage wird nicht gewährt, wenn eine andere Prüferlaubnis die Nachprüferlaubnis lediglich einschließt.

##### 6 Zulage für Beamtinnen und Beamte bei obersten Bundesbehörden sowie bei obersten Gerichtshöfen des Bundes

Beamtinnen und Beamte erhalten, wenn sie bei obersten Bundesbehörden oder bei obersten Gerichtshöfen des Bundes verwendet werden, eine Stellenzulage nach den für Beamtinnen und Beamte des Bundes geltenden Vorschriften.

##### 7 Zulage für Beamtinnen und Beamte beim Verfassungsschutz

Beamtinnen und Beamte, die beim Verfassungsschutz des Landes Brandenburg verwendet werden, erhalten eine Stellenzulage nach Anlage 8 (Sicherheitszulage).

##### 8 Zulage für Beamtinnen und Beamte mit vollzugspolizeilichen oder steuerfahndungsdienstlichen Aufgaben

8.1 Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte sowie Beamtinnen und Beamte des Steuerfahndungsdienstes erhalten eine Stellenzulage nach Anlage 8, soweit ihnen Dienstbezüge nach der Besoldungsordnung A zustehen (Polizeizulage).
Die Zulage erhalten unter den gleichen Voraussetzungen auch Beamtinnen und Beamte auf Widerruf.

8.2 Die Stellenzulage wird nicht neben einer Stellenzulage nach Nummer 7 gewährt.

8.3 Durch die Stellenzulage werden die Besonderheiten des jeweiligen Dienstes, insbesondere der mit Wach- und Streifendienst sowie dem Nachtdienst verbundene Aufwand sowie der Aufwand für Verzehr mit abgegolten.

##### 9 Zulage für Beamtinnen und Beamte der Feuerwehr

9.1 Beamtinnen und Beamte der Besoldungsordnung A im Einsatzdienst der Feuerwehr sowie Beamtinnen und Beamte, die entsprechend verwendet werden, erhalten eine Stellenzulage nach Anlage 8 (Feuerwehrzulage).
Die Zulage erhalten unter den gleichen Voraussetzungen auch Beamtinnen und Beamte auf Widerruf.
Satz 1 gilt für Beamtinnen und Beamte des feuerwehrtechnischen Dienstes an der Landesschule und Technischen Einrichtung für Brand- und Katastrophenschutz entsprechend, soweit sie regelmäßig einsatzgleichen Belastungen ausgesetzt sind.

9.2 Durch die Stellenzulage werden die Besonderheiten des Einsatzdienstes der Feuerwehr, insbesondere der mit dem Nachtdienst verbundene Aufwand sowie der Aufwand für Verzehr mit abgegolten und den Besonderheiten des Dienstes an der Landesschule und Technischen Einrichtung für Brand- und Katastrophenschutz Rechnung getragen.

##### 10  Zulage für Beamtinnen und Beamte in Justizvollzugsanstalten und Psychiatrischen Krankeneinrichtungen

10.1 Beamtinnen und Beamte in Ämtern der Besoldungsordnung A bei Justizvollzugsanstalten, in abgeschlossenen Vorführbereichen der Gerichte sowie in geschlossenen Abteilungen oder Stationen bei Psychiatrischen Krankeneinrichtungen, die ausschließlich dem Vollzug von Maßregeln der Sicherung und Besserung dienen, sowie Beamtinnen und Beamte in Abschiebehafteinrichtungen erhalten eine Stellenzulage nach Anlage 8 (Vollzugsdienstzulage).
Die Zulage erhalten unter den gleichen Voraussetzungen auch Beamtinnen und Beamte auf Widerruf.

10.2 Die Stellenzulage wird für Beamtinnen und Beamte in Abschiebehafteinrichtungen nicht neben einer Stellenzulage nach Nummer 8 gewährt.

##### 10a  Zulage für Beamtinnen und Beamte des Justizwachtmeisterdienstes im Vorführdienst

10a.1 Beamtinnen und Beamte im Justizwachtmeisterdienst in Ämtern der Besoldungsordnung A, die uneingeschränkt im Vorführdienst verwendet werden, erhalten eine Stellenzulage nach Anlage 8 (Vorführzulage).
Die Zulage erhalten unter den gleichen Voraussetzungen auch Beamtinnen und Beamte auf Widerruf.

10a.2 Die Stellenzulage wird Beamtinnen und Beamten nicht neben einer Stellenzulage nach Nummer 10.1 gewährt.

##### 11  Zulage für Beamtinnen und Beamte mit Meisterprüfung oder Abschlussprüfung als staatlich geprüfte Technikerin oder staatlich geprüfter Techniker

Beamtinnen und Beamte in Laufbahnen des mittleren Dienstes, in denen die Meisterprüfung oder die Abschlussprüfung als staatlich geprüfte Technikerin oder staatlich geprüfter Techniker vorgeschrieben ist, erhalten, wenn sie die Prüfung bestanden haben, eine Stellenzulage nach Anlage 8 (Meisterzulage).

##### 12  Zulage für Beamtinnen und Beamte im Außendienst der Steuerprüfung

12.1 Beamtinnen und Beamte des mittleren Dienstes und des gehobenen Dienstes in der Steuerverwaltung, denen ein Dienstposten im Außendienst der Steuerprüfung übertragen ist, erhalten eine Stellenzulage nach Anlage 8 (Außendienstzulage).
Satz 1 gilt nicht für Sachgebietsleiterinnen und Sachgebietsleiter der Außenprüfungsdienste.

12.2 Die Stellenzulage wird nicht neben einer Stellenzulage nach Nummer 8 gewährt.

##### 13  Allgemeine Stellenzulage

Eine das Grundgehalt ergänzende ruhegehaltfähige Stellenzulage nach Anlage 8 erhalten

1.  Beamtinnen und Beamte des mittleren Dienstes in Laufbahnen, deren Eingangsamt der Besoldungsgruppe A 5, A 6 oder A 7 zugeordnet ist, des mittleren technischen Dienstes, des mittleren Krankenpflegedienstes, des mittleren allgemeinen Vollzugsdienstes bei den Justizvollzugsanstalten, des mittleren Feuerwehrdienstes, der Gerichtsvollzieherlaufbahn und des mittleren Polizeivollzugsdienstes  
    aa) in den Besoldungsgruppen A 5 bis A 8,  
    bb) in den Besoldungsgruppen A 9 bis A 11,
2.  Beamtinnen und Beamte des gehobenen Dienstes in Laufbahnen, deren Eingangsamt nach § 22 Nummer 2 der Besoldungsgruppe A 9 oder A 10 zugeordnet ist, ihnen gleichgestellte Beamtinnen und Beamte in den Besoldungsgruppen A 9 bis A 13,
3.  Beamtinnen und Beamte des höheren Verwaltungsdienstes einschließlich der Beamtinnen und Beamten besonderer Fachrichtungen, Studienrätinnen und Studienräte sowie Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte in der Besoldungsgruppe A 13.

#### III.
Einstufung von Ämtern

##### 14  Leiterinnen und Leiter von unteren Landesbehörden sowie Leiterinnen und Leiter von allgemeinbildenden oder beruflichen Schulen

Die Ämter der Leiterinnen und Leiter von unteren Landesbehörden mit einem örtlich begrenzten Zuständigkeitsbereich sowie die Ämter der Leiterinnen und Leiter von allgemeinbildenden oder beruflichen Schulen dürfen nur in Besoldungsgruppen der Besoldungsordnungen A eingestuft werden.
Für die Leiterinnen und Leiter von besonders großen und besonders bedeutenden unteren Landesbehörden sowie für die Leiterinnen und Leiter von Landesoberbehörden können nach Maßgabe des Haushalts Planstellen der Besoldungsgruppe A 16 mit einer Amtszulage nach Anlage 8 ausgestattet werden.
Bei der Anwendung der Obergrenzen nach § 24 Absatz 3 auf die übrigen Leiterinnen und Leiter unterer Landesbehörden oder Landesoberbehörden bleiben die mit einer Amtszulage ausgestatteten Planstellen der Besoldungsgruppe A 16 unberücksichtigt.
Die Zahl der mit einer Amtszulage ausgestatteten Planstellen der Besoldungsgruppe A 16 darf 30 Prozent der Zahl der Planstellen der Besoldungsgruppe A 16 für Leiterinnen und Leiter unterer Landesbehörden oder Landesoberbehörden nicht überschreiten.

**Besoldungsordnung A**

**Besoldungsgruppe A 1**

**Besoldungsgruppe A 2**

**Besoldungsgruppe A 3**

**Besoldungsgruppe A 4**

(Die Besoldungsgruppen A 1 bis A 4 sind nicht besetzt)

**Besoldungsgruppe A 5**

Justizhauptwachtmeisterin, Justizhauptwachtmeister 1)

* * *

1) Erhält eine Amtszulage nach Anlage 8.

**Besoldungsgruppe A 6**

Erste Justizhauptwachtmeisterin, Erster Justizhauptwachtmeister 1) 2)

Sekretärin, Sekretär3)

* * *

1) Soweit nicht in der Besoldungsgruppe A 7.

2) Erhält eine Amtszulage nach Anlage 8.

3) Als Eingangsamt.

**Besoldungsgruppe A 7**

Brandmeisterin, Brandmeister 1)

Erste Justizhauptwachtmeisterin, Erster Justizhauptwachtmeister 2)

Obersekretärin, Obersekretär3) 4)

* * *

1) Als Eingangsamt.

2) Soweit nicht in der Besoldungsgruppe A 6.
Für bis zu 20 Prozent der Gesamtzahl der Planstellen des Justizwachtmeisterdienstes.

3) Auch als Eingangsamt für Laufbahnen des mittleren technischen Dienstes.

4) Auch als Eingangsamt für die Laufbahn des mittleren Steuerverwaltungsdienstes.

5) Auch als Eingangsamt.

**Besoldungsgruppe A 8**

Abteilungspfleger 1)

Abteilungsschwester 1)

Gerichtsvollzieherin, Gerichtsvollzieher 1)

Hauptsekretärin,  
Hauptsekretär 2)

Hauptwerkmeisterin, Hauptwerkmeister 1)

Kriminalobermeisterin, Kriminalobermeister 1)

Oberbrandmeisterin, Oberbrandmeister

Polizeiobermeisterin, Polizeiobermeister 1)

* * *

1) Als Eingangsamt.

2) Als Eingangsamt für die Laufbahn des mittleren allgemeinen Vollzugsdienstes bei den Justizvollzugsanstalten.

**Besoldungsgruppe A 9**

Amtsinspektorin, Amtsinspektor1)

Betriebsinspektorin, Betriebsinspektor1)

Hauptbrandmeisterin, Hauptbrandmeister 1)

Inspektorin, Inspektor

Kriminalhauptmeisterin, Kriminalhauptmeister 1)

Kriminalkommissarin, Kriminalkommissar

Obergerichtsvollzieherin, Obergerichtsvollzieher 1)

Oberpfleger 2)

Oberschwester 2)

Pflegevorsteherin, Pflegevorsteher 2) 3)

Polizeihauptmeisterin, Polizeihauptmeister 1)

Polizeikommissarin, Polizeikommissar

* * *

1) Soweit nicht in den Besoldungsgruppen A 10, A 11.
Für Funktionen, die sich von denen der Besoldungsgruppe A 9 abheben, können nach Maßgabe sachgerechter Bewertung jeweils bis zu 30 Prozent der Stellen mit einer Amtszulage nach Anlage 8 ausgestattet werden.

2) Erhält bei Bestellung zum Mitglied der Krankenhausbetriebsleitung für die Dauer dieser Tätigkeit eine Stellenzulage nach Anlage 8.

3) Erhält eine Amtszulage nach Anlage 8.

**Besoldungsgruppe A 10**

Amtsinspektorin, Amtsinspektor1)

Kriminaloberkommissarin, Kriminaloberkommissar

Oberinspektorin, Oberinspektor

Polizeioberkommissarin, Polizeioberkommissar

* * *

1) Soweit nicht in den Besoldungsgruppen A 9, A 11; für Funktionen, die nach sachgerechter Bewertung mindestens einem Amt der Besoldungsgruppe A 10 einer Laufbahn des gehobenen Dienstes zugeordnet sind.

**Besoldungsgruppe A 11**

Amtfrau, Amtmann

Amtsinspektorin, Amtsinspektor1)

Kriminalhauptkommissarin, Kriminalhauptkommissar 2)

Polizeihauptkommissarin, Polizeihauptkommissar 2)

* * *

1) Soweit nicht in den Besoldungsgruppen A 9, A 10; für Funktionen, die nach sachgerechter Bewertung mindestens einem Amt der Besoldungsgruppe A 11 einer Laufbahn des gehobenen Dienstes zugeordnet sind.

2) Soweit nicht in der Besoldungsgruppe A 12.

**Besoldungsgruppe A 12**

Amtsanwältin, Amtsanwalt 1)

Amtsrätin, Amtsrat

Kriminalhauptkommissarin, Kriminalhauptkommissar 2)

Polizeihauptkommissarin, Polizeihauptkommissar 2)

Rechnungsrätin, Rechnungsrat

*   als Prüfungsbeamtin oder Prüfungsbeamter beim Landesrechnungshof -

* * *

1) Als Eingangsamt.

2) Soweit nicht in der Besoldungsgruppe A 11.

**Besoldungsgruppe A 13**

Akademische Rätin, Akademischer Rat

*   als akademische Mitarbeiterin oder als akademischer Mitarbeiter an einer Hochschule -

Ärztin, Arzt1)

Erste Kriminalhauptkommissarin, Erster Kriminalhauptkommissar

Erste Polizeihauptkommissarin, Erster Polizeihauptkommissar

Förderschullehrerin, Förderschullehrer

*    mit der Befähigung für das Lehramt für Förderpädagogik - 2)

Konservatorin, Konservator

Kustodin, Kustos

Lehrerin, Lehrer

*   mit der Befähigung für das Lehramt für die Primarstufe - 2)
*   mit der Befähigung für das Lehramt für die Bildungsgänge der Sekundarstufe I und der Primarstufe an allgemeinbildenden Schulen - 2)
*   mit der Befähigung für das Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer) bei einer Schwerpunktbildung auf die Sekundarstufe I - 2)
*   mit der Befähigung für das Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer) bei einer Schwerpunktbildung auf die Sekundarstufe II bei überwiegender Verwendung in der Primarstufe oder der Sekundarstufe I - 2)

Oberamtsanwältin, Oberamtsanwalt 3)

Oberamtsrätin, Oberamtsrat 1) 4) 5)

Oberlehrerin, Oberlehrer

*   an einer Justizvollzugsanstalt -

Oberrechnungsrätin, Oberrechnungsrat

*   als Prüfungsbeamtin oder Prüfungsbeamter beim Landesrechnungshof -1)

Rätin, Rat

Rektorin, Rektor

*   bei einer Behörde oder Einrichtung des Landes im Geschäftsbereich des für Schule zuständigen Ministeriums - 6)

Schulpsychologierätin, Schulpsychologierat

Studienrätin, Studienrat

*   mit der Befähigung für das Lehramt an Gymnasien oder an beruflichen Schulen bei einer der jeweiligen Befähigung entsprechenden Verwendung -
*   mit der Befähigung für das Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer) bei einer Schwerpunktbildung auf die Sekundarstufe II und entsprechender Verwendung -
*   mit der Befähigung für das Lehramt für die Sekundarstufe II (berufliche Fächer) -

* * *

1) Soweit nicht in der Besoldungsgruppe A 14.

2) Als Eingangsamt.

3) Für Funktionen einer Amtsanwältin oder eines Amtsanwalts bei einer Staatsanwaltschaft, die sich von denen der Besoldungsgruppe A 13 abheben, können nach Maßgabe sachgerechter Bewertung bis zu 20 Prozent der Stellen für Oberamtsanwältinnen und Oberamtsanwälte mit einer Amtszulage nach Anlage 8 ausgestattet werden.

4) Für Beamtinnen und Beamte der Rechtspflegerlaufbahn können für Funktionen der Rechtspflegerinnen und Rechtspfleger bei Gerichten und Staatsanwaltschaften, die sich von denen der Besoldungsgruppe A 13 abheben, nach Maßgabe sachgerechter Bewertung bis zu 20 Prozent der für Rechtspflegerinnen und Rechtspfleger ausgebrachten Stellen der Besoldungsgruppe A 13 mit einer Amtszulage nach Anlage 8 ausgestattet werden.

5) Für Beamtinnen und Beamte des gehobenen technischen Dienstes können für Funktionen, die sich von denen der Besoldungsgruppe A 13 abheben, nach Maßgabe sachgerechter Bewertung bis zu 20 Prozent der für technische Beamtinnen und Beamte ausgebrachten Stellen der Besoldungsgruppe A 13 mit einer Amtszulage nach Anlage 8 ausgestattet werden.

6) Soweit nicht in den Besoldungsgruppen A 14, A 15.

**Besoldungsgruppe A 14**

Akademische Oberrätin, Akademischer Oberrat

*   als akademische Mitarbeiterin oder akademischer Mitarbeiter an einer Hochschule -

Ärztin, Arzt 1)

Chefärztin, Chefarzt 2)

Förderschulkonrektorin, Förderschulkonrektor

*   als die ständige Vertreterin oder der ständige Vertreter der Leiterin oder des Leiters einer Schule mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ mit mehr als 90 bis zu 180 Schülerinnen und Schülern oder einer Schule mit einem anderen sonderpädagogischen Förderschwerpunkt mit mehr als 45 bis zu 90 Schülerinnen und Schülern -
*   als die ständige Vertreterin oder der ständige Vertreter der Leiterin oder des Leiters einer Schule mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ mit mehr als 180 Schülerinnen und Schülern oder einer Schule mit einem anderen sonderpädagogischen Förderschwerpunkt mit mehr als 90 Schülerinnen und Schülern - 3)

Förderschulrektorin, Förderschulrektor

*   als Leiterin oder Leiter einer Schule mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ mit bis zu 90 Schülerinnen und Schülern oder einer Schule mit einem anderen sonderpädagogischen Förderschwerpunkt mit bis zu 45 Schülerinnen und Schülern -
*   als Leiterin oder Leiter einer Schule mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ mit mehr als 90 bis zu 180 Schülerinnen und Schülern oder einer Schule mit einem anderen sonderpädagogischen Förderschwerpunkt mit mehr als 45 bis zu 90 Schülerinnen und Schülern - 3)

Konrektorin, Konrektor

*   als die ständige Vertreterin oder der ständige Vertreter der Leiterin oder des Leiters einer Grundschule mit mehr als 180 bis zu 360 Schülerinnen und Schülern -
*   als die ständige Vertreterin oder der ständige Vertreter der Leiterin oder des Leiters einer Grundschule mit mehr als 360 Schülerinnen und Schülern - 3)

Oberamtsrätin, Oberamtsrat4)

Oberärztin, Oberarzt 5)

Oberkonservatorin, Oberkonservator

Oberkustodin, Oberkustos

Oberrätin, Oberrat

Oberrechnungsrätin, Oberrechnungsrat

*     als Prüfungsbeamtin oder Prüfungsbeamter beim Landesrechnungshof - 4)

Oberschulkonrektorin, Oberschulkonrektor

*   als die ständige Vertreterin oder der ständige Vertreter der Leiterin oder des Leiters einer Oberschule mit mehr als 180 bis zu 360 Schülerinnen und Schülern -
*   als die ständige Vertreterin oder der ständige Vertreter der Leiterin oder des Leiters einer Oberschule mit mehr als 360 Schülerinnen und Schülern - 3)
*   als die ständige Vertreterin oder der ständige Vertreter der Leiterin oder des Leiters einer Oberschule mit angegliedertem Primarstufenbereich mit mehr als 180 bis zu 360 Schülerinnen und Schülern der Primarstufe und der Sekundarstufe I - 6)

Oberschulrektorin, Oberschulrektor

*   als Leiterin oder Leiter einer Oberschule mit bis zu 180 Schülerinnen und Schülern -
*   als Leiterin oder Leiter einer Oberschule mit mehr als 180 bis zu 360 Schülerinnen und Schülern - 3)

Oberstudienrätin, Oberstudienrat

*   als zweite stellvertretende Schulleiterin oder zweiter stellvertretender Schulleiter eines Gymnasiums mit mehr als 540 Schülerinnen und Schülern in der Sekundarstufe I -
*   mit der Befähigung für das Lehramt an Gymnasien oder an beruflichen Schulen bei einer der jeweiligen Befähigung entsprechenden Verwendung -
*   mit der Befähigung für das Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer) bei einer Schwerpunktbildung auf die Sekundarstufe II und entsprechender Verwendung -
*   mit der Befähigung für das Lehramt für die Sekundarstufe II (berufliche Fächer) -
*   bei einer Behörde oder Einrichtung des Landes im Geschäftsbereich des für Schule zuständigen Ministeriums -

Rektorin, Rektor

*   als Leiterin oder Leiter einer Grundschule mit bis zu 180 Schülerinnen und Schülern -
*   als Leiterin oder Leiter einer Grundschule mit mehr als 180 bis zu 360 Schülerinnen und Schülern - 3)
*   bei einer Behörde oder Einrichtung des Landes im Geschäftsbereich des für Schule zuständigen Ministeriums - 7)

Rektorin, Rektor an einer Gesamtschule oder an einer Oberschule

*   als Leiterin oder Leiter des Primarstufenbereichs einer Gesamtschule oder einer Oberschule mit mehr als 180 bis zu 360 Schülerinnen und Schülern in der Primarstufe -
*   als Leiterin oder Leiter des Primarstufenbereichs einer Gesamtschule oder einer Oberschule mit mehr als 360 Schülerinnen und Schülern in der Primarstufe - 3)

Schulrätin, Schulrat

*   bei einer Behörde oder Einrichtung des Landes im Geschäftsbereich des für Schule zuständigen Ministeriums - 3) 8)

Zweite Gesamtschulkonrektorin, Zweiter Gesamtschulkonrektor

*   einer Gesamtschule mit mehr als 540 Schülerinnen und Schülern in der Sekundarstufe I -

Zweite Oberschulkonrektorin, Zweiter Oberschulkonrektor

*   einer Oberschule mit mehr als 540 Schülerinnen und Schülern in der Sekundarstufe I -

Zweite Konrektorin, Zweiter Konrektor

*   einer Grundschule mit mehr als 540 Schülerinnen und Schülern -

* * *

1) Soweit nicht in der Besoldungsgruppe A 13.

2) Soweit nicht in den Besoldungsgruppen A 15, A 16.

3) Erhält eine Amtszulage nach Anlage 8.

4) Soweit nicht in Besoldungsgruppe A 13; für Funktionen, die nach sachgerechter Bewertung mindestens einem Amt der Besoldungsgruppe A 14 einer Laufbahn des höheren Dienstes zugeordnet sind.

5) Soweit nicht in der Besoldungsgruppe A 15.

6) Dieses Amt darf nur vergeben werden, wenn die Schülerzahlen für den jeweiligen Bildungsgang allein jeweils nicht mehr als 180 Schülerinnen und Schüler erreichen.

7) Soweit nicht in den Besoldungsgruppen A 13, A 15.

8) Als Eingangsamt.

**Besoldungsgruppe A 15**

Akademische Direktorin, Akademischer Direktor

*   als akademische Mitarbeiterin oder akademischer Mitarbeiter an einer Hochschule -

Chefärztin, Chefarzt 1)

Direktorin, Direktor

Förderschulrektorin, Förderschulrektor

*   als Leiterin oder Leiter einer Schule mit dem sonderpädagogischen Förderschwerpunkt „Lernen“ mit mehr als 180 Schülerinnen und Schülern oder einer Schule mit einem anderen sonderpädagogischen Förderschwerpunkt mit mehr als 90 Schülerinnen und Schülern -

Gesamtschulkonrektorin, Gesamtschulkonrektor

*   als die ständige Vertreterin oder der ständige Vertreter der Leiterin oder des Leiters einer Gesamtschule mit nicht voll ausgebauter Oberstufe -
*   als die ständige Vertreterin oder der ständige Vertreter der Leiterin oder des Leiters einer Gesamtschule mit voll ausgebauter Oberstufe und bis zu 360 Schülerinnen und Schülern -
*   als die ständige Vertreterin oder der ständige Vertreter der Leiterin oder des Leiters einer Gesamtschule mit voll ausgebauter Oberstufe und mehr als 360 Schülerinnen und Schülern - 2)

Gesamtschulrektorin, Gesamtschulrektor

*   als Leiterin oder Leiter einer Gesamtschule mit nicht voll ausgebauter Oberstufe - 2)
*   als Leiterin oder Leiter einer Gesamtschule mit voll ausgebauter Oberstufe und bis zu 360 Schülerinnen und Schülern - 2)

Hauptkonservatorin, Hauptkonservator

Hauptkustodin, Hauptkustos

Kanzlerin, Kanzler der ...
8)

Museumsdirektorin und Professorin, Museumsdirektor und Professor 3)

Oberärztin, Oberarzt 4)

Oberschulrätin, Oberschulrat

*   bei einer Behörde oder Einrichtung des Landes im Geschäftsbereich des für Schule zuständigen Ministeriums - 3)

Oberschulrektorin, Oberschulrektor

*   als Leiterin oder Leiter an einer Oberschule mit mehr als 360 Schülerinnen und Schülern -

Rektorin, Rektor

*   als Leiterin oder Leiter einer Grundschule mit mehr als 360 Schülerinnen und Schülern -
*   bei einer Behörde oder Einrichtung des Landes im Geschäftsbereich des für Schule zuständigen Ministeriums - 5)

Studiendirektorin, Studiendirektor

*   zur Koordinierung schulfachlicher Aufgaben - 6)
*   als die ständige Vertreterin oder der ständige Vertreter der Leiterin oder des Leiters  
      
    eines Gymnasiums im Aufbau mit  
      
    mehr als 540 Schülerinnen und Schülern, wenn die oberste Jahrgangsstufe fehlt,7)  
      
    mehr als 670 Schülerinnen und Schülern, wenn die zwei oberen Jahrgangsstufen fehlen, 7)  
      
    mehr als 800 Schülerinnen und Schülern, wenn die drei oberen Jahrgangsstufen fehlen, 7)  
      
    eines nicht voll ausgebauten Gymnasiums,  
      
    eines voll ausgebauten Gymnasiums mit bis zu 360 Schülerinnen und Schülern,  
      
    eines voll ausgebauten Gymnasiums mit mehr als 360 Schülerinnen und Schülern7) \-
*   als Leiterin oder Leiter  
      
    eines nicht voll ausgebauten Gymnasiums, 7)  
      
    eines voll ausgebauten Gymnasiums mit bis zu 360 Schülerinnen und Schülern 7) \-
*   als die ständige Vertreterin oder der ständige Vertreter der Leiterin oder des Leiters einer Einrichtung des Zweiten Bildungsweges -
*   als die ständige Vertreterin oder der ständige Vertreter der Leiterin oder des Leiters eines Oberstufenzentrums - 2)
*   als Leiterin oder Leiter einer Abteilung an einem Oberstufenzentrum - 6)
*   bei einer Behörde oder Einrichtung des Landes im Geschäftsbereich des für Schule zuständigen Ministeriums -

* * *

1) Soweit nicht in den Besoldungsgruppen A 14, A 16.

2) Erhält eine Amtszulage nach Anlage 8.

3) Soweit nicht in der Besoldungsgruppe A 16.

4) Soweit nicht in der Besoldungsgruppe A 14.

5) Soweit nicht in den Besoldungsgruppen A 13, A 14.

6) Höchstens 30 Prozent der Gesamtzahl der planmäßigen Beamtinnen und Beamten in der Laufbahn der Studienrätinnen und Studienräte.

7) Erhält eine Amtszulage nach Anlage 8.

8) Der Amtsbezeichnung ist ein Zusatz beizufügen, der auf die Hochschule hinweist, der die Amtsinhaberin oder der Amtsinhaber angehört.

**Besoldungsgruppe A 16**

Abteilungsdirektorin, Abteilungsdirektor

Abteilungspräsidentin, Abteilungspräsident

Chefärztin, Chefarzt 1)

Direktorin, Direktor beim Polizeipräsidium 2)

Gesamtschulrektorin, Gesamtschulrektor

*   als Leiterin oder Leiter einer Gesamtschule mit voll ausgebauter Oberstufe und mit mehr als 360 Schülerinnen und Schülern -

Leitende Akademische Direktorin, Leitender Akademischer Direktor

*   als akademische Mitarbeiterin oder akademischer Mitarbeiter an einer Hochschule 3)

Landeskriminaldirektorin, Landeskriminaldirektor oder Landespolizeidirektorin, Landespolizeidirektor

*   als Referatsleiterin oder Referatsleiter für polizeiliche Einsatz- oder Kriminalitätsangelegenheiten im für Inneres zuständigen Ministerium - 2)

Leitende Direktorin, Leitender Direktor

Ministerialrätin, Ministerialrat

*   bei einer obersten Landesbehörde - 2)

Museumsdirektorin und Professorin, Museumsdirektor und Professor 4)

Oberschulrätin, Oberschulrat

*   als Leiterin oder Leiter eines staatlichen Schulamtes -
*   als Referatsleiterin oder Referatsleiter im Schulaufsichtsdienst oder als Leiterin oder Leiter von Schulaufsichtsbereichen bei einer Behörde oder Einrichtung des Landes im Geschäftsbereich des für Schule zuständigen Ministeriums -

Oberstudiendirektorin, Oberstudiendirektor

*   als Leiterin oder Leiter  
      
    eines Gymnasiums im Aufbau mit  
      
    mehr als 540 Schülerinnen und Schülern, wenn die oberste Jahrgangsstufe fehlt,  
      
    mehr als 670 Schülerinnen und Schülern, wenn die zwei oberen Jahrgangsstufen fehlen,  
      
    mehr als 800 Schülerinnen und Schülern, wenn die drei oberen Jahrgangsstufen fehlen,  
      
    eines voll ausgebauten Gymnasiums mit mehr als 360 Schülerinnen und Schülern -
*   als die ständige Vertreterin oder der ständige Vertreter der Direktorin oder des Direktors des Landesinstituts für Schule und Medien Berlin-Brandenburg -
*   als Leiterin oder Leiter einer Einrichtung des Zweiten Bildungsweges; eines Oberstufenzentrums -
*   bei einer Behörde oder Einrichtung des Landes im Geschäftsbereich des für Schule zuständigen Ministeriums -

Vizepräsidentin, Vizepräsident des Amtes für Statistik Berlin-Brandenburg

*   als die ständige Vertreterin oder der ständige Vertreter des Vorstandes der Anstalt öffentlichen Rechts -

* * *

1) Soweit nicht in den Besoldungsgruppen A 14, A 15.

2) Soweit nicht in der Besoldungsgruppe B 2.

3) Nur in Stellen von besonderer Bedeutung.

4) Soweit nicht in der Besoldungsgruppe A 15.

**Besoldungsordnung B**

**Besoldungsgruppe B 1**

(nicht besetzt)

**Besoldungsgruppe B 2**

Abteilungsdirektorin, Abteilungsdirektor oder Abteilungspräsidentin, Abteilungspräsident

*   als Leiterin oder Leiter einer großen und bedeutenden Abteilung bei einer Landesoberbehörde -
*   bei einer sonstigen Dienststelle oder Einrichtung, wenn deren Leiterin oder Leiter mindestens in Besoldungsgruppe B 5 eingestuft ist -

Abteilungsdirektorin, Abteilungsdirektor des Landesbetriebes Forst Brandenburg

*   als die ständige Vertreterin oder der ständige Vertreter der Direktorin oder des Direktors -

Direktorin, Direktor beim Landesbetrieb Straßenwesen

Direktorin, Direktor beim Polizeipräsidium 1)

Direktorin, Direktor der Fachhochschule für Finanzen

Direktorin, Direktor der Generalverwaltung der Stiftung Preußische Schlösser und Gärten Berlin-Brandenburg

*   als die ständige Vertreterin oder der ständige Vertreter der Generaldirektorin oder des Generaldirektors -

Direktorin, Direktor der Zentralen Ausländerbehörde

Direktorin, Direktor der Zentralen Bezügestelle des Landes Brandenburg

Direktorin, Direktor des Brandenburgischen IT-Dienstleisters

Direktorin, Direktor des Brandenburgischen Landesamtes für Denkmalpflege und Archäologischen Landesmuseums

Direktorin, Direktor des Brandenburgischen Landesbetriebes für Liegenschaften und Bauen

*   als Leiterin oder Leiter eines Bereichs - 4)

Direktorin, Direktor des Kommunalen Versorgungsverbandes

Direktorin, Direktor des Landesinstituts für Schule und Medien Berlin-Brandenburg

Direktorin, Direktor des Zentralen IT-Dienstleisters der Justiz des Landes Brandenburg

Kanzlerin, Kanzler der Brandenburgischen Technischen Universität Cottbus-Senftenberg 2)

Kanzlerin, Kanzler der Europa-Universität Frankfurt (Oder)

Landeskriminaldirektorin, Landeskriminaldirektor oder Landespolizeidirektorin, Landespolizeidirektor

*   als Referatsleiterin oder Referatsleiter für polizeiliche Einsatz- oder Kriminalitätsangelegenheiten im für Inneres zuständigen Ministerium - 1)

Leitende Kriminaldirektorin, Leitender Kriminaldirektor 1)

Leitende Oberschulrätin, Leitender Oberschulrat

*   als Leiterin oder Leiter eines bedeutenden Referates der obersten Schulaufsichtsbehörde -

Leitende Polizeidirektorin, Leitender Polizeidirektor 1)

Ministerialrätin, Ministerialrat

*   bei einer obersten Landesbehörde - 1)

Vizepräsidentin, Vizepräsident

*   als die ständige Vertreterin oder der ständige Vertreter einer oder eines in Besoldungsgruppe B 5 eingestuften Leiterin oder Leiters einer Dienststelle oder sonstigen Einrichtung - 3)

* * *

1) Soweit nicht in der Besoldungsgruppe A 16.

2) Der erste Dienstposteninhaber darf in der ersten Amtsperiode Besoldung nach der Besoldungsgruppe B 3 erhalten.

3) Der Amtsbezeichnung kann ein Zusatz beigefügt werden, der auf die Dienststelle oder sonstige Einrichtung hinweist, der die Amtsinhaberin oder der Amtsinhaber angehört.
Der Zusatz „und Professorin“ oder „und Professor“ darf beigefügt werden, wenn die Leiterin oder der Leiter der Dienststelle oder sonstigen Einrichtung diesen Zusatz in der Amtsbezeichnung führt.

4) Für dieses Amt können zwei Stellen ausgebracht werden.

**Besoldungsgruppe B 3**

Beauftragte, Beauftragter des Landes Brandenburg zur Aufarbeitung der Folgen der kommunistischen Diktatur

Direktorin, Direktor bei der Deutschen Rentenversicherung Berlin-Brandenburg

*   als stellvertretende Geschäftsführerin oder stellvertretender Geschäftsführer oder Mitglied der Geschäftsführung, wenn die Erste Direktorin oder der Erste Direktor in Besoldungsgruppe B 4 eingestuft ist -

Direktorin, Direktor des Brandenburgischen Landesbetriebes für Liegenschaften und Bauen

*   als technische Geschäftsführerin oder technischer Geschäftsführer -

Direktorin, Direktor des Zentraldienstes der Polizei

Kanzlerin, Kanzler der Universität Potsdam

Ministerialrätin, Ministerialrat

*   als Abteilungsleiterin oder Abteilungsleiter beim Landesrechnungshof -
*   als Integrationsbeauftragte oder Integrationsbeauftragter des Landes Brandenburg -

Polizeivizepräsidentin, Polizeivizepräsident

Präsidentin, Präsident des Amtes für Statistik Berlin-Brandenburg

*   als Vorstand der Anstalt öffentlichen Rechts -

Präsidentin, Präsident des Landesamtes für Arbeitsschutz, Verbraucherschutz und Gesundheit

Präsidentin, Präsident des Landesamtes für Bauen und Verkehr

Präsidentin, Präsident des Landesamtes für Bergbau, Geologie und Rohstoffe

Präsidentin, Präsident des Landesbetriebes für Landesvermessung und Geobasisinformation Brandenburg

Vizepräsidentin, Vizepräsident

*   als die ständige Vertreterin oder der ständige Vertreter einer oder eines in Besoldungsgruppe B 6 oder B 7 eingestuften Leiterin oder Leiters einer Dienststelle oder sonstigen Einrichtung - 1)

* * *

1) Der Amtsbezeichnung kann ein Zusatz beigefügt werden, der auf die Dienststelle oder sonstige Einrichtung hinweist, der die Amtsinhaberin oder der Amtsinhaber angehört.
Der Zusatz „und Professorin“ oder „und Professor“ darf beigefügt werden, wenn die Leiterin oder der Leiter der Dienststelle oder sonstigen Einrichtung diesen Zusatz in der Amtsbezeichnung führt.

**Besoldungsgruppe B 4**

Direktorin, Direktor bei der Deutschen Rentenversicherung Berlin-Brandenburg

*   als stellvertretende Geschäftsführerin oder stellvertretender Geschäftsführer oder Mitglied der Geschäftsführung, wenn die Erste Direktorin oder der Erste Direktor in Besoldungsgruppe B 5 eingestuft ist -

Direktorin, Direktor des Landesbetriebes Forst Brandenburg

Erste Direktorin, Erster Direktor des Brandenburgischen IT-Dienstleisters

Erste Direktorin, Erster Direktor des Brandenburgischen Landesbetriebes für Liegenschaften und Bauen

*   als kaufmännische Geschäftsführerin oder kaufmännischer Geschäftsführer -

Erste Direktorin, Erster Direktor der Deutschen Rentenversicherung Berlin-Brandenburg

*   als Geschäftsführerin oder Geschäftsführer oder Vorsitzende oder Vorsitzender der Geschäftsführung bei mehr als 900 000 und höchstens 2,3 Millionen Versicherten und laufenden Rentenfällen -

Generaldirektorin und Professorin, Generaldirektor und Professor der Stiftung Preußische Schlösser und Gärten Berlin-Brandenburg

Leitende Ministerialrätin und Leitender Ministerialrat als Beauftragte, Beauftragter für den Sport

Präsidentin, Präsident des Landesamtes für Ländliche Entwicklung, Landwirtschaft und Flurneuordnung

Präsidentin, Präsident des Landesamtes für Soziales und Versorgung

Präsidentin, Präsident des Landesamtes für Umwelt

Präsidentin, Präsident des Landesbetriebes Straßenwesen

**Besoldungsgruppe B 5**

Erste Direktorin, Erster Direktor der Deutschen Rentenversicherung Berlin-Brandenburg

*   als Geschäftsführerin oder Geschäftsführer oder Vorsitzende oder Vorsitzender der Geschäftsführung bei mehr als 2,3 Millionen und höchstens 3,7 Millionen Versicherten und laufenden Rentenfällen -

Direktorin, Direktor beim Landesrechnungshof - mit mindestens zwei Prüfungsgebieten

Ministerialdirigentin, Ministerialdirigent

*   als Leiterin oder Leiter einer Abteilung bei einer obersten Landesbehörde -

Polizeipräsidentin, Polizeipräsident

**Besoldungsgruppe B 6**

Vizepräsidentin, Vizepräsident des Landesrechnungshofes

**Besoldungsgruppe B 7**

(nicht besetzt)

**Besoldungsgruppe B 8**

Direktorin, Direktor des Landtages

**Besoldungsgruppe B 9**

Präsidentin, Präsident des Landesrechnungshofes

Staatssekretärin, Staatssekretär

**Besoldungsgruppe B 10**

Chefin der Staatskanzlei und Staatssekretärin, Chef der Staatskanzlei und Staatssekretär

**Besoldungsgruppe B 11**

(nicht besetzt)

#### Anhang zu den Besoldungsordnungen A und B

#### Künftig wegfallende Ämter

**Besoldungsgruppe A 12 kw**

Fachlehrerin, Fachlehrer

*   im Unterricht an Förderschulen - 1) 2) 3) 4)
*   im berufsbezogenen Unterricht an Schulen mit berufsbildenden Bildungsgängen - 1) 2) 3) 4)
*   im berufstheoretischen Unterricht an Schulen mit berufsbildenden Bildungsgängen - 1) 2) 4)

Lehrerin, Lehrer

*   mit der Befähigung als Lehrer für die unteren Klassen - 1) 2)
*   als Lehrerin oder Lehrer im Unterricht an Förderschulen - 1) 2)

* * *

1) Als Eingangsamt.

2) Soweit nicht in der Besoldungsgruppe A 13 kw.

3) Gilt bei nachgewiesener Meisterprüfung oder einer vom Ministerium für Bildung, Jugend und Sport als gleichwertig anerkannten Prüfung auch für Beamtinnen und Beamte, die bis zum 30. Juni 1995 eingestellt worden sind.

4) Für Ingenieurpädagoginnen und Ingenieurpädagogen, Medizinpädagoginnen und Medizinpädagogen, Agrarpädagoginnen und Agrarpädagogen, Ökonompädagoginnen und Ökonompädagogen ohne abgeschlossene Ingenieurausbildung oder gleichwertige Ausbildung mit einer Ausbildung als Lehrkraft nach dem Recht der Deutschen Demokratischen Republik für den berufspraktischen, teilweise auch berufstheoretischen Unterricht.

**Besoldungsgruppe A 13 kw**

Fachlehrerin, Fachlehrer

*   im Unterricht an Förderschulen oder im berufstheoretischen Unterricht an Schulen mit berufsbildenden Bildungsgängen - 1)
*   mit abgeschlossener Ingenieur- oder Fachhochschulausbildung - 2)

Förderschullehrerin, Förderschullehrer 3) 4)

Lehrerin, Lehrer

*   als Lehrerin oder Lehrer im allgemeinbildenden Schulunterricht, soweit nicht anderweitig eingereiht - 5) 6) 7)
*   mit einer Lehrbefähigung im berufstheoretischen Unterricht bei entsprechender Verwendung - 8)
*   mit der Befähigung für das Lehramt der Primarstufe bei entsprechender Verwendung - 9)
*   mit der Befähigung für das Lehramt für die Primarstufe und die Sekundarstufe I bei überwiegender Verwendung in der Sekundarstufe I - 9)
*   mit der Befähigung für das Lehramt der Sekundarstufe I bei entsprechender Verwendung - 9)

Studienrätin, Studienrat

*   im Unterricht in der Sekundarstufe II - 10)
*   mit der Befähigung für das Lehramt der Sekundarstufe II bei entsprechender Verwendung -

* * *

1) Als Eingangsamt für Fachlehrerinnen und Fachlehrer der Besoldungsgruppe A 12 kw, die eine Ergänzungsprüfung im Sinne der Vorbemerkung Nummer 2.2 zu den Besoldungsordnungen A und B für ein allgemeinbildendes oder berufsfeldübergreifendes Fach oder für eine berufliche oder sonderpädagogische Fachrichtung nachweisen, bei jeweils entsprechender Verwendung.

Auch als Beförderungsamt für Fachlehrerinnen und Fachlehrer der Besoldungsgruppe A 12 kw bei entsprechender Bewährung frühestens ab dem 1. August 2020.

2) Als Eingangsamt für Fachlehrerinnen und Fachlehrer mit abgeschlossener Ingenieur- oder Fachhochschulausbildung, wenn sie vorgeschrieben ist oder beim Fehlen laufbahnrechtlicher Vorschriften gefordert wird.

3) Als Eingangsamt; dies gilt auch für Diplomlehrerinnen und Diplomlehrer im Sinne der Vorbemerkung Nummer 2.1 zu den Besoldungsordnungen A und B mit einer Lehrbefähigung für zwei Fächer der allgemeinbildenden polytechnischen Oberschule und mit Zusatzstudium und Diplomabschluss als Diplomlehrer für eine sonderpädagogische Fachrichtung.

Als Eingangsamt auch für folgende Lehrkräfte mit einer Lehrbefähigung im Sinne der Vorbemerkung Nummer 2.1 zu den Besoldungsordnungen A und B:

1.  Diplomlehrer für Hilfsschulen mit einem Studiengang an der Universität Rostock, Diplomlehrer mit einer Lehrbefähigung für ein Fach der allgemeinbildenden polytechnischen Oberschule und mit Zusatzstudium und Diplomabschluss als Diplomlehrer für eine sonderpädagogische Fachrichtung,
2.  Diplomlehrer mit einer Lehrbefähigung für ein Fach oder zwei Fächer der allgemeinbildenden polytechnischen Oberschule und mit Erweiterungsstudium für mindestens eine sonderpädagogische Fachrichtung,
3.  Lehrerin oder Lehrer mit einer Ausbildung als Freundschaftspionierleiter oder Erzieher mit einer Lehrbefähigung für die unteren Klassen für Deutsch oder Mathematik und einem Wahlfach und mit zusätzlichem Diplomabschluss für eine sonderpädagogische Fachrichtung (mindestens zwei Jahre im Hochschuldirektstudium oder ein Äquivalent im Fern- oder Kombinationsstudium),
4.  Lehrerinnen und Lehrer mit nicht abgeschlossener pädagogischer Fachschulausbildung zum Lehrer für die unteren Klassen und Überleitung nach drei Jahren Ausbildung zum zweijährigen Hochschulstudium an der Pädagogischen Hochschule (Magdeburg) und mit Diplomabschluss für eine sonderpädagogische Fachrichtung als Diplomlehrer,
5.  Lehrer für die unteren Klassen mit Diplomabschluss für eine sonderpädagogische Fachrichtung (mindestens zwei Jahre im Hochschuldirektstudium oder ein Äquivalent im Fern- oder Kombinationsstudium).

4) Als Beförderungsamt für Lehrerinnen und Lehrer im Unterricht an Förderschulen nach Besoldungsgruppe A 12 kw bei entsprechender Bewährung frühestens ab dem 1. August 2020.

5) Als Eingangsamt für die Lehrerinnen und Lehrer für die unteren Klassen bei Nachweis einer Ergänzungsprüfung nach Vorbemerkung Nummer 2.2 zu den Besoldungsordnungen A und B für ein Fach der Primarstufe, Sekundarstufe I, für ein berufsfeldübergreifendes Fach, für eine berufliche oder mindestens eine sonderpädagogische Fachrichtung, bei jeweils entsprechender Verwendung.

Als Eingangsamt auch für Lehrerinnen und Lehrer unterer Klassen nach Besoldungsgruppe A 12 kw, wenn diese eine achtjährige Tätigkeit im neuen Schulsystem nach dem 1. August 1991 erfolgreich absolviert, an einer anerkannten Fortbildungsmaßnahme teilgenommen und am 1. Juli 1995 das 45. Lebensjahr vollendet haben.

Auch als Beförderungsamt für Lehrerinnen und Lehrer unterer Klassen der Besoldungsgruppe A 12 kw bei entsprechender Bewährung frühestens ab dem 1. August 2020.

6) Als Eingangsamt für folgende Lehrerinnen und Lehrer mit einer Lehrbefähigung im Sinne der Vorbemerkung Nummer 2.1 zu den Besoldungsordnungen A und B:

1.  Lehrer für die unteren Klassen der allgemeinbildenden polytechnischen Oberschule (Klassen 1 bis 4) mit zusätzlichem Diplomabschluss als Diplomlehrer für ein Fach der allgemeinbildenden polytechnischen Oberschule,
    
    Diplomlehrer mit einer Lehrbefähigung für ein Fach der allgemeinbildenden polytechnischen Oberschule (Klassen 5 bis 10),
    
    entsprechende Fachlehrer mit Staatsexamen für ein Fach (Abschluss der Ausbildung vor 1970),
    
    Diplomsportlehrer (Deutsche Hochschule für Körperkultur in Leipzig), die mit der grundständigen Ausbildung oder über eine postgraduale Zusatzausbildung auch die Ausbildung und Prüfung in Methodik des Schulsportunterrichts nachgewiesen haben,
    
    Lehrer, Fachlehrer und Diplomlehrer mit einer Lehrbefähigung für ein Fach für die Oberstufe der allgemeinbildenden Schulen, für die Erweiterte Oberschule, mit postgradualer Qualifizierung für die Abiturstufe sowie gleichgestellte Lehrkräfte,
    
    Diplomlehrer mit einer Lehrbefähigung für zwei Fächer, deren Examen nach dem Wegfall eines nicht mehr relevanten Fachs, zum Beispiel Staatsbürgerkunde, nicht mehr als ausreichend zu betrachten ist.
    
2.  Diplomlehrer mit einer Lehrbefähigung für zwei Fächer der allgemeinbildenden polytechnischen Oberschule (Klassen 5 bis 10),
    
    entsprechende Fachlehrer mit Staatsexamen für zwei Fächer (Abschluss der Ausbildung vor 1970).
    
3.  Lehrer, Fachlehrer und Diplomlehrer mit einer Lehrbefähigung für zwei Fächer für die Oberstufe der allgemeinbildenden Schulen, für die Erweiterte Oberschule, mit postgradualer Qualifizierung für die Abiturstufe und bei Verwendung in der Primarstufe oder Sekundarstufe I.
    

7) Als Beförderungsamt für Lehrerinnen und Lehrer nach Fußnote 6 Buchstabe b, die spätestens seit dem 30. Juni 1995 im Unterricht in der Sekundarstufe II verwendet werden.
Diese Lehrerinnen und Lehrer können in die Laufbahn der Studienrätin oder des Studienrates übernommen werden, wenn sie nach ihrer Ernennung zur Beamtin oder zum Beamten auf Lebenszeit mindestens zwei Jahre in der Sekundarstufe II tätig waren und sich bewährt haben.

Als Eingangsamt für Lehrerinnen und Lehrer nach Fußnote 6 Buchstabe b, die spätestens bis 31. Dezember 1996 mindestens drei Jahre in der Sekundarstufe II verwendet worden sind.
Diese Lehrerinnen und Lehrer können in die Laufbahn der Studienrätin oder des Studienrates übernommen werden, wenn sie nach ihrer Ernennung zur Beamtin oder zum Beamten auf Lebenszeit mindestens zwei Jahre in der Sekundarstufe II tätig waren und sich bewährt haben.

8) Als Eingangsamt für Diplomingenieurpädagoginnen und Diplomingenieurpädagogen, Diplomgewerbelehrerinnen und Diplomgewerbelehrer, Diplomhandelslehrerinnen und Diplomhandelslehrer, Diplomökonompädagoginnen und Diplomökonompädagogen, Diplomagrarpädagoginnen und Diplomagrarpädagogen, Diplommedizinpädagoginnen und Diplommedizinpädagogen, Diplomgartenbaupädagoginnen und Diplomgartenbaupädagogen sowie gleichgestellte Lehrkräfte mit einer Lehrbefähigung im Sinne der Vorbemerkung Nummer 2.1 zu den Besoldungsordnungen A und B.

Lehrerinnen und Lehrer, die nach ihrer Ernennung zur Beamtin oder zum Beamten auf Lebenszeit mindestens zwei Jahre in einer Schule mit berufsbildenden Bildungsgängen tätig waren und sich bewährt haben, können in die Laufbahn der Studienrätin oder des Studienrates übernommen werden.

9) Als Eingangsamt.

10) Als Eingangsamt für folgende Lehrerinnen und Lehrer mit einer Lehrbefähigung im Sinne der Vorbemerkung Nummer 2.1 zu den Besoldungsordnungen A und B:

Lehrer, Fachlehrer und Diplomlehrer mit einer Lehrbefähigung für zwei Fächer für die Oberstufe der allgemeinbildenden Schulen, für die Erweiterte Oberschule, mit postgradualer Qualifizierung für die Abiturstufe

und

Lehrerinnen und Lehrer, die nach den Fußnoten 7 und 8 in die Laufbahn der Studienrätin oder des Studienrates übernommen werden.

**Besoldungsgruppe A 14 kw**

Oberstudienrätin, Oberstudienrat

*   im Unterricht in der Sekundarstufe II - 1)
*   mit der Befähigung für das Lehramt der Sekundarstufe II bei entsprechender Verwendung -

* * *

1) Beförderungsamt für Lehrerinnen und Lehrer nach Fußnote 6 zu Besoldungsgruppe A 13 kw.

**Besoldungsgruppe B 3 kw**

Direktorin, Direktor des Landeslabors Brandenburg

Ministerialrätin, Ministerialrat

*   bei einer obersten Landesbehörde -

**Besoldungsgruppe B 4 kw**

Landesbeauftragte, Landesbeauftragter für den Datenschutz und für das Recht auf Akteneinsicht

**Besoldungsgruppe B 6 kw**

Ministerialdirigentin, Ministerialdirigent

*   bei einer obersten Landesbehörde als Leiterin oder Leiter einer großen oder bedeutenden Abteilung -

**Anlage 2**  
(zu § 29 Satz 1, § 62 Absatz 1)

#### Besoldungsordnung W (BbgBesO W)

#### Vorbemerkungen

##### 1 Zulagen

1.1 Professorinnen und Professoren erhalten, wenn sie bei obersten Bundesbehörden oder bei obersten Gerichtshöfen des Bundes verwendet werden, eine Stellenzulage nach den für Beamtinnen und Beamte des Bundes geltenden Vorschriften.

1.2 Professorinnen und Professoren der Besoldungsgruppe W 1 erhalten, wenn sie sich in der Lehrtätigkeit bewährt haben (§ 44 des Brandenburgischen Hochschulgesetzes), ab dem Zeitpunkt der ersten Verlängerung des Beamtenverhältnisses auf Zeit eine nicht ruhegehaltfähige Zulage in Höhe von monatlich 260 Euro.

1.3 Beamtinnen und Beamte, die bis zu ihrer Wahl in ein Präsidentenamt oder Rektorenamt einer Hochschule als Professorin oder Professor der Besoldungsgruppe C 4 ein höheres Grundgehalt zuzüglich der Zuschüsse im Sinne der Nummern 1 und 2 der Vorbemerkungen zu der Bundesbesoldungsordnung C 4 bezogen haben, erhalten eine Ausgleichszulage in Höhe des Unterschiedsbetrags.
Die Ausgleichszulage ist ruhegehaltfähig, soweit sie zum Ausgleich des Grundgehalts oder eines ruhegehaltfähigen Zuschusses dient.

##### 2 Dienstbezüge für Professorinnen und Professoren als Richterin oder Richter

Professorinnen und Professoren an einer Hochschule, die zugleich das Amt einer Richterin oder eines Richters der Besoldungsgruppen R 1 oder R 2 ausüben, erhalten, solange sie beide Ämter bekleiden, die Dienstbezüge aus ihrem Amt als Professorin oder Professor und eine nicht ruhegehaltfähige Zulage.
Die Zulage beträgt, wenn die Professorin oder der Professor ein Amt der Besoldungsgruppe R 1 ausübt, monatlich 205,54 Euro, wenn sie oder er ein Amt der Besoldungsgruppe R 2 ausübt, monatlich 230,08 Euro.

**Besoldungsordnung W**

**Besoldungsgruppe W 1**

Professorin als Juniorprofessorin, Professor als Juniorprofessor 1)

* * *

1) Nach § 44 des Brandenburgischen Hochschulgesetzes an einer Universität oder gleichgestellten Hochschule.

**Besoldungsgruppe W 2**

Professorin, Professor 1)

*   an einer Fachhochschule -

Professorin, Professor an einer Kunsthochschule 1)

Universitätsprofessorin, Universitätsprofessor 1)

* * *

1) Soweit nicht in der Besoldungsgruppe W 3.

**Besoldungsgruppe W 3**

Professorin, Professor 1)

*   an einer Fachhochschule -

Professorin, Professor an einer Kunsthochschule 1)

Universitätsprofessorin, Universitätsprofessor 1)

Präsidentin, Präsident der … 2)

Vizepräsidentin, Vizepräsident der ...
2)

Rektorin, Rektor der ...
2)

* * *

1) Soweit nicht in der Besoldungsgruppe W 2.

2) Der Amtsbezeichnung ist ein Zusatz beizufügen, der auf die Hochschule hinweist, der die Amtsinhaberin oder der Amtsinhaber angehört.

**Anlage 3**  
(zu § 38 Satz 1, § 62 Absatz 1)

#### Besoldungsordnung R (BbgBesO R)

#### Vorbemerkung

**Zulage für Richterinnen, Richter, Staatsanwältinnen und Staatsanwälte bei obersten Gerichtshöfen des Bundes sowie bei obersten Bundesbehörden**

Richterinnen, Richter, Staatsanwältinnen und Staatsanwälte erhalten, wenn sie bei obersten Gerichtshöfen des Bundes oder obersten Bundesbehörden verwendet werden, eine Stellenzulage nach den für Beamtinnen und Beamte des Bundes geltenden Vorschriften.

**Besoldungsordnung R**

**Besoldungsgruppe R 1**

Richterin, Richter am Amtsgericht

Richterin, Richter am Arbeitsgericht

Richterin, Richter am Landgericht

Richterin, Richter am Sozialgericht

Richterin, Richter am Verwaltungsgericht

Direktorin, Direktor des Amtsgerichts 1)

Direktorin, Direktor des Arbeitsgerichts 1)

Direktorin, Direktor des Sozialgerichts 1)

Staatsanwältin, Staatsanwalt 2)

* * *

1) An einem Gericht mit bis zu 3 Richterplanstellen; erhält eine Amtszulage nach Anlage 8.

2) Erhält als Gruppenleiterin oder Gruppenleiter bei der Staatsanwaltschaft bei einem Landgericht mit 4 Planstellen und mehr für Staatsanwältinnen und Staatsanwälte eine Amtszulage nach Anlage 8; anstatt einer Planstelle für eine Oberstaatsanwältin oder einen Oberstaatsanwalt als Abteilungsleiterin oder Abteilungsleiter können bei einer Staatsanwaltschaft mit 4 und 5 Planstellen für Staatsanwältinnen und Staatsanwälte eine Planstelle für eine Staatsanwältin oder einen Staatsanwalt als Gruppenleiterin oder Gruppenleiter und bei einer Staatsanwaltschaft mit 6 und mehr Planstellen für Staatsanwältinnen und Staatsanwälte 2 Planstellen für Staatsanwältinnen und Staatsanwälte als Gruppenleiterin oder Gruppenleiter ausgebracht werden.

**Besoldungsgruppe R 2**

Richterin, Richter am Amtsgericht

*   als weitere aufsichtführende Richterin oder weiterer aufsichtführender Richter - 1)
*    als die ständige Vertreterin oder der ständige Vertreter einer Direktorin oder eines Direktors - 2)

Richterin, Richter am Arbeitsgericht

*   als weitere aufsichtführende Richterin oder weiterer aufsichtführender Richter - 1)
*   als die ständige Vertreterin oder der ständige Vertreter einer Direktorin oder eines Direktors - 2)

Richterin, Richter am Finanzgericht

Richterin, Richter am Landessozialgericht

Richterin, Richter am Oberlandesgericht

Richterin, Richter am Sozialgericht

*   als weitere aufsichtführende Richterin oder weiterer aufsichtführender Richter - 1)
*   als die ständige Vertreterin oder der ständige Vertreter einer Direktorin oder eines Direktors - 2)

Vorsitzende Richterin, Vorsitzender Richter am Landgericht

Vorsitzende Richterin, Vorsitzender Richter am Verwaltungsgericht

Direktorin, Direktor des Amtsgerichts 3)

Direktorin, Direktor des Arbeitsgerichts 3)

Direktorin, Direktor des Sozialgerichts 3)

Vizepräsidentin, Vizepräsident des Amtsgerichts 4)

Vizepräsidentin, Vizepräsident des Arbeitsgerichts 4)

Vizepräsidentin, Vizepräsident des Landgerichts 5)

Vizepräsidentin, Vizepräsident des Sozialgerichts 4)

Vizepräsidentin, Vizepräsident des Verwaltungsgerichts 5)

Oberstaatsanwältin, Oberstaatsanwalt

*   als Abteilungsleiterin oder Abteilungsleiter bei einer Staatsanwaltschaft bei einem Landgericht - 6)

Leitende Oberstaatsanwältin, Leitender Oberstaatsanwalt

*   als Leiterin oder Leiter einer Staatsanwaltschaft bei einem Landgericht - 7)

* * *

1) An einem Gericht mit 15 und mehr Richterplanstellen.
Bei 22 Richterplanstellen und auf je 7 weitere Richterplanstellen kann für weitere aufsichtführende Richterinnen oder Richter je eine Richterplanstelle der Besoldungsgruppe R 2 ausgebracht werden.

2) An einem Gericht mit 8 und mehr Richterplanstellen.

3) An einem Gericht mit 4 und mehr Richterplanstellen; erhält an einem Gericht mit 8 und mehr Richterplanstellen eine Amtszulage nach Anlage 8.

4) Als die ständige Vertreterin oder der ständige Vertreter einer Präsidentin oder eines Präsidenten der Besoldungsgruppe R 3 oder R 4; erhält an einem Gericht mit 16 und mehr Richterplanstellen eine Amtszulage nach Anlage 8.

5) Erhält als die ständige Vertreterin oder der ständige Vertreter einer Präsidentin oder eines Präsidenten der Besoldungsgruppe R 3 oder R 4 eine Amtszulage nach Anlage 8.

6) Auf je 4 Planstellen für Staatsanwältinnen und Staatsanwälte kann eine Planstelle für eine Oberstaatsanwältin oder einen Oberstaatsanwalt als Abteilungsleiterin oder Abteilungsleiter ausgebracht werden; erhält als die ständige Vertreterin oder der ständige Vertreter einer Leitenden Oberstaatsanwältin oder eines Leitenden Oberstaatsanwalts der Besoldungsgruppe R 3 oder R 4 eine Amtszulage nach Anlage 8.

7) Mit bis zu 10 Planstellen für Staatsanwältinnen und Staatsanwälte; erhält eine Amtszulage nach Anlage 8.

**Besoldungsgruppe R 3**

Vorsitzende Richterin, Vorsitzender Richter am Finanzgericht

Vorsitzende Richterin, Vorsitzender Richter am Landessozialgericht

Vorsitzende Richterin, Vorsitzender Richter am Oberlandesgericht

Präsidentin, Präsident des Amtsgerichts 1)

Präsidentin, Präsident des Arbeitsgerichts 1)

Präsidentin, Präsident des Landgerichts 1)

Präsidentin, Präsident des Sozialgerichts 1)

Präsidentin, Präsident des Verwaltungsgerichts 1)

Vizepräsidentin, Vizepräsident des Finanzgerichts 2)

Vizepräsidentin, Vizepräsident des Landessozialgerichts 2)

Vizepräsidentin, Vizepräsident des Landgerichts 3)

Vizepräsidentin, Vizepräsident des Oberlandesgerichts 2)

Leitende Oberstaatsanwältin, Leitender Oberstaatsanwalt

*   als Leiterin oder Leiter einer Staatsanwaltschaft bei einem Landgericht - 4)
*   als Abteilungsleiterin oder Abteilungsleiter bei einer Staatsanwaltschaft bei einem Oberlandesgericht -

* * *

1) An einem Gericht mit bis zu 40 Richterplanstellen einschließlich der Richterplanstellen der Gerichte, über die die Präsidentin oder der Präsident die Dienstaufsicht führt.

2) Erhält als die ständige Vertreterin oder der ständige Vertreter einer Präsidentin oder eines Präsidenten der Besoldungsgruppe R 6 eine Amtszulage nach Anlage 8.

3) Als die ständige Vertreterin oder der ständige Vertreter der Präsidentin oder des Präsidenten eines Gerichts mit 81 und mehr Richterplanstellen, einschließlich der Richterplanstellen der Gerichte, über die die Präsidentin oder der Präsident die Dienstaufsicht führt.

4) Mit 11 bis 40 Planstellen für Staatsanwältinnen und Staatsanwälte.

**Besoldungsgruppe R 4**

Präsidentin, Präsident des Amtsgerichts 1)

Präsidentin, Präsident des Landgerichts 1)

Präsidentin, Präsident des Verwaltungsgerichts 1)

Vizepräsidentin, Vizepräsident des Landessozialgerichts 2)

Vizepräsidentin, Vizepräsident des Oberlandesgerichts 2)

Leitende Oberstaatsanwältin, Leitender Oberstaatsanwalt

*   als Leiterin oder Leiter einer Staatsanwaltschaft bei einem Landgericht - 3)

* * *

1) An einem Gericht mit 41 bis 80 Richterplanstellen einschließlich der Richterplanstellen der Gerichte, über die die Präsidentin oder der Präsident die Dienstaufsicht führt.

2) Als die ständige Vertreterin oder der ständige Vertreter einer Präsidentin oder eines Präsidenten der Besoldungsgruppe R 8.

3) Mit 41 und mehr Planstellen für Staatsanwältinnen und Staatsanwälte.

**Besoldungsgruppe R 5**

Präsidentin, Präsident des Finanzgerichts 1)

Präsidentin, Präsident des Landessozialgerichts 1)

Präsidentin, Präsident des Landgerichts 2)

Präsidentin, Präsident des Oberlandesgerichts 1)

Generalstaatsanwältin, Generalstaatsanwalt

*   als Leiterin oder Leiter einer Staatsanwaltschaft bei einem Oberlandesgericht - 3)

* * *

1) An einem Gericht mit bis zu 25 Richterplanstellen im Bezirk.

2) An einem Gericht mit 81 bis 150 Richterplanstellen einschließlich der Richterplanstellen der Gerichte, über die die Präsidentin oder der Präsident die Dienstaufsicht führt.

3) Mit bis zu 100 Planstellen für Staatsanwältinnen und Staatsanwälte im Bezirk.

**Besoldungsgruppe R 6**

Präsidentin, Präsident des Finanzgerichts 1)

Präsidentin, Präsident des Landessozialgerichts 2)

Präsidentin, Präsident des Oberlandesgerichts 2)

Generalstaatsanwältin, Generalstaatsanwalt

*   als Leiterin oder Leiter einer Staatsanwaltschaft bei einem Oberlandesgericht - 3)

* * *

1) An einem Gericht mit 26 und mehr Richterplanstellen im Bezirk.

2) An einem Gericht mit 26 bis 100 Richterplanstellen im Bezirk.

3) Mit 101 und mehr Planstellen für Staatsanwältinnen und Staatsanwälte im Bezirk.

**Besoldungsgruppe R 7**

(nicht besetzt)

**Besoldungsgruppe R 8**

Präsidentin, Präsident des Landessozialgerichts 1)

Präsidentin, Präsident des Oberlandesgerichts 1)

* * *

1) An einem Gericht mit 101 und mehr Richterplanstellen im Bezirk.

**Besoldungsgruppe R 9**

(nicht besetzt)

**Besoldungsgruppe R 10**

(nicht besetzt)

 

* * *

### Anlagen

1

[Anlage 4](/br2/sixcms/media.php/68/Anlage%204.pdf "Anlage 4") 184.5 KB

2

[Anlage 5](/br2/sixcms/media.php/68/Anlage%205.pdf "Anlage 5") 191.9 KB

3

[Anlage 6](/br2/sixcms/media.php/68/Anlage%206.pdf "Anlage 6") 19.4 KB

4

[Anlage 7](/br2/sixcms/media.php/68/Anlage%207.pdf "Anlage 7") 12.1 KB

5

[Anlage 8](/br2/sixcms/media.php/68/Anlage%208.pdf "Anlage 8") 291.9 KB