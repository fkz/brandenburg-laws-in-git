## Gesetz über die Rechtsverhältnisse der Mitglieder des Landtags Brandenburg (Abgeordnetengesetz - AbgG)

**Inhaltsübersicht**

### Abschnitt 1  
Mitgliedschaft im Landtag und Beruf

[§ 1 Erwerb und Verlust der Mitgliedschaft](#1)

[§ 2 Schutz der freien Mandatsausübung](#2)

[§ 3 Wahlvorbereitungsurlaub](#3)

[§ 4 Berufs- und Betriebszeiten](#4)

### Abschnitt 2  
Leistungen an Abgeordnete

[§ 5 Entschädigung](#5)

[§ 6 Anrechnung anderer Einkünfte, Doppelmandat](#6)

[§ 7 Amtsausstattung](#7)

[§ 8 Erstattung von Aufwendungen für Beschäftigte, Praktikantinnen und Praktikanten sowie Wahlkreisbüros](#8)

[§ 9 Pflichtsitzungen](#9)

[§ 10 Erstattung von Fahrkosten](#10)

[§ 11 Erstattung von Reisekosten in besonderen Fällen](#11)

[§ 12 Berufliche Qualifizierung](#12)

[§ 13 Ausführungsbestimmungen](#13)

### Abschnitt 3  
Leistungen nach Ausscheiden aus dem Landtag

[§ 14 Übergangsgeld](#14)

[§ 15 Pflichtmitgliedschaft im Versorgungswerk, Altersrente](#15)

[§ 16 Gesundheitsschäden](#16)

### Abschnitt 4  
Zuschuss zu den Kosten der Kranken- und Pflegeversicherung

[§ 17 Zuschuss zu den Kranken- und Pflegeversicherungsbeiträgen](#17)

### Abschnitt 5  
Gemeinsame Vorschriften

[§ 18 Begriffsbestimmungen](#18)

[§ 19 Beginn und Ende von Ansprüchen, Mitwirkungspflichten](#19)

[§ 20 Zahlungsvorschriften](#20)

[§ 21 Verzicht, Übertragbarkeit](#21)

[§ 22 Bericht der Präsidentin oder des Präsidenten](#22)

### Abschnitt 6  
Angehörige des öffentlichen Dienstes

[§ 23 Unvereinbarkeit von Amt und Mandat](#23)

[§ 24 Wiederverwendung nach Beendigung des Mandats](#24)

### Abschnitt 7  
Verhaltenspflichten

[§ 25 Annahme von Leistungen](#25)

[§ 25a Zulässigkeit der Annahme von Leistungen](#26)

[§ 25b Abführung von Leistungen](#27)

[§ 26 Anzeigepflichten](#28)

[§ 26a Verstöße gegen die Verhaltensregeln](#29)

[§ 27 Überprüfung von Abgeordneten](#30)

[§ 27a Verschwiegenheitspflicht und Aussagegenehmigung](#31)

### Abschnitt 8  
Weitergeltung alten Rechts, Übergangsbestimmungen, ergänzende Vorschriften

[§ 28 Versorgungsansprüche, -anwartschaften und -abfindungen](#32)

[§ 29 Abgeordnete mit Höchstversorgung](#33)

[§ 30 Zuschuss zu den Krankenversicherungsbeiträgen der Versorgungsberechtigten](#34)

[§ 31 Beginn der Ansprüche der in den sechsten Landtag Gewählten](#35)

[§ 32 Berufliche Qualifizierung und Evaluation](#36)

[§ 33 Evaluation der Altersrente](#37)

[§ 34 Datenverarbeitung](#38)

### Abschnitt 1  
Mitgliedschaft im Landtag und Beruf

#### § 1 Erwerb und Verlust der Mitgliedschaft

Erwerb und Verlust der Mitgliedschaft im Landtag regeln sich nach den Vorschriften des Brandenburgischen Landeswahlgesetzes.

#### § 2 Schutz der freien Mandatsausübung

(1) Niemand darf gehindert werden, sich um ein Mandat im Landtag zu bewerben, es zu übernehmen oder auszuüben.

(2) Benachteiligungen am Arbeitsplatz im Zusammenhang mit der Bewerbung um ein Mandat sowie der Annahme und Ausübung eines Mandats sind unzulässig.

(3) In der Zeit vom Einreichen des Wahlvorschlags bis ein Jahr nach der Beendigung des Mandats sind Kündigungen und Entlassungen nur aus wichtigem Grund zulässig; sie sind unzulässig, wenn sie im Zusammenhang mit der Annahme und Ausübung des Mandats erfolgen.

#### § 3 Wahlvorbereitungsurlaub

Wer sich um einen Sitz im Landtag bewirbt, hat innerhalb der letzten zwei Monate vor dem Wahltag auf Antrag Anspruch auf den zur Vorbereitung seiner Wahl erforderlichen Urlaub von bis zu zwei Monaten.
Ein Anspruch auf Fortzahlung des Gehaltes oder des Lohnes besteht für die Dauer der Beurlaubung nicht.

#### § 4 Berufs- und Betriebszeiten

(1) Die Zeit der Mitgliedschaft im Landtag ist nach Beendigung des Mandats auf die Berufs- und Betriebszugehörigkeit anzurechnen.

(2) Im Rahmen einer bestehenden betrieblichen oder überbetrieblichen Altersversorgung wird die Anrechnung nach Absatz 1 nur im Hinblick auf die Erfüllung der festgelegten Unverfallbarkeitsfristen der betrieblichen Altersversorgung vorgenommen.

### Abschnitt 2  
Leistungen an Abgeordnete

#### § 5 Entschädigung

(1) Ein Mitglied des Landtags erhält eine monatliche Entschädigung

1.  in Höhe von 7 604,62 Euro, die gemäß Absatz 4 angepasst wird, sowie
2.  in Höhe von 1 003,39 Euro, die gemäß Absatz 5 angepasst wird.

(2) Zusätzlich erhält das Mitglied eine monatliche Entschädigung in Höhe von 1 856,86 Euro, die zur Finanzierung der Alters- und Hinterbliebenenversorgung gemäß § 15 Absatz 4 direkt an das Versorgungswerk der Mitglieder der Landtage Nordrhein-Westfalen, Brandenburg und Baden-Württemberg abgeführt wird.
Die Entschädigung wird gemäß Absatz 4 angepasst.

(3) Die Präsidentin oder der Präsident des Landtags und die Vizepräsidentinnen und Vizepräsidenten, die Fraktionsvorsitzenden sowie die Parlamentarischen Geschäftsführerinnen und Parlamentarischen Geschäftsführer erhalten eine Amtszulage.
Vorbehaltlich der Sätze 4 und 5 beträgt die Amtszulage für die Präsidentin oder den Präsidenten und die Fraktionsvorsitzenden 70 Prozent sowie für die Vizepräsidentinnen und Vizepräsidenten sowie die Parlamentarischen Geschäftsführerinnen und Parlamentarischen Geschäftsführer 35 Prozent der Entschädigungen nach den Absätzen 1 und 2.
Hat eine Fraktion zwei gleichberechtigte Fraktionsvorsitzende, erhalten sie jeweils die Hälfte der Amtszulage nach Satz 2.
Der auf die Entschädigung nach Absatz 2 entfallende Anteil der Amtszulage wird an das Versorgungswerk abgeführt, soweit er die in § 5 Absatz 1 Nummer 8 Satz 2 des Körperschaftsteuergesetzes festgelegte Höchstgrenze nicht überschreitet; die Amtszulage vermindert sich um den die Höchstgrenze überschreitenden Betrag.
Hat eine Fraktion zwei Fraktionsvorsitzende, darf die Summe ihrer Amtszulagen die Amtszulage einer oder eines alleinigen Fraktionsvorsitzenden nicht übersteigen.

(4) Die Entschädigungen gemäß Absatz 1 Nummer 1 und Absatz 2 werden entsprechend der Einkommensentwicklung der Arbeitnehmerentgelte in Brandenburg angepasst; maßgeblich sind die zusammengefassten Wirtschaftsbereiche gemäß der Verordnung (EU) Nr. 549/2013 des Europäischen Parlaments und des Rates vom 21.
Mai 2013 zum Europäischen System Volkswirtschaftlicher Gesamtrechnungen auf nationaler und regionaler Ebene in der Europäischen Union (ABl. L 174 vom 26.6.2013, S. 1), die zuletzt durch die Verordnung (EU) 2015/1342 der Kommission vom 22. April 2015 (ABl.
L 207 vom 4.8.2015, S. 35) geändert worden ist, in der jeweils geltenden Fassung.
Bis zur Herstellung einheitlicher Einkommensverhältnisse im Gebiet der Bundesrepublik Deutschland erfolgt diese Anpassung abzüglich der Differenz der Einkommensveränderung der alten und der neuen Länder ohne Einbeziehung des Landes Berlin im Bezugsjahr in Prozentpunkten, soweit die Arbeitnehmerentgelte in den neuen Ländern stärker steigen als in den alten Ländern.

(5) Die Entschädigung gemäß Absatz 1 Nummer 2 wird entsprechend der Veränderung des Verbraucherpreisindex im Land Brandenburg angepasst.

(6) Die Anpassungen nach den Absätzen 4 und 5 erfolgen jährlich zum 1.
Januar und enden sechs Monate nach dem Zusammentritt des achten Landtags.

(7) Die jährliche Anpassung der Entschädigung gemäß Absatz 1 Nummer 1 und 2 nach den Maßgaben der Absätze 4 bis 6 und 8 wird für die Jahre 2021 und 2022 ausgesetzt.

(8) Das Amt für Statistik Berlin-Brandenburg teil die prozentuale Veränderung der nach Absatz 4 zu ermittelnden Einkommensentwicklung unter Berücksichtigung der Differenz zwischen der Einkommensentwicklung der alten und der neuen Länder ohne Einbeziehung des Landes Berlin sowie die prozentuale Veränderung des Verbraucherpreisindex im Land Brandenburg bis zum 1. September eines jeden Jahres der Präsidentin oder dem Präsidenten in Form eines Berichts mit.
Maßgeblich sind die Daten des abgelaufenen Jahres im Vergleich zum vorangegangenen Jahr.
Die Präsidentin oder der Präsident veröffentlicht den Bericht als Drucksache und die angepassten Beiträge der Entschädigungen nach den Absätzen 1 und 2 vor Ablauf des Jahres im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I.

#### § 6 Anrechnung anderer Einkünfte, Doppelmandat

(1) Hat ein Mitglied des Landtags neben den Entschädigungen nach § 5 Anspruch auf Einkommen aus einem Amtsverhältnis oder aus einer Verwendung im öffentlichen Dienst, so werden die Entschädigungen nach § 5 Absatz 1 und 2 um 75 Prozent gekürzt.

(2) Die Entschädigungen nach § 5 Absatz 1 und 2 ruhen neben Versorgungsbezügen aus einem Amtsverhältnis oder aus einer Verwendung im öffentlichen Dienst und neben Versorgungsbezügen aus der Mitgliedschaft in einer anderen gesetzgebenden Körperschaft in Höhe von 75 Prozent, höchstens jedoch in Höhe von 50 Prozent der Versorgungsbezüge aus dem Amtsverhältnis oder der Verwendung im öffentlichen Dienst.
Entsprechendes gilt beim Bezug einer Rente aus einer zusätzlichen Alters- und Hinterbliebenenversorgung für Angehörige des öffentlichen Dienstes.

(3) Die Absätze 1 und 2 sind nicht auf die Zahlung einer monatlichen oder jährlichen Sonderzuwendung, eines jährlichen Urlaubsgeldes oder auf sonstige vergleichbare Sonder- oder Einmalzahlungen oder entsprechende Leistungen aufgrund gesetzlicher oder tariflicher Regelungen anzuwenden.
Soweit nach den Absätzen 1 und 2 mehrere Kürzungstatbestände gleichzeitig zutreffen, erfolgt die Kürzung nur einmal mit dem jeweiligen Höchstbetrag.

(4) Für die Zeit, für die ein Mitglied des Landtags eine Entschädigung als Mitglied des Europäischen Parlaments oder des Deutschen Bundestages erhält, werden die Entschädigungen nach § 5 Absatz 1 und 2 nicht gewährt.
Für die Zeit, für die es Aufwandsentschädigung als Mitglied des Europäischen Parlaments oder des Deutschen Bundestages erhält, werden die Leistungen nach den §§ 7, 8 und 12 nicht gewährt.

#### § 7 Amtsausstattung

(1) Ein Mitglied des Landtags erhält für die Ausübung seines Mandats eine Amtsausstattung, die Sachleistungen umfasst.

(2) Zur Amtsausstattung gehören die Benutzung der durch den Landtag zur Verfügung gestellten Informations- und Kommunikationseinrichtungen, die Nutzung eines Büroraumes oder eines Büroarbeitsplatzes nach Maßgabe des § 6 Absatz 5 Satz 2 des Fraktionsgesetzes und die Inanspruchnahme sonstiger zur Verfügung gestellter Sachleistungen des Landtags.

(3) Die Mitglieder des Landtags erhalten auf Anforderung die Freifahrtberechtigung der Deutschen Bahn AG für die Mitglieder der gesetzgebenden Körperschaften des Bundes und der Länder für den Bereich des Landes Brandenburg.
Sie erstatten dem Land die hierfür von der Deutschen Bahn AG in Rechnung gestellten Kosten.

#### § 8 Erstattung von Aufwendungen für Beschäftigte, Praktikantinnen und Praktikanten sowie Wahlkreisbüros

(1) Auf Antrag werden den Mitgliedern des Landtags erstattet:

1.  die nachgewiesenen Aufwendungen für die Anstellung von Beschäftigten bis zu einem Betrag, der dem Bruttogehalt eines Beschäftigten des Landes Brandenburg in der Entgeltgruppe E 13 Stufe 3 TV-L zuzüglich der gesetzlichen Pflichtbeiträge und sonstiger Aufwendungen, die in sinngemäßer Anwendung tarifrechtlicher Bestimmungen für Beschäftigte des Landes gezahlt werden,
2.  die nachgewiesenen Aufwendungen für eine angemessene Vergütung von Praktikantinnen und Praktikanten bis zu einer Höhe von 1 800 Euro pro Jahr,
3.  die tatsächlich entstandenen und nachgewiesenen Mietkosten für angemessene Wahlkreisbüros als Stätte der mandatsbezogenen Wahlkreisarbeit bis zu einem Betrag von 800 Euro sowie die Aufwendungen für die erstmalige Ausstattung dieser Büros bis zu einem Höchstbetrag von 2 500 Euro in der Wahlperiode und einem Höchstbetrag von 1 000 Euro in den für das Mitglied des Landtags ununterbrochen daran anschließenden weiteren Wahlperioden und
4.  die nachgewiesenen Aufwendungen für die Verbesserung der Zugänglichkeit und Nutzbarkeit der Wahlkreisbüros, insbesondere für Menschen mit Beeinträchtigungen bis zu einem Betrag von 2 500 Euro pro Wahlperiode sowie für weitere unterstützende Maßnahmen zugunsten von Menschen mit Beeinträchtigungen bis zu einem Betrag von 300 Euro pro Kalenderjahr.

(2) Die Abrechnung und Auszahlung der Gehälter oder Vergütungen gemäß Absatz 1 Nummer 1 und 2 übernimmt die Präsidentin oder der Präsident des Landtags.

(3) Aufwendungen gemäß Absatz 1 Nummer 1 und 2, die anlässlich der Beschäftigung von Personen entstehen, die mit dem antragstellenden Mitglied oder einem anderen Mitglied des Landtags verheiratet sind oder in einer eingetragenen Lebenspartnerschaft leben oder mit dem Mitglied des Landtags verschwägert oder bis zum dritten Grad verwandt sind, werden nicht übernommen.
Eine Erstattung von Mietkosten für ein Wahlkreisbüro, das im Eigentum des Mitglieds des Landtags steht oder von einer Person vermietet wird, die mit dem Mitglied des Landtags verheiratet ist, mit ihm in einer eingetragenen Lebenspartnerschaft lebt, mit ihm verschwägert oder bis zum dritten Grad verwandt ist, erfolgt nicht.

(4) Ein Ersatz von Aufwendungen für eine Beschäftigte oder einen Beschäftigten gemäß Absatz 1 Nummer 1 kommt nur in Betracht, wenn der Präsidentin oder dem Präsidenten ein Führungszeugnis für diese Person vorgelegt wird.
Enthält das Führungszeugnis einen Eintrag wegen einer vorsätzlichen Straftat, aufgrund derer eine Gefährdung parlamentarischer Schutzgüter im konkreten Einzelfall nach Abwägung aller Umstände zu befürchten ist, kann die Erstattung von der Präsidentin oder dem Präsidenten abgelehnt werden.

#### § 9 Pflichtsitzungen

(1) Sitzungen des Landtags, des Präsidiums, der Ausschüsse und der sonstigen parlamentarischen Gremien sind Pflichtsitzungen.
Sie finden grundsätzlich am Sitz des Landtags statt.
Ausnahmen kann die Präsidentin oder der Präsident auf schriftlichen Antrag zulassen.
Pflichtsitzungen sind auch die Sitzungen der Fraktionen und Gruppen am Sitz des Landtags.

(2) In jeder Pflichtsitzung wird eine Anwesenheitsliste ausgelegt, in die sich jedes anwesende Mitglied des Landtags einzutragen hat.

#### § 10 Erstattung von Fahrkosten

(1) Die Mitglieder des Landtags erhalten für Fahrten zum Landtag aus Anlass von Pflichtsitzungen auf Antrag die Kosten in Höhe von 0,30 Euro je gefahrenen Kilometer erstattet.
Werden öffentliche Verkehrsmittel genutzt, so wird der tatsächlich aufgewendete Fahrpreis erstattet, soweit nicht das Mitglied über eine Freifahrtberechtigung gemäß § 7 Absatz 3 verfügt und diese eingesetzt werden kann.
Erstattet werden höchstens die Kosten, die für die kürzeste Wegstrecke zwischen Wohnort und Landtag anfallen.
Für Pflichtsitzungen an einem anderen Ort als dem Sitz des Landtags gemäß § 9 Absatz 1 Satz 3 ist die kürzeste Wegstrecke zwischen Wohnort und Sitzungsort maßgebend.
Eine andere als die kürzeste Wegstrecke kann zugrunde gelegt werden, wenn diese offensichtlich verkehrsgünstiger ist und vom Mitglied des Landtags regelmäßig für den Weg zwischen Wohnort und Landtag benutzt wird.

(2) Die Anträge sind grundsätzlich vierteljährlich innerhalb des laufenden Haushaltsjahres, spätestens bis zum 31. März des folgenden Jahres zu stellen.
Bei dieser Frist handelt es sich um eine Ausschlussfrist.
Ist der Antrag nicht fristgerecht gestellt worden, erlischt der Anspruch auf Fahrkostenerstattung.

(3) Mitgliedern des Landtags, denen in sinngemäßer Anwendung des § 52 Satz 2 der Landeshaushaltsordnung ein landeseigener Dienstwagen zur uneingeschränkten Nutzung zur Verfügung steht, werden Fahrkosten nicht erstattet.
Regelungen zur Nutzung der Dienstwagen des Landtags nach Satz 1 erlässt die Präsidentin oder der Präsident des Landtags.

#### § 11 Erstattung von Reisekosten in besonderen Fällen

(1) Vorbehaltlich der Absätze 2 bis 4 werden den Mitgliedern des Landtags auf Antrag die Kosten für mandatsbedingte Reisen außerhalb des Landes Brandenburg in sinngemäßer Anwendung der beamtenrechtlichen Reisekostenregelungen des Landes erstattet.
Die Reisekostenerstattung erstreckt sich auf

1.  die Teilnahme an Reisen von Ausschüssen, anderen parlamentarischen Gremien, des Präsidiums sowie vergleichbare Reisen,
2.  Reisen im Auftrag der Präsidentin oder des Präsidenten und
3.  Reisen einzelner Mitglieder des Landtags in Ausübung ihres Mandats, wenn sie im Interesse des Landes liegen.

Die Reisen bedürfen der Zustimmung der Präsidentin oder des Präsidenten im Einvernehmen mit dem Präsidium; in den Fällen des Satzes 2 Nummer 2 und 3 ist das Einvernehmen des Präsidiums nur bei Auslandsreisen erforderlich.

(2) Bei Reisen außerhalb des Landes Brandenburg werden den Mitgliedern des Landtags, die über eine Freifahrtberechtigung nach § 7 Absatz 3 verfügen, nur die außerhalb des Geltungsbereichs dieser Freifahrtberechtigung durch Benutzung öffentlicher Verkehrsmittel entstandenen Fahrkosten erstattet.
Auf schriftlichen Antrag kann die Präsidentin oder der Präsident die Benutzung anderer Verkehrsmittel genehmigen.

(3) Bei genehmigter Benutzung eines Kraftwagens gemäß Absatz 1 oder Absatz 2 wird eine Wegstreckenentschädigung in Höhe von 0,30 Euro je gefahrenen Kilometer gewährt, wenn das Mitglied des Landtags

1.  einen eigenen Kraftwagen,
2.  einen Kraftwagen gegen Entgelt,
3.  einen Kraftwagen, dessen Betriebskosten von ihm getragen werden,

benutzt.

(4) Findet während der sitzungsfreien Zeit eine Pflichtsitzung statt, so sind dem teilnehmenden Mitglied des Landtags die entstandenen notwendigen Fahrkosten für Hin- und Rückreise zum Sitzungs- und Urlaubsort zu erstatten, sofern es sich außerhalb des Landes Brandenburg aufhält und diesen Aufenthalt zur Teilnahme an der Sitzung unterbricht.

(5) Den Mitgliedern des Landtags werden die notwendigen Kosten der durch Pflichtsitzungen veranlassten Übernachtungen erstattet.
Anstelle der Übernahme der Kosten für einzelne Übernachtungen können die Mietkosten für eine Zweitwohnung am Sitz des Landtags gegen Nachweis bis zu einem monatlichen Höchstbetrag von 250 Euro erstattet werden.
Satz 1 gilt entsprechend, wenn Mitglieder des Landtags im Auftrag der Präsidentin oder des Präsidenten den Landtag bei Veranstaltungen vertreten.

(6) Bei Dienstreisen der Präsidentin oder des Präsidenten oder der Vizepräsidentinnen oder Vizepräsidenten im Auftrag der Präsidentin oder des Präsidenten werden die entstandenen Auslagen erstattet.

#### § 12 Berufliche Qualifizierung

(1) Während des letzten Jahres einer Wahlperiode oder innerhalb eines Jahres nach Ausscheiden aus dem Landtag haben die Mitglieder des Landtags unter den Voraussetzungen der Absätze 2 und 3 Anspruch auf Erstattung der Kosten einer angemessenen Übergangsqualifizierung auf der Grundlage eines dem jeweiligen Bedarf entsprechenden individuellen beruflichen Fort- oder Weiterbildungskonzepts zur Eingliederung des Mitglieds des Landtags in das Berufsleben.

(2) Die Kosten der Übergangsqualifizierung werden bis zu einem im Haushaltsgesetz festzulegenden Höchstbetrag auf Antrag erstattet.
Finanziert werden ausschließlich nachgewiesene Kosten für Qualifizierungsmaßnahmen.

(3) Ein Anspruch auf Kostenerstattung für eine Übergangsqualifizierung besteht nicht, wenn das Mitglied nach Beendigung seines Mandats keinen Anspruch auf Übergangsgeld hat oder Übergangsgeld aus den in § 14 Absatz 2 genannten Gründen nicht gezahlt wird.
Die Kostenübernahme endet, wenn das Mitglied in den Landtag der folgenden Wahlperiode wiedergewählt wird, mit Annahme des Mandats.
In diesem Fall ist die Hälfte der bis dahin erstatteten Kosten zurückzuzahlen.

#### § 13 Ausführungsbestimmungen

(1) Das Nähere über die Amtsausstattung nach § 7, über die Erstattung von Aufwendungen, Fahrkosten und Reisekosten nach den §§ 8, 10 und 11 sowie über die berufliche Qualifizierung nach § 12 regelt das Präsidium des Landtags in Richtlinien, wobei

1.  in einer Richtlinie zu § 8 Absatz 1 Nummer 1 insbesondere Näheres zu den erstattungsfähigen Aufwendungen in sinngemäßer Anwendung der tarifrechtlichen Bestimmungen für die Beschäftigten des Landes sowie zur Erstattungsfähigkeit von Aufwendungen in Bezug auf die persönlichen Voraussetzungen einer Beschäftigung bei einem Mitglied des Landtags, zur maximalen Anzahl der Mitarbeiterinnen oder Mitarbeiter, die von einem Mitglied des Landtags zur gleichen Zeit und innerhalb eines bestimmten Zeitraums beschäftigt werden können, und zur einheitlichen Gestaltung von Arbeitsverträgen geregelt werden kann,
2.  in einer Richtlinie zu § 8 Absatz 1 Nummer 2 hinsichtlich der Erstattungsfähigkeit von Aufwendungen insbesondere Näheres in Bezug auf die maximale Dauer eines Praktikumsverhältnisses, zur minimalen und maximalen Höhe der Vergütung, zu den persönlichen Voraussetzungen der Praktikantinnen und Praktikanten, zur maximalen Anzahl von Praktikantinnen und Praktikanten, die von einem Mitglied des Landtags innerhalb eines bestimmten Zeitraums beschäftigt werden können, und zur einheitlichen Gestaltung von Praktikumsverträgen geregelt werden kann,
3.  in Richtlinien zu § 8 Absatz 1 Nummer 3 insbesondere Näheres zu den Erstattungsvoraussetzungen von Aufwendungen in Bezug auf die Angemessenheit der Mietkosten und des Mietzinses von Wahlkreisbüros sowie zum Erstattungsverfahren geregelt werden kann,
4.  in einer Richtlinie zu § 8 Absatz 1 Nummer 4 insbesondere Näheres zu den Erstattungsvoraussetzungen von Aufwendungen für die Verbesserung der Zugänglichkeit und Nutzbarkeit von Wahlkreisbüros und von Aufwendungen für weitere unterstützende Maßnahmen sowie zum Erstattungsverfahren geregelt werden kann,
5.  in einer Richtlinie zu § 11 insbesondere weitere Sitzungsanlässe hinsichtlich der Fahrkosten- und Übernachtungskostenerstattungen den Pflichtsitzungen gleichgestellt werden können sowie Näheres zum Erstattungsverfahren geregelt werden kann,
6.  in einer Richtlinie zu § 12 insbesondere Näheres zur Erforderlichkeit einer beruflichen Eingliederung sowie zum Erstattungsverfahren geregelt werden kann und
7.  in einer Richtlinie zu § 7 insbesondere Näheres zu Art und Umfang der IT-Ausstattung, des Bereitstellungs- und Rückgabeverfahrens sowie sonstiger Serviceleistungen in diesem Zusammenhang geregelt werden kann.

(2) Die Belange von Mitgliedern des Landtags mit Behinderung sind in den Richtlinien angemessen zu berücksichtigen, wobei dem Grad der Behinderung ebenso wie möglichen Ansprüchen nach dem Neunten Buch Sozialgesetzbuch Rechnung zu tragen ist.

(3) Die Richtlinien werden auf der Internetseite des Landtags veröffentlicht.

### Abschnitt 3  
Leistungen nach Ausscheiden aus dem Landtag

#### § 14 Übergangsgeld

(1) Ein Mitglied des Landtags erhält nach seinem Ausscheiden aus dem Landtag auf Antrag ein Übergangsgeld, sofern es dem Landtag mindestens ein Jahr angehört hat.
Der Antrag ist innerhalb von drei Monaten nach dem Ausscheiden zu stellen.
Das Übergangsgeld wird in Höhe von 80 Prozent der zum Zeitpunkt des Ausscheidens maßgebenden Entschädigung nach § 5 Absatz 1 für mindestens drei Monate gewährt.
Für jedes weitere Jahr der ununterbrochenen Zugehörigkeit zum Landtag wird das Übergangsgeld für einen weiteren Monat, insgesamt jedoch höchstens für 18 Monate, gewährt.
Das Übergangsgeld wird auf Antrag monatlich in Höhe des halben Betrags gewährt; die Bezugsdauer verlängert sich dementsprechend.

(2) Das Übergangsgeld wird nicht gezahlt, wenn das ehemalige Mitglied

1.  Altersrente nach § 15 Absatz 5 dieses Gesetzes oder Altersversorgung nach den §§ 11 und 12 des Abgeordnetengesetzes in der bis zum Inkrafttreten dieses Gesetzes geltenden Fassung erhält oder die altersmäßigen Voraussetzungen für den ungeminderten Bezug dieser Leistungen erfüllt,
2.  eine Versorgung nach § 16 erhält,
3.  Versorgung aus der Mitgliedschaft in einer anderen gesetzgebenden Körperschaft erhält,
4.  Ruhegehalt aus einem öffentlich-rechtlichen Amtsverhältnis oder einer Verwendung im öffentlichen Dienst bezieht oder
5.  eine Rente nach § 33 Absatz 2 oder 3 Nummer 2 des Sechsten Buches Sozialgesetzbuch bezieht.

(3) Auf das Übergangsgeld nach Absatz 1 sind anzurechnen:

1.  Einkünfte aus
    1.  Land- und Forstwirtschaft,
    2.  Gewerbebetrieb,
    3.  selbstständiger Arbeit,
    4.  nichtselbstständiger Arbeit,
2.  Erwerbsersatzeinkommen, das aufgrund oder in entsprechender Anwendung öffentlich-rechtlicher Vorschriften kurzfristig erbracht wird, um Erwerbseinkommen zu ersetzen,
3.  Entschädigungen aus der Mitgliedschaft in einer anderen gesetzgebenden Körperschaft,
4.  Versorgungsbezüge aus einem öffentlich-rechtlichen Amtsverhältnis oder einer Verwendung im öffentlichen Dienst,
5.  Renten aus der gesetzlichen Rentenversicherung,

soweit nicht nach Absatz 2 ihr Bezug die Leistung von Übergangsgeld ganz ausschließt.
Bei der Ermittlung des Einkommens im Sinne des Satzes 1 sind Betriebsausgaben und Werbungskosten abzuziehen.
Wird ein ehemaliges Mitglied des Landtags, das gemäß § 24 Anspruch auf Wiederverwendung im öffentlichen Dienst hat, unter Wegfall der Bezüge beurlaubt oder nimmt es Sonderurlaub unter Verzicht auf die Fortzahlung des Entgelts, wird ein fiktives Einkommen angerechnet, das dem Einkommen entspricht, das es bei Fortsetzung seiner bisherigen Tätigkeit erhalten hätte.

(4) Die Anrechnung der Einkünfte und Bezüge nach Absatz 3 erfolgt monatsbezogen.
Werden sie nicht in Monatsbeträgen erzielt, ist ein Zwölftel der Einkünfte und Bezüge des Kalenderjahres zugrunde zu legen.
Soweit die Einkünfte nur durch einen Steuerbescheid nachgewiesen werden können, sind bis zur Vorlage prüfungsfähiger Unterlagen angemessene monatliche Abschlagszahlungen auf das Übergangsgeld zu gewähren.

(5) Tritt ein ehemaliges Mitglied wieder in den Landtag ein, so ruht der Anspruch nach Absatz 1 mit dem Zeitpunkt des Wiedereintrittes.

#### § 15 Pflichtmitgliedschaft im Versorgungswerk, Altersrente

(1) Zur Vorsorge für das Alter und zur Unterstützung des überlebenden Ehegatten, des Lebenspartners oder der Lebenspartnerin und der Waisen werden die Mitglieder des Landtags Brandenburg Mitglieder im Versorgungswerk der Mitglieder der Landtage von Nordrhein-Westfalen, Brandenburg und Baden-Württemberg (Versorgungswerk – VLT).
Das Nähere über den Aufbau und die innere Struktur des Versorgungswerks, über die Beteiligung der brandenburgischen Mitglieder an seinen Organen, über das Verfahren des Versorgungswerks sowie über die Rechte und Pflichten der Landtage einschließlich ihres Kündigungsrechts regelt das Versorgungswerkgesetz Brandenburg vom 19.
Juni 2013 (GVBl. I Nr. 23 S. 17), das zuletzt durch Artikel 1 des Gesetzes vom 11.
November 2019 (GVBl.
I Nr. 52) geändert worden ist, in der jeweils geltenden Fassung, der Vertrag zwischen dem Landtag Nordrhein-Westfalen, dem Landtag Brandenburg und dem Landtag von Baden-Württemberg über das Versorgungswerk der Mitglieder der Landtage von Nordrhein-Westfalen, Brandenburg und Baden-Württemberg und die Satzung des Versorgungswerks.

(2) Die Mitglieder des Landtags Brandenburg sind Pflichtmitglieder des Versorgungswerks.
§ 29 bleibt unberührt.
Die Mitgliedschaft im Versorgungswerk endet mit dem Tod des Mitglieds sowie im Falle der Erstattung der Beiträge zum Versorgungswerk nach Maßgabe der Satzung des Versorgungswerks (Satzung).

(3) Das Versorgungswerk erbringt nach Maßgabe des Vertrages und der Satzung auf Antrag folgende Leistungen:

1.  Altersrente,
2.  Hinterbliebenenrente,
3.  Erstattung von Beiträgen als Versorgungsabfindung beziehungsweise Nachversicherung in der gesetzlichen Rentenversicherung entsprechend den Bestimmungen im Abgeordnetengesetz des Deutschen Bundestages; anstelle der Erstattung der Beiträge wird die Mandatszeit auf Antrag als Dienstzeit im Sinne des Besoldungs- und Versorgungsrechts der Beamtinnen und Beamten, Richterinnen und Richter sowie Soldatinnen und Soldaten berücksichtigt,
4.  Kapitalabfindung für Witwen und Witwer, deren Rentenanspruch durch Wiederverheiratung oder Eingehen einer neuen Lebenspartnerschaft erlischt.

(4) Vorbehaltlich des § 29 zahlt jedes Mitglied des Landtags einen monatlichen Pflichtbeitrag zum Versorgungswerk in Höhe der Abgeordnetenentschädigung nach § 5 Absatz 2.
Die Entschädigung wird vom Land einbehalten und unmittelbar als Beitrag an das Versorgungswerk abgeführt.
Die Höhe der Altersrente ist von der Dauer der Beitragszahlung sowie dem Lebensalter des Mitglieds zum jeweiligen Zeitpunkt der Zahlung abhängig.
Eine Differenzierung der Rentenhöhen nach dem Geschlecht erfolgt nicht.
Die Rente wird erst nach dem Ausscheiden aus dem Landtag gewährt; sie ruht bei einer erneuten Mitgliedschaft im Landtag bis zum Ausscheiden.

(5) Jedes Mitglied hat nach dem Ausscheiden aus dem Landtag Anspruch auf eine lebenslange Altersrente, sobald es das 67.
Lebensjahr vollendet hat, sofern es zu diesem Zeitpunkt mindestens 30 Monate Beiträge in der gemäß Absatz 4 Satz 1 festgelegten Höhe in das Versorgungswerk gezahlt hat und davon mindestens zwölf Monate Beiträge nach Absatz 4 Satz 1 als Mitglied des Landtags erbracht wurden.
Ein Rentenbeginn mit Vollendung des 62. Lebensjahres ist möglich unter Inkaufnahme von Abschlägen nach Maßgabe der Satzung.

(6) Hinterbliebenenrenten werden gewährt, wenn das Mitglied zum Zeitpunkt des Todes mindestens 30 Monate Beiträge in der gemäß Absatz 4 Satz 1 festgelegten Höhe in das Versorgungswerk gezahlt hat und davon mindestens zwölf Monate Beiträge nach Absatz 4 Satz 1 als Mitglied des Landtags erbracht wurden.
Die Witwen- oder Witwerrente beträgt 55 Prozent des Rentenanspruchs oder der Rentenanwartschaft, die das Mitglied im Zeitpunkt seines Todes erreicht hat.
Sie vermindert sich für jedes volle Kalenderjahr, um das die Witwe oder der Witwer mehr als 15 Jahre jünger als das Mitglied ist, um 5 Prozent, höchstens jedoch auf 27,5 Prozent.
Die Waisenrente beträgt bei Halbwaisen 12 Prozent, bei Vollwaisen 20 Prozent des Rentenanspruchs oder der Rentenanwartschaft, die das Mitglied im Zeitpunkt seines Todes erreicht hat.

(7) Eine Anrechnung der Leistungen des Versorgungswerks auf das Ruhegehalt, auf Versorgungs- und Rentenbezüge der Angehörigen des öffentlichen Dienstes findet nicht statt.
Bei dem Zusammentreffen von Versorgungsansprüchen nach dem Abgeordnetengesetz in der bis zum Inkrafttreten dieses Gesetzes geltenden Fassung und Renten aus dem Versorgungswerk darf ein Betrag von 43,47 Prozent der Abgeordnetenbezüge nach § 5 Absatz 1 und 3 nicht überschritten werden.
Die verbleibenden Versorgungsansprüche werden in Höhe des übersteigenden Betrags gekürzt.
Rentenbeträge, die auf freiwilliger Höherversicherung beruhen, bleiben unberücksichtigt.
Im Übrigen erfolgt keine Anrechnung anderer Leistungen auf die Renten des Versorgungswerks.

(8) Die gesetzlichen und satzungsmäßigen Ansprüche auf Leistungen und Beiträge verjähren in vier Jahren.
Die Verjährung beginnt mit dem Ablauf des Jahres, in dem der Anspruch fällig geworden ist.
Für die Hemmung, die Unterbrechung und die Wirkungen der Verjährung gelten die Vorschriften des Bürgerlichen Gesetzbuchs entsprechend.

(9) Als Witwen und Witwer im Sinne dieser Vorschrift gelten sowohl hinterbliebene Ehegattinnen oder Ehegatten als auch hinterbliebene eingetragene Lebenspartnerinnen oder Lebenspartner von verstorbenen Mitgliedern und ehemaligen Mitgliedern des Landtags.

#### § 16 Gesundheitsschäden

(1) Hat ein Mitglied des Landtags während seiner Zugehörigkeit zum Landtag oder, sofern es dem Landtag mindestens fünf Jahre angehört hat, innerhalb von drei Jahren nach seinem Ausscheiden ohne sein grobes Verschulden Gesundheitsschäden erlitten, die seine Arbeitsfähigkeit dauernd so wesentlich beeinträchtigen, dass es sein Mandat und bei seinem Ausscheiden aus dem Landtag die bei seiner Wahl zum Landtag ausgeübte oder eine andere zumutbare Beschäftigung oder Tätigkeit nicht ausüben kann, so erhält es eine Versorgung in Höhe von 20 Prozent der Entschädigung nach § 5 Absatz 1.
Ist der Gesundheitsschaden durch einen Unfall in Ausübung oder infolge des Mandats eingetreten, so erhöht sich der Bemessungssatz auf 30 Prozent.

(2) Auf die Versorgungsansprüche nach Absatz 1 werden angerechnet

1.  Rente aus dem Versorgungswerk, soweit sie auf Pflichtbeiträgen beruht,
2.  Altersversorgung nach dem Abgeordnetengesetz in der bis zum Inkrafttreten dieses Gesetzes geltenden Fassung,
3.  Versorgungsbezüge aus der Mitgliedschaft in anderen gesetzgebenden Körperschaften,
4.  Versorgungsbezüge aus einem Amtsverhältnis oder einer Verwendung im öffentlichen Dienst in Höhe von 50 Prozent des Betrags, um den die Ansprüche nach Absatz 1 und die Versorgungsbezüge aus der öffentlichen Kasse 90 Prozent der Entschädigung nach § 5 Absatz 1 übersteigen.

(3) Leistungen nach Absatz 1 werden nur auf Antrag gewährt.
Für zurückliegende Zeiten werden sie höchstens für drei Monate vor Antragstellung gewährt.

(4) Die Feststellung von Gesundheitsschäden erfolgt am Sitz des Landtags durch amtsärztliches Gutachten; sie wird bis zu dreimal im Abstand von jeweils drei Jahren amtsärztlich überprüft.
Die Feststellung wird ersetzt durch den Bescheid über Rente wegen Erwerbsminderung oder durch den Bescheid über Dienstunfähigkeit im Sinne des Beamtenrechts.
Der Anspruch auf Versorgung nach Absatz 1 erlischt, wenn die Voraussetzungen entfallen sind oder die versorgungsberechtigte Person erneut Mitglied einer gesetzgebenden Körperschaft wird.

(5) Auf die Versorgung nach Absatz 1 sind die beamtenrechtlichen Vorschriften des Landes über die Versorgung sinngemäß anzuwenden.

(6) Die Mitglieder des Landtags sind gegen Unfall zu versichern.

### Abschnitt 4  
Zuschuss zu den Kosten der Kranken- und Pflegeversicherung

#### § 17 Zuschuss zu den Kranken- und Pflegeversicherungsbeiträgen

(1) Die Mitglieder des Landtags und die Versorgungsberechtigten nach diesem Gesetz erhalten auf Antrag einen zweckgebundenen Zuschuss zu ihren Krankenversicherungsbeiträgen bezogen auf die sich aus diesem Gesetz ergebenden Leistungen gemäß § 5 Absatz 1 oder eine der in § 18 Nummer 1 genannten Leistungen der Versorgung.
Das Bestehen eines Versicherungsverhältnisses ist nachzuweisen.
Ein Zuschuss wird nicht gezahlt, soweit aufgrund gesetzlicher Vorschriften gegenüber anderen Stellen Anspruch auf entsprechende Leistungen besteht.
Leistungen in diesem Sinne sind Zahlungen von Dritten, die insbesondere aufgrund der Vorschriften des Fünften oder Sechsten Buches Sozialgesetzbuch und des Zweiten Gesetzes über die Krankenversicherung der Landwirte gewährt werden.
Besteht nach beamtenrechtlichen Vorschriften oder Grundsätzen Anspruch auf Beihilfe, besteht kein Anspruch nach dieser Vorschrift.

(2) Der Zuschuss wird jeweils in Höhe des halben Betrags geleistet, der sich aus der Anwendung des ermäßigten Beitragssatzes der gesetzlichen Krankenversicherung im Sinne von § 243 des Fünften Buches Sozialgesetzbuch zuzüglich der Hälfte des durchschnittlichen Zusatzbeitragssatzes nach § 242a des Fünften Buches Sozialgesetzbuch ergibt.
Besteht die Mitgliedschaft nicht oder nicht ausschließlich in einer gesetzlichen Krankenkasse gemäß § 4 des Fünften Buches Sozialgesetzbuch, wird unter Berücksichtigung der Höchstgrenze nach Satz 1 höchstens der Betrag gezahlt, der sich in entsprechender Anwendung des § 257 Absatz 2 Satz 1 und 2 des Fünften Buches Sozialgesetzbuch bei Zugrundelegung des nach § 243 des Fünften Buches Sozialgesetzbuch ermäßigten Beitragssatzes zuzüglich der Hälfte des durchschnittlichen Zusatzbeitragssatzes nach § 242a des Fünften Buches Sozialgesetzbuch ergibt.

(3) Absatz 1 gilt entsprechend für Pflegeversicherungsbeiträge der Mitglieder des Landtags unter Berücksichtigung der §§ 58 und 61 des Elften Buches Sozialgesetzbuch.
Zu berücksichtigende Leistungen im Sinne des Absatzes 1 Satz 3 sind insbesondere Leistungen Dritter aufgrund des Elften Buches Sozialgesetzbuch.
Der Zuschuss wird in Höhe der Hälfte des aus eigenen Mitteln geleisteten Pflegeversicherungsbeitrags gewährt, höchstens jedoch die Hälfte des Höchstbeitrages der sozialen Pflegeversicherung.

### Abschnitt 5  
Gemeinsame Vorschriften

#### § 18 Begriffsbestimmungen

Im Sinne dieses Gesetzes sind

1.  Versorgungsberechtigte ehemalige Mitglieder des Landtags, die Übergangsgeld (§ 14), Altersrente (§ 15 Absatz 3 Nummer 1), Versorgung wegen Gesundheitsschäden (§ 16) oder Altersversorgung nach dem bis zum Inkrafttreten dieses Gesetzes geltenden Recht beziehen, sowie diejenigen, die Hinterbliebenenrente (§ 15 Absatz 3 Nummer 2) oder Hinterbliebenenversorgung nach dem bis zum Inkrafttreten dieses Gesetzes geltenden Recht erhalten,
2.  Versorgung die in Nummer 1 aufgeführten Leistungen,
3.  gesetzgebende Körperschaften das Europäische Parlament, der Deutsche Bundestag, die Landesparlamente und die aus der Wahl vom 18.
    März 1990 hervorgegangene Volkskammer der DDR.

#### § 19 Beginn und Ende von Ansprüchen, Mitwirkungspflichten

(1) Die in diesem Gesetz geregelten Ansprüche entstehen mit dem Tag der Annahme der Wahl oder können ab diesem Zeitpunkt geltend gemacht werden, sofern in diesem Gesetz nichts anderes geregelt ist.
Die Leistungen werden für einen Monat nur einmal gewährt.

(2) Ein aus dem Landtag ausscheidendes Mitglied erhält die Entschädigung nach § 5 Absatz 1 und 2, die Amtsausstattung nach § 7, die Erstattung von Aufwendungen nach § 8 Absatz 1 Nummer 1 bis 3 sowie für Aufwendungen zu einer Zweitwohnung nach § 11 Absatz 5 Satz 2 und die Zuschüsse nach § 17 bis zum Ende des Monats, in dem seine Mitgliedschaft endet.

(3) Leistungen nach § 8 Absatz 1 Nummer 1 werden im Falle der vorzeitigen Beendigung der Mitgliedschaft vom Zeitpunkt des Ausscheidens aus dem Landtag an längstens für einen Zeitraum gewährt, dessen Dauer sich in Anlehnung an die jeweiligen Kündigungsfristen nach dem jeweils geltenden Tarifvertrag für den öffentlichen Dienst der Länder (TV-L) bestimmt.
Die Aufwendungen gemäß § 8 Absatz 1 Nummer 3 werden für einen Zeitraum von vier Wochen nach dem Ausscheiden erstattet, Erstattungen gemäß § 8 Absatz 1 Nummer 2 erfolgen bis zum vereinbarten Ende des Praktikumsvertrages.

(4) Scheidet eine Person, die eine Amtszulage im Sinne des § 5 Absatz 3 erhält, aus dem entsprechenden Amt aus, so endet die Zahlung der zusätzlichen Leistungen nach § 5 Absatz 3 mit dem Ablauf des Monats des Ausscheidens aus diesem Amt.

(5) Leistungen der Versorgung im Sinne von § 18 Nummer 1 werden nicht gezahlt, wenn das Mitglied des Landtags durch rechtskräftigen Richterspruch seine Wählbarkeit und das Recht, öffentliche Ämter zu bekleiden verloren hat oder sie ihm aberkannt wurden und es in infolge dessen sein Mandat verloren hat.
Stattdessen werden die an das Versorgungswerk geleisteten Beiträge erstattet.

(6) Bei Berechnung der Mandatsdauer gemäß § 14 Absatz 1 Satz 4 und § 16 wird ein verbleibender Rest von mehr als 182 Tagen als volles Jahr gezählt.

(7) Die Mitglieder des Landtags und die Versorgungsberechtigten sind verpflichtet, der Präsidentin oder dem Präsidenten unverzüglich die Tatsachen und die Änderungen mitzuteilen, deren Kenntnis für die Feststellung von Leistungen nach diesem Gesetz erforderlich ist.
Kommt das Mitglied oder die versorgungsberechtigte Person der Anzeigepflicht nicht nach, kann die Zahlung von Leistungen ausgesetzt werden.

#### § 20 Zahlungsvorschriften

(1) Die Entschädigungen nach § 5 und die Leistungen nach den §§ 14, 16 und 17 werden monatlich im Voraus gezahlt.
Ist nur ein Teil zu leisten, so wird für jeden Kalendertag ein Dreißigstel gezahlt.

(2) Leistungen und Forderungen nach diesem Gesetz können miteinander verrechnet werden.

(3) Leistungen nach diesem Gesetz können durch Verwaltungsakt festgesetzt, entsprechende Rückforderungsansprüche durch Verwaltungsakt geltend gemacht werden.

(4) Sofern in diesem Gesetz nichts anderes geregelt ist, verjähren Ansprüche nach diesem Gesetz und entsprechende Ansprüche auf Rückzahlung von zu viel gezahlten Leistungen in drei Jahren; Ansprüche auf Rückzahlung von zu viel gezahlten Leistungen verjähren in zehn Jahren, wenn durch vorsätzlich oder leichtfertig unrichtige und unvollständige Angaben oder das vorsätzliche oder leichtfertige pflichtwidrige Unterlassen von Angaben die Gewährung oder Belassung solcher Ansprüche bewirkt wurde.
Die Verjährung beginnt mit dem Schluss des Jahres, in dem der Anspruch entstanden ist.
Im Übrigen sind die §§ 194 bis 218 des Bürgerlichen Gesetzbuchs entsprechend anzuwenden.

(5) Wegen einer nicht nur geringfügigen Verletzung der Ordnung oder der Würde des Parlaments kann die Präsidentin oder der Präsident gegen ein Mitglied des Landtags, auch ohne dass ein Ordnungsruf ergangen ist, ein Ordnungsgeld in Höhe von 500 Euro festsetzen.
Im Wiederholungsfall erhöht sich das Ordnungsgeld auf 1 000 Euro.
Die Präsidentin oder der Präsident des Landtags macht ein Ordnungsgeld, nach Abschluss des in der Geschäftsordnung des Landtags bestimmten Verfahrens als Forderung gemäß Absatz 2 geltend.

#### § 21 Verzicht, Übertragbarkeit

Ein Verzicht auf die Entschädigungen nach § 5 sowie auf die Amtsausstattung nach § 7 ist unzulässig.
Die Ansprüche aus § 5 Absatz 2 und den §§ 7, 8, 10, 11 und 17 sind nicht übertragbar.
Der Anspruch auf Entschädigung nach § 5 Absatz 1 ist nur bis zur Hälfte übertragbar.
Anwartschaften und Ansprüche auf Leistungen aus dem Versorgungswerk sind mit Ausnahme der Ansprüche auf laufende Leistungen nicht übertragbar.
Im Übrigen gelten die Vorschriften der §§ 850 ff. der Zivilprozessordnung.

#### § 22 Bericht der Präsidentin oder des Präsidenten

Die Präsidentin oder der Präsident des Landtags legt jährlich im Rahmen der Haushaltsrechnung einen Bericht über alle Leistungen vor, die die Mitglieder und ehemaligen Mitglieder des Landtags sowie ihre Hinterbliebenen im vorherigen Haushaltsjahr erhalten haben.
Der Bericht wird als Landtagsdrucksache veröffentlicht.

### Abschnitt 6  
Angehörige des öffentlichen Dienstes

#### § 23 Unvereinbarkeit von Amt und Mandat

(1) Beamtinnen und Beamte mit Dienstbezügen, Berufsrichterinnen und Berufsrichter, Staatsanwältinnen und Staatsanwälte, Berufssoldatinnen und Berufssoldaten sowie Soldatinnen und Soldaten auf Zeit dürfen Mitglied des Landtags sein, wenn ihre Rechte und Pflichten aus ihrem Dienstverhältnis mit Ausnahme der Pflicht zur Amtsverschwiegenheit und des Verbots der Annahme von Belohnungen und Geschenken vom Tage der Annahme der Wahl ab ruhen.
Ruht das Dienstverhältnis nicht kraft Gesetzes mit der Annahme der Wahl, so verliert das gewählte Mitglied des Landtags sein Mandat abweichend von der Maßgabe des Satzes 1 erst dann, wenn es nicht innerhalb einer von der Präsidentin oder dem Präsidenten des Landtags zu bestimmenden Frist nachweist, dass das Dienstverhältnis ruht oder beendet ist oder es unter Wegfall der Dienstbezüge beurlaubt ist.
Wird ein Mitglied des Landtags zur Beamtin oder zum Beamten mit Dienstbezügen, zur Berufsrichterin oder zum Berufsrichter, zur Staatsanwältin oder zum Staatsanwalt, zur Berufssoldatin oder zum Berufssoldaten oder zur Soldatin oder zum Soldaten auf Zeit ernannt, darf es nur unter den Voraussetzungen der Sätze 1 und 2 Mitglied des Landtags bleiben.

(2) Absatz 1 gilt entsprechend für Angestellte juristischer Personen des öffentlichen Rechts mit Ausnahme der Religionsgemeinschaften und für leitende Angestellte von Kapitalgesellschaften, Vereinen, Verbänden oder Stiftungen, wenn juristische Personen des öffentlichen Rechts mehr als 50 Prozent des Kapitals halten oder des Stiftungsvermögens beigetragen haben oder mehr als die Hälfte der Mitglieder stellen.
Leitende Angestellte im Sinne des Satzes 1 sind die Mitglieder des zur Geschäftsführung befugten Organs und deren ständige Vertreterinnen und Vertreter.

(3) In den Fällen der Absätze 1 und 2 erfolgen die Zahlungen nach den §§ 5, 7, 8, 10, 11 und 17 erst, wenn die Rechte und Pflichten aus dem Beamten-, Richter- oder Angestelltenverhältnis ruhen oder eine vergleichbare Regelung getroffen oder wenn das Beamten-, Richter- oder Angestelltenverhältnis beendet wurde.

#### § 24 Wiederverwendung nach Beendigung des Mandats

(1) Soweit das Amt oder der Dienst von Angestellten des öffentlichen Dienstes mit dem Mandat unvereinbar ist, ruht das Arbeitsverhältnis mit der Annahme der Wahl für die Dauer der Mitgliedschaft im Landtag.

(2) Hat das Arbeitsverhältnis während der Mitgliedschaft im Landtag nicht geendet, so ruht es nach Beendigung der Mitgliedschaft für längstens drei Monate.
Die Angestellten sind auf ihren Antrag, der innerhalb dieser drei Monate zu stellen ist, wieder einzustellen.
Ihnen ist die bis zur Wahl zum Landtag ausgeübte oder eine andere zumutbare Aufgabe zu übertragen.
Die übertragene Aufgabe muss mit mindestens derselben Höchstgrundvergütung ausgestattet sein, wie die zuletzt ausgeübte Tätigkeit.

(3) Nach Beendigung der Mitgliedschaft im Landtag ist die Zeit der Mitgliedschaft auf Dienst- und Beschäftigungszeiten anzurechnen.

(4) Die Absätze 1 und 2 gelten für Richterinnen und Richter entsprechend.

### Abschnitt 7  
Verhaltenspflichten

#### § 25 Annahme von Leistungen

(1) Ein Mitglied des Landtags darf mit Rücksicht auf sein Mandat keine anderen als die in diesem Gesetz geregelten Leistungen annehmen.

(2) Leistungen sind Geldzahlungen und die Gewährung geldwerter Vorteile.

#### § 25a Zulässigkeit der Annahme von Leistungen

(1) Zulässig ist die Annahme einer Leistung,

1.  mit der eine Fraktion oder eine Gruppe besondere Dienste eines Mitglieds vergütet, Aufwendungen nach § 7 Absatz 2 des Fraktionsgesetzes erstattet oder die Wahrnehmung herausgehobener Funktionen vergütet,
2.  soweit der Wert der Leistung den Wert einer von dem Mitglied des Landtags tatsächlich erbrachten Gegenleistung nicht übersteigt; maßgeblich ist der verkehrsübliche Wert; ist ein solcher nicht festzustellen, ist maßgeblich, dass Leistung und Gegenleistung nicht offensichtlich außer Verhältnis stehen,
3.  die für die politische Tätigkeit als Mitglied des Landtags gewährt wird (Spende); § 25 Absatz 2 des Parteiengesetzes gilt entsprechend,
4.  die dem Mitglied des Landtags zur Teilnahme an
    1.  internationalen oder interparlamentarischen Begegnungen oder
    2.  Veranstaltungen zur politischen Information, zur Darstellung der Standpunkte des Landtags oder seiner Fraktion oder Gruppe oder als Repräsentant des Landtagsgewährt wird,
5.  die ohne Erwartung einer Gegenleistung erbracht wird (Geschenk) und parlamentarischen oder gesellschaftlichen Gepflogenheiten entspricht, wenn der Wert im Einzelfall 100 Euro nicht übersteigt.

(2) Die Annahme einer Leistung nach Absatz 1 ist unzulässig, wenn die Leistung nur gewährt wird, weil dafür die Vertretung und Durchsetzung der Interessen der leistenden Person oder Dritter im Landtag erwartet wird.

#### § 25b Abführung von Leistungen

(1) Ein Mitglied des Landtags, das eine unzulässige Leistung angenommen hat, muss diese Leistung oder, falls dies nicht möglich ist, den Wert der Leistung an das Land abführen.

(2) Geschenke, die nicht nach § 25a Absatz 1 Nummer 5 zulässig sind, sind zurückzuweisen oder der Präsidentin oder dem Präsidenten auszuhändigen.
Abweichend von Satz 1 kann das Mitglied des Landtags bei der Präsidentin oder dem Präsidenten beantragen, ein Geschenk, das parlamentarischen oder gesellschaftlichen Gepflogenheiten entspricht, zu behalten.
Die Präsidentin oder der Präsident stellt in diesem Fall den Wert fest.
Das Mitglied des Landtags hat den Wert unter Abzug des Betrags von 100 Euro an das Land zu entrichten.

(3) Die Präsidentin oder der Präsident macht den Anspruch durch Verwaltungsakt geltend.
Für den Anspruch gelten die Vorschriften über die regelmäßige Verjährung nach dem Bürgerlichen Gesetzbuch entsprechend.
Der Anspruch wird durch den Verlust der Mitgliedschaft im Landtag nicht berührt.

#### § 26 Anzeigepflichten

(1) Die Mitglieder des Landtags haben der Präsidentin oder dem Präsidenten des Landtags unverzüglich anzuzeigen:

1.  die gegenwärtig neben dem Abgeordnetenmandat ausgeübten Berufe, insbesondere
    1.  die unselbstständige Tätigkeit unter Angabe des Arbeitgebers (mit Branche), der eigenen Funktion oder Dienststellung,
    2.  bei Gewerbetreibenden die Art des Gewerbes und die Firma,
    3.  bei freiberuflicher oder sonstiger selbstständiger Tätigkeit die Angabe des Berufes,
    4.  bei mehreren ausgeübten Berufen den Schwerpunkt der beruflichen Tätigkeit,
2.  früher ausgeübte Berufe nach Maßgabe von Nummer 1, soweit sie in Erwartung der Mandatsübernahme oder in Zusammenhang mit ihr aufgegeben worden sind,
3.  jede entgeltliche Tätigkeit unter Angabe des Auftraggebers oder Vertragspartners, soweit diese Tätigkeit nicht im Rahmen des ausgeübten Berufes liegt, insbesondere eine entgeltliche Beratung, Vertretung fremder Interessen, Erstattung von Gutachten, Vortragstätigkeit oder publizistische Tätigkeit,
4.  vergütete oder ehrenamtliche Tätigkeiten als Mitglied eines Vorstandes, Aufsichtsrates, Verwaltungsrates, sonstigen Organs oder Beirats einer Gesellschaft, Genossenschaft, eines in einer anderen Rechtsform betriebenen Unternehmens oder einer Körperschaft, Stiftung und Anstalt des öffentlichen Rechts mit Ausnahme der Mandate der Gebietskörperschaften unter Angabe der betreffenden juristischen Person,
5.  vergütete oder ehrenamtliche Funktionen in Berufsverbänden, Wirtschaftsvereinigungen, sonstigen Interessenverbänden oder ähnlichen Organisationen mit Bedeutung auf Landes- oder Bundesebene unter Angabe der betreffenden Organisation sowie herausgehobene Funktionen in einer Fraktion oder Gruppe, soweit diese von der Fraktion oder Gruppe vergütet werden,
6.  alle Einnahmen aus den gegenwärtig ausgeübten Berufen, Tätigkeiten und Funktionen, die nach den Nummern 1 und 3 bis 5 anzeigepflichtig sind; maßgeblich sind die für die Tätigkeit geleisteten Bruttobeträge einschließlich Entschädigungs-, Ausgleichs- und Sachleistungen,
7.  Leistungen nach § 25a Absatz 1 Nummer 4, wenn sie im Einzelfall den Wert von 500 Euro übersteigen, und Spenden (§ 25a Absatz 1 Nummer 3).

(2) Die Angaben der Mitglieder des Landtags nach Absatz 1 werden von der Präsidentin oder dem Präsidenten veröffentlicht.
Die Angaben nach Absatz 1 Nummer 6 und 7 werden nach Stufen gestaffelt veröffentlicht, soweit die Einnahmen und Leistungen bezogen auf den jeweiligen Sachverhalt einen Betrag von 500 Euro übersteigen.
Die Stufe 1 erfasst einmalige oder regelmäßige monatliche Einkünfte einer Größenordnung von über 500 bis 3 500 Euro, die Stufe 2 Einkünfte bis 7 000 Euro, die Stufe 3 Einkünfte bis 15 000 Euro, die Stufe 4 Einkünfte bis 30 000 Euro, die Stufe 5 Einkünfte über 30 000 Euro.
Bei unregelmäßigen Einkünften aus einer angezeigten Tätigkeit ist ein Zwölftel der Jahressumme als monatlicher Durchschnittswert für die Einstufung maßgeblich; dies ist entsprechend zu kennzeichnen.

(3) Wirkt ein Mitglied des Landtags in einem Ausschuss an der Beratung oder Abstimmung über einen Gegenstand mit, an welchem es selbst oder eine andere Person, für die es gegen Entgelt tätig ist, ein unmittelbares wirtschaftliches Interesse hat, so hat es diese Interessenverknüpfung zuvor im Ausschuss offen zu legen, soweit sie sich nicht aus der Veröffentlichung ergibt.

(4) Hinweise auf die Mitgliedschaft im Landtag in beruflichen und geschäftlichen Angelegenheiten sind zu unterlassen.

#### § 26a Verstöße gegen die Verhaltensregeln

(1) Bestehen Anhaltspunkte, dass ein Mitglied des Landtags gegen die Vorschriften über die Annahme und Abführung von Leistungen (§§ 25, 25a und 25b) oder die Anzeigepflichten (§ 26) verstoßen hat, so klärt die Präsidentin oder der Präsident den Sachverhalt auf und hört das Mitglied des Landtags an.
Die Präsidentin oder der Präsident kann die oder den Vorsitzenden der Fraktion, der das Mitglied des Landtags angehört, um Stellungnahme bitten.

(2) Das Ergebnis der Überprüfung teilt die Präsidentin oder der Präsident dem Mitglied des Landtags schriftlich mit.
Liegt zur Überzeugung der Präsidentin oder des Präsidenten ein Pflichtverstoß im Sinne des Absatzes 1 Satz 1 vor, wirkt sie oder er auf eine Beachtung der Vorschriften hin.
Bei erheblichen oder wiederholten Pflichtverstößen kann die Präsidentin oder der Präsident dem Mitglied des Landtags eine Rüge erteilen.

(3) Gegen die Rüge kann das betroffene Mitglied des Landtags innerhalb von einem Monat nach Zugang der Rüge schriftlich Einspruch bei der Präsidentin oder dem Präsidenten einlegen.
Über den Einspruch entscheidet das Präsidium.

(4) Wird ein Einspruch nicht erhoben oder hat das Präsidium dem Einspruch nicht stattgegeben, informiert die Präsidentin oder der Präsident den Landtag über die Rüge und die Gründe in einer Sitzung des Landtags vor Eintritt in die Tagesordnung.
Eine Aussprache findet nicht statt.

#### § 27 Überprüfung von Abgeordneten

(1) Die Mitglieder des Landtags Brandenburg werden nach Annahme des Mandats auf eine geheimpolizeiliche, insbesondere auf eine hauptamtliche oder inoffizielle Tätigkeit für den Staatssicherheitsdienst der ehemaligen DDR im Sinne des Stasi-Unterlagen-Gesetzes überprüft.
Die Überprüfung wird längstens bis zum 31.
Dezember 2030 durchgeführt.
Sie erstreckt sich auch auf Personen, die gegenüber Mitarbeiterinnen und Mitarbeitern des Staatssicherheitsdienstes hinsichtlich deren Tätigkeit für den Staatssicherheitsdienst rechtlich oder faktisch weisungsbefugt waren, und auf inoffizielle Mitarbeiterinnen und Mitarbeiter des Arbeitsgebietes 1 der Kriminalpolizei der Volkspolizei.
Abgeordnete, die erst nach dem 12.
Januar 1990 das 18.
Lebensjahr vollendeten, werden nicht überprüft.
Scheidet ein Mitglied des Landtags vor Abschluss des Überprüfungsverfahrens aus dem Landtag aus, ist das Verfahren einzustellen.
Die hierzu im Überprüfungsverfahren angefallenen Unterlagen sind umgehend zu vernichten.

(2) Die Präsidentin oder der Präsident des Landtags ersucht das Bundesarchiv um die Übermittlung von Unterlagen zum Zweck der Überprüfung.
Die Abgeordneten teilen der Präsidentin oder dem Präsidenten des Landtags zu diesem Zweck alle Vor- und Familiennamen (Geburtsnamen und Namen aus früheren Ehen), ihre Personenkennzahl nach dem Recht der DDR und die Wohnanschriften (Haupt- und Nebenwohnungen) vor dem 3.
Oktober 1990 mit.
Enthält die Antwort des Bundesarchivs Anhaltspunkte, die auf eine Tätigkeit oder Verantwortung nach Absatz 1 Satz 1 oder 3 hinweisen, übermittelt die Präsidentin oder der Präsident dem Mitglied des Landtags alle Unterlagen unter Berücksichtigung des § 16 des Stasi-Unterlagen-Gesetzes.
Das Mitglied hat die Möglichkeit, in einer angemessenen Frist Stellung zu nehmen.
Die Präsidentin oder der Präsident kann zu einem späteren Zeitpunkt eine erneute Überprüfung einleiten, wenn neue Tatsachen oder Unterlagen beigebracht werden.

(3) Zu Beginn einer Wahlperiode wird beim Landtag eine Kommission eingerichtet, die aus vier Mitgliedern besteht, die weder dem Landtag noch der Landesregierung angehören und auf Vorschlag der Präsidentin oder des Präsidenten vom Landtag mit einer Mehrheit von zwei Dritteln seiner Mitglieder für die Dauer der Wahlperiode gewählt werden.
Den Vorschlag unterbreitet die Präsidentin oder der Präsident im Benehmen mit den Fraktionen.

(4) Im Falle von Absatz 2 Satz 3 übermittelt die Präsidentin oder der Präsident alle Unterlagen und, soweit vorhanden, die Stellungnahme des Mitglieds des Landtags an die Kommission.
Die Kommission trifft in Auswertung der Mitteilungen des Bundesarchivs und sonstiger ihr zugeleiteter oder von ihr beigezogener Unterlagen und Informationen Feststellungen, ob eine Tätigkeit oder Verantwortung nach Absatz 1 Satz 1 oder 3 als erwiesen anzusehen ist.
Sie kann ergänzende Unterlagen und Stellungnahmen des Bundesarchivs oder anderer Stellen anfordern und bei Bedarf um Akteneinsicht ersuchen.
Entscheidungen bedürfen einer Mehrheit der gesetzlichen Zahl der Mitglieder der Kommission.
Vor Abschluss der Feststellungen sind die Tatsachen dem betroffenen Mitglied des Landtags zu eröffnen und mit ihm zu erörtern.
Es kann Akteneinsicht verlangen und sich einer Vertrauensperson bedienen.
Die Feststellungen der Kommission werden unter Angabe der wesentlichen Gründe von dem den Vorsitz wahrnehmenden Kommissionsmitglied ausgefertigt.
Vor der Übergabe des Berichts über die Feststellungen an die Präsidentin oder den Präsidenten des Landtags gibt die Kommission dem betroffenen Mitglied des Landtags Gelegenheit, zu den seine Person betreffenden Feststellungen eine schriftliche Erklärung abzugeben; sie ist dem Bericht als Anlage beizufügen.
Der Bericht wird als Drucksache veröffentlicht; der Landtag befasst sich mit ihr in einer seiner Sitzungen.

(5) Die Kommission tagt nichtöffentlich.
Ihre Mitglieder sind vorbehaltlich des Absatzes 4 Satz 5 bis 9 zur Verschwiegenheit verpflichtet.
Das Nähere über die bei der Überprüfung der Abgeordneten einzuhaltenden Geheimhaltungspflichten wird in einer gesonderten Anlage zur Geschäftsordnung des Landtags geregelt.

(6) Bei Übermittlungen nach Absatz 2 Satz 3, Akteneinsicht nach Absatz 4 Satz 6 und Veröffentlichungen nach Absatz 4 Satz 9 sind berechtigte Interessen Betroffener und Dritter im Sinne des § 6 Absatz 3 und 7 des Stasi-Unterlagen-Gesetzes zu berücksichtigen.
Insbesondere die Rechte zum Schutz der Betroffenen sind während des gesamten Überprüfungsverfahrens zu beachten.

(7) Die angefallenen Unterlagen sind mit Ablauf der Wahlperiode dem Brandenburgischen Landeshauptarchiv zur Übernahme anzubieten, sofern gesetzlich nichts anderes bestimmt ist.

#### § 27a Verschwiegenheitspflicht und Aussagegenehmigung

Die Abgeordneten des Landtags dürfen, auch nach Beendigung ihres Mandats, ohne Genehmigung weder vor Gericht noch außergerichtlich Aussagen oder Erklärungen abgeben über Angelegenheiten, die aufgrund eines Gesetzes oder nach den Bestimmungen der Geschäftsordnung des Landtags der Verschwiegenheit unterliegen.
Die Genehmigung erteilt die Präsidentin oder der Präsident des Landtags.
Sind Stellen außerhalb des Landtags an der Entstehung der geheim zu haltenden Angelegenheiten beteiligt gewesen, kann die Genehmigung nur im Einvernehmen mit ihnen erteilt werden.
Die Genehmigung darf nur versagt werden, wenn die Aussage oder Erklärung dem Wohl des Landes Brandenburg, eines anderen Landes oder des Bundes Nachteile bereiten oder die Erfüllung öffentlicher Aufgaben ernstlich gefährden oder erheblich erschweren würde.

### Abschnitt 8  
Weitergeltung alten Rechts, Übergangsbestimmungen, ergänzende Vorschriften

#### § 28 Versorgungsansprüche, -anwartschaften und -abfindungen

(1) Die innerhalb der ersten fünf Wahlperioden erworbenen Versorgungsansprüche oder Versorgungsanwartschaften bleiben erhalten.
Die sich daraus ergebenden Ansprüche richten sich, vorbehaltlich der Absätze 2 bis 6, nach bisherigem Recht.
Gleiches gilt für noch nicht abgegoltene Ansprüche auf Versorgungsabfindung, Nachversicherung oder Anrechnung der Mandatszeit als Dienstzeit.

(2) Für ehemalige Mitglieder des Landtags mit Anspruch auf Altersversorgung, die vor dem 13. Oktober 2007 aus dem Landtag ausgeschieden sind, sind die Versorgungsregelungen nach §§ 11 und 12 und die Anrechnungsvorschriften nach § 21 des Abgeordnetengesetzes jeweils in der bis zum Ablauf des 30. Juni 2006 geltenden Fassung anzuwenden.
Eine vorzeitige Inanspruchnahme der Altersversorgung nach § 12 Absatz 3 des Abgeordnetengesetzes in der bis zum Inkrafttreten dieses Gesetzes geltenden Fassung bleibt unberührt.

(3) Mit Beginn der 6.
Wahlperiode wird bei der Berechnung der Alters- und Hinterbliebenenversorgung sowie der Versorgung wegen Gesundheitsschäden nach Absatz 1 Satz 1 anstelle der Entschädigung nach § 5 Absatz 1 des Abgeordnetengesetzes in der bis zum Inkrafttreten dieses Gesetzes geltenden Fassung ein fiktiver Bemessungssatz in Höhe von 63 Prozent der Entschädigung nach § 5 Absatz 1 dieses Gesetzes zugrunde gelegt.
Dieser fiktive Bemessungssatz gilt auch bei der anteiligen Berücksichtigung von Zeiten, in denen ein Amt mit Amtszulage im Sinne des § 5 Absatz 3 wahrgenommen wurde.

(4) Für das Jahr 2022 wird bei der Berechnung der Versorgung gemäß Absatz 3 Satz 1 und 2 102 Prozent des fiktiven Bemessungssatzes nach Absatz 2 Satz 1 auf der Grundlage des Jahres 2020 zugrunde gelegt.
Für die Anpassung einer Versorgung gemäß § 16 Absatz 1 gilt Satz 1 sinngemäß.

(5) Die Berechnung und Durchführung des Versorgungsausgleichs bestimmt sich nach den §§ 14 bis 16 und 39 bis 42 des Versorgungsausgleichsgesetzes.

(6) Bei Mitgliedern des Landtags, die vor Beginn der 6.
Wahlperiode aus dem Landtag ausgeschieden sind und Anspruch auf Übergangsgeld haben, bemessen sich Dauer und Höhe des Übergangsgeldes nach § 10 des Abgeordnetengesetzes in der bis zum Inkrafttreten dieses Gesetzes geltenden Fassung.
Hat ein Mitglied dem Landtag sowohl vor als auch nach dem Inkrafttreten dieses Gesetzes angehört, werden bei der Ermittlung der Bezugsdauer des Übergangsgeldes Mandatszeiten aus vorherigen Wahlperioden berücksichtigt.
Die Höhe des Übergangsgeldes und die maximale Bezugsdauer richten sich im Übrigen nach diesem Gesetz.

#### § 29 Abgeordnete mit Höchstversorgung

Ein Mitglied des Landtags, das bereits vor Inkrafttreten dieses Gesetzes Mitglied des Landtags war und die Höchstversorgung gemäß den §§ 11 und 12 des Abgeordnetengesetzes in der bis zum Inkrafttreten dieses Gesetzes geltenden Fassung erreicht hat, erhält keine Entschädigung nach § 5 Absatz 2.
Eine Mitgliedschaft im Versorgungswerk ist ausgeschlossen.
Einem Mitglied des Landtags nach Satz 1, das eine Amtszulage nach § 5 Absatz 3 erhält, wird eine Entschädigung in Höhe des Prozentsatzes dieser Amtszulage bezogen auf den Betrag der Entschädigung nach § 5 Absatz 2 gewährt; die Regelungen über die Abführung an das Versorgungswerk und die Mitgliedschaft im Versorgungswerk finden Anwendung.

#### § 30 Zuschuss zu den Krankenversicherungsbeiträgen der Versorgungsberechtigten

Versorgungsberechtigten, denen nach dem bisherigen Recht zum Zeitpunkt des Inkrafttretens dieses Gesetzes ein Anspruch auf Zuschuss zu ihren Krankenversicherungsbeiträgen zustünde, der den nach § 17 Absatz 2 zu berechnenden Zuschuss übersteigt, wird der überschießende Betrag während einer Übergangszeit von einem Jahr nach Inkrafttreten dieses Gesetzes in der bisherigen Höhe und während eines weiteren Jahres zur Hälfte weitergewährt, sofern die für den Anspruch in der bisherigen Höhe notwendigen Voraussetzungen unverändert fortbestehen.

#### § 31 Beginn der Ansprüche der in den sechsten Landtag Gewählten

Wer in den sechsten Landtag gewählt wird, hat ab dem Tag der Annahme der Wahl Anspruch auf die Leistungen nach diesem Gesetz, auch wenn die 6.
Wahlperiode noch nicht begonnen hat.
Satz 1 gilt nicht für die Entschädigung nach § 5 Absatz 2.
Die Regelungen des Abgeordnetengesetzes in der bis zum Inkrafttreten dieses Gesetzes geltenden Fassung finden auf diesen Personenkreis keine Anwendung.

#### § 32 Berufliche Qualifizierung und Evaluation

(1) § 12 findet erstmalig auf Mitglieder des Landtags der 6.
Wahlperiode Anwendung.

(2) Während der 7.
Wahlperiode wird die Vorschrift des § 12 unter Berücksichtigung der Berichte der Präsidentin oder des Präsidenten des Landtags gemäß § 22 überprüft.
Bei der Evaluation sind Art und Umfang der durchgeführten Qualifizierungsmaßnahmen, ihre Auswirkungen und Ergebnisse sowie die Kosten zu berücksichtigen.

#### § 33 Evaluation der Altersrente

Spätestens im Jahr 2020 wird der Landtag Brandenburg auf der Grundlage eines Berichts der Präsidentin oder des Präsidenten die Angemessenheit der Altersrente und der Pflichtbeiträge nach § 5 Absatz 2 überprüfen.

#### § 34 Datenverarbeitung

Soweit es für die Erfüllung der Aufgaben nach diesem Gesetz erforderlich ist, darf die Präsidentin oder der Präsident des Landtags personenbezogene Daten von Mitgliedern und ehemaligen Mitgliedern des Landtags, von ihren Hinterbliebenen sowie von den Beschäftigten sowie von Praktikantinnen und Praktikanten der Mitglieder des Landtags verarbeiten.
Die Präsidentin oder der Präsident kann hierzu Ausführungsbestimmungen erlassen.
Sie oder er kann die bezeichneten Daten gemäß den Artikeln 28 und 29 der Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates vom 27. April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr und zur Aufhebung der Richtlinie 95/46/EG (Datenschutz-Grundverordnung) (ABl.
L 119 vom 4.5.2016, S. 1; L 314 vom 22.11.2016, S. 72; L 127 vom 23.5.2018, S. 2) im Auftrag zur Erfüllung der Aufgaben nach diesem Gesetz verarbeiten lassen.