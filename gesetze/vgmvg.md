## Gesetz zur Einführung der Verbandsgemeinde und der Mitverwaltung (Verbandsgemeinde- und Mitverwaltungsgesetz - VgMvG)

**Inhaltsübersicht**

### Abschnitt 1  
Verwaltungsmodelle auf der gemeindlichen Ebene

[§ 1 Träger von hauptamtlichen Verwaltungen auf der Gemeindeebene](#1)

### Abschnitt 2  
Verbandsgemeinde

[§ 2 Stellung und Struktur der Verbandsgemeinde](#2)  
[§ 3 Bildung, Änderung und Auflösung von Verbandsgemeinden](#3)  
[§ 4 Aufgaben der Verbandsgemeinde](#4)  
[§ 5 Personalüberleitung](#5)  
[§ 6 Verbandsgemeindevertretung](#6)  
[§ 7 Erstmalige Wahl der Verbandsgemeindevertretung](#7)  
[§ 8 Widerspruchsrecht](#8)  
[§ 9 Verbandsgemeindebürgermeisterin oder Verbandsgemeindebürgermeister](#9)  
[§ 10 Erstmalige Wahl der Verbandsgemeindebürgermeisterin oder des Verbandsgemeindebürgermeisters](#10)  
[§ 11 Wahlbehörde](#11)  
[§ 12 Wahlleiterin oder Wahlleiter](#12)  
[§ 13 Haushaltswirtschaft](#13)  
[§ 14 Finanzen](#14)  
[§ 15 Anwendung von Rechtsvorschriften](#15)

### Abschnitt 3  
Mitverwaltung

[§ 16 Stellung und Struktur der Mitverwaltung](#16)  
[§ 17 Bildung, Änderung und Auflösung der Mitverwaltung](#17)  
[§ 18 Mitverwaltungsvereinbarung](#18)  
[§ 19 Aufgabenverteilung in der Mitverwaltung](#19)  
[§ 20 Personalüberleitung](#20)  
[§ 21 Mitverwaltungsausschuss](#21)  
[§ 22 Zuständigkeiten des Mitverwaltungsausschusses](#22)  
[§ 23 Widerspruchsrecht gegen Entscheidungen des Mitverwaltungsausschusses](#23)  
[§ 24 Haushalts-, Kassen- und Rechnungswesen sowie Kostenersatz](#24)  
[§ 25 Anwendung von Rechtsvorschriften](#25)

### Abschnitt 4  
Beobachtungspflicht

[§ 26 Beobachtungspflicht](#26)

### Abschnitt 1  
Verwaltungsmodelle auf der gemeindlichen Ebene

#### § 1 Träger von hauptamtlichen Verwaltungen auf der Gemeindeebene

Träger einer hauptamtlichen Verwaltung auf der gemeindlichen Ebene können

1.  amtsfreie Gemeinden (Teil 1 der Kommunalverfassung des Landes Brandenburg),
2.  Ämter (Teil 3 der Kommunalverfassung des Landes Brandenburg),
3.  Verbandsgemeinden (Abschnitt 2) und
4.  mitverwaltende Gemeinden (Abschnitt 3)

sein.

### Abschnitt 2  
Verbandsgemeinde

#### § 2 Stellung und Struktur der Verbandsgemeinde

(1) Die Verbandsgemeinde ist ein gebietskörperschaftlicher Gemeindeverband, der aus aneinandergrenzenden Gemeinden desselben Landkreises besteht.
Sie erfüllt neben den verbandsgemeindeangehörigen Gemeinden (Ortsgemeinden) öffentliche Aufgaben der örtlichen Gemeinschaft im Rahmen der folgenden Vorschriften.
Sie verwaltet ihre Angelegenheiten selbst in eigener Verantwortung.

(2) Die Verbandsgemeinde besteht aus mindestens zwei Ortsgemeinden.

#### § 3 Bildung, Änderung und Auflösung von Verbandsgemeinden

(1) Gemeinden eines Landkreises, die unmittelbar aneinandergrenzen, können nach Beratung durch die untere Kommunalaufsichtsbehörde eine Verbandsgemeinde bilden, ändern oder auflösen.
Die Einzelheiten der Bildung oder Änderung, insbesondere der Name und der Sitz der Verwaltung, oder der Auflösung der Verbandsgemeinde sind in einer öffentlich-rechtlichen Vereinbarung zwischen den Gemeinden in entsprechender Anwendung des § 7 der Kommunalverfassung des Landes Brandenburg vom 18.
Dezember 2007 (GVBl.
I S. 286), die zuletzt durch das Gesetz vom 29.
Juni 2018 (GVBl.
I Nr. 15, 19) geändert worden ist, zu regeln.
In der öffentlich-rechtlichen Vereinbarung kann geregelt werden, dass die Verbandsgemeinde einen zweisprachigen Namen in deutscher und niedersorbischer Sprache trägt, wenn einzelne Gemeinden nach Satz 1 zum angestammten Siedlungsgebiet der Sorben/Wenden gehören.
Gehören alle Gemeinden nach Satz 1 zum angestammten Siedlungsgebiet der Sorben/Wenden, gilt § 9 Absatz 4 der Kommunalverfassung des Landes Brandenburg entsprechend.
Die Vereinbarung zur Bildung, Änderung oder Auflösung der Verbandsgemeinde muss in den Gemeindevertretungen beschlossen werden.
Sie bedarf der Genehmigung durch das für Inneres zuständige Ministerium.
Die Vereinbarung ist durch das für Inneres zuständige Ministerium im Amtsblatt für Brandenburg öffentlich bekannt zu machen.
Die Vereinbarung tritt, wenn kein späterer Zeitpunkt bestimmt ist, am Tag nach der öffentlichen Bekanntmachung in Kraft.
Die beteiligten Gemeinden haben in ihrem amtlichen Verkündungsblatt auf die erfolgte öffentliche Bekanntmachung hinzuweisen.

(2) In der öffentlich-rechtlichen Vereinbarung nach Absatz 1 sind insbesondere Regelungen über die vorläufige Vertretung der Bevölkerung der neugebildeten Verbandsgemeinde durch Mitglieder der Gemeindevertretungen der Gemeinden, die die Verbandsgemeinde bilden, in der vorläufigen Verbandsgemeindevertretung bis zur Neuwahl zu treffen; § 7 Absatz 2 Satz 2 bis 5 und Absatz 4 der Kommunalverfassung des Landes Brandenburg gilt entsprechend.
Darüber hinaus ist eine Auflistung des Vermögens und der Schulden, die den Aufgaben gemäß § 4 Absatz 1 bis 4 zuzuordnen sind und im Zuge der Rechtsnachfolge gemäß § 4 Absatz 5 Satz 1 übergehen, beizufügen oder in der öffentlich-rechtlichen Vereinbarung eine Regelung zum Übergang von Vermögen und Schulden zu treffen.

(3) Bei der Bildung einer Verbandsgemeinde unter Übertritt einer Hauptverwaltungsbeamtin oder eines Hauptverwaltungsbeamten nimmt diese oder dieser bis zum Beginn der Amtszeit der Verbandsgemeindebürgermeisterin oder des Verbandsgemeindebürgermeisters das Amt der Verbandsgemeindebürgermeisterin oder des Verbandsgemeindebürgermeisters der gebildeten Verbandsgemeinde wahr.
Führt die Bildung einer Verbandsgemeinde zum Übertritt mehrerer Hauptverwaltungsbeamtinnen oder Hauptverwaltungsbeamter, ist in der öffentlich-rechtlichen Vereinbarung festzulegen, welche der bisherigen Hauptverwaltungsbeamtinnen oder welcher Hauptverwaltungsbeamte das Amt nach Satz 1 wahrnimmt.

(4) Eine bislang amtsfreie Gemeinde wird mit der Bildung einer Verbandsgemeinde in eine Ortsgemeinde umgewandelt.
Die Gemeindevertretung einer Ortsgemeinde nach Satz 1 wählt die neue ehrenamtliche Bürgermeisterin oder den neuen ehrenamtlichen Bürgermeister für den Rest der laufenden Wahlperiode.
Die Amtszeit der oder des Neugewählten beginnt mit der Annahme der Wahl.
Mit Beginn der Amtszeit der neu gewählten ehrenamtlichen Bürgermeisterin oder des neu gewählten ehrenamtlichen Bürgermeisters, die oder der in entsprechender Anwendung des § 33 Absatz 1 und des § 51 Absatz 2 Satz 2 Nummer 2 der Kommunalverfassung des Landes Brandenburg kraft ihres oder seines Amtes den Vorsitz in der Gemeindevertretung führt, verliert die oder der bisherige Vorsitzende der Gemeindevertretung dieses Amt.
In der ersten Sitzung der Gemeindevertretung nach diesem Wechsel im Vorsitz der Gemeindevertretung sind die stellvertretenden Vorsitzenden der Gemeindevertretung neu zu wählen.

(5) Im Fall von genehmigten Gemeindezusammenschlüssen, die zur Änderung einer Verbandsgemeinde oder mehrerer Verbandsgemeinden führen und für die aufgrund § 15 Absatz 1 Satz 1 dieses Gesetzes § 6 Absatz 3 Satz 4 der Kommunalverfassung des Landes Brandenburg entsprechend Anwendung findet, passt die jeweilige Verbandsgemeindebürgermeisterin oder der jeweilige Verbandsgemeindebürgermeister die öffentlich-rechtliche Vereinbarung an und macht sie im Amtsblatt für Brandenburg öffentlich bekannt.

(6) Bei einem Zusammenschluss mehrerer Ortsgemeinden zu einer neuen amtsfreien Gemeinde unter Auflösung der Verbandsgemeinde nimmt die Verbandsgemeindebürgermeisterin oder der Verbandsgemeindebürgermeister bis zum Beginn der Amtszeit der hauptamtlichen Bürgermeisterin oder des hauptamtlichen Bürgermeisters der neuen amtsfreien Gemeinde das Amt der hauptamtlichen Bürgermeisterin oder des hauptamtlichen Bürgermeisters der neugebildeten Gemeinde wahr.
Soweit ein Zusammenschluss nach Satz 1 zur Auflösung mehrerer Verbandsgemeinden führt, ist in dem Gebietsänderungsvertrag nach § 6 Absatz 3 Satz 1 der Kommunalverfassung des Landes Brandenburg festzulegen, welche Verbandsgemeindebürgermeisterin oder welcher Verbandsgemeindebürgermeister das Amt der hauptamtlichen Bürgermeisterin oder des hauptamtlichen Bürgermeisters im Sinne von Satz 1 vorübergehend wahrnimmt.

(7) Die öffentlich-rechtliche Vereinbarung nach Absatz 1 bedarf der Genehmigung durch das für Inneres zuständige Ministerium.
Dieses kann die Genehmigung versagen, wenn die Vereinbarung den Maßstäben dieses Gesetzes oder dem öffentlichen Wohl widerspricht.
Das für Inneres zuständige Ministerium kann die Genehmigung der Bildung, Änderung oder Auflösung einer Verbandsgemeinde insbesondere versagen, wenn dadurch für einzelne angrenzende Gemeinden, die nicht in die Bildung oder Änderung der Verbandsgemeinde mit eingebunden werden sollen, der Fortbestand oder die Bildung einer eigenen leistungsfähigen Verwaltung gefährdet ist.
Gleiches gilt, wenn durch die Bildung, Änderung oder Auflösung einer Verbandsgemeinde die Verwaltungskraft eines danach nur noch teilweise fortbestehenden Amtes oder einer nur noch teilweise fortbestehenden Verbandsgemeinde gefährdet würde.
Die Genehmigung soll versagt werden, wenn die Auflistung des Vermögens und der Schulden gemäß Absatz 2 Satz 2 nicht beigefügt ist oder in der öffentlich-rechtlichen Vereinbarung nach § 3 Absatz 1 keine Regelung zum Übergang von Vermögen und Schulden getroffen wurde.

(8) Das für Inneres zuständige Ministerium kann die Änderung oder Auflösung von Verbandsgemeinden aus Gründen des Gemeinwohls nach den Maßstäben dieses Gesetzes anordnen.

#### § 4 Aufgaben der Verbandsgemeinde

(1) Die Verbandsgemeinde ist in ihrem Gebiet Trägerin der durch Gesetz oder Verordnung dem Amt übertragenen Aufgaben, soweit in diesem oder anderen Gesetzen oder aufgrund eines Gesetzes nicht abweichend geregelt.

(2) Die Verbandsgemeinde ist anstelle der Ortsgemeinden Trägerin der folgenden Aufgaben:

1.  die nach dem Brandenburgischen Schulgesetz in der Fassung der Bekanntmachung vom 2. August 2002 (GVBl. I S. 78), das zuletzt durch Artikel 30 des Gesetzes vom 8. Mai 2018 (GVBl.
    I Nr. 8 S. 22) geändert worden ist, den Gemeinden oder Gemeindeverbänden mit Ausnahme der den Landkreisen übertragenen Aufgaben,
2.  Bau und Unterhaltung von zentralen Sport-, Spiel- und Freizeitanlagen, die mehreren Ortsgemeinden dienen,
3.  Bau und Unterhaltung überörtlicher Sozialeinrichtungen, soweit nicht freie gemeinnützige Träger solche errichten,
4.  die Flächennutzungsplanung gemäß § 203 Absatz 2 des Baugesetzbuches in der Fassung der Bekanntmachung vom 3.
    November 2017 (BGBl.
    I S. 3634) mit der Maßgabe, dass die endgültige Entscheidung der Verbandsgemeinde über die Aufstellung, Änderung, Ergänzung oder Aufhebung des Flächennutzungsplans der Zustimmung der Ortsgemeinden bedarf; die Zustimmung gilt als erteilt, wenn mehr als die Hälfte der Ortsgemeinden zugestimmt hat und in diesen mehr als zwei Drittel der Einwohnerinnen und Einwohner der Verbandsgemeinde wohnen; sofern Änderungen oder Ergänzungen des Flächennutzungsplans die Grundsätze der Gesamtplanung nicht berühren, bedürfen sie nur der Zustimmung derjenigen Ortsgemeinden, die selbst oder als Nachbargemeinden von den Änderungen oder Ergänzungen berührt werden; kommt die Zustimmung nicht zustande, so entscheidet die Verbandsgemeindevertretung mit einer Mehrheit von zwei Dritteln der gesetzlichen Zahl ihrer Mitglieder,
5.  die nach dem Kindertagesstättengesetz in der Fassung der Bekanntmachung vom 27. Juni 2004 (GVBl.
    I S. 384), das zuletzt durch Artikel 1 des Gesetzes vom 18. Juni 2018 (GVBl.
    I Nr. 11) geändert worden ist, den Gemeinden übertragenen Aufgaben, einschließlich des Rechts nach § 12 Absatz 1 Satz 2 des Kindertagesstättengesetzes,
6.  die Aufgaben nach dem Schiedsstellengesetz in der Fassung der Bekanntmachung vom 21. November 2000 (GVBl.
    I S. 158; 2001 I S. 38), das zuletzt durch Artikel 2 des Gesetzes vom 8.
    März 2018 (GVBl.
    I Nr. 4 S. 3) geändert worden ist,
7.  die gesetzliche Verpflichtung zur Hilfe in Verwaltungsangelegenheiten gemäß § 17 der Kommunalverfassung des Landes Brandenburg.

Die Verbandsgemeinde kann zudem anstelle der Ortsgemeinden die Aufgaben der Tourismusförderung und der Wirtschaftsförderung erfüllen, soweit sie von überörtlicher Bedeutung sind.

(3) Die Verbandsgemeinde erfüllt eine einzelne Selbstverwaltungsaufgabe der Ortsgemeinden nur dann an deren Stelle, wenn die Gemeindevertretungen mehrerer Ortsgemeinden die Aufgabe auf die Verbandsgemeinde übertragen und die Verbandsgemeindevertretung zustimmt.
Der Antrag der Ortsgemeinde und die Zustimmung der Verbandsgemeindevertretung bedürfen jeweils der Zustimmung von zwei Dritteln der gesetzlichen Zahl der Mitglieder der Ortsgemeindevertretung und der Verbandsgemeindevertretung.
Die Übertragung wird wirksam, nachdem die Verbandsgemeinde die beabsichtigte Übertragung der unteren Kommunalaufsichtsbehörde angezeigt hat und diese nicht innerhalb eines Zeitraums von vier Wochen nach Zugang der Anzeige der Übertragung widersprochen hat.
Eine Rückübertragung einer einzelnen Aufgabe kann verlangt werden, wenn die Gemeindevertretungen aller Ortsgemeinden, die die Aufgabe übertragen haben, dies beschließen und sich die Verhältnisse, die der Übertragung zugrunde lagen, so wesentlich geändert haben, dass den Ortsgemeinden ein Festhalten an der Übertragung nicht weiter zugemutet werden kann.
Soweit erforderlich, erfolgt in diesen Fällen eine Auseinandersetzung in entsprechender Anwendung der für Gebietsänderungen der Gemeinden geltenden Vorschriften.
Die Rückübertragung bedarf der Zustimmung der unteren Kommunalaufsichtsbehörde, wenn die Verbandsgemeinde mit der Rückübertragung nicht einverstanden ist.

(4) Soweit amtsangehörige Gemeinden weitere Aufgaben nach § 135 Absatz 5 der Kommunalverfassung des Landes Brandenburg auf das Amt übertragen haben, kann abweichend von Absatz 3 Satz 1 bis 3 in der öffentlich-rechtlichen Vereinbarung nach § 3 Absatz 1 zur Bildung der Verbandsgemeinde vorgesehen werden, dass die Verbandsgemeinde Trägerin dieser Aufgaben für die betroffenen Ortsgemeinden wird.

(5) Für die in den Absätzen 1 bis 4 genannten Aufgaben ist die Verbandsgemeinde Rechtsnachfolgerin des Amtes oder der an der Verbandsgemeinde beteiligten Gemeinden, soweit gesetzlich oder in der öffentlich-rechtlichen Vereinbarung nach § 3 Absatz 1 nichts anderes bestimmt ist; § 107 des Brandenburgischen Schulgesetzes bleibt unberührt.
Die Verbandsgemeinde tritt als Rechtsnachfolgerin in die Verträge ein, die die an der Verbandsgemeinde beteiligten Gemeinden vor der Verbandsgemeindebildung in ihrer Funktion als Trägerinnen der Aufgaben nach den Absätzen 1, 2 und 3 abgeschlossen haben oder die ein Amt vor der Verbandsgemeindebildung als Trägerin der Aufgaben nach den Absätzen 1 und 4 abgeschlossen hat, soweit gesetzlich oder in der öffentlich-rechtlichen Vereinbarung nach § 3 Absatz 1 nichts anderes bestimmt ist.

(6) Die Verbandsgemeinde verwaltet und unterstützt die Ortsgemeinden.
Sie berät die Ortsgemeinden bei der Wahrnehmung ihrer gesetzlichen Aufgaben und wirkt auf deren Erfüllung hin.
Zwischen Verbandsgemeinde und Ortsgemeinden gilt das Gebot gegenseitiger Rücksichtnahme und vertrauensvoller Zusammenarbeit.

(7) Soweit eine bislang große kreisangehörige Stadt mit der Bildung einer Verbandsgemeinde nach § 3 Absatz 4 Ortsgemeinde wird, führt die Verbandsgemeinde die durch Gesetz, aufgrund eines Gesetzes oder auf Antrag der großen kreisangehörigen Stadt übertragenen Aufgaben für die betroffene Ortsgemeinde entsprechend deren Beschlussfassung in deren Namen durch; die Aufgabenträgerschaft der betroffenen Ortsgemeinde für diese Aufgaben bleibt unberührt.
Die Vorbereitung der Beschlüsse der Gemeindevertretung der betroffenen Ortsgemeinde erfolgt in diesen Fällen durch die Verbandsgemeindebürgermeisterin oder den Verbandsgemeindebürgermeister im Benehmen mit der jeweiligen ehrenamtlichen Bürgermeisterin oder dem jeweiligen ehrenamtlichen Bürgermeister.

(8) Die Verbandsgemeinde haftet für Schäden, die Ortsgemeinden dadurch entstehen, dass Bedienstete oder Organwalter der Verbandsgemeinde bei der Wahrnehmung von Aufgaben für die Ortsgemeinden schuldhaft die ihnen obliegenden Pflichten verletzen.
§ 25 Absatz 3 Satz 1 der Kommunalverfassung des Landes Brandenburg gilt entsprechend.

(9) Verbandsgemeinden, die ganz oder teilweise zum angestammten Siedlungsgebiet der Sorben/Wenden nach dem Sorben/Wenden-Gesetz vom 7.
Juli 1994 (GVBl.
I S. 294), das zuletzt durch Artikel 1 des Gesetzes vom 11.
Februar 2014 (GVBl.
I Nr. 7) geändert worden ist, gehören, wenden die in den jeweiligen Ortsgemeinden getroffene Regelungen zur Wahrung der Interessen der Sorben/Wenden nach § 6 des Sorben/Wenden-Gesetzes und § 2 Absatz 2 Satz 3 der Kommunalverfassung des Landes Brandenburg sinngemäß an.

#### § 5 Personalüberleitung

(1) Beamtinnen und Beamte der Gemeinden, die eine Verbandsgemeinde bilden, treten gemäß § 31 Absatz 1 des Landesbeamtengesetzes vom 3. April 2009 (GVBl. I S. 26), das zuletzt durch das Gesetz vom 29. Juni 2018 (GVBl. I Nr. 17) geändert worden ist, in Verbindung mit § 16 Absatz 1 und 4 erste Alternative des Beamtenstatusgesetzes vom 17. Juni 2008 (BGBl. I S. 1010), das zuletzt durch Artikel 2 des Gesetzes vom 8. Juni 2017 (BGBl. I S. 1570) geändert worden ist, in den Dienst der Verbandsgemeinde über.
Die hauptamtlichen Bürgermeisterinnen und Bürgermeister und die Beigeordneten der bisherigen Gemeinden können zu Beigeordneten der neu gebildeten Verbandsgemeinde bestellt werden.
§ 59 Absatz 1 und 2 der Kommunalverfassung des Landes Brandenburg findet bis zum Ablauf der Amtszeit übergetretener Beamtinnen und Beamter auf Zeit keine Anwendung.

(2) Im Übrigen richtet sich die Rechtsstellung der übergetretenen Beamtinnen und Beamten nach § 18 des Beamtenstatusgesetzes, die der Versorgungsempfängerinnen und Versorgungsempfänger nach § 19 des Beamtenstatusgesetzes, sofern in diesem Gesetz nichts anderes bestimmt ist.
§ 50 des Brandenburgischen Besoldungsgesetzes vom 20. November 2013 (GVBl. I Nr. 32 S. 2, Nr. 34), das zuletzt durch Artikel 2 des Gesetzes vom 8.
Mai 2018 (GVBl.
I Nr. 7 S. 17) geändert worden ist, findet Anwendung.
Beamtinnen und Beamte auf Zeit, die bei Anwendung des § 18 Absatz 2 des Beamtenstatusgesetzes die Voraussetzungen des § 12 Absatz 1 Satz 1 Nummer 1 des Brandenburgischen Beamtenversorgungsgesetzes vom 20. November 2013 (GVBl. I Nr. 32 S. 77), das zuletzt durch Artikel 4 des Gesetzes vom 29. Juni 2018 (GVBl. I Nr. 14 S. 29) geändert worden ist, nicht erfüllen und deshalb aufgrund der Umbildung nicht in den einstweiligen Ruhestand versetzt werden können, gelten als abgewählt und erhalten bis zum Ablauf ihrer Amtszeit Besoldung und Versorgung nach den für abgewählte Wahlbeamtinnen und Wahlbeamte auf Zeit geltenden Vorschriften.
Soweit die Bildung der Verbandsgemeinde einen Wechsel des Dienstortes zur Folge hat, gilt der Übertritt in den Dienst der Verbandsgemeinde als Versetzung im Sinne der umzugskostenrechtlichen und trennungsgeldrechtlichen Vorschriften.

(3) Die Arbeitsverhältnisse der Arbeitnehmerinnen und Arbeitnehmer der Gemeinden, die eine Verbandsgemeinde bilden, gehen kraft Gesetzes auf die neu gebildete Verbandsgemeinde über.
Die Verbandsgemeinde tritt in die Rechte und Pflichten aus den im Zeitpunkt der Personalüberleitung bestehenden Arbeitsverhältnissen ein; diese werden mit der Verbandsgemeinde fortgesetzt.
Die bis zum Tag vor dem Übergang der Arbeitsverhältnisse erworbene Rechtsstellung der Arbeitnehmerinnen und Arbeitnehmer, insbesondere im Hinblick auf erreichte tarifrechtlich maßgebliche Zeiten, bleibt gewahrt.
Den betroffenen Arbeitnehmerinnen und Arbeitnehmern ist der gesetzliche Übergang der Arbeitsverhältnisse, der Eintritt der Verbandsgemeinde in die Rechte und Pflichten aus den bestehenden Arbeitsverhältnissen, deren Fortsetzung mit dem neuen Arbeitgeber und die Wahrung ihrer erworbenen Rechtsstellung schriftlich zu bestätigen.
Absatz 2 Satz 4 gilt entsprechend.

(4) Betriebsbedingte Kündigungen aus Gründen, die im Zusammenhang mit der Bildung der Verbandgemeinde stehen, sind für einen in der öffentlich-rechtlichen Vereinbarung nach § 3 Absatz 1 zu bestimmenden Zeitraum ab dem Übergang des jeweiligen Arbeitsverhältnisses ausgeschlossen.
Dies gilt nicht für Änderungskündigungen, die wegen eines Wechsels des Arbeitsortes erforderlich werden.
Das Recht zur Kündigung aus anderen Gründen bleibt unberührt.

(5) Absatz 3 Satz 1, 2 und 5 sowie die Absätze 4 und 6 gelten für Ausbildungsverhältnisse der Auszubildenden entsprechend.
Die Verbandsgemeinde stellt sicher, dass Beamtinnen und Beamte auf Probe nach Bildung der Verbandsgemeinde weiter verwendet werden können und Beamtinnen und Beamte auf Widerruf die Gelegenheit erhalten, ihren Vorbereitungsdienst zu beenden und die Prüfung abzulegen.

(6) Die personalverwaltenden Stellen der an der Bildung einer Verbandsgemeinde beteiligten Gemeinden können zur Vorbereitung der künftigen Personalstruktur der Verbandsgemeinde ohne Einwilligung der Beschäftigten folgende personenbezogene Daten aus den Personalakten übermitteln, soweit dies zur Vorbereitung der Überleitung erforderlich ist:

1.  Name, Vorname, Geburtsdatum, Geschlecht,
2.  Familienstand, Anzahl der Kinder unter 18 Jahren im Haushalt, tatsächliche Betreuung oder Pflege mindestens eines Kindes unter 18 Jahren oder eines sonstigen pflegebedürftigen Angehörigen,
3.  Wohnort, Dienstort, Mobilität,
4.  Bildungsabschluss und sonstige Qualifikationen,
5.  Amtsbezeichnung, Besoldungsgruppe, Entgeltgruppe,
6.  bisherige berufliche Tätigkeiten seit dem 3. Oktober 1990 und Dauer der Zugehörigkeit zum bisherigen Dienstherrn oder Arbeitgeber,
7.  Umfang der regelmäßigen wöchentlichen Arbeitszeit,
8.  Vorliegen einer Schwerbehinderung oder einer gleichgestellten Behinderung, Erwerbsminderung durch Dienstunfall oder Berufskrankheit,
9.  Vorliegen einer Altersteilzeitvereinbarung oder -bewilligung.

(7) Die Absätze 1 bis 6 gelten entsprechend für die Fälle, dass eines oder mehrere Ämter beziehungsweise eine oder mehrere Verbandsgemeinden beteiligt sind.

(8) Wird eine Verbandsgemeinde geändert oder aufgelöst, sind in der hierfür nach § 3 Absatz 1 erforderlichen öffentlich-rechtlichen Vereinbarung Personalüberleitungsbestimmungen in entsprechender Anwendung der Absätze 1 bis 7 zu treffen.

#### § 6 Verbandsgemeindevertretung

(1) Die Verbandsgemeindevertretung besteht aus den ehrenamtlich tätigen Verbandsgemeindevertreterinnen und Verbandsgemeindevertretern und der hauptamtlich tätigen Verbandsgemeindebürgermeisterin oder dem hauptamtlich tätigen Verbandsgemeindebürgermeister als stimmberechtigtes Mitglied.
Die ehrenamtlichen Bürgermeisterinnen oder Bürgermeister der Ortsgemeinden können, auch wenn sie keine gewählten Verbandsgemeindevertreterinnen oder Verbandsgemeindevertreter sind, an den Sitzungen der Verbandsgemeindevertretung und an den Sitzungen der Ausschüsse der Verbandsgemeindevertretung, in denen Belange ihrer Ortsgemeinde berührt werden, teilnehmen.
Ihnen steht insoweit bezogen auf ihre Ortsgemeinde ein aktives Teilnahmerecht zu.

(2) Die Verbandsgemeindevertreterinnen und die Verbandsgemeindevertreter werden am Tag der landesweiten Kommunalwahlen in allgemeiner, unmittelbarer, freier, gleicher und geheimer Wahl von den Bürgerinnen und Bürgern der Verbandsgemeinde für die Dauer von fünf Jahren gewählt.
Die Regelungen des Brandenburgischen Kommunalwahlgesetzes in der Fassung der Bekanntmachung vom 9.
Juli 2009 (GVBl.
I S. 326), das zuletzt durch Artikel 2 des Gesetzes vom 29. Juni 2018 (GVBl. I Nr. 16 S. 2) geändert worden ist, und der Brandenburgischen Kommunalwahlverordnung vom 4. Februar 2008 (GVBl.
II S. 38), die zuletzt durch Artikel 2 Absatz 7 des Gesetzes vom 4. Juli 2014 (GVBl.
I Nr. 26 S. 4) geändert worden ist, für die unmittelbaren Wahlen der Gemeindevertretungen in den Gemeinden finden entsprechende Anwendung, soweit in diesem Gesetz nichts anderes bestimmt ist.

(3) Beamtinnen und Beamte sowie Arbeitnehmerinnen und Arbeitnehmer, die im Dienst der Verbandsgemeinde stehen, können nicht zugleich

1.  Mitglied der Verbandsgemeindevertretung sein; dies gilt nicht für die Verbandsgemeindebürgermeisterin und den Verbandsgemeindebürgermeister,
2.  der Gemeindevertretung einer der Ortsgemeinden angehören.

Im Übrigen gilt § 12 des Brandenburgischen Kommunalwahlgesetzes entsprechend.

(4) Die Gemeinden, die eine Verbandsgemeinde bilden, können in der öffentlich-rechtlichen Vereinbarung nach § 3 Absatz 1 für einen Zeitraum von bis zu zwei Wahlperioden bestimmen, dass die gesetzliche Höchstzahl der Wahlkreise abweichend von § 20 Absatz 3 und 4 des Brandenburgischen Kommunalwahlgesetzes und gleichzeitig die gesetzliche Anzahl der Verbandsgemeindevertreterinnen und Verbandsgemeindevertreter abweichend von § 6 Absatz 2 Nummer 1 des Brandenburgischen Kommunalwahlgesetzes um bis zu 50 Prozent erhöht werden kann.

(5) Die Wahlkreise können abweichend von § 21 Absatz 2 Satz 2 des Brandenburgischen Kommunalwahlgesetzes mit Rücksicht auf die Grenzen einzelner oder sämtlicher Ortsgemeinden oder Ortsteile unterschiedlich groß sein; § 21 Absatz 3 Satz 2 und 3 des Brandenburgischen Kommunalwahlgesetzes gilt entsprechend.

#### § 7 Erstmalige Wahl der Verbandsgemeindevertretung

(1) Die erstmalige Wahl der Verbandsgemeindevertretung findet innerhalb von sechs Monaten nach Wirksamwerden der Bildung der Verbandsgemeinde nach § 3 Absatz 1 statt.
Die Aufsichtsbehörde bestimmt rechtzeitig den Wahltag.
Die Wahlleiterin oder der Wahlleiter der Verbandsgemeinde macht spätestens am 92. Tag vor der Wahl den Wahltag und die Wahlzeit öffentlich bekannt.

(2) Die erstmalige Wahl der Verbandsgemeindevertretung findet für den Rest der allgemeinen Wahlperiode statt.
Findet die erstmalige Wahl der Verbandsgemeindevertretung 48 Monate nach dem Tag der letzten landesweiten Kommunalwahlen statt, so endet die Wahlperiode erst mit dem Ende der nächsten allgemeinen Wahlperiode.

(3) Abweichend von Absatz 1 kann in der öffentlich-rechtlichen Vereinbarung nach § 3 Absatz 1 die Fortdauer der vorläufigen Vertretung der Verbandsgemeinde bis zum Ablauf der allgemeinen Wahlperiode bestimmt werden.

(4) Für die Vorbereitung und Durchführung der erstmaligen Wahl der Verbandsgemeindevertretung finden die Regelungen des Brandenburgischen Kommunalwahlgesetzes und der Brandenburgischen Kommunalwahlverordnung für die unmittelbaren Wahlen der Gemeindevertretungen in den Gemeinden und die ergänzenden Vorschriften des § 94 Absatz 1 der Brandenburgischen Kommunalwahlverordnung für die erstmalige Wahl der Vertretung nach der Bildung einer neuen Gemeinde entsprechend Anwendung.

#### § 8 Widerspruchsrecht

Die Gemeindevertretung einer Ortsgemeinde kann einem Beschluss der Verbandsgemeindevertretung widersprechen, wenn der Beschluss die Ortsgemeinde betrifft und deren Wohl gefährdet.
Der Widerspruch muss binnen drei Wochen nach Zugang des Beschlusses bei der ehrenamtlichen Bürgermeisterin oder dem ehrenamtlichen Bürgermeister schriftlich erhoben und begründet werden.
Der Widerspruch hat aufschiebende Wirkung gegenüber allen Ortsgemeinden und führt zur Aufhebung des Beschlusses, wenn die Verbandsgemeindevertretung den Widerspruch nicht binnen eines Monats zurückweist; der Zurückweisungsbeschluss bedarf der Mehrheit von zwei Dritteln der gesetzlichen Anzahl der Mitglieder der Verbandsgemeindevertretung.

#### § 9 Verbandsgemeindebürgermeisterin oder Verbandsgemeindebürgermeister

(1) Die Verbandsgemeindebürgermeisterin oder der Verbandsgemeindebürgermeister ist Hauptverwaltungsbeamtin oder Hauptverwaltungsbeamter der Verbandsgemeinde.
Sie oder er ist hauptamtliche Beamtin auf Zeit oder hauptamtlicher Beamter auf Zeit, ihr oder ihm obliegt die Leitung der Verbandsgemeindeverwaltung sowie die rechtliche Vertretung und Repräsentanz der Verbandsgemeinde.

(2) Die Verbandsgemeindebürgermeisterin oder der Verbandsgemeindebürgermeister wird in allgemeiner, unmittelbarer, freier, gleicher und geheimer Wahl von den Bürgerinnen und Bürgern der Verbandsgemeinde für die Dauer von acht Jahren gewählt.
Die Regelungen des Brandenburgischen Kommunalwahlgesetzes und der Brandenburgischen Kommunalwahlverordnung für die unmittelbaren Wahlen der hauptamtlichen Bürgermeisterinnen und Bürgermeister in den amtsfreien Gemeinden gelten entsprechend, soweit in diesem Gesetz nichts anderes bestimmt ist.

(3) Die Verbandsgemeindebürgermeisterin oder der Verbandsgemeindebürgermeister kann nicht zugleich ehrenamtliche Bürgermeisterin oder ehrenamtlicher Bürgermeister einer Ortsgemeinde sein.

(4) Die Verbandsgemeindebürgermeisterin oder der Verbandsgemeindebürgermeister nimmt die Aufgabe der Hauptverwaltungsbeamtin oder des Hauptverwaltungsbeamten für die Ortsgemeinden wahr.
Ist die Verbandsgemeinde selbst oder sind mehrere Ortsgemeinden an einem gerichtlichen Verfahren oder an Rechts- und Verwaltungsgeschäften beteiligt, obliegt außer in den Fällen des § 97 Absatz 1 der Kommunalverfassung des Landes Brandenburg der ehrenamtlichen Bürgermeisterin oder dem ehrenamtlichen Bürgermeister die gesetzliche Vertretung der Ortsgemeinde, soweit nicht die Gemeindevertretung für einzelne Rechtsgeschäfte oder einen bestimmten Kreis von Rechtsgeschäften eine Befreiung der Verbandsgemeindebürgermeisterin oder des Verbandsgemeindebürgermeisters vom Verbot des Insichgeschäfts beschließt; Stellvertreterin oder Stellvertreter im Sinne des § 57 Absatz 2 Satz 2 der Kommunalverfassung des Landes Brandenburg sind die Stellvertreterinnen oder Stellvertreter der ehrenamtlichen Bürgermeisterin oder des ehrenamtlichen Bürgermeisters nach § 52 der Kommunalverfassung des Landes Brandenburg.

#### § 10 Erstmalige Wahl der Verbandsgemeindebürgermeisterin oder des Verbandsgemeindebürgermeisters

(1) Die erstmalige Wahl der Verbandsgemeindebürgermeisterin oder des Verbandsgemeindebürgermeisters findet innerhalb von sechs Monaten nach Wirksamwerden der Bildung der Verbandsgemeinde statt; § 7 Absatz 1 Satz 2 und 3 gilt entsprechend.

(2) Abweichend von Absatz 1 kann in der öffentlich-rechtlichen Vereinbarung nach § 3 Absatz 1 bestimmt werden, dass die vorläufige Vertretung der Verbandsgemeinde mit der Mehrheit der gesetzlichen Anzahl ihrer Mitglieder aus dem Kreis der Hauptverwaltungsbeamtinnen oder Hauptverwaltungsbeamten sowie Beigeordneten der bisherigen amtsfreien Gemeinden und der durch die Neubildung aufgelösten Ämter binnen acht Wochen nach Wirksamwerden der Verbandsgemeindebildung eine hierzu bereite Person zur Verbandsgemeindebürgermeisterin oder zum Verbandsgemeindebürgermeister der Verbandsgemeinde wählt; § 7 Absatz 3 Satz 6 der Kommunalverfassung des Landes Brandenburg gilt entsprechend.

(3) Die Regelungen des Brandenburgischen Kommunalwahlgesetzes und der Brandenburgischen Kommunalwahlverordnung für die unmittelbaren Wahlen der hauptamtlichen Bürgermeisterinnen und Bürgermeister in den amtsfreien Gemeinden sowie die ergänzenden Vorschriften des § 94 Absatz 2 der Brandenburgischen Kommunalwahlverordnung für die erstmalige Wahl der hauptamtlichen Bürgermeisterin oder des hauptamtlichen Bürgermeisters nach der Bildung einer neuen Gemeinde gelten entsprechend.

#### § 11 Wahlbehörde

Wahlbehörde ist die Verbandsgemeindebürgermeisterin oder der Verbandsgemeindebürgermeister oder die Hauptverwaltungsbeamtin oder der Hauptverwaltungsbeamte, die oder der nach § 3 Absatz 3 dieses Amt wahrnimmt.

#### § 12 Wahlleiterin oder Wahlleiter

(1) Für die Vorbereitung und Durchführung der kommunalen Wahlen und Abstimmungen auf der Verbandsgemeindeebene beruft die Verbandsgemeindevertretung eine Wahlleiterin oder einen Wahlleiter und eine Stellvertreterin oder einen Stellvertreter; die §§ 15 und 16 des Brandenburgischen Kommunalwahlgesetzes und die §§ 2 bis 4 der Brandenburgischen Kommunalwahlverordnung gelten entsprechend.

(2) Die Gemeindevertretung einer Ortsgemeinde kann beschließen, dass der Verbandsgemeindevertretung die Aufgabe übertragen wird, für die Ortsgemeinde eine Wahlleiterin oder einen Wahlleiter und eine Stellvertreterin oder einen Stellvertreter zu berufen.
Haben mehrere Ortsgemeinden einen solchen Beschluss gefasst, so kann die Verbandsgemeindevertretung für diese Ortsgemeinden auch insgesamt oder für mehrere von ihnen jeweils eine gemeinsame Wahlleiterin oder einen gemeinsamen Wahlleiter nebst Stellvertreterin oder Stellvertreter berufen.
Im Übrigen finden die §§ 14 bis 16 des Brandenburgischen Kommunalwahlgesetzes und § 1 der Brandenburgischen Kommunalwahlverordnung sinngemäß Anwendung.

(3) Die Wahlleiterin oder der Wahlleiter der Verbandsgemeinde kann zugleich zur Wahlleiterin oder zum Wahlleiter einzelner oder mehrerer Ortsgemeinden berufen werden.
Satz 1 gilt für die Stellvertreterin oder den Stellvertreter entsprechend.

#### § 13 Haushaltswirtschaft

(1) Die Verbandsgemeinde besorgt das Haushalts-, Kassen- und Rechnungswesen gemäß Teil 1 Kapitel 3 Abschnitt 1, 2 und 4 der Kommunalverfassung des Landes Brandenburg für die Ortsgemeinden.

(2) Soweit eine Verbandsgemeinde ein Rechnungsprüfungsamt eingerichtet hat oder sich eines anderen Rechnungsprüfungsamtes bedient, obliegt diesem Rechnungsprüfungsamt auch die örtliche Prüfung der Ortsgemeinden gemäß § 102 der Kommunalverfassung des Landes Brandenburg.

(3) Im Übrigen gilt für die Verbandsgemeinde Abschnitt 3 des Gemeindestrukturänderungsförderungsgesetzes vom 15.
Oktober 2018 (GVBl.
I Nr.
22 S.
17).

#### § 14 Finanzen

(1) Die Verbandsgemeinde nimmt am kommunalen Finanzausgleich nach Maßgabe des Brandenburgischen Finanzausgleichgesetzes vom 29.
Juni 2004 (GVBl.
I S. 262), das zuletzt durch das Gesetz vom 15.
März 2016 (GVBl. I Nr. 10) geändert worden ist, teil.

(2) Soweit die sonstigen Finanzmittel der Verbandsgemeinde den für die Aufgabenerfüllung notwendigen Finanzbedarf nicht decken, ist eine Umlage von den Ortsgemeinden zu erheben (Verbandsgemeindeumlage).

(3) Handelt es sich um Einrichtungen oder Leistungen der Verbandsgemeinde, die ausschließlich oder in besonders großem oder besonders geringem Maß einzelnen Ortsgemeinden zustattenkommen, so kann die Verbandsgemeindevertretung für diese Ortsgemeinden eine ausschließliche Belastung oder eine nach dem Umfang näher zu bestimmende Mehr- oder Minderbelastung beschließen.

(4) Die Verbandsgemeindeumlage ist für jedes Haushaltsjahr neu festzusetzen; § 130 Absatz 5 sowie § 139 Absatz 3 der Kommunalverfassung des Landes Brandenburg gelten entsprechend.

#### § 15 Anwendung von Rechtsvorschriften

(1) Auf die Verbandsgemeinden sind die Vorschriften des Teils 1 der Kommunalverfassung des Landes Brandenburg, die für die kreisangehörigen amtsfreien Gemeinden gelten, entsprechend anwendbar.
Dies gilt nicht, soweit in diesem Gesetz oder in anderen Rechtsvorschriften eine abweichende Regelung getroffen wird.
§ 1 Absatz 1 und § 2 sowie die §§ 45 bis 48 der Kommunalverfassung des Landes Brandenburg finden keine Anwendung.
An die Stelle der Gemeindevertretung tritt die Verbandsgemeindevertretung und an die Stelle der Gemeindevertreterinnen oder Gemeindevertreter treten die Verbandsgemeindevertreterinnen oder Verbandsgemeindevertreter, an die Stelle der hauptamtlichen Bürgermeisterin oder des hauptamtlichen Bürgermeisters tritt die Verbandsgemeindebürgermeisterin oder der Verbandsgemeindebürgermeister.

(2) Auf die Ortsgemeinden sind die Vorschriften des Teils 1 der Kommunalverfassung des Landes Brandenburg, die für die amtsangehörigen Gemeinden gelten, entsprechend anwendbar.
Dies gilt nicht, soweit in diesem Gesetz oder in anderen Rechtsvorschriften eine abweichende Regelung getroffen wird.

(3) Vorschriften, die aufgrund des Teils 1 der Kommunalverfassung des Landes Brandenburg für die Gemeinden erlassen wurden, gelten für die Verbandsgemeinde entsprechend, soweit nicht in diesen oder anderen Rechtsvorschriften abweichende Regelungen getroffen oder die Verbandsgemeinden von der Anwendung ausgenommen werden.

(4) Vorschriften, die für Ämter und amtsfreie Gemeinden erlassen wurden, gelten für die Verbandsgemeinde entsprechend, soweit nicht in diesen oder anderen Rechtsvorschriften abweichende Regelungen getroffen oder die Verbandsgemeinden von der Anwendung ausgenommen werden.

### Abschnitt 3  
Mitverwaltung

#### § 16 Stellung und Struktur der Mitverwaltung

(1) Die Mitverwaltung ist eine Organisationsform ohne eigene Rechtspersönlichkeit von aneinandergrenzenden Gemeinden desselben Landkreises, bei der die mitverwaltende Gemeinde für die mitverwalteten Gemeinden die Aufgaben der hauptamtlichen Verwaltung wahrnimmt.

(2) Die mitverwalteten Gemeinden haben jeweils eine ehrenamtliche Bürgermeisterin oder einen ehrenamtlichen Bürgermeister und eine Gemeindevertretung.
Die mitverwaltende Gemeinde hat eine hauptamtliche Bürgermeisterin oder einen hauptamtlichen Bürgermeister und eine Gemeindevertretung.
Die hauptamtliche Bürgermeisterin oder der hauptamtliche Bürgermeister der mitverwaltenden Gemeinde nimmt im Wege der horizontalen Organleihe auch die Funktion der Hauptverwaltungsbeamtin oder des Hauptverwaltungsbeamten für die mitverwalteten Gemeinden wahr.
Die hauptamtliche Bürgermeisterin oder der hauptamtliche Bürgermeister der mitverwaltenden Gemeinde kann nicht gleichzeitig Bürgermeisterin oder Bürgermeister oder Mitglied der Gemeindevertretung einer der mitverwalteten Gemeinden sein.
Die hauptamtliche Bürgermeisterin oder der hauptamtliche Bürgermeister hat in den Sitzungen der Gemeindevertretungen der mitverwalteten Gemeinden und ihrer Ausschüsse ein aktives Teilnahmerecht.
§ 22 der Kommunalverfassung des Landes Brandenburg gilt entsprechend.

(3) § 12 des Brandenburgischen Kommunalwahlgesetzes gilt mit der Maßgabe entsprechend, dass auch die Beamtinnen und Beamten sowie Arbeitnehmerinnen und Arbeitnehmer, die im Dienst der mitverwaltenden Gemeinde stehen, nicht zugleich Bürgermeisterin oder Bürgermeister oder Mitglied der Gemeindevertretung einer der mitverwalteten Gemeinden sein können.

(4) Wahlbehörde der mitverwaltenden Gemeinde und der mitverwalteten Gemeinden ist die hauptamtliche Bürgermeisterin oder der hauptamtliche Bürgermeister der mitverwaltenden Gemeinde.

#### § 17 Bildung, Änderung und Auflösung der Mitverwaltung

(1) Gemeinden eines Landkreises, die unmittelbar aneinandergrenzen, können nach Beratung durch die untere Kommunalaufsichtsbehörde eine Mitverwaltung vereinbaren, ändern oder auflösen.
Die Rechtsverhältnisse der Mitverwaltung sind in einer öffentlich-rechtlichen Vereinbarung zwischen den Gemeinden zu regeln (Mitverwaltungsvereinbarung).
Die Vereinbarung zur Bildung, Änderung oder Auflösung der jeweiligen Mitverwaltung muss in den Gemeindevertretungen der beteiligten Gemeinden beschlossen werden.
In der Vereinbarung kann bestimmt werden, dass die hauptamtlichen Bürgermeisterinnen oder hauptamtlichen Bürgermeister oder Beigeordneten der zukünftig mitverwalteten Gemeinden zu Beigeordneten der mitverwaltenden Gemeinde bestellt werden.
Sie bedarf der Genehmigung durch das für Inneres zuständige Ministerium.
Dieses kann die Genehmigung versagen, wenn die Vereinbarung den Maßstäben dieses Gesetzes oder dem öffentlichen Wohl widerspricht.
Die Vereinbarung ist durch das für Inneres zuständige Ministerium im Amtsblatt für Brandenburg öffentlich bekannt zu machen.
Die Mitverwaltungsvereinbarung tritt, wenn kein späterer Zeitpunkt bestimmt ist, am Tag nach der öffentlichen Bekanntmachung in Kraft.
Die beteiligten Gemeinden haben in ihrem amtlichen Verkündungsblatt auf die erfolgte öffentliche Bekanntmachung hinzuweisen.

(2) Entschließt sich eine bislang amtsfreie Gemeinde, sich von einer anderen amtsfreien Gemeinde mitverwalten zu lassen, so wird die bislang amtsfreie Gemeinde in eine mitverwaltete Gemeinde umgewandelt.
Die Gemeindevertretung einer mitverwalteten Gemeinde nach Satz 1 wählt die ehrenamtliche Bürgermeisterin oder den ehrenamtlichen Bürgermeister für den Rest der laufenden Wahlperiode.
Die Amtszeit der oder des Neugewählten beginnt mit der Annahme der Wahl.
Mit Beginn der Amtszeit der neu gewählten ehrenamtlichen Bürgermeisterin oder des neu gewählten ehrenamtlichen Bürgermeisters, die oder der in entsprechender Anwendung des § 33 Absatz 1 und des § 51 Absatz 2 Satz 2 Nummer 2 der Kommunalverfassung des Landes Brandenburg kraft Amtes den Vorsitz in der Gemeindevertretung führt, verliert die oder der bisherige Vorsitzende der Gemeindevertretung dieses Amt.
In der ersten Sitzung der Gemeindevertretung nach diesem Wechsel im Vorsitz der Gemeindevertretung sind die Stellvertreter der oder des Vorsitzenden der Gemeindevertretung neu zu wählen.

(3) Bei einem Zusammenschluss der an der Mitverwaltung beteiligten Gemeinden zu einer neuen amtsfreien Gemeinde und gleichzeitiger Auflösung der Mitverwaltung nimmt die hauptamtliche Bürgermeisterin oder der hauptamtliche Bürgermeister der mitverwaltenden Gemeinde bis zum Beginn der Amtszeit der hauptamtlichen Bürgermeisterin oder des hauptamtlichen Bürgermeisters der neuen amtsfreien Gemeinde das Amt der hauptamtlichen Bürgermeisterin oder des hauptamtlichen Bürgermeisters der neugebildeten Gemeinde wahr.
Soweit ein Zusammenschluss nach Satz 1 mit der Auflösung mehrerer Gemeinden, Ämter oder Verbandsgemeinden mit Hauptverwaltungsbeamtinnen oder Hauptverwaltungsbeamten verbunden ist, ist in dem Gebietsänderungsvertrag nach § 6 Absatz 3 Satz 1 der Kommunalverfassung des Landes Brandenburg festzulegen, welcher der Hauptverwaltungsbeamteninnen oder Hauptverwaltungsbeamten das Amt der hauptamtlichen Bürgermeisterin oder des hauptamtlichen Bürgermeisters im Sinne von Satz 1 wahrnimmt.

(4) Das für Inneres zuständige Ministerium kann die Genehmigung der Bildung, Änderung oder Auflösung einer Mitverwaltung insbesondere versagen, wenn dadurch für einzelne angrenzende Gemeinden, die nicht in die Bildung oder Änderung der Mitverwaltung mit eingebunden werden sollen, der Fortbestand oder die Bildung einer eigenen leistungsfähigen Verwaltung gefährdet ist.
Gleiches gilt, wenn durch die Bildung, Änderung oder Auflösung einer Mitverwaltung die Verwaltungskraft eines danach nur noch teilweise fortbestehenden Amtes oder einer nur noch teilweise fortbestehenden Verbandsgemeinde gefährdet würde oder eine Regelung zur anteiligen Überleitung des Personals zwischen den Dienstherrn oder den Arbeitgebern nicht getroffen wurde.
Die Regelung zur anteiligen Überleitung des Personals ist von der Gemeindevertretung der mitverwaltenden Gemeinde, dem Amtsausschuss oder der Verbandsgemeindevertretung zu beschließen.

#### § 18 Mitverwaltungsvereinbarung

(1) In der Mitverwaltungsvereinbarung nach § 17 Absatz 1 sind insbesondere die Beteiligten und der Beginn der Mitverwaltung zu regeln und eine Auflistung von Vermögen und Schulden, die im Zuge der Rechtsnachfolge gemäß § 19 Absatz 2 Satz 1 übergehen, beizufügen oder Regelungen zum Übergang von Vermögen und Schulden zu treffen.

(2) In der Mitverwaltungsvereinbarung können Regelungen zum Kostenersatz vereinbart werden.

(3) Für den Fall der Beendigung der Mitverwaltung sollen bereits Regelungen für eine Rückübertragung getroffen werden.

(4) Zur Vermeidung zukünftiger gerichtlicher Streitigkeiten können die beteiligten Gemeinden eine Schiedsklausel vereinbaren.

#### § 19 Aufgabenverteilung in der Mitverwaltung

(1) Die mitverwaltende Gemeinde ist anstelle der mitverwalteten Gemeinden Trägerin der Auftragsangelegenheiten.
Selbstverwaltungsaufgaben und Pflichtaufgaben zur Erfüllung nach Weisung führt die mitverwaltende Gemeinde für die mitverwalteten Gemeinden in deren Namen als hauptamtliche Verwaltung durch (Mitverwaltung); die Aufgabenträgerschaft der mitverwalteten Gemeinden für diese Aufgaben bleibt unberührt, soweit in der Mitverwaltungsvereinbarung keine abweichenden Regelungen getroffen werden.

(2) Für die Auftragsangelegenheiten ist die mitverwaltende Gemeinde Rechtsnachfolgerin der mitverwalteten Gemeinden, soweit gesetzlich oder in der öffentlich-rechtlichen Vereinbarung nach § 17 Absatz 1 nichts anderes bestimmt ist.
Die mitverwaltende Gemeinde tritt als Rechtsnachfolgerin in die Verträge ein, die die mitverwalteten Gemeinden vor Bildung der Mitverwaltung als Trägerinnen der Auftragsangelegenheiten abgeschlossen haben, soweit gesetzlich oder in der Mitverwaltungsvereinbarung nichts anderes bestimmt ist.

(3) Bei Selbstverwaltungsaufgaben und Pflichtaufgaben zur Erfüllung nach Weisung mit Selbstverwaltungscharakter bereitet die mitverwaltende Gemeinde durch ihre Hauptverwaltungsbeamtin oder ihren Hauptverwaltungsbeamten im Benehmen mit der jeweiligen ehrenamtlichen Bürgermeisterin oder dem jeweiligen ehrenamtlichen Bürgermeister die Beschlüsse der Gemeindevertretungen der mitverwalteten Gemeinden vor und führt sie nach deren Beschlussfassung durch.
Auf dem Gebiet der Pflichtaufgaben zur Erfüllung nach Weisung ohne Selbstverwaltungscharakter trifft die mitverwaltende Gemeinde durch ihre Hauptverwaltungsbeamtin oder ihren Hauptverwaltungsbeamten die Entscheidungen für die mitverwaltete Gemeinde in deren Namen als hauptamtliche Verwaltung.

(4) In gerichtlichen Verfahren und in Rechts- und Verwaltungsgeschäften vertritt die mitverwaltende Gemeinde die mitverwaltete Gemeinde.
Ist die mitverwaltende Gemeinde selbst oder sind mehrere an der Mitverwaltung beteiligte Gemeinden an einem gerichtlichen Verfahren oder an Rechts- und Verwaltungsgeschäften beteiligt, ist außer in den Fällen des § 97 Absatz 1 der Kommunalverfassung des Landes Brandenburg die ehrenamtliche Bürgermeisterin oder der ehrenamtliche Bürgermeister gesetzliche Vertreterin oder gesetzlicher Vertreter der mitverwalteten Gemeinde, soweit nicht die Gemeindevertretung für einzelne Rechtsgeschäfte oder einen bestimmten Kreis von Rechtsgeschäften eine Befreiung der mitverwaltenden Gemeinde vom Verbot des Insichgeschäfts beschließt; Stellvertreter im Sinne des § 57 Absatz 2 Satz 2 der Kommunalverfassung des Landes Brandenburg sind die Stellvertreter der ehrenamtlichen Bürgermeisterin oder des ehrenamtlichen Bürgermeisters nach § 52 der Kommunalverfassung des Landes Brandenburg.

(5) Die an der Mitverwaltung beteiligten Gemeinden und ihre Organe arbeiten unter Beachtung ihrer jeweiligen Verantwortungsbereiche vertrauensvoll zusammen.
Sie unterrichten sich gegenseitig über alle Beschlüsse der Gemeindevertretungen und alle Entscheidungen der hauptamtlichen Bürgermeisterin oder des hauptamtlichen Bürgermeisters von grundsätzlicher Bedeutung.

#### § 20 Personalüberleitung

(1) Beamtinnen und Beamte der Gemeinden, deren Aufgaben nach Maßgabe des § 19 Absatz 1 Satz 1 und Absatz 2 durch die mitverwaltende Gemeinde anstelle der mitverwalteten Gemeinde wahrgenommen werden, treten gemäß § 31 Absatz 1 des Landesbeamtengesetzes in Verbindung mit § 16 Absatz 1 und 4 letzte Alternative des Beamtenstatusgesetzes in den Dienst der mitverwaltenden Gemeinde über.
Satz 1 findet auch Anwendung, wenn eine Wahrnehmung der Aufgaben nach Maßgabe des § 19 Absatz 1 Satz 2 und Absatz 3 erfolgt.
Die hauptamtlichen Bürgermeisterinnen und Bürgermeister und die Beigeordneten der mitverwalteten Gemeinden können zu Beigeordneten der mitverwaltenden Gemeinde bestellt werden.
§ 59 Absatz 1 und 2 der Kommunalverfassung des Landes Brandenburg findet bis zum Ablauf der Amtszeit übergetretener Beamtinnen und Beamter auf Zeit keine Anwendung.

(2) Im Übrigen richtet sich die Rechtsstellung der übergetretenen Beamtinnen und Beamten nach § 18 des Beamtenstatusgesetzes, die der Versorgungsempfängerinnen und Versorgungsempfänger nach § 19 des Beamtenstatusgesetzes, sofern in diesem Gesetz nichts anderes bestimmt ist.
§ 50 des Brandenburgischen Besoldungsgesetzes findet Anwendung.
Beamtinnen und Beamte auf Zeit, die bei Anwendung des § 18 Absatz 2 des Beamtenstatusgesetzes die Voraussetzungen des § 12 Absatz 1 Satz 1 Nummer 1 des Brandenburgischen Beamtenversorgungsgesetzes nicht erfüllen und deshalb aufgrund der Umbildung nicht in den einstweiligen Ruhestand versetzt werden können, gelten als abgewählt und erhalten bis zum Ablauf ihrer Amtszeit Besoldung und Versorgung nach den für abgewählte Wahlbeamtinnen und Wahlbeamte auf Zeit geltenden Vorschriften.
Soweit die Bildung einer Mitverwaltung einen Wechsel des Dienstortes zur Folge hat, gilt der Übertritt in den Dienst der mitverwaltenden Gemeinde als Versetzung im Sinne der umzugskostenrechtlichen und trennungsgeldrechtlichen Vorschriften.

(3) Die Arbeitsverhältnisse der Arbeitnehmerinnen und Arbeitnehmer der mitverwalteten Gemeinden gehen kraft Gesetzes auf die mitverwaltende Gemeinde über.
Die mitverwaltende Gemeinde tritt in die Rechte und Pflichten aus den im Zeitpunkt der Personalüberleitung bestehenden Arbeitsverhältnisse ein; diese werden mit der mitverwaltenden Gemeinde fortgesetzt.
Die bis zum Tag vor dem Übergang der Arbeitsverhältnisse erworbene Rechtsstellung der Arbeitnehmerinnen und Arbeitnehmer, insbesondere im Hinblick auf erreichte tarifrechtlich maßgebliche Zeiten, bleibt gewahrt.
Den betroffenen Arbeitnehmerinnen und Arbeitnehmern ist der gesetzliche Übergang der Arbeitsverhältnisse, der Eintritt der mitverwaltenden Gemeinde in die Rechte und Pflichten aus den bestehenden Arbeitsverhältnissen, deren Fortsetzung mit dem neuen Arbeitgeber und die Wahrung ihrer erworbenen Rechtsstellung schriftlich zu bestätigen.
Absatz 2 Satz 4 gilt entsprechend.

(4) § 5 Absatz 4 bis 7 gilt entsprechend.

(5) Wird die Mitverwaltung geändert oder aufgelöst, sind in der hierfür nach § 17 Absatz 1 abzuschließenden Mitverwaltungsvereinbarung Personalüberleitungsbestimmungen in entsprechender Anwendung der Absätze 1 bis 4 zu treffen.

(6) Soweit bei der Bildung einer Mitverwaltung Gemeinden beteiligt sind, die nicht über eine eigene Verwaltung verfügen, sind Regelungen zur Personalüberleitung in einer öffentlich-rechtlichen Vereinbarung zu treffen.

#### § 21 Mitverwaltungsausschuss

(1) Für die Mitverwaltung ist ein gemeinsames Organ der beteiligten Gemeinden zu bilden (Mitverwaltungsausschuss).

(2) Der Mitverwaltungsausschuss besteht aus der hauptamtlichen Bürgermeisterin oder dem hauptamtlichen Bürgermeister der mitverwaltenden Gemeinde, den ehrenamtlichen Bürgermeisterinnen oder Bürgermeistern der mitverwalteten Gemeinden und den weiteren Mitgliedern nach Absatz 3, mit der Maßgabe, dass keine Gemeinde mehr als 50 Prozent aller Stimmen haben darf.

(3) Gemeinden mit mehr als 600 Einwohnerinnen und Einwohnern bestellen weitere Mitglieder in den Mitverwaltungsausschuss.
Ihre Anzahl beträgt in Gemeinden

1.  von 601 bis 1 500 Einwohnerinnen und Einwohnern eins,
2.  von 1 501 bis 3 000 Einwohnerinnen und Einwohnern zwei,
3.  von 3 001 bis 5 000 Einwohnerinnen und Einwohnern drei,
4.  von 5 001 bis 7 000 Einwohnerinnen und Einwohnern vier und
5.  ab 7 001 Einwohnerinnen und Einwohnern fünf.

Für die Anzahl der weiteren Mitglieder ist die Einwohnerzahl maßgebend, die der letzten allgemeinen Wahl zu den Gemeindevertretungen zugrunde gelegen hat.
Bei Gebietsänderungen ist die Einwohnerzahl maßgebend, die der letzten fortgeschriebenen Bevölkerungszahl entspricht, welche mindestens sechs Monate vor dem Wirksamwerden der Gebietsänderung von dem Amt für Statistik Berlin-Brandenburg veröffentlicht wurde.

(4) Jedes Mitglied im Mitverwaltungsausschuss hat eine Stimme.
Die Vertreterinnen oder Vertreter einer Gemeinde können im Mitverwaltungsausschuss nur einheitlich abstimmen; eine uneinheitliche Stimmabgabe ist ungültig.
Die Gemeindevertretung der mitverwalteten oder der mitverwaltenden Gemeinde kann ihren Vertreterinnen oder Vertretern im Mitverwaltungsausschuss insoweit Richtlinien und Weisungen erteilen.

(5) Die Vorsitzende oder der Vorsitzende des Mitverwaltungsausschusses ist die hauptamtliche Bürgermeisterin oder der hauptamtliche Bürgermeister der mitverwaltenden Gemeinde.
Der Mitverwaltungsausschuss benennt aus seiner Mitte die stellvertretende Vorsitzende oder den stellvertretenden Vorsitzenden und mindestens eine weitere Vertreterin oder einen weiteren Vertreter.

(6) Auf den Mitverwaltungsausschuss sind die §§ 27 bis 31, 33 bis 42 und 50a Absatz 2 der Kommunalverfassung des Landes Brandenburg entsprechend anwendbar.
Dies gilt nicht, soweit in diesem Gesetz oder in anderen Rechtsvorschriften eine abweichende Regelung getroffen wird.
§ 36 Absatz 1 und § 39 Absatz 3 der Kommunalverfassung des Landes Brandenburg gelten mit der Maßgabe, dass die Bekanntmachung nach den Regelungen für die jeweilige Gemeindevertretung erfolgt.

#### § 22 Zuständigkeiten des Mitverwaltungsausschusses

(1) Der Mitverwaltungsausschuss nimmt die nach § 28 Absatz 2 Satz 1 Nummer 1, 5, 7 und 12 der Kommunalverfassung des Landes Brandenburg den Gemeindevertretungen vorbehaltenen Zuständigkeiten sowohl für die mitverwaltende als auch die mitverwalteten Gemeinden wahr.

(2) Der Mitverwaltungsausschuss soll im Interesse einer effektiven Verwaltungsdurchführung bei mehreren mitverwalteten Gemeinden auf möglichst gleichlautende Beschlüsse hinwirken.

#### § 23 Widerspruchsrecht gegen Entscheidungen des Mitverwaltungsausschusses

Die Gemeindevertretung einer der beteiligten Gemeinden kann einem Beschluss des Mitverwaltungsausschusses widersprechen, wenn der Beschluss die Gemeinde betrifft und das Wohl der Gemeinde gefährdet.
Der Widerspruch muss binnen drei Wochen nach Zugang des Beschlusses bei der Bürgermeisterin oder dem Bürgermeister schriftlich erhoben und begründet werden.
War die Bürgermeisterin oder der Bürgermeister bei der Beschlussfassung anwesend, beginnt die Frist am Tag nach der Beschlussfassung.
Der Widerspruch hat aufschiebende Wirkung gegenüber allen an der Mitverwaltung beteiligten Gemeinden und führt zur Aufhebung des Beschlusses, wenn der Mitverwaltungsausschuss den Widerspruch nicht binnen eines Monats zurückweist.
Der Zurückweisungsbeschluss bedarf der Mehrheit von zwei Dritteln der gesetzlichen Anzahl der Mitglieder des Mitverwaltungsausschusses.

#### § 24 Haushalts-, Kassen- und Rechnungswesen sowie Kostenersatz

(1) Die mitverwaltende Gemeinde und die mitverwalteten Gemeinden verfügen als jeweils fortbestehende eigenständige juristische Personen des öffentlichen Rechts über ein eigenes Haushalts-, Kassen- und Rechnungswesen gemäß den Regelungen des Teils 1 Kapitel 3 Abschnitt 1, 2 und 4 der Kommunalverfassung des Landes Brandenburg.

(2) Die Erhebung der Kreisumlage wird von der Mitverwaltung nicht berührt.

(3) Die mitverwaltende Gemeinde besorgt das Haushalts-, Kassen- und Rechnungswesen für die mitverwalteten Gemeinden.

(4) Soweit die mitverwaltende Gemeinde ein Rechnungsprüfungsamt eingerichtet hat oder sich eines anderen Rechnungsprüfungsamtes bedient, obliegt diesem Rechnungsprüfungsamt auch die örtliche Prüfung der mitverwalteten Gemeinden.

(5) Für die hauptamtliche Verwaltung der mitverwalteten Gemeinden sind der mitverwaltenden Gemeinde die Kosten zu erstatten.

(6) Im Übrigen gilt für die Mitverwaltung Abschnitt 3 des Gemeindestrukturänderungsförderungsgesetzes.

#### § 25 Anwendung von Rechtsvorschriften

(1) Auf die mitverwaltenden Gemeinden sind die Vorschriften des Teils 1 der Kommunalverfassung des Landes Brandenburg, die für die kreisangehörigen amtsfreien Gemeinden gelten, entsprechend anwendbar.
Soweit diese auf die Einwohnerzahl einer kreisangehörigen amtsfreien Gemeinde abstellen, ist die Summe der Einwohnerzahlen der mitverwaltenden Gemeinde und aller mitverwalteten Gemeinden maßgeblich.
Dies gilt nicht, soweit in diesem Gesetz oder in anderen Rechtsvorschriften eine abweichende Regelung getroffen wird.

(2) Auf die mitverwalteten Gemeinden sind die Vorschriften des Teils 1 der Kommunalverfassung des Landes Brandenburg, die für die amtsangehörigen Gemeinden gelten, entsprechend anwendbar.
Dies gilt nicht, soweit in diesem Gesetz oder in anderen Rechtsvorschriften eine abweichende Regelung getroffen wird.

(3) Vorschriften, die aufgrund des Teils 1 der Kommunalverfassung des Landes Brandenburg für die Gemeinden erlassen wurden, gelten für die mitverwaltenden und die mitverwalteten Gemeinden entsprechend, soweit nicht in diesen oder anderen Rechtsvorschriften abweichende Regelungen getroffen oder die mitverwalteten und mitverwaltenden Gemeinden von der Anwendung ausgenommen werden.

(4) Vorschriften, die für Ämter und amtsfreie Gemeinden erlassen wurden, gelten für die mitverwaltende Gemeinde und die mitverwaltete Gemeinde entsprechend, soweit nicht in diesem Gesetz oder in anderen Rechtsvorschriften abweichende Regelungen getroffen oder die mitverwaltende Gemeinde oder die mitverwaltete Gemeinde von der Anwendung ausgenommen werden; § 19 Absatz 1 Satz 1 bleibt unberührt.

### Abschnitt 4  
Beobachtungspflicht

#### § 26 Beobachtungspflicht

Die Landesregierung beobachtet bis einschließlich 31.
Dezember 2024 die tatsächlichen Auswirkungen der Einführung der Verbandsgemeinde und der Mitverwaltung, dokumentiert ihre Bewertungen in geeigneter Form, um gegebenenfalls daraus entsprechenden Nachbesserungsbedarf abzuleiten, und erstattet dem Landtag Bericht.