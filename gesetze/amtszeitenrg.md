## Gesetz zur Regelung der Amtszeiten der Landrätinnen und Landräte vor den allgemeinen Kommunalwahlen im Jahr 2019 (Amtszeitenregelungsgesetz - AmtszeitenRG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Fortführung des Amtes der Landrätin oder des Landrates

(1) Endet die Amtszeit der Landrätin oder des Landrates vor den allgemeinen Kommunalwahlen im Jahr 2019, führt die Amtsinhaberin oder der Amtsinhaber mit ihrer oder seiner Zustimmung das Amt bis zum Ablauf des Tages vor Beginn der Amtszeit der neugewählten Landrätin oder des neugewählten Landrates im Beamtenverhältnis auf Zeit fort.
Dies gilt nicht in den Fällen eines vorzeitigen Ausscheidens aus dem Amt aus anderen Gründen.
Die Höchstdauer einer Fortführung des Amtes nach Satz 1 beträgt sechs Monate.
Die Zustimmung zur Fortführung des Amtes ist vor Ablauf der Amtszeit schriftlich gegenüber dem Dienstvorgesetzten zu erklären.

(2) Wird eine Zustimmung zur Fortführung des Amtes nach Absatz 1 Satz 1 und 4 nicht erklärt, tritt die Landrätin oder der Landrat abweichend von § 122 Absatz 3 des Landesbeamtengesetzes vom 3. April 2009 (GVBl. I S. 26), das zuletzt durch Artikel 2 des Gesetzes vom 11. Januar 2016 (GVBl.
I Nr. 3 S. 2) geändert worden ist, mit Ablauf ihrer oder seiner Amtszeit in den Ruhestand.

(3) Bleibt in den Fällen des Absatzes 1 eine Wiederwahl erfolglos oder wird die Höchstdauer der Fortführung des Amtes nach Absatz 1 Satz 3 erreicht, tritt die Landrätin oder der Landrat abweichend von § 122 Absatz 3 des Landesbeamtengesetzes in den Ruhestand.
In diesen Fällen findet § 122 Absatz 6 des Landesbeamtengesetzes auch nach Ablauf der Fortführung des Amtes Anwendung, sofern zuvor nur eine Amtszeit zurückgelegt worden ist.

#### § 2 Einschränkung von Grundrechten

Durch dieses Gesetz wird das Grundrecht der Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 3 Inkrafttreten, Außerkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt das Amtszeitenregelungsgesetz vom 10. Juli 2017 (GVBl.
I Nr. 15) außer Kraft.

Potsdam, den 16.
November 2017

Die Präsidentin  
des Landtages Brandenburg

Britta Stark