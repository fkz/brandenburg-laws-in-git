## Gesetz zum Abkommen über die gemeinsame Einrichtung einer Ethikkommission für Präimplantationsdiagnostik bei der Ärztekammer Hamburg

Der Landtag hat das folgende Gesetz beschlossen:

### § 1 

(1) Dem Abkommen zwischen den Ländern Brandenburg, Freie Hansestadt Bremen, Freie und Hansestadt Hamburg, Mecklenburg-Vorpommern, Niedersachsen und Schleswig-Holstein über die gemeinsame Einrichtung einer Ethikkommission für Präimplantationsdiagnostik bei der Ärztekammer Hamburg, das vom Land Brandenburg am 7. November 2013 unterzeichnet wurde, wird zugestimmt.

(2) Das in Absatz 1 genannte Abkommen wird als Anlage zu diesem Gesetz veröffentlicht.

(3) Die Übernahme einer gesamtschuldnerischen Haftung entsprechend § 9 des in Absatz 1 genannten Abkommens bedarf entgegen § 39 Absatz 1 der Landeshaushaltsordnung keiner der Höhe nach bestimmten Ermächtigung durch Landesgesetz.

### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem das Abkommen nach seinem § 12 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu machen.

Potsdam, den 11.
Februar 2014

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Abkommen](/vertraege/abkethikkpraeimpl_2014)