## Gesetz über die Aufhebung des Brandenburgischen Versorgungsrücklagengesetzes

#### § 1 Auflösung der Versorgungsrücklagen im Land Brandenburg

(1) Die Versorgungsrücklagen im Land Brandenburg werden zum 31.
Dezember 2017 aufgelöst.

(2) Die zum 31.
Dezember 2017 im Sondervermögen „Versorgungsrücklage des Landes Brandenburg“ angesammelten Vermögenswerte werden dem Sondervermögen „Versorgungsfonds des Landes Brandenburg“ übertragen.