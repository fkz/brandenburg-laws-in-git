## Gesetz zu dem Staatsvertrag zur Neuregulierung des Glücksspielwesens in Deutschland (Glücksspielstaatsvertrag 2021 - GlüStV 2021)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem am 28.
Oktober 2020 vom Land Brandenburg unterzeichneten Staatsvertrag zur Neuregulierung des Glücksspielwesens in Deutschland (Glücksspielstaatsvertrag 2021 – GlüStV 2021) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

#### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem § 35 Absatz 1 Satz 1 am 1.
Juli 2021 in Kraft.
Sollte der Staatsvertrag nach seinem § 35 Absatz 1 Satz 2 oder Satz 3 gegenstandslos werden, ist dies unverzüglich im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 9.
Februar 2021

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke

* * *

[zum Staatsvertrag](/vertraege/gluestv_2021)

* * *

### Anlagen

1

[Staatsvertrag zur Neuregulierung des Glücksspielwesens in Deutschland](/br2/sixcms/media.php/68/GVBl_I_06_2021-Anlage.pdf "Staatsvertrag zur Neuregulierung des Glücksspielwesens in Deutschland") 711.8 KB