## Gesetz zur Änderung der Arbeitsgerichtsstruktur

#### § 1 Aufhebung der Arbeitsgerichte Eberswalde und Potsdam

Die Arbeitsgerichte Eberswalde und Potsdam werden mit Ablauf des 31. Dezember 2022 aufgehoben.

#### § 2 Neustrukturierung der Bezirke der Arbeitsgerichte

(1) Ab dem 1.
Januar 2023 ist der Bezirk des aufgehobenen Arbeitsgerichts Eberswalde dem Bezirk des Arbeitsgerichts Frankfurt (Oder) zugeordnet.

(2) Ab dem 1.
Januar 2023 ist der Bezirk des aufgehobenen Arbeitsgerichts Potsdam dem Bezirk des Arbeitsgerichts Brandenburg an der Havel zugeordnet.

(3) Ab dem 1.
Januar 2023 ist aus dem Bezirk des Arbeitsgerichts Brandenburg an der Havel das Gebiet des Landkreises Havelland dem Bezirk des Arbeitsgerichts Neuruppin zugeordnet.

#### § 3 Aufhebung der in Senftenberg/Zły Komorow bestehenden Kammern des Arbeitsgerichts Cottbus/Chóśebuz

Die in Senftenberg/Zły Komorow bestehenden Kammern des Arbeitsgerichts Cottbus/Chóśebuz werden mit Ablauf des 31.
Dezember 2022 aufgehoben.

#### § 4 Errichtung von Kammern des Arbeitsgerichts Frankfurt (Oder) in Eberswalde

Ab dem 1.
Januar 2023 bestehen in Eberswalde Kammern des Arbeitsgerichts Frankfurt (Oder).
Diese Kammern nehmen für den Teil des Arbeitsgerichtsbezirks, der aus den Landkreisen Barnim und Uckermark besteht, die Geschäfte des Arbeitsgerichts wahr.

#### § 5 Gerichtstage

Ab dem 1.
Januar 2023 halten Gerichtstage ab:

1.  das Arbeitsgericht Cottbus/Chóśebuz in Senftenberg/Zły Komorow,
2.  das Arbeitsgericht Brandenburg an der Havel in Potsdam.