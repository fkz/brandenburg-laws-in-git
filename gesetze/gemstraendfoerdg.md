## Gesetz zu finanziellen Hilfen und zur Schaffung von Ausnahmeregelungen zur Erleichterung von freiwilligen Zusammenschlüssen zur Vergrößerung der Strukturen auf gemeindlicher Ebene und zur Verringerung der Anzahl der hauptamtlichen Verwaltungen auf der gemeindlichen Ebene (Gemeindestrukturänderungsförderungsgesetz - GemStrÄndFördG)

### Abschnitt 1  
Anwendungsbereich

#### § 1 Anwendungsbereich, Begriffsbestimmung

Dieses Gesetz gilt für alle Gemeindestrukturänderungen.
Gemeindestrukturänderungen im Sinne dieses Gesetzes sind Gebietsänderungen gemäß § 6 Absatz 3 der Kommunalverfassung des Landes Brandenburg vom 18.
Dezember 2007 (GVBl.
I S. 286), die zuletzt durch das Gesetz vom 29. Juni 2018 (GVBl.
I Nr. 15, 19) geändert worden ist, und Änderungen, Auflösungen oder Zusammenschlüsse von Ämtern gemäß § 134 der Kommunalverfassung des Landes Brandenburg sowie die Bildung, Änderung oder Auflösung einer Verbandsgemeinde oder Mitverwaltung nach § 3 Absatz 1 und § 17 Absatz 1 des Verbandsgemeinde- und Mitverwaltungsgesetzes vom 15. Oktober 2018 (GVBl.
I Nr.
22 S.
2).

### Abschnitt 2  
Finanzhilfen

#### § 2 Einmalkostenpauschale

(1) Auf Antrag kann nach Bestandskraft der Genehmigung einer freiwilligen

1.  Bildung oder Änderung einer amtsfreien oder mitverwalteten Gemeinde gemäß § 6 Absatz 3 der Kommunalverfassung des Landes Brandenburg oder
2.  Bildung oder Änderung einer Verbandsgemeinde gemäß § 3 Absatz 1 des Verbandsgemeinde- und Mitverwaltungsgesetzes oder
3.  Bildung einer Mitverwaltung gemäß § 17 Absatz 1 des Verbandsgemeinde- und Mitverwaltungsgesetzes

unter Wegfall mindestens einer hauptamtlichen Verwaltung eine Einmalkostenpauschale vom für Inneres zuständigen Ministerium gewährt werden.
Wird ein Amt unter Wegfall mindestens einer hauptamtlichen Verwaltung durch Aufnahme einer Gemeinde oder mehrerer Gemeinden gemäß § 134 Absatz 1 der Kommunalverfassung des Landes Brandenburg geändert oder werden bestehende Ämter gemäß § 134 Absatz 1 der Kommunalverfassung des Landes Brandenburg zusammengeschlossen, gilt Satz 1 entsprechend.
Wird aus einem Amt eine amtsfreie Gemeinde gebildet, kann auf Antrag nach Bestandskraft der Genehmigung des freiwilligen Zusammenschlusses eine Einmalkostenpauschale vom für Inneres zuständigen Ministerium gewährt werden.
Wird aus einem Amt eine Verbandsgemeinde gebildet, kann auf Antrag nach Bestandskraft der Genehmigung der freiwilligen Bildung einer Verbandsgemeinde eine Einmalkostenpauschale vom für Inneres zuständigen Ministerium gewährt werden.

(2) Die Einmalkostenpauschale in den Fällen des Absatzes 1 Satz 1 Nummer 1 und 2 und des Absatzes 1 Satz 2 beträgt ab einer Zuständigkeit der neuen hauptamtlichen Verwaltung für 6 000 Einwohnerinnen und Einwohner 400 000 Euro oder ab einer Zuständigkeit der neuen hauptamtlichen Verwaltung für 7 000 Einwohnerinnen und Einwohner 500 000 Euro, jeweils multipliziert mit der Zahl der insgesamt reduzierten Anzahl hauptamtlicher Verwaltungen.
Im Fall des Absatzes 1 Satz 1 Nummer 3 beträgt die Einmalkostenpauschale 300 000 Euro je wegfallender hauptamtlicher Verwaltung.
Werden unter Wegfall einer hauptamtlichen Verwaltung mehr als eine hauptamtliche Verwaltung in den Fällen des Absatzes 1 gebildet, wird die Einmalkostenpauschale nach dem Verhältnis der auf die neu gebildeten hauptamtlichen Verwaltungen übergehenden Bevölkerungszahl der wegfallenden hauptamtlichen Verwaltung geteilt.
In den Fällen des Absatzes 1 Satz 3 und 4 beträgt die Einmalkostenpauschale 300 000 Euro.
Die Gewährung einer Einmalkostenpauschale im Fall des Absatzes 1 Satz 2 schließt die Gewährung einer Einmalkostenpauschale im Fall des Absatzes 1 Satz 4 aus.

(3) Die Einmalkostenpauschale nach Absatz 2 Satz 2 ist regelmäßig zurückzufordern, wenn die Mitverwaltung innerhalb von fünf Jahren aufgelöst wird.
Von einer Rückforderung kann insbesondere dann abgesehen werden, wenn sich mit Wirksamwerden der Auflösung der Mitverwaltung die Zahl der hauptamtlichen Verwaltungen durch Bildung einer amtsfreien Gemeinde oder Verbandsgemeinde nicht erhöht.

(4) Maßgebende Zahl der Einwohnerinnen und Einwohner ist der Durchschnitt der letzten zwölf vom Amt für Statistik Berlin-Brandenburg veröffentlichten monatlich fortgeschriebenen Bevölkerungszahlen vor der Genehmigung der der freiwilligen Bildung oder Änderung in den Fällen des Absatzes 1 zugrunde liegenden öffentlich-rechtlichen Vereinbarung.

(5) Empfänger der Einmalkostenpauschale nach Absatz 2 Satz 1 und 4 ist der Träger der neuen hauptamtlichen Verwaltung.
Empfänger der Einmalkostenpauschale nach Absatz 2 Satz 2 ist die mitverwaltende Gemeinde treuhänderisch für die mitverwaltende und die mitverwalteten Gemeinden.

(6) Der Antrag auf Gewährung der Einmalkostenpauschale ist von den in den Fällen des Absatzes 1 beteiligten Gebietskörperschaften gemeinsam mit dem Antrag auf Genehmigung der der freiwilligen Bildung oder Änderung in den Fällen des Absatzes 1 zugrunde liegenden öffentlich-rechtlichen Vereinbarung zu stellen.
Dem Antrag ist eine zwischen den beteiligten Gebietskörperschaften vereinbarte Planung der Mittelverwendung beizufügen.
Die Verwendung ist durch einen einfachen Verwendungsnachweis durch den Empfänger nachzuweisen.

(7) Die Mittel können für investive und nicht investive Maßnahmen verwendet werden.

#### § 3 Teilentschuldung

(1) Empfänger nach § 2 Absatz 5 in Verbindung mit § 2 Absatz 1 Satz 1 bis 3 können darüber hinaus für die an der Gemeindestrukturveränderung beteiligten Gemeinden, Ämter, Verbandsgemeinden und Ortsgemeinden Zuweisungen zum Abbau der Kassenkredite zum Stichtag 31. Dezember 2017 erhalten.
Die Zuweisungen dürfen den Kassenkreditbestand zum 31. Dezember des Jahres vor der Gemeindestrukturänderung im Sinne des § 1 nicht überschreiten.
Bei mehrmaligen Gemeindestrukturveränderungen werden Kassenkreditbestände nur einmalig für eine Zuweisung berücksichtigt.

(2) Die Höhe der Zuweisungen beträgt 40 Prozent des Kassenkreditbestandes nach Absatz 1 und darf einen Maximalbetrag von 3 Millionen Euro nicht überschreiten.

(3) Die Entschuldungshilfen sind ergebnisneutral sowie im Finanzhaushalt und in der Finanzrechnung zu verbuchen.
Als Gegenkonto in der Bilanz ist das Basisreinvermögen zu verwenden.

#### § 4 Bereitstellung von Haushaltsmitteln des Landes

(1) Für die Finanzierung der Einmalkostenpauschale nach § 2 und der Teilentschuldung nach § 3 werden Landesmittel in Höhe von 28 000 000 Euro zur Verfügung gestellt.

(2) Leistungen nach den §§ 2 und 3 werden bis zum Erreichen der Obergrenze nach Absatz 1 gewährt.
Zur Gewährleistung der Obergrenze wird auf die zeitliche Reihenfolge der rechtlichen Umorganisation der Verwaltungen abgestellt.

(3) Das für Kommunales zuständige Ministerium erlässt eine Verwaltungsvorschrift über die Verwendung und Verteilung der Mittel nach § 3.

### Abschnitt 3  
Haushaltsrechtliche Ausnahmeregelungen

#### § 5 Haushaltssatzung

Bei der Aufstellung der Haushaltssatzung sowie einer Nachtragssatzung für das Haushaltsjahr, in dem die Gemeindestrukturänderung im Sinne des § 1 in Kraft tritt, kann auf die Erstellung folgender Bestandteile verzichtet werden:

1.  mittelfristige Ergebnis- und Finanzplanung gemäß § 72 Absatz 1 der Kommunalverfassung des Landes Brandenburg, soweit die Haushaltssatzung keine nach § 73 Absatz 4 und § 74 Absatz 2 der Kommunalverfassung des Landes Brandenburg genehmigungspflichtigen Teile enthält,
2.  die dem Haushaltsplan beizufügenden Anlagen gemäß § 3 Absatz 2 der Kommunalen Haushalts- und Kassenverordnung vom 14. Februar 2008 (GVBl. II S. 14), die zuletzt durch die Verordnung vom 15. Februar 2018 (GVBl. II Nr. 15) geändert worden ist, und
3.  das Haushaltssicherungskonzept gemäß § 63 Absatz 5 der Kommunalverfassung des Landes Brandenburg.

Bereits beschlossene und genehmigte Haushaltssicherungskonzepte gelten fort.

#### § 6 Jahresabschluss

Bei der Aufstellung eines Jahresabschlusses auf den Tag vor dem Inkrafttreten der Gemeindestrukturänderung im Sinne des § 1 kann auf die Erstellung folgender Bestandteile verzichtet werden:

1.  die Teilrechnungen nach § 82 Absatz 2 Satz 1 Nummer 3 der Kommunalverfassung des Landes Brandenburg,
2.  den Rechenschaftsbericht nach § 82 Absatz 2 Satz 1 Nummer 5 der Kommunalverfassung des Landes Brandenburg und
3.  die Anlagen-, Forderungs- und Verbindlichkeitenübersicht sowie den Beteiligungsbericht nach § 82 Absatz 2 Satz 2 Nummer 2 bis 5 der Kommunalverfassung des Landes Brandenburg.

#### § 7 Gesamtabschluss

Auf die Erstellung eines Gesamtabschlusses für das Jahr der Gemeindestrukturänderung im Sinne des § 1 sowie für das darauf folgende Haushaltsjahr kann verzichtet werden.

#### § 8 Übergang von Rechten und Pflichten sowie Vermögen und Schulden

(1) Der öffentlich-rechtlichen Vereinbarung nach § 3 Absatz 1 oder § 17 Absatz 1 des Verbandsgemeinde- und Mitverwaltungsgesetzes ist insbesondere eine Auflistung über das übergehende Vermögen und die übergehenden Schulden beizufügen oder in dieser sind Regelungen zum Übergang von Vermögen und Schulden zu treffen.

(2) Der Übergang des Vermögens und der Schulden erfolgt ergebnisneutral durch Buchung gegen das Basisreinvermögen.
Bei der Einbuchung von Vermögensgegenständen wird ein entsprechender Sonderposten gebildet.
Die mit dem Übergang verbundenen Rechte und Pflichten sowie Vermögen und Schulden sind darüber hinaus im Anhang zum Jahresabschluss aufzuführen.

(3) Zu den Rechten und Pflichten gehören auch alle nichtbilanzierten Rechte und Pflichten.
Zu dem Vermögen und den Schulden gehören auch nichtbilanziertes Vermögen und nichtbilanzierte Schulden.

(4) Alle Bilanzposten gehen zu den Wertansätzen des Jahresabschlusses des Haushaltsjahres vor dem Inkrafttreten der Gemeindestrukturänderung im Sinne des § 1 über.
Wertberichtigungen der Bilanzposten und Vereinheitlichungen der Ansatz- und Bewertungsgrundsätze können letztmalig im vierten der Gemeindestrukturänderung folgenden Jahresabschluss vorgenommen werden und sind im Anhang zu dokumentieren.
Die Wertberichtigungen erfolgen ergebnisneutral durch Buchung gegen das Basisreinvermögen.

#### § 9 Buchungsvorschriften

(1) Ein nicht durch Eigenkapital gedeckter Fehlbetrag in der Bilanz soll einmalig im ersten Jahresabschluss nach der Gemeindestrukturänderung im Sinne des § 1 mit den Überschussrücklagen und darüber hinaus mit dem Basisreinvermögen verrechnet werden.
Die entstandenen Fehlbeträge des ordentlichen und des außerordentlichen Ergebnisses sowie die Bedarfszuweisungen zum Abbau des Negativsaldos im Finanzhaushalt sollen einmalig im ersten Jahresabschluss nach der Gemeindestrukturänderung im Sinne des § 1 mit dem Basisreinvermögen verrechnet werden.

(2) Überschussrücklagen, die nicht durch Finanzmittel gemäß § 57 Absatz 3 Nummer 2.3 und 2.4 der Kommunalen Haushalts- und Kassenverordnung gedeckt sind und nicht innerhalb des mittelfristigen Ergebnis- und Finanzplanungszeitraumes zum Ausgleich des Ergebnishaushaltes benötigt werden, sind einmalig im ersten Jahresabschluss nach der Gemeindestrukturänderung dem Basisreinvermögen zuzuführen.

(3) Sonderrücklagen aus nicht verbrauchten investiven Schlüsselzuweisungen bleiben erhalten.

#### § 10 Prüfungswesen

(1) Das Rechnungsprüfungsamt kann sich bei der Prüfung der Jahresabschlüsse nach § 82 der Kommunalverfassung des Landes Brandenburg einer Wirtschaftsprüferin, eines Wirtschaftsprüfers oder einer Wirtschaftsprüfungsgesellschaft bedienen.

(2) Soweit aus Anlass der Gemeindestrukturänderung im Sinne des § 1 eine Eröffnungsbilanz erstellt wird, kann auf die Prüfung der Eröffnungsbilanz verzichtet werden.

#### § 11 Unterjähriges Haushalts-, Kassen-, Rechnungs- und Prüfungswesen

(1) Bei einer unterjährigen Gemeindestrukturänderung im Sinne des § 1 mit Gesamtrechtsnachfolge gelten alle Haushaltssatzungen der Gemeinden oder Gemeindeverbände, die ihre juristische Eigenständigkeit verlieren, bis zum Ende des Haushaltsjahres fort, in dem die Gemeindestrukturänderung im Sinne des § 1 in Kraft tritt.
Dies gilt auch für die Haushaltssatzungen, die Festsetzungen für zwei Haushaltsjahre enthalten (Doppelhaushalt).

(2) Die in den nach Absatz 1 fortgeltenden Haushaltssatzungen festgesetzten Aufwendungen sind innerhalb der jeweiligen Haushaltssatzung und mit den fortgeltenden Haushaltssatzungen der an der Gemeindestrukturänderung im Sinne des § 1 beteiligten Gemeinden oder Gemeindeverbände satzungsübergreifend gegenseitig deckungsfähig.
Dies gilt entsprechend für Auszahlungen, Erträge und Einzahlungen.

(3) Für den verbleibenden Zeitraum des Haushaltsjahres der Gemeindestrukturänderung im Sinne des § 1 kann in Abweichung zu Absatz 1 eine Haushaltssatzung erlassen werden (Teil-Haushaltssatzung).
Das Haushaltsjahr für die Teil-Haushaltssatzung beginnt mit Inkrafttreten der Gemeindestrukturänderung im Sinne des § 1.

(4) Im Fall des Absatzes 3 ist auf den Tag vor dem Inkrafttreten der Gemeindestrukturänderung im Sinne des § 1 ein Jahresabschluss zu erstellen.
Im Übrigen kann auf die Erstellung eines Jahresabschlusses gemäß § 82 der Kommunalverfassung des Landes Brandenburg auf einen Zeitpunkt vor dem 31. Dezember des Jahres der Gemeindestrukturänderung im Sinne des § 1 verzichtet werden.

(5) Auf die Prüfung der unterjährigen Jahresabschlüsse nach Absatz 4 Satz 1 kann verzichtet werden.

### Abschnitt 4  
Sonstige Ausnahmeregelungen zur Erleichterung von freiwilligen Gemeindestrukturänderungen

#### § 12 Regelung unterschiedlicher Steuersätze in Gemeindestrukturänderungsverträgen

An einer Gemeindeneubildung oder Eingliederung nach § 6 Absatz 3 der Kommunalverfassung des Landes Brandenburg beteiligte Gemeinden können im Gebietsänderungsvertrag die Fortgeltung und schrittweise Angleichung von Steuersätzen, höchstens jedoch für einen Zeitraum von fünf Jahren ab dem Wirksamwerden der Gemeindeneugliederung, regeln, soweit durch Gesetz oder Rechtsverordnung nichts anderes bestimmt ist.

#### § 13 Verzicht auf Neuwahlen von kommunalen Wahlbeamtinnen und Wahlbeamten

(1) Hat die Gemeindevertretung beschlossen, Verhandlungen über den Zusammenschluss oder eine andere Körperschaftsumbildung mit einer anderen Gemeinde aufzunehmen, so kann sie auch beschließen, auf eine erforderliche Wahl der Hauptverwaltungsbeamtin oder des Hauptverwaltungsbeamten für einen festzulegenden Zeitraum von längstens zwei Jahren nach dem Ablauf der Amtszeit oder dem Ausscheiden aus dem Amt vorläufig zu verzichten.
Der Beschluss über den vorläufigen Verzicht ist mindestens fünf Monate vor Ablauf der Amtszeit oder vor Beginn des Ruhestandes der Amtsinhaberin oder des Amtsinhabers oder innerhalb eines Monats nach dem vorzeitigen Ausscheiden aus dem Amt zu fassen.
Auf Antrag der Gemeinde kann der gemäß Satz 1 festgelegte Zeitraum durch die oberste Kommunalaufsichtsbehörde einmalig um bis zu zwölf Monate verlängert werden, wenn die nach Satz 1 geplante Körperschaftsumbildung innerhalb des Verlängerungszeitraumes voraussichtlich abgeschlossen sein wird.

(2) Der Beschluss der Gemeindevertretung nach Absatz 1 kann verbunden werden mit einem Beschluss über die Fortführung des Amtes im Beamtenverhältnis auf Zeit durch die Amtsinhaberin oder den Amtsinhaber mit ihrer oder seiner Zustimmung.
Die Zustimmung zur Fortführung des Amtes nach Ablauf der ursprünglichen Amtszeit ist schriftlich gegenüber der Gemeindevertretung als Dienstvorgesetzte zu erklären.
Einer Ernennung bedarf es nicht.
Die Fortführung des Amtes endet, wenn das Amt infolge der Körperschaftsumbildung wegfällt oder eine Nachfolgerin oder ein Nachfolger das Amt antritt.
Die Amtsinhaberin oder der Amtsinhaber, die oder der nach Zusammenschluss oder Körperschaftsumbildung nicht erneut zur Beamtin auf Zeit oder zum Beamten auf Zeit ernannt wird, tritt mit Ablauf der fortgeführten Amtszeit nach Maßgabe des § 122 Absatz 3 des Landesbeamtengesetzes vom 3. April 2009 (GVBl. I S. 26), das zuletzt durch das Gesetz vom 29. Juni 2018 (GVBl.
I Nr. 17) geändert worden ist, in den Ruhestand, ohne dass es der Erklärung der Bereitschaft für eine Wiederwahl bedarf; § 122 Absatz 6 des Landesbeamtengesetzes findet Anwendung.
Für eine Amtsinhaberin oder einen Amtsinhaber, die oder der in der neuen Gebietskörperschaft erneut in das Beamtenverhältnis auf Zeit berufen wird, gilt § 122 Absatz 4 Satz 2 des Landesbeamtengesetzes entsprechend.
Wird ein Beschluss nach Satz 1 nicht gefasst oder die Zustimmung nach Satz 2 nicht erklärt, tritt die Rechtsfolge des § 122 Absatz 3 des Landesbeamtengesetzes mit Ablauf der Amtszeit bei Vorliegen der sonstigen Voraussetzungen auch ohne Bereitschaftserklärung für eine Wiederwahl ein.

(3) Wenn der Beschluss der Gemeindevertretung nach Absatz 1 Satz 1 oder die Entscheidung der obersten Kommunalaufsichtsbehörde nach Absatz 1 Satz 3 aufgehoben wird oder die für den vorläufigen Wahlverzicht festgelegte Zeitdauer abgelaufen ist, so wird die Nachfolgerin oder der Nachfolger des Hauptverwaltungsbeamten oder der Hauptverwaltungsbeamtin innerhalb von sechs Monaten gewählt.
Die Wahl kann bis zu drei weitere Monate später stattfinden, wenn dadurch die gemeinsame Durchführung mit einer anderen Wahl ermöglicht wird.

(4) Die Absätze 1 bis 3 gelten für einen Amtsausschuss, der Verhandlungen über den Zusammenschluss mit einem anderen Amt oder einer amtsfreien Gemeinde oder die Umbildung zur Verbandsgemeinde oder die Vereinbarung einer Mitverwaltung beschlossen hat, entsprechend.

#### § 14 Wechsel einer bisher amtsfreien Gemeinde in ein bestehendes Amt

(1) Eine bislang amtsfreie Gemeinde wird mit der Aufnahme in ein bestehendes Amt in eine amtsangehörige Gemeinde umgewandelt.

(2) Die Gemeindevertretung der amtsangehörigen Gemeinde nach Absatz 1 wählt die neue ehrenamtliche Bürgermeisterin oder den neuen ehrenamtlichen Bürgermeister für den Rest der laufenden Wahlperiode.
Die Amtszeit der oder des Neugewählten beginnt mit der Annahme der Wahl.

(3) Mit Beginn der Amtszeit der neu gewählten ehrenamtlichen Bürgermeisterin oder des neu gewählten ehrenamtlichen Bürgermeisters nach Absatz 2, die oder der gemäß § 33 Absatz 1 und § 51 Absatz 2 Satz 2 Nummer 2 der Kommunalverfassung des Landes Brandenburg kraft ihres oder seines Amtes den Vorsitz in der Gemeindevertretung führt, verliert die oder der bisherige Vorsitzende der Gemeindevertretung dieses Amt.
In der ersten Sitzung der Gemeindevertretung nach diesem Wechsel im Vorsitz der Gemeindevertretung sind die Stellvertreter des Vorsitzenden der Gemeindevertretung neu zu wählen.