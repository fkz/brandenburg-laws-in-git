## Gesetz zur Weiterentwicklung der Hochschulregion Lausitz

### § 1  
Errichtung

(1) Mit Wirkung zum 1.
Juli 2013 ist die Brandenburgische Technische Universität Cottbus-Senftenberg mit den Standorten Cottbus und Senftenberg errichtet.

(2) Die Fakultäten, Einrichtungen und Studiengänge der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH) sind mit ihrer Errichtung solche der Brandenburgischen Technischen Universität Cottbus-Senftenberg.
Die sich auf sie beziehenden Studien- und Prüfungsordnungen und sonstigen Satzungen gelten bis zum Erlass neuer Satzungen sinngemäß als Satzungen der Brandenburgischen Technischen Universität Cottbus-Senftenberg weiter.

(3) Die Verwaltungen der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH) bilden die Hochschulverwaltung der Brandenburgischen Technischen Universität Cottbus-Senftenberg.

(4) Für die Brandenburgische Technische Universität Cottbus-Senftenberg gelten die Vorschriften des Brandenburgischen Hochschulgesetzes, soweit dieses Gesetz nichts Abweichendes bestimmt.

### § 2  
Organisatorische Grundeinheiten

(1) Bei der Einrichtung der organisatorischen Grundeinheiten für Lehre und Forschung soll die Brandenburgische Technische Universität Cottbus-Senftenberg die in der Anlage beigefügten Empfehlungen der Kommission zur Weiterentwicklung der Hochschulregion Lausitz vom Januar 2012 in der Fassung des Abschlussberichts der Hochschulstrukturkommission des Landes Brandenburg berücksichtigen.
Die Brandenburgische Technische Universität Cottbus-Senftenberg soll demgemäß organisatorische Grundeinheiten für ein stärker anwendungsbezogenes und für ein stärker theoriegeleitetes Angebot in Forschung und Lehre einrichten sowie solche, die miteinander verschränkte Elemente aufweisen.
Abweichungen sind im Rahmen der staatlichen Zielsetzungen der Hochschulentwicklung möglich.

(2) Abweichend von Absatz 1 kann sich die Brandenburgische Technische Universität Cottbus-Senftenberg in die drei in § 3 Absatz 1 Satz 1 Nummer 2 bis 4 genannten Schools als organisatorische Grundeinheiten für Lehre und Forschung gliedern.
In diesem Fall gilt:

1.  Die Schools nehmen nach Maßgabe der Grundordnung der Brandenburgischen Technischen Universität Cottbus-Senftenberg die Aufgaben, Rechte und Pflichten von Fachbereichen nach dem Brandenburgischen Hochschulgesetz sowie die Aufgaben nach § 3 Absatz 1 Satz 1 Nummer 2 bis 4 wahr.
    Sie untergliedern sich fachbezogen.
    § 69 Absatz 2 des Brandenburgischen Hochschulgesetzes findet auf die Schools keine Anwendung.
2.  Die Schools werden jeweils von einer Dekanin oder einem Dekan geleitet.
    Die Grundordnung kann vorsehen, dass die Dekanin oder der Dekan eine andere Bezeichnung führt.
    Die Schools müssen über mindestens ein von den Mitgliedern und Angehörigen gewähltes Kollegialorgan verfügen, das über die Dekanin oder den Dekan Aufsicht führt und dazu ein umfassendes Informationsrecht hat.
3.  Die Mitglieder der Brandenburgischen Technischen Universität Cottbus-Senftenberg nehmen ihre Rechte in der Undergraduate School und mindestens in einer weiteren School wahr.
    Studierende in Bachelorstudiengängen nehmen ihre Rechte ausschließlich in der Undergraduate School wahr, Promotionsstudierende ausschließlich in der Graduate Research School.
    Das Nähere zur Ausübung der Mitgliedschaftsrechte regelt die Grundordnung.
    Sie soll vorsehen, dass die Ausübung der Mitgliedschaftsrechte in der Graduate Research School für Hochschullehrerinnen und Hochschullehrer eine Mindestqualifikation voraussetzt.
4.  Promotionsordnungen und Habilitationsordnungen der Brandenburgischen Technischen Universität Cottbus-Senftenberg werden von der Graduate Research School im Benehmen mit der Undergraduate School und der Professional School erlassen.
5.  Die Entscheidung nach § 44 Absatz 2 des Brandenburgischen Hochschulgesetzes trifft die Präsidentin oder der Präsident, in der Gründungsphase die Gründungspräsidentin oder der Gründungspräsident, im Einvernehmen mit der Graduate Research School und im Benehmen mit den anderen Schools.
    Sie oder er bestellt die Gutachterinnen und Gutachter nach § 44 Absatz 2 Satz 2 des Brandenburgischen Hochschulgesetzes auf Vorschlag der Graduate Research School.
6.  In Berufungskommissionen muss mindestens ein Mitglied der Gruppe der Hochschullehrerinnen und Hochschullehrer auch der Graduate Research School angehören.

### § 3  
Zentrale wissenschaftliche Einrichtungen

(1) Zur Unterstützung der Aufgabenerfüllung im Bereich von Lehre, Forschung, Studium und Weiterbildung und zur Verfolgung der Ziele des Bologna-Prozesses werden folgende zentrale wissenschaftliche Einrichtungen gebildet:

1.  ein Zentrum für Studierendengewinnung und Studienvorbereitung (College),
2.  eine „Undergraduate School“,
3.  eine „Professional School“ als Zentrum für Weiterbildung und
4.  eine „Graduate Research School“.

Sie wirken an der Profilbildung der Hochschule mit und bieten fakultätsübergreifende Vernetzungs-, Service- und Qualifizierungsangebote an.
Ihre Aufgaben können im Rahmen der staatlichen Zielsetzungen der Hochschulentwicklung regelmäßig aktualisiert werden.
Die Einrichtungen werden von einem Beirat, bestehend aus für die Zielsetzung des College und der Schools relevanten externen Expertinnen und Experten beratend unterstützt.
Die Organisation und Benutzung der genannten Einrichtungen wird durch Satzung geregelt.
Die Hochschullehrerinnen und Hochschullehrer nehmen Lehre in mindestens zwei Schools wahr.

(2) Das College fördert die Studierfähigkeit von Bewerberinnen und Bewerbern an der Schnittstelle zwischen Schule und Hochschule sowie die Studierneigung insbesondere in technischen Studiengängen.
Es fördert die Durchlässigkeit zwischen beruflicher und tertiärer Bildung und unterstützt Studierende in der Studieneingangsphase.

(3) Die Undergraduate School fördert die Internationalität und Interdisziplinarität des Studiums, die Anerkennung von Leistungen, die Mobilität der Studierenden und die Vermittlung notwendiger Schlüsselkompetenzen im Bachelorbereich.

(4) Die Professional School fördert die hochschulische Weiterbildung, die Berufsqualifizierung und Beschäftigungsfähigkeit der Studierenden, den Praxisbezug des Studiums und die Verankerung der Hochschule in der Region.
Die Professional School fördert die Vermittlung und Anerkennung berufsbezogener Kompetenzen und das Lebenslange Lernen.

(5) Die Graduate Research School fördert das interdisziplinäre und international ausgerichtete Arbeiten und Forschen, die Promotionsphase, die Verbindung mit dem internationalen und insbesondere Europäischen Forschungsraum und die Vermittlung forschungsbezogener Schlüsselkompetenzen.

(6) Die Qualifizierungsangebote der unter Absatz 1 Satz 1 Nummer 2 bis 4 genannten Einrichtungen sind als integrale Bestandteile des Studiums in die Studien- und Prüfungsordnungen aufzunehmen.
Die Präsidentin oder der Präsident, in der Gründungsphase die Gründungspräsidentin oder der Gründungspräsident, weisen diesen Einrichtungen angemessene Mittel und Personal zur Bewirtschaftung zu.
Die Angebote und die Zusammenarbeit zwischen den Schools und den organisatorischen Grundeinheiten werden miteinander abgestimmt.

### § 4  
Hochschulzugang

Abweichend von § 8 Absatz 2 Satz 2 letzter Halbsatz des Brandenburgischen Hochschulgesetzes berechtigt die Fachhochschulreife auch zu einem grundständigen Studium an der Brandenburgischen Technischen Universität Cottbus-Senftenberg

1.  in Studiengängen, für die die Brandenburgische Technische Universität Cottbus-Senftenberg dies durch Satzung entsprechend dem Profil der Studiengänge festlegt und
2.  in Studiengängen der Hochschule Lausitz (FH), die nach § 1 Absatz 2 fortgeführt werden.

### § 5  
Hochschulpersonal, Studierende, korporationsrechtliche Stellung

(1) Die im Landesdienst stehenden Beamtinnen und Beamten, Beschäftigten und Auszubildenden, die am 30. Juni 2013 an der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH) tätig waren, sind Beamtinnen, Beamte, Beschäftigte oder Auszubildende im Landesdienst an der Brandenburgischen Technischen Universität Cottbus-Senftenberg, es sei denn, das Dienst-, Beschäftigungs- oder Ausbildungsverhältnis endete mit diesem Tag.

(2) Das nebenberufliche wissenschaftliche und künstlerische Personal, das am 30.
Juni 2013 an der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH) tätig war, ist nebenberufliches Personal an der Brandenburgischen Technischen Universität Cottbus-Senftenberg, es sei denn, das Dienst- oder Beschäftigungsverhältnis endete mit diesem Tag.
Absatz 1 bleibt unberührt.

(3) Die an der Brandenburgischen Technischen Universität Cottbus und an der Hochschule Lausitz (FH) eingeschriebenen Studierenden einschließlich Juniorstudierenden und Promotionsstudierenden, Zweithörerinnen und Zweithörer sowie Gasthörerinnen und Gasthörer sind an der Brandenburgischen Technischen Universität Cottbus-Senftenberg eingeschrieben.

(4) Die bisherige mitgliedschaftsrechtliche und dienstrechtliche Stellung der Hochschulmitglieder und -angehörigen und Funktionsträgerinnen und Funktionsträger bleibt unberührt, soweit in diesem Gesetz nichts Abweichendes bestimmt wird.

### § 6  
Hauptberufliches wissenschaftliches und künstlerisches Personal

(1) Das nach § 5 übergeleitete Lehrpersonal führt seine bisherigen Dienstaufgaben in unverändertem Umfang fort.

(2) Nach § 5 Absatz 1 übergeleiteten Professorinnen oder Professoren der Hochschule Lausitz (FH), bei denen zusätzliche wissenschaftliche oder künstlerische Leistungen im Sinne des § 39 Absatz 1 Nummer 4 Buchstabe a des Brandenburgischen Hochschulgesetzes vorliegen, kann durch die Gründungspräsidentin oder den Gründungspräsidenten dauerhaft die Funktion einer Professorin oder eines Professors für andere als anwendungsbezogene Studiengänge im Sinne des § 39 Absatz 3 Satz 2 des Brandenburgischen Hochschulgesetzes übertragen werden, wenn nach dem Struktur- und Entwicklungsplan der Brandenburgischen Technischen Universität Cottbus-Senftenberg ein entsprechender Bedarf besteht.
Art und Umfang der Dienstaufgaben sind in diesem Fall solche einer Professorin oder eines Professors für andere als anwendungsbezogene Studiengänge im Sinne des § 39 Absatz 3 Satz 2 des Brandenburgischen Hochschulgesetzes.
Ob die Voraussetzungen des Satzes 1 vorliegen, stellt die Gründungspräsidentin oder der Gründungspräsident unter Einbeziehung von mindestens zwei Gutachten von auf dem Fachgebiet anerkannten, auswärtigen Wissenschaftlerinnen oder Wissenschaftlern oder Künstlerinnen oder Künstlern, die über die Einstellungsvoraussetzungen des § 39 Absatz 1 Nummer 4 Buchstabe a des Brandenburgischen Hochschulgesetzes verfügen, auf Vorschlag des nach der Grundordnung zuständigen Organs fest, welches die Gutachterinnen und Gutachter bestellt.
Dabei müssen in dem Organ die Professorinnen und Professoren, die die Einstellungsvoraussetzungen nach § 39 Absatz 1 Nummer 4 Buchstabe a des Brandenburgischen Hochschulgesetzes erfüllen, und die Juniorprofessorinnen und Juniorprofessoren, die sich nach § 44 Absatz 1 Satz 2 und Absatz 2 des Brandenburgischen Hochschulgesetzes bewährt haben, über die Mehrheit der Stimmen verfügen.
Das Nähere zu weiteren dienstpostenbezogenen Voraussetzungen sowie zum Verfahren regelt die Brandenburgische Technische Universität Cottbus-Senftenberg durch Satzung, die der Zustimmung der für die Hochschulen zuständigen obersten Landesbehörde bedarf.

(3) An der Brandenburgischen Technischen Universität Cottbus-Senftenberg werden je nach Anforderung der Stelle auch Professorinnen und Professoren für anwendungsbezogene Studiengänge im Sinne des § 39 Absatz 3 Satz 2 des Brandenburgischen Hochschulgesetzes eingestellt, die die Einstellungsvoraussetzungen nach § 39 Absatz 1 Nummer 4 Buchstabe a und b oder Buchstabe b des Brandenburgischen Hochschulgesetzes erfüllen.

(4) An der Brandenburgischen Technischen Universität Cottbus-Senftenberg können Professuren mit Schwerpunkt in der Lehre für Professorinnen und Professoren für andere als anwendungsbezogene Studiengänge im Sinne des § 39 Absatz 3 Satz 2 des Brandenburgischen Hochschulgesetzes sowie Professuren mit Schwerpunkt in der Forschung für Professorinnen und Professoren für anwendungsbezogene Studiengänge im Sinne des § 39 Absatz 3 Satz 2 des Brandenburgischen Hochschulgesetzes eingerichtet werden.
Die Anteile dieser Professuren an der Gesamtzahl der Stellen für Professorinnen und Professoren der Brandenburgischen Technischen Universität Cottbus-Senftenberg dürfen jeweils 20 Prozent nicht übersteigen.
Professuren mit Schwerpunkt in der Lehre können auch vorübergehend eingerichtet werden.
Im Übrigen gilt § 45 des Brandenburgischen Hochschulgesetzes.

(5) Professorinnen und Professoren für anwendungsbezogene Studiengänge im Sinne des § 39 Absatz 3 Satz 2 des Brandenburgischen Hochschulgesetzes, die über die Einstellungsvoraussetzungen nach § 39 Absatz 1 Nummer 4 Buchstabe a oder a und b des Brandenburgischen Hochschulgesetzes verfügen, können Dissertationen betreuen, wenn das Vorliegen der Einstellungsvoraussetzungen in einem Berufungsverfahren nachgewiesen wurde.
Für die übrigen Professorinnen und Professoren, die bis zum 30.
Juni 2013 an der Hochschule Lausitz (FH) tätig waren, gilt § 29 Absatz 6 Satz 3 und 4 des Brandenburgischen Hochschulgesetzes entsprechend, es sei denn, dass ihnen nach Absatz 2 dauerhaft die Funktion einer Professorin oder eines Professors für andere als anwendungsbezogene Studiengänge im Sinne des § 39 Absatz 3 Satz 2 des Brandenburgischen Hochschulgesetzes übertragen wurde.
Satz 1 gilt für die Mitwirkung an Habilitationsverfahren entsprechend.

### § 7  
Haushaltsrechtliche Zuweisung der Stellen und Mittel

Die für Hochschulen zuständige oberste Landesbehörde weist die der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH) zugewiesenen Planstellen, Stellen und Mittel der Brandenburgischen Technischen Universität Cottbus-Senftenberg nach den einschlägigen haushaltsrechtlichen Bestimmungen zu.

### § 8  
Leitung der Brandenburgischen Technischen Universität Cottbus-Senftenberg

(1) Die Amtszeit der Präsidenten sowie ihrer Vertreterinnen oder Vertreter und weiterer Mitglieder der zentralen Leitung der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH) ist zum 1.
Juli 2013 beendet.

(2) Die Brandenburgische Technische Universität Cottbus-Senftenberg wird für die Dauer von sechs Jahren von einer Gründungspräsidentin oder einem Gründungspräsidenten geleitet.
_Bis zu ihrer oder seiner Bestellung wird die Brandenburgische Technische Universität Cottbus-Senftenberg von einer oder einem oder mehreren durch das für Hochschulen zuständige Mitglied der Landesregierung zu bestellenden Beauftragten geleitet._[\*](#*)

(3) Die Gründungspräsidentin oder der Gründungspräsident wird vertreten durch eine hauptberufliche Vizepräsidentin oder einen hauptberuflichen Vizepräsidenten.
_Die Bestellung erfolgt nach Anhörung des Gründungssenats durch die Gründungspräsidentin oder den Gründungspräsidenten._[\*\*](#**) Amtszeit und Dienstverhältnis bestimmen sich nach § 64 des [Brandenburgischen Hochschulgesetzes.](/gesetze/bbghg)

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
\* § 8 Absatz 2 Satz 2 ist mit Artikel 5 Absatz 3 Satz 1 des Grundgesetzes unvereinbar (vgl.
Beschluss des Bundesverfassungsgerichts vom 12.
Mai 2015 - 1 BvR 1501/13, 1 BvR 1682/13 -)  
  
\*\* § 8 Absatz 3 Satz 2 ist wegen Verstoßes gegen Artikel 31 Absatz 1 der Verfassung des Landes Brandenburg nichtig (vgl.
Urteil des Verfassungsgerichts des Landes Brandenburg vom 25.
Mai 2016 - VfGBbg 51/15 -)

### § 9  
Gründungspräsidentin, Gründungspräsident

(1) Die Gründungspräsidentin oder der Gründungspräsident wird nach öffentlicher Ausschreibung der Stelle auf Vorschlag einer Findungskommission durch das für die Hochschulen zuständige Mitglied der Landesregierung im Einvernehmen mit dem erweiterten Gründungssenat bestellt.

(2) Die Findungskommission setzt sich aus je einer Vertreterin oder einem Vertreter der Mitgliedergruppen der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH), die von dem für die Wahl der Präsidentin oder des Präsidenten zuständigen Organ dieser Hochschulen gewählt werden, sowie einer Vertreterin oder einem Vertreter der für die Hochschulen zuständigen obersten Landesbehörde zusammen.
Die zentralen Gleichstellungsbeauftragten der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH) wirken mit beratender Stimme mit.
Die Vertreterin oder der Vertreter der für die Hochschulen zuständigen obersten Landesbehörde führt den Vorsitz in der Findungskommission.

(3) Der Vorschlag der Findungskommission kann bis zu drei Personen umfassen.
Er bedarf der Mehrheit ihrer Mitglieder und der Zustimmung der Vertreterinnen und Vertreter aus der Mitgliedergruppe der Hochschullehrerinnen und Hochschullehrer sowie der oder des Vorsitzenden.

(4) Die Gründungspräsidentin oder der Gründungspräsident muss die Einstellungsvoraussetzungen für Präsidentinnen oder Präsidenten nach dem Brandenburgischen Hochschulgesetz erfüllen.
Sie oder er soll nicht Mitglied oder Angehörige oder Angehöriger der Brandenburgischen Technischen Universität Cottbus oder der Hochschule Lausitz (FH) gewesen sein.

(5) § 63 des Brandenburgischen Hochschulgesetzes findet, soweit sich aus diesem Gesetz nichts Abweichendes ergibt, auf die Gründungspräsidentin oder den Gründungspräsidenten mit der Maßgabe Anwendung, dass es zu ihrer oder seiner Abwahl einer Mehrheit von drei Vierteln der Mitglieder des nach der Grundordnung zuständigen Organs bedarf.

### § 10  
Kanzlerin, Kanzler

Die Kanzlerin oder der Kanzler der Brandenburgischen Technischen Universität Cottbus ist Kanzlerin oder Kanzler der Brandenburgischen Technischen Universität Cottbus-Senftenberg.
Ihre oder seine Amtszeit endet mit dem Zeitpunkt, mit dem ihre oder seine Amtszeit an der Brandenburgischen Technischen Universität Cottbus geendet hätte.

### § 11  
Gründungspräsidium

(1) Zur Unterstützung der Gründungspräsidentin oder des Gründungspräsidenten wird ein Gründungspräsidium gebildet.
Das Gründungspräsidium besteht aus der Gründungspräsidentin oder dem Gründungspräsidenten, der hauptberuflichen Vizepräsidentin oder dem hauptberuflichen Vizepräsidenten, mindestens zwei weiteren Vizepräsidentinnen oder Vizepräsidenten und der Kanzlerin oder dem Kanzler.

(2) Die weiteren Vizepräsidentinnen oder Vizepräsidenten nach Absatz 1 werden vom Gründungssenat auf Vorschlag der Gründungspräsidentin oder des Gründungspräsidenten gewählt.
Jeweils mindestens eine Vizepräsidentin oder ein Vizepräsident sollen der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH) angehört haben.

(3) Das Nähere regelt die Grundordnung.
Die Geschäftsverteilung regelt die Gründungspräsidentin oder der Gründungspräsident.

### § 12  
Gründungssenat, erweiterter Gründungssenat

(1) Mit der Errichtung der Brandenburgischen Technischen Universität Cottbus-Senftenberg sind die Senate oder die an ihre Stelle getretenen zentralen Organe der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH) aufgelöst.
Die Brandenburgische Technische Universität Cottbus-Senftenberg wählt unverzüglich, spätestens bis zum 31.
Oktober 2013, einen Gründungssenat und einen erweiterten Gründungssenat.
Eine Doppelmitgliedschaft ist zulässig.

(2) Stimmberechtigte Mitglieder des Gründungssenats sind insgesamt 14 Vertreterinnen oder Vertreter der Mitgliedergruppen gemäß § 59 Absatz 1 Satz 3 des Brandenburgischen Hochschulgesetzes, von denen acht der Mitgliedergruppe der Hochschullehrerinnen und Hochschullehrer und je zwei den weiteren Mitgliedergruppen angehören.
Die Vertreterinnen oder Vertreter der Mitgliedergruppe der Hochschullehrerinnen und Hochschullehrer werden dabei je zur Hälfte von den Mitgliedern dieser Mitgliedergruppe aus der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH) getrennt gewählt.
Wahlberechtigte in der Mitgliedergruppe der Hochschullehrerinnen und Hochschullehrer, die am 30.
Juni 2013 noch nicht Mitglied der Brandenburgischen Technischen Universität Cottbus oder der Hochschule Lausitz (FH) waren, können selbst bestimmen, innerhalb welcher Teilgruppe der getrennt wählenden Mitglieder der Hochschullehrerinnen und Hochschullehrer sie die Stimme abgeben.

(3) Stimmberechtigte Mitglieder des erweiterten Gründungssenats sind insgesamt 31 Vertreterinnen oder Vertreter der Mitgliedergruppen gemäß § 59 Absatz 1 Satz 3 des Brandenburgischen Hochschulgesetzes, von denen 16 der Mitgliedergruppe der Hochschullehrerinnen und Hochschullehrer und je fünf den weiteren Mitgliedergruppen angehören.
Hinsichtlich der Vertreterinnen und Vertreter der Mitgliedergruppe der Hochschullehrerinnen und Hochschullehrer gilt Absatz 2 Satz 2 und 3.
Von den fünf Vertreterinnen und Vertretern der weiteren Mitgliedergruppen waren mindestens je zwei Vertreterinnen und Vertreter am 30.
Juni 2013 Mitglieder der Hochschule Lausitz (FH).

(4) Das Nähere zur Wahl und zur Stellvertretung der gewählten Vertreterinnen oder Vertreter der Mitgliedergruppen regelt eine Wahlordnung, die die Leitung der Brandenburgischen Technischen Universität Cottbus-Senftenberg erlässt.

(5) Der Gründungssenat und der erweiterte Gründungssenat wählen jeweils aus ihrer Mitte eine Vorsitzende oder einen Vorsitzenden.

(6) Die Vorsitzenden der Personalräte und die zentralen Gleichstellungsbeauftragten gehören dem Gründungssenat und dem erweiterten Gründungssenat mit beratender Stimme an.

(7) Die Amtszeiten des Gründungssenats und des erweiterten Gründungssenats enden mit der Wahl und Konstituierung der zentralen Organe der Brandenburgischen Technischen Universität Cottbus-Senftenberg nach Maßgabe der neuen Grundordnung gemäß § 15 Absatz 2.

### § 13  
Aufgaben des Gründungssenats

(1) Der Gründungssenat beaufsichtigt und berät die Gründungspräsidentin oder den Gründungspräsidenten in Bezug auf die Erfüllung ihrer oder seiner Aufgaben.
Zur Durchführung seiner Aufsicht hat er ein umfassendes Informationsrecht gegenüber der Gründungspräsidentin oder dem Gründungspräsidenten und bis zu deren oder dessen Bestellung gegenüber der oder dem oder den gemäß § 8 Absatz 2 Satz 2 Beauftragten.

(2) Der Gründungssenat ist insbesondere zuständig für

1.  die Entscheidung in grundsätzlichen Fragen der Lehre, der Forschung, des Studiums und der Prüfungen sowie der Förderung des wissenschaftlichen und künstlerischen Nachwuchses,
2.  die Entscheidung über den Struktur- und Entwicklungsplan gemäß § 3 Absatz 2 des Brandenburgischen Hochschulgesetzes mit Ausnahme des ersten Struktur- und Entwicklungsplans, über den der erweiterte Gründungssenat entscheidet,
3.  den Vorschlag an den erweiterten Gründungssenat für eine neue Grundordnung und
4.  die Entscheidung über die Vorschläge der organisatorischen Grundeinheiten für die Berufung von Professorinnen und Professoren; § 38 Absatz 5 Satz 3 des Brandenburgischen Hochschulgesetzes bleibt unberührt.

(3) Das Nähere bestimmt die Vorläufige Grundordnung.

### § 14   
Neuordnung

Bis zum 31.
Dezember 2014 ordnet die Brandenburgische Technische Universität Cottbus-Senftenberg ihre organisatorischen Grundeinheiten, die Fächerstruktur, die zentralen wissenschaftlichen Einrichtungen und Studiengänge sowie die Hochschulverwaltung nach Maßgabe dieses Gesetzes und den staatlichen Zielsetzungen der Hochschulentwicklung neu.

### § 15   
Vorläufige Grundordnung, Grundordnung

(1) Die für die Hochschulen zuständige oberste Landesbehörde erlässt für die Brandenburgische Technische Universität Cottbus-Senftenberg unverzüglich eine Vorläufige Grundordnung.

(2) Der erweiterte Gründungssenat beschließt auf Vorschlag des Gründungssenats bis zum 31. Dezember 2014 eine neue Grundordnung, auf deren Grundlage die Organe der Brandenburgischen Technischen Universität Cottbus-Senftenberg mit Ausnahme ihrer Leitung, die Gremien und Kommissionen sowie die Funktionsträgerinnen und Funktionsträger unverzüglich zu wählen oder zu bestellen sind.

(3) Bei Entscheidungen in Organen oder Gremien der Brandenburgischen Technischen Universität Cottbus-Senftenberg, die

1.  Habilitationen,
2.  die Berufung von Hochschullehrerinnen und Hochschullehrern, die die Voraussetzungen nach § 41 Absatz 1 Nummer 4 Buchstabe a des Brandenburgischen Hochschulgesetzes erfüllen müssen, oder
3.  die Bewährung von Juniorprofessorinnen und Juniorprofessoren als Hochschullehrerinnen und Hochschullehrer gemäß § 46 Absatz 1 Satz 2 und § 46 Absatz 2 des Brandenburgischen Hochschulgesetzes

unmittelbar betreffen, müssen die Professorinnen und Professoren, die die Einstellungsvoraussetzungen nach § 41 Absatz 1 Nummer 4 Buchstabe a des Brandenburgischen Hochschulgesetzes erfüllen und dies in einem Berufungsverfahren nachgewiesen haben, sowie die Juniorprofessorinnen und Juniorprofessoren, welche sich nach § 46 Absatz 1 Satz 2 und § 46 Absatz 2 des Brandenburgischen Hochschulgesetzes bewährt haben, über die Mehrheit der Stimmen verfügen.
Das Nähere bestimmen die Grundordnungen.

### § 16  
Gleichstellungsbeauftragte

Die zentralen und dezentralen Gleichstellungsbeauftragten der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH) und deren Stellvertreterinnen nehmen ihre Aufgaben bis zur Neuwahl nach der neuen Grundordnung gemeinsam wahr.

### § 17  
Weitere Gremien, Kommissionen und Funktionen;  
Rechtswirksamkeit von Entscheidungen bei fehlerhafter Wahl

(1) Die übrigen Gremien, Kommissionen und Funktionsträgerinnen und Funktionsträger der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH) sind Gremien, Kommissionen und Funktionsträgerinnen und Funktionsträger der Brandenburgischen Technischen Universität Cottbus-Senftenberg und bleiben bis zu ihrer jeweiligen Neuwahl infolge der Neuordnung der Brandenburgischen Technischen Universität Cottbus-Senftenberg gemäß § 14 oder der neuen Grundordnung gemäß § 15 Absatz 2 im Amt, es sei denn, die Gründungspräsidentin oder der Gründungspräsident trifft nach Anhörung des Gründungssenats aus Gründen, die in der Errichtung der Brandenburgischen Technischen Universität Cottbus-Senftenberg liegen, eine abweichende Regelung.

(2) Ist eine Wiederholungs- oder Neuwahl eines Organs oder Gremiums der Brandenburgischen Technischen Universität Cottbus-Senftenberg oder einzelner Mitglieder eines Organs oder Gremiums der Brandenburgischen Technischen Universität Cottbus-Senftenberg aufgrund einer rechtskräftigen Entscheidung erforderlich, so führt dieses Organ oder Gremium in der bisherigen Zusammensetzung die Geschäfte bis zum Zusammentreten des aufgrund der Wiederholungs- oder Neuwahl neugebildeten Organs oder Gremiums weiter.
Die Rechtswirksamkeit der Tätigkeit dieser Mitglieder bleibt vom Erfordernis der Wiederholungs- oder Neuwahl unberührt.
Satz 2 gilt bei einer fehlerhaften Besetzung von Organen oder Gremien entsprechend.

### § 18  
Studierendenschaft

(1) Die am 30.
Juni 2013 bestehenden Studierendenschaften der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH) bilden die Studierendenschaft der Brandenburgischen Technischen Universität Cottbus-Senftenberg.
Zur Rechtsnachfolge gilt § 21 Absatz 2 entsprechend.

(2) Bis zum 31.
Dezember 2013 werden die Organe der Studierendenschaft neu gewählt.

(3) Bis zu ihrer Neuwahl bestehen die Organe der Studierendenschaft aus den Mitgliedern der entsprechenden Organe der Studierendenschaften der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH).

(4) Das neugewählte oberste beschlussfassende Organ der Studierendenschaft beschließt unverzüglich eine Satzung der Studierendenschaft.
Bis zu deren Inkrafttreten setzt das oberste beschlussfassende Organ der Studierendenschaft in seiner ersten Sitzung die Satzung der Brandenburgischen Technischen Universität Cottbus oder der Hochschule Lausitz (FH) als Übergangssatzung in Kraft und trifft Übergangsregelungen, soweit solche erforderlich sind.

(5) Das am 30.
Juni 2013 vorhandene Vermögen der Studierendenschaften der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH) bildet mit Wirkung vom 1.
Juli 2013 das Vermögen der Studierendenschaft der Brandenburgischen Technischen Universität Cottbus-Senftenberg.

### § 19  
Personalräte

§ 32 des Landespersonalvertretungsgesetzes zur Neuwahl bei Umorganisation von Dienststellen und Körperschaften findet Anwendung.

### _§ 20_  
_Ersatzvornahme_

_Soweit Entscheidungen oder Maßnahmen der zuständigen Organe, Gremien, Kommissionen oder Funktionsträgerinnen und Funktionsträger nach diesem Gesetz nicht oder nicht fristgemäß getroffen werden, kann die für die Hochschulen zuständige oberste Landesbehörde anstelle der Brandenburgischen Technischen Universität Cottbus-Senftenberg nach deren Anhörung entscheiden oder anstelle der Brandenburgischen Technischen Universität Cottbus-Senftenberg das Erforderliche veranlassen._[\*](#* ) 

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
\* § 20 ist wegen Verstoßes gegen Artikel 32 Absatz 1 der Verfassung des Landes Brandenburg nichtig (vgl.
Urteil des Verfassungsgerichts des Landes Brandenburg vom 25.
Mai 2016 - VfGBbg 51/15 -)

### § 21  
Rechtsnachfolge

(1) Zum 1.
Juli 2013 gehen die Brandenburgische Technische Universität Cottbus und die Hochschule Lausitz (FH) mit den Standorten Cottbus und Senftenberg in der Brandenburgischen Technischen Universität Cottbus-Senftenberg auf.

(2) Die Brandenburgische Technische Universität Cottbus-Senftenberg ist ab dem Zeitpunkt ihrer Errichtung Rechtsnachfolgerin der Brandenburgischen Technischen Universität Cottbus und der Hochschule Lausitz (FH).

(3) Der Sitz der Verwaltung der Brandenburgischen Technischen Universität Cottbus-Senftenberg im Sinne von § 17 der Zivilprozessordnung ist in Cottbus.

### § 22  
Experimentierklausel

Das für die Hochschulen zuständige Mitglied der Landesregierung kann auf Antrag der Brandenburgischen Technischen Universität Cottbus-Senftenberg, zu dem der erweiterte Gründungssenat angehört worden ist, zur Erprobung neuer Modelle der Organisation und der Steuerung der Hochschule mit dem Ziel der Verbesserung der Leistungsfähigkeit, der Wettbewerbsfähigkeit und der Qualitätssicherung für eine begrenzte Zeit abweichende organisationsrechtliche Regelungen von den Bestimmungen der §§ 2, 3, 6 Absatz 2, 8, 9 sowie 11 bis 18 durch Rechtsverordnung treffen.
An die Stelle des erweiterten Gründungssenats tritt nach Erlass der Grundordnung nach § 15 Absatz 2 das dort bestimmte Organ.
Die für die Hochschulen zuständige oberste Landesbehörde unterrichtet den zuständigen Ausschuss des Landtages über den Vollzug dieser Bestimmung.

### Anlage

(zu § 2 Absatz 1)

### Empfehlungen zur Weiterentwicklung der Hochschulregion Lausitz

#### Auszug aus dem Abschlussbericht der Hochschulstrukturkommission des Landes Brandenburg (S. 355 bis 371)

#### Empfehlungen zur Struktur und zum Profil der Hochschulregion Lausitz

Im Ergebnis der Begutachtung beider Hochschulen und der Analyse ihrer Stärken und Schwächen kommt die Kommission zur Weiterentwicklung der Hochschulregion Lausitz zu dem Schluss, dass nur eine radikale Neustrukturierung und grundlegende Neuausrichtung in Lehre und Forschung den Bedarfen des Landes, der Region und der Hochschulen selbst gerecht wird.

Sie hat einen Strukturvorschlag erarbeitet, der zu einer Steigerung der Attraktivität beider Hochschulen in Lehre und Forschung führen kann, ein Studienangebot empfiehlt, das sowohl den Bedürfnissen der Unternehmen zur Fachkräftesicherung gerecht wird, gleichzeitig aber auch die Studiennachfrage berücksichtigt, eine dauerhafte Steigerung der FuE-Leistungen der Hochschulen fördert, die Kooperationsfähigkeit und -willigkeit zu den klein- und mittelständischen Unternehmen genauso verstärkt wie die zu den Großunternehmen und außeruniversitären Forschungseinrichtungen.

#### Zur Breite und Tiefe der notwendigen Kooperationen zwischen den Hochschulen

Die Kommission und mit ihr die unterstützenden Fachgutachtergruppen kommen im Ergebnis der Begutachtung zu dem Schluss, dass die Kooperationspotentiale beider Hochschulen bei weitem nicht ausgeschöpft werden.
Häufig scheint die Betonung von Unterschieden wichtiger als die Suche nach Gemeinsamkeiten zur Nutzung von Synergien.
Mit dem von der Mercator-Stiftung geförderten David-Gilly-Institut gibt es zwar erwähnenswerte Ansätze, die Abgrenzung zwischen den Hochschulen zu überwinden, diese gehen aber nach Auffassung der Kommission nicht weit genug.

Die Kommission empfiehlt eine stark intensivierte Zusammenarbeit bis hin zu gemeinsamen Fakultäten.
Darüber hinaus sollten die Hochschulen in allen zentralen Querschnittsbereichen zusammenarbeiten.
Besonders zu erwähnen sind die Bereiche der Förderung des wissenschaftlichen Nachwuchses, der wissenschaftlichen Weiterbildung sowie der Bereich der Studierendengewinnung und -vorbereitung.
Im Bereich der Hochschulverwaltung sollte ebenfalls eine deutlich intensivierte Zusammenarbeit angestrebt werden.
Hier ist zu prüfen, welche Bereiche künftig zusammengelegt werden können.

Die Expertenkommission schlägt der Ministerin vor, auf Basis des in Abbildung 1 dargestellten Schemas eine Struktur zu entwickeln, die es ermöglicht, Kooperationspotentiale voll auszunutzen, ohne die für das Land und die Region wichtigen charakteristischen Schwerpunkte und Besonderheiten beider Hochschultypen aufzugeben:

Beide Hochschulen haben trotz eines in Teilen überlappenden Fächerspektrums je eigene spezifische Aufgabenstellungen.
Von der Hochschule Lausitz (FH) werden als Fachhochschule Beiträge im Wissens- und Technologietransfer insbesondere für die regionale, zum überwiegenden Teil klein- und mittelständisch geprägte Wirtschaft erwartet.
Dies dokumentiert sich auch in vielen Klein- und Kleinstprojekten, die gemeinsam mit Unternehmen der Region durchgeführt werden.
Dieser Aufgabe kommt die Hochschule gegenwärtig nach Auffassung der Kommission und auch nach Auffassung der Wirtschaftsvertreter der Region, mit denen die Kommission gesprochen hat, in hervorragender Weise nach.
Der Hochschule Lausitz (FH) wird eine gute Einbindung in die Region bescheinigt.
Das zeigt sich auch in der Weise, in der die Fachhochschule die Nachfrage an Fachkräften vornehmlich für das mittlere Management in der Region bedient.

Von der BTU Cottbus werden dagegen insbesondere Antworten auf überregionale und internationale Fragestellungen erwartet.
Dafür muss die Forschungsstärke deutlich erhöht werden, was aus Sicht der Kommission nur mit einer starken Fokussierung aller Fakultäten, einschließlich der gemeinsamen Fakultäten, auf die Themen Energie und Umwelt gelingen kann.
Dies erscheint überdies notwendig, um sich besser als bisher als kooperationsfähiger Partner der Großunternehmen in der Region zu positionieren.
Gleiches gilt für die Kooperationsfähigkeit der BTU Cottbus mit außeruniversitären Forschungseinrichtungen, die für die Zukunft der Hochschule von großer Bedeutung sind.
Die aktuellen Kooperationsbeziehungen zu den außeruniversitären Forschungseinrichtungen sind stark ausbaufähig.

Beide Hochschulen bedienen Zielgruppen, die sich nur in Teilen überschneiden: So verfügen über 40 Prozent der Studienanfänger der Hochschule Lausitz (FH) nicht über eine allgemeine Hochschulzugangsberechtigung.
Neben den lösbaren rechtlichen Problemen des Hochschulzugangs sind damit spezifische individuelle Studienvoraussetzungen verbunden, die in Teilen eine andere Betreuung und Unterstützung erfordern.
Dieses Problem wird sich perspektivisch verschärfen, wenn die sinnvollen Bemühungen um eine stärkere horizontale und vertikale Durchlässigkeit des Bildungssystems greifen und vermehrt Studierende ohne traditionelle Hochschulzugangsberechtigung an die Hochschulen gelangen.
Aus Sicht der Kommission können fachhochschulische Angebote sehr viel stärker auf die spezifischen Bedürfnisse dieser Klientel eingehen.
Gerade für Jugendliche aus bildungsfernen Herkunftsgruppen scheint das Vorhandensein regionaler Angebote von hoher Bedeutung.
Fachhochschulische Angebote sind notwendig, um Aufstiegschancen zu eröffnen, hohe Bildungsbeteiligung zu gewährleisten und Abwanderung aus der Region zu verhindern.

Das von der Kommission vorgeschlagene Schema bedeutet dabei keine Zementierung von undurchlässigen „Bildungssäulen“.
Vielmehr soll die künftige Hochschulstruktur Kooperationen in Lehre und Forschung fördern und damit die Durchlässigkeit deutlich erhöhen.
Dem dienen nicht nur die gemeinsamen Fakultäten, sondern auch die zu schaffenden strukturübergreifenden Zentren.

#### Zur Grundstruktur

Die Kommission schlägt vor, die Kooperation beider Hochschulen deutlich auszubauen, gleichzeitig aber beide Hochschulen zu erhalten.
Im Folgenden wird zunächst ein Überblick über die Gesamtstruktur gegeben.

Beide Hochschulen sollten aus je drei eigenständigen Fakultäten bestehen.
Zusätzlich sollten zwei gemeinsame Hochschultyp-übergreifende Fakultäten geschaffen werden (vgl.
Abb.
1).

#### Abbildung 1: Strukturvorschlag für die Hochschulregion Lausitz

![Die Grafik stellt einen Strukturvorschlag für die Hochschulregion Lausitz dar: Zunächst sind nebeneinander für die BTU Cottbus und die Hochschule Lausitz (FH) die eigenständigen Fakultäten dargestellt. Für die BTU Cottbus links die Fakultät 1: Energie- und Umwelttechnik, die Fakultät 2: Informationstechnologien und Mathematik und die Fakultät 3: Maschinenbau und Materialwissenschaften; für die Hochschule Lausitz (FH) rechts die Fakultät 1: Ingenieurwissenschaften und Informatik, die Fakultät 2: Biotechnologie und Chemische Verfahrenstechnik und die Fakultät 3: Gesundheits- und Sozialwesen. Darunter befinden sich die hochschulübergreifenden Fakultäten Fakultät 4: Architektur und Bauingenieurwesen (Lausitz Gilly School of Architecture and Civil Engineering) und die Fakultät 5: Fakultät für Betriebswirtschaftslehre und Wirtschaftsingenieurwesen (Lausitz Business School).Darunter sind die hochschulübergreifenden Einrichtungen dargestellt: Das Lausitz Doktorandenkolleg, das Lausitz-Zentrum für Studierendengewinnung und Studienvorbereitung und - gemeinsam mit der regionalen Wirtschaft - das Lausitz-Zentrum für Weiterbildung (Lausitz Professional School).  ](/br2/sixcms/media.php/69/Nr.%204-%201.JPG "Die Grafik stellt einen Strukturvorschlag für die Hochschulregion Lausitz dar: Zunächst sind nebeneinander für die BTU Cottbus und die Hochschule Lausitz (FH) die eigenständigen Fakultäten dargestellt. Für die BTU Cottbus links die Fakultät 1: Energie- und Umwelttechnik, die Fakultät 2: Informationstechnologien und Mathematik und die Fakultät 3: Maschinenbau und Materialwissenschaften; für die Hochschule Lausitz (FH) rechts die Fakultät 1: Ingenieurwissenschaften und Informatik, die Fakultät 2: Biotechnologie und Chemische Verfahrenstechnik und die Fakultät 3: Gesundheits- und Sozialwesen. Darunter befinden sich die hochschulübergreifenden Fakultäten Fakultät 4: Architektur und Bauingenieurwesen (Lausitz Gilly School of Architecture and Civil Engineering) und die Fakultät 5: Fakultät für Betriebswirtschaftslehre und Wirtschaftsingenieurwesen (Lausitz Business School).Darunter sind die hochschulübergreifenden Einrichtungen dargestellt: Das Lausitz Doktorandenkolleg, das Lausitz-Zentrum für Studierendengewinnung und Studienvorbereitung und - gemeinsam mit der regionalen Wirtschaft - das Lausitz-Zentrum für Weiterbildung (Lausitz Professional School).  ")

Neben den gemeinsamen Fakultäten soll es mit dem „Lausitz Doktorandenkolleg“, dem „Lausitz-Zentrum für Studierendengewinnung und Studienvorbereitung“ und dem „Lausitz-Zentrum für Weiterbildung“ weitere Hochschultyp-übergreifende Einrichtungen geben, die allen Fakultäten beider Hochschulen offen stehen.

Die BTU Cottbus muss stärker überregional und international sichtbar werden.
Dies wird ihr nur gelingen, wenn sie in der Lage ist, ein unverwechselbares Profil aufzubauen, das herausragende Wissenschaftler und Studierende aus ganz Deutschland und verstärkt aus dem Ausland anzieht.
Die Kommission ist der Auffassung, dass sich die Hochschule zu einer ingenieurwissenschaftlich geprägten Themenuniversität entwickeln sollte, bei der „Energie und Umwelt“ in Verbindung mit den Bereichen Material, Bauen und Informationstechnologien die Thematik bestimmen.
Dieses klare Profil erfordert, dass sich alle Fakultäten der Universität mit ihren Forschungsarbeiten und Lehrangeboten diesen Themen unterordnen.

Die Weiterentwicklung zu einer „Universität für Energie und Umwelt“ scheint aus vielerlei Gründen erfolgversprechend:

_Potentiale sind vorhanden:_ Schon jetzt besitzt die BTU Cottbus sehr gut aufgestellte Lehrstühle, die sich mit Einzelaspekten des Themas befassen.
Zu nennen sind etwa die Lehrstühle des Instituts für Energietechnik: „Kraftwerkstechnik“, „Energieverteilung und Hochspannungstechnik“ sowie „Regelungssysteme und Leittechnik“.
Im Bereich der Umwelt sind vor allem die Lehrstühle für „Bodenschutz und Rekultivierung“, für „Gewässerschutz“ und für „Hydrologie und Wasserressourcenbewirtschaftung“ zu nennen.
Durch nicht besetzte Professuren und frei werdende Stellen gibt es zudem Spielräume für die Entwicklung neuer energieorientierter Schwerpunkte, z.B.
im Bereich „Erneuerbare Energien /Speichersysteme“.
Auch andere Fakultäten können wesentlich zum Profil beitragen.
Anknüpfungspunkte ergeben sich an vielen Stellen: So könnte zum Beispiel das bestehende Schwerpunktthema „Optimierung“ der Mathematik zu interdisziplinären Forschungskooperationen auch im Bereich Energie, insbesondere durch die vorgesehene Zusammenarbeit mit der Informationstechnik, führen.
Im Bereich des Bauens ergeben sich ebenfalls vielfältige Anknüpfungspunkte durch das Thema Energieeffizienz, und zwar auch innerhalb bestehender und positiv bewerteter Schwerpunkte, wie z.
B.
„Innovatives Planen und Bauen im Bestand“ durch klimagerechtes und energieeffizientes Bauen.

_Aufbau kritischer Massen:_ Mit einer Konzentration auf das Wesentliche und einer klaren Profilierung wird es der Hochschule leichter gelingen, kritische Massen aufzubauen, um im Bereich der Forschung wettbewerbs- und kooperationsfähiger zu werden.
_Internationaler Forschungs- und Fachkräftebedarf im Energiebereich:_ Der steigende Forschungs- und Fachkräftebedarf findet seinen Niederschlag in einer Reihe von Dokumenten: So zum Beispiel im 7.
und kommenden 8.
Forschungsrahmenprogramm der Europäischen Union oder in der durch die Bundesregierung ausgerufenen „Energiewende“.
Zudem steigt der Bedarf an hoch- und höchstqualifizierten Fachkräften.
Schon jetzt bearbeitet die BTU Cottbus einige der von der Bundesregierung definierten Kernthemen wie die Effizienzsteigerung konventioneller Kraftwerke oder die Netzsicherheit.
Aus der zunehmenden Bedeutung der Energie- und Umweltfragen ergeben sich vielfältige Chancen zur Einwerbung von Drittmitteln, die ihrerseits für die Steigerung der Forschungsstärke genutzt werden können.

_Energieland Brandenburg stärken:_ Brandenburg sieht sich selbst als Energieland.
Es gehört zu den Bundesländern mit dem größten Anteil an Erneuerbaren Energien.
Daneben spielen weiter konventionelle Kohlekraftwerke eine große wirtschaftliche Rolle.
Sie sind gegenwärtig noch unverzichtbar.
Brandenburg hat als Energieexporteur ein besonderes Interesse an allen Fragen der Energieforschung.
Das gilt für Brandenburg insgesamt, herausgehoben aber für die „Energieregion Lausitz“, die als Zusammenschluss von den vier südlichen Kreisen Brandenburgs und von Cottbus getragen wird.
Die BTU Cottbus sollte zu einem unverzichtbaren Ansprechpartner Brandenburgs in allen Energiefragen werden.

_Kooperationsfähigkeit mit der strukturprägenden Wirtschaft stärken:_ Gespräche mit den relevanten Unternehmen haben gezeigt, dass es vielfältige Steigerungsmöglichkeiten bei den Kooperationsvorhaben gibt.
Allerdings verfüge die BTU Cottbus gegenwärtig in vielen Fällen noch nicht über das notwendige „Know How“ für den Ausbau der Forschungskooperationen.

_Kooperationsfähigkeit mit anderen (außeruniversitären) Forschungseinrichtungen stärken:_ Mit einer qualitativ hochwertigen Forschung im Bereich Energie und Umwelt entwickelt sich die Universität stärker als bisher zu einem möglichen Kooperationspartner anderer auch außeruniversitärer Forschungseinrichtungen in Brandenburg und darüber hinaus.
Zudem können perspektivisch Anreize für die Ansiedlung außeruniversitärer Forschungseinrichtungen entstehen.

Für die BTU Cottbus wird eine Struktur mit drei eigenständigen Fakultäten vorgeschlagen:

Fakultät 1: Fakultät für Energie- und Umwelttechnik

Das Institut für Energietechnik sollte den Kern des Themenschwerpunkts „Energie“ der künftigen Fakultät bilden.
Kern des Themenkomplexes „Umwelt“ sollten die Lehrstühle „Bodenschutz und Rekultivierung“, „Gewässerschutz“ und „Hydrologie und Wasserressourcenbewirtschaftung“ sein.
Das Bindeglied sollte aus der Umwelt- und Verfahrenstechnik entwickelt werden, wobei ein neuer Schwerpunkt im Bereich der Erneuerbaren Energien aufgebaut werden sollte.

Fakultät 2: Fakultät für Informationstechnologien und Mathematik

In dieser Fakultät sollten die Mathematik, die Informatik sowie die Informations- und Medientechnik der bisherigen Fakultät 1 mit der Elektronik und der Informationstechnik der bisherigen Fakultät 3 zusammengefasst werden.
Die Mathematik sollte durch den Bereich Technomathematik, Modellierung und Simulation gestärkt werden.

Fakultät 3: Fakultät für Maschinenbau und Materialwissenschaften

Diese Fakultät sollte aus den Instituten für Verkehrstechnik sowie Werkstoff- und Produktionsforschung bestehen und durch die materialwissenschaftlich ausgerichteten Lehrstühle der Physik verstärkt werden.

Die Hochschule Lausitz (FH) soll ebenfalls drei eigenständige Fakultäten umfassen.
Wo immer es sinnvoll ist, sollte die Hochschule Lausitz (FH) mit der BTU Cottbus kooperieren und Bezüge zu den Themen Energie und Umwelt suchen.
Eine ausschließliche Ausrichtung auf diese Thematik erscheint jedoch nicht sachangemessen.
Deshalb sollte die Hochschule in den drei Fakultäten auch ihre bereits gut etablierten fachlichen Schwerpunkte weiter stärken bzw.
herausbilden:

Fakultät 1: Fakultät für Ingenieurwissenschaften und Informatik

Fakultät 2: Fakultät für Biotechnologie und Chemische Verfahrenstechnik

Basis ist die aktuelle Fakultät für Naturwissenschaften.
Die bisherige Bezeichnung kann anhand der disziplinären Ausrichtung der vorhandenen Professuren nicht überzeugen.
Mit der neu vorgeschlagenen Bezeichnung soll die herausragende Stellung der Biotechnologie an der Hochschule Lausitz (FH) betont werden.

Fakultät 3: Fakultät für Gesundheits- und Sozialwesen

Basis für die neu zu schaffende Fakultät sind das Sozialwesen und das Fachgebiet Musikpädagogik.
Unterstützt wird zudem die Idee, weitere fachliche Schwerpunkte im Bereich Medizinpädagogik/Pflegepädagogik, Gesundheitsmanagement/ Pflegemanagement sowie Gesundheits- und Pflegewissenschaft aufzubauen.

Alle eigenständigen Fakultäten der Hochschule Lausitz (FH) sollten am Standort Senftenberg angesiedelt werden.

Neben den jeweils drei eigenständigen Fakultäten wird empfohlen, zwei Hochschultyp-übergreifende Fakultäten in den Bereichen zu schaffen, in denen es bereits gegenwärtig große inhaltliche Schnittmengen gibt.
Diese beiden Fakultäten sollten am Standort der BTU in Cottbus konzentriert werden.

Fakultät 4: Fakultät für Architektur und Bauingenieurwesen (Lausitz Gilly School of Architecture and Civil Engineering)

In dieser Fakultät sollten die Bereiche Architektur, Bauingenieurwesen und Stadtplanung erhalten und gestärkt werden.
Die Stärke und Alleinstellung der bisherigen Fakultät 2 der BTU Cottbus mit ihrem „Cottbuser Modell“, das auf einer engen Verzahnung der drei beteiligten Fachgebiete beruht, sollte weiter ausgebaut werden.
Die bisherige Zusammenarbeit im David-Gilly-Institut umfasst bislang nur das Bauingenieurwesen und ist damit nicht weitgehend genug.
Die Konzentration auf das Bauingenieurwesen gefährdet geradezu die Cottbuser Alleinstellungsmerkmale.

Fakultät 5: Fakultät für Betriebswirtschaftslehre und Wirtschaftsingenieurwesen (Lausitz Business School)

Im Bereich des Wirtschaftsingenieurwesens haben beide Hochschulen je eigene Stärken entwickelt, die es auch in einer gemeinsamen Fakultät zu erhalten gilt.
Anders sieht es im Bereich der Betriebswirtschaftslehre aus, die nach „herrschender Meinung“ eine angewandte Wissenschaft ist, die sich anspruchsvoller Methoden nicht nur bedienen kann, sondern muss, um wohl begründete Handlungsempfehlungen formulieren zu können.
Hier erscheint die bisherige Abgrenzung zwischen beiden Hochschulen künstlich und unnötig.

Die Kommission spricht sich für die Schaffung von drei weiteren gemeinsamen Einrichtungen aus:

Lausitz Doktorandenkolleg

Mit dem Lausitz Doktorandenkolleg sollte eine hochschulübergreifende „Graduate School“ geschaffen werden, die spezielle Angebote zur Förderung des wissenschaftlichen Nachwuchses unterbreitet.
Sie steht mit ihren Angeboten dem gesamten wissenschaftlichen Nachwuchs beider Hochschulen offen.

Lausitz-Zentrum für Studierendengewinnung und Studienvorbereitung

In diesem Zentrum sollten die Arbeiten beider Hochschulen in den Bereichen Studierendenwerbung, Steigerung der Studierneigung, Studienvorbereitung und Diversity-Management gebündelt werden.

Lausitz-Zentrum für Weiterbildung

Im Bereich der Weiterbildung sollten die Hochschulen Weiterbildung in allen Bereichen zu einem starken eigenständigen Profilmerkmal entwickeln, das viele Facetten im Bereich des Lebenslangen Lernens berücksichtigt.
Hierbei sollte eine fest institutionalisierte Wirtschafts-Wissenschafts-Kooperation mit finanzieller Beteiligung der Wirtschaft angestrebt werden.

Die Kommission spricht sich dafür aus, die gemeinsamen Fakultäten am Standort der BTU in Cottbus zu konzentrieren.
Die drei eigenständigen Fakultäten der Hochschule Lausitz (FH) sollten in Senftenberg angesiedelt werden.
Im Zuge dieser Neuordnung sollte geprüft werden, den Standort Cottbus der Hochschule Lausitz (FH) aufzugeben.

#### Zur Umsetzung der neuen Hochschulstruktur

Aus Sicht der Kommission sind die gegenwärtigen Strukturen nicht geeignet, die notwendige Neuaufstellung der beiden Lausitzer Hochschulen zu gewährleisten.
Die Kommission setzt sich daher dafür ein, den Prozess der Neukonzeption der Hochschulen und der neu zu etablierenden Fakultäten auf Basis der hier vorgestellten Vorschläge auf allen Stufen intensiv durch ein fest institutionalisiertes Peer Review-Verfahren zu begleiten.

Die vom Ministerpräsidenten des Landes eingesetzte Hochschulstrukturkommission wird hierzu allgemeine Vorschläge unterbreiten.

#### Wichtigste Aussagen und Empfehlungen zur Studiennachfrage und zum Studienangebot

Beide Hochschulen hatten und haben mit zum Teil erheblichen Auslastungsproblemen zu kämpfen.
Dabei spielen fraglos auch die eingangs genannten strukturellen Probleme seit Gründung der Hochschulen eine Rolle.
Hierzu gehören die geographische Randlage und Berlinferne sowie die in Teilen ähnlichen Studienangebote beider Hochschulen.
Diese Entwicklung wird durch die Bolognareform mit ihren einheitlichen Studienabschlüssen noch einmal stärker akzentuiert.

Mit ihrer technisch-naturwissenschaftlich orientierten Ausrichtung bewegen sich beide Hochschulen überdies in einem Feld, in dem es bundesweit eine eher schwache Nachfrage gibt.

Die Hochschulen sind zudem in besonderem Maße von der demographischen Entwicklung bedroht, die Brandenburg genauso betrifft wie die übrigen neuen Länder.
Zwar gibt es keinen unmittelbaren Zusammenhang zwischen der Studiennachfrage und der demographischen Entwicklung, doch ist das Risiko eines „Durchschlagens“ der demographischen Entwicklung auf die Studiennachfrage umso größer, je stärker regionalisiert das Einzugsgebiet der Hochschulen ist.
Die Hochschule Lausitz (FH) ist die Hochschule Brandenburgs mit dem größten Anteil Studierender aus Brandenburg und den übrigen neuen Ländern.
Auch die BTU Cottbus fiel lange Zeit durch einen überproportional hohen Anteil an Studierenden aus den neuen Ländern auf.
Es ist ihr erst in den letzten Jahren gelungen, vermehrt Studierende, vor allem aus Berlin, anzuwerben.

Eine vom HIS-Institut für Hochschulforschung für das Wissenschaftsministerium erarbeitete Studierendenvorausberechnung kommt zu der für Brandenburg insgesamt positiven Einschätzung, dass es dem Land gelingen kann, sich weitgehend von der demographischen Entwicklung zu entkoppeln.
Das bedeutet, dass es trotz des massiven Rückgangs an Schulabsolventen mit einer Hochschulzugangsberechtigung nur zu leichten Rückgängen bei den Studienanfängern kommen wird.
Zum Zeitpunkt der Verabschiedung dieser Empfehlungen lagen die regionalisierten Vorausberechnungen noch nicht vor.

Die Kommission geht vor dem Hintergrund der genannten Bedingungen in der Lausitz davon aus, dass es nicht gelingen wird, die Studierendennachfrage bei den Hochschulen dauerhaft zu steigern.
Eine Straffung des deutlich zu stark diversifizierten Ausbildungsangebots beziehungsweise ein teilweiser Rückbau von Studienplatzkapazitäten innerhalb bestehender Studiengänge erscheint daher angeraten.
Zudem gibt es bei einer Reihe von infrage stehenden Studienangeboten fachliche Erwägungen jenseits rein kapazitärer Überlegungen, die Änderungen in den Studienangeboten nahe legen.
Fachlich-inhaltliche oder auch qualitative Überlegungen können aber dazu führen, dass in Einzelfällen die Fortführung von Studienangeboten auch bei anhaltender Unterauslastung empfohlen wird.
Die gilt z.B.
für die Biotechnologie, der von den Gutachtern ein exzellentes Niveau bescheinigt wurde.
Ihre Absolventen sind stark nachgefragt.
Zudem werden von der Biotechnologie wirtschaftliche Effekte in der Region erwartet, welche die „Kosten der Unterauslastung“ bei weitem übersteigen.

Darüber hinaus legt die Kommission wert auf die Feststellung, dass der Abbau von Studiengängen oder die Verringerung von Studienplatzkapazitäten nicht automatisch größere finanzielle Ressourcen freisetzt.
Teile der frei werdenden Mittel sollten für neu aufzubauende hoch innovative Studiengänge an den Lausitzer Hochschulen eingesetzt werden.

In Stichworten werden folgende kapazitätsrelevante Veränderungen bei den Studiengängen empfohlen:

_Mathematik/Informatik/Natur- und Ingenieurwissenschaften/Umweltwissenschaften_

Die mathematischen Studiengänge an der BTU Cottbus sollten zugunsten der Verbesserung der Qualität der Serviceleistungen der Mathematik aufgegeben werden.

Die Grundlagenausbildung in der Mathematik beider Hochschulen sollte zusammengelegt werden, wobei die spezifischen Studienvoraussetzungen der Studierenden der Hochschule Lausitz (FH) zu beachten sind.

Die physikalischen Studiengänge der BTU Cottbus sollten auslaufen.

Die Kerninformatik sollte trotz (auch bundesweit) schwacher Nachfrage bei gleichzeitig stärkerer Konzentration auf die Informations- und Medientechnik sowie auf das E-Business erhalten werden.

Insgesamt erscheint den Gutachtern im Bereich von Mathematik, Physik, Informations- und Kommunikationstechnologien sowie in den „klassischen“ Ingenieurfächern eine Reduktion bei den Studiengängen und den Studienplatzkapazitäten um 20 Prozent zugunsten von leistungsstarken, innovativen Fächern möglich.

Die existierenden Schwerpunkte der BTU Cottbus in den Fachgebieten Maschinenbau und Elektrotechnik sollten bei gleichzeitigem Abbau von Randstudiengängen erhalten werden.

Schaffung gemeinsam von den Kernfakultäten der BTU Cottbus getragener Studiengänge zum Thema „Energieeffizienz“, ggf.
gemeinsam mit der hochschulübergreifenden Fakultät für Architektur und Bauingenieurwesen und relevanten Bereichen der Hochschule Lausitz (FH).

Im Bereich Umweltwissenschaften und Verfahrenstechnik sollte die Zahl der angebotenen Studiengänge auf je drei bis vier Studiengänge im BA- und MA-Bereich reduziert werden („Environmental and Ressource Management“, „Landnutzung und Wasserressourcenbewirtschaftung“, „Umweltingenieurwesen, Verfahrenstechnik“)

Die hohe Bedeutung des Energiethemas für die BTU Cottbus sollte sich auch in der Einrichtung eines Studiengangs „Energietechnik/Energiesysteme“ spiegeln.

An der Hochschule Lausitz (FH) sollte der BA-Studiengang „Chemie“ aufgrund fraglicher Berufschancen und zu geringer Nachfrage zugunsten eines Studiengangs „Chemische Verfahrenstechnik“ aufgegeben werden.
Der neue Studiengang sollte eng mit der Biotechnologie verbunden werden und an das bisherige Chemieingenieurwesen anknüpfen.

_Architektur, Stadt- und Regionalplanung, Bauingenieurwesen_

Im Bereich der Architektur und des Bauingenieurwesens sollte die Zahl der Masterstudiengänge reduziert werden.
„Architekturvermittlung“ und „Bauen und Erhalten“ sollten nur als Vertiefungsrichtungen angeboten werden.
Im Bereich des Bauingenieurwesens sollten die technischen Grundlagen gestärkt werden.
Für die Architektur wird ein zurückgehender Bedarf an universitär ausgebildeten Architekten gesehen; die Kapazitätsplanung sollte sich daran ausrichten.
Die Situation im Bauingenieurwesen ist angesichts der geringen Auslastung langfristig nicht tragfähig.
Kräfte sollten gebündelt werden.
Inhaltlich ist eine Verstärkung notwendig.

Die Studiengänge „Klimagerechtes Bauen“ und „Architektur“ der Hochschule Lausitz (FH) erscheinen ohne zusätzliche personelle und infrastrukturelle Ausstattung langfristig nicht tragfähig.
Diese Situation kann sich in einer gemeinsamen Fakultät beider Hochschulen durch die Nutzung von Synergien deutlich verbessern.

Im Ergebnis der Einzelbetrachtungen beider Hochschulen wird für den Baubereich eine Konzentration der Stärken der BTU Cottbus und der Hochschule Lausitz (FH) unter Beibehaltung der drei Disziplinen Architektur, Stadtplanung und Bauingenieurwesen empfohlen.
Die Verzahnung der Fachgebiete sollte trotz möglicher Kapazitätsreduktionen erhalten bleiben, wobei ein enger Bezug zum Kernprofil der BTU Cottbus „Energie und Umwelt“ herzustellen ist.

_Wirtschaftswissenschaften_

Der Studiengang E-Business könnte zu einer Vertiefungsrichtung innerhalb der Informatik oder der Betriebswirtschaftslehre werden, die Notwendigkeit eines eigenen Studiengangs ist nicht ersichtlich.

Die Zusammenarbeit in den Studiengängen Betriebswirtschaftslehre und Wirtschaftsingenieurwesen ist nur gering ausgeprägt.
Tendenzen zur der nicht nachvollziehbaren gegenseitigen Abgrenzung sind offensichtlich.
Im Bereich Wirtschaftsingenieurwesen haben die Hochschulen gut bewertete eigenständige Angebote erarbeitet, die unter dem Dach einer gemeinsamen Fakultät weiterentwickelt werden sollten.
Im Gegensatz dazu erscheint die bisherige Abgrenzung im Bereich der Betriebswirtschaftslehre nicht sinnvoll.
Hier sollten die Angebote beider Hochschulen zusammengeführt werden

_Soziale Arbeit/Gesundheitswesen_

Der Aufbau von neuen Angeboten an der Hochschule Lausitz (FH) im Bereich des Gesundheitswesens mit den fachlichen Schwerpunkten Medizinpädagogik/Pflegepädagogik, Gesundheitsmanagement/Pflegemanagement sowie Gesundheits-/Pflegewissenschaften wird begrüßt.
Die hierzu bestehenden Überlegungen der Landesregierung entsprechen dem künftigen Versorgungsbedarf.
Kritisch gesehen werden Überlegungen der Hochschule zur Einrichtung eines Studiengangs „Bewegung und Gesundheit“.
Es sollte bei der Einführung eines grundständigen Studiengangs „Physiotherapie“ in Ergänzung zum bisherigen Modell bleiben.

#### Wichtigste Empfehlungen zu den Fakultäten in der neu zu schaffenden Struktur  
BTU Cottbus Fakultät 1: „Energie- und Umwelttechnik“

Die Fakultät 1 spielt innerhalb der neuen Struktur eine zentrale Rolle bei der angestrebten Profilierung.
Sie soll Teile der bisherigen Fakultäten 3 und 4 vereinen.
Der Kern der Fakultät für den künftigen BTU-Schwerpunkt „Energie“ in Forschung und Lehre sollte vom Institut für Energietechnik gebildet werden.
Der Schwerpunkt „Umwelt“ sollte um die Lehrstühle Bodenschutz und Rekultivierung, Geopedologie und Landschaftsentwicklung, Hydrologie und Wasserressourcenbewirtschaftung sowie Gewässerschutz entwickelt werden.
Diese sollten eng mit relevanten Arbeitsrichtungen einer neu aufzustellenden Umwelttechnik verbunden werden.
Die prozessorientierte physikalische, chemische und mikrobiologische Ausrichtung sollte erhalten sowie deren ingenieurwissenschaftliche Umsetzung verstärkt werden.

Insgesamt sollte der Bereich „Umwelttechnik“ der künftigen Fakultät durch Neuberufungen so mit der „Energietechnik“ zusammengeführt werden, dass das „Energiesystem“ insgesamt behandelt werden kann und ein zusätzlicher Schwerpunkt „Erneuerbare Energien/Speichertechnologien“ entsteht.
Dabei ist die sogenannte „Tiefe Geothermie“ einzubeziehen und eine Geoengineering-Komponente (Erschließung und Nutzung des Untergrundes) aufzubauen.

Die Teile der Institute für Umweltmanagement, Umwelttechnik und Verfahrenstechnik, die sich in der Vergangenheit nicht gut etablieren konnten, sollten für notwendige Umstrukturierungen genutzt werden.

Im Bereich der Lehre sollte die Fakultät einen Studiengang „Energietechnik/Energiesysteme“ aufbauen.
Dies erhöht die Sichtbarkeit des Profils und trägt dazu bei, Studierende in diesem wichtigen FuE-Gebiet auszubilden und damit auch den eigenen wissenschaftlichen Nachwuchs zu generieren.

Der Neuaufbau der Fakultät, für den ein detailliertes Konzept noch zu erarbeiten ist, sollte mit einem Peer Review-Verfahren begleitet werden.

#### BTU Cottbus Fakultät 2: „Informationstechnologien und Mathematik“

Die Fakultät 2 „Informationstechnologien und Mathematik“ sollte die Lehrstühle des Lehr- und Forschungsbereichs Mathematik sowie das Institut für Informatik der bisherigen Fakultät 1 umfassen und durch die Lehrstühle des Instituts für Elektronik und Informationstechnik der bisherigen Fakultät 3 abgerundet werden.
Die Zusammenfassung dieser Fachgebiete in einer Fakultät führt zu kritischen Massen, die auch die gemeinsame Planung und Durchführung von Verbundforschungsprojekten ermöglichen.
Diese Neukonfiguration erlaubt, die von der Kommission für gut befundene Vision der Cottbuser Informatiker umzusetzen und ein Zentrum für Informatik, Medien- und Kommunikationstechnik zu entwickeln.
Hiervon erhofft sich die Kommission auch Impulse für die dringend notwendige Stärkung der Elektronik und Informationstechnik.

Die Mathematik sollte sich mit ihrer Forschung stärker in Projekte der ingenieurwissenschaftlichen Fachgebiete einbringen.
Um dem gerecht zu werden, ist eine Erweiterung der mathematischen Fachrichtungen um einen Bereich Technomathematik, Modellierung und Simulation erforderlich.
Das Schwerpunktthema „Optimierung“ ist aus Sicht der Kommission gut gewählt und könnte Basis für eine vertiefte Forschungskooperation in den Bereichen Energie, Material- und Produktionstechnik werden.

Für den Erfolg der BTU Cottbus insgesamt ist es notwendig, dass sich die Fakultät 2 den Schwerpunktthemen Energie und Umwelt widmet.
Aus Sicht der Kommission gibt es dafür eine Reihe vielversprechender Möglichkeiten, z.
B.
die Einrichtung eines gemeinsamen Studiengangs zum Thema „Energieeffizienz“.

Dafür kann auf die Studiengänge der Mathematik verzichtet werden, die seit Jahren unbefriedigende Absolventenzahlen aufweisen.
Im Bereich der Grundlagenausbildung sollte die Zusammenarbeit mit der Hochschule Lausitz (FH) gesucht werden.
Die Lehrleistung ist in Senftenberg zu erbringen.
Dabei sind die für die Studierenden einer Fachhochschule typischen Studienvoraussetzungen zu beachten.

Aus Sicht der Kommission sollte die Kerninformatik trotz einer geringen Auslastung aufrechterhalten werden.
Wichtig erscheint eine zusätzliche Ausrichtung auf die Informations- und Medientechnik sowie auf E-Business.
Der Ausbau der Informatik sollte auf den für Universitäten notwendigen Mindestausbaustand (ca.
11 Professuren) begrenzt werden.

#### BTU Cottbus Fakultät 3: „Maschinenbau und Materialwissenschaften“

Die Fakultät 3 sollte aus den bisherigen Instituten für Verkehrstechnik und Produktionsforschung bestehen.
Darüber hinaus wird eine Zuordnung der Physik in die Fakultät empfohlen.
Die einzelnen Forschungsschwerpunkte der Physik sind in die der Elektrotechnik und der Werkstofftechnik/Produktionsforschung einzubinden.
Eine Fokussierung auf materialwissenschaftliche Fragestellungen erscheint angeraten, ebenso wie die Zuordnung des Themas „Katalytische Umwandlung von CO2„zum Institut für Energietechnik.

Im Zuge der Neugründung der Fakultät sind gemeinsame Forschungsschwerpunkte der Fakultät zu entwickeln, die sowohl dem anzustrebenden Gesamtprofil der Universität als auch den bestehenden, in Teilen herausragenden Forschungsprojekten Rechnung tragen.
Dabei ist auch die fakultätsübergreifende Zusammenarbeit zu suchen.
Zu nennen ist hier insbesondere die Kooperation mit der Angewandten Mathematik, die zu einer stärkeren Berücksichtigung von Simulationsmethoden und Modellbildung in der Forschung führen kann.

Die Fakultätsneugründung sollte auch dazu führen, dass sich die Verkehrstechnik, wie bereits vom Wissenschaftsrat 2002 empfohlen, sehr viel stärker auf ausgewählte Forschungsprojekte konzentriert.
Grundsätzlich ist vor dem Hintergrund der zum Teil zu geringen Forschungsleistungen sowie vor dem Hintergrund der Schwerpunkte der TU Dresden zu prüfen, ob alle Lehrstühle der Verkehrstechnik dauerhaft erhalten bleiben sollten.
Darüber hinaus ist auch hier einzufordern, dass besonders national und international erfolgreiche Bereiche, zu denen z.B.
der Lehrstuhl „Aerodynamik und Strömungslehre“ zählt, in ein Gesamtprofil der Fakultät eingebunden werden.
Dabei ist auch an interdisziplinäre Ansätze zu denken, etwa in den Bereichen Aerodynamik, Leichtbau und Energieeinsparung.
Eine stärkere Ausrichtung auf anwendungstechnische Probleme könnte zudem auch die Zusammenarbeit mit der (regionalen) Industrie fördern.

Der Studiengang Maschinenbau wird von der Kommission in Übereinstimmung mit dem CHE-Ranking positiv bewertet.
Dringenden Verbesserungsbedarf sieht die Kommission allerdings bei der deutlich zu hohen Abbrecherquote.

Eigenständige Physikstudiengänge sollten in der neuen Fakultätsstruktur nicht fortgeführt werden.
Hier ist angesichts der über viele Jahre deutlich zu geringen Absolventenzahl nicht erkennbar, wie eine nachhaltige Verbesserung in der Auslastung erreicht werden könnte.

#### Hochschule Lausitz (FH) Fakultät 1: „Ingenieurwissenschaften und Informatik

Die Fachgebiete Informatik, Elektrotechnik und Maschinenbau sind durchweg gut in die Region eingebunden.
Diese Stärke gilt es nicht nur zu erhalten, sondern auszubauen, zumal sich hierdurch eine Steigerung der Drittmitteleinnahmen für alle drei Fachgebiete erreichen lässt.

Im Bereich des Maschinenbaus verfügt die Fakultät über einige besonders erfolgreiche Forschungsgruppen, wie FEM/CFD Modellierung für strukturmechanische und fluiddynamische Untersuchungen (vier Drittmittelbeschäftigte), Fördertechnik (vier Drittmittelbeschäftigte) und Fertigungstechnik (zehn Drittmittelbeschäftigte).

Die Initiative zur Gründung eines In-Instituts „Energieoptimierter Standort“ zeigt ein hohes Maß an Interdisziplinarität und eine ausgeprägte Kooperationsbereitschaft und wird daher von der Kommission begrüßt.
Bei einem erfolgreichen Aufbau des Instituts könnten sich langfristig Kooperationsmöglichkeiten mit der BTU Cottbus und insbesondere mit der gemeinsamen Fakultät 4 „Architektur und Bauingenieurwesen“ ergeben.

Das Forschungsprofil der Informatik ist auch im Vergleich mit den anderen Ingenieurbereichen schwach ausgeprägt und sollte gestärkt werden.
Hiervon verspricht sich die Kommission eine weitere Verbesserung der bereits gut etablierten Kooperation mit der Industrie.

Die Fakultät muss sich insgesamt bemühen, mehr Studienanfänger zu gewinnen und die Studienerfolgsquote zu erhöhen.

Unabhängig von den im Bereich der Forschung kaum vorhandenen Anknüpfungspunkten für eine Kooperation mit der BTU Cottbus müssen die Hochschulen in der Grundlagenausbildung, insbesondere in der Mathematik, zusammenarbeiten.
Hier sind die Kapazitäten der BTU Cottbus auch in Senftenberg zu nutzen.
Eine ausschließliche Öffnung der Lehrangebote der BTU Cottbus für Studierende der Hochschule Lausitz reicht dabei nicht nur wegen der räumlichen Entfernung beider Standorte nicht aus, sondern sie würde auch die spezifischen Studienvoraussetzungen der Studierenden der Hochschule Lausitz nicht berücksichtigen, die häufig über keine Allgemeine Hochschulzugangsberechtigung verfügen.

Die Medizintechnik sollte in der Fakultät verankert und durch Stellenumwidmungen gestärkt werden.
Dies befördert die als zukunftsweisend angesehene Zusammenarbeit der Medizintechnik mit der Physiotherapie, die auch durch die Einbeziehung weiterer Professuren aus den Ingenieurwissenschaften gefördert werden sollte.

Schließlich spricht sich die Kommission dafür aus, das Sozialwesen, die Musikpädagogik, den zu entwickelnden Pflege- und Gesundheitsbereich sowie die Medizintechnik und Physiotherapie mit einem innovativen Konzept zu verknüpfen.

#### Hochschule Lausitz (FH) Fakultät 2: „Biotechnologie und Chemische Verfahrenstechnik

Die Fakultät 2, die aus der bisherigen „Fakultät für Naturwissenschaften“ gebildet wird, hat einen hohen wissenschaftlichen Anspruch, der, wie bereits ausgeführt, nicht durchgehend erfüllt wird.
Deshalb ist trotz dieser im Grundsatz positiv zu bewertenden Entwicklung darauf hinzuweisen, dass die Forschungsorientierung nicht zu Lasten der originär anwendungsbezogenen Ausbildung an Fachhochschulen gehen sollte.
Der Hochschule Lausitz (FH) wird dringend geraten, die Balance zwischen einer praxisbezogenen Ausbildung und einer mehr forschungsorientierten Ausbildung mit universitärem Anspruch zu wahren.

Innerhalb der Fakultät ist die Biotechnologie noch einmal besonders herauszuheben.
Die von ihr gewählten Schwerpunkte in Technischer Mikrobiologie, Enzymtechnologie, Zellbiologie, Molekularbiologie und vor allem auch in der Bioanalytik führten bereits zu erfolgreichem Technologietransfer mit Produktentwicklungen und Firmengründungen.
Weitere Projekte in der Forschung sind inzwischen soweit vorangeschritten, dass konkrete Planungen zu Ausgründungen bestehen.
Das auf dem Campus geplante Innovationszentrum wird in diesem Zusammenhang von der Kommission sehr begrüßt, weil es helfen wird, die Anbindung der ausgegründeten Unternehmen an die Hochschule zu sichern.

Die beiden Biotechnologie-Studiengänge sollten auf dem hohen Niveau fortgeführt werden.
Sie überzeugen in ihrer Struktur und Ausrichtung.
Dabei sollten weitere forschungsstarke Fachkollegen aus den Naturwissenschaften in die Ausbildung zur Aufrechterhaltung der hohen Qualität einbezogen werden.

Schon jetzt ist sehr anerkennenswert, wie den Spitzenstudierenden Promotionsmöglichkeiten mit verschiedenen Universitäten in Deutschland vermittelt werden.
In diesem Gebiet ergibt sich aber weitergehender Handlungsbedarf.
So sollte überlegt werden, die Doktoranden in der Biotechnologie in einem Graduiertenkolleg zusammenzufassen, um sie zeitgemäß ausbilden zu können.
Dem aufzubauenden hochschulübergreifenden „Lausitz-Doktorandenkolleg“ kommt dabei eine wesentliche Bedeutung zu, weil es auch einen Beitrag zur Verbesserung der Betreuungssituation des wissenschaftlichen Nachwuchses der Hochschule Lausitz (FH) leisten soll.

Das Konzept des kürzlich eingerichteten Chemiestudiengangs konnte die Kommission nicht überzeugen.
Der Hochschule Lausitz (FH) fehlt das für eine Chemieausbildung nötige Fächerspektrum.
Zudem werden die Arbeitsmarktaussichten der BA-Absolventen eher skeptisch beurteilt.
Die Kommission schlägt daher vor, anstelle des bisherigen Studiengangs „Chemie“ einen Studiengang „Chemische Verfahrenstechnik“ zu konzipieren.
Ein solcher Studiengang führt das Chemieingenieurwesen mit der Chemischen Verfahrenstechnik zusammen und schafft Anknüpfungspunkte zur Biotechnologie.

Aus dem Genannten ergibt sich die Empfehlung zur Umbenennung der Fakultät in „Fakultät für Biotechnologie und Chemische Verfahrenstechnik“.
Mit der neuen Bezeichnung soll die Bedeutung der Biotechnologie für das künftige Profil hervorgehoben und als Markenzeichen im Fakultätsnamen verankert werden.
Die „Chemische Verfahrenstechnik“ wiederum hebt klarer auf die Stärke eines spezifischen fachhochschulischen Angebots ab.

#### Hochschule Lausitz (FH) Fakultät 3: „Gesundheits- und Sozialwesen

Diese Fakultät 3, die vollständig in Senftenberg angesiedelt werden sollte, setzt sich aus bewährten und neuen Teilen zusammen.
Zu den bewährten Teilen zählt das Sozialwesen, das bereits inhaltlich und konzeptionell der zukünftigen demografischen und sozialstrukturellen Entwicklung der Region Rechnung trägt.
Es wird begrüßt, dass zusätzlich zum Grundthema „Jugend und Familie“ das Thema „Alter“ behandelt wird.
Hier sollte allerdings noch das wichtige neue Berufsfeld „Arbeit mit alten Menschen“ hinzugefügt werden.

Die „Musikpädagogik“ muss ungeachtet ihrer sehr guten Arbeit stärker mit anderen Fachgebieten der Hochschule verknüpft werden.
Das gilt insbesondere für die Zusammenarbeit mit der labormäßig hervorragend ausgestatteten Physiotherapie, die es ermöglichen würde, angehenden Musikpädagogen spezielles Wissen und spezielle Diagnostiken über Fehlhaltungen am Instrument bzw.
die Optimierung von motorischen Prozessen beim Erwerb instrumenteller Fähigkeiten zu vermitteln.
Umgekehrt kann auch die Musikpädagogik ihre Kompetenzen in andere Studiengänge einbringen.
Hier ist an den Studiengang „Soziale Arbeit“ im Hinblick auf die Arbeit mit Kindern und Jugendlichen als auch mit Senioren genauso zu denken, wie an die Nutzungsmöglichkeiten im Gesundheitswesen.
Es wird nachdrücklich empfohlen, die Musikpädagogik stärker zu integrieren und nicht als Solitär mit Ausnahmestellung zu betrachten.

Das gilt auch und gerade im Rahmen der vorgeschlagenen deutlichen Stärkung der Fakultät durch den Aufbau von Angeboten in der Medizin- und Pflegepädagogik, dem Gesundheits- und Pflegemanagement sowie in den Gesundheits- und Pflegewissenschaften.
Die Gutachter unterstützen ausdrücklich die entsprechenden Pläne der Landesregierung für den Aufbau der genannten Angebote.
Als besonders prospektiv wird dabei der Anspruch des Landes an eine qualifizierte Lehrerbildung in den Gesundheitsbereichen auf Master-Ebene betrachtet.

Die Kommission ist der Überzeugung, dass die Voraussetzungen für die Einrichtung der neuen Angebote im Gesundheits- und Pflegebereich sowohl in der Hochschule als auch in der Region mit entsprechenden Kooperationspartnern gegeben sind, zumal derartige Angebote keine Anbindung an die Spitzenmedizin erfordern.

Als wichtig für die weitere Entwicklung der Fakultät wird auch die Stärkung der Internationalisierung angesehen.
Eine Auseinandersetzung mit sozialen, medizinischen und gesundheitlichen Problemen in anderen Gebieten der Welt würde die Attraktivität des Fachgebiets für ausländische Studierende steigern.

#### Fakultät 4: „Architektur und Bauingenieurwesen

Die Studiengänge Architektur und Bauingenieurwesen gehören zu den tradierten und wissenschaftspolitisch wichtigen Fachangeboten des Hochschulstandorts Cottbus.
Sie sollten als tragende Säulen für die weitere Entwicklung der Region auch künftige Ausbildungsschwerpunkte bleiben.

Die Entwicklungsaufgaben in der Region erfordern ein starkes Bauwesen am Ort, das getragen und unterstützt wird von der Ausbildung und Qualifizierung junger Menschen, die in der Region die dort vorliegenden Probleme kennen und durch ihren Verbleib in der Region das erforderliche Wissen für die Zukunftsfähigkeit des Standorts bereitstellen.
Neben der Qualifizierung von Fachkräften durch Aus- und Weiterbildung stellt die Forschungs- und Entwicklungsleistung von Hochschulen eine Stützung der regionalen Wirtschaft dar und erhöht die Attraktivität des Standorts für Zukunftstechnologien.

Vor dem Hintergrund der Größe der Region, ihres universitären Konkurrenzumfelds (Dresden, Berlin) und der demografischen Entwicklung ist aber die parallele Aufrechterhaltung der „Baufächer“ an der BTU Cottbus und der Hochschule Lausitz (FH) auf einem entwicklungs- und wettbewerbsfähigen Niveau nicht zu empfehlen.

Vielmehr ist eine Konzentration der Stärken der BTU Cottbus und der Hochschule Lausitz (FH) vorzunehmen, um auf diese Weise die unbedingt zu erhaltenden Disziplinen Architektur, Bauingenieurwesen und Stadtplanung attraktiver und zukunftssicher zu machen.
Bei dieser Umstrukturierung, die bereits von beiden Institutionen mit der Gründung des David-Gilly-Instituts begonnen wurde, werden Kapazitäten frei, die z.T.
für eine Schwerpunktbildung genutzt werden könnten.
Aus Sicht der Kommission von zentraler Bedeutung ist, dass die jetzt an der BTU Cottbus vorhandene starke Verzahnung von Architektur, Bauingenieurwesen, Stadtplanung und bauhistorischer Forschung auch in der neuen gemeinsamen Fakultät erhalten bleibt.
Die jetzige Form des David-Gilly-Instituts mit seiner ausschließlichen Konzentration auf das Bauingenieurwesen gefährdet das „Cottbuser Modell“.

Für die Bau-Fachgebiete könnten gemeinsame Zukunftsthemen aus dem Komplex „Stadt/Stadtversorgung/Energie/Bauen im Bestand/Technische Netze/Regionale Strukturen“ entwickelt werden.
Der Fokus sollte auf die regionalen Fragen der schrumpfenden und kleinen Städte, ihre energetische Sanierung und den Umbau im Bestand sowie auf Zukunftsfragen der südbrandenburgischen Region und hier insbesondere auf die wichtige Neugestaltung von Kulturlandschaften im direkten Umfeld gerichtet werden.

Ein besonderes Potential zur Profilentwicklung liegt in weitergehenden Kooperationen mit anderen Fachgebieten, wie zum Beispiel den Umweltwissenschaften der BTU Cottbus oder der Energietechnik der Hochschule Lausitz (FH) in Senftenberg.

Im Bereich der Architektur und Planung sind die forschungsstarken Gebiete (Geschichte, Bauforschung, Denkmalpflege, Stadttechnik, Bauökonomie) zu stärken.
Auf wissenschaftlich kaum tragfähige Modethemen wie „Architekturkommunikation“ sollte verzichtet werden.

Bei dem personellen Neuaufbau des Bauingenieurwesens sollte der Bereich Wasserbau wieder integriert werden.
Der Materialbereich (Werkstoffe des Bauwesens) ist neu aufzubauen.

Die angebotenen Studiengänge sind insgesamt zu weitgehend diversifiziert.
Die Profilstruktur durch BA- und MA-Programme im Bauwesen ist für einen kleinen Hochschulstandort nicht zukunftsfähig.
Breiter angelegte Studienangebote mit Wahlangeboten erlauben, die sehr positiv angesehene Einbeziehung der Architektur und der Regionalplanung in der Ausbildung der Bauingenieure fortzuführen und zu intensivieren.
Gemeinsame Lehrveranstaltungen für Bauingenieure und Architekten werden schon gehalten und sollten weiterhin auch bereits im Bachelorstudium durchgeführt werden.

Das im gegenwärtigen „David-Gilly-Institut“ angelegte „X-Modell“ mit jeweils zwei BA- und MA-Studienangeboten sollte in allen Baufächern angestrebt werden.
Es berücksichtigt die unterschiedlichen Bedarfe und Interessen der Studierenden und der Wirtschaft und kann dazu beitragen, die Durchlässigkeit zwischen den Hochschultypen zu erhöhen.
Allerdings erscheint das „X-Modell“ in der Umsetzung noch nicht ausgereift.

#### Fakultät 5: „Betriebswirtschaftslehre und Wirtschaftingenieurwesen

Wie im Bereich des Bauwesens gibt es auch im Bereich der Betriebswirtschaftslehre und des Wirtschaftsingenieurwesens ähnliche Studienangebote an beiden Hochschulen.
Die von den Hochschulen vorgetragenen Begründungen für eine bisher schwach ausgeprägte Zusammenarbeit können nicht überzeugen und scheinen eher Ergebnis eines fehlenden Kooperationswillens zu sein.

Die Kommission empfiehlt, eine gemeinsame Hochschultyp-übergreifende Fakultät zu schaffen.
Eine rein virtuelle Aufstellung ist in keinem Fall ausreichend.
Im akademischen Bereich dieser gemeinsamen Fakultät muss fachbezogen eine enge hochschulübergreifende Zusammenarbeit verwirklicht werden, z.B.
durch gemeinsame Institute, gemeinsame Masterstudienangebote, Mitwirkung an Berufungsverfahren und Promotionsordnungen, die eine Beteiligung von Fachhochschulprofessoren an Promotionsverfahren und den verlässlichen Zugang geeigneter Fachhochschulabsolventen zur Promotion sichern.

Die neu zu gründende Fakultät muss wie die anderen Fakultäten auch ihren Beitrag zur Profilierung der Hochschulen leisten.
Hierzu muss sie ein eigenständiges Konzept für Forschungs- und Lehrschwerpunkte entwickeln, das sich auf die Kernthemen der Lausitzer Hochschulen bezieht.

Im Bereich der Betriebswirtschaftslehre ist das Lehrprofil mit seinen Wahlpflichtfächern, stärker als es bisher an der BTU Cottbus der Fall ist, an den Forschungsschwerpunkten der Lehrenden auszurichten, um eine enge Verbindung von Lehre und Forschung zu erreichen.

Eine gemeinsame Neuaufstellung ist auch im Bereich des Wirtschaftsingenieurwesens notwendig, dabei sind Kapazitätsanpassungen zu prüfen, da die Studiengänge zuletzt nicht immer ausgelastet waren.
Zudem sollten sich die Studienrichtungen nicht an den bisherigen Schwerpunktsetzungen der BTU im Bereich des Wirtschaftsingenieurwesens orientieren.

#### Lausitz-Doktorandenkolleg

Das Lausitz-Doktorandenkolleg kann ein wichtiges Instrument werden, um die Versäulung des Hochschulsystems aufzubrechen.
Mit diesem Kolleg werden den Doktoranden der Hochschule Lausitz (FH), systematisch und institutionell abgesichert, Wege in eine adäquate Betreuung an der Universität eröffnet.

Für besonders zentral hält es die Kommission, dass den geeigneten Doktoranden, insbesondere aus dem Fachgebiet Biotechnologie, Qualifikationswege in der Lausitz eröffnet werden, um ihnen eine wissenschaftliche Karriere in der Region zu ermöglichen und die Fakultäten in die Lage zu versetzen, eigenen wissenschaftlichen Nachwuchs zu generieren.

Die Hochschulstrukturkommission des Landes wird Modelle prüfen und geeignete Instrumente vorschlagen, welche Promotionen an der BTU Cottbus ermöglichen.

Das zu entwickelnde Konzept für das Lausitz-Doktorandenkolleg sollte auch Instrumente enthalten, die begabten Studierenden in FH-Masterprogrammen einen leichteren Übergang in ein Promotionsvorhaben ermöglichen.

Bei der Gesamtkonzeption sind bewährte Instrumente der Nachwuchsförderung zu übernehmen.

#### Lausitz-Zentrum für Studierendengewinnung und Studienvorbereitung

Für die Zukunft der Lausitzer Hochschulen sind eine hohe Attraktivität, ein klares, überregional sichtbares Forschungsprofil und eine hervorragende Lehre notwendige aber nicht hinreichende Bedingungen.
Die Hochschulen müssen aktiv um Studierende werben und sich insbesondere auch um die verstärkte Gewinnung ausländischer Studierender bemühen.
Hierzu gibt es bereits eine Reihe attraktiver Maßnahmen der beiden Hochschulen, die in einem gemeinsamen Zentrum zu bündeln sind.

Die Hochschulen sollten sich auch weiterhin an den landesweiten Projekten zur Steigerung der Studierneigung beteiligen.
Hier kann eine gemeinsame starke Einrichtung zwei kleinere ersetzen.

Angesichts der demographischen Entwicklung mit dem Rückgang an Schulabsolventen, der sich bereits in Brandenburg und den anderen neuen Ländern überdeutlich zeigt, aber auch in den alten Ländern in nicht allzu ferner Zukunft absehbar ist, gilt es verstärkt, alle Bildungspotentiale zu nutzen.
Die Erhöhung der Durchlässigkeit ist hierbei ein wichtiger Aspekt.
Wesentliche Weichenstellungen, die den Hochschulzugang auch ohne eine traditionelle Hochschulzugangsberechtigung ermöglichen, sind vorgenommen worden.
Es ist nun auch an den Hochschulen, diese Potentiale vermehrt zu nutzen.
Das stellt sie aber vor neue große Herausforderungen: Je heterogener die Studierendenzusammensetzung ist, desto schwieriger ist es, die Lehrangebote an die Bedürfnisse der Studierenden anzupassen, ohne das Abschlussniveau zu gefährden.
Mit dem Lausitz-Zentrum für Studierendengewinnung und Studienvorbereitung wird eine gemeinsame Einrichtung vorgeschlagen, die Programme für diese wachsende Zielgruppe entwickeln und umsetzen kann.
Hier sollten auch die bereits bestehenden Vorbereitungskurse gebündelt und ausgebaut werden.
Auch ist zu überprüfen, ob entsprechend qualifizierte Lehrende für diese Aufgaben gewonnen werden sollen.
Auf jeden Fall ist eine enge Abstimmung der Lehrinhalte mit den einzelnen Fakultäten und Studiengängen zwingend notwendig.

#### Lausitz-Zentrum für Weiterbildung

Die demographische Entwicklung, die bis 2020 zu einem Rückgang des Anteils der erwerbsfähigen Bevölkerung um ca.
16 Prozent führen wird, erfordert nicht nur, die Durchlässigkeit formal und real zu ermöglichen, sondern auch verstärkt und systematisch wissenschaftliche Weiterbildung anzubieten.
Die Kommission ist überzeugt, dass Lebenslanges Lernen und wissenschaftliche Weiterbildung zu einem strategischen Handlungsfeld der Hochschulen in ganz Deutschland werden wird.
Die Implementierung erfolgreicher Maßnahmen zur Gewinnung, Entwicklung und Bindung von Mitarbeitern wird für Unternehmen zu einer zentralen Führungsaufgabe.
Hierin liegt nicht nur eine neue Herausforderung, sondern gerade auch für die Hochschulen in der Lausitz eine große Chance.
Angebote zur wissenschaftlichen Weiterbildung können langfristig helfen, die vorhandenen Kapazitäten zum Wohle des Landes und der Region auszulasten und damit auch die Unternehmen vor Ort zu fördern.
Der Bedarf ist vorhanden, aber noch nicht systematisch erfasst.

Die Potentiale von Bildungspartnerschaften zwischen Wissenschaft und Wirtschaft sind generell in Deutschland nicht ausgeschöpft.
In der Region Lausitz könnte eine modellhafte Kooperation entstehen.
Möglichkeiten der regionalen Zusammenarbeit mit Bildungseinrichtungen des Hochschulbereichs sollten geprüft werden, insbesondere im Bereich der berufsbegleitenden Bachelor-Studiengänge.

Die Weiterbildungsangebote sind so zu gestalten, dass sie überregional attraktiv und nutzbar werden.
Das erfordert eine flexible Gestaltung und insbesondere möglichst kurze, auf das notwendige Maß beschränkte Präsenzphasen.
Der Vermittlungsprozess sollte auf E-Learning-Modulen aufbauen und sich Lernplattformen bedienen.
Für beides ist an der BTU Cottbus eine Infrastruktur vorhanden, die auch für Weiterbildungszwecke eingesetzt werden kann.
Wichtig dabei ist allerdings, dass Lernende im Netz sehr gut betreut werden.
Dafür ist eine ausreichende Personalkapazität einzuplanen.

Es bedarf einer klaren Verpflichtung der beiden Hochschulen zum strategischen Ausbau des Handlungsfeldes Lebenslanges Lernen/Weiterbildung und der systematischen Einbettung der Weiterbildung und der Aktivitäten des Lebenslangen Lernens in die jeweilige Entwicklungsstrategie der Hochschulen.
Dazu muss jeweils ein interner Organisationsentwicklungs-Prozess in Gang gesetzt werden, an dem auch forschungsstarke Professuren beteiligt werden.
Für die strategische Entwicklung der Weiterbildung müssen die Hochschulleitungen die Verantwortung übernehmen.
Gleichzeitig bedarf es aber auch eines entsprechenden Engagements der Wissenschaftler in der Entwicklung und Umsetzung von Weiterbildungsangeboten; ihr Mitwirken ist eine Grundvoraussetzung für die Qualität und damit den langfristigen und nachhaltigen Erfolg in diesem Bereich.

Beide Hochschulen müssen weitere, eigenständige Entwicklungsarbeit (Strategie, Gesamtkonzeption inkl.
Finanzierungsmodell, Organisationsstruktur, Organisationsentwicklung, Akzeptanz des Arbeitsfeldes, bedarfsgerechte Produktgestaltung sowie ziel- und kundenorientierte Vermarktung, etc.) im Bereich der Weiterbildung leisten, um auf diesem Gebiet wettbewerbsfähig zu werden.

Bezüglich der aufzubauenden Struktur für die Weiterbildung sollte ein Konzept erarbeitet werden, das hinsichtlich seines Innovationspotentials bundesweit Beachtung findet.
Es wird empfohlen, dass sich die beiden Hochschulen gemeinsam mit Akteuren aus Wirtschaft und öffentlicher Hand als gleichberechtigte Partner dazu verständigen, eine gemeinsame Plattform zu schaffen, die das Thema Weiterbildung aktiv aufgreift.

Eine institutionalisierte Wirtschaft-Wissenschaft-Kooperation könnte ein Modellprojekt werden und aufzeigen, wie ein intensiver Dialog zwischen Hochschulen und regionaler Wirtschaft aufgebaut und zur strategischen Weiterentwicklung einer ganzen Region genutzt werden kann.

Das neue gemeinsame Weiterbildungszentrum soll der Institutionalisierung einer strategischen Partnerschaft zwischen Hochschulen und Unternehmen dienen und damit die Weiterbildungsbeziehungen in einen größeren Kontext stellen.

Als gemeinsame Aufgaben ergeben sich die Entwicklung von: auf die Region und deren Bedürfnisse ausgerichteten Weiterbildungsangeboten, speziell auf die KMU zugeschnittenen Angeboten, berufsbegleitenden Bachelor-Studiengängen und dualen Studiengängen sowie einzelnen internationalen Studiengängen (beispielsweise zum Schwerpunktthema Energie) mit Strahlkraft und Reputation.
In enger Zusammenarbeit mit der Wirtschaft ist eine Grundabnahme im Rahmen der Personalentwicklungsarbeit der Unternehmen sicherzustellen.
Dabei sind Weiterbildung und Wissenstransfer zu verknüpfen und die Möglichkeiten gemeinsamer FuE-Projekte zu nutzen.

Für ein derartiges Modellprojekt ist die Bereitstellung von angemessenen Finanzmitteln erforderlich, die sich aus den Beiträgen der Kooperationspartner und aus Fördermitteln zusammensetzen.
Die am Begutachtungsprozess beteiligten Vertreter der Wirtschaft haben ihre inhaltlich-konzeptionelle Unterstützung für eine derartige gemeinsame Weiterbildungsplattform zugesagt.
Eine darüber hinaus gehende finanzielle Unterstützung und Beteiligung durch die Wirtschaft, ist zwischen den Partnern im Rahmen der Konzepterarbeitung zu diskutieren.
Eine zusätzliche Möglichkeit der Finanzierung könnten länderübergreifende EU-Fördermittel darstellen.

#### Fazit

Die Kommission sieht trotz schwieriger Rahmenbedingungen gute Entwicklungspotentiale für die Hochschulregion Lausitz.
Um sie ausschöpfen zu können, müssen die beiden Hochschulen sich auf wenige klar benennbare Schwerpunkte konzentrieren.
Andere kleinere Technische Universitäten belegen, dass dies erfolgreich gelingen kann.
Aufgrund der in Teilen bereits vorhandenen Stärken der Hochschulen und der günstigen regionalen und überregionalen Trends bietet die fakultätsübergreifende Konzentration auf die Themen Energie und Umwelt besondere Chancen.
Für die Hochschule Lausitz (FH) eröffnen sich durch die Kooperation mit der BTU Cottbus völlig neue Entwicklungschancen.
Allerdings darf sie dabei ihre Stärken in der regionalen Einbindung und im Praxisbezug ihrer Studienangebote nicht aus den Augen verlieren.

Voraussetzung für den Erfolg ist, dass bei allen Beteiligten ein Prozess des Umdenkens einsetzt.
Der Erfolg wird nur miteinander zu erreichen sein.
Aufgrund der bisherigen Erfahrungen wird den Hochschulen und dem Ministerium für Wissenschaft, Forschung und Kultur geraten, den angedachten Entwicklungsprozess in allen Teilschritten durch bewährte Peer Review-Verfahren zu begleiten.