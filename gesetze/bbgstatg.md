## Gesetz über die Statistik im Land Brandenburg (Brandenburgisches Statistikgesetz - BbgStatG)

Der Landtag hat das folgende Gesetz beschlossen:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeine Bestimmungen

[§ 1 Geltungsbereich](#1)

[§ 2 Grundsätze der Landesstatistik und der Kommunalstatistik](#2)

### Abschnitt 2  
Amt für Statistik Berlin-Brandenburg

[§ 3 Aufgaben des Amtes für Statistik Berlin-Brandenburg](#3)

[§ 4 Zusammenarbeit und Vergabe statistischer Arbeiten](#4)

[§ 5 Register](#5)

### Abschnitt 3  
Anordnung von Statistiken

[§ 6 Landesstatistiken](#6)

[§ 7 Kommunalstatistiken](#7)

[§ 8 Anforderungen an Landes- und Kommunalstatistiken](#8)

[§ 9 Erprobung](#9)

### Abschnitt 4  
Kommunale Statistikstellen, Erhebungsstellen, Erhebungsbeauftragte

[§ 10 Kommunale Statistikstellen](#10)

[§ 11 Erhebungsstellen](#11)

[§ 12 Erhebungsbeauftragte](#12)

### Abschnitt 5  
Durchführung von Statistiken

[§ 13 Abschottung](#13)

[§ 14 Erhebungs- und Hilfsmerkmale](#14)

[§ 15 Trennung und Löschung der Hilfsmerkmale](#15)

[§ 16 Auskunftspflicht](#16)

[§ 17 Geheimhaltung und Übermittlung von Einzelangaben](#17)

[§ 18 Unterrichtung und andere Rechte](#18)

[§ 19 Statistische Aufbereitung von Daten aus dem Verwaltungsvollzug](#19)

[§ 20 Kosten](#20)

### Abschnitt 6  
Reidentifizierungsverbot, Strafvorschrift, Ordnungswidrigkeiten, Vollzug gegen Behörden und juristische Personen des öffentlichen Rechts

[§ 21 Reidentifizierungsverbot](#21)

[§ 22 Strafvorschrift](#22)

[§ 23 Ordnungswidrigkeiten](#23)

[§ 24 Vollzug gegen Behörden und juristische Personen des öffentlichen Rechts](#24)

### Abschnitt 7  
Schlussbestimmungen

[§ 25 Verordnungsermächtigungen](#25)

[§ 26 Einschränkung von Grundrechten](#26)

[§ 27 Übergangsvorschrift](#27)

[§ 28 Inkrafttreten, Außerkrafttreten](#28)

### Abschnitt 1  
Allgemeine Bestimmungen

#### § 1 Geltungsbereich

(1) Dieses Gesetz gilt für die Vorbereitung, Erhebung und Aufbereitung (Durchführung) von Landesstatistiken und Kommunalstatistiken sowie für die statistische Aufbereitung von Daten aus dem Verwaltungsvollzug (Geschäftsstatistiken).

(2) Für die Durchführung von Statistiken aufgrund unmittelbar geltender Rechtsakte der Europäischen Union (Statistiken der Europäischen Union) und aufgrund von Rechtsvorschriften des Bundes (Statistiken für Bundeszwecke) finden die Vorschriften dieses Gesetzes ergänzend Anwendung.

(3) Öffentliche Stellen im Sinne dieses Gesetzes sind

1.  die Behörden, Gerichte und Einrichtungen des Landes einschließlich der Landesbetriebe,
2.  die der Aufsicht des Landes unterstehenden Körperschaften sowie Anstalten und Stiftungen des öffentlichen Rechts und
3.  die Gemeinden und Gemeindeverbände einschließlich der ihrer Aufsicht unterstehenden juristischen Personen des öffentlichen Rechts und deren Vereinigungen.

#### § 2 Grundsätze der Landesstatistik und der Kommunalstatistik

(1) Landesstatistiken und Kommunalstatistiken sind auf das notwendige Maß zu beschränken und sollen nur angeordnet werden, wenn die benötigten Informationen nicht auf andere Art beschafft werden können.
Vorbehaltlich anderer Rechtsvorschriften soll, soweit möglich und angemessen, auf Verwaltungsdaten aus dem Bestand öffentlicher Stellen zurückgegriffen werden.
Die Möglichkeit und die Angemessenheit sind von der für die Durchführung der Statistik zuständigen Stelle (Amt für Statistik Berlin-Brandenburg, kommunale Statistikstelle) zu prüfen; diese kann hierzu von den in § 1 Absatz 3 Nummer 1 und 2 genannten Stellen Einzelangaben ohne Name und Anschrift (formal anonymisierte Einzelangaben) anfordern.
Die Übermittlung der Daten ist in der Rechtsvorschrift zu regeln, die die Landes- und Kommunalstatistik anordnet.

(2) Für die Landesstatistik und die Kommunalstatistik gelten die Grundsätze der Neutralität und Objektivität, der Genauigkeit und Zuverlässigkeit, der Transparenz, der statistischen Geheimhaltung und der fachlichen Unabhängigkeit.
Die für die Durchführung der Statistiken erforderlichen Daten werden unter Verwendung wissenschaftlicher Erkenntnisse und unter Einsatz der jeweils sachgerechten Methoden und statistischen Verfahren gewonnen.
Qualität und Aktualität der Statistiken sind sicherzustellen; eine übermäßige Belastung der Auskunftgebenden ist zu vermeiden.

### Abschnitt 2  
Amt für Statistik Berlin-Brandenburg

#### § 3 Aufgaben des Amtes für Statistik Berlin-Brandenburg

(1) Zuständig für die Durchführung von Statistiken der Europäischen Union sowie von Bundes- und Landesstatistiken (amtliche Statistiken) ist das Amt für Statistik Berlin-Brandenburg, soweit nicht durch Rechtsvorschrift etwas anderes bestimmt ist.
Das Gleiche gilt für Landesstatistiken, die auf einer vom fachlich zuständigen Ministerium mit den übrigen Bundesländern getroffenen Koordinierungsvereinbarung beruhen (koordinierte Länderstatistiken).
Aufgabe des Amtes für Statistik Berlin-Brandenburg ist es insbesondere,

1.  an der Vorbereitung von Rechts- und Verwaltungsvorschriften mitzuwirken, die amtliche Statistiken betreffen,
2.  diese Statistiken methodisch, organisatorisch und technisch vorzubereiten oder bei der Vorbereitung mitzuwirken,
3.  im Rahmen dieser Statistiken Daten zu erheben, aufzubereiten und zu speichern sowie
4.  statistische Ergebnisse in der erforderlichen Gliederung zu erstellen, in sachlich, räumlich und zeitlich vergleichbarer Form dauerhaft nutzbar zu speichern, auszuwerten, in wissenschaftlichen Gesamtsystemen zusammenzufassen, weiterzugeben und zu veröffentlichen.

Zu den Aufgaben des Amtes für Statistik Berlin-Brandenburg gehört auch die Feststellung der durch die Volks-, Gebäude- und Wohnungszählungen (Zensus) zum Zensusstichtag ermittelten Einwohnerzahlen des Landes und der Gemeinden.

(2) Neben den dem Amt für Statistik Berlin-Brandenburg aufgrund von Rechtsvorschriften obliegenden Aufgaben können die obersten Landesbehörden mit Zustimmung der für Statistik zuständigen obersten Landesbehörde dem Amt weitere Aufgaben im Zusammenhang mit der Statistik übertragen (Auftragsarbeiten).
Dazu gehört auch, bei Bedarf amtliche Verzeichnisse sowie Sonderauswertungen von statistischen Daten zu erstellen und zu veröffentlichen.
Weitere Aufgaben des Amtes für Statistik Berlin-Brandenburg sind in Artikel 3 Absatz 2 bis 5 des Staatsvertrages vom 13. Dezember 2005 zwischen dem Land Berlin und dem Land Brandenburg über die Errichtung eines Amtes für Statistik Berlin-Brandenburg (GVBl.
2006 I S. 49) geregelt.

#### § 4 Zusammenarbeit und Vergabe statistischer Arbeiten

(1) Das Amt für Statistik Berlin-Brandenburg darf hinsichtlich der Durchführung von Statistiken und sonstigen Arbeiten statistischer Art, die nach diesem Gesetz oder aufgrund dieses Gesetzes erfolgen, die Ausführungen einzelner Arbeiten oder hierzu erforderlicher Hilfsmaßnahmen durch Verwaltungsvereinbarungen auf andere statistische Ämter des Bundes oder der Länder übertragen oder von diesen sich übertragen lassen.
Davon ausgenommen sind die Heranziehung zur Auskunftserteilung und die Durchsetzung der Auskunftspflicht.
Zu den Arbeiten statistischer Art nach Satz 1 gehört auch die Bereitstellung von Mikrodaten für die Wissenschaft und für methodische und analytische Zwecke.

(2) Bei der Durchführung von Statistiken können einzelne Arbeiten Dritten übertragen werden, wenn sichergestellt ist, dass die Vorschriften zur Geheimhaltung und zum Schutz von Einzelangaben nach § 17 gewahrt sind.
Absatz 1 Satz 2 gilt entsprechend.
Für Personen, die zur Erledigung der übertragenen Aufgaben eingesetzt werden sollen, gilt § 12 entsprechend.

(3) Die öffentlichen Stellen des Landes haben dem Amt für Statistik Berlin-Brandenburg Daten, die dieses für die Durchführung amtlicher Statistiken benötigt, kostenfrei zur Verfügung zu stellen, sofern diese Daten im Verwaltungsvollzug anfallen und sofern nicht Rechtsvorschriften einer Übermittlung entgegenstehen.
Vor der Neukonstruktion oder Veränderung von Verwaltungsdatenbeständen, aus denen Statistikabzüge erfolgen, ist dem Amt für Statistik Berlin-Brandenburg von der zuständigen öffentlichen Stelle Gelegenheit zur Stellungnahme zu geben.

(4) Zu Gesetz- und Verordnungsentwürfen des Landes, die Auswirkungen auf die Aufgaben der amtlichen Statistik haben, kann die fachliche Stellungnahme des Amtes für Statistik Berlin-Brandenburg eingeholt werden.

#### § 5 Register

(1) Das Amt für Statistik Berlin-Brandenburg kann zur Vorbereitung und Erstellung von Landesstatistiken sowie für Auswertungszwecke ein Unternehmensregister gemäß Statistikregistergesetz vom 16.
Juni 1998 (BGBl. I S. 1300), das zuletzt durch Artikel 2 des Gesetzes vom 21.
Juli 2016 (BGBl. I S. 1768, 1772) geändert worden ist, in der jeweils geltenden Fassung führen, soweit es zur Erfüllung seiner Aufgaben nach diesem Gesetz oder einer sonstigen eine Landesstatistik anordnenden Rechtsvorschrift erforderlich ist.

(2) Das Amt für Statistik Berlin-Brandenburg kann zur Vorbereitung und Erstellung von Landesstatistiken sowie für Auswertungszwecke ein Anschriftenregister führen.
Es enthält zu jeder Anschrift die Postleitzahl, die Gemeindebezeichnung, die Straßenbezeichnung mit Hausnummer, die Geokoordinate des Grundstücks sowie eine Ordnungsnummer.
Für die Vorbereitung und Durchführung von Befragungen auf Stichprobenbasis dürfen zusätzlich die für die Schichtenklassifizierung notwendige Gesamtzahl der an der jeweiligen Anschrift wohnenden Personen sowie die Wohnraumeigenschaft gespeichert werden.

(3) Zur Pflege und Führung des Unternehmensregisters nach Absatz 1 und des Anschriftenregisters nach Absatz 2 dürfen Angaben aus allgemein zugänglichen Quellen sowie aus Bundes- und Landesstatistiken nach dem Verwaltungsdatenverwendungsgesetz vom 4.
November 2010 (BGBl. I S. 1480), das durch Artikel 1 des Gesetzes vom 18. Dezember 2018 (BGBl. I S. 2637) geändert worden ist, in der jeweils geltenden Fassung verwendet und zusammengeführt werden.

### Abschnitt 3  
Anordnung von Statistiken

#### § 6 Landesstatistiken

(1) Landesstatistiken sind Statistiken, die durch Rechtsvorschriften des Landes angeordnet oder bei denen zu Landeszwecken Angaben ausschließlich aus allgemein zugänglichen Quellen oder aus öffentlichen Registern verwendet werden.

(2) Landesstatistiken sind, soweit nicht in diesem Gesetz oder in einer sonstigen Rechtsvorschrift etwas anderes bestimmt ist, durch Gesetz oder Rechtsverordnung anzuordnen.
Landesstatistiken, bei denen ausschließlich Angaben aus allgemein zugänglichen Quellen verwendet werden, bedürfen keiner besonderen Anordnung.
Das Gleiche gilt für Landesstatistiken, bei denen Angaben ausschließlich aus öffentlichen Registern verwendet werden, soweit dem Amt für Statistik Berlin-Brandenburg in einer Rechtsvorschrift ein besonderes Zugangsrecht zu diesen Registern gewährt wird.

(3) Zur Erfüllung eines kurzfristig auftretenden Datenbedarfs oberster Landesbehörden und zur Klärung methodisch-wissenschaftlicher Fragen auf dem Gebiet der amtlichen Statistik (Erhebungen für besondere Zwecke) dürfen Landesstatistiken ohne Auskunftspflicht durchgeführt werden.
Sie bedürfen keiner gesonderten Anordnung.
Für Landesstatistiken nach Satz 1 darf nur ein Teil der Grundgesamtheit befragt werden.
Die Zahl der Befragten darf nicht höher sein als für den Erhebungszweck erforderlich.

#### § 7 Kommunalstatistiken

(1) Kommunalstatistiken sind Statistiken, die von Gemeinden und Gemeindeverbänden zur Wahrnehmung ihrer Selbstverwaltungsaufgaben angeordnet oder bei denen zu kommunalen Zwecken Angaben ausschließlich aus allgemein zugänglichen Quellen oder aus öffentlichen Registern verwendet werden.

(2) Die Gemeinden und Gemeindeverbände sind befugt, Kommunalstatistiken durchzuführen, wenn die benötigten Einzelangaben oder Ergebnisse vom Amt für Statistik Berlin-Brandenburg nicht zur Verfügung gestellt werden können.
Die Durchführung von Kommunalstatistiken obliegt den kommunalen Statistikstellen.

(3) Kommunalstatistiken sind durch Satzung anzuordnen.
Der Anordnung durch Satzung bedarf es nicht, wenn

1.  die einer Statistik zugrunde liegenden Daten
    1.  auf allgemein zugänglichen Quellen beruhen,
    2.  keine Einzelangaben enthalten oder
    3.  der Statistikstelle rechtmäßig übermittelt werden oder aufgrund einer Rechtsvorschrift zur Verfügung stehen

oder

2.  lediglich Sonderauswertungen vorhandenen statistischen Materials vorgenommen werden, dessen Verwendung eine Zweckbindung dieses Materials nicht entgegensteht.

(4) Einzelangaben aus Landesstatistiken, die aufgrund des § 17 Absatz 6 oder aufgrund bundesrechtlicher Vorschriften übermittelt werden, dürfen für kommunalstatistische Zwecke genutzt werden.

#### § 8 Anforderungen an Landes- und Kommunalstatistiken

(1) Vor der Anordnung von Landes- und Kommunalstatistiken ist zu prüfen, ob

1.  die Statistik dringend erforderlich ist,
2.  es einer Auskunftspflicht bedarf,
3.  der Schutz der Privatsphäre gewährleistet ist,
4.  der Arbeitsaufwand, den die Statistik bei den Befragten und bei den mit ihrer Durchführung betrauten öffentlichen Stellen verursacht, in einem angemessenen Verhältnis zu ihrem Nutzen steht und
5.  die Periodizität der Statistik, die Zahl der Befragten und die Zahl der Erhebungsmerkmale zur Erfüllung des Erhebungszwecks geeignet sind.

Statistiken dürfen nur angeordnet werden, wenn die in Satz 1 genannten Voraussetzungen vorliegen.

(2) Die eine Landes- oder Kommunalstatistik anordnende Rechtsvorschrift muss den Zweck der Erhebung, die Erhebungsmerkmale, die Hilfsmerkmale, die Art und Weise der Erhebung, den Berichtszeitraum oder den Berichtszeitpunkt, die Periodizität und den Kreis der zu Befragenden oder die Daten übermittelnden Stellen bestimmen.
Die Verarbeitung personenbezogener Daten im Sinne von Artikel 9 Absatz 1 der Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates vom 27.
April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr und zur Aufhebung der Richtlinie 95/46/EG (Datenschutz-Grundverordnung) (ABl. L 119 vom 4.5.2016, S. 1; L 314 vom 22.11.2016, S. 72; L 127 vom 23.5.2018, S. 2) für statistische Zwecke darf nur angeordnet werden, wenn

1.  dies für die Erfüllung des mit der Statistik verfolgten Zwecks zwingend erforderlich ist,
2.  das Interesse der betroffenen Person am Unterbleiben der Verarbeitung das öffentliche Interesse an der Erstellung der Statistik nicht überwiegt und
3.  angemessene und spezifische Maßnahmen zur Wahrung der Grundrechte und Interessen der betroffenen Person nach § 24 des Brandenburgischen Datenschutzgesetzes vorgesehen werden.

Laufende Nummern und Ordnungsnummern zur Durchführung von Landes- und Kommunalstatistiken bedürfen einer Bestimmung in der die Statistik anordnenden Rechts- oder Verwaltungsvorschrift nur insoweit, als sie Angaben über persönliche oder sächliche Verhältnisse enthalten, die über die Erhebungs- und Hilfsmerkmale hinausgehen.

#### § 9 Erprobung

(1) Das Amt für Statistik Berlin-Brandenburg kann zur Vorbereitung einer Rechtsvorschrift, durch die eine Landesstatistik angeordnet wird,

1.  zur Bestimmung des Kreises der zu Befragenden und deren statistischer Zuordnung Angaben erheben und
2.  die Zweckmäßigkeit von Erhebungsunterlagen und Erhebungsverfahren erproben.

Für die Angaben nach den Nummern 1 und 2 besteht keine Auskunftspflicht.
Sie werden zum frühestmöglichen Zeitpunkt gelöscht, die Angaben nach Nummer 2 spätestens drei Jahre nach der Durchführung der Erprobung.
Bei den Angaben nach Nummer 2 werden Name und Anschrift von den übrigen Angaben zum frühestmöglichen Zeitpunkt getrennt und gesondert aufbewahrt.

(2) Absatz 1 gilt entsprechend für kommunale Statistikstellen zur Vorbereitung einer Satzung, durch die eine Kommunalstatistik angeordnet wird.

### Abschnitt 4  
Kommunale Statistikstellen, Erhebungsstellen, Erhebungsbeauftragte

#### § 10 Kommunale Statistikstellen

(1) Kommunalstatistiken dürfen nur von einer Stelle innerhalb der Verwaltung der Gemeinde oder des Gemeindeverbandes durchgeführt werden, die den Anforderungen des § 13 entspricht (kommunale Statistikstelle).
§ 4 Absatz 2 gilt entsprechend.

(2) Für ausschließlich statistische Zwecke dürfen an die jeweils zuständige kommunale Statistikstelle Daten, die im Geschäftsgang anderer Verwaltungsstellen der Gemeinden und Gemeindeverbände rechtmäßig anfallen, übermittelt werden, soweit die Auswertungen der Daten zur Wahrnehmung der kommunalen Selbstverwaltungsaufgaben erforderlich sind und die Weitergabe gesetzlich nicht verboten ist.
§ 7 Absatz 3 gilt entsprechend.
Für die Weitergabe personenbezogener Daten im Sinne von Artikel 9 Absatz 1 der Datenschutz-Grundverordnung für statistische Zwecke gilt § 8 Absatz 2 Satz 2 entsprechend.

(3) Die Einrichtung sowie die Auflösung einer kommunalen Statistikstelle ist von den Gemeinden und Gemeindeverbänden ortsüblich bekannt zu geben sowie der Kommunalaufsichtsbehörde und der oder dem Landesbeauftragten für den Datenschutz und für das Recht auf Akteneinsicht schriftlich anzuzeigen.

(4) Gemeinden und Gemeindeverbände, die die Voraussetzungen nach den Absätzen 1 und 2 nicht erfüllen, können sich der Formen kommunaler Gemeinschaftsarbeit bedienen und eine gemeinsame kommunale Statistikstelle einrichten.

#### § 11 Erhebungsstellen

(1) Erhebungsstellen sind mit der Durchführung amtlicher Statistiken betraute amtliche Stellen.
Soweit nicht durch Rechtsvorschrift etwas anderes bestimmt ist, haben sie insbesondere die Aufgabe,

1.  die Erhebungsbeauftragten anzuwerben, auszuwählen, zu bestellen und abzuberufen, über ihre Rechte und Pflichten zu belehren, auf die Geheimhaltungsbestimmungen schriftlich zu verpflichten, zu schulen sowie die Schulung und die ordnungsgemäße Aufgabenerledigung der Erhebungsbeauftragten zu dokumentieren und die Dokumentation an das Amt für Statistik Berlin-Brandenburg zu übermitteln,
2.  die Erhebungsunterlagen auszuteilen, einzusammeln sowie sicher und für Unbefugte unzugänglich aufzubewahren, die Auskunftseingänge zu registrieren, die zu Befragenden über die Erhebung zu unterrichten und zur Auskunft aufzufordern, soweit Auskunftspflicht besteht,
3.  unvollständige oder fehlerhaft ausgefüllte Erhebungsunterlagen durch Nachfrage bei den Befragten zu ergänzen oder zu berichtigen sowie Unstimmigkeiten zu klären und
4.  die Erhebungsunterlagen nach Prüfung auf Vollzähligkeit und Vollständigkeit dem Amt für Statistik Berlin-Brandenburg oder der überörtlichen Erhebungsstelle entsprechend der Terminplanung zuzuleiten und die Vollzähligkeit und Vollständigkeit zu bestätigen.

(2) Werden den Gemeinden und Gemeindeverbänden die Einrichtung der Erhebungsstellen und die Aufgaben nach Absatz 1 übertragen, nehmen sie diese als Pflichtaufgaben zur Erfüllung nach Weisung wahr, wenn dies in einer Rechtsvorschrift gemäß § 6 Absatz 2 angeordnet ist.
Sind bei den Gemeinden und Gemeindeverbänden kommunale Statistikstellen eingerichtet, so können diese die Aufgaben der Erhebungsstelle wahrnehmen.
§ 10 Absatz 4 gilt entsprechend.

(3) Sonderaufsichtsbehörde über die Erhebungsstellen nach Absatz 2 ist das Amt für Statistik Berlin-Brandenburg, soweit nicht durch Rechtsvorschrift etwas anderes bestimmt ist.
Oberste Sonderaufsichtsbehörde ist die für Statistik zuständige oberste Landesbehörde.
Bei der Durchführung einer Landesstatistik wird die Aufsicht im Benehmen mit der fachlich zuständigen obersten Landesbehörde ausgeübt.

#### § 12 Erhebungsbeauftragte

(1) Die mit der Erhebung von Landes- und Kommunalstatistiken amtlich betrauten Personen (Erhebungsbeauftragte) müssen die Gewähr für Zuverlässigkeit und Verschwiegenheit bieten.
Erhebungsbeauftragte dürfen nicht eingesetzt werden, wenn aufgrund ihrer beruflichen Tätigkeit oder aus anderen Gründen Anlass zur Besorgnis besteht, dass Erkenntnisse aus der Tätigkeit als Erhebungsbeauftragte zu Lasten der Befragten oder Betroffenen genutzt werden.
Bürgerinnen und Bürger, die das 18.
Lebensjahr vollendet haben, können zur Übernahme der Tätigkeit als Erhebungsbeauftragte verpflichtet werden.

(2) Bei der Ausübung ihrer Tätigkeit haben die Erhebungsbeauftragten ihre Berechtigung nachzuweisen.

(3) Erhebungsbeauftragte dürfen die aus ihrer Tätigkeit gewonnenen Erkenntnisse nicht in anderen Verfahren oder für andere Zwecke verwenden.
Sie sind über ihre Rechte und Pflichten zu belehren und auf die Wahrung des Statistikgeheimnisses sowie zur Geheimhaltung auch solcher Erkenntnisse schriftlich zu verpflichten, die sie gelegentlich ihrer Tätigkeit gewonnen haben.
Die Verpflichtung besteht auch nach Beendigung ihrer Tätigkeit fort.

(4) Erhebungsbeauftragte unterstehen dem Weisungsrecht der Erhebungsstelle oder der kommunalen Statistikstelle.
Sofern bei Erhebungen keine Erhebungsstellen außerhalb des Amtes für Statistik Berlin-Brandenburg bestehen, hat das Amt dieses Recht.

### Abschnitt 5  
Durchführung von Statistiken

#### § 13 Abschottung

(1) Das Amt für Statistik Berlin-Brandenburg, die kommunalen Statistikstellen und die Erhebungsstellen haben durch räumliche, personelle, organisatorische und technische Maßnahmen die Trennung der für die Statistik zuständigen Organisationseinheiten von den anderen Organisationseinheiten sicherzustellen (Abschottung).
Für die Sicherung von Einzelangaben ist die Abschottung dieser Daten gegenüber anderen Verwaltungsdaten und ihre Zweckbindung durch zusätzliche geeignete organisatorische und technische Maßnahmen zu gewährleisten.

(2) Die im Amt für Statistik Berlin-Brandenburg, in den kommunalen Statistikstellen und in den Erhebungsstellen mit statistischen Aufgaben betrauten Personen müssen die Gewähr für Zuverlässigkeit und Verschwiegenheit bieten.
Während der Tätigkeit für statistische Aufgaben in den vorgenannten Stellen dürfen sie nicht mit anderen Aufgaben des Verwaltungsvollzugs betraut werden.
Sie dürfen die aus ihrer Tätigkeit gewonnenen Erkenntnisse über Auskunftspflichtige während und nach ihrer Tätigkeit in den vorgenannten Stellen nicht in anderen Verfahren oder für andere Zwecke verwenden oder offenbaren.

(3) Im Amt für Statistik Berlin-Brandenburg sind die für die Abschottung erforderlichen Maßnahmen zu dokumentieren.
In den Gemeinden und Gemeindeverbänden sind die für die Abschottung der kommunalen Statistikstellen und der Erhebungsstellen erforderlichen Maßnahmen in einer schriftlichen Dienstanweisung festzulegen.

(4) Sofern andere öffentliche Stellen des Landes Brandenburg dauerhaft statistische Aufgaben wahrnehmen, die die Sicherung von Einzelangaben erfordert, gelten die Absätze 1 und 2 sowie Absatz 3 Satz 1 entsprechend.
Auch bei einzelnen Arbeiten sind die Vorschriften zum Schutz von Einzelangaben und zur Geheimhaltung einzuhalten.

#### § 14 Erhebungs- und Hilfsmerkmale

(1) Landes- und Kommunalstatistiken werden auf der Grundlage von Erhebungs- und von Hilfsmerkmalen erstellt.

(2) Erhebungsmerkmale umfassen Angaben über persönliche und sächliche Verhältnisse, die zur statistischen Verwendung bestimmt sind.
Hilfsmerkmale sind Angaben, die zur technischen Durchführung von Statistiken erforderlich sind.
Soweit durch Rechtsvorschrift zugelassen, dürfen Hilfsmerkmale für weitere Erhebungen verwendet werden.

(3) Der Name der Gemeinde und des Gemeindeteils sowie die Blockseite und die geografische Gitterzelle dürfen für die regionale Zuordnung der Erhebungsmerkmale genutzt werden.
Besondere Regelungen in einer eine Landes- oder Kommunalstatistik anordnenden Rechtsvorschrift bleiben unberührt.
Blockseite ist innerhalb eines Gemeindegebietes die Seite mit gleicher Straßenbezeichnung von der durch Straßeneinmündungen oder vergleichbare Begrenzungen umschlossenen Fläche.
Eine geografische Gitterzelle ist eine Gebietseinheit, die bezogen auf eine vorgegebene Kartenprojektion quadratisch und mindestens 1 Hektar groß ist.

#### § 15 Trennung und Löschung der Hilfsmerkmale

(1) Die Hilfsmerkmale sind von den Erhebungsmerkmalen zum frühestmöglichen Zeitpunkt zu trennen und gesondert zu speichern.
Soweit nicht durch Rechtsvorschrift etwas anderes bestimmt ist, sind die Hilfsmerkmale zu löschen, sobald die Überprüfung der Erhebungs- und der Hilfsmerkmale auf ihre Schlüssigkeit und Vollständigkeit abgeschlossen ist.

(2) Bei periodischen Erhebungen für Zwecke der Landesstatistik oder der Kommunalstatistik dürfen die zur Bestimmung des Kreises der zu Befragenden erforderlichen Hilfsmerkmale, soweit sie für nachfolgende Erhebungen benötigt werden, gesondert gespeichert werden.
Nach Beendigung des Zeitraums der wiederkehrenden Erhebungen sind sie zu löschen.

#### § 16 Auskunftspflicht

(1) Soweit eine Auskunftspflicht angeordnet ist, sind alle in die Erhebung einbezogenen natürlichen Personen, juristischen Personen des privaten und öffentlichen Rechts, Personenvereinigungen und öffentlichen Stellen zur Auskunft gegenüber den Erhebungsstellen oder den kommunalen Statistikstellen sowie den von ihnen eingesetzten Erhebungsbeauftragten verpflichtet.

(2) Die Antworten sind von den Befragten in der von der Erhebungsstelle oder kommunalen Statistikstelle vorgegebenen Form zu erteilen.
Sie können elektronisch, schriftlich, mündlich oder telefonisch erteilt werden, soweit diese Möglichkeiten zur Antworterteilung angeboten werden.
Im Falle einer mündlichen oder telefonischen Befragung ist auch die Möglichkeit einer schriftlichen Antworterteilung vorzusehen.

(3) Öffentliche Auskunftsstellen sind zur elektronischen Datenlieferung an das Amt für Statistik Berlin-Brandenburg verpflichtet oder, soweit sie Daten mittels standardisierter Datenaustauschformate übermitteln, zu deren Nutzung auch für die Übermittlung der für die Statistik zu erhebenden Daten.
Werden Betrieben und Unternehmen für die Übermittlung der für eine Landesstatistik zu erhebenden Daten elektronische Verfahren zur Verfügung gestellt, sind sie verpflichtet, diese Verfahren zu nutzen, es sei denn, die zuständige Stelle hat zur Vermeidung unbilliger Härten eine Ausnahme vorgesehen.
Bei der elektronischen Datenübermittlung ist ein dem Stand der Technik entsprechendes elektronisches Verschlüsselungsverfahren zu nutzen.

(4) Die Antwort ist wahrheitsgemäß, vollständig und innerhalb der von der Erhebungsstelle oder der kommunalen Statistikstelle gesetzten Fristen zu erteilen.
Die Antwort ist erteilt, wenn sie

1.  bei postalischer Übermittlung der Erhebungsstelle oder der kommunalen Statistikstelle zugegangen ist oder
2.  bei elektronischer Übermittlung von der für den Empfang bestimmten Einrichtung in für die Erhebungsstelle oder für die kommunale Statistikstelle bearbeitbarer Weise aufgezeichnet worden ist.

Die Antwort ist, soweit nicht durch Rechtsvorschrift etwas anderes bestimmt ist, für den Empfänger kosten- und portofrei zu erteilen.

(5) Wird bei einer mündlichen oder telefonischen Befragung die Antwort nach Absatz 2 Satz 3 schriftlich erteilt, können die ausgefüllten Fragebogen den Erhebungsbeauftragten übergeben, bei der Erhebungsstelle oder der kommunalen Statistikstelle abgegeben oder dorthin übersandt werden.

(6) Widerspruch und Anfechtungsklage gegen die Aufforderung zur Auskunftserteilung bei der Durchführung von Landes- und Kommunalstatistiken, die durch Rechtsvorschrift angeordnet sind, haben keine aufschiebende Wirkung.

(7) Gegen die Aufforderung zur Auskunftserteilung bei der Durchführung von Volks-, Gebäude- und Wohnungszählungen (Zensus) findet kein Widerspruch statt.

#### § 17 Geheimhaltung und Übermittlung von Einzelangaben

(1) Einzelangaben über persönliche und sächliche Verhältnisse bestimmter oder bestimmbarer natürlicher oder juristischer Personen, die für eine Landes- oder Kommunalstatistik gemacht werden, sind von den mit der Durchführung der Statistiken betrauten Amtsträgern und für den öffentlichen Dienst besonders Verpflichteten geheim zu halten und nicht für andere Zwecke zu verwenden, soweit nicht in diesem Gesetz oder in einer eine Landes- oder Kommunalstatistik anordnenden Rechtsvorschrift etwas anderes bestimmt ist.
Die Geheimhaltungspflicht besteht auch nach Beendigung der Tätigkeit der genannten Personen fort.
Statistische Daten mit Einzelangaben sind so zu verarbeiten, dass Einzelangaben Unbefugten nicht bekannt und zugänglich werden.

(2) Die Pflicht zur Geheimhaltung nach Absatz 1 gilt für Empfänger von Einzelangaben entsprechend.
Sie sind vor der Übermittlung durch die Daten übermittelnde Stelle schriftlich zur Geheimhaltung zu verpflichten, soweit sie nicht Amtsträger oder für den öffentlichen Dienst besonders Verpflichtete sind.
Personen, die nach Satz 2 verpflichtet worden sind, stehen für die Anwendung des § 203 Absatz 2, 4 und 5 sowie der §§ 204, 205 und 353 b Absatz 1 des Strafgesetzbuches den für den öffentlichen Dienst besonders Verpflichteten gleich.

(3) Die Geheimhaltungspflicht gilt nicht für Einzelangaben,

1.  in deren Übermittlung oder Veröffentlichung die oder der Befragte oder Betroffene schriftlich eingewilligt hat, soweit nicht wegen besonderer Umstände eine andere Form der Einwilligung angemessen ist,
2.  die aus allgemein zugänglichen Quellen entnommen werden können, auch soweit sie aufgrund einer Auskunftspflicht erlangt wurden,
3.  die den Befragten oder Betroffenen nicht zuzuordnen sind oder
4.  deren Übermittlung oder Veröffentlichung durch Rechtsvorschrift zugelassen ist.

(4) Die Offenlegung statistischer Angaben, die Einzelangaben enthalten, zwischen den Erhebungsstellen oder kommunalen Statistikstellen und ihren Auftragnehmern ist nur zulässig, soweit dies zur Erstellung der Landes- oder Kommunalstatistik oder zur Durchführung eines Rechtsbehelfsverfahrens, eines Verwaltungsvollstreckungsverfahrens oder eines Bußgeldverfahrens erforderlich ist.

(5) Das Amt für Statistik Berlin-Brandenburg darf dem Statistischen Bundesamt und den statistischen Ämtern der anderen Länder die ihren jeweiligen Erhebungsbereich betreffenden Einzelangaben übermitteln, soweit dies zur Erstellung koordinierter Länderstatistiken oder zur Klärung methodischer Fragestellungen erforderlich ist.

(6) Für ausschließlich statistische Zwecke darf das Amt für Statistik Berlin-Brandenburg kommunalen Statistikstellen Einzelangaben für ihren Zuständigkeitsbereich übermitteln, wenn die Übermittlung sowie Art und Umfang der zu übermittelnden Einzelangaben in einer eine Statistik anordnenden Rechtsvorschrift festgelegt sind.
Vor der erstmaligen Übermittlung von Einzelangaben aus amtlichen Statistiken an eine kommunale Statistikstelle ist dem Amt für Statistik Berlin-Brandenburg die Dienstanweisung nach § 13 Absatz 3 Satz 2 sowie die Bekanntgabe über die Einrichtung einer kommunalen Statistikstelle nach § 10 Absatz 3 vorzulegen.
Kommunale Statistikstellen, die Einzelangaben nach Satz 1 erhalten haben, haben unverzüglich Zeitpunkt, Art, Umfang und Verwendungszweck der Übermittlung aufzuzeichnen.
Die Aufzeichnungen sind fünf Jahre aufzubewahren.

(7) Für die Verwendung gegenüber den gesetzgebenden Körperschaften und für Zwecke der Planungen, jedoch nicht für die Regelung von Einzelfällen, dürfen das Amt für Statistik Berlin-Brandenburg und die kommunalen Statistikstellen den obersten Bundes- oder Landesbehörden sowie den fachlich zuständigen Bundes- oder Landesoberbehörden, soweit diese von ihrer obersten Behörde dazu ermächtigt sind, Tabellen mit statistischen Ergebnissen übermitteln, auch soweit Tabellenfelder nur einen einzigen Fall ausweisen.
Die Übermittlung nach Satz 1 ist nur zulässig, soweit in den §§ 6 und 7 die Übermittlung von Einzelangaben an oberste Bundes- oder Landesbehörden oder an dazu ermächtigte Bundes- oder Landesoberbehörden vorgesehen ist.
Absatz 2 gilt entsprechend.

(8) Für die Durchführung wissenschaftlicher Vorhaben dürfen das Amt für Statistik Berlin-Brandenburg und die kommunalen Statistikstellen Hochschulen oder sonstige Einrichtungen mit der Aufgabe unabhängiger wissenschaftlicher Forschung

1.  Einzelangaben übermitteln, wenn diese nur mit einem unverhältnismäßig großen Aufwand an Zeit, Kosten und Arbeitskraft bestimmten oder bestimmbaren natürlichen oder juristischen Personen und deren Vereinigungen zugeordnet werden können (faktisch anonymisierte Einzelangaben), oder
2.  innerhalb speziell abgesicherter Bereiche des Amtes für Statistik Berlin-Brandenburg oder der kommunalen Statistikstellen Zugang zu formal anonymisierten Einzelangaben gewähren, wenn wirksame Vorkehrungen zur Wahrung der Geheimhaltung getroffen werden.

Absatz 2 Satz 2 und 3 gilt entsprechend.
Die nach Satz 1 Nummer 1 übermittelten Einzelangaben sind zu löschen, sobald das wissenschaftliche Vorhaben durchgeführt ist.

(9) Das Amt für Statistik Berlin-Brandenburg und die kommunalen Statistikstellen haben die Offenlegung von Einzelangaben nach den Absätzen 6, 7 oder 8 oder aufgrund einer besonderen Rechtsvorschrift nach Inhalt, Empfänger, Datum und Zweck der Offenlegung aufzuzeichnen.
Die Aufzeichnungen sind mindestens fünf Jahre aufzubewahren.

#### § 18 Unterrichtung und andere Rechte

(1) Ergänzend zu den Informationspflichten nach Artikel 13 und 14 der Datenschutz-Grundverordnung sind die zu Befragenden schriftlich oder elektronisch zu unterrichten über

1.  Zweck, Art und Umfang der Erhebung,
2.  die Rechtsgrundlage der jeweiligen Statistik,
3.  die Auskunftspflicht und die verschiedenen Möglichkeiten, ihr nachzukommen, oder die Freiwilligkeit der Auskunftserteilung (§ 16),
4.  die bei der Durchführung verwendeten Erhebungs- und Hilfsmerkmale (§ 14),
5.  die Trennung und Löschung der Hilfsmerkmale (§ 15),
6.  die statistische Geheimhaltung (§ 17 Absatz 1 bis 4),
7.  die Möglichkeiten der Übermittlung von Einzelangaben (§ 17 Absatz 5 bis 8),
8.  die Rechte und Pflichten der Erhebungsbeauftragten (§ 12),
9.  die Bedeutung und den Inhalt von laufenden Nummern und Ordnungsnummern (§ 5 Absatz 2 Satz 2 sowie § 8 Absatz 2 Satz 3),
10.  den Ausschluss der aufschiebenden Wirkung von Widerspruch und Anfechtungsklage gegen die Aufforderung zur Auskunftserteilung (§ 16 Absatz 6),
11.  den Ausschluss des Widerspruchs gegen die Aufforderung zur Auskunftserteilung bei der Durchführung von Volks-, Gebäude- und Wohnungszählungen (§ 16 Absatz 7) sowie
12.  die Möglichkeit der Ahndung von Verletzungen der Auskunftspflicht (§ 23).

(2) Die in den Artikeln 15, 16, 18 und 21 der Datenschutz-Grundverordnung vorgesehenen Rechte der betroffenen Personen bestehen nicht, soweit die Wahrnehmung dieser Rechte die Verwirklichung der Statistikzwecke unmöglich machen oder ernsthaft beeinträchtigen würde und der Ausschluss der Rechte für die Erfüllung dieser Zwecke notwendig ist.

#### § 19 Statistische Aufbereitung von Daten aus dem Verwaltungsvollzug

(1) Geschäftsstatistiken sind statistische Aufbereitungen von Daten, die

1.  öffentliche Stellen im Vollzug ihrer Aufgaben erheben, ohne hierbei aufgrund einer Rechts- oder Verwaltungsvorschrift über Statistiken zu handeln, oder
2.  bei diesen Stellen auf sonstige Weise rechtmäßig anfallen.

(2) Geschäftsstatistiken bedürfen keiner Anordnung durch Rechts- oder Verwaltungsvorschrift.
Zu ihrer Erstellung dürfen auch personenbezogene Daten verwendet werden, wenn die Geschäftsstatistiken ausschließlich für die Erfüllung der Aufgaben der öffentlichen Stelle, in deren Geschäftsgang die Daten rechtmäßig anfallen, oder der ihr übergeordneten öffentlichen Stelle bestimmt und hierzu geeignet und erforderlich sind.
§ 8 Absatz 2 Satz 2 gilt entsprechend.

(3) Die Aufbereitung von Geschäftsstatistiken der öffentlichen Stellen des Landes kann mit Zustimmung des fachlich zuständigen Ministeriums und der für die Statistik zuständigen obersten Landesbehörde ganz oder teilweise dem Amt für Statistik Berlin-Brandenburg übertragen werden.
Dieses ist mit vorheriger Zustimmung der fachlich zuständigen Ministerien berechtigt, die gewonnenen statistischen Ergebnisse für allgemeine Zwecke zu veröffentlichen und darzustellen.
Die Veröffentlichungen dürfen keine Angaben enthalten, die den Bezug auf eine bestimmte Person zulassen.
Zuständige Fachaufsichtsbehörde ist die fachlich zuständige oberste Landesbehörde.

(4) Für die kleinräumige Darstellung von Ergebnissen nutzt das Amt für Statistik Berlin-Brandenburg die Geobasisinformationen des amtlichen Vermessungswesens und des Bundes.

(5) Die statistische Aufbereitung von Verwaltungsdaten der Gemeinden und Gemeindeverbände (kommunale Daten) kann der jeweils zuständigen kommunalen Statistikstelle übertragen werden.

#### § 20 Kosten

(1) Die Kosten der Durchführung amtlicher Statistiken trägt das Land, soweit sie bei Landesbehörden anfallen oder bei den Gemeinden und Gemeindeverbänden im Rahmen von Pflichtaufgaben zur Erfüllung nach Weisung, von pflichtigen Selbstverwaltungsaufgaben oder von Auftragsangelegenheiten entstehen.

(2) Die Kosten der Durchführung von Erhebungen nach § 6 Absatz 3, von Kommunalstatistiken nach § 7 und von statistischen Aufbereitungen von Daten aus dem Verwaltungsvollzug nach § 19 trägt die jeweils auftraggebende Stelle.

(3) Die Kosten der Datenübermittlungen an das Amt für Statistik Berlin-Brandenburg, an die Erhebungsstellen und an die kommunalen Statistikstellen werden nicht erstattet, soweit nicht eine einzelstatistische Rechtsvorschrift etwas anderes bestimmt.

(4) Die öffentlichen Stellen des Landes haben dem Amt für Statistik Berlin-Brandenburg Daten, die dieses für die Durchführung amtlicher Statistiken benötigt, kostenfrei zur Verfügung zu stellen, sofern diese Daten im Verwaltungsvollzug anfallen und sofern nicht Rechtsvorschriften einer Übermittlung entgegenstehen.

### Abschnitt 6  
Reidentifizierungsverbot, Strafvorschrift, Ordnungswidrigkeiten, Vollzug gegen Behörden und juristische Personen des öffentlichen Rechts

#### § 21 Reidentifizierungsverbot

Eine Zusammenführung von Einzelangaben aus Landes- oder Kommunalstatistiken oder mit anderen Angaben zum Zwecke der Herstellung eines Personen-, Unternehmens-, Betriebs- oder Arbeitsstättenbezugs außerhalb der Aufgabenstellung dieses Gesetzes oder einer eine Landes- oder Kommunalstatistik anordnenden Rechtsvorschrift ist unzulässig.

#### § 22 Strafvorschrift

Wer entgegen § 21 Einzelangaben zusammenführt oder solche Einzelangaben mit anderen Angaben zusammenführt, wird mit Freiheitsstrafe bis zu einem Jahr oder mit Geldstrafe bestraft.

#### § 23 Ordnungswidrigkeiten

(1) Ordnungswidrig handelt, wer vorsätzlich oder fahrlässig entgegen § 16 Absatz 1, Absatz 2 Satz 1 oder Absatz 4 Satz 1 eine Auskunft nicht, nicht richtig, nicht vollständig, nicht rechtzeitig oder nicht in der vorgegebenen Form erteilt.
Ordnungswidrig handelt auch, wer vorsätzlich oder fahrlässig entgegen § 16 Absatz 3 Satz 2 ein dort genanntes Verfahren nicht nutzt.

(2) Ordnungswidrig handelt auch, wer vorsätzlich oder fahrlässig einer Auskunftspflicht zuwiderhandelt, die in einer nach § 7 Absatz 2 erlassenen Satzung festgelegt ist, soweit die Satzung für einen bestimmten Tatbestand auf diese Bußgeldvorschrift verweist.

(3) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu fünftausend Euro geahndet werden.

(4) Zuständig im Sinne von § 36 Absatz 1 Nummer 1 des Gesetzes über Ordnungswidrigkeiten sind

1.  das Amt für Statistik Berlin-Brandenburg für Ordnungswidrigkeiten nach Absatz 1, soweit Auskunftspflichten für Landesstatistiken betroffen sind, und für Ordnungswidrigkeiten nach § 23 des Bundesstatistikgesetzes in der Fassung der Bekanntmachung vom 20.
    Oktober 2016 (BGBl. I S. 2394), das zuletzt durch Artikel 10 Absatz 5 des Gesetzes vom 30.
    Oktober 2017 (BGBl. I S. 3618, 3623) geändert worden ist,
2.  abweichend von Nummer 1 die Hauptverwaltungsbeamtinnen und Hauptverwaltungsbeamten der Gemeinden und Gemeindeverbände, bei denen Erhebungsstellen eingerichtet sind, soweit nicht durch Rechtsvorschrift etwas anderes bestimmt ist, und
3.  die Gemeinden und Gemeindeverbände, welche die Statistik angeordnet haben, für Ordnungswidrigkeiten nach Absatz 2, soweit nicht durch Rechtsvorschrift etwas anderes bestimmt ist.

#### § 24 Vollzug gegen Behörden und juristische Personen des öffentlichen Rechts

Anordnungen zur Erteilung von Auskünften gegenüber Behörden und juristischen Personen des öffentlichen Rechts können in den Fällen für die Erteilung von Auskünften für den Zensus mit Zwangsmitteln durchgesetzt werden.
In den übrigen Fällen der Anordnung zur Erteilung von Auskünften ist die Absicht der Einleitung eines Vollstreckungsverfahrens der zuständigen Aufsichtsbehörde anzuzeigen.
Die Vollstreckung darf erst einen Monat nach Zugang der Anzeige beginnen.

### Abschnitt 7  
Schlussbestimmungen

#### § 25 Verordnungsermächtigungen

(1) Die Landesregierung wird ermächtigt, durch Rechtsverordnung

1.  die Durchführung einer Landesstatistik nach § 6 oder die Erhebung einzelner Merkmale einer Landesstatistik auszusetzen, die Periodizität zu verlängern, Erhebungstermine zu verschieben sowie den Kreis der zu Befragenden einzuschränken, wenn die Ergebnisse nicht mehr oder nicht mehr in der ursprünglich vorgesehenen Ausführlichkeit oder Häufigkeit benötigt werden oder wenn tatsächliche Voraussetzungen für eine Landesstatistik entfallen sind oder sich wesentlich geändert haben;
2.  von der in einer Rechtsvorschrift nach § 6 vorgesehenen Befragung mit Auskunftspflicht zu einer Befragung ohne Auskunftspflicht überzugehen, wenn und soweit ausreichende Ergebnisse einer Landesstatistik auch durch Befragung ohne Auskunftspflicht erreicht werden können.

(2) Die Landesregierung hat durch Rechtsverordnung zu bestimmen, dass andere staatliche Stellen sowie Gemeinden und Gemeindeverbände Erhebungsstellen nach § 11 einzurichten oder in sonstiger Weise an der Durchführung amtlicher Statistiken mitzuwirken haben, wenn dies wegen der Art der Erhebung, der Zahl oder der räumlichen Verteilung der zu Befragenden oder zur Sicherung der Qualität der Erhebung erforderlich ist.
Die Verordnung nach Satz 1 muss insbesondere Regelungen zur Bestimmung der Erhebungsstellen, zu den Aufgaben der Erhebungsstellen und der Erhebungsbeauftragten und zur Sicherung des Statistikgeheimnisses durch Organisation und Verfahren enthalten.
In der Verordnung ist eine Regelung über die Deckung der Kosten und zum Erstattungsverfahren nach Absatz 4 zu treffen.

(3) Die Landesregierung hat durch Rechtsverordnung Näheres zu den Aufgaben der Erhebungsbeauftragten bei der Durchführung amtlicher Statistiken zu bestimmen.

(4) Die Landesregierung hat durch Rechtsverordnung, Bestimmungen über die Deckung der Kosten und den entsprechenden Ausgleich der Mehrbelastungen der Gemeinden und Gemeindeverbände infolge der mit der Durchführung amtlicher Statistiken verbundenen Aufgabenübertragungen nach § 20 Absatz 1 zu treffen.
Dabei bemisst sich der Mehrbelastungsausgleich nach der Art und dem Umfang der Einbindung der Gemeinden und Gemeindeverbände in die Aufgabenerfüllung nach § 11 Absatz 1 und kann in Abschlägen sowie als aufwandsabhängiger Betrag gewährt werden.
Die Verordnung nach Satz 1 muss insbesondere Näheres bestimmen über

1.  die Höhe des Erstattungsbetrages für die Einrichtung und den Betrieb der Erhebungsstellen (Sachaufwendungen),
2.  die Höhe des Erstattungsbetrages für Personalaufwendungen und damit verbundene angemessene Verwaltungsgemeinkosten, die in den Erhebungsstellen für die Erfüllung der Aufgaben entstehen,
3.  die Höhe der Aufwandsentschädigungen der Erhebungsbeauftragten, sofern Erhebungsbeauftragte eingesetzt werden,
4.  die Voraussetzungen für gesonderte Erstattungsleistungen, die bei kostenbewusster Aufgabenwahrnehmung noch keinen Ausgleich gefunden haben, und
5.  die Voraussetzungen und das Verfahren des Kostennachweises und der Kostenerstattung.

(5) Die Landesregierung kann die Ermächtigungen nach den Absätzen 1 bis 4 durch Rechtsverordnung auf das Mitglied der Landesregierung übertragen, das für die jeweilige Landesstatistik fachlich zuständig ist.
Sofern mehrere Mitglieder der Landesregierung fachlich zuständig sind, kann die Landesregierung ihnen gemeinsam die Ermächtigung übertragen.

#### § 26 Einschränkung von Grundrechten

(1) Durch § 10 Absatz 2 Satz 1, § 16 Absatz 1, § 17 Absatz 5, Absatz 6 Satz 1, Absatz 7 Satz 1 und Absatz 8 Satz 1, § 18 Absatz 2 sowie § 19 Absatz 2 Satz 2 wird das Grundrecht auf Datenschutz (Artikel 11 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

(2) Durch § 12 Absatz 1 wird das Grundrecht auf Berufsfreiheit (Artikel 49 Absatz 1 Satz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 27 Übergangsvorschrift

Bei Inkrafttreten dieses Gesetzes bestehende Landesstatistiken und Kommunalstatistiken, die nach diesem Gesetz einer Anordnung durch Rechtsvorschrift bedürfen, können bis zu zwei Jahre nach Inkrafttreten dieses Gesetzes auch ohne eine solche Anordnung weitergeführt werden; dabei sind die Bestimmungen zur Durchführung von Statistiken nach diesem Gesetz zu beachten.

#### § 28 Inkrafttreten, Außerkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt das Brandenburgische Statistikgesetz vom 11.
Oktober 1996 (GVBl.
I S. 294), das zuletzt durch Artikel 2 des Gesetzes vom 8.
Mai 2018 (GVBl.
I Nr. 8 S. 3) geändert worden ist, außer Kraft.

Potsdam, den 1.
April 2020

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke