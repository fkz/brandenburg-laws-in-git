## Gesetz zu dem Staatsvertrag der Länder Berlin und Brandenburg über die Errichtung und den Betrieb einer gemeinsamen Jugendarrestanstalt

Der Landtag hat das folgende Gesetz beschlossen:

### § 1

Dem am 10.
September 2015 vom Land Brandenburg unterzeichneten Staatsvertrag der Länder Berlin und Brandenburg über die Errichtung und den Betrieb einer gemeinsamen Jugendarrestanstalt wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel 8 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu machen.

Potsdam, den 17.
Dezember 2015

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag](/de/vertraege-249103)

* * *

### Anlagen

1

[Staatsvertrag der Länder Berlin und Brandenburg über die Errichtung und den Betrieb einer gemeinsamen Jugendarrestanstalt](/br2/sixcms/media.php/68/GVBl_I_37_2015-001Anlage-zum-Hauptdokument.pdf "Staatsvertrag der Länder Berlin und Brandenburg über die Errichtung und den Betrieb einer gemeinsamen Jugendarrestanstalt") 641.9 KB