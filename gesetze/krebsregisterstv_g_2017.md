## Gesetz zur Änderung des Staatsvertrages über das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen

#### § 1 

Dem am 29.
März 2017 vom Land Brandenburg unterzeichneten Zweiten Staatsvertrag zur Änderung des Staatsvertrages über das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

* * *

### Anlagen

1

[Zweiter Staatsvertrag zur Änderung des Staatsvertrages über das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen](/br2/sixcms/media.php/68/GVBl_I_19_2017_001Anlage-zum-Hauptdokument.pdf "Zweiter Staatsvertrag zur Änderung des Staatsvertrages über das Gemeinsame Krebsregister der Länder Berlin, Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt und der Freistaaten Sachsen und Thüringen") 739.8 KB