## Gesetz zu dem Staatsvertrag über die Gründung der GKL Gemeinsame Klassenlotterie der Länder (GKL-StV)

Der Landtag hat das folgende Gesetz beschlossen:

### § 1

Dem am 15.
Dezember 2011 und 19.
Januar 2012 unterzeichneten Staatsvertrag über die Gründung der GKL Gemeinsame Klassenlotterie der Länder (GKL-StV) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Vertrag nach seinem § 20 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 21.
Juni 2012

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Staatsvertrag](/vertraege/gkl_stv_2012)