## Gesetz über die elektronische Verwaltung im Land Brandenburg (Brandenburgisches E-Government-Gesetz - BbgEGovG)

Der Landtag hat das folgende Gesetz beschlossen:

**Inhaltsübersicht**

### Abschnitt 1  
Grundlagen

[§ 1 Geltungsbereich](#1)

[§ 2 Begriffsbestimmungen](#2)

### Abschnitt 2  
Elektronische Verwaltung

[§ 3 Elektronischer Zugang zur Verwaltung, Verordnungsermächtigungen](#3)

[§ 4 Informationen über die Behörden und Verfahren, elektronische Formulare, Verordnungsermächtigung](#4)

[§ 5 Elektronische Zahlungsmöglichkeiten und Rechnungstellungen, Verordnungsermächtigung\*)](#5)

[§ 6 Elektronische Nachweise](#6)

[§ 7 Elektronische Aktenführung und Akteneinsicht, Übertragung und Vernichtung von Papierdokumenten, Verordnungsermächtigungen](#7)

[§ 8 Verwaltungsprozessoptimierung](#8)

[§ 9 Georeferenzierung](#9)

[§ 10 Barrierefreiheit, Berücksichtigung der Sprachrechte der Sorben/Wenden](#10)

### Abschnitt 3  
IT-Infrastrukturen und IT-Standards

[§ 11 IT-Basiskomponenten, Verordnungsermächtigung](#11)

[§ 12 Umsetzung von Standardisierungsbeschlüssen des IT-Planungsrates](#12)

### Abschnitt 4  
IT-Organisation und Zusammenarbeit

[§ 13 IT-Beauftragte oder IT-Beauftragter](#13)

[§ 14 Zusammenarbeit des Landes und der Kommunen](#14)

[§ 15 IT-Rat](#15)

[§ 16 Informationssicherheit, CERT](#16)

### Abschnitt 5  
Schlussbestimmungen

[§ 17 Experimentierklausel](#17)

[§ 18 Evaluierung](#18)

[§ 19 Einschränkung von Grundrechten](#19)

[§ 20 Inkrafttreten](#20)

### Abschnitt 1  
Grundlagen

#### § 1 Geltungsbereich

(1) Dieses Gesetz gilt für die öffentlich-rechtliche Verwaltungstätigkeit der Behörden des Landes Brandenburg, der Gemeinden, Ämter und Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden juristischen Personen des öffentlichen Rechts, soweit nicht besondere Rechtsvorschriften des Landes Brandenburg inhaltsgleiche oder entgegenstehende Bestimmungen enthalten.
Die §§ 3 und 5 Absatz 2 und die §§ 6 und 7 sowie die Abschnitte 3 und 4 dieses Gesetzes gehen inhaltsgleichen oder entgegenstehenden Bestimmungen in anderen Rechtsvorschriften des Landes vor; Verfahrensrechte nach anderen Rechtsvorschriften bleiben unberührt.

(2) Dieses Gesetz gilt nicht für

1.  den Landtag und seine Verwaltung,
2.  die öffentlichen Schulen und Krankenhäuser,
3.  den Verfassungsschutz,
4.  Beliehene des Landes Brandenburg oder der Gemeinden, Ämter und Gemeindeverbände,
5.  die Tätigkeit der Finanzbehörden nach der Abgabenordnung,
6.  die Tätigkeit der Kirchen, der Religionsgesellschaften und Weltanschauungsgemeinschaften sowie ihrer Verbände und Einrichtungen,
7.  die Behörden bei Leistungs-, Eignungs- und ähnlichen Prüfungen von Personen,
8.  die Strafverfolgung, die Verfolgung und Ahndung von Ordnungswidrigkeiten, die Rechtshilfe für das Ausland in Straf- und Zivilsachen und, unbeschadet des § 80 Absatz 4 des Verwaltungsverfahrensgesetzes in der Fassung der Bekanntmachung vom 23.
    Januar 2003 (BGBl.
    I S. 102), das zuletzt durch Artikel 11 Absatz 2 des Gesetzes vom 18.
    Juli 2017 (BGBl. I S. 2745, 2752) geändert worden ist, für Maßnahmen des Richterdienstrechts sowie
9.  die Verwaltungstätigkeiten nach dem Sozialgesetzbuch.

(3) Für die Tätigkeit der Gerichtsverwaltungen und der Behörden der Justizverwaltung einschließlich der ihrer Aufsicht unterliegenden Körperschaften des öffentlichen Rechts gilt dieses Gesetz nur, soweit die Tätigkeit der Nachprüfung durch die Gerichte der Verwaltungsgerichtsbarkeit oder durch die in verwaltungsrechtlichen Anwalts- und Notarsachen zuständigen Gerichte unterliegt.

(4) Das E-Government-Gesetz vom 25.
Juli 2013 (BGBl.
I S. 2749), das zuletzt durch Artikel 1 des Gesetzes vom 5.
Juli 2017 (BGBl.
I S. 2206) geändert worden ist, findet nur beim Vollzug von Bundesrecht im Auftrag des Bundes Anwendung.

#### § 2 Begriffsbestimmungen

Im Sinne dieses Gesetzes bezeichnet

1.  „E-Government“ alle geschäftlichen Prozesse, die im Zusammenhang mit Regieren und Verwalten (Government) mit Hilfe der Informations- und Kommunikationstechniken (IT) über elektronische Medien abgewickelt werden,
2.  „Behörde“ jede Stelle, die Aufgaben der öffentlichen Verwaltung wahrnimmt,
3.  „Schriftform“ die durch oder aufgrund eines Gesetzes angeordnete Schriftform,
4.  „schriftlich“ die Einhaltung der Schriftform,
5.  „elektronischer Zugang zur Verwaltung“ die Ermöglichung der elektronischen Kommunikation mit den Behörden über öffentlich zugängliche Netze,
6.  „elektronische Akten“ Akten der öffentlichen Verwaltung, die in elektronischer Form geführt und bearbeitet werden,
7.  „elektronische Zahlungsverfahren“ jedes im elektronischen Geschäftsverkehr übliche Zahlungsverfahren,
8.  „elektronische Rechnungen“ Rechnungen, die in einem strukturierten elektronischen Format ausgestellt, übermittelt und empfangen werden, das ihre automatische und elektronische Verarbeitung ermöglicht,
9.  „IT-Basiskomponenten“ alle für E-Government verwaltungs- und verfahrensübergreifend sowie fachunabhängig benötigten informationstechnischen Systeme, Dienste und Infrastrukturen,
10.  „Servicekonto“ eine zentrale Identifizierungskomponente, die der einmaligen oder dauerhaften Identifizierung von Bürgerinnen, Bürgern, Unternehmen oder sonstigen Dritten zu Zwecken der Inanspruchnahme von Leistungen der Behörden dient und deren Verwendung für die Nutzer freiwillig ist.

### Abschnitt 2  
Elektronische Verwaltung

#### § 3 Elektronischer Zugang zur Verwaltung, Verordnungsermächtigungen

(1) Jede Behörde ist verpflichtet, auch einen elektronischen Zugang zur Verwaltung zu eröffnen.
Die Behörden stellen die dafür benötigten Informationen in elektronischer Form bereit.

(2) Die Übermittlung elektronischer Dokumente durch die Behörden ist zulässig, soweit der Empfänger hierfür einen Zugang eröffnet.
Die Behörden setzen für die von ihnen übermittelten elektronischen Dokumente ein geeignetes Verschlüsselungsverfahren ein und bieten für den elektronischen Zugang zur Verwaltung ein geeignetes Verschlüsselungsverfahren an.
Der elektronische Zugang zur Verwaltung darf nicht ohne wichtigen Grund von besonderen formalen oder technischen Anforderungen abhängig gemacht werden.
Die Landesregierung kann durch Rechtsverordnung Einzelheiten des elektronischen Zugangs, insbesondere zum Austausch und der Aufbewahrung elektronischer Nachrichten und Dokumente und zum Einsatz von Verschlüsselungsverfahren, sowie zu den damit verbundenen datenschutzrechtlichen Anforderungen regeln.

(3) In Verwaltungsverfahren, in denen die Feststellung der Identität einer Person verlangt wird, kann der Identitätsnachweis nach § 18 des Personalausweisgesetzes vom 18.
Juni 2009 (BGBl.
I S. 1346), das zuletzt durch Artikel 4 des Gesetzes vom 18.
Juli 2017 (BGBl.
I S. 2745, 2751) geändert worden ist, nach § 12 des eID-Karte-Gesetzes vom 21.
Juni 2019 (BGBl.
I S.
846) oder § 78 Absatz 5 des Aufenthaltsgesetzes in der Fassung der Bekanntmachung vom 25. Februar 2008 (BGBl.
I S. 162), das zuletzt durch Artikel 1 des Gesetzes vom 12.
Juli 2018 (BGBl. I S. 1147) geändert worden ist, erfolgen.
Die Aufgaben der Verwaltung der für die Identitätsfeststellung der Behörden erforderlichen Berechtigungen gemäß § 21 des Personalausweisgesetzes und der Erbringung von Identifizierungsdiensten gemäß § 21b des Personalausweisgesetzes sowie der Bereitstellung der für den Einsatz der Berechtigungszertifikate erforderlichen technischen Dienste (eID-Management) können durch eine zentrale Stelle wahrgenommen werden.
Die Landesregierung kann durch Rechtsverordnung die zentrale Stelle nach Satz 2 bestimmen und die für den ordnungsgemäßen Betrieb des eID-Managements und die Aufgabenwahrnehmung durch die zentrale Stelle erforderlichen technisch-organisatorischen und datenschutzrechtlichen Einzelheiten regeln sowie weitere, der Identitätsfeststellung nach Satz 1 gleichwertige Verfahren zulassen.

(4) In Verwaltungsverfahren, in denen die Schriftform vorgeschrieben ist, müssen die zuständigen Behörden mindestens eine elektronische Schriftformersetzungsmöglichkeit nach § 3a Absatz 2 des Verwaltungsverfahrensgesetzes in Verbindung mit § 1 Absatz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg vom 7.
Juli 2009 (GVBl.
I S. 262, 264), das zuletzt durch Artikel 6 des Gesetzes vom 8.
Mai 2018 (GVBl.
I Nr. 8 S. 4) geändert worden ist, anbieten.
Abweichend von § 3a Absatz 2 Satz 4 Nummer 4 des Verwaltungsverfahrensgesetzes können auch durch Rechtsverordnung der Landesregierung sonstige sichere Verfahren festgelegt werden, welche den Datenübermittler (Absender der Daten) authentifizieren und die Integrität des elektronisch übermittelten Datensatzes sowie die Barrierefreiheit gewährleisten.
Die Landesregierung kann durch Rechtsverordnung eine oder mehrere Schriftformersetzungsmöglichkeiten als einheitlichen Standard für das Angebot der Behörden festlegen.
Ist durch Rechtsvorschrift die Verwendung eines bestimmten Formulars vorgeschrieben, das ein Unterschriftsfeld vorsieht, wird allein dadurch nicht die Anordnung der Schriftform bewirkt.
Die Behörde hat über das Bestehen oder Nichtbestehen der Schriftform, die elektronische Schriftformersetzung und das oder die von ihr nach Satz 1 angebotenen Verfahren zu informieren.
Bei einer für die elektronische Übersendung an die Behörde bestimmten Fassung des Formulars bedarf es keiner handschriftlichen Unterschrift.

#### § 4 Informationen über die Behörden und Verfahren,<br>elektronische Formulare, Verordnungsermächtigung

(1) Die Behörden des Landes stellen über öffentlich zugängliche Netze in allgemein verständlicher Sprache Informationen über ihre Anschrift, ihre Geschäftszeiten, die postalischen, telefonischen und elektronischen Erreichbarkeiten, ihrer Ansprechstellen und Zuständigkeiten sowie ihre nach außen wirkende öffentlich-rechtliche Tätigkeit und die damit zusammenhängenden Verfahrensformalitäten bereit.
Die obersten Landesbehörden stellen mit Unterstützung einer zentralen Landesredaktion zu leistungsbegründenden Gesetzen und Verordnungen des Landes allgemeine Leistungsinformationen in standardisierter Form bereit und ergänzen die Leistungsinformationen des Bundes nach § 3 Absatz 2a des E-Government-Gesetzes im Hinblick auf Umsetzungsregelungen in Gesetzen oder Verordnungen des Landes, soweit noch keine Informationen in geeigneter Form elektronisch abrufbar sind.
Die zentrale Landesredaktion ist Ansprechpartner des Bundes und unterstützt die Bereitstellung ergänzender allgemeiner Leistungsinformationen durch die Gemeinden, Ämter und Gemeindeverbände.
Das für E-Government zuständige Mitglied der Landesregierung kann durch Rechtsverordnung im Einvernehmen mit dem für Finanzen zuständigen Mitglied der Landesregierung Einzelheiten zur Organisation, Ausstattung und Aufgabenerfüllung der Landesredaktion regeln.

(2) Formulare sollen in elektronisch bearbeitungsfähiger Form über öffentlich zugängliche Netze bereitgestellt werden.
Rechtsvorschriften, welche die Schriftform anordnen oder die elektronische Ersetzung der Schriftform regeln, bleiben unberührt.

(3) Elektronische Informationen und Formulare sollen nutzerorientiert optimiert und vereinfacht werden.

#### § 5 Elektronische Zahlungsmöglichkeiten und Rechnungstellungen, Verordnungsermächtigung<a name="a"></a><a href="#aa"><sup>*)</sup></a>

(1) Die Behörden bieten für die Begleichung ihrer Geldansprüche mindestens ein elektronisches Zahlungsverfahren an.

(2) Auftraggeber im Sinne des Teils 4 des Gesetzes gegen Wettbewerbsbeschränkungen in der Fassung der Bekanntmachung vom 26.
Juni 2013 (BGBl.
I S.
1750, 3245), das zuletzt durch Artikel 1 des Gesetzes vom 25.
März 2020 (BGBl.
I S.
674) geändert worden ist, stellen den Empfang und die Verarbeitung elektronischer Rechnungen sicher, soweit für sie gemäß § 159 des Gesetzes gegen Wettbewerbsbeschränkungen die Vergabekammer des Landes Brandenburg zuständig ist.
Das für Finanzen zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit dem für E-Government zuständigen Mitglied der Landesregierung durch Rechtsverordnung besondere Vorschriften zur Ausgestaltung des elektronischen Rechnungsverkehrs zu erlassen.
Diese können sich beziehen auf

1.  die Art und Weise der Verarbeitung der elektronischen Rechnung, insbesondere die elektronische Verarbeitung,
2.  die Anforderungen an die elektronische Rechnungstellung, und zwar insbesondere auf die von den elektronischen Rechnungen zu erfüllenden Voraussetzungen, den Schutz personenbezogener Daten, das zu verwendende Rechnungsdatenmodell sowie auf die Verbindlichkeit der elektronischen Form,
3.  die Befugnis von öffentlichen Auftraggebern, in Ausschreibungsbedingungen die Erteilung elektronischer Rechnungen vorzusehen,
4.  die Ausnahmen für sicherheitsspezifische Aufträge im Sinne des § 104 des Gesetzes gegen Wettbewerbsbeschränkungen sowie
5.  die Erstreckung der Verpflichtungen nach Satz 1 auf den unterschwelligen Vergabebereich.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[\*)](#a) § 5 Absatz 2 dieses Gesetzes dient der Umsetzung der Richtlinie 2014/55/EU des Europäischen Parlaments und des Rates vom 16.
April 2014 über die elektronische Rechnungsstellung bei öffentlichen Aufträgen (ABl. L 133 vom 6.5.2014, S.
1).

#### § 6 Elektronische Nachweise

(1) Wird ein Verwaltungsverfahren ganz oder teilweise elektronisch durchgeführt, können die vorzulegenden Nachweise elektronisch eingereicht werden, es sei denn, dass durch Rechtsvorschrift etwas anderes bestimmt ist oder die Behörde für bestimmte Verfahren oder im Einzelfall nach pflichtgemäßem Ermessen die Vorlage des Originals verlangt.

(2) Die zuständige Behörde kann erforderliche Nachweise, die von einer deutschen öffentlichen Stelle stammen, auch auf Grundlage einer Einwilligung der oder des Verfahrensbeteiligten im Sinne des Artikels 4 Nummer 11 in Verbindung mit Artikel 7 der Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates vom 27.
April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr und zur Aufhebung der Richtlinie 95/46/EG (Datenschutz-Grundverordnung) (ABl.
L 119 vom 4.5.2016, S. 1; L 314 vom 22.11.2016, S. 72) direkt bei der ausstellenden öffentlichen Stelle elektronisch einholen.

#### § 7 Elektronische Aktenführung und Akteneinsicht,<br>Übertragung und Vernichtung von Papierdokumenten, Verordnungsermächtigungen

_(1) Die Behörden des Landes führen ihre Akten grundsätzlich elektronisch.
Dies gilt nicht für die Landrätinnen und Landräte sowie Oberbürgermeisterinnen und Oberbürgermeister als untere Landesbehörden und sonstige kommunale oder nichtstaatliche Stellen, die Aufgaben als untere Landesbehörden oder Beauftragte des Landes wahrnehmen.
Andere als die in Satz 1 genannten Behörden sowie die nach Satz 2 ausgenommenen Stellen können ihre Akten nach den Vorschriften dieses Gesetzes elektronisch führen._

_(2) Führen die Behörden ihre Akten elektronisch, bewahren sie an Stelle von Papierdokumenten deren elektronische Wiedergabe in der elektronischen Akte auf.
Ergänzend zu den Artikeln 24, 25 und 32 der Verordnung (EU) 2016/679 ist durch geeignete technisch-organisatorische Maßnahmen nach dem Stand der Technik sicherzustellen, dass_

1.  _die Grundsätze ordnungsgemäßer Aktenführung eingehalten werden,_
2.  _die gespeicherten Daten vor Informationsverlust sowie unberechtigten Zugriffen und Veränderungen geschützt sind und_
3.  _die in der Akte enthaltenen elektronischen Dokumente und Informationen zur Erhaltung ihrer Lesbarkeit in ein anderes elektronisches Format übertragen werden können._

_Die Behörden, die ihre Akten elektronisch führen, sollen untereinander Akten, Vorgänge und Dokumente elektronisch übermitteln; Satz 2 Nummer 2 gilt entsprechend.
Die datenschutzrechtlichen Bestimmungen zur Verarbeitung personenbezogener Daten bleiben unberührt._

_(3) Bei der Übertragung von Papierdokumenten in elektronische Dokumente ist nach dem Stand der Technik sicherzustellen, dass die elektronischen Dokumente mit den Papierdokumenten bildlich und inhaltlich übereinstimmen.
Wird die Übereinstimmung durch elektronischen Vermerk bestätigt und sind das elektronische Dokument und der Vermerk mit einer qualifizierten elektronischen Signatur oder einem qualifizierten elektronischen Siegel nach Artikel 3 Nummer 12 und 27 der Verordnung Nr. 910/2014 des Europäischen Parlaments und des Rates vom 23.
Juli 2014 über elektronische Identifizierung und Vertrauensdienste für elektronische Transaktionen im Binnenmarkt und zur Aufhebung der Richtlinie 1999/93/EG (ABl.
L 257 vom 28.8.2014, S. 73; L 23 vom 29.1.2015, S. 19; L 155 vom 14.6.2016, S. 44) verbunden, findet § 33 Absatz 6 des Verwaltungsverfahrensgesetzes auf das elektronische Dokument entsprechend Anwendung.
Papierdokumente sollen frühestens sechs Monate nach der Übertragung in elektronische Dokumente vernichtet werden, soweit keine entgegenstehenden Rechtspflichten zur Rückgabe oder weiteren Aufbewahrung bestehen._

_(4) Absatz 2 Satz 1 und Absatz 3 gelten entsprechend für in anderer körperlicher Form geführte Dokumente sowie für elektronische Dokumente, die zur Sicherung ihrer Nutzung in neue Formate umgewandelt werden._[1](#hh)

(5) Das für E-Government zuständige Mitglied der Landesregierung kann durch Rechtsverordnung nähere Einzelheiten zu den technisch-organisatorischen Maßnahmen, zur elektronischen Aktenübermittlung und Nutzbarkeit der eingesetzten Verfahren für Menschen mit Behinderungen sowie zur Übertragung von Papierdokumenten nach den Absätzen 2 bis 4 festlegen.
Über die Absätze 2 bis 4 hinausgehende Anforderungen an die elektronische Aktenführung in anderen Rechtsvorschriften des Landes bleiben unberührt.
Das für Justiz zuständige Mitglied der Landesregierung kann im Einvernehmen mit dem für E-Government zuständigen Mitglied der Landesregierung durch Rechtsverordnung abweichend von § 20 Absatz 6 für die Gerichtsverwaltungen und die Behörden der Justizverwaltung einen späteren Zeitpunkt für die Verpflichtung zur elektronischen Aktenführung bestimmen, jedoch nicht später als vier Jahre nach dem in § 20 Absatz 6 festgelegten Zeitpunkt.

_(6) Soweit ein Recht auf Akteneinsicht besteht, kann die Einsichtnahme in elektronische Akten dadurch bewirkt werden, dass_

1.  _der lesende elektronische Zugang ermöglicht wird,_
2.  _elektronische Dokumente aus der elektronischen Akte übermittelt werden oder_
3.  _Ausdrucke aus der elektronischen Akte zur Verfügung gestellt werden._[2](#ii)

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
[1](#h) § 7 Absatz 1 bis 4 (Kursivdruck) tritt gemäß § 20 Absatz 6 des Gesetzes vom 23. November 2018 (GVBl. I Nr. 28) am 1.
November 2024 in Kraft.
  
[2](#i) § 7 Absatz 6 (Kursivdruck) tritt gemäß § 20 Absatz 6 des Gesetzes vom 23. November 2018 (GVBl. I Nr. 28) am 1.
November 2024 in Kraft.

#### § 8 Verwaltungsprozessoptimierung

Die Behörden des Landes sollen Verwaltungsabläufe, die erstmals elektronisch unterstützt werden, vor Einführung der informationstechnischen Systeme mit dem Ziel der Vereinfachung und Medienbruchfreiheit anhand gängiger Methoden dokumentieren, analysieren und optimieren.
Satz 1 gilt entsprechend bei allen wesentlichen Änderungen der Verwaltungsabläufe oder der eingesetzten informationstechnischen Systeme.

#### § 9 Georeferenzierung

(1) Wird ein elektronisches Register, das Angaben mit Bezug zu Grundstücken in Brandenburg enthält, neu aufgebaut oder überarbeitet, hat die Behörde in das Register eine landesweit einheitlich festgelegte direkte Georeferenzierung (Koordinate) zu dem jeweiligen Flurstück, dem Gebäude oder zu einem in einer Rechtsvorschrift definierten Gebiet aufzunehmen, auf welches sich die Angaben beziehen.

(2) Register im Sinne dieses Gesetzes sind solche, für die Daten aufgrund von Rechtsvorschriften des Landes erhoben oder gespeichert werden; dies können öffentliche und nichtöffentliche Register sein.
Für Register, für die Daten aufgrund von Rechtsvorschriften des Bundes erhoben oder gespeichert werden, gilt § 14 des E-Government-Gesetzes.

(3) Die Behörden des Landes nutzen für die einheitliche Georeferenzierung einen Georeferenzierungsdienst, der durch den Landesbetrieb Landesvermessung und Geobasisinformation Brandenburg zentral zur Verfügung gestellt wird.
Den Gemeinden, Ämtern und Gemeindeverbänden sowie den sonstigen der Aufsicht des Landes unterstehenden juristischen Personen des öffentlichen Rechts wird der Georeferenzierungsdienst zur kostenfreien Nutzung angeboten.

#### § 10 Barrierefreiheit, Berücksichtigung der Sprachrechte der Sorben/Wenden

(1) Die Träger der öffentlichen Verwaltung nach § 1 Absatz 1 Satz 1 sollen die elektronische Kommunikation und die Bereitstellung sowie die Verwendung elektronischer Dokumente so ausgestalten, dass sie auch von Menschen mit Behinderungen uneingeschränkt genutzt werden können.
§ 9 Absatz 1 des Brandenburgischen Behindertengleichstellungsgesetzes vom 11. Februar 2013 (GVBl.
I Nr. 5), das durch Artikel 4 des Gesetzes vom 20.
September 2018 (GVBl. I Nr. 21 S. 5) geändert worden ist, gilt entsprechend.

(2) Durch die Träger der öffentlichen Verwaltung nach § 1 Absatz 1 Satz 1 ist die elektronische Kommunikation und die Bereitstellung sowie die Verwendung elektronischer Dokumente entsprechend § 8 Absatz 2 des Sorben/Wenden-Gesetzes vom 7.
Juli 1994 (GVBl.
I S.
294), das zuletzt durch Artikel 2 des Gesetzes vom 15.
Oktober 2018 (GVBl.
I Nr. 23) geändert worden ist, in der jeweils geltenden Fassung, auszugestalten.

### Abschnitt 3  
IT-Infrastrukturen und IT-Standards

#### § 11 IT-Basiskomponenten, Verordnungsermächtigung

(1) Die Behörden des Landes nutzen zur Erfüllung ihrer Verpflichtungen nach Abschnitt 2 gemeinsame IT-Basiskomponenten, die vom Land bereitgestellt werden.
Sie sind verpflichtet, die für die Einrichtung und den Betrieb der IT-Basiskomponenten erforderlichen Daten und Informationen elektronisch zur Verfügung zu stellen.
Zu den IT-Basiskomponenten gehören

1.  das Landesverwaltungsnetz,
2.  die elektronische Vergabeplattform des Landes Brandenburg,
3.  die virtuelle Poststelle der Landesverwaltung,
4.  das Verwaltungsdiensteverzeichnis der Deutschen Verwaltung,
5.  ein Multikanal-Nachrichtensammel- und -protokollierungsdienst,
6.  ein elektronisches Identitätsmanagement (eID-Service),
7.  eine elektronische Bezahlplattform,
8.  ein zentraler Zugang für die Nutzung von De-Mail-Diensten,
9.  ein Landesserviceportal mit Servicekonten,
10.  ein Langzeitspeichersystem.

Die Verpflichtung nach Satz 1 besteht nicht, soweit das Land Brandenburg im Rahmen der Zusammenarbeit mehrerer Länder oder der Länder und des Bundes gemeinschaftlich im E-Justice-Rat abgestimmte und finanzierte IT-Basiskomponenten einsetzt, die den IT-Basiskomponenten des Landes nach Satz 3 vergleichbare technische Funktionalitäten beinhalten.

(2) Die Landesregierung regelt durch Rechtsverordnung die Einzelheiten zum Betrieb, zur Bereitstellung, Nutzung, Sicherheit und den technischen Standards der IT-Basiskomponenten des Landes sowie zu den damit zusammenhängenden Aufgaben und Mitwirkungspflichten der Behörden und den datenschutzrechtlichen Anforderungen und kann dabei weitere Ausnahmen von der Verpflichtung nach Absatz 1 Satz 1 festlegen oder das Verfahren zur Festlegung weiterer Ausnahmen innerhalb der Landesregierung bestimmen; die Ausnahmen sind auf den Einzelfall zu beschränken und nur zulässig, soweit sich das Land Brandenburg im Rahmen der Zusammenarbeit mehrerer Länder oder der Länder und des Bundes an der Entwicklung und Finanzierung gemeinschaftlicher IT-Basiskomponenten beteiligt, die den IT-Basiskomponenten des Landes nach Absatz 1 Satz 3 vergleichbare technische Funktionalitäten beinhalten.
In der Rechtsverordnung nach Satz 1 kann auch die Einrichtung weiterer IT-Basiskomponenten vorgesehen werden.
Weitere IT-Basiskomponenten des Landes sollen eingerichtet werden, wenn das Land aufgrund von Rechtsvorschriften der Europäischen Union, des Bundes oder eines Staatsvertrages zur Bereitstellung elektronischer Verfahren, zum Einsatz von IT-Komponenten oder zur Einhaltung von IT-Standards verpflichtet ist und die Entwicklung und der Betrieb von IT-Basiskomponenten zur Erfüllung dieser Verpflichtungen wirtschaftlich ist.

(3) Die Einrichtung und der Betrieb der IT-Basiskomponenten des Landes erfolgen durch den Brandenburgischen IT-Dienstleister.
Dieser kann sich zur Aufgabenerfüllung der Leistungen Dritter bedienen und Dritte mit dem Betrieb einzelner IT-Basiskomponenten beauftragen.
In diesen Fällen bleibt er für die ordnungsgemäße Aufgabenerledigung und die Sicherstellung der Betreuung der Behörden verantwortlich.

(4) Für die Bereitstellung einschließlich des Betriebes und der Weiterentwicklung der IT-Basiskomponenten werden die erforderlichen Landesmittel im Landeshaushalt zentral veranschlagt.

#### § 12 Umsetzung von Standardisierungsbeschlüssen des IT-Planungsrates

Vom IT-Planungsrat verbindlich beschlossene fachunabhängige und fachübergreifende IT-Interoperabilitäts- oder IT-Sicherheitsstandards gemäß § 1 Absatz 1 Satz 1 Nummer 2 und § 3 des Vertrages über die Errichtung des IT-Planungsrates und über die Grundlagen der Zusammenarbeit beim Einsatz der Informationstechnologie in den Verwaltungen von Bund und Ländern – Vertrag zur Ausführung von Artikel 91c GG (GVBl.
2010 I Nr. 9 S. 2) sind nach Ablauf der vom IT-Planungsrat festgelegten Fristen durch die Behörden des Landes, der Gemeinden, Ämter und Gemeindeverbände und sonstigen der Aufsicht des Landes unterstehenden juristischen Personen des öffentlichen Rechts bei den von ihnen eingesetzten informationstechnischen Systemen einzuhalten.

### Abschnitt 4  
IT-Organisation und Zusammenarbeit

#### § 13 IT-Beauftragte oder IT-Beauftragter

(1) Die Landesregierung bestellt eine Beauftragte oder einen Beauftragten der Landesregierung für Informationstechnologie (IT-Beauftragte oder IT-Beauftragter).
Die oder der IT-Beauftragte steuert und koordiniert die Informationstechnik und das E-Government in der Landesverwaltung und legt die technischen und organisatorischen Rahmenbedingungen für den Einsatz der Informationstechnik in der Landesverwaltung in Abstimmung mit der Ministerpräsidentin oder dem Ministerpräsidenten und den Ministerien fest.
Die Aufgaben der oder des IT-Beauftragten werden vorbehaltlich abweichender Festlegung der Landesregierung von der Staatssekretärin oder dem Staatssekretär in der für E-Government zuständigen obersten Landesbehörde wahrgenommen.

(2) Der oder dem IT-Beauftragten werden folgende Aufgaben übertragen:

1.  die Vertretung des Landes im IT-Planungsrat,
2.  die Steuerung und Koordinierung der Zusammenarbeit mit der Europäischen Union, dem Bund, den Ländern, den Kommunen des Landes Brandenburg sowie sonstigen Dritten in Angelegenheiten des E-Governments und der Informationstechnologie von wesentlicher oder ressortübergreifender Bedeutung,
3.  die einheitliche Weiterentwicklung und Steuerung der Umsetzung der E-Government- und IT-Strategie des Landes,
4.  die Evaluierung dieses Gesetzes und Vorbereitung des Berichts der Landesregierung nach § 18,
5.  die Steuerung und Koordinierung der Umsetzung des Onlinezugangsgesetzes vom 14. August 2017 (BGBl. I S. 3122, 3138) im Land Brandenburg.

(3) Das für E-Government zuständige Mitglied der Landesregierung oder bei abweichender Festlegung nach Absatz 1 Satz 3 die Landesregierung legt durch Rechtsverordnung die Befugnisse und Einzelheiten zur Durchführung der Aufgaben der oder des IT-Beauftragten fest.

#### § 14 Zusammenarbeit des Landes und der Kommunen

(1) Das Land und die Kommunen arbeiten bei der Verwirklichung der elektronischen Verwaltung kooperativ mit dem Ziel einheitlicher Infrastrukturen und Standards zusammen.
Das Land, die Gemeinden und Gemeindeverbände und sonstige der Aufsicht des Landes unterstehende juristische Personen des öffentlichen Rechts können sich gegenseitig IT-Basiskomponenten zur Nutzung überlassen und schließen im Einvernehmen mit der oder dem IT-Beauftragten der Landesregierung entsprechende Vereinbarungen zur Nutzung ab.

(2) Das Land stellt den Gemeinden, Ämtern und Gemeindeverbänden und den sonstigen der Aufsicht des Landes unterstehenden juristischen Personen des öffentlichen Rechts für die Erfüllung ihrer Verpflichtungen nach den §§ 3 bis 6 und nach dem Onlinezugangsgesetz die IT-Basiskomponenten nach § 11 zur kostenfreien Mitnutzung bereit.
§ 11 Absatz 1 Satz 2 und Absatz 4 gilt entsprechend.

#### § 15 IT-Rat

(1) Für die informationstechnische Zusammenarbeit von Land und Kommunen wird der IT-Rat eingerichtet.
Dem IT-Rat gehören für das Land die Chefin oder der Chef der Staatskanzlei, die oder der IT-Beauftragte und die Staatssekretärinnen oder Staatssekretäre der für Finanzen und Wirtschaft zuständigen obersten Landesbehörden sowie jeweils zwei Vertreterinnen oder Vertreter des Städte- und Gemeindebundes Brandenburg und des Landkreistages Brandenburg an.
Den Vorsitz führt die oder der IT-Beauftragte.
An den Sitzungen des IT-Rates nimmt mit ausschließlich beratendem Status eine Vertreterin oder ein Vertreter des Brandenburgischen IT-Dienstleisters teil.
Der IT-Rat kann bei Bedarf Vertreterinnen oder Vertreter von Gemeinden, Ämtern oder Gemeindeverbänden oder sonstige Dritte beratend hinzuziehen und Arbeitsgruppen zur Unterstützung seiner Tätigkeit einsetzen.
Berührt die Zusammenarbeit von Land und Kommunen den Geschäftsbereich einer obersten Landesbehörde, ist diese zu beteiligen.

(2) Der IT-Rat ist in allen Angelegenheiten zu beteiligen, die für die Ebenen übergreifende Kooperation in der Informationstechnik von Bedeutung sind.
Dazu gehören insbesondere

1.  Themen des IT-Planungsrates mit Ebenen übergreifendem Bezug,
2.  die Umsetzung von Standardisierungsbeschlüssen des IT-Planungsrates im Sinne von § 12,
3.  die Weiterentwicklung der IT- und der E-Government-Strategie des Landes,
4.  die nach § 14 vom Land und den Gemeinden, Ämtern und Gemeindeverbänden gegenseitig überlassenen und gemeinsam genutzten IT-Basiskomponenten,
5.  die näheren Anforderungen an die Sicherheitskonzepte der Behörden nach § 16 Absatz 1 Satz 2,
6.  IT-Interoperabilitäts- und IT-Sicherheitsstandards für die Ebenen übergreifende Kommunikation, soweit der IT-Planungsrat hierzu nicht bereits verbindliche Standards beschlossen hat,
7.  Projektvorschläge der Behörden des Landes und der Kommunen für Ebenen übergreifende IT-Verfahren.

(3) Der IT-Rat beschließt Empfehlungen und Hinweise einstimmig.
Beschlüsse in Angelegenheiten nach Absatz 2 Satz 2 Nummer 4 und 6 sollen bei dem Erlass oder der Änderung von Verordnungen nach § 3 Absatz 2 Satz 4, Absatz 3 Satz 3 und Absatz 4 Satz 3, § 7 Absatz 5 Satz 1 und § 11 Absatz 2 berücksichtigt werden.

(4) Für die Umsetzung von IT-Interoperabilitäts- und IT-Sicherheitsstandards und die Durchführung von Projekten nach Absatz 2 Satz 2 Nummer 6 und 7 werden durch das Land Finanzmittel nach Maßgabe des Haushalts bereitgestellt.
Über die Verwendung der Finanzmittel entscheidet der IT-Rat durch Beschluss unter Beachtung der Grundätze der Sparsamkeit und Wirtschaftlichkeit und der geltenden Regelungen des Haushaltsrechts.

(5) Für die Geschäftsführung des IT-Rates wird bei der für E-Government zuständigen obersten Landesbehörde eine Geschäftsstelle eingerichtet.

#### § 16 Informationssicherheit, CERT

(1) Die Sicherheit der informationstechnischen Systeme der Behörden ist im Rahmen der Verhältnismäßigkeit zu gewährleisten.
Die Behörden treffen zu diesem Zweck angemessene technische und organisatorische Maßnahmen und erstellen die erforderlichen Sicherheitskonzepte unter Berücksichtigung der datenschutzrechtlichen Vorgaben.

(2) Zur Unterstützung und Beratung der an das Landesverwaltungsnetz Brandenburg angeschlossenen Behörden besteht für sicherheitsrelevante Vorfälle in IT-Systemen ein Computersicherheits-Ereignis- und Reaktionsteam (CERT).
Es sammelt und bewertet die zur Abwehr von Gefahren für die Sicherheit der Informationstechnik des Landes erforderlichen Daten und analysiert dabei insbesondere Sicherheitslücken, Schadprogramme und Vorgehensweisen bei Angriffen auf die Informationstechnik des Landes.
Die an das Landesverwaltungsnetz Brandenburg angeschlossenen Behörden melden dem CERT sicherheitsrelevante Vorfälle.
Das CERT spricht Warnungen und Empfehlungen gegenüber den Behörden des Landes und der Kommunen aus und informiert die für Informationssicherheit zuständigen Stellen des Bundes und der Länder über ihm bekannt gewordene sicherheitsrelevante Vorfälle.
Die Aufgaben des CERT werden durch den Brandenburgischen IT-Dienstleister wahrgenommen.

(3) Für die in Absatz 2 Satz 2 genannten Aufgaben dürfen vom CERT Protokolldaten, die beim Betrieb der Kommunikationstechnik des Landes anfallen, sowie die an den Schnittstellen der Kommunikationstechnik des Landes anfallenden Daten erhoben und ausschließlich automatisiert ausgewertet werden, soweit dies zur Verhinderung und Abwehr von Angriffen auf die Informationstechnik des Landes oder zum Erkennen und Beseitigen technischer Störungen oder Fehler erforderlich ist.
Die Daten sind nach der Erhebung zu pseudonymisieren, soweit dies automatisiert möglich ist.
Durch organisatorische und technische Maßnahmen ist sicherzustellen, dass eine Auswertung der gespeicherten Daten nur automatisiert erfolgt.
Die automatisierte Auswertung erfolgt unverzüglich; danach sind die Daten sofort und spurenlos zu löschen.
Abweichend von Satz 4 dürfen die Daten für längstens drei Monate gespeichert werden, wenn die Voraussetzungen nach Absatz 4 Satz 1 vorliegen.

(4) Eine über die automatisierte Auswertung nach Absatz 3 hinausgehende Verwendung von Daten ist nur zulässig, wenn bestimmte Tatsachen den Verdacht begründen, dass

1.  diese ein Schadprogramm enthalten,
2.  diese durch ein Schadprogramm übermittelt wurden oder
3.  sich aus ihnen Hinweise auf ein Schadprogramm ergeben können,

und soweit die Datenverarbeitung zur Bestätigung oder Widerlegung des Verdachts erforderlich ist.
Bei Bestätigung des Verdachts ist die weitere Verarbeitung der Daten zulässig, sofern dies

1.  zur Abwehr des Schadprogramms,
2.  zur Abwehr von Gefahren, die von dem aufgefundenen Schadprogramm ausgehen, oder
3.  zur Erkennung und Abwehr anderer Schadprogramme erforderlich ist.

Ein Schadprogramm kann beseitigt oder in seiner Funktionsweise gehindert werden.
Die nicht automatisierte Verwendung von Daten darf nur durch Bedienstete mit der Befähigung zum Richteramt angeordnet werden.
Soweit nach den Sätzen 1, 2 oder Absatz 5 die Wiederherstellung des Personenbezugs von nach Absatz 3 Satz 2 pseudonymisierten Daten erforderlich oder diese aufgrund besonderer bundes- oder landesrechtlicher Rechtsvorschriften zulässig ist, muss sie durch die Leiterin oder den Leiter des Brandenburgischen IT-Dienstleisters angeordnet werden.
Anordnungen nach den Sätzen 4 und 5 sind zu protokollieren; die Protokollierung soll binnen drei Tagen erfolgen.

(5) Von einer Maßnahme nach Absatz 3 oder Absatz 4 betroffene Beteiligte eines Kommunikationsvorgangs sind spätestens nach dem Erkennen oder der Abwehr eines Schadprogramms oder der davon ausgehenden Gefahren zu benachrichtigen, wenn sie bekannt sind und nicht überwiegende schutzwürdige Belange Dritter entgegenstehen.

(6) Die Regelungen zur Datenverarbeitung nach den Absätzen 3 und 4 sowie die Informationspflichten nach Absatz 5 gelten für die Verarbeitung der in Absatz 2 Satz 2 genannten Daten nur, sofern diese personenbezogene oder dem Fernmeldegeheimnis unterliegende Daten beinhalten.
Daten nach Satz 1 dürfen nicht weitergehend oder für andere Zwecke als nach den Absätzen 3 und 4 verarbeitet werden.
Die Zulässigkeit ihrer Übermittlung an die Strafverfolgungsbehörden und Polizeien sowie andere Behörden oder Stellen des Bundes und der Länder richtet sich nach den für diese geltenden gesetzlichen Ermächtigungen.
Von Übermittlungen nach Satz 3 sind Beteiligte eines Kommunikationsvorgangs entsprechend Absatz 5 zu unterrichten.

### Abschnitt 5  
Schlussbestimmungen

#### § 17 Experimentierklausel

(1) Zum Ausbau der elektronischen Verwaltung kann das jeweils fachlich zuständige Mitglied der Landesregierung im Benehmen mit der oder dem IT-Beauftragten und nach Zustimmung des für Inneres und Kommunales zuständigen Mitgliedes der Landesregierung durch Rechtsverordnung für den Zeitraum von höchstens drei Jahren sachlich begrenzte Abweichungen von folgenden Zuständigkeits-, Form- oder Verfahrensvorschriften zulassen:

1.  Zuständigkeits- und Formvorschriften gemäß § 1 Absatz 1 Satz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg in Verbindung mit den §§ 3, 3a, 33, 34, 37 Absatz 2 bis 5, §§ 41, 57, 64 und 69 Absatz 2 des Verwaltungsverfahrensgesetzes,
2.  § 1 Absatz 1 des Verwaltungszustellungsgesetzes für das Land Brandenburg vom 18. Oktober 1991 (GVBl.
    S. 457), das zuletzt durch Artikel 16 des Gesetzes vom 28. Juni 2006 (GVBl.
    I S. 74, 86) geändert worden ist, in Verbindung mit § 5 Absatz 4 bis 7, §§ 5a und 10 Absatz 2 des Verwaltungszustellungsgesetzes vom 12.
    August 2005 (BGBl. I S. 2354), das zuletzt durch Artikel 11 Absatz 3 des Gesetzes vom 18.
    Juli 2017 (BGBl. I S. 2745, 2752) geändert worden ist,
3.  sonstige landesgesetzliche Zuständigkeits-, Form- oder Verfahrensvorschriften, soweit dies zur Erprobung neuer elektronischer Formen des Schriftformersatzes, der Übermittlung und Bekanntgabe von Dokumenten oder Erklärungen, der Vorlage von Nachweisen, der Erhebung, Verarbeitung, Nutzung oder Weitergabe von Daten oder für die Erprobung der Dienste von zentralen Portalen erforderlich ist.

Zuständigkeitsänderungen nach Satz 1 Nummer 3 mit Auswirkungen auf die Kommunen bedürfen der Zustimmung des IT-Rates durch einstimmigen Beschluss.

(2) Führen die Abweichungen zu Mehrbelastungen für den Landeshaushalt, bedarf es des Einvernehmens mit der für Finanzen zuständigen obersten Landesbehörde.

#### § 18 Evaluierung

Die Landesregierung berichtet dem Landtag binnen fünf Jahren nach Inkrafttreten gemäß § 20 Absatz 1 über die erzielten Wirkungen und unterbreitet ihm Vorschläge für die Weiterentwicklung.

#### § 19 Einschränkung von Grundrechten

Durch § 16 Absatz 3 und 4 werden das Recht auf Datenschutz (Artikel 11 Absatz 1 der Verfassung des Landes Brandenburg) und der Schutz des Fernmeldegeheimnisses (Artikel 16 Absatz 1 der Verfassung des Landes Brandenburg und Artikel 10 Absatz 1 des Grundgesetzes) eingeschränkt.

#### § 20 Inkrafttreten

(1) Dieses Gesetz tritt vorbehaltlich der Absätze 2 bis 6 am Tag nach der Verkündung in Kraft.

(2) § 3 Absatz 2 Satz 2 und Absatz 4 Satz 1, § 5 Absatz 1 und § 16 Absatz 1 Satz 2 treten am 1. November 2020 in Kraft.

(3) § 3 Absatz 3 Satz 1 tritt am 1. Juli 2019 in Kraft.

(4) § 4 Absatz 1 Satz 2 und 3 tritt am 1.
November 2021 in Kraft.

(5) § 5 Absatz 2 Satz 1 tritt am 1.
April 2020 in Kraft.

(6) § 7 Absatz 1 bis 4 und 6 tritt am 1.
November 2024 in Kraft.

Potsdam, den 23.
November 2018

Die Präsidentin  
des Landtages Brandenburg

Britta Stark