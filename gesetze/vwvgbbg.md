## Verwaltungsvollstreckungsgesetz für das Land Brandenburg (VwVGBbg)

Der Landtag hat das folgende Gesetz beschlossen:

Inhaltsübersicht
----------------

### Abschnitt 1  
Allgemeine Vorschriften

[§ 1 Geltungsbereich des Gesetzes](#1)  
[§ 2 Vollstreckung durch Behörden der Finanz- und Justizverwaltung](#2)  
[§ 3 Allgemeine Voraussetzungen der Vollstreckung](#3)  
[§ 4 Vollstreckungshilfe](#4)  
[§ 5 Gemeinsame Aufgabenwahrnehmung](#5)  
[§ 6 Vollstreckungsschuldner](#6)  
[§ 7 Vollstreckung gegen juristische Personen des öffentlichen Rechts](#7)  
[§ 8 Vollstreckungsdienstkraft](#8)  
[§ 9 Vollstreckungsausweis](#9)  
[§ 10 Befugnisse der Vollstreckungsdienstkraft](#10)  
[§ 11 Zuziehung von Zeuginnen oder Zeugen](#11)  
[§ 12 Zeit der Vollstreckung](#12)  
[§ 13 Einstellung oder Beschränkung der Vollstreckung](#13)  
[§ 14 Einstweilige Einstellung oder Beschränkung der Vollstreckung](#14)  
[§ 15 Einwendungen gegen die Vollstreckung](#15)  
[§ 16 Aufhebung der aufschiebenden Wirkung](#16)

### Abschnitt 2  
Vollstreckung von öffentlich-rechtlichen Geldforderungen

[§ 17 Vollstreckungsbehörden](#17)  
[§ 18 Aufsicht bei der Vollstreckung von Geldforderungen des Landes](#18)  
[§ 19 Besondere Voraussetzungen der Beitreibung](#19)  
[§ 20 Ausnahmen von der Schonfrist und der Mahnung](#20)  
[§ 21 Vermögensermittlung](#21)  
[§ 22 Verfahren der Beitreibung](#22)  
[§ 23 Angabe des Schuldgrundes](#23)  
[§ 24 Ermächtigung zur verbandsübergreifenden Forderungspfändung](#24)

### Abschnitt 3  
Vollstreckung von Geldforderungen bürgerlichen Rechts

[§ 25 Beitreibung von Geldforderungen bürgerlichen Rechts](#25)

### Abschnitt 4  
Vollstreckung von sonstigen Handlungen, Duldungen oder Unterlassungen

[§ 26 Vollstreckungsbehörden](#26)  
[§ 27 Zwangsmittel](#27)  
[§ 28 Androhung des Zwangsmittels](#28)  
[§ 29 Anwendung der Zwangsmittel](#29)  
[§ 30 Zwangsgeld](#30)  
[§ 31 Ersatzzwangshaft](#31)  
[§ 32 Ersatzvornahme](#32)  
[§ 33 Fiktion der Abgabe einer Erklärung](#33)  
[§ 34 Unmittelbarer Zwang](#34)  
[§ 35 Zwangsräumung](#35)  
[§ 36 Wegnahme](#36)

### Abschnitt 5  
Kosten

[§ 37 Gebühren und Auslagen](#37)  
[§ 38 Kosten der Vollstreckung fremder Forderungen](#38)  
[§ 39 Ermächtigung zur Kostenordnung](#39)

### Abschnitt 6  
Schlussvorschriften

[§ 40 Einschränkung von Grundrechten](#40)  
[§ 41 Übergangsvorschrift](#41)

### Abschnitt 1  
Allgemeine Vorschriften

#### § 1  
Geltungsbereich des Gesetzes

(1) Dieses Gesetz gilt für die Vollstreckung von Verwaltungsakten

1.  der Behörden des Landes Brandenburg, der Gemeinden, der Gemeindeverbände sowie der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts,
2.  sonstiger Behörden, die die in Nummer 1 genannten Behörden um Vollstreckungshilfe ersuchen,

mit denen eine Geldleistung, eine sonstige Handlung, Duldung oder Unterlassung gefordert wird.
Natürliche Personen und juristische Personen des Privatrechts, denen durch ein Gesetz oder aufgrund eines Gesetzes hoheitliche Aufgaben übertragen sind (Beliehene), sind Behörden im Sinne des Satzes 1.

(2) Das Gesetz gilt für die Vollstreckung aus solchen öffentlich-rechtlichen Verträgen und gesetzlich zugelassenen Erklärungen, in denen sich die Schuldnerin oder der Schuldner der sofortigen Vollstreckung unterworfen hat, und für die Vollstreckung von Geldforderungen bürgerlichen Rechts der in § 25 Absatz 2 bestimmten Art.

(3) Dieses Gesetz gilt nicht, soweit auf die Verwaltungsvollstreckung Bundesrecht anzuwenden ist.
Es gilt jedoch, soweit Bundesrecht die Länder ermächtigt zu bestimmen, dass die landesrechtlichen Vorschriften über die Verwaltungsvollstreckung anzuwenden sind.

#### § 2  
Vollstreckung durch Behörden der Finanz- und Justizverwaltung

(1) Wird die Vollstreckung von den Finanzämtern vorgenommen, so ist sie nach den für die Finanzämter geltenden Bestimmungen durchzuführen.

(2) Wird die Vollstreckung von Gerichtsvollzieherinnen oder Gerichtsvollziehern im Wege der Amts- oder Vollstreckungshilfe vorgenommen, so ist sie nach den Vorschriften über die Zwangsvollstreckung in bürgerlichen Rechtsstreitigkeiten und den hierfür geltenden Kostenvorschriften durchzuführen; an die Stelle der vollstreckbaren Ausfertigung des Schuldtitels tritt der schriftliche Antrag der Vollstreckungsbehörde.

#### § 3  
Allgemeine Voraussetzungen der Vollstreckung

Ein Verwaltungsakt, der zu einer Geldleistung, einer sonstigen Handlung, Duldung oder Unterlassung verpflichtet, kann vollstreckt werden, wenn

1.  er unanfechtbar geworden ist,
2.  ein gegen ihn gerichteter Rechtsbehelf keine aufschiebende Wirkung hat und
3.  die sonstigen Vollstreckungsvoraussetzungen erfüllt sind.

#### § 4  
Vollstreckungshilfe

(1) Inländischen Behörden ist auf Ersuchen Vollstreckungshilfe zu leisten, wenn die Voraussetzungen für die Gewährung von Amtshilfe erfüllt sind.
Ausländischen Behörden darf Vollstreckungshilfe nur geleistet werden, wenn dies in einer völkerrechtlichen Vereinbarung oder in einem Rechtsakt der Europäischen Union vorgesehen ist.

(2) Einem Vollstreckungsersuchen darf, soweit nichts anderes bestimmt ist, nur entsprochen werden, wenn es folgende Angaben enthält:

1.  die Bezeichnung der ersuchenden Behörde sowie die Unterschrift oder die Namenswiedergabe der Behördenleiterin, des Behördenleiters oder deren Beauftragten; bei einem Vollstreckungsersuchen, das mit Hilfe automatischer Einrichtungen erstellt ist, kann die Unterschrift fehlen,
2.  die Bezeichnung des zu vollstreckenden Verwaltungsaktes unter Angabe der erlassenden Behörde,
3.  die Angabe der Verpflichtung des Vollstreckungsschuldners, im Falle der Beitreibung die Angabe des Grundes und der Höhe der Geldforderung,
4.  die Angabe, dass der Verwaltungsakt unanfechtbar geworden ist, ein gegen ihn gerichteter Rechtsbehelf kraft Gesetzes keine aufschiebende Wirkung hat oder seine sofortige Vollziehung angeordnet worden ist; im Falle der Vollstreckung aus einem öffentlich-rechtlichen Vertrag die Angabe, dass sich die Schuldnerin oder der Schuldner in dem Vertrag wirksam der sofortigen Vollstreckung unterworfen hat und die sonstigen Voraussetzungen der Vollstreckung aus dem Vertrag vorliegen,
5.  die Bezeichnung des Vollstreckungsschuldners,
6.  im Falle der Beitreibung die Angabe, dass der Vollstreckungsschuldner gemahnt worden ist oder aus welchem Grund die Mahnung unterblieben ist.

(3) Treten Umstände ein, welche die Einstellung, Beschränkung oder Aufhebung der Vollstreckung notwendig machen, ist die ersuchte Behörde unverzüglich zu unterrichten.

(4) Die Zulässigkeit der Vollstreckung richtet sich nach dem für die ersuchende Behörde, die Durchführung der Vollstreckungshilfe nach dem für die ersuchte Behörde geltenden Recht.
Die ersuchende Behörde trägt gegenüber der ersuchten Behörde die Verantwortung für die Rechtmäßigkeit der Vollstreckung.
Die ersuchte Behörde ist für die Durchführung der Vollstreckungshilfe verantwortlich.

#### § 5  
Gemeinsame Aufgabenwahrnehmung

(1) Für die Zusammenarbeit der Gemeinden und Gemeindeverbände im Bereich der Aufgaben nach diesem Gesetz gelten die Vorschriften des Gesetzes über kommunale Gemeinschaftsarbeit im Land Brandenburg entsprechend.
Abweichend von § 41 Absatz 3 des Gesetzes über kommunale Gemeinschaftsarbeit im Land Brandenburg bedürfen die Vereinbarungen keiner Genehmigung der Aufsichtsbehörde, wenn nur Ämter und amtsfreie Gemeinden die Aufgabe dem Landkreis übertragen, dem sie angehören.

(2) Im Rahmen der beamten- und tarifrechtlichen Vorschriften kann vorgesehen werden, dass Vollstreckungsdienstkräfte einer Körperschaft als Vollstreckungsdienstkräfte der anderen Körperschaft tätig werden (gemeinsame Vollstreckungsdienstkräfte).

#### § 6  
Vollstreckungsschuldner

(1) Vollstreckungsschuldner ist die Person, gegen die sich ein Vollstreckungsverfahren richtet.

(2) Als Vollstreckungsschuldner kann in Anspruch genommen werden,

1.  wer eine Verpflichtung im Sinne des § 1 Absatz 1 selbst zu erfüllen hat oder
2.  wer für eine Verpflichtung, die ein anderer aufgrund des § 1 Absatz 1 schuldet, persönlich haftet.

(3) Wer zur Duldung der Vollstreckung verpflichtet ist, steht dem Vollstreckungsschuldner gleich, soweit die Duldungspflicht reicht.

(4) Wird der Vollstreckungsschuldner als Rechtsnachfolger eines anderen in Anspruch genommen, so kann die Vollstreckung erst eingeleitet oder fortgesetzt werden, wenn die Vollstreckungsvoraussetzungen nach den Bestimmungen dieses Gesetzes auch für seine Person vorliegen.
Die Vollstreckung, die zurzeit des Todes des Vollstreckungsschuldners gegen diesen bereits begonnen hatte, kann auch ohne Vollstreckungsvoraussetzungen nach Satz 1 in den Nachlass fortgesetzt werden.
§ 779 Absatz 2 der Zivilprozessordnung ist entsprechend anzuwenden.

#### § 7  
Vollstreckung gegen juristische Personen des öffentlichen Rechts

(1) Die Einleitung eines Vollstreckungsverfahrens wegen einer Geldforderung gegen Gemeinden, Gemeindeverbände und kommunale Anstalten im Sinne der Kommunalverfassung des Landes Brandenburg richtet sich nach § 118 der Kommunalverfassung des Landes Brandenburg.

(2) Die Einleitung eines Vollstreckungsverfahrens wegen einer Geldforderung gegen andere als in Absatz 1 genannte, der Aufsicht des Landes unterstehende Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts mit Ausnahme der Kreditanstalten und Versicherungsanstalten des öffentlichen Rechts bedarf der Zustimmung der Aufsichtsbehörde, es sei denn, dass es sich um die Verfolgung dinglicher Rechte handelt.
Die Zustimmung ist zu versagen, wenn die Zwangsvollstreckung in die Vermögensgegenstände des Vollstreckungsschuldners die Erfüllung seiner öffentlich-rechtlichen Aufgaben gefährden würde.
Ein Insolvenzverfahren findet nicht statt.

(3) Auf Antrag der Vollstreckungsbehörde hat die Aufsichtsbehörde die Vermögensgegenstände zu bestimmen, in die eine Vollstreckung zugelassen wird, und über den Zeitpunkt zu befinden, zu dem sie stattfinden soll.

(4) Soweit gesetzlich nichts anderes bestimmt ist, ist die Erzwingung von Handlungen, Duldungen oder Unterlassungen gegen juristische Personen des öffentlichen Rechts und ihre Behörden unzulässig.

#### § 8  
Vollstreckungsdienstkraft

Soweit das Zwangsverfahren nicht der Vollstreckungsbehörde selbst zugewiesen ist, sind die Beitreibung von Geldforderungen, die Ausübung des unmittelbaren Zwangs, der Zwangsräumung und der Wegnahme durch ausdrücklich dazu bestimmte Dienstkräfte, die einen Diensteid oder ein Gelöbnis abgelegt haben (Vollstreckungsdienstkraft), durchzuführen.
Die Vollstreckungsbehörden bestimmen die für ihre Aufgaben notwendigen Vollstreckungsdienstkräfte.

#### § 9  
Vollstreckungsausweis

Die Vollstreckungsdienstkraft hat bei der Ausübung ihrer Tätigkeit einen behördlichen Ausweis bei sich zu führen.
Die elektronische Form des behördlichen Ausweises ist ausgeschlossen.
Der Ausweis ist auf Verlangen vorzulegen.
Dies gilt nicht, wenn bei der Anwendung des unmittelbaren Zwangs die Umstände es nicht zulassen oder der unmittelbare Zwang in Diensträumen der Anstellungskörperschaft ausgeübt wird.

#### § 10  
Befugnisse der Vollstreckungsdienstkraft

(1) Die Vollstreckungsdienstkraft ist befugt, die Wohn- und Geschäftsräume sowie die Behältnisse des Vollstreckungsschuldners zu durchsuchen, soweit dies der Zweck der Vollstreckung erfordert.

(2) Sie ist befugt, verschlossene Türen und Behältnisse öffnen zu lassen.

(3) Die Vollstreckungsdienstkraft ist bei Widerstand gegen eine Vollstreckungshandlung befugt, Gewalt anzuwenden.
Der Gebrauch von Schlagstock, Pistole oder Revolver (Waffen) durch die Vollsteckungsdienstkraft ist nur aufgrund besonderer gesetzlicher Ermächtigung zulässig.
Die Polizei leistet auf Verlangen nach den §§ 60 bis 68 des Brandenburgischen Polizeigesetzes vom 19. März 1996 (GVBl.
I S. 74), das zuletzt durch das Gesetz vom 21.
Juni 2012 (GVBl.
I Nr. 25) geändert worden ist, Vollzugshilfe.

(4) Die Wohn- und Geschäftsräume und sonstiges befriedetes Besitztum des Vollstreckungsschuldners dürfen ohne dessen Einwilligung nur aufgrund einer richterlichen Anordnung durchsucht werden.
Dies gilt nicht, wenn die Einholung der Anordnung den Erfolg der Durchsuchung gefährden würde.
Für die richterliche Anordnung einer Durchsuchung ist das Amtsgericht zuständig, in dessen Bezirk die Durchsuchung vorgenommen werden soll.

(5) Willigt der Vollstreckungsschuldner in die Durchsuchung ein oder ist eine Anordnung gegen ihn nach Absatz 4 Satz 1 ergangen oder nach Absatz 4 Satz 2 entbehrlich, so haben Personen, die Mitgewahrsam an den Wohn- oder Geschäftsräumen oder dem sonstigen befriedeten Besitztum des Vollstreckungsschuldners haben, die Durchsuchung zu dulden.
Unbillige Härten gegenüber Mitgewahrsamsinhaberinnen und Mitgewahrsamsinhabern sind zu vermeiden.

(6) Die richterliche Anordnung nach Absatz 4 ist bei der Vollstreckung vorzuzeigen.

#### § 11  
Zuziehung von Zeuginnen oder Zeugen

Wird bei einer Vollstreckungshandlung Widerstand geleistet oder ist bei einer Vollstreckungshandlung in den Wohn- oder Geschäftsräumen oder dem sonstigen befriedeten Besitztum des Vollstreckungsschuldners weder der Vollstreckungsschuldner noch eine Person, die zu seiner Familie gehört oder bei ihm beschäftigt ist, gegenwärtig, so hat die Vollstreckungsdienstkraft zwei Erwachsene oder eine Dienstkraft der Gemeinde, des Amtes oder des Polizeivollzugs als Zeuginnen oder Zeugen zuzuziehen.

#### § 12  
Zeit der Vollstreckung

In der Zeit zwischen 21 und 6 Uhr sowie an Sonntagen und staatlich anerkannten Feiertagen darf eine Vollstreckungshandlung nur mit schriftlicher Erlaubnis der Vollstreckungsbehörde vorgenommen werden.
Die Erlaubnis ist auf Verlangen bei der Vollstreckungshandlung vorzuzeigen.

#### § 13  
Einstellung oder Beschränkung der Vollstreckung

(1) Die Vollstreckung ist einzustellen oder zu beschränken, wenn

1.  der Zweck erreicht wurde,
2.  der zu vollstreckende Verwaltungsakt aufgehoben wurde,
3.  die Vollziehbarkeit des Verwaltungsaktes nachträglich entfallen ist,
4.  der mit dem Verwaltungsakt geltend gemachte Anspruch erloschen ist,
5.  die mit dem Verwaltungsakt geforderte Leistung gestundet wurde.

(2) In den Fällen des Absatzes 1 Nummer 2 und 4 sind bereits getroffene Vollstreckungsmaßnahmen aufzuheben.
Wurde der Verwaltungsakt durch eine gerichtliche Entscheidung aufgehoben, sind bereits getroffene Verwaltungsentscheidungen nur insoweit aufzuheben, als die Entscheidung unanfechtbar geworden und nicht aufgrund der Entscheidung ein neuer Verwaltungsakt zu erlassen ist.
Im Übrigen bleiben die Vollstreckungsmaßnahmen bestehen, soweit nicht ihre Aufhebung ausdrücklich angeordnet ist.

#### § 14  
Einstweilige Einstellung oder Beschränkung der Vollstreckung

Die Vollstreckungsbehörde kann die Vollstreckung einstweilig einstellen, beschränken oder die Vollstreckungsmaßnahmen aufheben, wenn die Vollstreckung unbillig ist oder wenn die Kosten der Beitreibung außer Verhältnis zu der beizutreibenden Geldforderung stehen.

#### § 15  
Einwendungen gegen die Vollstreckung

Einwendungen gegen Entstehung oder Höhe der Verpflichtung, deren Erfüllung erzwungen werden soll, sind außerhalb des Vollstreckungsverfahrens mit den hierfür zugelassenen Rechtsmitteln zu verfolgen.
§ 25 Absatz 3 bleibt unberührt.

#### § 16  
Aufhebung der aufschiebenden Wirkung

Rechtsbehelfe, die sich gegen Maßnahmen der Vollstreckungsbehörden richten, haben keine aufschiebende Wirkung.

### Abschnitt 2  
Vollstreckung von öffentlich-rechtlichen Geldforderungen

#### § 17  
Vollstreckungsbehörden

(1) Verwaltungsakte, mit denen eine öffentlich-rechtliche Geldleistung gefordert wird (Leistungsbescheid), werden durch Beitreibung vollstreckt.

(2) Die Beitreibung ist eine Aufgabe der Vollstreckungsbehörden.
Soweit nichts anderes bestimmt ist, erfolgt die Beitreibung der öffentlich-rechtlichen Geldforderungen

1.  des Landes durch
    1.  die Behörden der Finanzverwaltung, wenn es sich um Steuern und steuerliche Nebenleistungen handelt,
    2.  die Behörden der Justizverwaltung, wenn es sich um die Beitreibung von Forderungen handelt, die von ihnen einzuziehen sind,
    3.  die Landkreise und kreisfreien Städte, soweit nicht die Beitreibung nach Buchstabe a oder Buchstabe b erfolgt,
    4.  die Landeshauptkasse anstelle der Landkreise und kreisfreien Städte, wenn diese Vollstreckungsschuldner sind,
2.  der Stiftungen, Anstalten des öffentlichen Rechts mit Ausnahme der kommunalen Anstalten im Sinne der Kommunalverfassung des Landes Brandenburg sowie der sonstigen Körperschaften des öffentlichen Rechts mit Sitz im Land Brandenburg durch die kreisfreien Städte, amtsfreien Gemeinden und Ämter,
3.  der landesunmittelbaren Sozialversicherungsträger durch diese selbst, sofern hierfür auf der Grundlage des § 66 Absatz 3 Satz 2, Absatz 1 Satz 2 bis 5 des Zehnten Buches Sozialgesetzbuch Vollstreckungsdienstkräfte durch die jeweils zuständige Aufsichtsbehörde bestellt worden sind,
4.  der Beliehenen, die vom Land übertragene Aufgaben wahrnehmen, durch die kreisfreien Städte, amtsfreien Gemeinden und Ämter,
5.  der Zweckverbände nach dem Gesetz über kommunale Gemeinschaftsarbeit im Land Brandenburg, durch diese selbst, sofern die Verbandssatzung dies bestimmt, andernfalls durch
    1.  die kreisfreien Städte, amtsfreien Gemeinden und Ämter, die Mitglieder des Zweckverbandes sind,
    2.  den Landkreis, in dem der Zweckverband seinen Sitz hat, anstelle der kreisfreien Städte, amtsfreien Gemeinden und Ämter, wenn diese Vollstreckungsschuldner sind,
    3.  die amtsfreie Gemeinde oder das Amt, in der oder in dem der Landkreis seinen Sitz hat, wenn der dem Zweckverband angehörende Landkreis Vollstreckungsschuldner ist,
6.  der Gewässerunterhaltungsverbände durch
    1.  den Landkreis oder die kreisfreie Stadt, in dem der Gewässerunterhaltungsverband seinen Sitz hat,
    2.  die amtsfreie Gemeinde oder das Amt, in der oder in dem der Landkreis seinen Sitz hat, wenn der Landkreis, in dem der Gewässerunterhaltungsverband seinen Sitz hat, Vollstreckungsschuldner ist,
    3.  die dem Gewässerunterhaltungsverband angehörige amtsfreie Gemeinde oder das Amt mit der höchsten Einwohnerzahl nach der kreisfreien Stadt, wenn der Gewässerunterhaltungsverband seinen Sitz in einer kreisfreien Stadt hat und diese Vollstreckungsschuldner ist,
7.  der kommunalen Anstalten im Sinne der Kommunalverfassung des Landes Brandenburg durch diese selbst, sofern die Anstaltssatzung nichts anderes bestimmt,
8.  der kreisfreien Städte, amtsfreien Gemeinden, Ämter und Landkreise durch diese selbst,
9.  der amtsangehörigen Gemeinden durch das Amt, dem sie angehören.

Örtlich zuständig ist in den Fällen des Satzes 2 Nummer 1 Buchstabe c, Nummer 2, 4 und 5 Buchstabe a die Vollstreckungsbehörde, in deren Zuständigkeitsbereich der Vollstreckungsschuldner seinen Wohnsitz, Sitz oder gewöhnlichen Aufenthalt hat.
Hat der Vollstreckungsschuldner im Falle des Satzes 2 Nummer 5 Buchstabe a seinen Wohnsitz, Sitz oder gewöhnlichen Aufenthalt nicht im Gebiet des Zweckverbandes, ist das Mitglied des Zweckverbandes örtlich zuständig, in dem der Rechtsgrund für die Geldforderung entstanden ist.
Ist im Falle des Satzes 2 Nummer 5 Buchstabe a oder im Falle des Satzes 4 das Mitglied des Zweckverbandes eine amtsangehörige Gemeinde oder Ortsgemeinde und ist das Amt, dem die Gemeinde angehört oder die Verbandsgemeinde, der die Ortsgemeinde angehört, nicht Mitglied des Zweckverbandes, ist das Mitglied mit der höchsten Einwohnerzahl örtlich zuständig.

(3) Hat der Vollstreckungsschuldner seinen Wohnsitz, Sitz oder gewöhnlichen Aufenthalt nicht im Geltungsbereich dieses Gesetzes, ist die Vollstreckungsbehörde am Sitz des Vollstreckungsgläubigers der Geldforderung zuständig.

(4) Die Beitreibung nach Absatz 2 Satz 2 Nummer 1 Buchstabe c nehmen die Landkreise und kreisfreien Städte als Auftragsangelegenheit wahr.
Im Übrigen wird die Beitreibung als Selbstverwaltungsangelegenheit behandelt.

(5) Das für die Verwaltungsvollstreckung zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung im Einvernehmen mit dem für Finanzen und dem fachlich zuständigen Mitglied der Landesregierung weitere Vollstreckungsbehörden und abweichend von Absatz 2 Satz 2 Nummer 1 Buchstabe c Landesbehörden für bestimmte öffentlich-rechtliche Geldforderungen zu Vollstreckungsbehörden zu bestimmen.

#### § 18  
Aufsicht bei der Vollstreckung von Geldforderungen des Landes

Die Landkreise und kreisfreien Städte unterstehen bei der Beitreibung der Geldforderungen des Landes nach § 17 Absatz 2 Satz 2 Nummer 1 Buchstabe c der Aufsicht des für Finanzen zuständigen Mitglieds der Landesregierung.
Die Aufsicht erstreckt sich auf die rechtmäßige und zweckmäßige Wahrnehmung der Aufgaben.
§ 4 Absatz 3 gilt entsprechend.

#### § 19  
Besondere Voraussetzungen der Beitreibung

(1) Dem Vollstreckungsschuldner und Dritten gegenüber wird die Vollstreckungsdienstkraft zur Vollstreckung durch schriftlichen Auftrag der Vollstreckungsbehörde ermächtigt, der vorzulegen ist.

(2) Ein Leistungsbescheid kann vollstreckt werden, wenn

1.  er dem Vollstreckungsschuldner bekannt gegeben ist,
2.  die beizutreibende Forderung fällig ist,
3.  eine Frist von einer Woche seit Bekanntgabe des Leistungsbescheides oder, wenn die Leistung erst später fällig wird, eine Frist von einer Woche nach Eintritt der Fälligkeit (Schonfrist) abgelaufen ist, soweit gesetzlich nichts anderes bestimmt ist,
4.  der Vollstreckungsschuldner vor der Beitreibung schriftlich oder durch Postnachnahmeauftrag ergebnislos aufgefordert worden ist, innerhalb einer bestimmten Frist von mindestens einer Woche seit Bekanntgabe zu leisten (Mahnung).

(3) Dem Leistungsbescheid stehen gleich

1.  die vom Vollstreckungsschuldner abgegebene Selbstberechnungserklärung, wenn dieser die Höhe einer Abgabe aufgrund einer Rechtsvorschrift einzuschätzen hat,
2.  die Beitragsnachweisung, wenn die vom Träger einer gesetzlichen Krankenversicherung einzuziehenden Beiträge nach dem wirklichen Arbeitsverdienst errechnet werden und die Satzung des Krankenversicherungsträgers die Abgabe einer Beitragsnachweisung durch den Arbeitgeber vorsieht,
3.  im Falle des § 1 Absatz 2 die Zahlungsaufforderung.

(4) Vollstreckungskosten, Säumniszuschläge und Zinsen können ohne Leistungsbescheid zusammen mit der Hauptforderung vollstreckt werden, wenn in dem Leistungsbescheid über die Hauptforderung oder in der Mahnung auf diese Nebenforderungen dem Grunde nach hingewiesen wurde.
Erfolgt die Zahlung der Hauptforderung nach der Mahnung und vor der Einleitung der Vollstreckung, gilt der Leistungsbescheid über die Hauptforderung als Leistungsbescheid über diese Nebenforderungen, wenn der Leistungsbescheid oder die Mahnung den Anforderungen des Satzes 1 genügt.

#### § 20  
Ausnahmen von der Schonfrist und der Mahnung

(1) Der Einhaltung einer Schonfrist nach § 19 Absatz 2 Nummer 3 oder einer Mahnung nach § 19 Absatz 2 Nummer 4 bedarf es nicht, wenn

1.  Zwangsgeld oder Kosten der Ersatzvornahme beigetrieben werden sollen,
2.  Vollstreckungskosten, Säumniszuschläge und Zinsen beigetrieben werden sollen, sofern auf sie im Leistungsbescheid über die Hauptforderung oder in der Mahnung zur Hauptforderung dem Grunde nach hingewiesen worden ist.

(2) Einer Mahnung bedarf es auch nicht, wenn der Vollstreckungsschuldner vor Eintritt der Fälligkeit an die Zahlung erinnert wurde.
Bei regelmäßig wiederkehrender öffentlich-rechtlicher Geldleistung kann die Erinnerung durch ortsübliche öffentliche Bekanntmachung erfolgen.
Auf eine Mahnung kann verzichtet werden, wenn besondere Umstände die Annahme rechtfertigen, dass sie den Vollstreckungserfolg gefährden würde.

#### § 21  
Vermögensermittlung

(1) Zur Vorbereitung der Vollstreckung kann die Vollstreckungsbehörde die Vermögens- und Einkommensverhältnisse des Vollstreckungsschuldners ermitteln.
Die Vollstreckungsbehörde darf ihr bekannte Daten, soweit sie

1.  keinem besonderen gesetzlichen Berufs- oder Amtsgeheimnis unterliegen oder
2.  nach § 30 der Abgabenordnung geschützt sind und bei der Vollstreckung wegen Steuern und steuerlicher Nebenleistungen verwendet werden dürften,

auch bei der Vollstreckung anderer öffentlich-rechtlicher Geldleistungen verwenden.
Eine Weiterverarbeitung dieser Daten ist nur zu Vollstreckungszwecken zulässig.

(2) Der Vollstreckungsschuldner und andere Personen haben der Vollstreckungsbehörde die zur Feststellung eines für die Vollstreckung erheblichen Sachverhaltes erforderlichen Auskünfte zu erteilen.
Dies gilt auch für nicht rechtsfähige Vereinigungen, Vermögensmassen, Behörden und Betriebe gewerblicher Art der Körperschaften öffentlichen Rechts.
Andere Personen als der Vollstreckungsschuldner sollen erst dann zur Auskunft aufgefordert werden, wenn die Sachverhaltsaufklärung durch den Vollstreckungsschuldner nicht zum Ziel führt oder keinen Erfolg verspricht.
In dem Auskunftsersuchen ist anzugeben, worüber die Auskünfte erteilt werden sollen.
Auskunftsersuchen haben auf Verlangen der Auskunftspflichtigen schriftlich zu ergehen.

#### § 22  
Verfahren der Beitreibung

(1) Für das Beitreibungsverfahren gelten die nachfolgenden Vorschriften der Abgabenordnung entsprechend, soweit nicht dieses Gesetz etwas anderes bestimmt:

1.  die §§ 252, 262 bis 267, 290, 291 und 324 bis 327 als allgemeine Bestimmungen für die Beitreibung; § 324 gilt mit der Maßgabe, dass die in der Vorschrift genannten Befugnisse der für die Steuerfestsetzung zuständigen Finanzbehörde von dem Amtsgericht, in dessen Bezirk sich der mit Arrest zu belegende Gegenstand befindet, wahrgenommen werden,
2.  die §§ 281 bis 284 und 286, 292 bis 308 für die Vollstreckung in das bewegliche Vermögen (Pfändung),
3.  die §§ 309 bis 321 für die Vollstreckung in Forderungen und andere Vermögensrechte,
4.  § 77 Absatz 2 sowie die §§ 322 und 323 für die Vollstreckung in das unbewegliche Vermögen.

(2) Treibt die Vollstreckungsbehörde eine Geldforderung bei, deren Gläubiger nicht die Körperschaft ist, der die Vollstreckungsbehörde angehört (Vollstreckung fremder Forderung), und sind ihr vor Beginn der Vollstreckung Umstände bekannt, welche die Vollstreckung als aussichtslos erscheinen lassen, soll sie den Gläubiger der Geldforderung von den Umständen unterrichten.

#### § 23  
Angabe des Schuldgrundes

Im Vollstreckungsauftrag oder in der Pfändungsverfügung an den Vollstreckungsschuldner ist für die beizutreibenden Geldbeträge der Schuldgrund anzugeben.
Hat die Vollstreckungsbehörde oder der Gläubiger der Geldforderung den Vollstreckungsschuldner durch Kontoauszüge über Entstehung, Fälligkeit und Tilgung seiner Schulden fortlaufend unterrichtet, so genügt es, wenn die Vollstreckungsbehörde die Art der Forderung und die Höhe des beizutreibenden Betrages angibt und auf den Kontoauszug Bezug nimmt, der den Rückstand ausweist.

#### § 24  
Ermächtigung zur verbandsübergreifenden Forderungspfändung

(1) Innerhalb des Geltungsbereichs dieses Gesetzes kann die Vollstreckungsbehörde eine Pfändungs- und Einziehungsverfügung ohne Rücksicht auf den Wohnsitz, Sitz oder gewöhnlichen Aufenthalt der Schuldnerin oder des Schuldners, der Drittschuldnerin oder des Drittschuldners selbst erlassen oder auch im Wege der Postzustellung selbst bewirken.
Die Vollstreckungsbehörde kann auch eine Vollstreckungsbehörde des Bezirks, in dem die Maßnahme durchgeführt werden soll, um die Zustellung der Pfändungs- und Einziehungsverfügung ersuchen.

(2) Hat der Vollstreckungsschuldner oder die Drittschuldnerin oder der Drittschuldner den Wohnsitz, Sitz oder gewöhnlichen Aufenthaltsort außerhalb des Geltungsbereichs dieses Gesetzes, jedoch im Inland, gilt Absatz1 entsprechend, sofern das Recht des Landes, in dem die Zustellung erfolgt, dies zulässt.

(3) Vollstreckungsbehörden im Inland, die diesem Gesetz nicht unterliegen, können gegen Vollstreckungsschuldner, Drittschuldnerinnen oder Drittschuldner, die ihren Wohnsitz, Sitz oder gewöhnlichen Aufenthalt im Geltungsbereich dieses Gesetzes haben, selbst Pfändungs- und Einziehungsverfügungen erlassen und zustellen lassen.

### Abschnitt 3  
Vollstreckung von Geldforderungen bürgerlichen Rechts

#### § 25  
Beitreibung von Geldforderungen bürgerlichen Rechts

(1) Geldforderungen bürgerlichen Rechts der in Absatz 2 bestimmten Art der Gemeinden, Gemeindeverbände und kommunalen Anstalten im Sinne der Kommunalverfassung des Landes Brandenburg können von den Vollstreckungsbehörden nach den Vorschriften dieses Gesetzes beigetrieben werden.

(2) Die Forderungen müssen entstanden sein aus

1.  der Herstellung und Unterhaltung der Versorgungsleitungen und der Hausanschlüsse, der Lieferung von Gas, Wasser, Wärme, elektrischer Energie sowie der Beseitigung von Abwasser,
2.  der Inanspruchnahme der Krankentransporte und Krankenanstalten,
3.  der Inanspruchnahme von Pflegeeinrichtungen und Erziehungsheimen,
4.  der Inanspruchnahme von Kindertagesstätten, Volkshochschulen, Musikschulen, Bibliotheken, Museen oder sonstigen Kultureinrichtungen,
5.  der Tätigkeit der Katasterbehörden,
6.  dem Forderungsübergang nach
    1.  § 95 des Achten Buches Sozialgesetzbuch,
    2.  den §§ 93 und 94 des Zwölften Buches Sozialgesetzbuch,
    3.  § 37 des Bundesausbildungsförderungsgesetzes in der Fassung der Bekanntmachung vom 7. Dezember 2010 (BGBl.
        I S.
        1952, 2012 I S.
        197), das zuletzt durch Artikel 31 des Gesetzes vom 20.
        Dezember 2011 (BGBl.
        I S.
        2854, 2923) geändert worden ist, in der jeweils geltenden Fassung.

Zu den Forderungen gehören auch die Zinsen, die Kosten der Zahlungsaufforderung und sonstige Nebenforderungen.

(3) Die Beitreibung ist einzustellen, sobald der Vollstreckungsschuldner bei der Vollstreckungsbehörde gegen die Forderung als solche schriftlich oder zur Niederschrift Einwendungen erhebt.
Der Vollstreckungsschuldner ist hierüber zu belehren.
Bereits getroffene Vollstreckungsmaßnahmen sind unverzüglich aufzuheben, wenn

1.  Gläubiger nicht binnen einem Monat nach Geltendmachung der Einwendungen wegen ihrer Ansprüche vor den ordentlichen Gerichten Klage erhoben oder den Erlass eines Mahnbescheides beantragt haben oder
2.  Gläubiger mit der Klage rechtskräftig abgewiesen worden sind.

Ist die Beitreibung eingestellt worden, so kann sie nur nach Maßgabe der Zivilprozessordnung fortgesetzt werden.

### Abschnitt 4  
Vollstreckung von sonstigen Handlungen, Duldungen oder Unterlassungen

#### § 26  
Vollstreckungsbehörden

(1) Soweit nichts anderes bestimmt ist, wird ein Verwaltungsakt, mit dem eine sonstige Handlung, Duldung oder Unterlassung gefordert wird, von der Behörde vollstreckt, die ihn erlassen hat; sie vollzieht auch die Widerspruchsentscheidungen.

(2) Das jeweils zuständige Mitglied der Landesregierung kann im Einvernehmen mit dem für die Verwaltungsvollstreckung zuständigen Mitglied der Landesregierung durch Rechtsverordnung allgemein oder für bestimmte Fälle bestimmen, dass die Verwaltungsakte der obersten Landesbehörde, einer Landesoberbehörde oder einer unteren Landesbehörde durch eine andere Behörde zu vollstrecken sind.

#### § 27  
Zwangsmittel

(1) Verwaltungsakte, die zu einer sonstigen Handlung, Duldung oder Unterlassung verpflichten, werden mit Zwangsmitteln vollstreckt.
Zwangsmittel können ohne vorausgehenden Verwaltungsakt eingesetzt werden, soweit dies zur Abwehr einer gegenwärtigen Gefahr notwendig ist und die Vollstreckungsbehörde hierbei innerhalb ihrer Befugnisse handelt.

(2) Zwangsmittel sind

1.  Zwangsgeld (§ 30),
2.  Ersatzvornahme (§ 32),
3.  Fiktion der Abgabe einer Erklärung (§ 33),
4.  unmittelbarer Zwang (§ 34),
5.  Zwangsräumung (§ 35),
6.  Wegnahme (§ 36).

#### § 28  
Androhung des Zwangsmittels

(1) Zwangsmittel sind vor ihrer Anwendung schriftlich anzudrohen.
Dem Vollstreckungsschuldner ist in der Androhung zur Erfüllung der Verpflichtung eine angemessene Frist zu bestimmen.
Eine Frist braucht nicht bestimmt zu werden, wenn eine Duldung oder Unterlassung erzwungen werden soll.
Einer Androhung bedarf es im Falle des sofortigen Vollzuges (§ 27 Absatz 1 Satz 2) und im Falle der Fiktion der Abgabe einer Erklärung (§ 33) nicht.

(2) Die Androhung kann mit dem Verwaltungsakt verbunden werden, durch den die Handlung, Duldung oder Unterlassung aufgegeben wird.
Sie soll mit ihm verbunden werden, wenn ein Rechtsmittel keine aufschiebende Wirkung hat.

(3) Die Androhung muss sich auf bestimmte Zwangsmittel beziehen.
Werden mehrere Zwangsmittel angedroht, ist anzugeben, in welcher Reihenfolge sie angewandt werden sollen.
Auch die Wiederholung eines Zwangsmittels ist anzudrohen.

(4) Das Zwangsgeld ist in bestimmter Höhe anzudrohen.

(5) Wird Ersatzvornahme angedroht, sollen in der Androhung die voraussichtlichen Kosten angegeben werden.

(6) Die Androhung ist zuzustellen.
Das gilt auch dann, wenn sie mit dem zugrunde liegenden Verwaltungsakt verbunden und für diesen keine Zustellung vorgeschrieben ist.

#### § 29  
Anwendung der Zwangsmittel

(1) Zwangsmittel dürfen wiederholt und so lange angewandt werden, bis der Verwaltungsakt vollstreckt oder in anderer Weise erledigt ist.
Sie können auch neben der Verhängung einer Strafe oder Geldbuße angewandt werden.
Zur Erzwingung einer Duldung oder Unterlassung dürfen Zwangsmittel nicht mehr angewandt werden, wenn eine weitere Zuwiderhandlung nicht mehr zu befürchten ist.

(2) Kommen mehrere Zwangsmittel in Betracht, so hat die Vollstreckungsbehörde dasjenige Zwangsmittel anzuwenden, das den Vollstreckungsschuldner und die Allgemeinheit voraussichtlich am wenigsten beeinträchtigt.

(3) Durch die Anwendung eines Zwangsmittels darf kein Nachteil herbeigeführt werden, der erkennbar außer Verhältnis zum Zweck der Vollstreckung steht.

#### § 30  
Zwangsgeld

(1) Wird die Verpflichtung zu einer sonstigen Handlung, Duldung oder Unterlassung nicht oder nicht vollständig erfüllt, kann der Vollstreckungsschuldner zu der geforderten Handlung, Duldung oder Unterlassung durch Festsetzung eines Zwangsgeldes angehalten werden.

(2) Das Zwangsgeld beträgt mindestens 10 und höchstens 50 000 Euro.
Bei der Bemessung des Zwangsgeldes soll das wirtschaftliche Interesse der oder des Betroffenen an der Nichtbefolgung des Verwaltungsaktes berücksichtigt werden.

(3) Mit der Festsetzung des Zwangsgeldes ist dem Vollstreckungsschuldner eine angemessene Frist zur Zahlung einzuräumen.

(4) Das Zwangsgeld wird nach den Bestimmungen des Abschnittes 2 beigetrieben.

#### § 31  
Ersatzzwangshaft

(1) Ist das Zwangsgeld uneinbringlich, so kann das Verwaltungsgericht auf Antrag der Vollstreckungsbehörde die Ersatzzwangshaft nach Anhörung des Vollstreckungsschuldners anordnen, wenn bei Androhung des Zwangsgeldes oder nachträglich spätestens einen Monat vor Antragstellung auf die Zulässigkeit der Ersatzzwangshaft hingewiesen worden ist.
Der nachträgliche Hinweis ist zuzustellen.
Ordnet das Gericht Ersatzzwangshaft an, so hat es einen Haftbefehl auszufertigen, in dem die antragstellende Behörde, die oder der Pflichtige und der Grund der Verhaftung zu bezeichnen sind.

(2) Die Ersatzzwangshaft beträgt mindestens einen Tag und höchstens zwei Wochen.

(3) Die Ersatzzwangshaft ist auf Antrag der Vollstreckungsbehörde von der Justizverwaltung zu vollstrecken.
Die Vollziehung der Ersatzzwangshaft richtet sich nach § 802g Absatz 2 und § 802h der Zivilprozessordnung und nach den §§ 171 bis 175 des Strafvollzugsgesetzes.

(4) Ist der Anspruch auf das Zwangsgeld verjährt, so darf die Haft nicht mehr vollstreckt werden.

#### § 32  
Ersatzvornahme

(1) Wird die Verpflichtung eine Handlung vorzunehmen, deren Vornahme durch einen anderen möglich ist (vertretbare Handlung), nicht erfüllt, so kann die Vollstreckungsbehörde auf Kosten des Vollstreckungsschuldners eine andere Person mit der Vornahme der Handlung beauftragen oder die Handlung selbst ausführen.
Der Vollstreckungsschuldner sowie Personen, die Mitgewahrsam an den Räumen und beweglichen Sachen des Vollstreckungsschuldners haben, sind zur Duldung der Ersatzvornahme verpflichtet.

(2) Die Vollstreckungsbehörde kann vom Vollstreckungsschuldner die Vorauszahlung der voraussichtlichen Kosten der Ersatzvornahme verlangen.

(3) Die Kosten der Ersatzvornahme und die Vorauszahlung werden von der Vollstreckungsbehörde durch Leistungsbescheid erhoben.
Der Bescheid ist sofort vollziehbar.

(4) Die Kosten sind innerhalb von zwei Wochen nach Zustellung des Leistungsbescheides zu zahlen.
Von diesem Zeitpunkt an sind die Kosten der Ersatzvornahme zu verzinsen.
Die Vorauszahlung ist zu verzinsen, soweit sie die tatsächlichen Kosten der Ersatzvornahme übersteigt.
Der Zinssatz beträgt 5 Prozent über dem jeweiligen Basiszinssatz nach § 247 des Bürgerlichen Ges Basiszinssatzes nach § 247 des Bürgerlichen Gesetzbuchs sind für die Verzinsung ab dem Tag wirksam, an dem die Deutsche Bundesbank die Änderung im Bundesanzeiger bekannt gemacht hat.
Neben den Zinsen werden keine Säumniszuschläge erhoben.

#### § 33  
Fiktion der Abgabe einer Erklärung

(1) Ist jemand durch einen Verwaltungsakt verpflichtet, eine bestimmte Erklärung abzugeben, so gilt die Erklärung als abgegeben, sobald der Verwaltungsakt unanfechtbar geworden ist.
Voraussetzung ist, dass

1.  der Inhalt der Erklärung in dem Verwaltungsakt festgelegt worden ist,
2.  der Vollstreckungsschuldner in dem Verwaltungsakt auf die Bestimmung des Satzes 1 hingewiesen worden ist,
3.  er im Zeitpunkt des Eintritts der Unanfechtbarkeit des Verwaltungsaktes die Erklärung rechtswirksam abgeben kann und
4.  der Verwaltungsakt zugestellt worden ist.

(2) Die Behörde, die den Verwaltungsakt erlassen hat, teilt den Beteiligten mit, in welchem Zeitpunkt der Verwaltungsakt unanfechtbar geworden ist.
Sie ist berechtigt, die zur Wirksamkeit der Erklärung erforderlichen Genehmigungen und Zustimmungen einzuholen und Anträge auf Eintragung in öffentliche Bücher und Register zu stellen.
Bedarf die Behörde dazu einer Urkunde, die dem Vollstreckungsschuldner auf Antrag von einer anderen Behörde oder einem Notar zu erteilen ist, so kann sie an Stelle des Vollstreckungsschuldners die Erteilung verlangen.

#### § 34  
Unmittelbarer Zwang

(1) Unmittelbarer Zwang ist jede Einwirkung auf Personen oder Sachen durch einfache körperliche Gewalt sowie durch Waffengebrauch oder andere Hilfsmittel der körperlichen Gewalt.
Waffengebrauch ist nur zulässig, soweit dies durch Gesetz ausdrücklich gestattet ist.

(2) Unmittelbarer Zwang darf nur angewandt werden, wenn Zwangsgeld und Ersatzvornahme nicht zum Erfolg geführt haben oder deren Anwendung untunlich ist.

(3) Gegenüber Personen darf unmittelbarer Zwang nur angewandt werden, wenn der Zweck der Vollstreckung durch unmittelbaren Zwang gegen Sachen nicht erreichbar erscheint.
Das angewandte Mittel muss nach Art und Maß dem Alter und dem Zustand der Personen angemessen sein.

#### § 35  
Zwangsräumung

(1) Hat der Vollstreckungsschuldner eine unbewegliche Sache, einen Raum oder ein eingetragenes Schiff zu räumen, zu überlassen oder herauszugeben, so können er und die Personen, die zu seinem Haushalt oder Geschäftsbetrieb gehören, aus dem Besitz gesetzt werden.
Der Zeitpunkt der Zwangsräumung soll dem Vollstreckungsschuldner angemessene Zeit vorher mitgeteilt werden.

(2) Bewegliche Sachen, die nicht Gegenstand der Vollstreckung sind, werden dem Vollstreckungsschuldner oder, wenn dieser nicht anwesend ist, seiner Vertreterin oder seinem Vertreter oder einer zu seinem Haushalt oder Geschäftsbetrieb gehörenden erwachsenen Person übergeben.

(3) Weigert sich die nach Absatz 2 zum Empfang berechtigte Person, die Sachen in Empfang zu nehmen, sind sie zu verwahren.
Der Vollstreckungsschuldner ist aufzufordern, die Sachen binnen einer bestimmten Frist abzuholen.
Kommt er der Aufforderung nicht nach, kann die Vollstreckungsbehörde die Sachen verwerten und den Erlös verwahren.
Die §§ 296 bis 300 der Abgabenordnung gelten entsprechend.
Unverwertbare Sachen kann die Vollstreckungsbehörde auf Kosten des Vollstreckungsschuldners vernichten, wenn sie ihn auf diese Möglichkeit hingewiesen hat.

#### § 36  
Wegnahme

(1) Hat der Vollstreckungsschuldner eine bewegliche Sache herauszugeben oder vorzulegen, kann die Vollstreckungsbehörde sie ihm wegnehmen.

(2) Wird die Sache beim Vollstreckungsschuldner nicht vorgefunden, so hat er über ihren Verbleib Auskunft zu geben oder gegenüber der Vollstreckungsbehörde zu Protokoll an Eides statt zu versichern, dass er die Sache nicht in seinem Besitz habe und er nicht wisse, wo die Sache sich befindet.

### Abschnitt 5  
Kosten

#### § 37  
Gebühren und Auslagen

(1) Für die Maßnahmen der Vollstreckung nach diesem Gesetz werden Gebühren und Auslagen vom Vollstreckungsschuldner erhoben.
Die Gebühren dienen dem Zweck, die Aufwendungen der Vollstreckungsbehörde zu decken und den Vollstreckungsschuldner zur rechtzeitigen Erfüllung der bestehenden Verpflichtung anzuhalten.
Gläubigerin der Gebühren und Auslagen ist die Körperschaft des öffentlichen Rechts, deren Organ die Vollstreckungsmaßnahme vornimmt oder bei der die Auslagen entstanden sind.

(2) Gebühren und Auslagen, die durch eine unrichtige Behandlung der Sache entstanden sind, werden nicht erhoben.

(3) Die Vollstreckungsbehörde kann von der Festsetzung oder Erhebung der Gebühren und Auslagen ganz oder teilweise absehen, wenn es sich um geringfügige Beträge handelt oder nach Begleichung der Hauptschuld die Beitreibung der Gebühren und Auslagen für den Vollstreckungsschuldner eine unbillige Härte bedeuten oder nur neue nicht vertretbare Vollstreckungsgebühren und Auslagen verursachen würde.

(4) Die Vollstreckungsbehörde entnimmt bei der Beitreibung die Gebühren und Auslagen der Vollstreckung aus den beigetriebenen und eingezahlten Geldern.
Reicht der Erlös einer Vollstreckung oder die Zahlung des Vollstreckungsschuldners zur Deckung der beizutreibenden Forderung und Kosten nicht aus, sind zunächst die in Ansatz gebrachten Gebühren, sodann die übrigen Kosten der Zwangsvollstreckung zu decken, soweit nicht für die Reihenfolge der Anrechnung anderweitige Bestimmungen maßgebend sind.

#### § 38  
Kosten der Vollstreckung fremder Forderungen

(1) Vollstreckt die Vollstreckungsbehörde eine fremde Forderung, haftet der Gläubiger der Geldforderung für die beim Vollstreckungsschuldner uneinbringlichen Vollstreckungsgebühren und Auslagen.
Im Falle der Vollstreckungshilfe haftet die ersuchende Behörde.

(2) Bei Behörden des Bundes oder bei Behörden aus anderen Ländern kann die Vollstreckungsbehörde auf die Erhebung der uneinbringlichen Vollstreckungsgebühren und Auslagen verzichten, wenn auch diese auf die Erhebung uneinbringlicher Vollstreckungsgebühren und Auslagen verzichten.
Die Gemeinden, Gemeindeverbände und kommunalen Anstalten im Sinne der Kommunalverfassung des Landes Brandenburg können eine von Absatz 1 abweichende Regelung vereinbaren.

#### § 39  
Ermächtigung zur Kostenordnung

Das für Inneres zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit dem für Finanzen zuständigen Mitglied der Landesregierung die gebührenpflichtigen Maßnahmen, die Gebührensätze, den Umfang der zu erstattenden Auslagen sowie Näheres zur Erhebung der Gebühren und Auslagen durch Rechtsverordnung zu regeln.
Für die Beitreibung von Geldforderungen ist eine Grundgebühr festzulegen, die mit der Beauftragung der Vollstreckungsbehörde entsteht.

### Abschnitt 6  
Schlussvorschriften

#### § 40  
Einschränkung von Grundrechten

Durch Maßnahmen aufgrund dieses Gesetzes können das Recht auf körperliche Unversehrtheit (Artikel 2 Absatz 2 Satz 1 des Grundgesetzes und Artikel 8 Absatz 1 Satz 1 der Verfassung des Landes Brandenburg), die Freiheit der Person (Artikel 2 Absatz 2 Satz 2 des Grundgesetzes und Artikel 9 Absatz 1 Satz 1 der Verfassung des Landes Brandenburg), die Unverletzlichkeit der Wohnung (Artikel 13 Absatz 1 des Grundgesetzes und Artikel 15 Absatz 1 der Verfassung des Landes Brandenburg) und das Recht auf Datenschutz (Artikel 11 der Verfassung des Landes Brandenburg) eingeschränkt werden.

#### § 41  
Übergangsvorschrift

Vollstreckungsverfahren, die vor dem Inkrafttreten dieses Gesetzes bereits eingeleitet sind, werden nach den bislang für sie geltenden Bestimmungen durchgeführt.