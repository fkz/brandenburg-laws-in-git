## Gesetz zu dem Staatsvertrag über die Organisation eines gemeinsamen Akkreditierungssystems zur Qualitätssicherung in Studium und Lehre an deutschen Hochschulen

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem in Berlin am 1.
Juni 2017 vom Land Brandenburg unterzeichneten Staatsvertrag über die Organisation eines gemeinsamen Akkreditierungssystems zur Qualitätssicherung in Studium und Lehre an deutschen Hochschulen wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

#### § 2 

Die Rechtsverordnungen nach Artikel 4 und Artikel 16 Absatz 2 des Staatsvertrages erlässt das für Hochschulen zuständige Mitglied der Landesregierung.

#### § 3 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel 18 Absatz 1 Satz 2 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 14.
Dezember 2017

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag](/vertraege/studienakkreditierungsstaatsvertrag) 

* * *

### Anlagen

1

[Staatsvertrag über die Organisation eines gemeinsamen Akkreditierungssystems zur Qualitätssicherung in Studium und Lehre an deutschen Hochschulen (Studienakkreditierungsstaatsvertrag)](/br2/sixcms/media.php/68/GVBl_I_32_2017-Anlage.pdf "Staatsvertrag über die Organisation eines gemeinsamen Akkreditierungssystems zur Qualitätssicherung in Studium und Lehre an deutschen Hochschulen (Studienakkreditierungsstaatsvertrag)") 779.2 KB