## Gesetz über kommunale Gemeinschaftsarbeit im Land Brandenburg (GKGBbg)

Inhaltsübersicht
----------------

Teil 1  
Grundlagen
-------------------

[§ 1 Kooperationshoheit](#1)  
[§ 2 Formen der kommunalen Zusammenarbeit](#2)  
[§ 3 Beauftragung und Aufgabenübertragung](#3)

Teil 2  
Die Arbeitsgemeinschaft
--------------------------------

[§ 4 Arbeitsgemeinschaft](#4)

Teil 3  
Die öffentlich-rechtliche Vereinbarung
-----------------------------------------------

[§ 5 Mandatierende und delegierende Vereinbarung](#5)  
[§ 6 Beteiligte](#6)  
[§ 7 Inhalt der Vereinbarung](#7)  
[§ 8 Öffentliche Bekanntmachung](#8)  
[§ 9 Wirksamwerden](#9)

Teil 4  
Der Zweckverband
-------------------------

### Abschnitt 1  
Grundlagen des Zweckverbandes

[§ 10 Wesen und Rechtsnatur des Zweckverbandes](#10)  
[§ 11 Mitgliedschaft im Zweckverband](#11)  
[§ 12 Anwendung von Rechtsvorschriften](#12)

### Abschnitt 2  
Bildung des Zweckverbandes

[§ 13 Verbandssatzung](#13)  
[§ 14 Öffentliche Bekanntmachung der Verbandssatzung, Entstehung des Zweckverbandes](#14)  
[§ 15 Vereinbarung anderer Satzungen des Zweckverbandes](#15)  
[§ 16 Vermögensübertragung und Ausgleich](#16)

### Abschnitt 3  
Innere Verfassung und Verwaltung des Zweckverbandes

[§ 17 Organe](#17)  
[§ 18 Zuständigkeiten der Verbandsversammlung](#18)  
[§ 19 Mitgliedschaft und Stimmabgabe in der Verbandsversammlung](#19)  
[§ 20 Besondere Regelungen über Einberufung und Beschlussfassung der Verbandsversammlung](#20)  
[§ 21 Verbandsvorsteherin oder Verbandsvorsteher (Verbandsleitung)](#21)  
[§ 22 Ehrenamtliche Verbandsleitung](#22)  
[§ 23 Hauptamtliche Verbandsleitung](#23)  
[§ 24 Stellvertretung der Verbandsleitung](#24)  
[§ 25 Verbandsausschuss](#25)  
[§ 26 Besondere Regelungen zur Abgabe von Erklärungen](#26)  
[§ 27 Wahrnehmung der Verwaltungs- und Kassengeschäfte](#27)  
[§ 28 Anwendung der Vorschriften über die Wirtschaftsführung, das Rechnungswesen und die Jahresabschlussprüfung von Eigenbetrieben](#28)  
[§ 29 Deckung des Finanzbedarfes](#29)  
[§ 30 Örtliche Prüfung](#30)

### Abschnitt 4  
Änderungen des Zweckverbandes

[§ 31 Änderungen der Verbandssatzung](#31)  
[§ 32 Beitritt und Austritt von Verbandsmitgliedern](#32)  
[§ 33 Auflösung und Abwicklung des Zweckverbandes](#33)  
[§ 34 Rechtsnachfolge bei Verbandsmitgliedern](#34)  
[§ 35 Zusammenschluss von Zweckverbänden](#35)  
[§ 36 Umwandlung einer gemeinsamen kommunalen Anstalt in einen Zweckverband](#36)

Teil 5  
Die gemeinsame kommunale Anstalt
-----------------------------------------

[§ 37 Errichtung einer gemeinsamen kommunalen Anstalt](#37)  
[§ 38 Anwendung von Rechtsvorschriften](#38)  
[§ 39 Besondere Vorschriften für die gemeinsame kommunale Anstalt](#39)  
[§ 40 Verordnungsermächtigung](#40)

Teil 6  
Anzeige- und Genehmigungspflichten, Aufsicht
-----------------------------------------------------

[§ 41 Anzeige- und Genehmigungspflichten](#41)  
[§ 42 Aufsicht](#42)  
[§ 43 Anordnung der kommunalen Zusammenarbeit](#43)  
[§ 44 Schlichtung von Streitigkeiten](#44)

Teil 7  
Übergangs- und Schlussvorschriften
-------------------------------------------

[§ 45 Übergangsvorschriften](#45)  
[§ 46 Rechtsfehler bei der öffentlich-rechtlichen Vereinbarung](#46)  
[§ 47 Rechtsfehler beim Beitritt in einen Zweckverband und bei der Verbandssatzung](#47)  
[§ 48 Rechtsfehler beim Ausscheiden aus einem Zweckverband](#48)  
[§ 49 Rechtsfehler bei der gemeinsamen kommunalen Anstalt](#49)  
[§ 50 Planungsverbände](#50)  
[§ 51 Experimentierklausel](#51)

Teil 1   
Grundlagen
--------------------

#### § 1  
Kooperationshoheit

(1) Kommunen können zur Erfüllung öffentlicher Aufgaben zusammenarbeiten.
Dies gilt für alle Selbstverwaltungsaufgaben, Pflichtaufgaben zur Erfüllung nach Weisung und Auftragsangelegenheiten.

(2) Eine kommunale Zusammenarbeit ist nur ausgeschlossen, soweit dies durch Gesetz ausdrücklich geregelt ist.

(3) Kommunen im Sinne dieses Gesetzes sind die Gemeinden, Verbandsgemeinden und Landkreise.
Die Ämter, Zweckverbände, kommunalen Anstalten und gemeinsamen kommunalen Anstalten werden den Kommunen gleichgestellt, soweit durch dieses Gesetz nichts anderes geregelt ist.

(4) Die Erfüllung öffentlicher Aufgaben mit einer Kommune eines anderen Landes ist möglich, soweit dies aufgrund eines Staatsvertrages des Landes Brandenburg mit diesem Land zulässig ist.

#### § 2   
Formen der kommunalen Zusammenarbeit

(1) Kommunen können nach diesem Gesetz

1.  in Arbeitsgemeinschaften zusammenarbeiten (§ 4),
    
2.  mandatierende oder delegierende öffentlich-rechtliche Vereinbarungen abschließen (§§ 5 bis 9),
    
3.  Zweckverbände bilden oder sich an Zweckverbänden beteiligen (§§ 10 bis 36),
    
4.  gemeinsame kommunale Anstalten des öffentlichen Rechts (gemeinsame kommunale Anstalten) errichten oder sich an gemeinsamen kommunalen Anstalten als Träger beteiligen (§§ 37 bis 40).
    

Sonstige Regelungen über Formen der kommunalen Zusammenarbeit schließen die Formen nach Satz 1 nur aus, soweit dies durch Gesetz ausdrücklich geregelt ist.

(2) Die Möglichkeiten der Kommunen, in den Formen des Privatrechts zusammenzuarbeiten, bleiben unberührt.

#### § 3   
Beauftragung und Aufgabenübertragung

(1) Nach Maßgabe dieses Gesetzes können Kommunen insbesondere

1.  eine andere Kommune mit der Durchführung von öffentlichen Aufgaben beauftragen (Mandatierung) oder
    
2.  öffentliche Aufgaben auf eine andere Kommune übertragen (Delegation).
    

Die Zusammenarbeit kann sich auf sachlich und örtlich begrenzte Teile der Aufgaben beziehen.

(2) Mit der Beauftragung zur Durchführung einer Aufgabe nach Absatz 1 Satz 1 Nummer 1 bleiben die Rechte und Pflichten der beauftragenden Kommune in Bezug auf die Aufgabenerfüllung unberührt.
Die beauftragende Kommune kann der beauftragten Kommune fachliche Weisungen erteilen.

(3) Mit der Übertragung einer Aufgabe nach Absatz 1 Satz 1 Nummer 2 gehen alle mit der Trägerschaft der Aufgabe verbundenen Rechte und Pflichten über.
Die Befugnis, für die übertragene Aufgabe Satzungen und Verordnungen zu erlassen, geht über, soweit die delegierende öffentlich-rechtliche Vereinbarung, die Verbandssatzung oder die Anstaltssatzung nichts anderes bestimmen.
Die übertragene Aufgabe kann auf eine andere Kommune übertragen werden, soweit die delegierende öffentlich-rechtliche Vereinbarung, die Verbandssatzung oder die Anstaltssatzung nichts anderes bestimmen.

Teil 2   
Die Arbeitsgemeinschaft
---------------------------------

#### § 4  
Arbeitsgemeinschaft

(1) Kommunen können aufgrund eines öffentlich-rechtlichen Vertrages in Arbeitsgemeinschaften zusammenarbeiten.
An der Arbeitsgemeinschaft können sich auch sonstige Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts sowie natürliche Personen und juristische Personen des Privatrechts als Mitglieder beteiligen.

(2) In einer Arbeitsgemeinschaft beraten die Mitglieder Angelegenheiten, die sie gemeinsam betreffen.
Die Arbeitsgemeinschaft dient insbesondere dazu, Planungen und die Tätigkeit von Einrichtungen, Dienststellen oder Unternehmen der Mitglieder aufeinander abzustimmen, gemeinsame Flächennutzungspläne vorzubereiten, andere Formen kommunaler Zusammenarbeit vorzubereiten oder die gemeinsame wirtschaftliche und zweckmäßige Erfüllung der Aufgaben in einem größeren nachbarlichen Gebiet sicherzustellen.

(3) Durch Beschlüsse der Arbeitsgemeinschaft werden die Mitglieder nicht gebunden.

(4) In dem Vertrag über die Bildung der Arbeitsgemeinschaft sollen die Aufgaben der Arbeitsgemeinschaft, die Geschäftsordnung und die Deckung des Finanzbedarfs geregelt werden.
Der Vertrag wird wirksam, sobald er von allen Mitgliedern unterzeichnet ist.
In dem Vertrag kann ein anderer Zeitpunkt bestimmt werden.

Teil 3   
Die öffentlich-rechtliche Vereinbarung
------------------------------------------------

#### § 5   
Mandatierende und delegierende Vereinbarung

(1) Kommunen können durch öffentlich-rechtlichen Vertrag vereinbaren, eine am Vertrag beteiligte Kommune mit der Durchführung einzelner Aufgaben zu beauftragen (mandatierende öffentlich-rechtliche Vereinbarung) oder einzelne Aufgaben auf eine beteiligte Kommune zu übertragen (delegierende öffentlich-rechtliche Vereinbarung).
Zweckverbände, kommunale Anstalten oder gemeinsame kommunale Anstalten können Aufgaben nur durchführen oder übernehmen, soweit ihnen die Erfüllung dieser Aufgaben satzungsmäßig obliegt.

(2) Beauftragen sich die Beteiligten wechselseitig mit der Durchführung der gleichen Aufgabe, ist jede beteiligte Kommune berechtigt, die Aufgabe für die Beteiligten durchzuführen.

(3) Kommunen können in einer mandatierenden öffentlich-rechtlichen Vereinbarung auch die Bildung einer gemeinsamen Dienststelle vereinbaren.
Eine gemeinsame Dienststelle kann auch als Teil einer der beteiligten Körperschaften eingerichtet werden.
Die Bediensteten üben ihre Tätigkeiten in der gemeinsamen Dienststelle nach der fachlichen Weisung der im Einzelfall sachlich und örtlich zuständigen Kommune aus; ihre dienstrechtliche Stellung im Übrigen bleibt unberührt.
Verletzt ein Bediensteter in Ausübung seiner Tätigkeit in der gemeinsamen Dienststelle die ihm einem Dritten gegenüber obliegende Amtspflicht, haftet die Kommune, die für die Amtshandlung sachlich und örtlich zuständig ist.

#### § 6   
Beteiligte

(1) Neben Kommunen können

1.  andere juristische Personen des öffentlichen Rechts,
    
2.  natürliche Personen oder
    
3.  juristische Personen des Privatrechts
    

an einer mandatierenden öffentlich-rechtlichen Vereinbarung beteiligt werden, soweit Rechtsvorschriften nicht entgegenstehen und die Kommunen solche Personen beteiligen dürften, wenn sie die Aufgabe allein erfüllten.

(2) Kommunen können mandatierende öffentlich-rechtliche Vereinbarungen mit dem Land Berlin oder vom Land Berlin errichteten juristischen Personen des öffentlichen Rechts abschließen.

#### § 7   
Inhalt der Vereinbarung

(1) In der öffentlich-rechtlichen Vereinbarung sind die Beteiligten und die einzelnen Aufgaben zu bestimmen.

(2) In der delegierenden öffentlich-rechtlichen Vereinbarung kann bestimmt werden, dass die Befugnis, in Bezug auf die übertragene Aufgabe Satzungen und Verordnungen zu erlassen, bei der übertragenden Kommune verbleibt.

(3) In der öffentlich-rechtlichen Vereinbarung kann der beauftragenden oder übertragenden Kommune ein Mitwirkungsrecht bei der Erfüllung der Aufgabe eingeräumt werden.

(4) In der öffentlich-rechtlichen Vereinbarung soll eine Kostenregelung enthalten sein.

(5) Die öffentlich-rechtliche Vereinbarung kann befristet oder unbefristet geschlossen werden.
Wird eine öffentlich-rechtliche Vereinbarung unbefristet oder über mehr als zwanzig Jahre geschlossen, so ist in der Vereinbarung zu regeln, unter welchen Voraussetzungen sie durch einen einzelnen Beteiligten gekündigt oder durch alle Beteiligten aufgehoben werden kann.
Das besondere Kündigungsrecht nach § 60 des Verwaltungsverfahrensgesetzes bleibt unberührt.

#### § 8  
Öffentliche Bekanntmachung

(1) Die beteiligten Kommunen haben die öffentlich-rechtliche Vereinbarung nach den für ihre Satzungen geltenden Vorschriften bekannt zu machen.

(2) Eine delegierende öffentlich-rechtliche Vereinbarung mit mehr als zwei beteiligten Kommunen kann vorsehen, dass sie abweichend von Absatz 1 durch die Kommunalaufsichtsbehörde öffentlich bekannt zu machen ist.
In diesem Fall macht die Kommunalaufsichtsbehörde die Vereinbarung in der Form bekannt, die für die öffentliche Bekanntmachung der Satzungen ihres Landkreises vorgeschrieben ist; dabei ist ein Hinweis auf eine aufsichtsbehördliche Genehmigung nicht erforderlich.
Ist das für Inneres zuständige Ministerium Kommunalaufsichtsbehörde, macht es die Vereinbarung im Amtsblatt für Brandenburg öffentlich bekannt.
Die beteiligten Kommunen haben in der für die öffentliche Bekanntmachung ihrer Satzungen vorgeschriebenen Form auf die öffentliche Bekanntmachung der Kommunalaufsichtsbehörde hinzuweisen.

(3) Für die Änderung, Aufhebung und Kündigung einer öffentlich-rechtlichen Vereinbarung gelten die Absätze 1 und 2 entsprechend.
Die Änderung einer mandatierenden öffentlich-rechtlichen Vereinbarung bedarf nur dann einer öffentlichen Bekanntmachung, wenn der Kreis der Beteiligten oder der Bestand der von der Vereinbarung erfassten Aufgaben geändert wird.

(4) Erlässt eine Kommune zur Erfüllung einer ihr übertragenen Aufgabe Satzungen oder Verordnungen, hat die Kommune, die die Aufgaben übertragen hat, in der für die öffentliche Bekanntmachung ihrer Satzung vorgeschriebenen Form auf die Bekanntmachung der satzungsgebenden Kommune hinzuweisen.

#### § 9   
Wirksamwerden

(1) Die mandatierende öffentlich-rechtliche Vereinbarung wird mit ihrem Abschluss wirksam, wenn nicht ein anderer Zeitpunkt vereinbart ist.

(2) Die delegierende öffentlich-rechtliche Vereinbarung wird am Tag nach der letzten öffentlichen Bekanntmachung wirksam, wenn nicht in der Vereinbarung ein späterer Zeitpunkt geregelt ist.
Genehmigungspflichtige öffentlich-rechtliche Vereinbarungen werden frühestens mit Wirksamkeit der Genehmigung wirksam.

(3) Die Absätze 1 und 2 gelten für Änderungen und Aufhebungen entsprechend.
Absatz 2 gilt für die Kündigung delegierender öffentlich-rechtlicher Vereinbarungen entsprechend.

Teil 4   
Der Zweckverband
--------------------------

### Abschnitt 1   
Grundlagen des Zweckverbandes

#### § 10   
Wesen und Rechtsnatur des Zweckverbandes

(1) Kommunen können zur gemeinsamen Erfüllung öffentlicher Aufgaben in einem Zweckverband zusammenarbeiten, um den Zweckverband mit der Durchführung einzelner Aufgaben zu beauftragen oder um einzelne Aufgaben auf den Zweckverband zu übertragen.

(2) Der Zweckverband ist eine Körperschaft des öffentlichen Rechts.
Er verwaltet seine Angelegenheiten im Rahmen der Gesetze in eigener Verantwortung.
Der Zweckverband hat das Recht, Beamtinnen und Beamte zu haben, soweit die Verbandssatzung dies vorsieht.

#### § 11   
Mitgliedschaft im Zweckverband

(1) Neben den kommunalen Mitgliedern nach § 10 Absatz 1 können auch der Bund, die Länder der Bundesrepublik Deutschland und andere Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts Mitglieder eines Zweckverbandes sein, soweit nicht die für sie geltenden besonderen Vorschriften die Beteiligung ausschließen oder beschränken.
Ebenso können natürliche Personen und juristische Personen des Privatrechts Mitglieder eines Zweckverbandes sein, wenn die Erfüllung der Verbandsaufgaben dadurch gefördert wird und Gründe des öffentlichen Wohls nicht entgegenstehen.

(2) Sind neben Kommunen weitere Personen nach Absatz 1 Verbandsmitglieder, so dürfen ihre Stimmen insgesamt die Hälfte der satzungsmäßigen Stimmenzahl der Verbandsversammlung nicht erreichen.
Die Kommunalaufsichtsbehörde kann Ausnahmen zulassen.

(3) Die Verbandssatzung kann eine befristete Mitgliedschaft einzelner oder aller Mitglieder vorsehen.

#### § 12  
Anwendung von Rechtsvorschriften

(1) Auf die Zweckverbände sind die Vorschriften der Kommunalverfassung des Landes Brandenburg, die für die kreisangehörigen amtsfreien Gemeinden gelten, entsprechend anwendbar.
Dies gilt nicht, soweit in diesem Gesetz oder anderen Rechtsvorschriften eine abweichende Regelung getroffen wird.
An die Stelle der Gemeindevertretung tritt die Verbandsversammlung, an die Stelle der Fraktionen treten die Verbandsmitglieder und an die Stelle der hauptamtlichen Bürgermeisterin oder des hauptamtlichen Bürgermeisters tritt die Verbandsvorsteherin oder der Verbandsvorsteher.
§ 10 der Kommunalverfassung des Landes Brandenburg gilt mit der Maßgabe, dass ein Dienstsiegel geführt werden kann.

(2) Vorschriften, die aufgrund der Kommunalverfassung des Landes Brandenburg für die Gemeinden erlassen wurden, gelten für die Zweckverbände entsprechend, soweit nicht in diesen oder anderen Rechtsvorschriften abweichende Regelungen getroffen oder die Zweckverbände von der Anwendung ausgenommen werden.

(3) Soweit in Rechtsvorschriften der Gemeindeverband als Sammelbegriff verwendet wird, gilt auch der Zweckverband als Gemeindeverband.

### Abschnitt 2   
Bildung des Zweckverbandes

#### § 13   
Verbandssatzung

(1) Zur Bildung des Zweckverbandes vereinbaren die Beteiligten eine Verbandssatzung, in der die Rechtsverhältnisse des Zweckverbandes geregelt werden.
Vor der Bildung von Zweckverbänden, die Kreisgrenzen überschreiten, sind die betroffenen Landkreise rechtzeitig zu unterrichten.

(2) Die Verbandssatzung muss bestimmen

1.  die Verbandsmitglieder und die Zahl ihrer Stimmen in der Verbandsversammlung,
    
2.  die Aufgaben des Zweckverbandes,
    
3.  den Namen des Zweckverbandes,
    
4.  den Sitz des Zweckverbandes,
    
5.  den Maßstab, nach dem die Verbandsmitglieder nach § 29 zur Deckung des Finanzbedarfs beizutragen haben, und
    
6.  die Form der öffentlichen Bekanntmachung des Zweckverbandes.
    

(3) Darüber hinaus kann die Verbandssatzung insbesondere Bestimmungen enthalten über

1.  die innere Verfassung und Verwaltung des Zweckverbandes einschließlich der ehrenamtlichen oder der hauptamtlichen Tätigkeit der Verbandsvorsteherin oder des Verbandsvorstehers,
    
2.  die Abwicklung im Fall der Auflösung des Zweckverbandes einschließlich der Übernahme der Beschäftigten und
    
3.  den Maßstab für die Konsolidierung nach § 83 der Kommunalverfassung des Landes Brandenburg.
    

(4) Bestimmungen der Verbandssatzung nach Absatz 2 Nummer 6 können von den Bestimmungen der Rechtsverordnung im Sinne des § 3 Absatz 3 Satz 2 der Kommunalverfassung des Landes Brandenburg abweichen.
Die Verletzung von Bestimmungen über die öffentliche Bekanntmachung ist unter den Voraussetzungen des § 3 Absatz 4 der Kommunalverfassung des Landes Brandenburg unbeachtlich.

#### § 14  
Öffentliche Bekanntmachung der Verbandssatzung, Entstehung des Zweckverbandes

(1) Die Kommunalaufsichtsbehörde hat die Verbandssatzung in der Form öffentlich bekannt zu machen, die für die öffentliche Bekanntmachung der Satzungen ihres Landkreises vorgeschrieben ist; dabei ist ein Hinweis auf eine aufsichtsbehördliche Genehmigung nicht erforderlich.
Ist das für Inneres zuständige Ministerium Kommunalaufsichtsbehörde, erfolgt die öffentliche Bekanntmachung im Amtsblatt für Brandenburg.
§ 3 Absatz 4 Satz 3 der Kommunalverfassung des Landes Brandenburg gilt entsprechend.
Die kommunalen Mitglieder haben in der für die öffentliche Bekanntmachung ihrer Satzungen vorgeschriebenen Form auf die Bekanntmachung nach Satz 1 oder Satz 2 hinzuweisen.

(2) Der Zweckverband entsteht am Tag nach der öffentlichen Bekanntmachung der Verbandssatzung nach Absatz 1 Satz 1 oder Satz 2, wenn nicht in der Verbandssatzung ein späterer Zeitpunkt bestimmt ist.
Nach der öffentlichen Bekanntmachung können Rechtsfehler bei der Bildung des Zweckverbandes nur mit Wirkung für die Zukunft geltend gemacht werden.

#### § 15  
Vereinbarung anderer Satzungen des Zweckverbandes

– Neben der Verbandssatzung können die Beteiligten bei der Bildung des Zweckverbandes andere Satzungen des Zweckverbandes vereinbaren.
Die Satzungen sind mit der Verbandssatzung gemäß § 14 Absatz 1 öffentlich bekannt zu machen.
Die Satzungen treten am Tag nach ihrer öffentlichen Bekanntmachung nach § 14 Absatz 1 Satz 1 oder Satz 2 in Kraft, wenn nicht in den Satzungen ein späterer Zeitpunkt bestimmt ist.
Der Zweckverband kann die Satzungen ändern, ersetzen oder aufheben.
Die öffentliche Bekanntmachung der Satzungen nach Satz 4 erfolgt nach den Bekanntmachungsvorschriften für die Satzungen des Zweckverbandes.

#### § 16  
Vermögensübertragung und Ausgleich

(1) Neben der Verbandssatzung können die Beteiligten schriftliche Vereinbarungen abschließen über

1.  die Übertragung der für die Aufgabenerfüllung erforderlichen Vermögensgegenstände und der mit der Aufgabenerfüllung im Zusammenhang stehenden Verbindlichkeiten und Forderungen,
    
2.  den Ausgleich von Vorteilen und Nachteilen, die sich für sie aus der Bildung des Zweckverbandes ergeben.
    

(2) Auf Antrag sämtlicher Beteiligten kann die Kommunalaufsichtsbehörde die Vermögensübertragung und den Ausgleich nach pflichtgemäßem Ermessen durch Bescheid regeln.
Die Kommunalaufsichtsbehörde kann sich sachkundiger Dritter bedienen und dadurch entstandene Kosten den Beteiligten durch Bescheid auferlegen.

### Abschnitt 3   
Innere Verfassung und Verwaltung des Zweckverbandes

#### § 17   
Organe

Organe des Zweckverbandes sind

1.  die Verbandsversammlung und
    
2.  die Verbandsvorsteherin oder der Verbandsvorsteher (Verbandsleitung).
    

Die Verbandssatzung kann als weiteres Organ einen Verbandsausschuss vorsehen.

#### § 18  
Zuständigkeiten der Verbandsversammlung

Die Verbandsversammlung entscheidet über alle Angelegenheiten des Zweckverbandes, soweit gesetzlich oder durch die Verbandssatzung nichts anderes bestimmt ist.
Sie kann ihre Zuständigkeit in Einzelfällen oder für Gruppen von Angelegenheiten durch Beschluss auf die Verbandsleitung übertragen; § 28 Absatz 2 der Kommunalverfassung des Landes Brandenburg bleibt unberührt.

### § 19   
Mitgliedschaft und Stimmabgabe in der Verbandsversammlung

(1) Die Verbandsversammlung setzt sich aus den Vertreterinnen und Vertretern der Verbandsmitglieder (Vertretungspersonen) zusammen.
Jedes Verbandsmitglied entsendet eine Vertretungsperson in die Verbandsversammlung.
Jedes Verbandsmitglied hat eine Stimme, sofern nicht die Verbandssatzung für einzelne oder alle Verbandsmitglieder andere Stimmenzahlen festlegt.
Die Vertretungsperson gibt alle Stimmen des Verbandsmitgliedes einheitlich ab.

(2) Die Verbandssatzung kann bestimmen, dass einzelne oder alle Verbandsmitglieder mehrere Vertretungspersonen in die Verbandsversammlung entsenden.
Die bei der Beschlussfassung anwesenden Vertretungspersonen eines Verbandsmitgliedes geben alle dem Verbandsmitglied nach der Verbandssatzung zustehenden Stimmen ab.
Die Stimmen eines Verbandsmitgliedes sind einheitlich abzugeben; eine uneinheitliche Stimmabgabe ist ungültig.
Erfolgt ein Beschluss durch geheime Stimmabgabe oder zeigt die Person nach Absatz 3 der oder dem Vorsitzenden der Verbandsversammlung an, dass den Vertretungspersonen des Verbandsmitgliedes eine Weisung nach Absatz 7 erteilt wurde, so gibt eine Stimmführerin oder ein Stimmführer alle Stimmen des Verbandsmitgliedes einheitlich ab.
Hat die Gemeindevertretung, der Kreistag, der Amtsausschuss, die Verbandsversammlung oder der Verwaltungsrat (Vertretungskörperschaft) des kommunalen Verbandsmitgliedes keine Stimmführerin oder keinen Stimmführer bestimmt und einigen sich die anwesenden Vertretungspersonen des kommunalen Verbandsmitgliedes vor der Stimmabgabe nicht auf eine Stimmführerin oder einen Stimmführer, ist die Person nach Absatz 3 Stimmführerin oder Stimmführer.

(3) Die kommunalen Verbandsmitglieder werden in der Verbandsversammlung durch ihre Hauptverwaltungsbeamtin oder ihren Hauptverwaltungsbeamten vertreten; § 135 Absatz 4 Satz 2 der Kommunalverfassung des Landes Brandenburg findet keine Anwendung.
Im Fall der Verhinderung werden sie durch ihre allgemeinen Stellvertreterinnen oder Stellvertreter vertreten, wenn sie nicht eine andere Bedienstete oder einen anderen Bediensteten benennen.
Sie können eine Bedienstete oder einen Bediensteten mit der Wahrnehmung der Vertretung des Mitglieds in der Verbandsversammlung dauerhaft betrauen.
Ist die betraute Person verhindert, nimmt die Hauptverwaltungsbeamtin oder der Hauptverwaltungsbeamte die Vertretung wahr, wenn sie oder er die Verhinderungsvertretung der betrauten Person nicht auf eine andere Bedienstete oder auf einen anderen Bediensteten dauerhaft übertragen hat.
Abweichend von den Sätzen 1 bis 4 kann bei amtsangehörigen Gemeinden die Gemeindevertretung eine andere Vertretungsperson und deren Stellvertreterinnen oder Stellvertreter wählen; Absatz 4 findet entsprechende Anwendung.
Für Ortsgemeinden und mitverwaltete Gemeinden gilt Satz 5 entsprechend.

(4) Weitere Vertretungspersonen der kommunalen Mitglieder und deren Stellvertreterinnen und Stellvertreter werden gemäß den §§ 40, 41 der Kommunalverfassung des Landes Brandenburg von der Vertretungskörperschaft des Mitglieds für die Dauer ihrer Wahlperiode gewählt und üben ihr Amt bis zum Amtsantritt der neuen Vertretungspersonen weiter aus.
Wählbar sind die Mitglieder der Vertretungskörperschaft und die Bediensteten des Verbandsmitglieds, bei amtsangehörigen Gemeinden auch die Bediensteten des Amtes.

(5) Sind neben Kommunen weitere Personen nach § 11 Absatz 1 Verbandsmitglieder, werden die Vertretungspersonen durch diese Verbandsmitglieder in die Verbandsversammlung entsandt und üben ihr Amt nach Ablauf der Zeit, für die sie entsandt sind, bis zum Amtsantritt der neu entsandten Vertretungspersonen weiter aus.

(6) Die Vertretungsperson eines Verbandsmitgliedes scheidet aus der Verbandsversammlung aus, wenn die Voraussetzungen ihrer Wahl oder Entsendung wegfallen.

(7) Die Vertretungskörperschaft eines kommunalen Verbandsmitgliedes kann den Vertretungspersonen des Verbandsmitgliedes Richtlinien und Weisungen erteilen.
Für den Fall einer Weisung oder einer geheimen Stimmabgabe in der Verbandsversammlung kann sie eine Stimmführerin oder einen Stimmführer durch offenen Wahlbeschluss bestimmen.

#### § 20   
Besondere Regelungen über Einberufung und Beschlussfassung der Verbandsversammlung

(1) Die Einberufung zur ersten Sitzung der Verbandsversammlung eines neu gebildeten Zweckverbandes erfolgt durch die an Lebensjahren älteste, nicht verhinderte Person nach § 19 Absatz 3 Satz 1.

(2) Die Beschlussfähigkeit nach § 38 der Kommunalverfassung des Landes Brandenburg richtet sich nach der satzungsmäßigen Stimmenzahl.
Die oder der Vorsitzende hat die Beschlussunfähigkeit auch ohne Antrag festzustellen, wenn die anwesenden Vertretungspersonen der kommunalen Mitglieder weniger als die Hälfte der in der Sitzung vertretenen Stimmen erreichen.
Dies gilt auch für den Fall einer erneuten Einberufung nach § 38 Absatz 2 der Kommunalverfassung des Landes Brandenburg.

(3) Beschlüsse werden, soweit durch ein Gesetz oder die Verbandssatzung nichts anderes bestimmt ist, mit der Mehrheit der auf Ja oder Nein lautenden Stimmen gefasst.
Schreibt ein Gesetz oder die Verbandssatzung Einstimmigkeit bei der Beschlussfassung vor, so ist der Beschluss ohne Gegenstimme zu fassen.

#### § 21  
Verbandsvorsteherin oder Verbandsvorsteher (Verbandsleitung)

(1) Die Verbandsvorsteherin oder der Verbandsvorsteher (Verbandsleitung) wird von der Verbandsversammlung für die Dauer von acht Jahren gewählt.
Die Verbandssatzung kann eine kürzere Wahlzeit vorsehen.

(2) Die Verbandsleitung kann ehrenamtlich oder hauptamtlich tätig sein.

(3) In Rechts- und Verwaltungsgeschäften kann die Verbandsleitung die Bezeichnung „Verbandsvorsteherin“ oder „Verbandsvorsteher“ führen.

(4) Die Verbandsversammlung kann die Verbandsleitung vor Ablauf der Wahlzeit im Zweckverband abwählen.
Für den Antrag auf Abwahl ist die Mehrheit der satzungsmäßigen Stimmenzahl der Verbandsversammlung erforderlich.
Der Antrag ist von den antragstellenden Mitgliedern der Verbandsversammlung gemeinsam und eigenhändig unterschrieben zu stellen; § 19 Absatz 2 Satz 3 gilt entsprechend.
Zwischen dem Zugang des Antrages bei der oder dem Vorsitzenden der Verbandsversammlung und der Sitzung der Verbandsversammlung muss eine Frist von mindestens sechs Wochen liegen.
Über den Antrag ist ohne Aussprache abzustimmen.
Der Beschluss über die Abwahl bedarf einer Mehrheit von zwei Dritteln der satzungsmäßigen Stimmenzahl der Verbandsversammlung.
Die Sätze 2 bis 6 finden keine Anwendung, wenn die Verbandsleitung der oder dem Vorsitzenden der Verbandsversammlung mündlich zur Niederschrift oder schriftlich erklärt, dass sie mit ihrer vorzeitigen Abwahl einverstanden ist.

#### § 22  
Ehrenamtliche Verbandsleitung

(1) Die Verbandsleitung ist ehrenamtlich tätig, wenn die Verbandssatzung keine hauptamtliche Verbandsleitung vorsieht.

(2) Bei Zweckverbänden, denen Aufgaben mit Anschluss- und Benutzungszwang übertragen worden sind und die keine hauptamtliche Verbandsleitung haben, soll als ehrenamtliche Verbandsleitung eine Hauptverwaltungsbeamtin oder ein Hauptverwaltungsbeamter der Verbandsmitglieder, deren allgemeine Stellvertreterinnen oder Stellvertreter oder Beigeordnete gewählt werden.
Entfallen die Voraussetzungen nach Satz 1 für die Wahl, soll die ehrenamtliche Verbandsleitung abgewählt werden; § 21 Absatz 4 findet keine Anwendung.

(3) Die nach Absatz 2 gewählten Hauptverwaltungsbeamtinnen oder Hauptverwaltungsbeamten, deren allgemeine Stellvertreterinnen oder Stellvertreter oder Beigeordneten sind verpflichtet, die Wahl anzunehmen und die Funktion als ehrenamtliche Verbandsleitung auszuüben.
§ 20 Absatz 1 Satz 4 und 5 der Kommunalverfassung des Landes Brandenburg gilt entsprechend.

(4) Die ehrenamtliche Verbandsleitung kann vom Zweckverband eine angemessene Aufwandsentschädigung nach Maßgabe einer Entschädigungssatzung erhalten.

#### § 23  
Hauptamtliche Verbandsleitung

(1) Die hauptamtliche Verbandsleitung muss die erforderlichen fachlichen Voraussetzungen erfüllen und ausreichende Erfahrung für die wahrzunehmende Aufgabe haben.

(2) Die Stelle der hauptamtlichen Verbandsleitung ist öffentlich auszuschreiben.
Bei der Wiederwahl kann die Verbandsversammlung mit der Mehrheit der satzungsmäßigen Stimmenzahl durch Beschluss von der Ausschreibung absehen.

(3) Im Anstellungsvertrag einer hauptamtlichen Verbandsleitung sind die Befristung und die Möglichkeit einer vorzeitigen Abwahl gemäß § 21 zu berücksichtigen.

#### § 24  
Stellvertretung der Verbandsleitung

(1) Die Verbandsversammlung wählt eine ehrenamtliche allgemeine Stellvertreterin oder einen ehrenamtlichen allgemeinen Stellvertreter der Verbandsleitung für die Dauer von acht Jahren aus ihrer Mitte oder aus dem Kreis der Bediensteten des Zweckverbandes oder der Personen nach § 22 Absatz 2.
Die Verbandsversammlung kann eine kürzere Wahlzeit beschließen.
Für die Abwahl gilt § 21 Absatz 4 entsprechend.
Werden weitere Stellvertreterinnen oder Stellvertreter gewählt, bestimmt die Verbandsversammlung die Reihenfolge der Vertretung.

(2) Bei Zweckverbänden, denen Aufgaben mit Anschluss- und Benutzungszwang übertragen worden sind, soll die allgemeine Stellvertreterin oder der allgemeine Stellvertreter der Verbandsleitung aus dem Kreis der Personen nach § 22 Absatz 2 oder der Bediensteten des Zweckverbandes gewählt werden.
§ 22 Absatz 2 Satz 2 und § 23 Absatz 1 gelten entsprechend.

(3) Die nach Absatz 2 gewählten Hauptverwaltungsbeamtinnen oder Hauptverwaltungsbeamten, deren allgemeine Stellvertreterinnen oder Stellvertreter oder Beigeordneten sind verpflichtet, die Wahl anzunehmen und die Funktion der ehrenamtlichen allgemeinen Stellvertretung der Verbandsleitung auszuüben.
§ 20 Absatz 1 Satz 4 und 5 der Kommunalverfassung des Landes Brandenburg gilt entsprechend.

(4) Sind die Verbandsleitung und alle Stellvertreterinnen oder Stellvertreter verhindert oder sind diese Ämter vakant, so nehmen die Hauptverwaltungsbeamtinnen oder Hauptverwaltungsbeamten der kommunalen Mitglieder die Stellvertretung wahr.
Im Falle der Verhinderung oder Vakanz aller Hauptverwaltungsbeamtinnen oder Hauptverwaltungsbeamten treten deren allgemeine Stellvertreterinnen oder Stellvertreter an die Stelle.
Die Reihenfolge der Stellvertretung nach Satz 1 und 2 bestimmt sich nach dem Lebensalter, soweit die Verbandsversammlung nichts anderes beschließt.

(5) Für die Aufwandsentschädigung gilt § 22 Absatz 4 entsprechend.

#### § 25  
Verbandsausschuss

(1) Der Verbandsausschuss besteht aus der Verbandsleitung und weiteren Mitgliedern.
Die Anzahl der Mitglieder des Verbandsausschusses wird durch die Verbandssatzung bestimmt.

(2) Die weiteren Mitglieder des Verbandsausschusses werden durch die Verbandsversammlung gewählt.
Die Verbandssatzung kann die Wahl von stellvertretenden weiteren Mitgliedern vorsehen.
Die weiteren Mitglieder des Verbandsausschusses und ihre Stellvertreterinnen und Stellvertreter werden aus dem Kreis der ordentlichen Mitglieder der Verbandsversammlung gewählt.
Dem Verbandsausschuss können neben den Mitgliedern der Verbandsversammlung sachkundige Einwohnerinnen und Einwohner sowie Bedienstete des Zweckverbandes oder der Verbandsmitglieder, bei amtsangehörigen Gemeinden auch Bedienstete des Amtes, als beratende Mitglieder ohne eigenes Stimmrecht angehören.
Ihre Zahl darf insgesamt die Anzahl der Mitglieder der Verbandsversammlung im Verbandsausschuss nicht erreichen.

(3) Die Aufgaben des Verbandsausschusses zur dauernden Erledigung regelt ausschließlich die Verbandssatzung.
Einzelne Angelegenheiten können dem Verbandsausschuss auch durch Beschluss der Verbandsversammlung zur Erledigung übertragen werden.
Die Übertragung von Aufgaben nach Satz 1 oder Satz 2 ist nur zulässig, soweit diese nicht durch Rechtsvorschrift ausschließlich der Verbandsversammlung oder der Verbandsleitung zugewiesen sind.
Die Verbandssatzung kann vorsehen, dass der Verbandsausschuss zur Vorbereitung von Beschlüssen der Verbandsversammlung Empfehlungen abgibt.

#### § 26  
Besondere Regelungen zur Abgabe von Erklärungen

Erklärungen, durch die der Zweckverband verpflichtet werden soll und die nach § 57 der Kommunalverfassung des Landes Brandenburg der Unterschrift von zwei Personen bedürfen, sind

1.  von
    
    1.  der Verbandsleitung oder
        
    2.  einer Stellvertreterin oder einem Stellvertreter der Verbandsleitung und
        
2.  von
    
    1.  der Vorsitzenden oder dem Vorsitzenden der Verbandsversammlung,
        
    2.  einer Stellvertreterin oder einem Stellvertreter der Vorsitzenden oder des Vorsitzenden der Verbandsversammlung,
        
    3.  einer oder einem von der Verbandsversammlung zu bestimmenden Bediensteten des Zweckverbandes oder
        
    4.  einem von der Verbandsversammlung zu bestimmenden Mitglied der Verbandsversammlung
        

zu unterzeichnen.
Die Verbandssatzung kann allgemein oder für einen bestimmten Kreis von Geschäften bestimmen, dass die Unterschrift der Verbandsleitung oder einer Stellvertreterin oder eines Stellvertreters der Verbandsleitung genügt.

#### § 27  
Wahrnehmung der Verwaltungs- und Kassengeschäfte

Die Verbandssatzung kann vorsehen, dass die Kassen- und Verwaltungsgeschäfte einschließlich der Personalverwaltung ganz oder teilweise durch ein kommunales Mitglied im Namen des Zweckverbandes wahrgenommen werden.

#### § 28   
Anwendung der Vorschriften über die Wirtschaftsführung, das Rechnungswesen und die Jahresabschlussprüfung von Eigenbetrieben

Die Verbandssatzung kann bestimmen, dass die Vorschriften über die Wirtschaftsführung, das Rechnungswesen und die Jahresabschlussprüfung der Eigenbetriebe für den Zweckverband sinngemäß Anwendung finden, wenn der Zweckverband sich überwiegend wirtschaftlich betätigt (§ 91 der Kommunalverfassung des Landes Brandenburg).
Die Verbandssatzung kann vorsehen, dass zuständige Stelle für die Jahresabschlussprüfung das nach § 30 zuständige Rechnungsprüfungsamt ist.

#### § 29  
Deckung des Finanzbedarfes

(1) Der Zweckverband hat von den Verbandsmitgliedern eine Umlage zu erheben, soweit seine sonstigen Erträge, Einzahlungen und nicht benötigten Finanzmittel nicht ausreichen, um seinen Finanzbedarf zu decken.
Die Umlagepflicht einzelner Verbandsmitglieder kann, außer bei Sparkassenzweckverbänden, durch die Verbandssatzung auf einen Höchstbetrag beschränkt oder ausgeschlossen werden, wenn die übrigen Mitglieder sich in der Verbandssatzung verpflichten, den Restbetrag der Umlage zu übernehmen.
In der Verbandssatzung ist der Maßstab für die Bemessung der Verbandsumlage zu bestimmen.
Die Umlage soll in der Regel nach dem Verhältnis des Nutzens bemessen werden, den die einzelnen Verbandsmitglieder aus der Erfüllung der Aufgaben des Zweckverbandes haben.
Ein anderer Maßstab kann zugrunde gelegt werden, wenn dies angemessen ist.
Soweit die Umlage nach der Steuerkraft bemessen wird, gelten die Vorschriften über den Maßstab der Kreisumlage.

(2) Die Gesamthöhe der Umlage und der von den einzelnen Verbandsmitgliedern zu tragende Anteil sind in der Haushaltssatzung für jedes Haushaltsjahr neu festzulegen.
Die Umlage wird mit jeweils einem Viertel des Gesamtbetrages am 15.
Februar, 15.
Mai, 15.
August und 15.
November des Haushaltsjahres fällig.
Erfolgt die öffentliche Bekanntmachung der Haushaltssatzung erst nach Ablauf eines der in Satz 2 geregelten Fälligkeitstermine, so ist die Umlageteilschuld für den abgelaufenen Fälligkeitstermin innerhalb eines Monats nach der öffentlichen Bekanntmachung zu entrichten.
Der Zweckverband kann die von den einzelnen Mitgliedern zu tragende Umlage durch Bescheid festsetzen und dabei abweichende Fälligkeiten bestimmen.

(3) Der Zweckverband hat zur Deckung seines liquiditätswirksamen Finanzbedarfes Vorauszahlungen bis zur Höhe der nach Absatz 1 voraussichtlich erforderlichen Umlagen zu erheben, wenn die Haushaltssatzung oder die Nachtragssatzung nicht mehr rechtzeitig erlassen werden kann und soweit die Aufnahme eines Kassenkredites unzulässig, unmöglich oder für den Zweckverband unwirtschaftlich ist.
Die Vorauszahlungen sind mit der endgültigen Umlage zu verrechnen.

(4) Gegen eine Forderung auf Zahlung der Umlage oder der Vorauszahlung nach Absatz 3 ist die Aufrechnung nur mit unbestrittenen oder rechtskräftig festgestellten Gegenforderungen zulässig.
Der Zweckverband kann Zahlungsansprüche gegen die Verbandsmitglieder ganz oder teilweise an Dritte abtreten, soweit dies zur Erfüllung rechtlicher Pflichten des Zweckverbandes oder zur Aufrechterhaltung seiner Liquidität erforderlich ist.
Die Absicht der Abtretung ist dem betroffenen Verbandsmitglied und der Kommunalaufsichtsbehörde rechtzeitig anzuzeigen.

(5) Dem Zweckverband steht das Recht zur Erhebung von Steuern nicht zu.

(6) Über das Vermögen des Zweckverbandes findet ein Insolvenzverfahren nicht statt.

#### § 30  
Örtliche Prüfung

Hat der Zweckverband kein eigenes Rechnungsprüfungsamt eingerichtet, obliegt die Prüfung dem Rechnungsprüfungsamt des kommunalen Verbandsmitgliedes, dem die Zuständigkeit für die örtliche Prüfung durch öffentlich-rechtliche Vereinbarung oder durch die Verbandssatzung übertragen wurde.
Der Zweckverband kann sich auch des Rechnungsprüfungsamtes einer anderen Kommune bedienen.
In allen anderen Fällen obliegt die Prüfung dem Rechnungsprüfungsamt des Landkreises oder der kreisfreien Stadt, wo der Zweckverband seinen Sitz hat.
Der Zweckverband trägt die Kosten der Prüfung.

### Abschnitt 4   
Änderungen des Zweckverbandes

#### § 31   
Änderungen der Verbandssatzung

(1) Änderungen der Verbandssatzung bedürfen der Mehrheit der satzungsmäßigen Stimmenzahl der Verbandsversammlung, soweit nicht etwas anderes bestimmt ist.
Die Verbandssatzung kann andere Mehrheiten als die gesetzlichen Bestimmungen und die Notwendigkeit der Zustimmung einzelner oder aller Verbandsmitglieder vorschreiben.
Änderungen der Verbandssatzung können auch durch Neufassung erfolgen.

(2) Einer Mehrheit von zwei Dritteln der satzungsmäßigen Stimmenzahl der Verbandsversammlung bedürfen Änderungen der Regelungen der Verbandssatzung über die Verbandsaufgaben, die Verbandsmitglieder, die Zahl ihrer Stimmen in der Verbandssatzung und den Maßstab, nach dem die Verbandsmitglieder nach § 29 zur Deckung des Finanzbedarfs beizutragen haben, sowie die Aufhebung der Verbandssatzung.
Die Änderung der Verbandsaufgaben bedarf zudem der Einstimmigkeit der kommunalen Verbandsmitglieder.

(3) Für die öffentliche Bekanntmachung der Änderungen gilt § 14 Absatz 1 entsprechend.
Eine öffentliche Bekanntmachung genehmigungsfreier Änderungen erfolgt nicht, wenn die Kommunalaufsichtsbehörde den Beschluss gemäß § 113 der Kommunalverfassung des Landes Brandenburg beanstandet oder einstweilig beanstandet.

(4) Die Änderungen werden am Tag nach der öffentlichen Bekanntmachung wirksam, wenn nicht in der Verbandssatzung oder der Änderungssatzung ein anderer Zeitpunkt bestimmt ist.
Genehmigungspflichtige Änderungen werden frühestens mit Wirksamkeit der Genehmigung wirksam.
Nach der öffentlichen Bekanntmachung können Rechtsfehler nur mit Wirkung für die Zukunft geltend gemacht werden.

(5) Ändert sich der Kreis der Verbandsmitglieder, ohne dass es hierfür eines Beschlusses der Verbandsversammlung bedurfte, ist die Verbandssatzung entsprechend zu ändern.
Absatz 3 gilt entsprechend.
Die Änderung der Verbandssatzung wird insoweit am Tag der Änderung des Kreises der Verbandsmitglieder wirksam.

#### § 32  
Beitritt und Austritt von Verbandsmitgliedern

(1) Der Beitritt setzt einen Antrag bei dem Zweckverband voraus.
In dem Antrag soll erklärt werden, welche Vermögensgegenstände, Verbindlichkeiten und Forderungen mit dem Beitritt auf den Zweckverband übergehen sollen.

(2) Der Austritt eines Verbandsmitgliedes aus dem Zweckverband setzt den Antrag des Verbandsmitgliedes bei dem Zweckverband voraus.
Ist beim Austritt eine Auseinandersetzung notwendig, schließen das austrittswillige Verbandsmitglied, der Zweckverband und soweit erforderlich weitere Beteiligte eine Auseinandersetzungsvereinbarung.
Die Vereinbarung ist der Kommunalaufsichtsbehörde anzuzeigen.

(3) Einigen sich die Beteiligten nach mindestens zwei ernsthaften Einigungsversuchen nicht, entscheidet auf Antrag eines Beteiligten die Kommunalaufsichtsbehörde über die Auseinandersetzung nach pflichtgemäßem Ermessen durch Bescheid.
Der Antrag muss den Austrittsgrund sowie den Stand der Einigungsgespräche mit den offenen Streitpunkten und den von den Beteiligten vorgeschlagenen Lösungen dokumentieren.
Die Kommunalaufsichtsbehörde kann sich sachkundiger Dritter bedienen und die Kostentragung der Beteiligten in dem Bescheid regeln.

(4) Anstelle des Verfahrens nach Absatz 3 können die Beteiligten eine Schiedsvereinbarung schließen oder ein Schiedsverfahren in der Verbandssatzung vorsehen.

(5) Über den Antrag auf Beitritt oder Austritt entscheidet die Verbandsversammlung durch Beschluss über die Änderung der Verbandssatzung.
Für die öffentliche Bekanntmachung und die Wirksamkeit der Änderung gilt § 14 entsprechend.

(6) Erklärt ein Verbandsmitglied eine Kündigung, gilt dies als Antrag nach Absatz 2.
Das kündigende Verbandsmitglied hat einen Anspruch auf Änderung der Verbandssatzung nach Absatz 5, soweit das Recht zur Kündigung aufgrund einer Rechtsvorschrift besteht.

(7) Die Absätze 1 bis 6 gelten auch für einen sachlich oder örtlich begrenzten Beitritt oder Austritt eines Beteiligten, der bereits Verbandsmitglied ist.

#### § 33  
Auflösung und Abwicklung des Zweckverbandes

(1) Die Auflösung des Zweckverbandes erfolgt durch Aufhebung der Verbandssatzung durch die Verbandsversammlung.
§ 14 gilt entsprechend.

(2) Der Zweckverband ist kraft Gesetzes aufgelöst, wenn seine Aufgaben durch ein Gesetz oder aufgrund eines Gesetzes vollständig auf einen anderen Verwaltungsträger übergehen.
Gleiches gilt, wenn dem Zweckverband nur noch ein kommunales Mitglied angehört; in diesem Fall tritt das Mitglied an die Stelle des Zweckverbandes.
Die Kommunalaufsichtsbehörde hat die Auflösung nach § 14 Absatz 1 öffentlich bekannt zu machen.

(3) Wird der Zweckverband nach Absatz 1 aufgelöst, so hat er seine Geschäfte abzuwickeln.
Der Zweckverband gilt bis zum Ende der Abwicklung als fortbestehend, soweit es der Zweck der Abwicklung erfordert.

(4) Abwicklerin ist die Verbandsleitung, wenn nicht die Verbandsversammlung etwas anderes beschließt.

(5) Die Abwicklerin beendet die laufenden Geschäfte und zieht die Forderungen ein.
Um schwebende Geschäfte zu beenden, kann sie auch neue Geschäfte eingehen.
Sie fordert die bekannten Gläubigerinnen und Gläubiger besonders, andere Gläubigerinnen und Gläubiger durch öffentliche Bekanntmachung auf, ihre Ansprüche anzumelden.
Die Abwicklerin kann mit den kommunalen Mitgliedern die Übertragung von öffentlich-rechtlichen Forderungen vereinbaren.

(6) Die Abwicklerin befriedigt die Ansprüche der Gläubigerinnen und Gläubiger.
Im Übrigen ist das Verbandsvermögen nach dem Umlageschlüssel im Zeitpunkt der Auflösung auf die Verbandsmitglieder zu verteilen.
Reicht das Vermögen zur Befriedigung der Gläubigerinnen und Gläubiger nicht aus, ist von den Verbandsmitgliedern eine Umlage nach dem Umlageschlüssel im Zeitpunkt der Auflösung zu erheben.
Die Verbandssatzung oder eine Auseinandersetzungsvereinbarung können einen abweichenden Umlageschlüssel regeln.

(7) Die Bediensteten des Zweckverbandes sind von den kommunalen Verbandsmitgliedern anteilig zu übernehmen, soweit die Beschäftigungsverhältnisse nicht aufgelöst werden.
Dabei ist das Verhältnis der Stimmen der kommunalen Mitglieder in der Verbandsversammlung zueinander maßgeblich.
Die Abwicklerin bestimmt, von welchen Verbandsmitgliedern die einzelnen Bediensteten übernommen werden.
Von Satz 1 bis 3 abweichende Regelungen können in der Verbandssatzung oder in einer Auseinandersetzungsvereinbarung getroffen werden.

(8) Bei einem Zweckverband, der sich überwiegend wirtschaftlich betätigt (§ 91 der Kommunalverfassung des Landes Brandenburg), kann die Verbandssatzung die Abwicklung entsprechend den Regelungen des Handelsrechts vorsehen.

#### § 34  
Rechtsnachfolge bei Verbandsmitgliedern

(1) Fällt ein Verbandsmitglied weg oder verliert es die Aufgabe, die auf den Zweckverband übertragen oder mit deren Durchführung der Zweckverband beauftragt ist, so tritt der Rechtsnachfolger des Verbandsmitgliedes an seine Stelle, soweit nicht durch Gesetz etwas anderes bestimmt ist.
Gleiches gilt bei mehreren Rechtsnachfolgern.

(2) Der Zweckverband kann innerhalb von drei Monaten nach Eintritt der Rechtsnachfolge das Ausscheiden eines neuen Verbandsmitgliedes beschließen, wenn Gründe des öffentlichen Wohls nicht entgegenstehen.
Unter den gleichen Voraussetzungen kann ein neues Verbandsmitglied seine Mitgliedschaft im Zweckverband kündigen.
Der Beschluss oder die Kündigung werden mit Ablauf des auf den Beschluss oder die Kündigung folgenden Kalenderjahres wirksam, soweit zwischen dem Zweckverband und dem neuen Verbandsmitglied nichts anderes vereinbart ist.
Ist eine Auseinandersetzung notwendig, schließen der Zweckverband und das ausscheidende Verbandsmitglied eine Auseinandersetzungsvereinbarung.
Die Vereinbarung ist der Kommunalaufsichtsbehörde anzuzeigen.
§ 32 Absatz 3 und 4 gilt entsprechend.

(3) Das neue Verbandsmitglied kann gegen den Beschluss des Zweckverbandes über das Ausscheiden und der Zweckverband kann gegen die Kündigung Beschwerde mit der Begründung erheben, dass die Voraussetzungen des Absatzes 2 Satz 1 oder Satz 2 nicht vorliegen.
Die Beschwerde ist bei der für den Zweckverband zuständigen Kommunalaufsichtsbehörde innerhalb von drei Monaten nach Kenntnisnahme des Beschlusses oder der Kündigung zu erheben.
Die Beschwerde hat aufschiebende Wirkung.
Die Kommunalaufsichtsbehörde hat über die Beschwerde durch Bescheid zu entscheiden.
In diesem Bescheid kann die Kommunalaufsichtsbehörde auch die Wirksamkeit des Ausscheidens oder der Kündigung regeln.

#### § 35  
Zusammenschluss von Zweckverbänden

(1) Zweckverbände können einen neuen Zweckverband bilden.
Die Neubildung bedarf übereinstimmender Beschlüsse der Verbandsversammlungen.
Die Beschlüsse bedürfen einer Mehrheit von zwei Dritteln der satzungsmäßigen Stimmenzahl der Verbandsversammlungen.
Beteiligte, die gegen die Neubildung gestimmt haben, können die Mitgliedschaft innerhalb von drei Monaten nach Wirksamkeit der Neubildung des Zweckverbandes kündigen; § 34 Absatz 2 und 3 gilt entsprechend.
Der neue Zweckverband ist Rechtsnachfolger der bisherigen Zweckverbände.
Die bisherigen Zweckverbände gelten mit dem Zeitpunkt des Entstehens des neuen Zweckverbandes als aufgelöst.

(2) In den Beschlüssen nach Absatz 1 ist festzulegen, wer die Rechte der Verbandsleitung sowie der oder des Vorsitzenden der Verbandsversammlung des neuen Zweckverbandes bis zu ihrer erstmaligen, unverzüglich durchzuführenden Wahl wahrnimmt.
Zugleich ist die Verbandssatzung des neuen Zweckverbandes festzulegen; § 14 gilt entsprechend.
Für die Vereinbarung weiterer Satzungen gilt § 15 entsprechend.

(3) Ein Zweckverband kann sich mit seinem vollständigen Aufgabenbestand in einen anderen Zweckverband eingliedern.
Der eingegliederte Zweckverband gilt mit dem Zeitpunkt des Wirksamwerdens seiner Eingliederung als aufgelöst.
Der aufnehmende Zweckverband ist Rechtsnachfolger des eingegliederten Zweckverbandes.
Absatz 1 Satz 2 bis 4 sowie Absatz 2 Satz 2 gelten entsprechend.

#### § 36  
Umwandlung einer gemeinsamen kommunalen Anstalt in einen Zweckverband

Gemeinsame kommunale Anstalten können im Wege der Gesamtrechtsnachfolge in einen Zweckverband umgewandelt werden.
Die Umwandlung erfolgt durch Vereinbarung der Verbandssatzung durch die künftigen Mitglieder.
§ 14 gilt entsprechend.

Teil 5   
Die gemeinsame kommunale Anstalt
------------------------------------------

#### § 37   
Errichtung einer gemeinsamen kommunalen Anstalt

(1) Kommunen können zur gemeinsamen Erfüllung öffentlicher Aufgaben in einer gemeinsamen kommunalen Anstalt zusammenarbeiten, um die Anstalt mit der Durchführung einzelner Aufgaben zu beauftragen oder einzelne Aufgaben auf die Anstalt zu übertragen.
Neben Kommunen können auch der Bund, die Länder und andere Körperschaften des öffentlichen Rechts Träger einer gemeinsamen kommunalen Anstalt sein, soweit ihre Stimmen insgesamt die Hälfte der satzungsmäßigen Stimmenzahl im Verwaltungsrat nicht erreichen; § 1 Absatz 4 bleibt unberührt.

(2) Kommunen können durch Vereinbarung

1.  eine gemeinsame kommunale Anstalt errichten,
    
2.  sich an einer bestehenden kommunalen Anstalt einer Kommune als weiterer Träger beteiligen,
    
3.  sich an einer gemeinsamen kommunalen Anstalt als weiterer Träger beteiligen,
    
4.  im Wege der Gesamtrechtsnachfolge durch Umwandlung
    
    1.  eine Gesellschaft in privater Rechtsform oder einen Zweckverband, an denen nur Kommunen beteiligt sind, durch Formwechsel in die Rechtsform einer gemeinsamen kommunalen Anstalt überführen,
        
    2.  bestehende kommunale Anstalten, Gesellschaften in privater Rechtsform oder Zweckverbände, an denen nur Kommunen beteiligt sind, zu einer gemeinsamen kommunalen Anstalt verschmelzen.
        

(3) Die Errichtung einer gemeinsamen kommunalen Anstalt, die Beteiligung an einer bestehenden kommunalen Anstalt in Trägerschaft einer Kommune und die Umwandlung in eine gemeinsame kommunale Anstalt erfolgt durch Vereinbarung einer Anstaltssatzung durch die zukünftigen Träger.
Für die Beteiligung an einer bestehenden gemeinsamen Anstalt als neuer Träger gilt § 32 Absatz 1 und 5 entsprechend.

(4) Für die öffentliche Bekanntmachung der Anstaltssatzung gilt § 14 Absatz 1 entsprechend.
Die gemeinsame kommunale Anstalt entsteht am Tag nach der öffentlichen Bekanntmachung der Anstaltssatzung durch die Kommunalaufsichtsbehörde, soweit in der Anstaltssatzung nicht ein späterer Zeitpunkt bestimmt ist.

#### § 38  
Anwendung von Rechtsvorschriften

(1) Auf gemeinsame kommunale Anstalten sind die Vorschriften der §§ 94 und 95 der Kommunalverfassung des Landes Brandenburg sowie sonstige Rechtsvorschriften, die für die kommunale Anstalt gelten, entsprechend anzuwenden, soweit in diesem Gesetz oder aufgrund dieses Gesetzes nichts anderes bestimmt ist.

(2) Soweit in Rechtsvorschriften der Gemeindeverband als Sammelbegriff verwendet wird, gelten auch gemeinsame kommunale Anstalten als Gemeindeverbände, soweit in diesem Gesetz oder aufgrund dieses Gesetzes nichts anderes bestimmt ist.

#### § 39  
Besondere Vorschriften für die gemeinsame kommunale Anstalt

(1) In der Anstaltssatzung sind unbeschadet der Regelung in § 94 Absatz 2 der Kommunalverfassung des Landes Brandenburg zu bestimmen

1.  die Träger und der Sitz der gemeinsamen kommunalen Anstalt,
    
2.  der örtliche Zuständigkeitsbereich, wenn der gemeinsamen kommunalen Anstalt Aufgaben übertragen werden,
    
3.  die Form der öffentlichen Bekanntmachungen der gemeinsamen kommunalen Anstalt,
    
4.  der Betrag der von jedem Träger auf das Stammkapital zu leistenden Einlage (Stammeinlage),
    
5.  wenn Sacheinlagen geleistet werden sollen, der Gegenstand der Sacheinlage und der Betrag der Stammeinlage, auf die sich die Sacheinlage bezieht,
    
6.  die Sitz- und Stimmenverteilung der Träger im Verwaltungsrat,
    
7.  die Verteilung des Anstaltsvermögens sowie die Übernahme von Beschäftigten im Falle der Auflösung der gemeinsamen kommunalen Anstalt.
    

(2) Die Träger der gemeinsamen kommunalen Anstalt werden im Verwaltungsrat durch ihre Hauptverwaltungsbeamtinnen oder Hauptverwaltungsbeamten vertreten.
Die Anstaltssatzung kann zulassen, dass dem Verwaltungsrat weitere Mitglieder angehören, die von den Vertretungskörperschaften der Träger gewählt werden.
Für die Vertretung der Träger der gemeinsamen kommunalen Anstalt im Verwaltungsrat gilt im Übrigen § 95 Absatz 2 Satz 2 der Kommunalverfassung des Landes Brandenburg entsprechend.
Alle Stimmen eines Trägers werden von den anwesenden Verwaltungsratsmitgliedern des Trägers einheitlich abgegeben; eine uneinheitliche Stimmabgabe ist ungültig.
Zeigt die Hauptverwaltungsbeamtin oder der Hauptverwaltungsbeamte eines Trägers oder die stattdessen nach Satz 3 bestimmte Person dem Vorsitzenden des Verwaltungsrates an, dass den Verwaltungsratsmitgliedern dieses Trägers eine Weisung erteilt wurde, gibt eine Stimmführerin oder ein Stimmführer alle Stimmen des Trägers einheitlich ab.
Hat die Vertretungskörperschaft des Trägers keine Stimmführerin oder keinen Stimmführer bestimmt und einigen sich die anwesenden Verwaltungsratsmitglieder des Trägers vor der Stimmabgabe nicht auf eine Stimmführerin oder einen Stimmführer, ist die Hauptverwaltungsbeamtin oder der Hauptverwaltungsbeamte oder die stattdessen nach Satz 3 bestimmte Person Stimmführerin oder Stimmführer.
Bei geheimer Beschlussfassung erfolgt die Stimmabgabe aller Träger durch die nach Satz 6 zur Stimmführerschaft berechtigten Personen.
Der Verwaltungsrat wählt aus seiner Mitte eine Vorsitzende oder einen Vorsitzenden und mindestens eine Stellvertreterin oder einen Stellvertreter.

(3) Der Verwaltungsrat der gemeinsamen kommunalen Anstalt entscheidet über § 95 Absatz 2 der Kommunalverfassung des Landes Brandenburg hinaus über

1.  die Änderung, Neufassung und Aufhebung der Anstaltssatzung,
    
2.  den Beitritt und Austritt von Trägern,
    
3.  die Erhöhung des Stammkapitals sowie
    
4.  die Umwandlung.
    

Entscheidungen nach Satz 1 Nummer 2 bis 4 bedürfen der Zustimmung aller Träger, soweit die Anstaltssatzung keine abweichende Regelung trifft.
Für die Abwicklung der aufzulösenden gemeinsamen kommunalen Anstalt ist der Vorstand zuständig, soweit der Verwaltungsrat keinen abweichenden Beschluss fasst.

(4) Der Vorstand der gemeinsamen kommunalen Anstalt ist hauptamtlich tätig.
Die Anstaltssatzung kann abweichend von Satz 1 bestimmen, dass der Vorstand ehrenamtlich tätig ist, soweit die gemeinsame kommunale Anstalt nur mit der Durchführung von Aufgaben für ihre Träger beauftragt wurde.

(5) Die Träger können in der Anstaltssatzung oder durch schriftliche Vereinbarungen Regelungen über den Ausgleich von Vorteilen und Nachteilen, die sich aus der Errichtung der gemeinsamen kommunalen Anstalt ergeben, sowie Bestimmungen über Ausgleichsleistungen treffen, wenn der gemeinsamen kommunalen Anstalt im Rahmen der Aufgabenerfüllung ein Finanzbedarf entsteht, der nicht durch eigene Erträge, Einzahlungen oder nicht benötigte Finanzmittel gedeckt ist.

(6) Soweit die Träger für die Verbindlichkeiten der gemeinsamen kommunalen Anstalt gegenüber Dritten einzutreten haben, haften sie als Gesamtschuldner.
Der Ausgleich im Innenverhältnis zwischen den Trägern richtet sich nach dem Verhältnis der Stammeinlagen zueinander.
Die Anstaltssatzung kann eine andere Regelung vorsehen.

#### § 40  
Verordnungsermächtigung

Das für Inneres zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung das Verfahren bei der Errichtung, Beteiligung, Umwandlung, dem Austritt und der Auflösung einer gemeinsamen kommunalen Anstalt sowie deren Aufbau, die Verwaltung, die Wirtschaftsführung und das Rechnungswesen sowie die Prüfung der gemeinsamen kommunalen Anstalten zu regeln.

Teil 6   
Anzeige- und Genehmigungspflichten, Aufsicht
------------------------------------------------------

#### § 41   
Anzeige- und Genehmigungspflichten

(1) Vereinbarungen über eine kommunale Zusammenarbeit und sonstige Maßnahmen nach diesem Gesetz sowie deren Änderung, Kündigung und Aufhebung sind genehmigungsfrei, soweit nichts anderes bestimmt ist.

(2) Die Kommunen haben der Kommunalaufsichtsbehörde anzuzeigen, wenn sie nach den Bestimmungen dieses Gesetzes zusammenarbeiten.
Dabei sind die Beteiligten, die Form der Zusammenarbeit und die Aufgaben, die gemeinsam erfüllt werden, anzugeben.
Dies gilt entsprechend für Änderungen oder die Beendigung einer Zusammenarbeit.
Die Zusammenarbeit in der Form der Arbeitsgemeinschaft muss nicht angezeigt werden.

(3) Einer Genehmigung durch die Kommunalaufsichtsbehörde bedürfen

1.  delegierende öffentlich-rechtliche Vereinbarungen, soweit durch sie pflichtige Selbstverwaltungsaufgaben, Pflichtaufgaben zur Erfüllung nach Weisung oder Auftragsangelegenheiten gemäß § 3 Absatz 1 Satz 1 Nummer 2 übertragen werden, sowie deren Aufhebung,
    
2.  Änderungen und Kündigungen einer delegierenden öffentlich-rechtlichen Vereinbarung nach Nummer 1, soweit der Kreis der Beteiligten oder der Bestand der pflichtigen Selbstverwaltungsaufgaben, Pflichtaufgaben zur Erfüllung nach Weisung oder Auftragsangelegenheiten verändert wird,
    
3.  Vereinbarungen über die Verbands- oder Anstaltssatzung sowie die Aufhebung dieser Satzungen,
    
4.  Änderungen einer Verbands- oder Anstaltssatzung, soweit der Kreis der Beteiligten oder der Bestand der gemäß § 3 Absatz 1 Satz 1 Nummer 2 übertragenen pflichtigen Selbstverwaltungsaufgaben, Pflichtaufgaben zur Erfüllung nach Weisung oder Auftragsangelegenheiten geändert wird.
    

(4) Im Genehmigungsverfahren sind andere Behörden zu beteiligen, soweit deren Belange berührt sind.

(5) Die Genehmigung nach Absatz 3 ist zu erteilen, wenn die Vereinbarung nicht gegen Rechtsvorschriften verstößt.
Abweichend davon besteht kein Rechtsanspruch auf Genehmigung, wenn für die Übertragung oder die Durchführung der Aufgabe eine besondere Genehmigung erforderlich ist und zu erwarten ist, dass die besondere Genehmigung versagt wird.

(6) Beantragen die Beteiligten eine Genehmigung und entscheidet die Kommunalaufsichtsbehörde, dass eine Genehmigungspflicht nicht besteht, gilt die Vereinbarung oder Maßnahme auch gegenüber Dritten als genehmigungsfrei, wenn in der öffentlichen Bekanntmachung der Vereinbarung oder Maßnahme auf die Entscheidung hingewiesen wird.

#### § 42  
Aufsicht

(1) Für die Aufsicht gelten die §§ 108 und 109, 110 Absatz 3 und 4 sowie die §§ 111 bis 121 der Kommunalverfassung des Landes Brandenburg entsprechend.

(2) Kommunalaufsichtsbehörde ist die Landrätin oder der Landrat als allgemeine untere Landesbehörde.
Dies gilt auch bei Beteiligung Dritter nach § 4 Absatz 1, § 6 Absatz 1 oder § 11 Absatz 1.

(3) Arbeiten Kommunen aus mehreren Landkreisen aufgrund einer öffentlich-rechtlichen Vereinbarung zusammen, ist die Landrätin oder der Landrat des Landkreises zuständig, in dem die Kommune ihren Sitz hat, die mit der Durchführung der Aufgabe beauftragt wird oder auf die die Aufgabe übertragen wird.
Sind danach mehrere Kommunalaufsichtsbehörden zuständig, bestimmt die oberste Kommunalaufsichtsbehörde die Zuständigkeit.

(4) Arbeiten Kommunen aus mehreren Landkreisen in einem Zweckverband oder einer gemeinsamen kommunalen Anstalt zusammen, ist die Landrätin oder der Landrat des Landkreises zuständig, in dem der Zweckverband oder die gemeinsame kommunale Anstalt den Sitz hat.

(5) Das für Inneres zuständige Ministerium ist Kommunalaufsichtsbehörde, wenn eine Kommune beteiligt ist, die nicht der Kommunalaufsicht einer Landrätin oder eines Landrates untersteht.
Es kann die Zuständigkeit nach Anhörung der Beteiligten auf eine Landrätin oder einen Landrat übertragen.

(6) Oberste Kommunalaufsichtsbehörde ist das für Inneres zuständige Ministerium.

(7) Die Aufgabe der Kommunalaufsichtsbehörden, im öffentlichen Interesse sicherzustellen, dass die Verwaltung der beteiligten Kommunen im Einklang mit den Gesetzen erfolgt, bleibt unberührt.
Die Zuständigkeiten und Befugnisse der besonderen Rechts-, Sonder- und Fachaufsichtsbehörden bleiben ebenfalls unberührt.

#### § 43  
Anordnung der kommunalen Zusammenarbeit

(1) Die Kommunalaufsichtsbehörde kann Kommunen zur Zusammenarbeit bei einzelnen pflichtigen Selbstverwaltungsaufgaben, Pflichtaufgaben zur Erfüllung nach Weisung und Auftragsangelegenheiten verpflichten und dazu erforderliche Maßnahmen anordnen, soweit dies aus überwiegenden Gründen des öffentlichen Wohls erforderlich ist.
Gründe des öffentlichen Wohls liegen insbesondere dann vor, wenn die ordnungsgemäße Aufgabenerfüllung durch die mangelnde Leistungsfähigkeit einer der betroffenen Kommunen gefährdet oder aus sonstigen Gründen nicht dauerhaft gesichert ist.

(2) Die Kommunalaufsichtsbehörde kann insbesondere anordnen,

1.  eine andere Kommune mit der Durchführung einzelner Aufgaben durch mandatierende öffentlich-rechtliche Vereinbarung zu beauftragen,
    
2.  einzelne Aufgaben von einer Kommune auf eine andere Kommune durch delegierende öffentlich-rechtliche Vereinbarung zu übertragen,
    
3.  einem Zweckverband beizutreten oder sich an einer gemeinsamen kommunalen Anstalt zu beteiligen,
    
4.  mit anderen Kommunen einen Zweckverband zu bilden oder eine gemeinsame kommunale Anstalt zu errichten,
    
5.  mehrere Zweckverbände zu einem Zweckverband zusammenzuschließen,
    
6.  einen Zweckverband in einen anderen Zweckverband einzugliedern,
    
7.  kommunale Anstalten oder gemeinsame kommunale Anstalten zu einer gemeinsamen kommunalen Anstalt umzuwandeln.
    

(3) Darüber hinaus kann die Kommunalaufsichtsbehörde die zur Umsetzung von Maßnahmen nach den Absätzen 1 und 2 erforderlichen weiteren Anordnungen treffen.
Hierzu kann sie insbesondere anordnen,

1.  eine öffentlich-rechtliche Vereinbarung oder andere Verträge zu kündigen oder aufzuheben,
    
2.  einen Zweckverband oder eine gemeinsame kommunale Anstalt aufzulösen,
    
3.  aus einem Zweckverband oder einer gemeinsamen kommunalen Anstalt auszutreten oder die Mitgliedschaft oder Trägerschaft zu kündigen.
    

Zudem kann die Kommunalaufsichtsbehörde die Abgabe von durch Rechtsvorschriften vorgesehenen Erklärungen durch andere Kommunen anordnen, soweit dies zur Umsetzung ihrer Anordnungen oder der Anordnungen einer anderen Kommunalaufsichtsbehörde erforderlich ist.

(4) Die Anordnung der kommunalen Zusammenarbeit kann befristet oder räumlich beschränkt werden.

(5) Die Kommunalaufsichtsbehörde hört die betroffenen Kommunen schriftlich an.
Anschließend sind die beabsichtigten Maßnahmen mündlich zu erörtern.
Zu dem Erörterungstermin ist mindestens einen Monat vorher einzuladen.
Den Beteiligten ist vor einer Anordnung nach den Absätzen 1 bis 3 die Gelegenheit zu geben, in einer angemessenen Frist die beabsichtigten Maßnahmen freiwillig umzusetzen.

(6) Für die Ersatzvornahme gilt § 116 der Kommunalverfassung des Landes Brandenburg.

(7) Sind die Gründe für die Anordnung einer kommunalen Zusammenarbeit weggefallen, können die beteiligten Kommunen die kommunale Zusammenarbeit mit einer Frist von sechs Monaten kündigen.

(8) Für die Aufstellung oder Durchführung von Bauleitplänen kann die kommunale Zusammenarbeit nicht angeordnet werden.

#### § 44  
Schlichtung von Streitigkeiten

Bei Streitigkeiten über Rechte und Pflichten aus Vereinbarungen über eine kommunale Zusammenarbeit oder aus Verbands- oder Anstaltssatzungen nach diesem Gesetz kann vor Anrufung eines Gerichts die Kommunalaufsichtsbehörde zur Schlichtung unter Darlegung des Sach- und Streitstandes angerufen werden, soweit nicht in der Vereinbarung oder der Satzung ein besonderes Schiedsverfahren vorgesehen ist.
Die Kommunalaufsichtsbehörde unterbreitet einen Vorschlag zur Beilegung der Streitigkeit; die kommunalaufsichtsbehördlichen Befugnisse bleiben unberührt.
Die Kommunalaufsichtsbehörde kann sich sachkundiger Dritter bedienen und dadurch entstandene Kosten den Beteiligten durch Bescheid auferlegen.

Teil 7  
Übergangs- und Schlussvorschriften
-------------------------------------------

#### § 45   
Übergangsvorschriften

(1) Bisheriges Recht im Sinne dieser Vorschrift ist das Gesetz über kommunale Gemeinschaftsarbeit im Land Brandenburg in der Fassung der Bekanntmachung vom 28.
Mai 1999 (GVBl.
I S. 194), das zuletzt durch Artikel 3 des Gesetzes vom 16.
Mai 2013 (GVBl.
I Nr. 18 S. 17) geändert worden ist.

(2) Die Rechtmäßigkeit von Verbandssatzungen und anderen Satzungen, öffentlich-rechtlichen Vereinbarungen, Beschlüssen und sonstigen Maßnahmen, die vor Inkrafttreten dieses Gesetzes wirksam geworden sind, wird durch dieses Gesetz nicht berührt.
Verweisen Regelungen nach Satz 1 auf das bisherige Recht, gilt das bisherige Recht insoweit fort.

(3) § 8 Absatz 1 und 3 gilt auch für die öffentliche Bekanntmachung der vor Inkrafttreten dieses Gesetzes abgeschlossenen öffentlich-rechtlichen Vereinbarungen, die vor Inkrafttreten dieses Gesetzes noch nicht öffentlich bekannt gemacht worden sind, sowie für die öffentliche Bekanntmachung der Änderung, Aufhebung oder Kündigung öffentlich-rechtlicher Vereinbarungen, die vor Inkrafttreten dieses Gesetzes öffentlich bekannt gemacht worden sind.
§ 8 Absatz 4 gilt nicht für Satzungen und Verordnungen, die vor Inkrafttreten dieses Gesetzes öffentlich bekannt gemacht worden sind.

(4) § 9 gilt auch für die Wirksamkeit der vor Inkrafttreten dieses Gesetzes abgeschlossenen öffentlich-rechtlichen Vereinbarungen, die vor Inkrafttreten dieses Gesetzes noch nicht wirksam geworden sind, sowie für die Wirksamkeit der Änderung, Aufhebung oder Kündigung der vor Inkrafttreten dieses Gesetzes wirksam gewordenen öffentlich-rechtlichen Vereinbarungen.
Die Wirksamkeit nach Satz 1 beginnt frühestens mit Inkrafttreten dieses Gesetzes.
Macht eine öffentlich-rechtliche Vereinbarung, die vor Inkrafttreten dieses Gesetzes abgeschlossen, aber noch nicht nach bisherigem Recht öffentlich bekannt gemacht worden ist, die Wirksamkeit von der öffentlichen Bekanntmachung im Veröffentlichungsblatt der Aufsichtsbehörde abhängig, tritt an diese Stelle die letzte öffentliche Bekanntmachung nach § 8 Absatz 1.

(5) Bestehende Beteiligungen oder Mitgliedschaften der Kommunen an Unternehmen und Verbänden, die der gleichen oder einer ähnlichen Aufgabe dienen wie ein Zweckverband, der vor Inkrafttreten dieses Gesetzes gebildet wurde, bleiben unberührt.
Hat ein solcher Zweckverband nach der Verbandssatzung anzustreben, solche Beteiligungen oder Mitgliedschaften an Stelle seiner Verbandsmitglieder zu übernehmen, so sind die einzelnen Verbandsmitglieder zu den entsprechenden Rechtsgeschäften und Verwaltungsmaßnahmen verpflichtet.

(6) Die Regelungen des § 29 über die Verbandsumlage gelten erstmals für das erste Haushaltsjahr nach Inkrafttreten dieses Gesetzes.
Bis dahin gilt das bisherige Recht.

(7) § 41 gilt auch für Maßnahmen, die nach bisherigem Recht genehmigungspflichtig waren, aber noch nicht genehmigt worden sind.

#### § 46  
Rechtsfehler bei der öffentlich-rechtlichen Vereinbarung

(1) Ist in einer delegierenden öffentlich-rechtlichen Vereinbarung die Bestimmung unwirksam, dass die Befugnis, in Bezug auf die übertragene Aufgabe Satzungen und Verordnungen zu erlassen, bei der übertragenden Kommune verbleibt, so gilt von Anfang an als vereinbart, dass die Kommune zum Satzungs- oder Verordnungserlass befugt ist, die die Satzung oder Verordnung tatsächlich erlassen hat.

(2) Fehlt in einer öffentlich-rechtlichen Vereinbarung, die unbefristet oder über mehr als zwanzig Jahre geschlossen ist, eine wirksame Regelung über die Voraussetzungen, unter denen sie durch einen einzelnen Beteiligten gekündigt oder durch alle Beteiligten aufgelöst werden kann, so gilt von Anfang an als vereinbart, dass jeder Beteiligte zum Ende des auf die Kündigung folgenden Kalenderjahres kündigen kann.

#### § 47  
Rechtsfehler beim Beitritt in einen Zweckverband und bei der Verbandssatzung

(1) Fehlende oder nicht feststellbare Beschlüsse der Vertretungskörperschaft zum Verbandsbeitritt, fehlende oder nicht feststellbare Anträge von beitretenden Kommunen sowie fehlende oder nicht feststellbare Satzungsänderungsbeschlüsse der Verbandsversammlung zum Beitritt sind von Anfang an unbeachtlich, wenn die Beteiligten den Beitritt tatsächlich vollzogen haben.
Der Beitritt gilt als vollzogen, wenn die Kommunen als Verbandsmitglied aufgetreten sind.
Dies ist insbesondere der Fall, wenn Vertreterinnen und Vertreter mit Kenntnis der Vertretungskörperschaft für die Kommune mehr als einmal an den Sitzungen der Verbandsversammlung teilgenommen und sich an den Beschlussfassungen beteiligt haben.

(2) Weist die Verbandssatzung eines Zweckverbandes einzelne Bestimmungen, die nach § 13 Absatz 2 zum notwendigen Satzungsinhalt gehören, nicht auf, steht dies nach Maßgabe der nachfolgenden Absätze der Wirksamkeit der Verbandssatzung von Anfang an nicht entgegen.

(3) Fehlt in der Verbandssatzung die Bestimmung der Verbandsmitglieder oder ist diese nicht vollständig, gilt als Verbandsmitglied, wer tatsächlich als Verbandsmitglied aufgetreten ist.
Kommunen sind insbesondere dann als Verbandsmitglieder aufgetreten, wenn Vertreterinnen oder Vertreter mit Kenntnis der Vertretungskörperschaft für die Kommune mehr als einmal an den Sitzungen der Verbandsversammlung teilgenommen und sich an den Beschlussfassungen beteiligt haben.

(4) Fehlen in der Verbandssatzung wirksame Regelungen zu den Aufgaben des Zweckverbandes, gelten die ausschließlich von dem Zweckverband wahrgenommenen Aufgaben als vereinbart.

(5) Fehlt in der Verbandssatzung eine wirksame Regelung zum Namen des Zweckverbandes, gilt der von dem Zweckverband im Rechtsverkehr verwendete Name als vereinbart.
Fehlt in der Verbandssatzung eine wirksame Regelung zum Sitz des Zweckverbandes, gilt der Ort als vereinbarter Verbandssitz, an dem der Zweckverband seine Verwaltung oder die Geschäftsstelle unterhält.
Ist der Sitz nach Satz 2 nicht bestimmbar, gilt der Ort als vereinbarter Verbandssitz, der in der Anschrift des Zweckverbandes angegeben wird.

(6) Fehlt in der Verbandssatzung ein wirksamer Umlagemaßstab, gilt der Umlagemaßstab als vereinbart, nach dem die Verbandsmitglieder seit Aufnahme der Verbandstätigkeit einvernehmlich zur Deckung des Finanzbedarfs beigetragen haben.
Ist ein einheitlicher Umlagemaßstab nach Satz 1 nicht bestimmbar, bemisst sich die Verbandsumlage nach der Stimmenzahl der Verbandsmitglieder in der Verbandsversammlung.

(7) Fehlt in der Verbandssatzung eine wirksame Regelung zur Form der öffentlichen Bekanntmachungen, gilt die von dem Zweckverband verwendete Bekanntmachungsform als vereinbart, wenn sich die Betroffenen aufgrund der verwendeten Bekanntmachungsform in zumutbarer Weise verlässlich Kenntnis von dem Bekanntmachungsinhalt verschaffen können.
Ist eine einheitliche wirksame Bekanntmachungsform danach nicht feststellbar, gilt die öffentliche Bekanntmachung in der Form nach § 14 Absatz 1 Satz 1 oder im Fall des § 42 Absatz 5 Satz 1 in der Form nach § 14 Absatz 1 Satz 2 als vereinbart.

#### § 48  
Rechtsfehler beim Ausscheiden aus einem Zweckverband

(1) Verstöße gegen die Vertretungsberechtigung sowie Form- und Verfahrensfehler beim Ausscheiden von Verbandsmitgliedern gelten als von Anfang an unbeachtlich, wenn

1.  die Verbandsversammlung auf Antrag des ausscheidenden Mitgliedes mit der erforderlichen Mehrheit eine Änderung der Verbandssatzung beschlossen hat, die das Ausscheiden des Verbandsmitgliedes vorsieht,
    
2.  die Änderung der Verbandssatzung wirksam öffentlich bekannt gemacht worden ist und
    
3.  das ausscheidende Verbandsmitglied und der Zweckverband das Ausscheiden tatsächlich vollzogen haben.
    

Das Ausscheiden gilt insbesondere als vollzogen, wenn das ausscheidende Verbandsmitglied als Aufgabenträger hinsichtlich der Aufgaben, die auf den Zweckverband übertragen waren, aufgetreten ist.

(2) Hat die Verbandsversammlung auf Antrag eines Verbandsmitgliedes entgegen § 32 Absatz 5 einen Beschluss über das Ausscheiden dieses Mitgliedes mit der für Satzungsänderungen nach § 31 Absatz 2 Satz 1 erforderlichen Mehrheit gefasst, ohne eine Änderung der Verbandssatzung zu beschließen, so wird die Änderung der Verbandssatzung rückwirkend zum ersten Tag des übernächsten auf die Beschlussfassung der Verbandsversammlung über das Ausscheiden folgenden Monats wirksam, soweit nicht in der Beschlussfassung ein späterer Zeitpunkt für das Ausscheiden bestimmt gewesen ist.

#### § 49  
Rechtsfehler bei der gemeinsamen kommunalen Anstalt

Die §§ 47 und 48 gelten für die gemeinsame kommunale Anstalt entsprechend.
Dabei entsprechen

1.  der Zweckverband der gemeinsamen kommunalen Anstalt,
    
2.  das Verbandsmitglied dem Träger,
    
3.  die Verbandsversammlung dem Verwaltungsrat und
    
4.  die Verbandssatzung der Anstaltssatzung.
    

#### § 50  
Planungsverbände

Auf Planungsverbände nach § 205 des Baugesetzbuches sind die Vorschriften dieses Gesetzes entsprechend anzuwenden, soweit sich aus dem Baugesetzbuch nichts anderes ergibt.

#### § 51  
Experimentierklausel

Das für Inneres zuständige Ministerium kann zur Weiterentwicklung der kommunalen Gemeinschaftsarbeit im Einzelfall auf Antrag zeitlich befristete Ausnahmen von den §§ 4, 6 bis 9, 11 bis 36, 37 Absatz 2 bis 4, §§ 38 und 39 zulassen.