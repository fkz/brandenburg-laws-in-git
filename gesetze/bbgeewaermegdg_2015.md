## Gesetz zur Durchführung des Erneuerbare-Energien-Wärmegesetzes im Land Brandenburg (BbgEEWärmeGDG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Zuständigkeiten

(1) Zuständige Behörden für die Durchführung des Erneuerbare-Energien-Wärmegesetzes vom 7. August 2008 (BGBl.
I S. 1658), das zuletzt durch Artikel 14 des Gesetzes vom 21. Juli 2014 (BGBl. I S. 1066) geändert worden ist, sind die Landkreise und kreisfreien Städte.
Sie nehmen die Aufgaben der

1.  Erteilung von Befreiungen nach § 9 Absatz 1 Nummer 2 des Erneuerbare-Energien-Wärmegesetzes,
2.  Entgegennahme der Nachweise nach § 10 Absatz 1 Satz 1 Nummer 2 und Absatz 3 Satz 1 Nummer 1 des Erneuerbare-Energien-Wärmegesetzes,
3.  Entgegennahme der Abrechnungen nach § 10 Absatz 2 des Erneuerbare-Energien-Wärmegesetzes,
4.  Entgegennahme der Anzeige gemäß § 10 Absatz 4 des Erneuerbare-Energien-Wärmegesetzes,
5.  Überprüfungen nach § 11 Absatz 1 des Erneuerbare-Energien-Wärmegesetzes in Verbindung mit Absatz 2 und § 2 Absatz 1,
6.  Verfolgung und Ahndung von Ordnungswidrigkeiten nach § 17 des Erneuerbare-Energien-Wärmegesetzes und
7.  Berichterstattung zum Vollzug nach § 18a Satz 1 Nummer 3 des Erneuerbare-Energien-Wärmegesetzes gegenüber dem für das Bauwesen zuständige Ministerium

wahr.
In Zustimmungsverfahren nach § 72 Absatz 1 der Brandenburgischen Bauordnung nimmt die oberste Bauaufsichtsbehörde die Aufgaben nach Satz 2 wahr.

(2) Abweichend von den Regelungen des Erneuerbare-Energien-Wärmegesetzes erfolgt die Überprüfung der Erfüllung der Pflichten nach § 3 Absatz 1 und § 10 Absatz 1 Satz 1 Nummer 2 des Erneuerbare-Energien-Wärmegesetzes durch Sachkundige im Sinne des § 2 Absatz 2 Nummer 7 des Erneuerbare-Energien-Wärmegesetzes (Sachkundige).

(3) Den Bericht des Landes an die Bundesregierung gemäß § 18a des Erneuerbare-Energien-Wärmegesetzes erstellt das für das Bauwesen zuständige Ministerium.

#### § 2 Nachweise und Überprüfungen

(1) Die Verpflichteten im Sinne des § 3 Absatz 1 des Erneuerbare-Energien-Wärmegesetzes (Verpflichtete) haben die Erfüllung der Verpflichtung nach § 10 Absatz 1 Satz 1 Nummer 2 des Erneuerbare-Energien-Wärmegesetzes durch Sachkundige überprüfen und sich die Erfüllung formlos bestätigen zu lassen.
Als Bestätigung genügen die entsprechenden Angaben im Energieausweis gemäß § 16 der Energieeinsparverordnung vom 24. Juli 2007 (BGBl.
I S. 1519), die zuletzt durch Artikel 326 der Verordnung vom 31. August 2015 (BGBl.
I S. 1474) geändert worden ist.
Die Verpflichteten haben die Bestätigung den Landkreisen und kreisfreien Städten innerhalb von drei Monaten nach Inbetriebnahme der Heizungsanlage vorzulegen.

(2) Bei Anträgen auf Befreiung von der Nutzungspflicht nach § 9 Absatz 1 Nummer 2 des Erneuerbare-Energien-Wärmegesetzes sind durch die Verpflichteten die besonderen Umstände sowie die Art und Höhe des notwendigen Aufwandes der Nutzungsverpflichtung oder sonstige Gründe, die zu einer unbilligen Härte führen, zu begründen.
Ist der Antrag unvollständig oder weist er sonstige erhebliche Mängel auf, fordern die Landkreise und kreisfreien Städte den Verpflichteten mit der Eingangsbestätigung zur Behebung der Mängel innerhalb einer angemessenen Frist auf.
Werden die Mängel nicht innerhalb der Frist behoben, gilt der Antrag als zurückgenommen.

(3) Über Ausnahmen nach § 9 Absatz 1 Nummer 2 des Erneuerbare-Energien-Wärmegesetzes haben die Landkreise und kreisfreien Städte innerhalb von drei Monaten nach Antragstellung zu entscheiden.
Hat die Behörde nicht fristgemäß über den vollständigen Antrag entschieden, gilt die Befreiung als erteilt.

#### § 3 Pflichtaufgaben zur Erfüllung nach Weisung, Aufsicht

(1) Die Behörden nach § 1 Absatz 1 erfüllen ihre Aufgaben als Pflichtaufgaben zur Erfüllung nach Weisung.

(2) Sonderaufsichtsbehörde ist das für das Bauwesen zuständige Ministerium.

#### § 4 Verwaltungskosten

(1) Für die Befreiung von der Nutzungspflicht gemäß § 9 Absatz 1 Nummer 2 des Erneuerbaren-Energien-Wärmegesetzes und für die Verfolgung von Ordnungswidrigkeiten gemäß § 17 des Erneuerbare-Energien-Wärmegesetzes haben die zuständigen Behörden Gebühren zu erheben.
Das für das Bauwesen zuständige Ministerium wird ermächtigt, auf Grundlage von § 3 Absatz 1 des Gebührengesetzes für das Land Brandenburg vom 7. Juli 2009 (GVBl.
I S. 246), das durch Artikel 5 des Gesetzes vom 10. Juli 2014 (GVBl.
I Nr. 32 S. 27) geändert worden ist, eine Verordnung zur Bestimmung der Gebühren nach Satz 1 zu erlassen.

(2) Die sonstigen, den Vollzugsbehörden durch die Aufgabenübertragung entstehenden Kosten, soweit sie nicht über Gebühren abgedeckt werden können, werden den zuständigen Behörden durch eine pauschale zweckgebundene Zuweisung erstattet.

#### § 5 Inkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 4.
Dezember 2015

Die Präsidentin  
des Landtages Brandenburg

Britta Stark