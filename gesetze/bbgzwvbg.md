## Gesetz über das Verbot der Zweckentfremdung von Wohnraum in Brandenburg (Brandenburgisches Zweckentfremdungsverbotsgesetz - BbgZwVbG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Zweckentfremdungsverbotssatzung

(1) Gemeinden, in denen die ausreichende Versorgung der Bevölkerung mit Wohnraum zu angemessenen Bedingungen besonders gefährdet ist, können durch Satzung mit einer Geltungsdauer von höchstens fünf Jahren bestimmen, dass Wohnraum nur mit ihrer Genehmigung anderen als Wohnzwecken zugeführt werden darf.
Voraussetzung ist, dass sie dem Wohnraummangel nicht auf andere Weise mit zumutbaren Mitteln und in angemessener Zeit abhelfen können.
Eine Zweckentfremdung im Sinne dieses Gesetzes liegt vor, wenn Wohnraum zu anderen als Wohnzwecken genutzt wird, insbesondere wenn Wohnraum

1.  zu mehr als 50 Prozent der Gesamtfläche für gewerbliche oder berufliche Zwecke verwendet oder überlassen wird,
2.  mehr als insgesamt acht Wochen im Kalenderjahr für Zwecke der Fremdenbeherbergung, insbesondere zu einer gewerblichen Zimmervermietung oder der Einrichtung von Schlafstellen, genutzt wird,
3.  länger als sechs Monate leer steht,
4.  baulich derart verändert oder in einer Weise genutzt wird, dass er für Wohnzwecke nicht mehr geeignet ist, oder
5.  beseitigt wird.

(2) Einer Genehmigung bedarf es insbesondere nicht, wenn

1.  Wohnraum bereits zum Zeitpunkt des Inkrafttretens einer Satzung nach Absatz 1 zu mehr als 50 Prozent für gewerbliche oder berufliche Zwecke gemäß Absatz 1 Satz 3 Nummer 1 genutzt wird; dies gilt jedoch nur, solange das Nutzungsverhältnis besteht oder ein zu den genannten Zwecken in den Räumlichkeiten eingerichteter und ausgeübter gewerblicher oder freiberuflicher Betrieb fortgeführt wird,
2.  Wohnraum bereits zum Zeitpunkt des Inkrafttretens einer Satzung nach Absatz 1 in mehr als insgesamt acht Wochen im Kalenderjahr für Zwecke der Fremdenbeherbergung gemäß Absatz 1 Satz 3 Nummer 2 genutzt wird und der Verfügungsberechtigte dies innerhalb von drei Monaten nach Inkrafttreten der Satzung der Gemeinde anzeigt; dies gilt jedoch nur für eine Dauer von zwei Jahren nach Inkrafttreten der Satzung oder
3.  Wohnraum länger als sechs Monate leer steht, weil er trotz geeigneter Bemühungen während dieser Zeit nicht vermietet werden konnte.

Die Satzung kann weitere Ausnahmen von der Genehmigungspflicht zulassen.

(3) Wohnraum im Sinne dieses Gesetzes ist umbauter Raum, der tatsächlich und rechtlich zur dauernden Wohnnutzung geeignet ist.
Ausgenommen sind Räumlichkeiten, die zu anderen Zwecken als zu Wohnzwecken errichtet worden sind und zum Zeitpunkt des Inkrafttretens einer Satzung nach Absatz 1 auch entsprechend genutzt werden.
Wohnraum können Wohnungen oder einzelne Wohnräume sein.

#### § 2 Genehmigung

(1) Die Genehmigung

1.  ist zu erteilen, wenn vorrangige öffentliche Interessen oder schutzwürdige private Interessen das Interesse an der Erhaltung des Wohnraums überwiegen,
2.  kann erteilt werden, wenn dem Interesse an der Erhaltung des Wohnraums durch Ausgleichsmaßnahmen in verlässlicher und angemessener Weise Rechnung getragen wird; dies kann durch Bereitstellung von Ersatzwohnraum oder durch eine Ausgleichszahlung geschehen.

(2) Schutzwürdige private Interessen im Sinne des Absatzes 1 Nummer 1 sind im Regelfall auch dann gegeben, wenn die dinglich Verfügungsberechtigten oder die Besitzer ihre Hauptwohnung, in der der tatsächliche Lebensmittelpunkt begründet wird, während ihrer Abwesenheitszeiten zu anderen als Wohnzwecken verwenden und der Charakter als Hauptwohnung nicht angetastet wird.

(3) Schutzwürdige private Interessen im Sinne des Absatzes 1 Nummer 1 sind auch dann gegeben, wenn sich im Vertrauen auf die bisherige Rechtslage getätigte Investitionen in einer anderen Nutzung noch nicht substanziell amortisiert haben.

(4) Die Genehmigung kann befristet und mit Auflagen erteilt werden.

(5) Die Genehmigung wirkt für und gegen den Rechtsnachfolger.
Das Gleiche gilt für Personen, die den Besitz nach Erteilung der Genehmigung erlangt haben.

#### § 3 Rückführung von Wohnraum

Die Gemeinde kann anordnen, dass eine nicht genehmigungsfähige Zweckentfremdung beendet und der Wohnraum wieder Wohnzwecken zugeführt wird.

#### § 4 Verpflichtung zur Auskunft und zur Duldung des Betretens

(1) Die dinglich Verfügungsberechtigten und die Besitzer von Wohnraum haben der Gemeinde die Auskünfte zu geben und die Unterlagen vorzulegen, die erforderlich sind, um die Einhaltung der Vorschriften dieses Gesetzes zu überwachen.
Wenn eine Erhebung der Daten bei den in Satz 1 genannten Personen nicht oder nicht vollständig möglich ist oder einen unverhältnismäßig hohen Aufwand erfordern würde, haben Verwalter und Vermittler von Wohnraum die Auskünfte zu geben und die Unterlagen vorzulegen.
Satz 2 gilt auch für Diensteanbieter im Sinne von § 2 Satz 1 Nummer 1 des Telemediengesetzes vom 26.
Februar 2007 (BGBl. I S. 179), das zuletzt durch Artikel 1 des Gesetzes vom 28.
September 2017 (BGBl. I S. 3530) geändert worden ist.

(2) Die in Absatz 1 Satz 1 genannten Personen haben den von der Gemeinde beauftragten Personen zu ermöglichen, zu angemessener Tageszeit Grundstücke, Gebäude, Wohnungen und Wohnräume zu betreten, soweit dies zur Überwachung der Einhaltung der Vorschriften dieses Gesetzes erforderlich ist, insbesondere die Einholung von Auskünften nach Absatz 1 hierfür nicht ausreicht.

#### § 5 Sofortvollzug

Widersprüche und Anfechtungsklagen gegen Verwaltungsakte zum Vollzug dieses Gesetzes haben keine aufschiebende Wirkung.

#### § 6 Einschränkung von Grundrechten

Durch dieses Gesetz werden das Grundrecht der Unverletzlichkeit der Wohnung (Artikel 13 des Grundgesetzes, Artikel 15 der Verfassung des Landes Brandenburg), das Grundrecht auf Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) sowie das Grundrecht auf Datenschutz (Artikel 11 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 7 Ordnungswidrigkeiten

(1) Ordnungswidrig handelt, wer vorsätzlich oder fahrlässig

1.  ohne erforderliche Genehmigung nach § 2 Wohnraum anderen als Wohnzwecken zuführt,
2.  einer unanfechtbaren Anordnung nach § 3 nicht oder nicht fristgemäß nachkommt oder
3.  entgegen § 4 Absatz 1 Auskünfte nicht, nicht richtig oder nicht vollständig erteilt oder Unterlagen nicht oder nicht vollständig oder unrichtige Unterlagen vorlegt.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu 100 000 Euro geahndet werden.

#### § 8 Evaluierung

Acht Jahre nach Inkrafttreten dieses Gesetzes berichtet die Landesregierung dem Landtag über die Erfahrungen mit der Anwendung des Gesetzes.

#### § 9 Inkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 5.
Juni 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark