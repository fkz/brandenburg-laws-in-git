## Gesetz zu dem Staatsvertrag über die Einrichtung eines nationalen Mechanismus aller Länder nach Artikel 3 des Fakultativprotokolls vom 18. Dezember 2002 zu dem Übereinkommen der Vereinten Nationen gegen Folter und andere  grausame, unmenschliche oder erniedrigende Behandlung oder Strafe

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem in Dresden am 25.
Juni 2009 vom Land Brandenburg unterzeichneten Staatsvertrag über die Einrichtung eines nationalen Mechanismus aller Länder nach Artikel 3 des Fakultativprotokolls vom 18.
Dezember 2002 zu dem Übereinkommen der Vereinten Nationen gegen Folter und andere grausame, unmenschliche oder erniedrigende Behandlung oder Strafe wird zugestimmt.
Der Vertrag wird nachstehend veröffentlicht.

#### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel 11 Satz 2 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 16.
Februar 2010

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Staatsvertrag](/vertraege/folterstv_2010)