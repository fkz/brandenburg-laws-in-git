## Gesetz zum Dreiundzwanzigsten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Dreiundzwanzigster Rundfunkänderungsstaatsvertrag)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem am 11.
Oktober 2019 vom Land Brandenburg unterzeichneten Dreiundzwanzigsten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Dreiundzwanzigster Rundfunkänderungsstaatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

#### § 2 

Durch dieses Gesetz wird das Grundrecht auf Datenschutz (Artikel 11 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 3 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem Artikel 2 Absatz 2 Satz 1 am 1.
Juni 2020 in Kraft.
Sollte der Staatsvertrag nach seinem Artikel 2 Absatz 2 Satz 2 gegenstandslos werden, ist dies unverzüglich im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 17.
März 2020

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke

* * *

[zum Staatsvertrag](/vertraege/rbstv) - Rundfunkbeitragsstaatsvertrag

* * *

### Anlagen

1

[Dreiundzwanzigster Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Dreiundzwanzigster Rundfunkänderungsstaatsvertrag)](/br2/sixcms/media.php/68/GVBl_I_07_2020-Anlage.pdf "Dreiundzwanzigster Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Dreiundzwanzigster Rundfunkänderungsstaatsvertrag)") 249.6 KB