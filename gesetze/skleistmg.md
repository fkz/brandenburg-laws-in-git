## Gesetz über die Errichtung der Stiftung „Kleist-Museum“ (SKleistMG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Errichtung

(1) Das Land Brandenburg errichtet unter dem Namen Stiftung „Kleist-Museum“ eine rechtsfähige Stiftung des öffentlichen Rechts.

(2) Sitz der Stiftung ist Frankfurt (Oder).

#### § 2 Stiftungszweck

(1) Zweck der Stiftung ist die Pflege der Kunst und Kultur sowie die Förderung der Bildung und Forschung durch den Betrieb des Kleist-Museums.

(2) Die Stiftung hat die Aufgabe, die Zeugnisse des Lebens, Werkes und Wirkens Heinrich von Kleists und seines kulturellen Kontextes zu erhalten, zu sammeln, zu erforschen, zu pflegen, auszuwerten und diesen Kulturbesitz der Öffentlichkeit zugänglich zu machen sowie das literarische Erbe von Ewald Christian und Franz Alexander von Kleist sowie von Friedrich und Caroline de la Motte Fouqué zu pflegen.
Der Stiftungszweck wird insbesondere verwirklicht durch:

1.  die Übernahme und Pflege sowie den Ausbau der bestehenden Sammlungen des Museums, die Bewahrung, Erschließung, Erforschung, Dokumentation und Präsentation der Bestände und Sammlungen in Ausstellungen und Veranstaltungen sowie die digitale Erschließung und Vermittlung der Sammlungsobjekte,
2.  die Unterhaltung und Entwicklung der Dauerausstellung,
3.  die Planung und Realisierung von Sonderausstellungen, Führungen, Veranstaltungen und Bildungsformaten im Sinne des Stiftungszwecks,
4.  die Förderung und Unterstützung der kulturellen Bildung,
5.  die Unterstützung der nationalen und internationalen Forschung sowie die wissenschaftliche und publizistische Aufarbeitung und Dokumentation des Sammlungsbestandes,
6.  die Zusammenarbeit mit regionalen, nationalen und internationalen Museen sowie Kultur- und Forschungsinstitutionen,
7.  die bauliche Unterhaltung der in Anlage 1 aufgeführten Liegenschaft und der Gebäude.

(3) Die Stiftung verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke im Sinne des Abschnitts „Steuerbegünstigte Zwecke“ der Abgabenordnung.

#### § 3 Stiftungsvermögen

(1) Die Stiftung wird mit folgendem Vermögen ausgestattet:

1.  die in Anlage 1 aufgeführte Liegenschaft,
2.  die in Anlage 2 aufgeführte Literatur- und Kunstsammlung; ferner die Besitzrechte an Gegenständen, die durch die jeweiligen Leihgeber der Stiftung zur unentgeltlichen Nutzung überlassen werden,
3.  das in Anlage 3 aufgeführte Inventar.

(2) Mit Inkrafttreten dieses Gesetzes geht das Eigentum an dem in Anlage 1 aufgeführten Vermögen auf die Stiftung entschädigungslos und ergebnisneutral über.
Verpflichtungen, die sich aus dem Eigentum ergeben, gehen ebenfalls auf die Stiftung über.

(3) Der Bund, das Land Brandenburg oder die Stadt Frankfurt (Oder) können der Stiftung weitere Liegenschaften übertragen.

(4) Das Stiftungsvermögen ist in seinem Bestand dauernd und ungeschmälert zu erhalten.

#### § 4 Stiftungsmittel

(1) Zur Erfüllung des Stiftungszwecks erhält die Stiftung jährliche Zuwendungen im Sinne der öffentlichen Haushaltsvorschriften nach Maßgabe der jeweiligen Haushaltspläne des Bundes, des Landes Brandenburg und der Stadt Frankfurt (Oder).
Das Nähere regelt eine Verwaltungsvereinbarung.

(2) Die Stiftung ist berechtigt, Schenkungen, Erbschaften, Zuwendungen und Zustiftungen von dritter Seite zur Erfüllung des Stiftungszwecks anzunehmen.

(3) Das Stiftungsvermögen, die Stiftungsmittel sowie sonstige Einnahmen sind nur zur Erfüllung des Stiftungszwecks zu verwenden.
Die Stiftung ist selbstlos tätig; sie verfolgt nicht in erster Linie eigenwirtschaftliche Zwecke.
Es darf keine Person durch Ausgaben, die dem Zweck der Stiftung fremd sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.

#### § 5 Rechtsaufsicht

Die Rechtsaufsicht über die Stiftung führt die für Kultur zuständige oberste Landesbehörde.

#### § 6 Organe der Stiftung; Fachbeirat

(1) Organe der Stiftung sind:

1.  der Stiftungsrat,
2.  der Vorstand (geschäftsführende Direktorin oder geschäftsführender Direktor).

(2) Zur Beratung der Organe in Belangen des Kleist-Museums wird ein Fachbeirat gebildet.

#### § 7 Zusammensetzung des Stiftungsrates

(1) Dem Stiftungsrat gehören an:

1.  eine Vertreterin oder ein Vertreter der für Kultur zuständigen obersten Landesbehörde, die nicht zugleich mit der Rechtsaufsicht über die Stiftung befasst ist, als Vorsitzende oder Vorsitzender des Stiftungsrates,
2.  eine Vertreterin oder ein Vertreter der für Kultur zuständigen obersten Bundesbehörde als Stellvertreterin oder Stellvertreter der Vorsitzenden oder des Vorsitzenden des Stiftungsrates,
3.  eine Vertreterin oder ein Vertreter der Stadt Frankfurt (Oder) als zweite Stellvertreterin oder zweiter Stellvertreter der Vorsitzenden oder des Vorsitzenden des Stiftungsrates,
4.  eine Vertreterin oder ein Vertreter des Landkreises Oder-Spree,
5.  ein Mitglied des Landtages Brandenburgs.

(2) Das in Absatz 1 Nummer 1 genannte Mitglied wird von der für Kultur zuständigen obersten Landesbehörde entsandt und abberufen.
Das in Absatz 1 Nummer 2 genannte Mitglied wird von der für Kultur zuständigen obersten Bundesbehörde entsandt und abberufen.
Das in Absatz 1 Nummer 3 genannte Mitglied wird von der Stadt Frankfurt (Oder) entsandt und abberufen.
Das in Absatz 1 Nummer 4 genannte Mitglied wird vom Landkreis Oder-Spree entsandt und abberufen.
Das in Absatz 1 Nummer 5 genannte Mitglied wird vom Landtag entsandt und abberufen.
Für den Fall des Ausscheidens ist durch die entsendende Stelle eine Nachfolgerin oder ein Nachfolger unverzüglich zu berufen.

(3) Die in Absatz 1 Nummer 1 und 2 genannten Mitglieder verfügen je über zwei Stimmrechte.

(4) Für jedes Mitglied erhält die entsendende Stelle die Möglichkeit, eine Stellvertreterin oder einen Stellvertreter für den Verhinderungsfall zu berufen.
Sind sowohl das Mitglied und seine Stellvertreterin oder sein Stellvertreter verhindert, kann die entsendende Stelle eine Bevollmächtigte oder einen Bevollmächtigten entsenden.

(5) Die Tätigkeit im Stiftungsrat ist unentgeltlich.
Es besteht Anspruch auf Ersatz der notwendigen Aufwendungen nach Maßgabe der für die Verwaltung des Landes Brandenburg geltenden Bestimmungen des Reisekostenrechts.

(6) Das Nähere regelt die Satzung.

#### § 8 Aufgaben des Stiftungsrates

(1) Der Stiftungsrat erlässt und ändert eine Satzung nach Maßgabe dieses Gesetzes.
Die Satzung sowie Satzungsänderungen bedürfen der Zustimmung der Rechtsaufsichtsbehörde.

(2) Der Stiftungsrat beschließt über die grundsätzlichen Angelegenheiten der Stiftung und legt die wesentlichen Aufgaben und Tätigkeiten der Stiftung fest.
Der Stiftungsrat entscheidet insbesondere über

1.  die Feststellung des Haushaltsplanes und der Finanzplanung,
2.  die Beauftragung der Prüfung des Jahresabschlusses,
3.  die Feststellung des geprüften Jahresabschlusses und die Entlastung des Vorstandes,
4.  die Berufung und Abberufung des Vorstandes sowie dessen Einstellung und Entlassung.

(3) Die Vorsitzende oder der Vorsitzende des Stiftungsrates vertritt die Stiftung gegenüber dem Vorstand.

(4) Der Zustimmung des Stiftungsrates bedürfen

1.  die Einstellung aller Beschäftigten ab Entgeltgruppe 13 des Tarifvertrages für den öffentlichen Dienst der Länder,
2.  der Erwerb, die Veräußerung und die Belastung von Grundstücken,
3.  die Annahme von Erbschaften, Schenkungen, Zuwendungen und Zustiftungen,
4.  die Veräußerung von Sammlungsgegenständen; eine Veräußerung ist nur zulässig, wenn mit dem Veräußerungserlös der Sammlungsbestand ergänzt wird oder als Stiftungsvermögen erhalten bleibt.

(5) Der Stiftungsrat kann die Vorsitzende oder den Vorsitzenden beauftragen, über die Zustimmung in bestimmten Bereichen allein zu entscheiden.
Dies gilt nicht für den Erwerb, die Veräußerung und die Belastung von Grundstücken.

(6) Das Nähere regelt die Satzung.

#### § 9 Verfahren im Stiftungsrat

(1) Der Stiftungsrat tritt mindestens zweimal jährlich zu einer ordentlichen Sitzung zusammen.
Auf Antrag von mindestens zwei Mitgliedern muss der Stiftungsrat zu einer außerordentlichen Sitzung zusammentreten.
Der Stiftungsrat entscheidet in der Regel in seinen Sitzungen.

(2) Der Stiftungsrat ist beschlussfähig, wenn die Mehrheit der Mitglieder anwesend oder gemäß § 7 Absatz 4 vertreten sind, darunter jeweils eine Vertreterin oder ein Vertreter der in § 7 Absatz 1 Nummer 1 bis 3 genannten Stellen.
Die Beschlüsse werden mit der Stimmenmehrheit der anwesenden Mitglieder gefasst.

(3) Beschlüsse über den Inhalt der Satzung können nur mit Zustimmung der Mehrheit der Mitglieder des Stiftungsrates getroffen werden.

(4) In Haushalts- und Personalangelegenheiten können Beschlüsse nicht gegen die Stimmen der Vertreterinnen und Vertreter der zuständigen obersten Landesbehörde, des Bundes und der Stadt Frankfurt (Oder) gefasst werden.

(5) An den Sitzungen des Stiftungsrates nimmt der Vorstand teil.
Weitere Personen können vom Stiftungsrat beratend zu den Stiftungsratssitzungen hinzugezogen werden.

(6) Die oder der Vorsitzende des Fachbeirates oder seine Stellvertreterin oder sein Stellvertreter können an den Sitzungen des Stiftungsrates teilnehmen.

(7) Das Nähere regelt die Satzung.

#### § 10 Vorstand

(1) Der Vorstand besteht aus der geschäftsführenden Direktorin oder dem geschäftsführenden Direktor des Museums.
Diese oder dieser leitet das Museum.
Der Vorstand führt die laufenden Geschäfte der Stiftung nach Maßgabe der Satzung und legt seine Stellvertretung fest.
Der Vorstand ist insbesondere zuständig für

1.  die Aufstellung des Haushaltsplanes und der Finanzplanung,
2.  die Aufstellung des Jahresabschlusses einschließlich einer Vermögensübersicht und die Vorbereitung und Ausführung der Beauftragung der Prüfung des Jahresabschlusses,
3.  die Vorbereitung der Sitzungen und Beschlussfassungen des Stiftungsrates und seiner Beratungsgremien sowie für die Durchführung der Beschlüsse des Stiftungsrates,
4.  die Aufstellung eines Berichts über die Erfüllung des Stiftungszwecks für das Geschäftsjahr.

(2) Der Vorstand vertritt die Stiftung gerichtlich und außergerichtlich.

(3) Das Personal der Stiftung wird vom Vorstand angestellt und entlassen; § 8 Absatz 2 Satz 2 Nummer 4 und Absatz 4 Nummer 1 bleibt unberührt.

(4) Das Nähere regelt die Satzung.

#### § 11 Fachbeirat

(1) Der Fachbeirat besteht aus mindestens fünf Mitgliedern.
Bei den Mitgliedern des Fachbeirates handelt es sich um Persönlichkeiten mit wissenschaftlicher, musealer, kulturpolitischer oder internationaler Expertise, die aufgrund ihres Engagements und ihrer Sachkunde geeignet sind, die Stiftungsorgane in fachlichen Fragen sachkundig zu beraten.
Sie werden auf Vorschlag des Vorstandes vom Stiftungsrat für die Dauer von fünf Jahren bestellt.
Die Wiederbestellung ist zulässig.

(2) Der Fachbeirat wählt für die Dauer der Amtszeit aus seiner Mitte eine Vorsitzende oder einen Vorsitzenden sowie eine stellvertretende Vorsitzende oder einen stellvertretenden Vorsitzenden mit der Mehrheit der abgegebenen Stimmen.
Die Wiederwahl ist zulässig.

(3) Der Fachbeirat berät die Organe der Stiftung, insbesondere den Vorstand, in fachlichen Angelegenheiten.
Er gibt Empfehlungen ab.

(4) Der Fachbeirat soll mindestens einmal im Jahr zu einer ordentlichen Sitzung zusammentreffen.
Eine außerordentliche Sitzung ist einzuberufen, wenn mindestens ein Drittel der Mitglieder des Fachbeirates oder der Stiftungsrat dies verlangen.
Der Vorstand bereitet die Sitzungen in Abstimmung mit der oder dem Vorsitzenden vor und nimmt an den Sitzungen teil.

(5) Die Mitglieder des Fachbeirates sind ehrenamtlich und unentgeltlich tätig.
Es besteht Anspruch auf Ersatz der notwendigen Aufwendungen nach Maßgabe der für die Verwaltung des Landes Brandenburg geltenden Bestimmungen des Reisekostenrechts.

(6) Das Nähere regelt die Satzung.

#### § 12 Personal

Für die Arbeitnehmerinnen und Arbeitnehmer der Stiftung finden der Tarifvertrag für den öffentlichen Dienst der Länder und der Tarifvertrag für Auszubildende der Länder in Ausbildungsberufen nach dem Berufsbildungsgesetz sowie die diese ergänzenden, ändernden oder ersetzenden Tarifverträge in ihrer jeweils für das Land Brandenburg geltenden Fassung Anwendung.

#### § 13 Gesetzliche Überleitung von Arbeitsverhältnissen

Mit Inkrafttreten dieses Gesetzes gehen die Arbeitsverhältnisse der Arbeitnehmerinnen und Arbeitnehmer des Vereins „Kleist-Gedenk- und Forschungsstätte e.
V.“ kraft Gesetzes auf die Stiftung über.
Die Stiftung tritt in die Rechte und Pflichten aus diesen Arbeitsverhältnissen ein.
Für die Arbeitsverhältnisse der kraft Gesetzes übergeleiteten Arbeitnehmerinnen und Arbeitnehmer nach Satz 1 gilt § 12.

#### § 14 Beteiligung bei der Versorgungsanstalt des Bundes und der Länder

Die Beteiligung bei der Versorgungsanstalt des Bundes und der Länder ist durch die Stiftung unverzüglich nach ihrer Errichtung zu beantragen.
Die Beschäftigten sind nach Maßgabe der Beteiligungsvereinbarung bei der Versorgungsanstalt des Bundes und der Länder zu versichern.

#### § 15 Haushaltsplan, Haushaltsjahr und Rechnungsprüfung

(1) Der Haushaltsplan der Stiftung ist alljährlich rechtzeitig vor Beginn des Haushaltsjahres vom Vorstand im Entwurf aufzustellen und vom Stiftungsrat festzustellen.
Er soll in der Form dem Landeshaushaltsplan entsprechen und nach den für diesen geltenden Grundsätzen aufgestellt werden.
Er bedarf der Genehmigung der Rechtsaufsichtsbehörde.

(2) Das Haushaltsjahr der Stiftung ist das Kalenderjahr.

(3) Für das Haushalts-, Kassen- und Rechnungswesen sowie für die Rechnungsprüfung der Stiftung finden die für die Verwaltung des Landes Brandenburg geltenden Bestimmungen der Landeshaushaltsordnung entsprechend Anwendung.

#### § 16 Stiftungsauflösung

(1) Die Stiftung kann durch Gesetz aufgelöst werden.

(2) Bei Auflösung der durch dieses Gesetz errichteten Stiftung fallen die Vermögensgegenstände, die die Stadt Frankfurt (Oder) eingebracht hat, der Stadt Frankfurt (Oder) zu.
Die Vermögensgegenstände, die die Dauerleihgeber eingebracht haben, fallen bei Stiftungsauflösung diesen zu.
Über das während des Bestehens der Stiftung hinzugewonnene Vermögen treffen der Bund, das Land Brandenburg und die Stadt Frankfurt (Oder) im Falle der Stiftungsauflösung eine Vereinbarung, nach der das Stiftungsvermögen anteilig und unter Berücksichtigung der finanziellen Förderung dieser Stiftung dem Bund, dem Land Brandenburg und der Stadt Frankfurt (Oder) zufällt.

(3) Soweit Vermögen der Stadt Frankfurt (Oder) und dem Land Brandenburg zufällt, ist dieses unmittelbar und ausschließlich für gemeinnützige Zwecke im Sinne des Abschnitts „Steuerbegünstigte Zwecke“ der Abgabenordnung und in einer dem Stiftungszweck möglichst nahekommenden Weise für den Betrieb des Kleist-Museums zu verwenden.

#### § 17 Einschränkung von Grundrechten

Durch dieses Gesetz wird das Grundrecht der Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 18 Inkrafttreten

Dieses Gesetz tritt am 1.
Januar 2019 in Kraft.

Potsdam, den 23.
November 2018

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

### Anlagen

1

[Anlage 1 (zu § 2 Absatz 2 Satz 2 Nummer 7, § 3 Absatz 1 Nummer 1 und Absatz 2 Satz 1) - Liegenschaft der Stiftung „Kleist-Museum“](/br2/sixcms/media.php/68/GVBl_I_30_2018-Anlage-1.pdf "Anlage 1 (zu § 2 Absatz 2 Satz 2 Nummer 7, § 3 Absatz 1 Nummer 1 und Absatz 2 Satz 1) - Liegenschaft der Stiftung „Kleist-Museum“") 151.9 KB

2

[Anlage 2 (zu § 3 Absatz 1 Nummer 2) - Literatur- und Kunstsammlung](/br2/sixcms/media.php/68/GVBl_I_30_2018-Anlage-2.pdf "Anlage 2 (zu § 3 Absatz 1 Nummer 2) - Literatur- und Kunstsammlung") 351.0 KB

3

[Anlage 3 (zu § 3 Absatz 1 Nummer 3) - Inventar](/br2/sixcms/media.php/68/GVBl_I_30_2018-Anlage-3.pdf "Anlage 3 (zu § 3 Absatz 1 Nummer 3) - Inventar") 1.2 MB