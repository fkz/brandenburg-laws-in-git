## Gesetz zu dem Staatsvertrag zwischen dem Land Berlin und dem Land Brandenburg über die Einrichtung und den Betrieb eines klinischen Krebsregisters nach § 65c des Fünften Buches Sozialgesetzbuch

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem am 12.
April 2016 unterzeichneten Staatsvertrag zwischen dem Land Berlin und dem Land Brandenburg über die Einrichtung und den Betrieb eines klinischen Krebsregisters nach § 65c des Fünften Buches Sozialgesetzbuch wird zugestimmt.
Der Vertrag wird nachstehend veröffentlicht.

#### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel 40 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu machen.
An diesem Tag tritt das Gesetz zur Einführung einer Meldepflicht für Krebserkrankungen vom 20.
April 2006 (GVBl.
I S.
62) außer Kraft.

Potsdam, den 16.
Juni 2016

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag  
](/vertraege/stv_krebsregister_be_bb_2016)

* * *

### Anlagen

1

[Staatsvertrag zwischen dem Land Berlin und dem Land Brandenburg über die Einrichtung und den Betrieb eines klinischen Krebsregisters nach § 65c des Fünften Buches Sozialgesetzbuch](/br2/sixcms/media.php/68/GVBl_I_17_2016_001Anlage-zum-Hauptdokument.pdf "Staatsvertrag zwischen dem Land Berlin und dem Land Brandenburg über die Einrichtung und den Betrieb eines klinischen Krebsregisters nach § 65c des Fünften Buches Sozialgesetzbuch") 1.1 MB