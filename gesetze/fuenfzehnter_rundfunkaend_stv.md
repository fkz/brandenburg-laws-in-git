## Gesetz zu dem Fünfzehnten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Fünfzehnter Rundfunkänderungsstaatsvertrag)

Der Landtag hat das folgende Gesetz beschlossen:

### § 1

Dem am 21.
Dezember 2010 vom Land Brandenburg unterzeichneten Fünfzehnten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Fünfzehnter Rundfunkänderungsstaatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem Artikel 7 Absatz 2 Satz 1 vorbehaltlich des Satzes 2 am 1. Januar 2013 in Kraft.
§ 14 Absatz 1, 2 und 6 des Rundfunkbeitragsstaatsvertrages tritt nach Artikel 7 Absatz 2 Satz 2 des Staatsvertrages am 1.
Januar 2012 in Kraft.
Sollte der Staatsvertrag nach seinem Artikel 7 Absatz 2 Satz 3 gegenstandslos werden, ist dies unverzüglich im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 9.
Juni 2011

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

* * *

[zum Staatsvertrag](/de/vertraege-241960) - Rundfunkbeitragsstaatsvertrag

[zum Staatsvertrag](/de/vertraege-240374) - Rundfunkstaatsvertrag

[zum Staatsvertrag](/de/vertraege-240353) - ZDF-Staatsvertrag

[zum Staatsvertrag](/de/vertraege-241544) - Deutschlandradio-Staatsvertrag

[zum Staatsvertrag](/de/vertraege-237418) - Rundfunkfinanzierungsstaatsvertrag