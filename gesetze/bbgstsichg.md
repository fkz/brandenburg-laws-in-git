## Gesetz zur Errichtung eines Sondervermögens „Brandenburgs Stärken für die Zukunft sichern“ (Brandenburgs-Stärken-Sicherungsgesetz - BbgStSichG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Errichtung

Das Land Brandenburg errichtet unter dem Namen „Brandenburgs Stärken für die Zukunft sichern“ ein rechtlich unselbstständiges Sondervermögen des Landes gemäß § 26 Absatz 2 der Landeshaushaltsordnung.

#### § 2 Zweck des Sondervermögens

(1) Aus dem Sondervermögen werden bis 2022 Maßnahmen finanziert, die zur Beseitigung der direkten und indirekten Folgen der Coronapandemie und zur Verhinderung weiterer Schäden unabweisbar sind.
Die Bekämpfung der Folgen der Coronapandemie umfasst insbesondere:

1.  Leistungen und Ansprüche nach dem Infektionsschutzgesetz vom 20.
    Juli 2000 (BGBl. I S. 1045), das zuletzt durch Artikel 2 des Gesetzes vom 18.
    November 2020 (BGBl. I S. 2397, 2405) geändert worden ist, in der jeweils geltenden Fassung sowie Leistungen und Ansprüche, die auf Maßnahmen nach dem Infektionsschutzgesetz zurückzuführen sind,
2.  Maßnahmen zur Stärkung der brandenburgischen Kommunen,
3.  Maßnahmen zum Erhalt der brandenburgischen Wirtschaftskraft, zur Belebung der Konjunktur und zur Förderung nachhaltigen Wachstums insbesondere durch Investitionen in Klimaschutz und digitale Transformation,
4.  Maßnahmen zur Kofinanzierung von EU- und Bundesprogrammen,
5.  Maßnahmen zum Gesundheitsschutz und zur Sicherung der sozialen und kulturellen Infrastrukturen, der Bildungs- und Sportinfrastruktur sowie
6.  Maßnahmen zur Erhaltung der öffentlichen Infrastruktur sowie der Beteiligungen des Landes Brandenburg.

Die zweckentsprechende Verwendung ist zu begründen.

(2) Darüber hinaus kann das Sondervermögen dem Landeshaushalt Mittel zur Kompensation der nicht konjunkturbedingten Steuermindereinnahmen des Landes bis 2022 bereitstellen.
Die Kompensation ist für das Jahr 2022 beschränkt auf die um die Konjunkturkomponente nach § 18a der Landeshaushaltsordnung in der Fassung der Bekanntmachung vom 21.
April 1999 (GVBl.
I S. 106), das zuletzt durch Artikel 1 des Gesetzes vom 5.
Juni 2019 (GVBl.
I Nr. 20) geändert worden ist, bereinigten, tatsächlich entstandenen Mindereinnahmen.

#### § 3 Stellung im Rechtsverkehr

(1) Das Sondervermögen ist nicht rechtsfähig.
Es kann unter seinem Namen im Rechtsverkehr handeln, klagen und verklagt werden.

(2) Der allgemeine Gerichtsstand des Sondervermögens ist Potsdam.

(3) Das Sondervermögen ist vom übrigen Vermögen sowie von den Rechten und Verbindlichkeiten des Landes getrennt zu halten.

#### § 4 Verwaltung

(1) Das für Finanzen zuständige Ministerium verwaltet das Sondervermögen.
Es kann sich hierzu einer anderen Landesbehörde oder eines Dritten bedienen.
Die Kosten für die Verwaltung des Sondervermögens trägt das Land.

(2) Die Mittel des Sondervermögens bleiben unverzinst im Liquiditätsmanagement des Landes.

#### § 5 Zuführungen zum Sondervermögen

(1) Das Sondervermögen wird im Haushaltsjahr 2021 durch Zuführung von Mitteln aus dem Landeshaushalt gebildet.

(2) Die Höhe der Zuführung entspricht dem Teil der gemäß § 2 Absatz 1 Nummer 3 des Gesetzes über die Feststellung des Haushaltsplanes des Landes Brandenburg für das Haushaltsjahr 2021 (Haushaltsgesetz 2021 - HG 2021) vom 18.
Dezember 2020 (GVBl.
I Nr.
35) aufgenommenen Kredite, der nicht zur Deckung von coronabedingten Ausgaben oder zur Kompensation von nicht konjunkturbedingten Steuermindereinnahmen im Haushaltsjahr 2021 benötigt wird.

#### § 6 Verwendung des Sondervermögens und Wirtschaftsplan

Für die zweckentsprechende Verwendung der Mittel des Sondervermögens im Sinne des § 2 ist der Wirtschaftsplan des Sondervermögens maßgeblich.
Der Wirtschaftsplan als Teil des Haushaltsplans wird vom für Finanzen zuständigen Ministerium aufgestellt.

#### § 7 Berichterstattung

Das für Finanzen zuständige Ministerium berichtet dem für den Haushalt zuständigen Ausschuss des Landtages jährlich zum 31.
Dezember über die Bewilligungen und den Abfluss der Mittel aus dem Sondervermögen.

#### § 8 Jahresrechnung

(1) Das für Finanzen zuständige Ministerium stellt für jedes Haushaltsjahr die Jahresrechnung des Sondervermögens auf.
Diese wird der Haushaltsrechnung des Landes beigefügt.

(2) In der Jahresrechnung sind der Bestand des Sondervermögens sowie die Einnahmen und Ausgaben nachzuweisen.

#### § 9 Befristung

(1) Finanzierungen für Maßnahmen und Leistungen nach § 2 Absatz 1 sind nur bis zum 31. Dezember 2022 zulässig.

(2) Das Sondervermögen ist bis zum 31.
Dezember 2022 befristet.

(3) Die Auskehrung des Restbetrages erfolgt an den Landeshaushalt.
Dieser darf ausschließlich zur Tilgung der aufgrund der Kreditermächtigung im Jahr 2021 aufgenommenen Kredite genutzt werden.

#### § 10 Inkrafttreten, Außerkrafttreten

Dieses Gesetz tritt am 1.
Januar 2021 in Kraft und mit Ablauf des 31.
Dezember 2022 außer Kraft.

Potsdam, den 18.
Dezember 2020

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke