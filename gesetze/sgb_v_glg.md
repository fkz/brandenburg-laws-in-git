## Gesetz zur Errichtung eines gemeinsamen Landesgremiums im Sinne des § 90a des Fünften Buches Sozialgesetzbuch (SGB V gemeinsames Landesgremiumsgesetz - SGB V gLG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1  
Errichtung

(1) Bei dem für das Gesundheitswesen zuständigen Ministerium wird ein gemeinsames Landesgremium gemäß § 90a des Fünften Buches Sozialgesetzbuch errichtet.

(2) Dem gemeinsamen Landesgremium gehören als ständige Mitglieder an:

1.  das für das Gesundheitswesen zuständige Ministerium,
2.  das für das Rettungswesen zuständige Ministerium,
3.  die Kassenärztliche Vereinigung Brandenburg,
4.  die Landesverbände der Krankenkassen sowie die Ersatzkassen,
5.  die Landeskrankenhausgesellschaft Brandenburg,
6.  die kommunalen Spitzenverbände in Brandenburg.

Die Landesregierung wirkt darauf hin, dass die gleichberechtigte Teilhabe von Frauen und Männern gewährleistet ist.

(3) Die in Brandenburg für die Wahrnehmung der Interessen von Patientinnen und Patienten und der Selbsthilfe chronisch kranker und behinderter Menschen maßgeblichen Organisationen im Sinne des § 140f des Fünften Buches Sozialgesetzbuch, die Landesärztekammer Brandenburg, die Ostdeutsche Psychotherapeutenkammer und die auf Landesebene für die Wahrnehmung der Interessen der Pflegeberufe maßgebliche Dachorganisation haben ein Mitberatungsrecht.
Die genannten Organisationen benennen hierfür jeweils eine sachkundige Person.
Das Mitberatungsrecht umfasst auch das Recht auf Anwesenheit bei Beschlussfassung.

(4) Das für das Gesundheitswesen zuständige Ministerium führt den Vorsitz und richtet eine Geschäftsstelle ein.
Das gemeinsame Landesgremium gibt sich eine Geschäftsordnung.

#### § 2  
Aufgaben

(1) Das gemeinsame Landesgremium behandelt sektorenübergreifende Fragen der flächendeckenden medizinischen Versorgung.
Regionale Versorgungsbedürfnisse, raumplanerische Aspekte und Perspektiven der demografischen Entwicklung werden berücksichtigt.
Das gemeinsame Landesgremium soll Empfehlungen zu sektorenübergreifenden Versorgungsfragen abgeben.

(2) Das gemeinsame Landesgremium hat das Recht auf Stellungnahme gemäß § 90a Absatz 2 des Fünften Buches Sozialgesetzbuch.

(3) Zur Wahrnehmung seiner Aufgaben kann das gemeinsame Landesgremium weitere Beteiligte hinzuziehen.

(4) Jedes ständige Mitglied im gemeinsamen Landesgremium hat eine Stimme.
Beschlüsse werden einstimmig gefasst.
Das Nähere regelt die Geschäftsordnung.

(5) Die von den beteiligten Organisationen bestellten Mitglieder und entsandten Vertretungspersonen haben Anspruch auf Abgeltung ihres Zeitaufwandes und auf Erstattung ihrer baren Auslagen sowie auf Reisekostenvergütung nach den für die von ihren vertretenen Organisationen geltenden Grundsätzen.
Die Entschädigung ist von der entsendenden Organisation zu tragen.
Für die Patientenvertreter sind die Entschädigungsleistungen nach Maßgabe des § 140f Absatz 5 des Fünften Buches Sozialgesetzbuch gegenüber dem Land geltend zu machen.
Reisekosten werden nach den landesrechtlich geltenden Vorschriften erstattet.

#### § 3  
Inkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 15.
Oktober 2013

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch