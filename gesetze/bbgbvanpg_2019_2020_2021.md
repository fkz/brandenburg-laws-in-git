## Brandenburgisches Besoldungs- und Versorgungsanpassungsgesetz 2019/2020/2021 (BbgBVAnpG 2019/2020/2021)

#### § 1 Geltungsbereich

(1) Dieses Gesetz gilt für die

1.  Beamtinnen und Beamten des Landes, der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts,
2.  Richterinnen und Richter des Landes,
3.  Versorgungsempfängerinnen und Versorgungsempfänger, denen laufende Versorgungsbezüge zustehen, die das Land, eine Gemeinde, ein Gemeindeverband oder eine der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten oder Stiftungen des öffentlichen Rechts zu tragen hat.

(2) Dieses Gesetz gilt nicht für Ehrenbeamtinnen und Ehrenbeamte sowie für ehrenamtliche Richterinnen und Richter.

(3) Dieses Gesetz gilt nicht für öffentlich-rechtliche Religionsgesellschaften und ihre Verbände.

#### § 2 Anpassung der Besoldung im Jahr 2019

(1) Die nachfolgenden Dienstbezüge und sonstigen Bezüge werden ab 1. Januar 2019 um 3,7 Prozent erhöht:

1.  die Grundgehaltssätze,
2.  der Familienzuschlag,
3.  die Amtszulagen sowie die allgemeine Stellenzulage nach Vorbemerkung Nummer 13 der Besoldungsordnungen A und B der Anlage 1 des Brandenburgischen Besoldungsgesetzes.

(2) Absatz 1 gilt entsprechend für

1.  die Leistungsbezüge nach § 30 Absatz 1 des Brandenburgischen Besoldungsgesetzes in Verbindung mit § 31 Absatz 2 Satz 3, § 32 Satz 5 und § 33 Satz 6 des Brandenburgischen Besoldungsgesetzes,
2.  die in § 2 Absatz 1 Nummer 6 bis 8 des Brandenburgischen Besoldungs- und Versorgungsanpassungsgesetzes 2008 genannten Bezüge.

(3) Die Anwärtergrundbeträge werden ab 1.
Januar 2019 um 50 Euro und anschließend um 0,5 Prozent erhöht.

#### § 3 Anpassung der Besoldung im Jahr 2020

(1) Ab 1.
Januar 2020 werden die in § 2 Absatz 1 und 2 genannten Bezüge um 3,7 Prozent erhöht.

(2) Die Anwärtergrundbeträge werden ab 1.
Januar 2020 um 50 Euro und anschließend um 0,5 Prozent erhöht.

#### § 4 Anpassung der Besoldung im Jahr 2021

Ab 1.
Januar 2021 werden die in § 2 Absatz 1 und 2 genannten Bezüge um 1,4 Prozent erhöht.

#### § 5 Rundungsregelung

Bei der Berechnung der nach den §§ 2 bis 4 erhöhten Bezüge sind Bruchteile eines Cents unter 0,5 abzurunden und Bruchteile von 0,5 und mehr aufzurunden.

#### § 6 Anpassung der Versorgungsbezüge

(1) Für Versorgungsempfängerinnen und Versorgungsempfänger gelten die Erhöhungen nach den §§ 2 bis 4 für die dort aufgeführten Bezügebestandteile entsprechend, sofern diese Grundlage der Versorgung sind.

(2) Versorgungsbezüge, die in festen Beträgen festgesetzt sind, und der Betrag nach Artikel 13 § 2 Absatz 4 des Fünften Gesetzes zur Änderung besoldungsrechtlicher Vorschriften vom 28.
Mai 1990 (BGBl. I S. 967, 976) werden ab 1. Januar 2019 um 3,6 Prozent, ab 1.
Januar 2020 um 3,6 Prozent und ab 1. Januar 2021 um 1,3 Prozent erhöht.

(3) Bei Versorgungsempfängerinnen und Versorgungsempfängern, deren Versorgungsbezügen ein Grundgehalt der Besoldungsgruppe A 5 bis A 8 zugrunde liegt, vermindert sich das Grundgehalt ab 1. Januar 2019 um 61,90 Euro, ab 1. Januar 2020 um 64,19 Euro und ab 1. Januar 2021 um 65,09 Euro, wenn ihren ruhegehaltfähigen Dienstbezügen die Stellenzulage nach Vorbemerkung Nummer 27 Absatz 1 Buchstabe a oder b der Bundesbesoldungsordnungen A und B der Anlage I des Bundesbesoldungsgesetzes in der am 31. August 2006 geltenden Fassung bei Eintritt in den Ruhestand nicht zugrunde gelegen hat.

#### § 7 Bekanntmachung

Das Ministerium der Finanzen macht die Beträge der nach den §§ 2 bis 4 erhöhten Bezüge im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I durch Neubekanntmachung der Anlagen 4 bis 8 des Brandenburgischen Besoldungsgesetzes bekannt.