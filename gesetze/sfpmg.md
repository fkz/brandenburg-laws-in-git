## Gesetz über die Errichtung der Stiftung „Fürst-Pückler-Museum Park und Schloss Branitz“ (SFPMG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Errichtung

(1) Das Land Brandenburg errichtet unter dem Namen Stiftung „Fürst-Pückler-Museum Park und Schloss Branitz“ eine rechtsfähige Stiftung des öffentlichen Rechts.

(2) Sitz der Stiftung ist Cottbus/Chóśebuz.

#### § 2 Stiftungszweck

(1) Zweck der Stiftung ist die Erhaltung, Pflege, Erforschung, Erschließung und Präsentation des Gesamtkunstwerkes aus Garten- und Landschaftsgestaltung, Architektur, Raumausstattung und der Museumssammlungen des Fürsten Hermann von Pückler-Muskau in Branitz.
Park und Schloss Branitz sind als Orte kulturellen Lebens und der Künste in der Tradition des Fürsten Hermann von Pückler-Muskau zu nutzen und zu entwickeln.

(2) Die Stiftung hat die Aufgabe, die ihr übergebenen Kulturgüter zu bewahren und zu ergänzen, unter Berücksichtigung historischer, kunst- und gartenhistorischer und denkmalpflegerischer Belange zu pflegen, der Öffentlichkeit zugänglich zu machen und die Auswertung dieses Kulturbesitzes für die Interessen der Allgemeinheit, vor allem in Wissenschaft und Bildung, zu ermöglichen.
Insbesondere gehören hierzu:

1.  die bauliche und gärtnerische Unterhaltung und Sanierung der Liegenschaften und der Kulturdenkmale unter Beachtung der Anforderungen des Denkmalschutzes, der Denkmalpflege und der Gartendenkmalpflege,
2.  die Gewährleistung einer denkmal- und gartendenkmalverträglichen Nutzung der Kulturdenkmale, insbesondere als Museum, durch die Öffentlichkeit sowie die Unterhaltung von Einrichtungen, die der Betreuung der Besucher dienen,
3.  die Pflege und der Ausbau der Museumssammlungen sowie die digitale Erschließung und Vermittlung der Sammlungsobjekte,
4.  die wissenschaftliche und publizistische Aufarbeitung und Dokumentation des Kulturdenkmalbestandes sowie die Öffentlichkeitsarbeit und kulturelle Bildung,
5.  die Zusammenarbeit mit nationalen und internationalen Stätten und Institutionen, die das Erbe des Fürsten Hermann von Pückler-Muskau bewahren und erforschen.

(3) Die Stiftung verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke im Sinne des Abschnitts „Steuerbegünstigte Zwecke“ der Abgabenordnung.

#### § 3 Stiftungsvermögen

(1) Das Eigentum an den in der Anlage aufgeführten Liegenschaften des Denkmalensembles Park und Schloss Branitz geht mit Inkrafttreten dieses Gesetzes auf die Stiftung entschädigungslos und ergebnisneutral über.

(2) Das Land Brandenburg und die Stadt Cottbus/Chóśebuz können der Stiftung weitere Vermögensgegenstände übertragen.

(3) Das Vermögen der Stiftung ist zu erhalten.

#### § 4 Finanzierung

(1) Zur Erfüllung des Stiftungszwecks erhält die Stiftung Zuwendungen nach Maßgabe der jeweiligen Haushaltspläne des Landes Brandenburg und der Stadt Cottbus/Chóśebuz.
Das Nähere regelt das Finanzierungsabkommen.

(2) Die Stiftung ist berechtigt, Schenkungen, Erbschaften, Zustiftungen und Zuwendungen von dritter Seite zur Erfüllung des Stiftungszwecks anzunehmen.

(3) Die Erträge des Stiftungsvermögens sowie sonstige Einnahmen und Erträge sind nur im Sinne des Stiftungszwecks zu verwenden.
Die Stiftung ist selbstlos tätig; sie verfolgt nicht in erster Linie eigenwirtschaftliche Zwecke.
Es darf keine Person durch Ausgaben, die dem Zweck der Stiftung fremd sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.

#### § 5 Rechtsaufsicht

Die Rechtsaufsicht über die Stiftung führt die für Kultur zuständige oberste Landesbehörde.

#### § 6 Organe der Stiftung

Organe der Stiftung sind:

1.  der Stiftungsrat,
2.  der Vorstand (geschäftsführende Direktorin oder geschäftsführender Direktor).

#### § 7 Zusammensetzung des Stiftungsrates

(1) Dem Stiftungsrat gehören an:

1.  zwei Vertreterinnen oder Vertreter der für Kultur zuständigen obersten Landesbehörde, die nicht zugleich mit der Rechtsaufsicht über die Stiftung befasst sind, eine oder einer davon als Vorsitzende oder Vorsitzender des Stiftungsrates,
2.  eine Vertreterin oder ein Vertreter der für Finanzen zuständigen obersten Landesbehörde,
3.  zwei Vertreterinnen oder Vertreter der Stadt Cottbus/Chóśebuz, eine oder einer davon als Stellvertretung des Vorsitzes,
4.  eine Vertreterin oder ein Vertreter des Landtages Brandenburg,
5.  eine Vertreterin oder ein Vertreter der Stadtverordnetenversammlung Cottbus/Chóśebuz,
6.  eine Vertreterin oder ein Vertreter der für Kultur zuständigen obersten Bundesbehörde.

Die Erben nach Adrian Heinrich Kurt Günther Georg Graf von Pückler Freiherr von Groditz, geboren am 24. März 1905, gestorben am 19.
Januar 1945, sind berechtigt, eine Vertreterin oder einen Vertreter in den Stiftungsrat zu entsenden.

(2) Die in Absatz 1 Satz 1 Nummer 1 und 2 genannten Mitglieder werden von der Landesregierung Brandenburg entsandt und abberufen.
Die in Absatz 1 Satz 1 Nummer 3 genannten Mitglieder werden von der Stadt Cottbus/Chóśebuz entsandt und abberufen.
Das in Absatz 1 Satz 1 Nummer 4 genannte Mitglied wird vom Landtag Brandenburg entsandt und abberufen.
Das in Absatz 1 Satz 1 Nummer 5 genannte Mitglied wird von der Stadtverordnetenversammlung Cottbus/Chóśebuz entsandt und abberufen.
Das in Absatz 1 Satz 1 Nummer 6 genannte Mitglied wird von der Bundesrepublik Deutschland entsandt und abberufen.
Für den Fall des Ausscheidens ist eine Nachfolgerin oder ein Nachfolger zu berufen.

(3) Für jedes Mitglied ist eine Stellvertreterin oder ein Stellvertreter zu berufen.
Sind das Mitglied und seine Stellvertreterin oder sein Stellvertreter verhindert, kann die entsendende Stelle eine Bevollmächtigte oder einen Bevollmächtigten entsenden.

(4) Die Mitglieder nach Absatz 1 sind unentgeltlich tätig.
Sie haben Anspruch auf Ersatz ihrer notwendigen Aufwendungen nach Maßgabe der allgemein für die Verwaltung des Landes Brandenburg geltenden Bestimmungen des Reisekostenrechts.

(5) An den Sitzungen des Stiftungsrates nimmt der Vorstand teil.
Weitere Personen können vom Stiftungsrat beratend zu den Stiftungsratssitzungen hinzugezogen werden.
Das Nähere regelt die Satzung.

#### § 8 Aufgaben des Stiftungsrates

(1) Der Stiftungsrat erlässt und ändert eine Satzung nach Maßgabe dieses Gesetzes.
Die Satzung sowie Satzungsänderungen bedürfen der Zustimmung der für Kultur zuständigen obersten Landesbehörde.

(2) Der Stiftungsrat beschließt über die grundsätzlichen Angelegenheiten der Stiftung und legt die wesentlichen Aufgaben und Tätigkeiten der Stiftung fest.
Der Stiftungsrat entscheidet insbesondere über

1.  die Feststellung des Haushaltsplanes und der Finanzplanung,
2.  die Feststellung einer Eröffnungsbilanz,
3.  die Beauftragung der Prüfung des Jahresabschlusses,
4.  die Feststellung des geprüften Jahresabschlusses und die Entlastung des Vorstandes,
5.  die Einstellung und Entlassung des Vorstandes,
6.  alle nicht nach § 10 dem Vorstand obliegenden Geschäfte.

(3) Die Vorsitzende oder der Vorsitzende des Stiftungsrates vertritt die Stiftung gegenüber dem Vorstand.

(4) Der Zustimmung des Stiftungsrates bedürfen

1.  die Einstellung aller Beschäftigten ab Entgeltgruppe 13 des Tarifvertrages für den öffentlichen Dienst der Länder,
2.  der Erwerb, die Veräußerung und die Belastung von Grundstücken,
3.  die Annahme von Erbschaften,
4.  die Veräußerung von Sammlungsgegenständen; eine Veräußerung ist nur zulässig, wenn mit dem Veräußerungserlös der Sammlungsbestand ergänzt wird.

(5) Der Stiftungsrat kann die Vorsitzende oder den Vorsitzenden beauftragen, über die Zustimmung in bestimmten Bereichen allein zu entscheiden.
Dies gilt nicht für den Erwerb, die Veräußerung und die Belastung von Grundstücken.
Das Nähere regelt die Satzung.

#### § 9 Verfahren im Stiftungsrat

(1) Der Stiftungsrat tritt mindestens zweimal jährlich zu einer ordentlichen Sitzung zusammen.
Auf Antrag von mindestens einem Drittel der Mitglieder muss der Stiftungsrat zu weiteren Sitzungen zusammentreten.
Der Stiftungsrat entscheidet in der Regel in seinen Sitzungen.

(2) Der Stiftungsrat ist beschlussfähig, wenn zwei Drittel der Mitglieder, davon jeweils eine Vertreterin oder ein Vertreter des Landes Brandenburg und der Stadt Cottbus/Chóśebuz anwesend oder nach § 7 Absatz 3 vertreten sind.
Die Beschlüsse werden mit der Stimmenmehrheit der Anwesenden gefasst.

(3) Beschlüsse über den Inhalt der Satzung, über den Haushaltsplan und dessen Änderung sowie Beschlüsse, die über den bestehenden Haushaltsplan hinaus Auswirkungen auf den Haushalt der Stiftung haben, können nur mit Zustimmung von zwei Dritteln der Mitglieder des Stiftungsrates getroffen werden, wobei Beschlüsse gegen Stimmen des Landes Brandenburg oder der Stadt Cottbus/Chóśebuz nicht möglich sind.

(4) Das Nähere regelt die Satzung.

#### § 10 Vorstand

(1) Der Vorstand besteht aus der geschäftsführenden Direktorin oder dem geschäftsführenden Direktor der Stiftung.
Der Vorstand führt die Geschäfte der Stiftung nach Maßgabe der Satzung.
Der Vorstand ist insbesondere zuständig für

1.  die Aufstellung des Haushaltsplanes und der Finanzplanung,
2.  die Aufstellung des Jahresabschlusses einschließlich einer Vermögensübersicht und die Vorbereitung und Ausführung der Beauftragung der Prüfung des Jahresabschlusses,
3.  die Vorbereitung der Sitzungen und Entscheidungen des Stiftungsrates und seiner Beratungsgremien,
4.  die Aufstellung eines Berichts über die Erfüllung des Stiftungszwecks für das Geschäftsjahr.

(2) Der Vorstand vertritt die Stiftung gerichtlich und außergerichtlich.

(3) Das Personal der Stiftung wird vom Vorstand angestellt und entlassen; § 8 Absatz 2 Satz 2 Nummer 5 und Absatz 4 Nummer 1 bleibt unberührt.
Das Nähere regelt die Satzung.

#### § 11 Personal

Für die Arbeitnehmerinnen und Arbeitnehmer der Stiftung finden der Tarifvertrag für den öffentlichen Dienst der Länder und der Tarifvertrag für Auszubildende der Länder in Ausbildungsberufen nach dem Berufsbildungsgesetz sowie die diese ergänzenden, ändernden oder ersetzenden Tarifverträge Anwendung.

#### § 12 Beteiligung bei der Versorgungsanstalt des Bundes und der Länder

Die Beteiligung bei der Versorgungsanstalt des Bundes und der Länder ist durch die Stiftung unverzüglich nach ihrer Errichtung zu beantragen.
Die Beschäftigten sind nach Maßgabe der Beteiligungsvereinbarung bei der Versorgungsanstalt des Bundes und der Länder zu versichern.

#### § 13 Gesetzliche Personalgestellung von Arbeitnehmerinnen und Arbeitnehmern

(1) Arbeitnehmerinnen und Arbeitnehmer der Stadt Cottbus/Chóśebuz, die für die kommunale unselbständige Stiftung „Fürst-Pückler-Museum Park und Schloss Branitz“ tätig sind, werden der Stiftung mit ihrer Errichtung im Wege einer Personalgestellung kraft Gesetzes zur Aufgabenwahrnehmung zur Verfügung gestellt.
Die Personalgestellung nach Satz 1 lässt die Arbeitsverhältnisse der betroffenen Arbeitnehmerinnen und Arbeitnehmer mit der Stadt Cottbus/Chóśebuz unberührt.

(2) Die Stadt Cottbus/Chóśebuz und die Stiftung schließen unverzüglich nach Errichtung der Stiftung einen Personalgestellungsvertrag, der die Einzelheiten der Personalgestellung nach Absatz 1, insbesondere die Durchführung der Gestellung zwischen der Stadt Cottbus/Chóśebuz und der Stiftung, regelt.
Individuelle Maßnahmen hinsichtlich einzelner Arbeitnehmerinnen und Arbeitnehmer sind nicht Gegenstand des Personalgestellungsvertrages nach Satz 1.

(3) Die Personalgestellung nach Absatz 1 endet zu dem Zeitpunkt, zu dem die bestehenden Arbeitsverhältnisse der Arbeitnehmerinnen und Arbeitnehmer der Stadt Cottbus/Chóśebuz gemäß § 14 Absatz 1 auf die Stiftung übergehen.

#### § 14 Gesetzliche Überleitung von Arbeitsverhältnissen

(1) Am ersten Tag des auf den Abschluss der Beteiligungsvereinbarung nach § 12 folgenden übernächsten Kalendermonats, spätestens jedoch am 1.
Januar 2019, gehen die bestehenden Arbeitsverhältnisse der Arbeitnehmerinnen und Arbeitnehmer der Stadt Cottbus/Chóśebuz, die für die kommunale unselbständige Stiftung „Fürst-Pückler-Museum Park und Schloss Branitz“ tätig sind, kraft Gesetzes auf die Stiftung über.
Dies gilt nicht, soweit die betroffenen Arbeitnehmerinnen und Arbeitnehmer von ihrem Widerspruchsrecht nach Absatz 3 Gebrauch gemacht haben.

(2) Die Stadt Cottbus/Chóśebuz informiert die Arbeitnehmerinnen und Arbeitnehmer, deren Arbeitsverhältnisse gemäß Absatz 1 kraft Gesetzes auf die Stiftung übergehen, unverzüglich über den Abschluss der Beteiligungsvereinbarung nach § 12 und die Überleitung des Personals nach Absatz 1.

(3) Den nach Absatz 1 übergeleiteten Arbeitnehmerinnen und Arbeitnehmern steht ein Widerspruchsrecht in entsprechender Anwendung des § 613a Absatz 6 des Bürgerlichen Gesetzbuches zu.
Die Stadt Cottbus/Chóśebuz hat die betroffenen Arbeitnehmerinnen und Arbeitnehmer zugleich mit der Mitteilung über den Übergang ihrer bestehenden Arbeitsverhältnisse auf die Stiftung über ihr Widerspruchsrecht zu unterrichten.
Der Widerspruch ist gegenüber der Stadt Cottbus/Chóśebuz innerhalb eines Monats nach Zugang der Unterrichtung nach Satz 2 zu erklären.

(4) Die betriebsbedingte ordentliche Kündigung des Arbeitsverhältnisses einer Arbeitnehmerin oder eines Arbeitnehmers durch die Stadt Cottbus/Chóśebuz oder durch die Stiftung wegen des Übergangs des Arbeitsverhältnisses gemäß Absatz 1 ist unzulässig.
Das Recht zur Kündigung aus sonstigen Gründen bleibt unberührt.

#### § 15 Besitzstände

(1) Für die nach § 14 Absatz 1 Satz 1 übergegangenen Arbeitnehmerinnen und Arbeitnehmer finden ab dem Zeitpunkt des Übergangs der Tarifvertrag des öffentlichen Dienstes für den Bereich Verwaltung im Bereich der Vereinigung der kommunalen Arbeitgeberverbände sowie der Tarifvertrag zur Überleitung der Beschäftigten der kommunalen Arbeitgeber in den TVöD und zur Regelung des Übergangsrechts mit folgenden Maßgaben Anwendung:

1.  Der Arbeitnehmerin oder dem Arbeitnehmer ist im Ersteinsatz eine Tätigkeit in dem Aufgabengebiet zuzuweisen, das von der Stadt Cottbus/Chóśebuz auf die Stiftung übertragen wurde und in dem sie oder er vor dem Übergang des Arbeitsverhältnisses tätig war.
    Die Wertigkeit der Tätigkeit muss mindestens der Entgeltgruppe entsprechen, der die Arbeitnehmerin oder der Arbeitnehmer am Tag vor dem Übergang bei der Stadt Cottbus/Chóśebuz zugeordnet war.
2.  Bei der Berechnung tarifrechtlich maßgebender Zeiten, insbesondere der Beschäftigungszeiten und Stufenlaufzeiten, werden die bei der Stadt Cottbus/Chóśebuz am Tag vor dem Übergang erreichten Zeiten so berücksichtigt, als wären sie bei der Stiftung ununterbrochen zurückgelegt worden.

(2) Abweichende arbeitsvertragliche Regelungen bleiben davon unberührt.

#### § 16 Beratungsgremien

Der Stiftungsrat und der Vorstand können sich in allen wichtigen Fragen von einem wissenschaftlichen Beirat beraten lassen.
Der Stiftungsrat kann zur Vorbereitung seiner Entscheidungen und zu seiner sonstigen Beratung Arbeitsgruppen einsetzen.
Das Nähere regelt die Satzung.

#### § 17 Haushaltsplan, Haushaltsjahr und Rechnungsprüfung

(1) Der Haushaltsplan der Stiftung ist alljährlich rechtzeitig vor Beginn des Haushaltsjahres vom Vorstand im Entwurf aufzustellen und vom Stiftungsrat festzustellen.
Er bedarf der Genehmigung der für Kultur zuständigen obersten Landesbehörde.

(2) Das Haushaltsjahr der Stiftung ist das Kalenderjahr.

(3) Für das Haushalts-, Kassen- und Rechnungswesen sowie für die Rechnungsprüfung der Stiftung finden die für die Verwaltung des Landes Brandenburg geltenden Bestimmungen der Landeshaushaltsordnung entsprechend Anwendung.

#### § 18 Stiftungsauflösung

(1) Die Stiftung kann durch Gesetz aufgelöst werden.

(2) Bei Auflösung der durch dieses Gesetz errichteten Stiftung fallen die Vermögensgegenstände, die die Stadt Cottbus/Chóśebuz eingebracht hat, der Stadt Cottbus/Chóśebuz zu.
Die Vermögensgegenstände, die das Land Brandenburg eingebracht hat, fallen bei Stiftungsauflösung dem Land Brandenburg zu.
Über das während des Bestehens der Stiftung hinzugewonnene Vermögen treffen das Land Brandenburg und die Stadt Cottbus/Chóśebuz im Falle der Stiftungsauflösung eine Vereinbarung, nach der das Stiftungsvermögen anteilig und unter Berücksichtigung der finanziellen Förderung dieser Stiftung dem Land Brandenburg und der Stadt Cottbus/Chóśebuz zufällt.

(3) Soweit Vermögen der Stadt Cottbus/Chóśebuz und dem Land Brandenburg zufällt, ist dieses unmittelbar und ausschließlich für gemeinnützige Zwecke im Sinne des Abschnitts „Steuerbegünstigte Zwecke“ der Abgabenordnung und in einer dem Stiftungszweck möglichst nahekommenden Weise für die Erhaltung des Denkmalensembles Park und Schloss Branitz zu verwenden.

#### § 19 Einschränkung von Grundrechten

Durch dieses Gesetz wird das Grundrecht der Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 20 Inkrafttreten

Dieses Gesetz tritt am 1.
Januar 2018 in Kraft.

Potsdam, den 14.
Dezember 2017

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

### Anlagen

1

[Anlage (zu § 3 Absatz 1) - Liegenschaften der Stiftung Fürst-Pückler-Museum Park und Schloss Branitz](/br2/sixcms/media.php/68/GVBl_I_31_2017-Anlage.pdf "Anlage (zu § 3 Absatz 1) - Liegenschaften der Stiftung Fürst-Pückler-Museum Park und Schloss Branitz") 708.0 KB