## Gesetz zu dem Staatsvertrag über die gemeinsame Einrichtung für Hochschulzulassung

Der Landtag hat das folgende Gesetz beschlossen:

### § 1

Dem am 17.
März und 18.
März 2016 in Berlin und am 21.
März 2016 in Wiesbaden unterzeichneten Staatsvertrag über die gemeinsame Einrichtung für Hochschulzulassung wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel 19 Absatz 1 Satz 1 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 27.
Juni 2016

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

### Anlagen

1

[Staatsvertrag über die gemeinsame Einrichtung für Hochschulzulassung](/br2/sixcms/media.php/68/GVBl_I_19_2016_001Anlage-zum-Hauptdokument.pdf "Staatsvertrag über die gemeinsame Einrichtung für Hochschulzulassung") 797.8 KB