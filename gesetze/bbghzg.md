## Gesetz über die Hochschulzulassung im Land Brandenburg (Brandenburgisches Hochschulzulassungsgesetz - BbgHZG)

#### § 1 Anwendungsbereich

(1) Dieses Gesetz regelt die Vergabe von Studienplätzen in zulassungsbeschränkten Studiengängen an den staatlichen Hochschulen des Landes Brandenburg, soweit nicht die Vergabe der Studienplätze durch die Stiftung für Hochschulzulassung im zentralen Vergabeverfahren gemäß Abschnitt 3 des Staatsvertrages über die Errichtung einer gemeinsamen Einrichtung für Hochschulzulassung vom 5. Juni 2008 (GVBl.
I S. 310) nach den dort geregelten Vorschriften (zentrales Vergabeverfahren) erfolgt.

(2) Das Gesetz trifft darüber hinaus Regelungen zur Ausführung des Staatsvertrages über die Errichtung einer gemeinsamen Einrichtung für Hochschulzulassung (Staatsvertrag).

(3) Vom Anwendungsbereich des Gesetzes ausgenommen ist die Vergabe von Studienplätzen an der Hochschule der Polizei des Landes Brandenburg und der Fachhochschule für Finanzen Brandenburg sowie für den Studiengang „Öffentliche Verwaltung Brandenburg“ an der Technischen Hochschule Wildau.

#### § 2 Hochschulzugangsberechtigung

Hochschulzugangsberechtigung im Sinne dieses Gesetzes ist jede Form der Studienberechtigung gemäß § 9 Absatz 2 Satz 1 Nummer 1 bis 11 des Brandenburgischen Hochschulgesetzes.
Als Hochschulzugangsberechtigung im Sinne dieses Gesetzes gilt auch eine bestandene Zugangsprüfung nach § 9 Absatz 1 Satz 5 bis 7 des Brandenburgischen Hochschulgesetzes, wenn eine Rechtsverordnung nach § 9 Absatz 1 Satz 8 des Brandenburgischen Hochschulgesetzes dies vorsieht.

#### § 3 Studienbewerberinnen und Studienbewerber

(1) Die Studienplätze werden an Deutsche sowie an ausländische Staatsangehörige oder Staatenlose, die im Sinne dieses Gesetzes Deutschen gleichgestellt sind, vergeben.
Deutschen gleichgestellt sind Personen, die die Voraussetzungen nach § 9 Absatz 1 Satz 2, 3 oder Satz 5 des Brandenburgischen Hochschulgesetzes erfüllen sowie andere ausländische Staatsangehörige oder Staatenlose, die eine Qualifikation nach § 9 Absatz 2 Satz 1 Nummer 5 bis 11 des Brandenburgischen Hochschulgesetzes im Inland erworben haben.
Wer nach Satz 2 Deutschen gleichgestellt ist, wird nach den für Deutsche geltenden Bestimmungen am Vergabeverfahren beteiligt.

(2) Darüber hinaus werden die Studienplätze an ausländische Staatsangehörige und Staatenlose vergeben, die eine ausländische Hochschulzugangsberechtigung nachweisen.

#### § 4 Vorabquoten

(1) In einem Auswahlverfahren können bis zu 20 Prozent, jedoch nicht weniger als 10 Prozent, der zur Verfügung stehenden Studienplätze vorbehalten werden insbesondere für

1.  Bewerberinnen und Bewerber, für die die Ablehnung des Zulassungsantrags eine außergewöhnliche Härte bedeuten würde,
2.  Bewerberinnen und Bewerber, die sich aufgrund entsprechender Vorschriften verpflichtet haben, ihren Beruf in Bereichen besonderen öffentlichen Bedarfs auszuüben,
3.  ausländische Staatsangehörige und Staatenlose, soweit sie nicht Deutschen gleichgestellt sind,
4.  Bewerberinnen und Bewerber, die bereits ein Studium in einem anderen Studiengang abgeschlossen haben; hierzu zählen nicht Bewerberinnen und Bewerber für konsekutive Masterstudiengänge,
5.  Bewerberinnen und Bewerber, die einem im öffentlichen Interesse zu berücksichtigenden oder zu fördernden Personenkreis angehören und aufgrund begründeter Umstände an den Studienort gebunden sind (Profilquote),
6.  in der beruflichen Bildung Qualifizierte, die über keine Studienberechtigung nach § 9 Absatz 2 Satz 1 Nummer 1 bis 5 des Brandenburgischen Hochschulgesetzes verfügen,
7.  Bewerberinnen und Bewerber, deren Studiengangswahl oder Studienfach-Kombination auf einen Beruf vorbereitet, für den ein besonderer öffentlicher Bedarf festgestellt ist.

(2) Für die Quoten nach Absatz 1 Nummer 4 und 6 kann bestimmt werden, dass der Anteil der Studienplätze an der Gesamtzahl der Studienplätze je Bewerbergruppe nicht größer sein darf als der Anteil der jeweiligen Bewerbergruppe an der Bewerbergesamtzahl.

(3) Nach Absatz 1 nicht in Anspruch genommene Studienplätze werden nach § 6 oder § 7 vergeben.

#### § 5 Auswahlverfahren innerhalb der Vorabquoten

(1) Die Studienplätze nach § 4 Absatz 1 Nummer 1 werden auf Antrag nach dem Grad der außergewöhnlichen Härte vergeben.
Eine außergewöhnliche Härte liegt vor, wenn in der eigenen Person liegende besondere soziale, gesundheitliche oder familiäre Gründe die sofortige Aufnahme des Studiums zwingend erfordern.

(2) Die Studienplätze nach § 4 Absatz 1 Nummer 3 werden in erster Linie nach dem Grad der Qualifikation vergeben.
Besondere Umstände, die für ein Studium an einer deutschen Hochschule sprechen, können berücksichtigt werden.
Als ein solcher Umstand ist insbesondere anzusehen, wenn die Bewerberin oder der Bewerber

1.  von einer deutschen Einrichtung zur Förderung Studierender für ein Studium ein Stipendium erhält,
2.  aufgrund einer Einweisung in ein Studienkolleg oder einen Deutschkurs für die Zuteilung eines Studienplatzes in dem im Zulassungsantrag genannten Studiengang vorgemerkt ist,
3.  an einem strukturierten Studienvorbereitungsprogramm erfolgreich teilgenommen hat,
4.  im Geltungsbereich des Grundgesetzes Asylrecht genießt,
5.  einer deutschsprachigen Minderheit im Ausland angehört.

Verpflichtungen aufgrund zwischenstaatlicher Vereinbarungen oder aufgrund von Vereinbarungen zwischen Hochschulen sind zu berücksichtigen.

(3) Die Studienplätze nach § 4 Absatz 1 Nummer 4 werden nach den Prüfungsergebnissen des Erststudiums und nach der Bedeutung der für die Bewerbung für ein weiteres Studium maßgeblichen Gründe vergeben.

(4) Die Studienplätze nach § 4 Absatz 1 Nummer 5 werden nach auf das jeweilige Profil bezogenen Kriterien vergeben, die die jeweilige Hochschule durch Satzung festlegt.
Der Grad der Qualifikation ist zu berücksichtigen.

(5) Die Studienplätze nach § 4 Absatz 1 Nummer 6 werden in erster Linie nach dem Grad der Qualifikation vergeben.
Weitere Kriterien, die Auskunft über die Eignung für das beabsichtigte Studium geben, sollen berücksichtigt werden.

(6) Die Studienplätze nach § 4 Absatz 1 Nummer 7 werden in einem gesonderten Hochschulauswahlverfahren nach § 6 Absatz 2 oder § 7 Absatz 2 vergeben.

(7) Wer den Quoten nach § 4 Absatz 1 Nummer 2 bis 4 und 6 unterfällt, kann nicht im Verfahren nach § 6 zugelassen werden.

#### § 6 Auswahlverfahren in den Hauptquoten

(1) Die nach Abzug der Quoten gemäß § 4, der aufgrund eines früheren Zulassungsanspruchs Auszuwählenden und der aufgrund der Angehörigkeit zum Bundeskader eines Bundessportfachverbandes des Deutschen Olympischen Sportbundes Auszuwählenden verbleibenden Studienplätze werden zu 80 Prozent im Ergebnis eines Hochschulauswahlverfahrens und im Übrigen nach der Wartezeit vergeben.

(2) Die Hochschule vergibt die Studienplätze im Rahmen des Hochschulauswahlverfahrens

1.  nach dem Grad der Qualifikation (Durchschnittsnote),
2.  nach gewichteten Einzelnoten der Qualifikation, die über die fachspezifische Eignung Auskunft geben,
3.  nach dem Ergebnis eines fachspezifischen Studierfähigkeitstests,
4.  nach der Art der Berufsausbildung oder Berufstätigkeit,
5.  nach der erfolgreichen Teilnahme an einem strukturierten Studienvorbereitungsprogramm,
6.  nach dem Ergebnis eines von der Hochschule zu führenden Gesprächs mit den Bewerberinnen und Bewerbern, das Aufschluss über die Motivation der Bewerberin oder des Bewerbers und über die Identifikation mit dem gewählten Studium und dem angestrebten Beruf geben sowie zur Vermeidung von Fehlvorstellungen über die Anforderungen des Studiums dienen soll.

Bei der Auswahlentscheidung muss dem Grad der Qualifikation ein maßgeblicher Einfluss gegeben werden.
Daneben ist mindestens ein weiteres Auswahlkriterium nach Satz 1 Nummer 2 bis 6 zugrunde zu legen.
An dem Gespräch nach Satz 1 Nummer 6 sollen mindestens zwei Vertreterinnen oder Vertreter der auswählenden Hochschule teilnehmen.
Für die Auswahlverfahren nach Satz 1 Nummer 3 und Nummer 5 werden keine Gebühren erhoben.
Bei Bewerberinnen und Bewerbern für einen Lehramtsstudiengang ist der Nachweis vertiefter Kenntnisse der sorbischen/wendischen Sprache bei der Auswahlentscheidung angemessen zu berücksichtigen.

(3) Die Zahl der Teilnehmerinnen und Teilnehmer an einem Auswahlgespräch gemäß Absatz 2 Satz 1 Nummer 6 kann bis auf das Dreifache der Zahl der im Hochschulauswahlverfahren zu vergebenden Studienplätze begrenzt werden.
In diesem Fall entscheidet die Hochschule über die Teilnahme nach einem der in Absatz 2 Satz 1 Nummer 1 bis 4 genannten Maßstäbe.

(4) Die Hochschule regelt die Einzelheiten des Auswahlverfahrens durch Satzung.
Verfahren und Kriterien sind in der Satzung so zu gestalten, dass niemand mittelbar oder unmittelbar aufgrund des Geschlechts, der ethnischen Herkunft, der Religion oder Weltanschauung, der Behinderung oder der sexuellen Identität diskriminiert wird.
Fehlt eine Regelung der Hochschule zum Auswahlverfahren nach Absatz 2 oder ist eine solche Regelung aufgrund einer rechtskräftigen Entscheidung nicht anzuwenden, so erfolgt die Auswahlentscheidung nach dem Grad der Qualifikation gemäß Absatz 2 Satz 1 Nummer 1 und § 10.

#### § 7 Besondere Regelungen für die Vergabe der Studienplätze in Masterstudiengängen

(1) Bei der Vergabe von Studienplätzen in Masterstudiengängen werden die nach Abzug der Quoten gemäß § 4, der aufgrund eines früheren Zulassungsanspruchs Auszuwählenden und der aufgrund der Angehörigkeit zum Bundeskader eines Bundessportfachverbandes des Deutschen Olympischen Sportbundes Auszuwählenden verbleibenden Studienplätze zu mindestens 80 Prozent, höchstens jedoch zu 90 Prozent im Ergebnis eines Hochschulauswahlverfahrens und im Übrigen nach der Wartezeit vergeben.

(2) Die Hochschule vergibt die Studienplätze im Rahmen des Hochschulauswahlverfahrens

1.  nach der Abschlussnote des ersten Hochschulabschlusses oder, in den Fällen des § 9 Absatz 6 des Brandenburgischen Hochschulgesetzes, nach der vorläufigen Durchschnittsnote,
2.  _(aufgehoben)_
3.  nach gewichteten Einzel- oder Modulnoten der Qualifikation nach Nummer 1, die über die fachspezifische Eignung Auskunft geben,
4.  nach dem Ergebnis eines fachspezifischen Tests,
5.  nach der Art der Berufsausbildung oder Berufstätigkeit,
6.  nach zusätzlichen, außerhalb des Hochschulwesens erworbenen Qualifikationen,
7.  nach besonderen fachlichen Leistungen, die in Bezug zu dem angestrebten Studiengang stehen,
8.  nach dem Ergebnis eines von der Hochschule zu führenden Gesprächs mit den Bewerberinnen und Bewerbern, das Aufschluss über die Motivation der Bewerberin oder des Bewerbers und über die Identifikation mit dem gewählten Studium und dem angestrebten Beruf geben sowie zur Vermeidung von Fehlvorstellungen über die Anforderungen des Studiums dienen soll,
9.  nach weiteren Nachweisen der Bewerberin oder des Bewerbers, die Aufschluss über die Motivation und über die Identifikation mit dem gewählten Studium und dem angestrebten Beruf geben.

Bei der Auswahlentscheidung muss den ausgewiesenen Abschlussnoten oder den vorläufigen Noten nach Satz 1 Nummer 1 ein maßgeblicher Einfluss gegeben werden.
Daneben ist der Auswahlentscheidung mindestens ein weiteres Auswahlkriterium nach Satz 1 Nummer 3 bis 9 zugrunde zu legen.
An dem Gespräch nach Satz 1 Nummer 8 sollen mindestens zwei Vertreterinnen oder Vertreter der auswählenden Hochschule teilnehmen.
Für das Auswahlverfahren nach Satz 1 Nummer 4 wird keine Gebühr erhoben.
Bei Bewerberinnen und Bewerbern für einen Lehramtsstudiengang ist der Nachweis vertiefter Kenntnisse der sorbischen/wendischen Sprache bei der Auswahlentscheidung angemessen zu berücksichtigen.

(3) § 6 Absatz 3 und 4 gilt entsprechend.

(4) Bei Ranggleichheit entscheidet das Los.

#### § 8 Zulassungsverfahren für höhere Fachsemester

(1) Sind für das zweite oder ein höheres Fachsemester Zulassungszahlen festgesetzt, werden freie Studienplätze an deutsche und ausländische Bewerberinnen und Bewerber, die die für das angestrebte Fachsemester erforderlichen Studienzeiten nachweisen, in folgender Reihenfolge vergeben:

1.  an Bewerberinnen und Bewerber, die als Studienanfängerinnen und Studienanfänger in dem Studiengang, für den sie die Zulassung zu einem höheren Fachsemester beantragen, an der Hochschule zugelassen sind (Aufrückende),
2.  an Bewerberinnen und Bewerber, die im gleichen Studiengang an einer Hochschule endgültig und nicht nur auf einen Abschnitt des Studiengangs beschränkt zugelassen und immatrikuliert sind oder waren (Hochschulwechsel, Studienunterbrechung),
3.  an sonstige Bewerberinnen und Bewerber (Quereinstieg, Teilzulassung).

(2) Sofern innerhalb der in Absatz 1 genannten Gruppe von Bewerberinnen und Bewerbern eine Auswahl erforderlich wird, erfolgt die Bestimmung der Rangfolge nach bisherigen Studienleistungen sowie nach wissenschaftlichen und sozialen Gründen unter besonderer Berücksichtigung der Belange von Bewerberinnen und Bewerbern, die einem Bundeskader eines Bundessportfachverbandes des Deutschen Olympischen Sportbundes angehören; im Übrigen entscheidet bei Ranggleichheit das Los.

#### § 9 Auswahlverfahren für besondere Studiengänge

(1) In international ausgerichteten Studiengängen kann die Vorabquote nach § 4 Absatz 1 Nummer 3 die in § 4 Absatz 1 genannte Obergrenze überschreiten.

(2) In Studiengängen, die eine Hochschule gemeinsam mit einer anderen Hochschule betreibt, kann vorgesehen werden, dass ein Teil der Studienplätze durch die andere Hochschule vergeben wird.

(3) In künstlerischen Studiengängen mit einer Eignungsprüfung nach § 9 Absatz 4 Satz 2 oder Absatz 5 Satz 2 des Brandenburgischen Hochschulgesetzes kann die Hochschule die Auswahlentscheidung abweichend von § 5 Absatz 2 und den §§ 6 und 7 überwiegend oder ausschließlich nach dem Ergebnis der Eignungsprüfung treffen.

#### § 10 Grad der Qualifikation

(1) Der Grad der Qualifikation wird durch die Durchschnittsnote der Hochschulzugangsberechtigung bestimmt.
Eine Gesamtnote oder Abschlussnote gilt als Durchschnittsnote nach Satz 1.

(2) Für die Zulassung zum Masterstudium bestimmt sich der Grad der Qualifikation nach der Abschlussnote des ersten Hochschulabschlusses oder, in den Fällen des § 9 Absatz 6 des Brandenburgischen Hochschulgesetzes, nach der vorläufigen Durchschnittsnote.

(3) Werden mehrere einschlägige Hochschulzugangsberechtigungen vorgelegt, soll die Bewerberin oder der Bewerber angeben, auf welche Hochschulzugangsberechtigung der Zulassungsantrag gestützt wird.
Fehlt die Angabe, wird dem Zulassungsantrag die zuerst erworbene Hochschulzugangsberechtigung zugrunde gelegt.

(4) Wer keine Note nach Absatz 1 nachweist, wird hinter die letzte Bewerberin und den letzten Bewerber mit feststellbarer Durchschnittsnote eingeordnet.

(5) Wer nachweist, aus in der eigenen Person liegenden, nicht selbst zu vertretenden Gründen daran gehindert gewesen zu sein, eine bessere Note gemäß Absatz 1 zu erreichen, wird auf Antrag mit der besseren Durchschnittsnote berücksichtigt.

#### § 11 Wartezeit

(1) Die Rangfolge wird durch die Zahl der seit dem Erwerb der Hochschulzugangsberechtigung für das angestrebte Studium gemäß § 9 Absatz 2 des Brandenburgischen Hochschulgesetzes verstrichenen Halbjahre bestimmt.

(2) Wird der Zeitpunkt des Erwerbs der Hochschulzugangsberechtigung nicht nachgewiesen, wird die Zahl der Halbjahre seit dem Erwerb der Hochschulzugangsberechtigung nicht berücksichtigt.

(3) Von der Gesamtzahl der Halbjahre wird die Zahl der Halbjahre abgezogen, in denen die Bewerberin oder der Bewerber an einer deutschen Hochschule als Studentin oder Student eingeschrieben war.

(4) Wer nachweist, aus in der eigenen Person liegenden, nicht selbst zu vertretenden Gründen daran gehindert gewesen zu sein, die Hochschulzugangsberechtigung zu einem früheren Zeitpunkt zu erwerben, wird auf Antrag bei der Ermittlung der Wartezeit mit dem früheren Zeitpunkt des Erwerbs der Hochschulzugangsberechtigung berücksichtigt.

#### § 12 Auswahl aufgrund früheren Zulassungsanspruchs

Bewerberinnen und Bewerber, die eine Dienstpflicht nach Artikel 12a des Grundgesetzes erfüllt oder eine entsprechende oder gleichgestellte Dienstpflicht oder einen entsprechenden oder gleichgestellten Freiwilligendienst übernommen haben oder ein Kind unter 18 Jahren oder eine pflegebedürftige Person aus dem Kreis der sonstigen Angehörigen bis zur Dauer von drei Jahren betreut oder gepflegt haben (Dienst), werden für einen Studiengang zugelassen, wenn sie zu Beginn oder während eines Dienstes für diesen Studiengang bereits zugelassen worden sind oder wenn zu Beginn oder während eines Dienstes für diesen Studiengang keine Zulassungszahlen festgesetzt waren.

#### § 12a Auswahl aufgrund Angehörigkeit zum Bundeskader eines Bundessportfachverbandes des Deutschen Olympischen Sportbundes

Bewerberinnen und Bewerber, die einem Bundeskader eines Bundessportfachverbandes des Deutschen Olympischen Sportbundes angehören, können nach Maßgabe von Satzungen der Hochschulen zur besseren Vereinbarkeit von Studium und Training für den beantragten Studiengang zugelassen werden.
Die Bescheinigung der Angehörigkeit zum Bundeskader erfolgt durch den Olympiastützpunkt Brandenburg.
Die Satzungen sind von der für Hochschulen zuständigen obersten Landesbehörde zu genehmigen.

#### § 13 Nachrangige Auswahlkriterien

(1) Besteht bei der Auswahl nach dem Hochschulauswahlverfahren Ranggleichheit, bestimmt sich die Rangfolge nach den Bestimmungen über die Auswahl nach Wartezeit.
Besteht bei der Auswahl nach Wartezeit Ranggleichheit, bestimmt sich die Rangfolge nach dem Grad der Qualifikation.

(2) Besteht danach noch Ranggleichheit oder besteht bei der Auswahl in den übrigen Quoten Ranggleichheit, wird vorrangig ausgewählt, wer zu dem Personenkreis nach § 12 gehört und durch eine Bescheinigung glaubhaft macht, dass der Dienst in vollem Umfang abgeleistet ist oder bei einer Bewerbung für das Sommersemester bis zum 30.
April und bei einer Bewerbung für das Wintersemester bis zum 31.
Oktober in vollem Umfang abgeleistet sein wird.
Im Übrigen entscheidet bei Ranggleichheit das Los.

#### § 14 Anträge auf Zulassung außerhalb der Kapazität

(1) Ein Antrag, mit dem ein Anspruch auf Zulassung außerhalb der festgesetzten Zulassungszahl geltend gemacht wird, kann nur gestellt werden, wenn in dem gleichen Vergabeverfahren auch ein Antrag auf Zulassung für den betreffenden Studiengang gestellt worden ist.

(2) Sind Zulassungen außerhalb der festgesetzten Zulassungszahl auszusprechen, so hat sich die Vergabe unter den Antragstellern an dem Grad der Qualifikation gemäß § 10 zu orientieren.

#### § 15 Stiftungsrat der Stiftung für Hochschulzulassung

Die Vertreterin oder der Vertreter der Hochschulen des Landes Brandenburg im Stiftungsrat der Stiftung für Hochschulzulassung und ihre oder seine Stellvertreterin oder ihr oder sein Stellvertreter werden von den Präsidentinnen und Präsidenten der Hochschulen nach § 2 Absatz 1 des Brandenburgischen Hochschulgesetzes aus dem Kreis der von den Hochschulen benannten Bewerberinnen oder Bewerber bestimmt.
Jede Hochschule kann eine Bewerberin oder einen Bewerber vorschlagen; die Bewerberinnen oder Bewerber müssen hauptberuflich tätige Mitglieder der Hochschulen sein, von denen sie vorgeschlagen werden.

#### § 16 Verordnungsermächtigung

(1) Das für die Hochschulen zuständige Mitglied der Landesregierung wird ermächtigt, im Benehmen mit den Hochschulen durch Rechtsverordnung Einzelheiten des Bewerbungsverfahrens und des Vergabeverfahrens, der Verteilungs- und Auswahlkriterien sowie der Quoten für bestimmte Bewerbergruppen, insbesondere

1.  die Einzelheiten des Antragsverfahrens, die Form und Frist für die zu stellenden Anträge sowie die Form der Bekanntgabe der Verfahrensergebnisse,
2.  die Anwendung und Durchführung des Serviceverfahrens der Stiftung für Hochschulzulassung nach Artikel 4 des Staatsvertrags,
3.  Einzelheiten zu der Festsetzung der Quoten nach § 4 sowie im Einzelfall weitere Quoten und Abweichungen von der Obergrenze nach § 4 Absatz 1,
4.  die Reihenfolge der Berücksichtigung einzelner Quoten,
5.  Einzelheiten des Auswahlverfahrens für besondere Studiengänge nach § 9,
6.  Maßstäbe für die Umrechnung von Qualifikationen gemäß § 10,
7.  Einzelheiten der Wartezeitberechnung nach § 11,
8.  die Festlegung nachrangiger Auswahlkriterien,
9.  sowie Einzelheiten zu Art, Dauer und Umfang der gleichgestellten Dienste nach § 12

zu regeln.

(2) Die Rechtsverordnung kann auch bestimmen, dass die Hochschulen Einzelheiten des Bewerbungsverfahrens und des Vergabeverfahrens nach Maßgabe der Rechtsverordnung nach Absatz 1 durch Satzung regeln können.

(3) Die Hochschulen können die Teilnahme an studienvorbereitenden Kursen, die nicht Bestandteil eines Hochschulstudiums sind, durch Satzung regeln.

#### § 17 Übergangsbestimmung

Die Sorbisch-/Wendischkenntnisse der Bewerberinnen und Bewerber für einen Lehramtsstudiengang werden spätestens ab der Zulassung zu dem Wintersemester 2019/2020 berücksichtigt.

#### § 18 Einschränkung eines Grundrechts

Durch die §§ 3 bis 9 und 12 bis 14 wird das Grundrecht der Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.