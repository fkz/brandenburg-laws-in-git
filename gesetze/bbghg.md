## Brandenburgisches Hochschulgesetz (BbgHG)

Inhaltsübersicht
----------------

Abschnitt 1  
Allgemeine Bestimmungen
-------------------------------------

[§ 1 Geltungsbereich](#1)  
[§ 2 Hochschulen; Verordnungsermächtigung](#2)  
[§ 3 Aufgaben; Verordnungsermächtigung](#3)  
[§ 4 Freiheit von Lehre, Forschung und Studium in Wissenschaft und Kunst; wissenschaftliche Redlichkeit](#4)  
[§ 5 Rechtsstellung; Aufsicht](#5)  
[§ 6 Staatliche Finanzierung und Körperschaftsvermögen](#6)  
[§ 7 Gleichstellung von Frauen und Männern](#7)  
[§ 8 Theologische Ausbildung an staatlichen Hochschulen](#8)  
[§ 8a Maßnahmen zur Bewältigung einer Notlage; Verordnungsermächtigung](#8a)

Abschnitt 2  
Hochschulzugang und Zulassung, Immatrikulation, Exmatrikulation, Studierendenschaft
-------------------------------------------------------------------------------------------------

[§ 9 Hochschulzugangsberechtigung; Verordnungsermächtigung](#9)  
[§ 10 Studienkolleg; Verordnungsermächtigung](#10)  
[§ 11 Ermittlung der Ausbildungskapazität und Festsetzung von Zulassungszahlen; Verordnungsermächtigung](#11)  
[§ 12 Zentrale Vergabe von Studienplätzen; örtliche Zulassungsbeschränkungen; Verordnungsermächtigung](#12)  
[§ 13 Zulassungshindernisse](#13)  
[§ 14 Immatrikulation und Exmatrikulation; Verordnungsermächtigung](#14)  
[§ 15 Ordnungsverstöße; Ordnungsverfahren](#15)  
[§ 16 Studierendenschaft](#16)

Abschnitt 3  
Studium, Lehre, Prüfungen
---------------------------------------

[§ 17 Ziel des Studiums; Studienreform](#17)  
[§ 18 Studiengänge](#18)  
[§ 19 Studienordnungen](#19)  
[§ 20 Studienberatung](#20)  
[§ 21 Prüfungen](#21)  
[§ 22 Prüfungsordnungen für Hochschulprüfungen; Verordnungsermächtigung](#22)  
[§ 23 Rahmenordnungen für Studium, Prüfungen, Zugang und Zulassung](#23)  
[§ 24 Einstufungsprüfung; Anerkennung von Leistungen](#24)  
[§ 25 Wissenschaftliche Weiterbildung](#25)  
[§ 26 Lehrangebot](#26)  
[§ 27 Qualitätssicherung; Evaluation der Lehre](#27)

Abschnitt 4  
Hochschulgrade, Promotion und Habilitation
--------------------------------------------------------

[§ 28 Hochschulgrade](#28)  
[§ 29 Verleihung und Führung von Graden](#29)  
[§ 30 Ausländische Hochschulgrade](#30)  
[§ 31 Promotion](#31)  
[§ 32 Habilitation](#32)  
[§ 33 Förderung des wissenschaftlichen und künstlerischen Nachwuchses; Verordnungsermächtigung](#33)  
[§ 34 Ordnungswidrigkeiten, Ordnungsmaßnahmen](#34)

Abschnitt 5  
Forschung
-----------------------

[§ 35 Aufgaben und Koordination der Forschung](#35)  
[§ 36 Forschung und Lehre mit Mitteln Dritter](#36)

Abschnitt 6  
Personal der Hochschule
-------------------------------------

### Unterabschnitt 1  
Allgemeine Bestimmungen

[§ 37 Dienstrechtliche Zuordnung der Hochschulbediensteten; Verordnungsermächtigung](#37)  
[§ 38 Verarbeitung personenbezogener Daten; Verordnungsermächtigung](#38)

### Unterabschnitt 2  
Hauptberufliches wissenschaftliches und künstlerisches Personal der Hochschule

[§ 39 Personalkategorien](#39)  
[§ 40 Berufung von Hochschullehrerinnen und Hochschullehrern; Verordnungsermächtigung](#40)  
[§ 41 Einstellungsvoraussetzungen für Professorinnen und Professoren](#41)  
[§ 42 Dienstrechtliche Aufgaben der Hochschullehrerinnen und Hochschullehrer](#42)  
[§ 43 Dienstrechtliche Stellung der Professorinnen und Professoren](#43)  
[§ 44 Dienstrechtliche Sonderregelungen](#44)  
[§ 45 Einstellungsvoraussetzungen für Juniorprofessorinnen und Juniorprofessoren](#45)  
[§ 46 Dienstrechtliche Stellung der Juniorprofessorinnen und Juniorprofessoren](#46)  
[§ 47 Hochschullehrerinnen und Hochschullehrer mit Schwerpunktbildung in der Lehre oder Forschung](#47)  
[§ 48 Führung der Bezeichnung „Professorin“ oder „Professor“](#48)  
[§ 49 Akademische Mitarbeiterinnen und Mitarbeiter](#49)  
[§ 50 Lehrverpflichtung; Verordnungsermächtigung](#50)  
[§ 51 Nebentätigkeit; Verordnungsermächtigung](#51)  
[§ 52 Gastprofessorinnen und Gastprofessoren und Gastdozentinnen und Gastdozenten](#52)

### Unterabschnitt 3  
Nebenberufliches wissenschaftliches und künstlerisches Personal

[§ 53 Personalkategorien](#53)  
[§ 54 Nebenberufliche Professorinnen und Professoren](#54)  
[§ 55 Honorarprofessorinnen und Honorarprofessoren und Ehrenprofessorinnen und Ehrenprofessoren](#55)  
[§ 56 Privatdozentinnen und Privatdozenten](#56)  
[§ 57 Außerplanmäßige Professorinnen und Professoren](#57)  
[§ 58 Lehrbeauftragte](#58)  
[§ 59 Wissenschaftliche und künstlerische Hilfskräfte](#59)

Abschnitt 7  
Mitgliedschaft und Mitwirkung
-------------------------------------------

[§ 60 Mitglieder und Angehörige](#60)  
[§ 61 Allgemeine Grundsätze der Mitwirkung](#61)  
[§ 62 Wahlen](#62)  
[§ 63 Öffentlichkeit](63)

Abschnitt 8  
Zentrale Hochschulorganisation
--------------------------------------------

[§ 64 Zentrale Hochschulorgane](#64)  
[§ 65 Präsidentin oder Präsident](#65)  
[§ 66 Hauptberufliche Vizepräsidentin oder hauptberuflicher Vizepräsident](#66)  
[§ 67 Kanzlerin oder Kanzler](#67)  
[§ 68 Zentrale und dezentrale Gleichstellungsbeauftragte](#68)  
[§ 69 Beauftragte oder Beauftragter für die Belange von Hochschulmitgliedern mit Behinderungen](#69)  
[§ 70 Hochschulbibliothek](#70)

Abschnitt 9  
Dezentrale Hochschulorganisation
----------------------------------------------

[§ 71 Organisatorische Grundeinheiten; Verordnungsermächtigung](#71)  
[§ 72 Organe des Fachbereichs](#72)  
[§ 73 Wahl und Aufgaben der Dekanin oder des Dekans](#73)

Abschnitt 10  
Wissenschaftliche Einrichtungen
----------------------------------------------

[§ 74 Aufgaben; Einrichtung; Organisation wissenschaftlicher Einrichtungen](#74)  
[§ 75 Wissenschaftliche Einrichtungen und Betriebseinheiten für mehrere Hochschulen](#75)  
[§ 76 Wissenschaftliche Einrichtungen an der Hochschule](#76)

Abschnitt 11  
Landeshochschulrat
---------------------------------

[§ 77 Organisation und Aufgaben](#77)

Abschnitt 12  
Studentenwerke
-----------------------------

[§ 78 Organisation; Rechtsstellung; Aufgaben; Verordnungsermächtigung](#78)  
[§ 79 Verwaltungsrat](#79)  
[§ 80 Geschäftsführung](#80)  
[§ 81 Finanzierung, Wirtschaftsführung und Rechnungswesen](#81)  
[§ 82 Aufsicht](#82)

Abschnitt 13  
Anerkennung von Hochschulen und Berufsakademien
--------------------------------------------------------------

[§ 83 Anerkennung](#83)  
[§ 84 Anerkennungsverfahren](#84)  
[§ 85 Folgen der Anerkennung](#85)  
[§ 86 Verlust der Anerkennung](#86)  
[§ 86a Staatliche Anerkennung als Hochschulklinik; Verordnungsermächtigung](#86a)  
[§ 87 Berufsakademien](#87)  
[§ 88 Abschlussbezeichnungen](#88)  
[§ 89 Verlust der staatlichen Anerkennung](#89)  
[§ 90 Ordnungswidrigkeiten](#90)

Abschnitt 14  
Übergangsbestimmungen
------------------------------------

[§ 91 Übergangsbestimmungen zur Organisationsstruktur](#91)  
[§ 92 Überleitung des wissenschaftlichen und künstlerischen Personals](#92)  
[§ 93 Übergangsbestimmungen für bestimmte Dienstverhältnisse](#93)  
[§ 94 Übergangsbestimmung zu den Ordnungswidrigkeiten](#94)

Abschnitt 1   
Allgemeine Bestimmungen
--------------------------------------

#### § 1   
Geltungsbereich

(1) Dieses Gesetz gilt für die staatlichen Hochschulen des Landes Brandenburg und, soweit dies in den §§ 83 bis 90 bestimmt ist, für die staatlich anerkannten Hochschulen und Berufsakademien sowie für die Studentenwerke im Land Brandenburg.

(2) Fachhochschulen, die ausschließlich Studiengänge für den öffentlichen Dienst anbieten, müssen die Anforderungen des § 83 Absatz 2 Satz 1 Nummer 2 und 5 erfüllen.
Im Übrigen findet dieses Gesetz auf sie keine Anwendung.

#### § 2   
Hochschulen; Verordnungsermächtigung

(1) Staatliche Hochschulen nach § 1 Absatz 1 sind:

1.  die Universitäten Potsdam, Frankfurt (Oder) und die Brandenburgische Technische Universität Cottbus-Senftenberg,
    
2.  die Hochschule für Film und Fernsehen Potsdam-Babelsberg als Kunsthochschule sowie
    
3.  die Fachhochschulen Brandenburg, Eberswalde, Potsdam und Wildau.
    Die Fachhochschulen können die Bezeichnung „Hochschule“ verwenden.
    

Das für Hochschulen zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung die typisierende Bezeichnung einer Hochschule zu ändern.
Die Hochschulen können in der Grundordnung geeignete Namenszusätze bestimmen.

(2) Die Errichtung, die Zusammenlegung und die Schließung von Hochschulen des Landes erfolgt durch Gesetz.

(3) Einrichtungen des Bildungswesens, die nichtstaatliche Hochschulen gemäß Absatz 1 Satz 1 sind, können als Hochschulen nach § 83 staatlich anerkannt werden.

#### § 3   
Aufgaben; Verordnungsermächtigung

(1) Die Hochschulen dienen der Pflege und Entwicklung der Wissenschaften und Künste durch Lehre, Forschung, Studium und Weiterbildung.
Sie bereiten auf berufliche Tätigkeiten einschließlich unternehmerischer Selbstständigkeit vor, die die Anwendung wissenschaftlicher Erkenntnisse und wissenschaftlicher Methoden oder die Fähigkeit zu künstlerischer Gestaltung erfordern.
Sie betreiben Wissens- und Technologietransfer zur Umsetzung und Nutzung ihrer Forschungs- und Entwicklungsergebnisse in der Praxis und wirken untereinander und mit anderen Wissenschaftseinrichtungen sowie der Wirtschaft zusammen.
Die Fachhochschulen erfüllen ihre Aufgaben nach den Sätzen 1 und 2 insbesondere durch anwendungsbezogene Lehre und entsprechende Forschung.

(2) Die Hochschulen stellen für einen Zeitraum von fünf Jahren Struktur- und Entwicklungspläne, einschließlich der Personalentwicklung, auf und schreiben sie regelmäßig fort.
Sie sind dabei an staatliche Zielsetzungen der Hochschulentwicklung gebunden, die das für die Hochschulen zuständige Mitglied der Landesregierung nach Anhörung der Hochschulen zur Sicherung eines angemessenen Angebots an Hochschulleistungen vorgibt.
In den Struktur- und Entwicklungsplänen stellen die Hochschulen die vorgesehene fachliche, strukturelle, personelle und finanzielle Entwicklung dar.
Die Struktur- und Entwicklungsplanung soll ein fachlich ausreichendes und regional ausgewogenes Angebot in Forschung und Lehre sicherstellen.
Die Struktur- und Entwicklungspläne sind der für die Hochschulen zuständigen obersten Landesbehörde anzuzeigen.

(3) Die Hochschulen fördern den wissenschaftlichen und künstlerischen Nachwuchs.
Ihre Arbeit in der Forschung und bei der Förderung des wissenschaftlichen und künstlerischen Nachwuchses soll regelmäßig bewertet werden.
Sie evaluieren regelmäßig die Leistungen in der Lehre.

(4) Die Hochschulen tragen den besonderen Belangen von Hochschulmitgliedern mit Kindern oder mit Pflegepflichten Rechnung.
Sie wirken an der sozialen Förderung der Studierenden mit.
Sie fördern in ihrem Bereich kulturelle und musische Belange sowie den Sport.
Sie berücksichtigen die besonderen Bedürfnisse behinderter Hochschulmitglieder und treffen in allen Bereichen die erforderlichen Maßnahmen zu ihrer Integration.
Für die Durchführung des Studiums und der Prüfungen sind dabei geeignete Maßnahmen zu ergreifen, die unter Wahrung der Gleichwertigkeit einen Nachteilsausgleich und die diskriminierungsfreie und gleichberechtigte Teilhabe am Studium gewährleisten.

(5) Die Hochschulen fördern die internationale, insbesondere die europäische Zusammenarbeit im Hochschulbereich; sie fördern den Austausch mit ausländischen Hochschulen und anderen wissenschaftlichen oder künstlerischen Einrichtungen.
Sie berücksichtigen die besonderen Bedürfnisse der ausländischen Studierenden.

(6) Die Hochschulen tragen dem berechtigten Interesse ihres Personals auf angemessene Beschäftigungsbedingungen Rechnung.

(7) Die Hochschulen informieren die Öffentlichkeit über ihre Vorhaben und die Erfüllung ihrer Aufgaben.
Sie berichten regelmäßig über ihre Lehr- und Forschungstätigkeit einschließlich der Gegenstände, den Umfang und die Herkunft der Mittel Dritter sowie über Ergebnisse von Maßnahmen zur Frauen- und Familienförderung.

(8) Andere als die in diesem Gesetz genannten Aufgaben dürfen den Hochschulen durch Rechtsverordnung des für die Hochschulen zuständigen Mitglieds der Landesregierung nur übertragen werden, wenn sie mit den in Absatz 1 genannten Aufgaben zusammenhängen.

#### § 4   
Freiheit von Lehre, Forschung und Studium in Wissenschaft und Kunst; wissenschaftliche Redlichkeit

(1) Die Freiheit der Lehre umfasst im Rahmen der zu erfüllenden Lehraufgaben die inhaltliche und methodische Gestaltung von Lehrveranstaltungen sowie das Recht auf Äußerung von wissenschaftlichen und künstlerischen Lehrmeinungen.
Entscheidungen von Hochschulorganen zur Lehre sind insoweit zulässig, als sie sich auf die Organisation des Lehrbetriebes und auf die Aufstellung und Einhaltung von Studien- und Prüfungsordnungen beziehen; sie dürfen die Freiheit im Sinne von Satz 1 nicht beeinträchtigen.

(2) Die Freiheit der Forschung umfasst insbesondere die Fragestellung, die Grundsätze der Methodik sowie die Bewertung des Forschungsergebnisses und seine Verbreitung.
Entscheidungen von Hochschulorganen zur Forschung sind insoweit zulässig, als sie sich auf die Organisation des Forschungsbetriebes, die Förderung und Abstimmung von Forschungsvorhaben und auf die Bildung von Forschungsschwerpunkten beziehen.
Sie dürfen die Freiheit im Sinne von Satz 1 nicht beeinträchtigen.
Die Sätze 1 bis 3 gelten für künstlerische Entwicklungsvorhaben und für die Kunstausübung entsprechend.

(3) Die Freiheit des Studiums umfasst, unbeschadet der Studien- und Prüfungsordnungen, insbesondere die freie Wahl von Lehrveranstaltungen, das Recht, innerhalb eines Studienganges Schwerpunkte nach eigener Wahl zu bestimmen sowie die Erarbeitung und Äußerung wissenschaftlicher und künstlerischer Meinungen.
Entscheidungen von Hochschulorganen zum Studium sind insoweit zulässig, als sie sich auf die Organisation und ordnungsgemäße Durchführung des Lehr- und Studienbetriebes und auf die Gewährleistung eines ordnungsgemäßen Studiums beziehen.

(4) Die Wahrnehmung der in den Absätzen 1 bis 3 genannten Rechte entbindet nicht von der Beachtung der Regelungen, die das Zusammenleben an der Hochschule ordnen.

(5) Alle an der Hochschule wissenschaftlich Tätigen einschließlich der Promovierenden sowie die Studierenden sind zu wissenschaftlicher Redlichkeit verpflichtet.
Im Rahmen der Selbstkontrolle in der Wissenschaft stellen die Hochschulen Regeln zur Einhaltung der allgemein anerkannten Grundsätze guter wissenschaftlicher Praxis und zum Umgang mit wissenschaftlichem Fehlverhalten auf.
Arbeitsverträge des wissenschaftlichen Personals sollen zur wissenschaftlichen Redlichkeit verpflichten.
Bei der Veröffentlichung von Forschungsergebnissen sind Mitarbeiterinnen und Mitarbeiter, die einen eigenen wissenschaftlichen oder wesentlichen sonstigen Beitrag geleistet haben, als Mitautorinnen und Mitautoren zu nennen; soweit möglich, ist ihr Beitrag zu kennzeichnen.

#### § 5   
Rechtsstellung; Aufsicht

(1) Die staatlichen Hochschulen sind Körperschaften des öffentlichen Rechts und zugleich staatliche Einrichtungen.
Sie haben das Recht der Selbstverwaltung im Rahmen der Gesetze und regeln ihre Angelegenheiten durch die Grundordnung und sonstige Satzungen.
Die Grundordnung bedarf der Genehmigung der für die Hochschulen zuständigen obersten Landesbehörde.
Die Satzungen sind im amtlichen Mitteilungsblatt der Hochschule bekannt zu machen.
Sie sollen zusätzlich in elektronischer Form veröffentlicht werden.

(2) Hochschulen können abweichend von Absatz 1 Satz 1 auch in anderer Rechtsform errichtet, auf Antrag der Hochschule in eine andere Rechtsform umgewandelt oder in die Trägerschaft einer anderen juristischen Person überführt werden.
Das Nähere ist in einem besonderen Gesetz zu regeln.

(3) Die Hochschulen erfüllen die Aufgaben, auch soweit es sich um staatliche Angelegenheiten handelt, durch eine Einheitsverwaltung.
Staatliche Angelegenheiten sind die Personal-, Haushalts- und Finanzverwaltung der Hochschulen, die Hochschulzulassung und die Erhebung von Gebühren.

(4) Die Hochschulen können durch Satzung Gebühren für besondere Aufwendungen und für die Benutzung ihrer Einrichtungen erheben; die Satzung ist der für die Hochschulen zuständigen obersten Landesbehörde vor Inkrafttreten anzuzeigen.
Fordert sie nicht innerhalb von drei Monaten nach Vorlage zu Änderungen auf, tritt die Satzung nach Veröffentlichung im amtlichen Mitteilungsblatt der Hochschule in Kraft.
Für ein Studium bis zum ersten berufsqualifizierenden Abschluss und für ein Studium in einem konsekutiven Studiengang, der zu einem weiteren berufsqualifizierenden Hochschulabschluss führt, werden Studiengebühren nicht erhoben.

(5) Die Hochschulen unterstehen der Rechtsaufsicht der für die Hochschulen zuständigen obersten Landesbehörde.
Diese kann sich zur Erfüllung ihrer Aufgaben jederzeit über die Angelegenheiten der Hochschulen umfassend informieren, insbesondere mündliche und schriftliche Berichte sowie Akten und sonstige Unterlagen anfordern oder einsehen.
Sie kann rechtswidrige Beschlüsse und Maßnahmen der Hochschule beanstanden und ihre Aufhebung oder Änderung innerhalb einer angemessenen Frist verlangen; die Beanstandung hat aufschiebende Wirkung.
Kommt die Hochschule der Beanstandung oder einer Anordnung nicht fristgemäß nach oder erfüllt sie die ihr sonst obliegenden Pflichten nicht innerhalb der vorgeschriebenen Frist, kann die für die Hochschulen zuständige oberste Landesbehörde die notwendigen Maßnahmen an ihrer Stelle treffen sowie die erforderlichen Satzungen erlassen.
Einer Fristsetzung bedarf es nicht, wenn die Hochschule die Befolgung einer Beanstandung oder Anordnung oder die Erfüllung einer ihr obliegenden Pflicht verweigert oder ihre Gremien beschlussunfähig sind.

(6) Bei der Wahrnehmung staatlicher Angelegenheiten unterstehen die Hochschulen der Fachaufsicht der für die Hochschulen zuständigen obersten Landesbehörde.
Das Gleiche gilt, soweit die Hochschulen Aufgaben bei der Ermittlung der Ausbildungskapazität und der Festsetzung von Zulassungszahlen wahrnehmen.
Vor einer Weisung soll der Hochschule Gelegenheit zur Stellungnahme gegeben werden.

(7) Das für die Hochschulen zuständige Mitglied der Landesregierung kann insbesondere zur Umsetzung der staatlichen Hochschulentwicklungsplanung mit den einzelnen Hochschulen Hochschulverträge und andere Ziel- und Leistungsvereinbarungen über Aufgabenwahrnehmung und Entwicklung der Hochschulen mit einer mehrjährigen Laufzeit treffen.
Darin können Regelungen zur staatlichen Finanzierung im Rahmen des Haushaltsrechts, messbare und überprüfbare Ziele, die Prüfung des Umsetzungsstands der Vereinbarungen sowie die Folgen von nicht erreichten Zielen festgelegt werden.

#### § 6   
Staatliche Finanzierung und Körperschaftsvermögen

(1) Die staatliche Finanzierung der staatlichen Hochschulen orientiert sich an den in Lehre und Forschung sowie bei der Förderung des wissenschaftlichen und künstlerischen Nachwuchses erbrachten Leistungen.
Dabei sind auch Fortschritte bei der Erfüllung des Gleichstellungsauftrages zu berücksichtigen.
Die staatliche Anerkennung nichtstaatlicher Hochschulen gemäß § 83 begründet keinen Anspruch auf staatliche Zuschüsse.

(2) Die Hochschulen im Sinne des § 5 Absatz 1 Satz 1 können Körperschaftsvermögen bilden.
Das Körperschaftsvermögen der Hochschulen besteht aus den nichtstaatlichen Mitteln und den mit nichtstaatlichen Mitteln erworbenen Gegenständen.

(3) Einnahmen der Körperschaft sind die Erträge des Vermögens der Körperschaft und Zuwendungen Dritter an die Körperschaft.
Zuwendungen fallen in das Körperschaftsvermögen, es sei denn, die Zuwendungsgeberin oder der Zuwendungsgeber hat dies ausgeschlossen oder sie werden zur Finanzierung von Forschungsvorhaben und Lehrvorhaben gewährt.
Die Hochschule verwaltet das Körperschaftsvermögen getrennt vom Landesvermögen.

(4) Rechtsgeschäfte zu Lasten des Körperschaftsvermögens sind unter dem Namen der Hochschule mit dem Zusatz „für das Körperschaftsvermögen“ abzuschließen.
Aus solchen Rechtsgeschäften wird das Land weder berechtigt noch verpflichtet.
Die Hochschule darf keine Rechtsgeschäfte vornehmen, die geeignet sind, das Ansehen des Landes oder der Hochschule zu beeinträchtigen oder die mit erheblichen finanziellen Risiken verbunden sind.

(5) Die Landeshaushaltsordnung in der Fassung der Bekanntmachung vom 21. April 1999 (GVBl.
I S. 106), die zuletzt durch Artikel 2 des Gesetzes vom 19. Dezember 2011 (GVBl.
I Nr. 35 S. 4) geändert worden ist, findet mit Ausnahme der §§ 7, 23, 39, 44 und 55 der Landeshaushaltsordnung auf das Körperschaftsvermögen keine Anwendung.
Die Bewirtschaftung des Körperschaftsvermögens unterliegt der Prüfung durch den Landesrechnungshof nach § 111 der Landeshaushaltsordnung.

#### § 7   
Gleichstellung von Frauen und Männern

(1) Die Hochschulen fördern die tatsächliche Gleichstellung von Frauen und Männern und wirken bei der Wahrnehmung aller Aufgaben der Hochschule auf die Beseitigung bestehender Nachteile sowie auf die tatsächliche Vereinbarkeit von Beruf, Studium und Familie hin.
Bei allen Vorschlägen und Entscheidungen der Hochschulen sowie ihrer Organe und Gremien sind die geschlechtsspezifischen Auswirkungen zu beachten (Gender Mainstreaming).

(2) Zur Durchsetzung der Gleichstellung von Frauen und Männern werden Frauen unter Beachtung des Vorrangs von Eignung, Befähigung und fachlicher Leistung gefördert.
Ziel der Förderung ist vor allem die Erhöhung des Anteils der Frauen in Wissenschaft und Kunst.
Die Hochschulen sind verpflichtet, geeignete Maßnahmen zur Zielerreichung und zur Beseitigung bestehender Nachteile für Frauen nachzuweisen.

(3) Für jede Hochschule sind ein Gleichstellungskonzept und gegebenenfalls dezentrale Gleichstellungspläne zu erstellen, die den Abbau von Unterrepräsentanz von Frauen zum Gegenstand haben.
Unterrepräsentanz liegt dann vor, wenn in Besoldungs- oder Entgeltgruppen sowie Funktionen mit Vorgesetzten- und Leitungsaufgaben weniger Frauen als Männer beschäftigt sind.
Das Gleichstellungskonzept und die dezentralen Gleichstellungspläne sind einvernehmlich von der Präsidentin oder dem Präsidenten und der zuständigen Gleichstellungsbeauftragten zu erstellen.
Der Inhalt soll sich an § 6 des Landesgleichstellungsgesetzes vom 4. Juli 1994 (GVBl.
I S. 254), das zuletzt durch Artikel 1 des Gesetzes vom 5. Dezember 2013 (GVBl.
I Nr. 35; 2014 I Nr. 1) geändert worden ist, orientieren.

(4) Bei Einstellungen, Höhergruppierungen und Beförderungen ist auf eine Erhöhung des Frauenanteils hinzuwirken und die Situation von Personen mit besonderen familiären Belastungen zu berücksichtigen.
Solange eine Unterrepräsentanz von Frauen in der maßgeblichen Besoldungs- oder Entgeltgruppe oder in Funktionen mit Vorgesetzten- und Leitungsaufgaben besteht, sind Bewerbungen von Frauen unter Beteiligung der Gleichstellungsbeauftragten mit konkreten Maßnahmen aktiv zu fördern.
Weiterhin sind in diesem Fall Bewerberinnen

1.  grundsätzlich zur persönlichen Vorstellung einzuladen, sofern sie die für die Stelle erforderliche Qualifikation besitzen (ist die Zahl der Bewerberinnen zu groß, so sind mindestens ebenso viele Frauen wie Männer zur persönlichen Vorstellung einzuladen), und
    
2.  bei gleichwertiger Eignung, Befähigung und fachlicher Leistung bevorzugt zu berücksichtigen, sofern nicht in der Person eines Mitbewerbers liegende Gründe überwiegen.
    

§ 9 Absatz 2 bis 4 des Landesgleichstellungsgesetzes ist zu beachten.

(5) Maßnahmen zur Gleichstellung von Frauen und Männern können auch im Rahmen von Zielvereinbarungen berücksichtigt werden.
Bei Fortbildungen ist § 11 des Landesgleichstellungsgesetzes zu beachten.

(6) Frauen und Männer führen Funktions-, Status- und andere Bezeichnungen nach diesem Gesetz in geschlechtsspezifischer Form.
Im dienstlichen Schriftverkehr und in rechtsverbindlichen Dokumenten der Hochschule ist bei der Formulierung besonders auf die sprachliche Gleichbehandlung von Frauen und Männern zu achten.

(7) Die für die Hochschulen zuständige oberste Landesbehörde schließt dem Bericht der Landesregierung gemäß § 26 des Landesgleichstellungsgesetzes einen Bericht zur Verwirklichung der Gleichstellung im Hochschulbereich an.

#### § 8   
Theologische Ausbildung an staatlichen Hochschulen

(1) Die Einführung und Änderung von theologischen Studiengängen bedürfen der Zustimmung der Kirche oder Religionsgemeinschaft, deren Bekenntnis die angestrebte Ausbildung entspricht (kooperierende Kirche oder Religionsgemeinschaft).
Die Einführung von theologischen Studiengängen erfolgt im Benehmen mit dem zuständigen Ausschuss des Landtages.
Die Aufhebung von theologischen Studiengängen erfolgt nach vorheriger Anhörung der kooperierenden Kirche oder Religionsgemeinschaft mit dem Ziel des Einvernehmens.

(2) Erlass und Änderung von Studien-, Prüfungs-, Promotions- und Habilitationsordnungen in einem theologischen Studiengang oder Fach bedürfen grundsätzlich der Zustimmung der kooperierenden Kirche oder Religionsgemeinschaft.
Diese kann die Zustimmung nur aus Gründen verweigern, die sich auf Lehre oder Bekenntnis beziehen.
Die Zustimmung gilt als erteilt, wenn sie nicht binnen drei Monaten nach Zugang des Ersuchens auf Zustimmung bei der kooperierenden Kirche oder Religionsgemeinschaft verweigert worden ist.

(3) Bietet eine Hochschule ein Studium in bekenntnisgebundener Theologie an, so ist vor jeder Berufung auf eine theologische Professur oder Juniorprofessur von der Hochschule über die für die Hochschulen zuständige oberste Landesbehörde die Zustimmung zu dem Berufungsvorschlag von der Kirche oder der in der theologischen Ausbildung kooperierenden Religionsgemeinschaft einzuholen.
Absatz 2 Satz 2 und 3 gilt entsprechend.
Für die Bestellung von Honorarprofessorinnen und Honorarprofessoren und außerplanmäßigen Professorinnen und Professoren gilt Entsprechendes.

(4) Erfüllt eine Hochschullehrerin oder ein Hochschullehrer nicht mehr die Voraussetzungen für die Lehrtätigkeit in einer theologischen Ausbildung, insbesondere weil sie oder er einem anderen Bekenntnis folgt, so wird sie oder er auf Antrag der kooperierenden Kirche oder Religionsgemeinschaft nach Feststellung der Voraussetzungen in ein geeignetes gleichwertiges Amt der Hochschule versetzt.

(5) Die Art und Weise der Mitwirkung der kooperierenden Kirche oder Religionsgemeinschaft sollen durch öffentlich-rechtliche Vereinbarung zwischen dieser und der Hochschule mit Zustimmung der für die Hochschulen zuständigen obersten Landesbehörde geregelt werden.

(6) Regelungen in Staatsverträgen mit den Kirchen oder Religionsgemeinschaften bleiben unberührt.

#### § 8a   
Maßnahmen zur Bewältigung einer Notlage; Verordnungsermächtigung

(1) Für Fälle einer Notlage, in denen eine reguläre Aufgabenwahrnehmung durch die staatlichen Hochschulen ganz oder teilweise nicht möglich ist, wird das für die Hochschulen zuständige Mitglied der Landesregierung zur Sicherstellung des Studiums, zur Sicherstellung der Funktionsfähigkeit der Organisation der Hochschulen und der Studierendenschaften und zum Schutz der Grundrechte der Mitglieder der Hochschulen sowie der Studienbewerberinnen und -bewerber ermächtigt, im Einvernehmen mit dem für Wissenschaft zuständigen Ausschuss des Landtages durch Rechtsverordnung Regelungen betreffend die Immatrikulation und Exmatrikulation, das Studium, die Lehre, die Prüfungen und die Anerkennung von Leistungen, die Verfahrensgrundsätze hinsichtlich der Sitzungen und der Beschlüsse, die Amtszeit der Organe und Gremien der Hochschule und der Studierendenschaft zu erlassen und dabei von den Regelungen des § 14, des § 16 Absatz 2 Satz 1 und 2, der §§ 18 bis 22, des § 24, des § 26 Absatz 1, des § 61 Absatz 2 Satz 1, des § 62 und des § 63 abzuweichen.
Die Geltungsdauer der Rechtsverordnung ist auf den zur Zweckerreichung notwendigen Zeitraum zu befristen und kann in diesem Rahmen im Einvernehmen mit dem für Wissenschaft zuständigen Ausschuss des Landtages verlängert werden.
Die Rechtsverordnung ist aufzuheben, sobald die Voraussetzungen für ihren Erlass weggefallen sind.

(2) Das für die Hochschulen zuständige Mitglied der Landesregierung berichtet dem zuständigen Ausschuss des Landtages hinsichtlich der Rechtsverordnung fortlaufend über die ihren Fortbestand rechtfertigenden Gründe.

(3) Die Hochschulen können für die Dauer der Notlage, deren Vorliegen das für die Hochschulen zuständige Mitglied der Landesregierung festgestellt hat, in ihren Rahmenordnungen nach § 23 Abweichungen von den geltenden Regelungen treffen, soweit dies zur Abmilderung von Folgen der Notlage erforderlich ist.
Der Genehmigung durch die für die Hochschulen zuständigen obersten Landesbehörde bedarf es nicht, die Änderungssatzung ist ihr anzuzeigen.

(4) Notlagen im Sinne dieser Vorschrift sind insbesondere Fälle einer festgestellten epidemischen Lage von nationaler Tragweite oder Katastrophen im Sinne des § 1 Absatz 2 Nummer 2 des Brandenburgischen Brand- und Katastrophenschutzgesetzes.

Abschnitt 2   
Hochschulzugang und Zulassung, Immatrikulation, Exmatrikulation, Studierendenschaft
--------------------------------------------------------------------------------------------------

#### § 9   
Hochschulzugangsberechtigung; Verordnungsermächtigung

(1) Deutsche sind zu dem von ihnen gewählten Hochschulstudium berechtigt, wenn sie die für das Studium erforderliche Qualifikation nachweisen.
Ausländische Staatsangehörige oder Staatenlose, die eine Qualifikation nach Absatz 2 Satz 1 Nummer 1 bis 4 im Inland oder an einer deutschen Auslandsschule nicht ausschließlich nach ausländischem Recht erworben haben, sind Deutschen gleichgestellt.
Deutschen gleichgestellt sind auch:

1.  Staatsangehörige eines anderen Mitgliedstaates der Europäischen Union oder eines anderen Vertragsstaates des Abkommens über den Europäischen Wirtschaftsraum,
    
2.  in der Bundesrepublik Deutschland wohnende Kinder von Staatsangehörigen eines anderen Mitgliedstaates der Europäischen Union oder eines anderen Vertragsstaates des Abkommens über den Europäischen Wirtschaftsraum, sofern diese Staatsangehörigen in der Bundesrepublik Deutschland beschäftigt sind oder gewesen sind,
    
3.  in der Bundesrepublik Deutschland wohnende andere Familienangehörige im Sinne des Artikels 2 Nummer 2 der Richtlinie 2004/38/EG des Europäischen Parlaments und des Rates vom 29. April 2004 von Staatsangehörigen eines anderen Mitgliedstaates der Europäischen Union oder eines anderen Vertragsstaates des Abkommens über den Europäischen Wirtschaftsraum, sofern diese Staatsangehörigen in der Bundesrepublik Deutschland beschäftigt sind,
    

wenn sie die für das Studium erforderlichen Sprachkenntnisse nachweisen.
Bewerberinnen und Bewerber ohne die für das Studium erforderlichen Sprachkenntnisse können vorläufig zum Studium zugelassen werden, wenn sie zum Erwerb der Sprachkenntnisse einen Hochschulsprachkurs besuchen.
Deutsche und ihnen Gleichgestellte, die nicht von den Sätzen 1 bis 4 erfasst werden, sind zum Hochschulstudium berechtigt, wenn sie über einen ausländischen Bildungsnachweis verfügen, der sie zum Studium an einer im Ausstellungsstaat anerkannten Hochschule berechtigt, und wenn dieser einer Qualifikation gemäß Absatz 2 Satz 1 Nummer 1 bis 4 gleichwertig ist oder wenn sie die Zugangsprüfung einer deutschen Hochschule bestanden haben.
Sonstige Bewerberinnen und Bewerber können unter den Voraussetzungen des Satzes 5 ebenfalls zum Studium zugelassen werden.
Durch die Zugangsprüfung wird festgestellt, ob die fachliche Eignung und die sprachlichen und methodischen Fähigkeiten für das Studium eines Studienganges oder bestimmter fachlich verwandter Studiengänge bestehen.
Das Nähere regelt das für die Hochschulen zuständige Mitglied der Landesregierung durch Rechtsverordnung.
Die Rechtsverordnung kann bestimmen, ob und unter welchen Voraussetzungen mit einer bestandenen Zugangsprüfung an Vergabeverfahren im Sinne des Brandenburgischen Hochschulzulassungsgesetzes teilgenommen werden kann.
Die Rechtsverordnung kann auch bestimmen, dass die Hochschulen Einzelheiten der Zugangsprüfung und hierzu vorbereitender Hochschulkurse einschließlich der Möglichkeit einer auf die Kursdauer begrenzten Immatrikulation an der Hochschule durch Satzung regeln.
Die in Satz 10 genannten Satzungsregelungen können sich auch auf Teilnehmende von Hochschulsprachkursen erstrecken, die über eine Hochschulzugangsberechtigung im Sinne der Sätze 2 bis 6 verfügen.

(2) Zugangsberechtigt zu einem Studium, das zu einem ersten berufsqualifizierenden Hochschulabschluss führt, ist, wer eine der nachfolgenden Qualifikationen nachweisen kann:

1.  die allgemeine Hochschulreife,
    
2.  die fachgebundene Hochschulreife,
    
3.  die Fachhochschulreife,
    
4.  die fachgebundene Fachhochschulreife,
    
5.  einen berufsqualifizierenden Hochschulabschluss,
    
6.  eine aufgrund der §§ 45, 51a, 122 der Handwerksordnung in der Fassung der Bekanntmachung vom 24.
    September 1998 (BGBl.
    I S. 3074; 2006 I S. 2095), die zuletzt durch Artikel 19 des Gesetzes vom 25.
    Juli 2013 (BGBl.
    I S. 2749, 2758) geändert worden ist, bestandene Meisterprüfung oder den Erwerb einer der Meisterprüfung gleichwertigen Berechtigung gemäß § 7 Absatz 2a der Handwerksordnung,
    
7.  einen Fortbildungsabschluss aufgrund der §§ 53, 54 des Berufsbildungsgesetzes vom 23. März 2005 (BGBl.
    I S. 931), das zuletzt durch Artikel 22 des Gesetzes vom 25. Juli 2013 (BGBl.
    I S. 2749, 2758) geändert worden ist, oder nach den §§ 42, 42a der Handwerksordnung, sofern die Lehrgänge mindestens 400 Unterrichtsstunden umfasst haben,
    
8.  ein Befähigungszeugnis für den nautischen oder technischen Schiffsdienst nach der Schiffsoffizier-Ausbildungsverordnung in der Fassung der Bekanntmachung vom 15. Januar 1992 (BGBl.
    I S. 22, 227), die zuletzt durch Artikel 29 Nummer 5 des Gesetzes vom 25. Juli 2013 (BGBl.
    I S. 2749, 2759) geändert worden ist, das auf einem mindestens 400 Unterrichtsstunden umfassenden Lehrgang beruht,
    
9.  einen Abschluss einer Fachschule in öffentlicher Trägerschaft oder einer staatlich anerkannten Fachschule in freier Trägerschaft im Sinne des § 28 des Brandenburgischen Schulgesetzes in der Fassung der Bekanntmachung vom 2. August 2002 (GVBl.
    I S. 78), das zuletzt durch Artikel 2 des Gesetzes vom 14.
    März 2014 (GVBl.
    I Nr. 14 S. 2) geändert worden ist, oder einen Abschluss einer vergleichbaren Ausbildung in einem anderen Land der Bundesrepublik Deutschland,
    
10.  eine der unter den Nummern 6 und 7 genannten Fortbildung vergleichbare Qualifikation aufgrund einer landesrechtlich geregelten Fortbildungsmaßnahme für Berufe im Gesundheitswesen oder im Bereich der sozialpflegerischen oder pädagogischen Berufe oder
    
11.  den Abschluss der Sekundarstufe I oder einen gleichwertigen Abschluss und eine für das beabsichtigte Studium geeignete abgeschlossene Berufsausbildung mit einer danach erworbenen mindestens zweijährigen Berufserfahrung.
    

Die fachgebundene Hochschulreife und die fachgebundene Fachhochschulreife berechtigen an einer Universität nur zum Studium in der entsprechenden Fachrichtung.
Dies gilt für die fachgebundene Fachhochschulreife auch für das Studium an einer Fachhochschule.

(3) Zum Hochschulstudium kann auch zugelassen werden, wer eine im Ausland erworbene Qualifikation nachweist, die einer der in Absatz 2 Satz 1 Nummer 5 bis 11 genannten entspricht und die für das Studium erforderlichen Sprachkenntnisse nachweist.
Wer in einem Studiengang mindestens zwei Semester an einer Hochschule in der Bundesrepublik Deutschland studiert und die in diesem Zeitraum erforderlichen Leistungsnachweise erworben hat, kann das Studium in dem gleichen oder einem eng verwandten Studiengang an einer Hochschule im Land Brandenburg auch dann fortsetzen, wenn die Voraussetzungen nach Absatz 2 nicht vorliegen.
Die Regelungen über die Anerkennung von Leistungen bleiben unberührt.

(4) Für den Zugang zu künstlerischen Studiengängen, die zu einem ersten berufsqualifizierenden Abschluss führen, kann als weitere Voraussetzung oder anstelle einer Qualifikation nach Absatz 2 der Nachweis der künstlerischen Eignung, für den Zugang zu sportwissenschaftlichen und sprachwissenschaftlichen Studiengängen, die zu einem ersten berufsqualifizierenden Abschluss führen, als weitere Voraussetzung der Nachweis der besonderen Eignung für das Sport- oder Sprachstudium verlangt werden.
Durch Satzung der Hochschule kann bestimmt werden, dass die künstlerische Eignung oder die besondere Eignung für das sport- oder sprachwissenschaftliche Studium in einem besonderen Verfahren festgestellt wird.
Das Feststellungsverfahren ist eine Hochschulprüfung im Sinne des § 21 und durch Satzung der Hochschule zu regeln.

(5) Zugangsvoraussetzung für einen Masterstudiengang ist ein erster berufsqualifizierender Hochschulabschluss gleich welchen Hochschultyps.
Darüber hinausgehende Eignungs- und Qualifikationsvoraussetzungen können die Hochschulen für Masterstudiengänge in den Satzungen festlegen, wenn dies wegen spezieller fachlicher Anforderungen des jeweiligen Masterstudienganges nachweislich erforderlich ist.
Für weiterbildende Masterstudiengänge ist darüber hinaus der Nachweis einer in der Regel mindestens einjährigen beruflichen Tätigkeit erforderlich.
In künstlerischen und besonderen weiterbildenden Masterstudiengängen kann an die Stelle des berufsqualifizierenden Hochschulabschlusses eine Eingangsprüfung treten, bei der die Bewerberin oder der Bewerber Kenntnisse und Fähigkeiten nachweist, die einem geeigneten berufsqualifizierenden Hochschulabschluss entsprechen.
Weiterbildende Masterstudiengänge müssen sich darüber hinaus nach ihrer inhaltlichen und organisatorischen Ausgestaltung insbesondere an beruflich qualifizierte Bewerberinnen und Bewerber richten.
Eingangsprüfungen sind Hochschulprüfungen nach § 21 und durch Satzung der Hochschule zu regeln.
Die Zugangsvoraussetzungen für einen lehramtsbezogenen Masterstudiengang richten sich nach den Bestimmungen des Brandenburgischen Lehrerbildungsgesetzes vom 18.
Dezember 2012 (GVBl.
I Nr. 45), das durch Artikel 3 des Gesetzes vom 14. März 2014 (GVBl.
I Nr. 14 S. 9) geändert worden ist.

(6) Abweichend von Absatz 5 Satz 1 kann die Zulassung zu einem Masterstudiengang auch beantragt werden, wenn der Bachelorabschluss wegen Fehlens einzelner Prüfungsleistungen noch nicht vorliegt und aufgrund des bisherigen Studienverlaufs, insbesondere der bisherigen Prüfungsleistungen, zu erwarten ist, dass der Bachelorabschluss rechtzeitig vor Beginn des Masterstudienganges erlangt wird und die Maßgaben, die nach Absatz 5 Voraussetzung für den Zugang zu dem Masterstudiengang sind, ebenso rechtzeitig erfüllt sind.
Soweit die Hochschulen in ihren Satzungen nach Absatz 5 Satz 2 Auswahlverfahren vorsehen, in die das Ergebnis des Bachelorabschlusses einbezogen ist, nehmen Bewerberinnen und Bewerber nach Satz 1 am Auswahlverfahren mit einer Durchschnittsnote, die aufgrund der bisherigen Prüfungsleistungen ermittelt wird, teil.
Das Ergebnis des Bachelorabschlusses bleibt insoweit unbeachtet.
Eine Zulassung ist im Falle einer Bewerbung nach Satz 1 unter dem Vorbehalt auszusprechen, dass der Bachelorabschluss und die mit ihm zusammenhängenden Voraussetzungen des Absatzes 5 innerhalb einer von der Hochschule gesetzten Frist nachgewiesen werden.
Wird der Nachweis nicht fristgerecht geführt, erlischt die Zulassung.
Das Nähere regeln die Hochschulen in den Satzungen nach Absatz 5 Satz 2.

(7) Schülerinnen und Schüler, die nach einer einvernehmlichen Beurteilung von Schule und Hochschule besondere Begabungen aufweisen, können außerhalb des Immatrikulationsverfahrens nach § 14 Absatz 1 als Juniorstudierende eingeschrieben werden.
Sie erhalten damit das Recht, Module zu absolvieren, Studien- und Prüfungsleistungen zu erbringen und Leistungspunkte zu erwerben.
Die nachgewiesenen Studien- und Prüfungsleistungen sowie Leistungspunkte sind bei einem späteren Studium nach Maßgabe der fachlichen Gleichwertigkeit anzuerkennen.
§ 14 Absatz 3 Satz 1 Nummer 5, Absatz 5 Satz 2 Nummer 4 und § 15 gelten entsprechend.
Das Nähere können die Hochschulen in einer Satzung regeln.

(8) Teilnehmende von Zentren für Studierendengewinnung und Studienvorbereitung an den Hochschulen können als Collegestudierende eingeschrieben werden.
Absatz 7 Satz 2 bis 5 gilt entsprechend.

#### § 10   
Studienkolleg; Verordnungsermächtigung

(1) Die Hochschulen können Studienkollegs einrichten, die Studienbewerberinnen und Studienbewerber mit ausländischen Vorbildungsnachweisen, die keinen unmittelbaren Hochschulzugang eröffnen, die Eignung zur Aufnahme eines Studiums vermitteln.
Der Besuch des Studienkollegs dauert in der Regel zwei Semester und wird mit einer Prüfung abgeschlossen.
Die Prüfung kann auch ohne den vorherigen Besuch des Studienkollegs abgelegt werden.

(2) Das für die Hochschulen zuständige Mitglied der Landesregierung regelt im Einvernehmen mit dem für Schule zuständigen Mitglied der Landesregierung durch Rechtsverordnung das Nähere zur Ausgestaltung der Studienkollegs und der Prüfungen, insbesondere

1.  das Verfahren der Zulassung zum Studienkolleg und der Auswahl bei die Aufnahmekapazität übersteigender Bewerberzahl,
    
2.  die Festlegung der Lehrinhalte und -umfänge,
    
3.  die Zulassung zur Prüfung, die Prüfungsanforderungen und das Prüfungsverfahren.
    

(3) Studienbewerberinnen und Studienbewerber an Studienkollegs der Hochschulen werden für die Dauer der Ausbildung am Studienkolleg an der Hochschule immatrikuliert.
Sie gehören keinem Fachbereich an.

(4) Andere Einrichtungen an Hochschulen in nichtstaatlicher Trägerschaft, die Aufgaben nach Absatz 1 wahrnehmen, können als Studienkolleg staatlich anerkannt werden, wenn die Lehrinhalte, die Prüfungsanforderungen und das Prüfungsverfahren gleichwertig sind.
Die Gleichwertigkeit stellt die für die Hochschulen zuständige oberste Landesbehörde fest.
Die staatliche Anerkennung begründet keinen Anspruch auf staatliche Zuschüsse.

#### § 11   
Ermittlung der Ausbildungskapazität und Festsetzung von Zulassungszahlen; Verordnungsermächtigung

(1) Ist zu erwarten, dass an Hochschulen des Landes nicht alle Bewerberinnen und Bewerber eines Studienganges zugelassen werden können, so setzt das für die Hochschulen zuständige Mitglied der Landesregierung im Benehmen mit der jeweiligen Hochschule durch Rechtsverordnung Zulassungszahlen fest.
Zulassungszahl ist die Zahl der von der einzelnen Hochschule höchstens aufzunehmenden Bewerberinnen und Bewerber in einem Studiengang.
Sie wird auf der Grundlage der jährlichen Aufnahmekapazität festgesetzt.
Zulassungszahlen dürfen nur für einen bestimmten Zeitraum, höchstens für die Dauer eines Studienjahres, festgesetzt werden.
Zulassungszahlen sind für jede Hochschule festzusetzen, wenn ein Studiengang in die zentrale Vergabe von Studienplätzen einbezogen wird.

(2) Die Zulassungszahlen sind so festzusetzen, dass unter Berücksichtigung der personellen, räumlichen, sachlichen und fachspezifischen Gegebenheiten, auch besonderer bildungs- oder forschungspolitischer Ziele, die Ausbildungskapazität genutzt wird.
Die Qualität in Forschung und Lehre, die geordnete Wahrnehmung der Aufgaben der Hochschule insbesondere in Forschung, Lehre und Studium sind zu gewährleisten.

(3) Die jährliche Aufnahmekapazität wird auf der Grundlage des Lehrangebots, des Ausbildungsaufwands und weiterer kapazitätsbestimmender Kriterien ermittelt.
Das zuständige Mitglied der Landesregierung regelt im Benehmen mit der jeweiligen Hochschule durch Rechtsverordnung die Normwerte sowie die Einzelheiten für die Kapazitätsermittlung und für die Festsetzung von Zulassungszahlen.
Das für die Hochschulen zuständige Mitglied der Landesregierung kann das Recht zur Bestimmung von Zulassungszahlen durch Rechtsverordnung auf die Hochschulen übertragen.
In diesem Fall regeln die Hochschulen die Zulassungszahlen durch Satzung.

(4) Bei der Ermittlung der Aufnahmekapazität nach den Absätzen 2 und 3 können auf Antrag der jeweiligen Hochschule Maßnahmen unberücksichtigt bleiben, die

1.  der Minderung einer in einem Studiengang oder einer Lehreinheit bestehenden oder zu erwartenden Überlast,
    
2.  der Verbesserung der Studienbedingungen oder der Betreuungsrelationen oder
    
3.  der Umsetzung von Vereinbarungen zwischen Bund und Ländern im Hochschulbereich
    

dienen, wenn die Maßnahmen durch eigene Mittel der jeweiligen Hochschule, durch gesondert zugewiesene staatliche Mittel oder mit Mitteln Dritter finanziert werden oder keine Finanzierung erfordern.

#### § 12   
Zentrale Vergabe von Studienplätzen; örtliche Zulassungsbeschränkungen; Verordnungsermächtigung

(1) Sind für einen Studiengang für mehrere Hochschulen Zulassungszahlen festgesetzt oder ist aus anderen Gründen eine zentrale Vergabe der Studienplätze sinnvoll, können die Studienplätze zentral vergeben werden.
Wird in einem Studiengang, der nicht in das zentrale Vergabeverfahren einbezogen ist, für eine Hochschule eine Zulassungszahl festgesetzt, erfolgt die Studienplatzvergabe durch die Hochschule.

(2) Das für die Hochschulen zuständige Mitglied der Landesregierung regelt durch Rechtsverordnung im Benehmen mit den Hochschulen die Einbeziehung von Studiengängen in das zentrale Vergabeverfahren nach Absatz 1 Satz 1 und die Festsetzung von Zulassungszahlen nach Absatz 1 Satz 2.

#### § 13   
Zulassungshindernisse

Die Zulassung zu einem Studiengang muss versagt werden, wenn

1.  die Voraussetzungen des § 9 oder einer auf der Grundlage des § 9 ergangenen Rechtsvorschrift nicht vorliegen,
    
2.  die Zulassung zu einem Studiengang beantragt wird, für den eine frühere Zulassung der Bewerberin oder des Bewerbers erloschen ist, weil sie oder er entweder eine Prüfung in diesem Studiengang endgültig nicht bestanden hat oder der Prüfungsanspruch nicht mehr besteht, oder
    
3.  für den Studiengang die Zulassungszahl festgesetzt ist und die Studienbewerberin oder der Studienbewerber keinen Studienplatz zugewiesen bekam oder von der Zulassung nicht fristgerecht Gebrauch machte.
    

#### § 14   
Immatrikulation und Exmatrikulation; Verordnungsermächtigung

(1) Studienbewerberinnen und Studienbewerber sind zu immatrikulieren, wenn die Voraussetzungen nach § 9 erfüllt sind und Versagungsgründe für die Immatrikulation nicht vorliegen.
Mit der Immatrikulation werden Studienbewerberinnen und Studienbewerber Mitglied der Hochschule.
Zum Weiterstudium haben sich die Studierenden zu jedem Semester fristgerecht anzumelden (Rückmeldung).
Nach Maßgabe der Immatrikulationsordnung können Studierende von der Hochschule auf Antrag aus wichtigem Grund von der Verpflichtung zu einem ordnungsgemäßen Studium befristet befreit werden (Beurlaubung).
In besonders begründeten Fällen ist eine Beurlaubung auch außerhalb der Rückmeldefristen zulässig.
Studienbewerberinnen und Studienbewerber für einen Masterstudiengang, der nicht weiterbildend ist, können abweichend von Satz 1 vorläufig immatrikuliert werden, wenn sie nachweisen, dass die für den ersten berufsqualifizierenden Hochschulabschluss erforderlichen Studienleistungen erbracht oder der Hochschule vorgelegt sind und Versagungsgründe für die Immatrikulation nicht vorliegen.
In diesem Fall sind die Zugangsvoraussetzungen für das Masterstudium der Hochschule bis zum Ablauf des ersten Semesters nach der Immatrikulation nachzuweisen.
Wird der Nachweis nicht erbracht, so entfällt die Immatrikulation rückwirkend.
Eine nach Absatz 2 geleistete Gebühr wird nicht erstattet.

(2) Bei der Immatrikulation und bei jeder Rückmeldung werden für Verwaltungsleistungen, die die Hochschulen für die Studierenden im Rahmen der Durchführung des Studiums außerhalb der fachlichen Betreuung erbringen, Gebühren erhoben.
Hierzu zählen Verwaltungsleistungen für die Immatrikulation, Rückmeldung, Beurlaubung und Exmatrikulation.
Außerdem zählen hierzu Verwaltungsleistungen, die im Rahmen der allgemeinen Studienberatung sowie durch die Akademischen Auslandsämter und die Prüfungsämter erbracht werden.
Die Gebühr wird nicht erhoben in den Fällen der Beurlaubung vom Studium wegen Krankheit der Studierenden, wegen der Pflege naher Angehöriger der Studierenden im Sinne des Pflegezeitgesetzes vom 28.
Mai 2008 (BGBl.
I S. 874, 896), wegen der Inanspruchnahme der Schutzfristen aus § 3 Absatz 2 und § 6 Absatz 1 des Mutterschutzgesetzes in der Fassung der Bekanntmachung vom 20.
Juni 2002 (BGBl.
I S. 2318), das zuletzt durch Artikel 6 des Gesetzes vom 23.
Oktober 2012 (BGBl.
I S. 2246, 2261) geändert worden ist, wegen der Inanspruchnahme von Elternzeit gemäß den §§ 15 und 20 Absatz 1 des Bundeselterngeld- und Elternzeitgesetzes vom 5.
Dezember 2006 (BGBl.
I S. 2748), das zuletzt durch Artikel 1 des Gesetzes vom 15.
Februar 2013 (BGBl.
I S. 254) geändert worden ist, und wegen der Heranziehung zum Wehrpflicht- oder Zivildienst.
Die Gebühr entfällt auch für ausländische Studierende, die aufgrund eines zwischenstaatlichen oder übernationalen Abkommens oder einer Hochschulpartnerschaft immatrikuliert sind oder werden, soweit Gegenseitigkeit besteht, sowie für ausländische Studierende im Rahmen von Förderungsprogrammen, die ganz oder teilweise aus öffentlichen Mitteln des Bundes oder der Länder finanziert werden.
Sind Studienbewerberinnen oder -bewerber bereits in einem Studiengang oder Teilstudiengang an einer anderen Hochschule des Landes Brandenburg oder an einer Hochschule des Landes Berlin immatrikuliert, so erklären sie bei der Immatrikulation, an welcher Hochschule sie ihre Mitgliedschaftsrechte ausüben wollen.
Die Gebühr nach Satz 1 ist nur an der Hochschule zu entrichten, an der die Mitgliedschaftsrechte ausgeübt werden.
Die Höhe der Gebühr nach Satz 1 beträgt 51 Euro.

(3) Die Immatrikulation ist zu versagen, wenn die Studienbewerberin oder der Studienbewerber

1.  in einem zulassungsbeschränkten Studiengang nicht zugelassen ist,
    
2.  die in dem gewählten Studiengang vorgeschriebenen Leistungsnachweise oder Prüfungen an einer Hochschule der Bundesrepublik Deutschland endgültig nicht bestanden oder den Prüfungsanspruch verloren hat,
    
3.  die Zahlung von Gebühren nach Absatz 2 oder nach § 5 Absatz 4 oder Beiträgen nach § 16 Absatz 4 oder § 81 Absatz 1 Nummer 3 nicht nachweist,
    
4.  für einen ausbildungsintegrierten dualen Studiengang keinen Ausbildungsvertrag mit einer von der Hochschule zugelassenen Ausbildungsstätte nachweist, obwohl dies durch Satzung der Hochschule vorgeschrieben ist; der Ausbildungsvertrag muss den von der Hochschule aufgestellten Grundsätzen für die Ausgestaltung der Vertragsverhältnisse entsprechen; oder
    
5.  vom Studium an einer anderen Hochschule im Wege eines Ordnungsverfahrens ausgeschlossen worden ist.
    

Die Immatrikulation kann versagt werden, wenn das nach Absatz 8 Satz 1 geregelte Verfahren nicht eingehalten ist.

(4) Die Immatrikulation kann widerrufen werden, wenn sich nachträglich Immatrikulationshindernisse herausstellen, bei deren Bekanntsein die Immatrikulation hätte versagt werden müssen.

(5) Die Mitgliedschaft der Studierenden zur Hochschule endet mit der Exmatrikulation.
Studierende sind zu exmatrikulieren, wenn

1.  sie die Abschlussprüfung einschließlich einer Wiederholungsprüfung zur Notenverbesserung bestanden oder eine vorgeschriebene Prüfung endgültig nicht bestanden haben, sofern sie nicht innerhalb von zwei Monaten die Notwendigkeit der Immatrikulation für die Erreichung eines weiteren Studienzieles nachweisen, oder den Prüfungsanspruch verloren haben,
    
2.  sie der Verpflichtung zur Teilnahme an einer Studienfachberatung nach § 21 Absatz 2 Satz 2 in Verbindung mit § 20 Absatz 3 nicht nachgekommen sind oder den Abschluss einer Studienverlaufsvereinbarung abgelehnt oder die in einer Studienverlaufsvereinbarung gemäß § 20 Absatz 3 Satz 3 festgelegten Anforderungen bis zum festgesetzten Zeitpunkt in zu vertretender Weise nicht erfüllt haben.
    Dies gilt nicht, wenn der betreffende Studierende auf diese Folgen nicht zusammen mit der Einladung oder bei Abschluss der Studienverlaufsvereinbarung hingewiesen wurde,
    
3.  sie Gebühren nach Absatz 2 oder § 5 Absatz 4 oder Beiträge nach § 16 Absatz 4 oder § 81 Absatz 1 Nummer 3 trotz Mahnung und Androhung der Exmatrikulation nicht gezahlt haben,
    
4.  sie das Studium in keinem Studiengang fortführen dürfen,
    
5.  bei einem Studium nach Absatz 3 Satz 1 Nummer 4 das Ausbildungsverhältnis ohne den vorgesehenen Ausbildungsabschluss rechtswirksam beendet und nicht innerhalb von acht Wochen ein neuer Ausbildungsvertrag geschlossen und der Hochschule nachgewiesen worden ist oder
    
6.  sie mit der Ordnungsmaßnahme der Exmatrikulation belegt worden sind.
    

Bei Bestehen einer Abschlussprüfung findet die Exmatrikulation nach Satz 2 Nummer 1 zum Ende des Semesters statt, in dem die Studierenden die Abschlussprüfung bestanden haben; das Recht einen Antrag auf Exmatrikulation zu stellen, bleibt davon unberührt.

(6) In der Geschäftsfähigkeit beschränkte Personen mit Hochschulzugangsberechtigung, die mindestens 16 Jahre alt sind, sind für Verfahrenshandlungen zur Aufnahme, Durchführung und Beendigung eines Studiums handlungsfähig im Sinne von § 1 Absatz 1 und § 2 Absatz 3 Nummer 2 des Verwaltungsverfahrensgesetzes für das Land Brandenburg in Verbindung mit § 12 Absatz 1 Nummer 2 des Verwaltungsverfahrensgesetzes.
Entsprechendes gilt für in der Geschäftsfähigkeit beschränkte Juniorstudierende, die mindestens 16 Jahre alt sind, hinsichtlich der Verfahrenshandlungen zur Durchführung des Juniorstudiums.

(7) Studierende können exmatrikuliert werden, wenn sie sich nicht fristgerecht zurückgemeldet oder das Studium in einem zulassungsbeschränkten Studiengang trotz schriftlicher Aufforderung und Androhung der Exmatrikulation nicht unverzüglich aufgenommen haben.

(8) Einzelheiten des Verfahrens werden durch die Immatrikulationsordnung der Hochschule geregelt, die der Genehmigung der Präsidentin oder des Präsidenten bedarf.
Sie ist der für die Hochschulen zuständigen obersten Landesbehörde vor Inkrafttreten anzuzeigen.

(9) Die Hochschulen dürfen von Studienbewerberinnen und Studienbewerbern, Studierenden, Promotionsstudierenden, Prüfungskandidatinnen und Prüfungskandidaten und externen Nutzerinnen und Nutzern von Hochschuleinrichtungen die personenbezogenen Daten verarbeiten, die insbesondere für die Immatrikulation, die Rückmeldung, die Teilnahme an Lehrveranstaltungen, Prüfungen, die Nutzung von Hochschuleinrichtungen, die Beteiligung an der Evaluation von Lehre und Studium und für die Hochschulplanung erforderlich sind.
Das für die Hochschulen zuständige Mitglied der Landesregierung kann durch Rechtsverordnung bestimmen, welche personenbezogenen Daten für diese Zwecke verarbeitet, ferner welche Daten für die Zwecke der Hochschulstatistik verwendet und an das Amt für Statistik Berlin-Brandenburg übermittelt werden dürfen.

#### § 15   
Ordnungsverstöße; Ordnungsverfahren

(1) Studierende, die vorsätzlich durch Anwendung von Gewalt, Aufforderung zur Gewalt oder durch Drohung mit Gewalt

1.  den bestimmungsgemäßen Betrieb einer Einrichtung, die Tätigkeit eines Organs oder die Durchführung einer Veranstaltung der Hochschule behindern oder zu behindern versuchen oder
    
2.  ein Hochschulmitglied oder eine Hochschulangehörige oder einen Hochschulangehörigen von der Ausübung seiner oder ihrer Rechte und Pflichten abhalten oder abzuhalten versuchen,
    

begehen einen Ordnungsverstoß.
Gleiches gilt, wenn Studierende an den in Satz 1 genannten Handlungen teilnehmen oder wiederholt Anordnungen zuwiderhandeln, die gegen sie von der Hochschule wegen Verletzung ihrer Pflichten getroffen worden sind.

(2) Gegen Studierende, die einen Ordnungsverstoß nach Absatz 1 begangen haben, können Ordnungsmaßnahmen verhängt werden.
Ordnungsmaßnahmen sind:

1.  Androhung der Exmatrikulation,
    
2.  Ausschluss von der Benutzung von Einrichtungen der Hochschule,
    
3.  Ausschluss von der Teilnahme an einzelnen Lehrveranstaltungen bis zu einem Semester,
    
4.  Exmatrikulation.
    

Die Ordnungsmaßnahme nach Satz 2 Nummer 1 kann nur in Verbindung mit Ordnungsmaßnahmen nach Satz 2 Nummer 2 oder 3 ausgesprochen werden; die Ordnungsmaßnahmen nach Satz 2 Nummer 2 und 3 können nebeneinander verhängt werden.

(3) Von Ordnungsmaßnahmen ist abzusehen, wenn Maßnahmen aufgrund des Hausrechts ausreichen, um weitere Verstöße im Sinne von Absatz 1 auszuschließen.

(4) Über das Ordnungsverfahren erlässt das zuständige Organ der Hochschule eine Satzung, die der Genehmigung der Präsidentin oder des Präsidenten bedarf.
Die Satzung ist der zuständigen obersten Landesbehörde vor dem Inkrafttreten anzuzeigen.

#### § 16   
Studierendenschaft

(1) Die Studierenden einer Hochschule bilden die Studierendenschaft.
Sie ist eine rechtsfähige Teilkörperschaft der Hochschule.
Sie verwaltet ihre Angelegenheiten selbst.
Aufgaben der Studierendenschaft sind:

1.  die Wahrnehmung der Interessen der Studierenden,
    
2.  die Förderung der politischen Bildung einschließlich des staatsbürgerlichen Verantwortungsbewusstseins und der Bereitschaft ihrer Mitglieder zur aktiven Toleranz sowie zum Eintreten für die Grund- und Menschenrechte auf der Grundlage der verfassungsmäßigen Ordnung,
    
3.  die Förderung der geistigen und musischen Interessen ihrer Mitglieder,
    
4.  die Mitwirkung an der Erfüllung der Aufgaben der Hochschulen (§ 3), insbesondere durch Stellungnahmen zu hochschul- oder wissenschaftspolitischen Fragestellungen,
    
5.  die Unterstützung ihrer Mitglieder bei der Erreichung der Studienziele,
    
6.  die Unterstützung der sozialen, kulturellen und fachlichen Belange ihrer Mitglieder,
    
7.  die Pflege der überregionalen und internationalen Beziehungen der Studierenden sowie die Förderung der Integration ausländischer Studierender und
    
8.  die Förderung des Sports im Rahmen des Hochschulsports.
    

Stellungnahmen der Studierendenschaft zu wissenschaftspolitischen Fragestellungen nach Satz 4 Nummer 4 können auch Fragen zur gesellschaftlichen Aufgabenstellung der Hochschulen sowie zur Anwendung der wissenschaftlichen Erkenntnisse und zur Abschätzung ihrer Folgen für die Gesellschaft behandeln.

(2) Für die Wahlen zu den Organen der Studierendenschaft gilt § 62 entsprechend.
Sie sollen gleichzeitig mit den Wahlen der Organe der Hochschule durchgeführt werden.
Die Studierendenschaft untersteht der Rechtsaufsicht der Präsidentin oder des Präsidenten; § 5 Absatz 5 Satz 2 bis 5 gilt entsprechend.

(3) Die Studierendenschaft gibt sich eine Satzung.
Sie wird von ihrem obersten beschlussfassenden Organ beschlossen und enthält Vorschriften über ihre Änderung.
Die Satzung ist der Präsidentin oder dem Präsidenten anzuzeigen.

(4) Die Studierendenschaft erhebt von ihren Mitgliedern Beiträge.
Die Höhe der Beiträge ist auf das Maß zu beschränken, das zur Erfüllung ihrer Aufgaben unter Berücksichtigung der Grundsätze der Wirtschaftlichkeit und Sparsamkeit erforderlich ist.
Die Beiträge sind von der Hochschule kostenfrei einzuziehen.
Die Haushalts- und Wirtschaftsführung der Studierendenschaft bestimmt sich nach § 106 Absatz 1 der Landeshaushaltsordnung.

(5) Der Haushaltsplan und die Festsetzung der Beitragshöhe bedürfen der Genehmigung der Präsidentin oder des Präsidenten.
Die Genehmigung des Haushaltsplanes darf nur versagt werden, wenn die Grundsätze einer ordnungsgemäßen Haushaltsführung verletzt worden sind.
Die Haushalts- und Wirtschaftsführung der Studierendenschaft unterliegt der Prüfung durch den Landesrechnungshof Brandenburg.
Für ihre Verbindlichkeiten haftet nur ihr Vermögen.

(6) Die Studierendenschaften der Brandenburger Hochschulen können zur Wahrnehmung ihrer gemeinsamen Interessen eine Landeskonferenz der Studierendenschaften bilden.
Zur Vertretung der Angelegenheiten der Studierendenschaften wählt diese einen Sprecherrat.
Die Landeskonferenz ist vor dem Erlass oder der Änderung von hochschulrechtlichen Gesetzen oder Rechtsverordnungen, die die Belange Studierender berühren, von der für Hochschulen zuständigen obersten Landesbehörde oder, wenn der Gesetzentwurf aus der Mitte des Landtages kommt, von dem zuständigen Ausschuss des Landtages rechtzeitig zu informieren und anzuhören.

Abschnitt 3   
Studium, Lehre, Prüfungen
----------------------------------------

#### § 17   
Ziel des Studiums; Studienreform

(1) Lehre und Studium sollen die Studierenden auf berufliche Tätigkeiten vorbereiten und ihnen die dafür erforderlichen fachlichen Kenntnisse, Fähigkeiten und Methoden so vermitteln, dass sie zu wissenschaftlicher oder künstlerischer Arbeit, zu selbstständigem Denken und zu verantwortlichem Handeln in einem freiheitlichen, demokratischen und sozialen, den natürlichen Lebensgrundlagen verpflichteten Rechtsstaat befähigt werden.

(2) Die Hochschulen haben die ständige Aufgabe, im Zusammenwirken mit den zuständigen Stellen und der Wirtschaft Inhalt und Form des Studiums im Hinblick auf die Entwicklung in Wissenschaft und Kunst, die Bedürfnisse der beruflichen Praxis und die notwendigen Veränderungen in der Berufswelt zu überprüfen und weiterzuentwickeln.

#### § 18   
Studiengänge

(1) Studiengänge führen in der Regel zu einem berufsqualifizierenden Abschluss.
Als berufsqualifizierend gilt auch der Abschluss eines Studienganges, durch den die fachliche Eignung für einen beruflichen Vorbereitungsdienst oder eine berufliche Einführung vermittelt wird.
Soweit das jeweilige Studienziel eine berufspraktische Studienphase erfordert, ist sie mit den übrigen Teilen des Studiums inhaltlich und zeitlich abzustimmen und in den Studiengang einzuordnen.
Studiengänge können gemeinsam von mehreren Hochschulen, auch hochschultypenübergreifend, durchgeführt werden.
Für einen neuen Studiengang soll der Lehrbetrieb erst aufgenommen werden, wenn die entsprechende Prüfungsordnung genehmigt ist.

(2) In den Prüfungsordnungen sind die Studienzeiten vorzusehen, in denen ein erster berufsqualifizierender Abschluss erworben werden kann (Regelstudienzeit).
Die Regelstudienzeit schließt Zeiten einer in den Studiengang eingeordneten berufspraktischen Tätigkeit, praktische Studiensemester und Prüfungszeiten ein.
Sie ist maßgebend für die Gestaltung der Studiengänge, die Sicherstellung des Lehrangebots, die Gestaltung des Prüfungsverfahrens sowie für die Ermittlung und Feststellung der Ausbildungskapazitäten und die Berechnung von Studierendenzahlen bei der Hochschulplanung.

(3) Die Regelstudienzeit beträgt bei Studiengängen, die zu einem Bachelorgrad führen, mindestens drei und höchstens vier Jahre.
Bei Studiengängen, die zu einem Mastergrad führen, beträgt die Regelstudienzeit mindestens ein Jahr und höchstens zwei Jahre.
Bei konsekutiven Studiengängen, die zu Graden nach den Sätzen 1 und 2 führen, beträgt die Gesamtregelstudienzeit höchstens fünf Jahre.
Abweichend von Satz 3 können in künstlerischen Kernfächern an der Hochschule für Film und Fernsehen Potsdam-Babelsberg auf Antrag der Hochschule konsekutive Bachelor- und Masterstudiengänge mit einer Gesamtregelstudienzeit von bis zu sechs Jahren eingerichtet werden.
Bei Fachhochschulstudiengängen, die zu einem Diplomgrad führen, beträgt die Regelstudienzeit höchstens vier Jahre, bei anderen Studiengängen, die zu einem Diplom- oder Magistergrad führen, höchstens vierundeinhalb Jahre.

(4) Die Hochschulen können Studiengänge so organisieren und einrichten, dass Studierenden, die wegen persönlicher Gründe nicht in der Lage sind, ein Vollzeitstudium zu betreiben, ein Studium auch in Teilzeitform möglich wird.
Auf Antrag von mindestens 20 Prozent der Studierenden eines Studienganges hat die Hochschule den Bedarf für die Einrichtung eines Studienganges in Teilzeit zu ermitteln.
Die Hochschulen sollen darüber hinaus eine Immatrikulation oder Rückmeldung als Teilzeitstudierender zulassen, wenn die Antragstellerin oder der Antragsteller entsprechende persönliche Gründe nachweist.
Die Immatrikulation oder Rückmeldung als Teilzeitstudierende oder Teilstudierender soll semesterweise oder für jeweils ein Studienjahr ermöglicht werden.
Für Studiengänge, die in Teilzeitform angeboten werden, oder bei einer Immatrikulation als Teilzeitstudierende oder Teilzeitstudierender ist die Regelstudienzeit nach Absatz 3 entsprechend zu verlängern.
Von Absatz 3 abweichende Regelstudienzeiten dürfen im Übrigen bei entsprechender studienorganisatorischer Gestaltung im Ausnahmefall festgesetzt werden.

(5) Einrichtung, Änderung und Aufhebung von Studiengängen bedürfen der Genehmigung der für die Hochschulen zuständigen obersten Landesbehörde.
Der Antrag auf Genehmigung ist mindestens sechs Monate vor Aufnahme des Lehrbetriebs zu stellen.
Dies gilt nicht für Studiengänge, deren Einrichtung, Änderung oder Aufhebung in einem Hochschulvertrag oder einer anderen Ziel- oder Leistungsvereinbarung zwischen der Hochschule und dem für die Hochschulen zuständigen Mitglied der Landesregierung konkret vereinbart worden ist; die Einrichtung, Änderung und Aufhebung dieser Studiengänge sind der für die Hochschulen zuständigen obersten Landesbehörde abweichend von Satz 1 mindestens zwei Monate vor Aufnahme oder Aufgabe des Lehrbetriebs anzuzeigen.

(6) Neu eingerichtete und wesentlich geänderte Bachelor- und Masterstudiengänge sind durch eine anerkannte unabhängige Einrichtung daraufhin zu überprüfen, ob fachlich-inhaltliche Mindeststandards und die Berufsrelevanz der Abschlüsse gewährleistet sind (Akkreditierung).
Künstlerische Studiengänge an Kunsthochschulen sollen akkreditiert werden.
Im Rahmen der Akkreditierung sind auch die Schlüssigkeit des Studienkonzepts und die Studierbarkeit des Studiums unter Einbeziehung des Selbststudiums, die Voraussetzungen für die Vergabe von Leistungspunkten sowie die wechselseitige Anerkennung von Leistungen bei einem Hochschul- oder Studiengangwechsel zu überprüfen und zu bestätigen.
Die Akkreditierung ist regelmäßig und in angemessenen Zeitabständen zu wiederholen (Reakkreditierung).
Wird die Akkreditierung oder Reakkreditierung verweigert, entscheidet die für die Hochschulen zuständige oberste Landesbehörde gemäß Absatz 5 Satz 1 über die Aufhebung des Studienganges.
Das Gleiche gilt, wenn Akkreditierungsauflagen nicht erfüllt werden.

#### § 19   
Studienordnungen

(1) Für jeden Studiengang stellen die Fachbereiche eine Studienordnung auf.
Diese regelt auf der Grundlage der Prüfungsordnung nach § 22 und der Rahmenordnung nach § 23 Inhalt und Aufbau des Studiums einschließlich einer in den Studiengang eingeordneten berufspraktischen Studienphase.
Die Studieninhalte, der Studienablauf und die Prüfungen sind so zu organisieren, dass das Studium in der Regelstudienzeit abgeschlossen werden kann.
Die Belange Studierender mit Kinderbetreuungs- und Pflegepflichten sowie von Studierenden mit Behinderungen sind zu berücksichtigen.

(2) Die Studienordnungen werden von dem in der Grundordnung bestimmten Organ des Fachbereichs erlassen und bedürfen der Genehmigung der Präsidentin oder des Präsidenten.
Studienordnungen für Studiengänge, die auf den Erwerb einer Laufbahnbefähigung hinführen, sind der für die Hochschulen zuständigen obersten Landesbehörde anzuzeigen.
Diese kann Änderungen verlangen, wenn die Studienordnung nicht gewährleistet, dass die Voraussetzungen für die Anerkennung der Laufbahnbefähigung erfüllt sind.
Fordert sie nicht innerhalb von drei Monaten nach Vorlage zu Änderungen auf, tritt die Studienordnung nach Veröffentlichung im amtlichen Mitteilungsblatt der Hochschule in Kraft.

(3) Ordnungen über den Zugang oder die Zulassung zu einem Masterstudiengang sind der für die Hochschulen zuständigen obersten Landesbehörde rechtzeitig vor dem vorgesehenen Inkrafttreten anzuzeigen.

#### § 20   
Studienberatung

(1) Die Hochschule informiert Studienberechtigte über die Vorzüge eines Hochschulstudiums.
Sie unterrichtet Studienbewerberinnen und Studienbewerber sowie Studierende über die Studienmöglichkeiten sowie über Inhalte, Aufbau und Anforderungen eines Studiums.
Die Hochschule kann durch Satzung die verpflichtende Teilnahme an einer Studienberatung vor einem Antrag auf Zulassung zum Studium oder vor einem Antrag auf Immatrikulation vorschreiben.
Während des gesamten Studiums unterstützt sie die Studierenden durch eine studienbegleitende fachliche Beratung.
Die Studierenden sind so zu beraten und zu betreuen, dass sie ihr Studium zielgerichtet auf den Studienabschluss hin gestalten und in der Regelstudienzeit beenden können.
Die Hochschule soll bei der Studienberatung insbesondere mit den für Berufsberatung und staatliche Prüfungen zuständigen Stellen zusammenwirken.

(2) Jede Studierende und jeder Studierende wird einer Mentorin oder einem Mentor zugeordnet, die sie oder ihn während ihres oder seines Studiums nach Bedarf insbesondere in der Studiengestaltung, bei der zeitlichen Planung und der inhaltlichen Ausrichtung ihres oder seines Studiums beratend unterstützt.
Mentorinnen und Mentoren gehören dem Fachbereich der oder des Studierenden an; sie können Hochschullehrerinnen und Hochschullehrer, Akademische Mitarbeiterinnen und Mitarbeiter, geeignete wissenschaftliche oder künstlerische Hilfskräfte oder Tutorinnen oder Tutoren sein.
Die Zuordnung erfolgt innerhalb eines Jahres nach Aufnahme des Studiums an der Hochschule.

(3) Die Ordnungen der Hochschulen nach § 19 haben vorzusehen, dass die Studierenden im Falle des § 21 Absatz 2 Satz 2 zur Teilnahme an einer Studienfachberatung verpflichtet sind.
Die Studienfachberatung hat durch eine Hochschullehrerin oder einen Hochschullehrer zu erfolgen.
Ziel der Studienfachberatung nach Satz 1 ist der Abschluss einer Vereinbarung, in der das weitere Studium geplant wird und sich die oder der Studierende zu bestimmten Maßnahmen zur Erreichung der Studienziele verpflichtet und weitere zur Förderung des weiteren Studienverlaufs geeignete Maßnahmen der Hochschule vereinbart werden (Studienverlaufsvereinbarung).
Bei der Festlegung von Verpflichtungen ist die persönliche Situation der oder des Studierenden angemessen zu berücksichtigen.
Einzelheiten des Verfahrens werden durch die Hochschulen in den Ordnungen nach § 19 geregelt.

#### § 21   
Prüfungen

(1) Das Studium wird in der Regel durch eine Hochschulprüfung, eine staatliche Prüfung oder eine kirchliche Prüfung abgeschlossen.
In Studiengängen mit einer Regelstudienzeit von mindestens vier Jahren, die nicht zu einem Bachelorabschluss führen, findet bis zum Ende des zweiten Studienjahres eine Zwischenprüfung statt.
Prüfungen werden in der Regel studienbegleitend durchgeführt.
In den Diplom- und Magisterstudiengängen und in Studiengängen, die mit einer staatlichen Prüfung abschließen, setzt der Übergang in das Hauptstudium in der Regel die erfolgreiche Ablegung der Zwischenprüfung voraus.

(2) Haben Studierende eine nach der Prüfungsordnung erforderliche studienbegleitende Prüfungsleistung, eine Zwischen- oder Abschlussprüfung endgültig nicht bestanden, so findet § 14 Absatz 5 Satz 2 Nummer 1 Anwendung.
Haben sie eine Prüfung im Sinne von Satz 1 nicht innerhalb einer in der Prüfungsordnung zu bestimmenden angemessenen Frist, die vier Semester nicht unterschreiten darf, erfolgreich abgelegt, so sind sie verpflichtet, an einer Studienfachberatung im Sinne von § 20 Absatz 3 teilzunehmen.
Dies gilt nicht, wenn die Überschreitung einer Prüfungsfrist von ihnen nicht zu vertreten ist.
Die Prüfungsordnungen legen darüber hinaus fest, in welchen Fällen eine angemessene Verlängerung der Prüfungsfrist zu gewähren ist.

(3) § 19 Absatz 1 Satz 4 gilt entsprechend.

(4) Hochschulprüfungen können vor Ablauf einer für die Meldung festgelegten Frist abgelegt werden, sofern die für die Zulassung zur Prüfung erforderlichen Leistungen nachgewiesen sind.

(5) Zur Abnahme von Hochschulprüfungen sind das an den Hochschulen hauptberuflich tätige wissenschaftliche und künstlerische Personal, Lehrbeauftragte und in der beruflichen Praxis und Ausbildung erfahrene Personen befugt.
Hochschulprüfungen sollen nur von Personen abgenommen werden, die Lehraufgaben erfüllen.
Prüfungsleistungen dürfen nur von Personen bewertet werden, die selbst mindestens die durch die Prüfung festzustellende oder eine gleichwertige Qualifikation besitzen.

(6) Die Hochschulen sind berechtigt, von Prüfungskandidatinnen und -kandidaten eine Versicherung an Eides Statt abzunehmen, dass sie die Prüfungsleistung selbständig und ausschließlich unter Verwendung zulässiger Hilfsmittel erbracht haben.

#### § 22   
Prüfungsordnungen für Hochschulprüfungen; Verordnungsermächtigung

(1) Hochschulprüfungen werden aufgrund von Prüfungsordnungen der Fachbereiche und der Rahmenordnung nach § 23 abgelegt.
Die Prüfungsanforderungen und -verfahren sind so zu gestalten, dass die Abschlussprüfung innerhalb der Regelstudienzeit vollständig abgelegt werden kann.
§ 19 Absatz 1 Satz 4 gilt entsprechend.
Prüfungsordnungen müssen die Inanspruchnahme der Schutzfristen aus § 3 Absatz 2 und § 6 Absatz 1 des Mutterschutzgesetzes sowie der Fristen des Bundeselterngeld- und Elternzeitgesetzes ermöglichen.
Ein Nachteilsausgleich für Studierende mit nachgewiesenen körperlichen, geistigen oder psychischen Beeinträchtigungen und Behinderungen zur Anerkennung gleichwertiger Leistungen in anderer Form oder verlängerter Zeit ist vorzusehen.

(2) Prüfungsordnungen werden von dem nach der Grundordnung zuständigen Organ des Fachbereichs erlassen und bedürfen der Genehmigung der Präsidentin oder des Präsidenten.
Die Genehmigung ist insbesondere zu versagen, wenn die Prüfungsordnung eine mit § 18 Absatz 3 unvereinbare Regelstudienzeit vorsieht.
Prüfungsordnungen, die Grundlage von Prüfungen sind, aufgrund derer eine Laufbahnbefähigung verliehen wird, sind der für die Hochschulen zuständigen obersten Landesbehörde vor Inkrafttreten anzuzeigen.
§ 19 Absatz 2 Satz 3 und 4 gilt entsprechend.

(3) Für alle geeigneten Studiengänge sind in den Prüfungsordnungen die Voraussetzungen zu bestimmen, unter denen eine innerhalb der Regelstudienzeit abgelegte Abschlussprüfung im Falle des Nichtbestehens als nicht unternommen gilt (Freiversuch).
Eine im Freiversuch bestandene Prüfung kann zur Notenverbesserung wiederholt werden.

(4) Das für die Hochschulen zuständige Mitglied der Landesregierung kann insbesondere zur Gewährleistung der Gleichwertigkeit einander entsprechender Studien- und Prüfungsleistungen sowie Studienabschlüsse durch Rechtsverordnung Näheres zur Gestaltung von Prüfungsordnungen bestimmen.

#### § 23   
Rahmenordnungen für Studium, Prüfungen, Zugang und Zulassung

(1) In Rahmenordnungen für Studium, Prüfungen, Zugang und Zulassung erlassen die Hochschulen Bestimmungen zu folgenden Regelungsbereichen:

1.  allgemeine Zugangs- und Zulassungsvoraussetzungen zum Bachelor- und Masterstudium,
    
2.  das Zulassungsverfahren in zulassungsbeschränkten Studiengängen,
    
3.  Studienberatung,
    
4.  Teilzeitstudium,
    
5.  Aufbau, Modularisierung und Leistungspunktesystem,
    
6.  Regelstudienzeit und Abschlussgrade,
    
7.  Lehr- und Lernformen,
    
8.  Zeiträume für Studienaufenthalte an anderen Hochschulen und in der Praxis (Mobilitätsfenster),
    
9.  Bildung der Modul- und Gesamtnoten,
    
10.  Härteregelungen und Nachteilsausgleich nach § 19 Absatz 1 Satz 4,
    
11.  Regelungen zu den Fristen nach § 22 Absatz 1 Satz 4,
    
12.  Nachteilsausgleich nach § 22 Absatz 1 Satz 5,
    
13.  Anerkennung und Verfahren zur Anrechnung von Studien- und Prüfungsleistungen eines vorangegangenen Studiums bei der Aufnahme oder Fortsetzung eines Studiums nach § 24 Absatz 4 sowie zur Anrechnung von außerhalb des Hochschulwesens erworbenen Kenntnissen und Fähigkeiten nach § 24 Absatz 5,
    
14.  Organisation und Durchführung von Hochschulprüfungen,
    
15.  Zulassungsvoraussetzungen zu Hochschulprüfungen,
    
16.  Prüfungsanforderungen und Prüfungsverfahren unter Berücksichtigung der Vorgaben von § 22 Absatz 1 Satz 2,
    
17.  Verfahren zur Wiederholung von Prüfungen und bei Verhinderung einer Teilnahme an Prüfungen,
    
18.  Prüfungsformen,
    
19.  Anfertigung von Abschlussarbeiten,
    
20.  Zeugnis, Urkunde, Diploma Supplement.
    

Studiengangsspezifisch darüber hinaus erforderliche Regelungen trifft die Hochschule in den entsprechenden Ordnungen.

(2) Die Rahmenordnungen werden von dem nach der Grundordnung zuständigen Organ im Benehmen mit den organisatorischen Grundeinheiten der Hochschule erlassen.
Sie bedürfen der Genehmigung der Präsidentin oder des Präsidenten und der für die Hochschulen zuständigen obersten Landesbehörde.
Erlässt eine Hochschule bis drei Monate nach Inkrafttreten dieser Bestimmung keine Rahmenordnung, so kann das für die Hochschulen zuständige Mitglied der Landesregierung eine vorläufige Rahmenordnung erlassen, die mit Inkrafttreten der Rahmenordnung der Hochschule außer Kraft tritt.
Bis zum Inkrafttreten der Rahmenordnung oder der vorläufigen Rahmenordnung sind die Prüfungsordnungen der organisatorischen Grundeinheiten der Hochschule der für die Hochschulen zuständigen obersten Landesbehörde vor Inkrafttreten anzuzeigen.

### § 24   
Einstufungsprüfung; Anerkennung von Leistungen

(1) In einer besonderen Hochschulprüfung (Einstufungsprüfung) können Studienbewerberinnen und Studienbewerber nachweisen, dass sie über Kenntnisse und Fähigkeiten verfügen, die eine Einstufung in ein höheres Fachsemester rechtfertigen.

(2) Eine in einer Studien- oder Prüfungsordnung vorgesehene Studienleistung wird auch durch die erfolgreiche Teilnahme an einer entsprechenden Fernstudieneinheit nachgewiesen, soweit diese der entsprechenden Leistung des Präsenzstudiums inhaltlich gleichwertig ist.
Die Gleichwertigkeit wird bei Studiengängen, die mit einer Hochschulprüfung abgeschlossen werden, von der Hochschule bei Studiengängen, die mit einer staatlichen Prüfung abgeschlossen werden, von der für die Prüfung zuständigen Stelle festgestellt.

(3) Zum Nachweis von Studien- und Prüfungsleistungen des Präsenz- und des Fernstudiums wird ein Leistungspunktsystem eingeführt, das auch die Übertragung erbrachter Leistungen auf andere Studiengänge derselben oder einer anderen Hochschule ermöglicht.

(4) Bei der Aufnahme oder Fortsetzung eines Studiums oder bei einem Studiengangwechsel entscheidet die Hochschule, an der das Studium aufgenommen oder fortgesetzt werden soll, über die Anerkennung von Leistungen eines vorangegangenen Studiums.
Leistungen sind anzuerkennen, sofern sie sich nicht wesentlich unterscheiden.
Lehnt die Hochschule die Anerkennung nach Satz 2 ab, wird auf Antrag eine Anerkennungsprüfung durch die Hochschule durchgeführt, sofern die oder der Studierende oder die Studienbewerberin oder der Studienbewerber glaubhaft macht, die entsprechenden Kenntnisse und Fähigkeiten anderweitig erworben zu haben.
Die Sätze 1 bis 3 gelten auch für Studien- und Prüfungsleistungen, die an ausländischen Hochschulen erbracht worden sind.

(5) Außerhalb des Hochschulwesens erworbene Kenntnisse und Fähigkeiten sind bis zu 50 Prozent auf ein Hochschulstudium anzurechnen, wenn sie nach Inhalt und Niveau dem Teil des Studiums gleichwertig sind, der ersetzt werden soll.

#### § 25   
Wissenschaftliche Weiterbildung

(1) Die Hochschulen sollen zur Vermittlung weiterer wissenschaftlicher, künstlerischer und beruflicher Qualifikationen oder zur Heranbildung des wissenschaftlichen und künstlerischen Nachwuchses Angebote der wissenschaftlichen Weiterbildung entwickeln.
Die Inhalte der wissenschaftlichen Weiterbildung sollen mit dem übrigen Lehrangebot abgestimmt werden und berufspraktische Erfahrungen und Bedürfnisse einbeziehen.

(2) Weiterbildende Studiengänge vermitteln einen Hochschulabschluss nach § 28 Absatz 1 Satz 2.
Sie werden durch Studien- und Prüfungsordnungen geregelt.

(3) Das hauptberuflich tätige wissenschaftliche und künstlerische Personal kann Lehraufträge im Bereich der Weiterbildung auch als Nebentätigkeit wahrnehmen, sofern die Lehrverpflichtung erfüllt ist.
Die Hochschulen setzen die Höhe der Vergütung für Lehraufgaben im Rahmen der erzielten Einnahmen aus Gebühren und privatrechtlichen Entgelten fest.

(4) In begründeten Fällen können die Hochschulen in der wissenschaftlichen Weiterbildung mit geeigneten Einrichtungen außerhalb des Hochschulbereichs kooperieren, wobei die Hochschulen für Studieninhalte und Prüfungen verantwortlich bleiben.
Durch einen Kooperationsvertrag, der der für die Hochschulen zuständigen obersten Landesbehörde anzuzeigen ist, können Durchführung und Vermarktung des Weiterbildungsangebots der kooperierenden Einrichtung übertragen werden.

#### § 26   
Lehrangebot

(1) Die Hochschule stellt das zur Einhaltung der Studienordnungen erforderliche Lehrangebot sicher.
Dabei sollen Möglichkeiten von Fernstudien sowie der Informations- und Kommunikationstechnik genutzt werden.

(2) Für Lehre, die mit Mitteln Dritter finanziert wird, gilt § 36.

#### § 27   
Qualitätssicherung; Evaluation der Lehre

(1) Die Hochschulen entwickeln ein System zur Sicherung der Qualität ihrer Aufgabenerfüllung in Forschung und Lehre, Studium und Weiterbildung, bei der Förderung des wissenschaftlichen und künstlerischen Nachwuchses, bei der internationalen, insbesondere europäischen Zusammenarbeit im Hochschulbereich und bei der Durchsetzung der Chancengleichheit von Frauen und Männern.
Dieses System beinhaltet insbesondere Qualitätssicherungsinstrumente im Bereich von Studium, Prüfungen und Lehre und schließt Maßnahmen der Hochschulen zur Entwicklung und Förderung der Lehrkompetenz ebenso ein wie die Betreuung und Beratung der Studierenden.
Die Hochschulen führen Verbleibstatistiken und werten diese aus.
Das an den Hochschulen hauptberufliche wissenschaftliche und künstlerische Personal soll regelmäßig an Maßnahmen der Hochschulen zur Entwicklung und Förderung der Lehrkompetenz teilnehmen.

(2) Wesentlicher Bestandteil des hochschulinternen Qualitätssicherungssystems ist die regelmäßige Durchführung interner Evaluationen, insbesondere im Bereich der Lehre.
Die Studierenden und die Absolventen sind bei der Evaluation der Lehre zu beteiligen.
Die Studierenden wirken an der Erarbeitung der Evaluationsverfahren mit.
Die Mitglieder und Angehörigen der Hochschulen sind zur Mitwirkung an Evaluationsverfahren, insbesondere durch Erteilung der erforderlichen Auskünfte, verpflichtet.
Die Hochschulen regeln das Verfahren der Evaluation durch Satzung.

(3) Die Hochschulen legen der für sie zuständigen obersten Landesbehörde nach deren Vorgaben regelmäßig Berichte zum Qualitätsmanagement vor.
Die Berichte enthalten insbesondere Aussagen zum Ausbaustand des internen Qualitätssicherungssystems, zu den implementierten Verfahren der Qualitätssicherung, den Ergebnissen interner Evaluationen sowie den Schlussfolgerungen der Hochschule im Hinblick auf eine weitere Verbesserung der Qualität ihrer Aufgabenerfüllung und des Qualitätssicherungssystems unter Einbeziehung der Verbleibstatistiken.
Sie dienen zugleich der regelmäßig durchzuführenden Evaluation durch externe Begutachtungen in den in Absatz 1 genannten Leistungsbereichen; solcher bedarf es nicht, soweit die Leistungsbereiche von anderen regelmäßig durchzuführenden Verfahren der externen Qualitätssicherung hinreichend umfasst sind.

Abschnitt 4   
Hochschulgrade, Promotion und Habilitation
---------------------------------------------------------

#### § 28   
Hochschulgrade

(1) Aufgrund von Hochschulprüfungen, mit denen ein erster berufsqualifizierender Abschluss erworben wird, verleiht die Hochschule einen Bachelorgrad.
Aufgrund von Prüfungen, mit denen ein weiterer berufsqualifizierender Abschluss erworben wird, verleiht die Hochschule einen Mastergrad.
Bachelorabschlüsse verleihen grundsätzlich dieselben Berechtigungen wie Diplomabschlüsse an Fachhochschulen.
Masterabschlüsse verleihen dieselben Berechtigungen wie Diplom- und Magisterabschlüsse an Universitäten und gleichgestellten Hochschulen.

(2) Aufgrund der Hochschulprüfung, mit der ein berufsqualifizierender Abschluss erworben wird, kann die Hochschule einen Diplomgrad mit Angabe der Fachrichtung verleihen.
Aufgrund der Hochschulprüfung an Fachhochschulen oder in Fachhochschulstudiengängen anderer Hochschulen kann der Diplomgrad mit dem Zusatz „Fachhochschule“ („FH“) verliehen werden.
Die Hochschule kann einen Diplomgrad auch aufgrund einer staatlichen Prüfung oder einer kirchlichen Prüfung, mit der ein Hochschulstudium abgeschlossen wird, verleihen.
Die Universitäten können für den berufsqualifizierenden Abschluss eines Studiums einen Magistergrad verleihen.
Eine Hochschule kann für den berufsqualifizierenden Abschluss eines Studiums aufgrund einer Vereinbarung mit einer ausländischen Hochschule andere als die in den Sätzen 1, 2 und 4 genannten Grade verleihen.
Ein Grad nach Satz 5 kann auch zusätzlich zu einem der in den Sätzen 1, 2 und 4 genannten Grade verliehen werden.

(3) Die Hochschule für Film und Fernsehen kann für den berufsqualifizierenden Abschluss eines Studiums andere als die in Absatz 2 genannten Grade verleihen.

(4) Den Urkunden über die Verleihung der akademischen Grade fügen die Hochschulen eine Anlage in deutscher und englischer Sprache bei, die insbesondere die wesentlichen, dem Abschluss zugrunde liegenden Studieninhalte, den Studienverlauf, die mit dem Abschluss erworbene Qualifikation sowie die verleihende Hochschule enthalten muss (Diploma Supplement).

#### § 29   
Verleihung und Führung von Graden

(1) Von einer deutschen staatlichen oder staatlich anerkannten Hochschule verliehene Hochschulgrade, -bezeichnungen oder -titel sowie entsprechende staatliche Grade, Bezeichnungen oder Titel (Grade) können im Geltungsbereich dieses Gesetzes geführt werden.

(2) Grade dürfen nur nach den §§ 28, 31 und 32 verliehen werden.
Bezeichnungen, die Graden zum Verwechseln ähnlich sind, dürfen nicht verliehen werden.

#### § 30   
Ausländische Hochschulgrade

(1) Ein ausländischer Hochschulgrad, der von einer nach dem Recht des Herkunftslandes anerkannten Hochschule und aufgrund eines nach dem Recht des Herkunftslandes anerkannten Hochschulabschlusses nach einem ordnungsgemäß durch Prüfung abgeschlossenen Studium verliehen worden ist, darf in der Form, in der er verliehen wurde, unter Angabe der verleihenden Hochschule geführt werden.
Dabei kann die verliehene Form, soweit erforderlich, in lateinische Schrift übertragen und die im Herkunftsland zugelassene oder nachweislich allgemein übliche Abkürzung verwendet und eine wörtliche Übersetzung in Klammern hinzugefügt werden.
Hochschulgrade aus Mitgliedstaaten der Europäischen Union oder eines anderen Vertragsstaates des Abkommens über den Europäischen Wirtschaftsraum sowie des Europäischen Hochschulinstituts Florenz und der Päpstlichen Hochschulen können ohne Herkunftsbezeichnung geführt werden.
Eine Umwandlung in einen entsprechenden deutschen Grad findet nicht statt; eine Ausnahme hiervon gilt für Berechtigte nach § 10 des Bundesvertriebenengesetzes in der Fassung der Bekanntmachung vom 10. August 2007 (BGBl.
I S. 1902), das zuletzt durch Gesetz vom 6.
September 2013 (BGBl. I S. 3554) geändert worden ist.
Die Sätze 1 bis 4 gelten entsprechend für ausländische staatliche oder kirchliche Grade.

(2) Wer in einem wissenschaftlichen Promotionsverfahren einen Doktorgrad entweder in den in Absatz 1 Satz 3 bezeichneten Staaten oder Institutionen oder an Universitäten der sogenannten Carnegie-Liste der Vereinigten Staaten von Amerika erworben hat, kann anstelle der im Herkunftsland zugelassenen oder nachweislich allgemein üblichen Abkürzung gemäß Absatz 1 Satz 2 die Abkürzung „Dr.“ ohne fachlichen Zusatz und ohne Herkunftsbezeichnung führen.
Dies gilt nicht für Doktorgrade, die ohne Promotionsstudien und -verfahren vergeben werden (sogenannte Berufsdoktorate) und für Doktorgrade, die nach den rechtlichen Regelungen des Herkunftslandes nicht der dritten Ebene der sogenannten Bologna-Klassifikation der Studienabschlüsse zugeordnet sind.
Die für die Hochschulen zuständige oberste Landesbehörde kann durch Erlass für Doktorgrade aus bestimmten Staaten die Führung der Abkürzung „Dr.“ ohne fachlichen Zusatz mit Herkunftsbezeichnung zulassen.
Die gleichzeitige Führung der im Herkunftsland zugelassenen oder nachweislich allgemein üblichen Abkürzungen und der Abkürzung „Dr.“ ist nicht zulässig.

(3) Ein ausländischer Ehrengrad, der von einer nach dem Recht des Herkunftslandes zur Verleihung berechtigten Hochschule oder anderen Stelle verliehen wurde, darf nach Maßgabe der für die Verleihung geltenden Rechtsvorschriften in der verliehenen Form unter Angabe der verleihenden Stelle geführt werden.
Dies gilt nicht, wenn die verleihende Stelle kein Recht zur Vergabe des entsprechenden Grades nach Absatz 1 besitzt.

(4) Die Absätze 1 und 3 gelten sinngemäß für sonstige Hochschultitel und Hochschultätigkeitsbezeichnungen.

(5) Soweit Vereinbarungen und Abkommen der Bundesrepublik Deutschland mit anderen Staaten über Gleichwertigkeiten im Hochschulbereich und Vereinbarungen der Länder der Bundesrepublik Deutschland die Inhaberinnen und Inhaber ausländischer Grade abweichend von den Absätzen 1 bis 3 begünstigen, gehen diese Regelungen vor.

(6) Eine von den Absätzen 1 bis 4 abweichende Grad- oder Titelführung ist untersagt.
Grade oder Titel, die durch Kauf oder sonst in unrechtmäßiger Weise erworben wurden, dürfen nicht geführt werden.
Wer einen Grad oder Titel gemäß den Absätzen 1 bis 4 führt, hat auf Verlangen der jeweils zuständigen Ordnungsbehörde die Berechtigung nachzuweisen.
Ausländische Grade dürfen nicht gegen Entgelt vermittelt werden.

(7) Die für die Hochschulen zuständige oberste Landesbehörde kann eine von ihr vor dem 24. März 2004 erteilte Genehmigung zur Führung eines ausländischen Grades auch nach Inkrafttreten dieses Gesetzes zurücknehmen,

1.  wenn Umstände bekannt werden, dass die für den Erwerb des Grades vorauszusetzenden Prüfungsleistungen offensichtlich nicht erbracht worden sind oder qualitativ hinter den maßgeblichen Anforderungen an den Erwerb eines entsprechenden deutschen akademischen Grades erheblich zurückbleiben,
    
2.  wenn Umstände bekannt werden, dass der Grad aufgrund von Studien- und Prüfungsleistungen verliehen wurde, die bei einer in der Bundesrepublik Deutschland arbeitenden privaten Bildungseinrichtung ohne staatliche Anerkennung erbracht worden sind, oder
    
3.  sobald Anzeichen dafür vorliegen, dass die Verleihung des Grades von der Zahlung von Geld oder der Erbringung geldwerter Leistungen abhängig gemacht wurde, soweit es sich nicht um übliche Studien- oder Prüfungsgebühren handelt.
    

#### § 31   
Promotion

(1) Die Universitäten haben das Promotionsrecht.
Das für die Hochschulen zuständige Mitglied der Landesregierung kann der Hochschule für Film und Fernsehen für einzelne Studiengänge das Promotionsrecht verleihen.
Soweit das Promotionsrecht gegeben ist, darf auch die Doktorwürde ehrenhalber verliehen werden.

(2) Die Promotion dient dem Nachweis der Befähigung zu vertiefter wissenschaftlicher Arbeit.
Sie wird aufgrund einer wissenschaftlichen Arbeit (Dissertation), die auf selbstständiger Forschungstätigkeit beruht, und einer mündlichen Prüfung vorgenommen.
§ 21 Absatz 6 findet Anwendung.

(3) Aufgrund der Promotion wird der Doktorgrad verliehen.
Die Verleihung kann auch in Gestalt des Doktorgrades „Doctor of Philosophy (Ph.D.)“ erfolgen.
Der Grad „Doctor of Philosophy“ kann auch in Form der Abkürzung „Dr.“ ohne fachlichen Zusatz geführt werden.
Eine gleichzeitige Führung der Abkürzungen „Ph.D.“ und „Dr.“ ist unzulässig.
Näheres regelt die Promotionsordnung, die das nach der Grundordnung zuständige Organ des Fachbereichs erlässt und die der Genehmigung der Präsidentin oder des Präsidenten bedarf.

(4) Der Zugang zur Promotion setzt grundsätzlich den erfolgreichen Abschluss eines Hochschulstudiums voraus.
Masterabschlüsse, die an Universitäten und gleichgestellten Hochschulen oder an Fachhochschulen erworben wurden, berechtigen grundsätzlich zur Promotion.
Wer den Masterabschluss an einer Fachhochschule erworben hat, unterliegt den gleichen Zugangsvoraussetzungen zur Promotion wie die Absolventinnen und Absolventen mit Masterabschluss einer Universität.
Inhaberinnen und Inhaber eines Bachelorgrades können auch ohne Erwerb eines weiteren Grades im Wege eines Eignungsfeststellungsverfahrens unmittelbar zur Promotion zugelassen werden.

(5) In die Promotionsordnungen sind nach Anhörung der kooperierenden Fachhochschulen Bestimmungen über ein kooperatives Verfahren zwischen der Universität und den Fachhochschulen aufzunehmen.
Der Erwerb eines universitären Abschlusses darf nicht zur Voraussetzung für eine Zulassung zum Promotionsverfahren gemacht werden.
Die Dissertation soll von einer Hochschullehrerin oder einem Hochschullehrer einer Universität und einer Hochschullehrerin oder einem Hochschullehrer einer Fachhochschule betreut werden.
Hochschullehrerinnen und Hochschullehrer von Fachhochschulen sollen zu Gutachterinnen und Gutachtern und Prüferinnen und Prüfern in Promotionsverfahren nach Satz 1 bestellt werden.

(6) Doktorandinnen und Doktoranden werden als Promotionsstudierende an der Universität immatrikuliert, sofern sie nicht in einem hauptberuflichen Beschäftigungsverhältnis an der Universität stehen oder wegen einer Berufstätigkeit außerhalb der Universität oder aus anderen Gründen auf die Einschreibung verzichten.
In kooperativen Promotionsverfahren zwischen Universitäten und Fachhochschulen können die Doktorandinnen und Doktoranden an der Fachhochschule eingeschrieben werden, wenn sie nicht an der Universität eingeschrieben sind.

(7) Die Promotionsordnung ist vor dem Inkrafttreten der für die Hochschulen zuständigen obersten Landesbehörde anzuzeigen.
Ist die Promotion als Studiengang strukturiert, so bedarf dessen Einrichtung, Änderung und Aufhebung der Genehmigung der für die Hochschulen zuständigen obersten Landesbehörde.

(8) Rechte und Pflichten der Promovierenden, der jeweiligen wissenschaftlichen Betreuer sowie der Fachbereiche sind in Promotionsvereinbarungen zu regeln.
Näheres bestimmen die Hochschulen durch Satzung.
Zur Unterstützung von Aufnahme, Durchführung und erfolgreichem Abschluss von Promotionsvorhaben können an den Universitäten fächerübergreifende Graduiertenzentren geschaffen werden.
Ihr Angebot soll allen Promovierenden zur Verfügung stehen.

#### § 32   
Habilitation

(1) Die Universitäten haben das Habilitationsrecht.
Die Habilitation dient als Nachweis der Befähigung, ein wissenschaftliches Gebiet in Forschung und Lehre selbstständig zu vertreten.

(2) Näheres regelt die Habilitationsordnung.
Sie wird von dem nach der Grundordnung zuständigen Organ des Fachbereichs erlassen und bedarf der Genehmigung der Präsidentin oder des Präsidenten.

(3) Die Habilitationsordnung ist vor dem Inkrafttreten der für die Hochschulen zuständigen obersten Landesbehörde anzuzeigen.

(4) § 21 Absatz 6 findet Anwendung.

#### § 33   
Förderung des wissenschaftlichen und künstlerischen Nachwuchses; Verordnungsermächtigung

(1) Zur Förderung des wissenschaftlichen und künstlerischen Nachwuchses werden nach Maßgabe der im Haushalt dafür vorgesehenen Mittel Stellen und Stipendien für hochqualifizierte wissenschaftliche und künstlerische Nachwuchskräfte bereitgestellt und gewährt.
Dabei sind Frauen besonders zu berücksichtigen.

(2) Zur Bestimmung von Zweck, Art und Umfang der Förderung sowie der Gründe für ihren Widerruf kann das für die Hochschulen zuständige Mitglied der Landesregierung eine Rechtsverordnung erlassen.

#### § 34   
Ordnungswidrigkeiten, Ordnungsmaßnahmen

(1) Ordnungswidrig handelt, wer

1.  entgegen § 29 Absatz 2 Satz 1 Grade oder entgegen § 29 Absatz 2 Satz 2 Graden zum Verwechseln ähnliche Bezeichnungen verleiht,
    
2.  Grade oder Titel führt, obwohl dies nach § 30 Absatz 6 Satz 1 untersagt ist,
    
3.  entgegen § 30 Absatz 6 Satz 2 Grade oder Titel führt, die durch Kauf oder sonst in unrechtmäßiger Weise erworben wurden, oder
    
4.  entgegen § 30 Absatz 6 Satz 4 ausländische Grade gegen Entgelt vermittelt.
    

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu einhunderttausend Euro geahndet werden.
Zuständige Verwaltungsbehörde für die Verfolgung und Ahndung von Ordnungswidrigkeiten nach Satz 1 ist die für die Hochschulen zuständige oberste Landesbehörde.
Die für die Hochschulen zuständige oberste Landesbehörde kann die von den §§ 29 bis 31 abweichende Führung von Hochschulgraden, Hochschultiteln und Hochschultätigkeitsbezeichnungen untersagen.

Abschnitt 5   
Forschung
------------------------

#### § 35   
Aufgaben und Koordination der Forschung

(1) Die Forschung in den Hochschulen dient der Gewinnung wissenschaftlicher Erkenntnisse, der wissenschaftlichen Grundlegung und Weiterentwicklung von Lehre und Studium, der Qualifizierung des wissenschaftlichen Nachwuchses und dem Wissens- und Technologietransfer in alle Bereiche der Gesellschaft.
Gegenstand der Forschung in den Hochschulen können unter Berücksichtigung der Aufgaben der Hochschule alle wissenschaftlichen Bereiche sowie die Anwendung wissenschaftlicher Erkenntnisse in der Praxis einschließlich der Folgen sein, die sich aus der Anwendung wissenschaftlicher Erkenntnisse ergeben können.

(2) Zur gegenseitigen Abstimmung von Forschungsvorhaben und Forschungsschwerpunkten und zur Planung und Durchführung gemeinsamer Forschungsvorhaben wirken die Hochschulen untereinander, mit anderen Forschungseinrichtungen und mit Einrichtungen der überregionalen Forschungsplanung und Forschungsförderung zusammen.

(3) Die Vorschriften dieses Abschnitts gelten für Entwicklungsvorhaben im Rahmen angewandter Forschung sowie für künstlerische Entwicklungsvorhaben sinngemäß.

#### § 36   
Forschung und Lehre mit Mitteln Dritter

(1) Die in der Forschung und Lehre tätigen Hochschulmitglieder sind berechtigt, im Rahmen ihrer dienstlichen Aufgaben auch solche Forschungs- und Lehrvorhaben durchzuführen, die nicht aus den der Hochschule zur Verfügung stehenden Haushaltsmitteln, sondern aus Mitteln Dritter finanziert werden.
Ihre Verpflichtung zur Erfüllung der übrigen Dienstaufgaben bleibt unberührt.
Die Durchführung von Vorhaben nach Satz 1 ist Teil der Hochschulforschung und -lehre.

(2) Ein Hochschulmitglied ist berechtigt, ein Forschungs- oder Lehrvorhaben nach Absatz 1 in der Hochschule durchzuführen, wenn die Erfüllung anderer Aufgaben der Hochschule sowie die Rechte und Pflichten anderer Personen dadurch nicht beeinträchtigt werden und entstehende Folgelasten angemessen berücksichtigt worden sind.
Die Forschungsergebnisse sollen in der Regel in absehbarer Zeit veröffentlicht werden.

(3) Ein Forschungs- oder Lehrvorhaben nach Absatz 1 ist der Hochschule anzuzeigen.
Die Durchführung eines solchen Vorhabens darf nicht von einer Genehmigung abhängig gemacht werden.
Die Inanspruchnahme von Personal, Sachmitteln und Einrichtungen der Hochschule darf nur untersagt oder durch Auflagen beschränkt werden, soweit die Voraussetzungen des Absatzes 2 dies erfordern.

(4) Die Mittel für Forschungs- und Lehrvorhaben, die in der Hochschule durchgeführt werden, sollen von der Hochschule verwaltet werden.
Die Mittel sind für den vom Geldgeber bestimmten Zweck zu verwenden und nach dessen Bedingungen zu bewirtschaften, soweit gesetzliche Bestimmungen nicht entgegenstehen.
Treffen die Bedingungen keine Regelung, so gelten ergänzend die Bestimmungen des Landes.
Auf Antrag des Hochschulmitglieds, das das Vorhaben durchführt, soll von der Verwaltung der Mittel durch die Hochschule abgesehen werden, sofern dies mit den Bedingungen der Geldgeberin oder des Geldgebers vereinbar ist; Satz 3 gilt in diesem Falle nicht.

(5) Aus Mitteln Dritter bezahlte hauptberufliche Mitarbeiterinnen und Mitarbeiter an Forschungs- und Lehrvorhaben, die in der Hochschule durchgeführt werden, sollen vorbehaltlich des Satzes 3 als Personal der Hochschule im Arbeitsvertragsverhältnis eingestellt werden.
Die Einstellung setzt voraus, dass die Mitarbeiterin oder der Mitarbeiter von dem Hochschulmitglied, das das Vorhaben durchführt, vorgeschlagen wurde.
Sofern dies mit den Bedingungen der Geldgeberin oder des Geldgebers vereinbar ist, kann das Hochschulmitglied in begründeten Fällen die Arbeitsverträge mit den Mitarbeiterinnen und Mitarbeitern abschließen.

(6) Finanzielle Erträge der Hochschule aus Forschungs- und Lehrvorhaben, die in der Hochschule durchgeführt werden, insbesondere aus Einnahmen, die der Hochschule als Entgelt für die Inanspruchnahme von Personal, Sachmitteln und Einrichtungen zufließen, stehen der Hochschule für die Erfüllung ihrer Aufgaben zur Verfügung.

Abschnitt 6   
Personal der Hochschule
--------------------------------------

### Unterabschnitt 1   
Allgemeine Bestimmungen

#### § 37   
Dienstrechtliche Zuordnung der Hochschulbediensteten; Verordnungsermächtigung

(1) Die an der Hochschule tätigen Angehörigen des öffentlichen Dienstes (Hochschulbedienstete) stehen im Dienst des Landes.
Das für die Hochschulen zuständige Mitglied der Landesregierung kann durch Rechtsverordnung Aufgaben und Befugnisse einer obersten Dienstbehörde sowie Rechte und Pflichten einer Arbeitgeberin oder eines Arbeitgebers und einer Ausbilderin oder eines Ausbilders auf die Hochschulen übertragen.

(2) Das für die Hochschulen zuständige Mitglied der Landesregierung ist Dienstvorgesetzte oder Dienstvorgesetzter der Präsidentin oder des Präsidenten.
Die Präsidentin oder der Präsident ist Dienstvorgesetzte oder Dienstvorgesetzter des wissenschaftlichen und künstlerischen Personals sowie des nichtwissenschaftlichen Personals der Hochschule.

#### § 38   
Verarbeitung personenbezogener Daten; Verordnungsermächtigung

Die Hochschulen können von wissenschaftlichem und künstlerischem Personal personenbezogene Daten zur Beurteilung der Bewerbungssituation, der Lehr- und Forschungstätigkeit, des Studienangebots sowie des Ablaufs von Studium und Prüfungen verarbeiten.
Das für die Hochschulen zuständige Mitglied der Landesregierung kann durch Rechtsverordnung regeln, unter welchen Voraussetzungen eine Auskunftspflicht besteht oder eine Verarbeitung ohne Einwilligung der betroffenen Personen durchgeführt werden kann; dabei sind der Zweck, der Inhalt und der Umfang der Auskunftspflicht sowie die Erhebungsmerkmale und das Erhebungsverfahren festzulegen.
Hierzu gehören insbesondere Regelungen über

1.  die Erhebung der personenbezogenen Daten,
    
2.  die Speicherung,
    
3.  das Verfahren der Auswertung,
    
4.  die Übermittlung der personenbezogenen Daten, insbesondere die berechtigten Empfängerinnen und Empfänger,
    
5.  die Unterrichtung der betroffenen Personen über Zweck und Inhalt der Befragungen oder Evaluationen,
    
6.  die Ausgestaltung der Auskunftsrechte der betroffenen Personen,
    
7.  die Anonymisierung sowie
    
8.  die Löschung.
    

Eine Verarbeitung für andere Zwecke ist unzulässig.
§ 25 des Brandenburgischen Datenschutzgesetzes gilt entsprechend.

### Unterabschnitt 2   
Hauptberufliches wissenschaftliches und künstlerisches Personal der Hochschule

#### § 39   
Personalkategorien

Das hauptberufliche wissenschaftliche und künstlerische Personal an den Hochschulen besteht aus den Professorinnen und Professoren und Akademischen Mitarbeiterinnen und Mitarbeitern sowie an den Universitäten und Kunsthochschulen auch den Juniorprofessorinnen und Juniorprofessoren.

#### § 40   
Berufung von Hochschullehrerinnen und Hochschullehrern; Verordnungsermächtigung

(1) Die Stellen für Hochschullehrerinnen und Hochschullehrer (Professorinnen und Professoren und Juniorprofessorinnen und Juniorprofessoren) sind öffentlich und im Regelfall international auszuschreiben.
Die Ausschreibung muss Art und Umfang der zu erfüllenden Aufgaben enthalten und in Übereinstimmung mit einer von der für die Hochschulen zuständigen obersten Landesbehörde genehmigten Personalplanung stehen, die Bestandteil des Entwicklungsplanes (§ 3 Absatz 2) ist.
Die Ausschreibung ist der für die Hochschulen zuständigen obersten Landesbehörde drei Wochen vor der Veröffentlichung anzuzeigen.
Ausschreibungen im Sinne dieses Gesetzes sind auch Ausschreibungen durch Forschungsförderungsorganisationen im Rahmen von Förderprogrammen für Personen, die die Einstellungsvoraussetzungen für Hochschullehrerinnen und Hochschullehrer erfüllen.
Einer erneuten Ausschreibung und der Durchführung eines Berufungsverfahrens bedarf es nicht, wenn ein befristetes Angestelltenverhältnis oder ein Beamtenverhältnis auf Zeit mit einer Professorin oder einem Professor nach Fristablauf fortgesetzt werden soll und die Stelle vor der befristeten Besetzung unbefristet ausgeschrieben war oder die Professorin oder der Professor einen Ruf einer anderen Hochschule auf eine höherwertige Professur erhalten hat.
Im letzten Fall ist die Genehmigung der zuständigen obersten Landesbehörde erforderlich; sie kann verlangen, dass die Hochschule eines oder mehrere Gutachten von auf dem Berufungsgebiet anerkannten auswärtigen Wissenschaftlerinnen oder Wissenschaftlern oder Künstlerinnen oder Künstlern einholt.
Von einer Ausschreibung kann im begründeten Einzelfall im Benehmen mit der Gleichstellungsbeauftragten auch abgesehen werden, wenn eine Juniorprofessorin oder ein Juniorprofessor der Hochschule auf eine Professur berufen werden soll.
Wird Personen übergangsweise bis zur endgültigen Besetzung einer Professorenstelle oder zur Vertretung von Professorinnen wegen Mutterschutz oder von Professorinnen oder Professoren wegen Elternzeit die Wahrnehmung der Aufgaben einer Professorin oder eines Professors übertragen (Professorinnen- oder Professorenstellenvertretung), sind dafür eine vorherige Ausschreibung und Durchführung eines Berufungsverfahrens entbehrlich.

(2) Zur Vorbereitung von Berufungsvorschlägen werden Berufungskommissionen gebildet.
Die Mitglieder der Berufungskommission werden vom zuständigen Organ des Fachbereichs gewählt mit Ausnahme eines stimmberechtigten Mitglieds, das die Präsidentin oder der Präsident bestimmt.
Die Wahl der Mitglieder der Berufungskommission durch das zuständige Organ des Fachbereichs erfolgt nach Gruppen getrennt, mit Ausnahme derjenigen Mitglieder, die sich keiner Gruppe zuordnen lassen.
Ein Mitglied der Gruppe der Hochschullehrerinnen und Hochschullehrer kann einem anderen Fachbereich angehören.
Die Gruppe der Akademischen Mitarbeiterinnen und Mitarbeiter sowie die Gruppe der Studierenden stellen jeweils mindestens ein Mitglied.
In Fachbereichen an Fachhochschulen mit weniger als drei Akademischen Mitarbeiterinnen und Mitarbeitern kann in Ausnahmefällen anstelle der Akademischen Mitarbeiterin oder des Mitarbeiters nach Satz 5 ein Mitglied der Gruppe der sonstigen Mitarbeiterinnen und Mitarbeiter bei entsprechender wissenschaftlicher Qualifikation, Funktion, Verantwortung und Betroffenheit stimmberechtigtes Mitglied der Berufungskommission sein.
Den vom zuständigen Organ des Fachbereichs gewählten Vorsitz führt eine Hochschullehrerin oder ein Hochschullehrer der Hochschule.
Für das Stimmgewicht der Hochschullehrerinnen und Hochschullehrer gilt § 61 Absatz 1 Satz 5 und 7 entsprechend.
Den Berufungskommissionen sollen hochschulexterne sachverständige Personen angehören.
Mindestens 40 Prozent der stimmberechtigten Mitglieder sollen Frauen sein, darunter mindestens eine Hochschullehrerin.
Bei der Besetzung von Stellen für Professorinnen und Professoren mit der Qualifikation gemäß § 41 Absatz 1 Nummer 4 Buchstabe a oder Buchstabe b soll die Mehrheit der Professorinnen und Professoren in der Berufungskommission die entsprechende Qualifikation besitzen.

(3) Der Berufungsvorschlag hat mindestens die Namen von drei Bewerberinnen oder Bewerbern in einer Rangfolge zu enthalten; er kann Nichtbewerberinnen oder Nichtbewerber berücksichtigen.
Dem Berufungsvorschlag sollen mindestens zwei vergleichende Gutachten von auf dem Berufungsgebiet anerkannten, auswärtigen Wissenschaftlerinnen oder Wissenschaftlern oder Künstlerinnen oder Künstlern beigefügt werden.
Mit dem Vorschlag sind außerdem auf Verlangen der für die Hochschulen zuständigen obersten Landesbehörde alle eingegangenen Bewerbungen vorzulegen.
Die für die Hochschulen zuständige oberste Landesbehörde, in den Fällen des Absatzes 5 die Präsidentin oder der Präsident, kann in besonders begründeten Ausnahmefällen einen Berufungsvorschlag mit weniger als drei Namen zulassen.
Bei der Berufung auf eine Professur können Juniorprofessorinnen oder Juniorprofessoren der eigenen Hochschule nur dann berücksichtigt werden, wenn sie nach ihrer Promotion die Hochschule gewechselt hatten oder mindestens zwei Jahre außerhalb der berufenden Hochschule wissenschaftlich tätig waren.
Im Ausnahmefall können sie auch dann berücksichtigt werden, wenn sie aufgrund ausgezeichneter Lehr- und Forschungsleistungen einen Ruf an eine andere Universität oder Forschungseinrichtung erhalten haben.
Akademische Mitarbeiterinnen oder Mitarbeiter der eigenen Hochschule können nur in begründeten Ausnahmefällen und, wenn zusätzlich die Voraussetzungen des Satzes 5 vorliegen, bei der Berufung auf eine Professur berücksichtigt werden.

(4) Das für die Hochschulen zuständige Mitglied der Landesregierung beruft auf Vorschlag des zuständigen Organs der Hochschule die Hochschullehrerinnen und Hochschullehrer, soweit das Recht zur Berufung der Hochschullehrerinnen und Hochschullehrer nicht der Hochschule übertragen ist.
Eine Bindung an die im Berufungsvorschlag genannte Rangfolge besteht nicht.
Wird keine vorgeschlagene Bewerberin oder kein vorgeschlagener Bewerber berufen, ist ein neuer Vorschlag einzureichen.
Die Berufung von Nichtbewerberinnen und Nichtbewerbern ist zulässig.

(5) Das Recht zur Berufung der Hochschullehrerinnen und Hochschullehrer nach Absatz 4 kann den Hochschulen übertragen werden.
Die Übertragung erfolgt für jede Hochschule jeweils durch Rechtsverordnung des für die Hochschulen zuständigen Mitglieds der Landesregierung.
Soweit das Berufungsrecht der Hochschule übertragen ist, entscheidet die Präsidentin oder der Präsident.
Die Übertragung des Berufungsrechts setzt voraus, dass die Hochschule eine Berufungsordnung erlassen hat, die von der für die Hochschulen zuständigen obersten Landesbehörde genehmigt worden ist.
In den Berufungsordnungen, die als Satzung zu erlassen sind, treffen die Hochschulen nähere Regelungen über das Berufungsverfahren, insbesondere Regelungen über den Inhalt der Stellenausschreibungen, über die Wahl und Zusammensetzung der Berufungskommission, über das Auswahlverfahren und dessen Dokumentation, über die Gutachten nach Absatz 3 Satz 2, über den Beschluss zur Berufungsliste, über die Information und Betreuung von Bewerberinnen und Bewerbern sowie über Fristen für die Durchführung des Berufungsverfahrens und die Rufannahme, nach deren Überschreitung das Berufungsverfahren als unerledigt abgeschlossen gilt.
Erlässt eine Hochschule keine Berufungsordnung, obwohl sie ansonsten die Gewähr für die Gesetzmäßigkeit der Berufungsverfahren und die Effektivität der Berufungspraxis bietet, so kann das für die Hochschulen zuständige Mitglied der Landesregierung eine vorläufige Berufungsordnung erlassen, die mit Inkrafttreten der Berufungsordnung der Hochschule außer Kraft tritt.

(6) Zur Unterstützung der Rechtsaufsicht kann eine Sachverständigenkommission eingesetzt werden, die stichprobenartig die Gesetzmäßigkeit des Berufungsverfahrens und die Effektivität der Berufungspraxis an den Hochschulen, denen das Berufungsrecht übertragen wurde, überprüft.
Bestehen berechtigte Zweifel an der Gesetzmäßigkeit des Berufungsverfahrens oder der Effektivität der Berufungspraxis an einer Hochschule, kann der Hochschule das Berufungsrecht durch Rechtsverordnung des für die Hochschulen zuständigen Mitglieds der Landesregierung entzogen werden.
In der Sachverständigenkommission sind vertreten:

1.  eine Hochschullehrerin oder ein Hochschullehrer einer Universität,
    
2.  eine Hochschullehrerin oder ein Hochschullehrer einer Fachhochschule,
    
3.  eine Hochschullehrerin oder ein Hochschullehrer mit Erfahrungen auf dem Gebiet der Hochschuldidaktik,
    
4.  eine hochschulexterne sachverständige Person und
    
5.  eine Vertreterin oder ein Vertreter der für die Hochschulen zuständigen obersten Landesbehörde.
    

In der Sachverständigenkommission soll kein Mitglied einer staatlichen Hochschule des Landes Brandenburg mitwirken.
Mindestens ein Mitglied soll die Befähigung zum Richteramt haben.
Mindestens zwei Mitglieder sollen Frauen sein.
Die Mitglieder nach Satz 3 Nummer 1 bis 4 werden von dem für die Hochschulen zuständigen Mitglied der Landesregierung befristet bestellt.
Das Nähere regelt das für die Hochschulen zuständige Mitglied der Landesregierung durch eine Verwaltungsvorschrift.
Die Mitgliedschaft in der Sachverständigenkommission gehört für Landesbedienstete zu den dienstlichen Aufgaben.
Für auswärtige Mitglieder der Sachverständigenkommission gilt § 77 Absatz 7 Satz 1 entsprechend.

(7) Bei der Berufung von Hochschullehrerinnen und Hochschullehrern gilt § 7 Absatz 4 entsprechend.

(8) Abweichend von Absatz 3 Satz 1 bis 4 können in Ausnahmefällen aufgrund exzellenter Lehr- und Forschungsleistungen herausragend ausgewiesene Persönlichkeiten ohne Ausschreibung der Stelle in einem außerordentlichen Berufungsverfahren berufen werden.
Soweit das Berufungsrecht einer Hochschule nach Absatz 5 übertragen ist, erfolgt die Berufung im Einvernehmen mit der für die Hochschulen zuständigen obersten Landesbehörde.
In dem Berufungsvorschlag hat die Berufungskommission zu begründen, inwiefern die Persönlichkeit die mit der zu besetzenden Professur verbundenen hohen Qualitätsstandards erfüllt und aufgrund ihrer Erfahrungen und bisherigen Leistungen offenkundig geeignet ist, das Profil des Fachbereichs und der Hochschule zu stärken.
Dem Berufungsvorschlag sind mindestens vier Gutachten von auf dem Berufungsgebiet anerkannten auswärtigen Wissenschaftlerinnen oder Wissenschaftlern oder Künstlerinnen oder Künstlern beizufügen, von denen mindestens zwei im Ausland tätig sein sollen.
Zur Beschleunigung des außerordentlichen Berufungsverfahrens können die Hochschulen in ihren Berufungsordnungen Abweichungen von der Wahl und Zusammensetzung der Berufungskommission nach Absatz 2 Satz 2 bis 7 sowie von der Zuständigkeit für die Entscheidung über den Berufungsvorschlag nach § 64 Absatz 2 Nummer 7 Halbsatz 2 und § 72 Absatz 2 Nummer 4 regeln.

(9) Zur Förderung der Zusammenarbeit in Forschung und Lehre zwischen einer Hochschule und einer außerhochschulischen Forschungseinrichtung können diese die Durchführung von gemeinsamen Berufungsverfahren vereinbaren.
Die Vereinbarung bedarf der Genehmigung der für die Hochschulen zuständigen obersten Landesbehörde.
Die Besetzung der Berufungskommission erfolgt grundsätzlich nach Absatz 2 Satz 2.
Abweichend hiervon ist die Forschungseinrichtung berechtigt, die Hälfte der Mitglieder zu bestimmen, die den Gruppen der Hochschullehrerinnen und Hochschullehrer und der Akademischen Mitarbeiterinnen und Mitarbeiter zuzuordnen sind.
Bewerberinnen und Bewerber können aufgrund eines gemeinsamen Berufungsverfahrens in die mitgliedschaftsrechtliche Stellung einer Hochschullehrerin oder eines Hochschullehrers nach § 60 an der Hochschule, die am gemeinsamen Berufungsverfahren beteiligt war, auch berufen werden, ohne dass ein Beamten- oder Angestelltenverhältnis zum Land begründet wird.
In diesem Fall wird die berufene Bewerberin oder der berufene Bewerber in einem privatrechtlichen Arbeitsverhältnis an der am gemeinsamen Berufungsverfahren beteiligten Forschungseinrichtung außerhalb des Hochschulbereichs beschäftigt mit der Verpflichtung, mindestens zwei Semesterwochenstunden an der am gemeinsamen Berufungsverfahren beteiligten Hochschule zu lehren und dem Recht, für die Dauer des Beschäftigungsverhältnisses an der Forschungseinrichtung außerhalb des Hochschulbereichs die Bezeichnung „Professorin“ oder „Professor“ als Berufsbezeichnung zu führen.
Die für die Hochschulen zuständige oberste Landesbehörde kann im Falle von Satz 5 einen Verzicht auf die Ausschreibung zulassen.

(10) An jeder Hochschule wird mindestens eine Berufungsbeauftragte oder ein Berufungsbeauftragter bestellt.
Berufungsbeauftragte wirken qualitätssichernd und standardbildend als nicht stimmberechtigte Mitglieder der Berufungskommissionen in den Berufungsverfahren mit.
Sie unterrichten die Hochschulleitungen regelmäßig über den Fortgang des Verfahrens und achten darauf, dass die strategischen Ziele hinsichtlich der Hochschulentwicklung sowie die in der Ausschreibung formulierten Auswahlkriterien Berücksichtigung finden.

(11) Die Ausstattung des Fachgebietes einer Hochschullehrerin oder eines Hochschullehrers wird befristet gewährt.
Die Frist beträgt in der Regel fünf Jahre.

(12) Die Zusage zusätzlicher Mittel nach Absatz 11 in Berufungs- und Bleibevereinbarungen kann mit der Verpflichtung verbunden werden, dass die Professorin oder der Professor für eine angemessene, im Einzelnen zu bestimmende Zeit an der Hochschule bleiben wird.
Für den Fall eines von der Professorin oder dem Professor zu vertretenden vorzeitigen Ausscheidens aus der Hochschule kann eine vollständige oder teilweise Erstattung der Mittel nach Satz 1 vereinbart werden.
Die Erstattung setzt voraus, dass nach dem Ausscheiden der Professorin oder des Professors eine anderweitige Nutzung oder Verwertung dieser Mittel nicht oder nur mit wirtschaftlichem Verlust möglich ist.

#### § 41   
Einstellungsvoraussetzungen für Professorinnen und Professoren

(1) Als Professorin oder Professor kann eingestellt werden, wer die allgemeinen dienstrechtlichen Voraussetzungen erfüllt und mindestens folgende weitere Voraussetzungen nachweist:

1.  ein abgeschlossenes Hochschulstudium,
    
2.  pädagogische Eignung,
    
3.  besondere Befähigung zu wissenschaftlicher Arbeit, in der Regel durch eine qualifizierte Promotion oder besondere Befähigung zu künstlerischer Arbeit und
    
4.  darüber hinaus, je nach den Anforderungen der Stelle,
    
    1.  zusätzliche wissenschaftliche oder künstlerische Leistungen oder
        
    2.  besondere Leistungen bei der Anwendung oder Entwicklung wissenschaftlicher Erkenntnisse und Methoden in einer mindestens dreijährigen beruflichen Praxis, von der mindestens zwei Jahre außerhalb des Hochschulbereichs ausgeübt worden sein müssen, und
        
    3.  umfassende Kompetenzen im Wissenschaftsmanagement, insbesondere in Bereichen mit hohem Drittmittelaufkommen oder erheblicher Personalverantwortung.
        

(2) Die zusätzlichen wissenschaftlichen Leistungen nach Absatz 1 Nummer 4 Buchstabe a werden im Rahmen einer Juniorprofessur, im Rahmen einer Tätigkeit als Akademische Mitarbeiterin oder Mitarbeiter an einer Hochschule oder einer außeruniversitären Forschungseinrichtung oder im Rahmen einer wissenschaftlichen Tätigkeit in der Wirtschaft oder in einem anderen gesellschaftlichen Bereich im In- und Ausland erbracht oder durch eine Habilitation nachgewiesen.
Die Qualität der für die Besetzung einer Professur erforderlichen zusätzlichen wissenschaftlichen Leistungen wird ausschließlich und umfassend im Berufungsverfahren bewertet.
Das Brandenburgische Berufsqualifikationsfeststellungsgesetz findet mit Ausnahme des § 17 keine Anwendung.

(3) Auf eine Stelle, deren Funktionsbeschreibung die Wahrnehmung erziehungswissenschaftlicher oder fachdidaktischer Aufgaben in der Lehrerbildung vorsieht, soll nur berufen werden, wer eine dreijährige Schulpraxis nachweist.
Professorinnen und Professoren an Fachhochschulen und Professorinnen und Professoren für anwendungsbezogene Studiengänge an anderen Hochschulen müssen die Einstellungsvoraussetzungen nach Absatz 1 Nummer 4 Buchstabe b erfüllen; in begründeten Ausnahmefällen können sie auch unter der Voraussetzung des Absatzes 1 Nummer 4 Buchstabe a eingestellt werden.

(4) Soweit es der Eigenart des Faches und den Anforderungen der Stelle entspricht, kann abweichend von den Absätzen 1 bis 3 als Professorin oder Professor eingestellt werden, wer hervorragende fachbezogene Leistungen in der Praxis und pädagogische Eignung nachweist.

#### § 42   
Dienstrechtliche Aufgaben der Hochschullehrerinnen und Hochschullehrer

(1) Die Hochschullehrerinnen und Hochschullehrer nehmen die ihrer Hochschule obliegenden Aufgaben in Wissenschaft und Kunst durch Forschung, Lehre und Weiterbildung sowie durch Förderung des wissenschaftlichen und künstlerischen Nachwuchses in ihren Fächern nach näherer Ausgestaltung ihres Dienstverhältnisses selbstständig wahr.
Zu ihren hauptberuflichen Aufgaben gehören auch die Beteiligung an den Aufgaben der Studienreform und Studienberatung, die Mitwirkung an der Selbstverwaltung der Hochschule, die Abnahme von Hochschulprüfungen, die Beteiligung an Staatsprüfungen, die Förderung des Wissens- und Technologietransfers und die Mitgliedschaft in der Sachverständigenkommission nach § 40 Absatz 6.
Die Wahrnehmung von Aufgaben in Einrichtungen der Wissenschaftsförderung, die überwiegend aus staatlichen Mitteln finanziert werden, soll auf Antrag der Hochschullehrerin oder des Hochschullehrers zur dienstlichen Aufgabe erklärt werden, wenn dies mit der Erfüllung ihrer oder seiner übrigen Aufgaben vereinbar ist.

(2) Die Hochschullehrerinnen und Hochschullehrer sind im Rahmen der für ihr Dienstverhältnis geltenden Regelungen verpflichtet, Lehrveranstaltungen ihrer Fächer in allen Studiengängen abzuhalten.
Soweit es ihnen zumutbar ist, kann ihnen auch die Durchführung anderer Lehrveranstaltungen übertragen werden.
Sie haben im Rahmen der für ihr Dienstverhältnis geltenden Regelungen die zur Sicherstellung des Lehrangebots getroffenen Entscheidungen der Hochschulorgane zu verwirklichen.

(3) Art und Umfang der von Hochschullehrerinnen und Hochschullehrern wahrzunehmenden Aufgaben richten sich unter dem Vorbehalt einer Überprüfung in angemessenen Abständen nach der Ausgestaltung des Dienstverhältnisses und der Funktionsbeschreibung der Stelle.
Ihnen können überwiegend Aufgaben in der Forschung oder in der Lehre übertragen werden.
Bei Juniorprofessorinnen und Juniorprofessoren dürfen Festlegungen nach den Sätzen 1 und 2 das Ziel der Qualifizierung für die Berufung zu Professorinnen und Professoren an einer Universität nicht beeinträchtigen.

(4) Zur Durchführung von Forschungs- und Entwicklungsvorhaben oder zur Aktualisierung ihrer Kenntnisse in der Berufspraxis sollen Hochschullehrerinnen und Hochschullehrer von der Präsidentin oder dem Präsidenten in angemessenen Zeitabständen unter Fortzahlung ihrer Dienstbezüge auf Antrag für ein Semester von ihren übrigen dienstlichen Aufgaben freigestellt werden, wenn eine ordnungsgemäße Vertretung gewährleistet ist und über die Vertretung hinaus keine zusätzlichen Kosten entstehen.
Über die Ergebnisse der durchgeführten Forschungs- und Entwicklungsvorhaben ist der Dekanin oder dem Dekan zu berichten.
Eine Freistellung darf nur erfolgen, wenn die Hochschullehrerin oder der Hochschullehrer der zu erbringenden Lehrverpflichtung vor einer Freistellung nachgekommen ist.
Eine Freistellung darf frühestens nach jedem siebten Semester gewährt werden.
Für jedes Jahr einer Amtszeit als Dekanin oder Dekan verkürzt sich die Frist um ein Semester.
Die Präsidentin oder der Präsident kann Freistellungen von mehr als einem Semester oder früher als nach sieben Semestern im Einvernehmen mit der für die Hochschulen zuständigen obersten Landesbehörde gewähren, wenn eine ordnungsgemäße Vertretung gewährleistet ist und keine zusätzlichen Kosten entstehen.

#### § 43   
Dienstrechtliche Stellung der Professorinnen und Professoren

(1) Mit Professorinnen und Professoren können Angestelltenverhältnisse oder Beamtenverhältnisse auf Lebenszeit oder auf Zeit begründet werden; eine Probezeit ist nicht zurückzulegen.
Wird ein Angestelltenverhältnis begründet, soll die Vergütung, soweit allgemeine dienst- oder haushaltsrechtliche Regelungen nicht entgegenstehen, der Besoldung beamteter Professorinnen und Professoren entsprechen.
Insbesondere bei der Erstberufung zur Professorin oder zum Professor und bei der Berufung zur Professorin oder zum Professor zwecks Deckung eines vorübergehenden Lehrbedarfs ist die Begründung eines befristeten Angestelltenverhältnisses oder eines Beamtenverhältnisses auf Zeit zulässig; dies gilt nicht im Falle der Erstberufung einer Juniorprofessorin oder eines Juniorprofessors, die oder der sich nach § 46 Absatz 1 Satz 2 und Absatz 2 bewährt hat, und im Falle einer außerordentlichen Berufung.
Die Dauer des befristeten Angestelltenverhältnisses oder des Beamtenverhältnisses auf Zeit ist auf höchstens fünf Jahre begrenzt, im Falle der Erstberufung beträgt sie mindestens zwei Jahre.
Eine erneute zeitlich beschränkte Berufung zur Professorin oder zum Professor ist zulässig, sofern hierdurch im Falle eines befristeten Angestelltenverhältnisses eine Gesamtdauer von zehn Jahren, im Falle eines Beamtenverhältnisses auf Zeit eine Gesamtdauer von fünf Jahren, nicht überschritten wird.

(2) Ist im Falle einer Erstberufung die Professorin oder der Professor in ein Beamtenverhältnis auf Zeit berufen worden, so kann das Beamtenverhältnis vor Ablauf der Amtszeit in ein Beamtenverhältnis auf Lebenszeit umgewandelt werden, wenn die Stelle vor der befristeten Besetzung unbefristet ausgeschrieben war und die Professorin oder der Professor den Ruf auf eine unbefristete und mindestens gleichwertige Professur an einer anderen Hochschule vorlegt oder ein gleichwertiges Einstellungsangebot eines anderen Arbeitgebers glaubhaft macht.
Für Professorinnen und Professoren in einem befristeten Angestelltenverhältnis gilt Entsprechendes.

(3) Für die Berufung von Professorinnen und Professoren in das Beamtenverhältnis auf Lebenszeit gilt § 3 Absatz 2 des Landesbeamtengesetzes vom 3.
April 2009 (GVBl.
I S. 26), das zuletzt durch Artikel 2 des Gesetzes vom 5.
Dezember 2013 (GVBl.
I Nr. 37 S. 9) geändert worden ist, mit der Abweichung, dass die Vollendung des 50. Lebensjahres maßgeblich ist.

#### § 44   
Dienstrechtliche Sonderregelungen

(1) Hochschulbedienstete nach § 39 sollen ihren Erholungsurlaub in der vorlesungsfreien Zeit nehmen.
Hochschullehrerinnen und Hochschullehrer können innerhalb dieser Zeit den Zeitpunkt des Erholungsurlaubs unter Berücksichtigung dienstlicher Belange selbst bestimmen.
Sie haben Erholungsurlaub und Dienstreisen der Präsidentin oder dem Präsidenten über die Dekanin oder den Dekan anzuzeigen; Dienstreisen, die der Wahrnehmung von Lehr- oder Prüfungsverpflichtungen entgegenstehen, bedürfen der dienstrechtlichen Genehmigung durch die Dekanin oder den Dekan.
Genehmigungspflichten nach reisekostenrechtlichen Vorschriften bleiben hiervon unberührt.

(2) Auf Hochschullehrerinnen und Hochschullehrer in einem Beamtenverhältnis finden die allgemeinen beamtenrechtlichen Vorschriften über die Probezeit, die Laufbahnen und den einstweiligen Ruhestand keine Anwendung.
Ein Eintritt in den Ruhestand mit Ablauf der Dienstzeit ist für die Beamtinnen und Beamten auf Zeit ausgeschlossen.

(3) Wenn dringende dienstliche Gründe im Einzelfall die Fortführung der Dienstgeschäfte erfordern, kann die für die Versetzung in den Ruhestand zuständige Stelle mit Zustimmung der Beamtin oder des Beamten den Eintritt von Professorinnen und Professoren in den Ruhestand wegen Erreichens der Altersgrenze über das im Landesbeamtengesetz festgelegte Ruhestandsalter um eine bestimmte Frist, die mindestens ein Jahr und höchstens drei Jahre beträgt, hinausschieben.

(4) Bei Hochschullehrerinnen und Hochschullehrern in einem Beamtenverhältnis auf Zeit ist das Dienstverhältnis, soweit dienstliche Gründe nicht entgegenstehen, auf Antrag der Beamtin oder des Beamten aus den in Satz 2 genannten Gründen zu verlängern.
Gründe für eine Verlängerung sind:

1.  Beurlaubung nach einem Landesgesetz zur Ausübung eines mit ihrem oder seinem Amt zu vereinbarenden Mandats,
    
2.  Beurlaubung für eine wissenschaftliche oder künstlerische Tätigkeit oder eine außerhalb des Hochschulbereichs oder im Ausland durchgeführte wissenschaftliche, künstlerische oder berufliche Aus-, Fort- oder Weiterbildung,
    
3.  Wehrpflicht- oder Zivildienst oder
    
4.  Inanspruchnahme von Elternzeit nach den auf Beamtinnen und Beamte anzuwendenden landesrechtlichen Regelungen über die Elternzeit oder ein Beschäftigungsverbot nach den §§ 3, 4, 6 und 8 des Mutterschutzgesetzes oder entsprechenden landesrechtlichen Regelungen in dem Umfang, in dem eine Erwerbstätigkeit nicht erfolgt ist.
    

Satz 1 gilt entsprechend im Falle einer Teilzeitbeschäftigung, einer Ermäßigung der Arbeitszeit nach einem der in Satz 2 Nummer 1 genannten Landesgesetze oder Freistellung zur Wahrnehmung von Aufgaben in einer Personal- oder Schwerbehindertenvertretung oder als Gleichstellungsbeauftragte, wenn die Ermäßigung mindestens ein Fünftel der regelmäßigen Arbeitszeit betrug.
Eine Verlängerung darf den Umfang der Beurlaubung oder der Ermäßigung der Arbeitszeit und in den Fällen des Satzes 2 Nummer 1 und 2 und des Satzes 3 die Dauer von jeweils zwei Jahren nicht überschreiten.
Mehrere Verlängerungen nach Satz 2 Nummer 1 bis 3 und Satz 3 dürfen insgesamt die Dauer von drei Jahren nicht überschreiten.
Verlängerungen nach Satz 2 Nummer 4 dürfen, auch wenn sie mit anderen Verlängerungen zusammentreffen, insgesamt vier Jahre nicht überschreiten.

(5) Soweit für Hochschullehrerinnen und Hochschullehrer ein befristetes Angestelltenverhältnis begründet worden ist, gilt Absatz 4 entsprechend.

(6) Die Vorschriften über die regelmäßige Arbeitszeit der Beamtinnen und Beamten finden auf Hochschullehrerinnen und Hochschullehrer keine Anwendung.
Die Regelungen über die Teilzeitbeschäftigung, Ermäßigung der Arbeitszeit und Beurlaubung der Beamtinnen und Beamten sowie die Vorschriften über den Verlust der Bezüge wegen nicht genehmigten schuldhaften Fernbleibens vom Dienst gelten für Hochschullehrerinnen und Hochschullehrer entsprechend.
Den Professorinnen und Professoren stehen nach dem Eintritt in den Ruhestand die mit der Lehrbefugnis verbundenen Rechte zur Abhaltung von Lehrveranstaltungen und zur Beteiligung an Prüfungsverfahren zu.
Die Höchstdauer der Beurlaubung nach § 79 Absatz 1 und § 81 des Landesbeamtengesetzes gilt nicht bei gemeinsamen Berufungen im Sinne von § 40 Absatz 9.

(7) Hochschullehrerinnen und Hochschullehrer können nur mit ihrer Zustimmung abgeordnet oder versetzt werden.
Abordnung oder Versetzung in ein gleichwertiges Amt an einer anderen Hochschule des Landes Brandenburg sind auch ohne Zustimmung der Hochschullehrerin oder des Hochschullehrers zulässig, wenn die Hochschule oder die Hochschuleinrichtung, an der sie oder er tätig ist, aufgelöst oder mit einer anderen Hochschule zusammengeschlossen wird oder wenn die Studien- oder Fachrichtung, in der sie oder er tätig ist, ganz oder teilweise aufgegeben oder an eine andere Hochschule verlegt wird; in diesen Fällen beschränkt sich eine Mitwirkung der aufnehmenden Hochschule oder Hochschuleinrichtung bei der Einstellung von Hochschullehrerinnen und Hochschullehrern auf eine Anhörung.

(8) Das Recht von Professorinnen und Professoren aufgrund eines nach § 76 des Hochschulrahmengesetzes in der Fassung der Bekanntmachung vom 19.
Januar 1999 (BGBl.
I S. 18), das zuletzt durch Artikel 2 des Gesetzes vom 12. April 2007 (BGBl.
I S. 506, 507) geändert worden ist, ergangenen Gesetzes eines anderen Landes, von ihren amtlichen Pflichten entbunden zu werden (Entpflichtung), bleibt bei einem Wechsel in den Dienst des Landes unberührt.
Die Entpflichtung wird mit dem Ende des Monats wirksam, in dem das laufende Semester endet.

#### § 45   
Einstellungsvoraussetzungen für Juniorprofessorinnen und Juniorprofessoren

(1) Als Juniorprofessorin oder Juniorprofessor kann eingestellt werden, wer die allgemeinen dienstrechtlichen Voraussetzungen erfüllt und die folgenden weiteren Voraussetzungen nachweist:

1.  ein abgeschlossenes Hochschulstudium,
2.  pädagogische Eignung,
3.  besondere Befähigung zu wissenschaftlicher Arbeit, in der Regel durch die herausragende Qualität einer Promotion.

§ 41 Absatz 3 Satz 1 gilt entsprechend.

(2) Die Zeiten einer hauptberuflichen wissenschaftlichen Tätigkeit zwischen der letzten Prüfungsleistung der Promotion und der Bewerbung auf eine Juniorprofessur dürfen in der Regel vier Jahre nicht überschreiten.
Im Fall der Bewerbung auf eine Juniorprofessur, deren Ausschreibung unter der Voraussetzung einer erfolgreichen Evaluation den unmittelbaren Übergang auf eine Lebenszeitprofessur vorsieht (Tenure Track), dürfen die Zeiten einer hauptberuflichen wissenschaftlichen Tätigkeit zwischen der letzten Prüfungsleistung der Promotion und der Bewerbung in der Regel sechs Jahre nicht überschreiten.
Diese Zeiten verlängern sich im Umfang einer Ermäßigung der Arbeitszeit um mindestens ein Fünftel der regelmäßigen Arbeitszeit, die für die Betreuung oder Pflege eines oder mehrerer Kinder unter 18 Jahren oder pflegebedürftiger sonstiger Angehöriger gewährt worden ist.
Ausnahmen von den Regelungen der Sätze 1 und 2 sind nur in besonders begründeten Fällen zulässig.

#### § 46   
Dienstrechtliche Stellung der Juniorprofessorinnen und Juniorprofessoren

(1) Juniorprofessorinnen und Juniorprofessoren werden für die Dauer von bis zu vier Jahren zu Beamtinnen und Beamten auf Zeit ernannt.
Das Beamtenverhältnis einer Juniorprofessorin oder eines Juniorprofessors soll mit ihrer oder seiner Zustimmung auf insgesamt sechs Jahre verlängert werden, wenn sie oder er sich als Hochschullehrerin oder Hochschullehrer bewährt hat; anderenfalls kann das Beamtenverhältnis mit ihrer oder seiner Zustimmung um höchstens ein Jahr verlängert werden.
Für Fälle einer Notlage im Sinne von § 8a Absatz 4 kann das nach den Sätzen 1 und 2 begründete Beamtenverhältnis auf Antrag der Beamtin oder des Beamten ein- oder mehrmalig um insgesamt höchstens ein Jahr verlängert werden, wenn unmittelbar auf die Notlage zurückzuführende Umstände, die die Beamtin oder der Beamte nicht zu vertreten hat, die Bewährung gefährden.
Eine weitere Verlängerung ist nur zulässig:

1.  in den Fällen des § 44 Absatz 4 oder
    
2.  auf Antrag der Beamtin oder des Beamten bei Betreuung eines minderjährigen Kindes um bis zu zwei Jahre je betreutem Kind, soweit dienstliche Gründe nicht entgegenstehen und die Verlängerung notwendig ist, um die nach § 41 Absatz 1 Nummer 4 Buchstabe a erforderlichen zusätzlichen wissenschaftlichen Leistungen erfolgreich nachzuweisen.
    

Eine erneute Einstellung als Juniorprofessorin oder als Juniorprofessor ist unzulässig.

(2) Die Entscheidung über die Bewährung einer Juniorprofessorin oder eines Juniorprofessors nach Absatz 1 Satz 2 trifft die Dekanin oder der Dekan auf der Grundlage einer Stellungnahme des nach der Grundordnung zuständigen Organs des Fachbereichs unter Berücksichtigung eines Bewertungsverfahrens, das mindestens zwei externe Gutachten umfasst.
Die Gutachterinnen oder Gutachter werden vom zuständigen Organ des Fachbereichs bestimmt.
Näheres ist durch Satzung der Hochschule zu regeln.

(3) Mit Juniorprofessorinnen und Juniorprofessoren können auch Angestelltenverhältnisse begründet werden.
In diesem Fall gelten die Absätze 1 und 2 entsprechend.

#### § 47   
Hochschullehrerinnen und Hochschullehrer mit Schwerpunktbildung in der Lehre oder Forschung

(1) Universitäten können Professuren mit Schwerpunkt in der Lehre einrichten.
Der Anteil dieser Professuren an der Gesamtzahl der Professorenstellen einer Universität darf 20 Prozent nicht übersteigen.
Für die Einstellung als Professorin oder Professor mit Schwerpunkt in der Lehre gilt § 41 mit der Maßgabe, dass die pädagogische Eignung nach § 41 Absatz 1 Nummer 2 durch erfolgreiche Absolvierung der Juniorprofessur mit Schwerpunkt in der Lehre oder durch eine vergleichbare Lehrqualifikation nachgewiesen wird.
Die von Professorinnen und Professoren mit Schwerpunkt in der Lehre wahrzunehmenden Aufgaben weisen nach Art und Umfang dauerhaft einen Schwerpunkt in der Lehre auf.
Der Umfang ihrer Lehrverpflichtung übersteigt denjenigen von Professorinnen und Professoren an Universitäten ohne Schwerpunkt in der Lehre um maximal 50 Prozent.
Ein Wechsel zwischen einer Professur mit Schwerpunkt in der Lehre und einer Professur ohne Schwerpunkt in der Lehre ist nur unter den Voraussetzungen der §§ 40 und 41 in Verbindung mit Satz 3 möglich.

(2) Soweit Universitäten Professuren mit Schwerpunkt in der Lehre vorsehen, können sie auch Juniorprofessuren mit Schwerpunkt in der Lehre einrichten.
Der Anteil dieser Juniorprofessuren an der Gesamtzahl der Juniorprofessorenstellen einer Universität darf 20 Prozent nicht übersteigen.
Der Umfang ihrer Lehrverpflichtung kann die maximale Lehrverpflichtung von Juniorprofessorinnen und Juniorprofessoren ohne Schwerpunkt in der Lehre um maximal 35 Prozent übersteigen.
§ 46 gilt mit der Maßgabe, dass in dem Bewertungsverfahren nach § 46 Absatz 2 bei Juniorprofessuren mit Schwerpunkt in der Lehre besonderes Gewicht auf die pädagogische Eignung zu legen ist.
In der Satzung nach § 46 Absatz 2 Satz 3 sind insbesondere Kriterien zur Beurteilung der pädagogischen Eignung von Juniorprofessorinnen und Juniorprofessoren mit Schwerpunkt in der Lehre sowie von diesen zu ergreifende didaktische Qualifikationsmaßnahmen zu regeln.
Die für die Hochschulen zuständige oberste Landesbehörde kann durch Erlass Näheres zur didaktischen Qualifikation regeln.
Wenn eine Juniorprofessorin oder ein Juniorprofessor mit Schwerpunkt in der Lehre auf eine Professur mit Schwerpunkt in der Lehre in einem Beamtenverhältnis auf Lebenszeit oder in einem unbefristeten Angestelltenverhältnis berufen werden soll, kann von einer Ausschreibung der Stelle abgesehen werden.
Die für eine Berufung auf eine Professur ohne Schwerpunkt in der Lehre nach § 41 Absatz 1 Nummer 4 Buchstabe a erforderlichen zusätzlichen wissenschaftlichen Leistungen können auch im Rahmen der Juniorprofessur mit Schwerpunkt in der Lehre erbracht werden.

(3) Fachhochschulen können Professuren mit Schwerpunkt in der Forschung einrichten.
Der Anteil dieser Professuren an der Gesamtzahl der Professorenstellen einer Fachhochschule darf 20 Prozent nicht übersteigen.
Der Umfang der Lehrverpflichtung von Professorinnen und Professoren mit Schwerpunkt in der Forschung darf maximal 50 Prozent unter der Lehrverpflichtung von Professorinnen und Professoren an Fachhochschulen ohne Schwerpunkt in der Forschung liegen.
Für die Einstellung als Professorin oder Professor mit Schwerpunkt in der Forschung gilt § 41 mit der Maßgabe, dass die Voraussetzungen nach § 41 Absatz 1 Nummer 4 Buchstabe a und b erfüllt sein müssen oder zusätzlich zu den Voraussetzungen nach § 41 Absatz 1 Nummer 4 Buchstabe b besondere wissenschaftliche Leistungen in der Forschung nachgewiesen werden.
Die Übernahme einer Professur mit Schwerpunkt in der Forschung ist auch vorübergehend möglich.

(4) Im Übrigen finden auf Hochschullehrerinnen und Hochschullehrer mit Schwerpunktbildung in der Lehre oder Forschung die allgemeinen Vorschriften für Hochschullehrerinnen und Hochschullehrer Anwendung.

#### § 48   
Führung der Bezeichnung „Professorin“ oder „Professor“

(1) Mit der Berufung zur Professorin oder zum Professor oder zur Juniorprofessorin oder zum Juniorprofessor ist die akademische Bezeichnung „Professorin“ oder „Professor“ verliehen.
Juniorprofessorinnen und Juniorprofessoren führen die akademische Bezeichnung bis zum Ende ihres Dienstverhältnisses.

(2) Die Weiterführung der Bezeichnung kann durch die Hochschule mit Zustimmung der zuständigen obersten Landesbehörde wegen erwiesener Unwürdigkeit versagt werden.
Die Bezeichnung darf nach dem Ausscheiden aus der Hochschule wegen Erreichens der Altersgrenze oder Dienstunfähigkeit weitergeführt werden.
Scheidet eine Professorin oder ein Professor aus anderen Gründen aus, so darf die Bezeichnung nur weitergeführt werden, wenn die Hochschule im Einvernehmen mit der für die Hochschule zuständigen obersten Landesbehörde dem zustimmt oder die Person mindestens fünf Jahre in einem Dienstverhältnis als Professorin oder Professor stand.
Auf diesen Zeitraum werden Zeiten, die in einem Dienstverhältnis als Juniorprofessorin oder Juniorprofessor abgeleistet wurden, nicht angerechnet.

#### § 49   
Akademische Mitarbeiterinnen und Mitarbeiter

(1) Akademischen Mitarbeiterinnen und Mitarbeitern obliegen weisungsgebunden im Rahmen der Aufgabenerfüllung der Hochschule wissenschaftliche Dienstleistungen, insbesondere in Wissenschaft, Forschung, Lehre und Weiterbildung, nach Maßgabe ihrer Tätigkeitsbeschreibung.
Sie werden in nach Maßgabe des Wissenschaftszeitvertragsgesetzes vom 12.
April 2007 (BGBl.
I S.
506), das zuletzt durch Artikel 6 Absatz 4 des Gesetzes vom 23.
Mai 2017 (BGBl.
I S.
1228, 1241) geändert worden ist, befristeten oder unbefristeten Angestelltenverhältnissen beschäftigt.
Soweit Akademische Mitarbeiterinnen und Mitarbeiter mit den Aufgaben eines ehemaligen wissenschaftlichen Mitarbeiters in befristeten Angestelltenverhältnissen beschäftigt werden, soll die Dauer des Erstvertrages grundsätzlich zwei Jahre betragen.
Angestelltenverhältnisse, die überwiegend aus Mitteln Dritter finanziert werden und die nach § 2 Absatz 2 des Wissenschaftszeitvertragsgesetzes befristet worden sind, werden in der Regel für die Dauer der Bewilligung der Projektlaufzeit abgeschlossen.
Kürzere Vertragslaufzeiten sind in begründeten Ausnahmefällen möglich.
Von der Möglichkeit der Vertragsverlängerung nach § 2 Absatz 1 Satz 3 des Wissenschaftszeitvertragsgesetzes (familienpolitische Komponente) soll Gebrauch gemacht werden.
Die Regelungen des Wissenschaftszeitvertragsgesetzes bleiben unberührt.
Soweit Akademische Mitarbeiterinnen und Mitarbeiter Hochschullehrerinnen und Hochschullehrern zugeordnet sind, erbringen sie ihre wissenschaftlichen Dienstleistungen unter deren fachlicher Verantwortung und Betreuung.
Die Tätigkeitsbeschreibungen können jederzeit nach Anhörung der Betroffenen nach dem Bedarf der Hochschule geändert werden.

(2) Akademischen Mitarbeiterinnen und Mitarbeitern soll im Rahmen ihrer Dienstaufgaben Gelegenheit zu eigener vertiefter wissenschaftlicher Arbeit gegeben werden.
Akademischen Mitarbeiterinnen und Mitarbeitern, die befristet beschäftigt werden und zu deren Dienstaufgaben die Vorbereitung einer Promotion oder die Erbringung zusätzlicher wissenschaftlicher Leistungen gehört, steht für die eigene vertiefte wissenschaftliche Arbeit mindestens ein Drittel der jeweiligen Arbeitszeit zur Verfügung.

(3) Einstellungsvoraussetzung für Akademische Mitarbeiterinnen und Mitarbeiter ist neben den allgemeinen dienstrechtlichen Voraussetzungen in der Regel ein abgeschlossenes Hochschulstudium.
Abweichend von Satz 1 kann das abgeschlossene Hochschulstudium je nach den fachlichen Anforderungen durch eine mindestens dreijährige künstlerische Berufstätigkeit ersetzt werden.

(4) Wissenschaftliche und künstlerische Mitarbeiterinnen und Mitarbeiter sowie Lehrkräfte für besondere Aufgaben, die über einen Hochschulabschluss verfügen, gehören mit Inkrafttreten dieses Gesetzes zur Gruppe der Akademischen Mitarbeiterinnen und Mitarbeiter.
Für die bei Inkrafttreten dieses Gesetzes vorhandenen wissenschaftlichen und künstlerischen Mitarbeiterinnen und Mitarbeiter sowie für Lehrkräfte für besondere Aufgaben gilt bis zu einer vertraglichen Neufestlegung der individuellen Lehrverpflichtung die Lehrverpflichtung nach der Lehrverpflichtungsverordnung vom 6. September 2002 (GVBl.
II S. 568), die zuletzt durch Artikel 4 des Gesetzes vom 11.
Februar 2013 (GVBl.
I Nr. 4 S. 10) geändert worden ist.

#### § 50   
Lehrverpflichtung; Verordnungsermächtigung

Das für die Hochschulen zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung den Umfang der Lehrverpflichtung des wissenschaftlichen und künstlerischen Personals zu regeln.
Angehörige des wissenschaftlichen und künstlerischen Personals mit Lehraufgaben können verpflichtet werden, Lehr- und Prüfungsaufgaben an einer weiteren Hochschule zu erbringen, wenn dies zur Gewährleistung des Lehrangebots an dieser Hochschule erforderlich ist.

#### § 51   
Nebentätigkeit; Verordnungsermächtigung

Nebentätigkeiten dürfen die dienstlichen Interessen nicht beeinträchtigen.
Wissenschaftliches und künstlerisches Personal ist zur Übernahme einer Nebentätigkeit insoweit verpflichtet, als sie in einem unmittelbaren Zusammenhang mit ihrer hauptamtlichen Tätigkeit steht.
Das für die Hochschulen zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung die Nebentätigkeit des wissenschaftlichen und künstlerischen Personals zu regeln.
Die Rechtsverordnung hat insbesondere Folgendes vorzusehen:

1.  Nebentätigkeiten dürfen die dienstlichen Interessen nicht beeinträchtigen,
    
2.  die Einschränkung und das Verbot von Nebentätigkeiten, soweit diese geeignet sind, die dienstlichen Interessen zu beeinträchtigen,
    
3.  die Inanspruchnahme von Personal, Einrichtungen und Material der Hochschule sowie das dafür abzuführende Nutzungsentgelt,
    
4.  der Nachweis der Einkünfte aus Nebentätigkeit und
    
5.  die Ablieferungspflicht für Vergütungen aus Nebentätigkeiten im öffentlichen Dienst.
    

Das Nutzungsentgelt richtet sich nach den der Hochschule entstehenden Kosten und muss den besonderen Vorteil berücksichtigen, welcher der oder dem Hochschulbediensteten durch die Inanspruchnahme entsteht.

#### § 52   
Gastprofessorinnen und Gastprofessoren und Gastdozentinnen und Gastdozenten

Als Gastprofessorin oder Gastprofessor kann durch die Hochschulen in einem Dienstverhältnis (Angestelltenverhältnis oder freie Mitarbeiterschaft) für einen Zeitraum von höchstens drei Jahren beschäftigt werden, wer die Voraussetzungen des § 41 erfüllt.
Eine weitere Verlängerung ist nicht zulässig.
Eine erneute Einstellung ist nur in begründeten Ausnahmefällen nach mehrjähriger Unterbrechung zulässig.
Die Sätze 1 bis 3 gelten entsprechend für Gastdozentinnen und Gastdozenten.

### Unterabschnitt 3   
Nebenberufliches wissenschaftliches und künstlerisches Personal

#### § 53   
Personalkategorien

(1) Das nebenberuflich tätige Personal mit wissenschaftlichen oder künstlerischen Aufgaben an den Universitäten besteht aus den nebenberuflichen Professorinnen und Professoren, Honorarprofessorinnen und Honorarprofessoren, den außerplanmäßigen Professorinnen und Professoren, den Privatdozentinnen und Privatdozenten, den Lehrbeauftragten und den wissenschaftlichen und künstlerischen Hilfskräften.

(2) Das nebenberufliche Personal mit wissenschaftlichen und künstlerischen Aufgaben an der Hochschule für Film und Fernsehen und den Fachhochschulen besteht aus den nebenberuflichen Professorinnen und Professoren, Honorarprofessorinnen und Honorarprofessoren, den Lehrbeauftragten und den wissenschaftlichen und künstlerischen Hilfskräften.

#### § 54   
Nebenberufliche Professorinnen und Professoren

(1) Insbesondere in künstlerischen Studiengängen können Professorinnen und Professoren nebenberuflich in einem öffentlich-rechtlichen Dienstverhältnis eigener Art oder in einem privatrechtlichen Angestelltenverhältnis mit weniger als der Hälfte der Dienstaufgaben der hauptberuflich tätigen Professorinnen und Professoren befristet oder unbefristet beschäftigt werden, wenn der Hauptberuf zu den Aufgaben der Professur in einem förderlichen inhaltlichen Zusammenhang steht und durch ihn eine Beeinträchtigung dienstlicher Belange nicht zu besorgen ist.
Im Falle einer Erstberufung ist das öffentlich-rechtliche Dienstverhältnis oder das privatrechtliche Angestelltenverhältnis auf mindestens zwei, höchstens jedoch fünf Jahre zu befristen.
§ 40 gilt entsprechend.
Der Anteil der nebenberuflichen Professuren an der Gesamtzahl der Professuren an der Hochschule für Film und Fernsehen darf 20 Prozent nicht übersteigen.
An den übrigen Hochschulen darf er 10 Prozent nicht übersteigen.

(2) Das öffentlich-rechtliche Dienstverhältnis eigener Art wird durch Vertrag in Anlehnung an die für hauptberufliche Professorinnen und Professoren im Beamtenverhältnis geltenden Vorschriften geregelt.
§ 44 Absatz 1 Satz 1 gilt entsprechend.
Abweichend von § 61 Absatz 1 Satz 1 können nebenberufliche Professorinnen und Professoren stimmberechtigte Mitglieder in Berufungskommissionen nach § 40 Absatz 2 in der Gruppe der Hochschullehrerinnen und Hochschullehrer sein.

(3) Nebenberuflich beschäftigten Professorinnen und Professoren können ausschließlich oder überwiegend Aufgaben in der Lehre übertragen werden.

(4) Die Führung der akademischen Bezeichnung „Professorin“ oder „Professor“ kann nach ihrer Anhörung der betroffenen Person untersagt werden, wenn

1.  der Hauptberuf nicht nur vorübergehend wegfällt,
    
2.  sie sich der Führung der Bezeichnung als unwürdig erweist oder
    
3.  sie mindestens zwei Monate schuldhaft ihre Dienstaufgaben nicht erfüllt.
    

Im Übrigen bleibt § 48 unberührt.

#### § 55   
Honorarprofessorinnen und Honorarprofessoren und Ehrenprofessorinnen und Ehrenprofessoren

(1) Zur Honorarprofessorin oder zum Honorarprofessor kann bestellt werden, wer in einem Fach aufgrund besonderer wissenschaftlicher oder künstlerischer Leistungen den Anforderungen entspricht, die an Professorinnen und Professoren gestellt werden.
Die Bestellung setzt eine mehrjährige Lehrtätigkeit an einer Hochschule voraus.
Von diesen Voraussetzungen kann bei besonderen wissenschaftlichen oder künstlerischen Leistungen in einer mehrjährigen beruflichen Praxis abgesehen werden.
Zur Honorarprofessorin oder zum Honorarprofessor einer Hochschule darf nicht bestellt werden, wer dort hauptberuflich tätig ist.
Näheres zum Verfahren und zur Qualitätssicherung bestimmen die Hochschulen in einer Satzung, die von der für die Hochschulen zuständigen obersten Landesbehörde zu genehmigen ist.
Die Sachverständigenkommission bezieht Verfahren zur Bestellung von Honorarprofessorinnen und Honorarprofessoren in ihre Überprüfung nach § 40 Absatz 6 ein.

(2) Die Honorarprofessorinnen und Honorarprofessoren werden auf Antrag eines Fachbereichs von der Präsidentin oder dem Präsidenten bestellt und verabschiedet.
Mit der Bestellung ist die Berechtigung zur Führung der akademischen Bezeichnung „Professorin“ oder „Professor“ verbunden.
Bereits bestellten Honorarprofessorinnen und Honorarprofessoren gilt die Bezeichnung mit Inkrafttreten dieses Gesetzes als mit der Bestellung verliehen.
Die Hochschule kann die Aufhebung der Bestellung durch Satzung regeln.
Die Bezeichnung darf auch nach der Verabschiedung geführt werden, sofern zwischen der Bestellung als Honorarprofessorin oder Honorarprofessor und der Verabschiedung mindestens fünf Jahre liegen und in diesem Zeitraum die Lehrverpflichtung erfüllt wurde.
Darüber entscheidet die Hochschule auf Antrag.

(3) Honorarprofessorinnen und Honorarprofessoren stehen als solche in keinem Dienstverhältnis zur Hochschule.
Sie haben regelmäßig Lehrveranstaltungen durchzuführen; die Präsidentin oder der Präsident regelt den Umfang ihrer Lehrverpflichtung.

(4) Das für die Hochschulen zuständige Mitglied der Landesregierung hat das Recht, Personen, die sich in besonderer Weise auf dem Gebiet der Wissenschaft, Forschung, Kultur oder Technik für das Land Brandenburg verdient gemacht haben, zu Professorinnen und Professoren ehrenhalber zu bestellen.
Diese werden als solche nicht Mitglieder oder Angehörige einer Hochschule.
Mit der Bestellung wird die Bezeichnung „Professorin ehrenhalber (e.h.)“ oder „Professor ehrenhalber (e.h.)“ verliehen.
Die gewürdigten Leistungen sollen einen wissenschaftlichen oder künstlerischen Bezug aufweisen oder einen besonderen Verdienst um die Hochschulen im Land Brandenburg darstellen.

#### § 56   
Privatdozentinnen und Privatdozenten

(1) Wer nach § 32 Absatz 1 Satz 2 die Lehrbefähigung nachweist, kann die Befugnis erhalten, an der Hochschule Lehrveranstaltungen selbstständig durchzuführen (Lehrbefugnis).
Die Präsidentin oder der Präsident entscheidet auf Antrag der oder des Habilitierten über den Inhalt und den Umfang der Lehrbefugnis.
Sie kann verliehen werden, wenn von der Lehrtätigkeit der Bewerberin oder des Bewerbers eine sinnvolle Ergänzung des Lehrangebots der Hochschule zu erwarten ist und keine Gründe entgegenstehen, welche eine Berufung zur Professorin oder zum Professor gesetzlich ausschließen.

(2) Wird ihr oder ihm die Lehrbefugnis verliehen, ist die oder der Habilitierte berechtigt, die Bezeichnung „Privatdozentin“ oder „Privatdozent“ zu führen.
Ein Dienstverhältnis wird damit nicht begründet.

(3) § 55 Absatz 3 gilt entsprechend.
Die Lehrbefugnis erlischt mit Wegfall der Lehrbefähigung oder durch Erlangung der Lehrbefugnis an einer anderen Hochschule, sofern nicht die Hochschule die Fortdauer beschließt.
Die Entscheidungen zur Beendigung der Lehrbefugnis trifft die Präsidentin oder der Präsident auf Antrag des Fachbereichs.

(4) Juniorprofessorinnen oder Juniorprofessoren, die sich nach § 46 Absatz 1 Satz 2 und Absatz 2 bewährt haben, soll nach Ende ihres Dienstverhältnisses auf Antrag die Lehrbefähigung zuerkannt und die Lehrbefugnis entsprechend der Absätze 1 bis 3 verliehen werden.

#### § 57   
Außerplanmäßige Professorinnen und Professoren

Die Präsidentin oder der Präsident kann auf Antrag der Dekanin oder des Dekans Privatdozentinnen oder Privatdozenten, die mindestens vier Jahre habilitiert sind und hervorragende Leistungen in Forschung und Lehre erbracht haben, die Würde einer außerplanmäßigen Professorin oder eines außerplanmäßigen Professors verleihen.
Damit ist die Bezeichnung „außerplanmäßige Professorin“ oder „außerplanmäßiger Professor“ verbunden.
Die Sätze 1 und 2 gelten entsprechend für Juniorprofessorinnen und Juniorprofessoren, auf die § 56 Absatz 4 Anwendung findet.

#### § 58   
Lehrbeauftragte

(1) Zur Ergänzung des Lehrangebots können Lehraufträge erteilt werden.
Die Lehrbeauftragten nehmen die ihnen übertragenen Lehraufgaben selbstständig wahr.
Hauptberuflich tätiges wissenschaftliches und künstlerisches Personal darf nur im Falle des § 25 Absatz 3 an seiner Hochschule Lehraufträge erhalten.

(2) Lehrbeauftragte sollen mindestens ein abgeschlossenes Hochschulstudium, Erfahrungen in der Lehre oder Ausbildung sowie eine mehrjährige berufliche Praxis aufweisen; in anwendungsbezogenen und künstlerischen Studiengängen muss die berufliche Praxis außerhalb des Hochschulbereichs erworben sein.

(3) Der Lehrauftrag begründet ein öffentlich-rechtliches Rechtsverhältnis eigener Art zur Hochschule; er begründet kein Dienstverhältnis.
Er wird für höchstens vier Semesterwochenstunden und in der Regel für längstens zwei Semester von der Dekanin oder dem Dekan erteilt.
Lehraufträge dürfen an Personen, deren einschlägige hauptberufliche Praxis nicht andauert und mehr als zwei Jahre zurückliegt, nur in vier aufeinanderfolgenden Semestern vergeben werden.
Für Personen, die aus Altersgründen aus dem Berufsleben ausgeschieden sind, gilt diese Einschränkung nicht, sofern ihre berufliche Erfahrung weiterhin für die Erteilung des Lehrauftrags maßgebend und hinreichend aktuell ist.
In künstlerischen Studiengängen können Lehraufträge auch zur Sicherstellung des Lehrangebots in einem Fach erteilt werden.
Der Umfang der Lehrtätigkeit einer Lehrbeauftragten oder eines Lehrbeauftragten darf insgesamt die Hälfte des Umfangs der Lehrverpflichtung entsprechender hauptberuflicher Lehrkräfte nicht erreichen.
Der Lehrauftrag kann aus wichtigem Grund zurückgenommen oder widerrufen werden.
Satz 2 findet auf künstlerische Studiengänge keine Anwendung.

(4) Der Lehrauftrag ist zu vergüten; dies gilt nicht, wenn die oder der Lehrbeauftragte auf eine Vergütung schriftlich verzichtet oder die durch den Lehrauftrag entstehende Belastung bei der Bemessung der Dienstaufgaben einer oder eines hauptberuflich im öffentlichen Dienst Tätigen entsprechend berücksichtigt wird.
Lehrauftragsentgelte werden, außer im Falle genehmigter Unterbrechungen, nur insoweit gezahlt, als die Lehrtätigkeit auch tatsächlich ausgeübt wird.

#### § 59   
Wissenschaftliche und künstlerische Hilfskräfte

(1) Personen mit einem erfolgreich abgeschlossenen Hochschulstudium oder fortgeschrittene Studierende können als wissenschaftliche oder künstlerische Hilfskräfte beschäftigt werden.

(2) Sie haben die Aufgabe, Hochschullehrerinnen und Hochschullehrer, in begründeten Ausnahmefällen auch sonstiges wissenschaftliches oder künstlerisches Personal, bei den dienstlichen Aufgaben sowie Studierende unter der fachlichen Anleitung einer Hochschullehrerin oder eines Hochschullehrers im Rahmen der Studienordnung bei ihrem Studium zu unterstützen.
Die Aufgaben sollen zugleich der eigenen Aus- oder Weiterbildung dienen.

### Abschnitt 7   
Mitgliedschaft und Mitwirkung

#### § 60   
Mitglieder und Angehörige

(1) Mitglieder der Hochschule sind die an der Hochschule nicht nur vorübergehend oder gastweise hauptberuflich Tätigen sowie die eingeschriebenen Studierenden einschließlich der Promotionsstudierenden.
Hauptberuflich ist die Tätigkeit, wenn die Arbeitszeit oder der Umfang der Dienstaufgaben mindestens die Hälfte der regelmäßigen Arbeitszeit oder die Hälfte des durchschnittlichen Umfangs der Dienstaufgaben des entsprechenden vollbeschäftigten Personals beträgt.
Nicht nur vorübergehend ist eine Tätigkeit, die auf mehr als sechs Monate innerhalb eines Jahres angelegt ist.
Mitglieder sind auch Hochschullehrerinnen und Hochschullehrer, die nach gemeinsamer Berufung überwiegend an einer Forschungseinrichtung außerhalb der Hochschule tätig sind und Aufgaben in Lehre und Forschung an der Hochschule wahrnehmen.

(2) Die anderen an der Hochschule Tätigen sind Angehörige der Hochschule.
Professorinnen und Professoren werden nach Eintritt in den Ruhestand Angehörige der Hochschule, soweit sie an der Hochschule weiter forschen oder lehren.

(3) Die Präsidentin oder der Präsident kann auf Antrag des zuständigen Organs der Hochschule einer Honorarprofessorin oder einem Honorarprofessor den Status eines Mitglieds der Gruppe der Hochschullehrerinnen und Hochschullehrer verleihen, wenn sie oder er die Einstellungsvoraussetzungen nach § 41 erfüllt sowie Aufgaben der Hochschule in Forschung und Lehre selbstständig wahrnimmt.

#### § 61   
Allgemeine Grundsätze der Mitwirkung

(1) Die Mitwirkung an der Selbstverwaltung der Hochschule ist Recht und Pflicht aller Mitglieder.
Art und Umfang der Mitwirkung der einzelnen Mitgliedergruppen und innerhalb der Mitgliedergruppen bestimmen sich nach der Qualifikation, Funktion, Verantwortung und Betroffenheit der Mitglieder.
Für die Vertretung in den nach Mitgliedergruppen zusammengesetzten Gremien bilden die Hochschullehrerinnen und Hochschullehrer, die Akademischen Mitarbeiterinnen und Mitarbeiter, die Studierenden einschließlich der Promotionsstudierenden ohne Beschäftigungsverhältnis an der Hochschule und die sonstigen Mitarbeiterinnen und Mitarbeiter je eine Gruppe; alle Mitgliedergruppen müssen vertreten sein und wirken nach Maßgabe des Satzes 2 grundsätzlich stimmberechtigt an Entscheidungen mit.
Abweichend von Satz 3 ist das Fehlen studentischer Mitglieder in einem in der Grundordnung für den Fachbereich vorgesehenen Organ unerheblich, soweit sich Studierende bei den Wahlen zu diesem Organ auch in einem zweiten Wahldurchgang nicht zur Wahl gestellt haben.
In nach Mitgliedergruppen zusammengesetzten Entscheidungsgremien verfügen die Hochschullehrerinnen und Hochschullehrer bei der Entscheidung in Angelegenheiten, die die Lehre mit Ausnahme der Bewertung der Lehre betreffen, mindestens über die Hälfte der Stimmen, in Angelegenheiten, die die Forschung, künstlerische Entwicklungsvorhaben oder die Berufung von Juniorprofessorinnen und Juniorprofessoren unmittelbar betreffen, über die Mehrheit der Stimmen.
Die Studierenden verfügen in Angelegenheiten der Studienorganisation und Lehre über einen Stimmenanteil von mindestens 30 Prozent.
In Angelegenheiten, die die Entscheidung über Habilitationen, die Berufung von Professorinnen und Professoren oder die Bewährung von Juniorprofessorinnen und Juniorprofessoren als Hochschullehrerinnen und Hochschullehrer unmittelbar betreffen, verfügen Professorinnen und Professoren und Juniorprofessorinnen und Juniorprofessoren, welche sich nach § 46 Absatz 1 Satz 2 und Absatz 2 bewährt haben, über die Mehrheit der Stimmen.

(2) Die Mitglieder eines Gremiums werden, soweit sie dem Gremium nicht kraft Amtes angehören, nach Maßgabe der für das Gremium geltenden Satzung für eine bestimmte Amtszeit bestellt oder gewählt; sie sind an Weisungen nicht gebunden.
In allen Gremien sollen mindestens ein Drittel der stimmberechtigten Mitglieder Frauen sein.

(3) Die Hochschulmitglieder dürfen wegen ihrer Tätigkeit in der Selbstverwaltung nicht benachteiligt werden.

#### § 62   
Wahlen

(1) Die Vertreterinnen und Vertreter der Mitgliedergruppen in den Organen der Hochschule werden in freier, gleicher und geheimer Wahl von den jeweiligen Mitgliedergruppen und in der Regel nach den Grundsätzen der personalisierten Verhältniswahl gewählt.
Durch Bestimmung der Grundordnung kann von der Verhältniswahl insbesondere abgesehen werden, wenn wegen einer überschaubaren Zahl von Wahlberechtigten in einer Mitgliedergruppe die Mehrheitswahl angemessen ist.
Angehörige der Hochschule haben nur aktives Wahlrecht.

(2) Die Wahlordnung der Hochschule trifft unter Beachtung des Absatzes 1 Regelungen über die Ausübung des aktiven und passiven Wahlrechts, über Nachrückerinnen und Nachrücker, Stellvertreterinnen und Stellvertreter, Fristen sowie Grundsätze für die Durchführung von Wahlen an den Hochschulen, einschließlich der Wahlen in der Studierendenschaft.
Sie wird vom zuständigen Organ der Hochschule, für die Wahlen in der Studierendenschaft von ihrem obersten beschlussfassenden Organ, erlassen.

#### § 63   
Öffentlichkeit

(1) Die Gremien tagen hochschulöffentlich, soweit in diesem Gesetz nichts anderes bestimmt ist.
Sie können den Ausschluss der Hochschulöffentlichkeit zur Vermeidung von Störungen beschließen.
Die Mitglieder und Angehörigen der Hochschule sind regelmäßig über die Tätigkeit der Gremien zu unterrichten.

(2) Personal- und Prüfungsangelegenheiten werden in nicht öffentlicher Sitzung behandelt.
Entscheidungen über Personalangelegenheiten erfolgen in geheimer Abstimmung.
Die Teilnehmer nicht öffentlicher Gremiensitzungen sind zur Verschwiegenheit verpflichtet.

### Abschnitt 8   
Zentrale Hochschulorganisation

#### § 64   
Zentrale Hochschulorgane

(1) Zentrale Hochschulorgane sind die Präsidentin oder der Präsident und die in der Grundordnung bestimmten weiteren Organe.

(2) Die Grundordnung regelt die Organisationsstruktur der Hochschule und die Zuständigkeiten der Organe, soweit durch dieses Gesetz nichts anderes bestimmt ist, insbesondere

1.  Erlass und Änderung der Grundordnung,
    
2.  Erlass und Änderung sonstiger Satzungen der Hochschule, soweit nicht die Zuständigkeit der Fachbereiche begründet ist,
    
3.  Wahl und Abwahl der Präsidentin oder des Präsidenten,
    
4.  die Vertretung der Präsidentin oder des Präsidenten,
    
5.  die Aufsicht über die Präsidentin oder den Präsidenten, insbesondere in Bezug auf den Rechenschaftsbericht und die Entlastung der Präsidentin oder des Präsidenten sowie in Bezug auf den Entwurf des Haushaltsplanes,
    
6.  die Zuständigkeit zur Entscheidung in grundsätzlichen Fragen der Lehre, der Forschung, des Studiums und der Prüfungen sowie der Förderung des wissenschaftlichen und künstlerischen Nachwuchses,
    
7.  die Zuständigkeit zur Entscheidung über den Entwicklungsplan der Hochschule und über die Vorschläge der Fachbereiche für die Berufung von Hochschullehrerinnen und Hochschullehrern,
    
8.  die Zuständigkeit zur Stellungnahme zu den Satzungen der Fachbereiche.
    

(3) Das nach Absatz 1 durch die Grundordnung bestimmte weitere zentrale Hochschulorgan richtet eine Ethikkommission ein.
Die Ethikkommission befasst sich insbesondere mit Fragestellungen zum möglichen Einsatz von Forschungsergebnissen für nicht friedliche Zwecke sowie zu Forschungsvorhaben am Menschen sowie an Tieren und gibt dazu Empfehlungen ab.
In der Ethikkommission sind sowohl Mitglieder der Hochschule als auch externe sachverständige Personen vertreten.

#### § 65   
Präsidentin oder Präsident

(1) Die Präsidentin oder der Präsident leitet die Hochschule in eigener Zuständigkeit und Verantwortung und vertritt sie nach außen.
Sie oder er legt dem zuständigen aufsichtsführenden Organ der Hochschule jährlich sowie auf dessen begründetes Verlangen Rechenschaft über die Erfüllung ihrer oder seiner Aufgaben und ist in Bezug darauf diesem Organ zur umfassenden Information und Auskunft verpflichtet.
Die Präsidentin oder der Präsident ist für alle Aufgaben der Hochschule zuständig, soweit dieses Gesetz nichts anderes bestimmt.
Sie oder er ist insbesondere zuständig für

1.  die Vorbereitung von Konzepten für die Hochschulentwicklung, insbesondere des Struktur- und Entwicklungsplanes (§ 3 Absatz 2),
    
2.  die Einrichtung und Auflösung von Fachbereichen, Zentralen Einrichtungen und Betriebseinheiten sowie von Studiengängen nach Anhörung des zuständigen Organs der Hochschule,
    
3.  die Koordination der Tätigkeit der Fachbereiche und Zentralen Einrichtungen, insbesondere in Bezug auf Lehre und Forschung,
    
4.  die Evaluation der Forschung an den Fachbereichen und Zentralen Einrichtungen auf der Grundlage der Forschungsberichte,
    
5.  die Aufstellung und Bewirtschaftung des Haushalts sowie die befristete und leistungsbezogene Zuweisung von Mitteln und Stellen an die Fachbereiche und Zentralen Einrichtungen nach Maßgabe der Ergebnisse der Evaluation und
    
6.  die Wahrung der Ordnung und die Ausübung des Hausrechts.
    

Die Präsidentin oder der Präsident kann an den Sitzungen der Organe der Hochschule teilnehmen, hat Rede- und Antragsrecht, ist über ihre Beschlüsse unverzüglich zu unterrichten und hat sie zu beanstanden, wenn sie rechtswidrig sind.
Die Beanstandung hat aufschiebende Wirkung.
Das Nähere bestimmt die Grundordnung.

(2) Die Präsidentin oder der Präsident wird aufgrund des Wahlvorschlages einer Findungskommission vom zuständigen Organ der Hochschule auf Zeit gewählt und von dem für die Hochschulen zuständigen Mitglied der Landesregierung bestellt.
Die Findungskommission besteht aus fünf Mitgliedern, von denen drei vom Landeshochschulrat für die Dauer von drei Jahren sowie je eines von der für die Hochschulen zuständigen obersten Landesbehörde und dem zuständigen Organ der betroffenen Hochschule bestellt werden; den Vorsitz in der Findungskommission hat eines der vom Landeshochschulrat bestellten Mitglieder.
Sie erstellt einen Wahlvorschlag, der der Zustimmung aller Mitglieder bedarf und bis zu drei Personen umfassen kann.
Die zentrale Gleichstellungsbeauftragte der jeweiligen Hochschule kann mit beratender Stimme am Auswahlverfahren zur Erstellung des Wahlvorschlages teilnehmen.
Das Nähere zum Wahlverfahren bestimmt die Grundordnung.

(3) Zur Präsidentin oder zum Präsidenten kann bestellt werden, wer aufgrund einer mehrjährigen verantwortlichen beruflichen Tätigkeit, insbesondere in Wissenschaft, Wirtschaft, Verwaltung oder Rechtspflege, an Kunsthochschulen insbesondere auch in Kunst und Kultur, erwarten lässt, dass sie oder er den Aufgaben des Amtes gewachsen ist; sie oder er soll über eine abgeschlossene Hochschulausbildung verfügen.
Die Präsidentin oder der Präsident nimmt ihr oder sein Amt hauptberuflich wahr.
Die Amtszeit beträgt sechs Jahre.
Die Wiederwahl ist zulässig.

(4) Die Präsidentin oder der Präsident kann vom zuständigen Organ der Hochschule mit einer Mehrheit von zwei Dritteln seiner Mitglieder abgewählt werden; die Abwahl ist erst nach Ablauf von sechs Monaten nach Amtsantritt zulässig.
Vor Einleitung eines Abwahlverfahrens hat das zuständige Organ der Hochschule dem Landeshochschulrat schriftlich die Gründe des Abwahlbegehrens mitzuteilen und der Präsidentin oder dem Präsidenten Gelegenheit zur Stellungnahme zu den Gründen des Abwahlbegehrens zu geben.
Die Abwahl kann nur dadurch erfolgen, dass das zuständige Organ der Hochschule auf Vorschlag eines oder mehrerer seiner Mitglieder eine Nachfolgerin oder einen Nachfolger wählt und das für die Hochschulen zuständige Mitglied der Landesregierung ersucht, die Präsidentin oder den Präsidenten abzuberufen.
Das für die Hochschulen zuständige Mitglied der Landesregierung muss dem Ersuchen bei ordnungsgemäßer Durchführung des Abwahlverfahrens entsprechen und nach Maßgabe des Absatzes 3 die Gewählte oder den Gewählten bestellen.
Die Versorgung der abgewählten Präsidentin oder des abgewählten Präsidenten im Beamtenverhältnis auf Zeit richtet sich nach § 27 Absatz 6 des Brandenburgischen Beamtenversorgungsgesetzes.
War die abgewählte Präsidentin oder der abgewählte Präsident vor Amtsantritt Professorin oder Professor an derselben Hochschule, ist sie oder er auf Antrag in ein Professorenamt an dieser Hochschule zu übernehmen.
War die abgewählte Präsidentin oder der abgewählte Präsident vor Amtsantritt nicht Professorin oder Professor an derselben Hochschule, kann sie oder er auf Antrag in eine vergleichbare Rechtsstellung in den Landesdienst übernommen werden, wie sie oder er sie zum Zeitpunkt der Bestellung innehatte.
Der Antrag ist innerhalb von drei Monaten nach Abwahl zu stellen.
Mit der Übernahme endet das Beamtenverhältnis auf Zeit durch Entlassung; dies gilt nicht, wenn die Präsidentin oder der Präsident wiedergewählt war und ohne Wiederwahl in den Ruhestand getreten wäre.

(5) Wird die Präsidentin oder der Präsident aus einem Angestelltenverhältnis bestellt, übt sie oder er das Amt im Angestelltenverhältnis aus.
Wird sie oder er aus einem Beamtenverhältnis bestellt, so wird sie oder er in das Beamtenverhältnis auf Zeit berufen; die allgemeinen beamtenrechtlichen Vorschriften über die Laufbahnen finden keine Anwendung.
Die Präsidentin oder der Präsident tritt mit Ablauf der Amtszeit nur dann in den Ruhestand, wenn sie oder er eine Dienstzeit von mindestens zehn Jahren in einem Beamtenverhältnis mit Dienstbezügen zurückgelegt hat oder aus einem Beamtenverhältnis auf Lebenszeit zur Beamtin oder zum Beamten auf Zeit ernannt worden war; dabei findet § 122 des Landesbeamtengesetzes mit der Maßgabe Anwendung, dass die Bereitschaft zur Wiederwahl von der Präsidentin oder dem Präsidenten schriftlich gegenüber dem für die Wahl der Präsidentin oder des Präsidenten zuständigen Organ zu erklären ist.
Sind diese Voraussetzungen nicht erfüllt und war sie oder er vorher im öffentlichen Dienst tätig, ist sie oder er auf Antrag mindestens mit einer vergleichbaren Rechtsstellung, wie sie zum Zeitpunkt der Bestellung zur Präsidentin oder zum Präsidenten bestand, in den Landesdienst zu übernehmen.
In den Fällen des Satzes 3 und für Personen, die vorher nicht im öffentlichen Dienst tätig waren, kann eine solche Übernahme in den Landesdienst vereinbart werden.
War die Präsidentin oder der Präsident vor Amtsantritt beamtete Professorin oder beamteter Professor an einer Hochschule des Landes Brandenburg und tritt sie oder er in den Ruhestand, so ist sie oder er auf Antrag mit einer vergleichbaren Rechtsstellung, wie sie oder er sie zum Zeitpunkt der Bestellung zur Präsidentin oder zum Präsidenten hatte, in den Dienst ihrer oder seiner früheren Hochschule zu übernehmen.
Die Anträge nach den Sätzen 4 und 6 sind innerhalb von drei Monaten nach Ablauf der Amtszeit zu stellen.
§ 44 Absatz 3 gilt für Präsidentinnen und Präsidenten entsprechend.

(6) Die Grundordnung kann bestimmen, dass die Präsidentin oder der Präsident sich Rektorin oder Rektor nennen darf, sofern sie oder er Professorin oder Professor an dieser Hochschule ist.
Soweit die Aufgaben des Präsidentenamtes nicht berührt werden, ist eine Tätigkeit in Lehre und Forschung zulässig.
Nach Ablauf der Amtszeit als Präsidentin oder Präsident wird eine Professorin oder ein Professor auf Antrag bis zur Dauer eines Jahres zugunsten der Forschungsaufgaben freigestellt.

(7) Ist mit Ablauf der Amtszeit der Präsidentin oder des Präsidenten keine Nachfolgerin oder kein Nachfolger bestellt, nimmt in der Regel die bisherige Präsidentin oder der bisherige Präsident die Aufgaben bis zur Bestellung einer Nachfolgerin oder eines Nachfolgers geschäftsführend wahr.
Hat die bisherige Präsidentin oder der bisherige Präsident bei einer erneuten Kandidatur nicht die für eine Wiederwahl erforderliche Mehrheit erreicht oder ist aus anderen Gründen gehindert, die Aufgaben der Präsidentin oder des Präsidenten geschäftsführend wahrzunehmen, kann das für die Hochschulen zuständige Mitglied der Landesregierung im Benehmen mit dem Landeshochschulrat und dem zuständigen Organ der Hochschule eine bisherige Vertreterin oder einen bisherigen Vertreter der Präsidentin oder des Präsidenten beauftragen, die Geschäfte der Präsidentin oder des Präsidenten bis zur Bestellung einer Nachfolgerin oder eines Nachfolgers wahrzunehmen.

#### § 66   
Hauptberufliche Vizepräsidentin oder hauptberuflicher Vizepräsident

(1) Sieht die Grundordnung die Vertretung der Präsidentin oder des Präsidenten durch eine hauptberuflich tätige Vizepräsidentin oder einen hauptberuflich tätigen Vizepräsidenten vor, wird das Amt im Angestelltenverhältnis ausgeübt, wenn sie oder er aus einem Angestelltenverhältnis bestellt wird.
Wird sie oder er aus einem Beamtenverhältnis bestellt, so erfolgt die Berufung in ein Beamtenverhältnis auf Zeit.
Die Amtszeit beträgt sechs Jahre.
Für eine im Angestelltenverhältnis wahrgenommene Vizepräsidentschaft kann die Grundordnung eine abweichende Amtszeit bestimmen.
Die Wiederwahl ist zulässig.
Im Übrigen gilt § 65 Absatz 5 und 6 entsprechend.

(2) Absatz 1 gilt für zum Zeitpunkt des Inkrafttretens dieses Gesetzes hauptberuflich tätige Vizepräsidentinnen und Vizepräsidenten im Angestelltenverhältnis, die aus einem Beamtenverhältnis beurlaubt sind, mit der Maßgabe, dass die zurückgelegte Dienstzeit auf die Dauer des Beamtenverhältnisses auf Zeit angerechnet wird.

#### § 67   
Kanzlerin oder Kanzler

(1) Die Kanzlerin oder der Kanzler leitet die Verwaltung der Hochschule unter der Verantwortung der Präsidentin oder des Präsidenten.
Sie oder er ist Beauftragte oder Beauftragter für den Haushalt.

(2) Die Kanzlerin oder der Kanzler wird von der Präsidentin oder dem Präsidenten bestellt.
Wird die Kanzlerin oder der Kanzler aus einem Angestelltenverhältnis bestellt, übt sie oder er das Amt im Angestelltenverhältnis aus.
Wird sie oder er aus einem Beamtenverhältnis auf Lebenszeit bestellt, so erfolgt die Berufung in ein Beamtenverhältnis auf Zeit; die allgemeinen beamtenrechtlichen Vorschriften über die Laufbahnen finden keine Anwendung.
Die Amtszeit beträgt sechs Jahre, erneute Bestellungen sind möglich.

(3) Die Kanzlerin oder der Kanzler muss einen wissenschaftlichen Hochschulabschluss, einen gleichwertigen Abschluss oder die Befähigung für eine Laufbahn des höheren Dienstes besitzen und eine mehrjährige verantwortliche Tätigkeit in der Verwaltung, der Rechtspflege oder der Wirtschaft ausgeübt haben.

(4) Nach Ablauf der Amtszeit ist die Kanzlerin oder der Kanzler aus einem Beamtenverhältnis auf Zeit entlassen.
Die Übernahme in den Landesdienst kann vereinbart werden; dies gilt auch für Kanzlerinnen und Kanzler im Angestelltenverhältnis.

(5) Die Grundordnung kann bestimmen, dass an die Stelle der Kanzlerin oder des Kanzlers eine hauptberufliche Vizepräsidentin oder ein hauptberuflicher Vizepräsident tritt.

#### § 68   
Zentrale und dezentrale Gleichstellungsbeauftragte

(1) An jeder Hochschule werden im Aufgabenbereich nach § 7 Absatz 1 eine Gleichstellungsbeauftragte (zentrale Gleichstellungsbeauftragte) und bis zu zwei Stellvertreterinnen von den Mitgliedern und Angehörigen der Hochschule für die Dauer von vier Jahren gewählt und von der Präsidentin oder dem Präsidenten bestellt.
In Hochschulen mit mehr als 2 000 Mitgliedern kann die Aufgabe der zentralen Gleichstellungsbeauftragten hauptberuflich wahrgenommen werden.
In diesem Fall ist die Stelle auszuschreiben.
Näheres zur Wahl wird in der Grundordnung bestimmt.

(2) Die Gleichstellungsbeauftragten beraten und unterstützen die Präsidentin oder den Präsidenten und die übrigen Organe und Einrichtungen der Hochschule in allen die Gleichstellung von Frauen und Männern betreffenden Angelegenheiten und wirken insbesondere bei Zielvereinbarungen, Struktur- und Personalentscheidungen sowie bei der Erstellung und Kontrolle von Frauenförderrichtlinien und Frauenförderplänen sowie von Gleichstellungskonzepten und Gleichstellungsplänen mit.
Sie informieren die Mitglieder und Angehörigen der Hochschule und nehmen Anregungen und Beschwerden entgegen.

(3) Für die Wahrnehmung im Aufgabenbereich nach § 7 Absatz 1 kann in jeder organisatorischen Grundeinheit für Lehre und Forschung und in der Verwaltung sowie in den zentralen Einrichtungen eine Gleichstellungsbeauftragte (dezentrale Gleichstellungsbeauftragte), die die zentrale Gleichstellungsbeauftragte insbesondere bei ihren Aufgaben gemäß Absatz 4 Satz 3 berät und unterstützt, und jeweils bis zu zwei Stellvertreterinnen von den Mitgliedern und Angehörigen der jeweiligen Einrichtungen für die Dauer von zwei Jahren gewählt werden.
Auch Studentinnen sind wählbar.
Die zentrale Gleichstellungsbeauftragte kann die Wahrnehmung einzelner Aufgaben auf die dezentralen Gleichstellungsbeauftragten unwiderruflich für die Dauer der Amtszeit übertragen, es sei denn, sie ist hauptberuflich tätig.
In kleinen organisatorischen Grundeinheiten für Lehre und Forschung und auch in der Verwaltung, wenn auf die Wahl einer dezentralen Gleichstellungsbeauftragten nach Satz 1 verzichtet wird, sind die Aufgaben nach § 7 Absatz 1 von der zentralen Gleichstellungsbeauftragten selbst wahrzunehmen.
Näheres zur Wahl nach Satz 1 wird in der Grundordnung bestimmt.

(4) Die zentrale Gleichstellungsbeauftragte ist über alle Angelegenheiten, die die Gleichstellung an der Hochschule betreffen, rechtzeitig zu informieren.
In diesen Angelegenheiten macht sie Vorschläge und nimmt Stellung gegenüber den zuständigen Stellen der Hochschule.
Sie hat Informations-, Rede- und Antragsrecht in allen Gremien und das Teilnahmerecht bei Bewerbungsverfahren; in Verfahren, in denen sich Frauen und Männer beworben haben, insbesondere in Bereichen, in denen eine Unterrepräsentanz von Frauen besteht, ist sie zur Teilnahme verpflichtet.
Sie erhält Einsicht in alle Akten, die Maßnahmen betreffen, an denen sie zu beteiligen ist.
Das gilt auch für Personalakten.
Soweit dies zur Erfüllung der Aufgaben nach § 7 Absatz 1 und im Rahmen des Teilnahmerechts bei Bewerbungsverfahren erforderlich ist, sind die zuständigen Stellen verpflichtet und berechtigt, der Gleichstellungsbeauftragten dabei auch personenbezogene Daten zu übermitteln.
Die Gleichstellungsbeauftragte ist berechtigt, als Verantwortliche personenbezogene Daten in diesem Zusammenhang zu verarbeiten, soweit und solange dies zur Erfüllung dieser Aufgaben erforderlich ist.

(5) Wird die zentrale Gleichstellungsbeauftragte nicht gemäß Absatz 4 beteiligt, so ist die Entscheidung über eine Maßnahme für zwei Wochen auszusetzen und die Beteiligung nachzuholen.
In dringenden Fällen ist die Frist auf eine Woche, bei außerordentlichen Kündigungen auf drei Tage, zu verkürzen.

(6) Ist die Entscheidung eines Organs oder eines Gremiums der Hochschule im Aufgabenbereich der zuständigen Gleichstellungsbeauftragten gegen deren Stellungnahme getroffen worden, so kann sie innerhalb einer Woche nach Kenntnis widersprechen.
Widerspricht die Gleichstellungsbeauftragte, so ist in einem durch Satzung näher zu regelnden Verfahren ein Einigungsversuch zu unternehmen.
Die erneute Entscheidung darf frühestens eine Woche nach dem Einigungsversuch erfolgen.
In derselben Angelegenheit ist der Widerspruch nur einmal zulässig.
Eine Entscheidung gemäß Satz 1 darf erst nach Ablauf der Widerspruchsfrist oder der Bestätigung der Entscheidung ausgeführt werden.

(7) Bleibt der Widerspruch erfolglos, kann die Gleichstellungsbeauftragte sich unmittelbar an die für die Hochschulen zuständige oberste Landesbehörde wenden, um geltend zu machen, dass die Hochschule ihre Rechte aus diesem Gesetz verletzt hat oder kein oder ein nicht den Vorschriften dieses Gesetzes entsprechendes Gleichstellungskonzept oder einen nicht den Vorschriften dieses Gesetzes entsprechenden Gleichstellungsplan aufgestellt hat.

(8) Die zentrale Gleichstellungsbeauftragte berichtet der Präsidentin oder dem Präsidenten und anderen von der Grundordnung bestimmten Organen der Hochschule regelmäßig über ihre Tätigkeit.

(9) Die Gleichstellungsbeauftragten und ihre Stellvertreterinnen nehmen ihre Aufgaben als dienstliche Tätigkeit wahr.
Im Rahmen ihrer rechtmäßigen Aufgabenerfüllung sind sie von Weisungen frei.
Die Regelungen zur Verschwiegenheitspflicht gemäß § 37 des Beamtenstatusgesetzes und den tarifrechtlichen Bestimmungen gelten auch für die Tätigkeit als Gleichstellungsbeauftragte.
Die zentrale Gleichstellungsbeauftragte ist mindestens mit einem halben Vollzeitäquivalent von ihren Dienstaufgaben freizustellen.
Nimmt sie die Aufgabe hauptberuflich wahr und hat sie ein Beschäftigungsverhältnis mit der Hochschule, so wird sie von den Aufgaben dieses Beschäftigungsverhältnisses freigestellt.
Die dezentrale Gleichstellungsbeauftragte soll in angemessenem Umfang von ihren Dienstaufgaben freigestellt werden.
Die Hochschule stellt der zentralen Gleichstellungsbeauftragten nach Maßgabe des Haushalts der Hochschule im angemessenen Umfang Personal- und Sachmittel zur Erfüllung ihrer Aufgaben zur Verfügung.
Die Gleichstellungsbeauftragte und ihre Stellvertreterinnen dürfen wegen ihrer Tätigkeit nicht benachteiligt werden.
Das gilt auch für die berufliche Entwicklung.
Durch die Tätigkeit als Gleichstellungsbeauftragte erworbene besondere Kenntnisse und Fähigkeiten sind bei der beruflichen Entwicklung zu berücksichtigen.
Die Gleichstellungsbeauftragten und ihre Stellvertreterinnen sind vor Versetzung und Abordnung gegen ihren Willen sowie gegen Kündigung in gleicher Weise geschützt wie Mitglieder des Personalrats.

#### § 69   
Beauftragte oder Beauftragter für die Belange von Hochschulmitgliedern mit Behinderungen

Die oder der Beauftragte für die Belange von Hochschulmitgliedern mit Behinderungen wirkt bei der Organisation der Studienbedingungen nach den Bedürfnissen behinderter Mitglieder mit.
Sie oder er hat das Recht auf notwendige und sachdienliche Information sowie Teilnahme-, Antrags- und Rederecht in allen Gremien der Hochschule in Angelegenheiten, welche die Belange der Behinderten berühren.
Sie oder er berichtet der Präsidentin oder dem Präsidenten regelmäßig über die Tätigkeit.

#### § 70   
Hochschulbibliothek

(1) Die Hochschulbibliothek ist eine zentrale Betriebseinheit der Hochschule.
Sie versorgt Lehre, Forschung und Studium mit Literatur und anderen Informationsträgern.
Sie berät und unterstützt die Fachbereiche bei der Versorgung mit Informationsträgern und berücksichtigt deren Vorschläge bei der Beschaffung.
Sie fördert durch Schulungs- und Lehrangebote die Informations- und Medienkompetenz an der Hochschule.
Sie fördert den freien Zugang zu wissenschaftlichen Informationen.

(2) Die Hochschulbibliothek arbeitet mit anderen Bibliotheken und Einrichtungen der Information, Kommunikation und Dokumentation außerhalb des Hochschulwesens zusammen.
Sie nimmt gegebenenfalls regionale oder zentrale Aufgaben wahr, soweit die Erfüllung ihrer Aufgabe nach Absatz 1 Satz 2 nicht beeinträchtigt wird.

(3) Die Hochschule sorgt unter Beteiligung der Hochschulbibliothek dafür, dass die Informations- und Kommunikationsversorgung der Hochschule in eine einheitliche technische Struktur integriert wird.

### Abschnitt 9   
Dezentrale Hochschulorganisation

#### § 71   
Organisatorische Grundeinheiten; Verordnungsermächtigung

(1) Organisatorische Grundeinheiten der Hochschulen für Lehre und Forschung sind Fachbereiche, Fakultäten oder andere geeignete Strukturen.
Die Vorschriften über Fachbereiche finden auf Fakultäten oder andere organisatorische Grundeinheiten entsprechende Anwendung.
Das für die Hochschulen zuständige Mitglied der Landesregierung kann, nach Anhörung des zuständigen Ausschusses des Landtages, zur Erprobung aufgabenorientierter Strukturen an den Hochschulen auf Antrag der Hochschule durch Rechtsverordnung bestimmen, dass an die Stelle der Fachbereiche oder ihrer Organe andere zuständige Stellen treten und von den Bestimmungen des Abschnitts 9 dieses Gesetzes weitere abweichende organisatorische Regelungen zulassen.
Vor Antrag der Hochschule ist dazu ein entsprechender Beschluss des zuständigen, nach Mitgliedergruppen zusammengesetzten Organs erforderlich.

(2) Der Fachbereich umfasst verwandte oder benachbarte Fachgebiete.
Größe und Abgrenzung der Fachbereiche müssen gewährleisten, dass die dem Fachbereich obliegenden Aufgaben angemessen erfüllt werden können.

(3) Die Gründung und Auflösung von Fachbereichen ist der für die Hochschulen zuständigen obersten Landesbehörde anzuzeigen.

(4) Die Hochschulen können zur gemeinsamen Aufgabenwahrnehmung mit Zustimmung der in der Grundordnung bestimmten Organe aufgrund einer Vereinbarung gemeinsame Organisationseinheiten, insbesondere Fachbereiche bilden.
Dies ist auch mit Forschungseinrichtungen außerhalb der Hochschule oder mit Hochschulen anderer Länder der Bundesrepublik Deutschland möglich, soweit die Hochschulgesetze dieser Länder dies zulassen.
In der Vereinbarung sind Struktur, Organisation, Leitung und Selbstverwaltung der gemeinsamen Organisationseinheit festzulegen, insbesondere

1.  das Zusammenwirken der beteiligten Hochschulen sowie deren Zuständigkeiten in Bezug auf die gemeinsame Organisationseinheit einschließlich der Ausübung der Aufsicht,
    
2.  die Organisation der gemeinsamen Organisationseinheit, insbesondere ihrer Organe und Zuständigkeiten,
    
3.  die körperschafts- und dienstrechtliche Zuordnung des im Bereich der gemeinsamen Organisationseinheit tätigen Personals sowie
    
4.  die mitgliedschaftsrechtliche Zuordnung der Studierenden.
    

Die Vereinbarung bedarf der Genehmigung der für die Hochschulen zuständigen obersten Landesbehörde.
§ 75 bleibt unberührt.

#### § 72   
Organe des Fachbereichs

(1) Die Dekanin oder der Dekan leitet den Fachbereich.
Die Amtszeit beträgt nach Maßgabe der Grundordnung mindestens vier und höchstens sechs Jahre.
Die Dekanin oder der Dekan ist in Bezug auf die Erfüllung der Aufgaben dem aufsichtsführenden Organ zur umfassenden Information und Auskunft verpflichtet.

(2) Die Grundordnung sieht mindestens ein weiteres Organ des Fachbereichs mit folgenden Aufgaben vor:

1.  Erlass von Satzungen des Fachbereichs,
    
2.  Entscheidungen über die Struktur- und Entwicklungsplanung des Fachbereichs,
    
3.  Vorschläge für die Leitung wissenschaftlicher Einrichtungen und Betriebseinheiten von Fachbereichseinrichtungen,
    
4.  Entscheidung über Berufungsvorschläge,
    
5.  Entscheidung über Habilitationen,
    
6.  Mitwirkung an der Evaluation und Koordination von Lehre und Forschung im Fachbereich,
    
7.  Aufsicht über die Dekanin oder den Dekan und
    
8.  Wahl und Abwahl der Dekanin oder des Dekans und ihrer oder seiner Vertretung.
    

Die Aufgaben der Präsidentin oder des Präsidenten und der Dekanin oder des Dekans nach diesem Gesetz bleiben hiervon unberührt.

(3) Die Grundordnung sieht eine Vertretung der Dekanin oder des Dekans vor.
Sie kann bestimmen, dass zur Unterstützung der Dekanin oder des Dekans ein Dekanat gebildet wird.

#### § 73   
Wahl und Aufgaben der Dekanin oder des Dekans

(1) Die Dekanin oder der Dekan wird auf Vorschlag der Präsidentin oder des Präsidenten vom Fachbereich oder dem in der Grundordnung bestimmten Organ des Fachbereichs aus dem Kreis der dem Fachbereich angehörenden Hochschullehrerinnen und Hochschullehrer gewählt.
Die Wahl der Dekanin oder des Dekans bedarf außer der Mehrheit der Mitglieder des Fachbereichs oder zuständigen Organs auch der Mehrheit der abgegebenen Stimmen der diesem angehörenden Hochschullehrerinnen und Hochschullehrer.
Kommt hiernach eine Wahl auch im zweiten Wahlgang nicht zustande, so genügt für die Entscheidung in einem dritten Wahlgang die Mehrheit der abgegebenen Stimmen der dem Fachbereich oder zuständigen Organ angehörenden Hochschullehrerinnen und Hochschullehrer.
Für die Abwahl gilt Satz 2 entsprechend mit der Maßgabe, dass die Mehrheit der Mitglieder zwei Drittel betragen muss.
Die Wiederwahl ist zulässig.

(2) Die Hochschule kann in ihrer Grundordnung die Möglichkeit vorsehen, dass das Amt der Dekanin oder des Dekans auch hauptberuflich durch hochschulexterne Personen wahrgenommen werden kann.
Bestellt werden kann, wer eine abgeschlossene Hochschulausbildung besitzt und aufgrund einer mehrjährigen, verantwortlichen beruflichen Tätigkeit, insbesondere in Wissenschaft, Wirtschaft, Verwaltung oder Rechtspflege, erwarten lässt, dass sie oder er den Aufgaben des Amtes gewachsen ist.
Die Amtszeit einer hauptberuflichen Dekanin oder eines hauptberuflichen Dekans beträgt sechs Jahre.

(3) Die Dekanin oder der Dekan leitet den Fachbereich und vertritt ihn innerhalb der Hochschule.
Sie oder er ist für alle Aufgaben des Fachbereichs zuständig, soweit dieses Gesetz nichts anderes bestimmt.
Dies umfasst insbesondere die Verantwortung für die Studien- und Prüfungsorganisation und für die Koordinierung von Forschung und Lehre.
Die Dekanin oder der Dekan stellt das Lehrangebot sicher, das zur Einhaltung der Studienordnungen erforderlich ist.
Sie oder er wirkt darauf hin, dass die Mitglieder und Angehörigen des Fachbereichs ihre Aufgaben wahrnehmen und ist gegenüber den Hochschullehrerinnen und Hochschullehrern in Angelegenheiten der Lehr- und Prüfungsorganisation weisungsbefugt.
Sie oder er entscheidet über den Einsatz der Mitarbeiterinnen und Mitarbeiter des Fachbereichs.
Sie oder er stellt Konzepte für die Entwicklung des Fachbereichs auf und schlägt dem zuständigen Organ die Bildung von Fachbereichseinrichtungen vor.

(4) Die Dekanin oder der Dekan verteilt Mittel und Stellen unter Berücksichtigung des Ergebnisses der Evaluation von Lehre und Forschung aus den dem Fachbereich zur Verfügung stehenden Mitteln an die Einrichtungen.
Sie oder er erstattet regelmäßig einen Bericht des Fachbereichs über die Erfüllung der Aufgaben in Forschung und Lehre an die Präsidentin oder den Präsidenten.
Der Bericht dient auch der in § 27 Absatz 1 und 3 genannten Aufgabenerfüllung.

### Abschnitt 10   
Wissenschaftliche Einrichtungen

#### § 74   
Aufgaben; Einrichtung; Organisation wissenschaftlicher Einrichtungen

(1) Wissenschaftliche Einrichtungen und Betriebseinheiten dienen der Wahrnehmung der Aufgaben der Hochschule im Bereich von Lehre, Forschung, Studium und Weiterbildung.
Ihre Errichtung und Gestaltung ist der für die Hochschulen zuständigen obersten Landesbehörde anzuzeigen.

(2) Wissenschaftliche Einrichtungen und Betriebseinheiten können unter der Verantwortung eines Fachbereichs oder mehrerer Fachbereiche gebildet werden, soweit und solange für die Durchführung einer Aufgabe in größerem Umfang Stellen und Mittel des Fachbereichs ständig bereitgestellt werden müssen (Fachbereichseinrichtungen).
Soweit dies zweckmäßig ist, können wissenschaftliche Einrichtungen und Betriebseinheiten auch außerhalb eines Fachbereichs unter der Verantwortung der Präsidentin oder des Präsidenten gebildet werden (Zentrale Einrichtungen).

(3) Wissenschaftliche Einrichtungen und Betriebseinheiten entscheiden über die Verwendung der Mitarbeiterinnen und Mitarbeiter und Mittel, die ihnen zugewiesen sind.

(4) Die Leitung wissenschaftlicher Einrichtungen und Betriebseinheiten wird bei Fachbereichseinrichtungen von der Dekanin oder vom Dekan auf Vorschlag des zuständigen Organs des Fachbereichs, bei Zentralen Einrichtungen von der Präsidentin oder vom Präsidenten auf Vorschlag des zuständigen Organs der Hochschule bestellt.

(5) Wissenschaftliche Einrichtungen sollen befristet von einem oder mehreren Hochschullehrerinnen und Hochschullehrern geleitet werden.

#### § 75   
Wissenschaftliche Einrichtungen und Betriebseinheiten für mehrere Hochschulen

(1) Wissenschaftliche Einrichtungen und Betriebseinheiten für mehrere Hochschulen werden durch die Präsidentinnen und Präsidenten der beteiligten Hochschulen nach Stellungnahme der zuständigen Organe der Hochschule errichtet und gestaltet.
Die Leitung wird auf Vorschlag der zuständigen Organe der Hochschule von den Präsidentinnen und Präsidenten bestimmt.

(2) Die Hochschulen können gemeinsame wissenschaftliche Einrichtungen und Betriebseinheiten mit Hochschulen außerhalb des Landes Brandenburg errichten.
Die Regelungen über den Abschluss länderübergreifender oder internationaler Vereinbarungen und Abkommen bleiben unberührt.

#### § 76   
Wissenschaftliche Einrichtungen an der Hochschule

(1) Die Hochschulen können eine wissenschaftliche Einrichtung außerhalb der Hochschule, die insbesondere in den Bereichen Forschung, Lehre, Studium, Wissens- und Technologietransfer oder Weiterbildung tätig ist, als wissenschaftliche Einrichtung an der Hochschule (An-Institut) anerkennen, wenn

1.  die wissenschaftliche Einrichtung den auf den Gebieten der Forschung, der Lehre, des Studiums oder der wissenschaftlichen Weiterbildung zu stellenden Anforderungen genügt, insbesondere die Grundsätze der Wissenschaftsfreiheit beachtet werden,
    
2.  die Aufgaben von der Hochschule nicht angemessen wahrgenommen werden können und
    
3.  die Finanzierung der wissenschaftlichen Einrichtung nicht mit Haushaltsmitteln der Hochschule erfolgt.
    

An-Institut und Hochschule wirken nach Maßgabe einer Kooperationsvereinbarung zusammen, die der für die Hochschulen zuständigen obersten Landesbehörde rechtzeitig vor Wirksamwerden anzuzeigen ist.
Die Hochschule kann die Anerkennung widerrufen.

(2) Durch die Anerkennung wird die rechtliche Selbstständigkeit der wissenschaftlichen Einrichtung und die Rechtsstellung ihrer Bediensteten nicht berührt.

(3) Die an der Hochschule hauptberuflich Tätigen können vorübergehend Tätigkeiten in An-Instituten im Rahmen ihrer dienstlichen Aufgaben wahrnehmen.

### Abschnitt 11   
Landeshochschulrat

#### § 77   
Organisation und Aufgaben

(1) Der Landeshochschulrat unterstützt die staatlichen Hochschulen bei der Wahrnehmung ihrer Aufgaben und bei der Zusammenarbeit mit der Landesregierung und stellt seine Beratung auch den staatlich anerkannten Hochschulen sowie den Hochschulen des Landes im Sinne von § 1 Absatz 2 zur Verfügung.

(2) Der Landeshochschulrat

1.  berät die Präsidentinnen und Präsidenten und die in den Grundordnungen bestimmten Organe der Hochschulen in grundsätzlichen Angelegenheiten,
    
2.  wirkt bei der Entscheidung über die Entwicklungspläne der Hochschulen zur Gewährleistung einer ausgewogenen Strukturentwicklung der Hochschulen mit,
    
3.  berät die Landesregierung in strategischen Fragen der Landeshochschulplanung,
    
4.  bestellt drei Mitglieder der Findungskommission gemäß § 65 Absatz 2, darunter mindestens eine Frau, davon ein Mitglied aus dem nicht-wissenschaftlichen Bereich,
    
5.  unterstützt die Ethikkommissionen der Hochschulen und
    
6.  kann die Hochschulen bei der Planung, Einrichtung und Durchführung von dualen Studiengängen beraten.
    

(3) Zur Wahrnehmung seiner Aufgaben hat der Landeshochschulrat ein umfassendes Informationsrecht gegenüber den Präsidentinnen und Präsidenten und den weiteren Organen der Hochschulen.
Er hat keinen Anspruch auf Einsichtnahme in Personalakten.

(4) Der Landeshochschulrat setzt die Schwerpunkte seiner Befassung mit Struktur und Entwicklung des brandenburgischen Hochschulsystems selbst.
Das für die Hochschulen zuständige Mitglied der Landesregierung kann dem Landeshochschulrat vorschlagen, seine Tätigkeit auf bestimmte strategische Planungen und Fragen oder abgegrenzte Einzelthemen auszurichten.

(5) Der Landeshochschulrat berichtet der Landesregierung regelmäßig über seine Tätigkeit.
Er erörtert mindestens einmal im Jahr mit dem für die Hochschulen zuständigen Mitglied der Landesregierung die Hochschulentwicklung und Zielsetzungen für die weitere Entwicklung.

(6) Die Ministerpräsidentin oder der Ministerpräsident bestimmt auf Vorschlag des für die Hochschulen zuständigen Mitglieds der Landesregierung nach Anhörung der Hochschulen und im Benehmen mit dem zuständigen Ausschuss des Landtages die Mitglieder des Landeshochschulrats.
Dem Landeshochschulrat sollen in der Regel zwölf, mindestens aber sechs Personen, angehören, von denen mindestens ein Drittel weiblich sein soll.
Ihre Amtszeit beträgt vier Jahre.
Die Wiederbestellung ist zulässig; des abermaligen Benehmens mit dem zuständigen Ausschuss des Landtages bedarf es nicht.

(7) Den Mitgliedern des Landeshochschulrats werden Reisekosten erstattet.
Das für die Hochschulen zuständige Mitglied der Landesregierung trifft im Einvernehmen mit dem für Finanzen zuständigen Mitglied der Landesregierung eine diesbezügliche Regelung.

### Abschnitt 12   
Studentenwerke

#### § 78   
Organisation; Rechtsstellung; Aufgaben; Verordnungsermächtigung

(1) Studentenwerke sind rechtsfähige Anstalten des öffentlichen Rechts mit dem Recht auf Selbstverwaltung.
Ihre Organe sind der Verwaltungsrat und die Geschäftsführerin oder der Geschäftsführer.
Jedes Studentenwerk gibt sich eine Satzung und eine Beitragsordnung.
Diese bedürfen der Genehmigung des für die Hochschulen zuständigen Mitglieds der Landesregierung.

(2) Studentenwerke haben die Aufgabe, für die Studierenden Dienstleistungen auf sozialem, wirtschaftlichem, gesundheitlichem und kulturellem Gebiet zu erbringen.
Sie erfüllen diese Aufgaben insbesondere durch

1.  die Errichtung und Bewirtschaftung von Verpflegungseinrichtungen und von Einrichtungen für das studentische Wohnen,
    
2.  Maßnahmen zur Gesundheitsvorsorge und zur Bereitstellung einer Freizeitunfallversicherung, soweit nicht andere Vorschriften bestehen, und
    
3.  die Durchführung des Bundesausbildungsförderungsgesetzes, soweit ihnen diese Aufgabe übertragen ist, die Gewährung von Beihilfen und Darlehen sowie weitere Maßnahmen der Studienförderung.
    

Studentenwerke können Einrichtungen der Kinderbetreuung unterhalten sowie Räume und Anlagen zur Förderung kultureller und sportlicher Interessen der Studierenden bereitstellen, soweit dies nicht den Grundsätzen der Wirtschaftlichkeit und Sparsamkeit widerspricht.
Zur Erfüllung der Aufgaben nach Satz 2 Nummer 1 können Verpflegungsdienstleistungen im Rahmen von Kooperationsvereinbarungen auch an Studierende von Hochschulen außerhalb des Geltungsbereichs dieses Gesetzes erbracht werden, wenn und solange dies zweckmäßig erscheint und wirtschaftliche Nachteile nicht zu erwarten sind.

(3) Das für die Hochschulen zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung

1.  im Benehmen mit dem zuständigen Ausschuss des Landtages Studentenwerke zu bilden, ihre Zuständigkeit festzulegen und aufzulösen,
    
2.  weitere Einzelheiten zur Organisation von Studentenwerken festzulegen,
    
3.  den Studentenwerken weitere Aufgaben zu übertragen und
    
4.  im Einvernehmen mit dem für Finanzen zuständigen Mitglied der Landesregierung Regelungen über die Grundsätze der Finanzierung und Wirtschaftsführung von Studentenwerken zu treffen.
    

#### § 79   
Verwaltungsrat

Der Verwaltungsrat berät und entscheidet in Angelegenheiten des Studentenwerkes von grundsätzlicher Bedeutung, soweit durch dieses Gesetz oder die Satzung nichts anderes bestimmt ist.
Ihm obliegen insbesondere

1.  die Aufstellung von Grundsätzen über die Tätigkeit des Studentenwerkes und die Entwicklung seiner Einrichtungen,
    
2.  der Erlass der Satzung und der Beitragsordnung sowie die Festsetzung der Beitragshöhe,
    
3.  der Erlass der Ordnungen über die Nutzung der vom Studentenwerk betriebenen Einrichtungen,
    
4.  die Wahl der Geschäftsführung sowie deren Bestellung und Abberufung nach Zustimmung der für die Hochschulen zuständigen obersten Landesbehörde,
    
5.  die Beschlussfassung über den Wirtschaftsplanentwurf sowie die Kontrolle der Einhaltung des Wirtschaftsplanes,
    
6.  Entgegennahme und Feststellung des Jahresabschlusses sowie die Entlastung der Geschäftsführung und
    
7.  Zustimmung zum Erwerb, zur Veräußerung, zur Belastung von Grundstücken und Grundstücksrechten, zur Aufnahme von Darlehen und zur Übernahme von Bürgschaften, soweit es sich nicht um laufende Geschäfte handelt; im Anwendungsbereich der §§ 64 und 65 der Landeshaushaltsordnung bedarf es insoweit auch der Zustimmung der für die Hochschulen zuständigen obersten Landesbehörde.
    

#### § 80   
Geschäftsführung

(1) Die Geschäftsführung leitet das Studentenwerk und führt dessen Geschäfte in eigener Zuständigkeit, soweit der Verwaltungsrat nicht zuständig ist.
Sie vertritt das Studentenwerk nach außen.

(2) Die Bestellung der Geschäftsführung kann auf Zeit erfolgen.
Die Amtszeit beträgt in diesem Fall sechs Jahre.
Eine erneute Bestellung ist zulässig.

(3) Die Geschäftsführung ist dem Verwaltungsrat verantwortlich.
Sie bereitet dessen Beschlüsse vor und sorgt für die Ausführung.
Sie hat dem Verwaltungsrat Auskünfte zu erteilen.

(4) Die Geschäftsführung hat Beschlüsse des Verwaltungsrats, die rechtswidrig sind oder die Grundsätze der Wirtschaftlichkeit und Sparsamkeit verletzen, zu beanstanden.
Die Beanstandung hat aufschiebende Wirkung.
Erfolgt keine Abhilfe, unterrichtet die Geschäftsführung die für die Hochschulen zuständige oberste Landesbehörde.

#### § 81   
Finanzierung, Wirtschaftsführung und Rechnungswesen

(1) Zur Erfüllung seiner Aufgaben stehen jedem Studentenwerk folgende Einnahmen zur Verfügung:

1.  Einnahmen aus Verpflegungsbetrieben, Wohnheimen und sonstigen Dienstleistungen,
    
2.  nach Maßgabe des Haushalts des Landes staatliche Zuweisungen und Darlehen,
    
3.  Beiträge der Studierenden und
    
4.  Zuwendungen Dritter.
    

(2) Den Studentenwerken werden die erforderlichen Kosten für die Durchführung des Bundesausbildungsförderungsgesetzes erstattet.

(3) Die Beiträge nach Absatz 1 Nummer 3 werden durch ein Studentenwerk aufgrund der Beitragsordnung von den Studierenden erhoben.
Die Beiträge sind vor der Immatrikulation oder der Rückmeldung der Studierenden fällig, werden von der Hochschule gebührenfrei eingezogen und an das zuständige Studentenwerk überwiesen.
Die Höhe der Beiträge richtet sich nach dem für die Wahrnehmung der Aufgaben eines Studentenwerkes erforderlichen Aufwand.

(4) Die §§ 1 bis 87 sowie 106 bis 110 der Landeshaushaltsordnung finden mit Ausnahmen der §§ 7, 18 Absatz 3, §§ 55, 64 und 65 der Landeshaushaltsordnung keine Anwendung.
Für die Aufnahme von Darlehen durch die Studentenwerke beim Land gelten die §§ 23 und 44 der Landeshaushaltsordnung.

#### § 82   
Aufsicht

Studentenwerke unterstehen der Rechtsaufsicht des für die Hochschulen zuständigen Mitglieds der Landesregierung.
Soweit sie Angelegenheiten nach § 78 Absatz 2 Satz 2 Nummer 3 und Absatz 3 Nummer 3 wahrnehmen, unterstehen sie auch seiner Fachaufsicht.

### Abschnitt 13   
Anerkennung von Hochschulen und Berufsakademien

#### § 83   
Anerkennung

(1) Einrichtungen des Bildungswesens, die nicht Hochschulen des Landes gemäß § 2 Absatz 1 sind, können eine staatliche Anerkennung als Hochschule erhalten.
Sie bedürfen der staatlichen Anerkennung, wenn sie die Bezeichnung Universität, Hochschule, Fachhochschule, Kunsthochschule oder eine entsprechende fremdsprachliche Bezeichnung im Namen führen oder in vergleichbarer Weise verwenden sollen.
Die staatliche Anerkennung begründet keinen Anspruch auf staatliche Zuschüsse.

(2) Voraussetzungen der Anerkennung sind, dass

1.  die Einrichtung Aufgaben nach § 3 wahrnimmt und in ihr die Freiheit der Kunst und Wissenschaft, Forschung und Lehre im Rahmen der Zweckbestimmung durch die Trägerschaft und deren wirtschaftlichen Interessen gewährleistet ist,
    
2.  das Studium an den in § 17 Absatz 1 genannten Zielen ausgerichtet ist,
    
3.  eine Mehrzahl von nebeneinander bestehenden oder aufeinander folgenden Studiengängen an der Einrichtung allein oder im Verbund mit anderen Einrichtungen des Bildungswesens vorhanden oder im Rahmen einer Ausbauplanung vorgesehen ist.
    Dies gilt nicht, wenn innerhalb einer Fachrichtung die Einrichtung einer Mehrzahl von Studiengängen durch die wissenschaftliche Entwicklung oder das entsprechende berufliche Tätigkeitsfeld nicht nahegelegt wird,
    
4.  das Studium und die Abschlüsse aufgrund der Studien- und Prüfungsordnungen und des tatsächlichen Lehrangebots dem Studium und den Abschlüssen an staatlichen Hochschulen gleichwertig sind,
    
5.  nur Studienbewerberinnen und Studienbewerber angenommen werden, die die Voraussetzungen für eine Aufnahme in eine entsprechende staatliche Hochschule erfüllen,
    
6.  die hauptberuflich Lehrenden die Voraussetzungen erfüllen, die für entsprechende Tätigkeiten an staatlichen Hochschulen gefordert werden, wobei der von hauptberuflich Lehrenden, die die Einstellungsvoraussetzungen nach § 41 erfüllen, erbrachte Anteil an der Lehre 50 Prozent nicht unterschreiten soll,
    
7.  die Mitglieder der Hochschule an der Gestaltung des Studiums in sinngemäßer Anwendung dieses Gesetzes mitwirken,
    
8.  eine ihren Bestand und die wirtschaftliche Stellung des akademischen Personals sichernde Finanzierung der Hochschule langfristig gewährleistet ist und die Studierenden in den Studienverträgen über die Risiken für ihren Studienabschluss bei einer vorzeitigen Einstellung des Lehrbetriebs vollständig unterrichtet werden und
    
9.  der Träger oder die den Träger maßgeblich prägenden natürlichen Personen die freiheitlich demokratische Grundordnung achten und die für den Betrieb einer Hochschule erforderliche Zuverlässigkeit aufweisen.
    

Von der Aufklärung über die Risiken für den Studienabschluss bei einer vorzeitigen Einstellung des Lehrbetriebs nach Satz 1 Nummer 8 kann abgesehen werden, wenn der Anerkennungsbescheid davon befreit, weil der Bestand der Hochschule als dauerhaft gesichert vermutet werden kann.

(3) Eine staatliche oder staatlich anerkannte Hochschule eines anderen Mitgliedstaates der Europäischen Union oder aus anderen Ländern der Bundesrepublik Deutschland darf nach dem Recht ihres Sitzlandes unter dem Namen der Hochschule Hochschulqualifikationen ihres Herkunftsstaates vermitteln und ihre im Herkunftsstaat anerkannten Grade verleihen, soweit diese durch die Anerkennung des Herkunftsstaates erfasst sind und die Qualitätskontrolle durch das Sitzland gewährleistet ist.
Hochschulen nach Satz 1 sind verpflichtet, im Geschäftsverkehr neben ihrem Namen und der Rechtsform auch stets ihr Sitzland zu nennen.
Werden Studiengänge von Hochschulen nach Satz 1 in Kooperation mit einer Einrichtung durchgeführt, die selbst nicht Hochschule ist (Franchising), ist die Einrichtung verpflichtet, im Geschäftsverkehr bei allen im Zusammenhang mit dem Studienangebot stehenden Handlungen und bei der Bewerbung des Studienangebots darauf hinzuweisen, dass ihre Einrichtung selbst nicht Hochschule ist und die Studiengänge nicht von ihr angeboten werden sowie über Namen, Rechtsform und Sitzland der kooperierenden Hochschule zu informieren.
Der Wegfall der staatlichen Anerkennung oder Änderungen im Umfang der staatlichen Anerkennung sind der für Hochschulen zuständigen obersten Landesbehörde unverzüglich anzuzeigen.
Ansprüche Studierender an Hochschulen nach Satz 1 oder Einrichtungen nach Satz 3 gegen das Land Brandenburg auf Beendigung ihres Studiums bestehen nicht.

(4) Die Aufnahme einer Tätigkeit nach Absatz 3 ist der für Hochschulen zuständigen obersten Landesbehörde mindestens drei Monate vor Aufnahme des Studienbetriebs anzuzeigen.
Mit der Anzeige ist die staatliche Anerkennung durch den Herkunftsstaat und deren Umfang nachzuweisen sowie die Qualitätskontrolle durch das Sitzland zu bestätigen.
Einrichtungen nach Absatz 3 Satz 3 müssen mit der Anzeige zusätzlich nachweisen, in welcher Form die Qualitätssicherung durch das Sitzland erfolgt.

(5) Die für die Hochschulen zuständige oberste Landesbehörde kann den Betrieb einer Einrichtung untersagen, soweit diese ohne die nach Absatz 1 Satz 2 oder Absatz 3 erforderliche staatliche Anerkennung oder unter Verstoß gegen die Hinweis- und Informationspflichten nach Absatz 3 Satz 3 oder ohne rechtzeitige oder vollständige Anzeige nach Absatz 4

1.  Hochschulstudiengänge durchführt,
    
2.  Hochschulprüfungen abnimmt oder
    
3.  akademische Grade verleiht.
    

Führt eine Einrichtung die Bezeichnung Universität, Hochschule, Fachhochschule, Kunsthochschule oder entsprechende fremdsprachliche Bezeichnungen im Namen, ohne Aufgaben nach Satz 1 wahrzunehmen oder ohne nach Absatz 1 Satz 2 oder Absatz 3 staatlich anerkannt zu sein, ist von der für die Hochschulen zuständigen obersten Landesbehörde die Führung der Bezeichnung zu untersagen.

(6) Verwaltungsverfahren gemäß dieser Vorschrift können über den Einheitlichen Ansprechpartner für das Land Brandenburg abgewickelt werden.
Das Gesetz über den Einheitlichen Ansprechpartner für das Land Brandenburg vom 7.
Juli 2009 (GVBl.
I S. 262) sowie § 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg in Verbindung mit den §§ 71a bis 71e des Verwaltungsverfahrensgesetzes finden Anwendung.

#### § 84   
Anerkennungsverfahren

(1) Die für die Hochschulen zuständige oberste Landesbehörde spricht auf Antrag die staatliche Anerkennung aus.

(2) Vor der Entscheidung über den Antrag soll eine gutachtliche Stellungnahme einer von der für die Hochschulen zuständigen obersten Landesbehörde bestimmten sachverständigen Stelle eingeholt werden, in der das dem Antrag zugrunde liegende Konzept qualitativ bewertet wird.
Die oberste Landesbehörde kann vor der Entscheidung über den Antrag auch eine am Maßstab des § 18 Absatz 6 Satz 1 und 3 orientierte Akkreditierung der Studiengänge verlangen, auf die sich die beantragte Anerkennung erstrecken soll.
Die Anerkennung kann mit Nebenbestimmungen, insbesondere befristet ausgesprochen und mit Auflagen versehen werden, die der Erfüllung der Voraussetzungen von § 83 Absatz 2 dienen.
Die Verlängerung einer befristet ausgesprochenen Anerkennung soll von der Akkreditierung der Hochschule durch eine von der für die Hochschulen zuständigen obersten Landesbehörde bestimmten Stelle abhängig gemacht werden.
Die Kosten für Akkreditierungen oder andere einzuholende gutachtliche Stellungnahmen sind vom Träger der Einrichtung zu tragen, die die Anerkennung als Hochschule oder die Verlängerung dieser Anerkennung beantragt.

(3) In dem Anerkennungsbescheid sind die Standorte der Hochschule, die Studiengänge und die Abschlussgrade, auf die sich die Anerkennung erstreckt, und die Bezeichnung der Hochschule unter Angabe ihres Typs festzulegen.
Auf Antrag kann die für die Hochschulen zuständige oberste Landesbehörde staatlich anerkannten Fachhochschulen die Führung der Bezeichnung „Hochschule“ gestatten.

#### § 85   
Folgen der Anerkennung

(1) Das an einer staatlich anerkannten Hochschule abgeschlossene Studium ist ein abgeschlossenes Studium im Sinne dieses Gesetzes.
Dies gilt auch in den Fällen des § 84 Absatz 2 Satz 3 und § 86 Absatz 2 Satz 1, soweit das Studium während der Dauer der staatlichen Anerkennung aufgenommen wurde.

(2) Die staatlich anerkannten Hochschulen haben nach Maßgabe der Anerkennung das Recht, Hochschulprüfungen abzunehmen, Hochschulgrade zu verleihen sowie Promotionen und Habilitationen durchzuführen.

(3) Die Studien-, Prüfungs-, Promotions- und Habilitationsordnungen bedürfen der Feststellung der Gleichwertigkeit mit den Ordnungen der staatlichen Hochschulen durch die für die Hochschulen zuständige oberste Landesbehörde.

(4) Auf Antrag ist eine staatlich anerkannte Hochschule in die Zentrale Vergabe von Studienplätzen einzubeziehen.

(5) Die staatlich anerkannten Hochschulen können mit Zustimmung der für die Hochschulen zuständigen obersten Landesbehörde den an ihnen hauptberuflich oder nebenberuflich Lehrenden, welche die Einstellungsvoraussetzungen nach § 41 und im Falle einer nebenberuflichen Professur auch des § 54 erfüllen, die akademische Bezeichnung „Professorin“ oder „Professor“ verleihen.
Die für die Hochschulen zuständige oberste Landesbehörde kann die Zustimmung auch allgemein erteilen.
§ 48 Absatz 2 gilt entsprechend.
Der Anteil der nebenberuflichen Professuren an der Gesamtzahl der Professuren darf an einer Kunsthochschule 20 Prozent, an einer anderen Hochschule 10 Prozent nicht übersteigen.

(6) Staatlich anerkannte Hochschulen können mit Hochschullehrerinnen und Hochschullehrern ein befristetes Angestelltenverhältnis begründen.
Die Befristungsregelungen von § 43 Absatz 1 und § 46 Absatz 1 gelten entsprechend.
Mit Gastprofessorinnen und Gastprofessoren und Gastdozentinnen und Gastdozenten kann ein befristetes Dienstverhältnis nach Maßgabe des § 52 begründet werden.

(7) Die unter den Voraussetzungen des § 55 Absatz 1 von staatlich anerkannten Hochschulen bestellten Honorarprofessorinnen und Honorarprofessoren sind für die Dauer der Tätigkeit an der Hochschule zur Führung der Bezeichnung „Honorarprofessorin“ oder „Honorarprofessor“ mit einem die Hochschule bezeichnenden Zusatz berechtigt.
§ 55 Absatz 3 Satz 2 Halbsatz 1 gilt entsprechend.

(8) Die für die Hochschulen zuständige oberste Landesbehörde kann staatlich anerkannten Hochschulen die Beschäftigung von Lehrenden untersagen, wenn gegen diese so schwerwiegende Gründe vorliegen, dass sie bei vertraglich beschäftigten Lehrenden an staatlichen Hochschulen die Entlassung rechtfertigen würden, zum Beispiel, wenn sie bei ihrer Lehrtätigkeit erheblich von den Erfordernissen des Fachs und den Studien- und Prüfungsordnungen abweichen.

(9) Die staatlich anerkannten Hochschulen unterstehen der Rechtsaufsicht der für die Hochschulen zuständigen obersten Landesbehörde.
§ 5 Absatz 5 gilt entsprechend.
Besichtigungen und Besuche der Lehrveranstaltungen und Hochschulprüfungen durch Beauftragte des für die Hochschulen zuständigen Mitglieds der Landesregierung erfolgen im Benehmen mit der Hochschule.

#### § 86   
Verlust der Anerkennung

(1) Die Anerkennung erlischt, wenn die Hochschule nicht innerhalb einer von der für die Hochschulen zuständigen obersten Landesbehörde zu bestimmenden Frist den Studienbetrieb aufnimmt, wenn der Studienbetrieb ein Jahr geruht hat oder mit Ablauf des Tages, an dem die Hochschule ihren Hauptsitz im Geltungsbereich dieses Gesetzes aufgibt.
Dies gilt für Studiengänge, auf die sich die staatliche Anerkennung erstreckt, entsprechend.

(2) Die Anerkennung ist durch das für die Hochschulen zuständige Mitglied der Landesregierung aufzuheben, wenn die Voraussetzungen des § 83 Absatz 2 nicht gegeben waren, später weggefallen sind oder Auflagen gemäß § 84 Absatz 2 Satz 3 nicht erfüllt wurden und diesem Mangel trotz Beanstandung innerhalb einer bestimmten Frist nicht abgeholfen wurde.
Den Studierenden ist die Beendigung des Studiums zu ermöglichen; sie haben keinen Anspruch gegen das Land Brandenburg auf die Beendigung ihres Studiums.

#### § 86a   
Staatliche Anerkennung als Hochschulklinik; Verordnungsermächtigung

(1) Krankenhäuser, die gemeinsam mit einer als Hochschule staatlich anerkannten Einrichtung des Bildungswesens Lehre und Forschung im Studium der Humanmedizin gemäß der Approbationsordnung für Ärzte vom 27.
Juni 2002 (BGBl.
I S.
2405), die zuletzt durch Artikel 5 des Gesetzes vom 17.
Juli 2017 (BGBl.
I S.
2581, 2612) geändert worden ist, in der jeweils geltenden Fassung sicherstellen, können auf Antrag des Krankenhauses und mit Zustimmung der Hochschule eine staatliche Anerkennung als Hochschulklinik erhalten.
Sie bedürfen der staatlichen Anerkennung, wenn sie die Bezeichnung Hochschulklinik, Universitätsklinik oder eine entsprechende fremdsprachliche Bezeichnung im Namen führen oder in vergleichbarer Weise verwenden.

(2) Die weiteren Voraussetzungen für die staatliche Anerkennung, das Nähere zum Anerkennungsverfahren sowie zum Verlust der Anerkennung regelt das für die Hochschulen zuständige Mitglied der Landesregierung im Einvernehmen mit dem für Gesundheit zuständigen Mitglied der Landesregierung durch Rechtsverordnung.

(3) Die staatliche Anerkennung als Hochschulklinik begründet keinen Anspruch auf staatliche Zuschüsse.
Dies gilt insbesondere für den Hochschulbau.
Die Investitionsfinanzierung nach dem Krankenhausfinanzierungsgesetz in der Fassung der Bekanntmachung vom 10.
April 1991 (BGBl. I S. 886) in der jeweils geltenden Fassung in Verbindung mit dem Brandenburgischen Krankenhausentwicklungsgesetz vom 8. Juli 2009 (GVBl.
I S.
310) in der jeweils geltenden Fassung bleibt unberührt.

#### § 87   
Berufsakademien

(1) Berufsakademien sind Einrichtungen nichtstaatlicher Träger, die einschließlich der Abschlussprüfung eine mindestens dreijährige wissenschaftsbezogene und zugleich praxisorientierte berufliche Bildung vermitteln.
Die Ausbildung besteht aus einer praktischen Ausbildung in Betrieben der Wirtschaft oder vergleichbaren Einrichtungen der Berufspraxis (Betriebe) und aus einer mit der praktischen Ausbildung abgestimmten Ausbildung an der Berufsakademie, mit der die Betriebe zusammenwirken (duale Ausbildung).
Berufsakademien sind besondere Einrichtungen des tertiären Bildungsbereichs neben den Hochschulen.

(2) Eine Berufsakademie bedarf der staatlichen Anerkennung, wenn sie die Bezeichnung „Berufsakademie“ in ihrem Namen führen oder sonst verwenden will.
In dem Anerkennungsbescheid sind die Standorte der Berufsakademie, die Ausbildungsgänge, auf die sich die Anerkennung erstreckt, und die Bezeichnung der Berufsakademie festzulegen.
Die staatliche Anerkennung kann befristet und mit Auflagen versehen werden.
Eine langfristige oder unbefristete Anerkennung ist von der positiven Evaluation der Einrichtung durch eine von der für die Berufsakademien zuständigen obersten Landesbehörde bestimmten Stelle abhängig.

(3) Die staatliche Anerkennung kann auf Antrag des Trägers der Berufsakademie von der für die Berufsakademien zuständigen obersten Landesbehörde erteilt werden, wenn folgende Voraussetzungen erfüllt sind:

1.  Ausbildungsinhalte und -pläne für die theoretischen und praktischen Ausbildungsabschnitte werden zwischen der Berufsakademie und den betrieblichen Ausbildungsstätten nach Absatz 1 Satz 2 zeitlich und inhaltlich abgestimmt.
    
2.  Die Berufsakademie umfasst mindestens zwei verschiedene Ausbildungsgänge mit jeweils mehreren fachlichen Schwerpunkten oder sieht solche in einer Ausbauplanung vor.
    Dies gilt nicht, wenn innerhalb eines Ausbildungsganges die Einrichtung von fachlichen Schwerpunkten durch das entsprechende berufliche Tätigkeitsfeld nicht nahegelegt wird.
    
3.  Die Berufsakademie sieht als Voraussetzung für die Ausbildung den Erwerb der Qualifikation für ein Studium an einer Hochschule und den Abschluss eines Ausbildungsvertrages mit einem geeigneten Ausbildungsbetrieb vor.
    
4.  Die Ausbildungsgänge sollen von einer anerkannten Akkreditierungseinrichtung akkreditiert sein.
    
5.  Die Berufsakademie verfügt über eine ausreichende Anzahl pädagogisch geeigneter Lehrkräfte, wobei die hauptberuflichen Lehrkräfte und diejenigen, die zur Vergabe von Leistungspunkten im Sinne von § 24 Absatz 3 führende Lehrveranstaltungen anbieten oder als Prüfer an der Ausgabe oder Bewertung der Bachelorarbeit mitwirken, die für Professorinnen und Professoren geltenden Einstellungsvoraussetzungen an Fachhochschulen gemäß § 41 erfüllen oder einen geeigneten Hochschulabschluss und eine in der Regel mindestens fünfjährige einschlägige Berufserfahrung nachweisen können.
    Soweit Lehrangebote überwiegend der Vermittlung praktischer Fertigkeiten und Kenntnisse dienen, für die nicht die Einstellungsvoraussetzungen für Professorinnen und Professoren an Fachhochschulen erforderlich sind, können diese Akademischen Mitarbeiterinnen oder Mitarbeitern entsprechend den Regelungen nach § 49 übertragen werden.
    Soweit es der Eigenart des Faches und den Anforderungen der Stelle entspricht, können in Ausnahmefällen solche Lehrveranstaltungen von nebenberuflichen Lehrkräften angeboten werden, die über einen fachlich einschlägigen Hochschulabschluss oder einen gleichwertigen Abschluss sowie über eine fachwissenschaftliche und didaktische Befähigung und über eine mehrjährige fachlich einschlägige Berufserfahrung verfügen.
    Der von hauptberuflichen Lehrkräften, die die Einstellungsvoraussetzungen für Professorinnen und Professoren an Fachhochschulen gemäß § 41 erfüllen, erbrachte Anteil an der Lehre soll 40 Prozent nicht unterschreiten.
    
6.  Für die Berufsakademie besteht ein Kuratorium, das an Entscheidungen über die Entwicklung der Berufsakademie und über alle sie betreffenden Fragen von grundsätzlicher Bedeutung mitwirkt und dem mindestens jeweils eine Vertreterin oder ein Vertreter der Industrie- und Handelskammer oder einer anderen berufsständischen Kammer, der Arbeitgeber- und Arbeitnehmerorganisationen, der an der Ausbildung beteiligten Betriebe, der an der Berufsakademie tätigen Lehrkräfte und der Auszubildenden angehören.
    
7.  Die an der Berufsakademie tätigen Lehrkräfte und die Auszubildenden müssen an der Gestaltung des Studienbetriebes in sinngemäßer Anwendung dieses Gesetzes angemessen mitwirken können.
    
8.  Der Bestand der Trägerin oder des Trägers der Berufsakademie sowie die wirtschaftliche und rechtliche Stellung des Personals können nach der vorzulegenden Finanzierungsplanung für die Dauer der Ausbildung der jeweils Auszubildenden als finanziell gesichert vermutet werden.
    
9.  Die Berufsakademie regelt die Ausbildung und die Prüfung für jeden Ausbildungsgang durch eine Ausbildungs- und Prüfungsordnung.
    Die Prüfungsordnungen sind von der für die Berufsakademien zuständigen obersten Landesbehörde zu genehmigen.
    
10.  Die Trägerin oder der Träger oder die die Trägerin oder den Träger maßgeblich prägenden natürlichen Personen müssen die freiheitlich demokratische Grundordnung achten und die für den Betrieb einer Berufsakademie erforderliche Zuverlässigkeit aufweisen.
    

(4) Für Niederlassungen staatlicher oder staatlich anerkannter Berufsakademien aus Mitgliedstaaten der Europäischen Union oder aus anderen Ländern der Bundesrepublik Deutschland gilt § 83 Absatz 3 entsprechend.
Für Einrichtungen, die in Kooperation mit einer staatlichen oder staatlich anerkannten Berufsakademie aus Mitgliedstaaten der Europäischen Union (Franchising) duale Ausbildungsgänge durchführen, gilt § 83 Absatz 4 entsprechend.

(5) Die staatliche Anerkennung begründet keinen Anspruch auf staatliche Zuschüsse.
§ 85 Absatz 8 und 9 gilt entsprechend.

#### § 88   
Abschlussbezeichnungen

(1) Die Berufsakademien können nach der Akkreditierung ihrer Ausbildungsgänge die staatliche Abschlussbezeichnung „Bachelor“ mit dem Zusatz „(Berufsakademie – Brandenburg)“, abgekürzt „(BA – Brandenburg)“ nach den für entsprechende Fachhochschulstudiengänge geltenden Regeln verleihen.

(2) Bachelorabschlüsse nach Absatz 1 verleihen die gleichen Berechtigungen wie solche einer Hochschule.

#### § 89   
Verlust der staatlichen Anerkennung

(1) Die staatliche Anerkennung erlischt, wenn die Berufsakademie

1.  nicht innerhalb einer von der für die Berufsakademien zuständigen obersten Landesbehörde zu bestimmenden Frist den Ausbildungsbetrieb aufnimmt,
    
2.  ihren Hauptsitz im Geltungsbereich dieses Gesetzes aufgibt oder
    
3.  ein Jahr nicht betrieben worden ist.
    

Für Ausbildungsgänge, auf die sich die staatliche Anerkennung erstreckt, gilt Satz 1 Nummer 1 oder Nummer 3 entsprechend.

(2) Die staatliche Anerkennung ist zurückzunehmen oder zu widerrufen, wenn

1.  die Voraussetzungen für die Anerkennung im Zeitpunkt der Anerkennung nicht gegeben waren oder später weggefallen sind und diesem Mangel trotz Aufforderung durch die für Berufsakademien zuständige oberste Landesbehörde innerhalb einer zu bestimmenden Frist nicht abgeholfen worden ist oder
    
2.  die Trägerin oder der Träger oder die Leitung der Berufsakademie trotz schriftlicher Aufforderung der Verpflichtung nach Absatz 3 nicht nachkommt.
    

(3) Die Trägerin oder der Träger und die Leitung der Berufsakademie sind verpflichtet, der für die Berufsakademien zuständigen obersten Landesbehörde Auskünfte zu erteilen und alle Unterlagen zur Verfügung zu stellen, die erforderlich sind, um auf die fortlaufende Erfüllung der Voraussetzungen des § 87 Absatz 3 hinwirken zu können.

(4) Auszubildende haben keinen Anspruch gegen das Land Brandenburg auf die Beendigung ihrer Ausbildung.

#### § 90   
Ordnungswidrigkeiten

Ordnungswidrig handelt, wer

1.  unbefugt die Bezeichnung Universität, Hochschule, Fachhochschule, Kunsthochschule, Hochschulklinik, Universitätsklinik, eine zum Verwechseln ähnliche Bezeichnung oder eine entsprechende fremdsprachliche Bezeichnung im Namen führt oder in vergleichbarer Weise verwendet,
    
2.  eine nichtstaatliche Hochschule ohne die nach diesem Gesetz erforderliche staatliche Anerkennung errichtet und betreibt,
    
3.  den Studienbetrieb der Niederlassung einer staatlichen oder staatlich anerkannten Hochschule aus einem Mitgliedstaat der Europäischen Union oder aus einem anderen Land der Bundesrepublik Deutschland ohne vorherige rechtzeitige Anzeige bei der für die Hochschulen zuständigen obersten Landesbehörde aufnimmt oder
    
4.  unbefugt die Bezeichnung Berufsakademie führt.
    

Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu einhunderttausend Euro geahndet werden.
§ 34 Absatz 2 Satz 2 gilt entsprechend.

### Abschnitt 14   
Übergangsbestimmungen

#### § 91   
Übergangsbestimmungen zur Organisationsstruktur

Trifft die Grundordnung keine Bestimmungen nach § 64 Absatz 2 und § 72 Absatz 2, so nimmt der Senat die Aufgaben nach § 64 Absatz 2 und der Fachbereichsrat die Aufgaben nach § 72 Absatz 2 wahr.

#### § 92   
Überleitung des wissenschaftlichen und künstlerischen Personals

(1) Hauptberuflich tätige Angehörige des wissenschaftlichen und künstlerischen Personals, mit denen nicht ein Dienstverhältnis nach § 39 begründet wird, verbleiben in dem Arbeitsverhältnis, das vor Inkrafttreten dieses Gesetzes galt.
Das Arbeitsverhältnis kann nach anderen Vorschriften geändert oder beendet werden.
Für die Aufgaben, Rechte und Pflichten finden die Bestimmungen des Abschnitts 6 entsprechende Anwendung.

(2) Hauptberuflich tätige Angehörige des wissenschaftlichen und künstlerischen Personals nach Absatz 1 können nach Maßgabe ihrer Eignung, des Bedarfs in den jeweiligen Fächern und nach Maßgabe des Landeshaushalts in Dienstverhältnisse nach § 39 übernommen werden.
Die Übernahme setzt einen Antrag voraus; ein Anspruch auf Übernahme besteht nicht.

#### § 93   
Übergangsbestimmungen für bestimmte Dienstverhältnisse

(1) Die am 23.
März 2004 vorhandenen wissenschaftlichen und künstlerischen Assistentinnen und Assistenten, Oberassistentinnen und Oberassistenten, Oberingenieurinnen und Oberingenieure sowie Hochschuldozentinnen und Hochschuldozenten verbleiben in ihren bisherigen Dienstverhältnissen.
Ihre mitgliedschaftsrechtliche Stellung bleibt unverändert.
Für sie gelten die Bestimmungen des Brandenburgischen Hochschulgesetzes in der bis zum 23. März 2004 geltenden Fassung fort.

(2) Für Kanzlerinnen und Kanzler, die bereits am 19.
Dezember 2008 im Amt waren, gilt § 68 Absatz 4 des Brandenburgischen Hochschulgesetzes in der Fassung der Bekanntmachung vom 6.
Juli 2004 (GVBl.
I S. 394) fort; dies gilt auch, wenn aufgrund wiederholter Bestellung das Amt der Kanzlerin oder des Kanzlers erneut angetreten wurde.

#### § 94   
Übergangsbestimmung zu den Ordnungswidrigkeiten

§ 90 Satz 1 Nummer 1 gilt für Krankenhäuser, die am 21.
September 2018 die Ausbildung im Praktischen Jahr im Einvernehmen mit der zuständigen Gesundheitsbehörde nach der Approbationsordnung für Ärzte bereits übernommen haben, erst nach Abschluss des Verfahrens zur staatlichen Anerkennung als Hochschulklinik nach § 86a.