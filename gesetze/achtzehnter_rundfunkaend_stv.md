## Gesetz zum Achtzehnten Rundfunkänderungsstaatsvertrag

Der Landtag hat das folgende Gesetz beschlossen:

### § 1

Dem am 10.
September 2015 vom Land Brandenburg unterzeichneten Achtzehnten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Achtzehnter Rundfunkänderungsstaatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2

Durch dieses Gesetz wird das Recht auf Berufsfreiheit (Artikel 49 Absatz 1 Satz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

### § 3

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem Artikel 2 Absatz 2 Satz 1 am 1.
Januar 2016 in Kraft.
Sollte der Staatsvertrag nach seinem Artikel 2 Absatz 2 Satz 2 gegenstandslos werden, ist dies unverzüglich im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 17.
Dezember 2015

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag](/de/vertraege-241527) - Rundfunkstaatsvertrag

* * *

### Anlagen

1

[Achtzehnter Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Achtzehnter Rundfunkänderungsstaatsvertrag)](/br2/sixcms/media.php/68/GVBl_I_36_2015-001Anlage-zum-Hauptdokument.pdf "Achtzehnter Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Achtzehnter Rundfunkänderungsstaatsvertrag)") 626.1 KB