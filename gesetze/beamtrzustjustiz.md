## Gesetz zur Regelung beamtenrechtlicher Zuständigkeiten im Bereich der Justiz

#### § 1 

Das für Justiz zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung für beamtenrechtliche Entscheidungen der ihm im Geschäftsbereich Justiz nachgeordneten Beamtinnen und Beamten und Richterinnen und Richter die Zuständigkeit des Präsidenten oder der Präsidentin des Brandenburgischen Oberlandesgerichts, des Präsidenten oder der Präsidentin des Oberverwaltungsgerichts Berlin-Brandenburg, des Präsidenten oder der Präsidentin des Landessozialgerichts Berlin-Brandenburg oder des Generalstaatsanwalts oder der Generalstaatsanwältin des Landes Brandenburg zu bestimmen.
Der Präsident oder die Präsidentin des Brandenburgischen Oberlandesgerichts und der Generalstaatsanwalt oder die Generalstaatsanwältin des Landes Brandenburg können ihre Befugnis auf die ihnen unmittelbar nachgeordneten Dienstbehörden übertragen, soweit dies in der Rechtsverordnung nach Satz 1 bestimmt ist.