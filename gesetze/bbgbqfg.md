## Gesetz über die Feststellung der Gleichwertigkeit im Ausland erworbener Berufsqualifikationen in Brandenburg (Brandenburgisches Berufsqualifikationsfeststellungsgesetz - BbgBQFG)

Teil 1  
Allgemeiner Teil
-------------------------

#### § 1  
Zweck des Gesetzes

Dieses Gesetz dient der besseren Nutzung von im Ausland erworbenen Berufsqualifikationen für den deutschen Arbeitsmarkt, um eine qualifikationsnahe Beschäftigung zu ermöglichen.

#### § 2  
Anwendungsbereich

(1) Dieses Gesetz gilt für die Feststellung der Gleichwertigkeit im Ausland erworbener Ausbildungsnachweise unter Berücksichtigung sonstiger nachgewiesener Berufsqualifikationen und inländischer Ausbildungsnachweise für Berufe, die durch Rechtsvorschriften des Landes geregelt sind, sofern die entsprechenden berufsrechtlichen Regelungen des Landes nicht unter Bezugnahme auf dieses Gesetz etwas anderes bestimmen.
§ 10 des Bundesvertriebenengesetzes bleibt unberührt.

(2) Dieses Gesetz ist auf alle Personen anwendbar, die im Ausland einen Ausbildungsnachweis erworben haben und darlegen, in Brandenburg eine ihrer Berufsqualifikation entsprechende Erwerbstätigkeit ausüben zu wollen.

#### § 3  
Begriffsbestimmungen

(1) Berufsqualifikationen sind Qualifikationen, die durch Ausbildungsnachweise, Befähigungsnachweise oder einschlägige, im Inland oder Ausland erworbene Berufserfahrung nachgewiesen werden.

(2) Ausbildungsnachweise sind Prüfungszeugnisse und Befähigungsnachweise, die von verantwortlichen Stellen für den Abschluss einer erfolgreich absolvierten Ausbildung ausgestellt werden.

(3) Berufsbildung im Sinne dieses Gesetzes ist eine durch Rechts- oder Verwaltungsvorschriften geregelte Berufsausbildung, berufliche Fort- oder Weiterbildung.
Die Berufsausbildung vermittelt die zur Ausübung einer qualifizierten beruflichen Tätigkeit erforderliche berufliche Handlungsfähigkeit.
Sie findet in einem geordneten Ausbildungsgang statt, der auch den Erwerb der erforderlichen Berufserfahrungen umfassen kann.
Die berufliche Fort- und Weiterbildung erweitert die berufliche Handlungsfähigkeit über die Berufsausbildung hinaus.
Hochschulabschlüsse sind, soweit sie nicht Voraussetzung für die Aufnahme oder Ausübung eines reglementierten Berufs sind, keine Berufsbildung im Sinne dieses Gesetzes.

(4) Landesrechtlich geregelte Berufe umfassen nicht reglementierte Berufe und reglementierte Berufe.

(5) Reglementierte Berufe sind berufliche Tätigkeiten, deren Aufnahme oder Ausübung durch Rechts- oder Verwaltungsvorschriften an den Besitz bestimmter Berufsqualifikationen gebunden ist; eine Art der Ausübung ist insbesondere die Führung einer Berufsbezeichnung, die durch Rechts- oder Verwaltungsvorschriften auf Personen beschränkt ist, die über bestimmte Berufsqualifikationen verfügen.

(6) Der Europäische Berufsausweis ist eine elektronische Bescheinigung zum Nachweis der Anerkennung von Berufsqualifikationen für die Niederlassung in einem Aufnahmemitgliedstaat.

(7) Zuständige Behörden im Sinne der Richtlinie 2005/36/EG des Europäischen Parlaments und des Rates vom 7.
September 2005 über die Anerkennung von Berufsqualifikationen (ABl. L 255 vom 30.9.2005, S. 22), die zuletzt durch die Richtlinie 2013/55/EU (ABl.
L 354 vom 28.12.2013, S.
132) geändert worden ist, sowie der dazu ergehenden Durchführungsrechtsakte sind die zuständigen Stellen nach den §§ 8 und 13 Absatz 5 bis 7, soweit in diesem Gesetz oder im jeweiligen Fachrecht keine abweichende Regelung getroffen ist.

Teil 2  
Feststellung der Gleichwertigkeit
------------------------------------------

### Kapitel 1  
Nicht reglementierte Berufe

#### § 4  
Feststellung der Gleichwertigkeit

(1) Die zuständige Stelle stellt auf Antrag die Gleichwertigkeit fest, sofern

1.  der im Ausland erworbene Ausbildungsnachweis die Befähigung zu vergleichbaren beruflichen Tätigkeiten wie der entsprechende landesrechtlich geregelte Ausbildungsnachweis belegt und
    
2.  zwischen den nachgewiesenen Berufsqualifikationen und der entsprechenden landesrechtlich geregelten Berufsbildung keine wesentlichen Unterschiede bestehen.
    

(2) Wesentliche Unterschiede zwischen den nachgewiesenen Berufsqualifikationen und der entsprechenden landesrechtlich geregelten Berufsbildung liegen vor, sofern

1.  sich der im Ausland erworbene Ausbildungsnachweis auf Fertigkeiten, Kenntnisse und Fähigkeiten bezieht, die sich hinsichtlich der vermittelten Inhalte oder aufgrund der Ausbildungsdauer wesentlich von den Fertigkeiten, Kenntnissen und Fähigkeiten unterscheiden, auf die sich der entsprechende landesrechtlich geregelte Ausbildungsnachweis bezieht,
    
2.  die nach Nummer 1 abweichenden Fertigkeiten, Kenntnisse und Fähigkeiten für die Ausübung des jeweiligen Berufs wesentlich sind und
    
3.  die Antragstellerin oder der Antragsteller diese Unterschiede nicht durch sonstige Befähigungsnachweise, nachgewiesene einschlägige Berufserfahrung oder sonstige nachgewiesene einschlägige Qualifikationen ausgeglichen hat.
    

(3) In dem Umfang, in dem die zuständige Stelle eines Landes der Bundesrepublik Deutschland die Gleichwertigkeit festgestellt hat, ist die Inhaberin oder der Inhaber dieser Berufsqualifikation so zu behandeln, als sei insoweit die landesrechtlich geregelte Berufsqualifikation in diesem Land erworben worden.

#### § 5  
Vorzulegende Unterlagen

(1) Dem Antrag sind folgende Unterlagen beizufügen:

1.  eine tabellarische Aufstellung der absolvierten Ausbildungsgänge und der ausgeübten Erwerbstätigkeiten in deutscher Sprache,
    
2.  ein Identitätsnachweis,
    
3.  im Ausland erworbene Ausbildungsnachweise,
    
4.  Nachweise über einschlägige Berufserfahrung oder sonstige Befähigungsnachweise, sofern diese zur Feststellung der Gleichwertigkeit erforderlich sind,
    
5.  eine formlose Erklärung, ob und bei welcher Stelle bereits ein Antrag auf Feststellung der Gleichwertigkeit gestellt wurde, und
    
6.  ein gegebenenfalls erteilter Bescheid eines anderen Landes.
    

(2) Die Unterlagen nach Absatz 1 Nummer 2 bis 4 und 6 sind der zuständigen Stelle in Form von Kopien vorzulegen oder elektronisch zu übermitteln.
Von den Unterlagen nach Absatz 1 Nummer 3 und 4 sind Übersetzungen in deutscher Sprache vorzulegen.
Darüber hinaus kann die zuständige Stelle von den Unterlagen nach Absatz 1 Nummer 2 und allen nachgereichten Unterlagen Übersetzungen in deutscher Sprache verlangen.
Die Übersetzungen sind von einer öffentlich bestellten oder beeidigten Dolmetscherin oder Übersetzerin oder von einem öffentlich bestellten oder beeidigten Dolmetscher oder Übersetzer erstellen zu lassen.

(3) Die zuständige Stelle kann abweichend von Absatz 2 eine andere Form für die vorzulegenden Dokumente zulassen.

(4) Die zuständige Stelle kann die Antragstellerin oder den Antragsteller auffordern, innerhalb einer angemessenen Frist Informationen zu Inhalt und Dauer der im Ausland absolvierten Berufsbildung sowie zu sonstigen Berufsqualifikationen vorzulegen, soweit dies zur Feststellung der Gleichwertigkeit erforderlich ist.

(5) Bestehen begründete Zweifel an der Echtheit oder der inhaltlichen Richtigkeit der vorgelegten Unterlagen, kann die zuständige Stelle die Antragstellerin oder den Antragsteller auffordern, innerhalb einer angemessenen Frist Originale, beglaubigte Kopien oder weitere geeignete Unterlagen vorzulegen.

(6) Die Antragstellerin oder der Antragsteller hat durch geeignete Unterlagen darzulegen, in Brandenburg eine der Berufsqualifikationen entsprechende Erwerbstätigkeit ausüben zu wollen.
Geeignete Unterlagen können beispielsweise der Nachweis der Beantragung eines Einreisevisums zur Erwerbstätigkeit, der Nachweis einer Kontaktaufnahme mit potenziellen Arbeitgebern oder ein Geschäftskonzept sein.
Für Antragstellerinnen oder Antragsteller mit Wohnsitz in einem Mitgliedstaat der Europäischen Union, einem weiteren Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum oder in einem durch Abkommen gleichgestellten Staat sowie für Staatsangehörige dieser Staaten ist diese Darlegung entbehrlich, sofern keine besonderen Gründe gegen eine entsprechende Absicht sprechen.

#### § 6  
Verfahren

(1) Antragsberechtigt ist jede Person, die im Ausland einen Ausbildungsnachweis im Sinne des § 3 Absatz 2 erworben hat.
Der Antrag ist bei der zuständigen Stelle zu stellen.

(2) Die zuständige Stelle bestätigt der Antragstellerin oder dem Antragsteller innerhalb eines Monats den Eingang des Antrags einschließlich der nach § 5 Absatz 1 vorgelegten Unterlagen.
In der Empfangsbestätigung ist das Datum des Eingangs bei der zuständigen Stelle mitzuteilen und auf die Frist nach Absatz 3 und die Voraussetzungen für den Beginn des Fristlaufs hinzuweisen.
Sind die nach § 5 Absatz 1 vorzulegenden Unterlagen unvollständig, teilt die zuständige Stelle innerhalb der Frist des Satzes 1 mit, welche Unterlagen nachzureichen sind.
Die Mitteilung enthält den Hinweis, dass der Lauf der Frist nach Absatz 3 erst mit Eingang der vollständigen Unterlagen beginnt.

(3) Die zuständige Stelle muss innerhalb von drei Monaten über die Gleichwertigkeit entscheiden.
Die Frist beginnt mit Eingang der vollständigen Unterlagen.
Sie kann einmal angemessen verlängert werden, wenn dies wegen der Besonderheiten der Angelegenheit gerechtfertigt ist.
Die Fristverlängerung ist zu begründen und rechtzeitig mitzuteilen.

(4) Im Fall des § 5 Absatz 4 und 5 ist der Lauf der Frist nach Absatz 3 bis zum Ablauf der von der zuständigen Stelle festgelegten Frist gehemmt.
Im Fall des § 14 ist der Lauf der Frist nach Absatz 3 bis zu Beendigung des sonstigen geeigneten Verfahrens gehemmt.

(5) Der Antrag soll abgelehnt werden, soweit die Gleichwertigkeit im Rahmen anderer Verfahren oder durch Rechtsvorschrift bereits festgestellt ist.

(6) Das Verfahren kann auch über den Einheitlichen Ansprechpartner im Sinne des Gesetzes über den Einheitlichen Ansprechpartner für das Land Brandenburg abgewickelt werden.

#### § 7  
Form der Entscheidung

(1) Die Entscheidung über den Antrag nach § 4 Absatz 1 ergeht durch schriftlichen oder elektronischen Bescheid.

(2) Ist der Antrag abzulehnen, weil die Feststellung der Gleichwertigkeit wegen wesentlicher Unterschiede im Sinne des § 4 Absatz 2 nicht erfolgen kann, sind in der Begründung auch die vorhandenen Berufsqualifikationen der Antragstellerin oder des Antragstellers sowie die wesentlichen Unterschiede zwischen den vorhandenen Berufsqualifikationen und der entsprechenden landesrechtlich geregelten Berufsbildung darzulegen.

(3) Dem Bescheid ist eine Rechtsbehelfsbelehrung beizufügen.

#### § 8  
Zuständige Stelle

(1) Zuständige Stelle im Sinne dieses Kapitels ist die für die jeweilige Berufsbildung zuständige oberste Landesbehörde.
Die zuständige oberste Landesbehörde wird ermächtigt, die nach diesem Kapitel vorgesehenen Aufgaben abweichend von Satz 1 durch Rechtsverordnung auf Behörden oder Kammern zu übertragen.
Die Verordnung kann auch vorsehen, dass die Zuständigkeit weiter übertragen werden kann.

(2) Die zuständigen Stellen können vereinbaren, dass die ihnen durch oder aufgrund dieses Gesetzes übertragenen Aufgaben von einer anderen Stelle oder Kammer, deren Sitz auch in einem anderen Land der Bundesrepublik Deutschland sein kann, wahrgenommen werden.
Die Vereinbarung bedarf der Genehmigung der jeweils zuständigen Aufsichtsbehörden.

### Kapitel 2  
Reglementierte Berufe

#### § 9  
Voraussetzungen der Gleichwertigkeit

(1) Bei der Entscheidung über die Befugnis zur Aufnahme oder Ausübung eines in Brandenburg reglementierten Berufs gilt der im Ausland erworbene Ausbildungsnachweis, unter Berücksichtigung sonstiger nachgewiesener Berufsqualifikationen, als gleichwertig mit dem entsprechenden landesrechtlich geregelten Ausbildungsnachweis, sofern

1.  der im Ausland erworbene Ausbildungsnachweis die Befähigung zu vergleichbaren beruflichen Tätigkeiten wie der entsprechende landesrechtlich geregelte Ausbildungsnachweis belegt,
    
2.  die Antragstellerin oder der Antragsteller bei einem sowohl in Brandenburg als auch im Ausbildungsstaat reglementierten Beruf zur Ausübung des jeweiligen Berufs im Ausbildungsstaat berechtigt ist oder die Befugnis zu Aufnahme oder Ausübung des jeweiligen Berufs aus Gründen verwehrt wurde, die der Aufnahme oder Ausübung in Brandenburg nicht entgegenstehen, und
    
3.  zwischen den nachgewiesenen Berufsqualifikationen und der entsprechenden landesrechtlich geregelten Berufsbildung keine wesentlichen Unterschiede bestehen.
    

(2) Wesentliche Unterschiede zwischen den nachgewiesenen Berufsqualifikationen und der entsprechenden landesrechtlich geregelten Berufsbildung liegen vor, sofern

1.  sich der im Ausland erworbene Ausbildungsnachweis auf Fähigkeiten und Kenntnisse bezieht, die sich hinsichtlich des Inhalts oder aufgrund der Ausbildungsdauer wesentlich von den Fähigkeiten und Kenntnissen unterscheiden, auf die sich der entsprechende landesrechtlich geregelte Ausbildungsnachweis bezieht,
    
2.  die entsprechenden Fähigkeiten und Kenntnisse eine maßgebliche Voraussetzung für die Ausübung des jeweiligen Berufs darstellen und
    
3.  die Antragstellerin oder der Antragsteller diese Unterschiede nicht durch sonstige Befähigungsnachweise, nachgewiesene einschlägige Berufserfahrung oder sonstige nachgewiesene einschlägige Qualifikationen ausgeglichen hat.
    

#### § 10  
Feststellung der vorhandenen Berufsqualifikationen

(1) Sofern die Feststellung der Gleichwertigkeit wegen wesentlicher Unterschiede im Sinne des § 9 Absatz 2 nicht erfolgen kann, werden bei der Entscheidung über die Befugnis zur Aufnahme oder Ausübung eines in Brandenburg reglementierten Berufs die vorhandenen Berufsqualifikationen und die wesentlichen Unterschiede gegenüber der entsprechenden landesrechtlich geregelten Berufsbildung durch Bescheid festgestellt.
Der Bescheid hat sowohl eine Mitteilung über das Niveau der von der Antragstellerin oder dem Antragsteller vorgelegten Berufsqualifikation als auch über das durch die landesrechtlich geregelte Berufsbildung verlangte Niveau im Sinne des Artikels 11 der Richtlinie 2005/36/EG zu enthalten.

(2) In dem Bescheid wird zudem festgestellt, durch welche Maßnahmen nach § 11 die wesentlichen Unterschiede gegenüber der erforderlichen landesrechtlich geregelten Berufsqualifikation ausgeglichen werden können.

(3) In dem Umfang, in dem die zuständige Stelle eines Landes der Bundesrepublik Deutschland die Gleichwertigkeit festgestellt hat, ist die Inhaberin oder der Inhaber dieser Berufsqualifikation so zu behandeln, als sei insoweit die landesrechtlich geregelte Berufsqualifikation in diesem Land erworben worden.

#### § 11  
Ausgleichsmaßnahmen

(1) Wesentliche Unterschiede im Sinne des § 9 Absatz 2 können durch die Absolvierung eines höchstens dreijährigen Anpassungslehrgangs, der Gegenstand einer Bewertung sein kann, oder das Ablegen einer Eignungsprüfung im Inland ausgeglichen werden.

(2) Bei der Ausgestaltung der Ausgleichsmaßnahmen im Sinne des Absatzes 1 sind die vorhandenen Berufsqualifikationen der Antragstellerin oder des Antragstellers zu berücksichtigen.
Der Inhalt der Ausgleichsmaßnahmen ist auf die festgestellten wesentlichen Unterschiede im Sinne des § 9 Absatz 2 zu beschränken.
Inhalt und Durchführung der Ausgleichsmaßnahmen können durch das für das jeweilige Berufsrecht zuständige Mitglied der Landesregierung durch Rechtsverordnung geregelt werden.

(3) Die Antragstellerin oder der Antragsteller hat die Wahl zwischen der Absolvierung eines Anpassungslehrgangs und dem Ablegen einer Eignungsprüfung, sofern die entsprechenden berufsrechtlichen Regelungen nichts anderes bestimmen.

(4) Hat sich die Antragstellerin oder der Antragsteller für eine Eignungsprüfung nach Absatz 3 entschieden, muss diese innerhalb von sechs Monaten abgelegt werden können.
Legt aufgrund entsprechender berufsrechtlicher Regelungen im Sinne des Absatzes 3 die zuständige Stelle fest, dass eine Eignungsprüfung zu absolvieren ist, so muss diese innerhalb von sechs Monaten ab dem Zugang dieser Entscheidung abgelegt werden können.

#### § 12  
Vorzulegende Unterlagen

(1) Zur Bewertung der Gleichwertigkeit sind dem Antrag auf Befugnis zur Aufnahme oder Ausübung eines in Brandenburg reglementierten Berufs folgende Unterlagen beizufügen:

1.  eine tabellarische Aufstellung der absolvierten Ausbildungsgänge und der ausgeübten Erwerbstätigkeiten in deutscher Sprache,
    
2.  ein Identitätsnachweis,
    
3.  im Ausland erworbene Ausbildungsnachweise,
    
4.  Nachweise über einschlägige Berufserfahrungen und sonstige Befähigungsnachweise, sofern diese zur Feststellung der Gleichwertigkeit erforderlich sind,
    
5.  im Fall von § 9 Absatz 1 Nummer 2 eine Bescheinigung über die Berechtigung zur Berufsausübung im Ausbildungsstaat,
    
6.  eine formlose Erklärung, ob und bei welcher Stelle bereits ein Antrag auf Feststellung der Gleichwertigkeit gestellt wurde, und
    
7.  ein gegebenenfalls erteilter Bescheid eines anderen Landes.
    

(2) Die Unterlagen nach Absatz 1 Nummer 2 bis 5 und 7 sind der zuständigen Stelle in Form von Kopien vorzulegen oder elektronisch zu übermitteln.
Von den Unterlagen nach Absatz 1 Nummer 3 bis 5 sind Übersetzungen in deutscher Sprache vorzulegen.
Darüber hinaus kann die zuständige Stelle von den Unterlagen nach Absatz 1 Nummer 2 und allen nachgereichten Unterlagen Übersetzungen in deutscher Sprache verlangen.
Die Übersetzungen sind von einer öffentlich bestellten oder beeidigten Dolmetscherin oder Übersetzerin oder von einem öffentlich bestellten oder beeidigten Dolmetscher oder Übersetzer erstellen zu lassen.

(3) Die zuständige Stelle kann abweichend von Absatz 2 eine andere Form für die vorzulegenden Dokumente zulassen.

(4) Die zuständige Stelle kann die Antragstellerin oder den Antragsteller auffordern, innerhalb einer angemessenen Frist Informationen zu Inhalt und Dauer der im Ausland absolvierten Berufsbildung sowie zu sonstigen Berufsqualifikationen vorzulegen, soweit dies zur Bewertung der Gleichwertigkeit erforderlich ist.
Soweit die Berufsbildung in einem Mitgliedstaat der Europäischen Union, einem weiteren Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum oder in einem durch Abkommen gleichgestellten Staat absolviert wurde, kann sich die zuständige Stelle an die zuständige Stelle des Ausbildungsstaates wenden.

(5) Bestehen begründete Zweifel an der Echtheit oder der inhaltlichen Richtigkeit der vorgelegten Unterlagen, kann die zuständige Stelle die Antragstellerin oder den Antragsteller auffordern, innerhalb einer angemessenen Frist Originale, beglaubigte Kopien oder weitere geeignete Unterlagen vorzulegen.
Bei Unterlagen, die in einem Mitgliedstaat der Europäischen Union oder einem weiteren Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum ausgestellt oder anerkannt wurden, kann sich die zuständige Stelle im Fall begründeter Zweifel an der Echtheit der Unterlagen sowohl an die zuständige Stelle des Ausbildungs- oder Anerkennungsstaats wenden als auch die Antragstellerin oder den Antragsteller auffordern, beglaubigte Kopien vorzulegen.
In den Fällen des Satzes 2 hemmt eine solche Aufforderung nicht den Fristlauf nach § 13 Absatz 3.

(6) Die Antragstellerin oder der Antragsteller hat durch geeignete Unterlagen darzulegen, in Brandenburg eine ihren Berufsqualifikationen entsprechende Erwerbstätigkeit ausüben zu wollen.
Geeignete Unterlagen können beispielsweise der Nachweis der Beantragung eines Einreisevisums zur Erwerbstätigkeit, der Nachweis einer Kontaktaufnahme mit potenziellen Arbeitgebern oder ein Geschäftskonzept sein.
Für Antragstellerinnen oder Antragsteller mit Wohnsitz in einem Mitgliedstaat der Europäischen Union, einem weiteren Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum oder in einem durch Abkommen gleichgestellten Staat sowie für Staatsangehörige dieser Staaten ist diese Darlegung entbehrlich, sofern keine besonderen Gründe gegen eine entsprechende Absicht sprechen.

#### § 13  
Verfahren

(1) Die Bewertung der Gleichwertigkeit nach § 9 erfolgt im Rahmen der Entscheidung über die Befugnis zur Aufnahme oder Ausübung eines in Brandenburg reglementierten Berufs.
Auf Antrag erteilt die zuständige Stelle der Antragstellerin oder dem Antragsteller einen gesonderten Bescheid über die Feststellung der Gleichwertigkeit ihrer oder seiner Berufsqualifikation oder entscheidet auf Antrag nur über die Gleichwertigkeit der Berufsqualifikation.

(2) Die zuständige Stelle bestätigt der Antragstellerin oder dem Antragsteller innerhalb eines Monats den Eingang des Antrags einschließlich der nach § 12 Absatz 1 vorzulegenden Unterlagen.
In der Empfangsbestätigung ist das Datum des Eingangs bei der zuständigen Stelle mitzuteilen und auf die Frist nach Absatz 3 und die Voraussetzungen für den Beginn des Fristlaufs hinzuweisen.
Sind die nach § 12 Absatz 1 vorzulegenden Unterlagen unvollständig, teilt die zuständige Stelle innerhalb der Frist des Satzes 1 mit, welche Unterlagen nachzureichen sind.
Die Mitteilung enthält den Hinweis, dass der Lauf der Frist nach Absatz 3 erst mit Eingang der vollständigen Unterlagen beginnt.

(3) Die zuständige Stelle muss innerhalb von drei Monaten über die Gleichwertigkeit entscheiden.
Die Frist beginnt mit Eingang der vollständigen Unterlagen.
Sie kann einmal angemessen verlängert werden, wenn dies wegen der Besonderheiten der Angelegenheit gerechtfertigt ist.
Für Antragstellerinnen und Antragsteller, die ihren Ausbildungsnachweis in einem Mitgliedstaat der Europäischen Union oder einem Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum oder in einem durch Abkommen gleichgestellten Staat erworben haben oder deren Ausbildungsnachweise in einem dieser genannten Staaten anerkannt wurde, kann die Fristverlängerung nach Satz 3 höchstens einen Monat betragen.
Die Fristverlängerung ist zu begründen und rechtzeitig mitzuteilen.

(4) Im Fall des § 12 Absatz 4 und 5 Satz 1 ist der Lauf der Frist nach Absatz 3 bis zum Ablauf der von der zuständigen Stelle festgelegten Frist gehemmt.
Im Fall des § 14 ist der Lauf der Frist nach Absatz 3 bis zu Beendigung des sonstigen geeigneten Verfahrens gehemmt.

(5) Die zuständige Stelle richtet sich nach dem jeweiligen Fachrecht.

(6) Das für das jeweilige Berufsrecht zuständige Mitglied der Landesregierung wird ermächtigt, die Aufgaben durch Rechtsverordnung auf andere Stellen, die Aufgaben der öffentlichen Verwaltung wahrnehmen, zu übertragen.

(7) Zuständige Stellen können vereinbaren, dass die ihnen durch dieses oder aufgrund dieses Gesetzes übertragenen Aufgaben von einer anderen zuständigen Stelle, deren Sitz auch in einem anderen Land der Bundesrepublik Deutschland sein kann, wahrgenommen werden.
Die Vereinbarung bedarf der Genehmigung der jeweils zuständigen obersten Landesbehörde.

(8) Das Verfahren kann auch über den Einheitlichen Ansprechpartner im Sinne des Gesetzes über den Einheitlichen Ansprechpartner für das Land Brandenburg abgewickelt werden.

#### § 13a  
Europäischer Berufsausweis; Verordnungsermächtigung

(1) Für Berufe, für die aufgrund von Durchführungsrechtsakten der Europäischen Kommission nach Artikel 4a Absatz 7 der Richtlinie 2005/36/EG ein Europäischer Berufsausweis eingeführt ist, stellt die zuständige Stelle auf Antrag einen Europäischen Berufsausweis aus.

(2) Der Europäische Berufsausweis kann von Personen beantragt werden, die ihren Ausbildungsnachweis in einem Mitgliedstaat der Europäischen Union oder einem anderen Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum erworben haben oder deren Ausbildungsnachweise in einem dieser Staaten anerkannt wurden.

(3) Das Verfahren richtet sich nach den Artikeln 4a bis 4e der Richtlinie 2005/36/EG sowie den dazu ergangenen Durchführungsrechtsakten.

(4) Das für das jeweilige Berufsrecht zuständige Mitglied der Landesregierung wird ermächtigt, ergänzend zu den Bestimmungen des Durchführungsrechtsakts durch Rechtsverordnung weitere Regelungen zur Umsetzung des Artikels 4a Absatz 7 der Richtlinie 2005/36/EG zu treffen.

(5) Die Absätze 1 bis 4 lassen die Verfahren nach den §§ 9 bis 13 unberührt.

#### § 13b  
Vorwarnmechanismus; Verordnungsermächtigung

(1) Hat die zuständige Stelle davon Kenntnis erlangt, dass einer oder einem Berufsangehörigen durch gerichtliche Entscheidung oder durch Verwaltungsakt die Ausübung eines in Artikel 56a Absatz 1 der Richtlinie 2005/36/EG genannten Berufes ganz oder teilweise - auch vorübergehend - untersagt worden ist oder ihr oder ihm diesbezügliche Beschränkungen auferlegt worden sind, so hat sie die zuständigen Stellen aller anderen Mitgliedstaaten sowie der Länder der Bundesrepublik Deutschland hiervon zu unterrichten (Vorwarnung).
Die zuständige Stelle übermittelt die in Artikel 56a Absatz 2 der Richtlinie 2005/36/EG genannten Daten über das Binnenmarkt-Informationssystem (IMI).

(2) Die Vorwarnung hat zu erfolgen, sobald eine vollziehbare Entscheidung eines Gerichts oder einer sonst zuständigen Stelle vorliegt.
Ebenso sind die zuständigen Stellen der Mitgliedstaaten sowie der Länder der Bundesrepublik Deutschland unverzüglich zu unterrichten, wenn die Geltungsdauer einer Untersagung oder Beschränkung nach Absatz 1 abgelaufen ist.
Im Rahmen der Unterrichtung hat die zuständige Stelle auch das Datum des Ablaufs der Maßnahme und gegebenenfalls spätere Änderungen dieses Datums anzugeben.
Gleichzeitig mit der Übermittlung einer Vorwarnung ist die zuständige Stelle verpflichtet, die hiervon betroffene Person darüber zu unterrichten,

1.  welchen Rechtsbehelf sie gegen die Vorwarnung einlegen kann,
2.  dass sie die Berichtigung der Vorwarnung verlangen kann und
3.  dass ihr im Falle einer unrichtigen Übermittlung ein Schadensersatzanspruch zusteht.

Die zuständige Stelle unterrichtet die zuständigen Stellen der Mitgliedstaaten sowie der Länder der Bundesrepublik Deutschland darüber, wenn eine betroffene Person einen Rechtsbehelf gegen die Vorwarnung eingelegt hat.
Sobald die Vorwarnung oder Teile davon unrichtig werden, sind sie unverzüglich zu löschen.

(3) Hat jemand die Anerkennung seiner Berufsqualifikation beantragt und wird nachfolgend von einem Gericht rechtskräftig festgestellt, dass die Person dabei gefälschte Berufsqualifikationsnachweise im Sinne der §§ 267 bis 271 des Strafgesetzbuchs verwendet hat, so hat die zuständige Stelle alle übrigen Mitgliedstaaten sowie die Länder der Bundesrepublik Deutschland über das Binnenmarkt-Informationssystem (IMI) von der Identität dieser Person zu informieren.

(4) Soweit es sich bei der Untersagung der Berufsausübung nach Absatz 1 Satz 1 um eine vollziehbare gerichtliche Anordnung eines Berufsverbotes handelt oder eine rechtskräftige Entscheidung nach Absatz 3 vorliegt, hat das Gericht oder die Vollstreckungsbehörde, bei dem oder bei der das Verfahren zum Zeitpunkt des Eintritts der Vollziehbarkeit oder der Rechtskraft anhängig ist, spätestens drei Tage nach Eintritt der Vollziehbarkeit oder der Rechtskraft der Entscheidung die Unterrichtung der anderen Mitgliedstaaten sowie der Länder der Bundesrepublik Deutschland über die nach Absatz 1 Satz 2 oder Absatz 3 jeweils erforderlichen Angaben durch das Binnenmarkt-Informationssystem (IMI) vorzunehmen, sofern sich die oberste Landesbehörde, die für die jeweilige Gerichtsbarkeit zuständig ist, die Meldung nicht vorbehalten hat.

(5) Bei der Verarbeitung personenbezogener Daten sind die Rechtsvorschriften über den Schutz personenbezogener Daten zu beachten.

(6) Das Verfahren richtet sich im Übrigen nach Artikel 56a der Richtlinie 2005/36/EG sowie den dazu ergangenen Durchführungsrechtsakten.

(7) Das für das jeweilige Berufsrecht zuständige Mitglied der Landesregierung wird ermächtigt, ergänzend zu den Bestimmungen der Durchführungsrechtsakte durch Rechtsverordnung weitere Regelungen zur Umsetzung des Artikels 56a der Richtlinie 2005/36/EG zu treffen.

#### § 13c  
Partieller Zugang; Verordnungsermächtigung

(1) Liegen die Voraussetzungen des Artikels 4f der Richtlinie 2005/36/EG vor, so gewährt die zuständige Stelle gemäß den Vorgaben dieses Artikels auf Antrag und unter Berücksichtigung des Einzelfalls einen partiellen Zugang zu einer reglementierten Berufstätigkeit.

(2) Der partielle Zugang kann verweigert werden, wenn diese Verweigerung durch zwingende Gründe des Allgemeininteresses gerechtfertigt und geeignet ist, die Erreichung des verfolgten Ziels zu gewährleisten und nicht über das hinausgeht, was zur Erreichung dieses Ziels erforderlich ist.

(3) Die Berufsbezeichnung ist in deutscher Sprache zu führen.

(4) Das für das jeweilige Berufsrecht zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung weitere Regelungen zur Umsetzung des Artikels 4f der Richtlinie 2005/36/EG zu treffen.

### Kapitel 3  
Gemeinsame Vorschriften

#### § 14  
Sonstige Verfahren zur Feststellung der Gleichwertigkeit bei fehlenden Nachweisen

(1) Kann die Antragstellerin oder der Antragsteller die für die Feststellung oder Bewertung der Gleichwertigkeit erforderlichen Nachweise nach § 5 Absatz 1, 4 und 5 oder § 12 Absatz 1, 4 und 5 aus selbst nicht zu vertretenden Gründen nicht oder nur teilweise vorlegen oder ist die Vorlage der entsprechenden Unterlagen mit einem unangemessenen zeitlichen und sachlichen Aufwand verbunden, stellt die zuständige Stelle die für einen Vergleich mit der entsprechenden inländischen Berufsbildung maßgeblichen beruflichen Fertigkeiten, Kenntnisse und Fähigkeiten der Antragstellerin oder des Antragstellers durch sonstige geeignete Verfahren fest.
Die Antragstellerin oder der Antragsteller hat die Gründe glaubhaft zu machen, die einer Vorlage der entsprechenden Unterlagen entgegenstehen.
Die zuständige Stelle ist befugt, eine Versicherung an Eides statt zu verlangen und abzunehmen.

(2) Sonstige geeignete Verfahren zur Ermittlung der beruflichen Fertigkeiten, Kenntnisse und Fähigkeiten im Sinne des Absatzes 1 sind insbesondere Arbeitsproben, Fachgespräche, praktische und theoretische Prüfungen sowie Gutachten von Sachverständigen.

(3) Die Feststellung oder Bewertung der Gleichwertigkeit nach § 4 oder § 9 erfolgt auf der Grundlage der Ergebnisse der in den Absätzen 1 und 2 vorgesehenen sonstigen Verfahren.

#### § 14a  
Beschleunigtes Verfahren im Fall des § 81a des Aufenthaltsgesetzes

(1) Im Fall des § 81a des Aufenthaltsgesetzes erfolgt die Feststellung der Gleichwertigkeit nach den §§ 4 und 9 auf Antrag bei der dafür zuständigen Stelle.
Antragsberechtigt ist jede Person, die im Ausland einen Ausbildungsnachweis im Sinne des § 3 Absatz 2 erworben hat.
Die Zuleitung der Anträge erfolgt durch die zuständige Ausländerbehörde nach § 71 Absatz 1 des Aufenthaltsgesetzes.

(2) Die zuständige Stelle bestätigt der Antragstellerin oder dem Antragsteller innerhalb von zwei Wochen den Eingang des Antrags einschließlich der nach § 5 Absatz 1 oder § 12 Absatz 1 vorzulegenden Unterlagen.
In der Empfangsbestätigung ist das Datum des Eingangs bei der zuständigen Stelle mitzuteilen und auf die Frist nach Absatz 3 und die Voraussetzungen für den Beginn des Fristlaufs hinzuweisen.
Sind die nach § 5 Absatz 1 oder § 12 Absatz 1 vorzulegenden Unterlagen unvollständig, teilt die zuständige Stelle innerhalb der Frist des Satzes 1 mit, welche Unterlagen nachzureichen sind.
Die Mitteilung enthält den Hinweis, dass der Lauf der Frist nach Absatz 3 erst mit Eingang der vollständigen Unterlagen beginnt.
Der Schriftwechsel erfolgt über die zuständige Ausländerbehörde nach § 71 Absatz 1 des Aufenthaltsgesetzes.

(3) Die zuständige Stelle soll innerhalb von zwei Monaten über die Gleichwertigkeit entscheiden.
Die Frist beginnt mit Eingang der vollständigen Unterlagen.
Sie kann einmal angemessen verlängert werden, wenn dies wegen der Besonderheiten der Angelegenheit gerechtfertigt ist.
Die Fristverlängerung ist zu begründen und rechtzeitig mitzuteilen.
Der Schriftwechsel erfolgt über und die Zustellung der Entscheidung erfolgt durch die zuständige Ausländerbehörde nach § 71 Absatz 1 des Aufenthaltsgesetzes an den Arbeitgeber.

(4) In den Fällen des § 5 Absatz 4 oder Absatz 5 oder § 12 Absatz 4 oder Absatz 5 ist der Lauf der Frist nach Absatz 3 bis zum Ablauf der von der zuständigen Stelle festgelegten Frist gehemmt.
In den Fällen des § 14 ist der Lauf der Frist nach Absatz 3 bis zur Beendigung des sonstigen geeigneten Verfahrens gehemmt.

(5) Die Entscheidung der zuständigen Stelle richtet sich nach dem jeweiligen Fachrecht.
Das beschleunigte Verfahren kann auch über den Einheitlichen Ansprechpartner im Sinne des Gesetzes über den Einheitlichen Ansprechpartner für das Land Brandenburg abgewickelt werden.

(6) Der Antrag auf Feststellung nach § 4 soll abgelehnt werden, wenn die Gleichwertigkeit im Rahmen anderer Verfahren oder durch Rechtsvorschrift bereits festgestellt ist.

#### § 15  
Mitwirkungspflichten

(1) Die Antragstellerin oder der Antragsteller ist verpflichtet, alle für die Ermittlung der Gleichwertigkeit notwendigen Unterlagen vorzulegen sowie alle dazu erforderlichen Auskünfte zu erteilen.

(2) Kommt die Antragstellerin oder der Antragsteller dieser Mitwirkungspflicht nicht nach und wird hierdurch die Aufklärung des Sachverhalts erheblich erschwert, kann die zuständige Stelle ohne weitere Ermittlungen entscheiden.
Dies gilt entsprechend, wenn die Antragstellerin oder der Antragsteller in anderer Weise die Aufklärung des Sachverhalts erheblich erschwert.

(3) Der Antrag darf wegen fehlender Mitwirkung nur abgelehnt werden, nachdem die Antragstellerin oder der Antragsteller auf die Folge schriftlich oder elektronisch hingewiesen worden ist und der Mitwirkungspflicht nicht innerhalb einer angemessenen Frist nachgekommen ist.

### § 16  
Rechtsweg

Für Streitigkeiten nach diesem Gesetz ist der Verwaltungsrechtsweg gegeben.

### Teil 3  
Schlussvorschriften

#### § 17  
Statistik

(1) Über die Verfahren zur Feststellung der Gleichwertigkeit nach diesem Gesetz und nach anderen berufsrechtlichen Gesetzen und Verordnungen wird eine Landesstatistik durchgeführt.

(2) Die Statistik erfasst jährlich für das vorausgegangene Kalenderjahr folgende Erhebungsmerkmale:

1.  Staatsangehörigkeit, Geschlecht, Wohnort der Antragstellerin oder des Antragstellers, Datum der Empfangsbestätigung, Datum der Vollständigkeit der vorzulegenden Unterlagen,
    
2.  Ausbildungsstaat, deutscher Referenzberuf oder deutsche Referenzausbildung,
    
3.  Datum der Entscheidung, Gegenstand und Art der Entscheidung, Besonderheit im Verfahren,
    
4.  Meldungen und Entscheidungen betreffend die Dienstleistungsfreiheit nach Artikel 7 Absatz 1 und 4 der Richtlinie 2005/36/EG,
    
5.  eingelegte Rechtsbehelfe und Entscheidungen darüber.
    

(3) Hilfsmerkmale sind:

1.  Name und Anschrift der Auskunftspflichtigen,
    
2.  Name und Telefonnummer sowie Adresse für elektronische Post der für Rückfragen zur Verfügung stehenden Person,
    
3.  Datensatznummer.

(4) Für die Erhebung besteht Auskunftspflicht.
Die Angaben nach Absatz 3 Nummer 2 sind freiwillig.
Auskunftspflichtig sind die nach diesem Gesetz und nach anderen berufsrechtlichen Gesetzen und Verordnungen für die Verfahren zur Feststellung der Gleichwertigkeit zuständigen Stellen.

(5) Die Angaben sind elektronisch an das Amt für Statistik Berlin-Brandenburg zu übermitteln, wobei die für die amtliche Statistik geltenden Standards zu nutzen sind.
Ergänzend zu § 17 Absatz 5 des Brandenburgischen Statistikgesetzes darf das Amt für Statistik Berlin-Brandenburg Daten an das Statistische Bundesamt und die Statistischen Ämter der Länder auch zur Erstellung länderübergreifender Regionalstatistiken übermitteln.

(6) Die Landesregierung wird ermächtigt,

1.  die Erhebung einzelner Merkmale auszusetzen, die Periodizität zu verlängern sowie den Kreis der zu Befragenden einzuschränken, wenn die Ergebnisse nicht mehr oder nicht mehr in der ursprünglich vorgesehenen Ausführlichkeit oder Häufigkeit benötigt werden;
    
2.  einzelne neue Merkmale einzuführen, wenn dies zur Deckung eines geänderten Bedarfs für den in § 1 genannten Zweck erforderlich ist und durch gleichzeitige Aussetzung anderer Merkmale eine Erweiterung des Erhebungsumfangs vermieden wird; nicht eingeführt werden können Merkmale, die besondere Kategorien personenbezogener Daten nach Artikel 9 Absatz 1 der Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates vom 27.
    April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr und zur Aufhebung der Richtlinie 95/46/EG (Datenschutz-Grundverordnung) (ABl.
    L 119 vom 4.5.2016, S.
    1; L 314 vom 22.11.2016, S.
    72) betreffen;
    
3.  die Erhebung von Merkmalen anzuordnen, soweit dies zur Umsetzung oder Durchführung von Rechtsakten der Europäischen Gemeinschaft erforderlich ist.
    

(7) An die obersten Bundes- und Landesbehörden dürfen für die Verwendung gegenüber den gesetzgebenden Körperschaften und für Zwecke der Planung, nicht jedoch für die Regelung in Einzelfällen, vom Amt für Statistik Berlin-Brandenburg Tabellen mit statistischen Ergebnissen übermittelt werden, auch soweit Tabellenfelder nur einen einzigen Fall ausweisen.
Für die Übermittlung gilt § 8 des Brandenburgischen Datenschutzgesetzes.