## Gesetz für die Übergangsphase nach dem Austritt des Vereinigten Königreichs Großbritannien und Nordirland aus der Europäischen Union des Landes Brandenburg (Brexit-Übergangsgesetz des Landes Brandenburg - BbgBrexitÜG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Übergangsregelung

Das Vereinigte Königreich Großbritannien und Nordirland gilt während des Übergangszeitraums gemäß Artikel 126 und 132 des Abkommens über den Austritt des Vereinigten Königreichs Großbritannien und Nordirland aus der Europäischen Union und der Europäischen Atomgemeinschaft vorbehaltlich der in § 2 genannten Ausnahme als Mitgliedstaat der Europäischen Union und der Europäischen Atomgemeinschaft.

#### § 2 Ausnahme

§ 1 findet keine Anwendung auf das Wahlrecht und die Wählbarkeit von Unionsbürgerinnen und Unionsbürgern bei den Kommunalwahlen sowie auf das Eintragungsrecht bei Bürgerbegehren und das Stimmrecht bei Bürgerentscheiden.

#### § 3 Inkrafttreten

Dieses Gesetz tritt an dem Tag in Kraft, an dem das Abkommen über den Austritt des Vereinigten Königreichs Großbritannien und Nordirland aus der Europäischen Union und der Europäischen Atomgemeinschaft in Kraft tritt.
Der Minister der Justiz und für Europa und Verbraucherschutz gibt den Tag des Inkrafttretens im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt.

Potsdam, den 1.
April 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark