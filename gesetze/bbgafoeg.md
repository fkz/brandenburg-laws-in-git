## Brandenburgisches Ausbildungsförderungsgesetz (BbgAföG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1  
Ziele und Grundsätze

(1) Die Landesausbildungsförderung nach Maßgabe dieses Gesetzes soll Schülerinnen und Schülern helfen, einen zur allgemeinen Hochschulreife oder zur Fachhochschulreife führenden Bildungsgang erfolgreich abzuschließen, wenn ihnen die zum Lebensunterhalt und zur Ausbildung erforderlichen Mittel anderweitig nicht in ausreichendem Umfang zur Verfügung stehen.
Sie soll der Deckung ausbildungsspezifischer Bedarfe dienen.
Ausbildungsspezifische Bedarfe sind alle Aufwendungen für Bildungszwecke, die mittelbar oder unmittelbar den schulischen Kompetenzerwerb fördern.

(2) Die Landesausbildungsförderung dient nicht der Deckung ausbildungsspezifischer Bedarfe, soweit diese im Einzelfall bereits durch Leistungen gemäß

1.  § 6a des Bundeskindergeldgesetzes,
2.  dem Zweiten Buch Sozialgesetzbuch,
3.  dem Zwölften Buch Sozialgesetzbuch oder
4.  § 2 des Asylbewerberleistungsgesetzes

gedeckt werden.

#### § 2  
Anspruchsvoraussetzungen

(1) Die Landesausbildungsförderung wird Schülerinnen und Schülern gewährt, die ihren ständigen Wohnsitz im Land Brandenburg haben und die Voraussetzungen gemäß den Absätzen 2 bis 5 erfüllen.

(2) Landesausbildungsförderung erhält, wer

1.  den Bildungsgang in der gymnasialen Oberstufe oder
    
2.  einen zweijährigen Bildungsgang zum Erwerb der Fachhochschulreife in Vollzeitform
    

an einer Schule in öffentlicher oder freier Trägerschaft besucht und finanziell bedürftig ist.

(3) Finanziell bedürftig im Sinne dieses Gesetzes ist, wer hinsichtlich der maßgeblichen Einkommens- und Vermögensverhältnisse Leistungsbeziehern nach dem Bundesausbildungsförderungsgesetz gleich steht, jedoch aufgrund des § 2 Absatz 1a des Bundesausbildungsförderungsgesetzes keine Bundesausbildungsförderung erhält.
Für die Berechnung des Anspruchs ist der Bedarfssatz gemäß § 12 Absatz 1 Nummer 1 des Bundesausbildungsförderungsgesetzes zu Grunde zu legen.

(4) Personen, die

1.  einen Kinderzuschlag gemäß § 6a des Bundeskindergeldgesetzes,
    
2.  Wohngeld gemäß dem Wohngeldgesetz,
    
3.  Leistungen gemäß § 3 des Asylbewerberleistungsgesetzes,
    
4.  Leistungen gemäß § 2 des Asylbewerberleistungsgesetzes,
    
5.  Arbeitslosengeld II oder Sozialgeld nach Kapitel 3 Abschnitt 2 Unterabschnitte 1 und 2 des Zweiten Buches Sozialgesetzbuch oder
    
6.  Hilfen zum Lebensunterhalt nach dem Dritten Kapitel oder Leistungen der Grundsicherung im Alter und bei Erwerbsminderung nach dem Vierten Kapitel des Zwölften Buches Sozialgesetzbuch
    

erhalten oder bei der Berechnung einer dieser Leistungen berücksichtigt wurden, gelten nach diesem Gesetz als finanziell bedürftig.
Einer Berechnung nach Absatz 3 Satz 2 bedarf es in diesen Fällen nicht.
Die Sätze 1 und 2 gelten entsprechend für Personen, die ausschließlich Leistungen für die in § 28 des Zweiten Buches Sozialgesetzbuch, § 34 des Zwölften Buches Sozialgesetzbuch und § 6a Absatz 2 Satz 1 des Bundeskindergeldgesetzes genannten Bedarfe erhalten.

(5) Ein Anspruch auf Landesausbildungsförderung ist ausgeschlossen, wenn Leistungen gemäß dem Bundesausbildungsförderungsgesetz oder gemäß § 39 des Achten Buches Sozialgesetzbuch gewährt werden.

#### § 3  
Dauer der Förderung

(1) Die Landesausbildungsförderung wird für die Dauer des Schulverhältnisses einschließlich der unterrichtsfreien Zeit geleistet.

(2) Die Landesausbildungsförderung wird vom Beginn des Monats an geleistet, in dem der Eintritt in einen Bildungsgang gemäß § 2 Absatz 2 erfolgt.
Wird der Antrag auf Landesausbildungsförderung nach Eintritt in einen Bildungsgang gemäß § 2 Absatz 2 gestellt, so wird die Landesausbildungsförderung vom Beginn des Monats an gezahlt, in dem der Antrag gestellt wurde.
Die Landesausbildungsförderung endet mit dem Monat, in dem das Schulverhältnis beendet wird.

#### § 4  
Höhe

(1) Die Landesausbildungsförderung wird auf Antrag gewährt und unbar ausgezahlt.

(2) Die Landesausbildungsförderung wird in Höhe von monatlich 125 Euro gewährt.

(3) Die Auszahlung erfolgt in der Regel an die anspruchsberechtigten Schülerinnen und Schüler, bei nicht volljährigen Schülerinnen und Schülern an die einzeln oder gemeinsam Sorgeberechtigten.

#### § 5  
Durchführung des Gesetzes

(1) Zuständig für die Durchführung dieses Gesetzes sind die Landkreise und kreisfreien Städte.
Sie führen die mit der Durchführung dieses Gesetzes verbundenen Aufgaben im Auftrag des Landes unter der Fachaufsicht des für Wissenschaft zuständigen Ministeriums durch.

(2) Das für Wissenschaft zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit den für Bildung, Soziales und Finanzen zuständigen Mitgliedern der Landesregierung das Nähere zur Landesausbildungsförderung, insbesondere zum Verfahren der Antragstellung sowie der Feststellung der Anspruchsberechtigung, durch Rechtsverordnung zu bestimmen.

(3) Die für die Ausführung dieses Gesetzes erforderlichen Kosten trägt das Land.
Das für Wissenschaft zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit den für Bildung und Soziales zuständigen Mitgliedern der Landesregierung den Kostenausgleich gemäß Artikel 97 Absatz 3 der Verfassung des Landes Brandenburg in pauschalierter Form durch Rechtsverordnung zu regeln.

#### § 6  
Übergangsvorschriften

Landesausbildungsförderung wird für Schülerinnen und Schüler gewährt, die ab dem Schuljahr 2010/2011 erstmalig in einen Bildungsgang gemäß § 2 Absatz 2 eintreten.

#### § 7  
Besondere Vorschriften zum Bundesausbildungsförderungsgesetz und zur Anwendung des Sozialgesetzbuches

(1) Soweit dieses Gesetz oder eine aufgrund dieses Gesetzes erlassene Rechtsverordnung nicht etwas anderes bestimmen, sind

1.  § 5 Absatz 1, § 11 Absatz 2, 2a und 4, die §§ 19 bis 30, 47a und 53 des Bundesausbildungsförderungsgesetzes und
    
2.  die §§ 41, 45 Absatz 1, § 47 Absatz 2 bis 6 und § 58 des Bundesausbildungsförderungsgesetzes mit der Maßgabe, dass die Landkreise und kreisfreien Städte die für die Durchführung dieses Gesetzes zuständigen Stellen bestimmen,
    

entsprechend anzuwenden.

(2) Das Erste Buch Sozialgesetzbuch und das Zehnte Buch Sozialgesetzbuch sind entsprechend anzuwenden, soweit dieses Gesetz oder das Bundesausbildungsförderungsgesetz nicht etwas anderes bestimmen.

#### § 8  
Inkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 16.
Juni 2010

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch