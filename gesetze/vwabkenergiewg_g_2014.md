## Gesetz zum Verwaltungsabkommen zwischen der Bundesrepublik Deutschland und dem Land Brandenburg über die Wahrnehmung bestimmter Aufgaben nach dem Energiewirtschaftsgesetz

Der Landtag hat das folgende Gesetz beschlossen:

### § 1

Dem am 27.
November 2013 und am 9.
Dezember 2013 unterzeichneten Verwaltungsabkommen zwischen der Bundesrepublik Deutschland und dem Land Brandenburg über die Wahrnehmung bestimmter Aufgaben nach dem Energiewirtschaftsgesetz wird zugestimmt.
Das Verwaltungsabkommen wird als Anlage zu diesem Gesetz veröffentlicht.

### § 2 

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 14.
März 2014

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Verwaltungsabkommen](/vertraege/vwabkenergiewg_2014)