## Beamtenversorgungsgesetz für das Land Brandenburg (Brandenburgisches Beamtenversorgungsgesetz - BbgBeamtVG)

### Inhaltsübersicht

### Abschnitt 1  
Allgemeine Vorschriften

[§ 1 Geltungsbereich](#1)  
[§ 2 Regelung durch Gesetz](#2)  
[§ 3 Festsetzung, Zuständigkeit](#3)  
[§ 4 Arten der Versorgung](#4)  
[§ 5 Zahlungsweise](#5)  
[§ 6 Abtretung, Verpfändung, Aufrechnung, Zurückbehaltungsrecht](#6)  
[§ 7 Rückforderung von Versorgungsbezügen](#7)  
[§ 8 Verjährung](#8)  
[§ 9 Anzeige- und Mitwirkungspflichten](#9)  
[§ 10 Rundungsvorschriften](#10)  
[§ 11 Verlust der Versorgung](#11)

### Abschnitt 2  
Versorgungsbezüge

#### Unterabschnitt 1  
Ruhegehalt, Unterhaltsbeitrag

[§ 12 Entstehen und Berechnung des Ruhegehalts](#12)  
[§ 13 Ruhegehaltfähige Dienstbezüge](#13)  
[§ 14 Regelmäßige ruhegehaltfähige Dienstzeit](#14)  
[§ 15 Erhöhung der ruhegehaltfähigen Dienstzeit](#15)  
[§ 16 Wehrdienst und vergleichbare Zeiten](#16)  
[§ 17 Zeiten im privatrechtlichen Arbeitsverhältnis im öffentlichen Dienst](#17)  
[§ 18 Sonstige Zeiten](#18)  
[§ 19 Ausbildungszeiten](#19)  
[§ 20 Zeiten in dem in Artikel 3 des Einigungsvertrages genannten Gebiet](#20)  
[§ 21 Wissenschaftliche Qualifikationszeiten](#21)  
[§ 22 Zurechnungszeit und Zeit gesundheitsschädigender Verwendung](#22)  
[§ 23 Allgemeine Bestimmungen zur Berücksichtigung von Dienstzeiten](#23)  
[§ 24 Nicht zu berücksichtigende Zeiten](#24)  
[§ 25 Höhe des Ruhegehalts](#25)  
[§ 26 Vorübergehende Erhöhung des Ruhegehaltssatzes](#26)  
[§ 27 Beamtinnen und Beamte auf Zeit](#27)  
[§ 28 Unterhaltsbeitrag für entlassene Beamtinnen und Beamte](#28)  
[§ 29 Beamtinnen und Beamte auf Probe in leitender Funktion](#29)

#### Unterabschnitt 2  
Hinterbliebenenversorgung

[§ 30 Versorgungsurheberinnen und Versorgungsurheber](#30)  
[§ 31 Arten der Hinterbliebenenversorgung](#31)  
[§ 32 Bezüge für den Sterbemonat](#32)  
[§ 33 Sterbegeld](#33)  
[§ 34 Witwen- und Witwergeld](#34)  
[§ 35 Höhe des Witwen- und Witwergeldes](#35)  
[§ 36 Witwen- und Witwerabfindung](#36)  
[§ 37 Unterhaltsbeitrag für nicht witwen- und witwergeldberechtigte Witwen und Witwer oder hinterbliebene eingetragene Lebenspartnerinnen und Lebenspartner](#37)  
[§ 38 Waisengeld](#38)  
[§ 39 Höhe des Waisengeldes](#39)  
[§ 40 Zusammentreffen von Witwen- und Witwergeld, Waisengeld und Unterhaltsbeiträgen](#40)  
[§ 41 Unterhaltsbeitrag für Hinterbliebene](#41)  
[§ 42 Beginn der Zahlungen](#42)  
[§ 43 Erlöschen der Hinterbliebenenversorgung](#43)

#### Unterabschnitt 3  
Unfallfürsorge

[§ 44 Allgemeines](#44)  
[§ 45 Dienstunfall](#45)  
[§ 45a Meldung von Dienstunfalldaten an EUROSTAT](#45a)  
[§ 46 Einsatzversorgung](#46)  
[§ 47 Meldung und Untersuchungsverfahren](#47)  
[§ 48 Nichtgewährung von Unfallfürsorge](#48)  
[§ 49 Begrenzung der Unfallfürsorgeansprüche](#49)  
[§ 50 Erstattung von Sachschäden und besonderen Aufwendungen](#50)  
[§ 51 Schadensausgleich in besonderen Fällen](#51)  
[§ 52 Heilverfahren](#52)  
[§ 53 Pflegekosten](#53)  
[§ 54 Unfallausgleich](#54)  
[§ 55 Unfallruhegehalt](#55)  
[§ 56 Erhöhtes Unfallruhegehalt](#56)  
[§ 57 Unterhaltsbeitrag für frühere Beamtinnen und Beamte sowie frühere Ruhestandsbeamtinnen und Ruhestandsbeamte](#57)  
[§ 58 Unterhaltsbeitrag bei Schädigung eines ungeborenen Kindes](#58)  
[§ 59 Unfallsterbegeld](#59)  
[§ 60 Unfall-Hinterbliebenenversorgung](#60)  
[§ 61 Unterhaltsbeitrag für Hinterbliebene und für Verwandte der aufsteigenden Linie](#61)  
[§ 62 Höchstgrenzen der Hinterbliebenenversorgung](#62)  
[§ 63 Einmalige Unfallentschädigung](#63)  
[§ 64 Ehrenbeamtinnen und Ehrenbeamte](#64)

#### Unterabschnitt 4  
Übergangsgeld, Ausgleich, Bezüge bei Verschollenheit

[§ 65 Übergangsgeld](#65)  
[§ 66 Übergangsgeld für entlassene politische Beamtinnen und Beamte](#66)  
[§ 67 Ausgleich bei besonderen Altersgrenzen](#67)  
[§ 68 Bezüge bei Verschollenheit](#68)

#### Unterabschnitt 5  
Familienbezogene Leistungen

[§ 69 Familienzuschlag](#69)  
[§ 70 Ausgleichsbetrag](#70)  
[§ 71 Kindererziehungszuschlag](#71)  
[§ 72 Pflegezuschlag](#72)  
[§ 73 Vorübergehende Gewährung von Zuschlägen](#73)

### Abschnitt 3  
Anrechnungs-, Kürzungs- und Ruhensvorschriften

[§ 74 Zusammentreffen von Versorgungsbezügen mit Erwerbs- und Erwerbsersatzeinkommen](#74)  
[§ 75 Zusammentreffen mehrerer Versorgungsbezüge](#75)  
[§ 76 Zusammentreffen von Versorgungsbezügen mit Renten](#76)  
[§ 77 Zusammentreffen von Versorgungsbezügen mit Versorgung aus zwischenstaatlicher und überstaatlicher Verwendung](#77)  
[§ 78 Zusammentreffen von Versorgungsbezügen mit Entschädigung oder Versorgungsbezügen nach dem Beschluss 2005/684/EG, Euratom](#78)  
[§ 79 Reihenfolge der Anwendung der Anrechnungs-, Kürzungs- und Ruhensvorschriften](#79)  
[§ 80 Nichtberücksichtigung der Versorgungsbezüge](#80)  
[§ 81 Kürzung der Versorgungsbezüge wegen Versorgungsausgleich](#81)  
[§ 82 Abwendung der Kürzung der Versorgungsbezüge](#82)

### Abschnitt 4  
Versorgungslastenteilung bei landesinternen Dienstherrenwechseln

[§ 83 Dienstherrenwechsel](#83)

### Abschnitt 5  
Überleitungs- und Übergangsbestimmungen

[§ 84 Überleitung vorhandener Versorgungsempfängerinnen und Versorgungsempfänger](#84)  
[§ 84a Übergangsbestimmung aus Anlass des Wegfalls der Grundgehaltssätze in der Besoldungsgruppe A 4](#84a)  
[§ 85 Übergangsregelung für vorhandene Beamtinnen und Beamte](#85)  
[§ 86 Ausgleichsbetrag für Kommunale Wahlbeamtinnen und Wahlbeamte auf Zeit](#86)  
[§ 87 Regelung zu § 42 Absatz 8 des Brandenburgischen Hochschulgesetzes](#87)  
[§ 88 Übergangsregelungen zur Anhebung des Ruhestandseintrittsalters](#88)

### Abschnitt 6  
Schlussbestimmungen

[§ 89 Ermächtigung zum Erlass von Rechtsverordnungen und Verwaltungsvorschriften](#89)  
[§ 90 Als Landesrecht weitergeltende Rechts- und Verwaltungsvorschriften](#90)

### Abschnitt 1  
Allgemeine Vorschriften

##### § 1  
Geltungsbereich

(1) Dieses Gesetz regelt die Versorgung der Beamtinnen und Beamten des Landes, der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts; ausgenommen sind unbeschadet des § 64 die Ehrenbeamtinnen und Ehrenbeamten und die ehrenamtlichen Richterinnen und Richter.

(2) Dieses Gesetz gilt entsprechend für die Versorgung der Richterinnen und Richter des Landes.

(3) Dieses Gesetz gilt nicht für die öffentlich-rechtlichen Religionsgemeinschaften und ihre Verbände.

##### § 2  
Regelung durch Gesetz

(1) Die Versorgung der Beamtinnen, Beamten und ihrer Hinterbliebenen wird durch Gesetz geregelt.

(2) Zusicherungen, Vereinbarungen und Vergleiche, die der Beamtin oder dem Beamten eine höhere als die gesetzlich zustehende Versorgung verschaffen sollen, sind unwirksam.
Das Gleiche gilt für Versicherungsverträge, die zu diesem Zweck abgeschlossen werden.

(3) Auf die gesetzlich zustehende Versorgung kann weder ganz noch teilweise verzichtet werden.

(4) Wird die Besoldung allgemein angepasst, sind von demselben Zeitpunkt an die Versorgungsbezüge durch Gesetz entsprechend zu regeln.

(5) Als Anpassung gilt auch die Neufassung der Grundgehaltstabelle mit unterschiedlicher Änderung der Grundgehaltssätze und die allgemeine Erhöhung oder Verminderung der Besoldung um feste Beträge.

##### § 3  
Festsetzung, Zuständigkeit

(1) Die Festsetzung und Berechnung der Versorgungsbezüge, die Bestimmung der Zahlungsempfängerin oder des Zahlungsempfängers, die Entscheidung über die Berücksichtigung von Zeiten als ruhegehaltfähige Dienstzeit sowie über die Bewilligung von Versorgungsbezügen aufgrund von Ermessensvorschriften obliegt der für die Festsetzung und Regelung von Versorgungsbezügen bestimmten Stelle (Pensionsbehörde).
Für die Versorgungsberechtigten des Landes wird die Pensionsbehörde durch Rechtsverordnung der Landesregierung bestimmt.
In der Rechtsverordnung kann die Zuständigkeit der Pensionsbehörde für weitere Angelegenheiten der Versorgung und der Nachversicherung bestimmt werden.

(2) Entscheidungen über die Bewilligung von Versorgungsbezügen aufgrund von Ermessensvorschriften dürfen erst beim Eintritt des Versorgungsfalles getroffen werden; vorherige Zusicherungen sind unwirksam.

(3) Ob Zeiten aufgrund der §§ 17 bis 21 als ruhegehaltfähige Dienstzeit zu berücksichtigen sind, soll in der Regel bei der Berufung in das Beamtenverhältnis entschieden werden; diese Entscheidungen stehen unter dem Vorbehalt des Gleichbleibens der Rechtslage, die ihnen zugrunde liegt.

(4) Entscheidungen in versorgungsrechtlichen Angelegenheiten, die eine grundsätzliche, über den Einzelfall hinausgehende Bedeutung haben, sind von dem für das Versorgungsrecht zuständigen Ministerium zu treffen.

##### § 4  
Arten der Versorgung

Versorgungsbezüge sind

1.  Ruhegehalt oder Unterhaltsbeitrag,
2.  Hinterbliebenenversorgung,
3.  Bezüge bei Verschollenheit,
4.  Unfallfürsorge,
5.  Übergangsgeld,
6.  Ausgleich bei besonderen Altersgrenzen,
7.  Familienzuschlag nach § 69 Absatz 2,
8.  Ausgleichsbetrag nach § 70,
9.  Leistungen nach den §§ 71 bis 73.

##### § 5  
Zahlungsweise

(1) Die Versorgungsbezüge sind, soweit nichts anderes bestimmt ist, für die gleichen Zeiträume und zum gleichen Zeitpunkt zu zahlen wie die Dienstbezüge der Beamtinnen und Beamten.

(2) Werden Versorgungsbezüge nach dem Tag der Fälligkeit gezahlt, so besteht kein Anspruch auf Verzugszinsen.

(3) Bei Versorgungsberechtigten, die ihren Wohnsitz oder dauernden Aufenthalt außerhalb der Europäischen Union haben, soll die Pensionsbehörde die Zahlung der Versorgungsbezüge von der Vorlage einer Lebensbescheinigung abhängig machen.
Des Weiteren kann sie die Bestellung einer Empfangsbevollmächtigten oder eines Empfangsbevollmächtigten im Geltungsbereich des Beamtenstatusgesetzes verlangen.

(4) Für die Zahlung der Versorgungsbezüge haben Versorgungsberechtigte auf Verlangen der Pensionsbehörde ein Konto anzugeben oder einzurichten, auf das die Überweisung erfolgen kann.
Die Kontoeinrichtungs-, Kontoführungs- und Buchungsgebühren tragen die Versorgungsberechtigten.
Die Übermittlungskosten mit Ausnahme der Kosten für die Gutschrift auf dem Konto der Versorgungsberechtigten trägt die Pensionsbehörde.
Bei einer Überweisung der Versorgungsbezüge auf ein außerhalb der Europäischen Union geführtes Konto tragen die Versorgungsberechtigten die Kosten und die Gefahr der Übermittlung der Versorgungsbezüge sowie die Kosten einer Meldung nach § 67 der Außenwirtschaftsverordnung.
Eine Auszahlung auf andere Weise kann nur zugestanden werden, wenn den Versorgungsberechtigten die Einrichtung oder Benutzung eines Kontos aus wichtigem Grund nicht zugemutet werden kann.

##### § 6  
Abtretung, Verpfändung, Aufrechnung, Zurückbehaltungsrecht

(1) Ansprüche auf Versorgungsbezüge können nur abgetreten oder verpfändet werden, soweit sie der Pfändung unterliegen.
Gegenüber Ansprüchen auf Versorgungsbezüge kann der Dienstherr ein Aufrechnungs- oder Zurückbehaltungsrecht nur in Höhe des pfändbaren Teils der Versorgungsbezüge geltend machen.

(2) Ansprüche auf Sterbegeld (§ 33), auf Schadensausgleich (§ 51), auf Erstattung der Kosten des Heilverfahrens (§ 52) und der Pflege (§ 53), auf Unfallausgleich (§ 54), auf Unfallsterbegeld (§ 59) sowie auf einmalige Unfallentschädigung (§ 63) können nicht gepfändet, abgetreten oder verpfändet werden.
Forderungen des Dienstherrn gegen die Verstorbene oder den Verstorbenen aus Vorschuss- oder Darlehensgewährungen sowie aus Überzahlungen von Dienst- oder Versorgungsbezügen können auf das Sterbegeld angerechnet werden.

##### § 7  
Rückforderung von Versorgungsbezügen

(1) Werden Versorgungsberechtigte durch eine gesetzliche Änderung ihrer Versorgungsbezüge mit rückwirkender Kraft schlechter gestellt, so sind die Unterschiedsbeträge nicht zu erstatten.

(2) Im Übrigen regelt sich die Rückforderung zu viel gezahlter Versorgungsbezüge nach den Vorschriften des Bürgerlichen Gesetzbuchs über die Herausgabe einer ungerechtfertigten Bereicherung, soweit gesetzlich nichts anderes bestimmt ist.
Der Kenntnis des Mangels des rechtlichen Grundes der Zahlung steht es gleich, wenn der Mangel so offensichtlich war, dass die Empfängerin oder der Empfänger ihn hätte erkennen müssen.
Von der Rückforderung kann aus Billigkeitsgründen mit Zustimmung der obersten Dienstbehörde oder der von ihr bestimmten Stelle ganz oder teilweise abgesehen werden.

(3) Geldleistungen, die für die Zeit nach dem Tod von Versorgungsberechtigten auf ein Konto bei einem Geldinstitut überwiesen wurden, gelten als unter Vorbehalt erbracht.
Das Geldinstitut hat sie der überweisenden Stelle zurückzuüberweisen, wenn diese sie als zu Unrecht erbracht zurückfordert.
Eine Verpflichtung zur Rücküberweisung besteht nicht, soweit über den entsprechenden Betrag bei Eingang der Rückforderung bereits anderweitig verfügt wurde, es sei denn, dass die Rücküberweisung aus einem Guthaben erfolgen kann.
Das Geldinstitut darf den überwiesenen Betrag nicht zur Befriedigung eigener Forderungen verwenden.

(4) Soweit Geldleistungen für die Zeit nach dem Tod von Versorgungsberechtigten zu Unrecht erbracht worden sind, haben die Personen, die die Geldleistung in Empfang genommen oder über den entsprechenden Betrag verfügt haben, diesen Betrag der überweisenden Stelle zu erstatten, sofern er nicht nach Absatz 3 von dem Geldinstitut zurücküberwiesen wird.
Ein Geldinstitut, das eine Rücküberweisung mit dem Hinweis abgelehnt hat, dass über den entsprechenden Betrag bereits anderweitig verfügt wurde, hat der überweisenden Stelle auf Verlangen Namen und Anschrift der Personen, die über den Betrag verfügt haben, sowie etwaiger neuer Kontoinhaberinnen oder Kontoinhaber zu benennen.
Ein Anspruch gegen die Erbinnen und Erben bleibt unberührt.

##### § 8  
Verjährung

Ansprüche nach diesem Gesetz und nach Verordnungen, die auf der Grundlage dieses Gesetzes ergangen sind, und Ansprüche auf Rückzahlung von zu viel gezahlten Versorgungsbezügen verjähren in drei Jahren; Ansprüche auf Rückzahlung von Versorgungsbezügen verjähren in zehn Jahren, wenn durch vorsätzlich oder leichtfertig unrichtige oder unvollständige Angaben oder das vorsätzliche oder leichtfertige pflichtwidrige Unterlassen von Angaben die Gewährung oder Belassung von Versorgungsbezügen bewirkt wurde.
Die Verjährung beginnt mit dem Schluss des Jahres, in dem der Anspruch entstanden ist.
Im Übrigen sind die §§ 194 bis 218 des Bürgerlichen Gesetzbuchs entsprechend anzuwenden.

##### § 9  
Anzeige- und Mitwirkungspflichten

(1) Die personalaktenführende Stelle hat der Pensionsbehörde jede Verwendung von Versorgungsberechtigten unter Angabe der gewährten Bezüge, ebenso jede spätere Änderung der Bezüge oder die Zahlungseinstellung sowie die Gewährung einer Versorgung unverzüglich anzuzeigen.

(2) Die Versorgungsberechtigten haben der Pensionsbehörde unverzüglich

1.  alle Tatsachen anzugeben, die für die Versorgung erheblich sind, und auf Verlangen der Pensionsbehörde der Erteilung der erforderlichen Auskünfte durch Dritte zuzustimmen,
2.  den Familienstand anzugeben, der für die Berechnung der Versorgungsausgaben im Zusammenhang mit der mittelfristigen Finanzplanung erforderlich ist; das Grundrecht auf Datenschutz (Artikel 11 Absatz 1 der Verfassung des Landes Brandenburg) wird insoweit eingeschränkt,
3.  Änderungen in den persönlichen Verhältnissen, die für die Versorgung erheblich sind oder über die im Zusammenhang mit der Versorgung Erklärungen abgegeben worden sind, mitzuteilen,
4.  Beweismittel zu bezeichnen und auf Verlangen der Pensionsbehörde Beweisurkunden vorzulegen oder ihrer Vorlage zuzustimmen.

Satz 1 gilt entsprechend für Personen, die Versorgungsleistungen zu erstatten haben.
Die Pensionsbehörde kann Erkenntnisse und Beweismittel an Sachverständige weitergeben, soweit dies zur Entscheidung über die Versorgung notwendig ist.

(3) Auf Verlangen der obersten Dienstbehörde oder der von ihr bestimmten Stelle oder der Pensionsbehörde haben sich die Versorgungsberechtigten von einer von ihnen bestimmten Person ärztlich oder psychologisch untersuchen oder beobachten zu lassen und die erforderlichen Auskünfte zu erteilen, soweit dies zur Entscheidung über die Gewährung von Versorgungsbezügen (§ 4) erforderlich ist.
Die oberste Dienstbehörde oder die von ihr bestimmte Stelle oder die Pensionsbehörde darf Erkenntnisse und Beweismittel, die für die Begutachtung erforderlich sind, an die mit der Begutachtung beauftragte Person übermitteln.

(4) Kommen Versorgungsberechtigte den in den Absätzen 2 und 3 oder in anderen Bestimmungen dieses Gesetzes genannten Mitwirkungspflichten schuldhaft nicht nach, so kann ihnen die Versorgung ganz oder teilweise auf Zeit oder auf Dauer entzogen werden.
Beim Vorliegen besonderer Verhältnisse kann die Versorgung ganz oder teilweise wieder zuerkannt werden.
Die Entscheidung trifft die Pensionsbehörde.

##### § 10  
Rundungsvorschriften

(1) Bei der Berechnung von Versorgungsbezügen (§ 4) sind die sich ergebenden Bruchteile eines Cents unter 0,5 abzurunden und Bruchteile von 0,5 und mehr aufzurunden.
Zwischenrechnungen werden jeweils auf zwei Dezimalstellen durchgeführt.
Jeder Versorgungsbestandteil ist einzeln zu runden.

(2) Zur Ermittlung der gesamten ruhegehaltfähigen Dienstjahre sind etwa anfallende Tage unter Benutzung des Nenners 365 umzurechnen; Absatz 1 gilt entsprechend.

##### § 11  
Verlust der Versorgung

(1) Eine Ruhestandsbeamtin oder ein Ruhestandsbeamter verliert mit der Rechtskraft der Entscheidung die Rechte als Ruhestandsbeamtin oder als Ruhestandsbeamter, wenn

1.  sie oder er wegen einer vorsätzlichen Tat zu einer Freiheitsstrafe von mindestens zwei Jahren oder wegen einer vorsätzlichen Tat, die nach den Vorschriften über Friedensverrat, Hochverrat, Gefährdung des demokratischen Rechtsstaates oder Landesverrat und Gefährdung der äußeren Sicherheit strafbar ist, zu einer Freiheitsstrafe von mindestens sechs Monaten verurteilt worden ist oder
2.  aufgrund einer Entscheidung des Bundesverfassungsgerichts gemäß Artikel 18 des Grundgesetzes ein Grundrecht verwirkt wurde oder wegen einer vor Beendigung des Beamtenverhältnisses begangenen Tat eine Entscheidung ergeht, die nach § 24 des Beamtenstatusgesetzes zum Verlust der Beamtenrechte führt.

(2) Die §§ 35 und 36 des Landesbeamtengesetzes sind entsprechend anzuwenden.

(3) Die Absätze 1 und 2 gelten für Hinterbliebene entsprechend.

(4) Kommt eine Ruhestandsbeamtin oder ein Ruhestandsbeamter den Verpflichtungen aus § 29 Absatz 2, 4 und 5, § 30 Absatz 3 Satz 1 des Beamtenstatusgesetzes schuldhaft nicht nach, obwohl auf die Folgen eines solchen Verhaltens schriftlich hingewiesen worden ist, so verliert sie oder er für diese Zeit die Versorgungsbezüge.
Die oberste Dienstbehörde oder die von ihr bestimmte Stelle stellt den Verlust der Versorgungsbezüge fest.
Eine disziplinarrechtliche Verfolgung wird dadurch nicht ausgeschlossen.

(5) Die oberste Dienstbehörde oder die von ihr bestimmte Stelle kann anordnen, dass Empfängerinnen und Empfänger von Hinterbliebenenversorgung die Versorgungsbezüge auf Zeit oder auf Dauer teilweise oder ganz verlieren, wenn sie sich gegen die freiheitliche demokratische Grundordnung im Sinne des Grundgesetzes betätigt haben.
Der Sachverhalt ist in entsprechender Anwendung der Vorschriften des Landesdisziplinargesetzes über die Durchführung des Disziplinarverfahrens zu ermitteln.
Absatz 1 bleibt unberührt.

### Abschnitt 2  
Versorgungsbezüge

#### Unterabschnitt 1  
Ruhegehalt, Unterhaltsbeitrag

##### § 12  
Entstehen und Berechnung des Ruhegehalts

(1) Ein Ruhegehalt wird nur gewährt, wenn die Beamtin oder der Beamte

1.  eine Dienstzeit von mindestens fünf Jahren abgeleistet hat oder
2.  infolge Krankheit, Verwundung oder sonstiger Beschädigung, die sie oder er sich ohne grobes Verschulden bei Ausübung oder aus Veranlassung des Dienstes zugezogen hat, dienstunfähig geworden ist.

Die Dienstzeit wird ab dem Zeitpunkt der ersten Berufung in das Beamtenverhältnis gerechnet und nur berücksichtigt, sofern sie ruhegehaltfähig ist; § 23 Absatz 1 ist insoweit nicht anzuwenden.
Zeiten, die kraft gesetzlicher Vorschrift als ruhegehaltfähig gelten oder nach § 17 als ruhegehaltfähige Dienstzeit berücksichtigt werden, sind einzurechnen; Satz 2 zweiter Halbsatz gilt entsprechend.
Satz 3 gilt nicht für Zeiten, die die Beamtin oder der Beamte vor dem 3.
Oktober 1990 in dem in Artikel 3 des Einigungsvertrages genannten Gebiet zurückgelegt hat.

(2) Der Anspruch auf Ruhegehalt entsteht mit dem Beginn des Ruhestands, in den Fällen des § 4 des Brandenburgischen Besoldungsgesetzes nach Ablauf der Zeit, für die Dienstbezüge gewährt werden.

(3) Das Ruhegehalt wird auf der Grundlage der ruhegehaltfähigen Dienstbezüge und der ruhegehaltfähigen Dienstzeit berechnet.

##### § 13  
Ruhegehaltfähige Dienstbezüge

(1) Ruhegehaltfähige Dienstbezüge sind

1.  das Grundgehalt,
2.  sonstige Dienstbezüge, die nach dem Besoldungsrecht ruhegehaltfähig sind,
3.  Leistungsbezüge nach den §§ 31 bis 33 des Brandenburgischen Besoldungsgesetzes, soweit sie nach § 35 des Brandenburgischen Besoldungsgesetzes ruhegehaltfähig sind,

die der Beamtin oder dem Beamten in den Fällen der Nummern 1 und 2 zuletzt zugestanden haben.
Bei hauptberuflichen Hochschulleiterinnen und Hochschulleitern und hauptberuflichen Vizepräsidentinnen und Vizepräsidenten, die vor dem Wechsel in dieses Amt ein anderes Amt bekleidet haben, sind die ruhegehaltfähigen Dienstbezüge aus dem vorherigen Amt ruhegehaltfähig, wenn es für sie günstiger ist.
Bei Teilzeitbeschäftigung und Beurlaubung ohne Dienstbezüge (Freistellung) gelten als ruhegehaltfähige Dienstbezüge die dem letzten Amt entsprechenden vollen ruhegehaltfähigen Dienstbezüge.
Satz 3 gilt entsprechend bei eingeschränkter Verwendung einer Beamtin oder eines Beamten wegen begrenzter Dienstfähigkeit nach § 27 des Beamtenstatusgesetzes.

(2) Ist die Beamtin oder der Beamte wegen Dienstunfähigkeit aufgrund eines Dienstunfalls im Sinne des § 45 in den Ruhestand getreten, so ist das Grundgehalt der nach Absatz 1 Satz 1 Nummer 1, Absatz 3 oder Absatz 5 maßgebenden Besoldungsgruppe nach der Stufe zugrunde zu legen, die bis zum Eintritt in den Ruhestand wegen Erreichens der Altersgrenze hätte erreicht werden können.

(3) Ist eine Beamtin oder ein Beamter aus einem Amt in den Ruhestand getreten, das nicht dem Eingangsamt der Laufbahn oder das keiner Laufbahn angehört, und hat sie oder er die Dienstbezüge dieses oder eines mindestens gleichwertigen Amtes vor dem Eintritt in den Ruhestand nicht mindestens zwei Jahre erhalten, so sind ruhegehaltfähig nur die Bezüge des vorher bekleideten Amtes.
Hat die Beamtin oder der Beamte vorher ein Amt nicht bekleidet, so setzt die oberste Dienstbehörde im Einvernehmen mit dem für das Beamtenversorgungsrecht zuständigen Ministerium die ruhegehaltfähigen Dienstbezüge bis zur Höhe der ruhegehaltfähigen Dienstbezüge der nächstniedrigeren Besoldungsgruppe fest.
In die Zweijahresfrist einzurechnen ist die innerhalb dieser Frist liegende Zeit einer Beurlaubung ohne Dienstbezüge, soweit sie als ruhegehaltfähig berücksichtigt worden ist.

(4) Absatz 3 gilt nicht, wenn die Beamtin oder der Beamte vor Ablauf der Frist infolge von Krankheit, Verwundung oder sonstiger Beschädigung, die sie oder er sich ohne grobes Verschulden bei Ausübung oder aus Veranlassung des Dienstes zugezogen hat, in den Ruhestand getreten oder verstorben ist.

(5) Das Ruhegehalt einer Beamtin oder eines Beamten, die oder der früher ein mit höheren Dienstbezügen verbundenes Amt bekleidet und diese Bezüge mindestens zwei Jahre erhalten hat, wird, sofern sie oder er in ein mit geringeren Dienstbezügen verbundenes Amt nicht lediglich auf ihren oder seinen im eigenen Interesse gestellten Antrag übergetreten ist, nach den höheren ruhegehaltfähigen Dienstbezügen des früheren Amtes und der gesamten ruhegehaltfähigen Dienstzeit berechnet.
Absatz 3 Satz 3 und Absatz 4 gelten entsprechend.
Das Ruhegehalt darf jedoch die ruhegehaltfähigen Dienstbezüge des letzten Amtes nicht übersteigen.

(6) Verringern sich bei einem Wechsel in ein Amt der Besoldungsordnung W die ruhegehaltfähigen Dienstbezüge, so berechnet sich das Ruhegehalt aus den ruhegehaltfähigen Dienstbezügen des früheren Amtes und der gesamten ruhegehaltfähigen Dienstzeit, sofern die Beamtin oder der Beamte die Dienstbezüge des früheren Amtes mindestens zwei Jahre erhalten hat.
Ruhegehaltfähig ist die zum Zeitpunkt des Wechsels erreichte Stufe des Grundgehalts.
Auf die Zweijahresfrist wird der Zeitraum, in dem die Beamtin oder der Beamte Dienstbezüge aus einem Amt der Besoldungsordnung W erhalten hat, angerechnet.
Absatz 3 Satz 3 und Absatz 4 gelten entsprechend.

##### § 14  
Regelmäßige ruhegehaltfähige Dienstzeit

(1) Ruhegehaltfähig ist die Dienstzeit, die die Beamtin oder der Beamte von dem Tag der ersten Berufung in das Beamtenverhältnis an im Dienst eines öffentlich-rechtlichen Dienstherrn im Beamtenverhältnis zurückgelegt hat.
Dies gilt nicht für die Zeit

1.  in einem Amt, das die Arbeitskraft der Beamtin oder des Beamten nur nebenbei beansprucht,
2.  einer ehrenamtlichen Tätigkeit,
3.  einer Beurlaubung unter Wegfall der Besoldung; die Zeit einer Beurlaubung unter Wegfall der Besoldung kann berücksichtigt werden, wenn ein Versorgungszuschlag in Höhe von 30 Prozent der ohne die Beurlaubung jeweils zustehenden ruhegehaltfähigen Dienstbezüge gezahlt wurde und wenn spätestens bei Beendigung des Urlaubs schriftlich zugestanden worden ist, dass dieser öffentlichen Belangen oder dienstlichen Interessen dient; das für das Beamtenversorgungsrecht zuständige Ministerium kann Ausnahmen zulassen,
4.  eines schuldhaften Fernbleibens vom Dienst unter Verlust der Besoldung.

(2) Nicht ruhegehaltfähig sind Dienstzeiten

1.  in einem Beamtenverhältnis, das durch Verlust der Beamtenrechte (§ 24 Absatz 1 des Beamtenstatusgesetzes) oder durch Disziplinarurteil beendet worden ist,
2.  in einem Beamtenverhältnis auf Probe oder auf Widerruf, das durch Entlassung wegen einer Handlung beendet worden ist, die im Beamtenverhältnis auf Lebenszeit mindestens eine Kürzung der Dienstbezüge zur Folge hätte,
3.  in einem Beamtenverhältnis, das durch Entlassung auf Antrag der Beamtin oder des Beamten beendet worden ist,
    1.  wenn ihr oder ihm ein Verfahren mit der Folge des Verlustes der Beamtenrechte oder der Entfernung aus dem Dienst drohte oder
    2.  wenn die Beamtin oder der Beamte den Antrag gestellt hat, um einer drohenden Entlassung nach Nummer 2 zuvorzukommen.

Das für das Versorgungsrecht zuständige Ministerium kann Ausnahmen zulassen.

(3) Der im Beamtenverhältnis zurückgelegten Dienstzeit stehen gleich

1.  die im Richterverhältnis zurückgelegte Dienstzeit,
2.  die Zeit als Mitglied der Bundesregierung oder einer Landesregierung, sofern diese Zeit nicht zu einem eigenständigen dauerhaften Versorgungsanspruch geführt hat,
3.  die Zeit als Parlamentarische Staatssekretärin oder als Parlamentarischer Staatssekretär bei einem Mitglied der Bundesregierung oder einer Landesregierung, sofern diese Zeit nicht zu einem eigenständigen dauerhaften Versorgungsanspruch geführt hat,
4.  die im öffentlichen Dienst einer zwischenstaatlichen oder überstaatlichen Einrichtung zurückgelegte Dienstzeit,
5.  die in einem Rechtsverhältnis als dienstordnungsmäßig Angestellte oder dienstordnungsmäßig Angestellter zurückgelegte Dienstzeit.

##### § 15  
Erhöhung der ruhegehaltfähigen Dienstzeit

Die ruhegehaltfähige Dienstzeit nach § 14 erhöht sich um die Zeit, die eine Ruhestandsbeamtin oder ein Ruhestandsbeamter

1.  in einer die Arbeitskraft voll beanspruchenden entgeltlichen Beschäftigung als Beamtin, Beamter, Richterin, Richter, Berufssoldatin, Berufssoldat, dienstordnungsmäßig Angestellte, dienstordnungsmäßig Angestellter oder in einem Amtsverhältnis im Sinne des § 14 Absatz 3 Nummer 2 und 3 zurückgelegt hat, ohne einen neuen Versorgungsanspruch zu erlangen,
2.  in einer Tätigkeit im Sinne des § 14 Absatz 3 Nummer 4 zurückgelegt hat.

§ 14 Absatz 1 Satz 2 Nummer 3 und 4 und Absatz 2 gilt entsprechend.

##### § 16  
Wehrdienst und vergleichbare Zeiten

(1) Als ruhegehaltfähig gilt die Zeit, in der eine Beamtin oder ein Beamter vor der Berufung in das Beamtenverhältnis

1.  berufsmäßig im Dienst der Bundeswehr, der Nationalen Volksarmee der Deutschen Demokratischen Republik oder im Vollzugsdienst der Polizei gestanden hat,
2.  nichtberufsmäßigen Wehrdienst oder Polizeivollzugsdienst geleistet hat,
3.  den freiwilligen Wehrdienst nach § 58b des Soldatengesetzes geleistet hat,
4.  nichtberufsmäßigen Wehrdienst als Staatsangehörige oder als Staatsangehöriger eines Mitgliedstaates der Europäischen Gemeinschaft in ihrem oder seinem Heimatland geleistet hat, wenn zum Zeitpunkt des Ableistens des nichtberufsmäßigen Wehrdienstes das Heimatland der Beamtin oder des Beamten bereits Mitgliedstaat der Europäischen Gemeinschaft war,
5.  sich aufgrund einer Krankheit oder Verwundung als Folge eines Dienstes nach den Nummern 1 bis 3 im Anschluss an die Entlassung arbeitsunfähig in einer Heilbehandlung befunden hat.

(2) Dem nichtberufsmäßigen Wehrdienst stehen gleich

1.  der Zivildienst (§ 78 Absatz 2 des Zivildienstgesetzes),
2.  der Bundesfreiwilligendienst nach dem Bundesfreiwilligendienstgesetz,
3.  der Wehrersatzdienst als Bausoldat der Deutschen Demokratischen Republik gemäß der Anordnung des Nationalen Verteidigungsrates der Deutschen Demokratischen Republik über die Aufstellung von Baueinheiten im Bereich des Ministeriums für Nationale Verteidigung vom 7.
    September 1964 (GBI.
    I Nr. 11 S. 129) in der Zeit bis zum 28.
    Februar 1990,
4.  der Zivildienst aufgrund der Verordnung über den Zivildienst in der Deutschen Demokratischen Republik vom 20.
    Februar 1990 (GBI.
    I Nr.
    10 S.
    79) in der Zeit vom 1.
    März 1990 bis 2. Oktober 1990.

(3) § 14 Absatz 1 Satz 2 Nummer 1, 3 und 4 und Absatz 2 gilt entsprechend.

##### § 17  
Zeiten im privatrechtlichen Arbeitsverhältnis im öffentlichen Dienst

Als ruhegehaltfähig sollen auch folgende Zeiten bis zu fünf Jahren berücksichtigt werden, in denen eine Beamtin oder ein Beamter im privatrechtlichen Arbeitsverhältnis im Dienst eines öffentlich-rechtlichen Dienstherrn ohne von der Beamtin oder dem Beamten zu vertretende Unterbrechung tätig war, sofern diese Tätigkeit zur Ernennung geführt hat:

1.  Zeiten einer hauptberuflichen in der Regel einer Beamtin oder einem Beamten obliegenden oder später einer Beamtin oder einem Beamten übertragenen entgeltlichen Beschäftigung oder
2.  Zeiten einer für die Laufbahn der Beamtin oder des Beamten förderlichen Tätigkeit.

Das gilt auch für eine Tätigkeit im Dienst von Einrichtungen, die von mehreren öffentlich-rechtlichen Dienstherren durch Staatsvertrag oder Verwaltungsabkommen zur Erfüllung oder Koordinierung ihnen obliegender hoheitsrechtlicher Aufgaben geschaffen worden sind.
Zeiten mit einer geringeren als der regelmäßigen Arbeitszeit dürfen nur zu dem Teil als ruhegehaltfähig berücksichtigt werden, der dem Verhältnis der tatsächlichen zur regelmäßigen Arbeitszeit entspricht.

##### § 18  
Sonstige Zeiten

(1) Die Zeit, während der eine Beamtin oder ein Beamter vor der Berufung in das Beamtenverhältnis

1.  hauptberuflich
    
    1.  im Dienst öffentlich-rechtlicher Religionsgemeinschaften oder ihrer Verbände (Artikel 140 des Grundgesetzes) oder
    2.  im öffentlichen oder nichtöffentlichen Schuldienst oder
    3.  im Dienst der Fraktionen des Bundestages oder der Landtage oder kommunaler Vertretungskörperschaften oder
    4.  im Dienst von kommunalen Spitzenverbänden oder ihren Landesverbänden sowie von Spitzenverbänden der Sozialversicherung oder ihren Landesverbänden oder
    5.  im ausländischen öffentlichen Dienst
    
    tätig gewesen ist oder
    
2.  auf wissenschaftlichem, künstlerischem, technischem oder wirtschaftlichem Gebiet besondere Fachkenntnisse erworben hat, die die notwendige Voraussetzung für die Wahrnehmung ihres oder seines Amtes bilden, oder
3.  als Entwicklungshelferin oder als Entwicklungshelfer im Sinne des Entwicklungshelfer-Gesetzes tätig gewesen ist,

kann bis zu fünf Jahren als ruhegehaltfähige Dienstzeit berücksichtigt werden.

(2) In den Fällen des Absatzes 1 Nummer 1 werden nur Zeiten berücksichtigt, sofern ein innerer Zusammenhang zwischen dieser Tätigkeit und dem ersten im Beamten- oder Richterverhältnis übertragenen Amt besteht.

##### § 19  
Ausbildungszeiten

(1) Die Zeit

1.  der vorgeschriebenen Ausbildung (Fachschul-, Hochschul- und praktische Ausbildung, Vorbereitungsdienst, übliche Prüfungszeit),
2.  einer praktischen hauptberuflichen Tätigkeit, die für die Übernahme in das Beamtenverhältnis vorgeschrieben ist,

soll als ruhegehaltfähige Dienstzeit berücksichtigt werden.
Die Zeit einer Fachschulausbildung einschließlich der Prüfungszeit soll bis zu 1 095 Tagen und die Zeit einer Hochschulausbildung einschließlich der Prüfungszeit bis zu 855 Tagen, insgesamt höchstens bis zu 1 095 Tagen berücksichtigt werden.

(2) Für Beamtinnen und Beamte des Vollzugsdienstes und des feuerwehrtechnischen Dienstes können Zeiten einer praktischen Ausbildung und einer praktischen hauptberuflichen Tätigkeit anstelle einer Berücksichtigung nach Absatz 1 bis zu einer Gesamtzeit von fünf Jahren als ruhegehaltfähige Dienstzeit berücksichtigt werden, wenn sie für die Wahrnehmung des Amtes förderlich sind.

(3) Die allgemeine Schulbildung zählt nicht zur vorgeschriebenen Ausbildung, auch dann nicht, wenn sie durch eine andere Art der Ausbildung ersetzt wurde.

(4) Bei anderen Bewerberinnen oder anderen Bewerbern (§ 16 des Landesbeamtengesetzes) können Zeiten nach Absatz 1 als ruhegehaltfähig berücksichtigt werden, soweit sie für Laufbahnbewerberinnen oder Laufbahnbewerber vorgeschrieben sind.
Ist eine Laufbahn der Fachrichtung des ausgeübten Amtes bei einem Dienstherrn noch nicht gestaltet, so gilt das Gleiche für solche Zeiten, die bei Gestaltung der Laufbahn mindestens vorgeschrieben werden.

##### § 20  
Zeiten in dem in Artikel 3 des Einigungsvertrages genannten Gebiet

(1) Wehrdienstzeiten und vergleichbare Zeiten nach § 16, Beschäftigungszeiten nach § 17 und sonstige Zeiten nach den §§ 18 und 21, die die Beamtin oder der Beamte vor dem 3.
Oktober 1990 in dem in Artikel 3 des Einigungsvertrages genannten Gebiet zurückgelegt hat, werden nicht als ruhegehaltfähige Dienstzeit berücksichtigt, wenn die allgemeine Wartezeit für die gesetzliche Rentenversicherung erfüllt ist und diese Zeiten als rentenrechtliche Zeiten berücksichtigungsfähig sind.
Ausbildungszeiten im Sinne des § 19 sind nicht ruhegehaltfähig, soweit die allgemeine Wartezeit für die gesetzliche Rentenversicherung erfüllt ist.
Rentenrechtliche Zeiten sind auch solche im Sinne des Artikels 2 des Renten-Überleitungsgesetzes.

(2) Wenn die allgemeine Wartezeit für die gesetzliche Rentenversicherung nicht erfüllt ist, können die in Absatz 1 genannten Zeiten im Rahmen der dort genannten Vorschriften insgesamt höchstens bis zu fünf Jahren als ruhegehaltfähig berücksichtigt werden.

##### § 21  
Wissenschaftliche Qualifikationszeiten

(1) Für die Versorgung der Professorinnen und Professoren sowie des wissenschaftlichen Personals an Hochschulen im Beamtenverhältnis mit Bezügen nach der Bundesbesoldungsordnung C gemäß § 77 Absatz 2 und 3 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung und ihrer Hinterbliebenen gelten die Vorschriften dieses Gesetzes, soweit nachfolgend nichts anderes bestimmt ist.
Satz 1 gilt auch für die Versorgung der Professorinnen und Professoren, der hauptberuflichen Leiterinnen und Leiter sowie Mitglieder von Leitungsgremien an Hochschulen im Beamtenverhältnis mit Bezügen nach der Besoldungsordnung W des Brandenburgischen Besoldungsgesetzes und ihrer Hinterbliebenen.

(2) Ruhegehaltfähig ist auch die Zeit, in der Beamtinnen und Beamte im Sinne des Absatzes 1 nach der Habilitation oder einer Juniorprofessur dem Lehrkörper einer Hochschule angehört haben.
Als ruhegehaltfähig gilt auch die zur Vorbereitung auf die Promotion benötigte Zeit bis zu zwei Jahren.
Die in einer Habilitationsordnung vorgeschriebene Mindestzeit für die Erbringung der Habilitationsleistungen oder sonstiger gleichwertiger wissenschaftlicher Leistungen kann als ruhegehaltfähige Dienstzeit berücksichtigt werden; soweit die Habilitationsordnung eine Mindestdauer nicht vorschreibt, sind bis zu drei Jahren berücksichtigungsfähig.
Die nach erfolgreichem Abschluss eines Hochschulstudiums vor der Ernennung zur Professorin, zum Professor, zur Hochschuldozentin, zum Hochschuldozenten, zur Oberassistentin, zum Oberassistenten, zur Oberingenieurin, zum Oberingenieur, zur Wissenschaftlichen oder Künstlerischen Assistentin, zum Wissenschaftlichen oder Künstlerischen Assistenten liegende Zeit einer hauptberuflichen Tätigkeit, in der besondere Fachkenntnisse erworben wurden, die für die Wahrnehmung des Amtes förderlich sind, soll in den Fällen des § 39 Absatz 1 Nummer 4 Buchstabe b des Brandenburgischen Hochschulgesetzes als ruhegehaltfähig berücksichtigt werden; im Übrigen kann sie bis zu fünf Jahren in vollem Umfang, darüber hinaus bis zur Hälfte als ruhegehaltfähig berücksichtigt werden.
Zeiten nach Satz 4 können in der Regel insgesamt nicht über zehn Jahre hinaus als ruhegehaltfähig berücksichtigt werden.
Zeiten mit einer geringeren als der regelmäßigen Arbeitszeit dürfen nur zu dem Teil als ruhegehaltfähig berücksichtigt werden, der dem Verhältnis der tatsächlichen zur regelmäßigen Arbeitszeit entspricht.

##### § 22  
Zurechnungszeit und Zeit gesundheitsschädigender Verwendung

(1) Ist die Beamtin oder der Beamte vor Vollendung des 60.
Lebensjahres wegen Dienstunfähigkeit in den Ruhestand versetzt worden, wird die Zeit von der Versetzung in den Ruhestand bis zum Ablauf des Monats der Vollendung des 60.
Lebensjahres für die Berechnung des Ruhegehalts der ruhegehaltfähigen Dienstzeit zu zwei Dritteln hinzugerechnet, soweit sie nicht nach anderen Vorschriften als ruhegehaltfähig berücksichtigt wird (Zurechnungszeit).
Ist die Beamtin oder der Beamte nach § 29 des Beamtenstatusgesetzes erneut in das Beamtenverhältnis berufen worden, so wird eine der Berechnung des früheren Ruhegehalts zugrunde gelegene Zurechnungszeit insoweit berücksichtigt, als die Zahl der dem neuen Ruhegehalt zugrunde liegenden Dienstjahre hinter der Zahl der dem früheren Ruhegehalt zugrunde gelegenen Dienstjahre zurückbleibt.

(2) Die Zeit der Verwendung einer Beamtin oder eines Beamten in Ländern, in denen sie oder er gesundheitsschädigenden klimatischen Einflüssen ausgesetzt war, kann bis zum Doppelten als ruhegehaltfähige Dienstzeit berücksichtigt werden, wenn sie ununterbrochen mindestens ein Jahr gedauert hat.
Entsprechendes gilt für Beurlaubungen, wenn für die Beurlaubungszeit ein Versorgungszuschlag in Höhe von 30 Prozent der ohne die Beurlaubung jeweils zustehenden ruhegehaltfähigen Dienstbezüge gezahlt wird und die Tätigkeit öffentlichen Belangen oder dienstlichen Interessen diente und dies spätestens bei Beendigung des Urlaubs anerkannt worden ist; das für das Beamtenversorgungsrecht zuständige Ministerium kann Ausnahmen zulassen.

(3) Sind sowohl die Voraussetzungen des Absatzes 1 als auch die des Absatzes 2 erfüllt, findet die günstigere Vorschrift Anwendung.

(4) Zeiten einer besonderen Verwendung im Ausland nach § 46 Absatz 1 Satz 2 können bis zum Doppelten als ruhegehaltfähige Dienstzeit berücksichtigt werden, wenn sie insgesamt mindestens 180 Tage und einzeln ununterbrochen mindestens 30 Tage gedauert haben.

##### § 23  
Allgemeine Bestimmungen zur Berücksichtigung von Dienstzeiten

(1) Zeiten einer Teilzeitbeschäftigung sind nur zu dem Teil ruhegehaltfähig, der dem Verhältnis der ermäßigten zur regelmäßigen Arbeitszeit entspricht; Zeiten einer Altersteilzeit nach beamten- oder richterrechtlichen Bestimmungen sind zu neun Zehnteln der Arbeitszeit ruhegehaltfähig, die der Bemessung der ermäßigten Arbeitszeit während der Altersteilzeit zugrunde gelegt worden ist.
Zeiten der eingeschränkten Verwendung einer Beamtin oder eines Beamten wegen begrenzter Dienstfähigkeit nach § 27 des Beamtenstatusgesetzes sind nur zu dem Teil ruhegehaltfähig, der dem Verhältnis der ermäßigten zur regelmäßigen Arbeitszeit entspricht; bis zum Ablauf des Monats der Vollendung des 60.
Lebensjahres mindestens im Umfang des § 22 Absatz 1 Satz 1.

(2) Zeiten im Sinne der §§ 16 bis 18 und 20 sollen nur berücksichtigt werden, wenn sie vor der Berufung in das Beamtenverhältnis zurückgelegt wurden.

(3) Hauptberuflich ist eine Tätigkeit, wenn sie gegen Entgelt erbracht wird, den Schwerpunkt der beruflichen Tätigkeit darstellt, dem durch Ausbildung und Berufswahl geprägten Berufsbild entspricht und deren Beschäftigungsumfang im gleichen Zeitraum im Beamtenverhältnis zulässig gewesen wäre.

(4) Die Gesamtversorgung aus den im Rahmen der Ermessensausübung nach den §§ 18, 19 und 21 zu berücksichtigenden Versorgungsleistungen und den nach diesem Gesetz zu leistenden Versorgungsbezügen soll die Höchstgrenze nach § 76 Absatz 2 nicht übersteigen.

##### § 24  
Nicht zu berücksichtigende Zeiten

Zeiten im Sinne des § 64 Absatz 1 des Landesbeamtengesetzes sind nicht ruhegehaltfähig.

##### § 25  
Höhe des Ruhegehalts

(1) Das Ruhegehalt wird durch Anwendung eines Prozentsatzes (Ruhegehaltssatz) auf die ruhegehaltfähigen Bezüge (§ 13) ermittelt.
Der Ruhegehaltssatz beträgt für jedes Jahr ruhegehaltfähiger Dienstzeit 1,79375 Prozent, insgesamt jedoch höchstens 71,75 Prozent.

(2) Das Ruhegehalt vermindert sich um einen Versorgungsabschlag in Höhe von 3,6 Prozent für jedes Jahr, um das die Beamtin oder der Beamte

1.  vor Ablauf des Monats, in dem sie oder er die jeweils geltende gesetzliche Altersgrenze (§ 45 Absatz 1 oder § 123 Absatz 6 des Landesbeamtengesetzes) erreicht, nach § 46 Absatz 1 Satz 1 des Landesbeamtengesetzes (Antragsaltersgrenze) in den Ruhestand versetzt wird,
2.  vor Ablauf des Monats, in dem sie oder er das 65.
    Lebensjahr vollendet, nach § 46 Absatz 1 Satz 2 des Landesbeamtengesetzes (Antragsaltersgrenze bei Schwerbehinderung im Sinne des § 2 Absatz 2 des Neunten Buches Sozialgesetzbuch) in den Ruhestand versetzt wird,
3.  vor Ablauf des Monats, in dem sie oder er das 65.
    Lebensjahr vollendet, wegen Dienstunfähigkeit, die nicht auf einem Dienstunfall beruht, in den Ruhestand versetzt wird,
4.  vor Ablauf des Monats, in dem sie oder er die jeweils geltende Altersgrenze nach den §§ 110 Absatz 1 bis 5, 117 und 118 des Landesbeamtengesetzes erreicht, nach § 110 Absatz 8 des Landesbeamtengesetzes (besondere Antragsaltersgrenze für Beamtinnen und Beamte des Polizeivollzugsdienstes, des feuerwehrtechnischen Dienstes und des Justizvollzugsdienstes) in den Ruhestand versetzt wird.

Der Versorgungsabschlag darf im Fall des Satzes 1 Nummer 3 oder wenn die Beamtin oder der Beamte schwerbehindert im Sinne des § 2 Absatz 2 des Neunten Buches Sozialgesetzbuch ist, 10,8 Prozent nicht übersteigen.
§ 10 gilt entsprechend.
Gilt für die Beamtin oder den Beamten eine vor der Vollendung des 65.
Lebensjahres liegende Altersgrenze, tritt sie in den Fällen des Satzes 1 Nummer 2 und 3 an die Stelle des 65.
Lebensjahres.
Gilt für die Beamtin oder den Beamten eine nach der Regelaltersgrenze (§ 45 Absatz 1 des Landesbeamtengesetzes) liegende Altersgrenze, wird in den Fällen des Satzes 1 Nummer 1 nur die Zeit bis zum Ablauf des Monats berücksichtigt, in dem die Beamtin oder der Beamte die Regelaltersgrenze erreicht hätte.

(3) Ein Versorgungsabschlag entfällt

1.  in den Fällen des Absatzes 2 Satz 1 Nummer 1, wenn die Beamtin oder der Beamte zum Zeitpunkt des Eintritts in den Ruhestand das 65.
    Lebensjahr vollendet und eine Dienstzeit von 45 Jahren erreicht hat,
2.  in den Fällen des Absatzes 2 Satz 1 Nummer 3, wenn die Beamtin oder der Beamte zum Zeitpunkt des Eintritts in den Ruhestand das 63.
    Lebensjahr vollendet und eine Dienstzeit von 40 Jahren erreicht hat.

Dienstzeiten im Sinne des Satzes 1 sind ruhegehaltfähige Dienstzeiten nach den §§ 14, 16, 17 und nach § 26 Absatz 2 Satz 1 berücksichtigungsfähige Pflichtbeitragszeiten, soweit sie nicht im Zusammenhang mit Arbeitslosigkeit stehen, und Zeiten nach § 72 sowie Zeiten einer der Beamtin oder dem Beamten zuzuordnenden Erziehung eines Kindes bis zu dessen vollendetem zehnten Lebensjahr berücksichtigt.
Soweit sich bei der Berechnung Zeiten überschneiden, sind diese nur einmal zu berücksichtigen.

(4) Das Ruhegehalt beträgt mindestens 35 Prozent der ruhegehaltfähigen Dienstbezüge nach § 13 (amtsabhängige Mindestversorgung).
An dessen Stelle treten, wenn dies günstiger ist, 65,8 Prozent der ruhegehaltfähigen Bezüge aus der Endstufe der Besoldungsgruppe A 5 (amtsunabhängige Mindestversorgung).
Die Sätze 1 und 2 sind nicht anzuwenden, wenn die Beamtin oder der Beamte eine ruhegehaltfähige Dienstzeit nach den §§ 14, 16, 17 und 21 von weniger als fünf Jahren zurückgelegt hat; § 23 ist anzuwenden.
Dies gilt nicht, wenn in Fällen des § 12 Absatz 1 Satz 1 Nummer 2 die Beamtin oder der Beamte wegen Dienstunfähigkeit in den Ruhestand versetzt wurde.

(5) Übersteigt beim Zusammentreffen von Mindestversorgung nach Absatz 4 mit einer Rente nach Anwendung des § 76 die Versorgung das nach Absatz 1 erdiente Ruhegehalt, so ruht die Versorgung bis zur Höhe des Unterschieds zwischen dem erdienten Ruhegehalt und der Mindestversorgung.
Der Familienzuschlag nach § 69 Absatz 2 bleibt bei der Berechnung außer Betracht.
Die Summe aus Versorgung und Rente darf nicht hinter dem Betrag der Mindestversorgung zuzüglich des Familienzuschlags nach § 69 Absatz 2 zurückbleiben.
Zahlbar bleibt mindestens das erdiente Ruhegehalt zuzüglich des Familienzuschlags nach § 69 Absatz 2.
Die Sätze 1 bis 4 gelten entsprechend für Witwen und Witwer, für hinterbliebene eingetragene Lebenspartnerinnen und Lebenspartner und für Waisen.

(6) Bei in den einstweiligen Ruhestand versetzten Beamtinnen oder Beamten beträgt das Ruhegehalt für die Dauer der Zeit, die die Beamtin oder der Beamte das Amt, aus dem sie oder er in den einstweiligen Ruhestand versetzt worden ist, innehatte, mindestens für die Dauer von sechs Monaten, längstens für die Dauer von drei Jahren, 71,75 Prozent der ruhegehaltfähigen Dienstbezüge aus der Endstufe der Besoldungsgruppe, in der sich die Beamtin oder der Beamte zur Zeit der Versetzung in den einstweiligen Ruhestand befunden hat.
Das erhöhte Ruhegehalt darf die Dienstbezüge, die der Beamtin oder dem Beamten zu diesem Zeitpunkt zustanden, nicht übersteigen.

##### § 26  
Vorübergehende Erhöhung des Ruhegehaltssatzes

(1) Der nach § 25 Absatz 1, § 27 Absatz 2 und § 55 Absatz 3 Satz 1 berechnete Ruhegehaltssatz erhöht sich vorübergehend, wenn die Beamtin oder der Beamte vor Erreichen der Altersgrenzen nach § 45 Absatz 1 oder § 123 Absatz 6 des Landesbeamtengesetzes in den Ruhestand getreten oder versetzt worden ist und

1.  bis zum Beginn des Ruhestandes die allgemeine Wartezeit von fünf Jahren für eine Rente der gesetzlichen Rentenversicherung erfüllt war,
2.  die Beamtin oder der Beamte
    1.  wegen Dienstunfähigkeit im Sinne des § 26 Absatz 1 des Beamtenstatusgesetzes in den Ruhestand versetzt worden ist,
    2.  wegen Erreichens einer besonderen Altersgrenze in den Ruhestand getreten ist oder
    3.  vor Erreichen einer besonderen Altersgrenze auf Antrag in den Ruhestand getreten ist, ab dem Zeitpunkt, zu dem sie oder er wegen Erreichens einer besonderen Altersgrenze in den Ruhestand getreten wäre,
3.  einen Ruhegehaltssatz von 66,97 Prozent noch nicht erreicht hat und
4.  keine Einkünfte im Sinne des § 74 Absatz 5 bezieht; Einkünfte bleiben außer Betracht, soweit sie durchschnittlich im Monat 470 Euro nicht überschreiten; für den Monat November ist dieser Betrag um den jeweils zustehenden Betrag nach § 48b des Brandenburgischen Besoldungsgesetzes zu erhöhen.

(2) Die Erhöhung des Ruhegehaltssatzes beträgt 0,95667 Prozent der ruhegehaltfähigen Dienstbezüge für je zwölf Kalendermonate der für die Erfüllung der Wartezeit (Absatz 1 Nummer 1) anrechnungsfähigen Pflichtbeitragszeiten, soweit sie nicht von § 73 Absatz 1 erfasst werden, vor Begründung des Beamtenverhältnisses zurückgelegt wurden und nicht als ruhegehaltfähig berücksichtigt sind.
Der hiernach berechnete Ruhegehaltssatz darf 66,97 Prozent nicht überschreiten.
In den Fällen des § 25 Absatz 2 ist das Ruhegehalt, das sich nach Anwendung der Sätze 1 und 2 ergibt, entsprechend zu vermindern.
Für die Berechnung nach Satz 1 sind verbleibende Kalendermonate unter Benutzung des Nenners 12 umzurechnen.
§ 10 gilt entsprechend.

(3) Die Erhöhung fällt spätestens mit Ablauf des Monats weg, in dem die Ruhestandsbeamtin oder der Ruhestandsbeamte die maßgebliche Regelaltersgrenze nach § 35 oder § 235 des Sechsten Buches Sozialgesetzbuch erreicht.
Sie endet vorher, wenn die Ruhestandsbeamtin oder der Ruhestandsbeamte

1.  eine Versichertenrente der gesetzlichen Rentenversicherung bezieht, mit Ablauf des Tages vor dem Beginn der Rente oder
2.  in den Fällen des Absatzes 1 Nummer 2 Buchstabe a nicht mehr dienstunfähig ist, mit Ablauf des Monats, in dem ihr oder ihm der Wegfall der Erhöhung mitgeteilt wird, oder
3.  ein Erwerbseinkommen bezieht, mit Ablauf des Tages vor dem Beginn der Erwerbstätigkeit.

§ 9 Absatz 2 Satz 1 Nummer 2 gilt sinngemäß.

##### § 27  
Beamtinnen und Beamte auf Zeit

(1) Für die Versorgung der Beamtinnen und Beamten auf Zeit und ihrer Hinterbliebenen gelten die Bestimmungen für die Versorgung der Beamtinnen und Beamten und ihrer Hinterbliebenen entsprechend, soweit in diesem Gesetz nichts anderes bestimmt ist.

(2) Für Beamtinnen und Beamte auf Zeit, die eine ruhegehaltfähige Dienstzeit von zehn Jahren zurückgelegt haben, beträgt der Ruhegehaltssatz, wenn es für sie günstiger ist, nach einer Amtszeit von acht Jahren als Beamtin auf Zeit oder als Beamter auf Zeit 35 Prozent der ruhegehaltfähigen Bezüge und steigt mit jedem weiteren vollen Amtsjahr als Beamtin auf Zeit oder als Beamter auf Zeit um 1,91333 Prozent der ruhegehaltfähigen Bezüge bis zum Höchstruhegehaltssatz von 71,75 Prozent.
Als Amtszeit rechnet hierbei auch die Zeit bis zur Dauer von fünf Jahren, die eine Beamtin auf Zeit oder ein Beamter auf Zeit im einstweiligen Ruhestand zurückgelegt hat.
§ 25 Absatz 2 bis 4 ist entsprechend anzuwenden.

(3) Führen Beamtinnen und Beamte auf Zeit nach Ablauf ihrer Amtszeit das bisherige Amt unter erneuter Berufung in das Beamtenverhältnis auf Zeit oder durch Wiederwahl für die folgende Amtszeit weiter, gilt für die Anwendung dieses Gesetzes das Beamtenverhältnis als nicht unterbrochen.
Satz 1 gilt entsprechend für Beamtinnen und für Beamte auf Zeit, die aus ihrem bisherigen Amt ohne Unterbrechung in ein vergleichbares oder höherwertiges Amt unter erneuter Berufung in das Beamtenverhältnis auf Zeit gewählt werden.

(4) Ein Übergangsgeld nach § 65 wird nicht gewährt, wenn die Beamtin auf Zeit oder der Beamte auf Zeit einer gesetzlichen Verpflichtung, das Amt nach Ablauf der Amtszeit unter erneuter Berufung in das Beamtenverhältnis weiterzuführen, nicht nachkommt.

(5) Werden Beamtinnen und Beamte auf Zeit wegen Dienstunfähigkeit entlassen, gelten in Bezug auf die Gewährung eines Unterhaltsbeitrags die §§ 28 und 41 entsprechend.

(6) Wird eine Wahlbeamtin auf Zeit oder ein Wahlbeamter auf Zeit wegen Dienstunfähigkeit in den Ruhestand versetzt, ist § 25 Absatz 2 Satz 1 Nummer 3 nicht anzuwenden, wenn sie oder er nach Ablauf der Amtszeit das Amt weitergeführt hat, obwohl sie oder er nicht gesetzlich dazu verpflichtet war und mit Ablauf der Amtszeit bereits eine Versorgungsanwartschaft erworben hatte.

(7) Wird eine Wahlbeamtin auf Zeit oder ein Wahlbeamter auf Zeit abgewählt, erhält sie oder er bis zum Ablauf ihrer oder seiner Amtszeit, bei einem vorherigen Eintritt in den Ruhestand oder der Entlassung längstens bis zu diesem Zeitpunkt, Versorgung mit der Maßgabe, dass das Ruhegehalt während der ersten fünf Jahre 71,75 Prozent der ruhegehaltfähigen Dienstbezüge aus der Endstufe der Besoldungsgruppe, in der sich die Beamtin oder der Beamte zur Zeit der Abwahl befunden hat, beträgt.
Die ruhegehaltfähige Dienstzeit nach § 14 erhöht sich um die Zeit, in der eine Wahlbeamtin auf Zeit oder ein Wahlbeamter auf Zeit Versorgung nach Satz 1 erhält, bis zu fünf Jahren; das Höchstruhegehalt nach Absatz 2 darf nicht überschritten werden.

(8) Zeiten, während der eine Wahlbeamtin auf Zeit oder ein Wahlbeamter auf Zeit durch eine hauptberufliche Tätigkeit oder eine Ausbildung außerhalb der allgemeinen Schulbildung Fachkenntnisse erworben hat, die für die Wahrnehmung des Amtes förderlich sind, können bis zu einer Gesamtzeit von vier Jahren als ruhegehaltfähig berücksichtigt werden, die Zeit einer Fachschul- oder Hochschulausbildung einschließlich der Prüfungszeit bis zu drei Jahren.
§ 3 Absatz 3 gilt entsprechend.

##### § 28  
Unterhaltsbeitrag für entlassene Beamtinnen und Beamte

Einer Beamtin auf Lebenszeit, einem Beamten auf Lebenszeit, einer Beamtin auf Probe oder einem Beamten auf Probe, die oder der wegen Erreichens der Altersgrenze nach § 22 Absatz 1 Nummer 2 des Beamtenstatusgesetzes entlassen ist oder wegen Dienstunfähigkeit nach § 23 Absatz 1 Satz 1 Nummer 3 des Beamtenstatusgesetzes zu entlassen ist, kann auf Antrag ein Unterhaltsbeitrag für jedes Jahr der Dauer ihres oder seines Dienstverhältnisses, längstens für fünf Jahre bis zur Höhe des Ruhegehalts bewilligt werden.

##### § 29  
Beamtinnen und Beamte auf Probe in leitender Funktion

(1) § 28 findet auf Beamtenverhältnisse auf Probe in leitender Funktion keine Anwendung.

(2) Aus Beamtenverhältnissen auf Probe in leitender Funktion ergibt sich kein selbstständiger Anspruch auf Versorgung; die Unfallfürsorge bleibt hiervon unberührt.

#### Unterabschnitt 2  
Hinterbliebenenversorgung

##### § 30  
Versorgungsurheberinnen und Versorgungsurheber

Versorgungsurheberinnen und Versorgungsurheber für die in diesem Unterabschnitt geregelten Ansprüche sind, soweit nichts anderes bestimmt ist,

1.  verstorbene Beamtinnen und Beamte auf Lebenszeit und auf Zeit, die die Voraussetzungen des § 12 Absatz 1 erfüllt haben,
2.  verstorbene Ruhestandsbeamtinnen und Ruhestandsbeamte sowie
3.  verstorbene Beamtinnen und Beamte auf Probe, die an den Folgen einer Dienstbeschädigung (§ 28 Absatz 1 des Beamtenstatusgesetzes) verstorben sind oder denen die Entscheidung über die Versetzung in den Ruhestand wegen Dienstunfähigkeit (§ 28 Absatz 2 des Beamtenstatusgesetzes) zugestellt war.

##### § 31  
Arten der Hinterbliebenenversorgung

Die Hinterbliebenenversorgung umfasst

1.  Bezüge für den Sterbemonat (§ 32),
2.  Sterbegeld (§ 33),
3.  Witwen- und Witwergeld (§ 34),
4.  Witwen- und Witwerabfindung (§ 36),
5.  Waisengeld (§ 38),
6.  Unterhaltsbeiträge (§§ 37 und 41).

##### § 32  
Bezüge für den Sterbemonat

Die Bezüge einschließlich Aufwandsentschädigungen für den Sterbemonat stehen in voller Höhe zu.

##### § 33  
Sterbegeld

(1) Beim Tod einer Beamtin oder eines Beamten, einer Ruhestandsbeamtin oder eines Ruhestandsbeamten sowie einer entlassenen Beamtin oder eines entlassenen Beamten, die oder der im Sterbemonat einen Unterhaltsbeitrag erhalten hat, wird Sterbegeld an die überlebende Ehegattin, an den überlebenden Ehegatten, an die überlebende eingetragene Lebenspartnerin oder an den überlebenden eingetragenen Lebenspartner und an die Abkömmlinge der Beamtin oder des Beamten gezahlt.
Satz 1 gilt nicht für die Hinterbliebenen von Ehrenbeamtinnen oder Ehrenbeamten.

(2) Das Sterbegeld beträgt das Zweifache der laufenden monatlichen Bezüge oder der Anwärterbezüge der oder des Verstorbenen ausschließlich der Auslandsbesoldung und der Vergütungen und ist in einer Summe zu zahlen; § 13 Absatz 1 Satz 2 und 3 gilt entsprechend.
Sterbegeld aus anderen Beschäftigungsverhältnissen kann angerechnet werden.

(3) Sind Anspruchsberechtigte im Sinne des Absatzes 1 nicht vorhanden, so ist Sterbegeld auf Antrag zu gewähren

1.  Verwandten der aufsteigenden Linie, Geschwistern, Geschwisterkindern sowie Stiefkindern, wenn sie zur Zeit des Todes der Beamtin oder des Beamten mit dieser oder diesem in häuslicher Gemeinschaft gelebt haben oder wenn die oder der Verstorbene ganz oder überwiegend ihre Ernährerin oder ihr Ernährer gewesen ist,
2.  sonstigen Personen, die die Kosten der letzten Krankheit oder der Bestattung getragen haben, bis zur Höhe ihrer Aufwendungen, höchstens jedoch in Höhe des Sterbegeldes nach Absatz 2.

(4) Stirbt eine Bezieherin oder ein Bezieher von Witwen- oder Witwergeld, so erhalten die Kinder der in Absatz 1 Satz 1 genannten Personen Sterbegeld, wenn sie berechtigt sind, Waisengeld oder einen Unterhaltsbeitrag zu beziehen und zur Zeit des Todes zur häuslichen Gemeinschaft der oder des Verstorbenen gehört haben.
Das Sterbegeld beträgt das Zweifache des Witwen- oder Witwergeldes.
Dies gilt entsprechend, wenn an Stelle des Witwen- oder Witwergeldes Unterhaltsbeitrag bezogen wird.
Absatz 2 Satz 2 gilt entsprechend.

(5) Sind mehrere gleichberechtigte Personen vorhanden, ist das Sterbegeld an die Person, die die Kosten der letzten Krankheit oder der Bestattung überwiegend getragen hat, hilfsweise an einen anderen Berechtigten zu zahlen.

##### § 34  
Witwen- und Witwergeld

(1) Witwen, Witwer, hinterbliebene eingetragene Lebenspartnerinnen oder hinterbliebene eingetragene Lebenspartner einer Versorgungsurheberin oder eines Versorgungsurhebers (§ 30) erhalten Witwen- oder Witwergeld.

(2) Kein Anspruch besteht, wenn

1.  die Ehe oder die eingetragene Lebenspartnerschaft weniger als ein Jahr gedauert hat, es sei denn, nach den besonderen Umständen des Falls ist die Annahme nicht gerechtfertigt, dass es der alleinige oder überwiegende Zweck der Heirat oder der Begründung einer eingetragenen Lebenspartnerschaft war, der Witwe, dem Witwer, der hinterbliebenen Lebenspartnerin oder dem hinterbliebenen Lebenspartner eine Versorgung zu verschaffen oder
2.  die Versorgungsurheberin oder der Versorgungsurheber sich zum Zeitpunkt der Eheschließung oder der Begründung einer eingetragenen Lebenspartnerschaft bereits im Ruhestand befand und die Altersgrenze nach § 45 Absatz 1 des Landesbeamtengesetzes erreicht hatte.

(3) Der Anspruch einer Witwe oder eines Witwers aus einer zum Zeitpunkt des Todes bestehenden Ehe schließt den Anspruch einer hinterbliebenen Lebenspartnerin oder eines hinterbliebenen Lebenspartners aus einer zum Zeitpunkt des Todes bestehenden eingetragenen Lebenspartnerschaft aus.

##### § 35  
Höhe des Witwen- und Witwergeldes

(1) Das Witwen- und Witwergeld beträgt 55 Prozent des Ruhegehalts, das die Versorgungsurheberin oder der Versorgungsurheber erhalten hat oder hätte erhalten können, wenn sie oder er am Todestag in den Ruhestand getreten wäre.
§ 25 Absatz 6 und die §§ 26 und 27 Absatz 7 sind nicht anzuwenden.
Änderungen des Mindestruhegehalts (§ 25 Absatz 4) sind zu berücksichtigen.

(2) War die Witwe, der Witwer, die hinterbliebene eingetragene Lebenspartnerin oder der hinterbliebene eingetragene Lebenspartner mehr als zwanzig Jahre jünger als die oder der Verstorbene und ist aus der Ehe oder der eingetragenen Lebenspartnerschaft kein Kind hervorgegangen, so wird das Witwen- oder Witwergeld (Absatz 1) für jedes angefangene Jahr des Altersunterschiedes über zwanzig Jahre um 5 Prozent gekürzt, jedoch höchstens um 50 Prozent.
Nach fünfjähriger Dauer der Ehe oder der eingetragenen Lebenspartnerschaft werden für jedes angefangene Jahr ihrer weiteren Dauer dem gekürzten Betrag 5 Prozent des Witwen- oder Witwergeldes hinzugesetzt, bis der volle Betrag wieder erreicht ist.
Das nach Satz 1 errechnete Witwen- oder Witwergeld darf nicht hinter dem Mindestwitwen- oder Mindestwitwergeld (Absatz 1 in Verbindung mit § 25 Absatz 4) zurückbleiben.

##### § 36  
Witwen- und Witwerabfindung

(1) Witwen, Witwer, die hinterbliebene eingetragene Lebenspartnerin oder der hinterbliebene eingetragene Lebenspartner mit Anspruch auf Witwen- oder Witwergeld oder auf einen Unterhaltsbeitrag erhalten in den Fällen einer Wiederverheiratung oder einer Neubegründung einer eingetragenen Lebenspartnerschaft eine Witwen- und Witwerabfindung.

(2) Die Witwen- und Witwerabfindung beträgt das 24fache des für den Monat der Wiederverheiratung oder der Neubegründung einer eingetragenen Lebenspartnerschaft nach Anwendung der Anrechnungs-, Kürzungs- und Ruhensvorschriften zu zahlenden Betrags des Witwen- oder Witwergeldes oder des Unterhaltsbeitrags; eine Kürzung nach § 40 und die Anwendung der §§ 74 und 75 Absatz 1 Satz 1 Nummer 3 bleiben jedoch außer Betracht.
Die Abfindung ist in einer Summe zu zahlen.

##### § 37  
Unterhaltsbeitrag für nicht witwen- und witwergeldberechtigte Witwen und Witwer oder hinterbliebene eingetragene Lebenspartnerinnen und Lebenspartner

In den Fällen des § 34 Absatz 2 Nummer 2 ist ein angemessener Unterhaltsbeitrag bis zur Höhe des Witwen- und Witwergeldes zu gewähren.
Erwerbseinkommen und Erwerbsersatzeinkommen sind in angemessenem Umfang anzurechnen.
Wird ein Erwerbsersatzeinkommen nicht beantragt oder wird auf ein Erwerbs- oder Erwerbsersatzeinkommen verzichtet oder wird an deren Stelle eine Kapitalleistung, Abfindung oder Beitragserstattung gezahlt, ist der Betrag zu berücksichtigen, der ansonsten zu zahlen wäre.

##### § 38  
Waisengeld

(1) Kinder der Versorgungsurheberin oder des Versorgungsurhebers erhalten Waisengeld.

(2) Kein Waisengeld wird gewährt, wenn das Kindschaftsverhältnis durch Annahme als Kind begründet wurde und die Versorgungsurheberin oder der Versorgungsurheber in diesem Zeitpunkt bereits im Ruhestand war und die Altersgrenze nach § 45 Absatz 1 des Landesbeamtengesetzes erreicht hatte.
In diesen Fällen kann ein Unterhaltsbeitrag bis zur Höhe des Waisengeldes bewilligt werden.

##### § 39  
Höhe des Waisengeldes

(1) Das Waisengeld beträgt für die Halbwaise 12 Prozent und für die Vollwaise 20 Prozent des Ruhegehalts, das die Versorgungsurheberin oder der Versorgungsurheber erhalten hat oder hätte erhalten können, wenn sie oder er am Todestag in den Ruhestand getreten wäre.
§ 25 Absatz 6 und die §§ 26 und 27 Absatz 7 sind nicht anzuwenden.
Änderungen des Mindestruhegehalts (§ 25 Absatz 4) sind zu berücksichtigen.

(2) Wenn der überlebende Elternteil nicht zum Bezug von Witwen- oder Witwergeld berechtigt ist und auch keinen Unterhaltsbeitrag in Höhe des Witwen- oder Witwergeldes erhält, wird das Waisengeld nach dem Satz für Vollwaisen gezahlt; es darf zuzüglich des Unterhaltsbeitrags den Betrag des Witwen- und Witwergeldes und des Waisengeldes nach dem Satz für Halbwaisen nicht übersteigen.

(3) Ergeben sich für eine Waise Waisengeldansprüche aus Beamtenverhältnissen mehrerer Personen, wird nur das höchste Waisengeld gezahlt.

##### § 40  
Zusammentreffen von Witwen- und Witwergeld, Waisengeld und Unterhaltsbeiträgen

(1) Witwen- und Witwergeld, Waisengeld und Unterhaltsbeiträge dürfen weder einzeln noch zusammen den Betrag des ihrer Berechnung zugrunde zu legenden Ruhegehalts übersteigen.
Ergibt sich zusammen ein höherer Betrag, so werden die einzelnen Bezüge im gleichen Verhältnis gekürzt.

(2) Nach dem Ausscheiden Versorgungsberechtigter, die Bezüge nach Absatz 1 erhalten, erhöhen sich die verbleibenden Bezüge vom Beginn des folgenden Monats an insoweit, als die verbleibenden Versorgungsberechtigten nach Absatz 1 noch nicht den vollen Betrag erhalten.

(3) Unterhaltsbeiträge nach § 38 Absatz 2 Satz 2 dürfen nur insoweit bewilligt werden, als sie allein oder zusammen mit gesetzlichen Hinterbliebenenbezügen die in Absatz 1 Satz 1 bezeichnete Höchstgrenze nicht übersteigen.

##### § 41  
Unterhaltsbeitrag für Hinterbliebene

Der Witwe, dem Witwer, der hinterbliebenen eingetragenen Lebenspartnerin oder dem hinterbliebenen eingetragenen Lebenspartner und den Kindern einer Beamtin oder eines Beamten, der oder dem nach § 28 ein Unterhaltsbeitrag bewilligt worden war oder hätte bewilligt werden können, kann die in den §§ 34, 35 und den §§ 37 bis 40 vorgesehene Versorgung bis zu der dort bezeichneten Höhe als Unterhaltsbeitrag bewilligt werden.
§ 36 gilt entsprechend.

##### § 42  
Beginn der Zahlungen

Ansprüche nach diesem Unterabschnitt entstehen mit Beginn des Monats, in dem die gesetzlichen Voraussetzungen erfüllt sind, frühestens jedoch mit dem Ablauf des Sterbemonats.
Kinder, die nach diesem Zeitpunkt geboren werden, erhalten Waisengeld oder Unterhaltsbeitrag nach § 41 vom Beginn des Geburtsmonats an.

##### § 43  
Erlöschen der Hinterbliebenenversorgung

(1) Der Anspruch auf Hinterbliebenenversorgung erlischt

1.  für jeden Berechtigten mit dem Ende des Monats, in dem er stirbt,
2.  für jede Witwe, jeden Witwer, jede hinterbliebene eingetragene Lebenspartnerin und jeden hinterbliebenen eingetragenen Lebenspartner außerdem mit dem Ende des Monats, in dem sie oder er sich verheiratet oder eine neue eingetragene Lebenspartnerschaft begründet,
3.  für jede Waise außerdem mit dem Ende des Monats, in dem sie das 18.
    Lebensjahr vollendet.

(2) Die Ansprüche der Waisen auf Waisengeld und Unterhaltsbeitrag bestehen nach Vollendung des 18.
Lebensjahres auf Antrag längstens bis zur Vollendung des 27.
Lebensjahres, solange die Waise

1.  sich in Schul- oder Berufsausbildung oder in einer Übergangszeit von höchstens vier Monaten befindet oder
2.  ein freiwilliges soziales oder ökologisches Jahr oder einen vergleichbaren Dienst leistet oder
3.  wegen körperlicher, geistiger oder seelischer Behinderung außerstande ist, sich selbst zu unterhalten.

In den Fällen des Satzes 1 Nummer 3 wird das Waisengeld ungeachtet der Höhe eines eigenen Einkommens dem Grunde nach gewährt.
Soweit ein eigenes Einkommen der Waise das Zweifache des Mindestvollwaisengeldes (§ 39 Absatz 1 in Verbindung mit § 25 Absatz 4 Satz 2) übersteigt, wird es zur Hälfte auf das Waisengeld zuzüglich des Familienzuschlags nach § 69 Absatz 2 angerechnet.

(3) In den Fällen des Absatzes 2 Satz 1 Nummer 1 und 2 wird eine Waise, die

1.  den gesetzlichen Grundwehrdienst, den freiwilligen Wehrdienst nach § 54 des Wehrpflichtgesetzes oder Zivildienst geleistet hat oder
2.  den Bundesfreiwilligendienst nach dem Bundesfreiwilligendienstgesetz geleistet hat oder
3.  sich anstelle des gesetzlichen Grundwehrdienstes freiwillig für die Dauer von nicht mehr als drei Jahren zum Wehrdienst verpflichtet hat oder
4.  eine vom gesetzlichen Wehrdienst oder Zivildienst befreiende Tätigkeit als Entwicklungshelferin oder Entwicklungshelfer im Sinne des § 1 Absatz 1 des Entwicklungshelfer-Gesetzes ausgeübt hat,

für einen der Dauer dieser Dienste oder der Tätigkeit entsprechenden Zeitraum, höchstens für die Dauer des inländischen Grundwehrdienstes oder bei anerkannten Kriegsdienstverweigerern für die Dauer des inländischen gesetzlichen Zivildienstes, über das 27.
Lebensjahr hinaus berücksichtigt.
Wird der gesetzliche Grundwehrdienst oder Zivildienst in einem Mitgliedstaat der Europäischen Union oder einem Staat geleistet, auf den das Abkommen über den Europäischen Wirtschaftsraum Anwendung findet, so ist die Dauer dieses Dienstes maßgebend.
Dem gesetzlichen Grundwehrdienst oder Zivildienst steht der entsprechende Dienst gleich, der in dem in Artikel 3 des Einigungsvertrages genannten Gebiet geleistet worden ist.

(4) Das Waisengeld nach Absatz 2 Satz 1 Nummer 3 wird über das 27.
Lebensjahr hinaus nur gewährt, wenn

1.  die Behinderung bei Vollendung des 27.
    Lebensjahres bestanden hat oder bis zu dem sich nach Absatz 3 ergebenden Zeitpunkt eingetreten ist, wenn die Waise sich in verzögerter Schul- oder Berufsausbildung befunden hat, und
2.  die Waise ledig oder verwitwet ist oder ihre Ehegattin, ihr Ehegatte, ihre frühere Ehegattin, ihr früherer Ehegatte, ihre eingetragene Lebenspartnerin, ihr eingetragener Lebenspartner, ihre frühere eingetragene Lebenspartnerin oder ihr früherer eingetragener Lebenspartner ihr keinen ausreichenden Unterhalt leisten kann oder dem Grunde nach nicht unterhaltspflichtig ist und sie nicht unterhält.

#### Unterabschnitt 3  
Unfallfürsorge

##### § 44  
Allgemeines

(1) Wird eine Beamtin oder ein Beamter durch einen Dienstunfall (§ 45) oder durch einen Einsatzunfall (§ 46) verletzt, wird Unfallfürsorge gewährt.
Unfallfürsorge wird auch dem Kind einer Beamtin gewährt, das durch deren Dienstunfall während der Schwangerschaft unmittelbar geschädigt wurde.
Dies gilt auch, wenn die Schädigung durch besondere Einwirkungen verursacht worden ist, die generell geeignet sind, bei der Mutter eine Erkrankung im Sinne des § 45 Absatz 3 zu verursachen.

(2) Die Unfallfürsorge umfasst

1.  Erstattung von Sachschäden und besonderen Aufwendungen (§ 50),
2.  Schadensausgleich in besonderen Fällen (§ 51),
3.  Heilverfahren (§§ 52, 53),
4.  Unfallausgleich (§ 54),
5.  Unfallruhegehalt oder Unterhaltsbeitrag (§§ 55 bis 58),
6.  Unfallsterbegeld (§ 59),
7.  Unfall-Hinterbliebenenversorgung (§§ 60 bis 62),
8.  einmalige Unfallentschädigung (§ 63).

In den Fällen von Absatz 1 Satz 2 und 3 erhält das Kind der Beamtin als Unfallfürsorge Heilverfahren (§§ 52, 53), Unfallausgleich (§ 54) und Unterhaltsbeitrag (§ 58).

##### § 45  
Dienstunfall

(1) Dienstunfall ist ein auf äußerer Einwirkung beruhendes, plötzliches, örtlich und zeitlich bestimmbares, einen Körperschaden verursachendes Ereignis, das in Ausübung oder infolge des Dienstes eingetreten ist.
Zum Dienst gehören auch

1.  Dienstreisen und die dienstliche Tätigkeit am Bestimmungsort,
2.  die Teilnahme an dienstlichen Veranstaltungen und
3.  Nebentätigkeiten im öffentlichen Dienst oder in dem ihm gleichstehenden Dienst, zu deren Übernahme die Beamtin oder der Beamte gemäß § 84 des Landesbeamtengesetzes verpflichtet ist, oder Nebentätigkeiten, deren Wahrnehmung von ihr oder ihm im Zusammenhang mit den Dienstgeschäften erwartet wird, sofern die Beamtin oder der Beamte hierbei nicht in der gesetzlichen Unfallversicherung versichert ist (§ 2 des Siebten Buches Sozialgesetzbuch).

(2) Als Dienst gilt auch

1.  das Zurücklegen des mit dem Dienst zusammenhängenden Weges zwischen Wohnung und Dienststelle,
2.  ein Abweichen in vertretbarem Umfang von dem unmittelbaren Weg zwischen der Wohnung und der Dienststelle, wenn  
    1.  das dem Grunde nach kindergeldberechtigende Kind der Beamtin oder des Beamten, das mit ihr oder ihm in einem Haushalt lebt, wegen ihrer oder seiner beruflichen Tätigkeit oder der der Ehegattin, des Ehegatten, der eingetragenen Lebenspartnerin oder des eingetragenen Lebenspartners fremder Obhut anvertraut wird oder
    2.  die Beamtin oder der Beamte mit anderen berufstätigen oder in der gesetzlichen Unfallversicherung versicherten Personen gemeinsam ein Fahrzeug für den Weg nach und von der Dienststelle benutzt,
3.  das Zurücklegen der mit dem Dienst zusammenhängenden Wege zwischen der Unterkunft, die die Beamtin oder der Beamte wegen der Entfernung der Wohnung vom Dienstort an diesem oder in dessen Nähe genommen hat, und der Wohnung oder der Dienststelle.

Ein Unfall bei Durchführung des Heilverfahrens (§ 52) oder auf einem hierzu notwendigen Weg gilt als Folge eines Dienstunfalls.

(3) Als Dienstunfall gilt auch die Erkrankung an einer der in der Anlage 1 zur Berufskrankheiten-Verordnung vom 31.
Oktober 1997 (BGBl.
I S.
2623), die zuletzt durch Artikel 1 der Verordnung vom 11.
Juni 2009 (BGBl.
I S.
1273) geändert worden ist, in der jeweils geltenden Fassung genannten Krankheit, wenn die Beamtin oder der Beamte nach der Art der dienstlichen Verrichtung der Gefahr der Erkrankung besonders ausgesetzt war, es sei denn, dass die Beamtin oder der Beamte sich die Krankheit außerhalb des Dienstes zugezogen hat.
Die Erkrankung an einer solchen Krankheit gilt jedoch stets als Dienstunfall, wenn sie durch gesundheitsschädigende Verhältnisse verursacht worden ist, denen die Beamtin oder der Beamte am Ort des dienstlich angeordneten Aufenthalts im Ausland besonders ausgesetzt war.

(4) Als Dienstunfall gilt auch ein tätlicher rechtswidriger Angriff auf die Beamtin oder den Beamten außerhalb des Dienstes, der im Hinblick auf pflichtgemäßes dienstliches Verhalten oder wegen der Eigenschaft als Beamtin oder Beamter erfolgt ist und einen Körperschaden verursacht hat.
Als Dienstunfall gilt ferner ein Körperschaden, den eine Beamtin oder ein Beamter im Ausland erleidet, wenn sie oder er bei Kriegshandlungen, Aufruhr oder Unruhen, denen sie oder er am Ort ihres oder seines dienstlich angeordneten Aufenthalts im Ausland besonders ausgesetzt war, angegriffen wird.

(5) Unfallfürsorge wie bei einem Dienstunfall kann auch gewährt werden, wenn eine Beamtin oder ein Beamter, die oder der zur Wahrnehmung einer Tätigkeit, die öffentlichen Belangen oder dienstlichen Interessen dient, beurlaubt worden ist und in Ausübung oder infolge dieser Tätigkeit einen Körperschaden erleidet.

##### § 45a  
Meldung von Dienstunfalldaten an EUROSTAT

(1) Die Unfallkasse Brandenburg ist berechtigt, die nach der Verordnung (EU) Nr.
349/2011 der Kommission vom 11.
April 2011 zur Durchführung der Verordnung (EG) Nr.
1338/2008 des Europäischen Parlaments und des Rates zu Gemeinschaftsstatistiken über öffentliche Gesundheit und über Gesundheitsschutz und Sicherheit am Arbeitsplatz betreffend Statistiken über Arbeitsunfälle (ABl. L 97 vom 12.
April 2011, S.
3) meldepflichtigen Daten über Dienstunfälle der vom Geltungsbereich dieses Gesetzes erfassten Beamtinnen und Beamten von den Dienstherren entgegenzunehmen, zu verarbeiten und über ihren Spitzenverband an das zuständige Bundesministerium weiterzuleiten.

(2) Der Unfallkasse Brandenburg sind alle durch die Aufgabenwahrnehmung entstehenden Kosten zu erstatten.
Das Nähere zur Aufgabenwahrnehmung und Kostenerstattung regelt eine Verwaltungsvereinbarung.

##### § 46  
Einsatzversorgung

(1) Unfallfürsorge wie bei einem Dienstunfall wird auch dann gewährt, wenn eine Beamtin oder ein Beamter aufgrund eines in Ausübung oder infolge des Dienstes eingetretenen Unfalls oder einer derart eingetretenen Erkrankung im Sinne des § 45 bei einer besonderen Verwendung im Ausland eine gesundheitliche Schädigung erleidet (Einsatzunfall).
Eine besondere Verwendung im Ausland ist eine Verwendung, die aufgrund eines Übereinkommens oder einer Vereinbarung mit einer über- oder zwischenstaatlichen Einrichtung oder mit einem auswärtigen Staat auf Beschluss der Bundesregierung im Ausland oder außerhalb des deutschen Hoheitsgebietes auf Schiffen oder in Luftfahrzeugen stattfindet, oder eine Verwendung im Ausland oder außerhalb des deutschen Hoheitsgebietes auf Schiffen oder in Luftfahrzeugen mit vergleichbar gesteigerter Gefährdungslage.
Die besondere Verwendung im Ausland beginnt mit dem Eintreffen im Einsatzgebiet und endet mit dem Verlassen des Einsatzgebietes.

(2) Gleiches gilt, wenn bei einer Beamtin oder bei einem Beamten eine Erkrankung oder ihre Folgen oder ein Unfall auf gesundheitsschädigende oder sonst vom Inland wesentlich abweichende Verhältnisse bei einer Verwendung im Sinne des Absatzes 1 zurückzuführen sind oder wenn eine gesundheitliche Schädigung bei dienstlicher Verwendung im Ausland auf einen Unfall oder eine Erkrankung im Zusammenhang mit einer Verschleppung oder einer Gefangenschaft zurückzuführen ist oder darauf beruht, dass die Beamtin oder der Beamte aus sonstigen mit dem Dienst zusammenhängenden Gründen dem Einflussbereich des Dienstherrn entzogen ist.

(3) § 45 Absatz 5 gilt entsprechend.

(4) Die Unfallfürsorge ist ausgeschlossen, wenn sich die Beamtin oder der Beamte vorsätzlich oder grob fahrlässig der Gefährdung ausgesetzt oder die Gründe für eine Verschleppung, Gefangenschaft oder sonstige Einflussbereichsentziehung herbeigeführt hat, es sei denn, dass der Ausschluss für sie oder für ihn eine unbillige Härte wäre.

##### § 47  
Meldung und Untersuchungsverfahren

(1) Unfälle, aus denen Unfallfürsorgeansprüche nach diesem Gesetz entstehen können, sind der Dienstvorgesetzten oder dem Dienstvorgesetzten innerhalb einer Ausschlussfrist von zwei Jahren nach dem Eintritt des Unfalls schriftlich zu melden.

(2) Nach Ablauf der Ausschlussfrist wird Unfallfürsorge nur gewährt, wenn seit dem Unfall noch nicht zehn Jahre vergangen sind und glaubhaft gemacht wird, dass mit der Möglichkeit eines Körperschadens oder einer Erkrankung aufgrund des Unfallereignisses nicht habe gerechnet werden können oder dass die oder der Berechtigte durch außerhalb ihres oder seines Willens liegende Umstände gehindert war, den Unfall zu melden.
Die Meldung muss, nachdem mit der Möglichkeit eines Körperschadens oder einer Erkrankung gerechnet werden konnte oder das Hindernis für die Meldung weggefallen ist, innerhalb von drei Monaten erfolgen.
Die Unfallfürsorge wird in diesen Fällen vom Tag der Meldung an gewährt; zur Vermeidung von Härten kann sie auch von einem früheren Zeitpunkt an gewährt werden.

(3) Die Dienstvorgesetzte oder der Dienstvorgesetzte hat jeden Unfall, der ihr oder ihm gemeldet oder von Amts wegen bekannt wird, sofort zu untersuchen.
Über das Ergebnis ist eine Niederschrift zu fertigen.
Die oberste Dienstbehörde oder die von ihr bestimmte Stelle entscheidet über die Anerkennung als Dienstunfall und die Gewährung der Unfallfürsorge.

(4) Unfallfürsorge nach § 44 Absatz 1 Satz 2 wird nur gewährt, wenn der Unfall der Beamtin innerhalb der Fristen nach den Absätzen 1 und 2 gemeldet und als Dienstunfall anerkannt worden ist.
Der Anspruch auf Unfallfürsorge nach § 44 Absatz 2 Satz 2 ist innerhalb von zwei Jahren vom Tag der Geburt an von den Sorgeberechtigten geltend zu machen.
Absatz 2 gilt mit der Maßgabe, dass die Zehnjahresfrist am Tag der Geburt zu laufen beginnt.
Der Antrag muss, nachdem mit der Möglichkeit einer Schädigung durch einen Dienstunfall der Mutter während der Schwangerschaft gerechnet werden konnte oder das Hindernis für den Antrag weggefallen ist, innerhalb von drei Monaten gestellt werden.

##### § 48  
Nichtgewährung von Unfallfürsorge

(1) Unfallfürsorge wird nicht gewährt, wenn die Verletzte oder der Verletzte den Dienstunfall vorsätzlich herbeigeführt hat.

(2) Hat die Verletzte oder der Verletzte eine die Heilbehandlung betreffende Anordnung ohne gesetzlichen oder sonstigen wichtigen Grund nicht befolgt und wird dadurch die Dienst- oder Erwerbsfähigkeit ungünstig beeinflusst, so kann ihr oder ihm die oberste Dienstbehörde oder die von ihr bestimmte Stelle die Unfallfürsorge insoweit versagen.
Die Verletzte oder der Verletzte ist auf diese Folgen schriftlich hinzuweisen.

(3) Hinterbliebenenversorgung nach den Unfallfürsorgevorschriften wird in den Fällen des § 37 nicht gewährt.

##### § 49  
Begrenzung der Unfallfürsorgeansprüche

(1) Die verletzte Beamtin oder der verletzte Beamte und ihre oder seine Hinterbliebenen haben aus Anlass eines Dienstunfalls gegen den Dienstherrn nur die in diesem Unterabschnitt geregelten Ansprüche.
Ist die Beamtin oder der Beamte nach dem Dienstunfall in den Dienstbereich eines anderen öffentlich-rechtlichen Dienstherrn im Geltungsbereich dieses Gesetzes versetzt worden, so richten sich die Ansprüche gegen diesen.
Das Gleiche gilt in den Fällen des gesetzlichen Übertritts oder der Übernahme bei der Umbildung von Körperschaften.
Satz 2 gilt in den Fällen, in denen Beamtinnen oder Beamte aus dem Dienstbereich eines öffentlich-rechtlichen Dienstherrn außerhalb des Geltungsbereiches dieses Gesetzes zu einem Dienstherrn im Geltungsbereich dieses Gesetzes versetzt werden mit der Maßgabe, dass die Vorschriften dieses Gesetzes anzuwenden sind.

(2) Weitergehende Ansprüche aufgrund allgemeiner gesetzlicher Vorschriften können gegen einen öffentlich-rechtlichen Dienstherrn im Bundesgebiet oder gegen die in seinem Dienst stehenden Personen nur dann geltend gemacht werden, wenn der Dienstunfall

1.  durch eine vorsätzliche unerlaubte Handlung einer solchen Person verursacht worden ist oder
2.  bei der Teilnahme am allgemeinen Verkehr eingetreten ist.

Im Fall des Satzes 1 Nummer 2 sind Leistungen, die der Beamtin oder dem Beamten und ihren oder seinen Hinterbliebenen nach diesem Gesetz gewährt werden, auf diese weitergehenden Ansprüche anzurechnen.
Der Dienstherr, der Leistungen nach diesem Gesetz gewährt, hat keinen Anspruch auf Ersatz dieser Leistungen gegen einen anderen öffentlich-rechtlichen Dienstherrn im Bundesgebiet, der zu einem weitergehenden Schadensersatz verpflichtet ist.

(3) Ersatzansprüche gegen andere Personen bleiben unberührt.

(4) Auf laufende und einmalige Geldleistungen, die nach diesem Gesetz wegen eines Körper-, Sach- oder Vermögensschadens gewährt werden, sind Geldleistungen anzurechnen, die wegen desselben Schadens von anderer Seite erbracht werden.
Hierzu gehören insbesondere Geldleistungen, die von Drittstaaten oder zwischenstaatlichen oder überstaatlichen Einrichtungen gewährt oder veranlasst werden.
Nicht anzurechnen sind Leistungen privater Schadensversicherungen, die auf Beiträgen der Verletzten oder des Verletzten beruhen.
Dies gilt nicht, wenn von den in Satz 2 genannten Stellen mindestens die Hälfte der Beiträge oder Zuschüsse in dieser Höhe gezahlt wurden.

##### § 50  
Erstattung von Sachschäden und besonderen Aufwendungen

Sind bei einem Dienstunfall Kleidungsstücke oder sonstige Gegenstände, die die Beamtin oder der Beamte mit sich geführt hat, beschädigt oder zerstört worden oder abhanden gekommen, so kann dafür Ersatz geleistet werden.
Anträge auf Gewährung von Sachschadensersatz nach Satz 1 sind innerhalb einer Ausschlussfrist von drei Monaten zu stellen.
Sind durch die erste Hilfeleistung nach dem Unfall besondere Kosten entstanden, so ist der Beamtin oder dem Beamten der nachweisbar notwendige Aufwand zu ersetzen.

##### § 51  
Schadensausgleich in besonderen Fällen

(1) Schäden, die einer Beamtin oder einem Beamten während einer Verwendung im Sinne des § 46 Absatz 1 infolge von besonderen, vom Inland wesentlich abweichenden Verhältnissen, insbesondere infolge von Kriegshandlungen, kriegerischen Ereignissen, Aufruhr, Unruhen oder Naturkatastrophen oder als Folge der Ereignisse nach § 46 Absatz 2 entstehen, werden ihr oder ihm in angemessenem Umfang ersetzt.
Gleiches gilt für Schäden der Beamtin oder des Beamten durch einen Gewaltakt gegen staatliche Amtsträgerinnen oder Amtsträger, Einrichtungen oder Maßnahmen, wenn die Beamtin oder der Beamte von dem Gewaltakt in Ausübung des Dienstes oder wegen ihrer oder seiner Eigenschaft als Beamtin oder Beamter betroffen ist.

(2) Ist eine Beamtin oder ein Beamter an den Folgen eines schädigenden Ereignisses der in Absatz 1 bezeichneten Art verstorben, wird

1.  der Witwe, dem Witwer, der hinterbliebenen eingetragenen Lebenspartnerin oder dem hinterbliebenen eingetragenen Lebenspartner sowie den nach beamtenrechtlichen Grundsätzen versorgungsberechtigten Kindern,
2.  den Eltern sowie den nach beamtenrechtlichen Grundsätzen nicht versorgungsberechtigten Kindern, wenn Hinterbliebene nach Nummer 1 nicht vorhanden sind,

ein Ausgleich in angemessenem Umfang gewährt.
Der Ausgleich für ausgefallene Versicherungen wird der natürlichen Person gewährt, die die Beamtin oder der Beamte im Versicherungsvertrag begünstigt hat.
Sind Versicherungsansprüche zur Finanzierung des Erwerbs von Wohnungseigentum an eine juristische Person abgetreten worden, wird der Ausgleich für die ausgefallene Versicherung an diese juristische Person gezahlt, wenn die Abtretung durch die Beamtin oder den Beamten dazu gedient hat, eine natürliche Person von Zahlungspflichten aufgrund der Finanzierung des Wohnungseigentums freizustellen.

(3) § 45 Absatz 5 gilt entsprechend.

(4) Der Schadensausgleich nach den Absätzen 1 und 2 wird nur einmal gewährt.
Wird er aufgrund derselben Ursache nach § 63b des Soldatenversorgungsgesetzes vorgenommen, sind die Absätze 1 und 2 nicht anzuwenden.

(5) Die Absätze 1 bis 3 sind auch auf Schäden bei dienstlicher Verwendung im Ausland anzuwenden, die im Zusammenhang mit einer Verschleppung oder einer Gefangenschaft entstanden sind oder darauf beruhen, dass die oder der Geschädigte aus sonstigen mit dem Dienst zusammenhängenden Gründen dem Einflussbereich des Dienstherrn entzogen ist.

(6) Für den Schadensausgleich gilt § 46 Absatz 4 entsprechend.

##### § 52  
Heilverfahren

(1) Das Heilverfahren umfasst die notwendige

1.  ärztliche und zahnärztliche Behandlung,
2.  Versorgung mit Arznei- und anderen Heilmitteln, Ausstattung mit Körperersatzstücken, orthopädischen und anderen Hilfsmitteln, die den Erfolg der Heilbehandlung sichern oder die Unfallfolgen erleichtern sollen,
3.  Pflege (§ 53).

(2) An Stelle der ärztlichen Behandlung sowie der Versorgung mit Arznei- und anderen Heilmitteln kann Krankenhausbehandlung gewährt werden.
Die oder der Verletzte ist verpflichtet, sich einer Krankenhausbehandlung zu unterziehen, wenn sie nach einer Stellungnahme einer oder eines durch die Dienstbehörde bestimmten Ärztin oder bestimmten Arztes zur Sicherung des Heilerfolges notwendig ist.

(3) Die oder der Verletzte ist verpflichtet, sich einer ärztlichen Behandlung zu unterziehen, es sei denn, dass sie mit einer erheblichen Gefahr für Leben oder Gesundheit der oder des Verletzten verbunden ist.
Das Gleiche gilt für eine Operation dann, wenn sie keinen erheblichen Eingriff in die körperliche Unversehrtheit bedeutet.

(4) Verursachen die Folgen des Dienstunfalls außergewöhnliche Kosten für Kleider- und Wäscheverschleiß, so sind diese in angemessenem Umfang zu ersetzen.

(5) Das Nähere zu Umfang und Durchführung des Heilverfahrens regelt das für das Beamtenversorgungsrecht zuständige Mitglied der Landesregierung durch Rechtsverordnung.

##### § 53  
Pflegekosten

(1) Ist die oder der Verletzte infolge des Dienstunfalls so hilflos, dass sie oder er nicht ohne fremde Hilfe und Pflege auskommen kann, sind die Kosten einer notwendigen Pflege in angemessenem Umfang zu erstatten.

(2) Nach dem Beginn des Ruhestands ist der oder dem Verletzten auf Antrag für die Dauer der Hilflosigkeit ein Zuschlag zu dem Unfallruhegehalt bis zum Erreichen der ruhegehaltfähigen Dienstbezüge zu gewähren; die Kostenerstattung nach Absatz 1 entfällt.

##### § 54  
Unfallausgleich

(1) Ist die oder der Verletzte infolge des Dienstunfalls in der Erwerbsfähigkeit länger als sechs Monate um mindestens 25 Prozent beschränkt, so wird, solange dieser Zustand andauert, neben der Besoldung oder dem Ruhegehalt ein Unfallausgleich in Höhe der Grundrente nach § 31 Absatz 1 bis 4 des Bundesversorgungsgesetzes gewährt.

(2) Die Minderung der Erwerbsfähigkeit ist nach der körperlichen Beeinträchtigung im allgemeinen Erwerbsleben zu beurteilen.
Eine unfallunabhängige Minderung der Erwerbsfähigkeit bleibt außer Betracht.
Beruht eine Minderung der Erwerbsfähigkeit auf einem früheren Dienstunfall, kann ein einheitlicher Unfallausgleich festgesetzt werden.
Für äußere Körperschäden können Mindestprozentsätze festgesetzt werden.
Vorübergehende Gesundheitsstörungen sind nicht zu berücksichtigen; als vorübergehend gilt ein Zeitraum bis zu sechs Monaten.

(3) Der Unfallausgleich wird neu festgestellt, wenn in den Verhältnissen, die für die Feststellung maßgebend gewesen sind, eine wesentliche Änderung eingetreten ist.
§ 9 gilt entsprechend.

(4) Der Unfallausgleich wird auch während einer Beurlaubung ohne Dienstbezüge gewährt.

(5) Bei Gewährung von Leistungen nach § 53 Absatz 2 ist der Unfallausgleich um die Hälfte zu mindern.

##### § 55  
Unfallruhegehalt

(1) Eine Beamtin oder ein Beamter, die oder der wegen dauernder Dienstunfähigkeit infolge eines Dienstunfalls in den Ruhestand versetzt wird, erhält Unfallruhegehalt.

(2) Für die Berechnung des Unfallruhegehalts einer oder eines vor Vollendung des 60.
Lebensjahres in den Ruhestand getretenen Beamtin oder Beamten wird der ruhegehaltfähigen Dienstzeit nur die Hälfte der Zurechnungszeit nach § 22 Absatz 1 hinzugerechnet.
§ 22 Absatz 3 gilt entsprechend.

(3) Der nach § 25 Absatz 1 ermittelte Ruhegehaltssatz erhöht sich um 20 Prozent.
Das Unfallruhegehalt beträgt mindestens 63,78 Prozent der ruhegehaltfähigen Dienstbezüge und darf 71,75 Prozent der ruhegehaltfähigen Dienstbezüge nicht übersteigen.
Es darf nicht hinter 71 Prozent der jeweils ruhegehaltfähigen Dienstbezüge aus der Endstufe der Besoldungsgruppe A 5 zurückbleiben.

##### § 56  
Erhöhtes Unfallruhegehalt

(1) Erleidet eine Beamtin oder ein Beamter bei Ausübung einer Diensthandlung, mit der eine besondere Lebensgefahr verbunden ist, infolge dieser Gefährdung einen Dienstunfall (qualifizierter Dienstunfall), so sind bei der Bemessung des Unfallruhegehalts 80 Prozent der ruhegehaltfähigen Dienstbezüge aus der Endstufe der übernächsten Besoldungsgruppe zugrunde zu legen, wenn sie oder er infolge dieses Dienstunfalls dauernd dienstunfähig geworden und in den Ruhestand getreten und zum Zeitpunkt des Eintritts in den Ruhestand infolge des Dienstunfalls in der Erwerbsfähigkeit um mindestens 50 Prozent beschränkt ist.
Satz 1 gilt mit der Maßgabe, dass sich für Beamtinnen und Beamte der Laufbahn des Justizwachtmeisterdienstes die ruhegehaltfähigen Dienstbezüge mindestens nach der Besoldungsgruppe A 8, für die übrigen Beamtinnen und Beamten der Laufbahngruppe des mittleren Dienstes mindestens nach der Besoldungsgruppe A 9, für Beamtinnen und Beamte der Laufbahngruppe des gehobenen Dienstes mindestens nach der Besoldungsgruppe A 12 und für Beamtinnen und Beamte der Laufbahngruppe des höheren Dienstes mindestens nach der Besoldungsgruppe A 16 bemessen.

(2) Unfallruhegehalt nach Absatz 1 wird auch gewährt, wenn die Beamtin oder der Beamte

1.  in Ausübung des Dienstes durch einen rechtswidrigen Angriff oder
2.  außerhalb des Dienstes durch einen Angriff im Sinne des § 45 Absatz 4 einen Dienstunfall mit den in Absatz 1 genannten Folgen erleidet.

(3) Unfallruhegehalt nach Absatz 1 wird auch gewährt, wenn eine Beamtin oder ein Beamter einen Einsatzunfall oder ein diesem gleichstehendes Ereignis im Sinne des § 46 erleidet und infolge dessen dienstunfähig geworden ist und in den Ruhestand versetzt wurde und zum Zeitpunkt des Eintritts in den Ruhestand infolge des Einsatzunfalls oder des diesem gleichstehenden Ereignisses in ihrer oder seiner Erwerbsfähigkeit um mindestens 50 Prozent beschränkt ist.

##### § 57  
Unterhaltsbeitrag für frühere Beamtinnen und Beamte sowie frühere Ruhestandsbeamtinnen und Ruhestandsbeamte

(1) Eine frühere Beamtin oder ein früherer Beamter, die oder der durch einen Dienstunfall verletzt wurde und deren oder dessen Beamtenverhältnis nicht durch Eintritt oder Versetzung in den Ruhestand geendet hat, erhält neben dem Heilverfahren (§§ 52, 53) für die Dauer einer durch den Dienstunfall verursachten Erwerbsbeschränkung einen Unterhaltsbeitrag.

(2) Der Unterhaltsbeitrag beträgt

1.  bei voller Erwerbsunfähigkeit 63,78 Prozent der ruhegehaltfähigen Dienstbezüge nach Absatz 4,
2.  bei Minderung der Erwerbsfähigkeit um wenigstens 20 Prozent den diesem Grad entsprechenden Teil des Unterhaltsbeitrags nach Nummer 1.

(3) In den Fällen des Absatzes 2 Nummer 2 kann der Unterhaltsbeitrag, solange die oder der Verletzte aus Anlass des Unfalls unverschuldet arbeitslos ist, bis auf den Betrag nach Absatz 2 Nummer 1 erhöht werden.
Bei Hilflosigkeit der oder des Verletzten gilt § 53 entsprechend.

(4) Die ruhegehaltfähigen Dienstbezüge bestimmen sich nach § 13 Absatz 1.
Bei einer früheren Beamtin auf Widerruf im Vorbereitungsdienst oder einem früheren Beamten auf Widerruf im Vorbereitungsdienst sind die Dienstbezüge zugrunde zu legen, die sie oder er bei der Ernennung zur Beamtin auf Probe oder zum Beamten auf Probe zuerst erhalten hätte; das Gleiche gilt bei einer früheren Beamtin des Polizeivollzugsdienstes auf Widerruf oder einem früheren Beamten des Polizeivollzugsdienstes auf Widerruf mit Dienstbezügen.
Ist die Beamtin oder der Beamte wegen Dienstunfähigkeit infolge des Dienstunfalls entlassen worden, gilt § 13 Absatz 2 entsprechend.

(5) Ist die Beamtin oder der Beamte wegen Dienstunfähigkeit infolge des Dienstunfalls entlassen worden, darf der Unterhaltsbeitrag nach Absatz 2 Nummer 1 nicht hinter dem Mindestunfallruhegehalt (§ 55 Absatz 3 Satz 3) zurückbleiben.
Ist die Beamtin oder der Beamte wegen Dienstunfähigkeit infolge eines Dienstunfalls der in § 56 bezeichneten Art entlassen worden und war sie oder er zum Zeitpunkt der Entlassung infolge des Dienstunfalls in der Erwerbsfähigkeit um mindestens 50 Prozent beschränkt, treten an die Stelle des Mindestunfallruhegehalts 80 Prozent der ruhegehaltfähigen Dienstbezüge aus der Endstufe der Besoldungsgruppe, die sich bei sinngemäßer Anwendung des § 56 ergibt.

(6) Die Minderung der Erwerbsfähigkeit ist nach der körperlichen Beeinträchtigung im allgemeinen Erwerbsleben zu beurteilen.
Zum Zweck der Nachprüfung des Grades der Minderung der Erwerbsfähigkeit ist die frühere Beamtin oder der frühere Beamte verpflichtet, sich auf Anordnung der obersten Dienstbehörde oder der von ihr bestimmten Stelle durch eine von ihr bestimmte Ärztin oder einen von ihr bestimmten Arzt untersuchen zu lassen.

(7) Die Absätze 1 bis 6 gelten entsprechend für eine frühere Ruhestandsbeamtin oder für einen früheren Ruhestandsbeamten, die oder der durch einen Dienstunfall verletzt wurde und die Rechte als Ruhestandsbeamtin oder als Ruhestandsbeamter verloren hat oder das Ruhegehalt aberkannt wurde.

##### § 58  
Unterhaltsbeitrag bei Schädigung eines ungeborenen Kindes

(1) Der Unterhaltsbeitrag wird in den Fällen des § 44 Absatz 1 Satz 2 und 3 für die Dauer der durch einen Dienstunfall der Mutter verursachten Minderung der Erwerbsfähigkeit gewährt

1.  bei Verlust der Erwerbsfähigkeit in Höhe des Mindestunfallwaisengeldes nach § 60 Absatz 1 Satz 2 in Verbindung mit § 55 Absatz 3 Satz 3,
2.  bei Minderung der Erwerbsfähigkeit um mindestens 20 Prozent in Höhe eines der Minderung der Erwerbsfähigkeit entsprechenden Teils des Unterhaltsbeitrags nach Nummer 1.

(2) § 57 Absatz 6 gilt entsprechend.
Bei Minderjährigen wird die Minderung der Erwerbsfähigkeit nach den Auswirkungen bemessen, die sich bei Erwachsenen mit gleichem Gesundheitsschaden ergeben würden.
Die Sorgeberechtigten sind verpflichtet, Untersuchungen zu ermöglichen.

(3) Der Unterhaltsbeitrag beträgt vor Vollendung des 14.
Lebensjahres 30 Prozent, vor Vollendung des 18.
Lebensjahres 50 Prozent der Sätze nach Absatz 1.

(4) Der Anspruch auf Unterhaltsbeitrag ruht insoweit, als während einer Heimpflege von mehr als einem Kalendermonat Pflegekosten gemäß § 53 Absatz 2 erstattet werden.

(5) Hat eine Unterhaltsbeitragsberechtigte oder ein Unterhaltsbeitragsberechtigter Anspruch auf Waisengeld nach diesem Gesetz, wird nur der höhere Versorgungsbezug gezahlt.

##### § 59  
Unfallsterbegeld

Stirbt die verletzte Beamtin oder der verletzte Beamte an den Folgen des Dienstunfalls im Sinne des § 45, so wird anstelle des Sterbegeldes nach § 33 ein Unfallsterbegeld gewährt.
Das Unfallsterbegeld beträgt das Dreifache der laufenden monatlichen Bezüge der oder des Verstorbenen ausschließlich der Auslandsbesoldung und der Vergütungen, mindestens aber 8 000 Euro.
Im Übrigen gilt § 33 entsprechend.

##### § 60  
Unfall-Hinterbliebenenversorgung

(1) Ist eine Beamtin, ein Beamter, eine Ruhestandsbeamtin oder ein Ruhestandsbeamter an den Folgen eines Dienstunfalls verstorben, richtet sich die Unfall-Hinterbliebenenversorgung nach den Vorschriften der Hinterbliebenenversorgung unter Berücksichtigung des Unfallruhegehalts, soweit nichts Abweichendes bestimmt ist.
Das Waisengeld beträgt für jedes waisengeldberechtigte Kind (§ 38) 30 Prozent des Unfallruhegehalts und wird auch elternlosen Enkelinnen und Enkeln gewährt, deren Unterhalt zur Zeit des Dienstunfalls ganz oder überwiegend durch die Verstorbene oder den Verstorbenen bestritten wurde.
In den Fällen des § 37 wird keine Unfall-Hinterbliebenenversorgung gewährt.

(2) Ist eine Ruhestandsbeamtin oder ein Ruhestandsbeamter, die oder der Unfallruhegehalt bezog, nicht an den Folgen des Dienstunfalls verstorben, so steht den Hinterbliebenen nur Versorgung nach Abschnitt 2 Unterabschnitt 2 (§§ 30 bis 43) zu.
Die Bezüge sind aber unter Zugrundelegung des Unfallruhegehalts zu berechnen.

##### § 61  
Unterhaltsbeitrag für Hinterbliebene und für Verwandte der aufsteigenden Linie

(1) Ist in den Fällen des § 57 die Anspruchsberechtigte oder der Anspruchsberechtigte an den Folgen des Dienstunfalls verstorben, erhält die Witwe, der Witwer, die hinterbliebene eingetragene Lebenspartnerin oder der hinterbliebene eingetragene Lebenspartner für die Dauer von zwei Jahren einen Unterhaltsbeitrag in Höhe des Witwen- oder Witwergeldes, das sich nach den allgemeinen Vorschriften unter Zugrundelegung des Unterhaltsbeitrags nach § 57 Absatz 2 Nummer 1 ergibt.
Abweichend hiervon wird ein Unterhaltsbeitrag gewährt, solange die Witwe oder der Witwer ein Kind der oder des Verstorbenen erzieht.

(2) Der Unterhaltsbeitrag für die Waisen richtet sich nach den allgemeinen Vorschriften unter Zugrundelegung des Unterhaltsbeitrags nach § 57 Absatz 2 Nummer 1.

(3) Verwandten der aufsteigenden Linie, deren Unterhalt zur Zeit des Dienstunfalls ganz oder überwiegend durch die Verstorbene oder durch den Verstorbenen (§ 60 Absatz 1) bestritten wurde, ist für die Dauer der Bedürftigkeit und längstens für die Dauer von zwei Jahren ein Unterhaltsbeitrag von zusammen 30 Prozent des Unfallruhegehalts zu gewähren, mindestens jedoch 40 Prozent des nach § 55 Absatz 3 Satz 3 errechneten Betrags.
Sind mehrere Anspruchsberechtigte vorhanden, so wird der Unterhaltsbeitrag den Eltern vor den Großeltern gewährt; an die Stelle eines verstorbenen Elternteils treten dessen Eltern.

##### § 62  
Höchstgrenzen der Hinterbliebenenversorgung

Die Unfallversorgung der Hinterbliebenen (§§ 60 und 61) darf insgesamt die Bezüge (Unfallruhegehalt oder Unterhaltsbeitrag) nicht übersteigen, die die oder der Verstorbene erhalten hat oder hätte erhalten können.
Abweichend von Satz 1 sind in den Fällen des § 56 als Höchstgrenze mindestens die ruhegehaltfähigen Dienstbezüge aus der Endstufe der übernächsten anstelle der von der oder dem Verstorbenen tatsächlich erreichten Besoldungsgruppe zugrunde zu legen.
§ 40 ist entsprechend anzuwenden.
Der Unfallausgleich (§ 54) sowie der Zuschlag bei Hilflosigkeit (§ 53 Absatz 2) oder bei Arbeitslosigkeit (§ 57 Absatz 3) bleiben sowohl bei der Berechnung des Unterhaltsbeitrags nach § 61 als auch bei der vergleichenden Berechnung nach § 40 außer Betracht.

##### § 63  
Einmalige Unfallentschädigung

(1) Eine Beamtin oder ein Beamter, die oder der einen Dienstunfall der in § 56 bezeichneten Art erleidet, erhält neben einer beamtenrechtlichen Versorgung bei Beendigung des Dienstverhältnisses eine einmalige Unfallentschädigung, wenn von der obersten Dienstbehörde oder der von ihr bestimmten Stelle in diesem Zeitpunkt infolge des Unfalls eine Minderung der Erwerbsfähigkeit von wenigstens 50 Prozent festgestellt wird.
Die Höhe der einmaligen Unfallentschädigung hängt vom Grad der Minderung der Erwerbsfähigkeit ab und beträgt bei einer dauerhaften Minderung der Erwerbsfähigkeit von

1.  mindestens 50 Prozent 50 000 Euro,
2.  mindestens 60 Prozent 60 000 Euro,
3.  mindestens 70 Prozent 70 000 Euro,
4.  mindestens 80 Prozent 80 000 Euro,
5.  mindestens 90 Prozent 90 000 Euro,
6.  100 Prozent 100 000 Euro.

Nach der Feststellung sich ergebende Veränderungen der Minderung der Erwerbsfähigkeit bleiben unberücksichtigt.

(2) Ist eine Beamtin oder ein Beamter an den Folgen eines Dienstunfalls der in § 56 bezeichneten Art verstorben, ohne eine einmalige Unfallentschädigung nach Absatz 1 erhalten zu haben, wird den Hinterbliebenen eine einmalige Unfallentschädigung nach Maßgabe der folgenden Bestimmungen gewährt:

1.  Die Witwe, der Witwer, die hinterbliebene eingetragene Lebenspartnerin oder der hinterbliebene eingetragene Lebenspartner sowie die versorgungsberechtigten Kinder erhalten eine Entschädigung in Höhe von insgesamt 60 000 Euro.
2.  Sind Anspruchsberechtigte im Sinne der Nummer 1 nicht vorhanden, so erhalten die Eltern und die nicht versorgungsberechtigten Kinder eine Entschädigung in Höhe von insgesamt 20 000 Euro.
3.  Sind Anspruchsberechtigte im Sinne der Nummern 1 und 2 nicht vorhanden, so erhalten die Großeltern, Enkelinnen und Enkel eine Entschädigung in Höhe von insgesamt 10 000 Euro.

(3) Die Absätze 1 und 2 gelten entsprechend, wenn eine Beamtin oder ein Beamter, die oder der

1.  als Angehörige oder Angehöriger des besonders gefährdeten fliegenden Personals während des Flugdienstes,
2.  als Helm- oder Schwimmtaucherin oder Helm- oder Schwimmtaucher während des besonders gefährlichen Tauchdienstes,
3.  im Bergrettungsdienst während des Einsatzes und der Ausbildung oder
4.  als Angehörige oder Angehöriger des besonders gefährdeten Munitionsuntersuchungspersonals während des dienstlichen Umgangs mit Munition oder
5.  als Angehörige oder Angehöriger eines Polizeiverbands bei einer besonders gefährlichen Diensthandlung im Einsatz oder in der Ausbildung dazu oder
6.  im Einsatz beim Ein- und Aushängen von Außenlasten bei einem Drehflügelflugzeug

einen Unfall erleidet, der nur auf die eigentümlichen Verhältnisse des Dienstes nach den Nummern 1 bis 6 zurückzuführen ist.
Die Landesregierung bestimmt durch Rechtsverordnung den Personenkreis des Satzes 1 und die zum Dienst im Sinne des Satzes 1 gehörenden dienstlichen Verrichtungen.
Die Sätze 1 und 2 gelten entsprechend für andere Angehörige des öffentlichen Dienstes, zu deren Dienstobliegenheiten Tätigkeiten der in Satz 1 Nummer 1 bis 6 bezeichneten Art gehören.

(4) Absatz 1 gilt entsprechend, wenn eine Beamtin oder ein Beamter oder eine andere Angehörige oder ein anderer Angehöriger des öffentlichen Dienstes einen Einsatzunfall oder ein diesem gleichstehendes Ereignis im Sinne des § 46 erleidet.

(5) Die Hinterbliebenen erhalten eine einmalige Entschädigung nach Maßgabe des Absatzes 2, wenn eine Beamtin oder ein Beamter oder eine andere Angehörige oder ein anderer Angehöriger des öffentlichen Dienstes an den Folgen eines Einsatzunfalls oder eines diesem gleichstehenden Ereignisses im Sinne des § 46 verstorben ist.

(6) Für die einmalige Entschädigung nach den Absätzen 4 und 5 gelten § 45 Absatz 5 und § 46 Absatz 4 entsprechend.
Besteht aufgrund derselben Ursache Anspruch sowohl auf eine einmalige Unfallentschädigung nach den Absätzen 1 bis 3 als auch auf eine einmalige Entschädigung nach den Absätzen 4 oder 5, wird nur die einmalige Entschädigung gewährt.

##### § 64  
Ehrenbeamtinnen und Ehrenbeamte

Erleidet eine Ehrenbeamtin oder ein Ehrenbeamter einen Dienstunfall (§ 45), so besteht Anspruch auf ein Heilverfahren (§ 52).
Außerdem kann Ersatz von Sachschäden (§ 50) und ein nach billigem Ermessen von der obersten Dienstbehörde oder der von ihr bestimmten Stelle festzusetzender Unterhaltsbeitrag bewilligt werden.
Das Gleiche gilt für die Hinterbliebenen.

#### Unterabschnitt 4  
Übergangsgeld, Ausgleich, Bezüge bei Verschollenheit

##### § 65  
Übergangsgeld

(1) Beamtinnen oder Beamte mit Dienstbezügen, die nicht auf eigenen Antrag entlassen werden, erhalten als Übergangsgeld nach vollendeter einjähriger Beschäftigungszeit das Einfache und bei längerer Beschäftigungszeit für jedes weitere volle Jahr ihrer Dauer die Hälfte, insgesamt höchstens das Sechsfache der Dienstbezüge (§ 1 Absatz 3 Nummer 1 bis 4 des Brandenburgischen Besoldungsgesetzes) des letzten Monats.
Abweichend von Satz 1 erhalten Juniorprofessorinnen, Juniorprofessoren, Hochschuldozentinnen, Hochschuldozenten, Oberassistentinnen, Oberassistenten, Oberingenieurinnen, Oberingenieure, Wissenschaftliche oder Künstlerische Assistentinnen, Wissenschaftliche oder Künstlerische Assistenten als Übergangsgeld für ein Jahr Dienstzeit das Einfache, insgesamt höchstens das Sechsfache der Dienstbezüge des letzten Monats.
§ 13 Absatz 1 Satz 3 gilt entsprechend.
Das Übergangsgeld wird auch dann gewährt, wenn die Beamtin oder der Beamte zum Zeitpunkt der Entlassung ohne Dienstbezüge beurlaubt war.
Maßgebend sind die Dienstbezüge, die die Beamtin oder der Beamte zum Zeitpunkt der Entlassung erhalten hätte.

(2) Als Beschäftigungszeit gilt die Zeit ununterbrochener hauptberuflicher entgeltlicher Tätigkeit im Dienste desselben Dienstherrn oder der Verwaltung, deren Aufgaben der Dienstherr übernommen hat, sowie in den Fällen der Versetzung die entsprechende Zeit im Dienste des früheren Dienstherrn; die vor einer Beurlaubung ohne Dienstbezüge liegende Beschäftigungszeit wird mit berücksichtigt.
Zeiten mit einer Ermäßigung der regelmäßigen Arbeitszeit sind nur zu dem Teil anzurechnen, der dem Verhältnis der ermäßigten zur regelmäßigen Arbeitszeit entspricht.

(3) Das Übergangsgeld wird nicht gewährt, wenn

1.  die Beamtin oder der Beamte wegen eines Verhaltens im Sinne des § 22 Absatz 1 Nummer 1, § 22 Absatz 2, § 23 Absatz 1 Satz 1 Nummer 1, § 23 Absatz 2 und § 23 Absatz 3 Satz 1 Nummer 1 des Beamtenstatusgesetzes entlassen wird oder
2.  ein Unterhaltsbeitrag nach § 28 bewilligt wird oder
3.  die Beschäftigungszeit als ruhegehaltfähige Dienstzeit angerechnet wird oder
4.  die Beamtin oder der Beamte mit der Berufung in ein Richterinnenverhältnis oder Richterverhältnis oder mit der Ernennung zur Beamtin auf Zeit oder zum Beamten auf Zeit entlassen wird.

(4) Das Übergangsgeld wird in Monatsbeträgen für die der Entlassung folgende Zeit wie die Dienstbezüge gezahlt.
Es ist längstens bis zum Ende des Monats zu zahlen, in dem die Beamtin oder der Beamte die für das Beamtenverhältnis bestimmte gesetzliche Altersgrenze erreicht hat.
Beim Ableben der Empfängerin oder des Empfängers ist der noch nicht ausgezahlte Betrag den Hinterbliebenen in einer Summe zu zahlen.

(5) Bezieht die entlassene Beamtin oder der entlassene Beamte Erwerbs- oder Erwerbsersatzeinkommen im Sinne des § 74 Absatz 5, verringert sich das Übergangsgeld um den Betrag dieser Einkünfte.

##### § 66  
Übergangsgeld für entlassene politische Beamtinnen und Beamte

(1) Eine Beamtin oder ein Beamter, die oder der aus einem Amt im Sinne des § 30 des Beamtenstatusgesetzes nicht auf eigenen Antrag entlassen wird, erhält ein Übergangsgeld in Höhe von 71,75 Prozent der ruhegehaltfähigen Dienstbezüge aus der Endstufe der Besoldungsgruppe, in der sie oder er sich zur Zeit ihrer oder seiner Entlassung befunden hat.
§ 4 des Brandenburgischen Besoldungsgesetzes gilt entsprechend.

(2) Das Übergangsgeld wird für die Dauer der Zeit, die die Beamtin oder der Beamte das Amt, aus dem sie oder er entlassen worden ist, innehatte, mindestens für die Dauer von sechs Monaten, längstens für die Dauer von drei Jahren, gewährt.

(3) § 65 Absatz 3 Nummer 1 bis 4 und Absatz 4 gilt entsprechend.

(4) Bezieht die entlassene Beamtin oder der entlassene Beamte Erwerbs- oder Erwerbsersatzeinkommen im Sinne des § 74 Absatz 5, so verringern sich die in entsprechender Anwendung des § 4 des Brandenburgischen Besoldungsgesetzes fortgezahlten Bezüge und das Übergangsgeld um den Betrag dieser Einkünfte.

##### § 67  
Ausgleich bei besonderen Altersgrenzen

(1) Polizeivollzugsbeamtinnen und Polizeivollzugsbeamte auf Lebenszeit, Beamtinnen und Beamte des Justizvollzugsdienstes und des feuerwehrtechnischen Dienstes, die vor dem Erreichen der für Beamtinnen und Beamte auf Lebenszeit geltenden Regelaltersgrenze (§ 45 Absatz 1 des Landesbeamtengesetzes) aufgrund einer für sie geltenden besonderen Altersgrenze in den Ruhestand treten, erhalten neben dem Ruhegehalt einen Ausgleich in Höhe des Fünffachen der Dienstbezüge (§ 1 des Brandenburgischen Besoldungsgesetzes) des letzten Monats, jedoch nicht über 4 091 Euro.
Dieser Betrag verringert sich für die Beamtinnen und Beamten, die

1.  nach § 110 Absatz 1 Satz 1 Nummer 1 des Landesbeamtengesetzes in den Ruhestand treten um jeweils ein Fünftel,
2.  nach § 110 Absatz 1 Satz 1 Nummer 2 des Landesbeamtengesetzes in den Ruhestand treten um jeweils ein Drittel,
3.  nach § 110 Absatz 1 Satz 1 Nummer 3 des Landesbeamtengesetzes in den Ruhestand treten um jeweils die Hälfte,

für jedes Jahr, das über die jeweils geltende besondere Altersgrenze hinaus abgeleistet wird.
§ 13 Absatz 1 Satz 3 gilt entsprechend.
Der Ausgleich ist bei Eintritt in den Ruhestand in einer Summe zu zahlen.
Der Ausgleich wird nicht neben einer einmaligen Unfallentschädigung im Sinne des § 63 gewährt.

(2) Schwebt zum Zeitpunkt des Eintritts in den Ruhestand gegen die Beamtin oder den Beamten ein Verfahren auf Rücknahme der Ernennung oder ein Verfahren, das nach § 24 des Beamtenstatusgesetzes zum Verlust der Beamtenrechte führen könnte, oder ist gegen die Beamtin oder den Beamten Disziplinarklage erhoben worden, darf der Ausgleich erst nach dem rechtskräftigen Abschluss des Verfahrens und nur gewährt werden, wenn kein Verlust der Versorgungsbezüge eingetreten ist.
Die disziplinarrechtlichen Vorschriften bleiben unberührt.

(3) Der Ausgleich wird im Falle der Bewilligung von Urlaub bis zum Eintritt in den Ruhestand nach § 79 Absatz 1 Nummer 2 zweite Alternative des Landesbeamtengesetzes nicht gewährt.

##### § 68  
Bezüge bei Verschollenheit

(1) Verschollene Beamtinnen, Beamte, Ruhestandsbeamtinnen, Ruhestandsbeamte, sonstige Versorgungsempfängerinnen und Versorgungsempfänger erhalten die zustehenden Bezüge bis zum Ablauf des Monats, in dem die oberste Dienstbehörde oder die von ihr bestimmte Stelle feststellt, dass das Ableben mit Wahrscheinlichkeit anzunehmen ist.

(2) Mit Beginn des Folgemonats erhalten die Personen, die im Fall des Todes der oder des Verschollenen Witwen-, Witwer- oder Waisengeld erhalten würden oder einen Unterhaltsbeitrag erhalten könnten, diese Bezüge.
Die §§ 32 und 33 gelten nicht.

(3) Kehrt die oder der Verschollene zurück, so lebt der Anspruch auf Bezüge, soweit nicht besondere gesetzliche Gründe entgegenstehen, wieder auf.
Nachzahlungen sind längstens für die Dauer eines Jahres zu leisten; die nach Absatz 2 für den gleichen Zeitraum gewährten Bezüge sind anzurechnen.

(4) Ergibt sich, dass bei einer Beamtin oder bei einem Beamten die Voraussetzungen des § 9 des Brandenburgischen Besoldungsgesetzes vorliegen, so können die nach Absatz 2 gezahlten Bezüge von ihr oder ihm zurückgefordert werden.

(5) Wird die oder der Verschollene für tot erklärt oder die Todeszeit gerichtlich festgestellt oder eine Sterbeurkunde über den Tod der oder des Verschollenen ausgestellt, so ist die Hinterbliebenenversorgung ab dem ersten Tag des auf die Rechtskraft der gerichtlichen Entscheidung oder die Ausstellung der Sterbeurkunde folgenden Monats unter Berücksichtigung des festgestellten Todeszeitpunktes neu festzusetzen.

#### Unterabschnitt 5  
Familienbezogene Leistungen

##### § 69  
Familienzuschlag

(1) Auf den Familienzuschlag finden die für Beamtinnen und Beamte geltenden Vorschriften des Brandenburgischen Besoldungsgesetzes Anwendung.

(2) Der Familienzuschlag wird neben dem Ruhegehalt gezahlt.
Er wird unter Berücksichtigung der nach den Verhältnissen der Beamtin, des Beamten, der Ruhestandsbeamtin oder des Ruhestandsbeamten für die Bemessung des Familienzuschlags in Betracht kommenden Kinder neben dem Witwen- oder Witwergeld gezahlt, soweit die Witwe, der Witwer, die hinterbliebene eingetragene Lebenspartnerin oder der hinterbliebene eingetragene Lebenspartner Anspruch auf Kindergeld für diese Kinder hat oder ohne Berücksichtigung der §§ 64 und 65 des Einkommensteuergesetzes oder der §§ 3 und 4 des Bundeskindergeldgesetzes haben würde.
Soweit kein Anspruch nach Satz 2 besteht, wird der Familienzuschlag neben dem Waisengeld gezahlt, wenn die Waise im Familienzuschlag zu berücksichtigen ist oder zu berücksichtigen wäre, wenn die Beamtin, der Beamte, die Ruhestandsbeamtin oder der Ruhestandsbeamte noch lebte.
Sind mehrere Anspruchsberechtigte vorhanden, wird nach der Zahl der auf sie entfallenden Kinder zu gleichen Teilen aufgeteilt.

##### § 70  
Ausgleichsbetrag

Neben dem Waisengeld wird ein Ausgleichsbetrag gezahlt, der dem Betrag für das erste Kind nach § 66 Absatz 1 des Einkommensteuergesetzes entspricht, wenn in der Person der Waise die Voraussetzungen des § 32 Absatz 1 bis 5 des Einkommensteuergesetzes erfüllt sind, Ausschlussgründe nach § 65 des Einkommensteuergesetzes nicht vorliegen, keine Person vorhanden ist, die nach § 62 des Einkommensteuergesetzes oder nach § 1 des Bundeskindergeldgesetzes anspruchsberechtigt ist, und die Waise keinen Anspruch auf Kindergeld nach § 1 Absatz 2 des Bundeskindergeldgesetzes hat.
Der Ausgleichsbetrag gilt für die Anwendung der §§ 74 und 75 nicht als Versorgungsbezug.
Besteht Anspruch auf mehrere Waisengelder, wird der Ausgleichsbetrag nur neben den neuen Versorgungsbezügen gezahlt.

##### § 71  
Kindererziehungszuschlag

(1) Hat eine Beamtin oder ein Beamter ein nach dem 31.
Dezember 1991 geborenes Kind erzogen, erhöht sich das Ruhegehalt für jeden Monat einer ihr oder ihm zuzuordnenden Kindererziehungszeit um einen Kindererziehungszuschlag.
Dies gilt nicht, wenn die Beamtin oder der Beamte wegen der Erziehung des Kindes in der gesetzlichen Rentenversicherung versicherungspflichtig (§ 3 Satz 1 Nummer 1 des Sechsten Buches Sozialgesetzbuch) war und die allgemeine Wartezeit für eine Rente der gesetzlichen Rentenversicherung erfüllt ist.

(2) Die Kindererziehungszeit beginnt nach Ablauf des Monats der Geburt und endet nach 36 Kalendermonaten, spätestens mit dem Ablauf des Monats, in dem die Erziehung endet.
Wird während dieses Zeitraums ein weiteres Kind erzogen, für das der Beamtin oder dem Beamten eine Kindererziehungszeit zuzuordnen ist, wird die Kindererziehungszeit für dieses und jedes weitere Kind um die Anzahl der Kalendermonate der gleichzeitigen Erziehung verlängert.

(3) Für die Zuordnung der Kindererziehungszeit zu einem Elternteil (§ 56 Absatz 1 Satz 1 Nummer 3 und Absatz 3 Nummer 2 und 3 des Ersten Buches Sozialgesetzbuch) gilt § 56 Absatz 2 des Sechsten Buches Sozialgesetzbuch entsprechend.

(4) Der Kindererziehungszuschlag beträgt für jeden Monat der Kindererziehung ab 1. Januar 2019 2,84 Euro, ab 1.
Januar 2020 2,95 Euro und ab 1.
Januar 2021 2,99 Euro.
Er darf zusammen mit dem auf die Kindererziehungszeit entfallenden Anteil des Ruhegehalts das Ruhegehalt nicht übersteigen, das sich bei Berücksichtigung des Zeitraums der Kindererziehung als ruhegehaltfähige Dienstzeit für diesen Zeitraum ergeben würde.
Bei der Höchstgrenzenberechnung nach Satz 2 ist der Gesamtzeitraum der Kindererziehung nach Absatz 1 zu betrachten.

(5) Das um den Kindererziehungszuschlag erhöhte Ruhegehalt darf nicht höher sein als das Ruhegehalt, das sich unter Berücksichtigung des Höchstruhegehaltssatzes und der ruhegehaltfähigen Bezüge aus der Endstufe der Besoldungsgruppe, aus der sich das Ruhegehalt berechnet, ergeben würde.

(6) Der Kindererziehungszuschlag erhöht das nach § 25 Absatz 1, § 27 Absatz 1 und § 55 Absatz 3 Satz 1 berechnete Ruhegehalt.
Für die Anwendung des § 25 Absatz 2 sowie von Ruhens-, Kürzungs- und Anrechnungsvorschriften gilt der Kindererziehungszuschlag als Teil des Ruhegehalts.

(7) Hat eine Beamtin oder ein Beamter vor der Berufung in ein Beamtenverhältnis ein vor dem 1. Januar 1992 geborenes Kind erzogen, gelten die Absätze 1 bis 5 entsprechend mit der Maßgabe, dass die Kindererziehungszeit zwölf Kalendermonate nach Ablauf des Monats der Geburt endet.
Die §§ 249 und 249a des Sechsten Buches Sozialgesetzbuch gelten entsprechend.

(8) Hat eine Beamtin oder ein Beamter nach der Berufung in das Beamtenverhältnis ein bis zum 31. Dezember 1991 geborenes Kind erzogen, gelten die Absätze 1 bis 5 entsprechend mit der Maßgabe, dass die Kindererziehungszeit zwölf Kalendermonate nach Ablauf des Monats der Geburt endet.
Die §§ 249 und 249a des Sechsten Buches Sozialgesetzbuch gelten entsprechend.

##### § 72  
Pflegezuschlag

(1) War eine Beamtin oder ein Beamter wegen nicht erwerbsmäßiger Pflege von Pflegebedürftigen nach § 3 Satz 1 Nummer 1a des Sechsten Buches Sozialgesetzbuch versicherungspflichtig, wird für die Zeit der Pflege ein Pflegezuschlag zum Ruhegehalt ab 1.
Januar 2019 in Höhe von 2,16 Euro, ab 1. Januar 2020 in Höhe von 2,24 Euro und ab 1.
Januar 2021 in Höhe von 2,27 Euro für jeden Monat der Pflege gezahlt.
Dies gilt nicht, wenn die allgemeine Wartezeit in der gesetzlichen Rentenversicherung erfüllt ist.

(2) § 71 Absatz 4 Satz 2 gilt entsprechend mit der Maßgabe, dass für gleiche Zeiträume zustehende Kindererziehungszuschläge einzubeziehen sind; § 71 Absatz 5 und 6 gilt entsprechend.

##### § 73  
Vorübergehende Gewährung von Zuschlägen

(1) Versorgungsempfängerinnen und Versorgungsempfänger, die vor Erreichen der Regelaltersgrenze nach § 45 Absatz 1 des Landesbeamtengesetzes in den Ruhestand versetzt wurden oder in den Ruhestand getreten sind, erhalten vorübergehend Leistungen entsprechend den §§ 71 und 72, wenn

1.  bis zum Beginn des Ruhestandes die allgemeine Wartezeit von fünf Jahren für eine Rente der gesetzlichen Rentenversicherung erfüllt ist,
2.  sie in den Ruhestand
    1.  wegen Dienstunfähigkeit im Sinne des § 26 Absatz 1 des Beamtenstatusgesetzes in den Ruhestand versetzt worden sind,
    2.  wegen Erreichens einer besonderen Altersgrenze nach den §§ 110 Absatz 1 bis 5, 117 und 118 des Landesbeamtengesetzes in den Ruhestand getreten sind oder
    3.  vor Erreichen einer besonderen Altersgrenze nach den §§ 110 Absatz 1 bis 5, 117 und 118 des Landesbeamtengesetzes auf Antrag in den Ruhestand getreten sind, ab dem Zeitpunkt, zu dem sie oder er wegen Erreichens einer besonderen Altersgrenze in den Ruhestand getreten wäre,
3.  entsprechende Leistungen nach dem Sechsten Buch Sozialgesetzbuch dem Grunde nach zustehen, jedoch vor dem Erreichen der maßgebenden Altersgrenze noch nicht gewährt werden,
4.  sie einen Ruhegehaltssatz von 66,97 Prozent noch nicht erreicht haben und
5.  keine Einkünfte im Sinne des § 74 Absatz 5 bezogen werden; Einkünfte bleiben außer Betracht, soweit sie durchschnittlich im Monat 470 Euro nicht überschreiten.

Durch die Leistung nach Satz 1 darf der Betrag nicht überschritten werden, der sich bei Berechnung des Ruhegehalts mit einem Ruhegehaltssatz von 66,97 Prozent ergibt.

(2) Die Leistung entfällt spätestens mit Ablauf des Monats, in dem die Versorgungsempfängerin oder der Versorgungsempfänger die Regelaltersgrenze nach § 35 oder § 235 des Sechsten Buches Sozialgesetzbuch erreicht.
Sie endet vorher, wenn die Versorgungsempfängerin oder der Versorgungsempfänger

1.  eine Versichertenrente der gesetzlichen Rentenversicherung bezieht, mit Ablauf des Tages vor dem Beginn der Rente, oder
2.  ein Erwerbseinkommen über durchschnittlich im Monat 470 Euro hinaus bezieht, mit Ablauf des Tages vor dem Beginn der Erwerbstätigkeit.

(3) Die Leistung wird auf Antrag gewährt.
Anträge, die innerhalb von drei Monaten nach dem Eintritt oder der Versetzung in den Ruhestand gestellt werden, gelten als zum Zeitpunkt des Ruhestandseintritts oder der Ruhestandsversetzung gestellt.
Wird der Antrag zu einem späteren Zeitpunkt gestellt, so wird die Leistung vom Beginn des Antragsmonats an gewährt.

### Abschnitt 3  
Anrechnungs-, Kürzungs- und Ruhensvorschriften

##### § 74  
Zusammentreffen von Versorgungsbezügen mit Erwerbs- und Erwerbsersatzeinkommen

(1) Beziehen Versorgungsberechtigte Erwerbs- oder Erwerbsersatzeinkommen (Absatz 5), werden daneben Versorgungsbezüge nur bis zum Erreichen der in Absatz 2 bezeichneten Höchstgrenze gezahlt.
Satz 1 ist nicht auf Empfängerinnen und Empfänger von Waisengeld anzuwenden.

(2) Als Höchstgrenze gelten

1.  für Ruhestandsbeamtinnen, Ruhestandsbeamte, Witwen, Witwer, hinterbliebene eingetragene Lebenspartnerinnen und Lebenspartner die ruhegehaltfähigen Dienstbezüge aus der Endstufe der Besoldungsgruppe, aus der sich das Ruhegehalt berechnet, mindestens ein Betrag in Höhe des Eineinhalbfachen der jeweils ruhegehaltfähigen Dienstbezüge aus der Endstufe der Besoldungsgruppe A 5, zuzüglich des jeweils zustehenden Familienzuschlags nach § 69 Absatz 2,
2.  für Ruhestandsbeamtinnen und Ruhestandsbeamte, die wegen Dienstunfähigkeit, die nicht auf einem Dienstunfall beruht, oder nach § 46 Absatz 1 Satz 2 des Landesbeamtengesetzes in den Ruhestand getreten sind, bis zum Ablauf des Monats, in dem die Altersgrenze nach § 45 Absatz 1 des Landesbeamtengesetzes erreicht wird, 71,75 Prozent der ruhegehaltfähigen Dienstbezüge aus der Endstufe der Besoldungsgruppe, aus der sich das Ruhegehalt berechnet, mindestens ein Betrag in Höhe von 71,75 Prozent des Eineinhalbfachen der jeweils ruhegehaltfähigen Dienstbezüge aus der Endstufe der Besoldungsgruppe A 5, zuzüglich des jeweils zustehenden Familienzuschlags nach § 69 Absatz 2 sowie eines Betrags von monatlich 470 Euro.

Abweichend von Satz 1 Nummer 1 gilt bei Ruhestandsbeamtinnen und Ruhestandsbeamten, die wegen Erreichens der für sie geltenden Altersgrenze oder zu einem späteren Zeitpunkt in den Ruhestand getreten sind, eine Höchstgrenze von 130 Prozent der ruhegehaltfähigen Dienstbezüge aus der Endstufe der Besoldungsgruppe, aus der sich das Ruhegehalt berechnet, mindestens ein Betrag in Höhe des Eineinhalbfachen der jeweils ruhegehaltfähigen Dienstbezüge aus der Endstufe der Besoldungsgruppe A 5, zuzüglich des jeweils zustehenden Familienzuschlags nach § 69 Absatz 2.
Die Höchstgrenze nach den Sätzen 1 und 2 ist für den Monat November um den jeweils zustehenden Betrag nach § 48b des Brandenburgischen Besoldungsgesetzes zu erhöhen.

(3) Den Versorgungsberechtigten ist mindestens ein Betrag in Höhe von 20 Prozent des jeweiligen Versorgungsbezugs (§ 4) zu belassen.
Satz 1 gilt nicht beim Bezug von Verwendungseinkommen, das mindestens aus derselben Besoldungsgruppe oder einer vergleichbaren Entgeltgruppe berechnet wird, aus der sich auch die ruhegehaltfähigen Dienstbezüge bestimmen.
Für sonstiges in der Höhe vergleichbares Verwendungseinkommen gelten Satz 2 und Absatz 5 Satz 5 entsprechend.

(4) Bei der Ruhensberechnung für eine frühere Beamtin, einen früheren Beamten, eine frühere Ruhestandsbeamtin oder einen früheren Ruhestandsbeamten, die oder der Anspruch auf Versorgung nach § 57 hat, ist mindestens ein Betrag als Versorgung zu belassen, der unter Berücksichtigung der Minderung der Erwerbsfähigkeit infolge des Dienstunfalls dem Unfallausgleich entspricht.
Dies gilt nicht, wenn wegen desselben Unfalls Grundrente nach dem Bundesversorgungsgesetz zusteht.

(5) Erwerbseinkommen sind Einkünfte aus nichtselbstständiger Arbeit einschließlich Abfindungen, aus selbstständiger Arbeit, aus Gewerbebetrieb und aus Land- und Forstwirtschaft.
Nicht als Erwerbseinkommen gelten steuerfreie Aufwandsentschädigungen, im Rahmen der Einkunftsarten nach Satz 1 anerkannte Betriebsausgaben und Werbungskosten nach dem Einkommensteuergesetz, Jubiläumszuwendungen, ein Unfallausgleich (§ 54), steuerfreie Einnahmen für Leistungen zur Grundpflege oder hauswirtschaftlichen Versorgung sowie Einkünfte aus Tätigkeiten, die nach Art und Umfang Nebentätigkeiten im Sinne des § 86 Absatz 3 Nummer 2 des Landesbeamtengesetzes entsprechen.
Erwerbsersatzeinkommen sind Leistungen, die aufgrund oder in entsprechender Anwendung öffentlich-rechtlicher Vorschriften kurzfristig erbracht werden, um Erwerbseinkommen zu ersetzen.
Die Berücksichtigung des Erwerbs- und des Erwerbsersatzeinkommens erfolgt monatsbezogen.
Wird Einkommen nicht in Monatsbeträgen erzielt, ist das Einkommen des Kalenderjahres, geteilt durch zwölf Kalendermonate, anzusetzen.
Abweichend von Satz 5 werden Einmalzahlungen im Monat ihres Zuflusses berücksichtigt.

(6) Nach Ablauf des Monats, in dem die Versorgungsberechtigte oder der Versorgungsberechtigte die Altersgrenze nach § 45 Absatz 1 des Landesbeamtengesetzes erreicht, gelten die Absätze 1 bis 5 nur für Erwerbseinkommen aus einer Verwendung im öffentlichen Dienst (Verwendungseinkommen).
Dies ist jede Beschäftigung im Dienst von Körperschaften, Anstalten und Stiftungen des deutschen öffentlichen Rechts oder ihrer Verbände; ausgenommen ist die Beschäftigung bei öffentlich-rechtlichen Religionsgemeinschaften oder ihren Verbänden.
Der Verwendung im öffentlichen Dienst steht gleich die Verwendung im öffentlichen Dienst einer zwischenstaatlichen oder überstaatlichen Einrichtung, an der eine Körperschaft oder ein Verband im Sinne des Satzes 2 durch Zahlung von Beiträgen oder Zuschüssen oder in anderer Weise beteiligt ist.
Ob die Voraussetzungen zutreffen, entscheidet auf Antrag der zuständigen Stelle oder der oder des Versorgungsberechtigten das für das Versorgungsrecht zuständige Ministerium oder die von ihm bestimmte Stelle.

##### § 75  
Zusammentreffen mehrerer Versorgungsbezüge

(1) Erhalten aus einer Verwendung im öffentlichen Dienst (§ 74 Absatz 6) an neuen Versorgungsbezügen

1.  Ruhestandsbeamtinnen oder Ruhestandsbeamte Ruhegehalt oder eine ähnliche Versorgung,
2.  Witwen, Witwer, hinterbliebene eingetragene Lebenspartnerinnen und Lebenspartner oder Waisen aus der Verwendung der verstorbenen Beamtin, des verstorbenen Beamten, der verstorbenen Ruhestandsbeamtin oder des verstorbenen Ruhestandsbeamten Witwen- oder Witwergeld, Waisengeld oder eine ähnliche Versorgung,
3.  Witwen, Witwer, hinterbliebene eingetragene Lebenspartnerinnen und Lebenspartner Ruhegehalt oder eine ähnliche Versorgung,

so sind neben den neuen Versorgungsbezügen die früheren Versorgungsbezüge nur bis zum Erreichen der in Absatz 2 bezeichneten Höchstgrenze zu zahlen.
Dabei darf die Gesamtversorgung nicht hinter der früheren Versorgung zurückbleiben.

(2) Als Höchstgrenze gelten

1.  für Ruhestandsbeamtinnen und Ruhestandsbeamte (Absatz 1 Satz 1 Nummer 1) das Ruhegehalt, das sich unter Zugrundelegung der gesamten ruhegehaltfähigen Dienstzeit und der ruhegehaltfähigen Dienstbezüge aus der Endstufe der Besoldungsgruppe, aus der sich das frühere Ruhegehalt berechnet, ergibt, zuzüglich des Familienzuschlags nach § 69 Absatz 2,
2.  für Witwen, Witwer, hinterbliebene eingetragene Lebenspartnerinnen und Lebenspartner und Waisen (Absatz 1 Satz 1 Nummer 2) das Witwen-, Witwer- oder Waisengeld, das sich aus dem Ruhegehalt nach Nummer 1 ergibt, zuzüglich des Familienzuschlags nach § 69 Absatz 2,
3.  für Witwen, Witwer, hinterbliebene eingetragene Lebenspartnerinnen und Lebenspartner (Absatz 1 Satz 1 Nummer 3) 71,75 Prozent, in den Fällen des § 56 Absatz 1 80 Prozent, der ruhegehaltfähigen Dienstbezüge aus der Endstufe der Besoldungsgruppe, aus der sich das dem Witwen- oder Witwergeld zugrunde liegende Ruhegehalt bemisst, zuzüglich des Familienzuschlags nach § 69 Absatz 2.

Ist bei einem an der Ruhensregelung nach Satz 1 Nummer 1 oder Nummer 2 beteiligten Versorgungsbezug das Ruhegehalt nach § 25 Absatz 2 gemindert, ist das für die Höchstgrenze maßgebende Ruhegehalt in sinngemäßer Anwendung dieser Vorschrift festzusetzen.
Ist bei der Ruhensregelung nach Satz 1 Nummer 3 das dem Witwen- oder Witwergeld zugrunde liegende Ruhegehalt nach § 25 Absatz 2 gemindert, ist die Höchstgrenze entsprechend dieser Vorschrift zu berechnen, wobei dem zu vermindernden Ruhegehalt mindestens ein Ruhegehaltssatz von 71,75 Prozent zugrunde zu legen ist.

(3) In den Fällen des Absatzes 1 Satz 1 Nummer 3 ist neben dem neuen Versorgungsbezug mindestens ein Betrag in Höhe von 20 Prozent des früheren Versorgungsbezugs zu belassen.

(4) Erwerben Ruhestandsbeamtinnen oder Ruhestandsbeamte einen Anspruch auf Witwen- oder Witwergeld oder eine ähnliche Versorgung, so erhalten sie daneben ihr Ruhegehalt zuzüglich des Familienzuschlags nach § 69 Absatz 2 nur bis zum Erreichen der in Absatz 2 Satz 1 Nummer 3 sowie Satz 3 bezeichneten Höchstgrenze.
Die Gesamtbezüge dürfen nicht hinter ihrem Ruhegehalt zuzüglich des Familienzuschlags nach § 69 Absatz 2 sowie eines Betrags in Höhe von 20 Prozent des neuen Versorgungsbezugs zurückbleiben.

(5) § 74 Absatz 4 gilt entsprechend.

##### § 76  
Zusammentreffen von Versorgungsbezügen mit Renten

(1) Versorgungsbezüge werden neben Renten nur bis zum Erreichen der in Absatz 2 bezeichneten Höchstgrenze gezahlt.
Als Renten gelten

1.  Renten aus den gesetzlichen Rentenversicherungen,
2.  Renten aus einer zusätzlichen Alters- oder Hinterbliebenenversorgung für Angehörige des öffentlichen Dienstes,
3.  Renten aus der gesetzlichen Unfallversicherung, wobei ein dem Unfallausgleich (§ 54) entsprechender Betrag unberücksichtigt bleibt; bei einer Minderung der Erwerbsfähigkeit von 20 Prozent bleiben zwei Drittel der Mindestgrundrente nach dem Bundesversorgungsgesetz, bei einer Minderung der Erwerbsfähigkeit von 10 Prozent ein Drittel der Mindestgrundrente nach dem Bundesversorgungsgesetz unberücksichtigt,
4.  Leistungen aus einer berufsständischen Versorgungseinrichtung oder aus einer befreienden Lebensversicherung, zu denen der Arbeitgeber aufgrund eines Beschäftigungsverhältnisses im öffentlichen Dienst mindestens die Hälfte der Beiträge oder Zuschüsse in dieser Höhe geleistet hat,
5.  Betriebsrenten aus einer in der gesetzlichen Rentenversicherung versicherungsfreien Beschäftigung im öffentlichen Dienst mit Anspruch auf Versorgung bei verminderter Erwerbsfähigkeit und im Alter sowie auf Hinterbliebenenversorgung nach beamtenrechtlichen Vorschriften oder Grundsätzen, die nicht unter Nummer 2 fallen,
6.  Leistungen der Altershilfe der Landwirtinnen und Landwirte,
7.  Altersgeld aus einem öffentlich-rechtlichen Dienstverhältnis oder eine dem Altersgeld entsprechende Alterssicherung.

Wird eine Rente im Sinne des Satzes 2 nicht beantragt oder auf sie verzichtet oder wird an deren Stelle eine Kapitalleistung, Beitragserstattung oder Abfindung gezahlt, so tritt an die Stelle der Rente der Betrag, der vom Leistungsträger ansonsten zu zahlen wäre.
Bei Zahlung einer Abfindung, Beitragserstattung oder eines sonstigen Kapitalbetrags ist der sich bei einer Verrentung ergebende Betrag zugrunde zu legen.
Dies gilt nicht, wenn die Ruhestandsbeamtin oder der Ruhestandsbeamte innerhalb von drei Monaten nach Zufluss den Kapitalbetrag zuzüglich der hierauf gewährten Zinsen an den Dienstherrn abführt.
Zu den Renten und den Leistungen nach Satz 2 Nummer 4 rechnet nicht der Kinderzuschuss.
Renten, Rentenerhöhungen und Rentenminderungen, die auf § 1587b des Bürgerlichen Gesetzbuchs oder § 1 des Gesetzes zur Regelung von Härten im Versorgungsausgleich, jeweils in der bis zum 31.
August 2009 geltenden Fassung, beruhen, übertragene Anrechte nach Maßgabe des Versorgungsausgleichsgesetzes vom 3.
April 2009 (BGBl.
I S.
700), das zuletzt durch Artikel 25 des Gesetzes vom 8.
Dezember 2010 (BGBl.
I S.
1768, 1801) geändert worden ist, in der jeweils geltenden Fassung und Zuschläge oder Abschläge beim Rentensplitting unter Ehegatten nach § 76c des Sechsten Buches Sozialgesetzbuch bleiben unberücksichtigt.

(2) Als Höchstgrenze gilt für Ruhestandsbeamtinnen und Ruhestandsbeamte der Betrag, der sich als Ruhegehalt zuzüglich des Familienzuschlags nach § 69 Absatz 2 ergeben würde, wenn der Berechnung zugrunde gelegt werden

1.  bei den ruhegehaltfähigen Dienstbezügen die Endstufe der Besoldungsgruppe, aus der sich das Ruhegehalt berechnet
2.  als ruhegehaltfähige Dienstzeit die Zeit vom vollendeten 17.
    Lebensjahr sowie vor dem 17. Lebensjahr tatsächlich abgeleistete Dienstzeiten und Pflichtbeitragszeiten bis zum Eintritt des Versorgungsfalles.
    Diese Zeit erhöht sich um Zeiten, um die sich die ruhegehaltfähige Dienstzeit erhöht, und die bei der Rente berücksichtigten Zeiten einer rentenversicherungspflichtigen Beschäftigung oder Tätigkeit nach Eintritt des Versorgungsfalles.
    Nicht berücksichtigt werden die in § 64 Absatz 1 des Landesbeamtengesetzes genannten Zeiten mit Ausnahme der Zeiten, die vor diesen Zeiten zurückgelegt wurden.

Für Witwen, Witwer, hinterbliebene eingetragene Lebenspartnerinnen und Lebenspartner gilt als Höchstgrenze der Betrag, der sich als Witwen- oder Witwergeld zuzüglich des Familienzuschlags nach § 69 Absatz 2, für Waisen der Betrag, der sich als Waisengeld zuzüglich des Familienzuschlags nach § 69 Absatz 2, wenn dieser neben dem Waisengeld gezahlt wird, aus dem Ruhegehalt nach Satz 1 Nummer 1 ergeben würde.

Ist bei einem an der Ruhensregelung beteiligten Versorgungsbezug das Ruhegehalt nach § 25 Absatz 2 gemindert, ist das für die Höchstgrenze maßgebende Ruhegehalt in sinngemäßer Anwendung dieser Vorschrift festzusetzen.
Ist bei einem an der Ruhensregelung beteiligten Versorgungsbezug der Ruhegehaltssatz nach § 14 Absatz 1 Satz 1 Halbsatz 2 oder Halbsatz 3 des Beamtenversorgungsgesetzes in der bis zum 31.
Dezember 1991 geltenden Fassung gemindert, ist der für die Höchstgrenze maßgebende Ruhegehaltssatz in sinngemäßer Anwendung dieser Vorschrift festzusetzen.

(3) Als Renten im Sinne des Absatzes 1 gelten nicht

1.  bei Ruhestandsbeamtinnen und Ruhestandsbeamten (Absatz 2 Satz 1) Hinterbliebenenrenten aus einer Beschäftigung oder Tätigkeit der Ehegattin, des Ehegatten, der eingetragenen Lebenspartnerin oder des eingetragenen Lebenspartners,
2.  bei Witwen, Witwer, hinterbliebenen eingetragenen Lebenspartnerinnen und Lebenspartnern und Waisen (Absatz 2 Satz 4) Renten aufgrund einer eigenen Beschäftigung oder Tätigkeit.

(4) Bei Anwendung der Absätze 1 und 2 bleibt außer Ansatz der Teil der Rente (Absatz 1), der

1.  dem Verhältnis der Versicherungsjahre aufgrund freiwilliger Weiterversicherung oder Selbstversicherung zu den gesamten Versicherungsjahren oder, wenn sich die Rente nach Werteinheiten berechnet, dem Verhältnis der Werteinheiten für freiwillige Beiträge zu der Summe der Werteinheiten für freiwillige Beiträge, Pflichtbeiträge, Ersatzzeiten und Ausfallzeiten oder, wenn sich die Rente nach Entgeltpunkten berechnet, dem Verhältnis der Entgeltpunkte für freiwillige Beiträge zu der Summe der Entgeltpunkte für freiwillige Beiträge, Pflichtbeiträge, Ersatzzeiten, Zurechnungszeiten und Anrechnungszeiten entspricht,
2.  auf einer Höherversicherung beruht.

Dies gilt nicht, soweit der Arbeitgeber mindestens die Hälfte der Beiträge oder Zuschüsse in dieser Höhe geleistet hat.

(5) § 74 Absatz 4 gilt entsprechend.

(6) Den in Absatz 1 bezeichneten Renten stehen entsprechende wiederkehrende Geldleistungen gleich, die aufgrund der Zugehörigkeit zu Zusatz- oder Sonderversorgungssystemen der Deutschen Demokratischen Republik geleistet werden oder die von einem ausländischen Versicherungsträger nach einem für die Bundesrepublik Deutschland wirksamen zwischen- oder überstaatlichen Abkommen gewährt werden.

##### § 77  
Zusammentreffen von Versorgungsbezügen mit Versorgung aus zwischenstaatlicher und überstaatlicher Verwendung

(1) Erhält eine Ruhestandsbeamtin oder ein Ruhestandsbeamter aus der Verwendung im öffentlichen Dienst einer zwischenstaatlichen oder überstaatlichen Einrichtung eine Versorgung, ruht das nach diesem Gesetz zustehende Ruhegehalt in Höhe des Betrags, um den die Summe aus der genannten Versorgung und dem nach diesem Gesetz zustehenden Ruhegehalt die in Absatz 2 genannte Höchstgrenze übersteigt, mindestens jedoch in Höhe des Betrags, der einer Minderung des Prozentsatzes von 1,79375 für jedes Jahr im zwischenstaatlichen oder überstaatlichen Dienst entspricht; der Familienzuschlag nach § 69 Absatz 2 ruht in Höhe von 2,39167 Prozent für jedes Jahr im zwischenstaatlichen oder überstaatlichen Dienst.
§ 10 ist entsprechend anzuwenden.
Die Versorgungsbezüge ruhen in voller Höhe, wenn die Ruhestandsbeamtin oder der Ruhestandsbeamte als Invaliditätspension die Höchstversorgung aus dem Amt bei der zwischenstaatlichen oder überstaatlichen Einrichtung erhält.
Bei der Anwendung des Satzes 1 wird die Zeit, in welcher die Beamtin oder der Beamte, ohne ein Amt bei einer zwischenstaatlichen oder überstaatlichen Einrichtung auszuüben, dort einen Anspruch auf Vergütung oder sonstige Entschädigung hat und Ruhegehaltsansprüche erwirbt, als Zeit im zwischenstaatlichen oder überstaatlichen Dienst gerechnet; Entsprechendes gilt für Zeiten nach dem Ausscheiden aus dem Dienst einer zwischenstaatlichen oder überstaatlichen Einrichtung, die dort bei der Berechnung des Ruhegehalts wie Dienstzeiten berücksichtigt werden.

(2) Als Höchstgrenze gelten die in § 75 Absatz 2 bezeichneten Höchstgrenzen sinngemäß, wobei diese im Monat Dezember nicht zu verdoppeln sind; dabei ist als Ruhegehalt dasjenige deutsche Ruhegehalt zugrunde zu legen, das sich unter Einbeziehung der Zeiten einer Verwendung im öffentlichen Dienst einer zwischenstaatlichen oder überstaatlichen Einrichtung als ruhegehaltfähige Dienstzeit und auf der Grundlage der ruhegehaltfähigen Dienstbezüge aus der Endstufe der nächsthöheren Besoldungsgruppe ergibt.

(3) Verzichtet die Beamtin, der Beamte, die Ruhestandsbeamtin oder der Ruhestandsbeamte beim Ausscheiden aus dem öffentlichen Dienst einer zwischenstaatlichen oder überstaatlichen Einrichtung auf eine Versorgung oder wird an deren Stelle eine Abfindung, Beitragserstattung oder ein sonstiger Kapitalbetrag gezahlt, so findet Absatz 1 mit der Maßgabe Anwendung, dass an die Stelle der Versorgung der Betrag tritt, der vom Leistungsträger ansonsten zu zahlen wäre; erfolgt die Zahlung eines Kapitalbetrags, weil kein Anspruch auf laufende Versorgung besteht, so ist der sich bei einer Verrentung des Kapitalbetrags ergebende Betrag zugrunde zu legen.
Satz 1 gilt nicht, wenn die Beamtin, der Beamte, die Ruhestandsbeamtin oder der Ruhestandsbeamte innerhalb eines Jahres nach Beendigung der Verwendung oder der Berufung in das Beamtenverhältnis den Kapitalbetrag zuzüglich der hierauf gewährten Zinsen an den Dienstherrn abführt.

(4) Hat die Beamtin, der Beamte, die Ruhestandsbeamtin oder der Ruhestandsbeamte schon vor dem Ausscheiden aus dem zwischenstaatlichen oder überstaatlichen öffentlichen Dienst unmittelbar oder mittelbar Zahlungen aus dem Kapitalbetrag erhalten oder hat die zwischenstaatliche oder überstaatliche Einrichtung diesen durch Aufrechnung oder in anderer Form verringert, ist die Zahlung nach Absatz 3 in Höhe des ungekürzten Kapitalbetrags zu leisten.

(5) Erhalten die Witwe, der Witwer, die hinterbliebene eingetragene Lebenspartnerin oder der hinterbliebene eingetragene Lebenspartner oder die Waisen einer Beamtin, eines Beamten, einer Ruhestandsbeamtin oder eines Ruhestandsbeamten Hinterbliebenenbezüge von der zwischenstaatlichen oder überstaatlichen Einrichtung, ruht das deutsche Witwen- und Witwergeld sowie Waisengeld in Höhe des Betrags, der sich unter Anwendung der Absätze 1 und 2 nach dem entsprechenden Anteilsatz ergibt.
Absatz 1 Satz 1 Halbsatz 2 und die Absätze 3, 4 und 6 sind entsprechend anzuwenden.

(6) Der Ruhensbetrag darf die von der zwischenstaatlichen oder überstaatlichen Einrichtung gewährte Versorgung nicht übersteigen.
Der Ruhestandsbeamtin oder dem Ruhestandsbeamten ist mindestens ein Betrag in Höhe von 20 Prozent des deutschen Ruhegehalts zu belassen.
Satz 2 gilt nicht, wenn die Unterschreitung der Mindestbelassung darauf beruht, dass

1.  das nach diesem Gesetz zustehende Ruhegehalt in Höhe des Betrags ruht, der einer Minderung des Prozentsatzes um 1,79375 für jedes Jahr im zwischenstaatlichen oder überstaatlichen Dienst entspricht, oder
2.  Absatz 1 Satz 3 anzuwenden ist.

(7) § 74 Absatz 4 gilt entsprechend.

##### § 78  
Zusammentreffen von Versorgungsbezügen mit Entschädigung oder Versorgungsbezügen nach dem Beschluss 2005/684/EG, Euratom

(1) Beziehen Versorgungsberechtigte eine Entschädigung nach Artikel 10 des Beschlusses 2005/684/EG, Euratom des Europäischen Parlaments vom 28.
September 2005 zur Annahme des Abgeordnetenstatuts des Europäischen Parlaments (ABl.
L 262 vom 7.10.2005, S.
1), so werden die Versorgungsbezüge um 50 Prozent, jedoch höchstens um 50 Prozent der Entschädigung gekürzt.

(2) Beziehen Versorgungsberechtigte Versorgungsbezüge nach Artikel 14 bis 17 des Beschlusses 2005/684/EG, Euratom, ruhen die Versorgungsbezüge um 50 Prozent des Betrags, um den sie und die Versorgungsbezüge die Entschädigung nach Artikel 10 des Beschlusses 2005/684/EG, Euratom übersteigen.
Das Übergangsgeld nach Artikel 13 des Beschlusses 2005/684/EG, Euratom zählt zu den Versorgungsbezügen.

##### § 79  
Reihenfolge der Anwendung der Anrechnungs-, Kürzungs- und Ruhensvorschriften

(1) Der Anwendung der §§ 74 bis 78 gehen sonstige Anrechnungs- und Kürzungsvorschriften vor, soweit nichts anderes bestimmt ist.

(2) Beim Zusammentreffen von zwei Versorgungsbezügen mit Erwerbs- oder Erwerbsersatzeinkommen ist zunächst der neuere und dann der frühere Versorgungsbezug nach § 74 zu regeln.
Bei der Regelung des früheren Versorgungsbezugs ist dem Einkommen der nicht ruhende Teil des neueren Versorgungsbezugs hinzuzurechnen.
Die Berechnungsreihenfolge ist umzukehren, soweit dies für die Versorgungsberechtigten günstiger ist.

(3) Beim Zusammentreffen von Versorgungsbezügen mit Erwerbs- oder Erwerbsersatzeinkommen und Renten ist § 74 mit der nach § 76 verbleibenden Gesamtversorgung anzuwenden.

(4) Beim Zusammentreffen von zwei Versorgungsbezügen mit einer Rente ist zunächst der neuere Versorgungsbezug nach § 76 Absatz 1 bis 4 und danach der frühere Versorgungsbezug unter Berücksichtigung des gekürzten neueren Versorgungsbezugs nach § 75 zu regeln.
Der hiernach gekürzte frühere Versorgungsbezug ist unter Berücksichtigung des gekürzten neueren Versorgungsbezugs nach § 76 Absatz 1 bis 4 zu regeln; für die Berechnung der Höchstgrenze nach § 76 Absatz 2 ist hierbei die Zeit bis zum Eintritt des neueren Versorgungsfalls zu berücksichtigen.

(5) Der nach § 77 berechnete Ruhensbetrag ist von den nach Anwendung der §§ 74 bis 76 verbleibenden Versorgungsbezügen abzuziehen.

##### § 80  
Nichtberücksichtigung der Versorgungsbezüge

Werden Versorgungsberechtigte im öffentlichen Dienst verwendet, so sind ihre Bezüge aus dieser Beschäftigung ohne Rücksicht auf die Versorgungsbezüge zu bemessen.
Das Gleiche gilt für eine aufgrund der Beschäftigung zu gewährende Versorgung.

##### § 81  
Kürzung der Versorgungsbezüge wegen Versorgungsausgleich

(1) Sind durch Entscheidung des Familiengerichts

1.  Anwartschaften in einer gesetzlichen Rentenversicherung nach § 1587b Absatz 2 und 3 des Bürgerlichen Gesetzbuchs in der am 31.
    August 2009 geltenden Fassung oder
2.  Anrechte nach dem Versorgungsausgleichsgesetz

übertragen oder rechtskräftig begründet worden, werden die Versorgungsbezüge des Ausgleichsverpflichteten und ihrer oder seiner Hinterbliebenen nach Anwendung von Ruhens-, Kürzungs- und Anrechnungsvorschriften um den nach Absatz 2 oder Absatz 3 berechneten Betrag gekürzt.
Das einer Vollwaise zu gewährende Waisengeld wird nicht gekürzt, wenn nach dem Recht der gesetzlichen Rentenversicherung die Voraussetzungen für die Gewährung einer Waisenrente aus der Versicherung der ausgleichsberechtigten Person nicht erfüllt sind.

(2) Der Kürzungsbetrag für das Ruhegehalt berechnet sich aus dem Monatsbetrag der durch die Entscheidung des Familiengerichts begründeten Anwartschaften oder Anrechte.
Dieser Monatsbetrag erhöht oder vermindert sich um die Prozentsätze der nach dem Ende der Ehezeit oder dem Ende einer eingetragenen Lebenspartnerschaft bis zum Zeitpunkt des Eintritts oder der Versetzung in den Ruhestand eingetretenen Erhöhungen oder Verminderungen der beamtenrechtlichen Versorgungsbezüge, die in festen Beträgen festgesetzt sind.
Vom Zeitpunkt des Eintritts oder der Versetzung in den Ruhestand an, bei einer Ruhestandsbeamtin oder bei einem Ruhestandsbeamten vom Tag nach dem Ende der Ehezeit oder dem Ende einer eingetragenen Lebenspartnerschaft an, erhöht oder vermindert sich der Kürzungsbetrag in dem Verhältnis, in dem sich das Ruhegehalt vor Anwendung von Ruhens-, Kürzungs- und Anrechnungsvorschriften durch Anpassung der Versorgungsbezüge erhöht oder vermindert.

(3) Der Kürzungsbetrag für das Witwen- und Witwergeld sowie Waisengeld berechnet sich aus dem Kürzungsbetrag nach Absatz 2 für das Ruhegehalt, das die Beamtin oder der Beamte erhalten hat oder hätte erhalten können, wenn sie oder er am Todestag in den Ruhestand getreten wäre, nach den Anteilssätzen des Witwen- oder Witwergeldes oder des Waisengeldes.

(4) In den Fällen des Absatzes 1 Satz 2 und § 5 des Gesetzes zur Regelung von Härten im Versorgungsausgleich vom 21.
Februar 1983 (BGBl.
I S.
105), das zuletzt durch Artikel 65 des Gesetzes vom 17.
Dezember 2008 (BGBl.
I S.
2586, 2729) geändert worden ist, steht die Zahlung des Ruhegehalts der verpflichteten Ehegattin, des verpflichteten Ehegatten, der verpflichteten eingetragenen Lebenspartnerin oder des verpflichteten eingetragenen Lebenspartners für den Fall rückwirkender oder erst nachträglich bekannt werdender Rentengewährung an die berechtigte Ehegattin, den berechtigten Ehegatten, die berechtigte eingetragene Lebenspartnerin oder den berechtigten eingetragenen Lebenspartner unter dem Vorbehalt der Rückforderung.

##### § 82  
Abwendung der Kürzung der Versorgungsbezüge

(1) Die Kürzung der Versorgungsbezüge nach § 81 kann von der Beamtin, dem Beamten, der Ruhestandsbeamtin oder dem Ruhestandsbeamten ganz oder teilweise durch Zahlung eines Kapitalbetrags an den Dienstherrn abgewendet werden.

(2) Als voller Kapitalbetrag wird der Betrag angesetzt, der aufgrund der Entscheidung des Familiengerichts zur Begründung der Anwartschaft auf die bestimmte Rente zu leisten gewesen wäre, erhöht oder vermindert um die Prozentsätze der nach dem Tag, an dem die Entscheidung des Familiengerichts ergangen ist, bis zum Tag der Zahlung des Kapitalbetrags eingetretenen Erhöhungen oder Verminderungen der beamtenrechtlichen Versorgungsbezüge, die in festen Beträgen festgesetzt sind.
Vom Zeitpunkt des Eintritts in den Ruhestand an, bei einer Ruhestandsbeamtin oder einem Ruhestandsbeamten von dem Tag an, an dem die Entscheidung des Familiengerichts ergangen ist, erhöht oder vermindert sich der Kapitalbetrag in dem Verhältnis, in dem sich das Ruhegehalt vor Anwendung von Ruhens-, Kürzungs- und Anrechnungsvorschriften durch Anpassung der Versorgungsbezüge erhöht oder vermindert.

(3) Bei teilweiser Zahlung vermindert sich die Kürzung der Versorgungsbezüge in dem entsprechenden Verhältnis; der Betrag der teilweisen Zahlung soll den Monatsbetrag der Dienstbezüge der Beamtin oder des Beamten oder des Ruhegehalts der Ruhestandsbeamtin oder des Ruhestandsbeamten nicht unterschreiten.

(4) Ergeht nach der Scheidung eine Entscheidung zur Abänderung des Wertausgleichs und sind Zahlungen nach Absatz 1 erfolgt, sind im Umfang der Abänderung zu viel gezahlte Beträge unter Anrechnung der nach § 81 anteilig errechneten Kürzungsbeträge zurückzuzahlen.

### Abschnitt 4  
Versorgungslastenteilung bei landesinternen Dienstherrenwechseln

##### § 83  
Dienstherrenwechsel

(1) Für die Versorgungslastenteilung bei einem Dienstherrenwechsel von Beamtinnen und Beamten innerhalb des Geltungsbereiches dieses Gesetzes findet der Versorgungslastenteilungs-Staatsvertrag in Verbindung mit § 1 des Gesetzes zu dem Versorgungslastenteilungs-Staatsvertrag vom 15. Juli 2010 (GVBl.
I Nr.
27) entsprechende Anwendung.

(2) Der Staatsvertrag nach Absatz 1 gilt nicht für Wechsel zwischen Dienstherren, bei dem sowohl der abgebende als auch der aufnehmende Dienstherr Mitglied in der Versorgungskasse beim Kommunalen Versorgungsverband Brandenburg ist.

(3) Ist bei einem Dienstherrenwechsel nach Absatz 1 der aufnehmende Dienstherr Mitglied in der Versorgungskasse beim Kommunalen Versorgungsverband Brandenburg, so hat er die ihm vom abgebenden Dienstherrn gemäß dem Staatsvertrag nach Absatz 1 gezahlte Abfindung an die Versorgungskasse beim Kommunalen Versorgungsverband Brandenburg abzuführen.

(4) Ist bei einem Dienstherrenwechsel nach Absatz 1 der abgebende Dienstherr Mitglied in der Versorgungskasse beim Kommunalen Versorgungsverband Brandenburg, so hat die Versorgungskasse die an den aufnehmenden Dienstherrn gemäß dem Staatsvertrag nach Absatz 1 zu zahlende Abfindung zu tragen.

### Abschnitt 5  
Überleitungs- und Übergangsbestimmungen

##### § 84  
Überleitung vorhandener Versorgungsempfängerinnen und Versorgungsempfänger

Die Rechtsverhältnisse der am 1.
Januar 2014 vorhandenen Versorgungsempfängerinnen und Versorgungsempfänger und deren künftige Hinterbliebenen regeln sich nach dem am 31. Dezember 2013 geltenden Recht mit folgenden Maßgaben:

1.  Für die Neufestsetzung eines vor dem 1.
    Januar 2014 festgesetzten Unterhaltsbeitrags nach § 15 des Beamtenversorgungsgesetzes in der am 31.
    August 2006 geltenden Fassung gilt § 28.
    Die in § 28 genannte Frist beginnt am 1.
    Januar 2014.
2.  § 35 ist auf Versorgungsfälle anzuwenden, die nach dem 31.
    Dezember 2013 eintreten.
3.  Für Witwen und Witwer, die sich nach dem 31.
    Dezember 2013 wieder verheiratet haben, oder für hinterbliebene eingetragene Lebenspartnerinnen und Lebenspartner, die nach dem 31. Dezember 2013 eine neue eingetragene Lebenspartnerschaft begründet haben, ist § 61 Absatz 3 des Beamtenversorgungsgesetzes in der am 31.
    August 2006 geltenden Fassung nicht anzuwenden.
4.  Anstelle der §§ 50a, 50b, 50c, 50d und 50e des Beamtenversorgungsgesetzes in der am 31. August 2006 geltenden Fassung sind die §§ 71, 72 und 73 anzuwenden.
5.  Beim Zusammentreffen von Versorgungsbezügen mit Erwerbs- und Erwerbsersatzeinkommen sind die §§ 26 und 74 anzuwenden.
6.  Ist das Verfahren über den Versorgungsausgleich vor dem 1.
    Januar 2014 eingeleitet worden, wird die Kürzung des im Zeitpunkt der Wirksamkeit der Entscheidung des Familiengerichts zustehenden Ruhegehalts nach § 81 bei am 31.
    Dezember 2013 vorhandenen Versorgungsempfängerinnen und Versorgungsempfängern erst dann vorgenommen, wenn aus der Versicherung der berechtigten Ehegattin, des berechtigten Ehegatten, der berechtigten eingetragenen Lebenspartnerin oder des berechtigten eingetragenen Lebenspartners eine Rente zu gewähren ist.
7.  Verringern sich die Versorgungsbezüge mit Wirkung vom 1.
    Januar 2014 aufgrund der Erhöhung des amtsunabhängigen Mindestruhegehalts infolge der Anrechnung von Renten nach § 2 Nummer 9 der Beamtenversorgungs-Übergangsverordnung und § 14 Absatz 5 des Beamtenversorgungsgesetzes in der am 31.
    August 2006 geltenden Fassung, werden sie auch ab dem 1.
    Januar 2014 mindestens in der Höhe gezahlt, in der sie am 31.
    Dezember 2013 zugestanden haben.

##### § 84a  
Übergangsbestimmung aus Anlass des Wegfalls der Grundgehaltssätze aus der Besoldungsgruppe A 4

(1) Für die Berechnung der amtsunabhängigen Mindestversorgung der am 31. Dezember 2018 vorhandenen Versorgungsempfängerinnen und Versorgungsempfänger ist ab dem 1. Januar 2019 § 25 Absatz 4 Satz 2 maßgeblich.

(2) Bei Versorgungsempfängerinnen und Versorgungsempfängern, deren erdientes Ruhegehalt nach § 25 Absatz 1 sich bis zum 31.
Dezember 2018 aus der Besoldungsgruppe A 4 berechnet hat, tritt bei der Ermittlung der ruhegehaltfähigen Dienstbezüge nach § 13 ab dem 1.
Januar 2019 an die Stelle der jeweiligen Stufe des Grundgehalts der früheren Besoldungsgruppe A 4 die numerisch entsprechende Stufe des Grundgehalts der Besoldungsgruppe A 5.
Lag der Ruhegehaltsberechnung auf der Grundlage der Besoldungsgruppe A 4 eine Amtszulage nach Anlage 8 zum Brandenburgischen Besoldungsgesetz in der bis zum 31.
Dezember 2018 geltenden Fassung zugrunde, bleibt diese unberührt.

(3) Ein am 31.
Dezember 2018 zustehender Ausgleichsbetrag nach § 84 Nummer 7 wird weitergewährt.

(4) Verringern sich die Versorgungsbezüge mit Wirkung vom 1.
Januar 2019 aufgrund des Wegfalls der Grundgehaltssätze in der Besoldungsgruppe A 4 infolge der Anrechnung von Renten nach § 25 Absatz 5 und § 76, werden sie auch ab dem 1.
Januar 2019 mindestens in der Höhe gezahlt, in der sie am 31.
Dezember 2018 zugestanden haben.

##### § 85  
Übergangsregelung für vorhandene Beamtinnen und Beamte

(1) Für Beurlaubungen, die bis zum 31.
Dezember 2013 ausgesprochen und angetreten sind, ist § 6 Absatz 1 Satz 2 Nummer 5 des Beamtenversorgungsgesetzes in der am 31.
August 2006 geltenden Fassung anzuwenden.

(2) Für am 1.
Januar 2014 vorhandene Beamtinnen und Beamte, die bis zum 31.
Dezember 1957 geboren sind, ist bei der Berücksichtigung von Zeiten im privatrechtlichen Arbeitsverhältnis im öffentlichen Dienst als ruhegehaltfähige Dienstzeit § 10 des Beamtenversorgungsgesetzes in der am 31.
August 2006 geltenden Fassung anzuwenden.

(3) Für am 1.
Januar 2014 vorhandene Beamtinnen und Beamte ist bei der Berücksichtigung von Zeiten als Rechtsanwältin oder Rechtsanwalt als ruhegehaltfähige Dienstzeit § 11 des Beamtenversorgungsgesetzes in der am 31.
August 2006 geltenden Fassung anzuwenden.

(4) Für am 1.
Januar 2014 vorhandene Beamtinnen und Beamte und deren künftige Hinterbliebene, die infolge der Dienstunfähigkeit, die auf einem bis zum 31.
Dezember 2013 erlittenen Dienstunfall beruht, in den Ruhestand versetzt werden, sind die §§ 14, 36 und 38 des Beamtenversorgungsgesetzes in der am 31.
Dezember 2001 geltenden Fassung anzuwenden.

(5) Für Versorgungsfälle, die vor dem 1.
Januar 2018 eintreten, gilt anstelle der nach § 19 Absatz 1 höchstens anrechenbaren Zeit einer Hochschulausbildung einschließlich Prüfungszeit folgender Zeitraum bei einem Eintritt des Versorgungsfalles vor dem

1.  1.
    Juli 2014:      1 095 Tage,
2.  1.
    Januar 2015: 1 065 Tage,
3.  1.
    Juli 2015:      1 035 Tage,
4.  1.
    Januar 2016: 1 005 Tage,
5.  1.
    Juli 2016:        975 Tage,
6.  1.
    Januar 2017:   945 Tage,
7.  1.
    Juli 2017:        915 Tage,
8.  1.
    Januar 2018:   885 Tage.

(6) Für Versorgungsfälle, die vor dem 1.
Januar 2015 eintreten, gilt § 13 Absatz 1 Satz 1 mit der Maßgabe, dass auch der Familienzuschlag der Stufe 1 (§ 40 Absatz 1 des Bundesbesoldungsgesetzes in der am 31.
August 2006 geltenden Fassung), der der Beamtin oder dem Beamten zuletzt zugestanden hat, zu den ruhegehaltfähigen Dienstbezügen zählt.
§ 69 Absatz 2 gilt mit der Maßgabe, dass neben dem Ruhegehalt, dem Witwen- oder Witwergeld oder dem Waisengeld nur der Unterschiedsbetrag zwischen dem Familienzuschlag der Stufe 1 und der nach dem Besoldungsrecht in Betracht kommenden Stufe des Familienzuschlags gezahlt wird.

(7) Für Beamtinnen und Beamte, die nach § 133 Absatz 3 Satz 1 des Landesbeamtengesetzes mit dem vollendeten 65.
Lebensjahr in den Ruhestand treten, finden die §§ 26 und 73 entsprechend Anwendung mit der Maßgabe, dass die vorübergehende Erhöhung des Ruhegehaltssatzes 0,5 Prozent der ruhegehaltfähigen Dienstbezüge für je zwölf Kalendermonate beträgt.

(8) Die Zeit der Verwendung einer Beamtin oder eines Beamten aus dem früheren Bundesgebiet zum Zweck der Aufbauhilfe in dem in Artikel 3 des Einigungsvertrages genannten Gebiet bis zum 31. Dezember 1995 wird doppelt als ruhegehaltfähige Dienstzeit berücksichtigt, wenn sie ununterbrochen mindestens ein Jahr gedauert hat.
Satz 1 gilt nicht für eine Verwendung, die nach dem 31. Dezember 1994 begonnen hat.
Die Zeit nach Satz 1 sowie § 22 Absatz 2 und 4 wird nicht doppelt bei der Berechnung des Zeitraumes nach § 25 Absatz 3 berücksichtigt.

##### § 86  
Ausgleichsbetrag für Kommunale Wahlbeamtinnen und Wahlbeamte auf Zeit

(1) Kommunalen Wahlbeamtinnen und Wahlbeamten auf Zeit, die erstmalig vor dem 5.
Mai 1994 bei einem kommunalen Dienstherrn in Brandenburg in ein Beamtenverhältnis auf Zeit berufen wurden und Versorgungsbezüge erhalten, kann für die Zeit der Ausübung des Wahlamtes in einem Angestelltenverhältnis oder einem sonstigen öffentlich-rechtlichen Dienstverhältnis ein monatlicher Ausgleichsbetrag gewährt werden.
Für Amtsdirektorinnen und Amtsdirektoren, die von 1992 bis 1994 im Angestelltenverhältnis beschäftigt waren, gilt die in Satz 1 genannte Stichtagsregelung nicht.

(2) Als Ausgleichsbetrag wird die Differenz zwischen den der Kommunalen Wahlbeamtin auf Zeit oder dem Kommunalen Wahlbeamten auf Zeit zustehenden Versorgungsbezügen und den Versorgungsbezügen gewährt, die sie oder er erhalten hätte, wenn sie oder er unmittelbar mit Dienstantritt in das Beamtenverhältnis auf Zeit berufen worden wäre.

(3) Der nach Absatz 2 fiktiv ermittelte Ruhegehaltssatz vermindert sich beim Zusammentreffen der Versorgung mit einer Rente im Sinne von § 76 um 1,79375 Prozent der ruhegehaltfähigen Dienstbezüge für jedes nach Absatz 2 berücksichtigte Jahr.

(4) Der Ausgleichsbetrag ist Versorgung im Sinne des § 4.

##### § 87  
Regelung zu § 42 Absatz 8 des Brandenburgischen Hochschulgesetzes

Für Professorinnen und Professoren, die nach § 42 Absatz 8 des Brandenburgischen Hochschulgesetzes von ihren amtlichen Pflichten entbunden wurden oder werden (Entpflichtung), und ihre Hinterbliebenen gilt § 91 des Beamtenversorgungsgesetzes in der am 31.
August 2006 geltenden Fassung entsprechend.

##### § 88  
Übergangsregelung zur Anhebung des Ruhestandseintrittsalters

(1) Für Beamtinnen und Beamte, die nach dem 31.
Dezember 2013 nach § 46 Absatz 1 Satz 2 des Landesbeamtengesetzes (Antragsaltersgrenze bei Schwerbehinderung im Sinne des § 2 Absatz 2 des Neunten Buches Sozialgesetzbuch) in den Ruhestand versetzt werden, ist § 25 Absatz 2 Satz 1 Nummer 2 mit folgenden Maßgaben anzuwenden:

1.  An die Stelle der Vollendung des 65.
    Lebensjahres tritt, wenn sie vor dem 1.
    Januar 1954 geboren sind, die Vollendung des 63.
    Lebensjahres.
2.  An die Stelle der Vollendung des 65.
    Lebensjahres tritt, wenn sie nach dem 31. Dezember 1953 und vor dem 1.
    Januar 1969 geboren sind, das Erreichen folgenden Lebensalters:

Geburtsdatum bis

Lebensalter

Jahr

Monat

31. Dezember 1954

63

3

31.
Dezember 1955

63

4

31.
Dezember 1956

63

5

31.
Dezember 1957

63

6

31.
Dezember 1958

63

7

31.
Dezember 1959

63

8

31.
Dezember 1960

63

9

31.
Dezember 1961

63

10

31.
Dezember 1962

63

11

31.
Dezember 1963

64

0

31.
Dezember 1964

64

2

31.
Dezember 1965

64

4

31.
Dezember 1966

64

6

31.
Dezember 1967

64

8

31.
Dezember 1968

64

10.

(2) Für Beamtinnen und Beamte, die nach dem 31.
Dezember 2013 nach § 46 Absatz 1 des Landesbeamtengesetzes in den Ruhestand versetzt werden und denen Altersteilzeit nach § 133 des Landesbeamtengesetzes und § 39 Absatz 7 des Landesbeamtengesetzes in der am 8.
April 2009 geltenden Fassung bewilligt wurde, gilt § 14 Absatz 3 des Beamtenversorgungsgesetzes in der Fassung der Bekanntmachung vom 16.
März 1999 (BGBl.
I S. 322), das zuletzt durch Artikel 6 des Gesetzes vom 19.
Juli 2006 (BGBl.
I S. 1652) geändert worden ist.

(3) Für Beamtinnen und Beamte, die nach dem 31.
Dezember 2013 und vor dem 1.
Januar 2024 wegen Dienstunfähigkeit, die nicht auf einem Dienstunfall beruht, in den Ruhestand versetzt werden, gilt Folgendes:

1.  § 25 Absatz 2 Satz 1 Nummer 3 ist mit der Maßgabe anzuwenden, dass an die Stelle der Vollendung des 65.
    Lebensjahres das Erreichen folgenden Lebensalters tritt:

Zeitpunkt der Versetzung in den Ruhestand vor dem

Lebensalter

Jahr

Monat

1. Februar 2014

63

3

1.
März 2014

63

4

1.
April 2014

63

5

1.
Mai 2014

63

6

1.
Juni 2014

63

7

1.
Januar 2015

63

8

1.
Januar 2016

63

9

1.
Januar 2017

63

10

1.
Januar 2018

63

11

1.
Januar 2019

64

0

1.
Januar 2020

64

2

1.
Januar 2021

64

4

1.
Januar 2022

64

6

1.
Januar 2023

64

8

1.
Januar 2024

64

10.

2.  25 Absatz 3 Satz 1 Nummer 2 gilt mit der Maßgabe, dass an die Stelle der Zahl „40“ die Zahl „35“ tritt.

### Abschnitt 6  
Schlussbestimmungen

##### § 89  
Ermächtigung zum Erlass von Rechtsverordnungen und Verwaltungsvorschriften

(1) Die nach diesem Gesetz den obersten Dienstbehörden zugewiesenen Befugnisse können diese durch Rechtsverordnung auf andere Stellen auch außerhalb ihres Geschäftsbereiches übertragen.
Bei Übertragung auf eine Stelle außerhalb ihres Geschäftsbereiches ist das Einvernehmen der für diese Stelle zuständigen obersten Dienstbehörde erforderlich.
Soweit die oberste Dienstbehörde von der Ermächtigung nach Satz 1 keinen Gebrauch macht, kann die Landesregierung die nach diesem Gesetz den obersten Dienstbehörden zugewiesenen Befugnisse durch Rechtsverordnung auf andere Stellen übertragen.

(2) Soweit dieses Gesetz nichts anderes bestimmt, erlässt das für das Beamtenversorgungsrecht zuständige Ministerium die zur Durchführung des Gesetzes erforderlichen Verwaltungsvorschriften.

##### § 90  
Als Landesrecht weitergeltende Rechts- und Verwaltungsvorschriften

Die zur Ausführung des Beamtenversorgungsgesetzes in der Fassung der Bekanntmachung vom 16. März 1999 (BGBl.
I S.
322, 847, 2033), das zuletzt durch Artikel 6 des Gesetzes vom 19. Juli 2006 (BGBl.
I S.
1652, 1657) geändert worden ist, für Bundesbeamte anwendbaren Rechtsverordnungen und Verwaltungsvorschriften gelten, soweit dieses Gesetz nichts anderes bestimmt, bis zum Erlass entsprechender landesrechtlicher Vorschriften weiter.
Die Beamtenversorgungs-Übergangsverordnung findet keine Anwendung.