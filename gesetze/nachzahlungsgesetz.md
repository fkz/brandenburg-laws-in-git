## Gesetz zur Nachzahlung von Besoldung im Land Brandenburg (Nachzahlungsgesetz)

#### § 1 Nachzahlung der Grundgehälter und Amtszulagen für das Jahr 2004

(1) Beamtinnen, Beamte sowie Richterinnen und Richter, die im Jahr 2004 eine Klage oder einen Widerspruch mit dem Ziel der Feststellung erhoben haben, dass die für dieses Jahr gewährte Besoldung nicht amtsangemessen ist, über deren geltend gemachten Anspruch jedoch noch nicht abschließend entschieden worden ist, erhalten für das Jahr 2004 eine Nachzahlung.
Satz 1 gilt nicht für Anwärterinnen und Anwärter.

(2) Die Höhe des nachzuzahlenden Betrags ergibt sich aus der Differenz zwischen dem Jahresgrundgehalt des Jahres 2003 und dem Jahresgrundgehalt des Jahres 2004, jeweils zuzüglich Amtszulage und Sonderzahlung.
Bei Teilzeitbeschäftigung sind die Dienstbezüge im Sinne des Satzes 1 maßgebend, die bei Vollzeitbeschäftigung zustehen würden; der Ausgleichsbetrag wird im gleichen Verhältnis wie die Arbeitszeit gekürzt; bei Altersteilzeit unter Berücksichtigung des Altersteilzeitzuschlags.

#### § 2 Nachzahlung der Grundgehälter und Amtszulagen für die Jahre 2005 bis 2007

(1) Beamtinnen und Beamte sowie Richterinnen und Richter, die eine Klage oder einen Widerspruch mit dem Ziel der Feststellung erhoben haben, dass die in dem jeweiligen Jahr der Jahre 2005 bis 2007 gewährte Besoldung nicht amtsangemessen ist, über deren geltend gemachten Anspruch jedoch noch nicht abschließend entschieden worden ist, erhalten für das jeweilige Jahr der Jahre 2005 bis 2007 eine Nachzahlung in Höhe des in Absatz 2 bestimmten Prozentsatzes ihrer jeweiligen in diesem Zeitraum gewährten monatlichen Grundgehälter und Amtszulagen.
Satz 1 gilt für das Jahr 2005 nicht für Beamtinnen und Beamte der Besoldungsgruppen A 3 bis A 13, C 1, W 1 und W 2, für das Jahr 2006 nicht für Beamtinnen und Beamte der Besoldungsgruppen A 3 bis A 11 und für das Jahr 2007 nicht für Beamtinnen und Beamte der Besoldungsgruppen A 3 bis A 12 und W 1, solange sie ein Amt dieser Besoldungsgruppe innehatten.
Satz 1 gilt auch nicht für Anwärterinnen und Anwärter.

(2) Die Höhe der Nachzahlung nach Absatz 1 Satz 1 bemisst sich wie folgt:

Jahr

Prozentsatz der Grundgehälter und Amtszulagen

2005

0,46

2006

1,03

2007

0,87

(3) Der Anspruch nach Absatz 1 Satz 1 besteht nicht, soweit die Beamtin, der Beamte, die Richterin oder der Richter den Anspruch nicht in dem Haushaltsjahr, für das die zusätzliche Besoldung verlangt wird, schriftlich gegenüber der nach § 70 Absatz 2 des Brandenburgischen Besoldungsgesetzes bestimmten Stelle geltend gemacht hat.

#### § 3 Nachzahlung der Grundgehälter und Amtszulagen für die Jahre 2008 bis 2014

(1) Beamtinnen und Beamte sowie Richterinnen und Richter, die eine Klage oder einen Widerspruch mit dem Ziel der Feststellung erhoben haben, dass die in dem jeweiligen Jahr der Jahre 2008 bis 2014 gewährte Besoldung nicht amtsangemessen ist, über deren geltend gemachten Anspruch jedoch noch nicht abschließend entschieden worden ist, erhalten für das jeweilige Jahr der Jahre 2008 bis 2014 eine Nachzahlung in Höhe des in Absatz 2 bestimmten Prozentsatzes ihrer jeweiligen in diesem Zeitraum gewährten monatlichen Grundgehälter und Amtszulagen.
Satz 1 gilt für das Jahr 2009 nicht für Beamtinnen und Beamte der Besoldungsgruppen A 3 bis A 5 und für die Jahre 2013 und 2014 nicht für Beamtinnen und Beamte der Besoldungsgruppen W 2 und W 3 jeweils in den Monaten, in denen sie ein Amt dieser Besoldungsgruppe innehatten.
Satz 1 gilt auch nicht für Anwärterinnen und Anwärter.

(2) Die Höhe der Nachzahlung nach Absatz 1 Satz 1 bemisst sich wie folgt:

Jahr

Prozentsatz der Grundgehälter und Amtszulagen

2008

2,83

2009

1,44

2010

3,16

2011

2,03

2012

1,44

2013

1,10

2014

2,81

(3) Der Anspruch nach Absatz 1 Satz 1 besteht nicht, soweit die Beamtin, der Beamte, die Richterin oder der Richter den Anspruch nicht in dem Haushaltsjahr, für das die zusätzliche Besoldung verlangt wird, schriftlich gegenüber der nach § 70 Absatz 2 des Brandenburgischen Besoldungsgesetzes bestimmten Stelle geltend gemacht hat.

(4) Beamtinnen und Beamte der Besoldungsgruppen W 2 und W 3, die im Jahr 2012 eine Klage oder einen Widerspruch mit dem Ziel der Feststellung erhoben haben, dass die gewährte Besoldung in diesem Jahr nicht amtsangemessen ist, über deren geltend gemachten Anspruch jedoch noch nicht abschließend entschieden worden ist, werden unbeschadet des Absatzes 1 Satz 1 so gestellt, als ob ihnen für das Jahr 2012 mindestens Leistungsbezüge in Höhe von 644,30 Euro monatlich im Sinne des § 30 Absatz 2 des Brandenburgischen Besoldungsgesetzes zugestanden hätten.

#### § 4 Versorgungsempfängerinnen und Versorgungsempfänger

Die §§ 1 bis 3 gelten entsprechend für Versorgungsempfängerinnen und Versorgungsempfänger.
Ruhens-, Anrechnungs- und Kürzungsvorschriften nach dem Brandenburgischen Beamtenversorgungsgesetz, § 4 Absatz 2 des Brandenburgischen Besoldungsgesetzes und dem vor dem 1.
Januar 2014 geltenden entsprechenden Bundesrecht sowie Vorschriften über die anteilige Kürzung sind auf Ansprüche nach Satz 1 in Verbindung mit den §§ 1 bis 3 nicht anzuwenden.