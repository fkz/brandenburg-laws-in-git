## Gesetz zu dem Dritten Glücksspieländerungsstaatsvertrag

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem in Berlin am 29.
März 2019 vom Land Brandenburg unterzeichneten Dritten Staatsvertrag zur Änderung des Staatsvertrages zum Glücksspielwesen in Deutschland (Dritter Glücksspieländerungsstaatsvertrag – 3.
GlüÄndStV) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

#### § 2 

Durch dieses Gesetz werden das Grundrecht auf Datenschutz (Artikel 11 Absatz 1 der Verfassung des Landes Brandenburg) und das Grundrecht der Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 3 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem Artikel 2 Absatz 1 Satz 1 am 1.
Januar 2020 in Kraft.
Sollte der Staatsvertrag nach seinem Artikel 2 Absatz 1 Satz 2 gegenstandslos werden, ist dies unverzüglich im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 19.
Juni 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag](/vertraege/ersterglueaendstv_2012)

* * *

### Anlagen

1

[Dritter Staatsvertrag zur Änderung des Staatsvertrages zum Glücksspielwesen in Deutschland (Dritter Glücksspieländerungsstaatsvertrag – 3.
GlüÄndStV)](/br2/sixcms/media.php/68/GVBl_I_45_2019-Anlage.pdf "Dritter Staatsvertrag zur Änderung des Staatsvertrages zum Glücksspielwesen in Deutschland (Dritter Glücksspieländerungsstaatsvertrag – 3. GlüÄndStV)") 279.7 KB