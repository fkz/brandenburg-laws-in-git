## Gesetz über die Errichtung der Kulturstiftung Schloss Wiepersdorf (KSWiepG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Errichtung

(1) Das Land Brandenburg errichtet unter dem Namen Kulturstiftung Schloss Wiepersdorf eine rechtsfähige Stiftung des öffentlichen Rechts.

(2) Sitz der Stiftung ist Wiepersdorf (Teltow-Fläming).

#### § 2 Stiftungszweck

(1) Zweck der Stiftung ist der Betrieb des Künstlerhauses Schloss Wiepersdorf zur Förderung von Stipendiatinnen und Stipendiaten sowie die Pflege von Kunst, Kultur und Wissenschaft.

(2) Die Stiftung hat die Aufgabe, die kulturelle Tradition von Schloss Wiepersdorf als Künstlerhaus und historischen Ort der Epoche der Romantik zu bewahren und fortzuführen.
Dazu gewährt sie insbesondere Künstlerinnen und Künstlern Arbeits- und Aufenthaltsstipendien verbunden mit der Möglichkeit zu interdisziplinärem, überregionalem und internationalem Austausch.

(3) Der Stiftungszweck wird insbesondere verwirklicht durch:

1.  die Durchführung eines interdisziplinären und internationalen Stipendienprogramms,
2.  die Realisierung von öffentlichen Veranstaltungen mit Stipendiatinnen und Stipendiaten sowie mit Gästen,
3.  die Bewahrung der Erinnerung an die Geschichte des Ortes unter Einbeziehung des Bettina und Achim von Arnim-Museums,
4.  die Kooperation mit Dritten und
5.  die Pflege und die Erhaltung von Kulturgütern und Liegenschaften, sofern sie Stiftungseigentum sind.

(4) Die Stiftung verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke im Sinne des Abschnitts „Steuerbegünstigte Zwecke“ der Abgabenordnung.

#### § 3 Stiftungsvermögen

(1) Die Stiftung kann ein Stiftungsvermögen anlegen.
Das Stiftungsvermögen ist in seinem Wert ungeschmälert zu erhalten.

(2) Die Stiftung ist berechtigt, Zustiftungen anzunehmen.
Das Land Brandenburg und Dritte können ihr Liegenschaften übertragen.

(3) Das Stiftungsvermögen darf nur zur Verwirklichung des Stiftungszwecks eingesetzt werden.

#### § 4 Stiftungsmittel

(1) Zur Erfüllung ihrer Aufgaben erhält die Stiftung jährliche Förderungen im Sinne der öffentlichen Haushaltsvorschriften nach Maßgabe des Haushaltsplanes des Landes Brandenburg.

(2) Die Stiftung ist berechtigt, Schenkungen, Erbschaften und Förderungen anzunehmen und zur Erfüllung ihrer Aufgaben zu verwenden, soweit sie nicht dem Stiftungsvermögen zuzuführen sind.

(3) Die Erträge des Stiftungsvermögens sowie sonstige Einnahmen und Erträge sind nur zur Erfüllung des Stiftungszwecks zu verwenden.
Die Stiftung ist selbstlos tätig.
Sie verfolgt nicht in erster Linie eigenwirtschaftliche Zwecke.
Es darf keine Person durch Ausgaben, die dem Zweck der Stiftung fremd sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.

#### § 5 Rechtsaufsicht

Die Rechtsaufsicht über die Stiftung führt die für Kultur zuständige oberste Landesbehörde.

#### § 6 Organe der Stiftung; Kuratorium

(1) Organe der Stiftung sind:

1.  der Stiftungsrat und
2.  der Vorstand (geschäftsführende Direktorin oder geschäftsführender Direktor des Künstlerhauses).

(2) Zur Beratung der Organe in Belangen der Stiftung wird ein Kuratorium gebildet.

#### § 7 Zusammensetzung des Stiftungsrates

(1) Dem Stiftungsrat gehören an:

1.  eine Vertreterin oder ein Vertreter der für Kultur zuständigen obersten Landesbehörde, die oder der nicht zugleich mit der Rechtsaufsicht über die Stiftung befasst ist, als Vorsitzende oder Vorsitzender des Stiftungsrates,
2.  eine Vertreterin oder ein Vertreter der für Finanzen zuständigen obersten Landesbehörde,
3.  ein Mitglied des Landtages Brandenburg und
4.  eine Vertreterin oder ein Vertreter des Landkreises Teltow-Fläming als Stellvertreterin oder Stellvertreter der Vorsitzenden oder des Vorsitzenden des Stiftungsrates.

(2) Das in Absatz 1 Nummer 1 genannte Mitglied wird von der für Kultur zuständigen obersten Landesbehörde entsandt und abberufen.
Das in Absatz 1 Nummer 2 genannte Mitglied wird von der für Finanzen zuständigen obersten Landesbehörde entsandt und abberufen.
Das in Absatz 1 Nummer 3 genannte Mitglied wird vom Landtag entsandt und abberufen.
Das in Absatz 1 Nummer 4 genannte Mitglied wird vom Landkreis Teltow-Fläming entsandt und abberufen.
Für den Fall des Ausscheidens ist durch die entsendende Stelle eine Nachfolgerin oder ein Nachfolger unverzüglich zu berufen.

(3) Das in Absatz 1 Nummer 1 genannte Mitglied erhält zwei Stimmrechte.

(4) Für jedes Mitglied erhält die entsendende Stelle die Möglichkeit, eine Stellvertreterin oder einen Stellvertreter für den Verhinderungsfall zu berufen.
Sind sowohl das Mitglied als auch seine Stellvertreterin oder sein Stellvertreter verhindert, kann die entsendende Stelle eine Bevollmächtigte oder einen Bevollmächtigten entsenden.

(5) Es erfolgt keine gesonderte Vergütung für die Tätigkeit im Stiftungsrat.
Es besteht Anspruch auf Ersatz der notwendigen Aufwendungen nach Maßgabe der für die Verwaltung des Landes Brandenburg geltenden Bestimmungen des Reisekostenrechts.

#### § 8 Aufgaben des Stiftungsrates

(1) Der Stiftungsrat beschließt über die grundsätzlichen Angelegenheiten der Stiftung und legt die wesentlichen Aufgaben und Tätigkeiten der Stiftung fest.
Der Stiftungsrat entscheidet insbesondere über

1.  die Feststellung des Haushaltsplanes und der Finanzplanung,
2.  die Beauftragung der Prüfung des Jahresabschlusses,
3.  die Feststellung des geprüften Jahresabschlusses und die Entlastung des Vorstands,
4.  die Berufung und Abberufung des Vorstands sowie dessen Einstellung und Entlassung,
5.  die Anlagestrategie für das Stiftungsvermögen.

(2) Der Zustimmung des Stiftungsrates bedürfen

1.  die Einstellung aller Beschäftigten ab Entgeltgruppe 13 des Tarifvertrages für den öffentlichen Dienst der Länder,
2.  der Erwerb, die Veräußerung und die Belastung von Grundstücken,
3.  die Annahme von Erbschaften, Schenkungen, Liegenschaften, Förderungen und Zustiftungen und
4.  die Veräußerung von Sammlungsgegenständen; eine Veräußerung ist nur zulässig, wenn mit dem Veräußerungserlös der Sammlungsbestand ergänzt wird oder als Stiftungsvermögen erhalten bleibt.

(3) Der Stiftungsrat erlässt und ändert für die Tätigkeit der Stiftung erforderliche Satzungen nach Maßgabe dieses Gesetzes.
Dies umfasst insbesondere die Stiftungssatzung sowie Satzungen für Betriebe gewerblicher Art mit der Bestimmung, dass der Betrieb nach Maßgabe des Abschnitts „Steuerbegünstigte Zwecke“ der Abgabenordnung ausschließlich und unmittelbar gemeinnützige Zwecke verfolgt.
Satzungen sowie Satzungsänderungen bedürfen der Zustimmung der Rechtsaufsichtsbehörde.

(4) Die Vorsitzende oder der Vorsitzende des Stiftungsrates vertritt die Stiftung gegenüber dem Vorstand.

(5) Der Stiftungsrat kann die Vorsitzende oder den Vorsitzenden beauftragen, über die Zustimmung in bestimmten Bereichen allein zu entscheiden.
Dies gilt nicht für den Erwerb, die Veräußerung und die Belastung von Grundstücken sowie für die Anlage des Stiftungsvermögens.

#### § 9 Verfahren im Stiftungsrat

(1) Der Stiftungsrat tritt mindestens zweimal jährlich zu einer ordentlichen Sitzung zusammen.
Auf Antrag von mindestens zwei Mitgliedern muss der Stiftungsrat zu einer außerordentlichen Sitzung zusammentreten.
Der Stiftungsrat entscheidet in der Regel in seinen Sitzungen.

(2) Der Stiftungsrat ist beschlussfähig, wenn die Mehrheit der Mitglieder anwesend oder gemäß § 7 Absatz 4 vertreten ist, darunter jeweils eine Vertreterin oder ein Vertreter der in § 7 Absatz 1 Nummer 1 und 4 genannten Stellen.
Die Beschlüsse werden mit der Stimmenmehrheit der anwesenden Mitglieder gefasst.

(3) Beschlüsse über den Inhalt der Satzungen können nur mit Zustimmung der Mehrheit der Mitglieder des Stiftungsrates getroffen werden.

(4) In Haushalts- und Personalangelegenheiten können Beschlüsse nicht gegen die Stimmen der Vertreterinnen oder der Vertreter der für Kultur und für Finanzen zuständigen obersten Landesbehörden gefasst werden.

(5) An den Sitzungen des Stiftungsrates nimmt der Vorstand teil.
Weitere Personen können vom Stiftungsrat beratend zu den Stiftungsratssitzungen hinzugezogen werden.

(6) Die oder der Vorsitzende des Kuratoriums oder seine Stellvertreterin oder sein Stellvertreter können an den Sitzungen des Stiftungsrates teilnehmen.

#### § 10 Vorstand

(1) Der Vorstand besteht aus der geschäftsführenden Direktorin oder dem geschäftsführenden Direktor des Künstlerhauses.
Diese oder dieser leitet das Künstlerhaus.
Der Vorstand führt die laufenden Geschäfte der Stiftung nach Maßgabe der Satzung und legt seine Stellvertretung fest.
Der Vorstand ist insbesondere zuständig für

1.  die Aufstellung des Haushaltsplanes und der Finanzplanung,
2.  die Aufstellung des Jahresabschlusses einschließlich einer Vermögensübersicht und die Vorbereitung und Ausführung der Beauftragung der Prüfung des Jahresabschlusses,
3.  die Vorbereitung der Sitzungen und Beschlussfassungen des Stiftungsrates und seiner Beratungsgremien sowie für die Durchführung der Beschlüsse des Stiftungsrates und
4.  die Aufstellung eines Berichts über die Erfüllung des Stiftungszwecks für das Geschäftsjahr.

(2) Der Vorstand vertritt die Stiftung gerichtlich und außergerichtlich.

(3) Das Personal der Stiftung wird vom Vorstand angestellt und entlassen; § 8 Absatz 1 Satz 2 Nummer 4 und Absatz 2 Nummer 1 bleibt unberührt.

#### § 11 Kuratorium

(1) Das Kuratorium besteht aus maximal zehn Mitgliedern, die auf Vorschlag des Vorstands vom Stiftungsrat für die Dauer von fünf Jahren bestellt werden.
Die Wiederbestellung ist zulässig.
Es können Persönlichkeiten, die in den unterschiedlichen Sparten der Kunst und des Kulturlebens tätig sind und als Multiplikatoren in der Gesellschaft wirken, bestellt werden.
Die Persönlichkeiten sollen künstlerische, kulturpolitische und philosophische Expertise haben, so dass sie aufgrund ihres Engagements und ihrer Sachkunde geeignet sind, die Stiftungsorgane in fachlichen Fragen sachkundig zu beraten.

(2) Das Kuratorium wählt für die Dauer der Amtszeit aus seiner Mitte eine Vorsitzende oder einen Vorsitzenden sowie eine stellvertretende Vorsitzende oder einen stellvertretenden Vorsitzenden mit der Mehrheit der abgegebenen Stimmen.
Die Wiederwahl ist zulässig.

(3) Das Kuratorium berät die Organe der Stiftung, insbesondere den Vorstand, in fachlichen Angelegenheiten.
Es gibt Empfehlungen ab.

(4) Das Kuratorium soll mindestens einmal im Jahr zu einer ordentlichen Sitzung zusammentreffen.
Eine außerordentliche Sitzung ist einzuberufen, wenn mindestens ein Drittel der Mitglieder des Kuratoriums oder der Stiftungsrat dies verlangen.
Der Vorstand bereitet die Sitzungen in Abstimmung mit der oder dem Vorsitzenden vor und nimmt an den Sitzungen teil.

(5) Die Mitglieder des Kuratoriums sind ehrenamtlich und unentgeltlich tätig.
Es besteht Anspruch auf Ersatz der notwendigen Aufwendungen nach Maßgabe der für die Verwaltung des Landes Brandenburg geltenden Bestimmungen des Reisekostenrechts.

#### § 12 Personal

Für die Arbeitnehmerinnen und Arbeitnehmer der Stiftung finden der Tarifvertrag für den öffentlichen Dienst der Länder und der Tarifvertrag für Auszubildende der Länder in Ausbildungsberufen nach dem Berufsbildungsgesetz sowie die diese ergänzenden, ändernden oder ersetzenden Tarifverträge in ihrer jeweils für das Land Brandenburg geltenden Fassung Anwendung.

#### § 13 Beteiligung bei der Versorgungsanstalt des Bundes und der Länder

Die Beteiligung bei der Versorgungsanstalt des Bundes und der Länder ist durch die Stiftung unverzüglich nach ihrer Errichtung zu beantragen.
Die Beschäftigten sind nach Maßgabe der Beteiligungsvereinbarung bei der Versorgungsanstalt des Bundes und der Länder zu versichern.

#### § 14 Haushaltsplan, Haushaltsjahr und Rechnungsprüfung

(1) Der Haushaltsplan der Stiftung ist alljährlich rechtzeitig vor Beginn des Haushaltsjahres vom Vorstand im Entwurf aufzustellen und vom Stiftungsrat festzustellen.
Er soll in der Form dem Haushaltsplan des Landes Brandenburg entsprechen und nach den für diesen geltenden Grundsätzen aufgestellt werden.
Er bedarf der Genehmigung der Rechtsaufsichtsbehörde.

(2) Das Haushaltsjahr der Stiftung ist das Kalenderjahr.

(3) Für das Haushalts-, Kassen- und Rechnungswesen sowie für die Rechnungsprüfung der Stiftung finden die für die Verwaltung des Landes Brandenburg geltenden Bestimmungen der Landeshaushaltsordnung entsprechend Anwendung.

#### § 15 Stiftungsauflösung

(1) Die Stiftung kann durch Gesetz aufgelöst werden.

(2) Bei Auflösung der Stiftung fällt sämtliches materielle und immaterielle Vermögen dem Land Brandenburg zu, soweit es von diesem eingebracht worden ist oder per Gesetz auf die Stiftung übertragen worden ist.

(3) Sämtliches nicht vom Land Brandenburg eingebrachte materielle und immaterielle Vermögen fällt bei Auflösung der Stiftung dem Land zu, soweit dem nicht bei Einbringung des Vermögens ausdrücklich von den Einbringenden widersprochen wurde.

(4) Im Übrigen fällt das Vermögen dem Land Brandenburg zu.

(5) Soweit Vermögen dem Land Brandenburg zufällt, ist dieses unmittelbar und ausschließlich für gemeinnützige Zwecke im Sinne des Abschnitts „Steuerbegünstigte Zwecke“ der Abgabenordnung und in einer dem Stiftungszweck möglichst nahe kommenden Weise zu verwenden.

#### § 16 Inkrafttreten

Dieses Gesetz tritt am 1.
Juli 2019 in Kraft.

Potsdam, den 5.
Juni 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark