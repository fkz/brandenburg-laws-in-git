## Gesetz zum Einundzwanzigsten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Einundzwanzigster Rundfunkänderungsstaatsvertrag)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem am 14.
Dezember 2017 vom Land Brandenburg unterzeichneten Einundzwanzigsten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Einundzwanzigster Rundfunkänderungsstaatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

#### § 2 

Durch dieses Gesetz wird das Recht auf informationelle Selbstbestimmung (Artikel 11 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 3 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem Artikel 5 Absatz 2 Satz 1 am 25.
Mai 2018 in Kraft.
Sollte der Staatsvertrag nach seinem Artikel 5 Absatz 2 Satz 2 gegenstandslos werden, ist dies unverzüglich im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 8.
Mai 2018

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag](/de/vertraege-247172) - Rundfunkstaatsvertrag

[zum Staatsvertrag](/vertraege/rbstv) - Rundfunkbeitragsstaatsvertrag

[zum Staatsvertrag](/vertraege/zdf_stv) - ZDF-Staatsvertrag

[zum Staatsvertrag](/vertraege/dlr_stv) - Deutschlandradio-Staatsvertrag

* * *

### Anlagen

1

[Einundzwanzigster Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Einundzwanzigster Rundfunkänderungsstaatsvertrag)](/br2/sixcms/media.php/68/GVBl_I_09_2018-Anlage.pdf "Einundzwanzigster Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Einundzwanzigster Rundfunkänderungsstaatsvertrag)") 305.5 KB