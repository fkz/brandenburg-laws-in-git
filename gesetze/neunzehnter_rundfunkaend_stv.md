## Gesetz zum Neunzehnten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Neunzehnter Rundfunkänderungsstaatsvertrag)

Der Landtag hat das folgende Gesetz beschlossen:

### § 1

Dem am 3.
Dezember 2015 vom Land Brandenburg unterzeichneten Neunzehnten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Neunzehnter Rundfunkänderungsstaatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2

Durch dieses Gesetz wird das Recht auf informationelle Selbstbestimmung (Artikel 11 der Verfassung des Landes Brandenburg) eingeschränkt.

### § 3

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem Artikel 6 Absatz 2 Satz 1 vorbehaltlich des Satzes 2 am 1. Oktober 2016 in Kraft.
Artikel 4 des Staatsvertrages tritt nach Artikel 6 Absatz 2 Satz 2 des Staatsvertrages am 1.
Januar 2017 in Kraft.
Sollte der Staatsvertrag nach seinem Artikel 6 Absatz 2 Satz 3 gegenstandslos werden, ist dies unverzüglich im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 19.
Mai 2016

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag](/de/vertraege-243131) - Rundfunkstaatsvertrag

[zum Staatsvertrag](/vertraege/zdf_stv/15) - ZDF-Staatsvertrag

[zum Staatsvertrag](/vertraege/dlr_stv/13) - Deutschlandradio-Staatsvertrag

[zum Staatsvertrag](/vertraege/rbstv/3) - Rundfunkbeitragsstaatsvertrag

[zum Staatsvertrag](/vertraege/jmstv) - Jugendmedienschutz-Staatsvertrag

* * *

### Anlagen

1

[Neunzehnter Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Neunzehnter Rundfunkänderungsstaatsvertrag)](/br2/sixcms/media.php/68/GVBl_I_16_2016_001Anlage-zum-Hauptdokument.pdf "Neunzehnter Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Neunzehnter Rundfunkänderungsstaatsvertrag)") 933.2 KB