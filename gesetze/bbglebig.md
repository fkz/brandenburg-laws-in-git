## Gesetz über die Ausbildung und Prüfung für Lehrämter und die Fort- und Weiterbildung von Lehrerinnen und Lehrern im Land Brandenburg (Brandenburgisches Lehrerbildungsgesetz - BbgLeBiG )

Inhaltsübersicht
----------------

Abschnitt 1  
Allgemeines
-------------------------

[§ 1 Ziel und Inhalt der Lehrerbildung](#1)  
[§ 2 Lehrämter](#2)

Abschnitt 2  
Lehramtsstudium und Vorbereitungsdienst
-----------------------------------------------------

[§ 3 Lehramtsstudium](#3)  
[§ 4 Akkreditierung und Zugang zum Vorbereitungsdienst](#4)  
[§ 5 Vorbereitungsdienst](#5)  
[§ 6 Zulassung zum Vorbereitungsdienst](#6)  
[§ 7 Berufsbegleitender Vorbereitungsdienst und besonderer Zugang zum Vorbereitungsdienst](#7)  
[§ 8 Staatsprüfung](#8)

Abschnitt 3  
Fort- und Weiterbildung
-------------------------------------

[§ 9 Fortbildung der Lehrkräfte](#9)  
[§ 10 Weiterbildung der Lehrkräfte](#10)  
[§ 11 Nachträglicher Erwerb von Lehr- und Lehramtsbefähigungen](#11)  
[§ 12 Zusatzqualifikationen](#12)

Abschnitt 4  
Anerkennungen
---------------------------

[§ 13 Anerkennungen](#13)  
[§ 14 Anerkennung von Befähigungsprüfungen für Religionsunterricht](#14)

Abschnitt 5  
Zuständigkeiten, Verwaltungsrechtsweg, Mitwirkung und Datenschutz
-------------------------------------------------------------------------------

[§ 15 Zuständigkeiten und Verwaltungsrechtsweg](#15)  
[§ 16 Landesschulbeirat](#16)  
[§ 17 Schutz personenbezogener Daten](#17)

Abschnitt 6  
Übergangsvorschriften
-----------------------------------

[§ 18 Übergangsvorschriften](#18)

Abschnitt 1  
Allgemeines
-------------------------

### § 1  
Ziel und Inhalt der Lehrerbildung

(1) Die Lehrerbildung hat das Ziel, für die Tätigkeit als Lehrkraft an Schulen zu befähigen.
Sie gewährleistet den Aufbau, die Aktualisierung und die Erweiterung der auf den Lehrerberuf bezogenen Kompetenzen und qualifiziert die Lehrkräfte, eigenständig Verantwortung für die ihnen übertragenen Aufgaben zu übernehmen, am Prozess einer innovativen Schulentwicklung mitzuwirken und die eigenen Kompetenzen ständig weiterzuentwickeln.
Daneben erfolgt eine zielgerichtete Qualifizierung zur Aufgabenerfüllung im Rahmen der Schulverwaltung und des Schulrechts sowie insbesondere in der schulischen Medienbildung und der Gesundheitserziehung.
Sie befähigt die Lehrkräfte ferner zu verantwortlichem Handeln in den Schulen eines freiheitlichen, demokratischen und sozialen Rechtsstaates.

(2) Die Lehrerbildung bezieht sich auf den Erwerb von Kenntnissen, Fähigkeiten und Fertigkeiten, über die eine Lehrkraft zur Bewältigung ihrer allgemeinen und lehramtsspezifischen Aufgaben verfügen muss, und die die Weiterentwicklung des professionellen Selbstkonzeptes ermöglichen.
Die berufsfeldbezogenen Kompetenzen orientieren sich an den Zielen und Grundsätzen der Bildung und Erziehung des Brandenburgischen Schulgesetzes und konzentrieren sich unter Berücksichtigung des Aspekts der Inklusion auf die Bereiche Unterricht, Erziehung, Beurteilung und Innovation.
In der Lehrerbildung sind die Geschichte und die Kultur der Sorben/Wenden in angemessenem Umfang zu berücksichtigen.

(3) Die Lehrerbildung gliedert sich in drei Phasen.
Die erste Phase umfasst das Lehramtsstudium an einer Universität.
Die zweite Phase beinhaltet die pädagogisch-praktische Ausbildung im Vorbereitungsdienst.
Dem schließt sich die dritte Phase der Lehrerbildung mit der Fortbildung einschließlich der Berufseingangsphase und der Weiterbildung an.
Die Arbeit in den verschiedenen Phasen der Lehrerbildung ist eng aufeinander bezogen.
Die an der Lehrerbildung beteiligten Institutionen und Einrichtungen wirken nachhaltig zusammen und erfüllen die Aufgaben im Rahmen ihrer Zuständigkeiten.

(4) Zur Sicherung und Weiterentwicklung der Qualität der Lehrerbildung haben die lehrerbildenden Einrichtungen die Qualität und den Erfolg ihrer Arbeit regelmäßig zu ermitteln und zu bewerten (interne Evaluation).
Für Lehramtskandidatinnen und Lehramtskandidaten besteht die Pflicht zur Teilnahme an Befragungen und Erhebungen, soweit diese zur rechtmäßigen Erfüllung des Evaluationsauftrages erforderlich sind.

(5) Zur Erprobung neuer Konzepte der Berufsqualifizierung und des Berufseinstiegs wird das für Schule zuständige Mitglied der Landesregierung ermächtigt, versuchsweise andere, von diesem Gesetz abweichende Inhalte und Formen der Lehrerausbildung durch Rechtsverordnung zuzulassen.
Voraussetzung ist, dass die Gleichwertigkeit der Anforderungen und Inhalte sichergestellt ist.
Soweit hochschulrechtliche Belange betroffen sind, wird die Rechtsverordnung im Einvernehmen mit dem für Wissenschaft zuständigen Mitglied der Landesregierung erlassen.

(6) Für die Untersuchungen gemäß Absatz 4 dürfen personenbezogene Daten ohne Einwilligung der Lehramtskandidatinnen und Lehramtskandidaten erhoben werden, wenn ein besonderes öffentliches Interesse an der Durchführung der Untersuchungen festgestellt wurde.
Wissenschaftliche Untersuchungen, die nicht Grundlage für die Evaluation gemäß Absatz 4 sind, bedürfen der Genehmigung gemäß § 15 Absatz 1 Nummer 3 und der Einwilligung der betroffenen Personen.

### § 2  
Lehrämter

(1) Es wird für folgende Lehrämter ausgebildet:

1.  das Lehramt für die Primarstufe,
2.  das Lehramt für die Sekundarstufen I und II (allgemeinbildende Fächer),
3.  das Lehramt für die Sekundarstufe II (berufliche Fächer) und
4.  das Lehramt für Förderpädagogik.

(2) Die Ausbildung zur Befähigung für ein Lehramt umfasst das Lehramtsstudium und den Vorbereitungsdienst.
Beide Ausbildungsphasen sind berufsfeldorientiert und mit dem Ziel einer wissenschaftlich fundierten Berufsausbildung inhaltlich eng aufeinander bezogen.

Abschnitt 2  
Lehramtsstudium und Vorbereitungsdienst
-----------------------------------------------------

### § 3  
Lehramtsstudium

(1) Das Lehramtsstudium wird an der Universität durchgeführt und ist inhaltlich auf das angestrebte Lehramt ausgerichtet.
Für das Lehramt der Sekundarstufe II (berufliche Fächer) kann das Studium auch in Kooperation mit Fachhochschulen erfolgen.
Die Durchlässigkeit zwischen nicht lehramtsbezogenen und lehramtsbezogenen Studiengängen soll bei Anrechnung vergleichbarer Studien- und Prüfungsleistungen gewährleistet werden.

(2) Das Lehramtsstudium besteht aus einem lehramtsbezogenen Bachelorstudium mit einer dreijährigen Regelstudienzeit mit dem Abschluss „Bachelor of Education“ sowie einem darauf aufbauenden lehramtsbezogenen Masterstudium mit einer zweijährigen Regelstudienzeit mit dem Abschluss „Master of Education“.
Abweichend von Satz 1 kann der Zugang zu einem lehramtsbezogenen Masterstudiengang eröffnet werden, wenn die in einem nicht lehramtsbezogenen und abgeschlossenen Bachelorstudiengang erbrachten und nachgewiesenen Studienleistungen den Fächern gemäß Absatz 3 zugeordnet werden können.

(3) Die Studiengänge umfassen das Studium von mindestens zwei wissenschaftlichen oder künstlerischen Unterrichtsfächern, Lernbereichen oder Fachrichtungen und ihrer Didaktik (Fächer) sowie Bildungswissenschaften sowohl in der Bachelor- als auch in der Masterphase.
Schulpraktische Studien sind integrativer Bestandteil sowohl der Bachelor- als auch der Masterphase und in der Verantwortung der Universität durchzuführen.
Die Studien- und Prüfungsleistungen in den Fachdidaktiken, Bildungswissenschaften und schulpraktischen Studien umfassen zusammen beim Studium für die Lehrämter gemäß § 2 Absatz 1 Nummer 2 bis 4 mindestens ein Drittel des Gesamtumfanges eines Lehramtsstudiums.
Beim Studium für das Lehramt für die Primarstufe gemäß § 2 Absatz 1 Nummer 1 umfassen die Studien- und Prüfungsleistungen in den Fachdidaktiken, Bildungswissenschaften, in der Grundschulbildung und in den schulpraktischen Studien zusammen mindestens ein Drittel des Gesamtumfanges eines Lehramtsstudiums.
Darüber hinaus gilt für das Studium der einzelnen Lehrämter, dass

1.  für das Lehramt der Primarstufe Studienleistungen in der Grundschulbildung nachzuweisen sind,
2.  für das Lehramt für die Bildungsgänge der Sekundarstufen I und II (allgemeinbildende Fächer) eine Schwerpunktbildung auf die Sekundarstufe I oder die Sekundarstufe II erfolgen muss, wobei die jeweilige Stufenspezifik sowohl bei den fachwissenschaftlichen als auch bildungswissenschaftlichen Studien zu berücksichtigen ist,
3.  bei dem Lehramt für die Sekundarstufe II (berufliche Fächer)
    1.  unter den Fächern gemäß Satz 1 mindestens ein berufliches Fach zu studieren ist und
    2.  in den Bildungswissenschaften der Schwerpunkt auf Berufs- oder Wirtschaftspädagogik zu legen ist und
4.  bei dem Lehramt für Förderpädagogik Studienleistungen in der Förderpädagogik an die Stelle der Studienleistungen eines Faches treten.

(4) Im Lehramtsstudium für die Lehrämter gemäß § 2 Absatz 1 Nummer 1 bis 3 werden im Rahmen der bildungswissenschaftlichen Studien die Grundlagen der allgemeinen Inklusionspädagogik und -didaktik vermittelt.
Sie umfassen mindestens ein Zehntel der jeweils für die Bildungswissenschaften vorgesehenen Studien- und Prüfungsleistungen.
Fachbezogene inklusionspädagogische und -didaktische Inhalte werden in den jeweiligen fachdidaktischen Studien vermittelt.
Darüber hinaus kann eine inklusionspädagogische Schwerpunktbildung erfolgen.
In diesem Fall treten die dafür nachzuweisenden Studienleistungen ganz oder teilweise an die Stelle der Studienleistungen eines Faches oder der Grundschulbildung.

(5) Das Studium gliedert sich in Module, die jeweils mit einer Prüfung abgeschlossen werden (Modulabschlussprüfung) und im Diplomzusatz (Diploma Supplement) auszuweisen sind.
Den in den einzelnen Modulen zu erbringenden Studien- und Prüfungsleistungen werden Leistungspunkte zugeordnet.

(6) Das für Schule zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit dem für Wissenschaft zuständigen Mitglied der Landesregierung den Zugang, die Ausgestaltung und den Abschluss der Studiengänge durch Rechtsverordnung zu regeln, insbesondere

1.  die Feststellung der individuellen Voraussetzungen für die Tätigkeit als Lehrkraft,
2.  den Zugang zum lehramtsbezogenen Masterstudiengang,
3.  die für die einzelnen Lehrämter zugelassenen Fächer einschließlich deren Verbindungen und die inklusionspädagogische Schwerpunktbildung,
4.  Art und Umfang der nachzuweisenden Studien- und Prüfungsleistungen sowie deren Bewertung,
5.  die Voraussetzungen für den Erwerb des Abschlusses „Master of Education“ und
6.  die Zeugnisse und Bescheinigungen.

### § 4  
Akkreditierung und Zugang zum Vorbereitungsdienst

(1) Die fachlichen Voraussetzungen für den Zugang zum Vorbereitungsdienst für ein Lehramt sind erfüllt, wenn die von den Bewerberinnen und Bewerbern abgeschlossenen Studiengänge gemäß § 3 Absatz 2 akkreditiert oder reakkreditiert sind.
§ 13 bleibt hiervon unberührt.

(2) Das für Schule zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit dem für Wissenschaft zuständigen Mitglied der Landesregierung das Nähere zur Akkreditierung und Reakkreditierung von lehramtsbezogenen Studiengängen durch Rechtsverordnung zu regeln, insbesondere

1.  zur Beteiligung des für Schule zuständigen Ministeriums und
2.  zur inhaltlichen Ausgestaltung sowie zu den Gegenständen des Verfahrens der Akkreditierung und Reakkreditierung unter Berücksichtigung lehramtsbezogener Besonderheiten.

### § 5  
Vorbereitungsdienst

(1) Der Vorbereitungsdienst dauert zwölf Monate.
Er wird an Ausbildungsschulen und in den Studienseminaren durchgeführt.
Ausbildungsschulen sind die Schulen in öffentlicher Trägerschaft im Land Brandenburg.
Daneben können auch anerkannte Ersatzschulen Ausbildungsschulen sein.

(2) Abweichend von Absatz 1 Satz 1 dauert der Vorbereitungsdienst 18 Monate, wenn die Zulassung zum Vorbereitungsdienst aufgrund eines abgeschlossenen Lehramtsstudiums erfolgte, in dem

1.  die Regelstudienzeit nach diesem Gesetz unterschritten wird oder
2.  keine schulpraktischen Studien absolviert wurden, die im Wesentlichen den Anforderungen an die nach diesem Gesetz oder den hierzu erlassenen Rechtsverordnungen bestimmten schulpraktischen Studien entsprechen.

(3) Die Einstellung in den Vorbereitungsdienst erfolgt auf Antrag unter Berufung in das Beamtenverhältnis auf Widerruf, wobei Lehramtskandidatinnen und Lehramtskandidaten für ein

1.  Lehramt gemäß § 2 Absatz 1 Nummer 1, gemäß § 2 Absatz 1 Nummer 2 bei einer Schwerpunktbildung auf die Sekundarstufe I sowie gemäß § 2 Absatz 1 Nummer 4 zur Lehramtsanwärterin oder zum Lehramtsanwärter und
2.  Lehramt gemäß § 2 Absatz 1 Nummer 2 bei einer Schwerpunktbildung auf die Sekundarstufe II sowie gemäß § 2 Absatz 1 Nummer 3 zur Studienreferendarin oder zum Studienreferendar

ernannt werden.
Liegen die Voraussetzungen für die Berufung in ein Beamtenverhältnis auf Widerruf nicht vor, wird der Vorbereitungsdienst in einem öffentlich-rechtlichen Ausbildungsverhältnis absolviert.
In diesem Fall besteht ein Anspruch auf Unterhaltsgeld in Höhe der Anwärterbezüge für Beamtinnen und Beamte im Vorbereitungsdienst.

(4) Ziel des Vorbereitungsdienstes ist es, dass die Lehramtskandidatinnen und Lehramtskandidaten im Hinblick auf den Bildungsauftrag der Schule nach § 4 des Brandenburgischen Schulgesetzes befähigt werden, alle Schülerinnen und Schüler individuell so zu fordern und zu fördern, dass diese ihr Leben eigenverantwortlich gestalten und in Gesellschaft und Beruf Verantwortung für sich und andere übernehmen können.
Schwerpunkt in der Ausbildung ist die pädagogische Praxis und deren theoriegeleitete Reflexion.

(5) Die Ausbildung im Studienseminar umfasst insbesondere die pädagogische und fachdidaktische Ausbildung sowie ergänzende Ausbildungsangebote.
Die Ausbildung an der Ausbildungsschule besteht aus Ausbildungsunterricht und anderen, die Gestaltung des Unterrichts und des Schullebens betreffenden Tätigkeiten der Lehrkräfte.
Der im Rahmen des Vorbereitungsdienstes erteilte selbstständige Unterricht wird auf den Stellenbedarf der Ausbildungsschulen nicht angerechnet.

(6) Auf Antrag der Lehramtskandidatinnen und Lehramtskandidaten kann ein zeitlich begrenzter Teil der Ausbildung in einer Lehrerausbildungseinrichtung, die außerhalb des Landes Brandenburg absolviert wurde, auf die Dauer der Ausbildung angerechnet werden.

(7) Lehrkräfte, die die fachlichen Voraussetzungen für den Zugang zum Vorbereitungsdienst für ein Lehramt gemäß § 4 erfüllen und die zur Deckung des Unterrichtsbedarfs in den Schuldienst eingestellt wurden, können ohne Einstellung in den Vorbereitungsdienst im Rahmen der Kapazitäten gemäß § 6 Absatz 1 Satz 2 Nummer 2 berufsbegleitend am Vorbereitungsdienst mit dem Ziel teilnehmen, die Staatsprüfung abzulegen.
Abweichend von Absatz 1 Satz 1 dauert der Vorbereitungsdienst 18 Monate.

(8) Das für Schule zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit den für Inneres und Finanzen zuständigen Mitgliedern der Landesregierung das Nähere zum Vorbereitungsdienst durch Rechtsverordnung zu regeln, insbesondere

1.  die Zuordnung der Fächer der den Zugang zum Vorbereitungsdienst berechtigenden Studienabschlüsse zu Unterrichtsfächern, in denen Ausbildungsunterricht erteilt werden kann,
2.  die Einzelheiten zu den weiteren Voraussetzungen der Verkürzung, Unterbrechung oder Verlängerung und zur Beendigung des Vorbereitungsdienstes sowie für die Anrechnung gemäß Absatz 6 und der berufsbegleitenden Teilnahme am Vorbereitungsdienst,
3.  zu den Ausbildungszielen, Inhalten und der Organisation des Vorbereitungsdienstes und
4.  die Bewertung der Leistungen durch die Studienseminare sowie die Beurteilung durch die Ausbildungsschulen.

### § 6  
Zulassung zum Vorbereitungsdienst

(1) Die Zulassungen zum Vorbereitungsdienst für ein Lehramt sind zu beschränken, wenn die Zahl der Bewerbungen die für das jeweilige Lehramt bestehende Ausbildungskapazität überschreitet.
Die Ausbildungskapazität ergibt sich aus

1.  der Zahl der im jeweiligen Haushalt ausgewiesenen Stellen für Lehramtskandidatinnen und Lehramtskandidaten und
2.  den personellen, sächlichen und organisatorischen Kapazitäten der Studienseminare und der Ausbildungsschulen, die für die Gewährleistung einer ordnungsgemäßen Ausbildung erforderlich sind.

Für einzelne Fächer, in denen ein dringender Lehrkräftebedarf besteht, können entsprechende Ausbildungskapazitäten bereitgestellt werden.

(2) Sofern die Anzahl der rechtzeitig zum Bewerbungstermin gestellten Anträge auf Zulassung zum Vorbereitungsdienst die nach Absatz 1 zu bestimmenden Höchstzahlen übersteigt, sind

1.  vorab bis zu 10 Prozent der Ausbildungsplätze für Fälle außergewöhnlicher Härte,
2.  vorab bis zu 15 Prozent der Ausbildungsplätze an Bewerberinnen und Bewerber mit mindestens einem Fach, in dem ein dringender Lehrkräftebedarf besteht,
3.  von den verbleibenden Ausbildungsplätzen 65 Prozent nach Eignung der Bewerberinnen und Bewerber, insbesondere aufgrund der nachgewiesenen Leistungen des den Zugang zum Vorbereitungsdienst berechtigenden Prüfungsabschlusses und
4.  weitere 35 Prozent nach der Dauer der Wartezeit seit dem Bewerbungstermin, zu dem der erste Antrag auf Zulassung zum Vorbereitungsdienst gestellt worden ist,

zu vergeben.

(3) Lehrkräften, die an Ersatzschulen im Land Brandenburg unterrichten und die die Zulassungsvoraussetzungen für die Aufnahme in den Vorbereitungsdienst erfüllen, kann im Rahmen freier Ausbildungskapazitäten die Teilnahme an den Ausbildungsveranstaltungen gemäß § 5 Absatz 5 Satz 1 mit gleichen Rechten und Pflichten zur Vorbereitung auf die Staatsprüfung für ein Lehramt ohne Einstellung in den Vorbereitungsdienst gestattet werden.

(4) Das für Schule zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit den für Inneres und Finanzen zuständigen Mitgliedern der Landesregierung das Nähere zur Zulassung zum Vorbereitungsdienst durch Rechtsverordnung zu regeln, insbesondere

1.  die Einzelheiten der Zulassungsvoraussetzungen sowie die Festlegung der Höchstzahlen und Bewerbungsfristen einschließlich der Ausschlussfristen,
2.  die Gründe, die die Annahme einer außergewöhnlichen Härte bei der Vorgabe von Ausbildungsplätzen rechtfertigen,
3.  Tätigkeiten, die neben der Gesamtnote des den Zugang zum Vorbereitungsdienst berechtigenden Prüfungsabschlusses bei der Feststellung für die Zulassung berücksichtigt werden können,
4.  die Berücksichtigung von Wartezeiten bei der Zulassung zum Vorbereitungsdienst,
5.  die Voraussetzungen für die Teilnahme von Lehrkräften gemäß Absatz 3,
6.  die Festlegung der Zahl von Plätzen, die für Lehrkräfte gemäß § 13 Absatz 3 für die Teilnahme an einem Anpassungslehrgang zur Verfügung zu stellen sind,
7.  die Kriterien für die Feststellung und Festlegung der Ausbildungskapazitäten für die Fächer gemäß Absatz 1 Satz 3 und
8.  die Einzelheiten des Zulassungsverfahrens für die Teilnahme am berufsbegleitenden Vorbereitungsdienst gemäß § 5 Absatz 7.

### § 7  
Berufsbegleitender Vorbereitungsdienst und besonderer Zugang zum Vorbereitungsdienst

(1) Lehrkräfte ohne Lehrbefähigung, die zur Deckung des Unterrichtsbedarfs in den Schuldienst eingestellt werden und die einen Hochschulabschluss nachweisen, der die fachwissenschaftlichen oder künstlerischen Voraussetzungen für einen Einsatz in mindestens zwei Fächern gestattet, können ohne Einstellung in den Vorbereitungsdienst im Rahmen der Kapazitäten gemäß § 6 Absatz 1 Satz 2 Nummer 2 berufsbegleitend am Vorbereitungsdienst mit dem Ziel teilnehmen, die Staatsprüfung abzulegen.
Die Teilnahme kann mit Auflagen verbunden werden, dass vorab weitere Studien- und Prüfungsleistungen zu erbringen sind oder eine ergänzende Ausbildung, auch im Rahmen des Vorbereitungsdienstes, zu absolvieren ist.
Satz 1 gilt nicht für Personen, die ausschließlich einen Bachelorabschluss erworben haben.

(2) Sofern es zur Deckung des Unterrichtsbedarfs erforderlich ist, können Ausbildungsplätze im Rahmen freier Kapazitäten für Personen, die einen Hochschulabschluss gemäß Satz 2 nachweisen, zur Verfügung gestellt werden.
Voraussetzung für die Zulassung zum Vorbereitungsdienst ist ein Hochschulabschluss, mit dem die fachwissenschaftlichen oder künstlerischen Bildungsvoraussetzungen für die Ausbildung in mindestens einem Unterrichtsfach nachgewiesen werden, und dass Art und Umfang des dem Hochschulabschluss zugrunde liegenden Studiums die Ausbildung für ein weiteres Unterrichtsfach ermöglicht.
Absatz 1 Satz 3 und § 4 gelten entsprechend.

(3) In den Fällen der Absätze 1 und 2 dauert abweichend von § 5 Absatz 1 Satz 1 der Vorbereitungsdienst 24 Monate.

### § 8  
Staatsprüfung

(1) Die Staatsprüfung schließt den Vorbereitungsdienst ab.
Mit ihr wird festgestellt, ob die Lehramtskandidatin oder der Lehramtskandidat das Ausbildungsziel erreicht hat.
Mit dem Bestehen der Staatsprüfung erwirbt die Lehramtskandidatin oder der Lehramtskandidat die Befähigung für ein Lehramt gemäß § 2 Absatz 1.
Die Sätze 1 bis 3 gelten für Lehrkräfte, die berufsbegleitend am Vorbereitungsdienst teilnehmen, entsprechend.

(2) Lehramtskandidatinnen und Lehramtskandidaten gemäß § 5 Absatz 2 und § 7 Absatz 2 sowie Teilnehmerinnen und Teilnehmer am berufsbegleitenden Vorbereitungsdienst gemäß § 5 Absatz 7 und § 7 Absatz 1 können eine vorzeitige Zulassung zur Staatsprüfung beantragen, um den Vorbereitungsdienst frühestens nach zwölf Monaten zu beenden.
Wird die Staatsprüfung bestanden, endet der Vorbereitungsdienst oder der berufsbegleitende Vorbereitungsdienst in diesen Fällen mit Ablauf des Monats, in dem die Staatsprüfung bestanden wird.

(3) Lehrkräfte können ohne Nachweis eines Vorbereitungsdienstes zu einer besonderen Staatsprüfung mit dem Ziel des Erwerbs der Befähigung für ein Lehramt gemäß § 2 Absatz 1 zugelassen werden, wenn sie

1.  zur Deckung des Lehrkräftebedarfs im Land Brandenburg in den Schuldienst eingestellt wurden,
2.  über ein abgeschlossenes nicht lehramtsbezogenes Hochschulstudium verfügen, das einen Einsatz in mindestens zwei Unterrichtsfächern gestattet,
3.  eine mindestens einjährige Tätigkeit als Lehrkraft nachweisen, die dem angestrebten Lehramt im Wesentlichen entspricht,
4.  die Teilnahme an Qualifizierungsmaßnahmen nachweisen, die im Wesentlichen den im Rahmen eines Vorbereitungsdienstes für das angestrebte Lehramt vermittelten Inhalten und zu erwerbenden Kompetenzen entsprechen, sowie
5.  bisher noch nicht zu einer den Vorbereitungsdienst für das angestrebte Lehramt abschließenden Staatsprüfung zugelassen wurden.

Satz 1 gilt nicht für Personen, die ausschließlich einen Bachelorabschluss erworben haben.

(4) Lehrkräfte gemäß § 11 Absatz 2 können zu einer besonderen Staatsprüfung zum Erwerb einer Befähigung für ein weiteres Lehramt oder zum Erwerb einer Befähigung für ein Amt nach Maßgabe des Brandenburgischen Besoldungsgesetzes zugelassen werden, wenn sie

1.  die Teilnahme an Qualifizierungsmaßnahmen nachweisen, die im Wesentlichen den im Rahmen der nach diesem Gesetz geforderten Studien für das angestrebte Lehramt oder Amt vermittelten Inhalten und zu erwerbenden Kompetenzen entsprechen, und
2.  eine mindestens zweijährige erfolgreiche Tätigkeit in der dem angestrebten Lehramt oder Amt entsprechenden Schulform oder Schulstufe nachweisen.

(5) Das für Schule zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit den für Inneres und Finanzen zuständigen Mitgliedern der Landesregierung das Nähere zur Durchführung der Staatsprüfung durch Rechtsverordnung zu regeln, insbesondere

1.  die Zulassungsvoraussetzungen, das Verfahren und die Bestandteile sowie die Prüfungsanforderungen für die Staatsprüfung,
2.  die vorzeitige Zulassung zur Staatsprüfung und die besondere Staatsprüfung,
3.  den Nachweis der Leistungen während des Vorbereitungsdienstes, die Bewertung von Prüfungsleistungen, die Ermittlung der Noten und die Feststellung des Ergebnisses der Staatsprüfung,
4.  die Folgen der Leistungsverweigerung und des Versäumens von Prüfungsterminen, des Rücktritts sowie des prüfungswidrigen Verhaltens und des Nichtbestehens der Staatsprüfung,
5.  die Zusammensetzung der Prüfungsausschüsse sowie die Berufung der Prüferinnen und Prüfer und
6.  die Zeugnisse und Bescheinigungen.

Abschnitt 3  
Fort- und Weiterbildung
-------------------------------------

### § 9  
Fortbildung der Lehrkräfte

(1) Die Fortbildung der Lehrkräfte dient der Erhaltung, der Festigung und der Erweiterung der in Ausbildung und Berufspraxis erworbenen Kenntnisse und Fähigkeiten.
Ziel der Fortbildung ist es insbesondere, die Qualifikationen der Lehrkräfte gemäß § 1 Absatz 1 und 2 den sich verändernden Rahmenbedingungen und Anforderungen des Berufs inhaltlich anzupassen.

(2) Lehrkräfte, die erstmals und dauerhaft in den Schuldienst des Landes Brandenburg eintreten, werden bei der Weiterentwicklung und Vertiefung ihrer im Studium und im Vorbereitungsdienst erworbenen professionsbezogenen Kompetenzen unterstützt und beraten (Berufseingangsphase).

(3) Die Lehrkräfte sind zur ständigen Fortbildung verpflichtet.
§ 67 Absatz 3 Satz 1 des Brandenburgischen Schulgesetzes gilt entsprechend.
Inhaltlich soll die Lehrkräftefortbildung als Qualifizierungsfelder die schulische Qualitätsentwicklung sowie die Standards, Instrumente und Ziele der Personalentwicklung aufgreifen.
Maßnahmen der Personalentwicklung qualifizieren für besondere Aufgaben in der Schule, für Ausbildungs-, Beratungs-, Unterstützungs- und Fortbildungstätigkeiten sowie für Funktionen in den Schulbehörden.
Träger der staatlichen Fortbildung sind insbesondere Schulen, die staatlichen Schulämter, das Landesinstitut für Schule und Medien Berlin-Brandenburg, das Sozialpädagogische Fortbildungsinstitut Berlin-Brandenburg sowie die Sonderpädagogischen Förder- und Beratungsstellen.

(4) Für die Teilnahme an Maßnahmen der staatlichen Fortbildung und ihr gleichgestellten Veranstaltungen anderer Träger können Vorschriften erlassen werden, die insbesondere Fragen der Unterrichtsbefreiung und Auslagenerstattung regeln.
Fortbildungsveranstaltungen können auf den Anspruch auf Bildungsfreistellung nach dem Brandenburgischen Weiterbildungsgesetz angerechnet werden.

### § 10  
Weiterbildung der Lehrkräfte

Die Weiterbildung der Lehrkräfte dient dem Erwerb zusätzlicher fachlicher Lehrbefähigungen oder dem Erwerb der Befähigung für ein oder ein weiteres Lehramt nach diesem Gesetz oder für ein Amt nach Maßgabe des Brandenburgischen Besoldungsgesetzes oder von Zusatzqualifikationen.

### § 11  
Nachträglicher Erwerb von Lehr- und Lehramtsbefähigungen

(1) Wer die Befähigung für ein Lehramt oder die Zugangsvoraussetzungen für den Vorbereitungsdienst für ein Lehramt nachweist oder eine Lehrbefähigung nach dem Recht der Deutschen Demokratischen Republik erworben hat, kann eine Lehrbefähigung in einem weiteren Fach oder weiteren Fächern erwerben, wenn

1.  die erforderlichen Studien- und Prüfungsleistungen durch ein Hochschulstudium nachgewiesen werden oder
2.  eine gleichwertige Weiterbildungsmaßnahme an einer Einrichtung der Lehrerfort- und -weiterbildung absolviert wird.

Die dem jeweiligen Studium oder der jeweiligen Weiterbildungsmaßnahme zugrunde liegende Studien- oder Prüfungsordnung bedarf der Genehmigung.
Satz 2 gilt nicht, wenn ein entsprechendes Studienangebot einer Hochschule akkreditiert oder reakkreditiert wurde.

(2) Wer die Befähigung für ein Lehramt erworben hat, kann die Befähigung für ein oder ein weiteres Lehramt nach diesem Gesetz erwerben.
Wer eine Lehrbefähigung nach dem Recht der Deutschen Demokratischen Republik erworben hat, kann eine Befähigung für ein Amt nach Maßgabe des Brandenburgischen Besoldungsgesetzes erwerben.
An den Erwerb der Befähigung für ein Lehramt sind gleichwertige Anforderungen wie bei Studium und Abschluss gemäß den §§ 3 und 4 zu stellen.
Die Regelung des Absatzes 1 gilt entsprechend.

(3) Das für Schule zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit den für Inneres, Finanzen und Wissenschaft zuständigen Mitgliedern der Landesregierung das Nähere über den Erwerb der Befähigungen gemäß den Absätzen 1 und 2 durch Rechtsverordnung zu regeln, insbesondere

1.  die Voraussetzungen für den Erwerb einer Befähigung,
2.  den Umfang und die Art der zu erbringenden Studien- und Prüfungsleistungen und
3.  die Anerkennung von Studien- und Prüfungsleistungen.

### § 12  
Zusatzqualifikationen

Wer die Befähigung für ein Lehramt oder eine Lehrbefähigung nach dem Recht der Deutschen Demokratischen Republik erworben hat, kann Zusatzqualifikationen in schulischen Handlungsfeldern erwerben.
Voraussetzung für den Erwerb ist in der Regel ein Studium an einer Universität einschließlich des Nachweises von Prüfungsleistungen.
§ 11 Absatz 1 Satz 2 gilt entsprechend.
Studien- und Prüfungsordnungen bedürfen zur Anerkennung als Zusatzqualifikationen der vorherigen Genehmigung.

Abschnitt 4  
Anerkennungen
---------------------------

### § 13  
Anerkennungen

(1) Außerhalb des Landes Brandenburg abgelegte lehramtsbezogene Prüfungen (Erste Staatsprüfung oder lehramtsspezifische Hochschulabschlussprüfung) werden hinsichtlich des Zugangs zu einem entsprechenden Vorbereitungsdienst anerkannt, wenn sie im jeweiligen Land der Bundesrepublik Deutschland oder im Land Brandenburg zum Zugang zum Vorbereitungsdienst berechtigen.
Dies gilt entsprechend für die Anerkennung von außerhalb des Geltungsbereiches dieses Gesetzes erworbenen Zusatzqualifikationen gemäß § 12 sowie für die Anerkennung nachträglich erworbener Lehrbefähigungen und Lehramtsbefähigungen.

(2) Eine in einem Land der Bundesrepublik Deutschland erworbene Befähigung für ein Lehramt wird anerkannt und einem Lehramt gemäß § 2 Absatz 1 zugeordnet.

(3) Ausländische Lehrerberufsqualifikationen werden anerkannt, wenn die Ausbildung und Prüfung den Anforderungen für das angestrebte Lehramt im Wesentlichen entsprechen.
Das Brandenburgische Berufsqualifikationsfeststellungsgesetz findet mit Ausnahme der §§ 13b und 17 keine Anwendung.
Das für Schule zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit den für Inneres, Finanzen und Wissenschaft zuständigen Mitgliedern der Landesregierung das Nähere zur Anerkennung von ausländischen Lehrerberufsqualifikationen, insbesondere zu dem Anerkennungsverfahren und zu den Ausgleichsmaßnahmen, durch Rechtsverordnung zu regeln.

### § 14  
Anerkennung von Befähigungsprüfungen für Religionsunterricht

(1) Eine von den Kirchen und Religionsgemeinschaften abgenommene Prüfung zur Erlangung der Befähigung, Unterricht in evangelischer, katholischer oder jüdischer Religionslehre zu erteilen, kann für den nachträglichen Erwerb einer Lehr- oder Lehramtsbefähigung oder als Zugangsvoraussetzung für den Vorbereitungsdienst oder als Teil einer Staatsprüfung für ein Lehramt anerkannt werden, wenn ein Studienumfang nachgewiesen wird, der einem der staatlichen Fächer entspricht.
Die Anerkennung darf nicht versagt werden, wenn die Prüfung nach einer bestätigten Ausbildungs- und Prüfungsordnung durchgeführt worden ist.

(2) Die Regelung des Absatzes 1 gilt entsprechend für gleichwertige Ausbildungen anderer Religionsgemeinschaften oder Weltanschauungsgemeinschaften.

Abschnitt 5  
Zuständigkeiten, Verwaltungsrechtsweg, Mitwirkung und Datenschutz
-------------------------------------------------------------------------------

### § 15  
Zuständigkeiten und Verwaltungsrechtsweg

(1) Das für Schule zuständige Ministerium nimmt Aufgaben in der Lehrerbildung wahr, insbesondere die

1.  Organisation und Durchführung des Vorbereitungsdienstes einschließlich der Staatsprüfung,
2.  Durchführung des Zulassungsverfahrens zum Vorbereitungsdienst,
3.  nach diesem Gesetz vorgesehenen Anerkennungen, Zuordnungen und Genehmigungen sowie
4.  sonstigen Feststellungen und Maßnahmen, soweit nichts anderes geregelt ist.

(2) Zur Durchführung von Prüfungen gemäß Absatz 1 Nummer 1 werden Prüferinnen und Prüfer aus dem Schul- und Schulaufsichtsbereich berufen.

(3) Verwaltungsakte, die das für Schule zuständige Ministerium nach diesem Gesetz erlässt, bedürfen einer Nachprüfung in einem Vorverfahren nach den Vorschriften des 8.
Abschnitts der Verwaltungsgerichtsordnung.

### § 16  
Landesschulbeirat

Der Landesschulbeirat berät das für Schule zuständige Ministerium in grundsätzlichen Fragen der Lehrerbildung.
Das für Wissenschaft zuständige Ministerium sowie die an der Lehreraus- oder -weiterbildung beteiligten Hochschulen des Landes sind zu den betreffenden Beratungen einzuladen.
§ 139 Absatz 3 bis 6 des Brandenburgischen Schulgesetzes gilt entsprechend.

### § 17  
Schutz personenbezogener Daten

Das für Schule zuständige Ministerium und die staatlichen Schulämter dürfen personenbezogene Daten von Studierenden und von Lehramtskandidatinnen und Lehramtskandidaten verarbeiten, wenn dies für

1.  die Aufnahme in den Vorbereitungsdienst und seine Durchführung,
2.  die Zulassung zur Staatsprüfung und ihre Durchführung und ihren Abschluss und
3.  Anerkennungen

erforderlich ist.

Abschnitt 6  
Übergangsvorschriften
-----------------------------------

### § 18  
Übergangsvorschriften

(1) Die Fortgeltung der auf der Grundlage des Brandenburgischen Lehrerbildungsgesetzes vom 25. Juni 1999 (GVBl.
I S. 242), das zuletzt durch Artikel 17 des Gesetzes vom 3.
April 2009 (GVBl. I S. 26, 59) geändert worden ist, durchgeführten Lehramts-, Ergänzungs- und Erweiterungsprüfungen wird durch das Inkrafttreten dieses Gesetzes nicht berührt.
Die erworbene Befähigung für ein Lehramt wird einem entsprechenden Lehramt gemäß § 2 Absatz 1 zugeordnet, soweit die Sätze 3 bis 5 keine abweichende Regelung treffen.
Für das Lehramt für die Bildungsgänge der Sekundarstufe I und der Primarstufe an allgemeinbildenden Schulen erfolgt keine Zuordnung.
Die erworbene Befähigung für das Lehramt an Gymnasien bleibt bis einschließlich 31.
März 2020 bestehen und wird zusätzlich dem Lehramt für die Sekundarstufe I und II (allgemeinbildende Fächer) mit einer Schwerpunktbildung auf die Sekundarstufe II zugeordnet.
Die Sätze 3 und 4 gelten für Anerkennungen gemäß § 13 Absatz 1 bis 3 entsprechend.

(2) Wer auf der Grundlage des Brandenburgischen Lehrerbildungsgesetzes vom 25.
Juni 1999 (GVBl. I S. 242), das zuletzt durch Artikel 17 des Gesetzes vom 3.
April 2009 (GVBl.
I S.
26, 59) geändert worden ist, eine Erste Staatsprüfung für ein Lehramt bestanden hat oder wem ein Hochschulabschluss einer Ersten Staatsprüfung gleichgestellt wurde, erfüllt die Zugangsvoraussetzungen für den Vorbereitungsdienst für ein Lehramt.

(3) Für Lehramtskandidatinnen und Lehramtskandidaten mit einer Ersten Staatsprüfung für ein Lehramt oder bei denen ein Hochschulabschluss als Erste Staatsprüfung für ein Lehramt gleichgestellt wurde, wird die Staatsprüfung gemäß § 8 über den 1.
Juni 2013 hinaus als „Zweite Staatsprüfung“ bezeichnet.

(4) Studierende, die sich bei Inkrafttreten dieses Gesetzes in einer Ausbildung gemäß § 5a des Brandenburgischen Lehrerbildungsgesetzes vom 25.
Juni 1999 (GVBl.
I S.
242), das zuletzt durch Artikel 17 des Gesetzes vom 3.
April 2009 (GVBl.
I S.
26, 59) geändert worden ist, befinden und den Masterstudiengang bis spätestens 30.
September 2022 beenden, absolvieren das Studium auf der Grundlage des Brandenburgischen Lehrerbildungsgesetzes vom 25.
Juni 1999 (GVBl.
I S.
242), das zuletzt durch Artikel 17 des Gesetzes vom 3.
April 2009 (GVBl.
I S.
26, 59) geändert worden ist.

(5) Lehramtskandidatinnen oder Lehramtskandidaten, die sich zum Zeitpunkt des Inkrafttretens dieses Gesetzes im Vorbereitungsdienst befinden, absolvieren den Vorbereitungsdienst auf der Grundlage des Brandenburgischen Lehrerbildungsgesetzes vom 25.
Juni 1999 (GVBl.
I S.
242), das zuletzt durch Artikel 17 des Gesetzes vom 3.
April 2009 (GVBl.
I S.
26, 59) geändert worden ist.

(6) Lehramtskandidatinnen oder Lehramtskandidaten, die nach Inkrafttreten dieses Gesetzes und bis zum 31.
Dezember 2018 in den Vorbereitungsdienst aufgenommen werden, absolvieren den Vorbereitungsdienst auf der Grundlage des Brandenburgischen Lehrerbildungsgesetzes vom 25. Juni 1999 (GVBl.
I S.
242), das zuletzt durch Artikel 17 des Gesetzes vom 3.
April 2009 (GVBl. I S. 26, 59) geändert worden ist, mit der Maßgabe, dass der Vorbereitungsdienst 18 Monate dauert.
Dies gilt nicht für die Teilnahme am berufsbegleitenden Vorbereitungsdienst und den besonderen Zugang zum Vorbereitungsdienst gemäß § 7.

(7) Lehramtskandidatinnen oder Lehramtskandidaten, die ihr Studium gemäß Absatz 4 abgeschlossen haben und nach dem 31.
Dezember 2018 in den Vorbereitungsdienst aufgenommen werden, absolvieren den Vorbereitungsdienst auf der Grundlage dieses Gesetzes mit der Maßgabe, dass sie das Lehramt für die Bildungsgänge der Sekundarstufe I und der Primarstufe an allgemeinbildenden Schulen oder das Lehramt an Gymnasien erwerben.
Der Vorbereitungsdienst muss bis 31. Dezember 2025 abgeschlossen werden.

(8) Das für Schule zuständige Ministerium wird ermächtigt, die auf der Grundlage des Brandenburgischen Lehrerbildungsgesetzes vom 25.
Juni 1999 (GVBl.
I S.
242), das zuletzt durch Artikel 17 des Gesetzes vom 3.
April 2009 (GVBl.
I S.
26, 59) geändert worden ist, erlassenen Rechtsverordnungen zu ändern und außer Kraft zu setzen.