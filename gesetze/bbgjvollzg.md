## Gesetz über den Vollzug der Freiheitsstrafe, der Jugendstrafe und der Untersuchungshaft im Land Brandenburg (Brandenburgisches Justizvollzugsgesetz - BbgJVollzG)

Der Landtag hat das folgende Gesetz beschlossen:

Inhaltsübersicht
----------------

### Abschnitt 1  
Allgemeine Bestimmungen

[§ 1       Anwendungsbereich, Allgemeine Begriffsbestimmungen](#1)  
[§ 2       Ziel und Aufgabe des Vollzugs der Freiheits- und Jugendstrafe](#2)  
[§ 3       Aufgabe des Vollzugs der Untersuchungshaft, Zusammenarbeit](#3)  
[§ 4       Stellung der Gefangenen](#4)  
[§ 5       Besondere Stellung der Untersuchungsgefangenen](#5)  
[§ 6       Mitwirkung im Vollzug der Freiheits- und Jugendstrafe](#6)  
[§ 7       Allgemeine Gestaltungsgrundsätze](#7)  
[§ 8       Grundsätze der Gestaltung des Vollzugs der Freiheits- und Jugendstrafe](#8)  
[§ 9       Erzieherische Gestaltung des Vollzugs der Jugendstrafe](#9)  
[§ 10     Erzieherische Gestaltung des Vollzugs der Untersuchungshaft an jungen Untersuchungsgefangenen](#10)  
[§ 11     Soziale Hilfe](#11)

### Abschnitt 2  
Aufnahme, Diagnose, Vollzugs- und Eingliederungsplanung

[§ 12     Aufnahmeverfahren](#12)  
[§ 13     Diagnoseverfahren](#13)  
[§ 14     Vollzugs- und Eingliederungsplanung](#14)  
[§ 15     Inhalt des Vollzugs- und Eingliederungsplans](#15)  
[§ 16     Ermittlung des Förder- und Erziehungsbedarfs der jungen Untersuchungsgefangenen, Maßnahmen](#16)

### Abschnitt 3  
Unterbringung und Verlegung

[§ 17     Trennungsgrundsätze](#17)  
[§ 18     Unterbringung während der Einschlusszeiten](#18)  
[§ 19     Aufenthalt außerhalb der Einschlusszeiten](#19)  
[§ 20     Unterbringung in einer Wohneinheit](#20)  
[§ 21     Unterbringung von Eltern mit Kindern](#21)  
[§ 22     Geschlossener und offener Vollzug](#22)  
[§ 23     Wohngruppenvollzug](#23)  
[§ 24     Verlegung und Überstellung](#24)

### Abschnitt 4  
Sozial- und Psychotherapie

[§ 25     Sozialtherapie](#25)  
[§ 26     Psychotherapie](#26)

### Abschnitt 5  
Arbeitstherapeutische Maßnahmen, Arbeitstraining, schulische und  
berufliche Qualifizierungsmaßnahmen, Arbeit

[§ 27     Arbeitstherapeutische Maßnahmen](#27)  
[§ 28     Arbeitstraining](#28)  
[§ 29     Schulische und berufliche Qualifizierungsmaßnahmen](#29)  
[§ 30     Arbeit](#30)  
[§ 31     Freies Beschäftigungsverhältnis, Selbstbeschäftigung](#31)  
[§ 32     Freistellung von der Arbeit](#32)

### Abschnitt 6  
Besuche, Telefongespräche, Schriftwechsel,  
andere Formen der Telekommunikation und Pakete

[§ 33     Grundsatz](#33)  
[§ 34     Besuch](#34)  
[§ 35     Untersagung der Besuche](#34)  
[§ 36     Durchführung der Besuche](#36)  
[§ 37     Überwachung der Gespräche](#37)  
[§ 38     Telefongespräche](#38)  
[§ 39     Schriftwechsel](#39)  
[§ 40     Untersagung des Schriftwechsels](#40)  
[§ 41     Sichtkontrolle, Weiterleitung und Aufbewahrung von Schreiben](#41)  
[§ 42     Überwachung des Schriftwechsels](#42)  
[§ 43     Anhalten von Schreiben](#43)  
[§ 44     Andere Formen der Telekommunikation  
](#44)[§ 45     Pakete](#45)

### Abschnitt 7  
Lockerungen und sonstige Aufenthalte außerhalb der Anstalt

[§ 46     Lockerungen zur Erreichung des Vollzugsziels](#46)  
[§ 47     Lockerungen aus sonstigen Gründen](#47)  
[§ 48     Weisungen für Lockerungen, Zustimmung der Aufsichtsbehörde](#48)  
[§ 49     Ausführung, Außenbeschäftigung, Vorführung, Ausantwortung](#49)

### Abschnitt 8  
Vorbereitung der Eingliederung, Entlassung und nachgehende Betreuung

[§ 50     Vorbereitung der Eingliederung](#50)  
[§ 51     Entlassung der Straf- und Jugendstrafgefangenen](#51)  
[§ 52     Nachgehende Betreuung](#52)  
[§ 53     Verbleib oder Aufnahme auf freiwilliger Grundlage](#53)  
[§ 54     Entlassung der Untersuchungsgefangenen](#54)

### Abschnitt 9  
Grundversorgung und Freizeit

[§ 55     Einbringen von Gegenständen](#55)  
[§ 56     Gewahrsam an Gegenständen](#56)  
[§ 57     Ausstattung des Haftraums](#57)  
[§ 58     Aufbewahrung und Vernichtung von Gegenständen](#58)  
[§ 59     Religiöse Schriften und Gegenstände](#59)  
[§ 60     Zeitungen und Zeitschriften](#60)  
[§ 61     Rundfunk, Informations- und Unterhaltungselektronik](#61)  
[§ 62     Kleidung](#62)  
[§ 63     Verpflegung und Einkauf](#63)  
[§ 64     Annehmlichkeiten im Vollzug der Untersuchungshaft](#64)  
[§ 65     Freizeit](#65)

### Abschnitt 10  
Vergütung, Gelder der Gefangenen und Kosten

[§ 66     Vergütung](#66)  
[§ 67     Eigengeld](#67)  
[§ 68     Taschengeld](#68)  
[§ 69     Konten, Bargeld](#69)  
[§ 70     Hausgeld](#70)  
[§ 71     Zweckgebundene Einzahlungen](#71)  
[§ 72     Haftkostenbeitrag, Kostenbeteiligung](#72)  
[§ 73     Eingliederungsgeld](#73)

### Abschnitt 11  
Gesundheitsfürsorge

[§ 74     Art und Umfang der medizinischen Leistungen, Kostenbeteiligung](#74)  
[§ 75     Durchführung der medizinischen Leistungen, Forderungsübergang](#75)  
[§ 76     Ärztliche Behandlung zur sozialen Eingliederung](#76)  
[§ 77     Gesundheitsschutz und Hygiene](#77)  
[§ 78     Krankenbehandlung während Lockerungen](#78)  
[§ 79     Zwangsmaßnahmen auf dem Gebiet der Gesundheitsfürsorge](#79)  
[§ 80     Benachrichtigungspflicht](#80)

### Abschnitt 12  
Religionsausübung

[§ 81     Seelsorge](#81)  
[§ 82     Religiöse Veranstaltungen](#82)  
[§ 83     Weltanschauungsgemeinschaften](#83)

### Abschnitt 13  
Sicherheit und Ordnung

[§ 84     Grundsatz](#84)  
[§ 85     Allgemeine Verhaltenspflichten](#85)  
[§ 86     Absuchung, Durchsuchung](#86)  
[§ 87     Sichere Unterbringung](#87)  
[§ 88     Maßnahmen zur Feststellung von Suchtmittelgebrauch](#88)  
[§ 89     Festnahmerecht](#89)  
[§ 90     Besondere Sicherungsmaßnahmen](#90)  
[§ 91     Anordnung besonderer Sicherungsmaßnahmen, Verfahren](#91)  
[§ 92     Ärztliche Überwachung](#92)

### Abschnitt 14  
Unmittelbarer Zwang

[§ 93     Begriffsbestimmungen](#93)  
[§ 94     Allgemeine Voraussetzungen](#94)  
[§ 95     Grundsatz der Verhältnismäßigkeit](#95)  
[§ 96     Androhung](#96)  
[§ 97     Schusswaffengebrauch](#97)

### Abschnitt 15  
Erzieherische Maßnahmen, Einvernehmliche Streitbeilegung, Disziplinarmaßnahmen

[§ 98     Erzieherische Maßnahmen](#98)  
[§ 99     Einvernehmliche Streitbeilegung](#99)  
[§ 100   Disziplinarmaßnahmen](#100)  
[§ 101   Vollzug der Disziplinarmaßnahmen, Aussetzung zur Bewährung](#101)  
[§ 102   Disziplinarbefugnis](#102)  
[§ 103   Verfahren](#103)

### Abschnitt 16  
Aufhebung von Maßnahmen, Beschwerde

[§ 104   Aufhebung von Maßnahmen](#104)  
[§ 105   Beschwerderecht](#105)

### Abschnitt 17  
Kriminologische Forschung

[§ 106   Evaluation, kriminologische Forschung, Berichtspflicht](#106)

### Abschnitt 18  
Aufbau und Organisation der Anstalten

[§ 107   Anstalten](#107)  
[§ 108   Festsetzung der Belegungsfähigkeit, Verbot der Überbelegung](#108)  
[§ 109   Anstaltsleitung](#109)  
[§ 110   Bedienstete](#110)  
[§ 111   Anstaltsseelsorgerinnen, Anstaltsseelsorger](#111)  
[§ 112   Medizinische Versorgung](#112)  
[§ 113   Interessenvertretung der Gefangenen](#113)  
[§ 114   Hausordnung](#114)

### Abschnitt 19  
Aufsicht, Beirat

[§ 115   Aufsichtsbehörde](#115)  
[§ 116   Vollstreckungsplan, Vollzugsgemeinschaften](#116)  
[§ 117   Beirat](#117)

### Abschnitt 20  
Verhinderung von Mobilfunkverkehr

[§ 118   Verbot und Störung des Mobilfunkverkehrs](#118)

### Abschnitt 21  
Vollzug des Strafarrests

[§ 119   Grundsatz](#119)  
[§ 120   Besondere Bestimmungen](#120)

### Abschnitt 22  
Datenschutz

[§ 121   Anwendung des Brandenburgischen Polizei-, Justizvollzugs- und Maßregelvollzugsdatenschutzgesetzes](#121)  
[§ 122   Grundsatz, Begriffsbestimmungen](#122)  
[§ 123   Erhebung von Daten über Gefangene bei Dritten](#123)  
[§ 124   Erhebung von Daten über andere Personen](#124)  
[§ 125   Unterrichtungspflichten](#125)  
[§ 126   Besondere Formen der Datenerhebung](#126)  
[§ 127   Übermittlung und Nutzung für weitere Zwecke](#127)  
[§ 128   Datenübermittlung an öffentliche Stellen](#128)  
[§ 129   Verarbeitung besonders erhobener Daten](#129)  
[§ 130   Mitteilung über Haftverhältnisse](#130)  
[§ 131   Überlassung von Akten](#131)  
[§ 132   Kenntlichmachung in der Anstalt, Lichtbildausweise](#132)  
[§ 133   Offenbarungspflichten und -befugnisse der Berufsgeheimnisträgerinnen und Berufsgeheimnisträger](#133)  
[§ 134   _(weggefallen)_](#134)  
[§ 135   Auskunft an die Betroffenen, Akteneinsicht](#135)  
[§ 136   Auskunft und Akteneinsicht zur Wahrnehmung der Aufgaben des Europäischen Ausschusses zur Verhütung von Folter und unmenschlicher oder erniedrigender Behandlung oder Strafe](#136)  
[§ 137   Löschung](#137)  
[§ 138   Löschung besonders erhobener Daten](#138)  
[§ 139   Sperrung und Verwendungsbeschränkungen](#139)  
[§ 140   Aufbewahrungsfristen, Fristberechnung](#140)

### Abschnitt 23  
Schlussbestimmungen

[§ 141   Einschränkung von Grundrechten](#141)  
[§ 142   Übergangsregelung](#142)  
[§ 143   Inkrafttreten, Außerkrafttreten](#143)

### Abschnitt 1  
Allgemeine Bestimmungen

### § 1  
Anwendungsbereich, Allgemeine Begriffsbestimmungen

(1) Dieses Gesetz regelt den Vollzug der Freiheitsstrafe, der Jugendstrafe, der Untersuchungshaft und des Strafarrests in Justizvollzugsanstalten (Anstalten).

(2) Für den Vollzug der Haft nach § 127b Absatz 2, § 230 Absatz 2, §§ 236, 329 Absatz 4 Satz 1, § 412 Satz 1 und § 453c der Strafprozessordnung sowie der einstweiligen Unterbringung nach § 275a Absatz 6 der Strafprozessordnung gelten die Bestimmungen für den Vollzug der Untersuchungshaft entsprechend.

(3) Gefangene im Sinne dieses Gesetzes sind Strafgefangene, Jugendstrafgefangene und Untersuchungsgefangene.

(4) Junge Untersuchungsgefangene im Sinne dieses Gesetzes sind solche, die zur Tatzeit das 21.
Lebensjahr noch nicht vollendet hatten und die das 24.
Lebensjahr noch nicht vollendet haben.

(5) Junge Gefangene im Sinne dieses Gesetzes sind Jugendstrafgefangene und junge Untersuchungsgefangene.

### § 2  
Ziel und Aufgabe des Vollzugs der Freiheits- und Jugendstrafe

Der Vollzug der Freiheitsstrafe und der Jugendstrafe dient dem Ziel, die Straf- und Jugendstrafgefangenen zu befähigen, künftig in sozialer Verantwortung ein Leben ohne Straftaten zu führen.
Er hat die Aufgabe, die Allgemeinheit vor weiteren Straftaten zu schützen.

### § 3  
Aufgabe des Vollzugs der Untersuchungshaft, Zusammenarbeit

(1) Der Vollzug der Untersuchungshaft hat die Aufgabe, durch sichere Unterbringung der Untersuchungsgefangenen die Durchführung eines geordneten Strafverfahrens zu gewährleisten und der Gefahr weiterer Straftaten zu begegnen.

(2) Die Anstalt arbeitet eng mit Gericht und Staatsanwaltschaft zusammen, um die Aufgabe des Vollzugs der Untersuchungshaft zu erfüllen und die Sicherheit und Ordnung der Anstalt zu gewährleisten.

(3) Die Anstalt hat Anordnungen nach § 119 Absatz 1 der Strafprozessordnung zu beachten und umzusetzen.

### § 4  
Stellung der Gefangenen

(1) Die Persönlichkeit der Gefangenen ist zu achten.
Ihre Selbstständigkeit im Vollzugsalltag ist so weit wie möglich zu erhalten und zu fördern.

(2) Die Gefangenen werden an der Gestaltung des Vollzugsalltags beteiligt.
Vollzugliche Maßnahmen sind ihnen regelmäßig zu erläutern.

(3) Die Gefangenen unterliegen den in diesem Gesetz vorgesehenen Beschränkungen ihrer Freiheit.
Soweit das Gesetz eine besondere Regelung nicht enthält, dürfen ihnen nur Beschränkungen auferlegt werden, die zur Aufrechterhaltung der Sicherheit oder zur Abwendung einer schwerwiegenden Störung der Ordnung der Anstalt oder im Vollzug der Untersuchungshaft zur Umsetzung einer Anordnung nach § 119 Absatz 1 der Strafprozessordnung unerlässlich sind.
Sie müssen in einem angemessenen Verhältnis zum Zweck der Anordnung stehen und dürfen die Gefangenen nicht mehr und nicht länger als notwendig beeinträchtigen.

### § 5  
Besondere Stellung der Untersuchungsgefangenen

Die Untersuchungsgefangenen gelten als unschuldig.
Sie sind so zu behandeln, dass der Anschein vermieden wird, sie würden zur Verbüßung einer Strafe festgehalten.

### § 6   
Mitwirkung im Vollzug der Freiheits- und Jugendstrafe

(1) Zur Erreichung des Vollzugsziels bedarf es der Mitwirkung der Straf- und Jugendstrafgefangenen.
Ihre Bereitschaft hierzu ist zu wecken und zu fördern.

(2) Die Jugendstrafgefangenen sind verpflichtet, an der Erreichung des Vollzugsziels mitzuwirken.

### § 7  
Allgemeine Gestaltungsgrundsätze

(1) Das Leben im Vollzug ist den allgemeinen Lebensverhältnissen so weit wie möglich anzugleichen.

(2) Schädlichen Folgen des Freiheitsentzugs ist entgegenzuwirken.

(3) Ein besonderes Augenmerk ist auf die Verhütung von Selbsttötungen und den Schutz der Gefangenen vor Übergriffen Mitgefangener zu richten.

(4) Die unterschiedlichen Bedürfnisse der Gefangenen, insbesondere im Hinblick auf Geschlecht, Alter, Herkunft, Religion, Behinderung und sexuelle Identität werden bei der Vollzugsgestaltung berücksichtigt.

### § 8  
Grundsätze der Gestaltung des Vollzugs der Freiheits- und Jugendstrafe

(1) Der Vollzug der Freiheits- und Jugendstrafe ist auf die Auseinandersetzung der Straf- und Jugendstrafgefangenen mit ihren Straftaten, deren Ursachen und deren Folgen auszurichten.
Das Bewusstsein für die den Opfern zugefügten Schäden soll geweckt werden.

(2) Der Vollzug der Freiheits- und Jugendstrafe wird von Beginn an auf die Eingliederung der Straf- und Jugendstrafgefangenen in das Leben in Freiheit ausgerichtet.

(3) Die Straf- und Jugendstrafgefangenen sind zu einer selbstständigen, eigenverantwortlichen und gemeinschaftsfähigen Lebensführung in Achtung der Rechte anderer zu befähigen.

(4) Strafgefangene mit angeordneter oder vorbehaltener Sicherungsverwahrung und Jugendstrafgefangene mit vorbehaltener Sicherungsverwahrung sind individuell und intensiv zu betreuen, um ihre Unterbringung in der Sicherungsverwahrung entbehrlich zu machen.
Soweit standardisierte Maßnahmen nicht ausreichen oder keinen Erfolg versprechen, sind individuell zugeschnittene Behandlungsangebote zu unterbreiten.

(5) Der Bezug der Straf- und Jugendstrafgefangenen zum gesellschaftlichen Leben ist zu wahren und zu fördern.
Personen und Einrichtungen außerhalb des Vollzugs sollen in den Vollzugsalltag einbezogen werden.
Straf- und Jugendstrafgefangenen ist so bald wie möglich die Teilnahme am Leben in der Freiheit zu gewähren.

### § 9  
Erzieherische Gestaltung des Vollzugs der Jugendstrafe

(1) Der Vollzug der Jugendstrafe ist erzieherisch zu gestalten und auf die Förderung der Jugendstrafgefangenen auszurichten.

(2) Erziehung und Förderung erfolgen durch Maßnahmen und Programme zur Entwicklung und Stärkung der Fähigkeiten und Fertigkeiten der Jugendstrafgefangenen im Hinblick auf die Erreichung des Vollzugsziels.

(3) Durch differenzierte Angebote soll auf den jeweiligen Entwicklungsstand und den unterschiedlichen Erziehungs- und Förderbedarf der Jugendstrafgefangenen eingegangen werden.
Die besonderen Lebenslagen und Bedürfnisse von minderjährigen Jugendstrafgefangenen sind zu berücksichtigen.

(4) Die Maßnahmen und Programme richten sich insbesondere auf die Auseinandersetzung mit den eigenen Straftaten, deren Ursachen und Folgen, die schulische und berufliche Qualifizierung, die soziale Integration und die verantwortliche Gestaltung des alltäglichen Zusammenlebens, der freien Zeit sowie der Außenkontakte.

(5) Die Personensorgeberechtigten und die Eltern von volljährigen Jugendstrafgefangenen mit deren Einverständnis sind, soweit dies möglich ist und dem Vollzugsziel nicht zuwiderläuft, in die Planung und Gestaltung des Vollzugs einzubeziehen.

### § 10  
Erzieherische Gestaltung des Vollzugs der Untersuchungshaft an jungen Untersuchungsgefangenen

(1) Für den Vollzug der Untersuchungshaft an jungen Untersuchungsgefangenen gilt § 9 Absatz 1 und 3 entsprechend.

(2) Die Personensorgeberechtigten sind, soweit dies möglich ist, in die Gestaltung des Vollzugs einzubeziehen, ebenso die Eltern volljähriger junger Untersuchungsgefangener auf deren Antrag.

(3) Von der Anwendung der Bestimmungen dieses Gesetzes über junge Untersuchungsgefangene kann abgesehen werden, wenn diese volljährig sind und die erzieherische Ausgestaltung des Vollzugs für sie nicht oder nicht mehr angezeigt ist.
Diese Bestimmungen können ausnahmsweise auch über die Vollendung des 24.
Lebensjahres hinaus angewendet werden, wenn dies im Hinblick auf die voraussichtlich nur noch geringe Dauer der Untersuchungshaft zweckmäßig erscheint.

(4) Beschränkungen können minderjährigen Untersuchungsgefangenen auch auferlegt werden, soweit es dringend geboten ist, um sie vor einer Gefährdung ihrer Entwicklung zu bewahren.

### § 11  
Soziale Hilfe

(1) Die Gefangenen werden darin unterstützt, ihre persönlichen, wirtschaftlichen und sozialen Schwierigkeiten zu beheben.
Sie sollen dazu angeregt und in die Lage versetzt werden, ihre Angelegenheiten selbst zu regeln.
Bei Freiheits- und Jugendstrafen bis zu zwei Jahren hält die bislang zuständige Bewährungshelferin oder der bislang zuständige Bewährungshelfer auch während des Vollzugs Kontakt zu ihren oder seinen Probanden und beteiligt sich an der Gewährung der Hilfen.

(2) Die Straf- und Jugendstrafgefangenen sollen angehalten werden, den durch die Straftat verursachten materiellen und immateriellen Schaden wieder gutzumachen und eine Schuldenregulierung herbeizuführen.
Sie erhalten Hilfe insbesondere bei der Feststellung und Regelung von Unterhaltsverpflichtungen und Schadensersatzforderungen sowie Beratung in sozialen und finanziellen Angelegenheiten.

(3) Die Beratung der Untersuchungsgefangenen soll die Benennung von Stellen und Einrichtungen außerhalb der Anstalt umfassen, die sich um eine Vermeidung der weiteren Untersuchungshaft bemühen.
Auf Wunsch sind den Untersuchungsgefangenen Stellen und Einrichtungen zu benennen, die sie in ihrem Bestreben unterstützen können, einen Ausgleich mit dem Tatopfer zu erreichen oder auf andere Weise zur Wiedergutmachung beizutragen.

### Abschnitt 2  
Aufnahme, Diagnose, Vollzugs- und Eingliederungsplanung

### § 12   
Aufnahmeverfahren

(1) Mit den Gefangenen wird unverzüglich nach der Aufnahme ein Zugangsgespräch geführt, in dem ihre gegenwärtige Lebenssituation erörtert wird und sie über ihre Rechte und Pflichten in einer für sie verständlichen Form informiert werden.
Soweit erforderlich, ist eine Sprachmittlerin oder ein Sprachmittler oder eine Gebärdendolmetscherin oder ein Gebärdendolmetscher hinzuzuziehen.
Den Gefangenen wird ein Exemplar der Hausordnung ausgehändigt oder in anderer Form zur Verfügung gestellt.
Dieses Gesetz, die von ihm in Bezug genommenen Gesetze sowie die zu seiner Ausführung erlassenen Rechtsverordnungen und Verwaltungsvorschriften sind den Gefangenen auf Verlangen zugänglich zu machen.

(2) Während des Aufnahmeverfahrens dürfen andere Gefangene nicht zugegen sein.

(3) Die Gefangenen werden alsbald ärztlich untersucht.

(4) Die Gefangenen werden dabei unterstützt, etwa notwendige Maßnahmen für hilfsbedürftige Angehörige, zur Erhaltung des Arbeitsplatzes und der Wohnung und zur Sicherung ihrer Habe außerhalb der Anstalt zu veranlassen.

(5) Den Untersuchungsgefangenen ist Gelegenheit zu geben, eine Angehörige oder einen Angehörigen oder eine Vertrauensperson von der Aufnahme in die Anstalt zu benachrichtigen.

(6) Die Personensorgeberechtigen, die zuständige Vollstreckungsleiterin oder der zuständige Vollstreckungsleiter und das Jugendamt werden von der Aufnahme der jungen Gefangenen unverzüglich unterrichtet.

(7) Bei Strafgefangenen, die eine Ersatzfreiheitsstrafe verbüßen, sind die Möglichkeiten der Abwendung der Vollstreckung durch freie Arbeit oder ratenweise Tilgung der Geldstrafe zu erörtern und zu fördern, um so auf eine möglichst baldige Entlassung hinzuwirken.

### § 13  
Diagnoseverfahren

(1) Im Vollzug der Freiheits- und Jugendstrafe schließt sich an das Aufnahmeverfahren das Diagnoseverfahren zur Vorbereitung der Vollzugs- und Eingliederungsplanung an.

(2) Das Diagnoseverfahren muss wissenschaftlichen Erkenntnissen genügen.
Insbesondere bei Strafgefangenen mit angeordneter oder vorbehaltener Sicherungsverwahrung und Jugendstrafgefangenen mit vorbehaltener Sicherungsverwahrung ist es von Personen mit einschlägiger wissenschaftlicher Qualifikation durchzuführen.

(3) Das Diagnoseverfahren erstreckt sich auf die Persönlichkeit, die Lebensverhältnisse, die Ursachen und Umstände der Straftat sowie alle sonstigen Gesichtspunkte, deren Kenntnis für eine zielgerichtete und wirkungsorientierte Vollzugsgestaltung und die Eingliederung nach der Entlassung notwendig erscheint.
Neben den Unterlagen aus der Vollstreckung und dem Vollzug vorangegangener Freiheitsentziehungen sind insbesondere auch Erkenntnisse der Gerichts-, Jugendgerichts- und Bewährungshilfe sowie der Führungsaufsichtsstellen einzubeziehen.

(4) Im Diagnoseverfahren werden die im Einzelfall die Straffälligkeit begünstigenden Faktoren ermittelt.
Gleichermaßen werden die Fähigkeiten der Straf- und Jugendstrafgefangenen ermittelt, deren Stärkung einer erneuten Straffälligkeit entgegenwirken kann.

(5) Im Vollzug der Freiheitsstrafe kann bei einer voraussichtlichen Vollzugsdauer bis zu einem Jahr das Diagnoseverfahren auf die Umstände beschränkt werden, deren Kenntnis für eine angemessene Vollzugsgestaltung unerlässlich und für die Eingliederung erforderlich ist.
Unabhängig von der Vollzugsdauer gilt dies auch, wenn ausschließlich Ersatzfreiheitsstrafen zu vollziehen sind.

(6) Im Vollzug der Jugendstrafe ist das Diagnoseverfahren maßgeblich auf die Ermittlung des Förder- und Erziehungsbedarfs auszurichten.

(7) Das Ergebnis des Diagnoseverfahrens wird mit den Straf- und Jugendstrafgefangenen erörtert.

### § 14   
Vollzugs- und Eingliederungsplanung

(1) Auf der Grundlage des Ergebnisses des Diagnoseverfahrens wird ein Vollzugs- und Eingliederungsplan erstellt.
Er zeigt den Straf- und Jugendstrafgefangenen bereits zu Beginn der Haftzeit unter Berücksichtigung der voraussichtlichen Vollzugsdauer die zur Erreichung des Vollzugsziels erforderlichen Maßnahmen auf.
Daneben kann er weitere Hilfsangebote und Empfehlungen enthalten.
Auf die Fähigkeiten, Fertigkeiten und Neigungen der Straf- und Jugendstrafgefangenen ist Rücksicht zu nehmen.
Stehen zur Erreichung des Vollzugsziels mehrere geeignete Maßnahmen zur Verfügung, so haben die Straf- und Jugendstrafgefangenen ein Wahlrecht.

(2) Der Vollzugs- und Eingliederungsplan wird regelmäßig innerhalb der ersten acht Wochen nach der Aufnahme erstellt.
Diese Frist verkürzt sich bei einer voraussichtlichen Vollzugsdauer von unter einem Jahr auf vier Wochen.

(3) Der Vollzugs- und Eingliederungsplan sowie die darin vorgesehenen Maßnahmen werden für Straf- und Jugendstrafgefangene regelmäßig alle sechs Monate, spätestens aber alle zwölf Monate überprüft und fortgeschrieben.
Bei Jugendstrafen von weniger als drei Jahren erfolgt die Überprüfung regelmäßig alle vier Monate.
Die Entwicklung der Straf- und Jugendstrafgefangenen und die in der Zwischenzeit gewonnenen Erkenntnisse sind zu berücksichtigen.
Die durchgeführten Maßnahmen sind zu dokumentieren.

(4) Die Vollzugs- und Eingliederungsplanung wird mit den Straf- und Jugendstrafgefangenen erörtert.
Dabei werden deren Anregungen und Vorschläge einbezogen, soweit sie der Erreichung des Vollzugsziels dienen.

(5) Zur Erstellung und Fortschreibung des Vollzugs- und Eingliederungsplans führt die Anstaltsleiterin oder der Anstaltsleiter eine Konferenz mit den an der Vollzugsgestaltung maßgeblich Beteiligten durch.
Standen die Straf- und Jugendstrafgefangenen vor ihrer Inhaftierung unter Bewährungs- oder Führungsaufsicht, ist auch die für sie bislang zuständige Bewährungshelferin oder der für sie bislang zuständige Bewährungshelfer an der ersten Konferenz zu beteiligen.
Bei Freiheits- und Jugendstrafen bis zu zwei Jahren ist ihre oder seine regelmäßige Teilnahme auch an den weiteren Konferenzen vorzusehen.
Den Straf- und Jugendstrafgefangenen wird der Vollzugs- und Eingliederungsplan in der Konferenz eröffnet und erläutert.
Sie können auch darüber hinaus an der Konferenz beteiligt werden.

(6) Personen außerhalb des Vollzugs, die an der Eingliederung mitwirken, sind nach Möglichkeit in die Planung einzubeziehen.
Sie können mit Zustimmung der Straf- und Jugendstrafgefangenen auch an der Konferenz beteiligt werden.

(7) Werden die Straf- und Jugendstrafgefangenen nach der Entlassung voraussichtlich unter Bewährungs- oder Führungsaufsicht gestellt, so ist der künftig zuständigen Bewährungshelferin oder dem künftig zuständigen Bewährungshelfer in den letzten zwölf Monaten vor dem voraussichtlichen Entlassungszeitpunkt die Teilnahme an der Konferenz zu ermöglichen und sind ihr oder ihm der Vollzugs- und Eingliederungsplan und seine Fortschreibungen zu übersenden.

(8) Der Vollzugs- und Eingliederungsplan und seine Fortschreibungen werden den Straf- und Jugendstrafgefangenen ausgehändigt.
Im Vollzug der Jugendstrafe werden sie der Vollstreckungsleiterin oder dem Vollstreckungsleiter und auf Verlangen den Personensorgeberechtigten übersandt.

### § 15   
Inhalt des Vollzugs- und Eingliederungsplans

(1) Der Vollzugs- und Eingliederungsplan sowie seine Fortschreibungen enthalten insbesondere folgende Angaben:

1.  Zusammenfassung der für die Vollzugs- und Eingliederungsplanung maßgeblichen Ergebnisse des Diagnoseverfahrens,
2.  voraussichtlicher Entlassungszeitpunkt,
3.  Unterbringung im geschlossenen oder offenen Vollzug,
4.  Unterbringung in einer Wohneinheit,
5.  Maßnahmen zur Förderung der Mitwirkungsbereitschaft,
6.  Unterbringung in einer Wohngruppe und Teilnahme am Wohngruppenvollzug,
7.  Unterbringung in einer sozialtherapeutischen Abteilung und Teilnahme an deren Behandlungsprogrammen,
8.  Teilnahme an einzel- oder gruppentherapeutischen Maßnahmen, insbesondere Psychotherapie,
9.  Teilnahme an psychiatrischen Behandlungsmaßnahmen,
10.  Teilnahme an Maßnahmen zur Behandlung von Suchtmittelabhängigkeit und -missbrauch,
11.  Teilnahme an Trainingsmaßnahmen zur Verbesserung der sozialen Kompetenzen,
12.  Teilnahme an schulischen und beruflichen Qualifizierungsmaßnahmen einschließlich Alphabetisierungs- und Deutschkursen,
13.  Teilnahme an arbeitstherapeutischen Maßnahmen oder am Arbeitstraining,
14.  Arbeit,
15.  Freies Beschäftigungsverhältnis, Selbstbeschäftigung,
16.  Teilnahme an Sportangeboten und Maßnahmen zur strukturierten Gestaltung der Freizeit,
17.  Ausführungen, Außenbeschäftigung,
18.  Lockerungen zur Erreichung des Vollzugsziels,
19.  Aufrechterhaltung, Förderung und Gestaltung von Außenkontakten, insbesondere familiärer Beziehungen,
20.  Schuldnerberatung, Schuldenregulierung und Erfüllung von Unterhaltspflichten,
21.  Ausgleich von Tatfolgen,
22.  Maßnahmen zur Vorbereitung von Entlassung, Eingliederung und Nachsorge, Bildung eines Eingliederungsgeldes und
23.  Frist zur Fortschreibung des Vollzugs- und Eingliederungsplans.

Bei angeordneter oder vorbehaltener Sicherungsverwahrung enthalten der Vollzugs- und Eingliederungsplan sowie seine Fortschreibungen darüber hinaus Angaben zu sonstigen Maßnahmen im Sinne des § 8 Absatz 4 Satz 2 und einer Antragstellung im Sinne des § 119a Absatz 2 des Strafvollzugsgesetzes.

(2) Bei Strafgefangenen sind Maßnahmen nach Absatz 1 Satz 1 Nummer 7 bis 13 und Satz 2, die nach dem Ergebnis des Diagnoseverfahrens als zur Erreichung des Vollzugsziels zwingend erforderlich erachtet werden, als solche zu kennzeichnen und gehen allen anderen Maßnahmen vor.
Andere Maßnahmen dürfen nicht gestattet werden, soweit sie die Teilnahme an Maßnahmen nach Satz 1 beeinträchtigen würden.

(3) Die Jugendstrafgefangenen sind verpflichtet, an den im Vollzugs- und Eingliederungsplan als erforderlich erachteten Maßnahmen teilzunehmen.
§ 30 Absatz 1 bleibt unberührt.

(4) Spätestens ein Jahr vor dem voraussichtlichen Entlassungszeitpunkt hat die Planung zur Vorbereitung der Eingliederung zu beginnen.
Anknüpfend an die bisherige Vollzugsplanung werden ab diesem Zeitpunkt die Maßnahmen nach Absatz 1 Satz 1 Nummer 22 konkretisiert oder ergänzt.
Insbesondere ist Stellung zu nehmen zu:

1.  Unterbringung im offenen Vollzug, in einer Eingliederungsabteilung oder in einer Übergangseinrichtung,
2.  Unterkunft sowie Arbeit oder Ausbildung nach der Entlassung,
3.  Unterstützung bei notwendigen Behördengängen und der Beschaffung der notwendigen persönlichen Dokumente,
4.  Beteiligung der Bewährungshilfe und der forensischen Ambulanzen,
5.  Kontaktaufnahme zu Einrichtungen der Entlassenenhilfe und freien Trägern der Straffälligenhilfe sowie gegebenenfalls Unterrichtung der Personensorgeberechtigten und des Jugendamtes,
6.  Fortsetzung von im Vollzug noch nicht abgeschlossenen Maßnahmen,
7.  Anregung von Auflagen und Weisungen für die Bewährungs- oder Führungsaufsicht,
8.  Vermittlung in nachsorgende Maßnahmen und
9.  nachgehende Betreuung durch Bedienstete.

Ab diesem Zeitpunkt enthält der Vollzugs- und Eingliederungsplan zudem für diejenigen Straf- und Jugendstrafgefangenen, bei denen der Eintritt der Führungsaufsicht zu erwarten ist, eine Gefährlichkeitsprognose.
Diese dient, vorbehaltlich der Entscheidung des Gerichts, der Bewährungshilfe als Grundlage für die weitere Arbeit mit den Straf- und Jugendstrafgefangenen.

### § 16  
Ermittlung des Förder- und Erziehungsbedarfs der jungen Untersuchungsgefangenen, Maßnahmen

(1) Nach dem Aufnahmeverfahren wird der Förder- und Erziehungsbedarf der jungen Untersuchungsgefangenen unter Berücksichtigung ihrer Persönlichkeit und ihrer Lebensverhältnisse ermittelt.

(2) In einer Konferenz mit an der Erziehung maßgeblich beteiligten Bediensteten werden der Förder- und Erziehungsbedarf erörtert und die sich daraus ergebenden Maßnahmen festgelegt.
Diese werden mit den jungen Untersuchungsgefangenen besprochen und den Personensorgeberechtigten auf Verlangen mitgeteilt.

(3) Den jungen Untersuchungsgefangenen ist eine Bedienstete oder ein Bediensteter als besondere Vertrauensperson zuzuordnen.

### Abschnitt 3  
Unterbringung und Verlegung

### § 17   
Trennungsgrundsätze

(1) Jeweils getrennt voneinander werden untergebracht

1.  männliche und weibliche Gefangene,
2.  Strafgefangene, Jugendstrafgefangene und Untersuchungsgefangene und
3.  junge Untersuchungsgefangene und die übrigen Untersuchungsgefangenen.

Die Unterbringung erfolgt in eigenständigen Anstalten, zumindest in getrennten Abteilungen.

(2) Abweichend von Absatz 1 Satz 1 Nummer 2 können Untersuchungsgefangene zusammen mit Strafgefangenen untergebracht werden

1.  mit Zustimmung der einzelnen Untersuchungsgefangenen,
2.  zur Umsetzung einer Anordnung nach § 119 Absatz 1 der Strafprozessordnung oder
3.  aus Gründen der Sicherheit oder Ordnung der Anstalt.

Das gilt für junge Untersuchungsgefangene nur, wenn eine erzieherische Gestaltung des Vollzugs gewährleistet bleibt und schädliche Einflüsse auf die jungen Untersuchungsgefangenen nicht zu befürchten sind.
Unter den Voraussetzungen von Satz 1 und 2 können sie auch mit den übrigen Untersuchungsgefangenen und mit Jugendstrafgefangenen untergebracht werden.

(3) Über Absatz 2 hinaus können Gefangene ausnahmsweise mit solchen anderer Haftarten untergebracht werden, wenn ihre geringe Anzahl eine getrennte Unterbringung nicht zulässt und das Vollzugsziel nicht gefährdet wird.
Bei jungen Gefangenen muss zudem die erzieherische Gestaltung des Vollzugs gewährleistet sein.

(4) Absatz 1 gilt nicht für eine Unterbringung zum Zwecke der medizinischen Behandlung.

(5) Gemeinsame Maßnahmen, insbesondere zur schulischen und beruflichen Qualifizierung, sind zulässig.

### § 18  
Unterbringung während der Einschlusszeiten

(1) Die Gefangenen werden in ihren Hafträumen einzeln untergebracht.

(2) Eine gemeinsame Unterbringung ist zulässig:

1.  auf Antrag der Gefangenen, wenn schädliche Einflüsse nicht zu befürchten sind, oder
2.  wenn Gefangene hilfsbedürftig sind oder eine Gefahr für Leben oder Gesundheit besteht.

In den Fällen des Satzes 1 Nummer 2 bedarf es der Zustimmung der nicht gefährdeten oder hilfsbedürftigen Gefangenen zur gemeinsamen Unterbringung.

(3) Darüber hinaus ist eine gemeinsame Unterbringung nur vorübergehend und zur Überwindung einer nicht vorhersehbaren Notlage zulässig.

### § 19  
Aufenthalt außerhalb der Einschlusszeiten

(1) Außerhalb der Einschlusszeiten dürfen sich die Gefangenen in Gemeinschaft aufhalten.

(2) Der gemeinschaftliche Aufenthalt kann eingeschränkt werden,

1.  wenn es die Sicherheit oder Ordnung der Anstalt erfordert,
2.  wenn ein schädlicher Einfluss auf andere Gefangene zu befürchten ist,
3.  während des Diagnoseverfahrens, aber nicht länger als sechs Wochen,
4.  bei jungen Gefangenen, wenn dies aus erzieherischen Gründen unerlässlich ist,
5.  zur Umsetzung einer Anordnung nach § 119 Absatz 1 der Strafprozessordnung oder
6.  bei jungen Untersuchungsgefangenen während der ersten zwei Wochen nach der Aufnahme.

### § 20  
Unterbringung in einer Wohneinheit

(1) Zur Erhaltung ihrer Selbstständigkeit können Gefangene, die zu korrekter Führung unter geringerer Aufsicht fähig sind und die Bereitschaft zur Einordnung in die Gemeinschaft sowie zur Mitarbeit am Vollzugsziel mitbringen, mit ihrer Zustimmung in einer Wohneinheit untergebracht werden.

(2) Eine Wohneinheit wird in einem baulich abgegrenzten Bereich eingerichtet, zu dem neben den Hafträumen weitere Räume und Einrichtungen zur gemeinsamen Nutzung gehören.
Diese Form der Unterbringung ermöglicht es den dort Untergebrachten, ihren Vollzugsalltag selbstständig zu regeln.

### § 21  
Unterbringung von Eltern mit Kindern

(1) Ein Kind kann mit Zustimmung des Aufenthaltsbestimmungsberechtigten bis zur Vollendung des dritten Lebensjahres gemeinsam mit seiner Mutter oder seinem Vater in der Anstalt untergebracht werden, wenn die baulichen Gegebenheiten dies zulassen und Sicherheitsgründe nicht entgegenstehen.
Vor der Unterbringung ist das Jugendamt zu hören.

(2) Die Unterbringung erfolgt auf Kosten der oder des für das Kind Unterhaltspflichtigen.
Von der Geltendmachung des Kostenersatzanspruchs kann ausnahmsweise abgesehen werden, wenn hierdurch die gemeinsame Unterbringung von Mutter oder Vater und Kind gefährdet würde.

### § 22  
Geschlossener und offener Vollzug

(1) Die Straf- und Jugendstrafgefangenen werden im geschlossenen oder im offenen Vollzug untergebracht.
Anstalten des offenen Vollzugs sehen keine oder nur verminderte Vorkehrungen gegen Entweichungen vor.

(2) Die Strafgefangenen sind im offenen Vollzug unterzubringen, wenn sie dessen Anforderungen genügen, insbesondere verantwortet werden kann zu erproben, dass sie sich dem Vollzug nicht entziehen oder die Möglichkeiten des offenen Vollzugs nicht zu Straftaten missbrauchen werden.
Sie können mit ihrer Zustimmung im geschlossenen Vollzug untergebracht werden oder verbleiben, wenn dies der Erreichung des Vollzugsziels dient.

(3) Die Jugendstrafgefangenen sind im offenen Vollzug unterzubringen, wenn sie dessen Anforderungen genügen, insbesondere verantwortet werden kann zu erproben, dass sie sich dem Vollzug nicht entziehen oder die Möglichkeiten des offenen Vollzugs nicht zur Begehung von Straftaten missbrauchen werden.
Sie können im geschlossenen Vollzug untergebracht werden oder verbleiben, wenn dies der Erreichung des Vollzugsziels dient.

(4) Genügen die Straf- und Jugendstrafgefangenen den Anforderungen des offenen Vollzugs nicht mehr, werden sie im geschlossenen Vollzug untergebracht.

(5) Die Untersuchungsgefangenen werden im geschlossenen Vollzug untergebracht.

### § 23  
Wohngruppenvollzug

(1) Der Wohngruppenvollzug dient der Einübung sozialverträglichen Zusammenlebens, insbesondere von Toleranz sowie der Übernahme von Verantwortung für sich und andere.
Er ermöglicht den dort Untergebrachten, ihren Vollzugsalltag weitgehend selbstständig zu regeln.

(2) Eine Wohngruppe wird in einem baulich abgegrenzten Bereich für bis zu 15 Personen eingerichtet, zu dem neben den Hafträumen weitere Räume und Einrichtungen zur gemeinsamen Nutzung gehören.
Sie wird in der Regel von fest zugeordneten Bediensteten verschiedener Fachrichtungen betreut.

(3) Geeignete junge Gefangene sind in Wohngruppen unterzubringen.
Nicht geeignet sind in der Regel junge Gefangene, die aufgrund ihres Verhaltens nicht gruppenfähig sind.

(4) Geeignete Strafgefangene sollen in Wohngruppen untergebracht werden.

### § 24  
Verlegung und Überstellung

(1) Die Gefangenen können abweichend vom Vollstreckungsplan in eine andere Anstalt verlegt werden, wenn Gründe der Vollzugsorganisation oder andere wichtige Gründe dies erfordern.
Sie dürfen aus wichtigem Grund in eine andere Anstalt überstellt werden.

(2) Darüber hinaus können die Straf- und Jugendstrafgefangenen abweichend vom Vollstreckungsplan in eine andere Anstalt verlegt werden, wenn die Erreichung des Vollzugsziels hierdurch gefördert wird.

(3) Die Untersuchungsgefangenen können zur Umsetzung einer Anordnung nach § 119 Absatz 1 der Strafprozessordnung verlegt oder überstellt werden.

(4) Vor einer Verlegung oder Überstellung von Untersuchungsgefangenen ist dem Gericht und der Staatsanwaltschaft Gelegenheit zur Stellungnahme zu geben.
§ 12 Absatz 5 gilt entsprechend.

(5) Bei jungen Gefangenen werden die Personensorgeberechtigten und das Jugendamt, bei Jugendstrafgefangenen auch die Vollstreckungsleiterin oder der Vollstreckungsleiter von der Verlegung unverzüglich unterrichtet.

### Abschnitt 4  
Sozial- und Psychotherapie

### § 25   
Sozialtherapie

(1) Sozialtherapie dient der Verringerung einer erheblichen Gefährlichkeit der Straf- und Jugendstrafgefangenen.
Auf der Grundlage einer therapeutischen Gemeinschaft bedient sie sich psychotherapeutischer, sozialpädagogischer und arbeitstherapeutischer Methoden, die in umfassenden Behandlungsprogrammen verbunden werden.
Personen aus dem Lebensumfeld der Straf- und Jugendstrafgefangenen außerhalb des Vollzugs werden in die Behandlung einbezogen.

(2) Straf- und Jugendstrafgefangene sind in einer sozialtherapeutischen Abteilung unterzubringen, wenn ihre Teilnahme an den dortigen Behandlungsprogrammen zur Verringerung ihrer erheblichen Gefährlichkeit angezeigt ist.
Eine erhebliche Gefährlichkeit liegt vor, wenn schwerwiegende Straftaten gegen Leib oder Leben, die persönliche Freiheit oder die sexuelle Selbstbestimmung zu erwarten sind.

(3) Im Übrigen können Straf- und Jugendstrafgefangene in einer sozialtherapeutischen Abteilung untergebracht werden, wenn die Teilnahme an den dortigen Behandlungsprogrammen zur Erreichung des Vollzugsziels angezeigt ist.

(4) Die Unterbringung soll zu einem Zeitpunkt erfolgen, der entweder den Abschluss der Behandlung zum voraussichtlichen Entlassungszeitpunkt erwarten lässt oder die Fortsetzung der Behandlung nach der Entlassung ermöglicht.
Hierzu arbeitet die sozialtherapeutische Abteilung eng mit forensischen Ambulanzen oder anderen ambulanten Nachsorgeeinrichtungen zusammen.
Ist Sicherungsverwahrung angeordnet oder vorbehalten, soll die Unterbringung zu einem Zeitpunkt erfolgen, der den Abschluss der Behandlung noch während des Vollzugs der Freiheits- oder Jugendstrafe erwarten lässt.

(5) Die Unterbringung wird beendet, wenn das Ziel der Behandlung aus Gründen, die in der Person des Straf- oder Jugendstrafgefangenen liegen, nicht erreicht werden kann.

### § 26  
Psychotherapie

Psychotherapie im Vollzug dient insbesondere der Behandlung psychischer Störungen des Verhaltens und Erlebens, die in einem Zusammenhang mit der Straffälligkeit stehen.
Sie wird durch systematische Anwendung wissenschaftlich fundierter psychologischer Methoden der Gesprächsführung mit einer oder mehreren Personen durchgeführt.

### Abschnitt 5  
Arbeitstherapeutische Maßnahmen, Arbeitstraining, schulische und  
berufliche Qualifizierungsmaßnahmen, Arbeit

### § 27  
Arbeitstherapeutische Maßnahmen

Arbeitstherapeutische Maßnahmen dienen dazu, dass die Gefangenen Eigenschaften wie Selbstvertrauen, Durchhaltevermögen und Konzentrationsfähigkeit einüben, um sie stufenweise an die Grundanforderungen des Arbeitslebens heranzuführen.

### § 28  
Arbeitstraining

Arbeitstraining dient dazu, Gefangenen, die nicht in der Lage sind, einer regelmäßigen und erwerbsorientierten Beschäftigung nachzugehen, Fähigkeiten und Fertigkeiten zu vermitteln, die eine Eingliederung in das leistungsorientierte Arbeitsleben fördern.
Die in der Anstalt dafür vorzuhaltenden Maßnahmen müssen den Anforderungen des Arbeitsmarktes Rechnung tragen.

### § 29  
Schulische und berufliche Qualifizierungsmaßnahmen

(1) Schulische und berufliche Aus- und Weiterbildung und vorberufliche Qualifizierung (schulische und berufliche Qualifizierungsmaßnahmen) im Vollzug haben das Ziel, die Fähigkeiten der Gefangenen zur Eingliederung und zur Aufnahme einer Erwerbstätigkeit nach der Haftentlassung zu vermitteln, zu verbessern oder zu erhalten.
Bei der Festlegung von Inhalten, Methoden und Organisationsformen der Bildungsangebote werden die Besonderheiten der jeweiligen Zielgruppe berücksichtigt.
Schulische und berufliche Aus- und Weiterbildung werden in der Regel als Vollzeitmaßnahmen durchgeführt.

(2) Schulpflichtige junge Gefangene nehmen in der Anstalt am allgemein- oder berufsbildenden Unterricht nach den für öffentliche Schulen geltenden Bestimmungen teil.

(3) Die Jugendstrafgefangenen sind vorrangig zur Teilnahme an schulischen und beruflichen Orientierungs-, Berufsvorbereitungs-, Aus- und Weiterbildungsmaßnahmen oder speziellen Maßnahmen zur Förderung ihrer schulischen, beruflichen oder persönlichen Entwicklung verpflichtet.
Die minderjährigen Untersuchungsgefangenen können hierzu verpflichtet werden.

(4) Geeigneten Straf- und Jugendstrafgefangenen soll die Teilnahme an einer schulischen oder beruflichen Ausbildung ermöglicht werden, die zu einem anerkannten Abschluss führt.

(5) Geeigneten Untersuchungsgefangenen soll nach Möglichkeit Gelegenheit zum Erwerb oder zur Verbesserung schulischer und beruflicher Kenntnisse, auch zum Erwerb eines anerkannten Abschlusses, gegeben werden, soweit es die besonderen Bedingungen der Untersuchungshaft zulassen.

(6) Berufliche Qualifizierungsmaßnahmen sind danach auszurichten, dass sie den Gefangenen für den Arbeitsmarkt relevante Qualifikationen vermitteln.

(7) Bei der Vollzugsplanung ist darauf zu achten, dass die Straf- und Jugendstrafgefangenen Qualifizierungsmaßnahmen während ihrer Haftzeit abschließen oder sie nach der Inhaftierung fortsetzen können.
Können Maßnahmen während der Haftzeit nicht abgeschlossen werden, trägt die Anstalt in Zusammenarbeit mit außervollzuglichen Einrichtungen dafür Sorge, dass die begonnene Qualifizierungsmaßnahme nach der Haft fortgesetzt werden kann.

(8) Nachweise über schulische und berufliche Qualifizierungsmaßnahmen dürfen keinen Hinweis auf die Inhaftierung enthalten.

### § 30  
Arbeit

(1) Den Gefangenen soll Arbeit angeboten und ihnen auf Antrag oder mit ihrer Zustimmung zugewiesen werden, soweit dadurch nach dem Vollzugs- und Eingliederungsplan vor­rangige Maßnahmen nicht beeinträchtigt werden.

(2) Nehmen die Gefangenen eine Arbeit auf, gelten die von der Anstalt festgelegten Arbeitsbedingungen.
Die Arbeit darf nicht zur Unzeit niedergelegt werden.

### § 31  
Freies Beschäftigungsverhältnis, Selbstbeschäftigung

(1) Straf- und Jugendstrafgefangenen, die zum Freigang (§ 46 Absatz 1 Satz 1 Nummer 4) zugelassen sind, soll gestattet werden, einer Arbeit oder einer schulischen oder beruflichen Qualifizierungsmaßnahme auf der Grundlage eines freien Beschäftigungsverhältnisses oder der Selbstbeschäftigung außerhalb der Anstalt nachzugehen, wenn die Beschäftigungsstelle geeignet ist und nicht überwiegende Gründe des Vollzugs entgegenstehen.
§ 48 gilt entsprechend.

(2) Das Entgelt ist der Anstalt zur Gutschrift für die Straf- und Jugendstrafgefangenen zu überweisen.

### § 32  
Freistellung von der Arbeit

(1) Haben die Gefangenen ein halbes Jahr lang gearbeitet, so können sie beanspruchen, zehn Arbeitstage von der Arbeit freigestellt zu werden.
Zeiten, in denen die Gefangenen infolge Krankheit an der Arbeitsleistung gehindert waren, werden bis zu 15 Arbeitstagen auf das Halbjahr angerechnet.
Der Anspruch verfällt, wenn die Freistellung nicht innerhalb eines Jahres nach seiner Entstehung erfolgt ist.

(2) Auf die Zeit der Freistellung wird Langzeitausgang (§ 46 Absatz 1 Satz 1 Nummer 3) angerechnet, soweit er in die Arbeitszeit fällt.
Gleiches gilt für einen Langzeitausgang nach § 47 Absatz 1, soweit er nicht wegen des Todes oder einer lebensgefährlichen Erkrankung naher Angehöriger erteilt worden ist.

(3) Die Gefangenen erhalten für die Zeit der Freistellung ihr Arbeitsentgelt weiter.

(4) Urlaubsregelungen freier Beschäftigungsverhältnisse bleiben unberührt.

(5) Für Arbeitstraining, schulische und berufliche Qualifizierungsmaßnahmen gelten Absätze 1 bis 4 entsprechend, sofern diese den Umfang der regelmäßigen wöchentlichen Arbeitszeit erreichen.

### Abschnitt 6  
Besuche, Telefongespräche, Schriftwechsel, andere Formen der Telekommunikation und Pakete

### § 33  
Grundsatz

Die Gefangenen haben das Recht, mit Personen außerhalb der Anstalt im Rahmen der Bestimmungen dieses Gesetzes zu verkehren.
Der Verkehr mit der Außenwelt, insbesondere die Erhaltung der Kontakte zu Bezugspersonen und die Schaffung eines sozialen Empfangsraums, ist zu fördern.

### § 34  
Besuch

(1) Die Gefangenen dürfen regelmäßig Besuch empfangen.
Die Gesamtdauer beträgt im Vollzug der Freiheitsstrafe und der Untersuchungshaft mindestens vier, im Vollzug der Jugendstrafe und der Untersuchungshaft an jungen Untersuchungsgefangenen mindestens sechs Stunden im Monat.

(2) Besuche von Angehörigen im Sinne von § 11 Absatz 1 Nummer 1 des Strafgesetzbuches werden besonders unterstützt.

(3) Besuche sollen darüber hinaus zugelassen werden, wenn sie

1.  persönlichen, rechtlichen oder geschäftlichen Angelegenheiten der Gefangenen dienen, die von diesen nicht schriftlich erledigt, durch Dritte wahrgenommen oder bis zur voraussichtlichen Entlassung aufgeschoben werden können,
2.  die Eingliederung der Straf- und Jugendstrafgefangenen fördern oder
3.  die Erziehung der jungen Gefangenen fördern.

(4) Mehrstündige, unbeaufsichtigte Besuche (Langzeitbesuche) sind zuzulassen, wenn dies zur Pflege der familiären, partnerschaftlichen oder ihnen gleichzusetzender Kontakte der Straf- und Jugendstrafgefangenen geboten erscheint und die Straf- und Jugendstrafgefangenen hierfür geeignet sind.
Die Entscheidung trifft die Anstaltsleiterin oder der Anstaltsleiter.

(5) Besuche von Verteidigerinnen oder Verteidigern sowie von Rechtsanwältinnen oder Rechtsanwälten und Notarinnen oder Notaren in einer die Gefangenen betreffenden Rechtssache sind zu gestatten.
Dies gilt auch für Besuche von Beiständen nach § 69 des Jugendgerichtsgesetzes.

### § 35  
Untersagung der Besuche

Die Anstaltsleiterin oder der Anstaltsleiter kann Besuche untersagen, wenn

1.  die Sicherheit oder Ordnung der Anstalt gefährdet würde,
2.  bei Personen, die nicht Angehörige der Strafgefangenen und jungen Gefangenen im Sinne von § 11 Absatz 1 Nummer 1 des Strafgesetzbuches sind, zu befürchten ist, dass sie einen schädlichen Einfluss auf die Strafgefangenen oder jungen Gefangenen haben oder die Erreichung des Vollzugsziels behindern,
3.  bei Personen, die Opfer der Straftat waren oder im Haftbefehl als Opfer benannt werden, zu befürchten ist, dass die Begegnung mit den Gefangenen einen schädlichen Einfluss auf sie hat,
4.  die Personensorgeberechtigten nicht einverstanden sind.

### § 36  
Durchführung der Besuche

(1) Aus Gründen der Sicherheit können Besuche davon abhängig gemacht werden, dass sich die Besucherinnen und Besucher durchsuchen oder mit technischen Hilfsmitteln absuchen lassen.
§ 86 Absatz 1 Satz 2 und 3 gilt entsprechend.
Eine inhaltliche Überprüfung der von Verteidigerinnen und Verteidigern oder von Beiständen nach § 69 des Jugendgerichtsgesetzes mitgeführten Schriftstücke und sonstigen Unterlagen ist nicht zulässig.
§ 42 Absatz 2 Satz 2 und 3 bleibt unberührt.

(2) Besuche werden regelmäßig beaufsichtigt.
Über Ausnahmen entscheidet die Anstaltsleiterin oder der Anstaltsleiter.
Die Beaufsichtigung kann mittels optisch-elektronischer Einrichtungen durchgeführt werden; die betroffenen Personen sind vorher darauf hinzuweisen.
Eine Aufzeichnung findet nicht statt.

(3) Besuche von Verteidigerinnen und Verteidigern und von Beiständen nach § 69 des Jugendgerichtsgesetzes werden nicht beaufsichtigt.

(4) Besuche dürfen abgebrochen werden, wenn Besucherinnen oder Besucher oder Gefangene gegen dieses Gesetz oder aufgrund dieses Gesetzes getroffene Anordnungen trotz Abmahnung verstoßen oder von den Besucherinnen oder Besuchern ein schädlicher Einfluss auf junge Gefangene ausgeht.
Dies gilt auch bei einem Verstoß gegen eine Anordnung nach § 119 Absatz 1 der Strafprozessordnung.
Die Abmahnung unterbleibt, wenn es unerlässlich ist, den Besuch sofort abzubrechen.

(5) Gegenstände dürfen beim Besuch nicht übergeben werden.
Dies gilt nicht für die bei dem Besuch der Verteidigerinnen und Verteidiger oder der Beistände nach § 69 des Jugendgerichtsgesetzes übergebenen Schriftstücke und sonstigen Unterlagen sowie für die bei dem Besuch von Rechtsanwältinnen und Rechtsanwälten oder Notarinnen und Notaren zur Erledigung einer die Gefangenen betreffenden Rechtssache übergebenen Schriftstücke und sonstigen Unterlagen.
Bei dem Besuch von Rechtsanwältinnen und Rechtsanwälten oder Notarinnen und Notaren kann die Übergabe aus Gründen der Sicherheit oder Ordnung der Anstalt von der Erlaubnis der Anstaltsleiterin oder des Anstaltsleiters abhängig gemacht werden.
§ 42 Absatz 2 Satz 2 und 3 bleibt unberührt.

(6) Die Anstaltsleiterin oder der Anstaltsleiter kann im Einzelfall die Nutzung einer Trennvorrichtung anordnen, wenn dies zum Schutz von Personen oder zur Verhinderung einer Übergabe von Gegenständen erforderlich ist.

### § 37  
Überwachung der Gespräche

(1) Gespräche dürfen überwacht werden, soweit es im Einzelfall

1.  aus Gründen der Sicherheit,
2.  bei den Straf- und Jugendstrafgefangenen wegen einer Gefährdung der Erreichung des Vollzugsziels oder
3.  bei jungen Gefangenen aus Gründen der Erziehung

erforderlich ist.

(2) Gespräche mit Verteidigerinnen und Verteidigern oder mit Beiständen nach § 69 des Jugendgerichtsgesetzes werden nicht überwacht.

### § 38  
Telefongespräche

(1) Den Gefangenen kann gestattet werden, Telefongespräche zu führen.
Die Bestimmungen über den Besuch gelten entsprechend.
Eine beabsichtigte Überwachung teilt die Anstalt den Gefangenen rechtzeitig vor Beginn des Telefongesprächs und den Gesprächspartnerinnen oder Gesprächspartnern der Gefangenen unmittelbar nach Herstellung der Verbindung mit.

(2) Die Kosten der Telefongespräche tragen die Gefangenen.
Sind sie dazu nicht in der Lage, kann die Anstalt die Kosten in begründeten Fällen in angemessenem Umfang übernehmen.

### § 39  
Schriftwechsel

(1) Die Gefangenen haben das Recht, Schreiben abzusenden und zu empfangen.

(2) Die Kosten des Schriftwechsels tragen die Gefangenen.
Sind sie dazu nicht in der Lage, kann die Anstalt die Kosten in begründeten Fällen in angemessenem Umfang übernehmen.

### § 40  
Untersagung des Schriftwechsels

Die Anstaltsleiterin oder der Anstaltsleiter kann den Schriftwechsel mit bestimmten Personen untersagen, wenn

1.  die Sicherheit oder Ordnung der Anstalt gefährdet würde,
2.  bei Personen, die nicht Angehörige der Strafgefangenen und jungen Gefangenen im Sinne von § 11 Absatz 1 Nummer 1 des Strafgesetzbuches sind, zu befürchten ist, dass der Schriftwechsel einen schädlichen Einfluss auf die Strafgefangenen oder jungen Gefangenen haben oder die Erreichung des Vollzugsziels behindern würde, oder
3.  bei Personen, die Opfer der Straftat waren oder im Haftbefehl als Opfer benannt werden, zu befürchten ist, dass der Schriftwechsel mit den Gefangenen einen schädlichen Einfluss auf sie hätte,
4.  die Personensorgeberechtigten nicht einverstanden sind.

### § 41  
Sichtkontrolle, Weiterleitung und Aufbewahrung von Schreiben

(1) Die Gefangenen haben das Absenden und den Empfang ihrer Schreiben durch die Anstalt vermitteln zu lassen, soweit nichts anderes gestattet ist.

(2) Ein- und ausgehende Schreiben werden in Anwesenheit der Gefangenen auf verbotene Gegenstände kontrolliert und sind unverzüglich weiterzuleiten.

(3) Die Gefangenen haben eingehende Schreiben unverschlossen zu verwahren, sofern nichts anderes gestattet wird.
Sie können sie verschlossen zu ihrer Habe geben.

### § 42  
Überwachung des Schriftwechsels

(1) Der Schriftwechsel darf überwacht werden, soweit es im Einzelfall

1.  aus Gründen der Sicherheit,
2.  bei den Straf- und Jugendstrafgefangenen wegen einer Gefährdung der Erreichung des Vollzugsziels oder
3.  bei jungen Gefangenen aus Gründen der Erziehung

erforderlich ist.

(2) Der Schriftwechsel der Gefangenen mit ihren Verteidigerinnen und Verteidigern oder Beiständen nach § 69 des Jugendgerichtsgesetzes wird nicht überwacht.
Liegt dem Vollzug der Freiheits- oder Jugendstrafe eine Straftat nach § 129a, auch in Verbindung mit § 129b Absatz 1 des Strafgesetzbuches zugrunde, gelten § 148 Absatz 2 und § 148a der Strafprozessordnung entsprechend; dies gilt nicht, wenn die Straf- oder Jugendstrafgefangenen sich im offenen Vollzug befinden oder wenn ihnen Lockerungen nach § 46 gewährt worden sind und ein Grund, der die Anstaltsleiterin oder den Anstaltsleiter zum Widerruf von Lockerungen ermächtigt, nicht vorliegt.
Satz 2 gilt auch, wenn eine Freiheits- oder Jugendstrafe wegen einer Straftat nach § 129a, auch in Verbindung mit § 129b Absatz 1 des Strafgesetzbuches erst im Anschluss an den Vollzug der Freiheits- oder Jugendstrafe, der eine andere Verurteilung zugrunde liegt, zu vollstrecken ist.

(3) Nicht überwacht werden Schreiben der Gefangenen an Volksvertretungen des Bundes und der Länder sowie an deren Mitglieder, soweit die Schreiben an die Anschriften dieser Volksvertretungen gerichtet sind und die Absenderin oder den Absender zutreffend angeben.
Entsprechendes gilt für Schreiben an das Europäische Parlament und dessen Mitglieder, den Europäischen Gerichtshof für Menschenrechte, den Europäischen Ausschuss zur Verhütung von Folter und unmenschlicher oder erniedrigender Behandlung oder Strafe, den Ausschuss der Vereinten Nationen gegen Folter, den zugehörigen Unterausschuss zur Verhütung von Folter und die entsprechenden Nationalen Präventionsmechanismen, die konsularische Vertretung ihres Heimatlandes und weitere Einrichtungen, mit denen der Schriftverkehr aufgrund völkerrechtlicher Verpflichtungen der Bundesrepublik Deutschland geschützt ist.
Satz 1 gilt auch für den Schriftverkehr mit Gerichten, Staatsanwaltschaften und der Aufsichtsbehörde sowie den Bürgerbeauftragten der Länder und den Datenschutzbeauftragten des Bundes und der Länder.
Schreiben der in den Sätzen 1 bis 3 genannten Stellen, die an die Gefangenen gerichtet sind, werden nicht überwacht, sofern die Identität der Absenderin oder des Absenders zweifelsfrei feststeht.

### § 43  
Anhalten von Schreiben

(1) Die Anstaltsleiterin oder der Anstaltsleiter kann Schreiben anhalten, wenn

1.  die Sicherheit oder Ordnung der Anstalt gefährdet würde,
2.  die Weitergabe in Kenntnis ihres Inhalts einen Straf- oder Bußgeldtatbestand verwirklichen würde,
3.  sie grob unrichtige oder erheblich entstellende Darstellungen von Anstaltsverhältnissen oder grobe Beleidigungen enthalten,
4.  sie in Geheim- oder Kurzschrift, unlesbar, unverständlich oder ohne zwingenden Grund in einer fremden Sprache abgefasst sind,
5.  bei Straf- oder Jugendstrafgefangenen die Erreichung des Vollzugsziels gefährdet würde,
6.  es die Aufgabe des Vollzugs der Untersuchungshaft erfordert oder
7.  sie die Eingliederung anderer Straf- und Jugendstrafgefangener gefährden können.

(2) Ausgehenden Schreiben, die unrichtige Darstellungen enthalten, kann ein Begleitschreiben beigefügt werden, wenn die Gefangenen auf dem Absenden bestehen.

(3) Sind Schreiben angehalten worden, wird das den Gefangenen mitgeteilt.
Hiervon kann im Vollzug der Untersuchungshaft abgesehen werden, wenn und solange es dessen Aufgabe erfordert.
Soweit angehaltene Schreiben nicht beschlagnahmt werden, werden sie an die Absenderin oder den Absender zurückgegeben oder, sofern dies unmöglich oder aus besonderen Gründen nicht angezeigt ist, verwahrt.

(4) Schreiben, deren Überwachung ausgeschlossen ist, dürfen nicht angehalten werden.

### § 44  
Andere Formen der Telekommunikation

Nach Zulassung anderer Formen der Telekommunikation im Sinne des Telekommunikationsgesetzes durch die Aufsichtsbehörde (§ 115 Absatz 1) kann die Anstaltsleiterin oder der Anstaltsleiter den Gefangenen gestatten, diese Formen auf ihre Kosten zu nutzen.
Die Bestimmungen dieses Abschnitts gelten entsprechend.

### § 45  
Pakete

(1) Der Empfang von Paketen bedarf der Erlaubnis der Anstalt, welche Zeitpunkt und Höchstmengen für die Sendungen und für einzelne Gegenstände festsetzen kann.
Für den Ausschluss von Gegenständen gilt § 55 Satz 2 entsprechend.
Die Anstalt kann darüber hinaus Gegenstände und Verpackungsformen ausschließen, die einen unverhältnismäßigen Kontrollaufwand bedingen.

(2) Die Anstalt kann die Annahme von Paketen, deren Einbringung nicht gestattet ist oder die die Voraussetzungen des Absatzes 1 nicht erfüllen, ablehnen oder solche Pakete an die Absenderin oder den Absender zurücksenden.

(3) Pakete sind in Gegenwart der Gefangenen zu öffnen, an die sie adressiert sind.
Mit nicht zugelassenen oder ausgeschlossenen Gegenständen ist gemäß § 58 Absatz 3 zu verfahren.
Sie können auch auf Kosten der Gefangenen zurückgesandt werden.

(4) Der Empfang von Paketen kann vorübergehend versagt werden, wenn dies wegen der Gefährdung der Sicherheit oder Ordnung der Anstalt unerlässlich ist.

(5) Den Gefangenen kann gestattet werden, Pakete zu versenden.
Der Inhalt kann aus Gründen der Sicherheit oder Ordnung der Anstalt überprüft werden.

(6) Die Kosten des Paketversandes tragen die Gefangenen.
Sind sie dazu nicht in der Lage, kann die Anstalt die Kosten in begründeten Fällen in angemessenem Umfang übernehmen.

### Abschnitt 7  
Lockerungen und sonstige Aufenthalte außerhalb der Anstalt

### § 46   
Lockerungen zur Erreichung des Vollzugsziels

(1) Aufenthalte außerhalb der Anstalt ohne Aufsicht (Lockerungen) können den Straf- und Jugendstrafgefangenen zur Erreichung des Vollzugsziels gewährt werden, namentlich

1.  das Verlassen der Anstalt für bis zu 24 Stunden in Begleitung einer von der Anstalt zugelassenen Person (Begleitausgang),
2.  das Verlassen der Anstalt für bis zu 24 Stunden ohne Begleitung (unbegleiteter Ausgang),
3.  das Verlassen der Anstalt für mehrere Tage (Langzeitausgang),
4.  die regelmäßige Beschäftigung außerhalb der Anstalt (Freigang) und
5.  im Vollzug der Jugendstrafe die Unterbringung in besonderen Erziehungseinrichtungen.

Vor Gewährung von Lockerungen nach Satz 1 Nummer 5 wird die Vollstreckungsleiterin oder der Vollstreckungsleiter gehört.

(2) Die Lockerungen dürfen gewährt werden, wenn verantwortet werden kann zu erproben, dass die Straf- und Jugendstrafgefangenen sich dem Vollzug der Freiheitsstrafe nicht entziehen oder die Lockerungen nicht zu Straftaten missbrauchen werden.
Jugendstrafgefangenen können sie versagt werden, wenn sie ihren Mitwirkungspflichten nicht nachkommen.

(3) Durch Lockerungen wird die Vollstreckung der Freiheits- oder Jugendstrafe nicht unterbrochen.

### § 47   
Lockerungen aus sonstigen Gründen

(1) Lockerungen können auch aus wichtigem Anlass gewährt werden.
Wichtige Anlässe sind insbesondere die Teilnahme an gerichtlichen Terminen, die medizinische Behandlung der Straf- und Jugendstrafgefangenen sowie der Tod oder eine lebensgefährliche Erkrankung naher Angehöriger.

(2) Erkranken Straf- oder Jugendstrafgefangene so schwer, dass aufgrund ihrer Erkrankung in Kürze mit dem Tod gerechnet werden muss, so können sie bis zu einer Entscheidung der Vollstreckungsbehörde über einen Strafausstand nach § 455 Absatz 4 der Strafprozessordnung Langzeitausgang erhalten.

(3) § 46 Absatz 2 und 3 gilt entsprechend.

### § 48   
Weisungen für Lockerungen, Zustimmung der Aufsichtsbehörde

(1) Für Lockerungen sind die nach den Umständen des Einzelfalles erforderlichen Weisungen zu erteilen.
Bei der Ausgestaltung der Lockerungen ist nach Möglichkeit auch den Belangen der Opfer Rechnung zu tragen.

(2) Zu lebenslanger Freiheitsstrafe verurteilten Strafgefangenen dürfen Lockerungen nur mit Zustimmung der Aufsichtsbehörde gewährt werden.

### § 49   
Ausführung, Außenbeschäftigung, Vorführung, Ausantwortung

(1) Den Gefangenen kann das Verlassen der Anstalt unter ständiger und unmittelbarer Aufsicht gestattet werden, wenn dies aus besonderen Gründen notwendig ist (Ausführung).
Die Gefangenen können auch gegen ihren Willen ausgeführt werden.
Liegt die Ausführung ausschließlich im Interesse der Gefangenen, können ihnen die Kosten auferlegt werden.
Hiervon ist bei Straf- und Jugendstrafgefangenen abzusehen, soweit dies die Erreichung des Vollzugsziels, insbesondere die Eingliederung, behindert.

(2) Ausführungen zur Befolgung einer gerichtlichen Ladung sind zu ermöglichen, soweit darin das persönliche Erscheinen angeordnet ist.

(3) Vor der Gewährung einer Ausführung Untersuchungsgefangener ist dem Gericht und der Staatsanwaltschaft Gelegenheit zur Stellungnahme zu geben.

(4) Straf- und Jugendstrafgefangenen kann gestattet werden, außerhalb der Anstalt einer regelmäßigen Beschäftigung unter ständiger Aufsicht oder unter Aufsicht in unregelmäßigen Abständen (Außenbeschäftigung) nachzugehen.
§ 46 Absatz 2 gilt entsprechend.

(5) Auf Ersuchen eines Gerichts werden Gefangene vorgeführt, sofern ein Vorführungsbefehl vorliegt.
Über Untersuchungsgefangene betreffende Vorführungsersuchen in anderen als dem der Inhaftierung zugrunde liegenden Verfahren sind das Gericht und die Staatsanwaltschaft unverzüglich zu unterrichten.

(6) Gefangene dürfen befristet dem Gewahrsam eines Gerichts, einer Staatsanwaltschaft oder einer Polizei-, Zoll- oder Finanzbehörde auf Antrag überlassen werden (Ausantwortung).
Absatz 3 gilt entsprechend.

### Abschnitt 8  
Vorbereitung der Eingliederung, Entlassung und nachgehende Betreuung

### § 50   
Vorbereitung der Eingliederung

(1) Die Maßnahmen zur sozialen und beruflichen Eingliederung sind auf den Zeitpunkt der voraussichtlichen Entlassung in die Freiheit abzustellen.
Die Straf- und Jugendstrafgefangenen sind bei der Ordnung ihrer persönlichen, wirtschaftlichen und sozialen Angelegenheiten zu unterstützen.
Dies umfasst auch die Vermittlung in nachsorgende Maßnahmen.

(2) Die Anstalt arbeitet frühzeitig mit den Kommunen, den Agenturen für Arbeit, den Trägern der Sozialversicherung und der Sozialhilfe, den Hilfeeinrichtungen anderer Behörden, den forensischen Ambulanzen, den Verbänden der freien Wohlfahrtspflege und weiteren Personen und Einrichtungen außerhalb des Vollzugs zusammen, insbesondere um zu erreichen, dass die Straf- und Jugendstrafgefangenen nach ihrer Entlassung über eine geeignete Unterbringung und eine Arbeits- oder Ausbildungsstelle verfügen.
Bewährungshilfe und Führungsaufsichtsstellen beteiligen sich frühzeitig an der sozialen und beruflichen Eingliederung der Straf- und Jugendstrafgefangenen.
§ 12 Absatz 6 gilt entsprechend.

(3) Um die Eingliederung vorzubereiten, sollen die Straf- und Jugendstrafgefangenen in eine Eingliederungsabteilung verlegt werden.

(4) Den Straf- und Jugendstrafgefangenen können Aufenthalte in Einrichtungen außerhalb des Vollzugs (Übergangseinrichtungen) gewährt werden, wenn dies zur Vorbereitung der Eingliederung erforderlich ist.
Die Vollstreckungsleiterin oder der Vollstreckungsleiter ist zu hören.
Haben sich die Straf- und Jugendstrafgefangenen mindestens sechs Monate im Vollzug befunden, kann ihnen auch ein zusammenhängender Langzeitausgang bis zu sechs Monaten, zur Unterbringung in einer Einrichtung freier Träger auch darüber hinaus, gewährt werden, wenn dies zur Vorbereitung der Eingliederung erforderlich ist.
§ 46 Absatz 2 und 3 sowie § 48 gelten entsprechend.

(5) In einem Zeitraum von sechs Monaten vor der voraussichtlichen Entlassung sind den Straf- und Jugendstrafgefangenen die zur Vorbereitung der Eingliederung erforderlichen Lockerungen zu gewähren, sofern nicht mit hoher Wahrscheinlichkeit zu erwarten ist, dass die Straf- und Jugendstrafgefangenen sich dem Vollzug der Freiheits- oder Jugendstrafe entziehen oder die Lockerungen zu Straftaten missbrauchen werden.

(6) Die Beratung der Straf- und Jugendstrafgefangenen, die voraussichtlich nicht unter Bewährung oder Führungsaufsicht gestellt werden und bei denen nach ihrer Entlassung ein Hilfebedarf besteht, soll die Vermittlung des Kontaktes zu Stellen und Einrichtungen außerhalb der Anstalt umfassen, die die Eingliederung der Straf- und Jugendstrafgefangenen am Wohnort begleiten und fördern können.
Insbesondere sind die regionalen freien Träger frühzeitig in die Vorbereitung der Eingliederung einzubinden, um die Straf- und Jugendstrafgefangenen nach der Entlassung an ihrem Wohnort unverzüglich unterstützen zu können.

### § 51  
Entlassung der Straf- und Jugendstrafgefangenen

(1) Die Straf- und Jugendstrafgefangenen sollen am letzten Tag ihrer Strafzeit möglichst frühzeitig, jedenfalls noch am Vormittag, entlassen werden.

(2) Fällt das Strafende auf einen Sonnabend oder Sonntag, einen gesetzlichen Feiertag, den ersten Werktag nach Ostern oder Pfingsten oder in die Zeit vom 22.
Dezember bis zum 6.
Januar, so können die Straf- und Jugendstrafgefangenen an dem diesem Tag oder Zeitraum vorhergehenden Werktag entlassen werden, wenn dies gemessen an der Dauer der Strafzeit vertretbar ist und fürsorgerische Gründe nicht entgegenstehen.

(3) Der Entlassungszeitpunkt kann bis zu zwei Tage vorverlegt werden, wenn die Straf- und Jugendstrafgefangenen zu ihrer Eingliederung hierauf dringend angewiesen sind.

(4) Bedürftigen Straf- und Jugendstrafgefangenen kann eine Entlassungsbeihilfe in Form eines Reisekostenzuschusses, angemessener Kleidung oder einer sonstigen notwendigen Unterstützung gewährt werden.

### § 52  
Nachgehende Betreuung

Mit Zustimmung der Anstaltsleiterin oder des Anstaltsleiters können Bedienstete an der nachgehenden Betreuung entlassener Straf- und Jugendstrafgefangener mit deren Einverständnis mitwirken, wenn ansonsten die Eingliederung gefährdet wäre.
Die nachgehende Betreuung kann auch außerhalb der Anstalt erfolgen.
In der Regel ist sie auf die ersten sechs Monate nach der Entlassung begrenzt.

### § 53  
Verbleib oder Aufnahme auf freiwilliger Grundlage

(1) Sofern es die Belegungssituation zulässt, können die Straf- und Jugendstrafgefangenen auf Antrag ausnahmsweise vorübergehend in der Anstalt verbleiben oder wieder aufgenommen werden, wenn die Eingliederung gefährdet und ein Aufenthalt in der Anstalt aus diesem Grunde gerechtfertigt ist.

(2) Die Jugendstrafgefangenen können ausnahmsweise nach ihrer Entlassung im Vollzug begonnene Ausbildungs- oder Behandlungsmaßnahmen fortführen, soweit diese nicht anderweitig durchgeführt werden können.
Hierzu können sie vorübergehend in der Anstalt untergebracht werden.

(3) Die Unterbringung erfolgt auf vertraglicher Basis.
Gegen die in der Anstalt untergebrachten Entlassenen dürfen Maßnahmen des Vollzugs nicht mit unmittelbarem Zwang durchgesetzt werden.

(4) Bei Störung des Anstaltsbetriebes durch die Entlassenen oder aus vollzugsorganisatorischen Gründen können die Unterbringung und die Maßnahme jederzeit beendet werden.

### § 54  
Entlassung der Untersuchungsgefangenen

(1) Auf Anordnung des Gerichts oder der Staatsanwaltschaft entlässt die Anstalt die Untersuchungsgefangenen unverzüglich aus der Haft, es sei denn, es ist in anderer Sache eine richterlich angeordnete Freiheitsentziehung zu vollziehen.

(2) Aus fürsorgerischen Gründen kann den Untersuchungsgefangenen der freiwillige Verbleib in der Anstalt bis zum Vormittag des zweiten auf den Eingang der Entlassungsanordnung folgenden Werktages gestattet werden.
Der freiwillige Verbleib setzt das schriftliche Einverständnis der Untersuchungsgefangenen voraus, dass die bisher bestehenden Beschränkungen aufrechterhalten bleiben.

(3) § 51 Absatz 4 gilt entsprechend.

### Abschnitt 9  
Grundversorgung und Freizeit

### § 55   
Einbringen von Gegenständen

Gegenstände dürfen durch oder für die Gefangenen nur mit Zustimmung der Anstalt eingebracht werden.
Die Anstalt kann die Zustimmung verweigern, wenn die Gegenstände geeignet sind, die Sicherheit oder Ordnung der Anstalt oder die Erreichung des Vollzugsziels zu gefährden, oder ihre Aufbewahrung nach Art oder Umfang offensichtlich nicht möglich ist.

### § 56  
Gewahrsam an Gegenständen

(1) Die Gefangenen dürfen Gegenstände nur mit Zustimmung der Anstalt in Gewahrsam haben, annehmen oder abgeben.

(2) Ohne Zustimmung dürfen sie Gegenstände von geringem Wert an andere Gefangene weitergeben und von anderen Gefangenen annehmen; die Abgabe und Annahme auch dieser Gegenstände und der Gewahrsam daran können von der Zustimmung der Anstalt abhängig gemacht werden.

### § 57  
Ausstattung des Haftraums

Die Gefangenen dürfen ihren Haftraum in angemessenem Umfang mit eigenen Gegenständen ausstatten oder diese dort aufbewahren.
Gegenstände dürfen nicht in den Haftraum eingebracht werden oder werden aus dem Haftraum entfernt, wenn sie geeignet sind,

1.  die Sicherheit oder Ordnung der Anstalt, insbesondere die Übersichtlichkeit des Haftraums, zu gefährden oder
2.  bei den Straf- und Jugendstrafgefangenen die Erreichung des Vollzugsziels zu gefährden.

### § 58  
Aufbewahrung und Vernichtung von Gegenständen

(1) Gegenstände, die die Gefangenen nicht im Haftraum aufbewahren dürfen oder wollen, werden von der Anstalt aufbewahrt, soweit dies nach Art und Umfang möglich ist.

(2) Den Gefangenen wird Gelegenheit gegeben, ihre Gegenstände, die sie während des Vollzugs und für ihre Entlassung nicht benötigen, zu versenden.
§ 45 Absatz 6 gilt entsprechend.

(3) Werden Gegenstände, deren Aufbewahrung nach Art oder Umfang nicht möglich ist, von den Gefangenen trotz Aufforderung nicht aus der Anstalt verbracht, so darf die Anstalt diese Gegenstände auf Kosten der Gefangenen außerhalb der Anstalt verwahren, verwerten oder vernichten.
Für die Voraussetzungen und das Verfahren der Verwertung und Vernichtung gilt § 27 des Brandenburgischen Polizeigesetzes entsprechend.

(4) Aufzeichnungen und andere Gegenstände, die Kenntnisse über Sicherungsvorkehrungen der Anstalt vermitteln oder Schlussfolgerungen auf diese zulassen, dürfen vernichtet oder unbrauchbar gemacht werden.

### § 59  
Religiöse Schriften und Gegenstände

Die Gefangenen dürfen grundlegende religiöse Schriften sowie in angemessenem Umfang Gegenstände des religiösen Gebrauchs besitzen.
Diese dürfen ihnen nur bei grobem Missbrauch entzogen werden.

### § 60   
Zeitungen und Zeitschriften

(1) Die Gefangenen dürfen auf eigene Kosten Zeitungen und Zeitschriften in angemessenem Umfang durch Vermittlung der Anstalt beziehen.
Ausgeschlossen sind lediglich Zeitungen und Zeitschriften, deren Verbreitung mit Strafe oder Geldbuße bedroht ist.

(2) Den Straf- und Jugendstrafgefangenen können einzelne Ausgaben vorenthalten oder entzogen werden, wenn deren Inhalte die Erreichung des Vollzugsziels oder die Sicherheit oder Ordnung der Anstalt erheblich gefährden würden.

(3) Den Untersuchungsgefangenen können Zeitungen oder Zeitschriften vorenthalten werden, wenn dies zur Umsetzung einer Anordnung nach § 119 Absatz 1 der Strafprozessordnung erforderlich ist.
Für einzelne Ausgaben gilt dies auch dann, wenn deren Inhalte die Sicherheit oder Ordnung der Anstalt erheblich gefährden würden.

### § 61   
Rundfunk, Informations- und Unterhaltungselektronik

(1) Der Zugang zum Rundfunk ist zu ermöglichen.

(2) Eigene Hörfunk- und Fernsehgeräte werden zugelassen, wenn nicht Gründe des § 57 Satz 2 oder bei jungen Gefangenen erzieherische Gründe entgegenstehen.
Andere Geräte der Informations- und Unterhaltungselektronik können unter diesen Voraussetzungen zugelassen werden.
Die Gefangenen können auf Mietgeräte oder auf ein Haftraummediensystem verwiesen werden.
§ 44 bleibt unberührt.

(3) Der Rundfunkempfang kann vorübergehend ausgesetzt oder einzelnen Gefangenen untersagt werden, wenn dies zur Aufrechterhaltung der Sicherheit oder Ordnung der Anstalt, bei einzelnen Untersuchungsgefangenen auch zur Umsetzung einer Anordnung nach § 119 Absatz 1 der Strafprozessordnung unerlässlich ist.

### § 62  
Kleidung

(1) Im geschlossenen Vollzug tragen die Straf- und Jugendstrafgefangenen Anstaltskleidung.
Die Anstaltsleiterin oder der Anstaltsleiter kann eine abweichende Regelung treffen.

(2) Die Untersuchungsgefangenen dürfen eigene Kleidung tragen.
Dieses Recht kann eingeschränkt oder ausgeschlossen werden, soweit es zur Umsetzung einer Anordnung nach § 119 Absatz 1 der Strafprozessordnung oder zur Gewährleistung der Sicherheit oder Ordnung der Anstalt erforderlich ist.

(3) Für Reinigung und Instandsetzung eigener Kleidung haben die Gefangenen auf ihre Kosten zu sorgen.
Die Anstaltsleiterin oder der Anstaltsleiter kann anordnen, dass Reinigung und Instandhaltung nur durch Vermittlung der Anstalt erfolgen dürfen.

### § 63  
Verpflegung und Einkauf

(1) Zusammensetzung und Nährwert der Anstaltsverpflegung entsprechen den Anforderungen an eine gesunde Ernährung und werden ärztlich überwacht.
Auf ärztliche Anordnung wird besondere Verpflegung gewährt.
Den Gefangenen ist zu ermöglichen, Speisevorschriften ihrer Religionsgemeinschaft zu befolgen oder sich fleischlos zu ernähren.

(2) Den Gefangenen wird ermöglicht einzukaufen.
Die Anstalt wirkt auf ein Angebot hin, das auf Wünsche und Bedürfnisse der Gefangenen Rücksicht nimmt.
Das Verfahren des Einkaufs regelt die Anstaltsleiterin oder der Anstaltsleiter.
Straf- und Jugendstrafgefangene können Nahrungs-, Genuss- und Körperpflegemittel nur vom Haus- und Taschengeld, andere Gegenstände in angemessenem Umfang auch vom Eigengeld einkaufen.

### § 64  
Annehmlichkeiten im Vollzug der Untersuchungshaft

Die Untersuchungsgefangenen dürfen sich auf ihre Kosten von den §§ 57 sowie 59 bis 63 nicht umfasste Annehmlichkeiten verschaffen, soweit und solange die Sicherheit oder Ordnung der Anstalt nicht gefährdet wird.

### § 65   
Freizeit

(1) Zur Ausgestaltung der Freizeit hat die Anstalt insbesondere Angebote zur sportlichen und kulturellen Betätigung und Bildungsangebote vorzuhalten.
Die Anstalt stellt eine angemessen ausgestattete Mediathek zur Verfügung.

(2) Dem Sport kommt bei der Gestaltung des Vollzugs der Jugendstrafe und der Untersuchungshaft an jungen Untersuchungsgefangenen besondere Bedeutung zu.
Für die jungen Gefangenen sind ausreichende und geeignete Angebote vorzuhalten, um ihnen eine sportliche Betätigung von mindestens vier Stunden wöchentlich zu ermöglichen.

(3) Im Vollzug der Jugendstrafe dient der Sport auch der Erreichung des Vollzugsziels und kann zur Diagnostik und gezielten Behandlung eingesetzt werden.

### Abschnitt 10  
Vergütung, Gelder der Gefangenen und Kosten

### § 66   
Vergütung

(1) Die Gefangenen erhalten eine Vergütung in Form von

1.  Arbeitsentgelt für Arbeit,
2.  Ausbildungsbeihilfe für die Teilnahme an schulischen und beruflichen Qualifizierungsmaßnahmen,arbeitstherapeutischen Maßnahmen und Arbeitstraining oder
3.  finanzieller Anerkennung für die Teilnahme an Maßnahmen nach § 15 Absatz 1 Satz 1 Nummer 8 bis 11 und Satz 2, soweit sie für die Strafgefangenen nach § 15 Absatz 2 als zwingend erforderlich, für die Jugendstrafgefangenen nach § 15 Absatz 3 als erforderlich erachtet wurden oder Teil des Behandlungsprogramms der sozialtherapeutischen Abteilung sind.

(2) Der Bemessung der Vergütung sind 9 Prozent der Bezugsgröße nach § 18 des Vierten Buches Sozialgesetzbuch zugrunde zu legen (Eckvergütung).
Ein Tagessatz ist der 250.
Teil der Eckvergütung; die Vergütung kann nach einem Stundensatz bemessen werden.

(3) Die Vergütung kann je nach Art der Maßnahme und Leistung der Gefangenen gestuft werden.
Sie beträgt mindestens 75 Prozent der Eckvergütung.
Das für den Justizvollzug zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung Vergütungsstufen zu regeln.

(4) Soweit Beiträge zur Bundesagentur für Arbeit zu entrichten sind, kann vom Arbeitsentgelt oder der Ausbildungsbeihilfe ein Betrag einbehalten werden, der dem Anteil der Gefangenen am Beitrag entsprechen würde, wenn sie diese Bezüge als Arbeitnehmerin oder Arbeitnehmer erhielten.

(5) Die Höhe der Vergütung ist den Gefangenen schriftlich bekannt zu geben.

(6) Die Gefangenen, die an einer Maßnahme nach § 29 teilnehmen, erhalten hierfür nur eine Ausbildungsbeihilfe, soweit kein Anspruch auf Leistungen zum Lebensunterhalt besteht, die außerhalb des Vollzugs aus solchem Anlass gewährt werden.

### § 67  
Eigengeld

(1)Das Eigengeld besteht aus den Beträgen, die die Gefangenen bei der Aufnahme in die Anstalt mitbringen und die sie während der Haftzeit erhalten, und der Vergütung, soweit diese nicht im Vollzug der Freiheits- und Jugendstrafe als Hausgeld oder Eingliederungsgeld und im Vollzug der Freiheitsstrafe als Haftkostenbeitrag in Anspruch genommen wird.

(2) Die Gefangenen können über das Eigengeld verfügen.
§ 63 Absatz 2 sowie die §§ 70 und 71 bleiben unberührt.

### § 68  
Taschengeld

(1) Bedürftigen Straf- und Jugendstrafgefangenen wird auf Antrag Taschengeld gewährt.
Bedürftig sind sie, soweit ihnen aus Hausgeld (§ 70) und Eigengeld (§ 67) monatlich ein Betrag bis zur Höhe des Taschengeldes voraussichtlich nicht zur Verfügung steht.
Finanzielle Anerkennungen nach § 66 Absatz 1 Nummer 3 bleiben bis zur Höhe des Taschengeldbetrags unberücksichtigt.

(2) Straf- und Jugendstrafgefangene gelten als nicht bedürftig, wenn ihnen ein Betrag nach Absatz 1 Satz 2 deshalb nicht zur Verfügung steht, weil sie eine ihnen zumutbare Arbeit nicht angenommen oder eine ausgeübte Arbeit verschuldet verloren haben.
Das gilt auch dann, wenn Jugendstrafgefangene eine nach § 15 Absatz 3 als erforderlich erachtete Arbeit nicht aufgenommen oder verschuldet verloren haben.

(3) Bedürftigen Untersuchungsgefangenen wird auf Antrag Taschengeld gewährt.
Bedürftig sind sie, soweit ihnen im laufenden Monat ein Betrag bis zur Höhe des Taschengeldes voraussichtlich nicht aus eigenen Mitteln zur Verfügung steht.

(4) Das Taschengeld beträgt 14 Prozent der Eckvergütung (§ 66 Absatz 2).
Es wird zu Beginn des Monats im Voraus gewährt.
Gehen den Gefangenen im Laufe des Monats Gelder zu, wird zum Ausgleich ein Betrag bis zur Höhe des gewährten Taschengeldes einbehalten.

(5) Die Gefangenen dürfen über das Taschengeld im Rahmen der Bestimmungen dieses Gesetzes verfügen.
Im Vollzug der Freiheits- und Jugendstrafe wird es dem Hausgeldkonto gutgeschrieben.

### § 69  
Konten, Bargeld

(1) Für die Straf- und Jugendstrafgefangenen werden Hausgeld- und Eigengeldkonten, für die Untersuchungsgefangenen nur Eigengeldkonten in der Anstalt geführt.

(2) Der Besitz von Bargeld in der Anstalt ist den Gefangenen nicht gestattet.
Über Ausnahmen entscheidet die Anstaltsleiterin oder der Anstaltsleiter.

(3) Geld in Fremdwährung wird zur Habe genommen.

### § 70   
Hausgeld

(1) Das Hausgeld wird aus drei Siebteln der in diesem Gesetz geregelten Vergütung gebildet.

(2) Für Straf- und Jugendstrafgefangene, die aus einem freien Beschäftigungsverhältnis, aus einer Selbstbeschäftigung oder anderweitig regelmäßige Einkünfte haben, wird daraus ein angemessenes monatliches Hausgeld festgesetzt.

(3) Für Straf- und Jugendstrafgefangene, die über Eigengeld (§ 67) verfügen und keine hinreichende Vergütung nach diesem Gesetz erhalten, gilt Absatz 2 entsprechend.

(4) Die Straf- und Jugendstrafgefangenen dürfen über das Hausgeld im Rahmen der Bestimmungen dieses Gesetzes verfügen.
Der Anspruch auf Auszahlung ist nicht übertragbar.

### § 71  
Zweckgebundene Einzahlungen

Für Maßnahmen der Eingliederung, insbesondere Kosten der Gesundheitsfürsorge und der Aus- und Fortbildung, und für Maßnahmen der Pflege sozialer Beziehungen, insbesondere Telefonkosten und Fahrtkosten anlässlich Lockerungen, kann zweckgebunden Geld eingezahlt werden.
Das Geld darf nur für diese Zwecke verwendet werden.
Der Anspruch auf Auszahlung ist nicht übertragbar.

### § 72  
Haftkostenbeitrag, Kostenbeteiligung

(1) Die Anstalt erhebt von den Strafgefangenen, die sich in einem freien Beschäftigungsverhältnis befinden oder über anderweitige regelmäßige Einkünfte verfügen, für diese Zeit einen Haftkostenbeitrag.
Vergütungen nach diesem Gesetz bleiben unberücksichtigt.
Den Strafgefangenen muss täglich ein Tagessatz gemäß § 66 Absatz 2 Satz 2 verbleiben.
Von der Geltendmachung des Anspruchs ist abzusehen, soweit die Wiedereingliederung der Strafgefangenen hierdurch gefährdet würde.

(2) Der Haftkostenbeitrag wird in Höhe des Betrages erhoben, der nach § 17 Absatz 1 Satz 1 Nummer 4 des Vierten Buches Sozialgesetzbuch durchschnittlich zur Bewertung der Sachbezüge festgesetzt ist.
Bei Selbstverpflegung entfallen die für die Verpflegung vorgesehenen Beträge.
Für den Wert der Unterkunft ist die festgesetzte Belegungsfähigkeit maßgebend.

(3) Die Gefangenen können an den Betriebskosten der in ihrem Gewahrsam befindlichen Geräte beteiligt werden.

### § 73  
Eingliederungsgeld

(1) Die Strafgefangenen dürfen für Zwecke der Eingliederung nach der Entlassung ein Guthaben in angemessener Höhe bilden (Eingliederungsgeld).
Die Jugendstrafgefangenen sind hierzu verpflichtet.
Der Anspruch auf Auszahlung ist nicht übertragbar.

(2) Die Straf- und Jugendstrafgefangenen dürfen bereits vor der Entlassung für Zwecke des Absatzes 1 Satz 1 über das Eingliederungsgeld verfügen.
Die Anstaltsleiterin oder der Anstaltsleiter kann ihnen gestatten, das Eingliederungsgeld zur Entschädigung der Opfer ihrer Straftaten in Anspruch zu nehmen.

### Abschnitt 11   
Gesundheitsfürsorge

### § 74  
Art und Umfang der medizinischen Leistungen, Kostenbeteiligung

(1) Die Gefangenen haben einen Anspruch auf notwendige, ausreichende und zweckmäßige medizinische Leistungen unter Beachtung des Grundsatzes der Wirtschaftlichkeit und unter Berücksichtigung des allgemeinen Standards der gesetzlichen Krankenversicherung.
Der Anspruch umfasst auch Vorsorgeleistungen, ferner die Versorgung mit medizinischen Hilfsmitteln, soweit diese mit Rücksicht auf die Dauer des Freiheitsentzugs nicht ungerechtfertigt ist und die Hilfsmittel nicht als allgemeine Gebrauchsgegenstände des täglichen Lebens anzusehen sind.

(2) An den Kosten nach Absatz 1 können die Gefangenen in angemessenem Umfang beteiligt werden, höchstens jedoch bis zum Umfang der Beteiligung vergleichbarer gesetzlich Versicherter.
Für Leistungen, die über Absatz 1 hinausgehen, können den Gefangenen die gesamten Kosten auferlegt werden.

(3) Erhalten die Gefangenen Leistungen nach Absatz 1 infolge einer mutwilligen Selbstverletzung, sind sie in angemessenem Umfang an den Kosten zu beteiligen.
Bei den Straf- und Jugendstrafgefangenen unterbleibt die Kostenbeteiligung, wenn hierdurch die Erreichung des Vollzugsziels, insbesondere die Eingliederung, gefährdet würde.

(4) Den Untersuchungsgefangenen soll die Anstaltsleiterin oder der Anstaltsleiter nach Anhörung des ärztlichen Dienstes der Anstalt auf ihren Antrag hin gestatten, auf ihre Kosten externen ärztlichen Rat einzuholen.
Die Erlaubnis kann versagt werden, wenn die Untersuchungsgefangenen die gewählte ärztliche Vertrauensperson und den ärztlichen Dienst der Anstalt nicht wechselseitig von der Schweigepflicht entbinden oder wenn es zur Umsetzung einer Anordnung nach § 119 Absatz 1 der Strafprozessordnung oder zur Aufrechterhaltung der Sicherheit oder Ordnung der Anstalt erforderlich ist.
Die Konsultation soll in der Anstalt stattfinden.

### § 75  
Durchführung der medizinischen Leistungen, Forderungsübergang

(1) Medizinische Diagnostik, Behandlung und Versorgung kranker und hilfsbedürftiger Gefangener erfolgen in der Anstalt, erforderlichenfalls in einer hierfür besser geeigneten Anstalt oder einem Vollzugskrankenhaus, ausnahmsweise auch außerhalb des Vollzugs.
Erfolgt eine Behandlung junger Gefangener außerhalb der Anstalt, sind die Personensorgeberechtigten und das Jugendamt, im Vollzug der Jugendstrafe ist auch die Vollstreckungsleiterin oder der Vollstreckungsleiter zu unterrichten.
Im Vollzug der Untersuchungshaft ist dem Gericht und der Staatsanwaltschaft im Falle einer Behandlung außerhalb der Anstalt nach Möglichkeit Gelegenheit zur Stellungnahme zu geben.

(2) Wird die Strafvollstreckung während einer Behandlung von Straf- oder Jugendstrafgefangenen unterbrochen oder beendet oder werden Untersuchungsgefangene während einer Behandlung aus der Haft entlassen, so hat das Land nur diejenigen Kosten zu tragen, die bis zur Unterbrechung oder Beendigung der Strafvollstreckung oder bis zur Entlassung angefallen sind.

(3) Gesetzliche Schadensersatzansprüche, die Gefangenen infolge einer Körperverletzung gegen Dritte zustehen, gehen insoweit auf das Land über, als den Gefangenen Leistungen nach § 74 Absatz 1 zu gewähren sind.
Von der Geltendmachung der Ansprüche ist im Interesse der Straf- oder Jugendstrafgefangenen abzusehen, wenn hierdurch die Erreichung des Vollzugsziels, insbesondere die Eingliederung, gefährdet würde.

### § 76  
Ärztliche Behandlung zur sozialen Eingliederung

Mit Zustimmung der Straf- oder Jugendstrafgefangenen soll die Anstalt ärztliche Behandlungen, insbesondere Operationen oder prothetische Maßnahmen, durchführen lassen, die ihre soziale Eingliederung fördern.
Die Kosten tragen die Straf- oder Jugendstrafgefangenen.
Sind sie dazu nicht in der Lage, kann die Anstalt die Kosten in begründeten Fällen in angemessenem Umfang übernehmen.

### § 77   
Gesundheitsschutz und Hygiene

(1) Die Anstalt unterstützt die Gefangenen bei der Wiederherstellung und Erhaltung ihrer körperlichen, geistigen und seelischen Gesundheit.
Sie fördert das Bewusstsein für gesunde Ernährung und Lebensführung.
Die Gefangenen haben die notwendigen Anordnungen zum Gesundheitsschutz und zur Hygiene zu befolgen.

(2) Den Gefangenen wird ermöglicht, sich täglich mindestens eine Stunde im Freien aufzuhalten.

### § 78   
Krankenbehandlung während Lockerungen

(1) Die Straf- und Jugendstrafgefangenen haben während Lockerungen einen Anspruch auf medizinische Leistungen gegen das Land nur in der für sie zuständigen Anstalt.
§ 47 Absatz 1 bleibt unberührt.

(2) Der Anspruch auf Leistungen ruht, solange die Straf- und Jugendstrafgefangenen aufgrund eines freien Beschäftigungsverhältnisses krankenversichert sind.

### § 79   
Zwangsmaßnahmen auf dem Gebiet der Gesundheitsfürsorge

(1) Eine medizinische Untersuchung und Behandlung ist ohne Einwilligung der Gefangenen zulässig, um den Erfolg eines Selbsttötungsversuchs zu verhindern.
Eine Maßnahme nach Satz 1 ist auch zulässig, wenn von den Gefangenen eine schwerwiegende Gefahr für die Gesundheit einer anderen Person ausgeht und die Maßnahme verhältnismäßig ist.

(2) Eine medizinische Untersuchung und Behandlung sowie eine Zwangsernährung sind bei Lebensgefahr oder schwerwiegender Gefahr für die Gesundheit der Gefangenen zulässig, wenn diese zur Einsicht in das Vorliegen der Gefahr und die Notwendigkeit der Maßnahme oder zum Handeln gemäß solcher Einsicht krankheitsbedingt nicht fähig sind.

(3) Eine Maßnahme nach Absatz 2 darf nur angeordnet werden, wenn

1.  eine Patientenverfügung im Sinne des § 1901a Absatz 1 Satz 1 des Bürgerlichen Gesetzbuches, deren Festlegungen auf die aktuelle Lebens- und Behandlungssituation zutreffen und gegen die Durchführung der Maßnahme gerichtet sind, der Anstalt nicht vorliegt,
2.  die Gefangenen durch eine Ärztin oder einen Arzt über Notwendigkeit, Art, Umfang, Dauer, zu erwartende Folgen und Risiken der Maßnahme in einer ihrer Auffassungsgabe und ihrem Gesundheitszustand angemessenen Weise aufgeklärt wurden,
3.  der ernsthafte und ohne Ausübung von Druck unternommene Versuch einer Ärztin oder eines Arztes, ein Einverständnis der Gefangenen mit der Maßnahme zu erwirken, erfolglos geblieben ist,
4.  die Maßnahme zur Abwendung einer Gefahr nach Absatz 2 geeignet und erforderlich ist und
5.  der von der Maßnahme erwartete Nutzen die mit der Maßnahme verbundene Belastung und den durch das Unterlassen der Maßnahme möglichen Schaden deutlich überwiegt.

(4) Maßnahmen nach den Absätzen 1 und 2 dürfen nur von der Anstaltsleiterin oder dem Anstaltsleiter auf der Grundlage einer ärztlichen Empfehlung angeordnet werden.
Die Anordnung bedarf der Zustimmung der medizinischen Fachaufsicht.
Durchführung und Überwachung unterstehen ärztlicher Leitung.
Unberührt bleibt die Leistung erster Hilfe für den Fall, dass eine Ärztin oder ein Arzt nicht rechtzeitig erreichbar und mit einem Aufschub Lebensgefahr verbunden ist.
Die Gründe für die Anordnung einer Maßnahme nach Absatz 1 oder Absatz 2, in den Fällen des Absatzes 2 auch das Vorliegen der dort genannten Voraussetzungen sowie die ergriffene Maßnahme, einschließlich ihres Zwangscharakters, die Durchsetzungsweise, die Wirkungsüberwachung sowie der Untersuchungs- und Behandlungsverlauf sind zu dokumentieren.
Gleiches gilt für Erklärungen der Gefangenen, die im Zusammenhang mit Zwangsmaßnahmen von Bedeutung sein können.

(5) Die Anordnung einer Maßnahme nach Absatz 1 Satz 2 oder Absatz 2 ist den Gefangenen vor Durchführung der Maßnahme schriftlich bekannt zu geben.
Sie sind darüber zu belehren, dass sie gegen die Anordnung bei Gericht um einstweiligen Rechtsschutz ersuchen und auch Antrag auf gerichtliche Entscheidung stellen können.
Mit dem Vollzug einer Anordnung ist zuzuwarten, bis die Gefangenen Gelegenheit hatten, eine gerichtliche Entscheidung herbeizuführen.

(6) Bei Gefahr im Verzug finden Absatz 3 Nummer 2 und 3 und Absatz 5 keine Anwendung.

(7) Die zwangsweise körperliche Untersuchung der Gefangenen zum Gesundheitsschutz und zur Hygiene ist zulässig, wenn sie nicht mit einem körperlichen Eingriff verbunden ist.
Sie darf nur von der Anstaltsleiterin oder dem Anstaltsleiter auf der Grundlage einer ärztlichen Stellungnahme angeordnet werden.
Durchführung und Überwachung unterstehen ärztlicher Leitung.

### § 80   
Benachrichtigungspflicht

Erkranken Gefangene schwer oder versterben sie, werden Angehörige und Personensorgeberechtigte sowie gegebenenfalls Verteidigerinnen und Verteidiger und Beistände nach § 69 des Jugendgerichtsgesetzes benachrichtigt.
Dem Wunsch der Gefangenen, auch andere Personen zu benachrichtigen, soll nach Möglichkeit entsprochen werden.

### Abschnitt 12   
Religionsausübung

### § 81   
Seelsorge

Den Gefangenen darf religiöse Betreuung durch eine Seelsorgerin oder einen Seelsorger nicht versagt werden.
Auf Wunsch ist ihnen zu helfen, mit einer Seelsorgerin oder einem Seelsorger ihrer Religionsgemeinschaft in Verbindung zu treten.

### § 82  
Religiöse Veranstaltungen

(1) Die Gefangenen haben das Recht, am Gottesdienst und an anderen religiösen Veranstaltungen ihres Bekenntnisses in der Anstalt teilzunehmen.

(2) Die Zulassung zu den Gottesdiensten oder religiösen Veranstaltungen einer anderen Religionsgemeinschaft bedarf der Zustimmung der Seelsorgerin oder des Seelsorgers der Religionsgemeinschaft.

(3) Gefangene können von der Teilnahme am Gottesdienst oder an anderen religiösen Veranstaltungen ausgeschlossen werden, wenn dies aus überwiegenden Gründen der Sicherheit oder Ordnung, bei Untersuchungsgefangenen auch zur Umsetzung einer Anordnung nach § 119 Absatz 1 der Strafprozessordnung geboten ist; die Seelsorgerin oder der Seelsorger soll vorher gehört werden.

### § 83  
Weltanschauungsgemeinschaften

Für Angehörige weltanschaulicher Bekenntnisse gelten die §§ 59, 81 und 82 entsprechend.

### Abschnitt 13  
Sicherheit und Ordnung

### § 84   
Grundsatz

(1) Sicherheit und Ordnung der Anstalt bilden die Grundlage des Anstaltslebens und tragen dazu bei, dass in der Anstalt ein gewaltfreies Klima herrscht.

(2) Die Pflichten und Beschränkungen, die den Gefangenen zur Aufrechterhaltung der Sicherheit oder Ordnung der Anstalt auferlegt werden, sind so zu wählen, dass sie in einem angemessenen Verhältnis zu ihrem Zweck stehen und die Gefangenen nicht mehr und nicht länger als notwendig beeinträchtigen.

### § 85  
Allgemeine Verhaltenspflichten

(1) Die Gefangenen sind für das geordnete Zusammenleben in der Anstalt mitverantwortlich und müssen mit ihrem Verhalten dazu beitragen.
Auf eine einvernehmliche Streitbeilegung ist hinzuwirken.

(2) Die Gefangenen haben die Anordnungen der Bediensteten zu befolgen, auch wenn sie sich durch diese beschwert fühlen.

(3) Die Gefangenen haben ihren Haftraum und die ihnen von der Anstalt überlassenen Sachen in Ordnung zu halten und schonend zu behandeln.

(4) Die Gefangenen haben Umstände, die eine Gefahr für das Leben oder eine erhebliche Gefahr für die Gesundheit einer Person bedeuten, unverzüglich zu melden.

### § 86  
Absuchung, Durchsuchung

(1) Die Gefangenen, ihre Sachen und die Hafträume dürfen mit technischen Mitteln oder sonstigen Hilfsmitteln abgesucht und durchsucht werden.
Die Durchsuchung der männlichen Gefangenen darf nur von Männern, die Durchsuchung weiblicher Gefangener darf nur von Frauen vorgenommen werden.
Das Schamgefühl ist zu schonen.

(2) Nur bei Gefahr im Verzug oder auf Anordnung der Anstaltsleiterin oder des Anstaltsleiters im Einzelfall ist es zulässig, eine mit einer Entkleidung verbundene körperliche Durchsuchung vorzunehmen.
Sie darf bei männlichen Gefangenen nur in Gegenwart von Männern, bei weiblichen Gefangenen nur in Gegenwart von Frauen erfolgen.
Sie ist in einem geschlossenen Raum durchzuführen.
Andere Gefangene dürfen nicht anwesend sein.

(3) Die Anstaltsleiterin oder der Anstaltsleiter kann allgemein anordnen, dass die Gefangenen in der Regel bei der Aufnahme, nach Kontakten mit Besucherinnen und Besuchern sowie nach jeder unbeaufsichtigten Abwesenheit von der Anstalt nach Absatz 2 zu durchsuchen sind.

(4) Die Anordnung ist zu begründen.
Anordnung, Durchführung und Ergebnis der Durchsuchungen nach den Absätzen 2 und 3 sind aktenkundig zu machen.

### § 87  
Sichere Unterbringung

Gefangene können in eine Anstalt verlegt werden, die zu ihrer sicheren Unterbringung besser geeignet ist, wenn in erhöhtem Maße die Gefahr der Entweichung oder Befreiung gegeben ist oder sonst ihr Verhalten oder ihr Zustand eine Gefahr für die Sicherheit der Anstalt darstellt.
§ 24 Absatz 4 und 5 gilt entsprechend.

### § 88  
Maßnahmen zur Feststellung von Suchtmittelgebrauch

(1) Zur Aufrechterhaltung der Sicherheit oder Ordnung der Anstalt kann die Anstaltsleiterin oder der Anstaltsleiter allgemein oder im Einzelfall Maßnahmen anordnen, die geeignet sind, den Gebrauch von Suchtmitteln festzustellen.
Diese Maßnahmen dürfen nicht mit einem körperlichen Eingriff verbunden sein.

(2) Verweigern Gefangene die Mitwirkung an Maßnahmen nach Absatz 1 ohne hinreichenden Grund, ist davon auszugehen, dass Suchtmittelfreiheit nicht gegeben ist.

(3) Wird verbotener Suchtmittelgebrauch festgestellt, können die Kosten der Maßnahmen den Gefangenen auferlegt werden.

### § 89  
Festnahmerecht

Gefangene, die entwichen sind oder sich sonst ohne Erlaubnis außerhalb der Anstalt aufhalten, können durch die Anstalt oder auf deren Veranlassung festgenommen und zurückgebracht werden.
Führt die Verfolgung oder die von der Anstalt veranlasste Fahndung nicht alsbald zur Wiederergreifung, so sind die weiteren Maßnahmen der Vollstreckungsbehörde zu überlassen.

### § 90  
Besondere Sicherungsmaßnahmen

(1) Gegen Gefangene können besondere Sicherungsmaßnahmen angeordnet werden, wenn nach ihrem Verhalten oder aufgrund ihres seelischen Zustandes in erhöhtem Maße die Gefahr der Entweichung, von Gewalttätigkeiten gegen Personen oder Sachen, der Selbsttötung oder der Selbstverletzung besteht.

(2) Als besondere Sicherungsmaßnahmen sind zulässig:

1.  der Entzug oder die Vorenthaltung von Gegenständen,
2.  die Beobachtung der Gefangenen, auch mit optisch-elektronischen Einrichtungen,
3.  die Trennung von allen anderen Gefangenen (Absonderung),
4.  die Beschränkung des Aufenthalts im Freien,
5.  die Unterbringung in einem besonders gesicherten Haftraum ohne gefährdende Gegenstände und
6.  die Fesselung.

(3) Maßnahmen nach Absatz 2 Nummer 1 und 3 bis 5 sind auch zulässig, wenn die Gefahr einer Befreiung oder eine erhebliche Störung der Ordnung anders nicht vermieden oder behoben werden kann, nach Absatz 2 Nummer 4 jedoch nicht bei jungen Gefangenen.

(4) Eine Absonderung von mehr als 24 Stunden Dauer ist nur zulässig, wenn sie zur Abwehr einer in der Person des Gefangenen liegenden Gefahr unerlässlich ist.

(5) In der Regel dürfen Fesseln nur an den Händen oder an den Füßen angelegt werden.
Im Interesse der Gefangenen kann die Anstaltsleiterin oder der Anstaltsleiter eine andere Art der Fesselung anordnen.
Die Fesselung wird zeitweise gelockert, soweit dies notwendig ist.

(6) Eine Fesselung, durch die die Bewegungsfreiheit vollständig aufgehoben wird (Fixierung), ist nur zulässig, soweit und solange die Gefahr von Gewalttätigkeiten gegen Personen, der Selbsttötung oder der Selbstverletzung in erhöhtem Maß besteht und die Fixierung zur Abwehr dieser Gefahr unerlässlich ist.

(7) Besteht die Gefahr der Entweichung, dürfen die Gefangenen bei einer Ausführung, Vorführung oder beim Transport gefesselt werden.

### § 91  
Anordnung besonderer Sicherungsmaßnahmen, Verfahren

(1) Besondere Sicherungsmaßnahmen ordnet die Anstaltsleiterin oder der Anstaltsleiter an.
Bei Gefahr im Verzug können auch andere Bedienstete diese Maßnahmen vorläufig anordnen; die Entscheidung der Anstaltsleiterin oder des Anstaltsleiters ist unverzüglich einzuholen.

(2) Eine nicht nur kurzfristige Fixierung bedarf einer vorherigen richterlichen Anordnung.
Bei Gefahr im Verzug können auch die Anstaltsleiterin oder der Anstaltsleiter die Fixierung vorläufig anordnen; die Entscheidung des Gerichts ist unverzüglich einzuholen.
Wird die vorläufige Anordnung der Fixierung vor Erlangung einer richterlichen Entscheidung aufgehoben, so ist dies dem Gericht unverzüglich mitzuteilen.

(3) Werden die Gefangenen ärztlich behandelt oder beobachtet oder bildet ihr seelischer Zustand den Anlass der besonderen Sicherungsmaßnahme, ist vorher eine ärztliche Stellungnahme einzuholen.
Ist dies wegen Gefahr im Verzug nicht möglich, wird die Stellungnahme unverzüglich nachträglich eingeholt.

(4) Die Entscheidung wird den Gefangenen von der Anstaltsleiterin oder dem Anstaltsleiter mündlich eröffnet und mit einer kurzen Begründung schriftlich abgefasst.
Die Anordnung ist aktenkundig zu machen.
Die Anordnung einer Fixierung, deren Grund und deren Verlauf, insbesondere die Art der Überwachung und Betreuung, sind umfassend zu dokumentieren.

(5) Besondere Sicherungsmaßnahmen dürfen nur so weit aufrechterhalten werden, wie es ihr Zweck erfordert.
Sie sind in angemessenen Abständen daraufhin zu überprüfen, ob und in welchem Umfang sie aufrechterhalten werden müssen.
Das Ergebnis der Überprüfungen und die Durchführung der Maßnahmen einschließlich der Beteiligung des ärztlichen Dienstes sind aktenkundig zu machen.

(6) Absonderung, Unterbringung im besonders gesicherten Haftraum und Fesselung sind der Aufsichtsbehörde, im Vollzug der Untersuchungshaft auch dem Gericht und der Staatsanwaltschaft, unverzüglich mitzuteilen, wenn sie länger als zwei Tage aufrechterhalten werden.
Absonderung und Unterbringung im besonders gesicherten Haftraum von mehr als 20 Tagen Gesamtdauer innerhalb von zwölf Monaten bedürfen der Zustimmung der Aufsichtsbehörde.

(7) Während der Absonderung, der Fixierung und Unterbringung im besonders gesicherten Haftraum sind die Gefangenen in besonderem Maße zu betreuen.
Sind die Gefangenen fixiert, sind sie durch Bedienstete ständig und in unmittelbarem Sichtkontakt zu beobachten.

(8) Nach Beendigung der Fixierung sind die Gefangenen auf ihr Recht hinzuweisen, die Rechtmäßigkeit der Anordnung gerichtlich überprüfen zu lassen.
Der Hinweis ist aktenkundig zu machen.

### § 92  
Ärztliche Überwachung

(1) Sind die Gefangenen in einem besonders gesicherten Haftraum untergebracht oder gefesselt, sucht sie die Ärztin oder der Arzt alsbald und in der Folge täglich auf.
Dies gilt nicht bei einer Fesselung während einer Ausführung, Vorführung oder eines Transports sowie bei Bewegungen innerhalb der Anstalt.

(2) Die Ärztin oder der Arzt ist regelmäßig zu hören, solange Gefangene länger als 24 Stunden abgesondert sind.

### Abschnitt 14  
Unmittelbarer Zwang

### § 93  
Begriffsbestimmungen

(1) Unmittelbarer Zwang ist die Einwirkung auf Personen oder Sachen durch körperliche Gewalt, ihre Hilfsmittel oder durch Waffen.

(2) Körperliche Gewalt ist jede unmittelbare körperliche Einwirkung auf Personen oder Sachen.

(3) Hilfsmittel der körperlichen Gewalt sind namentlich Fesseln.
Waffen sind Hieb- und Schusswaffen.

(4) Es dürfen nur dienstlich zugelassene Hilfsmittel und Waffen verwendet werden.

### § 94  
Allgemeine Voraussetzungen

(1) Bedienstete dürfen unmittelbaren Zwang anwenden, wenn sie Vollzugs- und Sicherungsmaßnahmen rechtmäßig durchführen und der damit verfolgte Zweck auf keine andere Weise erreicht werden kann.

(2) Gegen andere Personen als Gefangene darf unmittelbarer Zwang angewendet werden, wenn sie es unternehmen, Gefangene zu befreien oder widerrechtlich in die Anstalt einzudringen, oder wenn sie sich unbefugt darin aufhalten.

(3) Das Recht zu unmittelbarem Zwang aufgrund anderer Regelungen bleibt unberührt.

### § 95  
Grundsatz der Verhältnismäßigkeit

(1) Unter mehreren möglichen und geeigneten Maßnahmen des unmittelbaren Zwangs sind diejenigen zu wählen, die den Einzelnen und die Allgemeinheit voraussichtlich am wenigsten beeinträchtigen.

(2) Unmittelbarer Zwang unterbleibt, wenn ein durch ihn zu erwartender Schaden erkennbar außer Verhältnis zu dem angestrebten Erfolg steht.

### § 96  
Androhung

Unmittelbarer Zwang ist vorher anzudrohen.
Die Androhung darf nur dann unterbleiben, wenn die Umstände sie nicht zulassen oder unmittelbarer Zwang sofort angewendet werden muss, um eine rechtswidrige Tat, die den Tatbestand eines Strafgesetzes erfüllt, zu verhindern oder eine gegenwärtige Gefahr abzuwenden.

### § 97  
Schusswaffengebrauch

(1) Der Gebrauch von Schusswaffen durch Bedienstete innerhalb der Anstalt ist verboten.
Das Recht zum Schusswaffengebrauch aufgrund anderer Vorschriften durch Polizeivollzugsbedienstete bleibt davon unberührt.

(2) Außerhalb der Anstalt dürfen Schusswaffen durch Bedienstete nur nach Maßgabe der folgenden Absätze und nur dann gebraucht werden, wenn andere Maßnahmen des unmittelbaren Zwangs bereits erfolglos waren oder keinen Erfolg versprechen.
Gegen Personen ist ihr Gebrauch nur zulässig, wenn der Zweck nicht durch Waffenwirkung gegen Sachen erreicht werden kann.

(3) Schusswaffen dürfen nur die dazu bestimmten Bediensteten gebrauchen und nur, um angriffs- oder fluchtunfähig zu machen.
Ihr Gebrauch unterbleibt, wenn dadurch erkennbar Unbeteiligte mit hoher Wahrscheinlichkeit gefährdet würden.

(4) Der Gebrauch von Schusswaffen ist vorher anzudrohen.
Als Androhung gilt auch ein Warnschuss.
Ohne Androhung dürfen Schusswaffen nur dann gebraucht werden, wenn dies zur Abwehr einer gegenwärtigen Gefahr für Leib oder Leben erforderlich ist.

(5) Gegen Gefangene dürfen Schusswaffen gebraucht werden,

1.  wenn sie eine Waffe oder ein anderes gefährliches Werkzeug trotz wiederholter Aufforderung nicht ablegen,
2.  wenn sie eine Meuterei (§ 121 des Strafgesetzbuches) unternehmen oder
3.  um ihre Entweichung zu vereiteln oder um sie wieder zu ergreifen.

Satz 1 Nummer 2 und 3 findet auf minderjährige Gefangene keine Anwendung.
Satz 1 Nummer 3 findet keine Anwendung auf Gefangene, die im offenen Vollzug untergebracht sind.

(6) Gegen andere Personen dürfen Schusswaffen gebraucht werden, wenn sie es unternehmen, Gefangene gewaltsam zu befreien.

### Abschnitt 15  
Erzieherische Maßnahmen, Einvernehmliche Streitbeilegung, Disziplinarmaßnahmen

### § 98  
Erzieherische Maßnahmen

(1) Verstöße der jungen Gefangenen gegen Pflichten, die ihnen durch oder aufgrund dieses Gesetzes auferlegt sind, sind unverzüglich im erzieherischen Gespräch aufzuarbeiten.
Daneben können Maßnahmen angeordnet werden, die geeignet sind, den jungen Gefangenen ihr Fehlverhalten bewusst zu machen (erzieherische Maßnahmen).
Als erzieherische Maßnahmen kommen namentlich in Betracht die Erteilung von Weisungen und Auflagen, die Beschränkung oder der Entzug einzelner Gegenstände für die Freizeitbeschäftigung und der Ausschluss von gemeinsamer Freizeit oder von einzelnen Freizeitveranstaltungen bis zur Dauer einer Woche.

(2) § 99 Absatz 1 findet entsprechende Anwendung.
Erfüllen die jungen Gefangenen die Vereinbarung, ist die Anordnung erzieherischer Maßnahmen ausgeschlossen.

(3) Die Anstaltsleiterin oder der Anstaltsleiter legt fest, welche Bediensteten befugt sind, erzieherische Gespräche zu führen und erzieherische Maßnahmen anzuordnen.

(4) Es sollen solche erzieherischen Maßnahmen angeordnet werden, die mit der Verfehlung in Zusammenhang stehen.

### § 99  
Einvernehmliche Streitbeilegung

(1) Verstoßen Gefangene gegen Pflichten, die ihnen durch oder aufgrund dieses Gesetzes auferlegt sind, können in geeigneten Fällen zur Abwendung von Disziplinarmaßnahmen im Wege einvernehmlicher Streitbeilegung Vereinbarungen getroffen werden.
Insbesondere kommen die Wiedergutmachung des Schadens, die Entschuldigung beim Geschädigten, die Erbringung von Leistungen für die Gemeinschaft und das vorübergehende Verbleiben im Haftraum in Betracht.
Die Vereinbarungen sind in der Gefangenenpersonalakte zu dokumentieren.

(2) Erfüllen die Gefangenen die Vereinbarungen, so ist die Durchführung eines Disziplinarverfahrens unzulässig.

### § 100  
Disziplinarmaßnahmen

(1) Disziplinarmaßnahmen können angeordnet werden, wenn die Gefangenen rechtswidrig und schuldhaft

1.  andere Personen verbal oder tätlich angreifen,
2.  Lebensmittel oder fremde Sachen zerstören oder beschädigen,
3.  in sonstiger Weise gegen Strafgesetze verstoßen oder eine Ordnungswidrigkeit begehen,
4.  verbotene Gegenstände in die Anstalt einbringen, sich an deren Einbringung beteiligen, sie besitzen oder weitergeben,
5.  unerlaubt Betäubungsmittel oder andere berauschende Stoffe herstellen oder konsumieren,
6.  entweichen oder zu entweichen versuchen,
7.  gegen Weisungen im Zusammenhang mit der Gewährung von Lockerungen verstoßen,
8.  gegen eine Anordnung nach § 119 Absatz 1 der Strafprozessordnung verstoßen,
9.  wiederholt oder schwerwiegend gegen sonstige Pflichten verstoßen, die ihnen durch dieses Gesetz oder aufgrund dieses Gesetzes auferlegt sind, und dadurch das geordnete Zusammenleben in der Anstalt stören oder
10.  im Vollzug der Jugendstrafe sich zugewiesenen Aufgaben entziehen.

(2) Disziplinarmaßnahmen dürfen gegen junge Gefangene nur angeordnet werden, wenn erzieherische Maßnahmen nicht nach § 98 Absatz 2 Satz 2 ausgeschlossen sind oder nicht ausreichen, um ihnen das Unrecht ihrer Handlung zu verdeutlichen.

(3) Zulässige Disziplinarmaßnahmen sind

1.  der Verweis,
2.  die Beschränkung oder der Entzug des Fernsehempfangs bis zu drei Monaten,
3.  die Beschränkung oder der Entzug der Gegenstände für die Freizeitbeschäftigung, mit Ausnahme des Lesestoffs, bis zu drei Monaten,
4.  die Beschränkung oder der Entzug des Aufenthalts in Gemeinschaft oder der Teilnahme an einzelnen Freizeitveranstaltungen bis zu vier Wochen,
5.  die Beschränkung des Einkaufs bis zu zwei Monaten,
6.  die Beschränkung oder der Entzug von Annehmlichkeiten nach § 64 bis zu drei Monaten,
7.  die Kürzung des Arbeitsentgelts um 10 Prozent bis zu drei Monaten.

Auf junge Gefangene findet Satz 1 Nummer 1 keine Anwendung; Maßnahmen nach Satz 1 Nummer 2, 3, 6 und 7 sind nur bis zu zwei Monaten, die Maßnahme nach Satz 1 Nummer 5 ist nur bis zu vier Wochen zulässig.
Maßnahmen nach Satz 1 Nummer 4 sind auf die Teilnahme an gemeinschaftlicher Freizeit und einzelne Freizeitveranstaltungen begrenzt.

(4) Mehrere Disziplinarmaßnahmen können miteinander verbunden werden.

(5) Disziplinarmaßnahmen sind auch zulässig, wenn wegen derselben Verfehlung ein Straf- oder Bußgeldverfahren eingeleitet wird.

(6) Bei der Auswahl der Disziplinarmaßnahmen im Vollzug der Untersuchungshaft sind Grund und Zweck der Haft sowie die psychischen Auswirkungen der Untersuchungshaft und des Strafverfahrens auf die Untersuchungsgefangenen zu berücksichtigen.
Durch die Anordnung und den Vollzug einer Disziplinarmaßnahme dürfen die Verteidigung, die Verhandlungsfähigkeit und die Verfügbarkeit der Untersuchungsgefangenen für die Verhandlung nicht beeinträchtigt werden.

### § 101  
Vollzug der Disziplinarmaßnahmen, Aussetzung zur Bewährung

(1) Disziplinarmaßnahmen werden in der Regel sofort vollstreckt.
Die Vollstreckung ist auszusetzen, soweit es zur Gewährung eines effektiven Rechtsschutzes erforderlich ist.

(2) Disziplinarmaßnahmen können ganz oder teilweise bis zu sechs Monaten zur Bewährung ausgesetzt werden.
Die Aussetzung zur Bewährung kann ganz oder teilweise widerrufen werden, wenn Gefangene die ihr zugrunde liegenden Erwartungen nicht erfüllen.

(3) Im Vollzug der Untersuchungshaft angeordnete Disziplinarmaßnahmen können ganz oder zum Teil auch während einer der Untersuchungshaft unmittelbar nachfolgenden Haft vollstreckt werden.

### § 102  
Disziplinarbefugnis

(1) Disziplinarmaßnahmen ordnet die Anstaltsleiterin oder der Anstaltsleiter an.
Bei einer Verfehlung auf dem Weg in eine andere Anstalt zum Zweck der Verlegung ist die Leiterin oder der Leiter der Bestimmungsanstalt zuständig.

(2) Die Aufsichtsbehörde entscheidet, wenn sich die Verfehlung gegen die Anstaltsleiterin oder den Anstaltsleiter richtet.

(3) Disziplinarmaßnahmen, die in einer anderen Anstalt angeordnet worden sind, werden auf Ersuchen vollstreckt.
§ 101 Absatz 2 bleibt unberührt.

### § 103  
Verfahren

(1) Der Sachverhalt ist zu klären.
Hierbei sind sowohl belastende als auch entlastende Umstände zu ermitteln.
Die betroffenen Gefangenen werden gehört.
Sie werden darüber unterrichtet, welche Verfehlungen ihnen zur Last gelegt werden.
Sie sind darauf hinzuweisen, dass es ihnen freisteht sich zu äußern.
Die Erhebungen werden in einer Niederschrift festgelegt; die Einlassung der Gefangenen wird vermerkt.

(2) Mehrere Verfehlungen, die gleichzeitig zu beurteilen sind, werden durch eine Entscheidung geahndet.

(3) Die Anstaltsleiterin oder der Anstaltsleiter soll sich vor der Entscheidung mit Personen besprechen, die an der Vollzugsgestaltung mitwirken.
Bei Schwangeren, stillenden Müttern oder bei Gefangenen, die sich in ärztlicher Behandlung befinden, ist eine Ärztin oder ein Arzt zu hören.

(4) Vor der Entscheidung über eine Disziplinarmaßnahme erhalten die Gefangenen Gelegenheit, sich zu dem Ergebnis der Ermittlungen zu äußern.
Die Entscheidung wird den Gefangenen von der Anstaltsleiterin oder vom Anstaltsleiter mündlich eröffnet und mit einer kurzen Begründung schriftlich abgefasst.

### Abschnitt 16  
Aufhebung von Maßnahmen, Beschwerde

### § 104  
Aufhebung von Maßnahmen

(1) Die Aufhebung von Maßnahmen zur Regelung einzelner Angelegenheiten auf dem Gebiet des Vollzugs richtet sich nach den nachfolgenden Absätzen, soweit dieses Gesetz keine abweichende Bestimmung enthält.

(2) Rechtswidrige Maßnahmen können ganz oder teilweise mit Wirkung für die Vergangenheit und die Zukunft zurückgenommen werden.

(3) Rechtmäßige Maßnahmen können ganz oder teilweise mit Wirkung für die Zukunft widerrufen werden, wenn

1.  aufgrund nachträglich eingetretener oder bekannt gewordener Umstände die Maßnahmen hätten unterbleiben können,
2.  die Maßnahmen missbraucht werden oder
3.  Weisungen nicht befolgt werden.

(4) Begünstigende Maßnahmen dürfen nach Absatz 2 oder Absatz 3 nur aufgehoben werden, wenn die vollzuglichen Interessen an der Aufhebung in Abwägung mit dem schutzwürdigen Vertrauen der Betroffenen auf den Bestand der Maßnahmen überwiegen.
Davon ist auszugehen, wenn die Aufhebung einer Maßnahme unerlässlich ist, um die Sicherheit der Anstalt zu gewährleisten.

(5) Der gerichtliche Rechtsschutz bleibt unberührt.

### § 105  
Beschwerderecht

(1) Die Gefangenen erhalten Gelegenheit, sich mit Wünschen, Anregungen und Beschwerden in vollzuglichen Angelegenheiten, die sie selbst betreffen, an die Anstaltsleiterin oder den Anstaltsleiter zu wenden.

(2) Besichtigen Vertreterinnen oder Vertreter der Aufsichtsbehörde die Anstalt, so ist zu gewährleisten, dass die Gefangenen sich in vollzuglichen Angelegenheiten, die sie selbst betreffen, an diese wenden können.

(3) Die Möglichkeit der Dienstaufsichtsbeschwerde bleibt unberührt.

### Abschnitt 17  
Kriminologische Forschung

### § 106   
Evaluation, kriminologische Forschung, Berichtspflicht

(1) Behandlungsprogramme für Straf- und Jugendstrafgefangene sind auf der Grundlage wissenschaftlicher Erkenntnisse zu konzipieren, zu standardisieren und auf ihre Wirksamkeit hin zu überprüfen.

(2) Der Vollzug der Freiheits- und der Jugendstrafe, insbesondere seine Aufgabenerfüllung und Gestaltung, die Umsetzung seiner Leitlinien sowie die Behandlungsprogramme und deren Wirkungen auf die Erreichung des Vollzugsziels, soll regelmäßig von dem kriminologischen Dienst, von einer Hochschule oder von einer anderen Stelle wissenschaftlich begleitet und erforscht werden.

(3) Das für den Justizvollzug zuständige Mitglied der Landesregierung berichtet dem Rechtsausschuss des Landtages erstmals im Jahr 2014 und sodann im zweiten und im vierten Jahr der jeweiligen Legislaturperiode unter Einbeziehung der sich aus den wissenschaftlichen Untersuchungen nach den Absätzen 1 und 2 ergebenden Erkenntnisse zum Stand des Justizvollzugs im Land Brandenburg.

### Abschnitt 18   
Aufbau und Organisation der Anstalten

### § 107   
Anstalten

(1) Es werden Anstalten und Abteilungen eingerichtet, die den unterschiedlichen vollzuglichen Anforderungen Rechnung tragen.
Für den Vollzug der Freiheits- und Jugendstrafe sind insbesondere sozialtherapeutische Abteilungen und Eingliederungsabteilungen vorzusehen.

(2) Es sind bedarfsgerechte Einrichtungen für therapeutische Maßnahmen, schulische und berufliche Qualifizierung, Arbeitstraining, Arbeitstherapie und Arbeit vorzusehen.
Gleiches gilt für Besuche, Freizeit, Sport und Seelsorge.

(3) Haft- und Funktionsräume sind zweckentsprechend auszustatten.

(4) Unterhalten private Unternehmen Betriebe in Anstalten, kann die technische und fachliche Leitung ihren Mitarbeiterinnen und Mitarbeitern übertragen werden.

### § 108  
Festsetzung der Belegungsfähigkeit, Verbot der Überbelegung

(1) Die Aufsichtsbehörde setzt die Belegungsfähigkeit der Anstalt so fest, dass eine angemessene Unterbringung der Gefangenen gewährleistet ist.
§ 107 Absatz 2 ist zu berücksichtigen.

(2) Hafträume dürfen nicht mit mehr Gefangenen als zugelassen, höchstens jedoch mit zwei Gefangenen, belegt werden.

(3) Ausnahmen von Absatz 2 sind nur vorübergehend und nur mit Zustimmung der Aufsichtsbehörde zulässig.

(4) Ein für eine Belegung mit zwei Gefangenen zugelassener Haftraum muss über eine Grundfläche von mindestens 14 Quadratmetern verfügen.

### § 109   
Anstaltsleitung

(1) Die Anstaltsleiterin oder der Anstaltsleiter trägt die Verantwortung für den gesamten Vollzug und vertritt die Anstalt nach außen.
Sie oder er kann einzelne Aufgabenbereiche auf andere Bedienstete übertragen.
Die Aufsichtsbehörde kann sich die Zustimmung zur Übertragung vorbehalten.

(2) Für jede Anstalt ist eine Beamtin oder ein Beamter des höheren Dienstes zur hauptamtlichen Leiterin oder zum hauptamtlichen Leiter zu bestellen.
Aus besonderen Gründen kann eine Anstalt auch von einer Beamtin oder einem Beamten des gehobenen Dienstes geleitet werden.

### § 110  
Bedienstete

(1) Die Anstalt wird mit dem für die Erreichung des Vollzugsziels und die Erfüllung ihrer Aufgaben erforderlichen Personal, insbesondere im medizinischen, psychologischen, pädagogischen und sozialen Dienst, im allgemeinen Vollzugsdienst und im Werkdienst, ausgestattet.
Die im Vollzug der Jugendstrafe und der Untersuchungshaft an jungen Untersuchungsgefangenen tätigen Bediensteten müssen für die erzieherische Gestaltung geeignet und qualifiziert sein.

(2) Für die Betreuung von Strafgefangenen mit angeordneter oder vorbehaltener Sicherungsverwahrung und Jugendstrafgefangenen mit vorbehaltener Sicherungsverwahrung nach § 66c Absatz 2 des Strafgesetzbuches ist besonders qualifiziertes Personal vorzusehen und eine fachübergreifende Zusammenarbeit zu gewährleisten.
Soweit erforderlich, sind externe Fachkräfte einzubeziehen.

(3) Fortbildung sowie Praxisberatung und -begleitung werden regelmäßig durchgeführt.

### § 111   
Anstaltsseelsorgerinnen, Anstaltsseelsorger

(1) Die Anstalt wird mit der für die religiöse Betreuung der Gefangenen erforderlichen Anzahl von Seelsorgerinnen oder Seelsorgern (Anstaltsseelsorgerinnen, Anstaltsseelsorger) ausgestattet.
Anstaltsseelsorgerinnen und Anstaltsseelsorger werden von der jeweiligen Religionsgemeinschaft im Einvernehmen mit der Aufsichtsbehörde berufen.
Sie wirken in enger Zusammenarbeit mit den anderen im Vollzug Tätigen eigenverantwortlich an der Erreichung des Vollzugsziels mit.

(2) Wenn die geringe Anzahl der Angehörigen einer Religionsgemeinschaft eine Seelsorge nach Absatz 1 nicht rechtfertigt, ist die seelsorgerische Betreuung auf andere Weise zuzulassen.

(3) Mit Zustimmung der Anstaltsleiterin oder des Anstaltsleiters darf die Anstaltsseelsorgerin oder der Anstaltsseelsorger sich freier Seelsorgehelferinnen und Seelsorgehelfer bedienen und diese für Gottesdienste sowie für andere religiöse Veranstaltungen von außen zuziehen.

### § 112  
Medizinische Versorgung

(1) Die ärztliche Versorgung ist sicherzustellen.

(2) Die Pflege der Kranken soll von Bediensteten ausgeführt werden, die eine Erlaubnis nach dem Krankenpflegegesetz besitzen.
Solange diese nicht zur Verfügung stehen, können auch Bedienstete eingesetzt werden, die eine sonstige Ausbildung in der Krankenpflege erfahren haben.

### § 113  
Interessenvertretung der Gefangenen

Den Gefangenen soll ermöglicht werden, Vertretungen zu bilden.
Diese können in Angelegenheiten von gemeinsamem Interesse, die sich ihrer Eigenart nach für eine Mitwirkung eignen, Vorschläge und Anregungen an die Anstalt herantragen.
Diese sind mit der Vertretung zu erörtern.

### § 114  
Hausordnung

Die Anstaltsleiterin oder der Anstaltsleiter erlässt zur Gestaltung und Organisation des Vollzugsalltags eine Hausordnung auf der Grundlage dieses Gesetzes.
Die Aufsichtsbehörde kann sich die Genehmigung vorbehalten.
Die Hausordnung ist in die am häufigsten benötigten Fremdsprachen zu übersetzen.

### Abschnitt 19   
Aufsicht, Beirat

### § 115   
Aufsichtsbehörde

(1) Das für den Justizvollzug zuständige Mitglied der Landesregierung führt die Aufsicht über die Anstalten (Aufsichtsbehörde).

(2) An der Aufsicht über die Gesundheitsfürsorge sowie die Betreuung und Behandlung der Gefangenen sind pädagogische, sozialpädagogische, psychologische, psychiatrische und medizinische Fachkräfte zu beteiligen.

(3) Die Aufsichtsbehörde kann sich Entscheidungen über Verlegungen und Überstellungen vorbehalten.

### § 116  
Vollstreckungsplan, Vollzugsgemeinschaften

(1) Die Aufsichtsbehörde regelt die örtliche und sachliche Zuständigkeit der Anstalten in einem Vollstreckungsplan.

(2) Im Rahmen von Vollzugsgemeinschaften kann der Vollzug auch in Vollzugseinrichtungen anderer Länder vorgesehen werden.

### § 117   
Beirat

(1) Bei der Anstalt ist ein Beirat zu bilden.
Auf eine ausgewogene Besetzung mit Frauen und Männern wird hingewirkt.
Bedienstete dürfen nicht Mitglieder des Beirats sein.

(2) Die Mitglieder des Beirats wirken beratend bei der Gestaltung des Vollzugs und der Eingliederung der Gefangenen mit.
Sie fördern das Verständnis für den Vollzug und seine gesellschaftliche Akzeptanz und vermitteln Kontakte zu öffentlichen und privaten Einrichtungen.

(3) Der Beirat steht der Anstaltsleiterin oder dem Anstaltsleiter, den Bediensteten und den Gefangenen als Ansprechpartner zur Verfügung.

(4) Die Mitglieder des Beirats können sich über die Unterbringung der Gefangenen und die Gestaltung des Vollzugs sowie die Arbeitsbedingungen der Bediensteten unterrichten und die Anstalt besichtigen.
Sie können die Gefangenen in ihren Räumen aufsuchen.
Unterhaltung und Schriftwechsel werden nicht überwacht.

(5) Die Mitglieder des Beirats sind verpflichtet, außerhalb ihres Amtes über alle Angelegenheiten, die ihrer Natur nach vertraulich sind, besonders über Namen und Persönlichkeit der Gefangenen, Verschwiegenheit zu bewahren.
Dies gilt auch nach Beendigung ihres Amtes.

### Abschnitt 20  
Verhinderung von Mobilfunkverkehr

### § 118  
Verbot und Störung des Mobilfunkverkehrs

(1) Der Besitz und die Benutzung von Geräten zur funkbasierten Übertragung von Informationen sind auf dem Anstaltsgelände des geschlossenen Vollzugs verboten.
Die Anstaltsleiterin oder der Anstaltsleiter kann abweichende Regelungen treffen.

(2) Die Anstalt darf technische Geräte betreiben, die

1.  das Auffinden von Geräten zur Funkübertragung ermöglichen,
2.  Geräte zur Funkübertragung zum Zwecke des Auffindens aktivieren können oder
3.  Frequenzen stören oder unterdrücken, die der Herstellung oder Aufrechterhaltung unerlaubter Funkverbindungen auf dem Anstaltsgelände dienen.

Sie hat die von der Bundesnetzagentur gemäß § 55 Absatz 1 Satz 5 des Telekommunikationsgesetzes festgelegten Rahmenbedingungen zu beachten.
Frequenznutzungen außerhalb des Anstaltsgeländes dürfen nicht erheblich gestört werden.

### Abschnitt 21  
Vollzug des Strafarrests

### § 119  
Grundsatz

(1) Für den Vollzug des Strafarrests in Anstalten gelten die die Strafgefangenen betreffenden Bestimmungen dieses Gesetzes entsprechend, soweit § 120 nicht Abweichendes bestimmt.

(2) § 120 Absatz 1 bis 3, 7 und 8 gilt nicht, wenn Strafarrest in Unterbrechung einer anderen freiheitsentziehenden Maßnahme vollzogen wird.

### § 120  
Besondere Bestimmungen

(1) Strafarrestanten sollen im offenen Vollzug untergebracht werden.

(2) Eine gemeinsame Unterbringung ist nur mit Einwilligung der Strafarrestanten zulässig.

(3) Besuche, Telefongespräche und Schriftwechsel dürfen nur untersagt oder überwacht werden, wenn dies aus Gründen der Sicherheit oder Ordnung der Anstalt notwendig ist.

(4) Den Strafarrestanten soll gestattet werden, einmal wöchentlich Besuch zu empfangen.

(5) Strafarrestanten dürfen eigene Kleidung tragen und eigenes Bettzeug benutzen, wenn Gründe der Sicherheit nicht entgegenstehen und sie für Reinigung, Instandsetzung und regelmäßigen Wechsel auf eigene Kosten sorgen.

(6) Sie dürfen Nahrungs- und Genussmittel sowie Mittel zur Körperpflege in angemessenem Umfang durch Vermittlung der Anstalt auf eigene Kosten erwerben.

(7) Eine mit einer Entkleidung verbundene körperliche Durchsuchung ist nur bei Gefahr im Verzug zulässig.

(8) Zur Vereitelung einer Entweichung und zur Wiederergreifung dürfen Schusswaffen nicht gebraucht werden.

### Abschnitt 22  
Datenschutz

### § 121  
Anwendung des Brandenburgischen Polizei-, Justizvollzugs- und Maßregelvollzugsdatenschutzgesetzes

Das Brandenburgische Polizei-, Justizvollzugs- und Maßregelvollzugsdatenschutzgesetz findet Anwendung, soweit in diesem Gesetz nicht Abweichendes geregelt ist.

### § 122  
Grundsatz, Begriffsbestimmungen

(1) Die Anstalt und die Aufsichtsbehörde dürfen personenbezogene Daten verarbeiten, soweit deren Kenntnis für vollzugliche Zwecke erforderlich ist.

(2) Vollzugliche Zwecke sind die Erreichung des Vollzugsziels, der Schutz der Allgemeinheit vor weiteren Straftaten der Gefangenen, die Aufrechterhaltung der Sicherheit und Ordnung der Anstalt sowie die Sicherung des Vollzugs.

### § 123  
Erhebung von Daten über Gefangene bei Dritten

Daten über Gefangene dürfen ohne deren Kenntnis bei Dritten nur erhoben werden, wenn

1.  eine Rechtsvorschrift dies vorsieht oder zwingend voraussetzt,
2.  die zu erfüllende Aufgabe nach Art oder Zweck eine Erhebung bei anderen Personen oder Stellen erforderlich macht oder
3.  die Erhebung bei den Gefangenen einen unverhältnismäßigen Aufwand erfordern würde

und keine Anhaltspunkte dafür bestehen, dass überwiegende schutzwürdige Interessen der Gefangenen beeinträchtigt werden.

### § 124  
Erhebung von Daten über andere Personen

Daten über andere Personen als die Gefangenen dürfen für vollzugliche Zwecke ohne deren Kenntnis nur erhoben werden, wenn dies unerlässlich ist und die Art der Erhebung schutzwürdige Interessen dieser Personen nicht beeinträchtigt.

### § 125  
Unterrichtungspflichten

Die Betroffenen werden über eine ohne ihre Kenntnis vorgenommene Erhebung ihrer Daten unterrichtet, soweit vollzugliche Zwecke dadurch nicht gefährdet werden.
Sind die Daten bei anderen Personen oder Stellen erhoben worden, kann die Unterrichtung unterbleiben, wenn

1.  die Daten nach einer Rechtsvorschrift oder ihrem Wesen nach, namentlich wegen des überwiegenden berechtigten Interesses Dritter, geheim gehalten werden müssen oder
2.  der Aufwand der Unterrichtung außer Verhältnis zum Schutzzweck steht und keine Anhaltspunkte dafür bestehen, dass überwiegende schutzwürdige Interessen der Betroffenen beeinträchtigt werden.

### § 126  
Besondere Formen der Datenerhebung

(1) Zur Sicherung des Vollzugs und zur Aufrechterhaltung der Sicherheit oder Ordnung der Anstalt, insbesondere zur Identitätsfeststellung, sind mit Kenntnis der Gefangenen folgende erkennungsdienstliche Maßnahmen zulässig:

1.  die Abnahme von Finger- und Handflächenabdrücken,
2.  die Aufnahme von Lichtbildern,
3.  die Feststellung äußerlicher körperlicher Merkmale,
4.  die elektronische Erfassung biometrischer Merkmale und
5.  Messungen.

(2) Aus Gründen der Sicherheit oder Ordnung ist die Beobachtung einzelner Bereiche des Anstaltsgebäudes einschließlich des Gebäudeinneren, des Anstaltsgeländes oder der unmittelbaren Umgebung der Anstalt mit optisch-elektronischen Einrichtungen (Videoüberwachung) zulässig.
Eine Aufzeichnung der Videobilder darf nur im Bereich der unmittelbaren Außensicherung der Anstalt sowie darüber hinaus im Einzelfall auf Anordnung der Anstaltsleiterin oder des Anstaltsleiters in besonders sicherheitsrelevanten Bereichen erfolgen.
Die Videoüberwachung ist durch geeignete Maßnahmen erkennbar zu machen, soweit ihr Zweck dadurch nicht vereitelt wird.
Eine Videoüberwachung von Hafträumen ist ausgeschlossen.
§ 90 Absatz 2 Nummer 2 bleibt unberührt.

(3) Das Betreten des Anstaltsgeländes durch vollzugsfremde Personen kann davon abhängig gemacht werden, dass diese zur Identitätsfeststellung

1.  ihren Vornamen, ihren Namen und ihre Anschrift angeben und durch amtliche Ausweise nachweisen und
2.  die Erfassung biometrischer Merkmale des Gesichts, der Augen, der Hände, der Stimme oder der Unterschrift dulden, soweit dies erforderlich ist, um eine verwechslungsbedingte Entlassung von Gefangenen zu verhindern.

(4) Die Anstaltsleiterin oder der Anstaltsleiter kann das Auslesen von elektronischen Datenspeichern sowie elektronischen Geräten mit Datenspeichern anordnen, die Gefangene ohne Erlaubnis besitzen, wenn konkrete Anhaltspunkte die Annahme rechtfertigen, dass dies für vollzugliche Zwecke erforderlich ist.
Die Gefangenen sind bei der Aufnahme über die Möglichkeit des Auslesens von Datenspeichern zu belehren.

### § 127  
Übermittlung und Nutzung für weitere Zwecke

(1) Für eine Übermittlung oder Nutzung von personenbezogenen Daten stehen die Zwecke des gerichtlichen Rechtsschutzes den vollzuglichen Zwecken des § 122 Absatz 2 gleich.

(2) Die Übermittlung und Nutzung von personenbezogenen Daten ist über Absatz 1 hinaus auch zulässig, soweit dies erforderlich ist

1.  zur Abwehr von sicherheitsgefährdenden oder geheimdienstlichen Tätigkeiten für eine fremde Macht oder von Bestrebungen im Geltungsbereich des Grundgesetzes, die durch Anwendung von Gewalt oder darauf gerichtete Vorbereitungshandlungen
    1.  gegen die freiheitliche demokratische Grundordnung, den Bestand oder die Sicherheit des Bundes oder eines Landes gerichtet sind,
    2.  eine ungesetzliche Beeinträchtigung der Amtsführung der Verfassungsorgane des Bundes oder eines Landes oder ihrer Mitglieder zum Ziel haben oder
    3.  auswärtige Belange der Bundesrepublik Deutschland gefährden,
2.  zur Abwehr erheblicher Nachteile für das Gemeinwohl oder einer Gefahr für die öffentliche Sicherheit,
3.  zur Abwehr einer schwerwiegenden Beeinträchtigung der Rechte einer anderen Person,
4.  zur Verhinderung oder Verfolgung von Straftaten sowie zur Verhinderung oder Verfolgung von Ordnungswidrigkeiten, durch welche die Sicherheit oder Ordnung der Anstalt gefährdet werden oder
5.  für Maßnahmen der Strafvollstreckung oder strafvollstreckungsrechtliche Entscheidungen sowie für die Anordnung von Maßnahmen nach § 119 Absatz 1 der Strafprozessordnung.

Weitergehende Übermittlungspflichten gegenüber der Verfassungsschutzbehörde nach § 14 Absatz 1 des Brandenburgischen Verfassungsschutzgesetzes bleiben unberührt.

### § 128  
Datenübermittlung an öffentliche Stellen

(1) Den zuständigen öffentlichen Stellen dürfen personenbezogene Daten übermittelt werden, soweit dies erforderlich ist für

1.  die Vorbereitung und Durchführung von Maßnahmen der Gerichtshilfe, Jugendgerichtshilfe, Bewährungshilfe, Führungsaufsicht oder forensischen Ambulanzen,
2.  Entscheidungen in Gnadensachen,
3.  gesetzlich angeordnete Statistiken der Rechtspflege,
4.  Maßnahmen der für Sozialleistungen zuständigen Leistungsträger,
5.  die Einleitung von Hilfsmaßnahmen für Angehörige der Gefangenen im Sinne des § 11 Absatz 1 Nummer 1 des Strafgesetzbuches,
6.  dienstliche Maßnahmen der Bundeswehr im Zusammenhang mit der Aufnahme und Entlassung von Soldaten,
7.  ausländerrechtliche Maßnahmen oder
8.  die Durchführung der Besteuerung.

Eine Übermittlung für andere Zwecke ist auch zulässig, soweit eine andere gesetzliche Bestimmung dies vorsieht und sich dabei ausdrücklich auf Daten über Gefangene bezieht.

(2) Absatz 1 gilt entsprechend, wenn sich die öffentlichen Stellen zur Erfüllung ihrer Aufgaben nichtöffentlicher Stellen bedienen und deren Mitwirkung ohne Übermittlung der Daten unmöglich oder wesentlich erschwert würde.

### § 129   
Verarbeitung besonders erhobener Daten

(1) Bei der Überwachung der Besuche, der Telefongespräche, anderer Formen der Telekommunikation oder des Schriftwechsels sowie bei der Überprüfung des Inhalts von Paketen bekannt gewordene personenbezogene Daten dürfen für die in § 122 Absatz 2 und § 127 Absatz 1 genannten Zwecke verarbeitet werden.

(2) Die aufgrund erkennungsdienstlicher Maßnahmen nach § 126 Absatz 1 gewonnenen Daten und Unterlagen werden zu den Gefangenenpersonalakten genommen oder in personenbezogenen Dateien gespeichert.
Sie dürfen nur für die in § 126 Absatz 1, § 127 Absatz 2 Satz 1 Nummer 4 genannten Zwecke verarbeitet oder den Vollstreckungs- und Strafverfolgungsbehörden zum Zwecke der Fahndung und Festnahme der entwichenen oder sich sonst ohne Erlaubnis außerhalb der Anstalt aufhaltenden Gefangenen übermittelt werden.

(3) Die zur Identifikation von vollzugsfremden Personen nach § 126 Absatz 3 erhobenen Daten dürfen ausschließlich verarbeitet werden

1.  zum Zweck des Abgleichs beim Verlassen der Vollzugsanstalt oder
2.  zur Verfolgung von während des Aufenthalts in der Anstalt begangenen Straftaten; in diesem Fall können die Daten auch an Strafverfolgungsbehörden ausschließlich zum Zwecke der Verfolgung dieser Straftaten übermittelt werden.

(4) Die beim Auslesen von Datenspeichern nach § 126 Absatz 4 erhobenen Daten dürfen nur verarbeitet werden, soweit dies zu den dort genannten Zwecken erforderlich ist.
Das gilt nicht, soweit sie zum Kernbereich der privaten Lebensgestaltung der Gefangenen oder Dritter gehören.

(5) Nach § 124 erhobene Daten über Personen, die nicht Gefangene sind, dürfen nur zur Erfüllung des Erhebungszwecks oder für die in § 127 Absatz 2 Satz 1 Nummer 1 bis 3 geregelten Zwecke oder zur Verhinderung oder Verfolgung von Straftaten von erheblicher Bedeutung verarbeitet werden.

### § 130   
Mitteilung über Haftverhältnisse

(1) Die Anstalt oder die Aufsichtsbehörde darf öffentlichen oder nichtöffentlichen Stellen auf schriftlichen Antrag mitteilen, ob sich eine Person in Haft befindet sowie ob und wann die Entlassung aus dem Vollzug einer Freiheits- oder Jugendstrafe voraussichtlich innerhalb eines Jahres bevorsteht, soweit

1.  die Mitteilung zur Erfüllung der in der Zuständigkeit der öffentlichen Stelle liegenden Aufgaben erforderlich ist oder
2.  von nichtöffentlichen Stellen
    1.  ein berechtigtes Interesse an dieser Mitteilung glaubhaft dargelegt wird und
    2.  die Gefangenen kein schutzwürdiges Interesse an dem Ausschluss der Übermittlung haben.

(2) Die Mitteilung ist in der Gefangenenpersonalakte zu dokumentieren.

(3) Den Verletzten einer Straftat sowie deren Rechtsnachfolgerinnen und Rechtsnachfolgern können darüber hinaus auf schriftlichen Antrag Auskünfte über die Entlassungsadresse oder die Vermögensverhältnisse von Straf- und Jugendstrafgefangenen erteilt werden, wenn die Erteilung zur Feststellung oder Durchsetzung von Rechtsansprüchen im Zusammenhang mit der Straftat erforderlich ist.

(4) Die Gefangenen werden vor der Mitteilung gehört, es sei denn, es ist zu besorgen, dass dadurch die Verfolgung des Interesses der Antragstellerinnen und Antragsteller vereitelt oder wesentlich erschwert werden würde, und eine Abwägung ergibt, dass dieses Interesse das Interesse der Gefangenen an ihrer vorherigen Anhörung überwiegt.
Ist die Anhörung unterblieben, werden die betroffenen Gefangenen über die Mitteilung nachträglich unterrichtet.

(5) Bei einer nicht nur vorläufigen Einstellung des Verfahrens, einer unanfechtbaren Ablehnung der Eröffnung des Hauptverfahrens oder einem rechtskräftigen Freispruch sind auf Antrag der betroffenen Untersuchungsgefangenen die Stellen, die eine Mitteilung nach Absatz 1 Nummer 1 erhalten haben, über den Verfahrensausgang in Kenntnis zu setzen.
Die betroffenen Untersuchungsgefangenen sind bei der Anhörung oder nachträglichen Unterrichtung nach Absatz 4 auf ihr Antragsrecht hinzuweisen.

### § 131   
Überlassung von Akten

(1) Akten dürfen nur

1.  anderen Anstalten und Aufsichtsbehörden,
2.  der Gerichtshilfe, der Jugendgerichtshilfe, der Bewährungshilfe, den Führungsaufsichtsstellen und den forensischen Ambulanzen,
3.  den für strafvollzugs-, strafvollstreckungs- und strafrechtliche Entscheidungen zuständigen Gerichten und
4.  den Strafvollstreckungs- und Strafverfolgungsbehörden

überlassen oder im Falle elektronischer Aktenführung in Form von Duplikaten übermittelt werden.

(2) Die Überlassung an andere öffentliche Stellen und nichtöffentliche Stellen, denen Aufgaben nach Absatz 1 Nummer 2 gemäß § 128 Absatz 2 übertragen sind, ist zulässig, soweit die Erteilung einer Auskunft einen unvertretbaren Aufwand erfordert oder nach Darlegung der Akteneinsicht begehrenden Stellen für die Erfüllung der Aufgabe nicht ausreicht.
Entsprechendes gilt für die Überlassung von Akten an die von einer Anstalt oder Aufsichtsbehörde, einer Strafvollstreckungsbehörde oder einem Gericht mit Gutachten beauftragten Stellen.

### § 132   
Kenntlichmachung in der Anstalt, Lichtbildausweise

(1) Mit Ausnahme des religiösen oder weltanschaulichen Bekenntnisses, personenbezogener Daten über die ethnische Herkunft, sexuelle Identität, politische Meinungen, die Gewerkschaftszugehörigkeit und der personenbezogenen Daten von Gefangenen, die anlässlich ärztlicher Untersuchungen erhoben worden sind, dürfen Daten von Gefangenen in der Anstalt allgemein kenntlich gemacht werden, soweit dies für ein geordnetes Zusammenleben erforderlich ist.

(2) Die Anstalt kann die Gefangenen verpflichten, einen Lichtbildausweis mit sich zu führen, wenn dies aus Gründen der Sicherheit oder Ordnung der Anstalt erforderlich ist.
Dieser ist bei der Verlegung in eine andere Anstalt oder bei der Entlassung einzuziehen oder zu vernichten.

### § 133   
Offenbarungspflichten und -befugnisse der Berufsgeheimnisträgerinnen und Berufsgeheimnisträger

(1) Soweit in den folgenden Absätzen nicht Abweichendes geregelt ist, unterliegen

1.  Ärztinnen und Ärzte, Zahnärztinnen und Zahnärzte oder Angehörige eines anderen Heilberufs, der für die Berufsausübung oder die Führung der Berufsbezeichnung eine staatlich geregelte Ausbildung erfordert,
2.  Berufspsychologinnen und Berufspsychologen mit staatlich anerkannter wissenschaftlicher Abschlussprüfung und
3.  staatlich anerkannte Sozialarbeiterinnen und Sozialarbeiter oder staatlich anerkannte Sozialpädagoginnen und Sozialpädagogen

hinsichtlich der ihnen als Berufsgeheimnisträgerinnen oder Berufsgeheimnisträger von Gefangenen anvertrauten oder sonst über Gefangene bekannt gewordenen Geheimnisse auch gegenüber der Anstalt und der Aufsichtsbehörde der Schweigepflicht.

(2) Die in Absatz 1 genannten Personen haben sich gegenüber der Anstaltsleiterin oder dem Anstaltsleiter zu offenbaren, soweit dies für die Aufgabenerfüllung der Anstalt oder der Aufsichtsbehörde oder zur Abwehr von erheblichen Gefahren für Leib oder Leben von Gefangenen oder Dritten erforderlich ist.

(3) Ärztinnen und Ärzte sind gegenüber der Anstaltsleiterin oder dem Anstaltsleiter zur Offenbarung im Rahmen der allgemeinen Gesundheitsfürsorge bekannt gewordener Geheimnisse befugt, soweit dies für die Aufgabenerfüllung der Anstalt oder der Aufsichtsbehörde unerlässlich oder zur Abwehr von erheblichen Gefahren für Leib oder Leben von Gefangenen oder Dritten erforderlich ist.
Sonstige Offenbarungsbefugnisse und -pflichten bleiben unberührt.

(4) Die Gefangenen sind vor der Erhebung über die nach Absatz 2 und 3 bestehenden Offenbarungspflichten zu unterrichten.

(5) Die nach den Absätzen 2 und 3 offenbarten Daten dürfen nur für den Zweck, für den sie offenbart wurden oder für den eine Offenbarung zulässig gewesen wäre, und nur unter denselben Voraussetzungen verarbeitet oder genutzt werden, unter denen eine in Absatz 1 genannte Person selbst hierzu befugt wäre.
Die Anstaltsleiterin oder der Anstaltsleiter kann unter diesen Voraussetzungen die unmittelbare Offenbarung gegenüber bestimmten Bediensteten allgemein zulassen.

(6) Sofern Ärztinnen und Ärzte oder Psychologinnen und Psychologen außerhalb des Vollzugs mit der Untersuchung, Behandlung oder Betreuung von Gefangenen beauftragt werden, gilt Absatz 1 bis 3 mit der Maßgabe entsprechend, dass die beauftragten Personen auch zur Unterrichtung der oder des in der Anstalt tätigen Ärztin oder Arztes oder der oder des in der Anstalt mit der Behandlung oder Betreuung der Gefangenen betrauten Psychologin oder Psychologen befugt sind.

### § 134   
_(aufgehoben)_

### § 135   
Auskunft an die Betroffenen, Akteneinsicht

(1) Den Gefangenen wird Akteneinsicht gewährt, wenn eine Auskunft für die Wahrnehmung ihrer rechtlichen Interessen nicht ausreicht und sie hierfür auf die Einsichtnahme angewiesen sind.

(2) Die Auskunftserteilung und die Gewährung von Akteneinsicht unterbleiben, soweit die Auskunft oder die Einsichtnahme die ordnungsgemäße Erfüllung der Aufgaben der datenverarbeitenden Stelle oder die Erreichung des Vollzugsziels gefährden würde.

### § 136   
Auskunft und Akteneinsicht zur Wahrnehmung der Aufgaben des Europäischen Ausschusses zur Verhütung von Folter und unmenschlicher oder erniedrigender Behandlung oder Strafe

Den Mitgliedern einer Delegation des Europäischen Ausschusses zur Verhütung von Folter und unmenschlicher oder erniedrigender Behandlung oder Strafe wird während des Besuchs in der Anstalt Einsicht in die Gefangenenpersonalakten, Gesundheitsakten und Krankenblätter gewährt oder Auskunft aus diesen Akten erteilt, soweit dies zur Wahrnehmung der Aufgaben des Ausschusses unerlässlich ist.

### § 137   
Löschung

Die in Dateien mit Ausnahme der in Gefangenenpersonalakten, Gesundheitsakten, Therapieakten, psychologischen und pädagogischen Testunterlagen und Krankenblättern sowie Gefangenenbüchern gespeicherten personenbezogenen Daten sind spätestens zwei Jahre nach der Entlassung oder der Verlegung der Gefangenen in eine andere Anstalt zu löschen.
Hiervon können bis zum Ablauf der Aufbewahrungsfrist nach § 140 die Angaben über Familienname, Vorname, Geburtsname, Geburtstag, Geburtsort, Eintritts- und Austrittsdatum der Gefangenen ausgenommen werden, soweit dies für das Auffinden der Gefangenenpersonalakte erforderlich ist.

### § 138   
Löschung besonders erhobener Daten

(1) Erkennungsdienstliche Unterlagen mit Ausnahme von Lichtbildern und der Beschreibung von körperlichen Merkmalen der Straf- und Jugendstrafgefangenen, die nach § 126 Absatz 1 erkennungsdienstlich behandelt worden sind, sind nach ihrer Entlassung aus dem Vollzug unverzüglich zu löschen, sobald die Vollstreckung der richterlichen Entscheidung, die dem Vollzug zugrunde gelegen hat, abgeschlossen ist.
Im Vollzug der Untersuchungshaft gilt dies bei einer nicht nur vorläufigen Einstellung des Verfahrens, einer unanfechtbaren Ablehnung der Eröffnung des Hauptverfahrens oder einem rechtskräftigen Freispruch.

(2) Mittels optisch-elektronischer Einrichtungen nach § 126 Absatz 2 erhobene Daten sind spätestens nach 72 Stunden zu löschen, soweit nicht die weitere Aufbewahrung im Einzelfall zu Beweiszwecken unerlässlich ist.

(3) Nach § 126 Absatz 3 Nummer 2 erhobene Daten sind unverzüglich zu löschen, nachdem die Personen die Anstalt verlassen haben.

(4) Nach § 126 Absatz 4 erhobene Daten sind unverzüglich zu löschen, soweit eine Verarbeitung nach § 129 Absatz 4 unzulässig ist.
Die Daten sind spätestens 72 Stunden nach dem Ende des Auslesens zu löschen, soweit nicht die weitere Aufbewahrung im Einzelfall zu Beweiszwecken unerlässlich ist.

### § 139   
Sperrung und Verwendungsbeschränkungen

(1) Personenbezogene Daten in den in § 137 Satz 1 genannten Dateien sind nach Ablauf von zwei Jahren seit der Entlassung oder der Verlegung der Gefangenen in eine andere Anstalt zu kennzeichnen, um ihre weitere Verarbeitung oder Nutzung einzuschränken (Sperrung).

(2) Die nach Absatz 1 gesperrten Daten dürfen nur übermittelt oder genutzt werden, soweit dies unerlässlich ist:

1.  zur Verfolgung von Straftaten,
2.  für die Durchführung wissenschaftlicher Forschungsvorhaben gemäß § 106,
3.  zur Behebung einer bestehenden Beweisnot oder
4.  zur Feststellung, Durchsetzung oder Abwehr von Rechtsansprüchen im Zusammenhang mit dem Vollzug einer Jugend- oder Freiheitsstrafe oder einer Untersuchungshaft.

(3) Die Sperrung nach Absatz 1 endet, wenn die Gefangenen erneut zum Vollzug einer Jugend- oder Freiheitsstrafe oder einer Untersuchungshaft aufgenommen werden oder die Betroffenen eingewilligt haben.

### § 140   
Aufbewahrungsfristen, Fristberechnung

(1) Bei der Aufbewahrung der nach § 139 gesperrten Daten darf eine Frist von 30 Jahren nicht überschritten werden.

(2) Die Aufbewahrungsfrist beginnt mit dem auf das Jahr der aktenmäßigen Weglegung folgenden Kalenderjahr.

(3) Die Bestimmungen des Brandenburgischen Archivgesetzes bleiben unberührt.

### Abschnitt 23   
Schlussbestimmungen

### § 141   
Einschränkung von Grundrechten

Durch dieses Gesetz werden die Rechte auf körperliche Unversehrtheit und Freiheit der Person (Artikel 2 Absatz 2 des Grundgesetzes, Artikel 8 Absatz 1 und Artikel 9 Absatz 1 der Verfassung des Landes Brandenburg), auf Unverletzlichkeit des Brief-, Post- und Fernmeldegeheimnisses (Artikel 10 Absatz 1 des Grundgesetzes, Artikel 16 Absatz 1 der Verfassung des Landes Brandenburg), auf Meinungsfreiheit (Artikel 19 Absatz 1 der Verfassung des Landes Brandenburg) und auf Datenschutz (Artikel 11 der Verfassung des Landes Brandenburg) eingeschränkt.

### § 142   
Übergangsregelung

Bis zum 31.
Dezember 2016 ist § 25 Absatz 2 in folgender Fassung anzuwenden: „Strafgefangene, die wegen einer Straftat nach den §§ 174 bis 180 oder 182 des Strafgesetzbuches zu zeitiger Freiheitsstrafe von mehr als zwei Jahren verurteilt worden sind oder bei denen Sicherungsverwahrung angeordnet oder vorbehalten ist, sowie Jugendstrafgefangene, bei denen Sicherungsverwahrung vorbehalten ist, sind in einer sozialtherapeutischen Abteilung unterzubringen, wenn ihre Teilnahme an den dortigen Behandlungsprogrammen angezeigt ist.“

### § 143   
Inkrafttreten, Außerkrafttreten

(1) Dieses Gesetz tritt vorbehaltlich des Absatzes 2 am 1.
Juni 2013 in Kraft.
Gleichzeitig treten das Brandenburgische Untersuchungshaftvollzugsgesetz vom 8.
Juli 2009 (GVBl.
I S. 271) und das Brandenburgische Jugendstrafvollzugsgesetz vom 18.
Dezember 2007 (GVBl.
I S.
348), das durch Artikel 15 des Gesetzes vom 3.
April 2009 (GVBl.
I S.
26, 59) geändert worden ist, außer Kraft.

(2) § 66 Absatz 3 Satz 3 tritt am Tag nach der Verkündung dieses Gesetzes in Kraft.

Potsdam, den 24.
April 2013

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch