## Gesetz zu dem Vierzehnten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Vierzehnter Rundfunkänderungsstaatsvertrag)

Der Landtag hat das folgende Gesetz beschlossen:

### § 1

Dem am 10.
Juni 2010 vom Land Brandenburg unterzeichneten Vierzehnten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Vierzehnter Rundfunkänderungsstaatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem Artikel 4 Absatz 2 Satz 1 am 1.
Januar 2011 in Kraft.
Sollte der Staatsvertrag nach seinem Artikel 4 Absatz 2 Satz 2 gegenstandslos werden, ist dies unverzüglich im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 20.
Dezember 2010

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

* * *

[GVBl.
I 2011 Nr.
3](/br2/sixcms/media.php/76/GVBl_I_03_2011.pdf) - Bekanntmachung der Gegenstandslosigkeit des Vierzehnten Rundfunkänderungsstaatsvertrages