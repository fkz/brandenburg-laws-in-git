## Gesetz zur Errichtung des Landesamtes für Umwelt und des Landesamtes für Arbeitsschutz, Verbraucherschutz und Gesundheit sowie zur Auflösung des Landesamtes für Umwelt, Gesundheit und Verbraucherschutz und des Landesamtes für Arbeitsschutz

#### § 1 Errichtung

Als Landesoberbehörden nach § 7 Absatz 1 Satz 1 des Landesorganisationsgesetzes werden das Landesamt für Umwelt im Geschäftsbereich des Ministeriums für Ländliche Entwicklung, Umwelt und Landwirtschaft sowie das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit im Geschäftsbereich des Ministeriums für Arbeit, Soziales, Gesundheit, Frauen und Familie errichtet.

#### § 2 Auflösung

Das Landesamt für Umwelt, Gesundheit und Verbraucherschutz sowie das Landesamt für Arbeitsschutz werden aufgelöst.

#### § 3 Aufgaben

(1) Die Aufgaben und Befugnisse des Landesamtes für Arbeitsschutz gehen auf das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit über.

(2) Die Aufgaben und Befugnisse des Landesamtes für Umwelt, Gesundheit und Verbraucherschutz in den Bereichen Lebensmittel- und Futtermittelüberwachung, Tierschutz, Tierarzneimittelüberwachung, Tierseuchenverhütung und -bekämpfung, Gentechnik, Chemikaliensicherheit, Strahlenschutz, Strahlenschutzvorsorge, Umsetzung der Verordnung über elektromagnetische Felder (26.
BImSchV), Trink- und Badebeckenwasserhygiene und Badegewässerqualität gehen auf das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit über.

(3) Die Aufgaben und Befugnisse des Landesamtes für Umwelt, Gesundheit und Verbraucherschutz in den Bereichen Gesundheits- und Krankenhauswesen, Prävention, Gesundheitsförderung, Rehabilitation, Kur- und Bäderwesen, Öffentlicher Gesundheitsdienst, Infektionsschutz, Umweltbezogener Gesundheitsschutz, Gesundheitsberichterstattung, Apotheken, Arzneimittel, Medizinprodukte, Heilberufe, Fachberufe des Gesundheitswesens (mit Ausnahme der Berufe in der Altenpflege) sowie Zivil- und Katastrophenschutz im Gesundheitswesen gehen auf das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit über.

(4) Die Aufgaben und Befugnisse des Landesamtes für Umwelt, Gesundheit und Verbraucherschutz in dem Bereich Maßregelvollzug und der öffentlich-rechtlichen Unterbringung gehen auf das Landesamt für Soziales und Versorgung über.

(5) Die sonstigen Aufgaben und Befugnisse des Landesamtes für Umwelt, Gesundheit und Verbraucherschutz gehen auf das Landesamt für Umwelt über.

#### § 4 Aufsicht

(1) Das für Arbeitsschutz zuständige Ministerium führt die Fachaufsicht über das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit in Bezug auf die Wahrnehmung der Aufgaben nach § 3 Absatz 1.

(2) Das für Verbraucherschutz zuständige Ministerium führt die Fachaufsicht über das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit in Bezug auf die Wahrnehmung der Aufgaben nach § 3 Absatz 2.

(3) Das für Gesundheit zuständige Ministerium führt die Fachaufsicht über das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit in Bezug auf die Wahrnehmung der Aufgaben nach § 3 Absatz 3.

(4) Die Dienstaufsicht über das Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit führt das für Arbeitsschutz und Gesundheit zuständige Ministerium.
Soweit sich die Dienstaufsicht auf die Wahrnehmung der Aufgaben nach § 3 Absatz 2 erstreckt, führt das für Arbeitsschutz und Gesundheit zuständige Ministerium die Dienstaufsicht im Einvernehmen mit dem für Verbraucherschutz zuständigen Ministerium, mit Ausnahme der Dienstaufsicht in Bezug auf die personelle, materielle und finanzielle Ausstattung, die das für Verbraucherschutz zuständige Ministerium führt.

(5) Das für Umwelt zuständige Ministerium führt die Dienst- und Fachaufsicht über das Landesamt für Umwelt.

#### § 5 Personal

(1) Die Beamtinnen und Beamten sowie die Beschäftigten des Landesamtes für Arbeitsschutz sowie des Landesamtes für Umwelt, Gesundheit und Verbraucherschutz, die in den in § 3 Absatz 2 und 3 genannten Bereichen tätig sind, werden dem Landesamt für Arbeitsschutz, Verbraucherschutz und Gesundheit zugeordnet.
Das für Arbeitsschutz und Gesundheit zuständige Ministerium übt die Befugnisse als oberste Dienstbehörde der Beamtinnen und Beamten, die in den in § 3 Absatz 2 genannten Bereichen tätig sind, im Einvernehmen mit dem für Verbraucherschutz zuständigen Ministerium aus.

(2) Die Beamtinnen und Beamten sowie die Beschäftigten des Landesamtes für Umwelt, Gesundheit und Verbraucherschutz, die in dem in § 3 Absatz 4 genannten Bereich tätig sind, werden dem Landesamt für Soziales und Versorgung zugeordnet.

(3) Die Beamtinnen und Beamten sowie die Beschäftigten des Landesamtes für Umwelt, Gesundheit und Verbraucherschutz, die nicht von den Regelungen in den Absätzen 1 und 2 betroffen sind, werden dem Landesamt für Umwelt zugeordnet.

#### § 6 Verwaltungsvereinbarung

Nähere Regelungen zum Umfang und zur Herbeiführung des Einvernehmens im Sinne des § 4 Absatz 4 Satz 2 und des § 5 Absatz 1 Satz 2 treffen das für Arbeitsschutz und Gesundheit zuständige Ministerium und das für Verbraucherschutz zuständige Ministerium in einer Verwaltungsvereinbarung.