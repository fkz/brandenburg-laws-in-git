## Gesetz zu dem Ersten Staatsvertrag zur Änderung des Staatsvertrages über die Errichtung einer gemeinsamen Rundfunkanstalt der Länder Berlin und Brandenburg

### § 1

Dem am 30.
August 2013 vom Land Brandenburg unterzeichneten Ersten Staatsvertrag zur Änderung des Staatsvertrages über die Errichtung einer gemeinsamen Rundfunkanstalt der Länder Berlin und Brandenburg (Erster RBB-Änderungsstaatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2

Der Tag, an dem der Erste RBB-Änderungsstaatsvertrag nach seinem Artikel 2 Absatz 1 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

[zum Staatsvertrag](/vertraege/rbb_stv_2014)