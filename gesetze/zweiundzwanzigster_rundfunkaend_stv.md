## Gesetz zum Zweiundzwanzigsten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Zweiundzwanzigster Rundfunkänderungsstaatsvertrag)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem am 26.
Oktober 2018 vom Land Brandenburg unterzeichneten Zweiundzwanzigsten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Zweiundzwanzigster Rundfunkänderungsstaatsvertrag) wird zugestimmt.
Der Vertrag wird nachstehend veröffentlicht.

#### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem Artikel 2 Absatz 2 Satz 1 am 1.
Mai 2019 in Kraft.
Sollte der Staatsvertrag nach seinem Artikel 2 Absatz 2 Satz 2 gegenstandslos werden, ist dies unverzüglich im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 1.
April 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag](/vertraege/rstv) - Rundfunkstaatsvertrag

* * *

### Anlagen

1

[Zweiundzwanzigster Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Zweiundzwanzigster Rundfunkänderungsstaatsvertrag)](/br2/sixcms/media.php/68/GVBl_I_07_2019-Anlage.pdf "Zweiundzwanzigster Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Zweiundzwanzigster Rundfunkänderungsstaatsvertrag)") 254.0 KB