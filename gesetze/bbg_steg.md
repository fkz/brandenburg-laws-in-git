## Gesetz zur Erprobung der Abweichung von landesrechtlichen Standards in kommunalen Körperschaften des Landes Brandenburg (Brandenburgisches Standarderprobungsgesetz - BbgStEG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Ziele

(1) Ziel dieses Gesetzes ist es, neue Maßnahmen zum Bürokratieabbau zu erproben, auszuwerten und erfolgreiche Modelle landesweit zur Anwendung zu empfehlen.
Zu diesem Zweck werden für einen begrenzten Zeitraum Abweichungen von Rechtsvorschriften zugelassen, um den kommunalen Körperschaften die Erprobung neuer Lösungen bei der kommunalen Aufgabenerledigung zu ermöglichen und zu testen, ob damit unternehmerisches Handeln und Existenzgründungen erleichtert und somit die wirtschaftliche Entwicklung gefördert sowie Verwaltungsverfahren beschleunigt, vereinfacht und kostengünstiger für die Unternehmen, die Bürgerinnen und Bürger und die Verwaltungen gestaltet werden können.

(2) Ein weiteres Ziel dieses Gesetzes ist es, die Handlungsspielräume auf kommunaler Ebene zu erhöhen, um den Herausforderungen des demografischen Wandels vor Ort mit flexiblen und örtlich angepassten Lösungen begegnen zu können.

#### § 2 Antragsrecht der kommunalen Körperschaften

(1) Zur Erprobung neuer Formen der Aufgabenerledigung oder des Aufgabenverzichts können Gemeinden, Verbandsgemeinden, Landkreise, Ämter sowie Zweckverbände des Landes Brandenburg (kommunale Körperschaften) auf Antrag im Einzelfall von der Anwendung landesrechtlicher Standards befreit werden, soweit Bundesrecht und Recht der Europäischen Union nicht entgegenstehen und die Rechte Dritter nicht verletzt werden.

(2) Standards im Sinne dieses Gesetzes sind Vorgaben in landesrechtlichen Vorschriften (Gesetze, Rechtsverordnungen und Verwaltungsvorschriften des Landes), die für die Aufgabenerfüllung der kommunalen Körperschaften erlassen wurden.

#### § 3 Antrags- und Genehmigungsverfahren

(1) Der Antrag ist an die jeweils fachlich zuständige oberste Landesbehörde (Genehmigungsbehörde) zu richten.
Die landesrechtlichen Standards, von denen abgewichen werden soll, und die Dauer der Erprobung sind im Einzelnen anzugeben.
Die angestrebte Öffnung im Sinne von § 1, die Vorgehensweise und die Wirkung, die dadurch erzielt werden soll, müssen beschrieben werden.

(2) Über den Antrag soll innerhalb von drei Monaten nach Eingang des Antrages von der Genehmigungsbehörde im Einvernehmen mit dem für Inneres zuständigen Ministerium entschieden werden.
Dem Antrag soll stattgegeben werden, es sei denn, dass eine Gefahr für Leib oder Leben einer Person entstehen würde oder überwiegende Belange des Allgemeinwohls entgegenstehen.
Die Genehmigungsbehörde hat den Antragsteller im Antragsverfahren anzuhören.
Stehen der Genehmigung Hindernisse entgegen, hat die Genehmigungsbehörde auf mögliche Veränderungen des Antrages hinzuwirken, um eine Genehmigung zu ermöglichen.

(3) Die Genehmigung ist für höchstens vier Jahre zu erteilen.
Wird eine Genehmigung erteilt, gibt die Genehmigungsbehörde dies unter Benennung der Erprobungskörperschaften, der Bezeichnung der Normen, die Gegenstände der Befreiung sind, und des Zeitraums der Erprobung im Amtsblatt für Brandenburg bekannt.
Die Erprobungskörperschaft hat in der für die öffentliche Bekanntmachung ihrer Satzungen vorgeschriebenen Form auf die Bekanntmachung nach Satz 2 hinzuweisen.

(4) Während des Zeitraums der Erprobung ist die jeweilige Erprobungskörperschaft von der Genehmigungsbehörde angemessen zu begleiten und zu unterstützen.

#### § 4 Antragstellung der kommunalen Spitzenverbände für ihre Mitglieder

(1) Der Städte- und Gemeindebund Brandenburg kann stellvertretend für mehrere Gemeinden, Ämter und Verbandsgemeinden Anträge gemäß § 2 Absatz 1 stellen.
Für das Verfahren gilt § 3 entsprechend.

(2) Der Landkreistag Brandenburg kann stellvertretend für mehrere Landkreise Anträge gemäß § 2 Absatz 1 stellen.
Für das Verfahren gilt § 3 entsprechend.

#### § 5 Berichtspflicht, Übertragbarkeit

(1) Die Erprobungskörperschaft berichtet der Genehmigungsbehörde über die Ergebnisse der Erprobung.
Die Genehmigungsbehörde wertet die Ergebnisse der Erprobung mit der Erprobungskörperschaft aus.
Dies gilt im Falle des § 4 entsprechend.

(2) Die Genehmigungsbehörde prüft unter Beteiligung des für Inneres zuständigen Ministeriums die generelle Übertragbarkeit des Ergebnisses der Erprobungen auf die anderen kommunalen Körperschaften im Land Brandenburg.

(3) Die Landesregierung berichtet dem Landtag alle zwei Jahre nach Inkrafttreten dieses Gesetzes über den Stand und die Auswirkungen des Verfahrens unter Berücksichtigung der Zielstellung gemäß § 1.

#### § 6 Inkrafttreten, Außerkrafttreten

Dieses Gesetz tritt am 2.
September 2021 in Kraft.
Es tritt am 1.
September 2026 außer Kraft.

Potsdam, den 31.
August 2021

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke