## Gesetz über die Einsetzung und das Verfahren von Untersuchungsausschüssen des Landtages Brandenburg (Untersuchungsausschussgesetz - UAG)

**Inhaltsübersicht**

### Abschnitt 1  
Aufgaben, Einsetzung und Zusammensetzung

[§ 1 Aufgabe](#1)

[§ 2 Einsetzung](#2)

[§ 3 Untersuchungsauftrag](#3)

[§ 4 Zusammensetzung, Vorsitz](#4)

[§ 5 Vorsitzende](#5)

[§ 6 Stellvertretende Vorsitzende](#6)

[§ 7 Stellvertretende Mitglieder](#7)

[§ 8 Ausscheiden von Mitgliedern](#8)

### Abschnitt 2  
Verfahren

[§ 9 Einberufung, Beschlussfassung, Anwendung der Geschäftsordnung des Landtages Brandenburg](#9)

[§ 10 Unterausschuss](#10)

[§ 11 Öffentlichkeit der Sitzungen, Geheimschutz](#11)

[§ 12 Mitteilungen über Sitzungen und Unterlagen](#12)

[§ 13 Ordnungsgewalt](#13)

[§ 14 Protokollierung](#14)

[§ 15 Beweisaufnahme](#15)

[§ 16 Aktenvorlage, Aussagegenehmigung, Zutrittsrecht](#16)

[§ 17 Zeuginnen und Zeugen](#17)

[§ 18 Zwangsmittel](#18)

[§ 19 Aussageverweigerungsrecht](#19)

[§ 20 Belehrung](#20)

[§ 21 Vernehmung, Fragerecht](#21)

[§ 22 Sachverständige](#22)

[§ 23 Herausgabepflicht, Beschlagnahme und Durchsuchung](#23)

[§ 24 Rechts- und Amtshilfe](#24)

[§ 25 Verlesen von Protokollen und Schriftstücken](#25)

### Abschnitt 3  
Beendigung des Verfahrens

[§ 26 Beendigung](#26)

[§ 27 Aussetzung und Auflösung](#27)

[§ 28 Berichte](#28)

[§ 29 Kosten und Auslagen](#29)

[§ 30 Gerichtliches Verfahren](#30)

[§ 31 Einschränkung von Grundrechten](#31)

### Abschnitt 1  
Aufgaben, Einsetzung und Zusammensetzung

#### § 1 Aufgabe

Im Rahmen der verfassungsmäßigen Zuständigkeit des Landtages hat ein Untersuchungsausschuss die Aufgabe, Sachverhalte, deren Aufklärung im öffentlichen Interesse liegt, zu untersuchen und dem Landtag darüber Bericht zu erstatten.

#### § 2 Einsetzung

(1) Ein Untersuchungsausschuss wird für einen bestimmten Untersuchungsauftrag durch Beschluss des Landtages eingesetzt.

(2) Der Landtag hat das Recht und auf Antrag von einem Fünftel seiner Mitglieder die Pflicht, einen Untersuchungsausschuss einzusetzen.
Der Antrag muss von der erforderlichen Zahl der Mitglieder des Landtages gezeichnet werden.
Zwischen Einbringung und Beschluss müssen 48 Stunden liegen.
Nach Ablauf dieser Frist ist der Antrag auf Verlangen der antragstellenden Mitglieder des Landtages unverzüglich zu behandeln.

(3) Ein Antrag auf Einsetzung eines Untersuchungsausschusses ist vom Landtag zurückzuweisen, soweit er Verfassungsrecht verletzt.

(4) Im Übrigen gelten für Anträge auf Einsetzung eines Untersuchungsausschusses die Bestimmungen der Geschäftsordnung des Landtages Brandenburg.

(5) Der Landtag beschließt über die angemessene sächliche und personelle Ausstattung des Untersuchungsausschusses.
In dem Beschluss kann bestimmt werden, dass die Fraktionen zusätzliche zweckgebundene finanzielle Mittel erhalten.
Je Fraktion und Haushaltsjahr sollen die Mittel den zur Finanzierung einer Vollzeitstelle der Entgeltgruppe 14 des Tarifvertrages für den öffentlichen Dienst der Länder erforderlichen Betrag nicht übersteigen.
Für die Gruppen gelten die Sätze 2 und 3 entsprechend, wenn sie ein Ausschussmitglied stellen.

#### § 3 Untersuchungsauftrag

(1) Im Antrag und im Einsetzungsbeschluss muss der Untersuchungsauftrag hinreichend bestimmt sein.

(2) Der Untersuchungsausschuss ist an den ihm erteilten Auftrag gebunden und zu seiner Änderung nicht berechtigt.

(3) Der Untersuchungsauftrag darf gegen den Willen der antragstellenden Mitglieder des Landtages nicht verändert werden.

#### § 4 Zusammensetzung, Vorsitz

(1) Der Untersuchungsausschuss setzt sich aus ordentlichen Mitgliedern und der gleichen Zahl von stellvertretenden Mitgliedern zusammen, die auf Vorschlag der Fraktionen und Gruppen vom Landtag gewählt werden.
Dem Untersuchungsausschuss können nur Mitglieder des Landtages angehören.
Die Zahl der Mitglieder des Untersuchungsausschusses bestimmt der Landtag im Einsetzungsbeschluss, wobei zu gewährleisten ist, dass die Stärkeverhältnisse der Fraktionen abgebildet werden können.
Die oder der Vorsitzende wird auf diese Zahl nicht angerechnet.

(2) Im Untersuchungsausschuss sind die Fraktionen mit mindestens je einem Mitglied vertreten.
Im Übrigen werden die Sitze unter Berücksichtigung des Stärkeverhältnisses der Fraktionen und Gruppen verteilt.

(3) Der Landtag wählt die Vorsitzenden der Untersuchungsausschüsse und die stellvertretenden Vorsitzenden aus seiner Mitte.
Die Vorsitzenden der in einer Wahlperiode eingesetzten Untersuchungsausschüsse werden von den Fraktionen in der Reihenfolge ihrer Stärke vorgeschlagen.
Die stellvertretenden Vorsitzenden der Untersuchungsausschüsse, in denen ein Mitglied einer Regierungsfraktion den Vorsitz innehat, werden von den Oppositionsfraktionen in der Reihenfolge ihrer Stärke vorgeschlagen.
Die stellvertretenden Vorsitzenden der Untersuchungsausschüsse, in denen ein Mitglied einer Oppositionsfraktion den Vorsitz hat, werden von den Regierungsfraktionen in der Reihenfolge ihrer Stärke vorgeschlagen.

(4) Der Landtag kann eine Vorsitzende oder einen Vorsitzenden abwählen.
Die Abstimmung über den Abwahlantrag kann frühestens nach Ablauf des Tages erfolgen, der auf den Tag des Eingangs des Antrags bei der Präsidentin oder dem Präsidenten des Landtages folgt.
Über den Antrag ist ohne Aussprache abzustimmen.
Die oder der Vorsitzende ist abgewählt, wenn zwei Drittel der anwesenden Mitglieder des Landtages dem Antrag zustimmen.

(5) Wird die oder der Vorsitzende abgewählt oder scheidet sie oder er aus sonstigen Gründen aus, bleibt das Recht ihrer oder seiner Fraktion auf den Vorsitz unberührt.

#### § 5 Vorsitzende

(1) Die oder der Vorsitzende ist Mitglied des Untersuchungsausschusses.
Sie oder er leitet das Untersuchungsverfahren unparteiisch und gerecht und wahrt die Ordnung des Ausschusses.
Die oder der Vorsitzende ist im Ausschuss nicht stimmberechtigt und wird auf die Zahl der Mitglieder nicht angerechnet.

(2) Der oder dem Vorsitzenden obliegt es, die verhandlungsleitenden Verfügungen zu erlassen, insbesondere Ort und Termin von Beweiserhebungen festzulegen, im Rahmen der durch den Ausschuss gefassten Beschlüsse Zeuginnen und Zeugen und Sachverständige zu laden, ihre Vernehmung einzuleiten und Beweismittel bei den zuständigen Stellen anzufordern.
Die oder der Vorsitzende hat ferner weitere, ihr oder ihm von diesem Gesetz übertragene Befugnisse.

(3) Jedes Mitglied kann die Entscheidung des Ausschusses beantragen, eine Anordnung der oder des Vorsitzenden ganz oder teilweise aufzuheben oder zu ändern oder die Vorsitzende oder den Vorsitzenden zu einer unterlassenen Anordnung zu verpflichten.

#### § 6 Stellvertretende Vorsitzende

Die Stellvertreterin oder der Stellvertreter der oder des Vorsitzenden besitzt bei Verhinderung der oder des Vorsitzenden deren oder dessen Rechte und Pflichten.
Übt sie oder er die Aufgaben der oder des Vorsitzenden aus, ist sie oder er im Untersuchungsausschuss nicht stimmberechtigt; ihre beziehungsweise seine Rechte und Pflichten als ordentliches Mitglied werden solange von einem stellvertretenden Mitglied aus ihrer beziehungsweise seiner Fraktion wahrgenommen.

#### § 7 Stellvertretende Mitglieder

Die stellvertretenden Mitglieder können an allen Sitzungen teilnehmen.
Bei Verhinderung eines ordentlichen Mitglieds nimmt ein stellvertretendes Mitglied aus der Fraktion oder der Gruppe, der das verhinderte Mitglied angehört, dessen Aufgaben wahr.

#### § 8 Ausscheiden von Mitgliedern

(1) Ein Mitglied des Landtages, das an den zu untersuchenden Vorgängen persönlich oder unmittelbar beteiligt ist oder war, darf dem Untersuchungsausschuss weder als ordentliches noch als stellvertretendes Mitglied angehören.
Wird dies erst nach der Wahl des ordentlichen oder des stellvertretenden Mitglieds bekannt, so hat es aus dem Untersuchungsausschuss auszuscheiden.

(2) Hält das betroffene ordentliche oder stellvertretende Mitglied die Voraussetzung des Absatzes 1 für nicht gegeben, entscheidet der Untersuchungsausschuss mit der Mehrheit von zwei Dritteln seiner Mitglieder über das Ausscheiden; bei dieser Entscheidung wird ein ordentliches Mitglied gemäß § 7 vertreten.
Der Untersuchungsausschuss gibt dem betroffenen Mitglied vor seiner Entscheidung Gelegenheit zur Stellungnahme.

(3) Scheidet ein ordentliches oder stellvertretendes Mitglied aus seiner Fraktion oder Gruppe aus, so scheidet es auch aus dem Untersuchungsausschuss aus.

(4) Bei Ausscheiden eines ordentlichen oder stellvertretenden Mitglieds wählt der Landtag auf Vorschlag der Fraktion oder der Gruppe, der das ordentliche oder das stellvertretende Mitglied angehört, unverzüglich ein neues ordentliches oder stellvertretendes Mitglied.

(5) Die Rechte eines ordentlichen oder stellvertretenden Mitglieds ruhen für die Zeit, in der es als Zeugin oder Zeuge oder als Sachverständige oder Sachverständiger vernommen wird.

### Abschnitt 2  
Verfahren

#### § 9 Einberufung, Beschlussfassung, Anwendung der Geschäftsordnung des Landtages Brandenburg

(1) Die oder der Vorsitzende beruft den Untersuchungsausschuss unter Angabe der Tagesordnung ein.
Sie oder er ist zur Einberufung einer Sitzung binnen zwei Wochen verpflichtet, wenn dies von mindestens einem Fünftel der Mitglieder unter Angabe des Beratungsgegenstandes verlangt wird.

(2) Der Untersuchungsausschuss ist nicht beschlussfähig, wenn nur die Hälfte seiner Mitglieder oder weniger anwesend ist und dies auf Antrag festgestellt worden ist.

(3) Ist der Untersuchungsausschuss nicht beschlussfähig, so unterbricht die oder der Vorsitzende sofort die Sitzung auf bestimmte Zeit.
Ist nach Ablauf dieser Zeit die Beschlussfähigkeit noch nicht eingetreten, so ist unverzüglich eine neue Sitzung einzuberufen.
In dieser Sitzung zur gleichen Tagesordnung ist der Untersuchungsausschuss beschlussfähig, auch wenn nicht die Mehrheit seiner Mitglieder anwesend ist.
Darauf ist in der Einladung hinzuweisen.

(4) Soweit in diesem Gesetz nichts anderes bestimmt ist, beschließt der Untersuchungsausschuss mit der Mehrheit der auf Ja oder Nein lautenden Stimmen.
Bei Stimmengleichheit ist der Antrag abgelehnt.
Soweit nach diesem Gesetz eine Zweidrittelmehrheit erforderlich ist, bedarf der Beschluss der Zustimmung von zwei Dritteln der anwesenden Mitglieder.

(5) Soweit dieses Gesetz keine Bestimmungen enthält, gelten die Regelungen der Geschäftsordnung des Landtages Brandenburg einschließlich ihrer Anlagen.

#### § 10 Unterausschuss

(1) Der Untersuchungsausschuss kann jederzeit einen Unterausschuss zur Vorbereitung oder Unterstützung der nach dem Untersuchungsauftrag anstehenden Untersuchung oder Teilen hiervon einsetzen.
Der Einsetzungsbeschluss legt den Auftrag an den Unterausschuss fest und bestimmt seine Sprecherin oder seinen Sprecher.
Die Vertreterinnen und Vertreter der antragstellenden Mitglieder des Landtages können mindestens ein Mitglied in den Unterausschuss entsenden.

(2) Der Unterausschuss sammelt, sichtet und gliedert den Untersuchungsstoff.
Er kann Personen informatorisch anhören.

(3) Die Sitzungen des Unterausschusses sind nichtöffentlich.
Die Bestimmungen des Geheimschutzes für den Untersuchungsausschuss gelten für den Unterausschuss entsprechend.
Die Sitzungen sind in entsprechender Anwendung des § 14 Absatz 1 und Absatz 2 Satz 2 zu protokollieren.

(4) Über Ort und Zeit der Beratungen des Unterausschusses werden die Mitglieder des Untersuchungsausschusses von der Sprecherin oder dem Sprecher unterrichtet.
Die ordentlichen und stellvertretenden Mitglieder des Untersuchungsausschusses haben jederzeit Zugang zu den Beratungen der Unterausschüsse.

#### § 11 Öffentlichkeit der Sitzungen, Geheimschutz

(1) Die Beweisaufnahme erfolgt in öffentlicher Sitzung.
Ton- und Bildaufnahmen und Ton- und Bildübertragungen sind nicht zulässig.
Für Ausnahmen von Satz 2 bedarf es eines Beschlusses des Untersuchungsausschusses sowie der Zustimmung der zu vernehmenden oder anzuhörenden Person.
Die oder der Vorsitzende kann die Nutzung von technischen Geräten, mit denen eine Ton- oder Bildaufnahme möglich ist, während der Sitzung untersagen.

(2) Die oder der Vorsitzende kann die Öffentlichkeit oder einzelne Personen ausschließen, wenn das überwiegende öffentliche Interesse oder berechtigte Interessen Einzelner dies gebieten oder wenn es zur Erlangung einer wahrheitsgemäßen Aussage erforderlich erscheint.
Jedes Mitglied kann beantragen, dass der Ausschuss die Entscheidung der oder des Vorsitzenden aufhebt.

(3) Beratung und Beschlussfassung sind nichtöffentlich.

(4) Mitglieder der Landesregierung und die von ihnen Beauftragten können an nichtöffentlichen Sitzungen sowie Sitzungen gemäß Absatz 5 Satz 1 mit Zustimmung des Untersuchungsausschusses teilnehmen.
Für sonstige Personen, die nicht dem Untersuchungsausschuss angehören, wird die Zustimmung nach den Maßgaben der Verschlusssachenordnung des Landtages Brandenburg erteilt.
Über die Teilnahme beschließt der Untersuchungsausschuss mit Zweidrittelmehrheit.

(5) Der Untersuchungsausschuss versieht durch Beschluss Beweismittel, Beweiserhebungen und Beratungen entsprechend den Maßgaben der Verschlusssachenordnung des Landtages Brandenburg mit einem angemessenen Geheimhaltungsgrad (Einstufung), soweit das öffentliche Interesse oder die berechtigten Interessen Einzelner dies gebieten und Maßnahmen gemäß Absatz 2 nicht geeignet oder nicht ausreichend sind.
Über die Einstufung entscheidet der Untersuchungsausschuss mit Zweidrittelmehrheit.

#### § 12 Mitteilungen über Sitzungen und Unterlagen

(1) Über Art und Umfang von Mitteilungen an die Öffentlichkeit aus nichtöffentlichen Sitzungen entscheidet der Untersuchungsausschuss unter Beachtung der in § 11 Absatz 2 Satz 1 genannten Interessen.
§ 11 Absatz 5 bleibt unberührt.

(2) Die ordentlichen und stellvertretenden Mitglieder des Untersuchungsausschusses sowie die Personen, die gemäß § 11 Absatz 4 zur Sitzung zugelassen wurden, sind zur Verschwiegenheit gegenüber jedermann verpflichtet, soweit es sich um Tatsachen handelt, die sie im Untersuchungsausschuss erfahren haben und die nicht Gegenstand der öffentlichen Verhandlung gewesen sind.

(3) Vor Abschluss der Beratung über einen Gegenstand der Verhandlung sollen sich die Mitglieder des Untersuchungsausschusses einer öffentlichen Beweiswürdigung enthalten.

#### § 13 Ordnungsgewalt

(1) Die Aufrechterhaltung der Ordnung in der Sitzung obliegt der Vorsitzenden oder dem Vorsitzenden.
Zeuginnen und Zeugen, Sachverständige, Beistände, Zuhörinnen und Zuhörer sowie sonstige anwesende Personen, die ihren oder seinen Anordnungen nicht Folge leisten, können auf Beschluss des Untersuchungsausschusses aus dem Sitzungssaal entfernt werden.

(2) Der Untersuchungsausschuss kann außerdem gegen Personen, die sich in der Sitzung einer Ungebühr schuldig gemacht haben, unbeschadet einer strafrechtlichen Verfolgung ein Ordnungsgeld bis zu 2 000 Euro verhängen.
Gegen den Beschluss über die Verhängung eines Ordnungsgeldes können die betroffenen Personen innerhalb einer Frist von 14 Tagen nach Bekanntgabe (§ 35 der Strafprozessordnung) Antrag auf gerichtliche Entscheidung beim Brandenburgischen Oberlandesgericht stellen.
Der Antrag auf gerichtliche Entscheidung hat aufschiebende Wirkung.
Im Übrigen gilt § 161a Absatz 3 Satz 3 und 4 der Strafprozessordnung entsprechend.

(3) Das Ordnungsgeld wird auf Veranlassung der oder des Vorsitzenden des Untersuchungsausschusses, nach Beendigung des Untersuchungsverfahrens auf Veranlassung der Präsidentin oder des Präsidenten des Landtages nach den Vorschriften des Justizbeitreibungsgesetzes durch die danach zuständige Stelle eingezogen.

#### § 14 Protokollierung

(1) Über die Sitzungen des Untersuchungsausschusses ist ein Protokoll anzufertigen und von der oder dem Vorsitzenden zu unterschreiben.

(2) Beweisaufnahmen sind wörtlich zu protokollieren.
Über die Art der Protokollierung der Beratungen entscheidet der Untersuchungsausschuss.

(3) Einsicht, Weitergabe und Veröffentlichung der Protokolle erfolgen gemäß der Geschäftsordnung des Landtages Brandenburg, soweit der Untersuchungsausschuss nicht eine andere Regelung beschließt.

#### § 15 Beweisaufnahme

(1) Der Untersuchungsausschuss erhebt die durch den Untersuchungsauftrag gebotenen Beweise aufgrund von Beweisbeschlüssen.

(2) Der Untersuchungsausschuss ist dazu verpflichtet, Beweise zu erheben, wenn dies von einem Fünftel der Mitglieder beantragt wird.

(3) Die Beweiserhebung ist unzulässig, wenn sie nicht im Rahmen des Untersuchungsauftrages liegt, wenn sie wegen Offenkundigkeit überflüssig ist, die Tatsache, die bewiesen werden soll, für die Untersuchung ohne Bedeutung oder schon erwiesen ist, wenn das Beweismittel ungeeignet oder auch im Fall der Anwendung der nach diesem Gesetz zulässigen Zwangsmittel unerreichbar ist oder wenn der Antrag ersichtlich zum Zwecke der Verschleppung des Verfahrens gestellt ist.
§ 244 Absatz 4 Satz 2 der Strafprozessordnung ist entsprechend anzuwenden.

#### § 16 Aktenvorlage, Aussagegenehmigung, Zutrittsrecht

(1) Die Landesregierung, die Behörden und Verwaltungseinrichtungen des Landes und die Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts, die der Aufsicht des Landes unterstehen, sind auf Ersuchen der oder des Vorsitzenden verpflichtet, dem Untersuchungsausschuss unverzüglich die sächlichen Beweismittel, insbesondere die Akten, vorzulegen, Auskünfte zu geben und Zutritt zu Behörden, Dienststellen und Einrichtungen zu gewähren.
Auf Ersuchen der oder des Vorsitzenden sind Dokumente in digitaler Form vorzulegen, soweit dies technisch möglich ist und der Geheimschutz gewahrt ist.

(2) Ersuchen um Zutritt und zur Vorlage sächlicher Beweismittel, insbesondere der Akten, sind an die zuständige oberste Dienstbehörde oder oberste Aufsichtsbehörde zu richten.
Wenn ein Ersuchen ganz oder teilweise abgelehnt wird, insbesondere sächliche Beweismittel als Verschlusssache eingestuft vorgelegt werden, ist der Untersuchungsausschuss von der ersuchten Stelle schriftlich über die Gründe zu unterrichten.
Die Vorlage ist mit einer Auskunft über die Vollständigkeit zu verbinden.

(3) Ersuchen nach Absatz 1 können insbesondere zurückgewiesen werden, soweit

1.  die räumliche, zeitliche oder sachliche Untersuchungskompetenz fehlt,
2.  sie unzulässig in den Kernbereich exekutiver Eigenverantwortung eingreifen oder
3.  sie den unantastbaren Kernbereich der privaten Lebensgestaltung einer Person betreffen.

(4) Ebenso können Ersuchen nach Absatz 1 zurückgewiesen werden, die

1.  dem Wohle des Landes, des Bundes oder eines anderen Landes Nachteile bereiten würden oder
2.  unverhältnismäßig in die Grundrechte einzelner Personen eingreifen würden,

sofern der Untersuchungsausschuss nicht die notwendigen Maßnahmen des Geheimschutzes zum Schutze dieser öffentlichen oder berechtigten privaten Interessen beschließt.

(5) Die Absätze 1 bis 4 gelten für die Erteilung einer Aussagegenehmigung entsprechend.

#### § 17 Zeuginnen und Zeugen

Zeuginnen und Zeugen sind verpflichtet, auf Ladung des Untersuchungsausschusses zu erscheinen.
Sie sind in der Ladung über das Beweisthema zu unterrichten, über ihre Rechte zu belehren sowie auf die gesetzlichen Folgen des Ausbleibens hinzuweisen; ihnen ist mitzuteilen, dass sie einen Beistand zu der Vernehmung hinzuziehen dürfen.
Die oder der Vorsitzende kann einen Zeugenbeistand von der Vernehmung ausschließen, wenn der Beistand die Teilnahme an der Vernehmung missbraucht, um eine geordnete Beweiserhebung zu erschweren oder zu verhindern.

#### § 18 Zwangsmittel

(1) Gegen ordnungsgemäß geladene Zeuginnen und Zeugen, die ohne genügende Entschuldigung nicht erscheinen oder ohne gesetzlichen Grund die Aussage verweigern, werden auf Antrag der oder des Vorsitzenden des Untersuchungsausschusses durch das zuständige Gericht nach dessen Ermessen Ordnungsgeld bis zu 10 000 Euro, Ordnungshaft bis zu sechs Wochen oder Erzwingungshaft festgesetzt und es werden ihnen die entstandenen Kosten auferlegt.
Auf Antrag der oder des Vorsitzenden kann das zuständige Gericht die Vorführung einer Zeugin oder eines Zeugen anordnen.

(2) Die oder der Vorsitzende stellt die Anträge nach Absatz 1 auf Beschluss des Untersuchungsausschusses oder auf Verlangen eines Fünftels der Mitglieder.

(3) Die Vorschriften über den Strafprozess finden im Übrigen für Maßnahmen nach dieser Vorschrift entsprechende Anwendung.

#### § 19 Aussageverweigerungsrecht

(1) Eine Zeugin oder ein Zeuge kann die Aussage zu solchen Fragen verweigern, deren Beantwortung sie oder ihn selbst oder eine in § 52 Absatz 1 der Strafprozessordnung bezeichnete Person der Gefahr aussetzen würde, einer Untersuchung in einem Straf-, Ordnungswidrigkeiten- oder Disziplinarverfahren oder in einem sonstigen gesetzlich geordneten Verfahren unterzogen zu werden.
Die Vorschriften der §§ 53 und 53a der Strafprozessordung gelten entsprechend.

(2) Die Tatsache, auf die eine Zeugin oder ein Zeuge die Verweigerung der Aussage stützt, ist auf Verlangen glaubhaft zu machen.

#### § 20 Belehrung

(1) Zeuginnen und Zeugen sind von der oder dem Vorsitzenden vor Beginn der Vernehmung über ihre Rechte nach § 19 sowie am Ende der Vernehmung über das Verfahren nach § 21 Absatz 3 und 4 zu belehren.

(2) Zeuginnen und Zeugen sind vor ihrer Vernehmung zur Wahrheit zu ermahnen und über die strafrechtlichen Folgen einer falschen uneidlichen Aussage zu belehren.

#### § 21 Vernehmung, Fragerecht

(1) Zeuginnen und Zeugen sollen einzeln und in Abwesenheit der später zu hörenden Zeuginnen und Zeugen vernommen werden.
Eine Gegenüberstellung mit anderen Zeuginnen und Zeugen ist zulässig, wenn es für den Untersuchungszweck geboten ist.

(2) Zeuginnen und Zeugen werden zunächst durch die oder den Vorsitzenden vernommen.
Anschließend können die übrigen Mitglieder Fragen stellen.
Sie können auch jeweils mehrere Fragen stellen, wenn diese im Sachzusammenhang stehen.
Zeuginnen und Zeugen dürfen nur zum Thema des Beweisbeschlusses befragt werden.
Die oder der Vorsitzende kann nicht zum Beweisthema gehörende Fragen zurückweisen.

(3) Den Zeuginnen und Zeugen und ihren Beiständen ist Einsicht in das Protokoll der Vernehmung zu gewähren.

(4) Der Untersuchungsausschuss entscheidet, ob die Vernehmung der Zeugin oder des Zeugen abgeschlossen ist.
Die Entscheidung darf erst ergehen, wenn nach der Bekanntgabe der Möglichkeit zur Einsichtnahme gegenüber der Zeugin oder dem Zeugen gemäß Absatz 3 zwei Wochen verstrichen sind oder die Zeugin oder der Zeuge auf die Einhaltung dieser Frist verzichtet hat.

(5) § 54 der Strafprozessordnung gilt entsprechend.

#### § 22 Sachverständige

(1) Auf Sachverständige finden die §§ 19 bis 21 entsprechende Anwendung, soweit nicht die nachfolgenden Vorschriften abweichende Regelungen treffen.

(2) Die Auswahl der Sachverständigen erfolgt durch den Untersuchungsausschuss; die Rechte der antragstellenden Mitglieder nach § 15 Absatz 2 sind zu berücksichtigen.

(3) Der Untersuchungsausschuss soll mit den Sachverständigen darüber eine Absprache treffen, innerhalb welcher Frist das Gutachten erstellt wird.
Sollen Sachverständige mit einem Auftrag wie der Sichtung und Vorauswahl von Beweismitteln oder der Gliederung wesentlicher Teile des Untersuchungsstoffes betraut werden, soll der Gutachtenauftrag einvernehmlich mit den Sachverständigen erteilt werden.

(4) Sachverständige haben das Gutachten innerhalb der vereinbarten Frist unparteiisch, vollständig und wahrheitsgemäß zu erstatten.
Auf Verlangen des Untersuchungsausschusses ist das Gutachten schriftlich zu erstellen und mündlich zu erläutern.

(5) Weigern sich die zur Erstattung des Gutachtens verpflichteten Sachverständigen, nach Absatz 3 Satz 1 eine angemessene Frist zu vereinbaren, oder versäumen sie die vereinbarte Frist, so kann die oder der Vorsitzende des Untersuchungsausschusses beim zuständigen Gericht die Festsetzung eines Ordnungsgeldes bis zu 10 000 Euro beantragen.
Dasselbe gilt, wenn die ordnungsgemäß geladenen Sachverständigen nicht erscheinen oder sich weigern, ihr Gutachten zu erstatten oder zu erläutern; in diesen Fällen kann der Untersuchungsausschuss zugleich den Sachverständigen die durch ihre Säumnis oder Weigerung verursachten Kosten auferlegen.
§ 18 Absatz 2 und 3 gilt entsprechend.

(6) Werden Sachverständige mit einem Auftrag im Sinne von Absatz 3 Satz 2 betraut, haben sie entsprechend § 16 das Recht auf Zutritt zu Behörden, Dienststellen und Einrichtungen sowie auf Auskünfte und Vorlage von Beweismitteln.
Sie können den Herausgabeanspruch entsprechend § 23 Absatz 1 und 4 geltend machen.
Werden ihnen Rechte gemäß Satz 1 nicht gewährt, bedarf es eines Beweisbeschlusses gemäß § 15 Absatz 1.
Die Sachverständigen können Personen informatorisch anhören.

#### § 23 Herausgabepflicht, Beschlagnahme und Durchsuchung

(1) Wer einen Gegenstand, der als Beweismittel für die Untersuchung von Bedeutung sein kann, in seinem Gewahrsam hat, ist auf Verlangen des Untersuchungsausschusses zur Herausgabe verpflichtet.
Dies gilt nicht in den Fällen des § 16 Absatz 3 Nummer 3 und des § 19.

(2) Verweigert die den Gewahrsam innehabende Person die Herausgabe, kann das zuständige Gericht auf Antrag der oder des Vorsitzenden ein Ordnungsgeld bis zu 10 000 Euro, Ordnungshaft bis zu sechs Wochen oder Erzwingungshaft festsetzen.

(3) Auf Antrag der oder des Vorsitzenden ordnet das zuständige Gericht Beschlagnahmen und Durchsuchungen und die Übergabe an den Untersuchungsausschuss an, wenn dies zur Aufklärung des Sachverhalts notwendig ist.
Die §§ 97, 104, 105 Absatz 2 und 3, §§ 106, 107 und 109 der Strafprozessordnung gelten entsprechend.
Der Untersuchungsausschuss kann die zuständige Staatsanwaltschaft ersuchen, die Anordnungen zu vollziehen.

(4) Der Untersuchungsausschuss prüft die erlangten Beweismittel unverzüglich auf ihre Beweiserheblichkeit.
Unerhebliche Beweismittel sind unverzüglich zurückgegeben.
Die Beweismittel gelten während der Prüfung als VS-VERTRAULICH im Sinne der Verschlusssachenordnung des Landtages Brandenburg eingestuft.
Nach der Prüfung hebt der Untersuchungsausschuss die Einstufung auf oder nimmt eine Einstufung gemäß § 11 Absatz 5 vor.

(5) § 18 Absatz 2 und 3 gilt entsprechend.

(6) Das Brief-, Post- und Fernmeldegeheimnis bleibt unangetastet.

#### § 24 Rechts- und Amtshilfe

(1) Bei Ersuchen um Rechtshilfe zur Vernehmung von Zeuginnen und Zeugen oder Sachverständigen sind die Fragen im Einzelnen festzulegen.
Dem Ersuchen ist eine schriftliche Fassung des Untersuchungsauftrages beizufügen.

(2) Das Ersuchen um Rechtshilfe ist an das Amtsgericht zu richten, in dessen Bezirk die Untersuchungshandlung vorgenommen werden soll.

(3) Über die Untersuchungshandlung hat die um Rechts- oder Amtshilfe ersuchte Stelle ein Protokoll aufzunehmen.

(4) Die Rechts- und Amtshilfe durch die Gerichte und Behörden eines anderen Landes oder des Bundes richtet sich nach den allgemeinen Bestimmungen.

#### § 25 Verlesen von Protokollen und Schriftstücken

(1) Die Protokolle über Untersuchungshandlungen von Gerichten, Verwaltungsbehörden und Untersuchungsausschüssen sowie Schriftstücke, die als Beweismittel dienen, sind vor dem Untersuchungsausschuss zu verlesen.

(2) Von der Verlesung kann Abstand genommen werden, wenn die Protokolle oder Schriftstücke allen Mitgliedern zugänglich gemacht worden sind und der Ausschuss auf die Verlesung verzichtet.

(3) Für die Verlesung gilt § 11 Absatz 2 und 5 entsprechend.

### Abschnitt 3  
Beendigung des Verfahrens

#### § 26 Beendigung

Die Arbeit des Untersuchungsausschusses endet

1.  mit der Kenntnisnahme des Schlussberichts durch den Landtag,
2.  durch die Auflösung des Ausschusses gemäß § 27 Absatz 2 oder
3.  mit dem Ende der Wahlperiode des Landtages.

#### § 27 Aussetzung und Auflösung

(1) Das Untersuchungsverfahren kann ausgesetzt werden, wenn eine alsbaldige Aufklärung auf andere Weise zu erwarten ist oder die Gefahr besteht, dass gerichtliche Verfahren oder Ermittlungsverfahren beeinträchtigt werden.
Über die Aussetzung entscheidet der Landtag auf Antrag des Untersuchungsausschusses.
Die Aussetzung darf nicht erfolgen, wenn ein Fünftel der Mitglieder des Landtages, die zu den antragstellenden Abgeordneten gehört haben, der Aussetzung widerspricht.
Ein ausgesetztes Verfahren kann jederzeit durch Beschluss des Landtages wieder aufgenommen werden.
Der Beschluss muss gefasst werden, wenn er von einem Fünftel der Mitglieder des Landtages, die zu den antragstellenden Abgeordneten gehört haben, beantragt wird.
§ 2 Absatz 2 und 4 gilt entsprechend.

(2) Der Landtag kann einen Untersuchungsausschuss vor Abschluss der Ermittlungen auflösen.
Eine Auflösung findet nicht statt, wenn ein Fünftel der Mitglieder des Landtages widerspricht.
Die widersprechenden Abgeordneten müssen jedoch zu den antragstellenden Abgeordneten gehört haben.

#### § 28 Berichte

(1) Nach Abschluss der Untersuchung erstattet der Untersuchungsausschuss dem Landtag einen schriftlichen Bericht.

(2) Die Anfertigung des Berichtsentwurfs obliegt der oder dem Vorsitzenden.
Über die Endfassung entscheidet der Untersuchungsausschuss.
Der Bericht hat den Gang der Untersuchung, die ermittelten Tatsachen und das Ergebnis der Untersuchung wiederzugeben.
Der Bericht kann Empfehlungen enthalten.
Der Bericht darf keine Tatsachendarstellungen enthalten, die gemäß § 11 Absatz 2 oder 5 nicht der Öffentlichkeit zugänglich gemacht werden können.
Sind solche Tatsachendarstellungen zur Erfüllung des Untersuchungsauftrages zwingend erforderlich, sind sie in einem gesonderten Teil des Berichts aufzunehmen.
Dieser Teil ist entsprechend § 11 Absatz 5 zu behandeln.

(3) Jedes Mitglied des Untersuchungsausschusses hat das Recht, seine im Untersuchungsverfahren vertretene abweichende Meinung darzulegen; dieser Bericht ist dem Bericht des Untersuchungsausschusses anzuschließen.
Absatz 2 Satz 3 bis 7 gilt entsprechend.

(4) Über abtrennbare Teile des Untersuchungsauftrages hat der Untersuchungsausschuss auf Verlangen des Landtages oder der antragstellenden Mitglieder des Landtages einen Teilbericht zu erstatten, wenn die Beweisaufnahme zu diesem Teil abgeschlossen und der Bericht ohne Vorgriff auf die Beweiswürdigung der übrigen Untersuchungsaufträge möglich ist.

(5) Der Landtag kann vom Untersuchungsausschuss jederzeit bei Vorliegen eines allgemeinen öffentlichen Interesses oder wenn ein Schlussbericht vor Ablauf der Wahlperiode nicht erstellt werden kann, einen Zwischenbericht über den Stand der Untersuchungen verlangen.
Soll der Zwischenbericht eine Beweiswürdigung enthalten, ist er mit der Mehrheit von zwei Dritteln der Mitglieder zu beschließen.

(6) Auf Teil- und Zwischenberichte finden die Regelungen der Absätze 1 bis 3 entsprechende Anwendung.

#### § 29 Kosten und Auslagen

Die Kosten des Untersuchungsverfahrens trägt das Land; das gilt auch für die Kosten der Ausstattung des Untersuchungsausschusses, der Fraktionen und Gruppen nach § 2 Absatz 5.
Zeuginnen und Zeugen, Sachverständige sowie informatorisch angehörte Personen erhalten auf Antrag eine Entschädigung oder Vergütung nach dem Justizvergütungs- und Entschädigungsgesetz.
Die Entschädigung oder Vergütung wird von der Landtagsverwaltung festgesetzt.
Gegen die Festsetzung können die betroffenen Personen einen Antrag auf Entscheidung des zuständigen Gerichts stellen.

#### § 30 Gerichtliches Verfahren

(1) Zuständiges Gericht im Sinne des Gesetzes ist das für den Sitz des Landtages örtlich zuständige Landgericht.

(2) Gegen die Entscheidung des Landgerichts können der Untersuchungsausschuss und die Personen, die betroffen sind, und nach Beendigung des Untersuchungsverfahrens die Präsidentin oder der Präsident des Landtages Beschwerde erheben.
Die Vorschriften der Strafprozessordnung über die Beschwerde sind mit der Maßgabe anzuwenden, dass an die Stelle der Staatsanwaltschaft die oder der Vorsitzende, nach Beendigung des Untersuchungsverfahrens die Präsidentin oder der Präsident des Landtages tritt.

(3) Zuständig für die Entscheidung nach Absatz 2 ist das Brandenburgische Oberlandesgericht.

(4) Die Zuständigkeiten des Verfassungsgerichts des Landes Brandenburg bleiben unberührt.

#### § 31 Einschränkung von Grundrechten

Aufgrund dieses Gesetzes werden die Grundrechte

1.  auf Freiheit der Person nach Artikel 2 Absatz 2 Satz 2, Artikel 104 des Grundgesetzes und Artikel 9 der Verfassung des Landes Brandenburg,
2.  auf Schutz personenbezogener Daten nach Artikel 2 Absatz 1 in Verbindung mit Artikel 1 Absatz 1 des Grundgesetzes und Artikel 11 Absatz 1 der Verfassung des Landes Brandenburg,
3.  auf Unverletzlichkeit der Wohnung nach Artikel 13 des Grundgesetzes und Artikel 15 der Verfassung des Landes Brandenburg,
4.  auf Berufsfreiheit nach Artikel 12 Absatz 1 des Grundgesetzes und Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg

eingeschränkt.