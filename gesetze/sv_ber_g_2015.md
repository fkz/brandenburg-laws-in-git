## Gesetz zur Errichtung eines Sondervermögens Finanzierungsfonds Flughafen BER (Finanzierungsfonds Flughafen BER-Gesetz - SV BER-G)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Errichtung

Das Land Brandenburg errichtet unter dem Namen „Sondervermögen Finanzierungsfonds Flughafen BER“ ein rechtlich unselbstständiges Sondervermögen des Landes gemäß § 26 Absatz 2 der Landeshaushaltsordnung.

#### § 2 Zweck des Sondervermögens

(1) Aus dem Sondervermögen werden Kapitalzuführungen des Landes Brandenburg an die Flughafen Berlin Brandenburg GmbH (FBB), soweit nicht im Haushaltsgesetz 2015/2016 enthalten, zur Finanzierung des Flughafens Berlin Brandenburg (BER) bereitgestellt und ausgereicht.

(2) Das Sondervermögen soll Kapitalzuführungen an die FBB in Form von Darlehen leisten.
Kapitalzuführungen in Form von Einzahlungen in das Eigenkapital der FBB oder die Umwandlung von Darlehen des Sondervermögens in Eigenkapital der FBB sind nur nach Einwilligung des Landtages zulässig.

(3) Im Sondervermögen wird der gesamte Schuldendienst abgewickelt.

#### § 3 Stellung im Rechtsverkehr

(1) Das Sondervermögen ist nicht rechtsfähig.
Es kann unter seinem Namen im Rechtsverkehr handeln, klagen und verklagt werden.
Der allgemeine Gerichtsstand des Sondervermögens ist Potsdam.

(2) Das Sondervermögen ist vom übrigen Vermögen des Landes, seinen Rechten und Verbindlichkeiten getrennt zu halten.
Das Land haftet unmittelbar für die Verbindlichkeiten des Sondervermögens; dieses haftet nicht für die sonstigen Verbindlichkeiten des Landes.

#### § 4 Verwaltung

(1) Das für Finanzen zuständige Ministerium verwaltet das Sondervermögen.
Es kann sich hierzu einer anderen Landesbehörde oder eines Dritten bedienen.
Die Kosten für die Verwaltung des Sondervermögens trägt das Land.

(2) Alle Einnahmen und Ausgaben des Sondervermögens werden in jährlichen Wirtschaftsplänen veranschlagt.
Für die Wirtschaftsjahre 2015 und 2016 werden die Wirtschaftspläne als Anlage zu diesem Gesetz veröffentlicht.
Ab dem Haushaltsjahr 2017 ist der Wirtschaftsplan dem Einzelplan 20 des Landeshaushalts als Anlage beizufügen.
Die Wirtschaftspläne sind in Einnahmen und Ausgaben auszugleichen.
Nicht verausgabte Mittel des laufenden Wirtschaftsjahres können einer Rücklage des Sondervermögens zugeführt werden.
Rücklagen aus dem Vorjahr dürfen zur Verstärkung der Ausgaben verwendet werden.
Im Übrigen ist § 113 Absatz 1 der Landeshaushaltsordnung anzuwenden.

#### § 5 Finanzierung

(1) Das Sondervermögen ist befugt, zur Finanzierung seiner Zwecke bis einschließlich des Wirtschaftsjahres 2019 in seinem Namen Kredite bis zur Höhe der im Wirtschaftsplan genannten Summe aufzunehmen.

(2) Soweit im laufenden Wirtschaftsjahr die Kreditermächtigung nicht ausgeschöpft wird, steht diese in den nachfolgenden Wirtschaftsjahren zusätzlich zur Verfügung, längstens jedoch bis 2019.
Im Jahr 2019 nicht ausgeschöpfte Kreditermächtigungen erlöschen am Jahresende.

(3) Zins- und Tilgungsleistungen der FBB auf Kapitalzuführungen in Form von Darlehen aus dem Sondervermögen fließen dem Sondervermögen zu und sind von diesem für seinen Schuldendienst zu verwenden.

(4) Für den anderweitig nicht gedeckten Schuldendienst erhält das Sondervermögen bedarfsgerechte Zuführungen aus dem Landeshaushalt.
Soweit der Landeshaushalt dabei in Vorleistung für Schuldendienstzahlungen der FBB tritt, können Zins- und Tilgungsleistungen nach Absatz 3 auch an den Landeshaushalt abgeführt werden.

#### § 6 Auflösung

Das Sondervermögen ist nach Erfüllung seines Zwecks gemäß § 2 aufzulösen.
Das restliche Vermögen und die Verbindlichkeiten des Sondervermögens verbleiben beim Land.
Die Einzelheiten zur Auflösung und Abwicklung des Sondervermögens bestimmt das für Finanzen zuständige Mitglied der Landesregierung durch Rechtsverordnung.

#### § 7 Inkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 1.
Juli 2015

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

### Anlagen

1

[Übersicht über den Wirtschaftsplan des Sondervermögens Finanziergsfonds Flughafen BER](/br2/sixcms/media.php/68/GVBl_I_20_2015-Anlage.pdf "Übersicht über den Wirtschaftsplan des Sondervermögens Finanziergsfonds Flughafen BER") 1.2 MB