## Gesetz zu dem Zweiten Staatsvertrag zur Änderung des Staatsvertrages über die Berlin-Brandenburgische Akademie der Wissenschaften

Der Landtag hat das folgende Gesetz beschlossen:

### § 1  
Zustimmung zum Staatsvertrag

Dem am 6.
April 2011 vom Land Brandenburg unterzeichneten Zweiten Staatsvertrag zur Änderung des Staatsvertrages über die Berlin-Brandenburgische Akademie der Wissenschaften wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2  
Inkrafttreten

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel II in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 7.
Juli 2011

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch

[zum Staatsvertrag](/vertraege/akwissenschaften_stv_2011)