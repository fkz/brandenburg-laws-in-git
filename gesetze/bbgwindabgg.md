## Gesetz zur Zahlung einer Sonderabgabe an Gemeinden im Umfeld von Windenergieanlagen (Windenergieanlagenabgabengesetz - BbgWindAbgG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Zahlungsverpflichtete

(1) Betreiber von Windenergieanlagen, die nach dem Bundes-Immissionsschutzgesetz in der Fassung der Bekanntmachung vom 17.
Mai 2013 (BGBl. I S. 1274), das zuletzt durch Artikel 1 des Gesetzes vom 8. April 2019 (BGBl. I S. 432) geändert worden ist, genehmigt und nach dem 31. Dezember 2019 in Betrieb genommen worden sind, sind zur Zahlung einer Sonderabgabe an anspruchsberechtigte Gemeinden verpflichtet.

(2) Ausgenommen von der Zahlungspflicht nach Absatz 1 sind Windenergieanlagen, die in den Ausschreibungsrunden nach § 28 Absatz 1 des Erneuerbare-Energien-Gesetzes vom 21. Juli 2014 (BGBl. I S. 1066), das zuletzt durch Artikel 5 des Gesetzes vom 13.
Mai 2019 (BGBl. I S. 706, 723) geändert worden ist, in den Jahren 2017, 2018 und 2019 bezuschlagt worden sind.

#### § 2 Ausgestaltung und Höhe der Sonderabgabe

(1) Die Sonderabgabe ist jährlich für die Dauer des Betriebs der jeweiligen Windenergieanlage an anspruchsberechtigte Gemeinden zu zahlen.

(2) Die Sonderabgabe beträgt 10 000 Euro je Windenergieanlage und Jahr.

(3) Die laufende Zahlung hat ab dem Inbetriebnahmejahr jeweils bis zum 30. April des darauffolgenden Jahres zu erfolgen.

#### § 3 Anspruchsberechtigte

(1) Anspruchsberechtigt sind die Gemeinden im Land Brandenburg, deren Gemeindegebiet sich ganz oder teilweise im Radius von 3 Kilometern um den Standort der jeweiligen Windenergieanlage befindet.

(2) Sind mehrere Gemeinden pro Windenergieanlage anspruchsberechtigt, wird der Zahlungsanspruch unter den Gemeinden aufgeteilt und dabei die Anspruchshöhe pro Gemeinde anhand des Anteils des Gemeindegebietes an der Fläche des Umkreises, der sich um die Windenergieanlage befindet, zur Grundlage genommen.

(3) Die Betreiber der zahlungspflichtigen Windenergieanlagen sind zur Ermittlung der anspruchsberechtigten Gemeinden und der Höhe des anteiligen Anspruchs pro Gemeinde verpflichtet.
Auf Verlangen der anspruchsberechtigten Gemeinden haben die Betreiber der zahlungspflichtigen Windenergieanlagen die ordnungsgemäße Berechnung der Anspruchshöhe in geeigneter Form nachzuweisen.

(4) Die Einnahmen aus der Sonderabgabe werden von den Finanzausgleichsvorschriften des Bundes und des Landes Brandenburg nicht erfasst.

#### § 4 Zweckbindung

Die Gemeinden haben die Mittel aus der Sonderabgabe für Maßnahmen in ihren Gemeinden zur Steigerung der Akzeptanz für Windenergieanlagen zu verwenden.
Zur Erreichung dieses Zwecks kommen insbesondere Maßnahmen

1.  zur Aufwertung von Ortsbild und ortsgebundener Infrastruktur,
2.  zur Information über Stromerzeugung aus Erneuerbaren Energien und über Möglichkeiten zur Nutzung Erneuerbarer Energien,
3.  zur Förderung kommunaler Veranstaltungen, sozialer Aktivitäten oder Einrichtungen, die der Kultur, Bildung oder Freizeit dienen, oder unternehmerischer Tätigkeit in der Gemeinde und
4.  zu Kommunalen Bauleitplanungen im Bereich der Erneuerbaren Energien

in Betracht, wobei für die Einwohner ein Bezug zu den aus der Windenergieerzeugung generierten Geldmitteln erkennbar sein soll.

#### § 5 Berichterstattung

Die Landesregierung berichtet dem Landtag vier Jahre nach dem Inkrafttreten dieses Gesetzes über dessen Auswirkungen und eventuell notwendige Anpassungen.

#### § 6 Ordnungswidrigkeit

(1) Ordnungswidrig handelt, wer vorsätzlich oder fahrlässig entgegen § 2 eine laufende Zahlung an anspruchsberechtigte Gemeinden trotz Fälligkeit nicht entrichtet.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu einhunderttausend Euro geahndet werden.

#### § 7 Zuständigkeit

(1) Zuständig für die Überwachung und Durchsetzung der Pflichten aus diesem Gesetz einschließlich der Verfolgung und Ahndung der Ordnungswidrigkeit nach § 6 ist das für Energiepolitik zuständige Mitglied der Landesregierung.

(2) Das für Energiepolitik zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung nähere Vorschriften zu erlassen über den Umfang, den Inhalt und die Form

1.  der Ausgestaltung und Berechnung der Höhe der Sonderabgabe nach § 2 Absatz 2 und 3,
2.  einer Geringfügigkeitsgrenze bei Auszahlungsbeträgen an Gemeinden nach § 3 Absatz 2,
3.  der Erfüllung der Pflichten der zahlungspflichtigen Windenergieanlagenbetreiber nach § 3 Absatz 3 und
4.  der zweckentsprechenden Verwendung der Sonderabgabe nach § 4.

#### § 8 Inkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 19.
Juni 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark