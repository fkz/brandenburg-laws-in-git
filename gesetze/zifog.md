## Gesetz zur Errichtung eines Sondervermögens „Zukunftsinvestitionsfonds des Landes Brandenburg“ (Zukunftsinvestitionsfonds-Errichtungsgesetz - ZifoG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Errichtung und Verwaltung

Das Land Brandenburg errichtet unter dem Namen „Zukunftsinvestitionsfonds des Landes Brandenburg“ ein Sondervermögen gemäß § 26 Absatz 2 der Landeshaushaltsordnung, welches vom für Finanzen zuständigen Ministerium verwaltet wird.

#### § 2 Zweck des Sondervermögens

(1) Zweck des Sondervermögens ist die Finanzierung investiver Ausgaben des Landeshaushalts für Projekte in den Bereichen Regionalentwicklung, Klimaschutz, moderne Infrastruktur, Digitalisierung und Innovationen.
Das Sondervermögen soll ergänzende Haushaltsmittel für Investitionen bereitstellen.

(2) Die Mittel werden dabei ausschließlich für landespolitisch strategisch bedeutende Projekte, für die eine besonders günstige Relation zwischen dem verfolgten Zweck und den einzusetzenden Mitteln zu erwarten ist, zur Verfügung gestellt.

#### § 3 Stellung im Rechtsverkehr

(1) Das Sondervermögen ist rechtlich unselbstständig und nicht rechtsfähig.

(2) Das Sondervermögen ist vom übrigen Vermögen des Landes, seinen Rechten und Verbindlichkeiten getrennt zu halten.

#### § 4 Zuführung zum Sondervermögen

(1) Das Sondervermögen wird aus Mitteln aus dem Landeshaushalt gebildet.

(2) Die Zuführung erfolgt auf der Grundlage des jeweiligen Haushaltsgesetzes und des Haushaltsplans.

#### § 5 Verwendung des Sondervermögens, Wirtschaftsplan und Berichterstattung

(1) Für die zweckentsprechende Verwendung der Mittel des Sondervermögens ist der Wirtschaftsplan des Sondervermögens maßgeblich.
Der Wirtschaftsplan als Teil des Haushaltsplans wird vom für Finanzen zuständigen Ministerium aufgestellt.

(2) Entnahmen aus dem Sondervermögen erfolgen zugunsten des Landeshaushalts.
Die entnommenen Mittel sind entsprechend der Festlegungen im Wirtschaftsplan umzusetzen und dem Zweck entsprechend zu verausgaben.

(3) Das für Finanzen zuständige Ministerium berichtet dem für den Haushalt zuständigen Ausschuss des Landtages jährlich zum 31.
Dezember über die Bewilligung und den Abfluss der Mittel aus dem Sondervermögen sowie über die Wirkung und Nachhaltigkeit der einzelnen mit den Mitteln finanzierten Projekte.

#### § 6 Jahresrechnung

(1) Das für Finanzen zuständige Ministerium stellt für jedes Haushaltsjahr die Jahresrechnung des Sondervermögens auf.
Diese wird der Haushaltsrechnung des Landes beigefügt.

(2) In der Jahresrechnung sind der Bestand des Sondervermögens sowie die Einnahmen und Ausgaben nachzuweisen.

#### § 7 Inkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 13.
Dezember 2019

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke