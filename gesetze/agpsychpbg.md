## Gesetz zur Ausführung des Gesetzes über die psychosoziale Prozessbegleitung im Strafverfahren (AGPsychPbG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Anerkennung von psychosozialen Prozessbegleiterinnen und psychosozialen Prozessbegleitern

Als psychosoziale Prozessbegleiterin oder psychosozialer Prozessbegleiter soll anerkannt werden, wer

1.  über die in § 3 des Gesetzes über die psychosoziale Prozessbegleitung im Strafverfahren genannten Qualifikationen verfügt,
2.  eine in der Regel mindestens zweijährige praktische Berufserfahrung in einem der unter § 3 Absatz 2 Satz 1 Nummer 1 des Gesetzes über die psychosoziale Prozessbegleitung im Strafverfahren genannten Bereiche nachweisen kann,
3.  über die erforderliche persönliche Zuverlässigkeit verfügt und
4.  an eine im Land Brandenburg ansässige Opferschutzeinrichtung angebunden ist.

#### § 2 Anerkennung von Aus- oder Weiterbildungen

(1) Eine Aus- oder Weiterbildung nach § 3 Absatz 2 Satz 1 Nummer 2 des Gesetzes über die psychosoziale Prozessbegleitung im Strafverfahren soll anerkannt werden, wenn

1.  der Aus- oder Weiterbildung ein geeignetes didaktisches und methodisches Konzept zu Grunde liegt,
2.  die Veranstaltungsform sowie ihre Dauer und die Teilnehmerzahl so bemessen ist, dass die angestrebten Lernziele erreicht werden können,
3.  die in der Aus- oder Weiterbildung vermittelten Inhalte die Teilnehmer befähigen, selbstständig fachlich adäquate psychosoziale Prozessbegleitung unter Einhaltung der den §§ 2 und 3 des Gesetzes über die psychosoziale Prozessbegleitung im Strafverfahren zu Grunde liegenden Standards durchzuführen.

(2) Zu den nach Absatz 1 Nummer 3 zu vermittelnden Inhalten gehören in der Regel mindestens die für die psychosoziale Prozessbegleitung relevanten Kenntnisse

1.  der rechtlichen Grundlagen und Grundsätze des Strafverfahrens sowie weiterer für die Opfer von Straftaten relevanter Rechtsgebiete,
2.  der Viktimologie, insbesondere Kenntnisse zu den besonderen Bedürfnissen spezieller Opfergruppen,
3.  der Psychologie und Psychotraumatologie,
4.  der Theorie und Praxis der psychosozialen Prozessbegleitung und
5.  der Methoden und Standards zur Qualitätssicherung und Eigenvorsorge.

(3) Die Anerkennung kann versagt werden, wenn begründete Zweifel an der fachlichen Qualifikation der in der Aus- oder Weiterbildung eingesetzten Referentinnen und Referenten oder der Zuverlässigkeit des Weiterbildungs- und Kursanbieters bestehen.

#### § 3 Zuständigkeit

Zuständig für die Anerkennungen nach den §§ 1 und 2 ist das für Justiz zuständige Ministerium des Landes Brandenburg.

#### § 4 Antrag

(1) Die Anerkennungen nach den §§ 1 und 2 sind schriftlich bei der für die Anerkennung zuständigen Stelle zu beantragen.

(2) Mit dem Antrag auf Anerkennung als psychosoziale Prozessbegleiterin oder als psychosozialer Prozessbegleiter sind Nachweise vorzulegen, aus denen sich ergibt, dass die in § 1 genannten Anerkennungsvoraussetzungen vorliegen.
Die antragstellende Person hat bei der Meldebehörde ein erweitertes Führungszeugnis nach § 30a Absatz 1 Nummer 1 des Bundeszentralregistergesetzes zur Vorlage bei der für die Anerkennung zuständigen Stelle zu beantragen.

(3) Mit dem Antrag auf Anerkennung einer Aus- oder Weiterbildung sind Nachweise vorzulegen, aus denen sich ergibt, dass die in § 2 Absatz 1 und 2 genannten Anerkennungsvoraussetzungen vorliegen.
Die für die Anerkennung zuständige Stelle kann bei begründeten Zweifeln nach § 2 Absatz 3 Nachweise über die fachliche Qualifikation der in der Aus- oder Weiterbildung eingesetzten Referentinnen und Referenten oder der Zuverlässigkeit des Anbieters verlangen.

#### § 5 Nebenbestimmungen

(1) Die Anerkennung nach § 1 ist auf höchstens fünf Jahre zu befristen.
Im Falle einer gerichtlichen Beiordnung gilt die Anerkennung nach § 1 auch nach Ablauf der in Satz 1 bestimmten Frist für das Verfahren fort, in dem die Beiordnung erfolgt ist.
Eine erneute Anerkennung nach Ablauf einer Befristung ist auf Antrag unter den Voraussetzungen des § 1 möglich.

(2) Die Anerkennungen nach den §§ 1 und 2 können mit Nebenbestimmungen gemäß § 36 des Verwaltungsverfahrensgesetzes in Verbindung mit § 1 Absatz 1 Satz 1 des Verwaltungsverfahrensgesetzes für das Land Brandenburg versehen werden.
Die nachträgliche Aufnahme, Änderung oder Ergänzung von Nebenbestimmungen ist zulässig, sofern dies zur Einhaltung der Anforderungen an die psychosoziale Prozessbegleitung erforderlich ist.

#### § 6 Wegfall von Anerkennungsvoraussetzungen

(1) Die psychosoziale Prozessbegleiterin oder der psychosoziale Prozessbegleiter ist verpflichtet, die zuständige Stelle über den Wegfall von Anerkennungsvoraussetzungen nach § 1 zu unterrichten.
Die zuständige Stelle kann verlangen, dass die psychosoziale Prozessbegleiterin oder der psychosoziale Prozessbegleiter den Nachweis des Fortbestehens der Anerkennungsvoraussetzungen führt.

(2) Der Anbieter der Aus- oder Weiterbildung ist verpflichtet, die zuständige Stelle über grundlegende Änderungen der Ausbildungsinhalte zu unterrichten.

(3) In den Fällen der Absätze 1 und 2 entscheidet die Anerkennungsbehörde über den Fortbestand der nach den §§ 1 und 2 erteilten Anerkennung.

#### § 7 Verzeichnis

(1) Die für die Anerkennung der psychosozialen Prozessbegleiterin oder des psychosozialen Prozessbegleiters zuständige Stelle führt für das Land Brandenburg ein Verzeichnis der nach § 1 anerkannten psychosozialen Prozessbegleiterinnen und psychosozialen Prozessbegleiter.
Dieses Verzeichnis soll folgende Angaben beinhalten: Name, Vorname, Geschlecht, Einrichtung und Erreichbarkeit.
Das Verzeichnis kann an Verletzte ausgehändigt werden.

(2) Auf Antrag kann die verzeichnisführende Stelle örtliche und sachliche Tätigkeitsschwerpunkte der psychosozialen Prozessbegleiterin und des psychosozialen Prozessbegleiters in das Verzeichnis aufnehmen.

#### § 8 Länderübergreifende Anerkennung

(1) Die Anerkennung als psychosoziale Prozessbegleiterin oder als psychosozialer Prozessbegleiter in einem anderen Land der Bundesrepublik Deutschland steht der Anerkennung nach § 1 gleich.
Dies gilt nicht, soweit der örtliche Tätigkeitsschwerpunkt der psychosozialen Prozessbegleiterin oder des psychosozialen Prozessbegleiters dauerhaft in Brandenburg liegt oder dieser nach Brandenburg verlagert wird.

(2) Abweichend von Absatz 1 Satz 1 kann die für die Anerkennung nach § 1 zuständige Stelle im Einzelfall bestimmen, dass eine in einem anderen Land der Bundesrepublik Deutschland anerkannte psychosoziale Prozessbegleiterin oder ein psychosozialer Prozessbegleiter in Brandenburg nicht anerkannt wird, wenn diese oder dieser die in § 1 genannten Voraussetzungen nicht oder nicht mehr erfüllt.

(3) Die Anerkennung einer Aus- oder Weiterbildung in einem anderen Land der Bundesrepublik Deutschland steht der Anerkennung nach § 2 gleich.
Abweichend von Satz 1 kann die für die Anerkennung nach § 2 zuständige Stelle im Einzelfall bestimmen, dass eine in einem anderen Land der Bundesrepublik Deutschland anerkannte Aus- oder Weiterbildung in Brandenburg nicht anerkannt wird, wenn die in § 2 genannten Voraussetzungen nicht oder nicht mehr erfüllt sind.
Die für die Anerkennungen zuständige Stelle hat vor Abschluss des personenbezogenen Anerkennungsverfahrens die Entscheidung über die Anerkennung der Aus- und Weiterbildung herbeizuführen.

#### § 9 Rechtsschutz

Für Streitigkeiten nach diesem Gesetz ist der Verwaltungsrechtsweg gegeben.

#### § 10 Verordnungsermächtigung

Das für Justiz zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung

1.  abweichend von § 3 eine zuständige Stelle für die Anerkennung nach den §§ 1 und 2 zu bestimmen,
2.  Einzelheiten der in § 2 genannten Anerkennungsvoraussetzungen sowie Einzelheiten des Anerkennungsverfahrens zu regeln.

#### § 11 Übergangsregelung

Abweichend von § 1 Nummer 1 können bis zum 31.
Juli 2017 Personen, die eine von einem Land der Bundesrepublik Deutschland anerkannte Aus- oder Weiterbildung im Sinne des Gesetzes über die psychosoziale Prozessbegleitung im Strafverfahren begonnen, aber noch nicht beendet haben, als psychosoziale Prozessbegleiterin oder als psychosozialer Prozessbegleiter nach § 1 anerkannt werden, sofern sie die übrigen in § 1 genannten Voraussetzungen erfüllen.
Die Anerkennung ist bis zum 31.
Juli 2017 zu befristen.

#### § 12 Inkrafttreten

Dieses Gesetz tritt am 1.
Januar 2017 in Kraft.

Potsdam, den 20.
Dezember 2016

Die Präsidentin  
des Landtages Brandenburg

Britta Stark