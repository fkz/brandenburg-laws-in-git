## Gesetz über die Gewährung von Jubiläumsprämien und pauschaliertem Aufwandsersatz, die Verleihung von Medaillen für Treue Dienste und die Stiftung von Ehrenzeichen im Brand- und Katastrophenschutz (Prämien- und Ehrenzeichengesetz - PrämEhrG)

Der Landtag hat das folgende Gesetz beschlossen:

**Inhaltsübersicht**

### Abschnitt 1  
Medaille für Treue Dienste in der Freiwilligen Feuerwehr

[§ 1 Anwendungsbereich für die Verleihung der Medaille für Treue Dienste](#1)

[§ 2 Verleihungsstufen](#2)

[§ 3 Antragsverfahren](#3)

[§ 4 Verleihung und Aushändigung](#4)

[§ 5 Ausführungen der Medaille](#5)

[§ 6 Trageerlaubnis](#6)

### Abschnitt 2  
Jubiläumsprämie Brand- und Katastrophenschutz

[§ 7 Gewährung der Jubiläumsprämie](#7)

[§ 8 Höhe der Jubiläumsprämien](#8)

[§ 9 Antrags- und Bewilligungsverfahren für die Jubiläumsprämie](#9)

[§ 10 Auszahlungsverfahren für die Jubiläumsprämie](#10)

### Abschnitt 3  
Zuschuss zum Aufwandsersatz im Brand- und Katastrophenschutz

[§ 11 Gewährung des Zuschusses zum Aufwandsersatz](#11)

[§ 12 Höhe des Zuschusses zum Aufwandsersatz](#12)

[§ 13 Antrags- und Bewilligungsverfahren für den Zuschuss zum Aufwandsersatz](#13)

[§ 14 Auszahlungsverfahren für den Zuschuss zum Aufwandsersatz](#14)

### Abschnitt 4  
Ehrenzeichen zur Würdigung von Verdiensten  
auf dem Gebiet des Brand- und Katastrophenschutzes

[§ 15 Anwendungsbereich für die Stiftung von Ehrenzeichen zur Würdigung von Verdiensten auf dem Gebiet des Brand- und Katastrophenschutzes](#15)

[§ 16 Verleihungsstufen](#16)

[§ 17 Voraussetzungen für die Verleihung des Ehrenzeichens im Brandschutz](#17)

[§ 18 Voraussetzungen für die Verleihung des Ehrenzeichens im Katastrophenschutz](#18)

[§ 19 Verleihung und Aushändigung](#19)

[§ 20 Ausführungen der Ehrenzeichen im Brandschutz](#20)

[§ 21 Ausführungen der Ehrenzeichen im Katastrophenschutz](#21)

[§ 22 Trageerlaubnis](#22)

[§ 23 Entziehung](#23)

### Abschnitt 5  
Übergangsvorschrift, Inkrafttreten

[§ 24 Übergangsvorschrift](#24)

[§ 25 Inkrafttreten, Außerkrafttreten](#25)

### Abschnitt 1  
Medaille für Treue Dienste in der Freiwilligen Feuerwehr

#### § 1 Anwendungsbereich für die Verleihung der Medaille für Treue Dienste

In Würdigung langjähriger treuer Dienste in der Freiwilligen Feuerwehr wird die Medaille für Treue Dienste in der Freiwilligen Feuerwehr verliehen.

#### § 2 Verleihungsstufen

(1) Die Medaille für Treue Dienste in der Freiwilligen Feuerwehr kann an ehrenamtliche Feuerwehrangehörige verliehen werden, die treu ihre Pflichten in einer Freiwilligen Feuerwehr im Rahmen einer aktiven Dienstzeit erfüllt haben.
Unterbrechungen sind gestattet.

(2) Die Medaille für Treue Dienste in der Freiwilligen Feuerwehr kann in neun Stufen für zehn-, 20-, 30-, 40-, 50-, 60-, 70-, 75- und 80-jährige Zugehörigkeit verliehen werden.

#### § 3 Antragsverfahren

(1) Die Medaille für Treue Dienste kann auf Antrag der Aufgabenträger des örtlichen Brandschutzes verliehen werden.
Antragsberechtigt ist der Aufgabenträger des örtlichen Brandschutzes, in dessen Freiwilliger Feuerwehr der aktive ehrenamtliche Dienst geleistet wird.
Die Aufgabenträger des örtlichen Brandschutzes übernehmen die Verantwortung für die Richtigkeit der Angaben, insbesondere dafür, dass die Angaben über die Dienstzeiten hinreichend belegt sind.

(2) Bewilligungsbehörde ist das für Brandschutz zuständige Ministerium.
Die Bewilligungsbehörde prüft die Vollständigkeit und Plausibilität der Anträge nach Absatz 1 und entscheidet über die Verleihung der Medaille für Treue Dienste nach pflichtgemäßem Ermessen.
Das für Brandschutz zuständige Mitglied der Landesregierung kann die Aufgabe im Wege der Rechtsverordnung auf eine nachgeordnete Behörde oder Einrichtung übertragen.

#### § 4 Verleihung und Aushändigung

(1) Über die Verleihung der Medaille für Treue Dienste in der Freiwilligen Feuerwehr wird eine durch das für Brandschutz zuständige Mitglied der Landesregierung unterzeichnete Urkunde ausgestellt.
Diese Urkunde wird der ausgezeichneten Person ausgehändigt und verbleibt in deren Eigentum.
Bisher verliehene Urkunden behalten ihre Gültigkeit.

(2) Die Medaille für Treue Dienste in der Freiwilligen Feuerwehr geht in das Eigentum der ausgezeichneten Person über.

(3) Zuständig für die Aushändigung der Medaille für Treue Dienste in der Freiwilligen Feuerwehr sind:

1.  die amtsfreien Gemeinden, Ämter und Verbandsgemeinden für eine zehn-, 20-, 30- und 40-jährige aktive Dienstzeit in der Freiwilligen Feuerwehr,
2.  die kreisfreien Städte für eine zehn-, 20-, 30-, 40-, 50-, 60-, 70-, 75- und 80-jährige aktive Dienstzeit in der Freiwilligen Feuerwehr und
3.  die Landkreise für eine 50-, 60-, 70-, 75- und 80-jährige aktive Dienstzeit in der Freiwilligen Feuerwehr.

(4) Die amtsfreien Gemeinden, Ämter und Verbandsgemeinden, kreisfreien Städte und Landkreise senden nicht ausgehändigte Medaillen für Treue Dienste an die Bewilligungsbehörde zurück.

#### § 5 Ausführungen der Medaille

(1) Die Medaille für Treue Dienste in der Freiwilligen Feuerwehr ist rund und für zehnjährige aktive Dienstzeit kupferfarben, für 20-jährige aktive Dienstzeit bronzefarben, für 30-jährige aktive Dienstzeit silberfarben und ab 40-jähriger aktiver Dienstzeit goldfarben.
Auf der Medaille befindet sich ein weißes, gleichschenkeliges Malteserkreuz mit roter Umrandung, das kupfer-, bronze-, silber- oder goldfarbig eingefasst ist.
Zwischen den Kreuzteilen befinden sich zwei Fackeln in gleichfarbiger Ausführung der einzelnen Stufen.
In der Mitte des Kreuzes ist das Landeswappen dargestellt.

(2) Die Medaille für zehn-, 20-, 30- und 40-jährige aktive Dienstzeit zeigt über dem oberen Kreuzschenkel eine Fackelflamme entsprechend der jeweiligen Stufe in kupfer-, bronze-, silber- oder goldfarben.
Die Medaille für 50- und 60-jährige aktive Dienstzeit zeigt über dem oberen Kreuzschenkel die Zahl 50 oder 60 in Gold.
Die Medaille für 70-, 75- oder 80-jährige aktive Dienstzeit zeigt über dem oberen Kreuzschenkel die Zahl 70 oder 75 oder 80 in Rot.

(3) Auf der Rückseite der Medaille für Treue Dienste in der Freiwilligen Feuerwehr befindet sich die Aufschrift „Medaille für Treue Dienste“.

(4) Die Medaille für Treue Dienste in der Freiwilligen Feuerwehr wird an einem Band getragen, und zwar in:

1.  Kupfer für zehnjährige aktive Dienstzeit an einem karmesinroten Band mit zwei marineblauen Streifen,
2.  Bronze für 20-jährige aktive Dienstzeit an einem marineblauen Band, an den Seiten bronzefarben eingefasst, mit einem karmesinroten Streifen in der Mitte,
3.  Silber für 30-jährige aktive Dienstzeit an einem marineblauen Band, an den Seiten silberfarben eingefasst, mit einem karmesinroten Streifen in der Mitte,
4.  Gold für 40-, 50- und 60-jährige aktive Dienstzeit an einem marineblauen Band, an den Seiten goldfarben eingefasst, mit einem karmesinroten Streifen in der Mitte,
5.  Gold für 70-, 75- und 80-jährige aktive Dienstzeit an einem marineblauen Band, an den Seiten karmesinrot eingefasst, mit einem goldfarbenen Streifen in der Mitte.

(5) Zu der Medaille für Treue Dienste in der Freiwilligen Feuerwehr gehört eine Bandschnalle für Uniformen.
Die Bandschnallen der Medaille für Treue Dienste entsprechen dem Band der jeweiligen Stufe und tragen

1.  für zehn-, 20-, 30- und 40-jährige aktive Dienstzeit keine zusätzliche Angabe,
2.  für 50- und 60-jährige aktive Dienstzeit die Zahl 50 oder 60 in Gold in der Mitte und
3.  für 70-, 75- und 80-jährige aktive Dienstzeit die Zahl 70 oder 75 oder 80 in Rot in der Mitte.

#### § 6 Trageerlaubnis

Das Tragen bereits verliehener Medaillen für Treue Dienste in der Freiwilligen Feuerwehr ist weiterhin gestattet.

### Abschnitt 2  
Jubiläumsprämie Brand- und Katastrophenschutz

#### § 7 Gewährung der Jubiläumsprämie

(1) Jubiläumsprämien können ehrenamtlichen Angehörigen der Freiwilligen Feuerwehr gewährt werden, wenn eine aktive Dienstzeit in der Einsatzabteilung einer Freiwilligen Feuerwehr von zehn, 20, 30, 40 oder 50 Jahren vollendet worden ist.

(2) Jubiläumsprämien können auch ehrenamtlichen Mitwirkenden in Einheiten und Einrichtungen des Katastrophenschutzes und des Technischen Hilfswerks im Land Brandenburg gewährt werden, wenn eine entsprechende aktive ehrenamtliche Dienstzeit in Einheiten und Einrichtungen des Katastrophenschutzes oder des Technischen Hilfswerks im Land Brandenburg von zehn, 20, 30, 40 oder 50 Jahren vollendet worden ist.

(3) Ehrenamtliche Mitwirkende in Einheiten und Einrichtungen des Katastrophenschutzes und des Technischen Hilfswerks im Land Brandenburg, die zugleich ehrenamtliche Angehörige einer Freiwilligen Feuerwehr sind, können die Jubiläumsprämie nur entweder für den Einsatzdienst in der Freiwilligen Feuerwehr oder für die aktive Dienstzeit in Einheiten und Einrichtungen des Katastrophenschutzes und des Technischen Hilfswerks im Land Brandenburg erhalten.

(4) Ein Rechtsanspruch auf die Gewährung einer Jubiläumsprämie besteht nicht.

#### § 8 Höhe der Jubiläumsprämien

Die Jubiläumsprämie nach § 7 beträgt bei einer aktiven ehrenamtlichen Dienstzeit von zehn, 20, 30, 40 und 50 Jahren jeweils 500 Euro.

#### § 9 Antrags- und Bewilligungsverfahren für die Jubiläumsprämie

(1) Die Jubiläumsprämie für Angehörige der Freiwilligen Feuerwehr wird gleichzeitig mit dem Antrag auf Verleihung einer Medaille für Treue Dienste gemäß § 3 für zehn-, 20-, 30-, 40- oder 50-jährige aktive Dienstzeit beantragt.
§ 3 Absatz 1 findet Anwendung.
Bewilligungsbehörde ist das für Brandschutz zuständige Ministerium.
Das für Brandschutz zuständige Mitglied der Landesregierung kann die Aufgabe im Wege der Rechtsverordnung auf eine nachgeordnete Behörde oder Einrichtung übertragen.

(2) Die Jubiläumsprämie für ehrenamtliche Mitwirkende in Einheiten und Einrichtungen des Katastrophenschutzes und des Technischen Hilfswerks in Brandenburg kann auf Antrag der unteren Katastrophenschutzbehörden gewährt werden.
Antragsberechtigt ist die untere Katastrophenschutzbehörde, bei dem die oder der ehrenamtliche Mitwirkende registriert ist.
Die unteren Katastrophenschutzbehörden übernehmen die Verantwortung für die Richtigkeit der Angaben, insbesondere dafür, dass die Angaben über die Dienstzeiten hinreichend belegt sind.
Bewilligungsbehörde ist das für Katastrophenschutz zuständige Ministerium.
Das für Katastrophenschutz zuständige Mitglied der Landesregierung kann die Aufgabe im Wege der Rechtsverordnung auf eine nachgeordnete Behörde oder Einrichtung übertragen.

(3) Anträge nach Absatz 2 können auch auf Vorschlag der im Katastrophenschutz des Landes Brandenburg mitwirkenden Hilfsorganisation oder der Bundesanstalt des Technischen Hilfswerks, bei der die oder der ehrenamtliche Mitwirkende die aktive Dienstzeit erbracht hat, erfolgen.
Die Vorschläge des Technischen Hilfswerks sind über die Regionalstellen des Technischen Hilfswerks einzureichen.
Die Vorschläge müssen die für die Antragstellung erforderlichen Daten enthalten.
Die Vorschläge sind bei der gemäß Absatz 2 Satz 2 zuständigen unteren Katastrophenschutzbehörde einzureichen.

(4) Bis zum Ablauf des Jahres 2019 kann die Jubiläumsprämie nach § 8 für eine vor dem 1. Januar 2019 vollendete aktive ehrenamtliche Dienstzeit von 50 Jahren gewährt werden.
Jubiläumsprämien nach § 8, die ab dem 1.
Januar 2019 gewährt werden können, können auch in den nachfolgenden Jahren gewährt werden.
Eine weitergehende Rückwirkung, insbesondere die rückwirkende Gewährung von Jubiläumsprämien für vor dem 1.
Januar 2019 erreichte zehn-, 20-, 30- oder 40-jährigen Jubiläen ist ausgeschlossen.

(5) Die Bewilligungsbehörde prüft die Vollständigkeit und Plausibilität der Anträge nach den Absätzen 1 und 2 und entscheidet über die Gewährung von Jubiläumsprämien nach pflichtgemäßem Ermessen im Rahmen der verfügbaren Haushaltsmittel.

#### § 10 Auszahlungsverfahren für die Jubiläumsprämie

(1) Die amtsfreien Gemeinden, Ämter und Verbandsgemeinden, kreisfreien Städte und Landkreise zahlen die Jubiläumsprämie an die ehrenamtlichen Angehörigen der Freiwilligen Feuerwehren aus.

(2) Zuständig für die Auszahlung der Jubiläumsprämie an die ehrenamtlichen Angehörigen der Freiwilligen Feuerwehren sind:

1.  die amtsfreien Gemeinden, Ämter und Verbandsgemeinden für eine zehn-, 20-, 30- und 40-jährige aktive Dienstzeit,
2.  die kreisfreien Städte für eine zehn-, 20-, 30-, 40- und 50-jährige aktive Dienstzeit und
3.  die Landkreise für eine 50-jährige aktive Dienstzeit.

(3) Die Auszahlung an die ehrenamtlichen Mitglieder der Freiwilligen Feuerwehren soll zeitgleich mit der Aushändigung der Medaille für Treue Dienste für zehn-, 20-, 30-, 40- und 50-jährige aktive Dienstzeit erfolgen.

(4) Die kreisfreien Städte und die Landkreise zahlen die Jubiläumsprämie an die ehrenamtlichen Mitwirkenden in Einheiten und Einrichtungen des Katastrophenschutzes und des Technischen Hilfswerks aus.

(5) Die amtsfreien Gemeinden, Ämter und Verbandsgemeinden, kreisfreien Städte und Landkreise zahlen nicht ausgezahlte Jubiläumsprämien an die Bewilligungsbehörde zurück.

### Abschnitt 3  
Zuschuss zum Aufwandsersatz im Brand- und Katastrophenschutz

#### § 11 Gewährung des Zuschusses zum Aufwandsersatz

(1) Ehrenamtlichen Angehörigen der Freiwilligen Feuerwehren, die im Bezugsjahr einen aktiven Dienst in der Einsatzabteilung einer Freiwilligen Feuerwehr geleistet haben, kann ein Zuschuss zum Aufwandsersatz gewährt werden.

(2) Der Zuschuss zum Aufwandsersatz kann auch ehrenamtlichen Mitwirkenden in Einheiten und Einrichtungen des Katastrophenschutzes oder des Technischen Hilfswerks im Land Brandenburg gewährt werden, wenn diese eine entsprechende aktive ehrenamtliche Dienstzeit in Einheiten und Einrichtungen des Katastrophenschutzes oder des Technischen Hilfswerks im Land Brandenburg geleistet haben.

(3) Ehrenamtliche Mitwirkende in Einheiten und Einrichtungen des Katastrophenschutzes oder des Technischen Hilfswerks, die zugleich ehrenamtliche Angehörige einer Freiwilligen Feuerwehr sind, können den Zuschuss zum Aufwandsersatz nur entweder für den Einsatzdienst in der Freiwilligen Feuerwehr oder für die aktive Dienstzeit in Einheiten und Einrichtungen des Katastrophenschutzes erhalten.

(4) Ein Rechtsanspruch auf die Gewährung des Zuschusses zum Aufwandsersatz besteht nicht.

#### § 12 Höhe des Zuschusses zum Aufwandsersatz

Der Zuschuss zum Aufwandsersatz nach § 11 beträgt jährlich pauschal 200 Euro.

#### § 13 Antrags- und Bewilligungsverfahren für den Zuschuss zum Aufwandsersatz

(1) Der Zuschuss zum Aufwandsersatz für Angehörige der Freiwilligen Feuerwehren kann auf Antrag der Aufgabenträger des örtlichen Brandschutzes gewährt werden.
Antragsberechtigt ist der Aufgabenträger des örtlichen Brandschutzes, in dessen Freiwilliger Feuerwehr der aktive ehrenamtliche Dienst geleistet wird.
Die Aufgabenträger des örtlichen Brandschutzes übernehmen die Verantwortung für die Richtigkeit der Angaben.
Bewilligungsbehörde ist das für Brandschutz zuständige Ministerium.
Das für Brandschutz zuständige Mitglied der Landesregierung kann die Aufgabe im Wege der Rechtsverordnung auf eine nachgeordnete Behörde oder Einrichtung übertragen.

(2) Auf das Antragsverfahren für den Zuschuss zum Aufwandsersatz für ehrenamtliche Mitwirkende in Einheiten und Einrichtungen des Katastrophenschutzes und des Technischen Hilfswerks im Land Brandenburg findet § 9 Absatz 2, 3 und 5 entsprechende Anwendung.
Bewilligungsbehörde ist das für Katastrophenschutz zuständige Ministerium.
Das für Katastrophenschutz zuständige Mitglied der Landesregierung kann die Aufgabe im Wege der Rechtsverordnung auf eine nachgeordnete Behörde oder Einrichtung übertragen.

(3) Anträge nach Absatz 2 können auch auf Vorschlag der im Katastrophenschutz des Landes Brandenburg mitwirkenden Hilfsorganisationen oder der Bundesanstalt des Technischen Hilfswerks, bei der die oder der ehrenamtliche Mitwirkende die Dienstzeit erbracht hat, erfolgen.
Die Vorschläge des Technischen Hilfswerks sind über die Regionalstellen des Technischen Hilfswerks einzureichen.
Die Vorschläge müssen die für die Antragstellung erforderlichen Daten enthalten.
§ 30 des Brandenburgischen Datenschutzgesetzes gilt entsprechend.

(4) Die Bewilligungsbehörde prüft die Vollständigkeit und Plausibilität der Anträge nach den Absätzen 1 und 2 und entscheidet über die Gewährung der Zuschüsse zum Aufwandsersatz nach pflichtgemäßem Ermessen im Rahmen der verfügbaren Haushaltsmittel.

#### § 14 Auszahlungsverfahren für den Zuschuss zum Aufwandsersatz

(1) Zuständig für die Auszahlung des Zuschusses zum Aufwandsersatz an die ehrenamtlichen Angehörigen der Freiwilligen Feuerwehren sind die amtsfreien Gemeinden, Ämter, Verbandsgemeinden und kreisfreien Städte.

(2) Die kreisfreien Städte und die Landkreise zahlen den Zuschuss zum Aufwandsersatz an die ehrenamtlichen Mitwirkenden in Einheiten und Einrichtungen des Katastrophenschutzes und des Technischen Hilfswerks aus.

(3) Die amtsfreien Gemeinden, Ämter und Verbandsgemeinden, kreisfreien Städte und Landkreise zahlen nicht ausgezahlte Zuschüsse zum Aufwandsersatz an die Bewilligungsbehörde zurück.

### Abschnitt 4  
Ehrenzeichen zur Würdigung von Verdiensten auf dem Gebiet des Brand- und Katastrophenschutzes

#### § 15 Anwendungsbereich für die Stiftung von Ehrenzeichen zur Würdigung von Verdiensten auf dem Gebiet des Brand- und Katastrophenschutzes

Zur Würdigung von Verdiensten auf dem Gebiet des Brand- und Katastrophenschutzes werden Ehrenzeichen gestiftet.

#### § 16 Verleihungsstufen

Die Ehrenzeichen im Brand- und Katastrophenschutz werden jeweils in drei Stufen verliehen:

1.  in Silber am Bande,
2.  in Gold am Bande,
3.  als Sonderstufe in Gold als Steckkreuz.

#### § 17 Voraussetzungen für die Verleihung des Ehrenzeichens im Brandschutz

(1) Das Ehrenzeichen im Brandschutz in Silber am Bande wird an Feuerwehrangehörige verliehen, die besondere Leistungen auf dem Gebiet des Feuerwehrwesens erbracht haben.

(2) Das Ehrenzeichen im Brandschutz in Gold am Bande wird an Feuerwehrangehörige für besonders mutiges und entschlossenes Verhalten im Feuerwehreinsatz verliehen.
Im Einzelfall kann eine Verleihung an verstorbene Feuerwehrangehörige erfolgen.

(3) Das Ehrenzeichen im Brandschutz der Sonderstufe in Gold wird an Feuerwehrangehörige und andere Personen verliehen, die sich in hervorragender Weise um das Feuerwehrwesen verdient gemacht haben und einen entscheidenden Anteil an der landesweiten Entwicklung und Festigung des Brandschutzes haben.

#### § 18 Voraussetzungen für die Verleihung des Ehrenzeichens im Katastrophenschutz

(1) Das Ehrenzeichen im Katastrophenschutz in Silber am Bande wird an Mitwirkende in Einheiten und Einrichtungen des Katastrophenschutzes verliehen, die besondere Leistungen im Katastrophenschutz erbracht haben.

(2) Das Ehrenzeichen im Katastrophenschutz in Gold am Bande wird an Mitwirkende in Einheiten und Einrichtungen des Katastrophenschutzes für besonders mutiges und entschlossenes Verhalten im Einsatzfall verliehen.
Im Einzelfall kann eine Verleihung an verstorbene Mitwirkende in Einheiten und Einrichtungen des Katastrophenschutzes erfolgen.

(3) Das Ehrenzeichen im Katastrophenschutz der Sonderstufe in Gold wird an Mitwirkende in Einheiten und Einrichtungen des Katastrophenschutzes und andere Personen verliehen, die sich in hervorragender Weise um den Katastrophenschutz verdient gemacht haben oder einen entscheidenden Anteil an der landesweiten Entwicklung und Festigung des Katastrophenschutzes haben.

#### § 19 Verleihung und Aushändigung

(1) Die Verleihung und Aushändigung von Ehrenzeichen im Brand- und Katastrophenschutz obliegt dem für Brand- und Katastrophenschutz zuständigen Mitglied der Landesregierung.

(2) Über die Verleihung des Ehrenzeichens im Brand- und Katastrophenschutz wird eine Urkunde ausgestellt.
Diese Urkunde wird der ausgezeichneten Person ausgehändigt und verbleibt in deren Eigentum.

(3) Das Ehrenzeichen geht in das Eigentum der ausgezeichneten Person über.

#### § 20 Ausführungen der Ehrenzeichen im Brandschutz

(1) Das Ehrenzeichen im Brandschutz besteht aus einem weißen gleichschenkeligen Malteserkreuz mit roter Umrandung, das silber- oder goldfarbig eingefasst ist.
Zwischen den Kreuzteilen befinden sich zwei Fackeln in Silber oder Gold und über dem oberen Kreuzschenkel eine silber- oder goldfarbene Fackelflamme.
In der Mitte des Malteserkreuzes ist das Landeswappen dargestellt und silber- oder goldfarben umrandet.

(2) Auf der Rückseite des Ehrenzeichens im Brandschutz befindet sich die Aufschrift: „Für hervorragende Verdienste im Brandschutz“.

(3) Die Ehrenzeichen im Brandschutz in Silber am Bande und in Gold am Bande werden an einem rot-weiß-roten Band, das an den Seiten silber- oder goldfarben eingefasst ist, getragen.
Das Ehrenzeichen im Brandschutz der Sonderstufe in Gold wird als Steckkreuz getragen.

(4) Zu dem Ehrenzeichen im Brandschutz in Silber am Bande und in Gold am Bande gehören eine Bandschnalle für Uniformen und ein Bandsteg.
Die Bandschnalle und der Bandsteg sind mit einem roten Band versehen, auf dem in der Mitte darstellend das verkleinerte Ehrenzeichen im Brandschutz in Silber oder Gold angebracht ist.

#### § 21 Ausführungen der Ehrenzeichen im Katastrophenschutz

(1) Das Ehrenzeichen im Katastrophenschutz besteht aus einem roten gleichschenkeligen Malteserkreuz mit roter Umrandung, das silber- oder goldfarbig eingefasst ist.
Zwischen den vier Kreuzschenkeln befindet sich jeweils silber- oder goldfarbenes Eichenlaub.
In der Mitte des Malteserkreuzes ist das Landeswappen dargestellt und silber- oder goldfarbig umrandet.

(2) Auf der Rückseite des Ehrenzeichens im Katastrophenschutz befindet sich die Aufschrift: „Für hervorragende Verdienste im Katastrophenschutz“.

(3) Die Ehrenzeichen im Katastrophenschutz in Silber am Bande und in Gold am Bande werden an einem rot-weiß-roten Band, das an den Seiten silber- oder goldfarben eingefasst ist, getragen.
Das Ehrenzeichen im Katastrophenschutz der Sonderstufe in Gold wird als Steckkreuz getragen.

(4) Zu dem Ehrenzeichen im Katastrophenschutz in Silber am Bande und in Gold am Bande gehören eine Bandschnalle für Uniformen und ein Bandsteg.
Die Bandschnalle und der Bandsteg sind mit einem roten Band versehen, auf dem in der Mitte darstellend das verkleinerte Ehrenzeichen im Katastrophenschutz in Silber oder Gold angebracht ist.

#### § 22 Trageerlaubnis

Das Tragen anderer verliehener Auszeichnungen aus dem Bereich des Brand- und Katastrophenschutzes ist weiterhin gestattet.

#### § 23 Entziehung

(1) Erweist sich die mit einem Ehrenzeichen im Brand- und Katastrophenschutz ausgezeichnete Person durch ihr späteres Verhalten, insbesondere durch eine entehrende Straftat, der Auszeichnung unwürdig oder wird ein solches Verhalten nachträglich bekannt, kann das für Brand- und Katastrophenschutz zuständige Mitglied der Landesregierung das Ehrenzeichen entziehen.
Das Ehrenzeichen, die Bandschnalle und die Verleihungsurkunde sind in diesem Fall zurückzugeben.

(2) Die betroffene Person ist vor der Entziehung des Ehrenzeichens anzuhören.

(3) Öffentliche Stellen des Landes Brandenburg sind verpflichtet, dem für Brand- und Katastrophenschutz zuständigen Ministerium die für die Prüfung der Auszeichnungswürdigkeit erforderlichen Auskünfte zu erteilen.

### Abschnitt 5  
Übergangsvorschrift, Inkrafttreten

#### § 24 Übergangsvorschrift

(1) Vor dem Inkrafttreten dieses Gesetzes verliehene Medaillen für Treue Dienste gelten weiterhin als ordnungsgemäß verliehen.

(2) Für ehrenamtliche Angehörige der Freiwilligen Feuerwehr, bei denen vor dem Inkrafttreten dieses Gesetzes bereits eine ehrenamtliche Dienstzeit von zehn oder mehr Jahren anerkannt wurde, erfolgt die Berechnung der aktiven ehrenamtlichen Dienstzeit gemäß § 2 Absatz 1 weiterhin unter Einbeziehung dieser in der Vergangenheit anerkannten Dienstzeiten.
Dasselbe gilt in diesem Fall für die Berechnung der aktiven Dienstzeit für die Gewährung einer Jubiläumsprämie gemäß § 7 Absatz 1.

#### § 25 Inkrafttreten, Außerkrafttreten

Dieses Gesetz tritt mit Wirkung vom 1.
Januar 2019 in Kraft.
Gleichzeitig treten das Feuerwehrmedaillengesetz vom 19. Mai 2016 (GVBl.
I Nr. 15) und das Ehrenzeichengesetz vom 18. Oktober 2011 (GVBl.
I Nr. 25), das durch Artikel 2 des Gesetzes vom 19. Mai 2016 (GVBl. I Nr. 15 S. 3) geändert worden ist, außer Kraft.

Potsdam, den 30.
April 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark