## Gesetz zur Ausführung des Staatsvertrages zur Neuregulierung des Glücksspielwesens in Deutschland im Land Brandenburg (Brandenburgisches Glücksspielausführungsgesetz - BbgGlüAG)

### Abschnitt 1  
Allgemeine Vorschriften

#### § 1 Anwendungsbereich

Dieses Gesetz regelt die Ausführung des Staatsvertrages zur Neuregulierung des Glücksspielwesens in Deutschland vom 29. Oktober 2020 (GVBl. I Nr. 6) im Land Brandenburg für öffentliche Lotterien, Ausspielungen und Wettvermittlungsstellen für Sportwetten.

#### § 2 Organisationen und Umfang des staatlichen Glücksspielangebotes

(1) Das Land Brandenburg ist zur Erfüllung der ordnungsrechtlichen Aufgabe gemäß § 10 Absatz 1 des Glücksspielstaatsvertrages 2021, ein ausreichendes Glücksspielangebot sicherzustellen, unbeschadet der Regelungen des Glücksspielstaatsvertrages 2021 und des Abschnitts 4 dieses Gesetzes allein befugt, innerhalb seines Gebietes Glücksspiele zu veranstalten.

(2) Das Land kann die ordnungsrechtliche Aufgabe, ein ausreichendes Glücksspielangebot sicherzustellen, durch die Veranstaltung von Lotterien und Ausspielungen erfüllen.
Das Land kann spielbanktypische Glücksspielangebote nach Maßgabe der Regelungen des Glückspielstaatsvertrages 2021 und des Spielbankgesetzes veranstalten.

(3) Das Land kann die ordnungsrechtliche Aufgabe, Glücksspiele zu veranstalten, selbst, durch eine von allen Vertragsländern des Glücksspielstaatsvertrages 2021 gemeinsam geführte öffentliche Anstalt oder durch juristische Personen des öffentlichen Rechts oder privatrechtliche Gesellschaften, an denen das Land Brandenburg allein oder gemeinschaftlich mit den anderen Ländern beteiligt ist, erfüllen.
Im Bereich der Klassenlotterien gilt § 10 Absatz 3 des Glücksspielstaatsvertrages 2021.

#### § 3 Erlaubnis

(1) Veranstalterinnen und Veranstalter von Glücksspielen, Annahmestellen, Wettvermittlungsstellen, Lotterieeinnehmerinnen und Lotterieeinnehmer und gewerbliche Spielvermittlerinnen und Spielvermittler bedürfen für die Veranstaltung und die Vermittlung von Glücksspielen der Erlaubnis.
Die Erlaubnis zum Veranstalten und Vermitteln von Glücksspielen im Sinne des § 4 Absatz 1 Satz 1 des Glücksspielstaatsvertrages 2021 darf nur erteilt werden, wenn

1.  die Ziele des § 1 des Glücksspielstaatsvertrages 2021 nicht entgegenstehen,
2.  die Einhaltung der Regelungen des Glücksspielstaatsvertrages 2021 sichergestellt ist,
3.  die Veranstalterin, der Veranstalter, die Vermittlerin oder der Vermittler zuverlässig ist, insbesondere die Gewähr dafür bietet, dass die Veranstaltung und die Vermittlung ordnungsgemäß und für die Spielteilnehmerinnen und die Spielteilnehmer nachvollziehbar durchgeführt werden,
4.  bei der Einführung neuer Glücksspielangebote und bei der Einführung neuer oder der erheblichen Erweiterung bestehender Vertriebswege den Anforderungen des § 9 Absatz 5 des Glücksspielstaatsvertrages 2021 genügt ist.

(2) Die Erlaubnis für das Vermitteln öffentlicher Glücksspiele setzt eine Erlaubnis für die Veranstaltung dieser Glücksspiele durch die zuständigen Behörden des Landes Brandenburg oder durch die nach § 9a des Glücksspielstaatsvertrages 2021 zuständige Behörde voraus.

(3) In der Erlaubnis sind neben den Regelungen nach § 9 Absatz 4 des Glücksspielstaatsvertrages 2021 festzulegen:

1.  die Veranstalterin, der Veranstalter, die Vermittlerin oder der Vermittler einschließlich eingeschalteter dritter Personen,
2.  das veranstaltete oder vermittelte Glücksspiel,
3.  die Form des Vertriebs,
4.  Art, Ort oder Gebiet sowie Beginn und Dauer der Veranstaltung oder Vermittlung,
5.  bei Lotterieveranstaltungen der Spielplan,
6.  bei Vermittlungen die Veranstalterin oder der Veranstalter, an den zu vermitteln ist.

(4) Der Erlaubnis bedürfen auch die Teilnahmebedingungen.
In den Teilnahmebedingungen sind insbesondere Bestimmungen zu treffen über die

1.  Voraussetzungen, unter denen ein Spiel- oder Wettvertrag zustande kommt,
2.  Gewinnpläne und Ausschüttungsquoten,
3.  Kosten für die Teilnahme an einem Glücksspiel,
4.  Frist, innerhalb der ein Gewinnanspruch geltend gemacht werden kann,
5.  Bekanntmachung der Gewinnentscheide und der Auszahlung der Gewinne und
6.  Verwendung der Gewinne, auf die ein Anspruch nicht fristgerecht geltend gemacht worden ist.

Die Erlaubnis kann bestimmen, dass die Ziehung

1.  unter Aufsicht der Erlaubnisbehörde stattfindet oder
2.  unter Aufsicht einer Notarin oder eines Notars oder einer von der Erlaubnisbehörde bestimmten Vertrauensperson stattfindet und die Veranstalterin oder der Veranstalter ein Protokoll über die Ziehung bei der zuständigen Behörde einreicht.

(5) In der Erlaubnis zum Veranstalten eines öffentlichen Glücksspiels wird die Veranstalterin oder der Veranstalter von Glücksspielen im Land Brandenburg nach § 2 Absatz 3 zur Zahlung einer Glücksspielabgabe an das Land Brandenburg oder zur zweckentsprechenden Verwendung des Ertrages verpflichtet.
Die Glücksspielabgabe beträgt 12,5 Prozent bei Sofortlotterien, im Übrigen 20 Prozent der Spieleinsätze.
Das für Inneres zuständige Ministerium kann im Einvernehmen mit dem für Finanzen zuständigen Ministerium eine abweichende Glücksspielabgabe im Erlaubnisbescheid festlegen.
Die Glücksspielabgabe wird im Landeshaushalt vereinnahmt; ein angemessener Anteil des Aufkommens dient der Finanzierung der Suchtprävention, Suchtberatung und gemeinnützigen Zwecken.

(6) Die Absätze 1 bis 5 finden bei ländereinheitlichen Verfahren nach § 9a des Glücksspielstaatsvertrages 2021 keine Anwendung.

#### § 4 Annahmestellen, Wettvermittlungsstellen

(1) Eine Erlaubnis für den Betrieb einer Annahmestelle oder einer Wettvermittlungsstelle darf nicht für Räumlichkeiten erteilt werden, die nach ihrer Art, Lage, Beschaffenheit, Ausstattung und Einteilung den Zielen des § 1 des Glücksspielstaatsvertrages 2021 entgegenstehen.
Die Vermittlung von Lotterien außerhalb der in Satz 1 aufgeführten Räumlichkeiten durch den Veranstalter nach § 10 Absatz 2 des Glücksspielstaatsvertrages 2021 und dessen Annahmestellen ist zulässig.
Der Betrieb einer Annahmestelle oder einer Wettvermittlungsstelle ist unzulässig, wenn

1.  sie als Vergnügungsstätte ausgestaltet ist,
2.  sie in unmittelbarer Nähe zu Vergnügungsstätten, insbesondere Gaststätten, Spielhallen und Spielbanken oder Anlagen für sportliche Zwecke belegen ist,
3.  sie in unmittelbarer Nähe zu einer Einrichtung, die ihrer Art nach oder tatsächlich vorwiegend von Kindern und Jugendlichen besucht wird, belegen ist,
4.  alkoholische Getränke zum Verzehr an Ort und Stelle ausgeschenkt werden.

(2) Der Betrieb einer Wettvermittlungsstelle ist weiter unzulässig, wenn der Abstand zu einer anderen Wettvermittlungsstelle 500 Meter Luftlinie unterschreitet.
Eine Wettvermittlungsstelle darf von außen nicht einsehbar sein.

(3) Von der äußeren Gestaltung der Wettvermittlungsstelle und in ihrer unmittelbaren Nähe darf keine Werbung für den Wettbetrieb oder die in der Wettvermittlungsstelle angebotenen Wetten ausgehen oder durch eine besonders auffällige Gestaltung ein zusätzlicher Anreiz für den Wettbetrieb geschaffen werden.

(4) Die Anzahl der Annahmestellen im Sinne des § 3 Absatz 5 des Glücksspielstaatsvertrages 2021 wird auf 720 Annahmestellen im Land Brandenburg begrenzt.

(5) Eine Erlaubnis zum Betreiben einer Annahmestelle oder einer Wettvermittlungsstelle darf nicht erteilt werden, wenn Tatsachen die Annahme rechtfertigen, dass die Betreiberin oder der Betreiber die für diese Tätigkeit erforderliche Zuverlässigkeit nicht besitzt.

(6) Der Antrag auf Erlaubnis zum Betreiben einer Annahmestelle oder einer Wettvermittlungsstelle kann nur von der Veranstalterin oder dem Veranstalter gestellt werden.
Der Eigenbetrieb von Annahmestellen durch den Veranstalter nach § 10 Absatz 2 des Glücksspielstaatsvertrages 2021 ist zulässig.

#### § 5 Gewerbliche Spielvermittlung

(1) Wer im Land Brandenburg öffentliche Glücksspiele gewerblich vermitteln will, bedarf unbeschadet sonstiger Anzeigepflichten einer Erlaubnis nach § 3 oder einer Erlaubnis der nach § 19 Absatz 2 des Glücksspielstaatsvertrages 2021 zuständigen Behörde.
Die Vermittlung darf nur an die Veranstalterinnen oder Veranstalter erfolgen, die über eine Veranstaltererlaubnis der zuständigen Behörde des Landes Brandenburg oder der nach § 9a des Glückspielstaatsvertrages 2021 zuständigen Behörde verfügen.

(2) Gewerbliche Spielvermittlung in örtlichen Geschäftslokalen ist unzulässig.

### Abschnitt 2  
Jugendschutz, Suchtprävention, Suchtberatung und Suchtforschung

#### § 6 Sicherstellung des Jugendschutzes

Das Veranstalten und das Vermitteln von öffentlichen Glücksspielen dürfen den Erfordernissen des Jugendschutzes nicht zuwiderlaufen.
Die Teilnahme von Minderjährigen ist unzulässig.
Dieser Sicherstellungspflicht haben die Veranstalterin und der Veranstalter, die Vermittlerin und der Vermittler jeweils für ihre Verantwortungssphäre zu genügen.
Bei unmittelbar an die Spielteilnehmerin oder den Spielteilnehmer gerichteten Angeboten trifft die Veranstalterin und den Veranstalter, die Vermittlerin und den Vermittler diese Sicherstellungspflicht; beim Vertrieb öffentlicher Glücksspiele durch Annahmestellen oder Wettvermittlungsstellen hat die Veranstalterin oder der Veranstalter den Ausschluss der Teilnahme Jugendlicher im Rahmen der Organisations- und Direktionspflichten zu gewährleisten.
Testkäufe oder Testspiele mit minderjährigen Personen durch die Glücksspielaufsichtsbehörde sind nur zulässig, wenn hinreichender Verdacht besteht, dass die Veranstalterin, der Veranstalter, die Vermittlerin oder der Vermittler von öffentlichen Glücksspielen nicht alle angemessenen und zumutbaren Maßnahmen des Jugendschutzes ergriffen haben.

#### § 7 Suchtprävention und Suchtberatung

(1) Das Land beteiligt sich an der Finanzierung von Suchtprävention und Suchtberatung zur Vermeidung und Bekämpfung der Glücksspielsucht.

(2) Die besonderen Gefahren des Online-Glücksspiels sind bei der Suchtprävention und Suchtberatung zur Vermeidung und Bekämpfung der Glücksspielsucht zu beachten.

#### § 8 Suchtforschung

(1) Das Land finanziert Projekte zur Erforschung der Glücksspielsucht.
Zur Erfüllung dieser Aufgabe kann das Land mit anderen Ländern gemeinsame Projekte fördern.

(2) Veranstalter von Glücksspielen im Land Brandenburg nach § 2 Absatz 3 sind berechtigt und auf Verlangen der Glücksspielaufsichtsbehörde auch verpflichtet, Daten im Sinne des § 23 des Glücksspielstaatsvertrages 2021 in anonymisierter Form für Zwecke der Glücksspielforschung zur Verfügung zu stellen.

### Abschnitt 3  
Lotterien und Ausspielungen mit geringerem Gefährdungspotential und kleine Lotterien und Ausspielungen

#### § 9 Lotterien und Ausspielungen mit geringerem Gefährdungspotential

Bei Lotterien und Ausspielungen mit geringerem Gefährdungspotential richten sich die Erteilung sowie Form und Inhalt der Erlaubnis nach den §§ 12 bis 17 des Glücksspielstaatsvertrages 2021.

#### § 10 Kleine Lotterien und Ausspielungen

(1) Die Erlaubnis für die Veranstaltung einer kleinen Lotterie oder Ausspielung kann für solche Veranstaltungen allgemein erteilt werden,

1.  bei denen die Summe der zu entrichtenden Entgelte den Betrag von 40 000 Euro nicht übersteigt und
2.  bei denen der Losverkauf die Dauer von drei Monaten nicht überschreitet.

Die allgemeine Erlaubnis nach Satz 1 kann abweichend von den §§ 4 bis 8, 12 Absatz 1, §§ 13, 14 Absatz 1 Satz 1 Nummer 1, §§ 15 bis 17 des Glücksspielstaatsvertrages 2021 erteilt werden.
Der Reinertrag und die Gewinnsumme müssen jeweils mindestens ein Drittel der Entgelte betragen.

(2) In der allgemeinen Erlaubnis ist zu bestimmen, dass bei den Veranstaltungen, bei denen Lose ausgegeben werden sollen, die den sofortigen Gewinnentscheid enthalten, Prämien- oder Schlussziehungen nicht vorgesehen werden dürfen.

(3) Die allgemeine Erlaubnis ist zu befristen.
Sie begründet die Pflicht, die vorgesehene Veranstaltung mindestens zwei Wochen vor Beginn der zuständigen Behörde und dem für den Veranstalter zuständigen Finanzamt schriftlich anzuzeigen.

#### § 11 Maßnahmen bei kleinen Lotterien und Ausspielungen

(1) Für kleine Lotterien und Ausspielungen können von der zuständigen Ordnungsbehörde im Einzelfall Auflagen erlassen werden.

(2) Im Einzelfall kann eine kleine Lotterie oder Ausspielung untersagt werden, wenn

1.  gegen die Vorschriften dieses Gesetzes oder gegen den Glücksspielstaatsvertrag 2021 oder gegen wesentliche Bestimmungen der allgemeinen Erlaubnis verstoßen wird,
2.  die Gefahr besteht, dass durch die Verwendung des Reinertrages die öffentliche Sicherheit oder Ordnung verletzt wird, oder
3.  keine Gewähr für die ordnungsgemäße Durchführung der kleinen Lotterie oder Ausspielung oder für die zweckentsprechende Verwendung des Reinertrages gegeben ist.

### Abschnitt 4  
Glücksspielaufsicht

#### § 12 Erlaubnisbehörden

(1) Zuständig für die Erteilung einer Erlaubnis zur Veranstaltung eines Glücksspiels sind

1.  die amtsfreien Gemeinden, die kreisfreien Städte, die Ämter, die Verbandsgemeinden, die mitverwaltenden Gemeinden und die mitverwalteten Gemeinden als örtliche Ordnungsbehörden, wenn die Veranstaltung innerhalb der Gebietsgrenzen dieser Körperschaften stattfindet,
2.  die Landkreise als Kreisordnungsbehörden, wenn die Veranstaltung in mehreren kreisangehörigen amtsfreien Gemeinden, Ämtern, Verbandsgemeinden, mitverwaltenden Gemeinden oder mitverwalteten Gemeinden stattfindet,
3.  das für Inneres zuständige Ministerium, wenn die Veranstaltung in mehreren Landkreisen oder kreisfreien Städten stattfindet,
4.  das für Inneres zuständige Ministerium, wenn die Veranstaltung landesweit oder in mehreren Ländern stattfindet.

(2) Zuständig für alle anderen Veranstaltungen und für die allgemeine Erlaubnis nach § 10 ist das für Inneres zuständige Ministerium.

(3) Zuständig für die Erteilung der Erlaubnis für Annahmestellen, Wettvermittlungsstellen, Lotterieeinnehmerinnen, Lotterieeinnehmer, gewerbliche Spielvermittlerinnen und gewerbliche Spielvermittler ist das für Inneres zuständige Ministerium.

(4) Absatz 1 Nummer 4, Absatz 2 erster Halbsatz und Absatz 3 für Lotterieeinnehmerinnen, Lotterieeinnehmer, gewerbliche Spielvermittlerinnen und gewerbliche Spielvermittler finden bei Erlaubnissen nach dem ländereinheitlichen Verfahren nach § 9a des Glücksspielstaatsvertrages 2021 und nach dem Verfahren nach § 19 Absatz 2 des Glücksspielstaatsvertrages 2021 keine Anwendung.

#### § 13 Glücksspielaufsichtsbehörden

(1) Für Maßnahmen gegen unerlaubte Glücksspiele, die innerhalb der Gebietsgrenzen einer amtsfreien Gemeinde, einer kreisfreien Stadt, eines Amtes, einer Verbandsgemeinde, einer mitverwaltenden Gemeinde oder einer mitverwalteten Gemeinde veranstaltet oder vermittelt werden, sowie die Werbung hierfür, sind die örtlichen Ordnungsbehörden zuständig.
Dies gilt auch für unerlaubte Glücksspiele im Internet, die in örtlichen Geschäftslokalen angeboten werden.
Für Maßnahmen gegen unerlaubte Glücksspiele, die in mehreren kreisangehörigen amtsfreien Gemeinden, Ämtern, Verbandsgemeinden, mitverwaltenden Gemeinden oder mitverwalteten Gemeinden veranstaltet oder vermittelt werden, sowie die Werbung hierfür, sind die Kreisordnungsbehörden zuständig.

(2) Die Überwachung der ordnungsgemäßen Durchführung erlaubter Glücksspiele nehmen die Behörden wahr, die die Erlaubnis erteilt haben.
Wird das Glücksspiel aufgrund einer allgemeinen Erlaubnis nach § 10 veranstaltet, gilt Satz 1 entsprechend.

(3) Ist eine örtliche Ordnungsbehörde oder eine Kreisordnungsbehörde nicht zuständig, liegt die Zuständigkeit bei dem für Inneres zuständigen Ministerium.

(4) Absatz 3 findet bei Aufsichtsmaßnahmen nach dem ländereinheitlichen Verfahren nach § 9a des Glücksspielstaatsvertrages 2021 und nach dem Verfahren nach § 19 Absatz 2 des Glücksspielstaatsvertrages 2021 keine Anwendung.

### Abschnitt 5  
Schlussbestimmungen

#### § 14 Ordnungswidrigkeiten

(1) Ordnungswidrig handelt, wer vorsätzlich oder fahrlässig

1.  entgegen § 4 Absatz 1 des Glücksspielstaatsvertrages 2021 ohne Erlaubnis ein Glücksspiel veranstaltet oder vermittelt,
2.  entgegen § 4 Absatz 3 Satz 2 und 3 des Glücksspielstaatsvertrages 2021 Minderjährige an Glücksspielen teilnehmen lässt,
3.  entgegen § 5 Absatz 7 des Glücksspielstaatsvertrages 2021 für unerlaubte Glücksspiele wirbt,
4.  entgegen § 9 Absatz 1 Satz 3 Nummer 1 des Glücksspielstaatsvertrages 2021 die erforderlichen Auskünfte nicht oder nicht vollständig erteilt oder verlangte Unterlagen und Nachweise nicht vorlegt oder das Betreten der Geschäftsräume und -grundstücke verwehrt,
5.  entgegen § 10 Absatz 1 eine kleine Lotterie veranstaltet oder eine gemäß § 11 Absatz 2 untersagte Veranstaltung durchführt,
6.  entgegen § 10 Absatz 3 die Veranstaltung einer kleinen Lotterie den zuständigen Behörden nicht oder nicht rechtzeitig schriftlich anzeigt oder gegen erteilte Auflagen (§ 11 Absatz 1) verstößt,
7.  gegen Bestimmungen oder Nebenbestimmungen einer behördlichen Erlaubnis verstößt,
8.  entgegen § 19 des Glücksspielstaatsvertrages 2021 als gewerbliche Spielvermittlerin oder gewerblicher Spielvermittler die für diese Tätigkeit geltenden Anforderungen nicht erfüllt, insbesondere der bestellten Treuhänderin oder dem bestellten Treuhänder die Spielunterlagen, die zur Führung der Geschäfte erforderlichen Unterlagen, die der Durchführung der Veranstaltung dienenden Gegenstände oder den Spielertrag ganz oder teilweise nicht herausgibt, die erforderlichen Auskünfte nicht oder nicht vollständig erteilt oder die zur einstweiligen Fortführung der Veranstaltung erforderlichen Dienstleistungen oder das hierfür erforderliche Personal nicht zur Verfügung stellt sowie nicht mindestens zwei Drittel der vereinnahmten Beträge an die Veranstalterin oder den Veranstalter gemäß § 2 Absatz 3 weiterleitet,
9.  gesperrte Spielerinnen oder gesperrte Spieler an Glücksspielen ohne die erforderliche Identitätskontrolle teilnehmen lässt.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu 500 000 Euro geahndet werden.

(3) Ist eine Ordnungswidrigkeit nach Absatz 1 begangen worden, so können die Gegenstände,

1.  auf die sich die Ordnungswidrigkeit bezieht oder
2.  die durch sie hervorgebracht oder zu ihrer Begehung oder Vorbereitung gebraucht worden oder bestimmt gewesen sind,

eingezogen werden.
Gleiches gilt für die durch die Ordnungswidrigkeit gewonnenen oder erlangten Gelder.
§ 23 des Gesetzes über Ordnungswidrigkeiten in der Fassung der Bekanntmachung vom 19. Februar 1987 (BGBl. I S. 602), das zuletzt durch Artikel 9a des Gesetzes vom 30. März 2021 (BGBl. I S. 448, 458) geändert worden ist, ist anzuwenden.
Der eingezogene Reinertrag ist dem in § 8 Absatz 1 genannten Zweck zuzuführen.

(4) Sachlich zuständige Verwaltungsbehörde im Sinne des § 36 Absatz 1 Nummer 1 des Gesetzes über Ordnungswidrigkeiten ist die für die Erteilung der Erlaubnis zuständige Behörde.
Sachlich zuständig für die Verfolgung und Ahndung unerlaubter Glücksspiele und der Werbung hierfür ist die Ordnungsbehörde nach § 5 des Ordnungsbehördengesetzes; im Übrigen ist das für Inneres zuständige Ministerium zuständig.

#### § 15 Einschränkung von Grundrechten

Durch dieses Gesetz wird das Grundrecht der Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.