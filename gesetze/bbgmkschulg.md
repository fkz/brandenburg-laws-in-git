## Gesetz zur Förderung der Musik- und Kunstschulen im Land Brandenburg (Brandenburgisches Musik- und Kunstschulgesetz - BbgMKSchulG)

Der Landtag hat das folgende Gesetz beschlossen:

### § 1   
Anwendungsbereich

(1) Dieses Gesetz gilt für im Land Brandenburg tätige Musikschulen und Kunstschulen.
Musikschulen im Sinne dieses Gesetzes sind auch Musikschulen, die Bildung in anderen künstlerischen Fachbereichen gemäß § 3 Absatz 4 Nummer 1 vermitteln (verbundene Musik- und Kunstschulen).

(2) Musikschulen und Kunstschulen sind Bildungs- und Kultureinrichtungen, deren Aufgabe es ist, vorrangig Kindern und Jugendlichen eine musische Bildung zu vermitteln, Begabungen zu erkennen und zu fördern sowie auf ein mögliches Studium der Musik oder sonstiger künstlerischer und kunstpädagogischer Fächer vorzubereiten.
Die Musikschulen und Kunstschulen sollen allen Interessierten den Zugang ermöglichen.

(3) Das Land gewährt Musikschulen und Kunstschulen nach Maßgabe der §§ 6 und 9 Förderungen als Beihilfen im Sinne des Artikels 107 Absatz 1 und 3 Buchstabe d des Vertrags über die Arbeitsweise der Europäischen Union unter Beachtung der Voraussetzungen des Kapitels I und des Artikels 53 der Verordnung (EU) Nr.
651/2014 der Kommission vom 17.
Juni 2014 zur Feststellung der Vereinbarkeit bestimmter Gruppen von Beihilfen mit dem Binnenmarkt in Anwendung der Artikel 107 und 108 des Vertrags über die Arbeitsweise der Europäischen Union (ABI.
L 187 vom 26.6.2014, S.
1, 64).

### § 2   
Träger

Träger von Musikschulen und Kunstschulen können insbesondere Gemeinden und Gemeindeverbände oder andere juristische Personen des öffentlichen oder privaten Rechts sein.

### § 3   
Staatliche Anerkennung, Verordnungsermächtigung

(1) Musikschulen sind berechtigt, die Bezeichnung „Anerkannte Musikschule im Land Brandenburg“ zu führen, wenn sie über eine gültige Anerkennung verfügen.
Die Anerkennung wird auf Antrag der Musikschule jeweils befristet auf fünf Jahre von der für Kultur zuständigen obersten Landesbehörde erteilt, wenn die Voraussetzungen nach Absatz 2 erfüllt sind.
Werden darüber hinaus die Voraussetzungen nach Absatz 5 erfüllt, berechtigt die Anerkennung, die Bezeichnung „Anerkannte Musik- und Kunstschule im Land Brandenburg“ zu führen (erweiterte Anerkennung).

(2) Die Anerkennung wird einer Musikschule erteilt, wenn

1.  sie eine kontinuierliche und pädagogisch planmäßige Arbeit gewährleistet,
2.  sie Unterricht von mindestens 150 Unterrichtsstunden pro Woche in folgenden Bereichen anbietet:
    1.  Fachbereich Musikalische Früherziehung/Grundausbildung,
    2.  Einzel- und Gruppenunterricht in der Unter-, Mittel- und Oberstufe mit einem Angebot an Instrumental- und Vokalfächern aus mindestens fünf der folgenden Fachbereiche: Streichinstrumente, Zupfinstrumente, Blasinstrumente, Tasteninstrumente, Schlaginstrumente, Vokalmusik, Popularmusik sowie Tanz/Musical,
    3.  Fachbereiche Ensemble- und Ergänzungsfächer und
    4.  spezielle Talenteförderung,
3.  sie von den angebotenen Fachbereichen gemäß Nummer 2 Buchstabe a bis c drei mit mindestens 10 Prozent und drei mit mindestens 5 Prozent der Unterrichtsstunden pro Woche belegt,
4.  sie auf der Grundlage von Rahmenlehrplänen unterrichtet, die auf der Angebotsstruktur gemäß Nummer 2 Buchstabe a bis d aufbauen,
5.  sie für die Erteilung der Unterrichtsstunden in der Mehrheit Lehrkräfte mit einem berufsqualifizierenden Hochschulabschluss im Fachbereich Musik oder Musikpädagogik oder einen gleichwertigen Abschluss einsetzt,
6.  die von ihr eingesetzten Lehrkräfte regelmäßig, mindestens jedoch alle drei Jahre, an musikpädagogischen Fortbildungen teilnehmen,
7.  sie unter Leitung einer fest angestellten Person steht, die über einen berufsqualifizierenden Hochschulabschluss im Fachbereich Musik oder Musikpädagogik und in der Regel über Berufserfahrungen in der pädagogischen Arbeit verfügt,
8.  sie geeignete Unterrichtsräume und Unterrichtsinstrumentarien vorhält,
9.  sie zur Vermittlung musikalischer Bildung auch Kooperationen mit anderen Bildungseinrichtungen und Trägern kultureller Bildung durchführt sowie
10.  sie geeignete Maßnahmen ergreift, um Menschen mit Behinderungen zugängliche Angebote zu gestalten.

(3) Kunstschulen sind berechtigt, die Bezeichnung „Anerkannte Kunstschule im Land Brandenburg“ zu führen, wenn sie über eine gültige Anerkennung verfügen.
Die Anerkennung wird auf Antrag der Kunstschule jeweils befristet auf fünf Jahre von der für Kultur zuständigen obersten Landesbehörde erteilt, wenn die Voraussetzungen nach Absatz 4 erfüllt sind.

(4) Die Anerkennung wird einer Kunstschule erteilt, wenn sie

1.  30 Unterrichtsstunden pro Woche in den Fachbereichen
    
    1.  Bildende Kunst und
    2.  Angewandte Kunst
    
    sowie in einem der folgenden Fachbereiche mindestens zehn Unterrichtsstunden pro Woche erbringt:
    
    3.  Theater
    4.  Tanz/Musical
    5.  Literatur
    6.  Medien
    7.  Zirkus,
2.  für die Erteilung der Unterrichtsstunden in den Fachbereichen gemäß Nummer 1 in der Mehrheit Lehrkräfte mit einem berufsqualifizierenden künstlerischen oder kulturpädagogischen Hochschulabschluss oder einen gleichwertigen Abschluss oder mit einem ausgewiesenen künstlerischen Schaffensprozess mit nachgewiesener pädagogischer Befähigung einsetzt,
3.  unter Leitung einer fest angestellten Person steht, die über einen berufsqualifizierenden Hochschulabschluss in einem künstlerischen Fachbereich oder in Kulturpädagogik oder in Kulturwissenschaften oder einen gleichwertigen Abschluss sowie über Berufserfahrungen in kulturellen Einrichtungen verfügt, und
4.  die Voraussetzungen gemäß Absatz 2 Nummer 1 und 6 sowie 8, 9 und 10 unter entsprechender Anwendung auf Kunstschulen erfüllt.

(5) Die erweiterte Anerkennung wird einer Musikschule erteilt, wenn sie zusätzlich zu den Voraussetzungen gemäß Absatz 2 die Voraussetzungen gemäß Absatz 4 Nummer 1, 2 und 4 erfüllt.

(6) Das für Kultur zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung die nähere Ausgestaltung der Voraussetzungen gemäß der Absätze 2 und 4 zu regeln.

(7) Wenn die Voraussetzungen für die Erteilung einer Anerkennung gemäß der Absätze 2, 4 oder Absatz 5 nicht mehr vorliegen, kann diese durch die für Kultur zuständige oberste Landesbehörde widerrufen werden.
Die Erteilung einer Anerkennung oder der Eintritt der Rechtsunwirksamkeit einer Anerkennung wird durch die für Kultur zuständige oberste Landesbehörde im Amtsblatt für Brandenburg öffentlich bekannt gemacht.
Der Inhalt der Bekanntmachung gemäß Satz 2 wird zusätzlich auf der Internetseite der für Kultur zuständigen obersten Landesbehörde veröffentlicht.

### § 4   
Ausnahmen, Abweichungen

(1) Von den Voraussetzungen gemäß § 3 Absatz 2 Nummer 1 bis 10 können für Musikschulen im Aufbau Ausnahmen für die Dauer von höchstens drei Jahren zugelassen werden.
Von den Voraussetzungen gemäß § 3 Absatz 4 Nummer 1 bis 4 können für Kunstschulen im Aufbau Ausnahmen für die Dauer von höchstens drei Jahren zugelassen werden.

(2) Von den Voraussetzungen gemäß § 3 Absatz 2 Nummer 2, 3, 6 und 9 können befristet auf zwei Jahre Abweichungen für Musikschulen zugelassen werden, wenn dies aus besonderen Gründen, insbesondere zur Erhaltung einer gleichmäßigen Grundversorgung mit musikalischen Bildungsangeboten im Land, geboten ist.
Steht die Musikschule unter der Leitung einer fest angestellten Person, die die Musikschule zum Stichtag 1.
Januar 2015 bereits zehn Jahre kontinuierlich und ordnungsgemäß geleitet hat, kann die für Kultur zuständige oberste Landesbehörde vom Nachweis eines berufsqualifizierenden Hochschulabschlusses gemäß § 3 Absatz 2 Nummer 7 absehen, wenn die Musikschule in diesem Zeitraum über die Berechtigung zum Führen der Bezeichnung „Anerkannte Musikschule im Land Brandenburg“ gemäß § 4 Absatz 1 des Brandenburgischen Musikschulgesetzes vom 19.
Dezember 2000 (GVBl.
I S.
178), das durch Artikel 3 des Gesetzes vom 22.
April 2003 (GVBl.
I S.
119, 120) geändert worden ist, verfügte.
Satz 1 gilt für die Voraussetzungen gemäß § 3 Absatz 4 Nummer 1 und 4 für Kunstschulen entsprechend.
Die Zulassung der Abweichung kann jeweils innerhalb des Geltungszeitraums der Anerkennung um bis zu zwei Jahre verlängert werden, wenn die besonderen Gründe fortbestehen.

(3) In den Fällen der Absätze 1 und 2 wird die Berechtigung gemäß § 3 Absatz 1 Satz 1 und 3 sowie Absatz 3 Satz 1 mit dem Wort „vorläufig“ verbunden.

### § 5   
Anerkennungsverfahren, Hinzuziehung Dritter

(1) Die für Kultur zuständige oberste Landesbehörde ist berechtigt, zur Durchführung der Verfahren gemäß § 3 Absatz 1 Satz 2 und 3 sowie Absatz 3 Satz 2 eine juristische Person des Privatrechts mit deren Einverständnis zu beleihen, wenn sie die Gewähr für eine sachgerechte Erfüllung der ihr übertragenen Aufgaben bietet und Gegenstand ihrer Tätigkeit nicht zugleich die Wahrnehmung von Interessen antragsberechtiger Musikschulen oder Kunstschulen ist.
Die beliehene juristische Person steht unter der Fachaufsicht der für Kultur zuständigen obersten Landesbehörde.
Die für Kultur zuständige oberste Landesbehörde ist berechtigt, sich bei Durchführung der Verfahren gemäß § 3 Absatz 1 Satz 2 und 3 sowie Absatz 3 Satz 2 zur Prüfung der Voraussetzungen gemäß § 3 Absatz 2, 4 und 5 Dritter zu bedienen.

(2) Die Musikschulen und Kunstschulen dürfen die zur Prüfung der Voraussetzungen gemäß § 3 Absatz 2, 4 und 5 erforderlichen Daten an die für Kultur zuständige oberste Landesbehörde, beauftragte Dritte oder beliehene juristische Personen des privaten Rechts übermitteln.
Die Stellen nach Absatz 1 Satz 1 und 3 dürfen im Rahmen ihrer Berechtigung die erforderlichen Daten bei den Musikschulen und Kunstschulen erheben.
Die für Kultur zuständige oberste Landesbehörde darf die von den Musikschulen und Kunstschulen übermittelten Daten an die Stellen nach Absatz 1 Satz 1 und 3 zu den dort genannten Zwecken übermitteln.

### § 6   
Förderung durch das Land, Verordnungsermächtigung

(1) Die Musikschulen und Kunstschulen werden auf Antrag durch das Land gefördert, wenn sie über eine gültige Anerkennung gemäß § 3 verfügen, ausschließlich und unmittelbar gemeinnützigen Zwecken im Sinne der Abgabenordnung dienen sowie die Bestimmungen der Absätze 3, 4 und 5 sowie des § 9 nicht entgegenstehen.
Bei Musikschulen und Kunstschulen in unmittelbarer Trägerschaft der Gemeinden und Gemeindeverbände ist ein gesonderter Nachweis, dass diese ausschließlich und unmittelbar gemeinnützigen Zwecken im Sinne der Abgabenordnung dienen, nicht erforderlich.

(2) Das Land fördert ab dem Haushaltsjahr 2017 die Musikschulen und die Kunstschulen jährlich insgesamt durch einen Zuschuss in Höhe von 5 127 000 Euro.
Die Höhe der Förderbeträge wird bei Musikschulen für die Fachbereiche gemäß § 3 Absatz 2 Nummer 2 nach der Anzahl der Unterrichtsstunden und der Anzahl der durch die Musikschule vertraglich gebundenen Schülerinnen und Schüler jeweils bezogen auf das dem Förderjahr vorausgegangene Kalenderjahr bemessen.
Satz 2 gilt für Kunstschulen und die Fachbereiche gemäß § 3 Absatz 4 Nummer 1 an Musikschulen mit der Maßgabe entsprechend, dass die Unterrichtsstunden gemäß § 3 Absatz 4 Nummer 1 heranzuziehen sind.
Können durch Musikschulen oder Kunstschulen im Aufbau die Daten gemäß Satz 2 und 3 im Förderjahr nicht vorgelegt werden, ist eine vorläufige Förderung auf der Grundlage einer prognostischen Ermittlung der Daten bezogen auf das Förderjahr zulässig.

(3) Der gemäß Absatz 2 in Verbindung mit der Rechtsverordnung gemäß Absatz 6 ermittelte Förderbetrag darf zusammen mit weiteren Beihilfen gemäß Artikel 107 Absatz 1 des Vertrags über die Arbeitsweise der Europäischen Union nicht mehr als 80 Prozent der beihilfefähigen Kosten der Musikschulen und Kunstschulen gemäß Artikel 53 Nummer 4 und 5 der Verordnung (EU) Nr.
651/2014 erreichen.
Liegt durch den ermittelten Förderbetrag eine Überschreitung vor, reduziert sich der gemäß Absatz 1 Satz 1 bestehende Anspruch auf Förderung der Musikschule oder Kunstschule entsprechend.
Ausnahmsweise ist eine Förderung von bis zu 100 Prozent der beihilfefähigen Kosten möglich, soweit durch die Musikschule oder Kunstschule nachgewiesen werden kann, dass nicht mehr als ein angemessener Gewinn im Sinne der Vorschriften gemäß Artikel 53 Nummer 6 und 7 der Verordnung (EU) Nr.
651/2014 erzielt wird.
Erreicht der gemäß Absatz 2 in Verbindung mit der Rechtsverordnung gemäß Absatz 6 ermittelte Förderbetrag zusammen mit weiteren Beihilfen gemäß Artikel 107 Absatz 1 des Vertrags über die Arbeitsweise der Europäischen Union mehr als 1 000 000 Euro, reduziert sich der Anspruch auf Förderung über die Bestimmungen in den Sätzen 1 und 2 hinausgehend in dem Maße, wie der nach den Methoden gemäß Artikel 53 Nummer 6 und 7 der Verordnung (EU) Nr.
651/2014 ermittelte Beihilfehöchstbetrag überschritten wird.

(4) Einer Musikschule oder einer Kunstschule, die einer Rückforderungsanordnung aufgrund eines früheren Beschlusses der Europäischen Kommission zur Feststellung der Unzulässigkeit einer Beihilfe und ihrer Unvereinbarkeit mit dem Binnenmarkt nicht nachgekommen ist, dürfen keine Beihilfen gewährt werden, ausgenommen sind Beihilferegelungen zur Bewältigung der Folgen bestimmter Naturkatastrophen.
Der Anspruch auf Förderung gemäß Absatz 1 Satz 1 ist unter der Voraussetzung des Satzes 1 ausgeschlossen.

(5) Der Antrag einer Musikschule oder Kunstschule gemäß Absatz 1 Satz 1 ist unbeschadet des Vorliegens der Voraussetzungen gemäß § 3 Absatz 2, 4 und 5 für ein Förderjahr ausgeschlossen, wenn die Musikschule den Antrag auf Anerkennung gemäß § 3 Absatz 1 Satz 2 und 3 oder die Kunstschule den Antrag auf Anerkennung gemäß § 3 Absatz 3 Satz 2 nicht vollständig bis zum 31. Dezember des dem Förderjahr vorausgegangenen Kalenderjahres bei der für Kultur zuständigen obersten Landesbehörde eingereicht hat.

(6) Das für Kultur zuständige Mitglied der Landesregierung hat durch Rechtsverordnung zur Ausführung dieses Gesetzes die Aufteilung des Zuschusses gemäß Absatz 2 Satz 1 zwischen Musikschulen und Kunstschulen einschließlich Fachbereichen gemäß § 3 Absatz 4 Nummer 1 an Musikschulen, die Verteilungsquotienten gemäß Absatz 2 Satz 2 und 3, die Ausschlussfristen für Anträge gemäß Absatz 1 Satz 1 und das nähere Verfahren zur Bemessung der Förderbeträge zu regeln.

### § 7   
Anpassung der Förderung

(1) Ändern sich nach dem Haushaltsjahr 2017 die Personalkosten für die fest angestellten Lehrkräfte an Musikschulen und Kunstschulen aufgrund einer tarifvertraglichen Anpassung der Gehälter, kann sich der anteilige Zuschuss nur in dem Umfang erhöhen, in dem entsprechende Haushaltsmittel zur Verfügung stehen.

(2) Verändert sich die Anzahl sämtlicher jährlich erteilten Unterrichtsstunden und vertraglich gebundenen Schülerinnen und Schüler gegenüber dem Haushaltsjahr 2017 um mehr als 10 Prozent, so verändert sich die Gesamtfördersumme des Landes nach Maßgabe der verfügbaren Haushaltsmittel entsprechend.

### § 8   
Bewilligungsverfahren, Hinzuziehung Dritter

(1) Die für Kultur zuständige oberste Landesbehörde wird ermächtigt, zur Durchführung der Bewilligungsverfahren gemäß § 6 Absatz 2 eine juristische Person des Privatrechts mit deren Einverständnis zu beleihen, wenn sie die Gewähr für eine sachgerechte Erfüllung der ihr übertragenen Aufgaben bietet.
Die beliehene juristische Person steht unter Fachaufsicht der für Kultur zuständigen obersten Landesbehörde.

(2) § 5 Absatz 2 findet hinsichtlich der Verarbeitung der zur Durchführung der Bewilligungsverfahren erforderlichen Daten der Musikschulen und Kunstschulen entsprechende Anwendung.

### § 9   
Finanzierungsbeteiligung der Träger

(1) Die Landesförderung wird einer Gemeinde, einem Gemeindeverband oder einer sonstigen juristischen Person, an der eine Gemeinde oder ein Gemeindeverband mehrheitlich beteiligt ist, als Träger einer Musikschule oder Kunstschule nur gewährt, wenn sich die Gemeinde oder der Gemeindeverband bezogen auf das dem Förderjahr vorausgegangene Kalenderjahr an den Gesamtausgaben für die Musikschule oder Kunstschule angemessen beteiligt hat.

(2) Absatz 1 gilt auch für die Träger, die einen Rechtsanspruch gegenüber einer Gemeinde oder einem Gemeindeverband auf Finanzierung der Musikschule oder Kunstschule haben.

### § 10   
Übergangsregelung

Die auf der Grundlage von § 4 Absatz 1 Satz 1 des Brandenburgischen Musikschulgesetzes vom 19. Dezember 2000 (GVBl.
I S. 178), das durch Artikel 3 des Gesetzes vom 22.
April 2003 (GVBl. I S.119, 120) geändert worden ist, erteilten Berechtigungen gelten als Anerkennungen im Sinne von § 3 Absatz 1 Satz 1 bis zum 31.
Dezember 2014 fort.
Mit Ablauf dieses Stichtages werden sie rechtsunwirksam.

### § 11   
Evaluation

Die für Kultur zuständige oberste Landesbehörde ist verpflichtet, dem Landtag einen Evaluationsbericht über die Umsetzung der mit § 1 Absatz 2 sowie den §§ 3 und 6 verbundenen gesetzgeberischen Zielstellungen und zur Angemessenheit und Wirksamkeit des § 7 bis zum 31. Dezember 2019 zu übermitteln.
Musikschulen und Kunstschulen, die Förderungen aufgrund dieses Gesetzes erhalten, sind verpflichtet, für die Evaluation erforderliche statistische Daten der für Kultur zuständigen obersten Landesbehörde oder von ihr beauftragten Dritten zu übermitteln.
Zu diesem Zweck kann der Bescheid über die Förderung gemäß § 6 Absatz 2 mit Nebenbestimmungen verbunden werden.

### § 12   
Einschränkung eines Grundrechts

Durch § 5 Absatz 2 und § 8 Absatz 2 wird das Grundrecht auf Datenschutz (Artikel 11 der Verfassung des Landes Brandenburg) eingeschränkt.

### § 13   
Inkrafttreten, Außerkrafttreten

Dieses Gesetz tritt am 1.
Januar 2014 in Kraft.
Gleichzeitig tritt das Brandenburgische Musikschulgesetz vom 19. Dezember 2000 (GVBl.
I S. 178), das durch Artikel 3 des Gesetzes vom 22.
April 2003 (GVBl.
I S. 119, 120) geändert worden ist, außer Kraft.

Potsdam, den 11.
Februar 2014

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch