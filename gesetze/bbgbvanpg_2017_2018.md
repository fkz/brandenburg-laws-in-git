## Brandenburgisches Besoldungs- und Versorgungsanpassungsgesetz 2017/2018 (BbgBVAnpG 2017/2018)

#### § 1 Geltungsbereich

(1) Dieses Gesetz gilt für die

1.  Beamtinnen und Beamten des Landes, der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts,
2.  Richterinnen und Richter des Landes,
3.  Versorgungsempfängerinnen und Versorgungsempfänger, denen laufende Versorgungsbezüge zustehen, die das Land, eine Gemeinde, ein Gemeindeverband oder eine der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten oder Stiftungen des öffentlichen Rechts zu tragen hat.

(2) Dieses Gesetz gilt nicht für Ehrenbeamtinnen und Ehrenbeamte sowie für ehrenamtliche Richterinnen und Richter.

(3) Dieses Gesetz gilt nicht für öffentlich-rechtliche Religionsgesellschaften und ihre Verbände.

#### § 2 Anpassung der Besoldung im Jahr 2017

(1) Die nachfolgenden Dienstbezüge und sonstigen Bezüge werden ab 1. Januar 2017 um 2,45 Prozent erhöht:

1.  die Grundgehaltssätze,
2.  der Familienzuschlag,
3.  die Amtszulagen sowie die allgemeine Stellenzulage nach Vorbemerkung Nummer 13 der Besoldungsordnungen A und B der Anlage 1 des Brandenburgischen Besoldungsgesetzes.

(2) Absatz 1 gilt entsprechend für

1.  die Leistungsbezüge nach § 30 Absatz 1 des Brandenburgischen Besoldungsgesetzes in Verbindung mit § 31 Absatz 2 Satz 3, § 32 Satz 5 und § 33 Satz 6 des Brandenburgischen Besoldungsgesetzes,
2.  die Beträge nach § 4 Absatz 1 und 3 der Verordnung über die Gewährung von Mehrarbeitsvergütung für Beamte in der am 31.
    August 2006 geltenden Fassung,
3.  die in § 2 Absatz 1 Nummer 6 bis 8 und 10 des Brandenburgischen Besoldungs- und Versorgungsanpassungsgesetzes 2008 genannten Bezüge.

(3) Die Anwärtergrundbeträge werden ab 1.
Januar 2017 um 35 Euro erhöht.

#### § 3 Anpassung der Besoldung im Jahr 2018

(1) Ab 1.
Januar 2018 werden die in § 2 Absatz 1 und 2 genannten Bezüge um 2,85 Prozent erhöht.

(2) Die Anwärtergrundbeträge werden ab 1.
Januar 2018 um 35 Euro erhöht.

#### § 4 Rundungsregelung

Bei der Berechnung der nach den §§ 2 und 3 erhöhten Bezüge sind Bruchteile eines Cents unter 0,5 abzurunden und Bruchteile von 0,5 und mehr aufzurunden.

#### § 5 Anpassung der Versorgungsbezüge

(1) Bei Versorgungsempfängerinnen und Versorgungsempfängern gelten die Erhöhungen nach den §§ 2 und 3 entsprechend für die in Artikel 2 § 2 Absatz 1 bis 5 des Bundesbesoldungs- und -versorgungsanpassungsgesetzes 1995 vom 18.
Dezember 1995 (BGBl. I S. 1942), das durch Artikel 61 des Gesetzes vom 19.
Februar 2006 (BGBl. I S. 334, 339) geändert worden ist, genannten Bezügebestandteile sowie für die in § 14 Absatz 2 Satz 1 Nummer 3 und § 84 Absatz 1 Nummer 4, 5 und 7 des Bundesbesoldungsgesetzes in der am 31. August 2006 geltenden Fassung aufgeführten Stellenzulagen und Bezüge.

(2) Versorgungsbezüge, die in festen Beträgen festgesetzt sind, und der Betrag nach Artikel 13 § 2 Absatz 4 des Fünften Gesetzes zur Änderung besoldungsrechtlicher Vorschriften vom 28.
Mai 1990 (BGBl. I S. 967, 976) werden ab 1.
Januar 2017 um 2,35 Prozent und ab 1.
Januar 2018 um 2,75 Prozent erhöht.

(3) Bei Versorgungsempfängerinnen und Versorgungsempfängern, deren Versorgungsbezügen ein Grundgehalt der Besoldungsgruppe A 1 bis A 8 zugrunde liegt, vermindert sich das Grundgehalt ab 1. Januar 2017 um 58,04 Euro und ab 1.
Januar 2018 um 59,69 Euro, wenn ihren ruhegehaltfähigen Dienstbezügen die Stellenzulage nach Vorbemerkung Nummer 27 Absatz 1 Buchstabe a oder b der Bundesbesoldungsordnungen A und B in der am 31.
August 2006 geltenden Fassung bei Eintritt in den Ruhestand nicht zugrunde gelegen hat.

#### § 6 Bekanntmachung

Das Ministerium der Finanzen macht die Beträge der nach den §§ 2 und 3 erhöhten Bezüge im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I durch Neubekanntmachung der Anlagen 4 bis 8 des Brandenburgischen Besoldungsgesetzes und der Anlage 15 des Brandenburgischen Besoldungs- und Versorgungsanpassungsgesetzes 2008 bekannt.

#### § 7 Verfahren der Anpassung für die Jahre 2019 und 2020

Bei der Anpassung der Besoldung und der Versorgung für die Jahre 2019 und 2020 wird das Ergebnis des Abschlusses der Tarifgemeinschaft der deutschen Länder für die Jahre 2019 und 2020 bezüglich der Erhöhung der Entgelte zeit- und wirkungsgleich übernommen.
Darüber hinaus werden die Besoldungs- und die Versorgungsbezüge in diesen beiden Jahren jeweils um weitere 0,5 Prozentpunkte erhöht.