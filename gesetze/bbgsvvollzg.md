## Gesetz über den Vollzug der Unterbringung in der Sicherungsverwahrung im Land Brandenburg (Brandenburgisches Sicherungsverwahrungsvollzugsgesetz -  BbgSVVollzG)

Der Landtag hat das folgende Gesetz beschlossen:

Inhaltsübersicht
----------------

### Abschnitt 1  
Allgemeine Bestimmungen

[§ 1     Anwendungsbereich](#1)  
[§ 2     Ziel und Aufgabe des Vollzugs](#2)  
[§ 3     Grundsätze der Vollzugsgestaltung](#3)  
[§ 4     Stellung der Untergebrachten, Mitwirkung](#4)  
[§ 5     Soziale Hilfe](#5)

### Abschnitt 2  
Aufnahme, Diagnose, Vollzugs- und Eingliederungsplanung

[§ 6     Aufnahmeverfahren](#6)  
[§ 7     Diagnoseverfahren](#7)  
[§ 8     Vollzugs- und Eingliederungsplanung](#8)  
[§ 9     Inhalt des Vollzugs- und Eingliederungsplans](#9)

**Abschnitt 3  
Unterbringung, Verlegung**

[§ 10     Trennungsgrundsätze](#10)  
[§ 11     Unterbringung und Bewegungsfreiheit](#11)  
[§ 12     Wohngruppenvollzug](#12)  
[§ 13     Geschlossener und offener Vollzug](#13)  
[§ 14     Verlegung und Überstellung](#14)

### Abschnitt 4  
Therapeutische Ausgestaltung und Maßnahmen

[§ 15     Therapeutische Ausgestaltung](#15)  
[§ 16     Motivierungsmaßnahmen](#16)  
[§ 17     Sozialtherapeutische Maßnahmen](#17)  
[§ 18     Psychotherapeutische Maßnahmen](#18)  
[§ 19     Psychiatrische Maßnahmen](#19)

### Abschnitt 5  
Arbeitstherapeutische Maßnahmen, Arbeitstraining,  
schulische und berufliche Qualifizierungsmaßnahmen, Arbeit

[§ 20     Arbeitstherapeutische Maßnahmen](#20)  
[§ 21     Arbeitstraining](#21)  
[§ 22     Schulische und berufliche Qualifizierungsmaßnahmen](#22)  
[§ 23     Arbeit](#23)  
[§ 24     Freies Beschäftigungsverhältnis, Selbstbeschäftigung](#24)  
[§ 25     Freistellung von der Arbeit](#25)

### Abschnitt 6  
Besuche, Telefongespräche, Schriftwechsel,  
andere Formen der Telekommunikation und Pakete

[§ 26     Grundsatz](#26)  
[§ 27     Besuch](#27)  
[§ 28     Untersagung der Besuche](#28)  
[§ 29     Durchführung der Besuche](#29)  
[§ 30     Überwachung der Gespräche](#30)  
[§ 31     Telefongespräche](#31)  
[§ 32     Schriftwechsel](#32)  
[§ 33     Untersagung des Schriftwechsels](#33)  
[§ 34     Sichtkontrolle, Weiterleitung und Aufbewahrung von Schreiben](#34)  
[§ 35     Überwachung des Schriftwechsels](#35)  
[§ 36     Anhalten von Schreiben](#36)  
[§ 37     Andere Formen der Telekommunikation](#37)  
[§ 38     Pakete](#38)

### Abschnitt 7  
Vollzugsöffnende Maßnahmen und sonstige Aufenthalte außerhalb der Einrichtung

[§ 39     Vollzugsöffnende Maßnahmen](#39)  
[§ 40     Lockerungen zur Erreichung des Vollzugsziels](#40)  
[§ 41     Lockerungen aus sonstigen Gründen](#41)  
[§ 42     Weisungen für Lockerungen, Zustimmung der Aufsichtsbehörde](#42)  
[§ 43     Ausführungen zur Erreichung des Vollzugsziels](#43)  
[§ 44     Ausführungen aus sonstigen Gründen](#44)  
[§ 45     Außenbeschäftigung](#45)  
[§ 46     Vorführung, Ausantwortung](#46)

### Abschnitt 8  
Vorbereitung der Eingliederung, Entlassung und nachgehende Betreuung

[§ 47     Vorbereitung der Eingliederung](#47)  
[§ 48     Entlassung](#48)  
[§ 49     Nachgehende Betreuung](#49)  
[§ 50     Verbleib oder Aufnahme auf freiwilliger Grundlage](#50)

### Abschnitt 9  
Grundversorgung und Freizeit

[§ 51     Einbringen von Gegenständen](#51)  
[§ 52     Gewahrsam an Gegenständen](#52)  
[§ 53     Ausstattung des Zimmers](#53)  
[§ 54     Aufbewahrung und Vernichtung von Gegenständen](#54)  
[§ 55     Zeitungen und Zeitschriften, religiöse Schriften und Gegenstände](#55)  
[§ 56     Rundfunk, Informations- und Unterhaltungselektronik](#56) [§ 57     Kleidung](#57)  
[§ 58     Verpflegung und Einkauf](#58)  
[§ 59     Freizeit](#59)

### Abschnitt 10  
Vergütung, Gelder der Untergebrachten und Kosten

[§ 60     Vergütung](#60)  
[§ 61     Eigengeld](#61)  
[§ 62     Taschengeld](#62)  
[§ 63     Konten, Bargeld](#63)  
[§ 64     Hausgeld](#64)  
[§ 65     Zweckgebundene Einzahlungen, Eingliederungsgeld](#65)  
[§ 66     Kosten](#66)

### Abschnitt 11  
Gesundheitsfürsorge

[§ 67     Art und Umfang der medizinischen Leistungen, Kostenbeteiligung](#67)  
[§ 68     Durchführung der medizinischen Leistungen, Forderungsübergang](#68)  
[§ 69     Ärztliche Behandlung zur sozialen Eingliederung](#69)  
[§ 70     Gesundheitsschutz und Hygiene](#70)  
[§ 71     Krankenbehandlung während Lockerungen](#71)  
[§ 72     Zwangsmaßnahmen auf dem Gebiet der Gesundheitsfürsorge](#72)  
[§ 73     Benachrichtigungspflicht](#73)

### Abschnitt 12  
Religionsausübung

[§ 74     Seelsorge](#74)  
[§ 75     Religiöse Veranstaltungen](#75)  
[§ 76     Weltanschauungsgemeinschaften](#76)

### Abschnitt 13  
Sicherheit und Ordnung

[§ 77     Grundsatz](#77)  
[§ 78     Allgemeine Verhaltenspflichten, Aufarbeitung von Pflichtverstößen](#78)  
[§ 79     Absuchung, Durchsuchung](#79)  
[§ 80     Sichere Unterbringung](#80)  
[§ 81     Maßnahmen zur Feststellung von Suchtmittelgebrauch](#81)  
[§ 82     Festnahmerecht](#82)  
[§ 83     Besondere Sicherungsmaßnahmen](#83)  
[§ 84     Anordnung besonderer Sicherungsmaßnahmen, Verfahren](#84)  
[§ 85     Ärztliche Überwachung](#85)

### Abschnitt 14  
Unmittelbarer Zwang

[§ 86     Begriffsbestimmungen](#86)  
[§ 87     Allgemeine Voraussetzungen](#87)  
[§ 88     Grundsatz der Verhältnismäßigkeit](#88)  
[§ 89     Androhung](#89)  
[§ 90     Schusswaffengebrauch](#90)

### Abschnitt 15  
Aufhebung von Maßnahmen, Beschwerde

[§ 91     Aufhebung von Maßnahmen](#91)  
[§ 92     Beschwerderecht](#92)

### Abschnitt 16  
Kriminologische Forschung

[§ 93     Evaluation, kriminologische Forschung, Berichtspflicht](#93)

### Abschnitt 17  
Aufbau und Organisation der Einrichtung

[§ 94     Einrichtung](#94)  
[§ 95     Festsetzung der Belegungsfähigkeit, Einzelbelegung](#95)  
[§ 96     Leitung der Einrichtung](#96)  
[§ 97     Bedienstete](#97)  
[§ 98     Seelsorgerin, Seelsorger](#98)  
[§ 99     Medizinische Versorgung](#99)  
[§ 100   Interessenvertretung der Untergebrachten](#100)  
[§ 101   Hausordnung](#101)

### Abschnitt 18  
Aufsicht, Beirat

[§ 102   Aufsichtsbehörde](#102)  
[§ 103   Vollstreckungsplan, Vollzugsgemeinschaften](#103)  
[§ 104   Beirat](#104)

### Abschnitt 19  
Verhinderung von Mobilfunkverkehr

[§ 105   Verbot und Störung des Mobilfunkverkehrs](#105)

### Abschnitt 20  
Datenschutz

[§ 106   Anwendung des Brandenburgischen Polizei-, Justizvollzugs- und Maßregelvollzugsdatenschutzgesetzes](#106)  
[§ 107   Grundsatz, Begriffsbestimmungen](#107)  
[§ 108   Erhebung von Daten über Untergebrachte bei Dritten](#108)  
[§ 109   Erhebung von Daten über andere Personen](#109)  
[§ 110   Unterrichtungspflichten](#110)  
[§ 111   Besondere Formen der Datenerhebung](#111)  
[§ 112   Übermittlung und Nutzung für weitere Zwecke](#112)  
[§ 113   Datenübermittlung an öffentliche Stellen](#113)  
[§ 114   Verarbeitung besonders erhobener Daten](#114)  
[§ 115   Mitteilung über den Aufenthalt im Vollzug](#115)  
[§ 116   Überlassung von Akten](#116)  
[§ 117   Kenntlichmachung in der Einrichtung, Lichtbildausweise](#117)  
[§ 118   Offenbarungspflichten und -befugnisse der Berufsgeheimnisträgerinnen und Berufsgeheimnisträger](#118)  
[§ 119   _(weggefallen)_](#119)  
[§ 120   Auskunft an die Betroffenen, Akteneinsicht](#120)  
[§ 121   Auskunft und Akteneinsicht zur Wahrnehmung der Aufgaben des Europäischen Ausschusses zur Verhütung von Folter und unmenschlicher oder erniedrigender Behandlung oder Strafe](#121)  
[§ 122   Löschung](#122)  
[§ 123   Löschung besonders erhobener Daten](#123)  
[§ 124   Sperrung und Verwendungsbeschränkungen](#124)  
[§ 125   Aufbewahrungsfristen, Fristberechnung](#125)

### Abschnitt 21  
Schlussbestimmungen

[§ 126   Einschränkung von Grundrechten](#126)  
[§ 127   Inkrafttreten](#127)

### Abschnitt 1  
Allgemeine Bestimmungen

§ 1  
**Anwendungsbereich**

Dieses Gesetz regelt den Vollzug der Unterbringung in der Sicherungsverwahrung (Vollzug) in Einrichtungen der Justizverwaltung des Landes Brandenburg.

§ 2  
**Ziel und Aufgabe des Vollzugs**

Der Vollzug dient dem Ziel, die Gefährlichkeit der Untergebrachten für die Allgemeinheit so zu mindern, dass die Vollstreckung der Maßregel möglichst bald zur Bewährung ausgesetzt oder sie für erledigt erklärt werden kann.
Er hat die Aufgabe, die Allgemeinheit vor weiteren Straftaten zu schützen.

§ 3  
**Grundsätze der Vollzugsgestaltung**

(1) Der Vollzug ist auf die Auseinandersetzung der Untergebrachten mit ihrer Gefährlichkeit und deren Folgen auszurichten.

(2) Der Vollzug ist therapiegerichtet und freiheitsorientiert auszugestalten.
Die Untergebrachten sind individuell und intensiv zu betreuen.
Fähigkeiten, die sie für ein selbstbestimmtes Leben in Freiheit und sozialer Verantwortung benötigen, sind zu erhalten und zu fördern.

(3) Das Leben im Vollzug ist den allgemeinen Lebensverhältnissen so weit wie möglich anzugleichen.
Selbst bei langer Dauer der Unterbringung muss den Untergebrachten ein Leben in Würde und weitgehender Selbstbestimmung ermöglicht werden.

(4) Schädlichen Folgen des Freiheitsentzugs ist entgegenzuwirken.

(5) Der Bezug der Untergebrachten zum gesellschaftlichen Leben ist zu wahren und zu fördern.
Personen und Einrichtungen außerhalb des Vollzugs sollen in den Vollzugsalltag einbezogen werden.
Den Untergebrachten ist so bald wie möglich die Teilnahme am Leben in der Freiheit zu gewähren.

(6) Die unterschiedlichen Bedürfnisse der Untergebrachten, insbesondere im Hinblick auf Alter, Geschlecht, Herkunft, Religion, Behinderung und sexuelle Identität, werden bei der Vollzugsgestaltung berücksichtigt.

§ 4  
**Stellung der Untergebrachten, Mitwirkung**

(1) Die Untergebrachten sind so zu behandeln, dass der Anschein vermieden wird, sie würden zur Verbüßung einer Strafe festgehalten.

(2) Die Persönlichkeit der Untergebrachten ist zu achten.
Ihre Selbstständigkeit im Vollzugsalltag ist so weit wie möglich zu erhalten und zu fördern.

(3) Die Untergebrachten werden an der Gestaltung des Vollzugsalltags beteiligt.
Vollzugliche Maßnahmen sind ihnen regelmäßig zu erläutern.

(4) Zur Erreichung des Vollzugsziels bedarf es der Mitwirkung der Untergebrachten.
Ihre Bereitschaft hierzu ist fortwährend zu wecken und zu fördern.

(5) Die Untergebrachten unterliegen den in diesem Gesetz vorgesehenen Beschränkungen ihrer Freiheit.
Soweit das Gesetz eine besondere Regelung nicht enthält, dürfen ihnen nur Beschränkungen auferlegt werden, die zur Aufrechterhaltung der Sicherheit oder zur Abwendung einer schwerwiegenden Störung der Ordnung der Einrichtung unerlässlich sind.

§ 5  
**Soziale Hilfe**

Die Untergebrachten werden darin unterstützt, ihre persönlichen, wirtschaftlichen und sozialen Schwierigkeiten zu beheben.
Sie sollen dazu angeregt und in die Lage versetzt werden, ihre Angelegenheiten selbst zu regeln.
Sie erhalten Hilfe und Beratung in sozialen und finanziellen Angelegenheiten.

### Abschnitt 2  
Aufnahme, Diagnose, Vollzugs- und Eingliederungsplanung

§ 6  
**Aufnahmeverfahren**

(1) Mit den Untergebrachten wird unverzüglich nach der Aufnahme ein Zugangsgespräch geführt, in dem ihre gegenwärtige Lebenssituation erörtert wird und sie über ihre Rechte und Pflichten sowie über die Ausgestaltung der Unterbringung in einer für sie verständlichen Form informiert werden.
Soweit erforderlich, ist eine Sprachmittlerin oder ein Sprachmittler oder eine Gebärdendolmetscherin oder ein Gebärdendolmetscher hinzuzuziehen.
Den Untergebrachten wird ein Exemplar der Hausordnung ausgehändigt.
Dieses Gesetz, die von ihm in Bezug genommenen Gesetze sowie die zu seiner Ausführung erlassenen Rechtsverordnungen und Verwaltungsvorschriften sind den Untergebrachten auf Verlangen zugänglich zu machen.

(2) Während des Aufnahmeverfahrens dürfen andere Untergebrachte nicht zugegen sein.

(3) Die Untergebrachten werden alsbald ärztlich untersucht.

§ 7  
**Diagnoseverfahren**

(1) An das Aufnahmeverfahren schließt sich zur Vorbereitung der Vollzugs- und Eingliederungsplanung das Diagnoseverfahren an.
Es kann auch bereits vor der Aufnahme durchgeführt werden.

(2) Das Diagnoseverfahren muss wissenschaftlichen Erkenntnissen genügen und von Personen mit einschlägiger wissenschaftlicher Qualifikation durchgeführt werden.

(3) Das Diagnoseverfahren erstreckt sich, aufbauend auf den Erkenntnissen aus dem Vollzug vorangegangener Freiheitsentziehungen, auf die Persönlichkeit, die sozialen Bezüge sowie alle sonstigen Gesichtspunkte, deren Kenntnis für eine Beurteilung der Gefährlichkeit der Untergebrachten, eine zielgerichtete und wirkungsorientierte Vollzugsgestaltung und die Eingliederung der Untergebrachten nach der Entlassung notwendig erscheint.

(4) Im Diagnoseverfahren werden die im Einzelfall die Gefährlichkeit begründenden Faktoren ermittelt.
Gleichzeitig sind die Fähigkeiten der Untergebrachten zu ermitteln, deren Stärkung der Gefährlichkeit entgegenwirken kann.

(5) Das Ergebnis des Diagnoseverfahrens wird mit den Untergebrachten erörtert.

§ 8  
**Vollzugs- und Eingliederungsplanung**

(1) Auf der Grundlage des Ergebnisses des Diagnoseverfahrens wird ein Vollzugs- und Eingliederungsplan erstellt.
Er zeigt den Untergebrachten bereits zu Beginn der Unterbringung die zur Erreichung des Vollzugsziels erforderlichen Maßnahmen auf.
Daneben enthält er weitere Angebote und Empfehlungen zur sinnvollen Gestaltung des Lebens im Vollzug.
Den Fähigkeiten, Fertigkeiten und Neigungen der Untergebrachten ist Rechnung zu tragen.
Stehen zur Erreichung des Vollzugsziels mehrere geeignete Maßnahmen zur Verfügung, so haben die Untergebrachten ein Wahlrecht.

(2) Der Vollzugs- und Eingliederungsplan wird unverzüglich, regelmäßig innerhalb der ersten acht Wochen nach der Aufnahme, erstellt.

(3) Der Vollzugs- und Eingliederungsplan sowie die darin vorgesehenen Maßnahmen werden regelmäßig alle sechs Monate überprüft und fortgeschrieben.
Die Entwicklung der Untergebrachten und die in der Zwischenzeit gewonnenen Erkenntnisse sind zu berücksichtigen.
Die durchgeführten Maßnahmen sind zu dokumentieren.

(4) Die Vollzugs- und Eingliederungsplanung wird mit den Untergebrachten erörtert.
Dabei werden deren Anregungen und Vorschläge einbezogen, soweit sie der Erreichung des Vollzugsziels dienen.

(5) Zur Erstellung und Fortschreibung des Vollzugs- und Eingliederungsplans führt die Leiterin oder der Leiter der Einrichtung (§ 96 Absatz 1 Satz 1) eine Konferenz mit den an der Vollzugsgestaltung maßgeblich Beteiligten durch.
Die im Vollzug einer vorangegangenen Freiheitsentziehung an der Vollzugsgestaltung maßgeblich Beteiligten können an der Konferenz beteiligt werden.
Standen die Untergebrachten vor ihrer Unterbringung unter Bewährung oder Führungsaufsicht, kann auch die für sie bislang zuständige Bewährungshelferin oder der für sie bislang zuständige Bewährungshelfer an der Konferenz beteiligt werden.
Den Untergebrachten wird der Vollzugs- und Eingliederungsplan in der Konferenz eröffnet und erläutert.
Sie können auch darüber hinaus an der Konferenz beteiligt werden.

(6) Personen außerhalb des Vollzugs, die an der Eingliederung mitwirken, sind nach Möglichkeit in die Planung einzubeziehen.
Sie können mit Zustimmung der Untergebrachten auch an der Konferenz beteiligt werden.

(7) Rechtzeitig vor einer voraussichtlichen Entlassung ist der künftig zuständigen Bewährungshelferin oder dem künftig zuständigen Bewährungshelfer die Teilnahme an der Konferenz zu ermöglichen und sind ihr oder ihm der Vollzugs- und Eingliederungsplan und seine Fortschreibungen zu übersenden.

(8) Der Vollzugs- und Eingliederungsplan und seine Fortschreibungen werden den Untergebrachten ausgehändigt.

§ 9  
**Inhalt des Vollzugs- und Eingliederungsplans**

(1) Der Vollzugs- und Eingliederungsplan sowie seine Fortschreibungen enthalten unter Berücksichtigung von § 15 Absatz 2 Satz 2 insbesondere folgende Angaben:

1.  Zusammenfassung der für die Vollzugs- und Eingliederungsplanung maßgeblichen Ergebnisse des Diagnoseverfahrens,
2.  Maßnahmen zur Förderung der Mitwirkungsbereitschaft,
3.  Teilnahme an psychiatrischen, psychotherapeutischen oder sozialtherapeutischen Maßnahmen,
4.  Teilnahme an anderen einzel- oder gruppentherapeutischen Maßnahmen,
5.  Unterbringung in einer Wohngruppe und Teilnahme am Wohngruppenvollzug,
6.  Teilnahme an Maßnahmen zur Behandlung von Suchtmittelabhängigkeit und -missbrauch,
7.  Teilnahme an Trainingsmaßnahmen zur Verbesserung der sozialen Kompetenz,
8.  Teilnahme an schulischen und beruflichen Qualifizierungsmaßnahmen einschließlich Alphabetisierungs- und Deutschkursen,
9.  Teilnahme an arbeitstherapeutischen Maßnahmen oder am Arbeitstraining,
10.  Arbeit,
11.  freies Beschäftigungsverhältnis, Selbstbeschäftigung,
12.  Teilnahme an Sportangeboten und Maßnahmen zur strukturierten Gestaltung der Freizeit,
13.  Ausführungen zur Erreichung des Vollzugsziels, Außenbeschäftigung,
14.  Lockerungen zur Erreichung des Vollzugsziels,
15.  Unterbringung im offenen Vollzug,
16.  Aufrechterhaltung, Förderung und Gestaltung von Außenkontakten, insbesondere familiärer Beziehungen,
17.  Schuldnerberatung, Schuldenregulierung und Erfüllung von Unterhaltspflichten,
18.  Maßnahmen zur Vorbereitung von Entlassung, Eingliederung und Nachsorge, Bildung eines Eingliederungsgeldes und
19.  Frist zur Fortschreibung des Vollzugs- und Eingliederungsplans.

(2) Maßnahmen nach Absatz 1 Nummer 3, 4, 6 bis 9, die nach dem Ergebnis des Diagnoseverfahrens als zur Erreichung des Vollzugsziels zwingend erforderlich erachtet werden, sind als solche zu kennzeichnen und gehen allen anderen Maßnahmen vor.
Andere Maßnahmen können versagt werden, soweit sie die Teilnahme an Maßnahmen nach Satz 1 beeinträchtigen würden.

(3) Rechtzeitig vor dem voraussichtlichen Entlassungszeitpunkt hat die Planung zur Vorbereitung der Eingliederung zu beginnen.
Anknüpfend an die bisherige Vollzugsplanung werden ab diesem Zeitpunkt die Maßnahmen nach Absatz 1 Nummer 18 konkretisiert oder ergänzt.
Insbesondere ist Stellung zu nehmen zu:

1.  Unterbringung im offenen Vollzug, Aufenthalt in einer Übergangseinrichtung,
2.  Unterkunft sowie Arbeit oder Ausbildung nach der Entlassung,
3.  Unterstützung bei notwendigen Behördengängen und der Beschaffung der notwendigen persönlichen Dokumente,
4.  Beteiligung der Bewährungshilfe und der forensischen Ambulanzen,
5.  Kontaktaufnahme zu Einrichtungen der Entlassenenhilfe,
6.  Fortsetzung von im Vollzug noch nicht abgeschlossenen Maßnahmen,
7.  Anregung von Auflagen und Weisungen für die Bewährungs- oder Führungsaufsicht,
8.  Vermittlung in nachsorgende Maßnahmen,
9.  nachgehende Betreuung durch Vollzugsbedienstete.

### Abschnitt 3  
Unterbringung, Verlegung

§ 10  
**Trennungsgrundsätze**

(1) Untergebrachte sind von Gefangenen zu trennen.

(2) Männliche und weibliche Untergebrachte sind zu trennen.

(3) Abweichend von Absatz 1 sind gemeinsame Maßnahmen im Bereich der Arbeitstherapie, des Arbeitstrainings, der schulischen und beruflichen Qualifizierung, der Arbeit, der Freizeit und der Religionsausübung zulässig, um ein differenziertes Angebot zu gewährleisten.
Für andere Maßnahmen gilt dies ausnahmsweise dann, wenn es die Behandlung nach § 66c Absatz 1 Nummer 1 des Strafgesetzbuches erfordert.

(4) Von einer getrennten Unterbringung nach Absatz 1 darf ausnahmsweise abgewichen werden, wenn es die Behandlung nach § 66c Absatz 1 Nummer 1 des Strafgesetzbuches erfordert.
Dies erfasst auch die Unterbringung in einer sozialtherapeutischen Abteilung oder im offenen Vollzug zur Entlassungsvorbereitung.
Eine Abweichung ist auch bei einer Überstellung nach § 14 Absatz 3 und 4 zulässig.
Die Unterbringungsbedingungen müssen sich außer in den Fällen des § 14 Absatz 4 im Rahmen der vorhandenen Gegebenheiten von denen der Gefangenen unterscheiden.

(5) Abweichend von Absatz 2 sind gemeinsame Maßnahmen, insbesondere zur schulischen und beruflichen Qualifizierung, zulässig.

(6) Die Absätze 1 und 2 gelten nicht für eine Unterbringung zum Zweck der medizinischen Behandlung.

§ 11  
**Unterbringung und Bewegungsfreiheit**

(1) Die Untergebrachten erhalten Zimmer zur alleinigen Nutzung.
Die Zimmer sind so zu gestalten, dass den Untergebrachten ausreichender Raum zum Wohnen und Schlafen zur Verfügung steht.
Ein baulich vollständig abgetrennter Sanitärbereich ist vorzusehen.
Die Zimmer befinden sich regelmäßig im Bereich einer Wohngruppe.

(2) Die Untergebrachten dürfen sich in den für sie vorgesehenen Bereichen der Einrichtung einschließlich des Außenbereichs frei bewegen.
Während der Nachtruhe können die Untergebrachten in ihren Zimmern eingeschlossen werden.
Weitere Einschränkungen sind zulässig, wenn es die Sicherheit oder schwerwiegende Gründe der Ordnung der Einrichtung erfordern oder ein schädlicher Einfluss auf andere Untergebrachte zu befürchten ist.

§ 12  
**Wohngruppenvollzug**

(1) Der Vollzug wird regelmäßig als Wohngruppenvollzug ausgestaltet.

(2) Der Wohngruppenvollzug dient der Einübung sozialverträglichen Zusammenlebens, insbesondere von Toleranz sowie der Übernahme von Verantwortung für sich und andere.

(3) Eine Wohngruppe wird in einem baulich abgegrenzten Bereich für bis zu zehn Untergebrachte eingerichtet, zu dem neben den Zimmern weitere Räume zur gemeinsamen Nutzung gehören.
Sie wird in der Regel von fest zugeordneten Bediensteten verschiedener Fachrichtungen betreut.

§ 13  
**Geschlossener und offener Vollzug**

(1) Die Unterbringung erfolgt im geschlossenen Vollzug.

(2) Die Untergebrachten sollen insbesondere zur Entlassungsvorbereitung im offenen Vollzug untergebracht werden, wenn sie dessen Anforderungen genügen, namentlich nicht zu befürchten ist, dass sie sich dem Vollzug entziehen oder die Möglichkeiten des offenen Vollzugs zu Straftaten missbrauchen werden.
Die Unterbringung im offenen Vollzug bedarf der Zustimmung der Aufsichtsbehörde.
Einrichtungen des offenen Vollzugs sehen verminderte Vorkehrungen gegen Entweichungen vor.

(3) Genügen die Untergebrachten den Anforderungen der Unterbringung im offenen Vollzug nicht mehr, werden sie im geschlossenen Vollzug untergebracht.

§ 14  
**Verlegung und Überstellung**

(1) Die Untergebrachten können abweichend vom Vollstreckungsplan in eine andere Einrichtung verlegt werden, wenn die Erreichung des Vollzugsziels hierdurch gefördert wird oder zwingende Gründe der Vollzugsorganisation oder andere wichtige Gründe dies erfordern.
Sie dürfen aus wichtigem Grund in eine andere Einrichtung überstellt werden.

(2) Die Untergebrachten dürfen ausnahmsweise in eine Justizvollzugsanstalt verlegt oder überstellt werden, wenn ihre Behandlung nach § 66c Absatz 1 Nummer 1 des Strafgesetzbuches es erfordert.

(3) Die Untergebrachten können in eine Justizvollzugsanstalt überstellt werden, wenn dies zur Wahrnehmung eines Gerichtstermins oder aus einem vergleichbaren Grund zwingend erforderlich ist.

(4) Auf ihren Antrag können Untergebrachte aus wichtigem Grund in eine Justizvollzugsanstalt überstellt werden, wenn dies die Behandlung nicht beeinträchtigt und sie sich mit den dortigen Bedingungen einverstanden erklären.

### Abschnitt 4  
Therapeutische Ausgestaltung und Maßnahmen

§ 15  
**Therapeutische Ausgestaltung**

(1) Der Vollzug ist auf der Grundlage des Lebens in einer Gemeinschaft therapeutisch auszugestalten.
Er bedient sich sozial- und psychotherapeutischer, psychiatrischer, sozialpädagogischer und arbeitstherapeutischer Methoden, die wissenschaftlichen Erkenntnissen entsprechen.

(2) Den Untergebrachten sind die zur Erreichung des Vollzugsziels im Einzelfall erforderlichen therapeutischen Maßnahmen anzubieten.
Soweit standardisierte Therapiemethoden nicht ausreichen oder keinen Erfolg versprechen, sind individuell zugeschnittene Behandlungsangebote zu unterbreiten.

(3) Bei der therapeutischen Ausgestaltung des Vollzugs wirken Bedienstete verschiedener Fachrichtungen in enger Abstimmung zusammen.
Soweit es erforderlich ist, sind externe Fachkräfte einzubeziehen.

§ 16  
**Motivierungsmaßnahmen**

(1) Motivierungsmaßnahmen fördern die Bereitschaft der Untergebrachten, an der Erreichung des Vollzugsziels mitzuwirken.
Hierzu gehören insbesondere wiederkehrende Gesprächsangebote, die Beziehungsfähigkeit fördernde Maßnahmen und die Vermittlung des therapeutischen Konzepts.

(2) Zur Motivierung können auch Vergünstigungen gewährt oder bereits gewährte Vergünstigungen wieder entzogen werden.
Die Ansprüche der Untergebrachten nach diesem Gesetz bleiben unberührt.

§ 17  
**Sozialtherapeutische Maßnahmen**

Sozialtherapeutische Maßnahmen bedienen sich auf der Grundlage einer therapeutischen Gemeinschaft psychotherapeutischer, sozialpädagogischer und arbeitstherapeutischer Methoden, die in umfassenden Behandlungsprogrammen verbunden werden.
Personen aus dem Lebensumfeld der Untergebrachten außerhalb des Vollzugs werden in die Behandlung einbezogen.

§ 18  
**Psychotherapeutische Maßnahmen**

Psychotherapeutische Maßnahmen im Vollzug dienen insbesondere der Behandlung psychischer Störungen des Verhaltens und Erlebens, die in einem Zusammenhang mit der Gefährlichkeit stehen.
Sie werden durch systematische Anwendung wissenschaftlich fundierter psychologischer Methoden der Gesprächsführung mit einer oder mehreren Personen durchgeführt.

§ 19  
**Psychiatrische Maßnahmen**

Psychiatrische Maßnahmen im Vollzug dienen der Behandlung psychiatrischer Krankheiten, die in einem Zusammenhang mit der Gefährlichkeit stehen.
Sie erfolgen auf der Grundlage ärztlicher Standards und Behandlungsleitlinien sowie standardisierter testpsychologischer Untersuchungen und berücksichtigen alle Lebensbereiche der Untergebrachten.
In geeigneten Fällen erfolgt eine medikamentöse Unterstützung der Behandlung.

### Abschnitt 5  
Arbeitstherapeutische Maßnahmen, Arbeitstraining,  
schulische und berufliche Qualifizierungsmaßnahmen, Arbeit

§ 20  
**Arbeitstherapeutische Maßnahmen**

Arbeitstherapeutische Maßnahmen dienen dazu, dass die Untergebrachten Eigenschaften wie Selbstvertrauen, Durchhaltevermögen und Konzentrationsfähigkeit einüben, um sie stufenweise an die Grundanforderungen des Arbeitslebens heranzuführen.

§ 21  
**Arbeitstraining**

Arbeitstraining dient dazu, Untergebrachten, die nicht in der Lage sind, einer regelmäßigen und erwerbsorientierten Beschäftigung nachzugehen, Fähigkeiten und Fertigkeiten zu vermitteln, die eine Eingliederung in das leistungsorientierte Arbeitsleben fördern.
Die dafür vorzuhaltenden Maßnahmen müssen den Anforderungen des Arbeitsmarktes Rechnung tragen.

§ 22  
**Schulische und berufliche Qualifizierungsmaßnahmen**

(1) Schulische und berufliche Aus- und Weiterbildung und vorberufliche Qualifizierung im Vollzug (schulische und berufliche Qualifizierungsmaßnahmen) haben das Ziel, den Untergebrachten Fähigkeiten zur Eingliederung und zur Aufnahme einer Erwerbstätigkeit nach der Entlassung zu vermitteln sowie vorhandene Fähigkeiten zu verbessern oder zu erhalten.
Sie werden in der Regel als Vollzeitmaßnahme durchgeführt.
Bei der Festlegung von Inhalten, Methoden und Organisationsformen der Bildungsangebote werden die Besonderheiten der jeweiligen Zielgruppe berücksichtigt.

(2) Berufliche Qualifizierungsmaßnahmen sind darauf auszurichten, den Untergebrachten für den Arbeitsmarkt relevante Qualifikationen zu vermitteln.

(3) Geeigneten Untergebrachten soll die Teilnahme an einer schulischen oder beruflichen Ausbildung ermöglicht werden, die zu einem anerkannten Abschluss führt.

(4) Können Maßnahmen während des Vollzugs nicht abgeschlossen werden, trägt die Einrichtung in Zusammenarbeit mit außervollzuglichen Einrichtungen dafür Sorge, dass die begonnene Qualifizierungsmaßnahme nach der Entlassung fortgesetzt werden kann.

(5) Nachweise über schulische und berufliche Qualifizierungsmaßnahmen dürfen keinen Hinweis auf die Unterbringung in der Sicherungsverwahrung enthalten.

§ 23  
**Arbeit**

Den Untergebrachten soll Arbeit angeboten werden.
§ 9 Absatz 2 bleibt unberührt.
Nehmen sie eine Arbeit auf, gelten die festgelegten Arbeitsbedingungen.
Die Arbeit darf nicht zur Unzeit niedergelegt werden.

§ 24  
**Freies Beschäftigungsverhältnis, Selbstbeschäftigung**

(1) Untergebrachten, die zum Freigang (§ 40 Absatz 1 Nummer 4) zugelassen sind, soll gestattet werden, einer Arbeit, Berufsausbildung oder beruflichen Weiterbildung auf der Grundlage eines freien Beschäftigungsverhältnisses oder der Selbstbeschäftigung außerhalb der Einrichtung nachzugehen, wenn die Beschäftigungsstelle geeignet ist und nicht überwiegende Gründe des Vollzugs entgegenstehen.
§ 42 Absatz 1 gilt entsprechend.

(2) Das Entgelt ist der Einrichtung zur Gutschrift für die Untergebrachten zu überweisen.

§ 25  
**Freistellung von der Arbeit**

(1) Haben die Untergebrachten ein halbes Jahr lang gearbeitet, so können sie beanspruchen, zehn Arbeitstage von der Arbeit freigestellt zu werden.
Zeiten, in denen die Untergebrachten infolge Krankheit an der Arbeitsleistung gehindert waren, werden auf das Halbjahr mit bis zu 15 Arbeitstagen angerechnet.
Der Anspruch verfällt, wenn die Freistellung nicht innerhalb eines Jahres nach seiner Entstehung erfolgt ist.

(2) Auf die Zeit der Freistellung wird Langzeitausgang (§ 40 Absatz 1 Nummer 3) angerechnet, soweit er in die Arbeitszeit fällt.
Gleiches gilt für einen Langzeitausgang nach § 41 Absatz 1, soweit er nicht wegen des Todes oder einer lebensgefährlichen Erkrankung naher Angehöriger erteilt worden ist.

(3) Die Untergebrachten erhalten für die Zeit der Freistellung ihr Arbeitsentgelt weiter.

(4) Urlaubsregelungen freier Beschäftigungsverhältnisse bleiben unberührt.

(5) Für Maßnahmen nach § 21 und § 22 Absatz 1 gelten die Absätze 1 bis 4 entsprechend, sofern diese den Umfang der regelmäßigen wöchentlichen Arbeitszeit erreichen.

### Abschnitt 6  
Besuche, Telefongespräche, Schriftwechsel, andere Formen der Telekommunikation und Pakete

§ 26  
**Grundsatz**

Die Untergebrachten haben das Recht, mit Personen außerhalb der Einrichtung im Rahmen der Bestimmungen dieses Gesetzes zu verkehren.
Der Verkehr mit der Außenwelt, insbesondere die Erhaltung der Kontakte zu Bezugspersonen und die Schaffung eines sozialen Empfangsraums, ist zu fördern.

§ 27  
**Besuch**

(1) Die Untergebrachten dürfen regelmäßig Besuch empfangen.
Die Gesamtdauer beträgt mindestens zehn Stunden im Monat.

(2) Besuche von Angehörigen im Sinne des § 11 Absatz 1 Nummer 1 des Strafgesetzbuches werden besonders unterstützt.

(3) Besuche sollen darüber hinaus zugelassen werden, wenn sie die Eingliederung der Untergebrachten fördern oder persönlichen, rechtlichen oder geschäftlichen Angelegenheiten dienen.

(4) Die Leiterin oder der Leiter der Einrichtung lässt über Absatz 1 hinausgehend mehrstündige, unbeaufsichtigte Besuche (Langzeitbesuche) zu, wenn dies zur Pflege familiärer, partnerschaftlicher oder ihnen gleichzusetzender Kontakte der Untergebrachten geboten erscheint und die Untergebrachten hierfür geeignet sind.

(5) Besuche von Verteidigerinnen und Verteidigern sowie von Rechtsanwältinnen und Rechtsanwälten und Notarinnen und Notaren in einer die Untergebrachten betreffenden Rechtssache sind zu gestatten.

§ 28  
**Untersagung der Besuche**

Die Leiterin oder der Leiter der Einrichtung kann Besuche untersagen, wenn

1.  die Sicherheit oder in schwerwiegender Weise die Ordnung der Einrichtung gefährdet würde,
2.  bei Personen, die nicht Angehörige der Untergebrachten im Sinne des § 11 Absatz 1 Nummer 1 des Strafgesetzbuches sind, zu befürchten ist, dass sie einen schädlichen Einfluss auf die Untergebrachten haben oder die Erreichung des Vollzugsziels behindern, oder
3.  bei Personen, die Opfer der Straftat waren, zu befürchten ist, dass die Begegnung mit den Untergebrachten einen schädlichen Einfluss auf sie hat.

§ 29  
**Durchführung der Besuche**

(1) Aus Gründen der Sicherheit können Besuche davon abhängig gemacht werden, dass sich die Besucherinnen und Besucher mit technischen Hilfsmitteln absuchen oder durchsuchen lassen.
§ 79 Absatz 1 Satz 2 und 3 gilt entsprechend.
Eine inhaltliche Überprüfung der von Verteidigerinnen und Verteidigern mitgeführten Schriftstücke und sonstigen Unterlagen ist nicht zulässig.
§ 35 Absatz 2 Satz 2 bleibt unberührt.

(2) Besuche können beaufsichtigt werden, wenn dies im Einzelfall aus Gründen der Sicherheit oder schwerwiegenden Gründen der Ordnung der Einrichtung erforderlich ist.
Die Beaufsichtigung kann mit optisch-elektronischen Hilfsmitteln durchgeführt werden; die betroffenen Personen sind vorher darauf hinzuweisen.
Eine Aufzeichnung findet nicht statt.

(3) Besuche von Verteidigerinnen und Verteidigern werden nicht beaufsichtigt.

(4) Besuche dürfen abgebrochen werden, wenn Besucherinnen oder Besucher oder Untergebrachte gegen dieses Gesetz oder aufgrund dieses Gesetzes getroffene Anordnungen trotz Abmahnung verstoßen.
Die Abmahnung unterbleibt, wenn es unerlässlich ist, den Besuch sofort abzubrechen.

(5) Gegenstände dürfen beim Besuch nur mit Erlaubnis übergeben werden.
Dies gilt nicht für die bei dem Besuch der Verteidigerinnen und Verteidiger übergebenen Schriftstücke und sonstigen Unterlagen sowie für die bei dem Besuch von Rechtsanwältinnen und Rechtsanwälten oder Notarinnen und Notaren zur Erledigung einer die Untergebrachten betreffenden Rechtssache übergebenen Schriftstücke und sonstigen Unterlagen.
Bei dem Besuch von Rechtsanwältinnen und Rechtsanwälten oder Notarinnen und Notaren kann die Übergabe aus Gründen der Sicherheit oder schwerwiegenden Gründen der Ordnung der Einrichtung von der Erlaubnis der Leiterin oder des Leiters der Einrichtung abhängig gemacht werden.
§ 35 Absatz 2 Satz 2 bleibt unberührt.

(6) Die Leiterin oder der Leiter der Einrichtung kann im Einzelfall die Nutzung einer Trennvorrichtung anordnen, wenn dies zum Schutz von Personen oder zur Verhinderung einer Übergabe von Gegenständen erforderlich ist.

§ 30  
**Überwachung der Gespräche**

(1) Gespräche dürfen nur überwacht werden, soweit es im Einzelfall wegen einer Gefährdung der Erreichung des Vollzugsziels oder aus Gründen der Sicherheit erforderlich ist.

(2) Gespräche mit Verteidigerinnen und Verteidigern werden nicht überwacht.

§ 31  
**Telefongespräche**

(1) Die Untergebrachten dürfen unter Vermittlung der Einrichtung Telefongespräche führen.
Die Vorschriften über den Besuch gelten entsprechend.
Eine beabsichtigte Überwachung teilt die Einrichtung den Untergebrachten rechtzeitig vor Beginn des Telefongesprächs und den Gesprächspartnerinnen und Gesprächspartnern der Untergebrachten unmittelbar nach Herstellung der Verbindung mit.

(2) Die Kosten der Telefongespräche tragen die Untergebrachten.
Sind sie dazu nicht in der Lage, kann die Einrichtung die Kosten in begründeten Fällen in angemessenem Umfang übernehmen.

§ 32  
**Schriftwechsel**

(1) Die Untergebrachten haben das Recht, Schreiben abzusenden und zu empfangen.

(2) Die Kosten des Schriftwechsels tragen die Untergebrachten.
Sind sie dazu nicht in der Lage, kann die Einrichtung die Kosten in begründeten Fällen in angemessenem Umfang übernehmen.

§ 33  
**Untersagung des Schriftwechsels**

Die Leiterin oder der Leiter der Einrichtung kann den Schriftwechsel mit bestimmten Personen untersagen, wenn

1.  die Sicherheit oder in schwerwiegender Weise die Ordnung der Einrichtung gefährdet würde,
2.  bei Personen, die nicht Angehörige der Untergebrachten im Sinne des § 11 Absatz 1 Nummer 1 des Strafgesetzbuches sind, zu befürchten ist, dass der Schriftwechsel einen schädlichen Einfluss auf die Untergebrachten hat oder die Erreichung des Vollzugsziels behindert, oder
3.  bei Personen, die Opfer der Straftat waren, zu befürchten ist, dass der Schriftwechsel einen schädlichen Einfluss auf sie hat.

§ 34  
**Sichtkontrolle, Weiterleitung und Aufbewahrung von Schreiben**

(1) Die Untergebrachten haben das Absenden und den Empfang von Schreiben durch die Einrichtung vermitteln zu lassen, soweit nichts anderes gestattet ist.

(2) Ein- und ausgehende Schreiben werden in Anwesenheit der Untergebrachten auf verbotene Gegenstände kontrolliert und sind unverzüglich weiterzuleiten.

(3) Die Untergebrachten haben eingehende Schreiben unverschlossen zu verwahren, sofern nichts anderes gestattet wird.
Sie können sie verschlossen zu ihrer Habe geben.

§ 35  
**Überwachung des Schriftwechsels**

(1) Der Schriftwechsel darf nur überwacht werden, soweit es im Einzelfall wegen einer Gefährdung der Erreichung des Vollzugsziels oder aus Gründen der Sicherheit erforderlich ist.
Eine Textkontrolle ist in Gegenwart der Untergebrachten vorzunehmen, soweit ihr Zweck dadurch nicht vereitelt wird.

(2) Der Schriftwechsel der Untergebrachten mit ihren Verteidigerinnen und Verteidigern wird nicht überwacht.
Liegt dem Vollzug eine Straftat nach § 129a, auch in Verbindung mit § 129b Absatz 1 des Strafgesetzbuches zugrunde, gelten § 148 Absatz 2 und § 148a der Strafprozessordnung entsprechend; dies gilt nicht, wenn die Untergebrachten sich im offenen Vollzug befinden oder wenn ihnen Lockerungen nach § 40 gewährt worden sind und ein Grund, der die Leiterin oder den Leiter der Einrichtung zum Widerruf von Lockerungen ermächtigt, nicht vorliegt.

(3) Nicht überwacht werden Schreiben der Untergebrachten an Volksvertretungen des Bundes und der Länder sowie an deren Mitglieder, soweit die Schreiben an die Anschriften dieser Volksvertretungen gerichtet sind und die Absenderin oder den Absender zutreffend angeben.
Entsprechendes gilt für Schreiben an das Europäische Parlament und dessen Mitglieder, den Europäischen Gerichtshof für Menschenrechte, den Europäischen Ausschuss zur Verhütung von Folter und unmenschlicher oder erniedrigender Behandlung oder Strafe, den Ausschuss der Vereinten Nationen gegen Folter, den zugehörigen Unterausschuss zur Verhütung von Folter und die entsprechenden Nationalen Präventionsmechanismen, die konsularische Vertretung ihres Heimatlandes und weitere Einrichtungen, mit denen der Schriftverkehr aufgrund völkerrechtlicher Verpflichtungen der Bundesrepublik Deutschland geschützt ist.
Satz 1 gilt auch für den Schriftverkehr mit Gerichten, Staatsanwaltschaften und der Aufsichtsbehörde sowie den Bürgerbeauftragten der Länder und den Datenschutzbeauftragten des Bundes und der Länder.
Schreiben der in den Sätzen 1 bis 3 genannten Stellen, die an die Untergebrachten gerichtet sind, werden nicht überwacht, sofern die Identität der Absenderin oder des Absenders zweifelsfrei feststeht.

§ 36  
**Anhalten von Schreiben**

(1) Die Leiterin oder der Leiter der Einrichtung kann Schreiben anhalten, wenn

1.  deren Weitergabe die Erreichung des Vollzugsziels oder die Sicherheit oder in schwerwiegender Weise die Ordnung der Einrichtung gefährden würde,
2.  die Weitergabe in Kenntnis ihres Inhalts einen Straf- oder Bußgeldtatbestand verwirklichen würde,
3.  sie grob unrichtige oder erheblich entstellende Darstellungen von Verhältnissen der Einrichtung oder grobe Beleidigungen enthalten,
4.  sie die Eingliederung anderer Untergebrachter oder Gefangener gefährden können oder
5.  sie in Geheim- oder Kurzschrift, unlesbar, unverständlich oder ohne zwingenden Grund in einer fremden Sprache abgefasst sind.

(2) Ausgehenden Schreiben, die unrichtige Darstellungen enthalten, kann ein Begleitschreiben beigefügt werden, wenn die Untergebrachten auf dem Absenden bestehen.

(3) Sind Schreiben angehalten worden, wird das den Untergebrachten mitgeteilt.
Angehaltene Schreiben werden an die Absenderin oder den Absender zurückgegeben oder, sofern dies unmöglich oder aus besonderen Gründen nicht angezeigt ist, verwahrt.

(4) Schreiben, deren Überwachung ausgeschlossen ist, dürfen nicht angehalten werden.

§ 37  
**Andere Formen der Telekommunikation**

Nach Zulassung anderer Formen der Telekommunikation im Sinne des Telekommunikationsgesetzes durch die Aufsichtsbehörde (§ 102 Absatz 1) soll die Leiterin oder der Leiter der Einrichtung den Untergebrachten gestatten, diese Formen auf ihre Kosten zu nutzen.
Die Bestimmungen dieses Abschnitts gelten entsprechend.

§ 38  
**Pakete**

(1) Die Untergebrachten dürfen Pakete empfangen.
Die Einrichtung kann Gewicht und Größe von Sendungen festsetzen und einzelne Gegenstände vom Paketempfang ausnehmen, wenn die Sicherheit oder in schwerwiegender Weise die Ordnung der Einrichtung oder die Erreichung des Vollzugsziels gefährdet werden.

(2) Die Einrichtung kann die Annahme von Paketen, die die Voraussetzungen des Absatzes 1 nicht erfüllen, ablehnen oder solche Pakete an die Absenderin oder den Absender zurücksenden.

(3) Pakete sind in Gegenwart der Untergebrachten zu öffnen, an die sie adressiert sind.
Mit nicht zugelassenen oder ausgeschlossenen Gegenständen ist gemäß § 54 Absatz 3 zu verfahren.
Sie können auch auf Kosten der Untergebrachten zurückgesandt werden.

(4) Der Empfang von Paketen kann vorübergehend versagt werden, wenn dies wegen der Gefährdung der Sicherheit oder Ordnung der Einrichtung unerlässlich ist.

(5) Die Untergebrachten dürfen Pakete versenden.
Der Inhalt kann aus Gründen der Sicherheit oder Ordnung der Einrichtung überprüft werden.

(6) Die Kosten des Paketversandes tragen die Untergebrachten.
Sind sie dazu nicht in der Lage, kann die Einrichtung die Kosten in begründeten Fällen in angemessenem Umfang übernehmen.

### Abschnitt 7  
Vollzugsöffnende Maßnahmen und sonstige Aufenthalte außerhalb der Einrichtung

§ 39  
**Vollzugsöffnende Maßnahmen**

Vollzugsöffnende Maßnahmen werden in Form von Lockerungen, Ausführungen und Außenbeschäftigung gewährt.

§ 40  
**Lockerungen zur Erreichung des Vollzugsziels**

(1) Aufenthalte außerhalb der Einrichtung ohne Aufsicht (Lockerungen) sind namentlich

1.  das Verlassen der Einrichtung für bis zu 24 Stunden in Begleitung einer von der Einrichtung zugelassenen Person (Begleitausgang),
2.  das Verlassen der Einrichtung für bis zu 24 Stunden ohne Begleitung (unbegleiteter Ausgang),
3.  das Verlassen der Einrichtung für mehrere Tage (Langzeitausgang) und
4.  die regelmäßige Beschäftigung außerhalb der Einrichtung (Freigang).

(2) Die Lockerungen sind zu gewähren, wenn sie der Erreichung des Vollzugsziels dienen und verantwortet werden kann zu erproben, dass die Untergebrachten sich dem Vollzug nicht entziehen oder die Lockerungen nicht zu Straftaten missbrauchen werden.

§ 41  
**Lockerungen aus sonstigen Gründen**

(1) Lockerungen können auch aus wichtigem Anlass gewährt werden.
Wichtige Anlässe sind insbesondere die Teilnahme an gerichtlichen Terminen, die medizinische Behandlung der Untergebrachten sowie der Tod oder eine lebensgefährliche Erkrankung naher Angehöriger.

(2) Die Lockerungen dürfen nur gewährt werden, wenn verantwortet werden kann zu erproben, dass die Untergebrachten sich dem Vollzug nicht entziehen oder die Lockerungen nicht zu Straftaten missbrauchen werden.

§ 42  
**Weisungen für Lockerungen, Zustimmung der Aufsichtsbehörde**

(1) Für Lockerungen sind die nach den Umständen des Einzelfalles erforderlichen Weisungen zu erteilen.
Bei der Ausgestaltung der Lockerungen ist nach Möglichkeit auch den Belangen der Opfer Rechnung zu tragen.

(2) Lockerungen bedürfen der Zustimmung der Aufsichtsbehörde.

§ 43  
**Ausführungen zur Erreichung des Vollzugsziels**

(1) Das Verlassen der Einrichtung unter ständiger und unmittelbarer Aufsicht (Ausführung) kann den Untergebrachten zur Erreichung des Vollzugsziels gestattet werden, wenn nicht konkrete Anhaltspunkte die Gefahr begründen, dass die Untergebrachten sich trotz besonderer Sicherungsmaßnahmen dem Vollzug entziehen oder die Ausführungen zu erheblichen Straftaten missbrauchen werden.

(2) Unter den Voraussetzungen des Absatzes 1 sind jährlich mindestens vier Ausführungen durchzuführen.
Lockerungen nach § 40 werden hierauf angerechnet.
Die Ausführungen dienen der Erhaltung der Lebenstüchtigkeit, der Förderung der Mitwirkung an der Behandlung oder der Vorbereitung von Lockerungen.
Sie unterbleiben, wenn die zur Sicherung erforderlichen Maßnahmen den Zweck der Ausführungen gefährden.

§ 44  
**Ausführungen aus sonstigen Gründen**

(1) Ausführungen können auch aus wichtigem Anlass erfolgen.
Die Untergebrachten können gegen ihren Willen ausgeführt werden.

(2) Für Ausführungen, die ausschließlich im Interesse der Untergebrachten erfolgen, gilt § 43 Absatz 1 entsprechend.
Die Kosten können den Untergebrachten auferlegt werden, soweit dies die Behandlung oder die Eingliederung nicht behindert.

§ 45  
**Außenbeschäftigung**

Den Untergebrachten kann gestattet werden, außerhalb der Einrichtung einer regelmäßigen Beschäftigung unter ständiger Aufsicht oder unter Aufsicht in unregelmäßigen Abständen (Außenbeschäftigung) nachzugehen.
§ 41 Absatz 2 gilt entsprechend.

§ 46  
**Vorführung, Ausantwortung**

(1) Auf Ersuchen eines Gerichts werden Untergebrachte vorgeführt, sofern ein Vorführungsbefehl vorliegt.

(2) Untergebrachte dürfen befristet dem Gewahrsam eines Gerichts, einer Staatsanwaltschaft oder einer Polizei-, Zoll- oder Finanzbehörde auf Antrag überlassen werden (Ausantwortung).

### Abschnitt 8  
Vorbereitung der Eingliederung, Entlassung und nachgehende Betreuung

§ 47  
**Vorbereitung der Eingliederung**

(1) Die Maßnahmen zur sozialen und beruflichen Eingliederung sind auf den Zeitpunkt der Entlassung in die Freiheit abzustellen.
Die Untergebrachten sind bei der Ordnung ihrer persönlichen, wirtschaftlichen und sozialen Angelegenheiten zu unterstützen.
Dies umfasst auch die Vermittlung in nachsorgende Maßnahmen.

(2) Die Einrichtung arbeitet frühzeitig mit den Kommunen, den Agenturen für Arbeit, den Trägern der Sozialversicherung und der Sozialhilfe, den Hilfeeinrichtungen anderer Behörden, den forensischen Ambulanzen, den Verbänden der freien Wohlfahrtspflege und weiteren Personen und Einrichtungen außerhalb des Vollzugs zusammen, insbesondere, um zu erreichen, dass die Untergebrachten nach ihrer Entlassung über eine geeignete Unterkunft und eine Arbeits- oder Ausbildungsstelle verfügen sowie bei Bedarf Zugang zu therapeutischen und anderen nachsorgenden Maßnahmen erhalten.
Bewährungshilfe und Führungsaufsichtsstellen beteiligen sich frühzeitig an der sozialen und beruflichen Eingliederung der Untergebrachten.

(3) Den Untergebrachten können Aufenthalte in Einrichtungen außerhalb des Vollzugs (Übergangseinrichtungen) gewährt werden, wenn dies zur Vorbereitung der Eingliederung erforderlich ist.
Ihnen kann auch ein zusammenhängender Langzeitausgang bis zu sechs Monaten, zur Unterbringung in einer Einrichtung freier Träger auch darüber hinaus, gewährt werden, wenn dies zur Vorbereitung der Eingliederung erforderlich ist.
§ 41 Absatz 2 sowie § 42 gelten entsprechend.

§ 48  
**Entlassung**

(1) Die Untergebrachten sollen am Tag ihrer Entlassung möglichst frühzeitig, jedenfalls noch am Vormittag, entlassen werden.

(2) Der Entlassungszeitpunkt kann bis zu fünf Tage vorverlegt werden, wenn die Untergebrachten zu ihrer Eingliederung hierauf dringend angewiesen sind.

(3) Bedürftigen Untergebrachten kann eine Entlassungsbeihilfe in Form eines Reisekostenzuschusses, angemessener Kleidung oder einer sonstigen notwendigen Unterstützung gewährt werden.

(4) Bei Bedarf soll die Einrichtung den Transport in eine Unterkunft sicherstellen.

§ 49  
**Nachgehende Betreuung**

(1) Die Einrichtung kann den Entlassenen auf Antrag Hilfestellung gewähren, soweit diese nicht anderweitig zur Verfügung steht und der Erfolg der Behandlung gefährdet erscheint.

(2) Mit Zustimmung der Leiterin oder des Leiters der Einrichtung können Bedienstete an der nachgehenden Betreuung Entlassener mit deren Einverständnis mitwirken, wenn ansonsten die Eingliederung gefährdet wäre.
Die nachgehende Betreuung kann auch außerhalb der Einrichtung erfolgen.
In der Regel ist sie auf die ersten sechs Monate nach der Entlassung beschränkt.

§ 50  
**Verbleib oder Aufnahme auf freiwilliger Grundlage**

(1) Sofern es die Belegungssituation zulässt, können die Untergebrachten auf Antrag ausnahmsweise vorübergehend in der Einrichtung verbleiben oder wieder aufgenommen werden, wenn die Eingliederung gefährdet und ein Aufenthalt in der Einrichtung aus diesem Grunde gerechtfertigt ist.

(2) Die Unterbringung erfolgt auf vertraglicher Basis.
Gegen die in der Einrichtung untergebrachten Entlassenen dürfen Maßnahmen des Vollzugs nicht mit unmittelbarem Zwang durchgesetzt werden.

(3) Bei Störung des Betriebs der Einrichtung durch die Entlassenen oder aus vollzugsorganisatorischen Gründen kann die Unterbringung jederzeit beendet werden.

### Abschnitt 9  
Grundversorgung und Freizeit

§ 51  
**Einbringen von Gegenständen**

Gegenstände dürfen durch oder für die Untergebrachten nur mit Zustimmung der Einrichtung eingebracht werden.
Die Einrichtung kann die Zustimmung verweigern, wenn die Gegenstände geeignet sind, die Sicherheit oder in schwerwiegender Weise die Ordnung der Einrichtung oder die Erreichung des Vollzugsziels zu gefährden oder ihre Aufbewahrung nach Art oder Umfang offensichtlich nicht möglich ist.

§ 52  
**Gewahrsam an Gegenständen**

Die Einrichtung kann Annahme und Abgabe von Gegenständen zwischen Untergebrachten und den Gewahrsam an ihnen von ihrer Zustimmung abhängig machen.
Sie kann die Zustimmung unter den Voraussetzungen des § 51 Satz 2 verweigern.

§ 53  
**Ausstattung des Zimmers**

Die Untergebrachten dürfen ihr Zimmer mit eigenen Gegenständen ausstatten oder diese dort aufbewahren.
Gegen­stände, die einzeln oder in ihrer Gesamtheit geeignet sind, die Sicherheit oder in schwerwiegender Weise die Ordnung der Einrichtung, insbesondere die Übersichtlichkeit des Zimmers, oder die Erreichung des Vollzugsziels zu gefährden, dürfen nicht in das Zimmer eingebracht werden oder werden daraus entfernt.

§ 54  
**Aufbewahrung und Vernichtung von Gegenständen**

(1) Gegenstände, die die Untergebrachten nicht im Zimmer aufbewahren dürfen oder wollen, werden von der Einrichtung aufbewahrt, soweit dies nach Art und Umfang möglich ist.

(2) Den Untergebrachten wird Gelegenheit gegeben, ihre Gegenstände, die sie während des Vollzugs und für ihre Entlassung nicht benötigen, zu versenden.
§ 38 Absatz 6 gilt entsprechend.

(3) Werden Gegenstände, deren Aufbewahrung nach Art oder Umfang nicht möglich ist, von den Untergebrachten trotz Aufforderung nicht aus der Einrichtung verbracht, so darf die Einrichtung diese Gegenstände auf Kosten der Untergebrachten außerhalb der Einrichtung verwahren, verwerten oder vernichten.
Für die Voraussetzungen und das Verfahren der Verwertung und Vernichtung gilt § 27 des Brandenburgischen Polizeigesetzes entsprechend.

(4) Aufzeichnungen und andere Gegenstände, die Kenntnisse über Sicherungsvorkehrungen der Einrichtung vermitteln oder Schlussfolgerungen auf diese zulassen, dürfen vernichtet oder unbrauchbar gemacht werden.

§ 55  
**Zeitungen und Zeitschriften, religiöse Schriften und Gegenstände**

(1) Die Untergebrachten dürfen auf eigene Kosten Zeitungen und Zeitschriften durch Vermittlung der Einrichtung beziehen.
Ausgeschlossen sind lediglich Zeitungen und Zeitschriften, deren Verbreitung mit Strafe oder Geldbuße bedroht ist.
Einzelne Ausgaben oder Teile von Zeitungen oder Zeitschriften können den Untergebrachten vorenthalten oder entzogen werden, wenn deren Inhalte die Erreichung des Vollzugsziels oder die Sicherheit oder Ordnung der Einrichtung erheblich gefährden würden.

(2) Die Untergebrachten dürfen grundlegende religiöse Schriften sowie Gegenstände des religiösen Gebrauchs besitzen.
Diese dürfen den Untergebrachten nur bei grobem Missbrauch entzogen werden.

§ 56  
**Rundfunk, Informations- und Unterhaltungselektronik**

(1) Der Zugang zum Rundfunk ist zu ermöglichen.

(2) Eigene Hörfunk- und Fernsehgeräte werden zugelassen, wenn nicht Gründe des § 53 Satz 2 entgegenstehen.
Andere Geräte der Informations- und Unterhaltungselektronik können unter diesen Voraussetzungen zugelassen werden.
Die Untergebrachten können auf Mietgeräte oder auf ein Mediensystem verwiesen werden.
§ 37 bleibt unberührt.

§ 57  
**Kleidung**

(1) Die Untergebrachten dürfen eigene Kleidung tragen und eigene Wäsche benutzen.
Auf Antrag stellt die Einrichtung den Untergebrachten Kleidung und Wäsche zur Verfügung und ordnet diese persönlich zu.

(2) Sofern die Untergebrachten nicht für eine regelmäßige Reinigung und Instandsetzung ihrer eigenen Kleidung und Wäsche auf ihre Kosten sorgen, können sie verpflichtet werden, von der Einrichtung gestellte Kleidung und Wäsche zu benutzen.

§ 58  
**Verpflegung und Einkauf**

(1) Die Untergebrachten dürfen sich selbst verpflegen, soweit nicht die Sicherheit oder schwerwiegende Gründe der Ordnung der Einrichtung entgegenstehen.

(2) Verpflegen sich die Untergebrachten selbst, tragen sie die Kosten und werden von der Gemeinschaftsverpflegung der Einrichtung ausgenommen.
Die Einrichtung unterstützt die Untergebrachten durch einen zweckgebundenen Zuschuss in Höhe der ersparten Aufwendungen.
Die Einrichtung kann stattdessen Lebensmittel zur Verfügung stellen.

(3) Soweit sich die Untergebrachten nicht selbst verpflegen, nehmen sie an der Gemeinschaftsverpflegung der Einrichtung teil.
Zusammensetzung und Nährwert der Gemeinschaftsverpflegung entsprechen den Anforderungen an eine gesunde Ernährung und werden ärztlich überwacht.
Auf ärztliche Anordnung wird besondere Verpflegung gewährt.
Den Untergebrachten ist zu ermöglichen, Speisevorschriften ihrer Religionsgemeinschaft zu befolgen oder sich fleischlos zu ernähren.

(4) Den Untergebrachten wird ermöglicht, mindestens einmal wöchentlich einzukaufen.
Die Einrichtung wirkt auf ein Angebot hin, das auf Wünsche und Bedürfnisse der Untergebrachten Rücksicht nimmt.
Das Verfahren des Einkaufs regelt die Leiterin oder der Leiter der Einrichtung.
Nahrungs-, Genuss- und Körperpflegemittel können nur vom Haus- und Taschengeld, andere Gegenstände in angemessenem Umfang auch vom Eigengeld eingekauft werden.

§ 59  
**Freizeit**

(1) Die Untergebrachten erhalten Gelegenheit und Anregung, ihre Freizeit sinnvoll zu gestalten.
Die Einrichtung hat insbesondere Angebote zur sportlichen und kulturellen Betätigung sowie Bildungsangebote vorzuhalten.
Die Benutzung einer angemessen ausgestatteten Mediathek ist zu ermöglichen.

(2) Die Untergebrachten sind zur Teilnahme an Angeboten der Freizeitgestaltung zu motivieren.
Die Gestaltung der Freizeit kann auch dazu dienen, die Untergebrachten an andere Maßnahmen heranzuführen.

### Abschnitt 10  
Vergütung, Gelder der Untergebrachten und Kosten

§ 60  
**Vergütung**

(1) Die Untergebrachten erhalten eine Vergütung in Form von

1.  finanzieller Anerkennung für die Teilnahme an Maßnahmen nach § 9 Absatz 1 Nummer 3, 4, 6 und 7, soweit sie nach § 9 Absatz 2 für zwingend erforderlich erachtet wurden,
2.  Ausbildungsbeihilfe für die Teilnahme an schulischen und beruflichen Qualifizierungsmaßnahmen, arbeitstherapeutischen Maßnahmen und Arbeitstraining oder
3.  Arbeitsentgelt für Arbeit.

(2) Der Bemessung der Vergütung sind 16 Prozent der Bezugsgröße nach § 18 des Vierten Buches Sozialgesetzbuch zugrunde zu legen (Eckvergütung).
Ein Tagessatz ist der 250.
Teil der Eckvergütung; die Vergütung kann nach einem Stundensatz bemessen werden.

(3) Die Vergütung kann je nach Art der Maßnahme und Leistung der Untergebrachten gestuft werden.
Sie beträgt mindestens 75 Prozent der Eckvergütung.
Das für den Justizvollzug zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung Vergütungsstufen zu regeln.

(4) Soweit Beiträge zur Bundesagentur für Arbeit zu entrichten sind, kann vom Arbeitsentgelt oder der Ausbildungsbeihilfe ein Betrag einbehalten werden, der dem Anteil der Untergebrachten am Beitrag entsprechen würde, wenn sie diese Vergütung als Arbeitnehmerin oder Arbeitnehmer erhielten.

(5) Die Höhe der Vergütung ist den Untergebrachten schriftlich bekannt zu geben.

(6) Die Untergebrachten, die an einer Maßnahme nach § 22 teilnehmen, erhalten hierfür nur eine Ausbildungsbeihilfe, soweit kein Anspruch auf Leistungen zum Lebensunterhalt besteht, die außerhalb des Vollzugs aus solchem Anlass gewährt werden.

§ 61  
**Eigengeld**

(1) Das Eigengeld besteht aus den Beträgen, die die Untergebrachten bei Aufnahme in den Vollzug mitbringen und die sie während des Vollzugs erhalten, und den Teilen der Vergütung, die nicht als Hausgeld oder Eingliederungsgeld in Anspruch genommen werden.

(2) Die Untergebrachten können über das Eigengeld verfügen.
§ 58 Absatz 4 sowie die §§ 64 und 65 bleiben unberührt.

§ 62  
**Taschengeld**

(1) Bedürftigen Untergebrachten wird auf Antrag Taschengeld gewährt.
Bedürftig sind Untergebrachte, soweit ihnen aus Hausgeld und Eigengeld monatlich ein Betrag bis zur Höhe des Taschengelds voraussichtlich nicht zur Verfügung steht.
Finanzielle Anerkennungen nach § 60 Absatz 1 Nummer 1 bleiben bis zur Höhe des Taschengeldbetrags unberücksichtigt.

(2) Untergebrachte gelten als nicht bedürftig, wenn ihnen ein Betrag nach Absatz 1 Satz 2 deshalb nicht zur Verfügung steht, weil sie eine ihnen angebotene zumutbare Arbeit nicht angenommen haben oder eine ausgeübte Arbeit verschuldet verloren haben.

(3) Das Taschengeld beträgt 24 Prozent der Eckvergütung (§ 60 Absatz 2 Satz 1).
Es wird zu Beginn des Monats im Voraus gewährt.
Gehen den Untergebrachten im Laufe des Monats Gelder zu, wird zum Ausgleich ein Betrag bis zur Höhe des gewährten Taschengelds einbehalten.

(4) Die Untergebrachten dürfen über das Taschengeld im Rahmen der Bestimmungen dieses Gesetzes verfügen.
Es wird dem Hausgeldkonto gutgeschrieben.

§ 63  
**Konten, Bargeld**

(1) Gelder der Untergebrachten werden auf Hausgeld- und Eigengeldkonten in der Einrichtung geführt.

(2) Der Besitz von Bargeld in der Einrichtung ist den Untergebrachten nicht gestattet.
Über Ausnahmen entscheidet die Leiterin oder der Leiter der Einrichtung.

(3) Geld in Fremdwährung wird zur Habe genommen.

§ 64  
**Hausgeld**

(1) Das Hausgeld wird aus vier Siebteln der in diesem Gesetz geregelten Vergütung gebildet.

(2) Für Untergebrachte, die aus einem freien Beschäftigungsverhältnis, aus einer Selbstbeschäftigung oder anderweitig regelmäßige Einkünfte haben, wird daraus ein angemessenes monatliches Hausgeld festgesetzt.

(3) Für Untergebrachte, die über Eigengeld verfügen und keine hinreichende Vergütung nach diesem Gesetz erhalten, gilt Absatz 2 entsprechend.

(4) Die Untergebrachten dürfen über das Hausgeld im Rahmen der Bestimmungen dieses Gesetzes verfügen.
Der Anspruch auf Auszahlung ist nicht übertragbar.

§ 65  
**Zweckgebundene Einzahlungen, Eingliederungsgeld**

(1) Für Maßnahmen der Eingliederung, insbesondere Kosten der Gesundheitsfürsorge und der Aus- und Fortbildung, und für Maßnahmen der Pflege sozialer Beziehungen, insbesondere Telefonkosten und Fahrtkosten anlässlich vollzugsöffnender Maßnahmen, kann zweckgebunden Geld eingezahlt werden.
Das Geld darf nur für diese Zwecke verwendet werden.
Der Anspruch auf Auszahlung ist nicht übertragbar.

(2) Die Untergebrachten dürfen für Zwecke der Eingliederung nach der Entlassung ein Guthaben in angemessener Höhe bilden (Eingliederungsgeld) und auch bereits vor der Entlassung für diese Zwecke darüber verfügen.
Der Anspruch auf Auszahlung ist nicht übertragbar.

§ 66  
**Kosten**

Die Untergebrachten werden an den Kosten des Vollzugs ihrer Unterbringung in der Sicherungsverwahrung nicht beteiligt, soweit dieses Gesetz nicht Anderes bestimmt.

### Abschnitt 11   
Gesundheitsfürsorge

§ 67  
**Art und Umfang der medizinischen Leistungen, Kostenbeteiligung**

(1) Die Untergebrachten haben einen Anspruch auf notwendige, ausreichende und zweckmäßige medizinische Leistungen unter Beachtung des Grundsatzes der Wirtschaftlichkeit und unter Berücksichtigung des allgemeinen Standards der gesetzlichen Krankenversicherung.
Der Anspruch umfasst auch Vorsorgeleistungen, ferner die Versorgung mit medizinischen Hilfsmitteln, soweit diese nicht als allgemeine Gebrauchsgegenstände des täglichen Lebens anzusehen sind.

(2) An den Kosten nach Absatz 1 können die Untergebrachten in angemessenem Umfang beteiligt werden, höchstens jedoch bis zum Umfang der Beteiligung vergleichbarer gesetzlich Versicherter.
Für Leistungen, die über Absatz 1 hinausgehen, können den Untergebrachten die gesamten Kosten auferlegt werden.

(3) Den Untergebrachten ist nach Anhörung des ärztlichen Dienstes auf ihren Antrag hin zu gestatten, auf ihre Kosten externen ärztlichen Rat einzuholen.
Die Erlaubnis kann versagt werden, wenn die Untergebrachten die gewählte ärztliche Vertrauensperson und den ärztlichen Dienst der Einrichtung nicht wechselseitig von der Schweigepflicht entbinden oder wenn es zur Aufrechterhaltung der Sicherheit oder Ordnung der Einrichtung erforderlich ist.
Die Konsultation soll in der Einrichtung stattfinden.

(4) Erhalten Untergebrachte Leistungen nach Absatz 1 infolge einer mutwilligen Selbstverletzung, sind sie in angemessenem Umfang an den Kosten zu beteiligen.
Die Kostenbeteiligung unterbleibt, wenn hierdurch die Erreichung des Vollzugsziels, insbesondere die Eingliederung der Untergebrachten, gefährdet würde.

§ 68  
**Durchführung der medizinischen Leistungen, Forderungsübergang**

(1) Medizinische Diagnostik, Behandlung und Versorgung kranker und hilfsbedürftiger Untergebrachter erfolgen in der Einrichtung, erforderlichenfalls in einer hierfür besser geeigneten Einrichtung, einer Krankenabteilung einer Justizvollzugsanstalt oder einem Justizvollzugskrankenhaus, ausnahmsweise auch außerhalb des Justizvollzugs.

(2) Wird die Vollstreckung der Maßregel während einer Behandlung von Untergebrachten unterbrochen oder beendet, so hat das Land nur diejenigen Kosten zu tragen, die bis zur Unterbrechung oder Beendigung der Vollstreckung angefallen sind.

(3) Gesetzliche Schadensersatzansprüche, die Untergebrachten infolge einer Körperverletzung gegen Dritte zustehen, gehen insoweit auf das Land über, als den Untergebrachten Leistungen nach § 67 Absatz 1 zu gewähren sind.
Von der Geltendmachung der Ansprüche ist im Interesse Untergebrachter abzusehen, wenn hierdurch die Erreichung des Vollzugsziels gefährdet würde.

§ 69  
**Ärztliche Behandlung zur sozialen Eingliederung**

Mit Zustimmung der Untergebrachten soll die Einrichtung ärztliche Behandlungen, insbesondere Operationen oder prothetische Maßnahmen, durchführen lassen, die die soziale Eingliederung fördern.
Die Kosten tragen die Untergebrachten.
Sind sie dazu nicht in der Lage, kann die Einrichtung die Kosten in begründeten Fällen in angemessenem Umfang übernehmen.

§ 70  
**Gesundheitsschutz und Hygiene**

(1) Die Einrichtung unterstützt die Untergebrachten bei der Wiederherstellung und Erhaltung ihrer körperlichen, geistigen und seelischen Gesundheit.
Sie fördert das Bewusstsein für gesunde Ernährung und Lebensführung.
Die Untergebrachten haben die notwendigen Anordnungen zum Gesundheitsschutz und zur Hygiene zu befolgen.

(2) Den Untergebrachten wird ermöglicht, sich täglich mindestens eine Stunde im Freien aufzuhalten.

§ 71  
**Krankenbehandlung während Lockerungen**

(1) Während Lockerungen haben die Untergebrachten einen Anspruch auf medizinische Leistungen gegen das Land nur in der für sie zuständigen Einrichtung.
§ 41 bleibt unberührt.

(2) Der Anspruch auf Leistungen ruht, solange die Untergebrachten aufgrund eines freien Beschäftigungsverhältnisses krankenversichert sind.

§ 72  
**Zwangsmaßnahmen auf dem Gebiet der Gesundheitsfürsorge**

(1) Eine medizinische Untersuchung und Behandlung ist ohne Einwilligung der Untergebrachten zulässig, um den Erfolg eines Selbsttötungsversuchs zu verhindern.
Eine Maßnahme nach Satz 1 ist auch zulässig, wenn von den Untergebrachten eine schwerwiegende Gefahr für die Gesundheit einer anderen Person ausgeht und die Maßnahme verhältnismäßig ist.

(2) Eine medizinische Untersuchung und Behandlung sowie eine Zwangsernährung sind bei Lebensgefahr oder schwerwiegender Gefahr für die Gesundheit der Untergebrachten zulässig, wenn diese zur Einsicht in das Vorliegen der Gefahr und die Notwendigkeit der Maßnahme oder zum Handeln gemäß solcher Einsicht krankheitsbedingt nicht fähig sind.

(3) Eine Maßnahme nach Absatz 2 darf nur angeordnet werden, wenn

1.  eine Patientenverfügung im Sinne des § 1901a Absatz 1 Satz 1 des Bürgerlichen Gesetzbuches, deren Festlegungen auf die aktuelle Lebens- und Behandlungssituation zutreffen und gegen die Durchführung der Maßnahme gerichtet sind, der Anstalt nicht vorliegt,
2.  die Untergebrachten durch eine Ärztin oder einen Arzt über Notwendigkeit, Art, Umfang, Dauer, zu erwartende Folgen und Risiken der Maßnahme in einer ihrer Auffassungsgabe und ihrem Gesundheitszustand angemessenen Weise aufgeklärt wurden,
3.  der ernsthafte und ohne Ausübung von Druck unternommene Versuch einer Ärztin oder eines Arztes, ein Einverständnis der Untergebrachten mit der Maßnahme zu erwirken, erfolglos geblieben ist,
4.  die Maßnahme zur Abwendung einer Gefahr nach Absatz 2 geeignet und erforderlich ist und
5.  der von der Maßnahme erwartete Nutzen die mit der Maßnahme verbundene Belastung und den durch das Unterlassen der Maßnahme möglichen Schaden deutlich überwiegt.

(4) Maßnahmen nach den Absätzen 1 und 2 dürfen nur von der Anstaltsleiterin oder dem Anstaltsleiter auf der Grundlage einer ärztlichen Empfehlung angeordnet werden.
Die Anordnung bedarf der Zustimmung der medizinischen Fachaufsicht.
Durchführung und Überwachung unterstehen ärztlicher Leitung.
Unberührt bleibt die Leistung erster Hilfe für den Fall, dass eine Ärztin oder ein Arzt nicht rechtzeitig erreichbar und mit einem Aufschub Lebensgefahr verbunden ist.
Die Gründe für die Anordnung einer Maßnahme nach Absatz 1 oder Absatz 2, in den Fällen des Absatzes 2 auch das Vorliegen der dort genannten Voraussetzungen sowie die ergriffene Maßnahme, einschließlich ihres Zwangscharakters, die Durchsetzungsweise, die Wirkungsüberwachung sowie der Untersuchungs- und Behandlungsverlauf sind zu dokumentieren.
Gleiches gilt für Erklärungen der Untergebrachten, die im Zusammenhang mit Zwangsmaßnahmen von Bedeutung sein können.

(5) Die Anordnung einer Maßnahme nach Absatz 1 Satz 2 oder Absatz 2 ist den Untergebrachten vor Durchführung der Maßnahme schriftlich bekannt zu geben.
Sie sind darüber zu belehren, dass sie gegen die Anordnung bei Gericht um einstweiligen Rechtsschutz ersuchen und auch Antrag auf gerichtliche Entscheidung stellen können.
Mit dem Vollzug einer Anordnung ist zuzuwarten, bis die Untergebrachten Gelegenheit hatten, eine gerichtliche Entscheidung herbeizuführen.

(6) Bei Gefahr im Verzug finden Absatz 3 Nummer 2 und 3 und Absatz 5 keine Anwendung.

(7) Die zwangsweise körperliche Untersuchung der Untergebrachten zum Gesundheitsschutz und zur Hygiene ist zulässig, wenn sie nicht mit einem körperlichen Eingriff verbunden ist.
Sie darf nur von der Anstaltsleiterin oder dem Anstaltsleiter auf der Grundlage einer ärztlichen Stellungnahme angeordnet werden.
Durchführung und Überwachung unterstehen ärztlicher Leitung.

§ 73  
**Benachrichtigungspflicht**

Erkranken Untergebrachte schwer oder versterben sie, werden die Angehörigen und gegebenenfalls die Verteidigerinnen und Verteidiger benachrichtigt.
Dem Wunsch der Untergebrachten, auch andere Personen zu benachrichtigen, soll nach Möglichkeit entsprochen werden.

### Abschnitt 12  
Religionsausübung

§ 74  
**Seelsorge**

Den Untergebrachten darf religiöse Betreuung durch eine Seelsorgerin oder einen Seelsorger nicht versagt werden.
Auf Wunsch ist ihnen zu helfen, mit einer Seelsorgerin oder einem Seelsorger ihrer Religionsgemeinschaft in Verbindung zu treten.

§ 75  
**Religiöse Veranstaltungen**

(1) Die Untergebrachten haben das Recht, am Gottesdienst und an anderen religiösen Veranstaltungen ihres Bekenntnisses teilzunehmen.

(2) Die Zulassung zu Gottesdiensten oder religiösen Veranstaltungen einer anderen Religionsgemeinschaft bedarf der Zustimmung der Seelsorgerin oder des Seelsorgers der Religionsgemeinschaft.

(3) Untergebrachte können von der Teilnahme am Gottesdienst oder an anderen religiösen Veranstaltungen ausgeschlossen werden, wenn dies aus überwiegenden Gründen der Sicherheit oder Ordnung geboten ist; die Seelsorgerin oder der Seelsorger soll vorher gehört werden.

§ 76  
**Weltanschauungsgemeinschaften**

Für Angehörige weltanschaulicher Bekenntnisse gelten § 55 Absatz 2 sowie die §§ 74 und 75 entsprechend.

### Abschnitt 13  
Sicherheit und Ordnung

§ 77  
**Grundsatz**

(1) Sicherheit und Ordnung der Einrichtung bilden die Grundlage des auf die Erreichung des Vollzugsziels ausgerichteten Lebens in der Einrichtung und tragen dazu bei, dass in der Einrichtung ein gewaltfreies Klima herrscht.

(2) Die Pflichten und Beschränkungen, die den Untergebrachten zur Aufrechterhaltung der Sicherheit oder Ordnung der Einrichtung auferlegt werden, sind so zu wählen, dass sie in einem angemessenen Verhältnis zu ihrem Zweck stehen und die Untergebrachten nicht mehr und nicht länger als notwendig beeinträchtigen.

§ 78  
**Allgemeine Verhaltenspflichten, Aufarbeitung von Pflichtverstößen**

(1) Die Untergebrachten haben sich so zu verhalten, dass ein geordnetes Zusammenleben in der Einrichtung möglich ist.
Ihr Bewusstsein hierfür ist zu entwickeln und zu stärken.
Die Untergebrachten sind zu einvernehmlicher Streitbeilegung zu befähigen.

(2) Die Untergebrachten haben die Anordnungen der Bediensteten zu befolgen, auch wenn sie sich durch diese beschwert fühlen.

(3) Die Untergebrachten haben ihr Zimmer und die ihnen von der Einrichtung überlassenen Sachen in Ordnung zu halten und schonend zu behandeln.

(4) Die Untergebrachten haben Umstände, die eine Gefahr für das Leben oder eine erhebliche Gefahr für die Gesundheit einer Person bedeuten, unverzüglich zu melden.

(5) Verstoßen die Untergebrachten gegen Pflichten, die ihnen durch oder aufgrund dieses Gesetzes auferlegt sind, sind die Ursachen und Folgen der Verstöße in einem Gespräch aufzuarbeiten.
In geeigneten Fällen können im Wege einvernehmlicher Streitbeilegung Vereinbarungen getroffen werden.
Insbesondere kommen die Wiedergutmachung des Schadens, die Entschuldigung beim Geschädigten, die Erbringung von Leistungen für die Gemeinschaft und der vorübergehende Verbleib auf dem Zimmer in Betracht.

§ 79  
**Absuchung, Durchsuchung**

(1) Die Untergebrachten, ihre Sachen und die Zimmer dürfen mit technischen Mitteln oder sonstigen Hilfsmitteln abgesucht und durchsucht werden.
Die Durchsuchung männlicher Untergebrachter darf nur von Männern, die Durchsuchung weiblicher Untergebrachter darf nur von Frauen vorgenommen werden.
Das Schamgefühl ist zu schonen.

(2) Nur bei Gefahr im Verzug oder auf Anordnung im Einzelfall ist es zulässig, eine mit einer Entkleidung verbundene körperliche Durchsuchung vorzunehmen.
Sie darf bei männlichen Untergebrachten nur in Gegenwart von Männern, bei weiblichen Untergebrachten nur in Gegenwart von Frauen erfolgen.
Sie ist in einem geschlossenen Raum durchzuführen.
Andere Untergebrachte dürfen nicht anwesend sein.

(3) Aufgrund einer allgemeinen Anordnung können die Untergebrachten in der Regel bei der Aufnahme, nach Kontakten mit Besucherinnen und Besuchern sowie nach jeder unbeaufsichtigten Abwesenheit von der Einrichtung nach Absatz 2 durchsucht werden.

(4) Die Anordnung ist zu begründen.
Anordnung, Durchführung und Ergebnis der Durchsuchungen nach den Absätzen 2 und 3 sind aktenkundig zu machen.

§ 80  
**Sichere Unterbringung**

Untergebrachte können in eine Einrichtung verlegt werden, die zu ihrer sicheren Unterbringung besser geeignet ist, wenn in erhöhtem Maße die Gefahr der Entweichung oder Befreiung gegeben ist oder sonst ihr Verhalten oder ihr Zustand eine Gefahr für die Sicherheit der Einrichtung darstellt.

§ 81  
**Maßnahmen zur Feststellung von Suchtmittelgebrauch**

(1) Zur Aufrechterhaltung der Sicherheit oder Ordnung der Einrichtung kann die Leiterin oder der Leiter der Einrichtung allgemein oder im Einzelfall Maßnahmen anordnen, die geeignet sind, den Gebrauch von Suchtmitteln festzustellen.
Diese Maßnahmen dürfen nicht mit einem körperlichen Eingriff verbunden sein.

(2) Verweigern Untergebrachte die Mitwirkung an Maßnahmen nach Absatz 1 ohne hinreichenden Grund, ist davon auszugehen, dass Suchtmittelfreiheit nicht gegeben ist.

(3) Wird verbotener Suchtmittelgebrauch festgestellt, können die Kosten der Maßnahmen den Untergebrachten auferlegt werden.

§ 82  
**Festnahmerecht**

Untergebrachte, die entwichen sind oder sich sonst ohne Erlaubnis außerhalb der Einrichtung aufhalten, können durch die Einrichtung oder auf deren Veranlassung festgenommen und zurückgebracht werden.
Führt die Verfolgung oder die von der Einrichtung veranlasste Fahndung nicht alsbald zur Wiederergreifung, so sind die weiteren Maßnahmen der Vollstreckungsbehörde zu überlassen.

§ 83  
**Besondere Sicherungsmaßnahmen**

(1) Gegen Untergebrachte können besondere Sicherungsmaßnahmen angeordnet werden, wenn nach ihrem Verhalten oder aufgrund ihres seelischen Zustandes in erhöhtem Maße die Gefahr der Entweichung, von Gewalttätigkeiten gegen Personen oder Sachen, der Selbsttötung oder der Selbstverletzung besteht.

(2) Als besondere Sicherungsmaßnahmen sind zulässig:

1.  der Entzug oder die Vorenthaltung von Gegenständen,
2.  die Beobachtung der Untergebrachten, auch mit optisch-elektronischen Hilfsmitteln,
3.  die Trennung von allen anderen Untergebrachten (Absonderung),
4.  die Beschränkung des Aufenthalts außerhalb des Zimmers,
5.  die Unterbringung in einem besonders gesicherten Raum ohne gefährdende Gegenstände und
6.  die Fesselung.

(3) Maßnahmen nach Absatz 2 Nummer 1 und 3 bis 5 sind auch zulässig, wenn die Gefahr einer Befreiung oder eine erhebliche Störung der Ordnung anders nicht vermieden oder behoben werden kann.

(4) Maßnahmen nach Absatz 2 Nummer 1 und 4 sind ferner zulässig, wenn Untergebrachte gegen ihnen durch dieses Gesetz oder aufgrund dieses Gesetzes auferlegte Pflichten verstoßen, die der Sicherheit der Einrichtung dienen, und die Gefahr einer Wiederholung des Verstoßes besteht.
Dasselbe gilt für schwerwiegende Verstöße gegen Pflichten, die der Ordnung der Einrichtung dienen.
Die Dauer der Maßnahmen nach Satz 1 und 2 ist in der Regel auf zwei Wochen beschränkt.

(5) Eine Absonderung von mehr als 24 Stunden Dauer ist nur zulässig, wenn sie zur Abwehr einer in der Person der Untergebrachten liegenden Gefahr unerlässlich ist.

(6) In der Regel dürfen Fesseln nur an den Händen oder an den Füßen angelegt werden.
Im Interesse der Untergebrachten kann die Leiterin oder der Leiter der Einrichtung eine andere Art der Fesselung anordnen.
Die Fesselung wird zeitweise gelockert, soweit dies notwendig ist.

(6) Eine Fesselung, durch die die Bewegungsfreiheit vollständig aufgehoben wird (Fixierung), ist nur zulässig, soweit und solange die Gefahr von Gewalttätigkeiten gegen Personen, der Selbsttötung oder der Selbstverletzung in erhöhtem Maß besteht und die Fixierung zur Abwehr dieser Gefahr unerlässlich ist.

(8) Besteht die Gefahr der Entweichung, dürfen die Untergebrachten bei einer Ausführung, Vorführung oder beim Transport gefesselt werden.

§ 84  
**Anordnung besonderer Sicherungsmaßnahmen, Verfahren**

(1) Besondere Sicherungsmaßnahmen dürfen nur auf Anordnung der Leiterin oder des Leiters der Justizvollzugsanstalt oder der von dieser oder diesem ausdrücklich hierzu ermächtigten Bediensteten durchgeführt werden.
Bei Gefahr im Verzug können auch andere Bedienstete diese Maßnahmen vorläufig anordnen; die Entscheidung der nach Satz 1 Befugten ist unverzüglich einzuholen.

(2) Eine nicht nur kurzfristige Fixierung bedarf einer vorherigen richterlichen Anordnung.
Bei Gefahr im Verzug können auch die Leiterin oder der Leiter der Justizvollzugsanstalt die Fixierung vorläufig anordnen; die Entscheidung des Gerichts ist unverzüglich einzuholen.
Wird die vorläufige Anordnung der Fixierung vor Erlangung einer richterlichen Entscheidung aufgehoben, so ist dies dem Gericht unverzüglich mitzuteilen.

(3) Werden die Untergebrachten ärztlich behandelt oder beobachtet oder bildet ihr seelischer Zustand den Anlass der besonderen Sicherungsmaßnahme, ist vorher eine ärztliche Stellungnahme einzuholen.
Ist dies wegen Gefahr im Verzug nicht möglich, wird die Stellungnahme unverzüglich nachträglich eingeholt.

(4) Die Entscheidung wird den Untergebrachten von der oder dem Anordnenden mündlich eröffnet und mit einer kurzen Begründung schriftlich abgefasst.
Die Anordnung ist aktenkundig zu machen.
Die Anordnung einer Fixierung, deren Grund und deren Verlauf, insbesondere die Art der Überwachung und Betreuung, sind umfassend zu dokumentieren.

(5) Besondere Sicherungsmaßnahmen dürfen nur so weit aufrechterhalten werden, wie es ihr Zweck erfordert.
Sie sind in angemessenen Abständen daraufhin zu überprüfen, ob und in welchem Umfang sie aufrechterhalten werden müssen.
Das Ergebnis der Überprüfungen und die Durchführung der Maßnahmen einschließlich der Beteiligung des ärztlichen Dienstes sind aktenkundig zu machen.

(6) Absonderung, Unterbringung im besonders gesicherten Raum und Fesselung sind der Aufsichtsbehörde unverzüglich mitzuteilen, wenn sie länger als zwei Tage aufrechterhalten werden, die übrigen Maßnahmen nach § 83 Absatz 2, wenn sie länger als zwei Wochen aufrechterhalten werden.
Absonderung und Unterbringung im besonders gesicherten Raum von mehr als 20 Tagen Gesamtdauer innerhalb von zwölf Monaten bedürfen der Zustimmung der Aufsichtsbehörde.

(7) Während der Absonderung, der Fixierung und Unterbringung im besonders gesicherten Raum sind die Untergebrachten in besonderem Maße zu betreuen.
Sind die Untergebrachten fixiert, sind sie durch Bedienstete ständig und in unmittelbarem Sichtkontakt zu beobachten.

(8) Nach Beendigung der Fixierung sind die Untergebrachten auf ihr Recht hinzuweisen, die Rechtmäßigkeit der Anordnung gerichtlich überprüfen zu lassen.
Der Hinweis ist aktenkundig zu machen.

§ 85  
**Ärztliche Überwachung**

(1) Sind die Untergebrachten in einem besonders gesicherten Raum untergebracht oder gefesselt, sucht sie die Ärztin oder der Arzt alsbald und in der Folge täglich auf.
Dies gilt nicht bei einer Fesselung während einer Ausführung, Vorführung oder eines Transportes sowie bei Bewegungen innerhalb der Einrichtung.

(2) Dauert die Absonderung länger als 24 Stunden, so ist die Ärztin oder der Arzt regelmäßig zu hören.

### Abschnitt 14  
Unmittelbarer Zwang

§ 86  
**Begriffsbestimmungen**

(1) Unmittelbarer Zwang ist die Einwirkung auf Personen oder Sachen durch körperliche Gewalt, ihre Hilfsmittel oder Waffen.

(2) Körperliche Gewalt ist jede unmittelbare körperliche Einwirkung auf Personen oder Sachen.

(3) Hilfsmittel der körperlichen Gewalt sind namentlich Fesseln.
Waffen sind Hieb- und Schusswaffen.

(4) Es dürfen nur dienstlich zugelassene Hilfsmittel und Waffen verwendet werden.

§ 87  
**Allgemeine Voraussetzungen**

(1) Bedienstete dürfen unmittelbaren Zwang anwenden, wenn sie Vollzugs- und Sicherungsmaßnahmen rechtmäßig durchführen und der damit verfolgte Zweck auf keine andere Weise erreicht werden kann.

(2) Gegen andere Personen als Untergebrachte darf unmittelbarer Zwang angewendet werden, wenn sie es unternehmen, Untergebrachte zu befreien oder widerrechtlich in die Einrichtung einzudringen, oder wenn sie sich unbefugt darin aufhalten.

(3) Das Recht zu unmittelbarem Zwang aufgrund anderer Regelungen bleibt unberührt.

§ 88  
**Grundsatz der Verhältnismäßigkeit**

(1) Unter mehreren möglichen und geeigneten Maßnahmen des unmittelbaren Zwangs sind diejenigen zu wählen, die den Einzelnen und die Allgemeinheit voraussichtlich am wenigsten beeinträchtigen.

(2) Unmittelbarer Zwang unterbleibt, wenn ein durch ihn zu erwartender Schaden erkennbar außer Verhältnis zu dem angestrebten Erfolg steht.

§ 89  
**Androhung**

Unmittelbarer Zwang ist vorher anzudrohen.
Die Androhung darf nur dann unterbleiben, wenn die Umstände sie nicht zulassen oder unmittelbarer Zwang sofort angewendet werden muss, um eine rechtswidrige Tat, die den Tatbestand eines Strafgesetzes erfüllt, zu verhindern oder eine gegenwärtige Gefahr abzuwenden.

§ 90  
**Schusswaffengebrauch**

(1) Der Gebrauch von Schusswaffen durch Bedienstete innerhalb der Einrichtung ist verboten.
Das Recht zum Schusswaffengebrauch aufgrund anderer Vorschriften durch Polizeivollzugsbedienstete bleibt davon unberührt.

(2) Außerhalb der Einrichtung dürfen Schusswaffen durch Bedienstete nur nach Maßgabe der folgenden Absätze und nur dann gebraucht werden, wenn andere Maßnahmen des unmittelbaren Zwangs bereits erfolglos waren oder keinen Erfolg versprechen.
Gegen Personen ist ihr Gebrauch nur zulässig, wenn der Zweck nicht durch Waffenwirkung gegen Sachen erreicht werden kann.

(3) Schusswaffen dürfen nur die dazu bestimmten Bediensteten gebrauchen und nur, um angriffs- oder fluchtunfähig zu machen.
Ihr Gebrauch unterbleibt, wenn dadurch erkennbar Unbeteiligte mit hoher Wahrscheinlichkeit gefährdet würden.

(4) Der Gebrauch von Schusswaffen ist vorher anzudrohen.
Als Androhung gilt auch ein Warnschuss.
Ohne Androhung dürfen Schusswaffen nur dann gebraucht werden, wenn dies zur Abwehr einer gegenwärtigen Gefahr für Leib oder Leben erforderlich ist.

(5) Gegen Untergebrachte dürfen Schusswaffen gebraucht werden,

1.  wenn sie eine Waffe oder ein anderes gefährliches Werkzeug trotz wiederholter Aufforderung nicht ablegen,
2.  wenn sie eine Meuterei (§ 121 des Strafgesetzbuches) unternehmen oder
3.  um ihre Entweichung zu vereiteln oder um sie wiederzuergreifen.

Satz 1 Nummer 3 findet keine Anwendung auf Untergebrachte im offenen Vollzug.

(6) Gegen andere Personen dürfen Schusswaffen gebraucht werden, wenn sie es unternehmen, Untergebrachte gewaltsam zu befreien.

### Abschnitt 15  
Aufhebung von Maßnahmen, Beschwerde

§ 91  
**Aufhebung von Maßnahmen**

(1) Die Aufhebung von Maßnahmen zur Regelung einzelner Angelegenheiten auf dem Gebiet des Vollzugs richtet sich nach den nachfolgenden Absätzen, soweit dieses Gesetz keine abweichende Bestimmung enthält.

(2) Rechtswidrige Maßnahmen können ganz oder teilweise mit Wirkung für die Vergangenheit und die Zukunft zurückgenommen werden.

(3) Rechtmäßige Maßnahmen können ganz oder teilweise mit Wirkung für die Zukunft widerrufen werden, wenn

1.  aufgrund nachträglich eingetretener oder bekannt gewordener Umstände die Maßnahmen hätten unterbleiben können,
2.  die Maßnahmen missbraucht werden oder
3.  Weisungen nicht befolgt werden.

(4) Begünstigende Maßnahmen dürfen nach Absatz 2 oder Absatz 3 nur aufgehoben werden, wenn die vollzuglichen Interessen an der Aufhebung in Abwägung mit dem schutzwürdigen Vertrauen der Betroffenen auf den Bestand der Maßnahmen überwiegen.
Davon ist auszugehen, wenn die Aufhebung einer Maßnahme unerlässlich ist, um die Sicherheit der Einrichtung zu gewährleisten.

(5) Der gerichtliche Rechtsschutz bleibt unberührt.

§ 92  
**Beschwerderecht**

(1) Die Untergebrachten erhalten Gelegenheit, sich in Angelegenheiten, die sie selbst betreffen, mit Wünschen, Anregungen und Beschwerden an die Leiterin oder den Leiter der Einrichtung zu wenden, hinsichtlich der Entscheidungen nach § 96 Absatz 2 Satz 1 und 2 auch an die Leiterin oder den Leiter der Justizvollzugsanstalt.

(2) Besichtigen Vertreterinnen und Vertreter der Aufsichtsbehörde die Einrichtung, so ist zu gewährleisten, dass die Untergebrachten sich in Angelegenheiten, die sie selbst betreffen, an diese wenden können.

(3) Die Möglichkeit der Dienstaufsichtsbeschwerde bleibt unberührt.

### Abschnitt 16  
Kriminologische Forschung

§ 93  
**Evaluation, kriminologische Forschung****, Berichtspflicht**

Die im Vollzug eingesetzten Maßnahmen, namentlich Therapien und Methoden zur Förderung der Untergebrachten, sind in Zusammenarbeit mit der Forschung und dem kriminologischen Dienst auf ihre Wirksamkeit wissenschaftlich zu überprüfen.
Auf der Grundlage der gewonnenen Erkenntnisse sind Konzepte für den Einsatz vollzuglicher Maßnahmen zu entwickeln und fortzuschreiben.
Auch im Übrigen sind die Erfahrungen mit der Ausgestaltung des Vollzugs durch dieses Gesetz sowie der Art und Weise der Anwendung der Vorschriften dieses Gesetzes zu überprüfen.
Die Berichte an den Landtag nach § 106 Absatz 3 des Brandenburgischen Justizvollzugsgesetzes haben sich auch zum Stand des Vollzugs der Unterbringung in der Sicherungsverwahrung zu verhalten.

### Abschnitt 17  
Aufbau und Organisation der Einrichtung

§ 94   
**Einrichtung**

(1) Der Vollzug erfolgt in einer vom Strafvollzug getrennten Abteilung einer Justizvollzugsanstalt (Einrichtung).
Die Gestaltung der Einrichtung muss therapeutischen Erfordernissen entsprechen und Wohngruppenvollzug ermöglichen.

(2) Es sind bedarfsgerecht ausgestattete Räume für therapeutische Maßnahmen, schulische und berufliche Qualifizierung, Arbeitstraining und Arbeitstherapie sowie zur Ausübung von Arbeit vorzusehen.
Gleiches gilt für Besuche, Freizeit, Sport und Seelsorge.
§ 10 Absatz 3 bleibt unberührt.

(3) Zimmer, Gemeinschafts- und Besuchsräume sind wohnlich und zweckentsprechend auszustatten.

(4) Unterhalten private Unternehmen Betriebe in der Einrichtung, kann die technische und fachliche Leitung ihren Mitarbeiterinnen und Mitarbeitern übertragen werden.

§ 95  
**Festsetzung der Belegungsfähigkeit, Einzelbelegung**

(1) Die Aufsichtsbehörde setzt die Belegungsfähigkeit der Einrichtung so fest, dass eine angemessene Unterbringung gewährleistet ist.
§ 94 Absatz 2 ist zu berücksichtigen.

(2) Zimmer dürfen nur mit einer oder einem Untergebrachten belegt werden.

§ 96  
**Leitung der Einrichtung**

(1) Die Leiterin oder der Leiter der Einrichtung trägt die Verantwortung für den gesamten Vollzug, soweit sich nicht aus Absatz 2 Abweichendes ergibt.
Sie oder er kann einzelne Aufgabenbereiche auf andere Bedienstete übertragen.
Die Aufsichtsbehörde kann sich die Zustimmung zur Übertragung vorbehalten.

(2) Die Leiterin oder der Leiter der Justizvollzugsanstalt trifft die Entscheidungen, die Auswirkungen auf die gesamte Justizvollzugsanstalt haben.
Auf Vorschlag der Leiterin oder des Leiters der Einrichtung entscheidet sie oder er über Zwangsmaßnahmen auf dem Gebiet der Gesundheitsfürsorge nach § 72, über mit einer Entkleidung verbundene körperliche Durchsuchungen nach § 79 Absatz 2 und 3 und ordnet besondere Sicherungsmaßnahmen nach § 84 Absatz 1 an.
Sie oder er vertritt die Einrichtung nach außen.
Absatz 1 Satz 2 und 3 gilt entsprechend.

§ 97  
**Bedienstete**

(1) Die Einrichtung wird mit dem für die Erreichung des Vollzugsziels und die Erfüllung ihrer Aufgaben erforderlichen Personal, insbesondere im medizinischen, psychologischen, pädagogischen und sozialen Dienst, im allgemeinen Vollzugsdienst und im Werkdienst, ausgestattet, um eine Betreuung nach § 66c Absatz 1 Nummer 1 des Strafgesetzbuches zu gewährleisten.

(2) Das Personal muss für die Betreuung und Behandlung der Untergebrachten persönlich geeignet und fachlich qualifiziert sein.
Fortbildungen sowie Praxisberatung und Praxisbegleitung für die Bediensteten werden regelmäßig durchgeführt.

(3) Die Bediensteten des allgemeinen Vollzugsdienstes, des psychologischen und sozialen Dienstes sollen Wohngruppen zugeordnet werden.
Eine Betreuung in den Wohngruppen ist auch in der beschäftigungs- und arbeitsfreien Zeit der Untergebrachten, insbesondere am Wochenende, zu gewährleisten.

§ 98  
**Seelsorgerin, Seelsorger**

(1) Die Anstaltsseelsorgerin oder der Anstaltsseelsorger der Justizvollzugsanstalt nehmen auch die Aufgaben der Seelsorgerinnen und Seelsorger der Einrichtung wahr.

(2) Ist für die Angehörigen einer Religionsgemeinschaft aufgrund ihrer geringen Anzahl eine Anstaltsseelsorgerin oder ein Anstaltsseelsorger im Hauptamt nicht bestellt oder vertraglich verpflichtet, ist die seelsorgerische Betreuung auf andere Weise zuzulassen.

§ 99  
**Medizinische Versorgung**

(1) Die ärztliche Versorgung ist sicherzustellen.

(2) Die Pflege der Kranken soll von Bediensteten ausgeführt werden, die eine Erlaubnis nach dem Krankenpflegegesetz besitzen.
Solange diese nicht zur Verfügung stehen, können auch Bedienstete eingesetzt werden, die eine sonstige Ausbildung in der Krankenpflege erfahren haben.

§ 100  
**Interessenvertretung der Untergebrachten**

(1) Den Untergebrachten soll ermöglicht werden, Vertretungen zu bilden.
Diese können in Angelegenheiten von gemeinsamem Interesse, die sich ihrer Eigenart nach für eine Mitwirkung eignen, Vorschläge und Anregungen an die Einrichtung herantragen.
Diese sind mit der Vertretung zu erörtern.

(2) Soweit Interessen und Belange der Untergebrachten in der Justizvollzugsanstalt berührt sind, wirkt die Interessenvertretung nach Absatz 1 mit der Interessenvertretung der Gefangenen (§ 113 Satz 1 des Brandenburgischen Justizvollzugsgesetzes) zusammen.

§ 101  
**Hausordnung**

(1) Die Leiterin oder der Leiter der Einrichtung erlässt zur Gestaltung und Organisation des Vollzugsalltags eine Hausordnung auf der Grundlage dieses Gesetzes.
Vor deren Erlass oder Änderung beteiligt sie oder er die Interessenvertretung der Untergebrachten.
Die Aufsichtsbehörde kann sich die Genehmigung der Hausordnung vorbehalten.

(2) Die Wohngruppen können sich eine Wohngruppenordnung geben.
Die Leiterin oder der Leiter der Einrichtung erklärt die Wohngruppenordnung für verbindlich, wenn sie sich im Rahmen der gesetzlichen Vorgaben und der Hausordnung hält.

### Abschnitt 18  
Aufsicht, Beirat

§ 102  
**Aufsichtsbehörde**

(1) Das für den Justizvollzug zuständige Mitglied der Landesregierung führt die Aufsicht über die Einrichtung (Aufsichtsbehörde).

(2) An der Aufsicht über die Gesundheitsfürsorge sowie die Betreuung und Behandlung der Untergebrachten sind pädagogische, sozialpädagogische, psychologische, psychiatrische und medizinische Fachkräfte zu beteiligen.

(3) Die Aufsichtsbehörde kann sich Entscheidungen über Verlegungen und Überstellungen vorbehalten.

§ 103  
**Vollstreckungsplan, Vollzugsgemeinschaften**

(1) Die Aufsichtsbehörde regelt die örtliche und sachliche Zuständigkeit der Einrichtung in einem Vollstreckungsplan.

(2) Im Rahmen von Vollzugsgemeinschaften kann der Vollzug auch in Einrichtungen anderer Länder vorgesehen werden.

§ 104  
**Beirat**

Der bei der Justizvollzugsanstalt gebildete Beirat nimmt auch die Aufgaben des Beirats der Einrichtung wahr.

### Abschnitt 19   
Verhinderung von Mobilfunkverkehr

§ 105  
**Verbot und Störung des Mobilfunkverkehrs**

(1) Der Besitz und die Benutzung von Geräten zur funkbasierten Übertragung von Informationen sind auf dem Gelände der Einrichtung verboten.
Die Leiterin oder der Leiter der Einrichtung kann abweichende Regelungen treffen.

(2) Die Einrichtung darf technische Geräte betreiben, die

1.  das Auffinden von Geräten zur Funkübertragung ermöglichen,
2.  Geräte zur Funkübertragung zum Zwecke des Auffindens aktivieren können oder
3.  Frequenzen stören oder unterdrücken, die der Herstellung oder Aufrechterhaltung unerlaubter Funkverbindungen auf dem Gelände der Einrichtung dienen.

Sie hat die von der Bundesnetzagentur gemäß § 55 Absatz 1 Satz 5 des Telekommunikationsgesetzes festgelegten Rahmenbedingungen zu beachten.
Frequenznutzungen außerhalb des Geländes der Einrichtung dürfen nicht erheblich gestört werden.

### Abschnitt 20  
Datenschutz

§ 106  
**Anwendung des Brandenburgischen Polizei-, Justizvollzugs- und Maßregelvollzugsdatenschutzgesetzes**

Das Brandenburgische Polizei-, Justizvollzugs- und Maßregelvollzugsdatenschutzgesetz findet Anwendung, soweit in diesem Gesetz nicht Abweichendes geregelt ist.

§ 107  
**Grundsatz, Begriffsbestimmungen**

(1) Die Einrichtung und die Aufsichtsbehörde dürfen personenbezogene Daten verarbeiten, soweit deren Kenntnis für vollzugliche Zwecke erforderlich ist.

(2) Vollzugliche Zwecke sind die Erreichung des Vollzugsziels, der Schutz der Allgemeinheit vor weiteren Straftaten der Untergebrachten, die Aufrechterhaltung der Sicherheit und Ordnung der Einrichtung sowie die Sicherung des Vollzugs.

§ 108  
**Erhebung von Daten über Untergebrachte bei Dritten**

Daten über Untergebrachte dürfen ohne deren Kenntnis bei Dritten nur erhoben werden, wenn

1.  eine Rechtsvorschrift dies vorsieht oder zwingend voraussetzt oder
2.  die zu erfüllende Aufgabe nach Art oder Zweck eine Erhebung bei anderen Personen oder Stellen erforderlich macht

und keine Anhaltspunkte dafür bestehen, dass überwiegende schutzwürdige Interessen der Untergebrachten beeinträchtigt werden.

§ 109  
**Erhebung von Daten über andere Personen**

Daten über andere Personen als die Untergebrachten dürfen für vollzugliche Zwecke ohne deren Kenntnis nur erhoben werden, wenn dies unerlässlich ist und die Art der Erhebung schutzwürdige Interessen dieser Personen nicht beeinträchtigt.

§ 110  
**Unterrichtungspflichten**

Die Betroffenen werden über eine ohne ihre Kenntnis vorgenommene Erhebung ihrer Daten unterrichtet, soweit vollzugliche Zwecke dadurch nicht gefährdet werden.
Sind die Daten bei anderen Personen oder Stellen erhoben worden, kann die Unterrichtung unterbleiben, wenn

1.  die Daten nach einer Rechtsvorschrift oder ihrem Wesen nach, namentlich wegen des überwiegenden berechtigten Interesses Dritter, geheim gehalten werden müssen oder
2.  der Aufwand der Unterrichtung außer Verhältnis zum Schutzzweck steht und keine Anhaltspunkte dafür bestehen, dass überwiegende schutzwürdige Interessen der Betroffenen beeinträchtigt werden.

§ 111  
**Besondere Formen der Datenerhebung**

(1) Zur Sicherung des Vollzugs und zur Aufrechterhaltung der Sicherheit oder Ordnung der Einrichtung, insbesondere zur Identitätsfeststellung, sind mit Kenntnis der Untergebrachten folgende erkennungsdienstliche Maßnahmen zulässig:

1.  die Abnahme von Finger- und Handflächenabdrücken,
2.  die Aufnahme von Lichtbildern,
3.  die Feststellung äußerlicher körperlicher Merkmale,
4.  die elektronische Erfassung biometrischer Merkmale und
5.  Messungen.

(2) Aus Gründen der Sicherheit oder Ordnung ist die Beobachtung einzelner Bereiche des Gebäudes der Einrichtung einschließlich des Gebäudeinneren, des Geländes der Einrichtung oder der unmittelbaren Umgebung der Einrichtung mit optisch-elektronischen Hilfsmitteln (Videoüberwachung) zulässig.
Eine Aufzeichnung der Videobilder darf nur im Bereich der unmittelbaren Außensicherung der Einrichtung sowie darüber hinaus im Einzelfall auf Anordnung der Leiterin oder des Leiters der Einrichtung in besonders sicherheitsrelevanten Bereichen erfolgen.
Die Videoüberwachung ist durch geeignete Maßnahmen erkennbar zu machen, soweit ihr Zweck dadurch nicht vereitelt wird.
Eine Videoüberwachung von Zimmern ist ausgeschlossen.
§ 83 Absatz 2 Nummer 2 bleibt unberührt.

(3) Das Betreten des Geländes der Einrichtung durch vollzugsfremde Personen kann davon abhängig gemacht werden, dass diese zur Identitätsfeststellung

1.  ihren Vornamen, ihren Namen und ihre Anschrift angeben und durch amtliche Ausweise nachweisen und
2.  die Erfassung biometrischer Merkmale des Gesichts, der Augen, der Hände, der Stimme oder der Unterschrift dulden, soweit dies erforderlich ist, um den Austausch von Untergebrachten zu verhindern.

(4) Die Leiterin oder der Leiter der Einrichtung kann das Auslesen von elektronischen Datenspeichern sowie elek­tronischen Geräten mit Datenspeichern anordnen, die Untergebrachte ohne Erlaubnis besitzen, wenn konkrete Anhaltspunkte die Annahme rechtfertigen, dass dies für vollzugliche Zwecke erforderlich ist.
Die Untergebrachten sind bei der Aufnahme über die Möglichkeit des Auslesens von Datenspeichern zu belehren.

§ 112  
**Übermittlung und Nutzung für weitere Zwecke**

(1) Für eine Übermittlung oder Nutzung von personenbezogenen Daten stehen die Zwecke des gerichtlichen Rechtsschutzes im Zusammenhang mit diesem Gesetz den vollzuglichen Zwecken des § 107 Absatz 2 gleich.

(2) Die Übermittlung und Nutzung von personenbezogenen Daten ist über Absatz 1 hinaus auch zulässig, soweit dies erforderlich ist

1.  zur Abwehr von sicherheitsgefährdenden oder geheimdienstlichen Tätigkeiten für eine fremde Macht oder von Bestrebungen im Geltungsbereich des Grundgesetzes, die durch Anwendung von Gewalt oder darauf gerichtete Vorbereitungshandlungen
    1.  gegen die freiheitliche demokratische Grundordnung, den Bestand oder die Sicherheit des Bundes oder eines Landes gerichtet sind,
    2.  eine ungesetzliche Beeinträchtigung der Amtsführung der Verfassungsorgane des Bundes oder eines Landes oder ihrer Mitglieder zum Ziel haben oder
    3.  auswärtige Belange der Bundesrepublik Deutschland gefährden,
2.  zur Abwehr erheblicher Nachteile für das Gemeinwohl oder einer Gefahr für die öffentliche Sicherheit,
3.  zur Abwehr einer schwerwiegenden Beeinträchtigung der Rechte einer anderen Person,
4.  zur Verhinderung oder Verfolgung von Straftaten sowie zur Verhinderung oder Verfolgung von Ordnungswidrigkeiten, durch welche die Sicherheit oder Ordnung der Einrichtung gefährdet werden oder
5.  für Maßnahmen der Strafvollstreckung oder strafvollstreckungsrechtliche Entscheidungen.

Weitergehende Übermittlungspflichten gegenüber der Verfassungsschutzbehörde nach § 14 Absatz 1 des Brandenburgischen Verfassungsschutzgesetzes bleiben unberührt.

§ 113  
**Datenübermittlung an öffentliche Stellen**

(1) Den zuständigen öffentlichen Stellen dürfen personenbezogene Daten übermittelt werden, soweit dies erforderlich ist für

1.  die Vorbereitung und Durchführung von Maßnahmen der Gerichtshilfe, Bewährungshilfe, Führungsaufsicht oder forensischen Ambulanzen,
2.  Entscheidungen in Gnadensachen,
3.  gesetzlich angeordnete Statistiken der Rechtspflege,
4.  Maßnahmen der für Sozialleistungen zuständigen Leistungsträger,
5.  die Einleitung von Hilfsmaßnahmen für Angehörige der Untergebrachten im Sinne des § 11 Absatz 1 Nummer 1 des Strafgesetzbuches,
6.  dienstliche Maßnahmen der Bundeswehr im Zusammenhang mit der Aufnahme und Entlassung von Soldatinnen und Soldaten,
7.  ausländerrechtliche Maßnahmen oder
8.  die Durchführung der Besteuerung.

Eine Übermittlung für andere Zwecke ist auch zulässig, soweit eine andere gesetzliche Bestimmung dies vorsieht und sich dabei ausdrücklich auf Daten über Untergebrachte bezieht.

(2) Absatz 1 gilt entsprechend, wenn sich die öffentlichen Stellen zur Erfüllung ihrer Aufgaben nichtöffentlicher Stellen bedienen und deren Mitwirkung ohne Übermittlung der Daten unmöglich oder wesentlich erschwert würde.

§ 114  
**Verarbeitung besonders erhobener Daten**

(1) Bei der Überwachung der Besuche, der Telefongespräche, anderer Formen der Telekommunikation oder des Schriftwechsels sowie bei der Überprüfung des Inhalts von Paketen bekannt gewordene personenbezogene Daten dürfen für die in § 107 Absatz 2 und § 112 Absatz 1 genannten Zwecke verarbeitet werden.

(2) Die aufgrund von erkennungsdienstlichen Maßnahmen nach § 111 Absatz 1 gewonnenen Daten und Unterlagen werden zu den Personalakten der Untergebrachten genommen oder in personenbezogenen Dateien gespeichert.
Sie dürfen nur für die in § 111 Absatz 1, § 112 Absatz 2 Nummer 4 genannten Zwecke verarbeitet oder den Vollstreckungs- und Strafverfolgungsbehörden zum Zwecke der Fahndung und Festnahme der entwichenen oder sich sonst ohne Erlaubnis außerhalb der Einrichtung aufhaltenden Untergebrachten übermittelt werden.

(3) Die zur Identifikation von vollzugsfremden Personen nach § 111 Absatz 3 erhobenen Daten dürfen ausschließlich verarbeitet werden

1.  zum Zweck des Abgleichs beim Verlassen der Einrichtung oder
2.  zur Verfolgung von während des Aufenthalts in der Einrichtung begangenen Straftaten; in diesem Fall können die Daten auch an Strafverfolgungsbehörden ausschließlich zum Zwecke der Verfolgung dieser Straftaten übermittelt werden.

(4) Die beim Auslesen von Datenspeichern nach § 111 Absatz 4 erhobenen Daten dürfen nur verarbeitet werden, soweit dies zu den dort genannten Zwecken erforderlich ist.
Dies gilt nicht, soweit sie zum Kernbereich der privaten Lebensgestaltung der Untergebrachten oder Dritter gehören.

(5) Nach § 109 erhobene Daten über Personen, die nicht Untergebrachte sind, dürfen nur zur Erfüllung des Erhebungszwecks oder für die in § 112 Absatz 2 Nummer 1 bis 3 geregelten Zwecke oder zur Verhinderung oder Verfolgung von Straftaten von erheblicher Bedeutung verarbeitet werden.

§ 115  
**Mitteilung über den Aufenthalt im Vollzug**

(1) Die Einrichtung oder die Aufsichtsbehörde darf öffentlichen oder nichtöffentlichen Stellen auf schriftlichen Antrag mitteilen, ob sich eine Person im Vollzug befindet und ob die Entlassung voraussichtlich innerhalb eines Jahres bevorsteht, soweit

1.  die Mitteilung zur Erfüllung der in der Zuständigkeit der öffentlichen Stelle liegenden Aufgaben erforderlich ist oder
2.  von nichtöffentlichen Stellen
    1.  ein berechtigtes Interesse an dieser Mitteilung glaubhaft dargelegt wird und
    2.  die Untergebrachten kein schutzwürdiges Interesse an dem Ausschluss der Übermittlung haben.

(2) Die Mitteilung ist in der Personalakte der Untergebrachten zu dokumentieren.

(3) Den Verletzten einer Straftat sowie deren Rechtsnachfolgerinnen und Rechtsnachfolgern können darüber hinaus auf schriftlichen Antrag Auskünfte über die Entlassungsadresse oder die Vermögensverhältnisse von Untergebrachten erteilt werden, wenn die Erteilung zur Feststellung oder Durchsetzung von Rechtsansprüchen im Zusammenhang mit der Straftat erforderlich ist.

(4) Die Untergebrachten werden vor der Mitteilung gehört, es sei denn, es ist zu besorgen, dass dadurch die Verfolgung des Interesses der Antragstellerinnen und Antragsteller vereitelt oder wesentlich erschwert werden würde, und eine Abwägung ergibt, dass dieses Interesse das Interesse der Untergebrachten an ihrer vorherigen Anhörung überwiegt.
Ist die Anhörung unterblieben, werden die betroffenen Untergebrachten über die Mitteilung nachträglich unterrichtet.

§ 116  
**Überlassung von Akten**

(1) Akten dürfen nur

1.  anderen Einrichtungen und Aufsichtsbehörden,
2.  der Gerichtshilfe, der Jugendgerichtshilfe, der Bewährungshilfe, den Führungsaufsichtsstellen und den forensischen Ambulanzen,
3.  den für strafvollzugs-, strafvollstreckungs- und strafrechtliche Entscheidungen zuständigen Gerichten und
4.  den Strafvollstreckungs- und Strafverfolgungsbehörden

überlassen oder im Falle elektronischer Aktenführung in Form von Duplikaten übermittelt werden.

(2) Die Überlassung an andere öffentliche Stellen und an nichtöffentliche Stellen, denen Aufgaben nach Absatz 1 Nummer 2 gemäß § 113 Absatz 2 übertragen sind, ist zulässig, soweit die Erteilung einer Auskunft einen unvertretbaren Aufwand erfordert oder nach Darlegung der Akteneinsicht begehrenden Stellen für die Erfüllung der Aufgabe nicht ausreicht.
Entsprechendes gilt für die Überlassung von Akten an die von einer Einrichtung oder Aufsichtsbehörde, einer Strafvollstreckungsbehörde oder einem Gericht mit Gutachten beauftragten Stellen.

§ 117  
**Kenntlichmachung in der Einrichtung, Lichtbildausweise**

(1) Mit Ausnahme des religiösen oder weltanschaulichen Bekenntnisses, personenbezogener Daten über die ethnische Herkunft, sexuelle Identität, politische Meinungen, die Gewerkschaftszugehörigkeit und der personenbezogenen Daten von Untergebrachten, die anlässlich ärztlicher Untersuchungen erhoben worden sind, dürfen Daten von Untergebrachten in der Einrichtung allgemein kenntlich gemacht werden, soweit dies für ein geordnetes Zusammenleben erforderlich ist.

(2) Die Einrichtung kann die Untergebrachten verpflichten, einen Lichtbildausweis mit sich zu führen, wenn dies aus Gründen der Sicherheit oder schwerwiegenden Gründen der Ordnung der Einrichtung erforderlich ist.
Dieser ist bei der Verlegung in eine andere Einrichtung oder bei der Entlassung einzuziehen oder zu vernichten.

§ 118  
**Offenbarungspflichten und -befugnisse der Berufsgeheimnisträgerinnen und Berufsgeheimnisträger**

(1) Soweit in den folgenden Absätzen nicht Abweichendes geregelt ist, unterliegen

1.  Ärztinnen und Ärzte, Zahnärztinnen und Zahnärzte oder Angehörige eines anderen Heilberufs, der für die Berufsausübung oder die Führung der Berufsbezeichnung eine staatlich geregelte Ausbildung erfordert,
2.  Berufspsychologinnen und Berufspsychologen mit staatlich anerkannter wissenschaftlicher Abschlussprüfung und
3.  staatlich anerkannte Sozialarbeiterinnen und Sozialarbeiter oder staatlich anerkannte Sozialpädagoginnen und Sozialpädagogen

hinsichtlich der ihnen als Berufsgeheimnisträgerin oder Berufsgeheimnisträger von Untergebrachten anvertrauten oder sonst über Untergebrachte bekannt gewordenen Geheimnisse auch gegenüber der Einrichtung und der Aufsichtsbehörde der Schweigepflicht.

(2) Die in Absatz 1 genannten Personen haben sich gegenüber der Leiterin oder dem Leiter der Einrichtung zu offenbaren, soweit dies für die Aufgabenerfüllung der Einrichtung oder der Aufsichtsbehörde oder zur Abwehr von erheblichen Gefahren für Leib oder Leben von Untergebrachten oder Dritten erforderlich ist.

(3) Ärztinnen und Ärzte sind gegenüber der Leiterin oder dem Leiter der Einrichtung zur Offenbarung ihnen im Rahmen der allgemeinen Gesundheitsfürsorge bekannt gewordener Geheimnisse befugt, soweit dies für die Aufgabenerfüllung der Einrichtung oder der Aufsichtsbehörde unerlässlich oder zur Abwehr von erheblichen Gefahren für Leib oder Leben von Untergebrachten oder Dritten erforderlich ist.
Sonstige Offenbarungsbefugnisse und -pflichten bleiben unberührt.

(4) Die Untergebrachten sind vor der Erhebung über die nach den Absätzen 2 und 3 bestehenden Offenbarungspflichten zu unterrichten.

(5) Die nach den Absätzen 2 und 3 offenbarten Daten dürfen nur für den Zweck, für den sie offenbart wurden oder für den eine Offenbarung zulässig gewesen wäre, und nur unter denselben Voraussetzungen verarbeitet oder genutzt werden, unter denen eine in Absatz 1 genannte Person selbst hierzu befugt wäre.
Die Leiterin oder der Leiter der Einrichtung kann unter diesen Voraussetzungen die unmittelbare Offenbarung gegenüber bestimmten Bediensteten allgemein zulassen.

(6) Sofern Ärztinnen und Ärzte oder Psychologinnen und Psychologen außerhalb des Vollzugs mit der Untersuchung, Behandlung oder Betreuung von Untergebrachten beauftragt werden, gelten die Absätze 1 bis 3 mit der Maßgabe entsprechend, dass die beauftragten Personen auch zur Unterrichtung der oder des in der Einrichtung tätigen Ärztin oder Arztes oder der oder des in der Einrichtung mit der Behandlung oder Betreuung der Untergebrachten betrauten Psychologin oder Psychologen befugt sind.

§ 119  
**_(aufgehoben)_**

§ 120  
**Auskunft an die Betroffenen, Akteneinsicht**

Die Auskunftserteilung und die Gewährung von Akteneinsicht unterbleiben, soweit die Auskunft oder die Einsichtnahme die ordnungsgemäße Erfüllung der Aufgaben der datenverarbeitenden Stelle oder die Erreichung des Vollzugsziels gefährden würden.

§ 121  
**Auskunft und Akteneinsicht zur Wahrnehmung der Aufgaben des Europäischen Ausschusses zur Verhütung von Folter und unmenschlicher oder erniedrigender Behandlung oder Strafe**

Den Mitgliedern einer Delegation des Europäischen Ausschusses zur Verhütung von Folter und unmenschlicher oder erniedrigender Behandlung oder Strafe wird während des Besuchs in der Einrichtung Einsicht in die Untergebrachtenpersonalakten, Gesundheitsakten und Krankenblätter gewährt oder Auskunft aus diesen Akten erteilt, soweit dies zur Wahrnehmung der Aufgaben des Ausschusses unerlässlich ist.

§ 122  
**Löschung**

Die in Dateien mit Ausnahme der in Personalakten der Untergebrachten, Gesundheitsakten, Therapieakten, psychologischen und pädagogischen Testunterlagen und Krankenblättern sowie Untergebrachtenbüchern gespeicherten personenbezogenen Daten sind spätestens zwei Jahre nach der Entlassung oder der Verlegung der Untergebrachten in eine andere Einrichtung zu löschen.
Hiervon können bis zum Ablauf der Aufbewahrungsfrist nach § 125 die Angaben über Familienname, Vorname, Geburtsname, Geburtstag, Geburtsort, Eintritts- und Austrittsdatum der Untergebrachten ausgenommen werden, soweit dies für das Auffinden der Personalakte der Untergebrachten erforderlich ist.

§ 123  
**Löschung besonders erhobener Daten**

(1) Erkennungsdienstliche Unterlagen mit Ausnahme von Lichtbildern und der Beschreibung von körperlichen Merkmalen der Untergebrachten, die nach § 111 Absatz 1 erkennungsdienstlich behandelt worden sind, sind nach ihrer Entlassung aus dem Vollzug unverzüglich zu löschen, sobald die Vollstreckung der richterlichen Entscheidung, die dem Vollzug zugrunde gelegen hat, abgeschlossen ist.

(2) Mittels optisch-elektronischer Hilfsmittel nach § 111 Absatz 2 erhobene Daten sind spätestens nach 72 Stunden zu löschen, soweit nicht die weitere Aufbewahrung im Einzelfall zu Beweiszwecken unerlässlich ist.

(3) Nach § 111 Absatz 3 Nummer 2 erhobene Daten sind unverzüglich zu löschen, nachdem die Personen die Einrichtung verlassen haben.

(4) Nach § 111 Absatz 4 erhobene Daten sind unverzüglich zu löschen, soweit eine Verarbeitung nach § 114 Absatz 4 unzulässig ist.
Die Daten sind spätestens 72 Stunden nach dem Ende des Auslesens zu löschen, soweit nicht die weitere Aufbewahrung im Einzelfall zu Beweiszwecken unerlässlich ist.

§ 124  
**Sperrung und Verwendungsbeschränkungen**

(1) Personenbezogene Daten in den in § 122 Satz 1 genannten Dateien sind nach Ablauf von zwei Jahren seit der Entlassung oder der Verlegung der Untergebrachten in eine andere Einrichtung zu kennzeichnen, um ihre weitere Verarbeitung oder Nutzung einzuschränken (Sperrung).

(2) Die nach Absatz 1 gesperrten Daten dürfen nur übermittelt oder genutzt werden, soweit dies unerlässlich ist

1.  zur Verfolgung von Straftaten,
2.  für die Durchführung wissenschaftlicher Forschungsvorhaben gemäß § 93,
3.  zur Behebung einer bestehenden Beweisnot oder
4.  zur Feststellung, Durchsetzung oder Abwehr von Rechtsansprüchen im Zusammenhang mit dem Vollzug.

(3) Die Sperrung nach Absatz 1 endet, wenn die Untergebrachten aufgenommen werden oder die Betroffenen eingewilligt haben.

§ 125  
**Aufbewahrungsfristen, Fristberechnung**

(1) Bei der Aufbewahrung der nach § 124 gesperrten Daten darf eine Frist von 30 Jahren nicht überschritten werden.

(2) Die Aufbewahrungsfrist beginnt mit dem auf das Jahr der aktenmäßigen Weglegung folgenden Kalenderjahr.

(3) Die Bestimmungen des Brandenburgischen Archivgesetzes bleiben unberührt.

### Abschnitt 21  
Schlussbestimmungen

§ 126  
**Einschränkung von Grundrechten**

Durch dieses Gesetz werden die Rechte auf körperliche Unversehrtheit und Freiheit der Person (Artikel 2 Absatz 2 des Grundgesetzes, Artikel 8 Absatz 1 und Artikel 9 Absatz 1 der Verfassung des Landes Brandenburg), auf Unverletzlichkeit des Brief-, Post- und Fernmeldegeheimnisses (Artikel 10 Absatz 1 des Grundgesetzes, Artikel 16 Absatz 1 der Verfassung des Landes Brandenburg), auf Meinungsfreiheit (Artikel 19 Absatz 1 der Verfassung des Landes Brandenburg) und auf Datenschutz (Artikel 11 der Verfassung des Landes Brandenburg) eingeschränkt.

§ 127  
**Inkrafttreten**

(1) Dieses Gesetz tritt vorbehaltlich des Absatzes 2 am 1.
Juni 2013 in Kraft.

(2) § 60 Absatz 3 Satz 3 tritt am Tag nach der Verkündung dieses Gesetzes in Kraft.

Potsdam, den 16.
Mai 2013

Der Präsident  
des Landtages Brandenburg

Gunter Fritsch