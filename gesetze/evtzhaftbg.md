## Gesetz zur Durchführung des Artikels 12 Absatz 2a Unterabsatz 1 der Verordnung über den Europäischen Verbund für territoriale Zusammenarbeit (EVTZ-Haftungsbeschränkungsgesetz - EVTZHaftbG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Haftungsbeschränkung

Ist die Haftung mindestens eines Mitglieds eines Europäischen Verbunds für territoriale Zusammenarbeit aus einem Mitgliedstaat der Europäischen Union nach Maßgabe des nationalen Rechts, dem dieses Mitglied unterliegt, beschränkt, so können die anderen Mitglieder ihre Haftung ebenfalls in der Übereinkunft beschränken.

#### § 2 Inkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 18.
Dezember 2020

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke