## Gesetz über die Befugnisse des Justizwachtmeisterdienstes (Justizwachtmeisterbefugnissegesetz - JWMBG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Anwendungsbereich

(1) Der Justizwachtmeisterdienst hat zur Wahrnehmung seiner Aufgaben im Vorführdienst, bei der Bewachung Gefangener, bei der Aufrechterhaltung der Sicherheit und Ordnung in Amtsgebäuden und bei der Vollziehung richterlicher oder staatsanwaltschaftlicher Anordnungen die in diesem Gesetz vorgesehenen Befugnisse.

(2) Die Befugnisse der Inhaberinnen und Inhaber des Hausrechts in Amtsgebäuden sowie diejenigen der Justizbediensteten aufgrund anderer Vorschriften bleiben unberührt.
Zur Vollziehung von Maßnahmen der Sitzungspolizei ist dieses Gesetz nur anwendbar, soweit Bundesrecht keine Regelung enthält.

(3) Die Aufgaben und Befugnisse der Polizei und des Justizvollzugsdienstes bleiben unberührt.

#### § 2 Allgemeine Befugnisse

Der Justizwachtmeisterdienst kann zur Wahrnehmung seiner Aufgaben im Einzelfall die nach pflichtgemäßem Ermessen notwendigen Maßnahmen treffen, um seine in § 1 Absatz 1 genannten Aufgaben wahrzunehmen, soweit nicht die §§ 3 bis 14 die Befugnisse des Justizwachtmeisterdienstes besonders regeln.

#### § 3 Identitätsfeststellung

(1) Der Justizwachtmeisterdienst kann die Identität einer Person feststellen,

1.  zur Abwehr einer Gefahr oder
2.  wenn sie sich in einem Amtsgebäude oder einem besonders gefährdeten Objekt oder in dessen unmittelbarer Nähe aufhält und Tatsachen die Annahme rechtfertigen, dass in oder an dem Amtsgebäude oder anderen Objekt Straftaten begangen werden sollen, durch die Personen oder diese Objekte gefährdet sind, und dies aufgrund der Gefährdungslage oder auf die Person bezogener Anhaltspunkte erforderlich ist.

(2) Der Justizwachtmeisterdienst kann die zur Feststellung der Identität erforderlichen Maßnahmen treffen.
Er kann die betroffene Person insbesondere anhalten, sie nach ihren Personalien befragen und verlangen, dass sie Angaben zur Feststellung ihrer Identität macht und mitgeführte Ausweispapiere zur Prüfung aushändigt.
Die betroffene Person kann festgehalten werden, wenn die Identität auf andere Weise nicht oder nur unter erheblichen Schwierigkeiten festgestellt werden kann.
Unter den Voraussetzungen des Satzes 3 können die betroffene Person sowie die von ihr mitgeführten Sachen durchsucht werden.

#### § 4 Platzverweisung

Der Justizwachtmeisterdienst kann zur Abwehr einer Gefahr eine Person vorübergehend von einem Ort verweisen oder ihr vorübergehend das Betreten eines Ortes verbieten.
Die Platzverweisung kann ferner gegen Personen angeordnet werden, die den Einsatz der Feuerwehr oder von Hilfs- oder Rettungsdiensten behindern.

#### § 5 Durchsuchung von Personen

(1) Der Justizwachtmeisterdienst kann außer in den Fällen des § 3 Absatz 2 Satz 4 eine Person durchsuchen, wenn

1.  sie nach diesem Gesetz oder anderen Rechtsvorschriften festgehalten werden kann,
2.  Tatsachen die Annahme rechtfertigen, dass sie Sachen mit sich führt, die sichergestellt werden dürfen,
3.  sie sich erkennbar in einem die freie Willensbestimmung ausschließenden Zustand oder sonst in hilfloser Lage befindet oder
4.  sie sich in einem Objekt im Sinne des § 3 Absatz 1 Nummer 2 oder in dessen unmittelbarer Nähe aufhält und Tatsachen die Annahme rechtfertigen, dass in oder an Objekten dieser Art Straftaten begangen werden sollen, durch die Personen oder diese Objekte gefährdet sind.

(2) Der Justizwachtmeisterdienst kann eine Person, deren Identität nach diesem Gesetz oder anderen Rechtsvorschriften festgestellt werden soll, nach Waffen, anderen gefährlichen Werkzeugen und Explosivmitteln durchsuchen, wenn das nach den Umständen zum Schutz der oder des Bediensteten oder Dritter gegen eine Gefahr für Leib oder Leben erforderlich erscheint.
Dasselbe gilt, wenn eine Person nach anderen Rechtsvorschriften vorgeführt oder zur Durchführung einer Maßnahme an einen anderen Ort gebracht werden soll.

(3) Personen dürfen nur von Personen gleichen Geschlechts, Ärztinnen oder Ärzten durchsucht werden; das gilt nicht, wenn die sofortige Durchsuchung zum Schutz gegen eine Gefahr für Leib oder Leben erforderlich ist.

#### § 6 Durchsuchung von Sachen

(1) Der Justizwachtmeisterdienst kann außer in den Fällen des § 3 Absatz 2 Satz 4 eine Sache durchsuchen, wenn

1.  sie von einer Person mitgeführt wird, die nach § 5 durchsucht werden darf,
2.  Tatsachen die Annahme rechtfertigen, dass sich in ihr eine Person befindet, die
    1.  in Gewahrsam genommen werden darf,
    2.  widerrechtlich festgehalten wird oder
    3.  hilflos ist,
3.  Tatsachen die Annahme rechtfertigen, dass sich in ihr eine andere Sache befindet, die sichergestellt werden darf, oder
4.  sie sich in einem Objekt im Sinne des § 3 Absatz 1 Nummer 2 oder in dessen unmittelbarer Nähe befindet und Tatsachen die Annahme rechtfertigen, dass in oder an Objekten dieser Art Straftaten begangen werden sollen, durch die Personen oder diese Objekte gefährdet sind.

(2) Bei der Durchsuchung von Sachen hat die Person, die die tatsächliche Gewalt innehat, das Recht, anwesend zu sein.
Ist sie abwesend, so sollen die sie vertretende Person oder eine andere Person als Zeugin oder Zeuge hinzugezogen werden.
Der Person, die die tatsächliche Gewalt innehat, ist auf Verlangen eine Bescheinigung über die Durchsuchung und ihren Grund zu erteilen.

#### § 7 Sicherstellung

(1) Der Justizwachtmeisterdienst kann eine Sache sicherstellen,

1.  um eine gegenwärtige Gefahr abzuwehren,
2.  um die Eigentümerin, den Eigentümer, die rechtmäßige Inhaberin oder den rechtmäßigen Inhaber der tatsächlichen Gewalt vor Verlust oder Beschädigung einer Sache zu schützen oder
3.  wenn sie von einer Person mitgeführt wird, die nach diesem Gesetz oder anderen Rechtsvorschriften fest- oder angehalten wird, und die Sache verwendet werden kann, um
    1.  sich zu töten oder zu verletzen,
    2.  Leben oder Gesundheit anderer zu schädigen,
    3.  fremde Sachen zu beschädigen oder
    4.  die Flucht zu ermöglichen oder zu erleichtern.

(2) Eine sichergestellte Sache ist unverzüglich der Polizei zu übergeben, wenn nicht die Sicherstellung vor Ablauf des Tages, an dem sie vorgenommen worden ist, aufgehoben werden soll.

#### § 8 Herausgabe sichergestellter Sachen

Sobald die Voraussetzungen für die Sicherstellung weggefallen sind, sind die Sachen an die Person herauszugeben, bei der sie sichergestellt worden sind.
Ist die Herausgabe an sie nicht möglich, können sie an eine andere Person herausgegeben werden, die ihre Berechtigung glaubhaft macht.
Die Herausgabe ist ausgeschlossen, wenn dadurch erneut die Voraussetzungen für eine Sicherstellung eintreten würden.

#### § 9 Gewahrsam

(1) Der Justizwachtmeisterdienst kann eine Person in Gewahrsam nehmen, wenn

1.  das zum Schutz der Person gegen eine Gefahr für Leib oder Leben erforderlich ist, insbesondere weil die Person sich erkennbar in einem die freie Willensbestimmung ausschließenden Zustand oder sonst in hilfloser Lage befindet, oder
2.  das unerlässlich ist, um die unmittelbar bevorstehende Begehung oder Fortsetzung einer Straftat oder einer Ordnungswidrigkeit, die hinsichtlich ihrer Art und Dauer geeignet ist, den Rechtsfrieden nachhaltig zu beeinträchtigen, zu verhindern; die Annahme, dass eine Person eine solche Tat begehen oder zu ihrer Begehung beitragen wird, kann sich insbesondere darauf stützen, dass
    1.  sie die Begehung der Tat angekündigt oder dazu aufgefordert hat oder entsprechende Transparente oder sonstige Gegenstände mit sich führt; dies gilt auch für Flugblätter solchen Inhalts, soweit sie in einer Menge mitgeführt werden, die zur Verteilung geeignet ist,
    2.  bei ihr Waffen, Werkzeuge oder sonstige Gegenstände aufgefunden werden, die ersichtlich zur Tatbegehung bestimmt sind oder erfahrungsgemäß bei derartigen Taten verwendet werden, oder ihre Begleitperson solche Gegenstände mit sich führt und sie den Umständen nach hiervon Kenntnis haben musste oder
    3.  sie bereits in der Vergangenheit aus vergleichbarem Anlass bei der Begehung von Straftaten oder Ordnungswidrigkeiten, die hinsichtlich ihrer Art und Dauer geeignet sind, den Rechtsfrieden nachhaltig zu beeinträchtigen, als Störer betroffen worden ist und nach den Umständen eine Wiederholung dieser Verhaltensweise zu erwarten ist, oder
3.  das unerlässlich ist, um eine Platzverweisung nach § 4 durchzusetzen.

(2) Der Justizwachtmeisterdienst kann eine Person, die aus dem Vollzug von Untersuchungshaft, Freiheitsstrafen oder freiheitsentziehenden Maßregeln der Besserung und Sicherung oder aus der Abschiebungshaft, dem Ausreisegewahrsam oder dem Gewahrsam nach Absatz 1 entwichen ist oder sich sonst ohne Erlaubnis außerhalb der Justizvollzugsanstalt aufhält, in Gewahrsam nehmen.

#### § 10 Behandlung festgehaltener Personen

(1) Wird eine Person aufgrund von § 3 Absatz 2 Satz 3 oder § 9 festgehalten, ist ihr unverzüglich der Grund bekannt zu geben.
Sie ist über die ihr zustehenden Rechtsbehelfe zu belehren.

(2) Der festgehaltenen Person ist unverzüglich Gelegenheit zu geben, einen Rechtsbeistand ihrer Wahl beizuziehen und eine Angehörige, einen Angehörigen oder eine Person ihres Vertrauens zu benachrichtigen, soweit dadurch der Zweck der Freiheitsentziehung nicht gefährdet wird.
Unberührt bleibt die Benachrichtigungspflicht bei einer richterlichen Freiheitsentziehung.
Der Justizwachtmeisterdienst soll die Benachrichtigung übernehmen, wenn die festgehaltene Person nicht in der Lage ist, von dem Recht nach Satz 1 Gebrauch zu machen und die Benachrichtigung ihrem mutmaßlichen Willen nicht widerspricht.
Ist die festgehaltene Person minderjährig oder ist für sie eine Betreuerin oder ein Betreuer bestellt, so ist in jedem Fall unverzüglich die Person zu benachrichtigen, der die Sorge für die festgehaltene Person oder die Betreuung der festgehaltenen Person nach dem ihr übertragenen Aufgabengebiet obliegt.

(3) Die festgehaltene Person soll gesondert, insbesondere ohne ihre Einwilligung nicht in demselben Raum mit Straf- oder Untersuchungsgefangenen untergebracht werden.
Männer und Frauen sollen getrennt untergebracht werden.
Der festgehaltenen Person dürfen nur solche Beschränkungen auferlegt werden, die der Zweck der Freiheitsentziehung oder die Ordnung im Gewahrsam erfordert.

(4) Sind medizinische Behandlungen erkennbar erforderlich oder benötigt die festgehaltene Person Medikamente, sind unverzüglich Maßnahmen einzuleiten, die auch die ärztliche Begutachtung der Gewahrsamsfähigkeit umfassen.

#### § 11 Dauer der Freiheitsentziehung

Die festgehaltene Person ist zu entlassen, sobald der Grund für die Maßnahme des Justizwachtmeisterdienstes weggefallen ist.
Sie ist unverzüglich der Polizei zu übergeben, wenn die Aufhebung der Freiheitsentziehung nicht unmittelbar bevorsteht.
Die Freiheitsentziehung durch den Justizwachtmeisterdienst darf die Dauer von insgesamt vier Stunden nicht überschreiten.

#### § 12 Verantwortlichkeit für das Verhalten von Personen

(1) Verursacht eine Person eine Gefahr, sind die Maßnahmen gegen sie zu richten.

(2) Ist die Person noch nicht vierzehn Jahre alt, können die Maßnahmen auch gegen die Person gerichtet werden, die zur Aufsicht über sie verpflichtet ist.
Ist für die Person eine Betreuerin oder ein Betreuer bestellt, so können die Maßnahmen auch gegen die Betreuerin oder den Betreuer im Rahmen ihres oder seines Aufgabenkreises gerichtet werden.
Dies gilt auch, wenn der Aufgabenkreis der Betreuerin oder des Betreuers die in § 1896 Absatz 4 und § 1905 des Bürgerlichen Gesetzbuches bezeichneten Angelegenheiten nicht erfasst.

(3) Verursacht eine Person, die zu einer Verrichtung bestellt ist, die Gefahr in Ausführung der Verrichtung, so können Maßnahmen auch gegen die Person gerichtet werden, die die andere zu der Verrichtung bestellt hat.

#### § 13 Verantwortlichkeit für den Zustand von Sachen und Tieren

(1) Geht von einer Sache oder von einem Tier eine Gefahr aus, so sind die Maßnahmen gegen die Inhaberin oder den Inhaber der tatsächlichen Gewalt zu richten.
Soweit nichts anderes bestimmt ist, sind die nachfolgenden für Sachen geltenden Vorschriften entsprechend auf Tiere anzuwenden.

(2) Maßnahmen können auch gegen die Eigentümerin, den Eigentümer oder andere Berechtigte gerichtet werden.
Das gilt nicht, wenn die Inhaberin oder der Inhaber der tatsächlichen Gewalt diese ohne den Willen der Eigentümerin, des Eigentümers oder der Berechtigten ausübt.

(3) Geht die Gefahr von einer herrenlosen Sache aus, so können die Maßnahmen gegen die Person gerichtet werden, die das Eigentum an der Sache aufgegeben hat.

#### § 14 Inanspruchnahme von Notstandspflichtigen

(1) Der Justizwachtmeisterdienst kann Maßnahmen gegen andere Personen als die nach § 12 oder § 13 Verantwortlichen richten, wenn

1.  eine gegenwärtige erhebliche Gefahr abzuwehren ist,
2.  Maßnahmen gegen die nach § 12 oder § 13 Verantwortlichen nicht oder nicht rechtzeitig möglich sind oder keinen Erfolg versprechen,
3.  der Justizwachtmeisterdienst die Gefahr nicht oder nicht rechtzeitig selbst oder durch Beauftragte abwehren kann und
4.  die Personen ohne erhebliche eigene Gefährdung und ohne Verletzung höherwertiger Pflichten in Anspruch genommen werden können.

(2) Die Maßnahmen nach Absatz 1 dürfen nur aufrechterhalten werden, solange die Abwehr dieser Gefahr nicht auf andere Weise möglich ist.

#### § 15 Entschädigungsansprüche

Die §§ 38 bis 42 des Ordnungsbehördengesetzes finden entsprechende Anwendung.

#### § 16 Wegfall der aufschiebenden Wirkung von Widerspruch und Anfechtungsklage; Widerspruchsbescheid

(1) Widerspruch und Anfechtungsklage gegen Anordnungen und Maßnahmen des Justizwachtmeisterdienstes haben keine aufschiebende Wirkung.

(2) Über den Widerspruch gegen Verwaltungsakte des Justizwachtmeisterdienstes entscheidet die Behörde, deren Justizwachtmeisterdienst den Verwaltungsakt erlassen hat.

#### § 17 Zulässigkeit des Verwaltungszwangs

(1) Der Verwaltungsakt, der auf die Vornahme einer Handlung oder auf Duldung oder Unterlassung gerichtet ist, kann mit Zwangsmitteln durchgesetzt werden, wenn er unanfechtbar ist oder wenn ein Rechtsmittel keine aufschiebende Wirkung hat.

(2) Der Verwaltungszwang kann ohne vorausgehenden Verwaltungsakt angewendet werden, wenn das zur Abwehr einer gegenwärtigen Gefahr notwendig ist und der Justizwachtmeisterdienst innerhalb seiner Befugnisse handelt.

#### § 18 Zwangsmittel

Der Justizwachtmeisterdienst wendet die Zwangsmittel Ersatzvornahme (§ 19) und unmittelbarer Zwang (§ 20) an.

#### § 19 Ersatzvornahme

Wird die Verpflichtung, eine Handlung vorzunehmen, deren Vornahme durch einen anderen möglich ist (vertretbare Handlung), nicht erfüllt, so kann der Justizwachtmeisterdienst auf Kosten des oder der Handlungspflichtigen die Handlung selbst ausführen.

#### § 20 Unmittelbarer Zwang

(1) Unmittelbarer Zwang ist die Einwirkung auf Personen oder Sachen durch körperliche Gewalt, ihre Hilfsmittel und durch Waffen.

(2) Hilfsmittel der körperlichen Gewalt sind Fesseln, technische Sperren und Reizstoffe.
Als Waffen sind Schlagstöcke zugelassen.

#### § 21 Androhung

Zwangsmittel sind vor ihrer Anwendung anzudrohen.
Von der Androhung kann abgesehen werden, wenn die Umstände sie nicht zulassen, insbesondere wenn die sofortige Anwendung des Zwangsmittels zur Abwehr einer gegenwärtigen Gefahr notwendig ist.

#### § 22 Verhältnismäßigkeit

(1) Unter mehreren möglichen und geeigneten Maßnahmen sind diejenigen zu wählen, die die einzelne Person und die Allgemeinheit voraussichtlich am wenigsten beeinträchtigen.
Eine Maßnahme unterbleibt, wenn ein durch sie zu erwartender Schaden erkennbar außer Verhältnis zu dem angestrebten Erfolg steht.

(2) Gegen Personen darf unmittelbarer Zwang nur angewandt werden, wenn der Zweck durch unmittelbaren Zwang gegen Sachen nicht erreichbar erscheint.
Das angewandte Mittel muss nach Art und Maß dem Verhalten, dem Alter und dem Zustand der betroffenen Person angemessen sein.
Unmittelbarer Zwang darf nicht mehr angewandt werden, wenn der Zweck erreicht ist oder wenn es sich zeigt, dass er durch die Anwendung von unmittelbarem Zwang nicht erreicht werden kann.

#### § 23 Beleihung

(1) Das für Justiz zuständige Ministerium kann natürliche Personen, juristische Personen oder rechtsfähige Personengesellschaften mit der Wahrnehmung der Zugangskontrolle in den Amtsgebäuden seines Geschäftsbereichs beleihen.

(2) Die Beleihung ist nur zulässig, wenn

1.  die oder der zu Beleihende für die zu übertragende Aufgabe geeignet, sach- und fachkundig und zuverlässig ist; insbesondere müssen die erforderlichen speziellen rechtlichen und technischen Kenntnisse nachgewiesen werden; und
2.  die Erfüllung der zu übertragenden Aufgaben sichergestellt ist.

(3) Die Beleihung kann jederzeit ganz oder teilweise zurückgenommen, widerrufen oder mit Nebenbestimmungen verbunden werden.

(4) Die oder der Beliehene hat im Rahmen der ihr oder ihm durch die Beleihung übertragenen Aufgabe die Befugnisse des Justizwachtmeisterdienstes entsprechend den §§ 2 bis 22.

(5) Die oder der Beliehene untersteht bei ihrer oder seiner Tätigkeit in einer Dienststelle der Aufsicht der Leitung dieser Dienststelle.
Die Dienststellenleitung kann einen Beamten oder eine Beamtin des Justizwachtmeisterdienstes zur Aufsicht ermächtigen.
Im Übrigen untersteht die oder der Beliehene im Rahmen der Beleihung der Aufsicht des für Justiz zuständigen Ministeriums.

(6) Wird das Land von Dritten wegen eines Schadens in Anspruch genommen, den die oder der Beliehene in Ausübung des ihr oder ihm anvertrauten Amtes diesen durch eine Amtspflichtverletzung zugefügt hat, so kann das Land bei Vorsatz und grober Fahrlässigkeit bei der oder dem Beliehenen Rückgriff nehmen.
Vertragliche Ansprüche des Landes aus demselben Schadensereignis gegen Dritte, insbesondere den Arbeitgeber der oder des Beliehenen, bleiben unberührt und sind vorrangig geltend zu machen.

#### § 24 Einschränkung von Grundrechten

Durch dieses Gesetz werden das Recht auf körperliche Unversehrtheit und die Freiheit der Person (Artikel 2 Absatz 2 Satz 1 und 2 des Grundgesetzes, Artikel 8 Absatz 1 und Artikel 9 Absatz 1 der Verfassung des Landes Brandenburg) sowie auf Datenschutz (Artikel 11 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 25 Inkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

Potsdam, den 5.
Juni 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark