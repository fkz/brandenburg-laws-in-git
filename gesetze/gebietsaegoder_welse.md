## Gesetz über die Gebietsänderung der amtsangehörigen Gemeinden des Amtes Oder-Welse, Berkholz-Meyenburg, Mark Landin, Passow und Pinnow (Uckermark) (Gebietsänderungsgesetz für das Amt Oder-Welse - GebietsÄGOder-Welse)

_Hinweis: § 8 Absatz 2 bis 4 und § 9 treten am Tag nach der Verkündung in Kraft.
_Im Übrigen tritt dieses Gesetz am 19.
April 2022 in Kraft.__

Der Landtag hat das folgende Gesetz beschlossen:

### _Teil 1_  
_Allgemeingültige Bestimmungen_

#### 

_Das Amt Oder-Welse (Uckermark), bestehend aus den Gemeinden Berkholz-Meyenburg, Mark Landin, Passow und Pinnow, wird mit Inkrafttreten dieses Gesetzes aufgelöst._

#### 

_(1) Die Gemeinden Berkholz-Meyenburg, Mark Landin und Passow werden in die Stadt Schwedt/Oder eingegliedert._

_(2) Die Gemeinde Pinnow wird im Rahmen der Mitverwaltung der Stadt Schwedt/Oder zugeordnet.
Soweit in diesem Gesetz keine abweichenden Regelungen getroffen werden, finden die Regelungen des Abschnitts 3 des Verbandsgemeinde- und Mitverwaltungsgesetzes Anwendung._

_(3) Die Strukturänderungen nach den Absätzen 1 und 2 gelten als Gemeindestrukturänderungen im Sinne des § 1 Satz 1 des Gemeindestrukturänderungsförderungsgesetzes.
Die Vorschriften der §§ 2 und 3 des Gemeindestrukturänderungsförderungsgesetzes sind mit der Maßgabe, dass die Zuwendungen nach § 2 Absatz 2 Satz 1 und 2 des Gemeindestrukturänderungsförderungsgesetzes nach dem Verhältnis der übergehenden Bevölkerungszahl der jeweiligen Gemeinde zu kürzen sind, entsprechend anzuwenden.
Der Antrag auf Gewährung der Einmalkostenpauschale nach § 2 Absatz 2 Satz 1 des Gemeindestrukturänderungsförderungsgesetzes ist von der Stadt Schwedt/Oder unter Beifügung einer Planung zur Mittelverwendung zu stellen._

#### 

_(1) Die Eigenschaft des Amtes Oder-Welse als früherer Dienstherr der Ruhestandsbeamten geht mit Inkrafttreten dieses Gesetzes auf die Stadt Schwedt/Oder über._

_(2) Die Leistungspflicht der für die Ruhestandsbeamten an den Kommunalen Versorgungsverband Brandenburg zu zahlenden Umlagen geht mit Inkrafttreten dieses Gesetzes vom Amt Oder-Welse auf die Stadt Schwedt/Oder über._

_(3) Die Leistungspflicht für das Ruhegehalt während des Abwahlverhältnisses des Amtsdirektors geht mit Inkrafttreten dieses Gesetzes vom Amt Oder-Welse auf die Stadt Schwedt/Oder über._

_(4) Die Kosten nach den Absätzen 2 und 3 werden der Stadt Schwedt/Oder durch die Gemeinde Pinnow im Verhältnis der Einwohnerzahlen der nach § 2 Absatz 1 eingegliederten Gemeinden zur Einwohnerzahl der Gemeinde Pinnow anteilig erstattet.
Für die Kostenermittlung nach Satz 1 ist die vom Amt für Statistik Berlin-Brandenburg auf den 31. Dezember 2021 festgestellte Einwohnerzahl maßgebend._

#### 

_(1) Die bestehenden Arbeitnehmerverhältnisse des Amtes Oder-Welse gehen auf die Stadt Schwedt/Oder über.
Zusätzlich zu den in § 613a des Bürgerlichen Gesetzbuches normierten Rechten und Pflichten wird die Stadt Schwedt/Oder zu einem Bestandsschutz dieser Arbeitsverhältnisse für die Dauer von fünf Jahren verpflichtet.
Dieser Bestandsschutz lässt das Recht der Stadt Schwedt/Oder auf personen- oder verhaltensbedingte Kündigung unberührt._

_(2) Abweichend von der Personalhoheit der Stadt Schwedt/Oder ist das Einvernehmen der Gemeindevertretung Pinnow bei der Einstellung der Kindertagesstättenleitung der Kindertagesstätte „Kleine Oderwelse“ einzuholen._

#### 

_In den Dienststellen der Stadt Schwedt/Oder sind die Personalräte nach den Bestimmungen des § 32 des Landespersonalvertretungsgesetzes neu zu wählen._

#### 

_(1) Die Stadt Schwedt/Oder ist Gesamtrechtsnachfolgerin der eingegliederten Gemeinden Berkholz-Meyenburg, Mark Landin und Passow._

_(2) Alle eigenen Rechte und Pflichten sowie das Vermögen und die Schulden der Gemeinde Pinnow verbleiben bei dieser und werden nicht infolge der Mitverwaltung auf die Stadt Schwedt/Oder übertragen._

_(3) Alle Rechte und Pflichten sowie das Vermögen und die Schulden des Amtes Oder-Welse gehen entschädigungslos und ergebnisneutral an den jeweiligen Rechtsnachfolger entsprechend der künftigen Aufgabenwahrnehmung über.
Insbesondere gilt Folgendes:_

1.  _Die ursprünglich auf das Amt Oder-Welse übertragene Kindertagesstätte „Kleine Oderwelse“ fällt in ihrer Gesamtheit auf die Gemeinde Pinnow zurück._
2.  _Die ursprünglich auf das Amt Oder-Welse übertragene Kindertagesstätte „Gänseblümchen“ geht in ihrer Gesamtheit auf die Stadt Schwedt/Oder über._
3.  _Die vorhandenen Ortsfeuerwehren bleiben nach Maßgabe der jeweiligen Gefahren- und Risikoanalyse und Gefahrenbedarfsplanung der Stadt Schwedt/Oder und der Gemeinde Pinnow als einsatzfähige Feuerwehreinheiten erhalten.
    Der örtliche Brandschutz und die örtliche Hilfeleistung stehen im Vordergrund.
    Das bewegliche Vermögen des Amtes in diesen Bereichen wird entsprechend den Aufgaben in der Weise aufgeteilt, dass es den Gemeinden zugeordnet wird, in deren Gebiet es bisher verwendet wurde oder stationiert war (insbesondere Fahrzeuge, Maschinen, technische Anlagen).
    Vermögensteile, die nicht zugeordnet werden können, werden wertmäßig wie folgt zugeteilt:_
    1.  _Dienstkleidung wird entsprechend der Anzahl der Kameraden der einzelnen Ortswehren zugeteilt._
    2.  _Atemschutzausrüstung wird entsprechend der Ausstattung der Einsatzfahrzeuge zugeteilt._
4.  _Das in der Bilanz aktivierte Vermögen, das im Rahmen von Fördermaßnahmen in Bezug auf den Hochwasserschutz erworben wurde, geht einschließlich der dazugehörigen Sonderposten entsprechend der Aufgabenwahrnehmung und dem Förderzweck auf die Stadt Schwedt/Oder über._
5.  _Rückstellungen für Pensionen und ähnliche Verpflichtungen gehen vollständig auf die Stadt Schwedt/Oder über.
    Für die Gemeinde Pinnow, die nicht in die Stadt Schwedt/Oder eingegliedert wird, hat die Stadt Schwedt/Oder Anspruch auf anteilige Kostenerstattung entsprechend § 3 Absatz 3._
6.  _Verwaltete Bestallungen und Jagden gehen auf die Stadt Schwedt/Oder über._
7.  _Kredite und Bürgschaften gehen mit dem ihnen zuzuordnenden Vermögen entsprechend der künftigen Aufgabenwahrnehmung auf die jeweiligen Rechtsnachfolger über._
8.  _Vermögen, das nicht eindeutig zugeordnet werden kann, wird wertmäßig nach dem Verhältnis der durchschnittlich gezahlten Amtsumlage der einzelnen Gemeinden in den Haushaltsjahren 2012 bis 2021 aufgeteilt._
9.  _Die Stadt Schwedt/Oder erstellt die Jahresabschlüsse bis zum Haushaltsjahr des Inkrafttretens dieses Gesetzes.
    Die Prüfung erfolgt durch das Rechnungsprüfungsamt der Stadt Schwedt/Oder.
    § 82 der Kommunalverfassung des Landes Brandenburg gilt entsprechend._

_(4) Für die Gemeinden Berkholz-Meyenburg, Mark Landin, Passow und Pinnow sowie für das Amt Oder-Welse findet Abschnitt 3 des Gemeindestrukturänderungsförderungsgesetzes entsprechend Anwendung._

### Teil 2  
Bestimmungen für die Gemeinden Berkholz-Meyenburg, Mark Landin und Passow

#### 

_(1) Die Gemeinde Berkholz-Meyenburg, die Ortsteile Briest, Jamikow, Passow/Wendemark und Schönow der Gemeinde Passow und die Ortsteile Grünow, Landin und Schönermark der Gemeinde Mark Landin werden gemäß § 45 Absatz 1 der Kommunalverfassung des Landes Brandenburg Ortsteile der Stadt Schwedt/Oder._

_(2) Die althergebrachten Namen der Gemeinde Berkholz-Meyenburg und der Ortsteile Briest, Jamikow, Schönow, Grünow, Landin und Schönermark werden beibehalten und gelten als Namen der Ortsteile weiter.
Der Ortsteil Passow/Wendemark erhält den Namen Passow._

_(3) Auf den Ortstafeln ist der Name des Ortsteils über dem Namen der Stadt Schwedt/Oder aufzuführen._

_(4) Die Stadt Schwedt/Oder beantragt bei der Deutschen Post AG deren Zustimmung zu der neuen postalischen Anschrift der Ortsteile._

#### § 8 Ortsvorsteher/Ortsbeirat

_(1) Der ehrenamtliche Bürgermeister der eingegliederten Gemeinde Berkholz-Meyenburg ist Ortsvorsteher des neuen Ortsteils Berkholz-Meyenburg.
Die Ortsvorsteher und Ortsvorsteherinnen der bisherigen Ortsteile der Gemeinde Passow und Mark Landin sind Ortsvorsteher und Ortsvorsteherinnen der Ortsteile Briest, Jamikow, Passow, Schönow, Grünow, Landin und Schönermark._

(2) In den Ortsteilen Berkholz-Meyenburg, Briest, Jamikow, Passow, Schönow, Grünow, Landin und Schönermark werden zum Zeitpunkt der Eingliederung Ortsbeiräte gebildet.
Die Anzahl der Mitglieder einschließlich Ortsvorsteher oder Ortsvorsteherin bestimmt sich durch die Hauptsatzung der Stadt Schwedt/Oder.

(3) Die Mitglieder und gegebenenfalls Ersatzmitglieder des Ortsbeirates für den Ortsteil Berkholz-Meyenburg werden vor Eintritt der Rechtswirksamkeit der Eingliederung von der Gemeindevertretung der Gemeinde Berkholz-Meyenburg gemäß § 41 der Kommunalverfassung des Landes Brandenburg aus dem Kreise der Mitglieder der Gemeindevertretung gewählt.

(4) Die Mitglieder und gegebenenfalls Ersatzmitglieder der Ortsbeiräte für die Ortsteile Briest, Jamikow, Passow, Schönow, Grünow, Landin und Schönermark werden vor Eintritt der Rechtswirksamkeit der Eingliederung von den jeweiligen Gemeindevertretungen der einzugliedernden Gemeinden aus dem Kreise der Gemeindevertreterinnen und Gemeindevertreter, die Bürgerinnen und Bürger des jeweiligen Ortsteiles sind, gewählt.
Abweichend von den Regelungen in der Hauptsatzung der Stadt Schwedt/Oder bestehen bis zum Ende der laufenden Kommunalwahlperiode die Ortsbeiräte in den Ortsteilen Briest, Jamikow, Passow, Schönow, Grünow, Landin und Schönermark aus drei Mitgliedern.
Soweit in einem Ortsteil in Anwendung des § 41 der Kommunalverfassung des Landes Brandenburg keine drei Mitglieder gewählt werden können, besteht für den Ortsteil bis zum Ende der laufenden Kommunalwahlperiode lediglich ein Ortsvorsteher oder eine Ortsvorsteherin.
Hat die Gemeindevertretung nur eine einzelne Person zu wählen, erfolgt die Wahl gemäß § 40 der Kommunalverfassung des Landes Brandenburg.
Hat die Gemeindevertretung mehrere Personen zu wählen, erfolgt die Wahl gemäß § 41 der Kommunalverfassung des Landes Brandenburg.

_(5) Die Anhörungs- und Entscheidungsrechte der Ortsteilvertretung bestimmen sich nach § 46 der Kommunalverfassung des Landes Brandenburg und der Hauptsatzung der Stadt Schwedt/Oder._

_(6) Für die Entschädigung der Ortsbeiräte und Ortsvorsteher oder Ortsvorsteherinnen gelten die Regelungen der Entschädigungssatzung der Stadt Schwedt/Oder._

#### § 9 Stadtverordnetenversammlung

(1) Die derzeitigen Mitglieder der Gemeindevertretung der Gemeinden Berkholz-Meyenburg und Mark Landin wählen für den Zeitraum bis zur nächsten Kommunalwahl in Anwendung des § 41 der Kommunalverfassung des Landes Brandenburg jeweils aus ihrer Mitte drei Vertreter oder Vertreterinnen, die Gemeinde Passow vier Vertreter oder Vertreterinnen, die der Stadtverordnetenversammlung der aufnehmenden Stadt Schwedt/Oder bis zum Ende der laufenden Kommunalwahlperiode mit Stimmrecht angehören.

(2) Die bisherigen Gemeindevertreter und Gemeindevertreterinnen, die keinen Sitz in der Stadtverordnetenversammlung der aufnehmenden Stadt Schwedt/Oder erhalten, sind in Anwendung des § 41 der Kommunalverfassung des Landes Brandenburg als Ersatzmitglieder zu bestimmen.

#### 

_Für den Fall, dass die Wahlen nach § 8 Absatz 3 und 4 sowie § 9 nicht vor Eintritt der Rechtswirksamkeit der Eingliederung durchgeführt werden können, gilt die Gemeindevertretung für diese Wahlhandlung als fortbestehend._

#### 

_(1) Die Stadt Schwedt/Oder ist verpflichtet, den dörflichen Charakter und das örtliche Brauchtum der zukünftigen Ortsteile Berkholz-Meyenburg, Grünow, Landin, Schönermark, Briest, Jamikow, Passow und Schönow zu erhalten.
Das kulturelle und sportliche Eigenleben, insbesondere die bestehenden Vereine und kirchlichen Einrichtungen, sind ebenso zu fördern, wie in den anderen Ortsteilen der Stadt Schwedt/Oder.
Die damit im Zusammenhang stehende Entscheidungsbefugnis über die Nutzung vorhandener Räumlichkeiten in den Gemeindehäusern erfolgt entsprechend den Regelungen, die in den bereits vorhandenen Ortsteilen der Stadt Schwedt/Oder gelten._

_(2) Bestand und Betrieb der in den ehemaligen Gemeinden Berkholz-Meyenburg, Mark Landin und Passow vorhandenen kommunalen Einrichtungen sind nach Maßgabe des Haushaltes zu gewährleisten, soweit sie einer sinnvollen Gesamtplanung entsprechen._

#### 

_(1) Das Ortsrecht der eingegliederten Gemeinden Berkholz-Meyenburg, Mark Landin und Passow tritt mit Wirksamwerden der Eingliederung außer Kraft, soweit gesetzlich nichts anderes bestimmt ist.
Zum gleichen Zeitpunkt tritt das Ortsrecht der aufnehmenden Stadt Schwedt/Oder in den zukünftigen Ortsteilen Berkholz-Meyenburg, Briest, Jamikow, Passow, Schönow, Grünow, Landin und Schönermark in Kraft._

_(2) Der Hebesatz der Realsteuern im Gebiet der eingegliederten Gemeinden Berkholz-Meyenburg, Mark Landin und Passow bleibt für die Dauer von fünf Jahren unverändert auf der Höhe der Hebesätze des Haushaltsjahres 2021, sofern der Hebesatz der eingegliederten Gemeinden Berkholz-Meyenburg, Mark Landin und Passow für diesen Zeitraum unter dem Hebesatz der aufnehmenden Gemeinde Stadt Schwedt/Oder liegt oder die gleiche Höhe aufweist.
Eine Änderung der Hebesätze für die Grundsteuer im Rahmen der Grundsteuerreform ist hiervon ausgenommen._

_(3) Die kommunalen Friedhöfe der Gemeinden Berkholz-Meyenburg, Mark Landin und Passow werden als öffentliche Einrichtungen der Stadt Schwedt/Oder weiter betrieben.
Die Friedhofssatzungen (in der am Tag vor der Eingliederung der Gemeinden Berkholz-Meyenburg, Mark Landin und Passow gültigen Fassung) und die Gebührensatzungen für die Benutzung der Friedhöfe der Gemeinden Berkholz-Meyenburg, Mark Landin und Passow (in der am Tag vor der Eingliederung der Gemeinden Berkholz-Meyenburg, Mark Landin und Passow gültigen Fassung) gelten so lange fort, bis sie durch neues gemeinsames Ortsrecht ersetzt werden oder aus anderen Gründen außer Kraft treten, jedoch nicht länger als fünf Jahre.
Soweit die Fortgeltung Benutzungsgebühren betrifft, gilt § 6 Absatz 3 Satz 1 des Kommunalabgabengesetzes._

_(4) Die Satzungen der Gemeinden Berkholz-Meyenburg, Mark Landin und Passow über die Erhebung einer Hundesteuer (Hundesteuersatzung) gelten für die Dauer von fünf Jahren fort._

_(5) Abweichend von Absatz 1 gelten der Flächennutzungsplan des Amtes Oder-Welse, Blatt 3 (in der Fassung der Ausfertigung vom 26.
Februar 2015) und die Bebauungspläne_

1.  _Bebauungsplan Nummer 8 Herrmannsberg - Amt Oder-Welse Gemeinde Berkholz-Meyenburg OT Berkholz,_
2.  _Bebauungsplan 06 Meyenburger Hang 3.
    Änderung - Amt Oder-Welse Gemeinde Berkholz-Meyenburg OT Meyenburg,_
3.  _Bebauungsplan 04 Waschpfuhl 1.
    Änderung - Amt Oder-Welse Gemeinde Berkholz-Meyenburg OT Meyenburg,_
4.  _Bebauungsplan 03 Gewerbegebiet Berkholz-Meyenburg 1.
    Änderung - Amt Oder-Welse Gemeinde Berkholz-Meyenburg OT Meyenburg,_
5.  _Bebauungsplan 02 Berkholz-Nordwest - Amt Oder-Welse Gemeinde Berkholz-Meyenburg OT Berkholz,_
6.  _Bebauungsplan 01 Planungsgebiet östlich des Dorfes - Amt Oder-Welse Gemeinde Berkholz-Meyenburg OT Berkholz,_
7.  _Bebauungsplan 01 Kastanienallee - 5.
    Änderung - Amt Oder-Welse Berkholz-Meyenburg OT Berkholz,_
8.  _Ergänzungssatzung Verlängerte Kastanienallee in Meyenburg 1.
    Änderung - Amt Oder-Welse Gemeinde Berkholz-Meyenburg OT Meyenburg_

_so lange fort, bis sie durch neues gemeinsames Ortsrecht ersetzt werden oder aus anderen Gründen außer Kraft treten._

_(6) Abweichend von Absatz 1 gelten der Flächennutzungsplan des Amtes Oder-Welse, Blatt 2 (in der Fassung der Ausfertigung vom 26.
Februar 2015), die Abrundungssatzung Hohenlandin (in der Fassung der Ausfertigung vom 24. Juni 1996), die Abrundungssatzung Niederlandin (in der Fassung der Ausfertigung vom 24.
Juni 1996) und die Gebührensatzung der Gemeinde Mark Landin über die Erhebung von Gebühren für die Inanspruchnahme von Kinderbetreuungsleistungen in der kommunalen Kindertagesstätte, (in der am Tag vor der Eingliederung der Gemeinde Mark Landin gültigen Fassung) so lange fort, bis sie durch neues gemeinsames Ortsrecht ersetzt werden oder aus anderen Gründen außer Kraft treten._

_(7) Die Gebiete der Ortsteile Grünow und Schönermark der eingegliederten Gemeinde Mark Landin verbleiben im Schulbezirk der Grundschule Passow und das Gebiet des Ortsteils Landin der eingegliederten Gemeinde Mark Landin verbleibt im Schulbezirk der Grundschule Pinnow._

_(8) Abweichend von Absatz 1 gelten der Flächennutzungsplan des Amtes Oder- Welse, Blatt 1 (in der Fassung der Ausfertigung vom 26.
Februar 2015), die Klarstellungs- und Abrundungssatzung OT Jamikow (in der Fassung der Ausfertigung vom 01.
Juni 1999), die Klarstellungs- und Abrundungssatzung OT Schönow (in der Fassung der Ausfertigung vom 01.
März 1994) und die Gebührensatzung des Amtes Oder-Welse über die Erhebung von Gebühren für die Inanspruchnahme von Kinderbetreuungsleistungen in den Kindertagesstätten in Trägerschaft des Amtes Oder-Welse für die Kindertagesstätte „Gänseblümchen“, (in der am Tag vor der Eingliederung der Gemeinde Passow gültigen Fassung) so lange fort, bis sie durch neues gemeinsames Ortsrecht ersetzt werden oder aus anderen Gründen außer Kraft treten._

_(9) Das Gebiet der eingegliederten Gemeinde Passow verbleibt im Schulbezirk der Grundschule Passow.
Die Satzung über den Schulbezirk der Cornelia-Funke-Grundschule (in der am Tag vor der Eingliederung der Gemeinde Passow gültigen Fassung) gilt so lange fort, bis sie durch neues gemeinsames Ortsrecht ersetzt wird oder aus anderen Gründen außer Kraft tritt._

_(10) Die Fünfjahresfrist beginnt am 1.
Januar 2022._

#### 

_Die Stadt Schwedt/Oder tritt bezüglich der Mitgliedschaften, Beteiligungen, Verträge und Rechtsstreitigkeiten an die Stelle der Gemeinden Berkholz-Meyenburg, Mark Landin und Passow.
Die Stadt Schwedt/Oder tritt vorbehaltlich der §§ 6 und 14 auch an die Stelle des bisherigen Amtes Oder-Welse bezüglich dessen bisherigen Rechten und Pflichten, Mitgliedschaften, Beteiligungen, Verträgen und Rechtsstreitigkeiten._

#### 

_Das Haushalts-, Kassen- und Rechnungswesen der eingegliederten Gemeinden Berkholz-Meyenburg, Mark Landin und Passow geht mit Wirksamwerden der Eingliederung in das Haushalts-, Kassen- und Rechnungswesen der aufnehmenden Stadt Schwedt/Oder über._

### _Teil 3_  
_Bestimmungen für die Gemeinde Pinnow_

#### 

_(1) Die Stadt Schwedt/Oder wird Trägerin der Auftragsangelegenheiten.
Dies betrifft insbesondere den Bereich des Personenstandswesens.
Hier wird die Stadt Schwedt/Oder Rechtsnachfolgerin und tritt in die Verträge der Gemeinde Pinnow und des Amtes Oder-Welse ein._

_(2) Bei Pflichtaufgaben zur Erfüllung nach Weisung ohne Selbstverwaltungscharakter trifft die Stadt Schwedt/Oder durch ihre Hauptverwaltungsbeamtin oder ihren Hauptverwaltungsbeamten die Entscheidungen für die Gemeinde Pinnow in deren Namen als hauptamtliche Verwaltung und führt diese durch.
Eine Beschlussfassung der Gemeindevertretung Pinnow erfolgt nicht._

_(3) Bei Selbstverwaltungsaufgaben und Pflichtaufgaben zur Erfüllung nach Weisung mit Selbstverwaltungscharakter bereitet die Stadt Schwedt/Oder durch ihre Hauptverwaltungsbeamtin oder ihren Hauptverwaltungsbeamten im Benehmen mit dem ehrenamtlichen Bürgermeister oder der ehrenamtlichen Bürgermeisterin die Beschlüsse der Gemeindevertretung Pinnow vor und führt sie nach deren Beschlussfassung aus.
Nicht oder nur fehlerhaft durchgeführte Beschlüsse werden der Gemeinde Pinnow zugerechnet._

_(4) Vorbehaltlich des § 6 tritt die Gemeinde Pinnow hinsichtlich der Pflichtaufgaben zur Erfüllung nach Weisung an die Stelle des Amtes Oder-Welse bezüglich der Mitgliedschaften, Beteiligungen, Verträge und Rechtsstreitigkeiten des Amtes Oder-Welse, sofern das Gemeindegebiet der Gemeinde Pinnow betroffen ist._

_(5) Investive Maßnahmen sind entsprechend der Hauptsatzung der Gemeinde Pinnow in der jeweils geltenden Fassung, bei Überschreitung der dort genannten Wertgrenze von zurzeit 1 000 Euro, der Gemeindevertretung zur Beschlussfassung vorzulegen._

_(6) In gerichtlichen Verfahren und in Rechts- und Verwaltungsgeschäften vertritt die Stadt Schwedt/Oder die Gemeinde Pinnow.
Sind beide Gemeinden an einem gerichtlichen Verfahren oder an Rechts- und Verwaltungsgeschäften beteiligt, ist außer in den Fällen des § 97 Absatz 1 der Kommunalverfassung des Landes Brandenburg die ehrenamtliche Bürgermeisterin oder der ehrenamtliche Bürgermeister gesetzliche Vertreterin oder gesetzlicher Vertreter der Gemeinde Pinnow, soweit nicht die Gemeindevertretung der Gemeinde Pinnow für einzelne Rechtsgeschäfte oder einen bestimmten Kreis von Rechtsgeschäften eine Befreiung der Stadt Schwedt/Oder vom Verbot des Insichgeschäfts beschließt._

_(7) Im Fall einer genehmigten Gemeindestrukturänderung der Gemeinde Pinnow gilt die Mitverwaltung der Gemeinde Pinnow durch die Stadt Schwedt/Oder als aufgelöst.
Gemeindestrukturänderung im Sinne des Satzes 1 ist eine Gebietsänderung gemäß § 6 Absatz 3 der Kommunalverfassung des Landes Brandenburgs, die Änderung eines Amtes gemäß § 134 der Kommunalverfassung des Landes Brandenburg, die Bildung einer Verbandsgemeinde nach § 3 des Verbandsgemeinde- und Mitverwaltungsgesetzes oder die Bildung einer Mitverwaltung nach § 17 des Verbandsgemeinde- und Mitverwaltungsgesetzes._

_(8) Für die Gemeinde Pinnow gilt das Ortsrecht des Amtes Oder-Welse so lange fort, bis es durch neues eigenes Ortsrecht ersetzt wird oder aus anderen Gründen außer Kraft tritt, jedoch nicht länger als fünf Jahre.
Soweit die Fortgeltung Benutzungsgebühren betrifft, gilt § 6 Absatz 3 Satz 1 des Kommunalabgabengesetzes.
Die Fünfjahresfrist beginnt am 1.
Januar 2022._

#### 

_Für die Mitverwaltung ist ein gemeinsames Organ der beteiligten Gemeinden zu bilden (Mitverwaltungsausschuss).
Auf den Mitverwaltungsausschuss sind die §§ 21 bis 23 des Verbandsgemeinde- und Mitverwaltungsgesetztes entsprechend anwendbar.
Keine Gemeinde darf mehr als 50 Prozent aller Stimmen besitzen.
Es erfolgt ein Ausgleich für die nach § 21 Absatz 3 des Verbandsgemeinde- und Mitverwaltungsgesetzes mit weniger Mitgliedern ausgestattete Gemeinde, bis die Hälfte der Gesamtzahl der Mitglieder erreicht ist._

#### 

_(1) Die Stadt Schwedt/Oder und die Gemeinde Pinnow verfügen als jeweils fortbestehende eigenständige juristische Personen des öffentlichen Rechts über ein eigenes Haushalts-, Kassen- und Rechnungswesen gemäß den Regelungen des Teils 1 Kapitel 3 Abschnitt 1, 2 und 4 der Kommunalverfassung des Landes Brandenburg._

_(2) Die Stadt Schwedt/Oder besorgt das Haushalts-, Kassen- und Rechnungswesen für die Gemeinde Pinnow._

_(3) Für die hauptamtliche Verwaltung der Gemeinde Pinnow sind der Stadt Schwedt/Oder gemäß § 24 Absatz 5 des Verbandsgemeinde- und Mitverwaltungsgesetzes die Kosten zu erstatten.
Auf die Kostenerstattung sind Abschläge je zu einem Zwölftel zum jeweils 15.
eines Monats zu zahlen.
Die geleisteten Abschlagszahlungen werden mit der endgültigen Festsetzung verrechnet._

#### 

_(1) Die Stadt Schwedt/Oder und die Gemeinde Pinnow sowie ihre Organe arbeiten unter Beachtung der jeweiligen Verantwortungsbereiche vertrauensvoll zusammen._

_(2) Sie unterrichten sich gemäß § 19 Absatz 5 des Verbandsgemeinde- und Mitverwaltungsgesetzes über die Beschlüsse der Gemeindevertretung Pinnow und der Stadtverordnetenversammlung der Stadt Schwedt/Oder sowie die Entscheidungen des Bürgermeisters oder der Bürgermeisterin, die von grundsätzlicher Bedeutung sind._

_(3) Soweit Zweifel bestehen, ob ein Beschluss oder eine Entscheidung grundsätzliche Bedeutung besitzt, erfolgt eine Unterrichtung im Interesse der vertrauensvollen Zusammenarbeit._

### Teil 4  
Inkrafttreten

#### § 18 Inkrafttreten

(1) § 8 Absatz 2 bis 4 und § 9 treten am Tag nach der Verkündung in Kraft.

(2) Im Übrigen tritt dieses Gesetz am 19.
April 2022 in Kraft.

Potsdam, den 24.
März 2022

  
Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke