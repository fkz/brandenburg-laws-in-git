## Gesetz zu dem Staatsvertrag der Länder Berlin und Brandenburg auf dem Gebiet der Landwirtschaft (Landwirtschaftsstaatsvertrag)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem in Berlin und Potsdam am 10. Juli 2020 und am 3. August 2020 unterzeichneten Staatsvertrag der Länder Berlin und Brandenburg auf dem Gebiet der Landwirtschaft (Landwirtschaftsstaatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

#### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem Artikel 19 am 16.
Oktober 2020 in Kraft.

Potsdam, den 14.
September 2020

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke

* * *

[zum Staatsvertrag](/vertraege/lwstv)

* * *

### Anlagen

1

[Staatsvertrag der Länder Berlin und Brandenburg auf dem Gebiet der Landwirtschaft (Landwirtschaftsstaatsvertrag - LwStV)](/br2/sixcms/media.php/68/GVBl_I_25_2020-Anlage.pdf "Staatsvertrag der Länder Berlin und Brandenburg auf dem Gebiet der Landwirtschaft (Landwirtschaftsstaatsvertrag - LwStV)") 279.7 KB