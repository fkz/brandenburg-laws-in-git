## Brandenburgisches Gesetz zur Sicherstellung ordnungsgemäßer Planungsverfahren für Straßen während der COVID-19-Pandemie (Brandenburgisches Straßenplanungssicherstellungsgesetz - BbgStrPlanSiG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Anwendungsbereich

Dieses Gesetz gilt für Planfeststellungsverfahren über den Bau oder die Änderung einer Straße nach dem Brandenburgischen Straßengesetz in der Fassung der Bekanntmachung vom 28. Juli 2009 (GVBl. I S. 358), das zuletzt durch Artikel 2 des Gesetzes vom 18. Dezember 2018 (GVBl. I Nr. 37 S. 3) geändert worden ist, in der jeweils geltenden Fassung.

#### § 2 Ortsübliche und öffentliche Bekanntmachungen

(1) Ist für die ortsübliche oder öffentliche Bekanntmachung nach den dafür geltenden Vorschriften der Anschlag an einer Amtstafel oder die Auslegung zur Einsichtnahme vorgesehen, so können der Anschlag oder die Auslegung durch eine Veröffentlichung des Inhalts der Bekanntmachung im Internet ersetzt werden, wenn die jeweilige Bekanntmachungsfrist spätestens mit Ablauf des 31. Dezember 2022 endet.
Zusätzlich hat zumindest eine Bekanntmachung in einem amtlichen Veröffentlichungsblatt oder einer örtlichen Tageszeitung zu erfolgen.

(2) Für die Veröffentlichung im Internet gilt § 27a Absatz 1 Satz 2 und Absatz 2 des Verwaltungsverfahrensgesetzes entsprechend.

#### § 3 Auslegung von Unterlagen oder Beschlüssen

(1) Die Auslegung von Unterlagen oder Beschlüssen kann durch eine Veröffentlichung im Internet ersetzt werden, wenn die jeweilige Auslegungsfrist spätestens mit Ablauf des 31. Dezember 2022 endet.
Für die Veröffentlichung im Internet gilt § 27a Absatz 1 Satz 2 des Verwaltungsverfahrensgesetzes entsprechend.
In der Bekanntmachung der Auslegung ist darauf hinzuweisen, dass und wo die Veröffentlichung im Internet erfolgt.
Soweit das Verfahrensrecht den Zugang über ein zentrales Internetportal vorsieht, bleibt dieses unberührt.
Der Vorhabenträger hat Anspruch darauf, dass seine Betriebs- und Geschäftsgeheimnisse von der Behörde nicht unbefugt offenbart werden.
Er kann der Veröffentlichung im Internet widersprechen, wenn er die Gefährdung von Betriebs- oder Geschäftsgeheimnissen oder wichtiger Sicherheitsbelange befürchtet.
Widerspricht der Vorhabenträger der Veröffentlichung im Internet, hat die Behörde das Verfahren bis zu einer Auslegung auszusetzen.

(2) Die angeordnete Auslegung soll daneben als zusätzliches Informationsangebot erfolgen, soweit dies nach Feststellung der zuständigen Behörde den Umständen nach möglich ist.
Unterbleibt eine Auslegung, hat die zuständige Behörde zusätzlich zur Veröffentlichung nach Absatz 1 Satz 1 andere leicht zu erreichende Zugangsmöglichkeiten, etwa durch öffentlich zugängliche Lesegeräte oder in begründeten Fällen durch Versendung zur Verfügung zu stellen.
Auf diese Zugangsmöglichkeiten ist in der Bekanntmachung nach § 2 Absatz 1 hinzuweisen.

(3) Die Behörde kann von einem Vorhabenträger verlangen, dass er die Unterlagen, die er bei der Behörde zum Zwecke der Bekanntmachung durch die Behörde einzureichen hat, in einem verkehrsüblichen elektronischen Format einreicht.

#### § 4 Erklärungen zur Niederschrift

(1) Die Abgabe von Erklärungen zur Niederschrift bei der Behörde kann ausgeschlossen werden, wenn die jeweilige Erklärungsfrist spätestens mit Ablauf des 31.
Dezember 2022 endet und die zuständige Behörde festgestellt hat, dass innerhalb der Erklärungsfrist eine Entgegennahme zur Niederschrift nicht oder nur mit unverhältnismäßigem Aufwand möglich sein würde.

(2) In Fällen des Absatzes 1 hat die zuständige Behörde einen Zugang für die Abgabe von elektronischen Erklärungen bereitzuhalten.
In den Bekanntmachungen, in denen sonst auf die Möglichkeit der Abgabe von Erklärungen zur Niederschrift hingewiesen wird, ist auf die Möglichkeit der Abgabe elektronischer Erklärungen und den Ausschluss der Abgabe von Erklärungen zur Niederschrift hinzuweisen.

#### § 5 Erörterungstermine

(1) Ist die Durchführung eines Erörterungstermins in das Ermessen der Behörde gestellt, können bei der Ermessensentscheidung auch geltende Beschränkungen aufgrund der COVID-19-Pandemie und das Risiko der weiteren Ausbreitung des Virus berücksichtigt werden.

(2) Ist die Durchführung eines Erörterungstermins angeordnet, auf den nach den dafür geltenden Vorschriften nicht verzichtet werden kann, genügt eine Online-Konsultation nach Absatz 4.

(3) Die zur Teilnahme an einem Erörterungstermin Berechtigten sind von der Durchführung der ersatzweisen Online-Konsultation zu benachrichtigen.
§ 73 Absatz 6 Satz 2 bis 4 des Verwaltungsverfahrensgesetzes gilt entsprechend.

(4) Für die Online-Konsultation werden den zur Teilnahme Berechtigten die sonst im Erörterungstermin zu behandelnden Informationen zugänglich gemacht.
Ihnen ist innerhalb einer angemessenen Frist Gelegenheit zu geben, sich schriftlich oder elektronisch dazu zu äußern.
Auf die Frist ist in der Bekanntmachung nach Absatz 3 hinzuweisen.
Die zuständige Behörde hat geeignete Vorkehrungen dafür zu treffen, dass nur die nach den Sätzen 1 und 2 Berechtigten Zugang zu der Online-Konsultation haben.
Die Regelungen über die Online-Konsultation lassen den bereits eingetretenen Ausschluss von Einwendungen unberührt.
§ 3 Absatz 1 Satz 5 bis 7 gilt entsprechend.

(5) Die Online-Konsultation nach Absatz 4 kann mit Einverständnis der zur Teilnahme Berechtigten durch eine Telefon- oder Videokonferenz ersetzt werden.
Absatz 4 gilt mit Ausnahme der Sätze 2 und 3 in diesem Fall entsprechend.
Über die Telefon- oder Videokonferenz ist ein Protokoll zu führen.

(6) § 3 Absatz 3 gilt entsprechend.

#### § 6 Übergangsregelung

(1) Die Regelungen dieses Gesetzes sind auch auf bereits vor Inkrafttreten dieses Gesetzes begonnene, aber noch nicht abgeschlossene Verfahren anwendbar.
Ein Verfahrensschritt, der bereits begonnen wurde, ist jedoch zu wiederholen, wenn er nach diesem Gesetz durchgeführt werden soll.
Abweichend von Satz 2 ist ein Verfahrensschritt, der bereits vor dem 16.
März 2020 begonnen wurde, nicht zu wiederholen, wenn der Beteiligungsschritt in diesem Verfahrensschritt, der teilweise oder ganz entfallen oder erschwert worden ist, nach diesem Gesetz hätte entfallen können und lediglich der Hinweis auf das Unterbleiben einer einzelnen Beteiligungsmöglichkeit vorab nicht erteilt werden konnte.

(2) Für Verfahrensschritte, bei denen von einer Regelung nach den §§ 2 bis 5 Gebrauch gemacht worden ist und die mit Ablauf des 31.
Dezember 2022 noch nicht abgeschlossen sind, gelten die Bestimmungen dieses Gesetzes bis zum Abschluss des jeweiligen Verfahrensschrittes weiter.

(3) Die für das Planfeststellungsverfahren geltenden Fehlerfolgenregelungen sind entsprechend anzuwenden und bleiben im Übrigen unberührt.
Fehler bei Bekanntmachungen haben keine Auswirkung auf die Rechtmäßigkeit der Verfahren, wenn der Hinweiszweck der Bekanntmachung erfüllt ist.

#### § 7 Inkrafttreten, Außerkrafttreten

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Die §§ 1 bis 5 dieses Gesetzes treten mit Ablauf des 31.
Dezember 2022 außer Kraft.
Im Übrigen tritt das Gesetz mit Ablauf des 30.
September 2027 außer Kraft.

Potsdam, den 4.
März 2021

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke