## Gesetz über die Enquete-Kommissionen des Landtages Brandenburg (Enquete-Gesetz - EnKoG)

#### § 1 Bildung und Auftrag

(1) Enquete-Kommissionen des Landtages haben die Aufgabe, umfangreiche und bedeutsame Fragestellungen zur Vorbereitung wesentlicher Entscheidungen des Landtages durch Sammlung und Auswertung von Material sowie durch Anhörung von Sachverständigen und anderen Personen zu bearbeiten.

(2) Die Einsetzung einer Enquete-Kommission muss durch den Landtag erfolgen, wenn mindestens ein Drittel der Mitglieder des Landtages dies beantragt.

(3) Der Antrag muss den Auftrag der Enquete-Kommission genau bestimmen und eine Begründung enthalten.
Der Auftrag kann durch Beschluss des Landtages jederzeit auch gegen den Willen der Antragstellerinnen und Antragsteller erweitert werden.

(4) Der Landtag beschließt über die angemessene sächliche und personelle Ausstattung der Enquete-Kommission.
In dem Beschluss kann bestimmt werden, dass die Fraktionen zusätzliche zweckgebundene finanzielle Mittel erhalten.
Je Fraktion und Haushaltsjahr sollen die Mittel den zur Finanzierung einer Vollzeitstelle der Entgeltgruppe 14 des Tarifvertrages für den öffentlichen Dienst der Länder erforderlichen Betrag nicht übersteigen.
Für die Gruppen gelten die Sätze 2 und 3 entsprechend, wenn sie ein parlamentarisches Mitglied nach § 2 Absatz 2 oder ein beratendes Mitglied nach § 2 Absatz 3 Satz 1 Nummer 1 benennen.

#### § 2 Zusammensetzung und Vorsitz

(1) Der Enquete-Kommission können neben parlamentarischen Mitgliedern auch sachverständige oder sachkundige Personen angehören, die nicht Mitglieder des Landtages sind (nichtparlamentarische Mitglieder).
Der Landtag bestimmt im Einsetzungsbeschluss die Zahl der parlamentarischen Mitglieder und der nichtparlamentarischen Mitglieder.
Die Enquete-Kommission muss mindestens zur Hälfte aus parlamentarischen Mitgliedern bestehen.
Die nichtparlamentarischen Mitglieder dürfen nicht in einem Beschäftigungsverhältnis zu einer Fraktion oder Gruppe oder zu einem Mitglied des Landtages stehen.

(2) Wird zwischen den Fraktionen und Gruppen kein Einvernehmen darüber erzielt, wie viele parlamentarische Mitglieder die jeweilige Fraktion oder Gruppe benennt, werden diese von den Fraktionen und Gruppen im Verhältnis ihrer Stärke benannt.
Jede Fraktion ist berechtigt, mit mindestens einem parlamentarischen Mitglied vertreten zu sein.
Für die parlamentarischen Mitglieder können Mitglieder des Landtages als stellvertretende Mitglieder benannt werden.
Wird zwischen den Fraktionen und Gruppen kein Einvernehmen über die Benennung der nichtparlamentarischen Mitglieder erzielt, werden diese von den Fraktionen und Gruppen im Verhältnis ihrer Stärke benannt.
Die Präsidentin oder der Präsident beruft die nichtparlamentarischen Mitglieder.
Gruppen müssen bei der Herstellung des Einvernehmens nach den Sätzen 1 oder 4 nicht einbezogen werden, wenn sie bei der Verteilung nach dem Stärkeverhältnis nicht zu berücksichtigen sind.

(3) Im Einsetzungsbeschluss kann bestimmt werden, dass

1.  Gruppen, die kein Mitglied nach Absatz 2 Satz 1 benennen können,
2.  die kommunalen Spitzenverbände, wenn der Auftrag im Schwerpunkt kommunale Belange berührt,

jeweils ein beratendes Mitglied der Enquete-Kommission benennen können.
Soweit der Einsetzungsbeschluss nichts anderes festlegt, stehen diesen Mitgliedern die Mitgliedschaftsrechte mit Ausnahme des Stimmrechts und sonstiger Mitbestimmungsrechte zu.
Im Einsetzungsbeschluss kann festgelegt werden, dass für beratende parlamentarische Mitglieder ein stellvertretendes Mitglied benannt werden kann.

(4) Der Landtag wählt die Vorsitzenden der Enquete-Kommissionen und die stellvertretenden Vorsitzenden aus seiner Mitte.

(5) Die Vorsitzenden der in einer Wahlperiode eingesetzten Enquete-Kommissionen werden von den Fraktionen in der Reihenfolge ihrer Stärke vorgeschlagen.

(6) Die stellvertretenden Vorsitzenden der Enquete-Kommissionen, in denen ein Mitglied einer Regierungsfraktion den Vorsitz innehat, werden von den Oppositionsfraktionen in der Reihenfolge ihrer Stärke vorgeschlagen.
Die stellvertretenden Vorsitzenden der Enquete-Kommissionen, in denen ein Mitglied einer Oppositionsfraktion den Vorsitz hat, werden von den Regierungsfraktionen in der Reihenfolge ihrer Stärke vorgeschlagen.

(7) Der Landtag kann die Vorsitzenden und stellvertretenden Vorsitzenden mit der Mehrheit seiner Mitglieder abwählen.
Über einen Antrag auf Abwahl ist ohne Aussprache abzustimmen.
Im Falle der Abwahl bleiben die Vorschlagsrechte der Fraktionen unberührt.

#### § 3 Geltung der Geschäftsordnung des Landtages, Berichterstattungsgruppen

(1) Soweit dieses Gesetz keine Bestimmungen enthält, gelten die Regelungen der Geschäftsordnung des Landtages über die Ausschüsse einschließlich der Regelungen über die Verschwiegenheitspflichten der Ausschussmitglieder entsprechend.

(2) Die Enquete-Kommission kann mit Zustimmung des Präsidiums zur Vorbereitung ihrer Beschlüsse Berichterstattungsgruppen einsetzen.
Für die Berichterstattungsgruppen gilt Absatz 1 mit der Maßgabe, dass

1.  die Enquete-Kommission die Mitglieder der Berichterstattungsgruppen aus ihrer Mitte bestimmt und stellvertretende Mitglieder und Vorsitzende nicht bestimmt werden,
2.  Sitzungen aufgrund einvernehmlicher Vereinbarung der Mitglieder ohne förmliche Einladung unabhängig vom Sitzungsplan stattfinden,
3.  über die Sitzungen ein Ergebnisprotokoll angefertigt wird, das den Mitgliedern der Enquete-Kommission zur Verfügung gestellt wird,
4.  die Öffentlichkeitsarbeit durch die Enquete-Kommission wahrgenommen wird.

#### § 4 Unterrichtungspflicht der Landesregierung

(1) Die Landesregierung ist verpflichtet, der Enquete-Kommission auf Verlangen die für ihre Arbeit erforderlichen Auskünfte zu erteilen sowie Akten und sonstige amtliche Unterlagen vorzulegen.

(2) Die Erteilung von Auskünften oder die Vorlage von Akten und sonstigen amtlichen Unterlagen darf nur abgelehnt werden, wenn überwiegende öffentliche oder private Interessen an der Geheimhaltung dies zwingend erfordern.
Die Entscheidung ist der Enquete-Kommission mitzuteilen und zu begründen.

#### § 5 Abschlussbericht

(1) Nach Abschluss ihrer Tätigkeit, spätestens jedoch drei Monate vor Ende der Wahlperiode des Landtages, erstattet die Enquete-Kommission dem Landtag einen schriftlichen Abschlussbericht.
Jedes Mitglied der Enquete-Kommission ist berechtigt, seine abweichende Meinung darzulegen; diese Darlegungen sind dem Bericht beizufügen.

(2) Der Landtag kann im Einsetzungsbeschluss festlegen, dass die Enquete-Kommission dem Landtag einen Zwischenbericht vorlegt.
Er kann jederzeit einen Bericht über den Stand des Verfahrens verlangen.

#### § 6 Entschädigungen

(1) Den nichtparlamentarischen Mitgliedern der Enquete-Kommission wird auf Antrag Ersatz für entstandenen Verdienstausfall und Reisekostenentschädigung gewährt.
Das Präsidium erlässt entsprechende Richtlinien, die anstelle des Ersatzes des Verdienstausfalles eine einheitliche pauschale Aufwandsentschädigung für alle nichtparlamentarischen Mitglieder im Sinne des § 2 Absatz 1 vorsehen können.

(2) Durch die Enquete-Kommission angehörte Sachverständige erhalten auf Antrag eine Zeugenentschädigung nach dem Justizvergütungs- und -entschädigungsgesetz vom 5.
Mai 2004 (BGBl.
I S. 718, 776), das zuletzt durch Artikel 5 Absatz 2 des Gesetzes vom 11.
Oktober 2016 (BGBl.
I S. 2222, 2224) geändert worden ist, in der jeweils geltenden Fassung.

#### § 7 Übergangsregelungen

Dieses Gesetz gilt für die Enquete-Kommissionen, die zum Zeitpunkt des Inkrafttretens dieses Gesetzes eingesetzt sind, mit der Maßgabe, dass

1.  die Entscheidungen über die Zusammensetzung der Enquete-Kommissionen unberührt bleiben,
2.  Beschlüsse des Präsidiums über die Ausstattung der Enquete-Kommissionen vom Präsidium geändert werden können,
3.  für Beschlüsse nach § 1 Absatz 4 Satz 2 bis 4 der Landtag zuständig ist und die Fraktionen und Gruppen die zusätzlichen Mittel rückwirkend zum 1.
    Januar 2018 erhalten können,
4.  im Falle des Ausscheidens eines Mitgliedes das neue Mitglied von der Fraktion oder Gruppe benannt wird, die das ausscheidende Mitglied benannt hat.