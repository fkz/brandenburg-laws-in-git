## Gesetz zur Umsetzung des Pflegeberufegesetzes in Brandenburg (Brandenburgisches Pflegeberufeumsetzungsgesetz - BbgPflBUmG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Ombudsstelle

(1) Für Streitigkeiten zwischen der oder dem Auszubildenden und dem Träger der praktischen Ausbildung wird bei der zuständigen Stelle nach § 26 Absatz 4 des Pflegeberufegesetzes vom 17. Juli 2017 (BGBl. I S. 2581), das zuletzt durch Artikel 9 des Gesetzes vom 19.
Mai 2020 (BGBl. I S. 1018, 1033) geändert worden ist, eine Ombudsstelle nach § 7 Absatz 6 des Pflegeberufegesetzes eingerichtet.
Die Aufgaben der Ombudsstelle werden durch eine oder mehrere Personen wahrgenommen.
Die Bestellung erfolgt durch die Leitung der zuständigen Stelle nach § 26 Absatz 4 des Pflegeberufegesetzes im Benehmen mit dem für Soziales und Gesundheit zuständigen Ministerium.

(2) Die Tätigkeit der Ombudsperson ist ehrenamtlich.
Die zuständige Stelle nach § 26 Absatz 4 des Pflegeberufegesetzes stellt für die Ombudsstelle die Diensträume zur Verfügung und erstattet die erforderlichen Sachkosten sowie die Kosten nach Absatz 3.

(3) Sofern der Ombudsperson durch ihre Tätigkeit ein Verdienstausfall entsteht, erhält sie eine Entschädigung.
Für die Höhe der Entschädigung gilt § 22 Satz 1 des Justizvergütungs- und -entschädigungsgesetzes vom 5.
Mai 2004 (BGBl. I S. 718, 776), das zuletzt durch Artikel 5 Absatz 2 des Gesetzes vom 11.
Oktober 2016 (BGBl. I S. 2222, 2224) geändert worden ist, entsprechend.

#### § 2 Hochschulische Ausbildung

Befristet bis zum 31.
Dezember 2029 kann die Praxisanleitung nach § 31 Absatz 1 Satz 2 und 3 der Pflegeberufe-Ausbildungs- und -Prüfungsverordnung vom 2.
Oktober 2018 (BGBl. I S. 1572), die durch Artikel 10 des Gesetzes vom 19.
Mai 2020 (BGBl. I S. 1018, 1033) geändert worden ist, auch von Personen durchgeführt werden, die die Anforderungen nach § 4 Absatz 2 der Pflegeberufe-Ausbildungs- und -Prüfungsverordnung erfüllen.

#### § 3 Verordnungsermächtigung

Das für Soziales und Gesundheit zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung Bestimmungen zu treffen über:

1.  die näheren Anforderungen an die Geeignetheit von Einrichtungen nach § 7 Absatz 1 und 2 in Verbindung mit § 7 Absatz 5 Satz 1 des Pflegeberufegesetzes zur Durchführung von Teilen der praktischen Ausbildung einschließlich der Angemessenheit des Verhältnisses von Auszubildenden zu Pflegefachkräften sowie die näheren Voraussetzungen, unter denen die Durchführung der Ausbildung nach § 7 Absatz 5 Satz 2 des Pflegeberufegesetzes untersagt werden kann,
2.  das Nähere zu den Kooperationsverträgen nach § 8 Absatz 1 Satz 2 der Pflegeberufe-Ausbildungs- und -Prüfungsverordnung,
3.  weitergehende Regelungen nach § 31 Absatz 1 Satz 3 der Pflegeberufe-Ausbildungs- und -Prüfungsverordnung zu den Anforderungen an die Qualifikation der Praxisanleiterinnen und Praxisanleiter für die hochschulische Ausbildung im Einvernehmen mit dem für die Hochschulen zuständigen Mitglied der Landesregierung,
4.  ergänzende Regelungen nach § 26 Absatz 6 Satz 1 des Pflegeberufegesetzes,
5.  ergänzende Regelungen nach § 33 Absatz 4 Satz 5 des Pflegeberufegesetzes zu den in einer Umlageordnung nach § 56 Absatz 3 Nummer 3 des Pflegeberufegesetzes geregelten Verfahren,
6.  das Nähere zum Prüfverfahren der Ausgleichszuweisungen nach § 34 Absatz 6 Satz 3 des Pflegeberufegesetzes, soweit nicht das Bundesministerium für Familie, Senioren, Frauen und Jugend und das Bundesministerium für Gesundheit von der Ermächtigung nach § 56 Absatz 3 Nummer 4 des Pflegeberufegesetzes Gebrauch machen,
7.  zusätzliche Anordnungen gemäß § 55 Absatz 2 des Pflegeberufegesetzes über Sachverhalte des Pflege- und Gesundheitswesens als Landesstatistik zu erheben, sofern sie nicht von § 55 Absatz 1 des Pflegeberufegesetzes erfasst sind,
8.  das Nähere zum Verfahren nach § 12 Absatz 3 Satz 2 der Pflegeberufe-Ausbildungsfinanzierungsverordnung vom 2.
    Oktober 2018 (BGBl. I S. 1622).

#### § 4 Einschränkung von Grundrechten

Durch § 3 Nummer 1 bis 8 wird das Grundrecht auf Datenschutz (Artikel 11 Absatz 1 der Verfassung des Landes Brandenburg), durch § 3 Nummer 1 und 2 wird das Grundrecht der Unverletzlichkeit der Wohnung (Artikel 13 des Grundgesetzes, Artikel 15 der Verfassung des Landes Brandenburg) und durch § 3 Nummer 3 wird das Grundrecht auf Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.