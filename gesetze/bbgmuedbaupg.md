## Brandenburgisches Marktüberwachungsdurchführungsgesetz für Bauprodukte (BbgMÜDBauPG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Aufbau und Aufgaben der Marktüberwachungsbehörden für Bauprodukte

(1) Oberste Marktüberwachungsbehörde ist das für Bauen zuständige Ministerium.
Obere Marktüberwachungsbehörde ist das Landesamt für Bauen und Verkehr als Bautechnisches Prüfamt.
Gemeinsame Marktüberwachungsbehörde ist das Deutsche Institut für Bautechnik mit dem Sitz in Berlin.

(2) Die Marktüberwachungsbehörden nehmen die Aufgaben nach folgenden Vorschriften wahr:

1.  Kapitel III der Verordnung (EG) Nr. 765/2008 des Europäischen Parlaments und des Rates vom 9. Juli 2008 über die Vorschriften für die Akkreditierung und die Marktüberwachung im Zusammenhang mit der Vermarktung von Produkten und zur Aufhebung der Verordnung (EWG) Nr. 339/93 des Rates (ABl. L 218 vom 13.8.2008, S. 30) hinsichtlich der Bauprodukte, die nach der Verordnung (EU) Nr. 305/2011 des Europäischen Parlaments und des Rates vom 9. März 2011 zur Festlegung harmonisierter Bedingungen für die Vermarktung von Bauprodukten und zur Aufhebung der Richtlinie 89/106/EWG des Rates (ABl.
    L 88 vom 4.4.2011, S. 5; L 103 vom 12.4.2013, S. 10), die zuletzt durch die Verordnung (EU) Nr. 574/2014 (ABl.
    L 159 vom 28.5.2014, S. 41) geändert worden ist, in den Verkehr gebracht und gehandelt werden dürfen,
2.  Produktsicherheitsgesetz vom 8.
    November 2011 (BGBl. I S. 2178, 2179; 2012 I S. 131), das durch Artikel 435 der Verordnung vom 31.
    August 2015 (BGBl. I S. 1474, 1538) geändert worden ist, soweit es auf die Marktüberwachung nach dem Bauproduktengesetz vom 5. Dezember 2012 (BGBl. I S. 2449, 2450), das durch Artikel 119 der Verordnung vom 31. August 2015 (BGBl. I S. 1474, 1494) geändert worden ist, Anwendung findet,
3.  Verordnung (EU) Nr. 305/2011 und
4.  Bauproduktengesetz.

Für die Aufsicht über die gemeinsame Marktüberwachungsbehörde gilt Artikel 5 des Abkommens über das Deutsche Institut für Bautechnik vom 2.
Dezember 1992 (GVBl. 1993 I S. 203), das zuletzt durch das Abkommen vom 26.
Oktober 2016 (GVBl. für Berlin 2018 S. 155, 192) geändert worden ist.

(3) Das für Bauprodukte zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung weitere öffentliche Aufgaben des Bautechnischen Prüfamtes im Landesamt für Bauen und Verkehr im Zusammenhang mit der Marktüberwachung von Bauprodukten zu regeln.

#### § 2 Zuständigkeit der Marktüberwachungsbehörden für Bauprodukte

(1) Zuständig für die Durchführung der Aufgaben nach § 1 Absatz 2 ist die obere Marktüberwachungsbehörde, soweit nachfolgend nichts Abweichendes bestimmt ist.

(2) Die gemeinsame Marktüberwachungsbehörde ist zuständig für

1.  die einheitliche Prüfung und Bewertung von Bauprodukten in technischer Hinsicht,
2.  das Ergreifen von Maßnahmen nach den Artikeln 56 und 58 der Verordnung (EU) Nr. 305/2011 für Fälle, in denen Bauprodukte nach den Anforderungen der Verordnung (EU) Nr. 305/2011 die in Bezug auf die Wesentlichen Merkmale erklärte Leistung nicht erbringen oder eine Gefahr im Sinne des Artikels 58 der Verordnung (EU) Nr. 305/2011 darstellen,
3.  das Ergreifen von Maßnahmen nach § 26 des Produktsicherheitsgesetzes für Fälle, in denen Bauprodukte nach den Anforderungen der Verordnung (EU) Nr. 305/2011 die in Bezug auf die Wesentlichen Merkmale erklärte Leistung nicht erbringen oder eine Gefahr im Sinne des Artikels 58 der Verordnung (EU) Nr. 305/2011 darstellen,
4.  das Ergreifen von Maßnahmen nach den Artikeln 16, 19, 20, 28 und 29 der Verordnung (EG) Nr. 765/2008 für Fälle, in denen Bauprodukte nach den Anforderungen der Verordnung (EU) 305/2011 die in Bezug auf die Wesentlichen Merkmale erklärte Leistung nicht erbringen oder eine Gefahr im Sinne des Artikels 58 der Verordnung (EU) Nr. 305/2011 darstellen.

(3) Besteht für die obere Marktüberwachungsbehörde Grund zur Annahme, dass Maßnahmen oder Anordnungen nach Absatz 2 in Betracht kommen, gibt sie die Sachbehandlung für das Produkt an die gemeinsame Marktüberwachungsbehörde ab.
Die Zuständigkeit der gemeinsamen Marktüberwachungsbehörde beginnt mit dem Eingang der Abgabe.
Soweit nachfolgend nichts Abweichendes bestimmt ist, umfasst sie alle Aufgaben und Befugnisse nach § 1 Absatz 2 und 3.
Sie schließt die Zuständigkeit der oberen Marktüberwachungsbehörde auch dann aus, wenn sie durch die Abgabe der Sachbehandlung für das Produkt durch eine Marktüberwachungsbehörde eines anderen Landes begründet worden ist.
Die Befugnis der oberen Marktüberwachungsbehörde, bei Gefahr im Verzug vorläufige Maßnahmen und Anordnungen zu treffen, bleibt unberührt.
Die Aufhebung eines Verwaltungsaktes einer Marktüberwachungsbehörde, der nicht nach § 44 des Verwaltungsverfahrensgesetzes nichtig ist, kann nicht allein deshalb beansprucht werden, weil die Voraussetzungen des Satzes 1 nicht vorgelegen haben oder die obere Marktüberwachungsbehörde die Sachbehandlung nicht an die gemeinsame Marktüberwachungsbehörde abgegeben hat, obwohl die Voraussetzungen des Satzes 1 vorgelegen haben.
Im Übrigen bleiben die §§ 45 und 46 des Verwaltungsverfahrensgesetzes unberührt.

(4) Maßnahmen und Anordnungen der gemeinsamen Marktüberwachungsbehörde gelten auch im Land Brandenburg.

(5) Der Vollzug der Maßnahmen und Anordnungen der gemeinsamen Marktüberwachungsbehörde einschließlich der Anordnung von Maßnahmen des Verwaltungszwangs obliegt der oberen Marktüberwachungsbehörde.

#### § 3 Inkrafttreten, Außerkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.
Gleichzeitig tritt das Brandenburgische Marktüberwachungsdurchführungsgesetz für Bauprodukte vom 14.
Mai 2012 (GVBl.
I Nr. 22), das durch Artikel 5 des Gesetzes vom 14. Mai 2012 (GVBl.
I Nr. 22, S. 4; 2014 I Nr. 24) geändert worden ist, außer Kraft.

Potsdam, den 1.
April 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark