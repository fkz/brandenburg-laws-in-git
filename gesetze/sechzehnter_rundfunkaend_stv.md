## Gesetz zum Sechzehnten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Sechzehnter Rundfunkänderungsstaatsvertrag)

Der Landtag hat das folgende Gesetz beschlossen:

### § 1

Dem am 9.
Juli 2014 vom Land Brandenburg unterzeichneten Sechzehnten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Sechzehnter Rundfunkänderungsstaatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem Artikel 2 Absatz 2 Satz 1 vorbehaltlich des Satzes 2 am 1. April 2015 in Kraft.
Artikel 1 Nummer 3 des Staatsvertrages tritt nach Artikel 2 Absatz 2 Satz 2 des Staatsvertrages am 1.
Januar 2017 in Kraft.
Sollte der Staatsvertrag nach seinem Artikel 2 Absatz 2 Satz 3 gegenstandslos werden, ist dies unverzüglich im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 10.
Februar 2015

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag](/vertraege/rfinstv/11) - Rundfunkfinanzierungsstaatsvertrag

* * *

### Anlagen

1

[Sechzehnter Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Sechzehnter Rundfunkänderungsstaatsvertrag)](/br2/sixcms/media.php/68/GVBl_I_05_2015-Anlage.pdf "Sechzehnter Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Sechzehnter Rundfunkänderungsstaatsvertrag)") 621.9 KB