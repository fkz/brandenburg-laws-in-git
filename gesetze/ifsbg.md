## Gesetz zur Beteiligung des Landtages bei Maßnahmen nach dem Infektionsschutzgesetz (Infektionsschutzbeteiligungsgesetz - IfSBG)

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 Zweck des Gesetzes

Zweck des Gesetzes ist es, den Landtag Brandenburg bei wesentlichen Entscheidungen über die im Land Brandenburg notwendigen Schutzmaßnahmen zur Verhinderung der Verbreitung des Coronavirus SARS-CoV-2 nach §§ 28 bis 31 des Infektionsschutzgesetzes zu beteiligen.

#### § 2 Anwendungsbereich

Dieses Gesetz findet Anwendung auf Verordnungen, die aufgrund des § 32 des Infektionsschutzgesetzes in Verbindung mit § 2 der Verordnung zur Regelung von Zuständigkeiten und zur Ermächtigung zum Erlass von Rechtsverordnungen nach dem Infektionsschutzgesetz zur Verhinderung der Verbreitung des Coronavirus SARS-CoV-2 erlassen werden.
Gleiches gilt für die Änderung, Verlängerung oder Aufhebung dieser Verordnungen.

#### § 3 Information und Beteiligung des Landtages

(1) Entwürfe für Rechtsverordnungen nach § 2 sowie deren Verlängerung, Änderung oder Aufhebung sind dem Landtag unverzüglich und vor dem Erlass zuzuleiten.

(2) Kann die Zuleitung nicht vor dem Erlass der Rechtsverordnung stattfinden, ist der Landtag hierüber in Kenntnis zu setzen.
In diesen Fällen ist die Zuleitung unverzüglich nachzuholen.
In der nachgeholten Zuleitung sollen die Gründe für das Abweichen von Absatz 1 dargelegt werden.

#### § 4 Geltungsdauer und Widerspruch

(1) Die Geltungsdauer der Rechtsverordnung in ihrer zuletzt geänderten Fassung beträgt grundsätzlich vier Wochen; sie kann verlängert werden.

(2) Der Landtag kann einer Rechtsverordnung nach § 2 innerhalb von sieben Tagen nach deren Verkündung mit der Mehrheit seiner Mitglieder widersprechen.

(3) Widerspricht der Landtag einer Verordnung, hat der Verordnungsgeber die Rechtsverordnung nach § 2 spätestens sieben Tage nach Beschlussfassung aufzuheben.

#### § 5 Geschäftsordnung des Landtages

Das Nähere zu den §§ 3 und 4 regelt die Geschäftsordnung des Landtages.

#### § 6 Rechtsverstöße

Die Rechtmäßigkeit einer Rechtsverordnung wird durch einen Verstoß gegen Pflichten nach diesem Gesetz nicht berührt.

#### § 7 Inkrafttreten, Außerkrafttreten

Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.
Es tritt am 31. Dezember 2022 außer Kraft.

Potsdam, 15.
Dezember 2020

Die Präsidentin  
des Landtages Brandenburg

Dr.
Ulrike Liedtke