## Gesetz über die Geodateninfrastruktur im Land Brandenburg  (Brandenburgisches Geodateninfrastrukturgesetz - BbgGDIG)

### Inhaltsübersicht

[§ 1    Ziel](#1)  
[§ 2    Anwendungsbereich](#2)  
[§ 3    Begriffsbestimmungen](#3)  
[§ 4    Betroffene Geodaten und Geodatendienste](#4)  
[§ 5    Geodaten](#5)  
[§ 6    Geodatendienste und weitere Netzdienste](#6)  
[§ 7    Metadaten](#7)  
[§ 8    Interoperabilität](#8)  
[§ 9    Geodateninfrastruktur und Geoportal](#9)  
[§ 10  Koordinierung](#10)  
[§ 11  Nutzung und Lizenzierung](#11)  
[§ 12  Schutz öffentlicher und sonstiger Belange](#12)  
[§ 13  Gebühren und Entgelte](#13)  
[§ 14  Verordnungsermächtigung](#14)

[Anhang 1](#A1)  
Geodatenthemen zu § 4 Absatz 1 Nummer 4 entsprechend Anhang I der Richtlinie 2007/2/EG

[Anhang 2](#A2)  
Geodatenthemen zu § 4 Absatz 1 Nummer 4 entsprechend Anhang II der Richtlinie 2007/2/EG

[Anhang 3](#A3)  
Geodatenthemen zu § 4 Absatz 1 Nummer 4 entsprechend Anhang III der Richtlinie 2007/2/EG

#### § 1  
Ziel

(1) Das Gesetz schafft den rechtlichen Rahmen für den Aufbau und den Betrieb der Geodateninfrastruktur Brandenburg als Bestandteil der nationalen Geodateninfrastruktur im Sinne der Richtlinie 2007/2/EG des Europäischen Parlaments und des Rates vom 14.
März 2007 zur Schaffung einer Geodateninfrastruktur in der Europäischen Gemeinschaft (ABl. L 108 vom 25.4.2007, S. 1).

(2) Die Geodateninfrastruktur Brandenburg soll sicherstellen, dass Geodaten über das Gebiet des Landes Brandenburg den öffentlichen Stellen, der Wirtschaft, der Wissenschaft und dem Einzelnen für eine breite Nutzung

1.  nachhaltig, aktuell und in hoher Qualität zur Verfügung stehen sowie
    
2.  einfach und schnell zugänglich sind.
    

#### § 2  
Anwendungsbereich

(1) Das Gesetz begründet Pflichten für Behörden.
Es eröffnet Rechte für die in § 1 Absatz 2 genannten Nutzer.

(2) Behörden im Sinne dieses Gesetzes sind

1.  die in § 1 Absatz 2 des Verwaltungsverfahrensgesetzes für das Land Brandenburg vom 7. Juli 2009 (GVBl.
    I S. 262, 264) in der jeweils geltenden Fassung bezeichneten Stellen; öffentliche Gremien, die diese Stellen beraten, gelten als Teil der Stelle, die deren Mitglieder beruft,
    
2.  natürliche oder juristische Personen des Privatrechts, soweit sie öffentliche Aufgaben wahrnehmen oder öffentliche Dienstleistungen erbringen und dabei der Kontrolle des Landes Brandenburg oder einer unter seiner Aufsicht stehenden juristischen Person des öffentlichen Rechts unterliegen.
    

(3) Dieses Gesetz gilt auch für natürliche und juristische Personen des Privatrechts (Dritte), denen nach § 9 Absatz 4 Anschluss an die Geodateninfrastruktur gewährt wird.

#### § 3  
Begriffsbestimmungen

(1) Geodaten sind Daten mit direktem oder indirektem Bezug zu einem bestimmten Standort oder geographischen Gebiet.
Sie gliedern sich in Geobasis- und Geofachdaten.
Geobasisdaten sind die Geodaten des amtlichen Vermessungswesens; Geodaten anderer Fachbereiche werden als Geofachdaten bezeichnet.

(2) Metadaten sind Informationen, die Geodaten oder Geodatendienste beschreiben und es ermöglichen, diese zu ermitteln, in Verzeichnisse aufzunehmen und zu nutzen.

(3) Geodatendienste sind Netzdienste, die Geodaten und Metadaten in strukturierter Form zugänglich machen.
Sie umfassen insbesondere

1.  Suchdienste, die es ermöglichen, auf der Grundlage des Inhalts entsprechender Metadaten nach Geodaten und Geodatendiensten zu suchen und den Inhalt der Metadaten anzuzeigen,
    
2.  Darstellungsdienste, die es mindestens ermöglichen, darstellbare Geodaten anzuzeigen, in ihnen zu navigieren, sie zu vergrößern oder zu verkleinern, zu verschieben, Daten zu überlagern sowie Informationen aus Legenden und sonstige relevante Inhalte von Metadaten anzuzeigen,
    
3.  Downloaddienste, die das Herunterladen von Geodaten ermöglichen,
    
4.  Transformationsdienste zur geodätischen Umwandlung von Geodaten und
    
5.  Dienste zum Abrufen von Geodatendiensten, die es erlauben, Anforderungen an Geodaten zu definieren und verschiedene Geodatendienste zu kombinieren.
    

(4) Interoperabilität ist die Kombinierbarkeit von Daten beziehungsweise die Kombinierbarkeit und Interaktionsfähigkeit verschiedener Systeme oder Techniken unter Einhaltung gemeinsamer Standards.

(5) Netzdienste sind netzbasierte Anwendungen zur Kommunikation, Transaktion und Interaktion.

(6) Die Geodateninfrastruktur ist eine Infrastruktur, bestehend aus Geodaten, Metadaten, Geodatendiensten, Netzdiensten und -technologien, Vereinbarungen über gemeinsame Nutzung, über Zugang und Verwendung sowie Koordinierungs- und Überwachungsmechanismen, -prozesse und -verfahren mit dem Ziel, Geodaten verschiedener Herkunft interoperabel verfügbar zu machen.

(7) Ein Geoportal ist eine Kommunikations-, Transaktions- und Interaktionsplattform, die über Geodatendienste und weitere Netzdienste den Zugang zu den Geodaten ermöglicht.

#### § 4  
Betroffene Geodaten und Geodatendienste

(1) Dieses Gesetz gilt für Geodaten, die noch in Verwendung stehen und folgende Bedingungen erfüllen:

1.  sie beziehen sich auf das Hoheitsgebiet des Landes Brandenburg,
    
2.  sie liegen in elektronischer Form vor,
    
3.  sie sind vorhanden bei
    
    1.  einer Behörde und wurden von einer Behörde erstellt oder sind bei einer solchen eingegangen oder sie werden von dieser Behörde verwaltet oder aktualisiert und fallen unter ihren öffentlichen Auftrag,
        
    2.  Dritten, denen gemäß § 9 Absatz 4 Anschluss an die Geodateninfrastruktur gewährt wird,
        
    
    oder werden für diese bereitgehalten und
    
4.  sie betreffen ein oder mehrere Themen der Anhänge 1, 2 oder 3 entsprechend den Anhängen zur Richtlinie 2007/2/EG.
    

(2) Sind identische Kopien derselben Geodaten bei verschiedenen Behörden vorhanden oder werden sie für diese bereitgehalten, so gilt das Gesetz nur für die Referenzversion, von der die verschiedenen Kopien abgeleitet sind.

(3) Dieses Gesetz gilt auch für Geodatendienste, die sich auf die in Absatz 1 genannten Geodaten beziehen.

(4) Im Fall von Geodaten nach Absatz 1 und Geodatendiensten nach Absatz 3, an denen Dritte Rechte geistigen Eigentums haben, kann die Behörde Maßnahmen gemäß diesem Gesetz nur mit deren Zustimmung treffen.

(5) Die bei den Gemeinden und Gemeindeverbänden vorhandenen Geodaten und Geodatendienste im Sinne von Absatz 1 und 3 unterliegen diesem Gesetz nur, wenn ihre Erfassung oder Verbreitung rechtlich vorgeschrieben ist.

#### § 5  
Geodaten

(1) Die Geodaten nach § 4 Absatz 1 sind Bestandteil der nationalen Geodatenbasis.
Sie werden durch die hierfür jeweils ursprünglich zuständigen Stellen bereitgestellt.

(2) Die Daten des amtlichen Vermessungswesens sind als Geobasisdaten nach § 6 Absatz 2 des Brandenburgischen Vermessungsgesetzes die fachneutralen Kernkomponenten der Geodateninfrastruktur Brandenburg.
Die Geofachdaten der öffentlichen Stellen sind auf der Grundlage der Geobasisdaten zu erfassen und zu führen.

(3) Soweit Geodaten sich auf einen geographischen Standort oder ein geographisches Gebiet beziehen, dessen Lage sich auf das Hoheitsgebiet eines oder mehrerer weiterer Länder oder auf das Hoheitsgebiet der Republik Polen erstreckt, stimmen die zuständigen Behörden mit den jeweils zuständigen Stellen dieser Länder, des Bundes oder der Republik Polen die Darstellung und die Position des geographischen Standorts beziehungsweise des geographischen Gebiets ab.

#### § 6  
Geodatendienste und weitere Netzdienste

(1) Die Behörden gewährleisten, dass für die bei ihnen vorgehaltenen Geodaten und Metadaten Geodatendienste bereitstehen.
Zur Erfüllung dieser Verpflichtung können sie sich übergeordneter oder zentraler Dienste bedienen.
Soweit für die Nutzung von Diensten Gebühren oder Entgelte gefordert werden oder lizenzrechtliche Regelungen zu treffen sind, sind Netzdienste zur Abwicklung eines elektronischen Geschäftsverkehrs und zur Sicherstellung des Betriebs von Geodatendiensten zur Verfügung zu stellen.

(2) Die Dienste nach Absatz 1 sollen Nutzeranforderungen berücksichtigen und müssen über computergestützte Netzwerke öffentlich verfügbar sein.

(3) Transformationsdienste sind mit anderen Diensten nach Absatz 1 so zu kombinieren, dass sämtliche Geodatendienste im Einklang mit diesem Gesetz betrieben werden können.

(4) Für Suchdienste ist zumindest folgende Kombination von Suchkriterien zu gewährleisten:

1.  Schlüsselwörter,
    
2.  Klassifizierung von Geodaten und Geodatendiensten,
    
3.  Qualitätsmerkmale,
    
4.  geographischer Standort,
    
5.  die für die Erfassung, Führung und Bereitstellung der Geodaten und Geodatendienste zuständige Behörde sowie
    
6.  Bedingungen für den Zugang zu und die Nutzung von Geodaten und Geodatendiensten.
    

#### § 7  
Metadaten

(1) Die Behörden, welche Geodaten als Referenzversion im Sinne von § 4 Absatz 2 und Geodatendienste bereitstellen, haben die zugehörigen Metadaten zu erstellen, zu führen und bereitzustellen sowie in Übereinstimmung mit den Geodaten und Geodatendiensten zu halten.

(2) Als Metadaten zu Geodaten sind mindestens anzugeben:

1.  Schlüsselwörter,
    
2.  Klassifizierung,
    
3.  Qualitätsmerkmale,
    
4.  geographischer Standort,
    
5.  die für die Erfassung, Führung und Bereitstellung zuständige Behörde und
    
6.  Bedingungen für den Zugang und die Nutzung einschließlich bestehender Beschränkungen nach § 12 und deren Gründe sowie gegebenenfalls anfallende Gebühren oder Entgelte.
    

(3) Als Metadaten zu Geodatendiensten sind mindestens anzugeben:

1.  Qualitätsmerkmale,
    
2.  die für die Erfassung, Führung und Bereitstellung zuständige Behörde sowie
    
3.  Bedingungen für den Zugang und die Nutzung einschließlich bestehender Beschränkungen nach § 12 und deren Gründe sowie gegebenenfalls anfallende Gebühren oder Entgelte.
    

#### § 8  
Interoperabilität

Geodaten und Geodatendienste sowie Metadaten sind interoperabel bereitzustellen.

#### § 9  
Geodateninfrastruktur und Geoportal

(1) Metadaten, Geodaten, Geodatendienste und weitere Netzdienste werden für den Aufbau und den Betrieb der Geodateninfrastruktur Brandenburg als Bestandteil der nationalen Geodateninfrastruktur über ein elektronisches Netzwerk verknüpft.

(2) Beim Aufbau und Betrieb der Geodateninfrastruktur arbeitet das Land Brandenburg mit dem Bund und den anderen Ländern der Bundesrepublik Deutschland, insbesondere mit dem Land Berlin, zusammen.
Mit dem Land Berlin wird eine gemeinsame Geodateninfrastruktur aufgebaut.

(3) Im Geschäftsbereich der gemäß § 10 Absatz 2 zuständigen obersten Landesbehörde wird ein Geoportal für den Zugang zum elektronischen Netzwerk nach Absatz 1 eingerichtet.

(4) Geodaten, Geodatendienste und Metadaten Dritter sind auf deren Anfrage über das Geoportal nach Absatz 3 bereitzustellen, sofern sie sich verpflichten, die Daten nach den Bestimmungen dieses Gesetzes bereitzustellen und hierfür die technischen Voraussetzungen zu schaffen.
Die Bereitstellung bedarf der Genehmigung durch die ressortübergreifende Kontaktstelle nach § 10 Absatz 1.
Die Genehmigung kann widerrufen werden, wenn wesentliche Genehmigungsvoraussetzungen wegfallen.

(5) Die Bereitstellung von Geodaten, Geodatendiensten und Metadaten an das Geoportal hat unter Beachtung der im Brandenburgischen Datenschutzgesetz in der Fassung der Bekanntmachung vom 15.
Mai 2008 (GVBl.
I S.
114) in der jeweils geltenden Fassung und im Bundesdatenschutzgesetz in der Fassung der Bekanntmachung vom 14.
Januar 2003 (BGBl.
I S.
66), das zuletzt durch Artikel 1 des Gesetzes vom 14.
August 2009 (BGBl.
I S.
2814) geändert worden ist, in der jeweils geltenden Fassung festgelegten Grundsätze des Schutzes personenbezogener Daten zu erfolgen.
Die Regelungen des Urheberrechtsgesetzes vom 9.
September 1965 (BGBl.
I S.
1273), das zuletzt durch Artikel 83 des Gesetzes vom 17.
Dezember 2008 (BGBl.
I S.
2586) geändert worden ist, in der jeweils geltenden Fassung bleiben unberührt.

#### § 10  
Koordinierung

(1) Die nationale Anlaufstelle auf Bundesebene nach Artikel 19 Absatz 2 der Richtlinie 2007/2/EG wird durch eine ressortübergreifende Kontaktstelle im Land Brandenburg unterstützt.

(2) Die für Inneres zuständige oberste Landesbehörde ist federführend bei der Konzeption und koordinierend bei der Umsetzung der Geodateninfrastruktur Brandenburg und deren künftigem Betrieb tätig.
Sie richtet die ressortübergreifende Kontaktstelle ein.

#### § 11  
Nutzung und Lizenzierung

(1) Geodaten und Geodatendienste sind vorbehaltlich des § 12 und nach Maßgabe des § 13 öffentlich bereitzustellen.
Werden Geodaten über Darstellungsdienste bereitgestellt, kann dies in einer Form geschehen, welche eine Weiterverwendung, insbesondere zu kommerziellen Zwecken, oder das Ausdrucken ausschließt.

(2) Behörden, die Geodaten nach § 4 Absatz 1 und Geodatendienste nach § 6 Absatz 1 Satz 1 und 2 anbieten, können für deren Nutzung Haftungsausschlüsse, elektronische Lizenzvereinbarungen oder Lizenzen in sonstiger Form festsetzen.

(3) Soweit für die Bereitstellung von Geodaten und Geodatendiensten an Behörden oder Organe und Einrichtungen der Europäischen Gemeinschaft zur Wahrnehmung öffentlicher Aufgaben oder zur Erfüllung ihrer aus dem Gemeinschaftsrecht erwachsenden Berichtspflichten Lizenzen erteilt werden, müssen sie mit dem allgemeinen Ziel des Austauschs von Geodaten und Geodatendiensten zwischen Behörden vereinbar sein.

(4) Soweit Behörden anderer Mitgliedstaaten der Europäischen Gemeinschaft öffentliche Aufgaben wahrnehmen, die Auswirkungen auf die Umwelt haben können, finden die Regelungen des Absatzes 3 auch auf diese Anwendung.
Satz 1 gilt auf der Grundlage von Gegenseitigkeit und Gleichwertigkeit auch für die Lizenzerteilung an durch internationale Übereinkünfte geschaffene Einrichtungen, soweit die Europäische Gemeinschaft oder ihre Mitgliedstaaten zu deren Vertragsparteien gehören.

(5) Die Bedingungen für den Zugang zu den Geodaten durch Organe und Einrichtungen der Europäischen Gemeinschaft sind einheitlich zu gestalten.

#### § 12  
Schutz öffentlicher und sonstiger Belange

(1) Der Zugang der Öffentlichkeit zu Geodaten und Geodatendiensten über Suchdienste im Sinne des § 3 Absatz 3 Satz 2 Nummer 1 kann beschränkt werden, wenn dieser Zugang nachteilige Auswirkungen auf die internationalen Beziehungen, die öffentliche Sicherheit oder die Verteidigung haben kann, es sei denn, das öffentliche Interesse am Zugang überwiegt.

(2) Der Zugang der Öffentlichkeit zu Geodaten nach § 4 Absatz 1 und Geodatendiensten nach § 3 Absatz 3 Satz 2 Nummer 2 bis 5 kann beschränkt werden, wenn dieser Zugang nachteilige Auswirkungen hat auf

1.  die internationalen Beziehungen, die Verteidigung oder die öffentliche Sicherheit,
    
2.  die Vertraulichkeit der Beratungen von Behörden,
    
3.  die Durchführung eines laufenden Gerichtsverfahrens, den Anspruch einer Person auf ein faires Verfahren oder die Durchführung strafrechtlicher, ordnungswidrigkeitenrechtlicher oder disziplinarrechtlicher Ermittlungen oder
    
4.  den Schutz der Umweltbereiche, auf die sich diese Daten beziehen, wie beispielsweise die Aufenthaltsorte seltener Tierarten,
    

es sei denn, das öffentliche Interesse am Zugang überwiegt.

(3) Der Zugang der Öffentlichkeit zu Geodaten nach § 4 Absatz 1 und Geodatendiensten nach § 3 Absatz 3 Satz 2 Nummer 2 bis 5 ist zu beschränken, soweit dadurch

1.  personenbezogene Daten offengelegt und dadurch schutzwürdige Interessen der Betroffenen beeinträchtigt oder
2.  Betriebs- oder Geschäftsgeheimnisse offengelegt

würden.
Dies gilt nicht, wenn die Betroffenen eingewilligt haben oder das öffentliche Interesse an dem Zugang überwiegt.
Vor der Entscheidung über die Offenlegung der durch Satz 1 Nummer 1 und 2 geschützten Informationen sind die Betroffenen anzuhören.
Die Behörde hat in der Regel von einer Betroffenheit im Sinne des Satzes 1 Nummer 2 auszugehen, soweit übermittelte Informationen als Betriebs- und Geschäftsgeheimnisse gekennzeichnet sind.
Soweit die Behörde dies verlangt, haben mögliche Betroffene im Einzelnen darzulegen, dass ein Betriebs- oder Geschäftsgeheimnis im Sinne des Satzes 1 Nummer 2 vorliegt.
Informationen, die Dritte einer Behörde übermittelt haben, ohne rechtlich dazu verpflichtet zu sein oder rechtlich dazu verpflichtet werden zu können, und deren Offenlegung nachteilige Auswirkungen auf die Interessen der Dritten hätte, dürfen ohne deren Einwilligung anderen nicht zugänglich gemacht werden, es sei denn, das öffentliche Interesse an der Bekanntgabe überwiegt.
Der Zugang zu Geodaten über Emissionen kann nicht unter Berufung auf die in Absatz 2 Nummer 2 und 4, Satz 1 Nummer 1 und 2 sowie in Satz 6 genannten Gründe abgelehnt werden.

(4) Gegenüber Behörden im Sinne von § 2 Absatz 2 Nummer 1 sowie gegenüber entsprechenden Stellen der Länder, des Bundes, der Gemeinden sowie Gemeindeverbände und anderer Mitgliedstaaten der Europäischen Gemeinschaft sowie gegenüber Organen und Einrichtungen der Europäischen Gemeinschaft können der Zugang zu Geodaten und Geodatendiensten sowie der Austausch und die Nutzung von Geodaten beschränkt werden, wenn hierdurch

1.  die Durchführung eines laufenden Gerichtsverfahrens, der Anspruch einer Person auf ein faires Verfahren oder die Durchführung strafrechtlicher, ordnungswidrigkeitenrechtlicher oder disziplinarrechtlicher Ermittlungen,
    
2.  die öffentliche Sicherheit,
    
3.  die Verteidigung oder
    
4.  die internationalen Beziehungen
    

gefährdet werden.
§ 9 Absatz 5 gilt entsprechend.

#### § 13  
Gebühren und Entgelte

(1) Behörden, die Geodaten nach § 4 und Geodatendienste nach § 3 Absatz 3 Satz 2 Nummer 2 bis 5 anbieten, können für deren Nutzung Gebühren oder Entgelte fordern, soweit durch besondere Rechtsvorschrift nichts anderes bestimmt ist.

(2) Suchdienste stehen kostenlos zur Verfügung.

(3) Darstellungsdienste stehen kostenlos zur Verfügung, soweit sie nicht über eine netzgebundene Bildschirmdarstellung hinausgehen.
Abweichend von Satz 1 können für die Nutzung von Darstellungsdiensten Gebühren oder Entgelte gefordert werden, wenn diese die Pflege der Geodaten und der entsprechenden Geodatendienste sichern, insbesondere in Fällen, in denen große Datenmengen mehrfach monatlich aktualisiert werden.

(4) Soweit von Behörden oder Organen und Einrichtungen der Europäischen Gemeinschaft Gebühren oder Entgelte gefordert werden, müssen sie mit dem allgemeinen Ziel des Austauschs von Geodaten und Geodatendiensten zwischen Behörden vereinbar sein.
Die Gebühren oder Entgelte dürfen das zur Gewährleistung der nötigen Qualität und des Angebots von Geodaten und Geodatendiensten notwendige Minimum zuzüglich einer angemessenen Rendite nicht übersteigen.
Dabei sind die Selbstfinanzierungserfordernisse der Behörden, die Geodaten und Geodatendienste anbieten, sowie der Aufwand der Datenerhebung und der öffentliche Zweck des Datenzugangs der Organe und Einrichtungen der Europäischen Gemeinschaft angemessen zu berücksichtigen.
Werden Geodaten oder Geodatendienste Organen oder Einrichtungen der Europäischen Gemeinschaft zur Erfüllung von aus dem Gemeinschaftsumweltrecht erwachsenden Berichtspflichten zur Verfügung gestellt, werden keine Gebühren oder Entgelte gefordert.

(5) Soweit Behörden anderer Mitgliedstaaten der Europäischen Gemeinschaft öffentliche Aufgaben wahrnehmen, die Auswirkungen auf die Umwelt haben können, finden die Regelungen des Absatzes 3 auch auf diese Anwendung.
Satz 1 gilt auf der Grundlage von Gegenseitigkeit und Gleichwertigkeit auch für durch internationale Übereinkünfte geschaffene Einrichtungen, soweit die Europäische Gemeinschaft oder ihre Mitgliedstaaten zu deren Vertragsparteien gehören.

#### § 14  
Verordnungsermächtigung

(1) Das für Inneres zuständige Mitglied der Landesregierung wird ermächtigt, im Einvernehmen mit dem für Umwelt zuständigen Mitglied der Landesregierung Rechtsverordnungen zur Erfüllung der Verpflichtungen der Richtlinie 2007/2/EG mit folgendem Inhalt zu erlassen, soweit nicht unmittelbar geltendes Gemeinschaftsrecht Anwendung findet:

1.  technische und organisatorische Einzelheiten
    
    1.  zu Spezifikationen der Geodaten gemäß § 4 Absatz 1,
        
    2.  zu Spezifikationen der Geodatendienste und weiterer Netzdienste gemäß § 6,
        
    3.  zu Spezifikationen der Metadaten gemäß § 7,
        
    4.  zur Herstellung der Interoperabilität gemäß § 8,
        
    5.  zur Sicherstellung der Berichterstattung des Landes durch die ressortübergreifende Kontaktstelle gemäß § 10 Absatz 1 über die nationale Anlaufstelle an die Europäische Kommission,
        
2.  Einzelheiten zur einheitlichen Gestaltung von Bedingungen betreffend den Zugang zu Geodaten und Geodatendiensten durch Organe und Einrichtungen der Europäischen Gemeinschaft,
    
3.  Geodaten und Geodatendienste, die ohne eine Einzelfallprüfung oder für bestimmte Verwendungszwecke allgemein zugänglich sind.
    

(2) Führt die Regelung der Einzelheiten zu einer Mehrbelastung der Gemeinden und Gemeindeverbände, die sich nicht bereits aus dem Gemeinschaftsrecht ergibt, kann in der Rechtsverordnung auch der finanzielle Ausgleich gemäß Artikel 97 Absatz 3 der Verfassung des Landes Brandenburg geregelt werden.

**Anhang 1**

Geodatenthemen zu § 4 Absatz 1 Nummer 4 entsprechend Anhang I der Richtlinie 2007/2/EG

1.  Koordinatenreferenzsysteme (Systeme zur eindeutigen räumlichen Referenzierung von Geodaten anhand eines Koordinatensatzes oder Angaben zu Breite, Länge und Höhe auf der Grundlage eines geodätischen horizontalen und vertikalen Datums),
    
2.  geographische Gittersysteme (harmonisiertes Gittersystem mit Mehrfachauflösung, gemeinsamem Ursprungspunkt und standardisierter Lokalisierung und Größe der Gitterzellen),
    
3.  geographische Bezeichnungen (Namen von Gebieten, Regionen, Orten, Großstädten, Vororten, Städten oder Siedlungen sowie jedes geographische oder topographische Merkmal von öffentlichem oder historischem Interesse),
    
4.  Verwaltungseinheiten (lokale, regionale und nationale Verwaltungseinheiten, die die Gebiete abgrenzen, in denen die Mitgliedstaaten Hoheitsbefugnisse haben oder ausüben und die durch Verwaltungsgrenzen voneinander getrennt sind),
    
5.  Adressen (Lokalisierung von Grundstücken anhand von Adressdaten, in der Regel Straßenname, Hausnummer und Postleitzahl),
    
6.  Flurstücke oder Grundstücke,
    
7.  Verkehrsnetze (Verkehrsnetze und zugehörige Infrastruktureinrichtungen für Straßen-, Schienen- und Luftverkehr sowie Schifffahrt; dies umfasst auch die Verbindungen zwischen den verschiedenen Netzen und das transeuropäische Verkehrsnetz im Sinne der Entscheidung Nummer 1692/96/EG des Europäischen Parlaments und des Rates vom 23.
    Juli 1996 über gemeinschaftliche Leitlinien für den Aufbau eines transeuropäischen Verkehrsnetzes \[ABl.
    L 228 vom 9.9.1996; L 15 vom 17.1.1997, S.
    1\]),
    
8.  Gewässernetz (Elemente des Gewässernetzes, einschließlich Meeresgebiete und aller sonstigen Wasserkörper und hiermit verbundener Teilsysteme, dar-unter Einzugsgebiete und Teileinzugsgebiete; gegebenenfalls gemäß den Definitionen der Richtlinie 2000/60/EG des Europäischen Parlaments und des Rates vom 23.
    Oktober 2000 zur Schaffung eines Ordnungsrahmens für Maßnahmen der Gemeinschaft im Bereich der Wasserpolitik \[ABl.
    L 327 vom 22.12.2000, S.
    1\], die zuletzt durch die Richtlinie 2008/105/EG \[ABl.
    L 348 vom 24.12.2008, S.
    84\] geändert worden ist),
    
9.  Schutzgebiete (Gebiete, die im Rahmen des internationalen und des gemeinschaftlichen Rechts der Mitgliedstaaten ausgewiesen sind oder verwaltet werden, um spezifische Erhaltungsziele zu erreichen).
    

**Anhang 2**

Geodatenthemen zu § 4 Absatz 1 Nummer 4 entsprechend Anhang II der Richtlinie 2007/2/EG

1.  Höhe (digitale Höhenmodelle für Land-, Eis- und Wasserflächen inklusive Tiefenmessung bei Gewässern und Mächtigkeit bei Eisflächen sowie Uferlinien \[Geländemodelle\]),
    
2.  Bodenbedeckung (physische und biologische Bedeckung der Erdoberfläche, einschließlich künstlicher Flächen, landwirtschaftlicher Flächen, Wälder, natürlicher \[naturnaher\] Gebiete, Feuchtgebiete und Wasserkörper),
    
3.  Orthophotographie (georeferenzierte Bilddaten der Erdoberfläche von satelliten- oder luftfahrzeuggestützten Sensoren),
    
4.  Geologie (geologische Beschreibung anhand von Zusammensetzung und Struktur des Untergrundes; dies umfasst auch Grundgebirgs- und Sedimentgesteine, Lockersedimente, Grundwasserleiter und -stauer und Geomorphologie, Störungen).
    

**Anhang 3**

Geodatenthemen zu § 4 Absatz 1 Nummer 4 entsprechend Anhang III der Richtlinie 2007/2/EG

1.  Statistische Einheiten (Einheiten für die Verbreitung oder Verwendung statistischer Daten),
    
2.  Gebäude (geographischer Standort von Gebäuden),
    
3.  Boden (Beschreibung von Boden und Unterboden anhand von Tiefe, Textur, Struktur und Gehalt an Teilchen sowie organischem Material, Steinigkeit, Erosion, gegebenenfalls durchschnittliches Gefälle und erwartete Wasserspeicherkapazität),
    
4.  Bodennutzung (Beschreibung von Gebieten anhand ihrer derzeitigen und geplanten künftigen Funktion oder ihres sozioökonomischen Zwecks, wie zum Beispiel Wohn-, Industrie- oder Gewerbegebiete, land- oder forstwirtschaftliche Flächen, Freizeitgebiete),
    
5.  Gesundheit und Sicherheit (geographische Verteilung verstärkt auftretender pathologischer Befunde \[z.
    B.
    Allergien, Krebserkrankungen, Erkrankungen der Atemwege\], Informationen über Auswirkungen auf die Gesundheit \[Biomarker, Rückgang der Fruchtbarkeit, Epidemien\] oder auf das Wohlbefinden \[z.
    B.
    Ermüdung, Stress\] der Menschen in unmittelbarem Zusammenhang mit der Umweltqualität \[z.
    B.
    Luftverschmutzung, Chemikalien, Abbau der Ozonschicht, Lärm\] oder in mittelbarem Zusammenhang mit der Umweltqualität \[z.
    B.
    Nahrung, genetisch veränderte Organismen\]),
    
6.  Versorgungswirtschaft und staatliche Dienste (Versorgungseinrichtungen, wie Abwasser- und Abfallentsorgung, Energieversorgung und Wasserversorgung; staatliche Verwaltungs- und Sozialdienste, wie öffentliche Verwaltung, Katastrophenschutz, Schulen und Krankenhäuser),
    
7.  Umweltüberwachung (Standort und Betrieb von Umweltüberwachungseinrichtungen einschließlich Beobachtung und Messung von Schadstoffen, des Zustands von Umweltmedien und anderen Parametern des Ökosystems, wie zum Beispiel Artenvielfalt, ökologischer Zustand der Vegetation durch oder im Auftrag von öffentlichen Behörden),
    
8.  Produktions- und Industrieanlagen (Standorte für industrielle Produktion, einschließlich durch die Richtlinie 2008/1/EG des Europäischen Parlaments und des Rates vom 15.
    Januar 2008 über die integrierte Vermeidung und Verminderung der Umweltverschmutzung \[ABl.
    L 24 vom 29.1.2008, S.
    8\] erfasste Anlagen und Einrichtungen zur Wasserentnahme sowie Bergbau- und Lagerstandorte),
    
9.  landwirtschaftliche Anlagen und Aquakulturanlagen (landwirtschaftliche Anlagen und Produktionsstätten einschließlich Bewässerungssysteme, Gewächshäuser und Ställe),
    
10.  Verteilung der Bevölkerung - Demographie (geographische Verteilung der Bevölkerung, einschließlich Bevölkerungsmerkmale und Tätigkeitsebenen, zusammengefasst nach Gitter, Region, Verwaltungseinheit oder sonstigen analytischen Einheiten),
    
11.  Bewirtschaftungsgebiete / Schutzgebiete / geregelte Gebiete und Berichterstattungseinheiten (auf internationaler, europäischer, nationaler, regionaler und lokaler Ebene bewirtschaftete, geregelte oder zu Zwecken der Berichterstattung herangezogene Gebiete; dazu zählen Deponien, Trinkwasserschutzgebiete, nitratempfindliche Gebiete, geregelte Fahrwasser auf Binnen- und Seewasserstraßen, Gebiete für die Abfallverklappung, Lärmschutzgebiete, für Exploration und Bergbau ausgewiesene Gebiete, Flussgebietseinheiten, entsprechende Berichterstattungseinheiten und Gebiete des Küstenzonenmanagements),
    
12.  Gebiete mit naturbedingten Risiken (gefährdete Gebiete, eingestuft nach naturbedingten Risiken, das betrifft sämtliche atmosphärischen, hydrologischen, seismischen, vulkanischen Phänomene sowie Naturfeuer, die aufgrund ihres örtlichen Auftretens sowie ihrer Schwere und Häufigkeit signifikante Auswirkungen auf die Gesellschaft haben können, zum Beispiel Überschwemmungen, Erdrutsche und Bodensenkungen, Lawinen, Waldbrände, Erdbeben oder Vulkanausbrüche),
    
13.  atmosphärische Bedingungen (physikalische Bedingungen in der Atmosphäre; dazu zählen Geodaten auf der Grundlage von Messungen, Modellen oder einer Kombination aus beiden sowie Angabe der Messstandorte),
    
14.  meteorologisch-geographische Objekte (Witterungsbedingungen und deren Messung, Niederschlag, Temperatur, Gesamtverdunstung \[Evapotranspiration\], Windgeschwindigkeit und Windrichtung),
    
15.  ozeanographisch-geographische Objekte (physikalische Bedingungen der Ozeane, wie zum Beispiel Strömungsverhältnisse, Salinität, Wellenhöhe),
    
16.  Meeresregionen (physikalische Bedingungen von Meeren und salzhaltigen Gewässern, aufgeteilt nach Regionen und Teilregionen mit gemeinsamen Merkmalen),
    
17.  biogeographische Regionen (Gebiete mit relativ homogenen ökologischen Bedingungen und gemeinsamen Merkmalen),
    
18.  Lebensräume und Biotope (geographische Gebiete mit spezifischen ökologischen Bedingungen, Prozessen, Strukturen und lebensunterstützenden Funktionen als physische Grundlage für dort lebende Organismen; dies umfasst auch durch geographische, abiotische und biotische Merkmale gekennzeichnete natürliche oder naturnahe terrestrische und aquatische Gebiete),
    
19.  Verteilung der Arten (geographische Verteilung des Auftretens von Tier- und Pflanzenarten, zusammengefasst in Gittern, Region, Verwaltungseinheit oder sonstigen analytischen Einheiten),
    
20.  Energiequellen (Energiequellen, wie zum Beispiel Kohlenwasserstofflagerstätten, Wasserkraft, Bioenergie, Sonnen- und Windenergie, gegebenenfalls mit Tiefen- beziehungsweise Höhenangaben zur Ausdehnung der Energiequelle),
    
21.  mineralische Bodenschätze (mineralische Rohstofflagerstätten, wie zum Beispiel Metallerze, Industrieminerale, gegebenenfalls mit Tiefen- beziehungsweise Höhenangaben zur Ausdehnung der Lagerstätten).