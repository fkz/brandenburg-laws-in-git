## Gesetz zum Schutz personenbezogener Daten im Land Brandenburg (Brandenburgisches Datenschutzgesetz - BbgDSG)

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeine Bestimmungen

[§ 1 Zweck](#1)

[§ 2 Anwendungsbereich](#2)

[§ 3 Begriffsbestimmung](#3)

[§ 4 Freigabeverfahren und Einsicht in das Verzeichnis der Verarbeitungstätigkeiten](#4)

### Abschnitt 2  
Grundsätze der Verarbeitung personenbezogener Daten

[§ 5 Zulässigkeit der Verarbeitung personenbezogener Daten](#5)

[§ 6 Zulässigkeit der Verarbeitung personenbezogener Daten zu anderen Zwecken](#6)

[§ 7 Erhebung personenbezogener Daten](#7)

[§ 8 Übermittlung personenbezogener Daten](#8)

[§ 9 Löschung personenbezogener Daten](#9)

### Abschnitt 3  
Rechte der betroffenen Person

[§ 10 Informationspflicht bei der Erhebung personenbezogener Daten](#10)

[§ 11 Auskunftsrecht der betroffenen Person und Einsicht in Akten](#11)

[§ 12 Benachrichtigung der von einer Verletzung des Schutzes personenbezogener Daten betroffenen Person](#12)

[§ 13 Widerspruchsrecht](#13)

### Abschnitt 4  
Die oder der Landesbeauftragte für den Datenschutz und  
für das Recht auf Akteneinsicht

[§ 14 Errichtung](#14)

[§ 15 Ernennung und Amtszeit](#15)

[§ 16 Amtsverhältnis](#16)

[§ 17 Rechte und Pflichten](#17)

[§ 18 Aufgaben und Befugnisse, Mitwirkungspflichten](#18)

[§ 19 Kostenerhebung](#19)

[§ 20 Gerichtlicher Rechtsschutz](#20)

[§ 21 Durchführung der Kontrolle](#21)

[§ 22 Mitteilungen an die Fach- oder Rechtsaufsichtsbehörde](#22)

[§ 23 Stellungnahme der Landesregierung zum Tätigkeitsbericht](#23)

### Abschnitt 5  
Besondere Verarbeitungssituationen

[§ 24 Verarbeitung besonderer Kategorien personenbezogener Daten](#24)

#### Unterabschnitt 1  
Besondere Verarbeitungssituationen im Anwendungsbereich  
der Verordnung (EU) 2016/679

[§ 25 Datenverarbeitung für wissenschaftliche und historische Forschungszwecke](#25)

[§ 26 Datenverarbeitung bei Beschäftigungsverhältnissen](#26)

[§ 27 Verarbeitung personenbezogener Daten zu Zwecken der parlamentarischen Kontrolle](#27)

[§ 28 Videoüberwachung öffentlich zugänglicher Räume](#28)

[§ 29 Verarbeitung personenbezogener Daten zu Zwecken der freien Meinungsäußerung und der Informationsfreiheit](#29)

#### Unterabschnitt 2  
Besondere Verarbeitungssituationen außerhalb des Anwendungsbereichs  
der Verordnung (EU) 2016/679

[§ 30 Öffentliche Auszeichnungen und Ehrungen](#30)

[§ 31 Begnadigungsverfahren](#31)

### Abschnitt 6  
Sanktionen, Einschränkung von Grundrechten

[§ 32 Ordnungswidrigkeiten](#32)

[§ 33 Strafvorschrift](#33)

[§ 34 Einschränkung eines Grundrechts](#34)

[§ 35 Übergangsvorschrift](#35)

### Abschnitt 1  
Allgemeine Bestimmungen

#### § 1 Zweck

(1) Dieses Gesetz trifft die zur Durchführung der Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates von 27.
April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr und zur Aufhebung der Richtlinie 95/46/EG (Datenschutz-Grundverordnung) (ABl. L 119 vom 4.5.2016, S. 1; L 314 vom 22.11.2016, S. 72) notwendigen ergänzenden Regelungen.
Gleichzeitig regelt es in den Grenzen der Verordnung spezifische Anforderungen an die Verarbeitung personenbezogener Daten.

(2) Dieses Gesetz dient neben den zur Umsetzung der Richtlinie (EU) 2016/680 des Europäischen Parlaments und des Rates vom 27.
April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten durch die zuständigen Behörden zum Zwecke der Verhütung, Ermittlung, Aufdeckung oder Verfolgung von Straftaten oder der Strafvollstreckung sowie zum freien Datenverkehr und zur Aufhebung des Rahmenbeschlusses 2008/977/JI des Rates (ABl.
L 119 vom 4.5.2016, S. 89) erlassenen Rechtsvorschriften auch der Umsetzung dieser Richtlinie.

#### § 2 Anwendungsbereich

(1) Dieses Gesetz gilt für die Behörden, Einrichtungen und sonstigen öffentlichen Stellen des Landes, die Gemeinden und Gemeindeverbände sowie für die sonstigen der Aufsicht des Landes oder der Gemeinden oder Gemeindeverbände unterstehenden juristischen Personen des öffentlichen Rechts und deren Vereinigungen (öffentliche Stellen), soweit diese personenbezogene Daten verarbeiten.
Für die Gerichte sowie für die Behörden der Staatsanwaltschaft gilt dieses Gesetz, soweit sie Verwaltungsaufgaben wahrnehmen; darüber hinaus gelten für die Behörden der Staatsanwaltschaft, soweit sie keine Verwaltungsaufgaben wahrnehmen, nur die Vorschriften des Abschnitts 4.
Nimmt eine nicht-öffentliche Stelle hoheitliche Aufgaben einer öffentlichen Stelle des Landes wahr, ist sie insoweit öffentliche Stelle im Sinne dieses Gesetzes.

(2) Der Landtag, seine Gremien, seine Mitglieder, die Fraktionen und Gruppen sowie deren Verwaltungen und deren Beschäftigte unterliegen nicht den Bestimmungen dieses Gesetzes, soweit sie zur Wahrnehmung parlamentarischer Aufgaben personenbezogene Daten verarbeiten.
Der Landtag erlässt insoweit unter Berücksichtigung seiner verfassungsrechtlichen Stellung, der Grundsätze der Verordnung (EU) 2016/679 und dieses Gesetzes eine Datenschutzordnung.

(3) Dieses Gesetz findet keine Anwendung, soweit öffentliche Stellen nach Absatz 1 am Wettbewerb teilnehmen und personenbezogene Daten zu wirtschaftlichen Zwecken oder Zielen verarbeiten.
Für diese Stellen gelten insoweit die auf nicht-öffentliche Stellen anzuwendenden Vorschriften.

(4) Öffentliche Stellen des Landes, die als öffentlich-rechtliche Unternehmen am Wettbewerb teilnehmen, gelten als nicht-öffentliche Stellen.

(5) Die Vorschriften der Verordnung (EU) 2016/679 in Verbindung mit diesem Gesetz gehen denen des Verwaltungsverfahrensgesetzes für das Land Brandenburg vor, soweit personenbezogene Daten verarbeitet werden.

(6) Soweit die Verarbeitung personenbezogener Daten durch öffentliche Stellen nicht in den sachlichen Anwendungsbereich der Verordnung (EU) 2016/679 fällt, sind deren Vorschriften und die Vorschriften dieses Gesetzes entsprechend anzuwenden, es sei denn, dieses Gesetz oder andere spezielle Rechtsvorschriften enthalten abweichende Regelungen.
Die Artikel 30, 35 und 36 der Verordnung (EU) 2016/679 gelten nur, soweit die Verarbeitung automatisiert erfolgt oder die Daten in einem Dateisystem gespeichert sind oder gespeichert werden sollen.

#### § 3 Begriffsbestimmung

Ergänzend zu Artikel 4 der Verordnung (EU) 2016/679 bezeichnet der Ausdruck „Anonymisieren“ das Verändern personenbezogener Daten dergestalt, dass die Einzelangaben über persönliche oder sachliche Verhältnisse nicht mehr oder nur mit einem unverhältnismäßigen Aufwand an Zeit, Kosten und Arbeitskraft einer bestimmten oder bestimmbaren natürlichen Person zugeordnet werden können.

#### § 4 Freigabeverfahren und Einsicht in das Verzeichnis der Verarbeitungstätigkeiten

(1) Jede mittels automatisierter Verfahren vorgesehene Verarbeitung personenbezogener Daten bedarf vor ihrem Beginn oder vor einer wesentlichen Änderung der schriftlichen Freigabe.
In der Freigabeerklärung ist zu bestätigen, dass

1.  die Verarbeitung im Einklang mit den Artikeln 5 und 6 der Verordnung (EU) 2016/679 erfolgt,
2.  ein aus einer Risikoanalyse und unter Berücksichtigung der Vorgaben von Artikel 32 der Verordnung (EU) 2016/679 entwickeltes Sicherheitskonzept ergeben hat, dass geeignete technische und organisatorische Maßnahmen getroffen sind, um ein dem Risiko für die Rechte und Freiheiten der betroffenen Personen angemessenes Schutzniveau zu gewährleisten, und
3.  für die Verfahren, von denen voraussichtlich ein hohes Risiko für die Rechte und Freiheiten der betroffenen Personen ausgeht, eine Datenschutz-Folgenabschätzung gemäß Artikel 35 der Verordnung (EU) 2016/679 erfolgt ist.

Die Freigabe erfolgt durch den Verantwortlichen.
Bei gemeinsamen Verfahren kann die Zuständigkeit für die Freigabe entsprechend Artikel 26 Absatz 1 der Verordnung (EU) 2016/679 vereinbart werden.
Die Freigabeerklärung ist dem Verzeichnis nach Artikel 30 der Verordnung (EU) 2016/679 beizufügen.

(2) Absatz 1 Satz 1 gilt nicht für

1.  Verfahren, deren einziger Zweck das Führen eines Registers ist, das zur Information der Öffentlichkeit bestimmt ist oder allen Personen, die ein berechtigtes Interesse nachweisen können, zur Einsichtnahme offensteht,
2.  Verfahren, soweit mit ihnen Datensammlungen erstellt werden, die nur vorübergehend vorgehalten und innerhalb von drei Monaten nach ihrer Erstellung gelöscht werden,
3.  Verfahren, die unter Einsatz handelsüblicher Schreibprogramme ablaufen,
4.  Verfahren, die ausschließlich der Datensicherung und Datenschutzkontrolle dienen,
5.  Verfahren, die ausschließlich dem Auffinden von Vorgängen, Anträgen oder Akten dienen (Registraturverfahren),
6.  Verfahren, die ausschließlich zur Überwachung von Terminen und Fristen dienen,
7.  Zimmer-, Inventar- und Softwareverzeichnisse,
8.  Bibliothekskataloge und Fundstellenverzeichnisse oder
9.  Anschriftenverzeichnisse, die ausschließlich für die Versendung von Informationen an betroffene Personen genutzt werden.

(3) Das Verzeichnis nach Artikel 30 der Verordnung (EU) 2016/679 einschließlich der Freigabeerklärung nach Absatz 1 kann von jedermann unentgeltlich eingesehen werden.
Dies gilt nicht für Angaben nach Artikel 30 Absatz 1 Satz 2 Buchstabe g und Absatz 2 Buchstabe d der Verordnung (EU) 2016/679, soweit hierdurch die Sicherheit des Verfahrens beeinträchtigt würde.
Satz 1 gilt nicht für

1.  Verfahren der Verfassungsschutzbehörde,
2.  Verfahren, die der Gefahrenabwehr oder der Strafverfolgung dienen, und
3.  Verfahren der Steuerfahndung,

soweit die verantwortliche Stelle eine Einsichtnahme im Einzelfall mit der Erfüllung ihrer Aufgaben für unvereinbar erklärt.

### Abschnitt 2  
Grundsätze der Verarbeitung personenbezogener Daten

#### § 5 Zulässigkeit der Verarbeitung personenbezogener Daten

(1) Die Verarbeitung personenbezogener Daten durch öffentliche Stellen ist zulässig, wenn sie zur Erfüllung der in der Zuständigkeit des Verantwortlichen liegenden Aufgabe oder in Ausübung öffentlicher Gewalt, die dem Verantwortlichen übertragen wurde, erforderlich ist.

(2) Personenbezogene Daten dürfen auch zu Zwecken der Wahrnehmung von Aufsichts- und Kontrollbefugnissen, der Rechnungsprüfung oder der Durchführung von Organisationsuntersuchungen verarbeitet werden.
Die Verarbeitung personenbezogener Daten ist nur zulässig, soweit sie für die Ausübung dieser Befugnisse erforderlich ist.
Zu Aus- und Fortbildungszwecken dürfen personenbezogene Daten nur verwendet werden, wenn dies unerlässlich ist und schutzwürdige Belange der betroffenen Person dem nicht entgegenstehen; zu Test- und Prüfungszwecken dürfen personenbezogene Daten nicht verarbeitet werden.
Die in den Sätzen 1 und 3 genannten Zwecke sind Erhebungszwecke im Sinne von Artikel 5 Absatz 1 Buchstabe b der Verordnung (EU) 2016/679.

(3) Absatz 2 gilt entsprechend für personenbezogene Daten im Sinne von Artikel 9 Absatz 1 der Verordnung (EU) 2016/679 mit der Maßgabe, dass eine Verarbeitung dieser Daten nur zulässig ist, wenn dies für die Verarbeitungszwecke unerlässlich ist und, sofern eine Verarbeitung zu Zwecken der Aus- und Fortbildung erfolgt, wenn diese durch Personen gemäß § 203 Absatz 3 Satz 1 des Strafgesetzbuches vorgenommen wird.

#### § 6 Zulässigkeit der Verarbeitung personenbezogener Daten zu anderen Zwecken

(1) Eine Verarbeitung personenbezogener Daten zu einem anderen Zweck, als zu demjenigen, zu dem die Daten erhoben wurden, ist im Rahmen der Aufgabenerfüllung des Verantwortlichen zulässig, wenn

1.  es zur Abwehr erheblicher Nachteile für das Gemeinwohl oder einer sonst unmittelbar drohenden Gefahr für die öffentliche Sicherheit, die Verteidigung oder die nationale Sicherheit, zur Wahrung erheblicher Belange des Gemeinwohls oder zur Sicherung des Steuer- und Zollaufkommens erforderlich ist,
2.  es zur Abwehr einer schwerwiegenden Beeinträchtigung der Rechte und Freiheiten einer anderen Person erforderlich ist,
3.  sich bei der rechtmäßigen Aufgabenerfüllung Anhaltspunkte für Straftaten oder Ordnungswidrigkeiten ergeben und die Unterrichtung der für die Verfolgung oder Vollstreckung zuständigen Behörden geboten erscheint,
4.  es erforderlich ist, Angaben der betroffenen Person zu überprüfen, weil tatsächliche Anhaltspunkte für deren Unrichtigkeit bestehen,
5.  sie im öffentlichen Interesse liegt oder zur Wahrung berechtigter Interessen einer dritten Person erforderlich ist und die betroffene Person in diesen Fällen der Datenverarbeitung nicht widersprochen hat,
6.  die Einholung der Einwilligung der betroffenen Person nicht möglich ist oder mit unverhältnismäßig hohem Aufwand verbunden wäre, aber offensichtlich ist, dass die Datenverarbeitung zum Schutz der Rechte dieser Person erforderlich ist und sie in Kenntnis des anderen Zweckes ihre Einwilligung erteilen würde,
7.  die Bearbeitung eines von der betroffenen Person gestellten Antrages ohne die Zweckänderung der Daten nicht möglich ist oder
8.  die Daten aus allgemein zugänglichen Quellen erhoben werden können oder die verantwortliche Stelle sie veröffentlichen dürfte, es sei denn, dass das Interesse der betroffenen Person an dem Ausschluss der Speicherung oder einer Veröffentlichung der gespeicherten Daten offensichtlich überwiegt.

Die Verarbeitung besonderer Kategorien personenbezogener Daten im Sinne von Artikel 9 Absatz 1 der Verordnung (EU) 2016/679 zu einem anderen Zweck als zu demjenigen, zu dem die Daten erhoben wurden, ist nur zu den in Satz 1 Nummer 1 und 3 genannten Zwecken und nur insoweit zulässig, als sie für die Erreichung dieser Zwecke unerlässlich ist.

(2) Eine Information der betroffenen Person über die Datenverarbeitung nach Absatz 1 Nummer 1 bis 4 erfolgt nicht, soweit und solange hierdurch der Zweck der Verarbeitung gefährdet würde.

(3) Unterliegen die personenbezogenen Daten einem Berufsgeheimnis und sind sie der verantwortlichen Stelle von der zur Verschwiegenheit verpflichteten Person in Ausübung ihrer Berufspflicht übermittelt worden, findet Absatz 1 keine Anwendung.

(4) Die Übermittlung personenbezogener Daten auf der Grundlage von Absatz 1 an nicht-öffentliche Stellen ist nur zulässig, wenn sich die nicht-öffentliche Stelle verpflichtet hat, die Daten nur für den Zweck zu verarbeiten, zu dessen Erfüllung sie ihr übermittelt werden sollen.

(5) Sind mit personenbezogenen Daten weitere Daten der betroffenen Person oder Dritter derart verbunden, dass ihre Trennung nach erforderlichen und nicht erforderlichen Daten nicht oder nur mit unverhältnismäßigem Aufwand möglich ist, so sind auch die Kenntnisnahme, die Weitergabe innerhalb des Verantwortlichen und die Übermittlung der Daten, die nicht zur Erfüllung der jeweiligen Aufgabe erforderlich sind, zulässig, soweit nicht schutzwürdige Belange der betroffenen Person oder Dritter überwiegen.
Die nicht erforderlichen Daten unterliegen insoweit einem Verarbeitungsverbot.

#### § 7 Erhebung personenbezogener Daten

Werden personenbezogene Daten bei einer dritten Person oder bei einer nicht-öffentlichen Stelle erhoben, sind diese auf Verlangen über den Erhebungszweck zu unterrichten, soweit dadurch schutzwürdige Interessen der betroffenen Person nicht beeinträchtigt werden.
Werden die Daten aufgrund einer Rechtsvorschrift erhoben, die zur Auskunft verpflichtet, ist auf die Auskunftspflicht, sonst auf die Freiwilligkeit der Angaben hinzuweisen.

#### § 8 Übermittlung personenbezogener Daten

Die Verantwortung für die Zulässigkeit der Übermittlung personenbezogener Daten trägt die übermittelnde Stelle.
Erfolgt die Übermittlung aufgrund eines Ersuchens einer öffentlichen Stelle, trägt diese die Verantwortung.
Die übermittelnde Stelle hat dann lediglich zu prüfen, ob das Übermittlungsersuchen im Rahmen der Aufgaben der ersuchenden Stelle liegt.
Die Rechtmäßigkeit des Ersuchens prüft sie nur, wenn im Einzelfall hierzu Anlass besteht.
Die ersuchende Stelle hat in dem Ersuchen die für diese Prüfung erforderlichen Angaben zu machen.
Erfolgt die Übermittlung durch automatisierten Abruf, trägt die Verantwortung für die Rechtmäßigkeit des Abrufes die abrufende Stelle.

#### § 9 Löschung personenbezogener Daten

Soweit öffentliche Stellen nach einer Rechtsvorschrift verpflichtet sind, Unterlagen einem öffentlichen Archiv zur Übernahme anzubieten, ist eine Löschung oder Vernichtung personenbezogener Daten erst zulässig, nachdem die Unterlagen dem öffentlichen Archiv angeboten und von diesem nicht als archivwürdig übernommen worden sind.
Dies gilt auch, wenn das Archiv nicht innerhalb einer durch Rechtsvorschrift bestimmten Frist über die Übernahme entschieden hat.

### Abschnitt 3  
Rechte der betroffenen Person

#### § 10 Informationspflicht bei der Erhebung personenbezogener Daten

(1) Die Pflicht zur Information der betroffenen Person gemäß Artikel 13 oder Artikel 14 Absatz 1, 2 und 4 der Verordnung (EU) 2016/679 erfolgt ergänzend zu den in Artikel 13 Absatz 4 oder Artikel 14 Absatz 5 der Verordnung (EU) 2016/679 genannten Ausnahmen nicht, soweit und solange

1.  die Information die öffentliche Sicherheit gefährden oder sonst dem Wohle des Bundes oder eines Landes Nachteile bereiten würde,
2.  die Information die Verfolgung von Straftaten oder Ordnungswidrigkeiten gefährden würde,
3.  die Information die Geltendmachung, Ausübung oder Verteidigung zivilrechtlicher Ansprüche beeinträchtigen würde und die Interessen des Verantwortlichen an der Nichterteilung der Information die Interessen der betroffenen Person überwiegen oder
4.  die Tatsache der Verarbeitung nach einer Rechtsvorschrift oder wegen der überwiegenden Rechte und Freiheiten anderer Personen geheim zu halten ist.

(2) Unterbleibt eine Information der betroffenen Person nach Maßgabe des Absatzes 1, ergreift der Verantwortliche geeignete Maßnahmen zum Schutz der berechtigten Interessen der betroffenen Person, einschließlich der Bereitstellung der in Artikel 13 Absatz 1 und 2 oder Artikel 14 Absatz 1 und 2 der Verordnung (EU) 2016/679 genannten Informationen für die Öffentlichkeit in präziser, transparenter, verständlicher und leicht zugänglicher Form in einer klaren und einfachen Sprache.
Der Verantwortliche hält schriftlich oder elektronisch fest, aus welchen Gründen er von einer Information abgesehen hat.

#### § 11 Auskunftsrecht der betroffenen Person und Einsicht in Akten

(1) Das Recht der betroffenen Person auf Auskunft gemäß Artikel 15 der Verordnung (EU) 2016/679 besteht nicht, soweit und solange

1.  die Auskunft die öffentliche Sicherheit, die nationale Sicherheit oder die Landesverteidigung gefährden würde,
2.  die Information die Verfolgung von Straftaten oder Ordnungswidrigkeiten gefährden würde oder
3.  die personenbezogenen Daten oder die Tatsache ihrer Verarbeitung nach einer Rechtsvorschrift oder wegen der Rechte und Freiheiten anderer Personen geheim zu halten sind und deswegen das Interesse der betroffenen Person an der Information zurücktreten muss.

Die betroffene Person kann keine Auskunft über personenbezogene Daten verlangen, die ausschließlich zu Zwecken der Datensicherung oder der Datenschutzkontrolle gespeichert sind und deren Verarbeitung zu anderen Zwecken durch geeignete technische und organisatorische Maßnahmen ausgeschlossen ist.

(2) Sind die Daten in Akten enthalten, so kann der betroffenen Person anstelle der Erteilung einer Auskunft auch Akteneinsicht gewährt werden.

(3) Bezieht sich die Auskunftserteilung oder die Akteneinsicht auf die Herkunft personenbezogener Daten von Stellen des Verfassungsschutzes, der Gerichte, der Staatsanwaltschaft und der Polizei oder von Landesfinanzbehörden, soweit diese personenbezogene Daten für Zwecke der Verhütung, Ermittlung, Aufdeckung oder Verfolgung von Straftaten oder zu Zwecken der Strafvollstreckung speichern, sowie vom Bundesnachrichtendienst, des Amtes für den Militärischen Abschirmdienst und, soweit die Sicherheit des Bundes berührt wird, von anderen Behörden im Geschäftsbereich des für Verteidigung zuständigen Bundesministeriums, ist sie nur mit Zustimmung dieser Stellen zulässig.
Gleiches gilt für die Erteilung einer Auskunft, die sich auf die Übermittlung personenbezogener Daten an diese Stellen bezieht.
Die Zustimmung darf nur versagt werden, wenn dies notwendig ist zum Schutz der in Artikel 23 Absatz 1 Buchstabe a, b, c, d oder Buchstabe e der Verordnung (EU) 2016/679 genannten Rechtsgüter.

(4) Die Ablehnung der Auskunft bedarf keiner Begründung, soweit durch die Begründung der Zweck der Ablehnung gefährdet würde.
In diesem Falle sind die wesentlichen Gründe der Entscheidung aufzuzeichnen.
Die betroffene Person ist auf die Möglichkeit der Beschwerde bei der oder dem Landesbeauftragten hinzuweisen.

(5) Wird der betroffenen Person keine Auskunft erteilt, so ist sie auf Verlangen der betroffenen Person der oder dem Landesbeauftragten zu erteilen, soweit nicht die jeweils zuständige oberste Landesbehörde im Einzelfall feststellt, dass dadurch die Sicherheit des Bundes oder eines Landes gefährdet würde.
Die Mitteilung der oder des Landesbeauftragten an die betroffene Person darf keine Rückschlüsse auf den Erkenntnisstand der verantwortlichen Stelle zulassen, sofern diese nicht einer weitergehenden Auskunft zugestimmt hat.

#### § 12 Benachrichtigung der von einer Verletzung des Schutzes<br>personenbezogener Daten betroffenen Person

Der Verantwortliche kann von der Benachrichtigung gemäß Artikel 34 der Verordnung (EU) 2016/679 der von einer Verletzung des Schutzes personenbezogener Daten betroffenen Person ergänzend zu den in Artikel 34 Absatz 3 der Verordnung (EU) 2016/679 genannten Ausnahmen absehen, soweit und solange

1.  die Information die öffentliche Sicherheit gefährden oder sonst dem Wohle des Bundes oder eines Landes Nachteile bereiten würde,
2.  die personenbezogenen Daten oder die Tatsache ihrer Verarbeitung nach einer Rechtsvorschrift oder wegen der Rechte und Freiheiten anderer Personen geheim zu halten sind oder
3.  die Information die Sicherheit von IT-Systemen gefährden würde.

#### § 13 Widerspruchsrecht

Das Recht auf Widerspruch gemäß Artikel 21 Absatz 1 der Verordnung (EU) 2016/679 besteht nicht, soweit an der Verarbeitung ein zwingendes öffentliches Interesse besteht, das die Interessen der betroffenen Person überwiegt, oder eine Rechtsvorschrift zur Verarbeitung verpflichtet.

### Abschnitt 4  
Die oder der Landesbeauftragte für den Datenschutz und für das Recht auf Akteneinsicht

#### § 14 Errichtung

(1) Das Amt der oder des Landesbeauftragten für den Datenschutz und für das Recht auf Akteneinsicht (der oder die Landesbeauftragte) wird bei der Präsidentin oder dem Präsidenten des Landtages Brandenburg errichtet.
Die oder der Landesbeauftragte führt die Amts- und Funktionsbezeichnung „Die Landesbeauftragte für den Datenschutz und für das Recht auf Akteneinsicht“ oder „Der Landesbeauftragte für den Datenschutz und für das Recht auf Akteneinsicht“.

(2) Für die Erfüllung der Aufgaben ist die notwendige Personal- und Sachausstattung zur Verfügung zu stellen.
Die Mittel sind im Einzelplan des Landtages in einem gesonderten Kapitel auszuweisen.
§ 28 Absatz 3, § 29 Absatz 3 und § 41 Absatz 1 der Landeshaushaltsordnung gelten entsprechend.

(3) Die Beamtinnen und Beamten bei der oder dem Landesbeauftragten werden auf deren oder dessen Vorschlag durch die Präsidentin oder den Präsidenten des Landtages ernannt.
Dienstvorgesetzte oder Dienstvorgesetzter der Beamtinnen und Beamten ist die oder der Landesbeauftragte, an deren oder dessen Weisungen sie ausschließlich gebunden sind.
Die oder der Landesbeauftragte übt für die bei ihr oder ihm tätigen Beamtinnen und Beamten die Aufgaben und Befugnisse der obersten Dienstbehörde aus.
Die Arbeitnehmerinnen und Arbeitnehmer werden von der oder dem Landesbeauftragten eingestellt und entlassen.

(4) Die oder der Landesbeauftragte bestellt aus dem Kreis der Beschäftigten eine Person, die die Stellvertretung wahrnimmt.
Diese führt die Geschäfte, wenn die oder der Landesbeauftragte an der Ausübung des Amtes verhindert ist oder wenn das Amtsverhältnis endet und er oder sie nicht zur Weiterführung der Geschäfte verpflichtet ist.

(5) Die oder der Landesbeauftragte kann Aufgaben der Personalverwaltung und Personalwirtschaft auf andere Stellen des Landes übertragen, soweit hierdurch die Unabhängigkeit der oder des Landesbeauftragten nicht beeinträchtigt wird.
Diesen Stellen dürfen personenbezogene Daten der Beschäftigten übermittelt werden, soweit deren Kenntnis zur Erfüllung der übertragenen Aufgaben erforderlich ist.

#### § 15 Ernennung und Amtszeit

(1) Der Landtag wählt die Landesbeauftragte oder den Landesbeauftragten mit mehr als der Hälfte der gesetzlichen Zahl seiner Mitglieder.
Die oder der Gewählte ist von der Präsidentin oder dem Präsidenten des Landtages zu ernennen.
Die oder der Landesbeauftragte muss die Befähigung zum Richteramt oder zum höheren Verwaltungsdienst oder eine nach dem Einigungsvertrag gleichgestellte Befähigung haben und die zur Erfüllung der Aufgaben erforderliche Qualifikation, Erfahrung und Sachkunde, insbesondere im Bereich des Schutzes personenbezogener Daten, besitzen.

(2) Die oder der Landesbeauftragte leistet vor der Präsidentin oder dem Präsidenten des Landtages folgenden Eid:

„Ich schwöre, mein Amt gerecht und unparteiisch getreu dem Grundgesetz, der Verfassung von Brandenburg und den Gesetzen zu führen und meine ganze Kraft dafür einzusetzen.“

Der Eid kann auch mit einer religiösen Beteuerung geleistet werden.

(3) Die Amtszeit der oder des Landesbeauftragten beträgt sechs Jahre.
Die Wiederwahl ist zulässig.
Die Amtszeit verlängert sich bis zur Bestellung einer Nachfolgerin oder eines Nachfolgers, längstens jedoch um sechs Monate.

#### § 16 Amtsverhältnis

(1) Die oder der Landesbeauftragte ist in der Ausübung des Amtes unabhängig und nur dem Gesetz unterworfen.
Eine Dienstaufsicht erfolgt nur nach Maßgabe von Absatz 3 und § 17 Absatz 2.
Sie darf die Unabhängigkeit des Amtes nicht berühren.

(2) Die oder der Landesbeauftragte steht zum Land Brandenburg nach Maßgabe dieses Gesetzes in einem öffentlich-rechtlichen Amtsverhältnis.

(3) Das Amtsverhältnis beginnt mit Aushändigung der Ernennungsurkunde.
Es endet mit Ablauf der Amtszeit, der Entlassung auf eigenen Antrag oder durch eine Amtsenthebung.
Stellt der Landtag durch Beschluss mit einer Mehrheit von zwei Dritteln seiner gesetzlichen Mitglieder fest, dass die oder der Landesbeauftragte eine schwere Verfehlung im Sinne von Artikel 53 Absatz 4 der Verordnung (EU) 2016/679 begangen hat, enthebt die Präsidentin des Landtages oder der Präsident des Landtages die Landesbeauftragte oder den Landesbeauftragten des Amtes.
Erfüllt die oder der Landesbeauftragte nicht mehr die Voraussetzungen für die Wahrnehmung der Aufgaben, enthebt die Landtagspräsidentin oder der Landtagspräsident sie oder ihn des Amtes.
Eine Entlassung auf eigenen Antrag und die Amtsenthebung werden mit Aushändigung der Entlassungsurkunde durch die Präsidentin oder den Präsidenten des Landtages wirksam.

(4) Die oder der Landesbeauftragte erhält Fürsorge und Schutz wie eine Beamtin oder ein Beamter der Besoldungsgruppe B 4 des Brandenburgischen Besoldungsgesetzes im Beamtenverhältnis auf Zeit, insbesondere Besoldung, Versorgung, Erholungsurlaub und Beihilfe im Krankheitsfall.

#### § 17 Rechte und Pflichten

(1) Die oder der Landesbeauftragte sieht von allen mit den Aufgaben des Amtes nicht zu vereinbarenden Handlungen ab und übt während der Amtszeit keine andere mit dem Amt nicht zu vereinbarende entgeltliche oder unentgeltliche Tätigkeit aus.
Insbesondere darf die oder der Landesbeauftragte neben dem Amt kein anderes besoldetes Amt, kein Gewerbe und keinen Beruf ausüben und weder der Leitung oder dem Aufsichtsrat oder Verwaltungsrat eines auf Erwerb gerichteten Unternehmens noch einer Regierung oder einer gesetzgebenden Körperschaft des Bundes oder eines Landes angehören.
Sie oder er darf nicht gegen Entgelt außergerichtliche Gutachten abgeben.
Wird eine Beamtin oder ein Beamter des Landes Brandenburg zur oder zum Landesbeauftragten ernannt, gilt § 4 des Brandenburgischen Ministergesetzes entsprechend.

(2) Die oder der Landesbeauftragte hat der Präsidentin oder dem Präsidenten des Landtages Mitteilung über Geschenke zu machen, die sie oder er in Bezug auf das Amt erhält.
Die Präsidentin oder der Präsident des Landtages entscheidet über die Verwendung der Geschenke.
Sie oder er kann Verfahrensvorschriften erlassen.

(3) Die oder der Landesbeauftragte ist, auch nach Beendigung des Amtsverhältnisses, verpflichtet, über die ihr oder ihm amtlich bekannt gewordenen Angelegenheiten Verschwiegenheit zu bewahren.
Dies gilt nicht für Mitteilungen im dienstlichen Verkehr oder über Tatsachen, die offenkundig sind oder ihrer Bedeutung nach keiner Geheimhaltung bedürfen.
Die oder der Landesbeauftragte entscheidet nach pflichtgemäßem Ermessen, ob und inwieweit sie oder er über solche Angelegenheiten vor Gericht oder außergerichtlich aussagt oder Erklärungen abgibt; wenn sie oder er nicht mehr im Amt ist, ist die Genehmigung der oder des amtierenden Landesbeauftragten erforderlich.
Unberührt bleibt die gesetzlich begründete Pflicht, Straftaten anzuzeigen und bei Gefährdung der freiheitlichen demokratischen Grundordnung für deren Erhaltung einzutreten.

(4) Die oder der Landesbeauftragte darf als Zeugin oder Zeuge aussagen, es sei denn, die Aussage würde

1.  dem Wohl des Bundes oder eines Landes Nachteile bereiten, insbesondere Nachteile für die Sicherheit der Bundesrepublik Deutschland oder ihre Beziehungen zu anderen Staaten, oder
2.  Grundrechte verletzen.

§ 24 des Verfassungsgerichtsgesetzes Brandenburg bleibt unberührt.

#### § 18 Aufgaben und Befugnisse, Mitwirkungspflichten

(1) Die oder der Landesbeauftragte ist zuständige Aufsichtsbehörde im Sinne von Artikel 51 Absatz 1 der Verordnung (EU) 2016/679 und Artikel 41 Absatz 1 der Richtlinie (EU) 2016/680 im Land Brandenburg, soweit der Anwendungsbereich dieses Gesetzes eröffnet ist.
Ihr oder ihm obliegt auch die Überwachung der Einhaltung der datenschutzrechtlichen Vorschriften, wenn die Datenverarbeitung weder der Verordnung (EU) 2016/679 noch der Richtlinie (EU) 2016/680 unterliegt.

(2) Soweit nicht dieses Gesetz oder andere Rechtsvorschriften abweichende Regelungen enthalten, bestimmen sich die Aufgaben und Befugnisse der Aufsichtsbehörde nach den Artikeln 57 bis 59 der Verordnung (EU) 2016/679.

(3) Sie oder er ist auch Aufsichtsbehörde nach § 40 des Bundesdatenschutzgesetzes für die Datenverarbeitung nicht-öffentlicher Stellen im Land Brandenburg.

(4) Die oder der Landesbeauftragte ist im Rahmen der ihr oder ihm durch die Absätze 1 und 3 zugewiesenen Aufgaben zuständig für die Verfolgung und Ahndung von Ordnungswidrigkeiten.

(5) Vor dem Erlass von Rechts- und Verwaltungsvorschriften, die die Verarbeitung personenbezogener Daten betreffen, ist die oder der Landesbeauftragte zu hören.
Sie oder er ist über Planungen des Landes zum Aufbau oder zur wesentlichen Änderung von Systemen zur automatisierten Verarbeitung personenbezogener Daten rechtzeitig zu unterrichten, sofern in den Systemen personenbezogene Daten verarbeitet werden.
Die oder der Landesbeauftragte kann Empfehlungen zur Verbesserung des Datenschutzes geben.
Insbesondere kann sie oder er die Landesregierung und einzelne Mitglieder der Landesregierung, die Gemeinden und Gemeindeverbände sowie die übrigen öffentlichen Stellen in Fragen des Datenschutzes beraten.

(6) Zur Erfüllung ihrer oder seiner Aufgaben kann die oder der Landesbeauftragte zu allen Fragen, die im Zusammenhang mit dem Schutz personenbezogener Daten stehen, von sich aus oder auf Anfrage Stellungnahmen an den Landtag, die Landesregierung, sonstige Einrichtungen und Stellen sowie an die Öffentlichkeit richten.

(7) Der Landtag, seine Ausschüsse oder die Landesregierung können die Landesbeauftragte oder den Landesbeauftragten ersuchen, Hinweisen auf Angelegenheiten und Vorgänge, die ihren oder seinen Aufgabenbereich unmittelbar betreffen, nachzugehen.

#### § 19 Kostenerhebung

(1) Die oder der Landesbeauftragte kann unbeschadet des Artikels 57 Absatz 3 der Verordnung (EU) 2016/679 für öffentliche Leistungen nach der Verordnung (EU) 2016/679 und dem Bundesdatenschutzgesetz Gebühren und Auslagen (Kosten) erheben.
Gläubiger der Kosten ist das Land Brandenburg.
Die oder der Landesbeauftragte ist Behörde im Sinne des Verwaltungsvollstreckungsgesetzes für das Land Brandenburg.

(2) Das für den Datenschutz zuständige Mitglied der Landesregierung wird ermächtigt, die gebührenpflichtigen öffentlichen Leistungen und Gebührensätze auf Vorschlag der oder des Landesbeauftragten durch Rechtsverordnung festzulegen.
Für öffentliche Leistungen nach der Verordnung (EU) 2016/679 und dem Bundesdatenschutzgesetz, für die in der Gebührenordnung keine Tarifstelle vorhanden ist, kann eine Verwaltungsgebühr bis zu 500 Euro erhoben werden, sofern die Leistung nicht ausschließlich in besonderem öffentlichen Interesse liegt.

(3) Die Gebührenordnung kann von der Gebührenpflicht absehen, wenn an der Erbringung der öffentlichen Leistung ein besonderes öffentliches Interesse besteht.
Insbesondere kann die Gebührenordnung vorsehen, dass bei öffentlichen Leistungen an steuerbegünstigte Einrichtungen von der Kostenerhebung abgesehen wird.

(4) § 2 Absatz 1 und die §§ 8 bis 10, 12 sowie 14 bis 25 des Gebührengesetzes für das Land Brandenburg gelten entsprechend.

#### § 20 Gerichtlicher Rechtsschutz

Für Streitigkeiten zwischen einer öffentlichen Stelle oder natürlichen Person und der oder dem Landesbeauftragten über Rechte gemäß § 2 Absatz 6 in Verbindung mit Artikel 78 Absatz 1 und 2 der Verordnung (EU) 2016/679 gilt § 20 des Bundesdatenschutzgesetzes entsprechend.

#### § 21 Durchführung der Kontrolle

(1) Die öffentlichen Stellen sowie deren Auftragsverarbeiterin oder Auftragsverarbeiter im Anwendungsbereich dieses Gesetzes sind verpflichtet, die Landesbeauftragte oder den Landesbeauftragten und ihre oder seine Beauftragten bei der Erfüllung der Aufgaben zu unterstützen und insbesondere

1.  Auskunft zu erteilen sowie Einsicht in alle Vorgänge und Aufzeichnungen, insbesondere in die gespeicherten Daten und in die Datenverarbeitungsprogramme, zu gewähren, die im Zusammenhang mit der Verarbeitung personenbezogener Daten stehen, und
2.  jederzeit Zutritt zu allen Diensträumen, einschließlich aller Datenverarbeitungsanlagen und Geräte zu gewähren.

Die Einsicht nach Satz 1 Nummer 1 kann auch elektronisch gewährt werden.

(2) Absatz 1 gilt für die in § 11 Absatz 3 Satz 1 genannten Behörden nicht, soweit die Datenverarbeitung nicht in den Anwendungsbereich der Verordnung (EU) 2016/679 fällt und das jeweils zuständige Mitglied der Landesregierung im Einzelfall feststellt, dass die Einsicht in die Unterlagen und Akten die Sicherheit des Bundes oder eines Landes gefährdet.
Auf Antrag der oder des Landesbeauftragten hat die Landesregierung dies im zuständigen Ausschuss des Landtages in geheimer Sitzung zu begründen.
Die Entscheidung des Ausschusses kann veröffentlicht werden.

(3) Berufs- und Amtsgeheimnisse entbinden nicht von der Unterstützungspflicht.

#### § 22 Mitteilungen an die Fach- oder Rechtsaufsichtsbehörde

Macht die oder der Landesbeauftragte von den Befugnissen nach Artikel 58 Absatz 2 der Verordnung (EU) 2016/679 Gebrauch, teilt sie oder er dies der zuständigen Fach- oder Rechtsaufsichtsbehörde mit.
Die verantwortliche Stelle gibt gegenüber der zuständigen Fach- oder Rechtsaufsichtsbehörde innerhalb eines Monats, nachdem die Maßnahme nach Satz 1 getroffen wurde, eine Stellungnahme ab.
In dieser Stellungnahme ist darzustellen und zu begründen, in welcher Weise auf die Maßnahme der oder des Landesbeauftragten reagiert wird.

#### § 23 Stellungnahme der Landesregierung zum Tätigkeitsbericht

Soweit der Tätigkeitsbericht der oder des Landesbeauftragten gemäß Artikel 59 der Verordnung (EU) 2016/679 ihren Verantwortungsbereich betrifft, nimmt die Landesregierung innerhalb von sechs Monaten gegenüber dem Landtag schriftlich Stellung.

### Abschnitt 5  
Besondere Verarbeitungssituationen

#### § 24 Verarbeitung besonderer Kategorien personenbezogener Daten

Werden auf der Grundlage dieses Gesetzes besondere Kategorien personenbezogener Daten im Sinne von Artikel 9 Absatz 1 der Verordnung (EU) 2016/679 verarbeitet, sind von der oder dem Verantwortlichen angemessene und spezifische Maßnahmen zur Wahrung der Grundrechte und Interessen der betroffenen Personen vorzusehen.
Unter Berücksichtigung des Stands der Technik, der Implementierungskosten und der Art, des Umfangs, der Umstände und der Zwecke der Verarbeitung sowie der unterschiedlichen Eintrittswahrscheinlichkeit und Schwere der mit der Verarbeitung verbundenen Risiken für die Rechte und Freiheiten natürlicher Personen können dazu insbesondere gehören:

1.  technische und organisatorische Maßnahmen, um sicherzustellen, dass die Verarbeitung gemäß der Verordnung (EU) 2016/679 erfolgt,
2.  Maßnahmen, die gewährleisten, dass nachträglich überprüft und festgestellt werden kann, ob und von wem personenbezogene Daten eingegeben, verändert oder entfernt worden sind,
3.  die Sensibilisierung der an Verarbeitungsvorgängen Beteiligten,
4.  die Beschränkung des Zugangs zu den personenbezogenen Daten innerhalb der verantwortlichen Stelle und von Auftragsverarbeitern,
5.  die Pseudonymisierung personenbezogener Daten,
6.  die Verschlüsselung personenbezogener Daten,
7.  die Sicherstellung der Fähigkeit, Vertraulichkeit, Integrität, Verfügbarkeit und Belastbarkeit der Systeme und Dienste im Zusammenhang mit der Verarbeitung personenbezogener Daten einschließlich der Fähigkeit, die Verfügbarkeit und den Zugang bei einem physischen oder technischen Zwischenfall unverzüglich wiederherzustellen,
8.  zur Gewährleistung der Sicherheit der Verarbeitung die Einrichtung eines Verfahrens zur regelmäßigen Überprüfung, Bewertung und Evaluierung der Wirksamkeit der technischen und organisatorischen Maßnahmen oder
9.  spezifische Verfahrensregelungen, die im Falle einer Übermittlung oder Verarbeitung für andere Zwecke, die Einhaltung der Vorgaben dieses Gesetzes sowie der Verordnung (EU) 2016/679 sicherstellen.

#### Unterabschnitt 1  
Besondere Verarbeitungssituationen im Anwendungsbereich der Verordnung (EU) 2016/679

#### § 25 Datenverarbeitung für wissenschaftliche und historische Forschungszwecke

(1) Öffentliche Stellen dürfen personenbezogene Daten einschließlich Daten im Sinne von Artikel 9 Absatz 1 der Verordnung (EU) 2016/679 ohne Einwilligung für ein bestimmtes Forschungsvorhaben verarbeiten und an andere Stellen oder Personen zu diesem Zweck übermitteln, wenn schutzwürdige Belange der betroffenen Person wegen der Art der Daten, wegen ihrer Offenkundigkeit oder wegen der Art der Verwendung nicht beeinträchtigt werden oder das öffentliche Interesse an der Durchführung des Forschungsvorhabens die schutzwürdigen Belange der betroffenen Person überwiegt und der Zweck der Forschung nicht auf andere Weise erreicht werden kann.
Die übermittelten Daten dürfen nicht für andere Zwecke verarbeitet werden.

(2) Die Daten sind zu anonymisieren, sobald dies nach dem Forschungszweck möglich ist.
Bis dahin sind die Merkmale gesondert zu speichern, mit denen Einzelangaben über persönliche oder sachliche Verhältnisse einer bestimmten oder bestimmbaren natürlichen Person zugeordnet werden können.
Sie sind zu löschen, sobald der Forschungszweck dies erlaubt.

(3) Die wissenschaftliche und historische Forschung betreibenden öffentlichen Stellen dürfen personenbezogene Daten nur veröffentlichen, wenn

1.  die betroffene Person eingewilligt hat oder
2.  dies für die Darstellung von Forschungsergebnissen über Ereignisse der Zeitgeschichte unerlässlich ist.

(4) An Dritte oder Stellen, die den Vorschriften dieses Gesetzes nicht unterliegen, dürfen personenbezogene Daten entsprechend Absatz 1 Satz 1 nur übermittelt werden, wenn diese sich verpflichten, die Bestimmungen des Absatzes 1 Satz 2 sowie der Absätze 2 und 3 einzuhalten.

(5) Das Recht auf Auskunft nach Artikel 15 der Verordnung (EU) 2016/679, auf Berichtigung nach Artikel 16 der Verordnung (EU) 2016/679, auf Einschränkung der Verarbeitung nach Artikel 18 der Verordnung (EU) 2016/679 und auf Widerspruch nach Artikel 21 der Verordnung (EU) 2016/679 besteht nicht, soweit die Wahrnehmung dieser Rechte die spezifischen Forschungszwecke unmöglich machen oder ernsthaft beeinträchtigen würde oder die Inanspruchnahme oder Gewährung dieser Rechte unmöglich ist oder einen unverhältnismäßigen Aufwand erfordern würde.

#### § 26 Datenverarbeitung bei Beschäftigungsverhältnissen

(1) Personenbezogene Daten von Bewerberinnen und Bewerbern sowie von Beschäftigten dürfen nur verarbeitet werden, wenn dies zur Eingehung, Durchführung, Beendigung oder Abwicklung des Dienst- oder Arbeitsverhältnisses oder zur Durchführung innerdienstlicher, planerischer, organisatorischer, personeller, sozialer oder haushalts- und kostenrechnerischer Maßnahmen, insbesondere zu Zwecken der Personalplanung und des Personaleinsatzes, erforderlich ist oder in einer Rechtsvorschrift, einem Tarifvertrag oder einer Dienst- oder Betriebsvereinbarung (Kollektivvereinbarung) vorgesehen ist.
Eine Übermittlung der Daten von Beschäftigten an Personen und Stellen außerhalb des öffentlichen Bereiches ist nur zulässig, wenn die Empfängerin oder der Empfänger ein rechtliches Interesse darlegt, der Dienstverkehr es erfordert oder die betroffene Person eingewilligt hat.
Die Datenübermittlung an einen künftigen Dienstherrn oder Arbeitgeber ist nur mit Einwilligung der betroffenen Person zulässig.

(2) Erfolgt die Verarbeitung personenbezogener Daten von Beschäftigten auf der Grundlage einer Einwilligung, so sind für die Beurteilung der Freiwilligkeit der Einwilligung insbesondere die im Beschäftigungsverhältnis bestehende Abhängigkeit der beschäftigten Person sowie die Umstände, unter denen die Einwilligung erteilt worden ist, zu berücksichtigen.
Freiwilligkeit kann insbesondere vorliegen, wenn für die beschäftigte Person ein rechtlicher oder wirtschaftlicher Vorteil erreicht wird oder Arbeitgeber und beschäftigte Person gleichgelagerte Interessen verfolgen.
Die Einwilligung bedarf der Schriftform, soweit nicht wegen besonderer Umstände eine andere Form angemessen ist.
Die beschäftigte Person ist über den Zweck der Datenverarbeitung und über ihr Widerrufsrecht nach Artikel 7 Absatz 3 der Verordnung (EU) 2016/679 in Textform aufzuklären.

(3) Abweichend von Artikel 9 Absatz 1 der Verordnung (EU) 2016/679 ist die Verarbeitung besonderer Kategorien personenbezogener Daten im Sinne des Artikels 9 Absatz 1 der Verordnung (EU) 2016/679 für Zwecke des Beschäftigungsverhältnisses zulässig, wenn sie zur Ausübung von Rechten oder zur Erfüllung rechtlicher Pflichten aus dem Arbeitsrecht, dem Beamtenrecht, dem Recht der sozialen Sicherheit und des Sozialschutzes erforderlich ist und kein Grund zu der Annahme besteht, dass das schutzwürdige Interesse der betroffenen Person an dem Ausschluss der Verarbeitung überwiegt.
Erfolgt die Verarbeitung auf der Grundlage einer Einwilligung, muss sich die Einwilligung ausdrücklich auf diese Daten beziehen.

(4) Auf die Verarbeitung von Personalaktendaten der Arbeitnehmerinnen und Arbeitnehmer sowie der Auszubildenden finden die für die Beamtinnen und Beamten geltenden Vorschriften des Landesbeamtengesetzes entsprechend Anwendung, es sei denn, besondere Rechtsvorschriften oder tarifliche Vereinbarungen gehen vor.

(5) Die Speicherung, Veränderung oder Nutzung der bei medizinischen oder psychologischen Untersuchungen und Tests zum Zweck der Feststellung der Eignung erhobenen Daten ist nur zulässig, wenn dies für Zwecke der Eingehung oder Durchführung eines Dienst- oder Arbeitsverhältnisses erforderlich ist.
Eine Verarbeitung dieser Daten zu anderen Zwecken ist nur mit Einwilligung der betroffenen Person zulässig.
Die Beschäftigungsbehörde darf von der untersuchenden Ärztin oder dem untersuchenden Arzt nur die Übermittlung des Ergebnisses der Eignungsuntersuchung und dabei festgestellter Risikofaktoren verlangen.

(6) Personenbezogene Daten, die vor der Eingehung eines Dienst- oder Arbeitsverhältnisses erhoben wurden, sind unverzüglich zu löschen, sobald feststeht, dass ein Dienst- oder Arbeitsverhältnis nicht zustande kommt, es sei denn, dass die betroffene Person in die weitere Speicherung eingewilligt hat.
Nach Beendigung eines Dienst- oder Arbeitsverhältnisses sind personenbezogene Daten zu löschen, wenn diese Daten nicht mehr benötigt werden, es sei denn, dass Rechtsvorschriften entgegenstehen.

(7) Soweit Daten der Beschäftigten im Rahmen der Durchführung der technischen und organisatorischen Maßnahmen nach Artikel 32 der Verordnung (EU) 2016/679 gespeichert werden, dürfen sie nicht zu Zwecken der Verhaltens- oder Leistungskontrolle genutzt werden.

#### § 27 Verarbeitung personenbezogener Daten zu Zwecken der parlamentarischen Kontrolle

(1) Die Landesregierung darf personenbezogene Daten einschließlich Daten im Sinne von Artikel 9 Absatz 1 der Verordnung (EU) 2016/679 zur Beantwortung parlamentarischer Anfragen sowie zur Vorlage von Unterlagen und Berichten an den Landtag in dem dafür erforderlichen Umfang verarbeiten.
Eine Übermittlung der Daten zu einem der in Satz 1 genannten Zwecke ist nicht zulässig, wenn dies wegen des streng persönlichen Charakters der Daten für die betroffene Person unzumutbar ist oder wenn der Eingriff in ihr informationelles Selbstbestimmungsrecht unverhältnismäßig ist.
Dies gilt nicht, wenn im Hinblick auf § 2 Absatz 2 Satz 2 oder durch sonstige geeignete Maßnahmen sichergestellt ist, dass schutzwürdige Interessen der betroffenen Personen nicht beeinträchtigt werden.
Besondere gesetzliche Übermittlungsverbote bleiben unberührt.

(2) Von der Landesregierung übermittelte personenbezogene Daten dürfen nicht in Landtagsdrucksachen aufgenommen oder in sonstiger Weise öffentlich zugänglich gemacht werden.
Dies gilt nicht, wenn keine Anhaltspunkte dafür bestehen, dass schutzwürdige Belange der betroffenen Personen beeinträchtigt werden.

#### § 28 Videoüberwachung öffentlich zugänglicher Räume

(1) Die Erhebung personenbezogener Daten mit Hilfe von optisch-elektronischen Einrichtungen (Videoüberwachung) und deren weitere Verarbeitung ist zulässig, wenn dies

1.  zur Erfüllung der Aufgaben öffentlicher Stellen,
2.  zur Wahrnehmung des Hausrechts,
3.  zum Schutz des Eigentums oder Besitzes oder
4.  zur Kontrolle von Zugangsberechtigungen

erforderlich ist und keine Anhaltspunkte bestehen, dass überwiegende schutzwürdige Interessen der betroffenen Personen entgegenstehen.

(2) Die Videoüberwachung, die Angaben nach Artikel 13 Absatz 1 Buchstabe a bis c der Verordnung (EU) 2016/679 sowie die Möglichkeit, bei der oder dem Verantwortlichen die weiteren Informationen nach Artikel 13 der Verordnung (EU) 2016/679 zu erhalten, sind durch geeignete Maßnahmen zum frühestmöglichen Zeitpunkt erkennbar zu machen.

(3) Eine Verarbeitung zu anderen Zwecken ist nur zulässig, soweit dies zur Abwehr von Gefahren für die öffentliche Sicherheit oder zur Verfolgung von Straftaten erforderlich ist.

(4) Werden durch eine Videoüberwachung erhobene personenbezogene Daten einer bestimmten Person zugeordnet oder zu anderen als den in Absatz 1 genannten Zwecken verarbeitet, ist die betroffene Person ergänzend zu den Artikeln 13 und 14 der Verordnung (EU) 2016/679 zu informieren, soweit und solange der Zweck der Verarbeitung hierdurch nicht gefährdet wird.
§ 10 gilt entsprechend.

#### § 29 Verarbeitung personenbezogener Daten zu Zwecken<br>der freien Meinungsäußerung und der Informationsfreiheit

(1) Soweit personenbezogene Daten in Ausübung des Rechts auf freie Meinungsäußerung und Informationsfreiheit zu journalistischen, künstlerischen oder literarischen Zwecken verarbeitet werden, gelten von den Kapiteln II bis VII sowie IX der Verordnung (EU) 2016/679 nur Artikel 5 Absatz 1 Buchstabe f sowie die Artikel 24, 32 und 33.
Artikel 82 der Verordnung (EU) 2016/679 gilt mit der Maßgabe, dass nur für Schäden gehaftet wird, die durch unzureichende technische oder organisatorische Maßnahmen im Sinne des Artikels 5 Absatz 1 Buchstabe f der Verordnung (EU) 2016/679 eintreten.

(2) Führt die Verarbeitung personenbezogener Daten gemäß Absatz 1 Satz 1 zur Verbreitung von Gegendarstellungen der betroffenen Person oder zu Verpflichtungserklärungen, Beschlüssen oder Urteilen über die Unterlassung der Verbreitung oder über den Widerruf des Inhalts der Daten, so sind diese Gegendarstellungen, Verpflichtungserklärungen, Beschlüsse, Urteile und Widerrufe zu den gespeicherten Daten zu nehmen und dort für dieselbe Zeitdauer aufzubewahren wie die Daten selbst sowie bei einer Übermittlung der Daten gemeinsam zu übermitteln.

(3) Die Absätze 1 und 2 gelten auch für nicht-öffentliche Stellen.

#### Unterabschnitt 2  
Besondere Verarbeitungssituationen außerhalb des Anwendungsbereichs  
der Verordnung (EU) 2016/679

#### § 30 Öffentliche Auszeichnungen und Ehrungen

(1) Zur Vorbereitung öffentlicher Auszeichnungen und Ehrungen dürfen die zuständigen Stellen die dazu erforderlichen Daten einschließlich Daten im Sinne von Artikel 9 Absatz 1 der Verordnung (EU) 2016/679 auch ohne Kenntnis der betroffenen Person verarbeiten.
Eine Verarbeitung dieser Daten für andere Zwecke ist nur mit Einwilligung der betroffenen Person zulässig.

(2) Auf Anforderung der in Absatz 1 genannten Stellen dürfen andere öffentliche Stellen die zur Vorbereitung der Auszeichnung oder Ehrung erforderlichen Daten übermitteln.

(3) Die Absätze 1 und 2 finden keine Anwendung, wenn der verantwortlichen Stelle bekannt ist, dass die betroffene Person keine öffentlichen Auszeichnungen oder Ehrungen wünscht oder der dazu notwendigen Datenverarbeitung widersprochen hat.

(4) Die in Absatz 1 genannten Stellen haben der betroffenen Person auf Antrag Auskunft zu erteilen über

1.  die zu ihr gespeicherten Daten,
2.  den Zweck und die Rechtsgrundlage der Speicherung sowie
3.  die Herkunft der Daten.

Die Form der Auskunftserteilung ist nach pflichtgemäßem Ermessen zu bestimmen.

(5) In Verfahren der Entscheidung über öffentliche Auszeichnungen und Ehrungen gelten nur die Artikel 4 bis 7, 16 bis 18, Kapitel IV sowie Kapitel VI der Verordnung (EU) 2016/679 entsprechend.

#### § 31 Begnadigungsverfahren

(1) In Begnadigungsverfahren ist die Verarbeitung personenbezogener Daten einschließlich Daten im Sinne von Artikel 9 Absatz 1 der Verordnung (EU) 2016/679 zulässig, soweit sie zur Ausübung des Gnadenrechts durch die zuständigen Stellen erforderlich ist.
Diese Datenverarbeitung unterliegt nicht der Kontrolle durch die oder den Landesbeauftragten.

(2) In Begnadigungsverfahren gelten nur die Artikel 4 bis 7 sowie Kapitel IV mit Ausnahme von Artikel 33 der Verordnung (EU) 2016/679 entsprechend.

### Abschnitt 6  
Sanktionen, Einschränkung von Grundrechten

#### § 32 Ordnungswidrigkeiten

(1) Ordnungswidrig handelt, wer entgegen den Vorschriften der Verordnung (EU) 2016/679, dieses Gesetzes oder einer anderen Rechtsvorschrift über den Schutz personenbezogener Daten, personenbezogene Daten, die nicht offenkundig sind,

1.  erhebt, speichert, verwendet, verändert, übermittelt, weitergibt, zum Abruf bereithält, den Personenbezug herstellt oder löscht,
2.  abruft, einsieht, sich verschafft oder durch Vortäuschung falscher Tatsachen ihre Übermittlung oder Weitergabe an sich oder andere veranlasst oder
3.  in anderer Weise verarbeitet.

Ordnungswidrig handelt auch, wer unter den in Satz 1 genannten Voraussetzungen Einzelangaben über persönliche oder sachliche Verhältnisse einer nicht mehr bestimmbaren Person mit anderen Informationen zusammenführt und dadurch die betroffene Person wieder bestimmbar macht.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

(3) Gegen Behörden oder sonstige öffentliche Stellen im Sinne von § 2 Absatz 1 und 2 werden keine Geldbußen verhängt.

#### § 33 Strafvorschrift

Wer gegen Entgelt oder in der Absicht, sich oder einen anderen zu bereichern oder einen anderen zu schädigen, eine der in § 32 Absatz 1 genannten Handlungen begeht, wird mit Freiheitsstrafe bis zu zwei Jahren oder mit Geldstrafe bestraft.
Die Tat wird nur auf Antrag verfolgt.
Antragsberechtigt sind die betroffenen Personen, die Verantwortlichen, die Auftragsverarbeiter und die oder der Landesbeauftragte.

#### § 34 Einschränkung eines Grundrechts

Durch dieses Gesetz wird das Grundrecht auf Datenschutz (Artikel 11 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 35 Übergangsvorschrift

(1) Die oder der zum Zeitpunkt des Inkrafttretens dieses Gesetzes im Amt befindliche Landesbeauftragte gilt als nach § 15 Absatz 1 Satz 2 ernannt.
Ihre oder seine statusrechtliche Stellung bleibt unberührt; § 17 Absatz 1 Satz 4 findet Anwendung.
Die Amtszeit nach § 15 Absatz 3 Satz 1 gilt als zum 7.
Juli 2017 begonnen.
Der Aushändigung einer Ernennungsurkunde bedarf es nicht.

(2) Für die Verarbeitung personenbezogener Daten durch Ordnungsbehörden, soweit sie Ordnungswidrigkeiten verfolgen oder ahnden sowie Sanktionen vollstrecken, finden die Vorschriften des Brandenburgischen Datenschutzgesetzes in der am 24.
Mai 2018 geltenden Fassung Anwendung.