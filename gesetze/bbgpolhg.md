## Gesetz über die Hochschule der Polizei des Landes Brandenburg (Brandenburgisches Polizeihochschulgesetz - BbgPolHG)

Der Landtag hat das folgende Gesetz beschlossen:

**Inhaltsübersicht**

[§ 1 Geltungsbereich, Rechtsstellung](#1)

[§ 2 Satzungsrecht](#2)

[§ 3 Aufgaben](#3)

[§ 4 Bachelorstudium](#4)

[§ 5 Forschung und wissenschaftliche Dienstleistung](#5)

[§ 6 Qualitätssicherung](#6)

[§ 7 Finanzierung](#7)

[§ 8 Senat](#8)

[§ 9 Senatsmitglieder](#9)

[§ 10 Hochschulleitung](#10)

[§ 11 Verwaltung, Dekanat](#11)

[§ 12 Lehrpersonal](#12)

[§ 13 Freiheit der Lehre, Lehrverpflichtung](#13)

[§ 14 Berufung von Professorinnen und Professoren](#14)

[§ 15 Bestellung von Lehrkräften](#15)

[§ 16 Lehraufträge](#16)

[§ 17 Personalvertretung](#17)

[§ 18 Einschränkung von Grundrechten](#18)

[§ 19 Übergangsregelungen](#19)

#### § 1 Geltungsbereich, Rechtsstellung

(1) Im Geschäftsbereich des für Inneres zuständigen Ministeriums wird als Einrichtung im Sinne des § 9 Absatz 1 des Landesorganisationsgesetzes vom 24.
Mai 2004 (GVBl.
I S. 186), das zuletzt durch Artikel 2 des Gesetzes vom 10.
Juli 2014 (GVBl.
I Nr. 28 S. 2) geändert worden ist, eine Hochschule der Polizei errichtet.
Sie führt die Bezeichnung „Hochschule der Polizei des Landes Brandenburg“ und hat ihren Sitz in der Stadt Oranienburg.

(2) Als Fachhochschule gemäß § 1 Absatz 2 des Brandenburgischen Hochschulgesetzes vom 28. April 2014 (GVBl.
I Nr. 18), das zuletzt durch Artikel 2 des Gesetzes vom 20. September 2018 (GVBl. I Nr. 21 S. 2) geändert worden ist, hat sie unbeschadet der Rechte ihres Trägers das Recht der Selbstverwaltung im Rahmen der Gesetze und ist teilrechtsfähig, soweit sie nach den Vorschriften dieses Gesetzes ihre Angelegenheiten durch Satzung regeln kann.

(3) Die Hochschule der Polizei unterliegt der Dienst- und Fachaufsicht, in Fragen von Lehre und Forschung der Rechtsaufsicht.
Die Aufsicht wird durch das für Inneres zuständige Ministerium ausgeübt, in hochschulrechtlichen Angelegenheiten im Einvernehmen mit dem für die Hochschulen zuständigen Ministerium.

#### § 2 Satzungsrecht

(1) Die Hochschule der Polizei gibt sich eine Grundordnung als Satzung, in der die Gliederung der Einrichtung und die Organisation des Lehrbetriebs geregelt werden.

(2) Angelegenheiten der Lehre und Forschung kann die Hochschule der Polizei auf der Grundlage der beamtenrechtlichen Ausbildungs- und Prüfungsordnungen durch weitere Satzungen regeln.
Dies betrifft insbesondere

1.  in der Studienordnung die inhaltliche Ausgestaltung der angebotenen Studiengänge,
2.  in der Praktikumsordnung die inhaltliche Ausgestaltung der fachpraktischen Ausbildungsabschnitte im Einvernehmen mit der Polizeibehörde, wobei auch die Möglichkeit vorgesehen werden soll, Teile der fachpraktischen Ausbildung bei Polizeidienststellen anderer Dienstherren zu absolvieren,
3.  in der Prüfungsordnung die Anforderungen und das Verfahren von Prüfungen, die die Hochschule der Polizei abnimmt,
4.  in der Lehrverpflichtungsordnung die Lehrverpflichtungen des Lehrpersonals der Hochschule der Polizei.

(3) Die Grundordnung bedarf vor ihrer Ausfertigung der Zustimmung durch das für Inneres zuständige Ministerium.
Alle übrigen Satzungen sind diesem vor ihrer Ausfertigung anzuzeigen.

(4) Satzungen sind durch das für Inneres zuständige Ministerium aufzuheben, wenn die darin enthaltenen Regelungen gegen Rechtsvorschriften verstoßen.
Sie können aufgehoben werden, wenn durch die Regelung die Erfüllung der der Hochschule der Polizei übertragenen Aufgaben gefährdet wird.

(5) Satzungen sind in den amtlichen Mitteilungen der Hochschule der Polizei des Landes Brandenburg bekannt zu machen.
Sie sind zusätzlich in elektronischer Form zu veröffentlichen.

#### § 3 Aufgaben

(1) Die Hochschule der Polizei hat die Aufgabe, Anwärterinnen und Anwärter für die Laufbahnen des mittleren und gehobenen Polizeivollzugsdienstes nach Maßgabe der Ausbildungs- und Prüfungsordnungen auszubilden (Lehraufgabe).
Sie sind zu verantwortlichem Handeln in einem freiheitlichen, demokratischen, dem Schutz der natürlichen Lebensgrundlagen und der Kultur verpflichteten sozialen Rechtsstaat zu befähigen.
Hierzu vermittelt die Hochschule der Polizei die theoretischen Kenntnisse und die berufsbezogenen praktischen Fähigkeiten, die zur Erfüllung polizeilicher Aufgaben erforderlich sind.

(2) Bewerberinnen und Bewerber für die Ausbildung werden von der Hochschule der Polizei nach Maßgabe der Brandenburgischen Polizeilaufbahnverordnung ausgewählt und in den Vorbereitungsdienst für den mittleren oder gehobenen Polizeivollzugsdienst eingestellt.

(3) Zusätzlich nimmt die Hochschule der Polizei folgende Lehraufgaben wahr:

1.  Ausrichtung von Studienabschnitten des Masterstudiengangs zum Erwerb der Laufbahnbefähigung für den höheren Polizeivollzugsdienst, soweit diese nach Maßgabe des Gesetzes über die Deutsche Hochschule der Polizei vom 15. Februar 2005 (GV.
    NRW. S. 88) und den darauf beruhenden Studien- und Prüfungsvorschriften in den Ländern stattfinden,
2.  Ausrichtung anwendungsorientierter Masterstudiengänge zum Erwerb vertieften Fachwissens für besondere polizeiliche Aufgabenbereiche, soweit Bedarf zur Ausbildung von Bediensteten mit Spezialkenntnissen besteht,
3.  Ausbildung für den Aufstieg in den gehobenen Polizeivollzugsdienst auf der Grundlage der Polizeiaufstiegsverordnung,
4.  Organisation und Durchführung der Weiterbildung in der Polizei und
5.  Aus- und Weiterbildung von Bediensteten der Landesverwaltung, anderer öffentlicher Dienstherren und die Weiterbildung von Beschäftigten Dritter.

(4) Bei erfolgreichem Abschluss von Masterstudiengängen zum Erwerb vertieften Fachwissens für besondere polizeiliche Aufgabenbereiche verleiht die Hochschule der Polizei den Mastergrad „Master of Arts“ (M.A.).

(5) Die Hochschule der Polizei fördert den Gedanken der europäischen Einigung und trägt im Rahmen von Lehre und Forschung zur Gestaltung eines einheitlichen europäischen Raums der Freiheit, der Sicherheit und des Rechts bei.
Sie unterhält zu diesem Zweck ein Internationales Zentrum.
Dieses hat die Aufgabe, die internationale und grenzüberschreitende polizeiliche Zusammenarbeit zu pflegen, die Zusammenarbeit mit vergleichbaren Hochschulen, insbesondere des europäischen Auslands, zu fördern, den Beitrag der Polizei des Landes Brandenburg zur Unterstützung internationaler Polizeimissionen zu organisieren und den internationalen polizeilichen Erfahrungsaustausch durch gegenseitige Besuche und Hospitationen zu vertiefen.

(6) Durch Rechtsverordnung kann das für Inneres zuständige Mitglied der Landesregierung der Hochschule der Polizei weitere Lehraufgaben aus dem Bereich des öffentlichen Dienstes, insbesondere Aufgaben der Weiterbildung, übertragen.

#### § 4 Bachelorstudium

(1) Die Ausbildung der Anwärterinnen und Anwärter für die Laufbahn des gehobenen Polizeivollzugsdienstes erfolgt einheitlich in einem akademischen Bachelorstudiengang.
Dabei werden neben den berufsbezogenen praktischen Fähigkeiten auch die wissenschaftlichen Kenntnisse und Methoden vermittelt, die für eine differenzierte Analyse und Bewertung komplexer Sachverhalte erforderlich sind.
Das Verständnis für die gesellschaftlichen und wirtschaftlichen Zusammenhänge vor dem Hintergrund europäischer Belange ist besonders zu fördern.

(2) Zum Bachelorstudium ist immatrikuliert, wer in den Vorbereitungsdienst für den gehobenen Polizeivollzugsdienst des Landes Brandenburg eingestellt wurde.
Die Hochschule der Polizei kann im Rahmen von Förder- und Austauschprogrammen Gaststudierende zum Studium zulassen.

(3) Die Studierenden haben das Recht, Lehrveranstaltungen zu wählen, Schwerpunkte nach eigener Wahl zu setzen sowie wissenschaftliche Meinungen zu erarbeiten und zu äußern.
Dienstrechtliche Verpflichtungen bleiben davon unberührt.
Näheres regelt die Studienordnung.

(4) Bei erfolgreichem Studienabschluss verleiht die Hochschule der Polizei den Bachelorgrad „Bachelor of Arts“ (B.A.).

(5) Studierende, die aus dem Vorbereitungsdienst für den gehobenen Polizeivollzugsdienst ausscheiden, sind ab diesem Zeitpunkt von der Hochschule der Polizei exmatrikuliert.

#### § 5 Forschung und wissenschaftliche Dienstleistung

(1) Die Hochschule der Polizei betreibt zur Erfüllung ihrer Lehraufgaben und zur Unterstützung der polizeilichen Praxis bedarfsgerechte und anwendungsbezogene Forschung.
Sie ist bei der Auswahl der Forschungsschwerpunkte und -methoden den von der Deutschen Forschungsgemeinschaft formulierten Standards guter wissenschaftlicher Praxis unterworfen.
Zur Erfüllung der Forschungsaufgaben sowie zur Förderung grundsätzlicher polizeilicher Entwicklungsvorhaben können eigenständige Institute gegründet werden.
Weiteres regelt die Grundordnung.

(2) Die Hochschule der Polizei erbringt wissenschaftliche Dienstleistungen zur Pflege und Entwicklung der polizeibezogenen Wissenschaften oder im Auftrag des für Inneres zuständigen Ministeriums.

#### § 6 Qualitätssicherung

Die Hochschule der Polizei sichert die Qualität ihrer Aufgabenerfüllung.
Wesentlicher Bestandteil der Qualitätssicherung ist die regelmäßige Durchführung von Evaluationen.
Das Nähere wird durch Satzung geregelt.

#### § 7 Finanzierung

(1) Das Land Brandenburg ist Träger der Hochschule der Polizei und stellt ihr nach Maßgabe des Haushalts die Mittel zur Erfüllung ihrer Aufgaben zur Verfügung.
Die Hochschule der Polizei meldet in einem Beitrag zum Haushaltsvoranschlag benötigte Stellen und Mittel bei dem für Inneres zuständigen Ministerium an.

(2) Soweit Bedienstete, die nicht dem Geschäftsbereich des für Inneres zuständigen Ministeriums angehören, oder Bedienstete Dritter an der Hochschule der Polizei aus- oder weitergebildet werden, sollen die dadurch entstandenen Kosten anteilig nach der Teilnehmerzahl umgelegt werden.
Ausgenommen davon sind Kosten für Grunderwerb, für Neu-, Um- und Erweiterungsbauten sowie für die Erstausstattung.

(3) Die Hochschule der Polizei kann finanzielle oder anders geartete Unterstützungen Dritter als Zuwendungen mit dem Ziel der Werbung oder Öffentlichkeitsarbeit (Sponsoring) entgegennehmen.
Dabei muss jede fremde Einflussnahme bereits dem Anschein nach vermieden werden, um die Integrität und die Neutralität des Lehrbetriebs zu wahren.
Weiteres regelt die Grundordnung.

(4) Die Hochschule der Polizei kann im Rahmen ihrer Aufgaben auch solche Forschungs- und Lehrvorhaben durchführen, die nicht aus ihr zur Verfügung stehenden Haushaltsmitteln finanziert werden.
Hierzu ist sie berechtigt, Drittmittel entgegenzunehmen.

#### § 8 Senat

(1) Der Senat beschließt die Satzungen der Hochschule der Polizei.
Diese Beschlüsse werden mit Ausfertigung durch die Präsidentin oder den Präsidenten wirksam.

(2) Der Senat fördert die Zusammenarbeit mit anderen Hochschulen und vergleichbaren in- und ausländischen Einrichtungen.
Er berät und unterstützt die Hochschulleitung in grundsätzlichen Angelegenheiten.
Hierzu zählen insbesondere:

1.  der Lehrbetrieb,
2.  Grundsatzfragen der fachpraktischen Ausbildung,
3.  die Organisation und Planung der weiteren Entwicklung der Hochschule der Polizei und
4.  der Beitrag der Hochschule zum Haushaltsvoranschlag.

(3) Der Senat ist in folgenden Angelegenheiten anzuhören:

1.  vor der Bestellung der Präsidentin oder des Präsidenten, der Vizepräsidentin oder des Vizepräsidenten, der Dekanin oder des Dekans, der Kanzlerin oder des Kanzlers,
2.  vor der Bildung der Kommission zur Berufung von Professorinnen und Professoren und zu Berufungsvorschlägen dieser Kommission,
3.  vor der Bestellung von Lehrkräften für die angebotenen Studiengänge,
4.  zum Bericht der Präsidentin oder des Präsidenten über erteilte Lehraufträge,
5.  vor der Stellungnahme der Hochschule der Polizei zu Änderungen der Ausbildungs-, Prüfungs- und Aufstiegsordnungen für die Laufbahnen des gehobenen Polizeivollzugsdienstes,
6.  vor der Vergabe von Leistungsbezügen an Professorinnen und Professoren.

(4) Die Beschlüsse des Senats zu den in Absatz 2 und 3 genannten Angelegenheiten werden mit Ausfertigung durch die Vorsitzende oder den Vorsitzenden des Senats wirksam.

#### § 9 Senatsmitglieder

(1) Dem Senat gehören an:

1.  die Präsidentin oder der Präsident,
2.  die Vizepräsidentin oder der Vizepräsident,
3.  die Kanzlerin oder der Kanzler,
4.  die Dekanin oder der Dekan,
5.  je eine Verantwortliche oder ein Verantwortlicher für
    1.  Masterstudiengänge, soweit diese an der Hochschule der Polizei durchgeführt werden,
    2.  den Ausbildungsgang und
    3.  die Weiterbildung,
6.  vier Vertretungspersonen für das Lehrpersonal,
7.  je eine Vertretungsperson aus jedem Studien- und Ausbildungsgang sowie
8.  mit beratender Stimme eine Vertreterin oder ein Vertreter der Lehrbeauftragten.

(2) Die Mitglieder des Senats nach Absatz 1 Nummer 6 bis 8 werden von den dort jeweils genannten Angehörigen der Hochschule der Polizei nach Gruppen getrennt gewählt.
Weiteres regelt die Grundordnung.
Unabhängig davon endet die Amtszeit in jedem Falle mit Beendigung des Studiums, der Ausbildung, der Bestellung zur Lehrkraft oder des Lehrauftrags.

(3) Der Senat gibt sich eine Geschäftsordnung und wählt aus seiner Mitte einen Vorsitz und eine Stellvertretung.
Die Hochschulleitung ist dabei nicht wählbar.

(4) Zur Erfüllung seiner Aufgaben hat der Senat ein umfassendes Unterrichtungsrecht gegenüber der Hochschulleitung.
Die Mitglieder des Senats sind im Rahmen ihrer Senatstätigkeit an Weisungen nicht gebunden und dürfen wegen ihrer Tätigkeit im Senat nicht benachteiligt werden.

(5) Der Senat lädt zu seinen Sitzungen das für Inneres zuständige Ministerium und, soweit die fachpraktische Ausbildung beraten wird, die Polizeibehörde ein.

#### § 10 Hochschulleitung

(1) Die Präsidentin oder der Präsident

1.  leitet die Hochschule der Polizei und vertritt sie nach außen,
2.  trifft alle beamten- und arbeitsrechtlichen Entscheidungen, soweit keine gesonderten Zuständigkeitsregelungen gelten,
3.  führt die Beschlüsse des Senats gemäß § 8 Absatz 1 aus und
4.  erstattet dem Senat Bericht über erteilte Lehraufträge.

(2) Die Vizepräsidentin oder der Vizepräsident koordiniert den ordnungsgemäßen Lehrbetrieb und die Forschungsvorhaben der Hochschule.
Im Falle der Verhinderung der Präsidentin oder des Präsidenten nimmt die Vizepräsidentin oder der Vizepräsident die Aufgaben gemäß Absatz 1 wahr.

(3) Die Ämter nach den Absätzen 1 und 2 (Hochschulleitung) können nur Personen übertragen werden, die Hochschullehrerin oder Hochschullehrer sind oder die Befähigung für die Laufbahnen des höheren Polizeivollzugsdienstes oder des höheren allgemeinen Verwaltungsdienstes besitzen und aufgrund einer mehrjährigen verantwortlichen beruflichen Tätigkeit, insbesondere in Verwaltung, Polizei, Wissenschaft oder Rechtspflege erwarten lassen, den Aufgaben des Amtes gewachsen zu sein.

(4) Die Ernennung zur Präsidentin oder zum Präsidenten, zur Vizepräsidentin oder zum Vizepräsidenten erfolgt durch das für Inneres zuständige Mitglied der Landesregierung.
Beide Hochschulleitungsämter werden durch das für Inneres zuständige Ministerium ausgeschrieben und anschließend zunächst als Ämter mit leitender Funktion im Beamtenverhältnis auf Probe im Sinne von § 120 des Landesbeamtengesetzes übertragen.
Bei einer Bestellung aus einem Beschäftigtenverhältnis heraus, kann das Amt auch in dieser Form ausgeübt werden.

#### § 11 Verwaltung, Dekanat

(1) Die Kanzlerin oder der Kanzler leitet die Verwaltung der Hochschule der Polizei.
Näheres regelt die Grundordnung.

(2) Die Dekanin oder der Dekan leitet die angebotenen Studiengänge und ist dem Senat über die Erfüllung dieser Lehraufgabe zur umfassenden Information verpflichtet.

(3) Das Amt der Kanzlerin oder des Kanzlers kann nur Beamtinnen und Beamten des höheren allgemeinen Verwaltungsdienstes oder Beschäftigten mit vergleichbarer Befähigung übertragen werden.
Das Amt der Dekanin oder des Dekans kann nur Beschäftigten, Beamtinnen oder Beamten, die zu einer Lehrkraft des höheren Dienstes bestellt sind oder die Voraussetzungen hierfür erfüllen, und Professorinnen oder Professoren übertragen werden.

(4) Die Ernennung zur Kanzlerin oder zum Kanzler und die Bestellung zur Dekanin oder zum Dekan erfolgen durch das für Inneres zuständige Ministerium.
Bei einer Bestellung aus einem Beschäftigtenverhältnis heraus, kann das jeweilige Amt auch in dieser Form ausgeübt werden.
Die Ausschreibung erfolgt durch das für Inneres zuständige Ministerium.

#### § 12 Lehrpersonal

(1) Die Lehraufgaben der Hochschule der Polizei werden in der Regel vom Lehrpersonal sowie von Trainerinnen und Trainern wahrgenommen.
Das Lehrpersonal setzt sich zusammen aus hauptamtlich tätigen Professorinnen oder Professoren und Lehrkräften des höheren und gehobenen Dienstes.

(2) Zur Vermittlung von überwiegend praktischen Fertigkeiten und Kenntnissen kann als Trainerin oder Trainer eingesetzt werden, wer die Unterrichtsbefähigung durch besondere fachbezogene oder fachpraktische Leistungen nachgewiesen hat und die erforderlichen pädagogischen Fähigkeiten besitzt.
Diese Dienstposten sind von der Hochschule der Polizei auszuschreiben.

(3) Angehörige des Lehr- und Trainingspersonals sollen ihren Erholungsurlaub in der vorlesungsfreien Zeit nehmen.
Sie können dabei den Zeitraum des Erholungsurlaubs unter Berücksichtigung dienstlicher Belange selbst bestimmen.

(4) Zur Erfüllung ihrer Forschungsaufgaben können an der Hochschule der Polizei Dienstposten in der Laufbahn des Dienstes als akademische Rätin oder als akademischer Rat eingerichtet werden, wenn Art und Umfang wissenschaftlicher Dienstleistungen dies erfordern und eine Übertragung dieser Aufgaben auf das Lehrpersonal oder die Lehrbeauftragten nach § 16 nicht möglich ist.
Das Nähere regeln die allgemeinen beamtenrechtlichen Vorschriften über die Laufbahnen, die insoweit auch für Bewerberinnen und Bewerber in einer Laufbahn des Polizeivollzugsdienstes anzuwenden sind.

#### § 13 Freiheit der Lehre, Lehrverpflichtung

(1) Das Lehrpersonal ist verpflichtet, Lehrveranstaltungen in seiner jeweiligen Fachrichtung in dem durch die Lehrverpflichtungsordnung vorgesehenen Umfang abzuhalten, diese Lehrveranstaltungen zu evaluieren, an wissenschaftlichen Dienstleistungen mitzuarbeiten, Prüfungen abzunehmen, Studierende zu beraten und an der Verwaltung der Hochschule der Polizei mitzuwirken.

(2) Bei der Vermittlung von Studieninhalten hat das Lehrpersonal das Recht, Lehrveranstaltungen inhaltlich und methodisch frei zu gestalten sowie wissenschaftliche Lehrmeinungen darzulegen.

(3) Professorinnen und Professoren nehmen die der Hochschule obliegenden Forschungsaufgaben nach näherer Ausgestaltung ihres Dienstverhältnisses selbstständig wahr.

(4) Soweit der Hochschule der Polizei Entscheidungen zu Nebentätigkeiten übertragen wurden, erfolgen diese nach Maßgabe des Landesbeamtengesetzes.

#### § 14 Berufung von Professorinnen und Professoren

(1) Zur Professorin oder zum Professor kann berufen werden, wer die allgemeinen dienstrechtlichen Voraussetzungen erfüllt und mindestens Folgendes nachweist:

1.  ein abgeschlossenes Hochschulstudium,
2.  pädagogische Eignung,
3.  besondere Befähigung zu wissenschaftlicher Arbeit, nachgewiesen in der Regel durch eine qualifizierte Promotion, und
4.  Leistungen in Form von
    1.  besonderen Leistungen bei der Anwendung oder Entwicklung wissenschaftlicher Erkenntnisse und Methoden in einer mindestens dreijährigen beruflichen Praxis, von der mindestens zwei Jahre außerhalb des Hochschulbereichs ausgeübt worden sein müssen, oder
    2.  zusätzlichen wissenschaftlichen Leistungen, soweit dies in begründeten Ausnahmefällen bezogen auf die Anforderungen der Stelle sachgerecht ist.

(2) Stellen für Professorinnen und Professoren sind von der Hochschule der Polizei im Einvernehmen mit dem für Inneres zuständigen Ministerium öffentlich auszuschreiben.
Dabei müssen Art und Umfang der zu erfüllenden Aufgaben beschrieben werden.
Soll ein befristetes Dienstverhältnis nach Fristablauf fortgesetzt werden, bedarf es keines erneuten Berufungsverfahrens, wenn die Stelle vor der befristeten Besetzung unbefristet ausgeschrieben war oder die Professorin oder der Professor den Ruf einer anderen Hochschule erhalten hat.

(3) Die zusätzlichen wissenschaftlichen Leistungen nach Absatz 1 Nummer 4 Buchstabe a werden im Rahmen einer Juniorprofessur, im Rahmen einer Tätigkeit als Akademische Mitarbeiterin oder Akademischer Mitarbeiter an einer Hochschule oder einer außeruniversitären Forschungseinrichtung oder im Rahmen einer wissenschaftlichen Tätigkeit in der Wirtschaft oder in einem anderen gesellschaftlichen Bereich im In- und Ausland erbracht oder durch eine Habilitation nachgewiesen.
Die Qualität der für die Besetzung einer Professur erforderlichen zusätzlichen wissenschaftlichen Leistungen wird ausschließlich und umfassend im Berufungsverfahren bewertet.
Das Brandenburgische Berufsqualifikationsfeststellungsgesetz findet keine Anwendung.

(4) Zur Vorbereitung des Berufungsvorschlags wird eine Berufungskommission gebildet.
Die Mitglieder der Berufungskommission werden von der Präsidentin oder dem Präsidenten nach Anhörung des Senats bestimmt.
Der Berufungskommission sollen auch hochschulexterne sachverständige Personen angehören.

(5) Der Berufungsvorschlag soll mindestens die Namen von drei Bewerberinnen oder Bewerbern in einer Rangfolge enthalten.
Dem Berufungsvorschlag sollen mindestens zwei vergleichende Gutachten von auf dem Berufungsgebiet anerkannten, hochschulexternen Wissenschaftlerinnen oder Wissenschaftlern beigefügt werden.

(6) Professorinnen und Professoren werden auf Vorschlag des Senats von dem für Inneres zuständigen Mitglied der Landesregierung berufen.
Eine Bindung an die im Berufungsvorschlag genannte Rangfolge besteht nicht.
Wird keine vorgeschlagene Bewerberin oder kein vorgeschlagener Bewerber berufen, ist ein neuer Vorschlag einzureichen.

(7) Mit der Berufung zur Professorin oder zum Professor ist die akademische Bezeichnung „Professorin“ oder „Professor“ verliehen.
Die dienstrechtliche Stellung und das Führen der Bezeichnung „Professorin“ oder „Professor“ richtet sich nach den §§ 43, 48 Absatz 2 des Brandenburgischen Hochschulgesetzes.

#### § 15 Bestellung von Lehrkräften

(1) Beamtinnen und Beamte des höheren oder gehobenen Dienstes oder Beschäftigte mit vergleichbarer Befähigung können zu hauptamtlichen Lehrkräften bestellt werden, wenn sie

1.  ein Studium an einer Hochschule oder einer Fachhochschule abgeschlossen haben,
2.  über eine in der Regel mindestens dreijährige Berufserfahrung, davon mindestens zwei Jahre außerhalb der Hochschule der Polizei, verfügen und
3.  die erforderlichen pädagogischen Fähigkeiten besitzen.

Die Hochschulleitung und die Dekanin oder der Dekan können Lehr- und Prüfungsaufgaben ohne Bestellung wahrnehmen.

(2) Bei der Ausbildung für den mittleren Polizeivollzugsdienst und für den Aufstieg in den gehobenen Polizeivollzugsdienst sowie bei Lehraufgaben im Studium zur Vermittlung von überwiegend praktischen Fertigkeiten und Kenntnissen und in der Weiterbildung kann der nach Absatz 1 Satz 1 Nummer 1 erforderliche Studienabschluss durch besondere fachbezogene Leistungen ersetzt werden.

(3) Die Bestellung zur Lehrkraft erfolgt in der Regel für die Dauer von fünf Jahren.
Lehrkräfte des höheren Dienstes werden durch das für Inneres zuständige Ministerium nach Anhörung des Senats bestellt.
Jeder Wiederbestellung soll eine Tätigkeit von mindestens sechs Monaten außerhalb der Hochschule der Polizei vorausgehen.
Soll eine Lehrkraft aus einem Beschäftigtenverhältnis heraus bestellt werden, kann sie ihre Aufgaben in diesem ausüben.

(4) Dienstposten für Lehrkräfte sind durch die Hochschule der Polizei auszuschreiben, Dienstposten für Lehrkräfte des höheren Dienstes im Einvernehmen mit dem für Inneres zuständigen Ministerium.
Wird eine Person übergangsweise bis zur endgültigen Besetzung einer Professorenstelle oder zur Vertretung von Lehrpersonal wegen Mutterschutz oder Elternzeit zur Lehrkraft bestellt, ist dafür eine Ausschreibung entbehrlich.

#### § 16 Lehraufträge

(1) Zur Ergänzung des Lehrangebotes und für einen durch das Lehrpersonal nicht gedeckten Lehrbedarf können Lehraufträge erteilt werden.
Der Lehrauftrag begründet ein öffentlich-rechtliches Rechtsverhältnis besonderer Art zur Hochschule, jedoch kein Dienstverhältnis.

(2) Lehrbeauftragte nehmen die ihnen übertragenen Lehraufgaben selbstständig wahr.
Hierfür müssen sie nach Eignung, Befähigung und fachlicher Leistung den Anforderungen an eine Lehrtätigkeit an der Hochschule der Polizei entsprechen.

#### § 17 Personalvertretung

Das Landespersonalvertretungsgesetz gilt an der Hochschule der Polizei mit folgenden Maßgaben:

1.  Abweichend von § 13 Absatz 4 Satz 2 des Landespersonalvertretungsgesetzes sind die Anwärterinnen und Anwärter an der Hochschule der Polizei auch dann wahlberechtigt, wenn sie im Rahmen ihrer Ausbildung außerhalb der Hochschule der Polizei tätig sind.
2.  Abweichend von § 14 Absatz 1 des Landespersonalvertretungsgesetzes sind die Anwärterinnen und Anwärter an der Hochschule der Polizei nicht für die Personalvertretung wählbar.
3.  Die Altersbeschränkungen in § 77 Nummer 1 und 2 des Landespersonalvertretungsgesetzes finden bei der Wahl zur Jugend- und Auszubildendenvertretung sowie bei der Wahl zur Jugend- und Auszubildendenstufenvertretung keine Anwendung.
4.  Mitbestimmung und Mitwirkung finden nicht statt bei Maßnahmen, die der Entscheidung des Senats unterliegen.
    In diesen Fällen ist die Personalvertretung anzuhören.
5.  § 90 des Landespersonalvertretungsgesetzes findet an der Hochschule der Polizei keine Anwendung.

#### § 18 Einschränkung von Grundrechten

Durch dieses Gesetz wird das Grundrecht der Berufsfreiheit (Artikel 49 Absatz 1 der Verfassung des Landes Brandenburg) eingeschränkt.

#### § 19 Übergangsregelungen

(1) Abweichend von § 10 Absatz 4 sind die beiden Hochschulleitungsämter denjenigen Personen zu übertragen, die diese Ämter zum Zeitpunkt des Inkrafttretens dieses Gesetzes innehaben.
Auf die Probezeit werden die Zeiten angerechnet, in denen die jeweiligen Ämter bereits ausgeübt wurden.
Bei Bestellung aus einem Beschäftigtenverhältnis heraus, können die Ämter auch in dieser Form ausgeübt werden.

(2) Die aufgrund dieses Gesetzes anzupassenden satzungsrechtlichen Regelungen gelten bis zu deren Änderung fort.