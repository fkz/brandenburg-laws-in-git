## Gesetz über die soziale Wohnraumförderung im Land Brandenburg (Brandenburgisches Wohnraumförderungsgesetz - BbgWoFG)

Der Landtag hat das folgende Gesetz beschlossen:

**Inhaltsübersicht**

### Abschnitt 1  
Allgemeines

[§ 1 Anwendungsbereich](#1)

[§ 2 Ziele, Zielgruppen](#2)

[§ 3 Begriffsbestimmungen](#3)

[§ 4 Beteiligung der Gemeinden; Verordnungsermächtigung](#4)

### Abschnitt 2  
Förderung

[§ 5 Fördergrundsätze](#5)

[§ 6 Fördermittel und Rückflüsse](#6)

[§ 7 Förderprogramme und Verfahren](#7)

[§ 8 Fördergegenstände](#8)

[§ 9 Förderinstrumente](#9)

[§ 10 Förderempfängerinnen und Förderempfänger](#10)

[§ 11 Förderzusage](#11)

[§ 12 Kooperationsvertrag](#12)

### Abschnitt 3  
Bindungen und Sicherung der Zweckbestimmung

[§ 13 Belegungsbindung](#13)

[§ 14 Wohnberechtigungsschein](#14)

[§ 15 Mietbindung](#15)

[§ 16 Dauer der Bindungen](#16)

[§ 17 Freistellung und Änderung von Bindungen](#17)

[§ 18 Sicherung der Zweckbestimmung](#18)

[§ 19 Mitteilungspflichten](#19)

[§ 20 Besondere Wohnformen](#20)

[§ 21 Datenverarbeitung](#21)

### Abschnitt 4  
Einkommen

[§ 22 Einkommensgrenzen; Verordnungsermächtigung](#22)

[§ 23 Maßgebendes Einkommen](#23)

[§ 24 Einkommensermittlung](#24)

[§ 25 Zeitraum für die Ermittlung des Einkommens](#25)

[§ 26 Frei- und Abzugsbeträge](#26)

### Abschnitt 5  
Ergänzungs- und Schlussvorschriften

[§ 27 Zuständigkeiten; Verordnungsermächtigung](#27)

[§ 28 Maßnahmen bei Verstößen](#28)

[§ 29 Übergangsvorschrift](#29)

[§ 30 Inkrafttreten](#30)

### Abschnitt 1  
Allgemeines

#### § 1 Anwendungsbereich

Dieses Gesetz regelt die Förderung des Wohnungsbaus und anderer Maßnahmen durch das Land zur Unterstützung von Haushalten bei der Versorgung mit Mietwohnraum und bei der Bildung von selbst genutztem Wohneigentum (soziale Wohnraumförderung).

#### § 2 Ziele, Zielgruppen

(1) Ziel der sozialen Wohnraumförderung ist die Unterstützung von Haushalten, die sich am Markt nicht angemessen mit Wohnraum versorgen können, bei der Versorgung mit Mietwohnraum einschließlich Genossenschaftswohnraum oder mit selbst genutztem Wohneigentum.

(2) Weitere Ziele der sozialen Wohnraumförderung sind die Erhaltung und Schaffung stabiler Wohn- und Nachbarschaftsverhältnisse, Bewohner- und Quartiersstrukturen.

(3) Die soziale Wohnraumförderung unterstützt den effizienten Einsatz und Verbrauch von Energie bei Wohngebäuden als Beitrag zum Klimaschutz.

(4) Zielgruppen der sozialen Wohnraumförderung von Miet- und Genossenschaftswohnraum sind Haushalte nach Absatz 1, insbesondere mit Kindern, älteren Menschen, Menschen mit Behinderungen, Personen in sozialen Notlagen, Studierenden und Auszubildenden.

(5) Zielgruppen der sozialen Wohnraumförderung von selbst genutztem Wohneigentum sind Haushalte nach Absatz 1, insbesondere mit Kindern und Menschen mit Behinderungen.

#### § 3 Begriffsbestimmungen

(1) Wohnraum ist umbauter Raum, der tatsächlich und rechtlich zur dauernden Wohnnutzung geeignet und von der verfügungsberechtigten Person dazu bestimmt ist.
Wohnraum können Wohnungen oder einzelne Wohnräume sein.
Die Größe des Wohnraums muss angemessen sein.
Dabei ist den Besonderheiten bei baulichen Maßnahmen in bestehendem Wohnraum sowie besonderen persönlichen und beruflichen Bedürfnissen des Haushalts, insbesondere von älteren Menschen und Menschen mit Behinderungen, Rechnung zu tragen.
Die Berechnung der Wohnfläche von Wohnraum bestimmt sich nach der Wohnflächenverordnung vom 25.
November 2003 (BGBl. I S. 2346) in der jeweils geltenden Fassung.

(2) Selbst genutztes Wohneigentum ist Wohnraum im eigenen Haus oder in einer eigenen Eigentumswohnung, der zu eigenen Wohnzwecken genutzt wird.

(3) Mietwohnraum ist Wohnraum, der den dort Wohnenden aufgrund eines Mietverhältnisses oder eines genossenschaftlichen oder sonstigen ähnlichen Nutzungsverhältnisses zum Gebrauch überlassen wird.

(4) Wohnraum gilt als bezugsfertig, wenn den künftigen Bewohnerinnen und Bewohnern zugemutet werden kann, ihn zu beziehen.

(5) Wohnungsbau ist das Schaffen von Wohnraum durch

1.  Neubau,
2.  Beseitigung von Schäden an Gebäuden unter wesentlichem Bauaufwand, durch die die Gebäude auf Dauer wieder zu Wohnzwecken nutzbar gemacht werden (Wiederaufbau),
3.  Änderung oder Erweiterung von Gebäuden unter wesentlichem Bauaufwand oder
4.  Änderung von Wohnraum unter wesentlichem Bauaufwand zur Anpassung an geänderte Wohnbedürfnisse.

(6) Modernisierung sind bauliche Maßnahmen, die

1.  den Gebrauchswert des Wohnraums oder des Wohngebäudes erhöhen,
2.  die allgemeinen Wohnverhältnisse auf Dauer verbessern oder
3.  Einsparungen von Energie und Wasser bewirken.

(7) Ersterwerb ist der erstmalige Erwerb von Wohnraum innerhalb von zwei Jahren nach Fertigstellung.

(8) Zum Haushalt gehören alle Personen, die miteinander eine Wohn- und Wirtschaftsgemeinschaft bilden (Haushaltsangehörige).
Zum Haushalt gehören auch Personen, wenn zu erwarten ist, dass diese alsbald und auf Dauer in den Haushalt aufgenommen werden, sowie Kinder, deren Geburt aufgrund einer bestehenden Schwangerschaft zu erwarten ist.

(9) Belegungsrechte werden als allgemeine Belegungsrechte, Benennungsrechte oder Besetzungsrechte (Art der Bindung) begründet.
Belegungsrechte können

1.  an den geförderten Wohnungen (unmittelbare Belegung),
2.  nur an anderen Wohnungen (mittelbare Belegung),
3.  an geförderten und an anderen Wohnungen (verbundene Belegung)

begründet werden (Gegenstand der Bindung).

(10) Ein allgemeines Belegungsrecht ist das Recht der zuständigen Stelle, von der verfügungsberechtigten Person zu fordern, den Wohnraum einer wohnungssuchenden Person, die ihre Wohnberechtigung durch Vorlage eines Wohnberechtigungsscheins nach § 14 nachweist, zu überlassen.

(11) Benennungsrecht ist das Recht der zuständigen Stelle, der verfügungsberechtigten Person für die Vermietung eines bestimmten belegungsgebundenen Wohnraums mindestens drei berechtigte wohnungssuchende Personen zur Auswahl zu benennen.

(12) Besetzungsrecht ist das Recht der zuständigen Stelle, eine wohnungssuchende Person zu benennen, der die verfügungsberechtigte Person einen bestimmten belegungsgebundenen Wohnraum zu überlassen hat.

(13) Verfügungsberechtigte Person ist die Person, in deren Eigentum sich der Wohnraum befindet oder die sonst aufgrund eines privaten dinglichen Rechts zum Besitz des Wohnraums berechtigt ist.
Der verfügungsberechtigten Person stehen eine von ihr beauftragte Person sowie die Vermieterin oder der Vermieter gleich.

(14) Bauherrin oder Bauherr ist diejenige oder derjenige, die oder der das Bauvorhaben auf eigene oder fremde Rechnung im eigenen Namen durchführt oder durch Dritte durchführen lässt.

#### § 4 Beteiligung der Gemeinden; Verordnungsermächtigung

(1) Die Förderung nach diesem Gesetz ist Aufgabe des Landes.
Die Gemeinden und Gemeindeverbände können mit eigenen Mitteln eine Förderung nach diesem Gesetz durchführen.

(2) Die wohnungspolitischen Belange der Gemeinden und der Gemeindeverbände sollen bei der sozialen Wohnraumförderung berücksichtigt werden; dies gilt insbesondere, wenn diese sich mit eigenen Mitteln an der Förderung beteiligen oder Wohnraumkonzepte vorlegen.
Vor der Entscheidung über die Vergabe der Fördermittel ist die örtliche Gemeinde zu beteiligen.

(3) Das für Wohnungswesen zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung Gebiete mit erhöhtem Wohnungsbedarf zu bestimmen.
In einem Gebiet nach Satz 1 besteht ein Benennungsrecht der zuständigen Stelle nach § 3 Absatz 11.
In der Verordnung können Bestimmungen getroffen werden, nach welchen Kriterien die Benennung erfolgen soll.

### Abschnitt 2  
Förderung

#### § 5 Fördergrundsätze

Bei der Förderung sollen insbesondere berücksichtigt werden

1.  die örtlichen und regionalen wohnungswirtschaftlichen Verhältnisse sowie die unterschiedlichen Investitionsbedingungen der Bauherrin oder des Bauherrn,
2.  die besonderen Anforderungen des zu versorgenden Personenkreises, insbesondere die des barrierefreien Bauens,
3.  der Beitrag des genossenschaftlichen Wohnens zur Erreichung der Ziele der Wohnraumförderung,
4.  die Schaffung und Erhaltung sozial stabiler Bewohnerstrukturen,
5.  die Schaffung und Erhaltung ausgewogener Siedlungsstrukturen sowie ausgeglichener wirtschaftlicher, sozialer und kultureller Verhältnisse,
6.  die funktional sinnvolle Zuordnung der Wohnbereiche zu den Arbeitsplätzen und der Infrastruktur sowie die ausreichende Anbindung des zu fördernden Wohnraums an den öffentlichen Personennahverkehr,
7.  die Nutzung des Wohnungs- und Gebäudebestandes,
8.  die Erhaltung preisgünstigen Wohnraums im Fall der Förderung der Modernisierung und
9.  der Einsatz kostensparender, energieeffizienter, insbesondere ökologischer und ressourcenschonender Bauweisen.

#### § 6 Fördermittel und Rückflüsse

(1) Die soziale Wohnraumförderung durch das Land erfolgt aus dem Wohnungsbauvermögen des Landes Brandenburg.
Zur Höhe des Neubewilligungsvolumens wird auf § 1 Absatz 3 Satz 4 des Gesetzes über das Wohnungsbauvermögen des Landes Brandenburg vom 17. Dezember 1996 (GVBl. I S. 358, 362), das zuletzt durch Artikel 3 des Gesetzes vom 15.
Oktober 2018 (GVBl. I Nr. 25 S. 10) geändert worden ist, in der jeweils geltenden Fassung verwiesen.

(2) Geldleistungen und Ausgleichzahlungen nach § 17 Absatz 2, § 18 Absatz 2 Satz 1 Nummer 3 und § 28 Absatz 1 fließen in das Wohnungsbauvermögen des Landes Brandenburg und sind zweckgebunden für die soziale Wohnraumförderung zu verwenden.

#### § 7 Förderprogramme und Verfahren

(1) Das für Wohnungswesen zuständige Ministerium erlässt Verwaltungsvorschriften über Voraussetzungen der Förderung und deren Durchführung.
Näheres regeln § 44 der Brandenburgischen Landeshaushaltsordnung und die hierzu erlassenen Verwaltungsvorschriften.

(2) Die Förderung erfolgt über einen angemessenen Zeitraum in einem offenen, transparenten und diskriminierungsfreien Verfahren.
Sie muss dem Förderzweck angemessen sein.

#### § 8 Fördergegenstände

Insbesondere können gefördert werden

1.  Wohnungsbau, einschließlich des Ersterwerbs,
2.  Modernisierung von Wohnraum,
3.  Erwerb bestehenden Wohnraums,
4.  Instandsetzungen in Verbindung mit Modernisierungsmaßnahmen,
5.  Maßnahmen zum Abbau von Barrieren bei bestehendem Wohnraum oder
6.  Erwerb von Belegungsrechten an bestehendem Wohnraum.

#### § 9 Förderinstrumente

Die Förderung kann erfolgen durch

1.  Gewährung von vergünstigten Darlehen,
2.  Gewährung von Zuschüssen,
3.  Übernahme von Bürgschaften oder
4.  sonstige geldwerte Leistungen.

#### § 10 Förderempfängerinnen und Förderempfänger

(1) Empfängerin oder Empfänger der Förderung ist

1.  beim Wohnungsbau und der Modernisierung die Bauherrin oder der Bauherr,
2.  beim Ersterwerb oder Erwerb bestehenden Wohnraums die Erwerberin oder der Erwerber oder
3.  beim Erwerb von Belegungsrechten die verfügungsberechtigte Person.

(2) Die Gewährung von Fördermitteln setzt voraus, dass die Förderempfängerin oder der Förderempfänger

1.  die erforderliche Leistungsfähigkeit und Zuverlässigkeit besitzt,
2.  die Gewähr für eine ordnungsmäßige und wirtschaftliche Durchführung des Bauvorhabens und für eine ordnungsmäßige Verwaltung des Wohnraums bietet,
3.  eine angemessene Eigenleistung erbringt,
4.  bei der Förderung von selbst genutztem Wohneigentum die Belastung auf Dauer tragen kann und
5.  in den Fällen des Absatzes 1 Nummer 1 Eigentum oder eine Erbbauberechtigung an einem geeigneten Baugrundstück hat oder nachweist, dass der Erwerb eines derartigen Grundstücks oder Erbbaurechts gesichert ist oder durch die Gewährung der Fördermittel gesichert wird.

#### § 11 Förderzusage

(1) Fördermittel werden auf Antrag durch eine Förderzusage von der Bewilligungsstelle gewährt.
Die Förderzusage erfolgt durch öffentlich-rechtlichen Vertrag oder durch Verwaltungsakt und bedarf der Schriftform.

(2) Die Förderzusage muss folgende Bestimmungen enthalten:

1.  Fördergegenstand,
2.  Höhe der Förderung,
3.  Art des Fördermittels sowie dessen Verzinsung und Tilgung im Darlehensfall,
4.  maßgebliche Einkommensgrenzen,
5.  Bindungen nach § 3 Absatz 9 bis 12, §§ 13, 15 und 16 und
6.  Rechtsfolgen eines Eigentumswechsels.

Weitere, für den jeweiligen Förderzweck erforderliche Bestimmungen können in die Förderzusage aufgenommen werden.

(3) Die Förderzusage wirkt auch für und gegen eine Rechtsnachfolgerin oder einen Rechtsnachfolger.

(4) Ein Anspruch auf Förderung besteht nicht.

#### § 12 Kooperationsvertrag

(1) Ein Kooperationsvertrag ist eine Vereinbarung zwischen der Gemeinde oder dem Gemeindeverband und der verfügungsberechtigten Person über Angelegenheiten der örtlichen Wohnraumversorgung.
Weitere öffentliche und private Partnerinnen oder Partner können beteiligt werden.
Ein Kooperationsvertrag kann eine Förderzusage enthalten.

(2) Ziel von Kooperationsverträgen ist die Verbesserung der Wohnraumversorgung, des Wohnumfelds und des Wohnquartiers durch die Zusammenarbeit der in Absatz 1 genannten Beteiligten.
Außer den in diesem Gesetz geregelten Fördergegenständen und Bindungen nach § 3 Absatz 9 bis 12, §§ 13, 15 und 16 können weitere Vertragsgegenstände vereinbart werden, soweit sie den in § 2 genannten Zielen entsprechen.

(3) Ein Kooperationsvertrag bedarf der Schriftform, soweit nicht durch Rechtsvorschriften eine andere Form vorgeschrieben ist.

### Abschnitt 3  
Bindungen und Sicherung der Zweckbestimmung

#### § 13 Belegungsbindung

Die verfügungsberechtigte Person darf Mietwohnraum nach Maßgabe der Förderzusage einer wohnungssuchenden Person überlassen, wenn sich deren Wohnberechtigung aus einem Wohnberechtigungsschein nach § 14 ergibt.
Der Wohnberechtigungsschein ist von der wohnungssuchenden Person vorzulegen.
Das gleiche gilt, wenn sich die Wohnberechtigung aus einer Benennung oder Besetzung der zuständigen Stelle ergibt.

#### § 14 Wohnberechtigungsschein

(1) Der Wohnberechtigungsschein wird einer wohnungssuchenden Person auf Antrag von der zuständigen Stelle für die Dauer eines Jahres erteilt, wenn der Haushalt die Einkommensgrenze nach § 22 nicht überschreitet.
Ist mit einer Änderung der Einkommensverhältnisse offensichtlich nicht zu rechnen, kann in begründeten Einzelfällen der Wohnberechtigungsschein für eine Dauer von zwei Jahren ausgestellt werden.
Antragsberechtigt sind Wohnungssuchende, die sich nicht nur vorübergehend im Geltungsbereich des Grundgesetzes aufhalten und rechtlich und tatsächlich in der Lage sind, für sich und ihre Haushaltsangehörigen auf längere Dauer einen Wohnsitz als Mittelpunkt der Lebensbeziehungen zu begründen.
Der Wohnberechtigungsschein gilt nur für Wohnraum im Land Brandenburg.
Ein in einem anderen Land der Bundesrepublik Deutschland ausgestellter Wohnberechtigungsschein wird nicht anerkannt.

(2) Im Wohnberechtigungsschein ist die für die wohnungssuchende Person und ihre Haushaltsangehörigen angemessene Wohnungsgröße nach der Raumzahl oder der Wohnfläche anzugeben.
Gehört die wohnungssuchende oder eine haushaltsangehörige Person einem bestimmten von der Förderung begünstigten Personenkreis an, so ist das in den Wohnberechtigungsschein aufzunehmen.

(3) Die zuständige Stelle kann bei der Erteilung eines Wohnberechtigungsscheins und einer Benennung abweichen von

1.  der Einkommensgrenze des Haushalts,
    1.  um eine besondere Härte für die wohnungssuchende Person zu vermeiden oder
    2.  wenn die wohnungssuchende Person anderen geförderten Wohnraum freimacht, dessen Miete je Quadratmeter Wohnfläche niedriger ist oder dessen Größe die für die wohnungssuchende Person maßgebliche Wohnungsgröße überschreitet,oder
2.  der angemessenen Größe des Wohnraums, um
    1.  besondere persönliche oder berufliche Bedürfnisse der wohnungssuchenden oder einer haushaltsangehörigen Person zu berücksichtigen oder
    2.  einen nach der Lebenserfahrung in absehbarer Zeit zu erwartenden zusätzlichen Raumbedarf zu berücksichtigen oder
    3.  eine besondere Härte für die wohnungssuchende Person zu vermeiden.

(4) Die Erteilung des Wohnberechtigungsscheins ist zu versagen, wenn sie auch bei Einhaltung der maßgeblichen Einkommensgrenze offensichtlich nicht gerechtfertigt wäre.

(5) Ist die Inhaberin oder der Inhaber des Wohnberechtigungsscheins aus einer Wohnung ausgezogen, so darf die verfügungsberechtigte Person die Wohnung den Haushaltsangehörigen nur nach Maßgabe der Absätze 1 bis 4 zum Gebrauch überlassen.
Eine neue Gebrauchsüberlassung im Sinne der Absätze 1 bis 4 liegt nicht vor, wenn die Wohnung weiterhin von der Ehegattin oder dem Ehegatten, der Lebenspartnerin oder dem Lebenspartner einer eingetragenen Lebensgemeinschaft oder der Partnerin oder dem Partner einer sonstigen auf Dauer angelegten Lebensgemeinschaft bewohnt wird.
Personen, die nach dem Tod der oder des Wohnberechtigungsscheininhabenden nach § 563 Absatz 1 bis 3 des Bürgerlichen Gesetzbuches in das Mietverhältnis eingetreten sind, dürfen die Wohnung auch ohne Übergabe eines Wohnberechtigungsscheins weiter bewohnen.

#### § 15 Mietbindung

(1) Die verfügungsberechtigte Person darf Wohnraum nicht gegen eine höhere als die in der Förderzusage festgelegte höchstzulässige Miete überlassen.
Bestimmungen über die höchstzulässige Miete dürfen nicht zum Nachteil der Mieterin oder des Mieters von den allgemeinen mietrechtlichen Vorschriften abweichen.

(2) Die verfügungsberechtigte Person darf fordern, sich versprechen lassen oder annehmen:

1.  eine Leistung zur Abgeltung von Betriebskosten nur nach Maßgabe der §§ 556, 556a und 560 des Bürgerlichen Gesetzbuches und
2.  eine einmalige oder sonstige Nebenleistung nur insoweit, als sie nach den Bestimmungen der Förderzusage zugelassen ist.

(3) Förderbestimmungen der Förderzusage zur Mietbindung und deren Dauer sind im Mietvertrag wiederzugeben.
Mietrechtliche Vereinbarungen dürfen nicht zum Nachteil der Mieterin oder des Mieters von den Förderbestimmungen zur Mietbindung abweichen.
Die Mieterin oder der Mieter kann sich gegenüber der verfügungsberechtigten Person auf diese Bestimmungen berufen.

(4) Die verfügungsberechtigte Person kann die Miete nach Maßgabe der Förderzusage und der allgemeinen mietrechtlichen Vorschriften erhöhen.

#### § 16 Dauer der Bindungen

(1) Die Dauer der Belegungs- und Mietbindungen nach den §§ 13 und 15 Absatz 1 ist in der Förderzusage durch Festlegung einer Frist zu bestimmen.
Bei der Förderung durch Darlehen ist die Bindungsdauer auch für den Fall vorzeitiger vollständiger Rückzahlung zu regeln.

(2) Für den Fall der Rückforderung wegen Verstoßes gegen die Regelungen der Förderzusage bleiben die Bindungen bei Darlehen und Zuschüssen bis zu dem in der Förderzusage bestimmten Ende der Bindungsdauer, längstens jedoch bis zum Ablauf des zwölften Kalenderjahrs nach dem Jahr der Rückzahlung, bestehen.

(3) Im Falle der Zwangsversteigerung enden die Bindungen bei

1.  Darlehen mit dem in der Förderzusage bestimmten Zeitpunkt, spätestens jedoch mit dem Ablauf des dritten Kalenderjahres nach dem Kalenderjahr, in dem der Zuschlag erteilt worden ist und die aufgrund der Darlehensförderung begründeten Grundpfandrechte mit dem Zuschlag erloschen sind; ist das Darlehen bereits vor dem Zuschlag zurückgezahlt, verbleibt es bei den Regelungen der Förderzusage nach Absatz 1 und
2.  Zuschüssen mit dem Zuschlag.

#### § 17 Freistellung und Änderung von Bindungen

(1) Die zuständige Stelle kann die verfügungsberechtigte Person auf Antrag befristet von Bindungen nach den §§ 13 und 15 Absatz 1 freistellen, soweit

1.  nach den örtlichen wohnungswirtschaftlichen Verhältnissen ein überwiegendes öffentliches Interesse an der Aufrechterhaltung der Bindungen nicht besteht,
2.  an der Freistellung ein überwiegendes öffentliches Interesse besteht, insbesondere wenn die Freistellung der Schaffung oder Erhaltung sozial stabiler Bewohnerstrukturen dient, oder
3.  an der Freistellung ein überwiegendes berechtigtes Interesse der verfügungsberechtigten oder einer dritten Person besteht.

Freistellungen können für bestimmten Wohnraum, für Wohnraum bestimmter Art oder für Wohnraum in bestimmten Gebieten erteilt werden.
Ein Anspruch auf Freistellung besteht nicht.

(2) Für die Freistellung ist ein angemessener Ausgleich zu leisten, indem der hierfür zuständigen Stelle Belegungs- und Mietbindungen für Ersatzwohnraum für die Dauer der Freistellung vertraglich eingeräumt werden oder ein Geldausgleich oder ein sonstiger angemessener Ausgleich geleistet wird.
Bei einer Freistellung nach Absatz 1 Satz 1 Nummer 1 und 2 kann von einem Ausgleich abgesehen werden.

(3) Die Bewilligungsstelle kann Wohnraum vertraglich aus den Belegungs- und Mietbindungen entlassen oder die Belegungs- und Mietbindungen vertraglich ändern, wenn

1.  dies der Schaffung oder Erhaltung sozial stabiler Bewohnerstrukturen dient oder aus anderen örtlichen wohnungswirtschaftlichen Gründen geboten ist und
2.  an anderem bezugsfertigen und freien Wohnraum Belegungs- und Mietbindungen von insgesamt gleichem Wert eingeräumt werden.

Im Fall des Satzes 1 Nummer 2 gilt mit dem Zeitpunkt der Entlassung aus den Bindungen der andere Wohnraum als geförderter Wohnraum.

(4) Enthält die Förderzusage eine Bindung zugunsten bestimmter Haushalte, kann die zuständige Stelle von dieser abweichen, wenn der geförderte Wohnraum zu diesem Zeitpunkt von der jeweiligen Gruppe nicht nachgefragt wird.

#### § 18 Sicherung der Zweckbestimmung

(1) In die Förderung einbezogener Mietwohnraum darf nicht zu anderen Zwecken als zur Vermietung als Wohnraum genutzt werden.
Wer Wohnraum entgegen der Bestimmung in der Förderzusage zweckentfremdet, hat auf Verlangen der hierfür zuständigen Stelle die Zweckentfremdung zu beenden.

(2) Die zuständige Stelle genehmigt auf Antrag eine Ausnahme von Absatz 1

1.  zur Selbstnutzung durch die verfügungsberechtigte Person, wenn die Voraussetzungen für die Erteilung eines Wohnberechtigungsscheins nach § 14 vorliegen,
2.  zum Leerstand, wenn und solange eine Vermietung nicht möglich ist und dem Förderzweck nicht auf andere Weise, auch nicht durch Freistellung oder Übertragung von Belegungsbindungen, entsprochen werden kann; einer Genehmigung bedarf es nicht, wenn der Wohnraum weniger als drei Monate leer steht,
3.  nach Ermessen, wenn Wohnraum aus überwiegendem öffentlichen Interesse oder überwiegendem berechtigten Interesse der verfügungsberechtigten oder einer dritten Person anderen als Wohnzwecken zugeführt oder entsprechend baulich geändert werden soll; die zuständige Stelle kann einen angemessenen Geldausgleich oder die Einräumung von Belegungsbindungen an anderem Wohnraum von insgesamt gleichem Wert verlangen.

Wer bauliche Änderungen des Wohnraums nach Satz 1 Nummer 3 ohne Genehmigung vornimmt, hat auf Verlangen der zuständigen Stelle dessen Eignung für Wohnzwecke auf seine Kosten wiederherzustellen.

(3) Wird Wohnraum an eine nicht berechtigte wohnungssuchende Person vermietet, hat die Vermieterin oder der Vermieter auf Verlangen der zuständigen Stelle das Mietverhältnis unter Berücksichtigung der geltenden mietrechtlichen Vorschriften zu beenden und den Wohnraum an eine berechtigte wohnungssuchende Person zu überlassen.

(4) Selbst genutztes Wohneigentum darf entsprechend den in der Förderzusage geregelten Bestimmungen genutzt werden.

#### § 19 Mitteilungspflichten

(1) Sobald voraussehbar ist, dass Wohnraum mit Belegungsbindung bezugsfertig oder frei wird, hat die verfügungsberechtigte Person der zuständigen Stelle unverzüglich schriftlich oder in elektronischer Form den voraussichtlichen Zeitpunkt der Bezugsfertigkeit oder des Freiwerdens des Wohnraums mitzuteilen.

(2) Binnen zwei Wochen, nachdem Wohnraum einer wohnungssuchenden Person überlassen wurde, hat die verfügungsberechtigte Person der hierfür zuständigen Stelle deren Namen schriftlich oder in elektronischer Form mitzuteilen und den ihr übergebenen Wohnberechtigungsschein vorzulegen.

(3) Die verfügungsberechtigte Person hat der Bewilligungsstelle die Veräußerung von belegungs- oder mietgebundenem Wohnraum und die Begründung von Wohneigentum an solchem Wohnraum unverzüglich schriftlich oder in elektronischer Form mitzuteilen.
Sofern die verfügungsberechtigte Person eine Wohnung erworben hat, an der nach der Überlassung an die Mieterin oder den Mieter Wohneigentum begründet worden ist, darf sie sich der Mieterin oder dem Mieter gegenüber auf berechtigte Interessen an der Beendigung des Mietverhältnisses im Sinne des § 573 Absatz 2 Nummer 2 des Bürgerlichen Gesetzbuches nicht berufen, solange die Wohnung Belegungs- oder Mietbindungen unterliegt.
Im Übrigen bleibt § 577a Absatz 1 und 2 des Bürgerlichen Gesetzbuches unberührt.

(4) Verfügungsberechtigte Personen, Mieterinnen und Mieter sind verpflichtet, der zuständigen Stelle oder der Bewilligungsstelle auf Verlangen Auskunft zu erteilen, Einsicht in ihre Unterlagen zu gewähren und die Besichtigung von Grundstücken, Gebäuden und Wohnraum zu gestatten, soweit dies zur Sicherung der Zweckbestimmung des Wohnraums nach § 18 Absatz 1 Satz 1 und der sonstigen Bestimmungen der Förderzusage erforderlich ist.
Das Grundrecht der Unverletzlichkeit der Wohnung (Artikel 13 des Grundgesetzes und Artikel 15 der Verfassung des Landes Brandenburg) wird insoweit eingeschränkt.

(5) Die verfügungsberechtigte Person hat auf Verlangen der Mieterin oder dem Mieter oder einer wohnungssuchenden Person Auskunft über die Miet- und Belegungsbindungen und deren Dauer zu geben.
Abweichende Vereinbarungen zum Nachteil der Mieterin oder des Mieters sind unwirksam.

(6) Die Bewilligungsstelle hat auf Antrag der verfügungsberechtigten Person und bei berechtigtem Interesse auch einer wohnungssuchenden Person und der Mieterin oder dem Mieter schriftlich oder in elektronischer Form zu bestätigen, wie lange die Belegungs- und Mietbindungen dauern.
Gegenüber der wohnungssuchenden Person und der Mieterin oder dem Mieter erfolgt die Bestätigung nur dann, wenn die verfügungsberechtigte Person sie nicht oder nur unzureichend erteilt hat.

#### § 20 Besondere Wohnformen

(1) Bei der Förderung besonderer Wohnformen kann das für Wohnungswesen zuständige Mitglied der Landesregierung zur Erreichung des besonderen Förderzwecks unter der Beachtung der allgemeinen Zielsetzung in § 1 von § 3 Absatz 8, §§ 8, 10 Absatz 1, §§ 13, 15 Absatz 1 und § 22 in den Verwaltungsvorschriften nach § 7 Absatz 1 abweichende Bestimmungen erlassen.
Dies gilt zum Beispiel für Wohnraum für Studierende, Auszubildende, ältere Menschen und Menschen mit Behinderungen sowie für Wohngemeinschaften zur gegenseitigen Unterstützung im Alter oder bei Hilfebedürftigkeit und betreute Wohngemeinschaften.

(2) Soll geförderter Wohnraum zur Erreichung eines besonderen Zwecks im Sinne des Absatzes 1 genutzt werden, kann die Bewilligungsstelle von der Förderzusage und von § 3 Absatz 8, §§ 8, 10 Absatz 1, §§ 13, 15 Absatz 1 und § 22 abweichende Bestimmungen treffen.

#### § 21 Datenverarbeitung

(1) Die zuständige Stelle und die Bewilligungsstelle haben über den Wohnraum, dessen Nutzung, die jeweiligen Mieterinnen und Mieter, Vermieterinnen und Vermieter sowie über die Belegungsrechte, die höchstzulässigen Mieten, Bindungen, Freistellungen und Ausnahmen von der Zweckbestimmung Daten zu verarbeiten, soweit dies zur Sicherung der Zweckbestimmung des Wohnraums und der sonstigen Bestimmungen der Förderzusage erforderlich ist.

(2) Soweit dies für die Förderung von Wohnraum oder zur Feststellung der Wohnberechtigung erforderlich ist und begründete Zweifel an der Richtigkeit der Angaben und vorgelegten Nachweise bestehen, haben Finanzbehörden und Arbeitgebende der zuständigen Stelle und der Bewilligungsstelle Auskunft über die Einkommensverhältnisse derjenigen Personen zu erteilen, von deren Einkommen die Förderung oder die Wohnberechtigung abhängt.
Vor einem Auskunftsersuchen an die Arbeitgebenden soll der betroffenen Person Gelegenheit zur Stellungnahme gegeben werden.

(3) Fördermittel, die in Abhängigkeit vom jeweiligen Haushaltseinkommen der Mieterinnen und Mieter gewährt werden, können auch dann an die geförderten Vermieterinnen und Vermieter ausgezahlt werden, wenn diese aus den geleisteten Zahlungen Rückschlüsse auf das Haushaltseinkommen der Mieterinnen und Mieter ziehen können.

### Abschnitt 4  
Einkommen

#### § 22 Einkommensgrenzen; Verordnungsermächtigung

(1) Begünstigte der sozialen Wohnraumförderung sind die Haushalte der Zielgruppen nach § 2, wenn ihr jährliches Gesamteinkommen die Einkommensgrenzen nach den Absätzen 2 und 3 nicht überschreitet.

(2) Die Einkommensgrenze beträgt für einen

1.  Einpersonenhaushalt 15 600 Euro,
2.  Zweipersonenhaushalt 22 000 Euro,
3.  zuzüglich für jede weitere zum Haushalt rechnende Person 4 900 Euro.

(3) Die Einkommensgrenzen nach Absatz 2 erhöhen oder verringern sich am 1.
Januar 2024 und am 1. Januar jedes darauf folgenden vierten Jahres um den Prozentsatz, um den sich das vom Amt für Statistik Berlin-Brandenburg im Rahmen der „Volkswirtschaftlichen Gesamtrechnung der Länder“ ermittelte „Verfügbare Einkommen der privaten Haushalte je Einwohner in Brandenburg“ verändert (Dynamisierte Veränderung).
Die Veränderung der Einkommensgrenzen entspricht dem Prozentsatz, um den sich das „Verfügbare Einkommen der privaten Haushalte je Einwohner in Brandenburg“ für das jüngste statistisch aufbereitete Jahr gegenüber jenem Wert, der für die vorangegangene Festsetzung der Einkommensgrenze angewendet wurde, verändert hat.
Die nach Maßgabe der Sätze 1 und 2 veränderten Einkommensgrenzen werden auf volle hundert Euro aufgerundet und durch das für Wohnungswesen zuständige Mitglied der Landesregierung im Amtsblatt für Brandenburg bekannt gegeben.

(4) Sind zum Haushalt rechnende Personen Kinder im Sinne des § 32 Absatz 1 bis 5 des Einkommensteuergesetzes, erhöht sich die maßgebliche Einkommensgrenze nach den Absätzen 2 und 3 für jedes Kind um weitere 2 000 Euro.
Gleiches gilt, wenn die Geburt eines Kindes aufgrund einer bestehenden Schwangerschaft zu erwarten ist.

(5) Das für Wohnungswesen zuständige Mitglied der Landesregierung wird ermächtigt, durch Rechtsverordnung von den in den Absätzen 2 bis 4 bezeichneten Einkommensgrenzen nach den örtlichen und regionalen wohnungswirtschaftlichen Verhältnissen, insbesondere zur Berücksichtigung von Haushalten mit besonderen Schwierigkeiten bei der Wohnraumversorgung, im Rahmen der Förderung von selbst genutztem Wohneigentum oder zur Schaffung oder Erhaltung sozial stabiler Bewohnerstrukturen, Abweichungen festzulegen.

#### § 23 Maßgebendes Einkommen

Maßgebendes Einkommen ist das jährliche Gesamteinkommen des Haushalts.
Gesamteinkommen des Haushalts ist die Summe der Jahreseinkommen der nach § 3 Absatz 8 zu berücksichtigenden Haushaltangehörigen abzüglich der Frei- und Abzugsbeträge nach § 26.
Für die Berechnung werden die Verhältnisse im Zeitpunkt der Antragstellung zugrunde gelegt.

#### § 24 Einkommensermittlung

Das nach diesem Gesetz maßgebende Jahreseinkommen ist nach den §§ 14 bis 16 des Wohngeldgesetzes vom 24.
September 2008 (BGBl. I S. 1856), das zuletzt durch Artikel 22 Absatz 4 des Gesetzes vom 11.
November 2016 (BGBl. I S. 2500, 2515) geändert worden ist, in der jeweils geltenden Fassung zu berechnen, soweit dieses Gesetz nichts anderes bestimmt.
Wohngeldrechtliche Vorschriften, die sich allein auf Wohngeld beziehen, sind dabei nicht anzuwenden.

#### § 25 Zeitraum für die Ermittlung des Einkommens

Bei der Ermittlung des Jahreseinkommens ist das Einkommen zugrunde zu legen, das innerhalb der letzten zwölf Monate vor dem Monat der Antragstellung erzielt worden ist.
Hat sich in diesem Zeitraum das monatliche Einkommen auf Dauer geändert, ist das Zwölffache des geänderten monatlichen Einkommens unter Hinzurechnung jahresbezogener Leistungen zugrunde zu legen.
Satz 2 gilt entsprechend, wenn eine solche Änderung innerhalb von zwölf Monaten ab dem Monat der Antragstellung mit Sicherheit zu erwarten ist.
Änderungen, deren Beginn oder Ausmaß nicht ermittelt werden können, bleiben außer Betracht.
Bei Einkünften, deren Höhe mit einer Gewinnermittlung nach § 4 des Einkommensteuergesetzes festgestellt wird, ist das Einkommen zugrunde zu legen, das durchschnittlich in den beiden Wirtschaftsjahren vor dem Monat der Antragstellung erzielt worden ist.

#### § 26 Frei- und Abzugsbeträge

(1) Bei der Ermittlung des Gesamteinkommens werden folgende Freibeträge abgesetzt:

1.  4 500 Euro für jede zum Haushalt gehörende Person mit einem Grad der Behinderung von wenigstens 50,
2.  600 Euro für jedes zum Haushalt gehörende Kind, für das Kindergeld nach dem Einkommensteuergesetz oder dem Bundeskindergeldgesetz oder eine Leistung im Sinne des § 65 Absatz 1 des Einkommensteuergesetzes oder des § 4 Absatz 1 des Bundeskindergeldgesetzes gewährt wird, wenn die antragsberechtigte Person allein mit einem oder mehreren Kindern zusammenwohnt und wegen Erwerbstätigkeit oder Ausbildung nicht nur kurzfristig vom Haushalt abwesend ist.

(2) Als Abzugsbeträge werden Aufwendungen zur Erfüllung gesetzlicher Unterhaltsverpflichtungen bis zu dem in einer notariell beurkundeten Unterhaltsvereinbarung festgelegten oder in einem Unterhaltstitel oder Unterhaltsbescheid festgestellten Betrag abgesetzt.
Liegen keine Nachweise nach Satz 1 vor, können Aufwendungen zur Erfüllung gesetzlicher Unterhaltsverpflichtungen wie folgt abgesetzt werden:

1.  bis zu 3 000 Euro für eine haushaltsangehörige Person, die auswärts untergebracht ist und sich in der Berufsausbildung befindet,
2.  bis zu 6 000 Euro für einen nicht zum Haushalt zu rechnenden früheren oder dauernd getrennt lebenden Ehegatten oder eine Ehegattin oder den Lebenspartner oder die Lebenspartnerin nach dem Lebenspartnerschaftsgesetz oder den Partner oder die Partnerin einer sonstigen auf Dauer angelegten Lebensgemeinschaft,
3.  bis zu 3 000 Euro für eine sonstige nicht zum Haushalt zu rechnende Person,
4.  bis zu 4 000 Euro für ein Kind dauernd getrennt lebender oder geschiedener Eltern, denen das elterliche Sorgerecht uneingeschränkt gemeinsam zusteht, für Aufwendungen, die an das Kind als Haushaltsmitglied bei dem anderen Elternteil geleistet werden.

### Abschnitt 5  
Ergänzungs- und Schlussvorschriften

#### § 27 Zuständigkeiten; Verordnungsermächtigung

(1) Zuständige Stellen im Sinne dieses Gesetzes sind die Ämter, Verbandsgemeinden, amtsfreien Gemeinden, mitverwalteten Gemeinden, mitverwaltenden Gemeinden und kreisfreien Städte.
Örtlich zuständig ist die Körperschaft, in deren Gebiet sich der geförderte Wohnraum befindet oder die Person, die einen Wohnberechtigungsschein beantragt, wohnt oder wohnen will.
Die Aufgaben der zuständigen Stellen werden als Pflichtaufgaben zur Erfüllung nach Weisung wahrgenommen.

(2) Bewilligungsstelle im Sinne dieses Gesetzes ist die Investitionsbank des Landes Brandenburg.
In den Fällen des § 4 Absatz 1 Satz 2 sind Bewilligungsstellen die Landkreise, Ämter, Verbandsgemeinden, amtsfreien Gemeinden, mitverwalteten Gemeinden, mitverwaltenden Gemeinden und kreisfreien Städte, soweit Wohnraum ausschließlich mit deren Mitteln gefördert worden ist.
Haben Landkreise, Ämter, Verbandsgemeinden, amtsfreie Gemeinden, mitverwaltete Gemeinden, mitverwaltende Gemeinden und kreisfreie Städte gemeinsam Mittel zur Verfügung gestellt, so ist diejenige Stelle Bewilligungsstelle, die den überwiegenden Anteil der Mittel zur Verfügung gestellt hat.

(3) Die Aufgaben der zuständigen Verwaltungsbehörden für die Verfolgung und Ahndung von Ordnungswidrigkeiten im Sinne des § 28 Absatz 2 und 3 nehmen die zuständigen Stellen wahr.

(4) Die Sonderaufsicht über die Ämter, Verbandsgemeinden, amtsfreien Gemeinden, mitverwalteten Gemeinden und mitverwaltenden Gemeinden als zuständige Stellen führt der Landrat als allgemeine untere Landesbehörde.
Die Sonderaufsicht über die kreisfreien Städte als zuständige Stellen führt das für Wohnungswesen zuständige Mitglied der Landesregierung.

(5) Die Sonderaufsichtsbehörden können Weisungen erteilen, um die gesetzmäßige Erfüllung der Aufgaben nach diesem Gesetz zu sichern.
Zur zweckmäßigen Erfüllung der Aufgaben dürfen die Sonderaufsichtsbehörden

1.  das Unterrichtungsrecht ausüben,
2.  allgemeine Weisungen erteilen, um die gleichmäßige Durchführung der Aufgaben zu sichern,
3.  besondere Weisungen erteilen, wenn das Verhalten der zuständigen Stellen zur Erledigung ihrer Aufgaben nicht geeignet erscheint oder wenn es überörtliche Interessen oder die Verwirklichung der Förderungsziele gebieten,
4.  die Befugnisse der zuständigen Stellen selbst auf deren Kosten ausüben, wenn erteilte Weisungen nach Nummer 3 von den zuständigen Stellen nicht innerhalb der bestimmten Frist durchgeführt werden.

(6) Die Fachaufsicht über die Investitionsbank des Landes Brandenburg führt das für Wohnungswesen zuständige Mitglied der Landesregierung.

(7) Die Kommunen erhalten einen vollständigen Ausgleich der im Rahmen einer sparsamen und wirtschaftlichen Aufgabenwahrnehmung entstehenden Kosten der Wahrnehmung der Aufgaben nach diesem Gesetz.
Der Ausgleich soll grundsätzlich über pauschalierte Zahlungen erfolgen.
Hierzu wird das für Wohnungswesen zuständige Mitglied der Landesregierung ermächtigt, im Einvernehmen mit den für Inneres und für Finanzen zuständigen Mitgliedern der Landesregierung durch Rechtsverordnung den gemäß Artikel 97 Absatz 3 Satz 2 und 3 der Verfassung des Landes Brandenburg gebotenen Ausgleich der Mehrbelastungen der Gemeinden und Gemeindeverbände infolge der Aufgabenübertragung nach diesem Gesetz zu bestimmen.
Dabei sind insbesondere Regelungen zu treffen über

1.  die Sachkosten, Gemeinkosten und Personalkosten sowie die Einnahmen aus Gebühren, die auch durch einen geeigneten, sachkundigen Dritten ermittelt werden können; den Kommunen obliegt im Rahmen der Kostenermittlung eine Auskunftspflicht gegenüber dem Land,
2.  die Höhe von möglichen Pauschalen und ihre jeweilige Fortschreibung an die Kostenentwicklung,
3.  die Berücksichtigung regionaler Unterschiede,
4.  den Verteilschlüssel sowie
5.  weitere Einzelheiten zu den Voraussetzungen und zu dem Verfahren der Kostenausgleichung.

#### § 28 Maßnahmen bei Verstößen

(1) Für die Zeit, während der die verfügungsberechtigte Person schuldhaft gegen die Regelungen der §§ 13, 15 Absatz 1 und 3, § 18 Absatz 2 und 3 sowie § 19 Absatz 1 und 2 verstößt, kann die hierfür zuständige Stelle für die Dauer des Verstoßes durch Verwaltungsakt von der verfügungsberechtigten Person Geldleistungen bis zu monatlich zehn Euro je Quadratmeter Wohnfläche des Wohnraums, auf die sich der Verstoß bezieht, erheben.
Für die Bemessung der Höhe der Geldleistungen sind ausschließlich der Wohnwert des Wohnraums und die Schwere des Verstoßes maßgebend.

(2) Ordnungswidrig handelt, wer

1.  entgegen § 13 Wohnraum zum Gebrauch überlässt,
2.  entgegen § 15 Absatz 1 Satz 1 Wohnraum gegen eine höhere als die höchstzulässige Miete zum Gebrauch überlässt,
3.  entgegen § 15 Absatz 2 eine dort genannte Leistung fordert, sich versprechen lässt oder annimmt,
4.  entgegen § 15 Absatz 3 Satz 1 die in der Förderzusage enthaltenen Bestimmungen über die Mietbindung und deren Dauer nicht im Mietvertrag angibt,
5.  ohne Genehmigung nach § 18 Absatz 2 Satz 1 Nummer 1 oder Nummer 2 Wohnraum selbst nutzt oder nicht nur vorübergehend, mindestens drei Monate, leer stehen lässt,
6.  ohne Genehmigung nach § 18 Absatz 2 Satz 1 Nummer 3 Wohnraum anderen als Wohnzwecken zuführt oder entsprechend baulich ändert,
7.  entgegen § 19 Absatz 1 oder Absatz 2 eine Mitteilung nicht, nicht richtig, nicht vollständig oder nicht rechtzeitig macht oder
8.  entgegen § 19 Absatz 4 Satz 1 der zuständigen Stelle Auskunft nicht erteilt, Einsicht in seine Unterlagen nicht gewährt oder die Besichtigung verweigert.

(3) Die Ordnungswidrigkeit kann in den Fällen des Absatzes 2 Nummer 4, 7 und 8 mit einer Geldbuße bis zu zweitausendfünfhundert Euro, in den Fällen des Absatzes 2 Nummer 1 und 5 mit einer Geldbuße bis zu zehntausend Euro, in den übrigen Fällen mit einer Geldbuße bis zu fünfzigtausend Euro geahndet werden.

#### § 29 Übergangsvorschrift

(1) Auf Wohnraum, der nach dem Zweiten Wohnungsbaugesetz in der Fassung der Bekanntmachung vom 19.
August 1994 (BGBl. I S. 2137), das zuletzt durch Artikel 2 des Gesetzes vom 13. September 2001 (BGBl. I S. 2376, 2393) geändert worden ist, oder dem Wohnraumförderungsgesetz vom 13.
September 2001 (BGBl. I S. 2376), das zuletzt durch Artikel 3 des Gesetzes vom 2.
Oktober 2015 (BGBl. I S. 1610, 1612) geändert worden ist, gefördert worden ist, finden die bisher geltenden Vorschriften weiter Anwendung, soweit nicht in Absatz 2 Abweichendes bestimmt ist.

(2) Abweichend von Absatz 1 richten sich

1.  die Erteilung eines Wohnberechtigungsscheins nach § 14,
2.  die Freistellung und Änderung von Belegungs- und Mietbindungen nach § 17,
3.  die Sicherung der Zweckbestimmung nach § 18,
4.  die Bestimmung der Einkommensgrenzen nach § 22 und
5.  die Einkommensermittlung nach den §§ 23 bis 26.

#### § 30 Inkrafttreten

Dieses Gesetz tritt am ersten Tag des vierten auf die Verkündung folgenden Kalendermonats in Kraft.
Abweichend von Satz 1 treten § 4 Absatz 3 und § 22 Absatz 5 am Tag nach der Verkündung in Kraft.

Potsdam, den 5.
Juni 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark