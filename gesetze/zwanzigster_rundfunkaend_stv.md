## Gesetz zum Zwanzigsten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Zwanzigster Rundfunkänderungsstaatsvertrag)

Der Landtag hat das folgende Gesetz beschlossen:

### § 1

Dem am 8.
Dezember 2016 vom Land Brandenburg unterzeichneten Zwanzigsten Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Zwanzigster Rundfunkänderungsstaatsvertrag) wird zugestimmt.
Der Staatsvertrag wird nachstehend veröffentlicht.

### § 2

Durch dieses Gesetz wird das Recht auf Datenschutz (Artikel 11 der Verfassung des Landes Brandenburg) eingeschränkt.

### § 3

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Staatsvertrag tritt nach seinem Artikel 4 Absatz 2 Satz 1 mit Ausnahme des Artikels 3 am 1. September 2017 in Kraft.
Artikel 3 des Staatsvertrages tritt nach Artikel 4 Absatz 2 Satz 2 am 1. Januar 2017 in Kraft.

(3) Sollte der Staatsvertrag nach seinem Artikel 4 Absatz 2 Satz 3 gegenstandslos werden, ist dies unverzüglich im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 6.
Juni 2017

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag](/de/vertraege-244644) - Rundfunkstaatsvertrag

[zum Staatsvertrag](/vertraege/dlr_stv/14) - Deutschlandradio-Staatsvertrag

[zum Staatsvertrag](/vertraege/rfinstv) - Rundfunkfinanzierungsstaatsvertrag

* * *

### Anlagen

1

[Zwanzigster Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Zwanzigster Rundfunkänderungsstaatsvertrag)](/br2/sixcms/media.php/68/GVBl_I_12_2017_001Anlage-zum-Hauptdokument.pdf "Zwanzigster Staatsvertrag zur Änderung rundfunkrechtlicher Staatsverträge (Zwanzigster Rundfunkänderungsstaatsvertrag)") 856.4 KB