## Gesetz zur Gewährung einer einmaligen Sonderzahlung aus Anlass der COVID-19-Pandemie an Besoldungsempfängerinnen und Besoldungsempfänger für das Land Brandenburg (Brandenburgisches Corona-Sonderzahlungsgesetz - BbgCorSZG)

#### § 1 Geltungsbereich

(1) Dieses Gesetz regelt die Gewährung einer Sonderzahlung zur Abmilderung der zusätzlichen Belastung durch die COVID-19-Pandemie (Corona-Sonderzahlung).

(2) Die Corona-Sonderzahlung nach diesem Gesetz erhalten:

1.  Beamtinnen und Beamte des Landes bis einschließlich der Besoldungsgruppe B 8, Richterinnen und Richter des Landes sowie Beamtinnen und Beamte der Gemeinden, der Gemeindeverbände und der sonstigen der Aufsicht des Landes unterstehenden Körperschaften, Anstalten und Stiftungen des öffentlichen Rechts mit Ausnahme der Ehrenbeamtinnen und Ehrenbeamten sowie der ehrenamtlichen Richterinnen und Richter,
2.  Empfängerinnen und Empfänger von Anwärterbezügen sowie
3.  Bedienstete, die in einem öffentlich-rechtlichen Ausbildungsverhältnis stehen.

(3) Dieses Gesetz gilt nicht für die Mitglieder der Landesregierung.

(4) Dieses Gesetz gilt nicht für die öffentlichen Religionsgemeinschaften und ihre Verbände.

#### § 2 Höhe und Voraussetzungen für die Entstehung des Anspruchs

(1) Berechtigte nach § 1 Absatz 2 Nummer 1 erhalten eine Corona-Sonderzahlung in Höhe von 1 300 Euro.
Die Sonderzahlung wird nur gewährt, wenn

1.  ein Dienstverhältnis am 29.
    November 2021 und
2.  mindestens an einem Tag zwischen dem 1.
    Januar 2021 und dem 29.
    November 2021 ein Anspruch auf laufende Bezüge aus diesem Dienstverhältnis.

bestand.

(2) Für Berechtigte nach § 1 Absatz 2 Nummer 2 und 3 beträgt die Corona-Sonderzahlung 650 Euro.
Die Sonderzahlung wird nur gewährt, wenn

1.  ein Dienst- oder öffentlich-rechtliches Ausbildungsverhältnis am 29.
    November 2021 und
2.  mindestens an einem Tag zwischen dem 1.
    Januar 2021 und dem 29.
    November 2021 ein Anspruch auf laufende Bezüge aus diesem Verhältnis

bestand.

#### § 3 Teilzeit- und Konkurrenzregelungen

(1) Teilzeitbeschäftigte erhalten die Corona-Sonderzahlung entsprechend dem Verhältnis der ermäßigten zur regelmäßigen wöchentlichen Arbeitszeit.
§ 6 Absatz 1 des Brandenburgischen Besoldungsgesetzes gilt entsprechend.
Maßgeblich sind die Verhältnisse zum 29. November 2021.

(2) Der Anspruch richtet sich gegen den Dienstherrn, der die Bezüge zu dem Stichtag zu zahlen hat.

(3) Die Corona-Sonderzahlung bleibt bei der Berechnung sonstiger Besoldungsleistungen unberücksichtigt.

(4) Beim Zusammentreffen von Corona-Sonderzahlung nach diesem Gesetz mit Versorgungsbezügen nach dem Brandenburgischen Beamtenversorgungsgesetz gilt die Corona-Sonderzahlung nicht als Erwerbseinkommen im Sinne von § 74 des Brandenburgischen Beamtenversorgungsgesetzes.

#### § 4 Zahlungszeitpunkt

Die Corona-Sonderzahlungen nach § 2 werden mit den laufenden Bezügen bis spätestens zum 31. März 2022 ausgezahlt.