## Gesetz zum Sechsten Staatsvertrag zur Änderung des Staatsvertrages über die Zusammenarbeit zwischen Berlin und Brandenburg im Bereich der Medien

Der Landtag hat das folgende Gesetz beschlossen:

#### § 1 

Dem in Potsdam am 26.
März 2019 vom Land Brandenburg unterzeichneten Sechsten Staatsvertrag zur Änderung des Staatsvertrages über die Zusammenarbeit zwischen Berlin und Brandenburg im Bereich der Medien wird zugestimmt.
Der Vertrag wird nachstehend veröffentlicht.

#### § 2 

(1) Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.

(2) Der Tag, an dem der Staatsvertrag nach seinem Artikel 2 Absatz 1 in Kraft tritt, ist im Gesetz- und Verordnungsblatt für das Land Brandenburg Teil I bekannt zu geben.

Potsdam, den 19.
Juni 2019

Die Präsidentin  
des Landtages Brandenburg

Britta Stark

* * *

[zum Staatsvertrag](/vertraege/medien_stv_2014) - Staatsvertrag über die Zusammenarbeit zwischen Berlin und Brandenburg im Bereich der Medien

* * *

### Anlagen

1

[Sechster Staatsvertrag zur Änderung des Staatsvertrages über die Zusammenarbeit zwischen Berlin und Brandenburg im Bereich der Medien](/br2/sixcms/media.php/68/GVBl_I_44_2019-Anlage.pdf "Sechster Staatsvertrag zur Änderung des Staatsvertrages über die Zusammenarbeit zwischen Berlin und Brandenburg im Bereich der Medien") 491.1 KB